/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_CS3KCOMMSERVERMCOM_H
#define _INC_CS3KCOMMSERVERMCOM_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifdef _MSC_VER

	// 6/23/2014 ajv : Cause all structures to be packed when compiling into a
	// DLL for use with the comm server.
	#pragma pack(1)

	// 6/23/2014 ajv : Cause the attribute command to be ignored when compiling
	// into a DLL for use with the comm server.
	#define __attribute__(A) /* do nothing */

	// 6/23/2014 ajv : Disable Visual Studio-specific warnings about using
	// unsecure routines such as sprintf (the secure version is sprintf_s)
	#define _CRT_SECURE_NO_WARNINGS

	// 7/11/2017 ajv : Per the Microsoft Visual Studio 2017 documentation:
	//
	//	"Beginning with the UCRT in Visual Studio 2015 and Windows 10, snprintf is no
	//	longer identical to _snprintf. The snprintf function behavior is now C99
	//	standard compliant."
	// 
	// Therefore, we only need to convert our snprintf to Visual Studio's version if
	// we're running VS 2013 or older.
	#if _MSC_VER <= 12

		// 6/23/2014 ajv : Define our internal snprintf to use Visual Studio's
		// built-in snprintf routine.
		#define snprintf _snprintf

	#endif

	// 9/3/2014 ajv : Create a new extern definition for routines that need to
	// be externally available for the comm server to use such as the one to 
	// parse alert lines.
	#define DLLEXPORT extern __declspec(dllexport)

#else

	// 9/3/2014 ajv : Create a native-C friendly version of the define to allow
	// access to routines externally.
	#define DLLEXPORT extern

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/27/2015 rmd : Had to move these out of the "weather_tables.h" file as these structures
// end up being included into 4 projects now. Not used by the tpmicro but get sucked into
// that code by virtue of the "packet_definitions" file.

#define		ET_STATUS_HISTORICAL							(0)
#define		ET_STATUS_FROM_ET_GAGE							(1)
#define		ET_STATUS_MANUALLY_EDITED_AT_CONTROLLER			(2)
#define		ET_STATUS_MANUALLY_EDITED_AT_THE_CENTRAL		(3)
// 9/24/2012 rmd : The implication is from another network with an actual et-gage.
#define		ET_STATUS_SHARED_FROM_THE_CENTRAL				(4)
#define		ET_STATUS_FROM_WEATHERSENSE						(5)

typedef struct
{ 
	// 9/14/2012 rmd : The GAGE pulses represent a hundredth of an inch. So as the pulses come
	// in this is bumped by one. And represent the total count of pulses so far this day. To
	// figure ET as inches divide this number by 100. This number can also be used to hold other
	// sources of ET. Such as user edited. It is kept as an UNS_16 to keep the overall table
	// size down (we sync the whole table through the communications). Additionally UNS_16 is
	// how we held this in previous product so there is a historical reason through that carries
	// little weight.
	UNS_16	et_inches_u16_10000u;
	
	UNS_16	status;

} ET_TABLE_ENTRY;

// ----------

#define		RAIN_STATUS_FROM_RAIN_BUCKET_M					(0)  // meaning below the minimum
#define		RAIN_STATUS_FROM_RAIN_BUCKET_R					(1)  // meaning above the minimum
#define		RAIN_STATUS_MANUALLY_EDITED_AT_CONTROLLER		(2)
#define		RAIN_STATUS_MANUALLY_EDITED_AT_THE_CENTRAL		(3)
// 9/24/2012 rmd : The implication is from another network with an actual rain bucket.
#define		RAIN_STATUS_SHARED_FROM_THE_CENTRAL				(4)
#define		RAIN_STATUS_FROM_WEATHERSENSE					(5)

typedef struct
{ 
	UNS_16	rain_inches_u16_100u;
	
	UNS_16	status;

} RAIN_TABLE_ENTRY;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern char *STATION_get_station_number_string( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, char *pstr, const UNS_32 pstrlen );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

