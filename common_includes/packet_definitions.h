/* file = packet_definitions.h                 2012.08.17 rmd */

/* ---------------------------------------------------------- */

#ifndef _INC_PACKET_DEFS_H
#define _INC_PACKET_DEFS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	"lpc_types.h"

#include	"cal_td_utils.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/16/2012 rmd : The bytes in this definition are REVERSED. This is intentional. Don't
// flip them around. The reason is because we are using our packet definitions when sending
// ourselves our own code during initial production loading and field upgrades. If the
// preamble and postamble are embedded within in the code (which they are) and they are in
// the correct order for a packet definition they then trip up the communications. Causing
// false packets and bad CRC's. Reversing the bytes IN THE DEFINITION solves that problem.
// They are not reversed when they are tacked onto the packets however.

typedef struct
{ 
	UNS_8   _4;

	UNS_8   _3;

	UNS_8   _2;

	UNS_8   _1;
	
} __attribute__((packed)) AMBLE_TYPE;

// 8/16/2012 rmd : The bytes in this definition are REVERSED. This is intentional. Don't
// flip them around. The reason is because we are using our packet definitions when sending
// ourselves our own code during initial production loading and field upgrades. If the
// preamble and postamble are embedded within in the code (which they are) and they are in
// the correct order for a packet definition they then trip up the communications. Causing
// false packets and bad CRC's.  Reversing the bytes IN THE DEFINITION solves that problem.
// They are not reversed when they are tacked onto the packets however.

extern const AMBLE_TYPE preamble;

extern const AMBLE_TYPE postamble;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{ 
	UNS_8	from[ 3 ];

	UNS_8	to[ 3 ];

}__attribute__((packed)) ADDR_TYPE;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// PACK these variables - we count on a known size
// the TPL_DATA_HEADER_TYPE must be 12 bytes. If we don't pack it will be 14.

typedef union
{
	UNS_8	B[ 2 ];
	
	UNS_16	S;

}__attribute__((packed)) MID_TYPE;


extern MID_TYPE  OM_MID;   /* for outgoing messages and incoming status */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// the routing AND outoging message classifications
// have a dual use
// the value choices are critical
// DO NOT CHANGE
//
// well lets set some guidelines about changing ... FIRST of all notice the unused values
// there are 4 bits and we can use values ONLY from 0..0x0F so we've assigned them ALL
// you cannot add any without adding a bit (which of course would double the allowable
// values) ... so if you need to add take one of the unused values and use it ... SECOND
// do not rearrange exisiting values else you will make an incompatbility at the communication
// level - which gets at another point should roll the communication version when these are
// changed
//
// unless central incorporates this scheme the FROM_CENTRAL must be 0
//
// REGARDING STATUS PACKETS THAT AN IM GENERATES: they take on the message_class
// of what the sender of the IM class would expect as a response - for example an
// IM of type FROM_CENTRAL would make a status of type TO_CENTRAL ... this way
// we can be assured the status packet would route through the system just as any
// application level response to the IM would.

#define MSG_CLASS_2000e_FROM_CENTRAL					(0)
#define MSG_CLASS_2000e_TO_CENTRAL						(1)

// This is the message class for the test packet between controllers or between the central
// and a controller (C_OR_C means CONTROLLER_OR_CENTRAL)
#define MSG_CLASS_2000e_TEST_PACKET_FROM_C_OR_C			(2)
#define MSG_CLASS_2000e_TEST_PACKET_ECHO_TO_C_OR_C		(3)

#define MSG_CLASS_2000e_SCAN							(4)
#define MSG_CLASS_2000e_SCAN_RESP						(5)
										  
#define MSG_CLASS_2000e_TOKEN							(6)
#define MSG_CLASS_2000e_TOKEN_RESP						(7)

#define MSG_CLASS_2000e_FROM_RRE	  					(8)
#define MSG_CLASS_2000e_TO_RRE							(9)

// 7/3/2013 rmd : This message class value was/is used in the 2000e. It's use was for
// communication between TPL_OUT and the COMM_MNGR. Remember we did not have the luxury of
// an OS (like FreeRTOS here). But anyway it was used in the 2000e and want to keep it as
// reserved here in the CS3000. Though could probably use it as the use in the 2000e was
// strictly internal. Meaning a packet with this class never hit the wires (or air).
#define MSG_CLASS_2000e_not_used_in_CS3000				(10)

// Needed to add this test packet class for two reasons ... when from RRE need to look at to
// SERIAL NUMBER instead of address and as a test packet it was reasoned desireable need to
// suppress the transport status.
#define MSG_CLASS_2000e_TEST_PACKET_FROM_RRE			(11)
#define MSG_CLASS_2000e_TEST_PACKET_ECHO_TO_RRE			(12)

#define MSG_CLASS_2000e_TEST_PACKET_FROM_CENTRAL		(13)
#define MSG_CLASS_2000e_TEST_PACKET_ECHO_TO_CENTRAL		(14)

// 7/3/2013 rmd : This message class was and is truly unused in the 2000e. So is available
// for use in the CS3000. We will use it though as a marker telling us to pull the real
// message class from the as of yet unused 'dummy' bytes present in the packet header
// definition.
#define MSG_CLASS_2000e_CS3000_FLAG						(15)


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/3/2013 rmd : The newly introduced messages classes for use within the CS3000. We had
// practically speaking consumed all 16 message classes in the legacy packet header
// definition. Though we may only need a few new ones for the CS3000 we had no expansion
// provision. I decided to bite down and make provision for many more classes within the
// existing packet header. We use the dummy byte that has been in the header since
// inception. Loading that dummy byte with the msg class value. I decided to use the dummy
// byte instead of expanding to 5 bits within the TPL_PACKET_ID itself because I couldn't
// guarantee that the 5th bit is always 0 in all legacy controllers. And if we make a router
// that has to handle both older packets and the newer CS3000 packets using a FLAG message
// class and the unused dummy byte within the STATUS and DATA headers is the way to go.
// Note: the FLAG CLASS is an unused value in all prior controllers.

// 7/11/2013 rmd : Note the scheme carefully allows 2000e packets to pass through a router
// that can handle 2000e AND CS3000 packets. [The CONVERSE is not TRUE. Meaning a 2000e
// based router will not properly handle CS3000 packets.]

// 7/11/2013 rmd : The scheme goes like this : when the 4-bit legacy ROUTING_CLASS takes on
// the FLAG value we turn to the new CS3000_MSG_CLASS byte for the class value.

#define	MSG_CLASS_CS3000_no_longer_used_0							(0)		// not yet used

#define	MSG_CLASS_CS3000_TO_COMMSERVER_VIA_HUB						(1)

#define	MSG_CLASS_CS3000_FROM_COMMSERVER							(2)

#define MSG_CLASS_CS3000_SCAN										(3)
#define MSG_CLASS_CS3000_SCAN_RESP									(4)
										  
#define MSG_CLASS_CS3000_TOKEN										(5)
#define MSG_CLASS_CS3000_TOKEN_RESP									(6)

#define MSG_CLASS_CS3000_TO_TP_MICRO								(7)
#define MSG_CLASS_CS3000_FROM_TP_MICRO								(8)


#define	MSG_CLASS_CS3000_no_longer_used_9							(9)		// not yet used

#define	MSG_CLASS_CS3000_no_longer_used_10							(10)	// not yet used

// ----------

// 3/31/2014 rmd : Two code distribution classes. One for from the comm server. And one for
// the controller to controller distribution. So we can handle, if needed, these packets
// differently.
#define MSG_CLASS_CS3000__FROM_COMMSERVER__CODE_DISTRIBUTION		(11)

#define MSG_CLASS_CS3000__FROM_CONTROLLER__CODE_DISTRIBUTION		(12)

// 7/12/2017 rmd : This packet class is BROADCAST. All controllers who 'hear' it, accept it
// and process it.
#define MSG_CLASS_CS3000__FROM_HUB__CODE_DISTRIBUTION_BROADCAST		(13)

// 7/12/2017 rmd : We introduced this packet class to allow the hub to controller by
// controller query the state of their code receipt.
#define MSG_CLASS_CS3000__FROM_HUB__CODE_DISTRIBUTION_ADDRESSABLE	(14)

// 7/11/2017 rmd : This packet must pass TO address test. In other words the HUB serial
// number must be the TO address.
#define MSG_CLASS_CS3000__TO_HUB__CODE_DISTRIBUTION_ADDRESSABLE		(15)

// ----------

#define MSG_CLASS_CS3000__TO_CONTROLLER__TEST_PACKET				(16)

#define MSG_CLASS_CS3000__TO_CONTROLLER__TEST_PACKET_ECHO			(17)

// ----------

// 7/18/2013 rmd : NOTE - as you add msg classes here, bump the following and add to the
// cs3000_msg_class_text array in the top of tpl_out.c.
//
// 7/11/2017 rmd : Being that we are using the FULL byte (so far) we can theoretically have
// 256 CLASSES for 3000 packets. Though think ahead it may be nice to reserve the 2 top
// order bits for some unknown use.
#define	MSG_CLASS_total_number_of_classes							(18)

/* ---------------------------------------------------------- */

// 3/29/2017 rmd : Support when determining the routing class of a packet.

// 3/29/2017 rmd : Using defines to make code more readable. Also these define values are
// used literally in the alerts. So DO NOT CHANGE them without considering that.
#define		ROUTING_CLASS_IS_2000_BASED		(2000)

#define		ROUTING_CLASS_IS_3000_BASED		(3000)

typedef struct
{
	// 4/13/2017 rmd : Because we do not have UNIQUE class numbers between 2000 packets and 3000
	// packets, we need this extra piece of information about which class_base they belong to:
	// 2000 or 3000.
	UNS_32		routing_class_base;

	UNS_32		rclass;

} ROUTING_CLASS_DETAILS_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 9/20/2012 rmd : Unfortunately bit field BIT ORDER varies from compiler to compiler. Some
	// compilers have switches to manage the order. Over the years we have relied upon the
	// default ordering. It seems the Green Hills and IAR compiler organize bit fields as MSBit
	// to LSBit. GCC organizes with the opposite with the LSBit first.
	//
	// This structure in particular wants the STATUS bit as the high order bit (bit 7). And the
	// SEND_ACK request as the next highest (bit 6). That is the way we have defined it in the
	// transport for many years. So it must stay this way.
	//
	// WE REALLY SHOULD BE USING MASKS AND BIT NUMBER DEFINES AND DO AWAY WITH THE BIT FIELD ALL
	// TOGETHER.

	// ----------------

	/*

			STATUS	SEND_ACKorEOC		MEANING
			0		0					Incoming Message packet - no request for status back to sender
			
			0		1					Incoming Message packet - send status packet about this IM back to sender

			1		0					Status packet for an OM

			1		1					not yet used - with reference to an OM - destroy it

	*/

	// ----------------

	// NOTE: This is the LSB in GCC.

	// ----------------

	// There was a bit here with the intention of using it to route packets around the transport
	// sort of speak ... for the test packet ... this can now be done with the routing_class.
	unsigned char  not_yet_used 	: 1;

	// ----------------

	// Routing class is assigned when the OM message is made ... it provides those who recieve a
	// packet with information about what message type this packet belongs to and therefore
	// allows the incoming packet router to handle the packet properly. The previous method of
	// doing this was to look at the TO/FROM addresses. This method was getting very complicated
	// and with the elimination of different addresses for the FOAL machine, IRRI machine, etc.
	// became damn near impossible.
	//
	// The value assignment is choosen to have no impact to the central which as of yet does not
	// utilize this setting ... therefore the FROM_CENTRAL class must be a 0 cause that is what
	// the central current sets these bits to by default ... I suppose the TO_CENTRAL will be a
	// 0001 ... that setting will be ignored by the central itself but the controller and
	// handheld routers will use.

	// 7/25/2013 rmd : UPDATE - no longer used as THE routing class for CS3000 packet
	// communication. This field is now a flag set to 0xF. We have taken over an unused byte in
	// the header for the CS3000. However the CS3000 could (if we develop the code) still route
	// 2000e packets. The packet design is backwards compatible.
	unsigned char  __routing_class	: 4;
	
	// the sender (originator) sets this bit based on should this message generate its own outgoing status's
	// to send to the originator if it doesn't see one ... also this same bit controls if the IM should track
	// that packets came in response to sending a status
	//
	// NOTE - we don't want both sides active, only one, and that is generally who is considered to be the master
	// of the exchange. UNDERSTAND - this bit is set by the sender (OM) and read and acted upon by the receiver (IM)
	//
	unsigned char  make_this_IM_active : 1;
	
	// 7/3/2013 rmd : This bit is set by the outgoing message driver indicating a request for
	// the receiving incoming message driver to send a status regarding the packet receipt
	// status for the message.
	unsigned char  request_for_status   : 1;
	
	// Is this a status packet to an OM or a packet for an IM.
	unsigned char  STATUS          : 1;

	// ----------------

	// NOTE: This is the MSB in GCC.

} __attribute__((packed)) TPL_PACKET_ID;



typedef struct
{
	// 7/12/2013 rmd : NOTE - the status header and the data header must be the same size and up
	// to and including the MID must be identical.

	TPL_PACKET_ID	PID;				// - 1 byte

	UNS_8			cs3000_msg_class;   // we'll start using the LSBits - the MSBits could be used someday for special meaning

	ADDR_TYPE		FromTo;				// to and from addresses - 6 bytes

	MID_TYPE		MID;				// message ID - 2 bytes

	UNS_8   		TotalBlocks;		// 1 based

	UNS_8   		ThisBlock;			// 1 based

} __attribute__((packed)) ___TPL_DATA_HEADER;


typedef union
{
	UNS_8				B[ sizeof(___TPL_DATA_HEADER) ];

	___TPL_DATA_HEADER	H;
	
} __attribute__((packed)) TPL_DATA_HEADER_TYPE;


typedef struct
{
	// 7/12/2013 rmd : NOTE - the status header and the data header must be the same size and up
	// to and including the MID must be identical.

	TPL_PACKET_ID	PID;				// 1 byte

	UNS_8			cs3000_msg_class;   // we'll start using the LSBits - the MSBits could be used someday for special meaning

	ADDR_TYPE		FromTo;				// to and from addresses - 6 bytes

	MID_TYPE		MID;				// message ID - 2 bytes

	UNS_8   		unused_in_status;	// 1 byte
	
	UNS_8   		AcksLength;			// 0 if 100% OK

} __attribute__((packed)) ___TPL_STATUS_HEADER;


typedef union
{
	UNS_8					B[ sizeof(___TPL_STATUS_HEADER) ];
	
	___TPL_STATUS_HEADER	H;
	
} __attribute__((packed)) TPL_STATUS_HEADER_TYPE;

// ----------

// 11/28/2017 rmd : This value comes from the 2000e. If you want to change it for the 3000
// you'll have to adjust packets based upon class, specifically the 2000e comm test packets
// need to remain at 496.
#define TPL_USER_DATA_MAX_SIZE	(496)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// must be two bytes ... what the communication interface is expecting
#define COMM_COMMANDS	UNS_16

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define MID_SCAN_TO_COMMMNGR							(40)
#define MID_SCAN_TO_COMMMNGR_RESP						(41)

// 4/14/2014 rmd : Sent after the scan. To NEW controllers in the chain as determined by the
// scan.
#define MID_FOAL_TO_IRRI__CLEAN_HOUSE_REQUEST			(60)
#define MID_IRRI_TO_FOAL__CLEAN_HOUSE_REQUEST_RESP		(61)

// Special clean house command for CRC error
#define MID_FOAL_TO_IRRI__CRC_ERROR_CLEAN_HOUSE_REQUEST			(62)

// 2011.10.26 rmd : This is the normal TOKEN and TOKEN_RESP message ID.
#define MID_FOAL_TO_IRRI__TOKEN							(80)	// from foal to irri
#define MID_IRRI_TO_FOAL__TOKEN_RESP					(81)	// from irri to foal


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*
	@DESCRIPTION - Message IDS between CS3000 and TPmicro
*/

// 8/3/2012 rmd : The message ID's between the main board and the TP Micro. These messages
// are UNSOLICITED messages. In that they are asynchronously sent (full duplex link). There
// is not a token and token-resp arrangement.
#define MID_MAIN_TO_TPMICRO							(90)



// 4/8/2013 rmd : Until all the bit positions have been taken this is the only message ID
// that is sent to the main board. The behavior to note however is that the formation and
// transmission of this message to the main board is not synchronized with traffic from the
// main board. The TP Micro builds up and send this message when it wants to. For example
// there is a function to build a msg with this MID that only sends ONE alert message.
// That's all that is in the message. And it is possible that back to back a second message
// is generated that contains other content.
#define MID_TPMICRO_TO_MAIN_PRIMARY_MESSAGE			(91)


// 9/3/2013 rmd : Provides the TP Micro the ability to send and fill out his own screen full
// of text that shows in the main board GUI. Present update rate is once per 5 seconds. And
// presently used to show task information. And I think the error bit field.
#define MID_TPMICRO_TO_MAIN_DEBUG_DISPLAY			(92)


// 10/14/2014 rmd : Introduced a second message to hold things like alert lines. And in case
// we need more bits to feed the mian board information.
#define MID_TPMICRO_TO_MAIN_SECONDARY_MESSAGE		(93)


// 3/13/2013 rmd : This is a message that contains only the fc mid and pieces. No
// accompnaying data. It is the signal to the main board that it may send another packet
// that is not of the MSG_CLASS_TO_TP_MICRO type. This means a packet that likely is sent
// out the M1 and M2 ports.
#define MID_TPMICRO_TO_MAIN_SW_HANDSHAKE			(95)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


// WHEN YOU ADD A COMMAND DON'T FORGET TO ADD THE PARSED STRING FOR THE ALERT LINE WHEN THE COMMAND
// COMES IN
//

// ALL commands start with the mid word ( 2 bytes ) which take on the values above.
//
// Commands can then have different structures to follow.
//
// FOAL commands all then have 4 bytes called "pieces" (unsigned long) which is used to
// mark bits for the different pieces of data being carried along in the meat of the message.


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// FOAL COMMUNICATIONS FORMAT

// ALL commands start with the mid word ( 2 bytes ) which take on the values above.
//
// Commands can then have different structures to follow.
//
// FOAL commands all then have 4 bytes called "pieces" (unsigned long) which is used to
// mark bits for the different pieces of data being carried along in the meat of the message.

// 4/4/2012 rmd : PACKED this structure to force size to 6 bytes in the ARM environment!
typedef struct
{
	COMM_COMMANDS	mid;

	UNS_32			pieces;

} __attribute__((packed)) FOAL_COMMANDS;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/3/2014 rmd : Network code distribution packet definition. Considerations include a
// simplified packet header that does not need to support our transport protocol or
// funcitonality. Packets need to include a backwards compatible pre/post amble, crc
// location, and a header that does at a minimum include a backward compatible MESSAGE
// CLASS.
//
// It may be the packet definition between the COMM_SERVER and DOWNLOADER programs if a
// variation of this header. If we ever want traffic to survive an LR type system messages
// from the COMM_SERVER must include the NETWORK_ID.

// ----------

// 8/23/2013 rmd : CS3000 comm_server exchange MID's. These are the message ID's. NOT THE
// MSG CLASS!

#define		MID_TO_COMMSERVER_REGISTRATION_DATA_init_packet				(  0)
#define		MID_TO_COMMSERVER_REGISTRATION_DATA_data_packet				(  1)
#define		MID_FROM_COMMSERVER_REGISTRATION_DATA_full_receipt_ack		(  2)
#define		MID_FROM_COMMSERVER_REGISTRATION_DATA_NAK					(  0 + 1000 )

#define		MID_TO_COMMSERVER_ENGINEERING_ALERTS_init_packet			(  3)
#define		MID_TO_COMMSERVER_ENGINEERING_ALERTS_data_packet			(  4)
#define		MID_FROM_COMMSERVER_ENGINEERING_ALERTS_full_receipt_ack		(  5)
#define		MID_FROM_COMMSERVER_ENGINEERING_ALERTS_NAK					(  3 + 1000 )
	
#define		MID_TO_COMMSERVER_FLOW_RECORDING_init_packet				(  6)
#define		MID_TO_COMMSERVER_FLOW_RECORDING_data_packet				(  7)
#define		MID_FROM_COMMSERVER_FLOW_RECORDING_full_receipt_ack			(  8)
#define		MID_FROM_COMMSERVER_FLOW_RECORDING_NAK						(  6 + 1000 )

#define		MID_TO_COMMSERVER_STATION_HISTORY_init_packet				(  9)
#define		MID_TO_COMMSERVER_STATION_HISTORY_data_packet				( 10)
#define		MID_FROM_COMMSERVER_STATION_HISTORY_full_receipt_ack		( 11)
#define		MID_FROM_COMMSERVER_STATION_HISTORY_NAK						(  9 + 1000 )

#define		MID_TO_COMMSERVER_STATION_REPORT_DATA_init_packet			( 12)
#define		MID_TO_COMMSERVER_STATION_REPORT_DATA_data_packet			( 13)
#define		MID_FROM_COMMSERVER_STATION_REPORT_DATA_full_receipt_ack	( 14)
#define		MID_FROM_COMMSERVER_STATION_REPORT_DATA_NAK					( 12 + 1000 )

// ----------

// 2/5/2014 rmd : The comm_server likely would never use these two. The code_downloader
// application does however. And the code_downloader application is the model behaviour for
// the comm server application to mimic. So we include here to keep all MID definitions
// between COMM_SERVER and CODE_DOWNLOADER together in one place.
#define		MID_CODE_DISTRIBUTION_kickstart_init_packet					( 16)
#define		MID_CODE_DISTRIBUTION_kickstart_data_packet					( 17)

// 3/31/2014 rmd : This mid may only be sent to the comm_server. Letting the comm_server
// know we are ready to accept the code image. We wouldn't want to send that much data if
// the controller was dropping all those packets! Waste of data plan.
#define		MID_CODE_DISTRIBUTION_kickstart_init_ack					( 18)
#define		MID_CODE_DISTRIBUTION_kickstart_full_receipt_ack			( 19)

// ----------

// 3/31/2014 rmd : The following MAIN CODE AND TPMICRO CODE distribution MID's are used by
// both the COMM_SERVER and the CONTROLLER during the code distribution process. We can tell
// by the class used, if needed, if the packet came from the COMM_SERVER or was a
// CONTROLLER-to-CONTROLLER distribution.
#define		MID_CODE_DISTRIBUTION_main_init_packet						( 20)
#define		MID_CODE_DISTRIBUTION_main_data_packet						( 21)

// 3/31/2014 rmd : This MID is sent to the comm_server when he sends the code update message
// init packet to us. He waits to send the data packets until he sees this init. A final
// check that we are ready before he streams what may be many data packets (letting the
// comm_server know we are ready to accept the code image). We wouldn't want to send that
// much data if the controller was dropping all those packets! Waste of data plan.
#define		MID_CODE_DISTRIBUTION_main_init_ack							( 22)
#define		MID_CODE_DISTRIBUTION_main_full_receipt_ack					( 23)

// ----------

// 7/18/2017 rmd : Following the broadcast transmission of all the packets that make up a
// binary, the hub then queries each 3000 controller in the hub list. These are a single
// packet, addressable, message. Both directions are addressable.
#define		MID_CODE_DISTRIBUTION_hub_query_packet						( 24)
#define		MID_CODE_DISTRIBUTION_hub_query_packet_ack					( 25)

// ----------

#define		MID_CODE_DISTRIBUTION_tpmicro_init_packet					( 30)
#define		MID_CODE_DISTRIBUTION_tpmicro_data_packet					( 31)

// 3/31/2014 rmd : This MID is sent to the comm_server when he sends the code update message
// init packet to us. He waits to send the data packets until he sees this init. A final
// check that we are ready before he streams what may be many data packets (letting the
// comm_server know we are ready to accept the code image). We wouldn't want to send that
// much data if the controller was dropping all those packets! Waste of data plan.
#define		MID_CODE_DISTRIBUTION_tpmicro_init_ack						( 32)
#define		MID_CODE_DISTRIBUTION_tpmicro_full_receipt_ack				( 33)

// ----------

#define		MID_TO_COMMSERVER_POC_REPORT_DATA_init_packet				( 34)
#define		MID_TO_COMMSERVER_POC_REPORT_DATA_data_packet				( 35)
#define		MID_FROM_COMMSERVER_POC_REPORT_DATA_full_receipt_ack		( 36)
#define		MID_FROM_COMMSERVER_POC_REPORT_DATA_NAK						( 34 + 1000 )

#define		MID_TO_COMMSERVER_SYSTEM_REPORT_DATA_init_packet			( 37)
#define		MID_TO_COMMSERVER_SYSTEM_REPORT_DATA_data_packet			( 38)
#define		MID_FROM_COMMSERVER_SYSTEM_REPORT_DATA_full_receipt_ack		( 39)
#define		MID_FROM_COMMSERVER_SYSTEM_REPORT_DATA_NAK					( 37 + 1000 )

// ----------

// 4/15/2015 rmd : The check for update process is kicked off by the controller. The
// controller first send the init and data CHECK_FOR_UPDATES message contain the running
// code main and tpmicro revisions. The commserver responds with the CHECK_FOR_UPDATE_ACK
// mids. And then if code needs to be sent follows right up with the CODE_DISTRIBUTION init
// and data packets.
#define		MID_TO_COMMSERVER_CHECK_FOR_UPDATES_init_packet							( 40)
#define		MID_TO_COMMSERVER_CHECK_FOR_UPDATES_data_packet							( 41)
#define		MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates					( 42)
#define		MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_one_update_available			( 43)
#define		MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_two_updates_available			( 44)
//
// 2/10/2017 rmd : NOTE - this 40/41 pair also includes extended result MIDs 180 through
// 183. Generated by the commserver when the request came from a hub.

// ----------

#define		MID_TO_COMMSERVER_DISCONNECT_not_used									( 45)

// ----------

// 5/14/2015 rmd : This is the controller delivering the ET and RAIN data to the comserver.
// The controller will do this just after 8PM controller time. The commserver acks the
// message. We try a second time if not succesful (not yet implemented).
#define		MID_TO_COMMSERVER_WEATHER_DATA_init_packet								( 46)
#define		MID_TO_COMMSERVER_WEATHER_DATA_data_packet								( 47)
#define		MID_FROM_COMMSERVER_WEATHER_DATA_receipt_ack							( 48)
#define		MID_FROM_COMMSERVER_WEATHER_DATA_NAK									( 46 + 1000 )

// 5/27/2015 rmd : The weather data FROM the commserver. This message is sent from the
// commserver to the controllers at 8:05pm. The source of the data can be from WEATHERSENSE,
// or from controller delivered ET, or controller delivered RAIN. Or any combination of
// (weathersense ET and controller delivered RAIN is actually quite common). The message is
// a single packet blast message and is ACK'd by the controller.
#define		MID_FROM_COMMSERVER_WEATHER_DATA_packet									( 53)
#define		MID_TO_COMMSERVER_WEATHER_DATA_receipt_ack								( 56)

// ----------

// 5/14/2015 rmd : This message is sent by the controller anytime the rain bucket measured
// rain crosses a minimum amount (typically 0.1 inches). Receipt at the commserver should
// setup a SHUTDOWN message request for all controllers that use this controller rain bucket
// as their source of rain.
#define		MID_TO_COMMSERVER_RAIN_INDICATION_init_packet							( 57)
#define		MID_TO_COMMSERVER_RAIN_INDICATION_data_packet							( 58)
#define		MID_FROM_COMMSERVER_RAIN_INDICATION_receipt_ack							( 59)
#define		MID_FROM_COMMSERVER_RAIN_INDICATION_NAK									( 57 + 1000 )

// 5/27/2015 rmd : Single packet blast message from the commserver. Actually contain no
// data. But is ACK'd by the controller to acknowledge receipt so the commserver knows the
// controller has it.
#define		MID_FROM_COMMSERVER_RAIN_SHUTDOWN_packet								( 64)
#define		MID_TO_COMMSERVER_RAIN_SHUTDOWN_receipt_ack								( 67)

// ----------

#define		MID_TO_COMMSERVER_PROGRAM_DATA_init_packet								( 68)
#define		MID_TO_COMMSERVER_PROGRAM_DATA_data_packet								( 69)
#define		MID_FROM_COMMSERVER_PROGRAM_DATA_receipt_ack							( 70)
#define		MID_FROM_COMMSERVER_PROGRAM_DATA_NAK									( 68 + 1000 )

#define		MID_TO_COMMSERVER_PROGRAM_DATA_REQUEST_init_packet						( 71)
#define		MID_TO_COMMSERVER_PROGRAM_DATA_REQUEST_data_packet						( 72)
#define		MID_FROM_COMMSERVER_PROGRAM_DATA_REQUEST_ACK_NO_CHANGES					( 73)
#define		MID_FROM_COMMSERVER_PROGRAM_DATA_REQUEST_ACK_CHANGES_AVAILABLE			( 74)
#define		MID_FROM_COMMSERVER_PROGRAM_DATA_REQUEST_NAK							( 71 + 1000 )

#define		MID_FROM_COMMSERVER_PROGRAM_DATA_init_packet							( 75)
#define		MID_FROM_COMMSERVER_PROGRAM_DATA_data_packet							( 76)
#define		MID_TO_COMMSERVER_PROGRAM_DATA_init_ack									( 77)
#define		MID_TO_COMMSERVER_PROGRAM_DATA_receipt_ack								( 78)

// ----------

#define		MID_TO_COMMSERVER_VERIFY_FIRMWARE_VERSION_init_packet					( 79)
#define		MID_TO_COMMSERVER_VERIFY_FIRMWARE_VERSION_data_packet					( 80)
#define		MID_FROM_COMMSERVER_VERIFY_FIRMWARE_VERSION_ACK_FW_UP_TO_DATE			( 81)
#define		MID_FROM_COMMSERVER_VERIFY_FIRMWARE_VERSION_ACK_FW_OUT_OF_DATE			( 82)
 
// ----------

// 6/5/2015 rmd : The commserver sends an IHSFY (meaning has pdata for the controller). In
// response to that we send this message. Asking is our firmware up to date. If it is we in
// turn send the PROGRAM_DATA_REQUEST to the commserver. If it is not we send the standard
// CHECK FOR UPDATES command.

#define		MID_TO_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_init_packet				( 83)
#define		MID_TO_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_data_packet				( 84)
#define		MID_FROM_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_ACK_FW_UP_TO_DATE		( 85)
#define		MID_FROM_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_ACK_FW_OUT_OF_DATE		( 86)
 
// ----------

// 4/15/2015 rmd : This is the first of the "I Have Something For You" MID. The idea is the
// comm_server can send this to the controller using the ACK packet definition (which is a
// packet that contains only the FROM_COMMSERVER_TOP_PACKET_HEADER). The controller can then
// decide WHEN (not if - but when) it wants to ask the comm_server for the program data
// changes it has. If the controller has program data changes of its own to send, it will
// wait till that transaction has completed first. Then ask the comm_server for the changes
// it has. This is the scheme we use to ensure changes cannot cross in the communications
// transactions, potentially leaving the comm_server and controller program_data databases
// out of sync.
#define		MID_FROM_COMMSERVER_IHSFY_program_data									( 90)

// ----------

// 5/14/2015 rmd : This is a single packet UNACKNOWLEDGED message from the commserver to the
// controller. The definition of the message is the FROM_COMM_SERVER_TOP_PACKET_HEADER plus
// the 6 byte DATE_TIME structure. Of course it has the preamble, CRC, and post amble too.
// This message is sent at 4PM to all controllers in the tracking table at that time.
#define		MID_FROM_COMMSERVER_set_your_time_and_date								( 91)

// ----------

// 5/14/2015 rmd : New MOBILE commands. These are single packet blast messages from the
// commserver to the controller. They do hold data and are ACK'd by the controller. The
// message to the controller and the ACK from the controller hold the 'job_number' field (as
// the first piece of data after the header).
#define		MID_FROM_COMMSERVER_mobile_station_ON									(100)
#define		MID_TO_COMMSERVER_mobile_station_ON_ack									(101)

#define		MID_FROM_COMMSERVER_mobile_station_OFF									(102)
#define		MID_TO_COMMSERVER_mobile_station_OFF_ack								(103)

// 6/11/2015 rmd : A periodic message from the controller to the commserver. Holds dynamic
// 'state of' information such as flow rate, remaining time for those on, etc. Will be sent
// in an unsolicited fashion at various times and rates to the commserver. Upon receipt
// contents put into a holding table and flag set indicating fresh data arrival.
#define		MID_TO_COMMSERVER_status_screen_init_packet								(104)
#define		MID_TO_COMMSERVER_status_screen_data_packet								(105)
#define		MID_FROM_COMMSERVER_status_screen_ack_packet							(106)

// 6/12/2015 rmd : This is a command that will sort of kick off a mobile session. It ought
// to be the first command sent when a user with the mobile app chooses a controller. Upon
// receipt the controller recognizes that there is a user preparing to perhaps send some
// mobile commands. The controller will create a pseudo 'mobile session' where it will
// periodically send a status screen at moments and rates defined by the controller. The
// rate slows as 'mobile inactivity' is sensed by the controller. And eventually the session
// times out and the status screen updates stop all together. A user at the mobile app can
// ask on demand for a new status while on the 'mobile app status screen'.
#define		MID_FROM_COMMSERVER_send_a_status_screen								(107)
#define		MID_TO_COMMSERVER_send_a_status_screen_ack								(108)

// ----------

// 8/31/2015 rmd : The following commands support swapping controllers in the field. During
// say a repair operation.

#define 	MID_FROM_COMMSERVER_panel_swap_factory_reset_to_new_panel				(109)
#define 	MID_TO_COMMSERVER_panel_swap_factory_reset_to_new_panel_ack  			(110)

#define 	MID_FROM_COMMSERVER_panel_swap_factory_reset_to_old_panel				(111)
#define 	MID_TO_COMMSERVER_panel_swap_factory_reset_to_old_panel_ack  			(112)

#define 	MID_FROM_COMMSERVER_set_all_bits										(113)
#define 	MID_TO_COMMSERVER_set_all_bits_ack										(114)

// ----------

// 7/21/2015 mpd : Added for LIGHTS.
#define		MID_TO_COMMSERVER_LIGHTS_REPORT_DATA_init_packet						(115)
#define		MID_TO_COMMSERVER_LIGHTS_REPORT_DATA_data_packet						(116)
#define		MID_FROM_COMMSERVER_LIGHTS_REPORT_DATA_full_receipt_ack					(117)
#define		MID_FROM_COMMSERVER_LIGHTS_REPORT_DATA_NAK								( 115 + 1000 )

// ----------

#define		MID_TO_COMMSERVER_ET_AND_RAIN_TABLE_init_packet							(118)
#define		MID_TO_COMMSERVER_ET_AND_RAIN_TABLE_data_packet							(119)
#define		MID_FROM_COMMSERVER_ET_AND_RAIN_TABLE_full_receipt_ack					(120)
#define		MID_FROM_COMMSERVER_ET_AND_RAIN_TABLE_NAK								( 118 + 1000 )

// ----------

// 9/30/2015 ajv : We using a single command to determine whether to initiate an MVOR (open
// or close) or cancel one that's in progress. Once we receive the command, the
// FOAL_IRRI_initiate_or_cancel_a_master_valve_override function is called with the
// appropriate parameters to perform the required action.
#define		MID_FROM_COMMSERVER_mobile_MVOR											(121)
#define		MID_TO_COMMSERVER_mobile_MVOR_ack										(122)

// ----------

// 9/30/2015 ajv : We're using a single command to handle lights activities. This command
// sends what's built up into a LIGHTS_ON_XFER_RECORD and either
// FOAL_LIGHTS_initiate_a_mobile_light_ON or FOAL_LIGHTS_initiate_a_mobile_light_OFF is
// called with that record as a parameter to perform the action.
#define		MID_FROM_COMMSERVER_mobile_light_cmd									(123)
#define		MID_TO_COMMSERVER_mobile_light_cmd_ack									(124)

// ----------

// 10/5/2015 ajv : Command to turn Scheduled Irrigation On or Off.
#define		MID_FROM_COMMSERVER_mobile_turn_controller_on_off						(125)
#define		MID_TO_COMMSERVER_mobile_turn_controller_on_off_ack						(126)

// ----------

#define		MID_FROM_COMMSERVER_mobile_now_days_all_stations						(127)
#define		MID_TO_COMMSERVER_mobile_now_days_all_stations_ack						(128)

#define		MID_FROM_COMMSERVER_mobile_now_days_by_group							(129)
#define		MID_TO_COMMSERVER_mobile_now_days_by_group_ack							(130)

#define		MID_FROM_COMMSERVER_mobile_now_days_by_station							(131)
#define		MID_TO_COMMSERVER_mobile_now_days_by_station_ack						(132)

// ----------

#define		MID_FROM_COMMSERVER_force_registration									(133)
#define		MID_TO_COMMSERVER_force_registration_ack								(134)

// ----------

// 12/14/2015 ajv : The enable or disable FLOWSENSE option command takes a TRUE or a FALSE
// as a parameter to indicate whether to enable or disable the option, respectively.
#define		MID_FROM_COMMSERVER_enable_or_disable_FL_option							(135)
#define		MID_TO_COMMSERVER_enable_or_disable_FL_option_ack						(136)

// ----------

// 12/14/2015 ajv : The clear mainline break command takes the mainline (system) GID as a
// parameter to clear the appropriate break if more than one exists.
#define		MID_FROM_COMMSERVER_clear_MLB											(137)
#define		MID_TO_COMMSERVER_clear_MLB_ack											(138)

// ----------

// 12/14/2015 ajv : The stop irrigation command emulates the user pressing the STOP key.
// That is, it stops the highest form of irrigation across all mainlines.
#define		MID_FROM_COMMSERVER_stop_all_irrigation									(139)
#define		MID_TO_COMMSERVER_stop_all_irrigation_ack								(140)

// 12/14/2015 ajv : This stop irrigation command emulates the user pressing the STOP key.
// That is, it stops the highest form of irrigation across all mainlines. Except, in the
// future, we may opt to enhance this so we'll accept the mainline (system) GID as well.
// However, we're not going to use it for now.
#define		MID_FROM_COMMSERVER_stop_irrigation										(141)
#define		MID_TO_COMMSERVER_stop_irrigation_ack									(142)

// ----------

// 1/11/2016 ajv : Manually force a check for updates from Command Center Online
#define		MID_FROM_COMMSERVER_check_for_updates									(143)
#define		MID_TO_COMMSERVER_check_for_updates_ack									(144)

// ----------

#define		MID_FROM_COMMSERVER_mobile_now_days_by_box								(145)
#define		MID_TO_COMMSERVER_mobile_now_days_by_box_ack							(146)

// ----------

// 02/22/2016 skc : Standard message ids for budget reports
#define		MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet						(147)
#define		MID_TO_COMMSERVER_BUDGET_REPORT_DATA_data_packet						(148)
#define		MID_FROM_COMMSERVER_BUDGET_REPORT_DATA_full_receipt_ack					(149)
#define		MID_FROM_COMMSERVER_BUDGET_REPORT_DATA_NAK								(147 + 1000)

// ----------

#define		MID_TO_COMMSERVER_MOISTURE_SENSOR_RECORDER_init_packet					(150)
#define		MID_TO_COMMSERVER_MOISTURE_SENSOR_RECORDER_data_packet					(151)
#define		MID_FROM_COMMSERVER_MOISTURE_SENSOR_RECORDER_full_receipt_ack			(152)
#define		MID_FROM_COMMSERVER_MOISTURE_SENSOR_RECORDER_NAK						(150 + 1000)

// ----------

#define		MID_FROM_COMMSERVER_perform_two_wire_discovery							(153)
#define		MID_TO_COMMSERVER_perform_two_wire_discovery_ack						(154)

// ----------

#define		MID_FROM_COMMSERVER_mobile_acquire_expected_flow_by_station				(155)
#define		MID_TO_COMMSERVER_mobile_acquire_expected_flow_by_station_ack			(156)

// ----------

// 4/25/2017 ajv : The enable or disable CS3-HUB-OPT command takes a TRUE or a FALSE as a
// parameter to indicate whether to enable or disable the option, respectively.
#define		MID_FROM_COMMSERVER_enable_or_disable_HUB_option						(157)
#define		MID_TO_COMMSERVER_enable_or_disable_HUB_option_ack						(158)

// ----------

#define		MID_TO_COMMSERVER_keep_alive											(160)
#define		MID_FROM_COMMSERVER_keep_alive_ack										(161)

// ----------

// 2/24/2017 rmd : This is the routine POLL message to the hub or any controller on the hub.
// There is no ACK response to the commserver for this single packet message. What does
// hapen though is the commserver is waiting for messages from the controller. And will
// continue to receive messages from this controller until it sees either the 'no more
// messages' message. Or the 'hub is busy' message.
//
// 2/24/2017 rmd : Note - this packet may be for the HUB itself or one of the controllers
// hanging on the HUB. If the packet is not for the HUB, the HUB forwards this single packet
// message out Port B, the HUB radio device port. If the HUB is busy with a code
// distribution the HUB does not forward the packet and responds to the commserver with the
// 'hub is busy' message FROM THE CONTROLLER THE POLL WAS ADDRESSED TO.
#define		MID_FROM_COMMSERVER_go_ahead_send_your_messages							(162)

#define		MID_TO_COMMSERVER_no_more_messages_init_packet							(163)
#define		MID_TO_COMMSERVER_no_more_messages_data_packet							(164)
#define		MID_FROM_COMMSERVER_no_more_messages_ack								(165)

#define		MID_TO_COMMSERVER_hub_is_busy_init_packet								(166)
#define		MID_TO_COMMSERVER_hub_is_busy_data_packet								(167)
#define		MID_FROM_COMMSERVER_hub_is_busy_ack										(168)

// ----------

// 3/30/2017 rmd : These two mids are a way for the hub to ask the commserver to send the
// hub list. The ack (170) is NOT the hub list. The commserver should queue the hub list as
// a high priority job, and instead of the next poll, the hub list would be sent. Presently
// this command is not used.
#define		MID_TO_COMMSERVER_please_send_me_the_hub_list							(169)
#define		MID_FROM_COMMSERVER_please_send_me_the_hub_list_ack						(170)

// ----------

// 12/10/2018 rmd : This command is used to STOP and CANCEL the HUB code distribution
// effort.
#define		MID_FROM_COMMSERVER_stop_and_cancel_hub_code_distribution				(171)
#define		MID_TO_COMMSERVER_stop_and_cancel_hub_code_distribution_ack				(172)

// ----------

// 3/30/2017 rmd : Following the intial connection Jay is going to send the hub a list of
// the controllers, both 2000 and 3000, that are on the hub. The list will be sent without
// the hub asking for it. The list is delivered in a single packet and therefore can hold
// 512 total 2000/3000 controllers.
#define		MID_FROM_COMMSERVER_here_is_the_hub_list								(174)
#define		MID_TO_COMMSERVER_here_is_the_hub_list_ack								(175)

// ----------

// 12/10/2018 rmd : This command is the ONLY way to start the HUB code distribution. It
// includes a duration so that the distribution will automatically terminate after that
// amount of time.
#define		MID_FROM_COMMSERVER_start_the_hub_code_distribution						(176)
#define		MID_TO_COMMSERVER_start_the_hub_code_distribution_ack					(177)

// ----------

// 2/10/2017 rmd : These are ADDITIONAL result mids from the commserver in response to the
// MID 40/41 pair to the commserver. The 40/41 pair is a check for updates query from the
// controller. These particular responses should only be sent to a hub.
#define		MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_one_update_but_distribute_both		(180)
#define		MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_tpmicro		(181)
#define		MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_main		(182)
#define		MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_both		(183)

// ----------
 
// 2/24/2017 skc : Only send POCs that are in use, superceded the older budget report
// message. We keep both because there will be two version in the field. The values
// here need to jibe with /ServerApp/Constants.cs
#define		MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet2						(200)
#define		MID_TO_COMMSERVER_BUDGET_REPORT_DATA_data_packet2						(201)
#define		MID_FROM_COMMSERVER_BUDGET_REPORT_DATA_full_receipt_ack2				(202)
#define		MID_FROM_COMMSERVER_BUDGET_REPORT_DATA_NAK2								(MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet2 + 1000)

// ----------

// 4/12/2017 wjb : This command used only by the CommServer and Command Center Online 
// to ask the CommServer to update his hub list and send the new list to the
// hub contoller.
#define		MID_TO_COMMSERVER_update_and_send_hub_list								(203)

// ---------- 

#define		MID_FROM_COMMSERVER_clear_rain_all_stations								(210)
#define		MID_TO_COMMSERVER_clear_rain_all_stations_ack							(211)

#define		MID_FROM_COMMSERVER_clear_rain_by_group									(212)
#define		MID_TO_COMMSERVER_clear_rain_by_group_ack								(213)

#define		MID_FROM_COMMSERVER_clear_rain_by_station								(214)
#define		MID_TO_COMMSERVER_clear_rain_by_station_ack								(215)

// ---------- 

// 7/17/2017 ajv : Commands to request data from the controller rather than relying on the 
// controller to send it automatically. These may be used in the future to provide the user 
// control over when the data is retrieved. 
#define		MID_FROM_COMMSERVER_send_engineering_alerts								(216)
#define		MID_TO_COMMSERVER_send_engineering_alerts_ack							(217)

#define		MID_FROM_COMMSERVER_send_flow_recording									(217)
#define		MID_TO_COMMSERVER_send_flow_recording_ack								(218)

#define		MID_FROM_COMMSERVER_send_station_history								(219)
#define		MID_TO_COMMSERVER_send_station_history_ack								(220)

#define		MID_FROM_COMMSERVER_send_station_report_data							(221)
#define		MID_TO_COMMSERVER_send_station_report_data_ack							(222)

#define		MID_FROM_COMMSERVER_send_poc_report_data								(223)
#define		MID_TO_COMMSERVER_send_poc_report_data_ack								(224)

#define		MID_FROM_COMMSERVER_send_system_report_data								(225)
#define		MID_TO_COMMSERVER_send_system_report_data_ack							(226)

#define		MID_FROM_COMMSERVER_send_lights_report_data								(227)
#define		MID_TO_COMMSERVER_send_lights_report_data_ack							(228)

#define		MID_FROM_COMMSERVER_send_budget_report_data								(229)
#define		MID_TO_COMMSERVER_send_budget_report_data_ack							(230)

#define		MID_FROM_COMMSERVER_send_moisture_sensor_recorder_data					(231)
#define		MID_TO_COMMSERVER_send_moisture_sensor_recorder_ack						(232)

// ----------

// 7/20/2017 ajv : Occassionally, engineering has had the desire to reboot a controller 
// remotely, whether to force a FLOWSENSE scan, rebuild a socket, or something else. This 
// command helps acheive that. 
#define		MID_FROM_COMMSERVER_reboot_controller									(233)
#define		MID_TO_COMMSERVER_reboot_controller_ack									(234)

// ----------

// 7/20/2017 ajv : When troubleshooting communication issues remotely, it can be useful to 
// have the ability to trigger a new FLOWSENSE scan. For example, if there is a large SR 
// chain and Bill is at a controller some distance from the master, this allows us to force 
// a rescan after he's made adjustments to the antenna. 
#define		MID_FROM_COMMSERVER_perform_FLOWSENSE_scan								(235)
#define		MID_TO_COMMSERVER_perform_FLOWSENSE_scan_ack							(236)

// 7/20/2017 ajv : Similar to the perform FLOWSENSE scan functionality, we've had cases 
// where engineering was not confident the program data had been synced properly down the 
// chain. To overcome this, we've had the users turn off FLOWSENSE, leave the screen, wait a 
// minute, and turn FLOWSENSE back on. This gives us direct control to trigger this behavior 
// remotely. 
#define		MID_FROM_COMMSERVER_perform_FLOWSENSE_data_sync							(237)
#define		MID_TO_COMMSERVER_perform_FLOWSENSE_data_sync_ack						(238)

// ----------

// 7/20/2017 ajv : In the ET2000 and ET2000e, Field Service occassionally ran into issues 
// where the flow table had invalid data in it. This resulted in user-level high and low 
// flow alerts that made little sense. To overcome this, they cleared the flow table using 
// CC4. City of Davis may have run into a similar issue with the CS3000 and we're therefore 
// adding this command to provide the same functionality. It's unclear whether Field Service 
// will be given access to this, though. 
#define		MID_FROM_COMMSERVER_reset_flow_table									(239)
#define		MID_TO_COMMSERVER_reset_flow_table_ack									(240)

// ----------

// 7/20/2017 ajv : In the HUB world, the CommServer doesn't send a new HUB list unless it 
// either has a new controller added or the HUB establishes a new socket connection. 
// Although a "send hub list" command exists, Bob has also requested a command to forcibly 
// disconnect from the CommServer and create a new socket connection. 
#define		MID_FROM_COMMSERVER_disconnect_from_cloud								(241)
#define		MID_TO_COMMSERVER_disconnect_from_cloud_ack								(242)

// ----------

// 7/20/2017 ajv : This command provides a method for a user to activate a software option, 
// such as FLOWSENSE or HUB, on a controller with a central option attached without having 
// to go out to the field. 
#define		MID_FROM_COMMSERVER_activate_option										(243)
#define		MID_TO_COMMSERVER_activate_option_ack									(244)

// ----------

// 10/8/2018 ajv : This command is used to enable and disable the new AQUAPONICS option that 
// tailors the controller to be used with an aquaponics system. 
#define		MID_FROM_COMMSERVER_enable_or_disable_AQUAPONICS_option					(245)
#define		MID_TO_COMMSERVER_enable_or_disable_AQUAPONICS_option_ack				(246)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
//
// MESSAGES FROM THE CONTROLLER (to the commserver)
//
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/14/2015 rmd : To commserver message make up is as follows -
//
//  init packet:		AMBLE_TYPE + TO_COMMSERVER_MESSAGE_HEADER +
//  					MSG_INITIALIZATION_STRUCT + CRC_BASE_TYPE + AMBLE_TYPE
//
//  2K data packet(s):	AMBLE_TYPE + TO_COMMSERVER_MESSAGE_HEADER +
//  					MSG_DATA_PACKET_STRUCT + (up to 2K of data) + CRC_BASE_TYPE +
//  					AMBLE_TYPE
//
// Minimum message size is one init packet and one data packet. Message is ACK'd once upon
// sucessful receipt (as defined by the init packet).

// 6/11/2015 rmd : ACK packets from controller to commserver. Two kinds. When needed holds
// the job number. For example job number included within ACK for the mobile ON and OFF
// commands when received by the controller. The returned job_number is picked out of the
// incoming ON/OFF message.
//
//  ACK packet:			AMBLE_TYPE + TO_COMMSERVER_MESSAGE_HEADER + CRC_BASE_TYPE +
//                      AMBLE_TYPE
//
//  ACK packet:			AMBLE_TYPE + TO_COMMSERVER_MESSAGE_HEADER + JOB_NUMBER
//  					+ CRC_BASE_TYPE + AMBLE_TYPE
//

// 3/16/2017 rmd : NOTE - with the introduction of the LR HUB we needed to add the
// traditional packet routing bytes to the front of the packets being built that go from a
// controller to the commserver via a hub. The messages to the commserver DO NOT have
// routing CLASS bytes up front, and we need them. This is so the hub packet router knows
// what to do with the packets as some packets from controllers to the hub may be for the
// hub itself (think about the query made by the hub asking each controller if they have the
// latest code and if not what portion of it).

typedef struct
{
	// 3/16/2017 rmd : Two bytes that allow us to indicate a MESSAGE CLASS. Only used when
	// controller is on a HUB.
	TPL_PACKET_ID	PID;				// - 1 byte

	UNS_8			cs3000_msg_class;	// - 1 byte

} __attribute__((packed)) TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER;

// ----------

typedef struct
{
	// 8/22/2013 rmd : When the message is from the controller to the comm server this is the
	// FROM serial number. When the message is from the comm server to the controller this
	// is the TO serial number. The from or to comm_server serial number is implicitly assumed
	// based upon the fact the port is internet connected.
	UNS_32			from_serial_number;  // little endian format
	
	// 12/4/2013 rmd : This is the chain identification. It may be a single controller. Or an
	// actual chain of them. The default is 0, which the comm server picks up as a new network
	// registering with the database, assigns us a network_id, returns it to us. During
	// controller initiated attempts to send data (for all except the registration packet
	// itself), if we see our network ID is a 0, the send attempt with the exception of the
	// registration packet will be aborted (no connection attempt is made).
	UNS_32			from_network_id;

	// ----------

	// 11/10/2016 rmd : This is the total size of the packet minus 12. This length does not
	// include the 2 ambles and the CRC. NOTE - this is the length the commserver uses to figure
	// out the when the packet has arrived. Apparently Jay did not implement the packet hunt
	// using the pre and post ambles. He sort of acts like he ignores the post amble - but I
	// think he does test for it. I think he relies more upon the expected length to conclude he
	// has received the whole packet. And then tests the CRC. In any case what he's done does
	// seem to work well.
	UNS_32			length;
	
	// ----------

	UNS_16			mid;
	
	// ----------
	
	// 11/10/2016 rmd : 14 bytes

} __attribute__((packed)) TO_COMMSERVER_MESSAGE_HEADER;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
//
// MESSAGES FROM THE COMMSERVER (to the controller)
//
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/19/2015 rmd : There are TWO message/packet conventions used between the commserver and
// the controller.

// TYPE 1: This the most elaborate message type. And is only used to send the large binary
// code update files and the somewhat large program data messages. This message consists of
// an init packet ACKNOWLEDGED by the controller. It is shown below and is followed by at
// least one data packet also as shown below. When the complete message is received and
// checksummed it is ACKNOWLEDGED as a final indication of receipt.

// TYPE 1:
//
//  	init packet:		AMBLE_TYPE + FROM_COMMSERVER_PACKET_TOP_HEADER +
//  						MSG_INITIALIZATION_STRUCT + CRC_BASE_TYPE + AMBLE_TYPE
//
//  	2K data packet(s):	AMBLE_TYPE + FROM_COMMSERVER_PACKET_TOP_HEADER +
//  	 		   			MSG_DATA_PACKET_STRUCT + (up to 2K of data) + CRC_BASE_TYPE +
//  						AMBLE_TYPE

// TYPE 2: These are single packet messages. That optionally hold data. Referred to as the
// BLAST messages by the commserver. That MAY or MAY NOT be ACKNOWLEDGED by the controller
// as indication to the commserver of sucessful receipt. Whether they are acknowledged or
// not is a message-by-message decision made by the commserver and controller code
// designers. The message data content is defined as a structure and is message dependant.
// Data content is optional meaning some messages may not have any data at all. The message
// packet is defined below. And some of the structures can be found lower in this
// file.
//
// Examples of TYPE 2 messages include time_and_date_sync. This message packet holds data
// but is not ACK'd. Another example is the rain_shutoff message. This message holds NO data
// and is ACK'd. And yet a third example is the IHSFY message packet that holds NO data and
// is not ACK'd.

// TYPE 2:
//
//  	single packet msg:	AMBLE_TYPE + FROM_COMMSERVER_PACKET_TOP_HEADER +
//  						(optional mid dependant data structure) + CRC_BASE_TYPE +
//  						AMBLE_TYPE
//
// 1/26/2017 rmd : And in this single packet msg case, the largest the mid dependant data
// structure can be is 2048 bytes (actually + 4 more bytes). Keep that in mind. If you
// exceed that you will go beyond the largest acceptable packet size and the packet hunt
// will not find the packet.

// ----------

// 5/19/2015 rmd : IMPORTANT NOTE REGARDING MESSAGE CLASS: In the FROM COMMSERVER PACKET TOP
// HEADER is a structure called TPL_PACKET_ID. It contains a field called __routing_class.
// The commserver must always ensure that field is set to 0xF. In addition the commserver
// always sets the cs3000_msg_class filed in the header to MSG_CLASS_CS3000_FROM_COMMSERVER.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 2/3/2014 rmd : The first two bytes are used for message class. And this header definition
	// MUST BE compatible with traditional transport packets up to the first two bytes. Enough
	// for initial packet handling to perform routing based upon the message class. The FIRST
	// byte is ONLY present to support a front-end router for a group of mixed 2000e and 3000
	// controllers. So that the router could handle both 2000e and 3000 packets! The second byte
	// would also be used by such a router to handle CS3000 type packets.
	//
	// NOTE that in the KICKSTART, COMM_SERVER, and CS3000 applications we use the second byte
	// as our primary packet identification field.
	TPL_PACKET_ID	PID;				// - 1 byte

	UNS_8			cs3000_msg_class;	// - 1 byte

	// ----------

	UNS_32			to_serial_number;	// little endian format
	
	UNS_32			to_network_id;

	// ----------

	// 3/31/2014 rmd : The message ID. We use the message CLASS to manage routing. The MID to
	// handle actual message intrepretation.
	UNS_16			mid;
	
	// 10/8/2013 rmd : Note packed to force to a known size.
} __attribute__((packed)) FROM_COMMSERVER_PACKET_TOP_HEADER;

// ----------

// 12/13/2017 rmd : A new packet definition used between 3000 controllers, so far only for
// the radio test functionality, but may have more use later.
typedef struct
{
	// 2/3/2014 rmd : The first two bytes are used for message class. And this header definition
	// MUST BE compatible with traditional transport packets up to the first two bytes. Enough
	// for initial packet handling to perform routing based upon the message class.
	TPL_PACKET_ID	PID;				// - 1 byte

	UNS_8			cs3000_msg_class;	// - 1 byte

	// ----------

	UNS_32			to_serial_number;	// little endian format
	
	UNS_32			from_serial_number;	// little endian format
	
	// ----------

	// 12/13/2017 rmd : The message ID. We use the message CLASS to manage routing. The MID to
	// handle actual message intrepretation. For the case of RADIO_TEST we don't use the mid,
	// only the CLASS.
	UNS_16			mid;
	
	// 10/8/2013 rmd : Note packed to force to a known size.
} __attribute__((packed)) FROM_CONTROLLER_PACKET_TOP_HEADER;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
//
// STRUCTURES USED IN MESSAGES GOING BOTH DIRECTIONS
//
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 2/4/2014 rmd : One of these is the start of the multiple packet outbound messages from
	// the COMMSERVER or CODEDOWNLOADER to the CS3000. If the INIT packet is sent there is
	// always DATA packet that follow. So far there are only two messages that use this format.
	// When sending a code update message to the controller. And when sending a program data
	// message to the controller.

	// ----------

	UNS_32	expected_size;

	// 2/4/2014 rmd : The full crc of the extracted payload data. Think of this as the crc of
	// the msg itself. If the msg is comprised of multiple packets note that each packet has a
	// crc too.
	UNS_32	expected_crc;
	
	UNS_16	expected_packets;

	// ----------

	// 3/31/2014 rmd : Another copy of the mid because when we get to parsing the actual message
	// content we no longer have access to the TOP_HEADER.
	UNS_16	mid;

	// ----------
	
	// 2/4/2014 rmd : Any needed message specific information can follow this STRUCT in the
	// intialization message content as an additional secondary structure. It's presence or not,
	// and content, if any, are by definition as per the mid.
	
	// ----------
	
	// 11/10/2016 rmd : 12 bytes

} __attribute__((packed)) MSG_INITIALIZATION_STRUCT;

// ----------

typedef struct
{
	// 2/4/2014 rmd : This is the header used for the multiple packet message 'data' packets.
	
	// ----------

	// 2/4/2014 rmd : Should start at 1. When matches expected packets is the last of the
	// message.
	UNS_16	packet_number;
	
	// ----------

	// 3/31/2014 rmd : Another copy of the mid because when we get to parsing the actual message
	// content we no longer have access to the TOP_HEADER. Also a sanity check as packets arrive
	// compare this mid to the initialization struct mid. Should match!
	UNS_16	mid;

	// ----------
	
	// 11/10/2016 rmd : 4 bytes

} __attribute__((packed)) MSG_DATA_PACKET_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/28/2015 rmd : *********** BLAST MESSAGES PACKET FORMAT ***********

// ----------

// 5/28/2015 rmd : For the TIME & DATE blast message to the controller a 30 byte packet.
// This packet is not ACK'd by the controller.

//  PRE_AMBLE (4)
//	FROM_COMMSERVER_PACKET_TOP_HEADER (12)
//	DATE_TIME (6)
//	CRC (4)
//	POST-AMBLE (4)

// ----------

// 5/28/2015 rmd : For the WEATHER DATA blast message to the controller a 33 byte packet.
// This packet is ACK'd by the controller.

//  PRE_AMBLE (4)
//	FROM_COMMSERVER_PACKET_TOP_HEADER (12)
//  UNS_8	whats_included (1)
//  ET_TABLE_ENTRY (4)
//	RAIN_TABLE_ENTRY (4)
//	CRC (4)
//	POST-AMBLE (4)

// ----------

// 5/28/2015 rmd : For the RAIN SHUTDOWN blast message to the controller a 24 byte packet.
// Notice NO data. The MID says it all. This packet is ACK'd by the controller.

//  PRE_AMBLE (4)
//	FROM_COMMSERVER_PACKET_TOP_HEADER (12)
//	CRC (4)
//	POST-AMBLE (4)

// ----------

// 5/28/2015 rmd : For the STATION TURN ON blast message to the controller a 36 byte packet.
// This packet is ACK'd by the controller.

//  PRE_AMBLE (4)
//	FROM_COMMSERVER_PACKET_TOP_HEADER (12)
//	UNS_32	job_number (4)
//	UNS_32	box_index_0 (4)
//	UNS_32	station_number_0 (4)
//  UNS_32	for_how_long_seconds (4)
//	CRC (4)
//	POST-AMBLE (4)

// ----------

// 5/28/2015 rmd : For the STATION TURN OFF blast message to the controller a 32 byte
// packet. This packet is ACK'd by the controller.

//  PRE_AMBLE (4)
//	FROM_COMMSERVER_PACKET_TOP_HEADER (12)
//	UNS_32	job_number (4)
//	UNS_32	box_index_0 (4)
//	UNS_32	station_number_0 (4)
//	CRC (4)
//	POST-AMBLE (4)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/28/2015 rmd : For the STATUS message to the controller. A normal init followed by data
// packet message. The message is ACK'd by the commserver. The blob contents is as
// follows:

// revision (1)


// mainline break (1)

// mainline break record (8?)


// et-gage (1)

// gage pulses (2)


// rain bucket (1)

// rain pulses (2)

// number of systems (1)

// flow rate gpm (2)
// flow rate gpm (2)
// flow rate gpm (2)
// flow rate gpm (2)

// number of stations on (2)

// reason on (2)

// box index (2)
// station number (2)
// remaining seconds (2)

// box index (2)
// station number (2)
// remaining seconds (2)

// box index (2)
// station number (2)
// remaining seconds (2)

// box index (2)
// station number (2)
// remaining seconds (2)

// box index (2)
// station number (2)
// remaining seconds (2)

// box index (2)
// station number (2)
// remaining seconds (2)

// 6/22/2015 rmd : To guide the mobile app in how to parse the status structure. Revision
// value must not exceed 255.
#define		MOBILE_STATUS_REVISION_CODE		(10)


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/4/2014 rmd : A decision was made to allow our packet size to grow considerably from the
// 512 bytes it had been at for many years. To reduce the overhead associated with the many
// packet transfer of the main app code image we grew our actual payload to 2048. This does
// not include the packet definition structures.
#define DESIRED_PACKET_PAYLOAD	(2048)


// 4/3/2012 rmd : Messages to the TP Micro are a single packet. And they do not pass through
// the transport. There are no status or retries. This size directly sets the largest TP
// MICRO memory pool block size. And directly sets the size of the TP MICRO uart receive
// ring buffers. This size includes the ambles, crc, and header. So the actual payload is 20
// bytes less than this. This affects the maximum packet size for any packet coming into or
// being shipped out of any port on the TP MICRO. The CS3000 main board also uses this value
// when building messages for the TP Micro.
//
// 1/31/2014 rmd : Note the 'amble_to_amble' description to emphasize includes pre and post
// ambles, header, and crc, in addition to the payload.

/* sizeof(TPL_DATA_HEADER_TYPE) + 				\ */

#define	TPMICRO_MAX_AMBLE_TO_AMBLE_PACKET_SIZE		( sizeof(AMBLE_TYPE) +							\
													  sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) +	\
													  sizeof(MSG_DATA_PACKET_STRUCT) +				\
													  DESIRED_PACKET_PAYLOAD +						\
													  sizeof(CRC_TYPE) +							\
													  sizeof(AMBLE_TYPE) )
													  
// 3/16/2017 rmd : The biggest packet one can see in the 3000 environment is when a
// hub-based controller sends a full data packet to the commserver via the hub. It passes
// through the hub packet router so the ring buffer must allow this size to pass.
#define	CS3000_MAX_AMBLE_TO_AMBLE_PACKET_SIZE		( sizeof(AMBLE_TYPE) +									\
													  sizeof(TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER) +	\
													  sizeof(TO_COMMSERVER_MESSAGE_HEADER) +				\
													  sizeof(MSG_DATA_PACKET_STRUCT) +						\
													  DESIRED_PACKET_PAYLOAD +								\
													  sizeof(CRC_TYPE) +									\
													  sizeof(AMBLE_TYPE) )
												  

#define	KICKSTART_MAX_AMBLE_TO_AMBLE_PACKET_SIZE			(CS3000_MAX_AMBLE_TO_AMBLE_PACKET_SIZE)


// 2/5/2014 rmd : This is a manual decision to assign the largest packect size to this
// define. I couldn't figure out how to make the pre-compiler do the math for us.
#define SYSTEM_WIDE_LARGEST_AMBLE_TO_AMBLE_PACKET_SIZE		(CS3000_MAX_AMBLE_TO_AMBLE_PACKET_SIZE)
	
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void get_this_packets_message_class( TPL_DATA_HEADER_TYPE const *const pheader, ROUTING_CLASS_DETAILS_STRUCT *routing );

extern void set_this_packets_CS3000_message_class( TPL_DATA_HEADER_TYPE *const pheader, UNS_32 const class_to_set );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

