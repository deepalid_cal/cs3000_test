/*  file = transport_layer_common.c           2012.08.16  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"packet_definitions.h"

#if ( !defined(TPMICRO_CODE_GENERATION) && !defined(KICKSTART_CODE_GENERATION) )

	#include	"alerts.h"

#endif



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/16/2012 rmd : The bytes in this definition are REVERSED. This is intentional. Don't
// flip them around. The reason is because we are using our packet definitions when sending
// ourselves our own code during initial production loading and field upgrades. If the
// preamble and postamble are embedded within in the code (which they are) and they are in
// the correct order for a packet definition they then trip up the communications. Causing
// false packets and bad CRC's.  Reversing the bytes IN THE DEFINITION solves that problem.
// They are not reversed when they are tacked onto the packets however.

const AMBLE_TYPE preamble = { 0x4C, 0x3D, 0x2E, 0x1F };

const AMBLE_TYPE postamble = { 0x88, 0x79, 0x6A, 0x5B };

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void get_this_packets_message_class( TPL_DATA_HEADER_TYPE const *const pheader, ROUTING_CLASS_DETAILS_STRUCT *routing )
{
	if( pheader->H.PID.__routing_class == MSG_CLASS_2000e_CS3000_FLAG )
	{
		routing->routing_class_base = ROUTING_CLASS_IS_3000_BASED;
		
		routing->rclass = pheader->H.cs3000_msg_class;
	}
	else
	{
		routing->routing_class_base = ROUTING_CLASS_IS_2000_BASED;
		
		routing->rclass = pheader->H.PID.__routing_class;
	}
}

/* ---------------------------------------------------------- */
extern void set_this_packets_CS3000_message_class( TPL_DATA_HEADER_TYPE *const pheader, UNS_32 const class_to_set )
{
	// 7/18/2013 rmd : ONLY for use setting the message class in a packet being built at the
	// CS3000 or TP_MICRO.

	// ----------
	
	pheader->H.PID.__routing_class = MSG_CLASS_2000e_CS3000_FLAG;

	pheader->H.cs3000_msg_class = class_to_set;
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

