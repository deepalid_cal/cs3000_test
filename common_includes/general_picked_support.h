//	
//  Description: All Around General Commonly Used Stuff
//
///////////////////////////////////////////////////////////////////////////////
#ifndef _general_picked_support_H_
#define _general_picked_support_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// a general purpose way of dealing with a data block
typedef struct
{ 
	// ptr to the data - used unsigned char* cause that's how we usually use it
	UNS_8		*dptr;

	// length of the data
	UNS_32		dlen;

} DATA_HANDLE;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/*! \brief Toggles the endianism of \a u16 (by swapping its bytes).
 *
 * \param u16 U16 of which to toggle the endianism.
 *
 * \return Value resulting from \a u16 with toggled endianism.
 *
 * \note More optimized if only used with values known at compile time.
 */
#define __swap_16(x) ((UNS_16)(((UNS_16)(x) >> 8) | ((UNS_16)(x) << 8)))

#define __swap_32(x) ((UNS_32)(((UNS_32)__swap_16((UNS_32)(x) >> 16)) | ((UNS_32)__swap_16((UNS_32)(x)) << 16)))

#define __swap_64(x) ((UNS_64)(((UNS_64)__swap_32((UNS_64)(x) >> 32)) | ((UNS_64)__swap_32((UNS_64)(x)) << 32)))


#define	swap_16( x ) (x = __swap_16( x ))

#define	swap_32( x ) (x = __swap_32( x ))

#define	swap_64( x ) (x = __swap_64( x ))

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//_____ M A C R O S ________________________________________________________



#ifndef _MSC_VER

#define DISABLE   0
#define ENABLE    1
#define DISABLED  0
#define ENABLED   1
#define OFF       0
#define ON        1
#define KO        0
#define OK        1
#define PASS      0
#define FAIL      1

#define CLR       0
#define SET       1

#endif


#define LOW       0
#define HIGH      1


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

