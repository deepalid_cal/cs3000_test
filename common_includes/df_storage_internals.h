/* file = df_storage_internals.h             03.28.2011  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_DF_STORAGE_MNGR_INTERNALS_H
#define INC_DF_STORAGE_MNGR_INTERNALS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"crc.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// This file is customized for the SST25VF032B 32Mbit serial flash. It is organized as 1024 sectors of 4096 bytes each.
// Making a total of 4Mbyte.
// 
// The first 57344 bytes (MAXIMUM) of the spi flash are for the BOOTCODE. Its level of functionality is TBD. As we can add
// to it as long as the compiled bin size does not exceed the 57344 bytes. This is a limitation of the LPC3250 itself. It's
// own resident BOOTSTRAP code uses the region of IRAM from 56K up to 64K for it's own SRAM space.
// 
// The LPC3250 BOOTSTRAP code will examine the SPI flash for bootable code. If it finds valid code it will then copy the
// specified amount to the IRAM starting at location 0. Then jump to it and run it.
// 
// SO the bottom 14 pages of the flash are dedicated to the BOOTCODE program.
// 
// BECAUSE THE PAGE SIZE IS 4096 BYTES IN THIS DEVICE THAT IS ALSO THE CLUSTER SIZE. AND THEREFORE THERE IS NO
// DIFFERENCE BETWEEN A PAGE NUMBER AND CLUSTER NUMBER.

// THE FOLLOWING ARE THE SAME ACROSS BOTH CHIPS
#define DF_PAGE_SIZE_IN_BYTES					4096

// For the 32Mbit device we have 1024 pages of 4096 bytes each
#define DF_PAGES_ON_CHIP						1024

// Last page NUMBER (0 based) where we store actual file data
#define DF_LAST_DATA_PAGE						(DF_PAGES_ON_CHIP-1)

// This next constant defines the minimum allocation size. For the SST25VF032 this is one page.
#define DF_PAGES_PER_CLUSTER					1

#define	DF_LAST_DATA_CLUSTER					(DF_LAST_DATA_PAGE)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// For the boot chip or what we refer to as FLASH INDEX 0 we have the following defines.

// Pages 0 through 13 are used to store the BOOTCODE which CANNOT be part of the file system.
#define	DF_FIRST_AVAILABLE_PAGE_0				14

// This is the page NUMBER (0 based) where the Master DF Record is located. The master record contains the master CAT.
#define DF_MASTER_RECORD_PAGE_0					(DF_FIRST_AVAILABLE_PAGE_0)

// First page NUMBER (0 based) where we store actual file data
#define DF_FIRST_DIR_PAGE_0						(DF_MASTER_RECORD_PAGE_0 + 1)

#define DF_MAX_DIR_ENTRIES_0					15

#define DF_LAST_DIR_PAGE_0						(DF_FIRST_DIR_PAGE_0 + DF_MAX_DIR_ENTRIES_0 - 1)


// First page NUMBER (0 based) where we store actual file data
#define DF_FIRST_DATA_PAGE_0					(DF_LAST_DIR_PAGE_0 + 1)


#define	DF_FIRST_DATA_CLUSTER_0					(DF_FIRST_DATA_PAGE_0)


/* ---------------------------------------------------------- */
// For the secondary FLASH chip we do not have the restrictions in place from boot requirements.

// Page 0 (the first page) is available for the Master Record.
#define	DF_FIRST_AVAILABLE_PAGE_1				0

// This is the page NUMBER (0 based) where the Master DF Record is located. The master record contains the master CAT.
#define DF_MASTER_RECORD_PAGE_1					(DF_FIRST_AVAILABLE_PAGE_1)

// First page NUMBER (0 based) where we store actual file data
#define DF_FIRST_DIR_PAGE_1						(DF_MASTER_RECORD_PAGE_1 + 1)

#define DF_MAX_DIR_ENTRIES_1					60

#define DF_LAST_DIR_PAGE_1						(DF_FIRST_DIR_PAGE_1 + DF_MAX_DIR_ENTRIES_1 - 1)


// First page NUMBER (0 based) where we store actual file data
#define DF_FIRST_DATA_PAGE_1					(DF_LAST_DIR_PAGE_1 + 1)


#define	DF_FIRST_DATA_CLUSTER_1					(DF_FIRST_DATA_PAGE_1)




//////////////////////////////////////////////////////////////////////
// The next expression sizes all Cluster Allocation Tables based upon
// DF_DF_PAGES_ON_CHIP and DF_DF_PAGES_PER_CLUSTER values.
/////////////////////////////////////////////////////////////////////
//
// 2010_01_22 rmd
// This is a good expression though will actually oversize the CAT as it doesn't
// factor in the reductions from the Master Record, Master CAT, and from the directory
// entries themselves. But that's okay. (>>5 is of course divide by 32). And this size
// is the same for BOTH chips.
//
#define DF_CAT_32_BIT_WORDS	(DF_PAGES_ON_CHIP % DF_PAGES_PER_CLUSTER) ?  \
						 	((DF_PAGES_ON_CHIP/DF_PAGES_PER_CLUSTER)>>5) + 1 : \
						 	((DF_PAGES_ON_CHIP/DF_PAGES_PER_CLUSTER)>>5)

//////////////////////////////////////////////////////////////////////

// Same number of bytes per cluster for both chips.
#define DF_BYTES_PER_CLUSTER	(DF_PAGES_PER_CLUSTER * DF_PAGE_SIZE_IN_BYTES)


/////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Master DF Record Structure
//
//	This structure is always found in DF page 0 of the DF chip and provides the layout of the 
//  DF storage device. Here is the general layout of the DF device memory, viewed as blocks
//  of contiguous pages.
//
//				NO LONGER ACCURATE!!!!!!!!!!!!!!!
//				+------------------+
//              |                  |
//              |                  |
//              |   Allocatable    |
//              |    DF Pages      |  # DF pages here = DF_Capacity_Pgs - MCAT_DF_Pgs - Dir_DF_Pgs
//              |                  |
//              |                  |  Note: The exact # pages here may be slightly less when the cluster
//              |                  |        size > 1 DF page and the directory pages don't end on a nice
//              |                  |        cluster boundary.
//				+------------------+
//              |                  |
//              |   Dir DF Pages   |  # DF pages here = Dir_DF_Pgs
//              |                  |
//				+------------------+
//              |                  |
//              |   MCAT Page(s)   |  # DF pages here = MCAT_DF_Pgs (= 1 right now)
//              |                  |
//				+------------------+
//              | Master DF Record |  DF page 0
//				+------------------+
//
/////////////////////////////////////////////////////////////////////////////////////////////////

// In some FLASH chips the bytes per cluster is somewhat smallish. And to manage the size of the CAT we define
// several pages together as a cluster.
//
// The SST25VF032 however has a 4KByte page size which will be our cluster size as well. So the minimum storage element
// is 4096 bytes. Even if the file is only 100 bytes. We will use one directory page and one file data page.



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

////////////////////////////////////////////////////////////////////
//
//	Cluster Allocation Table (CAT)
//
//  A CAT appears on the Master CAT page, and on each directory
//  entry page at offset CatOffset_us from the start of the DF page. 
//
////////////////////////////////////////////////////////////////////
typedef struct
{

	unsigned long data32[ DF_CAT_32_BIT_WORDS ];
	
} DF_CAT_s;


#define		DF_MAGIC_STR_LENGTH		27

#define		DF_FLASH_MAGIC_STR_0	"CALSENSE BOOTCODE FLASHFS_0"
#define		DF_FLASH_MAGIC_STR_1	"CALSENSE ALLFILES FLASHFS_1"

typedef struct
{

	char			magic_str[ 32 ];	// Long enough to hold either string plus the NULL terminate.

	DF_CAT_s		cat;

	CRC_BASE_TYPE	CRC32;				// CRC32 of fields except this one. Code expectes this to be last in the structure.

} MASTER_DF_RECORD_s;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// The following functions are used internally by the DF storage manager
// and should not be called by application programs.

unsigned char DF_test_cluster_bit(void *pCAT, unsigned long cluster_ul);

void DF_set_cluster_bit( void *pCAT, unsigned long cluster_ul, unsigned char state_uc );

void DfOrCatBits(DF_CAT_s *pDestCAT, DF_CAT_s *pSrcCAT);

void DfClearCatBits(DF_CAT_s *pDestCAT, DF_CAT_s *pSrcCAT);



UNS_32 DF_read_page( UNS_32 flash_index, unsigned long ppage, void *pbuffer, UNS_32 pbytes_to_read );

UNS_32 DF_write_page( UNS_32 flash_index, unsigned long ppage, void *pbuffer, UNS_32 pbytes_to_write );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /*DFSTORAGEMGRINTERNALS_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

