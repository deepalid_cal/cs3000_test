/*  file = cal_td_utils.h    2009.05.27  rmd                  */
/* ---------------------------------------------------------- */

#ifndef _INC_CAL_TD_UTILS_H
#define _INC_CAL_TD_UTILS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	<stdio.h>

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2011.09.16 rmd : By packing this structure you reduce its size from 8 bytes to 6 bytes.
// This effects critical storage concerns such as alerts. Where this structure is included
// in each and every alert. The trade off of the packing is references to the UNS_32 could
// be misaligned ie not on a 4 byte boundary. Causing the compiler to generate code to read
// and write to it on a byte-by-byte basis. The nice thing is in the ARM environment this
// misalignment does not cause a data access abort. By ordering the UNS_32 first within the
// structure we can reduce the instances of the misalignment. Cause more often than not the
// structure itself begins on a 4 byte boundary. Though when you have multiple DATE_TIME
// structures sequentially within a larger STRUCT you will have every other one misaligned.
// Again not a problem and the GCC compiler will deal with it. Actually an amazing feat!
typedef struct
{ 
	UNS_32  T;

	UNS_16  D;
	
} __attribute__((packed)) DATE_TIME;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// The universal date and time struct used to convey the time & date to other
// tasks. To get the latest time and date one must use a function call which
// will load up their own structure of this type. Once a second a copy will also
// be queued for our periodic one second processing.
typedef struct {

	DATE_TIME		date_time;

	UNS_16			__day, __month, __year;  // __day range is 1..31, __month range is 1..12, year is 4 digits

    UNS_16			__hours, __minutes, __seconds;  // __hours range is 0..23, __minutes range is 0..59, __seconds range is 0..59

    UNS_8			__dayofweek;  // __dayofweek range is 0..6

	UNS_8			dls_after_fall_back_ignore_start_times;

} DATE_TIME_COMPLETE_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	SUNDAY		(0)
#define	MONDAY		(1)
#define	TUESDAY		(2)
#define	WEDNESDAY	(3)
#define	THURSDAY	(4)
#define	FRIDAY		(5)
#define	SATURDAY	(6)

#define	DAYS_IN_A_WEEK		(7)
#define	MONTHS_IN_A_YEAR	(12)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/26/2012 rmd : Setting the DEFAULT date and time to 1/1/2011 @ 12:00AM (midnight). Not
// sure why but this used to be 12PM (noon). Just seems like a confusing time to use. I have
// changed to midnight.
#define		DEFAULT_TD_SECONDS			(0)
#define		DEFAULT_TD_MINUTES			(0)
#define		DEFAULT_TD_HOURS			(0)

// 11/17/2014 rmd : This is a real date. You must use a real date not just made up.
#define		DATE_MINIMUM_WEEKDAY			(SATURDAY)
#define		DATE_MINIMUM_DAY_OF_MONTH		(1)
#define		DATE_MINIMUM_MONTH				(1)
#define		DATE_MINIMUM_YEAR_4DIGIT		(2011)

// 11/17/2014 rmd : This is a real date. You must use a real date not just made up.
#define		DATE_MAXIMUM_WEEKDAY			(WEDNESDAY)
#define		DATE_MAXIMUM_DAY_OF_MONTH		(31)
#define		DATE_MAXIMUM_MONTH				(12)
#define		DATE_MAXIMUM_YEAR_4DIGIT		(2042)

// 2012.06.12 ajv : Typecast MIN_DATE to UNS_16 since we only use this to
// intialize the 16-bit unsigned date portion of a DATE_TIME variable.
#define		DATE_MINIMUM	((UNS_16)DMYToDate( DATE_MINIMUM_DAY_OF_MONTH, DATE_MINIMUM_MONTH, DATE_MINIMUM_YEAR_4DIGIT ))
#define		DATE_MAXIMUM	((UNS_16)DMYToDate( DATE_MAXIMUM_DAY_OF_MONTH, DATE_MAXIMUM_MONTH, DATE_MAXIMUM_YEAR_4DIGIT ))

#define		TIME_DEFAULT	(HMSToTime( DEFAULT_TD_HOURS, DEFAULT_TD_MINUTES, DEFAULT_TD_SECONDS ))

// 7/26/2012 rmd : Some useful defines.
#define		TIME_MINIMUM	(0)
#define		TIME_MAXIMUM	(86399)
#define		TIME_MIDNIGHT	(TIME_MINIMUM)
#define		TIME_NOON		((12) * (3600))

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define TDUTILS_TREAT_AS_A_SX			(true)
#define TDUTILS_DONT_TREAT_AS_A_SX		(false)

#define TDUTILS_CONCAT_TO_STRING		(true)
#define TDUTILS_DONT_CONCAT_TO_STRING	(false)

#define TDUTILS_INCLUDE_SECONDS			(true)
#define TDUTILS_DONT_INCLUDE_SECONDS	(false)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define DATESTR_show_long_year		100
#define DATESTR_show_short_year		125
#define DATESTR_show_year_not		150

#define DATESTR_show_long_dow		200
#define DATESTR_show_short_dow		225
#define DATESTR_show_dow_not		250

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern UNS_32 DayOfWeek( const UNS_32 pdate );
 
// ----------

extern void DateToDMY( const UNS_32 pdate, UNS_32 *const pday_ptr, UNS_32 *const pmonth_ptr, UNS_32 *const pyear_4digit_ptr, UNS_32 *const pdow_ptr );

extern UNS_32 DMYToDate( const UNS_32 pday, UNS_32 pmonth, UNS_32 pyear_4digit );
 
// ----------

extern void DateAndTimeToDTCS( const UNS_32 pdate, const UNS_32 ptime, DATE_TIME_COMPLETE_STRUCT* pdtcs_ptr );

// ----------

extern BOOL_32 IsLeapYear( const UNS_32 pyear_4digit );
 
// ----------

extern UNS_32 NumberOfDaysInMonth( const UNS_32 pmonth_1, const UNS_32 pyear_4digit );

// ----------

extern UNS_32 HMSToTime( UNS_32 phour, const UNS_32 pmin, const UNS_32 psec );
 
// ----------

extern void TimeToHMS( UNS_32 ptime, UNS_32 *const phour_ptr, UNS_32 *const pmin_ptr, UNS_32 *const psec_ptr );

extern char *TDUTILS_time_to_time_string_with_ampm( char *const dest, const UNS_32 pdest_size, const UNS_32 pT, const BOOL_32 ptreat_as_a_start_time, const BOOL_32 pinclude_seconds, const BOOL_32 pconcat_to_pstr );

extern char *GetDateStr( char *const pdest, const UNS_32 pdest_size, const UNS_32 pdate, const UNS_32 pshow_year, const UNS_32 pshow_dow );


extern char *DATE_TIME_to_DateTimeStr_32( char *const pdest, const UNS_32 pdest_size, const DATE_TIME pdate_time );
 
// ----------

extern BOOL_32 DT1_IsBiggerThan_DT2( const DATE_TIME *const DT1, const DATE_TIME *const DT2 );

extern BOOL_32 DT1_IsBiggerThanOrEqualTo_DT2( const DATE_TIME *const DT1, const DATE_TIME *const DT2 );

extern BOOL_32 DT1_IsEqualTo_DT2( const DATE_TIME *const DT1, const DATE_TIME *const DT2 );

// ----------

extern void TDUTILS_add_seconds_to_passed_DT_ptr( DATE_TIME *const pdt, const UNS_32 pseconds );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

