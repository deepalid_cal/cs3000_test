/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_SQL_MERGE_H
#define _INC_SQL_MERGE_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

// 10/7/2015 ajv : Required for DATE_TIME and associated DateToDMY and TimeToHMS functions.
#include	"cal_td_utils.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/29/2015 ajv : The CS3000Networks and CS3000Weather MERGE statements are currently the
// largest, coming in at roughly 2.8k each when the 48-character strings are fully
// populated. However, Make sure we have enough buffer to support future enhancements. So,
// to be save, let's start with 8k blocks.
#define	SQL_MAX_LENGTH_FOR_MERGE_STATEMENT						(8*1024)

// 4/29/2015 ajv : Since we've said that a single MERGE statement can be 8K, it's probably
// safe to assume that a single clause within that statement will never exceed half of
// it. So let's start with that.
#define	SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE				(SQL_MAX_LENGTH_FOR_MERGE_STATEMENT / 2)

// 4/29/2015 ajv : On a two-controller chain with 48-stations each, we've seen the total
// length exceed 64K. Doing some rough math, we can assume about 1K per station, 1K per POC,
// and 2.5K per station group. So, 768 stations + 12 POCs + 24 groups amounts to roughly
// 840K. We'll therefore allow for up to 1 MB for now, but we should actively monitor this
// to ensure we don't exceed our limit in the future.

// 10/7/2015 wjb: I've Increased the buffer size from 1 MB to 10 MB for now to ensure we have 
// enough buffer to process reports messages.
#define	SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS					(10*1024*1024)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	SQL_MERGE_do_not_include_field_name_in_insert_clause	(false)
#define	SQL_MERGE_include_field_name_in_insert_clause			(true)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern char *SQL_MERGE_build_insert_merge_and_append_to_SQL_statements( const char *ptable_name, const char *psearch_condition, const BOOL_32 pinclude_field_names, const char *pinsert_fields_str, const char *pinsert_values_str, char *pSQL_statements, char *pSQL_single_merge_statement_buf );

extern char *SQL_INSERT_build_insert_and_append_to_SQL_statements(const char *ptable_name, const BOOL_32 pinclude_field_names, const char *pinsert_fields_str, const char *pinsert_values_str, char *pSQL_statements, char *pSQL_single_merge_statement_buf);

extern char *SQL_MERGE_build_legacy_report_insert_merge_and_append_to_SQL_statements(const char *ptable_name, const DATE_TIME, const char *psearch_condition, const BOOL_32 pinclude_field_names, const char *pinsert_fields_str, const char *pinsert_values_str, char *pSQL_statements, char *pSQL_single_merge_statement_buf);

extern char *SQL_MERGE_build_legacy_report_insert_and_append_to_SQL_statements(const char *ptable_name, const DATE_TIME, const BOOL_32 pinclude_field_names, const char *pinsert_fields_str, const char *pinsert_values_str, char *pSQL_statements, char *pSQL_single_merge_statement_buf);

extern char *SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements( const char *ptable_name, const char *psearch_condition, const BOOL_32 pinclude_field_names, const char *pupdate_str, const char *pinsert_fields_str, const char *pinsert_values_str, char *pSQL_statements, char *pSQL_single_merge_statement_buf );

extern char *SQL_MERGE_build_legacy_pdata_insert_and_append_to_SQL_statements( const char *ptable_name, const BOOL_32 pinclude_field_names, const char *pinsert_fields_str, const char *pinsert_values_str, char *pSQL_statements, char *pSQL_single_merge_statement_buf);
// ----------

extern void SQL_MERGE_build_search_condition_bool( const char *pfield_name, const BOOL_32 pvalue, char *psearch_condition );

extern void SQL_MERGE_build_search_condition_char( const char *pfield_name, const char pvalue, char *psearch_condition );

extern void SQL_MERGE_build_search_condition_uint32( const char *pfield_name, const UNS_32 pvalue, char *psearch_condition );

extern void SQL_MERGE_build_search_condition_int32( const char *pfield_name, const INT_32 pvalue, char *psearch_condition );

extern void SQL_MERGE_build_search_condition_float( const char *pfield_name, const float pvalue, char *psearch_condition );

extern void SQL_MERGE_build_search_condition_double( const char *pfield_name, const double pvalue, char *psearch_condition );

extern void SQL_MERGE_build_search_condition_string( const char *pfield_name, const char *pstr, char *psearch_condition );

extern void SQL_MERGE_build_search_condition_sql_cursor_var( const char *pfield_name, const char *pstr, char *psearch_condition );

extern void SQL_MERGE_build_search_condition_date( const char *pfield_name, const UNS_32 pdate, char *psearch_condition );

extern void SQL_MERGE_build_search_condition_time( const char *pfield_name, const UNS_32 ptime, char *psearch_condition );

extern void SQL_MERGE_build_search_condition_timestamp( const char *pfield_name, const DATE_TIME pdt, char *psearch_condition );

// ----------

#define	SQL_MERGE_build_insert_bool_without_field_names( pnew_value, pinsert_values_str )			SQL_MERGE_build_insert_bool( pnew_value, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, NULL, pinsert_values_str )

#define	SQL_MERGE_build_insert_char_without_field_names( pnew_value, pinsert_values_str )			SQL_MERGE_build_insert_char( pnew_value, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, NULL, pinsert_values_str )

#define	SQL_MERGE_build_insert_uint32_without_field_names( pnew_value, pinsert_values_str )			SQL_MERGE_build_insert_uint32( pnew_value, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, NULL, pinsert_values_str )

#define	SQL_MERGE_build_insert_int32_without_field_names( pnew_value, pinsert_values_str )			SQL_MERGE_build_insert_int32( pnew_value, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, NULL, pinsert_values_str )

#define	SQL_MERGE_build_insert_float_without_field_names( pnew_value, pinsert_values_str )			SQL_MERGE_build_insert_float( pnew_value, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, NULL, pinsert_values_str )

#define	SQL_MERGE_build_insert_double_without_field_names( pnew_value, pinsert_values_str )			SQL_MERGE_build_insert_double( pnew_value, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, NULL, pinsert_values_str )

#define	SQL_MERGE_build_insert_string_without_field_names( pnew_value, pstr_len, pinsert_values_str )	SQL_MERGE_build_insert_string( pnew_value, pstr_len, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, NULL, pinsert_values_str )

#define	SQL_MERGE_build_insert_sql_cursor_var_without_field_names( pnew_value, pinsert_values_str )		SQL_MERGE_build_insert_sql_cursor_var( pnew_value, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, NULL, pinsert_values_str )

#define	SQL_MERGE_build_insert_raw_without_field_names( pnew_value, praw_data_len, pinsert_values_str )	SQL_MERGE_build_insert_string( pnew_value, praw_data_len, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, NULL, pinsert_values_str )

#define	SQL_MERGE_build_insert_date_without_field_names( pnew_value, pinsert_values_str )			SQL_MERGE_build_insert_date( pnew_value, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, NULL, pinsert_values_str )

#define	SQL_MERGE_build_insert_time_without_field_names( pnew_value, pinsert_values_str )			SQL_MERGE_build_insert_time( pnew_value, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, NULL, pinsert_values_str )

#define	SQL_MERGE_build_insert_timestamp_without_field_names( pnew_value, pinsert_values_str )		SQL_MERGE_build_insert_timestamp( pnew_value, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, NULL, pinsert_values_str )

// ----------

extern void SQL_MERGE_build_insert_bool( const BOOL_32 pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_insert_char( const char pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_insert_uint32( const UNS_32 pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_insert_int32( const INT_32 pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_insert_float( const float pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_insert_double( const double pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_insert_string( const char* pnew_value, const UNS_32 pstr_len, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_insert_sql_cursor_var( const char* pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_insert_raw( const char* pnew_value, const UNS_32 praw_data_len, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_insert_date( const UNS_32 pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_insert_time( const UNS_32 pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_insert_timestamp( const DATE_TIME pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pinsert_fields_str, char* pinsert_values_str );

// ----------

extern void SQL_MERGE_build_upsert_bool( const BOOL_32 pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pupdate_str, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_upsert_char( const char pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pupdate_str, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_upsert_uint32( const UNS_32 pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pupdate_str, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_upsert_int32( const INT_32 pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pupdate_str, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_upsert_float( const float pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pupdate_str, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_upsert_double( const double pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pupdate_str, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_upsert_string( const char* pnew_value, const UNS_32 pstr_len, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pupdate_str, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_upsert_sql_cursor_var( const char* pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pupdate_str, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_upsert_raw( const char* pnew_value, const UNS_32 praw_data_len, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pupdate_str, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_upsert_date( const UNS_32 pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pupdate_str, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_upsert_time( const UNS_32 pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pupdate_str, char* pinsert_fields_str, char* pinsert_values_str );

extern void SQL_MERGE_build_upsert_timestamp( const DATE_TIME pnew_value, const BOOL_32 pinclude_field_names, const char* pfield_name, char* pupdate_str, char* pinsert_fields_str, char* pinsert_values_str );

extern char *make_timestamp_SQL_ready(const DATE_TIME pnew_value, char *ptimestamp_str, const UNS_32 pstr_len);

extern void update_cs3000_last_timestamp_in_network_table(const UNS_32 pnetwork_id, DATE_TIME llast_time_report_was_retrieved, char *field_name, char *pSQL_statements, char *pSQL_single_merge_statement_buf);

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

