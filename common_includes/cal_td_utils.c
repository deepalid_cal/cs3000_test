/*  file = cal_td_utils.c    2009.05.27  rmd                  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* a group of julian time and date routines for years 1901 through 2078 */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/6/2014 rmd : Including this into the kickstart build sets off a long chain
// of complication. The kickstart build needs to break from the CS3000 main app
// here. And the TPMICRO doesn't need it either. All we are after is the first
// two functions for both the KICKSTART and TPMICRO.
#if !defined (KICKSTART_CODE_GENERATION) && !defined (TPMICRO_CODE_GENERATION)

	// 6/26/2014 ajv : Required since this file is shared between the CS3000 and
	// the comm server
	#include	"cs3000_comm_server_common.h"

	// 6/18/2014 ajv : Required for strlcat definition
	#include	<string.h>

	#include	"cal_string.h"

#endif

#include	"cal_td_utils.h"

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern UNS_32 DMYToDate( const UNS_32 pday, UNS_32 pmonth, UNS_32 pyear_4digit )
{
	// 2/6/2014 rmd : I believe the day and month are 1 based values. Not 0 based!
	
	UNS_32	rv;

	pyear_4digit -= 1900;
	
	if( pmonth > 2 )
	{
		pmonth -= 3;
	}
	else
	{
		pmonth += 9;

		--pyear_4digit;
	}

	rv = (((pyear_4digit * 1461) / 4) + ( ((153 * pmonth) + 2) / 5) + pday + 58);

	return( rv );
}

/* ---------------------------------------------------------- */
// 3/23/2016 skc : Return a completed DATE_TIME_COMPLETE_STRUCT given day and time values
extern void DateAndTimeToDTCS( const UNS_32 pdate, const UNS_32 ptime, DATE_TIME_COMPLETE_STRUCT* pdtcs_ptr )
{
	UNS_32	lday_of_month, lmonth_1_12, lyear, ldow;
	UNS_32  lhour, lmin, lsec;

	pdtcs_ptr->date_time.D = pdate; // Set the day for the running date

	// Calculate the other date values from the new day number
	DateToDMY( pdtcs_ptr->date_time.D, &lday_of_month, &lmonth_1_12, &lyear, &ldow );

	pdtcs_ptr->__month = lmonth_1_12;

	pdtcs_ptr->__day = lday_of_month;

	pdtcs_ptr->__year = lyear;

	pdtcs_ptr->__dayofweek = ldow;

	// ----------

	pdtcs_ptr->date_time.T = ptime;

	TimeToHMS( ptime, &lhour, &lmin, &lsec );

	pdtcs_ptr->__hours = lhour;

	pdtcs_ptr->__minutes = lmin;

	pdtcs_ptr->__seconds = lsec;
}

//------------------------------------------------------------------------
// HMSToTime
//
// This function converst the Hour, Minute, and Second actual parameters
// to seconds and returns the components to the caller.  This function
// is callable globally. (01.26.00 wm)
//------------------------------------------------------------------------
extern UNS_32 HMSToTime( UNS_32 phour, const UNS_32 pmin, const UNS_32 psec )
{
	UNS_32 ltime;

	phour = (phour % 24);

	ltime = (phour * 3600) + (pmin * 60) + psec;

	ltime = ltime % 86400;

	return( ltime );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#if !defined (KICKSTART_CODE_GENERATION) && !defined (TPMICRO_CODE_GENERATION)

// 2/6/2014 rmd : All the functions that follow are included into the CS3000 build. But not
// into the KICKSTART or TPMICRO buildS. This was the easiest way to allow the common use of
// the two above time and date functions.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern UNS_32 DayOfWeek( const UNS_32 pdate )
{
	UNS_32	rv;

	rv = ((pdate + 1) % 7);   // 0 - 6 (0 is Sunday)

	return( rv );
}

/* ---------------------------------------------------------- */
extern void DateToDMY( const UNS_32 pdate, UNS_32 *const pday_ptr, UNS_32 *const pmonth_ptr, UNS_32 *const pyear_4digit_ptr, UNS_32 *const pdow_ptr )
{
	UNS_32 II;

	*pdow_ptr = DayOfWeek( pdate );

	II = (4 * (pdate - 58)) - 1;
	*pyear_4digit_ptr = II / 1461;

	II = (5 * ((II % 1461) / 4)) + 2;

	*pmonth_ptr = II / 153;

	*pday_ptr = ((II % 153) + 5) / 5;

	if( *pmonth_ptr < 10 )
	{
		*pmonth_ptr += 3;
	}
	else
	{
		*pmonth_ptr -= 9;
		*pyear_4digit_ptr += 1;
	}

	*pyear_4digit_ptr += 1900;
}

/* ---------------------------------------------------------- */
extern BOOL_32 IsLeapYear( const UNS_32 pyear_4digit )
{
	// Ye should be the full 4 digit year i.e. 1978, or 2004, not 78 or 04
	return( ((pyear_4digit % 4 == 0) && (pyear_4digit % 100 != 0)) || (pyear_4digit % 400 == 0) );
}

/* ---------------------------------------------------------- */
extern UNS_32 NumberOfDaysInMonth( const UNS_32 pmonth_1, const UNS_32 pyear_4digit )
{
	UNS_32 rv;

	switch( pmonth_1 )
	{
		case 4:
		case 6:
		case 9:
		case 11:
			rv = 30;
			break;
			
		case 2:
			if( IsLeapYear( pyear_4digit ) == (true) )
			{
				rv = 29;
			}
			else
			{
				rv = 28;
			}
			break;
			
		default:
			rv = 31;
			break;
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern void TimeToHMS( UNS_32 ptime, UNS_32 *const phour_ptr, UNS_32 *const pmin_ptr, UNS_32 *const psec_ptr )
{
	*phour_ptr = ptime / 3600;
	ptime -= *phour_ptr * 3600;

	*pmin_ptr = ptime / 60;
	ptime -= *pmin_ptr * 60;

	*psec_ptr = ptime;
}

/* ---------------------------------------------------------- */
extern char *TDUTILS_time_to_time_string_with_ampm( char *const dest,
													const UNS_32 pdest_size, 
													const UNS_32 ptime,
													const BOOL_32 ptreat_as_a_start_time, 
													const BOOL_32 pinclude_seconds, 
													const BOOL_32 pconcat_to_pstr )
{
	char	str_32[ 32 ];	

	UNS_32	lhour, lmin, lsec;

	BOOL_32	lshow_PM;


	if( (ptreat_as_a_start_time == TDUTILS_TREAT_AS_A_SX ) && (ptime == 86400) )
	{
		snprintf( str_32, sizeof(str_32), "%s", GetOffOnStr(0) );
	}
	else
	{
		TimeToHMS( ptime, &lhour, &lmin, &lsec );

		lshow_PM = (false);

		if( lhour == 0 )
		{
			lhour = 12;
		}
		else if( lhour == 12 )
		{
		    lshow_PM = (true);
		}
		else if( lhour > 12 )
		{
		    lhour -= 12;
		    lshow_PM = (true);
		}

		// generates of the format " 4:08PM" or "12:10AM"
        snprintf( str_32, sizeof(str_32), "%2u:%02u", lhour, lmin );

		if( pinclude_seconds == TDUTILS_INCLUDE_SECONDS )
		{
			sp_strlcat( str_32, sizeof(str_32), ":%02u", lsec );
		}
		
		if( lshow_PM == (true) )
		{
			strlcat( str_32, "PM", sizeof(str_32) );
		}
		else
		{
		    strlcat( str_32, "AM", sizeof(str_32) );
		}
	}  // of IF not treated as a start time	
	

	// If we are NOT going to add to the string make sure its empty
	if( pconcat_to_pstr == TDUTILS_DONT_CONCAT_TO_STRING )
	{
		*dest = 0x00;
	}
	
	strlcat( dest, str_32, pdest_size );

	return( dest );
}

/* ---------------------------------------------------------- */
extern char *GetDateStr( char *const pdest, const UNS_32 pdest_size, const UNS_32 pdate, const UNS_32 pshow_year, const UNS_32 pshow_dow )
{
	UNS_32 lday, lmonth, lyear;

	UNS_32 ldow;

	DateToDMY( pdate, &lday, &lmonth, &lyear, &ldow );
	
	*pdest = 0x00;
	
	if ( pshow_dow == DATESTR_show_short_dow )
	{
		sp_strlcat( pdest, pdest_size, "%s ", GetDayShortStr( ldow ) );
	}
  	else
	if ( pshow_dow == DATESTR_show_long_dow )
	{
		sp_strlcat( pdest, pdest_size, "%s ", GetDayLongStr( ldow ) );
	}

	sp_strlcat( pdest, pdest_size, "%02d/%02d", lmonth, lday );
	
	if ( pshow_year == DATESTR_show_short_year )
	{
		if ( lyear >= 2000 )
		{
			lyear -= 2000;
		}
		else
		{
			lyear -= 1999;
		}

		sp_strlcat( pdest, pdest_size, "/%02d", lyear );
	}
	else if ( pshow_year == DATESTR_show_long_year )
	{
		sp_strlcat( pdest, pdest_size, "/%04d", lyear );
	}

	return( pdest );
}

/* ---------------------------------------------------------- */
// Convert the passed date_time structure to a date and time string not to
// exceed 32 bytes in length. Seconds are not returned in the returned
// string. pstr is assumed to be at least 32 (31 + the null) in length.
extern char *DATE_TIME_to_DateTimeStr_32( char *const pdest, const UNS_32 pdest_size, const DATE_TIME pdate_time )
{
	char t_str_32[ 32 ], d_str_32[ 32 ];

	// We use ShaveLeftPad on the time piece cause we want to pack the string up
	// when there is only 1 digit for the hours.
	snprintf( pdest, pdest_size, "%s %s", 
			  GetDateStr( d_str_32, sizeof(d_str_32), pdate_time.D, DATESTR_show_long_year, DATESTR_show_dow_not ), 
			  ShaveLeftPad( t_str_32, TDUTILS_time_to_time_string_with_ampm( t_str_32, sizeof(t_str_32), pdate_time.T, TDUTILS_DONT_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ) )
			);

	return( pdest );
}

/* ---------------------------------------------------------- */
extern BOOL_32 DT1_IsBiggerThan_DT2( const DATE_TIME *const DT1, const DATE_TIME *const DT2 )
{
	BOOL_32	rv;

	rv = (false);

	if ( DT1->D > DT2->D )
	{
		rv = (true);
	}
	else if ( DT1->D == DT2->D )
	{
		rv = ( DT1->T > DT2->T );
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 DT1_IsBiggerThanOrEqualTo_DT2( const DATE_TIME *const DT1, const DATE_TIME *const DT2 )
{
	BOOL_32	rv;

	rv = (false);

	if ( DT1->D > DT2->D )
	{
		rv = (true);
	}
	else if( DT1->D == DT2->D )
	{
		rv = ( DT1->T >= DT2->T );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 DT1_IsEqualTo_DT2( const DATE_TIME *const DT1, const DATE_TIME *const DT2 )
{
	BOOL_32	rv;

	rv = (false);

	if ( ( DT1->D == DT2->D ) && ( DT1->T == DT2->T ) )
	{
		rv = (true);
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#if 0
// 2012.02.08 ajv  - Commented out since this routine is defined but not used.
/* ---------------------------------------------------------- */
BOOL_32 MonthAndYearAreTheSame( const UNS_32 pdate_1, const UNS_32 pdate_2 )
{
	BOOL_32	rv;

	UNS_32	lyear_1, lyear_2;

	UNS_32	lmonth_1, lmonth_2;

	UNS_32	II;

	II = 0;
	II = (4 * (pdate_1 - 58)) - 1;
	lyear_1 = II / 1461;
	II = (5 * ((II % 1461) / 4)) + 2;
	lmonth_1 = II / 153;
	if( lmonth_1 < 10)
	{
		lmonth_1 += 3;
	}
	else
	{
		lmonth_1 -= 9;
		++lyear_1;
	}
	
	II = 0;
	II = (4 * (pdate_2 - 58))-1;
	lyear_2 = II / 1461;
	II = (5 * ((II % 1461) / 4)) + 2;
	lmonth_2 = II / 153;
	if( lmonth_2 < 10 )
	{
		lmonth_2 += 3;
	}
	else
	{
		lmonth_2 -= 9;
		++lyear_2;
	}
	
	rv = ( (lyear_1 == lyear_2) && (lmonth_1 == lmonth_2) );

	return( rv );
}
#endif

#if 0
// 2012.02.08 ajv  - Commented out since this routine is defined but not used.
/* ---------------------------------------------------------- */
char *TD_UTIL_build_long_date_time_string( char *const pdest, const size_t pdest_size, const DATE_TIME_COMPLETE_STRUCT *pdtcs )
{
	// NULL the string
	*pdest = 0x00;

	strlcat( pdest, GetDayLongStr( pdtcs->__dayofweek ), pdest_size );

	strlcat( pdest, ", ", pdest_size );

	strlcat( pdest, GetMonthLongStr( (pdtcs->__month - 1) ), pdest_size );

	sp_strlcat( pdest, pdest_size, " %02d %4d ", pdtcs->__day, pdtcs->__year );

	TDUTILS_time_to_time_string_with_ampm( pdest,
										   pdest_size,
										   pdtcs->date_time.T,
										   TDUTILS_DONT_TREAT_AS_A_SX,
										   TDUTILS_INCLUDE_SECONDS,
										   TDUTILS_CONCAT_TO_STRING );

	return( pdest );
}
*/
#endif

/* ---------------------------------------------------------- */
extern void TDUTILS_add_seconds_to_passed_DT_ptr( DATE_TIME *const pdt, const UNS_32 pseconds )
{
	UNS_32 ldays_to_add;

	UNS_32 lseconds_to_add, lseconds_to_start_of_next_day;  

	// ----------

	ldays_to_add = pseconds / 86400;
	
	lseconds_to_add = pseconds % 86400;
	
	// Deal with adding seconds that causes us to cross midnight
	lseconds_to_start_of_next_day = 86400 - pdt->T;
	
	if ( lseconds_to_add >= lseconds_to_start_of_next_day )
	{
		lseconds_to_add -= lseconds_to_start_of_next_day;
		
		++ldays_to_add;
		
		// And modify the passed dt to start at midnight since in our thoughts
		// we just jumped up to the start of the new day
		pdt->T = 0;
	}

	// ----------
	
	pdt->D += ldays_to_add;
	
	pdt->T += lseconds_to_add;
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

