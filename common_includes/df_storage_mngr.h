/* file = df_storage_mngr.h                  03.28.2011  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_DF_STORAGE_MNGR_H_
#define INC_DF_STORAGE_MNGR_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER



#ifdef KICKSTART_CODE_GENERATION

	#include	"ks_common.h"

#elif defined TPMICRO_CODE_GENERATION

	#include	"tpmicro_common.h"

#else

	#include	"cs_common.h"

	#include	"lpc_types.h"

#endif



#include	"df_storage_internals.h"


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/6/2014 rmd : The location in the code image binary of the build date and time revision
// strings.

#define	MAIN_APP_RUNNING_BUILD_DT	((char*)0x80000040)

#define	MAIN_APP_BUILD_DT_OFFSET	(0x00000040)

#define	TPMICRO_BUILD_DT_OFFSET		(0x000000d0)


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern const char CS3000_APP_FILENAME[];

extern const char TPMICRO_APP_FILENAME[];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Item Directory Entry Structure
//
//	A directory entry contains the details about a file as well as the CAT. What is important is that the
//  whole thing fits within one in page. The code is expecting that. But with 4096 bytes we are not even close
//  to filling a page!
// 
///////////////////////////////////////////////////////////////////////////////////////////////////

#define DF_MAX_FILENAME_LEN			32			// Not checked, filenames are truncated as needed

typedef struct
{
	char		Filename_c[ DF_MAX_FILENAME_LEN ];	// Null-padded filename		(KEY FIELD)
	
	// 1/21/2015 rmd : And unused field. Was the code image date / data version field. But
	// rendered out of use when we implemented the structure update scheme.
	UNS_32		aaaaa_ul;
	
	// as we edit an item and then want to update it's copy in flash we would bump this edit_revision number
	// WHY? so that would force us to store a new second copy of the item as it wouldn't yet exist...we then need
	// to go back and cleanup the flash as a background task...looking for older than the most recently edited
	// version of a particular item and delete it
	// 3/3/2014 rmd : This field is used for the code image TIME.
	UNS_32		edit_count_ul;				// (KEY FIELD)
	
	// 1/27/2015 rmd : The file size.
	UNS_32		ItemSize_ul;
	
	// ----------
	
	// 1/21/2015 rmd : For the case of the MAIN code image and the TPMICRO code image this is
	// the DATE field. This field also doubles as the file content revision field. When used for
	// content revision the starting value is 0. And then should only increment by one as the
	// contents definition changes. In order to preserve PRIOR file contents new variables must
	// be added to the END of existing structure.
	UNS_32		code_image_date_or_file_contents_revision;		// (KEY FIELD)

	// ----------
	
	UNS_16		bbbbb_us;   				// UNUSED SO FAR

	// ----------
	
	
	UNS_8		ccccc_uc;   				// UNUSED SO FAR
	
	// ----------
	
	UNS_8		InUse_uc;					// Non-zero if dir. entry active 
	
	
	CRC_BASE_TYPE	ItemCrc32_ul;			// Calculated CRC-32 value for file data. This is NOT the CRC of the directory entry itself. 
	
	
	DF_CAT_s		cat;					// The cat is 32 words or 128 bytes in size. Sometimes we read out the directory entry
											// and stop short of the cat. For example while searching for an open directory entry.
											// This of course saves time. But gee...the cat is only 32 bytes in this SST25VF implementation.
} DF_DIR_ENTRY_s;

// ----------

// DF_DIR_ENTRY_KEY_MASKS - these bit masks are used by the DfFindDirEntry function to tell it
//                          which DF_DIR_ENTRY_s fields should be compared when searching for a matching
//                          directory entry. A mask value of zero will always match. 
#define DF_COMPARISON_MASK_FILENAME				(0x0001)

#define DF_COMPARISON_MASK_EDIT_COUNT			(0x0002)

#define DF_COMPARISON_MASK_DATE_OR_REVISION		(0x0004)

// ----------

// The following values are returned by the DirEntryExists and other functions.
#define DF_RESULT_SUCCESSFUL			0x00000000
#define DF_DIR_ENTRY_NOT_FOUND			0xffffffff
#define DF_INSUFFICIENT_SPACE			0xfffffffe
#define DF_DUPL_ITEM_ALREADY_EXISTS		0xfffffffd	// based upon ItemId, VersionId_ul, Filename_c 
#define DF_ZERO_ITEM_SIZE_IS_INVALID	0xfffffffc
#define DF_NO_DIR_ENTRY_AVAIL			0xfffffffb
#define DF_INVALID_CLUSTER_VALUE		0xfffffffa
#define DF_INVALID_SIZE_SPECIFIED		0xfffffff9
#define DF_CRC_DOES_NOT_MATCH			0xfffffff8
#define DF_CLUSTER_NOT_FOUND			0xfffffff7
#define DF_IMPENDING_POWER_FAIL_ABORT	0xfffffff6
#define DF_TOO_FEW_BYTES_TO_WRITE		0xfffffff5
#define DF_TOO_MANY_BYTES_TO_WRITE		0xfffffff4
#define DF_FUNCTIONAL_ERROR				0xfffffff3


////////////////////////////////////////////////////////////////////////
// Function prototypes (public functions) - These functions are 
// designed to be called from application programs.
////////////////////////////////////////////////////////////////////////

extern UNS_32 convert_code_image_date_to_version_number( const UNS_8* const pbuffer_start, const char* const pfile_name );

extern UNS_32 convert_code_image_time_to_edit_count( const UNS_8* const pbuffer_start, const char* const pfile_name );



void init_FLASH_DRIVE_file_system( UNS_32 flash_index, unsigned char force_uc );


signed portBASE_TYPE DfTakeStorageMgrMutex( UNS_32 flash_index, portTickType TicksToWait );


void DfGiveStorageMgrMutex( UNS_32 flash_index );


unsigned long DfGetFreeSpace_NM( UNS_32 flash_index );


extern UNS_32 DfFindDirEntry_NM( UNS_32 flash_index, DF_DIR_ENTRY_s *psearch_dir_entry, DF_DIR_ENTRY_s *pfound_dir_entry, UNS_32 pfields_to_check_us, UNS_32 starting_dir_index_ul );


extern UNS_32 DfReadFileFromDF_NM( const UNS_32 flash_index, const DF_DIR_ENTRY_s *pdir_entry, const UNS_8 *pwhere_to );

// 9/5/2013 ajv : The pff_name parameter is only used by the CS3000 code as a
// way to index into the array of file CRCs. For the Kickstart, we don't have a
// list of file CRCs, so use the existing KICKSTART define to prevent the
// inclusion of this parameter.
extern UNS_32 DfWriteFileToDF_NM( UNS_32 flash_index, DF_DIR_ENTRY_s *pdir_entry, unsigned char *pitem_buffer_ptr );


void DfDelFileFromDF_NM( UNS_32 flash_index, unsigned long pdir_page, DF_DIR_ENTRY_s *pdir_entry );


// This one doesn't need a mutex...thus no _NM
UNS_32 DfDirEntryCompare(DF_DIR_ENTRY_s *pDirEntry1, DF_DIR_ENTRY_s *pDirEntry2, UNS_32 fields_to_check_us);


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /*INITDATAFLASH_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

