/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs3000_comm_server_common.h"

#include	<stdio.h>

#include	"cal_string.h"

#include	"foal_defs.h"

#ifndef _MSC_VER

	#include	"battery_backed_vars.h"

	#include	"flowsense.h"

#endif




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern char *STATION_get_station_number_string( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, char *pstr, const UNS_32 pstrlen )
{
	if( pstation_number_0 < MAX_CONVENTIONAL_STATIONS )
	{
		snprintf( pstr, pstrlen, "%u", (pstation_number_0 + 1) );
	}
	#ifndef _MSC_VER
	else if( controller_is_2_wire_only( pbox_index_0 ) )
	{
		// 9/6/2016 ajv : Suppress the "T" for 2-Wire stations if there are not station cards or
		// terminals installed.
		snprintf( pstr, pstrlen, "%u", (pstation_number_0 + 1 - MAX_CONVENTIONAL_STATIONS) );
	}
	#endif
	else
	{
		snprintf( pstr, pstrlen, "T%u", (pstation_number_0 + 1 - MAX_CONVENTIONAL_STATIONS) );
	}

	return( pstr );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

