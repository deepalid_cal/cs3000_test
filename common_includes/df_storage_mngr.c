/* file = df_storage_mngr.c                  03.28.2011  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for abs
#include	<stdlib.h>

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"df_storage_mngr.h"

#include	"cal_td_utils.h"


#ifndef TPMICRO_CODE_GENERATION

	#include	"app_startup.h"
	
	#include	"crc.h"
	
	#include	"cs_mem.h"

	#include	"flash_storage.h"
	
	#include	"spi_flash_driver.h"
	
	#include	"packet_definitions.h"
	
#endif



// ----------

#if defined (KICKSTART_CODE_GENERATION) || defined (TPMICRO_CODE_GENERATION)

	// not included for kickstart

#else

	#include	"wdt_and_powerfail.h"
	
	#include	"alerts.h"

#endif

// ----------

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

const char CS3000_APP_FILENAME[] = "CS_3000_APP";

const char TPMICRO_APP_FILENAME[] = "TP_MICRO_APP";

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static char const * const df_month_strings[ 12 ] =
{
	"Jan",
	"Feb",
	"Mar",
	"Apr",
	"May",
	"Jun",
	"Jul",
	"Aug",
	"Sep",
	"Oct",
	"Nov",
	"Dec"
};

extern UNS_32 convert_code_image_date_to_version_number( const UNS_8* const pbuffer_start, const char* const pfile_name )
{
	// 2/6/2014 rmd : We are using the __DATE__ macro in the assembly startup to embed the date
	// string into the code image at a specific location. Happens to be a different location for
	// the main code versus the tp micro code. We convert the date string to the number of days
	// since a starting date. And that becomes the file revision number.

	// 2/6/2014 rmd : The code revision string in memory has a very specific format. It is as
	// follows: 0123456789012345678901234567890
	//          Feb__6_2014_@_10:21:36
	//          mmm_dd_yyyy_@_hh:mm:ss
	UNS_32	day, month, year;
	
	UNS_32	rv;
	
	BOOL_32	found_month;
	
	char	*date_ptr;
	
	// ----------

	found_month = (false);
	
	// 2/7/2014 rmd : If 0 returned is picked up as an error.
	rv = 0;
	
	// ----------

	if( (pfile_name == CS3000_APP_FILENAME) )
	{
		date_ptr = (char*)pbuffer_start + MAIN_APP_BUILD_DT_OFFSET;	
	}
	else
	if( (pfile_name == TPMICRO_APP_FILENAME) )
	{
		date_ptr = (char*)pbuffer_start + TPMICRO_BUILD_DT_OFFSET;	
	}
	else
	{
		// 2/6/2014 rmd : Problem. Git.
		return( rv );
	}
	
	// ----------

	for( month=0; month<12; month++ )
	{
		if( strncmp( date_ptr, df_month_strings[ month ], 3 ) == 0 )
		{
			// 2/6/2014 rmd : BECAUSE month is 1 based! We must add one to the index. Our loop here is 0
			// based.
			month += 1;

			// 2/6/2014 rmd : We found the string. Stop.
			found_month = (true);
			
			break;
		}
	}

	if( found_month )
	{
		// 2/7/2014 rmd : Must skip over spaces else atoi will not convert. The __DATE__ format
		// includes a space for any single digit number.
		if( *(date_ptr + 4) != 0x20 )
		{
			day = atoi( (date_ptr + 4) );
		}
		else
		{
			day = atoi( (date_ptr + 5) );
		}
		
		year = atoi( (date_ptr + 7) );

		rv = DMYToDate( day, month, year );
	}

	return( rv );
}

extern UNS_32 convert_code_image_time_to_edit_count( const UNS_8* const pbuffer_start, const char* const pfile_name )
{
	// 2/6/2014 rmd : See date related function above for a bit more explanation.

	// 2/6/2014 rmd : The code revision string in memory has a very specific format. It is as
	// follows: 0123456789012345678901234567890
	//          Feb__6_2014_@_10:21:36
	//          mmm_dd_yyyy_@_hh:mm:ss
	UNS_32	hour, minute, second;
	
	UNS_32	rv;
	
	char	*time_ptr;

	// ----------

	// 2/7/2014 rmd : If 0 returned is picked up as an error.
	rv = 0;
	
	// ----------

	if( (pfile_name == CS3000_APP_FILENAME) )
	{
		time_ptr = (char*)pbuffer_start + MAIN_APP_BUILD_DT_OFFSET;	
	}
	else
	if( (pfile_name == TPMICRO_APP_FILENAME) )
	{
		time_ptr = (char*)pbuffer_start + TPMICRO_BUILD_DT_OFFSET;	
	}
	else
	{
		// 2/6/2014 rmd : Problem. Git.
		return( rv );
	}
	
	// ----------

	// 2/6/2014 rmd : Point to the hours.
	time_ptr += 14;

	// ----------

	if( *(time_ptr) != 0x20 )
	{
		hour = atoi( (time_ptr) );
	}
	else
	{
		hour = atoi( (time_ptr + 1) );
	}

	// ----------

	if( *(time_ptr + 3) != 0x20 )
	{
		minute = atoi( (time_ptr + 3) );
	}
	else
	{
		minute = atoi( (time_ptr + 4) );
	}

	// ----------

	if( *(time_ptr + 6) != 0x20 )
	{
		second = atoi( (time_ptr + 6) );
	}
	else
	{
		second = atoi( (time_ptr + 7) );
	}

	// ----------

	rv = HMSToTime( hour, minute, second );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/5/2014 rmd : Thats all we wanted for the TP MICRO. The two functions to get the code
// version information.

#ifndef TPMICRO_CODE_GENERATION

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//////////////////////////////////////////////////////////////
//
//  DF Storage Manager functions
//
//////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////
//
//	long DfGainStorageMgrAccess(portTickType TicksToWait)
//
//	By convention, this function must first be called to obtain 
//  exclusive access to the DF Storage Manager. Once DF access
//  is complete, then the DfReleaseStorageMgrAccess() function
//  must be called to make the DF_Storage Manager functions 
//  available for other callers.
//
//  TicksToWait : # FreeRTOS timer ticks to wait for access, or
//                specify portMAX_DELAY to wait indefinitely.
//
//  RETURNS: PdTRUE : Access was obtained before time-out
//           PdFALSE: Access was not obtained (time-out occurred) 
//
//	WARNING: Protection against reentrancy of the DF Storage
//           Manager functions only occurs when all callers
//           follow the get/release access convention. The DF 
//           Storage Manager code itself has no protection in 
//           its code to prevent reentrancy.
//	
///////////////////////////////////////////////////////////////
signed portBASE_TYPE DfTakeStorageMgrMutex( UNS_32 flash_index, portTickType TicksToWait )
{
	// NOTE: In the LPC3250 implementation we have two data flash parts. Each on a separate SPI
	// channel. And all operations are funneled through the task queue. One task for each
	// channel. So there is actually not a need for the flash file system MUTEX. But since I
	// already had it in place from the AVR32 board I left it here. It is a small impact to the
	// code. And if the design ever needs such it is done. But perhaps not well tested.

	signed portBASE_TYPE	rv;

	rv = xSemaphoreTake( *(ssp_define[ flash_index ].flash_mutex_ptr), TicksToWait );

	if( rv != pdPASS )
	{
		// 2/20/2014 rmd : This should NEVER happen as all operations take place via the
		// flash_manager task queue. !!! Except the startup file reads.
		Alert_Message( "DF_MNGR: couldn't take mutex!" );
	}

	return( rv );
}

///////////////////////////////////////////////////////////////
//
//	void DfReleaseStorageMgrAccess(void)
//
//  This function must be called to make access to the DF
//  Storage Manager available to other callers.
//
///////////////////////////////////////////////////////////////
void DfGiveStorageMgrMutex( UNS_32 flash_index )
{
	signed portBASE_TYPE	rv;

	rv = xSemaphoreGive( *(ssp_define[ flash_index ].flash_mutex_ptr) );

	if( rv != pdPASS )
	{
		Alert_Message( "DF_MNGR: couldn't give mutex!" );
	}
}

/* ---------------------------------------------------------- */
//  Find (First and Next) Cluster of a particular state -
//
//  To obtain the first cluster of a particular state, pass in a valid pointer to the CAT 
//  to check and set *pCluster to zero. If a cluster of the requested state is found, 
//  then its cluster number is stored in *pCluster and the function returns DF_RESULT_SUCCESSFUL
//  to indicate success.
//
//  If no cluster of the specified state is found, then *pCluster is not updated and the 
//  function returns DF_CLUSTER_NOT_FOUND.
//
//  To find the next cluster, pass in a valid pointer to the CAT to continue
//  checking and set *pCluster to one more than the last value returned in *pCluster. 
//  The return status and *pCluster will be modified as before based upon the availability 
//  of clusters.
//
//  This process can be repeated until no additional clusters are found. 
//
UNS_32 DfFindCluster( UNS_32 flash_index, void *pCAT, UNS_8 StateSought_uc, UNS_32 *pCluster_ul )
{
	unsigned long status;
	unsigned long cluster_start;
	unsigned long cluster;
	
	status = DF_CLUSTER_NOT_FOUND;
	
	// Check for cluster value in range
	if( *pCluster_ul <= DF_LAST_DATA_CLUSTER )
	{
		// Assign reasonable start cluster #
		if( *pCluster_ul < ssp_define[ flash_index ].first_data_cluster )
		{
			cluster_start = ssp_define[ flash_index ].first_data_cluster;
		}
		else
		{
			// We are doing a "find next" operation, so use the
			// caller's value for the starting cluster number
			cluster_start = *pCluster_ul;
		}
		
		for( cluster = cluster_start; cluster <= DF_LAST_DATA_CLUSTER; cluster++ )
		{
			// If the state detected isn't StateSought, keep on looking
			if( StateSought_uc != DF_test_cluster_bit( pCAT, cluster ) ) continue;
			{
				// we found a cluster of the state sought
				*pCluster_ul = cluster;
				status = DF_RESULT_SUCCESSFUL;
				break;
			}
		}
	}
	
	return( status );
}

/* ---------------------------------------------------------- */

// 11/14/2012 rmd : This function proved to not be needed. It was only used prior to reading
// a file from the file system into memory. We now read the file out and then check the CRC
// with the file contents in one big contiguous block. Which the user had to have secured
// prior to reading anyway.

/*
//////////////////////////////////////////////////////////////////////////////
//
//  static unsigned long DfCheckDirItemCRC_NM( DF_PAGE_BUFFER_UNION *pfull_dir_entry )
//
//  This routine checks the CRC of a stored data item against the CRC 
//  stored in its directory entry. It reads the item data and computes the CRC
//  one cluster-sized buffer at a time to minimize the buffer size needed.
//
//	RETURNS: 
//
// 		DF_RESULT_SUCCESSFUL 	: the CRC matches that calculated for the item's
//                                data in the DF.
//
//		DF_DIR_ENTRY_NOT_FOUND  : The specified data item was not found
//	
//		DF_CRC_DOES_NOT_MATCH	: The directory entry CRC does not match
//								  that calculated from the DF data itself. 
//
//////////////////////////////////////////////////////////////////////////////
static unsigned long DfCheckDirItemCRC_NM( UNS_32 flash_index, DF_DIR_ENTRY_s *pdir_entry )
{
	unsigned long	rstatus_ul;
	
	DF_CAT_s		*pCAT;
	
	unsigned long	BytesToCrc;
	
	unsigned long	BytesToRead;
	
	CRC_TYPE		lcrc;
	
	unsigned long	cluster;
	
	unsigned char	*cluster_buffer_ptr;


	// NOTE: upon entry we are requiring that pfull_dir_entry is valid...containing a true and accurate directory entry

	// Set up a pointer to the CAT for the directory entry.
	pCAT = &(pdir_entry->cat);
	
	// Initialize the CRC
	lcrc.crc = 0xffffffff;	// this is done prior to the first buffer only

	// First, get the size of the item's data
	BytesToCrc = pdir_entry->ItemSize_ul;
	
	// set up starting cluster # for DfFindCluster function
	cluster = 0;

	rstatus_ul = DF_CRC_DOES_NOT_MATCH; // so if we don't find any clusters we fail

	cluster_buffer_ptr = mem_malloc( DF_BYTES_PER_CLUSTER );

	// Stay in the while loop until error or no more clusters found.
	// Note that here we're looking for SET (TRUE) allocation bits.
	while( 0 == DfFindCluster( flash_index, pCAT, TRUE, &cluster ) )
	{
		// Decide how much to read this time. We will read a full cluster until
		// there is less than a cluster left to read.
		BytesToRead = (BytesToCrc > DF_BYTES_PER_CLUSTER) ? DF_BYTES_PER_CLUSTER : BytesToCrc;
		
		// Read next block of data into our buffer
		rstatus_ul = DF_read_page( flash_index, cluster, cluster_buffer_ptr, BytesToRead );
		
		if( rstatus_ul != DF_RESULT_SUCCESSFUL ) break;

		// Now update the CRC calculation using this buffer
		lcrc.crc = MCalcCRC32( lcrc.crc, cluster_buffer_ptr, BytesToRead );
		
		// Update variables
		BytesToCrc -= BytesToRead;		// reduce by bytes just read/crc'd

		cluster++;						// advance to pick up next cluster

	}  // of while
	
	mem_free( cluster_buffer_ptr );

	if( rstatus_ul == DF_RESULT_SUCCESSFUL)
	{
		// Finish the CRC by complementing all bits (XOR with 1's)
		lcrc.crc ^= 0xffffffff;
		
		// ----------
		
		// 11/6/2012 rmd : Because the crc is calculated in big endian format and this page by page
		// approach results in a little endian format we need to make the swap before comparing.
		CRC_BASE_TYPE	final_crc;
		
		UNS_8			*lucp;
		
		lucp = (UNS_8*)&final_crc;
		
		*lucp++ = lcrc.b[ 3 ];
		*lucp++ = lcrc.b[ 2 ];
		*lucp++ = lcrc.b[ 1 ];
		*lucp   = lcrc.b[ 0 ];
		
		// ----------
		
		// Now check if the CRCs match each other
		if( final_crc == pdir_entry->ItemCrc32_ul )
		{
			// Yes they do match
			rstatus_ul = DF_RESULT_SUCCESSFUL;
		}
		else
		{
			// Uh oh - they do not match
			rstatus_ul = DF_CRC_DOES_NOT_MATCH;
		}

	}

	return( rstatus_ul );
}
*/ 

/////////////////////////////////////////////////////////////////////////////////
//
//	CountAvailClusters - returns the number of cleared (i.e. available) clusters
//                       in a cluster allocation table (CAT).
//
/////////////////////////////////////////////////////////////////////////////////
static unsigned long DfCountAvailClusters( UNS_32 flash_index, void *pCAT )
{
	unsigned long avail;	// # available clusters
	unsigned long c;		// cluster being checked
	
	avail = 0;
	
	for( c = ssp_define[ flash_index ].first_data_cluster; c <= DF_LAST_DATA_CLUSTER; c++ )
	{
		if( FALSE == DF_test_cluster_bit( pCAT, c ) )
		{
			avail++;
		}
	}
	
	return (avail);
}

////////////////////////////////////////////////////////////////////////////////////////
//
//  void initDfStorageMgr(unsigned char force_uc)
//
//		force_uc : if TRUE, forces the DF to be initialized, effectively erasing the 
//                 chip even if the DF chip was already properly initialized and
//                 contained application data. USE WITH CAUTION!
//
//                 if FALSE, checks Master DF record and only reinitialized the chip
//                 if a problem is detected. THIS IS THE NORMAL PARAMETER.
//
//	This function must be called once before calling any other DF Storage Manager 
//  functions! It can also be used to re-initialize/erase the DF Storage system.
//
//  This function first checks the Master DF record of the DF storage medium 
//  to see if it has been initialized. If so, then the storage manager uses the 
//  values read from the Master DF record to compute the values for static variables.
//
////////////////////////////////////////////////////////////////////////////////////////
const char* format_reason[ ] = { "CRC Failed", "ID String Failed", "Forced" };

void init_FLASH_DRIVE_file_system( UNS_32 flash_index, unsigned char force_uc )
{
	unsigned long		lpage_ul;
	
	CRC_BASE_TYPE		CalculatedCRC_ul;
	
	DF_DIR_ENTRY_s		*ldir_entry;
	
	UNS_32				write_results, reason_to_format;
	
    // START of data flash operations...we don't want the data flash to change until we are done doing our operations
	DfTakeStorageMgrMutex( flash_index, portMAX_DELAY );

    // First read DF page 0 to see if the DF chip is initialized. This page **should** contain the Master DF record. First we read it into the page buffer.
	DF_read_page( flash_index, ssp_define[ flash_index ].master_record_page, ssp_define[ flash_index ].mr_struct_ptr, sizeof(MASTER_DF_RECORD_s) );

	// Next, we perform a CRC check on the Master DF record fields to see if they are sane
	CalculatedCRC_ul = CRC_calculate_32bit_big_endian( (unsigned char *)(ssp_define[ flash_index ].mr_struct_ptr), (sizeof(MASTER_DF_RECORD_s) - sizeof(CRC_BASE_TYPE)) );

	// Now we compare our calculated CRC value to the CRC read from DF chip and if good, 
	// then we also check the magic number. If either check fails, we assume the DF is not
	// properly initialized.
	if( (force_uc == TRUE) )
	{
		reason_to_format = 3;
	}
	else
	if( (strncmp( (const char*)((ssp_define[ flash_index ].mr_struct_ptr)->magic_str), (const char*)&(ssp_define[ flash_index ].key_string), DF_MAGIC_STR_LENGTH )!= 0) )
	{
		reason_to_format = 2;
	}
	else
	if( (CalculatedCRC_ul != (ssp_define[ flash_index ].mr_struct_ptr)->CRC32) )
	{
		reason_to_format = 1;
	}
	else
	{
		reason_to_format = 0;
	}



	if( reason_to_format != 0 )
	{
		/////////////////////////////////////////////////////////////////////////////////
		// If the force_uc flag is TRUE or the CRC is incorrect or the magic number 
		// does not match or is not even present so we will re)initialize the DF chip(s).
		// The very last step of this process is to write out the Master DF record. 
		// That way if something interrupts the processing before that, the DF chip 
		// will still appear uninitialized.
		/////////////////////////////////////////////////////////////////////////////////

		Alert_Message_va( "FORMATTING FLASH DRIVE : chip %d, reason is %s", flash_index, format_reason[ (reason_to_format-1) ] );

		// Initialize the whole file system. First all the directory entries.

		// Secure the memory.
		ldir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );

		// Zero it out. This sets them not in use and zeros the CAT within...as well as everything else.
		memset( ldir_entry, 0, sizeof(DF_DIR_ENTRY_s) );

		lpage_ul = sizeof( DF_DIR_ENTRY_s );

		// Need this statement else get a compiler warning about using potentially unitialized write_results.
		write_results = DF_RESULT_SUCCESSFUL;

		// Now write out the configured number of "free" directory entry pages to the DF system
		for( lpage_ul=ssp_define[ flash_index ].first_directory_page; lpage_ul<=ssp_define[ flash_index ].last_directory_page; lpage_ul++ )
		{
			// Only write to the page the amount we need to. In the flash the rest of the page will be 0xFF. 
			write_results = DF_write_page( flash_index, lpage_ul, ldir_entry, sizeof(DF_DIR_ENTRY_s) );
			
			if( write_results != DF_RESULT_SUCCESSFUL )  // this would include an impending power fail
			{
				break;	
			}
		}
		
		mem_free( ldir_entry );  // done with this

		// NOW set up the Master_Record. All CAT bits to zero - unallocated that is.
		memset( ssp_define[ flash_index ].mr_struct_ptr, 0x00, sizeof(MASTER_DF_RECORD_s) );

		memcpy( &((ssp_define[ flash_index ].mr_struct_ptr)->magic_str), &(ssp_define[ flash_index ].key_string), DF_MAGIC_STR_LENGTH );

		// Calculate the CRC32 of the Master DF record (sans the CRC32 field itself)
		(ssp_define[ flash_index ].mr_struct_ptr)->CRC32 = CRC_calculate_32bit_big_endian( (unsigned char *)(ssp_define[ flash_index ].mr_struct_ptr), (sizeof(MASTER_DF_RECORD_s)-sizeof(CRC_BASE_TYPE)) );
		
		if( write_results == DF_RESULT_SUCCESSFUL )
		{
			// It's ready. Write out the Master_Record. After which the flash will be considered intialized!
			DF_write_page( flash_index, ssp_define[ flash_index ].master_record_page, ssp_define[ flash_index ].mr_struct_ptr, sizeof(MASTER_DF_RECORD_s) );
		}
	}
	else
	{
		#if ALERT_SHOW_ROUTINE_INITIALIZATIONS_DURING_STARTUP
			Alert_flash_file_system_passed( flash_index );
		#endif
	}

	// That's it. One way or another the Master_Record is now valid.

	DfGiveStorageMgrMutex( flash_index );
}

//////////////////////////////////////////////////////////////////////////////
//
//  unsigned long DfReadDataFromDF(DF_DIR_ENTRY_s *pDirEntry, 
//									unsigned char *pBuffer_uc)
//
//		pDirEntry : pointer to directory entry structure - filled in
//                  by a previous call to DfFindDirEntry().
//
//		pBuffer   : pointer to caller's buffer into which data will
//					be read.
//
//		Returns:	DF_RESULT_SUCCESSFUL - requested data was read OK.
//
//					DF_DIR_ENTRY_NOT_FOUND - the dir entry was not found.
//                        This can happen if the directory entry does not 
//                        exist per the key fields specified.
//
//                  DF_CRC_DOES_NOT_MATCH - The calculated CRC of the data
//                       does not match the CRC in the directory entry. No
//                       data is read into the caller's buffer.
//
//		This function reads data from the DataFlash into the caller's buffer.
//
//////////////////////////////////////////////////////////////////////////////
// The _NM means NO MUTEX activity within this function there for the caller must be responsible for the
// DATA_FLASH mutex.
//
extern UNS_32 DfReadFileFromDF_NM( const UNS_32 flash_index, const DF_DIR_ENTRY_s *pdir_entry, const UNS_8 *pwhere_to )
{
	UNS_32		cluster;

	UNS_32		rstatus_ul;

	UNS_32		ItemSize_ul;

	UNS_32		bytes_to_read;
	
	UNS_8		*read_to;

	DF_CAT_s	*pCAT;
			
	// ----------

	// NOTE: upon entry we are requiring that pfull_dir_entry is valid...containing a true and
	// accurate directory entry. And the assumption is that the caller knows what they are doing
	// in that there is ample memory available to 'read the file into'. Cause we are going to
	// charge ahead and do that. Then test the CRC.
	
	// ----------

	// CRC is okay ... so set up a pointer to the CAT of this directory entry.
	pCAT = (DF_CAT_s*)(&(pdir_entry->cat));
	
	// Set up variables for the cluster read processing loop 
	ItemSize_ul = pdir_entry->ItemSize_ul;

	rstatus_ul = DF_RESULT_SUCCESSFUL;

	cluster = 0;

	read_to = (UNS_8*)pwhere_to;

	while( (rstatus_ul == DF_RESULT_SUCCESSFUL) && (ItemSize_ul > 0))
	{
		// Figure out how much to read for this cluster
		bytes_to_read = ItemSize_ul > DF_BYTES_PER_CLUSTER ? DF_BYTES_PER_CLUSTER : ItemSize_ul;
		
		// Find first (next) avail (State sought is TRUE) cluster #
		rstatus_ul = DfFindCluster( flash_index, pCAT, TRUE, &cluster );
		
		// We have a cluster #. Read its data
		if( rstatus_ul == DF_RESULT_SUCCESSFUL )
		{
			rstatus_ul = DF_read_page( flash_index, cluster, read_to, bytes_to_read );

			if( rstatus_ul != DF_RESULT_SUCCESSFUL )
			{
				// Error, abort! abort! Should not happen unless we try to read more bytes than a cluster
				// represents or pass in an invalid cluster #!!! OR THERE IS AN IMPENDING POWER FAIL. In
				// which case do not flag an impending power fail with an alert line. Not appropriate.
				if( rstatus_ul != DF_IMPENDING_POWER_FAIL_ABORT )
				{
					Alert_Message( "DF MNGR: error reading cluster" );
				}

			}
			else
			{
				// Now update our variables
				cluster++;
				
				// Adjust the # item bytes left to read
				ItemSize_ul -= bytes_to_read;
				
				// Advance the pointer into the destination data buffer
				read_to += bytes_to_read;
			}

		}
		else
		{
			// This should not happen unless there was a problem when writing the 
			// original data item and its directory entry.
			//
			Alert_Message( "DF MNGR: error finding cluster" );
		}

	}
	
	// 11/14/2012 rmd : Now test the CRC of what we just read from the file into the memory
	// space.
	if( rstatus_ul == DF_RESULT_SUCCESSFUL )
	{
		CRC_BASE_TYPE	lcrc;
		
		lcrc = CRC_calculate_32bit_big_endian( (UNS_8*)pwhere_to, (UNS_32)pdir_entry->ItemSize_ul );
		
		// Now check if the CRCs match each other
		if( lcrc != pdir_entry->ItemCrc32_ul )
		{
			// Uh oh - they do not match
			rstatus_ul = DF_CRC_DOES_NOT_MATCH;
		}
	}

	// ----------
	
	return( rstatus_ul );
}

//////////////////////////////////////////////////////////////////
//
//	unsigned long DfGetFreeSpace(void)
//
//	This function returns the amount of free space on the DF chip,
//	expressed in bytes.
//
//////////////////////////////////////////////////////////////////
// The _NM means NO MUTEX activity within this function there for the caller must be responsible for the
// DATA_FLASH mutex.
//
unsigned long DfGetFreeSpace_NM( UNS_32 flash_index )
{
	unsigned long	size_ul;

	// Read in the MCAT page data
	// (fyi: this takes 160usec with a 12MHz spi clock, and of course would vary with the size of Master_Record) 
	if( DF_read_page( flash_index, ssp_define[ flash_index ].master_record_page, ssp_define[ flash_index ].mr_struct_ptr, sizeof(MASTER_DF_RECORD_s) ) != DF_RESULT_SUCCESSFUL )
	{
		// The only way this can happen is an impending power failure. So quit.
		size_ul = 0;  // indicate no available space
	}
	else
	{
		// First get the avail cluster count
		size_ul = DfCountAvailClusters( flash_index, &((ssp_define[ flash_index ].mr_struct_ptr)->cat) );
		
		// Then apply cluster size and DF page size factors
		size_ul = size_ul * DF_BYTES_PER_CLUSTER;
		
	}

	// Now we have the space expressed in bytes
	return( size_ul );
}

////////////////////////////////////////////////////////////////////////////////
//
//	unsigned int GetFreeDirEntry(void)
//
//	This function returns the index of the first free (unused) directory entry.
//  If no such entry is found, then the unique #defined value 
//	DF_DIR_ENTRY_NOT_FOUND is returned instead.
//
///////////////////////////////////////////////////////////////////////////////
unsigned long DfGetFreeDirEntry_NM( UNS_32 flash_index, DF_DIR_ENTRY_s *pdir_entry )
{
	unsigned long		df_page;
	
	unsigned long		rv_ul;

	rv_ul = DF_DIR_ENTRY_NOT_FOUND;

	for( df_page = ssp_define[ flash_index ].first_directory_page; df_page <= ssp_define[ flash_index ].last_directory_page; df_page++ )
	{
		// Read a directory entry. Only what we need inorder to see if it is in use or not.
		if( DF_read_page( flash_index, df_page, pdir_entry, (sizeof(DF_DIR_ENTRY_s) - sizeof(DF_CAT_s)) ) != DF_RESULT_SUCCESSFUL )
		{
			// The only way this can happen is an impending power failure. So quit.
			break;
		}
		else
		{
			// Check if it's free/avail
			if( TRUE == pdir_entry->InUse_uc ) continue;	// keep looking
			
			// We found a dir entry that's not in use so calculate its directory index.
			rv_ul = df_page - ssp_define[ flash_index ].first_directory_page;

			break;
		}
	}

	return( rv_ul );
}


//////////////////////////////////////////////////////////////////////////////
//
//	unsigned long WriteItemToDF(unsigned char *pItem_uc, 
//								unsigned long ItemSize_ul, 
//								unsigned short ItemId_us,
//								unsigned long DestMemId_ul,
//								unsigned long VersionId_ul,
//								char *pFilename_c,
//								unsigned long ItemLoadAddr_ul)
//
//	This function writes a data item to the DF system. The return status can
//  be any of:
//				DF_RESULT_SUCCESSFUL
//				DF_ZERO_ITEM_SIZE_IS_INVALID
//				DF_NO_DIR_ENTRY_AVAIL
//				DF_INSUFFICIENT_SPACE
//				DF_DUPL_ITEM_ALREADY_EXISTS
//
//////////////////////////////////////////////////////////////////////////////
// The _NM means NO MUTEX activity within this function there for the caller must be responsible for the
// DATA_FLASH mutex.
//
extern UNS_32 DfWriteFileToDF_NM( UNS_32 flash_index, DF_DIR_ENTRY_s *pdir_entry, unsigned char *pitem_buffer_ptr )
{

	unsigned long	rvstatus_ul;
	
	unsigned long	dir_index;		// index of avail dir entry
	
	unsigned long	dir_stat;		// status of dir check/compare
	
	UNS_32			cluster;
	
	unsigned long	left_to_write_ul, bytes_to_write;
	
	DF_CAT_s		*pCAT;

	DF_DIR_ENTRY_s	*lresults_dir_entry;


    // UPON ENTRY pdir_entry is space that this function may modify. But it won't do so in a harmful way. It will only complete building a valid directory
	// entry. Upon entry pfull_dir_entry should be clean and MUST contain a valid filename, version_ul, edit_count_ul, and ItemSize_ul.
	// This function will build into this memory space and ultimately write the pfull_dir_entry to the data flash.

	rvstatus_ul = DF_RESULT_SUCCESSFUL;
	
    lresults_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );

	// Check for obvious problems
	if( lresults_dir_entry == NULL )
	{
		rvstatus_ul = DF_INSUFFICIENT_SPACE;  // not the same kind of insufficient space but this will do
	}
	
	if( rvstatus_ul == DF_RESULT_SUCCESSFUL )
	{
		if( 0 == pdir_entry->ItemSize_ul )
		{
			rvstatus_ul = DF_ZERO_ITEM_SIZE_IS_INVALID;
		}
	}
	
	if( rvstatus_ul == DF_RESULT_SUCCESSFUL )
	{
		// See if a directory entry is available
		dir_index = DfGetFreeDirEntry_NM( flash_index, lresults_dir_entry );  // let it use our local dir entry memory space to do its work - will trash it but we don't care at this point
		
		if( dir_index == DF_DIR_ENTRY_NOT_FOUND )
		{
			rvstatus_ul = DF_NO_DIR_ENTRY_AVAIL;
		}
	}
	
	if( rvstatus_ul == DF_RESULT_SUCCESSFUL )
	{
		// See if there's enough space in the DF for this item
		if( pdir_entry->ItemSize_ul > DfGetFreeSpace_NM( flash_index ) )  // this makes sure the MasterClusterAllocRec_u is current as it reads it out of the data flash
		{
			rvstatus_ul = DF_INSUFFICIENT_SPACE;
		}
	}
	
	if( rvstatus_ul == DF_RESULT_SUCCESSFUL )
	{
		// In the following call DfFindDirEntry_NM will NOT disturb the memory space of the first
		// argument. And that's the one we care about as it contains our filename, version, and
		// editcount and ItemSize_ul!!!
		dir_stat = DfFindDirEntry_NM( flash_index,
									  pdir_entry, 	// entry with fields to search on
									  lresults_dir_entry,		// Filled in if found
									  DF_COMPARISON_MASK_FILENAME | DF_COMPARISON_MASK_DATE_OR_REVISION | DF_COMPARISON_MASK_EDIT_COUNT,  // fields to check
									  0);								// Starting dir entry to search
		
		if( restart_info.SHUTDOWN_impending )
		{
			rvstatus_ul = DF_IMPENDING_POWER_FAIL_ABORT; 			
		}
		else
		if( dir_stat != DF_DIR_ENTRY_NOT_FOUND )
		{
			rvstatus_ul = DF_DUPL_ITEM_ALREADY_EXISTS;
		}
	}
	
	if( rvstatus_ul == DF_RESULT_SUCCESSFUL )
	{

		// Set up a pointer to the CAT inside the directory entry we are building
		pCAT = &(pdir_entry->cat);

		// OK, let's make sure! Zero out the CAT for our directory entry.
		memset( pCAT, 0, sizeof(DF_CAT_s));
		
		// Now calculate the CRC for the item's data, store in dir entry
		pdir_entry->ItemCrc32_ul = CRC_calculate_32bit_big_endian( pitem_buffer_ptr, pdir_entry->ItemSize_ul );

		left_to_write_ul = pdir_entry->ItemSize_ul;

		cluster = 0;
		while( (rvstatus_ul == DF_RESULT_SUCCESSFUL) && (left_to_write_ul > 0) )
		{
			// Figure out how much to write for this cluster
			bytes_to_write = left_to_write_ul > DF_BYTES_PER_CLUSTER ? DF_BYTES_PER_CLUSTER : left_to_write_ul;
			
			// Find first (next) avail (State sought is FALSE) cluster #
			rvstatus_ul = DfFindCluster( flash_index, &((ssp_define[ flash_index ].mr_struct_ptr)->cat), FALSE, &cluster );
			
			if( rvstatus_ul != DF_RESULT_SUCCESSFUL )
			{
				// This should not happen because we previously checked that adequate clusters were available!
				Alert_Message(  "DF MNGR: error finding avail cluster" );
			}
			else
			{
				// We have a cluster # to use. Write its data
				rvstatus_ul = DF_write_page( flash_index, cluster, pitem_buffer_ptr, bytes_to_write );

				if( rvstatus_ul != DF_RESULT_SUCCESSFUL )
				{
					// Error, abort! abort! Should not happen unless we try to write more
					// bytes than a cluster represents or pass in an invalid cluster #!!!
					// OR THERE IS AN IMPENDING POWER FAIL. In which case do not flag
					// an impending power fail with an alert line. Not appropriate.
					if( rvstatus_ul != DF_IMPENDING_POWER_FAIL_ABORT )
					{
						Alert_Message( "DF MNGR: error during write." );
					}
				}
				else
				{
					// Good cluster data write so set this cluster's bit in the CAT of our directory entry. 
					DF_set_cluster_bit( pCAT, cluster, TRUE );

					// Now update our variables
					cluster++;
					
					// Adjust the # item bytes left to write
					left_to_write_ul -= bytes_to_write;
					
					// Advance the pointer into the source data buffer
					pitem_buffer_ptr += bytes_to_write;
				}
			}
		}
		
		////////////////////////////////////////
		// Check status before continuing..
		////////////////////////////////////////
		
		if( rvstatus_ul == DF_RESULT_SUCCESSFUL )
		{
			// We has a successful write of item data. So now:
			// a) set the InUse_uc field in the dir entry struct in RAM
			// b) Copy the dir_entry and its CAT into a DF page buffer
			// c) write the dir entry page buffer to DF
			// d) OR the dir entry CAT cluster bits into those of the MCAT
			// e) write the MCAT image back into DF
			
			// a) Set the nUse_uc field in the dir entry struct in RAM
			pdir_entry->InUse_uc = TRUE;
			
			// c) write the complete dir entry (in RAM) to DF
			rvstatus_ul = DF_write_page( flash_index, (ssp_define[ flash_index ].first_directory_page + dir_index), pdir_entry, sizeof(DF_DIR_ENTRY_s) );
			
			if( rvstatus_ul == DF_RESULT_SUCCESSFUL )
			{
				// d) Set dir entry CAT cluster bits into the MCAT in RAM
				DfOrCatBits( &((ssp_define[ flash_index ].mr_struct_ptr)->cat), pCAT );
	
				// Calculate the CRC32 of the Master DF record (sans the CRC32 field itself)
				(ssp_define[ flash_index ].mr_struct_ptr)->CRC32 = CRC_calculate_32bit_big_endian( (unsigned char *)(ssp_define[ flash_index ].mr_struct_ptr), (sizeof(MASTER_DF_RECORD_s)-sizeof(CRC_BASE_TYPE)) );
				
				// e) Now write the updated MCAT page out to DF
				rvstatus_ul = DF_write_page( flash_index, ssp_define[ flash_index ].master_record_page, (ssp_define[ flash_index ].mr_struct_ptr), sizeof(MASTER_DF_RECORD_s) );
			}
		}
	}

	mem_free( lresults_dir_entry );
	
	return( rvstatus_ul );
}

//////////////////////////////////////////////////////////////////////////////////////
//
//	void DfOrCatBits(DF_CAT_s *pDestCAT,	// ptr to destination CAT
//					 DF_CAT_s *pSrcCAT)		// ptr to source CAT
//
//	This function does a bit-wise OR of the source CAT into the destination CAT.
//	This operation is used, for example, when adding clusters for a directory entry
//  (i.e. source CAT) to the Master Cluster Allocation Table (destination CAT).
//
//////////////////////////////////////////////////////////////////////////////////////
void DfOrCatBits( DF_CAT_s *pDestCAT, DF_CAT_s *pSrcCAT )
{
	int w;
	int lim;
	
	lim = DF_CAT_32_BIT_WORDS;
	
	for (w = 0; w < lim; w++)
	{
		pDestCAT->data32[w] |= pSrcCAT->data32[w];
	}
}

//////////////////////////////////////////////////////////////////////////////////////
//
//	void DfClearCatBits(DF_CAT_s *pDestCAT,		// ptr to destination CAT 
//						DF_CAT_s *pSrcCAT)		// ptr to source CAT
//
//	This function CLEARS bits in the destination CAT that are SET in the source CAT.
//
//////////////////////////////////////////////////////////////////////////////////////
void DfClearCatBits( DF_CAT_s *pDestCAT, DF_CAT_s *pSrcCAT )
{
	int w;
	int lim;
	
	lim = DF_CAT_32_BIT_WORDS;
	
	for (w = 0; w < lim; w++)
	{
		pDestCAT->data32[w] &= (~(pSrcCAT->data32[w]));
	}
}

///////////////////////////////////////////////////////////////////////////////////////
//
//  unsigned short DirEntryCompare(DF_DIR_ENTRY_s *pDirEntry1, 
//                               DF_DIR_ENTRY_s *pDirEntry2, 
//                               unsigned short fields_to_check)
//
//	This function compares the specified key fields of two directory entries and
//  returns the result of the comparison:
//
//	0       = All of the specified key fields match
//
//  Nonzero = One or more of the specified key fields do not match. The value 
//            returned is a bitmask indicating which key fields specified to check
//            did not match.
//
//  The key fields are specified using a bitwise OR containing the masks defined
//  for the fields of interest. The bitmask can be made up of the bitwise OR of
//  one or more of these defined values:
//
//		DF_DIR_ENTRY_KEY_ITEM_ID
//	 	DF_DIR_ENTRY_KEY_ITEM_SIZE
//      DF_DIR_ENTRY_KEY_DEST_MEM_ID
//      DF_DIR_ENTRY_KEY_VERSION_ID
// 		DF_DIR_ENTRY_KEY_FILENAME
//
////////////////////////////////////////////////////////////////////////////////////////
// This one doesn't need a mutex...thus no _NM
UNS_32 DfDirEntryCompare(DF_DIR_ENTRY_s *pDirEntry1, DF_DIR_ENTRY_s *pDirEntry2, UNS_32 fields_to_check_us)
{
	UNS_32 rv;	// Bit mask of compare result for each specified key field 
	
	rv = 0;
	
	// ----------
	
	if( fields_to_check_us & DF_COMPARISON_MASK_FILENAME )
	{
		if (strncmp(pDirEntry1->Filename_c, pDirEntry2->Filename_c, sizeof(pDirEntry1->Filename_c)))
		{
			rv |= DF_COMPARISON_MASK_FILENAME;
		}
	}

	if( fields_to_check_us & DF_COMPARISON_MASK_EDIT_COUNT )
	{
		if( pDirEntry1->edit_count_ul != pDirEntry2->edit_count_ul )
		{
			rv |= DF_COMPARISON_MASK_EDIT_COUNT;
		}
	}
	
	if( fields_to_check_us & DF_COMPARISON_MASK_DATE_OR_REVISION )
	{
		if( pDirEntry1->code_image_date_or_file_contents_revision != pDirEntry2->code_image_date_or_file_contents_revision )
		{
			rv |= DF_COMPARISON_MASK_DATE_OR_REVISION;
		}
	}
	
	// ----------
	
	return( rv );
}

//////////////////////////////////////////////////////////////////////////////////////////
//
//  unsigned long DfDelItemFromDF_NM( DF_PAGE_BUFFER_UNION *pfull_dir_entry )
// 
//  The caller must know for sure that pfull_dir_entry is VALID!
// 
//
//////////////////////////////////////////////////////////////////////////////////////////
// The _NM means NO MUTEX activity within this function there for the caller must be responsible for the
// DATA_FLASH mutex.
//
void DfDelFileFromDF_NM( UNS_32 flash_index, unsigned long pdir_page, DF_DIR_ENTRY_s *pdir_entry )
{
	// pdir_page is ZERO based!
	
	DF_CAT_s	*pCAT;
	
	UNS_32		write_result;

	// USE THIS FUNCTION CAREFULLY ... there are no validity checks in the interest of efficiency ... those checks are all done in the calls leading up to the use of this function
	// pfull_dir_entry MUST be COMPLETELY valid as a full directory entry ...including the CAT!!!!!
	// The directory entry at directory page pdir_page will be deleted. The CAT bits set in the pfull_dir_entry will be reset in the MCAT.
 
	// Set a pointer to the CAT for the item (in its directory buffer)
	pCAT = &(pdir_entry->cat);

    // Clear its clusters from the Master CAT
	DfClearCatBits( &((ssp_define[ flash_index ].mr_struct_ptr)->cat), pCAT );
	
	// Then clear clusters in the dir entry CAT
	memset( pCAT, 0, sizeof(DF_CAT_s) );
	
	// Calculate the CRC32 of the Master DF record (sans the CRC32 field itself)
	(ssp_define[ flash_index ].mr_struct_ptr)->CRC32 = CRC_calculate_32bit_big_endian( (UNS_8*)(ssp_define[ flash_index ].mr_struct_ptr), (sizeof(MASTER_DF_RECORD_s) - sizeof(CRC_BASE_TYPE)) );

	// Write MCAT to DF first to free item's clusters
	write_result = DF_write_page( flash_index, ssp_define[ flash_index ].master_record_page, (ssp_define[ flash_index ].mr_struct_ptr), sizeof(MASTER_DF_RECORD_s) );

	if( write_result == DF_RESULT_SUCCESSFUL )
	{
		// Mark the directory entry as NOT in use
		pdir_entry->InUse_uc = FALSE;
		
		// Write updated (now inactive) dir entry to DF
		DF_write_page( flash_index, (ssp_define[ flash_index ].first_directory_page + pdir_page), pdir_entry, sizeof(DF_DIR_ENTRY_s) );
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  unsigned long DfFindDirEntry_NM( DF_DIR_ENTRY_s			*psearch_dir_entry, 
//                                   DF_PAGE_BUFFER_UNION	*pfound_full_dir_entry, 
//                                   unsigned short			pfields_to_check_us, 
//                                   unsigned long			starting_dir_index_ul )
// 
//  This function searches the directory entries of the Data Flash for an entry that matches 
//  pDirEntry1 fields. Only the fields specified in fields_to_check_s are compared and the directory
//  entries starting with starting_dir_index are checked. For the initial call to this function,
//  the starting_dir_index_ul value should be zero.
//
//	To find ALL directory entries, set fields_to_check_us to zero. All directory entries will
//  then be considered matches, regardless of the fields provided in *pDirEntry1. Only when all
//  directory entries have been found and returned in *pDirEntry2 will the DF_DIR_ENTRY_NOT_FOUND
//  status be returned.
//
//  If a matching entry is found, then *pDirEntry2 is populated with the information from 
//  the matching directory entry and the directory index of the matching entry is
//  the return value of the function. Subsequent calls to the function using the previous return 
//  value + 1 as the starting_dir_index_ul can be performed to find additional matching directory 
//  entries. This method could be used, for example, to find all versions of a data item having a 
//	particular filename or Item ID.
//
//  If no matching directory entry is found (or all directory items have been checked), then 
//  pDirEntry2 is not used and the function returns the unique #defined value of 
//  DF_DIR_ENTRY_NOT_FOUND. This value is an extremely large number (almost 2 ^ 32) that could 
//  not ever be an actual directory index.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////
// The _NM means NO MUTEX activity within this function there for the caller must be responsible for the
// DATA_FLASH mutex.
//
extern UNS_32 DfFindDirEntry_NM( UNS_32 flash_index, DF_DIR_ENTRY_s *psearch_dir_entry, DF_DIR_ENTRY_s *pfound_dir_entry, UNS_32 pfields_to_check_us, UNS_32 starting_dir_index_ul )
{
	// starting_dir_index_ul IS zero based

	UNS_32		rv_ul;
	
	UNS_32		DF_Page;
	
	UNS_32		compare_status;
	
	DF_DIR_ENTRY_s	*dir_buffer;
	
	// ----------
	
    // THIS FUNCTION IS NOT ALLOWED TO MODIFY THE psearch_dir_entry. USERS OF THE FUNCTION COUNT ON THAT!

	// The caller should only consider pfound_full_dir_entry valid if the rv_ul !=
	// DF_DIR_ENTRY_NOT_FOUND. ALSO NOTE that the pfound_full_dir_entry memory space will not be
	// disturbed UNLESS a matching entry is found.

	rv_ul = DF_DIR_ENTRY_NOT_FOUND;
	
	dir_buffer = mem_malloc( sizeof(DF_DIR_ENTRY_s) );

	// Check for valid starting index
	if( starting_dir_index_ul < ssp_define[ flash_index ].max_directory_entries )
	{
		// Set up to read in and check directory entries, starting with the starting_dir_index
		for( DF_Page = (ssp_define[ flash_index ].first_directory_page + starting_dir_index_ul); DF_Page <= ssp_define[ flash_index ].last_directory_page; DF_Page++ )
		{
			// Read a directory entry - including the CAT
			if( DF_read_page( flash_index, DF_Page, dir_buffer, sizeof(DF_DIR_ENTRY_s) ) != DF_RESULT_SUCCESSFUL )
			{
				// The only way this can happen is an impending power failure. So quit what we are doing.
				break;
			}
			else
			{
				// Skip over directory entries that are inactive/not in-use
				if( dir_buffer->InUse_uc == FALSE ) continue;
				
				// OK, we have an active directory entry in our buffer. So now we will compare the caller's dir info against that we just read from DF.
				compare_status = DfDirEntryCompare( psearch_dir_entry, dir_buffer, pfields_to_check_us );
				
				// If fields_to_check_us is zero, then we make every active dir entry match. This provides
				// a means to find all directory entries without knowing what they are.
				if( (pfields_to_check_us != 0) && (compare_status != 0) ) continue;	// keep checking
	
				// OKAY we've got a match. Copy the local dir buffer to the users pfound_dir_entry.
				*pfound_dir_entry = *dir_buffer;
	
				// Calculate what index this entry is
				rv_ul = (DF_Page - ssp_define[ flash_index ].first_directory_page);
	
				break;
			}
		}

	}
	else
	{
		Alert_Message( "DF Find Dir Entry: start index out of range" );
	}
	
	mem_free( dir_buffer );

	return( rv_ul );
}

//////////////////////////////////////////////////////////////////////
//
//	DF_test_cluster_bit(void *pCAT, unsigned long cluster_ul)
//
//      pCAT: 		pointer to start of cluster allocation table
//      cluster:	the cluster number to check
//
//      Returns the state (TRUE/FALSE) of a cluster in a cluster 
//      allocation table (CAT). TRUE = allocated, FALSE = free.
//
/////////////////////////////////////////////////////////////////////
unsigned char DF_test_cluster_bit(void *pCAT, unsigned long cluster_ul)
{
	// The passed in 'cluster_ul' is 0 based. 0 being the first cluster or page.

	unsigned long *pAlloc;
	unsigned long w;			// 32-bit word offset in cluster allocation map 
	unsigned long b;			// bit within a 32-bit word
	unsigned char val;			// test value
	
	pAlloc = (unsigned long *) pCAT;
	w = cluster_ul >> 5;			// compute which 32-bit word
	b = cluster_ul % 32;			// compute which bit
	
	// Test the cluster state, put result into val
	val = ((*(pAlloc + w) & (1 << b)) != 0) ? TRUE : FALSE;
	
	return (val);
}


///////////////////////////////////////////////////////////////////////////////
//
//	void DF_set_cluster_bit(void *pCAT, unsigned long cluster_ul, unsigned char state_uc)
//
//  This function sets or resets a cluster bit in a cluster allocation table 
//  (CAT) based upon the value of the state parameter as follows:
//
//  state == TRUE  : Specified cluster bit is set to one (allocated)
//  state == FALSE : Specified cluster bit is reset to zero (free) 
//
///////////////////////////////////////////////////////////////////////////////
void DF_set_cluster_bit( void *pCAT, unsigned long cluster_ul, unsigned char state_uc )
{
	unsigned long *pAlloc;
	unsigned long w;			// 32-bit word offset in cluster allocation map 
	unsigned long b;			// bit within a 32-bit word

	pAlloc = (unsigned long *) pCAT;
	w = cluster_ul >> 5;			// compute which 32-bit word
	b = cluster_ul % 32;			// compute which bit

	if( TRUE == state_uc )
	{
		// Set the cluster to 1
		(*(pAlloc + w)) |= (1 << b);		
	}
	else
	{
		// Clear the cluster to 0
		(*(pAlloc + w)) &= ~(1 << b);
	}
}

/* ---------------------------------------------------------- */
UNS_32 DF_read_page( UNS_32 flash_index, unsigned long ppage, void *pbuffer, UNS_32 pbytes_to_read )
{
	// Read from the spi flash into the callers buffer the length specified.

	DATA_HANDLE		ldh;

	UNS_32			laddr, results;

	ldh.dptr = (unsigned char*)pbuffer;
	ldh.dlen = pbytes_to_read;
	laddr = (DF_PAGE_SIZE_IN_BYTES * ppage);
	results = SPI_FLASH_fast_read_as_much_as_needed_to_buffer( flash_index, laddr, ldh );

	return( results );
}

/* ---------------------------------------------------------- */
UNS_32 DF_write_page( UNS_32 flash_index, unsigned long ppage, void *pbuffer, UNS_32 pbytes_to_write )
{
	// Write to the spi flash part the amount of data specified. Pulling of course from the pbuffer.

	DATA_HANDLE		ldh;

	UNS_32			laddr, results;

	ldh.dptr = (unsigned char*)pbuffer;
	ldh.dlen = pbytes_to_write;
	laddr = (DF_PAGE_SIZE_IN_BYTES * ppage);
	results = SPI_FLASH_erase_4k_sector_and_write_up_to_4k_using_byte_programming( flash_index, laddr, ldh );

	return( results );
}


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/5/2014 rmd : The end of compiling for all EXCEPT the TP MICRO code.
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// 4/3/2013 ajv : Function used to verify local file CRCs for comparison with
// master's CRCs.
#if 0
extern void DF_output_file_CRCs_to_alerts( const UNS_32 pflash_index )
{
	DF_DIR_ENTRY_s			*search_dir_entry, *found_dir_entry;

	UNS_32					dir_index, rstatus;

	char					str_64[ 64 ];

	dir_index = 0;

	// -------------------

	search_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );
	memset( search_dir_entry, 0, sizeof(DF_DIR_ENTRY_s) );

	found_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );
	memset( found_dir_entry, 0, sizeof(DF_DIR_ENTRY_s) );

	// -------------------

	// START of data flash operations...we don't want the data flash to change
	// until we are done doing our operations
	DfTakeStorageMgrMutex( pflash_index, portMAX_DELAY );

	// -------------------

	do
	{
		// With a fields_to_check of 0 this will return every in-use dir entry
		rstatus = DfFindDirEntry_NM( pflash_index, search_dir_entry, found_dir_entry, 0, dir_index );

		if( rstatus != DF_DIR_ENTRY_NOT_FOUND )
		{
			snprintf( str_64, sizeof(str_64), "%s CRC: %d", found_dir_entry->Filename_c, found_dir_entry->ItemCrc32_ul ); 
			Alert_Message( str_64 ); 

			// Continue the search at the next entry
			dir_index = rstatus + 1;
		}

	} while( (rstatus != DF_DIR_ENTRY_NOT_FOUND) && (rstatus != ((ssp_define[ pflash_index ].max_directory_entries) - 1)) );

	// -------------------

	// Restore the mutex.
	DfGiveStorageMgrMutex( pflash_index );

	// -------------------

	mem_free( search_dir_entry );
	mem_free( found_dir_entry );
}
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

