/*  file = cs3000_tpmicro_common.h            2012.08.16 rmd  */

/* ---------------------------------------------------------- */

#ifndef _INC_CS3KTPMCOM_H
#define _INC_CS3KTPMCOM_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	"lpc_types.h"

#include	<stdint.h>

#include	"protocolmsgs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/8/2013 rmd : Used in the CS3000 alerts functions as well as during checks made when the
// TP Micro is preparing an alert message for the main board. Maximum characters of text
// (not including the time & date) in a line - 127 characters plus a NULL.
#define ALERTS_MAX_STORAGE_OF_TEXT_CHARS		(128)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/17/2013 rmd : For DISCRETE (usually manually instigated) decoder solenoid operation.
// Not used during regular irrigation control. That is a different structure.
typedef struct
{
	// 1/29/2013 rmd : Serial Number of the decoder to operate.
	UNS_32	sn;

	// 1/29/2013 rmd : Output 0 or 1.
	UNS_32	output;

	// 1/29/2013 rmd : Set true to turn on, false to turn off.
	BOOL_32	on;
	
} TWO_WIRE_DECODER_SOLENOID_OPERATION_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// BITs To T P Micro

/* ---------------------------------------------------------- */

#define		BIT_TTPM_here_are_the_stations_to_turn_on						(0)

#define		BIT_TTPM_here_are_the_lights_to_turn_on							(1)

#define		BIT_TTPM_here_are_the_pocs_to_turn_on							(2)

// ------------

// 8/20/2012 rmd : When this bit is set within the msg to the tpmicro it is an indication to
// the tpmicro that it may now go ahead and rcv new incoming mv, pump, station, and lights
// turn on commands. I think there is no data associated with these bits. Should look at
// this bit first while parsing the incoming message.
#define		BIT_TTPM_short_ack_terminal										(3)

// ------------

#define		BIT_TTPM_isp_entry_requested									(4)

// ------------

#define		BIT_TTPM_no_longer_used_5										(5)

// ------------

#define		BIT_TTPM_request_executing_code_date_and_time					(6)

// ------------

// 1/29/2013 rmd : The bit followed by a mode word.
#define		BIT_TTPM_two_wire_control_bus_power								(7)

// 1/29/2013 rmd : Commands the discovery process to begin. As proceeding the TPMicro sends
// updates. And when complete the TP Micro sends the final results.
#define		BIT_TTPM_two_wire_perform_discovery_process						(8)


#define		BIT_TTPM_two_wire_clear_statistics_at_all_decoders				(9)

#define		BIT_TTPM_two_wire_request_statistics_from_all_decoders			(10)

#define		BIT_TTPM_two_wire_start_all_decoder_loopback_test				(11)

#define		BIT_TTPM_two_wire_stop_all_decoder_loopback_test				(12)

// ----------

// 1/29/2013 rmd : Command decoder solenoid on or off.
#define		BIT_TTPM_two_wire_decoder_solenoid_on_off						(13)

// ----------

#define		BIT_TTPM_clear_run_away_gage_state								(14)

// ----------

#define		BIT_TTPM_request_whats_installed								(15)

// ----------

#define		BIT_TTPM_here_are_the_wind_settings								(16)

// ----------

#define		BIT_TTPM_two_wire_set_serial_number								(17)

// ----------

// 8/20/2012 rmd : This ACK is used to clear the shorted 2W cable condition and to clear the
// xmission status for both the cable short and over_heated xmission state. Also upon reset
// we wipe all pending decoder commands so as to start fresh. We can be assured that the
// main board foal and irri irrigation lists have been purged of the related 2 wire stations
// (perhaps the whole system irrigation was terminated) so we won't go and try again to turn
// ON station when the short state is cleared UNTIL a station is newly added to the
// list.
//
// 11/12/2014 rmd : UPDATE - not exactly true - after the excessive current is cleared in
// the tpmicro if there is a POC decoder he re-energizes the cable and tries again to get a
// poc decoder flow meter reading. Which if the cable is truly shorted will repeat the
// cycle. Over and over again. I've decided that since we wait to clear the IRRI side cable
// excessive current flag until a station is turned ON, that I'll also wait to clear the
// TPMicro setting until that time. I'll have to make sure however that is processed in the
// tpmicro BEFORE the ON state changes are detected.
#define		BIT_TTPM_clear_two_wire_cable_excessive_current					(18)

// 10/29/2013 rmd : Just clears the xmission state and wipes any pending decoder commands.
// Does not clear the overheated state. That happens automatically when the thermistors
// indicate the temperature has fallen below the threshold.
#define		BIT_TTPM_ack_two_wire_cable_overheated							(19)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// BITs To The Main Board

/* ---------------------------------------------------------- */

#define		BIT_TTMB_PRIMARY_here_are_the_poc_flow_meter_counts			(0)
	
#define		BIT_TTMB_PRIMARY_measured_currents_for_the_stations_ON		(1)

#define		BIT_TTMB_PRIMARY_run_away_gage_state_latched				(2)

// 2/7/2013 rmd : Does not include any decoders. Only CARDS and TERMINALS.
#define		BIT_TTMB_PRIMARY_here_is_whats_installed					(3)

#define		BIT_TTMB_PRIMARY_it_is_currently_raining					(4)

#define		BIT_TTMB_PRIMARY_it_is_currently_freezing					(5)

// ----------

// 10/31/2013 rmd : Provides the current as a percentage of maximum, the excessive current
// state, and the over-heated state. Sent with each and every message to the main board.
#define		BIT_TTMB_PRIMARY_2W_cable_current_measurement				(6)

// ----------

#define		BIT_TTMB_PRIMARY_we_have_wind_speed							(7)

#define		BIT_TTMB_PRIMARY_we_had_an_et_gage_pulse					(8)

#define		BIT_TTMB_PRIMARY_we_had_a_rain_bucket_pulse					(9)

#define		BIT_TTMB_PRIMARY_fuse_is_blown								(10)


#define		BIT_TTMB_PRIMARY_here_is_a_moisture_reading					(11)


#define		BIT_TTMB_PRIMARY_here_is_the_1s_current_draw				(12)


#define		BIT_TTMB_PRIMARY_no_longer_used_13							(13)


#define		BIT_TTMB_PRIMARY_wind_requesting_settings					(14)


#define		BIT_TTMB_PRIMARY_wind_is_in_paused_state					(15)

// ----------

#define		BIT_TTMB_PRIMARY_decoders_discovered_so_far					(16)

// 10/16/2013 rmd : This indicates we've reached the end of the discovery process. And ALL
// the discovered decoders are presented along with this bit.
#define		BIT_TTMB_PRIMARY_sending_discovered_decoders				(17)

// ----------

#define		BIT_TTMB_PRIMARY_sending_decoder_statistics					(18)

// ----------

#define		BIT_TTMB_PRIMARY_terminal_short_report						(19)

// ----------

#define		BIT_TTMB_PRIMARY_two_wire_cable_excessive_current_marker	(20)

#define		BIT_TTMB_PRIMARY_two_wire_cable_overheated_marker			(21)

#define		BIT_TTMB_PRIMARY_two_wire_cable_cooled_off_marker			(22)

// ----------

#define		BIT_TTMB_PRIMARY_two_wire_decoder_output_short				(23)

#define		BIT_TTMB_PRIMARY_two_wire_decoder_output_voltage_too_low	(24)

// ----------

#define		BIT_TTMB_PRIMARY_executing_code_date_and_time				(25)

// ----------

#define		BIT_TTMB_PRIMARY_two_wire_decoder_inoperative				(26)

// ----------
// 3/8/2016 mjb : added bit 27 to send sw1 status to main

#define     BIT_TTMB_PRIMARY_status_sw1_status_in_message               (27)

// ----------
// 3/8/2016 mjb : went ahead and populated all spares

#define     BIT_TTMB_PRIMARY_spare_28                                   (28)

#define     BIT_TTMB_PRIMARY_spare_29                                   (29)

// ----------

#define		BIT_TTMB_PRIMARY_no_longer_used_30							(30)


#define		BIT_TTMB_PRIMARY_we_have_errors_to_report					(31)


// ----------
// ----------

#define		BIT_TTMB_SECONDARY_alert_message_string						(0)






/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// FOR BERMAD

// 1/14/2015 rmd : we are using a 4kHz isr to run a counter used to time stamp each pulse.
// If the delta runs beyond a count representing less than 10gpm we say the flow is 0. If
// the delta is too short we keep processing the flow but the error will rise. Take 1200gpm
// at 1 pulse per 1 gallon. Could be off by 1 out of 200 or half a percent. Which isn't bad
// at all. But we need to recognize such.

// @ 1 pulse per 1 gallon
//		@  10gpm we get one pulse every  6.00 seconds or  24000 4khz clock pulses

//		@ 100gpm we get one pulse every  0.60 seconds or   2400 4khz clock pulses

//		@ 600gpm we get one pulse every  0.10 seconds or    400 4khz clock pulses

//		@1200gpm we get one pulse every  0.05 seconds or    200 4khz clock pulses

// @ 1 pulse per 10 gallon
//		@  10gpm we get one pulse every 60.00 seconds or 240000 4khz clock pulses

//		@ 100gpm we get one pulse every  6.00 seconds or  24000 4khz clock pulses

//		@ 600gpm we get one pulse every  1.00 seconds or   4000 4khz clock pulses

//		@1200gpm we get one pulse every  0.50 seconds or   2000 4khz clock pulses


typedef struct
{
	// 1/14/2015 rmd : To work with the Bermad type flow meters we need to know the time between
	// the last two pulses. And the number of pulses accumulated since last sent to main board.
	// The time between the pulses is used to derive the flow rate. And the accumulated number
	// of pulses is used to feed the accumulators.
	
	// 5/22/2014 rmd : If this is zero that implies the terminal poc.
	UNS_32	serial_number;
	
	UNS_32	fm_accumulated_pulses;
	
	UNS_32	fm_accumulated_ms;
	
	UNS_32	fm_latest_5_second_count;
	
	// 1/14/2015 rmd : Number of 4kHz ticks between the last two pulses. This is active for all
	// flow meter types. The TPMicro does not know which flow meter has been choosen by the user
	// nor what is installed.
	UNS_32	fm_delta_between_last_two_fm_pulses_4khz;
	
	// 1/13/2015 rmd : This is used to deduce that the flow rate has gone below a minimum agreed
	// upon value. For example for the 1 pulse per 10 gallons transducer if this value is
	// greater than 60 seconds that means the flow rate is less than 10 gpm. And the main board
	// then can make the decision to ignore the delta and call the flow rate 0.
	UNS_32	fm_seconds_since_last_pulse;
	
} FLOW_COUNT_XFER_FROM_THE_TPMICRO_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*
	@DESCRIPTION - TPMicro error bits 
*/

// 8/15/2012 raf :  Set when constructing messages in internal comms task if the message length exceeds our max transfer size
// (TP_MICRO_MAX_PAYLOAD_SIZE_BEFORE_CRC bytes)
#define		BIT_TPME_constructed_message_length_too_large		(0)

#define		BIT_TPME_not_used_1									(1)

#define		BIT_TPME_not_used_2									(2)

#define		BIT_TPME_two_wire_bus_mode_error					(3)

#define		BIT_TPME_not_used_4									(4)


#define		BIT_TPME_not_used_5									(5)

// 8/15/2012 raf :  Set if the header info didnt match what we expected. E.g. a mismatched routing ID
#define		BIT_TPME_message_fom_main_contained_invalid_routing	(6)



// 8/15/2012 raf : Set if our main stack gets close to our current stack size. Checked in
// the HTBT task.
#define		BIT_TPME_main_stack_is_in_danger_of_overflow		(8)


#define		BIT_TPME_memory_pool_empty							(9)


#define		BIT_TPME_memory_partition_empty						(10)


#define		BIT_TPME_queue_full									(11)


// 8/15/2012 raf :  Set if the RX packethunter looped around before we got to process the data in it
#define		BIT_TPME_main_rx_tail_caught_index					(12)

// 8/15/2012 raf :  Set if the RX packethunter looped around before we got to process the data in it
#define		BIT_TPME_M1_rx_tail_caught_index					(13)

#define		BIT_TPME_not_used_14								(14)



// 8/15/2012 raf :  Set when we have DMA transfer errors during UART transmission
#define		BIT_TPME_DMA_error_on_uarts							(17)

// 8/15/2012 raf :  Set if we detected serial errors on our incoming UART transmissions.
#define		BIT_TPME_serial_transmission_had_serial_errors		(18)

// 9/18/2012 raf :  Set if our eeprom mirror didnt match our critical section, this means we reset our eeprom back to default safe values
#define		BIT_TPME_eeprom_mirror_didnt_match					(19)

// 8/15/2012 raf :  Set if main instructed us to do something that didnt make sense E.g. too many stations on, nonexisiting stations, etc
#define		BIT_TPME_the_message_from_main_was_invalid			(20)

// 8/15/2012 raf :  Set if the packet router detects an incorrect CRC, we abandon the message if so
#define		BIT_TPME_the_message_from_M1_failed_crc				(21)

// 8/15/2012 raf :  Set if the packet router detects an incorrect CRC, we abandon the message if so
#define		BIT_TPME_the_message_from_M2_failed_crc				(22)

// 8/15/2012 raf :  Set if we didnt report our current results to main before recieveing new instructions
#define		BIT_TPME_we_didnt_finish_last_irri_from_main		(23)

// 8/15/2012 raf :  Set if the triac states dont match on our i2c bus.  We set the states and then read them back, so they should always match
#define		BIT_TPME_the_i2c_states_dont_match_what_we_sent		(24)

#define		BIT_TPME_not_used_25								(25)



// 4/18/2012 rmd : TPME (TP Micro Error)
typedef struct
{

	UNS_32		errorBitField;
	
} ERROR_LOG_s;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/24/2013 rmd : A short occured. The hunt proceeded normally. And a suspect output has
// been located.
#define		SHORT_OR_NO_CURRENT_RESULT_short_and_found										(1)

// 9/23/2013 rmd : This report result is feed to the main board AFTER several unsuccessful
// hunts have been tried. Meaning with stations ON we get a short. But we run through the
// hunt process without finding the output. This can occur when the combined currents push
// us over the short threshold BUT individually each output is not enough on it own to trip
// the short. I'm sure this will occur from time to time.
#define		SHORT_OR_NO_CURRENT_RESULT_short_but_hunt_was_unsuccessful						(2)

// 9/24/2013 rmd : An output was turned ON and no discernable increase in the current was
// measured. This result os sent to the MASTER in the next token response. But no
// confirmation is needed so as soon as the token response is sent the state machine moves
// ahead to allow the next output to turn ON.
#define		SHORT_OR_NO_CURRENT_RESULT_no_current											(5)


typedef struct
{
	UNS_32		result;

	// 9/16/2013 rmd : When the hunt was successful, this takes on the PERIPHERAL_TYPE define
	// value of station, light, MV, or PUMP. Also for the case of NO_CURRENT for the this
	// variable is in use.
	UNS_32		terminal_type;
	
	// 9/16/2013 rmd : When the hunt was successful, this takes on the station or light number
	// if it is one of those output types. Also for the case of NO_CURRENT for the this
	// variable is in use.
	UNS_32		station_or_light_number_0;
	
} TERMINAL_SHORT_OR_NO_CURRENT_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 11/11/2014 rmd : The only item that may still be functional is the percentage of current.
	// The overheated and excessive current have been implemented differently using IRRI_COMM
	// variables. This strucutre is so far gathered at the master but not yet distributed out to
	// all the slaves.

	UNS_32		current_percentage_of_max;
	
	//BOOL_32		excessive_current;
	
	//BOOL_32		over_heated;

} TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/24/2014 rmd : We depend on this value to be zero. That is what the array is
// initialized to implictly setting the fault_type_code to this value.
#define DECODER_FAULT_CODE_not_in_use					(0)

// 10/24/2014 rmd : This is used for both a station decoder and a poc decoder.
#define DECODER_FAULT_CODE_solenoid_output_short		(1)

#define DECODER_FAULT_CODE_solenoid_output_no_current	(2)

// 10/24/2014 rmd : This is during a solenoid turn ON attempt. The solenoid channel caps
// will not pump up to the required minimum voltage.
#define DECODER_FAULT_CODE_low_voltage_detected			(3)

// 10/24/2014 rmd : This is used for both a station decoder and a poc decoder.
#define DECODER_FAULT_CODE_inoperative_decoder			(4)


// ----------

// 11/7/2013 ajv : Although the TP Micro only sends a single station when sending Two-Wire
// stations or decoders with problems, it does so asynchronously to the FLOWSENSE token.
// Meaning we don't know how many records will arrive before we are building our next token
// response. Therefore, we need to be prepared to store multiple records. I've decided 8
// ought to be enough. It is rather time consuming for a decoder to be determined
// inoperative. We could have 6 stations on at once. All with shorts on their solenoid
// outputs. So that's 6. Ahh we'll make it 8. Also note - this size is also used when
// defining the foal_irri copies of these structures and therefore affects battery-backed
// SRAM consumption.
#define TWO_WIRE_DECODER_FAULTS_TO_BUFFER										(8)

// 11/17/2014 ajv : Since protocolmsgs is part of the decoder source code and
// not common includes, we don't have access to it when compiling the ParserDLL.
// Therefore, block it from compiling in in that case.
#ifndef _MSC_VER

/**
 * This shared structure is used transfer decoder fault information from the tpmicro to the 
 * main board irri side. And then again on to the foal master - which of course may be 
 * physically remote to the originating tpmicro. It stops there.
 *
 * @author 10/23/2014 rmd
 */
struct DECODER_FAULT_BASE_STRUCT
{
	// 10/23/2014 rmd : The type of fault.
	UNS_32			fault_type_code;

	// 10/23/2014 rmd : Included so we know the decoder type.
	ID_REQ_RESP_s	id_info;
	
	UNS_32			decoder_sn;
	
	// 10/23/2014 rmd : Only used for the case of a shorted solenoid output. Probably also for
	// no current reporting too. The inoperative decoder figures out which stations to remove
	// from the list at the master foal side.
	UNS_32			afflicted_output;
};

typedef struct DECODER_FAULT_BASE_STRUCT DECODER_FAULT_BASE_TYPE;

typedef struct DECODER_FAULT_BASE_STRUCT DECODER_FAULTS_ARRAY_TYPE[ TWO_WIRE_DECODER_FAULTS_TO_BUFFER ];

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION - Output types used in the short report. And used when reporting the
    individual currents seen as each output is turned ON. Shared definition between the
    TPMicro and the Main Board.
*/

#define	OUTPUT_TYPE_TERMINAL_STATION			( 0 )
#define	OUTPUT_TYPE_TERMINAL_LIGHT				( 1 )
#define	OUTPUT_TYPE_TERMINAL_MASTER_VALVE		( 2 )
#define	OUTPUT_TYPE_TERMINAL_PUMP				( 3 )

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define STATION_CARD_COUNT					(6)

#define STATIONS_PER_CARD					(8)

#define MAX_CONVENTIONAL_STATIONS_PER_BOX	(STATION_CARD_COUNT * STATIONS_PER_CARD)

#define MAX_LIGHTS_OUTPUTS_PER_BOX			(4)

// ----------

typedef struct
{
	union
	{
		UNS_32	sizer;  // to force the union to 4 bytes

		struct
		{
			BITFIELD_BOOL	card_present		: 1;   // 1 = card is present in system

			BITFIELD_BOOL	tb_present			: 1;   // 1 = associated Terminal Block is present
		};

	};

} I2C_CARD_DETAILS_STRUCT;


typedef struct
{
	// 2/8/2013 rmd : The I2C based cards.

	I2C_CARD_DETAILS_STRUCT		stations[ STATION_CARD_COUNT ];  // for the terminal stations of course

	I2C_CARD_DETAILS_STRUCT		poc;  // for the terminal poc of course

	I2C_CARD_DETAILS_STRUCT		lights;

	// ----------

	// 2/8/2013 rmd : And these signals are not I2C based. They are direct gpio reads.

	BOOL_32						weather_card_present;
	BOOL_32						weather_terminal_present;

	BOOL_32						dash_m_card_present;
	BOOL_32						dash_m_terminal_present;
	BOOL_32						dash_m_card_type;

	BOOL_32						two_wire_terminal_present;

} WHATS_INSTALLED_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 2/6/2013 rmd : These 4 are delivered to the tp micro from the main board. And the tp
	// micro uses them to manage declaration of wind pause and resume states.

	// 7/26/2012 rmd : When the wind speed is ABOVE the pause mph irrigation is paused. We use
	// the average wind speed over the pause minutes period.
	UNS_32	wind_pause_mph;

	// 7/26/2012 rmd : This is not editable by the user. It is set in the set_default_values
	// function. Being fixed values we almost don't need them but I have kept them for
	// continuity sakes between previous generation product. They default to 10 minutes.
	UNS_32	wind_pause_minutes;
	
	// 7/26/2012 rmd : When the wind speed is BELOW the resume mph irrigation will resume. We
	// use the average wind speed over the resume minutes period.
	UNS_32	wind_resume_mph;

	// 7/26/2012 rmd : This is not editable by the user. It is set in the set_default_values
	// function. Being fixed values we almost don't need them but I have kept them for
	// continuity sakes between previous generation product. They default to 10 minutes.
	UNS_32	wind_resume_minutes;
	
} WIND_SETTINGS_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/10/2013 rmd : Used when we send to the TP Micro the stations that are supposed to be
// ON. This can include both terminal and decoder based stations. So we need more than just
// the station number.
typedef struct
{
	// 9/23/2015 rmd : For stations this is 0..47. For lights this is 0..3.
	UNS_32	terminal_number_0;
	
	// 5/15/2014 rmd : The default decoder serial number value indicates a terminal based
	// station.
	UNS_32	decoder_serial_number;
	
	// 5/15/2014 rmd : Which output? Black or Orange.
	UNS_32	decoder_output;
	
} STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT;

// ----------

#define		POC_CONTENT_TO_TP_MICRO_energize_mv		(0)

#define		POC_CONTENT_TO_TP_MICRO_energize_pump	(1)

// 5/15/2014 rmd : And the equivalent POC record used to transfer which pocs are to be ON
// from the main board to the tpmicro.
typedef struct
{
	// 5/15/2014 rmd : If the decoder serial number is the default serial number value that
	// means it is the terminal based POC.
	UNS_32	decoder_serial_number;
	
	UNS_8	pump_and_mv_control;
	
	// 5/16/2014 rmd : It will be 8 bytes unless packed. Not packing, so 3 unused bytes.
} POC_ON_FOR_TPMICRO_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 1/26/2016 rmd : The moisture reading scheme is not quite known at this time. However as
// with flow I will give it my best guess. I think reading once every 10 minutes gives us
// plenty of data points. We surely could go to 4 times and hour but from an engineering
// point of view I'll go with a bit more data - 6 times and hours.
//
// 2/16/2016 rmd : NOTE - as you change this beaware of the impact to the stored records up
// at the master. If we want to hold 4 hours worth and we theorize 24 sensors in the
// network, that storage needs to hold 576 records.
//
// 2/22/2016 rmd : NOTE - do not set this to less than 75 seconds!! There is initialization
// code at the beginning of the two_wire_task that may be affected.
//DECODER_MS_BETWEEN_MOISTURE_READING_COMMANDS (10*60*1000) 
#define		TPMICRO_MS_BETWEEN_GETTING_MOISTURE_READING_FROM_DECODERS	(2*60*1000)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*
  ----------------------------------------------------------
  Flow Data Packet Format
  ----------------------------------------------------------
	| # of POC's
	|
	| |----------------------
	| |type of POC
	| |
	| | |--------------------
	| |	| (potential) decoder serial number
	| | |
	| | | |------------------
	| | | | # of readings
	| | | |
	| | | | (reading)
	| | | | ...
	| | | | ...
	| | | | ...
	| | | | ...
	| | | | (reading)
	| | | |
	| | | | # of readings
	| | | |
	| | | | (reading)
	| | | | ...
	| | | | ...
	| | | | ...
	| | | | ...
	| | | | (reading)
	| | | |
	| | | | # of readings
	| | | |
	| | | | (reading)
	| | | | ...
	| | | | ...
	| | | | ...
	| | | | ...
	| | | | (reading)
	| | | |------------------
	| | |
	| | |--------------------
	| |
	| |----------------------
	|
	-------------------------
*/


/*
  ----------------------------------------------------------
  Current Data Packet Format
  ----------------------------------------------------------
	| # of Device Types to report						//U8
	|
	| |----------------------
	| | type of Device  								//U8
	| |
	| | |------------------
	| | | # of readings for device  					//U8
	| | |
	| | | |------------------
	| | | | index of device (optional)					//U8
	| | | |
	| | | | (reading)   								//U32
	| | | | ...
	| | | | ...
	| | | | ...
	| | | | index of device (optional)					//U8
	| | | |
	| | | | (reading)   								//U32
	| | | | ...
	| | | | ...
	| | | | ...
	| | | | index of device (optional)					//U8
	| | | |
	| | | | (reading)   								//U32
	| | | |------------------
	| | |
	| | |--------------------
    | |
	| |----------------------
	|
	-------------------------
*/


/*
  ----------------------------------------------------------
  Short Report format within msg rcvd from TPMicro
  ----------------------------------------------------------

  Type of short event									//U8	1=normal 2=couldnt find shorts 3=nothing was on when short occured

	| # of devices shorted								//U8
	|
	| |----------------------
	| | type of Device									//U8
	| |
	| | index of device 								//U8
	| | ...
	| | ...
	| | ...
	| | type of Device									//U8
	| |
	| | index of device 								//U8
	| | ...
	| | ...
	| | ...
	| | type of Device									//U8
	| |
	| | index of device 								//U8
	| |----------------------
	|
	-------------------------
*/

/*
  ----------------------------------------------------------
  FORMAT OF POC STATE SETTINGS PIECE
  ----------------------------------------------------------
	| # of POCs 											//U8
	|
	| |----------------------
	| |type of POC  										//U8
	| |
	| | |--------------------
	| |	| (potential) decoder serial number 				//U32
	| | |
	| | | |------------------
	| | | | content byte:   								//U8
	| | | | 	bit 0 => MV1 Engergized
	| | | | 	bit 1 => Pump1 Engergized
	| | | | 	bit 2 => MV2 Engergized
	| | | | 	bit 3 => Pump2 Engergized
	| | | | 	bit 4 => MV3 Engergized
	| | | | 	bit 5 => Pump3 Engergized
	| | | | 	bit 6 => Unused
	| | | | 	bit 7 => Unused
	| | | |------------------
	| | |
	| | |--------------------
	| |
	| |----------------------
	|
	-------------------------
*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/7/2014 rmd : By agreement with David Byma and myself the maximum decoders on the
// two-wire cable is 80. This fufills the 128 stations per controller requirement when
// considering the use of 2-station decoders. Also cable load testing was done to a maximum
// of 80 decoders on the cable at 7000 feet with 6 stations ON at the end of the cable. More
// decoders on the cable would nullify this test.
//
// 11/4/2014 rmd : This number used to be 128. Then per the note above was lowered to 80.
// Now it seems that's not the way we want the spec to read. The powers at be want the spec
// to say 70 2-station decoders plus whatever poc and other decoders you need. I don't
// particularly like that as there is no hard upper limit on the decoder count. We shall
// see.
#define MAX_DECODERS_IN_A_BOX	(80)


// 10/1/2015 rmd : A hardware restriction.
#define MAX_LIGHTS_IN_A_BOX		(4)


// 2012.01.04 rmd : I proposed this number and David Byma and I agreed it was plenty.
// 4/11/2012 rmd : Upon further consideration AJ and I realized that the default state of
// any controller should be to contain it's resident terminal POC as valid. Therefore a
// chain of 12 controllers all with resident terminal poc cards plugged in will have 12
// poc's.
#define MAX_POCS_IN_NETWORK		(12)

// ----------

// 7/7/2015 mpd : 12 Controllers with 4 LIGHTS each.
#define MAX_LIGHTS_IN_NETWORK	( MAX_CHAIN_LENGTH * MAX_LIGHTS_IN_A_BOX )

// ----------

// 5/16/2013 rmd : We're allowed 6 conventional station on at a time. This number is
// supposed to include the PUMP and MV.
#define MAX_NUMBER_OF_TERMINAL_BASED_OUTPUTS_ON_AT_A_TIME		(6)

// 5/16/2013 rmd : The two-wire spec calls out 6 stations ON at a time. Not much more to say
// about that.
#define MAX_NUMBER_OF_DECODER_BASED_OUTPUTS_ON_AT_A_TIME		(6)

// 5/15/2014 rmd : I suppose all 12 of the allowed POC's could be active at the same time.
// However we have a problem if they are all on the decoder cable. As we can only have 6 ON
// at a time there. So I'm going to limit the number of pocs ON to the cable limit. That
// should cover 99.9% of the cases. This number limits the message size to the tpmicro board
// and includes both TERMINAL and DECODER pocs. And as a matter of fact a BYPASS counts as
// THREE.
#define MAX_NUMBER_OF_POCS_ON_AT_A_TIME		(6)


#define MAX_NUMBER_OF_LIGHTS_ON_IN_A_BOX	(MAX_LIGHTS_IN_A_BOX)



// 5/16/2014 rmd : We do have something to work out. Right now we are counting poc outputs
// ON separate from station outputs ON. And that's wrong. The sum total must not exceed the
// limits. TODO!!!!!!!!!!!!!!!!!

// 5/16/2014 rmd : What can the xfmr handle. I don't really know. This needs testing! I'm
// just gonna say 9.
#define MAX_NUMBER_OF_TERMINAL_AND_DECODER_BASED_OUTPUTS_ON_AT_A_TIME		(9)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	DECODER_SERIAL_NUM_MIN		(0)

// 2012.01.24 rmd : The decoder serial number field is actually 24 bits. However
// I picked a maximum serial number using 21 bits. Possibly reserving high order bit serial
// numbers for a second generation decoder?
#define	DECODER_SERIAL_NUM_MAX		(0x1FFFFF)

// 2012.01.24 rmd : This default value is used when the value pulled from the
// list item is out of range. It is also the default value assigned to a new poc
// list item when it is first added to the list. With regards to the
// poc_preserves the decoder serial number is a key field and is always used
// when looking for a poc_preserves that is REGISTERED (sync'd) to an actual
// POC. I mean the decoder ser num is always used, even though the
// POC_TYPE_TERMINAL poc technically doesn't use a decoder serial number. And it
// is important that the decoder serial number is 0 for non-decoder poc's. The
// messages from the tp micro count on that.
#define	DECODER_SERIAL_NUM_DEFAULT	(DECODER_SERIAL_NUM_MIN)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	DECODER_OUTPUT_A_BLACK			(0)
#define	DECODER_OUTPUT_B_ORANGE			(1)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


// 4/30/2013 rmd : Watch the size. We use 16 bit counters to manage BSS sram consumption.
typedef struct
{
	UNS_16		unicast_msgs_sent;

	UNS_16		unicast_no_replies;

	UNS_16		unicast_response_crc_errs;

	UNS_16		unicast_response_length_errs;

	UNS_16		loop_data_bytes_sent;

	UNS_16		loop_data_bytes_recd;

} TWO_WIRE_COMM_STATS_PER_DECODER_STRUCT;

// ----------

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

