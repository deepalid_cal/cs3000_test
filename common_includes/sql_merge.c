/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 1/19/2015 ajv : Required for strchr
#include	<string.h>

#include	"sql_merge.h"

#include	"cs3000_comm_server_common.h"

// 10/7/2015 ajv : Required for NUMBER_OF_CHARS_IN_A_NAME
#include	"cal_string.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static char* make_string_SQL_ready( const char *const orig_string, UNS_32 orig_string_length, char *const new_string, const UNS_32 new_string_length )
{
	UNS_32 counter;

	// ----------

	memset( new_string, 0x00, new_string_length );

	// ----------

	const char *o_str = orig_string;
	char *n_str = new_string;

	counter = 0;

	// ----------

	// loop through the original string and look for single quote's
	while( counter < orig_string_length )
	{
		if( *o_str == '\'' )
		{
			// add single quote and increase the new_str pointer
			*n_str = '\'';
			n_str++;
		}

		// keep copying the original string into the new string
		*n_str = *o_str;
		n_str++;
		o_str++;
		counter++;
	}

	return( new_string );
}

/* ---------------------------------------------------------- */
static char* make_date_SQL_ready( const UNS_32 pnew_value, char *pdate_string, const UNS_32 pstr_len )
{
	UNS_32	lday, lmonth, lyear, ldow;

	memset( pdate_string, 0x00, pstr_len );

	// 10/7/2015 ajv : If the date is not set, which means it's still initialized to 0, don't
	// run through the logic to build a date because it will end up with an invalid date string
	// of 2941644-11-25.
	if( pnew_value == 0 )
	{
		lmonth = 1;
		lday = 1;
		lyear = 1900;
	}
	else
	{
		DateToDMY( pnew_value, &lday, &lmonth, &lyear, &ldow );
	}

	// 12/2/2014 ajv : Format the date into a valid SQL Date type (YYYY-MM-DD)
	snprintf( pdate_string, pstr_len, "%04u-%02u-%02u", lyear, lmonth, lday );

	return( pdate_string );
}

/* ---------------------------------------------------------- */
static char* make_time_SQL_ready( const UNS_32 pnew_value, char *ptime_str, const UNS_32 pstr_len )
{
	UNS_32	lhour, lmin, lsec;

	memset( ptime_str, 0x00, pstr_len );

	// 10/7/2015 ajv : If the time is not set, which means it's still initialized to 0, don't
	// run through the logic to build a time because it will end up with an invalid time string.
	//
	// 8/23/2016 ajv : Wissam indicated that, like the CS3000, the ET2000e uses 86400 for off.
	// However, unliked the CS3000, the ET2000e stores this a a TIME field rather than an
	// interger. This results in trying to insert 24:00:00 into the table, which isn't valid.
	// Therefore, at Wissam's request, if the passed in time is 86400, reset it to 0. Just make
	// sure that there's an associated "Enabled" flag for each time field and that the SQL used
	// to build the data to send back handles this properly because we don't want to
	// accidentally send back a 0 instead of 86400.
	if( (pnew_value == 0) || (pnew_value == 86400) )
	{
		lhour = 0;
		lmin = 0;
		lsec = 0;
	}
	else
	{
		TimeToHMS( pnew_value, &lhour, &lmin, &lsec );
	}

	// 12/2/2014 ajv : Format the date into a valid SQL Time type (HH:MM:SS)
	snprintf( ptime_str, pstr_len, "%02u:%02u:%02u", lhour, lmin, lsec );

	return( ptime_str );
}

/* ---------------------------------------------------------- */
char* make_timestamp_SQL_ready( const DATE_TIME pnew_value, char *ptimestamp_str, const UNS_32 pstr_len )
{
	UNS_32	lday, lmonth, lyear, ldow, lhour, lmin, lsec;

	memset( ptimestamp_str, 0x00, pstr_len );

	// 10/7/2015 ajv : If the date is not set, which means it's still initialized to 0, don't
	// run through the logic to build a date and time because it will end up with an invalid
	// date string of 2941644-11-25.
	if( pnew_value.D == 0 )
	{
		lmonth = 1;
		lday = 1;
		lyear = 1900;

		lhour = 0;
		lmin = 0;
		lsec = 0;
	}
	else
	{
		DateToDMY( pnew_value.D, &lday, &lmonth, &lyear, &ldow );
		TimeToHMS( pnew_value.T, &lhour, &lmin, &lsec );
	}

	// 12/2/2014 ajv : Format the date into a valid SQL Timestamp type (YYYY-MM-DD HH:MM:SS:mmm)
	snprintf( ptimestamp_str, pstr_len, "%04u-%02u-%02u %02u:%02u:%02u.000", lyear, lmonth, lday, lhour, lmin, lsec );

	return( ptimestamp_str );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void build_merge_search_string( const char *pfield_name, const char *pstr, char *psearch_condition )
{
	if( psearch_condition[ 0 ] == 0 )
	{
		snprintf( psearch_condition, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s='%s'", pfield_name, pstr );
	}
	else
	{
		sp_strlcat( psearch_condition, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, " AND %s='%s'", pfield_name, pstr );
	}
}

/* ---------------------------------------------------------- */
static void build_merge_search_sql_cursor_var( const char *pfield_name, const char *pstr, char *psearch_condition )
{
	if( psearch_condition[ 0 ] == 0 )
	{
		snprintf( psearch_condition, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s=%s", pfield_name, pstr );
	}
	else
	{
		sp_strlcat( psearch_condition, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, " AND %s=%s", pfield_name, pstr );
	}
}

/* ---------------------------------------------------------- */
static void build_merge_insert_fields( const char *pfield_name, char *pinsert_fields_str )
{
	// 10/7/2015 ajv : For the case of reports, we're doing a blind INSERT which doesn't include
	// the field names. The intent behind this is to reduce the size of the MERGE statements.
	// Therefore, before we do anything, make sure the field name is valid because we'll pass in
	// a NULL if it's unused.
	if( pfield_name != NULL )
	{
		if( pinsert_fields_str[ 0 ] == 0 )
		{
			snprintf( pinsert_fields_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s", pfield_name );
		}
		else
		{
			sp_strlcat( pinsert_fields_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, ", %s", pfield_name );
		}
	}
}

/* ---------------------------------------------------------- */
static void build_merge_update_bool( const char *pfield_name, const BOOL_32 pnew_value, char *pupdate_str )
{
	// 10/7/2015 ajv : For the case of reports, we don't want to perform an UPSERT, instead we
	// just want to INSERT the data if it's not already in the database. In this case, we pass
	// in a pfield_name pointer of NULL so make sure it's valid before we try to use it.
	if( pfield_name != NULL )
	{
		if( pupdate_str[ 0 ] == 0 )
		{
			snprintf( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s=%u", pfield_name, pnew_value );
		}
		else
		{
			sp_strlcat( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, ", %s=%u", pfield_name, pnew_value );
		}
	}
}

/* ---------------------------------------------------------- */
static void build_merge_insert_values_bool( const BOOL_32 pnew_value, char *pinsert_values_str )
{
	if( pinsert_values_str[ 0 ] == 0 )
	{
		snprintf( pinsert_values_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%u", pnew_value );
	}
	else
	{
		sp_strlcat( pinsert_values_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, ", %u", pnew_value );
	}
}

/* ---------------------------------------------------------- */
static void build_merge_update_char( const char *pfield_name, const char pnew_value, char *pupdate_str )
{
	// 10/7/2015 ajv : For the case of reports, we don't want to perform an UPSERT, instead we
	// just want to INSERT the data if it's not already in the database. In this case, we pass
	// in a pfield_name pointer of NULL so make sure it's valid before we try to use it.
	if( pfield_name != NULL )
	{
		if( pupdate_str[ 0 ] == 0 )
		{
			snprintf( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s='%c'", pfield_name, pnew_value );
		}
		else
		{
			sp_strlcat( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, ", %s='%c'", pfield_name, pnew_value );
		}
	}
}

/* ---------------------------------------------------------- */
static void build_merge_insert_values_char( const BOOL_32 pnew_value, char *pinsert_values_str )
{
	if( pinsert_values_str[ 0 ] == 0 )
	{
		snprintf( pinsert_values_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "'%c'", pnew_value );
	}
	else
	{
		sp_strlcat( pinsert_values_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, ", '%c'", pnew_value );
	}
}

/* ---------------------------------------------------------- */
static void build_merge_update_uint32( const char *pfield_name, const UNS_32 pnew_value, char *pupdate_str )
{
	if( pupdate_str[ 0 ] == 0 )
	{
		snprintf( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s=%u", pfield_name, pnew_value );
	}
	else
	{
		sp_strlcat( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, ", %s=%u", pfield_name, pnew_value );
	}
}

/* ---------------------------------------------------------- */
static void build_merge_insert_values_uint32( const UNS_32 pnew_value, char *pinsert_values_str )
{
	if( pinsert_values_str[ 0 ] == 0 )
	{
		snprintf( pinsert_values_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%u", pnew_value );
	}
	else
	{
		sp_strlcat( pinsert_values_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, ", %u", pnew_value );
	}
}

/* ---------------------------------------------------------- */
static void build_merge_update_int32( const char *pfield_name, const INT_32 pnew_value, char *pupdate_str )
{
	// 10/7/2015 ajv : For the case of reports, we don't want to perform an UPSERT, instead we
	// just want to INSERT the data if it's not already in the database. In this case, we pass
	// in a pfield_name pointer of NULL so make sure it's valid before we try to use it.
	if( pfield_name != NULL )
	{
		if( pupdate_str[ 0 ] == 0 )
		{
			snprintf( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s=%d", pfield_name, pnew_value );
		}
		else
		{
			sp_strlcat( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, ", %s=%d", pfield_name, pnew_value );
		}
	}
}

/* ---------------------------------------------------------- */
static void build_merge_insert_values_int32( const INT_32 pnew_value, char *pinsert_values_str )
{
	if( pinsert_values_str[ 0 ] == 0 )
	{
		snprintf( pinsert_values_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%d", pnew_value );
	}
	else
	{
		sp_strlcat( pinsert_values_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, ", %d", pnew_value );
	}
}

/* ---------------------------------------------------------- */
static void build_merge_update_float( const char *pfield_name, const float pnew_value, char *pupdate_str )
{
	// 10/7/2015 ajv : For the case of reports, we don't want to perform an UPSERT, instead we
	// just want to INSERT the data if it's not already in the database. In this case, we pass
	// in a pfield_name pointer of NULL so make sure it's valid before we try to use it.
	if( pfield_name != NULL )
	{
		if( pupdate_str[ 0 ] == 0 )
		{
			snprintf( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s=%f", pfield_name, pnew_value );
		}
		else
		{
			sp_strlcat( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, ", %s=%f", pfield_name, pnew_value );
		}
	}
}

/* ---------------------------------------------------------- */
static void build_merge_insert_values_float( const float pnew_value, char *pinsert_values_str )
{
	if( pinsert_values_str[ 0 ] == 0 )
	{
		snprintf( pinsert_values_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%f", pnew_value );
	}
	else
	{
		sp_strlcat( pinsert_values_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, ", %f", pnew_value );
	}
}

/* ---------------------------------------------------------- */
static void build_merge_update_double( const char *pfield_name, const double pnew_value, char *pupdate_str )
{
	// 10/7/2015 ajv : For the case of reports, we don't want to perform an UPSERT, instead we
	// just want to INSERT the data if it's not already in the database. In this case, we pass
	// in a pfield_name pointer of NULL so make sure it's valid before we try to use it.
	if( pfield_name != NULL )
	{
		if( pupdate_str[ 0 ] == 0 )
		{
			snprintf( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s=%f", pfield_name, pnew_value );
		}
		else
		{
			sp_strlcat( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, ", %s=%f", pfield_name, pnew_value );
		}
	}
}

/* ---------------------------------------------------------- */
static void build_merge_insert_values_double( const double pnew_value, char *pinsert_values_str )
{
	if( pinsert_values_str[ 0 ] == 0 )
	{
		snprintf( pinsert_values_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%f", pnew_value );
	}
	else
	{
		sp_strlcat( pinsert_values_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, ", %f", pnew_value );
	}
}

/* ---------------------------------------------------------- */
static void build_merge_update_string( const char *pfield_name, const char *pnew_value, char *pupdate_str )
{
	// 10/7/2015 ajv : For the case of reports, we don't want to perform an UPSERT, instead we
	// just want to INSERT the data if it's not already in the database. In this case, we pass
	// in a pfield_name pointer of NULL so make sure it's valid before we try to use it.
	if( pfield_name != NULL )
	{
		if( pupdate_str[ 0 ] == 0 )
		{
			snprintf( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s='%s'", pfield_name, pnew_value );
		}
		else
		{
			sp_strlcat( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, ", %s='%s'", pfield_name, pnew_value );
		}
	}
}

/* ---------------------------------------------------------- */
static void build_merge_insert_values_string( const char *pnew_value, char *pinsert_values_str )
{
	if( pinsert_values_str[ 0 ] == 0 )
	{
		snprintf( pinsert_values_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "'%s'", pnew_value );
	}
	else
	{
		sp_strlcat( pinsert_values_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, ", '%s'", pnew_value );
	}
}

/* ---------------------------------------------------------- */
static void build_merge_update_sql_cursor_var( const char *pfield_name, const char *pnew_value, char *pupdate_str )
{
	// 10/7/2015 ajv : For the case of reports, we don't want to perform an UPSERT, instead we
	// just want to INSERT the data if it's not already in the database. In this case, we pass
	// in a pfield_name pointer of NULL so make sure it's valid before we try to use it.
	if( pfield_name != NULL )
	{
		if( pupdate_str[ 0 ] == 0 )
		{
			snprintf( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s=%s", pfield_name, pnew_value );
		}
		else
		{
			sp_strlcat( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, ", %s=%s", pfield_name, pnew_value );
		}
	}
}

/* ---------------------------------------------------------- */
static void build_merge_insert_values_sql_cursor_var( const char *pnew_value, char *pinsert_values_str )
{
	if( pinsert_values_str[ 0 ] == 0 )
	{
		snprintf( pinsert_values_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s", pnew_value );
	}
	else
	{
		sp_strlcat( pinsert_values_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, ", %s", pnew_value );
	}
}

/* ---------------------------------------------------------- */
static void build_merge_update_raw( const char *pfield_name, const char *pnew_value, const UNS_32 praw_data_len, char *pupdate_str )
{
	char	lnew_value_arr[ 48 ];

	UNS_32 i;

	// ----------

	// 10/7/2015 ajv : For the case of reports, we don't want to perform an UPSERT, instead we
	// just want to INSERT the data if it's not already in the database. In this case, we pass
	// in a pfield_name pointer of NULL so make sure it's valid before we try to use it.
	if( pfield_name != NULL )
	{
		if( pupdate_str[ 0 ] == 0 )
		{
			snprintf( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s=CONVERT('", pfield_name );
		}
		else
		{
			sp_strlcat( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, ", %s=CONVERT('", pfield_name );
		}

		memset( lnew_value_arr, 0x00, sizeof(lnew_value_arr) );

		memcpy( lnew_value_arr, pnew_value, praw_data_len );

		for( i = 0; i < praw_data_len; ++i )
		{
			sp_strlcat( pupdate_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%c", lnew_value_arr[ i ] );
		}

		strlcat( pupdate_str, "', SQL_BINARY)", SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	}
}

/* ---------------------------------------------------------- */
static void build_merge_insert_values_raw( const char *pnew_value, const UNS_32 praw_data_len, char *pinsert_values_str )
{
	char	lnew_value_arr[ 48 ];

	UNS_32 i;

	// ----------

	if( pinsert_values_str[ 0 ] == 0 )
	{
		strlcpy( pinsert_values_str, "CONVERT('", SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	}
	else
	{
		strlcat( pinsert_values_str, ", CONVERT('", SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	}

	memset( lnew_value_arr, 0x00, sizeof(lnew_value_arr) );

	memcpy( lnew_value_arr, pnew_value, praw_data_len );

	for( i = 0; i < praw_data_len; ++i )
	{
		sp_strlcat( pinsert_values_str, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%c", lnew_value_arr[ i ] );
	}

	strlcat( pinsert_values_str, "', SQL_BINARY)", SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern char* SQL_MERGE_build_insert_merge_and_append_to_SQL_statements( const char *ptable_name, const char *psearch_condition, const BOOL_32 pinclude_field_names, const char *pinsert_fields_str, const char *pinsert_values_str, char *pSQL_statements, char *pSQL_single_merge_statement_buf )
{
	if( pinclude_field_names == SQL_MERGE_include_field_name_in_insert_clause )
	{
		snprintf( pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT,
				  "MERGE %s ON %s WHEN NOT MATCHED THEN INSERT (%s) VALUES (%s);",
				  ptable_name, psearch_condition, pinsert_fields_str, pinsert_values_str );
	}
	else
	{
		snprintf( pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT,
				  "MERGE %s ON %s WHEN NOT MATCHED THEN INSERT VALUES (%s);",
				  ptable_name, psearch_condition, pinsert_values_str );
	}

	// ----------

	// 9/16/2015 ajv : Now append the MERGE statement to the list of other SQL statements that
	// may already be waiting to be processed.
	strlcat( pSQL_statements, pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS );

	// ----------

	return( pSQL_statements );
}

extern char* SQL_INSERT_build_insert_and_append_to_SQL_statements( const char *ptable_name, const BOOL_32 pinclude_field_names, const char *pinsert_fields_str, const char *pinsert_values_str, char *pSQL_statements, char *pSQL_single_merge_statement_buf )
{
	if( pinclude_field_names == SQL_MERGE_include_field_name_in_insert_clause )
	{
		snprintf( pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT,
				  "INSERT INTO %s (%s) VALUES (%s);",
				  ptable_name, pinsert_fields_str, pinsert_values_str );
	}
	else
	{
		snprintf( pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT,
				  "INSERT INTO %s VALUES (%s);",
				  ptable_name, pinsert_values_str );
	}

	// ----------

	// 9/16/2015 ajv : Now append the MERGE statement to the list of other SQL statements that
	// may already be waiting to be processed.
	strlcat( pSQL_statements, pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS );

	// ----------

	return( pSQL_statements );
}


/* ---------------------------------------------------------- */
extern char* SQL_MERGE_build_legacy_report_insert_merge_and_append_to_SQL_statements( const char *ptable_name, const DATE_TIME pdt, const char *psearch_condition, const BOOL_32 pinclude_field_names, const char *pinsert_fields_str, const char *pinsert_values_str, char *pSQL_statements, char *pSQL_single_merge_statement_buf )
{
	char	ltimestamp_str[ 32 ];

	make_timestamp_SQL_ready( pdt, ltimestamp_str, sizeof(ltimestamp_str) );

	if( pinclude_field_names == SQL_MERGE_include_field_name_in_insert_clause )
	{
		snprintf( pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT,
				  "IF ('%s'<CURRENT_TIMESTAMP()) THEN MERGE %s ON %s WHEN NOT MATCHED THEN INSERT (%s) VALUES (%s);ENDIF;",
				  ltimestamp_str, ptable_name, psearch_condition, pinsert_fields_str, pinsert_values_str );
	}
	else
	{
		snprintf( pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT,
				  "IF('%s'<CURRENT_TIMESTAMP()) THEN MERGE %s ON %s WHEN NOT MATCHED THEN INSERT VALUES(%s);ENDIF;",
				  ltimestamp_str, ptable_name, psearch_condition, pinsert_values_str );
	}

	// ----------

	// 9/16/2015 ajv : Now append the MERGE statement to the list of other SQL statements that
	// may already be waiting to be processed.
	strlcat( pSQL_statements, pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS );

	// ----------

	return( pSQL_statements );
}

extern char* SQL_MERGE_build_legacy_report_insert_and_append_to_SQL_statements( const char *ptable_name, const DATE_TIME pdt, const BOOL_32 pinclude_field_names, const char *pinsert_fields_str, const char *pinsert_values_str, char *pSQL_statements, char *pSQL_single_merge_statement_buf )
{
	char	ltimestamp_str[ 32 ];

	make_timestamp_SQL_ready( pdt, ltimestamp_str, sizeof(ltimestamp_str) );

	if( pinclude_field_names == SQL_MERGE_include_field_name_in_insert_clause )
	{
		snprintf( pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT,
				  "IF ('%s'<CURRENT_TIMESTAMP()) THEN INSERT INTO %s (%s) VALUES (%s);ENDIF;",
				  ltimestamp_str, ptable_name, pinsert_fields_str, pinsert_values_str );
	}
	else
	{
		snprintf( pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT,
				  "IF('%s'<CURRENT_TIMESTAMP()) THEN INSERT INTO %s VALUES(%s);ENDIF;",
				  ltimestamp_str, ptable_name, pinsert_values_str );
	}

	// ----------

	// 9/16/2015 ajv : Now append the MERGE statement to the list of other SQL statements that
	// may already be waiting to be processed.
	strlcat( pSQL_statements, pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS );

	// ----------

	return( pSQL_statements );
}

extern char* SQL_MERGE_build_legacy_pdata_insert_and_append_to_SQL_statements( const char *ptable_name, const BOOL_32 pinclude_field_names, const char *pinsert_fields_str, const char *pinsert_values_str, char *pSQL_statements, char *pSQL_single_merge_statement_buf )
{
	if( pinclude_field_names == SQL_MERGE_include_field_name_in_insert_clause )
	{
		snprintf( pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT,
				  "INSERT INTO %s (%s) VALUES (%s);",
				  ptable_name, pinsert_fields_str, pinsert_values_str );
	}
	else
	{
		snprintf( pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT,
				  "INSERT INTO %s VALUES (%s);",
				  ptable_name, pinsert_values_str );
	}

	// ----------

	// 9/16/2015 ajv : Now append the MERGE statement to the list of other SQL statements that
	// may already be waiting to be processed.
	strlcat( pSQL_statements, pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS );

	// ----------

	return( pSQL_statements );
}


/* ---------------------------------------------------------- */
extern char* SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements( const char *ptable_name, const char *psearch_condition, const BOOL_32 pinclude_field_names, const char *pupdate_str, const char *pinsert_fields_str, const char *pinsert_values_str, char *pSQL_statements, char *pSQL_single_merge_statement_buf )
{
	if( pinclude_field_names == SQL_MERGE_include_field_name_in_insert_clause )
	{
		// 9/16/2015 ajv : Build the individual MERGE statement for this row.
		snprintf( pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT,
				  "MERGE %s ON %s WHEN MATCHED THEN UPDATE SET %s WHEN NOT MATCHED THEN INSERT (%s) VALUES (%s);",
				  ptable_name, psearch_condition, pupdate_str, pinsert_fields_str, pinsert_values_str );
	}
	else
	{
		// 9/16/2015 ajv : Build the individual MERGE statement for this row.
		snprintf( pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT,
				  "MERGE %s ON %s WHEN MATCHED THEN UPDATE SET %s WHEN NOT MATCHED THEN INSERT VALUES (%s);",
				  ptable_name, psearch_condition, pupdate_str, pinsert_values_str );
	}

	// ----------

	// 9/16/2015 ajv : Now append the MERGE statement to the list of other SQL statements that
	// may already be waiting to be processed.
	strlcat( pSQL_statements, pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS );

	// ----------

	return( pSQL_statements );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_search_condition_bool( const char *pfield_name, const BOOL_32 pvalue, char *psearch_condition )
{
	if( psearch_condition[ 0 ] == 0 )
	{
		snprintf( psearch_condition, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s=%u", pfield_name, pvalue );
	}
	else
	{
		sp_strlcat( psearch_condition, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, " AND %s=%u", pfield_name, pvalue );
	}
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_search_condition_char( const char *pfield_name, const char pvalue, char *psearch_condition )
{
	if( psearch_condition[ 0 ] == 0 )
	{
		snprintf( psearch_condition, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s='%c'", pfield_name, pvalue );
	}
	else
	{
		sp_strlcat( psearch_condition, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, " AND %s='%c'", pfield_name, pvalue );
	}
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_search_condition_uint32( const char *pfield_name, const UNS_32 pvalue, char *psearch_condition )
{
	if( psearch_condition[ 0 ] == 0 )
	{
		snprintf( psearch_condition, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s=%u", pfield_name, pvalue );
	}
	else
	{
		sp_strlcat( psearch_condition, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, " AND %s=%u", pfield_name, pvalue );
	}
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_search_condition_int32( const char *pfield_name, const INT_32 pvalue, char *psearch_condition )
{
	if( psearch_condition[ 0 ] == 0 )
	{
		snprintf( psearch_condition, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s=%d", pfield_name, pvalue );
	}
	else
	{
		sp_strlcat( psearch_condition, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, " AND %s=%d", pfield_name, pvalue );
	}
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_search_condition_float( const char *pfield_name, const float pvalue, char *psearch_condition )
{
	if( psearch_condition[ 0 ] == 0 )
	{
		snprintf( psearch_condition, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s=%f", pfield_name, pvalue );
	}
	else
	{
		sp_strlcat( psearch_condition, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, " AND %s=%f", pfield_name, pvalue );
	}
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_search_condition_double( const char *pfield_name, const double pvalue, char *psearch_condition )
{
	if( psearch_condition[ 0 ] == 0 )
	{
		snprintf( psearch_condition, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, "%s=%f", pfield_name, pvalue );
	}
	else
	{
		sp_strlcat( psearch_condition, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE, " AND %s=%f", pfield_name, pvalue );
	}
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_search_condition_string( const char *pfield_name, const char *pstr, char *psearch_condition )
{
	build_merge_search_string( pfield_name, pstr, psearch_condition );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_search_condition_sql_cursor_var( const char *pfield_name, const char *pstr, char *psearch_condition )
{
	build_merge_search_sql_cursor_var( pfield_name, pstr, psearch_condition );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_search_condition_date( const char *pfield_name, const UNS_32 pdate, char *psearch_condition )
{
	char	ldate_str[ 32 ];

	make_date_SQL_ready( pdate, ldate_str, sizeof(ldate_str) );

	// ----------

	build_merge_search_string( pfield_name, ldate_str, psearch_condition );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_search_condition_time( const char *pfield_name, const UNS_32 ptime, char *psearch_condition )
{
	char	ltime_str[ 32 ];

	make_time_SQL_ready( ptime, ltime_str, sizeof(ltime_str) );

	// ----------

	build_merge_search_string( pfield_name, ltime_str, psearch_condition );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_search_condition_timestamp( const char *pfield_name, const DATE_TIME pdt, char *psearch_condition )
{
	char	ltimestamp_str[ 32 ];

	make_timestamp_SQL_ready( pdt, ltimestamp_str, sizeof(ltimestamp_str) );

	// ----------

	build_merge_search_string( pfield_name, ltimestamp_str, psearch_condition );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_insert_bool( const BOOL_32 pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pinsert_fields_str, char *pinsert_values_str )
{
	if( pinclude_field_names == SQL_MERGE_include_field_name_in_insert_clause )
	{
		build_merge_insert_fields( pfield_name, pinsert_fields_str );
	}

	build_merge_insert_values_bool( pnew_value, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_insert_char( const char pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pinsert_fields_str, char *pinsert_values_str )
{
	if( pinclude_field_names == SQL_MERGE_include_field_name_in_insert_clause )
	{
		build_merge_insert_fields( pfield_name, pinsert_fields_str );
	}

	build_merge_insert_values_char( pnew_value, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_insert_uint32( const UNS_32 pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pinsert_fields_str, char *pinsert_values_str )
{
	if( pinclude_field_names == SQL_MERGE_include_field_name_in_insert_clause )
	{
		build_merge_insert_fields( pfield_name, pinsert_fields_str );
	}

	build_merge_insert_values_uint32( pnew_value, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_insert_int32( const INT_32 pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pinsert_fields_str, char *pinsert_values_str )
{
	if( pinclude_field_names == SQL_MERGE_include_field_name_in_insert_clause )
	{
		build_merge_insert_fields( pfield_name, pinsert_fields_str );
	}

	build_merge_insert_values_int32( pnew_value, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_insert_float( const float pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pinsert_fields_str, char *pinsert_values_str )
{
	if( pinclude_field_names == SQL_MERGE_include_field_name_in_insert_clause )
	{
		build_merge_insert_fields( pfield_name, pinsert_fields_str );
	}

	build_merge_insert_values_float( pnew_value, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_insert_double( const double pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pinsert_fields_str, char *pinsert_values_str )
{
	if( pinclude_field_names == SQL_MERGE_include_field_name_in_insert_clause )
	{
		build_merge_insert_fields( pfield_name, pinsert_fields_str );
	}

	build_merge_insert_values_double( pnew_value, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_insert_string( const char *pnew_value, const UNS_32 pstr_len, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pinsert_fields_str, char *pinsert_values_str )
{
	// 1/19/2015 ajv : SQL uses apostrophes as the beginning and end markers of strings. In
	// order to store a string that contains an apostrophe, it needs to be converted to two
	// apostrophes.
	//
	// Assume, worst-case, that every character in the name is an apostrophe, so allocate double
	// the size of a normal string.
	char	lnew_value_with_fixed_apostrophes[ (NUMBER_OF_CHARS_IN_A_NAME * 2) + 1 ];

	make_string_SQL_ready( pnew_value, pstr_len, lnew_value_with_fixed_apostrophes, sizeof(lnew_value_with_fixed_apostrophes) );

	// ----------

	if( pinclude_field_names == SQL_MERGE_include_field_name_in_insert_clause )
	{
		build_merge_insert_fields( pfield_name, pinsert_fields_str );
	}

	build_merge_insert_values_string( lnew_value_with_fixed_apostrophes, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_insert_sql_cursor_var( const char *pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pinsert_fields_str, char *pinsert_values_str )
{
	if( pinclude_field_names == SQL_MERGE_include_field_name_in_insert_clause )
	{
		build_merge_insert_fields( pfield_name, pinsert_fields_str );
	}

	build_merge_insert_values_sql_cursor_var( pnew_value, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_insert_raw( const char *pnew_value, const UNS_32 praw_data_len, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pinsert_fields_str, char *pinsert_values_str )
{
	if( pinclude_field_names == SQL_MERGE_include_field_name_in_insert_clause )
	{
		build_merge_insert_fields( pfield_name, pinsert_fields_str );
	}

	build_merge_insert_values_raw( pnew_value, praw_data_len, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_insert_date( const UNS_32 pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pinsert_fields_str, char *pinsert_values_str )
{
	char	ldate_str[ 32 ];

	make_date_SQL_ready( pnew_value, ldate_str, sizeof(ldate_str) );

	// ----------

	SQL_MERGE_build_insert_string( ldate_str, sizeof(ldate_str), pinclude_field_names, pfield_name, pinsert_fields_str, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_insert_time( const UNS_32 pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pinsert_fields_str, char *pinsert_values_str )
{
	char	ltime_str[ 32 ];

	make_time_SQL_ready( pnew_value, ltime_str, sizeof(ltime_str) );

	// ----------

	SQL_MERGE_build_insert_string( ltime_str, sizeof(ltime_str), pinclude_field_names, pfield_name, pinsert_fields_str, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_insert_timestamp( const DATE_TIME pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pinsert_fields_str, char *pinsert_values_str )
{
	char	ltimestamp_str[ 32 ];

	make_timestamp_SQL_ready( pnew_value, ltimestamp_str, sizeof(ltimestamp_str) );

	// ----------

	SQL_MERGE_build_insert_string( ltimestamp_str, sizeof(ltimestamp_str), pinclude_field_names, pfield_name, pinsert_fields_str, pinsert_values_str );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_upsert_bool( const BOOL_32 pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pupdate_str, char *pinsert_fields_str, char *pinsert_values_str )
{
	build_merge_update_bool( pfield_name, pnew_value, pupdate_str );

	SQL_MERGE_build_insert_bool( pnew_value, pinclude_field_names, pfield_name, pinsert_fields_str, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_upsert_char( const char pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pupdate_str, char *pinsert_fields_str, char *pinsert_values_str )
{
	build_merge_update_char( pfield_name, pnew_value, pupdate_str );

	SQL_MERGE_build_insert_char( pnew_value, pinclude_field_names, pfield_name, pinsert_fields_str, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_upsert_uint32( const UNS_32 pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pupdate_str, char *pinsert_fields_str, char *pinsert_values_str )
{
	build_merge_update_uint32( pfield_name, pnew_value, pupdate_str );

	SQL_MERGE_build_insert_uint32( pnew_value, pinclude_field_names, pfield_name, pinsert_fields_str, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_upsert_int32( const INT_32 pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pupdate_str, char *pinsert_fields_str, char *pinsert_values_str )
{
	build_merge_update_int32( pfield_name, pnew_value, pupdate_str );

	SQL_MERGE_build_insert_int32( pnew_value, pinclude_field_names, pfield_name, pinsert_fields_str, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_upsert_float( const float pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pupdate_str, char *pinsert_fields_str, char *pinsert_values_str )
{
	build_merge_update_float( pfield_name, pnew_value, pupdate_str );

	SQL_MERGE_build_insert_float( pnew_value, pinclude_field_names, pfield_name, pinsert_fields_str, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_upsert_double( const double pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pupdate_str, char *pinsert_fields_str, char *pinsert_values_str )
{
	build_merge_update_double( pfield_name, pnew_value, pupdate_str );

	SQL_MERGE_build_insert_double( pnew_value, pinclude_field_names, pfield_name, pinsert_fields_str, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_upsert_string( const char *pnew_value, const UNS_32 pstr_len, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pupdate_str, char *pinsert_fields_str, char *pinsert_values_str )
{
	// 1/19/2015 ajv : SQL uses apostrophes as the beginning and end markers of strings. In
	// order to store a string that contains an apostrophe, it needs to be converted to two
	// apostrophes.
	//
	// Assume, worst-case, that every character in the name is an apostrophe, so allocate double
	// the size of a normal string.
	char	lnew_value_with_fixed_apostrophes[ (NUMBER_OF_CHARS_IN_A_NAME * 2) + 1 ];

	make_string_SQL_ready( pnew_value, pstr_len, lnew_value_with_fixed_apostrophes, sizeof(lnew_value_with_fixed_apostrophes) );

	// ----------

	build_merge_update_string( pfield_name, lnew_value_with_fixed_apostrophes, pupdate_str );

	if( pinclude_field_names == SQL_MERGE_include_field_name_in_insert_clause )
	{
		build_merge_insert_fields( pfield_name, pinsert_fields_str );
	}

	build_merge_insert_values_string( lnew_value_with_fixed_apostrophes, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_upsert_sql_cursor_var( const char *pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pupdate_str, char *pinsert_fields_str, char *pinsert_values_str )
{
	build_merge_update_sql_cursor_var( pfield_name, pnew_value, pupdate_str );

	if( pinclude_field_names == SQL_MERGE_include_field_name_in_insert_clause )
	{
		build_merge_insert_fields( pfield_name, pinsert_fields_str );
	}

	build_merge_insert_values_sql_cursor_var( pnew_value, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_upsert_raw( const char *pnew_value, const UNS_32 praw_data_len, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pupdate_str, char *pinsert_fields_str, char *pinsert_values_str )
{
	build_merge_update_raw( pfield_name, pnew_value, praw_data_len, pupdate_str );

	if( pinclude_field_names == SQL_MERGE_include_field_name_in_insert_clause )
	{
		build_merge_insert_fields( pfield_name, pinsert_fields_str );
	}

	build_merge_insert_values_raw( pnew_value, praw_data_len, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_upsert_date( const UNS_32 pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pupdate_str, char *pinsert_fields_str, char *pinsert_values_str )
{
	char	ldate_str[ 32 ];

	make_date_SQL_ready( pnew_value, ldate_str, sizeof(ldate_str) );

	// ----------

	SQL_MERGE_build_upsert_string( ldate_str, sizeof(ldate_str), pinclude_field_names, pfield_name, pupdate_str, pinsert_fields_str, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_upsert_time( const UNS_32 pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pupdate_str, char *pinsert_fields_str, char *pinsert_values_str )
{
	char	ltime_str[ 32 ];

	make_time_SQL_ready( pnew_value, ltime_str, sizeof(ltime_str) );

	// ----------

	SQL_MERGE_build_upsert_string( ltime_str, sizeof(ltime_str), pinclude_field_names, pfield_name, pupdate_str, pinsert_fields_str, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void SQL_MERGE_build_upsert_timestamp( const DATE_TIME pnew_value, const BOOL_32 pinclude_field_names, const char *pfield_name, char *pupdate_str, char *pinsert_fields_str, char *pinsert_values_str )
{
	char	ltimestamp_str[ 32 ];

	make_timestamp_SQL_ready( pnew_value, ltimestamp_str, sizeof(ltimestamp_str) );

	// ----------

	SQL_MERGE_build_upsert_string( ltimestamp_str, sizeof(ltimestamp_str), pinclude_field_names, pfield_name, pupdate_str, pinsert_fields_str, pinsert_values_str );
}

/* ---------------------------------------------------------- */
extern void update_cs3000_last_timestamp_in_network_table( const UNS_32 pnetwork_id,
														   DATE_TIME llast_time_report_was_retrieved,
														   char *field_name,
														   char *pSQL_statements,
														   char *pSQL_single_merge_statement_buf )
{
	memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );

	char	ltimestamp_str[ 32 ];

	make_timestamp_SQL_ready( llast_time_report_was_retrieved, ltimestamp_str, sizeof(ltimestamp_str) );

	snprintf( pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT,
			  "UPDATE CS3000NetworkSettings SET %s='%s' WHERE NetworkID=%u;",
			  field_name, ltimestamp_str, pnetwork_id );

	strlcat( pSQL_statements, pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

