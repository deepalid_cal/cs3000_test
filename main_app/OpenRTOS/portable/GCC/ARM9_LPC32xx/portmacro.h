/*
	OpenRTOS V6.1.0 Copyright (C) Wittenstein High Integrity Systems.

	OpenRTOS is distributed exclusively by Wittenstein High Integrity Systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on distribution.  It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses are issued for each concurrent user working on a specified product
	line.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court,, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com
	Registered in England No. 3711047; VAT No. GB 729 1583 15

	http://www.HighIntegritySystems.com
*/


#ifndef PORTMACRO_H
#define PORTMACRO_H

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------
 * Port specific definitions.  
 *
 * The settings in this file configure FreeRTOS correctly for the
 * given hardware and compiler.
 *
 * These settings should not be altered.
 *-----------------------------------------------------------
 */

/* Type definitions. */
#define portCHAR		char
#define portFLOAT		float
#define portDOUBLE		double
#define portLONG		long
#define portSHORT		short
#define portSTACK_TYPE	unsigned portLONG
#define portBASE_TYPE	portLONG

#if( configUSE_16_BIT_TICKS == 1 )
	typedef unsigned portSHORT portTickType;
	#define portMAX_DELAY ( portTickType ) 0xffff
#else
	typedef unsigned portLONG portTickType;
	#define portMAX_DELAY ( portTickType ) 0xffffffff
#endif
/*-----------------------------------------------------------*/	

/* Architecture specifics. */
#define portSTACK_GROWTH				( -1 )

// ----------

#define portTICK_RATE_MS				( ( portTickType ) 1000 / configTICK_RATE_HZ )		

// Use this macro to set delays directly in ms. Good for OS tick rates up to but not
// exceeding 1000Hz.
#define MS_to_TICKS( ms )				((ms)/portTICK_RATE_MS)   

// 6/27/2018 rmd : This macro will convert the number of ticks into seconds, using integer
// math. So the smallest whole number of seconds, which is usually fine if measuring a large
// number of seconds.
#define TICKS_to_SECONDS( ticks )		( (ticks) / configTICK_RATE_HZ )

// 6/27/2018 rmd : A higher precision measurment, which will produce a floating point value
// for the number of seconds, including the fractional part of course.
#define TICKS_to_FLOAT_SECONDS( ticks )		( (float)(ticks) / (float)configTICK_RATE_HZ )
   
// ----------
   
// 06.13.2011 rmd
// As received from Wittenstein the portBYTE_ALIGNMENT was set to an 8. This caused an assert failure in a stack alignment test
// performed when tasks are created. It reports an error at tasks.c line 469. In portmacro.h defines you'll find the stack type
// defined as a long (4 bytes) and the byte alignment set at 8 bytes. Well the top of stack starts out okay. But after the call
// to pxPortInitialiseStack there is an odd (as in not even) number of decrements made. So the resulting top of stack is now 4
// byte aligned. And fails that assert. I set portBYTE_ALIGNMENT to 4. And all 'seems' to work for now. 8 feels more comfy of a
// place to be. But it seems the stack type size and byte alignment value need to be the same. Which makes me wonder why both
// exist.Why isn't it stated this way: #define portBYTE_ALIGNMENT  sizeof(portSTACK_TYPE).
// 
// 2011.06.24 rmd
// UPDATE : Wittenstein reports that this must be an 8. And the fix is to add a decrement to the stack pointer in the stack
// intialization routine (called when a task is created).
#define portBYTE_ALIGNMENT			8


#define portNOP()					__asm volatile ( "NOP" );

/* There are 32 * 32bit floating point registers, plus the FPSCR to save. */
#define portNO_FLOP_REGISTERS_TO_SAVE  ( 32 + 1 )

/*-----------------------------------------------------------*/	
/* Scheduler utilities. */

/*
 * portRESTORE_CONTEXT, portRESTORE_CONTEXT, portENTER_SWITCHING_ISR
 * and portEXIT_SWITCHING_ISR can only be called from ARM mode, but
 * are included here for efficiency.  An attempt to call one from
 * THUMB mode code will result in a compile time error.
 */
/* Scheduler utilities. */

#define portRESTORE_CONTEXT()											\
{																		\
extern volatile void * volatile pxCurrentTCB;							\
extern volatile unsigned portLONG ulCriticalNesting;					\
																		\
	/* Set the LR to the task stack. */									\
	__asm volatile (													\
	"LDR		R0, =pxCurrentTCB								\n\t"	\
	"LDR		R0, [R0]										\n\t"	\
	"LDR		LR, [R0]										\n\t"	\
																		\
	/* The critical nesting depth is the first item on the stack. */	\
	/* Load it into the ulCriticalNesting variable. */					\
	"LDR		R0, =ulCriticalNesting							\n\t"	\
	"LDMFD	LR!, {R1}											\n\t"	\
	"STR		R1, [R0]										\n\t"	\
																		\
	/* Get the SPSR from the stack. */									\
	"LDMFD	LR!, {R0}											\n\t"	\
	"MSR		SPSR, R0										\n\t"	\
																		\
	/* Restore all system mode registers for the task. */				\
	"LDMFD	LR, {R0-R14}^										\n\t"	\
	"NOP														\n\t"	\
																		\
	/* Restore the return address. */									\
	"LDR		LR, [LR, #+60]									\n\t"	\
																		\
	/* And return - correcting the offset in the LR to obtain the */	\
	/* correct address. */												\
	"SUBS	PC, LR, #4											\n\t"	\
	);																	\
	( void ) ulCriticalNesting;											\
	( void ) pxCurrentTCB;												\
}
/*-----------------------------------------------------------*/

#define portSAVE_CONTEXT()												\
{																		\
extern volatile void * volatile pxCurrentTCB;							\
extern volatile unsigned portLONG ulCriticalNesting;					\
																		\
	/* Push R0 as we are going to use the register. */					\
	__asm volatile (													\
	"STMDB	SP!, {R0}											\n\t"	\
																		\
	/* Set R0 to point to the task stack pointer. */					\
	"STMDB	SP,{SP}^											\n\t"	\
	"NOP														\n\t"	\
	"SUB	SP, SP, #4											\n\t"	\
	"LDMIA	SP!,{R0}											\n\t"	\
																		\
	/* Push the return address onto the stack. */						\
	"STMDB	R0!, {LR}											\n\t"	\
																		\
	/* Now we have saved LR we can use it instead of R0. */				\
	"MOV	LR, R0												\n\t"	\
																		\
	/* Pop R0 so we can save it onto the system mode stack. */			\
	"LDMIA	SP!, {R0}											\n\t"	\
																		\
	/* Push all the system mode registers onto the task stack. */		\
	"STMDB	LR,{R0-LR}^											\n\t"	\
	"NOP														\n\t"	\
	"SUB	LR, LR, #60											\n\t"	\
																		\
	/* Push the SPSR onto the task stack. */							\
	"MRS	R0, SPSR											\n\t"	\
	"STMDB	LR!, {R0}											\n\t"	\
																		\
	"LDR	R0, =ulCriticalNesting								\n\t"	\
	"LDR	R0, [R0]											\n\t"	\
	"STMDB	LR!, {R0}											\n\t"	\
																		\
	/* Store the new top of stack for the task. */						\
	"LDR	R0, =pxCurrentTCB									\n\t"	\
	"LDR	R0, [R0]											\n\t"	\
	"STR	LR, [R0]											\n\t"	\
	);																	\
	( void ) ulCriticalNesting;											\
	( void ) pxCurrentTCB;												\
}

/*-----------------------------------------------------------*/

#define portYIELD_FROM_ISR( )                    \
{                                                \
extern void vTaskSwitchContext( void );          \
    vTaskSwitchContext( );                       \
}
#define portYIELD()					__asm volatile ( "SWI 0" )
/*-----------------------------------------------------------*/

/* Critical section management. */

/*
 * The interrupt management utilities can only be called from ARM mode.  When
 * THUMB_INTERWORK is defined the utilities are defined as functions in 
 * portISR.c to ensure a switch to ARM mode.  When THUMB_INTERWORK is not 
 * defined then the utilities are defined as macros here - as per other ports.
 */
#ifdef THUMB_INTERWORK
extern void vPortDisableInterruptsFromThumb( void ) __attribute__ ((naked));
extern void vPortEnableInterruptsFromThumb( void ) __attribute__ ((naked));

    #define portDISABLE_INTERRUPTS( )	vPortDisableInterruptsFromThumb( )											
	#define portENABLE_INTERRUPTS( )	vPortEnableInterruptsFromThumb( )											
#else 
	#define portDISABLE_INTERRUPTS( )													\
			__asm volatile (															\
					"STMDB	SP!, {R0}		\n\t"	/* Push R0.						*/	\
					"MRS	R0, CPSR		\n\t"	/* Get CPSR.					*/	\
					"ORR	R0, R0, #0xC0	\n\t"	/* Disable IRQ, FIQ.			*/	\
					"MSR	CPSR, R0		\n\t"	/* Write back modified value.	*/	\
					"LDMIA	SP!, {R0}			" )	/* Pop R0.						*/
					
	#define portENABLE_INTERRUPTS()														\
			__asm volatile (															\
					"STMDB	SP!, {R0}		\n\t"	/* Push R0.						*/	\
					"MRS	R0, CPSR		\n\t"	/* Get CPSR.					*/	\
					"BIC	R0, R0, #0xC0	\n\t"	/* Enable IRQ, FIQ.				*/	\
					"MSR	CPSR, R0		\n\t"	/* Write back modified value.	*/	\
					"LDMIA	SP!, {R0}			" )	/* Pop R0.						*/									
#endif /* THUMB_INTERWORK */
								
extern void vPortEnterCritical( void );
extern void vPortExitCritical( void );

#define portENTER_CRITICAL( )		vPortEnterCritical( );
#define portEXIT_CRITICAL( )		vPortExitCritical( );
/*-----------------------------------------------------------*/

/* Task function macros as described on the FreeRTOS.org WEB site. */
#define portTASK_FUNCTION_PROTO( vFunction, pvParameters ) void vFunction( void *pvParameters )
#define portTASK_FUNCTION( vFunction, pvParameters ) void vFunction( void *pvParameters )

#ifdef __cplusplus
}
#endif

#endif /* PORTMACRO_H */

