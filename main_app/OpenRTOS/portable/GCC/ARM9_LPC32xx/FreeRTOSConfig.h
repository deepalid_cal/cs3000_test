/*
	OpenRTOS V6.1.0 Copyright (C) Wittenstein High Integrity Systems.

	OpenRTOS is distributed exclusively by Wittenstein High Integrity Systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on distribution.  It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses are issued for each concurrent user working on a specified product
	line.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court,, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com
	Registered in England No. 3711047; VAT No. GB 729 1583 15

	http://www.HighIntegritySystems.com
*/

#ifndef FREERTOS_CONFIG_H
#define FREERTOS_CONFIG_H

/*-----------------------------------------------------------
 * Application specific definitions.
 *
 * These definitions should be adjusted for your particular hardware and
 * application requirements.
 *
 * THESE PARAMETERS ARE DESCRIBED WITHIN THE 'CONFIGURATION' SECTION OF THE
 * FreeRTOS API DOCUMENTATION AVAILABLE ON THE FreeRTOS.org WEB SITE. 
 *
 * See http://www.freertos.org/a00110.html.
 *----------------------------------------------------------*/

// ----------

// 2010.10.20 rmd : Calsense has their own copy of assert.h within our project directories.
// Include with "filename" instead of <filename>. Though I think it is actually okay as the
// compiler first searches our specified includes and then the defined system directories.
#include	"assert.h"

#define configASSERT(e) assert(e)

// ----------

#define configUSE_PREEMPTION				1

#define configUSE_IDLE_HOOK         		0
#define configUSE_TICK_HOOK         		0
#define configUSE_MALLOC_FAILED_HOOK		1


#define configCPU_CLOCK_HZ          		( ( unsigned long ) 208000000 )	
#define configTICK_RATE_HZ          		( ( portTickType ) 200 )

#define configMAX_PRIORITIES				16

//#define configMINIMAL_STACK_SIZE			( ( unsigned short ) 120 )
// 11/30/2017 rmd : This is the STACK SIZE used for the IDLE task.
#define configMINIMAL_STACK_SIZE			( ( unsigned short ) 256 )

#define configTOTAL_HEAP_SIZE				( 0 ) /* This parameter is not used when heap_3 is included in a project. */

#define configMAX_TASK_NAME_LEN				( 32 )

// 3/12/2013 rmd : Hey this is a 32 bit machine. Definately use 32-bit tick type.
#define configUSE_16_BIT_TICKS				0

#define configIDLE_SHOULD_YIELD				1

// --------

// 9/11/2012 rmd : Yes we are using the LPC3250 on-chip VFP.
#define configUSE_VFP						1

// 9/11/2012 rmd : With regards to FreeRTOS, where this define is used, I think there is no
// difference between declaring this a 0 or a 1. The end result is the same. All 32 32-bit
// VFP registers are saved and restored upon a context switch. Declaring as DOUBLE_PRECISION
// causes the 32 32-bit registers to be saved using 16 64-bit register save instructions.
// But the end result is the same. And the data saved is the same. The registers saved are
// the same. However it may be more efficient to use the 16 64-bit register save approach.
// So I will define the use of double precision to a 1.
#define configUSE_DOUBLE_PRECISION_VFP   	1

// 9/11/2012 rmd : ONLY tasks that actually perform any VFP register manipulation need to
// save and restore the VFP registers as the task is switched out and back in. VFP register
// manipulation is only achieved in this application via floating point math. The TASK_TAG
// is used as a pointer to the storage location a task is to use when storing or restoring
// the VFP registers upon context switch. And almost EVERY task has this set so that the
// store and restore VFP register operation will be performed upon context switch. There are
// TWO tasks that will not have an application task tag set - the IDLE task and the
// FreeRTOS_TIMER task. These two perform no floating point operations and therefore do not
// need such. However if the task handle is easily available for the timer task you may find
// the task tag being set for it.
#define configUSE_APPLICATION_TASK_TAG		1

// 9/11/2012 rmd : Hey we can get the timer task handle. Yeah!
#define INCLUDE_xTimerGetTimerDaemonTaskHandle		1

// --------

// 9/12/2012 rmd : See stackmacros.h. Setting this define to a 2 provides the most thorough
// stack check mechanism. And allows us to see the unused stack space.
#define configCHECK_FOR_STACK_OVERFLOW		2

// --------

#define configUSE_MUTEXES               	1

#define configUSE_RECURSIVE_MUTEXES     	1

#define INCLUDE_xQueueGetMutexHolder		1

// --------

#define configQUEUE_REGISTRY_SIZE			10

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


extern unsigned long my_tick_count;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define configGENERATE_RUN_TIME_STATS		1

extern void vConfigureTimerForRunTimeStats( void );

#define portCONFIGURE_TIMER_FOR_RUN_TIME_STATS() vConfigureTimerForRunTimeStats()

unsigned long return_run_timer_counter_value( void );

#define portGET_RUN_TIME_COUNTER_VALUE()	return_run_timer_counter_value()

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* Co-routine definitions. */
#define configUSE_CO_ROUTINES 				0
#define configMAX_CO_ROUTINE_PRIORITIES 	( 2 )


/* Set the following definitions to 1 to include the API function, or zero
to exclude the API function. */

#define INCLUDE_vTaskPrioritySet            1

#define INCLUDE_uxTaskPriorityGet           1

#define INCLUDE_vTaskDelete                 1

#define INCLUDE_vTaskCleanUpResources       1

#define INCLUDE_vTaskSuspend                1

#define INCLUDE_vTaskDelayUntil             1

#define INCLUDE_vTaskDelay                  1

#define INCLUDE_uxTaskGetStackHighWaterMark	1

#define INCLUDE_xTaskGetCurrentTaskHandle	1

#define INCLUDE_pcTaskGetTaskName			1

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#if configUSE_VFP == 1
	/* Include the header that define the traceTASK_SWITCHED_IN() and
	traceTASK_SWITCHED_OUT() macros to save and restore the floating
	point registers for tasks that have requested this behaviour. */
	#include "FPU_Macros.h"
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// configUSE_TIMERS
// Set to 1 to include timer functionality. The timer service task will be automatically created as the scheduler starts when configUSE_TIMERS
// is set to 1.  
#define configUSE_TIMERS	1

// ----------

// configTIMER_TASK_PRIORITY
// Sets the priority of the timer service task. Like all tasks, the timer service task can run at any priority between 0 and
// ( configMAX_PRIORITIES - 1 ). 
// This value needs to be chosen carefully to meet the requirements of the application. For example, if the timer service
// task is made the highest priority task in the system, then commands sent to the timer service task (when a timer API function is called)
// and expired timers will both get processed immediately. Conversely, if the timer service task is given a low priority, then commands sent
// to the timer service task and expired timers will not be processed until the timer service task is the highest priority task that is able
// to run. It is worth noting here however, that timer expiry times are calculated relative to when a command is sent, and not relative to
// when a command is processed.
//
// 2011.08.09 rmd : NOTE - the application design counts on the timer task queue processing to be a very high priority task.
// HIGHER than any task using the timer service task API. This gives us that assurance that timers are started when we ask
// them to be and STOPPED as well when we put in the request. We want that behavior.
//#define configTIMER_TASK_PRIORITY	(PRIORITY_LEVEL_FREE_RTOS_TIMER_TASK_13) too hard to compile this way 
#define configTIMER_TASK_PRIORITY	(13)

// ----------

// configTIMER_QUEUE_LENGTH
// This sets the maximum number of unprocessed commands that the timer command queue can hold at any one time. 
// Reasons the timer command queue might fill up include: 
// Making multiple timer API function calls before the scheduler has been started, and therefore before the timer service task has been created. 
// Making multiple (interrupt safe) timer API function calls from an interrupt service routine (ISR). 
// Making multiple timer API function calls from a task that has a priority above that of the timer service task. 

// 10/9/2012 rmd : A timer start error was reported in the tpl_out.c code. When an OM is
// created and the exist timer was attempted to start. Apparently the timer queue was FULL.
// However we are not exactly sure about all the circumstances and I've decided to leave the
// queue size at the 20 it is and see if the error occurs again.
#define configTIMER_QUEUE_LENGTH	(20)

// ----------

// configTIMER_TASK_STACK_DEPTH  
// Sets the size of the stack (in words, not bytes) allocated to the timer service task. Timer callback functions execute in the context
// of the timer service task. The stack requirement of the timer service task therefore depends on the stack requirements of the timer
// callback functions.
#define configTIMER_TASK_STACK_DEPTH	(0x2000/4)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/7/2012 rmd : For the Precipio FreeRTOS + Trace capability.

#define configUSE_TRACE_FACILITY	(0)

#include	"trcHooks.h"

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

