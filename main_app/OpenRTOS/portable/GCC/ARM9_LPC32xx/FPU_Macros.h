/*
	OpenRTOS V6.1.0 Copyright (C) Wittenstein High Integrity Systems.

	OpenRTOS is distributed exclusively by Wittenstein High Integrity Systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on distribution.  It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses are issued for each concurrent user working on a specified product
	line.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court,, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com
	Registered in England No. 3711047; VAT No. GB 729 1583 15

	http://www.HighIntegritySystems.com
*/


/* ORIGINAL WITTENSTEIN MACROS. The CALSENSE version ONLY saves the VFP REGISTERS IF A
   CONTEXT SWITCH ACTUALLY OCCURRED*/

/* When switching out a task, if the task tag contains a buffer address then
save the flop context into the buffer. */
#define traceTASK_SWITCHED_OUT()											\
	if( pxCurrentTCB->pxTaskTag != NULL )									\
	{																		\
		extern void vPortSaveVFPRegisters( void * );						\
		vPortSaveVFPRegisters( ( void * ) ( pxCurrentTCB->pxTaskTag ) );	\
	}

/* When switching in a task, if the task tag contains a buffer address then
load the flop context from the buffer. */
#define traceTASK_SWITCHED_IN()												\
	if( pxCurrentTCB->pxTaskTag != NULL )									\
	{																		\
		extern void vPortRestoreVFPRegisters( void * );						\
		vPortRestoreVFPRegisters( ( void * ) ( pxCurrentTCB->pxTaskTag ) );	\
	}


// 1/8/2013 rmd : Macro to provide a meaningful name when taking over the task tag for use
// during context switching to direct where to store the VFP registers (including the FPSCR
// register). This macro also initializes the register storage location by reading a copy of
// the actual registers first into the storage location. This is important to prevent
// initial corruption of the registers we are saving/restoring on context switch. You see
// the first time a task executes, any task with a task tag set, it restores the registers
// from the storage location. Not yet being used, that storage loaction contains potentially
// fatal data. If the storage location is in intialized RAM it contains 0's on startup. If
// it is in internal on-chip SRAM the storage array contains who knows what! And it is
// possible to write to the FPSCR register in such a way that upon execution of the first
// VFP instruction we suffer a fatal exception! I have demonstrated it by initializing the
// register storage of FF. When initialized to 00 everything works but that is NOT the
// default FPSCR value I see. So this macro here first reads the VFP registers to intialize
// the proposed context switch storage locations. An important step prior to switching in a
// task that uses them.

// ----------

// 1/8/2013 rmd : UPDATE - in actuality it is only necessary to have an INITIALIZED stored
// copy of the FPSCR register for the FIRST task (using the task tag) that switches IN. In
// our application this will be the app_startup task. However for coding consistancy sakes
// we'll use this macro for all tasks when making the task VFP capable.
#define OS_make_task_VFP_capable( task_handle, reg_storage_ptr )					\
			extern void vPortSaveVFPRegisters( void * );							\
			/* Initialize the storage location by reading the registers now. */		\
			vPortSaveVFPRegisters( (void*)reg_storage_ptr );						\
			/* Set the tag in the TCB */											\
			vTaskSetApplicationTaskTag( task_handle, (void*)reg_storage_ptr );
		
// ----------

/*
// NO LONGER USED : provide an alternative and more meaningful name to use when defining the
// application task tag register bank.
#define vTaskSetApplicationTaskTag( xTaskHandle, xFlopRegisterBank ) vTaskUsesFLOP( xTaskHandle, xFlopRegisterBank ) 
*/ 

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifdef UNTESTED_CALSENSE_MACROS
   THE UNTESTED EVER SO SLIGHTLY MORE EFFICIENT CALSENSE WAY. But this macro can be called from within an ISR
   (via portYIELD_FROM_ISR) so I got nervous and decided not to do it this way. Due to the use of the global
	xTaskThatWasRunning variable.

// defined in port.c
extern void*	xTaskThatWasRunning;

/* When switching out a task, if the task tag contains a buffer address then
save the flop context into the buffer. */
#define traceTASK_SWITCHED_OUT()											\
	xTaskThatWasRunning = pxCurrentTCB;


/* When switching in a task, if the task tag contains a buffer address then
load the flop context from the buffer. */
#define traceTASK_SWITCHED_IN()																\
	if( pxCurrentTCB != (tskTCB*)xTaskThatWasRunning )										\
    {                                                                                   	\
		/* A context switch has occurred, do something. */									\
		if( ((tskTCB*)xTaskThatWasRunning)->pxTaskTag != NULL )								\
		{																					\
			extern void vPortSaveVFPRegisters( void * );									\
			vPortSaveVFPRegisters( (void*)( ((tskTCB*)xTaskThatWasRunning)->pxTaskTag ) );	\
		}																					\
		if( pxCurrentTCB->pxTaskTag != NULL )												\
		{																					\
			extern void vPortRestoreVFPRegisters( void * );									\
			vPortRestoreVFPRegisters( ( void * ) ( pxCurrentTCB->pxTaskTag ) );				\
		}																					\
	}
end of calsense way
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

