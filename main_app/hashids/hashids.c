/******************************************************************************/ 
/******************************************************************************/ 
/******************************************************************************/ 
//  
// 10/14/2018 ajv 
// MUCH OF THIS CODE WAS COPIED FROM https://hashids.org/ AND HAS NOT BEEN ADEQUATELY 
// VERIFIED TO ENSURE IT'S HANDLED PROPERLY IN THE FREERTOS ENVIRONMENT. FOR EXAMPLE, THE 
// AUTHOR OF THE VERSION USED HERE INDICATES THE ENCODING PROCESS IS *NOT* THREAD SAFE! 
// IT IS ALSO OUTDATED (CURRENT VERSION AS OF 10/14/2018 is 1.2.0)
//  
// WARNING!!! DO NOT MAKE ANY CHANGES TO HASHIDS FUNCTIONS WITHOUT REVIEWING THE C# 
// EQUIVALENT THAT IS USED TO GENERATE THE CODE ON THE PC. DOING SO MAY CREATE AN 
// INCOMPATIBILITY. 
//
/******************************************************************************/ 
/******************************************************************************/ 
/******************************************************************************/ 

/* (... from README.md)
 
## Some Documentation

A small C library to generate Youtube-like IDs from one or many numbers.
Use __Hashids__ when you don't want to expose your database IDs to the user.

Read more about the library at [http://hashids.org/c/](http://hashids.org/c/).
 
### API 

The C version of Hashids works (presumably, and only) with `unsigned long long` arguments.
Negative (signed) integers are, by design, treated as very big unsigned ones.

Note: The API has changed a bit since version `1.1.0`. `hashids_t` is now a type alias of `struct hashids_t`.
 
*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/17/2017 ajv : Required for strlen
#include	<string.h>

// 10/14/2018 ajv : Required for ceil and floor
#include	<math.h>

#include	"hashids.h"

#include	"alerts.h"



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* fast ceil(x / y) for size_t arguments */
static inline size_t
hashids_div_ceil_size_t( size_t x, size_t y )
{
	return( x / y + !!(x % y) );
}

/* shuffle loop step */
#define hashids_shuffle_step(iter) \
    if (i == 0) { break; }                                      \
    if (v == salt_length) { v = 0; }                            \
    p += salt[v]; j = (salt[v] + v + p) % (iter);               \
    temp = str[(iter)]; str[(iter)] = str[j]; str[j] = temp;    \
    --i; ++v;

/* consistent shuffle */
static void
hashids_shuffle( char *str, size_t str_length, char *salt, size_t salt_length )
{
	size_t i;   //BE++ was: ssize_t i;
	size_t j, v, p;
	char temp;

    /* meh, meh */
	if( !salt_length )
	{
		return;
	}

    /* pure evil : loop unroll */
	for( i = str_length - 1, v = 0, p = 0; i > 0; /* empty */)
	{
		switch( i % 32 )
		{
			case 31:
				hashids_shuffle_step( i );
			case 30:
				hashids_shuffle_step( i );
			case 29:
				hashids_shuffle_step( i );
			case 28:
				hashids_shuffle_step( i );
			case 27:
				hashids_shuffle_step( i );
			case 26:
				hashids_shuffle_step( i );
			case 25:
				hashids_shuffle_step( i );
			case 24:
				hashids_shuffle_step( i );
			case 23:
				hashids_shuffle_step( i );
			case 22:
				hashids_shuffle_step( i );
			case 21:
				hashids_shuffle_step( i );
			case 20:
				hashids_shuffle_step( i );
			case 19:
				hashids_shuffle_step( i );
			case 18:
				hashids_shuffle_step( i );
			case 17:
				hashids_shuffle_step( i );
			case 16:
				hashids_shuffle_step( i );
			case 15:
				hashids_shuffle_step( i );
			case 14:
				hashids_shuffle_step( i );
			case 13:
				hashids_shuffle_step( i );
			case 12:
				hashids_shuffle_step( i );
			case 11:
				hashids_shuffle_step( i );
			case 10:
				hashids_shuffle_step( i );
			case  9:
				hashids_shuffle_step( i );
			case  8:
				hashids_shuffle_step( i );
			case  7:
				hashids_shuffle_step( i );
			case  6:
				hashids_shuffle_step( i );
			case  5:
				hashids_shuffle_step( i );
			case  4:
				hashids_shuffle_step( i );
			case  3:
				hashids_shuffle_step( i );
			case  2:
				hashids_shuffle_step( i );
			case  1:
				hashids_shuffle_step( i );
			case  0:
				hashids_shuffle_step( i );
		}
	}
}

/* common init */
hashids_t*
hashids_init3( const char *salt, size_t min_hash_length, const char *alphabet, hashids_t *p_result )
{
	hashids_t *result;
	size_t i, j, len;
	char ch, *p;

	// allocate the structure
	result = p_result;

	/* allocate enough space for the alphabet */
	len = strlen( alphabet ) + 1;

	/* extract only the unique characters */
	result->alphabet[ 0 ] = '\0';
	for( i = 0, j = 0; i < len; ++i )
	{
		ch = alphabet[ i ];
		if( !strchr( result->alphabet, ch ) )
		{
			result->alphabet[ j++ ] = ch;
		}
	}
	result->alphabet[ j ] = '\0';

	/* store alphabet length */
	result->alphabet_length = j;

	/* check length and whitespace */
	if( result->alphabet_length < HASHIDS_MIN_ALPHABET_LENGTH )
	{
		Alert_Message( "Hashids: Alphabet is too short" );
		return NULL;
	}
	if( strchr( result->alphabet, 0x20 ) || strchr( result->alphabet, 0x09 ) )
	{
		Alert_Message( "Hashids: Alphabet contains whitespace characters" );
		return NULL;
	}

	/* copy salt */
	result->salt_length = salt ? strlen( salt ) : 0;
	strncpy( result->salt, salt, result->salt_length );

	/* allocate enough space for separators */
	len = strlen( HASHIDS_DEFAULT_SEPARATORS );
	j = (size_t)
		(ceil( (float)result->alphabet_length / HASHIDS_SEPARATOR_DIVISOR ) + 1);
	if( j < len + 1 )
	{
		j = len + 1;
	}

	/* take default separators out of the alphabet */
	for( i = 0, j = 0; i < strlen( HASHIDS_DEFAULT_SEPARATORS ); ++i )
	{
		ch = HASHIDS_DEFAULT_SEPARATORS[ i ];

		/* check if separator is actually in the used alphabet */
		if( (p = strchr( result->alphabet, ch )) )
		{
			result->separators[ j++ ] = ch;

			/* remove that separator */
			memmove( p, p + 1,
					 strlen( result->alphabet ) - (p - result->alphabet) );
		}
	}

	/* store separators length */
	result->separators_count = j;

	/* subtract separators count from alphabet length */
	result->alphabet_length -= result->separators_count;

	/* shuffle the separators */
	if( result->separators_count )
	{
		hashids_shuffle( result->separators, result->separators_count,
						 result->salt, result->salt_length );
	}

	/* check if we have any/enough separators */
	if( !result->separators_count
		|| (((float)result->alphabet_length / (float)result->separators_count)
			> HASHIDS_SEPARATOR_DIVISOR) )
	{
		size_t separators_count = (size_t)ceil(
			(float)result->alphabet_length / HASHIDS_SEPARATOR_DIVISOR );

		if( separators_count == 1 )
		{
			separators_count = 2;
		}

		if( separators_count > result->separators_count )
		{
			/* we need more separators - get some from alphabet */
			size_t diff = separators_count - result->separators_count;
			strncat( result->separators, result->alphabet, diff );
			memmove( result->alphabet, result->alphabet + diff,
					 result->alphabet_length - diff + 1 );

			result->separators_count += diff;
			result->alphabet_length -= diff;
		}
		else
		{
			/* we have more than enough - truncate */
			result->separators[ separators_count ] = '\0';
			result->separators_count = separators_count;
		}
	}

	/* shuffle alphabet */
	hashids_shuffle( result->alphabet, result->alphabet_length,
					 result->salt, result->salt_length );

	/* allocate guards */
	result->guards_count = hashids_div_ceil_size_t( result->alphabet_length,
													HASHIDS_GUARD_DIVISOR );

	{
		/* take them from alphabet */
		strncpy( result->guards, result->alphabet, result->guards_count );
		memmove( result->alphabet, result->alphabet + result->guards_count,
				 result->alphabet_length - result->guards_count + 1 );

		result->alphabet_length -= result->guards_count;
	}

	/* set min hash length */
	result->min_hash_length = min_hash_length;

    /* return result happily */
	return( result );
}

/* encode many (generic) */
size_t
hashids_encode( hashids_t *hashids, char *buffer,
				size_t numbers_count, unsigned long long *numbers )
{
	size_t i, j, result_len, guard_index, half_length_ceil, half_length_floor;
	unsigned long long number, number_copy, numbers_hash;
	int p_max;
	char lottery, ch, temp_ch, *p, *buffer_end, *buffer_temp;

	/* copy the alphabet into internal buffer 1 */
	strncpy( hashids->alphabet_copy_1, hashids->alphabet,
			 hashids->alphabet_length );

	/* walk arguments once and generate a hash */
	for( i = 0, numbers_hash = 0; i < numbers_count; ++i )
	{
		number = numbers[ i ];
		numbers_hash += number % (i + 100);
	}

	/* lottery character */
	lottery = hashids->alphabet[ numbers_hash % hashids->alphabet_length ];

	/* start output buffer with it (or don't) */
	buffer[ 0 ] = lottery;
	buffer_end = buffer + 1;

	/* alphabet-like buffer used for salt at each iteration */
	hashids->alphabet_copy_2[ 0 ] = lottery;
	hashids->alphabet_copy_2[ 1 ] = '\0';
	strncat( hashids->alphabet_copy_2, hashids->salt,
			 hashids->alphabet_length - 1 );
	p = hashids->alphabet_copy_2 + hashids->salt_length + 1;
	p_max = hashids->alphabet_length - 1 - hashids->salt_length;
	if( p_max > 0 )
	{
		strncat( hashids->alphabet_copy_2, hashids->alphabet,
				 p_max );
	}
	else
	{
		hashids->alphabet_copy_2[ hashids->alphabet_length ] = '\0';
	}

	for( i = 0; i < numbers_count; ++i )
	{
		/* take number */
		number = number_copy = numbers[ i ];

		/* create a salt for this iteration */
		if( p_max > 0 )
		{
			strncpy( p, hashids->alphabet_copy_1, p_max );
		}

		/* shuffle the alphabet */
		hashids_shuffle( hashids->alphabet_copy_1, hashids->alphabet_length,
						 hashids->alphabet_copy_2, hashids->alphabet_length );

		/* hash the number */
		buffer_temp = buffer_end;
		do
		{
			ch = hashids->alphabet_copy_1[ number % hashids->alphabet_length ];
			*buffer_end++ = ch;
			number /= hashids->alphabet_length;
		} while( number );

		/* reverse the hash we got */
		for( j = 0; j < (buffer_end - buffer_temp) / 2; ++j )
		{
			temp_ch = *(buffer_temp + j);
			*(buffer_temp + j) = *(buffer_end - 1 - j);
			*(buffer_end - 1 - j) = temp_ch;
		}

		if( i + 1 < numbers_count )
		{
			number_copy %= ch + i;
			*buffer_end = hashids->separators[ number_copy % hashids->separators_count ];
			++buffer_end;
		}
	}

	/* intermediate string length */
	result_len = buffer_end - buffer;

	if( result_len < hashids->min_hash_length )
	{
		/* add a guard before the encoded numbers */
		guard_index = (numbers_hash + buffer[ 0 ]) % hashids->guards_count;
		memmove( buffer + 1, buffer, result_len );
		buffer[ 0 ] = hashids->guards[ guard_index ];
		++result_len;

		if( result_len < hashids->min_hash_length )
		{
			/* add a guard after the encoded numbers */
			guard_index = (numbers_hash + buffer[ 2 ]) % hashids->guards_count;
			buffer[ result_len ] = hashids->guards[ guard_index ];
			++result_len;

			/* pad with half alphabet before and after */
			half_length_ceil = hashids_div_ceil_size_t(
				hashids->alphabet_length, 2 );
			half_length_floor = floor( (float)hashids->alphabet_length / 2 );

			/* pad, pad, pad */
			while( result_len < hashids->min_hash_length )
			{
				/* shuffle the alphabet */
				strncpy( hashids->alphabet_copy_2, hashids->alphabet_copy_1,
						 hashids->alphabet_length );
				hashids_shuffle( hashids->alphabet_copy_1,
								 hashids->alphabet_length, hashids->alphabet_copy_2,
								 hashids->alphabet_length );

				/* left pad from the end of the alphabet */
				i = hashids_div_ceil_size_t(
					hashids->min_hash_length - result_len, 2 );
				/* right pad from the beginning */
				j = floor( (float)(hashids->min_hash_length - result_len) / 2 );

				/* check bounds */
				if( i > half_length_ceil )
				{
					i = half_length_ceil;
				}
				if( j > half_length_floor )
				{
					j = half_length_floor;
				}

				/* handle excessively excessive excess */
				if( (i + j) % 2 == 0 && hashids->alphabet_length % 2 == 1 )
				{
					++i;
					--j;
				}

				/* move the current result to "center" */
				memmove( buffer + i, buffer, result_len );
				/* pad left */
				memmove( buffer,
						 hashids->alphabet_copy_1 + hashids->alphabet_length - i, i );
				/* pad right */
				memmove( buffer + i + result_len, hashids->alphabet_copy_1, j );

				/* increment result_len */
				result_len += i + j;
			}
		}
	}

	buffer[ result_len ] = '\0';
	return( result_len );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
