/****************************************************************************
*
*            Copyright (c) 2008-2010 by HCC Embedded
*
* This software is copyrighted by and is the sole property of
* HCC.  All rights, title, ownership, or other interests
* in the software remain the property of HCC.  This
* software may only be used in accordance with the corresponding
* license agreement.  Any unauthorized use, duplication, transmission,
* distribution, or disclosure of this software is expressly forbidden.
*
* This Copyright notice may not be removed or modified without prior
* written consent of HCC.
*
* HCC reserves the right to modify this software without notice.
*
* HCC Embedded
* Budapest 1133
* Vaci ut 76.
* Hungary
*
* Tel:  +36 (1) 450 1302
* Fax:  +36 (1) 450 1303
* http: www.hcc-embedded.com
* email: info@hcc-embedded.com
*
***************************************************************************/
#include "usbd_api.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Number of hardware endpoints for the LPC2468. */
#define NO_OF_HW_EP     32

/* Set this to 1 to use the dedicated device port on the 
   board. 0 means using the OTG port. */
#define USE_EA_DEVICE_PORT 1

#define USB_DEVICE_ISR     2

int usbd_lcp_port_init(void);
int usbd_lcp_port_delete(void);
void usbd_lpc_port_pup_on_off(hcc_u8 on);


#ifdef __cplusplus
}
#endif

/****************************** END OF FILE **********************************/
