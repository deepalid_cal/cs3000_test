/****************************************************************************
*
*            Copyright (c) 2008-2010 by HCC Embedded
*
* This software is copyrighted by and is the sole property of
* HCC.  All rights, title, ownership, or other interests
* in the software remain the property of HCC.  This
* software may only be used in accordance with the corresponding
* license agreement.  Any unauthorized use, duplication, transmission,
* distribution, or disclosure of this software is expressly forbidden.
*
* This Copyright notice may not be removed or modified without prior
* written consent of HCC.
*
* HCC reserves the right to modify this software without notice.
*
* HCC Embedded
* Budapest 1133
* Vaci ut 76.
* Hungary
*
* Tel:  +36 (1) 450 1302
* Fax:  +36 (1) 450 1303
* http: www.hcc-embedded.com
* email: info@hcc-embedded.com
*
***************************************************************************/
#include <assert.h>
#include "lpc3250_regs.h"
#include "usbdi.h"
#include "usbd_lpc_config.h"
#include "os.h"
#include "hcc_types.h"
#include <string.h>

#if USBD_STD_MAJOR != 7
#error "Invalis USBD_STD version."
#endif

#if USBD_MAJOR != 6
#error "Invalid USBD version."
#endif

#define DEBUG_TRACE 0

#if DEBUG_TRACE==1
  #define MK_DBGTRACE(evt, inf) (dbg_htraces[trace_hndx].event=(evt), dbg_htraces[trace_hndx++].info=(inf))
#else
  #define MK_DBGTRACE(evt, inf)      ((void)0)
#endif

#define MIN(a, b)  ((a)<(b) ? (a) : (b))

#define LEP2PEP(lep,t)	(((lep)<<1)+(!!(t)))
#define PEP2LEP(pep)	((pep)>>1)

#define USB_CMDCODE_WR(val)\
  {\
    USBCMDCODE = val;\
    while ((USBDEVINTST & USBDEVINTCLR_CCEMPTY)==0)\
      ;\
    USBDEVINTCLR = USBDEVINTCLR_CCEMPTY;\
  }

#define USB_CMDDATA_RD(val)\
  {\
    while((USBDEVINTST & USBDEVINTCLR_CDFULL) == 0)\
      ;\
    val=USBCMDDATA;\
    USBDEVINTCLR = USBDEVINTCLR_CDFULL;\
  }

/* Device status bits */
#define DEVST_CON        (1<<0)
#define DEVST_CON_CH    (1<<1)
#define DEVST_SUS        (1<<2)
#define DEVST_SUS_CH    (1<<3)
#define DEVST_RST        (1<<4)

/* Endpoint status bits */
#define EPST_FE            (1<<0)
#define EPST_ST            (1<<1)
#define EPST_STP        (1<<2)
#define EPST_PO            (1<<3)
#define EPST_EPN        (1<<4)
#define EPST_B1_FULL    (1<<5)
#define EPST_B2_FULL    (1<<6)

/* Endpoint set status bits */
#define EPST_SET_ST        (1<<0)
#define EPST_SET_DA        (1<<5)
#define EPST_SET_RF_MO     (1<<6)
#define EPST_SET_CND_ST    (1<<7)

#define EP_OUT            0
#define EP_IN            1

/*****************************************************************************
 * Execute protocol command on the USB module.
 *****************************************************************************/
#define PCMD_SET_ADDRESS                0x0
#define PCMD_CONFIGURE_DEVICE           0x1
#define PCMD_SET_MODE                   0x2
#define PCMD_READ_CURRENT_FRAME_NUMBER  0x3
#define PCMD_READ_TEST_REGISTER         0x4
#define PCMD_SET_DEVICE_STATUS          0x5
#define PCMD_GET_DEVICE_STATUS          0x6
#define PCMD_GET_ERROR_CODE             0x7
#define PCMD_READ_ERROR_STATUS          0x8
#define PCMD_SELECT_ENDPOINT            0x9
#define PCMD_SELECT_ENDPOINT_CLI        0xa
#define PCMD_SET_ENDPOINT_STATUS        0xb
#define PCMD_CLEAR_BUFFER               0xc
#define PCMD_VALIDATE_BUFFER            0xd


typedef enum {
  evt_none=0,
  evt_get_stall,
  evt_clr_stall,
  evt_set_stall,
  evt_set_addr,
  evt_send_ok,
/* 6-10*/
  evt_receive_ok,
  evt_abort,
  evt_add_ep,
  evt_remove_ep,
  evt_hw_init,
/*11-15*/
  evt_def_stat,
  evt_hw_start,
  evt_hw_stop,
  evt_hw_delete,
  evt_it,
/*16-20*/
  evt_tx_packet,
  evt_rx_packet,
  evt_stp_host_abort,
  evt_stp_bad_length,
  evt_got_stp,
/*21-25*/
  evt_rx_host_abort,
  evt_rx_short_packet,
  evt_tx_short_packet,
  evt_dis_irq,
  evt_enb_tx_buf_2,
/*26-30*/
  evt_stall_it,
  evt_stat_it,
  evt_ep_it,
  evt_rx_overrun,
  evt_rxit_ep_stopped,
/*31- */
  evt_error_it
} dbg_evt;

static hcc_u8 usb_state_bf_suspend;
static volatile hcc_u8 setup_packet_received=0;

static hcc_u8 in_interrupt=0;

static union {
     hcc_u32 align;
     hcc_u8 data[8];
   } setup_data;

static hcc_u8 pep2gndx[NO_OF_HW_EP];

#if DEBUG_TRACE==1
volatile struct {
  dbg_evt event;
  hcc_u32 info;
} dbg_htraces[0x100];

hcc_u8 trace_hndx=0;
#endif


#define IS_DOUBLE_BUFFERED(pep) (pep2buff[pep] != 0xff)
/*                                             0     1     2     3     4     5     6      7     8    9    10    11    12    13    14    15 */
static const hcc_u8 pep2buff[NO_OF_HW_EP]={ 0xff, 0xff, 0xff, 0xff,    0, 0xff,    1,  0xff, 0xff, 0xff,   2, 0xff,    3, 0xff, 0xff, 0xff,
/*                                     16     17    18    19    20    21    22    23     24    25    26    27   28    29    30     31*/
                                        4,  0xff,    5, 0xff, 0xff, 0xff,    6, 0xff,     7, 0xff, 0xff, 0xff,   8, 0xff,    9,  0xff};
static void enter_default_state(void);

static hcc_u16 exec_cmd(hcc_u8 cmd, hcc_u8 pep, hcc_u8 data)
{
  hcc_u32 tmp=0, tmp1=0;
  if(!in_interrupt)
  {
    os_int_disable();
  }

  switch (cmd)
  {
    case PCMD_SET_ADDRESS:
      USB_CMDCODE_WR(0x00D00500ul);
      USB_CMDCODE_WR(0x00000100ul | (((hcc_u32)data) << 16));
      break;

    case PCMD_CONFIGURE_DEVICE:
      USB_CMDCODE_WR(0x00d80500ul);
      USB_CMDCODE_WR(0x00000100ul | (((hcc_u32)data) << 16));
      break;

    case PCMD_SET_MODE:
      USB_CMDCODE_WR(0x00f30500ul);
      USB_CMDCODE_WR(0x00000100ul | (((hcc_u32)data) << 16));
      break;

    case PCMD_READ_CURRENT_FRAME_NUMBER:
      USB_CMDCODE_WR(0x00f50500ul);
      USB_CMDCODE_WR(0x00f50200ul | (((hcc_u32)data) << 16));
      USB_CMDDATA_RD(tmp);
      USB_CMDCODE_WR(0x00f50200ul | (((hcc_u32)data) << 16));
      USB_CMDDATA_RD(tmp1);
      tmp |= tmp1<<8;
      break;

    case PCMD_READ_TEST_REGISTER:
      USB_CMDCODE_WR(0x00fd0500ul);
      USB_CMDCODE_WR(0x00fd0200ul | (((hcc_u32)data) << 16));
      USB_CMDDATA_RD(tmp);
      USB_CMDCODE_WR(0x00fd0200ul | (((hcc_u32)data) << 16));
      USB_CMDDATA_RD(tmp1);
      tmp |= tmp1<<8;
      break;

    case PCMD_SET_DEVICE_STATUS:
      USB_CMDCODE_WR(0x00fe0500ul);
      if (data & 1)
      {
        tmp1=1;
      }
      USB_CMDCODE_WR(0x00000100ul | (((hcc_u32)data) << 16));
      break;

    case PCMD_GET_DEVICE_STATUS:
      USB_CMDCODE_WR(0x00fe0500ul);
      USB_CMDCODE_WR(0x00fe0200ul);
      USB_CMDDATA_RD(tmp);
      break;

    case PCMD_GET_ERROR_CODE:
      USB_CMDCODE_WR(0x00ff0500ul);
      USB_CMDCODE_WR(0x00ff0200ul);
      USB_CMDDATA_RD(tmp);
      break;

    case PCMD_READ_ERROR_STATUS:
      USB_CMDCODE_WR(0x00fb0500ul);
      USB_CMDCODE_WR(0x00fb0200ul);
      USB_CMDDATA_RD(tmp);
      break;

    case PCMD_SELECT_ENDPOINT:
      USB_CMDCODE_WR(0x00000500ul + (((hcc_u32)pep) << 16));
      USB_CMDCODE_WR(0x00000200ul + (((hcc_u32)pep) << 16));
      USB_CMDDATA_RD(tmp);
      break;

    case PCMD_SELECT_ENDPOINT_CLI:
      USBDEVINTCLR = USBDEVINTCLR_CDFULL;
      USB_CMDCODE_WR(0x00400500ul + (((hcc_u32)pep) << 16));
      USB_CMDCODE_WR(0x00400200ul + (((hcc_u32)pep) << 16));
      USB_CMDDATA_RD(tmp);
      break;

    case PCMD_SET_ENDPOINT_STATUS:
      USB_CMDCODE_WR(0x00400500ul + ((((hcc_u32)pep) << 16) & 0xff0000));
      USB_CMDCODE_WR(0x00000100ul | ((((hcc_u32)data) << 16) & 0xff0000));
      break;

    case PCMD_CLEAR_BUFFER:
      USB_CMDCODE_WR(0x00f20500ul);
      USB_CMDCODE_WR(0x00f20200ul);
      USB_CMDDATA_RD(tmp);
      break;

    case PCMD_VALIDATE_BUFFER:
      USB_CMDCODE_WR(0x00fa0500ul);
      break;
  }
  if(!in_interrupt)
  {
    os_int_restore();
  }
  return tmp;
}

void usbd_get_setup_data(usbd_setup_data_t *stp)
{
  stp->bmRequestType=setup_data.data[0];
  stp->bRequest=setup_data.data[1];
  stp->wValue=setup_data.data[2] | (setup_data.data[3]<<8);
  stp->wIndex=setup_data.data[4] | (setup_data.data[5]<<8);
  stp->wLength=setup_data.data[6] | (setup_data.data[7]<<8);
}

int usbd_at_high_speed(void)
{
  return 0;
}

hcc_u16 usbd_get_frame_ctr(void)
{
  return exec_cmd(PCMD_READ_CURRENT_FRAME_NUMBER, 0, 0);
}

/*****************************************************************************
 * Sets the endpoint tx state to stall.
 *****************************************************************************/
static void usb_stop_ep_tx(int pep)
{
  exec_cmd(PCMD_SET_ENDPOINT_STATUS, pep, EPST_SET_ST);
}

/*****************************************************************************
 * Sets the endpoint rx state to stall.
 *****************************************************************************/
static void usb_stop_ep_rx(int pep)
{
  exec_cmd(PCMD_SET_ENDPOINT_STATUS, pep, EPST_SET_ST);
}


/*****************************************************************************
 * Name:
 *    usbd_set_stall
 * In:
 *    ndx          - endpoint index
 *
 * Out:
 *    N/A
 * Description:
 *    A call to this function will make the endpoint to stall any request
 *    till usb_clr_halt() is not called. Usb reset, or device reconfiguration
 *    will clear the halt feature.
 *****************************************************************************/
void usbd_set_stall(int ndx)
{
  int endx=usbd_ep_list[ndx].hw_info.pep;
  MK_DBGTRACE(evt_set_stall, endx);
  if (endx>2)
  {
    if (endx & 1)
    {
      usb_stop_ep_rx(endx);
    }
    else
    {
      usb_stop_ep_tx(endx);
    }
  }
  else
  {
    usb_stop_ep_rx(1);
	usb_stop_ep_tx(0);
  }
}

void usbd_clr_stall(int ndx)
{
  int endx=usbd_ep_list[ndx].hw_info.pep;
  if (endx>2)
  {
    /* TODO: after clearing stall we are not able to
       start the next IN transfre on the LPC2468.
       Other LPC:s can be affected too.
       LPC2478 works fine. */
    exec_cmd(PCMD_SET_ENDPOINT_STATUS, endx, 0);
  }
}

int usbd_get_stall(int ndx)
{
  int endx=usbd_ep_list[ndx].hw_info.pep;
  return exec_cmd(PCMD_SELECT_ENDPOINT, endx, 0) & EPST_ST;
}

void usbd_set_addr_pre(hcc_u8 daddr)
{
  exec_cmd(PCMD_SET_ADDRESS, 0, 0x80|daddr);

  /* empty */
}

void usbd_set_addr_post(hcc_u8 daddr)
{
  (void)daddr;
}

void usbd_set_cfg_pre(void)
{
  /* empty */
     exec_cmd(PCMD_CONFIGURE_DEVICE, 0, 0);            /* set configured state */
}

void usbd_set_cfg_post(void)
{
    exec_cmd(PCMD_CONFIGURE_DEVICE, 0, 1);            /* set configured state */

}

static void copy_to_ep(hcc_u8 ep, hcc_u8 *src, hcc_u16 length)
{
  hcc_u32 w;
  int i;

  /* Select Endpoint */
  USBCTRL = (PEP2LEP(ep)<<2) | USBCTRL_WR_EN;
  /* Set packet length. */
  USBTXPLEN = length;


  for(i=0;i<length;i+=4)
  {
      w=src[0];
      w|=(src[1] << 8);
      w|=(src[2] << 16);
      w|=(src[3] << 24);
      USBTXDATA=w;
      src+=4;
   }
    USBCTRL = 0;
}

int usbd_send(usbd_transfer_t *tr)
{
  int length;
  hcc_u8 *src_ptr;
  hcc_u32 left;

  usbd_ep_info_t *eph=usbd_ep_list+USBD_EPH_NDX(tr->eph);
  hcc_u8 pep=eph->hw_info.pep;
  volatile hcc_u8 epst;

  if (!pep) pep=1;		/* set physical address back on control EP */
  MK_DBGTRACE(evt_send_ok, pep);

  tr->state=USBDTRST_BUSY;

  if (setup_packet_received && pep == 1)
  {
    os_int_disable();
    epst=exec_cmd(PCMD_SELECT_ENDPOINT, 0, 0);
    exec_cmd(PCMD_CLEAR_BUFFER, 0, 0);
    setup_packet_received = 0;
    os_int_restore();
  }
  /* Number of bytes left from transfer. */
  left=tr->length-tr->csize;
  /* Length of data packet to be sent. */
  length=MIN(left, eph->psize);

  /* Pointer to packet data. */
  src_ptr=(hcc_u8*)(tr->buffer+tr->csize);
  /* Fill buffer */

  os_int_disable();
  exec_cmd(PCMD_SELECT_ENDPOINT, pep, 0);
  copy_to_ep(pep, src_ptr, length);

  /* Ready just filled buffer. */

  exec_cmd(PCMD_VALIDATE_BUFFER, 0, 0);
  os_int_restore();
  return(0);
}

static void usb_copy_from_ep(hcc_u8 ep, hcc_u8* dst, int length)
{
  hcc_u32 tmp=0;
  int i;
  exec_cmd(PCMD_SELECT_ENDPOINT, ep, 0);
  if (length == 0)
  {
    USBCTRL = 0;
    return;
  }
  for(i=0; i<(length>>2); i++)
  {
    tmp=USBRXDATA;
    dst[0]=tmp&0xff;
    dst[1]=(tmp>>8)&0xff;
    dst[2]=(tmp>>16)&0xff;
    dst[3]=(tmp>>24)&0xff;
    dst+=4;
  }
  length-=(i<<2);
  if (length)
  {
    tmp=USBRXDATA;
  }
  while (length)
  {
    *dst++=tmp&0xff;
    tmp>>=8;
    --length;
  }
  USBCTRL = 0;
}

int usbd_receive(usbd_transfer_t *tr)
{
  hcc_u8 epst;
  hcc_u8 event;
  hcc_u8 pep=usbd_ep_list[USBD_EPH_NDX(tr->eph)].hw_info.pep;
  usbd_ep_info_t *eph=usbd_ep_list+USBD_EPH_NDX(tr->eph);

  event=0;
  os_int_disable();

  /* if the transfer already ended, notify common part */
  if ((tr->length !=0 && tr->length <= tr->csize) || tr->state==USBDTRST_SHORTPK)
  {
    if (tr->state!=USBDTRST_SHORTPK)
    {
      tr->state=USBDTRST_CHK;
    }
    event=1;
  }
  else
  {
    /* The handshake transaction is dropped in the interrupt handler. The hanshake
	   transfer will allways be ended succesfully. */
    if (USBDTRDIR_HS_IN==tr->dir)
	{
	  tr->state=USBDTRST_CHK;
	  event=1;
	}
    else if(IS_DOUBLE_BUFFERED(pep) || pep<2)
    {
      int length=0;
      /* See if some packets have been received while the endpoint had no active
         transfer. */
      if (eph->hw_info.buff_len>0 )
      {
        /* This must only happen at the start of the transfer. */
        assert(tr->csize==0);
        /* Clear todo counter since we will handle stored events now. */
        eph->hw_info.todo_ctr=0;
        /* Wait till USB module tells us the packet length. */
        epst=exec_cmd(PCMD_SELECT_ENDPOINT, pep, 0);
        /* See if endpoint buffer is not empty. */
        if ((epst & EPST_FE) != 0)
        {
          USBCTRL = (PEP2LEP(pep)<<2) | USBCTRL_RD_EN;
          do
          {
            length = USBRXPLEN;
          } while((length & USBRXPLEN_PKT_RDY) == 0);
          length&=USBRXPLEN_LEN_MASK;

          if (length > tr->length-tr->csize)
          {
            MK_DBGTRACE(evt_rx_overrun, pep);
            tr->state=USBDTRST_COMM;
            eph->hw_info.stopped=1;
          }
          else
          {
            usb_copy_from_ep(pep, ((hcc_u8*)tr->buffer)+tr->csize, length);
            tr->csize+=length;
            /* If the transfer is not ended, re-enable interrupt operation. */
            if (tr->length > tr->csize)
            {
              eph->hw_info.stopped=0;
            }
            else
            {
              eph->hw_info.stopped=1;
            }
          }
          eph->hw_info.buff_len = 0;
          exec_cmd(PCMD_CLEAR_BUFFER, 0, 0);
        }
        else
        {
          /* If the buffer is empty, then we have an error. The only possible
             reason is a bus reset while the endpoint had no active TR. Even in
             this situation a race condition shall end badly to get here.
             Return with error. */
          eph->hw_info.stopped=1;
          os_int_restore();
          return 1;
        }

        if ( length!= eph->psize)
        {
          tr->state=USBDTRST_SHORTPK;
          eph->hw_info.stopped=1;
          event=1;
        }
        else
        {
          tr->state=USBDTRST_CHK;
          event=1;
        }

      }
      else
      {
        /* Check if interrupt wanted to inform common part about a reception.
           If no, set transfer state to busy. As a result the common part will
           suspend processing (wait for an event).*/
        if (0==eph->hw_info.todo_ctr)
        {
          tr->state=USBDTRST_BUSY;
        }
        else
        {
          /* If there is a pending notifycation, set the state to make the common
             part re-check transfer status. */
          if (tr->state!=USBDTRST_SHORTPK
              && tr->state!=USBDTRST_COMM)
          {
            tr->state=USBDTRST_CHK;
            /* if this is ep0, then the rx buffer needs to bee freed up */
            if (pep<2)
            {
              /* Wait till USB module tells us the packet length. */
              epst=exec_cmd(PCMD_SELECT_ENDPOINT, pep, 0);
              if (epst & EPST_FE)
              {
                exec_cmd(PCMD_CLEAR_BUFFER, 0, 0);
              }
            }
          }
          event=1;
          eph->hw_info.todo_ctr--;
        }

        if (tr->length > tr->csize)
        {
          eph->hw_info.stopped=0;
        }
        else
        {
          eph->hw_info.stopped=1;
        }
      }
    }
    else /* not double buffered */
    {
      MK_DBGTRACE(evt_receive_ok, pep);
      epst=exec_cmd(PCMD_SELECT_ENDPOINT, pep, 0);
      if (epst & EPST_FE)
      {
        exec_cmd(PCMD_CLEAR_BUFFER, 0, 0);
      }

      tr->state=USBDTRST_BUSY;
      eph->hw_info.stopped=0;
    }
  }

  os_int_restore();

  if(event && NULL != tr->event)
  {
    os_event_set(*tr->event);
  }
  return 0;
}

int usbd_abort(usbd_ep_handle_t ep)
{
  hcc_u8 pep=usbd_ep_list[USBD_EPH_NDX(ep)].hw_info.pep;

  MK_DBGTRACE(evt_abort, pep);
  usb_stop_ep_rx(pep);
  return(OS_SUCCESS);
}

int usbd_add_ep(int ndx)
{
  usbd_ep_info_t *eph=usbd_ep_list+ndx;
  hcc_u8 pep=LEP2PEP(eph->addr & 0x7f, (eph->addr & 0x80));
  assert(eph->psize);

  os_int_disable();

  pep2gndx[pep]=ndx;
  if (pep<2)
  {
    pep2gndx[1]=ndx;
  }

  eph->hw_info.stopped=1;
  eph->hw_info.pep=pep;
  eph->hw_info.buff_len=0;
  eph->hw_info.todo_ctr=0;
  MK_DBGTRACE(evt_add_ep, 0);

  /* Add a new endpoint. */
  USBREEP|=1<<pep;
  /* Select the rigth max packet size register. */
  USBEPIND = pep;
  /* Set max packet size. */
  USBMAXPSIZE = eph->psize;
  exec_cmd(PCMD_SET_ENDPOINT_STATUS, pep, 0);
  /* Wait for hardware to realize endpoint. */
  while ((USBDEVINTST & USBDEVINTST_EP_RLZED) == 0)
    ;
  USBDEVINTCLR = USBDEVINTST_EP_RLZED;
  /* Clear any pending endpoint interrupt. */
  USBEPINTCLR = 1<<pep;
  /* Enable endpoint interrupt. */
  USBEPINTEN |= (1<<pep) | 0x3;

  os_int_restore();
  /* Enable control ep interrupt */
  return(OS_SUCCESS);
}

void usbd_remove_ep(int ndx)
{
  hcc_u8 pep=usbd_ep_list[ndx].hw_info.pep;

  MK_DBGTRACE(evt_remove_ep, pep);

  USBEPINTEN &= ~(1<<pep);
  exec_cmd(PCMD_SET_ENDPOINT_STATUS, pep, EPST_SET_DA);
  USBREEP&=~(1<<pep);

  pep2gndx[pep]=0xff;
  usbd_ep_list[ndx].hw_info.pep=0;
  usbd_ep_list[ndx].hw_info.stopped=1;
}

OS_ISR_FN(usbd_it_handler);
int usbd_hw_init(void)
{
  MK_DBGTRACE(evt_hw_init, 0);

  return(OS_SUCCESS);
}

static void enter_default_state(void)
{
  int pep;
  MK_DBGTRACE(evt_def_stat, 0);
  /* Clear all pending USB interrupts. */
  USBDEVINTCLR = 0xffffffff;

  for (pep=2; pep<NO_OF_HW_EP; pep++)
  {
    if (pep2gndx[pep] != 0xff)
    {
      usbd_remove_ep(pep2gndx[pep]);
    }
  }
  USBEPINTEN = 0;
  /* Disable function endpoints, exit adressed and configured state. */
  exec_cmd(PCMD_CONFIGURE_DEVICE, 0, 0);			/* go to non-configured state */
  /* Set address to 0. */
  exec_cmd(PCMD_SET_ADDRESS, 0, 0);
  /* This call assumes usbd_std_start() already configured EP0 in
    the global endpoint list. */
  usbd_add_ep(0);

  usb_state_bf_suspend=USBDST_DEFAULT;
}

int usbd_hw_start(void)
{
  MK_DBGTRACE(evt_hw_start, 0);

  memset(pep2gndx, 0xff, sizeof(pep2gndx));

  os_isr_init(USB_DEVICE_ISR,usbd_it_handler);

  usbd_lcp_port_init();  

  /****** Configure USB interrupt */
  /* Clear all pending interrupts. */
  USBEPINTCLR = 0xffffffff;
  USBDEVINTCLR = 0x3ff;
  USBDEVINTPRI = 0;
  USBEPINTPRI = 0;

  exec_cmd(PCMD_SET_MODE, 0, 0);

 /* Reset USB. */
  exec_cmd(PCMD_SET_DEVICE_STATUS, 0, DEVST_RST);

  enter_default_state();
 /* Connect USB to USB pins. */
  exec_cmd(PCMD_SET_DEVICE_STATUS, 0, DEVST_CON);

  /* Enable some USB interrupts. */
  USBDEVINTEN = USBDEVINTEN_DEV_STAT
    | USBDEVINTEN_EP_SLOW | USBDEVINTEN_EP_FAST;

  os_isr_enable(USB_DEVICE_ISR);
  usbd_lpc_port_pup_on_off(1);

  return(OS_SUCCESS);
}

int usbd_hw_stop(void)
{
  MK_DBGTRACE(evt_hw_stop, 0);

  os_isr_disable(USB_DEVICE_ISR);
  usbd_lpc_port_pup_on_off(0);

  /* Reset the USB module. */
  exec_cmd(PCMD_SET_DEVICE_STATUS, 0, DEVST_RST);
  /* wait some time in case init is called quickly again */
  {
    volatile long to=100000;
    while (to--)
	  ;
  }

  return(OS_SUCCESS);
}

int usbd_hw_delete(void)
{
  MK_DBGTRACE(evt_hw_delete, 0);
  usbd_lcp_port_delete();
  return(OS_SUCCESS);
}

static void handle_rx_ev(usbd_ep_info_t *eph)
{
  int length;
  hcc_u8 epst;
  hcc_u8 pep=eph->hw_info.pep;
  usbd_transfer_t *tr=eph->tr;

  USBEPINTCLR = 1<<pep;
  USB_CMDDATA_RD(epst);

  if ((epst&EPST_STP) && !setup_packet_received)  /* SETUP packet received */
  {
    assert(pep==0);
    eph->hw_info.stopped=1;
    if (tr)
    {
      /* If host aborts ongoing transfer by sending next setup packet. */
      MK_DBGTRACE(evt_stp_host_abort, (hcc_u32)tr);
      tr->state=USBDTRST_COMM;
      if (NULL != tr->event)
      {
        os_event_set_int(*tr->event);
      }
    }
    USBCTRL = (PEP2LEP(pep)<<2) | USBCTRL_RD_EN;
    /* Wait till USB module tells us the packet length. */
    do
    {
      length = USBRXPLEN;
    } while((length & USBRXPLEN_PKT_RDY) == 0);

    length&=USBRXPLEN_LEN_MASK;
    if (length != 8)
    {
      MK_DBGTRACE(evt_stp_bad_length, 0);
      usb_copy_from_ep(0, setup_data.data, 0);
      usbd_set_stall(0);
    }
    else
    {
      MK_DBGTRACE(evt_got_stp, 0);
      usb_copy_from_ep(0, setup_data.data, 8);
      os_event_set_int(usbd_stprx_event);
    }
    exec_cmd(PCMD_CLEAR_BUFFER, 0, 0);
    return;
  }

  USBCTRL = (PEP2LEP(pep)<<2) | USBCTRL_RD_EN;
  do
  {
    length = USBRXPLEN;
  } while((length & USBRXPLEN_PKT_RDY) == 0);

  length&=USBRXPLEN_LEN_MASK;

  if  (pep!=0 && epst&EPST_ST)   /* Stalled */
  {
    MK_DBGTRACE(evt_stall_it, pep);
    return;
  }

 /* see if endpoint is stopped */
  if (eph->hw_info.stopped)
  {
    MK_DBGTRACE(evt_rxit_ep_stopped, pep);
    /* Dropp handshakes on ep0. */
    if (pep<2 && length==0)
    {
      usb_copy_from_ep(0,0,0);
    }
    else
    {
      eph->hw_info.buff_len=length;
    }
    return;
  }

  if (pep == 0 && tr->dir == USBDTRDIR_IN)
  {
    MK_DBGTRACE(evt_rx_host_abort, 0);
    if (length != 0)
    {
      usbd_set_stall(0);
    }
    /* Do this to clear the interrupt. */
    usb_copy_from_ep(0, 0, 0);
    tr->state=USBDTRST_COMM;
    if (NULL != tr->event)
    {
      os_event_set_int(*tr->event);
    }
  }
  else
  {
    usb_copy_from_ep(pep, ((hcc_u8*)tr->buffer)+tr->csize, length);
    tr->csize+=length;

    if(IS_DOUBLE_BUFFERED(pep))
    {
      exec_cmd(PCMD_CLEAR_BUFFER, 0, 0);
    }

    if (length != eph->psize)
    {
      MK_DBGTRACE(evt_rx_short_packet, pep);
      tr->state=USBDTRST_SHORTPK;
      /* Stopp further processing if transfer ended. */
      eph->hw_info.stopped=1;
    }
    else
    {
      tr->state=USBDTRST_CHK;
      if (tr->csize>=tr->length)
      {
        /* Stop further processing if TR ended. */
        eph->hw_info.stopped=1;
      }
    }

    eph->hw_info.todo_ctr++;
    if (NULL != tr->event)
    {
      os_event_set_int(*tr->event);
    }
  }
}

static void handle_tx_ev(usbd_ep_info_t *eph)
{
  hcc_u8 pep=eph->hw_info.pep;
  usbd_transfer_t *tr=eph->tr;
  hcc_u16 psize;

  // Because I compile with -Wall GCC directive this flags as an unused variable. So I've commented it out.
  //volatile hcc_u8 epst;

if (NULL==tr)
{
  return;
}
  MK_DBGTRACE(evt_tx_packet, pep);
  if (pep==0) pep=1;

  USBEPINTCLR=1<<(pep);
  /* Advance transfer status. */
  psize=MIN(tr->length-tr->csize, eph->psize);
  tr->csize+=psize;
  if (psize != eph->psize)
  {
    MK_DBGTRACE(evt_tx_short_packet, (hcc_u8)pep);
    tr->state=USBDTRST_SHORTPK;
  }
  else
  {
    tr->state=USBDTRST_CHK;
  }

  if (tr->dir == USBDTRDIR_HS_OUT)
  {
    MK_DBGTRACE(evt_dis_irq, pep);
    USBEPINTEN |= (1<<(pep));
  }

  if (NULL != tr->event)
  {
    os_event_set_int(*tr->event);
  }
}

OS_ISR_FN(usbd_it_handler)
{
  hcc_u32 endp_istr;
  hcc_u32 devintst;

  /* Save irq USB status. Note: ISR bits are
     not directly routed to the AIC. Thus
     ISR must be masked with IMR to avoid
     process masked interrupts if another
     interrupt source strikes. */
  devintst=USBDEVINTST;
  MK_DBGTRACE(evt_it, devintst);

  in_interrupt=1;	
  if (devintst & USBDEVINTST_ERR_INT)
  {
#if DEBUG_TRACE
    hcc_u8 err_stat;
    err_stat=exec_cmd(PCMD_READ_ERROR_STATUS, 0, 0);
    MK_DBGTRACE(evt_error_it, err_stat);
    err_stat=exec_cmd(PCMD_GET_ERROR_CODE, 0, 0);
    MK_DBGTRACE(evt_error_it, err_stat);
#endif
    USBDEVINTCLR = USBDEVINTST_ERR_INT;
  }

  if (devintst & USBDEVINTST_EP_SLOW)
  {
    endp_istr=USBEPINTST;
    MK_DBGTRACE(evt_ep_it, endp_istr);

    while (endp_istr)
    {
      hcc_u8 ep;
      int mask=1;

      for(ep=0; ep<NO_OF_HW_EP;ep++, mask<<=1)
      {
        if (endp_istr & mask)
        {
          break;
        }
      }
      assert(ep<NO_OF_HW_EP);
      endp_istr &= ~mask;

      assert(pep2gndx[ep] != 0xff);
      if ((ep & 1) == 0 )
      {/* This is an RX packet.*/
        handle_rx_ev(usbd_ep_list+pep2gndx[ep]);
      }
      else
      {
        handle_tx_ev(usbd_ep_list+pep2gndx[ep]);
      }
    }
    USBDEVINTCLR = USBDEVINTST_EP_SLOW;
  }

  /* If there is a status change. */
  if (devintst & USBDEVINTST_DEV_STAT)
  {
    hcc_u8 state;
    /* Clear IT flag. */
    USBDEVINTCLR = USBDEVINTST_DEV_STAT;
    state=exec_cmd(PCMD_GET_DEVICE_STATUS, 0, 0);
    MK_DBGTRACE(evt_stat_it, state);
    if (state & DEVST_RST)
    {
      usbd_bus_state_chg_event(USBDST_DEFAULT);
      goto isr_end;
    }
    if (state & DEVST_SUS_CH)
    {
      /* We are suspended. */
      if (state & DEVST_SUS)
      {
        usb_state_bf_suspend=usbd_state;
        usbd_bus_state_chg_event(USBDST_SUSPENDED);
      }
      else if (USBDST_SUSPENDED==usbd_state)
      {/* Wake up. */
        usbd_bus_state_chg_event(usb_state_bf_suspend);
      }
    }

    /* Connection state changed. */
    if (state & DEVST_CON_CH)
    {
      if ((state&DEVST_CON)==0)
      {
        usbd_state=USBDST_DEFAULT;
      }
    }
    goto isr_end;
  }

  isr_end:
    ;
  in_interrupt=0;
}
/****************************** END OF FILE **********************************/
