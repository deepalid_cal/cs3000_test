/****************************************************************************
*
*            Copyright (c) 2008-2010 by HCC Embedded
*
* This software is copyrighted by and is the sole property of
* HCC.  All rights, title, ownership, or other interests
* in the software remain the property of HCC.  This
* software may only be used in accordance with the corresponding
* license agreement.  Any unauthorized use, duplication, transmission,
* distribution, or disclosure of this software is expressly forbidden.
*
* This Copyright notice may not be removed or modified without prior
* written consent of HCC.
*
* HCC reserves the right to modify this software without notice.
*
* HCC Embedded
* Budapest 1133
* Vaci ut 76.
* Hungary
*
* Tel:  +36 (1) 450 1302
* Fax:  +36 (1) 450 1303
* http: www.hcc-embedded.com
* email: info@hcc-embedded.com
*
***************************************************************************/
#include "lpc3250_regs.h"


#include	"lpc32xx_gpio.h"

#include	"gpio_setup.h"

#include	"lpc32xx_intc_driver.h"

#include	"cs_common.h"


#include "lpc3250_otg_i2c.h"
#include "usbd_lpc_config.h"
#include "usbdi.h"
#include "LPC3250_chip.h"

/* Clear and set bits with one write. */
#define SET_PINSEL(reg, clrmask, nevwal)\
   (reg=((reg) & ~(clrmask)) | nevwal)


static UNS_32 bd_count;

void usb_vbus_detect_isr( void )
{
	// THIS IS AN ISR

	// Clear the edge interrupt - the function that decides which interrupt to handle (prvExecuteHighestPriorityISR)
	// actually handles the clearing of edge triggered interrupts. So we do not have to do it here.
	//SIC2_RSR |= SIC2_RSR_GPI_06_MASK;

	bd_count += 1;
}



int usbd_lcp_port_init()
{
	
	bd_count = 0;

	/* Enable USB clocks. */
	USBCLKDIV  = 0x0C;
	USBCLKCTRL = (1 << 24) | (1 << 22) | (2 << 19) | 
	(1 << 17) | (1 << 16) | (1 << 11) | 
	(0 << 9) | (191 << 1);
	while ((USBCLKCTRL & 1)==0);
	USBCLKCTRL |= (1 << 18); /* Enable USB clock */
	
	// ONLY route the UART5 TX/RX to the U5_TX and U5_RX pins. Do not also route to the USB D+ and D- pins.
	UART_CTRL &= ~(1 << 0);
	
	otg_i2c_init();
	
	#ifdef NOT_FOR_MAXIM_CHIP
		/* check if ISP1301 is connected */
		{
		hcc_u8 vl=0,vh=0;
		vl=otg_i2c_read(0x00);
		vh=otg_i2c_read(0x01);
		if ((((hcc_u16)vh<<8)|((hcc_u16)vl))!=0x04cc) return 1;
		vl=otg_i2c_read(0x02);
		vh=otg_i2c_read(0x03);
		if ((((hcc_u16)vh<<8)|((hcc_u16)vl))!=0x1301) return 1;
		}
		
		/* Clear mode 1 register. */
		otg_i2c_write(0x05,0xfe);
		/* set speed mode to full speed. */
		otg_i2c_write(0x04,0x5);
		
		/* clear mode control 2 register */
		otg_i2c_write(0x13,0xff);
		/* set bi-directional mode for the SE0/VM pin and psw_oe */
		otg_i2c_write(0x12,0x46);
		
		/* enter idls OTG state, disable pull-ups, pull-downs, etc... */
		otg_i2c_write(0x07,0xff);
		
		/* clear all interrupts */
		otg_i2c_write(0x0b,0xff);
		/* disable all interrupts */
		otg_i2c_write(0x0d,0xff);
		otg_i2c_write(0x0f,0xff);
	#endif
	
	
	// Make sure interrupts are OFF for the channel in the MIC, enable the CLK, assign the interrupt vector
	xDisable_ISR( GPI_06_INT );
	
	// assign the isr vector - DOES NOT ENABLE THE INTERRUPT
	xSetISR_Vector( GPI_06_INT, INTERRUPT_PRIORITY_usb_bd, ISR_TRIGGER_POSITIVE_EDGE, usb_vbus_detect_isr, NULL );
	
	// ENABLE INTERRUPT IN THE MIC FOR THIS CHANNEL
	xEnable_ISR( GPI_06_INT );
	
	
	return( OS_SUCCESS );
}

void usbd_lpc_port_pup_on_off(hcc_u8 on)
{
	GPIO_REGS_T		*Lgpio;

	Lgpio = (GPIO_REGS_T*)GPIO_BASE;

	if ( on )
	{
		//otg_i2c_write(6, 1);  for ISP1301
		Lgpio->p0_outp_set = ( USB_ENUM );  // setting ENUM high connects the MAX13345E D+ pullup to VBUS.
	}
	else
	{
		/* Remove D+ pull-up through ISP1301 */
		//otg_i2c_write(7, 1);
		Lgpio->p0_outp_clr = ( USB_ENUM );  // setting ENUM low disconnects the MAX13345E D+ pullup from VBUS.
	}
}

int usbd_lcp_port_delete(void)
{
  usbd_lpc_port_pup_on_off(0);
  return(OS_SUCCESS);
}

/****************************** END OF FILE **********************************/
