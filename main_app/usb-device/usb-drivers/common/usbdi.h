/****************************************************************************
 *
 *            Copyright (c) 2008-2010 by HCC Embedded
 *
 * This software is copyrighted by and is the sole property of
 * HCC.  All rights, title, ownership, or other interests
 * in the software remain the property of HCC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of HCC.
 *
 * HCC reserves the right to modify this software without notice.
 *
 * HCC Embedded
 * Budapest 1133
 * Vaci ut 76.
 * Hungary
 *
 * Tel:  +36 (1) 450 1302
 * Fax:  +36 (1) 450 1303
 * http: www.hcc-embedded.com
 * email: info@hcc-embedded.com
 *
 ***************************************************************************/
#ifndef _USBDI_H_
#define _USBDI_H_

#include "usbd_api.h"
#include "usbdi_std.h"
#include "usbdi_dev.h"

#if USBD_SOFTMR_SUPPORT
#include "sof_timeri.h"
#endif

/*  USB device stack internal common declarations.

    This file contains declarations common for all USB
    peripheral (device) drivers. These declarationsh shall only be used
    in other driver modules but not in class-drivers or user applications. */

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  vbst_off,
  vbst_to_on,
  vbst_to_off,
  vbst_on
} usbd_vbus_state_t;

typedef enum {
  rst_stop,
  rst_run,
  rst_to_run,
  rst_to_stop
} usbd_run_state_t;

/* USB transfer state values. Used by the low-level layer to
           talk to the common part. */
#define USBDTRST_DONE        0     /* Transfer ended. */
#define USBDTRST_BUSY        1     /* Low-level is busy. */
#define USBDTRST_CHK         2     /* Check status. */
#define USBDTRST_SHORTPK     3     /* Ended with short packet. */
#define USBDTRST_EP_KILLED   4     /* Failed, endpoint gone. */
#define USBDTRST_COMM        5     /* Communication error. */

/* Transfer direction values */
#define USBDTRDIR_SETUP     2  /* Setup transaction */
#define USBDTRDIR_HS_IN     3  /* Hand-shake in (rx 0 length packet) */
#define USBDTRDIR_HS_OUT    4  /* Hand-shake out (tx 0 length packet) */

/* Endpoint handle management. */
#define USBD_EPH_AGE(eph)   ((hcc_u8)((eph)>>8))  /* Endpoint handle -> age. */
#define USBD_EPH_NDX(eph)   ((hcc_u8)((eph) & 0x00ff)) /* Endpoint handle -> index*/
#define USBD_EPH_CREATE(age, ndx)  (usbd_ep_handle_t)(((age)<<8) | (ndx)) /* Cerate a handle value from the age and index. */

/* Endpoint list */
extern usbd_ep_info_t usbd_ep_list[];
/* Synchronization object to make reconfiguration of an endpoints
           atomic.*/
extern OS_MUTEX_TYPE *usbd_cfg_mutex;

extern usbd_vbus_state_t usbd_vbus_state;

extern usbd_run_state_t usbd_run_state;

#ifdef __cplusplus
}
#endif

#endif
/****************************** END OF FILE **********************************/


