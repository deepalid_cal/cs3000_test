/****************************************************************************
 *
 *            Copyright (c) 2008-2010 by HCC Embedded
 *
 * This software is copyrighted by and is the sole property of
 * HCC.  All rights, title, ownership, or other interests
 * in the software remain the property of HCC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of HCC.
 *
 * HCC reserves the right to modify this software without notice.
 *
 * HCC Embedded
 * Budapest 1133
 * Vaci ut 76.
 * Hungary
 *
 * Tel:  +36 (1) 450 1302
 * Fax:  +36 (1) 450 1303
 * http: www.hcc-embedded.com
 * email: info@hcc-embedded.com
 *
 ***************************************************************************/
#ifndef _USBD_H_
#define _USBD_H_

/*  World-wide common declarations.

    This file contains world wide declarations common for all USB
    peripheral (device) drivers. */

/*Major Version number of the module.  */
#define USBD_MAJOR  6
/*Minor version number of the module. */
#define USBD_MINOR  0

#include "usbd_dev.h"
#include "usbd_config.h"
#include "os.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 ************************ Type definitions ************************************
 *****************************************************************************/
/*Endpoint identifyer type. */
typedef hcc_u16 usbd_ep_handle_t;

/* Type of return value for most API functions. */
typedef int usbd_error_t;

/* USB transfer descriptor.
    This structure is used to describe/manage an USB transfer. */
typedef struct {
  usbd_ep_handle_t eph;      /* Endpoint handle. */
  hcc_u8 *buffer;            /* Data buffer*/
  hcc_u32 length;            /* Number of bytes to send/receive */
  hcc_u32 csize;             /* Number of bytes already sent/received */
  hcc_u32 slength;           /* Used when sending zero length packet. */
  hcc_u32 scsize;            /* Used when sending zero length packet. */
  OS_EVENT_BIT_TYPE *event;  /* Event to notify caller when transfer ends */
  volatile int state;        /* State of transfer */
  hcc_u8 dir;                /* Direction of transfer (#USBDTRDIR_IN #USBDTRDIR_OUT) */
  hcc_u8 zp_needed;          /* Set to one if transfer shall be closed with short packet. */
} usbd_transfer_t;

/******************************************************************************
 ************************ Macro definitions ***********************************
 *****************************************************************************/
/* Error codes. */
#define USBDERR_NONE        0      /* no error */
#define USBDERR_BUSY        1      /* endpoint is busy */
#define USBDERR_INVALIDEP   3      /* invalid endpoint */
#define USBDERR_NOTREADY    5      /* transfer can not be started */
#define USBDERR_INTERNAL    6      /* internal error (shall only happen
                                        during development) */
#define USBDERR_COMM        7      /* communication error */

/* Transfer direction values. */
#define USBDTRDIR_IN        0      /* peripheral to host */
#define USBDTRDIR_OUT       1      /* host to peripheral */

/******************************************************************************
 ************************ Inported functions **********************************
 *****************************************************************************/
/* Pull-up control call-back.
  Called by the driver to enable or disable the pull-up resistor. Some
  implementations hawe on-chip pull-up resistor. In this case this function
  will never be called. */
void usbd_pup_on_off(int on);

/******************************************************************************
 ************************ Exported functions **********************************
 *****************************************************************************/
/* Inicialize.
    This function will allocate all dynamic resources (events, mutexes, etc)
    and preconfigure the hardware for normal operation. This functiol calls
    the _init() function of all USB modules. */
int usbd_init(void);
/* Start operating.

    This function enables normal driver operation.
       - pull-up resistor is enabled to make the host detect the device
       - processing of standard requests is enabled
       - enable USB related interrupt generation */
int usbd_start(usbd_config_t *conf);
/* Stop operating.

    Stop normal operation.
       -disable pull-up resistor (host detects device removal)
       -processing of standard requests is stopped
       -USB related interrupt generation is disabled
       -USB hardware is out to low-power mode if possible */
int usbd_stop(void);
/* Kill driver.

    Free allocated resources. Call to any other driver function than usbd_init()
    is illegal after this function is called. */
int usbd_delete(void);
/* Query current driver state.

    This function returns USBDST_XXX values. Please see the documentation of
    these for more details. */
int usbd_get_state(void);

/* Start a non-blocking transfer.

    Using this function a transfer can be requested to be done on the USB bus.
    The function will "enque" the request and return and will not wayt till the
    transfer is done.
    The return value can be used to determine if the transfer is successfully
    enqued or not. If yes, the event specifyed in the transfer descriptor
    will be set if the transfer end due to any reason. If the transfer can not
    be enqued the event will newer be set.*/
usbd_error_t usbd_transfer(usbd_transfer_t *tr);
/* Start a blocking transfer.

    Using this function a transfer can be requested to be done on the USB bus.
    The function will not return till the transfer is not finished and will
    block execution of the calling task.*/
usbd_error_t usbd_transfer_b(usbd_transfer_t *tr);
/* Query status of a transfer.

    This function is to be used with non-blocking transfers. It works well for
    blocking transfers too but uting it that way is pointless since the
    usbd_transfer_b() will return the same status as this call.
    This function allocates CPU time for transfer management and must be called
    by the transfer initiator (application or class-driver) when the event
    specifyed in the transfer descriptor becomes set. */
usbd_error_t usbd_transfer_status(usbd_transfer_t *tr);

/* Halt a bulk or interrupt endpoint.

    This function can be used to report an error to the USB host. After the call
    any communication to the specifyedendpoint will result in a STALL handshake
    and thus will fail with an error condition on the host side.*/
void usbd_set_halt(usbd_ep_handle_t ep);
/* Enable host to clear halted state.

    When the firmware is recovered the error condition due to the endpoint has
    been halted it shall call this function. This will enable the host to
    restart communication on the endpoint.*/
void usbd_clr_halt(usbd_ep_handle_t ep);
/* Check if endpoint is halted.

    A halted endpoint can be restarted ony by the host. This function can be
    used to see if the host restarted the endpoint or not. */
int usbd_get_halt(usbd_ep_handle_t ep);

/* Return current value of frame counter.

    While the bus is not suspended, the host is sending a SOF packet at the
    start of each frame. Any hadware counts the number of SOF:s seen on the
    bus. The counter is at least 11 bits wide.*/
hcc_u16 usbd_get_frame_ctr(void);

#if USBD_ISOCHRONOUS_SUPPORT
/* Attach a FIFO to an endpoint. */
int usbd_stream_attach(usbd_ep_handle_t ep, rngbuf_t *fifo);

/* Detach a FIFO from an endpoint. */
int usbd_stream_detach(usbd_ep_handle_t ep);
#endif

/* Tell current VBUS state to USB stack. When VBUS is detected this function
   shall be called with nonzero paramether value. When VBUS removed the function
   shall be called with paramether value 0. */
void usbd_vbus(int on);

#ifdef __cplusplus
}
#endif

#endif
/****************************** END OF FILE **********************************/


