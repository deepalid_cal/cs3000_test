/****************************************************************************
 *
 *            Copyright (c) 2009-2010 by HCC Embedded
 *
 * This software is copyrighted by and is the sole property of
 * HCC.  All rights, title, ownership, or other interests
 * in the software remain the property of HCC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of HCC.
 *
 * HCC reserves the right to modify this software without notice.
 *
 * HCC Embedded
 * Budapest 1133
 * Vaci ut 76.
 * Hungary
 *
 * Tel:  +36 (1) 450 1302
 * Fax:  +36 (1) 450 1303
 * http: www.hcc-embedded.com
 * email: info@hcc-embedded.com
 *
 ***************************************************************************/
#include <assert.h>
#include <string.h>

#include "sof_timeri.h"
#include "sof_timer_config.h"
#include "os.h"
#include "os_common.h"
#include "hcc_types.h"
#include "usbdi.h"

#define FRAME_CTR_MASK  ((1u<<11)-1)

#if SOFTMR_MAJOR != 1 || SOFTMR_MINOR != 2
#error "Invalid version numbers."
#endif

/* Timer event descriptor */
struct
{
  hcc_u32 due_time;
  hcc_u32 period;
  softmr_event_func_t cb;
  int cb_param;
  int used;
} event_list[SOFTMR_MAX_EVENTS];

hcc_u32 ctr;
hcc_u16 last_fctr_val;

OS_MUTEX_TYPE *softmr_mutex;

int softmr_init(void)
{
  ctr=0;
  memset(event_list, 0, sizeof(event_list));
  return os_mutex_create(&softmr_mutex);
}

int softmr_start(void)
{
  ctr=usbd_get_frame_ctr() & FRAME_CTR_MASK;
  last_fctr_val=(hcc_u16)ctr;
  return OS_SUCCESS;
}

int softmr_stop(void)
{
  int ndx;
  os_mutex_get(softmr_mutex);
  for(ndx=0; ndx<SOFTMR_MAX_EVENTS; ndx++)
  {
     if (event_list[ndx].used)
     {
       (*event_list[ndx].cb)(event_list[ndx].cb_param);
       event_list[ndx].used=0;
     }
  }
  os_mutex_put(softmr_mutex);

  return OS_SUCCESS;
}

int softmr_delete(void)
{
  return os_mutex_delete(softmr_mutex);
}


int softmr_add_event(softmr_event_func_t cb_fn, int cb_param, hcc_u32 period)
{
  int ndx;

  os_mutex_get(softmr_mutex);

  for(ndx=0; ndx<SOFTMR_MAX_EVENTS; ndx++)
  {
    if (0==event_list[ndx].used)
    {
      event_list[ndx].period=period;
      event_list[ndx].due_time=ctr+period;
      event_list[ndx].cb=cb_fn;
      event_list[ndx].cb_param=cb_param;
      event_list[ndx].used=1;
      break;
    }
  }
  os_mutex_put(softmr_mutex);

  if (ndx<SOFTMR_MAX_EVENTS)
  {
    return ndx;
  }

  return SOFTMR_INVALID_EVENT;
}

int softmr_del_event(softmr_event_handle_t evh)
{
  if (SOFTMR_INVALID_EVENT == evh)
  {
    return OS_ERR;
  }
  event_list[evh].used=0;
  return OS_SUCCESS;
}

int softmr_reset_event(softmr_event_handle_t evh)
{
  if (SOFTMR_INVALID_EVENT == evh)
  {
    return OS_ERR;
  }
  event_list[evh].due_time=ctr+event_list[evh].period;
  return OS_SUCCESS;
}

void softmr_tick(hcc_u16 sof_ctr)
{
  int ndx;

  /* Increment mS counter. */
  ctr+=FRAME_CTR_MASK & (sof_ctr-last_fctr_val);
  last_fctr_val=sof_ctr;

  /* Check for due events. */
  for(ndx=0; ndx<SOFTMR_MAX_EVENTS; ndx++)
  {
     if (event_list[ndx].used
         && event_list[ndx].due_time < ctr)
     {
       event_list[ndx].due_time+=event_list[ndx].period;
       (*event_list[ndx].cb)(event_list[ndx].cb_param);
     }
  }
}

/****************************** END OF FILE **********************************/
