/****************************************************************************
 *
 *            Copyright (c) 2008-2010 by HCC Embedded
 *
 * This software is copyrighted by and is the sole property of
 * HCC.  All rights, title, ownership, or other interests
 * in the software remain the property of HCC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of HCC.
 *
 * HCC reserves the right to modify this software without notice.
 *
 * HCC Embedded
 * Budapest 1133
 * Vaci ut 76.
 * Hungary
 *
 * Tel:  +36 (1) 450 1302
 * Fax:  +36 (1) 450 1303
 * http: www.hcc-embedded.com
 * email: info@hcc-embedded.com
 *
 ***************************************************************************/
#include <assert.h>
#include <string.h>
#include "usbdi.h"
#include "os.h"
#include "hcc_types.h"
#include "usbd_std_config.h"
#include "usbd_std.h"

#define DEBUG_TRACE  0

#if USBD_MAJOR != 6
#error "Invalis USBD version."
#endif

#if USBD_STD_MAJOR != 7 || USBD_STD_MINOR != 5
#error "Invalid USBD_STD version."
#endif

#if USBD_HWAPI_MAJOR != 4
#error "Incorrect low-level driver version."
#endif

#if OS_API_MAJOR != 2
#error "Incorrect OS API version."
#endif

/* This configures if remote wakeup is supported by the driver/device or not. */
#ifndef USBD_REMOTE_WAKEUP
#error "USBD_REMOTE_WAKEUP must be defined with value 0 or 1."
#endif

#if USBD_REMOTE_WAKEUP
hcc_u8 usbd_rw_en;
#endif

#define RD_CONFIG_LENGTH(c)  (((hcc_u8*)(c))[2] | (((hcc_u8*)(c))[3]<<8))
#define INVALID_DEV_ADDR 0x80
#define INVALID_CFG_VAL  0xff

#define BUILD_REQ16(rqt, rq)   ((hcc_u16)(((rqt) <<8) | (rq)))

#define MIN(a,b)  (((a) < (b)) ? (a) : (b))

#if DEBUG_TRACE==1
  #define MK_DBGTRACE(evt, inf) (dbg_stdtraces[trace_stdndx].event=(evt), dbg_stdtraces[trace_stdndx++].info=(inf))
#else
  #define MK_DBGTRACE(evt, inf)      ((void)0)
#endif

/* This type hods all information needed to manage class-drivers. */
typedef struct {
  usbd_cdrv_cb_t *cb;
  usbd_ep0_cb_t *ep0_cb;
  usbd_parse_cb_t *parse_cb;
  int param;
  hcc_u8 _class;
  hcc_u8 _sclass;
  hcc_u8 _proto;
} cdrv_info_t;


/* This event is set by the low-level-driver when a setup packet is received.
   The even is consumed by the usbd_ep0_task. */
OS_EVENT_BIT_TYPE usbd_stprx_event;

/* This even is set by the low-level and by the user app if state change is
   needed. The following state changes are flaged:
      -bus reset
      -suspend and resume
      -VBUS state change
      -stack start
      -stack stop
   The event is consumed by the usbd_control_task. */
OS_EVENT_BIT_TYPE usbd_state_event;

/* This event is used when doing transfers on ep0. The event is consumed by the
   usbd_ep0_task (or one of its call-back funstions).*/
OS_EVENT_BIT_TYPE ep0_event;

/* Information about endpoints. */
usbd_ep_info_t usbd_ep_list[MAX_NO_OF_EP];

/* Handle for EP0. */
usbd_ep_handle_t ep0_handle;

/* Pointer to the active configuration. This pointer is changed by usbd_start()*/
usbd_config_t *usbd_config;

/* The current state os the VBUS. usbd_vbus() and usbd_control_task is changing
   its value. */
usbd_vbus_state_t usbd_vbus_state;

/* The current run state os the stack. usbd_start() usbd_stop() and
   usbd_control_task is canging its value. */
usbd_run_state_t usbd_run_state;

/* This variable hods the current state of the stack. (See USBDST_XXXX values.)
   */
int usbd_state;
int usbd_state_chg;

static usbd_transfer_t ep0_tr;
static usbd_setup_data_t setup_data;
static hcc_u8 current_cfg;
static hcc_u8 new_addr;
static hcc_u16 state;
static cdrv_info_t cdrv_list[MAX_NO_OF_CDRVS];

static struct
{
  hcc_u8 as;
  usbd_ep_info_t *first_ep;
  usbd_cdrv_cb_t *old_cb;
} alt_setting_list[MAX_NO_OF_INTERFACES];

static hcc_u32 tmp_dsc_buf[64/4];

#if DEBUG_TRACE==1
typedef enum {
  evt_clr_halt,
  evt_set_halt,
  evt_kill_tr,
  evt_kill_fifo,
  evt_std_init,
  evt_std_start,
  evt_std_stop,
  evt_std_delete,
  evt_reg_cdrv,
  evt_findcdrv,
  evt_add_ep,
  evt_set_config,
  evt_bus_event,
  evt_stprx,
  evt_get_devdesc,
  evt_getcfg,
  evt_devqualif,
  evt_get_string,
  evt_get_config,
  evt_rwake_enabled,
  evt_set_addr,
  evt_set_cfg,
  evt_set_ifc,
  evt_get_ifc,
  evt_clrhalt_noep,
  evt_clrhalt_accept,
  evt_clrhalt,
  evt_sethalt,
  evt_sethalt_noep,
  evt_sethalt_ok,
  evt_gethalt_noep,
  evt_gethalt,
  evt_usrclb,
  evt_hshkin,
  evt_hshkout,
  evt_hshkerr,
  evt_remove_ep,
  evt_rwake_disabled
} dbg_stdevt;

volatile struct {
  dbg_stdevt event;
  hcc_u8 info;
} dbg_stdtraces[0x100];

hcc_u8 trace_stdndx=0;
#endif


static usbd_ep_info_t *add_ep(hcc_u16 psize, hcc_u8 ep_type, hcc_u8 addr
                                   , usbd_ep_info_t *prev_ep);
static int usbd_set_config(hcc_u8 cid);
static void remove_ep(int index);
static void do_state_cb(usbd_conn_state_t new_state);

/*****************************************************************************
 * Name:
 *    usbd_clr_halt
 * In:
 *    ep - endpoint handle
 * Out:
 *    n/a
 *
 * Description:
 *    Clear halted flag of an endpoint.
 *
 * Assumptions:
 *
 *****************************************************************************/
void usbd_clr_halt(usbd_ep_handle_t ep)
{
  int endx=USBD_EPH_NDX(ep);
  if (ep != USBD_INVALID_EP_HANDLE_VALUE
       && USBD_EPH_AGE(ep)== usbd_ep_list[endx].age)
  {
    usbd_ep_list[endx].halted=0;
    MK_DBGTRACE(evt_clr_halt, endx);
  }
}

/*****************************************************************************
 * Name:
 *    usbd_set_halt
 * In:
 *    ep - endpoint handle
 * Out:
 *    n/a
 *
 * Description:
 *    Set halted flag of an endpoint.
 *
 * Assumptions:
 *
 *****************************************************************************/
void usbd_set_halt(usbd_ep_handle_t ep)
{
  int endx=USBD_EPH_NDX(ep);
  if (ep != USBD_INVALID_EP_HANDLE_VALUE
       && USBD_EPH_AGE(ep)== usbd_ep_list[endx].age)
  {
    usbd_set_stall(endx);
    usbd_ep_list[endx].halted=1;
    MK_DBGTRACE(evt_set_halt, endx);

    /* If endpoint has an ongoing trnsfer, send error notification. */
    if (NULL != usbd_ep_list[endx].tr )
    {
      usbd_transfer_t *tr=usbd_ep_list[endx].tr;
      MK_DBGTRACE(evt_kill_tr, endx);
      tr->state=USBDTRST_COMM;
      if (tr->event)
      {
        os_event_set(*tr->event);
      }
    }
#if USBD_ISOCHRONOUS_SUPPORT
    else if (NULL != usbd_ep_list[endx].fifo)
    {
      MK_DBGTRACE(evt_kill_fifo, endx);
      usbd_drop_fifo(endx);
    }
#endif
  }
}

/*****************************************************************************
 * Name:
 *    usbd_get_halt
 * In:
 *    ep - endpoint handle
 * Out:
 *    0 - endpoint is not halted
 *    1 - endpoint is halted
 *
 * Description:
 *    Return halted status of an endpoiint
 *
 * Assumptions:
 *
 *****************************************************************************/
int usbd_get_halt(usbd_ep_handle_t ep)
{
  int r=0;
  int endx=USBD_EPH_NDX(ep);
  if (ep != USBD_INVALID_EP_HANDLE_VALUE
       && USBD_EPH_AGE(ep)== usbd_ep_list[endx].age)
  {
    r=usbd_ep_list[endx].halted || usbd_get_stall(endx);
    return(r);
  }
  return (r);
}

/*****************************************************************************
 * Name:
 *    usbd_std_init
 * In:
 *    n/a
 * Out:
 *    OS_SUCCESS
 *    OS_ERR
 *
 * Description:
 *    Init module.
 *
 * Assumptions:
 *
 *****************************************************************************/
int usbd_std_init(void)
{
  MK_DBGTRACE(evt_std_init, 0);
  current_cfg=0;
  new_addr=INVALID_DEV_ADDR;
  usbd_state=USBDST_DISABLED;
  usbd_state_chg=0;
#if USBD_REMOTE_WAKEUP
  usbd_rw_en=0;
#endif
  usbd_config=NULL;
  ep0_handle=USBD_INVALID_EP_HANDLE_VALUE;
  memset(cdrv_list, 0 , sizeof(cdrv_list));
  if (OS_SUCCESS != os_event_create(&ep0_event))
  {
    return(OS_ERR);
  }
  ep0_tr.event=&ep0_event;
  if (OS_SUCCESS != os_event_create(&usbd_stprx_event))
  {
    return(OS_ERR);
  }

  return(OS_SUCCESS);
}

/*****************************************************************************
 * Name:
 *    usbd_std_start
 * In:
 *    n/a
 * Out:
 *    OS_SUCCESS
 *    OS_ERR
 *
 * Description:
 *    Start module.
 *
 * Assumptions:
 *  usbd_config variable must be initialized before calling this function!
 *****************************************************************************/
int usbd_std_start(void)
{
  int ndx;

  MK_DBGTRACE(evt_std_start, 0);
  for(ndx=0; ndx<sizeof(usbd_ep_list)/sizeof(usbd_ep_list[0]); ndx++)
  {
  	usbd_ep_list[ndx].ep_type=0xff;
  }

  current_cfg=0;
  new_addr=0;

  (void)add_ep(usbd_config->device_descriptor[7], EPD_ATTR_CTRL, 0, NULL);

  usbd_state=USBDST_DEFAULT;

  return(OS_SUCCESS);
}

/*****************************************************************************
 * Name:
 *    usbd_std_stop
 * In:
 *    n/a
 * Out:
 *    OS_SUCCESS
 *    OS_ERR
 *
 * Description:
 *    Stop module.
 *
 * Assumptions:
 *
 *****************************************************************************/
int usbd_std_stop(void)
{
  MK_DBGTRACE(evt_std_stop, 0);
  usbd_set_config(0);
  remove_ep(0);

  new_addr=INVALID_DEV_ADDR;
  usbd_state=USBDST_DISABLED;
#if USBD_REMOTE_WAKEUP
  usbd_rw_en=0;
#endif
  ep0_handle=USBD_INVALID_EP_HANDLE_VALUE;

  return(OS_SUCCESS);
}

/*****************************************************************************
 * Name:
 *    usbd_std_delete
 * In:
 *    n/a
 * Out:
 *    OS_SUCCESS
 *    OS_ERR
 *
 * Description:
 *    Delete module.
 *
 * Assumptions:
 *
 *****************************************************************************/
int usbd_std_delete(void)
{
  MK_DBGTRACE(evt_std_delete, 0);
  (void)os_event_delete(usbd_stprx_event);
  (void)os_event_delete(ep0_event);
  return(OS_SUCCESS);
}

/*****************************************************************************
 * Name:
 *    usdb_register_cdrv
 * In:
 *    _class - ifc class
 *    sclass - ifc sub class
 *    proto  - ifc protocol
 *    drv_cb - class driver call back
 *    param  - class driver call back parameter
 *    ep0_cb - ep0 call back
 * Out:
 *    OS_SUCCESS
 *    OS_FAILURE
 *
 * Description:
 *    Register a new class driver.
 * Assumptions:
 *
 *****************************************************************************/
int usdb_register_cdrv(hcc_u8 _class, hcc_u8 sclass, hcc_u8 proto
                                , usbd_cdrv_cb_t *drv_cb, int param, usbd_ep0_cb_t *ep0_cb, usbd_parse_cb_t *parse_cb)
{
  int ndx;

  for(ndx=0; ndx<sizeof(cdrv_list)/sizeof(cdrv_list[0]); ndx++)
  {
    if (NULL==cdrv_list[ndx].cb)
    {
      cdrv_list[ndx]._class=_class;
      cdrv_list[ndx]._sclass=sclass;
      cdrv_list[ndx]._proto=proto;
      cdrv_list[ndx].param=param;
      cdrv_list[ndx].cb=drv_cb;
      cdrv_list[ndx].ep0_cb=ep0_cb;
      cdrv_list[ndx].parse_cb=parse_cb;
      MK_DBGTRACE(evt_reg_cdrv, 0);
      return(OS_SUCCESS);
    }
  }
  MK_DBGTRACE(evt_reg_cdrv, 1);
  return(OS_ERR);
}

/*****************************************************************************
 * Name:
 *    find_cdrv
 * In:
 *    _class - ifc class
 *    sclass - ifc sub class
 *    proto  - ifc protocol
 * Out:
 *    pointer to entry
 *    0 if not found
 *
 * Description:
 *    Find reg info for an interface
 * Assumptions:
 *
 *****************************************************************************/
static cdrv_info_t* find_cdrv(hcc_u8 _class, hcc_u8 sclass, hcc_u8 proto, const hcc_u8 *start, const hcc_u8 *end)
{
  int ndx;
  for(ndx=0; ndx<sizeof(cdrv_list)/sizeof(cdrv_list[0]); ndx++)
  {
    if (NULL != cdrv_list[ndx].cb
      && cdrv_list[ndx]._class==_class
      && cdrv_list[ndx]._sclass==sclass
      && cdrv_list[ndx]._proto==proto)
    {
      MK_DBGTRACE(evt_findcdrv, 0);
      if (NULL==cdrv_list[ndx].parse_cb || 0==(*cdrv_list[ndx].parse_cb)(start, end))
      {
        return(&cdrv_list[ndx]);
      }
    }
  }
  MK_DBGTRACE(evt_findcdrv, 1);
  return NULL;
}

/*****************************************************************************
 * Name:
 *    lang_supported
 * In:
 *    lng: USB spec defined language id
 * Out:
 *    index of language if found
 *    -1 otherwise
 *
 * Description:
 *    Check if the specified language is supported in the current config.
 * Assumptions:
 *
 *****************************************************************************/
static int lang_supported(hcc_u16 lng)
{
  int lndx;
  char *lang_p=(char *)(usbd_config->string_descriptor+2);
  for(lndx=0; lndx<usbd_config->number_of_languages; lndx++)
  {
    hcc_u16 clng=(hcc_u16)(*lang_p | (*(lang_p+1)<<8));
    if (clng==lng)
    {
      return(lndx);
    }
    lang_p+=2;
  }
  return(-1);
}

/*****************************************************************************
 * Name:
 *    find_string
 * In:
 *    ndx: string index
 *    lang: USB spec defined language id
 * Out:
 *    0 if error
 *    pointer to descriptor otherwise.
 *
 * Description:
 *    Return pointer to specified string descriptor.
 * Assumptions:
 *
 *****************************************************************************/
static hcc_u8 *find_string(hcc_u8 ndx, hcc_u16 lang)
{
  int lndx;

  if (ndx==0)
  {
    return((hcc_u8 *)usbd_config->string_descriptor);
  }

  if (usbd_config->number_of_strings < ndx)
  {
    return((hcc_u8 *)0);
  }

  ndx--;
  lndx=lang_supported(lang);
  if (lndx<0)
  {
    return((hcc_u8 *)0);
  }

  return((hcc_u8*)((usbd_config->strings[lndx])[ndx]));
}

/*****************************************************************************
 * Name:
 *    add_ep
 * In:
 *    psize: maximum packet size
 *    ep_type: endpoint type value
 *    addr: endpoint address
 *    prev_ep: index of previous endpoint
 * Out:
 *    ndx of allocated endpoint
 *
 * Description:
 *    Create a new endpoint.
 * Assumptions:
 *
 *****************************************************************************/
static usbd_ep_info_t *add_ep(hcc_u16 psize, hcc_u8 ep_type, hcc_u8 addr
                                     , usbd_ep_info_t *prev_ep)
{
  int ndx;

  /* Look for a free endpoint. */
  for(ndx=0;ndx<sizeof(usbd_ep_list)/sizeof(usbd_ep_list[0]); ndx++)
  {
  	if (0xff==usbd_ep_list[ndx].ep_type)
  	{
      usbd_ep_list[ndx].addr=addr;
      usbd_ep_list[ndx].psize=psize;
      usbd_ep_list[ndx].ep_type=(hcc_u8)(ep_type & 0x3);
      usbd_ep_list[ndx].halted=0;
      usbd_ep_list[ndx].tr=0;
#if USBD_ISOCHRONOUS_SUPPORT
      usbd_ep_list[ndx].fifo=NULL;
#endif
      if (prev_ep)
      {
        prev_ep->next=usbd_ep_list+ndx;
      }
      usbd_ep_list[ndx].next=NULL;
	
      if (addr==0)
      {
        usbd_ep_list[ndx].age=1;
        ep0_handle=USBD_EPH_CREATE(1, 0);
        usbd_ep_list[ndx].eph=ep0_handle;
      }
      else
      {
        usbd_ep_list[ndx].age++;
        usbd_ep_list[ndx].eph=(usbd_ep_handle_t)USBD_EPH_CREATE(usbd_ep_list[ndx].age, ndx);
      }

      /* never add endpoint 0 */
      if (addr)
      {
        if(OS_SUCCESS!=usbd_add_ep(ndx))
        {
          MK_DBGTRACE(evt_add_ep, 1);
          assert(0);
          return(NULL);
        }
      }
      MK_DBGTRACE(evt_add_ep, 0);
      return(usbd_ep_list+ndx);
    }
  }
  MK_DBGTRACE(evt_add_ep, 2);
  /* If getting here, then MAX_NO_OF_EP is too small. */
  assert(0);
  return(NULL);
}

/*****************************************************************************
 * Name:
 *    remove_ep
 * In:
 *    index: enpoint index
 * Out:
 *    none
 *
 * Description:
 *    Destroy the specified endpoint.
 * Assumptions:
 *
 *****************************************************************************/
static void remove_ep(int index)
{
  /* Check if the endpoint is allocated. */
  if (usbd_ep_list[index].ep_type!=0xff)
  {
    MK_DBGTRACE(evt_remove_ep, index);

    /* Kill endpoint in hardware. */
    usbd_remove_ep(index);
    /* Mark endpoint un-allocated. */
    usbd_ep_list[index].ep_type=0xff;
    /* Invalidate any existing endpoint handles. */
    usbd_ep_list[index].age++;
    /* If endpoint has an ongoing trnsfer, send error notitycation. */
    if (usbd_ep_list[index].tr != NULL)
    {
      usbd_transfer_t *tr=usbd_ep_list[index].tr;
      MK_DBGTRACE(evt_kill_tr, index);

      tr->state=USBDTRST_EP_KILLED;
      if (tr->event)
      {
        os_event_set(*tr->event);
      }
    }
#if USBD_ISOCHRONOUS_SUPPORT
    else if (usbd_ep_list[index].fifo)
    {
      MK_DBGTRACE(evt_kill_fifo, index);
      usbd_drop_fifo(index);
      usbd_ep_list[index].fifo=NULL;
    }
#endif
  }
}

/*****************************************************************************
 * Name:
 *    find_ep
 * In:
 *    addr: enpoint address
 * Out:
 *    index if found
 *    -1 otherwise
 *
 * Description:
 *    Find the endpoint with the specified address
 *
 * Assumptions:
 *
 *****************************************************************************/
static int find_ep(hcc_u8 addr)
{
  int x;
  for(x=0; x< sizeof(usbd_ep_list)/sizeof(usbd_ep_list[0]); x++)
  {
    if (usbd_ep_list[x].ep_type != 0xff
        && usbd_ep_list[x].addr==addr)
    {
      return(x);
    }
  }
  return(-1);
}

/*****************************************************************************
 * Name:
 *    usbd_next_descriptor
 * In:
 *    start: start of current descriptor
 * Out:
 *    start of next descritor
 *
 * Description:
 *    Return the start address of the next descriptor
 *
 * Assumptions:
 *
 *****************************************************************************/
const hcc_u8 *usbd_next_descriptor(const hcc_u8 *start)
{
  return(start+start[0]);
}

/*****************************************************************************
 * Name:
 *    usbd_find_descriptor
 * In:
 *    start: buffer start pointer
 *    end:   buffer end pointer
 *    type:  descriptor type value to look for
 * Out:
 *    start of descriptor if found
 *    value end otherwise
 *
 * Description:
 *    Return the start address of the next descriptor with mathcing type
 *
 * Assumptions:
 *
 *****************************************************************************/
const hcc_u8 *usbd_find_descriptor(const hcc_u8 *start, const hcc_u8 *end, hcc_u8 type)
{
  while(start<end)
  {
  	if (start[1] == type)
  	{
  	  return(start);
  	}
    start=usbd_next_descriptor(start);  	
  }

  return(end);
}

/*****************************************************************************
 * Name:
 *    init_ifc
 * In:
 *    id:    interface ID
 *    as:    alternate setting
 *    start: start of configuration descriptor.
 * Out:
 *    OS_SUCCESS
 *    OS_ERROR
 *
 * Description:
 *    initialize the specified interface
 *
 * Assumptions:
 *
 *****************************************************************************/
static int init_ifc(hcc_u8 id, hcc_u8 as, const hcc_u8 *start)
{
  const hcc_u8 *end=start+RD_CONFIG_LENGTH(start);

  for(;;)
  {
  	start=usbd_find_descriptor(start, end, STDD_INTERFACE);
  	/* Exit loop if not found. */
  	if (start == end)
  	{
  	  break;
  	}
    /* If ifc id and alternate setting matches. */
    if (start[2]==id && start[3] == as)
    {
      const hcc_u8* ifc_end;
      usbd_ep_info_t *this_ep=NULL;
      const hcc_u8 *ep_start=usbd_next_descriptor(start);
      cdrv_info_t *cdrv_inf;

      /* Find end of configuration data of this interface. */
      ifc_end=usbd_find_descriptor(ep_start, end, STDD_INTERFACE);

      cdrv_inf=find_cdrv(start[5], start[6], start[7], start, ifc_end);

      assert(id<MAX_NO_OF_INTERFACES);
      /* Notify class driver about configuration change. */
      if( alt_setting_list[id].old_cb )
      {
        alt_setting_list[id].old_cb( 0 , 0 , 0 );
        alt_setting_list[id].old_cb = 0;
      }

      alt_setting_list[id].as=as;
      os_mutex_get(usbd_cfg_mutex);
      while(alt_setting_list[start[2]].first_ep)
      {
        usbd_ep_info_t *p=alt_setting_list[start[2]].first_ep;
      	alt_setting_list[start[2]].first_ep=alt_setting_list[start[2]].first_ep->next;
      	remove_ep(USBD_EPH_NDX(p->eph));
      }
  	  alt_setting_list[start[2]].first_ep=NULL;
      os_mutex_put(usbd_cfg_mutex);


  	  /* If a class driver wants this interface. */
  	  if (NULL!=cdrv_inf)
  	  {
  	    /* Look for endpoints. */
  	    do
  	    {
  	      /* Find next endpoint. */
  	      ep_start=usbd_find_descriptor(ep_start, ifc_end, STDD_ENDPOINT);
  	      if(ep_start < ifc_end)
  	      {
  	      	this_ep=add_ep((hcc_u16)(ep_start[4] | (ep_start[5]<<8))
                           ,(hcc_u8) (ep_start[3] & 0x3), ep_start[2]
                           , this_ep);
  	      }
  	      else
  	      {
  	      	break;
  	      }
  	      /* If this is the first endpoint, remember the start index. */
  	      if (NULL==alt_setting_list[start[2]].first_ep)
  	      {
  	        alt_setting_list[start[2]].first_ep=this_ep;
  	      }
  	      ep_start=usbd_next_descriptor(ep_start);
  	    }while(ep_start < ifc_end);

  	    /* Notify class driver about its configuration. */
        (*cdrv_inf->cb)(alt_setting_list[start[2]].first_ep, start[2], cdrv_inf->param);
        /* Remember which class driver is assigned to the interface currently.*/
        alt_setting_list[start[2]].old_cb = cdrv_inf->cb;
        return(OS_SUCCESS);
  	  }
  	}
  	/* Prepare looking for next interface. */
    start=usbd_next_descriptor(start);
  }
  return(OS_ERR);
}

/*****************************************************************************
 * Name:
 *    init_config
 * In:
 *    start: start of configuration descriptor.
 * Out:
 *    OS_SUCCESS
 *    OS_ERROR
 *
 * Description:
 *
 * Assumptions:
 *
 *****************************************************************************/
static int init_config(const hcc_u8 *start)
{
  const hcc_u8 *end=start+RD_CONFIG_LENGTH(start);
  int r=OS_ERR;

  for(;;)
  {
    start=usbd_find_descriptor(start, end, STDD_INTERFACE);
  	
    /* Exit loop if not found. */
    if (start == end)
    {
      break;
    }
  	
    /* If this is the default alternate setting. */
    if (start[3] == 0)
    {
      const hcc_u8* ifc_end;
      const hcc_u8 *ep_start=usbd_next_descriptor(start);
      usbd_ep_info_t *this_ep=NULL;
      cdrv_info_t *cdrv_inf;

      /* Find end of configuration data of this interface. */
      ifc_end=usbd_find_descriptor(ep_start, end, STDD_INTERFACE);

      cdrv_inf=find_cdrv(start[5], start[6], start[7], start, end);
  	
      /* Set alternate setting of this interface. */
      assert(start[2]<MAX_NO_OF_INTERFACES);
 	
      alt_setting_list[start[2]].as=0;
  	  alt_setting_list[start[2]].first_ep=NULL;
      alt_setting_list[start[2]].old_cb=0;

  	  /* If a class driver wants this interface. */
  	  if (NULL!=cdrv_inf)
  	  {
  	    /* Look for endpoints. */
  	    do
  	    {
  	      /* Find next endpoint. */
  	      ep_start=usbd_find_descriptor(ep_start, ifc_end, STDD_ENDPOINT);
  	      if(ep_start < ifc_end)
  	      {
  	      	this_ep=add_ep((hcc_u16)(ep_start[4] | (ep_start[5]<<8)),(hcc_u8) (ep_start[3] & 0x3), ep_start[2], this_ep);
  	      }
  	      else
  	      {
  	      	break;
  	      }
  	
  	      if (NULL==alt_setting_list[start[2]].first_ep)
  	      {
  	      	alt_setting_list[start[2]].first_ep=this_ep;
  	      }
  	
  	      ep_start=usbd_next_descriptor(ep_start);
  	    }while(ep_start < ifc_end);
  	     	
  	    /* Notify class driver about its configuration. */
        (*cdrv_inf->cb)(alt_setting_list[start[2]].first_ep, start[2], cdrv_inf->param);
        /* Remember which class driver is assigned to the interface currently.*/
        alt_setting_list[start[2]].old_cb = cdrv_inf->cb;

        r=OS_SUCCESS;
  	  }
  	}
  	/* Prepare looking for next interface. */
    start=usbd_next_descriptor(start);
  }

  usbd_state=USBDST_CONFIGURED;
  return(r);
}

/*****************************************************************************
 * Name:
 *    usbd_set_config
 * In:
 *    cid: id of configuration
 * Out:
 *    OS_
 *    pointer to descriptor otherwise.
 *
 * Description:
 *    Return pointer to specified string descriptor.
 * Assumptions:
 *
 *****************************************************************************/
static int usbd_set_config(hcc_u8 cid)
{
  int x;
  int r=OS_ERR;
  const unsigned char * const *configs;

  MK_DBGTRACE(evt_set_config, cid);

  os_mutex_get(usbd_cfg_mutex);

  current_cfg=cid;

  usbd_set_cfg_pre();

  /* Kill all endpoints except 0. */
  for(x=1; x<sizeof(usbd_ep_list)/sizeof(usbd_ep_list[0]); x++)
  {
    remove_ep(x);
  }

  /* Kill all class drivers. */
  for(x=0; x<sizeof(cdrv_list)/sizeof(cdrv_list[0]); x++)
  {
    if (NULL != cdrv_list[x].cb)
    {
      cdrv_list[x].cb(NULL, -1, cdrv_list[x].param);
    }
  }

  os_mutex_put(usbd_cfg_mutex);

  current_cfg=0;
  usbd_state=USBDST_ADDRESSED;
  if (cid==0)
  {
    do_state_cb(usbdcst_not_cfg);
    usbd_set_cfg_post();
    return(OS_SUCCESS);
  }

  configs=usbd_at_high_speed() ? usbd_config->configurations_hs : usbd_config->configurations_fsls;

  /* Search for the configuration with matching ID. */
  for(x=0; x<usbd_config->device_descriptor[17]; x++)
  {

    if ((configs[x])[5]==cid)
    {
      r=init_config(configs[x]);
      if (OS_SUCCESS==r)
      {
        current_cfg=cid;
        usbd_set_cfg_post();
        do_state_cb(usbdcst_cfg);
        return(r);
      }
    }
    break;
  }
  usbd_set_cfg_post();
  do_state_cb(usbdcst_not_cfg);
  return(OS_ERR);
}

/*****************************************************************************
 * Name:
 *    usbd_get_state
 * In:
 *    N/A
 * Out:
 *    N/A
 *
 * Description:
 *
 *
 * Assumptions:
 *
 *****************************************************************************/
int usbd_get_state(void)
{
  return(usbd_state);
}

#if USBD_ISOCHRONOUS_SUPPORT
/*****************************************************************************
 * Name:
 *    usbd_stream_attach
 * In:
 *    ep - endpoint handle
 *    fifo - FIFO
 * Out:
 *    OS_SUCCESS - if all ok
 *    OS_ERR - on failure
 *
 * Description:
 *    Attach a FIFO to an endpoint.
 *
 * Assumptions:
 *
 *****************************************************************************/
int usbd_stream_attach(usbd_ep_handle_t ep, rngbuf_t *fifo)
{
  int endx=USBD_EPH_NDX(ep);
  if (ep != USBD_INVALID_EP_HANDLE_VALUE
       && USBD_EPH_AGE(ep)== usbd_ep_list[endx].age)
  {
    usbd_ep_list[endx].fifo=fifo;
    return(usbd_add_fifo(endx));
  }
  return(OS_ERR);
}

/*****************************************************************************
 * Name:
 *    usbd_stream_detach
 * In:
 *    ep - endpoint handle
 * Out:
 *    OS_SUCCESS - if all ok
 *    OS_ERR - on failure
 *
 * Description:
 *
 *
 * Assumptions:
 *
 *****************************************************************************/
int usbd_stream_detach(usbd_ep_handle_t ep)
{
  int endx=USBD_EPH_NDX(ep);
  if (ep != USBD_INVALID_EP_HANDLE_VALUE
       && USBD_EPH_AGE(ep)== usbd_ep_list[endx].age)
  {
    usbd_drop_fifo(endx);
    usbd_ep_list[endx].fifo=NULL;
    return OS_SUCCESS;
  }
  return OS_ERR;
}
#endif


/*****************************************************************************
 * Name:
 *    stop_stack
 * In:
 *    N/A
 * Out:
 *    N/A
 *
 * Description:
 *    Stop the USB stack. Disable all class-drivers and endpoints.
 *
 * Assumptions:
 *
 *****************************************************************************/
static void stop_stack(void)
{
  (void)usbd_hw_stop();
  (void)usbd_std_stop();
#if USBD_SOFTMR_SUPPORT
  (void)softmr_stop();
#endif
}

/*****************************************************************************
 * Name:
 *    start_stack
 * In:
 *    N/A
 * Out:
 *    N/A
 *
 * Description:
 *    Start the USB stack.
 *
 * Assumptions:
 *
 *****************************************************************************/
static void start_stack(void)
{
#if USBD_SOFTMR_SUPPORT
  (void)softmr_start();
#endif
  (void)usbd_std_start();
  (void)usbd_hw_start();
}

/*****************************************************************************
 * Name:
 *    usbd_bus_state_chg_event
 * In:
 *    new_state: USBDST_XXX value to be set.
 * Out:
 *    N/A
 *
 * Description:
 *    Notify common part from usb interrupt handler about a bus state
 *    change. (reset, suspend, vakeup).
 *
 * Assumptions:
 *
 *****************************************************************************/
void usbd_bus_state_chg_event(int new_state)
{
  usbd_state=new_state;
  usbd_state_chg=1;
  os_event_set_int(usbd_state_event);
}

/*****************************************************************************
 * Name:
 *    do_state_cb
 * In:
 *    new_state:  new state to be sent to application
 * Out:
 *    N/A
 *
 * Description:
 *    Do a state call-back to the application is needed.
 *
 * Assumptions:
 *
 *****************************************************************************/
static void do_state_cb(usbd_conn_state_t new_state)
{
  static usbd_conn_state_t old_state=usbdcst_invalid;

  if (old_state!=new_state)
  {
    usbd_conn_state(new_state);
    old_state=new_state;
  }
}

/*****************************************************************************
 * Name:
 *    usbd_control_task
 * In:
 *    N/A
 * Out:
 *    N/A
 *
 * Description:
 *    Handle USB reset event.
 *
 * Assumptions:
 *
 *****************************************************************************/
OS_TASK_FN(usbd_control_task)
{
#if OS_TASK_POLL_MODE
  if (OS_SUCCESS==os_event_get(usbd_state_event))
  {
    int state_cb;
#else
  while(1)
  {
    int state_cb;
    if (OS_SUCCESS!=os_event_get(usbd_state_event))
    {
      continue;
    }
#endif
    state_cb=0;
    MK_DBGTRACE(evt_bus_event, usbd_state);
    /* see if there is a run state change requested */
    /* VBUS has been turned off */
    if (vbst_to_off==usbd_vbus_state)
    {
      usbd_vbus_state=vbst_off;
      /* if stack is running stop it */
      if (rst_run==usbd_run_state)
      {
        stop_stack();
      }
      state_cb=1;
    }
    /* VBUS has been turned on */
    else if (vbst_to_on==usbd_vbus_state)
    {
      usbd_vbus_state=vbst_on;
      if (rst_run==usbd_run_state)
      {
        start_stack();
      }
      state_cb=1;
    }

    if (rst_to_run==usbd_run_state)
    {
      usbd_run_state=rst_run;
      if (vbst_on==usbd_vbus_state)
      {
        start_stack();
      }
      state_cb=1;
    }
    else if (rst_to_stop==usbd_run_state)
    {
      usbd_run_state=rst_stop;
      if (vbst_on==usbd_vbus_state)
      {
        stop_stack();
      }
      state_cb=1;
    }

    if (state_cb)
    {
      state_cb=0;
      if (vbst_off==usbd_vbus_state && rst_stop==usbd_run_state)
      {
        do_state_cb(usbdcst_offline);
      }
      else if (vbst_off==usbd_vbus_state && rst_run==usbd_run_state)
      {
        do_state_cb(usbdcst_ready);
      }
      else if (vbst_on==usbd_vbus_state && rst_stop==usbd_run_state)
      {
        do_state_cb(usbdcst_stopped);
      }
      else if (vbst_on==usbd_vbus_state && rst_run==usbd_run_state)
      {
        do_state_cb(usbdcst_not_cfg);
      }
    }

    if (usbd_state_chg)
    {
      usbd_state_chg=0;

      /* there was a bus reset */
      if (USBDST_DEFAULT==usbd_state)
      {
        usbd_set_config(0);
        /* Set config0 will incorrectly move us to adressed state. */
        usbd_state=USBDST_DEFAULT;
      }
      /* the bus has been suspended */
      else if (USBDST_SUSPENDED==usbd_state)
      {
#if USBD_REMOTE_WAKEUP
        if (usbd_rw_en)
        {
          do_state_cb(usbdcst_suspend_hic);
        }
        else
#endif
        {
          do_state_cb(usbdcst_suspend);
        }
      }
      /* waked up in configured state */
      else if (USBDST_CONFIGURED==usbd_state)
      {
        do_state_cb(usbdcst_cfg);
      }
      /* wakeup in addressed or default state */
      else if (USBDST_ADDRESSED==usbd_state
               ||USBDST_DEFAULT==usbd_state)
      {
        do_state_cb(usbdcst_not_cfg);
      }
    }
  }
}

/*****************************************************************************
 * Name:
 *    send_in_chunks
 * In:
 *    type: descriptor type to change to
 *    buf:  buffer to be sent
 *    len:  buffer length
 *    requested: number of bytes requested
 * Out:
 *    N/A
 *
 * Description:
 *    Send a descriptor changing its type.
 *
 * Assumptions:
 *
 *****************************************************************************/
static usbd_error_t send_in_chunks(hcc_u8 type, hcc_u8* buf, hcc_u16 len, hcc_u16 requested)
{
  hcc_u16 left=(hcc_u16)(MIN(len, requested));
  int start=1;
  int r=USBDERR_NONE;

  ep0_tr.eph=ep0_handle;
  ep0_tr.buffer=(hcc_u8*)tmp_dsc_buf;
  ep0_tr.zp_needed=0;

  while(left && USBDERR_NONE==r)
  {
    /* avoid copying more bytes than buffer can hold. */
    ep0_tr.length=(hcc_u16)MIN(left, sizeof(tmp_dsc_buf));
    memcpy(tmp_dsc_buf, buf, ep0_tr.length);
    buf+=ep0_tr.length;
    left-=ep0_tr.length;
    if (start)
    {
      start=0;
      ((hcc_u8*)tmp_dsc_buf)[1]=type;
    }

    /* send zero length packet when needed */
    if (left==0)
    {
      ep0_tr.zp_needed=(hcc_u8)(len < requested);
    }
    r=usbd_transfer_b(&ep0_tr);
  }
  return r;
}

/*****************************************************************************
 * Name:
 *    usbd_ep0_task
 * In:
 *    N/A
 * Out:
 *    N/A
 *
 * Description:
 *    Handle transfers on the control channel. Started by an event sent by the
 *    driver.
 *
 * Assumptions:
 *
 *****************************************************************************/
OS_TASK_FN(usbd_ep0_task)
{
  int r;
  usbd_error_t tr_err;
#if OS_TASK_POLL_MODE
  if (OS_SUCCESS==os_event_get(usbd_stprx_event))
  {
#else
  while(1)
  {
    if (OS_SUCCESS!=os_event_get(usbd_stprx_event))
    {
      continue;
    }
#endif
    MK_DBGTRACE(evt_stprx, 0);

    tr_err=USBDERR_NONE;
    usbd_get_setup_data(&setup_data);
    switch(BUILD_REQ16(setup_data.bmRequestType, setup_data.bRequest))
    {
    case BUILD_REQ16(USBRQT_DIR_IN | USBRQT_TYP_STD | USBRQT_RCP_DEVICE, USBRQ_GET_DESCRIPTOR):
      r=clbstc_in;
      ep0_tr.dir=USBDTRDIR_IN;
      switch(setup_data.wValue>>8)
      {
      case STDD_DEVICE:
        if (setup_data.wValue && setup_data.wIndex)
        {
          /* Invalid request. */
          r=clbstc_error;
          MK_DBGTRACE(evt_get_devdesc, 0xff);
        }
        else
        {
          ep0_tr.eph=ep0_handle;
          ep0_tr.buffer=(hcc_u8*)usbd_config->device_descriptor;
          ep0_tr.length=(hcc_u32)MIN(usbd_config->device_descriptor[0], setup_data.wLength);
          ep0_tr.zp_needed=(hcc_u8)(ep0_tr.length < setup_data.wLength);
          tr_err=usbd_transfer_b(&ep0_tr);
          MK_DBGTRACE(evt_get_devdesc, tr_err);
        }
        break;
      case STDD_CONFIG:
        /* Do we have a CFG descriptor with the requested index? */
        {
          const unsigned char * const * const configs=usbd_at_high_speed() ?
                              usbd_config->configurations_hs : usbd_config->configurations_fsls;

          if (usbd_config->device_descriptor[17] > (hcc_u8)setup_data.wValue
              && setup_data.wIndex == 0)
          {
            hcc_u8 *cfg=(hcc_u8 *)configs[(hcc_u8)setup_data.wValue];
            ep0_tr.eph=ep0_handle;
            ep0_tr.buffer=cfg;
            ep0_tr.length=(hcc_u32)MIN(RD_CONFIG_LENGTH(cfg), setup_data.wLength);
            ep0_tr.zp_needed=(hcc_u8)(ep0_tr.length < setup_data.wLength);
            tr_err=usbd_transfer_b(&ep0_tr);
            MK_DBGTRACE(evt_getcfg, tr_err);
          }
          else
          {
            MK_DBGTRACE(evt_getcfg, 0xff);
            /* Invalid request. */
            r=clbstc_error;
          }
        }
        break;
      case STDD_DEV_QUALIF:
        if ((setup_data.wValue&0xff) || setup_data.wIndex
            || usbd_config->dev_qualify_fsls==NULL)
        {
          MK_DBGTRACE(evt_devqualif, 0xff);
          /* Invalid request. */
          r=clbstc_error;
        }
        else
        {

          ep0_tr.eph=ep0_handle;
          ep0_tr.buffer=(void *)(usbd_at_high_speed() ? usbd_config->dev_qualify_fsls
                                               : usbd_config->dev_qualify_hs);
          ep0_tr.length=(hcc_u32)MIN(10, setup_data.wLength);
          ep0_tr.zp_needed=(hcc_u8)(10 < setup_data.wLength);
          tr_err=usbd_transfer_b(&ep0_tr);
          MK_DBGTRACE(evt_devqualif, tr_err);
        }
        break;

      case STDD_OTHER_SPEED:
        /* Do we have a CFG descriptor with the requested index? */
        {
          const unsigned char * const *configs=usbd_at_high_speed() ?
                                              usbd_config->configurations_fsls
                                              : usbd_config->configurations_hs;

          if (usbd_config->device_descriptor[17] >= (hcc_u8)setup_data.wValue
              && setup_data.wIndex == 0
              && NULL != configs)
          {
            hcc_u8 *cfg=(hcc_u8 *)configs[(hcc_u8)setup_data.wValue];
            send_in_chunks(STDD_OTHER_SPEED, cfg, (hcc_u16)RD_CONFIG_LENGTH(cfg), setup_data.wLength);
            MK_DBGTRACE(evt_devqualif, 0);
          }
          else
          {
            MK_DBGTRACE(evt_devqualif, 0xff);
            /* Invalid request. */
            r=clbstc_error;
          }
        }
        break;
      case STDD_STRING:
        /* See if te required descriptor exists. */
        {
          hcc_u8 *pstr=find_string((hcc_u8)setup_data.wValue, setup_data.wIndex);
          if (pstr != (hcc_u8 *)0)
          {
            ep0_tr.eph=ep0_handle;
            ep0_tr.buffer=pstr;
            ep0_tr.length=(hcc_u32)MIN(pstr[0], setup_data.wLength);
            ep0_tr.zp_needed=(hcc_u8)(ep0_tr.length < setup_data.wLength);
            tr_err=usbd_transfer_b(&ep0_tr);
            MK_DBGTRACE(evt_get_string, tr_err);
            break;
          }
          else
          {
            MK_DBGTRACE(evt_get_string, 0xff);
            /* Invalid request. */
            r=clbstc_error;
          }
        }

        /* Fall trough and call class driver to handle the string descriptor. */

      /* Non standard descriptor type. */
      default:
        /* Call user callback. */
        goto call_usercb;
      }
      break;

    case BUILD_REQ16(USBRQT_DIR_IN | USBRQT_TYP_STD | USBRQT_RCP_DEVICE, USBRQ_GET_CONFIGURATION):
      r=clbstc_in;
      ep0_tr.dir=USBDTRDIR_IN;

      if (setup_data.wValue == 0 && setup_data.wLength == 1
          && setup_data.wIndex == 0)
      {
        ep0_tr.eph=ep0_handle;
        ep0_tr.buffer=(hcc_u8*)&current_cfg;
        ep0_tr.length=1;
        ep0_tr.zp_needed=0;
        tr_err=usbd_transfer_b(&ep0_tr);
        MK_DBGTRACE(evt_get_config, tr_err);
      }
      else
      {
        MK_DBGTRACE(evt_get_config, 0xff);
        /* Invalid request */
        r=clbstc_error;
      }
      break;
    case BUILD_REQ16(USBRQT_DIR_OUT | USBRQT_TYP_STD | USBRQT_RCP_DEVICE, USBRQ_SET_FEATURE):
  #if USBD_REMOTE_WAKEUP
      if(setup_data.wValue == FEAT_DEVICE_REMOTE_WAKEUP)
      {
        r=clbstc_out;
        usbd_rw_en=(hcc_u8)(1<<1);
        MK_DBGTRACE(evt_rwake_enabled, 0);
      }
      else
  #endif
      {
        MK_DBGTRACE(evt_rwake_enabled, 1);
        r=clbstc_error;
      }
      break;
    case BUILD_REQ16(USBRQT_DIR_OUT | USBRQT_TYP_STD | USBRQT_RCP_DEVICE, USBRQ_CLEAR_FEATURE):
  #if USBD_REMOTE_WAKEUP
      if(setup_data.wValue == FEAT_DEVICE_REMOTE_WAKEUP)
      {
        r=clbstc_out;
        usbd_rw_en=0;
        MK_DBGTRACE(evt_rwake_disabled, 0);
      }
      else
  #endif
      {
        MK_DBGTRACE(evt_rwake_disabled, 1);
        r=clbstc_error;
      }
      break;
    case BUILD_REQ16(USBRQT_DIR_IN | USBRQT_TYP_STD | USBRQT_RCP_DEVICE, USBRQ_GET_STATUS):
      if (setup_data.wValue == 0 && setup_data.wLength == 2)
      {
  #if USBD_REMOTE_WAKEUP
    #ifdef LE16
        state=LE16(usbd_rw_en | usbd_is_self_powered()); /* wakeup state, power state */
    #else
        WR_LE16(&state, usbd_rw_en | usbd_is_self_powered());
    #endif
  #else
   #ifdef LE16
        state=LE16(usbd_is_self_powered()); /* no wakeup, power state */
    #else
        WR_LE16(&state, usbd_is_self_powered());
    #endif
  #endif
        ep0_tr.eph=ep0_handle;
        ep0_tr.buffer=(hcc_u8*)&state;
        ep0_tr.length=2;
        ep0_tr.zp_needed=0;
        ep0_tr.dir=USBDTRDIR_IN;

        tr_err=usbd_transfer_b(&ep0_tr);
        r=clbstc_in;
      }
      else
      {
        /* Invalid request */
        r=clbstc_error;
      }
      break;
    case BUILD_REQ16(USBRQT_DIR_OUT | USBRQT_TYP_STD | USBRQT_RCP_DEVICE, USBRQ_SET_ADDRESS):
      if (setup_data.wValue < 0x80 && setup_data.wLength == 0
           && setup_data.wIndex ==0)
      {
        new_addr=(hcc_u8)setup_data.wValue;
        usbd_set_addr_pre(new_addr);
        r=clbstc_out;
        MK_DBGTRACE(evt_set_addr, 0);
      }
      else
      {
        /* Invalid request */
        r=clbstc_error;
        MK_DBGTRACE(evt_set_addr, 1);
      }
      break;
    case BUILD_REQ16(USBRQT_DIR_OUT | USBRQT_TYP_STD | USBRQT_RCP_DEVICE, USBRQ_SET_CONFIGURATION):
      if (setup_data.wIndex == 0 && setup_data.wLength==0
               && usbd_config->device_descriptor[17] >= (hcc_u8)setup_data.wValue)
      {
        usbd_set_config((hcc_u8)setup_data.wValue);
        if (current_cfg==(hcc_u8)setup_data.wValue)
        {
          r=clbstc_out;
        }
        else
        {
          r=clbstc_error;
        }
        MK_DBGTRACE(evt_set_cfg, 0);
      }
      else
      {
        MK_DBGTRACE(evt_set_cfg, 1);
        r=clbstc_error;
      }
      break;
    case BUILD_REQ16(USBRQT_DIR_OUT | USBRQT_TYP_STD | USBRQT_RCP_IFC, USBRQ_SET_INTERFACE):
      if (setup_data.wIndex < MAX_NO_OF_INTERFACES)
      {
        int x;
        const unsigned char * const *configs=usbd_at_high_speed() ?
                              usbd_config->configurations_hs : usbd_config->configurations_fsls;

        /* Search for the configuration with matching ID. */
        for(x=0; x<usbd_config->device_descriptor[17]; x++)
        {
          if ((configs[x])[5]==current_cfg)
          {
            init_ifc((hcc_u8)setup_data.wIndex, (hcc_u8)setup_data.wValue,configs[x]);
            break;
          }
        }
        MK_DBGTRACE(evt_set_ifc,0);
        r=clbstc_out;
      }
      else
      {
        MK_DBGTRACE(evt_set_ifc,1);
        r=clbstc_error;
      }
      break;
    case BUILD_REQ16(USBRQT_DIR_IN | USBRQT_TYP_STD | USBRQT_RCP_IFC, USBRQ_GET_INTERFACE):
      if (setup_data.wIndex < MAX_NO_OF_INTERFACES && setup_data.wLength ==1 )
      {
        ep0_tr.eph=ep0_handle;
        ep0_tr.buffer=&alt_setting_list[setup_data.wIndex].as;
        ep0_tr.length=1;
        ep0_tr.zp_needed=0;
        ep0_tr.dir=USBDTRDIR_IN;
        tr_err=usbd_transfer_b(&ep0_tr);
        r=clbstc_in;
        MK_DBGTRACE(evt_get_ifc, tr_err);
      }
      else
      {
        MK_DBGTRACE(evt_get_ifc, 0xff);
        r=clbstc_error;
      }
      break;
    case BUILD_REQ16(USBRQT_DIR_IN | USBRQT_TYP_STD | USBRQT_RCP_IFC, USBRQ_GET_STATUS):
      if (setup_data.wValue == 0 && setup_data.wLength == 2)
      {
        /* This request returns always 0. Defined by the usb spec... */
        state=0;
        ep0_tr.eph=ep0_handle;
        ep0_tr.buffer=(hcc_u8*)&state;
        ep0_tr.length=2;
        ep0_tr.zp_needed=0;
        ep0_tr.dir=USBDTRDIR_IN;
        tr_err=usbd_transfer_b(&ep0_tr);

        r=clbstc_out;
      }
      else
      {
        r=clbstc_error;
      }
      break;
    case BUILD_REQ16(USBRQT_DIR_OUT | USBRQT_TYP_STD | USBRQT_RCP_EP, USBRQ_CLEAR_FEATURE):
      r=clbstc_out;
      switch(setup_data.wValue)
      {
      case FEAT_ENDPOINT_HALT:
        {
          /* Find the endpoint with the address specified in setup packet. */
          int index=find_ep((hcc_u8)setup_data.wIndex);
          usbd_ep_info_t *ep=usbd_ep_list+index;

          MK_DBGTRACE(evt_clrhalt, 0);

          if (index == -1 || ep->ep_type == 0xff)
          {
            MK_DBGTRACE(evt_clrhalt_noep, 0);
            r=clbstc_error;
            break;
          }
          if (usbd_ep_list[index].halted==0)
          {
            MK_DBGTRACE(evt_clrhalt_accept, 0);
            usbd_clr_stall(index);
          }
        }
        break;
      default:
         r=clbstc_error;
      }
      break;
    case BUILD_REQ16(USBRQT_DIR_OUT | USBRQT_TYP_STD | USBRQT_RCP_EP, USBRQ_SET_FEATURE):
      r=clbstc_out;
      switch(setup_data.wValue)
      {
      case FEAT_ENDPOINT_HALT:
        {
          /* Find the endpoint with the address specified in setup packet. */
          int index=find_ep((hcc_u8)setup_data.wIndex);
          usbd_ep_info_t *ep=usbd_ep_list+index;

          MK_DBGTRACE(evt_sethalt, 0);

          if (index == -1 || ep->ep_type == 0xff)
          {
            r=clbstc_error;
            MK_DBGTRACE(evt_sethalt_noep, 0);
            break;
          }
          if (usbd_ep_list[index].halted==0)
          {
            usbd_ep_handle_t eph=USBD_EPH_CREATE(usbd_ep_list[index].age, index);
            usbd_set_halt(eph);
            usbd_clr_halt(eph);
            MK_DBGTRACE(evt_sethalt_ok, 0);
          }
        }
        break;
      default:
         r=clbstc_error;
      }
      break;

    case BUILD_REQ16(USBRQT_DIR_IN | USBRQT_TYP_STD | USBRQT_RCP_EP, USBRQ_GET_STATUS):
      if (setup_data.wValue == 0 && setup_data.wLength == 2)
      {
        /* Find the endpoint with the address specified in setup packet. */
        int index=find_ep((hcc_u8)setup_data.wIndex);
        usbd_ep_info_t *ep=usbd_ep_list+index;
        if (index == -1 || ep->ep_type == 0xff)
        {
          MK_DBGTRACE(evt_gethalt_noep, 0);
          r=clbstc_error;
          break;
        }
        r=clbstc_in;
#ifdef LE16
        state=LE16((hcc_u16)((usbd_ep_list[index].halted || usbd_get_stall(index)) ? 1u : 0u));
#else
        WR_LE16(&state, (hcc_u16)((usbd_ep_list[index].halted || usbd_get_stall(index)) ? 1u : 0u));
#endif
        ep0_tr.eph=ep0_handle;
        ep0_tr.buffer=(hcc_u8*)&state;
        ep0_tr.length=2;
        ep0_tr.zp_needed=0;
        ep0_tr.dir=USBDTRDIR_IN;

        tr_err=usbd_transfer_b(&ep0_tr);
        MK_DBGTRACE(evt_gethalt, tr_err);
      }
      else
      {
        MK_DBGTRACE(evt_gethalt, 0xff);
        r=clbstc_error;
      }
      break;
    /* Unknown or not implemented request. */
    default:
      call_usercb:
      {
        int ndx;
        r=clbstc_error;
        tr_err=USBDERR_NONE;
        for(ndx=0; ndx<sizeof(cdrv_list)/sizeof(cdrv_list[0]); ndx++)
        {
          if (NULL != cdrv_list[ndx].ep0_cb)
          {
            r=(*cdrv_list[ndx].ep0_cb)(&setup_data, &ep0_tr);
            if (r != clbstc_error)
            {
              break;
            }
          }
        }
        MK_DBGTRACE(evt_usrclb, 0);
      }
    }

    /* Do the handshake phase only if the data phase was success. */
    if (USBDERR_NONE == tr_err)
    {
      switch(r)
      {
      case clbstc_in:
        MK_DBGTRACE(evt_hshkin, 0);
        ep0_tr.eph=ep0_handle;
        ep0_tr.buffer=0;
        ep0_tr.length=0;
        ep0_tr.zp_needed=0;
        ep0_tr.dir=USBDTRDIR_HS_IN;
        (void)usbd_transfer_b(&ep0_tr);
        break;
      case clbstc_out:
        MK_DBGTRACE(evt_hshkout, 0);
        ep0_tr.eph=ep0_handle;
        ep0_tr.buffer=0;
        ep0_tr.length=0;
        ep0_tr.zp_needed=0;
        ep0_tr.dir=USBDTRDIR_HS_OUT;
        (void)usbd_transfer_b(&ep0_tr);
        if (new_addr != INVALID_DEV_ADDR)
        {
          usbd_set_addr_post(new_addr);
          if (USBDST_DEFAULT != usbd_state && USBDST_ADDRESSED != usbd_state)
          {
            usbd_set_config(0);
          }
          new_addr=INVALID_DEV_ADDR;
          usbd_state=USBDST_ADDRESSED;
        }
        break;
      case clbstc_error:
        MK_DBGTRACE(evt_hshkerr, 0);
        usbd_set_stall(USBD_EPH_NDX(ep0_handle));
        break;
      }
    }
  }
}
/****************************** END OF FILE **********************************/
