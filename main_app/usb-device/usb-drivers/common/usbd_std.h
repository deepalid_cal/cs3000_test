/****************************************************************************
 *
 *            Copyright (c) 2008-2010 by HCC Embedded
 *
 * This software is copyrighted by and is the sole property of
 * HCC.  All rights, title, ownership, or other interests
 * in the software remain the property of HCC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of HCC.
 *
 * HCC reserves the right to modify this software without notice.
 *
 * HCC Embedded
 * Budapest 1133
 * Vaci ut 76.
 * Hungary
 *
 * Tel:  +36 (1) 450 1302
 * Fax:  +36 (1) 450 1303
 * http: www.hcc-embedded.com
 * email: info@hcc-embedded.com
 *
 ***************************************************************************/
#ifndef _USBD_STD_H_
#define _USBD_STD_H_

/* World-wide declarations for the STD module.

    This file contains all world-wide public declarations of the standard module.
    The standard mosule is responsible to implement handlers for standard
    USB requests and everyting tightly related to these. */

#include "os.h"
#include "usbd_config.h"
/* Major version number of the module. */
#define USBD_STD_MAJOR  7
/* Minor version number of the module. */
#define USBD_STD_MINOR  5

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 ************************ Macro definitions ***********************************
 *****************************************************************************/
/* Envalid endpoint handle value.
    Use this value to initialize endpoint handles. */
#define USBD_INVALID_EP_HANDLE_VALUE ((hcc_u16 ) 0xff00)

/* USB driver state values.*/
#define USBDST_DISABLED       0    /* State after usbd_init() */
#define USBDST_DEFAULT        1    /* State after USB reset */
#define USBDST_ADDRESSED      2    /* State after set address */
#define USBDST_CONFIGURED     3    /* State after set config */
#define USBDST_SUSPENDED      4    /* State after suspend */

/* Bitmask for the Request type field of the setup packet (bmRequestType) */
#define USBRQT_DIR_IN           (1u<<7)  /* IN request */
#define USBRQT_DIR_OUT          (0u<<7)  /* OUT request */
#define USBRQT_TYP_STD          (0u<<5)  /* Standard request */
#define USBRQT_TYP_CLASS        (1u<<5)  /* Class-specific request */
#define USBRQT_TYP_VENDOR       (2u<<5)  /* Vendor-specific request */
#define USBRQT_TYP_MASK         (3u<<5)  /* Mask for request type bits */
#define USBRQT_RCP_DEVICE       (0u<<0)  /* Recipient is the device */
#define USBRQT_RCP_IFC          (1u<<0)  /* Recipient is an interface */
#define USBRQT_RCP_EP           (2u<<0)  /* Recipient is an endpoint */
#define USBRQT_RCP_OTHER        (3u<<0)  /* Recipient is something else */
#define USBRQT_RCP_MASK         (3u<<0)  /* Mask for recipient bits */

/* Values for the request filed of the setup packet.(bmRequest). */
#define USBRQ_GET_STATUS         0u  /* Get status of something */
#define USBRQ_CLEAR_FEATURE      1u  /* Clear a feature */
#define USBRQ_SET_FEATURE        3u  /* Set a feature */
#define USBRQ_SET_ADDRESS        5u  /* Set USB address of the device */
#define USBRQ_GET_DESCRIPTOR     6u  /* Read out a descriptor */
#define USBRQ_SET_DESCRIPTOR     7u  /* Change a descriptor */
#define USBRQ_GET_CONFIGURATION  8u  /* Read out the ID of the active configuration */
#define USBRQ_SET_CONFIGURATION  9u  /* Change the active configuration */
#define USBRQ_GET_INTERFACE      10u /* Read the state of an interface */
#define USBRQ_SET_INTERFACE      11u /* Set alternate setting of an interface */
#define USBRQ_SYNCH_FRAME        12u /* Set and report ISO endpoint sych frame */

/* Standard USB descriptor type vaules.

    Used in configuration data and in standared request processing.*/
#define STDD_DEVICE         0x1u /* Device descriptor */
#define STDD_CONFIG         0x2u /* Configuration descriptor */
#define STDD_STRING         0x3u /* String descriptor */
#define STDD_INTERFACE      0x4u /* Interface descriptor */
#define STDD_ENDPOINT       0x5u /* Endpoint descriptor */
#define STDD_DEV_QUALIF     0x6u /* Device qualifier descriptor */
#define STDD_OTHER_SPEED    0x7u /* Other speed configuration descriptor */
#define STDD_IFC_ASSOC      0xbu /* Interface association descriptor */

/* USB configuration descriptor attribute masks.

  Values for the attrib field of the configuration descriptor. */
#define CFGD_ATTR_BUS_PWR  (3u<<6)  /* devide ic BUS powered */
#define CFGD_ATTR_SELF_PWR (1u<<6)  /* device is self powered */
#define CFGD_ATTR_RWAKEUP  (1u<<5)  /* device can signal remote wakeup */

/* Endpoint direction specifyers */
/* IN endpoint (device to host) */
#define EPD_DIR_TX                0x80
/* Out endpoint (host todevice) */
#define EPD_DIR_RX                0

/* Control endpoint */
#define EPD_ATTR_CTRL             0
/* Isochronous endpoint. */
#define EPD_ATTR_ISO              1
/* Bulk endpoint. */
#define EPD_ATTR_BULK             2
/* Interrupt endpoint. */
#define EPD_ATTR_INT              3
/* Iso endpoint synchronisation type: none */
#define EPD_ATTR_ISO_SYNC_NONE    (0 << 2)
/* Iso endpoint synchronisation type: asynchronous */
#define EPD_ATTR_ISO_SYNC_ASYNC   (1 << 2)
/* Iso endpoint synchronisation type: adaptive */
#define EPD_ATTR_ISO_SYNC_ADAPT   (2 << 2)
/* Iso endpoint synchronisation type: synchronous */
#define EPD_ATTR_ISO_SYNC_SYNC    (3 << 2)
/* Iso endpoint usage type: data endpoint */
#define EPD_ATTR_ISO_USAGE_DATA   (0 << 4)
/* Iso endpoint usage type: feedback endpoint */
#define EPD_ATTR_ISO_USAGE_FEEDB  (1 << 4)
/* Iso endpoint usage type: explicite feedback endpoint */
#define EPD_ATTR_ISO_USAGE_EFEEDB (2 << 4)

/* Standard USB feature selector values. */
#define FEAT_ENDPOINT_HALT        0u  /* Endpoin halt feature selector */
#define FEAT_DEVICE_REMOTE_WAKEUP 1u  /* Remote wakeup feature selector */


/******************************************************************************
 ************************ Type definitions ************************************
 *****************************************************************************/
/*Return values for ep0 callbacks. */
typedef enum {
  clbstc_error,    /* Error encountered, stop endpoint. */
  clbstc_in,       /* Start IN transfer. */
  clbstc_out       /* Start OUT transfer. */
} usbd_callback_state_t;

/* Strucutre to hold setup transaction data.
    Used in ep0 callback functions. */
typedef struct {
   hcc_u8 bmRequestType;
   hcc_u8 bRequest;
   hcc_u16 wValue;
   hcc_u16 wIndex;
   hcc_u16 wLength;
} usbd_setup_data_t;

/* Endpoint descriptor.

    Holds information about an endpoint. The driver uses this to manage the
    endpoint. It is only world wide public because class drivers need read only
    access to this information during initialization. */
typedef struct usbd_ep_info_s{
  usbd_transfer_t *tr;
  struct usbd_ep_info_s *next;
#if USBD_ISOCHRONOUS_SUPPORT
  rngbuf_t *fifo;
#endif
  usbd_ep_handle_t eph;
  usbd_hw_ep_info_t hw_info;
  hcc_u16 psize;
  hcc_u8 ep_type;
  hcc_u8 addr;
  hcc_u8 age;
  hcc_u8 halted;
} usbd_ep_info_t;

/* Current limit values for devices. */
typedef enum {
  usbdcst_invalid,    /* invalid state, newer sent */
  usbdcst_offline,    /* cable not connected stack stopped */
  usbdcst_ready,      /* cable not connected stack started */
  usbdcst_stopped,    /* cable connected, usbd stack stopped */
  usbdcst_not_cfg,    /* not configured */
  usbdcst_cfg,        /* configured */
  usbdcst_suspend,    /* suspended */
  usbdcst_suspend_hic /* suspended hi current draw enabled */
} usbd_conn_state_t;

/* Class-driver call-back function type.

    This type defines the class-driver call-back function. It is called by
    the standard request handler task when the USB configuration of the
    device is changeing. The change can be triggered by the following events:
      -bus reset
      -set configuration request sent by the host
      -set interface request sent by the host */
typedef void usbd_cdrv_cb_t(const usbd_ep_info_t * eps /* array with endpoint information for this interface */
                            , const int ifc_ndx /* id field of interface descriptor */
                            , const int param); /* parameter spefified by usdb_register_cdrv() */

/* Endpoint 0 call-back type.

    This type defines the EP0 (default pipe) call-back function. It is called
    by the standard request handler task and is used to extend the set of
    request the device is ablt to handle on EP0. */
typedef usbd_callback_state_t usbd_ep0_cb_t(const usbd_setup_data_t *stp,
                                            usbd_transfer_t *tr);
/* Descriptor parse extension callback. Using this call-back the class-driver
   can do class driver specific processing on the configuration data. If the
   return value is 0, the class driver accepst the intercafe. if not, the
   class driver denies using this interface. */
typedef int usbd_parse_cb_t(const hcc_u8 *start, const hcc_u8 *end);

/******************************************************************************
 ************************ Global variables ************************************
 *****************************************************************************/
/* Handle of endpoint 0.

    This variable can be used if some transfer needs to be done on EP0. Note:
    EP0 is a shared resource. The driver will lock the endpoint and report
    USBDERR_BUSY to avoid concurrency problems.*/
extern usbd_ep_handle_t ep0_handle;

/* Pointer to the current configuration data. */
extern usbd_config_t *usbd_config;

/******************************************************************************
 ************************ Imported functions **********************************
 *****************************************************************************/
/* Power status call-back.
   Called by the driver to query if the device is currently running self
   powered or not (is powered from the USB). Bus powered only devices
   must never return nonzero value.*/
int usbd_is_self_powered(void);

/******************************************************************************
 ************************ Exported functions **********************************
 *****************************************************************************/
/* Register a class driver

    This function registers a class driver. The class driver can specify what
    interface type it is able to drive and what call-back functions the driver
    shall call. drv_cb is used to tell the class-driver which endpoint handles
    it shall use for communication. ep0_cb is called to enable extednig the set
    of requests the device is able to handle on EP0. */
int usdb_register_cdrv(hcc_u8 _class, hcc_u8 sclass, hcc_u8 proto
                                , usbd_cdrv_cb_t *drv_cb, int param, usbd_ep0_cb_t *ep0_cb, usbd_parse_cb_t *parse_cb);

/* Find a descriptor in the spefifyed range with matching type. The return
   value is either a pointer ot the matching descriptor or end. */
const hcc_u8 *usbd_find_descriptor(const hcc_u8 *start, const hcc_u8 *end, hcc_u8 type);

/* Return a pointer to the next descriptor. Start must refer to a valid USB
   descriptor. */
const hcc_u8 *usbd_next_descriptor(const hcc_u8 *start);

/* This is a call-back function tells the application what is the maximum
   current it may draw from the USB.*/
void usbd_conn_state(usbd_conn_state_t new_state);

#if 0
/* This is a template for the usbd_conn_state function. */
void usbd_conn_state(usbd_conn_state_t new_state)
{
  switch(new_state)
  {
  case usbdcst_offline:
    /* cable not connected, stack stopped */
    break;
    /* cabe not connected, stack started */
  case usbdcst_ready:
    break;
  case usbdcst_stopped:
    /* cable connected, stack stopped; bus powered device may draw 500uA from USB */
    break;
  case usbdcst_not_cfg:
    /* online, not configured; bus powered device may draw 10mA from USB */
    break;
  case usbdcst_cfg:
    /* configured; bus powered device may the maximum amount of current
       specifyed in the configuration descriptor */
    break;
  case usbdcst_suspend:
    /* suspended; bus powered device may draw 500uA from USB */
    break;
  case usbdcst_suspend_hic:
    /* suspended; bus powered device may draw 2.5 mA from USB */
    break;
  }
}
#endif

/* The standard request handling task. */
OS_TASK_DEF(usbd_ep0_task);

/* USB bus state change handling task. */
OS_TASK_DEF(usbd_control_task);

#ifdef __cplusplus
}
#endif


#endif
/****************************** END OF FILE **********************************/
