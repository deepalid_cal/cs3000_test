/****************************************************************************
 *
 *            Copyright (c) 2009-2010 by HCC Embedded
 *
 * This software is copyrighted by and is the sole property of
 * HCC.  All rights, title, ownership, or other interests
 * in the software remain the property of HCC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of HCC.
 *
 * HCC reserves the right to modify this software without notice.
 *
 * HCC Embedded
 * Budapest 1133
 * Vaci ut 76.
 * Hungary
 *
 * Tel:  +36 (1) 450 1302
 * Fax:  +36 (1) 450 1303
 * http: www.hcc-embedded.com
 * email: info@hcc-embedded.com
 *
 ***************************************************************************/
#ifndef _SOF_TIMERI_H_
#define _SOF_TIMERI_H_

#include "hcc_types.h"
#include "os.h"

#include "sof_timer.h"

#ifdef __cplusplus
#extern "C" {
#endif

int softmr_init(void);
int softmr_start(void);
int softmr_stop(void);
int softmr_delete(void);

void softmr_tick(hcc_u16 sof_ctr);

#ifdef __cplusplus
}
#endif

#endif
/****************************** END OF FILE **********************************/
