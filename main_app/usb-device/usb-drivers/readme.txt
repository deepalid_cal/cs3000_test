Low-level USB driver for AT91SAM MCU:s
======================================================
Feb 02, 2009
--------------------------------

Files:
  usbd_dev.h
  usbd_sam.c
  usbdi_dev.h

This file holds low-level driver specific information for this driver.

Limitations for AT91SAM9263.
  The number of avilable endpoints is 6 on this device.
  Only endpoint 1,2,4 and 5 can be configured to work in ISO mode.
  Only endpoint 0 and 3 can be configured to work as control endpoints.
  Maximum packet size of endpoint 0-3 is 64 bytes.
  Maximum packet size of endpoint 4 and 5 256 bytes.

These devices use fixed endpoint addresses. Because of this the endpoint
descriptors may ony specify the following endpoint addresses:
   0x01,0x02,0x03,0x04,0x05,0x81,0x82,0x83,0x84,0x85
Endpoints are single direction. Thus specifying both address 0x05 and 0x85 is
illegal.

Please ensure the endpoint configuration keeps the limits specified in the
previous section for your device.

DEBUG_TRACE
  This macro is defined at the start of the driver. If the value is non zero,
  then the driver will put trace of important events into a ring buffer. This
  is only usefull when debugging the driver and by default it is turned off.

======= END OF FILE ===========================================================
