/****************************************************************************
 *
 *            Copyright (c) 2007-2009 by HCC Embedded
 *
 * This software is copyrighted by and is the sole property of
 * HCC.  All rights, title, ownership, or other interests
 * in the software remain the property of HCC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of HCC.
 *
 * HCC reserves the right to modify this software without notice.
 *
 * HCC Embedded
 * Budapest 1133
 * V�ci �t 110.
 * Hungary
 *
 * Tel:  +36 (1) 450 1302
 * Fax:  +36 (1) 450 1303
 * http: www.hcc-embedded.com
 * email: info@hcc-embedded.com
 *
 ***************************************************************************/
#ifndef _OS_H_
#define _OS_H_

#include "hcc_types.h"
#include "os_cfg.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#ifdef __cplusplus
extern "C" {
#endif

#define OS_ISR_DEF(name)          extern void name(void)
#define OS_ISR_FN(name)           void name(void)

#define OS_TASK_FN(fn)		  void (fn)( void *pvParameters )
#define OS_TASK_DEF(fn)		  void (fn)( void *pvParameters )

#define OS_MUTEX_TYPE             xSemaphoreHandle

#define OS_EVENT_BIT_TYPE         hcc_u32
#define OS_EVENT_TYPE             xSemaphoreHandle

typedef struct {
  hcc_u8 vector;
  hcc_u8 priority;
  void (*handler)(void);
} os_it_info_t;

#ifdef __cplusplus
}
#endif

#include "os_common.h"

#if OS_API_MAJOR != 2
#error "Incorrect OS API version."
#endif

#endif
/****************************** END OF FILE **********************************/
