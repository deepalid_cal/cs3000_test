/****************************************************************************
 *
 *            Copyright (c) 2007-2009 by HCC Embedded
 *
 * This software is copyrighted by and is the sole property of
 * HCC.  All rights, title, ownership, or other interests
 * in the software remain the property of HCC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of HCC.
 *
 * HCC reserves the right to modify this software without notice.
 *
 * HCC Embedded
 * Budapest 1133
 * V�ci �t 110.
 * Hungary
 *
 * Tel:  +36 (1) 450 1302
 * Fax:  +36 (1) 450 1303
 * http: www.hcc-embedded.com
 * email: info@hcc-embedded.com
 *
 ***************************************************************************/
#include "hcc_types.h"
#include "os.h"
#include "usbd_api.h"
#include "usbd_lpc_config.h"
#include "lpc32xx_intc_driver.h"

// 6/24/2014 ajv : Required for INTERRUPT_PRIORITY_usb_dev_hp and _lp
//definitions 
#include	"cs_common.h" 



#if OS_EVENT_BIT_COUNT
OS_EVENT_TYPE event[OS_EVENT_BIT_COUNT];
#endif

#if OS_MUTEX_COUNT
OS_MUTEX_TYPE mutex[OS_MUTEX_COUNT];
#endif
/*-----------------------------------------------------------*/
void os_int_enable(void)
{
  taskENABLE_INTERRUPTS();
}

/*-----------------------------------------------------------*/
void os_int_disable(void)
{
  vPortEnterCritical();
}
/*-----------------------------------------------------------*/
void os_int_restore(void)
{
#if OS_INTERRUPT_ENABLE
  vPortExitCritical();
#endif
}
/*-----------------------------------------------------------*/
#if OS_INTERRUPT_ENABLE
#define os_idle()
#else
#endif

int os_isr_init (hcc_u32 pos, void(*ih)(void))
{
    if (pos==USB_DEVICE_ISR)
    {
      os_int_disable();
      xSetISR_Vector(USB_dev_hp_INT, INTERRUPT_PRIORITY_usb_dev_hp, ISR_TRIGGER_HIGH_LEVEL, ih, 0);
      xSetISR_Vector(USB_dev_lp_INT, INTERRUPT_PRIORITY_usb_dev_lp, ISR_TRIGGER_HIGH_LEVEL, ih, 0);
      os_int_restore();         /* Enable interrupts */
      return 0;
    }
  return OS_SUCCESS;
}
/*-----------------------------------------------------------*/
int os_isr_enable (hcc_u32 pos)
{
  if (pos==USB_DEVICE_ISR)
  {
    xEnable_ISR(USB_dev_hp_INT);
    xEnable_ISR(USB_dev_lp_INT);
    return OS_SUCCESS;
  }
  return OS_ERR;
}
/*-----------------------------------------------------------*/
int os_isr_disable (hcc_u32 pos)
{
  if (pos==USB_DEVICE_ISR)
  {
    xDisable_ISR(USB_dev_hp_INT);
    xDisable_ISR(USB_dev_lp_INT);
    return OS_SUCCESS;
  }
  return OS_ERR;
}
/*-----------------------------------------------------------*/
#if OS_MUTEX_COUNT
int os_mutex_create(OS_MUTEX_TYPE **pmutex)
{
  unsigned int i;
  for (i=0;i<OS_MUTEX_COUNT;i++)
  {
    if (mutex[i]==NULL)
    {
      mutex[i]=xSemaphoreCreateMutex();
      *pmutex=&mutex[i];
      return OS_SUCCESS;
    }
  }
  return OS_ERR;
}
/*-----------------------------------------------------------*/
int os_mutex_get(OS_MUTEX_TYPE *pmutex)
{
  if  ( xSemaphoreTake( *pmutex, portMAX_DELAY ) != pdTRUE ) return OS_ERR;
  return OS_SUCCESS;
}
/*-----------------------------------------------------------*/
int os_mutex_put(OS_MUTEX_TYPE *pmutex)
{
  xSemaphoreGive( *pmutex );
  return OS_SUCCESS;
}
/*-----------------------------------------------------------*/
int os_mutex_delete(OS_MUTEX_TYPE *pmutex)
{
  *pmutex = 0;;
  return OS_SUCCESS;
}
#endif
/*-----------------------------------------------------------*/
#if OS_EVENT_BIT_COUNT
int os_event_create(OS_EVENT_BIT_TYPE *event_bit)
{
  unsigned int i;
  for (i=0;i<OS_EVENT_BIT_COUNT;i++)
  {
    if (event[i]==NULL)
    {
      vSemaphoreCreateBinary(event[i]);
      if  (  xSemaphoreTake( event[i], 1 ) != pdTRUE ) return OS_ERR;
      *event_bit=(OS_EVENT_BIT_TYPE)i;
      return OS_SUCCESS;
    }
  }
  return OS_ERR;
}
/*-----------------------------------------------------------*/
int os_event_get(OS_EVENT_BIT_TYPE event_bit)
{
  if  ( xSemaphoreTake( event[event_bit], portMAX_DELAY ) != pdTRUE ) return OS_ERR;
  return OS_SUCCESS;
}
/*-----------------------------------------------------------*/
int os_event_delete(OS_EVENT_BIT_TYPE event_bit)
{
  (void)event_bit;
  /* No way to destroy a semaphore in FreeRTOS. */
  return(OS_SUCCESS);
}
/*-----------------------------------------------------------*/
int os_event_set(OS_EVENT_BIT_TYPE event_bit)
{
  xSemaphoreGive(event[event_bit]);
  return OS_SUCCESS;
}
/*-----------------------------------------------------------*/
int os_event_set_int(OS_EVENT_BIT_TYPE event_bit)
{
  portBASE_TYPE xHigherPriorityTaskWoken=pdFALSE;
  xSemaphoreGiveFromISR( event[event_bit], &xHigherPriorityTaskWoken );
    
  if( pdTRUE == xHigherPriorityTaskWoken )
  {
    portYIELD_FROM_ISR();
  }

  return OS_SUCCESS;
}
#endif
/*-----------------------------------------------------------*/

void os_delay (hcc_u32 ms)
{
  if (configTICK_RATE_HZ<1000)
  {
    hcc_u32 tmp=1000/configTICK_RATE_HZ;
    ms=(ms+tmp-1)/tmp;
  }
  else if (configTICK_RATE_HZ>1000)
  {
    ms*=(configTICK_RATE_HZ/1000);
  }
  vTaskDelay(ms);
}
/*-----------------------------------------------------------*/
int usb_os_interface_init(void)
{
#if OS_MUTEX_COUNT || OS_EVENT_BIT_COUNT
  unsigned int i;
#endif
#if OS_EVENT_BIT_COUNT
  for (i=0;i<OS_EVENT_BIT_COUNT;i++) event[i]=NULL;
#endif
#if OS_MUTEX_COUNT
  for (i=0;i<OS_MUTEX_COUNT;i++) mutex[i]=NULL;
#endif
  /* Setup to use the external interrupt controller. */
  portDISABLE_INTERRUPTS( );
  return OS_SUCCESS;
}
/*-----------------------------------------------------------*/

int os_start (void)
{
  vTaskStartScheduler();
  return OS_SUCCESS;
}


/****************************** END OF FILE **********************************/
