
/****************************************************************************
*
*            Copyright (c) 2008-2010 by HCC Embedded
*
* This software is copyrighted by and is the sole property of
* HCC.  All rights, title, ownership, or other interests
* in the software remain the property of HCC.  This
* software may only be used in accordance with the corresponding
* license agreement.  Any unauthorized use, duplication, transmission,
* distribution, or disclosure of this software is expressly forbidden.
*
* This Copyright notice may not be removed or modified without prior
* written consent of HCC.
*
* HCC reserves the right to modify this software without notice.
*
* HCC Embedded
* Budapest 1133
* Vaci ut 76.
* Hungary
*
* Tel:  +36 (1) 450 1302
* Fax:  +36 (1) 450 1303
* http: www.hcc-embedded.com
* email: info@hcc-embedded.com
*
***************************************************************************/

#ifndef _USBD_CONFIG_H_
#define _USBD_CONFIG_H_

/* HCC Embedded generated source */

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    const unsigned char *device_descriptor;
    const unsigned char *string_descriptor;
    int number_of_languages;
    int number_of_strings;
    const unsigned char * const * const * const strings;
    int number_of_configurations;
    const unsigned char * const * const configurations_fsls;
    const unsigned char * const * const configurations_hs;
    const unsigned char * const dev_qualify_fsls;
    const unsigned char * const dev_qualify_hs;
} usbd_config_t;

/* Global definitions: */

extern const usbd_config_t device_cfg_cdc_ser;



#ifdef __cplusplus
}
#endif

#endif /* _USBD_CONFIG_H_ */

/****************************** END OF FILE **********************************/

