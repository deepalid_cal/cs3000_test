/****************************************************************************
 *
 *            Copyright (c) 2006-2010 by HCC Embedded
 *
 * This software is copyrighted by and is the sole property of
 * HCC.  All rights, title, ownership, or other interests
 * in the software remain the property of HCC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of HCC.
 *
 * HCC reserves the right to modify this software without notice.
 *
 * HCC Embedded
 * Budapest 1133
 * Vaci ut 76.
 * Hungary
 *
 * Tel:  +36 (1) 450 1302
 * Fax:  +36 (1) 450 1303
 * http: www.hcc-embedded.com
 * email: info@hcc-embedded.com
 *
 ***************************************************************************/
#ifndef _USBD_CDCSER_CFG_H_
#define _USBD_CDCSER_CFG_H_

#ifdef __cplusplus
extern "C" {
#endif

/* Set this to one if notifications shall be sent over the interrupt channel. 
   This is the way defined by the standard. 
   The built in windows XP driver ignores notifications. */
#define NOTIFY_ON_IT_EP 1

/* The number of serial lines to implement. USB configuration must match
   this value. */
#define N_LINES         1

/* Set this to non 0 value in order to enable send break support. */
#define SUPPORT_SEND_BREAK  1

/* Default settings for serial lines. */
#define DEFAULT_BPS    9600
#define DEFAULT_STOP   LCT_STOP_1
#define DEFAULT_PARITY LCT_PARITY_NONE
#define DEFAULT_CLEN   8


#ifdef __cplusplus
}
#endif

#endif

/****************************** END OF FILE **********************************/
