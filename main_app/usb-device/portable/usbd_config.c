/****************************************************************************
*
*            Copyright (c) 2008-2010 by HCC Embedded
*
* This software is copyrighted by and is the sole property of
* HCC.  All rights, title, ownership, or other interests
* in the software remain the property of HCC.  This
* software may only be used in accordance with the corresponding
* license agreement.  Any unauthorized use, duplication, transmission,
* distribution, or disclosure of this software is expressly forbidden.
*
* This Copyright notice may not be removed or modified without prior
* written consent of HCC.
*
* HCC reserves the right to modify this software without notice.
*
* HCC Embedded
* Budapest 1133
* Vaci ut 76.
* Hungary
*
* Tel:  +36 (1) 450 1302
* Fax:  +36 (1) 450 1303
* http: www.hcc-embedded.com
* email: info@hcc-embedded.com
*
***************************************************************************/
/* definition: */
#include "usbd_config.h" 

/* Configuration descriptor (FullSpeed) */
const unsigned char cfgDesc_cfg_cdc[68] = {
    9,	/* bLength */
    2,	/* bDescriptorType */
    0x43, 0x00,	/* wTotalLength */
    2,	/* bNumInterfaces */
    1,	/* bConfigValue */
    0,	/* iConfiguration */
    0xc0,	/* bmAttributes */
    0x0,	/* bMaxPower */


/* Interface descriptor */
    9,	/* bLength */
    4,	/* bDescriptorType */
    0,	/* bInterfaceNumber */
    0,	/* bAlternateSetting */
    1,	/* bNumEndpoints */
    0x2,	/* bInterfaceClass */
    0x2,	/* bInterfaceSubClass */
    0x0,	/* bInterfaceProtocol */
    0,	/* iInterface */

/* CDC descriptors: header */
    5, 0x24,        /* Length, DescType */
    0,              /* CDCDescSubType header  */
    0x00, 0x12,	/* release version */
    
/* CDC descriptor: union */
    5,	/* bLength */
    0x24,       /* DescType */
    6,              /* CDCDescSubType union  */
    0,	/* master interface */
    1,  /* SlaveIF-0 */

/* CDC descriptor: call mgmt */
    5, 0x24,        /* Length, DescType */
    1,              /* CDCDescSubType call mgmt */
    0,	/* capability */
    1,	/* data interface */
    
/* CDC descriptor: acm */
    4, 0x24,        /* Length, DescType */
    2,              /* CDCDescSubType acm */
    2,	/* bitmask */
	
/* end of CDC descriptors */
    
/* Endpoint descriptor EP_0x81  */
    7,	/* bLength */
    5,	/* bDescriptorType */
    0x81,	/* bEndpointAddress */
    3,	/* bmAttributes */
    0x10, 0x00,	/* wMaxPacketSize */
    10,	/* bInterval */

/* Interface descriptor */
    9,	/* bLength */
    4,	/* bDescriptorType */
    1,	/* bInterfaceNumber */
    0,	/* bAlternateSetting */
    2,	/* bNumEndpoints */
    0xa,	/* bInterfaceClass */
    0x0,	/* bInterfaceSubClass */
    0x0,	/* bInterfaceProtocol */
    0,	/* iInterface */

/* Endpoint descriptor EP_0x82  */
    7,	/* bLength */
    5,	/* bDescriptorType */
    0x82,	/* bEndpointAddress */
    2,	/* bmAttributes */
    0x40, 0x00,	/* wMaxPacketSize */
    0,	/* bInterval */

/* Endpoint descriptor EP_0x02  */
    7,	/* bLength */
    5,	/* bDescriptorType */
    0x02,	/* bEndpointAddress */
    2,	/* bmAttributes */
    0x40, 0x00,	/* wMaxPacketSize */
    0,	/* bInterval */

    0     /* last byte (not in cfg total) */
};
/* ----eof  Full cfg_cdc config Desc ---- */

/* strings generated from XML */

/*
static const unsigned char string_1_en[] = {
0x1a, 3, // length+type
'H', 0,'C', 0,'C', 0,'-', 0,
'E', 0,'m', 0,'b', 0,'e', 0,
'd', 0,'d', 0,'e', 0,'d', 0

};

static const unsigned char string_2_en[] = {
0x2a, 3, // length+type
'C', 0,'D', 0,'C', 0,'-', 0,
'E', 0,'c', 0,'h', 0,'o', 0,
' ', 0,'d', 0,'e', 0,'m', 0,
'o', 0,' ', 0,'d', 0,'e', 0,
'v', 0,'i', 0,'c', 0,'e', 0

};

static const unsigned char string_3_en[] = {
0x0c, 3, // length+type
'V', 0,'1', 0,'.', 0,'0', 0,
'0', 0

};
*/


static const unsigned char string_1_en[] = {
0x1c, 3, /* length+type  */
'C', 0,'A', 0,'L', 0,'S', 0,
'E', 0,'N', 0,'S', 0,'E', 0,
' ', 0,'I', 0,'n', 0,'c', 0,
'.', 0
};

static const unsigned char string_2_en[] = {
0x2c, 3, /* length+type  */
'C', 0,'A', 0,'L', 0,'S', 0,
'E', 0,'N', 0,'S', 0,'E', 0,
' ', 0,'M', 0,'o', 0,'d', 0,
'e', 0,'l', 0,' ', 0,'C', 0,
'S', 0,'3', 0,'0', 0,'0', 0,
'0', 0
};

// Calsense just made up the V1.05 version number. It was V1.00 from HCC.
static const unsigned char string_3_en[] = {
0x0c, 3, /* length+type  */
'V', 0,'1', 0,'.', 0,'0', 0,
'5', 0
};

static const unsigned char * const string_descriptor_en[3]={
 string_1_en,
 string_2_en,
 string_3_en
};   /* eof strDesc */

/* Device descriptor */
static const unsigned char devDesc[] = {
    18,	/* bLength */
    1,	/* bDescriptorType */
    0x10, 0x01,	/* bcdUSB */
    0x2,	/* bDeviceClass */
    0x0,	/* bDeviceSubClass */
    0x0,	/* bDeviceProtocol */
    0x40,	/* bMaxPacketSize */
    0xca, 0xc1,	/* idVendor */
     0xbe, 0xba,	/* idProduct */
    0x00, 0x00,	/* bcdDevice */
    1,	/* iManufacturer */
    2,	/* iProduct */
    3,	/* iSerialNumber */
    1,	        /* bNumConfigurations */
    0   /* no M$ OS string */
};	/* eof devDesc */


/* generated configurations: */

static const unsigned char * const configs_fsls[1]={
  cfgDesc_cfg_cdc
};   /* eof configurations */

/* generated language descriptor: */

static const unsigned char str_desc[4]={
    4, 3, 0x09, 0x04
};   /* eof string desc. */

/* generated Array of string descriptors: */

static const unsigned char * const * const strings[1]={
   string_descriptor_en
};   /* eof strings */

/* generated USB cfg descriptor: */

const usbd_config_t device_cfg_cdc_ser={
      devDesc,
      str_desc,
      1,
      3,
      strings,
      1,  /* num configs */
      configs_fsls,  /* full/low configs */
      (const unsigned char **)0, /* no hs config */
      (const unsigned char *)0,  /* no 2.0 */
      (const unsigned char *)0
};

