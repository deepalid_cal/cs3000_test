/****************************************************************************
 *
 *            Copyright (c) 2006-2010 by HCC Embedded
 *
 * This software is copyrighted by and is the sole property of
 * HCC.  All rights, title, ownership, or other interests
 * in the software remain the property of HCC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of HCC.
 *
 * HCC reserves the right to modify this software without notice.
 *
 * HCC Embedded
 * Budapest 1133
 * Vaci ut 76.
 * Hungary
 *
 * Tel:  +36 (1) 450 1302
 * Fax:  +36 (1) 450 1303
 * http: www.hcc-embedded.com
 * email: info@hcc-embedded.com
 *
 ***************************************************************************/
#include <string.h>
#include <assert.h>
#include "usbd_api.h"
#include "usbd_cdcser.h"
#include "os.h"
#include "usbd_config.h"

/* Set this to one to enable debug tracing. If so a ring buffer will be filled
   with trace info. This can be usefull when debugging the driver. */
#define DEBUG_TRACE     0

/* This class driver is compatible with the API version below. */
#if USBD_MAJOR != 6 || USBD_STD_MAJOR != 7
#error "Incorrect USB driver version."
#endif

/****************************************************************************
 ************************** Macro definitions *******************************
 ***************************************************************************/
/* Mandatory class specific requests. */
#define CDCRQ_SEND_ENCAPSULATED_COMMAND 0x0
#define CDCRQ_GET_ENCAPSULATED_RESPONSE 0x1

/* Optional class specific requests. Windows usbser.sys depends on these. */
#define CDCRQ_SET_LINE_CODING           0x20
#define CDCRQ_GET_LINE_CODING           0x21
#define CDCRQ_SET_CONTROL_LINE_STATE    0x22
#define CDCNOT_SERIAL_STATE             0x20
#define CDCRQ_SEND_BREAK                0x23

/* Optional not implemented class specific requests.
#define CDCRQ_SET_COMM_FEATURE          0x2
#define CDCRQ_GET_COMM_FEATURE          0x3
#define CDCRQ_CLEAR_COMM_FEATURE        0x4

*/

/* This macro will evaluate to an array inicializer list. It will set the
   content of a line coding structure to the defined values. */
#define FILL_LINE_CODING(bps, stop, parity, data_bits) \
  (bps) & 0xff, ((bps)>>8) & 0xff, ((bps)>>16) & 0xff, ((bps)>>24) & 0xff\
  , (hcc_u8)(stop), (hcc_u8)(parity), (hcc_u8)(data_bits)

#if DEBUG_TRACE==1
  #define MK_DBGTRACE(evt, l) (dbg_cstraces[trace_csndx].devt=(evt), dbg_cstraces[trace_csndx++].line=(l))
#else
  #define MK_DBGTRACE(evt, l) ((void)0)
#endif

#define RX_BUFFER_EMPTY(line) (line_info[line].rx_ndx >= line_info[line].rx_length)

#define LE32(l)  ((hcc_u32)(l))

/****************************************************************************
 ************************** Type definitions ********************************
 ***************************************************************************/
typedef enum {
  st_invalid,
  st_idle,
  st_busy
} tr_state_t;

typedef struct {
  hcc_u32 rx_buffer[64/4];   /* holds data recevide from the host */
  hcc_u32 notification[12/4];   /* notification buffer */
  hcc_u32 tx_buf;            /* Buffer for putch() */
#if NOTIFY_ON_IT_EP
  usbd_ep_handle_t it_tx_ep; /* Endpoint handles */
#endif
  usbd_ep_handle_t tx_ep;
  usbd_ep_handle_t rx_ep;
  usbd_transfer_t tx_tr;     /* Transfers */
  usbd_transfer_t rx_tr;
  tr_state_t tx_state;
  tr_state_t rx_state;
#if NOTIFY_ON_IT_EP
  usbd_transfer_t it_tx_tr;
#endif

  OS_MUTEX_TYPE *state_mutex;
  OS_EVENT_BIT_TYPE cfg_event_rx;
  OS_EVENT_BIT_TYPE cfg_event_tx;
  OS_EVENT_BIT_TYPE tx_event;
  OS_EVENT_BIT_TYPE rx_event;
  OS_EVENT_BIT_TYPE it_tx_event;
  hcc_u8 cmd_ifc_ndx;        /* interface indexes */
  hcc_u8 data_ifc_ndx;
  union  {                   /* holds current line conding */
    hcc_u32 align32;
    hcc_u8 data[7];
  } line_coding;
  hcc_u8 rx_length;          /* number of bytes in the RX buffer */
  hcc_u8 rx_ndx;             /* next byte to be read from RX buffer */
  hcc_u8 line_state;         /* line state flags */
  hcc_u8 ls_changed;         /* 1 if line state changed */
  hcc_u8 dtr;                /* state of DTR. set by host */
  hcc_u8 new_line_coding;    /* 1 if line coding changed */
} line_info_t;

typedef enum {
  evt_none = 0,
  evt_drvcb_cmd_epok,
  evt_drvcb_data_epok,
  evt_reset,
  evt_ep0_get_lcoding,
  evt_ep0_get_ser_state_notif,
  evt_ep0_set_l_conding,
  evt_ep0_change_dtr,
  evt_get_ch,
  evt_kbhit_1,
  evt_putch,
  evt_new_tx,
  evt_nex_tx_buf,
  evt_rx_done,
  evt_new_rx,
  evt_it_notif
} dbg_evt;

/****************************************************************************
 ************************** Function predefinitions. ************************
 ***************************************************************************/
/* none */

/****************************************************************************
 ************************** Global variables ********************************
 ***************************************************************************/
#if DEBUG_TRACE==1
struct {
  dbg_evt devt;
  hcc_u8 line;
} dbg_cstraces[0x100];
hcc_u8 trace_csndx=0;
#endif
/****************************************************************************
 ************************** Module variables ********************************
 ***************************************************************************/
static hcc_u8 def_line_coding[7]= {
  FILL_LINE_CODING(DEFAULT_BPS, DEFAULT_STOP, DEFAULT_PARITY, DEFAULT_CLEN)
};

static line_info_t line_info[N_LINES];
/****************************************************************************
 ************************** Function definitions ****************************
 ***************************************************************************/

/*****************************************************************************
 ****************************************************************************/
static void cdcserd_reset_event(void)
{
  int line;
  MK_DBGTRACE(evt_reset, 0);
  for(line=0;line<N_LINES; line++)
  {
    char *p=(char*)line_info[line].notification;
    p[0]=(char)(USBRQT_DIR_IN | USBRQT_TYP_CLASS | USBRQT_RCP_IFC);
    p[1]=CDCNOT_SERIAL_STATE;
    p[6]=2;
    memcpy(line_info[line].line_coding.data, def_line_coding, 7);
    line_info[line].cmd_ifc_ndx=
    line_info[line].data_ifc_ndx=0xff;
#if NOTIFY_ON_IT_EP
    line_info[line].it_tx_ep=
#endif
    line_info[line].tx_ep=
    line_info[line].rx_ep=USBD_INVALID_EP_HANDLE_VALUE;
  }
}

/*****************************************************************************
 * USB callback function. Is called by the USB driver if a non standard
 * request is received from the HOST. This callback extends the known
 * requests by masstorage related ones.
 ****************************************************************************/
void cdcserd_cdrv_cb(const usbd_ep_info_t *eps, const int ifc_ndx, const int para)
{
  (void) para;
  if (eps==NULL)
  {
    cdcserd_reset_event();
    return;
  }
  else if (eps->next==NULL)
  {
#if NOTIFY_ON_IT_EP
    int line;
    /* find the serial line that has this master interface */
    for(line=0;line<N_LINES && line_info[line].cmd_ifc_ndx!=ifc_ndx; line++)
      ;
    /* if no match the configuration is invalid! */
    assert(line<N_LINES);

    MK_DBGTRACE(evt_drvcb_cmd_epok, line);

    line_info[line].it_tx_ep=USBD_INVALID_EP_HANDLE_VALUE;
    if (eps->ep_type == EPD_ATTR_INT &&  (eps->addr & EPD_DIR_TX))
    {
      line_info[line].it_tx_ep=eps[0].eph;
    }
    assert(line_info[line].it_tx_ep != USBD_INVALID_EP_HANDLE_VALUE);
#endif
    return;
  }
  else
  {
    int line;
    /* find the serial line that has this data interface */
    for(line=0;line<N_LINES && line_info[line].data_ifc_ndx!=ifc_ndx; line++)
      ;
    /* if no line found the configuration data is invalid! */
    assert(line<N_LINES);

    MK_DBGTRACE(evt_drvcb_data_epok, line);
    line_info[line].tx_ep=USBD_INVALID_EP_HANDLE_VALUE;
    line_info[line].rx_ep=USBD_INVALID_EP_HANDLE_VALUE;

    while(eps)
    {
      if (eps->ep_type == EPD_ATTR_BULK)
      {
        if (eps->addr & EPD_DIR_TX)
        {
          line_info[line].tx_ep=eps->eph;
        }
        else
        {
          line_info[line].rx_ep=eps->eph;
        }
      }
      eps=eps->next;
    }
    /* both bulk endpoints must be present */
    assert(line_info[line].tx_ep != USBD_INVALID_EP_HANDLE_VALUE
            && line_info[line].rx_ep!=USBD_INVALID_EP_HANDLE_VALUE);
    os_event_set(line_info[line].cfg_event_rx);
    os_event_set(line_info[line].cfg_event_tx);
    return;
  }
}

/*****************************************************************************
 * USB callback function. Is called by the USB driver if a non standard
 * request is received from the HOST. This callback extends the known
 * requests by masstorage related ones.
 ****************************************************************************/
usbd_callback_state_t cdcserd_ep0_event(const usbd_setup_data_t *stp
                                                ,usbd_transfer_t *tr)
{
  int line;
  usbd_callback_state_t r=clbstc_error;

  for(line=0; line<N_LINES; line++)
  {
    if (line_info[line].cmd_ifc_ndx==stp->wIndex
        || line_info[line].data_ifc_ndx==stp->wIndex)
    {
      break;
    }
  }

  /* A request to a command interface. */
  if (line < N_LINES)
  {
    switch(stp->bmRequestType)
    {
    /* Class specific in request. */
    case (USBRQT_DIR_IN | USBRQT_TYP_CLASS | USBRQT_RCP_IFC):
      /* Host wants to get a descriptor */
      switch (stp->bRequest)
      {
      case CDCRQ_GET_LINE_CODING:
        MK_DBGTRACE(evt_ep0_get_lcoding, line);
        if (os_mutex_get(line_info[line].state_mutex) == OS_SUCCESS)
        {
          tr->eph=ep0_handle;
          tr->buffer=(hcc_u8*)&line_info[line].line_coding;
          tr->length=(hcc_u32)(stp->wLength<7 ? stp->wLength : 7);
          tr->dir=USBDTRDIR_IN;
          tr->zp_needed=0;
          if (USBDERR_NONE==usbd_transfer_b(tr))
          {
            r=clbstc_in;
          }
          else
          {
            r=clbstc_error;
          }
          os_mutex_put(line_info[line].state_mutex);
        }
        break;
#if !NOTIFY_ON_IT_EP
      case CDCNOT_SERIAL_STATE:
        MK_DBGTRACE(evt_ep0_get_ser_state_notif, line);
        /* Note: this is a notification and shall not be queryied over the management
           element (ep0). This shall be sent over the notification element (interrupt
           endpoint). */
        if (os_mutex_get(line_info[line].state_mutex) == OS_SUCCESS)
        {
          tr->eph=ep0_handle;
          tr->buffer=(hcc_u8*)&line_info[line].line_state;
          tr->length=(hcc_u32)(stp->wLength<2 ? stp->wLength : 2);
          tr->dir=USBDTRDIR_IN;
          tr->zp_needed=0;
          if (USBDERR_NONE==usbd_transfer_b(tr))
          {
            r=clbstc_in;
            line_info[line].ls_changed=0;
          }
          else
          {
            r=clbstc_error;
          }
          os_mutex_put(line_info[line].state_mutex);
        }
        break;
#endif
      case CDCRQ_GET_ENCAPSULATED_RESPONSE:
      default:
        break;
      }
      break;
    /* Class specific out request. */
    case (USBRQT_DIR_OUT | USBRQT_TYP_CLASS | USBRQT_RCP_IFC):
      switch (stp->bRequest)
      {
#if SUPPORT_SEND_BREAK
      case CDCRQ_SEND_BREAK:
        MK_DBGTRACE(evt_ep0_send_break, stp->wValue);
        r=clbstc_out;
        cdcserd_send_brk(stp->wValue);
        break;
#endif
      case CDCRQ_SET_LINE_CODING:
        MK_DBGTRACE(evt_ep0_set_l_conding, line);
        if (os_mutex_get(line_info[line].state_mutex) == OS_SUCCESS)
        {
          tr->eph=ep0_handle;
          tr->buffer=(hcc_u8*)&line_info[line].line_coding;
          tr->length=7;
          tr->dir=USBDTRDIR_OUT;
          tr->zp_needed=0;
          if (USBDERR_NONE==usbd_transfer_b(tr))
          {
            r=clbstc_out;
            line_info[line].new_line_coding=1;
          }
          else
          {
            r=clbstc_error;
          }

          os_mutex_put(line_info[line].state_mutex);
        }
        break;
      case CDCRQ_SET_CONTROL_LINE_STATE:
        MK_DBGTRACE(evt_ep0_change_dtr, line);
        line_info[line].dtr=(hcc_u8)(stp->wValue & 1);
        r=clbstc_out;
        break;
      case CDCRQ_SEND_ENCAPSULATED_COMMAND:
      default:
        break;
      }
      break;
    default:
      break;
    }
  }
  return(r);
}

/*****************************************************************************
* Name:
*    cdcserd_rx_stm
* In:
*    line - virtual serial line index
*    my_block - 1 if function may block the current task
*
* Out:
*    n/a
*
* Description:
*    Receive state machine. Hande reception on the USB.
*
* Assumptions:
*    --
*****************************************************************************/
static void cdcserd_rx_stm(int line, int may_block)
{
  usbd_error_t err;
  (void)may_block;
  switch (line_info[line].rx_state)
  {
  case st_invalid:
#if !OS_TASK_POLL_MODE
    if (may_block)
    {
      do {
        os_event_get(line_info[line].cfg_event_rx);
      } while(!cdcserd_present(line));
    }
    else
#endif
    {
      if (!cdcserd_present(line))
      {
        break;
      }
    }

    line_info[line].rx_state=st_idle;
  case st_idle:
    /* reset buffer */
    line_info[line].rx_length=0;
    line_info[line].rx_ndx=0;

    /* New transfer need to be made. */
    line_info[line].rx_tr.eph=line_info[line].rx_ep;
    line_info[line].rx_tr.buffer=(hcc_u8*)line_info[line].rx_buffer;
    line_info[line].rx_tr.dir=USBDTRDIR_OUT;
    line_info[line].rx_tr.length=sizeof(line_info[0].rx_buffer);
#if OS_TASK_POLL_MODE
    err=usbd_transfer(&line_info[line].rx_tr);
#else
    if (may_block)
    {
      err=usbd_transfer_b(&line_info[line].rx_tr);
    }
    else
    {
      err=usbd_transfer(&line_info[line].rx_tr);
    }
#endif
    if (USBDERR_NONE==err || USBDERR_BUSY==err)
    {
      line_info[line].rx_state=st_busy;
    }
    else
    {
      line_info[line].rx_state=st_invalid;
      break;
    }
  case st_busy:
#if OS_TASK_POLL_MODE
    err=usbd_transfer_status(&line_info[line].rx_tr);
#else
    do {
      err=usbd_transfer_status(&line_info[line].rx_tr);
      if (may_block && USBDERR_BUSY==err)
      {
        os_event_get(*line_info[line].rx_tr.event);
      }
    } while(may_block && USBDERR_BUSY==err);
#endif
    if (USBDERR_BUSY==err)
    {
      break;
    }
    else if (USBDERR_NONE==err)
    {
      line_info[line].rx_state = st_idle;
      /* set new buffer length */
      line_info[line].rx_ndx=0;
      line_info[line].rx_length=(hcc_u8)line_info[line].rx_tr.csize;
    }
    else if (USBDERR_COMM==err)
    {
      line_info[line].rx_state=st_idle;
    }
    else
    {
      line_info[line].rx_state = st_invalid;
    }
  }/* switch */
}

/*****************************************************************************
* Name:
*    cdcserd_rx_chars
* In:
*    line - index of communicatin channel
*
* Out:
*    number of characters available
*
* Description:
*    Get the number of characters in the rx buffer.
*
* Assumptions:
*    --
*****************************************************************************/
unsigned int cdcserd_rx_chars (int line)
{
  if (RX_BUFFER_EMPTY(line))
  {
    /* Drive reception. */
	//cdcserd_rx_stm(line, 0);
	cdcserd_rx_stm( line, 1 );  // RMD 2010.12.14 Steve Ridley asked me to change this to a 1 from a 0 so the function would block
  }

  return((unsigned int)(line_info[line].rx_length - line_info[line].rx_ndx));
}

/*****************************************************************************
* Name:
*    cdcserd_getch
* In:
*    N/A
*
* Out:
*    N/A
*
* Description:
*    Get the next character from rx_buffer.
*
* Assumptions:
*    --
*****************************************************************************/
int cdcserd_getch(int line)
{
  int r;
  MK_DBGTRACE(evt_get_ch, line);

  while (RX_BUFFER_EMPTY(line))
  {
    cdcserd_rx_stm(line, 1);
  }

  /* Return the next character, and advance read index. */
  r=(int)((hcc_u8*)line_info[line].rx_buffer)[line_info[line].rx_ndx];
  line_info[line].rx_ndx++;
  return(r);
}

/*****************************************************************************
* Name:
*    cdcserd_receive
* In:
*    N/A
*
* Out:
*    N/A
*
* Description:
*    Get the next character from rx_buffer.
*
* Assumptions:
*    --
*****************************************************************************/
unsigned int cdcserd_receive(int line, void* buffer, unsigned int len)
{
  unsigned int chunk=cdcserd_rx_chars(line);

  if (chunk > len)
  {
    chunk=len;
  }

  if (chunk)
  {
    memcpy( buffer, ((hcc_u8*)line_info[line].rx_buffer)+line_info[line].rx_ndx, chunk );

	line_info[line].rx_ndx+=chunk;
  }

  return(chunk);
}

/*****************************************************************************
* Name:
*    cdcserd_kbhit
* In:
*    N/A
*
* Out:
*    N/A
*
* Description:
*    Will return one, if the rx_buffer contains any unread characters.
*
* Assumptions:
*    --
*****************************************************************************/
int cdcserd_kbhit(int line)
{
  /* If there is an unread character (read index not reached end of buffer)
     retun 1. */
  if (!RX_BUFFER_EMPTY(line))
  {
    MK_DBGTRACE(evt_kbhit_1, line);
    return(1);
  }
  /* buffer is empty, get next chunk from USB */
  else
  {
    cdcserd_rx_stm(line, 0);
    if (!RX_BUFFER_EMPTY(line))
    {
      MK_DBGTRACE(evt_kbhit_1, line);
      return(1);
    }
  }
  return(0);
}

/*****************************************************************************
* Name:
*    cdcserd_send
* In:
*    line - index of communication channel to use
*    buffer - buffer with data to be sent
*    len - number of bytes to be sent
*
* Out:
*    0 - no error
*    other - failed
*
* Description:
*    Send multiple characters.
*
* Assumptions:
*    Putch and _send shall not be used in parallel for the same channel.
*****************************************************************************/
int cdcserd_send(int line, void* buffer,  unsigned int len)
{
  usbd_error_t err;
  MK_DBGTRACE(evt_send, line);

  switch(line_info[line].tx_state)
  {
  case st_invalid:
#if !OS_TASK_POLL_MODE
      os_event_get(line_info[line].cfg_event_tx);
#endif

    if (!cdcserd_present(line))
    {
      return CDC_FAILED;
    }
    line_info[line].tx_state=st_idle;

  case st_idle:
     /* Start sending next chunk. */
    line_info[line].tx_tr.eph=line_info[line].tx_ep;
    line_info[line].tx_tr.length=len;
    line_info[line].tx_tr.dir=USBDTRDIR_IN;
    line_info[line].tx_tr.zp_needed=1;
    line_info[line].tx_tr.buffer=(void*)buffer;
#if OS_TASK_POLL_MODE
    err=usbd_transfer(&line_info[line].tx_tr);
    if (USBDERR_NONE==err || USBDERR_BUSY==err)
    {
      line_info[line].tx_state=st_busy;
    }
    else
    {
      line_info[line].tx_state=st_invalid;
      return CDC_FAILED;
    }
#else
    err=usbd_transfer_b(&line_info[line].tx_tr);
    if (USBDERR_NONE!=err)
    {
      line_info[line].tx_state=st_invalid;
      return CDC_FAILED;
    }
#endif
    break;
  case st_busy:
    (void)cdcserd_get_tx_status(line);
     return CDC_FAILED;
  }
  return CDC_SUCCESS;
}

/*****************************************************************************
* Name:
*    cdcserd_get_tx_status
* In:
*    line - index of communication channel to use
*
* Out:
*    USBDERR_XXX value
*
* Description:
*    Get status of previous transfer.
*
* Assumptions:
*****************************************************************************/
int cdcserd_get_tx_status(int line)
{
  usbd_error_t err=USBDERR_NONE;
  switch(line_info[line].tx_state)
  {
  case st_busy:
    err=usbd_transfer_status(&line_info[line].tx_tr);
    if (USBDERR_NONE==err)
    {
      line_info[line].tx_state=st_idle;
    }
    else if (USBDERR_BUSY != err)
    {
      line_info[line].tx_state=st_invalid;
    }
    break;
  case st_invalid:
    err=USBDERR_INVALIDEP;
    break;
  case st_idle:
    /* keep no error return value */
    break;
  }
  return(err);
}

/*****************************************************************************
* Name:
*    cdcserd_putch
* In:
*    N/A
*
* Out:
*    N/A
*
* Description:
*    Send one character.
*
* Assumptions:
*    Putch and _send shall not be used in parallel for the same channel.
*****************************************************************************/
int cdcserd_putch(int line, char c)
{
  int r=(int)c;

  line_info[line].tx_buf=(hcc_u32)c;
  if (cdcserd_send(line, &line_info[line].tx_buf, sizeof(c)))
  {
    r++;
  }

  return(r);
}

/*****************************************************************************
 *
 ****************************************************************************/
int cdcserd_line_coding_changed(int line)
{
  int r=0;
  if (os_mutex_get(line_info[line].state_mutex) == OS_SUCCESS)
  {
    if(line_info[line].new_line_coding)
    {
      line_info[line].new_line_coding=0;
      r=1;
    }
    os_mutex_put(line_info[line].state_mutex);
  }
  return(r);
}

/*****************************************************************************
 *
 ****************************************************************************/
#if 0

void cdcserd_get_line_coding(int line, line_coding_t *l)
{
  /* Since this data can be modified by other task during the read
     we first copy it to a temporary storage. If the copy is ok, we return
     values from there. */
  union {
    hcc_u32 dummy;
    hcc_u8 data[7];
  } d;

  do {
    memcpy(d.data, line_info[line].line_coding.data, 7);
  } while(memcmp(d.data, line_info[line].line_coding.data, 7));

  l->bps=LE32(*(hcc_u32*)d.data);
  l->nstp=d.data[4];
  l->parity=d.data[5];
  l->ndata=d.data[6];
}

/*****************************************************************************
 *
 ****************************************************************************/
int cdcserd_set_line_coding(int line, line_coding_t *l)
{
  if (os_mutex_get(line_info[line].state_mutex) == OS_SUCCESS)
  {

    *(hcc_u32*)line_info[line].line_coding.data=LE32(l->bps);
    line_info[line].line_coding.data[4]=l->nstp;
    line_info[line].line_coding.data[5]=l->parity;
    line_info[line].line_coding.data[6]=l->ndata;
    os_mutex_put(line_info[line].state_mutex);
    return(0);
  }
  return(1);
}

#endif
/*****************************************************************************
 *
 ****************************************************************************/
int cdcserd_parse_cb(const hcc_u8 *start, const hcc_u8 *end)
{
  int line;
  /* this is the master interface */
  if (0x02==start[5] && 0x02==start[6] && 0x00 == start[7])
  {
    /* find a non used line info structure */
    for(line=0;line<N_LINES && 0xff!=line_info[line].cmd_ifc_ndx; line++)
      ;
    assert(line<N_LINES);

    /* remember master interface index */
    line_info[line].cmd_ifc_ndx=start[2];

    /* look for the CDC union descriptor */
    for(;;)
    {
      start=usbd_find_descriptor(usbd_next_descriptor(start),end,0x24);
      if (start<end)
      {
        if (0x06==start[2])
        {
          /* remember data interface index */
          line_info[line].data_ifc_ndx=start[4];
          /* return success */
          return 0;
        }
      }
      else
      {
        /* return failure */
        return 1;
      }
    }
  }
  else if (0x0a==start[5] && 0x00==start[6] && 0x00 == start[7])
  {
    /* look for a serial line which own this data interface */
    for(line=0;line<N_LINES && start[2]!=line_info[line].data_ifc_ndx; line++)
      ;
    /* return success if this is a cdc-ser data interface */
    return(!(line<N_LINES));
  }

  /* return failure */
  return 1;
  
}

/*****************************************************************************
* Name:
*    cdcserd_init
* In:
*    N/A
*
* Out:
*    0: success
*    1: error
*
* Description:
*    Inicialize CDC driver.
*
* Assumptions:
*    --
*****************************************************************************/
int cdcserd_init(void)
{
  int r=OS_SUCCESS;
  int line;

  memset(line_info, 0, sizeof(line_info));
  for(line=0; line<N_LINES; line++)
  {
    if (!r) r=os_event_create(&line_info[line].tx_event);
    if (!r) line_info[line].tx_tr.event=&line_info[line].tx_event;
    if (!r) r=os_event_create(&line_info[line].rx_event);
    if (!r) line_info[line].rx_tr.event=&line_info[line].rx_event;
    if (!r) r=os_event_create(&line_info[line].cfg_event_rx);
    if (!r) r=os_event_create(&line_info[line].cfg_event_tx);
#if NOTIFY_ON_IT_EP
    if (!r) r=os_event_create(&line_info[line].it_tx_event);
    if (!r) line_info[line].it_tx_tr.event=&line_info[line].it_tx_event;
#endif
    if (!r) r=os_mutex_create(&line_info[line].state_mutex);
    line_info[line].tx_state=line_info[line].rx_state=st_invalid;
    line_info[line].rx_ndx=line_info[line].rx_length=0;
  }

  cdcserd_reset_event();

  if (!r) r=usdb_register_cdrv(0x2, 0x2, 0x0, cdcserd_cdrv_cb, 0, cdcserd_ep0_event, cdcserd_parse_cb);
  if (!r) r=usdb_register_cdrv(0xa, 0x0, 0x0, cdcserd_cdrv_cb, 0, cdcserd_ep0_event, cdcserd_parse_cb);

  return(r);
}

/*****************************************************************************
 *
 ****************************************************************************/
int cdcserd_start(void)
{
  cdcserd_reset_event();
  return(OS_SUCCESS);
}

/*****************************************************************************
 *
 ****************************************************************************/
int cdcserd_get_dtr(int line)
{
  return (line_info[line].dtr);
}

/*****************************************************************************
 *
 ****************************************************************************/
int cdcserd_set_lsflags(int line, int flags)
{
  MK_DBGTRACE(evt_it_notif, line);
  if (flags != line_info[line].line_state)
  {
    hcc_u8 *p=(hcc_u8 *)line_info[line].notification;

#if NOTIFY_ON_IT_EP
    if (USBDERR_BUSY==usbd_transfer_status(&line_info[line].it_tx_tr))
    {
      return(1);
    }
    line_info[line].line_state = (hcc_u8)flags;
#endif

    p[4]=line_info[line].cmd_ifc_ndx;
    p[8]=line_info[line].line_state;                /* Data lo */
    p[9]=0;                                         /* Data hi */
#if NOTIFY_ON_IT_EP
    line_info[line].it_tx_tr.eph=line_info[line].it_tx_ep;
    line_info[line].it_tx_tr.length=10;
    line_info[line].it_tx_tr.buffer=(hcc_u8 *)line_info[line].notification;
    line_info[line].it_tx_tr.zp_needed=0;
    line_info[line].it_tx_tr.dir=USBDTRDIR_IN;
    if (USBDERR_BUSY != usbd_transfer(&line_info[line].it_tx_tr))
    {
      return(1);
    }
#endif
  }
  return(0);
}

/*****************************************************************************
 *
 ****************************************************************************/
int cdcserd_present(int line)
{
  return( (line_info[line].tx_ep != USBD_INVALID_EP_HANDLE_VALUE) && (line_info[line].rx_ep!=USBD_INVALID_EP_HANDLE_VALUE) );
}
/****************************** END OF FILE **********************************/
