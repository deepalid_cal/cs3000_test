USB CDC Abstract Control Model Serial Emulation driver
======================================================
July 29, 2010
--------------------------------

Files
  usbd_cdcser.c
  usbd_cdcser.h
  usbd_cdcser_cfg.h         Class driver configuration file.

Note:
  Configuration files are usually stored in a project specific directory. The
  source assumes a "include path" for the compiler is set on the command line
  or in project options to this directory.


This driver implements an USB Communication Device Class (version 1.1) compliant
"USB abstract control model serial emulation" device. Shortly this is an USB to
serial line converter device.

Due to Windows XP driver limitations the PC->Device buffer (rx buffer) size is
limited to the size of one packet. The limitation is needed because Windows
will not terminate transfers with a length exact multiple of the packet size by
a 0 length packet. Windows released a hot-fix with the ID KB943198 to solve
this problem. Unfortunately this hot-fix it not automatically installed and
can be requested only by email.

If the device emulates multiple COM ports windows XP needs two hot fixes for
proper operation. One is KB918365 and the second is KB935892. Without these
windows will recognize the COM port, but data communication will not work.
To easy the changes need to be done in the registry to enable KB935892 we
provide KB835892.reg. Right-click it in explorer and click merge to enable
the fix for the demo device.

Configuration
  The driver can be configured using C pre-processor macro definitions. Most of
  these can be found in usbd_cdcser_cfg.h.

  DEBUG_TRACE
    When this macro is defined with a non 0 value, then the CDC driver will put
    trace values to a ring buffer. This trace is only useful when debugging
    CDC driver problems and by default it is turned off. This macro is defined
    in the beginning of usbd_cdcser.c.

  NOTIFY_ON_IT_EP
    The CDC standard requires notifications to be sent over the interrupt IN
    endpoint. Unfortunately the built-in driver of Windows XP seems to ignore
    any notifications sent this way.
    The "HCC Vcomm" driver queries these notifications over the control channel.
    When this macro is defined with a non 0 value, then notifications are sent
    as per the standard. Otherwise they can be queried over the control
    channel. By default the value is set to 1.
    
  SUPPORT_SEND_BREAK
    Set this to non 0 value in order to enable send break support. If enabled
    and the host is issuing a send_break request, then the class driver will
    call the "cdc_send_brk()" call-back function (in usbd_ep0_task context).    

  N_LINES
    The number of emulated serial ports the driver can support. Each serial
    port needs proper definitions in the USB configuration. Thus  changing this
    definition only will probably make your device not working.
    This definition must be equal to the number of serial lines in the USB
    configuration.  Note: a value greater than in the USB configuration will
    work, but will waste data memory space.

  DEFAULT_BPS
  DEFAULT_STOP
  DEFAULT_PARITY
  DEFAULT_CLEN
    Default line coding properties. These values can be read out by the USB
    host after the class driver is reset.

    DEFAULT_BPS
      The baud rate.
    DEFAULT_STOP
      The number of stop bits.
    DEFAULT_PARITY
      The default parity type.
    DEFAULT_CLEN
      The length of a character in bits.

Programmesr API
  int cdc_init(void);
    Initialize class-driver. Must be called before any other class-driver
    function.

  int cdc_start(void);
    Start the driver.

  int cdc_present(void);
    Returns non 0 if any CDC line is active.

  int cdc_putch(int line, char c);
    Send one character. Returns the value of "c" if the character is en-queued
    successfully.

  int cdc_send(int line, void* buffer, unsigned int len);
    Send multiple characters. Returns 0 if buffer is en-queued successfully. The
    buffer shall not be changed while cdc_get_tx_status returns non 0 value.

  int cdc_get_tx_status(int line);
    Return status of previous transfer. Has to be called periodically to see
    if the buffer locked by cdc_send() is still locked or not.

  int cdc_kbhit(int line);
    Check if data is pending in the input buffer. Returns non 0 if there are
    any characters to be read.

  int cdc_getch(int line);
    Read one character from input buffer.

  unsigned int cdc_receive(int line, void* buffer, unsigned int len);
    Read multiple characters from input buffer. Returns the number of characters
    read.

  unsigned int cdc_rx_chars (int line);
    Return number of characters pending in the input buffer. These can be read
    either by cdc_getch() or cdc_receive().

  int cdc_get_dtr(int line);
    Get the status of the emulated DTR line. DTR is set by the host when COM
    port has been opened.

  int cdc_set_lsflags(int line, int flags);
    Set line state flags. See LS_XXXX values.

  int cdc_line_coding_changed(int line);
    Check if line coding has been change by the host.

  void cdc_get_line_coding(int line, line_coding_t *l);
    Get current active line coding.

  int cdc_set_line_coding(int line, line_coding_t *l);
    Set line coding.
    
  void cdc_send_brk(hcc_u16 interval);
    This call-back function is called by the class driver (in context of
    usbd_ep0_task) when the host is executing a SEND_BREAK request. The
    parameter specifies the length for the break signal to be generated.
    Interval value 0xffff means the break signaling shall be kept active
    till the next send break request (indefinite length).

======= END OF FILE ===========================================================

