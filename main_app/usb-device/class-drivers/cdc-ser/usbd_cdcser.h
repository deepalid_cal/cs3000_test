/****************************************************************************
 *
 *            Copyright (c) 2006-2010 by HCC Embedded
 *
 * This software is copyrighted by and is the sole property of
 * HCC.  All rights, title, ownership, or other interests
 * in the software remain the property of HCC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of HCC.
 *
 * HCC reserves the right to modify this software without notice.
 *
 * HCC Embedded
 * Budapest 1133
 * Vaci ut 76.
 * Hungary
 *
 * Tel:  +36 (1) 450 1302
 * Fax:  +36 (1) 450 1303
 * http: www.hcc-embedded.com
 * email: info@hcc-embedded.com
 *
 ***************************************************************************/
#ifndef _USB_CDCSER_H_
#define _USB_CDCSER_H_

#include "usbd_api.h"
#include "usbd_cdcser_cfg.h"

#ifdef __cplusplus
extern "C" {
#endif

#define LCT_PARITY_NONE 0
#define LCT_PARITY_ODD  1
#define LCT_PARITY_EVEN 2
#define LCT_PARITY_MARK 3
#define LCT_PARITY_SPACE 4

#define LCT_STOP_1  0
#define LCT_STOP_15 1
#define LCT_STOP_2  2

typedef struct {
  hcc_u32 bps;
  hcc_u8 ndata;
  hcc_u8 nstp;
  hcc_u8 parity;
} line_coding_t;

#define CDC_SUCCESS 0
#define CDC_FAILED  1

/* Data carrier detect. Asserted when a connection has been established with
   remote equipment. */
#define LS_DCD  (1<<0)
/* Data Set Ready. Asserted to indicate an active connection. */
#define LS_DSR  (1<<1)
/* Break detection state. */
#define LS_BRAK (1<<2)
/* Ring indicator state. */
#define LS_RING (1<<3)
/* Framing error detected. */
#define LS_FE   (1<<4)
/* Parity error detected. */
#define LS_PE   (1<<5)
/* Overrun error detected. */
#define LS_OE   (1<<6)

/* Initialize class driver. */
int cdcserd_init(void);

/* Start class driver. */
int cdcserd_start(void);

/* Returns one if CDC line is active. */
int cdcserd_present(int line);

/* Send one character. */
int cdcserd_putch(int line, char c);
/* Send multiple characters. */
int cdcserd_send(int line, void* buffer, unsigned int len);
/* Return status of previous transfer. */
int cdcserd_get_tx_status(int line);

/* Check if data is pending in the input buffer. */
int cdcserd_kbhit(int line);
/* Read one character from input buffer. */
int cdcserd_getch(int line);
/* Read multiple characters from input bufer. */
unsigned int cdcserd_receive(int line, void* buffer, unsigned int len);
/* Return number of buffered characters. */
unsigned int cdcserd_rx_chars (int line);

/* DTR is set by the host when COM port has been opened.*/
int cdcserd_get_dtr(int line);
/* Set line state flags. See LS_XXXX. */
int cdcserd_set_lsflags(int line, int flags);

/* Check if line coding has been change by the host. */
int cdcserd_line_coding_changed(int line);
/* Get current active line coding. */
void cdcserd_get_line_coding(int line, line_coding_t *l);
/* Set line coding. */
int cdcserd_set_line_coding(int line, line_coding_t *l);

/* USB driver interface functions. */
usbd_callback_state_t cdcserd_ep0_event(const usbd_setup_data_t *stp
                                                , usbd_transfer_t *tr);
void cdcserd_cdrv_cb(const usbd_ep_info_t *eps, const int ifc_ndx, const int para);

/* Application call-back. This shall be implemented by the application. 
   The function will be called by the CDC class-driver. */
void cdcserd_send_brk(hcc_u16 interval);

#ifdef __cplusplus
}
#endif

#endif

/****************************** END OF FILE **********************************/
