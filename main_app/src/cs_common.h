/*  file = cs_common.h                        2011.05.05 rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_CS_COMMON_H
#define _INC_CS_COMMON_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

// Use this as the function header.
// <<header start>>
/* ---------------------------------------------------------- */
/**
	@DESCRIPTION (none)
	
	@CALLER_MUTEX_REQUIREMENTS (none)
	
	@MEMORY_RESPONSIBILITES (none)
	
	@EXECUTED Executed within the context of the xxx task.
	
	@RETURN (none)
	
	@ORIGINAL 2012.09.14 rmd
	
	@REVISIONS (none)
*/
// <<header end>>

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/*
Convenient bit field definition that eliminates the need for a discrete union to set the 
size and then have to pass through to get at the items. Can reference them directly when 
using this structure like this:
 
NOTE: the minimum size using this construct seems to be 4 bytes.

typedef struct { 
    union {
		UNS_64	overall_size;

		struct
		{
			// NOTE: In GCC this is the Least Significant Bit (Little Endian)

			UNS_32				two_digit_number					: 2;

			BITFIELD_BOOL		true_falsey							: 1;

			// NOTE: This is the MS bit in GCC.
		};
	};
} BIG_BIT_FIELD_STRUCT;

BIG_BIT_FIELD_STRUCT	bbf;
 
bbf.true_falsey
*/ 

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// LPC3250 NOTE
// Only supported in GCC 4.4 or later. This is what we need to allow us to turn off the optimization on only a file.
// This would be used on all files. For the file we don't want optimization on we would set to another level. If we use
// this we should remove the optimization in the config.mk??
// 
// EXAMPLE: control level in a file with pragma
// #pragma GCC optimize ( "Os" )
//
// EXAMPLE: Function by function optimization attribute syntax. Needs GCC 4.4.4 or greater.
// __attribute__((__optimize__("0"))) void Function_Name( void )
//{
//}

/*
void * const EE_RamAddr[ EE_NumberOfItems ] = {	 // this declaration puts it in FLASH
(description: EE_RamAddr is an array of const pointers to void)

const void * EE_RamAddr[ EE_NumberOfItems ] = {	// this declaration puts it a intialized data in SRAM
(EE_RamAddr is an array of pointers to void   (I THINK THE CONST DOES NOTHING))

*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/11/2012 rmd : I think this should the first include file. Because some of the low level
// code (FreeRTOS for example and maybe some of the NXP provided) uses assert. And we want
// to define its behaviour.
//
// 8/20/2014 rmd : NOTE the DEFINE to use ASSERT or not in in the ASSERT.H file. MUST  be
// there to get the FreeRTOS files to compile with ASSERT defined.
#include	"assert.h"

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef NO_DEBUG_PRINTF

	// Debug I/O support .. but does NOT redefine printf to the debug version debug_printf.
	// If you want to do that include __debug_stdio.h instead of __cross_studio.h
	#include <__cross_studio_io.h>

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Important defines that control low level behaviour of the alert subsystem.

// 6/8/2012 rmd : During startup there are many normal initializations and file finds that
// occur. Which generate alot of alert lines. This define causes all normal results lines to
// be supressed. Leaving alert lines on startup only when the unexpected occurs. Such as a
// battery backed structure fails its intact test, or a file is not found, or a file size
// has changed.

#define ALERT_SHOW_ROUTINE_FILE_DELETION					(0)

#define ALERT_SHOW_ROUTINE_FILE_WRITES						(1)

#define ALERT_SHOW_ROUTINE_FILE_FINDS_DURING_STARTUP		(0)

#define ALERT_SHOW_ROUTINE_INITIALIZATIONS_DURING_STARTUP	(0)

// -----------------------------

// 6/8/2012 ajv : When we're generating sanity check and range check failures,
// we've added the ability to include the location in the code where the check
// fails. However, once we RTM, we may not want this information to be visible,
// so setting this define to 0 will suppress that information. An added benefit
// is that the code and data size decrease by just over 16k and 1k,
// respectively.
#define ALERT_INCLUDE_FILENAME								(1)

// -----------------------------

// 10/7/2013 rmd : This define saves about 100 bytes of data space for the
// strings and functions involved in generating fake data to test the Comm
// Server. The code is automatically removed by the linker if the function is
// never called. But apparently not the string definitions.
// 
// Disabling this also frees about 5K of code space as the functions used to
// generate report data are removed since the calls are suppressed.
//
// 2/18/2016 rmd : Added the ifdef about only if in DEBUG. To protect the release code from
// ever having these functions compiled in (if developer accidentally leaves defined 1).
#ifdef DEBUG
	#define ALERTS_ABILITY_TO_GENERATE_ALL_ALERTS_AND_REPORTS_FOR_DEBUG		(0)
#endif	

// ----------

#define ALERT_MESSAGE_NORMAL
//#define ALERT_MESSAGE_WRITES_TO_DEBUG_PRINTF
//#define ALERT_MESSAGE_HALTS_AT_CALLER_LOCATION
//#define ALERT_MESSAGE_GENERATES_NO_CODE_AT_ALL

#if defined(ALERT_MESSAGE_NORMAL)
	#if (defined(ALERT_MESSAGE_HALTS_AT_CALLER_LOCATION) || defined(ALERT_MESSAGE_WRITES_TO_DEBUG_PRINTF) || defined(ALERT_MESSAGE_GENERATES_NO_CODE_AT_ALL))
		#error ONLY ONE ALERT MESSAGE TYPE CAN BE DEFINED!
	#endif
#endif

#if defined(ALERT_MESSAGE_WRITES_TO_DEBUG_PRINTF)
	#if (defined(ALERT_MESSAGE_HALTS_AT_CALLER_LOCATION) || defined(ALERT_MESSAGE_NORMAL) || defined(ALERT_MESSAGE_GENERATES_NO_CODE_AT_ALL))
		#error ONLY ONE ALERT MESSAGE TYPE CAN BE DEFINED!
	#endif
#endif

#if defined(ALERT_MESSAGE_HALTS_AT_CALLER_LOCATION)
	#if (defined(ALERT_MESSAGE_NORMAL) || defined(ALERT_MESSAGE_WRITES_TO_DEBUG_PRINTF) || defined(ALERT_MESSAGE_GENERATES_NO_CODE_AT_ALL))
		#error ONLY ONE ALERT MESSAGE TYPE CAN BE DEFINED!
	#endif
#endif

#if defined(ALERT_MESSAGE_GENERATES_NO_CODE_AT_ALL)
	#if (defined(ALERT_MESSAGE_NORMAL) || defined(ALERT_MESSAGE_WRITES_TO_DEBUG_PRINTF) || defined(ALERT_MESSAGE_HALTS_AT_CALLER_LOCATION))
		#error ONLY ONE ALERT MESSAGE TYPE CAN BE DEFINED!
	#endif
#endif

#if !(defined(ALERT_MESSAGE_NORMAL) || defined(ALERT_MESSAGE_HALTS_AT_CALLER_LOCATION) || defined(ALERT_MESSAGE_WRITES_TO_DEBUG_PRINTF) || defined(ALERT_MESSAGE_GENERATES_NO_CODE_AT_ALL))
	#error NEED TO DEFINE AT LEAST ONE KIND OF ALERT MESSAGE ERROR
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#if ALERT_INCLUDE_FILENAME
	#define ALERT_MESSAGE_WITH_FILE_NAME( s ) Alert_Message_va( s" : %s, %u", RemovePathFromFileName( __FILE__ ), __LINE__ )
#else
	#define ALERT_MESSAGE_WITH_FILE_NAME( s ) Alert_Message( s )
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// If we include the __attribute__ as part of the define my editor doesn't parse the variables
// appropriately and then I can't have a code sense pop-up when my curosr hits them in the code.
// Soooo...leave the __attribute__ piece of the define off

// The ordering of the battery backed log vars first followed by the irri vars second is actually managed by the $(ProjectDir)/System/sdram_placement.xml
// file which we keep in our system directory. The order of the lines controls the linker. This ordering is important so that upon code updates we
// will typically hold the same definition for the alerts pile and some tohers. And this will allow us to not have to reset them on a code update.

// Some interest tidbits about performance. WITHOUT the data cache enabled, the ordering of performance best to worst is: IRAM, SDRAM, SRAM_BATT.
// With the IRAM being the CLEAR winner. Better than twice as fast as the other two. Depending on the code. And the other two are comparable - about
// the same that is. With the caches enabled .... well things are very different and much faster. Hard to say what it means though.


// TIMERS.C also uses this definition to locate its FLOP task switch registers
#define APPLICATION_IRAM_VARS		(section(".iram_vars"))

// 10/19/2012 rmd : The battery backed SRAM section declaration.
#define	BATTERY_BACKED_VARS			(section(".battery_backed_vars"))

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Task Priorities - a list to make them easier to read in the task table.

// The HIGHEST PRIORITY is (configMAX_PRIORITIES-1) which at this time equals (16-1)=15.

#define		PRIORITY_LEVEL_POWERFAIL_TASK_15		(15)
#define		PRIORITY_LEVEL_EPSON_RTC_TASK_14		(14)

// 10/16/2012 rmd : As tasks are created they in general are created at a higher priority
// than the 'startup' task. And therefore the newly created tasks run till they are pending
// on their queue. The RTC task is a special exception to this. We need that task to go
// through several relatively lengthy SPI initi sequences. We therefore use a semaphore
// mechanism to suspend the startup task till the RTC task signals it to proceed.

// 7/16/2012 rmd : Changing this TIMER task priority level will not change the timer task
// priority. It is here for reference and so you are aware of the timer task priority. Go to
// FreeRTOSConfig.h file to actually change the TIMER task priority level, but really be
// aware of what you're doing. Changing the timer task priority can change many behaviors
// and task execution orders.
#define		PRIORITY_LEVEL_FREE_RTOS_TIMER_TASK_13	(13)

// 3/5/2014 rmd : Piority level 12 is not yet used. I've opted to use it as the level to
// elevate the ISP code download task to during the code transfer to the tp micro. It could
// just as well been level 10 or 11 as those also are not used. But i thought if a new high
// prioirty task came into being it could be set at 10 or 11.
#define		PRIORITY_LEVEL_12						(12)
#define		PRIORITY_LEVEL_DURING_ISP_CODE_XFER		(PRIORITY_LEVEL_12)

#define		PRIORITY_LEVEL_11						(11)
#define		PRIORITY_LEVEL_10						(10)

#define		PRIORITY_LEVEL_9						(9)

// 8/4/2016 rmd : I wanted to boost the comm_mngr task above TD_CHECK when processing a
// token. This was to solve a MUTEX DEADLOCK that occurs if td_check interrupts the token
// processing. Which can happen given their relative priorities. While I was at it it seemed
// best to push the comm_mngr above both the keyprocessing and display tasks too. So this is
// where it is. BE CAREFUL if you think you can just move this around. I'd advise you think
// this out for a day or two.
#define		PRIORITY_LEVEL_COMM_MNGR_BOOSTED		(PRIORITY_LEVEL_9)

#define		PRIORITY_LEVEL_8						(8)
#define		PRIORITY_LEVEL_7						(7)
#define		PRIORITY_LEVEL_6						(6)

#define		PRIORITY_LEVEL_5						(5)
#define		PRIORITY_LEVEL_RING_BUFFER_TASK			(PRIORITY_LEVEL_5)
#define		PRIORITY_LEVEL_COMM_MNGR_TASK			(PRIORITY_LEVEL_5)
#define		PRIORITY_LEVEL_CODE_DISTRIBUTION_TASK	(PRIORITY_LEVEL_5)

#define		PRIORITY_LEVEL_4						(4)
#define		PRIORITY_LEVEL_TPL_IN_OUT_TASKS			(PRIORITY_LEVEL_4)

#define		PRIORITY_LEVEL_3						(3)
#define		PRIORITY_LEVEL_SERIAL_DRIVER_TASK		(PRIORITY_LEVEL_3)

#define		PRIORITY_LEVEL_2						(2)

#define		PRIORITY_LEVEL_1						(1)
#define		PRIORITY_LEVEL_WDT_TASK_1				(1)
#define		PRIORITY_LEVEL_STARTUP_TASK_1			(1)

// 7/16/2012 rmd : A reminder the IDLE task is actually priority 0. However this define is
// not actually used when the IDLE task is created.
#define		PRIORITY_LEVEL_FREE_RTOS_IDLE_TASK_0	(0)



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// FreeRTOS ... FreeRTOS.h in turn brings in FreeRTOSConfig.h. */
#include	"FreeRTOS.h"

#include	"task.h"

#include	"semphr.h"

#include	"queue.h"

#include	"timers.h"

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Increment a ring buffer index. The parmater 'index' can be either (variable_name) OR
// (*variable_name). Either approach works.
#define INC_RING_BUFFER_INDEX( index, length )		\
{													\
	(index) += 1;				   					\
													\
	if ( (index) >= (length) )     					\
	{   											\
		(index) = 0;			   					\
	}   											\
}

// Decrement a ring buffer index. The parmater 'index' can be either (variable_name) OR
// (*variable_name). Either approach works.
#define DEC_RING_BUFFER_INDEX( index, length )		\
{													\
	if( (index) == 0 )								\
	{												\
		(index) = (length);		   					\
	}												\
													\
	(index) -= 1;				   					\
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*              CRITICAL SECTION HANDLING                     */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// THERE IS A CONCERN: The following statement can be found in the documentation "FreeRTOS API
// functions should not be called while the scheduler is suspended." To address that read the
// following: RE: vTaskSuspendAll By: Richard (richardbarry ) - 2009-01-22 23:24 There is no problem
// with interrupts attempting to yield while the scheduler is suspended, or interrupts reading and
// writing to queues/semaphores. The scheduler takes care of these situations. If an interrupts
// attempts to yield then the yield is held pending until the scheduler is resumed. Pending yields
// are actually performed within xTaskResumeAll().

// We suspend the scheduler to protect the memory pool from corruption that would occur when multiple tasks are trying to
// take or give a block. There are TWO rules to follow:
//
// 1. While the scheduler is suspended there may be no calls to most FreeRTOS API functions. For example taking or giving a
// mutex. The fallout of this is NO alert lines within the suspended sections. We have done a good job to minimize those
// sections and they contain no alert lines.
//
// 2. ISR's may not call mem_malloc or mem_free. That would cause re-entrancy problems which could certainly lead to
// corruption. Don't do it.

#define calsense_mem_ENTER_CRITICAL() vTaskSuspendAll()

#define calsense_mem_EXIT_CRITICAL() xTaskResumeAll()

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// LPC3250 INTERRUPT PRIORITIES
//
// Interrupt priorities are manifested in software. There is no prioritization mechanism
// within the hardware. With the exception that an FIQ is a higher priority than an IRQ.
// More on that later. Interrupts do not nest in the LPC3250.
// 
// The lower the priority number the higher the priority of processing. Range 0 ..
// MAX_INTERRUPTS (72). But using MAX_INTERRUPTS as a priority level is questionable as it
// brings in the FIQ story.
//
// MUST BE A UNIQUE PRIORITY LEVEL FOR EACH GUY. THE LOWER THE NUMBER THE HIGHER THE
// PRIORITY.

#define INTERRUPT_PRIORITY_powerfail_warning	0

#define INTERRUPT_PRIORITY_speaker_timer		1

#define INTERRUPT_PRIORITY_free_rtos_tick		2

#define INTERRUPT_PRIORITY_keypad_scanner		3

#define INTERRUPT_PRIORITY_usb_dev_hp			4
#define INTERRUPT_PRIORITY_usb_dev_lp			5

#define INTERRUPT_PRIORITY_uart_1				6
#define INTERRUPT_PRIORITY_uart_3				7
#define INTERRUPT_PRIORITY_uart_4				8
#define INTERRUPT_PRIORITY_uart_5				9
#define INTERRUPT_PRIORITY_uart_6				10

#define INTERRUPT_PRIORITY_utility_mstimer		11

// Though this usb interrupt is setup we don't take any action when the int occurs.
#define INTERRUPT_PRIORITY_usb_bd				12

#define INTERRUPT_PRIORITY_dac_i2c_channel		13

#define INTERRUPT_PRIORITY_epson_i2c_channel	14

#define INTERRUPT_PRIORITY_epson_1hz			15


#define INTERRUPT_PRIORITY_port_a_cts			16

#define INTERRUPT_PRIORITY_port_b_cts			17

// 5/29/2015 rmd : Not used in this hardware.
#define INTERRUPT_PRIORITY_port_rre_cts			18


#define INTERRUPT_PRIORITY_port_a_cd			19

#define INTERRUPT_PRIORITY_port_b_cd			20

#define INTERRUPT_PRIORITY_a_to_d				21

/*

Table to use when setting the TRIGGER_TYPE

LOW LEVEL		APR=0	ATR=0

HIGH LEVEL		APR=1	ATR=0

RISING EDGE		APR=1	ATR=1

FALLING EDGE	APR=0	ATR=1

*/



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*               TIMERS USED THROUGHTOUT SYSTEM               */
/* ---------------------------------------------------------- */

#define FreeRTOS_TICK_TIMER						TIMER_CNTR5
#define FreeRTOS_TICK_INT						Timer5_INT

#define SPEAKER_TIMER							TIMER_CNTR4
#define SPEAKER_TIMER_INT						Timer4_Mcpwm_INT
#define SPEAKER_TIMER_CLK_EN					CLKPWR_TIMER4_CLK

#define FreeRTOS_RUN_TIME_STATS_TIMER			TIMER_CNTR3
#define FreeRTOS_RUN_TIME_STATS_TIMER_CLK_EN	CLKPWR_TIMER3_CLK

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define MIST_SUCCESS				0x0000
#define MIST_RESOURCE_IN_USE		0x0003
#define MIST_RESOURCE_BUSY			0x0004
#define MIST_RESOURCE_FULL			0x0005
#define MIST_INVALID_PARAMETER		0x0006
#define MIST_INVALID_HANDLE			0x000C
#define MIST_NOT_ON_LIST			0x0012
#define MIST_RESOURCE_NOT_ENABLED	0x001E

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// easyGUI .... including guilib.h includes the rest of the easyGUI library
// including GuiConst, GuiDisplay, GuiFont, GuiStruct, and GuiVar
#include	"GuiLib.h"

/*
2012.02.15 ajv : CHANGES MADE TO EASYGUI LIBRARY FILES
 
GuiGraph4H.c
 
  1. Removed definition of GuiLib_DisplayBuf since we don't use easyGUI's
     double-buffering feature.
 
  2. Added #include "lcd_init.h" to allow access to Calsense-defined
     GuiLib_DisplayBuf.
 
  3. Resolved "missing braces around initializer" error raised by definition of
     Color4BitPixelPattern. Each 16 byte segment needs to be enclosed in it's
     own set of curley braces as below. (Reported to easyGUI on 2012.02.15 ajv)
 
	const GuiConst_INT8U Color4BitPixelPattern[4][16] =
	  {{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
 
GuiDisplay.h
 
  1. Removed external declarations of GuiDisplayLock, GuiDisplayUnlock,
     and GuiDisplay_Init.
 
  2. Defined empty marcos to replace removed external declarations such as:
 
	#define GuiDisplay_Lock() {}
	#define GuiDisplay_Unlock() {}
	#define GuiDisplay_Init() {}
 
  3. Added #define GuiConst_DISPLAY_BUFFER_EASYGUI to disable "#ifndef 
     GuiConst_DISPLAY_BUFFER_EASYGUI" code. This seems to have been added to the
     file to support easyGUI 6, which we don't have. (NOTE: Reported to easyGUI
     on 2012.02.15 ajv)
 
GuiDisplay.c
 
  1. Uncommented #define LCD_CONTROLLER_TYPE_S1D13705
 
  2. Removed GuiDisplay_Lock and GuiDisplay_Unlock functions.
 
  3. Removed "Command writing for 68000 mode" and "Data writing for 68000 mode"
     sections to resolve "multi-line comment" warning in GCC.  (Reported to
     easyGUI on 2012.02.15 ajv)
 
  4. Within #ifdef LCD_CONTROLLER_TYPE_S1D13705, commented out the variable
     declarations and GuiDisplay_Init procedure since we don't use these.
 
  5. Added #include  "lcd_init.h"
 
  6. In GuiLib_DisplayRefresh, changed "DisplayData[Y][X] =
     GuiLib_DisplayBuf[Y][X];" to "primary_display_buf[Y][X] =
     gui_lib_display_buf[Y][X];"
 
GuiLib.c
 
  1. Added CharCode = NULL and PreviousCharCode = NULL to the top of "static
     GuiConst_TEXT GetCharCode" function to resolve compilation warnings when
     compiling using ARM Release.
 
  2. Added #ifndef GuiConst_SCROLLITEM_BAR_NONE block around "static void
     ScrollBox_ShowBarBlock" function to resolve "'ScrollBox_ShowBarBlock'
     defined but not used" compilation warning when
     -Wunused-variable flag is set. (Reported to easyGUI on 2012.02.15 ajv)
 
  3. In the "GuiConst_INT8U GuiLib_ScrollBox_Redraw" function, add #ifndef
     GuiConst_SCROLLITEM_BAR_NONE" block around the following variable
     declarations to resolve compilation warnings when -Wunused-variable flag is
     set. (Reported to easyGUI on 2012.02.15 ajv)
 
	GuiConst_INT16U StructToCallIndex;
	GuiLib_StructPtr StructToCall;
 
	GuiConst_INT16S SX1, SY1;
	GuiConst_INT16S X1, Y1;
	#ifdef GuiConst_CLIPPING_SUPPORT_ON
		GuiConst_INT16S SX2, SY2;
		GuiConst_INT16S X2, Y2;
		GuiConst_INT16S RemClippingX1, RemClippingY1, RemClippingX2, RemClippingY2;
	#endif
	GuiConst_INT32S BackColor;

    BarMarkerHeight = 0;
 
GuiGraph.c 
 
  1. In the "GuiLib_LinePattern" function, initialized Slope = 0 if (Vertical).
     This value is only used if !Vertical, but the compiler complains that it
     may be used uninitialized.
*/ 

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//	A VARIETY OF DEFINES USED DURING DEVELOPMENT OR FOR SPECIAL BUILDS

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/29/2015 rmd : Development define to help diagnose cellular issues.
//														NORMALLY SET TO (1)!
#define INCLUDE_LANTRONIX_MSG_TRANSACTION_TIME_CODE		(1)

/* ---------------------------------------------------------- */

//														NORMALLY SET TO (0)!
#define	BUILT_FOR_TWO_WIRE_DECODER_ASSEMBLY				(0)

/* ---------------------------------------------------------- */

// 9/10/2018 rmd : A define to control building with variables and code to show stuck keys
// on the keypad. We had a manufacturing problem and needed to see how many controllers
// actively are affected. A midnight alert line for the offending keys does that.
//														NORMALLY SET TO (0)!
#define	SHOW_STUCK_KEY_ALERTS_AT_MIDNIGHT				(1)

/* ---------------------------------------------------------- */

//															NORMALLY SET TO (0)!
#define	SHOW_PACKET_ROUTER_ALERTS								(0)

//															NORMALLY SET TO (0)!
#define	SHOW_PACKET_ROUTER_NOT_ON_HUB_LIST_ALERTS				(0)

//															NORMALLY SET TO (0)!
#define	SHOW_PACKET_ROUTER_hubs_overhearing_one_another			(0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/17/2017 rmd : ONLY DEFINE ONE OF THE FOLLOWING DEFINES AT A TIME.

// 9/7/2017 rmd : Standard production commserver.
#define	PROGRAM_CENTRAL_DEVICE_TO_PORT_16001		(1)

// 9/7/2017 rmd : Our normal, production LR test commserver.
#define	PROGRAM_CENTRAL_DEVICE_TO_PORT_16003		(0)

// 9/7/2017 rmd : Special test commserver port. Be careful with use.
#define	PROGRAM_CENTRAL_DEVICE_TO_PORT_16004		(0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/6/2018 rmd : Wipe out any attempted code distribution by the HUB. This is useful to be
// able to load a binary to a HUB and not have it go through the long winded hub code
// distribution process.
//
//																	NORMALLY SET TO (0)!
#define	KILL_ANY_HUB_CODE_DISTRIBUTION_ATTEMPT							(0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/18/2014 ajv : During chain DEBUG, this define can be used to suppress the
// distribution of code when the master is updated.
//
//																	NORMALLY SET TO (1)!
#define	CODE_DOWNLOAD_ALLOW_MASTER_TO_DISTRIBUTE_CODE					(1)

// 7/14/2016 rmd : During development we may want to say watch the alerts during the code
// distribution. The progress dialog blocks us out from checking other screens (alerts or
// serial port activity for example). This define allows us to suppress the progress dialog.
//
//																	NORMALLY SET TO (1)!
#define	CODE_DOWNLOAD_SHOW_PROGRESS_DIALOG								(1)

// 7/27/2016 rmd : THIS SHOULD BE SET TO A ZERO for the normal release build. When set to a
// 1 allows the developer to supress the code distribution DIALOG BOX for the RELEASE build!
//
// 3/15/2017 rmd : Note that this does not allow the developer to build release code with
// the code distribution disabled. Only allows building of release code with the associated
// dialogs suppressed.
//
//  																NORMALLY SET TO (0)!
#define	CODE_DOWNLOAD_DEFEAT_PROGRESS_DIALOG_RELEASE_BUILD_SAFETY		(0)

/* ---------------------------------------------------------- */

// 8/1/2017 rmd : During the scan it can be very annoying if you are running debug code to
// have the master suddenly reboot! Impossible to debug with that activity taking place.
//
//																	NORMALLY SET TO (1)!
#define DURING_SCAN_ALLOW_MASTER_TO_REBOOT_IF_SLAVE_HAS_REBOOTED		(1)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

