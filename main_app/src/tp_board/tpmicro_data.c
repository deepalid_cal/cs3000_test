/*  file = tpmicro_data.c                    08.03.2012  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"tpmicro_data.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"bithacks.h"

#include	"flash_storage.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/27/2013 rmd : Held in SDRAM and saved to a file in the GENERAL STORAGE flash device.
TPMICRO_DATA_STRUCT	tpmicro_data;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called at program startup. ONLY.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the STARTUP task.
	
	@RETURN (none)

	@ORIGINAL 2012.08.08 rmd

	@REVISIONS (none)
*/
extern void init_tpmicro_data_on_boot( void )
{
	xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );
	
	// ----------
	
	// 3/21/2013 rmd : When the cs3000 code restarts we send all items to the tp micro that
	// contain settings the tp micro uses to operate. We do this to make sure the tp micro is in
	// sync with the latest settings.

	tpmicro_data.send_wind_settings_structure_to_the_tpmicro = (true);
	
	tpmicro_data.request_whats_installed_from_tp_micro = (true);

	tpmicro_data.whats_installed_has_arrived_from_the_tpmicro = (false);

	// ----------
	
	// 9/27/2013 rmd : Set the state to ACK to the TP Micro in the first message we send to it.
	// This is a precaution in case the TPMicro told the main board about a short but then the
	// main board got hit with a restart (bug of some sort or funny power failure). The main
	// board would never acknowledge the short to the tpmicro and the result is the tpmicro will
	// ignore all commands to turn ON valves. Do not need to send to the master though. Just
	// setup to trigger the SHORT ACK to go to the tp micro. No data accompanies that, it is
	// just a bit in pieces.
	tpmicro_data.terminal_short_or_no_current_state = TERMINAL_SONC_STATE_ack_to_tpmicro;

	// 10/28/2013 rmd : Same applies to the two wire cable short state. Get it cleared on
	// program start.
	tpmicro_data.two_wire_cable_excessive_current_xmission_state = TWO_WIRE_CABLE_ANOMALY_STATE_send_ack_to_tpmicro;

	tpmicro_data.two_wire_cable_over_heated_xmission_state = TWO_WIRE_CABLE_ANOMALY_STATE_send_ack_to_tpmicro;
	
	// 11/13/2013 ajv : We don't actually send an ACK to the TP Micro in
	// response to a cooled off command, so set the state to idle.
	tpmicro_data.two_wire_cable_cooled_off_xmission_state = TWO_WIRE_CABLE_ANOMALY_STATE_idle;

	// ----------
	
	// 9/18/2012 rmd : If there is a power failure from the time a pulse is reported to us and
	// the token being sent to the master that pulse will be lost. The tpmicro_data structure is
	// not held in battery backed SRAM. Maybe this will show to be a problem.
	tpmicro_data.et_gage_pulse_count_to_send_to_the_master = 0;

	tpmicro_data.rain_bucket_pulse_count_to_send_to_the_master = 0;

	tpmicro_data.et_gage_runaway_gage_in_effect_to_send_to_master = (false);
	
	// ----------
	
	// 1/29/2013 rmd : Ensure ui activated variables have been cleared. They are only part of
	// this structure for convenience and do not need to be saved to the file. If they
	// accidentally get saved in their active state we clear here.
	tpmicro_data.send_2w_solenoid_location_on_off_command = (false);
	
	tpmicro_data.two_wire_cable_power_operation.send_command = (false);

	tpmicro_data.two_wire_perform_discovery = (false);
	tpmicro_data.two_wire_clear_statistics_at_all_decoders = (false);
	tpmicro_data.two_wire_request_statistics_from_all_decoders = (false);
	tpmicro_data.two_wire_start_all_decoder_loopback_test = (false);
	tpmicro_data.two_wire_stop_all_decoder_loopback_test = (false);
	
	tpmicro_data.two_wire_set_decoder_sn = (false);

	// ----------

	// 10/16/2013 rmd : Not necessarily needed but for cleanliness 0 this out. When the user
	// 'presses' the button to request the discovery to commence this should be set to 0.
	tpmicro_data.decoders_discovered_so_far = 0;

	// ----------

	tpmicro_data.twccm.current_percentage_of_max = 0;

	// ----------

	// 10/24/2014 rmd : Initialize to 0. And that sets the fault code to 0. The not in use
	// value. Which is what we want.
	memset( tpmicro_data.decoder_faults, 0, sizeof(tpmicro_data.decoder_faults) );

	// ----------
	
	// 10/28/2014 rmd : The concept is the filler is always initialized to 0. If the file is
	// saved the 0's will be in the file. In the future if you take part of the filler for a new
	// variable you at least know its present value to be 0.
	memset( tpmicro_data.filler, 0, sizeof(tpmicro_data.filler) );
	
	// ----------

	xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// filenames ARE ALLOWED 32 CHARS ... truncated if exceeds this limit

const char TPMICRO_DATA_FILENAME[] =	"TPMICRO_DATA";

// 1/26/2015 rmd : Starts at 0. And typically should only be incremented by 1 and accounted
// for in the updater function.

#define	TPMICRO_DATA_LATEST_FILE_REVISION		(0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void nm_tpmicro_data_structure_init( void )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only if the
	// tpmicro_data file does not yet exist. The caller is to be holding the tpmicro_data MUTEX.

	memset( &tpmicro_data, 0x00, sizeof(TPMICRO_DATA_STRUCT) );

}

/* ---------------------------------------------------------- */
static void nm_tpmicro_data_updater( UNS_32 pfrom_revision )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only when
	// the file revision is changed. The caller is to be holding the tpmicro_data
	// recursive_MUTEX.

	// ----------
	
	if( pfrom_revision == TPMICRO_DATA_LATEST_FILE_REVISION )
	{
		Alert_Message_va( "TPMICRO DATA file unexpd update %u", pfrom_revision );	
	}
	else
	{
		Alert_Message_va( "TPMICRO DATA file update : to revision %u from %u", TPMICRO_DATA_LATEST_FILE_REVISION, pfrom_revision );

		// ----------
		
		if( pfrom_revision == 0 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 0 to REVISION 1.

			
			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 1 to 2 changes if
			// needed.
			pfrom_revision += 1;
		}
		
		// ----------
		
		/*
		if( pfrom_revision == 1 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 1 to REVISION 2.
				


			pfrom_revision += 1;
		}
		*/
	}

	// ----------

	// 1/29/2015 rmd : Well after the updater has run the pfrom_revision is supposed to equal
	// the latest revision. By design of this update process.
	if( pfrom_revision != TPMICRO_DATA_LATEST_FILE_REVISION )
	{
		Alert_Message( "TPMICRO DATA updater error" );
	}
}

/* ---------------------------------------------------------- */
extern void init_file_tpmicro_data( void )
{
	FLASH_FILE_find_or_create_variable_file(	FLASH_INDEX_1_GENERAL_STORAGE,
												TPMICRO_DATA_FILENAME,
												TPMICRO_DATA_LATEST_FILE_REVISION,
												&tpmicro_data,
												sizeof( tpmicro_data ),
												tpmicro_data_recursive_MUTEX,
												&nm_tpmicro_data_updater,
												&nm_tpmicro_data_structure_init,
												FF_TPMICRO_DATA );
}

/* ---------------------------------------------------------- */
extern void save_file_tpmicro_data( void )
{
	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE,
															TPMICRO_DATA_FILENAME,
															TPMICRO_DATA_LATEST_FILE_REVISION,
															&tpmicro_data,
															sizeof( tpmicro_data ),
															tpmicro_data_recursive_MUTEX,
															FF_TPMICRO_DATA );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/3/2012 rmd : This definition is carefully constructed to place the array itself and the
// strings all in the .rodata section. If not careful you end up with the array in the .data
// section which is the initialized data section. And we don't want it there cause it
// occupies both code space and SDRAM space. Twice the space!
static const char * const (error_strings[]) =
{
	"outgoing message too big",

	"flow readings buffer overflow",
	"flow reading out of range",

	"asked to turn on too many stations",
	"asked to turn on invalid stations",

	"crc failed - main",

	"incoming message has invalid route",

	"incoming message too big",

	"close to main stack overflow",
	"out of memory pool",
	"bumped to next memory block size",
	"queue full",

	"main ring buffer overflow",
	"m1 ring buffer overflow",
	"m2 ring buffer overflow",

	"_m1 packet too big",
	"_m2 packet too big",

	"uart dma error",
	"serial data xmit error",

	"ee mirror test failed",

	"bad msg from main",
	
	"crc failed - m1",
	"crc failed - m2",

	"prior turn-ON request incomplete",

	"i2c states don't match",

	"too many POCs!"
	
};

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Receives an incoming error structure from the tp micro card. Returns the
    length of the delivered data.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task.
	
    @RETURN The size of the structure pulled from the message. Typically used by the caller
    to advance the pointer (into the message) ahead to the next piece of the payload.

	@ORIGINAL 2012.08.03 rmd

	@REVISIONS (none)
*/
extern UNS_32 TPMICRO_DATA_extract_reported_errors_out_of_msg_from_tpmicro( UNS_8 **pdata_ptr )
{
	ERROR_LOG_s		lerror;
	
	memcpy( &lerror, *pdata_ptr, sizeof(ERROR_LOG_s) );
	
	*pdata_ptr += sizeof(ERROR_LOG_s);

	// ------------

	UNS_32	i;
	
	// 8/9/2012 rmd : Always go through and show the alert lines for the newly rcvd error field.
	// The tpmicro ZEROs its copy of the error field as part of sending it to us. So if
	// the bit is set again it signals the error occurred again.
	for( i=0; i<32; i++ )
	{
		if( B_IS_SET( lerror.errorBitField, i ) )
		{
			char	str_128[ 128 ];

			snprintf( str_128, sizeof(str_128), "from error bf - %s", error_strings[ i ] );

			Alert_message_on_tpmicro_pile_M( str_128 );

			// ------------

			// 8/3/2012 rmd : Prepare to write to the tpmicro_data structure. A shared and mutex
			// protected structure. And the B_SET operation are likely not atomic.
			xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );
		
			// 8/20/2012 rmd : We have decided it doesn't make any sense to keep updating the file with
			// the latest newly set bit. In time all the bits could end up set. As a compromise what we
			// will do is keep a 'tally' of all the bits sent to us since the last TP MICRO reboot. This
			// way you can look at the bit field and at least be able to say something like 'since the
			// last start these error occurred'. Not to mention you see the matching alert lines for
			// them.
			B_SET( tpmicro_data.rcvd_errors.errorBitField, i );

			xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );
			
			// ------------
		}
	}

	// 8/3/2012 rmd : ALWAYS return this amount. If the bit is set in pieces we operate on good
	// faith the data is here. What else can we do?
	return( sizeof(ERROR_LOG_s) );
}

/* ---------------------------------------------------------- */
extern CS3000_DECODER_INFO_STRUCT* find_two_wire_decoder( UNS_32 pserial_num )
{
	UNS_32	i;
	
	CS3000_DECODER_INFO_STRUCT	*rv;
	
	rv = NULL;
	
	// 2/5/2013 rmd : A serial number of 0 is invalid. Yet could fool us as that is the default
	// serial number in the array. But is just that the default. Not a decoder.
	if( pserial_num != 0 )
	{
		for( i=0; i<MAX_DECODERS_IN_A_BOX; i++ )
		{
			if( tpmicro_data.decoder_info[ i ].sn == pserial_num )
			{
				rv = &tpmicro_data.decoder_info[ i ];
				
				break;
			}
		}

		if( rv == NULL )
		{
			Alert_Message_va( "Decoder not found sn: %08x", pserial_num );
		}
	}
	else
	{
		Alert_Message( "Looking for Decoder with ser num of 0!" );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

