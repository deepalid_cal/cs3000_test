/*  file = tpmicro_data.h                    08.03.2012  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_TPMICRO_DATA_H
#define _INC_TPMICRO_DATA_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"tpmicro_comm.h"

#include	"foal_defs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef	_MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 8/15/2012 rmd : The measured BOX level current. In milli-amps.
	UNS_32	measured_ma_current;

	// 8/15/2012 rmd : Indicates either the value needs to be sent to the master or the master
	// needs to distribute to all slaves. Depending on the variable of this type. There are
	// two.
	BOOL_32	current_needs_to_be_sent;

} MEAS_MA_FOR_DISTRIBUTION;

typedef struct
{
	// 1/29/2013 rmd : Only when true is the contents sent to the tpmicro as a command.
	BOOL_32	send_command;

	// 1/29/2013 rmd : Set true to turn on, false to turn off.
	BOOL_32	on;
	
} TWO_WIRE_CABLE_POWER_OPERATION_STRUCT;

// 8/16/2012 rmd : To provide some msg content range checking the largest box current we
// accept is 10 amps.
#define MAXIMUM_BOX_MILLI_AMPS		(10000)

// ----------

// 4/15/2013 rmd : There is an array of the DECODER_LIST_STRUCT reflecting all discovered
// decoders the TP Micro knows about (learned through the discovery process).

typedef struct
{
	// ----------
	
	UNS_32									sn;

	// ----------
	
	// 2/1/2013 rmd : Sent to the main board after retrieval from a decoder.
	ID_REQ_RESP_s							id_info;

	// ----------

	// 2/1/2013 rmd : Sent to the main board at the request of the main board.
	DECODER_STATS_s							decoder_statistics;

	// ----------

	// 2/1/2013 rmd : Sent to the main board after retrieval from a decoder.
	STAT2_REQ_RSP_s							stat2_response;

	// ----------
	
	// 2/1/2013 rmd : Kept at the TP Micro and sent with the above statistics. When we send we
	// send as many as will fit into the 512 byte max message. It may take several seconds to
	// update the data for all the decoders on the bus.
	TWO_WIRE_COMM_STATS_PER_DECODER_STRUCT	comm_stats;

	// ----------

} CS3000_DECODER_INFO_STRUCT;

// ----------

// 9/27/2013 rmd : SONC = SHORT_OR_NO_CURRENT


// 9/29/2015 rmd : When a short or no current activity is initiated at the TPMicro all
// further turn ON's and turn OFF's are frozen. In the case of a no current the next turn ON
// is inhibited and there are no turn OFF's. In the case of a short we end up with nothing
// ON after the hunt. And nothing will turn ON till the message from the TPMicro makes its
// round trip to the master and back to us and then back down to the TPMicro. We need this
// behavior for a couple of reasons. One is the short reporting infrastructure can only
// carry the information about one station at a time. So we surely do not want to experience
// say a NO CURRENT and then 400ms later a short. We may overwrite our state if so. Secondly
// in the case of a short we want the master to make the decision to turn OFF the output and
// to disseminate that information to the IRRI side before the next group of turn ON data is
// sent to the TPMicro (otherwise we may turn back ON the shorted station again!).
#define	TERMINAL_SONC_STATE_idle								(0)
#define	TERMINAL_SONC_STATE_send_to_master						(1)
#define	TERMINAL_SONC_STATE_waiting_for_response_from_master	(2)
#define	TERMINAL_SONC_STATE_ack_to_tpmicro						(3)

// ----------

#define	TWO_WIRE_CABLE_ANOMALY_STATE_idle								(0)

// 10/28/2013 rmd : Received from tp_micro. Now needs to be sent to the master.
#define	TWO_WIRE_CABLE_ANOMALY_STATE_send_to_master						(1)

// 10/28/2013 rmd : Sent to the master ... waiting for response.
#define	TWO_WIRE_CABLE_ANOMALY_STATE_waiting_for_reponse_from_master	(2)

// 10/28/2013 rmd : Flag the tp_micro to clear the cable short state so another turn ON
// attempt may be made. Remember the cable overheated state is automatically reset when the
// 2W terminal cools off.
#define	TWO_WIRE_CABLE_ANOMALY_STATE_send_ack_to_tpmicro				(3)

// ----------

typedef struct
{
	// 3/21/2013 rmd : An indication when building a message to the TP Micro to include the wind
	// settings structure. Should be set (true) when there is change to the settings, when the
	// cs3000 (this code) boots, and when the tp mciro tells us it has booted.
	BOOL_32					send_wind_settings_structure_to_the_tpmicro;

	// ----------

	// 4/24/2014 rmd : Request the whats installed settings from the tp micro. Always done when
	// the main code boots. Also set (true) by a slave being asked to prepare itself to join a
	// chain.
	BOOL_32					request_whats_installed_from_tp_micro;

	// ----------

	// 8/7/2012 rmd : The errors as rcvd from the TP Micro card itself. A record of the tp
	// micros own internal errors since the last main board code start. As the errors appear
	// within the tpmicro msg to us we set the bits within this. This field is cleared when we
	// receive the 'just booted' indication from the tp micro.
	ERROR_LOG_s				rcvd_errors;
	
	// ----------

	// 4/1/2014 rmd : So that we know what we are sending the master is valid. There seems to be
	// somewhat of a race condition between getting this from the tpmicro and the arrival of
	// tokens from the master. Maybe even within the master itself. So I've decided to IGNORE
	// scan messages until this is valid. That way the scan will fail. And eventually retry. We
	// will alert to see if this is taking place.
	BOOL_32					whats_installed_has_arrived_from_the_tpmicro;

	// ----------

	// 9/18/2012 rmd : Each time the tp micro reports an et gage pulse this counts up by one.
	// When a token response is made this is transferred to the master controller and reset back
	// to 0.
	UNS_32					et_gage_pulse_count_to_send_to_the_master;
		
	// 2/7/2013 ajv : If a runaway gage is in effect, the TP Micro will report
	// it to pass up to the master to take the appropriate action (set the flag
	// in weather_preserves, display a message on the status screen, and so on).
	BOOL_32					et_gage_runaway_gage_in_effect_to_send_to_master;

	BOOL_32					et_gage_clear_runaway_gage;

	BOOL_32					et_gage_clear_runaway_gage_being_processed;

	// 9/18/2012 rmd : Each time the tp micro reports a rain bucket pulse this counts up by one.
	// When a token response is made this is transferred to the master controller and reset back
	// to 0.
	UNS_32					rain_bucket_pulse_count_to_send_to_the_master;
	
	// ----------

	// 10/15/2014 rmd : A group of weather variables which have been moved to tpmicro_comm. A
	// more logical location. Available for use now. You cannot remove these without losing the
	// decoder_info array. Even if you keep the size of this structure the same.
	UNS_32					nlu_wind_mph;
	BOOL_32					nlu_rain_switch_active;
	BOOL_32					nlu_freeze_switch_active;
	BOOL_32					nlu_fuse_blown;
	
	// ----------

	// 8/15/2012 rmd : The measured current for this BOX as rcvd from the tpmicro. Used as a
	// holding cell for the value on its way up to the master. Is only written to and read from
	// by the COMM_MNGR task and therefore is not MUTEX protected. Is an IRRI side variable.
	MEAS_MA_FOR_DISTRIBUTION	as_rcvd_from_tp_micro;
	
	// 8/15/2012 rmd : The measured current by BOX as rcvd from each slave. Marked for
	// distribution if the incoming value is different. Is only written to and read from by the
	// COMM_MNGR task and therefore is not MUTEX protected. Is a FOAL side variable.
	MEAS_MA_FOR_DISTRIBUTION	as_rcvd_from_slaves[ MAX_CHAIN_LENGTH ];
	
	// 8/15/2012 rmd : This is the final destination for the box level current numbers. This is
	// the value to display. Is written to by the COMM_MNGR task and read from by the DISPLAY
	// task. Is an IRRI side variable. I suppose to be safe should use the tpmicro data MUTEX
	// but I suspect truly not required.
	UNS_32						measured_ma_current_as_rcvd_from_master[ MAX_CHAIN_LENGTH ];
	
	// ----------

	// 9/27/2013 rmd : This state tracks the progress of a short or no current RECORD from the
	// moment it arrives from the TPMICRO, on its journey to the MASTER, and then back to all
	// the IRRI machines in the chain. Most importantly back to ourselves BECAUSE in the case of
	// a SHORT the TPMICRO is expecting an ACK. Until that ACK is received all further turn ON's
	// activity is inhibited. NOTE: upon initialization the state is set such that an ACK is
	// sent to the TP MICRO as a precautionary step in case the TPMICRO is waiting for the ACK.
	UNS_32									terminal_short_or_no_current_state;
	
	// 9/24/2013 rmd : This structure is what is sent from the TP MICRO to the main board. And
	// then again from the IRRI side to the FOAL side.
	TERMINAL_SHORT_OR_NO_CURRENT_STRUCT		terminal_short_or_no_current_report;
	
	// ----------

	CS3000_DECODER_INFO_STRUCT		decoder_info[ MAX_DECODERS_IN_A_BOX ];
	
	TWO_WIRE_CABLE_POWER_OPERATION_STRUCT	two_wire_cable_power_operation;
	
	// ----------

	// 1/29/2013 rmd : All set true via the ui. Evaluated and cleared as a message is made for
	// the tpmicro. Also cleared on start up.
	BOOL_32				two_wire_perform_discovery;

	BOOL_32				two_wire_clear_statistics_at_all_decoders;

	BOOL_32				two_wire_request_statistics_from_all_decoders;

	BOOL_32				two_wire_start_all_decoder_loopback_test;

	BOOL_32				two_wire_stop_all_decoder_loopback_test;

	// ----------

	// 10/16/2013 rmd : Kept live during the discovery process via messages from the tp micro.
	UNS_32				decoders_discovered_so_far;
	
	// ----------

	// 10/31/2013 rmd : Arrives once per second from the TP Micro. For display. Only valid at
	// this box. TODO - likely need to distribute to all boxes.
	TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT		twccm;
	
	// 10/31/2013 rmd : These 3 are needed to manage the cable anomaly state
	// xmission as it arrives from the tp micro, makes its way to the master,
	// and then back again when broadcast out in the next token.
	UNS_32				two_wire_cable_excessive_current_xmission_state;
	
	UNS_32				two_wire_cable_over_heated_xmission_state;

	UNS_32				two_wire_cable_cooled_off_xmission_state;

	// ----------	

	// 6/17/2013 rmd : Initialized to (false) on startup.
	BOOL_32				two_wire_set_decoder_sn;
	
	UNS_32				sn_to_set;
	
	// ----------

	// 1/29/2013 rmd : Only when true is the contents sent to the tpmicro as a command. Keep OUT
	// OF THE structure that is actually sent (below). Because this structure is queued over in
	// the TP Micro on the two-wire queue. And that means this 4 bytes times the 20 queue
	// elements space is consumed. Keeping this BOOL_32 outside the structure saves 80 bytes of
	// precious TPMicro SRAM.
	BOOL_32				send_2w_solenoid_location_on_off_command;

	TWO_WIRE_DECODER_SOLENOID_OPERATION_STRUCT		two_wire_solenoid_location_on_off_command;
	
	// ----------

	// 10/24/2014 rmd : This is an array that can hold up to 8 faulted decoder reasons. Needs to
	// be buffered because of the asynchronous and not one-to-one relationship between the
	// messages from the tpmicro and the token from the master.
	DECODER_FAULTS_ARRAY_TYPE		decoder_faults;

	//----------

	// 10/28/2014 rmd : This data is initialized to 0 on each. The idea being a new varialbe
	// could take some of this space and it would be known to be zero (IF THE TPMICRO FILE IS
	// SAVED BETWEEN THE TIME OF filler (today) CREATION TILL THE NEW VARIABELS ARE CREATED).
	// Newly started boards with this code indeed will have filler set to ZERO cause when the
	// file is made for the first time all of the tpmicro_data structure is set to zero. Note -
	// this is soooo big because this really did used to be variable space that was recovered by
	// code re-design.
	UNS_32							filler[ 66 ];

	// ----------

	// 10/24/2014 rmd : WARNING! Do not change the size of this structure unless you are
	// prepared to perform an update procedure which involves reading out the exsisting file
	// into a existing format structure and then copying the like content into the new
	// structure. Perhaps item by item.
	
} TPMICRO_DATA_STRUCT;



// 9/27/2013 rmd : Held in SDRAM and saved to a file in the GENERAL STORAGE flash device.
extern TPMICRO_DATA_STRUCT	tpmicro_data;



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_tpmicro_data_on_boot( void );

extern void init_file_tpmicro_data( void );

extern void save_file_tpmicro_data( void );


extern UNS_32 TPMICRO_DATA_extract_reported_errors_out_of_msg_from_tpmicro( UNS_8 **pdata_ptr );

extern CS3000_DECODER_INFO_STRUCT* find_two_wire_decoder( UNS_32 pserial_num );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#endif


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

