/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_FTIMES_TASK_H
#define _INC_FTIMES_TASK_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"gpio_setup.h"

#include	"lpc_types.h"

#include	"cal_td_utils.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#define		FTIMES_CALCULATION_DURATION_DAYS	(14)

// 6/25/2018 rmd : For the display, an indication of percent complete. How often should we
// recalculate the value? Well probably at ONE percent intervals. So do the math for the
// number of calculation seconds, that interval, to trigger a display variable recalculation
// and possibly trigger a display update too. Note - 86400 seconds in a day.
//#define		FTIMES_CALCULATION_PERCENT_COMPLETE_RECALCULATION_INTERVAL_SECONDS		((FTIMES_CALCULATION_DURATION_DAYS * 86400)/100)
// 6/27/2018 rmd : Doing the math ...
#define		FTIMES_CALCULATION_PERCENT_COMPLETE_RECALCULATION_INTERVAL_SECONDS		(FTIMES_CALCULATION_DURATION_DAYS * 864)


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	FTIMES_EVENT_restart_calculation		(0x1111)

#define	FTIMES_EVENT_iterate					(0x2211)


typedef struct
{

	UNS_32			event;

} FTIMES_TASK_QUEUE_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define FTIMES_MODE_idle				(0x1000)

#define FTIMES_MODE_calculating			(0x1001)

#define FTIMES_MODE_results_available	(0x1002)


typedef struct
{
	// 2/8/2018 rmd : Are we running the calculation, or what?
	UNS_32		mode;
	
	// ----------

	// 7/19/2018 rmd : Want to run once following reboot, after system is stable, meaning pdata
	// variables are in place and accurate. Inherently start (false) due to C startup code.
	BOOL_32		calculation_has_run_since_reboot;
	
	// ----------
	
	// 6/22/2018 rmd : The actual date and time used during the calculation.
	// 6/28/2018 dmd: Changing to the date time complete structure variable, since we need the
	// month, year and day of the week information.
	DATE_TIME_COMPLETE_STRUCT	calculation_date_and_time;

	// 06/28/2018 dmd: Adding a flag to check if we need to call the conversion function 
	// to update the calculation_date_and_time struct above.
	BOOL_32		dtcs_data_is_current;
	
	// ----------
	
	// 4/26/2018 rmd : At this time, set a fixed 2 weeks ahead of the start of the calculation.
	DATE_TIME	calculation_END_date_and_time;
	
	// ----------
	
	// 6/25/2018 rmd : To support a progress bar on the screen, we keep these variables. The
	// only one the screen uses in the percent_complete.
	UNS_32		percent_complete_total_in_calculation_seconds;

	UNS_32		percent_complete_completed_in_calculation_seconds;

	UNS_32		percent_complete_next_calculation_boundary;

	// 6/25/2018 rmd : For the screen.
	float		percent_complete;
	
	// ----------
	
	// 6/27/2018 rmd : How long it took to do the calculation.
	float		duration_calculation_float_seconds;
	
	UNS_32		duration_start_time_stamp;

	// 07/06/2018 dmd: how many seconds to advance time.
	UNS_32		seconds_to_advance;
	
} FTIMES_CONTROL_STRUCT;


extern FTIMES_CONTROL_STRUCT	ftcs;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FTIMES_TASK_task( void *pvParameters );


extern void FTIMES_TASK_restart_calculation( void );



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

