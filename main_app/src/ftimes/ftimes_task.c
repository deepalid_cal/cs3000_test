/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"ftimes_task.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"ftimes_funcs.h"

#include	"foal_irri.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

FTIMES_CONTROL_STRUCT	ftcs;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void ftimes_post_event_with_details( FTIMES_TASK_QUEUE_STRUCT *pq_item_ptr )
{
	// 6/24/2015 rmd : Can be executed within the context of a wide variety of tasks.

	//  WARNING: DO NOT CALL THIS FUNCTION FROM AN INTERRUPT because it is not using the fromISR
	//  xQueue function.

	if( xQueueSendToBack( FTIMES_task_queue, pq_item_ptr, 0 ) != pdPASS )	 // do not wait for queue space ... flag an error
	{
		Alert_Message( "FTimes queue overflow!" );
	}
}

/* ---------------------------------------------------------- */
static void ftimes_post_event( UNS_32 pevent )
{
	// 6/24/2015 rmd : Can be executed within the context of a wide variety of tasks.

	//  WARNING: DO NOT CALL THIS FUNCTION FROM AN INTERRUPT because it is not using the fromISR
	//  xQueue function.

	FTIMES_TASK_QUEUE_STRUCT		ftqs;
	
	ftqs.event = pevent;
	
	ftimes_post_event_with_details( &ftqs );
}

/* ---------------------------------------------------------- */
static void calculate_seconds_to_advance( void )
{
	// 7/9/2018 rmd : This function is only used in the ftimes environment. And MUST GUARANTEE
	// that ftcs.seconds_to_add is set.

	FT_STATION_STRUCT 	*lstation_ptr;

	UNS_32	least_time_jmp_value;

	UNS_32	curr_time_jmp_value;

	UNS_32	time_to_10min;

	// ----------

	// 7/9/2018 rmd : On the very first iterate the irrigating list is empty, by definition, and
	// therefore we'll fall into this first if, and advance to the first 10 minute sSX boundary.
	if( ft_irrigating_stations_list_hdr.count == 0 )
	{
		// 7/5/2018 rmd : Not irrigating, then we can advance the time to the next start time.
		ftcs.seconds_to_advance = 600 - (ftcs.calculation_date_and_time.date_time.T % 600);
	}
	else
	{
		// 7/5/2018 rmd : The optimizations while irrigating fall here.

		least_time_jmp_value = UNS_32_MAX; 

		lstation_ptr = nm_ListGetFirst( &ft_irrigating_stations_list_hdr );

		while( lstation_ptr )
		{
			// 07/06/2018 dmd: A station can be either running or soaking, only one can be true at a time. 
			if( lstation_ptr->remaining_seconds_ON != 0 )
			{
				curr_time_jmp_value = lstation_ptr->remaining_seconds_ON;
			}
			else  
			if( lstation_ptr->soak_seconds_remaining_ul != 0 )
			{
				curr_time_jmp_value = lstation_ptr->soak_seconds_remaining_ul;
			}
			else // 07/06/2018 dmd: If station in not running or soaking state, it is waiting.
			{
				curr_time_jmp_value = UNS_32_MAX;
			}

			//Compare with the saved least time in the current irrigating stations list
			if( curr_time_jmp_value  < least_time_jmp_value )
			{
				least_time_jmp_value = curr_time_jmp_value;
			}

			lstation_ptr = nm_ListGetNext( &ft_irrigating_stations_list_hdr, lstation_ptr );
		} 

		if( least_time_jmp_value == UNS_32_MAX )
		{
			// 7/9/2018 rmd : This would indicate ALL stations are waiting. And thinking about that I
			// don't think that condition can exist. If they are in the list waiting, at least one
			// station ends up turning ON.
			// 7/12/2018 rmd : Ahhh ... update: this does happen - after the very last second of the
			// last station during a MANUAL PROGRAM. The station last station is OFF, it's soak seconds
			// are set to 0 (unique to manual programs), but the station is not yet removed from the
			// irrigation list. It takes one more iteration to actually remove it (just been the way it
			// was done - so accept it). So catching and handling this case is appropriate. And moving
			// by one second is correct.
			ftcs.seconds_to_advance = 1;
		}
		else 
		{
			// 7/9/2018 rmd : So we have learned the smallest permissible amount we can advance based
			// upon remaining run time and remaining soak time. Now make sure that learned amount
			// doesn't have us skip over a start time - which are on the 10 minute boundaries.
			time_to_10min = 600 - ( ftcs.calculation_date_and_time.date_time.T % 600 );

			ftcs.seconds_to_advance = (least_time_jmp_value > time_to_10min ) ? time_to_10min : least_time_jmp_value;
		}
	}
}

static void clear_done_with_cycle_for_irrigating_stations( void )
{
	FT_STATION_STRUCT 	*lstation_ptr;


	lstation_ptr = nm_ListGetFirst( &ft_irrigating_stations_list_hdr );

	while( lstation_ptr )
	{
		lstation_ptr->ftimes_support.done_with_runtime_cycle = (false);

		lstation_ptr = nm_ListGetNext( &ft_irrigating_stations_list_hdr, lstation_ptr );
	}
}

/* ---------------------------------------------------------- */
static void iterate( void )
{
	BOOL_32	keep_going;
	
	// ----------

	//set all the stations runtime cycle done flag back to false for the next iteration. 
	clear_done_with_cycle_for_irrigating_stations();

	// ----------
	
	// 7/9/2018 rmd : Guaranteed to set ftcs.seconds_to_advance.
	calculate_seconds_to_advance();
	
	TDUTILS_add_seconds_to_passed_DT_ptr( &ftcs.calculation_date_and_time.date_time, ftcs.seconds_to_advance );

	ftcs.dtcs_data_is_current = (false);

	// ----------
	
	ftcs.percent_complete_completed_in_calculation_seconds += ftcs.seconds_to_advance;

	// 6/25/2018 rmd : If we added the equivalent of 1% of the total number of seconds in the
	// calculation, or more, then recalculate the percent complete variable, and possibly signal
	// the display to update the progress bar.
	if( ftcs.percent_complete_completed_in_calculation_seconds >= ftcs.percent_complete_next_calculation_boundary )
	{
		ftcs.percent_complete = ( (ftcs.percent_complete_completed_in_calculation_seconds * 100) / ftcs.percent_complete_total_in_calculation_seconds );
		
		// 6/25/2018 rmd : And update the next calculation boundary.
		ftcs.percent_complete_next_calculation_boundary = ftcs.percent_complete_completed_in_calculation_seconds + FTIMES_CALCULATION_PERCENT_COMPLETE_RECALCULATION_INTERVAL_SECONDS;
	}
	
	// ----------
	
	keep_going = (false);

	if( ftcs.calculation_date_and_time.date_time.D < ftcs.calculation_END_date_and_time.D )
	{
		keep_going = (true);	
	}
	else
	{
		// 4/26/2018 rmd : We are going to assume the day is equal to the end day. So check the
		// time.
		if( ftcs.calculation_date_and_time.date_time.T < ftcs.calculation_END_date_and_time.T )
		{
			keep_going = (true);
		}
	}
	
	// ----------
	
	if( keep_going )
	{
		// 7/9/2018 rmd : Only check for SX's at a 10 minute boundary. That is the definition of a
		// SX.
		if( (ftcs.calculation_date_and_time.date_time.T % 600) == 0 )
		{
			FTIMES_FUNCS_check_for_a_start();
		}

		FTIMES_FUNCS_maintain_irrigation_list();

		// ----------
		
		// 4/30/2018 rmd : And keep going. You might think this is a pretty slow way to keep going.
		// But I don't. I estimate for every day processed this approach adds only one second to the
		// calculation time. The big benefit is it allows us to keep the task watchdog tight on this
		// task, and easily allows us the opportunity to restart the calculation.
		ftimes_post_event( FTIMES_EVENT_iterate );
	}
	else
	{
		// 6/27/2018 rmd : Figure out how long the calculation actually took and assign to the
		// display variable.
		ftcs.duration_calculation_float_seconds = TICKS_to_FLOAT_SECONDS( (my_tick_count - ftcs.duration_start_time_stamp) );
		
		Alert_Message_va( "Ftimes duration: %5.2f", ftcs.duration_calculation_float_seconds );
		
		// ----------
		
		// 6/27/2018 rmd : Indicate the calc is done. May want to post an event to the key process
		// task here, if we are on the screen, to trigger a screen update.
		ftcs.mode = FTIMES_MODE_results_available;
	}

}

/* ---------------------------------------------------------- */
void FTIMES_TASK_task( void *pvParameters )
{
	FTIMES_TASK_QUEUE_STRUCT	ftqs;
	
	// 10/23/2012 rmd : To support the WDT activity monitor.
	UNS_32		task_index;
	
	// ----------

	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// ----------

	// 6/7/2017 rmd : Be explicit. Always start in the idle mode.
	ftcs.mode = FTIMES_MODE_idle;
	
	// ----------

	while( (true) )
	{
		// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
		if( xQueueReceive( FTIMES_task_queue, &ftqs, MS_to_TICKS( 500 ) ) == pdTRUE )
		{
			switch( ftqs.event )
			{
				case FTIMES_EVENT_restart_calculation:

					// 7/19/2018 rmd : This IS the ONLY way to start (or restart if mid-calculation) the calc.
					
					// ----------
					
					// 7/19/2018 rmd : TEMPORARY alert line to show how frequently we post the restart event.
					//Alert_Message( "FTIMES: calc restarted" );
					
					// ----------
					
					FTIMES_VARS_clean_and_reload_all_ftimes_data_structures();
					
					// ----------
					
					// 4/26/2018 rmd : Set the mode to calculating now.
					ftcs.mode = FTIMES_MODE_calculating;
					
					ftimes_post_event( FTIMES_EVENT_iterate );
					
					break;
					
				// ----------
				
				case FTIMES_EVENT_iterate:

					iterate();
					
					break;
					
				// ----------
				
				default:
					Alert_Message( "unhandled ftimes event" );
					
			}  // of event processing switch statement

		}  // of a sucessful queue item is available

		// ----------

		// 10/23/2012 rmd : Keep the activity indication going.
		task_last_execution_stamp[ task_index ] = xTaskGetTickCount();

	}  // of forever task loop
}

/* ---------------------------------------------------------- */
void FTIMES_TASK_restart_calculation( void )
{
	// 6/22/2018 rmd : This can be called from any task, at any time. Generally thought of
	// associated with a program data change, but could be more broad, such as an ET Table
	// change.
	
	// ----------
	
	FTIMES_TASK_QUEUE_STRUCT		ftqs;

	// ----------
	
	// 6/22/2018 rmd : First empty the queue, there is no reason to keep any iterate events as
	// those are self generated as part of the restart. We only need the one restart calculation
	// event in place to accomplish the job. BUT, be careful to only wait the 0 amount of time.
	// Imagine what would happen if you waited portMAX_DELAY, would reach the end and never
	// return, app would crash!
	while( xQueueReceive( FTIMES_task_queue, &ftqs, 0 ) );
	
	// ----------
	
	ftimes_post_event( FTIMES_EVENT_restart_calculation );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

