/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"ftimes_vars.h"

#include	"cs_mem.h"

#include	"epson_rx_8025sa.h"

#include	"weather_tables.h"

#include	"station_groups.h"

#include	"irrigation_system.h"

#include	"configuration_network.h"

#include	"ftimes_task.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/1/2018 rmd : Needed an array for ftimes keep track of the number of valves ON by box,
// used for electrical limit checking. It isn't really part of anything else, kind of like
// the ft_time_and_date, so here she sits.
UNS_32	ft_stations_ON_by_controller[ MAX_CHAIN_LENGTH ];

UNS_32	ft_electrical_limits[ MAX_CHAIN_LENGTH ];

// 5/3/2018 rmd : Cleared each second. Used to detect the clash between PROGRAMMED
// IRRIGATION and a MANUAL PROGRAM.
BOOL_32	ufim_one_or_more_in_the_list_for_manual_program;



// 4/1/2018 rmd : HOW IS THIS USED IN FTIMES ??? - to massage the historical used to
// calculate run times - at least thats the plan.
float	ft_et_ratio;



// ----------

FT_STATION_STRUCT			ft_stations[ FTIMES_MAX_NUMBER_OF_STATIONS ];

// 2/12/2018 rmd : A list of the irrigating stations. They don't have to be ON, but are
// either ON, SOAKING, or waiting to irrigate.
MIST_LIST_HDR_TYPE			ft_irrigating_stations_list_hdr;

// 2/22/2018 rmd : A list of the stations actually ON. To support the maintain function.
MIST_LIST_HDR_TYPE			ft_stations_ON_list_hdr;

// ----------

MIST_LIST_HDR_TYPE			ft_station_groups_list_hdr;

MIST_LIST_HDR_TYPE			ft_system_groups_list_hdr;

// ----------

MIST_LIST_HDR_TYPE			ft_manual_programs_list_hdr;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void unwind_list( MIST_LIST_HDR_TYPE_PTR plist_hdr_ptr )
{
	void *lptr;

	lptr = nm_ListRemoveHead( plist_hdr_ptr );

	while( lptr != NULL )
	{
		mem_free( lptr );

		lptr = nm_ListRemoveHead( plist_hdr_ptr );
	}
}

/* ---------------------------------------------------------- */
static void free_memory_and_initialize_all_ftimes_lists()
{
	// 2/12/2018 rmd : No memory to free as the stations exist in a permanately allocated array.
	// But we'll clean the array.
	memset( ft_stations, 0x00, sizeof(ft_stations) );
	
	nm_ListInit( &ft_irrigating_stations_list_hdr, offsetof( FT_STATION_STRUCT, list_of_irrigating_stations ) );

	nm_ListInit( &ft_stations_ON_list_hdr, offsetof( FT_STATION_STRUCT, list_of_stations_ON ) );

	// ----------
	
	// 2/12/2018 rmd : Because their is dynamic memory involved must free it.
	unwind_list( &ft_station_groups_list_hdr );
	
	nm_ListInit( &ft_station_groups_list_hdr, offsetof( FT_STATION_GROUP_STRUCT, list_support_station_groups ) );

	// ----------
	
	// 2/12/2018 rmd : Because their is dynamic memory involved must free it.
	unwind_list( &ft_system_groups_list_hdr );
	
	nm_ListInit( &ft_system_groups_list_hdr, offsetof( FT_SYSTEM_GROUP_STRUCT, list_support_systems ) );

	// ----------
	
	// 2/12/2018 rmd : Because their is dynamic memory involved must free it.
	unwind_list( &ft_manual_programs_list_hdr );
	
	nm_ListInit( &ft_manual_programs_list_hdr, offsetof( FT_MANUAL_PROGRAM_STRUCT, list_support_manual_program ) );
}
	
/* ---------------------------------------------------------- */
FT_MANUAL_PROGRAM_STRUCT *FTIMES_VARS_return_ptr_to_manual_program_group_in_ftimes_list_with_this_GID( UNS_32 const pgid )
{
	// 2/14/2018 rmd : This function called within the context of the ftimes task. No list mutex
	// is necessary to access the ftimes lists or variables when done so within the ftimes task.
	
	// ----------

	FT_MANUAL_PROGRAM_STRUCT	*rv;
	
	FT_MANUAL_PROGRAM_STRUCT	*working_ptr;
	
	rv = NULL;
	
	working_ptr = nm_ListGetFirst( &ft_manual_programs_list_hdr );
	
	while( working_ptr != NULL )
	{
		if( working_ptr->group_identity_number == pgid )
		{
			rv = working_ptr;
			
			break;
		}

		working_ptr = nm_ListGetNext( &ft_manual_programs_list_hdr, working_ptr );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
void FTIMES_VARS_clean_and_reload_all_ftimes_data_structures( void )
{
	// 2/14/2018 rmd : Executed within the context of the finish times task. Called once at the
	// start/restart of the calculation.
	
	// ----------
	
	UNS_32	iii;
	
	// ----------
	
	// 2/8/2018 rmd : Free up any previously taken list memory, emptying all dynamic memory
	// lists, and initializing their headers. Also zero out (meaning empty) the ftimes station
	// array and its associated lists.
	free_memory_and_initialize_all_ftimes_lists();
	
	// ----------
	
	// 7/9/2018 rmd : Go ahead and get the current complete date and time structure, and set
	// that as where we start the calcualtion from. At the first iteration, we will bump up to
	// the next 10 minute boundary - when we calculate how many seconds to bump ahead.
	EPSON_obtain_latest_complete_time_and_date( &ftcs.calculation_date_and_time );
	
	ftcs.dtcs_data_is_current = (true);
	
	// ----------
	
	// 4/26/2018 rmd : And now set the end time as 14 days ahead.
	ftcs.calculation_END_date_and_time.T = ftcs.calculation_date_and_time.date_time.T;

	ftcs.calculation_END_date_and_time.D = ftcs.calculation_date_and_time.date_time.D + FTIMES_CALCULATION_DURATION_DAYS;
	
	// ----------
	
	// 6/25/2018 rmd : Establish the percent complete tracking variables.
	ftcs.percent_complete_completed_in_calculation_seconds = 0;
	
	ftcs.percent_complete_total_in_calculation_seconds = (FTIMES_CALCULATION_DURATION_DAYS * 86400);

	// 6/25/2018 rmd : We calculate the percent complete display indicator on or when we cross
	// 10 minute boundaries.
	ftcs.percent_complete_next_calculation_boundary = FTIMES_CALCULATION_PERCENT_COMPLETE_RECALCULATION_INTERVAL_SECONDS;

	ftcs.percent_complete = 0.0;
	
	ftcs.duration_calculation_float_seconds = 0.0;

	ftcs.duration_start_time_stamp = my_tick_count;
	
	// ----------
	
	// 4/5/2018 rmd : ELECTRICAL LIMIT SUPPORT
	
	// 4/1/2018 rmd : Indicate NO stations are on at any box.
	memset( &ft_stations_ON_by_controller, 0, sizeof(ft_stations_ON_by_controller) );
	
	// 4/5/2018 rmd : And load up the electrical limits array.
	for( iii=0; iii<MAX_CHAIN_LENGTH; iii++ )
	{
		ft_electrical_limits[ iii ] = NETWORK_CONFIG_get_electrical_limit( iii );
	}

	// ----------
	
	// 2/14/2018 rmd : This may change. The et ratio should be calculated at the et roll time,
	// and available to the application at any time.
	WEATHER_TABLES_set_et_ratio( ET_DAYS_IN_RATIO );
	
	ft_et_ratio = WEATHER_TABLES_get_et_ratio();
	
	// ----------
	
	// 2/12/2018 rmd : Load the station group list first, so that while we load the STATIONS
	// array with all the stations we can also find and set a pointer to the stations station
	// group.
	//
	// 7/2/2018 rmd : ftcs.calculation_date_and_time MUST be initialized to the start of
	// calculation prior to loading the ftimes STATION GROUPS.
	STATION_GROUPS_load_ftimes_list( ftcs.calculation_date_and_time.date_time.D );
	
	// 3/31/2018 rmd : Now create the systems (mainlines) list.
	IRRIGATION_SYSTEM_load_ftimes_list();
	
	// 3/29/2018 rmd : This must be loaded before the stations are. The MP list is used during
	// the station loading process.
	MANUAL_PROGRAMS_load_ftimes_list();

	// 2/12/2018 rmd : This process refers to the FTIMES STATION GROUPS list as we try to find a
	// pointer for each station to its STATION GROUP, to use throughout ftimes calculations, SO
	// MUST LOAD STATIONS AFTER WE LOAD THE FTIMES STATION GROUP LIST.
	//
	// 3/29/2018 rmd : UPDATE - and it refers to the FTIMES MANUAL PROGRAMS list, so must come
	// after that is in place.
	STATIONS_load_all_the_stations_for_ftimes();
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

