/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"ftimes_funcs.h"

#include	"irri_time.h"

#include	"foal_irri.h"

#include	"alerts.h"

#include	"ftimes_task.h"

#include	"alert_parsing.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static FT_STATION_STRUCT* add_duplicate_ft_station( UNS_32 const pindex, UNS_32 preason_in_list )
{
	// 3/15/2018 rmd : pindex is pointing to the station we want to make a duplicate of. So look
	// AHEAD through the array for an unused slot. When found occupy it. If none avalable return
	// (false).
	
	// ----------
	
	UNS_32		iii;
	
	FT_STATION_STRUCT	*rv;
	
	// ----------
	
	rv = NULL;
	
	iii = pindex;
	
	// ----------
	
	// 3/14/2018 rmd : If not at the end of the array, can look ahead.
	while( iii < (FTIMES_MAX_NUMBER_OF_STATIONS-1) )
	{
		iii += 1;
		
		// 3/15/2018 rmd : See if the slot is occupied.
		if( !ft_stations[ iii ].ftimes_support.slot_loaded )
		{
			// 3/15/2018 rmd : Substantiate the duplicate.
			memcpy( &ft_stations[ iii ], &ft_stations[ pindex ], sizeof(FT_STATION_STRUCT) );
			
			// 3/15/2018 rmd : Set the duplicate flag.
			ft_stations[ iii ].ftimes_support.is_a_duplicate_in_the_array = (true);
			
			// 3/15/2018 rmd : REMEMBER, the station we are copying from is in the irrigation list, so
			// make sure this new one is not.
			memset( &ft_stations[ iii ].list_of_irrigating_stations, 0x00, sizeof(MIST_DLINK_TYPE) );

			// 3/15/2018 rmd : And also is not to be on the list of stations ON.
			memset( &ft_stations[ iii ].list_of_stations_ON, 0x00, sizeof(MIST_DLINK_TYPE) );
			
			// 3/15/2018 rmd : And clean up some things that might affect behavior during irrigation.
			ft_stations[ iii ].ftimes_support.did_not_irrigate_last_time_holding_copy = (false);
			ft_stations[ iii ].water_sense_deferred_seconds = 0;
			ft_stations[ iii ].soak_seconds_remaining_ul = 0;
			ft_stations[ iii ].bbf.station_is_ON = (false);
			ft_stations[ iii ].bbf.w_reason_in_list = preason_in_list;

			rv = &ft_stations[ iii ];

			break;
		}
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
static FT_STATION_STRUCT* find_a_duplicate_ahead_of_this_station( UNS_32 const pindex )
{
	// 3/14/2018 rmd : pindex is pointing to a station in the array, look AHEAD of this station
	// for a duplicate, staying within the bounds of the array of course.
	
	// ----------
	
	UNS_32				iii;
	
	FT_STATION_STRUCT	*rv;
	
	// ----------
	
	rv = NULL;
	
	iii = pindex;
	
	// ----------
	
	// 3/14/2018 rmd : If not at the end of the array, can look ahead.
	while( iii < (FTIMES_MAX_NUMBER_OF_STATIONS-1) )
	{
		iii += 1;
		
		// 3/14/2018 rmd : Checking only for the is_a_duplicate_in_the_array flag is sufficient,
		// because when we remove the array slot from the list we ZERO the whole entry.
		if( ft_stations[ iii ].ftimes_support.is_a_duplicate_in_the_array )
		{
			if( (ft_stations[ iii ].box_index_0 == ft_stations[ pindex ].box_index_0) && (ft_stations[ iii ].station_number_0 == ft_stations[ pindex ].station_number_0) )
			{
				// 3/14/2018 rmd : This then qualifies as a duplicate!
				rv = &ft_stations[ iii ];
				
				break;  // all done
			}
		}
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
BOOL_32 FTIMES_FUNCS_load_run_time_calculation_support( FT_STATION_GROUP_STRUCT *pft_station_group_ptr, RUN_TIME_CALC_SUPPORT_STRUCT *psupport_struct_ptr )
{
	BOOL_32	rv;
	
	rv = (false);
	
	if( pft_station_group_ptr != NULL )
	{
		rv = (true);
			
		psupport_struct_ptr->schedule_enabled = pft_station_group_ptr->schedule_enabled_bool;
	
		psupport_struct_ptr->schedule_type = pft_station_group_ptr->schedule_type;
		
		memcpy( psupport_struct_ptr->wday, pft_station_group_ptr->water_days_bool, sizeof(psupport_struct_ptr->wday) );
	
		psupport_struct_ptr->irrigates_after_29th_or_31st = pft_station_group_ptr->irrigate_on_29th_or_31st_bool;
		
		psupport_struct_ptr->uses_et_averaging = pft_station_group_ptr->use_et_averaging_bool;
		
		// 4/5/2016 rmd : And get the start time.
		psupport_struct_ptr->sx = pft_station_group_ptr->start_time;
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
FT_STATION_GROUP_STRUCT *FTIMES_FUNCS_find_station_group_in_ftimes_list_with_this_GID( UNS_32 const psg_gid )
{
	// 2/14/2018 rmd : This function called within the context of the ftimes task. No list mutex
	// is necessary to access the ftimes lists or variables when done so within the ftimes task.
	
	// 6/25/2018 rmd : This function can return a NULL if the station doesn't belong to a
	// station group.
	
	// ----------

	FT_STATION_GROUP_STRUCT	*rv;
	
	FT_STATION_GROUP_STRUCT	*working_ptr;
	
	rv = NULL;
	
	working_ptr = nm_ListGetFirst( &ft_station_groups_list_hdr );
	
	while( working_ptr != NULL )
	{
		if( working_ptr->group_identity_number == psg_gid )
		{
			rv = working_ptr;
			
			break;
		}

		working_ptr = nm_ListGetNext( &ft_station_groups_list_hdr, working_ptr );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
FT_SYSTEM_GROUP_STRUCT *FTIMES_FUNCS_find_system_group_in_ftimes_list_with_this_GID( UNS_32 const psg_gid )
{
	// 2/14/2018 rmd : This function called within the context of the ftimes task. No list mutex
	// is necessary to access the ftimes lists or variables when done so within the ftimes task.
	
	// ----------

	FT_SYSTEM_GROUP_STRUCT	*rv;
	
	FT_SYSTEM_GROUP_STRUCT	*working_ptr;
	
	rv = NULL;
	
	working_ptr = nm_ListGetFirst( &ft_system_groups_list_hdr );
	
	while( working_ptr != NULL )
	{
		if( working_ptr->system_gid == psg_gid )
		{
			rv = working_ptr;
			
			break;
		}

		working_ptr = nm_ListGetNext( &ft_system_groups_list_hdr, working_ptr );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
FT_STATION_STRUCT* ftimes_if_station_is_ON_turn_it_OFF( MIST_LIST_HDR_TYPE_PTR const plist_hdr, FT_STATION_STRUCT *const pftstation_ptr )
{
	FT_STATION_STRUCT	*rv;
	
	BOOL_32		inhibit_next_turn_on;

	// ----------
	
	rv = NULL;
	
	// ----------
	
	// 5/10/2012 rmd : Must be on the irrigation list.
	if( !nm_OnList( &ft_irrigating_stations_list_hdr, pftstation_ptr ) )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "FTIMES: pftstation not on irrigation list!" );
	}
	else
	{
		// 5/10/2012 rmd : To support those callers who are actually traversing the main ilc list.
		// The re-ordering dictates we take this action. That is get the next and return it to the
		// caller. The result of the re-ordering may be the caller actually bumps into this list
		// item AGAIN further down the list. But the station will already be OFF and this is okay.
		// The caller will get through the whole list without missing any stations. Even in the
		// presences of the re-ordering! We pass the list argument because sometimes the caller is
		// in the process of traversing the ON list as opposed to the main irri list.
		//
		// 4/4/2018 rmd : We call this function with a NULL pointer from where we check for start
		// times. In that case we are not trying to find the next list item.
		if( plist_hdr )
		{
			rv = nm_ListGetNext( plist_hdr, pftstation_ptr );
		}
		
		// ----------
		
		// 5/10/2012 rmd : In some cases we do not actually know if the station is ON. And this
		// function is called as a cleanup measure before the list item is to be abruptly deleted
		// from the list. In other cases the station is expected to be ON such as due to a flow
		// check violation or even simply just a normal turn OFF cause out of time.
		if( pftstation_ptr->bbf.station_is_ON == (true) )
		{
			if( ft_stations_ON_by_controller[ pftstation_ptr->box_index_0 ] == 0 )
			{
				ALERT_MESSAGE_WITH_FILE_NAME( "FTIMES: unexp ON count" );
			}
			else
			{
				ft_stations_ON_by_controller[ pftstation_ptr->box_index_0 ] -= 1;
			}
	
			// ----------
			
			// List remove will remove the item if it is on the list. If not on the list will generate
			// an error message. So test first. AND we can remove from this list even if the caller is
			// traversing this ON list cause we already have obtained the next in the list!
			if( nm_OnList( &ft_stations_ON_list_hdr, (void*)pftstation_ptr ) )
			{
				nm_ListRemove( &ft_stations_ON_list_hdr, (void*)pftstation_ptr );
			}
			else
			{
				ALERT_MESSAGE_WITH_FILE_NAME( "FTIMES: should been on the ON list" );
			}
	
			// ----------
			
			// 9/9/2014 rmd : Do NOT move this relative to the SLOW-CLOSING valve timer logic below. We
			// count on this value being updated first before that logic is executed.
			if( pftstation_ptr->system_ptr->system_master_number_of_valves_ON > 0 )
			{
				pftstation_ptr->system_ptr->system_master_number_of_valves_ON -= 1;
			}
			else
			{
				// 5/30/2012 rmd : Apparently the number ON is 0.
				Alert_Message( "FTIMES: system number ON 0" );
			}

			// ------------------------------			

			if( pftstation_ptr->system_ptr->ufim_expected_flow_rate_for_those_ON >= pftstation_ptr->expected_flow_rate_gpm_u16 )
			{
				pftstation_ptr->system_ptr->ufim_expected_flow_rate_for_those_ON -= pftstation_ptr->expected_flow_rate_gpm_u16;
			}
			else
			{
				Alert_Message( "FTIMES: unexp system expected flow rate" );
				
				// 5/30/2012 rmd : Zero it. Couldn't think of anything else to do.
				pftstation_ptr->system_ptr->ufim_expected_flow_rate_for_those_ON = 0;
			}
	
			// ------------------------------			

			if( pftstation_ptr->system_ptr->ufim_flow_check_group_count_of_ON[ pftstation_ptr->bbf.flow_check_group ] > 0 )
			{
				pftstation_ptr->system_ptr->ufim_flow_check_group_count_of_ON[ pftstation_ptr->bbf.flow_check_group ] -= 1;
			}
			else
			{
				// 5/30/2012 rmd : Well apparently the flow group count is already 0.
				Alert_Message( "FTIMES: unexp flow group count" );
			}

			// ----------

			// 9/10/2015 rmd : START of slow closing valve section.
			
			// 2/20/2013 rmd : Set the SLOW CLOSING VALVE INHIBIT timer. Test the time to set will take
			// us out past the time already ticking down. If the slow_closing_valve is non-zero that
			// means the user wants to delay between valves.
			inhibit_next_turn_on = (true);
			
			// ----------
			
			// 9/9/2014 rmd : The logic we want to implement is that if we were running the pump
			// (irrigating pump valves) and this was the only valve ON then skip the slow closing valve
			// mechanism. This was decided by David Byma, Richard Wilkinson, and myself. This is being
			// done to avoid the resultant condition where we would be left with the pump running and no
			// valves ON.
			if( pftstation_ptr->bbf.w_uses_the_pump )
			{
				// 9/9/2014 rmd : Check the number_of_valves_ON in this system. It has already been
				// decremented reflecting how many would be ON after this valve we are shutting OFF is OFF.
				if( pftstation_ptr->system_ptr->system_master_number_of_valves_ON == 0 )
				{
					inhibit_next_turn_on = (false);
				}
			}
			
			// ----------
			
			if( inhibit_next_turn_on )
			{
				if( pftstation_ptr->system_ptr->inhibit_next_turn_ON_remaining_seconds < pftstation_ptr->station_group_ptr->delay_between_valve_time_sec )
				{
					pftstation_ptr->system_ptr->inhibit_next_turn_ON_remaining_seconds = pftstation_ptr->station_group_ptr->delay_between_valve_time_sec;
				}
			}
			else
			{
				// 9/9/2014 rmd : A couple of scenarios here to cover. Let's take the case during regular
				// irrigation where 2 valves expire at the same time. And they were the only 2 ON at the
				// time. And they used the pump. The first one OFF would pass through here and set the
				// inhibit time because as far as we could see there was still another valve ON. The second
				// one and last one ON would then pass through here and not set the time as he was the only
				// one ON. HOWEVER the inhibit time is still set and ticking down. So the issue we are
				// trying to prevent was created. That is the pump ON and no valves ON. The solution is to 0
				// the time if we are not setting the time. This works for this scenario and I think is safe
				// for all other scenarios.
				//
				// 9/9/2014 rmd : The other case this covers is if a TEST interrupts a scheduled irrigation
				// when the pump was running and multiple valves were ON. If we didn't 0 the time here the
				// TEST valve would be delayed.
				pftstation_ptr->system_ptr->inhibit_next_turn_ON_remaining_seconds = 0;
			}
				
			// 9/10/2015 rmd : END of slow closing valve section.

			// ----------
			
			pftstation_ptr->bbf.station_is_ON = (false);
		
			// ----------
			
			// 5/10/2012 rmd : Cleanup remaining seconds to zero. Don't have to but might look funny
			// during the soak to see a negative (if one developed). Or during a flow check hunting
			// process to see time remaining in the cycle but not ON.
			pftstation_ptr->remaining_seconds_ON = 0;
	
			// ----------
			
			// 6/26/2012 rmd : START the soak cycle for a NORMAL cycle end (for problem turn offs we set
			// soak to 0! UPDATE: AJ convinced me that on the other side of a power fail the stations
			// that were ON should now be soaking. Technically I think he is right. As we could start
			// them right back up with another full cycle. And likely cause run-off. My idea was to have
			// the ones ON come back ON. To avoid confusion. But hey how often is somebody watching.
			//
			// 4/4/2018 rmd : For FTIMES we always set the soak time. Make SURE we set the soak time to
			// the correct "used during irrigation" value!
			if( pftstation_ptr->requested_irrigation_seconds_balance_ul > 0  )
			{
				pftstation_ptr->soak_seconds_remaining_ul = pftstation_ptr->soak_seconds_used_during_irrigation;
			} else
			{
				pftstation_ptr->soak_seconds_remaining_ul = 0;
			}

			// ----------
			
			// 2012.02.17 rmd : Typically we want to re-order the list to juggle the effective
			// priorities of which station will come ON next. Except for the case of WALK_THRU. Which
			// should retain it's list order once they are added to the list. Even if say during
			// WALK-THRU a radio remote station comes on for a bit. Then off. We want the WALK-THRU to
			// pick up where it left off. They should go in order.
			//
			// 4/4/2018 rmd : For FTIMES we always remove and reinsert.
			FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list( FOR_FTIMES, pftstation_ptr );

		}  // of if the station is ON
		else
		{
			// 5/3/2012 rmd : SANITY CHECK. This ilc should NOT be on the ON list.
			if( nm_OnList( &ft_stations_ON_list_hdr, (void*)pftstation_ptr ) )
			{
				ALERT_MESSAGE_WITH_FILE_NAME( "FTIMES: on ON list!" );
			
				nm_ListRemove( &ft_stations_ON_list_hdr, (void*)pftstation_ptr );
			}
		}

	}  // of if the pilc is not on the irrigation list
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
FT_STATION_STRUCT* ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists( MIST_LIST_HDR_TYPE_PTR const plist_hdr, FT_STATION_STRUCT *const pftstation_ptr )
{
	// 4/4/2018 rmd : This FTIMES function is called to make sure a station is OFF, removed from
	// the ON list, and removed from the irrigation list. Furthermore if we discover this
	// station is a DUPLICATE we must remove it from the array of stations by zeroing out the
	// complete station. This is because duplicates exist ONLY during the time they are on the
	// irrigation list, that is the list of actively irrigating stations. Duplicates are created
	// ONLY when a station must be on the activiely irrigating list for TWO reasons. And in the
	// FTIMES world those two reasons can ONLY be for PROGRAMMED IRRIGATION and MANUAL PROGRAMS.
	// Afetr removing the duplicate we assume the station still exists in the stations array, by
	// definition it should.

	// ----------
	
	FT_STATION_STRUCT	*rv;

	// ----------
			
	// 5/9/2012 rmd : Turn OFF the station if it is ON.
	rv = ftimes_if_station_is_ON_turn_it_OFF( plist_hdr, pftstation_ptr );
	
	// ----------
	
	// 12/11/2015 rmd : NOW - Remove from the irrigation list. That is what this function is all
	// about after all. This does not affect the fact that the ilc is on the ACTION NEEDED list.
	nm_ListRemove( &ft_irrigating_stations_list_hdr, pftstation_ptr );
	
	// ----------
	
	if( pftstation_ptr->ftimes_support.is_a_duplicate_in_the_array )
	{
		// 3/19/2018 rmd : MUST removefrom array of stations. Step on the item.
		memset( pftstation_ptr, 0x00, sizeof(FT_STATION_STRUCT) );
	}

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
void ftimes_complete_the_turn_ON( FT_STATION_STRUCT *pftstation_ptr )
{
	// 4/26/2012 rmd : We are sending the action_needed record to turn ON the station. So do it
	// now.
	pftstation_ptr->bbf.station_is_ON = (true);
	
	// ----------
	
	// 5/9/2012 rmd : Update the count ON by controller.
	ft_stations_ON_by_controller[ pftstation_ptr->box_index_0 ] += 1;
	
	// --------------------------------
	
	// Add to the ON list here - so far this is the only place. Always add the last one ON to
	// the end of the list.
	nm_ListInsertTail( &ft_stations_ON_list_hdr, pftstation_ptr );
	
	// ----------
	
	// 6/5/2012 rmd : SYSTEM level flow checking type flags that are cleared.
	
	pftstation_ptr->system_ptr->sbf.checked_or_updated_and_made_flow_recording_lines = (false);
	
	pftstation_ptr->system_ptr->sbf.system_level_no_valves_ON_therefore_no_flow_checking = (false);
	pftstation_ptr->system_ptr->sbf.system_level_valves_are_ON_but_will_not_be_checking_flow = (false);
	pftstation_ptr->system_ptr->sbf.system_level_valves_are_ON_and_waiting_to_check_flow = (false);
	pftstation_ptr->system_ptr->sbf.system_level_valves_are_ON_and_actively_checking = (false);
	pftstation_ptr->system_ptr->sbf.system_level_valves_are_ON_and_waiting_to_update_derate_table = (false);
	pftstation_ptr->system_ptr->sbf.system_level_valves_are_ON_and_has_updated_the_derate_table = (false);
	pftstation_ptr->system_ptr->sbf.system_level_valves_are_ON_and_waiting_to_acquire_expected = (false);
	
	// --------------------------------

	if( pftstation_ptr->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
	{
		pftstation_ptr->ftimes_support.did_not_irrigate_last_time_holding_copy = (false);
	}
}

/* ---------------------------------------------------------- */
UNS_32 ftimes_get_percent_adjust_100u( FT_STATION_GROUP_STRUCT *pft_group_ptr, UNS_32 const pdate )
{
	UNS_32	rv;

	// ----------
	
	// 6/25/2015 ajv : Initialize to the default in case any of the conditions fail.
	rv = PERCENT_ADJUST_PERCENT_DEFAULT_100u;

	// ----------

	if( pft_group_ptr != NULL )
	{
		if( (pdate >= pft_group_ptr->percent_adjust_start_date) && (pdate <= pft_group_ptr->percent_adjust_end_date) && (pft_group_ptr->percent_adjust_start_date != pft_group_ptr->percent_adjust_end_date) )
		{
			rv = pft_group_ptr->percent_adjust_100u;
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
UNS_32 ftimes_number_of_starts_for_this_manual_program( FT_MANUAL_PROGRAM_STRUCT const *const pmanual_program_ptr )
{
	UNS_32	rv;
	
	UNS_32	i;
				
	// ----------
	
	rv = 0;
	
	// ----------
	
	if( pmanual_program_ptr != NULL )
	{
		// 10/4/2012 rmd : Are we within the date range allowed to run?
		if( (ftcs.calculation_date_and_time.date_time.D >= pmanual_program_ptr->start_date) && (ftcs.calculation_date_and_time.date_time.D <= pmanual_program_ptr->stop_date) )
		{
			// 10/4/2012 rmd : Is today a water day?
			for( i=0; i<MANUAL_PROGRAMS_NUMBER_OF_START_TIMES; i++ )
			{
				if( pmanual_program_ptr->start_times[ i ] == ftcs.calculation_date_and_time.date_time.T )
				{
					if( !ftcs.dtcs_data_is_current )
					{
						// 4/26/2018 rmd : Because this conversion is calculation intensive, avoid conversion until
						// absolutely needed. We want this ftimes calculation to be as efficient as possible.
						DateAndTimeToDTCS( ftcs.calculation_date_and_time.date_time.D, ftcs.calculation_date_and_time.date_time.T, &ftcs.calculation_date_and_time );

						ftcs.dtcs_data_is_current = (true);
					}

					if( pmanual_program_ptr->days[ ftcs.calculation_date_and_time.__dayofweek ] )
					{
						// 10/4/2012 rmd : Indicate one more cycle requested.
						rv += 1;
					}
				}
			}
		}  // of we're within the date range
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
void ftimes_add_to_the_irrigation_list( FT_STATION_STRUCT *pft_station_ptr )
{ 
	// 3/15/2018 rmd : Used queue up irrigation for both PI and MP. When the SX is for MP and
	// the station is already in the irrigation list for MP this function is bypassed because we
	// just add to the time balance in the SX detect function.
	
	// ----------
	
	// 3/15/2018 rmd : Ensure this flag isn't set.
	pft_station_ptr->bbf.station_is_ON = (false);
		
	// -------------------------
	
	// 10/5/2012 rmd : Having a soak time obviously affects the delay between cycles. And for
	// the cases of PROGRAMMED IRRIGATION and MANUAL we desire that behaviour. Not for MANUAL
	// PROGRAMS. The soak time also affects the behaviour on the other side of a power fail if
	// the station was irrigating. For some reaons in the list we would like to see the valve
	// come right back ON. If there is a soak time that won't happen. Setting to 0 here creates
	// that behaviour.
	if( pft_station_ptr->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
	{
		pft_station_ptr->soak_seconds_used_during_irrigation = pft_station_ptr->soak_seconds_original_from_station_info;
	}
	else
	{
		pft_station_ptr->soak_seconds_used_during_irrigation = 0;
	}
	
	pft_station_ptr->soak_seconds_remaining_ul = 0;

	pft_station_ptr->remaining_seconds_ON = 0;

	// ----------

	// 4/4/2018 rmd : If BUDGETS were to be included in the FTIMES calculation this is where to
	// adjust the requested time. But AJ and I said no budgets - thank God.
	
	// ----------
	
	// 4/4/2018 rmd : The hi and low flow action, and flow check group are set when the station
	// is added to the array.
	
	// ----------
	
	// 4/4/2018 rmd : In the FTIMES environment I have elected to keep some of the flow checking
	// tracking for error checking purposes. This gets back the flow group and how we should
	// only have station ON from a single flow group at a time.
	pft_station_ptr->bbf.flow_check_when_possible_based_on_reason_in_list = FOAL_IRRI_we_test_flow_when_ON_for_this_reason( pft_station_ptr->bbf.w_reason_in_list );
	
	// ----------
	
	// 4/4/2018 rmd : The expected flow rate is set when the station is added to the array.

	// 4/4/2018 rmd : The slow closing valve setting is pulled as needed from the
	// ftimes_station_group - remember the station carries a pointer to the station group it
	// belongs to.
	
	// ----------
	
	// Now add it to the end of the list.
	nm_ListInsertTail( &ft_irrigating_stations_list_hdr, pft_station_ptr );

	// And order it properly based on the weighting criteria we define.
	FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list( FOR_FTIMES, pft_station_ptr );
}

/* ---------------------------------------------------------- */
/** 
 * Check if the current DATE_TIME complete value is a irrigation date. We are only
 * considering the current date and the begin date from the STATION GROUP structure.
 * This routine is based off SCHEDULE_does_it_irrigate_on_this_date function from station_groups.c.
 * The only difference is that begin_date is not modified in here, since we donot need to change it
 * for ftimes.
 *
 * @mutex No mutex is held in finish times functions.
 *
 * @executed Executed within the context of the we_are_at_a_start_time_and_a_water_day and
 *  FTIMES_FUNCS_check_for_a_start functions to check if the time needs to be used to
 *  calculate the ftimes.
 *
*/ 
static BOOL_32 start_time_check_does_it_irrigate_on_this_date( FT_STATION_GROUP_STRUCT *const pft_sgs ) 
{
	BOOL_32	rv;

	// ----------

	rv = (false);

	// ----------

	UNS_32	num_of_days;

	UNS_32	schedule_type_days;

	// ----------
	
	switch( pft_sgs->schedule_type )
	{
		case SCHEDULE_TYPE_EVERY_DAY:
			rv = (true);
			break;

		case SCHEDULE_TYPE_EVEN_DAYS:
			if( (ftcs.calculation_date_and_time.__day % 2) == 0 )
			{
				rv = (true);
			}
			break;

		case SCHEDULE_TYPE_ODD_DAYS:
			if( (ftcs.calculation_date_and_time.__day % 2) == 1 )
			{
				// 7/3/2015 ajv : If it's the first of the month and the previous month ended on an odd day,
				// check to see whether the user has indicated to irrigate the 1st or not.
				if( (ftcs.calculation_date_and_time.__day == 1) &&
					((ftcs.calculation_date_and_time.__month == 1) || (ftcs.calculation_date_and_time.__month == 2) ||
					 (ftcs.calculation_date_and_time.__month == 4) || (ftcs.calculation_date_and_time.__month == 6) ||
					 (ftcs.calculation_date_and_time.__month == 8) || (ftcs.calculation_date_and_time.__month == 9) ||
					 (ftcs.calculation_date_and_time.__month == 11)) )
				{
					if( pft_sgs->irrigate_on_29th_or_31st_bool )
					{
						rv = (true);
					}
				}
				else
				if( (IsLeapYear(ftcs.calculation_date_and_time.__year) == (true)) && (ftcs.calculation_date_and_time.__day == 1) && (ftcs.calculation_date_and_time.__month == 3) )
				{
					if( pft_sgs->irrigate_on_29th_or_31st_bool )
					{
						rv = (true);
					}
				}
				else
				{
					rv = (true);
				}
			}
			break;

		case SCHEDULE_TYPE_WEEKLY_SCHEDULE:
			if( pft_sgs->water_days_bool[ ftcs.calculation_date_and_time.__dayofweek ] )
			{
				rv = (true);
			}
			break;

		case SCHEDULE_TYPE_ONCE_EVERY_THREE_WEEKS:
		case SCHEDULE_TYPE_ONCE_EVERY_FOUR_WEEKS:
		case SCHEDULE_TYPE_EVERY_OTHER_DAY:
		case SCHEDULE_TYPE_EVERY_THREE_DAYS:
		case SCHEDULE_TYPE_EVERY_FOUR_DAYS:
		case SCHEDULE_TYPE_EVERY_FIVE_DAYS:
		case SCHEDULE_TYPE_EVERY_SIXTH_DAYS:
		case SCHEDULE_TYPE_EVERY_SEVEN_DAYS:
		case SCHEDULE_TYPE_EVERY_EIGHT_DAYS:
		case SCHEDULE_TYPE_EVERY_NINE_DAYS:
		case SCHEDULE_TYPE_EVERY_TEN_DAYS:
		case SCHEDULE_TYPE_EVERY_ELEVEN_DAYS:
		case SCHEDULE_TYPE_EVERY_TWELVE_DAYS:
		case SCHEDULE_TYPE_EVERY_THIRTEEN_DAYS:
		case SCHEDULE_TYPE_EVERY_FOURTEEN_DAYS:
			if( pft_sgs->schedule_type == SCHEDULE_TYPE_ONCE_EVERY_THREE_WEEKS )
			{
				schedule_type_days = 21;
			}
			else if( pft_sgs->schedule_type == SCHEDULE_TYPE_ONCE_EVERY_FOUR_WEEKS )
			{
				schedule_type_days = 28;
			}
			else 
			{
				schedule_type_days = pft_sgs->schedule_type - SCHEDULE_TYPE_NUMBER_OF_DAYS_OFFSET;
			}

			// 06/27/2018 dmd: We need to calculate the number of days between the two dates, i.e the
			// begin_date and the current date. Then we can mod (%) with the number_of_days_in_schedule
			// value to check if this is an irrigation day.
			//
			// 7/2/2018 rmd : NOTE - we guarantee when we start the calculation that the
			// calculation_date_and_time.D is >= to the a_scheduled_irrigation_date_in_the_past. So this
			// integer math will not produce negative values (or in this case extremely large positive
			// numbers).
			num_of_days = (ftcs.calculation_date_and_time.date_time.D - pft_sgs->a_scheduled_irrigation_date_in_the_past); 

			if( ( num_of_days % schedule_type_days ) == 0 )
			{
				rv = (true);
			}
			else
			{
				rv = (false);
			}
			break;

		default:
			Alert_Message( "Invalid schedule type" );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 we_are_at_a_start_time_and_a_water_day( void )
{
	// 2/14/2018 rmd : This function called within the context of the ftimes task. No list mutex
	// is necessary to access the ftimes lists or variables when done so within the ftimes task.
	
	// 6/25/2018 rmd : Loop through the station groups seeing if we are at a start time. And if
	// so a water day.
	
	// ----------

	FT_STATION_GROUP_STRUCT	*station_group_ptr;
	
	BOOL_32		rv;
	
	// ----------
	
	rv = (false);
	
	// ----------
	
	station_group_ptr = nm_ListGetFirst( &ft_station_groups_list_hdr );
	
	while( station_group_ptr != NULL )
	{
		// 8/16/2018 rmd : The schedule must be enabled AND the calculation time match the start
		// time. This covers an obscure start up condition where the default station group start
		// time is 0, and the schedule is disabled. If you do not visit the start time screen, turn
		// off ET, and add a station to the station group, it will run. Even though the scheduled is
		// actually disabled. This did not particularly hurt anything but ... to be exact fixed.
		// After you visit the start time screen and set the start time to off, then it is set to
		// 86400 and we'll never get a match here. So to duplicate, must come out of reset, and do
		// exactly what I said above.
		if( station_group_ptr->schedule_enabled_bool && (station_group_ptr->start_time == ftcs.calculation_date_and_time.date_time.T) )
		{
			if( !ftcs.dtcs_data_is_current )
			{
				DateAndTimeToDTCS( ftcs.calculation_date_and_time.date_time.D, ftcs.calculation_date_and_time.date_time.T, &ftcs.calculation_date_and_time );

				ftcs.dtcs_data_is_current = (true);
			}

			// 6/25/2018 rmd : DEEPALI - is it safe to call this here. I don't understand the BEGIN_DATE
			// action that takes place. I would think its okay to do since it repetively call this
			// function, once for each station, below in the start time check function - but I'm not
			// sure.
			if( start_time_check_does_it_irrigate_on_this_date( station_group_ptr ) )
			{
				rv = (true);
				
				// 6/25/2018 rmd : We are done, at least one station group is going to start, so we can exit
				// the function with our answer.
				break;
			}
		}

		station_group_ptr = nm_ListGetNext( &ft_station_groups_list_hdr, station_group_ptr );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 a_manual_program_wants_to_start( void )
{
	// 2/14/2018 rmd : This function called within the context of the ftimes task. No list mutex
	// is necessary to access the ftimes lists or variables when done so within the ftimes task.
	
	// ----------

	FT_MANUAL_PROGRAM_STRUCT	*working_ptr;
	
	BOOL_32		rv;
	
	// ----------
	
	rv = NULL;
	
	working_ptr = nm_ListGetFirst( &ft_manual_programs_list_hdr );
	
	while( working_ptr != NULL )
	{
		if( ftimes_number_of_starts_for_this_manual_program( working_ptr ) )
		{
			rv = (true);
			
			// 6/25/2018 rmd : We are done, at least one manual program wants to start, so we can exit
			// the function with our answer.
			break;
		}
		
		working_ptr = nm_ListGetNext( &ft_manual_programs_list_hdr, working_ptr );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
void FTIMES_FUNCS_check_for_a_start( void )
{
	UNS_32	lstation_count;
	
	FT_STATION_STRUCT	*lftstation_ptr;
	
	FT_STATION_STRUCT	*duplicate_ahead_ft_station_ptr;

	BOOL_32		restart_calculation_due_to_error;

	BOOL_32		done_considering_this_station;
	
	UNS_32	mp;

	UNS_32	number_of_mp_starts;
	
	BOOL_32	add_the_time_to_the_remaining_balance;
	
	BOOL_32		loop_through_the_stations;
	
	// ----------
	
	const volatile float SIXTY_POINT_ZERO = 60.0;
	
	// ----------
	
	// 6/25/2018 rmd : An optimization to see if we should even bother looping through the
	// stations testing for a programmed irrigation or manual program start time.
	loop_through_the_stations = (false);
	
	if( we_are_at_a_start_time_and_a_water_day() )
	{
		loop_through_the_stations = (true);
	}
	
	if( a_manual_program_wants_to_start() )
	{
		loop_through_the_stations = (true);
	}
	
	// ----------

	if( loop_through_the_stations )
	{
		restart_calculation_due_to_error = (false);
		
		lstation_count = 0;
		
		// 3/14/2018 rmd : Because of duplicates coming and going into the array, which leaves
		// potential HOLES in slots loaded, we MUST look at every array item.
		for( lstation_count=0; lstation_count<FTIMES_MAX_NUMBER_OF_STATIONS; lstation_count++ )
		{
			lftstation_ptr = &ft_stations[ lstation_count ];
			
			// ----------
			
			// 4/4/2018 rmd : Only if the slot is occupied.
			if( lftstation_ptr->ftimes_support.slot_loaded )
			{
				// 7/30/2018 rmd : Duplicates only have a fleeting existence, and while they exist they are
				// on the irrigating list - by definition. If this station is a duplicate and on the list
				// for MANUAL_PROGRAM it is not eligible to be considered for the case of PI. If it is NOT A
				// DUPLICATE but shows a list reason of MANUAL_PROGRAM, we want to fall though into this if,
				// so that we can check if it is on the irrigation list, and if not, make a duplicate if
				// needed.
				if( !(lftstation_ptr->ftimes_support.is_a_duplicate_in_the_array && (lftstation_ptr->bbf.w_reason_in_list == IN_LIST_FOR_MANUAL_PROGRAM)) )
				{
					// 7/30/2018 rmd : Station occupy a slot if they belong to a station group OR belong to a
					// manual program. Check here if this station belongs to a group group, to avoid working
					// with potentially NULL pointer.
					if( lftstation_ptr->station_group_ptr )
					{
						// 2/26/2018 rmd : Are we at a enabled start time, and the stop time does not equal the
						// start time.
						if( lftstation_ptr->station_group_ptr->schedule_enabled_bool && (lftstation_ptr->station_group_ptr->start_time == ftcs.calculation_date_and_time.date_time.T) && (lftstation_ptr->station_group_ptr->stop_time != ftcs.calculation_date_and_time.date_time.T) )
						{
							if( !ftcs.dtcs_data_is_current )
							{
								// 4/26/2018 rmd : Take the calculation date and time and make the complete date time
								// structure, for use below. Because this conversion is calculation intensive, avoid
								// conversion until absolutely needed. We want this ftimes calculation to be as efficient as
								// possible.
								DateAndTimeToDTCS( ftcs.calculation_date_and_time.date_time.D, ftcs.calculation_date_and_time.date_time.T, &ftcs.calculation_date_and_time );

								ftcs.dtcs_data_is_current = (true);
							}
							
							// ----------
							
							// 12/03/2018 dmd : Moved checking of a mow day with the start time check. We donot want to
							// manipulate the irrigation list when it is trying to figure out if a station is in both PI
							// and MP or if it is duplicate entry with error condition.
							if( ( start_time_check_does_it_irrigate_on_this_date( lftstation_ptr->station_group_ptr ) ) && !( STATION_GROUPS_skip_irrigation_due_to_a_mow_day( FOR_FTIMES, NULL, lftstation_ptr, &ftcs.calculation_date_and_time ) ) )
							{
								done_considering_this_station = (false);
						
								// 3/14/2018 rmd : If this station is currently on the irrigation list we have a tricky
								// series of considerations to go through, UNIQUE TO THE FTIMES IMPLEMENTATION. There is no
								// equivalent of this in the regular irrigation SX check function.
								if( nm_OnList( &ft_irrigating_stations_list_hdr, lftstation_ptr ) )
								{
									// 3/14/2018 rmd : If it is the list for PI, then we need to terminate the irrigation for
									// this station and remove it from the irrigation list, but not from the array.
									if( lftstation_ptr->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
									{
										// 3/14/2018 rmd : This means the station ran START TIME to START TIME, never finishing. And
										// we probably record some ftimes results here. MAYBE we need to put some code here to test
										// if there is irrigation remaining/is it ON - and if so flag ftimes results that station
										// group never finishes irrigation.
										ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists( NULL, lftstation_ptr );
									}
									else
									if( lftstation_ptr->bbf.w_reason_in_list == IN_LIST_FOR_MANUAL_PROGRAM )
									{
										// 3/14/2018 rmd : Not going to use this array entry for a PI start, that's for sure, its
										// already in the list for MP.
										done_considering_this_station = (true);
										
										// ----------
				
										// 3/14/2018 rmd : If this list item is a duplicate?
										if( lftstation_ptr->ftimes_support.is_a_duplicate_in_the_array )
										{
											// 3/14/2018 rmd : This array entry is a duplicate, which implies there is another array
											// entry for this station, and that copy exists to handle PI. So nothing further to do, we
											// are skipping this station.
										}
										else
										{
											// 3/14/2018 rmd : So the array entry is not a duplicate, it is the primary entry and being
											// used for MP. Therefore we need to see if there is a duplicate array entry ahead to be
											// used for PI. If not we must add one.
											duplicate_ahead_ft_station_ptr = find_a_duplicate_ahead_of_this_station( lstation_count );
											
											if( duplicate_ahead_ft_station_ptr )
											{
												// 3/14/2018 rmd : If there is a duplicate it SHOULD be in the irrigation list AND in the
												// list for PI.
												if( nm_OnList( &ft_irrigating_stations_list_hdr, duplicate_ahead_ft_station_ptr ) && (duplicate_ahead_ft_station_ptr->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION) )
												{
													// 3/15/2018 rmd : Okay, so the duplicate seems to be in order, and we are already set to be
													// done with this station.
												}
												else
												{
													// 3/15/2018 rmd : There's a problem! Either the duplicate is not in the irrigation list or
													// it is in the list for the wrong reason. Declare an error, restart the calculation, and
													// skip this station.
													ALERT_MESSAGE_WITH_FILE_NAME( "FTIMES ERROR: duplicate problem" );
													
													restart_calculation_due_to_error = (true);
												}
											}
											else
											{
												duplicate_ahead_ft_station_ptr = add_duplicate_ft_station( lstation_count, IN_LIST_FOR_PROGRAMMED_IRRIGATION );
	
												if( !duplicate_ahead_ft_station_ptr )
												{
													// 3/15/2018 rmd : Apparently the array is FULL! So declare a calculation error. BUT I
													// hesitate to actually restart the calculation as this would just happen over and over
													// again, flooding the alerts.
													ALERT_MESSAGE_WITH_FILE_NAME( "FTIMES ERROR: no room for duplicate" );
												}
												
												// 3/15/2018 rmd : And we are already set to be done with this station.
											}
										}
									}
									else
									{
										// 3/14/2018 rmd : This is an error. If a station is in the irrigation list it must be there
										// for either PI or MP. Restart the calculation!
										ALERT_MESSAGE_WITH_FILE_NAME( "FTIMES ERROR: station in irri list for UNK reason" );
										
										restart_calculation_due_to_error = (true);
									}
								}
								
								// ----------
								
								if( restart_calculation_due_to_error )
								{
									FTIMES_TASK_restart_calculation();
									
									// 3/29/2018 rmd : Terminate the SX check.
									break;
								}
								
								// ----------
								
								if( !done_considering_this_station )
								{
									lftstation_ptr->requested_irrigation_seconds_balance_ul = (SIXTY_POINT_ZERO * nm_WATERSENSE_calculate_run_minutes_for_this_station(	FOR_FTIMES,
																																										NULL,
																																										NULL,
																																										lftstation_ptr,
																																										&ftcs.calculation_date_and_time,
																																										lftstation_ptr->distribution_uniformity_100u ) );
													
									// 2/22/2018 rmd : Add in any WaterSense 3 minute left-over irrigation.
									if( lftstation_ptr->station_group_ptr->et_in_use )
									{
										lftstation_ptr->requested_irrigation_seconds_balance_ul += lftstation_ptr->water_sense_deferred_seconds;
					
										lftstation_ptr->water_sense_deferred_seconds = 0;
					
										if( lftstation_ptr->requested_irrigation_seconds_balance_ul > 0 )
										{
											// 7/17/2015 ajv : WATERSENSE MINIMUM CYCLE adjustment for normal programmed time.
											// 
											// Also store whatever time was thrown away so that we can reapply it during the next
											// irrigation. This ensures our moisture balance doesn't consistently drop simply because a
											// there's a cycle of 3-minutes or less.
											lftstation_ptr->water_sense_deferred_seconds = WATERSENSE_make_min_cycle_adjustment(	FOR_FTIMES,
																																	&(lftstation_ptr->requested_irrigation_seconds_balance_ul),
																																	lftstation_ptr->cycle_seconds_original_from_station_info,
																																	NULL);
					
											// 2/22/2018 rmd : The total_run_seconds could be 0 here now!
										}
									}  // of if the station use et
									
									// ----------
									
									// 6/30/2015 ajv : Now calculate how long we are actually scheduled to run for based on the
									// station adjust and percent adjust factors.
									lftstation_ptr->requested_irrigation_seconds_balance_ul = ( SIXTY_POINT_ZERO * nm_WATERSENSE_calculate_adjusted_run_time(	FOR_FTIMES,
																																								NULL,
																																								lftstation_ptr,
																																								((float)lftstation_ptr->requested_irrigation_seconds_balance_ul / SIXTY_POINT_ZERO),
																																								lftstation_ptr->et_factor_100u,
																																								ftimes_get_percent_adjust_100u(lftstation_ptr->station_group_ptr, ftcs.calculation_date_and_time.date_time.D) ) );
					
									// ----------
					
									// 8/4/2015 ajv : Finally, reperform the minimum cycle adjustment but DO NOT deduct any
									// left-over from the WaterSense calculated number. This second adjustment is to ensure we
									// don't have any run times of 3-minutes or less after performing our station or percent
									// adjust, which don't impact WaterSense.
									if( lftstation_ptr->station_group_ptr->et_in_use )
									{
										if( lftstation_ptr->requested_irrigation_seconds_balance_ul > 0 )
										{
											// 2/26/2018 rmd : Do not track the throw away.
											WATERSENSE_make_min_cycle_adjustment(	FOR_FTIMES,
																					&(lftstation_ptr->requested_irrigation_seconds_balance_ul),
																					lftstation_ptr->cycle_seconds_original_from_station_info,
																					NULL);
					
											// 2/22/2018 rmd : The total_run_seconds could be 0 here now!
										}
									}  // of if the station use et
									
									// ----------
									
									// 2/26/2018 rmd : Adjust the stop day based upon the stop time.
									lftstation_ptr->stop_datetime_t = lftstation_ptr->station_group_ptr->stop_time;
					
									lftstation_ptr->stop_datetime_d = (UNS_16)FOAL_IRRI_return_stop_date_according_to_stop_time( &ftcs.calculation_date_and_time.date_time, lftstation_ptr->station_group_ptr->stop_time );
									
									// ----------
									
									// 7/2/2018 rmd : NOTE - for ftimes, this mow day check can be incorporated into our
									// is_this_a_water_day function - called ealier NOTE NOTE NOTE
									// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
									
									// 12/03/2018 dmd : We are removing the code to check if it is a mow day that was previously
									// here. Doing this check here is too late, we donot want to disrupt the list contents. So ,
									// it is moved to the beginning when we are deciding to add the station to the irrigation
									// list, where the start time check occurs.
									
									// ----------
									
									if( lftstation_ptr->requested_irrigation_seconds_balance_ul > 0 )
									{
										lftstation_ptr->bbf.w_reason_in_list = IN_LIST_FOR_PROGRAMMED_IRRIGATION;
						
										lftstation_ptr->bbf.w_to_set_expected = (false);
										
										lftstation_ptr->bbf.w_did_not_irrigate_last_time = lftstation_ptr->ftimes_support.did_not_irrigate_last_time_holding_copy;
										
										// 7/30/2018 rmd : Make sure to set this, if this slot was used for MP the value could be
										// different.
										lftstation_ptr->cycle_seconds_used_during_irrigation = lftstation_ptr->cycle_seconds_original_from_station_info;
										
										// ----------
										
										ftimes_add_to_the_irrigation_list( lftstation_ptr );
										
										// ----------
										
										// 3/15/2018 rmd : This is set (false) when the station turns ON.
										lftstation_ptr->ftimes_support.did_not_irrigate_last_time_holding_copy = (true);
	
										// ----------
										
										// 5/2/2018 rmd : And set the RESULTS TRACKING VARIABLES. Yes we do this for each station in
										// the station group that is eligible to irrigate, over and over again, easiest way to
										// handle setting them, at a small performance impact.
										lftstation_ptr->station_group_ptr->results_elapsed_irrigation_time_holding = 0;
	
										lftstation_ptr->station_group_ptr->results_start_date_and_time_holding = ftcs.calculation_date_and_time.date_time;
		
										lftstation_ptr->station_group_ptr->results_hit_the_stop_time_holding = (false);
										
										lftstation_ptr->station_group_ptr->results_exceeded_stop_time_by_seconds_holding = 0;
		
										// 5/3/2018 rmd : Set (true) in one of the main functions.
										lftstation_ptr->station_group_ptr->results_manual_programs_and_programmed_irrigation_clashed_holding = (false);
									}
				
								}  // of not done considering this station
								
							}  // of if the station irrigates on this date
							
						}  // of we are at a start time
						
					}  // of if it belongs to a station group
					
				}  // of if this station is a duplicate and in the list for MANUAL PROGRAM

				// ----------
				
				// 3/15/2018 rmd : MANUAL PROGRAMS SX check.
		

				// 7/30/2018 rmd : Duplicates only have a fleeting existence, and while they exist they are
				// on the irrigating list - by definition. If this station is a duplicate and on the list
				// for PROGRAMMED_IRRIGATION it is not eligible to be considered for the case of MP. If it
				// is NOT A DUPLICATE but shows a list reason of PROGRAMMED_IRRIGATION we want to fall
				// though into this if, so that we can check if it is on the irrigation list, and if not
				// make a duplicate if needed.
				if( !(lftstation_ptr->ftimes_support.is_a_duplicate_in_the_array && (lftstation_ptr->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION)) )
				{
					// 3/29/2018 rmd : First we have to make sure the station we're on can tolerate being added
					// to the irrigation list for MP (if needed) or do we need to add a duplicate station
					// because this station is already on the irrigation list for PI. But only do this if there
					// are starts detected - and only do this once for both MP that a station could belong to.
					number_of_mp_starts = 0;
					
					for( mp=0; mp<2; mp++ )
					{
						number_of_mp_starts += ftimes_number_of_starts_for_this_manual_program( lftstation_ptr->manual_program_ptrs[ mp ] );
					}
					
					if( number_of_mp_starts )
					{
						add_the_time_to_the_remaining_balance = (false);
			
						done_considering_this_station = (false);
					
						// 3/14/2018 rmd : If this station is currently on the irrigation list we have a tricky
						// series of considerations to go through, UNIQUE TO THE FTIMES IMPLEMENTATION. There is no
						// equivalent of this in the regular manual program SX check.
						if( nm_OnList( &ft_irrigating_stations_list_hdr, lftstation_ptr ) )
						{
							// 3/14/2018 rmd : If it is the list for PI, then we need to terminate the irrigation for
							// this station and remove it from the irrigation list, but not from the array.
							if( lftstation_ptr->bbf.w_reason_in_list == IN_LIST_FOR_MANUAL_PROGRAM )
							{
								// 3/29/2018 rmd : We will continue on with this station, and since it is already on the
								// irrigation list for the reason of MANUAL_PROGRAM, we will add to the remaining time to
								// reflect these newly detected starts. So our indication of such (true).
								add_the_time_to_the_remaining_balance = (true);
							}
							else
							if( lftstation_ptr->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
							{
								// 3/14/2018 rmd : Not going to use this array entry for a PI start, that's for sure, its
								// already in the list for MP.
								done_considering_this_station = (true);
								
								// ----------
				
								// 3/14/2018 rmd : If this list item is a duplicate?
								if( lftstation_ptr->ftimes_support.is_a_duplicate_in_the_array )
								{
									// 3/14/2018 rmd : This array entry is a duplicate, which implies there is another array
									// entry for this station, and that copy exists to handle MP. So nothing further to do, we
									// are skipping this station.
								}
								else
								{
									// 3/14/2018 rmd : So the array entry is not a duplicate, it is the primary entry and being
									// used for PI. Therefore we need to see if there is a duplicate array entry ahead to be
									// used for MP. If not we must add one.
									duplicate_ahead_ft_station_ptr = find_a_duplicate_ahead_of_this_station( lstation_count );
									
									if( duplicate_ahead_ft_station_ptr )
									{
										// 3/14/2018 rmd : If there is a duplicate it SHOULD be in the irrigation list AND in the
										// list for MP.
										if( nm_OnList( &ft_irrigating_stations_list_hdr, duplicate_ahead_ft_station_ptr ) && (duplicate_ahead_ft_station_ptr->bbf.w_reason_in_list == IN_LIST_FOR_MANUAL_PROGRAM ) )
										{
											// 3/15/2018 rmd : Okay, so the duplicate seems to be in order, and we are already set to be
											// done with this station.
										}
										else
										{
											// 3/15/2018 rmd : There's a problem! Either the duplicate is not in the irrigation list or
											// it is in the list for the wrong reason. Declare an error, restart the calculation, and
											// skip this station.
											ALERT_MESSAGE_WITH_FILE_NAME( "FTIMES ERROR: duplicate problem" );
											
											restart_calculation_due_to_error = (true);
										}
									}
									else
									{
										duplicate_ahead_ft_station_ptr = add_duplicate_ft_station( lstation_count, IN_LIST_FOR_MANUAL_PROGRAM );
	
										if( !duplicate_ahead_ft_station_ptr )
										{
											// 3/15/2018 rmd : Apparently the array is FULL! So declare a calculation error. BUT I
											// hesitate to actually restart the calculation as this would just happen over and over
											// again, flooding the alerts.
											ALERT_MESSAGE_WITH_FILE_NAME( "FTIMES ERROR: no room for duplicate" );
										} 

										// 7/30/2018 rmd : So we've added a duplicate. It is flagged as being in the list for the
										// reason of MANUAL_PROGRAM, but it is NOT in the list. And that is a good thing, otherwise
										// we'd think it was already irrigating for MP, which it is not.
										
										// 3/15/2018 rmd : And we are already set to be done with this station.
									}
								}
							}
							else
							{
								// 3/14/2018 rmd : This is an error. If a station is in the irrigation list it must be there
								// for either PI or MP. Restart the calculation!
								ALERT_MESSAGE_WITH_FILE_NAME( "FTIMES ERROR: station in irri list for UNK reason" );
								
								restart_calculation_due_to_error = (true);
							}
						}
						
						// ----------
						
						if( restart_calculation_due_to_error )
						{
							FTIMES_TASK_restart_calculation();
							
							// 3/29/2018 rmd : Terminate the SX check.
							break;
						}
						
						if( !done_considering_this_station )
						{
							for( mp=0; mp<2; mp++ )
							{
								number_of_mp_starts = ftimes_number_of_starts_for_this_manual_program( lftstation_ptr->manual_program_ptrs[ mp ] );
					
								if( number_of_mp_starts )
								{
									// 3/29/2018 rmd : So there are starts for this MP. If it is already in the list for MP just
									// add to the remaining time.
									if( add_the_time_to_the_remaining_balance )
									{
										lftstation_ptr->requested_irrigation_seconds_balance_ul += (number_of_mp_starts * lftstation_ptr->manual_program_ptrs[ mp ]->run_time_seconds);
									}
									else
									{
										lftstation_ptr->cycle_seconds_used_during_irrigation = lftstation_ptr->manual_program_ptrs[ mp ]->run_time_seconds;
			
										lftstation_ptr->requested_irrigation_seconds_balance_ul = (number_of_mp_starts * lftstation_ptr->manual_program_ptrs[ mp ]->run_time_seconds);
			
										lftstation_ptr->bbf.w_reason_in_list = IN_LIST_FOR_MANUAL_PROGRAM;
	
										// ----------
										
										ftimes_add_to_the_irrigation_list( lftstation_ptr );
										
										// ----------
										
										// 7/31/2018 rmd : Now be prepared, if the SECOND manual program has a start at this time
										// too. We know for sure the station is now on the irrigation list (we just added it), and
										// what we should do now is ADD the time to the balance. Don't try to put the station on the
										// list for a second time!
										add_the_time_to_the_remaining_balance = (true);
									}
									
								}
								
							}
							
						}
				
					}  // of we got some manual program starts

				}  // of if this station is a duplicate and in the list for PI
				
			}  // of if the slot is loaded
			
		}  // of for all stations
		
	}  // of if we decided to loop through all the stations
	
}

/* ---------------------------------------------------------- */
BOOL_32 ftimes_will_exceed_system_capacity( FT_STATION_STRUCT *const pftstation_ptr )
{
	// I don't think the all_remaining_greater_than_limit variable is necessary. If there are
	// none ON and one ready to turn on has an Estimated greater than the SystemMax - it's
	// greater than the system Max - why do we wait until the end to adjust it? why not adjust
	// it now??

	BOOL_32	rv;

	BOOL_32	with_pump;

	UNS_32	capacity;

	// ----------
	
	rv = (false);

	// ----------
	
	if( pftstation_ptr->system_ptr->capacity_in_use_bool )
	{
		// If the pump is on - which in the case of say radio remote or test the pump could be on
		// for one station and we are going to turn on a station that doesn't use the pump - we still
		// need to compare to the pump number.
		//
		// If the one we are to turn on is going to use the pump - compare to pump capacity	number.
	
		if( (pftstation_ptr->system_ptr->ufim_stations_ON_with_the_pump_b == (true)) || (pftstation_ptr->bbf.w_uses_the_pump == (true)) )
		{
			capacity = pftstation_ptr->system_ptr->capacity_with_pump_gpm;
			
			with_pump = (true);
		}
		else
		{
			capacity = pftstation_ptr->system_ptr->capacity_without_pump_gpm;

			with_pump = (false);
		}
		
		if( pftstation_ptr->system_ptr->system_master_number_of_valves_ON == 0 )
		{
			// If nothing is on - can check expected against capacity numbers. Include pSum so the logic
			// works even if pSum is not 0.
			if( pftstation_ptr->expected_flow_rate_gpm_u16 > capacity )
			{
				/*
				// Tell user what we are overriding the capacity limit.
				// 4/5/2018 rmd : THESE SHOULD BE COMMENTTED OUT AFTER TEST. TODO!!!!!!!!!!!!!!!!
				char	str_8[ 8 ];
	
				ALERTS_get_station_number_with_or_without_two_wire_indicator( pftstation_ptr->box_index_0, pftstation_ptr->station_number_0, str_8, sizeof(str_8) );

				if( with_pump )
				{
					Alert_Message_va( "FTIMES: Pump Capacity Override - Station %s expected is higher.", str_8 );
				}
				else
				{
					Alert_Message_va( "FTIMES: Non-Pump Capacity Override - Station %s expected is higher.", str_8 );
				}
				*/
				
				// Adjust it! So it will come on. Don't let the user step in a pile of shit i.e. blocking
				// out a station.
				capacity = pftstation_ptr->expected_flow_rate_gpm_u16;
			}
		}

		if( (pftstation_ptr->system_ptr->ufim_expected_flow_rate_for_those_ON + pftstation_ptr->expected_flow_rate_gpm_u16) > capacity )
		{
			rv = (true);
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
BOOL_32 ftimes_the_on_at_a_time_rules_in_the_mainline_allow_this_valve_to_come_ON( FT_STATION_STRUCT *const pftstation_ptr )
{
	BOOL_32	rv;

	// ----------
	
	rv = (false);

	// ----------
	
	// FIRST test. Are we under the limit for the valves on within this system?
	if( pftstation_ptr->system_ptr->system_master_number_of_valves_ON < pftstation_ptr->system_ptr->ufim_on_at_a_time__presently_allowed_ON_in_mainline__learned )
	{
		// If it's less update the limit. There is room according to the rules for the valves
		// already ON. Now test according to the rule for the valve we are planning on turning ON.
		if( pftstation_ptr->station_group_ptr->on_at_a_time__allowed_ON_in_mainline__user_setting > pftstation_ptr->system_ptr->system_master_number_of_valves_ON )
		{
			// This means the number presently ON is less than the system limit for the valve we are
			// asking to turn ON. So it could come ON and all system rules would be satified.
			rv = (true);
		}
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
BOOL_32 ftimes_can_another_valve_come_ON_within_this_station_group( FT_STATION_STRUCT *const pftstation_ptr )
{
	BOOL_32	rv;

	// ----------
	
	// 2011.11.02 rmd : If we cannot find the group or we've exceeded the limit, do not allow
	// the valve to turn ON.
	// 
	// 2012.02.17 ajv : Set this early on so that any failure will always result in returning
	// false.
	rv = (false);

	// ----------
	
	if( pftstation_ptr->station_group_ptr->on_at_a_time__presently_ON_in_station_group_count < pftstation_ptr->station_group_ptr->on_at_a_time__allowed_ON_in_station_group__user_setting )
	{
		// 2011.11.02 rmd : We are not yet at the limit. 
		rv = (true);
	}

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
void ftimes_merge_new_mainline_allowed_ON_into_the_limit( FT_STATION_STRUCT *const pftstation_ptr )
{
	// 4/19/2018 rmd : For on_at_a_time support.
	
	UNS_32	allowed_ON_within_mainline;

	allowed_ON_within_mainline = pftstation_ptr->station_group_ptr->on_at_a_time__allowed_ON_in_mainline__user_setting;

	if( allowed_ON_within_mainline < pftstation_ptr->system_ptr->ufim_on_at_a_time__presently_allowed_ON_in_mainline__learned )
	{
		// If it's less update the limit.
		pftstation_ptr->system_ptr->ufim_on_at_a_time__presently_allowed_ON_in_mainline__learned = allowed_ON_within_mainline;
	}

	// By the way now that we have adjusted the limit can we test against how many are ON. This
	// should not fail. Else we have turned on more valves than allowed and our logic failed to
	// get to this point.
	if( pftstation_ptr->system_ptr->system_master_number_of_valves_ON > pftstation_ptr->system_ptr->ufim_on_at_a_time__presently_allowed_ON_in_mainline__learned )
	{
		Alert_Message( "FTIMES: too many valves ON in mainline" );
	}
}
/* ---------------------------------------------------------- */
void ftimes_bump_this_station_groups_on_at_a_time_ON_count( FT_STATION_STRUCT *const pftstation_ptr )
{
	// 4/19/2018 rmd : For on_at_a_time support.
	
	pftstation_ptr->station_group_ptr->on_at_a_time__presently_ON_in_station_group_count += 1;
	
	if( pftstation_ptr->station_group_ptr->on_at_a_time__presently_ON_in_station_group_count > pftstation_ptr->station_group_ptr->on_at_a_time__allowed_ON_in_station_group__user_setting )
	{
		Alert_Message( "FTIMES: too many valves ON in station group" );
	}
}

/* ---------------------------------------------------------- */
void ftimes_set_remaining_seconds_on( FT_STATION_STRUCT *const pftstation_ptr )
{
	// We are here to adjust the remaining times in preparation of a turn on. if we return with
	// no time to run that is recognized as an error so set that error condition now.
	pftstation_ptr->remaining_seconds_ON = 0;
	
	// Set the time to run for based on why its on.
	if( (pftstation_ptr->bbf.w_reason_in_list == IN_LIST_FOR_MANUAL_PROGRAM) || (pftstation_ptr->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION) )
	{
		// When a station is added to the irrigation list for the SMS reason, if the station doesn't
		// exsist in the list for that reason set the cycle time equal to the total time and set the
		// total time to the amount to irrigate. If it already exists add to the amount to irrigate
		// and leave the cycle time alone. Therefore here we look at the total as compared to the
		// cycle and figure the remaining seconds on just like for regular programmed irrigation.
		if( pftstation_ptr->requested_irrigation_seconds_balance_ul > pftstation_ptr->cycle_seconds_used_during_irrigation )
		{
			pftstation_ptr->remaining_seconds_ON = pftstation_ptr->cycle_seconds_used_during_irrigation;

			pftstation_ptr->requested_irrigation_seconds_balance_ul -= pftstation_ptr->cycle_seconds_used_during_irrigation;
		}
		else
		{
			pftstation_ptr->remaining_seconds_ON = pftstation_ptr->requested_irrigation_seconds_balance_ul;

			pftstation_ptr->requested_irrigation_seconds_balance_ul = 0;
		}
	}
}
	
/* ---------------------------------------------------------- */
void ftimes_init_ufim_variables_for_all_systems( void )
{
	FT_SYSTEM_GROUP_STRUCT	*ftsystem_ptr;
	
	ftsystem_ptr = nm_ListGetFirst( &ft_system_groups_list_hdr );
	
	while( ftsystem_ptr != NULL )
	{
		// 4/26/2018 rmd : We know all the systems in this list are in_use and used for irrigation,
		// so no need to make those tests.
		
		// ----------
		
		// 2011.11.09 rmd : DO NOT reset the "what_we_are_turning_on" variable here. It is intended
		// to be preserved between calls to the foal irrigation maintenance function. The rule here
		// is to initialize those to be LEARNED each pass through the maintenance list. And they are
		// only used within the list maintenance function.
	
		ftsystem_ptr->ufim_valves_in_the_list_for_this_system = UNS_32_MIN;
	
	
		ftsystem_ptr->ufim_on_at_a_time__presently_allowed_ON_in_mainline__learned = UNS_32_MAX;
	
	
		ftsystem_ptr->ufim_one_ON_from_the_problem_list_b = (false);
	
	
		ftsystem_ptr->ufim_highest_priority_pump_waiting = PRIORITY_LOW;
	
		ftsystem_ptr->ufim_highest_priority_non_pump_waiting = PRIORITY_LOW;
	
	
		ftsystem_ptr->ufim_list_contains_waiting_programmed_irrigation_b = (false);
	
		ftsystem_ptr->ufim_list_contains_waiting_pump_valves_b = (false);
	
		ftsystem_ptr->ufim_list_contains_waiting_non_pump_valves_b = (false);
	
	
		ftsystem_ptr->ufim_list_contains_some_to_setex_that_are_not_ON_b = (false);
	
	
		ftsystem_ptr->ufim_list_contains_some_pump_to_setex_that_are_not_ON_b = (false);
	
		ftsystem_ptr->ufim_list_contains_some_non_pump_to_setex_that_are_not_ON_b = (false);
	
	
		ftsystem_ptr->ufim_there_is_a_PUMP_mix_condition_b = (false);
	
		ftsystem_ptr->ufim_based_on_reason_in_list_at_least_one_valve_is_ON_that_wants_to_check_flow = (false);
	
		// Capture this for use in the turn on logic. If there is a mix condition we don't want to
		// aggrevate the mix in the case of programmed irrigation by adding more valves to the mix.
		// We want programmed irrigation valves to run down until there is no longer a mix - then we may
		// turn on programmed irrigation valves that are ready. Set these in support of determining
		// ThereIsAMixOfPumpAndNonPumpValvesOn. And also used to signal the IRRI MACHINES to
		// activate their pump output.
		ftsystem_ptr->ufim_stations_ON_with_the_pump_b = (false);
		ftsystem_ptr->ufim_stations_ON_without_the_pump_b = (false);
	
	
		// Can start at a 0 that's fine - even though we are using priority level 0 (SMS_HO).
		ftsystem_ptr->ufim_highest_reason_in_list_available_to_turn_ON = UNS_32_MIN;
		ftsystem_ptr->ufim_highest_pump_reason_in_list_available_to_turn_ON = UNS_32_MIN;
		ftsystem_ptr->ufim_highest_non_pump_reason_in_list_available_to_turn_ON = UNS_32_MIN;
		
		// ----------
		
		ftsystem_ptr = nm_ListGetNext( &ft_system_groups_list_hdr, ftsystem_ptr );
	}
	
	// ----------
	
	// 5/3/2018 rmd : And clear the global that tracks if there are any MANUAL PROGRAM stations
	// trying to irrigate in the list.
	ufim_one_or_more_in_the_list_for_manual_program = (false);
}

/* ---------------------------------------------------------- */
void ftimes_init_on_at_a_time_and_completion_variables_in_all_station_groups( void )
{
	FT_STATION_GROUP_STRUCT	*lgroup;
	
	lgroup = nm_ListGetFirst( &ft_station_groups_list_hdr );
	
	while( lgroup != NULL )
	{
		lgroup->on_at_a_time__presently_ON_in_station_group_count = 0;
		
		// ----------
		
		// 6/22/2018 rmd : And once each second, at the start of the maintain function, initialize
		// the completion tracking variables.
		lgroup->at_start_of_second_one_or_more_in_the_list_for_programmed_irrigation = (false);

		lgroup->after_removal_opportunity_one_or_more_in_the_list_for_programmed_irrigation = (false);

		lgroup->for_this_SECOND_add_to_exceeded_stop_time_by_seconds_holding = (true);
		
		// ----------
		
		lgroup = nm_ListGetNext( &ft_station_groups_list_hdr, lgroup );
	}
}

/* ---------------------------------------------------------- */
void ftimes_for_each_station_group_check_if_irrigation_just_completed( void )
{
	FT_STATION_GROUP_STRUCT	*lgroup;
	
	//BOOL_32		capture_finish;
	
	// ----------
	
	lgroup = nm_ListGetFirst( &ft_station_groups_list_hdr );
	
	while( lgroup != NULL )
	{

		if( lgroup->at_start_of_second_one_or_more_in_the_list_for_programmed_irrigation )
		{
			if( lgroup->after_removal_opportunity_one_or_more_in_the_list_for_programmed_irrigation )
			{
				// 7/9/2018 rmd : Its still irrigating, so increment the time. BUT NOT IF THE CALCULATION
				// TIME/DATE IS AT THE START TIME/DATE, because in effect there is no elapsed time yet, so
				// do not add to it. Include the date in the comparison to handle irrigations that last more
				// than 24 hours.
				if( (ftcs.calculation_date_and_time.date_time.D != lgroup->results_start_date_and_time_holding.D) || (ftcs.calculation_date_and_time.date_time.T != lgroup->results_start_date_and_time_holding.T) )
				{
					lgroup->results_elapsed_irrigation_time_holding += ftcs.seconds_to_advance;
				}

				lgroup->results_latest_finish_date_and_time_holding = ftcs.calculation_date_and_time.date_time;
			}
			else
			{
				// 7/9/2018 rmd : Apparently the last station in this station group has been removed. Test
				// if this was the longest to complete?
				if( lgroup->results_elapsed_irrigation_time_holding > lgroup->results_elapsed_irrigation_time )
				{
					// 7/9/2018 rmd : Update our criteria for the test.
					lgroup->results_elapsed_irrigation_time = lgroup->results_elapsed_irrigation_time_holding;

					// 7/9/2018 rmd : It took the longest time to complete, so capture the ending.
					lgroup->results_latest_finish_date_and_time = lgroup->results_latest_finish_date_and_time_holding;
					
					// 7/19/2018 Ryan : Round to the nearest minute for display.
					lgroup->results_latest_finish_date_and_time.T = ( lgroup->results_latest_finish_date_and_time.T + 30 );
					lgroup->results_latest_finish_date_and_time.T = lgroup->results_latest_finish_date_and_time.T - ( lgroup->results_latest_finish_date_and_time.T % 60 );
					
					// 6/22/2018 rmd : And capture the assciated start date and time.
					lgroup->results_start_date_and_time_associated_with_the_latest_finish_date_and_time = lgroup->results_start_date_and_time_holding;
					
					// 6/22/2018 rmd : And the STOP time trivia associated with this irrigation too.
					lgroup->results_hit_the_stop_time = lgroup->results_hit_the_stop_time_holding;
					
					lgroup->results_exceeded_stop_time_by_seconds = lgroup->results_exceeded_stop_time_by_seconds_holding;

					lgroup->results_manual_programs_and_programmed_irrigation_clashed = lgroup->results_manual_programs_and_programmed_irrigation_clashed_holding;
				}
			}
		}
		
		// ----------
		
		lgroup = nm_ListGetNext( &ft_station_groups_list_hdr, lgroup );
	}
}

/* ---------------------------------------------------------- */
static void ftimes_decrement_system_timers( void )
{
	FT_SYSTEM_GROUP_STRUCT	*ftsystem_ptr;
	
	ftsystem_ptr = nm_ListGetFirst( &ft_system_groups_list_hdr );
	
	while( ftsystem_ptr != NULL )
	{
		// 4/26/2018 rmd : We know all the systems in this list are in_use and used for irrigation,
		// so no need to make those tests.
		
		// ----------
		
		if( ftsystem_ptr->inhibit_next_turn_ON_remaining_seconds > 0 )
		{
			ftsystem_ptr->inhibit_next_turn_ON_remaining_seconds -= 1;
		}

		// ----------
		
		ftsystem_ptr = nm_ListGetNext( &ft_system_groups_list_hdr, ftsystem_ptr );
	}
}

/* ---------------------------------------------------------- */
static BOOL_32 ftimes_station_is_eligible_to_be_removed_from_the_irrigating_list( FT_STATION_STRUCT* pftstation_ptr )
{
	// 7/21/2018 rmd : A function created so we didn't have to recreate the list removal
	// criteria in multiple places within the code. We use this test in a few places.

	BOOL_32	rv;
	
	// 7/21/2018 rmd : A station should be removed from the irrigation list if it is not ON and
	// it has no remaining requested time. After its final irrigation cycle it could considered
	// soaking, but that final 'perpetual' soak is not part of the removal criteria. As a matter
	// of fact, testing the remaining soak seconds should be excluded all together from the list
	// removal test, because any legit soak would always be accompanied by remaining requested
	// time.
	if( !pftstation_ptr->bbf.station_is_ON && (pftstation_ptr->requested_irrigation_seconds_balance_ul == 0) )
	{
		rv = (true);
	}
	else
	{
		rv = (false);
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
static void ftimes_irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations( void )
{
	// 3/31/2018 rmd : This is executed within the context of the TDCHECK task, and only for the
	// REAL irrigation maintenance. NOT USED FOR FTIMES.
	
	// ----------
	
	FT_STATION_STRUCT	*lftstation_ptr;
	
	DATE_TIME			stop_dt;

	BOOL_32				ilc_still_in_the_list_and_need_to_get_the_next_ilc;

	BOOL_32				for_this_STATION_add_to_exceeded_stop_time_by_seconds_holding;
	
	// ----------
	
	lftstation_ptr = nm_ListGetFirst( &ft_irrigating_stations_list_hdr );
	
	// Actually the only reason we run the following loop when there are stations on the xfer list
	// is to keep the decrement of soak seconds moving down so that we don't lengthen the soak times
	// by some unexpected and indeterminent amount.
	while( lftstation_ptr != NULL )
	{
		// 07/13/2018 dmd: Check if the station is already done with it run. If yes, move on to next one
		// in the irrigation list.
		if ( lftstation_ptr->ftimes_support.done_with_runtime_cycle )
		{
			// Done with this station, it is already removed and re-inserted in the list. 
			// So just jump to the next station in the irrigating list.
			
			lftstation_ptr = nm_ListGetNext( &ft_irrigating_stations_list_hdr, lftstation_ptr );
			continue;
		}

		// station is going to run its cycle now, so set the flag so that it 
		// is not considered twice for the same run cycle. 
		lftstation_ptr->ftimes_support.done_with_runtime_cycle = (true);

		//THIS MUST LOOP THROUGH ALL STATIONS IN THE LIST. Certain settings - such as Pump Outputs 
		// Should Be On and There Is A Mix Of Pump And Non Pump Valves On depend upon a complete
		// look at all station in the list.
		
		// ----------
		
		// 9/25/2012 rmd : As we encounter removal situations, such as hitting the stop time, this
		// flag is set (false) so that we do not include the station in the list state details
		// gathered.
		ilc_still_in_the_list_and_need_to_get_the_next_ilc = (true);
		
		for_this_STATION_add_to_exceeded_stop_time_by_seconds_holding = (false);
		
		// ----------
		
		// 10/5/2012 rmd : Only honor the stop time and date for the case of programmed irrigation.
		if( lftstation_ptr->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
		{
			stop_dt.D = lftstation_ptr->stop_datetime_d;
			stop_dt.T = lftstation_ptr->stop_datetime_t;
			
			if( DT1_IsBiggerThanOrEqualTo_DT2( &ftcs.calculation_date_and_time.date_time, &stop_dt ) == (true) )
			{
				// 5/3/2018 rmd : If we detect the transition we can correctly tally the seconds, otherwise
				// we'll be off by one second, and that bothered me.
				if( lftstation_ptr->station_group_ptr->results_hit_the_stop_time_holding == (false) )
				{
					// 5/3/2018 rmd : We are AT the stop time. This should not count as a second of irrigation
					// exceeding the stop time.
					lftstation_ptr->station_group_ptr->for_this_SECOND_add_to_exceeded_stop_time_by_seconds_holding = (false);
				}
				
				// 5/2/2018 rmd : For FINISH TIMES, all we do is set a flag and keep on irrigating - to see
				// where it would finally finish if there were no stop time. In other words we do not stop
				// the irrigation when we hit the stop time.
				lftstation_ptr->station_group_ptr->results_hit_the_stop_time_holding = (true);
				
				// 6/22/2018 rmd : Only for Programmed Irrigation, and only if it still has more irrigation
				// to run (tested later), are we going to add to the exceeded STOP time accumulator.
				for_this_STATION_add_to_exceeded_stop_time_by_seconds_holding = (true);
			}
			
			// ----------
			
			// 6/22/2018 rmd : Regardless of hitting STOP time or not, the station is in the irrigation
			// list, so set flag that his station group is still running. Remember this flag was set
			// false at start of maintain sequence of function calls.
			lftstation_ptr->station_group_ptr->at_start_of_second_one_or_more_in_the_list_for_programmed_irrigation = (true);
			
		}
		
		// ----------
		
		// 2011.11.09 rmd : Perhaps should only be counted if they are not being removed from the
		// list cause they are done.
		lftstation_ptr->system_ptr->ufim_valves_in_the_list_for_this_system += 1;
		
		// ----------
		
		// 5/3/2018 rmd : During ftimes need to see is there is anybody in the list for manual
		// programs, so we can flag the MANUAL PROGRAMS / PROGRAMMED IRRIGATION clash in the FOURTH
		// maintain function.
		if( (lftstation_ptr->bbf.w_reason_in_list) == IN_LIST_FOR_MANUAL_PROGRAM )
		{
			ufim_one_or_more_in_the_list_for_manual_program = (true);
		}
		
		// ----------
		
		// Find the highest one in the list - ABLE TO IRRIGATE - whether its ON or not!
		//
		// Because of the ordering of the list (the reason in the list is the highest order sort
		// criteria) we can be sure that the first station we look at is the highest priority reason
		// in the list. But that may not be true since we introduced the "RemainingSoakSeconds == 0"
		// term. We can be sure that once the highest_reason_in_list is set it won't get set again.
		// We can be assured that the soak time is ONLY set when a soaking is intended. If it is
		// unintentionally set not only will this logic fail and we'll have problems with stations
		// not coming on when expected cause of the soak term in the turn on criteria loop.
		if ( lftstation_ptr->soak_seconds_remaining_ul == 0 )
		{
			// 608 rmd : A comment. Don't be fooled by these statements and the variable names such as
			// "highest_reason_in_list_uc" - what they REALLY mean is "highest reason in list available
			// for turn ON". This distinction is because of the "RemainingSoakSeconds == 0" term.
			if ( lftstation_ptr->bbf.w_reason_in_list > (lftstation_ptr->system_ptr->ufim_highest_reason_in_list_available_to_turn_ON) )
			{
				// Set the "highest_reason_in_list".
				lftstation_ptr->system_ptr->ufim_highest_reason_in_list_available_to_turn_ON = lftstation_ptr->bbf.w_reason_in_list;
			}

			// 607 ajv : Find the highest reasons in the list for both pump valves and non-pump valves.
			if( (lftstation_ptr->bbf.w_uses_the_pump == (true)) && (lftstation_ptr->bbf.w_reason_in_list > (lftstation_ptr->system_ptr->ufim_highest_pump_reason_in_list_available_to_turn_ON)) )
			{
				lftstation_ptr->system_ptr->ufim_highest_pump_reason_in_list_available_to_turn_ON = lftstation_ptr->bbf.w_reason_in_list;
			}

			if( (lftstation_ptr->bbf.w_uses_the_pump == (false)) && (lftstation_ptr->bbf.w_reason_in_list > (lftstation_ptr->system_ptr->ufim_highest_non_pump_reason_in_list_available_to_turn_ON)) )
			{
				lftstation_ptr->system_ptr->ufim_highest_non_pump_reason_in_list_available_to_turn_ON = lftstation_ptr->bbf.w_reason_in_list;
			}
		}
		
		// ----------

		// 7/21/2018 rmd : Check here to add to the exceeded STOP TIME HOLDING. We do it here while
		// the lftstation_ptr is still pointed at this station, remember if we turn a station OFF we
		// bump the station pointer to the next station in the list (related to the call to that
		// 'remove and reinsert' function). Bottom line - must use the lftstation_ptr now.
		//
		// 7/21/2018 rmd : The check here to decide if we are going to add to the exceeded stop time
		// holding essentially can be read as 'if the station is NOT eligible to be removed from the
		// list, add the time'. (NOTE - Deepali gets the credit for cleaning this up - she tested
		// and found a bug here, now fixed of course.)
		if( !ftimes_station_is_eligible_to_be_removed_from_the_irrigating_list( lftstation_ptr ) )
		{
			if( lftstation_ptr->station_group_ptr->for_this_SECOND_add_to_exceeded_stop_time_by_seconds_holding )
			{
				if( for_this_STATION_add_to_exceeded_stop_time_by_seconds_holding )
				{
					lftstation_ptr->station_group_ptr->results_exceeded_stop_time_by_seconds_holding += ftcs.seconds_to_advance;

					// 5/3/2018 rmd : And now prevent any further adds to the accumulator during this second.
					lftstation_ptr->station_group_ptr->for_this_SECOND_add_to_exceeded_stop_time_by_seconds_holding = (false);
				}
			}
		}

		// ----------
		
		if( lftstation_ptr->bbf.station_is_ON )
		{
			// 06.07.2006 ajv - Verify that, since the station status is ON that the station is, in fact,
			// on the ONList.
			if( nm_OnList( &ft_stations_ON_list_hdr, lftstation_ptr ) == (false) )
			{
				Alert_Message( "FTIMES: not on ON list!" );
			}

			// ----------
			
			// If its ON we adjust remaining seconds.
			lftstation_ptr->remaining_seconds_ON -= ftcs.seconds_to_advance;

			if( lftstation_ptr->remaining_seconds_ON < 0 )
			{
				Alert_Message ( "FTIMES: -ve remaining seconds!!" );
			}


			// ----------
			
			// 2011.11.09 rmd : This is where we actually turn OFF the valves. As opposed how we used to
			// let the irri machines do the thinking for that. In this new CS3000 fully centralized
			// environment the irri machines are mostly just slaves!
			if( lftstation_ptr->remaining_seconds_ON <= 0 )
			{
				// 11/11/2014 rmd : The CYCLE has ended. Turn OFF the station. But do not remove from the
				// irrigation lists. There may well be remaining run time. The turn OFF function does place
				// the ilc on the action needed list too.
				lftstation_ptr = ftimes_if_station_is_ON_turn_it_OFF( &ft_irrigating_stations_list_hdr, lftstation_ptr );
				
				ilc_still_in_the_list_and_need_to_get_the_next_ilc = (false);
			}
			else
			{
				// 6/1/2012 rmd : Maintain if any valves are ON that want to check flow. This is support for
				// a sanity check about is there a mix of flow groups among the valves ON.
				if( lftstation_ptr->bbf.flow_check_when_possible_based_on_reason_in_list )
				{
					lftstation_ptr->system_ptr->ufim_based_on_reason_in_list_at_least_one_valve_is_ON_that_wants_to_check_flow = (true);
				}

				// ----------
				
				// 4/4/2018 rmd : Update the ON-AT-A_TIME tracking in the mainline and the station group.
				ftimes_merge_new_mainline_allowed_ON_into_the_limit( lftstation_ptr );
				
				ftimes_bump_this_station_groups_on_at_a_time_ON_count( lftstation_ptr );
				
				// ----------
				
				// 5/3/2012 rmd : Keep our pump needed flags up to date.
				if( lftstation_ptr->bbf.w_uses_the_pump == (true) )
				{
					lftstation_ptr->system_ptr->ufim_stations_ON_with_the_pump_b = (true);
				}
				else
				{
					lftstation_ptr->system_ptr->ufim_stations_ON_without_the_pump_b = (true);
				}

			}  // of there is still time left

		}  // of if it is ON
		else
		{
			// STATION IS OFF

			// 2011.12.07 rmd : L06.07.2006 ajv - Verify that, since the station status is OFF that the
			// station is, in fact, no longer on the ONList.  If not, we need to pull the chain down
			// into forced to destroy and rebuild the lists. 2011.11.03 rmd : Just alert about it. No
			// more of this pulling the chain down.
			if( nm_OnList( &ft_stations_ON_list_hdr, lftstation_ptr ) )
			{
				Alert_Message( "FTIMES: is OFF but on the ON list" );
			}
			
			// ----------
			
			// 7/9/2018 rmd : Decrement SOAK time.
			if ( lftstation_ptr->soak_seconds_remaining_ul > 0 )
			{
				// 7/9/2018 rmd : But guard against generating a huge positive number.
				if( lftstation_ptr->soak_seconds_remaining_ul > ftcs.seconds_to_advance )
				{
					lftstation_ptr->soak_seconds_remaining_ul -= ftcs.seconds_to_advance;
				}
				else
				{
					lftstation_ptr->soak_seconds_remaining_ul = 0;
				}
			}
			
			// ----------

			// 6/5/2012 rmd : REGULAR LIST REMOVAL HERE - If the station is OFF and the requested
			// seconds has been driven to zero, it is done.
			if( ftimes_station_is_eligible_to_be_removed_from_the_irrigating_list( lftstation_ptr ) )
			{
				// 11/5/2014 rmd : We know it is not ON but this routine function call performs the steps we
				// need to do. That is to put the ilc on the action_needed list, to get to the next ilc in
				// the list, and to remove the ilc from the list.
				lftstation_ptr = ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists( &ft_irrigating_stations_list_hdr,  lftstation_ptr );
				
				ilc_still_in_the_list_and_need_to_get_the_next_ilc = (false);
				
				// 5/3/2018 rmd : And this station should not trigger an increase in the exceeded stop time
				// accumulation. It stopped irrigating the 'prior' second, and is only here to be removed
				// during this second.
				for_this_STATION_add_to_exceeded_stop_time_by_seconds_holding = (false);
			}
			else
			{
				// 6/7/2012 rmd : Do not keep up list statistics on a station no longer in the list. That
				// just doesn't seem right now does it!
					
				if( lftstation_ptr->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
				{
					// Looking at stations that are OFF. If they are not soaking and have regular time left to
					// irrigate they are waiting.
					if( (lftstation_ptr->soak_seconds_remaining_ul == 0) && (lftstation_ptr->requested_irrigation_seconds_balance_ul > 0) )
					{
						// 5/19/2015 ajv : Find the highest priorities for both pump and non-pump valves. Note the
						// '>' symbol... This is because PRIORITY_LOW is 1 while PRIORITY_HIGH is 3.
						if( (lftstation_ptr->bbf.station_priority > lftstation_ptr->system_ptr->ufim_highest_priority_pump_waiting) && lftstation_ptr->bbf.w_uses_the_pump )
						{
							lftstation_ptr->system_ptr->ufim_highest_priority_pump_waiting = lftstation_ptr->bbf.station_priority;
						}
	
						if ( (lftstation_ptr->bbf.station_priority > lftstation_ptr->system_ptr->ufim_highest_priority_non_pump_waiting) && !lftstation_ptr->bbf.w_uses_the_pump )
						{
							lftstation_ptr->system_ptr->ufim_highest_priority_non_pump_waiting = lftstation_ptr->bbf.station_priority;
						}
					}
				}
				
				// ----------
				
				// 607 ajv : Examine each ilc to see whether there are any pump valves and non-pump valves
				// waiting. This will be used to determine whether to continue turning on what we have been,
				// or whether we need to switch from pump to non-pump or vice versa. Looking at stations
				// that are OFF. If they are not soaking and have regular time left to irrigate they are
				// waiting.
				if( (lftstation_ptr->soak_seconds_remaining_ul == 0) && (lftstation_ptr->requested_irrigation_seconds_balance_ul > 0) )
				{
					if( lftstation_ptr->bbf.w_uses_the_pump )
					{
						lftstation_ptr->system_ptr->ufim_list_contains_waiting_pump_valves_b = (true);
					}
					else
					{
						lftstation_ptr->system_ptr->ufim_list_contains_waiting_non_pump_valves_b = (true);
					}
				}
	
			}  // of if we didn't remove the station from the list ie it wasn't done
			
		}  // if entry is OFF
		
		// ----------
		
		// 5/10/2012 rmd : If we turned OFF a valve or removed the ilc from the list the next list
		// item has already been obtained.
		if( ilc_still_in_the_list_and_need_to_get_the_next_ilc )
		{
			lftstation_ptr = nm_ListGetNext( &ft_irrigating_stations_list_hdr, lftstation_ptr );
		}

	}  // of all stations
}

/* ---------------------------------------------------------- */
static void ftimes_irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars( void )
{
	FT_SYSTEM_GROUP_STRUCT		*ftsystem_ptr;

	// ----------

	ftsystem_ptr = nm_ListGetFirst( &ft_system_groups_list_hdr );

	while( ftsystem_ptr )
	{
		// 4/4/2018 rmd : By definition any systems (mainlines) in the ftimes list are in use and
		// used for irrigation. That criteria is screened before adding to the list.

		// SANITY check here.
		if( ftsystem_ptr->system_master_number_of_valves_ON == 0 )
		{
			if( ftsystem_ptr->ufim_expected_flow_rate_for_those_ON != 0 )
			{
				Alert_Message( "FOAL_IRRI: expected flow rate should be zero." );

				ftsystem_ptr->ufim_expected_flow_rate_for_those_ON = 0;  // fix it up --- why not
			}
		}

		// ----------
		

		// got to track the clash by mainline ??????????
		// 6/22/2018 rmd : I think this refers to a clash with Manual Programs


		// 607 ajv : Now that we have a number of statistics related to pump and non-pump activity
		// and items in the list, we need to determine whether we should continue turning on what we
		// were turning on last pass or whether we need to switch from pump to non-pump or vice
		// versa.
		if( ftsystem_ptr->ufim_valves_in_the_list_for_this_system == 0 )
		{
			ftsystem_ptr->ufim_what_are_we_turning_on_b = TURNING_ON_PUMP_VALVES;
		}
		else
		{	// the list is not empty
	
			// 5/19/2015 ajv : Check the priority levels for pump and non-pump valves first. Note the
			// '>' symbol... This is because PRIORITY_LOW is 1 while PRIORITY_HIGH is 3.
			if( (ftsystem_ptr->ufim_highest_priority_pump_waiting > ftsystem_ptr->ufim_highest_priority_non_pump_waiting) &&
				(ftsystem_ptr->ufim_highest_reason_in_list_available_to_turn_ON == IN_LIST_FOR_PROGRAMMED_IRRIGATION) )
			{
				ftsystem_ptr->ufim_what_are_we_turning_on_b = TURNING_ON_PUMP_VALVES;
			}
			else if( (ftsystem_ptr->ufim_highest_priority_non_pump_waiting > ftsystem_ptr->ufim_highest_priority_pump_waiting) &&
					 (ftsystem_ptr->ufim_highest_reason_in_list_available_to_turn_ON == IN_LIST_FOR_PROGRAMMED_IRRIGATION) )
			{
				ftsystem_ptr->ufim_what_are_we_turning_on_b = TURNING_ON_NON_PUMP_VALVES;
			}
			else	// priorities are all equal OR there is a higher reason in the list than PROGRAMMED IRRIGATION
			{	
				if ( ftsystem_ptr->ufim_stations_ON_with_the_pump_b == (true) )
				{
					ftsystem_ptr->ufim_what_are_we_turning_on_b = TURNING_ON_PUMP_VALVES;
				}
				else
				if ( ftsystem_ptr->ufim_stations_ON_without_the_pump_b == (true) )
				{
					ftsystem_ptr->ufim_what_are_we_turning_on_b = TURNING_ON_NON_PUMP_VALVES;
				}
				else
				{	// nothing is ON
	
					// We only really care about the highest reason in the list if
					// it is higher than Manual Programs and Programmed Irrigation
					if ( ftsystem_ptr->ufim_highest_reason_in_list_available_to_turn_ON > IN_LIST_FOR_MANUAL_PROGRAM )
					{
						if ( ftsystem_ptr->ufim_highest_pump_reason_in_list_available_to_turn_ON > ftsystem_ptr->ufim_highest_non_pump_reason_in_list_available_to_turn_ON ) {
						
							ftsystem_ptr->ufim_what_are_we_turning_on_b = TURNING_ON_PUMP_VALVES;
	
						} else if ( ftsystem_ptr->ufim_highest_pump_reason_in_list_available_to_turn_ON < ftsystem_ptr->ufim_highest_non_pump_reason_in_list_available_to_turn_ON ) {
	
							ftsystem_ptr->ufim_what_are_we_turning_on_b = TURNING_ON_NON_PUMP_VALVES;
	
						} else {	// the highest reasons are equal
	
							// Make no change - keep WhatAreWeTurning on to whatever
							// we are currently turning on - it will switch whenever
							// the highest priority of this type has completed
	
						}
					
					}
					else
					{	// there is nothing higher than Manual Programs & Programmed Irrigation
	
						// 609.a ajv
						// What if we are acquiring expecteds? A potential hole exists where a pump station
						// can acquire an expected and then will never switch to non-pump to allow the non-
						// pump station to acquire its expected.
						//
						if ( ftsystem_ptr->ufim_list_contains_some_to_setex_that_are_not_ON_b == (true) ) {
							
							if ( ( ftsystem_ptr->ufim_what_are_we_turning_on_b == TURNING_ON_PUMP_VALVES ) &&
								 ( ftsystem_ptr->ufim_list_contains_some_pump_to_setex_that_are_not_ON_b == (true) ) ) {
								
								ftsystem_ptr->ufim_what_are_we_turning_on_b = TURNING_ON_PUMP_VALVES;
								
							} else if ( ( ftsystem_ptr->ufim_what_are_we_turning_on_b == TURNING_ON_NON_PUMP_VALVES ) &&
								 ( ftsystem_ptr->ufim_list_contains_some_non_pump_to_setex_that_are_not_ON_b == (true) ) ) {
								
								ftsystem_ptr->ufim_what_are_we_turning_on_b = TURNING_ON_NON_PUMP_VALVES;
								
							} else {	// we need to switch from pump to non-pump or vice versa
	
								if ( ftsystem_ptr->ufim_what_are_we_turning_on_b == TURNING_ON_PUMP_VALVES ) {
	
									ftsystem_ptr->ufim_what_are_we_turning_on_b = TURNING_ON_NON_PUMP_VALVES;
	
								} else {
	
									ftsystem_ptr->ufim_what_are_we_turning_on_b = TURNING_ON_PUMP_VALVES;
	
								}
	
							}
							
						} else {
							
							if ( ( ftsystem_ptr->ufim_what_are_we_turning_on_b == TURNING_ON_PUMP_VALVES ) &&
								 ( ftsystem_ptr->ufim_list_contains_waiting_pump_valves_b == (true) ) ) {
		
								ftsystem_ptr->ufim_what_are_we_turning_on_b = TURNING_ON_PUMP_VALVES;
		
							} else if ( ( ftsystem_ptr->ufim_what_are_we_turning_on_b == TURNING_ON_NON_PUMP_VALVES ) &&
										( ftsystem_ptr->ufim_list_contains_waiting_non_pump_valves_b == (true) ) ) {
		
								ftsystem_ptr->ufim_what_are_we_turning_on_b = TURNING_ON_NON_PUMP_VALVES;
		
							} else {	// we need to switch from pump to non-pump or vice versa
		
								if ( ftsystem_ptr->ufim_what_are_we_turning_on_b == TURNING_ON_PUMP_VALVES ) {
		
									ftsystem_ptr->ufim_what_are_we_turning_on_b = TURNING_ON_NON_PUMP_VALVES;
		
								} else {
		
									ftsystem_ptr->ufim_what_are_we_turning_on_b = TURNING_ON_PUMP_VALVES;
		
								}
		
							}
							
						}
	
					}
	
				}
	
			}
	
		}
		
		// --------------------

		// SANITY test on one aspect of the statistics gathered. Cannot have valves ON from multiple
		// flow groups at once except during test - during which we allow it.
		if( (ftsystem_ptr->ufim_based_on_reason_in_list_at_least_one_valve_is_ON_that_wants_to_check_flow) && (ftsystem_ptr->ufim_number_ON_during_test <= 1) )
		{
			if( FOAL_IRRI_there_is_more_than_one_flow_group_ON( &ftsystem_ptr->ufim_flow_check_group_count_of_ON[ 0 ] ) )
			{
				Alert_Message( "FTIMES: mix of flow groups ON." );
			}
		}
		
		// --------------------

		// 2000e : Set this global in support of flow checking. We shouldn't check flow when there
		// are pumper valves on with non-pumper valves also on. The way this is coded today this can
		// ONLY happen with TEST and DTMF.
		//
		// 2000e : And now we have modified the code so that we never test flow for DTMF and we only
		// test flow when doing TEST if one valve is on - when more than one we do not test flow -
		// so this term is esentially useless but keep it around now that it is here.
		if( (ftsystem_ptr->ufim_stations_ON_without_the_pump_b == (true)) && (ftsystem_ptr->ufim_stations_ON_with_the_pump_b == (true)) )
		{
			ftsystem_ptr->ufim_there_is_a_PUMP_mix_condition_b = (true);
		}
		else
		{
			ftsystem_ptr->ufim_there_is_a_PUMP_mix_condition_b = (false);
		}
		
		// ----------

		ftsystem_ptr = nm_ListGetNext( &ft_system_groups_list_hdr, ftsystem_ptr );

	}  // of each mainline in the list

}

/* ---------------------------------------------------------- */
static void ftimes_irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON( void )
{
	FT_STATION_STRUCT	*lftstation_ptr;

	// ----------

	lftstation_ptr = nm_ListGetFirst( &ft_irrigating_stations_list_hdr );

	while( lftstation_ptr )
	{
		// 6/22/2018 rmd : Set the station group tracking flag indicating the station group this
		// station belongs to still has stations in the list. Remember this flag is set (false) at
		// start of the one hertz maintain processing function suite.
		if( lftstation_ptr->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
		{
			lftstation_ptr->station_group_ptr->after_removal_opportunity_one_or_more_in_the_list_for_programmed_irrigation = (true);
		}

		// ----------
		
		lftstation_ptr = nm_ListGetNext( &ft_irrigating_stations_list_hdr, lftstation_ptr );
		
	}  // of all stations in the irrigation list
	
	// ----------
	
	// 6/22/2018 rmd : Now, test each station group to see if it just FINISHED irrigation.
	ftimes_for_each_station_group_check_if_irrigation_just_completed();
}

/* ---------------------------------------------------------- */
static void ftimes_irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON( void )
{
	FT_STATION_STRUCT	*lftstation_ptr;

	FT_STATION_STRUCT	*ltmp_ftstation_ptr;

	BOOL_32	need_to_get_the_next_ilc;

	// ----------

	// ACTUAL HUNT FOR STATIONS TO TURN ON
	//	
	// If any stations on the xfer list let that empty first so system is settled. This covers stations that have
	// been marked to turn on as well as those marked to turn off. Remember it isn't actually considered on till
	// the token to the controller has been mailed. Turn off go a step further in that they require the response
	// to the token to turn off. If we remove the test for the xferlist.count to be empty then we must perform a
	// loop on the each of the stations in the foal_irri.StationsONByController[ i ] to learn the following:
	// est_total_flow, number_currently_on, one_on_from_the_problem_list, pump_should_be_on, 
	// StationsON_Pump, StationsON_NoPump, and one_on_to_set_expected.
	// We also have to add a statement such as "if ( FOAL_This_ilc_IsOnTheXferList( ilc ) == TRUE ) continue;"
	// to the tests in the decision to turn on. This is the way it is in 507_f.
	//
	// Now that we've decided to implement the "TURN OFF TO MAKE ROOM FOR" (turn off all lower priority stations)
	// rules we can say that we want the turn offs complete before we look to turn anything on. Turn offs decisions
	// made at the foal machine will show on the foalirri.xferlist so we check its count to know that things have
	// settled.

	// -------------------
	
	need_to_get_the_next_ilc = (false);

	lftstation_ptr = nm_ListGetFirst( &ft_irrigating_stations_list_hdr );

	while( (true) )
	{
		// 6/7/2012 rmd : If we turned OFF a valve or removed the ilc from the list the next list
		// item has already been obtained.
		if( need_to_get_the_next_ilc )
		{
			lftstation_ptr = nm_ListGetNext( &ft_irrigating_stations_list_hdr, lftstation_ptr );
		}

		need_to_get_the_next_ilc = (true);
		
		// 6/7/2012 rmd : This is how the logic works when we finally reach the end of the list.
		if( lftstation_ptr == NULL ) break;

		// ----------
		
		// 5/3/2018 rmd : Catch and set the CLASH before any chance to hit a 'continue' statement.
		if( lftstation_ptr->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
		{
			// 5/3/2018 rmd : Remember the test is against a GLOBAL assesment of if there is any MANUAL
			// PROGRAM station in the list. So if there is one, ALL station groups current with stations
			// in the list get this set. It only gets transferred to the real results is a station group
			// latest finish is exceeded.
			if( ufim_one_or_more_in_the_list_for_manual_program )
			{
				lftstation_ptr->station_group_ptr->results_manual_programs_and_programmed_irrigation_clashed_holding = (true);
			}
		}
		
		// ----------
		
		// 7/30/2018 rmd : This is NOT required to make finish times produce the correct answer,
		// however it helps to cleanup a 'dirty' condition where we proceed here and try and
		// calculate run time for this station but there is none left. This condition actually only
		// exists for MANUAL PROGRAMS, PI stations end up with a soak time when they expire and that
		// protects them for trying to calculate a run time again, we catch the soak time a few
		// lines below. MOSTLY THIS EXISTS BECAUSE - 1. turn OFF and removal is a 2 iterate process,
		// and 2. because we set the soak time to zero when a manual program station runs its time
		// down, and 3. because I want to keep in place the alert about an attempted ZERO run time.
		if( ftimes_station_is_eligible_to_be_removed_from_the_irrigating_list( lftstation_ptr ) )
		{
			// 7/30/2018 rmd : DON'T REMOVE IT HERE, just skip over trying to turn it ON.
			continue;
		}

		// ----------

		// 2011.11.08 rmd : Obviously if it's already ON skip this station.
		if( lftstation_ptr->bbf.station_is_ON == (true) ) continue;
		
		// 2011.11.08 rmd : If it's soaking skip this station.
		if( lftstation_ptr->soak_seconds_remaining_ul > 0 ) continue;
		
		// ----------
		
		// 2011.12.12 rmd : If the inhibit timer for the system this valve belongs to is still
		// counting down then discontinue the attempt to turn another ON.
		//
		// 4/5/2018 rmd : NOTE - for FTIMES we always adhere to the slow closing valve delay - no
		// qualification about not being in MOBILE or TEST needed.
		if( lftstation_ptr->system_ptr->inhibit_next_turn_ON_remaining_seconds > 0 ) continue;

		// ----------
		
		// 6/1/2012 rmd : When we are in the list for a reason that allows flow checking we must
		// take into account the mixing of flow check groups. They cannot mix else the flow check
		// could report false errors. Also, when in the list for test we allow flow groups to mix to
		// avoid confusion - when the user asks to TEST we turn it ON for them.
		if( lftstation_ptr->bbf.flow_check_when_possible_based_on_reason_in_list && (lftstation_ptr->bbf.w_reason_in_list != IN_LIST_FOR_TEST) )
		{
			if( FOAL_IRRI_there_are_other_flow_check_groups_already_ON( &lftstation_ptr->bbf, &lftstation_ptr->system_ptr->ufim_flow_check_group_count_of_ON[ 0 ] ) )
			{
				// skip over it
				continue;
			}
		}			
		
		// ----------
		
		// 4/5/2018 rmd : Always test against capacity during ftimes calculation.
		if( ftimes_will_exceed_system_capacity( lftstation_ptr ) ) continue;
		
		// ----------
		
		// When there is a mix of pump and non-pump valves ON don't add more to the mess - I think
		// this should never happen.
		if( lftstation_ptr->system_ptr->ufim_there_is_a_PUMP_mix_condition_b == (true) ) continue;
		
		// If this is a pump station
		if( lftstation_ptr->bbf.w_uses_the_pump == (true) )
		{
			// When working pumpers is TRUE it implies several things. It implies a station
			// is on that requires the pump. Or it implies that we were in non-pump mode and made
			// one pass through this turn-on algorithm and didn't turn any non-pumpers on so we switched
			// to look at pumpers.
			if ( lftstation_ptr->system_ptr->ufim_what_are_we_turning_on_b == TURNING_ON_NON_PUMP_VALVES ) continue;
			
			// This condition could exist if you had say 4 pumpers on and we added a non-pump test to
			// the list. The test would come on when the pumpers went off. Lets say they turned off
			// together. If there were more pumpers ready to turn on we would turn on the first one
			// before declaring a mix condition (the mix condition would prevent more from turning on).
			if ( (lftstation_ptr->system_ptr->system_master_number_of_valves_ON > 0) && (lftstation_ptr->system_ptr->ufim_stations_ON_with_the_pump_b == (false)) ) continue;
		}
		else
		{
			if ( lftstation_ptr->system_ptr->ufim_what_are_we_turning_on_b == TURNING_ON_PUMP_VALVES ) continue;
			
			// Same condition but in reverse for the pumpers above. A pump valve could be on (for TEST)
			// even though we are hunting for NON-PUMPERS.
			if ( lftstation_ptr->system_ptr->ufim_stations_ON_with_the_pump_b == (true) ) continue;
		}
		
		// ----------
		
		// ELECTRICAL LIMIT - all reasons in the list follow this.
		if( ft_stations_ON_by_controller[ lftstation_ptr->box_index_0 ] >= ft_electrical_limits[ lftstation_ptr->box_index_0 ] )
		{
			// for all reasons - test or not - skip over it - if its for test it won't be here next time
			continue;
		}
		
		// ----------
		
		// ON-AT-A-TIME rules
		if( !ftimes_the_on_at_a_time_rules_in_the_mainline_allow_this_valve_to_come_ON( lftstation_ptr ) )
		{
			continue;	
		}

		if( !ftimes_can_another_valve_come_ON_within_this_station_group( lftstation_ptr ) )
		{
			continue;	
		}
		
		// ----------

		// CAN'T HAVE A STATION ON FOR 2 REASONS. Check that the station we are about to turn on
		// isn't already on - for some other reason!! Don't want to run the station for two
		// different reasons at the same time - could happen if reasons are equal (OR TREATED AS
		// EQUALS!). Such as can happen with Manual program and Programmed Irrigation?
		BOOL_32 lthis_station_is_already_on_b;

		lthis_station_is_already_on_b = (false);
		
		// 5/9/2012 rmd : We are presently going through the main irrigation ilc list. Here we can
		// safely pass through the list of those ON. Looking to see if this station is already in
		// the list. If so we won't be attempting to turn him ON again.
		ltmp_ftstation_ptr = nm_ListGetFirst( &ft_stations_ON_list_hdr );
		
		while( ltmp_ftstation_ptr )
		{
			if( (ltmp_ftstation_ptr->box_index_0 == lftstation_ptr->box_index_0) && (ltmp_ftstation_ptr->station_number_0 == lftstation_ptr->station_number_0) )
			{
				lthis_station_is_already_on_b = (true);
			}
		
			ltmp_ftstation_ptr = nm_ListGetNext( &ft_stations_ON_list_hdr, ltmp_ftstation_ptr );
		}
		
		if( lthis_station_is_already_on_b == (true) ) continue;
		
		// ----------
		
		ftimes_set_remaining_seconds_on( lftstation_ptr );
		
		if( lftstation_ptr->remaining_seconds_ON > 0 )
		{
			// BEFORE we update the local_pump_is_on state and the number_currently_on we MUST check if
			// we are entering a mix condition. We need to do it before the update of those two as the
			// logic depends on it. If there is a already a mix, there still is a mix. If there are
			// stations on, then if the pump is off and we are adding a station that uses the pump we
			// have a mix. If the pump is on and we adding a station that doesn't use the pump we have a
			// mix.
			if( lftstation_ptr->system_ptr->system_master_number_of_valves_ON > 0 )
			{
				if ( lftstation_ptr->system_ptr->ufim_stations_ON_with_the_pump_b == (false) )
				{
					if ( lftstation_ptr->bbf.w_uses_the_pump )
					{
						// Here we are...the pump is off, there are stations on and we are adding a station that
						// uses the pump.
						lftstation_ptr->system_ptr->ufim_there_is_a_PUMP_mix_condition_b = (true);
					}
				}
				else
				{
					if ( !lftstation_ptr->bbf.w_uses_the_pump )
					{
						// Here we are...the pump is on, there are stations on and we are adding a station that
						// doesn't use the pump.
						lftstation_ptr->system_ptr->ufim_there_is_a_PUMP_mix_condition_b = (true);
					}
				}
			}

			// ----------
			
			ftimes_complete_the_turn_ON( lftstation_ptr );

			// ----------
			
			// And KEEP THE EXPECTED FLOW RATE up to date of course.
			lftstation_ptr->system_ptr->ufim_expected_flow_rate_for_those_ON += lftstation_ptr->expected_flow_rate_gpm_u16;

			// update our FLOW TEST group count
			lftstation_ptr->system_ptr->ufim_flow_check_group_count_of_ON[ lftstation_ptr->bbf.flow_check_group ] += 1;

			// 6/18/2012 rmd : And of course the number ON in the system. Set this before we merge in
			// the allowed ON into the limit. So the test made in that function is accurate.
			lftstation_ptr->system_ptr->system_master_number_of_valves_ON += 1;
			
			// ----------
			
			// Update the system limit of how many are allowed ON.
			ftimes_merge_new_mainline_allowed_ON_into_the_limit( lftstation_ptr );
				
			// Keep the STATION GROUP on_at_a_time ON count up to date.
			ftimes_bump_this_station_groups_on_at_a_time_ON_count( lftstation_ptr );

			// ----------
			
			// And keep the pump_on indicator up to date too.
			if( lftstation_ptr->bbf.w_uses_the_pump )
			{
				lftstation_ptr->system_ptr->ufim_stations_ON_with_the_pump_b = (true);
			}
			else
			{
				lftstation_ptr->system_ptr->ufim_stations_ON_without_the_pump_b = (true);
			}
		}
		else
		{
			// 4/19/2018 rmd : During ftimes the runtime should never come back ZERO. Alert for this so
			// if it happens we can look into it.
			Alert_Message( "FTIMES: run time 0" );
		}  // of the time came back as 0!
		
	}  // of for loop looking through the list for one to turn on
}

/* ---------------------------------------------------------- */
void FTIMES_FUNCS_maintain_irrigation_list( void )
{
	// 6/22/2018 rmd : Executes ONLY within the ontext of the FINISH TIMES TASK.
	
	// ----------
	
	// 4/26/2018 rmd : For ftimes there are no mutexes to take and no flow recording
	// considerations.

	// ----------
	
	ftimes_decrement_system_timers();
	
	// ----------
	
	ftimes_init_ufim_variables_for_all_systems();
	
	ftimes_init_on_at_a_time_and_completion_variables_in_all_station_groups();
	
	// 4/26/2018 rmd : And we completely step around MOISTURE SENSING during ftimes. Not used.
		
	// ----------
		
	ftimes_irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations();

	// ----------

	ftimes_irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars();
	
	ftimes_irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON();
	
	// ----------
	
	ftimes_irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON();
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

