/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_FTIMES_FUNCS_H
#define _INC_FTIMES_FUNCS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"gpio_setup.h"

#include	"lpc_types.h"

#include	"ftimes_vars.h"

#include	"station_groups.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern BOOL_32 FTIMES_FUNCS_load_run_time_calculation_support( FT_STATION_GROUP_STRUCT *pft_station_group_ptr, RUN_TIME_CALC_SUPPORT_STRUCT *psupport_struct_ptr );

extern FT_STATION_GROUP_STRUCT *FTIMES_FUNCS_find_station_group_in_ftimes_list_with_this_GID( UNS_32 const psg_gid );

extern FT_SYSTEM_GROUP_STRUCT *FTIMES_FUNCS_find_system_group_in_ftimes_list_with_this_GID( UNS_32 const psg_gid );


extern void FTIMES_FUNCS_check_for_a_start( void );

extern void FTIMES_FUNCS_maintain_irrigation_list( void );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

