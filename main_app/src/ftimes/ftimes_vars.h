/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_FTIMES_VARS_H
#define _INC_FTIMES_VARS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"gpio_setup.h"

#include	"lpc_types.h"

#include	"foal_defs.h"

#include	"cal_td_utils.h"

#include	"cal_list.h"

#include	"manual_programs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/14/2018 rmd : Defines used to control function behavior, to process for the ftimes
// calculation or for regular irrigation. These must remain as (true) (false) defines.

#define	FOR_FTIMES			(true)

#define	NOT_FOR_FTIMES		(false)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/1/2018 rmd : Needed an array for ftimes to track the number of valves ON by box, used
// for electrical limit checking. It isn't really part of anything else, kind of like the
// ft_time_and_date, so here she sits.
extern UNS_32	ft_stations_ON_by_controller[ MAX_CHAIN_LENGTH ];

extern UNS_32	ft_electrical_limits[ MAX_CHAIN_LENGTH ];

// 5/3/2018 rmd : Cleared each second. Used to detect the clash between PROGRAMMED
// IRRIGATION and a MANUAL PROGRAM.
extern BOOL_32	ufim_one_or_more_in_the_list_for_manual_program;




// 4/1/2018 rmd : HOW IS THIS USED IN FTIMES ??? - to massage the historical used to
// calculate run times - at least thats the plan.
//extern float	ft_et_ratio;   !!!!!!!!!!




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/12/2018 rmd : A list of the irrigating stations. They don't have to be ON, but are
// either ON, SOAKING, or waiting to irrigate.
extern MIST_LIST_HDR_TYPE			ft_irrigating_stations_list_hdr;

// 2/22/2018 rmd : A list of the stations actually ON. To support the maintain function.
extern MIST_LIST_HDR_TYPE			ft_stations_ON_list_hdr;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/8/2018 rmd : A station group structure, minimized to only what is needed to support
// start time checks and the running of irrigation.
typedef struct
{
	MIST_DLINK_TYPE		list_support_station_groups;

	UNS_32				group_identity_number;
	

	UNS_32				precip_rate_in_100000u;

	UNS_32				crop_coefficient_100u[ MONTHS_IN_A_YEAR ];

	// ----------
	
	// The priority level - Low (1), Medium (2), or High (3)
	UNS_32			priority_level;

	// ----------
	
	// The percentage to increase or decrease the station's run times by
	UNS_32			percent_adjust_100u;

	// The date to start the percent adjust
	UNS_32			percent_adjust_start_date;

	// The date to end the percent adjust
	UNS_32			percent_adjust_end_date;

	// ----------
	
	// TODO Add enabled flag to UI
	BOOL_32			schedule_enabled_bool;

	UNS_32			schedule_type;

	UNS_32			start_time;

	UNS_32			stop_time;

	// 7/2/2018 rmd : This is used as a reference point for the once every X days schedule
	// types. The name of this variable says it all - this was a previous date(day) that we
	// irrigated on, in the past. We are very particular about it being in the past to support
	// the math performed with this variable, when figuring if a particular date after this date
	// is an irrigation date(day).
	UNS_32			a_scheduled_irrigation_date_in_the_past;

	BOOL_32			water_days_bool[ DAYS_IN_A_WEEK ];

	// 4/7/2016 rmd : This is IMPROPERLY named. What it should say is
	// irrigates_after_29th_or_31st. And applies to only an ODD day irrigation schedule when we
	// are on the FIRST of the month when the prior month ended with 29 or 31 days. This
	// variable if (true) signifies we should irrigate in that case on the FIRST even after
	// irrigating the day before.
	BOOL_32			irrigate_on_29th_or_31st_bool;

	UNS_32			mow_day;

	BOOL_32			et_in_use;

	// 11/12/2015 ajv : Muddy Mondays - smoothed out the skip day effect in DailyET such that
	// we can now irrigate the same amount each day even following skip days. We take the
	// skip day water and spread it out evenly across all the irrigation days in the schedule.
	// This "ET AVERAGING" as we should probably call it can be enabled or disabled by program
	// in set-up. After your ROM update it will be enabled and controllers will ship with this
	// enabled when you first turn on daily ET.
	//
	// 4/8/2016 rmd : NOTE - ONLY affects the irrigated amount when irrigating using a weekly
	// schedule.
	BOOL_32			use_et_averaging_bool;

	// ----------

	// The number of stations allowed to turn on a a time within this group
	UNS_32			on_at_a_time__allowed_ON_in_station_group__user_setting;

	// The number of stations allowed to turn on a a time within the system.
	// Note that this may cross multiple systems.
	UNS_32			on_at_a_time__allowed_ON_in_mainline__user_setting;

	// 2011.11.02 rmd : The dynamically maintained count of stations ON in this
	// group. Once per second (on average) the foal irrigation maintenance
	// function zeros and then updates this number. And uses it to DECIDE if
	// another valve from this group can turn ON.
	//
	// The system count is extracted by looking at the system count for each
	// station ON (that allowed value is part of this group but we could have
	// valve ON across several groups). And 'remembering' the lowest allowed
	// within system value (by sytem .. and therefore it is stored within the
	// system (file) list similarly to how this one is stored.
	//
	// We do not care about saving these numbers to the actual file system. It's
	// okay if it happens. Gee would we ever get into a position where we'll
	// read out a file in the midst of irrigation. I think not. But beware!
	UNS_32			on_at_a_time__presently_ON_in_station_group_count;

	// ----------
	
	BOOL_32			pump_in_use;
	
	// ----------
	
	UNS_32			line_fill_time_sec;

	// 4/4/2018 rmd : The slow closing valve setting.
	UNS_32			delay_between_valve_time_sec;

	// ----------
	
	UNS_32			high_flow_action;

	UNS_32			low_flow_action;

	// ----------
	
	// 9/22/2015 ajv : From this point forth, station groups are assigned to mainlines rather
	// than individual stations. This resolves a number of issues related to on-at-a-time, flow
	// checking, and budgets which were caused by a station group consisting of stations
	// spanning multiple mainlines.
	UNS_32			GID_irrigation_system;
	
	// ----------

	// 6/22/2018 rmd : To track when the Programmed Irrigation ends for this station group.
	BOOL_32			at_start_of_second_one_or_more_in_the_list_for_programmed_irrigation;

	BOOL_32			after_removal_opportunity_one_or_more_in_the_list_for_programmed_irrigation;
	
	// ----------
	
	// 5/2/2018 rmd : The finish times results are collected by station group. Listed below are
	// the things I wanted to collect.

	// 5/2/2018 rmd : This is stamped at the start time and transferred to the start date and
	// time associated with the latest finish time when that is set. I think the best use of
	// this is when the irrigation hits the START TIME (meaning the irrigation never finishes),
	// we can say which start time is the offender.
	DATE_TIME		results_start_date_and_time_holding;

	// 5/2/2018 rmd : Tranferred to hit_the_stop_time results only when the latest finish time
	// is updated.
	BOOL_32			results_hit_the_stop_time_holding;
	
	// 5/2/2018 rmd : By how many seconds do we exceed the stop time.
	UNS_32			results_exceeded_stop_time_by_seconds_holding;

	// 5/3/2018 rmd : For this second, if needed, we only want to add to the tracking variable
	// one time.
	// 07/17/2018 dmd: This should be per station group, since it is tracking if stop time is
	// exceeded for all the stations in the group.
	BOOL_32				for_this_SECOND_add_to_exceeded_stop_time_by_seconds_holding;
	
	// 5/3/2018 rmd : Does this STATION GROUP have station running when a manual program starts,
	// any manual program that is, containing stations from ANY station group.
	BOOL_32			results_manual_programs_and_programmed_irrigation_clashed_holding;
	
	// ----------
	
	// 7/9/2018 rmd : Counts the seconds it takes for the irrigation to complete. Compared
	// against the saved non-holding value and the longer one wins, and indicates the latest
	// finish time.
	UNS_32			results_elapsed_irrigation_time_holding;

	UNS_32			results_elapsed_irrigation_time;

	// ----------
	
	// 5/2/2018 rmd : Programmed Irrigation start time and date for the worst case finish. For
	// the display if needed.
	DATE_TIME		results_start_date_and_time_associated_with_the_latest_finish_date_and_time;
	
	// 5/2/2018 rmd : For the display.
	DATE_TIME		results_latest_finish_date_and_time;

	DATE_TIME		results_latest_finish_date_and_time_holding;
	
	// 5/2/2018 rmd : A flag for the display logic to add a note about running past the STOP
	// time on the worst case day.
	BOOL_32			results_hit_the_stop_time;
	
	// 5/2/2018 rmd : Ran past the STOP time this many seconds on the worst case day. For the
	// display if needed.
	UNS_32			results_exceeded_stop_time_by_seconds;

	// 5/2/2018 rmd : On the worst case day, a manual program was still irrigating or started
	// while stations from this station group were irrigating. For the display if needed.
	BOOL_32			results_manual_programs_and_programmed_irrigation_clashed;

} FT_STATION_GROUP_STRUCT;


extern MIST_LIST_HDR_TYPE	ft_station_groups_list_hdr;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	MIST_DLINK_TYPE		list_support_manual_program;

	UNS_32				group_identity_number;
	

	UNS_32				start_times[ MANUAL_PROGRAMS_NUMBER_OF_START_TIMES ];

	BOOL_32				days[ DAYS_IN_A_WEEK ];
	
	UNS_32				start_date;

	UNS_32				stop_date;

	UNS_32				run_time_seconds;

} FT_MANUAL_PROGRAM_STRUCT;

// ----------

extern MIST_LIST_HDR_TYPE		ft_manual_programs_list_hdr;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 3/31/2018 rmd : NOTE : all in all this structure is a combination of the systems file
	// list and the system preserves variables. Within the finish times implementation we do not
	// have both system peserves and a systems list. Just one list of systems.
	
	// ----------
	
	MIST_DLINK_TYPE		list_support_systems;

	// A value of 0 means it ain't in use. We look for a matching gid or find an struct not in
	// use and take it. When the roll time hits we close the report record and release the
	// structure by setting the system_gid to 0. We we try to add to a record or set a value if
	// we do not find a matching gid we make such a record and zero out the entries (picture
	// trying to add to an accumulator and we cannot find the system gid match...so we take the
	// next available and 0 the record then add to the accumulator.
	UNS_32		system_gid;
	
	// ----------
	
	// 4/5/2018 rmd : Used during irrigation turn ON decisions.
	BOOL_32		capacity_in_use_bool;

	UNS_32		capacity_with_pump_gpm;

	UNS_32		capacity_without_pump_gpm;

	// ----------
	
	// 2012.01.24 rmd : ufim = USED_FOR_IRRIGATION_MANAGEMENT.

	// This is used when deciding to switch from turnon pump to non-pump.
	UNS_32		ufim_valves_in_the_list_for_this_system;

	// ----------
	
	// 4/19/2018 rmd : This on_at_a_time allowed on within the mainline value is learned each
	// time the irrigation maintenance function runs. And is used during each valve turn ON
	// decision, and updated if the valve is turned ON. The value is learned by the on at a time
	// in the mainline user setting stored within the station group.
	UNS_32		ufim_on_at_a_time__presently_allowed_ON_in_mainline__learned;

	// ----------
	
	// 6/15/2012 rmd : Broadly used by several tasks. Not set to 0 on each call to the foal irri
	// list maintenance function. Though use requires possesion of the system_preserves
	// MUTEX.
	UNS_32		system_master_number_of_valves_ON;

	// ------------------

	// ET2000e note : This was introduced to handle the unique situation of when there are
	// non-pump valves irrigating with both pump and non-pump in the list. If we reached the
	// point where all valves turned off and both pump and no-pump are eligible to come on we
	// would want to continue with the non-pumpers for continuity sakes. We use this variable to
	// override the pump priority in the list for ONE pass through the turn on loop. Without
	// this variable we would switch to pumpers even though we were just doing non-pumpers. This
	// may not be a problem but doesn't seem like the most expected behavior one would expect.
	//
	// ET2000e note : This variable protects us from jumping to pump valves when we are
	// irrigating non-pump valves just cause the pumper are in the list and ready and the valves
	// on dropped to zero. It may have dropped to zero for just the brief instant between valve
	// transitions. This allows us to continue with non-pumpers. THE FLIP SIDE OF THIS is if we
	// are irrigating pumpers and the on count drops to zero. If there are any pumpers in the
	// list we will continue with them due to the priority of the list organization - pumpers
	// are looked at first.
	//
	// ET2000e note : I'm actually not sure why we need this variable anymore...the list
	// organization should cover the pump non-pump segregation???? but i'm chicken to remove it
	//
	// 2011.11.09 rmd : Note this variable tracking pump or non-pump turn ON mode is meant to be
	// preserved from call to call of the foal irrigation maintenance function. Be careful not
	// to reset it at the report roll time. We DO NOT want to reset this variable during that
	// activity.
	BOOL_32		ufim_what_are_we_turning_on_b;

	// --------------------------

	// 5/30/2012 rmd : This value is computed under protection of the system_preserves MUTEX and
	// is available for the COMM_MNGR to distribute via the token if desired. It is equal to the
	// sum of the expecteds of those valves ON in the system (flow meter or no flow meter within
	// the system). When 'tallys_driven_with_expecteds' is true then this is the value to
	// display as the current flow rate. The current flow rate is the sum of the expecteds for
	// those ON.
	UNS_32		ufim_expected_flow_rate_for_those_ON;

	// --------------------------

	// 5/30/2012 rmd : This variable is used during the turn ON decision and the also during
	// flow checking. It's accuracy can lag certain actions such as the STOP key or a mainline
	// break. And that may be a concern during flow checking. But I think not as all the valves
	// are OFF for those events. For its use during flow checking see the notes that follow:

	// <notes regarding use during flow checking> Here is a scenario that occurs with the new
	// flow table learning. Two valves learn all the way so they are checking. If they are on
	// together and we get a flow error both go on the problem list - the problem comes about
	// when the valve that we just saw a possible problem with and is now on the problem list
	// turns back on it may be joined with other valves that cause them to use a cell not yet
	// learned! and therefore the group will not check flow. We have decided that when one of
	// the valves ON is from the problem list that causes us to disregard the learning
	// requirement of the cell in order to test flow. We can assume the valve has run enough
	// cycles as it was involved in a flow test to begin with.
	//
	// This was argued as no worse off than we were before the flow table learning was
	// introduced and we ran that way for two years and did not have any problems.
	//
	// The question comes in that if the flow passes should we then learn the derate table if it
	// hasn't learned its iterations - probably so. We are trying to characterize the system in
	// the derate table and any opportunities we have to record more about the flow behaviour we
	// should. One concern is that it didn't fail cause it wasn't learned and now we are
	// learning under broken system conditions. Again not worse than we did it without flow
	// table learning.
	BOOL_32		ufim_one_ON_from_the_problem_list_b;

	// --------------------------

	UNS_32		ufim_highest_priority_pump_waiting;

	UNS_32		ufim_highest_priority_non_pump_waiting;

	// ----------
	
	// 4/1/2016 rmd : NOTE - this flag is NOT USED in the CS3000 code. The is no positive HO in
	// the CS3000 product like there is in the 2000e. The 2000e comment follows below.
	//
	// In order to complete regular irrigation before hold-over is started we need to KNOW if
	// there are any with programmed time that are ready to go - if so we must skip over
	// stations with only HO to irrigate.
	BOOL_32		ufim_list_contains_waiting_programmed_irrigation_b;

	// ----------
	
	BOOL_32		ufim_list_contains_waiting_pump_valves_b;

	BOOL_32		ufim_list_contains_waiting_non_pump_valves_b;

	// --------------------------

	//UNS_32		ufim_highest_reason_of_OFF_valve_to_set_expected;

	//BOOL_32		ufim_one_ON_to_set_expected_b;

	BOOL_32		ufim_list_contains_some_to_setex_that_are_not_ON_b;

	// 608.rmd to support the new rre valve mode where we want to have only a single valve ON in
	// the whole system - including no other rre valves - the result when one of these types
	// hits the list is to pause everything
	//BOOL_32		ufim_one_RRE_ON_to_set_expected_b;

	//BOOL_32		ufim_list_contains_some_RRE_to_setex_that_are_not_ON_b;


	BOOL_32		ufim_list_contains_some_pump_to_setex_that_are_not_ON_b;

	BOOL_32		ufim_list_contains_some_non_pump_to_setex_that_are_not_ON_b;


	// 2012.01.05 rmd : Old 2000e note. Not sure if it is even applicable. As I would think the
	// Walk-Thru valve would SMSThis can happen if we're doing say a SMS with non-pump valves
	// and someone walks up to the controller and pushes TEST for a PUMP valve. The TEST will
	// come on and the pump will be running. When the current SMS valves expire only PUMP SMS
	// valves can come on (can replace SMS with ProgrammedIrrigation in this story). This
	// variable is kept up to date in the maintain irri list function of the FOAL machine.
	BOOL_32		ufim_there_is_a_PUMP_mix_condition_b;


	// 4/4/2018 rmd : Used during the sanity check logic about if we have valves on from more
	// than one flow group at a time.
	BOOL_32		ufim_based_on_reason_in_list_at_least_one_valve_is_ON_that_wants_to_check_flow;


	// 5/30/2012 rmd : Used to decide if we should throw the "mix of flow groups on" error
	// message. Also used during flow checking. If more than ONE valve ON for test we do not
	// test flow. Not sure why. But the 2000e works this way. This variable is initialized to
	// zero when irrigation restarts (program startup, scan, etc..) and dynamically maintained
	// as turn ON and OFF events occur.
	UNS_32		ufim_number_ON_during_test;

	// ------------------

	// 8/8/2012 rmd : When (true) means that the pump output for all the POCs attached to this
	// system should be energized. This information is communicated to all the IRRI machines.
	// Which in-turn use it to inform the POCs attached to this system that are owned by their
	// respective box. When referring to this value one must possess the system_preserves MUTEX.
	// As the foal maintenance function manipulates the value in such a way there are moments of
	// time when it is invalid!
	BOOL_32   	ufim_stations_ON_with_the_pump_b;

	BOOL_32		ufim_stations_ON_without_the_pump_b;

	// ------------------

	UNS_32		ufim_highest_reason_in_list_available_to_turn_ON;

	UNS_32		ufim_highest_pump_reason_in_list_available_to_turn_ON;

	UNS_32		ufim_highest_non_pump_reason_in_list_available_to_turn_ON;


	// Now for the variable that controls which segregated group we are turning ON - segregated
	// by if we are actively testing flow for the valves ON...this grouping is controlled by the
	// ALERT ACTIONS and the exclusion bit setting and if the reason in the list dictates we
	// will not measure flow.
	UNS_32		ufim_flow_check_group_count_of_ON[ 4 ];

	// ----------
	
	// 2/20/2013 rmd : This timer is used to help slow closing valves close. By blocking the
	// turn ON of the next valve until this timer counts down to zero. The default is 0 but the
	// user can set. When any valve closes it is reloaded. And literally blocks subsequent
	// valves from energizing till counts down to zero.
	UNS_32		inhibit_next_turn_ON_remaining_seconds;

	// ----------
	
	// 2012.01.31 rmd : And here we have a bunch of vitals that are used during the system
	// operation AND are also desirable to send to each slave in the network. We've wrapped them
	// up into a bit field for tranfer efficiency.
	SYSTEM_BIT_FIELD_STRUCT		sbf;

} FT_SYSTEM_GROUP_STRUCT;


extern MIST_LIST_HDR_TYPE	ft_system_groups_list_hdr;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	union
	{
		UNS_32	whole_thing;

		struct
		{
			BITFIELD_BOOL		slot_loaded											: 1;
			
			// ----------

			// 3/14/2018 rmd : When PI and MP irrigations overlap, we need to add a second instance of
			// the station into the array. So the station can be in the irrigation list twice, once for
			// each reason. When the irrigation associated with the duplicate finishes we remove the
			// duplicate from the array.
			BITFIELD_BOOL		is_a_duplicate_in_the_array							: 1;
			
			// ----------
			
			// 2/26/2018 rmd : During regular non-ftimes irrigation this is tracked via a variable
			// within the station preserves, but we don't need a station preserves for ftimes, BECAUSE
			// the regular array of stations we use in ftimes is persistent (like the station preserves)
			// as far as the calculation is concerned. BUT we do need an interm copy. That's the way the
			// logic works. This term is only used during PI, set (true) when station is queued to
			// irrigate, and when the station actually turns ON we set this (false).
			BITFIELD_BOOL		did_not_irrigate_last_time_holding_copy		: 1;
			
			// ----------

			// 07/13/2018 dmd: During regular irrigation a station done with its run cycle is removed
			// and re-inserted in the irrigation list. This changes the order of stations i nthe
			// irrigation list. In case of non-ftimes irrigation, it is okay since it cases may an 1
			// second difference in soak time following the run cycle. Not a visible difference. But in
			// case of finish times the time difference is almost noticeable, can cause errorneous time
			// value calculations. In order to keep track if a station has done with irrigations, we
			// will use a tracking flag. THis will be cleared and will be set when it is done with its
			// cycle.
			BITFIELD_BOOL		done_with_runtime_cycle		: 1;

			// 3/8/2018 rmd : Using 4 bits out of the 32 allocated.

		};

	};

} FTIMES_STATION_BIT_FIELD_STRUCT;


typedef struct
{
	// 3/31/2018 rmd : NOTE : all in all this structure is a combination of the stations list
	// and the station preserves variables. Within the finish times implementation we do not
	// have both station preserves and a stations list. Just one array of stations.
	
	// ----------
	
	// 2/12/2018 rmd : Signifies the station exists and is occupying a slot in our ftimes array
	// of stations, meaning it is in the network, is physically available and in use, and is
	// tied to a station group or manual program.
	FTIMES_STATION_BIT_FIELD_STRUCT		ftimes_support;
	
	MIST_DLINK_TYPE		list_of_irrigating_stations;
	
	MIST_DLINK_TYPE		list_of_stations_ON;
	
	// ----------
	
	FT_SYSTEM_GROUP_STRUCT		*system_ptr;
	
	FT_STATION_GROUP_STRUCT		*station_group_ptr;
	
	// ----------

	// 3/31/2018 rmd : Reflecting the two MP's a station can belong to.
	FT_MANUAL_PROGRAM_STRUCT	*manual_program_ptrs[ 2 ];

	// ----------
	
	UNS_32				station_number_0;

	UNS_32				box_index_0;
	
	// ----------
	
	// 2/14/2018 rmd : IRRIGATION TIME RELATED.
	
	// 2/14/2018 rmd : The user programmed amount. This amount is transferred to the irrigation
	// seconds balance at a start time.
	UNS_32				user_programmed_seconds;

	// 4/4/2018 rmd : This is the minimum watersense 3-minute cycle time that is pushed forward
	// to the next irrigation.
	UNS_32				water_sense_deferred_seconds;

	// 2/14/2018 rmd : The amount left to irrigate. Starts with the user programmed amount
	// at a SX. Then ratchets down each cycle irrigated.
	UNS_32				requested_irrigation_seconds_balance_ul;

	// 2/14/2018 rmd : The seconds remaining in an irrigation CYCLE, only active when the
	// station is ON.
	INT_32				remaining_seconds_ON;

	// ----------
	
	// 2/14/2018 rmd : The programmed, user set, cycle amount.
	UNS_32				cycle_seconds_original_from_station_info;

	// 7/30/2018 rmd : Needed because MP has it's own unique cycle time. During MP the cycle
	// time is the run time associated with a MP start. And we set this variable accordingly at
	// a detected PI or MP start time.
	UNS_32				cycle_seconds_used_during_irrigation;

	// ----------
	
	// 2/14/2018 rmd : The programmed, user set, soak time.
	UNS_32				soak_seconds_original_from_station_info;

	// 3/15/2018 rmd : Needed because when irrigating by PI we load with the original from
	// station info, when irrigating by MP we set to 0. MIGHT be eliminated later, we'll see.
	UNS_32				soak_seconds_used_during_irrigation;

	// 2/14/2018 rmd : Active when a station is actually soaking.
	UNS_32				soak_seconds_remaining_ul;

	// ----------
	
	UNS_32				et_factor_100u;

	UNS_32				distribution_uniformity_100u;

	// 3/14/2018 rmd : I think FTIMES has no use for the moisture_balance_percent. It is only
	// used to control irrigation after there has been rain, and in ftimes of course there is no
	// rain.

	// ----------
	
	// 2/14/2018 rmd : At a START TIME these are calculated and set, well the date anyway, the
	// time is known. The date would reflect the current ftimes date.
	UNS_32				stop_datetime_t;

	UNS_16				stop_datetime_d;

	// ----------
	
	// 2/12/2018 rmd : Needed when irrigating by capacity.
	UNS_16				expected_flow_rate_gpm_u16;

	// ----------

	BIG_BIT_FIELD_FOR_ILC_STRUCT	bbf;

} FT_STATION_STRUCT;

// 2/26/2018 rmd : There is an interesting thing that happens when the station count gets
// towards the maximum of 768. As a station can be irrigating BOTH manual programs and
// programmed irrigation simultaneously, meaning it is in the list for both reasons, we have
// to introduce a second copy of a station when this occurs. Because of this we could run
// out of stations available in the array. But man, that would be an extreme. One thing we
// HAVE to make sure of, is once in the irrigation list, to only check for starts for the
// reason it is in the list. Otherwise we would get some doubled up strange results, as we
// are using the station list as the irrigation list. (THIS IS A COMPLICATED THOUGHT!!!)
#define FTIMES_MAX_NUMBER_OF_STATIONS	(MAX_STATIONS_PER_NETWORK)

extern FT_STATION_STRUCT	ft_stations[ FTIMES_MAX_NUMBER_OF_STATIONS ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


FT_MANUAL_PROGRAM_STRUCT *FTIMES_VARS_return_ptr_to_manual_program_group_in_ftimes_list_with_this_GID( UNS_32 const pgid );

extern void FTIMES_VARS_clean_and_reload_all_ftimes_data_structures( void );



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

