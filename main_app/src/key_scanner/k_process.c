/*  file = process_key.c                       04.21.2011 rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	<string.h>

#include	"k_process.h"

#include	"key_scanner.h"

#include	"speaker.h"

#include	"app_startup.h"

#include	"screen_utils.h"

#include	"scrollbox.h"

#include	"irri_irri.h"

#include	"irri_comm.h"

#include	"e_status.h"

#include	"flash_storage.h"

#include	"m_main.h"

#include	"dialog.h"

#include	"code_distribution_task.h"

#include	"d_copy_dialog.h"

#include	"lpc3250_chip.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"contrast_and_speaker_vol.h"

#include	"radio_test.h"

#include	"d_comm_options.h"

#include	"d_live_screens.h"

#include	"d_tech_support.h"

#include	"d_two_wire_assignment.h"

#include	"d_two_wire_debug.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#if SHOW_STUCK_KEY_ALERTS_AT_MIDNIGHT

	// 9/10/2018 rmd : Not a constant. Is an initialized variable so we can record the repeat
	// count when it is >REPEAT_COUNT_THRESHOLD.
	STUCK_KEY_TRACKING_STRUCT stuck_key_data[ NUMBER_OF_KEYS ] =
	{
		{	KEY_C_UP, 		0,	"UP KEY"		},
		{	KEY_C_DOWN, 	0,	"DOWN KEY"		},
		{	KEY_C_LEFT, 	0,	"LEFT KEY"		},
		{	KEY_C_RIGHT, 	0,	"RIGHT KEY"		},
		
		{	KEY_SELECT, 	0,	"SELECT KEY"	},

		{	KEY_NEXT, 		0,	"NEXT KEY"		},
		{	KEY_PREV, 		0,	"PREV KEY"		},
		
		{	KEY_PLUS, 		0,	"PLUS KEY"		},
		{	KEY_MINUS, 		0,	"MINUS KEY"		},

		{	KEY_STOP, 		0,	"STOP KEY"		},
		{	KEY_HELP, 		0,	"HELP KEY"		},
		{	KEY_ENG_SPA, 	0,	"ENG_SPA KEY"	},
		{	KEY_BACK, 		0,	"BACK KEY"		}
	};
		
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/**
 * Processes the BACK key to return to the previous screen. If there are no more
 * screens in the queue, a bad key beep is played.
 * 
 * @executed Executed within the context of the key processing task when the
 * user presses the BACK key.
 *
 * @author rmd
 *
 * @revisions
 *    2/9/2012 Removed legacy code that is no longer relevant for the CS3000.
 */
static void KEY_process_BACK( void )
{
	if( screen_history_index > 0 )
	{
		good_key_beep();

		SCROLL_BOX_close_all_scroll_boxes();

		KEY_PROCESS_free_memory_allocated_for_screen_before_changing_screen();		

		// ----------

		--screen_history_index;

		if( screen_history_index > 0 )
		{
			GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._08_screen_to_draw;
		}

		// post to the queue but wait no time if the queue is full... it should
		// never be full... if so that means the queue is too small or we have
		// some other more serious problem
		Display_Post_Command( &ScreenHistory[ screen_history_index ] );
	}
	else
	{
		bad_key_beep();
	}
}

extern void KEY_process_BACK_from_editing_screen( void *preturn_to_menu_func_ptr )
{
	DISPLAY_EVENT_STRUCT	lde;

	good_key_beep();

	lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
	lde._04_func_ptr = preturn_to_menu_func_ptr;
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
/**
 * Processes the English/Spanish key to set the appropriate language.
 * 
 * @executed Executed within the context of the key processing task when the
 * user presses the English/Spanish key.
 *
 * @author 2/9/2012 Adrianusv
 *
 * @revisions
 *    2/9/2012 Initial release
 */
extern void KEY_process_ENG_SPA( void )
{
	DISPLAY_EVENT_STRUCT	lde;

	// 8/20/2018 Ryan : If any dialog box is open, do not translate, as this causes the dialog
	// box to close when the translation occurs.
	if( ( g_COPY_DIALOG_visible == (false) ) && ( DIALOG_a_dialog_is_visible() == (false) ) && ( g_COMM_OPTIONS_dialog_visible == (false) ) && ( GuiVar_DeviceExchangeSyncingRadios == (false) ) && 
		( g_LIVE_SCREENS_dialog_visible == (false) ) && ( g_TECH_SUPPORT_dialog_visible == (false) ) && ( g_TWO_WIRE_dialog_visible == (false) ) && ( g_TWO_WIRE_DEBUG_dialog_visible == (false) ) )
	{
		good_key_beep();
	
		if ( GuiLib_LanguageIndex == GuiConst_LANGUAGE_ENGLISH )
		{
			GuiLib_SetLanguage( GuiConst_LANGUAGE_SPANISH );
		}
		else
		{
			GuiLib_SetLanguage( GuiConst_LANGUAGE_ENGLISH );
		}
		
		// ----------
	
		// 8/28/2012 rmd : Capture and save the latest setting. Use the delayed file save mechanism
		// to allow user to 'play' without triggering umpteen file saves.
		CONTRAST_AND_SPEAKER_VOL_set_screen_language( GuiLib_LanguageIndex );
		
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_CONTRAST_AND_VOLUME, 10 );
	
		// ----------
	
		// Retain the display event parameters so the screen is redrawn
		// exactly how it is, besides the language change
		lde._01_command = ScreenHistory[ screen_history_index ]._01_command;
		lde._02_menu = ScreenHistory[ screen_history_index ]._02_menu;
		lde._03_structure_to_draw = ScreenHistory[ screen_history_index ]._03_structure_to_draw;
		lde._04_func_ptr = ScreenHistory[ screen_history_index ]._04_func_ptr;
		lde._07_u32_argument2 = ScreenHistory[ screen_history_index ]._07_u32_argument2;
		lde.key_process_func_ptr = ScreenHistory[ screen_history_index ].key_process_func_ptr;
		lde.populate_scroll_box_func_ptr = ScreenHistory[ screen_history_index ].populate_scroll_box_func_ptr;
	
		// Don't force the redraw - this retains the current cursor position
		lde._06_u32_argument1 = (false);
	
		Display_Post_Command( &lde );
	}
	else
	{
		bad_key_beep();
	}
}
	
/* ---------------------------------------------------------- */
/**
 * Handles key processing that is not unique to a particular screen such as
 * BACK, MAIN MENU, and ENGLISH/SPANISH.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author rmd
 *
 * @revisions
 *    
 */
extern void KEY_process_global_keys( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		case KEY_BACK:
			KEY_process_BACK();
			break;

		case KEY_ENG_SPA:
			KEY_process_ENG_SPA();

			// 9/6/2012 rmd : We accept that the displayed screen may contain an ENGLISH or SPANISH time
			// and date string at the bottom. And that string may not be converted to the new language
			// for up to one second from now. That is acceptable. We could obtain the present time and
			// date here and then post the display command to display it but to conserve code space I
			// will not. It's not that big of a deal!
			break;
			
		case KEY_STOP:
			// 2012.02.14 rmd : We are going to read the list count. It is an atomic read so we don't
			// need to take the list MUTEX. If there are any stations in the list set the flag.
			if( irri_irri.list_of_irri_all_irrigation.count > 0 )
			{
				good_key_beep();

				// ----------

				IRRI_COMM_process_stop_command( (false) );
				
				// ----------
				
				// 11/7/2014 rmd : This variable may be a bit too powerful. It is not cleared until a token
				// arrives holding a stop key record. If that event never occurs the keypad is hopelessly
				// locked up. It is necessary for the user to cycle the power to get out of it. Perhaps a
				// timer? Or a count of tokens received. The stop key record should arrive the VERY NEXT
				// TOKEN AFTER the stop record we are building here is SENT to the master (NOT simply the
				// next token that arrives!). Perhaps that is how this should be cleared.
				GuiVar_StopKeyPending = (true);

				GuiVar_ScrollBoxHorizScrollPos = 0;
				GuiVar_ScrollBoxHorizScrollMarker = 0;

				DIALOG_draw_ok_dialog( GuiStruct_dlgStop_0 );
				break;
			}
			else
			{
				bad_key_beep();
			}
		    break;


		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */

#define BACKLIGHT_TIMEOUT_AFTER_KEYPRESSED_MS	(15 * 60 * 1000)

#define BACKLIGHT_TIMEOUT_AFTER_STARTUP_MS		(10 * 1000)

#define KEY_SCAN_QUEUE_TIMEOUT_MS				(500)

/* ---------------------------------------------------------- */
/**
 * The task that reads keypad data from a queue and processes it accordingly. If
 * no key shows for x amount of time, we turn off the backlight. The first key
 * turns it on.
 * 
 * @executed 
 * 
 * @param pvParameters 
 *
 * @author rmd
 *
 * @revisions
 *    4/21/2011 Initial release
 *    2/9/2012  Added calls to Process_ENG_SPA_key when the warning screen or
 *              help screen is visible.
 */
extern void Key_Processing_Task( void *pvParameters )
{
	UNS_32							backlight_turn_off_ms;
	
	KEY_TO_PROCESS_QUEUE_STRUCT		lktpqs;
	
	// ----------
	
	// 10/23/2012 rmd : To support the WDT activity monitor.
	UNS_32	task_index;
	
	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// ----------

	// Initialize the screen_history pile. The key process task uses these values as keys come in.
	screen_history_index = 0;

	ScreenHistory[ 0 ]._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	ScreenHistory[ 0 ]._02_menu = SCREEN_MENU_MAIN;
	ScreenHistory[ 0 ]._04_func_ptr = (void *)&FDTO_MAIN_MENU_draw_menu;
	ScreenHistory[ 0 ]._06_u32_argument1 = (true);
	ScreenHistory[ 0 ]._08_screen_to_draw = CP_MAIN_MENU_STATUS;

	ScreenHistory[ 0 ].key_process_func_ptr = MAIN_MENU_process_menu;

	GuiVar_MenuScreenToShow = ScreenHistory[ 0 ]._08_screen_to_draw;

	backlight_turn_off_ms = BACKLIGHT_TIMEOUT_AFTER_STARTUP_MS;

	// Let the task roll.
	while( (true) )
	{
		// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
		if( xQueueReceive( Key_To_Process_Queue, &lktpqs, MS_to_TICKS( KEY_SCAN_QUEUE_TIMEOUT_MS ) ) == pdPASS )
		{
			#if SHOW_STUCK_KEY_ALERTS_AT_MIDNIGHT
			
				UNS_32		iii;
		
				if( lktpqs.repeats >= STUCK_KEY_REPEAT_COUNT_THRESHOLD )
				{
					for( iii=0; iii<NUMBER_OF_KEYS; iii++ )
					{
						if( stuck_key_data[ iii ].key_code == lktpqs.keycode )
						{
							stuck_key_data[ iii ].recorded_repeats = lktpqs.repeats;
							
							// 9/10/2018 rmd : Should only be one match. Git.
							break;
						}
					}
				}

			#endif
			
			// ----------
			
			// Successful queue receive meaning a key came in. First key turns on the back light.
			if( BACKLIGHT_is_OFF )
			{
				BACKLIGHT_ON;

				postContrast_or_Volume_Event( CVQC_SET_VOLUME_DAC, CONTRAST_AND_SPEAKER_VOL_get_speaker_volume() );
			}
			else
			{
				if( ScreenHistory[ screen_history_index ].key_process_func_ptr != NULL )
				{
					if( g_DIALOG_ok_dialog_visible == (true) )
					{
						DIALOG_process_ok_dialog( lktpqs );
					}
					else if( g_COPY_DIALOG_visible == (true) )
					{
						COPY_DIALOG_process_dialog( lktpqs );
					}
					else
					{
						// THIS IS IT. This calls the key handler for all the
						// screens. When the screen is first moved to this is
						// setup.
						(ScreenHistory[ screen_history_index ].key_process_func_ptr)( lktpqs );
					}
				}
	
				// 
				// sprintf((void *)buf, "Key: 0x%02x, seq: %04d\n\r", key_event.keycode, key_event.seq_num);  format for the uart
				//sprintf( str_64, "Key: 0x%02x, seq: %04d", key_event.keycode, key_event.seq_num );
				//DFP_ANSI2( 1000, GuiLib_ALIGN_LEFT, GuiLib_PS_NUM, GuiLib_UNDERLINE_OFF, str_64 );
	
				//sprintf( str_32, "key: %04x", key_event.keycode );
				//DFP_ANSI2(  LL16,  GuiLib_ALIGN_LEFT,  GuiLib_PS_NUM,  GuiLib_UNDERLINE_OFF,  str_32 );
			}
	
			// Now that a key is pressed we wait.
			backlight_turn_off_ms = BACKLIGHT_TIMEOUT_AFTER_KEYPRESSED_MS;  // now that someone is there wait 5 minutes before we turn off the backlight

			//gpio_clr_gpio_pin( PIN_RRE_CONFIG_TO_RRE );  // scope probe
		}
		else
		{
			if( backlight_turn_off_ms >= KEY_SCAN_QUEUE_TIMEOUT_MS )
			{
				backlight_turn_off_ms -= KEY_SCAN_QUEUE_TIMEOUT_MS;
	
				if( backlight_turn_off_ms < KEY_SCAN_QUEUE_TIMEOUT_MS )
				{
					BACKLIGHT_OFF;
					
					postContrast_or_Volume_Event( CVQC_SET_VOLUME_DAC, 0 );
					
					// ----------
					
					// 12/13/2017 rmd : See that CODE_DOWNLOAD test below, well I think regardless of what code
					// download is doing stop any potential RADIO TEST that may be ongoing. We stop it at the 15
					// minute keypad timeout to prevent a never ending potential LR radio blaring the test data
					// out, if thte user didn't STOP the test.
					RADIO_TEST_post_event( RADIO_TEST_EVENT_stop_the_test );
					
					// ----------
					
					// 3/27/2014 ajv : Prevent the screen from being redrawn if
					// the keypad times out while a code download is in
					// progress.
					if( GuiVar_CodeDownloadState == CODE_DOWNLOAD_STATE_IDLE )
					{
						// 5/18/2015 ajv : Disable the speaker to ensure no good or bad key beeps are played as we
						// close any open dialogs and roll back to the Status screen.
						speaker_disable();

						// ----------

						// 5/15/2015 ajv : When the keypad times out, return to the Status screen by
						// processing BACK keys until we're all the way back. However, since we're using the BACK
						// key, we needed to turn off the speaker during this process.
						while( screen_history_index > 0 )
						{
							// 6/5/2015 ajv : There are some cases where a dialog may display while rolling back through
							// the screens. For example, if a user is viewing the MEMORY USAGE screen, processing the
							// BACK key will result in the TECH SUPPORT menu displaying. We need to make sure such
							// dialogs are closed before processing the next BACK key. Therefore, ensure all dialogs are
							// closed during every iteration of this loop.
							DIALOG_close_all_dialogs();

							lktpqs.keycode = KEY_BACK;
							lktpqs.repeats = 0;
							(ScreenHistory[ screen_history_index ].key_process_func_ptr)( lktpqs );
						}

						// 6/5/2015 ajv : There are some cases where a dialog may display while rolling back through
						// the screens. For example, if a user is viewing the MEMORY USAGE screen, processing the
						// BACK key will result in the TECH SUPPORT menu displaying. We need to make sure such
						// dialogs are closed before processing the next BACK key. Therefore, ensure all dialogs are
						// closed after completing the above loop.
						DIALOG_close_all_dialogs();

						// In case the user is already on the Main menu, jump to the status menu item to ensure the
						// Status screen is displayed.
						GuiVar_MenuScreenToShow = CP_MAIN_MENU_STATUS;

						// 5/6/2015 ajv : If we were on a cursor position on the Status screen when the keypad timed
						// out, make sure we jump back to Status cursor position.
						if( GuiVar_StatusShowLiveScreens )
						{
							GuiVar_StatusShowLiveScreens = (false);
						}

						// ----------

						// 5/18/2015 ajv : Reenable the speaker now that we're all the way back to the Status
						// screen.
						speaker_enable();

						// ----------

						Display_Post_Command( &ScreenHistory[ screen_history_index ] );
					}
				}
			}
		}

		// ----------
		
		// 10/23/2012 rmd : Keep the activity indication going.
		task_last_execution_stamp[ task_index ] = xTaskGetTickCount();

		// ----------
		
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

