/*  file = key_scanner.c                       01.07.2011 rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy
#include	<string.h>

#include	"key_scanner.h"

#include	"lpc32xx_intc_driver.h"

#include	"lpc32xx_kscan_driver.h"

#include	"lpc32xx_clkpwr_driver.h"

#include	"app_startup.h"

#include	"alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	UNS_32		maximum_hertz;

	UNS_32		repeats_to_get_to_maximum_hertz;

} KS_REPEAT_CONTROL_STRUCT;



#define		KRATE_DEFAULT_REPEATS_TO_GET_TO_MAX_HERTZ	100

#define		KS_REPEAT_INITIAL_REPEAT_RATE_HZ			5

#define		KS_REPEAT_MAXIMUM_REPEAT_RATE_HZ			20

#define		KS_REPEAT_TILL_FIRST_REPEAT_MS				500



const KS_REPEAT_CONTROL_STRUCT Regular_Repeat_Profile =
{ 
	.maximum_hertz = KS_REPEAT_MAXIMUM_REPEAT_RATE_HZ,

	.repeats_to_get_to_maximum_hertz = KRATE_DEFAULT_REPEATS_TO_GET_TO_MAX_HERTZ,
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	KS_STATE_NO_KEY					1
#define	KS_STATE_REPEATING				2
#define	KS_STATE_WAITING_FOR_NO_KEY		3

typedef struct
{
	UNS_32						state;
	
	// The latest status and set of registers captured in the isr.
	KEY_SCANNER_QUEUE_STRUCT	ksqs;
	
	UNS_32						captured_column_state [ KEY_ROWS ];
	UNS_32						captured_row;
	UNS_32						captured_col;

	// 6/15/2012 rmd : This is a float so we can slow the progress towards the maximum rate as
	// we get closer to the maximum rate.
	float						internal_repeat_count;

	// 2/21/2013 ajv : This tracks the number of repeats so far and passes it to
	// the key structure.
	UNS_32						external_repeat_count;

	UNS_16						already_dwelled_at_25; 
	UNS_16						already_dwelled_at_50; 
	UNS_16						already_dwelled_at_75; 
	
	KS_REPEAT_CONTROL_STRUCT	rcs;

	xTimerHandle				repeat_timer;

} KEY_SCANNER_CONTROL_STRUCT;


KEY_SCANNER_CONTROL_STRUCT  ks_control;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void kscan_isr( void )
{
	// NOTE: It is possible for two keys to be released simultaneously and cause only ONE interrupt and therefore only one queue
	//  	 message. I think the same is true with key presses.
	UNS_32						i;

	KEY_SCANNER_QUEUE_STRUCT	ks_qs;

	portBASE_TYPE				higher_priority_woken;
	
	// Obtain the new keyscan data. Somewhere in the register data is the change that caused the interrupt.
	for( i=0; i<KEY_ROWS; i++ )
	{
		ks_qs.new_column_state[ i ] = KSCAN->ks_data[ i ];
	}

	// If we are going to take the last spot...do so and flag it as an error. Even though technically
	// the queue didn't overflow. Taking the last spot is our definition of an overflow.
	// 
	// This IS within an ISR so do not attempt an alert line. Rather signal the outside world of such.
	ks_qs.queue_full_error = ( uxQueueMessagesWaitingFromISR( Key_Scanner_Queue ) >= (KEY_SCANNER_QUEUE_DEPTH-1) );
	
	ks_qs.message = KEY_SCANNER_QUEUE_MESSAGE_matrix_change;
	
	// If the queue is FULL this function just returns. No harm done. But we lose an event.
	xQueueSendToBackFromISR( Key_Scanner_Queue, &ks_qs, &higher_priority_woken );

	// And clears the interrupt.
	KSCAN->ks_irq = KSCAN_IRQ_PENDING_CLR;
}

/* ---------------------------------------------------------- */
static void _ks_repeat_timer_callback( xTimerHandle pxTimer )
{
	// This is NOT an interrupt. But rather executed within the context of the timer task.
	
	KEY_SCANNER_QUEUE_STRUCT	ks_qs;
	
	// If we are going to take the last spot...do so and flag it as an error. Even though technically the queue didn't overflow.
	// Taking the last spot is our definition of an overflow that way we'll get an alert when it gets close?
	ks_qs.queue_full_error = ( uxQueueMessagesWaiting( Key_Scanner_Queue ) >= (KEY_SCANNER_QUEUE_DEPTH-1) );
	
	ks_qs.message = KEY_SCANNER_QUEUE_MESSAGE_repeat_timer_expired;

	xQueueSendToBack( Key_Scanner_Queue, &ks_qs, portMAX_DELAY );
}

/* ---------------------------------------------------------- */
static void __ks_process_state_no_key( void )
{
	// In this function all we are doing is looking for the first key pressed. Then latching it. Functionally we will then
	// generate repeats until that key has been released.

	UNS_32	row, col; 
	
	KEY_TO_PROCESS_QUEUE_STRUCT		lktpqs;

	if( ks_control.ksqs.message == KEY_SCANNER_QUEUE_MESSAGE_matrix_change )
	{
		// Hunt for differences
		for( row=0; row<KEY_ROWS; row++ )
		{
			// We could look for a change from the captured. Or simply look for a none zero value indicating a key is down. I think all
			// we need to do is look for a non-zero value until we need to complicate the behavior.
			if( ks_control.ksqs.new_column_state[ row ] != 0 )
			{
				for( col=0; col<KEY_COLUMNS; col++  )
				{
					if( (ks_control.ksqs.new_column_state[ row ] & _BIT(col)) != 0 )
					{
						// Okay we've got a key pressed. Compute it's value and tell the outside world about it. Also start the repeat timer and
						// switch states.
						// 
						// What is the keys value? Since each row and column only count to 8 we can create a value by sliding over the row 4 bits
						// and adding to that the column value.
						lktpqs.keycode = ((row<<4) + col);
						
						lktpqs.repeats = 0;  // This is the initial key.
						
						if( xQueueSendToBack( Key_To_Process_Queue, &lktpqs, 0 ) != pdPASS )
						{
							Alert_Message( "KEY TO PROCESS queue overflow" );
						}
	
						// Reset the timer to the ms till the first key repeat. The timer function to change the period actually changes the period
						// and STARTS the timer!
						//
						// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
						// when posting is safe. Anything on the timer queue is quickly processed and therefore the
						// queue is effectively always empty.
						xTimerChangePeriod( ks_control.repeat_timer, MS_to_TICKS( KS_REPEAT_TILL_FIRST_REPEAT_MS ), portMAX_DELAY );

						// And capture the new register pattern. And the row and column so we know when this key is released.
						memcpy( &ks_control.captured_column_state, &ks_control.ksqs.new_column_state, sizeof(ks_control.captured_column_state) );
	
						ks_control.captured_row = row;  // Used to see when the key is released.
						ks_control.captured_col = col;
						
						ks_control.internal_repeat_count = 0;
						ks_control.external_repeat_count = 0;

						ks_control.already_dwelled_at_25 = 0;
						ks_control.already_dwelled_at_50 = 0;
						ks_control.already_dwelled_at_75 = 0;

						ks_control.state = KS_STATE_REPEATING;
					}
				}
			}
		}
	}
	else
	if( ks_control.ksqs.message == KEY_SCANNER_QUEUE_MESSAGE_repeat_timer_expired )
	{
		// In this state ignore this message.
	}
}

/* ---------------------------------------------------------- */
static void __ks_process_state_repeats( void )
{
	KEY_TO_PROCESS_QUEUE_STRUCT		lktpqs;

	UNS_32		final_hz;

	UNS_32		calculated_ms;
	
	float		increased_hz;
	
	UNS_32		new_hz;

	if( ks_control.ksqs.message == KEY_SCANNER_QUEUE_MESSAGE_matrix_change )
	{
		// Check to see if the same key is still depressed. If so the ISR was triggered from other key activity in the matrix. At
		// this time our behavior definition will be to ignore other key activity till this originally captured key has been
		// released.
		if( (ks_control.ksqs.new_column_state[ ks_control.captured_row ] & _BIT( ks_control.captured_col )) == 0 )
		{
			// It has been released! Stop the timer and return to the no_key state.
			xTimerStop( ks_control.repeat_timer, portMAX_DELAY );

			ks_control.state = KS_STATE_NO_KEY;
		}
	}
	else
	if( ks_control.ksqs.message == KEY_SCANNER_QUEUE_MESSAGE_repeat_timer_expired )
	{
		// Range check what's set. Not to be 0 to avoid divide by zero error.
		if( ks_control.rcs.repeats_to_get_to_maximum_hertz == 0 )
		{
			ks_control.rcs.repeats_to_get_to_maximum_hertz = KRATE_DEFAULT_REPEATS_TO_GET_TO_MAX_HERTZ;		
		}
		
		// Range check upper limit on final rate.
		final_hz = ((ks_control.rcs.maximum_hertz > KS_REPEAT_MAXIMUM_REPEAT_RATE_HZ) ? KS_REPEAT_MAXIMUM_REPEAT_RATE_HZ : ks_control.rcs.maximum_hertz);
		
		// ----------
		
		// 8/21/2014 rmd : Bump the count exposed to the user.
		ks_control.external_repeat_count += 1;

		// ----------
		
		// 6/15/2012 rmd : I've found that a straight linear progression on the repeat rate does not
		// dwell long enough at the higher rates before shifting into yet a higher gear. I need to
		// use the linear progression up till the 50% mark and then slow the progression towards the
		// final rate. By toying with the rate of rise of the repeat count (towards the maximum) we
		// effectively change how many repeats we get at a given repeat rate.
		if( ks_control.internal_repeat_count < (0.25 * ks_control.rcs.repeats_to_get_to_maximum_hertz) )
		{
			ks_control.internal_repeat_count += 1.0;
		}
		else 
		if( ks_control.internal_repeat_count < (0.75 * ks_control.rcs.repeats_to_get_to_maximum_hertz) )
		{
			ks_control.internal_repeat_count += 0.4;
		}
		else 
		if( ks_control.internal_repeat_count < ks_control.rcs.repeats_to_get_to_maximum_hertz )
		{
			ks_control.internal_repeat_count += 0.4;
		}

		// ----------
		
		// 8/21/2014 rmd : Suppress increased rep rate for the first 10 key strokes. If not we see
		// the acceleration on some of the longer menus. And that's not a desireable nor expected
		// effect.
		if( ks_control.external_repeat_count <= 10 )
		{
			new_hz = KS_REPEAT_INITIAL_REPEAT_RATE_HZ;	
		}
		else
		{
			increased_hz = ( (final_hz - KS_REPEAT_INITIAL_REPEAT_RATE_HZ) / (float)ks_control.rcs.repeats_to_get_to_maximum_hertz );
		
			new_hz = KS_REPEAT_INITIAL_REPEAT_RATE_HZ + ((UNS_32)(ks_control.internal_repeat_count * increased_hz));
	
			// ----------
			
			// 8/20/2014 rmd : Range check for a decent value.
			if( new_hz > KS_REPEAT_MAXIMUM_REPEAT_RATE_HZ )
			{
				new_hz = KS_REPEAT_MAXIMUM_REPEAT_RATE_HZ;	
			}
			
			if( new_hz < KS_REPEAT_INITIAL_REPEAT_RATE_HZ )
			{
				new_hz = KS_REPEAT_INITIAL_REPEAT_RATE_HZ;	
			}
		}
		
		calculated_ms = (UNS_32)(1000.0/new_hz);
	
		// The timer function to change the period actually changes the period and STARTS the timer!
		//
		// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
		// when posting is safe. Anything on the timer queue is quickly processed and therefore the
		// queue is effectively always empty.
		xTimerChangePeriod( ks_control.repeat_timer, MS_to_TICKS( calculated_ms ), portMAX_DELAY );

	
		/* 
		mark_25 = (0.25 * ks_control.rcs.repeats_to_get_to_final_hertz);
		mark_50 = (0.50 * ks_control.rcs.repeats_to_get_to_final_hertz);
		mark_75 = (0.75 * ks_control.rcs.repeats_to_get_to_final_hertz);
		
		// Does the profile want us to hold the timing at the 25 percent mark?
		if( ks_control.rcs.dwell_at_25_percent_count > 0 )
		{
			if( ks_control.already_dwelled_at_25 < ks_control.rcs.dwell_at_25_percent )
			{
				
				
				ks_control.already_dwelled_at_25 += 1;
			}
		}
		
		if( ks_control.repeat_count <= 47 )
		{
			calculated_ms = (500 - (ks_control.repeat_count * 10));	
	
			// Range check value of delay computed.
			if( (calculated_ms > 500) || (calculated_ms < 10) )
			{
				calculated_ms = 300;  // Just something reasonable.
			}
		}
		else
		{
			calculated_ms = 10;
		}
		
		*/
	
	
		lktpqs.keycode = ((ks_control.captured_row<<4) + ks_control.captured_col);

		lktpqs.repeats = ks_control.external_repeat_count;  // Set which repeat this is.

		if( xQueueSendToBack( Key_To_Process_Queue, &lktpqs, 0 ) != pdPASS )
		{
			Alert_Message( "KEY TO PROCESS queue overflow" );
		}
	}
}

/* ---------------------------------------------------------- */
/*
static void __set_the_repeat_control_structure( KS_REPEAT_CONTROL_STRUCT *ptr_rcs )
{
	// This function should be called from the key_scanner task itself and invoked via a task queue message that carries with it
	// the new settings.
	ks_control.rcs = *ptr_rcs;
	
}
*/

/* ---------------------------------------------------------- */
void key_scanner_task( void *pvParameters )
{
	// ----------
	
	// 10/23/2012 rmd : To support the WDT activity monitor.
	UNS_32	task_index;
	
	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// ----------

	// This task is responsible for deciding if a key has been hit. We may incorparate into that decision
	// if it is a valid key or not for this screen we are on. But not yet sure about that. The key scanner
	// ISR produces a message to this task for EVERY keypad change of state. So when a key is pressed we get
	// a message. And when a key is released we get a message. I'll start out simple. Upon keypress I'll send
	// a message to the key process task.
	// 
	// Next we'll incorporate key repeats. Utilizing a timer.

	// Last column state for each row. To compare against the new one to see changes.
	KSCAN_REGS_T	*lkscan;


	xDisable_ISR( KEY_IRQ_INT );  // good practice as were about to change the ISR

	// Disable ethernet (which uses a lot of KSCAN pins)
	clkpwr_select_enet_iface( 0, 0 );

	/* Enable clock to key scanner block */
	clkpwr_clk_en_dis(CLKPWR_KEYSCAN_CLK, 1);


	lkscan = ((KSCAN_REGS_T *)(KSCAN_BASE));

	lkscan->ks_deb = KSCAN_DEB_NUM_DEB_PASS(3);

	lkscan->ks_scan_ctl = KSCAN_SCTRL_SCAN_DELAY(5);

	lkscan->ks_fast_tst = KSCAN_FTST_USE32K_CLK;

	lkscan->ks_matrix_dim = KSCAN_MSEL_SELECT(8);

	lkscan->ks_irq = KSCAN_IRQ_PENDING_CLR;

	// Install key scanner interrupt handlers as an IRQ interrupt. Per the datasheet the KEY_SCAN interrupt
	// is to be set as a HIGH_LEVEL interrupt. The interrupt is not enabled with this function call. 
	xSetISR_Vector( KEY_IRQ_INT, INTERRUPT_PRIORITY_keypad_scanner, ISR_TRIGGER_HIGH_LEVEL, kscan_isr, NULL );

	// Enable key scanner interrupt in the interrupt controller
	xEnable_ISR( KEY_IRQ_INT );

	// The value of the timeout period used in the next line is irrelevant. The timeout period is truly established when the
	// first key is picked up.
	ks_control.repeat_timer = xTimerCreate( (const signed char*)"KS Timer", MS_to_TICKS( KS_REPEAT_TILL_FIRST_REPEAT_MS ), FALSE, NULL, _ks_repeat_timer_callback );

	if( ks_control.repeat_timer == NULL )
	{
		Alert_Message_va( "Timer NOT CREATED : %s, %u", CS_FILE_NAME( __FILE__ ), __LINE__ );
	}


	ks_control.rcs = Regular_Repeat_Profile;
	

	ks_control.state = KS_STATE_NO_KEY;

	// Let the endless task begin
	while( TRUE )
	{
		// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
		if( xQueueReceive( Key_Scanner_Queue, &ks_control.ksqs, MS_to_TICKS( 500 ) ) == pdTRUE )
		{
			if( ks_control.ksqs.queue_full_error == TRUE )
			{
				Alert_Message( "KEY SCANNER queue overflow" );
			}
			else
			{
				switch( ks_control.state )
				{
					case KS_STATE_NO_KEY:
						__ks_process_state_no_key();
						break;
		
					case KS_STATE_REPEATING:
						__ks_process_state_repeats();
						break;
		
					case KS_STATE_WAITING_FOR_NO_KEY:
						break;
				}

			}

		}

		// ----------
		
		// 10/23/2012 rmd : Keep the activity indication going.
		task_last_execution_stamp[ task_index ] = xTaskGetTickCount();

		// ----------
		
	}  // of task while loop

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

