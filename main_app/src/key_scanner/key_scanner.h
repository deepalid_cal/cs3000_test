/*  file = key_scanner.h                      02.09.2010  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef KEY_SCANNER_H
#define KEY_SCANNER_H


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// The LPC3250 supports an 8 x 8 keypad. And we wired it up to support the maximum possible 64 keys.

#define KEY_COLUMNS		8

#define KEY_ROWS 		8


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#define KEY_SCANNER_QUEUE_MESSAGE_matrix_change				1

#define KEY_SCANNER_QUEUE_MESSAGE_repeat_timer_expired		2


typedef struct
{
	// If in the isr the queue is full this allows us to tell about it.
	UNS_32		message;
	
	UNS_32		queue_full_error;

	UNS_32		new_column_state[ KEY_ROWS ];

} KEY_SCANNER_QUEUE_STRUCT;


// The next define sets the depth of the keypad event queue
#define KEY_SCANNER_QUEUE_DEPTH		10


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


void key_scanner_task( void *pvParameters );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

