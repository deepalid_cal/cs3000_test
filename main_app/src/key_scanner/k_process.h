/*  file = k_process.h    2009.05.21  rmd                     */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_K_PROCESS_H
#define _INC_K_PROCESS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"cs_common.h"



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// The keys by name we will use in the project. DO NOT CHANGE THEIR VALUES. The values
// come from the actual keypad hardware and cannot be changed unless you tweak the key
// scanner code.


/*
codes for REV E hardware

#define	KEY_C_UP		0x34
#define KEY_C_DOWN		0x30
#define KEY_C_LEFT		0x31
#define	KEY_C_RIGHT		0x33
#define KEY_SELECT		0x32

#define	KEY_NEXT		0x20
#define	KEY_PREV		0x24
#define KEY_PLUS		0x44
#define KEY_MINUS		0x40

#define KEY_STOP		0x00
#define KEY_HELP		0x41
#define KEY_ENG_SPA		0x43
#define KEY_BACK		0x53
*/

// ----------

// 6/7/2013 rmd : Codes for Rev G pcb.
#define	KEY_C_UP		0x04
#define KEY_C_DOWN		0x00
#define KEY_C_LEFT		0x01
#define	KEY_C_RIGHT		0x03

#define KEY_SELECT		0x02

#define	KEY_NEXT		0x14 // formerly 0x10 for the old overlay layout with NEXT on top
#define	KEY_PREV		0x10 // formerly 0x14 for the old overlay layout with PREV on bottom

#define KEY_PLUS		0x54
#define KEY_MINUS		0x50

#define KEY_STOP		0x30
#define KEY_HELP		0x51
#define KEY_ENG_SPA		0x53
#define KEY_BACK		0x43

// ----------

// 3/18/2014 rmd : So think of keycodes as events to be processed by the key_process task.
// That's what they are. It helps to think of them that way when understanding the
// additional codes that follow. These are special events for the key processing task. Only
// handled on certain sceens. Very important : these defines must not be potential keycodes!

#define	DEVICE_EXCHANGE_KEY_busy_try_again_later			(0x9000)

#define	DEVICE_EXCHANGE_KEY_read_settings_completed_ok		(0x9001)
#define	DEVICE_EXCHANGE_KEY_read_settings_error				(0x9002)
#define	DEVICE_EXCHANGE_KEY_read_settings_in_progress		(0x9003)

#define	DEVICE_EXCHANGE_KEY_write_settings_completed_ok		(0x9004)
#define	DEVICE_EXCHANGE_KEY_write_settings_error			(0x9005)
#define	DEVICE_EXCHANGE_KEY_write_settings_in_progress		(0x9006)

// 6/1/2015 mpd : Screens created with EasyGUI use these device exchange keys to make decisions. Unfortunately, EasyGUI 
// indexed structures can only handle index values from 0 - 99. These keys defines must be outside ASCII values of 0 - 
// 255 to avoid potential keycodes. So, for GUI use, the device exchange keys will be indexed from 0 -6 by subtracting
// the DEVICE_EXCHANGE_KEY_MIN from the current key. Keep this value equal to the first key.
#define	DEVICE_EXCHANGE_KEY_MIN								( DEVICE_EXCHANGE_KEY_busy_try_again_later )

// ----------

// 3/21/2018 rmd : A key define for WALK_THRU delay countdown processing.
#define WALK_THRU_KEY_countdown_timer_expired				(0x9100)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#if SHOW_STUCK_KEY_ALERTS_AT_MIDNIGHT

	#define		NUMBER_OF_KEYS						(13)

	#define		STUCK_KEY_REPEAT_COUNT_THRESHOLD	(2000)
		
	typedef struct
	{
		UNS_32	key_code;
		
		UNS_32	recorded_repeats;
		
		char	*key_name_string;
		
	} STUCK_KEY_TRACKING_STRUCT;
	
	extern STUCK_KEY_TRACKING_STRUCT stuck_key_data[ NUMBER_OF_KEYS ];
	
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// The next define sets the depth of the keypad event queue
#define KEY_TO_PROCESS_QUEUE_DEPTH			(25)

// The following structure is the output of the k_process interaction with the key_scannner task.
typedef struct
{
	UNS_32		keycode;
	
	UNS_32		repeats;

} KEY_TO_PROCESS_QUEUE_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void KEY_process_BACK_from_editing_screen( void *preturn_to_menu_func_ptr );

extern void KEY_process_ENG_SPA( void );

extern void KEY_process_global_keys( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

extern void Key_Processing_Task( void *pvParameters );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

