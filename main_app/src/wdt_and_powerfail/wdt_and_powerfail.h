/*  file = wdt_and_powerfail.h                05.09.2011  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_WDT_AND_POWERFAIL_H
#define INC_WDT_AND_POWERFAIL_H


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern BOOL_32 init_restart_info( const BOOL_32 pforce_the_init );




#define	SYSTEM_SHUTDOWN_EVENT_powerfail_detected					(0x001A)

// ----------

// 9/3/2015 rmd : Used when the reset is initiated via controller keypad. On the other side
// of the factory reset this means the registration asks the commserver to clear the web
// pdata tables and we allow the default pdata to be sent to the commserver.
#define	SYSTEM_SHUTDOWN_EVENT_factory_reset_via_keypad							(0x001B)

// 9/3/2015 rmd : Used when the reset is commanded by the commserver as part of a panel swap
// procedure for the NEW panel. By definition on the other side of the factory reset this
// means the registration does not ask the commserver to clear the pdata tables and we also
// block sending program data to the commserver till after we have rcvd pdata from the
// commserver.
#define	SYSTEM_SHUTDOWN_EVENT_factory_reset_via_cloud_panel_swap_for_new_panel	(0x001C)

// 9/3/2015 rmd : Used when the reset is commanded by the commserver as part of a panel swap
// procedure for the OLD panel. By definition on the other side of the factory reset this
// means the registration asks the commserver to clear the web pdata tables and we allow the
// default pdata to be sent to the commserver.
#define	SYSTEM_SHUTDOWN_EVENT_factory_reset_via_cloud_panel_swap_for_old_panel	(0x001D)

// ----------

// 3/3/2017 rmd : These SHUTDOWN_EVENTs are primarily used to generate an alert line on
// startup about why the restart. So the values must be preserved else we'll create a
// backwards incompatiblity with existing alerts in the database.

#define	SYSTEM_SHUTDOWN_EVENT_main_code_file_updated				(0x0020)

#define	SYSTEM_SHUTDOWN_EVENT_tpmicro_code_file_updated				(0x0021)

#define	SYSTEM_SHUTDOWN_EVENT_main_code_distribution_completed		(0x0022)

#define	SYSTEM_SHUTDOWN_EVENT_tpmicro_code_distribution_completed	(0x0023)

#define	SYSTEM_SHUTDOWN_EVENT_code_distribution_error				(0x0024)

#define	SYSTEM_SHUTDOWN_EVENT_code_receipt_error					(0x0025)

#define	SYSTEM_SHUTDOWN_EVENT_one_of_the_slaves_rebooted			(0x0026)

// ----------

// 3/3/2017 rmd : Have expanded the list to include the new reasons for when the commserver
// instructs the hub to distribute code. These are used not only to make an alert line, but
// also to flag instructions to the HUB about what types of binary distribution(s) need to
// take place. It isn't like on a chain where on reboot the master can 'see' during the scan
// what kind of code distribution needs to take place. The hub has to be told what to do.
// And these defines do that.
#define	SYSTEM_SHUTDOWN_EVENT_hub_to_distribute_tpmicro_binary		(0x0027)

#define	SYSTEM_SHUTDOWN_EVENT_hub_to_distribute_main_binary			(0x0028)

#define	SYSTEM_SHUTDOWN_EVENT_hub_to_distribute_both_binaries		(0x0029)

// ----------



extern void SYSTEM_application_requested_restart( UNS_32 preason );



extern void system_shutdown_task( void *pvParameters );


extern void wdt_monitoring_task( void *pvParameters );


extern void freeze_and_restart_app( void );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// This define is to handle TRULY FATAL errors that are detected.
#define FREEZE_and_RESTART_APP freeze_and_restart_app();

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

