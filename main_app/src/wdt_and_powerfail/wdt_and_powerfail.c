/*  file = wdt_and_powerfail.c                05.09.2011  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memset
#include	<string.h>

#include	"wdt_and_powerfail.h"

#include	"gpio_setup.h"

#include	"lpc32xx_intc_driver.h"

#include	"lpc32xx_clkpwr_driver.h"

#include	"epson_rx_8025sa.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"lpc32xx_wdt.h"

#include	"trcuser.h"

#include	"comm_mngr.h"

#include	"df_storage_mngr.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 2/21/2014 rmd : To identify the event. Which is the reason for the shutdown and eventual
	// restart.
	UNS_32	event;
	
} SYSTEM_SHUTDOWN_EVENT_QUEUE_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static const char	RESTART_INFO_VERIFY_STRING_PRE[] = "RESTART v03";

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// 11/16/2012 rmd : Return (true) if there has been a code update. So that we can manage a
// clean start for certain resources that become invalid upon a code update. Such as the
// battery backed irrigation list which contains pointers to variables in the SDRAM.
// Variables that may have moved!
extern BOOL_32 init_restart_info( const BOOL_32 pforce_the_init )
{
	BOOL_32	rv;
	
	// 11/16/2012 rmd : Initialize (true). This way if the battery backed restart_info fails its
	// OWN test for validity we trigger a reinitialization of most battery backed variables. The
	// reasoning being that is the restart_info is trashed we don't really know what is going
	// on. There may or may not have been a code update. And following a code update one MUST
	// not rely on the battery backed variables. They contain pointers to SDRAM variables which
	// could be invalid. And this is a FATAL error if one tries to use them. A FATAL error which
	// can lead to an UNBOOTABLE state!!!!!
	rv = (true);
	
	// ----------
	
	// If the string is intact or we are being asked to restart it.
	if( (pforce_the_init == (true)) || (strncmp( restart_info.verify_string_pre, RESTART_INFO_VERIFY_STRING_PRE, sizeof(RESTART_INFO_VERIFY_STRING_PRE) ) != 0 ) )
	{
		Alert_battery_backed_var_initialized( "Restart Info", pforce_the_init );

		// ----------

		// 9/25/2012 rmd : The following memset does indeed (of course) set all the UNS_32 numbers
		// to 0. And all _Bools to (false).
		memset( &restart_info, 0x0, sizeof(restart_info) );

		// ----------

		// 7/24/2015 rmd : The restart_info.controller_index is implicitly set to a zero. Which
		// corresponds to an 'A' letter controller. Which is okay to start. But we still set this
		// after we read out the FLOWSENSE file. See comment there for further info.
		
		// ----------

		// 11/16/2012 rmd : DO NOT set the code revision string here. That can produce a FATAL
		// NON-BOOTABLE condition. Read comments surrounding the code_revision_string.
		
		// ----------

		strlcpy( restart_info.verify_string_pre, RESTART_INFO_VERIFY_STRING_PRE, BB_PRE_STRING_LENGTH );
	}
	else
	{
		#if ALERT_SHOW_ROUTINE_INITIALIZATIONS_DURING_STARTUP
			Alert_battery_backed_var_valid( "Restart Info" );
		#endif
		
		// ----------

		if( restart_info.exception_noted )
		{
			Alert_Message_va( "EXCEPTION: %s (%s)", restart_info.exception_description_string, restart_info.exception_current_task );
			
			// ----------

			restart_info.exception_noted = (false);

			memset( restart_info.exception_description_string, 0x00, sizeof(restart_info.exception_description_string) );

			memset( restart_info.exception_current_task, 0x00, sizeof(restart_info.exception_current_task) );
		}

		// ----------

		if( restart_info.assert_noted )
		{
			Alert_Message_va( "%s (%s)", restart_info.assert_description_string, restart_info.assert_current_task );
			
			// ----------

			restart_info.assert_noted = (false);

			memset( restart_info.assert_description_string, 0x00, sizeof(restart_info.assert_description_string) );

			memset( restart_info.assert_current_task, 0x00, sizeof(restart_info.assert_current_task) );
		}

		// ----------
		
		WDT_REGS_T	*wdt;
		
		wdt = ((WDT_REGS_T*)WDTIM_BASE);

		BOOL_32	watchdog_initiated_reset;
		
		// 10/16/2012 rmd : Read the reset source register and mask appropriately. If a WDT caused
		// the reset our variable will be (true).
		watchdog_initiated_reset = ( (wdt->reset_source) & 0x01 );

		// ----------
		
		if( restart_info.SHUTDOWN_impending == (true) )
		{
			if( restart_info.shutdown_reason == SYSTEM_SHUTDOWN_EVENT_powerfail_detected )
			{
				// 10/16/2012 rmd : If we detected a powerfailure but the power never went away the code is
				// set up to eventually trip the WDT. And this would be considered a brownout.
				if( watchdog_initiated_reset )
				{
					// 7/24/2015 rmd : Make the alert line using the battery backed copy of the controller
					// index.
					Alert_power_fail_brownout_idx( &(restart_info.shutdown_td), restart_info.controller_index );
				}
				else
				{
					// 7/24/2015 rmd : Make the alert line using the battery backed copy of the controller
					// index.
					Alert_power_fail_idx( &(restart_info.shutdown_td), restart_info.controller_index );
				}
			}
			else
			{
				Alert_reboot_request( restart_info.shutdown_reason );
				
				// ----------
				
				// 5/23/2017 rmd : Now support for unique shutdown reasons. Typically requested by
				// ourselves.
				switch( restart_info.shutdown_reason )
				{
					case SYSTEM_SHUTDOWN_EVENT_factory_reset_via_cloud_panel_swap_for_new_panel:
						// 9/3/2015 rmd : In the case of the NEW panel as part of the swap procedure we want to send
						// a regular registration that does not cause the web to clear the pdata database. And we
						// want to BLOCK pdata from being sent to the web until we have rcvd pdata from the web.
						Alert_Message( "Factory Reset for NEW panel" );
	
						weather_preserves.factory_reset_registration_to_tell_commserver_to_clear_pdata = (false);
	
						weather_preserves.factory_reset_do_not_allow_pdata_to_be_sent = (true);
						break;
						
					case SYSTEM_SHUTDOWN_EVENT_factory_reset_via_cloud_panel_swap_for_old_panel:
						// 9/3/2015 rmd : In the case of the OLD panel as part of the swap procedure we want to send
						// the registration indicating to the commserver to clear his pdata in preparation of
						// receipt of the OLD panels now default pdata. And certainly in light of that cannot block
						// the sending of the pdata.
						Alert_Message( "Factory Reset for OLD panel" );
	
						weather_preserves.factory_reset_registration_to_tell_commserver_to_clear_pdata = (true);
	
						weather_preserves.factory_reset_do_not_allow_pdata_to_be_sent = (false);
						break;
						
					case SYSTEM_SHUTDOWN_EVENT_factory_reset_via_keypad:
						// 9/3/2015 rmd : When the keypad initiates the factory reset we want cause the registration
						// to clear the web copy of the pdata in the database tables. And allow sending of the
						// default pdata.
						Alert_Message( "Factory Reset by user KEYPAD" );
	
						weather_preserves.factory_reset_registration_to_tell_commserver_to_clear_pdata = (true);
	
						weather_preserves.factory_reset_do_not_allow_pdata_to_be_sent = (false);
						break;
						
					case SYSTEM_SHUTDOWN_EVENT_hub_to_distribute_tpmicro_binary:
						// 5/23/2017 rmd : Cannot make this test as this function is called so early on the
						// information is unavailable. So comment out - but show for the spirit of it.
						// 5/23/2017 rmd : Should only see this if we are a hub!
						//if( CONFIG_this_controller_is_a_configured_hub() )
						{
							Alert_Message( "Reset: HUB set to distribute tpmicro binary" );
							
							weather_preserves.hub_needs_to_distribute_tpmicro_binary = (true);
						}
						//else
						{
						//	Alert_Message( "Reset: HUB logical error tpmicro" );
						}
						break;
						
					case SYSTEM_SHUTDOWN_EVENT_hub_to_distribute_main_binary:
						// 5/23/2017 rmd : Cannot make this test as this function is called so early on the
						// information is unavailable. So comment out - but show for the spirit of it.
						// 5/23/2017 rmd : Should only see this if we are a hub!
						//if( CONFIG_this_controller_is_a_configured_hub() )
						{
							Alert_Message( "Reset: HUB set to distribute main binary" );
							
							weather_preserves.hub_needs_to_distribute_main_binary = (true);
						}
						//else
						{
						//	Alert_Message( "Reset: HUB logical error main" );
						}
						break;
						
					case SYSTEM_SHUTDOWN_EVENT_hub_to_distribute_both_binaries:
						// 5/23/2017 rmd : Cannot make this test as this function is called so early on the
						// information is unavailable. So comment out - but show for the spirit of it.
						// 5/23/2017 rmd : Should only see this if we are a hub!
						//if( CONFIG_this_controller_is_a_configured_hub() )
						{
							Alert_Message( "Reset: HUB to distribute both binaries" );
							
							weather_preserves.hub_needs_to_distribute_tpmicro_binary = (true);
	
							weather_preserves.hub_needs_to_distribute_main_binary = (true);
						}
						//else
						{
						//	Alert_Message( "Reset: HUB logical error both" );
						}
						break;
				}
				
			}
		}
		else
		if( watchdog_initiated_reset == (true) )
		{
			Alert_watchdog_timeout();
		}
		else
		{
			// 10/18/2012 rmd : We'll have to keep an eye on this line. It is triggered by the
			// RESET_BUTTON or what I call DIRTY POWER. Meaning it takes a fairly abnormal power profile
			// (power coming and going) to achieve this state. If it turns out this occurs more
			// 'routinely' than I expect we may need to change the wording for this alert to more along
			// the lines of "POWER FAIL or RESET BUTTON PUSHED".
			Alert_Message( "DIRTY POWER or RESET BUTTON PUSHED" );
		}
		
		// 10/18/2012 rmd : This divider really helps readability. The line we just wrote are
		// stamped with the date and time when the terminating event (pwr fail, wdt, or reset
		// button) occurred. This line will be stamped with the actual present time and date. And
		// serves as a nice divider.
		Alert_divider_large();

		// ----------
		
		// 10/16/2012 rmd : Clear the history.

		restart_info.SHUTDOWN_impending = (false);

		// 2/21/2014 rmd : This shouldn't have to be cleared. But doing so proves our system and
		// logic are functioning as expected.
		restart_info.shutdown_reason = 0;

		restart_info.we_were_reading_spi_flash_data = (false);

		restart_info.we_were_writing_spi_flash_data = (false);
		
		// ----------

		if( strncmp( restart_info.main_app_code_revision_string, MAIN_APP_RUNNING_BUILD_DT, strlen(restart_info.main_app_code_revision_string) ) != 0 )
		{
			Alert_firmware_update_idx( restart_info.main_app_code_revision_string, MAIN_APP_RUNNING_BUILD_DT, restart_info.controller_index );
			
			// 11/16/2012 rmd : DO NOT update the code revision string here. A code update triggers a
			// reboot and integrity testing of the battery backed vars. If that process doesn't complete
			// due to another power failure, when we still want to see that a code update has occured
			// (not sure why but ...). Therefore we update the code revision string in one place...deep
			// into the startup!
		}
		else
		{
			// 11/16/2012 rmd : The only place this can be set (false). Meaning we are SURE the
			// structure was valid and there was absolutely NOT a code update.
			rv = (false);
		}
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static xQueueHandle		SYSTEM_shutdown_event_queue;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void powerfail_isr( void )
{
	SYSTEM_SHUTDOWN_EVENT_QUEUE_STRUCT	sseqs;

	// ----------

	// 10/19/2012 rmd : Because we have decided it is more robust to set this interrupt up as a
	// level triggered interrupt and the hardware cannot be cleared of the LOW condition that
	// caused this interrupt, we MUST DISABLE the INTERRUPT otherwise we would spend all of our
	// time in the interrupt. Everything would come to a dead halt. And either the MAXIM chip
	// would reset us (power too low) or the WDT would strike. These two things aren't so bad.
	// However none of this would happen gracefully. For example we couldn't save state so our
	// alert lines wouldn't indicate what happened. And the flash file system would not back out
	// of an on-going operation gracefully. So disable the int.
	SIC2_ER &= ~( SIC2_ER_GPI_01_MASK );

	// ----------

	portBASE_TYPE	higher_priority_woken;

	higher_priority_woken = pdFALSE;

	// ----------

	sseqs.event = SYSTEM_SHUTDOWN_EVENT_powerfail_detected;
		
	xQueueSendFromISR( SYSTEM_shutdown_event_queue, &sseqs, &higher_priority_woken );

	if( higher_priority_woken == pdTRUE )
	{
		portYIELD_FROM_ISR();
	}

	// ----------

	// WHAT'S THE RESULT...this ISR takes less than 5usec to run and happens like a usec or 2 from the falling edge of our
	// power fail advance PFO output from the MAX16033. And within about another 5 usec the power fail task itself is
	// running. A HIGH priority task. That SUSPENDS all the other tasks within about 20 usecs. So from the time the
	// powerfail interrupt happens till we are ready to go see if the FLASH STORAGE task has anything to do is like 30usec.

	// ----------
}

/* ---------------------------------------------------------- */
static void init_powerfail_isr( void )
{
	xDisable_ISR( GPI_01_INT );

	//xSetISR_Vector( GPI_01_INT, INTERRUPT_PRIORITY_powerfail_warning, ISR_TRIGGER_NEGATIVE_EDGE, powerfail_isr, NULL );
	xSetISR_Vector( GPI_01_INT, INTERRUPT_PRIORITY_powerfail_warning, ISR_TRIGGER_LOW_LEVEL, powerfail_isr, NULL );

	xEnable_ISR( GPI_01_INT );
}

/* ---------------------------------------------------------- */
extern void SYSTEM_application_requested_restart( UNS_32 preason )
{
	SYSTEM_SHUTDOWN_EVENT_QUEUE_STRUCT	sseqs;

	sseqs.event = preason;
	
	xQueueSend( SYSTEM_shutdown_event_queue, &sseqs, portMAX_DELAY );

	// 3/28/2014 rmd : We just posted a message to the HIGHEST priority task in the system.
	// There is no return from the xQueueSend function!! We will never execute the following
	// code.
	for(;;);
}

/* ---------------------------------------------------------- */
extern void system_shutdown_task( void *pvParameters )
{
	// 2/21/2014 rmd : This is the highest priority task in the system. It runs for two reasons.
	// First and foremost when a powerfail is detected. Secondly upon application demand to
	// cause a restart.

	// --------

	SYSTEM_SHUTDOWN_EVENT_QUEUE_STRUCT	sseqs;
	
	// --------

	// 2/21/2014 rmd : Do not move this queue create to the function that creates all the other
	// queues in app_startup. This task is created early before that function call and this
	// queue must be created as part of this task startup.
	SYSTEM_shutdown_event_queue = xQueueCreate( 10, sizeof(SYSTEM_SHUTDOWN_EVENT_QUEUE_STRUCT) );

	// --------

	// Kick off the hardware.
	init_powerfail_isr();

	// --------

	while( (true) )
	{
		// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
		if( xQueueReceive( SYSTEM_shutdown_event_queue, &sseqs, MS_to_TICKS( 500 ) ) == pdTRUE )
		{
			// 2/21/2014 rmd : This task is the VERY highest priority task in the system. So when it
			// runs here it has COMPLETE control.
			
			// ----------

			// 10/15/2012 rmd : Get set up to talk to the WDT hardware.
			WDT_REGS_T	*wdt;
			
			wdt = ((WDT_REGS_T*)WDTIM_BASE);

			// ----------

			// 10/17/2012 rmd : While we wait for the power fail or brown out to takes it's course we
			// take over control of the watchdog timer. As a matter of fact we suspend the wdt task just
			// a few line below. The definition of a BROWN out is we've been sitting in this task long
			// enough that we finally decided to reset ourselves (using the WDT hardware). NOTE: we are
			// assuming the WDT hardware is fully programmed at this point. And it ought to be as this
			// task cannot even exist without the WDT task having been already created based upon their
			// order of creation in the startup task.
			//
			// 2/21/2014 rmd : Note - The watchdog timeout period is 8 seconds.
			wdt->counter_value = 0;

			// ----------

			// 10/16/2012 rmd : To conserve the charge in our caps turn off any unecessary hardware.
			// Such as the backlight. And the devices on the ports.
			BACKLIGHT_OFF;
			
			// 11/14/2016 rmd : Because this task may fire BEFORE the system is fully initialized
			// (certain dirty power brown out scenarios), do not rely upon the configuration device
			// index and attempt a call to the formal device power control funciton. All interface cards
			// have been designed so that driving the ENABLE line low turns off the device interface
			// board power supply. If there is a board and a supply. Reminder: some boards don't have a
			// supply.
			PORT_A_ENABLE_LOW;
			
			PORT_B_ENABLE_LOW;
			
			// ----------

			// 6/3/2015 rmd : Update the accumulators otherwise we will lose the DAILY accumulation
			// values. A restart is for sure going to happen, whether that be from a power fail or a
			// code receipt, or other restart reason, we come through here. We are only interested in
			// the accumulators for UPORT_A since the point of the accumulators is to measure the
			// traffic on the GR account (for billing purposes), so only do port A. And we can do it
			// regardless of device. When we make the alert lines we only show alerts if there is a GR.
			SERIAL_add_daily_stats_to_monthly_battery_backed( UPORT_A );
			
			// ----------

			// 10/17/2012 rmd : SUSPEND ALL the tasks except the IDLE task, this task, and the
			// FLASH_STORAGE task! Don't forget about the tasks created before our normal task creation
			// loop. Such as the timer_task, the epson_rtc task, and the wdt task. The app_startup task
			// is handled very differently. It suspends itself by frequently checking the power fail
			// flag. See that task for more information. And I don't have a good reason to suspend the
			// idle task.
			
			if( timer_task_handle != NULL)
			{
				// 10/17/2012 rmd : Should we delete or suspend? I vote to suspend. I think it it faster to
				// suspend a task than delete it. Plus we don't have to think about the idle task calling
				// mem_free (which may present a problem with the impending pwr fail).
				vTaskSuspend( timer_task_handle );
			}

			if( epson_rtc_task_handle != NULL)
			{
				// 10/17/2012 rmd : Should we delete or suspend? I vote to suspend. I think it it faster to
				// suspend a task than delete it. Plus we don't have to think about the idle task calling
				// mem_free (which may present a problem with the impending pwr fail).
				vTaskSuspend( epson_rtc_task_handle );
			}

			if( wdt_task_handle != NULL)
			{
				// 10/17/2012 rmd : This may seem awfully brave. However if this task exists the wdt
				// hardware has been programmed and started. So stopping the wdt task here should be
				// perfectly fine to do.
				vTaskSuspend( wdt_task_handle );
			}
			 
			// ----------

			UNS_32	i;

			// Suspend all the other tasks except the flash storage tasks.
			for( i=0; i<NUMBER_OF_TABLE_TASKS; i++ )
			{
				if( created_task_handles[ i ] != NULL )
				{
					if( (created_task_handles[ i ] != flash_storage_0_task_handle) && (created_task_handles[ i ] != flash_storage_1_task_handle) )
					{
						// 10/17/2012 rmd : Should we delete or suspend? I vote to suspend. I think it it faster to
						// suspend a task than delete it. Plus we don't have to think about the idle task calling
						// mem_free (which may present a problem with the impending pwr fail).
						vTaskSuspend( created_task_handles[ i ] );
					}

				}

			} 
			 
			// ----------

			// So now all that is running is the Flash Storage Task, IDLE, and this task. In some cases
			// the app_startup task may still be running too ... but not for long. That task will
			// shortly suspend itself.
			 
			// ----------

			// INDICATE THERE IS AN IMPENDING PWR FAIL SO THE FLASH STORAGE TASKS WILL EXIT OUT OF ANY
			// ACTIVITY THEY WERE IN. AS WE NOW KNOW THIS IS CRITICAL WHEN FLASH_0 IS IN THE MIDDLE OF A
			// WRITE PROCESS AS THE PART WILL BE IN THE AAI MODE WHERE WE CANNOT USE IT TO BOOT!!!!!!
			//
			// 2/21/2014 rmd : Set the flag that any remaining running tasks can use to gracefully exit
			// what they are doing. Such as the flash task. Or the startup task.
			restart_info.SHUTDOWN_impending = (true);

			// Capture when this was detected so we can build an alert line upon restart.
			EPSON_obtain_latest_time_and_date( &(restart_info.shutdown_td) );
			
			// ----------

			// 2/21/2014 rmd : And capture the reason so when we restart we can generate an accurate
			// alert line reflecting the reason for the restart.
			restart_info.shutdown_reason = sseqs.event;
			
			// ----------

			// 30 usec (or so - measured 20 usec) after the powerfail interrupt we are HERE!
			//
			// NOW we want the flash_storage_task to run. It is the ONLY other task left alive.
			// Well that's not true. The IDLE task is still active!
			//
			// WATCHDOG: the watchdog timeout should be set to about 100ms at this point. And is used to
			// cover the issue of a brown out where the power fluctuation does indeed trip the powerfail
			// interrupt. But the power recovers and the reset line is never activated by the MAXIM
			// chip.
			//
			// How much time do we have during a normal powerfail till reset is slammed? Well that
			// depends on the power supply loading. This changes based on the communication peripherals,
			// and the CPU work load. And gees louise don't forget the 5VDC goes down to the motherboard
			// and is sucked on. So a hard number just doesn't exist. A reasonable guess is 30ms though.
			//
			// The vTaskDelay approach may not be the best but will achieve providing the FLASH_STORAGE
			// task with nearly all of the remaining CPU time.

			// ----------
			
			// 10/19/2012 rmd : Under a normal power fail condition the power is dropping rapidly at
			// this point. And has likely less than 100ms left before the MAXIM generated hard reset
			// kicks. When it does the restart will recognize this condition as a 'clean' power
			// failure. If the hard reset never occurs that means the power came back. So what we had
			// was a momentary dip in the line. In that case we have to cause the reset ourselves. So
			// what we do is wait here for 300ms. If the hard reset hasn't occurred by then we will
			// force one ourselves using the WDT. And this condition is recognized upon the restart as a
			// BROWN_OUT.
			for( i=0; i<30; i++ )
			{
				// 10/19/2012 rmd : During the delay we are either executing the startup_task till it
				// suspends itself. Or we are exiting from any flash work that was under way. If neither of
				// those two we are sitting in the IDLE task.
				vTaskDelay( MS_to_TICKS( 10 ) );
			}
			
			// ----------

			// 10/18/2012 rmd : Set the wdt counter to the match value - 2. It will trip a match in two
			// (or is that one to two?) PERIPHERAL CLOCK periods. That is a 13MHz clock by the way. I
			// choose the -2 to avoid any funny business about the write and such a short period of time
			// before the WDT match occurs. I dunno this just seemed safer.
			wdt->counter_value = ((wdt->match_value) - 2);
			
			// 10/17/2012 rmd : And now wait. The WDT generated reset will be along in a very short
			// while - no more than two 13MHz clock periods. The net result if for reasons other than a
			// true powerfail - meaning an application requested restart - we achieve the restart is
			// about 300ms from the call to the SYSTEM_application_requested function.
			for( ;; );
			
			// ----------
		}

		// ----------
		
		// 10/24/2012 rmd : Keep the task activity marker updated.
		pwrfail_task_last_x_stamp =  xTaskGetTickCount();
		
		// ----------
		
	}  // of the task itself
	
}

/* ---------------------------------------------------------- */
extern void wdt_monitoring_task( void *pvParameters )
{
	// 10/15/2012 rmd : First before anything enable the clock to the WDT peripheral block.
	// Otherwise I don't think even writes to the registers will work.
	clkpwr_clk_en_dis( CLKPWR_WDOG_CLK, 1 );

	// ----------
	
	// 10/15/2012 rmd : Initialize the WDT hardware.
	WDT_REGS_T	*wdt;
	
	wdt = ((WDT_REGS_T*)WDTIM_BASE);
	
	// ----------

	// 10/16/2012 rmd : Now initialize the WDT counter. ha

	// 10/15/2012 rmd : Going to initialize the WDT hardware. Coming out of hardware RESET is
	// the only way we're executing this code here (normally ... I suppose anything is possible
	// with an error or other anomaly).
	
	// 10/15/2012 rmd : Following reset the wdt is supposed to be disabled. HOWEVER the data
	// sheet is not clear about this. In the data sheet the attained reset value for the enable
	// bit is strangely blank. So we make a point to disable here.
	wdt->main_control = 0;
	
	// 10/24/2012 rmd : During the startup phase there is a lot going on. This task runs till
	// its 500ms delay, returns control to the app_startup task, and at that point the priority
	// of this task gets pushed down to the lowest priority. That means by intention everything
	// else will run FIRST. Including potentially a great deal of flash file writes which take a
	// long time. Therefore we make the initial timeout period 10 seconds. When this task
	// finally does run we will then tighten the wdt timeout period down to the intended period.
	wdt->match_value = ( 10 * clkpwr_get_base_clock_rate( CLKPWR_PERIPH_CLK ) );
	
	// 10/16/2012 rmd : Set up to generate both the internal chip reset and an external reset.
	// Also set for the WDT to stop on match? It seems it may continue to be active AFTER a
	// match detect? Hmmm. I don't think we want that. Will we have a race between the startup
	// getting back to this point in the code and another WDT timeout? I don't think so because
	// there are other bits I am sure are not setup making the WDT non-operational post reset.
	// Even post a WDT reset. So these bits (bits 0,1, and 2 that STOP, RESET, and generate an
	// INT) must be more geared for when this peripheral block is used as a general purpose
	// timer.
	wdt->match_control = ( WDT_M_RES2 | WDT_STOP_ON_MATCH );

	// 10/15/2012 rmd : The maximum value in this register is 0xFFFF. With a 13MHz PERIPHERAL
	// CLOCK the widest reset pulse we get is 5.04ms. Note: This register must be written to
	// else the WDT block will not operate.
	wdt->reset_pulse_length = 0xFFFF;

	// 10/15/2012 rmd : Set up so that the match output is active upon a match.
	wdt->external_match_control = ( 0x20 );
	
	// 10/15/2012 rmd : Start the WDT up. Allowing the debugger to pause the count. So that when
	// the debugger takes a hold we don't generate a watchdog initiated reset.
	wdt->main_control = (WDT_PAUSE_ENABLE | WDT_COUNT_ENABLE);

	// ----------
	
	// 10/23/2012 rmd : This watchdog task starts at the highest priority possible. Ensuring it
	// runs till blocked. Initialize the activity time stamps here before entering the forever
	// task loop.
	rtc_task_last_x_stamp =  xTaskGetTickCount();

	pwrfail_task_last_x_stamp =  xTaskGetTickCount();

	UNS_32	i;
	
	for( i=0; i<NUMBER_OF_TABLE_TASKS; i++ )
	{
		task_last_execution_stamp[ i ] = xTaskGetTickCount();
	}
	
	// ----------
	
	// 10/24/2012 rmd : We need to postpone the checking of tasks until after the startup has
	// complete and task are running normally. During startup there is alot of residual task
	// activity (file write, usb recognition) that cause some temporary task starvation. Now we
	// have a 10 second watchdog hovering over this so if things don't settle out within that
	// window the watchdog will strike.
	#define	UNBLOCK_BEFORE_ACTIVE	(6)

	UNS_32	unblock_count;

	unblock_count = 0;
	
	// ----------

	static UNS_32	last_x_stamp, greatest_diff_ms;

	UNS_32	now_stamp;

	last_x_stamp  = xTaskGetTickCount();
	
	greatest_diff_ms = 0;
	
	// ----------
	
	while( (true) )
	{
		// 2/21/2014 rmd : This task has a priority of 1 (incidentally the flash write tasks also
		// have a priority of 1). At one the only task lower is the IDLE task.
	
		// ----------
	
		vTaskDelay( MS_to_TICKS( 500 ) );
	
		// ----------
	
		// 3/12/2013 rmd : The tick count rate in this application is 200 hertz. Which means it will
		// roll over each 240 some days. Unless the code is restarted within that period.
		now_stamp = xTaskGetTickCount();
	
		// ----------
	
		// 4/10/2013 rmd : In the case of roll-over lets not create an alarming alert line. So test
		// the stamp relative values make sense first.
		if( now_stamp > last_x_stamp )
		{
			if( (now_stamp - last_x_stamp) > MS_to_TICKS( greatest_diff_ms ) )
			{
				greatest_diff_ms = ((now_stamp - last_x_stamp) * (portTICK_RATE_MS) );
	
				// 11/4/2016 rmd : The WDT timer task is the lowest priority task. That's by design. It is
				// interesting to note any extreme delays in getting to this task. What is extreme? Well
				// I'll use a 250 ms window with no opportunity to run this task. Under normal conditions we
				// should be running once every 500 ms (plus 5 to 20 ms).
				if( greatest_diff_ms > 750 )
				{
					Alert_Message_va( "New Monitoring Delta: %u ms", greatest_diff_ms );
				}
			}
		}
		
		last_x_stamp = now_stamp;
		
		// ----------

		if( unblock_count < UNBLOCK_BEFORE_ACTIVE )
		{
			unblock_count += 1;
			
			if( unblock_count == UNBLOCK_BEFORE_ACTIVE )
			{
				// 10/25/2012 rmd : I have seen the greatest_diff_ms grow to around 5000ms. That means yes
				// this task was starved for that long. Generally during program starts for all 768
				// stations. Between TD_CHECK, COMM_MNGR, and the FLASH_STORAGE activities we are busy. It
				// doesn't mean the controller is frozen. The relative task priorities keep all tasks moving
				// along and none of them fail their 1000ms activity test. However this task is left high
				// and dry for a while. Because of that we need to set the watchdog period to more than 5
				// seconds. Like say 8 seconds.
				wdt->match_value = ( 8 * clkpwr_get_base_clock_rate( CLKPWR_PERIPH_CLK ) );
			}
		}
	
		// ----------
	
		BOOL_32	care_for_the_dog;
		
		care_for_the_dog = (true);
	
		// ----------
	
		if( unblock_count >= UNBLOCK_BEFORE_ACTIVE )
		{
			// 10/24/2012 rmd : Here is an interesting dilemma. This wdt task is the lowest priority
			// task. So when we grab the present tick count (x_stamp) it is quite possible another task
			// goes ready and executes itself till blocking (say a comm_mngr message comes in). Well
			// then that task has updated its task_last_execution_stamp. And it may be a newer number
			// (ie greater than our x_stamp value). Well that sure is assurance that the task has
			// executed. And we don't need to check that task. FURTHER more if we did check that task
			// the math all being unsigned would fail. For example 1211-1218 is a VERY big number. So we
			// MUST avoid even performing that math. How do I know all this? I HAVE SEEN IT HAPPEN!
			//
			// 10/25/2012 rmd : Further more what we have two options here. Suspend the scheduler so
			// that between "the test if the task_last_execution_stamp is less than our x_stamp" AND
			// "the actual delta comparison" task_least_execution_stamp cannot change. OR take a copy
			// and work with the copy. Taking a copy is an atomic operation so is safe. And to avoid
			// suspending the scheduler I've decided to take a copy of the tasks last x stamp and work
			// with that.
			UNS_32	task_x_stamp;
			
			// ----------

			for( i=0; i<NUMBER_OF_TABLE_TASKS; i++ )
			{
				// 10/23/2012 rmd : Were we to make the task, and has it been made, and are we to monitor
				// it.
				if( Task_Table[ i ].bCreateTask )
				{
					if( created_task_handles[ i ] != NULL )
					{
						if( Task_Table[ i ].include_in_wdt == WATCHDOG_MONITORS )
						{
							// 10/25/2012 rmd : Atomic copy. Safe.
							task_x_stamp = task_last_execution_stamp[ i ];
							
							// 3/12/2013 rmd : Watch for the tick count roll-over (it happens every 248 some days) or
							// the phenomenon I mentioned above where the task runs between the capturing of the
							// now_stamp and this test here. Which does happen.
							if( now_stamp >= task_x_stamp )
							{
								// 10/23/2012 rmd : If it's been more than the limit we've set for this task we've got a
								// problem.
								if( (now_stamp - task_x_stamp) > MS_to_TICKS( Task_Table[ i ].execution_limit_ms ) )
								{
									// 10/23/2012 rmd : Alert about it.
									Alert_task_frozen( (char*)pcTaskGetTaskName( created_task_handles[ i ] ), ((now_stamp - task_x_stamp) * portTICK_RATE_MS) );
									
									// ----------
									
									// 10/23/2012 rmd : Let the watchdog expire.
									care_for_the_dog = (false);
								}
				
							}
							else
							{
								// 3/12/2013 rmd : If the now_stamp is LESS THAN the task time stamp the OS tick count
								// MUST HAVE ROLLED over. This will happen once every 248 days. In this case just start the
								// task stamps over setting them equal to the present tick count. Missing this check once
								// each roll-over is okay. If the task is being starved that will show soon enough.
								//
								// Normally the task_last_execution_stamp is 'stamped' within the respective task itself
								// when the task unblocks. In this case we are reinitializing the last_execution_stamps.
								//
								// 2/21/2014 rmd : EVEN if we ended up here for the other phenomenon where the task ran
								// between taking the now_stamp and the mathematical test resetting the task time stamp like
								// this is just fine.
								task_last_execution_stamp[ i ] = now_stamp;
							}
							
						}
						
					}
					else
					{
						// 3/12/2013 rmd : If it was to be created it better have a non-NULL handle!
						//
						// 9/4/2018 rmd : UPDATE - we had an error somwhat early during app startup execution that
						// end up with a dead-lock condition between the appstartup task and the flash storage task
						// both vying for the same mutex. Well this error occurred early enough that the task table
						// tasks were not yet made, so every 500ms we would get one of these alert lines for each
						// task in the task table - like 20 of them! Flooding the alerts, when this isn't really
						// actually an error. So I've decided to suppress this alert.
						//
						//Alert_Message_va( "Unexplained NULL handle for %s", Task_Table[ i ].TaskName );
					}
					
				}
	
			}
			
			// ----------

			// 3/12/2013 rmd : And now cover the rtc task which is not part of the startup task table.
			
			// 10/25/2012 rmd : Atomic copy. Safe.
			task_x_stamp = rtc_task_last_x_stamp;
			
			// 3/12/2013 rmd : Watch for the tick count roll-over. It happens every 248 some days.
			if( now_stamp >= task_x_stamp )
			{
				// 10/23/2012 rmd : If it's been more than 1 second since the task ran we've got a problem.
				if( (now_stamp - task_x_stamp) > MS_to_TICKS( 1000 ) )
				{
					// 10/23/2012 rmd : Alert about it.
					Alert_task_frozen( (char*)pcTaskGetTaskName( epson_rtc_task_handle ), ((now_stamp - task_x_stamp) * portTICK_RATE_MS) );
					
					// ----------
					
					// 10/23/2012 rmd : Let the watchdog expire.
					care_for_the_dog = (false);
				}
			}
			else
			{
				rtc_task_last_x_stamp = now_stamp;
			}
			
			// ----------

			// 3/12/2013 rmd : And now cover the pwr_fail task which is not part of the startup task
			// table.
			
			// 10/25/2012 rmd : Atomic copy. Safe.
			task_x_stamp = pwrfail_task_last_x_stamp;
			
			// 3/12/2013 rmd : Watch for the tick count roll-over. It happens every 248 some days.
			if( now_stamp >= task_x_stamp )
			{
				// 10/23/2012 rmd : If it's been more than 1 second since the task ran we've got a problem.
				if( (now_stamp - task_x_stamp) > MS_to_TICKS( 1000 ) )
				{
					// 10/23/2012 rmd : Alert about it.
					Alert_task_frozen( (char*)pcTaskGetTaskName( powerfail_task_handle ), ((now_stamp - task_x_stamp) * portTICK_RATE_MS) );
					
					// ----------
					
					// 10/23/2012 rmd : Let the watchdog expire.
					care_for_the_dog = (false);
				}
			}
			else
			{
				pwrfail_task_last_x_stamp = now_stamp;
			}

		}
	
		// ----------
	
		if( care_for_the_dog )
		{
			// 10/16/2012 rmd : Restarts the count to avoid a match.
			wdt->counter_value = 0;
		}
	
		// ----------
	
	}  // of the task itself
	
}

/* ---------------------------------------------------------- */
/*
extern void WATCHDOG_clear_the_dog( void )
{
	// 4/22/2015 rmd : A function that may be called from any task. To reset the watchdog
	// timeout. Good gravy this should not ever be needed. An indication of poor code design.

	WDT_REGS_T	*wdt;
	
	wdt = ((WDT_REGS_T*)WDTIM_BASE);

	// 4/22/2015 rmd : Restarts the count to avoid a match.
	wdt->counter_value = 0;
}
*/
	
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*
void pop_registers_from_fault_stack(unsigned int * hardfault_args)
{
unsigned int stacked_r0;
unsigned int stacked_r1;
unsigned int stacked_r2;
unsigned int stacked_r3;
unsigned int stacked_r12;
unsigned int stacked_lr;
unsigned int stacked_pc;
unsigned int stacked_psr;

	stacked_r0 = ((unsigned long) hardfault_args[0]);
	stacked_r1 = ((unsigned long) hardfault_args[1]);
	stacked_r2 = ((unsigned long) hardfault_args[2]);
	stacked_r3 = ((unsigned long) hardfault_args[3]);

	stacked_r12 = ((unsigned long) hardfault_args[4]);
	stacked_lr = ((unsigned long) hardfault_args[5]);
	stacked_pc = ((unsigned long) hardfault_args[6]);
	stacked_psr = ((unsigned long) hardfault_args[7]);

    // Inspect stacked_pc to locate the offending instruction.
	for( ;; );
}
*/

/* ---------------------------------------------------------- */
// This code just sets up everything ready, then jumps to a function called
// pop_registers_from_fault_stack - which is defined below.
/*
extern void data_abort_handler( void ) __attribute__ ((naked));
extern void data_abort_handler( void )
{
	__asm volatile
	(
		" tst lr, #4										\n"
		" ite eq											\n"
		" mrseq r0, msp										\n"
		" mrsne r0, psp										\n"
		" ldr r1, [r0, #24]									\n"
		" ldr r2, handler2_address_const					\n"
		" bx r2												\n"
		" handler2_address_const: .word pop_registers_from_fault_stack	\n"
	);
}
*/

/* ---------------------------------------------------------- */
/**
 * Falls into an endless loop to trigger a watchdog timeout. This is useful for
 * debugging as it allows the developer to set a single breakpoint and all calls
 * to either this function or the FREEZE_and_RESTART_APP macro will pause the
 * debugger at which point the Call Stack can be used to determine how the code
 * fell into this routine.
 * 
 * @author 2/27/2013 Adrianusv
 *
 * @revisions (none)
 */
extern void freeze_and_restart_app( void )
{
	for(;;);
}

/* ---------------------------------------------------------- */
void undefined_instruction_handler(void) __attribute__ ((naked));
void undefined_instruction_handler(void)
{
	register UNS_32		*lnk_ptr;

	__asm__ __volatile__
	(
		"sub lr, lr, #8\n"
		"mov %0, lr" : "=r" (lnk_ptr)
	);
	
	// ----------

	// 10/23/2012 rmd : Though we could probably just go ahead and use the regular obtain time &
	// date function. And we could go ahead and write an alert line here. We really do not know
	// where in the code this exception occurred. For example if we were writing to the alert
	// pile and an exception occurred (even while in the middle of writing an alert a task
	// switch to another task that triggered the exception) then if we tried to write an alert
	// here we would likely crash the pile. The safest thing to do is record here and then on
	// the startup generate the appropriate alert lines.
	
	// ----------

	restart_info.exception_noted = (true);
	
	EXCEPTION_USE_ONLY_obtain_latest_time_and_date( &(restart_info.exception_td) );
	
	/* On data abort exception the LR points to PC+8 */
	// Exception at this address showing the instruction at that address.
	snprintf( restart_info.exception_description_string, sizeof(restart_info.exception_description_string), "Undef Instr at 0x%08lX 0x%08lX", lnk_ptr, *(lnk_ptr) );

	snprintf( restart_info.exception_current_task, sizeof(restart_info.exception_current_task), "%s", pcTaskGetTaskName(NULL) );
	
	// ----------

	// 10/22/2012 rmd : Endless ... this will freeze this task we were in (yes there is ALWAYS a
	// current task) and cause the wdt task to fire. If the watchdog is not running (hardware
	// uninitialized) I would assume the developer is using the debugger so when he pauses his
	// program he'll be at this line.
	for(;;);
}

/* ---------------------------------------------------------- */
void prefetch_abort_handler(void) __attribute__ ((naked));
void prefetch_abort_handler(void)
{
	register UNS_32		*lnk_ptr;

	__asm__ __volatile__
	(
		"sub lr, lr, #8\n"
		"mov %0, lr" : "=r" (lnk_ptr)
	);
	
	// ----------

	// 10/23/2012 rmd : Though we could probably just go ahead and use the regular obtain time &
	// date function. And we could go ahead and write an alert line here. We really do not know
	// where in the code this exception occurred. For example if we were writing to the alert
	// pile and an exception occurred (even while in the middle of writing an alert a task
	// switch to another task that triggered the exception) then if we tried to write an alert
	// here we would likely crash the pile. The safest thing to do is record here and then on
	// the startup generate the appropriate alert lines.
	
	// ----------

	restart_info.exception_noted = (true);
	
	EXCEPTION_USE_ONLY_obtain_latest_time_and_date( &(restart_info.exception_td) );
	
	/* On data abort exception the LR points to PC+8 */
	// Exception at this address showing the instruction at that address.
	snprintf( restart_info.exception_description_string, sizeof(restart_info.exception_description_string), "Prefetch Abort at 0x%08lX 0x%08lX", lnk_ptr, *(lnk_ptr) );

	snprintf( restart_info.exception_current_task, sizeof(restart_info.exception_current_task), "%s", pcTaskGetTaskName(NULL) );
	
	// ----------

	// 10/22/2012 rmd : Endless ... this will freeze this task we were in (yes there is ALWAYS a
	// current task) and cause the wdt task to fire. If the watchdog is not running (hardware
	// uninitialized) I would assume the developer is using the debugger so when he pauses his
	// program he'll be at this line.
	for(;;);
}

/* ---------------------------------------------------------- */
void data_abort_handler(void) __attribute__ ((naked));
void data_abort_handler(void)
{
	register UNS_32		*lnk_ptr;
	
	__asm__ __volatile__
	(
		"sub lr, lr, #8\n"
		"mov %0, lr" : "=r" (lnk_ptr)
	);
	
	// ----------

	// 10/23/2012 rmd : Though we could probably just go ahead and use the regular obtain time &
	// date function. And we could go ahead and write an alert line here. We really do not know
	// where in the code this exception occurred. For example if we were writing to the alert
	// pile and an exception occurred (even while in the middle of writing an alert a task
	// switch to another task that triggered the exception) then if we tried to write an alert
	// here we would likely crash the pile. The safest thing to do is record here and then on
	// the startup generate the appropriate alert lines.
	
	// ----------

	restart_info.exception_noted = (true);
	
	EXCEPTION_USE_ONLY_obtain_latest_time_and_date( &(restart_info.exception_td) );
	
	/* On data abort exception the LR points to PC+8 */
	// Exception at this address showing the instruction at that address.
	snprintf( restart_info.exception_description_string, sizeof(restart_info.exception_description_string), "Data Abort at 0x%08lX 0x%08lX", lnk_ptr, *(lnk_ptr) );

	snprintf( restart_info.exception_current_task, sizeof(restart_info.exception_current_task), "%s", pcTaskGetTaskName(NULL) );
	
	// ----------

	// 10/22/2012 rmd : Endless ... this will freeze this task we were in (yes there is ALWAYS a
	// current task) and cause the wdt task to fire. If the watchdog is not running (hardware
	// uninitialized) I would assume the developer is using the debugger so when he pauses his
	// program he'll be at this line.
	for(;;);
}

/*-----------------------------------------------------------*/

// 10/23/2012 rmd : During debug you should always implement this function. Assert is your
// friend! And beyond that i.e. release code it is also reasonable to continue the use of
// assert. As long as it is not a nuisance.
void __assert( const char *__expression, const char *__filename, int __line )
{
	// ----------

	restart_info.assert_noted = (true);
	
	EXCEPTION_USE_ONLY_obtain_latest_time_and_date( &(restart_info.assert_td) );
	
	snprintf( restart_info.assert_description_string,  sizeof(restart_info.assert_description_string), "ASSERT: %s, %s, %d", __expression,  CS_FILE_NAME( (char*)__filename ),  __line );

	snprintf( restart_info.assert_current_task, sizeof(restart_info.assert_current_task), "%s", pcTaskGetTaskName(NULL) );
	
	// ----------

	// 10/22/2012 rmd : Endless ... this will freeze this task we were in (yes there is ALWAYS a
	// current task) and cause the wdt task to fire. If the watchdog is not running (hardware
	// uninitialized) I would assume the developer is using the debugger so when he pauses his
	// program he'll be at this line.
	for(;;);
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

