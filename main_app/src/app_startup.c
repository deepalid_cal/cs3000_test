// file = app_startup.c                      01.24.2011  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memset
#include	<string.h>

#include	"app_startup.h"

#include	"configuration_controller.h"

#include	"configuration_network.h"

#include	"serport_drvr.h"

#include	"rcvd_data.h"

#include	"contrast_and_speaker_vol.h"

#include	"lcd_init.h"

#include	"k_process.h"

#include	"key_scanner.h"

#include	"speaker.h"

#include	"epson_rx_8025sa.h"

#include	"wdt_and_powerfail.h"

#include	"flash_storage.h"

#include	"spi_flash_driver.h"

#include	"td_check.h"

#include	"gpio_setup.h"

#include	"alerts.h"

#include	"r_alerts.h"

#include	"stations.h"

#include	"tpl_in.h"

#include	"tpl_out.h"

#include	"comm_mngr.h"

#include	"foal_comm.h"

#include	"foal_irri.h"

#include	"foal_flow.h"

#include	"irri_comm.h"

#include	"irri_irri.h"

#include	"irri_lights.h"

#include	"irri_flow.h"

#include	"packet_router.h"

#include	"battery_backed_vars.h"

#include	"irrigation_system.h"

#include	"poc.h"

#include	"lights.h"

#include	"d_process.h"

#include	"td_check.h"

#include	"budget_report_data.h"

#include	"poc_report_data.h"

#include	"lights_report_data.h"

#include	"system_report_data.h"

#include	"tpmicro_data.h"

#include	"trcUser.h"

#include	"weather_control.h"

#include	"manual_programs.h"

#include	"station_groups.h"

#include	"controller_initiated.h"

#include	"background_calculations.h"

#include	"flowsense.h"

#include	"bypass_operation.h"

#include	"device_GENERIC_GR_card.h"

#include	"moisture_sensors.h"

#include	"code_distribution_task.h"

#include	"radio_test.h"

#include	"walk_thru.h"

#include	"ftimes_task.h"

#include	"chain_sync_vars.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Because we don't have a main.h proto this here.
void vled_TASK( void *pvParameters );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
////////////////////////////////
// System Semaphore Handles
////////////////////////////////

#ifndef NO_DEBUG_PRINTF
	xSemaphoreHandle	debugio_MUTEX;
#endif

// 7/17/2012 rmd : A semaphore used to manage completion of the EPSON RTC task to a certain
// point before allowing the startup task to proceed.
xSemaphoreHandle	startup_rtc_task_sync_semaphore;
	

xSemaphoreHandle	alerts_pile_recursive_MUTEX;


// Handle to the DF Storage Manager MUTEX
xSemaphoreHandle	Flash_0_MUTEX;

xSemaphoreHandle	Flash_1_MUTEX;


// These semaphores signal the ring buffer task to go ahead and take a look as some data has come in.
xSemaphoreHandle	rcvd_data_binary_semaphore[ UPORT_TOTAL_PHYSICAL_PORTS ];


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// The list mutex. Used to protect all operation to each list. From simultaneous
// manipulation by another task. We decided to individually protect the list operations for
// each list with it's own mutex. Originally the list functions themselves contained calls
// to suspend all and resume all. However for big loops that traverse through a list (of
// which we have many) that form of protection is in adequate. As outside of the actual list
// operations but within the major loop traversing the list a preemption could occur. And
// another task could modify the very list we were moving through. So an external mutex had
// to be introduced. What followed was to then remove the OS suspend and resume protection
// within the actual list API functions.
//
// This is all the list mutexes. For lists that need protecting. There may be lists that
// exist and are ONLY manipulated by a single task. In which case they may not need
// protection. It may be easier however to always use the mutex. For consistancy sake.

// 2011.09.01 rmd : A mutex that is intended to protect all the lists that contain the user
// editable data. Hence the name 'program_data'. The lists contain more than just directly
// editable user data. For example the stations_info list contains all the GID's for the
// other settings that station uses. It is a recursive mutex to ease the use of it
// throughout a function heirachy. This MUTEX will protect many list. Perhaps more than 20.
xSemaphoreHandle	list_program_data_recursive_MUTEX;



xSemaphoreHandle	list_tpl_in_messages_MUTEX;

xSemaphoreHandle	list_tpl_out_messages_MUTEX;



xSemaphoreHandle	list_incoming_messages_recursive_MUTEX;


xSemaphoreHandle	list_packets_waiting_for_token_MUTEX;



xSemaphoreHandle	xmit_list_MUTEX[ UPORT_TOTAL_PHYSICAL_PORTS ];


// 3/17/2014 rmd : This MUTEX is used to allow either the comm_mngr or the controller
// initiated task to perform activities (such as device programming) with the port A and
// port B devices. Activites that must be mutually exclusive. What this MUTEX does is to
// assure the two tasks cannot both independently and simultaneously start a serial port
// activity. The MUTEX is taken while the activity is started and then released. If the
// other task tries to start one of the managed activites it will see via a state or mode
// variable that it is not allowed to be started.
xSemaphoreHandle	ci_and_comm_mngr_activity_recursive_MUTEX;


// 2012.02.07 rmd : This MUTEX protects the irri_irri structure. Which contains the irri
// side irrigation list as well as the irri SXRU list. And several other additional
// variables. All protected with this one MUTEX.
xSemaphoreHandle	irri_irri_recursive_MUTEX;

// 2011.08.20 rmd : Again a single MUTEX to protect multiple lists. This time we are
// protecting 5 lists. They are the IrrigationList, the ONList, the StationsONByController
// lists (all 12 of them), the XferList, and the Flow_Check_List.
//
// And the use of the recursive mutex is to facilitate the Taking and Giving when the call
// structure of several functions requiring list protection becomes complicated and deep.
xSemaphoreHandle	list_foal_irri_recursive_MUTEX;


// 10/29/2018 rmd : To protect both the comm_mngr structure and the chain_members
// structure.
xSemaphoreHandle	comm_mngr_recursive_MUTEX;

// 2011.11.01 rmd : This mutex is used to protect the battery backed record of what the
// chain looks like. It is referenced from several task contexts. The comm_mngr generally
// changes it while TD_CHECK and other look at it. And the TD_CHECK task expects the
// structure to be stable until it is done with certain activites.
xSemaphoreHandle	chain_members_recursive_MUTEX;

// 5/1/2014 rmd : Introduced to protect the send_box_configuration_to_master variable. Which
// can be changed via the keypad (controller name in config_c) or via the comm_mngr when a
// tpmicro message is recvd with the what's installed.
xSemaphoreHandle	irri_comm_recursive_MUTEX;



// 9/4/2012 rmd : To protect the system_report_data_completed_records.
xSemaphoreHandle	system_report_completed_records_recursive_MUTEX;

// 2011.09.23 rmd : To protect the poc_report_data_completed_records.
xSemaphoreHandle	poc_report_completed_records_recursive_MUTEX;

// 2011.09.23 rmd : To protect the station_report_data_completed_records.
xSemaphoreHandle	station_report_data_completed_records_recursive_MUTEX;

// 2011.09.23 rmd : To protect the station_history_completed_records.
xSemaphoreHandle	station_history_completed_records_recursive_MUTEX;

// 2011.09.23 rmd : To protect the weather preserves data.
xSemaphoreHandle	weather_preserves_recursive_MUTEX;

// 2011.09.23 rmd : To protect the station preserves array data.
xSemaphoreHandle	station_preserves_recursive_MUTEX;

// 2011.11.08 rmd : To protect the system preserves array data.
xSemaphoreHandle	system_preserves_recursive_MUTEX;

// 2012.01.05 rmd : To protect the poc preserves array data.
xSemaphoreHandle	poc_preserves_recursive_MUTEX;

// 2012.02.08 ajv : To protect the irrigation system list and associated data.
xSemaphoreHandle	list_system_recursive_MUTEX;

// 2012.02.08 ajv : To protect the POC list and associated data.
xSemaphoreHandle	list_poc_recursive_MUTEX;

// 9/13/2012 rmd : To specifically protect the weather control variables.
xSemaphoreHandle	weather_control_recursive_MUTEX;

// 9/13/2012 rmd : To protect the et and rain tables during access of value and rolling.
xSemaphoreHandle	weather_tables_recursive_MUTEX;

// 8/2/2012 rmd : There was some concern that when direct access is introduced it may be
// implemented in such a way that two tasks of dirrerent priorities have the ability to
// generate key beeps. Say if the communications and key process were able to
// simulataneously switch screens. Not sure. But introduced this simple MUTEX mechanism to
// guard the speaker hardware from re-entrancy. Especially concerning as it diddles low
// level clock and counter registers. Note the speaker hardware MUTEX is not recursive. No
// need.
xSemaphoreHandle	speaker_hardware_MUTEX;

// 8/3/2012 rmd : Introduced to protect the 'tpmicro_data' structure from multiple tasks.
xSemaphoreHandle	tpmicro_data_recursive_MUTEX;


// 5/1/2015 ajv : Introduced to protect the
// weather_preserves.pending_changes_to_send_to_comm_server variable. This is used to
// prevent the controller from accepting Program Data changes from the Comm Server if there
// are changes that need to be sent. This is set as part of either the KEY PROCESSING or
// CONTROLLER INITIATED tasks, and cleared as part of the CONTROLLER INITIATED task.
xSemaphoreHandle	pending_changes_recursive_MUTEX;

// 9/9/2015 mpd :  	This mutex protects all instances of the LIGHT_STRUCT. This lights-specific mutex 
// is used in place of "list_program_data_recursive_MUTEX".
xSemaphoreHandle	list_lights_recursive_MUTEX;

// 9/9/2015 mpd :  	This mutex protects the "foal_lights" FOAL_LIGHTS_BATTERY_BACKED_STRUCT.
xSemaphoreHandle	list_foal_lights_recursive_MUTEX;

// 9/9/2015 mpd :  	This mutex protects the "irri_lights" IRRI_LIGHTS.
xSemaphoreHandle	irri_lights_recursive_MUTEX;

// 9/9/2015 mpd :  	This mutex protects the "lights_preserves" LIGHTS_BB_STRUCT.
xSemaphoreHandle	lights_preserves_recursive_MUTEX;

// 9/9/2015 mpd :  	This mutex protects the "lights_report_data_completed" COMPLETED_LIGHTS_REPORT_DATA_STRUCT as well 
// as all instances of LIGHTS_REPORT_RECORD.
xSemaphoreHandle	lights_report_completed_records_recursive_MUTEX;

// ----------

// 2/4/2016 rmd : I'm going to introduce a MUTEX that is used for all MOISTURE_SENSOR list
// operations.
xSemaphoreHandle	moisture_sensor_items_recursive_MUTEX;

// 2/17/2016 rmd : This MUTEX is focused at the moisture_sensor_recorder records.
xSemaphoreHandle	moisture_sensor_recorder_recursive_MUTEX;

// 2/5/2018 Ryan : This mutex is used to protect WALK_THRU
xSemaphoreHandle	walk_thru_recursive_MUTEX;

// ----------
// 02/22/2016 skc : This mutex protects the COMPLETED_BUDGET_REPORT_RECORDS_STRUCT.
xSemaphoreHandle	budget_report_completed_records_recursive_MUTEX;

// 4/24/2017 rmd : A non-recursive MUTEX for the controller initiated message list queue.
// This is not the task queue. It is not recursive because I did not need it to be and am
// saving the recursive mutex overhead (perhaps).
xSemaphoreHandle	ci_message_list_MUTEX;

xSemaphoreHandle	code_distribution_control_structure_recursive_MUTEX;


xSemaphoreHandle	chain_sync_control_structure_recursive_MUTEX;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/30/2017 rmd : Hub List related. The hub list is a msg rcvd and processed by the
// COMM_MNGR. And then the RING_BUFFER_ANALYSIS TASK uses the list, frequently pull all
// items off the front of the queue, and adding them to the back.

xSemaphoreHandle	router_hub_list_MUTEX;

// 4/30/2017 rmd : We are allowing up to 150 controllers on a hub. Sounds like a good limit
// based upon the installs I have seen. Gotta check with Bill about Rancho Cucumonga though.
//
// 5/1/2017 rmd : Checked with Bill, Rancho has 500 some controllers, and TWO hubs. So the
// maximum number needs to be up there. I'm going to use 512.
//
// 5/1/2017 rmd : The queue takes a single 2K block of memory for the queue items, not 500
// smaller blocks, thank goodness.
#define				ROUTER_HUB_LIST_MAX_QUEUE_ITEMS		(512)

xQueueHandle		router_hub_list_queue;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
////////////////////////////////
// System Queue Handles
////////////////////////////////

xQueueHandle	Contrast_and_Volume_Queue;


xQueueHandle	Key_Scanner_Queue;

xQueueHandle	Key_To_Process_Queue;


xQueueHandle	FLASH_STORAGE_task_queue_0;

xQueueHandle	FLASH_STORAGE_task_queue_1;


#define			DISPLAY_QUEUE_EVENTS_DEPTH  (25)
xQueueHandle	Display_Command_Queue;


// TRANSPORT INCOMING message event queue
xQueueHandle	TPL_IN_event_queue;

// TRANSPORT OUTGOING message event queue
xQueueHandle	TPL_OUT_event_queue;


xQueueHandle	COMM_MNGR_task_queue;

xQueueHandle	CODE_DISTRIBUTION_task_queue;

xQueueHandle	CONTROLLER_INITIATED_task_queue;


xQueueHandle	BYPASS_event_queue;

xQueueHandle	RADIO_TEST_task_queue;


// Background calculation event queue to manage low priority calculations such as the next
// scheduled irrigation to be displayed on the Status screen.
xQueueHandle	Background_Calculation_Queue;


xQueueHandle	FTIMES_task_queue;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Our plan is to ALWAYS swap out the flop registers for all task context switches. The
// alternative would be to pick and choose which tasks contain floating point operations and
// which do not. I find that error prone. The reason I prefer to make all tasks FLOP capable
// is it would be too easy years from now for someone to ADD a floating point operation to a
// task that did not have it's FLOP register array set up. And this error could go
// undetected for a long time. Until there was context switch in the middle of the FLOP
// operation. Then the math could go very wrong if the task it switched into had a FLOP
// register storage array.

// These are the buffers into which the flop registers will be saved.  There is a buffer for
// each task created.  Zeroing out this array is the normal and safe option as this will
// cause the task to start with all zeros in its flop context.

// Because we create THESE tasks outside the task table they need their own FLOP registers.
// Store them in onchip memory as I think that is actually the fastest RAM we have in the
// system. And the swap register storage area is used on each and every context switch.

// Some of the FLOP register definitions are not static cause we wanted reference them
// outside this function.
UNS_32	startup_task_FLOP_registers[ portNO_FLOP_REGISTERS_TO_SAVE ] __attribute__( APPLICATION_IRAM_VARS );

static UNS_32	epson_RTC_task_FLOP_registers[ portNO_FLOP_REGISTERS_TO_SAVE ] __attribute__( APPLICATION_IRAM_VARS );

static UNS_32	powerfail_task_FLOP_registers[ portNO_FLOP_REGISTERS_TO_SAVE ] __attribute__( APPLICATION_IRAM_VARS );

static UNS_32	wdt_task_FLOP_registers[ portNO_FLOP_REGISTERS_TO_SAVE ] __attribute__( APPLICATION_IRAM_VARS );

// The DFP context switch registers for the internal to the RTOS timer task. Timer callback functions may never do any
// floating point operations but with these the stage is set to do so.
UNS_32	timer_task_FLOP_registers[ portNO_FLOP_REGISTERS_TO_SAVE ] __attribute__( APPLICATION_IRAM_VARS );

static UNS_32	task_table_FLOP_registers[ NUMBER_OF_TABLE_TASKS ][ portNO_FLOP_REGISTERS_TO_SAVE ] __attribute__( APPLICATION_IRAM_VARS );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// TASK HANDLES. Why are these important? The power fail task uses them to suspend all
// tasks. Except for the power fail task itself and the FLASH_STORAGE task.

// Don't forget about these THREE which are created outside of the task table. These task
// handles are used within the POWER FAIL task to get at and SUSPEND these tasks.
xTaskHandle		startup_task_handle, epson_rtc_task_handle, powerfail_task_handle, wdt_task_handle;

// And provide a task handle for the internally generated timer task (internal to the OS that is).
xTaskHandle		timer_task_handle;

// 3/10/2017 rmd : And let both flash tasks make a graceful attempt at an activity exit in
// the impending face of a power failure. Need to capture their handles to do this. The shut
// down task suspends almost all tasks except the two flash tasks. UPDATE: we also use the
// general storage task handle 1 during a factory reset to boost the task priority of the
// flash task.
xTaskHandle		flash_storage_0_task_handle;

xTaskHandle		flash_storage_1_task_handle;

// 3/5/2014 rmd : And we need the handles of the COMM_MNGR task, and the TP Port RING_BUFFER
// task, and the TP Port SERIAL DRIVER task. So that we can elevate these task priorites
// during the ISP based tpmicro code update process. The reason we have to do this is some
// display activities (during startup and could be induced via keypad) take so long our
// response timer is timing out. We would flag that as an error and abandon the code upload
// process. For example, at startup when the splash screen is taken down and the status
// screen is drawn, if a code load to the tpmicro was under way it would get an error and
// quick. The result if it could never be succesful. Cause you'd restart and the same thing
// would happen.
static xTaskHandle		comm_mngr_task_handle;

static xTaskHandle		port_tp_ring_buffer_analysis_task_handle;

static xTaskHandle		port_tp_serial_driver_task_handle;

// ----------

// And this array of handle for the task table entries.
xTaskHandle		created_task_handles[ NUMBER_OF_TABLE_TASKS ];


UNS_32			rtc_task_last_x_stamp, pwrfail_task_last_x_stamp __attribute__( APPLICATION_IRAM_VARS );

// 10/23/2012 rmd : To support watchdog functionality. I've decided to keep in IRAM variable
// space. No special reason why.
UNS_32			task_last_execution_stamp[ NUMBER_OF_TABLE_TASKS ] __attribute__( APPLICATION_IRAM_VARS );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
//////////////////////////////////////////////////////////////////////////////
//
// SYSTEM TASK TABLE
//
// Notes:
//
//   1. Setting the first column value to 1 or 0 enables or 
//      disables creation of that task.
//
//   2. For many tasks, the Task Parameter is the UART port used for
//      output or managed by the task. See some specific cases below.
//
//   3. To enable the USB CDC driver for UPORT_USB, you must 
//      enable three tasks:
//
//        a) "SerDrvr USB" with a task parameter set to 4 (the uart port number)
//        b) "USB Enum" (the task parameter is not used and can be NULL)
//        c) "USB Dev CDC" (the task parameter is not used and can be NULL)
//
//////////////////////////////////////////////////////////////////////////////
const TASK_ENTRY_STRUCT Task_Table[ NUMBER_OF_TABLE_TASKS ] =
{

//	SORT THE TASK ORDER IN THIS TABLE BY PRIORITY
//												             															
//																														
//  CREATE 											TASK FUNCTION    			TASK NAME 				STACK DEPTH (32-BIT WORDS)				TASK PARAM.				TASKPRIORITY         				PTR TO TASK HANDLE
//																				(31 chars + NULL)                                                               		[(configMAX_PRIORITIES-1) is the
//              									    	                                                                                                    		 highest possible priority]
//              									
//																																										NOTE: (configMAX_PRIORITIES - 1)
//																																										is reserved for the startup task
//																																										which deletes itself thereby making
//																																										(configMAX_PRIORITIES - 2) the highest
//																																										task in our system. We do this to guarantee
//																																										the startup task runs to completion before
//																																										any other task runs.
									
// 8/11/2014 rmd : NOTE - leave the task names in the table. They are used in the alert line
// when the watchdog task considers the task to be 'frozen'. If you take them out to save
// code space the alert line cannot indicate which task is 'frozen'.

// PRIORITY_LEVEL_11 is the highest available for use in this table!!!!!!!!


	// STACK:		I've seen the display processing task use 3032 bytes of stack space. So for
	// 				now I'm setting it to 8192 bytes.
	//
	// PRIORITY:	If you are doing heavy display processing (say displaying alerts while
	//  			creating rapid fire new alerts) all tasks below the display task priority
	//  			can be starved of CPU time. If you have a task with a priority less the
	//  			display task generating mucho alerts continuously (not a normal scenario
	//  			unless something is wrong - this has happened in current product in the
	//  			past) you can actually COMPLETELY starve all tasks with priorities less than
	//  			the one generating the alerts. So beware!
	//
	//  			We may learn over time the display task priority needs to be lowered ... or
	//  			is dynamic perhaps?? So it's a balancing act. If it is too low you could
	//  			fill up the display task queue because the task won't be given enough time.
	//
	{   1,		WATCHDOG_MONITORS,		1000,		Display_Processing_Task,	"Dsply_Proc",		(0x2000/4),							NULL, 					PRIORITY_LEVEL_8},
	
	
	// STACK:		I've seen the Key Process task use up to 2364 bytes of stack space. Let's
	// 				try a 4K stack right now.
	//
	// PRIORITY:
	//
	{   1,		WATCHDOG_MONITORS,		1000,		Key_Processing_Task,		"Key_Proc",			(0x1000 / 4),						NULL, 					PRIORITY_LEVEL_7},
	

	
	
	// DESCRIPTION: 10.17.2011 rmd : This task processes two interrupt sources. The first
	// interrupt source is the key_scanner hardware which sense any CHANGES in the scan matrix.
	// These changes are a key press and a key release. The second interrupt source is the
	// repeat timer. This timer is started when a key press is seen. When it times out an
	// interrupt is generated which puts an event on this task. This task processes the
	// interrupt which normally results in a key event for the KEY PROCESSING TASK. Which being
	// a higher priority task will execute. We also restart the repeat timer (perhaps changing
	// its period). When the key is processed that in turn typically generates display task
	// events. Which are an even yet higher priority. So in effect with this task at the lower
	// of the key processing and dislay processing tasks, the generation of keys is self pacing.
	// They will only repeat upon completion of the prior keys key and display task activites!
	// And that is pretty cool.
	// 
	// STACK:
	//
	// PRIORITY: (see write up above)
	//
	{   1,		WATCHDOG_MONITORS,		1000,		key_scanner_task,        	"Key_Scan",			(0x1000 / 4),						NULL, 					PRIORITY_LEVEL_6},




	// DESCRIPTION: This tasks queue is feed once each s econd by the RTC task. The queue will
	//  			have a copy of the latest time & date to process. This is where all the
	//  			start time comparisons are made. Additionally tests against the roll time
	//  			for report data and all weather tables is made here.
	// 
	// STACK:		default
	//            
	// PRIORITY:	2011.11.30 rmd : There IS a delicate interaction between the TD_CHECK and
	//  			the COMM_MNGR tasks regarding MUTEXES. If they are at the same priority they
	//  			are of course time slicing each other. And TD_CHECK list maintenance
	//  			functions hold the foal_irri MUTEX for an extended period. The COMM_MNGR on
	//  			the other hand is trying to make tokens and when the token is of an extended
	//  			size (768 station start for example) then it too needs the same MUTEX for a
	//  			long time. And we have some contention. Which isn't so bad but we have
	//  			alerts about such. One could opt to ignore this by lengthening the
	//  			contention time. Another way to alleviate the issue is to raise the priority
	//  			of the TD_CHECK task higher than the COMM_MNGR task. TD_CHECK holds the
	//  			foal_irri mutex longer during its list maintenance function.
	// 
	{   1,		WATCHDOG_MONITORS,		1000,		TD_CHECK_task,    			"TD_Check",		(0x1000 / 4),							NULL,					PRIORITY_LEVEL_6},




	// DESCRIPTION: Handles bypass manifold operations.
	//
	// STACK:
	//
	// PRIORITY: Set at same priority as the td_check task. The logic being in a sense this is a
	// communications driven task. As flow readings come in the task runs. Also runs though at a
	// 1 hertz rate driven from the tdcheck task. So when messages are being built for the
	// tpmicro I can be assured events posted to the bypass task have been processed if we are
	// at the td_check priority level.
	//
	{   1,		WATCHDOG_MONITORS,		1000,		BYPASS_task,				"Bypass",     	(0x1000 / 4),							NULL,					PRIORITY_LEVEL_6},


	
	
	// DESCRIPTION: Kicks out the scan messages and manages the scan. Also processes the TOKEN
	// RESPONSE to the FOAL machine. Actually has evolved into processing ALL incoming messages.
	//
	// 7/21/2016 rmd : About the WDT timeout. Used to have brute force OS delay for the SR radio
	// allowing them time to establish themselves. But that has been gracefully integrated into
	// the task with a timer. So don't need to account for that.
	//
	// STACK:
	//
	// PRIORITY:
	//
	{   1,		WATCHDOG_MONITORS,		5000,		COMM_MNGR_task,				"Comm_Mngr",		(0x1000 / 4),						NULL,					PRIORITY_LEVEL_COMM_MNGR_TASK},


	// 6/20/2017 rmd : Allow 5 seconds for the brute force file write completion events, so they
	// don't trigger ICED TASK alerts. See the comment, in the task, by the file write
	// completion event processing for more details justifying this timeout.
	//
	{   1,		WATCHDOG_MONITORS,		5000,		CODE_DISTRIBUTION_task,		"Code Distrib",		(0x1000 / 4),						NULL,					PRIORITY_LEVEL_CODE_DISTRIBUTION_TASK},
	

	// STACK:
	//
	// PRIORITY:
	// 5/23/2017 rmd : The choice of priority considers several things. First in some cases,
	// ethernet or even wi-fi, data can arrive very quickly. So to avoid overruning the ring
	// buffers this task should have a fairly high priority. Perhaps even higher than TD_CHECK.
	// But for now I'll stop here at priority level 5 and see if we ever have overrun problems.
	//
	// 5/23/2017 rmd : Secondly, some message processing takes place within this task.
	// Pariticularly messages from the commserver. So to keep that moving along I feel a higher
	// priority is warranted.
	//
	{   1,		WATCHDOG_MONITORS,		1000,		ring_buffer_analysis_task,		"Ring_TP",      	(0x1000 / 4),					(void *)UPORT_TP, 		PRIORITY_LEVEL_RING_BUFFER_TASK},
	// STACK:
	//
	// PRIORITY:
	//
	// WDT TIMEOUT : 9/1/2015 rmd : About the WDT timeout - when we receive a factory reset
	// command via the cloud (communication channel). The commserver is expecting a reply ack.
	// If we proceed immediately with the factory reset and restart the ack will not be sent. So
	// we wait 10 seconds to ensure it is sent by the radio before performing the reset and
	// powering down the peripherals and performing the restart. So set the watchdog allowance
	// to 10500.
	//
	{   1,		WATCHDOG_MONITORS,		(10500),		ring_buffer_analysis_task,		"Ring_A",			(0x1000 / 4),					(void *)UPORT_A, 		PRIORITY_LEVEL_RING_BUFFER_TASK},
	// STACK:
	//
	// PRIORITY:
	//
	{   1,		WATCHDOG_MONITORS,		1000,		ring_buffer_analysis_task,		"Ring_B",			(0x1000 / 4),					(void *)UPORT_B,		PRIORITY_LEVEL_RING_BUFFER_TASK},
	// STACK:
	//
	// PRIORITY:
	//
	{   1,		WATCHDOG_MONITORS,		1000,		ring_buffer_analysis_task,		"Ring_RRE",      	(0x1000 / 4),					(void *)UPORT_RRE, 		PRIORITY_LEVEL_RING_BUFFER_TASK},



	// DESCRIPTION: Manages all the controller initiated communications. Meaning makes the
	// messages. And manages the message queuing and flow to the serial port.
	//
	// PRIORITY:
	//
	{   1,		WATCHDOG_MONITORS,		(2000),		CONTROLLER_INITIATED_task,	"CI task",			(0x1000 / 4),						NULL,					PRIORITY_LEVEL_5},

	// ----------
	
	// 11/30/2017 rmd : Link test (communications test by port). Put in at priority 5 along with
	// controller initiated and other serial comms tasks. No real reason - probably could run at
	// a lower priority too. I wouldn't go higher!
	{   1,		WATCHDOG_MONITORS,		(2000),		RADIO_TEST_task,			"LINK task",		(0x1000 / 4),						NULL,					PRIORITY_LEVEL_5},


	// STACK:
	//
	// PRIORITY:
	//
	{   1,		WATCHDOG_MONITORS,		1000,		TPL_IN_task,    			"TPL_IN",     		(0x1000 / 4),						NULL,					PRIORITY_LEVEL_TPL_IN_OUT_TASKS},
	
	// STACK:
	//
	// PRIORITY:
	//
	{   1,		WATCHDOG_MONITORS,		1000,		TPL_OUT_task,    			"TPL_OUT",     		(0x1000 / 4),						NULL,					PRIORITY_LEVEL_TPL_IN_OUT_TASKS},
	
	

	// STACK:
	//
	// PRIORITY:
	// 10/12/2017 rmd : BE CAREFUL thinking you might want to move the serial OUT-BOUND driver
	// tasks priority to up there with the IN-BOUND ring buffer analysis task. The IN-BOUND task
	// priority is above that of the controller initiated task, and if you moved the outbound
	// above the controller initiated task priority, you will change the dynamics of the code
	// during message processing. I think it may not be detrimental - but it is a big change.
	//
	{   1,		WATCHDOG_MONITORS,		1000,		serial_driver_task,    			"SerDrvr_TP",     		(0x1000 / 4),					(void *)UPORT_TP,		PRIORITY_LEVEL_SERIAL_DRIVER_TASK},


	// STACK:
	//
	// PRIORITY:
	//
	{   1,		WATCHDOG_MONITORS,		1000,		serial_driver_task, 	 		"SerDrvr_A",      	(0x1000 / 4),						(void *)UPORT_A,		PRIORITY_LEVEL_SERIAL_DRIVER_TASK},

	// STACK:
	//
	// PRIORITY:
	//
	{   1,		WATCHDOG_MONITORS,		1000,		serial_driver_task, 	 		"SerDrvr_B",      	(0x1000 / 4),						(void *)UPORT_B,		PRIORITY_LEVEL_SERIAL_DRIVER_TASK},

	// STACK:
	//
	// PRIORITY:
	//
	{   1,		WATCHDOG_MONITORS,		1000,		serial_driver_task, 	 		"SerDrvr_RRE",    		(0x1000 / 4),					(void *)UPORT_RRE,		PRIORITY_LEVEL_SERIAL_DRIVER_TASK},
	
	// NOTE: There is NO USB serial driver task. Implemented in a different fashion.
	
	
	// STACK:
	//
	// PRIORITY:
	//
	{   1,		WATCHDOG_MONITORS,		1000,		contrast_and_speaker_volume_control_task,	"Contrast",  		(0x1000 / 4),			NULL,					PRIORITY_LEVEL_2},

	
	
	// STACK:		
	//
	// PRIORITY:	When writing a large file to the flash file system if the task is
	//  			afforded a high priority the flash operation will starve lower priority
	//  			tasks. I've really decided to push the flash file writes to the lowest
	//  			application priority. It has been tested at this level and seems to work
	//  			well.
	//
	//  			Regarding the watchdog execution period we tolerate. Well when writing a
	//  			large file (500K+) the task can be busy for 10+ seconds. So we have to allow
	//  			that maybe for 15 seconds the task appears as if it is frozen. But it isn't.
	//  			We are writing the file. There are opportunities of course for the OS to
	//  			execute any higher priority tasks that become ready during that period. And
	//  			that actually COMPOUNDS the time needed as the write is interruptted by
	//  			other work (thank goodness!). I have seen writes out to 16.5 seconds for the
	//  			station history file. Allow the flash 0 task to write a really big file with
	//  			a 30 second limit!
	//
	{   1,		WATCHDOG_MONITORS,		30000,		FLASH_STORAGE_flash_storage_task,			"Flash_0",			(0x1000 / 4),				(void*)FLASH_INDEX_0_BOOTCODE_AND_EXE, 		PRIORITY_LEVEL_2},
	
	{   1,		WATCHDOG_MONITORS,		20000,		FLASH_STORAGE_flash_storage_task,			"Flash_1",			(0x1000 / 4),				(void*)FLASH_INDEX_1_GENERAL_STORAGE, 		PRIORITY_LEVEL_2},


	// This task manages low priority calculations such as the next scheduled irrigation for
	// display on the Status screen.
	// 
	// STACK:
	//
	// PRIORITY: This task is intended to be the lowest priority task. This ensures the
	// calculations won't interrupt any higher priority tasks.
	// 8/13/2018 rmd : WHY IS THE TASK WATCHDOG TIME SET TO 20 SECONDS? I THINK SCOTT DID THIS.
	//
	// 8/13/2018 rmd : NOTE - THIS ENTIRE TASK SHOULD GO AWAY. THE NEXT SCHEDULED SHOULD FALL
	// OUT OF THE FINISH TIMES CALCULATION. VERY EASY TO DO.
	//
	{   1,		WATCHDOG_MONITORS,		20000,		background_calculation_task,	"BCalc",  		(0x1000 / 4),			NULL,					PRIORITY_LEVEL_1},

	// ----------

	// This task manages the low priority finish times calculation.
	// 
	// STACK: 4K byte should be enough. We will monitor.
	//
	// PRIORITY: This task is intended to be the lowest priority task. This ensures the
	// calculation won't interrupt any higher priority tasks.
	//
	// WATCHDOG CHECKIN LIMIT: The task is an iterative process, one task event at a time. Each
	// task event represents a time step as small as a second, as large as 10 minutes. The task
	// checks in A LOT while the calculation is running. So the task timeout can remain at our
	// default 1000ms.
	{   1,		WATCHDOG_MONITORS,		1000,		FTIMES_TASK_task,	"FTimes",  		(0x1000 / 4),			NULL,					PRIORITY_LEVEL_1},
	
	// ----------
	
	/*
	// STACK:
	//
	// PRIORITY:
	//
	{   0,		WATCHDOG_EXCLUDED,		1000,		vled_TASK,									"",  			(0x1000 / 4),				(void*)1, 									PRIORITY_LEVEL_1},

	// STACK:
	//
	// PRIORITY:
	//
	{   0,		WATCHDOG_EXCLUDED,		1000,		vled_TASK,									"",  			(0x1000 / 4),				(void*)2, 									PRIORITY_LEVEL_1},

	// STACK:
	//
	// PRIORITY:
	//
	{   0,		WATCHDOG_EXCLUDED,		1000,		vled_TASK,									"",  			(0x1000 / 4),				(void*)3, 									PRIORITY_LEVEL_1},

	// STACK:
	//
	// PRIORITY:
	//
	{   0,		WATCHDOG_EXCLUDED,		1000,		vled_TASK,									"",  			(0x1000 / 4),				(void*)4, 									PRIORITY_LEVEL_1},
	*/
	
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void elevate_priority_of_ISP_related_code_update_tasks( void )
{
	// 3/5/2014 rmd : During ISP mode code download boost these task priorities way up there.
	// Above all other task except for the timer task, the rtc task, and the powerfail task.
	// Otherwise things like display writes can cause the isp expected response timer to timer
	// out. This was the cleanest fix rather than go re-time ISP activity and figure out longest
	// display write time (and the longest today may not be the longest tomorrow).

	if( comm_mngr_task_handle && port_tp_ring_buffer_analysis_task_handle && port_tp_serial_driver_task_handle )
	{
		vTaskPrioritySet( comm_mngr_task_handle, PRIORITY_LEVEL_DURING_ISP_CODE_XFER );
	
		vTaskPrioritySet( port_tp_ring_buffer_analysis_task_handle, PRIORITY_LEVEL_DURING_ISP_CODE_XFER );
	
		vTaskPrioritySet( port_tp_serial_driver_task_handle, PRIORITY_LEVEL_DURING_ISP_CODE_XFER );
	}
	else
	{
		Alert_Message( "NULL Task Handle!" );
	}
}

extern void restore_priority_of_ISP_related_code_update_tasks( void )
{
	// 3/5/2014 rmd : After code xfer completed restore them to normal.
	
	if( comm_mngr_task_handle && port_tp_ring_buffer_analysis_task_handle && port_tp_serial_driver_task_handle )
	{
		vTaskPrioritySet( comm_mngr_task_handle, PRIORITY_LEVEL_COMM_MNGR_TASK );
	
		vTaskPrioritySet( port_tp_ring_buffer_analysis_task_handle, PRIORITY_LEVEL_RING_BUFFER_TASK );
	
		vTaskPrioritySet( port_tp_serial_driver_task_handle, PRIORITY_LEVEL_SERIAL_DRIVER_TASK );
	}
	else
	{
		Alert_Message( "NULL Task Handle!" );
	}
}

/* ---------------------------------------------------------- */
static void __create_system_semaphores( void )
{
	int i;
	
	#ifndef NO_DEBUG_PRINTF

		assert( ( debugio_MUTEX = xSemaphoreCreateMutex() ) ); // create and test to be non-zero

	#endif
	
	// --------
	
	for( i=UPORT_TP; i<UPORT_TOTAL_PHYSICAL_PORTS; i++ )
	{
		#if (configUSE_TRACE_FACILITY == 1)
			char	str_32[ 32 ];
			
			// Create the binary semaphore for the port. These are used to signal the ring_buffer_analysis task that data has arrived.
			vSemaphoreCreateBinary( rcvd_data_binary_semaphore[ i ] );
	
			snprintf( str_32, sizeof(str_32), "%s ring semaphore", port_names[ i ] );
	
			vTraceSetQueueName( rcvd_data_binary_semaphore[ i ], str_32 );
	
	
			xmit_list_MUTEX[ i ] = xSemaphoreCreateMutex();
	
			snprintf( str_32, sizeof(str_32), "%s xmit MUTEX", port_names[ i ] );
	
			vTraceSetQueueName( xmit_list_MUTEX[ i ], str_32 );
		#else
			// Create the binary semaphore for the port. These are used to signal the ring_buffer_analysis task that data has arrived.
			vSemaphoreCreateBinary( rcvd_data_binary_semaphore[ i ] );
	
			xmit_list_MUTEX[ i ] = xSemaphoreCreateMutex();
		#endif
	}
	
	// --------
	
	// Create the mutex for the DF Storage Manager
	Flash_0_MUTEX = xSemaphoreCreateMutex();
	vTraceSetQueueName( Flash_0_MUTEX, "Flash 0 MUTEX" );

	Flash_1_MUTEX = xSemaphoreCreateMutex();
	vTraceSetQueueName( Flash_1_MUTEX, "Flash 1 MUTEX" );
	
	// --------

	alerts_pile_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( alerts_pile_recursive_MUTEX, "alerts MUTEX" );
	
	// --------
	
	// Create the mutex which protects the group structure lists
	list_program_data_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( list_program_data_recursive_MUTEX, "Program Data MUTEX" );
	
	// --------
	
	list_tpl_in_messages_MUTEX = xSemaphoreCreateMutex();
	vTraceSetQueueName( list_tpl_in_messages_MUTEX, "tpl_in MUTEX" );

	list_tpl_out_messages_MUTEX = xSemaphoreCreateMutex();
	vTraceSetQueueName( list_tpl_out_messages_MUTEX, "tpl_out MUTEX" );
	
	// --------
	
	list_incoming_messages_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( list_incoming_messages_recursive_MUTEX, "incoming msgs MUTEX" );

	list_packets_waiting_for_token_MUTEX = xSemaphoreCreateMutex();
	vTraceSetQueueName( list_packets_waiting_for_token_MUTEX, "packets 2 xmit MUTEX" );
	
	// --------
	
	irri_irri_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( irri_irri_recursive_MUTEX, "irri_irri MUTEX" );
	
	list_foal_irri_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( list_foal_irri_recursive_MUTEX, "foal_irri MUTEX" );
	
	// --------
	
	comm_mngr_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( comm_mngr_recursive_MUTEX, "comm_mngr MUTEX" );
	
	chain_members_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( chain_members_recursive_MUTEX, "iccc MUTEX" );
	
	irri_comm_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( irri_comm_recursive_MUTEX, "irri_comm MUTEX" );
	
	// --------
	
	ci_and_comm_mngr_activity_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	
	// --------
	
	system_report_completed_records_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( system_report_completed_records_recursive_MUTEX, "system report MUTEX" );

	poc_report_completed_records_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( poc_report_completed_records_recursive_MUTEX, "poc report MUTEX" );

	station_report_data_completed_records_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( station_report_data_completed_records_recursive_MUTEX, "station report MUTEX" );

	station_history_completed_records_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( station_history_completed_records_recursive_MUTEX, "station history MUTEX" );
	
	// --------
	
	weather_preserves_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( weather_preserves_recursive_MUTEX, "weather preserves MUTEX" );
	
	station_preserves_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( station_preserves_recursive_MUTEX, "station preserves MUTEX" );

	system_preserves_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( system_preserves_recursive_MUTEX, "system preserves MUTEX" );

	poc_preserves_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( poc_preserves_recursive_MUTEX, "poc preserves MUTEX" );
	
	// --------
	
	list_system_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( list_system_recursive_MUTEX, "system list MUTEX" );

	list_poc_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( list_poc_recursive_MUTEX, "poc list MUTEX" );
	
	// --------
	
	weather_control_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( weather_control_recursive_MUTEX, "weather control MUTEX" );

	weather_tables_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( weather_tables_recursive_MUTEX, "weather tables MUTEX" );

	// ----------

	list_lights_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( list_lights_recursive_MUTEX, "lights list MUTEX" );

	list_foal_lights_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( list_foal_lights_recursive_MUTEX, "foal_lights MUTEX" );

	irri_lights_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( irri_lights_recursive_MUTEX, "irri_lights MUTEX" );

	lights_report_completed_records_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( lights_report_completed_records_recursive_MUTEX, "lights report MUTEX" );

	lights_preserves_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( lights_preserves_recursive_MUTEX, "lights preserves MUTEX" );

	// ----------

	// 8/2/2012 rmd : Note the speaker hardware MUTEX is not recursive. No need.
	speaker_hardware_MUTEX = xSemaphoreCreateMutex();
	vTraceSetQueueName( speaker_hardware_MUTEX, "speaker MUTEX" );


	tpmicro_data_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	vTraceSetQueueName( tpmicro_data_recursive_MUTEX, "tpmicro data MUTEX" );

	// ----------

	pending_changes_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();

	// ----------
	
	moisture_sensor_items_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();

	moisture_sensor_recorder_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();
	
	// ----------

	// 2/13/2018 Ryan : This mutex is used to protect the WALK_THRU_STRUCT.
	walk_thru_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();

	// ----------

	// 02/22/2016 skc : This mutex protects the COMPLETED_BUDGET_REPORT_RECORDS_STRUCT.
	budget_report_completed_records_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();

	// ----------
	
	ci_message_list_MUTEX = xSemaphoreCreateMutex();

	// ----------
	
	code_distribution_control_structure_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();

	// ----------
	
	chain_sync_control_structure_recursive_MUTEX = xSemaphoreCreateRecursiveMutex();

	// ----------
	
	router_hub_list_MUTEX = xSemaphoreCreateMutex();
}

/* ---------------------------------------------------------- */
static void __create_system_queues( void )
{
	// --------
	
	Contrast_and_Volume_Queue = xQueueCreate( C_AND_V_NUMBER_OF_QUEUE_ITEMS, sizeof(C_AND_V_QUEUE_EVENT_STRUCT) );
	
	// --------
	
	// Create an event queue for each serial port driver instance
	UNS_32	i;
	
	for( i=UPORT_TP; i<UPORT_TOTAL_PHYSICAL_PORTS; i++ )
	{
		#if (configUSE_TRACE_FACILITY == 1)
			char	str_32[ 32 ];
			
			SerDrvrVars_s[ i ].SerportDrvrEventQHandle = xQueueCreate( SERPORT_DRVR_EVENT_QSIZE, sizeof( SERPORT_EVENT_DEFS ) );
			snprintf( str_32, sizeof(str_32), "%s xmit Queue", port_names[ i ] );
			vTraceSetQueueName( SerDrvrVars_s[ i ].SerportDrvrEventQHandle, str_32 );
		#else
			SerDrvrVars_s[ i ].SerportDrvrEventQHandle = xQueueCreate( SERPORT_DRVR_EVENT_QSIZE, sizeof( SERPORT_EVENT_DEFS ) );
		#endif
	}
	
	// --------
	
	Key_Scanner_Queue = xQueueCreate( KEY_SCANNER_QUEUE_DEPTH, sizeof(KEY_SCANNER_QUEUE_STRUCT) );
	
	Key_To_Process_Queue = xQueueCreate( KEY_TO_PROCESS_QUEUE_DEPTH, sizeof(KEY_TO_PROCESS_QUEUE_STRUCT) );
	
	// --------
	
	// create the message queue for the FLASH CLEANER task - on startup all the inits run and a brand new controller has a lot of files to save so need 50 slots in the queue
	FLASH_STORAGE_task_queue_0 = xQueueCreate( 50, sizeof( FLASH_STORAGE_QUEUE_STRUCT ) );

	FLASH_STORAGE_task_queue_1 = xQueueCreate( 50, sizeof( FLASH_STORAGE_QUEUE_STRUCT ) );
	
	// --------
	
	Display_Command_Queue = xQueueCreate( DISPLAY_QUEUE_EVENTS_DEPTH, sizeof(DISPLAY_EVENT_STRUCT) );
	
	// --------
	
	TPL_IN_event_queue = xQueueCreate( 30, sizeof(TPL_IN_EVENT_QUEUE_STRUCT) );

	TPL_OUT_event_queue = xQueueCreate( 30, sizeof(TPL_OUT_EVENT_QUEUE_STRUCT) );


	COMM_MNGR_task_queue = xQueueCreate( 30, sizeof(COMM_MNGR_TASK_QUEUE_STRUCT) );
	
	CODE_DISTRIBUTION_task_queue = xQueueCreate( 30, sizeof(CODE_DISTRIBUTION_TASK_QUEUE_STRUCT) );

	CONTROLLER_INITIATED_task_queue = xQueueCreate( 15, sizeof(CONTROLLER_INITIATED_TASK_QUEUE_STRUCT) );


	BYPASS_event_queue = xQueueCreate( 30, sizeof(BYPASS_EVENT_QUEUE_STRUCT) );
	

	RADIO_TEST_task_queue = xQueueCreate( 10, sizeof(RADIO_TEST_TASK_QUEUE_STRUCT) );
	
	// --------

	Background_Calculation_Queue = xQueueCreate( BKGRND_CALC_NUMBER_OF_QUEUE_ITEMS, sizeof(BKGRND_CALC_EVENT_DEFS) );

	// ----------
	
	// 2/8/2018 rmd : I just picked 15 queue items. Not yet sure what the queue activity or
	// demand will be yet.
	FTIMES_task_queue = xQueueCreate( 15, sizeof(FTIMES_TASK_QUEUE_STRUCT) );

	// ----------
	
	// 5/1/2017 rmd : Grabs a single block, (NUMBER_OF_ITEMS * 4) in size, currently that would
	// be a single 2048 byte block from out of our memory pool. The point is a single block for
	// all the queue items.
	//
	// 9/21/2017 rmd : Here's an interesting scenario to think about, and this throws out a
	// scenario explaining why we don't want the hub list to be battery backed (which as a
	// FreeRTOS queue would actually be hard to do, not impossible, but hard). Anyway, here's
	// the scenario, the commserver sends out hub lists to 2 hubs. And then updates the main
	// binary in one. That hub begins a code distribution. Suppose that hub suffers a power
	// outage. During the power outage a second hub asks for and receives its binary update, and
	// begins its code distribution. Well if the first hub regained power and had its hub list
	// it could possibly restart its code distribution. Since the hub list is not battery backed
	// that won't happen, code distribution waits till the hub list has been received, and the
	// commserver shouldn't send it if another hub is distributing code.
	router_hub_list_queue = xQueueCreate( ROUTER_HUB_LIST_MAX_QUEUE_ITEMS, sizeof(UNS_32) );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/17/2012 rmd : I don't necessaryily like this brute force method. But the startup task
// is a beast when thinking about the flash file system coupled with structure inits taking
// place. In the face of an impending power failure the process must be halted. However not
// at just anypoint. Functions should exit gracefully else odd things may occur. Such as
// partially intialized battery backed structs. Or the file system may abort at a dangerous
// moment which crashes the file system. So at the task body level we intersperse this line
// to decide if it's time to quit! I just couldn't think of another way to do this. Believe
// me I think it's somewhat ugly. It is however quite functional.

#define TEST_FOR_SHUTDOWN_INDICATION		if( restart_info.SHUTDOWN_impending == (true) ) vTaskSuspend( NULL )
		
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void system_startup_task( void *pvParameters )
{
	// -------------------------

	// 7/16/2012 rmd : At this point the scheduler has started and THREE tasks exist. This task,
	// the IDLE task, and the TIMER task. This task goes about most of the initialization
	// business. During which all other tasks are spawned. This task, the startup task, has a
	// priority level of 1.

	// -------------------------

	// STEP 1

	// 10/16/2012 rmd : Create the WDT task. So we get the watchdog counter up and running. To
	// cover any actual coding errors, exceptions, etc. Also remember the watchdog needs to be
	// running to support the powerfail task. When the voltage threshold pwr fail interrupt
	// fires we count on the WDT to restart us in a brown out condition.
	//
	// 10/16/2012 rmd : A word on the WDT task priority. We want it to eventually be a low
	// priority task. That way if it doesn't get time to run due to starvation the WDT counter
	// will expire and we'll reset. So we'll be able to detect starvation that way. HOWEVER we
	// want the WDT task to run now till it hits it's delay. This way we know we've got the WDT
	// hardware intialized. So we create it with a high priority and then lower it after all the
	// other tasks have been created.
	xTaskCreate( wdt_monitoring_task, (signed char *) "WDT", (0x2000 / 4), NULL, (configMAX_PRIORITIES-1), &wdt_task_handle );
	//
	// 10/16/2012 rmd : Because the TaskCreate function yields to higher priority tasks the
	// watchdog counter hardware is now initialized and active.
	OS_make_task_VFP_capable( wdt_task_handle, (void*)&wdt_task_FLOP_registers );

	// 10/16/2012 rmd : At this point we KNOW the wdt task has run till blocking on its 500ms
	// delay (cause we created the task at the highest possible priority). And there is a 10
	// second watchdog timer over our heads. So now set the WDT task to it's final desired
	// priority level 1. Which is is the same as the flash file task priority level. And during
	// large file writes to flash the RTOS should time slice and allow the wdt task to run and
	// service the wdt hardware.
	vTaskPrioritySet( wdt_task_handle, PRIORITY_LEVEL_WDT_TASK_1 );

	// -------------------------

	// STEP 2

	vSemaphoreCreateBinary( startup_rtc_task_sync_semaphore );
	
	// Take the semaphore once as it's created signaled.
	xSemaphoreTake( startup_rtc_task_sync_semaphore, 0 );

	// 10/16/2012 rmd : Create the RTC task next. Very early on. As a matter of fact BEFORE the
	// pwr fail detect task. BECAUSE we want to have the current time & date available for any
	// process that needs it (for alert lines typically). So a key piece of this tasks startup
	// is to read the clock and get the T & D available after which it then allows the
	// startup_task to resume.
	xTaskCreate( EPSON_rtc_control_task, (signed char *) "RTC", (0x2000 / 4), NULL, PRIORITY_LEVEL_EPSON_RTC_TASK_14, &epson_rtc_task_handle );

	OS_make_task_VFP_capable( epson_rtc_task_handle, (void*)&epson_RTC_task_FLOP_registers );

	// 7/16/2012 rmd : We want the EPSON RTC initialization to have proceeded to a certain point
	// before this startup task continues. That is so the creation of alert lines can be done
	// with an accurate time and date stamp. We will synchronize this startup task with the RTC
	// task with a binary semaphore. [The results of the scope tests are most interesting. It
	// appears we are actually waiting for the first 1HZ tick in the physical RTC to occur. So
	// the delay time we are pending on the semaphore is all over the map. Anywhere from just
	// over 1ms to 500ms or more. I suppose up to 1 whole second.
	xSemaphoreTake( startup_rtc_task_sync_semaphore, portMAX_DELAY );  // wait
	
	// ----------

	// STEP 3

	// 9/11/2012 rmd : Obtain the timer task handle for the timer task that was created when the
	// scheduler started. Make this assignment BEFORE the powerfail_task is created as this is
	// used in the power fail task to suspend the timer task. We also use it to set the task TAG
	// for the timer task.
	timer_task_handle = xTimerGetTimerDaemonTaskHandle();
	
	// 9/11/2012 rmd : We don't have to do this as I am quite certain the timer task has NO
	// floating point operations in it. But to be on the safe side ....
	OS_make_task_VFP_capable( timer_task_handle, (void*)&timer_task_FLOP_registers );
	
	// ----------

	// STEP 4
	
	// 10/16/2012 rmd : Now that the time and date are available for the pwr fail task to stuff
	// into the restart_info strucuture we go ahead and create the power_fail_task.
	xTaskCreate( system_shutdown_task, (signed char *) "PWRFAIL", (0x2000 / 4), NULL, PRIORITY_LEVEL_POWERFAIL_TASK_15, &powerfail_task_handle );

	// 10/19/2012 rmd : As with all tasks created in this task (startup_task), we WILL NOT
	// continue execution (of the startup task) till the powerfail_task has intialized itself
	// and is pending on the semaphore. The pwr fail task initialization code enables the pwr
	// fail isr and then runs till pending on it's binary semaphore. But we still cannot
	// reference the RESTART_info structure yet. Need to wait till the init_restart_info
	// function is called.

	OS_make_task_VFP_capable( powerfail_task_handle, (void*)&powerfail_task_FLOP_registers );
	
	// -------------------------

	// 10/25/2013 rmd : Next get all the MUTEXES, SEMAPHORES, and QUEUES made. This activity
	// basically consists of a bunch of memory grabs from our memory pool. Followed by the
	// initialization of those items by FreeRTOS. It is important to get these made as early on
	// as possible considering it is very easy to make future changes where you expect, as part
	// of a startup sequence or otherwise, these facilities exist. Why not create them even
	// earlier on in the startup? Well the WDT and PWRFAIL tasks should come first.

	__create_system_semaphores();

	__create_system_queues();

	// -------------------------

	// 10/11/2013 rmd : Hey. Hey. Well guess what we are getting ready to allow alerts to be
	// created. As a matter of fact the call to test all the piles creates an alert if there is
	// a pile restart. SO THE ALERTS TIMER NEEDS TO EXIST. So create the application level
	// alerts timer. Default is 15 minutes. Some alerts may change to a shorted timeout
	// depending on importance level, for example the MLB alert.
	cics.alerts_timer = xTimerCreate( (const signed char*)"ci alerts tmr", MS_to_TICKS(CONTROLLER_INITIATED_DEFAULT_SEND_ALERTS_TIMEOUT_MS), FALSE, NULL, ci_alerts_timer_callback );
		
	// ----------

	// 10/11/2013 rmd : Check the piles are intialized. If not, performs pile restarts. ALERT
	// LINES can made after the piles are tested. NOT BEFORE THIS. THE PILES AREN'T READY!
	ALERTS_test_all_piles( INIT_ONLY_IF_CHECK_FAILS );
	

	// 10/25/2013 rmd : NOW you can make ALERTS! Not before.
	

	// ----------
	// STEP 6
	// ----------

	// 10/16/2012 rmd : This init function makes alerts. So now that the piles are ready for use
	// call the function that determines the reason for the restart and makes indicitive alert
	// lines. AND it is AFTER this call to init the restart_info that we can begin again
	// checking for the SHUTDOWN impending flag to be set (true).
	BOOL_32	code_updated;
	
	code_updated = init_restart_info( INIT_ONLY_IF_CHECK_FAILS );
	
	// 10/25/2013 rmd : Now AFTER we have initialized the restart_info we are allowed to use the
	// TEST_FOR_SHUTDOWN_INDICATION macro. Not before! You will not be able to start because ...
	// well you had a shutdown and the captured state is such. So the macro will stop you dead
	// in your tracks.
	
	// --------

	// 9/7/2012 rmd : The Precipio trace intialization.
	#if (configUSE_TRACE_FACILITY == 1)

		#if (TRACE_DATA_ALLOCATION == TRACE_DATA_ALLOCATION_DYNAMIC)
			/* Put the recorder data structure on the heap (malloc), since no 
			static allocation. (See TRACE_DATA_ALLOCATION in trcConfig.h). */
			xTraceInitTraceData();
		#endif
	
		vTraceStartStatusMonitor();
		
		if (! uiTraceStart() )
		{
				vTraceConsoleMessage("Could not start recorder!");
		}

	#endif
    
	// -------------------------

	// STEP 8

	TEST_FOR_SHUTDOWN_INDICATION;

	// -------------------------

	// 1/19/2017 rmd : Learn the hardware revision. This is not yet used. So this is as good as
	// a place as any in the startup sequence to do this. In practice this may need to be done
	// earlier.
	GPIO_SETUP_learn_and_set_board_hardware_id();
	
	// -------------------------

	// STEP 9

	// 7/16/2012 rmd : Before we perform any file save activites we must have created the
	// timers. Because during a file save activity (coming soon in this task) if the memory is
	// not available to hold a copy of the data to be saved, the file save will be postponed.
	// And the timer that manages the postponment MUST exist else we will crash when an attempt
	// to start the timer is made.
	TEST_FOR_SHUTDOWN_INDICATION;
	FLASH_STORAGE_create_delayed_file_save_timers();

	// -------------------------

	// STEP 10

	// STARTUP THE FLASH FILE SYSTEM SO WE CAN BEGIN READING FILES AND ESTABLISH STRUCTURES AND LIST INTIALIZATIONS
	
	// 4/21/2016 rmd : Do not move these functions to before the point within the startup that
	// we can make ALERTS. There are ALERTS made if something goes wrong during the startup.
	
	// 1/18/2017 rmd : The display model variable is set below during the serial channel init
	// function for flash index 0. However for throughness force a valid display model here. Use
	// the new blck and white AZ model instead of the older greyscale Kyocera model.
	display_model_is = DISPLAY_MODEL_AZDISPLAYS_ATM3224H;
	
	TEST_FOR_SHUTDOWN_INDICATION;
	// 1/18/2017 rmd : Display model variable is set here according to the detected serial flash
	// device.
	init_FLASH_DRIVE_SSP_channel( FLASH_INDEX_0_BOOTCODE_AND_EXE );  // hardware: serial channel

	TEST_FOR_SHUTDOWN_INDICATION;
	init_FLASH_DRIVE_SSP_channel( FLASH_INDEX_1_GENERAL_STORAGE );  // hardware: serial channel

	TEST_FOR_SHUTDOWN_INDICATION;
	init_FLASH_DRIVE_file_system( FLASH_INDEX_0_BOOTCODE_AND_EXE, (false) );  // See if file system is valid. Do not force an initialization.

	TEST_FOR_SHUTDOWN_INDICATION;
	init_FLASH_DRIVE_file_system( FLASH_INDEX_1_GENERAL_STORAGE, (false) );  // See if file system is valid. Do not force an initialization.

	// ----------

	// STEP 11

	// 8/13/2018 rmd : Because the chain_sync checksums are developed as part of the classic
	// "init_file" function call, this must be called before the series of init_file function
	// calls, because this init for the chain sync zeros out all the chain_sync checksums.
	init_chain_sync_vars();
	
	// ----------

	// 10/17/2012 rmd : The init_lcd and init_speaker are dependant on config_c structure being
	// valid. Therefore those init functions MUST be invoked AFTER this call to init the
	// controller configuration file.
	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_configuration_controller();

	// ----------
	
	// 9/17/2015 rmd : DO NOT CHANGE THE LOCATION OF THIS FILE INIT IN THE STARTUP SEQUENCE
	// unless you are sure what you are doing. Much code, including other file init updater
	// functions, depend upon being able to get and know the controller index which is stored
	// in this file!
	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_flowsense();

	// ----------
	
	// 4/21/2014 rmd : This initialization of the NETWORK CONFIGURATION file must come after the
	// initialization for the FLOWSENSE file. Because the network configuration initialization,
	// if initialization is needed, relies upon a valid controller_index for use with the SET
	// functions.
	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_configuration_network();

	// ----------
	
	// 4/21/2014 rmd : Because the flowsense data is now in place we can obtain this controllers
	// index for the restart alert.
	Alert_program_restart_idx( FLOWSENSE_get_controller_index() );
	
	// ----------
	
	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_contrast_and_speaker_vol();

	// ----------
	
	// STEP 12

	// 10.17.2011 rmd : Init_lcd REQUIRES the hl_config structure to be valid because we set the
	// speaker and contrast settings to their saved levels. AND init_lcd requires the the
	// contrast and voltage dac task queue to exist. As we post to it. So must be called after
	// the queues are made. After the display is powered we apply the bias voltage. AFTER the
	// controller is up and running. Which I believe is a requirement of the LCD spec. Bias
	// voltage last.
	TEST_FOR_SHUTDOWN_INDICATION;
	init_lcd_2();

	// 10.17.2011 rmd : Initialize the speaker amplifier hardware.
	TEST_FOR_SHUTDOWN_INDICATION;
	init_speaker();

	// -------------------------

	// 8/28/2013 rmd : Init the ADC peripheral. So far only used by serial port task when
	// initializing UART hardware. We use the ADC to determine the device card on the port.
	init_a_to_d();

	// 11/11/2016 rmd : In order to discover the devices on the serial port we need two things
	// to be in place. First the ADC peripheral must be initialized. And two the controller
	// configuration file must have gone through its initialization process.
	//
	// 11/14/2016 rmd : We want to identify the device early on in the startup process. I have
	// seen a CD transition event posted to the CI task queue BEFORE the task is created (on
	// with some devices). If that happens when the task is created it runs not only its startup
	// code but also processes that first event. Which in itself could be a problem if that code
	// runs into other yet to be initialized software or hardware. But at least we know which
	// device is out there. We'll start there.
	CONFIG_device_discovery( UPORT_A );

	CONFIG_device_discovery( UPORT_B );
	
	
	// -------------------------

	// NOTE : hl_config needs to be valid for some of this intialization.
	
	TEST_FOR_SHUTDOWN_INDICATION;
	init_irri_irri();
	
	// 8/13/2015 mpd : Added for lights.
	TEST_FOR_SHUTDOWN_INDICATION;
	init_irri_lights();

	// -------------------------

	// 1/28/2015 rmd : These files have been removed from ALL controllers. Therefore this
	// section deleting OBSOLETE files is commented out. I've left for reference if again
	// needed.
	/*
	// 2011.11.16 rmd : Clean the file system of obsolete files. Add the file name string to the
	// array below to cause a search to delete ALL files by that name. The request to perform
	// this work is queued. It occurs sometime after the startup task has completed.
	// 
	// 2011.12.19 ajv : Make sure you include a comma after each array element. This code
	// compiles error/warning free if you don't separate the values with a comma, but the
	// strings end up being concatenated.
	char* const obsolete_flash1_file_names[] =
	{

		"HEAD_TYPE",
		"PLANT_MATERIAL",
		"STATION_SLOPE",
		"STATION_SOIL_TYPE",
		"STATION_TAG",
		"IRRIGATION_PRIORITIES",
		"PERCENT_ADJUST",
		"SCHEDULE",
		"DAILY_ET_USE",
		"RAIN_USE",
		"WIND_USE",
		"ALERT_ACTIONS",
		"ON_AT_A_TIME",
		"PUMP_USE",
		"LINE_FILL_VALVE_CLOSE_TIMES",

		// 3/25/2014 ajv : Added removal of CONTROLLER_CONFIG from FLASH 1 since
		// this file is now stored on FLASH 0 to make it less likely to be
		// overwritten since we don't clear FLASH 0 on a factory reset.
		"CONTROLLER_CONFIGURATION"
	};

	UNS_32	i;

	for( i = 0; i < N_ARRAY_ELEMENTS(obsolete_flash1_file_names); ++i )
	{
		// Queue the request. Actual search and delete activities performed when RTOS allows.
		FLASH_STORAGE_delete_all_files_with_this_name( FLASH_INDEX_1_GENERAL_STORAGE, obsolete_flash1_file_names[ i ] );
	}
	*/ 

	// -------------------------

	// 10/25/2013 rmd : This is the file-by-file discovery. If the file doesn't exist it will be
	// created and the group list populated with a single default group. It is important to do
	// this before the bulk of the tasks are created as the tasks depend on this information
	// being valid and intact. Note the files activities are NOT queued. They are performed in
	// line here. And therefore the execution of this code can be somewhat time consuming.
	//
	// The order is important. There are some notes on ordering. DO NOT go ahead and re-order
	// without a very determined reason. A lot of testing under a wide variety of conditions and
	// states would be subsequently needed. That testing has been achieved so far only by time
	// of use.

	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_system_report_records();

	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_poc_report_records();

	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_station_report_data();

	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_station_history();

	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_lights_report_data();

	//03/11/2016 skc : Seems like budgets ia always right after lights.
	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_budget_report_records();

	// ----------
	
	// 10/25/2013 rmd : This must come BEFORE the group files whose lists depend on which cards
	// are installed in the motherboard. Because for example if we had a new station file
	// version we would reset the station file - now there are no stations. We need to see a
	// 'changed' tpmicro_data 'whats installed' coming from the tp micro to trigger re-making
	// the entities (stations, pocs, weather devices) based upon what's installed. So we've
	// added a mechanism in the file checking such that whenever any list file is started again
	// from scratch we clear the tpmicro_data 'whats installed' structure. And that clearing
	// must come AFTER we've read the tpmicro_data out from it's file. Otherwise we'd un-do our
	// clearing by overwriting it with the file contents.
	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_tpmicro_data();

	// ----------
	
	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_weather_control();


	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_weather_tables();


	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_manual_programs();


	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_irrigation_system();


	// 2012.01.26 rmd : This MUST come after the call to init_file_irrigation_system(). Because
	// we assign the system GID to the POC created (if file is created/updated).
	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_POC();


	// 9/22/2015 ajv : This MUST come after the call to init_file_irrigation_system(). Because
	// we assign the system GID to the station group created (if file is created/updated).
	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_station_group();  // this is the LANDSCAPE_DETAILS file


	// This MUST come after the calls to any groups - the groups must exist before stations can
	// be assigned to them.
	//
	// 2011.09.30 rmd : One of the reasons this MUST occur last being if a new station file is
	// created the station itself is set to the default information. And that consists of GID's
	// from other groups. Which had better exist at the time. For example the schedule GID
	// setting requires the schedule group list to exist.
	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_station_info();


	// 7/7/2015 mpd : This location is based on lights using GIDs as the POCs do.  
	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_LIGHTS();


	// 2/26/2016 rmd : So far no known dependancies as far as the ordering of the moisture
	// sensor file init function.
	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_moisture_sensor();


	// 2/13/2018 Ryan : So far no known dependancies for walk thru file.
	TEST_FOR_SHUTDOWN_INDICATION;
	init_file_walk_thru();

	// ----------

	// 8/20/2012 rmd : To ensure the TPMicro is enabled to accept turn ON commands.
	TEST_FOR_SHUTDOWN_INDICATION;
	init_tpmicro_data_on_boot();

	// -------------------------

	// 12/13/2017 rmd : Get the radio test parameters ready. Make sure this is called AFTER the
	// port device discovery that assigns the port_device_index.
	TEST_FOR_SHUTDOWN_INDICATION;
	init_radio_test_gui_vars_and_state_machine();

	// -------------------------

	// NOTE: DO WE WANT to invoke a routine file system cleanup activity for each file that
	// exists???? To clean out older versions that for some reason or another (power failure?)
	// failed to be deleted?

	/* ---------------------------------------------------------- */
	/* ---------------------------------------------------------- */

	// BATTERY BACKED INITIALIZATION SECTION
	
	/* ---------------------------------------------------------- */
	/* ---------------------------------------------------------- */

	// 6/1/2012 rmd : I moved the entire group of battery backed initialization routines to
	// AFTER the file list are in place. The system_preserves for one depends on the
	// irrigation_sysytem file list being intact. There may be other dependancies. Which we
	// should identify as we learn them. Ahhh. Here is one. The poc_preserves depends on the
	// both the POC list to be intact in memory (and therefore the file exists) and additionally
	// the poc_preserves is dependant upon the system_preserves being valid (as we each
	// poc_preserve record contains a pointer to the system_preserve record the poc belongs to
	// (well the poc actually belongs to a system but the system_preserve record is tied to the
	// system as well).
	
	// ---------------------------
	
	TEST_FOR_SHUTDOWN_INDICATION;
	init_battery_backed_weather_preserves( INIT_ONLY_IF_CHECK_FAILS );
		
	// ---------------------------

	TEST_FOR_SHUTDOWN_INDICATION;

	// 8/11/2015 rmd : The init for station preserves MUST come AFTER the read out of the
	// station history file. This is because the preserves contains potentially unwritten
	// records that must be added to the completed records strucutre AFTER the file has been
	// read from the file system.
	init_battery_backed_station_preserves( INIT_ONLY_IF_CHECK_FAILS );
	
	// ---------------------------
	
	// 6/8/2012 rmd : The initialization of the system_preserves and the foal_irri structure are
	// linked. The system_preserves initialization does very little and count on the foal_irri
	// initialization to complete the system_preserves restart. Which it does after the list of
	// stations ON is unwound. The system_preserves init should come FIRST however. To indicate
	// the flow recording memory has not been allocated.
	TEST_FOR_SHUTDOWN_INDICATION;
	init_battery_backed_system_preserves( INIT_ONLY_IF_CHECK_FAILS );
	
	TEST_FOR_SHUTDOWN_INDICATION;
	SYSTEM_PRESERVES_synchronize_preserves_to_file();
	
	// ---------------------------
	
	// 7/18/2012 rmd : Must come after the battery backed system_preserves is verified and
	// initialized. As the poc_preserve records contain a ptr to their corresponding
	// system_preserves record.
	TEST_FOR_SHUTDOWN_INDICATION;
	init_battery_backed_poc_preserves( INIT_ONLY_IF_CHECK_FAILS );
	
	TEST_FOR_SHUTDOWN_INDICATION;
	POC_PRESERVES_synchronize_preserves_to_file();

	// ---------------------------
	
	// 7/18/2012 rmd : Must come after the battery backed system_preserves is verified and
	// initialized. As the lights_preserve records contain a ptr to their corresponding
	// system_preserves record.
	TEST_FOR_SHUTDOWN_INDICATION;
	init_battery_backed_lights_preserves( (INIT_ONLY_IF_CHECK_FAILS) );

	// ---------------------------

	// 6/18/2012 rmd : And the foal_irri init causes the resync of the copy of the user settings
	// in the system_preserve. This foal_irri init must come after the irrigation system file
	// read has happened. And after the system_preserves initialization has happened.
	TEST_FOR_SHUTDOWN_INDICATION;
	init_battery_backed_foal_irri( INIT_ONLY_IF_CHECK_FAILS );

	// 8/13/2015 mpd : The foal lights init verifies the lights battery backed test strings are
	// valid. If not valid, initialization of the battery backed structures is performed. This 
	// foal lights init must come after the lights system file read has happened. And after the 
	// system_preserves initialization has happened.
	TEST_FOR_SHUTDOWN_INDICATION;
	init_battery_backed_foal_lights( (INIT_ONLY_IF_CHECK_FAILS) );

	TEST_FOR_SHUTDOWN_INDICATION;
	init_battery_backed_general_use( INIT_ONLY_IF_CHECK_FAILS );

	// ---------------------------
	
	TEST_FOR_SHUTDOWN_INDICATION;
	init_battery_backed_chain_members( INIT_ONLY_IF_CHECK_FAILS );
	
	/* ---------------------------------------------------------- */
	/* ---------------------------------------------------------- */
	
	// 11/16/2012 rmd : OKAY! Now it is safe to update the code revision string. Do not be
	// tempted to move this to earlier in the startup process. Under dirty power conditions, if
	// we set the string ealier, we would not see a startup as a code update. And assume all the
	// battery backed vars had been update as part of a prior startup/code update sequence. This
	// may not be true. And can produce a FATAL NON_BOOTABLE condition. By not updating the
	// string till here we solve that possibility. [At location 0x80000040 (in the SDRAM) is a
	// string we have the compiler embed. It shows the date and time the startup assembly file
	// was assembled.]
	memset( restart_info.main_app_code_revision_string, 0x00, sizeof(restart_info.main_app_code_revision_string) );

	snprintf( restart_info.main_app_code_revision_string, sizeof(restart_info.main_app_code_revision_string), "%s", MAIN_APP_RUNNING_BUILD_DT );
	
	// ----------
	
	TEST_FOR_SHUTDOWN_INDICATION;

	// ----------
	
	// CREATE THE BALANCE OF THE APPLICATION TASKS NOW
	
	// Buffers into which the flop registers will be saved.  There is a buffer for each task
	// created.  Zeroing out this array is the normal and safe option as this will cause the
	// task to start with all zeros in its flop context.
	memset( &task_table_FLOP_registers, 0x00, sizeof( task_table_FLOP_registers ) );

	// Explicitly indicate each task handle is NULL. Meaning not in use. Important to the
	// powerfail task that the handles not used are indeed NULL.
	memset( &created_task_handles, 0x00, sizeof( created_task_handles ) );

	// ----------

	TASK_ENTRY_STRUCT	*pTaskEntry;

	UNS_32				n;
	
	xTaskHandle			just_created_task;  // ptr to handle for new task 

	pTaskEntry = (TASK_ENTRY_STRUCT*)&Task_Table;

	for( n=0; n<sizeof(Task_Table)/sizeof(TASK_ENTRY_STRUCT); n++, pTaskEntry++ )
	{
		if( pTaskEntry->bCreateTask == TRUE )
		{
			TEST_FOR_SHUTDOWN_INDICATION;

			// Create the task for this entry
			xTaskCreate( pTaskEntry->pTaskFunc,

						 (signed char *) pTaskEntry->TaskName,

						 (UNS_16)pTaskEntry->stack_depth,

						 pTaskEntry,	// the parameter is the task table entry address itself

						 pTaskEntry->priority,

						 &just_created_task
					   );

			OS_make_task_VFP_capable( just_created_task, (void*)&(task_table_FLOP_registers[ n ]) );

			// set the global task handle
			created_task_handles[ n ] = just_created_task;
			
			// ----------
			
			// 3/10/2017 rmd : Capture the 2 flash storage task handles.
			if( (pTaskEntry->pTaskFunc == FLASH_STORAGE_flash_storage_task) && (pTaskEntry->parameter == (void*)FLASH_INDEX_0_BOOTCODE_AND_EXE) )
			{
				flash_storage_0_task_handle = just_created_task;
			}

			if( (pTaskEntry->pTaskFunc == FLASH_STORAGE_flash_storage_task) && (pTaskEntry->parameter == (void*)FLASH_INDEX_1_GENERAL_STORAGE) )
			{
				flash_storage_1_task_handle = just_created_task;
			}

			// ----------

			// 3/5/2014 rmd : Pick out these 3 to support TPMICRO ISP code update process.

			if( pTaskEntry->pTaskFunc == COMM_MNGR_task )
			{
				comm_mngr_task_handle = just_created_task;
			}

			if( (pTaskEntry->pTaskFunc == ring_buffer_analysis_task) && (pTaskEntry->parameter == (void*)UPORT_TP) )
			{
				port_tp_ring_buffer_analysis_task_handle = just_created_task;
			}

			if( (pTaskEntry->pTaskFunc == serial_driver_task) && (pTaskEntry->parameter == (void*)UPORT_TP) )
			{
				port_tp_serial_driver_task_handle = just_created_task;
			}

			// ----------
		}
	}

	// ----------
	
	// 9/7/2017 rmd : Now that all tasks exist, activate the CI task. This allows an orderly
	// startup. The CI task in turn activates the COMM_MNGR task, after a bit.
	CONTROLLER_INITIATED_post_event( CI_EVENT_commence );
	
	// ----------

	// We're all done!
	vTaskDelete( NULL );  // Delete ourselves ... we never return from this call.

	// ----------

	while( (true) );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

