/*  file = device_EN_UDS1100.h                06.28.2013  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_DEVICE_EN_UDS1100_H
#define INC_DEVICE_EN_UDS1100_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"device_common.h"	// Needed for extraction functions.

// PuTTY output:
/* 
*** Lantronix UDS1100-IAP Device Server ***
MAC address 00204AB2463E
Software version V6.1.0.3 (060825) UDS1100
Press Enter for Setup Mode				<- CRLF

Model: Device Server Plus+! (Firmware Code:UA)

*** basic parameters
Hardware: Ethernet TPI
IP addr - 0.0.0.0/DHCP/BOOTP/AutoIP, no gateway set
DHCP device name : not set

*** Security
SNMP is              enabled
SNMP Community Name: public
Telnet Setup is      enabled
TFTP Download is     enabled
Port 77FEh is        enabled
Web Server is        enabled
Web Setup is         enabled
ECHO is              disabled
Enhanced Password is disabled

*** Channel 1
Baudrate 9600, I/F Mode 4C, Flow 00
Port 10001
Connect Mode : C0
Send '+++' in Modem Mode enabled
Auto increment source port disabled
Remote IP Adr: --- none ---, Port 00000
Disconn Mode : 00
Flush   Mode : 00

*** Expert
TCP Keepalive    : 45s
ARP cache timeout: 600s
Monitor Mode @ bootup : enabled
HTTP Port Number : 80
MTU Size: 1400
Alternate MAC: disabled
Ethernet connection type: auto-negotiate

Change Setup:
  0 Server
  1 Channel 1
  5 Expert
  6 Security
  7 Defaults
  8 Exit without save
  9 Save and exit            Your choice ? 0

IP Address : (000) 000.(000) 000.(000) 5.(000) 000
Set Gateway IP Address (N) ? N
Netmask: Number of Bits for Host Part (0=default) (0) 0
Change telnet config password (N) ? N
Change DHCP device name (not set) ? (N) ? N


Change Setup:
  0 Server
  1 Channel 1
  5 Expert
  6 Security
  7 Defaults
  8 Exit without save
  9 Save and exit            Your choice ? 0			<- "0" CRLF

IP Address : (000) 
IP Address : (000) 0.						<- "0."
IP Address : (000) 0.(000) 
IP Address : (000) 0.(000) 0.					<- "0."
IP Address : (000) 0.(000) 0.(000)
IP Address : (000) 0.(000) 0.(000) 0.				<- "0."
IP Address : (000) 0.(000) 0.(000) 0. (000)
IP Address : (000) 0.(000) 0.(000) 0. (000) 0			<- "0" CRLF
Set Gateway IP Address (N) ? N					<- "N"
Netmask: Number of Bits for Host Part (0=default) (0) 0		<- "0" CRLF
Change telnet config password (N) ? N				<- "N"
Change DHCP device name (not set) ? (N) ? N			<- "N"


Change Setup:
  0 Server
  1 Channel 1
  5 Expert
  6 Security
  7 Defaults
  8 Exit without save
  9 Save and exit            Your choice ?

*/ // End of PuTTY output.

// 4/8/2015 mpd : EN State Machine States.
#define EN_DEVEX_READ						(4000)
#define EN_DEVEX_WRITE						(5000)
#define EN_DEVEX_FINISH						(6000)

#define EN_TICKS_PER_MS						( 5 )

// 4/23/2015 mpd : The initial large response from the Lantronix is 1053 bytes. At 9600 baud, that's 1.097 seconds to
// receive our expected termination string! Add 10% for possible errors. That's 1.2 seconds.
//#define EN_COMM_TIMER_MS		( 1200 )
// 5/20/2015 mpd : We are getting timeouts in the field on the first command. Wait longer.
#define EN_COMM_TIMER_MS		( 10000 )

// 4/24/2015 mpd : When switching to MONITOR MODE, the Lantronix device needs to power-cycle, reboot and read network
// settings (we need MONITOR MODE with network connections enabled: "zzz"). This takes about 6 seconds if connected to a
// network. If the network is not responding, the first MONITOR MODE response is around 15 seconds after the internal
// network timeout. That's well beyond what COMM_MNGR will allow, so stick to 10 seconds for mode entry.
#define EN_COMM_MODE_ENTRY_MS	( 10000 )

// 3/19/2015 mpd : TODO: Add more errors and use them!
#define EN_ERROR_NONE			( 0 )

#define NULL_TERM_LEN (1)
// 3/13/2015 mpd : These are not derived from Lantronix responses.
#define EN_EMPTY_STR									( 0xFF ) // Special case for initial I/O cycle. 
#define EN_EMPTY_STR_LEN								( 1 ) 	 // Length > 0 is required. 
#define EN_CRLF_STR										( "\r\n" )
#define EN_CRLF_STR_LEN									( 2 + NULL_TERM_LEN )
#define EN_CR_STR										( "\r" )
#define EN_CR_STR_LEN									( 1 + NULL_TERM_LEN )
#define EN_LF_STR										( "\n" )
#define EN_LF_STR_LEN									( 1 + NULL_TERM_LEN )
#define EN_NULL_IP_ADD_STR				 				( "0.0.0.0" ) 
#define EN_NULL_IP_OCTET_STR			 				( "0" ) 
#define EN_NULL_IP_ADD_LEN				 				( 7 + NULL_TERM_LEN ) 
#define EN_IP_ADD_LEN 					 				( 15 + NULL_TERM_LEN ) 
#define EN_IP_ADDRESS_OCTET_LEN						   	( 3 + EN_CRLF_STR_LEN )
#define EN_MASK_BITS_LEN					 			( 2 + EN_CRLF_STR_LEN )

// 9/24/2015 mpd : FogBugz #3178: EN needs a default mask for device responses that do not include the field. 
#define EN_DEFAULT_IP_ADD_STR			 				( "255.255.255.0" ) 

#define EN_PVS_TOKEN_STR    ( "PVS" )
#define EN_PVS_TOKEN_LEN    ( 4 )

#define EN_IDLE_OPERATION					( 73 )		// "I"
#define EN_READ_SETUP_OPERATION				( 83 )		// "S"
#define EN_READ_MONITOR_OPERATION			( 77 )		// "M"
#define EN_WRITE_OPERATION					( 87 )		// "W"
#define EN_IDLE_TEXT_STR					( "IDLE" )
#define EN_SETUP_READ_TEXT_STR				( "S-READ" )
#define EN_MONITOR_READ_TEXT_STR			( "M-READ" )
#define EN_DHCP_WRITE_TEXT_STR				( "D-WRITE" )
#define EN_STATIC_WRITE_TEXT_STR			( "S-WRITE" )
#define EN_OPERATION_STR_LEN				( 7 + NULL_TERM_LEN )
#define EN_INFO_TEXT_LEN					( 39 + NULL_TERM_LEN ) 	// This should match the size of GuiVar_SRInfoText.  
#define EN_PROGRESS_TEXT_LEN				( 39 + NULL_TERM_LEN ) 	// This should match the size of GuiVar_SRProgressText.  
#define EN_ALERT_MESSAGE_LEN				( 60 + NULL_TERM_LEN )  // 60 is about the limit of the alert screen width. More requires scrolling.  

// 4/27/2015 mpd : NOTE: Most of the Lantronix programming command strings require a CR following the text. A CRLF does 
// not work!. This EN programming code requires the null termination to make life simpler when dealing with strings.

// 3/11/2015 mpd : These are text strings of interest on the Lantronix initial response and subsequent menus. The
// offsets identify the location of data in relation to the "anchor" text string. Include an extra character in the
// length since it will be null-terminated during extraction.

// 4/17/2015 mpd : This is the SETUP MODE response. It is entered with a power cycle followed by "xxx". There are four
// lines of immediate response before the device requires a "CRLF" to continue.
 
// 4/17/2015 mpd : The SETUP MODE response has been broken up into five sections to organize parsing defines. There is
// the "CRLF" after line four, and then a prompt for commands at the end of the long response. There is no way to get
// back to this initial response without exiting and reentering SETUP MODE.
 
//*** Lantronix UDS1100-IAP Device Server ***
//MAC address 00204AB2463E
//Software version V6.1.0.3 (060825) UDS1100
//Press Enter for Setup Mode
//
//Model: Device Server Plus+! (Firmware Code:UA)
//01234567890123456789012345678901234567890123456789012345678901234567890	<- Ruler
//
//*** basic parameters
//Hardware: Ethernet TPI
//IP addr - 0.0.0.0/DHCP/BOOTP/AutoIP, no gateway set
//DHCP device name : not set
//01234567890123456789012345678901234567890123456789012345678901234567890	<- Ruler

// 6/12/2015 mpd : V6.11.0.0 
//*** basic parameters
//Hardware: Ethernet TPI
//IP addr - 0.0.4.0/DHCP/AutoIP, no gateway set,netmask 255.255.255.0
//DNS Server not set							// New for V6.11.0.0
//DHCP device name : 00204AD7162D
//DHCP FQDN option: Disabled					// New for V6.11.0.0

#define EN_SERVER_INFO_DEVICE_STR		    	"*** Lantronix"
#define EN_SERVER_INFO_DEVICE_OFFSET	    	( 14 )
#define EN_SERVER_INFO_DEVICE_LEN		    	( 29 + NULL_TERM_LEN )	// This is the value currently in use.

#define EN_SERVER_INFO_TITLE_STR		    	"*** Lantronix UDS1100-IAP Device Server ***"

// 4/17/2015 mpd : Lantronix uses the MAC address as the device serial number.
#define EN_SERVER_INFO_MAC_ADD_STR				"MAC address"
#define EN_SERVER_INFO_MAC_ADD_OFFSET			( 12 )
#define EN_SERVER_INFO_MAC_ADD_LEN				( 12 + NULL_TERM_LEN )	

// 4/22/2015 mpd : Software Version contains 3 space delimited fields. We only want the first one. The current value 
// has 8 characters plus null. Save space for each of the 4 numbers to be 3 digits long. 
#define EN_SERVER_INFO_SW_VERSION_STR			"Software version"		
#define EN_SERVER_INFO_SW_VERSION_OFFSET		( 17 )
#define EN_SERVER_INFO_SW_VERSION_LEN			( 16 + NULL_TERM_LEN )	

#define EN_SERVER_INFO_SETUP_STR				"Mode"	// "Press Enter for Setup Mode"

#define EN_SERVER_INFO_MODEL_STR  				"Model:"
#define EN_SERVER_INFO_MODEL_OFFSET  			( 7 )
#define EN_SERVER_INFO_MODEL_LEN  				( 39 + NULL_TERM_LEN )	// This is the value currently in use.

#define EN_SERVER_INFO_FW_CODE_STR  			"Firmware Code:"
#define EN_SERVER_INFO_FW_CODE_OFFSET  			( 14 )
#define EN_SERVER_INFO_FW_CODE_LEN  			( 2 + NULL_TERM_LEN )	

#define EN_BASIC_INFO_TITLE_STR		 		   	"*** basic parameters"

#define EN_BASIC_INFO_HW_STR  					"Hardware:"
#define EN_BASIC_INFO_HW_OFFSET  				( 10 )
#define EN_BASIC_INFO_HW_LEN  					( 12 + NULL_TERM_LEN )	// This is the value currently in use.

#define EN_BASIC_INFO_IP_ADD_STR  				"IP addr "
#define EN_BASIC_INFO_IP_ADD_HYPHEN_STR			"-"
#define EN_BASIC_INFO_IP_ADD_OFFSET 			( 8 )
#define EN_BASIC_INFO_IP_ADD_HYPHEN_OFFSET 		( 10 )
#define EN_BASIC_INFO_IP_ADD_LEN  				( 15 + NULL_TERM_LEN )	// This is the MAXIMUM value! Shorter addresses will have to be parsed. 

#define EN_BASIC_INFO_GW_ADD_STR  				", gateway"				 
#define EN_BASIC_INFO_GW_ADD_OFFSET 			( 10 )
#define EN_BASIC_INFO_GW_ADD_LEN  				( 15 + NULL_TERM_LEN )	// This is the MAXIMUM value! Shorter addresses will have to be parsed. 

#define EN_BASIC_INFO_GW_NOT_SET_STR			"no gateway set"	 

#define EN_BASIC_INFO_MASK_STR  				",netmask"		 
#define EN_BASIC_INFO_MASK_OFFSET	 			( 9 )
#define EN_BASIC_INFO_MASK_LEN  				( 15 + NULL_TERM_LEN )	// This is the MAXIMUM value! Shorter addresses will have to be parsed. 

#define EN_BASIC_INFO_DHCP_NAME_STR  			"DHCP device name :"
#define EN_BASIC_INFO_DHCP_NAME_OFFSET 			( 19 )
#define EN_BASIC_INFO_DHCP_NAME_LEN  			( 16 + NULL_TERM_LEN )	// Length matches GUIVar length. 

#define EN_BASIC_INFO_NAME_NOT_SET_STR			"not set"	 
#define EN_BASIC_INFO_NAME_NOT_SET_LEN			( 7 )	 

// 4/24/2015 mpd : SERVER MENU during DHCP enabled programming.
//IP Address : (010) 0.(000) 0.(030) 0.(040) 0 
//Set Gateway IP Address (Y) ? Y 
//Gateway IP addr (050) 0.(060) 0.(070) 0.(080) 0 
//Netmask: Number of Bits for Host Part (0=default) (8) 16 
//Change telnet config password (N) ? N 
//Change DHCP device name (not set) ? (N) ? Y                             <- Only for DHCP enabled
//Enter new DHCP device Name :                            				  <- Only for DHCP enabled
//01234567890123456789012345678901234567890123456789012345678901234567890 <-Ruler 

// 4/27/2015 mpd : SERVER MENU during DHCP disabled programming.
//IP Address : (000) 100.(000) 110.(000) 120.(000) 130
//Set Gateway IP Address (N) ? Y
//Gateway IP addr (000) 200.(000) 210.(000) 220.(000) 230
//Netmask: Number of Bits for Host Part (0=default) (8) 8
//Change telnet config password (N) ? N
//01234567890123456789012345678901234567890123456789012345678901234567890 <-Ruler 
 
// 6/12/2015 mpd : V6.11.0.0 SERVER MENU during DHCP enabled programming.
//IP Address : (000) 0.(000) 0.(005) 4.(000) 0
//Set Gateway IP Address (N) ? N
//Netmask: Number of Bits for Host Part (0=default) (8) 8
//Set DNS Server IP addr  (N) ?
//Change Telnet/Web Manager password (N) ? N
//Change DHCP device name (not set) ? (N) ? Y
//Enter new DHCP device Name : 00204AD7162D
//Enable DHCP FQDN option :  (N) ?

#define EN_SERVER_INPUT_IP_ADD_STR 				"IP Address :"
#define EN_SERVER_INPUT_GATEWAY_YN_STR			"Set Gateway IP Address"
#define EN_SERVER_INPUT_GATEWAY_IP_STR			"Gateway IP addr"
#define EN_SERVER_INPUT_NETMASK_STR				"Netmask: Number of Bits for Host Part (0=default)"
#define EN_SERVER_INPUT_DHCP_NAME_YN_STR		"Change DHCP device name"
#define EN_SERVER_INPUT_DHCP_NAME_STR			"Enter new DHCP device Name"
#define EN_SERVER_INPUT_IP_ADD_RESP_STR			")"
#define EN_SERVER_INPUT_QUESTION_STR			"?"
#define EN_SERVER_INPUT_COLON_STR				":"

// 6/12/2015 mpd : Lantronix changed the text of the Telnet question in V6.11.0.0. Search for the text tht is common.
//#define EN_SERVER_INPUT_TELNET_YN_STR 		"Change telnet config password (N)"			// V6.9.0.3
//#define EN_SERVER_INPUT_TELNET_YN_STR 		"Change Telnet/Web Manager password (N)"	// V6.11.0.0
#define EN_SERVER_INPUT_TELNET_YN_STR 			"password (N)"								// Works for both

// 6/12/2015 mpd : Firmware V6.11.0.0 added a question following NetMask. 
#define EN_SERVER_INPUT_DNS_IP_YN_STR			"Set DNS Server IP addr "

// 6/8/2015 mpd : Firmware V6.11.0.0 added a question following DHCP name. 
#define EN_SERVER_INPUT_FQDN_YN_STR				"Enable DHCP FQDN"

// 4/29/2015 mpd : Programming input strings need an extra character for the CR.
#define EN_SERVER_INPUT_IP_ADD_LEN 				( 15 + EN_CR_STR_LEN )	// This is the MAXIMUM value! Shorter addresses will have to be parsed. 
#define EN_SERVER_INPUT_GW_ADD_LEN  			( 15 + EN_CR_STR_LEN )	// This is the MAXIMUM value! Shorter addresses will have to be parsed. 
#define EN_SERVER_INPUT_MASK_LEN  				( 15 + EN_CR_STR_LEN )	// This is the MAXIMUM value! Shorter addresses will have to be parsed. 
#define EN_SERVER_INPUT_DHCP_NAME_LEN  			( 16 + EN_CR_STR_LEN )	// Length matches GUIVar length. 

// 4/17/2015 mpd : The SECURITY menu is not analyzed or modified at this time.
//*** Security
//SNMP is              enabled
//SNMP Community Name: public
//Telnet Setup is      enabled
//TFTP Download is     enabled
//Port 77FEh is        enabled
//Web Server is        enabled
//Web Setup is         enabled
//ECHO is              disabled
//Enhanced Password is disabled 
//01234567890123456789012345678901234567890123456789012345678901234567890	<- Ruler

#define EN_SECURITY_INFO_TITLE_STR		    	"*** Security"

//*** Channel 1
//Baudrate 9600, I/F Mode 4C, Flow 00
//Port 10001
//Connect Mode : C0
//Send '+++' in Modem Mode enabled
//Auto increment source port disabled
//Remote IP Adr: --- none ---, Port 00000
//Disconn Mode : 00
//Flush   Mode : 00
//01234567890123456789012345678901234567890123456789012345678901234567890	<- Ruler

// 6/12/2015 mpd : V6.11.0.0 
//*** Channel 1
//Baudrate 115200, I/F Mode 4C, Flow 02
//Port 10001
//Connect Mode : C5
//Send '+++' in Modem Mode enabled
//Show IP addr after 'RING' enabled				// New for V6.11.0.0
//Auto increment source port enabled
//Remote IP Adr: 64.73.242.99, Port 16001
//Disconn Mode : 80
//Flush   Mode : 00

#define EN_CHANNEL_INFO_TITLE_STR		    	"*** Channel 1"

// 4/17/2015 mpd : Lantronix uses the MAC address as the device serial number.
#define EN_CHANNEL_INFO_BAUD_RATE_STR			"Baudrate"
#define EN_CHANNEL_INFO_BAUD_RATE_OFFSET		( 9 )
#define EN_CHANNEL_INFO_BAUD_RATE_LEN			( 6 + NULL_TERM_LEN )	// This is the MAXIMUM value! Shorter values will have to be parsed.

#define EN_CHANNEL_INFO_IF_MODE_STR				"I/F Mode"
#define EN_CHANNEL_INFO_IF_MODE_OFFSET			( 9 )
#define EN_CHANNEL_INFO_IF_MODE_LEN				( 2 + NULL_TERM_LEN )	

#define EN_CHANNEL_INFO_FLOW_STR				"Flow"
#define EN_CHANNEL_INFO_FLOW_OFFSET				( 5 )
#define EN_CHANNEL_INFO_FLOW_LEN				( 2 + NULL_TERM_LEN )	

#define EN_CHANNEL_INFO_PORT_STR				"Port"
#define EN_CHANNEL_INFO_PORT_OFFSET				( 5 )
#define EN_CHANNEL_INFO_PORT_LEN				( 5 + NULL_TERM_LEN )	// This is the MAXIMUM value! Shorter values will have to be parsed.

#define EN_CHANNEL_INFO_CONN_MODE_STR			"Connect"
#define EN_CHANNEL_INFO_CONN_MODE_OFFSET		( 15 )
#define EN_CHANNEL_INFO_CONN_MODE_LEN			( 2 + NULL_TERM_LEN )	

#define EN_CHANNEL_INFO_AUTO_INCR_STR			"Auto increment source port"
#define EN_CHANNEL_INFO_AUTO_INCR_OFFSET		( 27 )
#define EN_CHANNEL_INFO_AUTO_INCR_LEN			( 8 + NULL_TERM_LEN )	

#define EN_CHANNEL_INFO_ENABLED_STR				"enabled"
#define EN_CHANNEL_INFO_ENABLED_LEN				( 7 + NULL_TERM_LEN )
#define EN_CHANNEL_INFO_DISABLED_STR			"disabled"
#define EN_CHANNEL_INFO_DISABLED_LEN			( 8 + NULL_TERM_LEN )

#define EN_CHANNEL_INFO_REMOTE_IP_STR			"Remote IP Adr:"
#define EN_CHANNEL_INFO_REMOTE_IP_OFFSET		( 15 )
#define EN_CHANNEL_INFO_REMOTE_IP_LEN			( 15 + NULL_TERM_LEN )	// This is the MAXIMUM value! Shorter addresses will have to be parsed.

#define EN_CHANNEL_INFO_RIP_NOT_SET_STR			"--- none ---"	 

// 4/23/2015 mpd : There are two "Port" anchor keys in this response section. Use the ", " to differentiate!
#define EN_CHANNEL_INFO_REMOTE_PORT_STR			", Port"
#define EN_CHANNEL_INFO_REMOTE_PORT_OFFSET		( 7 )
#define EN_CHANNEL_INFO_REMOTE_PORT_LEN			( 5 + NULL_TERM_LEN )	

#define EN_CHANNEL_INFO_DISCONN_MODE_STR		"Disconn Mode :"
#define EN_CHANNEL_INFO_DISCONN_MODE_OFFSET		( 15 )
#define EN_CHANNEL_INFO_DISCONN_MODE_LEN		( 2 + NULL_TERM_LEN )	

#define EN_CHANNEL_INFO_FLUSH_MODE_STR			"Flush   Mode :"
#define EN_CHANNEL_INFO_FLUSH_MODE_OFFSET		( 15 )
#define EN_CHANNEL_INFO_FLUSH_MODE_LEN			( 2 + NULL_TERM_LEN )	

// 4/27/2015 mpd : CHANNEL MENU during programming.
//Baudrate (9600) ? 
//I/F Mode (4C) ? 
//Flow (00) ? 
//Port No (10001) ? 
//ConnectMode (C0) ? 
//Send '+++' in Modem Mode (Y) ? 
//Auto increment source port  (N) ? 
//Remote IP Address : (000) 
//Remote Port  (0) ? 
//DisConnMode (00) ? 
//FlushMode (00) ? 
//DisConnTime (00:00) ? 
//SendChar 1 (00) ? 
//SendChar 2  (00) ? 
//01234567890123456789012345678901234567890123456789012345678901234567890 <-Ruler

// 6/12/2015 mpd : V6.11.0.0 CHANNEL MENU during programming.
//Baudrate (115200) ? 115200
//I/F Mode (4C) ? 4C
//Flow (02) ? 02
//Port No (10001) ? 10001
//ConnectMode (C5) ? C5
//Send '+++' in Modem Mode  (Y) ? Y
//Show IP addr after 'RING'  (Y) ?
//Auto increment source port  (Y) ? Y
//Remote IP Address : (192) 64.(168) 73.(254) 242.(137) 99
//Remote Port  (16001) ? 16001
//DisConnMode (80) ? 80
//FlushMode   (00) ? 00
//DisConnTime (00:00) ?00:00
//SendChar 1  (00) ? 00
//SendChar 2  (00) ? 00

#define EN_CHANNEL_INPUT_BAUD_RATE_STR 			"Baudrate"
#define EN_CHANNEL_INPUT_IF_MODE_STR 			"I/F Mode"
#define EN_CHANNEL_INPUT_FLOW_STR 				"Flow"
#define EN_CHANNEL_INPUT_PORT_STR 				"Port No"
#define EN_CHANNEL_INPUT_CONN_MODE_STR 			"ConnectMode"
#define EN_CHANNEL_INPUT_SEND_STR 				"Send '+++' in Modem Mode"
#define EN_CHANNEL_INPUT_SHOW_IP_RING_STR		"Show IP addr after"			// "Show IP addr after 'RING'  (Y)"
#define EN_CHANNEL_INPUT_AUTO_INCR_STR 			"Auto increment source port"
#define EN_CHANNEL_INPUT_REMOTE_IP_STR  		"Remote IP Address"
#define EN_CHANNEL_INPUT_REMOTE_PORT_STR		"Remote Port"
#define EN_CHANNEL_INPUT_DISCONN_MODE_STR		"DisConnMode" 
#define EN_CHANNEL_INPUT_FLUSH_MODE_STR     	"FlushMode"
#define EN_CHANNEL_INPUT_DISCONN_TIME_STR     	"DisConnTime"
#define EN_CHANNEL_INPUT_SEND_CHAR_1_STR     	"SendChar 1"
#define EN_CHANNEL_INPUT_SEND_CHAR_2_STR     	"SendChar 2"
#define EN_CHANNEL_INPUT_IP_ADD_RESP_STR		")"
#define EN_CHANNEL_INPUT_QUESTION_STR			"?"
#define EN_CHANNEL_INPUT_COLON_STR				":"
 
// 4/29/2015 mpd : Programming input strings need an extra character for the CR.
#define EN_CHANNEL_INPUT_BAUD_RATE_LEN 			( 6 + EN_CR_STR_LEN )
#define EN_CHANNEL_INPUT_IF_MODE_LEN			( 2 + EN_CR_STR_LEN )	
#define EN_CHANNEL_INPUT_FLOW_LEN				( 2 + EN_CR_STR_LEN )	
#define EN_CHANNEL_INPUT_PORT_LEN				( 5 + EN_CR_STR_LEN )	// This is the MAXIMUM value! Shorter values will have to be parsed.
#define EN_CHANNEL_INPUT_CONN_MODE_LEN			( 2 + EN_CR_STR_LEN )	
#define EN_CHANNEL_INPUT_AUTO_INCR_LEN			( 8 + EN_CR_STR_LEN )	
#define EN_CHANNEL_INPUT_REMOTE_IP_LEN			( 15 + EN_CR_STR_LEN )	// This is the MAXIMUM value! Shorter addresses will have to be parsed.
#define EN_CHANNEL_INPUT_REMOTE_PORT_LEN		( 5 + EN_CR_STR_LEN )	
#define EN_CHANNEL_INPUT_DISCONN_MODE_LEN		( 2 + EN_CR_STR_LEN )	
#define EN_CHANNEL_INPUT_FLUSH_MODE_LEN			( 2 + EN_CR_STR_LEN )	
 
// 4/17/2015 mpd : The EXPERT menu is not analyzed or modified at this time.
//*** Expert
//TCP Keepalive    : 45s
//ARP cache timeout: 600s
//Monitor Mode @ bootup : enabled
//HTTP Port Number : 80
//MTU Size: 1400
//Alternate MAC: disabled
//Ethernet connection type: auto-negotiate
//01234567890123456789012345678901234567890123456789012345678901234567890	<- Ruler 
 
#define EN_EXPERT_INFO_TITLE_STR		   		"*** Expert"

//Change Setup:
//  0 Server
//  1 Channel 1
//  5 Expert
//  6 Security
//  7 Defaults
//  8 Exit without save
//  9 Save and exit            Your choice ?
#define EN_CHANGE_MENU_TITLE_STR		"Change Setup:"
#define EN_CHANGE_MENU_CHOICE_STR		"Your choice ?"
#define EN_CHANGE_MENU_EXIT_WO_STR		"exiting without save !"
#define EN_CHANGE_SUCCESS_STR			"Parameters stored ..."
#define EN_NO_TEXT_STR					""

// 4/17/2015 mpd : This is the MONITOR MODE response. It is entered with a power cycle followed by "zzz". There is only
// one line of initial response followed by a prompt for commands. The VS and GC commands are useless until we can get a
// breakdown of the response.
 
//*** NodeSet 2.0 ***
//9>VS
//:10000000001010065541F201CD0D000062A7A7A512
//:00000001FF
//0>GC
//:20000010000000000000002D00000000000000004C0200001127000000000000C00000005D
//:200020100000000000000000000000000000000000000000000000000000000000000000B0
//:200040104C0200001227000000000000C00000000000000000000000000000000000000049
//:1800601000000000000000000000000000000000000000000000000078
//:00000001FF
//0>GM
//00:20:4A:B2:46:3E
//0>NC
//IP 192.168.254.112 GW 192.168.254.250 Mask 255.255.255.000
//0>

//#define EN_MON_MODE_TITLE_STR			    	"*** NodeSet 2.0 ***"
#define EN_MON_MODE_TITLE_STR			    	"NodeSet 2.0"
#define EN_MON_MODE_PROMPT_STR			    	"0>"
#define EN_MON_MODE_QU_STR				    	"QU"

// 4/24/2015 mpd : The command sent to the Lantronix in MONITOR MODE appears in the response followed by CRLF. It can be
// used as the title string for analyzing the response.
#define EN_MON_MODE_GM_TITLE_STR		    	"GM"
#define EN_MON_MODE_MAC_ADD_STR					""						
#define EN_MON_MODE_MAC_ADD_OFFSET				( 4 )					// This is the offset from "GM\r\n"
#define EN_MON_MODE_MAC_ADD_LEN					( 17 + NULL_TERM_LEN )	

#define EN_MON_MODE_NC_TITLE_STR		    	"NC"
#define EN_MON_MODE_IP_ADD_STR					"IP"
#define EN_MON_MODE_IP_ADD_OFFSET				( 3 )
#define EN_MON_MODE_IP_ADD_LEN					( 15 + NULL_TERM_LEN )	// This is the MAXIMUM value! Shorter values will have to be parsed.

#define EN_MON_MODE_GW_ADD_STR					"GW"
#define EN_MON_MODE_GW_ADD_OFFSET				( 3 )
#define EN_MON_MODE_GW_ADD_LEN					( 15 + NULL_TERM_LEN )	// This is the MAXIMUM value! Shorter values will have to be parsed.

#define EN_MON_MODE_MASK_STR					"Mask"
#define EN_MON_MODE_MASK_OFFSET					( 5 )
#define EN_MON_MODE_MASK_LEN					( 15 + NULL_TERM_LEN )	

#define EN_LONGEST_COMMAND_LEN					( EN_BASIC_INFO_DHCP_NAME_LEN + EN_CRLF_STR_LEN ) // This is the longest known command. Update it if a longer one is found.

//#define EN_SUPPORT_MONITOR_MODE

// 6/9/2015 mpd : This struct needs to be named for the forward declaration (needed by DEV_STATE_STRUCT) to work.
typedef struct EN_PROGRAMMABLE_VALUES_STRUCT {

	char pvs_token[ EN_PVS_TOKEN_LEN ];

	// 4/20/2015 mpd : These device parameters are read from the SETUP MODE response.
	char ip_address[ EN_SERVER_INPUT_IP_ADD_LEN ];  
	char ip_address_1[ EN_IP_ADDRESS_OCTET_LEN ];            
	char ip_address_2[ EN_IP_ADDRESS_OCTET_LEN ];            
	char ip_address_3[ EN_IP_ADDRESS_OCTET_LEN ];            
	char ip_address_4[ EN_IP_ADDRESS_OCTET_LEN ];            
	char dhcp_name[ EN_SERVER_INPUT_DHCP_NAME_LEN ];                      

	char baud_rate[ EN_CHANNEL_INPUT_BAUD_RATE_LEN ];
	char if_mode[ EN_CHANNEL_INPUT_IF_MODE_LEN ];
	char flow[ EN_CHANNEL_INPUT_FLOW_LEN ];
	char port[ EN_CHANNEL_INPUT_PORT_LEN ];
	char conn_mode[EN_CHANNEL_INPUT_CONN_MODE_LEN];
	BOOL_32 auto_increment;

	// 4/28/2015 mpd : Whole IP addresses are read from the device and used in decision making. Octets are set by 
	// individual GUI fields and are used primarily to program the device. It's important to keep both types of strings
	// in sync.
	char remote_ip_address[ EN_CHANNEL_INPUT_REMOTE_IP_LEN ];              
	char remote_ip_address_1[ EN_IP_ADDRESS_OCTET_LEN ];            
	char remote_ip_address_2[ EN_IP_ADDRESS_OCTET_LEN ];            
	char remote_ip_address_3[ EN_IP_ADDRESS_OCTET_LEN ];            
	char remote_ip_address_4[ EN_IP_ADDRESS_OCTET_LEN ];            

	char remote_port[ EN_CHANNEL_INPUT_REMOTE_PORT_LEN ];
	char disconn_mode[EN_CHANNEL_INPUT_DISCONN_MODE_LEN];
	char flush_mode[EN_CHANNEL_INPUT_FLUSH_MODE_LEN];

	char gw_address[ EN_SERVER_INPUT_GW_ADD_LEN ];              
	char gw_address_1[ EN_IP_ADDRESS_OCTET_LEN ];            
	char gw_address_2[ EN_IP_ADDRESS_OCTET_LEN ];            
	char gw_address_3[ EN_IP_ADDRESS_OCTET_LEN ];            
	char gw_address_4[ EN_IP_ADDRESS_OCTET_LEN ]; 
	           
	// 9/24/2015 mpd : FogBugz #3178: The mask string is needed after all (for determining the mask bit count after 
	// device read operations)! Reactivate this field! Leave the octets commented out.
	// 5/1/2015 mpd : These fields are not currently needed by EN Programming. They have been tested and are functional.
	char mask[ EN_SERVER_INPUT_MASK_LEN ]; 
	//char mask_1[ EN_IP_ADDRESS_OCTET_LEN ]; 
	//char mask_2[ EN_IP_ADDRESS_OCTET_LEN ]; 
	//char mask_3[ EN_IP_ADDRESS_OCTET_LEN ]; 
	//char mask_4[ EN_IP_ADDRESS_OCTET_LEN ];

	// 4/20/2015 mpd : The SECURITY and EXPERT responses are not read or analyzed at this time. Those device attributes 
	// are not part of the Calsense programming at this time.

	// 4/29/2015 mpd : This structure is currently 196 bytes.

} EN_PROGRAMMABLE_VALUES_STRUCT;

// 6/9/2015 mpd : This struct needs to be named for the forward declaration (needed by DEV_STATE_STRUCT) to work.
typedef struct EN_DETAILS_STRUCT {

	// 9/23/2015 mpd : FogBugz #3164: The Lantronix EN UDS1100 does not use CIDR (Classless Inter Domain Routing) format
	// to define the network mask. The number of bits indicates the available bits for host addressing (e.g. A mask of 
	// "8" produces a netmask of "255.255.255.0" which has 8 bits available for hosts on the subnet. Other Lantronix
	// devices use CIDR format for programming (e.g. "192.168.254.150/24" specifies the IP address as well as a network 
	// mask of "255.255.255.0"). The CIDR mask bit value indicates the leading ones; not the trailing zeros.
	// The CS3000 GUI code was written for UDS1100. It's backwards for all newer Lantronix devices. The mask numbers are 
	// internal to CS3000 code. They become external at the GUI and in device programming. To eliminate extensive 
	// rework, the GUI side for all device types will retain pre-CIDR mask values. As mask values pass to and from the 
	// GUI code to the device code (e.g. "e_wen_programming.c" to "device_WEN_PremierWaveXN.c"), the value will be 
	// transformed to match the receiving format.  
	UNS_32 mask_bits;

	// 6/11/2015 mpd : Scenario: DHCP was enabled (a DHCP name existed), it was disabled, and it's about to be 
	// reenabled. We don't want to stomp on a long custom name if we ever knew it.
	// This flag tells us it's safe to stomp on any DHCP name that existed in the device the last time DHCP was enabled.
	// Or conversely, it prevents us from stomping on a name that was temporarily hidden from view during the current 
	// session.
	BOOL_32 dhcp_name_not_set;

	// 4/20/2015 mpd : These values are pulled from the SETUP MODE response. 
	char device[ EN_SERVER_INFO_DEVICE_LEN ];  
	char mac_address[ EN_SERVER_INFO_MAC_ADD_LEN ];
	//char sw_version[ EN_SERVER_INFO_SW_VERSION_LEN ];                

	// 5/1/2015 mpd : These fields are not currently needed by EN Programming. They have been tested and are functional.
	//char model[ EN_SERVER_INFO_MODEL_LEN ];  
	//char firmware_code[ EN_SERVER_INFO_FW_CODE_LEN ];  
	//char hardware[ EN_BASIC_INFO_HW_LEN ];

	// 5/1/2015 mpd : These fields are not currently needed by EN Programming. They have been tested and are functional.
	// 4/20/2015 mpd : These values are pulled from the MONITOR MODE response. If DHCP is enabled, SETUP MODE does not 
	// display current IP address and network settings. We have to use MONITOR MODE.  
	//char mon_mac_address[ EN_MON_MODE_MAC_ADD_LEN ];

    #ifdef EN_SUPPORT_MONITOR_MODE
	// 4/28/2015 mpd : Whole IP addresses are read from the device and used in decision making. These octets will be
	// sent back to the GUI for display. It's important to keep both types of strings in sync.
	char mon_ip_address[ EN_MON_MODE_IP_ADD_LEN ];              
	char mon_ip_address_1[ EN_IP_ADDRESS_OCTET_LEN ];            
	char mon_ip_address_2[ EN_IP_ADDRESS_OCTET_LEN ];            
	char mon_ip_address_3[ EN_IP_ADDRESS_OCTET_LEN ];            
	char mon_ip_address_4[ EN_IP_ADDRESS_OCTET_LEN ];  
	          
	char mon_gw_address[ EN_MON_MODE_GW_ADD_LEN ];              
	char mon_gw_address_1[ EN_IP_ADDRESS_OCTET_LEN ];            
	char mon_gw_address_2[ EN_IP_ADDRESS_OCTET_LEN ];            
	char mon_gw_address_3[ EN_IP_ADDRESS_OCTET_LEN ];            
	char mon_gw_address_4[ EN_IP_ADDRESS_OCTET_LEN ];  
	          
	char mon_mask[ EN_MON_MODE_MASK_LEN ];              
	char mon_mask_1[ EN_IP_ADDRESS_OCTET_LEN ];            
	char mon_mask_2[ EN_IP_ADDRESS_OCTET_LEN ];            
	char mon_mask_3[ EN_IP_ADDRESS_OCTET_LEN ];            
	char mon_mask_4[ EN_IP_ADDRESS_OCTET_LEN ];            
	#endif

	// 5/1/2015 mpd : This structure is currently 320 bytes.

} EN_DETAILS_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern EN_DETAILS_STRUCT en_details;

extern const DEV_MENU_ITEM en_setup_read_list[]; 

extern const DEV_MENU_ITEM en_monitor_read_list[]; 

extern const DEV_MENU_ITEM en_dhcp_write_list[];  

extern const DEV_MENU_ITEM en_static_write_list[];
  
extern void en_copy_active_values_to_static_values( DEV_STATE_STRUCT *dev_state );

extern UNS_32 en_sizeof_setup_read_list();

extern UNS_32 en_sizeof_monitor_read_list();

extern UNS_32 en_sizeof_dhcp_write_list();

extern UNS_32 en_sizeof_static_write_list();

extern void en_initialize_detail_struct( DEV_STATE_STRUCT *dev_state );




extern void UDS1100_initialize_the_connection_process( UNS_32 pport );

extern void UDS1100_connection_processing( UNS_32 pevent );




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

