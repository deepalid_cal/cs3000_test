/*  file = device_WEN_WiBox.c                 03.22.2016  ajv */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	<string.h>

#include	"device_WEN_WiBox.h"

#include	"alerts.h"

#include	"comm_mngr.h"

#include	"configuration_controller.h"

#include	"cs_mem.h"

#include	"device_generic_gr_card.h"

#include	"epson_rx_8025sa.h"

#include	"e_wen_wifi_settings.h"

#include	"gpio_setup.h"

#include	"rcvd_data.h"

#include	"serial.h"

#include	"serport_drvr.h"

#include	"speaker.h"

#include	"controller_initiated.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
 
//#define WIBOX_DEBUG_ANALYSIS
//#define WIBOX_DEBUG_WRITE
//#define WIBOX_DEBUG_VERIFY
//#define WIBOX_DEBUG_MEM_MALLOC
//#define WIBOX_VERBOSE_ALERTS 
//#define WIBOX_SUPPORT_MONITOR_MODE

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/23/2016 ajv : The "DEV_STATE_STRUCT" struct contains fields that apply to all the
// WIBOX_PROGRAMMABLE_VALUES_STRUCT.
WIBOX_DETAILS_STRUCT WIBOX_details;

// 3/23/2015 mpd : NOTE: No functions should directly modify any PVS fields. Use the
// pointers in the "dev_state" struct!
 
// 6/9/2015 mpd : These are the programmable variables structures used by WiBox devices.
// READ operations load the "active_pvs" with values obtained from the device. These values
// are copied to the GUI for users to see. All the user variables are copied back to the
// "active_pvs" values when the "Save Changes" button is selected. Just before programming
// the device, the "active_pvs" is copied to the "static_pvs" which will be used to program
// the device. After the WRITE, a READ is performed to verify the results (using the
// "active_pvs" which again gets copied to the user screen).
WIBOX_PROGRAMMABLE_VALUES_STRUCT WIBOX_active_pvs;

WIBOX_PROGRAMMABLE_VALUES_STRUCT WIBOX_static_pvs;
	 
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  			 String Extraction and Analysis 			  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void WIBOX_analyze_server_info( DEV_STATE_STRUCT *dev_state )
{
	WIBOX_DETAILS_STRUCT *ldetails;

	// ----------

	#ifdef WIBOX_DEBUG_ANALYSIS
		Alert_Message( "WIBOX_analyze_server_info" );
	#endif

	// ----------

	ldetails = dev_state->WIBOX_details;

	// ----------

	// 3/23/2016 ajv : Update easyGUI dialog
	strlcpy( dev_state->info_text, "Reading device settings...", sizeof( dev_state->info_text ) );                
	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );

	// ----------

	// 3/23/2016 ajv : This field is of varying length. Use extraction with a delimiter.
	dev_extract_delimited_text_from_buffer( dev_state->resp_ptr, " ", WIBOX_SERVER_INFO_DEVICE_STR, WIBOX_SERVER_INFO_DEVICE_OFFSET, sizeof( ldetails->device ), ldetails->device, "DEVICE" );

	dev_extract_text_from_buffer( dev_state->resp_ptr, WIBOX_SERVER_INFO_MAC_ADD_STR, WIBOX_SERVER_INFO_MAC_ADD_OFFSET, sizeof( ldetails->mac_address ), ldetails->mac_address, "MAC ADDRESS" );

	// 3/23/2016 ajv : This field is of varying length. Use extraction with a delimiter.
	dev_extract_delimited_text_from_buffer( dev_state->resp_ptr, " ", WIBOX_SERVER_INFO_SW_VERSION_STR, WIBOX_SERVER_INFO_SW_VERSION_OFFSET, sizeof( dev_state->firmware_version ), dev_state->firmware_version, "SW VERSION" );
}

/* ---------------------------------------------------------- */
static void WIBOX_analyze_basic_info( DEV_STATE_STRUCT *dev_state )
{
	WIBOX_DETAILS_STRUCT *ldetails;

	WIBOX_PROGRAMMABLE_VALUES_STRUCT *lactive_pvs;

	char	temp_text[ WIBOX_BASIC_INFO_IP_ADD_LEN ];

	char	*temp_ptr;

	// ----------

	#ifdef WIBOX_DEBUG_ANALYSIS
		Alert_Message( "WIBOX_analyze_basic_info" );
	#endif

	// ----------

	ldetails = dev_state->WIBOX_details;

	lactive_pvs = dev_state->wibox_active_pvs;

	// ----------

	// 3/23/2016 ajv : IP address has a "-" before the first octet if DHCP is enabled. The
	// delimiter changes too. Put the IP text in a local buffer for further analysis.
	dev_extract_text_from_buffer( dev_state->resp_ptr, WIBOX_BASIC_INFO_IP_ADD_STR, WIBOX_BASIC_INFO_IP_ADD_OFFSET, sizeof( temp_text ), temp_text, "IP ADDRESS" );

	// 3/23/2016 ajv : Check for a "-" which is displayed when the unit is in DHCP
	if( strncmp( temp_text, WIBOX_BASIC_INFO_IP_ADD_HYPHEN_STR, 1 ) == NULL )
	{
		// 3/23/2016 ajv : The text is likely "IP addr - 0.0.0.0/DHCP/BOOTP/AutoIP, no gateway set".
		// Use extraction anyway just in case it's something different.
		dev_extract_delimited_text_from_buffer( dev_state->resp_ptr, "/", WIBOX_BASIC_INFO_IP_ADD_STR, WIBOX_BASIC_INFO_IP_ADD_HYPHEN_OFFSET, sizeof( lactive_pvs->ip_address ), lactive_pvs->ip_address, "IP ADDRESS" );
	}
	else
	{
		// 3/23/2016 ajv : The text looks something like: "IP addr 10.20.30.40, gateway
		// 200.44.80.90,netmask 255.255.255.0". Use extraction with a ",' delimiter.
		dev_extract_delimited_text_from_buffer( dev_state->resp_ptr, ",", WIBOX_BASIC_INFO_IP_ADD_STR, WIBOX_BASIC_INFO_IP_ADD_OFFSET, sizeof( lactive_pvs->ip_address ), lactive_pvs->ip_address, "IP ADDRESS" );
	}

	dev_extract_ip_octets( lactive_pvs->ip_address, lactive_pvs->ip_address_1, lactive_pvs->ip_address_2, lactive_pvs->ip_address_3, lactive_pvs->ip_address_4 );

	// 3/23/2016 ajv : Only octets 1 and 2 need to be zero to enable DHCP. No need to test for
	// "0.0.0.0".
	if( ( ( strncmp( lactive_pvs->ip_address_1, WIBOX_NULL_IP_OCTET_STR, sizeof( lactive_pvs->ip_address_1 ) ) ) == NULL ) &&
		( ( strncmp( lactive_pvs->ip_address_2, WIBOX_NULL_IP_OCTET_STR, sizeof( lactive_pvs->ip_address_2 ) ) ) == NULL ) )
	{
		dev_state->dhcp_enabled = (true);
	}
	else
	{
		dev_state->dhcp_enabled = (false);
	}

	// ----------

	// 3/23/2016 ajv : Is a gateway set?
	if( ( strstr( dev_state->resp_ptr, WIBOX_BASIC_INFO_GW_NOT_SET_STR ) ) != NULL )
	{
		// No gateway set
		strlcpy( lactive_pvs->gw_address, WIBOX_NULL_IP_ADD_STR, sizeof(lactive_pvs->gw_address ) );
	}
	else if( ( strstr( dev_state->resp_ptr, WIBOX_BASIC_INFO_GW_ADD_STR ) ) != NULL )
	{
		dev_extract_delimited_text_from_buffer( dev_state->resp_ptr, ",", WIBOX_BASIC_INFO_GW_ADD_STR, WIBOX_BASIC_INFO_GW_ADD_OFFSET, sizeof( lactive_pvs->gw_address ), lactive_pvs->gw_address, "GW ADDRESS" );
	}
	else
	{
		// 3/23/2016 ajv : Unknown gateway format.
		strlcpy( lactive_pvs->gw_address, WIBOX_NULL_IP_ADD_STR, sizeof( lactive_pvs->gw_address ) );
	}

	dev_extract_ip_octets( lactive_pvs->gw_address, lactive_pvs->gw_address_1, lactive_pvs->gw_address_2, lactive_pvs->gw_address_3, lactive_pvs->gw_address_4 );

	// ----------

	// 3/24/2016 ajv : Is there a value for mask?
	if( ( strstr( dev_state->resp_ptr, WIBOX_BASIC_INFO_MASK_STR ) ) != NULL )
	{
		dev_extract_delimited_text_from_buffer( dev_state->resp_ptr, WIBOX_CR_STR, WIBOX_BASIC_INFO_MASK_STR, WIBOX_BASIC_INFO_MASK_OFFSET, sizeof( lactive_pvs->mask ), lactive_pvs->mask, "MASK" );
	}
	else
	{
		// 4/22/2015 mpd : No mask info available.
		strlcpy( lactive_pvs->mask, WIBOX_DEFAULT_IP_ADD_STR, sizeof( lactive_pvs->mask ) );
	}

	// 9/24/2015 mpd : FogBugz #3178: This is what all the work was about. The counting function
	// was written to be generic. Most devices care about the number of bits set, so that's what
	// is returned. UDS1100 cares about the ( false ) bits. Do the math.
	ldetails->mask_bits = ( XE_XN_DEVICE_NETMASK_MAX - dev_count_true_mask_bits( lactive_pvs->mask ) );

	// 3/24/2016 ajv : This should never happen. 
 	if( ( ldetails->mask_bits < EN_PROGRAMMING_NETMASK_MIN ) || ( ldetails->mask_bits > EN_PROGRAMMING_NETMASK_MAX ) )
	{
		ldetails->mask_bits = EN_PROGRAMMING_NETMASK_DEFAULT;
	}

	// ----------

	// 3/24/2016 ajv : V6.8.0.4 added "DNS Server not set" before the DHCP name line. Go past
	// that, or we will get the wrong result for the DHCP name "not set" comparison.

	// ----------

	// 3/24/2016 ajv : The DHCP name line is completely missing if DHCP is disabled. See if it
	// exists before attempting to extract it.
	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, WIBOX_BASIC_INFO_DHCP_NAME_STR, dev_state->resp_len ) ) != NULL )
	{
		// 3/24/2016 ajv : The name line exists. Clear the destination.
		memset( lactive_pvs->dhcp_name, 0x00, WIBOX_BASIC_INFO_DHCP_NAME_LEN );

		if( ( strstr( temp_ptr, WIBOX_BASIC_INFO_NAME_NOT_SET_STR ) ) != NULL )
		{
			if( ldetails->dhcp_name_not_set )
			{
				// 3/24/2016 ajv : No DHCP name is set. Supply the MAC address as the recommended default.
				strlcpy( lactive_pvs->dhcp_name, ldetails->mac_address, sizeof( lactive_pvs->dhcp_name ) );

				// 3/24/2016 ajv : This flag tells us it's safe to stomp on any DHCP name that existed in
				// the device the last time DHCP was enabled. Or conversely, it prevents us from stomping on
				// a name that was temporarily hidden from view during the current session.
				ldetails->dhcp_name_not_set = (false);
			}
		}
		else
		{
			dev_extract_delimited_text_from_buffer( temp_ptr, WIBOX_CR_STR, WIBOX_BASIC_INFO_DHCP_NAME_STR, WIBOX_BASIC_INFO_DHCP_NAME_OFFSET, sizeof( lactive_pvs->dhcp_name ), lactive_pvs->dhcp_name, "DHCP NAME" );

			// 3/24/2016 ajv : This flag tells us it's safe to stomp on any DHCP name that existed in
			// the device the last time DHCP was enabled. Or conversely, it prevents us from stomping on
			// a name that was temporarily hidden from view during the current session.
			ldetails->dhcp_name_not_set = (false);
		}
	}
	// IMPLIED ELSE: DHCP is disabled

	// 3/24/2016 ajv : The current response does not include a DHCP name. One may have existed
	// during this session. Don't clear the string variable or modify the flag.
}

/* ---------------------------------------------------------- */
static void WIBOX_analyze_channel_1_info( DEV_STATE_STRUCT *dev_state )
{
	WIBOX_PROGRAMMABLE_VALUES_STRUCT *lactive_pvs;

	char 	temp_text[ WIBOX_CHANNEL_INFO_AUTO_INCR_LEN ];

	char	*channel_ptr;

	BOOL_32	read_error_occured;

	// ----------

	#ifdef WIBOX_DEBUG_ANALYSIS
		Alert_Message( "WIBOX_analyze_channel_info" );
	#endif

	// ----------

	lactive_pvs = dev_state->wibox_active_pvs;

	read_error_occured = (false);

	// ----------

	// 3/24/2016 ajv : The Lantronix response has duplicate key words (eg. "Port") that screw up
	// extraction. Start at the beginning of the channel section instead of at the beginning of
	// the response buffer.
	if( ( channel_ptr = strstr( dev_state->resp_ptr, WIBOX_CHANNEL_INFO_TITLE_STR ) ) != NULL )
	{
		dev_extract_delimited_text_from_buffer( channel_ptr, ",", WIBOX_CHANNEL_INFO_BAUD_RATE_STR, WIBOX_CHANNEL_INFO_BAUD_RATE_OFFSET, sizeof( lactive_pvs->baud_rate ), lactive_pvs->baud_rate, "BAUD RATE" );

		dev_extract_text_from_buffer( channel_ptr, WIBOX_CHANNEL_INFO_IF_MODE_STR, WIBOX_CHANNEL_INFO_IF_MODE_OFFSET, sizeof( lactive_pvs->if_mode ), lactive_pvs->if_mode, "IF MODE" );

		dev_extract_text_from_buffer( channel_ptr, WIBOX_CHANNEL_INFO_FLOW_STR,    WIBOX_CHANNEL_INFO_FLOW_OFFSET,    sizeof( lactive_pvs->flow ),    lactive_pvs->flow,    "FLOW" );

		dev_extract_delimited_text_from_buffer( channel_ptr, WIBOX_CR_STR, WIBOX_CHANNEL_INFO_PORT_STR, WIBOX_CHANNEL_INFO_PORT_OFFSET, sizeof( lactive_pvs->port ), lactive_pvs->port, "PORT" );

		dev_extract_text_from_buffer( channel_ptr, WIBOX_CHANNEL_INFO_CONN_MODE_STR, WIBOX_CHANNEL_INFO_CONN_MODE_OFFSET, sizeof( lactive_pvs->conn_mode ), lactive_pvs->conn_mode, "CONN MODE" );

		// ----------

		// 3/24/2016 ajv : The Auto Increment response text is "enabled" or "disabled". For the
		// WRITE operation, we need a "Y" or "N". Use a boolean to store the value.
		dev_extract_text_from_buffer( channel_ptr, WIBOX_CHANNEL_INFO_AUTO_INCR_STR, WIBOX_CHANNEL_INFO_AUTO_INCR_OFFSET, sizeof( temp_text ), temp_text, "AUTO INCR" );

		if( strncmp( temp_text, WIBOX_CHANNEL_INFO_ENABLED_STR, WIBOX_CHANNEL_INFO_ENABLED_LEN ) == NULL )
		{
			lactive_pvs->auto_increment = (true);
		}
		else if( strncmp( temp_text, WIBOX_CHANNEL_INFO_DISABLED_STR, WIBOX_CHANNEL_INFO_DISABLED_LEN ) == NULL )
		{
			lactive_pvs->auto_increment = (false);
		}
		else
		{
			// 3/24/2016 ajv : Something went wrong... set it true
			lactive_pvs->auto_increment = (true);

			read_error_occured = (true);
		}

		// ----------

		if( ( strstr( channel_ptr, WIBOX_CHANNEL_INFO_RIP_NOT_SET_STR ) ) != NULL )
		{
			// 4/22/2015 mpd : No remote IP address is set.
			strlcpy( lactive_pvs->remote_ip_address, WIBOX_NULL_IP_ADD_STR, sizeof( lactive_pvs->remote_ip_address ) );
		}
		else
		{
			dev_extract_delimited_text_from_buffer( channel_ptr, ",", WIBOX_CHANNEL_INFO_REMOTE_IP_STR, WIBOX_CHANNEL_INFO_REMOTE_IP_OFFSET, sizeof( lactive_pvs->remote_ip_address ), lactive_pvs->remote_ip_address, "REMOTE IP ADD" );
		}

		dev_extract_ip_octets( lactive_pvs->remote_ip_address, lactive_pvs->remote_ip_address_1, lactive_pvs->remote_ip_address_2, lactive_pvs->remote_ip_address_3, lactive_pvs->remote_ip_address_4 );

		// ----------

		dev_extract_delimited_text_from_buffer( channel_ptr, WIBOX_CR_STR, WIBOX_CHANNEL_INFO_REMOTE_PORT_STR, WIBOX_CHANNEL_INFO_REMOTE_PORT_OFFSET, sizeof( lactive_pvs->remote_port ), lactive_pvs->remote_port, "REMOTE PORT" );

		dev_extract_text_from_buffer( channel_ptr, WIBOX_CHANNEL_INFO_DISCONN_MODE_STR, WIBOX_CHANNEL_INFO_DISCONN_MODE_OFFSET, sizeof( lactive_pvs->disconn_mode ), lactive_pvs->disconn_mode, "DISCONN MODE" );

		dev_extract_text_from_buffer( channel_ptr, WIBOX_CHANNEL_INFO_FLUSH_MODE_STR,   WIBOX_CHANNEL_INFO_FLUSH_MODE_OFFSET,   sizeof( lactive_pvs->flush_mode ),   lactive_pvs->flush_mode,   "FLUSH MODE" );
	}
	else
	{
		read_error_occured = (true);
	}

	if( read_error_occured )
	{
		Alert_Message( "Error reading Channel 1 settings" );
	}
}

/* ---------------------------------------------------------- */
static void WIBOX_analyze_wlan_info( DEV_STATE_STRUCT *dev_state )
{
	WIBOX_PROGRAMMABLE_VALUES_STRUCT *lactive_pvs;

	// 4/1/2016 ajv : 32 is an arbirtary number. Whatever this is, it should be enough to handle
	// the longest string on the screen, which is likely WPA2/802.11i<CR>
	char 	temp_text[ 32 ];

	char	*wlan_ptr;

	BOOL_32	read_error_occured;

	// ----------

	#ifdef WIBOX_DEBUG_ANALYSIS
		Alert_Message( "WIBOX_analyze_channel_info" );
	#endif

	// ----------

	lactive_pvs = dev_state->wibox_active_pvs;

	read_error_occured = (false);

	// ----------

	// 3/24/2016 ajv : The Lantronix response has duplicate key words (eg. "Port") that screw up
	// extraction. Start at the beginning of the channel section instead of at the beginning of
	// the response buffer.
	if( ( wlan_ptr = strstr(dev_state->resp_ptr, "*** WLAN")) != NULL )
	{
		// 3/29/2016 ajv : For some reason, the SSID is followed by a Line Feed first, rather than a
		// Carriage Return. Therefore, set that as the delimiter
		dev_extract_delimited_text_from_buffer( wlan_ptr, WIBOX_LF_STR, "Network name: ", strlen("Network name: "), sizeof( lactive_pvs->ssid ), lactive_pvs->ssid, "SSID" );

		// ----------

		dev_extract_delimited_text_from_buffer( wlan_ptr, WIBOX_CR_STR, "Security suite: ", strlen("Security suite: "), sizeof( temp_text ), temp_text, "Security" );

		if( strncmp(temp_text, "none", strlen("none")) == 0 )
		{
			lactive_pvs->security = WEN_WIRELESS_SECURITY_NONE;
		}
		else if( strncmp(temp_text, "WEP", strlen("WEP")) == 0 )
		{
			lactive_pvs->security = WEN_WIRELESS_SECURITY_WEP;
		}
		else if( strncmp(temp_text, "WPA2/802.11i", strlen("WPA2/802.11i")) == 0 )
		{
			lactive_pvs->security = WEN_WIRELESS_SECURITY_WPA2;
		}
		else // "WPA2"
		{
			lactive_pvs->security = WEN_WIRELESS_SECURITY_WPA;
		}

		// ----------

		if( lactive_pvs->security == WEN_WIRELESS_SECURITY_WEP )
		{
			dev_extract_delimited_text_from_buffer( wlan_ptr, WIBOX_CR_STR, "Authentication: ", strlen("Authentication: "), sizeof( temp_text ), temp_text, "Authentication" );

			if( strncmp(temp_text, "open/none", strlen("open/none")) == 0 )
			{
				lactive_pvs->wep_authentication = WEN_WEP_AUTHENTICATION_OPEN;
			}
			else // "shared"
			{
				lactive_pvs->wep_authentication = WEN_WEP_AUTHENTICATION_SHARED;
			}

			// ----------

			dev_extract_delimited_text_from_buffer( wlan_ptr, WIBOX_CR_STR, "Encryption: ", strlen("Encryption: "), sizeof( temp_text ), temp_text, "Encryption" );

			if( strncmp(temp_text, "WEP64", strlen("WEP64")) == 0 )
			{
				lactive_pvs->wep_key_size = WEN_KEY_SIZE_40;
			}
			else // "WEP128"
			{
				lactive_pvs->wep_key_size = WEN_KEY_SIZE_104;
			}
		}
		else if( lactive_pvs->security == WEN_WIRELESS_SECURITY_WPA )
		{
			dev_extract_delimited_text_from_buffer( wlan_ptr, WIBOX_CR_STR, "Encryption: ", strlen("Encryption: "), sizeof( temp_text ), temp_text, "Encryption" );

			if( strncmp(temp_text, "TKIP+WEP", strlen("TKIP+WEP")) == 0 )
			{
				lactive_pvs->wpa_encryption = WEN_WPA_ENCRYPTION_TKIP_WEP;
			}
			else // "TKIP"
			{
				lactive_pvs->wpa_encryption = WEN_WPA_ENCRYPTION_TKIP;
			}
		}
		else if( lactive_pvs->security == WEN_WIRELESS_SECURITY_WPA2 )
		{
			dev_extract_delimited_text_from_buffer( wlan_ptr, WIBOX_CR_STR, "Encryption: ", strlen("Encryption: "), sizeof( temp_text ), temp_text, "Encryption" );

			if( strncmp( temp_text, "CCMP+TKIP", strlen("CCMP+TKIP")) == 0 )
			{
				lactive_pvs->wpa2_encryption = WEN_WPA2_ENCRYPTION_CCMP_TKIP;
			}
			else if( strncmp( temp_text, "CCMP+WEP", strlen("CCMP+WEP")) == 0 )
			{
				lactive_pvs->wpa2_encryption = WEN_WPA2_ENCRYPTION_CCMP_WEP;
			}
			else if( strncmp(temp_text, "CCMP", strlen("CCMP")) == 0 )
			{
				lactive_pvs->wpa2_encryption = WEN_WPA2_ENCRYPTION_CCMP;
			}
			else if( strncmp(temp_text, "TKIP+WEP", strlen("TKIP+WEP")) == 0 )
			{
				lactive_pvs->wpa2_encryption = WEN_WPA2_ENCRYPTION_TKIP_WEP;
			}
			else // "TKIP"
			{
				lactive_pvs->wpa2_encryption = WEN_WPA2_ENCRYPTION_TKIP;
			}
		}
	}
	else
	{
		read_error_occured = (true);
	}

	if( read_error_occured )
	{
		Alert_Message( "Error reading WLAN settings" );
	}
}

/* ---------------------------------------------------------- */
static void WIBOX_analyze_setup_mode( DEV_STATE_STRUCT *dev_state )
{
	#ifdef WIBOX_DEBUG_ANALYSIS
		Alert_Message( "WIBOX_analyze_setup_mode" );
	#endif

	// ----------

	// 3/24/2016 ajv : Analysis functions have been broken into sections (Server, Channel 1, and
	// WLAN) for maintainability. Unfortunately, the read list needs a single function to call
	// for analysis of the huge initial response. This is it.
	WIBOX_analyze_basic_info( dev_state );

	WIBOX_analyze_channel_1_info( dev_state );

	WIBOX_analyze_wlan_info( dev_state );
}

/* ---------------------------------------------------------- */
static void WIBOX_device_write_progress( DEV_STATE_STRUCT *dev_state )
{
	// 3/28/2016 ajv : Update easyGUI progress dialog
	strlcpy( dev_state->info_text, "Writing device settings...", sizeof( dev_state->info_text ) );                

	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_write_settings_in_progress );
}

/* ---------------------------------------------------------- */
/**
 * This function is called at the end of READ and WRITE cycles. All device values have been
 * read into an "WIBOX_PROGRAMMABLE_VALUES_STRUCT" and specific fields have been anyalzed
 * for consistency. This function gives the final stamp of approval.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param dev_state 
 *
 * @author 3/28/2016 AdrianusV
 *
 * @revisions (none)
 */
static void WIBOX_final_device_analysis( DEV_STATE_STRUCT *dev_state )
{
	WIBOX_PROGRAMMABLE_VALUES_STRUCT *lactive_pvs;

	WIBOX_DETAILS_STRUCT *ldetails;

	BOOL_32 *success_ptr;

	// ----------

	#ifdef WIBOX_DEBUG_ANALYSIS
	Alert_Message( "WIBOX_final_device_analysis" );
	#endif

	// ----------

	lactive_pvs = dev_state->wibox_active_pvs;

	ldetails = dev_state->WIBOX_details;

	// ----------

	if( dev_state->read_after_a_write )
	{
		success_ptr = &dev_state->programming_successful;
	}
	else
	{
		success_ptr = &dev_state->device_settings_are_valid;
	}

	*success_ptr = (true);

	// ----------

	// 3/28/2016 ajv : Verify the Channel 1 settings were written properly
	if( dev_state->read_after_a_write )
	{
		if( strncmp( lactive_pvs->baud_rate, DEV_TEXT_CMD_SU_BAUD_RATE, WIBOX_CHANNEL_INFO_BAUD_RATE_LEN - WIBOX_NULL_TERM_LEN ) != NULL ) 
		{
			*success_ptr = (false);
		}

		if( strncmp( lactive_pvs->if_mode, DEV_TEXT_CMD_SU_IF_MODE_4C, WIBOX_CHANNEL_INFO_IF_MODE_LEN - WIBOX_NULL_TERM_LEN ) != NULL ) 
		{
			*success_ptr = (false);
		}

		if( strncmp( lactive_pvs->flow, DEV_TEXT_CMD_SU_FLOW_02, WIBOX_CHANNEL_INFO_FLOW_LEN - WIBOX_NULL_TERM_LEN ) != NULL ) 
		{
			*success_ptr = (false);
		}

		if( strncmp( lactive_pvs->port, DEV_TEXT_CMD_SU_PORT, WIBOX_CHANNEL_INFO_PORT_LEN - WIBOX_NULL_TERM_LEN ) != NULL ) 
		{
			*success_ptr = (false);
		}

		if( strncmp( lactive_pvs->conn_mode, DEV_TEXT_CMD_SU_CONN_MODE, WIBOX_CHANNEL_INFO_CONN_MODE_LEN - WIBOX_NULL_TERM_LEN ) != NULL ) 
		{
			*success_ptr = (false);
		}

		if( strncmp( lactive_pvs->disconn_mode, DEV_TEXT_CMD_SU_DISC_MODE, WIBOX_CHANNEL_INFO_DISCONN_MODE_LEN - WIBOX_NULL_TERM_LEN ) != NULL ) 
		{
			*success_ptr = (false);
		}

		if( strncmp( lactive_pvs->flush_mode, DEV_TEXT_CMD_SU_FLUSH_MODE, WIBOX_CHANNEL_INFO_FLUSH_MODE_LEN - WIBOX_NULL_TERM_LEN ) != NULL ) 
		{
			*success_ptr = (false);
		}

		if( strlen( lactive_pvs->dhcp_name ) > 0 )
		{
			// 3/28/2016 ajv : We successfully wrote a DHCP name. This flag prevents us from
			// stomping on that name if we disable DHCP and then reenable it during this session.
			ldetails->dhcp_name_not_set = (false);
		}
	}

	// ----------

	// 3/28/2016 ajv : There is no mimimum WiBox firmware version at this time.
	#if 0
	dev_analyze_firmware_version( dev_state );

	if( !dev_state->acceptable_version )
	{
		*success_ptr = (false);
	}
	#endif

	// ----------

	if( *success_ptr == (false) )
	{
		Alert_Message( "Invalid Channel 1 Setting" );
	}
}

/* ---------------------------------------------------------- */
extern void WIBOX_copy_active_values_to_static_values( DEV_STATE_STRUCT *dev_state )
{
	WIBOX_PROGRAMMABLE_VALUES_STRUCT *lactive_pvs;

	WIBOX_PROGRAMMABLE_VALUES_STRUCT *lstatic_pvs;

	// ----------

	#ifdef WIBOX_DEBUG_WRITE
	Alert_Message( "WIBOX_copy_active_values_to_static_values" );
	#endif
	
	// ----------

	lactive_pvs = dev_state->wibox_active_pvs;

	lstatic_pvs = dev_state->wibox_static_pvs;

	// ----------

	// 3/28/2016 ajv : Before copying values from the "active_pvs" to the "static_pvs", all
	// programming details that are hidden from the user need to be updated based on current
	// user selections.

	// 3/28/2016 ajv : These are fixed CS3000 values that cannot be controlled by the user.
	// These are critical to proper operation of the device, so they get set every time we
	// program the device. Set them in the "active_pvs" fields so the post-write verification
	// will match. They will be copied to the "static_pvs" further down in this function.
	strlcpy( lactive_pvs->baud_rate,           DEV_TEXT_CMD_SU_BAUD_RATE,   sizeof( lactive_pvs->baud_rate ) );
	strlcpy( lactive_pvs->if_mode,             DEV_TEXT_CMD_SU_IF_MODE_4C,  sizeof( lactive_pvs->if_mode ) );
	strlcpy( lactive_pvs->flow,                DEV_TEXT_CMD_SU_FLOW_02,     sizeof( lactive_pvs->flow ) );
	strlcpy( lactive_pvs->port,                DEV_TEXT_CMD_SU_PORT,        sizeof( lactive_pvs->port ) );
	strlcpy( lactive_pvs->conn_mode,           DEV_TEXT_CMD_SU_CONN_MODE,   sizeof( lactive_pvs->conn_mode ) );
	lactive_pvs->auto_increment =  (true);
	strlcpy( lactive_pvs->remote_ip_address,   DEV_CALSENSE_IP_ADD_STR,     sizeof( lactive_pvs->remote_ip_address ) );
	strlcpy( lactive_pvs->remote_ip_address_1, DEV_TEXT_CMD_SU_REMOTE_IP_1, sizeof( lactive_pvs->remote_ip_address_1 ) );
	strlcpy( lactive_pvs->remote_ip_address_2, DEV_TEXT_CMD_SU_REMOTE_IP_2, sizeof( lactive_pvs->remote_ip_address_2 ) );
	strlcpy( lactive_pvs->remote_ip_address_3, DEV_TEXT_CMD_SU_REMOTE_IP_3, sizeof( lactive_pvs->remote_ip_address_3 ) );
	strlcpy( lactive_pvs->remote_ip_address_4, DEV_TEXT_CMD_SU_REMOTE_IP_4, sizeof( lactive_pvs->remote_ip_address_4 ) );
	strlcpy( lactive_pvs->remote_port,         DEV_TEXT_CMD_SU_REMOTE_PORT, sizeof( lactive_pvs->remote_port ) );
	strlcpy( lactive_pvs->disconn_mode,        DEV_TEXT_CMD_SU_DISC_MODE,   sizeof( lactive_pvs->disconn_mode ) );
	strlcpy( lactive_pvs->flush_mode,          DEV_TEXT_CMD_SU_FLUSH_MODE,  sizeof( lactive_pvs->flush_mode ) );

	// ----------

	// 3/28/2016 ajv : Fixed settings have been merged with user settings. Make a copy to
	// "static_pvs" for post-WRITE verification. The verification will be done by the READ that
	// immediately follows this WRITE.

	// SERVER PARAMETERS
	strlcpy( lstatic_pvs->ip_address,   lactive_pvs->ip_address,   sizeof( lstatic_pvs->ip_address ) );
	strlcpy( lstatic_pvs->ip_address_1, lactive_pvs->ip_address_1, sizeof( lstatic_pvs->ip_address_1 ) );
	strlcpy( lstatic_pvs->ip_address_2, lactive_pvs->ip_address_2, sizeof( lstatic_pvs->ip_address_2 ) );
	strlcpy( lstatic_pvs->ip_address_3, lactive_pvs->ip_address_3, sizeof( lstatic_pvs->ip_address_3 ) );
	strlcpy( lstatic_pvs->ip_address_4, lactive_pvs->ip_address_4, sizeof( lstatic_pvs->ip_address_4 ) );

	strlcpy( lstatic_pvs->dhcp_name, lactive_pvs->dhcp_name, sizeof( lstatic_pvs->dhcp_name ) );

	strlcpy( lstatic_pvs->gw_address,   lactive_pvs->gw_address,   sizeof( lstatic_pvs->gw_address ) );
	strlcpy( lstatic_pvs->gw_address_1, lactive_pvs->gw_address_1, sizeof( lstatic_pvs->gw_address_1 ) );
	strlcpy( lstatic_pvs->gw_address_2, lactive_pvs->gw_address_2, sizeof( lstatic_pvs->gw_address_2 ) );
	strlcpy( lstatic_pvs->gw_address_3, lactive_pvs->gw_address_3, sizeof( lstatic_pvs->gw_address_3 ) );
	strlcpy( lstatic_pvs->gw_address_4, lactive_pvs->gw_address_4, sizeof( lstatic_pvs->gw_address_4 ) );

	// 9/24/2015 mpd : FogBugz #3178: We use mask bits for programming, not the mask string. Leave this commented out.
	//strlcpy( static_pvs->mask,   active_pvs->mask,   sizeof( static_pvs->mask ) ); 

	// ----------

	// CHANNEL 1 PARAMETERS
	strlcpy( lstatic_pvs->baud_rate,           lactive_pvs->baud_rate,           sizeof( lstatic_pvs->baud_rate ) );
	strlcpy( lstatic_pvs->if_mode,             lactive_pvs->if_mode,             sizeof( lstatic_pvs->if_mode ) );
	strlcpy( lstatic_pvs->flow,                lactive_pvs->flow,                sizeof( lstatic_pvs->flow ) );
	strlcpy( lstatic_pvs->port,                lactive_pvs->port,                sizeof( lstatic_pvs->port ) );
	strlcpy( lstatic_pvs->conn_mode,           lactive_pvs->conn_mode,           sizeof( lstatic_pvs->conn_mode ) );
	lstatic_pvs->auto_increment =              lactive_pvs->auto_increment;
	strlcpy( lstatic_pvs->remote_ip_address,   lactive_pvs->remote_ip_address,   sizeof( lstatic_pvs->remote_ip_address ) );
	strlcpy( lstatic_pvs->remote_ip_address_1, lactive_pvs->remote_ip_address_1, sizeof( lstatic_pvs->remote_ip_address_1 ) );
	strlcpy( lstatic_pvs->remote_ip_address_2, lactive_pvs->remote_ip_address_2, sizeof( lstatic_pvs->remote_ip_address_2 ) );
	strlcpy( lstatic_pvs->remote_ip_address_3, lactive_pvs->remote_ip_address_3, sizeof( lstatic_pvs->remote_ip_address_3 ) );
	strlcpy( lstatic_pvs->remote_ip_address_4, lactive_pvs->remote_ip_address_4, sizeof( lstatic_pvs->remote_ip_address_4 ) );
	strlcpy( lstatic_pvs->remote_port,         lactive_pvs->remote_port,         sizeof( lstatic_pvs->remote_port ) );
	strlcpy( lstatic_pvs->disconn_mode,        lactive_pvs->disconn_mode,        sizeof( lstatic_pvs->disconn_mode ) );
	strlcpy( lstatic_pvs->flush_mode,          lactive_pvs->flush_mode,          sizeof( lstatic_pvs->flush_mode ) );

	// ----------
								
	// WLAN PARAMETERS																   
	strlcpy( lstatic_pvs->ssid, lactive_pvs->ssid, sizeof( lstatic_pvs->ssid ) );
	strlcpy( lstatic_pvs->key, lactive_pvs->key, sizeof( lstatic_pvs->key ) );
	strlcpy( lstatic_pvs->passphrase, lactive_pvs->passphrase, sizeof( lstatic_pvs->passphrase ) );
	lstatic_pvs->w_key_type = lactive_pvs->w_key_type;
	lstatic_pvs->security = lactive_pvs->security;
	lstatic_pvs->wep_authentication = lactive_pvs->wep_authentication;
	lstatic_pvs->wep_key_size = lactive_pvs->wep_key_size;
	lstatic_pvs->wpa_encryption = lactive_pvs->wpa_encryption;
	lstatic_pvs->wpa2_encryption = lactive_pvs->wpa2_encryption;
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  				   READ and WRITE Lists 				  */
/* ---------------------------------------------------------- */

// LANTRONIX ENTER SETUP MODE INSTRUCTIONS:
// 1. Reset the WiBox unit by cycling the unit's power.
// 2. Immediately upon resetting the device, enter three lowercase x characters (xxx).

// 3/28/2016 ajv : TODO: WiBox is using long title strings to verify menu context. Use brief
// strings or the termination string as the PremierWave devices do.
const DEV_MENU_ITEM WIBOX_setup_read_list[] =
{ 
	// Response Title 					Response Handler				Next Command				Next Termination String
	{ WIBOX_NO_TEXT_STR,				NULL,							DEV_COMMAND_START,			WIBOX_SERVER_INFO_SETUP_STR },	// This first entry is used by device exchange initialization.
	{ WIBOX_SERVER_INFO_SETUP_STR,		WIBOX_analyze_server_info,		DEV_COMMAND_CR,				WIBOX_CHANGE_MENU_CHOICE_STR },	// This second entry is used by the DEV_WAITING_FOR_RESPONSE state. Analyze the initial response. Return ENTER to continue into SETUP.
	{ WIBOX_BASIC_INFO_TITLE_STR,		WIBOX_analyze_setup_mode,		DEV_COMMAND_EXIT_WO_SAVE,	WIBOX_CHANGE_MENU_EXIT_WO_STR },// Analyze the bulk of the response. Exit without saving.
	{ WIBOX_CHANGE_MENU_EXIT_WO_STR,	WIBOX_final_device_analysis,	DEV_COMMAND_CR_NR,			WIBOX_NO_TEXT_STR },			// Perform final analysis. CLI Disconnect. 
	{ WIBOX_NO_TEXT_STR, 				dev_cli_disconnect,		  		DEV_COMMAND_END,			WIBOX_NO_TEXT_STR }				// Hardware disconnect. 
};

/* ---------------------------------------------------------- */

// 3/28/2016 ajv : For consistency, navigate in the same menu order for WRITE as we did for
// READ. Note that WRITE requires additional analyze/hunt I/O cycles between the menu
// navigation cycles. Once on the appropriate menu, each WRITE operation involves a cycle to
// select a menu option, and another to input the new value. Also note that CR and LF
// characters ARE NOT INCLUDED ON EVERY LINE!!! The device is very fussy regarding CR and
// LF.
 
// 3/28/2016 ajv : The "Response Title" requires enough characters to determine a unique
// match; not the entire line! The defines were created to exclude anything that could
// change during run time. The "Next Termination String" has similar run time issues, such
// as IP addess entry responses. A ")" character is all we can use for the termination
// string since the device will proceed ")" with the dynamic value of the octet. The final
// octet response termination is the "?" on the subsequent programming question.
const DEV_MENU_ITEM WIBOX_dhcp_write_list[] =
{  
	// Response Title                    Response Handler               Next Command                   	Next Termination String
	{ WIBOX_NO_TEXT_STR,			  		NULL,					  	DEV_COMMAND_START,    	   		WIBOX_SERVER_INFO_SETUP_STR },			// This first entry is used by device exchange initialization.
	{ WIBOX_SERVER_INFO_SETUP_STR,   		NULL,	  					DEV_COMMAND_CR,		       		WIBOX_CHANGE_MENU_CHOICE_STR },		// This second entry is used by the DEV_WAITING_FOR_RESPONSE state. Analyze the initial response. Return ENTER to continue into SETUP.

	// ----------
	// ----------

	{ WIBOX_BASIC_INFO_TITLE_STR,	        NULL,			            DEV_COMMAND_GOTO_SERVER,	    	WIBOX_SERVER_INPUT_NETWORK_MODE_STR }, // Go to the server menu. No need to perform any analysis here.
	{ WIBOX_SERVER_INPUT_NETWORK_MODE_STR,  WIBOX_device_write_progress,DEV_COMMAND_GET_WIBOX_NETWORK_MODE,	WIBOX_SERVER_INPUT_IP_ADD_STR },       // Send the network mode (wireless only)
	{ WIBOX_SERVER_INPUT_IP_ADD_STR,        NULL,						DEV_COMMAND_GET_IP_1,           	WIBOX_SERVER_INPUT_IP_ADD_RESP_STR },  // Send IP octet 1.
	{ WIBOX_SERVER_INPUT_IP_ADD_RESP_STR,   NULL,			            DEV_COMMAND_GET_IP_2,           	WIBOX_SERVER_INPUT_IP_ADD_RESP_STR },  // Send IP octet 2.
	{ WIBOX_SERVER_INPUT_IP_ADD_RESP_STR,   NULL,			            DEV_COMMAND_GET_IP_3,           	WIBOX_SERVER_INPUT_IP_ADD_RESP_STR },  // Send IP octet 3.
	{ WIBOX_SERVER_INPUT_IP_ADD_RESP_STR,   NULL,			            DEV_COMMAND_GET_IP_4,           	WIBOX_SERVER_INPUT_QUESTION_STR },     // Send IP octet 4.
	{ WIBOX_SERVER_INPUT_GATEWAY_YN_STR,    NULL,			            DEV_COMMAND_GET_GW_BOOL,        	WIBOX_SERVER_INPUT_NETMASK_STR }, 	    // Send "No" to changing the gateway IP address.

	// 6/12/2015 mpd : TODO: Both EN WRITE lists could be combined by using "dev_state->dhcp_enabled" and 
	// "dev_increment_list_item_pointer( dev_state )". This is a low priority.

	{ WIBOX_SERVER_INPUT_NETMASK_STR, 	    NULL,			            DEV_COMMAND_GET_MASK_BITS,      WIBOX_SERVER_INPUT_QUESTION_STR }, 	// Send the mask bits.
	{ WIBOX_SERVER_INPUT_TELNET_YN_STR,     NULL,			            DEV_COMMAND_GET_TELNET_BOOL,    WIBOX_SERVER_INPUT_QUESTION_STR }, 	// Send "No" to changing the telnet password.

	// 6/12/2015 mpd : These 2 lines are only used when DHCP is enabled. 
	{ WIBOX_SERVER_INPUT_DHCP_NAME_YN_STR,  NULL,			            DEV_COMMAND_GET_DHCP_NAME_BOOL, WIBOX_SERVER_INPUT_COLON_STR }, 		// Send "Yes" to changing the DHCP name.

	// 6/12/2015 mpd : Just test for a "?" terminator after DHCP. We may get a "FQDN" question instead of the "Your Choice?" we expect.
	{ WIBOX_SERVER_INPUT_DHCP_NAME_STR,     NULL,			            DEV_COMMAND_GET_DHCP_NAME,      WIBOX_CHANNEL_INPUT_QUESTION_STR },    // Send the new DHCP name.

	// ----------
	// ----------

	{ WIBOX_CHANGE_MENU_CHOICE_STR,	        NULL,  		                DEV_COMMAND_GOTO_CHANNEL,	   	WIBOX_CHANNEL_INPUT_QUESTION_STR },    // Go to the channel menu.
	{ WIBOX_CHANNEL_INPUT_BAUD_RATE_STR,    NULL,  						DEV_COMMAND_GET_BAUD_RATE,	   	WIBOX_CHANNEL_INPUT_QUESTION_STR },    // Send the baud rate.
	{ WIBOX_CHANNEL_INPUT_IF_MODE_STR,  	NULL,  		                DEV_COMMAND_GET_IF_MODE,	    WIBOX_CHANNEL_INPUT_QUESTION_STR },    // Send the IF mode.
	{ WIBOX_CHANNEL_INPUT_FLOW_STR,  	    NULL,  		                DEV_COMMAND_GET_FLOW,	   	   	WIBOX_CHANNEL_INPUT_QUESTION_STR },    // Send the flow.
	{ WIBOX_CHANNEL_INPUT_PORT_STR,  	    NULL,  		                DEV_COMMAND_GET_PORT,	       	WIBOX_CHANNEL_INPUT_QUESTION_STR },    // Send the port.
	{ WIBOX_CHANNEL_INPUT_CONN_MODE_STR,  	NULL,  		                DEV_COMMAND_GET_CONN_MODE,	   	WIBOX_CHANNEL_INPUT_QUESTION_STR },    // Send the connect mode.
	{ WIBOX_CHANNEL_INPUT_SEND_STR,    	    NULL,  		                DEV_COMMAND_GET_SEND_BOOL,	   	WIBOX_CHANNEL_INPUT_QUESTION_STR },    // Send "Yes" to "Send ESC in Modem Mode". 
	{ WIBOX_CHANNEL_INPUT_AUTO_INCR_STR,  	NULL,  		                DEV_COMMAND_GET_AUTO_BOOL,	   	WIBOX_SERVER_INPUT_IP_ADD_STR }, 	    // Send "Yes" to "Auto Increment Source Port". 
	{ WIBOX_CHANNEL_INPUT_REMOTE_IP_STR,  	NULL,  		                DEV_COMMAND_GET_REMOTE_IP_1,	WIBOX_CHANNEL_INPUT_IP_ADD_RESP_STR }, // Send Remote IP octet 1.
	{ WIBOX_CHANNEL_INPUT_IP_ADD_RESP_STR,	NULL,  		                DEV_COMMAND_GET_REMOTE_IP_2,	WIBOX_CHANNEL_INPUT_IP_ADD_RESP_STR }, // Send Remote IP octet 2.
	{ WIBOX_CHANNEL_INPUT_IP_ADD_RESP_STR,	NULL,  		                DEV_COMMAND_GET_REMOTE_IP_3,	WIBOX_CHANNEL_INPUT_IP_ADD_RESP_STR }, // Send Remote IP octet 3.
	{ WIBOX_CHANNEL_INPUT_IP_ADD_RESP_STR,	NULL,  		                DEV_COMMAND_GET_REMOTE_IP_4,	WIBOX_CHANNEL_INPUT_QUESTION_STR },    // Send Remote IP octet 4.
	{ WIBOX_CHANNEL_INPUT_REMOTE_PORT_STR,	NULL,  		                DEV_COMMAND_GET_REMOTE_PORT,	WIBOX_CHANNEL_INPUT_QUESTION_STR },    // Send the remote port.
	{ WIBOX_CHANNEL_INPUT_DISCONN_MODE_STR, NULL,  		                DEV_COMMAND_GET_DISCONN_MODE,   WIBOX_CHANNEL_INPUT_QUESTION_STR },    // Send the disconnect mode.
	{ WIBOX_CHANNEL_INPUT_FLUSH_MODE_STR, 	NULL,  		                DEV_COMMAND_GET_FLUSH_MODE,	   	WIBOX_CHANNEL_INPUT_QUESTION_STR },    // Send the flush mode.
	{ WIBOX_CHANNEL_INPUT_DISCONN_TIME_STR, NULL,  		                DEV_COMMAND_GET_DISCONN_TIME,   WIBOX_CHANNEL_INPUT_COLON_STR },       // Send the disconnect time minutes.
	{ WIBOX_CHANNEL_INPUT_COLON_STR,		NULL,  		                DEV_COMMAND_GET_DISCONN_TIME,   WIBOX_CHANNEL_INPUT_QUESTION_STR },    // Send the disconnect time seconds.
	{ WIBOX_CHANNEL_INPUT_SEND_CHAR_1_STR,	NULL,  		                DEV_COMMAND_GET_SEND_CHAR_1,	WIBOX_CHANNEL_INPUT_QUESTION_STR },    // Disable "2-Byte Send Character Sequence". 
	{ WIBOX_CHANNEL_INPUT_SEND_CHAR_2_STR,  NULL,  		                DEV_COMMAND_GET_SEND_CHAR_2,	WIBOX_CHANGE_MENU_CHOICE_STR },        // Disable "Send Immediately After Characters". 

	// ----------
	// ----------

	{ WIBOX_CHANGE_MENU_CHOICE_STR,			NULL,						DEV_COMMAND_GOTO_WLAN,				WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_TOPOLOGY,			NULL,						DEV_COMMAND_GET_WIBOX_TOPOLOGY,		WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_SSID,				NULL,						DEV_COMMAND_GET_WIBOX_SSID,			WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_SECURITY_SUITE,		NULL,						DEV_COMMAND_GET_WIBOX_SECURITY,		WIBOX_WLAN_INPUT_QUESTION_STR },

	// ----------
	// 3/31/2016 ajv : These are skipped if security suite == 0, 2, or 3

	{ WIBOX_WLAN_INPUT_WEP_AUTH,			NULL,						DEV_COMMAND_GET_WEP_AUTH,			WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WEP_ENCRYPTION,		NULL,						DEV_COMMAND_GET_WEP_ENCRYPTION,		WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WEP_CHANGE_KEY,		NULL,						DEV_COMMAND_GET_WEP_CHANGE_KEY,		WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WEP_DISPLAY_KEY,		NULL,						DEV_COMMAND_GET_WEP_DISPLAY_KEY,	WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WEP_KEY_TYPE,		NULL,						DEV_COMMAND_GET_WEP_KEY_TYPE,		WIBOX_SERVER_INPUT_COLON_STR },
	{ WIBOX_WLAN_INPUT_WEP_KEY,				NULL,						DEV_COMMAND_GET_WEP_KEY,			WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WEP_TX_KEY_INDEX,	NULL,						DEV_COMMAND_GET_WEP_TX_KEY_INDEX,	WIBOX_WLAN_INPUT_QUESTION_STR },

	// ----------
	// 3/31/2016 ajv : These are skipped if security suite == 0, 1, or 3

	{ WIBOX_WLAN_INPUT_WPA_CHANGE_KEY,		NULL,						DEV_COMMAND_GET_WPA_CHANGE_KEY,		WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WPA_DISPLAY_KEY,		NULL,						DEV_COMMAND_GET_WPA_DISPLAY_KEY,	WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WPA_KEY_TYPE,		NULL,						DEV_COMMAND_GET_WPA_KEY_TYPE,		WIBOX_SERVER_INPUT_COLON_STR },
	{ WIBOX_WLAN_INPUT_WPA_KEY,				NULL,						DEV_COMMAND_GET_WPA_KEY,			WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WPA_ENCRYPTION,		NULL,						DEV_COMMAND_GET_WPA_ENCRYPTION,		WIBOX_WLAN_INPUT_QUESTION_STR },

	// ----------
	// 3/31/2016 ajv : These are skipped if security suite == 0, 1, or 2
	
	{ WIBOX_WLAN_INPUT_WPA2_CHANGE_KEY,		NULL,						DEV_COMMAND_GET_WPA2_CHANGE_KEY,	WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WPA2_DISPLAY_KEY,	NULL,						DEV_COMMAND_GET_WPA2_DISPLAY_KEY,	WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WPA2_KEY_TYPE,		NULL,						DEV_COMMAND_GET_WPA2_KEY_TYPE,		WIBOX_SERVER_INPUT_COLON_STR },
	{ WIBOX_WLAN_INPUT_WPA2_KEY,			NULL,						DEV_COMMAND_GET_WPA2_KEY,			WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WPA2_ENCRYPTION,		NULL,						DEV_COMMAND_GET_WPA2_ENCRYPTION,	WIBOX_WLAN_INPUT_QUESTION_STR },

	// ----------
	// 3/31/2016 ajv : These are always present, regardless of the security suite detected.

	{ WIBOX_WLAN_INPUT_TX_DATA_RATE_1,		NULL,						DEV_COMMAND_GET_WIBOX_TX_DATA_RATE_1,		WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_TX_DATA_RATE_2,		NULL,						DEV_COMMAND_GET_WIBOX_TX_DATA_RATE_2,		WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_MIN_TX_DATA_RATE,	NULL,						DEV_COMMAND_GET_WIBOX_MIN_TX_DATA_RATE,		WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_POWER_MGMT,			NULL,						DEV_COMMAND_GET_WIBOX_POWER_MGMT,			WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_SOFT_AP_ROAMING,		NULL,						DEV_COMMAND_GET_WIBOX_SOFT_AP_ROAMING,		WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_MAX_FAILED_PACKETS,	NULL,						DEV_COMMAND_GET_WIBOX_MAX_FAILED_PACKETS,	WIBOX_WLAN_INPUT_QUESTION_STR },

	// ----------
	// ----------

	{ WIBOX_CHANGE_MENU_CHOICE_STR,  		NULL,  		                DEV_COMMAND_CR,   				WIBOX_CHANGE_MENU_CHOICE_STR },        // Perform a read of the programmed values. 
	{ WIBOX_CHANGE_MENU_CHOICE_STR,		    NULL,				 		DEV_COMMAND_SAVE_AND_EXIT,	   	WIBOX_CHANGE_SUCCESS_STR },				// Save and exit programming mode.
	{ WIBOX_CHANGE_SUCCESS_STR,		 	    NULL,						DEV_COMMAND_CR_NR, 		   		WIBOX_NO_TEXT_STR }, 					// CLI Disconnect. 
	{ WIBOX_NO_TEXT_STR, 			 		dev_cli_disconnect,		  	DEV_COMMAND_END, 			   	WIBOX_NO_TEXT_STR }						// Hardware disconnect. 
};

/* ---------------------------------------------------------- */

const DEV_MENU_ITEM WIBOX_static_write_list[] =
{
	// Response Title                    Response Handler               Next Command                   	Next Termination String
	{ WIBOX_NO_TEXT_STR,			  		NULL,					  	  	DEV_COMMAND_START,    	   		WIBOX_SERVER_INFO_SETUP_STR },			// This first entry is used by device exchange initialization.
	{ WIBOX_SERVER_INFO_SETUP_STR,   		NULL,	  						DEV_COMMAND_CR,		       		WIBOX_CHANGE_MENU_CHOICE_STR },		// This second entry is used by the DEV_WAITING_FOR_RESPONSE state. Analyze the initial response. Return ENTER to continue into SETUP.
	{ WIBOX_BASIC_INFO_TITLE_STR,	        NULL,			                DEV_COMMAND_GOTO_SERVER,	    WIBOX_SERVER_INPUT_IP_ADD_STR },       // Go to the server menu. No need to perform any analysis here.
	{ WIBOX_SERVER_INPUT_IP_ADD_STR,        WIBOX_device_write_progress,	DEV_COMMAND_GET_IP_1,           WIBOX_SERVER_INPUT_IP_ADD_RESP_STR },  // Send IP octet 1.
	{ WIBOX_SERVER_INPUT_IP_ADD_RESP_STR,   NULL,			                DEV_COMMAND_GET_IP_2,           WIBOX_SERVER_INPUT_IP_ADD_RESP_STR },  // Send IP octet 2.
	{ WIBOX_SERVER_INPUT_IP_ADD_RESP_STR,   NULL,			                DEV_COMMAND_GET_IP_3,           WIBOX_SERVER_INPUT_IP_ADD_RESP_STR },  // Send IP octet 3.
	{ WIBOX_SERVER_INPUT_IP_ADD_RESP_STR,   NULL,			                DEV_COMMAND_GET_IP_4,           WIBOX_SERVER_INPUT_QUESTION_STR },     // Send IP octet 4.
	{ WIBOX_SERVER_INPUT_GATEWAY_YN_STR,    NULL,			                DEV_COMMAND_GET_GW_BOOL,        WIBOX_SERVER_INPUT_GATEWAY_IP_STR },   // Send "Yes" to changing the gateway IP address.

	// 4/27/2015 mpd : These 4 lines are only used when DHCP is disabled. 
	{ WIBOX_SERVER_INPUT_GATEWAY_IP_STR,  	NULL,  		                DEV_COMMAND_GET_GW_1,	        WIBOX_SERVER_INPUT_IP_ADD_RESP_STR },   // Send Gateway IP octet 1.
	{ WIBOX_SERVER_INPUT_IP_ADD_RESP_STR,	NULL,  		                DEV_COMMAND_GET_GW_2,	        WIBOX_SERVER_INPUT_IP_ADD_RESP_STR },   // Send Gateway IP octet 2.
	{ WIBOX_SERVER_INPUT_IP_ADD_RESP_STR,	NULL,  		                DEV_COMMAND_GET_GW_3,	        WIBOX_SERVER_INPUT_IP_ADD_RESP_STR },   // Send Gateway IP octet 3.
	{ WIBOX_SERVER_INPUT_IP_ADD_RESP_STR,	NULL,  		                DEV_COMMAND_GET_GW_4,	        WIBOX_SERVER_INPUT_NETMASK_STR },       // Send Gateway IP octet 4.
																		
	{ WIBOX_SERVER_INPUT_NETMASK_STR, 	    NULL,			                DEV_COMMAND_GET_MASK_BITS,      WIBOX_SERVER_INPUT_QUESTION_STR }, 	 // Send the mask bits.
	{ WIBOX_SERVER_INPUT_TELNET_YN_STR,     NULL,			                DEV_COMMAND_GET_TELNET_BOOL,    WIBOX_SERVER_INPUT_QUESTION_STR }, 	 // Send "No" to changing the telnet password.
																		
 	{ WIBOX_CHANGE_MENU_CHOICE_STR,	     	NULL,  		                DEV_COMMAND_GOTO_CHANNEL,	    WIBOX_CHANNEL_INPUT_QUESTION_STR },     // Go to the channel menu.
	{ WIBOX_CHANNEL_INPUT_BAUD_RATE_STR,    NULL,  						DEV_COMMAND_GET_BAUD_RATE,	    WIBOX_CHANNEL_INPUT_QUESTION_STR },     // Send the baud rate.
	{ WIBOX_CHANNEL_INPUT_IF_MODE_STR,  	NULL,  		                DEV_COMMAND_GET_IF_MODE,	    WIBOX_CHANNEL_INPUT_QUESTION_STR },     // Send the IF mode.
	{ WIBOX_CHANNEL_INPUT_FLOW_STR,  	    NULL,  		                DEV_COMMAND_GET_FLOW,	   	    WIBOX_CHANNEL_INPUT_QUESTION_STR },     // Send the flow.
	{ WIBOX_CHANNEL_INPUT_PORT_STR,  	    NULL,  		                DEV_COMMAND_GET_PORT,	        WIBOX_CHANNEL_INPUT_QUESTION_STR },     // Send the port.
	{ WIBOX_CHANNEL_INPUT_CONN_MODE_STR,  	NULL,  		                DEV_COMMAND_GET_CONN_MODE,	    WIBOX_CHANNEL_INPUT_QUESTION_STR },     // Send the connect mode.
	{ WIBOX_CHANNEL_INPUT_SEND_STR,    		NULL,  		                DEV_COMMAND_GET_SEND_BOOL,	    WIBOX_CHANNEL_INPUT_QUESTION_STR },     // Send "Yes" to "Send ESC in Modem Mode". 
	{ WIBOX_CHANNEL_INPUT_AUTO_INCR_STR,  	NULL,  		                DEV_COMMAND_GET_AUTO_BOOL,	    WIBOX_SERVER_INPUT_IP_ADD_STR }, 	     // Send "Yes" to "Auto Increment Source Port". 
	{ WIBOX_CHANNEL_INPUT_REMOTE_IP_STR,  	NULL,  		                DEV_COMMAND_GET_REMOTE_IP_1,	WIBOX_CHANNEL_INPUT_IP_ADD_RESP_STR },  // Send Remote IP octet 1.
	{ WIBOX_CHANNEL_INPUT_IP_ADD_RESP_STR,	NULL,  		                DEV_COMMAND_GET_REMOTE_IP_2,	WIBOX_CHANNEL_INPUT_IP_ADD_RESP_STR },  // Send Remote IP octet 2.
	{ WIBOX_CHANNEL_INPUT_IP_ADD_RESP_STR,	NULL,  		                DEV_COMMAND_GET_REMOTE_IP_3,	WIBOX_CHANNEL_INPUT_IP_ADD_RESP_STR },  // Send Remote IP octet 3.
	{ WIBOX_CHANNEL_INPUT_IP_ADD_RESP_STR,	NULL,  		                DEV_COMMAND_GET_REMOTE_IP_4,	WIBOX_CHANNEL_INPUT_QUESTION_STR },     // Send Remote IP octet 4.
	{ WIBOX_CHANNEL_INPUT_REMOTE_PORT_STR,	NULL,  		                DEV_COMMAND_GET_REMOTE_PORT,	WIBOX_CHANNEL_INPUT_QUESTION_STR },     // Send the remote port.
	{ WIBOX_CHANNEL_INPUT_DISCONN_MODE_STR,	NULL,  		                DEV_COMMAND_GET_DISCONN_MODE,   WIBOX_CHANNEL_INPUT_QUESTION_STR },     // Send the disconnect mode.
	{ WIBOX_CHANNEL_INPUT_FLUSH_MODE_STR, 	NULL,  		                DEV_COMMAND_GET_FLUSH_MODE,	    WIBOX_CHANNEL_INPUT_QUESTION_STR },     // Send the flush mode.
	{ WIBOX_CHANNEL_INPUT_DISCONN_TIME_STR,	NULL,  		                DEV_COMMAND_GET_DISCONN_TIME,   WIBOX_CHANNEL_INPUT_COLON_STR },        // Send the disconnect time minutes.
	{ WIBOX_CHANNEL_INPUT_COLON_STR,		NULL,  		                DEV_COMMAND_GET_DISCONN_TIME,   WIBOX_CHANNEL_INPUT_QUESTION_STR },     // Send the disconnect time seconds.
	{ WIBOX_CHANNEL_INPUT_SEND_CHAR_1_STR,	NULL,  		                DEV_COMMAND_GET_SEND_CHAR_1,	WIBOX_CHANNEL_INPUT_QUESTION_STR },     // Disable "2-Byte Send Character Sequence". 
	{ WIBOX_CHANNEL_INPUT_SEND_CHAR_2_STR,  NULL,  		                DEV_COMMAND_GET_SEND_CHAR_2,	WIBOX_CHANGE_MENU_CHOICE_STR },         // Disable "Send Immediately After Characters". 

	// ----------
	// ----------

	{ WIBOX_CHANGE_MENU_CHOICE_STR,			NULL,						DEV_COMMAND_GOTO_WLAN,				WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_TOPOLOGY,			NULL,						DEV_COMMAND_GET_WIBOX_TOPOLOGY,		WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_SSID,				NULL,						DEV_COMMAND_GET_WIBOX_SSID,			WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_SECURITY_SUITE,		NULL,						DEV_COMMAND_GET_WIBOX_SECURITY,		WIBOX_WLAN_INPUT_QUESTION_STR },

	// ----------
	// 3/31/2016 ajv : These are skipped if security suite == 0, 2, or 3

	{ WIBOX_WLAN_INPUT_WEP_AUTH,			NULL,						DEV_COMMAND_GET_WEP_AUTH,			WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WEP_ENCRYPTION,		NULL,						DEV_COMMAND_GET_WEP_ENCRYPTION,		WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WEP_CHANGE_KEY,		NULL,						DEV_COMMAND_GET_WEP_CHANGE_KEY,		WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WEP_DISPLAY_KEY,		NULL,						DEV_COMMAND_GET_WEP_DISPLAY_KEY,	WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WEP_KEY_TYPE,		NULL,						DEV_COMMAND_GET_WEP_KEY_TYPE,		WIBOX_SERVER_INPUT_COLON_STR },
	{ WIBOX_WLAN_INPUT_WEP_KEY,				NULL,						DEV_COMMAND_GET_WEP_KEY,			WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WEP_TX_KEY_INDEX,	NULL,						DEV_COMMAND_GET_WEP_TX_KEY_INDEX,	WIBOX_WLAN_INPUT_QUESTION_STR },

	// ----------
	// 3/31/2016 ajv : These are skipped if security suite == 0, 1, or 3

	{ WIBOX_WLAN_INPUT_WPA_CHANGE_KEY,		NULL,						DEV_COMMAND_GET_WPA_CHANGE_KEY,		WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WPA_DISPLAY_KEY,		NULL,						DEV_COMMAND_GET_WPA_DISPLAY_KEY,	WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WPA_KEY_TYPE,		NULL,						DEV_COMMAND_GET_WPA_KEY_TYPE,		WIBOX_SERVER_INPUT_COLON_STR },
	{ WIBOX_WLAN_INPUT_WPA_KEY,				NULL,						DEV_COMMAND_GET_WPA_KEY,			WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WPA_ENCRYPTION,		NULL,						DEV_COMMAND_GET_WPA_ENCRYPTION,		WIBOX_WLAN_INPUT_QUESTION_STR },

	// ----------
	// 3/31/2016 ajv : These are skipped if security suite == 0, 1, or 2

	{ WIBOX_WLAN_INPUT_WPA2_CHANGE_KEY,		NULL,						DEV_COMMAND_GET_WPA2_CHANGE_KEY,	WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WPA2_DISPLAY_KEY,	NULL,						DEV_COMMAND_GET_WPA2_DISPLAY_KEY,	WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WPA2_KEY_TYPE,		NULL,						DEV_COMMAND_GET_WPA2_KEY_TYPE,		WIBOX_SERVER_INPUT_COLON_STR },
	{ WIBOX_WLAN_INPUT_WPA2_KEY,			NULL,						DEV_COMMAND_GET_WPA2_KEY,			WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_WPA2_ENCRYPTION,		NULL,						DEV_COMMAND_GET_WPA2_ENCRYPTION,	WIBOX_WLAN_INPUT_QUESTION_STR },

	// ----------
	// 3/31/2016 ajv : These are always present, regardless of the security suite detected.

	{ WIBOX_WLAN_INPUT_TX_DATA_RATE_1,		NULL,						DEV_COMMAND_GET_WIBOX_TX_DATA_RATE_1,		WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_TX_DATA_RATE_2,		NULL,						DEV_COMMAND_GET_WIBOX_TX_DATA_RATE_2,		WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_MIN_TX_DATA_RATE,	NULL,						DEV_COMMAND_GET_WIBOX_MIN_TX_DATA_RATE,		WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_POWER_MGMT,			NULL,						DEV_COMMAND_GET_WIBOX_POWER_MGMT,			WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_SOFT_AP_ROAMING,		NULL,						DEV_COMMAND_GET_WIBOX_SOFT_AP_ROAMING,		WIBOX_WLAN_INPUT_QUESTION_STR },
	{ WIBOX_WLAN_INPUT_MAX_FAILED_PACKETS,	NULL,						DEV_COMMAND_GET_WIBOX_MAX_FAILED_PACKETS,	WIBOX_WLAN_INPUT_QUESTION_STR },

	// ----------
	// ----------

	{ WIBOX_CHANGE_MENU_CHOICE_STR,  		NULL,  		                DEV_COMMAND_CR,   				WIBOX_CHANGE_MENU_CHOICE_STR },        // Perform a read of the programmed values. 
	{ WIBOX_CHANGE_MENU_CHOICE_STR,		    NULL,				 		DEV_COMMAND_SAVE_AND_EXIT,	   	WIBOX_CHANGE_SUCCESS_STR },				// Save and exit programming mode.
	{ WIBOX_CHANGE_SUCCESS_STR,		 	    NULL,						DEV_COMMAND_CR_NR, 		   		WIBOX_NO_TEXT_STR }, 					// CLI Disconnect. 
	{ WIBOX_NO_TEXT_STR, 			 		dev_cli_disconnect,		  	DEV_COMMAND_END, 			   	WIBOX_NO_TEXT_STR }						// Hardware disconnect. 
};                                                                      

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern UNS_32 WIBOX_sizeof_setup_read_list( void )
{
	return( (sizeof(WIBOX_setup_read_list) / sizeof(DEV_MENU_ITEM)) ); 
}

/* ---------------------------------------------------------- */
extern UNS_32 WIBOX_sizeof_dhcp_write_list( void )
{
	return( (sizeof(WIBOX_dhcp_write_list) / sizeof(DEV_MENU_ITEM)) ); 
}

/* ---------------------------------------------------------- */
extern UNS_32 WIBOX_sizeof_static_write_list()
{
	return( (sizeof(WIBOX_static_write_list) / sizeof(DEV_MENU_ITEM)) ); 
}

/* ---------------------------------------------------------- */
/**
 * This function prepares the generic device exchange pointers as well as some of the
 * default values for the settings to be programmed.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the COMM_MNGR task during the "device
 * exchange" process which reads and writes values to the radio.
 * 
 * @param dev_state 
 *
 * @author 3/23/2016 AdrianusV
 */
extern void WIBOX_initialize_detail_struct( DEV_STATE_STRUCT *dev_state )
{
	// 3/23/2016 ajv : Initialize the generic device exchange pointers to the WiBox specific
	// detail structure.
	dev_state->WIBOX_details = &WIBOX_details;

	dev_state->wibox_active_pvs = &WIBOX_active_pvs;

	dev_state->wibox_static_pvs = &WIBOX_static_pvs;

	// ----------

	// 3/23/2016 ajv : These tokens are an artifact from a development phase when the
	// "dev_state" and all the PVS structs were allocated. The tokens were a sanity check for
	// future device exchanges to verify the struct pointer had not been clobbered. Keep it here
	// since the work is done.
	strlcpy( dev_state->wibox_active_pvs->pvs_token, WIBOX_PVS_TOKEN_STR, sizeof( dev_state->wibox_active_pvs->pvs_token ) );

	strlcpy( dev_state->wibox_static_pvs->pvs_token, WIBOX_PVS_TOKEN_STR, sizeof( dev_state->wibox_static_pvs->pvs_token ) );

	// ----------

	// 3/23/2016 ajv : Set some reasonable default values.

	WIBOX_details.mask_bits = EN_PROGRAMMING_NETMASK_DEFAULT;

	// 3/23/2016 ajv : Scenario: DHCP was enabled (a DHCP name existed), it was disabled, and
	// it's about to be reenabled. We don't want to stomp on a long custom name if we ever knew
	// it. This flag tells us it's safe to stomp on any DHCP name that existed in the device the
	// last time DHCP was enabled. Or conversely, it prevents us from stomping on a name that
	// was temporarily hidden from view during the current session.
	WIBOX_details.dhcp_name_not_set = (true);

	// 3/23/2016 ajv : Clear all the string values.
	strlcpy( WIBOX_details.device,      "", sizeof( WIBOX_details.device ) ); 

	strlcpy( WIBOX_details.mac_address, "", sizeof( WIBOX_details.mac_address ) );                
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	WIBOX_STATE_wait_after_a_powerdown		(1)

#define	WIBOX_STATE_waiting_for_connection		(2)

typedef struct
{
	// 2/8/2017 rmd : Precaution - using underscore so the 'state' variable is not confused with
	// the cics.state variable!
	UNS_32	_state;
	
	UNS_32	port;
	
	UNS_32	wait_count;
	
} WIBOX_control_structure;

static WIBOX_control_structure	wibox_cs;

/* ---------------------------------------------------------- */
extern void WIBOX_initialize_the_connection_process( UNS_32 pport )
{
	if( pport != UPORT_A )
	{
		Alert_Message( "Why is the WiBox not on PORT A?" );
	}

	// 11/11/2016 rmd : For the UDS1100 we use the CD output to determine if connected. So check
	// to make sure that function is not NULL. Otherwise we couldn't call it and the connection
	// process would error out. Over and over again.
	if( port_device_table[ config_c.port_A_device_index ].__is_connected == NULL )
	{
		Alert_Message( "Unexpected NULL is_connected function" );
		
		// 2/15/2017 rmd : I think there is no point to trying to send messages anyway. They won't
		// go because we don't have a valid 'is connected' function! So do nothing in the face of
		// this error.
	}
	else
	{
		wibox_cs.port = pport;
		
		wibox_cs.wait_count = 0;
		
		// 11/11/2016 rmd : Do it. The UDS is set to auto connect. So our connect or reconnect
		// process involves cycling the power. And waiting for the connection. That's all.
		power_down_device( wibox_cs.port );
		
		// ----------
		
		// 11/11/2016 rmd : First thing we do is to power down the device. And wait 5 seconds.
		xTimerChangePeriod( cics.process_timer, MS_to_TICKS(5000), portMAX_DELAY );
	
		wibox_cs._state = WIBOX_STATE_wait_after_a_powerdown;
	}
}

/* ---------------------------------------------------------- */
extern void WIBOX_connection_processing( UNS_32 pevent )
{
	BOOL_32	lerror;
	
	lerror = (false);
	
	// ----------
	
	if( (pevent != CI_EVENT_process_string_found) && (pevent != CI_EVENT_process_timer_fired) )
	{
		// 10/29/2016 rmd : ERROR.
		Alert_Message( "Connection Process : UNXEXP EVENT" );
		
		lerror = (true);
	}
	else
	{
		switch( wibox_cs._state )
		{
			case WIBOX_STATE_wait_after_a_powerdown:
				power_up_device( wibox_cs.port );
				
				// 11/11/2016 rmd : Power up the device and wait 15 seconds to let it boot before checking
				// for the connection. Be careful not to catch some transitory CD action that may take place
				// during the boot process. I have seen some devices do this. I don't know if this one does.
				// But it doesn't matter. We can wait however long we want before checking the CD line.
				// Let's wait 15 seconds.
				xTimerChangePeriod( cics.process_timer, MS_to_TICKS(15000), portMAX_DELAY );
			
				wibox_cs._state = WIBOX_STATE_waiting_for_connection;
				break;
				
			case WIBOX_STATE_waiting_for_connection:
				if((port_device_table[ config_c.port_A_device_index ].__is_connected != NULL) )
				{
					if( port_device_table[ config_c.port_A_device_index ].__is_connected( wibox_cs.port ) )
					{
						// 11/11/2016 rmd : Okay we're done. Device connected. Update the Comm Test string so the
						// user can see the current status. And alert.
						strlcpy( GuiVar_CommTestStatus, "Wi-Fi Connection", sizeof(GuiVar_CommTestStatus) );
					
						Alert_Message( "WiBox Connected" );
						
						// ----------
						
						CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities();
					}
					else
					{
						// 1/24/2017 rmd : There is a HUGE disconnect between the device programming and the
						// controller initiated connection sequence. One is really NOT aware of the other. And they
						// do not co-exist well. For example if the connection process fails it just keeps trying
						// over and over again. And if you are performing device programming the power down-->up
						// associated with the connection process could occur right in the middle of device
						// programming. Not sure how well that would play out. And we have to allow the device
						// programming to proceed right now cause there ain't a way to stop the connection process.
						// Leaving this time large helps give the user a better chance of success while device
						// programming. A real cop-out but all I can do for right now.
						//
						// 1/24/2017 rmd : PERHAPS a mechanism to terminate the connection sequence to allow the
						// user a device programming interlude. A flag we set that the connection_processing
						// function would act upon.
						// 
						// 6/6/2017 ajv : With PremierWave XN devices using 802.1X authentication, 120-seconds
						// wasn't long enough for the device to authenticate, send data and receive a responsefrom
						// the CommServer. For now, we've arbitrarily bumped it to a full 5-minutes. Excessive,
						// yes, but without a deeper understanding of how long it actually takes, we want to make
						// sure we don't disconnect prematurely.
						// Note that this will affect both WiBox and PremierWave XN devices since they're sharing
						// this same function.
						if( wibox_cs.wait_count >= 300 )
						{
							// 11/11/2016 rmd : We've been waiting. EN normally connects very quickly. Restart the
							// connection process.
							lerror = (true);
						}
						else
						{
							wibox_cs.wait_count += 1;
							
							xTimerChangePeriod( cics.process_timer, MS_to_TICKS(1000), portMAX_DELAY );
						}
					}
				}
				else
				{
					// 11/11/2016 rmd : The sequence will restart and we have a test in the initialization
					// function for the NULL function.
					lerror = (true);
				}
				break;
				
		}  // of the switch statement
		
	}  // of an UNK event.


	if( lerror )
	{
		// 2/3/2017 rmd : Okay well the mode is no longer 'connecting'. And it shouldn't be 'ready'.
		// So set it to 'waiting to connect'. Doing so ensures when the connection timer fires and
		// we try to start a connection sequence we really to start it. If the mode were still
		// 'connecting' it is possible to not re-start the sequence.
		cics.mode = CI_MODE_waiting_to_connect;
		
		// 10/29/2016 rmd : Send the connection error event. To start the sequence over.
		CONTROLLER_INITIATED_post_event( CI_EVENT_connection_process_error );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

