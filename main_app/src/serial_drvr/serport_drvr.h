/*  file = serport_drvr.h                     02.09.2010  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef SERPORT_DRVR_H_
#define SERPORT_DRVR_H_


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"serial.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Serial port driver event queue size (# messages). At times we could queue alot of blocks. And each block queued adds an
// event to the queue. Plus the TX interrupt adds an event. So add 10 extra.
#define SERPORT_DRVR_EVENT_QSIZE			(MAX_QUEUED_BLOCKS + 10)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Serial Port Driver Event IDs
#define UPEV_NEW_TX_BLK_AVAIL			(1)
#define UPEV_TX_BLK_SENT				(2)
#define UPEV_CTS_CANNOT_SEND			(3)
#define UPEV_CTS_OKAY_TO_SEND_AGAIN		(4)
#define UPEV_CTS_MAIN_TIMER_EXPIRED		(5)
//#define UPEV_CONFIG_REQ				(6)
//#define UPEV_DISABLE_PORT				(7)
#define	UPEV_FLOW_CONTROL_GOAHEAD		(8)
#define UPEV_FLOW_CONTROL_TIMEOUT		(9)


#define SERPORT_EVENT_DEFS				UNS_32

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Serial port driver task states ... it is important that the TX_DISABLED state
// is 0 on startup. We count on that so we can test that a port has at least been
// intialized once before we service an interrupt that could sneak in during startup.
// This is related to the spi uart right now but could be important for any port channel.
// DON'T CHANGE THESE VALUES.

#define UPORT_STATE_DISABLED						(0)

#define UPORT_STATE_IDLE							(1)

#define UPORT_STATE_TX_IN_PROGRESS					(2)

#define UPORT_STATE_TX_CTS_WAIT						(3)

#define UPORT_STATE_FLOW_CONTROL_WAIT				(4)


#define	CTS_TIMER_STATE_main_timer_running			_BIT(1)
#define	CTS_TIMER_STATE_main_timer_expired			_BIT(2)


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//  This structure is used for each serial port driver instance (task)
typedef struct
{
	xQueueHandle						SerportDrvrEventQHandle;		// event queue handle

	xTimerHandle						cts_main_timer;					// FreeRTOS timer for CTS timeout
	
	// Some CTS lines are feed into GPIO pins. And hence cannot sense the interrupt transition
	// in both directions. That is a limitation of the LPC3250 itself. So once we get the
	// interrupt to signal the 'active' transition of CTS - that is to the state where we can no
	// longer transmit - we start a polling timer so that we look at CTS at an interval looking
	// to see we can transmit again.
	xTimerHandle						cts_polling_timer;				// FreeRTOS timer for CTS timeout
	
	volatile UNS_32						cts_main_timer_state;			// To allow us to sequence the timers.
	
	UNS_32								SerportTaskState;				// Serial driver task state

	volatile UART_CTL_LINE_STATE_s		modem_control_line_status;		// control line statuses

	UART_RING_BUFFER_s					UartRingBuffer_s;				// Ring buffer structure

	UART_STATS_STRUCT					stats;

	// 3/19/2013 rmd : To time how long the driver sits in the flow control state. To prevent
	// driver lock up if the TPMicro never responds. Only needed for the UPORT_TP.
	xTimerHandle						flow_control_timer;				// FreeRTOS timer

} SERPORT_DRVR_TASK_VARS_s;


// Counting on this to be zeroed on startup ... which it is ... that's good so the task state starts out
// in the TX_DISABLED state.
extern SERPORT_DRVR_TASK_VARS_s		SerDrvrVars_s[ UPORT_TOTAL_PHYSICAL_PORTS ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Function prototypes

void serial_driver_task( void *pvParameters );

void postSerportDrvrEvent( UPORTS pport, SERPORT_EVENT_DEFS pevent );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /*SERPORT_DRVR_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

