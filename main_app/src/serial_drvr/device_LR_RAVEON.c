/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// 4/9/2015 mpd : Required for atoi
#include	<stdlib.h>

#include	<string.h>

#include	<stdlib.h>

#include	"device_lr_raveon.h"

#include	"alerts.h"

#include	"comm_mngr.h"

#include	"cs_mem.h"

#include	"d_device_exchange.h"

#include	"device_common.h"

#include 	"e_lr_programming_raveon.h"	

#include	"rcvd_data.h"

#include	"serport_drvr.h"

#include 	"device_GENERIC_GR_card.h"

#include 	"flash_storage.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define RAVEON_STATE_start_of_read_stmc		(1)
#define	RAVEON_STATE_wait_for_srlnmr		(3)
#define	RAVEON_STATE_waiting_for_freq		(4)
#define RAVEON_STATE_end_of_read_stmc		(5)
#define RAVEON_STATE_wait_for_TxFreq       	(6)
#define RAVEON_STATE_wait_for_transmit_power (7)
#define RAVEON_STATE_wait_for_sw_version     (8)
#define RAVEON_STATE_wait_for_temperature    (9)
#define RAVEON_STATE_wait_for_exit			(10)
#define RAVEON_STATE_start_of_write_stmc 	(11)
#define RAVEON_STATE_pgm_TxFreq          	(12)
#define RAVEON_STATE_pgm_RxFreq				(13)
#define RAVEON_STATE_end_write_stmc 		(14)
#define RAVEON_STATE_pgm_TxPower     		(15)
#define RAVEON_STATE_store_settings			(16)
#define LR_RAVEON_EMPTY_STR					( 0xFF ) // Special case for initial I/O cycle. 
#define LR_RAVEON_CRLF_STR					"\r\n"
#define LR_RAVEON_CRLF_STR_LEN				( 2 )
#define RAVEON_VALUE_RESP_STR				"\r\nOK\r\n"
#define RAVEON_OKENDING 					"OK\r\n"
#define PGM_MODE_STRING 					"+++"
#define MAX_RESP_STRING_LEN					(30)
#define MAX_VAL_LEN							(10)
#define CR_LF_LEN							(2)
#define SONIK_ID_SRTNG 						"modem"
#define SONIK_RESP_END						"OK>"
#define FREQREQDSIZE						(8) 

// ----------

#define START_TRUE_RAVEON_STR 				"+++"
#define START_SONIK_RAVEON_STR 				"...//"
#define SERIAL_NMB_CMND_STR 				"ATSL\r"
#define TX_FRQ_CMND_STR 					"ATFT\r"
#define PWR_OUT_CMND_STR					"ATPO\r"
#define EXIT_CMND_STR						"EXIT\r"  // used by both device exchange and the id processing
#define FW_VER_CMND_STR						"ATVR\r"
#define TEMP_CMND_STR 						"ATTE\r"
#define NOCR_EXIT_CMND_STR					"EXIT"
#define SAV_CMND_STR						"ATSV\r"

// ----------

// 9/7/2017 rmd : This time is actually more than just how long it takes the radio to boot.
// It also gives us the required 500ms quite time before sending the "+++" to the radio.
// This time is used by both the ID and connection processes.
//
// 8/7/2018 rmd : You know, we have been fighting an intermittent radio id problem. I can't
// put my finger on it, but suspect the timing is on the edge. Look at this time. Includes
// powering up, booting, and +++ guard time. This was set to 2000ms, I am going to add
// 1000ms to make a total of 3000ms. In addition to lengthening this time, I reqrote the
// complete string detection method, to be more straight forward, and allow the RV-M7 to
// show anywhere in the string. So far I haven't been able to pin-point the exact problem,
// so making reasonable changes here, and hopefully fix it.
#define	RAVEON_DELAY_AFTER_POWERUP_MS	(3000)

// 9/12/2018 rmd : Still fighting the same intermittent radio id problem one month later.
// This time I saw it incorrectly ID the radio as a SONIK, which means it saw no string at
// all from the radio. The event was associated with a brown out and then 2 power failures,
// all back to back. The 2 power failures were early on during the boot process, right as we
// were powering and id'ing the radio. Maybe the radio doesn't reset correctly. So we'll
// baby the piece of shit. I'm adding a power down phase, where we'll wait 3 seconds with no
// power. This is instead of booting and immediately applying power. We shall see, this is
// just a stab at the problem, but it makes sense to try.
#define	RAVEON_DELAY_AFTER_POWERDOWN_MS	(3000)

// ----------

#define	RAVEON_ID_STATE_waiting_after_powerdown			(1)

#define	RAVEON_ID_STATE_waiting_after_powerup			(2)

#define	RAVEON_ID_STATE_waiting_for_response			(3)

#define	RAVEON_ID_STATE_test_if_raveon_on_port_B		(4)

#define	RAVEON_ID_STATE_exit							(5)

#define	RAVEON_CONNECTION_STATE_waiting_for_connection	(6)

/*------------------------------------------------------------*/
/*------------------------------------------------------------*/

//Responses from Raveon and Sonik were observed to between 1.5 to 2 seconds. So for catering 
//for field variability, we are using 3 seconds. This is the time out that is being used for
//Raveon in general 
#define LR_RAVEON_DEV_SHORTTIMER_MS  (3000) 
 
/*------------------------------------------------------------*/
/*------------------------------------------------------------*/

#define NULL_TERM_LEN 	(1)

#define TEMPSTRINGMAX 	(10)//maximum string size expected for temperature (with a little margin)
 

#define LR_RAVEON_XMT_POWER_LEN  	( 4 + NULL_TERM_LEN ) // Leave space for the "9999" invalid value.

#define LR_RAVEON_VERSION_LEN		( 6 + NULL_TERM_LEN )	// Currently only 4 in use

#define LR_RAVEON_SERIAL_LEN		( 8 + NULL_TERM_LEN )

#define LR_RAVEON_MODEL_LEN  		( 5 + NULL_TERM_LEN )

/*------------------------------------------------------------*/
/*------------------------------------------------------------*/

typedef struct
{
	UNS_32	state;
	
	UNS_32	port;
	
	UNS_32	wait_count;
	
} RAVEON_LR_control_structure;


typedef struct
{
	char xmit_freq[ 10 ];
	
	char recv_freq[ 10 ];

	char rf_xmit_power[ LR_RAVEON_XMT_POWER_LEN ];

} LR_RAVEON_DETAILS_STRUCT_LRS;


typedef struct
{
	UNS_32 operation;

	// 3/23/2015 mpd : Centralize the response buffer pointer to make "mem_free()" easier.
	char  *resp_ptr;

	UNS_32 resp_len;

	// 3/20/2015 mpd : Pointers to the programmable variables structs.
	//LR_DETAILS_STRUCT_LRS *lrs_struct;
    LR_RAVEON_DETAILS_STRUCT_LRS  *lrs_raveon_struct; 

	// 3/19/2015 mpd : These are read-only radio parameters. They are here to avoid 3 copies.
	char sw_version[ LR_RAVEON_VERSION_LEN ];

	char serial_number[ LR_RAVEON_SERIAL_LEN ];

	char model[ LR_RAVEON_MODEL_LEN ];

	char  LRTemperature[TEMPSTRINGMAX]; 

} LR_RAVEON_STATE_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static RAVEON_LR_control_structure	LR_RAVEON_cs;

static LR_RAVEON_STATE_STRUCT state_raveon_struct; 

static LR_RAVEON_STATE_STRUCT *LR_RAVEON_state = &state_raveon_struct;

static LR_RAVEON_DETAILS_STRUCT_LRS v_lrs_raveon_struct; 

BOOL_32	LR_RAVEON_PROGRAMMING_querying_device;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/15/2017 rmd : The structure and variable used to control the raveon id process which is
// run during system startup.
typedef struct
{
	UNS_32	port;
	
	UNS_32	state;
	
	BOOL_32	save_config_file_and_send_registration_to_commserver;
	
} RAVEON_id_control_structure;

static RAVEON_id_control_structure	rid_cs;

// ----------

// 9/15/2017 rmd : The structure and variable used to control the connection process. Only
// used if the device is on UPORT_A.
typedef struct
{
	UNS_32	state;
	
	UNS_32	port;
	
} RAVEON_connection_control_structure;

static RAVEON_connection_control_structure	rccs;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*!
 * 
 * @name void LR_RAVEON_initialize_state_struct()
 * @description: assigns memory to the pointer lrs_raveon_struct in the LR_RAVEON_state 
 * struct 
 * @constraints: none
 */
extern void LR_RAVEON_initialize_state_struct( void )
{
	// 3/20/2015 mpd : Set pointers to the programmable variables structs
	LR_RAVEON_state->lrs_raveon_struct = &v_lrs_raveon_struct;

	strlcpy( LR_RAVEON_state->sw_version,    "", sizeof( LR_RAVEON_state->sw_version ) );                

	strlcpy( LR_RAVEON_state->serial_number, "", sizeof( LR_RAVEON_state->serial_number ) );            

	strlcpy( LR_RAVEON_state->model,         "", sizeof( LR_RAVEON_state->model ) );                      
}

/*-------------------------------------------------------------------------------*/ 
/*!
 * 
 * @name void RAVEON_string_exchange_with_specific_termination_string_hunt(const char *pcommand_str, char *presponse_str)
 * @description: sets up the command to be sent to the serial Tx, and response type to look 
 * for by the serial receive and enables the correct "hunt" function to capture this 
 * response 
 * @constraints: none
 * @param const char* pcommand_str: command string to be sent
 * @param char* presponse_str: response string expected
 */
static void RAVEON_string_exchange_with_specific_termination_string_hunt( const char* pcommand_str, char* presponse_str )
{
	// 3/14/2014 rmd : This function is executed within the context of the comm_mngr task. We
	// can reference and change the device exchange variables as needed.

	DATA_HANDLE	ldh;
	
	// 2/13/2014 rmd : Clean and restart the port buffer and string hunt. Note this disables
	// packet hunting mode.
	RCVD_DATA_enable_hunting_mode( comm_mngr.device_exchange_port, PORT_HUNT_FOR_SPECIFIED_TERMINATION, WHEN_STRING_FOUND_NOTIFY_THE_COMM_MNGR_TASK );

	// ----------

	// 3/10/2015 mpd : Identify the termination string.
	SerDrvrVars_s[ comm_mngr.device_exchange_port ].UartRingBuffer_s.th.str_to_find =  presponse_str;
	
	SerDrvrVars_s[ comm_mngr.device_exchange_port ].UartRingBuffer_s.th.chars_to_match = strlen( presponse_str );
	
	// ----------

	ldh.dptr = (UNS_8*)pcommand_str;

	ldh.dlen = strlen( pcommand_str );

	// 3/12/2015 mpd : Don't try to add a zero length command to the Xmit list. That triggers
	// alerts in "comm_mngr". Also, check for a special "LR_EMPTY_STR" value used to flag the
	// first I/O cycle.  The first read cycle sets up the hunt for an input that is triggered by
	// radio initialization hardware - not a xmit command. Sending a command here for the first
	// cycle would trigger an additional unwanted radio response.
	if( (ldh.dlen > 0) && (pcommand_str[ 0 ] != LR_RAVEON_EMPTY_STR) )
	{
		AddCopyOfBlockToXmitList( comm_mngr.device_exchange_port, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );
	}
}

/*--------------------------------------------------------------- */ 
/*!
 * 
 * @name void Extract_val_from_msg(char *rcvd_buff, char *valString)
 * @description: The serial data analysis task, sends the entire received message (ending in 
 * a specific ending); This function extracts the required string from the message  
 * @constraints: none
 * @param char* rcvd_buff: pointer to the buffer holding the full message
 * @param char* valString: pointer to the extracted required string 
 */
static void Extract_val_from_msg(char * rcvd_buff, char * valString)
{
	int indx = 0; 
	int indx2 = 0;  

	while( (rcvd_buff[indx] != '\r') && (indx < MAX_RESP_STRING_LEN) )
	{
		indx++; 
	}
	if( indx <= MAX_RESP_STRING_LEN  )
	{
		indx += CR_LF_LEN; 		//move to the first chacter of data string, by stepping over "\r\n"

		while( (rcvd_buff[indx] != NULL) && (indx2 < MAX_VAL_LEN) && (rcvd_buff[indx+indx2] != '\r') )
		{
			//rcvd_str[1][indx2] = pq_msg->dh.dptr[indx +indx2]; 
			valString[indx2] = rcvd_buff[indx +indx2];
			indx2++; 
		}
		if( indx2 < MAX_VAL_LEN )
		{
			//rcvd_str[1][indx2] = NULL; 
			valString[indx2] = NULL;
		}
		else Alert_Message( "Radio response parsing error in function LR_RAVEON_exchange_processing" ); 
	}
	else Alert_Message( "Radio response parsing error in function LR_RAVEON_exchange_processing" ); 
}

/*---------------------------------------------------------------  */
/*!
 * 
 * @name void LR_RAVEON_initialize_device_exchange(void) 
 * @description: Makes call to initialize the LR_RAVEON_state struct;  power cycles the M5 
 * and SONIK radios and sends command (depending on Raveon or Sonik) to the LR to go to
 * program mode. 
 * @constraints: none 
 */
extern void LR_RAVEON_initialize_device_exchange(void)
{
	if( LR_RAVEON_state->lrs_raveon_struct != &v_lrs_raveon_struct )
	{
		// 4/8/2015 mpd : The state structure is invalid. This is normal upon first entering the LR programming screen. 
		// Handle all the first-time initialization.
		LR_RAVEON_initialize_state_struct( );
	}

	if( GuiVar_LRRadioType != RAVEON_RADIO_ID_M7 ) //for M5 and sonik skyline
	{
		//8/29/2017 BE: cycle power because because after 30 seconds of silence, M5 is 
		//non-responsive and SONIK needs the program command within five seconds of power up 
		__GENERIC_gr_card_power_control(GuiVar_LRPort, false); 
		//Based on power up measurements, allowing two seconds for proper shutdown of the radios
		vTaskDelay(MS_to_TICKS( 2000 )); 
	
		__GENERIC_gr_card_power_control(GuiVar_LRPort, true); 
		//After power up, the average time noted for the Raveon/Sonik radios to get ready to 
		//respond, is close to 2 seconds, so allowing for field variability, making it 3 seconds 
		vTaskDelay(MS_to_TICKS( 3000 ));  
	}

	if( comm_mngr.device_exchange_state == LR_RAVEON_WRITE)
	{
		LR_RAVEON_cs.state = RAVEON_STATE_start_of_write_stmc; 

		if( GuiVar_LRRadioType == RAVEON_RADIO_ID_SONIK )
		{ 
			//9/11/2017 be: CLARIFICATION: per C standard: static variables, constants and allocations 
			//by malloc, etc(i.e. allocation of memory to the heap) do NOT go on the stack of the 
			//function. Hence in the function below, the pointer parameter equals the argument
			//(SONIK_RESP_END) that is a constant; this pointer will be passed to other pointers that 
			//are outside the scope of this function, but the data needs to be valid, which is achieved 
			//by the fact that the constant is NOT stored on the stack. 
			//In some cases, when the value is not known a-priori at compile time (e.g. the frequency 
			//the user is going to select to program), pointer to a constant is used (const char *), to 
			//ensure, the called functions do not unintentially or intentionally change this value. 
			//IMPORTANT NOTE: In this case the value will be on the stack of this function, and so is 
			//available to all functions called inside this function only; to pass this value outside 
			//the function, copy the contents of this pointer to a specific location, using, e.g. 
			//"memcpy" instruction or copy an element at a time for a struct. 
			 
			//8/7/2017 BE: call function to send command string to the Radio, and setup the 
			//receive-ring-buffer-analysis to look for expected ending from the Radio  
			RAVEON_string_exchange_with_specific_termination_string_hunt( START_SONIK_RAVEON_STR, SONIK_RESP_END );
		}
		else
		{
			RAVEON_string_exchange_with_specific_termination_string_hunt( START_TRUE_RAVEON_STR, RAVEON_OKENDING );
		}
	}
	else
	{
		RAVEON_string_exchange_with_specific_termination_string_hunt( START_TRUE_RAVEON_STR, RAVEON_OKENDING );
		LR_RAVEON_cs.state = RAVEON_STATE_start_of_read_stmc;
	}

	//8/29/2017 BE: send the program mode command and set up the resp. hunt
	xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LR_RAVEON_DEV_SHORTTIMER_MS), portMAX_DELAY );
}

/* ---------------------------------------------------------- */
/*!
 * 
 * @name void LR_RAVEON_exchange_processing(COMM_MNGR_TASK_QUEUE_STRUCT *pq_msg)
 * @description: Runs two state machines for device exchange: i) for reading the M7 & M5;
 * iii) programming all three: M7, M5, and Sonik. 
 * @constraints: none 
 * @param COMM_MNGR_TASK_QUEUE_STRUCT* pq_msg: pointer to the comm_mngr Q
 */
extern void LR_RAVEON_exchange_processing ( COMM_MNGR_TASK_QUEUE_STRUCT *pq_msg)
{
	char  cmnd_Str[40]; 
	UNS_32 device_exchange_result; 
	UNS_32 degC_temp;
	UNS_32 degF_temp;   
	BOOL_32  dev_rd_error = (false);
	BOOL_32 dev_pgm_error = (false);   
	UNS_8 indx1; 
	UNS_8 freqSize ; 
	DATA_HANDLE ldh; 
	static UNS_8 chnlIndx = 0; 
	static char chnlString[3]; 
	static char asciFreq[10]; 

	if(comm_mngr.device_exchange_state == LR_RAVEON_READ )
	{
		//8/7/17 BE: RAVEON is in the read mode, and therefore will run the read state machine
		switch( LR_RAVEON_cs.state )
		{
			//8/15/2017 BE: Reminder: length of the message returned by the 
			//specified_termination_string_hunt is the whole received buffer plus one termination NULL 
			//character 
	 
			case RAVEON_STATE_start_of_read_stmc: 
			{
				if( pq_msg->event == COMM_MNGR_EVENT_device_exchange_response_timer_expired)
				{
					dev_rd_error = (true); //timer fired
					LR_RAVEON_cs.state  = RAVEON_STATE_end_of_read_stmc;
				} 
				else
				{
					mem_free( pq_msg->dh.dptr ); 
					//Setup the srldrvr and the rcvd_data task for sending the request 
					RAVEON_string_exchange_with_specific_termination_string_hunt( SERIAL_NMB_CMND_STR, RAVEON_OKENDING );

					xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LR_RAVEON_DEV_SHORTTIMER_MS), portMAX_DELAY );	
					LR_RAVEON_cs.state =  RAVEON_STATE_wait_for_srlnmr;
				}
				break; 
			}

			case RAVEON_STATE_wait_for_srlnmr: 
			{
				if( pq_msg->event == COMM_MNGR_EVENT_device_exchange_response_timer_expired)
					{
						dev_rd_error = (true); //timer fired
						LR_RAVEON_cs.state  = RAVEON_STATE_end_of_read_stmc;
					} 
				else 
					{
						xTimerStop( comm_mngr.timer_device_exchange, portMAX_DELAY );

						//get the serial number and store in state settings; release the allocated memory by the 
						// rcvd_data task  
						Extract_val_from_msg((char *)pq_msg->dh.dptr, LR_RAVEON_state->serial_number ); 
				 
						//Free the memory obained by the receive task.
						mem_free( pq_msg->dh.dptr );

						//Setup the srldrvr and the rcvd_data task for sending the request  
						RAVEON_string_exchange_with_specific_termination_string_hunt( TX_FRQ_CMND_STR, RAVEON_OKENDING );

						xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LR_RAVEON_DEV_SHORTTIMER_MS), portMAX_DELAY );	
						LR_RAVEON_cs.state =  RAVEON_STATE_wait_for_TxFreq; 
					}
				break; 
			}
			case RAVEON_STATE_wait_for_TxFreq: 
			{
				if( pq_msg->event == COMM_MNGR_EVENT_device_exchange_response_timer_expired)
					{
						dev_rd_error = (true); //timer fired
						LR_RAVEON_cs.state  = RAVEON_STATE_end_of_read_stmc;
					} 
				else 
					{
						xTimerStop( comm_mngr.timer_device_exchange, portMAX_DELAY );

						//get the frequency and store in device state parameter settings; release the allocated
						//memory for the comm_mngr Q 
						Extract_val_from_msg((char *)pq_msg->dh.dptr, LR_RAVEON_state->lrs_raveon_struct->xmit_freq ); 

						//Free the memory obained by the receive task.
						mem_free( pq_msg->dh.dptr );	

						if( GuiVar_LRRadioType == RAVEON_RADIO_ID_M7 )
						{
							//Setup the srldrvr and the rcvd_data task for sending the request   
							RAVEON_string_exchange_with_specific_termination_string_hunt( PWR_OUT_CMND_STR, RAVEON_OKENDING );
							xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LR_RAVEON_DEV_SHORTTIMER_MS), portMAX_DELAY );	 
							LR_RAVEON_cs.state =  RAVEON_STATE_wait_for_transmit_power; 
						}
						else
						{
							//Setup the srldrvr and the rcvd_data task for exit
							RAVEON_string_exchange_with_specific_termination_string_hunt( EXIT_CMND_STR, RAVEON_OKENDING);

							xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LR_RAVEON_DEV_SHORTTIMER_MS), portMAX_DELAY );	

							LR_RAVEON_cs.state  = RAVEON_STATE_wait_for_exit;  //8/23/2017 BE: for M5 do not run anymore reads
						}
					}
				break; 
			}
			case RAVEON_STATE_wait_for_transmit_power:
			{	
				if( pq_msg->event == COMM_MNGR_EVENT_device_exchange_response_timer_expired)
					{
						dev_rd_error = (true); //timer fired
						LR_RAVEON_cs.state  = RAVEON_STATE_end_of_read_stmc;
					}
				else 
					{
						if( GuiVar_LRRadioType == RAVEON_RADIO_ID_M7 )
						{	
							xTimerStop( comm_mngr.timer_device_exchange, portMAX_DELAY );

							//Extract power from mesage and store in device state parameter settings; release the 
							//allocated memory for the comm_mngr Q 
							Extract_val_from_msg((char *)pq_msg->dh.dptr, LR_RAVEON_state->lrs_raveon_struct->rf_xmit_power);

							//Free the memory obained by the receive task.
							mem_free( pq_msg->dh.dptr );
							
							//Setup the srldrvr and the rcvd_data task for sending the request     
							RAVEON_string_exchange_with_specific_termination_string_hunt( FW_VER_CMND_STR, RAVEON_OKENDING );

							//start timer for timeout for the srlnmr request
							xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LR_RAVEON_DEV_SHORTTIMER_MS), portMAX_DELAY );	

							LR_RAVEON_cs.state =  RAVEON_STATE_wait_for_sw_version; 
						}
						else
						{
							dev_rd_error = (true);
							LR_RAVEON_cs.state  = RAVEON_STATE_end_of_read_stmc;
						}
					}
				break;  
			}
			case RAVEON_STATE_wait_for_sw_version:
			{
				if( pq_msg->event == COMM_MNGR_EVENT_device_exchange_response_timer_expired)
				{
						dev_rd_error = (true); //timer fired
						LR_RAVEON_cs.state  = RAVEON_STATE_end_of_read_stmc;
				}
				else
				{
					if( GuiVar_LRRadioType == RAVEON_RADIO_ID_M7 )
					{
						xTimerStop( comm_mngr.timer_device_exchange, portMAX_DELAY );
						// get the Radio software version and store in device setttings; release the allocated
						// memory by the rcvd_data task 
						Extract_val_from_msg((char *)pq_msg->dh.dptr, LR_RAVEON_state->sw_version); 
			
						//Free the memory obained by the receive task.
						mem_free( pq_msg->dh.dptr );

						//Setup the srldrvr and the rcvd_data task for requesting temperature BE???????????? const 
						//char  cmnd_Str[] = "ATTE\r"; 
						RAVEON_string_exchange_with_specific_termination_string_hunt( TEMP_CMND_STR, RAVEON_OKENDING );

						xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LR_RAVEON_DEV_SHORTTIMER_MS), portMAX_DELAY );	

						LR_RAVEON_cs.state =  RAVEON_STATE_wait_for_temperature; 
					}
					else 
					{
						dev_rd_error = (true);
						LR_RAVEON_cs.state  = RAVEON_STATE_end_of_read_stmc;
					}
				}
				break;
			}
			case RAVEON_STATE_wait_for_temperature:
			{	
				if( pq_msg->event == COMM_MNGR_EVENT_device_exchange_response_timer_expired)
				{
						dev_rd_error = (true); //timer fired
						LR_RAVEON_cs.state  = RAVEON_STATE_end_of_read_stmc;
				}
				else 
				{
					if( GuiVar_LRRadioType == RAVEON_RADIO_ID_M7 )
					{
						xTimerStop( comm_mngr.timer_device_exchange, portMAX_DELAY );
					
						//get the Radio temp, convert from C to F,  and store in device state parameter settings; 
						Extract_val_from_msg((char *)pq_msg->dh.dptr, LR_RAVEON_state->LRTemperature); 

						//The temperature received is in deg C! Need to convert to deg F
						degC_temp = atoi(LR_RAVEON_state->LRTemperature); 
						degF_temp = degC_temp*(9/5) + 32; 
						sprintf( LR_RAVEON_state->LRTemperature, "%d", degF_temp); 

						//Free the memory obained by the receive task.
						mem_free( pq_msg->dh.dptr );
						//Setup the srldrvr and the rcvd_data task for exit BE????????????? const char  cmnd_Str[] =
						//"EXIT\r"; 
						RAVEON_string_exchange_with_specific_termination_string_hunt( EXIT_CMND_STR, RAVEON_OKENDING);

						xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LR_RAVEON_DEV_SHORTTIMER_MS), portMAX_DELAY );	

						LR_RAVEON_cs.state =  RAVEON_STATE_wait_for_exit; 
					} 
					else 
					{
						dev_rd_error = (true);
						LR_RAVEON_cs.state  = RAVEON_STATE_end_of_read_stmc;
					}
				}
				break;
			}
			case RAVEON_STATE_wait_for_exit:
			{	 
				if( pq_msg->event == COMM_MNGR_EVENT_device_exchange_response_timer_expired)
				{
						dev_rd_error = (true); //timer fired
						LR_RAVEON_cs.state  = RAVEON_STATE_end_of_read_stmc;
				}
				else 
				{
					//9/8/2017 BE: This check ensures the EXIT command got to the radio and it went into program 
					//mode.  
					xTimerStop( comm_mngr.timer_device_exchange, portMAX_DELAY );

					//Free the memory obained by the receive task.
					mem_free( pq_msg->dh.dptr );

					LR_RAVEON_cs.state =  RAVEON_STATE_end_of_read_stmc; 
				}
				break;
			}
		} //8/23/2017 BE: end of switch() for the read state machine
		if( dev_rd_error )
		{
			//send error message 
			//8/15/2017 set the error key code and post on the key processing Q
			device_exchange_result = DEVICE_EXCHANGE_KEY_read_settings_error;
			COMM_MNGR_device_exchange_results_to_key_process_task( device_exchange_result );
		}
		else 
		{
			if(LR_RAVEON_cs.state  == RAVEON_STATE_end_of_read_stmc )
			{
				device_exchange_result = DEVICE_EXCHANGE_KEY_read_settings_completed_ok; 
				COMM_MNGR_device_exchange_results_to_key_process_task( device_exchange_result );
			}
		}
	}//8/15/2017 BE endof:  if(comm_mngr.device_exchange_state == LR_RAVEON_READ )   
	else 
	{		
		//Start of comm_mngr.device_exchange_state == LR_RAVEON_WRITE 	 
		//No checking of "OK\r\n", in repsonse from the Radio; since the only way, aside from the 
		//timeout timer firing, this code is executing is because the "OK\r\n" was received by the 
		//ring buffer. 
		//RAVEON is in write mode, the write state machine will run 
		switch( LR_RAVEON_cs.state )
		{
			//8/15/2017 BE NOTE: the length of the message returned by the specified_termination_string 
			//is the whole buffer plus one termination NULL character
			case RAVEON_STATE_start_of_write_stmc: 
			{	
				//this state waits for response to the start_pgm_string. 

				if( pq_msg->event == COMM_MNGR_EVENT_device_exchange_response_timer_expired)
				{
					dev_pgm_error = (true); //timer fired
					LR_RAVEON_cs.state =  RAVEON_STATE_end_write_stmc; //skip other programming step
				} 
				else
				{
					xTimerStop( comm_mngr.timer_device_exchange, portMAX_DELAY ); 

					//Free the memory obained by the receive task.
					mem_free( pq_msg->dh.dptr );

					if(( GuiVar_LRRadioType == RAVEON_RADIO_ID_M7) || (GuiVar_LRRadioType == RAVEON_RADIO_ID_M5))
					{
						//Setup the srldrvr and the rcvd_data task for programming transmit frequency 
						sprintf(cmnd_Str,    "%s%s\r", "ATFT ", (char *) LR_RAVEON_state->lrs_raveon_struct->xmit_freq) ;
						RAVEON_string_exchange_with_specific_termination_string_hunt( cmnd_Str, RAVEON_VALUE_RESP_STR ); 

						//change state to RAVEON_STATE_pgm_TxFreq
						LR_RAVEON_cs.state =  RAVEON_STATE_pgm_TxFreq; 
						xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LR_RAVEON_DEV_SHORTTIMER_MS), portMAX_DELAY );	
					}
					else if( GuiVar_LRRadioType == RAVEON_RADIO_ID_SONIK )
					{
						if(  chnlIndx == 0)
						{
							//create fulll eight digit ascii rep of the frequency: 
							strcpy(asciFreq, (char *) LR_RAVEON_state->lrs_raveon_struct->xmit_freq );  //just for ease of reading
							freqSize = strlen(asciFreq); 
							for(indx1 = 0; indx1 <(FREQREQDSIZE - freqSize); indx1++ )
							{
								asciFreq[freqSize + indx1] = '0'; 
							}
							asciFreq[freqSize + indx1] = '\0'; 
						}
						//Program all 64 channels for sonik 
						if( chnlIndx <64 )
						{
							//convert channel index to two character string: 
							sprintf(chnlString, "%d", chnlIndx );
							if(chnlIndx <= 9  )
							{
								chnlString[2] = '\0'; 
								chnlString[1] = chnlString[0];
								chnlString[0] = '0';  
							}
							sprintf(cmnd_Str,    "%s%sR%sT%s\r", ".FRQ", chnlString, asciFreq, asciFreq ) ;
							RAVEON_string_exchange_with_specific_termination_string_hunt( cmnd_Str, SONIK_RESP_END ); 
							if(chnlIndx >= 63  )
							{
								LR_RAVEON_cs.state =  RAVEON_STATE_pgm_TxFreq; 
							}
							chnlIndx++; 
							xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LR_RAVEON_DEV_SHORTTIMER_MS), portMAX_DELAY );	
						}
					}
					else
					{
						Alert_Message ("Unknown Response from the Radio" );
					}
				}
				break; 
			}
			case RAVEON_STATE_pgm_TxFreq: 
			{					 
				if( pq_msg->event == COMM_MNGR_EVENT_device_exchange_response_timer_expired)
				{
					dev_pgm_error = (true); 
					LR_RAVEON_cs.state =  RAVEON_STATE_end_write_stmc; //skip other programming steps
				}
				else
				{
					xTimerStop( comm_mngr.timer_device_exchange, portMAX_DELAY );   
					//Free the memory obained by the receive task.
					mem_free( pq_msg->dh.dptr );
					//------------------ 
					//skip other programming steps for SONIK 
					if( GuiVar_LRRadioType == RAVEON_RADIO_ID_SONIK )
					{
						//the Setup the srldrvr and the rcvd_data task for storing the programmed settings 

						//No receive buffer is enabled, because SONIK does not send back anything, on receiving the 
						//EXIT command. Only the command is loaded on the serial transmit Q
						ldh.dptr = (UNS_8*)NOCR_EXIT_CMND_STR;

						ldh.dlen = strlen( NOCR_EXIT_CMND_STR );
						
						AddCopyOfBlockToXmitList( comm_mngr.device_exchange_port, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );
						
						LR_RAVEON_cs.state  = RAVEON_STATE_end_write_stmc; 
					}
					else 
					{
						//Setup the srldrvr and the rcvd_data task for programming the Rx frequency
						sprintf(cmnd_Str,    "%s%s\r", "ATFR ", (char *) LR_RAVEON_state->lrs_raveon_struct->recv_freq) ; 
						RAVEON_string_exchange_with_specific_termination_string_hunt( cmnd_Str, RAVEON_OKENDING );  

						xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LR_RAVEON_DEV_SHORTTIMER_MS), portMAX_DELAY );	

						LR_RAVEON_cs.state =  RAVEON_STATE_pgm_RxFreq; 
                    }
				}
				break; 
			}
			case RAVEON_STATE_pgm_RxFreq: 
			{
				if( pq_msg->event == COMM_MNGR_EVENT_device_exchange_response_timer_expired)
				{
					dev_pgm_error = (true); //timer fired
					LR_RAVEON_cs.state =  RAVEON_STATE_end_write_stmc; //skip other programming steps
				} 
				else
				{
					xTimerStop( comm_mngr.timer_device_exchange, portMAX_DELAY ); 

					//Free the memory obained by the rcvd_data task.
					mem_free( pq_msg->dh.dptr );

					if( GuiVar_LRRadioType == RAVEON_RADIO_ID_M7 )
					{
						//Setup the srldrvr and the rcvd_data task for programming the power out 
						sprintf(cmnd_Str,    "%s%s\r", "ATPO ", (char *) LR_RAVEON_state->lrs_raveon_struct->rf_xmit_power) ;
						RAVEON_string_exchange_with_specific_termination_string_hunt( cmnd_Str, RAVEON_OKENDING );  

						xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LR_RAVEON_DEV_SHORTTIMER_MS), portMAX_DELAY );	 

						LR_RAVEON_cs.state =  RAVEON_STATE_pgm_TxPower; 
					}
					else
					{
						//Setup the srldrvr and the rcvd_data task for storing the programmed settings for M5 
						RAVEON_string_exchange_with_specific_termination_string_hunt( SAV_CMND_STR, RAVEON_OKENDING );  

						xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LR_RAVEON_DEV_SHORTTIMER_MS), portMAX_DELAY );

						LR_RAVEON_cs.state =  RAVEON_STATE_store_settings; //skip other programming steps for M5 and store settings
					}
				}
				break; 
			}
			case RAVEON_STATE_pgm_TxPower: 
			{				 
				if( pq_msg->event == COMM_MNGR_EVENT_device_exchange_response_timer_expired)
				{
					dev_pgm_error = (true); //timer fired
					LR_RAVEON_cs.state =  RAVEON_STATE_end_write_stmc; //skip other programming steps
				}
				else
				{
					if( GuiVar_LRRadioType == RAVEON_RADIO_ID_M7 )
					{
						xTimerStop( comm_mngr.timer_device_exchange, portMAX_DELAY );  

						//Free the memory allocated by the receive task.
						mem_free( pq_msg->dh.dptr );

						//Setup the srldrvr and the rcvd_data task for storing the programmed settings 
						RAVEON_string_exchange_with_specific_termination_string_hunt( SAV_CMND_STR, RAVEON_OKENDING );  

						xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LR_RAVEON_DEV_SHORTTIMER_MS), portMAX_DELAY );	 

						LR_RAVEON_cs.state =  RAVEON_STATE_store_settings;
					}
				}
				break;
			}
			case RAVEON_STATE_store_settings:					
			{

				if( pq_msg->event == COMM_MNGR_EVENT_device_exchange_response_timer_expired)
				{
					dev_pgm_error = (true); 
					LR_RAVEON_cs.state =  RAVEON_STATE_end_write_stmc; //skip other programming steps
				}
				else
				{
					xTimerStop( comm_mngr.timer_device_exchange, portMAX_DELAY );
					//Free the memory obained by the rcvd_data task.
					mem_free( pq_msg->dh.dptr );
					LR_RAVEON_cs.state =  RAVEON_STATE_end_write_stmc;
				}
				break;
			}
		}  //8/24/2017 BE: end of switch( LR_RAVEON_cs.state ) for the WRITE stmc

		if( dev_pgm_error == (true) )
		{
			chnlIndx = 0;
			//If the timer expired in the write mode 
			//8/15/2017 set the device write error key code and post on the key processing Q
			device_exchange_result = DEVICE_EXCHANGE_KEY_write_settings_error; 
			COMM_MNGR_device_exchange_results_to_key_process_task( device_exchange_result );
		}
		else
		{
			if( LR_RAVEON_cs.state == RAVEON_STATE_end_write_stmc )
			{
				chnlIndx = 0; 
				device_exchange_result = DEVICE_EXCHANGE_KEY_write_settings_completed_ok; 
				COMM_MNGR_device_exchange_results_to_key_process_task( device_exchange_result );
			}//8/24/2017 send the success message to the key processing task
		}
	} //8/15/2017 BE: End of write mode processing
}

/* ---------------------------------------------------------- */
/*!
 * 
 * @name void LR_RAVEON_PROGRAMMING_extract_changes_from_guivars(void)
 * @description: copies from gui variables to the settings' variables for the lr 
 * @constraints: none 
 */
extern void LR_RAVEON_PROGRAMMING_extract_changes_from_guivars( void )
{
	if( GuiVar_LRRadioType == RAVEON_RADIO_ID_M7 )
	{
		//9/5/2017 is to map the 1 -- 10 power to 10 -- 100% as required by the M7 radio interface
		snprintf( LR_RAVEON_state->lrs_raveon_struct->rf_xmit_power, sizeof(LR_RAVEON_state->lrs_raveon_struct->rf_xmit_power), "%2d", GuiVar_LRTransmitPower*10 ); 
	}

	// 2/26/2018 ajv : Because we're working with integer values, a demical value of 0675 will 
	// lose it's leading 0, resulting in a value of 6750. Therefore, manually insert the leading 
	// 0 if there are only three digits in the decimal value. 
	if( GuiVar_LRFrequency_Decimal < 1000 )
	{
		snprintf( LR_RAVEON_state->lrs_raveon_struct->xmit_freq, 9, "%d.0%d", GuiVar_LRFrequency_WholeNum, GuiVar_LRFrequency_Decimal );

		snprintf(LR_RAVEON_state->lrs_raveon_struct->recv_freq, 9, "%d.0%d", GuiVar_LRFrequency_WholeNum, GuiVar_LRFrequency_Decimal); 
	}
	else
	{
		snprintf( LR_RAVEON_state->lrs_raveon_struct->xmit_freq, 9, "%d.%d", GuiVar_LRFrequency_WholeNum, GuiVar_LRFrequency_Decimal );

		snprintf(LR_RAVEON_state->lrs_raveon_struct->recv_freq, 9, "%d.%d", GuiVar_LRFrequency_WholeNum, GuiVar_LRFrequency_Decimal); 
	}
}

/*----------------------------------------------------------------------------------------------*/ 
/*!
 * 
 * @name void LR_RAVEON_PROGRAMMING_copy_settings_into_guivars(void)
 * @description: copies the new lr settings to gui variables for display
 * @constraints: none
 */
extern void LR_RAVEON_PROGRAMMING_copy_settings_into_guivars( void )
{
	char  tempWholeNum[10];

	char  tempDecimal[10];

	UNS_16 lpIndx;  

	strlcpy( GuiVar_LRSerialNumber, LR_RAVEON_state->serial_number, sizeof(GuiVar_LRSerialNumber) );
	//place first three chars in temp_FreqWhole, next four in temp_FreqDecimal, convert to 
	//integers and load in corresponding Gui variables
	for(lpIndx = 0; lpIndx <3; lpIndx++ )
	{
		tempWholeNum[lpIndx] = LR_RAVEON_state->lrs_raveon_struct->xmit_freq[lpIndx];
	}
	tempWholeNum[lpIndx] = '\0'; //make it a string

	if(LR_RAVEON_state->lrs_raveon_struct->xmit_freq[3] == '.' )
	{			
		for(lpIndx=4; lpIndx <8; lpIndx++ )
		{
			tempDecimal[lpIndx-4] = LR_RAVEON_state->lrs_raveon_struct->xmit_freq[lpIndx];
		}
		tempDecimal[lpIndx-4] = '\0'; //make it a string
	}
	else
	{
		for(lpIndx=3; lpIndx <7; lpIndx++ )
		{
			tempDecimal[lpIndx-3] = LR_RAVEON_state->lrs_raveon_struct->xmit_freq[lpIndx];
		}
		tempDecimal[lpIndx-3] = '\0'; //make it a string
	}
	GuiVar_LRFrequency_WholeNum = (UNS_32) atoi(tempWholeNum);
	GuiVar_LRFrequency_Decimal = (UNS_32) atoi(tempDecimal);  

	//snap the reading to 12.5Khz boundary
	if( GuiVar_LRFrequency_Decimal % 125 != 0 )
	{
		GuiVar_LRFrequency_Decimal = GuiVar_LRFrequency_Decimal - (GuiVar_LRFrequency_Decimal % 125) + 125;
	}
						
	if( GuiVar_LRRadioType == RAVEON_RADIO_ID_M7 )
	{
		GuiVar_LRTransmitPower = atoi(LR_RAVEON_state->lrs_raveon_struct->rf_xmit_power);
		//map power from 10% -- 100% to 10 -- 1 to conform to the UI of other radios
		GuiVar_LRTransmitPower	 = 	GuiVar_LRTransmitPower/10; 	
						
		strlcpy(GuiVar_LRFirmwareVer, LR_RAVEON_state->sw_version, 3);
		GuiVar_LRTemperature = atoi(LR_RAVEON_state->LRTemperature);

		// 9/15/2017 ajv : The HUB feature is always available for Raveon RV-M7 radios.
		GuiVar_LRHubFeatureNotAvailable = (false);
	}
	else if( config_c.purchased_options.option_HUB == (true) ) 
	{
		// 9/15/2017 ajv : We've now identified the radio is either a Sonik SkyLine or Raveon RV-M5 
		// (FireLine) radio. As we know, code distribution is not functional on these radios due to 
		// the inability to run them at 100% duty cycle. Therefore, if the HUB option is set on one 
		// of these radios, display a message on the screen that the HUB feature will not function. 
		GuiVar_LRHubFeatureNotAvailable = (true);
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*!
 * 
 * @name void raveon_id_setup_string_hunt(const char *pcommand_str, char *presponse_str)
 * @description: sets up the string hunt for determining if the lr is M7, M5 or SONIK 
 * @constraints: none 
 * @param const char* pcommand_str: command string to be sent 
 * @param char* presponse_str: response end that is expected 
 */
static void raveon_id_setup_string_hunt( const char* pcommand_str )
{
	// 3/14/2014 rmd : This function is executed within the context of the CI task.

	DATA_HANDLE	ldh;
	
	// ----------
	
	// 2/13/2014 rmd : Clean and restart the port buffer and string hunt. Note this disables
	// packet hunting mode.
	RCVD_DATA_enable_hunting_mode( rid_cs.port, PORT_HUNT_FOR_CRLF_DELIMITED_STRING, WHEN_STRING_FOUND_NOTIFY_THE_CI_TASK );
	
	SerDrvrVars_s[ rid_cs.port ].UartRingBuffer_s.sh.find_initial_CRLF = (false); 

	// ----------
	
	ldh.dptr = (UNS_8*)pcommand_str;

	ldh.dlen = strlen( pcommand_str );

	// 10/29/2016 rmd : Sometimes we aren't sending a string but are looking for a response. Out
	// of reset for example.
	if( ldh.dlen > 0 )
	{
		AddCopyOfBlockToXmitList( rid_cs.port, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );
	}
}

/* ---------------------------------------------------------- */
static void raveon_id_record_change_in_config_file( UNS_32 const praveon_radio_id )
{
	if( rid_cs.port == UPORT_A )
	{
		if( config_c.purchased_options.port_a_raveon_radio_type != praveon_radio_id )
		{
			config_c.purchased_options.port_a_raveon_radio_type = praveon_radio_id;
			
			rid_cs.save_config_file_and_send_registration_to_commserver = (true);
		}
	}
	else
	if( rid_cs.port == UPORT_B )
	{
		if( config_c.purchased_options.port_b_raveon_radio_type != praveon_radio_id )
		{
			config_c.purchased_options.port_b_raveon_radio_type = praveon_radio_id;
			
			rid_cs.save_config_file_and_send_registration_to_commserver = (true);
		}
	}
}

/* ---------------------------------------------------------- */
/*!
 * 
 * @name void RAVEON_id_start_the_process(void)
 * @description: Powers up the LR on port A or Port B. Sets up a timer to send message to
 * CI, when the wait for power up is completed.
 * @constraints: none 
 */
extern void RAVEON_id_start_the_process( void )
{
	// 9/7/2017 rmd : Executed solely within the context of the CONTROLLER_INITATED task.
	// There are no MUTEX requirements for the caller.
	
	// ----------
	
	BOOL_32	powerdown_and_start_the_timer;
	
	// ----------
	
	powerdown_and_start_the_timer = (true);
	
	// ----------

	// 9/7/2017 rmd : Though this should be needed as the screen would never be drawn without
	// any Raveon radio present, but set a known default radio type. At least then if our ID
	// scheme fails we have a valid type in place.
	GuiVar_LRRadioType = RAVEON_RADIO_ID_M7;
	
	// 9/15/2017 rmd : No need to set the config_c.purchased_options raveon type to a default,
	// that is already done upon structure initialization, they are set to valid values.
	
	// 9/15/2017 rmd : Indicate no changes yet that need a config file save or registration
	// sent.
	rid_cs.save_config_file_and_send_registration_to_commserver = (false);
	
	// ----------
	
	if( config_c.port_A_device_index == COMM_DEVICE_LR_RAVEON )
	{
		// 9/7/2017 rmd : Remember this function is called very early on during the startup
		// sequence. All the tasks have been made, but these RAVEON devices, whether on UPORT_A or
		// UPORT_B, have yet to be powered. So power it up. And wait 2 seconds, more than ample, for
		// it to boot and become ready.
		//
		// 8/7/2018 rmd : Updated to wait 3 seconds to take a stab at an intermittent identification
		// process.
		rid_cs.port = UPORT_A;			
	}
	else
	if( config_c.port_B_device_index == COMM_DEVICE_LR_RAVEON )
	{
		// 9/7/2017 rmd : Same comment as for UPORT_A.
		rid_cs.port = UPORT_B;			
	}
	else
	{
		powerdown_and_start_the_timer = (false);
		
		// 9/7/2017 rmd : There are no RAVEON LR radios, so skip right to the exit.
		rid_cs.state = RAVEON_ID_STATE_exit;
		
		// 9/7/2017 rmd : The event parameter is not important, the EXIT state doesn't care, but
		// post one up to the task so we chug along on this process.
		CONTROLLER_INITIATED_post_event( CI_EVENT_process_timer_fired );
	}
	
	// ----------
	
	if( powerdown_and_start_the_timer )
	{
		// 9/7/2017 rmd : Per the notes above, powerup the device and wait 2 seconds.
		power_down_device( rid_cs.port );
		
		// 9/7/2017 rmd : The needed state if there is a device to id.
		rid_cs.state = RAVEON_ID_STATE_waiting_after_powerdown;
		
		xTimerChangePeriod( cics.process_timer, MS_to_TICKS( RAVEON_DELAY_AFTER_POWERDOWN_MS ), portMAX_DELAY );
	}
}

/* ---------------------------------------------------------- */
/*!
 * 
 * @name void RAVEON_id_event_processing(CONTROLLER_INITIATED_TASK_QUEUE_STRUCT *pcitqs)
 * @description: runs the state machine to send command and check responses to ID the type 
 * of LR: M7, M5, or SONIK 
 * @constraints: none @param CONTROLLER_INITIATED_TASK_QUEUE_STRUCT* pcitqs: pointer to the 
 * CI message Q 
 */
extern void RAVEON_id_event_processing( CONTROLLER_INITIATED_TASK_QUEUE_STRUCT *pcitqs )
{
	// 9/7/2017 rmd : Executed solely within the context of the CONTROLLER_INITATED task.
	// There are no MUTEX requirements for the caller.
	
	// ----------

	DATA_HANDLE	ldh;
	
	// ----------
	
	if( (pcitqs->event != CI_EVENT_process_string_found) && (pcitqs->event != CI_EVENT_process_timer_fired) )
	{
		// 9/7/2017 rmd : ERROR.
		Alert_Message( "Raveon ID Process : UNXEXP EVENT" );
		
		// 9/7/2017 rmd : Proceed to exit! We are done.
		rid_cs.state = RAVEON_ID_STATE_exit;
		
		// 9/7/2017 rmd : The event parameter is not important, the EXIT state doesn't care, but
		// post one up to the task so we chug along on this process.
		CONTROLLER_INITIATED_post_event( CI_EVENT_process_timer_fired );
	}
	else
	{
		switch( rid_cs.state )
		{
			case RAVEON_ID_STATE_waiting_after_powerdown:
				
				power_up_device( rid_cs.port );
				
				rid_cs.state = RAVEON_ID_STATE_waiting_after_powerup;
				
				// 9/7/2017 rmd : This time is actually more than just how long it takes the radio to boot.
				// It also gives us the required 500ms quite time before sending the "+++" to the radio.
				xTimerChangePeriod( cics.process_timer, MS_to_TICKS( RAVEON_DELAY_AFTER_POWERUP_MS ), portMAX_DELAY );
				break;
				
			case RAVEON_ID_STATE_waiting_after_powerup:
				
				raveon_id_setup_string_hunt( PGM_MODE_STRING );

				// 9/7/2017 rmd : If it doesn't show within two seconds it ain't coming. On the scope I
				// always see the response within 1 second.
				xTimerChangePeriod( cics.process_timer, MS_to_TICKS(2000), portMAX_DELAY );
			
				rid_cs.state = RAVEON_ID_STATE_waiting_for_response;
				break;
				
			case RAVEON_ID_STATE_waiting_for_response:

				// 9/7/2017 rmd : Restore packet hunting for the port! This is critical to do here, BECAUSE
				// of the funny way this process can cycle through 2 ports if we find an Raveon LR on each
				// PORT A & B (though we do not support that yet). This prepares us to do either another
				// port or EXIT the process.
				RCVD_DATA_enable_hunting_mode( rid_cs.port, PORT_HUNT_FOR_PACKETS, NOT_STRING_HUNTING );
				
				// ----------
				
				if( pcitqs->event == CI_EVENT_process_timer_fired)
				{
					Alert_Message_va( "SONIK or unknown radio found on %s", port_names[ rid_cs.port ] );
					
					//set SONIK as the default LR radio EVEN THOUGH IT MIGHT NOT EXIST OR NOT WORK
					GuiVar_LRRadioType = RAVEON_RADIO_ID_SONIK;
										
					// 9/15/2017 rmd : Now update the config file if needed.
					raveon_id_record_change_in_config_file( RAVEON_RADIO_ID_SONIK );
				}
				else
				{
					// 8/7/2018 rmd : REMINDER - must FREE the memory the string hunt returned the string in.
					
					// ----------
					
					// 9/7/2017 rmd : Whenever a string is found, make sure to STOP the timer, else it could
					// fire producing an extra, unexpected, event.
					xTimerStop( cics.process_timer, portMAX_DELAY );

					// ----------
					
					// 9/7/2017 rmd : Now test the response to be either an M7 or M5 radio.
					if( strstr( (char*)pcitqs->dh.dptr, "RV-M7" ) )
					{
						// 9/7/2017 rmd : A temporary alert line.
						Alert_Message_va( "M7 radio found on %s", port_names[ rid_cs.port ] );
						
						//8/17/2017 BE: M7 defines the radio type to be 1
						GuiVar_LRRadioType = RAVEON_RADIO_ID_M7;

						// 9/15/2017 rmd : Now update the config file if needed.
						raveon_id_record_change_in_config_file( RAVEON_RADIO_ID_M7 );
					}
					else
					if( strstr( (char*)pcitqs->dh.dptr, "FireLine" ) )
					{
						// 9/7/2017 rmd : A temporary alert line.
						Alert_Message_va( "M5 radio found on %s", port_names[ rid_cs.port ] );
						
						GuiVar_LRRadioType = RAVEON_RADIO_ID_M5; 

						// 9/15/2017 rmd : Now update the config file if needed.
						raveon_id_record_change_in_config_file( RAVEON_RADIO_ID_M5 );
					}
					else
					{
						// 9/7/2017 rmd : We just bomb out and leave with our default M7 guivar radio type, set
						// during the function that started this id process.
						Alert_Message( "Raveon response neither M7 or M5" );
					}
					
					// ----------
					
					// 9/7/2017 rmd : Free the memory allocated by the ring_buffer_analysis task.
					mem_free( pcitqs->dh.dptr );
				}

				// ----------
				
				// 9/8/2017 rmd : Test if were are dealing with a radio type that went into 'command mode'
				// as a result of sending it the +++ string. If so we need to exit it from that
				// mode.
				if( (GuiVar_LRRadioType == RAVEON_RADIO_ID_M5) || (GuiVar_LRRadioType == RAVEON_RADIO_ID_M7) )
				{
					// 9/8/2017 rmd : I've decided not to wait for the OK response that happens when sending the
					// exit. I'm just going to send the exit and brute force wait 250ms for the exit to be sent
					// and the radio to exit. Based upon scope measurements, the radio is typically back into
					// normal mode in less than 100ms. But still wait the full 250 for perhaps the units rf
					// section to become fully ready. That's just a theory, but this delay won't hurt anything
					// and has the effect of extending the id process a bit. Remember the comm_mngr and ci tasks
					// are WAITING for this id process to finish, so they are idle.
					ldh.dptr = (UNS_8*)EXIT_CMND_STR;
					ldh.dlen = strlen( EXIT_CMND_STR );

					AddCopyOfBlockToXmitList( rid_cs.port, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );
					
					vTaskDelay( MS_to_TICKS( 250 ) );
				}
				
				// ----------
				
				// 9/7/2017 rmd : So if we were doing for port A, let's go back and try to id a device on
				// port B.
				if( rid_cs.port == UPORT_A )
				{
					rid_cs.state = RAVEON_ID_STATE_test_if_raveon_on_port_B;
				}
				else
				{
					// 9/7/2017 rmd : Proceed to exit! We are done.
					rid_cs.state = RAVEON_ID_STATE_exit;
				}

				// 9/7/2017 rmd : The event parameter is not important, the EXIT state doesn't care, but
				// post one up to the task so we chug along on this process.
				CONTROLLER_INITIATED_post_event( CI_EVENT_process_timer_fired );
				
				break;
				
			case RAVEON_ID_STATE_test_if_raveon_on_port_B:
				// 9/7/2017 rmd : NOTE - the ONLY way to be in this state is to have FIRST gone through the
				// process and ID'd a raveon radio on UPORT_A.
				if( config_c.port_B_device_index == COMM_DEVICE_LR_RAVEON )
				{
					// 9/7/2017 rmd : WELL functionally this is OKAY, we can ID a second radio within this
					// process, but we DO NOT HAVE 2 GUIVARS to support the answer. So alert that there are 2 LR
					// radios on a controller! And proceed.
					Alert_Message( "Unexpectedly TWO LR radios detected" );
					
					// ----------

					rid_cs.port = UPORT_B;
					
					// 9/12/2018 rmd : Well the device should have been off the whole time we were id'ing the
					// device on port A, so we could just skip right to the power up phase. But, just make sure,
					// i don't think this can hurt, and this isn't even supposed to be a supported
					// configuration. So run though the power down first.
					power_down_device( UPORT_B );
					
					// 9/7/2017 rmd : The needed state if there is a device to id.
					rid_cs.state = RAVEON_ID_STATE_waiting_after_powerdown;
					
					xTimerChangePeriod( cics.process_timer, MS_to_TICKS( RAVEON_DELAY_AFTER_POWERDOWN_MS ), portMAX_DELAY );
				}
				else
				{
					// 9/7/2017 rmd : There are no RAVEON LR radios, so skip right to the exit.
					rid_cs.state = RAVEON_ID_STATE_exit;
					
					// 9/7/2017 rmd : The event parameter is not important, the EXIT state doesn't care, but
					// post one up to the task so we chug along on this process.
					CONTROLLER_INITIATED_post_event( CI_EVENT_process_timer_fired );
				}
				break;


			case RAVEON_ID_STATE_exit:

				// 9/15/2017 rmd : If there were changes that require the config file to be saved and for us
				// to re-register with the commserver, do it.
				if( rid_cs.save_config_file_and_send_registration_to_commserver )
				{
					// 9/15/2017 rmd : Setup a request to send registration to the commserver.
					CONTROLLER_INITIATED_update_comm_server_registration_info();

					// 9/15/2017 rmd : This delay of 2 seconds works in conjunction with the device discovery
					// delay of 4 seconds. See note there (in DEVICE DISCOVERY function). No big deal, but
					// results in one less file write.
					FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_CONTROLLER_CONFIGURATION, 2 );
				}

				// ----------
				
				// 3/26/2013 rmd : The COMM_MNGR is waiting for a go ahead signal. We wait until any
				// potential RAVEON devices on PORT_A or PORT_B have been identified by the CI task.
				COMM_MNGR_post_event( COMM_MNGR_EVENT_commence );
				
				// ----------
				
				// 9/6/2017 rmd : Set CI mode to waiting to connect. To get the very first connect started.
				cics.mode =  CI_MODE_waiting_to_connect;
			
				// 6/28/2017 rmd : Try to connect. If not eligible won't. There are quite a few safeguards
				// in place so directly calling the start function with no checking here is safe to do.
				CONTROLLER_INITIATED_post_event( CI_EVENT_start_the_connection_process );

				break;
				
		}  // of the switch statement
		
	}  // of an UNK event.
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*!
 * 
 * @name BOOL_32 RAVEON_is_connected(UNS_32 pport)
 * @description: returns a boolean flag to indicate if the LR is connected. Since 
 * RAVEON/SONIK are connected, after up this always returns true. 
 * @constraints: none @param UNS_32 pport 
 * @return BOOL_32 connection status of lr
 */
extern BOOL_32 RAVEON_is_connected( UNS_32 pport )
{
	// 4/25/2017 rmd : There is no such thing as a 450MHz Raveon radio being 'connected'. There
	// is no carrier present on an on-going bases. It might be true that the CD line transitions
	// when the hub keys up, but that doesn't do us any good when used in this scenario. We
	// always assume the Raveon LR hub is up and ready, and if we transmit he will receive us.
	//
	// 9/7/2017 rmd : If is actually more complicated than I described above. First of all the
	// LR radios that are subjected to the 'connection' process are those on the controller
	// BEHIND the hub, on PORT_A. The LR radio on the hub itself is on PORT_B and all we do to
	// it is to power it up, which happens during the identification process. BUT what makes
	// this exceptionally tricky is that the FIRELINE radio have a port driver power save
	// feature, that cannot be disabled, and it FLOATS the output lines after 30 seconds of no
	// port activity, and to the controller this flips the CD to inactive. So for these
	// controllers we have to be careful not to respond to the DISCONNECT event that is feed to
	// the task.
	return( (true) );
}

/* ---------------------------------------------------------- */
/*!
 * 
 * @name void RAVEON_initialize_the_connection_process(UNS_32 pport)
 * @description: Initializes the connection process by powering up the lr and setting the 
 * connection status to RAVEON_CONNECTION_STATE_waiting_for_connection 
 * @constraints: this function is called in the context of CI task only 
 * @param UNS_32 pport: port on which the lr is connected
 */
extern void RAVEON_initialize_the_connection_process( UNS_32 pport )
{
	// 9/7/2017 rmd : Executed solely within the context of the CONTROLLER_INITATED task. There
	// are no MUTEX requirements for the caller.

	// 9/8/2017 rmd : The connection process is the first thing that takes place AFTER the ID
	// process has completed.
	
	// ----------
	
	if( pport != UPORT_A )
	{
		// 9/6/2017 rmd : The ONLY LR radio that we should run through the connection process would
		// be a radio on PORT A. This is by definition, only the 'central device' establishes a
		// connection.
		Alert_Message( "Connecting LR device on PORT_B?" );
	}

	// 11/11/2016 rmd : For the UDS1100 we use the CD output to determine if connected. So check
	// to make sure that function is not NULL. Otherwise we couldn't call it and the connection
	// process would error out. Over and over again.
	if( port_device_table[ config_c.port_A_device_index ].__is_connected == NULL )
	{
		Alert_Message( "Unexpected NULL is_connected function" );
		
		// 2/15/2017 rmd : I think there is no point to trying to send CI messages. They won't go
		// because we don't have a valid 'is connected' function! So do nothing in the face of this
		// error.
	}
	else
	{
		// 9/8/2017 rmd : WARNING - the port assignment during the connection process is not fully
		// implemented. We make the hard use of PORT_A when testing for a connection. That's okay
		// though because by definition we NEVER run the connection process on a device on
		// UPORT_B.
		rccs.port = pport;

		// 9/8/2017 rmd : As a result of the ID process the radio has been and is now powered up. So
		// we expect that it is. As a sanity check test for that, if not powered then power it!
		if( !GENERIC_GR_card_power_is_on( rccs.port ) )
		{
			Alert_Message( "LR: expected to be powered?" );
			
			power_up_device( rccs.port );
		}
		
		// ----------
		
		// 9/8/2017 rmd : Wait for unit to boot, whether we powered up unit or not. We're not in a
		// particular hurry to 'connect'.
		xTimerChangePeriod( cics.process_timer, MS_to_TICKS( RAVEON_DELAY_AFTER_POWERUP_MS ), portMAX_DELAY );
		
		rccs.state = RAVEON_CONNECTION_STATE_waiting_for_connection;
	}
}

/* ---------------------------------------------------------- */
/*!
 * 
 * @name void RAVEON_connection_processing(UNS_32 pevent)
 * @description: Basically passes the connection status to the Gui, vai the Gui variable 
 * "GuiVar_CommTestStatus" 
 * @constraints: called only in the context of CI task 
 * @param UNS_32 pevent: CI event message that triggered this processing. Primarily, this is 
 * to check that event is NOT:  CI_EVENT_process_string_found or 
 * CI_EVENT_process_timer_fired. 
 */
extern void RAVEON_connection_processing( UNS_32 pevent )
{	
	// 9/7/2017 rmd : Executed solely within the context of the CONTROLLER_INITATED task. There
	// are no MUTEX requirements for the caller.
	
	// ----------
	
	BOOL_32	lerror;
	
	lerror = (false);
	
	// ----------
	if( (pevent != CI_EVENT_process_string_found) && (pevent != CI_EVENT_process_timer_fired) )
	{
		// 10/29/2016 rmd : ERROR.
		Alert_Message( "Connection Process : UNXEXP EVENT" );
		
		lerror = (true);
	}
	else
	{
		switch( rccs.state )
		{
			case RAVEON_CONNECTION_STATE_waiting_for_connection:
				if( port_device_table[ config_c.port_A_device_index ].__is_connected != NULL )
				{
					// 4/25/2017 rmd : It ALWAYS returns connected, but go through the formalities.
					if( port_device_table[ config_c.port_A_device_index ].__is_connected( rccs.port ) )
					{
						strlcpy( GuiVar_CommTestStatus, "LR Radio Connection", sizeof(GuiVar_CommTestStatus) );
					
						Alert_Message( "Raveon LR Connected" );
						
						// ----------
						
						CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities();
					}
				}
				else
				{
					// 11/11/2016 rmd : The sequence will restart and we have a test in the initialization
					// function for the NULL function.
					lerror = (true);
				}
				break;
				
		}  // of the switch statement
		
	}  // of an UNK event.


	if( lerror )
	{
		// 2/3/2017 rmd : Okay well the mode should no longer be 'is connecting'. And it shouldn't
		// be 'ready'. So set it to 'waiting to connect'. Doing so ensures when the connection timer
		// fires and we try to start a connection sequence we really to start it. If the mode were
		// still 'connecting' we would not re-start the sequence.
		cics.mode = CI_MODE_waiting_to_connect;
		
		// 10/29/2016 rmd : Send the connection error event. To start the sequence over.
		CONTROLLER_INITIATED_post_event( CI_EVENT_connection_process_error );
	}
	
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

