/*  file = device_SR_FREEWAVE.c                           rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/9/2015 mpd : Required for atoi
#include	<stdlib.h>

#include	<string.h>

#include	"device_sr_freewave.h"

#include	"device_EN_UDS1100.h"

#include	"device_generic_freewave_card.h"

#include	"e_sr_programming.h"

#include	"configuration_controller.h"

#include	"gpio_setup.h"

#include	"alerts.h"

#include	"d_process.h"

#include	"d_device_exchange.h"

#include	"serial.h"

#include	"speaker.h"

#include	"serport_drvr.h"

#include	"comm_mngr.h"

#include	"rcvd_data.h"

#include	"cs_mem.h"

#include	"epson_rx_8025sa.h"

#include	"controller_initiated.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/12/2017 rmd : NEW notes about FREEWAVE SR radio programming - 
//
//	1.	I learned that for the frequency key, the SLAVES follow the master. So only the
//		master needs to have the key programmed?
//
//	2.	asdasd

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  	  		 FreeWave Hardware Configuration			  */
/* ---------------------------------------------------------- */

// 4/27/2015 mpd : Development progress has been improved a great deal by the use of the FreeWave ribbon cable which 
// provides device feedback in a PuTTY window as the SC3000 controller is communitating in real time. 
// 
// CONNECTION->SERIAL:
//  Speed: 19200
//  Data Bits: 8
//  Stop Bits: 1
//  Parity: None
//  Flow Control: None

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//#define SR_VERBOSE_ALERTS
//#define SR_DEBUG_ANALYSIS  
//#define SR_DEBUG_GROUP 
//#define SR_DEBUG_WRITE
//#define SR_DEBUG_VERIFY		
//#define SR_DEBUG_GET_TEXT
//#define SR_DEBUG_MEM_MALLOC 
//#define SR_DEBUG_HUNT		
//#define SR_DEBUG_PROCESS_LIST
//#define SR_DEBUG_STATE

// 5/20/2015 mpd : Timeouts on the first command to the device have been reported in the field. Add some alert messages 
// to see if field times differ from times seen during product development.
//#define SR_FIELD_DEBUGGING_ALERTS

/* ---------------------------------------------------------- */
/*  					Global Structures   				  */
/* ---------------------------------------------------------- */

// 3/19/2015 mpd : Replaced "SR_SETUP_STRUCT" with a "SR_STATE_STRUCT" and a "SR_PROGRAMMABLE_VALUES_STRUCT". This
// separates global state info from multiple copies of programmable values. 

// 3/23/2015 mpd : New Plan! The PVS structs are roughly 38 bytes and we only need 3 of them. Keep the contents and 
// pointers the same, but use global storage instead of "malloc()". The same cost vs. benefit analysis applies to the
// "sr_state" struct.

// The "SR_STATE_STRUCT" struct contains fields that apply to all the SR_PROGRAMMABLE_VALUES_STRUCTs. 
static SR_STATE_STRUCT state_struct;

SR_STATE_STRUCT *sr_state = &state_struct;

// 3/23/2015 mpd : NOTE: No functions should directly modify any PVS fields. Use the pointers in the "sr_state" struct! 

// 4/8/2015 mpd : The "dynamic_pvs" is the dynamic copy of the programmable values struct. It contains what is read from
// the radio and any user modifications sent from the GUI. This is the programmable values struct used to provide
// command text strings for all WRITE operations except initialization.
static SR_PROGRAMMABLE_VALUES_STRUCT sr_dynamic_pvs;

// 4/8/2015 mpd : The "write_pvs" programmable values struct receives all the programmable values from the "dynamic_pvs"
// immediately before every WRITE. It is used after the WRITE to verify that all the "dynamic_pvs" values read from the
// radio match what was written. Any differences will be noted on the screen as a programming failure with details in
// alert messages.
static SR_PROGRAMMABLE_VALUES_STRUCT sr_write_pvs;

#ifdef SR_FIELD_DEBUGGING_ALERTS
static UNS_32 sr_first_command_ticks;
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  			 String Extraction and Analysis 			  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void sr_analyze_main_menu( SR_STATE_STRUCT *sr_state )
{
#ifdef SR_DEBUG_ANALYSIS
	Alert_Message( "sr_analyze_main_menu" );
#endif

	// 4/16/2015 mpd : Send progress info to the GUI.
	if( sr_state->operation == SR_READ_OPERATION )
	{
		strlcpy( sr_state->info_text, "Reading device settings...", sizeof( sr_state->info_text ) );                
		COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );
	}
	else
	{
		strlcpy( sr_state->info_text, "Saving device settings...", sizeof( sr_state->info_text ) );                
		COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_write_settings_in_progress );
	}

	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_MAIN_MENU_VERSION_STR, SR_MAIN_MENU_VERSION_OFFSET, SR_MAIN_MENU_VERSION_LEN, sr_state->sw_version,    "VERSION" );

	// 5/1/2015 mpd : These fields are not currently needed by SR Programming. They have been tested and are functional.
	//sr_extract_text_from_buffer( sr_state->resp_ptr, SR_MAIN_MENU_VERSION_STR, SR_MAIN_MENU_DATE_OFFSET,    SR_MAIN_MENU_DATE_LEN,    sr_state->sw_date,       "DATE" );

	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_MAIN_MENU_SERIAL_STR,  SR_MAIN_MENU_SERIAL_OFFSET,  SR_MAIN_MENU_SERIAL_LEN,  sr_state->serial_number, "SN" );
	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_MAIN_MENU_MODEL_STR,   SR_MAIN_MENU_MODEL_OFFSET,   SR_MAIN_MENU_MODEL_LEN,   sr_state->model,         "MODEL" );
}

/* ---------------------------------------------------------- */
static void sr_analyze_set_modem_mode( SR_STATE_STRUCT *sr_state )
{
#ifdef SR_DEBUG_ANALYSIS
	Alert_Message( "sr_analyze_set_modem_mode" );
#endif

	// 5/1/2015 mpd : This message is on the screen too briefly to read. Save the effort.
	// 4/16/2015 mpd : Send progress info to the GUI.
	//if( sr_state->operation == SR_READ_OPERATION )
	//{
	//	strlcpy( sr_state->info_text, "Reading radio operation mode...", sizeof( sr_state->info_text ) );       
	//	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );
	//}
	//else
	//{
	//  strlcpy( sr_state->info_text, "Writing/Reading radio operation mode...", sizeof( sr_state->info_text )
	//  ); COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_write_settings_in_progress
	//  );
	//}

	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_MODE_MENU_MODE_STR, SR_MODE_MENU_MODE_OFFSET, SR_MODE_MENU_MODE_LEN, sr_state->active_pvs->modem_mode, "MODE" );

	// 3/19/2015 mpd : This field, along with the SR fileds associated with GROUP, are used to determine if the radio has 
	// been initialized with valid Calsense values. If the mode is not one of our's, assume the radio needs initialization.
	if( ( strncmp( sr_state->active_pvs->modem_mode, SR_MODE_P_TO_M_MASTER,   SR_MODE_MENU_MODE_LEN ) != NULL ) &&
		( strncmp( sr_state->active_pvs->modem_mode, SR_MODE_P_TO_M_SLAVE,    SR_MODE_MENU_MODE_LEN ) != NULL ) &&
		( strncmp( sr_state->active_pvs->modem_mode, SR_MODE_P_TO_M_REPEATER, SR_MODE_MENU_MODE_LEN ) != NULL ) )
	{
		sr_state->mode_is_valid = false;
	}
	else
	{
		sr_state->mode_is_valid = true;
	}

}

/* ---------------------------------------------------------- */
static void sr_analyze_set_baud_rate( SR_STATE_STRUCT *sr_state )
{
#ifdef SR_DEBUG_ANALYSIS
	Alert_Message( "sr_analyze_set_baud_rate" );
#endif

	// 5/1/2015 mpd : This message is on the screen too briefly to read. Save the effort.
	// 4/16/2015 mpd : Send progress info to the GUI.
	//if( sr_state->operation == SR_READ_OPERATION )
	//{
	//	strlcpy( sr_state->info_text, "Reading baud rate...", sizeof( sr_state->info_text ) );                
	//	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );
	//}
	//else
	//{
	//	strlcpy( sr_state->info_text, "Writing/Reading baud rate...", sizeof( sr_state->info_text ) );          
	//	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_write_settings_in_progress );
	//}

	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_BAUD_MENU_RATE_STR, SR_BAUD_MENU_RATE_OFFSET, SR_BAUD_MENU_RATE_LEN, sr_state->active_pvs->baud_rate, "BAUD" );

	// 5/1/2015 mpd : These fields are not currently needed by SR Programming. They have been tested and are functional.
	//dev_extract_text_from_buffer( sr_state->resp_ptr, SR_BAUD_MENU_SETUP_PORT_STR,   SR_BAUD_MENU_SETUP_PORT_OFFSET,   SR_BAUD_MENU_SETUP_PORT_LEN,   sr_state->active_pvs->setup_port,   "SETUP" );
	//dev_extract_text_from_buffer( sr_state->resp_ptr, SR_BAUD_MENU_FLOW_CONTROL_STR, SR_BAUD_MENU_FLOW_CONTROL_OFFSET, SR_BAUD_MENU_FLOW_CONTROL_LEN, sr_state->active_pvs->flow_control, "FLOW" );
}

/* ---------------------------------------------------------- */
static void sr_analyze_radio_parameters( SR_STATE_STRUCT *sr_state )
{
#ifdef SR_DEBUG_ANALYSIS
	Alert_Message( "sr_analyze_radio_parameters" );
#endif

	// 5/1/2015 mpd : This message is on the screen too briefly to read. Save the effort.
	// 4/16/2015 mpd : Send progress info to the GUI.
	//if( sr_state->operation == SR_READ_OPERATION )
	//{
	//	strlcpy( sr_state->info_text, "Reading radio parameters...", sizeof( sr_state->info_text ) );           
	//	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );
	//}
	//else
	//{
	//	strlcpy( sr_state->info_text, "Writing/Reading radio parameters...", sizeof( sr_state->info_text ) );   
	//	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_write_settings_in_progress );
	//}

	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_RADIO_MENU_FREQ_KEY_STR,      SR_RADIO_MENU_FREQ_KEY_OFFSET,      SR_RADIO_MENU_FREQ_KEY_LEN,      sr_state->active_pvs->freq_key,        "FKEY" );
	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_RADIO_MENU_MAX_PACKET_STR,    SR_RADIO_MENU_MAX_PACKET_OFFSET,    SR_RADIO_MENU_MAX_PACKET_LEN,    sr_state->active_pvs->max_packet_size, "MAXP" );
	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_RADIO_MENU_MIN_PACKET_STR,    SR_RADIO_MENU_MIN_PACKET_OFFSET,    SR_RADIO_MENU_MIN_PACKET_LEN,    sr_state->active_pvs->min_packet_size, "MINP" );
	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_RADIO_MENU_DATA_RATE_STR,     SR_RADIO_MENU_DATA_RATE_OFFSET,     SR_RADIO_MENU_DATA_RATE_LEN,     sr_state->active_pvs->rf_data_rate,    "RFRATE" );
	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_RADIO_MENU_XMT_POWER_STR,     SR_RADIO_MENU_XMT_POWER_OFFSET,     SR_RADIO_MENU_XMT_POWER_LEN,     sr_state->active_pvs->rf_xmit_power,   "XPOWER" );
	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_RADIO_MENU_LOWPOWER_MODE_STR, SR_RADIO_MENU_LOWPOWER_MODE_OFFSET, SR_RADIO_MENU_LOWPOWER_MODE_LEN, sr_state->active_pvs->low_power_mode,  "LOWPOW" );
}

/* ---------------------------------------------------------- */
static void sr_analyze_multipoint_parameters( SR_STATE_STRUCT *sr_state )
{
#ifdef SR_DEBUG_ANALYSIS
	Alert_Message( "sr_analyze_multipoint_parameters" );
#endif

	// 5/1/2015 mpd : This message is on the screen too briefly to read. Save the effort.
	// 4/16/2015 mpd : Send progress info to the GUI.
	//if( sr_state->operation == SR_READ_OPERATION )
	//{
	//	strlcpy( sr_state->info_text, "Reading multipoint parameters...", sizeof( sr_state->info_text ) );      
	//	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );
	//}
	//else
	//{
	//	strlcpy( sr_state->info_text, "Writing/Reading multipoint params...", sizeof( sr_state->info_text ) );  
	//	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_write_settings_in_progress );
	//}

	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_MULTIPOINT_MENU_NUM_REPEATERS_STR,        SR_MULTIPOINT_MENU_NUM_REPEATERS_OFFSET,        SR_MULTIPOINT_MENU_NUM_REPEATERS_LEN,        sr_state->active_pvs->number_of_repeators,  "NREP" );
	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_MULTIPOINT_MENU_MASTER_PACKET_REPEAT_STR, SR_MULTIPOINT_MENU_MASTER_PACKET_REPEAT_OFFSET, SR_MULTIPOINT_MENU_MASTER_PACKET_REPEAT_LEN, sr_state->active_pvs->master_packet_repeat, "MPREP" );
	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_MULTIPOINT_MENU_MAX_SLAVE_RETRY_STR,      SR_MULTIPOINT_MENU_MAX_SLAVE_RETRY_OFFSET,      SR_MULTIPOINT_MENU_MAX_SLAVE_RETRY_LEN,      sr_state->active_pvs->max_slave_retry,      "SRETRY" );
	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_MULTIPOINT_MENU_RETRY_ODDS_STR,           SR_MULTIPOINT_MENU_RETRY_ODDS_OFFSET,           SR_MULTIPOINT_MENU_RETRY_ODDS_LEN,           sr_state->active_pvs->retry_odds,           "RODDS" );
	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_MULTIPOINT_MENU_REPEATER_FREQ_STR,        SR_MULTIPOINT_MENU_REPEATER_FREQ_OFFSET,        SR_MULTIPOINT_MENU_REPEATER_FREQ_LEN,        sr_state->active_pvs->repeater_frequency,   "RFREQ" );
	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_MULTIPOINT_MENU_NETWORK_ID_STR,           SR_MULTIPOINT_MENU_NETWORK_ID_OFFSET,           SR_MULTIPOINT_MENU_NETWORK_ID_LEN,           sr_state->active_pvs->network_id,           "NETID" );
	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_MULTIPOINT_MENU_SLAVE_REPEATER_STR,       SR_MULTIPOINT_MENU_SLAVE_REPEATER_OFFSET,       SR_MULTIPOINT_MENU_SLAVE_REPEATER_LEN,       sr_state->active_pvs->slave_and_repeater,   "S_R" );
	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_MULTIPOINT_MENU_DIAGNOSTICS_STR,          SR_MULTIPOINT_MENU_DIAGNOSTICS_OFFSET,          SR_MULTIPOINT_MENU_DIAGNOSTICS_LEN,          sr_state->active_pvs->diagnostics,          "DIAG" );
	dev_extract_text_from_buffer( sr_state->resp_ptr, SR_MULTIPOINT_MENU_SUBNET_ID_STR,            SR_MULTIPOINT_MENU_SUBNET_ID_OFFSET,            SR_MULTIPOINT_MENU_SUBNET_ID_LEN,            sr_state->active_pvs->subnet_id,            "SNID" );

	// 3/12/2015 mpd : Look for "Roaming" or "Disabled". If not there, look for "Rcv=" string.
	if( ( strncmp( sr_state->active_pvs->subnet_id, SR_MULTIPOINT_MENU_SUBNET_ROAMING_STR, SR_MULTIPOINT_MENU_SUBNET_ROAMING_LEN ) ) == NULL )
	{
		//Alert_Message( "SubnetID is ROAMING" );
		strlcpy( sr_state->active_pvs->subnet_rcv_id, "0", sizeof( sr_state->active_pvs->subnet_rcv_id ) );
		strlcpy( sr_state->active_pvs->subnet_xmt_id, "0", sizeof( sr_state->active_pvs->subnet_xmt_id ) );
	}
	else if( ( strncmp( sr_state->active_pvs->subnet_id, SR_MULTIPOINT_MENU_SUBNET_DISABLED_STR, SR_MULTIPOINT_MENU_SUBNET_DISABLED_LEN ) ) == NULL )
	{
		//Alert_Message( "SubnetID is DISABLED" );
		strlcpy( sr_state->active_pvs->subnet_rcv_id, "F", sizeof( sr_state->active_pvs->subnet_rcv_id ) );
		strlcpy( sr_state->active_pvs->subnet_xmt_id, "F", sizeof( sr_state->active_pvs->subnet_xmt_id ) );
	}
	else if( ( strncmp( sr_state->active_pvs->subnet_id, SR_MULTIPOINT_MENU_SUBNET_RCV_ID_STR, SR_MULTIPOINT_MENU_SUBNET_RCV_ID_LEN ) ) == NULL )
	{
		dev_extract_text_from_buffer( sr_state->resp_ptr, SR_MULTIPOINT_MENU_SUBNET_RCV_ID_STR, SR_MULTIPOINT_MENU_SUBNET_RCV_ID_OFFSET, SR_MULTIPOINT_MENU_SUBNET_RCV_ID_LEN, sr_state->active_pvs->subnet_rcv_id, "RCVID" );// Alternate of ROAMING, requires special handling
		dev_extract_text_from_buffer( sr_state->resp_ptr, SR_MULTIPOINT_MENU_SUBNET_XMT_ID_STR, SR_MULTIPOINT_MENU_SUBNET_XMT_ID_OFFSET, SR_MULTIPOINT_MENU_SUBNET_XMT_ID_LEN, sr_state->active_pvs->subnet_xmt_id, "XMTID" );// Alternate of ROAMING, requires special handling
	}
	else
	{
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "ERROR: Unexpected SubnetID format" );
#else
		Alert_Message( "SR1" );
#endif

		strlcpy( sr_state->active_pvs->subnet_rcv_id, "0", sizeof( sr_state->active_pvs->subnet_rcv_id ) );
		strlcpy( sr_state->active_pvs->subnet_xmt_id, "0", sizeof( sr_state->active_pvs->subnet_xmt_id ) );
	}

	// 5/1/2015 mpd : These fields are not currently needed by SR Programming. They have been tested and are functional.
	// 4/13/2015 mpd : The user currently has no access to fields such as "Radio ID" and "Radio Name". They are included
	// here as possible enhancements to the SR Programming GUI.
	//dev_extract_text_from_buffer( sr_state->resp_ptr, SR_MULTIPOINT_MENU_RADIO_ID_STR,   SR_MULTIPOINT_MENU_RADIO_ID_OFFSET,   SR_MULTIPOINT_MENU_RADIO_ID_LEN,   sr_state->active_pvs->radio_id,   "RID" );
	//dev_extract_text_from_buffer( sr_state->resp_ptr, SR_MULTIPOINT_MENU_RADIO_NAME_STR, SR_MULTIPOINT_MENU_RADIO_NAME_OFFSET, SR_MULTIPOINT_MENU_RADIO_NAME_LEN, sr_state->active_pvs->radio_name, "RNAME" );

	// 3/13/2015 mpd : The "Radio Name" string is left-justified and the length varies. Truncate the unwanted characters.
	//dev_strip_crlf_characters( sr_state->active_pvs->radio_name, "RNAME" );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  			  Group, Mode, and Baud Rate Handling   	  */
/* ---------------------------------------------------------- */

// 3/13/2015 mpd : "Group" is a Calsense creation. Each group is a collection of specific network related values for SR
// radio parameters. The group number is visible on the LCD. The internal radio parameters associated with the group are
// not.

// 3/13/2015 mpd : SR radio responses are string values. Stay in the world of strings for comparisons and display. 
// Every field is defined as the display length plus a null-termination.
typedef struct
{
	char group_id[ SR_NETWORK_GROUP_ID_LEN ]; // Controller only. This field does not get sent to the radio. 
	char freq_key[ SR_RADIO_MENU_FREQ_KEY_LEN ];
	char max_packet_size[ SR_RADIO_MENU_MAX_PACKET_LEN ];
	char min_packet_size[ SR_RADIO_MENU_MIN_PACKET_LEN ];
	char network_id[ SR_MULTIPOINT_MENU_NETWORK_ID_LEN ];

} sr_group_properties;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define SR_GROUP_DEFAULT	( 0 )
#define SR_GROUP_LIST_LEN	( 10 )

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

sr_group_properties sr_group_list[ SR_GROUP_LIST_LEN ] =
{
	{ SR_GROUP_ID_1,  SR_FREQ_KEY_5, SR_PACKET_SIZE_8, SR_PACKET_SIZE_9, SR_NETWORK_ID_0100},
	{ SR_GROUP_ID_2,  SR_FREQ_KEY_4, SR_PACKET_SIZE_7, SR_PACKET_SIZE_8, SR_NETWORK_ID_0110},
	{ SR_GROUP_ID_3,  SR_FREQ_KEY_3, SR_PACKET_SIZE_6, SR_PACKET_SIZE_7, SR_NETWORK_ID_0120},
	{ SR_GROUP_ID_4,  SR_FREQ_KEY_2, SR_PACKET_SIZE_5, SR_PACKET_SIZE_6, SR_NETWORK_ID_0130},
	{ SR_GROUP_ID_5,  SR_FREQ_KEY_1, SR_PACKET_SIZE_4, SR_PACKET_SIZE_5, SR_NETWORK_ID_0140},
	{ SR_GROUP_ID_6,  SR_FREQ_KEY_6, SR_PACKET_SIZE_7, SR_PACKET_SIZE_9, SR_NETWORK_ID_0150},
	{ SR_GROUP_ID_7,  SR_FREQ_KEY_7, SR_PACKET_SIZE_6, SR_PACKET_SIZE_8, SR_NETWORK_ID_0160},
	{ SR_GROUP_ID_8,  SR_FREQ_KEY_8, SR_PACKET_SIZE_5, SR_PACKET_SIZE_7, SR_NETWORK_ID_0170},
	{ SR_GROUP_ID_9,  SR_FREQ_KEY_9, SR_PACKET_SIZE_4, SR_PACKET_SIZE_6, SR_NETWORK_ID_0180},
	{ SR_GROUP_ID_10, SR_FREQ_KEY_0, SR_PACKET_SIZE_3, SR_PACKET_SIZE_5, SR_NETWORK_ID_0190}
};


// 3/13/2015 mpd : This function sets a group of SR radio parameters based on the "sr_group_list" array. The string
// values for the group parameters are placed in the pvs structure by this function. It does not write those values to
// the radio.
/* ---------------------------------------------------------- */
static void sr_set_network_group_values( SR_STATE_STRUCT *sr_state )
{
	UNS_32 lgroup_index;

#ifdef SR_DEBUG_GROUP
	Alert_Message( "sr_set_network_group_values" );
#endif

	sr_state->group_is_valid = false;

	// 4/14/2015 mpd : Set the new group based on the "network_group_id" string. This is the latest value set by the user.
	for( lgroup_index = 0; lgroup_index < SR_GROUP_LIST_LEN; ++lgroup_index )
	{
		// 4/14/2015 mpd : Compare just the frequency to identify the group.
		if( ( strncmp( sr_state->network_group_id, sr_group_list[ lgroup_index ].group_id, SR_NETWORK_GROUP_ID_LEN ) ) == NULL )
		{
			sr_state->group_index = lgroup_index;

			// 3/13/2015 mpd : Set all the SR radio parameters associated with this group.
			strlcpy( sr_state->network_group_id,             sr_group_list[ sr_state->group_index ].group_id,        sizeof( sr_state->network_group_id ) );
			strlcpy( sr_state->active_pvs->freq_key,         sr_group_list[ sr_state->group_index ].freq_key,        sizeof( sr_state->active_pvs->freq_key ) );
			strlcpy( sr_state->active_pvs->max_packet_size,  sr_group_list[ sr_state->group_index ].max_packet_size, sizeof( sr_state->active_pvs->max_packet_size ) );
			strlcpy( sr_state->active_pvs->min_packet_size,  sr_group_list[ sr_state->group_index ].min_packet_size, sizeof( sr_state->active_pvs->min_packet_size ) );
			strlcpy( sr_state->active_pvs->network_id,       sr_group_list[ sr_state->group_index ].network_id,      sizeof( sr_state->active_pvs->network_id ) );
			sr_state->group_is_valid = true;
		}
	}

}

/* ---------------------------------------------------------- */
static BOOL_32 sr_analyze_network_group( SR_STATE_STRUCT *sr_state )
{
	UNS_32 lgroup_index;

#ifdef SR_DEBUG_GROUP
	Alert_Message( "sr_analyze_network_group" );
#endif

	// 4/13/2015 mpd : Assume the worst.
	sr_state->group_is_valid = false;

	// 3/13/2015 mpd : Do a top-to-bottom search for a group match. The list is short and the search frequency is low. 
	// A switch or lookup table may be necessary if either assumption changes. 
	for( lgroup_index = 0; lgroup_index < SR_GROUP_LIST_LEN; ++lgroup_index )
	{
		// 3/13/2015 mpd : Compare just the network ID to identify the group.
		if( ( strncmp( sr_state->active_pvs->network_id, sr_group_list[ lgroup_index ].network_id, SR_MULTIPOINT_MENU_NETWORK_ID_LEN ) ) == NULL )
		{
			// 3/13/2015 mpd : Now verify the other fields. 
			if( ( ( strncmp( sr_state->active_pvs->max_packet_size,  sr_group_list[ lgroup_index ].max_packet_size, SR_RADIO_MENU_MAX_PACKET_LEN ) ) == NULL ) &&
				( ( strncmp( sr_state->active_pvs->min_packet_size,  sr_group_list[ lgroup_index ].min_packet_size, SR_RADIO_MENU_MIN_PACKET_LEN ) ) == NULL ) )
			{
				// 3/13/2015 mpd : Enough fields match. Assign the group ID using the current index.
				strlcpy( sr_state->network_group_id, sr_group_list[ lgroup_index ].group_id, sizeof( sr_state->network_group_id ) );
				sr_state->group_is_valid = true;
				sr_state->group_index = lgroup_index;
			}

			// 3/13/2015 mpd : The network ID matched a unique entry. There are no better matches. Exit the loop with or without all
			// fields matching.
			continue;
		}
	}

	// 4/14/2015 mpd : NOTE: "freq_key" verification has been removed from this function to simplify the code. Special 
	// handling of MASTER or SLAVE vs. REPEATER along with the "A" - "E" values being converted by the radio to "10" - "14"
	// made the test too complex for the little value it added. A match of 3 fields should adequately define a group.

	return( sr_state->group_is_valid );
}

// 4/20/2015 mpd : ************************* SR Radio Repeater, Group, and FreqKey Discussion ************************* 
// The "FGR2 User Manual and Reference Guide (LUM0049AA Rev D).pdf" handles the use of "Frequency Key" with repeaters
// and slaves in a section described as: "settings and scenarios that you are not as likely to use in your network".
// This note is intended to fill in the missing details. The critical radio parameter in this discussion is the boolean
// "Repeater Frequency". We correctly set this to "true" only for radios acting as repeaters. A "true" value tells the
// radio to ignore the "Frequency Key" assigned to the master and use the one set in it's own radio parameters
// configuration (see page 59, first paragraph). This implies (and has been proven through experimentation) that a
// "false" value for "Repeater Frequency", tells the device to use the "Frequency Key" assigned to it's master, and
// ignore the one set in it's own radio parameters configuration. This explains how a slave with "Frequency Key = 5"
// is able to communicate with a repeater using "Frequency Key = A". It ignores it's internal "Frequency Key" setting.
// 
// EXAMPLE: A network is configured using the Calsense "Group 1" setting. "Group 1" is the user-visable designation that
// contains 4 hidden radio parameters: "Frequency Key", "Max Packet Size", "Min Packet Size", and "Network ID". Having
// multiple groups allows overlapping multipoint networks to exist in the same geographic area. Setting all radios on
// the network to the same group allows them to communicate. Calsense "Group = 1" sets "Frequency Key = 5" in the radio
// (the other group settings are not important to this discussion). Two additional user-visable parameters link the
// radios together in the desired order: "Subnet ID Rcv" and Subnet ID Xmt". The "Rcv" value identifies which
// transceiver a device listens to. The "Xmt" value identifies on which ID the device transmits, which in turn
// identifies the listeners (see pages 42-44 for Subnet ID diagrams). The master always configures "Subnet Rcv = 0, and
// Xmt = 0". A repeater linked to the master receives using "Rcv = 0" and transmits on a different "Subnet ID" (ie "Xmt
// = 1") that uniquely identifies it's slave(s). Slave(s) linked to that repeater would use "Rcv = 1, Xmt = F" (slave
// "Xmt" is always "F" indicating the end of the network). Multiple repeaters can be linked serially to form long chains
// using "Rcv" and "Xmt" to define the connections. Parallel repeaters (multiple repeaters connected to the same master)
// would create significant problems if they have overlapping geographic coverage. Having them on the same "Frequency
// Key" in the same time slot would cause message collisions. This is where the "Repeater Frequency" parameter overrides
// the group "Frequency Key" value. We set the "Repeater Frequency" parameter to "true" for all repeaters. This forces
// the repeater to use a unique "Frequency Key" equivalent to it's repeater ID (ie "A"). Any slave(s) attached to that
// repeater (with "Repeater Frequency = false") automatically use the "Frequency Key" assigned to their master (the
// repeater in this case). In this example, The slave(s) on "Rcv = 1" would be communication with "Frequency Key = A",
// not the "Frequency Key = 5" as specified by "Group = 1".

// 4/13/2015 mpd : This function is the repeater version of "sr_analyze_network_group()" function. There is little to
// verify, and negligible change of these values being out of range since the user has no ability to directly modify the
//"freqKey", min or max values. 
/* ---------------------------------------------------------- */
static BOOL_32 sr_analyze_repeater_settings( SR_STATE_STRUCT *sr_state )
{

#ifdef SR_DEBUG_GROUP
	Alert_Message( "sr_analyze_repeater_settings" );
#endif

	// 4/13/2015 mpd : Assume the worst.
	sr_state->repeater_is_valid = false;

	// 4/14/2015 mpd : The SR radio requires "A" - "E" as inputs, but "10" - "14" appear on the output line. This is a
	// generic function, so accept either format as valid.
	if( ( strncmp( sr_state->active_pvs->freq_key, SR_FREQ_KEY_A,  SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) ||
		( strncmp( sr_state->active_pvs->freq_key, SR_FREQ_KEY_10, SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) )
	{
		strlcpy( sr_state->repeater_group_id, SR_FREQ_KEY_A, sizeof( sr_state->repeater_group_id ));
		sr_state->repeater_is_valid = true;
	}
	else if( ( strncmp( sr_state->active_pvs->freq_key, SR_FREQ_KEY_B,  SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) ||
			 ( strncmp( sr_state->active_pvs->freq_key, SR_FREQ_KEY_11, SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) )
	{
		strlcpy( sr_state->repeater_group_id, SR_FREQ_KEY_B, sizeof( sr_state->repeater_group_id ));
		sr_state->repeater_is_valid = true;
	}
	else if( ( strncmp( sr_state->active_pvs->freq_key, SR_FREQ_KEY_C,  SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) ||
			 ( strncmp( sr_state->active_pvs->freq_key, SR_FREQ_KEY_12, SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) )
	{
		strlcpy( sr_state->repeater_group_id, SR_FREQ_KEY_C, sizeof( sr_state->repeater_group_id ));
		sr_state->repeater_is_valid = true;
	}
	else if( ( strncmp( sr_state->active_pvs->freq_key, SR_FREQ_KEY_D,  SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) ||
			 ( strncmp( sr_state->active_pvs->freq_key, SR_FREQ_KEY_13, SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) )
	{
		strlcpy( sr_state->repeater_group_id, SR_FREQ_KEY_D, sizeof( sr_state->repeater_group_id ));
		sr_state->repeater_is_valid = true;
	}
	else if( ( strncmp( sr_state->active_pvs->freq_key, SR_FREQ_KEY_E,  SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) ||
			 ( strncmp( sr_state->active_pvs->freq_key, SR_FREQ_KEY_14, SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) )
	{
		strlcpy( sr_state->repeater_group_id, SR_FREQ_KEY_E, sizeof( sr_state->repeater_group_id ));
		sr_state->repeater_is_valid = true;
	}

	// 4/14/2015 mpd : TODO: Is it worth verifying any other repeater specific variables here?

	return( sr_state->repeater_is_valid );
}

/* ---------------------------------------------------------- */
static BOOL_32 sr_verify_freq_key( SR_STATE_STRUCT *sr_state )
{
	BOOL_32 match = false;

#ifdef SR_DEBUG_GROUP
	Alert_Message( "sr_verify_freq_key" );
#endif

	if( strncmp( sr_state->active_pvs->modem_mode, SR_MODE_P_TO_M_REPEATER, SR_MODE_MENU_MODE_LEN ) == NULL )
	{
		// 4/14/2015 mpd : The SR radio requires "A" - "E" as inputs, but "10" - "14" appear on the output line. See if there is
		// a match between input and output.
		if( ( strncmp( sr_state->active_pvs->freq_key,  SR_FREQ_KEY_10, SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) &&
			( strncmp( sr_state->dynamic_pvs->freq_key, SR_FREQ_KEY_A,  SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) )
		{
			match = true;
		}
		else if( ( strncmp( sr_state->active_pvs->freq_key,  SR_FREQ_KEY_11, SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) &&
				 ( strncmp( sr_state->dynamic_pvs->freq_key, SR_FREQ_KEY_B,  SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) )
		{
			match = true;
		}
		else if( ( strncmp( sr_state->active_pvs->freq_key,  SR_FREQ_KEY_12, SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) &&
				 ( strncmp( sr_state->dynamic_pvs->freq_key, SR_FREQ_KEY_C,  SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) )
		{
			match = true;
		}
		else if( ( strncmp( sr_state->active_pvs->freq_key,  SR_FREQ_KEY_13, SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) &&
				 ( strncmp( sr_state->dynamic_pvs->freq_key, SR_FREQ_KEY_D,  SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) )
		{
			match = true;
		}
		else if( ( strncmp( sr_state->active_pvs->freq_key,  SR_FREQ_KEY_14, SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) &&
				 ( strncmp( sr_state->dynamic_pvs->freq_key, SR_FREQ_KEY_E,  SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL ) )
		{
			match = true;
		}
	}
	else
	{
		// 4/14/2015 mpd : The freq_key input command is 1 char wide. The output is 2 due to white space on MASTER and 
		// SLAVE "0" - "9" values. Handle the space in the comparisons.
		if( sr_state->active_pvs->freq_key[0] == ' ' )
		{
			if( strncmp( &sr_state->active_pvs->freq_key[1],  sr_state->dynamic_pvs->freq_key, SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL )
			{
				match = true;
			}
		}
		else
		{
			if( strncmp( sr_state->active_pvs->freq_key,  sr_state->dynamic_pvs->freq_key, SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL )
			{
				match = true;
			}
		}
	}

	return( match );
}

/* ---------------------------------------------------------- */
static void sr_get_baud_rate_command( SR_STATE_STRUCT *sr_state, char *command_buffer  )
{
#ifdef SR_DEBUG_GROUP
	Alert_Message( "sr_get_baud_rate" );
#endif

	// 4/15/2015 mpd : Set the radio baud rate based on "configuration_controller" settings. Use 57600 as the default.
	switch( port_device_table[ COMM_DEVICE_SR_FREEWAVE ].baud_rate )
	{
		case 9600:
			// 4/16/2015 mpd : Copy the 1 character command along with a null terminator into the buffer.
			strlcpy( command_buffer, SR_BAUD_RATE_CMD_9600, sizeof( command_buffer ) );

			// 4/16/2015 mpd : After the WRITE, the "dynamic_pvs" value will be used to determine a successful result. 
			// Update it with the expected value or it will not match.
			strlcpy( sr_state->dynamic_pvs->baud_rate, SR_BAUD_RATE_OUT_9600, sizeof( sr_state->dynamic_pvs->baud_rate ) );
			break;

		case 19200:
			strlcpy( command_buffer, SR_BAUD_RATE_CMD_19200, sizeof( command_buffer ) );
			strlcpy( sr_state->dynamic_pvs->baud_rate, SR_BAUD_RATE_OUT_19200, sizeof( sr_state->dynamic_pvs->baud_rate ) );
			break;

		case 38400:
			strlcpy( command_buffer, SR_BAUD_RATE_CMD_38400, sizeof( command_buffer ) );
			strlcpy( sr_state->dynamic_pvs->baud_rate, SR_BAUD_RATE_OUT_38400, sizeof( sr_state->dynamic_pvs->baud_rate ) );
			break;

		case 76800:
			strlcpy( command_buffer, SR_BAUD_RATE_CMD_57600, sizeof( command_buffer ) );
			strlcpy( sr_state->dynamic_pvs->baud_rate, SR_BAUD_RATE_OUT_57600, sizeof( sr_state->dynamic_pvs->baud_rate ) );
			break;

		case 115200:
			strlcpy( command_buffer, SR_BAUD_RATE_CMD_115200, sizeof( command_buffer ) );
			strlcpy( sr_state->dynamic_pvs->baud_rate, SR_BAUD_RATE_OUT_115200, sizeof( sr_state->dynamic_pvs->baud_rate ) );
			break;

		case 230400:
			strlcpy( command_buffer, SR_BAUD_RATE_CMD_230400, sizeof( command_buffer ) );
			strlcpy( sr_state->dynamic_pvs->baud_rate, SR_BAUD_RATE_OUT_230400, sizeof( sr_state->dynamic_pvs->baud_rate ) );
			break;

		case 57600:
		default:
			strlcpy( command_buffer, SR_BAUD_RATE_CMD_57600, sizeof( command_buffer ) );
			strlcpy( sr_state->dynamic_pvs->baud_rate, SR_BAUD_RATE_OUT_57600, sizeof( sr_state->dynamic_pvs->baud_rate ) );
			break;
	}

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/20/2016 rmd : These 3 values have been changed over the years. Espcially when it came
// to the Model 3000. In the 2000e they were all 5's. And that seemed to work well. The
// consequence of the master_packet_repeat is it directly affects throughput. As such when
// we went to do the 3000 the concern over code distribution time convinced me to lower the
// master packet repeat to a 3. WELL THAT WAS A MISTAKE. In rf dense environments the 3
// repeats is not enough to ensure reliable communication between the point to multi-point
// masters and slaves. After quite a bit experimenting and balancing code distribution time
// I settled on a 6. And also it follows to use a 6 for slave repeats. The RETRY_ODDS has
// always been vague to me but I think I finally understand. And I've decided to not allow
// more slave retries to continue beyond the 6 attempts. If the slave to master packet fails
// after 6 tries we've got bigger problems. And I don't want to choke the network with a
// chatty slave trying to get that last packet through.
#define	MASTER_PACKET_REPEAT_STRING		(" 6")
#define	MAX_SLAVE_RETRY_STRING			(" 6")
#define	RETRY_ODDS_STRING				(" 0")

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void sr_set_repeater_values( SR_STATE_STRUCT *sr_state )
{
	// 4/13/2015 mpd : This function sets a group of SR radio parameters required for REPEATER mode. 

	#ifdef SR_DEBUG_GROUP
		Alert_Message( "sr_set_repeater_values" );
	#endif

	// 4/13/2015 mpd : Repeater groups "A" - "E" directly correspond to the SR radio "freqkey" values. 
	// 5/12/2015 mpd : NOTE: "sr_state->repeater_group_id" has increased in size 1 character to allow "99" to be passed
	// to the GUI for invalid values. The GUI should block us from getting to this point in a WRITE if that was not 
	// caught and changed. Test and fix just in case. We don't want a "9" going to the radio. 
	if( strncmp( sr_state->active_pvs->freq_key, SR_INVALID_VALUE, SR_RADIO_MENU_FREQ_KEY_LEN ) == NULL )
	{
		strlcpy( sr_state->active_pvs->freq_key, SR_FREQ_KEY_A, sizeof( sr_state->active_pvs->freq_key ) );
	}
	else
	{
		strlcpy( sr_state->active_pvs->freq_key, sr_state->repeater_group_id, sizeof( sr_state->active_pvs->freq_key ) );
	}

	// 5/13/2015 mpd : Set RF data rate to 115200 (normal).
	strlcpy( sr_state->active_pvs->rf_data_rate, " 3", sizeof( sr_state->active_pvs->rf_data_rate ) );

	// 4/14/2015 mpd : Slaves use Low Power Mode = " 1". All others use " 0".
	strlcpy( sr_state->active_pvs->low_power_mode, " 0", sizeof( sr_state->active_pvs->low_power_mode ) );

	// 4/14/2015 mpd : Repeaters set this to a "1". Master and Slaves use the Master Hop Table ("0").
	strlcpy( sr_state->active_pvs->repeater_frequency, "1", sizeof( sr_state->active_pvs->repeater_frequency ) );

	// 4/14/2015 mpd : Per 2000e: "act as a slave as well as a repeater when set as a repeater".
	strlcpy( sr_state->active_pvs->slave_and_repeater, "1", sizeof( sr_state->active_pvs->slave_and_repeater ) );

	// 4/14/2015 mpd : Have the master report every 32 slots. Others, " 0".
	strlcpy( sr_state->active_pvs->diagnostics, " 0", sizeof( sr_state->active_pvs->diagnostics ) );

	// 11/23/2015 ajv : Set to "1" to enable diagnostics to work.
	strlcpy( sr_state->active_pvs->number_of_repeators, " 1", sizeof( sr_state->active_pvs->number_of_repeators ) );

	strlcpy( sr_state->active_pvs->master_packet_repeat, MASTER_PACKET_REPEAT_STRING, sizeof( sr_state->active_pvs->master_packet_repeat ) );
	strlcpy( sr_state->active_pvs->max_slave_retry, MAX_SLAVE_RETRY_STRING, sizeof( sr_state->active_pvs->max_slave_retry ) );
	strlcpy( sr_state->active_pvs->retry_odds, RETRY_ODDS_STRING, sizeof( sr_state->active_pvs->retry_odds ) );

	sr_state->repeater_is_valid = true;
}

// 4/13/2015 mpd : This function sets a group of SR radio parameters required for MASTER mode. 
/* ---------------------------------------------------------- */
static void sr_set_master_values( SR_STATE_STRUCT *sr_state )
{
#ifdef SR_DEBUG_GROUP
	Alert_Message( "sr_set_master_values" );
#endif

	// 5/13/2015 mpd : Set RF data rate to 115200 (normal).
	strlcpy( sr_state->active_pvs->rf_data_rate, " 3", sizeof( sr_state->active_pvs->rf_data_rate ) );

	// 4/14/2015 mpd : Slaves use Low Power Mode = " 1". All others use " 0".
	strlcpy( sr_state->active_pvs->low_power_mode, " 0", sizeof( sr_state->active_pvs->low_power_mode ) );

	// 4/14/2015 mpd : Repeaters set this to a "1". Master and Slaves use the Master Hop Table ("0").
	strlcpy( sr_state->active_pvs->repeater_frequency, "0", sizeof( sr_state->active_pvs->repeater_frequency ) );

	// 4/14/2015 mpd : Per 2000e: "act as a slave as well as a repeater when set as a repeater".
	strlcpy( sr_state->active_pvs->slave_and_repeater, "0", sizeof( sr_state->active_pvs->slave_and_repeater ) );

	// 4/14/2015 mpd : Have the master report every 32 slots. Others, "0".
	strlcpy( sr_state->active_pvs->diagnostics, "32", sizeof( sr_state->active_pvs->diagnostics ) );

	// 5/12/2015 mpd : Force RCV and XMT subnet ID values.
	strlcpy( sr_state->active_pvs->subnet_rcv_id, "0", sizeof( sr_state->active_pvs->subnet_rcv_id ) );
	strlcpy( sr_state->active_pvs->subnet_xmt_id, "0", sizeof( sr_state->active_pvs->subnet_xmt_id ) );

	// 11/23/2015 ajv : Set to "1" to enable diagnostics to work.
	strlcpy( sr_state->active_pvs->number_of_repeators, " 1", sizeof( sr_state->active_pvs->number_of_repeators ) );

	strlcpy( sr_state->active_pvs->master_packet_repeat, MASTER_PACKET_REPEAT_STRING, sizeof( sr_state->active_pvs->master_packet_repeat ) );
	strlcpy( sr_state->active_pvs->max_slave_retry, MAX_SLAVE_RETRY_STRING, sizeof( sr_state->active_pvs->max_slave_retry ) );
	strlcpy( sr_state->active_pvs->retry_odds, RETRY_ODDS_STRING, sizeof( sr_state->active_pvs->retry_odds ) );
}

// 4/13/2015 mpd : This function sets a group of SR radio parameters required for SLAVE mode. 
/* ---------------------------------------------------------- */
static void sr_set_slave_values( SR_STATE_STRUCT *sr_state )
{
#ifdef SR_DEBUG_GROUP
	Alert_Message( "sr_set_slave_values" );
#endif

	// 5/13/2015 mpd : Set RF data rate to 115200 (normal).
	strlcpy( sr_state->active_pvs->rf_data_rate, " 3", sizeof( sr_state->active_pvs->rf_data_rate ) );

	// 4/14/2015 mpd : Slaves use Low Power Mode = " 1". All others use " 0".
	strlcpy( sr_state->active_pvs->low_power_mode, " 1", sizeof( sr_state->active_pvs->low_power_mode ) );

	// 4/14/2015 mpd : Repeaters set this to a "1". Master and Slaves use the Master Hop Table ("0").
	strlcpy( sr_state->active_pvs->repeater_frequency, "0", sizeof( sr_state->active_pvs->repeater_frequency ) );

	// 4/14/2015 mpd : Per 2000e: "act as a slave as well as a repeater when set as a repeater".
	strlcpy( sr_state->active_pvs->slave_and_repeater, "0", sizeof( sr_state->active_pvs->slave_and_repeater ) );

	// 4/14/2015 mpd : Have the master report every 32 slots. Others, " 0".
	strlcpy( sr_state->active_pvs->diagnostics, " 0", sizeof( sr_state->active_pvs->diagnostics ) );

	// 5/12/2015 mpd : Force XMT subnet ID value.
	strlcpy( sr_state->active_pvs->subnet_xmt_id, "F", sizeof( sr_state->active_pvs->subnet_xmt_id ) );

	// 11/23/2015 ajv : Set to "1" to enable diagnostics to work.
	strlcpy( sr_state->active_pvs->number_of_repeators, " 1", sizeof( sr_state->active_pvs->number_of_repeators ) );

	strlcpy( sr_state->active_pvs->master_packet_repeat, MASTER_PACKET_REPEAT_STRING, sizeof( sr_state->active_pvs->master_packet_repeat ) );
	strlcpy( sr_state->active_pvs->max_slave_retry, MAX_SLAVE_RETRY_STRING, sizeof( sr_state->active_pvs->max_slave_retry ) );
	strlcpy( sr_state->active_pvs->retry_odds, RETRY_ODDS_STRING, sizeof( sr_state->active_pvs->retry_odds ) );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  			  Functions That Span All Menu Items		  */
/* ---------------------------------------------------------- */

// 3/23/2015 mpd : This function is called at the end of READ and WRITE cycles. All SR radio values have been read into
// a "SR_PROGRAMMABLE_VALUES_STRUCT" and specific fields have been anyalzed for consistency. This function gives the
// final stamp of approval.
/* ---------------------------------------------------------- */
static void sr_final_radio_analysis( SR_STATE_STRUCT *sr_state )
{
#ifdef SR_DEBUG_ANALYSIS
	Alert_Message( "sr_final_radio_analysis" );
#endif

	// 4/16/2015 mpd : Send progress info to the GUI.
	strlcpy( sr_state->info_text, "Performing final analysis...", sizeof( sr_state->info_text ) );                
	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );

	// 4/13/2015 mpd : Assume the best.
	sr_state->radio_settings_are_valid = true;

	// 4/13/2015 mpd : Mode, repeater, and group settings are checked here to verify consistent Calsense settings. This 
	// is a sanity check, not a complete check of every parameter.
	if( sr_state->mode_is_valid )
	{
		if( !sr_analyze_network_group( sr_state ) )
		{
			// 5/12/2015 mpd : Invalid group setting. Set the visable user field to an invalid value.
			strlcpy( sr_state->network_group_id, SR_INVALID_INDEX, sizeof( sr_state->network_group_id ) );

#ifdef SR_VERBOSE_ALERTS
			Alert_Message( "Invalid SR Group Settings." );
#else
			Alert_Message( "SR2" );
#endif

			sr_state->radio_settings_are_valid = false;
		}

		// 4/13/2015 mpd : Analyze repeater values if we are in repeater mode.
		if( strncmp( sr_state->active_pvs->modem_mode, SR_MODE_P_TO_M_REPEATER, SR_MODE_MENU_MODE_LEN ) == NULL )
		{
			if( !sr_analyze_repeater_settings( sr_state ) )
			{
				// 5/12/2015 mpd : Invalid repeater setting. Set the visable user field to an invalid value.
				strlcpy( sr_state->repeater_group_id, SR_INVALID_VALUE, sizeof( sr_state->repeater_group_id ) );

#ifdef SR_VERBOSE_ALERTS
				Alert_Message( "Invalid SR Repeater Settings." );
#else
				Alert_Message( "SR3" );
#endif

				sr_state->radio_settings_are_valid = false;
			}
		}
	}
	else
	{
		// 5/12/2015 mpd : Invalid MODE. This may be a new radio. Set all user visable user fields to invalid values.
		sr_state->radio_settings_are_valid = false;

		strlcpy( sr_state->active_pvs->modem_mode,    SR_INVALID_VALUE, sizeof( sr_state->active_pvs->modem_mode ) );
		strlcpy( sr_state->network_group_id,          SR_INVALID_INDEX, sizeof( sr_state->network_group_id ) );
		strlcpy( sr_state->repeater_group_id,         SR_INVALID_VALUE, sizeof( sr_state->repeater_group_id ) );
		strlcpy( sr_state->active_pvs->subnet_rcv_id, SR_INVALID_VALUE, sizeof( sr_state->active_pvs->subnet_rcv_id ) );
		strlcpy( sr_state->active_pvs->subnet_xmt_id, SR_INVALID_VALUE, sizeof( sr_state->active_pvs->subnet_xmt_id ) );
		strlcpy( sr_state->active_pvs->rf_xmit_power, SR_INVALID_INDEX, sizeof( sr_state->active_pvs->rf_xmit_power ) );
	}

	// 4/13/2015 mpd : NOTE: In repeater mode, the "sr_state->group_is_valid" flag is left in the default "false" state 
	// and ignored. In other modes the "sr_state->repeater_is_valid" flag is left in the default "false" state and ignored. 

}

/* ---------------------------------------------------------- */
static void sr_copy_active_values_to_write( SR_STATE_STRUCT *sr_state )
{
#ifdef SR_DEBUG_WRITE
	Alert_Message( "sr_copy_active_values_to_write" );
#endif

	// 4/14/2015 mpd : Before copying values from the "active_pvs" to the "write_pvs", all programming details that are 
	// hidden from the user need to be updated based on current user selections. These include group settings, and specific
	// fields that apply to MASTER, SLAVE, and REPEATER.
	sr_set_network_group_values( sr_state );

	if( strncmp( sr_state->active_pvs->modem_mode, SR_MODE_P_TO_M_REPEATER, SR_MODE_MENU_MODE_LEN ) == NULL )
	{
		sr_set_repeater_values( sr_state );
	}
	else if( strncmp( sr_state->active_pvs->modem_mode, SR_MODE_P_TO_M_MASTER, SR_MODE_MENU_MODE_LEN ) == NULL )
	{
		sr_set_master_values( sr_state );
	}
	else // Slave
	{
		sr_set_slave_values( sr_state );
	}

	// MODEM MODE MENU
	strlcpy( sr_state->write_pvs->modem_mode,           sr_state->active_pvs->modem_mode,            sizeof( sr_state->write_pvs->modem_mode ) );

	// BAUD RATE MENU                                                                                                                                 
	strlcpy( sr_state->write_pvs->baud_rate,            sr_state->active_pvs->baud_rate,             sizeof( sr_state->write_pvs->baud_rate ) );

	// 5/1/2015 mpd : These fields are not currently needed by SR Programming. They have been tested and are functional.
	//strlcpy( sr_state->write_pvs->setup_port,			sr_state->active_pvs->setup_port,            sizeof( sr_state->write_pvs->setup_port ) );
	//strlcpy( sr_state->write_pvs->flow_control,			sr_state->active_pvs->flow_control,          sizeof( sr_state->write_pvs->flow_control ) );

	// RADIO PARAMETERS MENU                                                                                                                          
	strlcpy( sr_state->write_pvs->freq_key,             sr_state->active_pvs->freq_key,              sizeof( sr_state->write_pvs->freq_key ) );
	strlcpy( sr_state->write_pvs->max_packet_size,      sr_state->active_pvs->max_packet_size,       sizeof( sr_state->write_pvs->max_packet_size ) );
	strlcpy( sr_state->write_pvs->min_packet_size,      sr_state->active_pvs->min_packet_size,       sizeof( sr_state->write_pvs->min_packet_size ) );
	strlcpy( sr_state->write_pvs->rf_xmit_power,        sr_state->active_pvs->rf_xmit_power,         sizeof( sr_state->write_pvs->rf_xmit_power ) );
	strlcpy( sr_state->write_pvs->rf_data_rate,         sr_state->active_pvs->rf_data_rate,          sizeof( sr_state->write_pvs->rf_data_rate ) );
	strlcpy( sr_state->write_pvs->low_power_mode,       sr_state->active_pvs->low_power_mode,        sizeof( sr_state->write_pvs->low_power_mode ) );

	// MULTIPOINT PARAMETERS MENU                                                                                                                     
	strlcpy( sr_state->write_pvs->number_of_repeators,  sr_state->active_pvs->number_of_repeators,   sizeof( sr_state->write_pvs->number_of_repeators ) );
	strlcpy( sr_state->write_pvs->master_packet_repeat, sr_state->active_pvs->master_packet_repeat,  sizeof( sr_state->write_pvs->master_packet_repeat ) );
	strlcpy( sr_state->write_pvs->max_slave_retry,      sr_state->active_pvs->max_slave_retry,       sizeof( sr_state->write_pvs->max_slave_retry ) );
	strlcpy( sr_state->write_pvs->retry_odds,           sr_state->active_pvs->retry_odds,            sizeof( sr_state->write_pvs->retry_odds ) );
	strlcpy( sr_state->write_pvs->repeater_frequency,   sr_state->active_pvs->repeater_frequency,    sizeof( sr_state->write_pvs->repeater_frequency ) );
	strlcpy( sr_state->write_pvs->network_id,           sr_state->active_pvs->network_id,            sizeof( sr_state->write_pvs->network_id ) );
	strlcpy( sr_state->write_pvs->slave_and_repeater,   sr_state->active_pvs->slave_and_repeater,    sizeof( sr_state->write_pvs->slave_and_repeater ) );
	strlcpy( sr_state->write_pvs->diagnostics,          sr_state->active_pvs->diagnostics,           sizeof( sr_state->write_pvs->diagnostics ) );
	strlcpy( sr_state->write_pvs->subnet_id,            sr_state->active_pvs->subnet_id,             sizeof( sr_state->write_pvs->subnet_id ) );		// This is read-only, but needs to be with Rcv and Xmt for decisions.
	strlcpy( sr_state->write_pvs->subnet_rcv_id,        sr_state->active_pvs->subnet_rcv_id,         sizeof( sr_state->write_pvs->subnet_rcv_id ) );	// Alternate of ROAMING and DISABLED, requires special handling
	strlcpy( sr_state->write_pvs->subnet_xmt_id,        sr_state->active_pvs->subnet_xmt_id,         sizeof( sr_state->write_pvs->subnet_xmt_id ) );	// Alternate of ROAMING and DISABLED, requires special handling

	// 5/1/2015 mpd : These fields are not currently needed by SR Programming. They have been tested and are functional.
	//strlcpy( sr_state->write_pvs->radio_id,				sr_state->active_pvs->radio_id,              sizeof( sr_state->write_pvs->radio_id ) );
	//strlcpy( sr_state->write_pvs->radio_name,			sr_state->active_pvs->radio_name,            sizeof( sr_state->write_pvs->radio_name ) );

}

/* ---------------------------------------------------------- */
static void sr_verify_dynamic_values_match_write_values( SR_STATE_STRUCT *sr_state )
{
#ifdef SR_DEBUG_WRITE
	Alert_Message( "sr_verify_dynamic_values_match_write_values" );
#endif

	// 4/14/2015 mpd : The "write_pvs" is the "active_pvs" during the WRITE and the subsequent READ operations on each 
	// menu. That means it will have the post-programming values. Contrary to its name, the "dynamic_pvs" is the
	// pre-programming snap-shot. TBD: Better names?

	// 4/16/2015 mpd : Send progress info to the GUI.
	strlcpy( sr_state->info_text, "Performing final verification...", sizeof( sr_state->info_text ) );                
	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_write_settings_in_progress );

	// 4/13/2015 mpd : Assume the best.
	sr_state->programming_successful = true;

	// MODEM MODE MENU
	if( strncmp( sr_state->write_pvs->modem_mode, sr_state->dynamic_pvs->modem_mode, SR_MODE_MENU_MODE_LEN ) != NULL )
	{
		sr_state->programming_successful = false;
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Write Failed: Modem Mode mismatch" );
#else
		Alert_Message( "SR4" );
#endif
	}

	// BAUD RATE MENU
	if( strncmp( sr_state->write_pvs->baud_rate, sr_state->dynamic_pvs->baud_rate, SR_BAUD_MENU_RATE_LEN ) != NULL )
	//if( !sr_verify_baud_rate( sr_state ) )
	{
		sr_state->programming_successful = false;
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Write Failed: Baud Rate mismatch" );
#else
		Alert_Message( "SR5" );
#endif
	}
	// 5/1/2015 mpd : These fields are not currently needed by SR Programming. They have been tested and are functional.
	//if( strncmp( sr_state->write_pvs->setup_port, sr_state->dynamic_pvs->setup_port, SR_BAUD_MENU_SETUP_PORT_LEN ) != NULL ) 
	//{
	//	sr_state->programming_successful = false;
	//	#ifdef SR_VERBOSE_ALERTS
	//		Alert_Message( "Write Failed: Setup Port mismatch" );
	//	#else
	//		Alert_Message( "SR6" );
	//	#endif
	//}
	//if( strncmp( sr_state->write_pvs->flow_control, sr_state->dynamic_pvs->flow_control, SR_BAUD_MENU_FLOW_CONTROL_LEN ) != NULL ) 
	//{
	//	sr_state->programming_successful = false;
	//	#ifdef SR_VERBOSE_ALERTS
	//		Alert_Message( "Write Failed: Flow Control mismatch" );
	//	#else
	//		Alert_Message( "SR7" );
	//	#endif
	//}

	// 4/13/2015 mpd : NOTE: The repeater/group fields are not tested here since they are logical groupings the SR radio
	// knows nothing about.

	// RADIO PARAMETERS MENU
	if( !sr_verify_freq_key( sr_state ) )
	{
		sr_state->programming_successful = false;
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Write Failed: Freq Key mismatch" );
#else
		Alert_Message( "SR8" );
#endif
	}
	if( strncmp( sr_state->write_pvs->max_packet_size, sr_state->dynamic_pvs->max_packet_size, SR_RADIO_MENU_MAX_PACKET_LEN ) != NULL )
	{
		sr_state->programming_successful = false;
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Write Failed: Max Packet Size mismatch" );
#else
		Alert_Message( "SR9" );
#endif
	}

	if( strncmp( sr_state->write_pvs->min_packet_size, sr_state->dynamic_pvs->min_packet_size, SR_RADIO_MENU_MIN_PACKET_LEN ) != NULL )
	{
		sr_state->programming_successful = false;
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Write Failed: Min Packet Size mismatch" );
#else
		Alert_Message( "SR10" );
#endif
	}

	if( strncmp( sr_state->write_pvs->rf_xmit_power, sr_state->dynamic_pvs->rf_xmit_power, SR_RADIO_MENU_XMT_POWER_V_LEN ) != NULL )
	{
		sr_state->programming_successful = false;
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Write Failed: RF Xmt Power mismatch" );
#else
		Alert_Message( "SR11" );
#endif
	}

	if( strncmp( sr_state->write_pvs->rf_data_rate, sr_state->dynamic_pvs->rf_data_rate, SR_RADIO_MENU_DATA_RATE_LEN ) != NULL )
	{
		sr_state->programming_successful = false;
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Write Failed: RF Data Rate mismatch" );
#else
		Alert_Message( "SR12" );
#endif
	}

	if( strncmp( sr_state->write_pvs->low_power_mode, sr_state->dynamic_pvs->low_power_mode, SR_RADIO_MENU_LOWPOWER_MODE_LEN ) != NULL )
	{
		sr_state->programming_successful = false;
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Write Failed: Low Power Mode mismatch" );
#else
		Alert_Message( "SR13" );
#endif
	}

	// MULTIPOINT PARAMETERS MENU                                              
	if( strncmp( sr_state->write_pvs->number_of_repeators, sr_state->dynamic_pvs->number_of_repeators, SR_MULTIPOINT_MENU_NUM_REPEATERS_LEN ) != NULL )
	{
		sr_state->programming_successful = false;
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Write Failed: Number of Repeaters mismatch" );
#else
		Alert_Message( "SR14" );
#endif
	}

	if( strncmp( sr_state->write_pvs->master_packet_repeat, sr_state->dynamic_pvs->master_packet_repeat, SR_MULTIPOINT_MENU_MASTER_PACKET_REPEAT_LEN ) != NULL )
	{
		sr_state->programming_successful = false;
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Write Failed: Master Packet Repeat mismatch" );
#else
		Alert_Message( "SR15" );
#endif
	}

	if( strncmp( sr_state->write_pvs->max_slave_retry, sr_state->dynamic_pvs->max_slave_retry, SR_MULTIPOINT_MENU_MAX_SLAVE_RETRY_LEN ) != NULL )
	{
		sr_state->programming_successful = false;
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Write Failed: Max Slave Retry mismatch" );
#else
		Alert_Message( "SR16" );
#endif
	}

	if( strncmp( sr_state->write_pvs->retry_odds, sr_state->dynamic_pvs->retry_odds, SR_MULTIPOINT_MENU_RETRY_ODDS_LEN ) != NULL )
	{
		sr_state->programming_successful = false;
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Write Failed: Retry Odds mismatch" );
#else
		Alert_Message( "SR17" );
#endif
	}

	if( strncmp( sr_state->write_pvs->repeater_frequency, sr_state->dynamic_pvs->repeater_frequency, SR_MULTIPOINT_MENU_REPEATER_FREQ_LEN ) != NULL )
	{
		sr_state->programming_successful = false;
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Write Failed: Repaeter Frequency mismatch" );
#else
		Alert_Message( "SR18" );
#endif
	}

	if( strncmp( sr_state->write_pvs->network_id, sr_state->dynamic_pvs->network_id, SR_MULTIPOINT_MENU_NETWORK_ID_LEN ) != NULL )
	{
		sr_state->programming_successful = false;
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Write Failed: Network ID mismatch" );
#else
		Alert_Message( "SR19" );
#endif
	}

	if( strncmp( sr_state->write_pvs->slave_and_repeater, sr_state->dynamic_pvs->slave_and_repeater, SR_MULTIPOINT_MENU_SLAVE_REPEATER_LEN ) != NULL )
	{
		sr_state->programming_successful = false;
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Write Failed: Slave and Repeater mismatch" );
#else
		Alert_Message( "SR20" );
#endif
	}

	if( strncmp( sr_state->write_pvs->diagnostics, sr_state->dynamic_pvs->diagnostics, SR_MULTIPOINT_MENU_DIAGNOSTICS_LEN )  != NULL )
	{
		sr_state->programming_successful = false;
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Write Failed: Diagnostics mismatch" );
#else
		Alert_Message( "SR21" );
#endif
	}

	// 4/14/2015 mpd : The SubnetID string is complex due to "Roaming", "Disabled", and "Rcv=x Xmit=y" possibilities. We
	// really don't care about it as long as the "subnet_rcv_id" and "subnet_xmt_id" fields it represents match. Ignore this
	// field, and verify the meaningful fields. 
	//if( strncmp( sr_state->write_pvs->subnet_id, sr_state->dynamic_pvs->subnet_id, SR_MULTIPOINT_MENU_SUBNET_ID_LEN ) != NULL ) 
	//{
	//	sr_state->programming_successful = false;
	//	Alert_Message( "Write Failed: Subnet ID mismatch" );
	//}

	if( strncmp( sr_state->write_pvs->subnet_rcv_id, sr_state->dynamic_pvs->subnet_rcv_id, SR_MULTIPOINT_MENU_SUBNET_RCV_ID_V_LEN ) != NULL )
	{
		sr_state->programming_successful = false;
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Write Failed: Subnet Rcv ID mismatch" );
#else
		Alert_Message( "SR22" );
#endif
	}

	if( strncmp( sr_state->write_pvs->subnet_xmt_id, sr_state->dynamic_pvs->subnet_xmt_id, SR_MULTIPOINT_MENU_SUBNET_XMT_ID_V_LEN ) != NULL )
	{
		sr_state->programming_successful = false;
#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Write Failed: Subnet Xmt ID mismatch" );
#else
		Alert_Message( "SR23" );
#endif
	}

	// 5/1/2015 mpd : These fields are not currently needed by SR Programming. They have been tested and are functional.
	// 4/13/2015 mpd : TBD: The user currently has no access to fields such as "Radio ID" and "Radio Name". They are 
	// included here as possible enhancements to the SR Programming GUI. 
	//if( strncmp( sr_state->write_pvs->radio_id, sr_state->dynamic_pvs->radio_id, SR_MULTIPOINT_MENU_RADIO_ID_LEN ) != NULL ) 
	//{
	//	sr_state->programming_successful = false;
	//	#ifdef SR_VERBOSE_ALERTS
	//		Alert_Message( "Write Failed: Radio ID mismatch" );
	//	#else
	//		Alert_Message( "SR24" );
	//	#endif
	//}

	//if( strncmp( sr_state->write_pvs->radio_name, sr_state->dynamic_pvs->radio_name, SR_MULTIPOINT_MENU_RADIO_NAME_LEN ) != NULL ) 
	//{
	//	sr_state->programming_successful = false;
	//	#ifdef SR_VERBOSE_ALERTS
	//		Alert_Message( "Write Failed: Radio Name mismatch" );
	//	#else
	//		Alert_Message( "SR25" );
	//	#endif
	//}

}

/* ---------------------------------------------------------- */
static void sr_final_radio_verification( SR_STATE_STRUCT *sr_state )
{
#ifdef SR_DEBUG_VERIFY
	Alert_Message( "sr_final_radio_verification" );
#endif

	// 4/13/2015 mpd : Perform final analysis as we do at the end of a READ operation.
	sr_final_radio_analysis( sr_state );

	// 4/13/2015 mpd : Verify every field in the WRITE struct made it into the radio programming.
	sr_verify_dynamic_values_match_write_values( sr_state );

}

// 4/8/2015 mpd : This function returns a pointer to an ASCII command string that will be sent to the SR radio. Some
// commands are single ASCII characters, others are strings terminated with CR and LF. Command strings are selected
// based on the active programmable values structure. This provided a transparent switch between dynamic radio/use
// command strings, and initialization command strings.
/* ---------------------------------------------------------- */
static char *get_command_text( SR_STATE_STRUCT *sr_state, const UNS_32 command )
{
	char *command_ptr = NULL;
	BOOL_32 send_just_cr_lf = false;

	// 4/8/2015 mpd : Create a local space to store and append to command strings. 
	char command_buffer[ ( SR_LONGEST_COMMAND_LEN + SR_CRLF_STR_LEN ) ];

#ifdef SR_DEBUG_GET_TEXT
	Alert_Message_va( "get_command_text: %d", command );
#endif

	memset( command_buffer, 0x00, sizeof( command_buffer ) );

	switch( command )
	{
		// 3/17/2015 mpd : These are single-character commands with no CR or LF attached. The defines are the ASCII 
		// values of the required character.
		case  SR_MENU_CMD_STR_0:
		case  SR_MENU_CMD_STR_1:
		case  SR_MENU_CMD_STR_2:
		case  SR_MENU_CMD_STR_3:
		case  SR_MENU_CMD_STR_4:
		case  SR_MENU_CMD_STR_5:
		case  SR_MENU_CMD_STR_6:
		case  SR_MENU_CMD_STR_7:
		case  SR_MENU_CMD_STR_8:
		case  SR_MENU_CMD_STR_9:
		case  SR_MENU_CMD_STR_A:
		case  SR_MENU_CMD_STR_B:
		case  SR_MENU_CMD_STR_C:
		case  SR_MENU_CMD_STR_D:
		case  SR_MENU_CMD_STR_E:
		case  SR_MENU_CMD_STR_F:
		case  SR_MENU_CMD_STR_G:
		case  SR_MENU_CMD_STR_ESC:
			memset( command_buffer, command, 1 );
			break;

		case  SR_TEXT_CMD_NONE:
			memset( command_buffer, SR_EMPTY_STR, SR_EMPTY_STR_LEN );
			break;

		case  SR_TEXT_CMD_STR_MODE:
			strlcpy( command_buffer, (char*)sr_state->active_pvs->modem_mode, sizeof( command_buffer ) );
			break;

		case  SR_TEXT_CMD_STR_BAUD_RATE:
			sr_get_baud_rate_command( sr_state, command_buffer  );
			break;

			// 5/1/2015 mpd : These fields are not currently needed by SR Programming. They have been tested and are functional.
			//case  SR_TEXT_CMD_STR_SETUP_PORT:
			//	strlcpy( command_buffer, (char*)sr_state->active_pvs->setup_port, sizeof( command_buffer ) );
			//	break;

			//case  SR_TEXT_CMD_STR_FLOW_CONTROL:
			//	strlcpy( command_buffer, (char*)sr_state->active_pvs->flow_control, sizeof( command_buffer ) );
			//	break;

		case  SR_TEXT_CMD_STR_FREQ_KEY:
			strlcpy( command_buffer, sr_state->active_pvs->freq_key, sizeof( command_buffer ) );
			break;

		case  SR_TEXT_CMD_STR_MAX_PACKET:
			strlcpy( command_buffer, sr_state->active_pvs->max_packet_size, sizeof( command_buffer ) );
			break;

		case  SR_TEXT_CMD_STR_MIN_PACKET:
			strlcpy( command_buffer, sr_state->active_pvs->min_packet_size, sizeof( command_buffer ) );
			break;

			// 5/13/2015 mpd : This value should always be "1". Force it. This late-in-development shortcut skips creating
			// a PVS field, extraction from READ responses, and verification after the WRITE. That can be added when time
			// allows for full regression testing.
		case  SR_TEXT_CMD_STR_XMIT_RATE:
			strlcpy( command_buffer, "1", sizeof( command_buffer ) );
			break;

		case  SR_TEXT_CMD_STR_DATA_RATE:
			strlcpy( command_buffer, sr_state->active_pvs->rf_data_rate, sizeof( command_buffer ) );
			break;

		case  SR_TEXT_CMD_STR_XMT_POWER:
			strlcpy( command_buffer, (char*)sr_state->active_pvs->rf_xmit_power, sizeof( command_buffer ) );
			break;

		case  SR_TEXT_CMD_STR_LOWPOWER_MODE:
			strlcpy( command_buffer, sr_state->active_pvs->low_power_mode, sizeof( command_buffer ) );
			break;

		case  SR_TEXT_CMD_STR_NUM_REPEATERS:
			strlcpy( command_buffer, (char*)sr_state->active_pvs->number_of_repeators, sizeof( command_buffer ) );
			break;

		case  SR_TEXT_CMD_STR_MASTER_PACKET_REPEAT:
			strlcpy( command_buffer, (char*)sr_state->active_pvs->master_packet_repeat, sizeof( command_buffer ) );
			break;

		case  SR_TEXT_CMD_STR_MAX_SLAVE_RETRY:
			strlcpy( command_buffer, (char*)sr_state->active_pvs->max_slave_retry, sizeof( command_buffer ) );
			break;

		case  SR_TEXT_CMD_STR_RETRY_ODDS:
			strlcpy( command_buffer, (char*)sr_state->active_pvs->retry_odds, sizeof( command_buffer ) );
			break;

			// 5/13/2015 mpd : This value should always be "0". Force it. This late-in-development shortcut skips creating
			// a PVS field, extraction from READ responses, and verification after the WRITE. That can be added when time
			// allows for full regression testing.
		case  SR_TEXT_CMD_STR_DTR_CONNECT:
			strlcpy( command_buffer, "0", sizeof( command_buffer ) );
			break;

		case  SR_TEXT_CMD_STR_REPEATER_FREQ:
			strlcpy( command_buffer, sr_state->active_pvs->repeater_frequency, sizeof( command_buffer ) );
			break;

		case  SR_TEXT_CMD_STR_NETWORK_ID:
			strlcpy( command_buffer, sr_state->active_pvs->network_id, sizeof( command_buffer ) );
			break;

		case  SR_TEXT_CMD_STR_SLAVE_REPEATER:
			strlcpy( command_buffer, sr_state->active_pvs->slave_and_repeater, sizeof( command_buffer ) );
			break;

		case  SR_TEXT_CMD_STR_DIAGNOSTICS:
			strlcpy( command_buffer, sr_state->active_pvs->diagnostics, sizeof( command_buffer ) );
			break;

		case  SR_TEXT_CMD_STR_SUBNET_ID:
			// 3/18/2015 mpd : This field is dynamic based on SR, MODE, and GROUP. 
			strlcpy( command_buffer, (char*)sr_state->active_pvs->subnet_id, sizeof( command_buffer ) );
			break;

		case  SR_TEXT_CMD_STR_SUBNET_RCV_ID:
			// 3/18/2015 mpd : This field is dynamic based on SR, MODE, and GROUP. 
			strlcpy( command_buffer, (char*)sr_state->active_pvs->subnet_rcv_id, sizeof( command_buffer ) );
			break;

		case  SR_TEXT_CMD_STR_SUBNET_XMT_ID:
			// 3/18/2015 mpd : This field is dynamic based on SR, MODE, and GROUP. 
			strlcpy( command_buffer, (char*)sr_state->active_pvs->subnet_xmt_id, sizeof( command_buffer ) );
			break;

			// 5/1/2015 mpd : These fields are not currently needed by SR Programming. They have been tested and are functional.
			//case  SR_TEXT_CMD_STR_RADIO_ID:
			//	// 4/16/2015 mpd : This field may not be used at the moment. We can't transmit an empty string or the 
			//	// radio will hang. Use the SR_EMPTY_STR to get around this. 
			//	if( strlen( sr_state->active_pvs->radio_id ) > 0 )
			//	{
			//		strlcpy( command_buffer, (char*)sr_state->active_pvs->radio_id, sizeof( command_buffer ) );
			//	}
			//	else
			//	{
			//		// 4/16/2015 mpd : This will cause only CR and LF characters to be transmitted.
			//		memset( command_buffer, 0x00, 1 );
			//		send_just_cr_lf = true;
			//	}
			//	break;

			//case  SR_TEXT_CMD_STR_RADIO_NAME:
			//	// 4/16/2015 mpd : This field may not be used at the moment. We can't transmit an empty string or the 
			//	// radio will hang.  
			//	if( strlen( sr_state->active_pvs->radio_name ) > 0 )
			//	{
			//		strlcpy( command_buffer, (char*)sr_state->active_pvs->radio_name, sizeof( command_buffer ) );
			//	}
			//	else
			//	{
			//		// 4/16/2015 mpd : This will cause only CR and LF characters to be transmitted.
			//		memset( command_buffer, 0x00, 1 );
			//		send_just_cr_lf = true;
			//	}
			//	break;

		case  SR_MENU_CMD_END:
		case  SR_TEXT_CMD_END:
			command_ptr = NULL;
			break;

		default:
			command_ptr = NULL;
#ifdef SR_VERBOSE_ALERTS
			Alert_Message( "Unknown SR Command" );
#else
			Alert_Message( "SR26" );
#endif
			break;
	}

	// 3/18/2015 mpd : Is there a new command in the buffer?
	if( ( command_buffer[ 0 ] ) || ( send_just_cr_lf ) )
	{
		// 3/17/2015 mpd : All the text commands sent to the radio (SR_TEXT_CMD_...) require CR and LF characters at the 
		// end of the string. This does not apply to the spacial case SR_TEXT_CMD_NONE which is never sent to the radio.
		if( command > SR_TEXT_CMD_NONE )
		{
			strlcat( command_buffer, SR_CRLF_STR, ( SR_LONGEST_COMMAND_LEN + SR_CRLF_STR_LEN ) );
		}

		// 3/18/2015 mpd : Allocate memory, set the return pointer, and copy the buffer. The calling function is responsible
		// for freeing the memory.
		command_ptr = mem_malloc( strlen( command_buffer ) + NULL_TERM_LEN );
		memcpy( command_ptr, command_buffer, strlen( command_buffer ) );

#ifdef SR_DEBUG_MEM_MALLOC
		Alert_Message_va( "GET Mem Given: %p", command_ptr );
#endif

		// 3/18/2015 mpd : Null-terminate the string.
		*( command_ptr + strlen( command_buffer ) ) = 0x00;

#ifdef SR_DEBUG_GET_TEXT
		if( command == SR_TEXT_CMD_NONE )
		{
			Alert_Message( "CMD( 1 ): NONE" );
		}
		else if( command == SR_MENU_CMD_STR_ESC )
		{
			Alert_Message( "CMD( 1 ): ESC" );
		}
		else
		{
			Alert_Message_va( "CMD( %d ): %s", strlen( command_buffer ), command_ptr );
		}
#endif

	}

	return( command_ptr );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  				   READ and WRITE Lists 				  */
/* ---------------------------------------------------------- */

// 3/17/2015 mpd : This struct contains the variables and the function pointer necessary to process each individual
// response from the SR radio. The first 2 fields deal with the current response. The last 2 fields drive the subsequent
// radio response.
typedef struct
{
	// 4/8/2015 mpd : This is the text string we expect to see in the response buffer. It is a sanity check performed 
	// before attempting to parse the rest of the buffer.
	char *title_str;

	// 4/8/2015 mpd : This function pointer identifies the response handler for this specific response buffer.
	void (*sr_response_handler)( SR_STATE_STRUCT *sr_state ); 

	// 4/8/2015 mpd : The next command is what will be sent to the SR radio. It can be a simple ASCII character without CR
	// and LF, or a string.
	UNS_32 next_command;

	// 4/8/2015 mpd : This string tells rcvd_data what constitutes an end to the subsequent radio response. The comm_mngr
	// timer will expire if this string is not received within the expected time.
	char  *next_termination_str;
} sr_menu_item;

/* ---------------------------------------------------------- */
sr_menu_item sr_read_list[] =
{
	// Response Title           Response Handler                  Next Command             Next Termination String
	{ "not used",               NULL,                             SR_TEXT_CMD_NONE,        "Enter Choice" },	 // The radio provides the first buffer without a text command. 
	{ "MAIN MENU",              sr_analyze_main_menu,             SR_MENU_CMD_STR_0,       "Enter Choice"},		// Analyze all menu values. Go to MODEM MODE menu
	{ "SET MODEM MODE",         sr_analyze_set_modem_mode,        SR_MENU_CMD_STR_ESC,     "Enter Choice"},		// Analyze all menu values. Go to MAIN MENU
	{ "MAIN MENU",              NULL,                             SR_MENU_CMD_STR_1,       "Enter Choice"},		// Go to BAUD RATE MENU
	{ "SET BAUD RATE",          sr_analyze_set_baud_rate,         SR_MENU_CMD_STR_ESC,     "Enter Choice"},		// Analyze all menu values. Go to MAIN MENU     
	{ "MAIN MENU",              NULL,                             SR_MENU_CMD_STR_3,       "Enter Choice"},		// Go to RADIO PARAMETERS MENU                                       
	{ "RADIO PARAMETERS",       sr_analyze_radio_parameters,      SR_MENU_CMD_STR_ESC,     "Enter Choice"},		// Analyze all menu values. Go to MAIN MENU     
	{ "MAIN MENU",              NULL,                             SR_MENU_CMD_STR_5,       "Enter Choice"},		// Go to M?ULTIPOINT PARAMETERS MENU                                       
	{ "MULTIPOINT PARAMETERS",  sr_analyze_multipoint_parameters, SR_MENU_CMD_STR_ESC,     "Enter Choice"},		// Analyze all menu values. Go to MAIN MENU
	{ "MAIN MENU",              sr_final_radio_analysis,          SR_TEXT_CMD_END,         ""}					// Final analysis                     
};

// 4/8/2015 mpd : Determine the size of the read list. This will be used later to determine if we successfully reached 
// the end.
#define SR_READ_LIST_LEN	( sizeof( sr_read_list ) / sizeof( sr_menu_item ) )

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/16/2015 mpd : For consistency, navigate in the same menu order for WRITE as we did for READ. Note that WRITE
// requires additional analyze/hunt I/O cycles between the menu navigation cycles. Once on the appropriate menu, each
// WRITE operation involves a cycle to select a menu option, and another to input the new value. Also note that CR and
// LF characters ARE NOT INCLUDED ON EVERY LINE!!! The radio is very fussy regarding CR and LF.
// 
// The "analyze" functions following all WRITE operations on the menu are to capture the final values on that menu.
// 
// 3/16/2015 mpd : Verification of each WRITE operation can be performed on the last line associated with each menu.
// Verification of the MAIN MENU is strictly a sanity check since there are no writable fields on that SR radio menu.
//
// 4/13/2015 mpd : The "Response Title" requires enough characters to determine a unique match; not the entire line!
sr_menu_item sr_write_list[] =
{
	// Response Title           Response Handler                  Next Command                          Next Termination String
	{ "not used",               NULL,                             SR_TEXT_CMD_NONE,                     "Enter Choice" },											 // The radio provides the first buffer without a text command. 

	{ "MAIN MENU",              sr_analyze_main_menu,             SR_MENU_CMD_STR_0,                    "Enter Choice"},											// Verify all menu values. Go to MODEM MODE menu
	{ "SET MODEM MODE",         NULL,                             SR_TEXT_CMD_STR_MODE,                 "Enter Choice"},											// Set dynamic value
	{ "SET MODEM MODE",         sr_analyze_set_modem_mode,        SR_MENU_CMD_STR_ESC,                  "Enter Choice"},											// Verify all menu values. Go to MAIN MENU

	{ "MAIN MENU",              NULL,                             SR_MENU_CMD_STR_1,                    "Enter Choice"},											// Go to BAUD RATE menu
	{ "SET BAUD RATE",          NULL,                             SR_TEXT_CMD_STR_BAUD_RATE,            "Enter Choice"},											// Set rate  
	{ "SET BAUD RATE",          NULL,                             SR_MENU_CMD_STR_D,                    "3 for Both"},												// Get SETUP PORT submenu 
	{ "3 for Both",             NULL,                             SR_MENU_CMD_STR_3,                    "Enter Choice"},											// Set to BOTH 
	{ "SET BAUD RATE",          NULL,                             SR_MENU_CMD_STR_F,                    "Enter 0 for None,1 For RTS,2 for DTR"},					// Get FLOW CONTROL submenu 
	{ "Enter 0 for None",       NULL,                             SR_MENU_CMD_STR_1,                    "Enter Choice"},											// Set to RTS 
	{ "SET BAUD RATE",          sr_analyze_set_baud_rate,         SR_MENU_CMD_STR_ESC,                  "Enter Choice"},											// Verify all menu values. Go to MAIN MENU

	{ "MAIN MENU",              NULL,                             SR_MENU_CMD_STR_3,                    "Enter Choice"},											// Go to RADIO PARAMETERS menu
	{ "RADIO PARAMETERS",       NULL,                             SR_MENU_CMD_STR_0,                    "Enter New Frequency Key (0-E) (F for more)"},				// Get FREQ KEY submenu
	{ "Enter New Frequency",    NULL,                             SR_TEXT_CMD_STR_FREQ_KEY,             "Enter Choice"},											// Set dynamic value
	{ "RADIO PARAMETERS",       NULL,                             SR_MENU_CMD_STR_1,                    "Enter Max Packet (0-9)"},									// Get MAX PACKET submenu
	{ "Enter Max Packet",       NULL,                             SR_TEXT_CMD_STR_MAX_PACKET,           "Enter Choice"},											// Set dynamic value
	{ "RADIO PARAMETERS",       NULL,                             SR_MENU_CMD_STR_2,                    "Enter Min Packet (0-9)"},									// Get MIN PACKET submenu
	{ "Enter Min Packet",       NULL,                             SR_TEXT_CMD_STR_MIN_PACKET,           "Enter Choice"},											// Set dynamic value
	{ "RADIO PARAMETERS",       NULL,                             SR_MENU_CMD_STR_3,                    "Enter New Xmit Rate (0-1)"},							// Get DATA RATE submenu
	{ "Enter New Xmit Rate",    NULL,                             SR_TEXT_CMD_STR_XMIT_RATE,            "Enter Choice"},											// Set to 3
	{ "RADIO PARAMETERS",       NULL,                             SR_MENU_CMD_STR_4,                    "Enter New RF Data Rate (2-3)"},							// Get DATA RATE submenu
	{ "Enter New RF Data ",     NULL,                             SR_TEXT_CMD_STR_DATA_RATE,            "Enter Choice"},											// Set to 3
	{ "RADIO PARAMETERS",       NULL,                             SR_MENU_CMD_STR_5,                    "Enter New XmitPower (0-10)"},								// Get XMT POWER submenu
	{ "Enter New XmitPower",    NULL,                             SR_TEXT_CMD_STR_XMT_POWER,            "Enter Choice"},											// Set dynamic value
	{ "RADIO PARAMETERS",       NULL,                             SR_MENU_CMD_STR_9,                    "Enter LowPower Option or 0 to disable (0-31)"},			// Go to LP MODE submenu
	{ "Enter LowPower Option",  NULL,                             SR_TEXT_CMD_STR_LOWPOWER_MODE,        "Enter Choice"},											// Set to 0
	{ "RADIO PARAMETERS",       sr_analyze_radio_parameters,      SR_MENU_CMD_STR_ESC,                  "Enter Choice"},											// Verify all menu values. Go to MAIN MENU

	{ "MAIN MENU",              NULL,                             SR_MENU_CMD_STR_5,                    "Enter Choice"},											// Go to MULTIPOINT PARAMETERS menu
	{ "MULTIPOINT PARAMETERS",  NULL,                             SR_MENU_CMD_STR_0,                    "Enter Number of Parallel Repeaters in Network(0-9)"},		// Get NUM REPEATERS submenu
	{ "Enter Number of Para",   NULL,                             SR_TEXT_CMD_STR_NUM_REPEATERS,        "Enter Choice"},											// Set dynamic value
	{ "MULTIPOINT PARAMETERS",  NULL,                             SR_MENU_CMD_STR_1,                    "Enter Number Times Master Repeats Packets(0-9)"},			// Get MASTER REPEAT submenu
	{ "Enter Number Times M",   NULL,                             SR_TEXT_CMD_STR_MASTER_PACKET_REPEAT, "Enter Choice"},											// Set dynamic value
	{ "MULTIPOINT PARAMETERS",  NULL,                             SR_MENU_CMD_STR_2,                    "Enter Number Times Slave Tries Before Backing Off (0-9)"},	// Get SLAVE RETRY submenu
	{ "Enter Number Times S",   NULL,                             SR_TEXT_CMD_STR_MAX_SLAVE_RETRY,      "Enter Choice"},											// Set dynamic value
	{ "MULTIPOINT PARAMETERS",  NULL,                             SR_MENU_CMD_STR_3,                    "Enter Slave Backing Off Retry Odds (0-9)"},				// Get RETRY ODDS submenu
	{ "Enter Slave Backing",    NULL,                             SR_TEXT_CMD_STR_RETRY_ODDS,           "Enter Choice"},											// Set dynamic value
	{ "MULTIPOINT PARAMETERS",  NULL,                             SR_MENU_CMD_STR_4,                    "Enter 1 For DTR Sensing, 2 For Burst Mode, Otherwise 0"},				  // Get RETRY ODDS submenu
	{ "Enter 1 For DTR",        NULL,                             SR_TEXT_CMD_STR_DTR_CONNECT,          "Enter Choice"},											// Set dynamic value

	// 3/18/2015 mpd : TBD: Is "DTR Connect" needed here? How about other params?

	{ "MULTIPOINT PARAMETERS",  NULL,                             SR_MENU_CMD_STR_5,                    "Enter 0 To Use Master Hop Table, 1 To Use Repeater"},		// Get REP FREQ submenu
	{ "Enter 0 To Use Master",  NULL,                             SR_TEXT_CMD_STR_REPEATER_FREQ,        "Enter Choice"},											// Set dynamic value
	{ "MULTIPOINT PARAMETERS",  NULL,                             SR_MENU_CMD_STR_6,                    "Enter Network ID Number (0-4095)"},						// Get NETWORK ID submenu
	{ "Enter Network ID",       NULL,                             SR_TEXT_CMD_STR_NETWORK_ID,           "Enter Choice"},											// Set dynamic value
	{ "MULTIPOINT PARAMETERS",  NULL,                             SR_MENU_CMD_STR_A,                    "Enter 1 to enable Slave/Repeater or 0 for Normal"},		// Get SLAVE/REPEATER submenu
	{ "Enter 1 to enable S",    NULL,                             SR_TEXT_CMD_STR_SLAVE_REPEATER,       "Enter Choice"},											// Set dynamic value
	{ "MULTIPOINT PARAMETERS",  NULL,                             SR_MENU_CMD_STR_B,                    "Enter 1 to 129 Enable Diagnostics, 0 To Disable"},			// Get DIAG submenu
	{ "Enter 1 to 129",         NULL,                             SR_TEXT_CMD_STR_DIAGNOSTICS,          "Enter Choice"},											// Set dynamic value
	{ "MULTIPOINT PARAMETERS",  NULL,                             SR_MENU_CMD_STR_C,                    "Enter Rcv SubNetID (0-F)"},								// Get SUBNET ID submenu
	{ "Enter Rcv SubNetID",     NULL,                             SR_TEXT_CMD_STR_SUBNET_RCV_ID,        "Enter Xmit SubNetID (0-F)"},								// Set dynamic value
	{ "Enter Xmit SubNetID",    NULL,                             SR_TEXT_CMD_STR_SUBNET_XMT_ID,        "Enter Choice"},											// Set dynamic value (second submenu entry)

	// 5/11/2015 mpd : These fields are not currently needed by SR Programming. They have been tested and are functional.
	//{ "MULTIPOINT PARAMETERS",NULL,							  SR_MENU_CMD_STR_D, 		            "Enter Radio ID (0-9999)" },                                 // Get RADIO ID submenu
	//{ "Enter Radio ID",		NULL,							  SR_TEXT_CMD_STR_RADIO_ID, 		    "Enter Choice" },                                            // "Not Set" at this time

	//{ "Enter Radio ID",		NULL,							  SR_MENU_CMD_STR_ESC, 		            "Enter Choice" },                                            // ESC back to parent menu

	//{ "MULTIPOINT PARAMETERS",NULL,							  SR_MENU_CMD_STR_G, 		            "Enter Radio Name: " },                                      // Get RADIO NAME submenu
	//{ "Enter Radio Name",		NULL,							  SR_TEXT_CMD_STR_RADIO_NAME, 		    "Enter Choice" },                                            // Set dynamic value

	{ "MULTIPOINT PARAMETERS",  sr_analyze_multipoint_parameters, SR_MENU_CMD_STR_ESC,                  "Enter Choice"},											// Verify all menu values. Go to MAIN MENU
	{ "MAIN MENU",              sr_final_radio_verification,      SR_TEXT_CMD_END,                      ""}															// Final verification
};

// 4/8/2015 mpd : Determine the size of the write list. This will be used later to determine if we successfully reached
// the end.
#define SR_WRITE_LIST_LEN	( sizeof( sr_write_list ) / sizeof( sr_menu_item ) )

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  					 Radio Mode Functions   			  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void SR_FREEWAVE_set_radio_to_programming_mode( void )
{
	//Alert_Message( "DEVEX: SR_FREEWAVE_set_radio_to_programming_mode" );

	// 4/16/2015 mpd : Send progress info to the GUI. Keep it simple. Use the READ key for both READ and WRITE.
	strlcpy( sr_state->info_text, "Entering radio programming mode...", sizeof( sr_state->info_text ) );                
	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );

	// 4/15/2015 mpd : The SR radio can only be programmed at a baud rate of 19200. Since the port is at 57600 for 
	// normal communication, we have to temporarily modify the speed for programming mode.
	SERIAL_set_baud_rate_on_A_or_B( comm_mngr.device_exchange_port, SR_PROGRAMMING_BAUD_RATE );

	// 4/15/2015 mpd : Temporarily ground from the brown interrupt wire to put the radio into programming mode.
	set_reset_ACTIVE_to_serial_port_device( comm_mngr.device_exchange_port );
	vTaskDelay( MS_to_TICKS(100) );
	set_reset_INACTIVE_to_serial_port_device( comm_mngr.device_exchange_port ); 

}

/* ---------------------------------------------------------- */
static void SR_FREEWAVE_set_radio_inactive( void )
{
	//Alert_Message( "DEVEX: SR_FREEWAVE_set_radio_inactive" );

	set_reset_INACTIVE_to_serial_port_device( comm_mngr.device_exchange_port );
	//vTaskDelay( MS_to_TICKS(100) );

}

/* ---------------------------------------------------------- */
static void send_no_response_esc_to_radio()
{
	DATA_HANDLE ldh;
	char command = SR_MENU_CMD_STR_ESC;

	//Alert_Message( "send_no_response_esc_to_radio" );

	// ----------

	// 4/9/2015 mpd : Restore packet hunting (what the rest of the application expects!).
	RCVD_DATA_enable_hunting_mode( comm_mngr.device_exchange_port, PORT_HUNT_FOR_PACKETS, NOT_STRING_HUNTING  );

	ldh.dptr = (UNS_8*)&command;

	ldh.dlen = SR_MENU_CMD_LEN;

	AddCopyOfBlockToXmitList( comm_mngr.device_exchange_port, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );

}

/* ---------------------------------------------------------- */
static void exit_radio_programming_mode()
{
	//Alert_Message( "exit_radio_programming_mode" );

	// ----------

	// 4/9/2015 mpd : Send a final ESC to the radio to exit MAIN MENU.
	send_no_response_esc_to_radio();

	// 4/9/2015 mpd : Remove the ground from the brown interrupt wire.
	SR_FREEWAVE_set_radio_inactive();

	// 4/15/2015 mpd : The SR radio was temporarily set to a baud rate of 19200 for programming. It's time to put it back
	// into normal communication speed.
	SERIAL_set_baud_rate_on_A_or_B( comm_mngr.device_exchange_port, port_device_table[ COMM_DEVICE_SR_FREEWAVE ].baud_rate );

	comm_mngr.device_exchange_state = SR_DEVEX_FINISH;

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  				 COMM_MNGR Command Interface			  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void setup_for_termination_string_hunt( const char* pcommand_str, char* presponse_str)
{
	// 3/14/2014 rmd : This function is executed within the context of the comm_mngr task. We can
	// reference and change the device exchange variables as needed.

	DATA_HANDLE ldh;

	//Alert_Message( "setup_for_termination_string_hunt" );

	// 2/13/2014 rmd : Clean and restart the port buffer and string hunt. Note this disables
	// packet hunting mode.
	RCVD_DATA_enable_hunting_mode( comm_mngr.device_exchange_port, PORT_HUNT_FOR_SPECIFIED_TERMINATION, WHEN_STRING_FOUND_NOTIFY_THE_COMM_MNGR_TASK );

	// ----------

	ldh.dptr = (UNS_8*)pcommand_str;

	ldh.dlen = strlen( pcommand_str );

#ifdef SR_DEBUG_HUNT
	Alert_Message_va( "SENDING(%d): ->%s<-", ldh.dlen, ldh.dptr );
#endif

	// 3/10/2015 mpd : Identify the termination string.
	SerDrvrVars_s[ comm_mngr.device_exchange_port ].UartRingBuffer_s.th.str_to_find = presponse_str;

	SerDrvrVars_s[ comm_mngr.device_exchange_port ].UartRingBuffer_s.th.chars_to_match = strlen( presponse_str );

	// ----------

	// 3/12/2015 mpd : Don't try to add a zero length command to the Xmit list. That triggers alerts in "comm_mngr". Also,
	// check for a special "SR_EMPTY_STR" value used to flag the first I/O cycle.  The first read cycle sets up the hunt for
	// an input that is triggered by radio initialization hardware - not a xmit command. Sending a command here for the
	// first cycle would trigger an additional unwanted radio response.
	if( ( ldh.dlen > 0 ) && ( pcommand_str[ 0 ] != SR_EMPTY_STR ) )
	{
		AddCopyOfBlockToXmitList( comm_mngr.device_exchange_port, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );
	}

}

/* ---------------------------------------------------------- */
static BOOL_32 sr_get_and_process_command( SR_STATE_STRUCT *sr_state, const UNS_32 next_command, char *next_termination_str )
{
	char *command_ptr = NULL;
	BOOL_32 command_found = true;

	//Alert_Message( "sr_get_and_process_command" );

	// 3/19/2015 mpd : Get the command string.
	if( ( command_ptr = get_command_text( sr_state, next_command ) ) != NULL )
	{
		// 3/10/2015 mpd : Set up the string hunt.
		setup_for_termination_string_hunt( command_ptr, next_termination_str );  

#ifdef SR_DEBUG_MEM_MALLOC
		Alert_Message_va( "GET Mem Freed: %p", command_ptr );
#endif

		// 3/18/2015 mpd : Free the memory obtained by "get_command_text()".
		mem_free( command_ptr );
	}
	else
	{
		command_found = false;
	}

	return( command_found );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  			  Operation Initialization  				  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void sr_set_read_operation( SR_STATE_STRUCT *sr_state )
{
	//Alert_Message( "sr_set_read_operation" );

	sr_state->operation = SR_READ_OPERATION;
	strlcpy( sr_state->operation_text, SR_READ_TEXT_STR, sizeof( sr_state->operation_text ) );                
	strlcpy( sr_state->info_text, "Beginning read operation...", sizeof( sr_state->info_text ) );                

	// 4/13/2015 mpd : Send a status message to the key task.
	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );

	// 3/12/2015 mpd : Reset the read list to the beginning. This is used here at initialization and by the read state
	// machine to navigate through all the radio menus and process responses.
	sr_state->read_list_index = 0;

	// 3/20/2015 mpd : Initialize the WRITE index even though we will not be using it at the moment.
	sr_state->write_list_index = 0;

	sr_state->active_pvs  = sr_state->dynamic_pvs; // The initial SR_READ_OPERATION needs the dynamic struct.        

	// 3/20/2015 mpd : Setup the state machine entry point.
	comm_mngr.device_exchange_state = SR_DEVEX_READ;

}

/* ---------------------------------------------------------- */
static void sr_set_write_operation( SR_STATE_STRUCT *sr_state )
{
	//Alert_Message( "sr_set_write_operation" );

	sr_state->operation = SR_WRITE_OPERATION;
	strlcpy( sr_state->operation_text, SR_WRITE_TEXT_STR, sizeof( sr_state->operation_text ) );                
	strlcpy( sr_state->info_text, "Beginning write operation...", sizeof( sr_state->info_text ) );                

	// 4/13/2015 mpd : Send a status message to the key task.
	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_write_settings_in_progress );

	// 3/12/2015 mpd : Reset the write list to the beginning. This is used here at initialization and by the read state
	// machine to navigate through all the radio menus and process responses.
	sr_state->write_list_index = 0;

	// 3/20/2015 mpd : Initialize the READ index even though we will not be using it at the moment.
	sr_state->read_list_index = 0;

	// 4/13/2015 mpd : Set the "dynamic_pvs" active for the copy operation.
	sr_state->active_pvs  = sr_state->dynamic_pvs;         

	// 4/13/2015 mpd : Copy all the dynamic values to the "write_pvs".
	sr_copy_active_values_to_write( sr_state );

	// 4/13/2015 mpd : Set the "write_pvs" active for for the remainder of the WRITE operation.
	sr_state->active_pvs  = sr_state->write_pvs;         

	// 3/20/2015 mpd : Setup the state machine entry point.
	comm_mngr.device_exchange_state = SR_DEVEX_WRITE;

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  				  State Struct Handling 				  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void sr_set_state_struct_for_new_device_exchange( SR_STATE_STRUCT *sr_state )
{
	//Alert_Message( "sr_set_state_struct_for_new_device_exchange" );

	// 3/23/2015 mpd : These are fields that need to be set each time the SR radio programming screen is entered or a 
	// button is selected by the user.

	// 4/8/2015 mpd : TODO: This field is not yet implemented.
	sr_state->error_code = SR_ERROR_NONE;

	// 4/16/2015 mpd : Clear any stale text messages.
	strlcpy( sr_state->operation_text, "", sizeof( sr_state->operation_text ) );                
	strlcpy( sr_state->info_text,      "", sizeof( sr_state->info_text ) );                
	strlcpy( sr_state->progress_text,  "", sizeof( sr_state->progress_text ) );                

	if( comm_mngr.device_exchange_initial_event == COMM_MNGR_EVENT_request_write_device_settings )
	{
		COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_write_settings_in_progress );

		sr_set_write_operation( sr_state );

		//Alert_Message( "DEVEX: WRITE received from COMM_MNGR" );
	}
	else
	{
		COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );

		// 3/16/2015 mpd : Default to READ.
		sr_set_read_operation( sr_state );
		//Alert_Message( "DEVEX: READ received from COMM_MNGR" );
	}

	// 3/19/2015 mpd : Assume the worst. Analysis after the first READ will set these to the correct values.
	sr_state->mode_is_valid = false;
	sr_state->group_is_valid = false;
	sr_state->repeater_is_valid = false;
	sr_state->radio_settings_are_valid = false;

}

/* ---------------------------------------------------------- */
static void sr_initialize_state_struct( SR_STATE_STRUCT *sr_state )
{
	//Alert_Message( "sr_initialize_state_struct" );

	// 3/20/2015 mpd : Set pointers to the programmable variables structs.

	// 3/19/2015 mpd : The "dynamic_pvs" is the dynamic copy of the programmable values struct.
	// It contains what is read from the radio and possibly modified by the user. This is the
	// programmable values struct used to provide command text strings during (most) SR radio
	// programming WRITE operations.
	sr_state->dynamic_pvs = &sr_dynamic_pvs;

	// 3/20/2015 mpd : The "write_pvs" programmable values struct receives all the programmable
	// values from the "dynamic_pvs" immediately before every WRITE. It is used after the
	// WRITE to verify that all the "dynamic_pvs" values read from the radio match what was
	// written. Any differences will be noted on the screen as a programming failure with
	// details in alert messages.
	sr_state->write_pvs = &sr_write_pvs;

	// 3/23/2015 mpd : These tokens are an artifact from a development phase when the "sr_state" and all the PVS structs 
	// were allocated. The tokens were a sanity check for future device exchanges to verify the struct pointer had not been 
	//clobbered. Keep it here since the work is done. 
	strlcpy( sr_state->dynamic_pvs->pvs_token, SR_PVS_TOKEN_STR,   sizeof( sr_state->dynamic_pvs->pvs_token ) );
	strlcpy( sr_state->write_pvs->pvs_token,   SR_PVS_TOKEN_STR,   sizeof( sr_state->write_pvs->pvs_token ) );

	// 3/23/2015 mpd : Common tasks required for every device exchange have been pulled out.
	//sr_set_state_struct_for_new_device_exchange( sr_state );

	// 3/19/2015 mpd : These are read-only radio parameters. They are here to avoid 3 copies.
	strlcpy( sr_state->sw_version,    "", sizeof( sr_state->sw_version ) );                

	// 5/1/2015 mpd : These fields are not currently needed by SR Programming. They have been tested and are functional.
	//strlcpy( sr_state->sw_date,       "", sizeof( sr_state->sw_date ) );                     

	strlcpy( sr_state->serial_number, "", sizeof( sr_state->serial_number ) );            
	strlcpy( sr_state->model,         "", sizeof( sr_state->model ) );                      

	// 3/19/2015 mpd : These are Calsense defined groups.
	sr_state->group_index = SR_GROUP_DEFAULT;
	strlcpy( sr_state->network_group_id,  "", sizeof( sr_state->network_group_id ) );
	strlcpy( sr_state->repeater_group_id, "", sizeof( sr_state->repeater_group_id ) );

}

/* ---------------------------------------------------------- */
static BOOL_32 sr_verify_state_struct( SR_STATE_STRUCT *sr_state )
{
	BOOL_32 valid_state_struct = false;

	//Alert_Message( "sr_verify_state_struct" );

	// 3/23/2015 mpd : Verify the "dynamic_pvs" pointer.
	if( sr_state->dynamic_pvs != NULL )
	{
		// 3/23/2015 mpd : Verify the "pvs_token".
		if( strncmp( sr_state->dynamic_pvs->pvs_token, SR_PVS_TOKEN_STR, SR_PVS_TOKEN_LEN ) == NULL )
		{
			// 3/23/2015 mpd : This is a valid "dynamic_pvs" structure from earlier excution on this screen. 

			// 3/23/2015 mpd : Verify the "write_pvs" pointer.
			if( sr_state->write_pvs != NULL )
			{
				// 3/23/2015 mpd : Verify the "pvs_token".
				if( strncmp( sr_state->write_pvs->pvs_token, SR_PVS_TOKEN_STR, SR_PVS_TOKEN_LEN ) == NULL )
				{
					valid_state_struct = true;
				}
			}
		}
	}

	return( valid_state_struct );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  	 READ/WRITE Item processing and the State Machine     */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static BOOL_32 process_list( SR_STATE_STRUCT *sr_state, COMM_MNGR_TASK_QUEUE_STRUCT *q_msg, const sr_menu_item *list_item, UNS_32 operation )
{
	char *title_ptr = NULL;
	BOOL_32 continue_cycle = true;

#ifdef SR_FIELD_DEBUGGING_ALERTS
	//DATE_TIME current_time;
	BOOL_32 timeout_on_first = false;
#endif

	// 3/12/2015 mpd : This function performs two separate operations.
	// 
	// 1. The first operation deals with the current contents of the receive buffer. The list_item struct identifies the
	// title string and the appropriate handler for the work.
	// 
	// 2. The second operation sets up RCVD_DATA to wait for the next receive buffer. The list_item struct specifies the
	// command string needed to put the SR radio into the requested menu and what string indicates the end of the radio
	// response.

#ifdef SR_DEBUG_PROCESS_LIST
	Alert_Message( "process_list" );
#endif

	// 3/12/2015 mpd : FIRST OPERATION: Deal with the existing receive buffer.

	if( q_msg->event == COMM_MNGR_EVENT_device_exchange_response_arrived )
	{
#ifdef SR_FIELD_DEBUGGING_ALERTS
		if( ( sr_state->read_list_index == 1 ) || ( sr_state->write_list_index == 1 ) )
		{
			// 5/20/2015 mpd : Use tick count for better granularity.
			//EPSON_obtain_latest_time_and_date( &current_time );
			//Alert_Message_va( "First %s Command Delay: %d", sr_state->operation_text, current_time.T - sr_first_command_time.T );
			Alert_Message_va( "First %s Command Delay: %d ms", sr_state->operation_text, ( ( my_tick_count - sr_first_command_ticks ) * SR_TICKS_PER_MS ) );
		}
#endif

		// 3/23/2015 mpd : The memory handed to us by "rcvd_data" is our's to keep until we are done parsing. Better still, we
		// free it at the end of parsing before a user can create a "key timeout" by modifying a field and going home for the
		// night. Make convenient local copies of the pointer and response length so the rest of SR radio code does not
		// reference "comm_mngr" variables.
		if( ( q_msg->dh.dptr != NULL ) && ( q_msg->dh.dlen > 0 ) )
		{
#ifdef SR_DEBUG_MEM_MALLOC
			Alert_Message_va( "RCVD Mem Given( %d ): %p", q_msg->dh.dlen, (char*)(q_msg->dh.dptr) );
#endif

			//Alert_Message( list_item->title_str );

			// 3/11/2015 mpd : Look for the menu title string in the data buffer.

			// 4/10/2015 mpd : Embedded '\0' characters have been seen at the beginning of SR radio response buffers. These
			// end strstr() searches. Use a custom search based on buffer length.
			//if( ( title_ptr = strstr( (char*)q_msg->dh.dptr, list_item->title_str ) ) != NULL )
			if( ( title_ptr = find_string_in_block( (char*)q_msg->dh.dptr, list_item->title_str, q_msg->dh.dlen ) ) != NULL )
			{
				// 3/10/2015 mpd : The SR radio seems to add various CR and LF characters at the start of the buffer. We need a
				// predicatble offset for all fields in the response. Set the menu title string as the start of the response.
				sr_state->resp_ptr = title_ptr;

				// 3/23/2015 mpd : The length may have been modified since we started at "title_ptr". Calculate how much 
				// we took off the front, then subtract from the total.
				sr_state->resp_len =  ( q_msg->dh.dlen - ( title_ptr - (char*)q_msg->dh.dptr ) );

#ifdef SR_DEBUG_PROCESS_LIST
				Alert_Message_va( "%s: ->%s<- found", sr_state->operation_text, list_item->title_str );
#endif

				// 3/20/2015 mpd : Call the response handler to do something with this buffer. This is the meat!
				if( list_item->sr_response_handler != NULL )
				{
					(list_item->sr_response_handler)( sr_state );
				}
			}

			// 3/23/2015 mpd : The string we are looking for was not found in the response buffer.
			else
			{
#ifdef SR_VERBOSE_ALERTS
				Alert_Message_va( "%s: ->%s<- not found", sr_state->operation_text, list_item->title_str );
#else
				Alert_Message( "SR27" );
#endif
				continue_cycle = false;
			}

			// 3/11/2015 mpd : Free the memory obained by the receive task.
#ifdef SR_DEBUG_MEM_MALLOC
			Alert_Message_va( "RCVD Mem Freed( %d ): %p", q_msg->dh.dlen, (char*)(q_msg->dh.dptr) );
#endif

			mem_free( q_msg->dh.dptr );
		}

	} // END COMM_MNGR_EVENT_device_exchange_response_arrived

	else if( q_msg->event == COMM_MNGR_EVENT_device_exchange_response_timer_expired )
	{
#ifdef SR_FIELD_DEBUGGING_ALERTS
		if( ( sr_state->read_list_index == 1 ) || ( sr_state->write_list_index == 1 ) )
		{
			timeout_on_first = true;
			// 5/20/2015 mpd : Use tick count for better granularity.
			//EPSON_obtain_latest_time_and_date( &current_time );
			//Alert_Message_va( "First %s Command Timeout: %d", sr_state->operation_text, current_time.T - sr_first_command_time.T );
			Alert_Message_va( "First %s Command Timeout: %d ms", sr_state->operation_text, ( ( my_tick_count - sr_first_command_ticks ) * SR_TICKS_PER_MS ) );

			// 5/20/2015 mpd : Power-cycle the device. This is a special case for timeouts on the first command.
			SR_FREEWAVE_power_control( comm_mngr.device_exchange_port, false );
			vTaskDelay( MS_to_TICKS(250) );
			SR_FREEWAVE_power_control( comm_mngr.device_exchange_port, true );
			vTaskDelay( MS_to_TICKS(2000) );
		}
#else
	#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "COMM MNGR Timer Expired" );
	#else
		Alert_Message( "SR28" );
	#endif
#endif

		continue_cycle = false;
	}
	else
	{
#ifdef SR_FIELD_DEBUGGING_ALERTS
		if( ( sr_state->read_list_index == 1 ) || ( sr_state->write_list_index == 1 ) )
		{
			// 5/20/2015 mpd : Use tick count for better granularity.
			//EPSON_obtain_latest_time_and_date( &current_time );
			//Alert_Message_va( "First %s Unknown Delay: %d", sr_state->operation_text, current_time.T - sr_first_command_time.T );
			Alert_Message_va( "First %s Unknown Delay: %d ms", sr_state->operation_text, ( ( my_tick_count - sr_first_command_ticks ) * SR_TICKS_PER_MS ) );
		}
#else
	#ifdef SR_VERBOSE_ALERTS
		Alert_Message( "Unknown event" );
	#else
		Alert_Message( "SR29" );
	#endif
#endif

		continue_cycle = false;
	}

	// 3/12/2015 mpd : SECOND OPERATION: Set up the COMM_MNGR to wait for the next receive buffer.

	if( continue_cycle )
	{
#ifdef SR_DEBUG_PROCESS_LIST
		if( list_item->next_command != SR_TEXT_CMD_END )
		{
			if( list_item->next_command == SR_MENU_CMD_STR_ESC )
			{
				Alert_Message_va( "Next CMD[0]: ESC  Next Term: ->%s<-", list_item->next_termination_str );
			}
			else if( list_item->next_command == SR_TEXT_CMD_NONE )
			{
				Alert_Message_va( "Next CMD[0]: NONE  Next Term: ->%s<-", list_item->next_termination_str );
			}
			else
			{
				Alert_Message_va( "Next CMD[0]: ->%c<-  Next Term: ->%s<-", list_item->next_command, list_item->next_termination_str );
			}
		}
#endif

		// 5/11/2015 mpd : Give the SR time to catch it's breath between commands.
		//vTaskDelay( MS_to_TICKS(500) );

		// 3/20/2015 mpd : The "active_pvs" pointer determines what command set "get_command_text()" will be looking at. That 
		// pointer was handled when the "sr_state->operation" was set. There is no decision to be made here.

		if( !sr_get_and_process_command( sr_state, list_item->next_command, (char *)( list_item->next_termination_str ) ) )
		{
			continue_cycle = false;
		}
	}
	else
	{
#ifdef SR_FIELD_DEBUGGING_ALERTS
		// 5/20/2015 mpd : A timeout on the first command triggers a power-cycle. Skip sending ESC.
		if( !timeout_on_first )
		{
			// 4/9/2015 mpd : This condition should never happen. If it does, the radio needs to be blown out of programming mode to
			// recover. Throw multiple ESC keys at it to exit all menus.
			send_no_response_esc_to_radio();
			send_no_response_esc_to_radio();
			send_no_response_esc_to_radio();
		}
#else
		// 4/9/2015 mpd : This condition should never happen. If it does, the radio needs to be blown out of programming mode to
		// recover. Throw multiple ESC keys at it to exit all menus.
		send_no_response_esc_to_radio();
		send_no_response_esc_to_radio();
		send_no_response_esc_to_radio();
#endif
	}

	return( continue_cycle );
}

/* ---------------------------------------------------------- */
static void sr_state_machine( SR_STATE_STRUCT *sr_state, COMM_MNGR_TASK_QUEUE_STRUCT *pq_msg )
{
	// 3/14/2014 rmd : This function executed within the context of the comm_mngr task. We can
	// reference and change the device exchange variables as needed.

	// ----------

	UNS_32 i;

	UNS_32  device_exchange_result;

	// ----------

	//Alert_Message( "DEVEX: sr_state_machine" );

	switch( comm_mngr.device_exchange_state )
	{
		case SR_DEVEX_READ:

			// 3/12/2015 mpd : Initialization used the first READ "sr_menu_list" entry. This state machine needs to begin at the
			// second entry. Bump the index here BEFORE using it.
			sr_state->read_list_index++;

#ifdef SR_DEBUG_STATE
			Alert_Message_va( "RECEIVED %d bytes.", pq_msg->dh.dlen );
			Alert_Message_va( "Read List Index: %d", sr_state->read_list_index );
#endif

			// 3/18/2015 mpd : Loop through the list until there are no more commands (return == "false").
			if( ( process_list( sr_state, pq_msg, &( sr_read_list[ sr_state->read_list_index ] ), SR_READ_OPERATION ) ) == false )
			{
				exit_radio_programming_mode();

				// 3/18/2015 mpd : We just finished looping through the list. Did we finist too early?
				if( sr_state->read_list_index == 1 )
				{
					// 4/16/2015 mpd : We never heard from COMM_MNGR.
#ifdef SR_VERBOSE_ALERTS
					Alert_Message( "Radio READ ended early: 1" );
#else
					Alert_Message( "SR30" );
#endif

					strlcpy( sr_state->info_text, "COMM_MNGR Timeout", sizeof( sr_state->info_text ) );                

					device_exchange_result = DEVICE_EXCHANGE_KEY_read_settings_error;
				}
				else if( sr_state->read_list_index < ( SR_READ_LIST_LEN - 1 ) )
				{
					// 4/16/2015 mpd : We got part way through the list. The final count will help with debugging.
#ifdef SR_VERBOSE_ALERTS
					Alert_Message_va( "Radio MENU %s ended early: %d", sr_state->operation_text, sr_state->read_list_index );
#else
					Alert_Message_va( "SR31 %d", sr_state->read_list_index );
#endif

					strlcpy( sr_state->info_text, "Read ended early", sizeof( sr_state->info_text ) );                

					device_exchange_result = DEVICE_EXCHANGE_KEY_read_settings_error;
				}
				// 4/13/2015 mpd : We successfully got to the end of the list.
				else
				{
					if( sr_state->radio_settings_are_valid )
					{
						strlcpy( sr_state->info_text, "Read completed", sizeof( sr_state->info_text ) );                
						//Alert_Message( "Radio READ successful" );
					}
					else
					{
						strlcpy( sr_state->info_text, "Read completed. Settings Invalid", sizeof( sr_state->info_text ) );                
					}

					// 5/12/2015 mpd : Invalid device settings are considered a configuration issue, not a READ error.
					// Return "OK" for invalid so the GUI will display values and allow access to the WRITE function.
					device_exchange_result = DEVICE_EXCHANGE_KEY_read_settings_completed_ok;
				}

				// 4/16/2015 mpd : After exiting programming mode, wait for 10 seconds for the radios to sync before allowing comm_mngr
				// to start token processing.
				strlcpy( sr_state->progress_text, "Syncing Radios ", sizeof( sr_state->progress_text ) );
				COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );

				// 5/18/2015 ajv : Notify the device exchange dialog that the radios are syncing so the
				// progress bar moves.
				GuiVar_DeviceExchangeSyncingRadios = (true);

				// 5/18/2015 ajv : And now draw the dialog.
				DEVICE_EXCHANGE_draw_dialog();

				// 4/16/2015 mpd : The user needs some feedback during this 10 second wait! Break up the time and add a 
				// progress indicator.
				for( i = 0; i < 10; ++i )
				{
					// 5/18/2015 ajv : No need to do anything here since TD_CHECK will ensure the text on the
					// screen is updated real-time.
					//vTaskDelay( MS_to_TICKS( 1000 ) );
				}

				// 5/18/2015 ajv : Notify the device exchange dialog that we're done syncing the radios.
				GuiVar_DeviceExchangeSyncingRadios = (false);

				// 4/16/2015 mpd : The wait is over. Clear the progress text.
				strlcpy( sr_state->progress_text, "", sizeof( sr_state->progress_text ) );
				COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress ); 

				// 4/13/2015 mpd : Send the device exchange result as message to the key task.
				COMM_MNGR_device_exchange_results_to_key_process_task( device_exchange_result );
			}
			else
			{
				// 4/13/2015 mpd : There is more to do. Set the COMM_MNGR timer and wait for another response.

				// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
				// when posting is safe. Anything on the timer queue is quickly processed and therefore the
				// queue is effectively always empty.
				xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(SR_COMM_TIMER_MS), portMAX_DELAY );
			}
			break;

		case SR_DEVEX_WRITE:
			// 4/9/2015 mpd : The WRITE operation uses the "sr_dynamic_pvs" as a source for programming the SR radio. Values in that
			// struct may be from the previous READ operation, or they may have been modified by a user in the GUI code. A copy is
			// made before the WRITE ("sr_dynamic_pvs" is copied to "sr_write_pvs") so the intended values can be verified against
			// what actually ended up in the radio after the WRITE ("sr_dynamic_pvs").

			// 3/12/2015 mpd : Initialization used the first WRITE "sr_menu_list" entry. This state machine needs to begin at the
			// second entry. Bump the index here BEFORE using it.
			sr_state->write_list_index++;

			// 3/18/2015 mpd : Loop through the list until there are no more commands (return == "false").
			if( ( process_list( sr_state, pq_msg, &( sr_write_list[ sr_state->write_list_index ] ), SR_WRITE_OPERATION  ) ) == false )
			{
				exit_radio_programming_mode();

				// 3/18/2015 mpd : We just finished looping through the list. Did we finist too early?
				if( sr_state->write_list_index == 1 )
				{
					// 4/16/2015 mpd : We never heard from COMM_MNGR.
#ifdef SR_VERBOSE_ALERTS
					Alert_Message( "Radio WRITE ended early: 1" );
#else
					Alert_Message( "SR32" );
#endif

					strlcpy( sr_state->info_text, "COMM_MNGR Timeout", sizeof( sr_state->info_text ) );                

					device_exchange_result = DEVICE_EXCHANGE_KEY_write_settings_error;
				}
				else if( sr_state->write_list_index < ( SR_WRITE_LIST_LEN - 1 ) )
				{
					// 4/16/2015 mpd : We got part way through the list. The final count will help with debugging.
#ifdef SR_VERBOSE_ALERTS
					Alert_Message_va( "Radio MENU %s ended early: %d", sr_state->operation_text, sr_state->write_list_index );
#else
					Alert_Message_va( "SR33 %d", sr_state->write_list_index );
#endif

					strlcpy( sr_state->info_text, "Write ended early", sizeof( sr_state->info_text ) );                

					device_exchange_result = DEVICE_EXCHANGE_KEY_write_settings_error;
				}
				// 4/13/2015 mpd : We successfully got to the end of the list.
				else
				{
					if( sr_state->programming_successful )
					{
						strlcpy( sr_state->info_text, "Write completed", sizeof( sr_state->info_text ) );                
						//Alert_Message( "Radio programming successful" );
					}
					else
					{
						strlcpy( sr_state->info_text, "Write completed with mismatches", sizeof( sr_state->info_text ) );  

						// 4/10/2015 mpd : The verification function has already listed details in alerts. Add one last note.
#ifdef SR_VERBOSE_ALERTS
						Alert_Message( "Radio programming failed" );
#else
						Alert_Message( "SR34" );
#endif
					}
					// 5/12/2015 mpd : A mismatch after a WRITE is considered a configuration issue, not a WRITE error.
					// We were able to get to the end of the WRITE list. Return "OK" for invalid so the GUI will display 
					// values and allow the user to see the problem.
					device_exchange_result = DEVICE_EXCHANGE_KEY_write_settings_completed_ok;
				}

				// 4/16/2015 mpd : After exiting programming mode, wait for 10 seconds for the radios to sync before allowing comm_mngr
				// to start token processing.
				strlcpy( sr_state->progress_text, "Syncing Radios ", sizeof( sr_state->progress_text ) );
				COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_write_settings_in_progress );

				// 5/18/2015 ajv : Notify the device exchange dialog that the radios are syncing so the
				// progress bar moves.
				GuiVar_DeviceExchangeSyncingRadios = (true);

				// 5/18/2015 ajv : And now draw the dialog.
				DEVICE_EXCHANGE_draw_dialog();

				// 4/16/2015 mpd : The user needs some feedback during this 10 second wait! Break up the time and add a 
				// progress indicator.
				for( i = 0; i < 10; ++i )
				{
					// 5/18/2015 ajv : No need to do anything here since TD_CHECK will ensure the text on the
					// screen is updated real-time.
					//vTaskDelay( MS_to_TICKS( 1000 ) );
				}

				// 5/18/2015 ajv : Notify the device exchange dialog that we're done syncing the radios.
				GuiVar_DeviceExchangeSyncingRadios = (false);

				// 4/16/2015 mpd : The wait is over. Clear the progress text.
				strlcpy( sr_state->progress_text, "", sizeof( sr_state->progress_text ) );
				COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_write_settings_in_progress );

				// 4/13/2015 mpd : Send the device exchange result as message to the key task.
				COMM_MNGR_device_exchange_results_to_key_process_task( device_exchange_result );
			}
			else
			{
				// 4/13/2015 mpd : There is more to do. Set the COMM_MNGR timer and wait for another response.

				// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
				// when posting is safe. Anything on the timer queue is quickly processed and therefore the
				// queue is effectively always empty.
				xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(SR_COMM_TIMER_MS), portMAX_DELAY );
			}
			break;

		case SR_DEVEX_FINISH:
		default:
			// ----------

			//Alert_Message( "End of WRITE STATE MACHINE" );

			// 4/22/2015 mpd : The READ or WRITE is complete. Clean up the operation fields.
			sr_state->operation = SR_IDLE_OPERATION;
			strlcpy( sr_state->operation_text, SR_IDLE_TEXT_STR, sizeof( sr_state->operation_text ) );                

			break;
	}

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  					Extern Functions					  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void SR_FREEWAVE_power_control( const UNS_32 pport, const BOOL_32 pon_or_off )
{
	// 11/21/2014 rmd : This function is executed within the context of the COMM_MNGR task.

	// 6/28/2013 rmd : This is an extern function, but NOT intended to be directly called. It
	// can be, but that is not its intended use. Should be referenced via the
	// config_c.port_settings.

	//Alert_Message( "DEVEX: SR_FREEWAVE_power_control" ) 

	// 3/25/2014 rmd : On the Freewave device card the reset line is hooked up to the menu pin.
	// When momentarily driven low will put the device into menu mode.
	set_reset_INACTIVE_to_serial_port_device( pport );

	// 3/10/2014 rmd : Call the generic power control function. FALSE means to power down. TRUE
	// means to power up.
	GENERIC_freewave_card_power_control( pport, pon_or_off );
}

/* ---------------------------------------------------------- */
extern void SR_FREEWAVE_initialize_device_exchange( void )
{
	//Alert_Message( "DEVEX: SR_FREEWAVE_initialize_device_exchange" );

	// 4/8/2015 mpd : Determine if the state structure has ever been initialized.
	if( !sr_verify_state_struct( sr_state ) )
	{
		// 4/8/2015 mpd : The state structure is invalid. This is normal upon first entering the SR programming screen. 
		// Handle all the first-time initialization.
		sr_initialize_state_struct( sr_state );
	}

	// 4/13/2015 mpd : Handle the state structure fields that reset on every event. 
	sr_set_state_struct_for_new_device_exchange( sr_state );

	// The SR radio menu READ process is a cycle typically triggered by RCVD_DATA when it recognizes a complete buffer. The
	// cycle starts with analyzing the current response buffer from the radio, then requesting another buffer. The
	// iterations are driven by an arrays of "sr_menu_item" structures. Here at initialization, only the "hunt" half of the
	// first READ "sr_menu_item" entry is being used since there is no initial buffer to analyze. The "next_termination_str"
	// is the critical piece that informs RCVD_DATA what we are waiting for. The first "next_command_str" is also a special
	// case. It is set to a special value indicating not to send a command to the radio. We need a process cycle to read the
	// contents of the first buffer.

	// 5/21/2015 mpd : TODO: Fix this using EN as a model.

	// 3/20/2015 mpd : Get the first command string from the list and set up the string hunt so the initial response from
	// the radio is expected, captured, and returned to the state machine.
	if( sr_get_and_process_command( sr_state, sr_read_list[ sr_state->read_list_index ].next_command, sr_read_list[ sr_state->read_list_index ].next_termination_str ) )
	{
#ifdef SR_FIELD_DEBUGGING_ALERTS
		// 5/20/2015 mpd : Use tick count for better granularity.
		//EPSON_obtain_latest_time_and_date( &sr_first_command_time );
		sr_first_command_ticks = my_tick_count;
#endif

		// 3/12/2015 mpd : This triggers the SR radio to enter programming mode and send the initial menu buffer.
		SR_FREEWAVE_set_radio_to_programming_mode();

		// 3/14/2014 rmd : Start the timer so that 10ms from now the first exchange will start.

		// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
		// when posting is safe. Anything on the timer queue is quickly processed and therefore the
		// queue is effectively always empty.
		xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(SR_COMM_TIMER_MS), portMAX_DELAY );
	}

}

/* ---------------------------------------------------------- */
extern void SR_FREEWAVE_exchange_processing( void *pq_msg )
{
	// 3/18/2014 rmd : Note how unfortunately we had to use a parameter of type void*. We could
	// not use the COMM_MNGR_EVENT_QUEUE_EVENT_STRUCT type as it wouldn't compile. Circular
	// references. void* was the easiest way out of the problem.

	//Alert_Message( "DEVEX: SR_FREEWAVE_exchange_processing" );

	// 3/19/2014 rmd : Always stop the response timer for the device exchange process.
	xTimerStop( comm_mngr.timer_device_exchange, portMAX_DELAY );

	// 3/19/2014 rmd : During the last exchange the timer is started again and eventually fires
	// off. This is the block to prevent further unwanted processing.
	if( comm_mngr.mode == COMM_MNGR_MODE_device_exchange )
	{
		sr_state_machine( sr_state, (COMM_MNGR_TASK_QUEUE_STRUCT*)pq_msg );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	FREEWAVE_SR_STATE_wait_after_a_powerdown		(1)

#define	FREEWAVE_SR_STATE_waiting_for_connection		(2)

typedef struct
{
	// 2/8/2017 rmd : Precaution - using underscore so the 'state' variable is not confused with
	// the cics.state variable!
	UNS_32	_state;
	
	UNS_32	port;
	
	UNS_32	wait_count;
	
	UNS_32	connection_start_time;
	
} FREEWAVE_SR_control_structure;

static FREEWAVE_SR_control_structure	sr_cs;

/* ---------------------------------------------------------- */
extern void SR_FREEWAVE_initialize_the_connection_process( UNS_32 pport )
{
	if( pport != UPORT_A )
	{
		Alert_Message( "Why is the SR not on PORT A?" );
	}

	// 11/11/2016 rmd : Check to make sure the is connected function is not NULL. Otherwise we
	// couldn't call it and the connection process would error out. Over and over again.
	if( port_device_table[ config_c.port_A_device_index ].__is_connected == NULL )
	{
		Alert_Message( "Unexpected NULL is_connected function" );
		
		// 2/15/2017 rmd : I think there is no point to trying to send messages anyway. They won't
		// go because we don't have a valid 'is connected' function! So do nothing in the face of
		// this error.
	}
	else
	{
		sr_cs.port = pport;
		
		sr_cs.wait_count = 0;

		sr_cs.connection_start_time = my_tick_count;
		
		// 2/24/2017 rmd : We have defined the SR radio connection process as a cycling of power.
		// Followed by waiting for CD to indicate synchonization with the master. So start with a
		// power down.
		power_down_device( sr_cs.port );
		
		// ----------
		
		// 11/11/2016 rmd : First thing we do is to power down the device. And wait 5 seconds.
		xTimerChangePeriod( cics.process_timer, MS_to_TICKS(5000), portMAX_DELAY );
	
		sr_cs._state = FREEWAVE_SR_STATE_wait_after_a_powerdown;
	}
}

/* ---------------------------------------------------------- */
extern void SR_FREEWAVE_connection_processing( UNS_32 pevent )
{
	BOOL_32	lerror;
	
	lerror = (false);
	
	// ----------
	
	if( (pevent != CI_EVENT_process_string_found) && (pevent != CI_EVENT_process_timer_fired) )
	{
		// 10/29/2016 rmd : ERROR.
		Alert_Message( "Connection Process : UNXEXP EVENT" );
		
		lerror = (true);
	}
	else
	{
		switch( sr_cs._state )
		{
			case FREEWAVE_SR_STATE_wait_after_a_powerdown:
				power_up_device( sr_cs.port );
				
				// 2/24/2017 rmd : Power up the SR radio and wait 10 seconds. In case of an CD chatter that
				// may occur during power up sequence. That way we don't false trigger a connected state. So
				// wait 10 seconds.
				xTimerChangePeriod( cics.process_timer, MS_to_TICKS(10000), portMAX_DELAY );
			
				sr_cs._state = FREEWAVE_SR_STATE_waiting_for_connection;
				break;
				
			case FREEWAVE_SR_STATE_waiting_for_connection:
				if((port_device_table[ config_c.port_A_device_index ].__is_connected != NULL) )
				{
					if( port_device_table[ config_c.port_A_device_index ].__is_connected( sr_cs.port ) )
					{
						// 11/11/2016 rmd : Okay we're done. Device connected. Update the Comm Test string so the
						// user can see the current status. And alert.
						strlcpy( GuiVar_CommTestStatus, "SR Connection", sizeof(GuiVar_CommTestStatus) );
					

						Alert_Message_va( "SR Radio sync'd to master in %u seconds", ((my_tick_count - sr_cs.connection_start_time)/200) );
						
						// ----------
						
						CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities();
					}
					else
					{
						// 1/24/2017 rmd : There is a HUGE disconnect between the device programming and the
						// controller initiated connection sequence. One is really NOT aware of the other. And they
						// do not co-exist well. For example if the connection process fails it just keeps trying
						// over and over again. And if you are performing device programming the power down-->up
						// associated with the connection process could occur right in the middle of device
						// programming. Not sure how well that would play out. And we have to allow the device
						// programming to proceed right now cause there ain't a way to stop the connection process.
						// Leaving this time large helps give the user a better chance of success while device
						// programming. A real cop-out but all I can do for right now.
						//
						// 1/24/2017 rmd : PERHAPS a mechanism to terminate the connection sequence to allow the
						// user a device programming interlude. A flag we set that the connection_processing
						// function would act upon.
						if( sr_cs.wait_count >= 120 )
						{
							// 11/11/2016 rmd : We've been waiting. SR normally connects in about 20 seconds so after 2
							// minutes of waiting let's restart the connection process.
							lerror = (true);
						}
						else
						{
							sr_cs.wait_count += 1;
							
							xTimerChangePeriod( cics.process_timer, MS_to_TICKS(1000), portMAX_DELAY );
						}
					}
				}
				else
				{
					// 11/11/2016 rmd : The sequence will restart and we have a test in the initialization
					// function for the NULL function.
					lerror = (true);
				}
				break;
				
		}  // of the switch statement
		
	}  // of an UNK event.


	if( lerror )
	{
		// 2/3/2017 rmd : Okay well the mode is no longer 'connecting'. And it shouldn't be 'ready'.
		// So set it to 'waiting to connect'. Doing so ensures when the connection timer fires and
		// we try to start a connection sequence we really to start it. If the mode were still
		// 'connecting' it is possible to not re-start the sequence.
		cics.mode = CI_MODE_waiting_to_connect;
		
		// 10/29/2016 rmd : Send the connection error event. To start the sequence over.
		CONTROLLER_INITIATED_post_event( CI_EVENT_connection_process_error );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

