/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_LINK_TEST_H
#define _INC_LINK_TEST_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"lpc_types.h"

#include	"general_picked_support.h"

#include	"packet_definitions.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define RADIO_TEST_EVENT_start_the_test					    (0x1000)

#define RADIO_TEST_EVENT_build_next_message				    (0x2000)

#define RADIO_TEST_EVENT_response_timeout				    (0x3000)

#define RADIO_TEST_EVENT_bad_crc						    (0x3500)

#define RADIO_TEST_EVENT_here_is_an_inbound_echo_to_process (0x4000)

#define RADIO_TEST_EVENT_stop_the_test					    (0x5000)

#define RADIO_TEST_EVENT_1hz_screen_update                  (0x6000)

#define RADIO_TEST_EVENT_here_is_an_inbound_message_to_echo (0x7000)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 11/27/2017 rmd : Communication port test, between controllers, using a specific port.

typedef struct
{
	// To identify the event.
	UNS_32							event;

	// 6/7/2017 rmd : Include the routing class when used to deliver packets to the task.
	ROUTING_CLASS_DETAILS_STRUCT	message_class;
	
	// 12/11/2017 rmd : To deliver the messages to the task.
	DATA_HANDLE						dh;
	
	// 6/16/2017 rmd : Port the packet arrived on.
	UNS_32							from_port;
	
	ADDR_TYPE						from_to_addr;
	
} RADIO_TEST_TASK_QUEUE_STRUCT;

// ----------

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 11/29/2017 rmd : Test functionality related functions.

extern void RADIO_TEST_if_addressed_to_the_hub_pass_packet_on_to_the_inbound_transport( const UNS_32 pfrom_which_port, const DATA_HANDLE pdh );

extern void RADIO_TEST_echo_inbound_2000e_test_message_back_to_where_it_came_from( const ADDR_TYPE pfrom_to, const UNS_32 pfrom_which_port, const DATA_HANDLE pdh );

// ----------

extern BOOL_32 RADIO_TEST_there_is_a_port_device_to_test( void );

extern void init_radio_test_gui_vars_and_state_machine( void );

extern void RADIO_TEST_task( void *pvParameters );


extern void RADIO_TEST_post_event_with_details( RADIO_TEST_TASK_QUEUE_STRUCT *pq_item_ptr );

extern void RADIO_TEST_post_event( UNS_32 pevent );

// ----------

// 11/29/2017 rmd : GUI related functions.

extern void FDTO_RADIO_TEST_draw_radio_comm_test_screen( const BOOL_32 pcomplete_redraw );

extern void RADIO_TEST_process_radio_comm_test_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

