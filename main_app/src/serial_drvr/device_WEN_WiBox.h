/*  file = device_WIBOX_WiBox.h                 03.22.2016  ajv */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_DEVICE_WIBOX_WIBOX_H
#define INC_DEVICE_WIBOX_WIBOX_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"device_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// PuTTY output:
#if 0
*** Lantronix WiBox Device Server ***
MAC address 00204AB3D4DA
Software version V6.8.0.4 (130110) 
AES library version 1.8.2.1

Press Enter for Setup Mode 				<- CRLF


*** basic parameters 
Hardware: Ethernet TPI, WLAN 802.11bg
Network mode: Wireless Only
IP addr - 0.0.0.0/DHCP/BOOTP/AutoIP, no gateway set,netmask 255.255.255.0
DNS Server not set
DHCP device name : not set
DHCP FQDN option: Disabled

*** Security
SNMP is              enabled
SNMP Community Name: public
Telnet Setup is      enabled
TFTP Download is     enabled
Port 77FEh is        enabled
Web Server is        enabled
Web Setup is         enabled
ECHO is              disabled
Encryption is        disabled
Enhanced Password is disabled


*** Channel 1
Baudrate 19200, I/F Mode 4C, Flow 02
Port 02000
Connect Mode : C0
Send '+++' in Modem Mode enabled
Show IP addr after 'RING' enabled
Auto increment source port disabled
Remote IP Adr: --- none ---, Port 00000
Disconn Mode : 00
Flush   Mode : 33


*** Channel 2
Baudrate 9600, I/F Mode 4C, Flow 00
Port 10002
Connect Mode : C0
Send '+++' in Modem Mode enabled
Show IP addr after 'RING' enabled
Auto increment source port disabled
Remote IP Adr: --- none ---, Port 00000
Disconn Mode : 00
Flush   Mode : 00


*** Expert
TCP Keepalive    : 45s
ARP cache timeout: 600s
CPU performance: Regular
Monitor Mode @ bootup : enabled
HTTP Port Number : 80
MTU Size: 1400
TCP Re-transmission timeout: 500 ms
Alternate MAC: disabled
Ethernet connection type: auto-negotiate

*** WLAN 
WLAN: enabled
Topology: Infrastructure
Network name: calsense_hq
Country: US
Security suite: WPA2/802.11i
Authentication: PSK
Encryption: TKIP
TX Data rate: 54 Mbps auto fallback
Minimum TX Data rate: 1 Mbps
Power management: disabled
Soft AP Roaming: disabled
WLAN Max failed packets: 6

Change Setup:
  0 Server
  1 Channel 1
  2 Channel 2
  4 WLAN
  5 Expert
  6 Security
  7 Defaults
  8 Exit without save
  9 Save and exit            Your choice ? 0

Network mode: 0=Wired Only, 1=Wireless Only, 2=Bridging(One Host) (1) ? 


IP Address : (000) .(000) .(000) .(000) 
Set Gateway IP Address (N) ? 
Netmask: Number of Bits for Host Part (0=default) (8) 
Set DNS Server IP addr  (N) ? 
Change Telnet/Web Manager password (N) ? 
Change DHCP device name (not set) ? (N) ? 
Enable DHCP FQDN option :  (N) ? 


Change Setup:
  0 Server
  1 Channel 1
  2 Channel 2
  4 WLAN
  5 Expert
  6 Security
  7 Defaults
  8 Exit without save
  9 Save and exit            Your choice ? 1

Network mode: 0=Wired Only, 1=Wireless Only, 2=Bridging(One Host) (1) ? 


IP Address : (000) .(000) .(000) .(000) 
Set Gateway IP Address (N) ? 
Netmask: Number of Bits for Host Part (0=default) (8) 
Set DNS Server IP addr  (N) ? 
Change Telnet/Web Manager password (N) ? 
Change DHCP device name (not set) ? (N) ? 
Enable DHCP FQDN option :  (N) ? 


Change Setup:
  0 Server
  1 Channel 1
  2 Channel 2
  4 WLAN
  5 Expert
  6 Security
  7 Defaults
  8 Exit without save
  9 Save and exit            Your choice ? 4

Topology: 0=Infrastructure, 1=Ad-Hoc (0) ? 
Network name (SSID) (calsense_hq) ? 
Security suite: 0=none, 1=WEP, 2=WPA, 3=WPA2/802.11i (3) ? 
Change Key (N) ? 
Encryption: 0=CCMP, 1=CCMP+TKIP, 2=CCMP+WEP, 3=TKIP, 4=TKIP+WEP (3) ? 
TX Data rate: 0=fixed, 1=auto fallback (1) ? 
TX Data rate: 0=1, 1=2, 2=5.5, 3=11, 4=18, 5=24, 6=36, 7=54 Mbps (7) ? 
Minimum TX Data rate: 0=1, 1=2, 2=5.5, 3=11, 4=18, 5=24, 6=36, 7=54 Mbps (0) ? 
Enable power management (N) ? 
Enable Soft AP Roaming (N) ? 
Max failed packets (6 - 64; 255=disable):  (6) ? 


Change Setup:
  0 Server
  1 Channel 1
  2 Channel 2
  4 WLAN
  5 Expert
  6 Security
  7 Defaults
  8 Exit without save
  9 Save and exit            Your choice ? 

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/23/2016 ajv : WiBox State Machine States.
#define WIBOX_DEVEX_READ					( 4000 )
#define WIBOX_DEVEX_WRITE					( 5000 )
#define WIBOX_DEVEX_FINISH					( 6000 )

// ----------

#define WIBOX_TICKS_PER_MS					( 5 )

// ----------

// 4/23/2015 mpd : The initial large response from the Lantronix is 1053 bytes. At 9600
// baud, that's 1.097 seconds to receive our expected termination string! Add 10% for
// possible errors. That's 1.2 seconds.
//#define WIBOX_COMM_TIMER_MS				( 1200 )
// 5/20/2015 mpd : We are getting timeouts in the field on the first command. Wait longer.
#define WIBOX_COMM_TIMER_MS					( 10000 )

// ----------

#define WIBOX_NULL_TERM_LEN					( 1 )
#define WIBOX_EMPTY_STR						( 0xFF ) // Special case for initial I/O cycle. 
#define WIBOX_EMPTY_STR_LEN					( 1 ) 	 // Length > 0 is required. 
#define WIBOX_CRLF_STR						( "\r\n" )
#define WIBOX_CRLF_STR_LEN					( 2 + WIBOX_NULL_TERM_LEN )
#define WIBOX_CR_STR						( "\r" )
#define WIBOX_CR_STR_LEN					( 1 + WIBOX_NULL_TERM_LEN )
#define WIBOX_LF_STR						( "\n" )
#define WIBOX_LF_STR_LEN					( 1 + WIBOX_NULL_TERM_LEN )
#define WIBOX_NULL_IP_ADD_STR		 		( "0.0.0.0" ) 
#define WIBOX_NULL_IP_OCTET_STR		 		( "0" ) 
#define WIBOX_NULL_IP_ADD_LEN				( 7 + WIBOX_NULL_TERM_LEN ) 
#define WIBOX_IP_ADD_LEN 			 		( 15 + WIBOX_NULL_TERM_LEN ) 
#define WIBOX_IP_ADDRESS_OCTET_LEN			( 3 + WIBOX_CRLF_STR_LEN )
#define WIBOX_MASK_BITS_LEN				 	( 2 + WIBOX_CRLF_STR_LEN )

// ----------

// 3/23/2016 ajv : The WiBox, like the UDS-1100, requires a default mask for device
// responses that do not include the field (FogBugz Case 3178)
#define WIBOX_DEFAULT_IP_ADD_STR			 ( "255.255.255.0" ) 

// ----------

#define WIBOX_PVS_TOKEN_STR					( "PVS" )
#define WIBOX_PVS_TOKEN_LEN    				( 4 )

#define WIBOX_READ_MONITOR_OPERATION		( 77 )		// "M"

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/27/2015 mpd : NOTE: Most of the Lantronix programming command strings require a CR
// following the text. A CRLF does not work!. This EN programming code requires the null
// termination to make life simpler when dealing with strings.

// 4/17/2015 mpd : This is the SETUP MODE response. It is entered with a power cycle
// followed by "xxx". There are four lines of immediate response before the device requires
// a "CRLF" to continue.
 
#if 0
*** Lantronix WiBox Device Server ***
MAC address 00204AB3D4DA
Software version V6.8.0.4 (130110) 
AES library version 1.8.2.1

Press Enter for Setup Mode 				<- CRLF


*** basic parameters 
Hardware: Ethernet TPI, WLAN 802.11bg
Network mode: Wireless Only
IP addr - 0.0.0.0/DHCP/BOOTP/AutoIP, no gateway set,netmask 255.255.255.0
DNS Server not set								// Added in V6.8.0.4
DHCP device name : not set
DHCP FQDN option: Disabled						// Added in V6.8.0.4

01234567890123456789012345678901234567890123456789012345678901234567890	<- Ruler
#endif

#define WIBOX_SERVER_INFO_DEVICE_STR		   	"*** Lantronix"
#define WIBOX_SERVER_INFO_DEVICE_OFFSET	    	( 14 )
#define WIBOX_SERVER_INFO_DEVICE_LEN		   	( 23 + WIBOX_NULL_TERM_LEN )	// This is the value currently in use.

#define WIBOX_SERVER_INFO_TITLE_STR		    	"*** Lantronix WiBox Device Server ***"

#define WIBOX_SERVER_INFO_MAC_ADD_STR			"MAC address"
#define WIBOX_SERVER_INFO_MAC_ADD_OFFSET		( 12 )
#define WIBOX_SERVER_INFO_MAC_ADD_LEN			( 12 + WIBOX_NULL_TERM_LEN )	

// 3/23/2016 ajv : Software Version contains 3 space-delimited fields. We only want the
// first one. The current value has 8 characters plus null. Save space for each of the 4
// numbers to be 3 digits long.
#define WIBOX_SERVER_INFO_SW_VERSION_STR		"Software version"		
#define WIBOX_SERVER_INFO_SW_VERSION_OFFSET		( 17 )
#define WIBOX_SERVER_INFO_SW_VERSION_LEN		( 16 + WIBOX_NULL_TERM_LEN )	

#define WIBOX_SERVER_INFO_SETUP_STR				"Mode"	// "Press Enter for Setup Mode"

#define WIBOX_BASIC_INFO_TITLE_STR		 	   	"*** basic parameters"

#define WIBOX_BASIC_INFO_IP_ADD_STR  			"IP addr "
#define WIBOX_BASIC_INFO_IP_ADD_HYPHEN_STR		"-"
#define WIBOX_BASIC_INFO_IP_ADD_OFFSET 			( 8 )
#define WIBOX_BASIC_INFO_IP_ADD_HYPHEN_OFFSET 	( 10 )
#define WIBOX_BASIC_INFO_IP_ADD_LEN  			( 15 + WIBOX_NULL_TERM_LEN )	// This is the MAXIMUM value! Shorter addresses will have to be parsed. 

#define WIBOX_BASIC_INFO_GW_ADD_STR  			", gateway"				 
#define WIBOX_BASIC_INFO_GW_ADD_OFFSET 			( 10 )
#define WIBOX_BASIC_INFO_GW_ADD_LEN  			( 15 + WIBOX_NULL_TERM_LEN )	// This is the MAXIMUM value! Shorter addresses will have to be parsed. 

#define WIBOX_BASIC_INFO_GW_NOT_SET_STR			"no gateway set"	 

#define WIBOX_BASIC_INFO_MASK_STR  				",netmask"		 
#define WIBOX_BASIC_INFO_MASK_OFFSET	 		( 9 )
#define WIBOX_BASIC_INFO_MASK_LEN  				( 15 + WIBOX_NULL_TERM_LEN )	// This is the MAXIMUM value! Shorter addresses will have to be parsed. 

#define WIBOX_BASIC_INFO_DHCP_NAME_STR  		"DHCP device name :"
#define WIBOX_BASIC_INFO_DHCP_NAME_OFFSET 		( 19 )
#define WIBOX_BASIC_INFO_DHCP_NAME_LEN  		( 16 + WIBOX_NULL_TERM_LEN )	// Length matches GUIVar length. 

#define WIBOX_BASIC_INFO_NAME_NOT_SET_STR		"not set"	 
#define WIBOX_BASIC_INFO_NAME_NOT_SET_LEN		( 7 )	 

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#if 0
// Server Menu
IP Address : (000) 0.(000) 0.(005) 4.(000) 0
Set Gateway IP Address (N) ? N
Netmask: Number of Bits for Host Part (0=default) (8) 8
Set DNS Server IP addr  (N) ?											// Added in V6.8.0.4
Change Telnet/Web Manager password (N) ? N
Change DHCP device name (not set) ? (N) ? Y								// Only for DHCP enabled
Enter new DHCP device Name : 00204AD7162D								// Only for DHCP enabled
Enable DHCP FQDN option :  (N) ?										// Added in V6.8.0.4

01234567890123456789012345678901234567890123456789012345678901234567890	<- Ruler
#endif

#define WIBOX_SERVER_INPUT_NETWORK_MODE_STR		"Network mode:"
#define WIBOX_SERVER_INPUT_IP_ADD_STR 			"IP Address :"
#define WIBOX_SERVER_INPUT_GATEWAY_YN_STR		"Set Gateway IP Address"
#define WIBOX_SERVER_INPUT_GATEWAY_IP_STR		"Gateway IP addr"
#define WIBOX_SERVER_INPUT_NETMASK_STR			"Netmask: Number of Bits for Host Part (0=default)"
#define WIBOX_SERVER_INPUT_DHCP_NAME_YN_STR		"Change DHCP device name"
#define WIBOX_SERVER_INPUT_DHCP_NAME_STR		"Enter new DHCP device Name"
#define WIBOX_SERVER_INPUT_IP_ADD_RESP_STR		")"
#define WIBOX_SERVER_INPUT_QUESTION_STR			"?"
#define WIBOX_SERVER_INPUT_COLON_STR			":"

// 3/23/2016 ajv : Lantronix changed the text of the Telnet question in V6.8.0.4. Search for
// the text that is common.
#define WIBOX_SERVER_INPUT_TELNET_YN_STR 		"password (N)"								// Works for both

// 3/23/2016 ajv : Firmware V6.8.0.4 added a question following NetMask. 
#define WIBOX_SERVER_INPUT_DNS_IP_YN_STR		"Set DNS Server IP addr "

// 3/23/2016 ajv : Firmware V6.8.0.4 added a question following DHCP name. 
#define WIBOX_SERVER_INPUT_FQDN_YN_STR			"Enable DHCP FQDN"

// 4/29/2015 mpd : Programming input strings need an extra character for the CR.
#define WIBOX_SERVER_INPUT_IP_ADD_LEN 			( 15 + WIBOX_CR_STR_LEN )	// This is the MAXIMUM value! Shorter addresses will have to be parsed. 
#define WIBOX_SERVER_INPUT_GW_ADD_LEN  			( 15 + WIBOX_CR_STR_LEN )	// This is the MAXIMUM value! Shorter addresses will have to be parsed. 
#define WIBOX_SERVER_INPUT_MASK_LEN  			( 15 + WIBOX_CR_STR_LEN )	// This is the MAXIMUM value! Shorter addresses will have to be parsed. 
#define WIBOX_SERVER_INPUT_DHCP_NAME_LEN  		( 16 + WIBOX_CR_STR_LEN )	// Length matches GUIVar length. 

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#if 0
*** Channel 1
Baudrate 115200, I/F Mode 4C, Flow 02
Port 10001
Connect Mode : C5
Send '+++' in Modem Mode enabled
Show IP addr after 'RING' enabled				// Added in V6.8.0.4
Auto increment source port enabled
Remote IP Adr: 64.73.242.99, Port 16001
Disconn Mode : 80
Flush   Mode : 00

01234567890123456789012345678901234567890123456789012345678901234567890	<- Ruler
#endif

#define WIBOX_CHANNEL_INFO_TITLE_STR		   	"*** Channel 1"

// 4/17/2015 mpd : Lantronix uses the MAC address as the device serial number.
#define WIBOX_CHANNEL_INFO_BAUD_RATE_STR		"Baudrate"
#define WIBOX_CHANNEL_INFO_BAUD_RATE_OFFSET		( 9 )
#define WIBOX_CHANNEL_INFO_BAUD_RATE_LEN		( 6 + WIBOX_NULL_TERM_LEN )	// This is the MAXIMUM value! Shorter values will have to be parsed.

#define WIBOX_CHANNEL_INFO_IF_MODE_STR			"I/F Mode"
#define WIBOX_CHANNEL_INFO_IF_MODE_OFFSET		( 9 )
#define WIBOX_CHANNEL_INFO_IF_MODE_LEN			( 2 + WIBOX_NULL_TERM_LEN )	

#define WIBOX_CHANNEL_INFO_FLOW_STR				"Flow"
#define WIBOX_CHANNEL_INFO_FLOW_OFFSET			( 5 )
#define WIBOX_CHANNEL_INFO_FLOW_LEN				( 2 + WIBOX_NULL_TERM_LEN )	

#define WIBOX_CHANNEL_INFO_PORT_STR				"Port"
#define WIBOX_CHANNEL_INFO_PORT_OFFSET			( 5 )
#define WIBOX_CHANNEL_INFO_PORT_LEN				( 5 + WIBOX_NULL_TERM_LEN )	// This is the MAXIMUM value! Shorter values will have to be parsed.

#define WIBOX_CHANNEL_INFO_CONN_MODE_STR		"Connect"
#define WIBOX_CHANNEL_INFO_CONN_MODE_OFFSET		( 15 )
#define WIBOX_CHANNEL_INFO_CONN_MODE_LEN		( 2 + WIBOX_NULL_TERM_LEN )	

#define WIBOX_CHANNEL_INFO_AUTO_INCR_STR		"Auto increment source port"
#define WIBOX_CHANNEL_INFO_AUTO_INCR_OFFSET		( 27 )
#define WIBOX_CHANNEL_INFO_AUTO_INCR_LEN		( 8 + WIBOX_NULL_TERM_LEN )	

#define WIBOX_CHANNEL_INFO_ENABLED_STR			"enabled"
#define WIBOX_CHANNEL_INFO_ENABLED_LEN			( 7 + WIBOX_NULL_TERM_LEN )
#define WIBOX_CHANNEL_INFO_DISABLED_STR			"disabled"
#define WIBOX_CHANNEL_INFO_DISABLED_LEN			( 8 + WIBOX_NULL_TERM_LEN )

#define WIBOX_CHANNEL_INFO_REMOTE_IP_STR		"Remote IP Adr:"
#define WIBOX_CHANNEL_INFO_REMOTE_IP_OFFSET		( 15 )
#define WIBOX_CHANNEL_INFO_REMOTE_IP_LEN		( 15 + WIBOX_NULL_TERM_LEN )	// This is the MAXIMUM value! Shorter addresses will have to be parsed.

#define WIBOX_CHANNEL_INFO_RIP_NOT_SET_STR		"--- none ---"	 

// 4/23/2015 mpd : There are two "Port" anchor keys in this response section. Use the ", " to differentiate!
#define WIBOX_CHANNEL_INFO_REMOTE_PORT_STR		", Port"
#define WIBOX_CHANNEL_INFO_REMOTE_PORT_OFFSET	( 7 )
#define WIBOX_CHANNEL_INFO_REMOTE_PORT_LEN		( 5 + WIBOX_NULL_TERM_LEN )	

#define WIBOX_CHANNEL_INFO_DISCONN_MODE_STR		"Disconn Mode :"
#define WIBOX_CHANNEL_INFO_DISCONN_MODE_OFFSET	( 15 )
#define WIBOX_CHANNEL_INFO_DISCONN_MODE_LEN		( 2 + WIBOX_NULL_TERM_LEN )	

#define WIBOX_CHANNEL_INFO_FLUSH_MODE_STR		"Flush   Mode :"
#define WIBOX_CHANNEL_INFO_FLUSH_MODE_OFFSET	( 15 )
#define WIBOX_CHANNEL_INFO_FLUSH_MODE_LEN		( 2 + WIBOX_NULL_TERM_LEN )	

#if 0
// Channel 1 Menu
Baudrate (115200) ? 115200
I/F Mode (4C) ? 4C
Flow (02) ? 02
Port No (10001) ? 10001
ConnectMode (C5) ? C5
Send '+++' in Modem Mode  (Y) ? Y
Show IP addr after 'RING'  (Y) ?							// Added in 6.8.0.4
Auto increment source port  (Y) ? Y
Remote IP Address : (192) 64.(168) 73.(254) 242.(137) 99
Remote Port  (16001) ? 16001
DisConnMode (80) ? 80
FlushMode   (00) ? 00
DisConnTime (00:00) ?00:00
SendChar 1  (00) ? 00
SendChar 2  (00) ? 00

01234567890123456789012345678901234567890123456789012345678901234567890	<- Ruler
#endif

#define WIBOX_CHANNEL_INPUT_BAUD_RATE_STR 		"Baudrate"
#define WIBOX_CHANNEL_INPUT_IF_MODE_STR 		"I/F Mode"
#define WIBOX_CHANNEL_INPUT_FLOW_STR 			"Flow"
#define WIBOX_CHANNEL_INPUT_PORT_STR 			"Port No"
#define WIBOX_CHANNEL_INPUT_CONN_MODE_STR 		"ConnectMode"
#define WIBOX_CHANNEL_INPUT_SEND_STR 			"Send '+++' in Modem Mode"
#define WIBOX_CHANNEL_INPUT_SHOW_IP_RING_STR	"Show IP addr after"			// "Show IP addr after 'RING'  (N)"
#define WIBOX_CHANNEL_INPUT_AUTO_INCR_STR 		"Auto increment source port"
#define WIBOX_CHANNEL_INPUT_REMOTE_IP_STR  		"Remote IP Address"
#define WIBOX_CHANNEL_INPUT_REMOTE_PORT_STR		"Remote Port"
#define WIBOX_CHANNEL_INPUT_DISCONN_MODE_STR	"DisConnMode" 
#define WIBOX_CHANNEL_INPUT_FLUSH_MODE_STR     	"FlushMode"
#define WIBOX_CHANNEL_INPUT_DISCONN_TIME_STR    "DisConnTime"
#define WIBOX_CHANNEL_INPUT_SEND_CHAR_1_STR     "SendChar 1"
#define WIBOX_CHANNEL_INPUT_SEND_CHAR_2_STR     "SendChar 2"
#define WIBOX_CHANNEL_INPUT_IP_ADD_RESP_STR		")"
#define WIBOX_CHANNEL_INPUT_QUESTION_STR		"?"
#define WIBOX_CHANNEL_INPUT_COLON_STR			":"
 
// 3/31/2016 ajv : Programming input strings need an extra character for the CR.
#define WIBOX_CHANNEL_INPUT_BAUD_RATE_LEN 		( 6 + WIBOX_CR_STR_LEN )
#define WIBOX_CHANNEL_INPUT_IF_MODE_LEN			( 2 + WIBOX_CR_STR_LEN )	
#define WIBOX_CHANNEL_INPUT_FLOW_LEN			( 2 + WIBOX_CR_STR_LEN )	
#define WIBOX_CHANNEL_INPUT_PORT_LEN			( 5 + WIBOX_CR_STR_LEN )	// This is the MAXIMUM value! Shorter values will have to be parsed.
#define WIBOX_CHANNEL_INPUT_CONN_MODE_LEN		( 2 + WIBOX_CR_STR_LEN )	
#define WIBOX_CHANNEL_INPUT_AUTO_INCR_LEN		( 8 + WIBOX_CR_STR_LEN )	
#define WIBOX_CHANNEL_INPUT_REMOTE_IP_LEN		( 15 + WIBOX_CR_STR_LEN )	// This is the MAXIMUM value! Shorter addresses will have to be parsed.
#define WIBOX_CHANNEL_INPUT_REMOTE_PORT_LEN		( 5 + WIBOX_CR_STR_LEN )	
#define WIBOX_CHANNEL_INPUT_DISCONN_MODE_LEN	( 2 + WIBOX_CR_STR_LEN )	
#define WIBOX_CHANNEL_INPUT_FLUSH_MODE_LEN		( 2 + WIBOX_CR_STR_LEN )	
 
// ----------

#define WIBOX_WLAN_INPUT_TOPOLOGY				"Topology"
#define WIBOX_WLAN_INPUT_SSID					"Network name"
#define WIBOX_WLAN_INPUT_SECURITY_SUITE			"Security suite"

#define WIBOX_WLAN_INPUT_WEP_AUTH				"Authentication"
#define WIBOX_WLAN_INPUT_WEP_ENCRYPTION			"Encryption"
#define WIBOX_WLAN_INPUT_WEP_CHANGE_KEY			"Change Key"
#define WIBOX_WLAN_INPUT_WEP_DISPLAY_KEY		"Display key"
#define WIBOX_WLAN_INPUT_WEP_KEY_TYPE			"Key type"
#define WIBOX_WLAN_INPUT_WEP_KEY				"Enter Key"
#define WIBOX_WLAN_INPUT_WEP_TX_KEY_INDEX		"TX Key index"

#define WIBOX_WLAN_INPUT_WPA_CHANGE_KEY			"Change Key"
#define WIBOX_WLAN_INPUT_WPA_DISPLAY_KEY		"Display key"
#define WIBOX_WLAN_INPUT_WPA_KEY_TYPE			"Key type"
#define WIBOX_WLAN_INPUT_WPA_KEY				"Enter Key"
#define WIBOX_WLAN_INPUT_WPA_ENCRYPTION			"Encryption"

#define WIBOX_WLAN_INPUT_WPA2_CHANGE_KEY		"Change Key"
#define WIBOX_WLAN_INPUT_WPA2_DISPLAY_KEY		"Display key"
#define WIBOX_WLAN_INPUT_WPA2_KEY_TYPE			"Key type"
#define WIBOX_WLAN_INPUT_WPA2_KEY				"Enter Key"
#define WIBOX_WLAN_INPUT_WPA2_ENCRYPTION		"Encryption"

#define WIBOX_WLAN_INPUT_TX_DATA_RATE_1			"TX Data rate: 0=fixed"
#define WIBOX_WLAN_INPUT_TX_DATA_RATE_2			"TX Data rate: 0=1"
#define WIBOX_WLAN_INPUT_MIN_TX_DATA_RATE		"Minimum TX Data rate"
#define WIBOX_WLAN_INPUT_POWER_MGMT				"Enable power"
#define WIBOX_WLAN_INPUT_SOFT_AP_ROAMING		"Enable Soft"
#define WIBOX_WLAN_INPUT_MAX_FAILED_PACKETS		"Max failed"
 
 
#define WIBOX_WLAN_INPUT_QUESTION_STR			"?"
#define WIBOX_WLAN_INPUT_COLON_STR				":"
 
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define WIBOX_SSID_LEN 							( 32 + WIBOX_NULL_TERM_LEN )
#define WIBOX_KEY_LEN 							( 64 + DEV_NULL_LEN )
#define WIBOX_PASS_PHRASE_LEN					( 63 + DEV_NULL_LEN )

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#if 0
Change Setup:
  0 Server
  1 Channel 1
  2 Channel 2
  4 WLAN
  5 Expert
  6 Security
  7 Defaults
  8 Exit without save
  9 Save and exit            Your choice ?
#endif

#define WIBOX_CHANGE_MENU_TITLE_STR			"Change Setup:"
#define WIBOX_CHANGE_MENU_CHOICE_STR		"Your choice ?"
#define WIBOX_CHANGE_MENU_EXIT_WO_STR		"exiting without save !"
#define WIBOX_CHANGE_SUCCESS_STR			"Parameters stored ..."
#define WIBOX_NO_TEXT_STR					""

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/23/2016 ajv : This struct needs to be named for the forward declaration needed by
// DEV_STATE_STRUCT to work.
typedef struct WIBOX_PROGRAMMABLE_VALUES_STRUCT
{
	char pvs_token[ WIBOX_PVS_TOKEN_LEN ];

	// 3/23/2016 ajv : These device parameters are read from the SETUP MODE response.
	char ip_address[ WIBOX_SERVER_INPUT_IP_ADD_LEN ];  
	char ip_address_1[ WIBOX_IP_ADDRESS_OCTET_LEN ];            
	char ip_address_2[ WIBOX_IP_ADDRESS_OCTET_LEN ];            
	char ip_address_3[ WIBOX_IP_ADDRESS_OCTET_LEN ];            
	char ip_address_4[ WIBOX_IP_ADDRESS_OCTET_LEN ];            
	char dhcp_name[ WIBOX_SERVER_INPUT_DHCP_NAME_LEN ];                      

	char baud_rate[ WIBOX_CHANNEL_INPUT_BAUD_RATE_LEN ];
	char if_mode[ WIBOX_CHANNEL_INPUT_IF_MODE_LEN ];
	char flow[ WIBOX_CHANNEL_INPUT_FLOW_LEN ];
	char port[ WIBOX_CHANNEL_INPUT_PORT_LEN ];
	char conn_mode[ WIBOX_CHANNEL_INPUT_CONN_MODE_LEN ];
	BOOL_32 auto_increment;

	// 3/23/2016 ajv : Whole IP addresses are read from the device and used in decision making.
	// Octets are set by individual GUI fields and are used primarily to program the device.
	// It's important to keep both types of strings in sync.
	char remote_ip_address[ WIBOX_CHANNEL_INPUT_REMOTE_IP_LEN ];              
	char remote_ip_address_1[ WIBOX_IP_ADDRESS_OCTET_LEN ];            
	char remote_ip_address_2[ WIBOX_IP_ADDRESS_OCTET_LEN ];            
	char remote_ip_address_3[ WIBOX_IP_ADDRESS_OCTET_LEN ];            
	char remote_ip_address_4[ WIBOX_IP_ADDRESS_OCTET_LEN ];            

	char remote_port[ WIBOX_CHANNEL_INPUT_REMOTE_PORT_LEN ];
	char disconn_mode[ WIBOX_CHANNEL_INPUT_DISCONN_MODE_LEN ];
	char flush_mode[ WIBOX_CHANNEL_INPUT_FLUSH_MODE_LEN ];

	char gw_address[ WIBOX_SERVER_INPUT_GW_ADD_LEN ];              
	char gw_address_1[ WIBOX_IP_ADDRESS_OCTET_LEN ];            
	char gw_address_2[ WIBOX_IP_ADDRESS_OCTET_LEN ];            
	char gw_address_3[ WIBOX_IP_ADDRESS_OCTET_LEN ];            
	char gw_address_4[ WIBOX_IP_ADDRESS_OCTET_LEN ]; 
	           
	// 9/24/2015 mpd : FogBugz #3178: The mask string is needed after all (for determining the
	// mask bit count after device read operations)! Reactivate this field! Leave the octets
	// commented out.
	char mask[ WIBOX_SERVER_INPUT_MASK_LEN ]; 

	// ----------
	// WLAN Settings

	char ssid[ WIBOX_SSID_LEN ];

	char key[ WIBOX_KEY_LEN ];

	char passphrase[ WIBOX_PASS_PHRASE_LEN ];

	UNS_32 w_key_type;

	UNS_32 security;	// 0=none, 1=WEP, 2=WPA, 3=WPA2/802.11i

	UNS_32 wep_authentication; // 0=open, 1=shared

	UNS_32 wep_key_size;	// 1=WEP64, 2=WEP128

	UNS_32 wpa_encryption;  // 0=TKIP, 1=TKIP+WEP
	UNS_32 wpa2_encryption;	// 0=CCMP, 1=CCMP+TKIP, 2=CCMP+WEP, 3=TKIP, 4=TKIP+WEP

	// 3/29/2016 ajv : For security reasons, several fields cannot be read from the device. That
	// means we cannot blindly write the original values back to the device whn the user hits
	// the "Save" key. Only modify these fields if the GUI explicitly tells us to do so.
	BOOL_32 user_changed_passphrase;
	BOOL_32 user_changed_key;

} WIBOX_PROGRAMMABLE_VALUES_STRUCT;

// ----------

// 3/23/2016 ajv : This struct needs to be named for the forward declaration (needed by
// DEV_STATE_STRUCT) to work.
typedef struct WIBOX_DETAILS_STRUCT
{
	// 9/23/2015 mpd : FogBugz #3164: The Lantronix EN UDS1100 does not use CIDR (Classless
	// Inter Domain Routing) format to define the network mask. The number of bits indicates the
	// available bits for host addressing (e.g. A mask of "8" produces a netmask of
	// "255.255.255.0" which has 8 bits available for hosts on the subnet. Other Lantronix
	// devices use CIDR format for programming (e.g. "192.168.254.150/24" specifies the IP
	// address as well as a network mask of "255.255.255.0"). The CIDR mask bit value indicates
	// the leading ones; not the trailing zeros. The CS3000 GUI code was written for UDS1100.
	// It's backwards for all newer Lantronix devices. The mask numbers are internal to CS3000
	// code. They become external at the GUI and in device programming. To eliminate extensive
	// rework, the GUI side for all device types will retain pre-CIDR mask values. As mask
	// values pass to and from the GUI code to the device code (e.g. "e_wen_programming.c" to
	// "device_WIBOX_PremierWaveXN.c"), the value will be transformed to match the receiving
	// format.
	UNS_32 mask_bits;

	// 6/11/2015 mpd : Scenario: DHCP was enabled (a DHCP name existed), it was disabled, and
	// it's about to be reenabled. We don't want to stomp on a long custom name if we ever knew
	// it. This flag tells us it's safe to stomp on any DHCP name that existed in the device the
	// last time DHCP was enabled. Or conversely, it prevents us from stomping on a name that
	// was temporarily hidden from view during the current session.
	BOOL_32 dhcp_name_not_set;

	// 4/20/2015 mpd : These values are pulled from the SETUP MODE response. 
	char device[ WIBOX_SERVER_INFO_DEVICE_LEN ];  
	char mac_address[ WIBOX_SERVER_INFO_MAC_ADD_LEN ];

} WIBOX_DETAILS_STRUCT;

extern WIBOX_DETAILS_STRUCT WIBOX_details;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern const DEV_MENU_ITEM WIBOX_setup_read_list[]; 

extern const DEV_MENU_ITEM WIBOX_dhcp_write_list[];  

extern const DEV_MENU_ITEM WIBOX_static_write_list[];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
  
extern void WIBOX_copy_active_values_to_static_values( DEV_STATE_STRUCT *dev_state );

// ----------

extern UNS_32 WIBOX_sizeof_setup_read_list( void );

extern UNS_32 WIBOX_sizeof_dhcp_write_list( void );

extern UNS_32 WIBOX_sizeof_static_write_list( void );

// ----------

extern void WIBOX_initialize_detail_struct( DEV_STATE_STRUCT *dev_state );



extern void WIBOX_initialize_the_connection_process( UNS_32 pport );

extern void WIBOX_connection_processing( UNS_32 pevent );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

