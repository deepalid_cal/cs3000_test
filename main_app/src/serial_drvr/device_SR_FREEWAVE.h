/*  file = device_SR_FREEWAVE.h                           rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_DEVICE_SR_FREEWAVE_H
#define INC_DEVICE_SR_FREEWAVE_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// PuTTY output:
/* 
                                MAIN MENU
                      D2 AES Version v10.6.8      08-21-2013
                           902 - 928 MHz
                        Modem Serial Number 861-4576
                          Model Code DV2T
(0)   Set Operation Mode
(1)   Set Baud Rate
(2)   Edit Call Book
(3)   Edit Radio Transmission Characteristics 
(4)   Show Radio Statistics
(5)   Edit MultiPoint Parameters
(6)   TDMA Menu
(8)   Chg Password
(Esc) Exit Setup

=====================================================================================
                    MAIN MENU CHOICE (0)   Set Operation Mode
=====================================================================================

                                SET MODEM MODE
                        Modem Mode is   2

(0)   Point to Point Master
(1)   Point to Point Slave
(2)   Point to MultiPoint Master
(3)   Point to MultiPoint Slave
(4)   Point to Point Slave/Repeater
(5)   Point to Point Repeater
(6)   Point to Point Slave/Master Switchable
(7)   Point to MultiPoint Repeater
(A)   Mirrorbit Master
(B)   Mirrorbit Slave
(F)   Ethernet Options
(Esc) Exit to Main Menu

=====================================================================================
                    MAIN MENU CHOICE (1)   Set Baud Rate
=====================================================================================

                                SET BAUD RATE
                        Modem Baud is  019200

(0)   230,400
(1)   115,200
(2)   76,800
(3)   57,600
(4)   38,400
(5)   19,200
(6)   9,600
(7)   4,800
(8)   2,400
(9)   1,200
(A)   Data, Parity  0
(B)   MODBus RTU    0
(C)   RS232/485     0
(D)   Setup Port    3
(E)   TurnOffDelay  0      TurnOnDelay   0
(F)   FlowControl   1
(G)   Use break to access setup          0
(Esc) Exit to Main Menu

=====================================================================================
                    MAIN MENU CHOICE (2)   Edit Call Book
=====================================================================================

                                MODEM CALL BOOK
                                Entry to Call is 00
Entry    Number     Repeater1    Repeater2
(0)     868-0070
(1)     000-0000
(2)     000-0000
(3)     000-0000
(4)     000-0000
(5)     000-0000
(6)     000-0000
(7)     000-0000
(8)     000-0000
(9)     000-0000
(C)     Change Entry to Use (0-9) or A(ALL)
(Esc)   Exit to Main Menu
Enter all zeros (000-0000) as your last number in list

=====================================================================================
                    MAIN MENU CHOICE (3)   Edit Radio Transmission Characteristics
=====================================================================================

                                RADIO PARAMETERS

WARNING: Do not change parameters without reading manual

(0)   FreqKey          4
(1)   Max Packet Size  7
(2)   Min Packet Size  8
(3)   Xmit Rate        1
(4)   RF Data Rate     3
(5)   RF Xmit Power   10
(6)   Slave Security   0
(7)   RTS to CTS       0
(8)   Retry Time Out 255
(9)   Lowpower Mode    0
(A)   High Noise       0
(B)   MCU Speed        0
(C)   RemoteLED        0
(D)
(E)
(Esc) Exit to Main Menu

=====================================================================================
                    MAIN MENU CHOICE (4)   Show Radio Statistics
=====================================================================================

                                MODEM STATISTICS

Master-Slave Distance(m) 0062464

Number of Disconnects     0
Radio Temperature        17
Antenna Reflected Power   0
         Local  Remote1 Remote2 Remote3
         J dBm    dBm     dBm     dBm
Noise    0 120
Signal   0 234
Rate %     0
0000 00 00

Press <Ret> for Freq Table, <Esc> to return to main menu

=====================================================================================
          IN MENU CHOICE (4)   Show Radio Statistics - RETURN PRESSED
=====================================================================================

                               Stats vs Frequency
Freq     0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15
Noise    0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
Signal   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
% Rcv    0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0

Freq    16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31
Noise    0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
Signal   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
% Rcv    0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0

Freq    32  33  34  35  36  37  38  39  40  41  42  43  44  45  46  47
Noise    0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
Signal   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
% Rcv    0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0

Freq    48  49  50  51  52  53  54  55  56  57  58  59  60  61  62  63
Noise    0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
Signal   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
% Rcv    0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0

Freq    64  65  66  67  68  69  70  71  72  73  74  75  76  77  78  79
Noise    0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
Signal   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
% Rcv    0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0

Freq    80  81  82  83  84  85  86  87  88  89  90  91  92  93  94  95
Noise    0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
Signal   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
% Rcv    0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0

Freq    96  97  98  99 100 101 102 103 104 105 106 107 108 109 110 111
Noise    0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
Signal   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
% Rcv    0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0

=====================================================================================
                    MAIN MENU CHOICE (5)   Edit MultiPoint Parameters
=====================================================================================

                                MULTIPOINT PARAMETERS


(0)   Number Repeaters      1
(1)   Master Packet Repeat  5
(2)   Max Slave Retry       5
(3)   Retry Odds            5
(4)   DTR Connect           0
(5)   Repeater Frequency    0
(6)   NetWork ID         0110
(7)   Reserved
(8)   MultiMasterSync       0
(9)   1 PPS Enable/Delay  255
(A)   Slave/Repeater        0
(B)   Diagnostics          32
(C)   SubNet ID          Roaming
(D)   Radio ID           Not Set
(E)   Local Access         0
(F)
(G)   Radio Name
(Esc) Exit to Main Menu

=====================================================================================
                    MAIN MENU CHOICE (6)   TDMA Menu
=====================================================================================

Enter Choice  TDMA is not installed

=====================================================================================
                    MAIN MENU CHOICE (8)   Chg Password
=====================================================================================

����
New PW? (<esc> to exit)
 
*/ // End of PuTTY output.

// 3/13/2015 mpd : These were created early in development. They will likely not be needed.
// 2/27/2015 mpd : Create complete define lists for all Freewave menus and options. Individual items may or may not get used.
/*
enum main_menu_item_t {
	mmi_set_operation_mode                      = '0',
	mmi_set_baud_rate                           = '1',
	mmi_edit_call_book                          = '2',
	mmi_edit_radio_transmission_characteristics = '3',
	mmi_show_radio_statistics                   = '4',
	mmi_edit_multipoint_parameters              = '5',
	mmi_tdma_menu                               = '6',
	mmi_unused_7                                = '7',
	mmi_chg_password                            = '8'
};

enum set_modem_mode_t {
	smmi_point_to_point_master                  = '0',
	smmi_point_to_point_slave                   = '1',
	smmi_point_to_multipoint_master             = '2',
	smmi_point_to_multipoint_slave              = '3',
	smmi_point_to_point_slave_repeater          = '4',
	smmi_point_to_point_repeater                = '5',
	smmi_point_to_point_slave_master_switchable = '6',
	smmi_point_to_multipoint_repeater           = '7',
	smmi_unused_8                               = '8',
	smmi_unused_9                               = '9',
	smmi_mirrorbit_master                       = 'A',
	smmi_mirrorbit_slave                        = 'B',
	smmi_ethernet_options                       = 'C'
};

enum set_baud_rate_t {
	sbri_230400                    = '0',
	sbri_115200                    = '1',
	sbri_76800                     = '2',
	sbri_57600                     = '3',
	sbri_38400                     = '4',
	sbri_19200                     = '5',
	sbri_9600                      = '6',
	sbri_4800                      = '7',
	sbri_2400                      = '8',
	sbri_1200                      = '9',
	sbri_data_parity               = 'A',
	sbri_mod_bus_rtu               = 'B',
	sbri_rs232_485                 = 'C',
	sbri_setup_port                = 'D',
	sbri_turn_off_delay            = 'E',
	sbri_flow_control              = 'F',
	sbri_use_break_to_access_setup = 'G'
};

enum modem_call_book_t {
	mcbi_unused_0     = '0',
	mcbi_unused_1     = '1',
	mcbi_unused_2     = '2',
	mcbi_unused_3     = '3',
	mcbi_unused_4     = '4',
	mcbi_unused_5     = '5',
	mcbi_unused_6     = '6',
	mcbi_unused_7     = '7',
	mcbi_unused_8     = '8',
	mcbi_unused_9     = '9',
	mcbi_unused_A     = 'A',
	mcbi_unused_B     = 'B',
	mcbi_change_entry = 'C'
};

enum radio_parameters_t {
	rpi_freqkey           = '0',     
	rpi_max_packet_size   = '1',     
	rpi_min_packet_size   = '2',     
	rpi_xmit_rate         = '3',     
	rpi_rf_data_rate      = '4',     
	rpi_rf_xmit_power     = '5',     
	rpi_slave_security    = '6',     
	rpi_rts_to_cts        = '7',     
	rpi_retry_time_out    = '8',     
	rpi_lowpower_mode     = '9',     
	rpi_high_noise        = 'A',     
	rpi_mcu_speed         = 'B',     
	rpi_remoteled         = 'C'
};

enum modem_statistics_t {
	msi_unused_0 = '0'     
};

enum multipoint_parameters_t {
	mpi_number_repeaters     = '0',
	mpi_master_packet_repeat = '1',
	mpi_max_slave_retry      = '2',
	mpi_retry_odds           = '3',
	mpi_dtr_connect          = '4',
	mpi_repeater_frequency   = '5',
	mpi_network_id           = '6',
	mpi_reserved             = '7',
	mpi_multimastersync      = '8',
	mpi_1_pps_enable_delay   = '9',
	mpi_slave_repeater       = 'A',
	mpi_diagnostics          = 'B',
	mpi_subnet_id            = 'C',
	mpi_radio_id             = 'D', 
	mpi_local_access         = 'E', 
	mpi_unused_f             = 'F', 
	mpi_radio_name           = 'G' 
};

enum tdma_t {
	tdma_unused_0 = '0'     
};

enum change_password_t {
	cpi_unused_0 = '0'     
};

// 2/27/2015 mpd : End of radio define lists. 
*/

// 4/8/2015 mpd : SR State Machine States.
#define SR_DEVEX_INITIALIZE					(3000)
#define SR_DEVEX_READ						(4000)
#define SR_DEVEX_WRITE						(5000)
#define SR_DEVEX_FINISH						(6000)

#define SR_TICKS_PER_MS						( 5 )

// 6/1/2015 mpd : A 1 second COMM_MNGR timer was adequate during development. Circumstances in the field have left 
// support personnel stuck as devices timeout under different conditions. The SR timeout has been increased as a 
// precaution even though no SR devices have experienced this problem.
//#define SR_COMM_TIMER_MS		( 1000 )
#define SR_COMM_TIMER_MS		( 10000 )

// 3/19/2015 mpd : TODO: Add more errors and use them!
#define SR_ERROR_NONE			( 0 )

#define NULL_TERM_LEN (1)

#define SR_IDLE_OPERATION					( 73 )		// "I"
#define SR_READ_OPERATION					( 82 )		// "R"
#define SR_WRITE_OPERATION					( 87 )		// "W"
#define SR_IDLE_TEXT_STR					( "IDLE" )
#define SR_READ_TEXT_STR					( "READ" )
#define SR_WRITE_TEXT_STR					( "WRITE" )
#define SR_OPERATION_STR_LEN				( 5 + NULL_TERM_LEN )
#define SR_INFO_TEXT_LEN					( 39 + NULL_TERM_LEN ) 	// This should match the size of GuiVar_SRInfoText.  
#define SR_PROGRESS_TEXT_LEN				( 39 + NULL_TERM_LEN ) 	// This should match the size of GuiVar_SRProgressText.  
#define SR_ALERT_MESSAGE_LEN				( 60 + NULL_TERM_LEN )  // 60 is about the limit of the alert screen width. More requires scrolling.  

// 4/8/2015 mpd : MENU navigaion commands to send to the SR radio. These are direct ASCII except for the END flag.
#define SR_MENU_CMD_STR_0			            ( 0x30 )
#define SR_MENU_CMD_STR_1			            ( 0x31 )
#define SR_MENU_CMD_STR_2			            ( 0x32 )
#define SR_MENU_CMD_STR_3			            ( 0x33 )
#define SR_MENU_CMD_STR_4			            ( 0x34 )
#define SR_MENU_CMD_STR_5			            ( 0x35 )
#define SR_MENU_CMD_STR_6			            ( 0x36 )
#define SR_MENU_CMD_STR_7			            ( 0x37 )
#define SR_MENU_CMD_STR_8			            ( 0x38 )
#define SR_MENU_CMD_STR_9			            ( 0x39 )
#define SR_MENU_CMD_STR_A			            ( 0x41 )
#define SR_MENU_CMD_STR_B			            ( 0x42 )
#define SR_MENU_CMD_STR_C			            ( 0x43 )
#define SR_MENU_CMD_STR_D			            ( 0x44 )
#define SR_MENU_CMD_STR_E			            ( 0x45 )
#define SR_MENU_CMD_STR_F			            ( 0x46 )
#define SR_MENU_CMD_STR_G			            ( 0x47 )	// Yes, there is a "G" on some SR menus. This is ASCII, not hex!
#define SR_MENU_CMD_STR_ESC			            ( 0x1B )
#define SR_MENU_CMD_END							( 0x5A )	// ASCII "Z". This could be any value not used by the radio.
#define SR_MENU_CMD_LEN				            ( 1 )

// 4/8/2015 mpd : TEXT commands to send to the SR radio. These defines are used by a get function to retrieve the 
// specific ASCII text that needs to be sent to the SR radio.
#define SR_TEXT_CMD_NONE						( 5000 )
#define SR_TEXT_CMD_STR_MODE					( 10000 ) 
#define SR_TEXT_CMD_STR_BAUD_RATE               ( 11000 ) 
#define SR_TEXT_CMD_STR_SETUP_PORT              ( 11001 ) 
#define SR_TEXT_CMD_STR_FLOW_CONTROL            ( 11002 )
#define SR_TEXT_CMD_STR_FREQ_KEY                ( 13001 ) 
#define SR_TEXT_CMD_STR_MAX_PACKET              ( 13002 ) 
#define SR_TEXT_CMD_STR_MIN_PACKET              ( 13003 ) 
#define SR_TEXT_CMD_STR_XMIT_RATE               ( 13004 ) 
#define SR_TEXT_CMD_STR_DATA_RATE               ( 13005 ) 
#define SR_TEXT_CMD_STR_XMT_POWER               ( 13006 ) 
#define SR_TEXT_CMD_STR_LOWPOWER_MODE           ( 13010 ) 
#define SR_TEXT_CMD_STR_NUM_REPEATERS           ( 15001 ) 
#define SR_TEXT_CMD_STR_MASTER_PACKET_REPEAT    ( 15002 ) 
#define SR_TEXT_CMD_STR_MAX_SLAVE_RETRY         ( 15003 ) 
#define SR_TEXT_CMD_STR_RETRY_ODDS              ( 15004 ) 
#define SR_TEXT_CMD_STR_DTR_CONNECT             ( 15005 ) 
#define SR_TEXT_CMD_STR_REPEATER_FREQ           ( 15006 ) 
#define SR_TEXT_CMD_STR_NETWORK_ID              ( 15007 ) 
#define SR_TEXT_CMD_STR_SLAVE_REPEATER          ( 15011 ) 
#define SR_TEXT_CMD_STR_DIAGNOSTICS				( 15012 )
#define SR_TEXT_CMD_STR_SUBNET_ID               ( 15013 )
#define SR_TEXT_CMD_STR_SUBNET_RCV_ID           ( 15014 )
#define SR_TEXT_CMD_STR_SUBNET_XMT_ID           ( 15015 )
#define SR_TEXT_CMD_STR_RADIO_ID                ( 15016 )
#define SR_TEXT_CMD_STR_RADIO_NAME              ( 15017 )
#define SR_TEXT_CMD_STR_LAST	                ( SR_TEXT_CMD_STR_RADIO_NAME )
#define SR_TEXT_CMD_END							( 99999 )

// 3/23/2015 mpd : // Recorded responses from the SR radio have maxed-out at 599 for the MULTIPOINT MENU. A longer radio
// name would add a few bytes as would other fields. Allow some fudge room. NOTE: This is not enough to implement the
// RADIO STATISTICS MENU which is not on our TODO list.
#define SR_MAX_RESP_BUFFER_LEN	( 700 ) 	

// 4/15/2015 mpd : Baud Rate strings.
#define SR_BAUD_RATE_CMD_9600	( "6" )
#define SR_BAUD_RATE_CMD_19200	( "5" )
#define SR_BAUD_RATE_CMD_38400	( "4" )
#define SR_BAUD_RATE_CMD_57600	( "3" )
#define SR_BAUD_RATE_CMD_76800	( "2" )
#define SR_BAUD_RATE_CMD_115200	( "1" )
#define SR_BAUD_RATE_CMD_230400	( "0" )
#define SR_BAUD_RATE_OUT_9600	( "009600" )
#define SR_BAUD_RATE_OUT_19200	( "019200" )
#define SR_BAUD_RATE_OUT_38400	( "038400" )
#define SR_BAUD_RATE_OUT_57600	( "057600" )
#define SR_BAUD_RATE_OUT_76800	( "076800" )
#define SR_BAUD_RATE_OUT_115200	( "115200" )
#define SR_BAUD_RATE_OUT_230400	( "230400" )

#define SR_PROGRAMMING_BAUD_RATE ( 19200 )
 
// 3/13/2015 mpd : These string defines are for "groups". "Group" is a Calsense creation. Each group is a collection of 
// specific network related values for SR radio parameters. The group number is visible on the LCD. The internal radio
// parameters associated with the group are not.
#define SR_FREQ_KEY_0		( "0" )
#define SR_FREQ_KEY_1   	( "1" )
#define SR_FREQ_KEY_2   	( "2" )
#define SR_FREQ_KEY_3   	( "3" )
#define SR_FREQ_KEY_4   	( "4" )
#define SR_FREQ_KEY_5   	( "5" )
#define SR_FREQ_KEY_6   	( "6" )
#define SR_FREQ_KEY_7   	( "7" )
#define SR_FREQ_KEY_8   	( "8" )
#define SR_FREQ_KEY_9   	( "9" )
#define SR_FREQ_KEY_A   	( "A" )
#define SR_FREQ_KEY_B   	( "B" )
#define SR_FREQ_KEY_C   	( "C" )
#define SR_FREQ_KEY_D   	( "D" )
#define SR_FREQ_KEY_E   	( "E" )
#define SR_FREQ_KEY_F   	( "F" )
// 4/14/2015 mpd : These last 5 FREQ_KEY values appear only on the output.  
#define SR_FREQ_KEY_10   	( "10" )
#define SR_FREQ_KEY_11   	( "11" )
#define SR_FREQ_KEY_12   	( "12" )
#define SR_FREQ_KEY_13   	( "13" )
#define SR_FREQ_KEY_14   	( "14" )

#define SR_PACKET_SIZE_0	( "0" )
#define SR_PACKET_SIZE_1	( "1" )
#define SR_PACKET_SIZE_2	( "2" )
#define SR_PACKET_SIZE_3	( "3" )
#define SR_PACKET_SIZE_4	( "4" )
#define SR_PACKET_SIZE_5	( "5" )
#define SR_PACKET_SIZE_6	( "6" )
#define SR_PACKET_SIZE_7	( "7" )
#define SR_PACKET_SIZE_8	( "8" )
#define SR_PACKET_SIZE_9	( "9" )

#define SR_NETWORK_ID_0100	( "0100" )
#define SR_NETWORK_ID_0110	( "0110" )
#define SR_NETWORK_ID_0120	( "0120" )
#define SR_NETWORK_ID_0130	( "0130" )
#define SR_NETWORK_ID_0140	( "0140" )
#define SR_NETWORK_ID_0150	( "0150" )
#define SR_NETWORK_ID_0160	( "0160" )
#define SR_NETWORK_ID_0170	( "0170" )
#define SR_NETWORK_ID_0180	( "0180" )
#define SR_NETWORK_ID_0190	( "0190" )

#define SR_GROUP_ID_NONE	( "  " )
#define SR_GROUP_ID_1		( " 1" )
#define SR_GROUP_ID_2		( " 2" )
#define SR_GROUP_ID_3		( " 3" )
#define SR_GROUP_ID_4		( " 4" )
#define SR_GROUP_ID_5		( " 5" )
#define SR_GROUP_ID_6		( " 6" )
#define SR_GROUP_ID_7		( " 7" )
#define SR_GROUP_ID_8		( " 8" )
#define SR_GROUP_ID_9		( " 9" )
#define SR_GROUP_ID_10		( "10" )

#define SR_INVALID_VALUE	( "99" )
#define SR_INVALID_INDEX	( "9999" )

// 3/19/2015 mpd : These are the Calsense supported modem modes. 
#define SR_MODE_P_TO_M_MASTER   ( "2" )
#define SR_MODE_P_TO_M_SLAVE    ( "3" )
#define SR_MODE_P_TO_M_REPEATER ( "7" )

// 3/11/2015 mpd : These are text strings of interest on SR radio menus. The offsets identify the location of data in
// relation to the "anchor" text string. Include an extra character in the length since it will be null-terminated
// during extraction.
// 
//                                 MAIN MENU
//  					D2 AES Version v10.6.8      08-21-2013
//  					01234567890123456789012345678901234567890       <- Ruler
//                           902 - 928 MHz
//                        Modem Serial Number 861-4576
//                        01234567890123456789012345678901234567890     <- Ruler
//  						Model Code DV2T
//  						01234567890123456789012345678901234567890	<- Ruler
#define SR_MAIN_MENU_VERSION_STR		"D2 AES Version"
#define SR_MAIN_MENU_VERSION_OFFSET		( 15 )
#define SR_MAIN_MENU_VERSION_LEN		( 12 + NULL_TERM_LEN )	// Currently only 7 in use

#define SR_MAIN_MENU_DATE_STR			"D2 AES Version"		// Same line as Version
#define SR_MAIN_MENU_DATE_OFFSET		( 28 )
#define SR_MAIN_MENU_DATE_LEN			( 10 + NULL_TERM_LEN )

#define SR_MAIN_MENU_SERIAL_STR			"Modem Serial Number"
#define SR_MAIN_MENU_SERIAL_OFFSET		( 20 )
#define SR_MAIN_MENU_SERIAL_LEN			( 8 + NULL_TERM_LEN )

#define SR_MAIN_MENU_MODEL_STR  		"Model Code"
#define SR_MAIN_MENU_MODEL_OFFSET  		( 11 )
#define SR_MAIN_MENU_MODEL_LEN  		( 4 + NULL_TERM_LEN )

//                                SET MODEM MODE
//                        Modem Mode is   2
//                        01234567890123456789012345678901234567890	<- Ruler
#define SR_MODE_MENU_MODE_STR  			"Modem Mode is"
#define SR_MODE_MENU_MODE_OFFSET  		( 16 )
#define SR_MODE_MENU_MODE_LEN  			( 2 + NULL_TERM_LEN ) // Leave space for the "99" invalid value.
 
//                                SET BAUD RATE
//                        Modem Baud is  019200
//  					  01234567890123456789012345678901234567890	<- Ruler
// 
//  				  < lines deleted >
// 
//(D)   Setup Port    3
//(E)   TurnOffDelay  0      TurnOnDelay   0
//(F)   FlowControl   1
//      01234567890123456789012345678901234567890	<- Ruler
#define SR_BAUD_MENU_RATE_STR  				"Modem Baud is"
#define SR_BAUD_MENU_RATE_OFFSET  			( 15 )
#define SR_BAUD_MENU_RATE_LEN  				( 6 + NULL_TERM_LEN )

#define SR_BAUD_MENU_SETUP_PORT_STR  		"Setup Port"
#define SR_BAUD_MENU_SETUP_PORT_OFFSET  	( 14 )
#define SR_BAUD_MENU_SETUP_PORT_LEN  		( 1 + NULL_TERM_LEN )

#define SR_BAUD_MENU_FLOW_CONTROL_STR		"FlowControl"
#define SR_BAUD_MENU_FLOW_CONTROL_OFFSET  	( 14 )
#define SR_BAUD_MENU_FLOW_CONTROL_LEN  		( 1 + NULL_TERM_LEN )

// 4/16/2015 mpd : The command to set the baud rate is completely separae from the display size.
#define SR_BAUD_RATE_COMMAND_LEN 			( 1 + NULL_TERM_LEN )

//                                RADIO PARAMETERS
//
//WARNING: Do not change parameters without reading manual
//
//(0)   FreqKey          4
//(1)   Max Packet Size  7
//(2)   Min Packet Size  8
//(3)   Xmit Rate        1
//(4)   RF Data Rate     3
//(5)   RF Xmit Power   10
//(6)   Slave Security   0
//(7)   RTS to CTS       0
//(8)   Retry Time Out 255
//(9)   Lowpower Mode    0
//      01234567890123456789012345678901234567890	<- Ruler
#define SR_RADIO_MENU_FREQ_KEY_STR  		"FreqKey"
#define SR_RADIO_MENU_FREQ_KEY_OFFSET  		( 16 )
#define SR_RADIO_MENU_FREQ_KEY_LEN  		( 2 + NULL_TERM_LEN )

#define SR_RADIO_MENU_MAX_PACKET_STR  		"Max Packet Size"
#define SR_RADIO_MENU_MAX_PACKET_OFFSET 	( 17 )
#define SR_RADIO_MENU_MAX_PACKET_LEN  		( 1 + NULL_TERM_LEN )

#define SR_RADIO_MENU_MIN_PACKET_STR  		"Min Packet Size"
#define SR_RADIO_MENU_MIN_PACKET_OFFSET 	( 17 )
#define SR_RADIO_MENU_MIN_PACKET_LEN  		( 1 + NULL_TERM_LEN )

#define SR_RADIO_MENU_DATA_RATE_STR  		"RF Data Rate"
#define SR_RADIO_MENU_DATA_RATE_OFFSET	 	( 16 )
#define SR_RADIO_MENU_DATA_RATE_LEN  		( 2 + NULL_TERM_LEN )

#define SR_RADIO_MENU_XMT_POWER_STR  		"RF Xmit Power"
#define SR_RADIO_MENU_XMT_POWER_OFFSET	 	( 16 )
#define SR_RADIO_MENU_XMT_POWER_LEN  		( 4 + NULL_TERM_LEN ) // Leave space for the "9999" invalid value.
#define SR_RADIO_MENU_XMT_POWER_V_LEN  		( 2 )				  // The number of characters to verify after a WRITE.

#define SR_RADIO_MENU_LOWPOWER_MODE_STR  	"Lowpower Mode"
#define SR_RADIO_MENU_LOWPOWER_MODE_OFFSET	( 16 )
#define SR_RADIO_MENU_LOWPOWER_MODE_LEN  	( 2 + NULL_TERM_LEN )

//                                MULTIPOINT PARAMETERS
//
//
//(0)   Number Repeaters      1
//(1)   Master Packet Repeat  5
//(2)   Max Slave Retry       5
//(3)   Retry Odds            5
//(4)   DTR Connect           0
//(5)   Repeater Frequency    0
//(6)   NetWork ID         0110
//      01234567890123456789012345678901234567890	<- Ruler
//(7)   Reserved
//(8)   MultiMasterSync       0
//(9)   1 PPS Enable/Delay  255serial      
//(A)   Slave/Repeater        0
//(B)   Diagnostics          32
//(C)   SubNet ID          Roaming					<- "Roaming" format for line C
//(C)   SubNet ID          Rcv=3 Xmit=7 			<- Not "Roaming" format for line C
//(D)   Radio ID Not Set
//      01234567890123456789012345678901234567890	<- Ruler
//(E)   Local Access         0
//(F)
//(G)   Radio Name         012345678901234567890    <- 20 characters that can include single quote.
#define SR_MULTIPOINT_MENU_NUM_REPEATERS_STR  			"Number Repeaters"
#define SR_MULTIPOINT_MENU_NUM_REPEATERS_OFFSET  		( 21 )
#define SR_MULTIPOINT_MENU_NUM_REPEATERS_LEN  			( 2 + NULL_TERM_LEN )

#define SR_MULTIPOINT_MENU_MASTER_PACKET_REPEAT_STR  	"Master Packet Repeat"
#define SR_MULTIPOINT_MENU_MASTER_PACKET_REPEAT_OFFSET 	( 21 )
#define SR_MULTIPOINT_MENU_MASTER_PACKET_REPEAT_LEN  	( 2 + NULL_TERM_LEN )

#define SR_MULTIPOINT_MENU_MAX_SLAVE_RETRY_STR  		"Max Slave Retry"
#define SR_MULTIPOINT_MENU_MAX_SLAVE_RETRY_OFFSET  		( 21 )
#define SR_MULTIPOINT_MENU_MAX_SLAVE_RETRY_LEN  		( 2 + NULL_TERM_LEN )

#define SR_MULTIPOINT_MENU_RETRY_ODDS_STR  				"Retry Odds"
#define SR_MULTIPOINT_MENU_RETRY_ODDS_OFFSET  			( 21 )
#define SR_MULTIPOINT_MENU_RETRY_ODDS_LEN  				( 2 + NULL_TERM_LEN )

#define SR_MULTIPOINT_MENU_REPEATER_FREQ_STR  			"Repeater Frequency"
#define SR_MULTIPOINT_MENU_REPEATER_FREQ_OFFSET  		( 22 )
#define SR_MULTIPOINT_MENU_REPEATER_FREQ_LEN  			( 1 + NULL_TERM_LEN )	

#define SR_MULTIPOINT_MENU_NETWORK_ID_STR  				"NetWork ID"
#define SR_MULTIPOINT_MENU_NETWORK_ID_OFFSET  			( 19 )
#define SR_MULTIPOINT_MENU_NETWORK_ID_LEN  				( 4 + NULL_TERM_LEN )

#define SR_MULTIPOINT_MENU_SLAVE_REPEATER_STR  			"Slave/Repeater"
#define SR_MULTIPOINT_MENU_SLAVE_REPEATER_OFFSET  		( 22 )					
#define SR_MULTIPOINT_MENU_SLAVE_REPEATER_LEN  			( 1 + NULL_TERM_LEN )	

#define SR_MULTIPOINT_MENU_DIAGNOSTICS_STR  			"Diagnostics"
#define SR_MULTIPOINT_MENU_DIAGNOSTICS_OFFSET 	 		( 21 )					
#define SR_MULTIPOINT_MENU_DIAGNOSTICS_LEN  			( 2 + NULL_TERM_LEN )	

#define SR_MULTIPOINT_MENU_SUBNET_ID_STR  				"SubNet ID"
#define SR_MULTIPOINT_MENU_SUBNET_ID_OFFSET 	 		( 19 )				
#define SR_MULTIPOINT_MENU_SUBNET_ID_LEN  				( 8 + NULL_TERM_LEN )	

#define SR_MULTIPOINT_MENU_SUBNET_DISABLED_STR 			"Disabled"				// Special case for SubnetID Rcv=F, Xmt=F
#define SR_MULTIPOINT_MENU_SUBNET_DISABLED_LEN 			( 8 )					// Special case for SubnetID Rcv=F, Xmt=F
#define SR_MULTIPOINT_MENU_SUBNET_ROAMING_STR  			"Roaming"				// Special case for SubnetID Rcv=0, Xmt=0
#define SR_MULTIPOINT_MENU_SUBNET_ROAMING_LEN  			( 7 )					// Special case for SubnetID Rcv=0, Xmt=0
				
#define SR_MULTIPOINT_MENU_SUBNET_RCV_ID_STR  			"Rcv="					// Does not exist if DISABLED or ROAMING
#define SR_MULTIPOINT_MENU_SUBNET_RCV_ID_OFFSET 		( 4 )					
#define SR_MULTIPOINT_MENU_SUBNET_RCV_ID_LEN  			( 2 + NULL_TERM_LEN )	// Leave space for the "99" invalid value.
#define SR_MULTIPOINT_MENU_SUBNET_RCV_ID_V_LEN  		( 1 )					// The number of characters to verify after a WRITE.

#define SR_MULTIPOINT_MENU_SUBNET_XMT_ID_STR  			"Xmit="					// Does not exist if DISABLED or ROAMING
#define SR_MULTIPOINT_MENU_SUBNET_XMT_ID_OFFSET 		( 5 )					
#define SR_MULTIPOINT_MENU_SUBNET_XMT_ID_LEN  			( 2 + NULL_TERM_LEN )	// Leave space for the "99" invalid value.
#define SR_MULTIPOINT_MENU_SUBNET_XMT_ID_V_LEN  		( 1 )					// The number of characters to verify after a WRITE.

#define SR_MULTIPOINT_MENU_RADIO_ID_STR  				"Radio ID"
#define SR_MULTIPOINT_MENU_RADIO_ID_OFFSET 		 		( 19 )					
#define SR_MULTIPOINT_MENU_RADIO_ID_LEN  				( 4 + NULL_TERM_LEN )	

#define SR_MULTIPOINT_MENU_RADIO_NAME_STR  				"Radio Name"
#define SR_MULTIPOINT_MENU_RADIO_NAME_OFFSET  			( 19 )					
#define SR_MULTIPOINT_MENU_RADIO_NAME_LEN  				( 20 + NULL_TERM_LEN )	

// 3/13/2015 mpd : These are not derived from SR responses.
#define SR_EMPTY_STR									( 0xFF ) // Special case for initial I/O cycle. 
#define SR_EMPTY_STR_LEN								( 1 ) 	 // Length > 0 is required. 
#define SR_CRLF_STR										"\r\n"
#define SR_CRLF_STR_LEN									( 2 )
#define SR_NETWORK_GROUP_ID_LEN 						( 4 + NULL_TERM_LEN ) // Leave space for the "9999" invalid value.
#define SR_REPEATER_GROUP_ID_LEN 						( 2 + NULL_TERM_LEN ) // Leave space for the "99" invalid value.
#define SR_LONGEST_COMMAND_LEN							( SR_MULTIPOINT_MENU_RADIO_NAME_LEN )	

// 3/19/2015 mpd : Replaced "SR_SETUP_STRUCT" with a "SR_STATE_STRUCT" and a "SR_PROGRAMABLE_VALUES_STRUCT". This
// separates global state info from multiple copies of programmable values.
 
#define SR_PVS_TOKEN_STR    ( "PVS" )
#define SR_PVS_TOKEN_LEN    ( 4 )

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	char pvs_token[ SR_PVS_TOKEN_LEN ];

	// MODEM MENU
	char modem_mode[ SR_MODE_MENU_MODE_LEN ];

	// BAUD RATE MENU
	char baud_rate[ SR_BAUD_MENU_RATE_LEN ];

	// 5/1/2015 mpd : These fields are not currently needed by SR Programming. They have been tested and are functional.
	//char setup_port[ SR_BAUD_MENU_SETUP_PORT_LEN ];
	//char flow_control[ SR_BAUD_MENU_FLOW_CONTROL_LEN ];

	// RADIO PARAMETERS MENU
	char freq_key[ SR_RADIO_MENU_FREQ_KEY_LEN ];
	char max_packet_size[ SR_RADIO_MENU_MAX_PACKET_LEN ];
	char min_packet_size[ SR_RADIO_MENU_MIN_PACKET_LEN ];
	char rf_data_rate[ SR_RADIO_MENU_DATA_RATE_LEN ];
	char rf_xmit_power[ SR_RADIO_MENU_XMT_POWER_LEN ];
	char low_power_mode[ SR_RADIO_MENU_LOWPOWER_MODE_LEN ];

	// MULTIPOINT PARAMETERS MENU
	char number_of_repeators[ SR_MULTIPOINT_MENU_NUM_REPEATERS_LEN ];
	char master_packet_repeat[ SR_MULTIPOINT_MENU_MASTER_PACKET_REPEAT_LEN ];
	char max_slave_retry[ SR_MULTIPOINT_MENU_MAX_SLAVE_RETRY_LEN ];
	char retry_odds[ SR_MULTIPOINT_MENU_RETRY_ODDS_LEN ];
	char repeater_frequency[ SR_MULTIPOINT_MENU_REPEATER_FREQ_LEN ];
	char network_id[ SR_MULTIPOINT_MENU_NETWORK_ID_LEN ];
	char slave_and_repeater[ SR_MULTIPOINT_MENU_SLAVE_REPEATER_LEN ];
	char diagnostics[ SR_MULTIPOINT_MENU_DIAGNOSTICS_LEN ];
	char subnet_id[ SR_MULTIPOINT_MENU_SUBNET_ID_LEN ];			// This is read-only, but needs to be with Rcv and Xmt for decisions.
	char subnet_rcv_id[ SR_MULTIPOINT_MENU_SUBNET_RCV_ID_LEN ];	// Alternate of ROAMING and DISABLED, requires special handling
	char subnet_xmt_id[ SR_MULTIPOINT_MENU_SUBNET_XMT_ID_LEN ];	// Alternate of ROAMING and DISABLED, requires special handling

	// 5/1/2015 mpd : These fields are not currently needed by SR Programming. They have been tested and are functional.
	//char radio_id[ SR_MULTIPOINT_MENU_RADIO_ID_LEN ];
	//char radio_name[ SR_MULTIPOINT_MENU_RADIO_NAME_LEN ];

} SR_PROGRAMMABLE_VALUES_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	UNS_16 error_code;
	UNS_32 operation;
	char operation_text[ SR_OPERATION_STR_LEN ];                
	char info_text[ SR_INFO_TEXT_LEN ];                
	char progress_text[ SR_INFO_TEXT_LEN ];                
	UNS_32 read_list_index;
	UNS_32 write_list_index;
	BOOL_32 mode_is_valid;
	BOOL_32 group_is_valid;
	BOOL_32 repeater_is_valid;
	BOOL_32 radio_settings_are_valid;
	BOOL_32 programming_successful;

	// 3/23/2015 mpd : Centralize the response buffer pointer to make "mem_free()" easier.
	char  *resp_ptr;
	UNS_32 resp_len;

	// 3/20/2015 mpd : Pointers to the programmable variables structs.
	SR_PROGRAMMABLE_VALUES_STRUCT *dynamic_pvs;
	SR_PROGRAMMABLE_VALUES_STRUCT *write_pvs;

	// 3/20/2015 mpd : Generic analysis and verification functions require this SR_STATE_STRUCT to make some decisions.
	// They also require a specific SR_PROGRAMMABLE_VALUES_STRUCT as the focus of their efforts. Use this field to set the
	// applicable struct before calling the generic functions.
	SR_PROGRAMMABLE_VALUES_STRUCT *active_pvs;

	// 3/19/2015 mpd : These are read-only radio parameters. They are here to avoid 3 copies.
	char sw_version[ SR_MAIN_MENU_VERSION_LEN ];                

	// 5/1/2015 mpd : These fields are not currently needed by SR Programming. They have been tested and are functional.
	//char sw_date[ SR_MAIN_MENU_DATE_LEN ];                      

	char serial_number[ SR_MAIN_MENU_SERIAL_LEN ];              
	char model[ SR_MAIN_MENU_MODEL_LEN ];                       

	// 3/19/2015 mpd : These are Calsense defined groups.
	UNS_32 group_index;
	char network_group_id[ SR_NETWORK_GROUP_ID_LEN ];
	char repeater_group_id[ SR_REPEATER_GROUP_ID_LEN ];

} SR_STATE_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 3/19/2015 mpd : These are read-only radio parameters. They are here to avoid 3 copies.
	char sw_version[ SR_MAIN_MENU_VERSION_LEN ];                

	// 5/1/2015 mpd : These fields are not currently needed by SR Programming. They have been tested and are functional.
	//char sw_date[ SR_MAIN_MENU_DATE_LEN ];                      

	char serial_number[ SR_MAIN_MENU_SERIAL_LEN ];              
	char model[ SR_MAIN_MENU_MODEL_LEN ];                       

	// 3/19/2015 mpd : These are Calsense defined groups.
	UNS_32 group_index;
	char network_group_id[ SR_NETWORK_GROUP_ID_LEN ];
	char repeater_group_id[ SR_REPEATER_GROUP_ID_LEN ];

} SR_DETAILS_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern SR_STATE_STRUCT *sr_state;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void SR_FREEWAVE_power_control( const UNS_32 pport, const BOOL_32 pon_or_off );

extern void SR_FREEWAVE_exchange_processing( void *pq_msg );

extern void SR_FREEWAVE_initialize_device_exchange( void );

// ----------

extern void SR_FREEWAVE_initialize_the_connection_process( UNS_32 pport );

extern void SR_FREEWAVE_connection_processing( UNS_32 pevent );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

