/*  file = rcvd_data.c                        2009.06.23  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"rcvd_data.h"

#include	"crc.h"

#include	"serport_drvr.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"packet_router.h"

#include	"comm_mngr.h"

#include	"cs_mem.h"

#include	"controller_initiated.h"

#include	"code_distribution_task.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void __test_packet_echo( UNS_32 pfrom_port, DATA_HANDLE pdh )
{
	// The Data handle contains the packet without the ambles on it. We also should FREE the Data Handle
	// memory passed to us. And any of our own allocations need to be freed.

	unsigned char	*ucp;

	UNS_32			actual_packet_length, to_port;

	DATA_HANDLE		ldh;


	// Because we have to add the pre and post ambles to define the packet its size is + 2*sizeof(AMBLE_TYPE)
	actual_packet_length = pdh.dlen + (2 * sizeof(AMBLE_TYPE));

	// allocate and take a copy of the packet
	ldh.dptr = mem_malloc( actual_packet_length );
	ldh.dlen = actual_packet_length;

	// put the preamble in place
	ucp = ldh.dptr;  // acquire start of block

	*ucp++ = preamble._1;
	*ucp++ = preamble._2;
	*ucp++ = preamble._3;
	*ucp++ = preamble._4;

	// put the data in place
	memcpy( ucp, pdh.dptr, pdh.dlen );

	ucp += pdh.dlen;  // hop over data

	// put the postamble in place
	*ucp++ = postamble._1;
	*ucp++ = postamble._2;
	*ucp++ = postamble._3;
	*ucp   = postamble._4;

	if( pfrom_port == UPORT_A )
	{
		to_port = UPORT_A;
		//to_port = UPORT_TP;
		//to_port = UPORT_USB;
		//to_port = UPORT_B;
	}
	else
	if( pfrom_port == UPORT_TP )
	{
		to_port = UPORT_TP;
		//to_port = UPORT_USB;
		//to_port = UPORT_B;
	}
	else
	{
		//to_port = UPORT_A;
		to_port = UPORT_A;
		//to_port = UPORT_RRE;
	}


	AddCopyOfBlockToXmitList( to_port, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_PACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );

	mem_free( ldh.dptr );

	mem_free( pdh.dptr );
}

/* ---------------------------------------------------------- */
extern void RCVD_DATA_enable_hunting_mode( UNS_32 pport, UNS_32 prequested_hunt, UNS_32	pwho_to_notify )
{
	// 2/12/2014 rmd : Critical section in case data is coming in. Changing hunting modes is
	// destructive. All previously received data is lost. Full cleaning of the ring buffer
	// structure is technically not necessary. But I do it to ease ring buffer viewing during
	// development. And will leave as there should not be harm. We will not switch hunt modes in
	// the middle of a stream of data and expect intact data.
	taskENTER_CRITICAL();

	// ----------

	// 2/12/2014 rmd : Full initialization. Zero everything. This has the effect of disabling
	// all hunting modes and restarting the ring buffer too.
	memset( (void*)&SerDrvrVars_s[ pport ].UartRingBuffer_s, 0, sizeof(UART_RING_BUFFER_s) );

	// ----------

	if( prequested_hunt == PORT_HUNT_FOR_DATA )
	{
		SerDrvrVars_s[ pport ].UartRingBuffer_s.hunt_for_data = (true);
	}
	else
	if( prequested_hunt == PORT_HUNT_FOR_SPECIFIED_STRING )
	{
		SerDrvrVars_s[ pport ].UartRingBuffer_s.hunt_for_specified_string = (true);
	}
	else
	if( prequested_hunt == PORT_HUNT_FOR_CRLF_DELIMITED_STRING )
	{
		SerDrvrVars_s[ pport ].UartRingBuffer_s.hunt_for_crlf_delimited_string = (true);
	}
	// 3/10/2015 mpd : Added hunt for SR radio menues. These are 300 - 500 byte multi-line responses (multiple CRLFs).  
	else
	if( prequested_hunt == PORT_HUNT_FOR_SPECIFIED_TERMINATION )
	{
		SerDrvrVars_s[ pport ].UartRingBuffer_s.hunt_for_specified_termination = (true);
	}
	else
	{
		// 2/12/2014 rmd : The default, even if the requested is out of range, is for packets.
		SerDrvrVars_s[ pport ].UartRingBuffer_s.hunt_for_packets = (true);
	}
	
	// ----------
	
	// 10/26/2016 rmd : Set 'em up. This is for the string HUNT modes only.
	SerDrvrVars_s[ pport ].UartRingBuffer_s.task_to_signal_when_string_found = pwho_to_notify;
	
	// ----------

	taskEXIT_CRITICAL();
}

/* ---------------------------------------------------------- */
static void check_ring_buffer_for_CalsenseTPL_packet( UNS_32 pport )
{
	UART_RING_BUFFER_s		*ptr_ring_buffer_s;		// for convienance
	
	unsigned short  itc1,itc2,itc3,itc4;
	
	unsigned char   *tptr;
	
	unsigned short  plen,psri;
	
	unsigned short  packet_size_limit;
	
	DATA_HANDLE dh;

	ptr_ring_buffer_s = &SerDrvrVars_s[ pport ].UartRingBuffer_s;

	// loop around looking for either the pre or post amble until we strike the tail
	do
	{

		itc1 = ptr_ring_buffer_s->ph.packet_index;
		if ( itc1 == ptr_ring_buffer_s->next ) return;

		itc2 = itc1+1; if ( itc2 == RING_BUFFER_SIZE ) itc2=0;
		if ( itc2 == ptr_ring_buffer_s->next ) return;

		itc3 = itc2+1; if ( itc3 == RING_BUFFER_SIZE ) itc3=0;
		if ( itc3 == ptr_ring_buffer_s->next ) return;

		itc4 = itc3+1; if ( itc4 == RING_BUFFER_SIZE ) itc4=0;
		if ( itc4 == ptr_ring_buffer_s->next ) return;

		//    sprintf(buffer,"       _1=%02X _2=%02X _3=%02X _4=%02X \0",preamble._1,preamble._2,preamble._3,preamble._4);
		//    DFromPtr(buffer,0);
		//    sprintf(buffer,"       i1=%02X i2=%02X i3=%02X i4=%02X \0",itc1,itc2,itc3,itc4);
		//    DFromPtr(buffer,40);
		//    sprintf(buffer,"       b1=%02X b2=%02X b3=%02X b4=%02X \0",B_ringbuffer[itc1],B_ringbuffer[itc2],B_ringbuffer[itc3],B_ringbuffer[itc4]);
		//    DFromPtr(buffer,80);
		//    sprintf(buffer,"       index=%04d tail=%04d len=%04d         \0",B_ringindex,B_ringtail,B_packetlength);
		//    DFromPtr(buffer,120);
		//    WaitTillKey(1);

		if (ptr_ring_buffer_s->ring[itc1] == preamble._1)
		{
			if (ptr_ring_buffer_s->ring[itc2] == preamble._2)
			{
				if (ptr_ring_buffer_s->ring[itc3] == preamble._3)
				{
					if (ptr_ring_buffer_s->ring[itc4] == preamble._4)
					{

						ptr_ring_buffer_s->ph.ringhead1 = itc1;    /* head points to the preamble */
						ptr_ring_buffer_s->ph.ringhead2 = itc2;
						ptr_ring_buffer_s->ph.ringhead3 = itc3;
						ptr_ring_buffer_s->ph.ringhead4 = itc4;

						ptr_ring_buffer_s->ph.packetlength = 0;

						// 2/4/2014 rmd : Note the data start. When we deliver the data to the router the pre and
						// post ambles have already been stripped off.
						ptr_ring_buffer_s->ph.datastart = itc4 + 1;

						if ( ptr_ring_buffer_s->ph.datastart == RING_BUFFER_SIZE ) ptr_ring_buffer_s->ph.datastart = 0;

						//            WaitTillKeyText("found complete pre-amble in B \0");
					}
				}
			}
		}


		if (ptr_ring_buffer_s->ring[itc1] == postamble._1)
		{
			if (ptr_ring_buffer_s->ring[itc2] == postamble._2)
			{
				if (ptr_ring_buffer_s->ring[itc3] == postamble._3)
				{
					if (ptr_ring_buffer_s->ring[itc4] == postamble._4)
					{
						// check the preamble is still there. if not move on
						if ( (ptr_ring_buffer_s->ring[ ptr_ring_buffer_s->ph.ringhead1 ] == preamble._1) &&
							 (ptr_ring_buffer_s->ring[ ptr_ring_buffer_s->ph.ringhead2 ] == preamble._2) &&
							 (ptr_ring_buffer_s->ring[ ptr_ring_buffer_s->ph.ringhead3 ] == preamble._3) &&
							 (ptr_ring_buffer_s->ring[ ptr_ring_buffer_s->ph.ringhead4 ] == preamble._4) )
						{
							// 5/30/2013 rmd : Adjust the packet length so that the length reflects the packet size
							// without the ambles. This is historical code and it is confusing as shit. Apparently the
							// comparison for size has in the past ignored the pre and post ambles.
							ptr_ring_buffer_s->ph.packetlength -= 4;

							// ----------

							// 2/5/2014 rmd : All ports can handle our maximum sized packet. This is driven by a few
							// factors, one of them being the code download scheme during which 2048+ byte packets make
							// their way from the comm_server through tpmicro cards and down the chain to slaves.
							packet_size_limit = SYSTEM_WIDE_LARGEST_AMBLE_TO_AMBLE_PACKET_SIZE;

							// ----------

							if ( ptr_ring_buffer_s->ph.packetlength > packet_size_limit )
							{
								// for the case of LR units - sometimes they can hear each other but not well and that can generate
								// a nusiance number of "packet > 512" error messages turn off with the low_level_transport_errors bit
								//

								/* TODO LPC3250 commentted out cause we don't have a config table yet
								if ( ( ConfigTable.Items.Debug.b.low_level_transport_errors == 1 ) &&
									 ( ConfigTable.Items.A_CommOption != OPTION_LR ) ) {

									Alert_packet_greater_than_512();

								}
								*/

								// 5/30/2013 rmd : Not an appropriate alert when it comes to the TP port. But ahhh good
								// enough.
								Alert_packet_greater_than_512( pport );
							}
							else
							{
								// now we have a pre and post amble identified testthe CRC

								/* allocate memory on the heap for this packet. When
								 * transferred to IM using PacketToTransport the
								 * transport layer then becomes responsible for releasing
								 * this memory
								 */

								dh.dlen = ptr_ring_buffer_s->ph.packetlength;
								dh.dptr = mem_malloc( dh.dlen );

								/* get a copy of the memory block ptr (packet start) */
								tptr = dh.dptr;

								psri = ptr_ring_buffer_s->ph.datastart;    /* packet start ring index */

								/* plen is just for debugging and can be done
								 * away with when working
								 */
								plen = 0;
								do
								{

									*tptr++ = ptr_ring_buffer_s->ring[ psri++ ];

									if( psri == RING_BUFFER_SIZE ) psri=0;

									plen++;

								} while( psri!=itc1 );


								if( plen != ptr_ring_buffer_s->ph.packetlength )
								{

									Alert_Message( "Packet lengths don't match" );

								}
								else
								{
									// 2/4/2014 rmd : Give the data to the router with the pre and post ambles already stripped
									// off.
									Route_Incoming_Packet( pport, dh );
								}

								// 2/10/2017 rmd : Do not free the allocated packet memory here. All we have done is posted
								// a queue event which carries the data handle. A copy of the packet has not yet been taken.

							}  // of packet size makes sense

						}  // of if the preamble is still there

						// reset these to guarantee new packet start must be found
						ptr_ring_buffer_s->ph.ringhead1 = itc1;
						ptr_ring_buffer_s->ph.ringhead2 = itc1;
						ptr_ring_buffer_s->ph.ringhead3 = itc1;
						ptr_ring_buffer_s->ph.ringhead4 = itc1;

					}  // post amble check 4
				}  // post amble check 3
			}  // post amble check 2
		}  // post amble check 1

		// each loop move index along
		if (++ptr_ring_buffer_s->ph.packet_index == RING_BUFFER_SIZE)
		{

			ptr_ring_buffer_s->ph.packet_index = 0;

		}

		ptr_ring_buffer_s->ph.packetlength++;  // each loop makes the packet 1 bigger

	} while( (true) );  // of do while loop

}

/* ---------------------------------------------------------- */
static void check_ring_buffer_for_a_specified_string( UNS_32 pport )
{
	UNS_32	ddd;

	// 2/18/2014 rmd : If we aren't setup to see a string don't try. Not sure what calling
	// strncmp with a NULL does to it. And make a range check on the depth into the buffer to
	// hunt. A sort of sanity check to make sure it was set.
	//
	// 11/8/2016 rmd : The depth limit of 100 is a form of sanity check. I want to know if we
	// are intentionally searching deeper than that. One should think about the task starve
	// impact. And also consider a more efficient design - noting which part of the buffer has
	// been previously searched.
	if( (SerDrvrVars_s[ pport ].UartRingBuffer_s.sh.str_to_find != NULL) && (SerDrvrVars_s[ pport ].UartRingBuffer_s.sh.depth_into_the_buffer_to_look < 100) )
	{
		for( ddd=0; ddd<=SerDrvrVars_s[ pport ].UartRingBuffer_s.sh.depth_into_the_buffer_to_look; ddd++ )
		{
			// 10/30/2016 rmd : This function runs as often as once per character arrival. In an attempt
			// to aid efficiency we'll test the first char of the string for a match before performing
			// a full string test.
			if( SerDrvrVars_s[ pport ].UartRingBuffer_s.ring[ ddd ] == SerDrvrVars_s[ pport ].UartRingBuffer_s.sh.str_to_find[ 0 ] )
			{
				if( strncmp( (char*)(SerDrvrVars_s[ pport ].UartRingBuffer_s.ring + ddd), SerDrvrVars_s[ pport ].UartRingBuffer_s.sh.str_to_find, SerDrvrVars_s[ pport ].UartRingBuffer_s.sh.chars_to_match ) == 0 )
				{
					// 9/7/2017 rmd : I'm adding cancelling any further string detections. It seems like an
					// oversight to me. Once the string has been found, if more characters arrive, what is to
					// stop the string from being 'found' again and again, generating several events? Well this
					// line will. I guess the specific strings we have been hunting for so far include up to the
					// last character that is produced from the device, so that are no 'after' characters that
					// arrive.
					SerDrvrVars_s[ pport ].UartRingBuffer_s.hunt_for_specified_string = (false);
					
					// ----------
					
					if( SerDrvrVars_s[ pport ].UartRingBuffer_s.task_to_signal_when_string_found == WHEN_STRING_FOUND_NOTIFY_THE_COMM_MNGR_TASK )
					{
						COMM_MNGR_post_event( COMM_MNGR_EVENT_isp_expected_response_arrived );
					}
					else
					if( SerDrvrVars_s[ pport ].UartRingBuffer_s.task_to_signal_when_string_found == WHEN_STRING_FOUND_NOTIFY_THE_CI_TASK )
					{
						CONTROLLER_INITIATED_post_event( CI_EVENT_process_string_found );
					}
					else
					if( SerDrvrVars_s[ pport ].UartRingBuffer_s.task_to_signal_when_string_found == WHEN_STRING_FOUND_NOTIFY_THE_CODE_DISTRIBUTION_TASK )
					{
						CODE_DISTRIBUTION_post_event( CODE_DISTRIBUTION_EVENT_specified_string_found );
					}
					else
					{
						Alert_Message( "Specified HUNT incorrectly set up." );
					}
					
					// ----------
					
					// 10/30/2016 rmd : And stop hunting the depth.
					break;
				}
				else
				{
					// 2/13/2014 rmd : If no match nothing to do! Just remain quiet and eventually the
					// application timer guiding the process (ISP or connection process for example) will
					// timeout. Indicating the expected response did not arrive.
				}
				
			}  // of first chars match
			
		}  // of for the depth to hunt
		
	}  // of there is a string to find
}

/* ---------------------------------------------------------- */
static void check_ring_buffer_for_a_crlf_delimited_string( UNS_32 pport )
{
	// 8/8/2017 rmd : This function can pick out a string that is nested between two CR-LF
	// character pairs. Optionally, we can also choose to find a string that has only a CR-LF at
	// the end of the string. The string found is returned via an event queue to the task
	// requested to be sent to when the hunt mode is setup. The string returned via the event
	// does not contain the CR-LF characters.

	// ----------
	
	char	CRLF_str[] = "\r\n";
	
	char	*start_ptr, *end_ptr, *tmp_ptr;
	
	UNS_32	str_length;
	
	COMM_MNGR_TASK_QUEUE_STRUCT	cmtqs;

	CODE_DISTRIBUTION_TASK_QUEUE_STRUCT	cdtqs;
	
	CONTROLLER_INITIATED_TASK_QUEUE_STRUCT	citqs;

	BOOL_32	proceed;
	
	DATA_HANDLE	ldh;
	
	// ----------

	proceed = (true);
	
	// 8/8/2017 rmd : Check if we are supposed to be looking for an initial CR-LF.
	if( SerDrvrVars_s[ pport ].UartRingBuffer_s.sh.find_initial_CRLF )
	{
		// 8/8/2017 rmd : If so see if we can find it.
		if( strncmp( (char*)(SerDrvrVars_s[ pport ].UartRingBuffer_s.ring), CRLF_str, 2 ) == 0 )
		{
			start_ptr = (char*)&(SerDrvrVars_s[ pport ].UartRingBuffer_s.ring[ 2 ]);
		}
		else
		{
			// 8/8/2017 rmd : Not here yet so no more hunting at this time.
			proceed = (false);
		}
	}
	else
	{
		// 8/8/2017 rmd : If not looking for a preceeding CR-LR, always start at the beginning of
		// the buffer, waiting for the trailing CR-LF to appear.
		start_ptr = (char*)&(SerDrvrVars_s[ pport ].UartRingBuffer_s.ring[ 0 ]);
	}
	
	// ----------
	
	if( proceed )
	{
		if( (end_ptr = strstr( start_ptr, CRLF_str )) != NULL )
		{
			// 8/8/2017 rmd : Now we need to figure the length of the target string in the buffer.
			tmp_ptr = start_ptr;
			
			str_length = 0;
			
			while( tmp_ptr++ != end_ptr )
			{
				str_length += 1;
			}
			
			// 3/17/2014 rmd : Now build up a queue message for the comm_mngr to feed the device
			// exchange state machine. Need to get memory for the string in order to deliver through the
			// queue message.
			
			// 3/17/2014 rmd : We need enough memory for the string plus a terminating NULL char.
			ldh.dlen = (str_length + 1);
			
			ldh.dptr = mem_malloc( ldh.dlen );
			
			// 3/18/2014 rmd : Only copy the characters we are interested in. And that is a count of
			// str_length chars.
			memcpy( ldh.dptr, start_ptr, str_length );
			
			// 3/18/2014 rmd : Put the NULL terminate into place.
			*(ldh.dptr + str_length) = 0x00;

			// ----------
			
			if( SerDrvrVars_s[ pport ].UartRingBuffer_s.task_to_signal_when_string_found == WHEN_STRING_FOUND_NOTIFY_THE_COMM_MNGR_TASK )
			{
				// 3/17/2014 rmd : Clean queue message for good measure.
				memset( &cmtqs, 0x00, sizeof(COMM_MNGR_TASK_QUEUE_STRUCT) );
				
				cmtqs.dh = ldh;
				
				cmtqs.event = COMM_MNGR_EVENT_device_exchange_response_arrived;
	
				COMM_MNGR_post_event_with_details( &cmtqs );
			}
			else
			if( SerDrvrVars_s[ pport ].UartRingBuffer_s.task_to_signal_when_string_found == WHEN_STRING_FOUND_NOTIFY_THE_CODE_DISTRIBUTION_TASK )
			{
				// 3/17/2014 rmd : Clean queue message for good measure.
				memset( &cdtqs, 0x00, sizeof(CODE_DISTRIBUTION_TASK_QUEUE_STRUCT) );
				
				cdtqs.dh = ldh;
				
				cdtqs.event = CODE_DISTRIBUTION_EVENT_cr_lf_string_found;
	
				CODE_DISTRIBUTION_post_event_with_details( &cdtqs );
			}
			else
			if( SerDrvrVars_s[ pport ].UartRingBuffer_s.task_to_signal_when_string_found == WHEN_STRING_FOUND_NOTIFY_THE_CI_TASK )
			{
				// 3/17/2014 rmd : Clean queue message for good measure.
				memset( &citqs, 0x00, sizeof(CONTROLLER_INITIATED_TASK_QUEUE_STRUCT) );
				
				citqs.dh = ldh;
				
				citqs.event = CI_EVENT_process_string_found;
	
				CONTROLLER_INITIATED_post_event_with_details( &citqs );
			}
			else
			{
				Alert_Message( "CR-LF HUNT incorrectly set up." );
			}
			
			// ----------

			// 3/19/2014 rmd : This function was designed to pull CR LF delimited responses from a
			// device response. Typically AT command driven. Usually there is more data that follows the
			// target response. Typically a CR LF OK CR LF. This additional data will cause additional
			// device exchange response arrived queue messages to be created. Actually 'finding' the
			// response we already found. This generates botched results. The solution is to turn OFF
			// the hunting once the first target string has been located. And we do that here.
			SerDrvrVars_s[ pport ].UartRingBuffer_s.hunt_for_crlf_delimited_string = (false);

		}  // of if we found the terminating CRLF
	}
}

/* ---------------------------------------------------------- */
static void check_ring_buffer_for_a_specified_termination( UNS_32 pport )
{
	// 3/10/2015 mpd : Look for a specified string to indicate the end of the input data. Then signal the comm manager. 
	// This function was added to handle the multi-line responses from Freewave SR radios. It was modeled after the
	// "check_ring_buffer_for_a_crlf_delimited_string()" function with the addition of "string_index" handling. Comments
	// from the original function remain where appropriate.
	
	char	*start_ptr, *end_ptr, *termination_ptr;

	UNS_32	str_length;

	COMM_MNGR_TASK_QUEUE_STRUCT	cmtqs;

	CODE_DISTRIBUTION_TASK_QUEUE_STRUCT	cdtqs;
	
	CONTROLLER_INITIATED_TASK_QUEUE_STRUCT	citqs;

	DATA_HANDLE	ldh;

	//char alert_buffer[50];

	// ----------

	if( SerDrvrVars_s[ pport ].UartRingBuffer_s.th.str_to_find != NULL )
	{
		// 3/13/2015 mpd : Search for the specified string in the buffer. Use the "string_index" field to begin the 
		// search at the last possible location. This function will be called many times before the terminating string
		// is found.
		termination_ptr = strstr( (char*)&(SerDrvrVars_s[ pport ].UartRingBuffer_s.ring[ SerDrvrVars_s[ pport ].UartRingBuffer_s.th.string_index ] ), 
								  SerDrvrVars_s[ pport ].UartRingBuffer_s.th.str_to_find );


		//snprintf( alert_buffer, sizeof(alert_buffer), "RCVD(%d): ->%s<-", SerDrvrVars_s[ pport ].UartRingBuffer_s.th.string_index,
		//		  (char*)&(SerDrvrVars_s[ pport ].UartRingBuffer_s.ring[ SerDrvrVars_s[ pport ].UartRingBuffer_s.th.string_index ]) );
		//Alert_Message( alert_buffer );

		if( termination_ptr != NULL )
		{
			// 3/13/2015 mpd : The termination string was found. Package the entire receive buffer for the client.

			// 3/13/2015 mpd : Get the beginning address of the buffer. 
			start_ptr = (char*)&(SerDrvrVars_s[ pport ].UartRingBuffer_s.ring[ 0 ]);

			// 3/10/2015 mpd : Get the ending address of the buffer. Include the termination string.
			end_ptr = (termination_ptr + SerDrvrVars_s[ pport ].UartRingBuffer_s.th.chars_to_match);

			// 3/10/2015 mpd : Sanity check.
			if( end_ptr > start_ptr )
			{
				// 3/13/2015 mpd : Calculate the buffer length. A null-termination char will be added after the data 
				// copy. Leave it out of this count.
				str_length = end_ptr - start_ptr;

				// 3/17/2014 rmd : Now build up a queue message for the comm_mngr to feed the device
				// exchange state machine. Need to get memory for the string in order to deliver through the
				// queue message.

				// 3/17/2014 rmd : We need enough memory for the entire buffer plus a terminating NULL char.
				ldh.dlen = (str_length + 1);

				ldh.dptr = mem_malloc( ldh.dlen );

				// 3/13/2015 mpd : Copy the entire buffer.
				memcpy( ldh.dptr, start_ptr, str_length );

				// 3/18/2014 rmd : Put the NULL terminate into place.
				*(ldh.dptr + str_length) = 0x00;

				// ----------

				if( SerDrvrVars_s[ pport ].UartRingBuffer_s.task_to_signal_when_string_found == WHEN_STRING_FOUND_NOTIFY_THE_COMM_MNGR_TASK )
				{
					// 3/17/2014 rmd : Clean queue message for good measure.
					memset( &cmtqs, 0x00, sizeof(COMM_MNGR_TASK_QUEUE_STRUCT) );
					
					cmtqs.dh = ldh;
					
					cmtqs.event = COMM_MNGR_EVENT_device_exchange_response_arrived;
		
					COMM_MNGR_post_event_with_details( &cmtqs );
				}
				else
				if( SerDrvrVars_s[ pport ].UartRingBuffer_s.task_to_signal_when_string_found == WHEN_STRING_FOUND_NOTIFY_THE_CODE_DISTRIBUTION_TASK )
				{
					// 3/17/2014 rmd : Clean queue message for good measure.
					memset( &cdtqs, 0x00, sizeof(CODE_DISTRIBUTION_TASK_QUEUE_STRUCT) );
					
					cdtqs.dh = ldh;
					
					cdtqs.event = CODE_DISTRIBUTION_EVENT_specified_termination_found;
		
					CODE_DISTRIBUTION_post_event_with_details( &cdtqs );
				}
				else
				if( SerDrvrVars_s[ pport ].UartRingBuffer_s.task_to_signal_when_string_found == WHEN_STRING_FOUND_NOTIFY_THE_CI_TASK )
				{
					// 3/17/2014 rmd : Clean queue message for good measure.
					memset( &citqs, 0x00, sizeof(CONTROLLER_INITIATED_TASK_QUEUE_STRUCT) );
					
					citqs.dh = ldh;
					
					citqs.event = CI_EVENT_process_string_found;
		
					CONTROLLER_INITIATED_post_event_with_details( &citqs );
				}
				else
				{
					Alert_Message( "TH HUNT incorrectly set up." );
				}
				
				// ----------

				// 3/13/2015 mpd : A comment in the "check_ring_buffer_for_a_crlf_delimited_string()" function notes 
				// the possibility of additional data arriving after the terminating indicator. That could happen here
				// as well. Turn off hunting so any subsequent data is ignored.
				SerDrvrVars_s[ pport ].UartRingBuffer_s.hunt_for_specified_termination = ( false );
				
			}  // END end_ptr > start_ptr

		}  // END if we found the terminating string
		else 
		{
			// 3/13/2015 mpd : Update the "string_index" for the next search. The search needs to start near the end of the
			// existing data in case part of the termination string has already arrived. Worst-case is 1 char of the
			// termination string arriving in the next buffer. So, set up to search for all but 1 char at the end of the current
			// buffer.

			// 3/17/2015 mpd : Unsigned comparisons do not always produce expected boolean results! Test each pair for positive
			// results before doing any math!
			
			// Are there more than enough characters in the buffer to match the termination string?
			if( SerDrvrVars_s[ pport ].UartRingBuffer_s.next > SerDrvrVars_s[ pport ].UartRingBuffer_s.th.chars_to_match ) 
			{
				// 3/17/2015 mpd : There are more than enough characters to match, but we have not found the terminating 
				// string yet. Calculate the worst-case start location for the next receive (one character into the next buffer).
				UNS_32 temp_index = ( SerDrvrVars_s[ pport ].UartRingBuffer_s.next - SerDrvrVars_s[ pport ].UartRingBuffer_s.th.chars_to_match + 1 );

				// 3/17/2015 mpd : Is the calculated start location deeper into the buffer?
				if( temp_index > SerDrvrVars_s[ pport ].UartRingBuffer_s.th.string_index )
				{
					// 3/17/2015 mpd : Update where the next search will start.
					SerDrvrVars_s[ pport ].UartRingBuffer_s.th.string_index = temp_index;
				}
				else
				{
					// 3/17/2015 mpd : No change. Don't do anything.
				}
			}
			else
			{
				// 3/13/2015 mpd : The data length is less than or equal to the length of termination string. Do not move
				// the search starting point.
			}
			//snprintf( alert_buffer, sizeof(alert_buffer), "Next: %d  Match: %d  Index %d", 
			//		  SerDrvrVars_s[ pport ].UartRingBuffer_s.next,
			//		  ( SerDrvrVars_s[ pport ].UartRingBuffer_s.th.chars_to_match + 1 ),
			//		  SerDrvrVars_s[ pport ].UartRingBuffer_s.th.string_index );
			//Alert_Message( alert_buffer );

		} // END did not find terminating string

	} // END if there is a terminating string to hunt for
}

/* ---------------------------------------------------------- */
static UNS_16 __bytes_available_to_process( unsigned short process_end_us, unsigned short process_start_us, unsigned short pbuffer_size_us )
{
	UNS_16  rv;

	// this returns the number of new bytes in the ring buffer that have not yet been processed
	//
	// remember the tail points to a location the does NOT YET contain a byte to xfer
	// 			the start points to a location that DOES contain a byte to xfer
	//
	if (process_start_us == process_end_us)
	{
		rv = 0;
	}
	else
	if (process_start_us < process_end_us)
	{
		rv = process_end_us - process_start_us;
	}
	else
	{
		// in the presence of a wrap around
		//
		rv = (pbuffer_size_us - process_start_us) + process_end_us;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/*
BOOLEAN A_ScanRingBufferForString( unsigned short MilliSecondsToWait, char *str )  {


BOOLEAN rv;

char bstr[ 20 ];

unsigned short hunt_len;  // length of wat we are hunting for

unsigned long TimeIn16;

	// This is one powerful function in that we don't leave it until either we see the string
	// in the new incoming data OR we timeout. Should only be called as part of
	// the phone modem programming. While programming the modem we are not running the
	// main loop. (Multi Tasking will change this behavior perhaps.)
	//
	// This function assumes that the buffer pointers have been moved to 0 prior to
	// its call and that the response will arrive before the pointers wrap around
	// at the end of the ring. WE DO NOT ACCOUNT FOR THE WRAP AT THE END OF THE RING.
	//
	// This function also assumes that the string we are hunting for is not longer than 19 chars.
	//
	//
	TimeIn16 = Time16;

	rv = FALSE;  // set to FALSE as the start up to accomodate timeout

	hunt_len = strlen( str );

	while ( (62*(Time16 - TimeIn16)) < MilliSecondsToWait )  {	  // each timer tick is 62.5ms (16Hz)

		BangWDT();

		// A_ringtail is where new incoming data is going to be put
		// A_ringindex is our processing pointer - we try to keep it within strlen(str) chars from A_ringtail

		// The (A_ringtail >= huntlen) term is to cover the startup. I feel more comfortable with the math
		// of the second term if I know at least this many characters have come in.
		//
		if ( (A_ringtail >= hunt_len) && (A_ringindex <= A_ringtail - hunt_len) ) {

			memcpy( &bstr, (char*)&A_ringbuffer[ A_ringindex ], hunt_len );

			bstr[ hunt_len ] = 0;  // null terminate

			if ( strcmp( bstr, str ) == 0 ) {

				// that means we found the string we're hunting for
				rv = TRUE;
				break;  // from the while loop

			}

			A_ringindex += 1;

		}

	}


	return( rv );

}
*/

/* ---------------------------------------------------------- */
static void xfer_bytes_to_another_port( unsigned short from_port, volatile UART_RING_BUFFER_s *urb, unsigned short to_port ) {

unsigned char	*ucp;

unsigned short	data_index_us, xfer_length_us;

unsigned short	tail_copy_us, packet_length_us;

DATA_HANDLE		ldh;

char			str_64[ 64 ];


	// tail is where new bytes are being added ... get a copy so we work with that ... it changes via interrupt
	tail_copy_us = urb->next;

	data_index_us = urb->dh.data_index;  // work with this variable so we don't have to keep doing the pointer reference

	// and now we can get a measure of the potential number of bytes to xfer - do it once
	xfer_length_us = __bytes_available_to_process( tail_copy_us, data_index_us, RING_BUFFER_SIZE );


	// watch out for 0 ... it is possible for the rcvd_data task to ask us to try and xfer bytes and in fact there
	// aren't any ... this is due to the 100ms delay and the semaphore that is produced with each interrupt. We could get
	// a semaphore then while waiting get one more then go xfer all the data loop around and find the semaphore again and
	// therefore try to do the xfer again but actually there are no more bytes to xfer...and that's okay and expected behaviour.
	if( xfer_length_us > 0 )
	{

		// Test how many xfers blocks are piled up on the port driver...if too many I would say we need to play with the
		// delay this returns to the calling task...that delay allows more byte to build up before we go and make a transfer
		// but that delay also DELAYS our response time to incoming data
		if (xmit_cntrl[to_port].xlist.count >= 24)
		{

			// should never see
			sprintf( str_64, "Port Xfer: %s too many blocks", port_names[ to_port ] );
			Alert_Message( str_64 );

		}
		else
		{

			// alocate the packet space
			//
			ldh.dlen = xfer_length_us;
			ldh.dptr = mem_malloc( ldh.dlen );

			//sprintf( str_64, "From %s to %s : block is %d", port_names[ from_port ], port_names[ to_port ], xfer_length_us );
			//Alert_Message( str_64 );

			// get a copy of the memory block ptr (packet start)
			//
			ucp = ldh.dptr;

			// sanity check
			packet_length_us = 0;

			while (data_index_us != tail_copy_us)
			{

				*ucp++ = urb->ring[ data_index_us++ ];

				packet_length_us += 1;

				if (data_index_us == RING_BUFFER_SIZE)
				{

					data_index_us = 0;

				}

			}

			if (packet_length_us == xfer_length_us)
			{

				urb->dh.data_index = data_index_us;

				AddCopyOfBlockToXmitList( to_port, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );

			}
			else
			{

				// sanity check
				Alert_Message( "Data Transfer: lengths don't match" );

			}


			mem_free( ldh.dptr );

		}

	}

}

#if 0
/* ---------------------------------------------------------- */
static void put_rxdata_into_ringbuffer( unsigned short port, unsigned char val8 )
{

UART_RING_BUFFER_s	*ptr_ring_buffer_s;		// for convience - easier code to read


	ptr_ring_buffer_s = &SerDrvrVars_s[ port ].UartRingBuffer_s;

	ptr_ring_buffer_s->ring[ ptr_ring_buffer_s->next ] = val8;

	ptr_ring_buffer_s->next++;
	if( ptr_ring_buffer_s->next == RING_BUFFER_SIZE )
	{

		ptr_ring_buffer_s->next = 0x00;

	}

	// if the tail ever catches up to index we've over run the buffer
	// so either make it bigger or check it more often
	// ONLY test for the mode we are active in cause for the inactive modes the tail will always eventually catch the index
	ptr_ring_buffer_s->ph_tail_caught_index = ( ptr_ring_buffer_s->hunt_for_packets && (ptr_ring_buffer_s->next == ptr_ring_buffer_s->ph.packet_index) );
	ptr_ring_buffer_s->dh_tail_caught_index = ( ptr_ring_buffer_s->hunt_for_data && (ptr_ring_buffer_s->next == ptr_ring_buffer_s->dh.data_index) );
	ptr_ring_buffer_s->sh_tail_caught_index = ( ptr_ring_buffer_s->hunt_for_specified_string && (ptr_ring_buffer_s->next == ptr_ring_buffer_s->sh.string_index) );

}
#endif

/* ---------------------------------------------------------- */
extern void ring_buffer_analysis_task( void *pvParameters )
{
	UNS_32							port, task_index;

	volatile UART_RING_BUFFER_s		*ptr_ring_buffer_s;		// for convience - easier code to read

	char							str_64[ 64 ];

	// 10/23/2012 rmd : Passed parameter is the address to the Task_Table entry used to create
	// this task. So we can figure out it's index to use when keeping the watchdog activity
	// monitor moving. And extract the port this task is responsible for.
	TASK_ENTRY_STRUCT	*tt_ptr;
	
	tt_ptr = pvParameters;
	
	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// 10/23/2012 rmd : And in this case the port we are responsible for is in the table.
	port = (UNS_32)tt_ptr->parameter;

	// ----------

	if( port >= UPORT_TOTAL_PHYSICAL_PORTS )
	{
		Alert_Message( "RING BUF TASK : bad paramter?" );
		for(;;);
	}

	// ----------

	if( rcvd_data_binary_semaphore[ port ] == NULL )
	{
		Alert_Message( "RING BUF TASK : NULL semaphore?" );
		for(;;);
	}

	// ----------

	// Take the semaphore once since it's created signaled.
	if( xSemaphoreTake( rcvd_data_binary_semaphore[ port ], 0 ) != pdTRUE )
	{
		Alert_Message( "RING BUF TASK : can't take semaphore?" );
	}

	// ----------

	// 2/12/2014 rmd : Init the port accumulators.
	memset( &SerDrvrVars_s[ port ].stats, 0, sizeof(UART_STATS_STRUCT) );

	// ----------

	// 3/3/2014 rmd : This next call to enable the packet hunting does two things. First it
	// completely 0's the whole serdrvrvars structure for this port (which sets up for any new
	// hunting mode with ring buffers restarted). And secondly, it enables packet hunting. Now
	// as it turns out the TPMicro port is going to call this function again to turn off packet
	// hunting and enable string hunting as it makes an attempt to see if the tpmicro is in ISP
	// mode.
	RCVD_DATA_enable_hunting_mode( port, PORT_HUNT_FOR_PACKETS, NOT_STRING_HUNTING );
	
	// ----------

	ptr_ring_buffer_s = &SerDrvrVars_s[ port ].UartRingBuffer_s;  // create a ptr to the ring buffer controlling struct
	
	while( (true) )
	{
		// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
		if( xSemaphoreTake( rcvd_data_binary_semaphore[ port ], MS_to_TICKS( 500 ) ) == pdTRUE )
		{
			// 2/12/2014 rmd : Don't need to test operational mode when looking if buffer as that is
			// considered when setting the error.
			if( ptr_ring_buffer_s->ph_tail_caught_index )
			{
				ptr_ring_buffer_s->ph_tail_caught_index = (false);

				snprintf( str_64, sizeof(str_64), "%s : PH Tail caught INDEX", port_names[ port ] );

				Alert_Message( str_64 );
			}

			// ----------
			
			// 2/12/2014 rmd : Don't need to test operational mode when looking if buffer as that is
			// considered when setting the error.
			if( ptr_ring_buffer_s->dh_tail_caught_index == TRUE )
			{
				ptr_ring_buffer_s->dh_tail_caught_index = (false);

				snprintf( str_64, sizeof(str_64), "%s : DH Tail caught INDEX", port_names[ port ] );

				Alert_Message( str_64 );
			}

			// ----------
			
			// 2/12/2014 rmd : Don't need to test operational mode when looking if buffer as that is
			// considered when setting the error.
			if( ptr_ring_buffer_s->sh_tail_caught_index == TRUE )
			{
				ptr_ring_buffer_s->sh_tail_caught_index = (false);

				snprintf( str_64, sizeof(str_64), "%s : SH Tail caught INDEX", port_names[ port ] );

				Alert_Message( str_64 );
			}

			// 3/13/2015 mpd : The TH struct was added for SR radio menu buffers. Handle alerts just like other types.
			if( ptr_ring_buffer_s->th_tail_caught_index == TRUE )
			{
				ptr_ring_buffer_s->th_tail_caught_index = (false);

				snprintf( str_64, sizeof(str_64), "%s : TH Tail caught INDEX", port_names[ port ] );

				Alert_Message( str_64 );
			}

			// ----------

			// HUNT FOR PACKETS
			if( ptr_ring_buffer_s->hunt_for_packets )
			{
				check_ring_buffer_for_CalsenseTPL_packet( port );
			}

			// ----------

			// HUNT FOR A SPECIFIC STRING
			if( ptr_ring_buffer_s->hunt_for_specified_string )
			{
				check_ring_buffer_for_a_specified_string( port );
			}

			// ----------

			// HUNT FOR A CRLF DELIMITED STRING
			if( ptr_ring_buffer_s->hunt_for_crlf_delimited_string )
			{
				check_ring_buffer_for_a_crlf_delimited_string( port );
			}

			// HUNT FOR A SPECIFIED TERMINATION STRING
			if( ptr_ring_buffer_s->hunt_for_specified_termination )
			{
				check_ring_buffer_for_a_specified_termination( port );
			}

			// ----------

			// ON THE DATA HUNT
			if( ptr_ring_buffer_s->hunt_for_data )
			{
				if( port == UPORT_TP )
				{
					xfer_bytes_to_another_port( port, ptr_ring_buffer_s, UPORT_A );
					//xfer_bytes_to_another_port( port, ptr_ring_buffer_s, UPORT_B );
					//xfer_bytes_to_another_port( port, ptr_ring_buffer_s, UPORT_RRE );
					//xfer_bytes_to_another_port( ptr_ring_buffer_s, UPORT_USB );
				}
				else
				if( port == UPORT_A )
				{
					xfer_bytes_to_another_port( port, ptr_ring_buffer_s, UPORT_TP );
					//xfer_bytes_to_another_port( port, ptr_ring_buffer_s, UPORT_B );
					//xfer_bytes_to_another_port( port, ptr_ring_buffer_s, UPORT_RRE );
					//xfer_bytes_to_another_port( ptr_ring_buffer_s, UPORT_USB );
				}
				else
				if( port == UPORT_B )
				{
					//xfer_bytes_to_another_port( port, ptr_ring_buffer_s, UPORT_A );
					//xfer_bytes_to_another_port( port, ptr_ring_buffer_s, UPORT_B );
				}
				else
				if( port == UPORT_RRE )
				{
					//xfer_bytes_to_another_port( port, ptr_ring_buffer_s, UPORT_A );
					xfer_bytes_to_another_port( port, ptr_ring_buffer_s, UPORT_B );
				}

			}  // of if hunting for data

			// ----------

		}  // of waiting for the semaphore

		// ----------
		
		// 10/23/2012 rmd : Keep the activity indication going.
		task_last_execution_stamp[ task_index ] = xTaskGetTickCount();

		// ----------
		
	}  // of the while forever of the task

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

