/*  file = device_EN_UDS1100.c                06.28.2013  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	<string.h>

#include	"device_en_uds1100.h"

#include	"gpio_setup.h"

#include	"alerts.h"

#include	"serial.h"

#include	"configuration_controller.h"

#include	"serport_drvr.h"

#include	"device_generic_gr_card.h"

#include	"speaker.h"

#include	"comm_mngr.h"

#include	"rcvd_data.h"

#include	"cs_mem.h"

#include	"controller_initiated.h"




/* ---------------------------------------------------------- */
/*  			Lantronix Hardware Configuration			  */
/* ---------------------------------------------------------- */

// 4/27/2015 mpd : Development progress has been improved a great deal by the use of a 25-pin D-Sub Y-cable that 
// provides device feedback in a PuTTY window as the SC3000 controller is communitating in real time. The PuTTY
// view is a big improvement over searching RCVD_DATA response buffers in debug to see what the device is doing.
// To make the Y-cable work, the PuTTY configuration needs to be slightly modified or it will block controller
// commands from reaching the device.
// 
// NOTE: The UDS1100 connection DOES NOT USE the 9-pin null modem cable. It uses a standard 25-pin cable. Isolation of
// the 2 wires needed by PuTTY can be done with 9-pin reducers with the 2-wire (blue and black) bridge connector. 
// 
// Only 2 wires from the modem cable should connect to the PC COM port through a standard 25-pin null modem cable. 9-pin D-Sub should connect to the PC COM port through a null modem cable
// TX	Pin 3	Brown (25-pin connector)
// Grd	Pin 7	Green (25-pin connector)
//
// Using the 9-pin reducers and the bridge connector:
// TX	Pin 2	Blue (engineering's bridge connector)
// Grd	Pin 5	Black (engineering's bridge connector)
//
// CONNECTION->SERIAL:
//  Speed: 9600
//  Data Bits: 8
//  Stop Bits: 1
//  Parity: None
//  Flow Control: None
// 
// TERMINAL:
//  Line Decipline Options->Local Echo: Force Off			(normally: Auto)
//  Line Decipline Options->Local Line Editing: Force Off	(normally: Auto)

/* ---------------------------------------------------------- */
/*  					Global Structures   				  */
/* ---------------------------------------------------------- */

// The "DEV_STATE_STRUCT" struct contains fields that apply to all the EN_PROGRAMMABLE_VALUES_STRUCTs. 
EN_DETAILS_STRUCT en_details;

// 3/23/2015 mpd : NOTE: No functions should directly modify any PVS fields. Use the pointers in the "dev_state" struct! 
 
// 6/9/2015 mpd : The "dynamic", "active", and "write" programmable values structures created for SR have been 
// simplified within the "device_common.c" code module. We now have just "active" and "static" structures.

// 6/9/2015 mpd : These are the programmable variables structures used by EN devices. READ operations 
// load the "active_pvs" with values obtained from the device. These values are copied to the GUI for users to see. 
// All the user variables are copied back to the "active_pvs" values when the "Save Changes" button is selected. 
// Just before programming the device, the "active_pvs" is copied to the "static_pvs" which will be used to program
// the device. After the WRITE, a READ is performed to verify the results (using the "active_pvs" which again gets
// copied to the user screen).
EN_PROGRAMMABLE_VALUES_STRUCT en_active_pvs;
EN_PROGRAMMABLE_VALUES_STRUCT en_static_pvs;

// 5/22/2015 mpd : Now that MONITOR MODE reads have been moved to the diagnostic screen, we need a SETUP MODE read after
// every WRITE to verify the settings successfully made it into the radio.
//BOOL_32 en_setup_mode_after_a_write = false;
	 
/* ---------------------------------------------------------- */
/*  			 String Extraction and Analysis 			  */
/* ---------------------------------------------------------- */

//#define EN_DEBUG_ANALYSIS		

void en_analyze_server_info( DEV_STATE_STRUCT *dev_state )
{
	EN_DETAILS_STRUCT *en_details;

	#ifdef EN_DEBUG_ANALYSIS
	Alert_Message( "en_analyze_server_info" );
	#endif

	// 4/16/2015 mpd : Send progress info to the GUI.
	strlcpy( dev_state->info_text, "Reading device settings...", sizeof( dev_state->info_text ) );                
	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );

	en_details  = dev_state->en_details;

	// 4/30/2015 mpd : The Lantronix model response is a long string extracted in the next function that looks like:
	// "Device Server Plus+! (Firmware Code:UA)". That's really not what we want to see. The "Model" string we want is
	// "UDS1100-IAP" which is on the very first line of the response. It will be called "Device" to differentiate it
	// from the Lantronix "Model" string.

	// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
	dev_extract_delimited_text_from_buffer( dev_state->resp_ptr, " ", EN_SERVER_INFO_DEVICE_STR, EN_SERVER_INFO_DEVICE_OFFSET, sizeof( en_details->device ), en_details->device, "DEVICE" );

	dev_extract_text_from_buffer( dev_state->resp_ptr, EN_SERVER_INFO_MAC_ADD_STR, EN_SERVER_INFO_MAC_ADD_OFFSET, sizeof( en_details->mac_address ), en_details->mac_address, "MAC ADDRESS" );

	// 4/22/2015 mpd : This field is of varying length. Use extrction with a delimiter.
	dev_extract_delimited_text_from_buffer( dev_state->resp_ptr, " ", EN_SERVER_INFO_SW_VERSION_STR, EN_SERVER_INFO_SW_VERSION_OFFSET, sizeof( dev_state->firmware_version ), dev_state->firmware_version, "SW VERSION" );

}

void en_analyze_basic_info( DEV_STATE_STRUCT *dev_state )
{
	EN_DETAILS_STRUCT *en_details;
	EN_PROGRAMMABLE_VALUES_STRUCT *active_pvs;
	char  temp_text[ EN_BASIC_INFO_IP_ADD_LEN ];
	char *temp_ptr;

	#ifdef EN_DEBUG_ANALYSIS
	Alert_Message( "en_analyze_basic_info" );
	#endif

	en_details  = dev_state->en_details;
	active_pvs  = dev_state->en_active_pvs;

	// 5/1/2015 mpd : These fields are not currently needed by EN Programming. They have been tested and are functional.
	//dev_extract_text_from_buffer( dev_state->resp_ptr, EN_SERVER_INFO_MODEL_STR,   EN_SERVER_INFO_MODEL_OFFSET,   sizeof( dev_state->model ),         dev_state->model,         "MODEL" );
	//dev_extract_text_from_buffer( dev_state->resp_ptr, EN_SERVER_INFO_FW_CODE_STR, EN_SERVER_INFO_FW_CODE_OFFSET, sizeof( dev_state->firmware_code ), dev_state->firmware_code, "FW CODE" );
	//dev_extract_text_from_buffer( dev_state->resp_ptr, EN_BASIC_INFO_HW_STR,       EN_BASIC_INFO_HW_OFFSET,       sizeof( dev_state->hardware ),      dev_state->hardware,      "HARDWARE" );

	// 4/24/2015 mpd : IP address will have a "-" before the first octet if DHCP is enabled. The delimiter changes too. 
	// Put the IP text in a local buffer for further analysis.
	dev_extract_text_from_buffer( dev_state->resp_ptr, EN_BASIC_INFO_IP_ADD_STR, EN_BASIC_INFO_IP_ADD_OFFSET, sizeof( temp_text ), temp_text, "IP ADDRESS" );

	// 4/24/2015 mpd : Is there a "-"?
	if( strncmp( temp_text, EN_BASIC_INFO_IP_ADD_HYPHEN_STR, 1 ) == NULL )
	{
		// 4/24/2015 mpd : The text is likely "IP addr - 0.0.0.0/DHCP/BOOTP/AutoIP, no gateway set". Use extraction anyway just
		// in case it's something different.

		// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
		dev_extract_delimited_text_from_buffer( dev_state->resp_ptr, "/", EN_BASIC_INFO_IP_ADD_STR, EN_BASIC_INFO_IP_ADD_HYPHEN_OFFSET, sizeof( active_pvs->ip_address ), active_pvs->ip_address, "IP ADDRESS" );
	}
	else
	{
		// 4/24/2015 mpd : The text looks something like: "IP addr 10.20.30.40, gateway 200.44.80.90,netmask 255.255.255.0". Use
		// extraction with a ",' delimiter.

		// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
		dev_extract_delimited_text_from_buffer( dev_state->resp_ptr, ",", EN_BASIC_INFO_IP_ADD_STR, EN_BASIC_INFO_IP_ADD_OFFSET, sizeof( active_pvs->ip_address ), active_pvs->ip_address, "IP ADDRESS" );
	}

	dev_extract_ip_octets( active_pvs->ip_address, active_pvs->ip_address_1, active_pvs->ip_address_2, active_pvs->ip_address_3, active_pvs->ip_address_4 );

	// 5/21/2015 mpd : Only octets 1 and 2 need to be zero to enable DHCP. Don't test for "0.0.0.0".
	if( ( ( strncmp( active_pvs->ip_address_1, EN_NULL_IP_OCTET_STR, sizeof( active_pvs->ip_address_1 ) ) ) == NULL ) &&
		( ( strncmp( active_pvs->ip_address_2, EN_NULL_IP_OCTET_STR, sizeof( active_pvs->ip_address_2 ) ) ) == NULL ) )
	{
		dev_state->dhcp_enabled = true;
	}
	else
	{
		dev_state->dhcp_enabled = false;
	}

	// 4/24/2015 mpd : Is there a value for gateway?
	if( ( strstr( dev_state->resp_ptr, EN_BASIC_INFO_GW_NOT_SET_STR ) ) != NULL )
	{
		// 4/22/2015 mpd : No gateway is set.
		strlcpy( active_pvs->gw_address, EN_NULL_IP_ADD_STR, sizeof(active_pvs->gw_address ) );
	}
	else if( ( strstr( dev_state->resp_ptr, EN_BASIC_INFO_GW_ADD_STR ) ) != NULL )
	{
		// 4/22/2015 mpd : This gateway field is of varying length. Use extrction with a delimiter.
		dev_extract_delimited_text_from_buffer( dev_state->resp_ptr, ",", EN_BASIC_INFO_GW_ADD_STR, EN_BASIC_INFO_GW_ADD_OFFSET, sizeof( active_pvs->gw_address ), active_pvs->gw_address, "GW ADDRESS" );
	}
	else
	{
		// 4/22/2015 mpd : Unknown gateway format.
		strlcpy( active_pvs->gw_address, EN_NULL_IP_ADD_STR, sizeof( active_pvs->gw_address ) );
	}

	dev_extract_ip_octets( active_pvs->gw_address, active_pvs->gw_address_1, active_pvs->gw_address_2, active_pvs->gw_address_3, active_pvs->gw_address_4 );

	// 9/24/2015 mpd : FogBugz #3178: The mask is needed after all! Reactivate this code!
	// 5/1/2015 mpd : These fields are not currently needed by EN Programming. They have been tested and are functional.
	// 4/22/2015 mpd : Is there a value for mask?
	if( ( strstr( dev_state->resp_ptr, EN_BASIC_INFO_MASK_STR ) ) != NULL )
	{
		// 4/22/2015 mpd : This field is of varying length. Use extrction with a delimiter.
		dev_extract_delimited_text_from_buffer( dev_state->resp_ptr, EN_CR_STR, EN_BASIC_INFO_MASK_STR, EN_BASIC_INFO_MASK_OFFSET, sizeof( active_pvs->mask ), active_pvs->mask, "MASK" );
	}
	else
	{
		// 4/22/2015 mpd : No mask info available.
		strlcpy( active_pvs->mask, EN_DEFAULT_IP_ADD_STR, sizeof( active_pvs->mask ) );
	}

	// 9/24/2015 mpd : FogBugz #3178: We don't care about the individual octets. Leave them commented out.
	//dev_extract_ip_octets( active_pvs->mask, active_pvs->mask_1, active_pvs->mask_2, active_pvs->mask_3, active_pvs->mask_4 );

	// 9/24/2015 mpd : FogBugz #3178: This is what all the work was about. The counting function was written to be
	// generic. Most devices care about the number of bits set, so that's what is returned. UDS1100 cares about the 
	// ( false ) bits. Do the math.
	en_details->mask_bits = ( XE_XN_DEVICE_NETMASK_MAX - dev_count_true_mask_bits( active_pvs->mask ) );

	// 9/24/2015 mpd : This should never happen. 
 	if( ( en_details->mask_bits < EN_PROGRAMMING_NETMASK_MIN ) || ( en_details->mask_bits > EN_PROGRAMMING_NETMASK_MAX ) )
	{
		en_details->mask_bits = EN_PROGRAMMING_NETMASK_DEFAULT;
	}

	// 6/12/2015 mpd : V6.11.0.0 added "DNS Server not set" before the DHCP name line. Go past that, or we will get
	// the wrong result for the DHCP name "not set" comparison.

	// 4/24/2015 mpd : The DHCP name line is completely missing if DHCP is disabled. See if it exists before attempting to extract it.
	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, EN_BASIC_INFO_DHCP_NAME_STR, dev_state->resp_len ) ) != NULL )
	{
		// 4/27/2015 mpd : The name line exists. Clear the destination.
		memset( active_pvs->dhcp_name, 0x00, EN_BASIC_INFO_DHCP_NAME_LEN );

		if( ( strstr( temp_ptr, EN_BASIC_INFO_NAME_NOT_SET_STR ) ) != NULL )
		{
			if( en_details->dhcp_name_not_set )
			{
				// 4/22/2015 mpd : No DHCP name is set. Supply the MAC address as the recommended default.
				strlcpy( active_pvs->dhcp_name, en_details->mac_address, sizeof( active_pvs->dhcp_name ) );

				// 6/11/2015 mpd : This flag tells us it's safe to stomp on any DHCP name that existed in the device the 
				// last time DHCP was enabled. Or conversely, it prevents us from stomping on a name that was temporarily 
				// hidden from view during the current session. 
				en_details->dhcp_name_not_set = false;
			}
		}
		else
		{
			// 4/22/2015 mpd : This field is of varying length. Use extrction with a delimiter.
			dev_extract_delimited_text_from_buffer( temp_ptr, EN_CR_STR, EN_BASIC_INFO_DHCP_NAME_STR, EN_BASIC_INFO_DHCP_NAME_OFFSET, sizeof( active_pvs->dhcp_name ), active_pvs->dhcp_name, "DHCP NAME" );

			// 6/11/2015 mpd : This flag tells us it's safe to stomp on any DHCP name that existed in the device the 
			// last time DHCP was enabled. Or conversely, it prevents us from stomping on a name that was temporarily 
			// hidden from view during the current session. 
			en_details->dhcp_name_not_set = false;
		}
	}
	// IMPLIED ELSE: DHCP is disabled

		// 6/11/2015 mpd : The current response does not include a DHCP name. One may have existed during this session.
		// Don't clear the string variable or modify the flag.

}

void en_analyze_channel_info( DEV_STATE_STRUCT *dev_state )
{
	EN_PROGRAMMABLE_VALUES_STRUCT *active_pvs;
	char  temp_text[ EN_CHANNEL_INFO_AUTO_INCR_LEN ];
	char *channel_ptr;

	#ifdef EN_DEBUG_ANALYSIS
	Alert_Message( "en_analyze_channel_info" );
	#endif

	active_pvs  = dev_state->en_active_pvs;

	// 4/23/2015 mpd : The Lantronix response has duplicate key words (eg. "Port") that screw up extraction. Start at the 
	// beginning of the channel section instead of at the beginning of the response buffer.
	if( ( channel_ptr = strstr( dev_state->resp_ptr, EN_CHANNEL_INFO_TITLE_STR ) ) != NULL )
	{
		// 4/22/2015 mpd : This field is of varying length. Use extrction with a delimiter.
		dev_extract_delimited_text_from_buffer( channel_ptr, ",", EN_CHANNEL_INFO_BAUD_RATE_STR, EN_CHANNEL_INFO_BAUD_RATE_OFFSET, sizeof( active_pvs->baud_rate ), active_pvs->baud_rate, "BAUD RATE" );

		dev_extract_text_from_buffer( channel_ptr, EN_CHANNEL_INFO_IF_MODE_STR, EN_CHANNEL_INFO_IF_MODE_OFFSET, sizeof( active_pvs->if_mode ), active_pvs->if_mode, "IF MODE" );
		dev_extract_text_from_buffer( channel_ptr, EN_CHANNEL_INFO_FLOW_STR,    EN_CHANNEL_INFO_FLOW_OFFSET,    sizeof( active_pvs->flow ),    active_pvs->flow,    "FLOW" );

		// 4/22/2015 mpd : This field is of varying length. Use extrction with a delimiter.
		dev_extract_delimited_text_from_buffer( channel_ptr, EN_CR_STR, EN_CHANNEL_INFO_PORT_STR, EN_CHANNEL_INFO_PORT_OFFSET, sizeof( active_pvs->port ), active_pvs->port, "PORT" );

		dev_extract_text_from_buffer( channel_ptr, EN_CHANNEL_INFO_CONN_MODE_STR, EN_CHANNEL_INFO_CONN_MODE_OFFSET, sizeof( active_pvs->conn_mode ), active_pvs->conn_mode, "CONN MODE" );

		// 4/22/2015 mpd : The Auto Increment response text is "enabled" or "disabled". For the WRITE operation, we need a 
		// "Y" or "N". Use a boolean to store the value.
		dev_extract_text_from_buffer( channel_ptr, EN_CHANNEL_INFO_AUTO_INCR_STR, EN_CHANNEL_INFO_AUTO_INCR_OFFSET, sizeof( temp_text ), temp_text, "AUTO INCR" );

		if( strncmp( temp_text, EN_CHANNEL_INFO_ENABLED_STR, EN_CHANNEL_INFO_ENABLED_LEN ) == NULL )
		{
			active_pvs->auto_increment = true;
		}
		else if( strncmp( temp_text, EN_CHANNEL_INFO_DISABLED_STR, EN_CHANNEL_INFO_DISABLED_LEN ) == NULL )
		{
			active_pvs->auto_increment = false;
		}
		else
		{
			#ifdef EN_VERBOSE_ALERTS
				Alert_Message( "Analyze Auto Increment failure" );
			#else
				Alert_Message( "EN1" );
			#endif
		}

		if( ( strstr( channel_ptr, EN_CHANNEL_INFO_RIP_NOT_SET_STR ) ) != NULL )
		{
			// 4/22/2015 mpd : No remote IP address is set.
			strlcpy( active_pvs->remote_ip_address, EN_NULL_IP_ADD_STR, sizeof( active_pvs->remote_ip_address ) );
		}
		else
		{
			// 4/22/2015 mpd : This field is of varying length. Use extrction with a delimiter.
			dev_extract_delimited_text_from_buffer( channel_ptr, ",", EN_CHANNEL_INFO_REMOTE_IP_STR, EN_CHANNEL_INFO_REMOTE_IP_OFFSET, sizeof( active_pvs->remote_ip_address ), active_pvs->remote_ip_address, "REMOTE IP ADD" );
		}

		dev_extract_ip_octets( active_pvs->remote_ip_address, active_pvs->remote_ip_address_1, active_pvs->remote_ip_address_2, active_pvs->remote_ip_address_3, active_pvs->remote_ip_address_4 );

		// 4/22/2015 mpd : This field is of varying length. Use extrction with a delimiter.
		dev_extract_delimited_text_from_buffer( channel_ptr, EN_CR_STR, EN_CHANNEL_INFO_REMOTE_PORT_STR, EN_CHANNEL_INFO_REMOTE_PORT_OFFSET, sizeof( active_pvs->remote_port ), active_pvs->remote_port, "REMOTE PORT" );

		dev_extract_text_from_buffer( channel_ptr, EN_CHANNEL_INFO_DISCONN_MODE_STR, EN_CHANNEL_INFO_DISCONN_MODE_OFFSET, sizeof( active_pvs->disconn_mode ), active_pvs->disconn_mode, "DISCONN MODE" );
		dev_extract_text_from_buffer( channel_ptr, EN_CHANNEL_INFO_FLUSH_MODE_STR,   EN_CHANNEL_INFO_FLUSH_MODE_OFFSET,   sizeof( active_pvs->flush_mode ),   active_pvs->flush_mode,   "FLUSH MODE" );
	}
	else
	{
		#ifdef EN_VERBOSE_ALERTS
			Alert_Message( "Analyze channel section failure" );
		#else
			Alert_Message( "EN2" );
		#endif
	}

}

void en_analyze_setup_mode( DEV_STATE_STRUCT *dev_state )
{
	#ifdef EN_DEBUG_ANALYSIS
	Alert_Message( "en_analyze_setup_mode" );
	#endif

	// 4/23/2015 mpd : Analysis functions have been broken into sections (basic, security, channel, and expert) for
	// maintainability. Unfortunately, the read list needs a single function to call for analysis of the huge initial
	// response. This is it.
	en_analyze_basic_info( dev_state );
	en_analyze_channel_info( dev_state );

	// 4/23/2015 mpd : We don't care about "Security" or "Expert" settings at this time. Ignore them.

}

// 4/22/2015 mpd : This is a very useful MONITOR MODE response that is not currently in use. It follows the "NC"
// (network connection) command and provides the only way to get valid network settings if DHCP is enabled. Note that
// the values are stored in a separate location from the "Basic Parameters" values. These are separate sources, so the
// destinations have been kept separate as well. 
void en_analyze_monitor_mode_nc( DEV_STATE_STRUCT *dev_state )
{
    #ifdef EN_SUPPORT_MONITOR_MODE
	EN_DETAILS_STRUCT *en_details;

	#ifdef EN_DEBUG_ANALYSIS
	Alert_Message( "en_analyze_monitor_mode_nc" );
	#endif

	// 4/30/2015 mpd : This message is on the screen too briefly to read. Save the effort.
	// 4/16/2015 mpd : Send progress info to the GUI. There are no WRITE operations in MONITOR MODE. 
	//strlcpy( dev_state->info_text, "Reading device monitor mode NC...", sizeof( dev_state->info_text ) );    
	//COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );

	en_details  = dev_state->en_details;

	dev_extract_text_from_buffer( dev_state->resp_ptr, EN_MON_MODE_IP_ADD_STR, EN_MON_MODE_IP_ADD_OFFSET, sizeof( en_details->mon_ip_address ), en_details->mon_ip_address, "MON IP ADD" );
	dev_extract_text_from_buffer( dev_state->resp_ptr, EN_MON_MODE_GW_ADD_STR, EN_MON_MODE_GW_ADD_OFFSET, sizeof( en_details->mon_gw_address ), en_details->mon_gw_address, "MON GW ADD" );
	dev_extract_text_from_buffer( dev_state->resp_ptr, EN_MON_MODE_MASK_STR,   EN_MON_MODE_MASK_OFFSET,   sizeof( en_details->mon_mask ),       en_details->mon_mask,       "MON MASK" );

	dev_extract_ip_octets( en_details->mon_ip_address, en_details->mon_ip_address_1, en_details->mon_ip_address_2, en_details->mon_ip_address_3, en_details->mon_ip_address_4 );
	dev_extract_ip_octets( en_details->mon_gw_address, en_details->mon_gw_address_1, en_details->mon_gw_address_2, en_details->mon_gw_address_3, en_details->mon_gw_address_4 );
	dev_extract_ip_octets( en_details->mon_mask,       en_details->mon_mask_1,       en_details->mon_mask_2,       en_details->mon_mask_3,       en_details->mon_mask_4 );
	#endif

}

// 4/22/2015 mpd : This is MONITOR MODE response of questionable usefullness. It follows the "GM" (get MAC address)
// command and provides an alternate way to get the MAC address. Note that the value is a different format (this one
// includes ":" to separate elements) and it is stored in a separate location from the SETUP MODE value.
void en_analyze_monitor_mode_gm( DEV_STATE_STRUCT *dev_state )
{
    #ifdef EN_SUPPORT_MONITOR_MODE

	#ifdef EN_DEBUG_ANALYSIS
	Alert_Message( "en_analyze_monitor_mode_gm" );
	#endif

	// 4/30/2015 mpd : This message is on the screen too briefly to read. Save the effort.
	// 4/16/2015 mpd : Send progress info to the GUI. There are no WRITE operations in MONITOR MODE. 
	//strlcpy( dev_state->info_text, "Reading device monitor mode GM...", sizeof( dev_state->info_text ) );    
	//COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );

	// 5/1/2015 mpd : These fields are not currently needed by EN Programming. They have been tested and are functional.
	//dev_extract_text_from_buffer( dev_state->resp_ptr, EN_MON_MODE_MAC_ADD_STR, EN_MON_MODE_MAC_ADD_OFFSET, sizeof( dev_state->mon_mac_address ), dev_state->mon_mac_address, "MON MAC ADD" );
	#endif

}

void en_verify_active_values_match_static_values( DEV_STATE_STRUCT *dev_state )
{
	EN_PROGRAMMABLE_VALUES_STRUCT *active_pvs;
	EN_PROGRAMMABLE_VALUES_STRUCT *static_pvs;

	#ifdef EN_DEBUG_WRITE
	Alert_Message( "en_verify_active_values_match_static_values" );
	#endif

	// 5/22/2015 mpd : This function is now called by the READ that immediately follows every WRITE. The WRITE used 
	// the "static_pvs" to program the device. The "active_pvs" contains the post-WRITE values we just read from the 
	// device.

	active_pvs = dev_state->en_active_pvs;
	static_pvs = dev_state->en_static_pvs;

	// 4/13/2015 mpd : Assume the best.
	dev_state->programming_successful = true;

	// BASIC PARAMETERS
	if( !dev_state->dhcp_enabled )
	{
		// 5/22/2015 mpd : DHCP is disabled. This is the only time we have valid IP address and gateway strings from the 
		// SETUP MODE READ. Verify them.
		if( strncmp( static_pvs->ip_address, active_pvs->ip_address, strlen( static_pvs->ip_address ) ) != NULL ) 
		{
			dev_state->programming_successful = false;
			#ifdef EN_VERBOSE_ALERTS
				Alert_Message( "Write Failed: IP Address mismatch" );
			#else
				Alert_Message( "EN3" );
			#endif
		}

		if( strncmp( static_pvs->gw_address, active_pvs->gw_address, strlen( static_pvs->gw_address ) ) != NULL ) 
		{
			dev_state->programming_successful = false;
			#ifdef EN_VERBOSE_ALERTS
				Alert_Message( "Write Failed: Gateway Address mismatch" );
			#else
				Alert_Message( "EN4" );
			#endif
		}

		// 9/24/2015 mpd : FogBugz #3178: EasyGUI currently uses "GuiVar_ENNetmask", but is leaving 
		// "GuiVar_ENSubnetMask" all NULL. Post write verification will have to be scrapped since we do not have pre and 
		// post copies of the mask bits. 
		// 5/1/2015 mpd : These fields are not currently needed by EN Programming. They have been tested and are functional.
		//if( strncmp( static_pvs->mask, active_pvs->mask, EN_BASIC_INFO_MASK_LEN ) != NULL ) 
		//{
		//	dev_state->programming_successful = false;
		//	#ifdef EN_VERBOSE_ALERTS
		//		Alert_Message( "Write Failed: Mask mismatch" );
		//	#else
		//		Alert_Message( "EN5" );
		//	#endif
		//}
	}
	// 5/22/2015 mpd : DHCP is enabled. Verify the name. Just verify the length we wrote.
	else if( strncmp( static_pvs->dhcp_name, active_pvs->dhcp_name, strlen( static_pvs->dhcp_name ) ) != NULL ) 
	{
		dev_state->programming_successful = false;
		#ifdef EN_VERBOSE_ALERTS
			Alert_Message( "Write Failed: DHCP Name mismatch" );
		#else
			Alert_Message( "EN6" );
		#endif
	}
	
	// CHANNEL PARAMETERS

	// 5/1/2015 mpd : This setting was already verified against the required value in "en_final_device_analysis()". 
	// Don't repeat the work here. 
	//if( strncmp( static_pvs->baud_rate, active_pvs->baud_rate, EN_CHANNEL_INFO_BAUD_RATE_LEN ) != NULL ) 
	//{
	//	dev_state->programming_successful = false;
	//	#ifdef EN_VERBOSE_ALERTS
	//		Alert_Message( "Write Failed: Baud Rate mismatch" );
	//	#else
	//		Alert_Message( "EN7" );
	//	#endif
	//}

	// 5/1/2015 mpd : This setting was already verified against the required value in "en_final_device_analysis()". 
	// Don't repeat the work here. 
	//if( strncmp( static_pvs->if_mode, active_pvs->if_mode, EN_CHANNEL_INFO_IF_MODE_LEN ) != NULL ) 
	//{
	//	dev_state->programming_successful = false;
	//	#ifdef EN_VERBOSE_ALERTS
	//		Alert_Message( "Write Failed: IF Mode mismatch" );
	//	#else
	//		Alert_Message( "EN8" );
	//	#endif
	//}

	// 5/1/2015 mpd : This setting was already verified against the required value in "en_final_device_analysis()". 
	// Don't repeat the work here. 
	//if( strncmp( static_pvs->flow, active_pvs->flow, EN_CHANNEL_INFO_FLOW_LEN ) != NULL ) 
	//{
	//	dev_state->programming_successful = false;
	//	#ifdef EN_VERBOSE_ALERTS
	//		Alert_Message( "Write Failed: Flow mismatch" );
	//	#else
	//		Alert_Message( "EN9" );
	//	#endif
	//}

	// 5/1/2015 mpd : This setting was already verified against the required value in "en_final_device_analysis()". 
	// Don't repeat the work here. 
	//if( strncmp( static_pvs->port, active_pvs->port, EN_CHANNEL_INFO_PORT_LEN ) != NULL ) 
	//{
	//	dev_state->programming_successful = false;
	//	#ifdef EN_VERBOSE_ALERTS
	//		Alert_Message( "Write Failed: Port mismatch" );
	//	#else
	//		Alert_Message( "EN10" );
	//	#endif
	//}

	// 5/1/2015 mpd : This setting was already verified against the required value in "en_final_device_analysis()". 
	// Don't repeat the work here. 
	//if( strncmp( static_pvs->conn_mode, active_pvs->conn_mode, EN_CHANNEL_INFO_CONN_MODE_LEN ) != NULL ) 
	//{
	//	dev_state->programming_successful = false;
	//	#ifdef EN_VERBOSE_ALERTS
	//		Alert_Message( "Write Failed: Connection Mode mismatch" );
	//	#else
	//		Alert_Message( "EN11" );
	//	#endif
	//}

	if( static_pvs->auto_increment != active_pvs->auto_increment ) 
	{
		dev_state->programming_successful = false;
		#ifdef EN_VERBOSE_ALERTS
			Alert_Message( "Write Failed: Auto Increment mismatch" );
		#else
			Alert_Message( "EN12" );
		#endif
	}

	if( strncmp( static_pvs->remote_ip_address, active_pvs->remote_ip_address, strlen( static_pvs->remote_ip_address ) ) != NULL ) 
	{
		dev_state->programming_successful = false;
		#ifdef EN_VERBOSE_ALERTS
			Alert_Message( "Write Failed: Remote IP Address mismatch" );
		#else
			Alert_Message( "EN13" );
		#endif
	}

	// 5/26/2015 mpd : The write string contains a CR that won't be in the string we read back. Compare 1 byte less.
	if( strncmp( static_pvs->remote_port, active_pvs->remote_port, ( strlen( static_pvs->remote_port ) - 1 ) ) != NULL ) 
	{
		dev_state->programming_successful = false;
		#ifdef EN_VERBOSE_ALERTS
			Alert_Message( "Write Failed: Remote Port mismatch" );
		#else
			Alert_Message( "EN14" );
		#endif
	}

	// 5/1/2015 mpd : This setting was already verified against the required value in "en_final_device_analysis()". 
	// Don't repeat the work here. 
	//if( strncmp( static_pvs->disconn_mode, active_pvs->disconn_mode, EN_CHANNEL_INFO_DISCONN_MODE_LEN ) != NULL ) 
	//{
	//	dev_state->programming_successful = false;
	//	#ifdef EN_VERBOSE_ALERTS
	//		Alert_Message( "Write Failed: Disconnect Mode mismatch" );
	//	#else
	//		Alert_Message( "EN15" );
	//	#endif
	//}

	// 5/1/2015 mpd : This setting was already verified against the required value in "en_final_device_analysis()". 
	// Don't repeat the work here. 
	//if( strncmp( static_pvs->flush_mode, active_pvs->flush_mode, EN_CHANNEL_INFO_FLUSH_MODE_LEN ) != NULL ) 
	//{
	//	dev_state->programming_successful = false;
	//	#ifdef EN_VERBOSE_ALERTS
	//		Alert_Message( "Write Failed: Flush Mode mismatch" );
	//	#else
	//		Alert_Message( "EN16" );
	//	#endif
	//}
																							   
}

void en_device_write_progress( DEV_STATE_STRUCT *dev_state )
{
	// 4/16/2015 mpd : Send progress info to the GUI.
	strlcpy( dev_state->info_text, "Writing device settings...", sizeof( dev_state->info_text ) );                
	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_write_settings_in_progress );

}

// 3/23/2015 mpd : This function is called at the end of READ and WRITE cycles. All device values have been read into an
// "EN_PROGRAMMABLE_VALUES_STRUCT" and specific fields have been anyalzed for consistency. This function gives the final
// stamp of approval.
void en_final_device_analysis( DEV_STATE_STRUCT *dev_state )
{
	EN_PROGRAMMABLE_VALUES_STRUCT *active_pvs;
	EN_DETAILS_STRUCT *en_details;

	BOOL_32 *success_ptr;

	#ifdef EN_DEBUG_ANALYSIS
	Alert_Message( "en_final_device_analysis" );
	#endif

	active_pvs = dev_state->en_active_pvs;
	en_details  = dev_state->en_details;

	// 5/26/2015 mpd : READ and WRITE have separate success flags. 
	if( dev_state->read_after_a_write )
	{
		success_ptr = &dev_state->programming_successful;
	}
	else
	{
		success_ptr = &dev_state->device_settings_are_valid;
	}

	// 5/1/2015 mpd : Assume the best.
	*success_ptr = true;

	// 5/1/2015 mpd : The seven critical settings identified in the
	// "http://odin.domain.calsense.com/documentation/docs/kb/CommOptions/EN/Setup_(EN).htm" are verified here.
	// The "EN_TEXT_CMD..." command strings we are comparing have a CR at the end for output purposes (sitting
	// in the null-termination character position of the read string). Don't expect that in the read value!

	// 6/22/2015 mpd : Only perform these checks after a WRITE. 
	if( dev_state->read_after_a_write )
	{
		// CHANNEL PARAMETERS
		if( strncmp( active_pvs->baud_rate, DEV_TEXT_CMD_SU_BAUD_RATE, EN_CHANNEL_INFO_BAUD_RATE_LEN - NULL_TERM_LEN ) != NULL ) 
		{
			*success_ptr = false;

			#ifdef EN_VERBOSE_ALERTS
				Alert_Message( "Invalid Baud Rate setting" );
			#else
				Alert_Message( "EN17" );
			#endif
		}

		if( strncmp( active_pvs->if_mode, DEV_TEXT_CMD_SU_IF_MODE_4C, EN_CHANNEL_INFO_IF_MODE_LEN - NULL_TERM_LEN ) != NULL ) 
		{
			*success_ptr = false;

			#ifdef EN_VERBOSE_ALERTS
				Alert_Message( "Invalid IF Mode setting" );
			#else
				Alert_Message( "EN18" );
			#endif
		}

		if( strncmp( active_pvs->flow, DEV_TEXT_CMD_SU_FLOW_02, EN_CHANNEL_INFO_FLOW_LEN - NULL_TERM_LEN ) != NULL ) 
		{
			*success_ptr = false;

			#ifdef EN_VERBOSE_ALERTS
				Alert_Message( "Invalid Flow setting" );
			#else
				Alert_Message( "EN19" );
			#endif
		}

		if( strncmp( active_pvs->port, DEV_TEXT_CMD_SU_PORT, EN_CHANNEL_INFO_PORT_LEN - NULL_TERM_LEN ) != NULL ) 
		{
			*success_ptr = false;

			#ifdef EN_VERBOSE_ALERTS
				Alert_Message( "Invalid Port setting" );
			#else
				Alert_Message( "EN20" );
			#endif
		}

		if( strncmp( active_pvs->conn_mode, DEV_TEXT_CMD_SU_CONN_MODE, EN_CHANNEL_INFO_CONN_MODE_LEN - NULL_TERM_LEN ) != NULL ) 
		{
			*success_ptr = false;

			#ifdef EN_VERBOSE_ALERTS
				Alert_Message( "Invalid Connection Mode setting" );
			#else
				Alert_Message( "EN21" );
			#endif
		}

		if( strncmp( active_pvs->disconn_mode, DEV_TEXT_CMD_SU_DISC_MODE, EN_CHANNEL_INFO_DISCONN_MODE_LEN - NULL_TERM_LEN ) != NULL ) 
		{
			*success_ptr = false;

			#ifdef EN_VERBOSE_ALERTS
				Alert_Message( "Invalid Disconnect Mode setting" );
			#else
				Alert_Message( "EN22" );
			#endif
		}

		if( strncmp( active_pvs->flush_mode, DEV_TEXT_CMD_SU_FLUSH_MODE, EN_CHANNEL_INFO_FLUSH_MODE_LEN - NULL_TERM_LEN ) != NULL ) 
		{
			*success_ptr = false;

			#ifdef EN_VERBOSE_ALERTS
				Alert_Message( "Invalid Flush Mode setting" );
			#else
				Alert_Message( "EN23" );
			#endif
		}

		// 5/22/2015 mpd : Verify what was done by the preceeding WRITE operation.
		en_verify_active_values_match_static_values( dev_state );

		if( strlen( active_pvs->dhcp_name ) > 0 )
		{
			// 6/12/2015 mpd : We just successfully wrote a DHCP name. This flag prevents us from stomping on that name 
			// if we disable DHCP and then reenable it during this session. 
			en_details->dhcp_name_not_set = false;
		}
	}

	// 6/22/2015 mpd : There is no mimimum EN firmware version at this time.
	#if 0
	dev_analyze_firmware_version( dev_state );

	if( !dev_state->acceptable_version )
	{
		*success_ptr = false;
	}
	#endif

}

//#define EN_DEBUG_WRITE

extern void en_copy_active_values_to_static_values( DEV_STATE_STRUCT *dev_state )
{
	EN_PROGRAMMABLE_VALUES_STRUCT *active_pvs;
	EN_PROGRAMMABLE_VALUES_STRUCT *static_pvs;

	#ifdef EN_DEBUG_WRITE
	Alert_Message( "en_copy_active_values_to_static_values" );
	#endif

	active_pvs = dev_state->en_active_pvs;
	static_pvs = dev_state->en_static_pvs;

	// 4/14/2015 mpd : Before copying values from the "active_pvs" to the "static_pvs", all programming details that are 
	// hidden from the user need to be updated based on current user selections. 

	// 4/27/2015 mpd : These are fixed CS3000 values that cannot be controlled by the user. These are critical to proper 
	// operation of the device, so they get set every time we program the device. Set them in the "active_pvs" fields so 
	// the post-write verification will match. They will be copied to the "static_pvs" further down in this function.
	strlcpy( active_pvs->baud_rate,           DEV_TEXT_CMD_SU_BAUD_RATE,   sizeof( active_pvs->baud_rate ) );
	strlcpy( active_pvs->if_mode,             DEV_TEXT_CMD_SU_IF_MODE_4C,  sizeof( active_pvs->if_mode ) );
	strlcpy( active_pvs->flow,                DEV_TEXT_CMD_SU_FLOW_02,     sizeof( active_pvs->flow ) );
	strlcpy( active_pvs->port,                DEV_TEXT_CMD_SU_PORT,        sizeof( active_pvs->port ) );
	strlcpy( active_pvs->conn_mode,           DEV_TEXT_CMD_SU_CONN_MODE,   sizeof( active_pvs->conn_mode ) );
	active_pvs->auto_increment =  true;
	strlcpy( active_pvs->remote_ip_address,   DEV_CALSENSE_IP_ADD_STR,     sizeof( active_pvs->remote_ip_address ) );
	strlcpy( active_pvs->remote_ip_address_1, DEV_TEXT_CMD_SU_REMOTE_IP_1, sizeof( active_pvs->remote_ip_address_1 ) );
	strlcpy( active_pvs->remote_ip_address_2, DEV_TEXT_CMD_SU_REMOTE_IP_2, sizeof( active_pvs->remote_ip_address_2 ) );
	strlcpy( active_pvs->remote_ip_address_3, DEV_TEXT_CMD_SU_REMOTE_IP_3, sizeof( active_pvs->remote_ip_address_3 ) );
	strlcpy( active_pvs->remote_ip_address_4, DEV_TEXT_CMD_SU_REMOTE_IP_4, sizeof( active_pvs->remote_ip_address_4 ) );
	strlcpy( active_pvs->remote_port,         DEV_TEXT_CMD_SU_REMOTE_PORT, sizeof( active_pvs->remote_port ) );
	strlcpy( active_pvs->disconn_mode,        DEV_TEXT_CMD_SU_DISC_MODE,   sizeof( active_pvs->disconn_mode ) );
	strlcpy( active_pvs->flush_mode,          DEV_TEXT_CMD_SU_FLUSH_MODE,  sizeof( active_pvs->flush_mode ) );

	// 6/9/2015 mpd : Fixed settings have been merged with user settings. Make a copy to "static_pvs" for post-WRITE 
	// verification. The verification will be done by the READ that immediately follows this WRITE.

	// BASIC PARAMETERS
	strlcpy( static_pvs->ip_address,   active_pvs->ip_address,   sizeof( static_pvs->ip_address ) );
	strlcpy( static_pvs->ip_address_1, active_pvs->ip_address_1, sizeof( static_pvs->ip_address_1 ) );
	strlcpy( static_pvs->ip_address_2, active_pvs->ip_address_2, sizeof( static_pvs->ip_address_2 ) );
	strlcpy( static_pvs->ip_address_3, active_pvs->ip_address_3, sizeof( static_pvs->ip_address_3 ) );
	strlcpy( static_pvs->ip_address_4, active_pvs->ip_address_4, sizeof( static_pvs->ip_address_4 ) );

	strlcpy( static_pvs->dhcp_name, active_pvs->dhcp_name, sizeof( static_pvs->dhcp_name ) );

	strlcpy( static_pvs->gw_address,   active_pvs->gw_address,   sizeof( static_pvs->gw_address ) );
	strlcpy( static_pvs->gw_address_1, active_pvs->gw_address_1, sizeof( static_pvs->gw_address_1 ) );
	strlcpy( static_pvs->gw_address_2, active_pvs->gw_address_2, sizeof( static_pvs->gw_address_2 ) );
	strlcpy( static_pvs->gw_address_3, active_pvs->gw_address_3, sizeof( static_pvs->gw_address_3 ) );
	strlcpy( static_pvs->gw_address_4, active_pvs->gw_address_4, sizeof( static_pvs->gw_address_4 ) );

	// 9/24/2015 mpd : FogBugz #3178: We use mask bits for programming, not the mask string. Leave this commented out.
	// 6/10/2015 mpd : TBD: Is "mask" needed at this time on the redesigned screen?
	// 5/1/2015 mpd : These fields are not currently needed by EN Programming. They have been tested and are functional.
	//strlcpy( static_pvs->mask,   active_pvs->mask,   sizeof( static_pvs->mask ) );
	//strlcpy( static_pvs->mask_1, active_pvs->mask_1, sizeof( static_pvs->mask_1 ) );
	//strlcpy( static_pvs->mask_2, active_pvs->mask_2, sizeof( static_pvs->mask_2 ) );
	//strlcpy( static_pvs->mask_3, active_pvs->mask_3, sizeof( static_pvs->mask_3 ) );
	//strlcpy( static_pvs->mask_4, active_pvs->mask_4, sizeof( static_pvs->mask_4 ) );

	// CHANNEL PARAMETERS
	strlcpy( static_pvs->baud_rate,           active_pvs->baud_rate,           sizeof( static_pvs->baud_rate ) );
	strlcpy( static_pvs->if_mode,             active_pvs->if_mode,             sizeof( static_pvs->if_mode ) );
	strlcpy( static_pvs->flow,                active_pvs->flow,                sizeof( static_pvs->flow ) );
	strlcpy( static_pvs->port,                active_pvs->port,                sizeof( static_pvs->port ) );
	strlcpy( static_pvs->conn_mode,           active_pvs->conn_mode,           sizeof( static_pvs->conn_mode ) );
	static_pvs->auto_increment =              active_pvs->auto_increment;
	strlcpy( static_pvs->remote_ip_address,   active_pvs->remote_ip_address,   sizeof( static_pvs->remote_ip_address ) );
	strlcpy( static_pvs->remote_ip_address_1, active_pvs->remote_ip_address_1, sizeof( static_pvs->remote_ip_address_1 ) );
	strlcpy( static_pvs->remote_ip_address_2, active_pvs->remote_ip_address_2, sizeof( static_pvs->remote_ip_address_2 ) );
	strlcpy( static_pvs->remote_ip_address_3, active_pvs->remote_ip_address_3, sizeof( static_pvs->remote_ip_address_3 ) );
	strlcpy( static_pvs->remote_ip_address_4, active_pvs->remote_ip_address_4, sizeof( static_pvs->remote_ip_address_4 ) );
	strlcpy( static_pvs->remote_port,         active_pvs->remote_port,         sizeof( static_pvs->remote_port ) );
	strlcpy( static_pvs->disconn_mode,        active_pvs->disconn_mode,        sizeof( static_pvs->disconn_mode ) );
	strlcpy( static_pvs->flush_mode,          active_pvs->flush_mode,          sizeof( static_pvs->flush_mode ) );
																								   
}

//#define EN_DEBUG_VERIFY		

// 5/22/2015 mpd : Now that every WRITE operation is not followed by a MONITOR MODE READ, these analysis and
// verification steps are useless. A SETUP MODE READ has been added after the WRITE. Verification will have to be done 
// following that. If DHCP is enabled, the network responses will have to be ignored since they will not match.
//void en_final_device_verification( DEV_STATE_STRUCT *dev_state )
//{
//	#ifdef EN_DEBUG_VERIFY
//	Alert_Message( "en_final_device_verification" );
//	#endif
//
//	 
//	// 4/13/2015 mpd : Perform final analysis as we do at the end of a READ operation.
//	en_final_device_analysis( dev_state );
//
//	// 4/13/2015 mpd : Verify every field in the WRITE struct made it into the device programming.
//	en_verify_dynamic_values_match_write_values( dev_state );
//
//}

//#define EN_DEBUG_MEM_MALLOC 
 
/* ---------------------------------------------------------- */
/*  				   READ and WRITE Lists 				  */
/* ---------------------------------------------------------- */

// LANTRONIX ENTER SETUP MODE INSTRUCTIONS:
// 1. Reset the UDS1100 unit by cycling the unit's power (turning the power off and back on). 
// 2. Immediately upon resetting the device, enter three lowercase x characters (xxx). Note: The easiest way to enter 
// Setup Mode is to hold down the x key at the terminal (or emulation) while resetting the unit. You must do this within 
// three seconds of resetting the UDS1100  ( DEV_COMMAND_START ).
// NOTE: The string "Press Enter for Setup Mode" ( EN_SERVER_INFO_SETUP_STR )is returned after the "xxx" or repeated "x".
// 3. To enter Setup Mode, press Enter within 5 seconds. The configuration settings display, followed by the Change 
// Setup menu ( performed in the DEV_WAITING_FOR_RESPONSE state using DEV_COMMAND_CLI ).

// 6/10/2015 mpd : TODO: EN is using long title strings to verify menu context. Use brief strings or the termination 
// string as WEN and GR do.

/* 
void en_analyze_device_type( DEV_STATE_STRUCT *dev_state )
void en_analyze_mac_address( DEV_STATE_STRUCT *dev_state )
void en_analyze_fw_version( DEV_STATE_STRUCT *dev_state )
*/ 
const DEV_MENU_ITEM en_setup_read_list[] = { 
	// Response Title             Response Handler        	  Next Command                 Next Termination String
	//{ EN_NO_TEXT_STR,			  NULL,					  	  EN_COMMAND_ENTER_SETUP,      EN_SERVER_INFO_SETUP_STR },	// PREVIOUS IMPLEMENTATION. The device provides the first response immediately after the "xxx". 
	//{ EN_SERVER_INFO_TITLE_STR, en_analyze_server_info, 	  EN_COMMAND_CONTINUE_SETUP,   EN_CHANGE_MENU_CHOICE_STR },	// PREVIOUS IMPLEMENTATION. Analyze the initial response. Return ENTER to continue into SETUP.
	{ EN_NO_TEXT_STR,			  NULL,					  	  DEV_COMMAND_START,    	   EN_SERVER_INFO_SETUP_STR },	// This first entry is used by device exchange initialization.
	{ EN_SERVER_INFO_SETUP_STR,   en_analyze_server_info,	  DEV_COMMAND_CR,		       EN_CHANGE_MENU_CHOICE_STR },	// This second entry is used by the DEV_WAITING_FOR_RESPONSE state. Analyze the initial response. Return ENTER to continue into SETUP.
	{ EN_SERVER_INFO_MODEL_STR,	  en_analyze_setup_mode, 	  DEV_COMMAND_EXIT_WO_SAVE,    EN_CHANGE_MENU_EXIT_WO_STR },// Analyze the bulk of the response. Exit without saving.
	{ EN_CHANGE_MENU_EXIT_WO_STR, en_final_device_analysis,   DEV_COMMAND_CR_NR, 	       EN_NO_TEXT_STR },			// Perform final analysis. CLI Disconnect. 
	{ EN_NO_TEXT_STR, 			  dev_cli_disconnect,		  DEV_COMMAND_END, 			   EN_NO_TEXT_STR }				// Hardware disconnect. 
};

// LANTRONIX ENTER MONITOR MODE INSTRUCTIONS:
// 1. Reset the UDS1100 unit by cycling the unit's power (turning the power off and back on). 
// 2. Immediately upon resetting the device, enter three lowercase z characters (zzz). ASSUMPTION: The "z" can be held
// down as in SETUP MODE. Also, the three seconds probably applies here too.
// NOTE: The string "0>" is returned after the "zzz". No further entry steps are necessary. 

const DEV_MENU_ITEM en_monitor_read_list[] = { 
	// Response Title             Response Handler            Next Command                 Next Termination String
	//{ EN_NO_TEXT_STR,			  NULL,						  EN_COMMAND_ENTER_MONITOR,    EN_MON_MODE_PROMPT_STR },	// PREVIOUS IMPLEMENTATION. The device provides the first response immediately after the "zzz".
	{ EN_NO_TEXT_STR,			  NULL,					  	  DEV_COMMAND_START,   	       EN_MON_MODE_PROMPT_STR },	// This first entry is used by device exchange initialization.
	{ EN_MON_MODE_PROMPT_STR,     NULL,						  DEV_COMMAND_CR,		       EN_MON_MODE_PROMPT_STR },	// This second entry is used by the DEV_WAITING_FOR_RESPONSE state. Just do a CR to move on to the next state.
	{ EN_MON_MODE_TITLE_STR,	  NULL, 					  DEV_COMMAND_MON_GET_MAC, 	   EN_MON_MODE_PROMPT_STR },    // Get the MAC address. 
	{ EN_MON_MODE_GM_TITLE_STR,	  en_analyze_monitor_mode_gm, DEV_COMMAND_MON_GET_NETWORK, EN_MON_MODE_PROMPT_STR },    // Analyze the MAC address. Get the network connections.
	{ EN_MON_MODE_NC_TITLE_STR,	  en_analyze_monitor_mode_nc, DEV_COMMAND_MON_EXIT,        EN_MON_MODE_QU_STR },        // Analyze the network connections. Exit MONITOR MODE.
	{ EN_NO_TEXT_STR,			  NULL,						  DEV_COMMAND_CR_NR,           EN_NO_TEXT_STR },            // There is no analysis on MONITOR MODE values. CLI Disconnect. 
	{ EN_NO_TEXT_STR, 			  dev_cli_disconnect,		  DEV_COMMAND_END, 			   EN_NO_TEXT_STR }				// Hardware disconnect.  
};

// 3/16/2015 mpd : For consistency, navigate in the same menu order for WRITE as we did for READ. Note that WRITE
// requires additional analyze/hunt I/O cycles between the menu navigation cycles. Once on the appropriate menu, each
// WRITE operation involves a cycle to select a menu option, and another to input the new value. Also note that CR and
// LF characters ARE NOT INCLUDED ON EVERY LINE!!! The device is very fussy regarding CR and LF.
 
// 4/13/2015 mpd : The "Response Title" requires enough characters to determine a unique match; not the entire line! The
// defines were created to exclude anything that could change during run time. The "Next Termination String" has similar
// run time issues, such as IP addess entry responses. A ")" character is all we can use for the termination string
// since the device will proceed ")" with the dynamic value of the octet. The final octet response termination is the
// "?" on the subsequent programming question.

const DEV_MENU_ITEM en_dhcp_write_list[] = {  
	// Response Title                    Response Handler               Next Command                   	Next Termination String
	//{ EN_NO_TEXT_STR,			         NULL,			                EN_COMMAND_ENTER_SETUP,         EN_SERVER_INFO_SETUP_STR },	        // PREVIOUS IMPLEMENTATION. The device provides the first response immediately after the "xxx". 
	//{ EN_SERVER_INFO_TITLE_STR,	     NULL,					        EN_COMMAND_CONTINUE_SETUP,      EN_CHANGE_MENU_CHOICE_STR },	    // PREVIOUS IMPLEMENTATION. Send ENTER to continue into SETUP. No need to perform any analysis here.
	{ EN_NO_TEXT_STR,			  		 NULL,					  	  	DEV_COMMAND_START,    	   		EN_SERVER_INFO_SETUP_STR },			// This first entry is used by device exchange initialization.
	{ EN_SERVER_INFO_SETUP_STR,   		 NULL,	  						DEV_COMMAND_CR,		       		EN_CHANGE_MENU_CHOICE_STR },		// This second entry is used by the DEV_WAITING_FOR_RESPONSE state. Analyze the initial response. Return ENTER to continue into SETUP.
	{ EN_SERVER_INFO_MODEL_STR,	         NULL,			                DEV_COMMAND_GOTO_SERVER,	    EN_SERVER_INPUT_IP_ADD_STR },       // Go to the server menu. No need to perform any analysis here.
	{ EN_SERVER_INPUT_IP_ADD_STR,        en_device_write_progress,	    DEV_COMMAND_GET_IP_1,           EN_SERVER_INPUT_IP_ADD_RESP_STR },  // Send IP octet 1.
	{ EN_SERVER_INPUT_IP_ADD_RESP_STR,   NULL,			                DEV_COMMAND_GET_IP_2,           EN_SERVER_INPUT_IP_ADD_RESP_STR },  // Send IP octet 2.
	{ EN_SERVER_INPUT_IP_ADD_RESP_STR,   NULL,			                DEV_COMMAND_GET_IP_3,           EN_SERVER_INPUT_IP_ADD_RESP_STR },  // Send IP octet 3.
	{ EN_SERVER_INPUT_IP_ADD_RESP_STR,   NULL,			                DEV_COMMAND_GET_IP_4,           EN_SERVER_INPUT_QUESTION_STR },     // Send IP octet 4.
	{ EN_SERVER_INPUT_GATEWAY_YN_STR,    NULL,			                DEV_COMMAND_GET_GW_BOOL,        EN_SERVER_INPUT_NETMASK_STR }, 	    // Send "No" to changing the gateway IP address.

	// 6/12/2015 mpd : There are 4 gateway lines here when DHCP is disabled. 
	// 6/12/2015 mpd : TODO: Both EN WRITE lists could be combined by using "dev_state->dhcp_enabled" and 
	// "dev_increment_list_item_pointer( dev_state )". This is a low priority.


	{ EN_SERVER_INPUT_NETMASK_STR, 	     NULL,			                DEV_COMMAND_GET_MASK_BITS,      EN_SERVER_INPUT_QUESTION_STR }, 	// Send the mask bits.
	{ EN_SERVER_INPUT_TELNET_YN_STR,     NULL,			                DEV_COMMAND_GET_TELNET_BOOL,    EN_SERVER_INPUT_QUESTION_STR }, 	// Send "No" to changing the telnet password.

	// 6/12/2015 mpd : These 2 lines are only used when DHCP is enabled. 
	{ EN_SERVER_INPUT_DHCP_NAME_YN_STR,  NULL,			                DEV_COMMAND_GET_DHCP_NAME_BOOL, EN_SERVER_INPUT_COLON_STR }, 		// Send "Yes" to changing the DHCP name.

	// 6/12/2015 mpd : Just test for a "?" terminator after DHCP. We may get a "FQDN" question instead of the "Your Choice?" we expect.
	{ EN_SERVER_INPUT_DHCP_NAME_STR,     NULL,			                DEV_COMMAND_GET_DHCP_NAME,      EN_CHANNEL_INPUT_QUESTION_STR },    // Send the new DHCP name.

	{ EN_CHANGE_MENU_CHOICE_STR,	     NULL,  		                DEV_COMMAND_GOTO_CHANNEL,	   	EN_CHANNEL_INPUT_QUESTION_STR },    // Go to the channel menu.
	{ EN_CHANNEL_INPUT_BAUD_RATE_STR,    NULL,  						DEV_COMMAND_GET_BAUD_RATE,	   	EN_CHANNEL_INPUT_QUESTION_STR },    // Send the baud rate.
	{ EN_CHANNEL_INPUT_IF_MODE_STR,  	 NULL,  		                DEV_COMMAND_GET_IF_MODE,	    EN_CHANNEL_INPUT_QUESTION_STR },    // Send the IF mode.
	{ EN_CHANNEL_INPUT_FLOW_STR,  	     NULL,  		                DEV_COMMAND_GET_FLOW,	   	   	EN_CHANNEL_INPUT_QUESTION_STR },    // Send the flow.
	{ EN_CHANNEL_INPUT_PORT_STR,  	     NULL,  		                DEV_COMMAND_GET_PORT,	       	EN_CHANNEL_INPUT_QUESTION_STR },    // Send the port.
	{ EN_CHANNEL_INPUT_CONN_MODE_STR,  	 NULL,  		                DEV_COMMAND_GET_CONN_MODE,	   	EN_CHANNEL_INPUT_QUESTION_STR },    // Send the connect mode.
	{ EN_CHANNEL_INPUT_SEND_STR,    	 NULL,  		                DEV_COMMAND_GET_SEND_BOOL,	   	EN_CHANNEL_INPUT_QUESTION_STR },    // Send "Yes" to "Send ESC in Modem Mode". 
	{ EN_CHANNEL_INPUT_AUTO_INCR_STR,  	 NULL,  		                DEV_COMMAND_GET_AUTO_BOOL,	   	EN_SERVER_INPUT_IP_ADD_STR }, 	    // Send "Yes" to "Auto Increment Source Port". 
	{ EN_CHANNEL_INPUT_REMOTE_IP_STR,  	 NULL,  		                DEV_COMMAND_GET_REMOTE_IP_1,	EN_CHANNEL_INPUT_IP_ADD_RESP_STR }, // Send Remote IP octet 1.
	{ EN_CHANNEL_INPUT_IP_ADD_RESP_STR,	 NULL,  		                DEV_COMMAND_GET_REMOTE_IP_2,	EN_CHANNEL_INPUT_IP_ADD_RESP_STR }, // Send Remote IP octet 2.
	{ EN_CHANNEL_INPUT_IP_ADD_RESP_STR,	 NULL,  		                DEV_COMMAND_GET_REMOTE_IP_3,	EN_CHANNEL_INPUT_IP_ADD_RESP_STR }, // Send Remote IP octet 3.
	{ EN_CHANNEL_INPUT_IP_ADD_RESP_STR,	 NULL,  		                DEV_COMMAND_GET_REMOTE_IP_4,	EN_CHANNEL_INPUT_QUESTION_STR },    // Send Remote IP octet 4.
	{ EN_CHANNEL_INPUT_REMOTE_PORT_STR,	 NULL,  		                DEV_COMMAND_GET_REMOTE_PORT,	EN_CHANNEL_INPUT_QUESTION_STR },    // Send the remote port.
	{ EN_CHANNEL_INPUT_DISCONN_MODE_STR, NULL,  		                DEV_COMMAND_GET_DISCONN_MODE,   EN_CHANNEL_INPUT_QUESTION_STR },    // Send the disconnect mode.
	{ EN_CHANNEL_INPUT_FLUSH_MODE_STR, 	 NULL,  		                DEV_COMMAND_GET_FLUSH_MODE,	   	EN_CHANNEL_INPUT_QUESTION_STR },    // Send the flush mode.
	{ EN_CHANNEL_INPUT_DISCONN_TIME_STR, NULL,  		                DEV_COMMAND_GET_DISCONN_TIME,   EN_CHANNEL_INPUT_COLON_STR },       // Send the disconnect time minutes.
	{ EN_CHANNEL_INPUT_COLON_STR,		 NULL,  		                DEV_COMMAND_GET_DISCONN_TIME,   EN_CHANNEL_INPUT_QUESTION_STR },    // Send the disconnect time seconds.
	{ EN_CHANNEL_INPUT_SEND_CHAR_1_STR,	 NULL,  		                DEV_COMMAND_GET_SEND_CHAR_1,	EN_CHANNEL_INPUT_QUESTION_STR },    // Disable "2-Byte Send Character Sequence". 
	{ EN_CHANNEL_INPUT_SEND_CHAR_2_STR,  NULL,  		                DEV_COMMAND_GET_SEND_CHAR_2,	EN_CHANGE_MENU_CHOICE_STR },        // Disable "Send Immediately After Characters". 
	{ EN_CHANGE_MENU_CHOICE_STR,  		 NULL,  		                DEV_COMMAND_CR,   				EN_CHANGE_MENU_CHOICE_STR },        // Perform a read of the programmed values. 
	{ EN_CHANGE_MENU_CHOICE_STR,		 NULL,					 		DEV_COMMAND_SAVE_AND_EXIT,	   	EN_CHANGE_SUCCESS_STR },			// Save and exit programming mode.
	//{ EN_CHANGE_SUCCESS_STR,		 	 en_final_device_verification,  DEV_COMMAND_DISCONNECT, 		EN_NO_TEXT_STR }, 					// Final verification. CLI Disconnect. 
	{ EN_CHANGE_SUCCESS_STR,		 	 NULL,							DEV_COMMAND_CR_NR, 		   		EN_NO_TEXT_STR }, 					// CLI Disconnect. 
	{ EN_NO_TEXT_STR, 			 		 dev_cli_disconnect,		  	DEV_COMMAND_END, 			   	EN_NO_TEXT_STR }					// Hardware disconnect. 
};

const DEV_MENU_ITEM en_static_write_list[] = {  
	// Response Title                    Response Handler               Next Command                   	Next Termination String
	//{ EN_NO_TEXT_STR,			         NULL,			                EN_COMMAND_ENTER_SETUP,         EN_SERVER_INFO_SETUP_STR },	        // PREVIOUS IMPLEMENTATION. The device provides the first response immediately after the "xxx". 
	//{ EN_SERVER_INFO_TITLE_STR,	     NULL,					        EN_COMMAND_CONTINUE_SETUP,      EN_CHANGE_MENU_CHOICE_STR },	    // PREVIOUS IMPLEMENTATION. Send ENTER to continue into SETUP. No need to perform any analysis here.
	{ EN_NO_TEXT_STR,			  		 NULL,					  	  	DEV_COMMAND_START,    	   		EN_SERVER_INFO_SETUP_STR },			// This first entry is used by device exchange initialization.
	{ EN_SERVER_INFO_SETUP_STR,   		 NULL,	  						DEV_COMMAND_CR,		       		EN_CHANGE_MENU_CHOICE_STR },		// This second entry is used by the DEV_WAITING_FOR_RESPONSE state. Analyze the initial response. Return ENTER to continue into SETUP.
	{ EN_SERVER_INFO_MODEL_STR,	         NULL,			                DEV_COMMAND_GOTO_SERVER,	    EN_SERVER_INPUT_IP_ADD_STR },       // Go to the server menu. No need to perform any analysis here.
	{ EN_SERVER_INPUT_IP_ADD_STR,        en_device_write_progress,		DEV_COMMAND_GET_IP_1,           EN_SERVER_INPUT_IP_ADD_RESP_STR },  // Send IP octet 1.
	{ EN_SERVER_INPUT_IP_ADD_RESP_STR,   NULL,			                DEV_COMMAND_GET_IP_2,           EN_SERVER_INPUT_IP_ADD_RESP_STR },  // Send IP octet 2.
	{ EN_SERVER_INPUT_IP_ADD_RESP_STR,   NULL,			                DEV_COMMAND_GET_IP_3,           EN_SERVER_INPUT_IP_ADD_RESP_STR },  // Send IP octet 3.
	{ EN_SERVER_INPUT_IP_ADD_RESP_STR,   NULL,			                DEV_COMMAND_GET_IP_4,           EN_SERVER_INPUT_QUESTION_STR },     // Send IP octet 4.
	{ EN_SERVER_INPUT_GATEWAY_YN_STR,    NULL,			                DEV_COMMAND_GET_GW_BOOL,        EN_SERVER_INPUT_GATEWAY_IP_STR },   // Send "Yes" to changing the gateway IP address.

	// 4/27/2015 mpd : These 4 lines are only used when DHCP is disabled. 
	{ EN_SERVER_INPUT_GATEWAY_IP_STR,  	 NULL,  		                DEV_COMMAND_GET_GW_1,	        EN_SERVER_INPUT_IP_ADD_RESP_STR },   // Send Gateway IP octet 1.
	{ EN_SERVER_INPUT_IP_ADD_RESP_STR,	 NULL,  		                DEV_COMMAND_GET_GW_2,	        EN_SERVER_INPUT_IP_ADD_RESP_STR },   // Send Gateway IP octet 2.
	{ EN_SERVER_INPUT_IP_ADD_RESP_STR,	 NULL,  		                DEV_COMMAND_GET_GW_3,	        EN_SERVER_INPUT_IP_ADD_RESP_STR },   // Send Gateway IP octet 3.
	{ EN_SERVER_INPUT_IP_ADD_RESP_STR,	 NULL,  		                DEV_COMMAND_GET_GW_4,	        EN_SERVER_INPUT_NETMASK_STR },       // Send Gateway IP octet 4.
																		
	{ EN_SERVER_INPUT_NETMASK_STR, 	     NULL,			                DEV_COMMAND_GET_MASK_BITS,      EN_SERVER_INPUT_QUESTION_STR }, 	 // Send the mask bits.
	{ EN_SERVER_INPUT_TELNET_YN_STR,     NULL,			                DEV_COMMAND_GET_TELNET_BOOL,    EN_SERVER_INPUT_QUESTION_STR }, 	 // Send "No" to changing the telnet password.
																		
	// 6/12/2015 mpd : These 2 lines are only used when DHCP is enabled. 
	//{ EN_SERVER_INPUT_DHCP_NAME_YN_STR,NULL,                      	DEV_COMMAND_GET_DHCP_NAME_BOOL, EN_CHANGE_MENU_CHOICE_STR }, 		 // Send "No" to changing the DHCP name.  
	//{ EN_SERVER_INPUT_DHCP_NAME_STR,   NULL,	                        DEV_COMMAND_GET_DHCP_NAME,      EN_CHANGE_MENU_CHOICE_STR }, 	     // Send the new DHCP name.
																		 																  
 	{ EN_CHANGE_MENU_CHOICE_STR,	     NULL,  		                DEV_COMMAND_GOTO_CHANNEL,	    EN_CHANNEL_INPUT_QUESTION_STR },     // Go to the channel menu.
	{ EN_CHANNEL_INPUT_BAUD_RATE_STR,    NULL,  						DEV_COMMAND_GET_BAUD_RATE,	    EN_CHANNEL_INPUT_QUESTION_STR },     // Send the baud rate.
	{ EN_CHANNEL_INPUT_IF_MODE_STR,  	 NULL,  		                DEV_COMMAND_GET_IF_MODE,	    EN_CHANNEL_INPUT_QUESTION_STR },     // Send the IF mode.
	{ EN_CHANNEL_INPUT_FLOW_STR,  	     NULL,  		                DEV_COMMAND_GET_FLOW,	   	    EN_CHANNEL_INPUT_QUESTION_STR },     // Send the flow.
	{ EN_CHANNEL_INPUT_PORT_STR,  	     NULL,  		                DEV_COMMAND_GET_PORT,	        EN_CHANNEL_INPUT_QUESTION_STR },     // Send the port.
	{ EN_CHANNEL_INPUT_CONN_MODE_STR,  	 NULL,  		                DEV_COMMAND_GET_CONN_MODE,	    EN_CHANNEL_INPUT_QUESTION_STR },     // Send the connect mode.
	{ EN_CHANNEL_INPUT_SEND_STR,    	 NULL,  		                DEV_COMMAND_GET_SEND_BOOL,	    EN_CHANNEL_INPUT_QUESTION_STR },     // Send "Yes" to "Send ESC in Modem Mode". 
	{ EN_CHANNEL_INPUT_AUTO_INCR_STR,  	 NULL,  		                DEV_COMMAND_GET_AUTO_BOOL,	    EN_SERVER_INPUT_IP_ADD_STR }, 	     // Send "Yes" to "Auto Increment Source Port". 
	{ EN_CHANNEL_INPUT_REMOTE_IP_STR,  	 NULL,  		                DEV_COMMAND_GET_REMOTE_IP_1,	EN_CHANNEL_INPUT_IP_ADD_RESP_STR },  // Send Remote IP octet 1.
	{ EN_CHANNEL_INPUT_IP_ADD_RESP_STR,	 NULL,  		                DEV_COMMAND_GET_REMOTE_IP_2,	EN_CHANNEL_INPUT_IP_ADD_RESP_STR },  // Send Remote IP octet 2.
	{ EN_CHANNEL_INPUT_IP_ADD_RESP_STR,	 NULL,  		                DEV_COMMAND_GET_REMOTE_IP_3,	EN_CHANNEL_INPUT_IP_ADD_RESP_STR },  // Send Remote IP octet 3.
	{ EN_CHANNEL_INPUT_IP_ADD_RESP_STR,	 NULL,  		                DEV_COMMAND_GET_REMOTE_IP_4,	EN_CHANNEL_INPUT_QUESTION_STR },     // Send Remote IP octet 4.
	{ EN_CHANNEL_INPUT_REMOTE_PORT_STR,	 NULL,  		                DEV_COMMAND_GET_REMOTE_PORT,	EN_CHANNEL_INPUT_QUESTION_STR },     // Send the remote port.
	{ EN_CHANNEL_INPUT_DISCONN_MODE_STR, NULL,  		                DEV_COMMAND_GET_DISCONN_MODE,   EN_CHANNEL_INPUT_QUESTION_STR },     // Send the disconnect mode.
	{ EN_CHANNEL_INPUT_FLUSH_MODE_STR, 	 NULL,  		                DEV_COMMAND_GET_FLUSH_MODE,	    EN_CHANNEL_INPUT_QUESTION_STR },     // Send the flush mode.
	{ EN_CHANNEL_INPUT_DISCONN_TIME_STR, NULL,  		                DEV_COMMAND_GET_DISCONN_TIME,   EN_CHANNEL_INPUT_COLON_STR },        // Send the disconnect time minutes.
	{ EN_CHANNEL_INPUT_COLON_STR,		 NULL,  		                DEV_COMMAND_GET_DISCONN_TIME,   EN_CHANNEL_INPUT_QUESTION_STR },     // Send the disconnect time seconds.
	{ EN_CHANNEL_INPUT_SEND_CHAR_1_STR,	 NULL,  		                DEV_COMMAND_GET_SEND_CHAR_1,	EN_CHANNEL_INPUT_QUESTION_STR },     // Disable "2-Byte Send Character Sequence". 
	{ EN_CHANNEL_INPUT_SEND_CHAR_2_STR,  NULL,  		                DEV_COMMAND_GET_SEND_CHAR_2,	EN_CHANGE_MENU_CHOICE_STR },         // Disable "Send Immediately After Characters". 
	{ EN_CHANGE_MENU_CHOICE_STR,  		 NULL,  		                DEV_COMMAND_CR,   				EN_CHANGE_MENU_CHOICE_STR },         // Perform a read of the programmed values. 
	{ EN_CHANGE_MENU_CHOICE_STR,		 NULL,					 		DEV_COMMAND_SAVE_AND_EXIT,	    EN_CHANGE_SUCCESS_STR },			 // Save and exit programming mode.
	//{ EN_CHANGE_SUCCESS_STR,		 	 en_final_device_verification,  DEV_COMMAND_DISCONNECT, 		EN_NO_TEXT_STR }, 					 // Final verification. CLI Disconnect. 
	{ EN_CHANGE_SUCCESS_STR,		 	 NULL,							DEV_COMMAND_CR_NR, 		        EN_NO_TEXT_STR }, 					 // CLI Disconnect. 
	{ EN_NO_TEXT_STR, 			 		 dev_cli_disconnect,		  	DEV_COMMAND_END, 			    EN_NO_TEXT_STR }					 // Hardware disconnect. 
};                                                                      

// 4/8/2015 mpd : Determine the size of the lists. These will be used later to determine if we successfully reached 
// the end.
#define EN_SETUP_READ_LIST_LEN		( sizeof( en_setup_read_list )   / sizeof( DEV_MENU_ITEM ) )
#define EN_MONITOR_READ_LIST_LEN 	( sizeof( en_monitor_read_list ) / sizeof( DEV_MENU_ITEM ) )
#define EN_DHCP_WRITE_LIST_LEN	    ( sizeof( en_dhcp_write_list )   / sizeof( DEV_MENU_ITEM ) )
#define EN_STATIC_WRITE_LIST_LEN	( sizeof( en_static_write_list ) / sizeof( DEV_MENU_ITEM ) )

extern UNS_32 en_sizeof_setup_read_list()
{
	return( EN_SETUP_READ_LIST_LEN ); 
}

extern UNS_32 en_sizeof_monitor_read_list()
{
	return( EN_MONITOR_READ_LIST_LEN ); 
}

extern UNS_32 en_sizeof_dhcp_write_list()
{
	return( EN_DHCP_WRITE_LIST_LEN ); 
}

extern UNS_32 en_sizeof_static_write_list()
{
	return( EN_STATIC_WRITE_LIST_LEN ); 
}

/* ---------------------------------------------------------- */
/*  						  Help Function 				  */
/* ---------------------------------------------------------- */

void en_programming_help( void ) {

//UNS_16 i = 0;

	//help.text_ptrs[i++] = "";
	//help.text_ptrs[i++] = "This screen allows you to program or";
	//help.text_ptrs[i++] = "reprogram an EN device attached to your";
	//help.text_ptrs[i++] = "controller.";
	//help.text_ptrs[i++] = "";
	//help.text_ptrs[i++] = "This feature is not recommended for";
	//help.text_ptrs[i++] = "end users. If a device needs to be";
	//help.text_ptrs[i++] = "reprogrammed, please contact your";
	//help.text_ptrs[i++] = "local Calsense Field Service";
	//help.text_ptrs[i++] = "Representative who will program the";
	//help.text_ptrs[i++] = "device for you.";
	//help.text_ptrs[i++] = "";

	//HELP_prepare_for( "EN RADIO PROGRAMMING HELP", i );
	//HELP_draw_it();								 
	
}

/* ---------------------------------------------------------- */
/*  			    Structure Initialization				  */
/* ---------------------------------------------------------- */
extern void en_initialize_detail_struct( DEV_STATE_STRUCT *dev_state )
{
	//Alert_Message( "en_initialize_detail_struct" );

	// 5/27/2015 mpd : Initialize pointers to the EN specific detail structure.
	dev_state->en_details = &en_details;
	dev_state->en_active_pvs = &en_active_pvs;
	dev_state->en_static_pvs = &en_static_pvs;

	// 3/23/2015 mpd : These tokens are an artifact from a development phase when the "dev_state" and all the PVS structs 
	// were allocated. The tokens were a sanity check for future device exchanges to verify the struct pointer had not been 
	//clobbered. Keep it here since the work is done. 
	strlcpy( dev_state->en_active_pvs->pvs_token, EN_PVS_TOKEN_STR, sizeof( dev_state->en_active_pvs->pvs_token ) );
	strlcpy( dev_state->en_static_pvs->pvs_token, EN_PVS_TOKEN_STR, sizeof( dev_state->en_static_pvs->pvs_token ) );

	// 4/21/2015 mpd : Set some reasonable default values.
	en_details.mask_bits = EN_PROGRAMMING_NETMASK_DEFAULT;

	// 6/11/2015 mpd : Scenario: DHCP was enabled (a DHCP name existed), it was disabled, and it's about to be 
	// reenabled. We don't want to stomp on a long custom name if we ever knew it.
	// This flag tells us it's safe to stomp on any DHCP name that existed in the device the last time DHCP was enabled.
	// Or conversely, it prevents us from stomping on a name that was temporarily hidden from view during the current 
	// session.
	en_details.dhcp_name_not_set = true;

	// 4/21/2015 mpd : Clear all the string values.

	// 5/1/2015 mpd : These fields are not currently needed by EN Programming. They have been tested and are functional.
	//strlcpy( dev_state->model,           "", sizeof( dev_state->model ) ); 
	//strlcpy( dev_state->firmware_code,   "", sizeof( dev_state->firmware_code ) ); 
	//strlcpy( dev_state->hardware,        "", sizeof( dev_state->hardware ) ); 
	//strlcpy( dev_state->mon_mac_address, "", sizeof( dev_state->mon_mac_address ) ); 

	//strlcpy( dev_state->mon_ip_address,    "", sizeof( dev_state->mon_ip_address ) );          
	//strlcpy( dev_state->mon_gw_address,    "", sizeof( dev_state->mon_gw_address ) );           
	//strlcpy( dev_state->mon_mask,          "", sizeof( dev_state->mon_mask ) );              

	strlcpy( en_details.device,      "", sizeof( en_details.device ) ); 
	strlcpy( en_details.mac_address, "", sizeof( en_details.mac_address ) );                

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	UDS_STATE_wait_after_a_powerdown		(1)

#define	UDS_STATE_waiting_for_connection		(2)

typedef struct
{
	UNS_32	state;
	
	UNS_32	port;
	
	UNS_32	wait_count;
	
} UDS_control_structure;

static UDS_control_structure	uds_cs;

/* ---------------------------------------------------------- */
extern void UDS1100_initialize_the_connection_process( UNS_32 pport )
{
	if( pport != UPORT_A )
	{
		Alert_Message( "Why is the UDS1100 unit not on PORT A?" );
	}

	// 11/11/2016 rmd : For the UDS1100 we use the CD output to determine if connected. So check
	// to make sure that function is not NULL. Otherwise we couldn't call it and the connection
	// process would error out. Over and over again.
	if( port_device_table[ config_c.port_A_device_index ].__is_connected == NULL )
	{
		Alert_Message( "Unexpected NULL is_connected function" );
		
		// 2/15/2017 rmd : I think there is no point to trying to send messages anyway. They won't
		// go because we don't have a valid 'is connected' function! So do nothing in the face of
		// this error.
	}
	else
	{
		uds_cs.port = pport;
		
		uds_cs.wait_count = 0;
		
		// 11/11/2016 rmd : Do it. The UDS is set to auto connect. So our connect or reconnect
		// process involves cycling the power. And waiting for the connection. That's all.
		power_down_device( uds_cs.port );
		
		// ----------
		
		// 11/11/2016 rmd : First thing we do is to power down the device. And wait 5 seconds.
		xTimerChangePeriod( cics.process_timer, MS_to_TICKS(5000), portMAX_DELAY );
	
		uds_cs.state = UDS_STATE_wait_after_a_powerdown;
	}
}

/* ---------------------------------------------------------- */
extern void UDS1100_connection_processing( UNS_32 pevent )
{
	BOOL_32	lerror;
	
	lerror = (false);
	
	// ----------
	
	if( (pevent != CI_EVENT_process_string_found) && (pevent != CI_EVENT_process_timer_fired) )
	{
		// 10/29/2016 rmd : ERROR.
		Alert_Message( "Connection Process : UNXEXP EVENT" );
		
		lerror = (true);
	}
	else
	{
		switch( uds_cs.state )
		{
			case UDS_STATE_wait_after_a_powerdown:
				power_up_device( uds_cs.port );
				
				// 11/11/2016 rmd : Power up the device and wait 15 seconds to let it boot before checking
				// for the connection. Be careful not to catch some transitory CD action that may take place
				// during the boot process. I have seen some devices do this. I don't know if this one does.
				// But it doesn't matter. We can wait however long we want before checking the CD line.
				// Let's wait 15 seconds.
				xTimerChangePeriod( cics.process_timer, MS_to_TICKS(15000), portMAX_DELAY );
			
				uds_cs.state = UDS_STATE_waiting_for_connection;
				break;
				
			case UDS_STATE_waiting_for_connection:
				if((port_device_table[ config_c.port_A_device_index ].__is_connected != NULL) )
				{
					if( port_device_table[ config_c.port_A_device_index ].__is_connected( UPORT_A ) )
					{
						// 11/11/2016 rmd : Okay we're done. Device connected. Update the Comm Test string so the
						// user can see the current status. And alert.
						strlcpy( GuiVar_CommTestStatus, "Ethernet Connection", sizeof(GuiVar_CommTestStatus) );
					
						Alert_Message( "UDS1100-IAP Ethernet Connected" );
						
						// ----------
						
						CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities();
					}
					else
					{
						// 1/24/2017 rmd : There is a HUGE disconnect between the device programming and the
						// controller initiated connection sequence. One is really NOT aware of the other. And they
						// do not co-exist well. For example if the connection process fails it just keeps trying
						// over and over again. And if you are performing device programming the power down-->up
						// associated with the connection process could occur right in the middle of device
						// programming. Not sure how well that would play out. And we have to allow the device
						// programming to proceed right now cause there ain't a way to stop the connection process.
						// Leaving this time large helps give the user a better chance of success while device
						// programming. A real cop-out but all I can do for right now.
						//
						// 1/24/2017 rmd : PERHAPS a mechanism to terminate the connection sequence to allow the
						// user a device programming interlude. A flag we set that the connection_processing
						// function would act upon.
						if( uds_cs.wait_count >= 120 )
						{
							// 11/11/2016 rmd : We've been waiting. EN normally connects very quickly. Restart the
							// connection process.
							lerror = (true);
						}
						else
						{
							uds_cs.wait_count += 1;
							
							xTimerChangePeriod( cics.process_timer, MS_to_TICKS(1000), portMAX_DELAY );
						}
					}
				}
				else
				{
					// 11/11/2016 rmd : The sequence will restart and we have a test in the initialization
					// function for the NULL function.
					lerror = (true);
				}
				break;
				
		}  // of the switch statement
		
	}  // of an UNK event.


	if( lerror )
	{
		// 2/3/2017 rmd : Okay well the mode is no longer 'connecting'. And it shouldn't be 'ready'.
		// So set it to 'waiting to connect'. Doing so ensures when the connection timer fires and
		// we try to start a connection sequence we really to start it. If the mode were still
		// 'connecting' it is possible to not re-start the sequence.
		cics.mode = CI_MODE_waiting_to_connect;
		
		// 10/29/2016 rmd : Send the connection error event. To start the sequence over.
		CONTROLLER_INITIATED_post_event( CI_EVENT_connection_process_error );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

