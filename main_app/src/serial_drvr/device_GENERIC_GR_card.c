/*  file = device_GENERIC_GR_card.c                 2014  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy
#include	<string.h>

#include	"device_generic_gr_card.h"

#include	"alerts.h"

#include	"configuration_controller.h"

#include	"gpio_setup.h"

#include	"lpc3250_chip.h"

#include	"lpc32xx_gpio.h"

#include	"serial.h"

#include	"serport_drvr.h"

#include	"cs_mem.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/10/2014 rmd : A group of common functions to control the GR card. Introduced
// to save code space. These are used by several devices. The GR card is used to interface
// to most any 232 serially interfaced device. In retrospect, a better name for the card
// would have been '232 card'.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void __GENERIC_gr_card_power_control( const UNS_32 pport, const BOOL_32 pturn_me_ON )
{
	// 11/14/2016 rmd : Any task may call this function. At any time.
	
	// 11/14/2016 rmd : Directly controls the hardware ENABLE pin of the switching power supply
	// on the device interface cards. The interface card itself want a LOW to turn ON the
	// supply. There is an hardware inversion (HC14) on the main board. So logically this looks
	// like we set ENABLE HIGH to turn ON. And LOW to turn OFF.
	
	if( pport == UPORT_A )
	{
		if( pturn_me_ON )
		{
			if( PORT_A_ENABLE_is_LOW )
			{
				Alert_device_powered_on_or_off( pport, pturn_me_ON, (char *)comm_device_names[ config_c.port_A_device_index ] );

				PORT_A_ENABLE_HIGH;
			}
		}
		else
		{
			if( PORT_A_ENABLE_is_HIGH )
			{
				Alert_device_powered_on_or_off( pport, pturn_me_ON, (char *)comm_device_names[ config_c.port_A_device_index ] );
	
				PORT_A_ENABLE_LOW;
			}
		}
	}
	else
	if( pport == UPORT_B )
	{
		if( pturn_me_ON )
		{
			if( PORT_B_ENABLE_is_LOW )
			{
				Alert_device_powered_on_or_off( pport, pturn_me_ON, (char *)comm_device_names[ config_c.port_B_device_index ] );
	
				PORT_B_ENABLE_HIGH;
			}
		}
		else
		{
			if( PORT_B_ENABLE_is_HIGH )
			{
				Alert_device_powered_on_or_off( pport, pturn_me_ON, (char *)comm_device_names[ config_c.port_B_device_index ] );
				
				PORT_B_ENABLE_LOW;
			}
		}
	}
	else
	{
		Alert_Message( "GR PWR CONTROL: invalid port" );
	}
}
	
/* ---------------------------------------------------------- */
extern BOOL_32 GENERIC_GR_card_power_is_on( const UNS_32 pport )
{
	// 6/5/2015 mpd : This function returns the power status of the device on the specified port.

	BOOL_32 power_is_on;

	power_is_on = (false);
	
	// ----------
		
	if( ( pport == UPORT_A ) && ( PORT_A_ENABLE_is_HIGH ) )
	{
		power_is_on = (true);
	}
	else
	if( ( pport == UPORT_B ) && ( PORT_B_ENABLE_is_HIGH ) )
	{
		power_is_on = (true);
	}

	// ----------

	return( power_is_on );
}
	
/* ---------------------------------------------------------- */
extern void __GENERIC_gr_card_disconnect( const UNS_32 pport )
{
	UNS_32		connect_level, disconnect_level, elapsed_ms;
	
	BOOL_32		has_disconnected;

	// ----------
	
	// 8/13/2014 rmd : A sanity check of sorts. To see if we are ever asked to disconnect when
	// the unit is already disconnected.
	//if( !__GENERIC_gr_card_is_connected( pport ) )
	//{
		// 8/13/2014 rmd : Not really that interesting so I supressed.
		//Alert_Message( "Being asked to disconnect: already disconnected!" );
	//}
	
	// ----------
	
	// 8/6/2013 rmd : Is it okay to use the logical inverse (boolean) of the connect state value
	// to get the disconnect? Yes it is, but maybe better to use EXOR.
	if( pport == UPORT_A )
	{
		connect_level = port_device_table[ config_c.port_A_device_index ].dtr_level_to_connect;

		disconnect_level = (port_device_table[ config_c.port_A_device_index ].dtr_level_to_connect ^ 1);
	}
	else
	{
		connect_level = port_device_table[ config_c.port_B_device_index ].dtr_level_to_connect;

		disconnect_level = (port_device_table[ config_c.port_B_device_index ].dtr_level_to_connect ^ 1);
	}

	// ----------

	// 9/11/2014 rmd : An update on the disconnect philosophy. First of all after each and any
	// line transition we want to give the device some breather room. We have experienced some
	// issues with what MAY be serial port front-end misbehavior on both the WBX2100E and the
	// Lantronix GR. So in light of that I am going to intentionally slow things down. Kepp in
	// mind thie VERY important point. After the disconnect process completes and the serial
	// port DTR line is once again static, there is an intentional delay to pause the controller
	// initiated task from feeding its next ATDT string to the device. This could happen very
	// quickly following a disconnect if a second message was queued and waiting. And this does
	// happen fairly frequently. For example alerts and station history back to back bam bam.
	
	SetDTR( pport, disconnect_level );

	elapsed_ms = 0;
	
	has_disconnected = (false);
	
	// 8/8/2013 rmd : Wait up to 10 seconds for the CD line to indicate the device is not
	// connected.
	do
	{
		// 9/16/2014 rmd : Actual disconnect times are unknown. Assume a few seconds is needed.
		if( !__GENERIC_gr_card_is_connected( pport ) )
		{
			has_disconnected = (true);

			break;
		}
		
		// ----------
		
		vTaskDelay( MS_to_TICKS( 25 ) );
		
		elapsed_ms += 25;

	} while( elapsed_ms < GENERIC_GR_DISCONNECT_WAIT_MS );

	// ----------

	// 5/8/2015 rmd : Device breathing room delays. To avoid any device quirks. I despise these
	// kinds of delays but over the years have seen sometimes they help. And probably don't
	// hurt.
	
	// 9/11/2014 rmd : Wait before restoring DTR.
	vTaskDelay( MS_to_TICKS( 250 ) );

	// 8/8/2013 rmd : Restore DTR to allow incoming connections.
	SetDTR( pport, connect_level );

	// 9/11/2014 rmd : And wait after restoring before allowing the CI task to potentially fire
	// data at the device.
	vTaskDelay( MS_to_TICKS( 250 ) );
	
	// ----------

	if( has_disconnected )
	{
		Alert_Message( "Disconnect : successful" );

		// 5/8/2015 rmd : Serial driver no longer involved with disconnects.
		//postSerportDrvrEvent( pport, UPEV_DISCONNECT_SUCCESS );
	}
	else
	{
		Alert_Message( "Disconnect : failed" );

		// 5/8/2015 rmd : Serial driver no longer involved with disconnects.
		//postSerportDrvrEvent( pport, UPEV_DISCONNECT_FAILED );
	}
}

/* ---------------------------------------------------------- */
extern BOOL_32 __GENERIC_gr_card_is_connected( const UNS_32 pport )
{
	// 8/5/2014 ajv : For devices that use the DCD output to indicate a data
	// mode TCP/IP connection we can utilize this function. So far that includes
	// the Lantronix UDS1100-IAP and the Sierra Wireless AirLink Raven X and
	// LS300 radios.
	//
	// 8/8/2014 rmd : The Lantronix WBX2100E and the PremierWave XC cell radio use their DTR
	// output to signal that the unit has an active IP connection. On the interface card we have
	// wired the DTR output to our CD input. And therefore we can continue to use this function
	// with those devices. With no CS300 firmware deviations from the GENERIC code.

	// ----------

	if( pport == UPORT_A )
	{
		PORT_CD_STATE( A );
	}
	else
	{
		PORT_CD_STATE( B );
	}

	return( SerDrvrVars_s[ pport ].modem_control_line_status.i_cd );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

