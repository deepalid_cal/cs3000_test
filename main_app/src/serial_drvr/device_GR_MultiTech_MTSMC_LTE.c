/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/5/2015 mpd : Required for atoi
#include	<stdlib.h>

#include	<string.h>

#include	"device_gr_multitech_mtsmc_lte.h"

#include	"alerts.h"

#include	"rcvd_data.h"

#include	"serport_drvr.h"

#include	"controller_initiated.h"

#include	"comm_mngr.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/24/2018 rmd : NOTES about 3G/3.5G/4G

//	1.	The MultiTech MTSMC-LAT1 is a high speed CAT3 radio.
//
//	2.	The MTSMC-LAT3 is a lower speed CAT1 radio. It uses the Telit LE910-NA1 chip.
//
//	3.	UTRAN is a 3G/3.5G network access.
//
//	4.	E-UTRAN is considered network access at a 4G level.
//
//  5.	Packet Data Protocol (PDP) context is a data structure that allows the device to
//  	transmit data using Internet Protocol. It includes the device�s IP address, IMSI
//  	and additional parameters to properly route data to and from the network. The
//  	#sgact=1,1 command activates the PDP context.
//
//	6.	asdasd

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

const char pacsp0_str[] = "\r\n+PACSP0\r\n";

// 10/29/2016 rmd : Factory default is echo is ON. Turn it off to get it out of our hair.
// But we do this in the blind because we don't know if it is on or off. And therefore we
// don't know what to expect for the response (echoed chars along with response, or not)
const char turn_echo_off_str[] = "ATE0\r";


const char at_str[] = "AT\r";

const char ok_response_str[] = "\r\nOK\r\n";

const char diverity_antenna_off_str[] = "at#rxdiv=0,1\r";

const char apn_str[] = "at+cgdcont=1,\"IP\",\"10429.mcs\"\r";

// ----------

const char creg_response_str[] = "\r\n+CREG: ";

const char cops_response_str[] = "\r\n+COPS: ";

const char csq_response_str[] = "\r\n+CSQ: ";

const char iccid_response_str[] = "\r\n#CCID: ";

// ----------

// 11/1/2016 rmd : DISABLE the socket inactivity timeout. And transmit data in 100ms after a
// partial packet of 300 bytes is received.
const char socket_cfg_str[] = "at#scfg=1,1,300,0,600,1\r";

// 11/1/2016 rmd : Configure the socket for a xx minute keepalive. If it fails the next one
// is sent 75 seconds later. If 9 in a row fail the connection is dropped.
//
// 11/4/2016 rmd : Note a 45 minute keep alive does not work. A 35 minute keep alive does
// not work. A 29 does. So I'm setting to a 25 minute keep alive. What I mean by does not
// work is that 'somebody' between the controller and the commserver breaks the socket due
// to the inactivity it sees. It is pretty apparent there is a 30 minute inactivity timer
// somewhere in the AT&T network.
const char socket_cfgext_str[] = "at#scfgext=1,0,0,25\r";

// ----------

const char creg_format_str[] = "at+creg=2\r";

const char creg_query_str[] = "at+creg?\r";

const char cops_format_str[] = "at+cops=3,2\r";

const char cops_query_str[] = "at+cops?\r";

const char csq_query_str[] = "at+csq\r";

const char iccid_query_str[] = "at#ccid\r";

// ----------

// 7/24/2018 rmd : This is referred to as PDP CONTEXT ACTIVATION in the Telit manual.
const char pdp_context_str[] = "at#sgact=1,1\r";

const char connect_response_str[] = "\r\nCONNECT\r\n";

// 11/30/2016 rmd : For production regular commserver use port 16001.
//  				For production hub commserver use port 16003.
//
//  				For prerelease commserver use port 16002.
//
//  				For bob's hub test commserver use port 16004.
#if PROGRAM_CENTRAL_DEVICE_TO_PORT_16001

	const char socket_connection_str[] = "at#sd=1,0,16001,\"64.73.242.99\"\r";

#elif PROGRAM_CENTRAL_DEVICE_TO_PORT_16003

	const char socket_connection_str[] = "at#sd=1,0,16003,\"64.73.242.99\"\r";

#elif PROGRAM_CENTRAL_DEVICE_TO_PORT_16004

	const char socket_connection_str[] = "at#sd=1,0,16004,\"64.73.242.99\"\r";
	
#else

	#error COMPILING CODE WITHOUT DEVICE PORT SELECTION!
	
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	MTCS_STATE_wait_following_a_powerup				(1)

#define	MTCS_STATE_waiting_for_string_out_of_reset		(2)

#define	MTCS_STATE_waiting_for_turning_echo_off			(3)

#define	MTCS_STATE_waiting_for_initial_AT_ok			(4)

#define	MTCS_STATE_waiting_for_div_ant_off_ok			(5)

#define	MTCS_STATE_waiting_for_APN_programming_ok		(6)

#define	MTCS_STATE_waiting_for_socket_cfg_ok			(7)

#define	MTCS_STATE_waiting_for_socket_cfgext_ok			(8)

#define	MTCS_STATE_waiting_for_NETWORK_connection		(9)

#define	MTCS_STATE_waiting_for_CREG_format_ok			(10)

#define	MTCS_STATE_waiting_for_CREG_response_string		(11)

#define	MTCS_STATE_waiting_for_COPS_format_ok			(12)

#define	MTCS_STATE_waiting_for_COPS_response_string		(13)

#define	MTCS_STATE_waiting_for_CSQ_response_string		(14)

#define	MTCS_STATE_waiting_for_ICCID_response_string	(15)

#define	MTCS_STATE_waiting_for_PDP_CONTEXT_ok			(16)

#define	MTCS_STATE_waiting_for_SOCKET_CONNECT_str		(17)

// ----------

// 2/1/2017 rmd : The formula is 40 seconds plus the count times 10 seconds. So a value of 8
// means we wait 120 seconds for the APN link and data connection to become established.
// Well that is not enough. I have seen connections take 135 seconds. So we need to wait
// something like 200 seconds. Which would be a count of 16.
#define	MTCS_DATA_CONNECT_ATTEMPTS_LIMIT			(16)

// 2/2/2017 rmd : Well this is a bit of a an unknown. Only once have I seen the data
// connection succesful but the socket connection fail. I don't think this can hurt anything
// by trying a few times to make the socket connection.
#define	MTCS_SOCKET_CONNECT_ATTEMPTS_LIMIT			(4)

// ----------

typedef struct
{
	UNS_32	state;
	
	UNS_32	connection_start_time;
	
	UNS_32	connect_attempts; 
	
} MT_control_structure;

MT_control_structure	mtcs;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void mt_string_exchange( const char* pcommand_str, const char* presponse_str, UNS_32 pms_to_wait, UNS_32 pdepth_into_buffer )
{
	DATA_HANDLE	ldh;
	
	// 2/13/2014 rmd : Clean and restart the port buffer and string hunt.
	RCVD_DATA_enable_hunting_mode( UPORT_A, PORT_HUNT_FOR_SPECIFIED_STRING, WHEN_STRING_FOUND_NOTIFY_THE_CI_TASK );
	
	// ----------
	
	// 10/29/2016 rmd : Define what we're huting for.
	SerDrvrVars_s[ UPORT_A ].UartRingBuffer_s.sh.str_to_find = (char*)presponse_str;
	
	SerDrvrVars_s[ UPORT_A ].UartRingBuffer_s.sh.chars_to_match = strlen( presponse_str );

	SerDrvrVars_s[ UPORT_A ].UartRingBuffer_s.sh.depth_into_the_buffer_to_look = pdepth_into_buffer;
	
	// ----------
	
	ldh.dptr = (UNS_8*)pcommand_str;
	
	ldh.dlen = strlen( pcommand_str );
	
	// 10/29/2016 rmd : Sometimes we aren't sending a string but are looking for a response. Out
	// of reset for example.
	if( ldh.dlen > 0 )
	{
		AddCopyOfBlockToXmitList( UPORT_A, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );
	}
	
	// ----------
	
	xTimerChangePeriod( cics.process_timer, MS_to_TICKS(pms_to_wait), portMAX_DELAY );
}

/* ---------------------------------------------------------- */
extern void MTSMC_LAT_initialize_the_connection_process( UNS_32 pport )
{
	if( pport != UPORT_A )
	{
		Alert_Message( "Why is the MultiTech LTE unit not on PORT A?" );
	}

	// ----------
	
	// 11/11/2016 rmd : If the device is already powered on this function recognizes that and
	// does nothing. If not powered on it powers up the radio.
	power_up_device( UPORT_A );
	
	mtcs.connect_attempts = 0;

	mtcs.connection_start_time = my_tick_count;

	mtcs.state = MTCS_STATE_wait_following_a_powerup;
	
	xTimerChangePeriod( cics.process_timer, MS_to_TICKS(4000), portMAX_DELAY );
}

/* ---------------------------------------------------------- */
extern void MTSMC_LAT_connection_processing( UNS_32 pevent )
{
	BOOL_32	lerror;
	
	lerror = (false);
	
	// ----------
	
	if( (pevent != CI_EVENT_process_string_found) && (pevent != CI_EVENT_process_timer_fired) )
	{
		// 10/29/2016 rmd : ERROR.
		Alert_Message( "Connection Process : UNXEXP EVENT" );
		
		lerror = (true);
	}
	else
	{
		switch( mtcs.state )
		{
			case MTCS_STATE_wait_following_a_powerup:
	
				// 10/29/2016 rmd : Perform the reset sequence. I've seen it take up to 30 seconds for the
				// string to arrive out of reset. NOTE - if you don't have the SIM installed you get NO
				// STRING AT ALL.
				// 1/10/2017 rmd : The new CAT1 LTE unit (part no MTSMC-LAT3), spits out some other string
				// content PRIOR to the pacsp0_str. So we need to allow searching into the buffer 20 deep to
				// find the string.
				mt_string_exchange( "", pacsp0_str, 60000, 20 );
				
				set_reset_ACTIVE_to_serial_port_device( UPORT_A );
				
				// 11/1/2016 rmd : Haven't seen a specification on the width of the reset pulse.
				vTaskDelay( MS_to_TICKS( 250 ) );
				
				set_reset_INACTIVE_to_serial_port_device( UPORT_A );
				
				mtcs.state = MTCS_STATE_waiting_for_string_out_of_reset;
				break;
				
			case MTCS_STATE_waiting_for_string_out_of_reset:
	
				if( pevent == CI_EVENT_process_timer_fired )
				{
					Alert_Message( "ERROR: REMOVE and REINSTALL YOUR SIM (missing reset string)" );

					lerror = (true);
				}
				else
				if( pevent == CI_EVENT_process_string_found )
				{
					// 10/29/2016 rmd : Okay we got the string. Now send an AT and wait for the OK. Which is
					// echo is ON we won't get the OK. The echoed chars will mess us up. But that is ok because
					// we don't care at this stage if we time out. We won't consider it an error.
					mt_string_exchange( turn_echo_off_str, ok_response_str, 1000, 6 );
					
					mtcs.state = MTCS_STATE_waiting_for_turning_echo_off;
				}
				break;
				
			case MTCS_STATE_waiting_for_turning_echo_off:
	
				if( pevent == CI_EVENT_process_timer_fired )
				{
					Alert_Message( "ERROR: waiting for echo off ok" );

					lerror = (true);
				}
				else
				if( pevent == CI_EVENT_process_string_found )
				{
					// 10/29/2016 rmd : Either way it is not an error. Because if echo is on the string hunt
					// fails. We'll declare errors from this state forward.
					mt_string_exchange( at_str, ok_response_str, 1000, 4 );
					
					mtcs.state = MTCS_STATE_waiting_for_initial_AT_ok;
				}
				break;
				
			case MTCS_STATE_waiting_for_initial_AT_ok:
	
				if( pevent == CI_EVENT_process_timer_fired )
				{
					Alert_Message( "ERROR: waiting for simple AT ok" );

					lerror = (true);
				}
				else
				if( pevent == CI_EVENT_process_string_found )
				{
					// 10/29/2016 rmd : Okay we got the string. Now program the APN.
					mt_string_exchange( diverity_antenna_off_str, ok_response_str, 1000, 4 );
					
					mtcs.state = MTCS_STATE_waiting_for_div_ant_off_ok;
				}
				break;
				
			case MTCS_STATE_waiting_for_div_ant_off_ok:
	
				if( pevent == CI_EVENT_process_timer_fired )
				{
					Alert_Message( "ERROR: waiting for div ant off ok" );

					lerror = (true);
				}
				else
				if( pevent == CI_EVENT_process_string_found )
				{
					// 10/29/2016 rmd : Okay we got the string. Now program the APN.
					mt_string_exchange( apn_str, ok_response_str, 1000, 4 );
					
					mtcs.state = MTCS_STATE_waiting_for_APN_programming_ok;
				}
				break;
				
			case MTCS_STATE_waiting_for_APN_programming_ok:
	
				if( pevent == CI_EVENT_process_timer_fired )
				{
					Alert_Message( "ERROR: waiting for APN ok" );

					lerror = (true);
				}
				else
				if( pevent == CI_EVENT_process_string_found )
				{
					// 10/29/2016 rmd : Okay we got the string. Now configure the socket. In particular, disable
					// the inactivity timer.
					mt_string_exchange( socket_cfg_str, ok_response_str, 1000, 4 );
					
					mtcs.state = MTCS_STATE_waiting_for_socket_cfg_ok;
				}
				break;
				
			case MTCS_STATE_waiting_for_socket_cfg_ok:
	
				if( pevent == CI_EVENT_process_timer_fired )
				{
					Alert_Message( "ERROR: waiting for socket cfg ok" );

					lerror = (true);
				}
				else
				if( pevent == CI_EVENT_process_string_found )
				{
					mt_string_exchange( socket_cfgext_str, ok_response_str, 1000, 4 );
					
					mtcs.state = MTCS_STATE_waiting_for_socket_cfgext_ok;
				}
				break;
				
			case MTCS_STATE_waiting_for_socket_cfgext_ok:
	
				if( pevent == CI_EVENT_process_timer_fired )
				{
					Alert_Message( "ERROR: waiting for socket cfgext ok" );

					lerror = (true);
				}
				else
				if( pevent == CI_EVENT_process_string_found )
				{
					// 8/1/2018 rmd : Now wait. My assumption is for the unit to find and interact with a cell
					// signal, after which we should be able to gather some info about the access technology (3G
					// or 4G), tower identification, and signal quality. I have waited as little as 10 seconds
					// here, and it mostly worked fine. I'll wait 20 seconds to give us more time before
					// querying the unit.
					mt_string_exchange( "", "", 20000, 0 );

					mtcs.state = MTCS_STATE_waiting_for_NETWORK_connection;
				}
				break;
				
			// ----------

			case MTCS_STATE_waiting_for_NETWORK_connection:
	
				// 7/28/2018 rmd : The only way to reach here is when the TIMER FIRES. And that is by
				// design, we effectively just waited 10 seconds to let the network connection stabilize.
				//
				// 7/28/2018 rmd : Now program the CREG response format.
				mt_string_exchange( creg_format_str, ok_response_str, 1000, 4 );
				
				mtcs.state = MTCS_STATE_waiting_for_CREG_format_ok;

				break;
				
			case MTCS_STATE_waiting_for_CREG_format_ok:
			
				if( pevent == CI_EVENT_process_timer_fired )
				{
					Alert_Message( "ERROR: waiting for creg format ok" );

					lerror = (true);
				}
				else
				if( pevent == CI_EVENT_process_string_found )
				{
					mt_string_exchange( creg_query_str, creg_response_str, 1000, 4 );
					
					mtcs.state = MTCS_STATE_waiting_for_CREG_response_string;
				}
				break;
				
			case MTCS_STATE_waiting_for_CREG_response_string:
			
				// 7/28/2018 rmd : Let more characters come in, terminate the string, then skip over the
				// initial <CR><LF> and display what is in the buffer.
				vTaskDelay( 25 );
				SerDrvrVars_s[ UPORT_A ].UartRingBuffer_s.ring[ 31 ] = 0x00;
				Alert_Message_va( "%s", &(SerDrvrVars_s[ UPORT_A ].UartRingBuffer_s.ring[ 2 ]) );

				// ----------
				
				if( pevent == CI_EVENT_process_timer_fired )
				{
					Alert_Message( "ERROR: waiting for creg response" );

					lerror = (true);
				}
				else
				if( pevent == CI_EVENT_process_string_found )
				{
					// 7/28/2018 rmd : Now program the COPS response format.
					mt_string_exchange( cops_format_str, ok_response_str, 1000, 4 );
					
					mtcs.state = MTCS_STATE_waiting_for_COPS_format_ok;
				}
				break;
				

			case MTCS_STATE_waiting_for_COPS_format_ok:
			
				if( pevent == CI_EVENT_process_timer_fired )
				{
					Alert_Message( "ERROR: waiting for cops format ok" );

					lerror = (true);
				}
				else
				if( pevent == CI_EVENT_process_string_found )
				{
					mt_string_exchange( cops_query_str, cops_response_str, 1000, 4 );
					
					mtcs.state = MTCS_STATE_waiting_for_COPS_response_string;
				}
				break;
				

			case MTCS_STATE_waiting_for_COPS_response_string:
			
				// 7/28/2018 rmd : Let more characters come in, terminate the string, then skip over the
				// initial <CR><LF> and display what is in the buffer.
				vTaskDelay( 25 );
				SerDrvrVars_s[ UPORT_A ].UartRingBuffer_s.ring[ 23 ] = 0x00;
				Alert_Message_va( "%s", &(SerDrvrVars_s[ UPORT_A ].UartRingBuffer_s.ring[ 2 ]) );

				// ----------
				
				if( pevent == CI_EVENT_process_timer_fired )
				{
					Alert_Message( "ERROR: waiting for cops response" );

					lerror = (true);
				}
				else
				if( pevent == CI_EVENT_process_string_found )
				{
					// 7/28/2018 rmd : Now program the COPS response format.
					mt_string_exchange( csq_query_str, csq_response_str, 1000, 4 );
					
					mtcs.state = MTCS_STATE_waiting_for_CSQ_response_string;
				}
				break;
				
			case MTCS_STATE_waiting_for_CSQ_response_string:
			
				// 7/28/2018 rmd : Let more characters come in, terminate the string, then skip over the
				// initial <CR><LF> and display what is in the buffer.
				vTaskDelay( 25 );
				SerDrvrVars_s[ UPORT_A ].UartRingBuffer_s.ring[ 12 ] = 0x00;
				Alert_Message_va( "%s", &(SerDrvrVars_s[ UPORT_A ].UartRingBuffer_s.ring[ 2 ]) );

				// ----------
				
				if( pevent == CI_EVENT_process_timer_fired )
				{
					Alert_Message( "ERROR: waiting for csq response" );

					lerror = (true);
				}
				else
				if( pevent == CI_EVENT_process_string_found )
				{
					// 7/28/2018 rmd : Now program the COPS response format.
					mt_string_exchange( iccid_query_str, iccid_response_str, 1000, 4 );
					
					mtcs.state = MTCS_STATE_waiting_for_ICCID_response_string;
				}
				break;
				
			case MTCS_STATE_waiting_for_ICCID_response_string:
	
				// 7/28/2018 rmd : Let more characters come in, terminate the string, then skip over the
				// initial <CR><LF> and display what is in the buffer.
				vTaskDelay( 25 );
				SerDrvrVars_s[ UPORT_A ].UartRingBuffer_s.ring[ 29 ] = 0x00;
				Alert_Message_va( "%s", &(SerDrvrVars_s[ UPORT_A ].UartRingBuffer_s.ring[ 2 ]) );

				// ----------
				
				if( pevent == CI_EVENT_process_timer_fired )
				{
					Alert_Message( "ERROR: waiting for iccid response string" );

					lerror = (true);
				}
				else
				if( pevent == CI_EVENT_process_string_found )
				{
					// 7/28/2018 rmd : Regarding the PDP CONTEXT response: the OK string is buried in there
					// after the connected IP number. The response looks like #SGACT: 10.10.2.83 for example.
					// Then the OK. So dig into the buffer 35 chars.
					mt_string_exchange( pdp_context_str, ok_response_str, 5000, 35 );
					
					mtcs.connect_attempts = 0;
					
					Alert_Message_va( "PDP Context Activation attempt %u", mtcs.connect_attempts + 1 );
	
					mtcs.state = MTCS_STATE_waiting_for_PDP_CONTEXT_ok;
				}
				break;
				
			// ----------

			case MTCS_STATE_waiting_for_PDP_CONTEXT_ok:
	
				if( pevent == CI_EVENT_process_timer_fired )
				{
					if( mtcs.connect_attempts < MTCS_DATA_CONNECT_ATTEMPTS_LIMIT )
					{
						Alert_Message_va( "PDP Context Activation attempt %u", mtcs.connect_attempts + 1 );

						// 2/3/2017 rmd : This can be surpressed after some time of seeing it work.
						// 3/20/2018 rmd : Commented out to manage alerts.
						// Alert_cellular_data_connection_attempt(); 
						
						mtcs.connect_attempts += 1;
						
						// 2/1/2017 rmd : And try again looping on this state.

						//mt_string_exchange( data_connection_str, ok_response_str, 10000, 35 );

						mt_string_exchange( pdp_context_str, ok_response_str, 5000, 35 );
					}
					else
					{
						Alert_Message( "ERROR: no data connection" );
						
						lerror = (true);
					}
				}
				else
				if( pevent == CI_EVENT_process_string_found )
				{
					Alert_Message( "PDP Context Activated" );

					// 10/29/2016 rmd : Okay we got the string. Now program the APN.
					mt_string_exchange( socket_connection_str, connect_response_str, 5000, 4 );
					
					mtcs.connect_attempts = 0;
					
					Alert_Message_va( "Socket Create attempt %u", mtcs.connect_attempts + 1 );

					mtcs.state = MTCS_STATE_waiting_for_SOCKET_CONNECT_str;
				}
				break;
				
			case MTCS_STATE_waiting_for_SOCKET_CONNECT_str:
	
				if( pevent == CI_EVENT_process_timer_fired )
				{
					if( mtcs.connect_attempts < MTCS_SOCKET_CONNECT_ATTEMPTS_LIMIT )
					{
						Alert_Message_va( "Socket Create attempt %u", mtcs.connect_attempts + 1 );

						// 2/3/2017 rmd : This can be surpressed after some time of seeing it work.
						// 3/20/2018 rmd : Commented out to manage alerts.
						// Alert_cellular_socket_attempt(); 
						
						mtcs.connect_attempts += 1;
						
						// 2/1/2017 rmd : And try again looping on this state.
						mt_string_exchange( socket_connection_str, connect_response_str, 5000, 4 );
					}
					else
					{
						Alert_Message( "ERROR: no socket connection" );
	
						lerror = (true);
					}
				}
				else
				if( pevent == CI_EVENT_process_string_found )
				{
					// 10/30/2016 rmd : Okay we're done. We have connected. Update the Comm Test string so the
					// user can see the current status. And alert.
					strlcpy( GuiVar_CommTestStatus, "Cellular Connection", sizeof(GuiVar_CommTestStatus) );
				
					#if PROGRAM_CENTRAL_DEVICE_TO_PORT_16001
					
						Alert_Message_va( "MultiTech Cell LTE 16001 Connected in %u seconds", ((my_tick_count - mtcs.connection_start_time)/200) );

					#elif PROGRAM_CENTRAL_DEVICE_TO_PORT_16003

						Alert_Message_va( "MultiTech Cell LTE 16003 Connected in %u seconds", ((my_tick_count - mtcs.connection_start_time)/200) );
						
					#elif PROGRAM_CENTRAL_DEVICE_TO_PORT_16004
					
						Alert_Message_va( "MultiTech Cell LTE 16004 Connected in %u seconds", ((my_tick_count - mtcs.connection_start_time)/200) );
						
					#else
					
						// 11/29/2017 rmd : If you got your defines wrong this would come up - and it did once upon
						// my screw-up!
						Alert_Message_va( "MultiTech UNKNOWN PORT connected in %u seconds", ((my_tick_count - mtcs.connection_start_time)/200) );
					
					#endif
					
					// ----------

					// 10/30/2016 rmd : Stop hunting.
					RCVD_DATA_enable_hunting_mode( UPORT_A, PORT_HUNT_FOR_PACKETS, NOT_STRING_HUNTING );
					
					// ----------
					
					CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities();
				}
				break;
				
		}  // of the switch statement
		
	}  // of an UNK event.


	if( lerror )
	{
		// 2/3/2017 rmd : Okay well the mode is no longer 'connecting'. And it shouldn't be 'ready'.
		// So set it to 'waiting to connect'. Doing so ensures when the connection timer fires and
		// we try to start a connection sequence we really to start it. If the mode were still
		// 'connecting' it is possible to not re-start the sequence.
		cics.mode = CI_MODE_waiting_to_connect;
		
		// 10/29/2016 rmd : Send the connection error event. To start the sequence over.
		CONTROLLER_INITIATED_post_event( CI_EVENT_connection_process_error );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

