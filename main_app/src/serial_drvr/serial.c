/*  file = serial.c                           02.09.2011  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy
#include	<string.h>

#include	"serial.h"

#include	"lpc32xx_uart_driver.h"
									
#include	"lpc32xx_hsuart_driver.h"

#include	"serport_drvr.h"

#include	"lpc32xx_intc_driver.h"

#include	"lpc32xx_clkpwr_driver.h"

#include	"lpc32xx_gpio_driver.h"

#include	"gpio_setup.h"

#include	"app_startup.h"

#include	"wdt_and_powerfail.h"

#include	"alerts.h"

#include	"configuration_controller.h"

#include	"crc.h"

#include	"controller_initiated.h"

#include	"cs_mem.h"

#include	"flowsense.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Hardware table for the Modem Control Signals

/*
    				|----------------------INPUTS TO LPC3250----------------------| 				|--------OUTPUTS------|				LPC3250 UART
					CTS					RI					CD					DSR					RTS					DTR
 
UPORT_TP			NONE				NONE				NONE				NONE				NONE				NONE			UART 1
 
UPORT_A				U3_CTS				UC_RI				U3_DCD				NONE				U3_RTS				U3_DTR			UART 3	
 
UPORT_B				GPI_00				GPI_03				GPI_02				NONE				GPO_01				GPO_04			UART 6
 
UPORT_RRE			GPI_22(note 2)		NONE				GPI_23(note 1)		NONE				NONE				NONE			UART 5
 
UPORT_USB			NONE				NONE				NONE				NONE				NONE				NONE			USB Channel

 
>>NOTES:	1) The RRE CD input must be polled. There is no interrupt input to the interrupt controller fo rhtis pin. This is okay. There is no 
    		known use for the CD input to us from the RRE.
 
    		2) The RRE CTS input is interrupt driven. However we can only see the transition in one direction. CTS is NL from the RRE. Our hardware
    		performs no inversion of the signal. Therefore the interrupt is set to look for the rising edge. After that a timer polls the signal
    		once each 100ms. When the low state is once again detected the driver task will be signaled.
 
*/ 

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	void*		addr;

	UNS_32		mic_uart_isr_number;
	
	UNS_32		mic_cts_isr_number;
	
} UART_DEFS;

UART_DEFS const uinfo[ UPORT_TOTAL_PHYSICAL_PORTS ] =
{
	{ UART1,	IIR1_INT,	0 },			// UPORT_TP high speed uart
	{ UART3,	IIR3_INT,	GPI_16_INT },	// UPORT_A regular uart
	{ UART6,	IIR6_INT,	GPI_00_INT },	// UPORT_B regular uart
	{ UART5,	IIR5_INT,	0 },			// UPORT_RRE regular uart - no cts implementation
	{ NULL,		0,			0 }				// UPORT_USB not a uart
};



//char* const port_names[ UPORT_TOTAL_SYSTEM_PORTS ] = { "Port TP", "Port A", "Port B", "Port RRE", "Port USB", "Port M1" };
const char* const port_names[ UPORT_TOTAL_SYSTEM_PORTS ] = { "Port TP", "Port A", "Port B", "Port RRE", "Port USB", "Port M1" };

 
// The transmit control structures. One for each serial port
XMIT_CONTROL_STRUCT_s xmit_cntrl[ UPORT_TOTAL_PHYSICAL_PORTS ];


char* const CTS_main_timer_names[ 5 ] = { "TP Main", "A Main", "B Main", "RRE Main", "USB Main" };  // The TP and USB timer names are never used.
char* const CTS_poll_timer_names[ 5 ] = { "TP Poll", "A Poll", "B Poll", "RRE Poll", "USB Poll" };  // The TP and USB timer names are never used.


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// When devices signal CTS to stop us transmitting their input buufer MUST be able to handle this many more incoming bytes. For
// some devices in the past I have seen 32 to be a problem. I think a value of 16 is universally trouble free.
#define BYTES_TO_PUSH	16

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void standard_uart_INT_HANDLER( UPORTS puport, UART_REGS_T *puart_base )
{
	// THIS IS AN INTERRUPT HANDLER. THUS BE AWARE OF WHAT CODE YOU US HEREIN.

	portBASE_TYPE			xTaskWoken;
	
	volatile UNS_32			jjj, iir_ul, llsr, lmsr, bytes_to_read_ul, number_to_load;
	
	UART_RING_BUFFER_s		*ptr_ring_buffer_s;		// for convenience - easier code to read

	SERPORT_EVENT_DEFS		event;

	XMIT_CONTROL_LIST_ITEM	*pListItem;
	

	// initialize if a higher priority task than us has been woken
	xTaskWoken = pdFALSE;

	// Determine the interrupt source
	iir_ul = (puart_base->iir_fcr & UART_IIR_INTSRC_MASK);
	
	// ONLY handle one level of priority PER INTERRUPT. I tried to loop reading the iir until it produced a 0x01. To no avail. Kept reading a 0x00.
	// Apparently not meant to work that way. The NXP code only handles one interrupt at a time. Then leaves the isr. So we will too.
	switch( iir_ul )
	{
		case UART_IIR_INTSRC_RXLINE:
			// The highest priority interrupt. Shows all the line errors. We keep stats on such. Read the LSR to clear.
			llsr = puart_base->lsr;
			
			if( llsr & UART_LSR_BI )
			{
				SerDrvrVars_s[ puport ].stats.errors_break += 1;				
			}
			
			if( llsr & UART_LSR_FR )
			{
				SerDrvrVars_s[ puport ].stats.errors_frame += 1;				
			}

			if( llsr & UART_LSR_PE )
			{
				SerDrvrVars_s[ puport ].stats.errors_parity += 1;				
			}

			if( llsr & UART_LSR_OE )
			{
				SerDrvrVars_s[ puport ].stats.errors_overrun += 1;				
			}
			break;

		case UART_IIR_INTSRC_RDA:
		case UART_IIR_INTSRC_CTI:
			// These are the 2nd highest priority interrupt. And means the rcv FIFO crossed it's trigger level OR some data arrived but not enough bytes
			// to reach the trigger level. And then the data stopped. In any case read the number of bytes in the FIFO.
			bytes_to_read_ul = (puart_base->rxlev & 0x7F);

			ptr_ring_buffer_s = &SerDrvrVars_s[ puport ].UartRingBuffer_s;

			for( jjj=0; jjj<bytes_to_read_ul; jjj++ )
			{
				ptr_ring_buffer_s->ring[ ptr_ring_buffer_s->next ] = puart_base->dll_fifo;

				ptr_ring_buffer_s->next++;
				if( ptr_ring_buffer_s->next == RING_BUFFER_SIZE )
				{
					ptr_ring_buffer_s->next = 0x00;
				}

				// If the tail ever catches up to index we've over run the buffer so either make it bigger or check it more often. ONLY test
				// for the mode we are active in cause for the inactive modes the tail will always eventually catch the index. And Latch
				// the response if ever TRUE.
				if ( (ptr_ring_buffer_s->ph_tail_caught_index == FALSE) && (ptr_ring_buffer_s->hunt_for_packets == TRUE) )
				{
					ptr_ring_buffer_s->ph_tail_caught_index = (ptr_ring_buffer_s->next == ptr_ring_buffer_s->ph.packet_index);
				}
				if ( (ptr_ring_buffer_s->dh_tail_caught_index == FALSE) && (ptr_ring_buffer_s->hunt_for_data == TRUE) )
				{
					ptr_ring_buffer_s->dh_tail_caught_index = (ptr_ring_buffer_s->next == ptr_ring_buffer_s->dh.data_index);
				}
				if ( (ptr_ring_buffer_s->sh_tail_caught_index == FALSE) && (ptr_ring_buffer_s->hunt_for_specified_string == TRUE) )
				{
					ptr_ring_buffer_s->sh_tail_caught_index = (ptr_ring_buffer_s->next == ptr_ring_buffer_s->sh.string_index);
				}
				if ( (ptr_ring_buffer_s->th_tail_caught_index == FALSE) && (ptr_ring_buffer_s->hunt_for_specified_termination == TRUE) )
				{
					ptr_ring_buffer_s->th_tail_caught_index = (ptr_ring_buffer_s->next == ptr_ring_buffer_s->th.string_index);
				}
			}
			
			SerDrvrVars_s[ puport ].stats.rcvd_bytes += bytes_to_read_ul;  // Update what has been rcvd.

			// signal that data has arrived
			xSemaphoreGiveFromISR( rcvd_data_binary_semaphore[ puport ], &xTaskWoken );
			break;

		case UART_IIR_INTSRC_THRE:
			// 6/2/2015 rmd : This is the third level priority interrupt. And was caused by the THR
			// becoming empty. Are there any more characters to transmit? Check CTS status
			//
			// 6/2/2015 rmd : Must check both cts and now_xmitting to be set. Has to do with the
			// interaction between the SER_DRVR task and the CTS interrupt. Once CTS goes false only the
			// 50ms polling timer can set it back true. But by then we rely on the TX FIFO empty
			// interrupt has occurred seeing cts false. There will not be another TX interrupt until the
			// serdrvr task can get around to processing the CTS events and kicking off the block
			// again.
			if( (xmit_cntrl[ puport ].now_xmitting != NULL) && SerDrvrVars_s[ puport ].modem_control_line_status.i_cts )
			{
				pListItem = xmit_cntrl[ puport ].now_xmitting;

				// 5/27/2015 rmd : The KICK off function loads only a single byte. And interrupts
				// immediately. If the block to xmit was only 1 byte in length the length here would be zero
				// - by design. The interrupt ALWAYS happens at the end of the block since it fires when the
				// FIFO goes empty.
				if( pListItem->Length == 0 )
				{
					// Post an EV_TX_BLK_SENT event to the serial driver for this port
					event = UPEV_TX_BLK_SENT;

					xQueueSendToBackFromISR( SerDrvrVars_s[ puport ].SerportDrvrEventQHandle, &event, &xTaskWoken );
				}
				else
				{
					// 5/27/2015 rmd : Since the FIFO TX interrupt level is set to 0 that means we KNOW the FIFO
					// is empty. We could load up to 32 bytes. But to be conservative will load up to 16. There
					// is no direct way to query the FIFO level. A bad oversight of the UART designers if you
					// ask me.
					number_to_load = ( (pListItem->Length >= BYTES_TO_PUSH) ? BYTES_TO_PUSH : pListItem->Length );

					for( jjj=0; jjj<number_to_load; jjj++ )
					{
						puart_base->dll_fifo = *pListItem->From;  // write one byte to the FIFO

						pListItem->From++;
					}

					pListItem->Length -= number_to_load;

					// Reflect whats xmitted. These are guaranteed to go. Regardless of what CTS does.
					SerDrvrVars_s[ puport ].stats.xmit_bytes += number_to_load;
				}	

			}  // of if we are currently xmitting a block
			break;

		case UART_IIR_MODEM_STS:
			// The lowest priority interrupt. The Modem Status Register. We have choosen to implement all the modem status signals using GPIO.
			// This is to keep the code consistent across all the ports. The LPC3250 only provides these signals on ONE prt and we are using 4
			// ports. Read the register to clear the interrupt just in case.
			lmsr = puart_base->modem_status;
			break;

	}  // of the switch
		
	if( xTaskWoken )
	{
		// This sets us up so that when we return from the interrupt we return to the highest priority task now ready.
		portYIELD_FROM_ISR();	
	}
}

/* ---------------------------------------------------------- */
static void highspeed_uart_INT_HANDLER( UPORTS puport, HSUART_REGS_T *puart_base )
{
	// THIS IS AN INTERRUPT HANDLER. THUS BE AWARE OF WHAT CODE YOU US HEREIN.

	portBASE_TYPE			xTaskWoken;
	
	volatile UNS_32			jjj, iir_source, bytes_to_read_ul, number_to_load;
	
	UART_RING_BUFFER_s		*ptr_ring_buffer_s;		// for convenience - easier code to read

	SERPORT_EVENT_DEFS		event;

	XMIT_CONTROL_LIST_ITEM	*pListItem;
	

	// initialize if a higher priority task than us has been woken
	xTaskWoken = pdFALSE;

	// Determine the interrupt source.
	iir_source = (puart_base->iir);
	
	if( (iir_source & (HSU_RX_TRIG_INT | HSU_RX_TIMEOUT_INT)) != 0 )
	{
		// Receiver interrupt needs servicing. And means the rcv FIFO crossed it's trigger level OR some data arrived but not enough
		// bytes to reach the trigger level. And then the data stopped. In any case read the number of bytes in the FIFO.
		bytes_to_read_ul =  HSU_RX_LEV( puart_base->level );

		ptr_ring_buffer_s = &SerDrvrVars_s[ puport ].UartRingBuffer_s;

		for( jjj=0; jjj<bytes_to_read_ul; jjj++ )
		{
			ptr_ring_buffer_s->ring[ ptr_ring_buffer_s->next ] = puart_base->txrx_fifo;

			ptr_ring_buffer_s->next++;
			if( ptr_ring_buffer_s->next == RING_BUFFER_SIZE )
			{
				ptr_ring_buffer_s->next = 0x00;
			}

			// If the tail ever catches up to index we've over run the buffer so either make it bigger or check it more often. ONLY test
			// for the mode we are active in cause for the inactive modes the tail will always eventually catch the index. And Latch
			// the response if ever TRUE.
			if ( (ptr_ring_buffer_s->ph_tail_caught_index == FALSE) && (ptr_ring_buffer_s->hunt_for_packets == TRUE) )
			{
				ptr_ring_buffer_s->ph_tail_caught_index = (ptr_ring_buffer_s->next == ptr_ring_buffer_s->ph.packet_index);
			}
			if ( (ptr_ring_buffer_s->dh_tail_caught_index == FALSE) && (ptr_ring_buffer_s->hunt_for_data == TRUE) )
			{
				ptr_ring_buffer_s->dh_tail_caught_index = (ptr_ring_buffer_s->next == ptr_ring_buffer_s->dh.data_index);
			}
			if ( (ptr_ring_buffer_s->sh_tail_caught_index == FALSE) && (ptr_ring_buffer_s->hunt_for_specified_string == TRUE) )
			{
				ptr_ring_buffer_s->sh_tail_caught_index = (ptr_ring_buffer_s->next == ptr_ring_buffer_s->sh.string_index);
			}
			if ( (ptr_ring_buffer_s->th_tail_caught_index == FALSE) && (ptr_ring_buffer_s->hunt_for_specified_termination == TRUE) )
			{
				ptr_ring_buffer_s->th_tail_caught_index = (ptr_ring_buffer_s->next == ptr_ring_buffer_s->th.string_index);
			}
		}

		SerDrvrVars_s[ puport ].stats.rcvd_bytes += bytes_to_read_ul;  // Update what has been rcvd.

		// signal that data has arrived
		xSemaphoreGiveFromISR( rcvd_data_binary_semaphore[ puport ], &xTaskWoken );
	}
	
	if( (iir_source & HSU_TX_INT) != 0 )
	{
		// Transmitter interrupt needs servicing. And was caused by the TX FIFO dropping below its trigger level. Are there any more
		// characters to transmit? No CTS test here. Not for the HSUART.

		// 6/3/2015 rmd : For safety test the now_xmitting pointer.
		if( xmit_cntrl[ puport ].now_xmitting != NULL )
		{
			pListItem = xmit_cntrl[ puport ].now_xmitting;

			// 5/27/2015 rmd : The KICK off function loads only a single byte. And interrupts
			// immediately. If the block to xmit was only 1 byte in length the length here would be zero
			// - by design. The interrupt ALWAYS happens at the end of the block since it fires when the
			// FIFO goes empty.
			if( pListItem->Length == 0 )
			{
				// Post an EV_TX_BLK_SENT event to the serial driver for this port
				event = UPEV_TX_BLK_SENT;

				xQueueSendToBackFromISR( SerDrvrVars_s[ puport ].SerportDrvrEventQHandle, &event, &xTaskWoken );
			}
			else
			{
				// 6/3/2015 rmd : NOTE - this code is for reference. For the high seed UART we can read the
				// current TX fifo level. And knowing this is a 64 byte FIFO can then feed the UART
				// appropriately. (This is much better than I found the standard UART fifo information to
				// be.
				//number_to_load = 64 - HSU_TX_LEV( puart_base->level );
				
				// 6/3/2015 rmd : This UART does have 64 byte FIFO buffers. However I'm sticking with 16
				// bytes maximum to be conservative.
				number_to_load = ( (pListItem->Length >= BYTES_TO_PUSH) ? BYTES_TO_PUSH : pListItem->Length );

				for( jjj=0; jjj<number_to_load; jjj++ )
				{
					puart_base->txrx_fifo = *pListItem->From;  // write one byte to the FIFO

					pListItem->From++;
				}

				pListItem->Length -= number_to_load;

				// Reflect whats xmitted.
				SerDrvrVars_s[ puport ].stats.xmit_bytes += number_to_load;
			}	
		}
	}
	
	if( (iir_source & (HSU_RX_OE_INT | HSU_BRK_INT | HSU_FE_INT)) != 0 )
	{
		// Error interrupt needs servicing.

		if( (iir_source & HSU_RX_OE_INT) != 0 )
		{
			SerDrvrVars_s[ puport ].stats.errors_overrun += 1;		
		}

		if( (iir_source & HSU_BRK_INT) != 0 )
		{
			SerDrvrVars_s[ puport ].stats.errors_break += 1;		
		}

		if( (iir_source & HSU_FE_INT) != 0 )
		{
			SerDrvrVars_s[ puport ].stats.errors_frame += 1;		
		}
	}
	
	// Clear the interrupt(s)
	puart_base->iir = iir_source;
	
	if( xTaskWoken == pdTRUE )
	{
		// This sets us up so that when we return from the interrupt we return to the highest priority task now ready.
		portYIELD_FROM_ISR();	
	}
}

/* ---------------------------------------------------------------- */
static void uart1_isr( void )
{
	highspeed_uart_INT_HANDLER( UPORT_TP, UART1 );
}

/* ---------------------------------------------------------------- */
static void uart3_isr( void )
{
	standard_uart_INT_HANDLER( UPORT_A, UART3 );
}

/* ---------------------------------------------------------------- */
static void uart6_isr( void )
{
	standard_uart_INT_HANDLER( UPORT_B, UART6 );
}

/* ---------------------------------------------------------------- */
static void uart5_isr( void )
{
	standard_uart_INT_HANDLER( UPORT_RRE, UART5 );
}

/* ---------------------------------------------------------- */
static void common_cts_INT_HANDLER( UNS_32 pport )
{
	// THIS IS AN INTERRUPT HANDLER. THUS BE AWARE OF WHAT CODE YOU US HEREIN.

	portBASE_TYPE		higher_priority_task_woken;
	
	SERPORT_EVENT_DEFS	levent;

	higher_priority_task_woken = pdFALSE;
	
	// ----------

	// 7/30/2013 rmd : This interrupt fires on the edge programmed which is based upon the okay
	// to transmit level of CTS defined for the device. If that level is LOW, then the interrupt
	// is programmed to fire on the rising edge - meaning it is transitioning away from okay to
	// transmit. So we should STOP transmitting. And the modem_control_status.i_cts should go
	// FALSE. And we should signal the task.

	// 7/30/2013 rmd : Note because the interrupt on can be programmed on ONE edge - not both
	// edges - I had to introduce a polling timer. To use to poll the ongoing state of CTS to
	// eventually recognize CTS is once again telling us it is okay to transmit.
	
	// SET the level. Recognize the fact this interrupt fired. Inhibit transmission for at least
	// one polling timer timeout. At each polling timer timeout we will actually sample the
	// signal level and decide what to do.
	SerDrvrVars_s[ pport ].modem_control_line_status.i_cts = (false);

	// ----------

	// Always force the main timer to restart on the first polling timer timeout. Which it will do if CTS is still FALSE.
	SerDrvrVars_s[ pport ].cts_main_timer_state &= (~CTS_TIMER_STATE_main_timer_running);

	// And ALWAYS start the polling timer.
	xTimerStartFromISR( SerDrvrVars_s[ pport ].cts_polling_timer, &higher_priority_task_woken );

	// ----------

	// There can be noise on the line. Occurs almost for sure when manully "sparking" CTS to test the driver. The result being
	// i_cts is actually TRUE or okay to send data by the time we go to read it. But no matter. Assume it is FALSE and issue the
	// CTS_CANNOT_SEND event.
	levent = UPEV_CTS_CANNOT_SEND;

	// 6/3/2015 rmd : Note that with a NOISY CTS line the following will fill the queue. And it
	// is possible then for the serial driver cts callback function to report the serial driver
	// queue is full when it wants to post its CTS okay again event.
	xQueueSendToBackFromISR( SerDrvrVars_s[ pport ].SerportDrvrEventQHandle, &levent, &higher_priority_task_woken );

	// ----------

	if( higher_priority_task_woken == pdTRUE )
	{
		// This sets us up so that when we return from the interrupt we return to the highest priority task now ready.
		portYIELD_FROM_ISR();	
	}
}

/* ---------------------------------------------------------- */
static void port_a_cts_INT_HANDLER( void )
{
	// THIS IS AN INTERRUPT HANDLER. THUS BE AWARE OF WHAT CODE YOU US HEREIN.

	common_cts_INT_HANDLER( UPORT_A );
}

/* ---------------------------------------------------------- */
static void port_b_cts_INT_HANDLER( void )
{
	// THIS IS AN INTERRUPT HANDLER. THUS BE AWARE OF WHAT CODE YOU US HEREIN.

	common_cts_INT_HANDLER( UPORT_B );
}

/* ---------------------------------------------------------- */
/*
static void port_rre_cts_INT_HANDLER( void )
{
	// THIS IS AN INTERRUPT HANDLER. THUS BE AWARE OF WHAT CODE YOU US HEREIN.

	__common_cts_INT_HANDLER( UPORT_RRE );
}
*/

/* ---------------------------------------------------------- */
static void cts_main_timer_callback( xTimerHandle pxTimer )
{
	// This is NOT an interrupt. But rather, executed within the context of the timer task.

	UNS_32	lport;
	
	lport = (UNS_32)pvTimerGetTimerID( pxTimer );
	
	// Having both timers running we need to be careful about who is running the show. Well its the polling timer. Can't have
	// both timers signaling the serdrv task else we open the door to complicated sequences of who expired first kind of thing.
	// Think of the polling timer as starting the main timer. And when the main timer expires this callback sets a flag for the
	// polling timer.
	SerDrvrVars_s[ lport ].cts_main_timer_state |= CTS_TIMER_STATE_main_timer_expired;  // Set indication for the polling timer callback function.
}

/* ---------------------------------------------------------- */
static void cts_poll_timer_callback( xTimerHandle pxTimer )
{
	// This is NOT an interrupt. But rather, executed within the context of the timer task.

	UNS_32	lport;
	
	lport = (UNS_32)pvTimerGetTimerID( pxTimer );
	
	if( lport == UPORT_A )
	{
		// 7/30/2013 rmd : Sample the GPI line level and set the i_cts variable.
		PORT_CTS_STATE( A );
	}
	else
	if( lport == UPORT_B )
	{
		// Sample the actual line level.
		PORT_CTS_STATE( B );
	}
	else
	if( lport == UPORT_RRE )
	{
		// 3/12/2014 rmd : Port RRE cts is not used. Just force it true.
		SerDrvrVars_s[ UPORT_RRE ].modem_control_line_status.i_cts = (true);
	}
	

	if( SerDrvrVars_s[ lport ].modem_control_line_status.i_cts == (true) )
	{
		// It went TRUE. So stop the main timer.
		xTimerStop( SerDrvrVars_s[ lport ].cts_main_timer, portMAX_DELAY );

		postSerportDrvrEvent( lport, UPEV_CTS_OKAY_TO_SEND_AGAIN );
	}
	else
	{
		// So CTS is (still) not good. We cannot send. And if that is the case we'll want to make
		// sure the main timer is running.
		if( (SerDrvrVars_s[ lport ].cts_main_timer_state & CTS_TIMER_STATE_main_timer_running) != CTS_TIMER_STATE_main_timer_running )
		{
			// Start the main timer.
			xTimerStart( SerDrvrVars_s[ lport ].cts_main_timer, portMAX_DELAY );

			SerDrvrVars_s[ lport ].cts_main_timer_state |= CTS_TIMER_STATE_main_timer_running;

			SerDrvrVars_s[ lport ].cts_main_timer_state &= (~CTS_TIMER_STATE_main_timer_expired);
		}
		
		if( (SerDrvrVars_s[ lport ].cts_main_timer_state & CTS_TIMER_STATE_main_timer_expired) == CTS_TIMER_STATE_main_timer_expired )
		{
			postSerportDrvrEvent( lport, UPEV_CTS_MAIN_TIMER_EXPIRED );
		}
		else
		{
			// Well then RE-START the polling timer again.
			if( xTimerStart( SerDrvrVars_s[ lport ].cts_polling_timer, 0 ) != pdPASS )
			{
				Alert_Message_va( "%s CTS: Polling Timer Queue Full!" , port_names[ lport ]);
			}
		}
	}
}

/* ---------------------------------------------------------- */
static void flow_control_timer_callback( xTimerHandle pxTimer )
{
	// This is NOT an interrupt. But rather, executed within the context of the timer task.

	UNS_32	lport;
	
	lport = (UNS_32)pvTimerGetTimerID( pxTimer );
	
	if( lport != UPORT_TP )
	{
		Alert_Message_va( "Only for TP port (not for %s)", port_names[ lport ] );
	}

	// 3/19/2013 rmd : Give the driver the go ahead.
	postSerportDrvrEvent( lport, UPEV_FLOW_CONTROL_TIMEOUT );
}

/* ---------------------------------------------------------- */
extern void set_reset_ACTIVE_to_serial_port_device( UPORTS port )
{
	// 6/19/2013 rmd : This function handles the reset output from Ports A & B. The RRE port
	// also has a reset output but setting it needs to be handled through a different function
	// (there is no config_c.port_settings for the RRE port).
	switch( port )
	{
		case UPORT_A:
			port_device_table[ config_c.port_A_device_index ].reset_active_level ? PORT_A_RESET_HIGH : PORT_A_RESET_LOW;

			SerDrvrVars_s[ port ].modem_control_line_status.o_reset = port_device_table[ config_c.port_A_device_index ].reset_active_level;

			break;
		
		case UPORT_B:
			port_device_table[ config_c.port_B_device_index ].reset_active_level ? PORT_B_RESET_HIGH : PORT_B_RESET_LOW;

			SerDrvrVars_s[ port ].modem_control_line_status.o_reset = port_device_table[ config_c.port_B_device_index ].reset_active_level;

			break;
	}
}

/* ---------------------------------------------------------- */
extern void set_reset_INACTIVE_to_serial_port_device( UPORTS port )
{
	// 6/19/2013 rmd : This function handles the reset output from Ports A & B. The RRE port
	// also has a reset output but setting it needs to be handled through a different function
	// (there is no config_c.port_settings for the RRE port).
	switch( port )
	{
		case UPORT_A:
			port_device_table[ config_c.port_A_device_index ].reset_active_level ? PORT_A_RESET_LOW : PORT_A_RESET_HIGH;

			SerDrvrVars_s[ port ].modem_control_line_status.o_reset = !port_device_table[ config_c.port_A_device_index ].reset_active_level;  // note we use logic inversion - not a bitwise inversion

			break;
		
		case UPORT_B:
			port_device_table[ config_c.port_B_device_index ].reset_active_level ? PORT_B_RESET_LOW : PORT_B_RESET_HIGH;

			SerDrvrVars_s[ port ].modem_control_line_status.o_reset = !port_device_table[ config_c.port_B_device_index ].reset_active_level;  // note we use logic inversion - not a bitwise inversion

			break;
	}
}

/* ---------------------------------------------------------- */
void SetRTS( UPORTS pport, UNS_32 pto_level )
{
	// Only UART 3 in the LPC3250 has the actual RTS modem control line. However we run the RTS
	// outputs as GPIO/GPO. Easier that way.
	/*
		LPC3250 RTS SUMMARY:

			UPORT A:	On GPO_23 as a GPO line. Ball M16.	 


			UPORT B:	On GPO_01 as a GPO line. Ball D4.


			RRE:		NO RTS LINE. This function takes no action.


			TP:			NO RTS LINE. This function takes no action.


			USB:		NO RTS LINE. This function takes no action.
	*/

	if( (pport == UPORT_A) || (pport == UPORT_B) )
	{
		// 6/19/2013 rmd : Record the state.
		SerDrvrVars_s[ pport ].modem_control_line_status.o_rts = pto_level;

		if( pport == UPORT_A )
		{
			// 6/19/2013 rmd : Direct GPIO manipulation.
			if( pto_level == HIGH )
			{
				PORT_A_RTS_HIGH;
			}
			else
			{
				PORT_A_RTS_LOW;
			}
		}
		else
		if( pport == UPORT_B )
		{
			// 6/19/2013 rmd : Direct GPO manipulation.
			if( pto_level == HIGH )
			{
				PORT_B_RTS_HIGH;
			}
			else
			{
				PORT_B_RTS_LOW;
			}
		}
	}
}

/* ---------------------------------------------------------- */
extern void SetDTR( UPORTS pport, UNS_32 pto_level )
{
	// Only UART 3 in the LPC3250 has the actual DTR modem control line. However we run the DTR
	// outputs as GPIO/GPO. Easier that way.
	/*
		LPC3250 DTR SUMMARY:

			UPORT A:	On GPO_20 as a GPO line. Ball B2.	 


			UPORT B:	On GPO_04 as a GPO line. Ball D8.


			RRE:		NO RTS LINE. This function takes no action.


			TP:			NO RTS LINE. This function takes no action.


			USB:		NO RTS LINE. This function takes no action.
	*/

	if( (pport == UPORT_A) || (pport == UPORT_B) )
	{
		// 6/19/2013 rmd : Record the state.
		SerDrvrVars_s[ pport ].modem_control_line_status.o_dtr = pto_level;
	
		if( pport == UPORT_A )
		{
			// 6/19/2013 rmd : Direct GPIO manipulation.
			if( pto_level == HIGH )
			{
				PORT_A_DTR_HIGH;
			}
			else
			{
				PORT_A_DTR_LOW;
			}
		}
		else
		if( pport == UPORT_B )
		{
			// 6/19/2013 rmd : Direct GPO manipulation.
			if( pto_level == HIGH )
			{
				PORT_B_DTR_HIGH;
			}
			else
			{
				PORT_B_DTR_LOW;
			}
		}
	}
}

/* ---------------------------------------------------------- */
static void port_a_carrier_detect_INT_HANDLER( void )
{
	// THIS IS AN INTERRUPT HANDLER. THUS BE AWARE OF WHAT CODE YOU US HEREIN.

	// 3/23/2017 rmd : Only used by UPORT_A. Sends an event to the controller initiated task.
	// Other ports should have their own handler to implement a different behavior.

	// 3/23/2017 rmd : NOTES
	//
	// 1. When set to edge triggered (positive EDGE) the response time (latency) is 8usec.
	//
	// 2. When set to level triggered (positive LEVEL) the response time (latency) is 30usec.
	//
	// 3. When set to level triggered (negative LEVEL) the response time (latency) is 8usec. Odd
	//    it is different than the positive level.
	//
	// 4. Processing time appears to be around 7usec for the entire ISR.
	//
	// 5. The stated usec times above are when running DEBUG code. RELEASE code numbers are a
	// good bit better. I observed more than 25% better. For example the ISR executes in 4 usec.
	// And negative level latency is about 6 usec. And positive going latency is about
	// 22usec.

	UNS_32	cd_level;
	
	volatile UNS_32	*ulAPR;
	
	CONTROLLER_INITIATED_TASK_QUEUE_STRUCT	citqs;
	
	portBASE_TYPE			xTaskWoken;

	// ----------
	
	// 3/23/2017 rmd : Capture the address for SIC2_APR. So we can look at the setting for
	// GPI_05. Which is our CD line on port A.
	ulAPR = (volatile UNS_32*)0x4001000C;

	// 3/23/2017 rmd : GPI_05 is bit 27 in the interrupt control register. If the bit is set
	// then we just tripped on the HIGH LEVEL and now we want to set it to trip on the LOW
	// LEVEL. And vice-a-versa.
	if( *ulAPR & _BIT(27) )
	{
		// 3/23/2017 rmd : Signal just went high. Capture and set to trip on low. Could read the
		// GPI_05 pin directly. But I think in the face of very high speed transitions things could
		// get screwy. Let's just count on the interrupt setting to 'assume' the current state of
		// the CD line. As bad as that sounds I think its better than reading a level that is
		// opposite to what we triggered on. It may work out either way but I feel better with this
		// way.
		*ulAPR &= ~_BIT(27);
		
		cd_level = HIGH;
	}
	else
	{
		*ulAPR |= _BIT(27);

		cd_level = LOW;
	}
	
	// ----------
	
	if( cd_level == port_device_table[ config_c.port_A_device_index ].cd_when_connected )
	{
		citqs.event = CI_EVENT_connected;
	}
	else
	{
		citqs.event = CI_EVENT_disconnected;
	}
	
	// ----------
	
	xTaskWoken = pdFALSE;
	
	xQueueSendToBackFromISR( CONTROLLER_INITIATED_task_queue, &citqs, &xTaskWoken );
	
	// ----------
	
	// 6/9/2015 rmd : It is not critical to process this event asap therefore not necessary to
	// use the portYIELD_FROM_ISR macro.
}

/* ---------------------------------------------------------- */
extern void SERIAL_set_baud_rate_on_A_or_B( UPORTS pport, UNS_32 pbaud_rate )
{
	// 4/15/2015 rmd : This function is executed within the context of the comm_mngr task. And
	// can ONLY be called subsequent to the startup initial UART full programming.
	//
	// 4/15/2015 rmd : This FUNCTION should ONLY be called for UPORT_A or UPORT_B. It was
	// created for use with DEVICE PROGRAMMING. Specifically the SR radio. The MENU mode always
	// communicates at 19200. However we want normal data mode to operate at 57600. So in order
	// to program the unit we need to change the UART baud rates. And then after programming
	// restore them.
	
	UART_CFG_T			uart_cfg;

	UART_CONTROL_T		uart_setup;

	PORT_DEVICE_SETTINGS_STRUCT const	*device_table;
	
	// 4/15/2015 rmd : Only ports A or B.
	if( (pport == UPORT_A) || (pport == UPORT_B) )
	{
		// 8/21/2014 rmd : Compile complains about uninitialized use without this line.
		device_table = NULL;

		if( pport == UPORT_A )		// UPORT_A is on UART 3 3333333333333333333333333333333
		{
			// 6/18/2013 rmd : Point to the device settings.
			device_table = &port_device_table[ config_c.port_A_device_index ];

			// set uart_cfg->uartnum & regptr for use in later functions
			uart_cfg.regptr = UART3;
			uart_cfg.uartnum = 0;  // for UART3: uartnum=0, UART4: uartnum=1, UART5: uartnum=2, UART6: uartnum=3
		}
		else
		if( pport == UPORT_B )		// UPORT_B is on UART 6 6666666666666666666666666666666
		{
			// 6/18/2013 rmd : Point to the device settings.
			device_table = &port_device_table[ config_c.port_B_device_index ];

			// set uart_cfg->uartnum & regptr for use in later functions
			uart_cfg.regptr = UART6;
			uart_cfg.uartnum = 3;  // for UART3: uartnum=0, UART4: uartnum=1, UART5: uartnum=2, UART6: uartnum=3
		}

		// ----------

		// 6/2/2015 rmd : Advisable to block all interrupts during the write to the UART
		// registers. Don't need a task switch suprisingly attempting a block load to the UART via
		// the serial driver task.
		portENTER_CRITICAL();
		
		// ----------
		
		// UART baud rate generator isn't used, so just set it to divider by 1
		uart_cfg.regptr->lcr |= UART_LCR_DIVLATCH_EN;
		uart_cfg.regptr->dll_fifo = 1;	// write to the DLL register
		uart_cfg.regptr->dlm_ier = 0;	// write to the DLM register
		uart_cfg.regptr->lcr &= ~UART_LCR_DIVLATCH_EN;

		// ----------

		// 4/15/2015 rmd : Set the requested baud rate.
		uart_setup.baud_rate = pbaud_rate;

		// ----------

		uart_setup.parity = UART_PAR_NONE;
		uart_setup.stopbits = 1;
		uart_setup.databits = 8;
		
		// SET THE baud rate, parity, stop bits, databits AND sets the clock mode to AUTOMATIC CLOCKING
		uart_setup_trans_mode( &uart_cfg, &uart_setup );

		// ----------

		uart_cfg.regptr->lcr &= ~UART_LCR_DIVLATCH_EN;  // make SURE we are pointed to the rbr and thr
		
		// ----------
		
		portEXIT_CRITICAL();
	}
	else
	{
		Alert_Message( "BAUD PROGRAMMING: port out of range." );
	}
}

/* ---------------------------------------------------------- */
static void standard_uart_init( UPORTS pport )
{
	UART_CFG_T			uart_cfg;

	UART_CONTROL_T		uart_setup;

	UNS_32				tmp_ul;

	PORT_DEVICE_SETTINGS_STRUCT const	*device_settings;
	
	ISR_TRIGGER_TYPE	trigger_type;
	
	// This FUNCTION should ONLY be called for UPORT_A, UPORT_B, and UPORT_RRE. They are the standard uarts.
	// This FUNCTION is only valid for the LPC3250 UART's 3 through 6.
	if( (pport == UPORT_A) || (pport == UPORT_B) || (pport == UPORT_RRE) )
	{
		// 8/21/2014 rmd : Compile complains about uninitialized use without this line.
		device_settings = NULL;

		if( pport == UPORT_A )		// UPORT_A is on UART 3 3333333333333333333333333333333
		{
			// 3/23/2017 rmd : NOTE - very early on in the startup in the gpio_setup function the
			// MODEM CONTROL lines were disabled. They are not connected to nor able to be controlled by
			// the uart3 registers.
			
			// ----------
			
			// 6/18/2013 rmd : Point to the device settings.
			device_settings = &port_device_table[ config_c.port_A_device_index ];

			// set uart_cfg->uartnum & regptr for use in later functions
			uart_cfg.regptr = UART3;

			uart_cfg.uartnum = 0;  // for UART3: uartnum=0, UART4: uartnum=1, UART5: uartnum=2, UART6: uartnum=3

			// ----------
			
			clkpwr_clk_en_dis( CLKPWR_UART3_CLK, 1 );

			// ----------
			
			// 7/30/2013 rmd : UART interrupt

			// 7/30/2013 rmd : For completeness sakes disable this interrupt. Have no proof needed to do
			// this. But could execute the re-configure of the serial port at a point other than
			// startup. And I simply believe it is always a good practice to disable the interrupt when
			// jiggering the interrupt controller settings.
			xDisable_ISR( IIR3_INT );

			// 7/29/2013 rmd : Assign the UART interrupt vector. This does not enable the interrupt.
			// Already been disabled within this function.
			xSetISR_Vector( IIR3_INT, INTERRUPT_PRIORITY_uart_3, ISR_TRIGGER_HIGH_LEVEL, uart3_isr, NULL );

			// ----------
			
			// 7/30/2013 rmd : CTS GPI interrupt

			// 7/30/2013 rmd : See note above about diabling before jiggering.
			xDisable_ISR( GPI_16_INT );

			// 7/30/2013 rmd : Because we can only 'see' one edge with the interrupt set for the edge
			// that transitions to the halt xmission state.
			if( device_settings->cts_when_OK_to_send == LOW )
			{
				trigger_type = ISR_TRIGGER_POSITIVE_EDGE;
			}
			else
			{
				trigger_type = ISR_TRIGGER_NEGATIVE_EDGE;
			}

			// 7/29/2013 rmd : Assign the CTS interrupt vector. This does not enable the interrupt.
			xSetISR_Vector( GPI_16_INT, INTERRUPT_PRIORITY_port_a_cts, trigger_type, port_a_cts_INT_HANDLER, NULL );

			// ----------
			
			// 6/8/2015 rmd : We test the CD GPIO pin at times as needed. In the case of an internet
			// connected device we also use the interrupt to signal the CI task to send alerts. Also, in
			// the PORT A case, we use the CD interrupt to notify the Controller Initiated task when the
			// unit has disconnected. So that we may log that event and reconnect. After all in the
			// CS3000 the internet device is to be connected at all times.
			
			// 6/8/2015 rmd : CD is tied to GPI_05 for Port A.
			xDisable_ISR( GPI_05_INT );

			// 2/3/2017 rmd : We want the interrupt to fire when the unit disconnects. So we can signal
			// the CI task to begin the re-connect sequence.
			//
			// 3/23/2017 rmd : With the introduction of LEVEL sensing for the interrupt the rational as
			// to which level should be intiially set is different. I would say in general ALWAYS set to
			// detect the HIGH level. Why? Because when the unit is powered off (as it is during startup
			// when this function is executed) the CD line is for sure LOW. So if we set to LOW LEVEL
			// the interrupt would fire right away and in most cases indicate the unit is connectted
			// even before the unit was powered up.
			//
			// 3/23/2017 rmd : SO LEAVE THE INITIAL SETTING CHOICE CODE COMMENTED OUT. ALWAYS SET TO
			// HIGH LEVEL.
			//if( device_settings->cd_when_connected == LOW )
			//{
				// 3/23/2017 rmd : DO NOT use the EDGE triggered trigger type. It mostly works. But for some
				// reason on the Freewave SR radios the positive edge transition would not work. Well I went
				// around and around for a day. And could not understand why. But did come up with another
				// scheme. Which as it turns out I like alot better as it allows me to see BOTH transitions.
				// This technique should one day be extrapoloated to the CTS infrastructure. Instead of the
				// polling timer I've implemented. Using the LEVEL and reprogramming the interrupt
				// controller in the ISR to the alternate LEVEL seems to work very well.
				trigger_type = ISR_TRIGGER_HIGH_LEVEL;
			//}
			//else
			//{
			//	trigger_type = ISR_TRIGGER_LOW_LEVEL;
			//}

			// 7/29/2013 rmd : Assign the CD interrupt vector. This does not enable the interrupt.
			xSetISR_Vector( GPI_05_INT, INTERRUPT_PRIORITY_port_a_cd, trigger_type, port_a_carrier_detect_INT_HANDLER, NULL );

			// 6/9/2015 rmd : And go ahead and enable the isr now.
			xEnable_ISR( GPI_05_INT );
		}
		else
		if( pport == UPORT_B )		// UPORT_B is on UART 6 6666666666666666666666666666666
		{
			// 6/18/2013 rmd : Point to the device settings.
			device_settings = &port_device_table[ config_c.port_B_device_index ];

			// set uart_cfg->uartnum & regptr for use in later functions
			uart_cfg.regptr = UART6;
			uart_cfg.uartnum = 3;  // for UART3: uartnum=0, UART4: uartnum=1, UART5: uartnum=2, UART6: uartnum=3

			clkpwr_clk_en_dis( CLKPWR_UART6_CLK, 1 );

			// ----------
			
			// 7/30/2013 rmd : UART interrupt

			// 7/30/2013 rmd : For completeness sakes disable this interrupt. Have no proof needed to do
			// this. But could execute the re-configure of the serial port at a point other than
			// startup. And I simply believe it is always a good practice to disable the interrupt when
			// jiggering the interrupt controller settings.
			xDisable_ISR( IIR6_INT );

			// 7/29/2013 rmd : Assign the UART interrupt vector. This does not enable the interrupt.
			// Already been disabled within this function.
			xSetISR_Vector( IIR6_INT, INTERRUPT_PRIORITY_uart_6, ISR_TRIGGER_HIGH_LEVEL, uart6_isr, NULL );

			// ----------
			
			// 7/30/2013 rmd : CTS GPI interrupt

			// 7/30/2013 rmd : See note above about diabling before jiggering.
			xDisable_ISR( GPI_00_INT );

			// 7/30/2013 rmd : Because we can only 'see' one edge with the interrupt set for the edge
			// that transitions to the halt xmission state.
			if( device_settings->cts_when_OK_to_send == LOW )
			{
				trigger_type = ISR_TRIGGER_POSITIVE_EDGE;
			}
			else
			{
				trigger_type = ISR_TRIGGER_NEGATIVE_EDGE;
			}

			// 7/29/2013 rmd : Assign the CTS interrupt vector. This does not enable the interrupt.
			xSetISR_Vector( GPI_00_INT, INTERRUPT_PRIORITY_port_b_cts, trigger_type, port_b_cts_INT_HANDLER, NULL );

			// ----------
			
			// 6/8/2015 rmd : We test the CD GPIO pin at times as needed. In the case of an internet
			// connected device we also use the interrupt to signal the CI task to send alerts. So that
			// the commserver socket can be identified as quickly as possible after a new connection has
			// been established.
			
			// 6/8/2015 rmd : CD is tied to GPI_02 for Port B.
			xDisable_ISR( GPI_02_INT );

			// 10/27/2016 rmd : Suppress the PORT B CD monitor. Not interested at this time.
			/*
			// 6/9/2015 rmd : Only if the port holds an internet connected device do we want this
			// interrupt firing because it posts without checking an event to the controller initiated
			// task triggering an outbound CI message (alerts).
			if( device_settings->internet_connected_device )
			{
				// 6/9/2015 rmd : We want the interrupt to fire when the signal transitions to the connected
				// state.
				if( device_settings->cd_when_connected == LOW )
				{
					trigger_type = ISR_TRIGGER_NEGATIVE_EDGE;
				}
				else
				{
					trigger_type = ISR_TRIGGER_POSITIVE_EDGE;
				}
	
				// 7/29/2013 rmd : Assign the CD interrupt vector. This does not enable the interrupt.
				//
				// 6/8/2015 rmd : Note for the Lantronix devices (the only internet connected manufacturer
				// we use at this time) the CD (which may be their DTR output wired to our CD input) flips
				// HI when connected in the 232 world. Which means LOW when cnnected at the GPIO pin. So
				// look for the falling edge.
				xSetISR_Vector( GPI_02_INT, INTERRUPT_PRIORITY_port_b_cd, trigger_type, common_cd_INT_HANDLER, NULL );

				// 6/9/2015 rmd : And go ahead and enable the isr now.
				xEnable_ISR( GPI_02_INT );
			}
			*/
		}
		else
		if( pport == UPORT_RRE )	// UPORT_RRE is on UART 5 55555555555555555555555555555
		{
			// 6/18/2013 rmd : Point to the device settings. For the case of RRE the port settings are
			// fixed. And therefore hardcoded.
			device_settings = NULL;  

			// set uart_cfg->uartnum for use in later functions
			uart_cfg.regptr = UART5;
			uart_cfg.uartnum = 2;  // for UART3: uartnum=0, UART4: uartnum=1, UART5: uartnum=2, UART6: uartnum=3

			clkpwr_clk_en_dis( CLKPWR_UART5_CLK, 1 );

			xDisable_ISR( IIR5_INT );
			// assign the isr vector - DOES NOT ENABLE THE INTERRUPT
			xSetISR_Vector( IIR5_INT, INTERRUPT_PRIORITY_uart_5, ISR_TRIGGER_HIGH_LEVEL, uart5_isr, NULL );
			
			// Set up the CTS interrupt handler for the RRE port.
			xDisable_ISR( GPI_22_INT );
			// assign the isr vector - DOES NOT ENABLE THE INTERRUPT  -- commented out cause not using
			//xSetISR_Vector( GPI_22_INT, INTERRUPT_PRIORITY_port_rre_cts, ISR_TRIGGER_POSITIVE_EDGE, port_rre_cts_INT_HANDLER, NULL );
		}

		// ----------

		// UART baud rate generator isn't used, so just set it to divider by 1
		uart_cfg.regptr->lcr |= UART_LCR_DIVLATCH_EN;
		uart_cfg.regptr->dll_fifo = 1;	// write to the DLL register
		uart_cfg.regptr->dlm_ier = 0;	// write to the DLM register
		uart_cfg.regptr->lcr &= ~UART_LCR_DIVLATCH_EN;

		// ----------

		// 6/19/2013 rmd : Default CS3000 main board port rate is 57600.
		uart_setup.baud_rate = 57600;

		if( (pport == UPORT_A) || (pport == UPORT_B) )
		{
			uart_setup.baud_rate = device_settings->baud_rate;
		}
		else if( pport == UPORT_RRE )
		{
			// 2/14/2014 ajv : Currently, the RRe port is only used for the
			// purpose of code download into a controller without a central
			// communication option. Therefore, let's push it to 115200 since
			// that's what we're using for the Kickstart and it seems to be
			// working well.
			uart_setup.baud_rate = 115200;
		}

		// ----------

		uart_setup.parity = UART_PAR_NONE;
		uart_setup.stopbits = 1;
		uart_setup.databits = 8;
		
		// SET THE baud rate, parity, stop bits, databits AND sets the clock mode to AUTOMATIC CLOCKING
		uart_setup_trans_mode( &uart_cfg, &uart_setup );

		// ----------

		uart_cfg.regptr->lcr &= ~UART_LCR_DIVLATCH_EN;  // make SURE we are pointed to the rbr and thr


		// BE CAREFUL of the iir_fcr register. It is listed as a WRITE ONLY. Reading this location actually will read the IIR.
		// set up fifo levels
		
		// 5/27/2015 rmd : Set the TXFIFO trigger level to 0. This works best for the interrupt and
		// kick off functions. See them for further discussion.
		uart_cfg.regptr->iir_fcr = ( UART_FCR_RXFIFO_TL32 | UART_FCR_TXFIFO_TL0 );
		// enable the fifos
		uart_cfg.regptr->iir_fcr = ( UART_FCR_RXFIFO_TL32 | UART_FCR_TXFIFO_TL0 | UART_FCR_FIFO_CTRL | UART_FCR_FIFO_EN );
		// flush the fifos
		uart_cfg.regptr->iir_fcr = ( UART_FCR_RXFIFO_TL32 | UART_FCR_TXFIFO_TL0 | UART_FCR_FIFO_CTRL | UART_FCR_FIFO_EN | UART_FCR_TXFIFO_FLUSH | UART_FCR_RXFIFO_FLUSH );
		

		// It is recommended to ALWAYS read the RBR after the FIFOs are flushed. (from manual)
		while( uart_cfg.regptr->lsr & UART_LSR_RDR )
		{
			tmp_ul = uart_cfg.regptr->dll_fifo;
		}


		// Enable the appropriate interrupts. WE ARE NOT USING THE MODEM CONTROL LINE STATUS INTERRUPTS. This came about primarily because of CTS. The is
		// CTS line is only available on PORT_A as an actual uart managed input. The other ports implement with GPIO. For consistancy sake within the
		// code it became easier and more sound to test and validate one method of handling CTS. Use GPIO and the main timer and polling timer
		// approach. So we discounted the use of CTS via the Modem Status Register. And the same discussion holds for the other control lines. The others
		// not nearly as hard as CTS to manage. They are all polled on an as need basis.
		uart_cfg.regptr->dlm_ier = ( UART_IER_RXLINE_STS | UART_IER_RDA | UART_IER_THRE );  // NOTE: no MODEM_STATUS interrupt
		
		// ----------

		if( (pport == UPORT_A) || (pport == UPORT_B) )
		{
			// 6/18/2013 rmd : Set the RTS and DTR to the levels dictated by the port_device_settings.
			// Has no effect on ports without these lines.
			SetRTS( pport, device_settings->rts_level_to_cause_device_to_send );
	
			SetDTR( pport, device_settings->dtr_level_to_connect );
		}
		
		// ----------

		// 7/31/2013 rmd : Read & store the initial modem control line states.
		if( pport == UPORT_A )
		{
			// 7/31/2013 rmd : Sample the actual line levels.
			PORT_CTS_STATE( A );
			
			PORT_CD_STATE( A );
			
			PORT_RI_STATE( A );
		}
		else
		if( pport == UPORT_B )
		{
			// 7/31/2013 rmd : Sample the actual line levels.
			PORT_CTS_STATE( B );
			
			PORT_CD_STATE( B );

			PORT_RI_STATE( B );
		}
		else
		if( pport == UPORT_RRE )
		{
			// 3/12/2014 rmd : The RRE port has been rendered to SERIN and SEROUT. No modem control
			// lines are implemented.

			// 3/12/2014 rmd : CTS is not active for the RRE port. Force true.
			SerDrvrVars_s[ UPORT_RRE ].modem_control_line_status.i_cts = (true);

			SerDrvrVars_s[ UPORT_RRE ].modem_control_line_status.i_cd = (true);

			SerDrvrVars_s[ UPORT_RRE ].modem_control_line_status.i_ri = (true);

			// 3/22/2012 rmd : We need to drive the config line to the RRE HIGH. Maybe this should be
			// done more formally.
			PORT_RRE_CONFIG_HIGH;
			
			// 3/22/2012 rmd : To the RRE if the SLEEP line is low it will NOT turn ON (i guess it's
			// sleeping - a mode we don't use). Must drive high.
			// 
			// 2/14/2014 ajv : Although this doesn't cause a problem when sending data out the RRe port,
			// I suspect this is no longer necessary since we're not powering the RRe board anymore.
			// Consider removing this. TODO
			//
			// 2/19/2014 rmd : AJ is probably right. In that sometime or another we may want to remove
			// all these references to the RRE signals and their needs.
			PORT_RRE_SLEEP_HIGH;
		}
		
		// ----------
		
		// 5/27/2015 rmd : Only A and B have a CTS implementation. Don't waste the memory resources
		// on RRE.
		if( (pport == UPORT_A) || (pport == UPORT_B) )
		{
			// 5/27/2015 rmd : Only ports A & B have an active CTS input to us. So only those two ports
			// use the main and polling CTS timers. For the main timer I am going to use a 4 second
			// overall timeout. This is choosen based upon experience watching CTS on a scope. If is
			// goes inactive (to stop us feeding the device) it usually on stays this way for less than
			// a second. During power on transitions we don't care if CTS times out particularly though
			// we don't want to see a nuisance alert.
			//
			// 2/1/2017 rmd : We have seen with the Lantronix GR radios trying to transfer large amounts
			// of station report data (300Kbytes+) that CTS goes inactive for long periods of time. I
			// tried a 15 second timeout and that did not work. I then moved it to 2 minutes and that
			// worked. I feel 2 minutes is excessive and that 1 minute will work so without testing that
			// is what I'm using: 1 minute.
			//
			// 3/13/2017 rmd : Well guess what. For the Lantronix GR units when there is some 400K of
			// data to send (station report data) Wissam reports we really do need more than 60 seconds.
			// Well I'm not going to cater this to the device. We'll just use 120 seconds across the
			// board. I don't think this hurts anything. Just results in a long wait during a real CTS
			// error that occurs.
			SerDrvrVars_s[ pport ].cts_main_timer = xTimerCreate( (const signed char*)CTS_main_timer_names[ pport ], MS_to_TICKS(120000), FALSE, (void*)pport, cts_main_timer_callback );
	
			// 5/27/2015 rmd : The polling timer period is important. From the time that CTS goes
			// inactive to the polling timeout which samples CTS we want to allow enough time that the
			// UART FIFO has emptied. Because if the polling timer callback samples CTS and sets it back
			// true again and then the UART TX interrupt fires and happens to drive the block to zero
			// after the serial driver tested to call the "kick off" function - we may get a 0 block
			// length or other effect resulting from both the kick off and the interrupt simultaneously
			// working on the same block. You'll see 50ms is easily enough time to transmit 32 bytes at
			// 9600 baud. Remember we only load 16 at a time into the UART FIFO and run at much higher
			// baud rates. IT REALLY IS IMPORTANT THAT THE POLLING TIMER PERIOD IS LONG ENOUGH FOR THE
			// TX FIFO TO COMPLETE THE TRANSMISSION OF THE 16 BYTES WE LOAD INTO IT. HAS TO DO WITH THE
			// TIMING BETWEEN CTS GOING FROM FALSE TO TRUE VERSUS THE SERIAL DRIVER TASK PROCESSING THE
			// EVENT THE CTS INTERRUPT GENERATED.
			SerDrvrVars_s[ pport ].cts_polling_timer = xTimerCreate( (const signed char*)CTS_poll_timer_names[ pport ], MS_to_TICKS(50), FALSE, (void*)pport, cts_poll_timer_callback );
	
			// ----------
			
			if( (SerDrvrVars_s[ pport ].cts_main_timer == NULL) || (SerDrvrVars_s[ pport ].cts_polling_timer == NULL) )
			{
				Alert_Message_va( "%s CTS Timer NOT CREATED!", port_names[ pport ] );
			}
		}

		// 5/27/2015 rmd : Can always reset the main timer state.
		SerDrvrVars_s[ pport ].cts_main_timer_state = 0x00;
		
		// ----------
		
		// Clear any pending interrupts - ITS A GOOD IDEA.
		tmp_ul = uart_cfg.regptr->iir_fcr;
		tmp_ul = uart_cfg.regptr->dll_fifo;
		tmp_ul = uart_cfg.regptr->lsr;
		tmp_ul = uart_cfg.regptr->modem_status;

		if( pport == UPORT_A )
		{
			// Eable the CTS interrupt for the A port.
			xEnable_ISR( GPI_16_INT );
		}
		else
		if( pport == UPORT_B )
		{
			// Eable the CTS interrupt for the B port.
			xEnable_ISR( GPI_00_INT );
		}
		else
		if( pport == UPORT_RRE )
		{
			// Eable the CTS interrupt for the RRE port. Commented OUT cause not using.
			//xEnable_ISR( GPI_22_INT );
		}

		// Enable the UART interrupt in the MIC for the port.
		xEnable_ISR( uinfo[ pport ].mic_uart_isr_number );
	}
	else
	{
		Alert_Message( "UART INIT: port out of range." );
	}
}

/* ---------------------------------------------------------- */
static void highspeed_uart_init( UPORTS pport )
{
	HSUART_CFG_T		hsuart_cfg;

	HSUART_CONTROL_T 	hsuart_setup;

	// This FUNCTION should ONLY be called for UPORT_TP. It is the high speed 14 clock uart. This FUNCTION is only valid for the
	// LPC3250 UART's 1, 2, or 7. We call it for UPORT_TP which is UART 1.
	if( pport == UPORT_TP )
	{
		// Make sure interrupts are OFF for the channel in the MIC, enable the CLK, assign the interrupt vector
		xDisable_ISR( IIR1_INT );

		// set uart_cfg->uartnum & regptr for use in later functions
		hsuart_cfg.regptr = UART1;
		hsuart_cfg.hsuartnum = 0;  // for UART1: uartnum=0

		// assign the isr vector - DOES NOT ENABLE THE INTERRUPT
		xSetISR_Vector( IIR1_INT, INTERRUPT_PRIORITY_uart_1, ISR_TRIGGER_HIGH_LEVEL, uart1_isr, NULL );

		hsuart_setup.baud_rate = 115200;
		hsuart_setup.rts_en = FALSE;
		hsuart_setup.cts_en = FALSE;

		// SET THE baud rate, parity, stop bits, databits AND sets the clock mode to AUTOMATIC CLOCKING
		hsuart_setup_trans_mode( &hsuart_cfg, &hsuart_setup );


		// Set the control register to configure the receiver timeout for 8 bytes inactivity, the HSU Offset at 20 clocks (the
		// default), enable the data error, receiver, and transmitter interrupts, set the recieve trigger level to 16 and the
		// transmit trigger level to 8.
		//
		// 5/27/2015 rmd : Set the TXFIFO trigger level to 0. This works best for the interrupt and
		// kick off functions. See them for further discussion.
		hsuart_cfg.regptr->ctrl = ( HSU_TMO_INACT_8B | HSU_OFFSET(0x14) | HSU_ERR_INT_EN | HSU_RX_INT_EN | HSU_TX_INT_EN | HSU_RX_TL16B | HSU_TX_TL0B );

		// Flush the receive FIFO.
		while( (hsuart_cfg.regptr->txrx_fifo & HSU_RX_EMPTY) == 0 );

		// ----------

		// 3/19/2013 rmd : CTS is always (true) for this port. There is no CTS hardware
		// implementation.
		SerDrvrVars_s[ UPORT_TP ].modem_control_line_status.i_cts = (true);
		SerDrvrVars_s[ UPORT_TP ].modem_control_line_status.i_cd = (true);
		SerDrvrVars_s[ UPORT_TP ].modem_control_line_status.i_ri = (true);

		// ----------

		SerDrvrVars_s[ pport ].cts_main_timer_state = 0x00;  // Reset the timers state - though this port does not use.

		// ----------

		// 3/19/2013 rmd : Create the flow control timer with a timeout of 2 seconds. Which is
		// plenty for the TP Micro to respond within regardless of M1 / M2 baud rates.
		SerDrvrVars_s[ pport ].flow_control_timer = xTimerCreate( (const signed char*)"TP_flow", MS_to_TICKS(2000), FALSE, (void*)pport, flow_control_timer_callback );

		if( SerDrvrVars_s[ pport ].flow_control_timer == NULL )
		{
			Alert_Message_va( "%s Flow Control Timer NOT CREATED!", port_names[ pport ] );
		}

		// ----------

		// Clear certain pending interrupts - ITS A GOOD IDEA.
		hsuart_cfg.regptr->iir = HSU_RX_OE_INT;
		hsuart_cfg.regptr->iir = HSU_BRK_INT;
		hsuart_cfg.regptr->iir = HSU_FE_INT;

		// Enable the UART interrupt in the MIC for the port.
		xEnable_ISR( IIR1_INT );
	}
	else
	{
		Alert_Message( "HSUART INIT: port out of range." );
	}
}

/* ---------------------------------------------------------- */
void initialize_uart_hardware( UPORTS pport )
{
	// 6/29/2015 rmd : Invoked within the context of the serial driver task. During serial
	// driver task startup.
	
	switch( pport )
	{
		case UPORT_TP:
			highspeed_uart_init( pport );
			break;

		case UPORT_A:
		case UPORT_B:
		case UPORT_RRE:
			// UPORT_A, UPORT_B, and UPORT_RRE are one of the STANDARD UARTS. (Uart's 3, 6, and 5 respectively.
			standard_uart_init( pport );
			break;

		default:
			Alert_Message( "Serial Driver Task for an UNK port!" );  // There is nothing to do for the USB port.
			break;
	}
}

/* ---------------------------------------------------------- */
extern void kick_off_a_block_by_feeding_the_uart( UPORTS pport )
{
	UART_REGS_T				*uart_ptr;

	HSUART_REGS_T			*hsuart_ptr;

	XMIT_CONTROL_LIST_ITEM	*list_item_ptr;

	if( (pport == UPORT_A) | (pport == UPORT_B) | (pport == UPORT_RRE) )
	{
		uart_ptr = (UART_REGS_T*)uinfo[ pport ].addr;

		// the now_xmitting pointer is ONLY changed elsewhere within the task...not at the interrupt level ... so therefore is safe to reference here
		list_item_ptr = xmit_cntrl[ pport ].now_xmitting;

		if( list_item_ptr != NULL )
		{
			// 5/26/2015 rmd : We are counting on the UART transmitter to be IDLE when this function is
			// called. For two reasons. First of all when we set the now_xmitting pointer to the new
			// block if the UART TRANSMITTER we active we would begin transmitting the new block.
			// Potentially even before we got to this function. And it therefore would be possible the
			// block length is now 0. If we didn't check the length and pushed in one byte and then
			// decremented the length it would ROLL-OVER to a giant number (64K).
			//
			// Secondly we count on the uart transmitter to be IDLE so we know we have room in the FIFO
			// for the byte we want to push in here. Since the ISR only pushes in 16 bytes and this
			// function 1 byte this is hardly if at all a concern.
			
			// 5/26/2015 rmd : Let's test for some error conditions. First that the transmitter is IDLE.
			//
			// 5/27/2015 rmd : As it turns out this is intimately intertwined with the CTS
			// sub-functionality. The CTS polling timer must be long enough to guarantee the UART has
			// emptied before CTS is being seen as TRUE again resulting in this function being called.
			// If not we stand the potential to corrupt the data transmitted as the interrupt moves our
			// from pointer and length count too.
			if( (uart_ptr->lsr & (UART_LSR_THRE | UART_LSR_TEMT)) != (UART_LSR_THRE | UART_LSR_TEMT) )
			{
				// 5/26/2015 rmd : Well it did so wait till it isn't idle.
				while( (uart_ptr->lsr & (UART_LSR_THRE | UART_LSR_TEMT)) != (UART_LSR_THRE | UART_LSR_TEMT) );
			}
			
			// ----------
			
			// 5/26/2015 rmd : Disable the xmit interrupt to protect our pointers and the packet
			// remaining length. Don't want the interrupt to occur until we get our pointers settled.
			// Remember it will occur immediately as the single byte we push into the UART is
			// transferred to the xmitter shift register.
			xDisable_ISR( uinfo[ pport ].mic_uart_isr_number );

			// 5/26/2015 rmd : This should never occur! And do not feed the UART if so.
			if( list_item_ptr->Length == 0 )
			{
				// 5/28/2015 rmd : Yes UART interrupts are disabled during this lengthy function call ...
				// but hey we've got a problem that should never occur anyway.
				Alert_Message_va( "%s UART: zero length block!", port_names[ pport ] );
			}
			else
			{
				// 5/26/2015 rmd : The xmit FIFO trip interrupt level is set at 0. Which means when the FIFO
				// count transitions from 1 to 0 we see an interrupt. The LPC3250 is normally plenty fast
				// enough to process the interrupt and feed the UART without seeing a gap in the data. I
				// suppose ocassionally a gao is possible depending upon other interrupts being processed or
				// if interrupts are disabled (more likely). But a small gap in the data (think about it as
				// a long stop bit) will be no problem. So being set to FIFO level 0 we only need to load
				// ONE byte here and that will indeed start the process.
				
				uart_ptr->dll_fifo = *(list_item_ptr->From);  // write one byte to the FIFO
	
				list_item_ptr->From++;
				
				list_item_ptr->Length--;
	
				SerDrvrVars_s[ pport ].stats.xmit_bytes++;  // update stats to reflect
			}

			xEnable_ISR( uinfo[ pport ].mic_uart_isr_number );
		}
	}
	else
	if( pport == UPORT_TP )
	{
		hsuart_ptr = (HSUART_REGS_T*)uinfo[ pport ].addr;

		// the now_xmitting pointer is ONLY changed elsewhere within the task...not at the interrupt level ... so therefore is safe to reference here
		list_item_ptr = xmit_cntrl[ pport ].now_xmitting;

		if( list_item_ptr != NULL )
		{
			// 6/3/2015 rmd : Remember there is no CTS on the UPORT_TP port. So this UART should always
			// be ready to go. Don't have to wait for TSR to be emptied like we do with the UPORT_A and
			// B device.
			
			// 5/26/2015 rmd : Disable the xmit interrupt to protect our pointers and the packet
			// remaining length. Don't want the interrupt to occur until we get our pointers settled.
			// Remember it will occur immediately as the single byte we push into the UART is
			// transferred to the xmitter shift register.
			xDisable_ISR( uinfo[ pport ].mic_uart_isr_number );

			// 5/26/2015 rmd : This should never occur! And do not feed the UART if so.
			if( list_item_ptr->Length == 0 )
			{
				// 5/28/2015 rmd : Yes UART interrupts are disabled during this lengthy function call ...
				// but hey we've got a problem that should never occur anyway.
				Alert_Message_va( "%s UART: zero length block!", port_names[ pport ] );
			}
			else
			{
				// 5/26/2015 rmd : The xmit FIFO trip interrupt level is set at 0. Which means when the FIFO
				// count transitions from 1 to 0 we see an interrupt. The LPC3250 is normally plenty fast
				// enough to process the interrupt and feed the UART without seeing a gap in the data. I
				// suppose ocassionally a gao is possible depending upon other interrupts being processed or
				// if interrupts are disabled (more likely). But a small gap in the data (think about it as
				// a long stop bit) will be no problem. So being set to FIFO level 0 we only need to load
				// ONE byte here and that will indeed start the process.
				
				hsuart_ptr->txrx_fifo = *(list_item_ptr->From);  // write one byte to the FIFO

				list_item_ptr->From++;
					
				list_item_ptr->Length--;

				SerDrvrVars_s[ pport ].stats.xmit_bytes++;  // update stats to reflect
			}

			xEnable_ISR( uinfo[ pport ].mic_uart_isr_number );
		}
	}
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Makes a copy of the block of data defined by the pdh and queues it up for
    transmission on the serial port driver. The TPL_OUT task uses the pom argument to MARK
    the packet as needing a status response timer to be started. The serial driver sends a
    message to the TPL_OUT task queue to start the timer. Set pom to NULL if not applicable
    such as when sending a packet to the tpmicro (which are not transport style messages).
    Or sending a packet to an internet connected device. Messages for internet connected
    devices (anything to the comm server) is assumed to consist of the
    INETRNET_CONNECTED_PACKET_HEADER and the payload. The serial driver will add the CRC and
    the pre and post ambles to form the true transmitted packet.

	@CALLER_MUTEX_REQUIREMENTS

    @MEMORY_RESPONSIBILITES No memory to release. The caller is responsible for his own
    message or packet memory. Note this functions name regarding making a copy of the data.

	@EXECUTED Typically called from the TPL_OUT and TPL_IN tasks. Also routinely called from 
    the RING_BUFFER_ANALYSIS task as incoming packets are retransmitted. Can also be called
    from the keypad task under develop conditions.
	
	@RETURN (true) if packet sucessfully added to driver. (false) otherwise

	@ORIGINAL 2012.03.12 rmd

	@REVISIONS
*/
extern BOOL_32 AddCopyOfBlockToXmitList( const UPORTS pport, DATA_HANDLE pdh, OUTGOING_MESSAGE_STRUCT *pom, const UNS_32 phandling_instructions, const UNS_32 psertport_drvr_posting_instructions )
{
	XMIT_CONTROL_LIST_ITEM	*newlistitem_ptr;
	
	BOOL_32		rv;
	
	ROUTING_CLASS_DETAILS_STRUCT	routing;
	
	// ----------
	
	rv = (false);
	
	// 9/9/2013 rmd : Check port is within range. Note how now we 'find' the port to send to for
	// the controller initiated type messages. This is a verification of a valid port.
	if( pport > UPORT_RRE )
	{
		Alert_Message_va( "Port out of range: %d", pport );
	}
	else
	if( pdh.dlen == 0 )
	{
		// 8/6/2013 rmd : DO NOT allow 0 byte blocks onto the list ... why do so ... and what bad
		// behaviors would this bring out?
		Alert_Message_va( "%s: trying to add a 0 byte block", port_names[ pport ] );
	}
	else
	{
		// 7/25/2013 rmd : If no memory alert and ignore request to add block to ser driver.
		if( mem_obtain_a_block_if_available( sizeof( XMIT_CONTROL_LIST_ITEM ), (void**)&newlistitem_ptr ) )
		{
			// 10/4/2013 rmd : The block sent by the caller is considered complete. For blocks sent by
			// the outgoing transport they contain pre & post ambles and CRC's. For controller initiated
			// blocks they also must be complete. Though they do not contain any pre or post ambles or
			// CRC's. They also do not have the standard transport header.
			
			// 7/25/2013 rmd : We must get a new block of memory for the actual message payload. This
			// function is to take a copy of the payload and transmit it. This supports sending the same
			// data out multiple ports.
			if( mem_obtain_a_block_if_available( pdh.dlen, (void**)&(newlistitem_ptr->From) ) )
			{
				newlistitem_ptr->OriginalPtr = newlistitem_ptr->From;

				newlistitem_ptr->Length = pdh.dlen;
				
				// 10/4/2013 rmd : Copy the block into place.
				memcpy( newlistitem_ptr->From, pdh.dptr, pdh.dlen );
				
				// ----------

				// 3/21/2017 rmd : It is okay for pom to be NULL. That means the packet did not originate
				// from tpl_out.
				newlistitem_ptr->pom = pom;
				
				// ----------

				// 3/14/2013 rmd : All ports have this set false by default. And only the UPORT_TP can end
				// up with this being set (true).
				newlistitem_ptr->flow_control_packet = (false);
				
				// ----------

				// 3/13/2013 rmd : If it is going to the TP board check if it is of a class that may be
				// distributed to the M ports.
				if( pport == UPORT_TP )
				{
					// 2/12/2014 rmd : Only if this data is a packet. With a header does this make any sense to
					// do. Remember the need for flow control is to prevent packets that need transmission down
					// the chain of M's from piling up in the TPMicro (115200 versus 19200).
					if( phandling_instructions == SERIAL_DRIVER_HANDLING_SENDING_PACKETIZED_DATA )
					{
						TPL_DATA_HEADER_TYPE	header;
						
						UNS_8	*ucp;
				
						ucp = pdh.dptr;
						
						ucp += sizeof(AMBLE_TYPE);
						
						memcpy( &header, ucp, sizeof(TPL_DATA_HEADER_TYPE) );
						
						get_this_packets_message_class( &header, &routing );

						if( (routing.routing_class_base == ROUTING_CLASS_IS_3000_BASED) && (routing.rclass != MSG_CLASS_CS3000_TO_TP_MICRO) )
						{
							// 3/13/2013 rmd : All packets that are not for the TP MICRO itself are classified as
							// needing flow control.
							newlistitem_ptr->flow_control_packet = (true);
						}
					}
				}
				
				// ----------
				
				// 7/24/2013 rmd : And make sure to assign the handling. For the case of internet connected
				// this instructs the serial port driver how to manage the connection.
				newlistitem_ptr->handling_instructions = phandling_instructions;
				
				// ----------

				// 3/26/2013 rmd : Add it to the end of the list. If list an error alert. Take list MUTEX
				// before adding to the list. The serial driver could be for example in the midst of
				// deleting a block just transmitted.
				xSemaphoreTake( xmit_list_MUTEX[ pport ], portMAX_DELAY );

				if( nm_ListInsertTail( &xmit_cntrl[ pport ].xlist, newlistitem_ptr ) != MIST_SUCCESS )
				{
					ALERT_MESSAGE_WITH_FILE_NAME( "List Insert Failed" );  
				}
	
				xSemaphoreGive( xmit_list_MUTEX[ pport ] );
				
				// ----------

				if( psertport_drvr_posting_instructions == SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL )
				{
					// 8/4/2014 ajv : Post event to the serial driver event
					// queue
					postSerportDrvrEvent( pport, UPEV_NEW_TX_BLK_AVAIL );
				}
				else
				{
					// 8/4/2014 ajv : Do not post an event because the caller
					// will handle the post. This is important when loading a
					// significant number of packets onto the xmit list because
					// we will overflow the serial port event queue. An example
					// where we do this is when sending controller-initiated
					// data packets.
				}
				
				rv = (true);
			}
			else
			{
				// 7/25/2013 rmd : Hey this block request was fullfilled and now we are abandoning the
				// request to add to the serial driver so have to explicitly free this here.
				mem_free( newlistitem_ptr );

				Alert_Message( "SERIAL: no memory to add data" );
			}
		}
		else
		{
			Alert_Message( "SERIAL: no memory to add list item" );
		}

	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern void strout( const UPORTS pport, const char *pString )
{
	DATA_HANDLE dh;
	
	dh.dptr = (UNS_8*)pString;

	dh.dlen = strlen(pString);
	
	// 7/1/2013 rmd : Takes a copy of the data to transmit and queues onto the serial driver for
	// xmission.
	AddCopyOfBlockToXmitList( pport, dh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );
}

/* ---------------------------------------------------------- */
void term_dat_out( UNS_8 *pstring )
{
	DATA_HANDLE dh;
	
	// Create and post some data to transmit
	dh.dptr = (unsigned char *) pstring;
	dh.dlen = strlen( (char*)pstring );
	
	AddCopyOfBlockToXmitList( UPORT_RRE, dh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );  // remember this is for the program loader
}

/* ---------------------------------------------------------- */
void term_dat_out_crlf( UNS_8 *pstring )
{
	char	str_64[ 64 ];

	snprintf( str_64, sizeof(str_64), "%s\r\n", pstring );
	term_dat_out( (UNS_8*)str_64 );
}

/* ---------------------------------------------------------- */
extern void SERIAL_add_daily_stats_to_monthly_battery_backed( UNS_32 pinternet_port )
{
	// 6/3/2015 rmd : This function is called from the TDCHECK task and fromthe POWERFAIL task.
	// It is called at a critical time during the power fail / restart process and should be
	// kept short and speed efficient. For example NO ALERT LINES here.
	weather_preserves.commserver_monthly_xmit_bytes += SerDrvrVars_s[ pinternet_port ].stats.xmit_bytes;

	weather_preserves.commserver_monthly_rcvd_bytes += SerDrvrVars_s[ pinternet_port ].stats.rcvd_bytes;
	
	weather_preserves.commserver_monthly_code_receipt_bytes += SerDrvrVars_s[ pinternet_port ].stats.code_receipt_bytes;

	weather_preserves.commserver_monthly_mobile_status_updates_bytes += SerDrvrVars_s[ pinternet_port ].stats.mobile_status_updates_bytes;
}

/* ---------------------------------------------------------- */
extern void SERIAL_at_midnight_manage_xmit_and_rcvd_accumulators_and_make_alert_lines( DATE_TIME_COMPLETE_STRUCT *pcdts_ptr )
{
	// 6/4/2015 rmd : This function is executed within the context of the TDCHECK task. It is
	// executed once per day at midnight.
	
	// ----------
	
	// 10/12/2017 rmd : Regardless of what is on UPORT_A manipulate the accumulators as it is
	// midnight. Then only stamp alerts if there is a GR on UPORT_A (which whould mean a link to
	// the commserver). These accumulators are for the purpose of measuring GR plan traffic.
	
	// ----------
	
	// 10/12/2017 rmd : Only for GR's show an alert line.
	if( (config_c.port_A_device_index == COMM_DEVICE_MULTITECH_LTE) || (config_c.port_A_device_index == COMM_DEVICE_GR_PREMIERWAVE) )
	{
		// 10/12/2017 rmd : It's the start of a new day. First show the raw daily numbers.
		Alert_Message_va( "Daily (bytes): xmit %u, rcvd %u, mobile %u", SerDrvrVars_s[ UPORT_A ].stats.xmit_bytes, SerDrvrVars_s[ UPORT_A ].stats.rcvd_bytes, SerDrvrVars_s[ UPORT_A ].stats.mobile_status_updates_bytes );
	}
	
	// ----------
	
	SERIAL_add_daily_stats_to_monthly_battery_backed( UPORT_A );
	
	// 6/4/2015 rmd : And since it is a new day reset the daily totals. Even though these
	// variables are added to during the UART ISR manipulating them here is safe to do because
	// these are ATOMIC operations (32-bit writes to SDRAM).
	SerDrvrVars_s[ UPORT_A ].stats.xmit_bytes = 0;

	SerDrvrVars_s[ UPORT_A ].stats.rcvd_bytes = 0;

	SerDrvrVars_s[ UPORT_A ].stats.code_receipt_bytes = 0;

	SerDrvrVars_s[ UPORT_A ].stats.mobile_status_updates_bytes = 0;
	
	// ----------
		
	// 10/12/2017 rmd : Now write the month to date alert lines, if it is a GR on port A.
	if( (config_c.port_A_device_index == COMM_DEVICE_MULTITECH_LTE) || (config_c.port_A_device_index == COMM_DEVICE_GR_PREMIERWAVE) )
	{
		// 10/12/2017 rmd : Be nice and change the text when the month completes!
		if( pcdts_ptr->__day == 1 )
		{
			Alert_Message_va( "PRIOR MONTH USE (bytes): xmit %u, rcvd %u, code %u, mobile %u", weather_preserves.commserver_monthly_xmit_bytes, weather_preserves.commserver_monthly_rcvd_bytes, weather_preserves.commserver_monthly_code_receipt_bytes, weather_preserves.commserver_monthly_mobile_status_updates_bytes );
		}
		else
		{
			Alert_Message_va( "Month to date (bytes): xmit %u, rcvd %u, code %u, mobile %u", weather_preserves.commserver_monthly_xmit_bytes, weather_preserves.commserver_monthly_rcvd_bytes, weather_preserves.commserver_monthly_code_receipt_bytes, weather_preserves.commserver_monthly_mobile_status_updates_bytes );
		}
	}
	
	// ----------
	
	// 10/12/2017 rmd : Now if the start of the new month clear the month accumulators.
	if( pcdts_ptr->__day == 1 )
	{
		weather_preserves.commserver_monthly_xmit_bytes = 0;
	
		weather_preserves.commserver_monthly_rcvd_bytes = 0;
	
		weather_preserves.commserver_monthly_code_receipt_bytes = 0;
	
		weather_preserves.commserver_monthly_mobile_status_updates_bytes = 0;
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

