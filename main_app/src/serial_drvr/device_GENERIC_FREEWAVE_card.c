/*  file = device_GENERIC_FREEWAVE_card.c                 rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"device_generic_freewave_card.h"

#include	"alerts.h"

#include	"configuration_controller.h"

#include	"gpio_setup.h"

#include	"lpc3250_chip.h"

#include	"lpc32xx_gpio.h"

#include	"serport_drvr.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/10/2014 rmd : A group of common functions to control the cmos level FREEWAVE card.
// Introduced to save code space. These function can be referenced by several devices. The
// FREEWAVE card is used to interface to most any 232 serially interfaced device. In
// retrospect, a better name for the card would have been 'CMOS card' or 'TTL card'.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void GENERIC_freewave_card_power_control( const UNS_32 pport, const BOOL_32 pon_or_off )
{
	// 3/10/2014 rmd : This function should be called directly from the appropriate device power
	// control function.
	
	// 6/28/2013 rmd : Power control for the GR card. And as such we need to drive a LOW to that
	// card to turn on the 12VDC supply to the device. And obviously a HIGH to the card to shut
	// down the supply. BUT BE AWARE - the hardware has an inversion in it so the logic is
	// reversed. A HIGH to turn ON, and a LOW to turn OFF.

	if( pport == UPORT_A )
	{
		// 3/23/2017 rmd : Regarding pon_or_off, (true) means to power ON, and (false) means to
		// power OFF.
		if( pon_or_off )
		{
			if( PORT_A_ENABLE_is_LOW )
			{
				// 3/23/2017 rmd : If enable is LOW the power is OFF.
				Alert_device_powered_on_or_off( pport, pon_or_off, (char *)comm_device_names[ config_c.port_A_device_index ] );

				// 3/23/2017 rmd : Power ON.
				PORT_A_ENABLE_HIGH;
			}
		}
		else
		{
			if( PORT_A_ENABLE_is_HIGH )
			{
				Alert_device_powered_on_or_off( pport, pon_or_off, (char *)comm_device_names[ config_c.port_A_device_index ] );
	
				PORT_A_ENABLE_LOW;
			}
		}
	}
	else
	if( pport == UPORT_B )
	{
		if( pon_or_off )
		{
			if( PORT_B_ENABLE_is_LOW )
			{
				Alert_device_powered_on_or_off( pport, pon_or_off, (char *)comm_device_names[ config_c.port_B_device_index ] );
	
				PORT_B_ENABLE_HIGH;
			}
		}
		else
		{
			if( PORT_B_ENABLE_is_HIGH )
			{
				Alert_device_powered_on_or_off( pport, pon_or_off, (char *)comm_device_names[ config_c.port_B_device_index ] );
				
				PORT_B_ENABLE_LOW;
			}
		}
	}
}
	
/* ---------------------------------------------------------- */
extern BOOL_32 GENERIC_freewave_card_is_connected( const UNS_32 pport )
{
	// 3/12/2014 rmd : For devices that use the CD output to indicate a data mode TCP/IP
	// connection we can utilize this function. So far that includes the Lantronix EN (UDS1100),
	// WEN (WBX2100), and the Sierra Wireless Raven X and LS300.

	// ----------

	if( pport == UPORT_A )
	{
		PORT_CD_STATE( A );
	}
	else
	{
		PORT_CD_STATE( B );
	}

	return( SerDrvrVars_s[ pport ].modem_control_line_status.i_cd );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

