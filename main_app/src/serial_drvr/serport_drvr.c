/*  file = serport_drvr.c                     02.09.2011  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"serport_drvr.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"configuration_controller.h"

#include	"controller_initiated.h"

#include	"gpio_setup.h"

#include	"lpc3250_chip.h"

#include	"lpc32xx_gpio.h"

#include	"cs_mem.h"

#include	"cent_comm.h"

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"flowsense.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

SERPORT_DRVR_TASK_VARS_s		SerDrvrVars_s[ UPORT_TOTAL_PHYSICAL_PORTS ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void postSerportDrvrEvent( UPORTS pport, SERPORT_EVENT_DEFS pevent )
{
	//	WARNING: DO NOT CALL THIS FUNCTION FROM AN INTERRUPT!
	// post the event using the queue handle for this serial port driver
	if ( xQueueSendToBack( SerDrvrVars_s[ pport ].SerportDrvrEventQHandle, &pevent, 0 ) != pdPASS )  // do not wait for queue space ... flag an error
	{
		Alert_Message( "SerPort queue overflow!" );
    }
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void serdrvr_disabled( int port, SERPORT_EVENT_DEFS event)
{
	// we NEVER disable our serial ports
	Alert_Message( "Serial_Drvr is DISABLED!" );
}

/* ---------------------------------------------------------- */
static void serdrvr_idle( int port, SERPORT_EVENT_DEFS event)
{	
	XMIT_CONTROL_LIST_ITEM		*llist_item;
	
	BOOL_32						cts_error;

	switch( event )
	{
		case UPEV_NEW_TX_BLK_AVAIL:
			// When CTS is already FALSE we restart the polling timer. It is when the polling tiemr
			// expires that we read the value of CTS to see if it went TRUE again. If we add blocks at a
			// faster rate than the polling timer duration it will never expire. And so even though CTS
			// went TRUE we won't see it until the serial driver fills up and we stop issueing the
			// NEW_TX_BLK_AVAIL message. To work around this phenomenon simply manually read the value
			// of CTS here. In addition to at the timer. It is okay that the timer callback will see it
			// too. It will just issue another TX BLK available message.
			//
			// Sample the actual line level.

			if( port == UPORT_A )
			{
				PORT_CTS_STATE( A );
			}
			else
			if( port == UPORT_B )
			{
				PORT_CTS_STATE( B );
			}
			else
			if( port == UPORT_RRE )
			{
				// 6/19/2013 rmd : From the 554 RRE device CTS is normally LOW. So test if it is.
				//
				// 2/19/2014 rmd : Update - we are not using the RRE port for an RRE. There is no longer a
				//  554 receiver present on the back of the controller. However we are using the RRE port
				//  for CodeDownloader and CommServer communications. We achieve this by cabling the RRE
				//  port to the GR Card. The GR card was designed for this. Note the GR does not connect the
				//  CTS pin 11 in the RRE cable to anything. But it is pulled low on the main card via a
				//  pull down resistor. So we get our TRUE cts state.
				//SerDrvrVars_s[ UPORT_RRE ].modem_control_line_status.i_cts = PORT_RRE_CTS_is_LOW;
				//
				// 3/12/2014 rmd : The heck with that. Don't read anything. Just FORCE it true. Why not? We
				// aren't using it!
				SerDrvrVars_s[ UPORT_RRE ].modem_control_line_status.i_cts = (true);
			}
			else
			if( port == UPORT_TP )
			{
				// 9/16/2014 rmd :  Remember there is no CTS line on UPORT_TP. UPORT_TP cts line should
				// NEVER be (false). It is set (true) at code start and should remain that way during
				// runtime.
				if( SerDrvrVars_s[ UPORT_TP ].modem_control_line_status.i_cts == (false) )
				{
					Alert_Message( "UPORT_TP ERROR: CTS went FALSE!" );

					SerDrvrVars_s[ UPORT_TP ].modem_control_line_status.i_cts = (true);  // correct it
				}
			}

			// ----------
			
			// 9/16/2014 rmd : Check that nothing is presently being sent. Should NOT be.
			if( xmit_cntrl[ port ].now_xmitting == NULL )
			{
				// 8/8/2013 rmd : Take MUTEX to protect list.
				xSemaphoreTake( xmit_list_MUTEX[ port ], portMAX_DELAY );
				
				// No, we are idle, so set up to transmit the block at the head of the list.
				// Get (but do not remove) the item at the head of the transmit list.
				llist_item = nm_ListGetFirst( &xmit_cntrl[ port ].xlist );

				xSemaphoreGive( xmit_list_MUTEX[ port ] );

				// ----------

				// 9/16/2014 rmd : NEW_TX_BLK_AVAIL events can show even though there is nothing on the list
				// to xmit. I believe this may happen as part of the TP MICRO flow control scheme. So make
				// sure there is one.
				if( llist_item != NULL )
				{
					// 7/26/2013 rmd : Regardless of the handling required, occupy the serial driver.
					xmit_cntrl[ port ].now_xmitting = llist_item;
	
					// 9/16/2014 rmd : Most messages do need to be concerned with the state of CTS. However some
					// do not. For example the DISCONNECT or DEVICE POWER CYCLING messages do not depend on the
					// state of CTS.
					cts_error = (false);
					
					// ----------

					if( SerDrvrVars_s[ port ].modem_control_line_status.i_cts )
					{
						BOOL_32	proceed;

						proceed = (false);
						
						// 3/20/2017 rmd : All controllers that initiate commserver messages send such messages out
						// port A. And the device on port A should be connected else we should drop and not send the
						// packet.
						//
						// 3/20/2017 rmd : It is debatable that this check should be in here at all. It may cause
						// problems with LR and need to be removed.
						if( CONFIG_this_controller_originates_commserver_messages() )
						{
							if( (port == UPORT_A) && (xmit_cntrl[ port ].now_xmitting->handling_instructions == SERIAL_DRIVER_HANDLING_SENDING_PACKETIZED_DATA) )
							{
								if( CONFIG_is_connected() )
								{
									proceed = (true);
								}
								else
								{
									Alert_Message( "Packet(s) dropped : NOT connected" );
								}
							}
							else
							{
								proceed = (true);
							}
						}
						else
						{
							proceed = (true);
						}

						// ----------
						
						if( proceed )
						{
							SerDrvrVars_s[ port ].SerportTaskState = UPORT_STATE_TX_IN_PROGRESS;
	
							kick_off_a_block_by_feeding_the_uart( port );
						}
						else
						{
							// 10/27/2016 rmd : If the connection is not present dump all packets. If this is a CI
							// message the CI response timer will time out.
							xSemaphoreTake( xmit_list_MUTEX[ port ], portMAX_DELAY );
				
							while( (llist_item = nm_ListRemoveHead( &xmit_cntrl[ port ].xlist)) )
							{
								mem_free( llist_item->OriginalPtr );
								mem_free( llist_item );
							}

							xSemaphoreGive( xmit_list_MUTEX[ port ] );
							
							// 3/21/2017 rmd : Not transmitting so NULL now_xmitting. Could this make a memory leak?
							xmit_cntrl[ port ].now_xmitting = NULL;
						}

					}
					else
					{
						cts_error = (true);
					}
					
					// ----------
					
					if( cts_error )
					{
						// Here's the scenario. CTS went FALSE for a long time. All the way to a CTS Timeout
						// occurred. Which stopped the polling timer. If CTS subsequently switches back TRUE no body
						// will know. Remember the FALSE to TRUE transition is polled. So we use the fact that the
						// driver is trying to send a block as a reason to restart the polling timer. The polling
						// timer starts the main timer after its first timeout. The polling timer checks CTS and if
						// it sees CTS TRUE it will post a UPEV_CTS_OKAY_TO_SEND_AGAIN message to this task. And
						// that will cause the packet to xmit.
						//
						// The other way we end up here is if CTS went FALSE while the driver (us) was idle. That
						// will not cause any overt action. But will generate ONE CTS TIMEOUT line if it stays FALSE
						// long enough.
						
						// 9/16/2014 rmd : Wipe the fact that we got started on a message. That way when the polling
						// timer senses CTS is true we can again try the block. So it had better not look the driver
						// is busy. It's NOT after all.
						xmit_cntrl[ port ].now_xmitting = NULL;

						// ----------
						
						// 5/19/2015 rmd : If the blocks are posted to the serial driver at a rate faster than
						// before the cts_main_timer expires then it never will because we keep flagging for it to
						// be restarted here. Additionally if the polling timer never expires that also would be a
						// problem because the entire CTS polling and timeout scheme depends on the polling timer to
						// expire. So do not flag the main timer to restart nor restart the polling timer if the
						// polling timer is already running.
						if( !xTimerIsTimerActive( SerDrvrVars_s[ port ].cts_polling_timer ) )
						{
							// 5/19/2015 rmd : If starting polling timer for the first time flag for the main timeout
							// timer to be started.
							SerDrvrVars_s[ port ].cts_main_timer_state &= (~CTS_TIMER_STATE_main_timer_running);
		
							xTimerStart( SerDrvrVars_s[ port ].cts_polling_timer, portMAX_DELAY );
						}

					}
					
				}  // of there was one on the list
			}
			else
			{
				// A block is already being transmitted?
				Alert_Message( "ERROR: State is IDLE yet TX in progress!" );
				
				// 6/2/2015 rmd : Brute force recovery. This could allow a memory leak. But not supposed to
				// happen.
				xmit_cntrl[ port ].now_xmitting = NULL;

				// 6/3/2015 rmd : We could change states to TX_IN_PROGRESS. Or stay in IDLE. Not sure which.
				// May not matter. Let's stay in IDLE so that we give a chance to start a new block.
				postSerportDrvrEvent( port, UPEV_NEW_TX_BLK_AVAIL );
			}
			
			break;
			 
		case UPEV_TX_BLK_SENT:
			Alert_Message_va( "%s: IDLE - tx blk sent event", port_names[ port ] );
			
			postSerportDrvrEvent( port, UPEV_NEW_TX_BLK_AVAIL );

			break;
	
		case UPEV_CTS_CANNOT_SEND:  				  
			// The interrupt fired. And CTS has been deemed FALSE. We are not doing anything but could
			// be between blocks. Meaning there could be another packet queued up waiting to go. Do not
			// need to change driver states but in a sense we are waiting for either CTS to allow us to
			// send that packet or the main timer to expire. So really nothing explicitly but wait.
			//snprintf( str_64, sizeof(str_64), "%s: (idle) CTS went FALSE", port_names[ port ] );
			//Alert_Message( str_64 );
			break;
			
		case UPEV_CTS_OKAY_TO_SEND_AGAIN:
			// 8/9/2013 rmd : The polling timer has seen that CTS is once again good. Check if there is
			// something to send. Even if there isn't it's okay to look.
			postSerportDrvrEvent( port, UPEV_NEW_TX_BLK_AVAIL );
			break;
			
		case UPEV_CTS_MAIN_TIMER_EXPIRED:

			xSemaphoreTake( xmit_list_MUTEX[ port ], portMAX_DELAY );

			// ----------
			
			// 4/3/2014 rmd : The main CTS timer expired. There are cases this is not an error. For
			// example a GR radio while attempting to get on the network may report this. An SR radio
			// may report this if it doesn't see the master. And that could happen if the master was
			// powered down. As an attempt to limit when we make an alert for this only alert if
			// something is on the list.
			if( xmit_cntrl[ port ].xlist.count )
			{
				Alert_cts_timeout( port, CTS_IDLE );
			}

			// ----------
			
			// 8/8/2013 rmd : With a CTS timeout FLUSH the xmit list.
			while( (llist_item = nm_ListRemoveHead( &xmit_cntrl[ port ].xlist)) )
			{
				mem_free( llist_item->OriginalPtr );
				mem_free( llist_item );
			}

			// ----------
			
			xSemaphoreGive( xmit_list_MUTEX[ port ] );
			
			break;

	}  // of switch

}

/* ---------------------------------------------------------- */
static void serdrvr_tx_blk_in_prog( int port, SERPORT_EVENT_DEFS event)
{

	XMIT_CONTROL_LIST_ITEM	*lremoved_item;

	BOOL_32		was_a_flow_control_packet;
	
	switch( event )
	{
		case UPEV_NEW_TX_BLK_AVAIL:	// We can ignore this event in this state
			break;
	
		case UPEV_TX_BLK_SENT:
			if( xmit_cntrl[ port ].now_xmitting == NULL )
			{
				Alert_Message_va( "%s: UNDERWAY - tx blk sent now_xmitting NULL", port_names[ port ] );
				
				// Go to the IDLE state and look for another block!
				SerDrvrVars_s[ port ].SerportTaskState = UPORT_STATE_IDLE;
	
				postSerportDrvrEvent( port, UPEV_NEW_TX_BLK_AVAIL );
			}
			else
			if( xmit_cntrl[ port ].now_xmitting->Length != 0 )
			{
				Alert_Message_va( "%s: UNDERWAY - tx blk sent length != 0", port_names[ port ] );
				
				// 6/2/2015 rmd : Brute force recovery. This could allow a memory leak. But not supposed to
				// happen.
				xmit_cntrl[ port ].now_xmitting = NULL;
	
				// Go to the IDLE state and look for another block!
				SerDrvrVars_s[ port ].SerportTaskState = UPORT_STATE_IDLE;
	
				postSerportDrvrEvent( port, UPEV_NEW_TX_BLK_AVAIL );
			}
			else
			{
				// For this packet were we asked to start the OM resp timer?
				if( xmit_cntrl[ port ].now_xmitting->pom != NULL )
				{
					TPL_OUT_queue_a_message_to_start_resp_timer( xmit_cntrl[ port ].now_xmitting->pom );
				}
				
				// ----------
				
				// 3/13/2013 rmd : Capture (as we are going to remove the list item and free the memory) if
				// this packet was marked to be part of the flow control scheme to the TP Micro.
				if( port == UPORT_TP )
				{
					was_a_flow_control_packet = xmit_cntrl[ port ].now_xmitting->flow_control_packet;
				}
				else
				{
					was_a_flow_control_packet = (false);
				}
				
				// 3/19/2013 rmd : Sanity check. Only packets out TP port should be involved with flow
				// control.
				if( xmit_cntrl[ port ].now_xmitting->flow_control_packet )
				{
					if( port != UPORT_TP )
					{
						Alert_Message( "Unexpectedly part of flow control."	);
					}
				}
				
				// ----------
				
				// 8/8/2013 rmd : Take MUTEX to protect list.
				xSemaphoreTake( xmit_list_MUTEX[ port ], portMAX_DELAY );
						
				// Remove this block from the transmit list.
				lremoved_item = nm_ListRemoveHead( &xmit_cntrl[ port ].xlist );
				
				xSemaphoreGive( xmit_list_MUTEX[ port ] );
				
				// ----------
				
				// do sanity check, then free the block
				if( lremoved_item != xmit_cntrl[ port ].now_xmitting )
				{
					// 7/31/2013 rmd : Hmmmm. Things are not as we expected.
					Alert_Message( "TX BLK IN PROG : not at head of list" );
				
					// 8/9/2013 rmd : Try to free this block too. I mean it just came from the list. What are we
					// going to do just leave it hanging in memory.
					if( lremoved_item != NULL )
					{
						mem_free( lremoved_item->OriginalPtr );
				
						mem_free( lremoved_item );
					}
				}
				
				// 8/9/2013 rmd : This is SUPPOSED to be the block that was on the list and should have
				// matched what we just removed from thelist. In any case should attempt its release.
				if( xmit_cntrl[ port ].now_xmitting != NULL )
				{
					mem_free( xmit_cntrl[ port ].now_xmitting->OriginalPtr );
				
					mem_free( xmit_cntrl[ port ].now_xmitting );
				}
				
				// Indicate no block being transmitted
				xmit_cntrl[ port ].now_xmitting = NULL;
				
				// ----------
				
				// 3/14/2013 rmd : Whether or not there are more packets to xmit on the list we MUST switch
				// to the FLOW_CONTROL_WAIT state if the packet we just finished with is part of the flow
				// control scheme. This way if another packet is added to the list we won't start
				// transmitting it until we receive the go ahead from the TP Micro. And also if any are
				// already on the list we also wait for the go ahead. This has the disadvantage of holding
				// back a non flow control packet if it is next on the list. But I think this is a necessary
				// evil in order to stop flow control packets that could be after the next packet (picture
				// alternating packet types on the list). The most a non flow control packet would be held
				// back would be the transmission time of a full 512 byte packet out the M ports. At 19200
				// baud this is around 300ms. But I think that's okay.
				if( was_a_flow_control_packet )
				{
					// 3/19/2013 rmd : Change driver state and start the timer. Waiting one second for the
					// TPMicro to give this driver the go ahead.
					SerDrvrVars_s[ port ].SerportTaskState = UPORT_STATE_FLOW_CONTROL_WAIT;
					
					xTimerStart( SerDrvrVars_s[ port ].flow_control_timer, portMAX_DELAY );
				}
				else
				{
					// 8/9/2013 rmd : Go look for another block in the IDLE state.
					SerDrvrVars_s[ port ].SerportTaskState = UPORT_STATE_IDLE;	
			
					postSerportDrvrEvent( port, UPEV_NEW_TX_BLK_AVAIL );
				}

			}  // of now_xmitting is not NULL and block length is zero

			break;
	
		case UPEV_CTS_CANNOT_SEND:
			// 6/2/2015 rmd : The CTS interrupt fired and posted this event. Change states so that
			// future events from the CTS polling timer or the CTS ISR itself (high frequency when
			// manually tapping CTS) are processed properly.
			SerDrvrVars_s[ port ].SerportTaskState = UPORT_STATE_TX_CTS_WAIT;
			break;

		case UPEV_CTS_OKAY_TO_SEND_AGAIN:
			//snprintf( str_64, sizeof(str_64), "%s: (xmit_in_prog) CTS went TRUE", port_names[ port ] );
			//Alert_Message( str_64 );
			break;

		case UPEV_CTS_MAIN_TIMER_EXPIRED:
			// Not sure how this can occur. I think it would be quite difficult to stage the sequence and timing to make it occur.
			Alert_cts_timeout( port, CTS_TRANSMITTING );
			break;
	}
}

/* ---------------------------------------------------------- */
static void serdrvr_cts_wait( int port, SERPORT_EVENT_DEFS event)
{
	XMIT_CONTROL_LIST_ITEM	*lremoved_item;

	switch (event)
	{
		case UPEV_NEW_TX_BLK_AVAIL:	// Nothing to do in this state. Can happen and is okay.
			break;
	
		case UPEV_TX_BLK_SENT:
			Alert_Message_va( "%s: CTS_WAIT - tx blk sent event", port_names[ port ] );
			
			// 6/2/2015 rmd : Brute force recovery. This could allow a memory leak. But not supposed to
			// happen.
			xmit_cntrl[ port ].now_xmitting = NULL;

			// Go to the IDLE state and look for another block!
			SerDrvrVars_s[ port ].SerportTaskState = UPORT_STATE_IDLE;

			postSerportDrvrEvent( port, UPEV_NEW_TX_BLK_AVAIL );

			break;
	
		case UPEV_CTS_CANNOT_SEND:
			//snprintf( str_64, sizeof(str_64), "%s: (waiting) CTS went FALSE", port_names[ port ] );
			//Alert_Message( str_64 );
			break;

		case UPEV_CTS_OKAY_TO_SEND_AGAIN:

			if( SerDrvrVars_s[ port ].modem_control_line_status.i_cts == (false) )
			{
				// 6/2/2015 rmd : I don't think this is an error as much as it is a curiosity. Certain CTS
				// waveform patterns will generate this. I played with a function generator and occasionally
				// this line would appear. But saw no mal-function in the driver from it.
				Alert_Message_va( "%s: CTS_WAIT - cts okay again but it is false", port_names[ port ] );
			}
			else
			{
				// 6/2/2015 rmd : Just being careful.
				if( xmit_cntrl[ port ].now_xmitting != NULL )
				{
					// 6/2/2015 rmd : This event could occur with the xmission_waiting_for_cts_to_resume block
					// showing a length of ZERO. And that is not an error. It depends on when CTS went false
					// versus the UART isr timing.
					if( xmit_cntrl[ port ].now_xmitting->Length == 0 )
					{
						// 6/2/2015 rmd : So CTS went false just before the final interrupt of the block. Go ahead
						// and switch back to block in progress and issue the end of block event.
						SerDrvrVars_s[ port ].SerportTaskState = UPORT_STATE_TX_IN_PROGRESS;
		
						postSerportDrvrEvent( port, UPEV_TX_BLK_SENT );
					}
					else
					{
						// 6/2/2015 rmd : Still have more to send.
						SerDrvrVars_s[ port ].SerportTaskState = UPORT_STATE_TX_IN_PROGRESS;
	
						// Start up the block again.
						kick_off_a_block_by_feeding_the_uart( port );
					}
				}
				else
				{
					// 6/2/2015 rmd : This should not happen. The only way to get to the cts_wait state was that
					// a block was in progress.
					Alert_Message_va( "%s: CTS_WAIT - cts okay again but no block", port_names[ port ] );
					
					// ----------
					
					// 6/2/2015 rmd : Brute force recovery. This could allow a memory leak. But not supposed to
					// happen.
					SerDrvrVars_s[ port ].SerportTaskState = UPORT_STATE_IDLE;
	
					postSerportDrvrEvent( port, UPEV_NEW_TX_BLK_AVAIL );
				}
			}
			break;

		case UPEV_CTS_MAIN_TIMER_EXPIRED:
			// This is the main event. Something is wrong with the attached device or our modem control line settings.
			Alert_cts_timeout( port, CTS_WAITING );

			if( xmit_cntrl[ port ].now_xmitting != NULL )
			{
				// 9/17/2014 ajv : Generate an alert which can be filtered to be displayed for the user.
				Alert_comm_command_failure( CI_EVENT_cts_error_make_an_alert );
				
				// 10/29/2016 rmd : And fix up AJ's screen variable.
				strlcpy( GuiVar_CommTestStatus, "Communication failed: CTS Timeout", sizeof(GuiVar_CommTestStatus) );
			}
			
			// ----------
			
			// 8/8/2013 rmd : Take MUTEX to protect list.
			xSemaphoreTake( xmit_list_MUTEX[ port ], portMAX_DELAY );
					
			// Flush/free any blocks on the transmit list.
			while( (lremoved_item = nm_ListRemoveHead( &xmit_cntrl[ port ].xlist)) )
			{
				mem_free( lremoved_item->OriginalPtr );
				mem_free( lremoved_item );
			}

			xSemaphoreGive( xmit_list_MUTEX[ port ] );

			// Note that we're not transmitting anything
			xmit_cntrl[ port ].now_xmitting = NULL;

			// ----------

			// Go to the IDLE state and look for another block!
			SerDrvrVars_s[ port ].SerportTaskState = UPORT_STATE_IDLE;

			postSerportDrvrEvent( port, UPEV_NEW_TX_BLK_AVAIL );

			break;

		default:
			break;
	}
}

/* ---------------------------------------------------------- */
static void serdrvr_flow_control_wait( int port, SERPORT_EVENT_DEFS event)
{
	// 3/14/2013 rmd : No matter what event comes in, only the TP port driver should be in this
	// state. If it is not the TP port switch back to IDLE and post a check to see if there is
	// data to send.
	if( port != UPORT_TP )
	{
		Alert_Message( "Port in unexpected state!" );

		SerDrvrVars_s[ port ].SerportTaskState = UPORT_STATE_IDLE;
	
		postSerportDrvrEvent( port, UPEV_NEW_TX_BLK_AVAIL );
	}
	else
	{
		// 3/14/2013 rmd : The only events we acknowledge in this state are the FLOW_CONTROL_GOAHEAD
		// event and the FLOW_CONTROL_TIMEOUT event. All others ignored.
		if( event == UPEV_FLOW_CONTROL_GOAHEAD )
		{
			// 9/10/2013 rmd : I noticed we do not stop the flow control timer here. Hmmmm, that would
			// seem to be what we should do. I suppose no harm as it just throws an event. And if there
			// is another block to xmit the timer is restarted. Still it seem it should be stopped. But
			// all tested and working for some time now so leave out.
			//
			// 12/2/2013 rmd : UPDATE - we are seeing some sporadic "UPORT_TP: flow control timeout"
			// alert messages in the engineering alerts. Meaning the timer has timed out. In one case,
			// perhaps related, it was accompanied by a "FLOWSENSE communication retry". Since without
			// this line we never stop the timer, if we do not have another token or token response type
			// packet (central traffic too) to send to the TP Micro within 2 seconds - we WILL see the
			// alert line for no good reason. This should stop those from occurring. I don't think I
			// believe that is behind our original sporadic timeout alerts. But may be.
			xTimerStop( SerDrvrVars_s[ port ].flow_control_timer, portMAX_DELAY );

			// 3/19/2013 rmd : We received the go ahead from the TP Micro. Change state to IDLE and post
			// a new block event to cause the driver to start another transmission if there is one.
			SerDrvrVars_s[ port ].SerportTaskState = UPORT_STATE_IDLE;
		
			postSerportDrvrEvent( port, UPEV_NEW_TX_BLK_AVAIL );
		}
		else
		if( event == UPEV_FLOW_CONTROL_TIMEOUT )
		{
			// 3/19/2013 rmd : Well we timed out. Make an alert for the engineering pile. The free the
			// driver just as if the tp micro had given us the go ahead.
			Alert_Message( "UPORT_TP: flow control timeout" );
			
			SerDrvrVars_s[ port ].SerportTaskState = UPORT_STATE_IDLE;
		
			postSerportDrvrEvent( port, UPEV_NEW_TX_BLK_AVAIL );
		}
		
	}

}

/* ---------------------------------------------------------- */
void serial_driver_task( void *pvParameters )
{
	xQueueHandle		EvtQHandle;
	
	UNS_32				port, task_index;
	
	SERPORT_EVENT_DEFS	event;
	
	// 10/23/2012 rmd : Passed parameter is the address to the Task_Table entry used to create
	// this task. So we can figure out it's index to use when keeping the watchdog activity
	// monitor moving. And extract the port this task is responsible for.
	TASK_ENTRY_STRUCT	*tt_ptr;
	
	tt_ptr = pvParameters;
	
	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// 10/23/2012 rmd : And in this case the port we are responsible for is in the table.
	port = (UNS_32)tt_ptr->parameter;
	
	if( port > UPORT_RRE )
	{
		Alert_Message( "PORT out of range!" );
		for( ;; );
	}
	
	// This driver is responsible for the transmission of data out the serial port. The
	// reception of data is managed by the ring_buffer_analysis task. These two task are tightly
	// coupled. As we are intializing parts of a shared structure here. And certainly the
	// hardware is shared. Ans the uart ISR is shared. So be careful. For example only
	// initialize the variables after great thgought and making sure we are not overwriting
	// settings that are made by the ring buffer task. For example do not mess with the ring
	// buffer variables here.

	// A NOTE. In this application we only call this function once. At the startup of the task.
	// We never re-initialize our serial ports. If you did though and the xmit list was
	// populated the following ListInit would lead to a memory leak. One should first clean the
	// list, removing and releasing the appropriate memory allocations. Then call this function
	// if you wish.
	xSemaphoreTake( xmit_list_MUTEX[ port ], portMAX_DELAY );

	nm_ListInit( &(xmit_cntrl[ port ].xlist), offsetof( XMIT_CONTROL_LIST_ITEM, dl ) );
	
	xSemaphoreGive( xmit_list_MUTEX[ port ] );

	// ----------

	// Be explicit. We are not transmitting anything!
	xmit_cntrl[ port ].now_xmitting = NULL;

	// ----------

	initialize_uart_hardware( port );
	
	// ----------

	// Go directly to IDLE. The port is initialized and waiting for action.
	SerDrvrVars_s[ port ].SerportTaskState = UPORT_STATE_IDLE;

	// Get handle to our port's Event queue
	EvtQHandle = SerDrvrVars_s[ port ].SerportDrvrEventQHandle;
	
	while( EvtQHandle )  // Must be non-zero to be valid
	{
		// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
		if( xQueueReceive( EvtQHandle, &event, MS_to_TICKS( 500 ) ) )
		{
			switch( SerDrvrVars_s[ port ].SerportTaskState )
			{
				case UPORT_STATE_DISABLED:
					serdrvr_disabled( port, event );
					break;
					
				case UPORT_STATE_IDLE:
					serdrvr_idle( port, event );				
					break;
					
				case UPORT_STATE_TX_IN_PROGRESS:
					serdrvr_tx_blk_in_prog( port, event );				
					break;
					
				case UPORT_STATE_TX_CTS_WAIT:
					serdrvr_cts_wait( port, event );				
					break;
					
				case UPORT_STATE_FLOW_CONTROL_WAIT:
					serdrvr_flow_control_wait( port, event );				
					break;
					
				default:
					break;
			}

		} 

		// ----------
		
		// 10/23/2012 rmd : Keep the activity indication going.
		task_last_execution_stamp[ task_index ] = xTaskGetTickCount();

		// ----------
		
	}

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

