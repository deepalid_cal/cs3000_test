/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_DEVICE_LR_RAVEON_H
#define INC_DEVICE_LR_RAVEON_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"lpc_types.h"

#include	"controller_initiated.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define LR_RAVEON_READ								( 0x52 )

#define LR_RAVEON_WRITE								( 0x57 )

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/7/2017 rmd : The 3 defines for the different RAVEON LR radios. This value is assigned
// to the variable GuiVar_LRRadioType, and used by the gui to determine how to write the
// screen. These values are also assigned to 2 bits in the PURCHASED_OPTIONS structure that
// is part of the configuration_controller structure. It is that purchased option variable
// that is used by the code distribution process to see if there is an M7 radio present,
// which the code distribution process requires.

#define RAVEON_RADIO_ID_M7		(1) 

#define RAVEON_RADIO_ID_M5		(2)

#define RAVEON_RADIO_ID_SONIK	(3)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void LR_RAVEON_initialize_state_struct(void);

extern void LR_RAVEON_initialize_device_exchange(void);

extern void LR_RAVEON_exchange_processing();

extern void LR_RAVEON_PROGRAMMING_extract_changes_from_guivars(void); 

extern void LR_RAVEON_PROGRAMMING_copy_settings_into_guivars(void); 


// ----------

extern void RAVEON_id_start_the_process( void );

extern void RAVEON_id_event_processing( CONTROLLER_INITIATED_TASK_QUEUE_STRUCT *pcitqs );

// ----------

extern BOOL_32 RAVEON_is_connected( UNS_32 pport );

extern void RAVEON_initialize_the_connection_process( UNS_32 pport );

extern void RAVEON_connection_processing( UNS_32 pevent );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

