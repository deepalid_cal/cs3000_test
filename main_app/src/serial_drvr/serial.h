/*  file = serial.h                           02.09.2010  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_SERIAL_SERIAL_H
#define INC_SERIAL_SERIAL_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"lpc_types.h"

#include	"cal_list.h"

#include	"gpio_setup.h"

#include	"tpl_out.h"



																							
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define RING_BUFFER_SIZE 0x1000


// This limit is checked when adding TX data to a serial port. When working with the low level USB loopback test the
// incoming block (say 8K) is broken into the 64 byte chunks due to the end point definition. Now this loop back mode
// is not normally used but when it is the amount you can send will be limited by this number of 64 byte blocks.
#define MAX_QUEUED_BLOCKS	160

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// A value of 1 means they are in their normally desired level. So these DO NOT reflect the actual signal level. But rather
// their logical level relative to the desired level.
typedef struct
{
	// 7/31/2013 rmd : CTS as a true / false. Interpreted as true to transmit, false to wait.
	// Port A and B fully implemented with interrupt and polling timers. 'i_cts' reflects the
	// current value. The value for the RRE port is always (true).
	UNS_8	i_cts;			// Clear-to-send (input)

	UNS_8	not_used_i_dsr;		// Data set ready (input) NOT IMPLEMENTED on any port in this design.

	// 7/31/2013 rmd : If you want to know the value use the macro PORT_A_RI_level.
	UNS_8	i_ri;			// Ring indicator (input)

	// 7/30/2013 rmd : Is set true when connected. But not necessarily (false) when
	// disconnected. To be sure must always read the value using the macro.
	UNS_8	i_cd;			// Carrier Detect (input)

	// ----------
	
	UNS_8	o_rts;			// RTS (output)

	UNS_8	o_dtr;			// DTR (output)

	UNS_8	o_reset;		// Reset (output)
	
} UART_CTL_LINE_STATE_s;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define PORT_CTS_STATE( port ) SerDrvrVars_s[ UPORT_##port ].modem_control_line_status.i_cts = port_device_table[ config_c.port_##port##_device_index ].cts_when_OK_to_send ? PORT_##port##_CTS_is_HIGH : PORT_##port##_CTS_is_LOW

#define PORT_CD_STATE( port ) SerDrvrVars_s[ UPORT_##port ].modem_control_line_status.i_cd = port_device_table[ config_c.port_##port##_device_index ].cd_when_connected ? PORT_##port##_CD_is_HIGH : PORT_##port##_CD_is_LOW

#define PORT_RI_STATE( port ) SerDrvrVars_s[ UPORT_##port ].modem_control_line_status.i_ri = port_device_table[ config_c.port_##port##_device_index ].ri_polarity ? PORT_##port##_RI_is_HIGH : PORT_##port##_RI_is_LOW

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// ring_index is where we start examining for 500 series packets from
	unsigned short	packet_index;
	
	// 500 series packet hunting support
	unsigned short	ringhead1;
	unsigned short	ringhead2;
	unsigned short	ringhead3;
	unsigned short	ringhead4;
	
	unsigned short	datastart;
	
	unsigned short	packetlength;
	
} PACKET_HUNT_S;

/* ---------------------------------------------------------- */
typedef struct
{
	// data_index is where we start collecting the data to xfer from
	unsigned short	data_index;

	BOOL_32			transfer_from_this_port_to_TP;
	BOOL_32			transfer_from_this_port_to_A;
	BOOL_32			transfer_from_this_port_to_B;
	BOOL_32			transfer_from_this_port_to_RRE;
	BOOL_32			transfer_from_this_port_to_USB;

} DATA_HUNT_S;

/* ---------------------------------------------------------- */
typedef struct
{
	// FOR THE SPECIFIED STRING HUNT

	// 2/19/2014 rmd : String_index is where we start examining for the string. In the present
	// implementation we only search for strings that start at the begginning of the buffer. So
	// this variable is not used.
	UNS_32	string_index;

	char	*str_to_find;

	// 2/12/2014 rmd : If we didn't load this when setting the str_to_find we would have to
	// calculate the length every time the rcvd_data hunt task ran.
	UNS_32	chars_to_match;

	// 10/30/2016 rmd : This allows the string we're looking for to be preceeding by a few
	// characters. This is designed to be a small value. Normally less than 10. This is to the
	// START of the string we're trying to find.
	UNS_32	depth_into_the_buffer_to_look;

	// ----------

	// FOR THE CRLF DELIMITED STRING HUNT 
	//enables searching or not searching for the first CRLF in the function:  search 
	//check_ring_buffer_for_a_crlf_delimited_string
	BOOL_32 	find_initial_CRLF; 

} STRING_HUNT_S;

typedef struct
{
	// FOR THE SPECIFIED TERMINATION STRING HUNT

	// 3/10/2015 mpd : This is where to continue searching when the subsequent stream of characters arrives in the
	// buffer. 
	UNS_32	string_index;

	// 3/13/2015 mpd : This is the termination string to search for. Finding it indicates the end of the receive data.
	char	*str_to_find;

	// 2/12/2014 rmd : If we didn't load this when setting the str_to_find we would have to
	// calculate the length every time the rcvd_data hunt task ran.
	UNS_32	chars_to_match;

} TERMINATION_HUNT_S;

/* ---------------------------------------------------------- */
//
// UART_RING_BUFFER_s - UART Ring Buffer data structure
//                      One instance for each UART port
typedef struct
{
	UNS_8				ring[ RING_BUFFER_SIZE ];	// the actual ring buffer

	// 2/12/2014 rmd : Where the next incoming byte from the UART is placed into the ring
	// buffer.
	volatile UNS_32		next;						

	// ----------

	BOOL_32				hunt_for_packets;

	BOOL_32				hunt_for_data;

	// 3/13/2014 rmd : Search for a specified string. And signal the comm_mngr when found. So
	// far only used in tpmicro ISP mode. If need to use for another process (say programming a
	// radio) will have to make the queue event message returned programmable. Meaning be able
	// to specify which event is queued in response to finding the string.
	BOOL_32				hunt_for_specified_string;

	
	BOOL_32				hunt_for_crlf_delimited_string;

	// 3/10/2015 mpd : Added hunt type for SR radio menus.
	BOOL_32				hunt_for_specified_termination;

	// 10/26/2016 rmd : Used when string hunting. We do under control of two tasks. When the
	// hunt mode is setup we also specify which task to signal when we find the string.
	UNS_32				task_to_signal_when_string_found;
	
	// ----------

	// variables used during the transport PACKET hunt
	PACKET_HUNT_S		ph;

	// variables used during port to port data transfer (used only during test)
	DATA_HUNT_S			dh;

	// variables used during string hunting during say menu driven radio programming or AT commands to some device
	STRING_HUNT_S		sh;

	// 3/10/2015 mpd : Added for SR radio menus. Variables used during termination hunting during menu driven radio
	// programming.
	TERMINATION_HUNT_S	th;

	// ----------

	volatile BOOL_32	ph_tail_caught_index;		// set TRUE when this happens - (set within an interrupt - except for the USB channel)

	volatile BOOL_32	dh_tail_caught_index;		// set TRUE when this happens - (set within an interrupt - except for the USB channel)

	volatile BOOL_32	sh_tail_caught_index;		// set TRUE when this happens - (set within an interrupt - except for the USB channel)

	volatile BOOL_32	th_tail_caught_index;		// set TRUE when this happens - (set within an interrupt - except for the USB channel)

} UART_RING_BUFFER_s;

/* ---------------------------------------------------------- */

typedef struct
{
	UNS_32	errors_overrun;
	
	UNS_32	errors_parity;
	
	UNS_32	errors_frame;

	UNS_32	errors_break;
	
	UNS_32	errors_fifo;

	// ----------
	
	// 6/3/2015 rmd : Non-battery back accumulators that are added to monthly accumulators which
	// are indeed stored in battery backed. We add to the monthly accumulators before any
	// shutdown of the controller (due to power failure or intentional code receipt restart).
	// There is a copy of these for each serial driver task - one per UPORT. Though only the one
	// tied to the A port of the A controller ever makes it into alert lines that are sent to
	// CCO.
	//
	// 8/19/2016 rmd : These are zeroed by C startup code. And again at the start of the ring
	// buffer analysis task. In other words when the code restarts they are all zero. When there
	// is a requested restart though we quickly add these values to the monthly in an attempt
	// not to lose count so far in a day.
	UNS_32	rcvd_bytes;

	UNS_32	xmit_bytes;

	UNS_32	code_receipt_bytes;

	UNS_32	mobile_status_updates_bytes;
	
} UART_STATS_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/13/2014 rmd : This handling instruction is the normally used choice serial port data
// handling instruction. Most data is sent using this. When we need special handling (during
// text exchanges for example) then we use the other handling instructions.
#define	SERIAL_DRIVER_HANDLING_SENDING_PACKETIZED_DATA			(0)

// 8/13/2014 rmd : Used to signify the data being sent is not a packet (per our packet
// definition). This handling instruction is necessary so the serial driver class does not
// attempt to look at the data as if it were a packet to determine if the 'data' requires
// the main-to-tp_micro flow control scheme. We use this handling instruction when
// programming the TPMicro in ISP mode. Or to send text strings to a communication device.
#define	SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA		(2)

// ----------

// 8/11/2014 rmd : Two defines used to manage posting a NEW_TX_BLK_AVAIL event to the serial
// driver queue for each packet of data added to the serial driver transmission list. To be
// used when a task is going to add many packets of data for transmission - for example when
// a message is split up into a bunch of 2K packets like we do when sending to the comm
// server. If we posted the NEW_TX_AVAIL again and again the serial driver task queue would
// have to be very deep to handle all the events queued (when the task generating the
// message is at a higher priority than the serial driver).
#define	SERIAL_DRIVER_DO_NOT_POST_NEW_TX_BLK_AVAIL		(0)

// 8/11/2014 rmd : Used normally when adding a block of data for transmission to the serial
// driver.
#define	SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL				(1)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	MIST_DLINK_TYPE				dl;				// for list control

	unsigned char				*From;

	unsigned char				*OriginalPtr;	// used when releasing the memory

	unsigned short				Length;

	// Used to allow the serial driver to signal the TPL_OUT about a packet that just
	// transmitted with it's request for status bit set. This was introduced so that the OM
	// status response timer could be accurately started. When the packet actually transmitted.
	// Not when the transport handed the packet tot he serial driver. There can be quite a bit
	// of difference in time when handling long messages. Especially with certain radios.
	OUTGOING_MESSAGE_STRUCT		*pom;
	
	// 3/13/2013 rmd : Introduced to manage the flow of packets to the TP Micro. If the packets
	// are being routed to the M1 and/or M2 ports we have a buffering problem at the TP Micro
	// due to the fast main board to tp micro versus the slow M1/M2 port rates. And the
	// resources at the TP Micro cannot handle too many buffered packets before overflowing
	// either a queue or running out of pool memory.
	BOOL_32						flow_control_packet;

	// 7/24/2013 rmd : Newly introduced to support internet connected devices which send their
	// data as one large single packet, bypassing the transport. Also used to support controller
	// initiated to know when to establish as connection, use and existing connection, or to
	// disconnect.
	UNS_32						handling_instructions;

} XMIT_CONTROL_LIST_ITEM;

typedef struct
{
	MIST_LIST_HDR_TYPE		xlist;

	XMIT_CONTROL_LIST_ITEM	*now_xmitting;

} XMIT_CONTROL_STRUCT_s;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern const char* const port_names[ UPORT_TOTAL_SYSTEM_PORTS ];  // = { "Port TP", "Port A", "Port B", "Port RRE", "Port USB", "Port M1" };

extern XMIT_CONTROL_STRUCT_s xmit_cntrl[ UPORT_TOTAL_PHYSICAL_PORTS ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void SetDTR( UPORTS pport, UNS_32 pto_level );

extern void set_reset_ACTIVE_to_serial_port_device( UPORTS port );

extern void set_reset_INACTIVE_to_serial_port_device( UPORTS port );

extern void kick_off_a_block_by_feeding_the_uart( UPORTS pport );

extern void SERIAL_set_baud_rate_on_A_or_B( UPORTS pport, UNS_32 pbaud_rate );

void initialize_uart_hardware( UPORTS pport );


extern BOOL_32 AddCopyOfBlockToXmitList( const UPORTS pport, DATA_HANDLE pdh, OUTGOING_MESSAGE_STRUCT *pom, const UNS_32 phandling_instructions, const UNS_32 psertport_drvr_posting_instructions );


extern void strout( const UPORTS pport, const char *pString );

void term_dat_out( UNS_8 *pstring );

void term_dat_out_crlf( UNS_8 *pstring );


extern void SERIAL_add_daily_stats_to_monthly_battery_backed( UNS_32 pinternet_port );

extern void SERIAL_at_midnight_manage_xmit_and_rcvd_accumulators_and_make_alert_lines( DATE_TIME_COMPLETE_STRUCT *pcdts_ptr );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

