/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	<stdlib.h>

#include	"radio_test.h"

#include	"tpl_out.h"

#include	"tpl_in.h"

#include	"cs_mem.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"speaker.h"

#include	"cursor_utils.h"

#include	"m_main.h"

#include	"d_comm_options.h"

#include	"e_keyboard.h"

#include	"e_numeric_keypad.h"

#include	"screen_utils.h"

#include	"comm_mngr.h"

#include	"crc.h"

#include	"packet_router.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


// 11/29/2017 rmd : This is one full packet for the old 2000e transport, and also for the
// CS3000 transport when used (not used throughout the 3000). I think this is a reasonable
// packet size to test with, for both the 2000 and 3000. I will fill it with random data to
// be echoed by the receiving controller. The data is tested for a match upon receipt.
#define RADIO_TEST_PACKET_SIZE		(TPL_USER_DATA_MAX_SIZE)

// ----------

#define RADIO_TEST_STATE_idle							(0x100)

#define RADIO_TEST_STATE_waiting_to_build_next_message	(0x200)

#define RADIO_TEST_STATE_waiting_for_response			(0x300)

// ----------

typedef struct
{
	// 11/29/2017 rmd : Are we idle, creating a message, waiting on a response, or what.
	UNS_32			state;

	// ----------
	
	UNS_32			test_start_tick_stamp;

	// 11/30/2017 rmd : Used to measure how long the response takes to show up.
	UNS_32			message_sent_tick_stamp;
	
	// 12/11/2017 rmd : This is the current response time the test is using. This value starts
	// out high and ratchets down as we learn the actual response times for the controller under
	// test.
	UNS_32			how_long_to_wait_for_a_response_ms;
	
	// ----------
	
	// 11/30/2017 rmd : The timer setting the message rate.
	xTimerHandle	message_rate_timer;

	// 11/30/2017 rmd : The timer waiting for the response.
	xTimerHandle	waiting_for_response_timer;

	// ----------
	
	// 11/30/2017 rmd : I want to us my own variables in the code not the GUIVar version (I like
	// my names) so will copy into these when the test starts.
	UNS_32			serial_number_3000;
	
	UNS_8			comm_addr_2000[ 3 ];

	UNS_32			port;

	// 11/30/2017 rmd : Testing 2000 or 3000.
	BOOL_32			test_with_2000;

	// ----------
	
	// 11/30/2017 rmd : The random data sent that we look for when echoed.
	UNS_8			test_data_sent[ RADIO_TEST_PACKET_SIZE ];
	
	// ----------
	
	// 12/11/2017 rmd : Statistics that are updated as the test progresses.
	UNS_32			response_time_minimum_ms;

	UNS_32			response_time_maximum_ms;

	// 12/11/2017 rmd : Used to calculate the average response ms.
	UNS_32			response_time_cummulative_ms;

	UNS_32			response_time_average_ms;

	// ----------
	
	UNS_32			messages_sent;

	UNS_32			messages_received_damaged;

	UNS_32			messages_received_missing;

	UNS_32			messages_received_okay;
	
	// 12/13/2017 rmd : And we needed another variable for calculating the average because we
	// get into divide by zero conditions if the received message content actually fails!
	UNS_32			messages_received;
	
	// ----------

} RADIO_TEST_CONTROL_STRUCT;


RADIO_TEST_CONTROL_STRUCT	rtcs;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void RADIO_TEST_if_addressed_to_the_hub_pass_packet_on_to_the_inbound_transport( const UNS_32 pfrom_which_port, const DATA_HANDLE pdh )
{
	TPL_DATA_HEADER_TYPE	tplh;

	// ----------

	// 11/27/2017 rmd : The packet is delivered with pre and post ambles stripped off, but the
	// CRC is still there. Examine the header to see if it is addressed to the HUB (our agreed
	// upon special address to use when running the comm test from a 2000e to a 3000 hub). If it
	// isn't to us, just return from this function, and no need to worry about the memory the
	// packet was delivered in, that is freed by the function that called us.
	memcpy( &tplh, pdh.dptr, sizeof( TPL_DATA_HEADER_TYPE ) );
	
	if( (tplh.H.FromTo.to[ 0 ] == 'H') && (tplh.H.FromTo.to[ 1 ] == 'U') && (tplh.H.FromTo.to[ 2 ] == 'B') )
	{
		// 11/28/2017 rmd : Okay so it is for us, so now give to the transport so the full inbound
		// message can be assembled (apparently the 2000e test message is multiple packets!).
		//Alert_Message_va( "test packet to hub %u bytes from %c%c%c", pdh.dlen, tplh.H.FromTo.from[0], tplh.H.FromTo.from[1], tplh.H.FromTo.from[2] );

		TPL_IN_make_a_copy_and_direct_incoming_packet( pfrom_which_port, pdh );
	}
}

/* ---------------------------------------------------------- */
extern void RADIO_TEST_echo_inbound_2000e_test_message_back_to_where_it_came_from( const ADDR_TYPE pfrom_to, const UNS_32 pfrom_which_port, const DATA_HANDLE pdh )
{
	DATA_HANDLE					ldh;

	UNS_16						command;
	
	TPL_OUT_EVENT_QUEUE_STRUCT	toqs;

	// ----------

	// 11/28/2017 rmd : The message delivered from the 2000e is the command (UNS_16) followed by
	// the payload. Copy this into our new message structure, and change the command ID to the
	// ECHO value. And hand it off to outgoing transport.
	
	// 11/27/2017 rmd : Build the response into a new memory block. This memory will be freed
	// when TPL_OUT builds the message.
	ldh.dptr = mem_malloc( pdh.dlen );

	ldh.dlen = pdh.dlen;
	
	// ----------
	
	// 11/28/2017 rmd : Copy the whole arriving message as that is what we want to echo back.
	memcpy( ldh.dptr, pdh.dptr, pdh.dlen );
	
	// ----------
	
	// 11/27/2017 rmd : This is the 2000e test packet ECHO command number. In the 3000 code base
	// we do not have reference to the 2000e command numbers so this is hard coded. And
	// furthermore it is byte swapped for the BIG ENDIAN 2000e 68K environment.
	//command = 1451;
	command = 43781;
	
	memcpy( ldh.dptr, &command, sizeof( UNS_16 ) );
	
	// ----------
	
	// 11/27/2017 rmd : Build an outgoing message. The message memory WILL NOT be freed until
	// the TPL_OUT queue message is processed in it's task and the OM has been created. Then it
	// is freed.

	toqs.event = TPL_OUT_QUEUE_COMMAND_here_is_an_outgoing_message;
	toqs.dh = ldh;
	toqs.port = pfrom_which_port;
	toqs.from_to = pfrom_to;
	toqs.msg_class.routing_class_base = ROUTING_CLASS_IS_2000_BASED;
	toqs.msg_class.rclass = MSG_CLASS_2000e_TEST_PACKET_ECHO_TO_C_OR_C;

	if( xQueueSend( TPL_OUT_event_queue, &toqs, 0 ) != pdPASS )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "TPL_OUT queue full" );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void radio_test_message_rate_timer_callback( xTimerHandle pxTimer )
{
	// 4/22/2014 rmd : Executed within the context of the high priority timer task.

	// 11/30/2017 rmd : The echoed back message never arrived and this timer expired. Indicate
	// such by throwing an event to the link_test task.
	RADIO_TEST_post_event( RADIO_TEST_EVENT_build_next_message );
}

/* ---------------------------------------------------------- */
static void radio_test_waiting_for_response_timer_callback( xTimerHandle pxTimer )
{
	// 4/22/2014 rmd : Executed within the context of the high priority timer task.

	// 11/30/2017 rmd : The echoed back message never arrived and this timer expired. Indicate
	// such by throwing an event to the link_test task.
	RADIO_TEST_post_event( RADIO_TEST_EVENT_response_timeout );
}

/* ---------------------------------------------------------- */
static BOOL_32 the_message_echoed_from_the_2000e_matched( RADIO_TEST_TASK_QUEUE_STRUCT *plttqs )
{
	// 12/11/2017 rmd : Compare the received message to what was sent. Skip over the first two
	// bytes, that is the command.
	BOOL_32		rv;
	
	UNS_8		*ucp1, *ucp2;
	
	// 12/11/2017 rmd : Create both pointers skipping over the UNS_16 command in both cases.
	ucp1 = rtcs.test_data_sent;
	ucp1 += sizeof(UNS_16);

	ucp2 = plttqs->dh.dptr;
	ucp2 += sizeof(UNS_16);
	
	// 12/11/2017 rmd : Do the comparison
	if( memcmp( ucp1, ucp2, (RADIO_TEST_PACKET_SIZE - sizeof(UNS_16)) ) == 0 )
	{
		rv = (true);
	}
	else
	{
		rv = (false);
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
static void build_and_send_the_next_2000e_test_message( void )
{
	// 12/11/2017 rmd : In this function we are building a message for the outbound transport,
	// which means all we are concerned with here is the payload, which in this case is the
	// UNS_16 command and the test data. I have decided to keep the 2000e link test to single
	// packet messages, which mean a total payload of not to exceed 496 bytes.
	DATA_HANDLE				ldh;
	
	UNS_8					*ucp;
	
	UNS_32					iii;
	
	TPL_OUT_EVENT_QUEUE_STRUCT  toqs;
	
	ADDR_TYPE				lfrom_to;
	
	UNS_16					command;
	
	// ----------

	// 11/27/2017 rmd : Build the response into a new memory block. This memory will be freed
	// when TPL_OUT builds the message.
	ldh.dlen = RADIO_TEST_PACKET_SIZE;
	
	ldh.dptr = mem_malloc( ldh.dlen );
	
	// ----------
	
	// 12/11/2017 rmd : Load the test data into place. Use a random sequence of data.
	ucp = ldh.dptr;
	
	srand( my_tick_count );
	
	for( iii=0; iii<RADIO_TEST_PACKET_SIZE; iii++ )
	{
		*ucp = rand();
	}
	
	// 12/11/2017 rmd : And snatch a copy so we can test the echoed response when it arrives.
	memcpy( rtcs.test_data_sent, ldh.dptr, RADIO_TEST_PACKET_SIZE );

	// ----------
	
	// 11/27/2017 rmd : This is the 2000e test packet ECHO command number. In the 3000 code base
	// we do not have reference to the 2000e command numbers so this is hard coded. And
	// furthermore it is byte swapped for the BIG ENDIAN 2000e 68K environment.
	//MID_TEST_PACKET_BETWEEN_CONTROLLERS_TO_ECHO = 1450,
	//MID_TEST_PACKET_BETWEEN_CONTROLLERS_TO_ECHO_RESP = 1451,
	command = 43525;
	
	memcpy( ldh.dptr, &command, sizeof( UNS_16 ) );
	
	// ----------

	// 12/11/2017 rmd : Set up the to from addresses. Notice we have to put them in REVERSE to
	// appease to 2000e BIG ENDIANESS.
	lfrom_to.from[ 0 ] = 'B';
	lfrom_to.from[ 1 ] = 'U';
	lfrom_to.from[ 2 ] = 'H';
	
	lfrom_to.to[ 0 ] = rtcs.comm_addr_2000[ 2 ];
	lfrom_to.to[ 1 ] = rtcs.comm_addr_2000[ 1 ];
	lfrom_to.to[ 2 ] = rtcs.comm_addr_2000[ 0 ];
	
	// ----------
	
	// 11/27/2017 rmd : Build an outgoing message. The message memory WILL NOT be freed until
	// the TPL_OUT queue message is processed in it's task and the OM has been created. Then it
	// is freed.
	toqs.event = TPL_OUT_QUEUE_COMMAND_here_is_an_outgoing_message;
	toqs.dh = ldh;
	toqs.port = rtcs.port;
	toqs.from_to = lfrom_to;
	toqs.msg_class.routing_class_base = ROUTING_CLASS_IS_2000_BASED;
	toqs.msg_class.rclass = MSG_CLASS_2000e_TEST_PACKET_FROM_C_OR_C;

	if( xQueueSend( TPL_OUT_event_queue, &toqs, 0 ) != pdPASS )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "TPL_OUT queue full" );
	}
}

/* ---------------------------------------------------------- */
static void build_and_send_the_next_3000_test_message( void )
{
	// 12/11/2017 rmd : In this function we are building up a packet that we will directly send
	// out the choosen serial port.
	
	DATA_HANDLE		ldh;
	
	UNS_8			*ucp;

	UNS_8			*working_ptr;
	
	UNS_32			iii;
	
	FROM_CONTROLLER_PACKET_TOP_HEADER	fcpth;
	
	CRC_BASE_TYPE   lcrc;
	
	// ----------

	// 12/13/2017 rmd : Build the outbound message into a new memory block.
	ldh.dlen = sizeof(FROM_CONTROLLER_PACKET_TOP_HEADER) + RADIO_TEST_PACKET_SIZE + sizeof(CRC_BASE_TYPE);

	ldh.dptr = mem_malloc( ldh.dlen );
	
	// ----------
	
	// 12/15/2017 rmd : Build up the header.
	memset( &fcpth, 0x00, sizeof(FROM_CONTROLLER_PACKET_TOP_HEADER) );
	
	fcpth.PID.__routing_class = MSG_CLASS_2000e_CS3000_FLAG;
	
	fcpth.cs3000_msg_class = MSG_CLASS_CS3000__TO_CONTROLLER__TEST_PACKET;
	
	fcpth.from_serial_number = config_c.serial_number;
	
	fcpth.to_serial_number = rtcs.serial_number_3000;

	// 12/15/2017 rmd : And we don't need an mid, we'll just use the class.

	memcpy( ldh.dptr, &fcpth, sizeof(FROM_CONTROLLER_PACKET_TOP_HEADER) );
	
	// ----------
	
	// 12/11/2017 rmd : Load the test data into place. Use a random sequence of data.
	working_ptr = ldh.dptr + sizeof(FROM_CONTROLLER_PACKET_TOP_HEADER);
	
	ucp = working_ptr;
		
	srand( my_tick_count );
	
	for( iii=0; iii<RADIO_TEST_PACKET_SIZE; iii++ )
	{
		*ucp++ = rand();
	}
	
	// 12/11/2017 rmd : And snatch a copy so we can test the echoed response when it arrives.
	memcpy( rtcs.test_data_sent, working_ptr, RADIO_TEST_PACKET_SIZE );

	// ----------

	// 12/18/2017 rmd : Calculate and place CRC into message.
	lcrc = CRC_calculate_32bit_big_endian( ldh.dptr, (ldh.dlen - sizeof(CRC_BASE_TYPE)) );

	working_ptr += RADIO_TEST_PACKET_SIZE;
	
	memcpy( working_ptr, &lcrc, sizeof(CRC_BASE_TYPE) );
	
	// ----------
	
	// 12/18/2017 rmd : The following call sends the packet but does not free the packet memory.
	// We need to do that following.
	attempt_to_copy_packet_and_route_out_another_port( rtcs.port, ldh, NULL );
	
	mem_free( ldh.dptr );
}

/* ---------------------------------------------------------- */
static void if_addressed_to_us_echo_this_3000_packet( UNS_32 const pport, DATA_HANDLE const pdh )
{
    // 1/16/2018 Ryan : Executed within the context of the radio test task.

	// ----------
	
	FROM_CONTROLLER_PACKET_TOP_HEADER	*fcpth_ptr;
	
	CRC_BASE_TYPE   lcrc;
	
	// ----------
	
	fcpth_ptr = (FROM_CONTROLLER_PACKET_TOP_HEADER*)pdh.dptr;
	
	// 12/18/2017 rmd : And test the packet length since we are going to be working on it
	// assuming it is the correct test packet size, writing directly into its memory space.
	if( (fcpth_ptr->to_serial_number == config_c.serial_number) && (pdh.dlen == (sizeof(FROM_CONTROLLER_PACKET_TOP_HEADER) + RADIO_TEST_PACKET_SIZE + sizeof(CRC_BASE_TYPE))) )
	{
		// 12/18/2017 rmd : This is a bit risky, but we could just work within the same memory
		// block, modify the to/from addresses and the CRC, and send it back. This is possible
		// because the attempt_to_route_packet_out_another_port call at the end of this function
		// makes a copy of the packet.
		fcpth_ptr->cs3000_msg_class = MSG_CLASS_CS3000__TO_CONTROLLER__TEST_PACKET_ECHO;
		
		fcpth_ptr->to_serial_number = fcpth_ptr->from_serial_number;
		
		fcpth_ptr->from_serial_number = config_c.serial_number;
		
		// ----------
		
		// 12/18/2017 rmd : Calculate the CRC.
		lcrc = CRC_calculate_32bit_big_endian( pdh.dptr, (pdh.dlen - sizeof(CRC_BASE_TYPE)) );
	
		memcpy( pdh.dptr + (sizeof(FROM_CONTROLLER_PACKET_TOP_HEADER) + RADIO_TEST_PACKET_SIZE), &lcrc, sizeof(CRC_BASE_TYPE) );
		
		// ----------
		
		// 12/18/2017 rmd : The following call sends the packet but does not free the packet memory.
		// We need to do that following.
		attempt_to_copy_packet_and_route_out_another_port( pport, pdh, NULL );

        // 1/16/2018 Ryan : The packet memory is freed upon return from this call by the radio test
        // task.
	}
}

/* ---------------------------------------------------------- */
static BOOL_32 the_message_echoed_from_the_3000_matched( RADIO_TEST_TASK_QUEUE_STRUCT *plttqs )
{
	// 12/18/2017 rmd : Compare the received message to what was sent. Skip over the header.

	BOOL_32		rv;
	
	UNS_8		*ucp;
	
	ucp = plttqs->dh.dptr;
	ucp += sizeof(FROM_CONTROLLER_PACKET_TOP_HEADER);
	
	// 12/11/2017 rmd : Do the comparison
	if( memcmp( rtcs.test_data_sent, ucp, RADIO_TEST_PACKET_SIZE ) == 0 )
	{
		rv = (true);
	}
	else
	{
		rv = (false);
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
static void update_radio_test_gui_vars( BOOL_32 pupdate_sucessful_percent )
{
	// 2/29/2016 rmd : DEFINES to avoid GCC floating point compiler bug.
	const volatile float ONE_HUNDRED_POINT_ZERO = 100.0;

	const volatile float ONE_THOUSAND_POINT_ZERO = 1000.0;
	
	// ----------
	
	// 12/13/2017 ajv : Update the GuiVar which controls whether the Start or Stop button is 
	// shown. 
	GuiVar_RadioTestInProgress = (rtcs.state != RADIO_TEST_STATE_idle);

	// ----------
	
	// 12/12/2017 rmd : Figure the test elapsed time.
	GuiVar_RadioTestStartTime = (((my_tick_count - rtcs.test_start_tick_stamp) * portTICK_RATE_MS)/ONE_THOUSAND_POINT_ZERO);

	// ----------
	
	// 12/13/2017 rmd : Only want to update AFTER an exchange has ended, otherwise we see a
	// bouncing percentage.
	if( pupdate_sucessful_percent )
	{
		GuiVar_RadioTestSuccessfulPercent = (ONE_HUNDRED_POINT_ZERO * ((float)rtcs.messages_received_okay / rtcs.messages_sent));
	}

	// ----------
	
	GuiVar_RadioTestPacketsSent = rtcs.messages_sent;
	
	GuiVar_RadioTestBadCRC = rtcs.messages_received_damaged;

	GuiVar_RadioTestNoResp = rtcs.messages_received_missing;

	GuiVar_RadioTestRoundTripAvgMS = ((float)rtcs.response_time_average_ms/ONE_THOUSAND_POINT_ZERO);

	// 12/13/2017 ajv : Since we initialize the minimum to an extremely high number, easyGUI 
	// can't display it properly. Therefore, display 0 if we haven't calculated a minimum time 
	// yet. 
	if( rtcs.response_time_minimum_ms != 0xFFFFFFFF )
	{
		GuiVar_RadioTestRoundTripMinMS = ((float)rtcs.response_time_minimum_ms / ONE_THOUSAND_POINT_ZERO);
	}
	else
	{
		GuiVar_RadioTestRoundTripMinMS = 0.0;
	}

	GuiVar_RadioTestRoundTripMaxMS = ((float)rtcs.response_time_maximum_ms/ONE_THOUSAND_POINT_ZERO);
}

/* ---------------------------------------------------------- */
static void process_radio_test_event( RADIO_TEST_TASK_QUEUE_STRUCT *plttqs )
{
	// 12/1/2017 rmd : We have already assured that the test mode is RUNNING, so no need to
	// check that here. Proceed based upon the STATE.
	
	UNS_32	duration_ms;
	
	BOOL_32	update_sucessful_percentage;
	
	// ----------
	
	// 12/13/2017 rmd : Only after we create a new message do we avoid updating the sucessful %,
	// this is to avoid the % displayed bouncing lower (below 100%) when mid-exchange.
	update_sucessful_percentage = (true);
	
	// ----------
	
	switch( rtcs.state )
	{
		case RADIO_TEST_STATE_waiting_to_build_next_message:

			if( plttqs->event == RADIO_TEST_EVENT_build_next_message )
			{
				if( rtcs.test_with_2000 )
				{
					build_and_send_the_next_2000e_test_message();
				}
				else
				{
					build_and_send_the_next_3000_test_message();
				}
				
				// ----------
				
				// 12/13/2017 rmd : We just sent a message, if we were to update the guivar the % would be
				// distorted.
				update_sucessful_percentage = (false);
				
				// ----------
				
				rtcs.messages_sent += 1;
				
				rtcs.message_sent_tick_stamp = my_tick_count;

				rtcs.state = RADIO_TEST_STATE_waiting_for_response;

				xTimerChangePeriod( rtcs.waiting_for_response_timer, MS_to_TICKS(rtcs.how_long_to_wait_for_a_response_ms), portMAX_DELAY );
			}
			else
			{
				Alert_Message_va( "LINK TEST: waiting to build - unexp event %u", plttqs->event );
			}
			break;
			

		case RADIO_TEST_STATE_waiting_for_response:

			// 12/12/2017 rmd : No matter the event always stop the response timer.
			xTimerStop( rtcs.waiting_for_response_timer, portMAX_DELAY );
			
			// ----------
			
			if( plttqs->event == RADIO_TEST_EVENT_here_is_an_inbound_echo_to_process )
			{
				// 12/13/2017 rmd : Regardless of good or bad.
				rtcs.messages_received += 1;
				
				// ----------

				if( rtcs.test_with_2000 )
				{
					if( the_message_echoed_from_the_2000e_matched( plttqs ) )
					{
						// 12/11/2017 rmd : Update the statistics.
						rtcs.messages_received_okay += 1;
					}
					else
					{
						// 12/12/2017 rmd : This probably NEVER happens, how could the CRC pass and the content
						// be bad? BTW, there is a catch for bad CRC's, in the packet router code, that generates a
						// BAD_CRC event for this task.
						rtcs.messages_received_damaged += 1;
					}
				}
				else
				{
					// 12/12/2017 rmd : We are expecting a 3000 response.
					if( the_message_echoed_from_the_3000_matched( plttqs ) )
					{
						// 12/11/2017 rmd : Update the statistics.
						rtcs.messages_received_okay += 1;
					}
					else
					{
						// 12/12/2017 rmd : This probably NEVER happens, how could the CRC pass and the content
						// be bad? BTW, there is a catch for bad CRC's, in the packet router code, that generates a
						// BAD_CRC event for this task.
						rtcs.messages_received_damaged += 1;
					}
				}

				// ----------
				
				// 12/13/2017 rmd : WELL, we received an inbound message of the proper CLASS and TO address,
				// and in the case of a hub a FROM address on our hub list. Sooooo, update these delivery
				// time stats.
				duration_ms = (my_tick_count - rtcs.message_sent_tick_stamp) * portTICK_RATE_MS;
				
				if( duration_ms > rtcs.response_time_maximum_ms )
				{
					rtcs.response_time_maximum_ms = duration_ms;
				}
				
				if( duration_ms < rtcs.response_time_minimum_ms )
				{
					rtcs.response_time_minimum_ms = duration_ms;
				}

				// ----------
				
				rtcs.response_time_cummulative_ms += duration_ms;

				// 12/13/2017 rmd : NO divide by zero fear, messages_received will never be zero here!
				rtcs.response_time_average_ms = (rtcs.response_time_cummulative_ms / rtcs.messages_received);
			}
			else
			if( plttqs->event == RADIO_TEST_EVENT_response_timeout )
			{
				Alert_Message( "RADIO TEST: waiting for response - timeout" );
				
				rtcs.messages_received_missing += 1;
			}
			else
			if( plttqs->event == RADIO_TEST_EVENT_bad_crc )
			{
				Alert_Message( "RADIO TEST: waiting for response - bad crc" );
				
				// 12/12/2017 rmd : This count is a little loose because any bad CRC on ANY PORT will bump
				// this count if the test is underway. That's a risk I'm willing to take.
				rtcs.messages_received_damaged += 1;
			}
			else
			{
				Alert_Message_va( "RADIO TEST: waiting for response - unexp event %u", plttqs->event );
			}

			// ----------
			
			rtcs.state = RADIO_TEST_STATE_waiting_to_build_next_message;

			// 12/12/2017 rmd : If we got a bad crc or the response timed out send the next message
			// right away, otherwise use our average response time as the delay. Delay between packets
			// is self learning based upon response time. We're shooting for a 50% duty cycle.
			if( (plttqs->event == RADIO_TEST_EVENT_response_timeout) || (plttqs->event == RADIO_TEST_EVENT_bad_crc) )
			{
				RADIO_TEST_post_event( RADIO_TEST_EVENT_build_next_message );
			}
			else
			{
				// 12/13/2017 rmd : If we timed out on the very first message attempt, the
				// response_time_average will be ZERO. Or if somehow the average became less than 5ms (our
				// portTICK_RATE) and we try to convert that to ticks with the MS_to_TICKS macro, we'll get
				// a zero, and if we try to set the timer with that, we will trigger an ASSERT exception
				// which will result in a reboot! So avoid that here.
				if( rtcs.response_time_average_ms < portTICK_RATE_MS )
				{
					rtcs.response_time_average_ms = portTICK_RATE_MS;
				}
				
				xTimerChangePeriod( rtcs.message_rate_timer, MS_to_TICKS(rtcs.response_time_average_ms), portMAX_DELAY );
			}

			break;  // of waiting for response STATE
		
		// ----------
		
		default:
			Alert_Message( "RADIO TEST: unhandled STATE" );
			
	}
	
	// ----------
	
	update_radio_test_gui_vars( update_sucessful_percentage );
}

/* ---------------------------------------------------------- */
static void radio_test_stop_and_cleanup( void )
{
	// 12/1/2017 rmd : Doesn't matter the STATE or MODE, stop the timers and set the mode and
	// state to IDLE. This function may ONLY be called from the LINK_TEST_task, and is therefore
	// executed within that tasks context. Calling from the task makes it safe to step on the
	// mode and state.
		
	// ----------
	
	// 11/3/2017 rmd : Stop these timers otherwise we'll report the timer fired unexpectedly.
	xTimerStop( rtcs.waiting_for_response_timer, portMAX_DELAY );

	xTimerStop( rtcs.message_rate_timer, portMAX_DELAY );
	
	rtcs.state = RADIO_TEST_STATE_idle;
}

/* ---------------------------------------------------------- */
static BOOL_32 gui_this_port_has_a_radio_test_device( UNS_32 puart_port )
{
	BOOL_32		rv;
	
	if( puart_port == UPORT_A )
	{
		rv = ( (config_c.port_A_device_index == COMM_DEVICE_LR_RAVEON) || (config_c.port_A_device_index == COMM_DEVICE_LR_FREEWAVE) || (config_c.port_A_device_index == COMM_DEVICE_SR_FREEWAVE) );
	}
	else
	{
		rv = ( (config_c.port_B_device_index == COMM_DEVICE_LR_RAVEON) || (config_c.port_B_device_index == COMM_DEVICE_LR_FREEWAVE) || (config_c.port_B_device_index == COMM_DEVICE_SR_FREEWAVE) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 RADIO_TEST_there_is_a_port_device_to_test( void )
{
	// 12/14/2017 rmd : Use this function to decide if we should show the radio test menu item
	// as a choice.

	return( gui_this_port_has_a_radio_test_device( UPORT_A ) || gui_this_port_has_a_radio_test_device( UPORT_B ) );
}

/* ---------------------------------------------------------- */
static void gui_set_the_port_and_device_index_gui_vars( UNS_32 pport )
{
	if( pport == UPORT_A )
	{
		GuiVar_RadioTestPort = UPORT_A;

		GuiVar_RadioTestDeviceIndex = config_c.port_A_device_index;
	}
	else
	{
		GuiVar_RadioTestPort = UPORT_B;

		GuiVar_RadioTestDeviceIndex = config_c.port_B_device_index;
	}
}

/* ---------------------------------------------------------- */
static void gui_find_the_first_port_to_test_and_set_the_port_and_device_index_gui_vars( void )
{
	// 12/14/2017 rmd : Use of this function ASSUMES that either port A, or port B, or both have
	// devices on them that qualify for use during the radio test.
	
	if( gui_this_port_has_a_radio_test_device( UPORT_A ) )
	{
		gui_set_the_port_and_device_index_gui_vars( UPORT_A );
	}
	else
	{
		gui_set_the_port_and_device_index_gui_vars( UPORT_B );
	}
}

/* ---------------------------------------------------------- */
extern void init_radio_test_gui_vars_and_state_machine( void )
{ 
	// 12/13/2017 rmd : On startup initialize the guivars and radio_test control structure.
	
	// ----------

	// 12/15/2017 rmd : Technically the C startup code has done this already, but to explicitly
	// show that we must make sure the results variables are all clean for the first showing
	// make sure the whole strucutre is zeroed.
	memset( &rtcs, 0x00, sizeof(RADIO_TEST_CONTROL_STRUCT) );
	
	// 6/7/2017 rmd : Be explicit. Always start in the idle mode.
	rtcs.state = RADIO_TEST_STATE_idle;
	
	// ----------

	// 12/1/2017 rmd : Create the echo timeout (receive error) timer. Default to 15 seconds,
	// generally a long time when it comes to round trip messaging of this test. However 2000e's
	// may present a twist if the controller is on a large chain, and we deal with that when
	// sending the first packet.
	rtcs.waiting_for_response_timer = xTimerCreate( NULL, MS_to_TICKS(30000), (false), NULL, radio_test_waiting_for_response_timer_callback );


	rtcs.message_rate_timer = xTimerCreate( NULL, MS_to_TICKS(30000), (false), NULL, radio_test_message_rate_timer_callback );

	// ----------
	
	// 11/29/2017 rmd : Initialize the involved GUI vars.
	GuiVar_RadioTestCommWithET2000e = (false);

	// 12/15/2017 rmd : The 2000 represents about where we are today.
	GuiVar_RadioTestCS3000SN = CONTROLLER_MINIMUM_SERIAL_NUMBER + 2000;

	strlcpy( GuiVar_RadioTestET2000eAddr0, "!", sizeof(GuiVar_RadioTestET2000eAddr0) );
	strlcpy( GuiVar_RadioTestET2000eAddr1, "!", sizeof(GuiVar_RadioTestET2000eAddr1) );
	strlcpy( GuiVar_RadioTestET2000eAddr2, "A", sizeof(GuiVar_RadioTestET2000eAddr2) );
	
	// ----------
	
	gui_find_the_first_port_to_test_and_set_the_port_and_device_index_gui_vars();

	update_radio_test_gui_vars( (false) );
	
	// 12/15/2017 rmd : But need to clean up the elapsed time variable, because normally the
	// prior function is only called when the test is running, which it is not now.
	GuiVar_RadioTestStartTime = 0;
}

/* ---------------------------------------------------------- */
extern void RADIO_TEST_task( void *pvParameters )
{
	RADIO_TEST_TASK_QUEUE_STRUCT		lttqs;
	
	// 10/23/2012 rmd : To support the WDT activity monitor.
	UNS_32		task_index;
	
	// ----------

	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// ----------

	while( (true) )
	{
		// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
		if( xQueueReceive( RADIO_TEST_task_queue, &lttqs, MS_to_TICKS( 500 ) ) == pdTRUE )
		{
			switch( lttqs.event )
			{
				case RADIO_TEST_EVENT_start_the_test:
				
					// 6/30/2017 rmd : Don't try if the user has entered device exchange mode.
					if( !in_device_exchange_hammer )
					{
						if( rtcs.state == RADIO_TEST_STATE_idle )
						{
							rtcs.test_start_tick_stamp = my_tick_count;
							
							// ----------
							
							// 12/1/2017 rmd : Load my control variables from the GUI.
							rtcs.comm_addr_2000[ 0 ] = GuiVar_RadioTestET2000eAddr0[ 0 ];
							rtcs.comm_addr_2000[ 1 ] = GuiVar_RadioTestET2000eAddr1[ 0 ];
							rtcs.comm_addr_2000[ 2 ] = GuiVar_RadioTestET2000eAddr2[ 0 ];
							
							rtcs.serial_number_3000 = GuiVar_RadioTestCS3000SN;
							
							rtcs.test_with_2000 = GuiVar_RadioTestCommWithET2000e;
							
							rtcs.port = GuiVar_RadioTestPort;
							
							// ----------
							
							// 12/11/2017 rmd : Wait 10 seconds for 3000's, and 20 seconds for 2000e (because of the
							// possibility of a long SR chain). So far this is just a fixed time, not adjusted as the
							// test progresses.
							if( rtcs.test_with_2000 )
							{
								rtcs.how_long_to_wait_for_a_response_ms = 20000;
							}
							else
							{
								rtcs.how_long_to_wait_for_a_response_ms = 10000;
							}
							
							// ----------
							
							rtcs.response_time_maximum_ms = 0x00;
							rtcs.response_time_minimum_ms = 0xFFFFFFFF;
							rtcs.response_time_cummulative_ms = 0x00;
							rtcs.response_time_average_ms = 0x00;
							

							rtcs.messages_sent = 0;
							rtcs.messages_received_damaged = 0;
							rtcs.messages_received_missing = 0;
							rtcs.messages_received_okay = 0;
							rtcs.messages_received = 0;

							// ----------
							
							rtcs.state = RADIO_TEST_STATE_waiting_to_build_next_message;

							// ----------
							
							// 11/30/2017 rmd : Ready to go. Post the next message event to ourselves.
							RADIO_TEST_post_event( RADIO_TEST_EVENT_build_next_message );

							// ----------
							
							// 12/12/2017 rmd : Now that the control structure variables are all initialized reflect
							// them into the gui vars.
							update_radio_test_gui_vars( (true) );

							// 12/13/2017 rmd : And make the start/stop button cycle.
							Redraw_Screen( (false) );
						}
						else
						{
							Alert_Message( "RADIO TEST: request to start when not idle" );
						}
					}
					else
					{
						// 12/1/2017 rmd : Well they're trying to program a device, don't start, and clean up if
						// needed.
						RADIO_TEST_post_event( RADIO_TEST_EVENT_stop_the_test );
					}
					break;
					
				// ----------
				
				case RADIO_TEST_EVENT_build_next_message:

					// 6/30/2017 rmd : Don't try if the user has entered device exchange mode.
					if( !in_device_exchange_hammer )
					{
						if( rtcs.state != RADIO_TEST_STATE_idle )
						{
							process_radio_test_event( &lttqs );
						}
						else
						{
							// 12/1/2017 rmd : This is not necessarily an error, and should be extremely rare, but could
							// happen if a STOP event was starting to process, but the message rate timer fired before
							// the STOP processing stopped that timer.
							Alert_Message( "RADIO TEST unexp build message event" );
							
							// 12/1/2017 rmd : And clean up if it isn't already.
							RADIO_TEST_post_event( RADIO_TEST_EVENT_stop_the_test );
						}
					}
					else
					{
						// 12/1/2017 rmd : Well they're trying to program a device, stop any test that may be
						// ongoing.
						RADIO_TEST_post_event( RADIO_TEST_EVENT_stop_the_test );
					}
					break;

				// ----------
				
				case RADIO_TEST_EVENT_here_is_an_inbound_echo_to_process:
					// 12/12/2017 rmd : This is the ECHOED result to our outbound test message. It can come from
					// a 2000e or a 3000. Our queue item holds the message in memory that we are responsible to
					// release.
					if( !in_device_exchange_hammer )
					{
						if( rtcs.state != RADIO_TEST_STATE_idle )
						{
							process_radio_test_event( &lttqs );
						}
						else
						{
							// 12/1/2017 rmd : This is not necessarily an error, and should be extremely rare, but could
							// happen if a STOP event was starting to process, but the message rate timer fired before
							// the STOP processing stopped that timer.
							Alert_Message( "RADIO TEST unexp inbound message event" );
							
							// 12/1/2017 rmd : And clean up if it isn't already.
							RADIO_TEST_post_event( RADIO_TEST_EVENT_stop_the_test );
						}
					}
					else
					{
						RADIO_TEST_post_event( RADIO_TEST_EVENT_stop_the_test );
					}

					// ----------

					// 12/1/2017 rmd : No matter what just took place, BE SURE we free the message memory!
					mem_free( lttqs.dh.dptr );
					
					break;
					
                // ----------
				
				case RADIO_TEST_EVENT_here_is_an_inbound_message_to_echo:
                    // 1/16/2018 Ryan : The message being received is the test message from another CS3000
                    // controller. We must echo the message back to this controller, but not if an device
                    // programming is underway.
                    if( !in_device_exchange_hammer )
					{
                        if_addressed_to_us_echo_this_3000_packet( lttqs.from_port, lttqs.dh );
                    }

                    // ----------

                    // 1/16/2018 Ryan : Free the memory.
					mem_free( lttqs.dh.dptr );
					
					break;

                // ----------
				
				case RADIO_TEST_EVENT_bad_crc:
				case RADIO_TEST_EVENT_response_timeout:

					// 12/11/2017 rmd : Don't process if the user has entered device exchange mode.
					if( !in_device_exchange_hammer )
					{
						if( rtcs.state != RADIO_TEST_STATE_idle )
						{
							process_radio_test_event( &lttqs );
						}
						else
						{
							// 12/1/2017 rmd : This is not necessarily an error, and should be extremely rare, but could
							// happen if a STOP event was starting to process, but the message rate timer fired before
							// the STOP processing stopped that timer.
							//
							// The BAD CRC event is thrown whenever there is a bad CRC and can show at aytime, even when
							// the test is not running, so don't make an alert line for it.
							if( lttqs.event != RADIO_TEST_EVENT_bad_crc )
							{
								Alert_Message( "RADIO TEST unexp response timeout event" );
							}
							
							// 12/1/2017 rmd : And clean up if it isn't already.
							RADIO_TEST_post_event( RADIO_TEST_EVENT_stop_the_test );
						}
					}
					else
					{
						// 12/1/2017 rmd : Well they're trying to program a device, stop any test that may be
						// ongoing.
						RADIO_TEST_post_event( RADIO_TEST_EVENT_stop_the_test );
					}
					break;

				// ----------
				
				case RADIO_TEST_EVENT_stop_the_test:
					radio_test_stop_and_cleanup();

					// 12/13/2017 rmd : This event can be issued when the keypad times out, when we are NOT on
					// the radio test screen, so test for that and don't update screen if we aren't here.
					if( GuiLib_CurStructureNdx == GuiStruct_scrRadioTest_0 )
					{
						// 12/13/2017 rmd : DON'T change the % successful results just casue we are stopping the
						// test.
						update_radio_test_gui_vars( (false) );
						
						// 12/13/2017 rmd : And make the start/stop button cycle.
						Redraw_Screen( (false) );
					}

					
					break;
					
				// ----------

				case RADIO_TEST_EVENT_1hz_screen_update:

					if( rtcs.state != RADIO_TEST_STATE_idle )
					{
						// 12/13/2017 rmd : We need to do this to get the elapsed test time to smooth out and count
						// second by second. DON'T change the % successful results, if you do it bounces.
						update_radio_test_gui_vars( (false) );
						
						// 12/13/2017 rmd : And update the screen - not doing a complete redraw.
						Redraw_Screen( (false) );
					}
					
					break;
					
				// ----------
				
				default:
					Alert_Message( "unhandled link test event" );
					
			}  // of event processing switch statement

		}  // of a sucessful queue item is available

		// ----------

		// 10/23/2012 rmd : Keep the activity indication going.
		task_last_execution_stamp[ task_index ] = xTaskGetTickCount();

	}  // of forever task loop
}

/* ---------------------------------------------------------- */
extern void RADIO_TEST_post_event_with_details( RADIO_TEST_TASK_QUEUE_STRUCT *pq_item_ptr )
{
	// 6/24/2015 rmd : Can be executed within the context of a wide variety of tasks.

	//  WARNING: DO NOT CALL THIS FUNCTION FROM AN INTERRUPT because it is not using the fromISR
	//  xQueue function.

	if( xQueueSendToBack( RADIO_TEST_task_queue, pq_item_ptr, 0 ) != pdPASS )	 // do not wait for queue space ... flag an error
	{
		Alert_Message( "Radio Test queue overflow!" );
	}
}

/* ---------------------------------------------------------- */
extern void RADIO_TEST_post_event( UNS_32 pevent )
{
	// 6/24/2015 rmd : Can be executed within the context of a wide variety of tasks.

	//  WARNING: DO NOT CALL THIS FUNCTION FROM AN INTERRUPT because it is not using the fromISR
	//  xQueue function.

	RADIO_TEST_TASK_QUEUE_STRUCT		lttqs;
	
	lttqs.event = pevent;
	
	RADIO_TEST_post_event_with_details( &lttqs );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/*
 GuiVar_RadioTestCommWithET2000e controls the radio button at the top (cursor positions 0
 and 1, respectively). When ET2000e is selected, the variable should be set TRUE and vice
 versa. This dictates what is displayed below. I've played a game with the radio buttons so
 they're mutually exclusive - when one is set, the other is automatically unset. It should
 be set FALSE by default to default to the CS3000.

 Cursor 2 is GuiVar_RadioTestCS3000SN and is a uint32_t and is only visible when
 GuiVar_RadioTestCommWithET2000e is false. I would default this to
 CONTROLLER_MINIMUM_SERIAL_NUMBER on power-up.

 Cursor 3 (same screen position as Cursor 2) is GuiVar_RadioTestET2000eAddr, is a
 three-character (+ NULL term) string visible only when GuiVar_RadioTestCommWithET2000e is
 true. I would default this to !!A on power-up.

 GuiVar_RadioTestPort is a numerical representation of the port displayed at Cursor 4. The
 displayed text (A or B) matches our UPORT definitions in gpio_setup.h so use those numbers
 when setting the variable. Additionally, you'll need to set GuiVar_RadioTestDeviceIndex to
 the appropriate config_c.port_A_device_index or config_c.port_B_device_index so it displays
 the correct radio string.

The Start and Stop buttons are cursors 5 and 6, respectively.

As for the statistics, these are the variables and should all be set to 0 when the user presses Start:
"	GuiVar_RadioTestPacketsSent: uint32_t
"	GuiVar_RadioTestSuccessfulPercent: uint32_t
"	GuiVar_RadioTestNoResp: uint32_t
"	GuiVar_RadioTestBadCRC: uint32_t
"	GuiVar_RadioTestRoundTripMinMS: float
"	GuiVar_RadioTestRoundTripMaxMS: float
"	GuiVar_RadioTestRoundTripAvgMS: float
"	GuiVar_RadioTestStartTime: uint32_t; ranges from 0 to 86400 but display rolls at 5999 milliseconds (99:59 minutes)

Finally, here are the two iterations of the screen:
*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_RADIO_TEST_2000_BUTTON			(0)
#define	CP_RADIO_TEST_3000_BUTTON			(1)
#define	CP_RADIO_TEST_3000_SERIAL_NUMBER	(2)
#define	CP_RADIO_TEST_2000_ADDR_0			(3)
#define	CP_RADIO_TEST_2000_ADDR_1			(4)
#define	CP_RADIO_TEST_2000_ADDR_2			(5)
#define	CP_RADIO_TEST_PORT					(6)
#define	CP_RADIO_TEST_START_BUTTON			(7)

/* ---------------------------------------------------------- */
extern void FDTO_RADIO_TEST_draw_radio_comm_test_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	// ----------
	
	if( pcomplete_redraw == (true) )
	{
		// 12/13/2017 rmd : PRESERVE the results in the GUI vars as they were from the last test, or
		// how they were initialized when the program started. HOWEVER, if the test timed out while
		// not on the Radio Test screen, we need to update the gui var that determines how to draw
		// the start/stop key.
		GuiVar_RadioTestInProgress = (rtcs.state != RADIO_TEST_STATE_idle);
		
		// ----------
		
		// 12/13/2017 rmd : Depending on how the user last left it put the cursor on the radio
		// button that is selected.
		if( GuiVar_RadioTestCommWithET2000e )
		{
			lcursor_to_select = CP_RADIO_TEST_2000_BUTTON;
		}
		else
		{
			lcursor_to_select = CP_RADIO_TEST_3000_BUTTON;
		}
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrRadioTest_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void RADIO_TEST_process_radio_comm_test_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_dlgNumericKeypad_0:
			NUMERIC_KEYPAD_process_key( pkey_event, &FDTO_RADIO_TEST_draw_radio_comm_test_screen );
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_RADIO_TEST_2000_BUTTON:
							if( GuiVar_RadioTestCommWithET2000e == (false) )
							{
								good_key_beep();
								GuiVar_RadioTestCommWithET2000e	= (true);
							}
							else
							{
								bad_key_beep();
							}
							Redraw_Screen( (false) );
							break;
		
						case CP_RADIO_TEST_3000_BUTTON:
							if( GuiVar_RadioTestCommWithET2000e == (true) )
							{
								good_key_beep();
								GuiVar_RadioTestCommWithET2000e	= (false);
							}
							else
							{
								bad_key_beep();
							}
							Redraw_Screen( (false) );
							break;
		
						case CP_RADIO_TEST_3000_SERIAL_NUMBER:
							good_key_beep();
							NUMERIC_KEYPAD_draw_uint32_keypad( &GuiVar_RadioTestCS3000SN, CONTROLLER_MINIMUM_SERIAL_NUMBER, CONTROLLER_MAXIMUM_SERIAL_NUMBER );
							break;
							
						case CP_RADIO_TEST_START_BUTTON:
							if( rtcs.state != RADIO_TEST_STATE_idle )
							{
								// 12/13/2017 rmd : Our job is to STOP the test.
								good_key_beep();
									
								RADIO_TEST_post_event( RADIO_TEST_EVENT_stop_the_test );
							}
							else
							{
								// 12/13/2017 rmd : Our job is to START the test.
								
								// 11/29/2017 rmd : Remember to check the port choosen has a device. If not bad key beep.
								if( ((GuiVar_RadioTestPort == UPORT_A) && (config_c.port_A_device_index == COMM_DEVICE_NONE_PRESENT)) || ((GuiVar_RadioTestPort == UPORT_B) && (config_c.port_B_device_index == COMM_DEVICE_NONE_PRESENT)) )
								{
									// 11/29/2017 rmd : Don't run the test with empty ports.
									bad_key_beep();
								}
								else
								{
									good_key_beep();
	
									// 11/29/2017 rmd : Start the test.
									RADIO_TEST_post_event( RADIO_TEST_EVENT_start_the_test );
								}
							}
							break;
							
						// ----------
		
						default:
							bad_key_beep();
					}
					break;
		
				case KEY_MINUS:
				case KEY_PLUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_RADIO_TEST_2000_ADDR_0:
							// 1/11/2018 ajv : Use the range of the ET2000e comm addresses which is '!' (0x21) to '~' 
							// (0x7E). This does exclude spaces which is only an issue when trying to talk to a 
							// 500-series hub which can be <space> <space> 'A', but why would a CS3000 ever be talking 
							// to an ET2000 hub?
							process_char( pkey_event.keycode, &GuiVar_RadioTestET2000eAddr0[ 0 ], '!', '~', 1, (true) );

							Refresh_Screen();
							break;

						case CP_RADIO_TEST_2000_ADDR_1:
							// 1/11/2018 ajv : Use the range of the ET2000e comm addresses which is '!' (0x21) to '~' 
							// (0x7E). This does exclude spaces which is only an issue when trying to talk to a 
							// 500-series hub which can be <space> <space> 'A', but why would a CS3000 ever be talking 
							// to an ET2000 hub?
							process_char( pkey_event.keycode, &GuiVar_RadioTestET2000eAddr1[ 0 ], '!', '~', 1, (true) );

							Refresh_Screen();
							break;

						case CP_RADIO_TEST_2000_ADDR_2:
							// 1/11/2018 ajv : Use the range of the ET2000e comm addresses which is '!' (0x21) to '~' 
							// (0x7E). This does exclude spaces which is only an issue when trying to talk to a 
							// 500-series hub which can be <space> <space> 'A', but why would a CS3000 ever be talking 
							// to an ET2000 hub?
							process_char( pkey_event.keycode, &GuiVar_RadioTestET2000eAddr2[ 0 ], '!', '~', 1, (true) );

							Refresh_Screen();
							break;

						case CP_RADIO_TEST_PORT:
							if( GuiVar_RadioTestPort == UPORT_A )
							{
								if( gui_this_port_has_a_radio_test_device( UPORT_B ) )
								{
									good_key_beep();
									
									gui_set_the_port_and_device_index_gui_vars( UPORT_B );
								}
								else
								{
									bad_key_beep();
								}
							}
							else
							{
								if( gui_this_port_has_a_radio_test_device( UPORT_A ) )
								{
									good_key_beep();
									
									gui_set_the_port_and_device_index_gui_vars( UPORT_A );
								}
								else
								{
									bad_key_beep();
								}
							}
							break;
							
						default:
							bad_key_beep();
					}
					break;
		
				case KEY_C_UP:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_RADIO_TEST_2000_ADDR_0:
						case CP_RADIO_TEST_2000_ADDR_1:
						case CP_RADIO_TEST_2000_ADDR_2:
							CURSOR_Select( CP_RADIO_TEST_3000_BUTTON, (true) );
							break;

                        case CP_RADIO_TEST_PORT:
							// 2/2/2018 Ryan : Added if statment so up cursor would got to correct place for testing
							// with 2000e controllers, before up cursor did nothing and made a bad beep at this case
                            if( GuiVar_RadioTestCommWithET2000e == (true) )
							{
                                CURSOR_Select( CP_RADIO_TEST_2000_ADDR_0, (true) );
                                break;
                            } // if not a 2000e controller test, then it intentionally falls out of the bottom of this case and goes to default.

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_DOWN:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_RADIO_TEST_2000_ADDR_0:
						case CP_RADIO_TEST_2000_ADDR_1:
						case CP_RADIO_TEST_2000_ADDR_2:
							CURSOR_Select( CP_RADIO_TEST_PORT, (true) );
							break;

						default:
							CURSOR_Down( (true) );
					}
					break;

				case KEY_C_LEFT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
                        case CP_RADIO_TEST_PORT:
							// 2/2/2018 Ryan : Makes sure that hitting cursor up and cursor left take us to the same
							// spot for testing with 2000e controllers
                            if( GuiVar_RadioTestCommWithET2000e == (true) )
							{
                                CURSOR_Select( CP_RADIO_TEST_2000_ADDR_0, (true) );
                                break;
                            }	// if not a 2000e controller test, then it intentionally falls out of the bottom of this case and goes to default.

						default:
							CURSOR_Up( (true) );
                    }
                    break;
		
				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;
		
				default:
					if( pkey_event.keycode == KEY_BACK )
					{
						GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;
					}
		
					KEY_process_global_keys( pkey_event );
		
					if( pkey_event.keycode == KEY_BACK )
					{
						// 6/5/2015 ajv : Since the user got here from the COMM OPTIONS dialog box, redraw that
						// dialog box.
						COMM_OPTIONS_draw_dialog( (true) );
					}
			}					

	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

