/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/5/2015 mpd : Required for atoi
#include	<stdlib.h>

#include	<string.h>

#include	"cs_mem.h"

#include	"device_common.h"

#include	"device_EN_PremierWaveXE.h"

#include	"k_process.h"



/* ---------------------------------------------------------- */
/*  			Lantronix Hardware Configuration			  */
/* ---------------------------------------------------------- */

// 4/27/2015 mpd : Development progress has been improved a great deal by the use of a 9-pin D-Sub Y-cable that 
// provides device feedback in a PuTTY window as the SC3000 controller is communitating in real time. The PuTTY
// view is a big improvement over searching RCVD_DATA response buffers in debug to see what the device is doing.
// 
// NOTE: The PremierWave XE connection requires a 9-pin null modem cable. Isolation of the 2 wires needed by PuTTY can 
// be done with the 9-pin, 2-wire (red and black) bridge connector. 
//
// Only 2 wires from the 9-pin D-Sub should connect to the PC COM port through a null modem cable:
// TX	Pin 3	Brown
// Grd	Pin 5	Green
// 
// CONNECTION->SERIAL:
//  Speed: 9600
//  Data Bits: 8
//  Stop Bits: 1
//  Parity: None
//  Flow Control: None


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/4/2015 ajv : The "PW_XE_DETAILS_STRUCT" struct contains fields that specifically apply
// to the PremierWave XE (PW-XE) devices.
static PW_XE_DETAILS_STRUCT PW_XE_details;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/4/2015 ajv : Forward declrations necessary to keep read and write lists at the top of
// the file.
static void PW_XE_analyze_xcr_dump_interface( DEV_STATE_STRUCT *dev_state );

static void PW_XE_write_progress_eth0( DEV_STATE_STRUCT *dev_state );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  					Read and Write Lists  				  */
/* ---------------------------------------------------------- */

// 5/6/2015 mpd : Except for the SHOW commands, PremierWave responses are very short strings
// preceeding the "#" prompt. The termination strings have been chosen with as few unique
// characters as possible to reduce data space. For additional savings, reuse the
// termination string as the title of the response.

// LANTRONIX ENTER CLI MODE INSTRUCTIONS:
// 1. Power off the device.
// 2. Press and hold down the exclamation point "!" key ( DEV_COMMAND_START ).
// 3. Power on the device. 
// 4. After about 1 seconds, a response containing the exclamation point will arrive ( DEV_TERM_START_STR ).
// 5. Type "xyz" within 5 seconds to display the CLI prompt ( performed in the DEV_WAITING_FOR_RESPONSE state using 
//    DEV_COMMAND_CLI ).
// 6. Type "enable" to access further command menus ( DEV_COMMAND_ENABLE ).

const DEV_MENU_ITEM PW_XE_read_list[] =
{ 
	// Response Title             Response Handler        	      Next Command            Next Termination String
	{ DEV_NO_TITLE_STR,   		  NULL,							  DEV_COMMAND_START,      DEV_TERM_START_STR },		// This first entry is used by device exchange initialization.  
	{ DEV_TERM_START_STR,   	  NULL,							  DEV_COMMAND_CLI,        DEV_TERM_PROMPT_STR },	// This second entry is used by the DEV_WAITING_FOR_RESPONSE state.  
	{ DEV_TERM_PROMPT_STR,   	  NULL,					  	      DEV_COMMAND_ENABLE,     DEV_TERM_ENABLE_STR },	// Got to the ENABLE menu. 
	{ DEV_TERM_ENABLE_STR,	      NULL,					 	   	  DEV_COMMAND_CR_DELAY,   DEV_TERM_ENABLE_STR },	// Give the device time to accept commands. 
	{ DEV_TERM_ENABLE_STR,		  NULL,					 	   	  DEV_COMMAND_XML, 	      DEV_TERM_XML_STR },		// Go to the XML menu.
	{ DEV_TERM_XML_STR,		  	  NULL,  						  DEV_COMMAND_X_CD_DUMP,  DEV_TERM_XML_STR },		// Get the XCR dump response. 
																													
	{ DEV_TERM_XML_STR, 		  dev_analyze_xcr_dump_device, 	  DEV_COMMAND_X_ETH0_DUMP,  DEV_TERM_XML_STR },		// Analyze the dump response. Get the XCR interface dump response.
	{ DEV_TERM_XML_STR, 		  PW_XE_analyze_xcr_dump_interface, DEV_COMMAND_EXIT,  		DEV_TERM_ENABLE_STR },	// Analyze the dump response. Get the XCR profile dump response.
																												
	{ DEV_TERM_ENABLE_STR,		  NULL,					 	   	  DEV_COMMAND_SHOW, 	  DEV_TERM_ENABLE_STR },	// Get the SHOW response.
	{ DEV_TERM_ENABLE_STR,		  dev_analyze_enable_show_ip,     DEV_COMMAND_CONFIG,     DEV_TERM_CONFIG_STR },	// Analyze the SHOW response. Go to the CONFIGURE menu. 
	{ DEV_TERM_CONFIG_STR,		  dev_final_device_analysis,      DEV_COMMAND_EXIT,       DEV_TERM_ENABLE_STR },	// Final analysis. Exit CONFIGURE. 
	{ DEV_TERM_ENABLE_STR,		  NULL,							  DEV_COMMAND_EXIT_FINAL, DEV_TERM_NO_RESP_STR },	// Exit ENABLE. There is no response to the final exit. 
	{ DEV_TERM_ENABLE_STR,		  dev_cli_disconnect,			  DEV_COMMAND_END, 	      DEV_TERM_NO_RESP_STR }	// Drop DTR to exit CLI. 
};

const DEV_MENU_ITEM PW_XE_write_list[] =
{  
	// Response Title             Response Handler              Next Command           		 Next Termination String
	{ DEV_NO_TITLE_STR,   		  NULL, 					    DEV_COMMAND_START,           DEV_TERM_START_STR },		// This first entry is used by device exchange initialization.
	{ DEV_TERM_START_STR,   	  NULL,							DEV_COMMAND_CLI,             DEV_TERM_PROMPT_STR },		// This second entry is used by the DEV_WAITING_FOR_RESPONSE state.  
	{ DEV_TERM_PROMPT_STR, 		  NULL,				            DEV_COMMAND_ENABLE,          DEV_TERM_ENABLE_STR },		// Got to the ENABLE menu. 
	{ DEV_TERM_ENABLE_STR,	  	  NULL,					 	   	DEV_COMMAND_CR_DELAY,        DEV_TERM_ENABLE_STR },  	// Give the device time to accept commands. 
	{ DEV_TERM_ENABLE_STR,		  PW_XE_write_progress_eth0,    DEV_COMMAND_CONFIG,          DEV_TERM_CONFIG_STR },		// Go to the CONFIGURE menu. 
																						   
	{ DEV_TERM_CONFIG_STR,		  NULL,				            DEV_COMMAND_IF_ETH,          DEV_TERM_IF_ETH0_STR },	// Go to the eth0 menu. 
	{ DEV_TERM_IF_ETH0_STR,		  NULL,				            DEV_COMMAND_DHCP,            DEV_TERM_IF_ETH0_STR },	// Set DHCP based on user selection. 
	{ DEV_TERM_IF_ETH0_STR,		  NULL,				            DEV_COMMAND_PW_XE_IP_CIDR,   DEV_TERM_IF_ETH0_STR },	// Set IP/CIDR. 
	{ DEV_TERM_IF_ETH0_STR,		  NULL,				            DEV_COMMAND_PW_XE_GATEWAY,   DEV_TERM_IF_ETH0_STR },	// Set default gateway. 
	{ DEV_TERM_IF_ETH0_STR,		  NULL,				            DEV_COMMAND_PW_XE_HOSTNAME,  DEV_TERM_IF_ETH0_STR },	// Set the hostname. 
	{ DEV_TERM_IF_ETH0_STR,		  NULL,			            	DEV_COMMAND_WRITE,      	 DEV_TERM_IF_ETH0_STR },	// Save the config to permanent memory.
	{ DEV_TERM_IF_ETH0_STR,		  NULL,				            DEV_COMMAND_EXIT,  	    	 DEV_TERM_CONFIG_STR },		// Exit eth0. 

	{ DEV_TERM_CONFIG_STR,		  NULL,				            DEV_COMMAND_EXIT,            DEV_TERM_ENABLE_STR },		// Exit CONFIGURE. 
	{ DEV_TERM_ENABLE_STR,		  NULL,							DEV_COMMAND_EXIT_FINAL,      DEV_TERM_NO_RESP_STR },	// Exit ENABLE. There is no response to the final exit. 
	{ DEV_NO_TITLE_STR,			  dev_cli_disconnect,			DEV_COMMAND_END, 	         DEV_TERM_NO_RESP_STR }		// Drop DTR to exit CLI. 

	// 8/4/2015 ajv : There needs to be a reboot or power cycle after this WRITE. Do it here
	// only if the subsequent READ no longer does it.
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/4/2015 ajv : Determine the size of the lists. These will be used later to determine if
// we successfully reached the end.
#define PW_XE_READ_LIST_LEN		(sizeof(PW_XE_read_list) / sizeof(DEV_MENU_ITEM))

#define PW_XE_WRITE_LIST_LEN	(sizeof(PW_XE_write_list) / sizeof(DEV_MENU_ITEM))

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void PW_XE_analyze_xcr_dump_interface( DEV_STATE_STRUCT *dev_state )
{
	char *temp_ptr;
	char  temp_text[ sizeof( DEV_DISABLED_STR ) ];

	PW_XE_DETAILS_STRUCT *PW_XE_details;

	// ----------

	PW_XE_details = dev_state->PW_XE_details;

	// ----------

	// 4/16/2015 mpd : Send progress info to the GUI.
	dev_update_info( dev_state, "Reading Ethernet settings...", DEVICE_EXCHANGE_KEY_read_settings_in_progress );

	// ----------

	// DHCP
	// 10/26/2015 ajv : Default DHCP enabled to TRUE unless it's explicitly disabled
	dev_state->dhcp_enabled = (true);

	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_X_DUMP_DHCP_STR, dev_state->resp_len ) ) != NULL )
	{
		// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
		dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_DUMP_VALUE_STR, DEV_X_DUMP_VALUE_OFFSET, sizeof( temp_text ), temp_text, "DHCP" );

		if( ( strncmp( temp_text, DEV_DISABLED_STR, sizeof( DEV_DISABLED_STR ) ) ) == NULL ) 
		{
			dev_state->dhcp_enabled = (false);
		}
	}

	// ----------

	// IP ADDRESS
	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_X_DUMP_IP_ADD_STR, dev_state->resp_len ) ) != NULL )
	{
		// 9/22/2015 mpd : FogBugz #3163. The XCR dump contains "None" starting at temp_ptr + 43 if no IP address is 
		// set. Check for this before attempting to extract a legitimate IP address (which would fail due to not finding
		// the "<" delimiter within the IP address length).
		if( ( find_string_in_block( temp_ptr, DEV_X_DUMP_NONE_STR, DEV_X_DUMP_NONE_FIND_LEN ) ) == NULL ) 
		{
			// 9/22/2015 mpd : FogBugz #3163. "None" was not found. Continue normal extraction. 

			// 9/24/2015 mpd : FogBugz #3177: IP addresses in CIDR format contain up to 3 extra characters (e.g. 
			// "xxx.xxx.xxx.xxx/24") in the XCR dump response. Current XN and XE parsing code expects a delimiter of 
			// "</value>" at or before byte 15. The extra characters in the response mean that the delimiter will not 
			// be seen and parsing will fail if the IP address contains more than 12 characters. 
			
			// 9/24/2015 mpd : FogBugz #3177: Provide a temporary container to hold the CIDR content.
			char temp_cidr[ DEV_IP_ADD_CIDR_LEN ];
			char *cidr_ptr;

			// 9/24/2015 mpd : FogBugz #3177: Extract to the temporary container instead of directly to the IP address.
			//dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_DUMP_VALUE_STR, DEV_X_DUMP_VALUE_OFFSET, sizeof( PW_XE_details->ip_address ), PW_XE_details->ip_address, "IP" );
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_DUMP_VALUE_STR, DEV_X_DUMP_VALUE_OFFSET, sizeof( temp_cidr ), temp_cidr, "CIDR" );

			// 9/24/2015 mpd : FogBugz #3177: Isolate the IP address before the "/".
			cidr_ptr = strtok ( temp_cidr, DEV_X_DUMP_SLASH_DELIMITER );

			if( cidr_ptr != NULL )
			{
				strlcpy( PW_XE_details->ip_address, cidr_ptr, sizeof( PW_XE_details->ip_address ) );
			}
			else 
			{
				strlcpy( PW_XE_details->ip_address, DEV_NULL_IP_ADD_STR, sizeof( PW_XE_details->ip_address ) );
			}

			// 9/24/2015 mpd : FogBugz #3178: Isolate the net mask bit count before the "<". A subsequent call to 
			// "strtok()" always returns a NULL, so we will have to test the result instead of the pointer for validity.
			cidr_ptr = strtok ( NULL, DEV_X_DUMP_DELIMITER );

			PW_XE_details->mask_bits = atoi( cidr_ptr );

			if( ( PW_XE_details->mask_bits < XE_XN_DEVICE_NETMASK_MIN ) || ( PW_XE_details->mask_bits > XE_XN_DEVICE_NETMASK_MAX ) )
			{
				PW_XE_details->mask_bits = XE_XN_DEVICE_NETMASK_DEFAULT;
			}
		}
		else
		{
			// 9/22/2015 mpd : FogBugz #3163. "None" was found. Skip normal extraction and assign "0.0.0.0". 
			strlcpy( PW_XE_details->ip_address, DEV_NULL_IP_ADD_STR, sizeof( PW_XE_details->ip_address ) );

			// 9/24/2015 mpd : FogBugz #3178: Use the default number of mask bits. 
			PW_XE_details->mask_bits = XE_XN_DEVICE_NETMASK_DEFAULT;
		}

		dev_extract_ip_octets( PW_XE_details->ip_address, PW_XE_details->ip_address_1, PW_XE_details->ip_address_2, PW_XE_details->ip_address_3, PW_XE_details->ip_address_4 );
	}

	// ----------

	// GATEWAY
	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_X_DUMP_DEF_GW_STR, dev_state->resp_len ) ) != NULL )
	{
		// 9/22/2015 mpd : FogBugz #3163. The XCR dump contains "None" starting at temp_ptr + 43 if no GW address is 
		// set. Check for this before attempting to extract a legitimate GW address (which would fail due to not finding
		// the "<" delimiter within the GW address length).
		if( ( find_string_in_block( temp_ptr, DEV_X_DUMP_NONE_STR, DEV_X_DUMP_NONE_FIND_LEN ) ) == NULL ) 
		{
			// 9/22/2015 mpd : FogBugz #3163. "None" was not found. Continue normal extraction. 
			// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_DUMP_VALUE_STR, DEV_X_DUMP_VALUE_OFFSET, sizeof( PW_XE_details->gw_address ), PW_XE_details->gw_address, "GW" );
		}
		else
		{
			// 9/22/2015 mpd : FogBugz #3163. "None" was found. Skip normal extraction and assign "0.0.0.0". 
			strlcpy( PW_XE_details->gw_address, DEV_NULL_IP_ADD_STR, sizeof( PW_XE_details->gw_address ) );
		}

		dev_extract_ip_octets( PW_XE_details->gw_address, PW_XE_details->gw_address_1, PW_XE_details->gw_address_2, PW_XE_details->gw_address_3, PW_XE_details->gw_address_4 );
	}

	// ----------

	// HOSTNAME
	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_X_DUMP_HOSTNAME_STR, dev_state->resp_len ) ) != NULL )
	{
		// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
		dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_DUMP_VALUE_STR, DEV_X_DUMP_VALUE_OFFSET, sizeof( PW_XE_details->dhcp_name ), PW_XE_details->dhcp_name, "NAME" );

		// 6/22/2015 mpd : Determine if the DHCP name has ever been known. This flag prevents us from stomping on an 
		// existing name with the MAC address (the suggested default name).
		if( strlen( PW_XE_details->dhcp_name ) == 0 )
		{
			PW_XE_details->dhcp_name_not_set = (true);
		}
		else
		{
			PW_XE_details->dhcp_name_not_set = (false);
		}
	}
}

/* ---------------------------------------------------------- */
static void PW_XE_write_progress_eth0( DEV_STATE_STRUCT *dev_state )
{
	// 4/16/2015 mpd : Send progress info to the GUI.
	dev_update_info( dev_state, "Writing Ethernet settings...", DEVICE_EXCHANGE_KEY_write_settings_in_progress );

}

/* ---------------------------------------------------------- */
extern UNS_32 PW_XE_sizeof_read_list( void )
{
	return( PW_XE_READ_LIST_LEN ); 
}

/* ---------------------------------------------------------- */
extern UNS_32 PW_XE_sizeof_write_list( void )
{
	return( PW_XE_WRITE_LIST_LEN ); 
}

/* ---------------------------------------------------------- */
extern void PW_XE_PROGRAMMING_copy_programming_into_GuiVars( void )
{
	DEV_DETAILS_STRUCT *dev_details;

	PW_XE_DETAILS_STRUCT *PW_XE_details;

	// ----------

	if( ( dev_state != NULL ) && ( dev_state->dev_details != NULL ) )
	{
		dev_details = dev_state->dev_details;

		if( dev_state->PW_XE_details != NULL )
		{
			PW_XE_details = dev_state->PW_XE_details;

			GuiVar_ENObtainIPAutomatically = dev_state->dhcp_enabled;

			// 5/27/2015 mpd : These WEN GuiVars are shared with EN.
			strlcpy( GuiVar_ENModel, dev_state->model, sizeof( GuiVar_ENModel ) );
			strlcpy( GuiVar_ENFirmwareVer, dev_state->firmware_version, sizeof( GuiVar_ENFirmwareVer ) );
			strlcpy( GuiVar_ENMACAddress, dev_state->serial_number, sizeof( GuiVar_ENMACAddress ) );

			// 9/24/2015 mpd : FogBugz #3178: Neither the Gui side or the device side code cares about the mask string.
			// It's the mask bits that count. Remove this field. 
			//strlcpy( GuiVar_ENSubnetMask, PW_XE_details->mask, sizeof( GuiVar_ENSubnetMask ) );

			e_SHARED_string_validation( GuiVar_ENModel, sizeof( GuiVar_ENModel ) );
			e_SHARED_string_validation( GuiVar_ENFirmwareVer, sizeof( GuiVar_ENFirmwareVer ) );
			e_SHARED_string_validation( GuiVar_ENMACAddress, sizeof( GuiVar_ENMACAddress ) );

			// 9/24/2015 mpd : FogBugz #3178: Neither the Gui side or the device side code cares about the mask string.
			// It's the mask bits that count. Remove this field. 
			//e_SHARED_string_validation( GuiVar_ENSubnetMask, sizeof( GuiVar_ENSubnetMask ) );

			GuiVar_ENDHCPNameNotSet = PW_XE_details->dhcp_name_not_set;

			if( GuiVar_ENDHCPNameNotSet )
			{
				strlcpy( GuiVar_ENDHCPName, DEV_NOT_SET_STR, sizeof( GuiVar_ENDHCPName ) );
			}
			else
			{
				strlcpy( GuiVar_ENDHCPName, PW_XE_details->dhcp_name, sizeof( GuiVar_ENDHCPName ) );
			}

			GuiVar_ENIPAddress_1 = atoi( PW_XE_details->ip_address_1 );
			GuiVar_ENIPAddress_2 = atoi( PW_XE_details->ip_address_2 );
			GuiVar_ENIPAddress_3 = atoi( PW_XE_details->ip_address_3 );
			GuiVar_ENIPAddress_4 = atoi( PW_XE_details->ip_address_4 );

			GuiVar_ENGateway_1 = atoi( PW_XE_details->gw_address_1 );
			GuiVar_ENGateway_2 = atoi( PW_XE_details->gw_address_2 );
			GuiVar_ENGateway_3 = atoi( PW_XE_details->gw_address_3 );
			GuiVar_ENGateway_4 = atoi( PW_XE_details->gw_address_4 );

			// 9/24/2015 mpd : FogBugz #3178: Get the mask bits from the device side. This step was missing.

			// 9/23/2015 mpd : FogBugz #3164: The Lantronix EN UDS1100 does not use CIDR (Classless Inter Domain Routing) format
			// to define the network mask. The number of bits indicates the available bits for host addressing (e.g. A mask of 
			// "8" produces a netmask of "255.255.255.0" which has 8 bits available for hosts on the subnet. Other Lantronix
			// devices use CIDR format for programming (e.g. "192.168.254.150/24" specifies the IP address as well as a network 
			// mask of "255.255.255.0"). The CIDR mask bit value indicates the leading ones; not the trailing zeros.
			// The CS3000 GUI code was written for UDS1100. It's backwards for all newer Lantronix devices. The mask numbers are 
			// internal to CS3000 code. They become external at the GUI and in device programming. To eliminate extensive 
			// rework, the GUI side for all device types will retain pre-CIDR mask values. As mask values pass to and from the 
			// GUI code to the device code (e.g. "e_wen_programming.c" to "device_WEN_PremierWaveXN.c"), the value will be 
			// transformed to match the receiving format.  
			GuiVar_ENNetmask = ( XE_XN_DEVICE_NETMASK_MAX - PW_XE_details->mask_bits );
		}
	}
}

/* ---------------------------------------------------------- */
extern void PW_XE_PROGRAMMING_extract_and_store_GuiVars( void )
{
	DEV_DETAILS_STRUCT	*dev_details;

	PW_XE_DETAILS_STRUCT *PW_XE_details;

	// ----------

	if( ( dev_state != NULL ) && ( dev_state->dev_details != NULL ) && ( dev_state->PW_XE_details != NULL ) )
	{
		dev_details = dev_state->dev_details;
		PW_XE_details = dev_state->PW_XE_details;

		dev_state->dhcp_enabled = GuiVar_ENObtainIPAutomatically;

		// 9/23/2015 mpd : FogBugz #3164: The Lantronix EN UDS1100 does not use CIDR (Classless Inter Domain Routing) format
		// to define the network mask. The number of bits indicates the available bits for host addressing (e.g. A mask of 
		// "8" produces a netmask of "255.255.255.0" which has 8 bits available for hosts on the subnet. Other Lantronix
		// devices use CIDR format for programming (e.g. "192.168.254.150/24" specifies the IP address as well as a network 
		// mask of "255.255.255.0"). The CIDR mask bit value indicates the leading ones; not the trailing zeros.
		// The CS3000 GUI code was written for UDS1100. It's backwards for all newer Lantronix devices. The mask numbers are 
		// internal to CS3000 code. They become external at the GUI and in device programming. To eliminate extensive 
		// rework, the GUI side for all device types will retain pre-CIDR mask values. As mask values pass to and from the 
		// GUI code to the device code (e.g. "e_wen_programming.c" to "device_WEN_PremierWaveXN.c"), the value will be 
		// transformed to match the receiving format.  
		//PW_XE_details->mask_bits = GuiVar_ENNetmask;
		PW_XE_details->mask_bits = ( XE_XN_DEVICE_NETMASK_MAX - GuiVar_ENNetmask );

		// 6/22/2015 mpd : Calsense recommends using the MAC address as the DHCP name. If the user
		// has not set a value in the name field, use the MAC address. Don't override any string
		// value other than "Not Set".
		if( ( strncmp( GuiVar_ENDHCPName, DEV_NOT_SET_STR, strlen( DEV_NOT_SET_STR ) ) == NULL ) &&
			( PW_XE_details->dhcp_name_not_set ) )
		{
			// 9/23/2015 mpd : FogBugz #3175: EN devices will not accept the ":" delimiter during programming when we go 
			// to process the "DEV_COMMAND_PW_XE_HOSTNAME" command. Don't copy them into the DHCP name.
			//strlcpy( PW_XE_details->dhcp_name, dev_state->serial_number, sizeof( PW_XE_details->dhcp_name ) );
			snprintf( PW_XE_details->dhcp_name, sizeof( PW_XE_details->dhcp_name ), "%c%c%c%c%c%c%c%c%c%c%c%c", 
					  dev_state->serial_number[0],  dev_state->serial_number[1], 
					  dev_state->serial_number[3],  dev_state->serial_number[4], 
					  dev_state->serial_number[6],  dev_state->serial_number[7],
					  dev_state->serial_number[9],  dev_state->serial_number[10], 
					  dev_state->serial_number[12], dev_state->serial_number[13],
					  dev_state->serial_number[15], dev_state->serial_number[16] );
		}
		else
		{
			strlcpy( PW_XE_details->dhcp_name, GuiVar_ENDHCPName, sizeof( PW_XE_details->dhcp_name ) );
		}

		snprintf( PW_XE_details->ip_address,   sizeof(PW_XE_details->ip_address),   "%d.%d.%d.%d", GuiVar_ENIPAddress_1, GuiVar_ENIPAddress_2, GuiVar_ENIPAddress_3, GuiVar_ENIPAddress_4 );
		snprintf( PW_XE_details->ip_address_1, sizeof(PW_XE_details->ip_address_1), "%d",          GuiVar_ENIPAddress_1 );
		snprintf( PW_XE_details->ip_address_2, sizeof(PW_XE_details->ip_address_2), "%d",          GuiVar_ENIPAddress_2 );
		snprintf( PW_XE_details->ip_address_3, sizeof(PW_XE_details->ip_address_3), "%d",          GuiVar_ENIPAddress_3 );
		snprintf( PW_XE_details->ip_address_4, sizeof(PW_XE_details->ip_address_4), "%d",          GuiVar_ENIPAddress_4 );

		snprintf( PW_XE_details->gw_address,   sizeof(PW_XE_details->gw_address),   "%d.%d.%d.%d", GuiVar_ENGateway_1, GuiVar_ENGateway_2, GuiVar_ENGateway_3, GuiVar_ENGateway_4 );
		snprintf( PW_XE_details->gw_address_1, sizeof(PW_XE_details->gw_address_1), "%d",          GuiVar_ENGateway_1 );
		snprintf( PW_XE_details->gw_address_2, sizeof(PW_XE_details->gw_address_2), "%d",          GuiVar_ENGateway_2 );
		snprintf( PW_XE_details->gw_address_3, sizeof(PW_XE_details->gw_address_3), "%d",          GuiVar_ENGateway_3 );
		snprintf( PW_XE_details->gw_address_4, sizeof(PW_XE_details->gw_address_4), "%d",          GuiVar_ENGateway_4 );

		// 9/24/2015 mpd : FogBugz #3178: Neither the Gui side or the device side code cares about the mask string.
		// It's the mask bits that count. Remove this field. 
		//strlcpy( PW_XE_details->mask,         GuiVar_ENSubnetMask,  sizeof( PW_XE_details->mask ) );
	}
}

/* ---------------------------------------------------------- */
extern void PW_XE_initialize_detail_struct( DEV_STATE_STRUCT *dev_state )
{
	// 8/4/2015 ajv : Initialize pointers to the device and PW-XE specific detail structures.
	dev_state->dev_details = &dev_details;

	// 8/5/2015 ajv : I'm not entirely sure why this global even exists. Perhaps simply to not
	// need to allocate memory dev_state->PW_XE_details? Regardless, we'll stick with it for
	// now.
	dev_state->PW_XE_details = &PW_XE_details;

	// ----------

	memset( &PW_XE_details, 0x00, sizeof(PW_XE_details) );

	// ----------

	// 8/4/2015 ajv : Determine if the DHCP name has ever been known. This flag prevents us from
	// stomping on an existing name with the MAC address (the suggested default name).
	PW_XE_details.dhcp_name_not_set = (true);
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

