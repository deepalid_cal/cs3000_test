/*  file = device_GR_PremierWaveXC.c          08.05.2014  ajv */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/5/2015 mpd : Required for atoi
#include	<stdlib.h>

#include	<string.h>

#include	"device_GR_PremierWaveXC.h"

#include	"gpio_setup.h"

#include	"alerts.h"

#include	"serial.h"

#include	"configuration_controller.h"

#include	"controller_initiated.h"

#include	"serport_drvr.h"

#include	"device_generic_gr_card.h"

#include	"speaker.h"

#include	"comm_mngr.h"

#include	"rcvd_data.h"

#include	"cs_mem.h"

#include	"device_EN_UDS1100.h"	// Needed for extraction functions.


/* ---------------------------------------------------------- */
/*  			Lantronix Hardware Configuration			  */
/* ---------------------------------------------------------- */

// 4/27/2015 mpd : Development progress has been improved a great deal by the use of a 9-pin D-Sub Y-cable that 
// provides device feedback in a PuTTY window as the SC3000 controller is communitating in real time. The PuTTY
// view is a big improvement over searching RCVD_DATA response buffers in debug to see what the device is doing.
// 
// CONNECTION->SERIAL:
//  Speed: 9600
//  Data Bits: 8
//  Stop Bits: 1
//  Parity: None
//  Flow Control: None
//
// Only 2 wires from the 9-pin D-Sub should connect to the PC COM port through a null modem cable:
// TX	Pin 3	Brown
// Grd	Pin 5	Green

/* ---------------------------------------------------------- */
/*  					Global Structures   				  */
/* ---------------------------------------------------------- */

// 3/23/2015 mpd : NOTE: No functions should directly modify any PVS fields. Use the pointers in the "gr_state" struct! 
 
// 4/8/2015 mpd : The users cannot modify any programmable values on the GR PremierWave radio. There is no need for a
// "dynamic_pvs" for this device.

// 4/8/2015 mpd : All values written to the GR PremierWave radio are set at compile time. There is no need for a
// "write_pvs" for this device.

/* ---------------------------------------------------------- */
/*  			 String Extraction and Analysis 			  */
/* ---------------------------------------------------------- */

// 5/15/2015 mpd : FUTURE: This set of commands will hang until the network is connected. Handle this on the proposed 
// "trouble shooting" screen. 
#if 0
static void gr_analyze_cellular_show_info( DEV_STATE_STRUCT *dev_state )
{
	char *temp_ptr;
	DEV_DETAILS_STRUCT *dev_details = dev_state->dev_details;

	#ifdef DEV_DEBUG_ANALYSIS
	Alert_Message( "gr_analyze_cellular_show_info" );
	#endif

	// 5/6/2015 mpd : The "enable->configure->cellular->show status" response is 329 bytes with no SIM card. 
	
	// 5/8/2015 mpd : With a SIM card in place, any command that involves WWAN0 info will timeout unless we have waited 
	// 30 seconds after the power-cycle for the host to assign an IP address. 

	// 5/7/2015 mpd : The response typically begins with "\r\r\n" which breaks all existing extraction functions. Use 
	// "find_string_in_block()" to get directly to the anchor text string. Then do the extraction.

	// 5/5/2015 mpd : All the possible values of these responses is unknown. Get everything to the end of line.
	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_C_SHOW_SIM_STATUS_STR, dev_state->resp_len ) ) != NULL )
	{
		dev_extract_text_from_buffer( temp_ptr, DEV_C_SHOW_SIM_STATUS_STR, DEV_C_SHOW_SIM_STATUS_OFFSET, sizeof( dev_details->sim_status ), dev_details->sim_status, "SIM" );
	}

	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_C_SHOW_NET_STATUS_STR, dev_state->resp_len ) ) != NULL )
	{
		dev_extract_text_from_buffer( temp_ptr, DEV_C_SHOW_NET_STATUS_STR, DEV_C_SHOW_NET_STATUS_OFFSET, sizeof( dev_details->network_status ), dev_details->network_status, "NET" );
	}

	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_C_SHOW_GPRS_STR, dev_state->resp_len ) ) != NULL )
	{
		dev_extract_text_from_buffer( temp_ptr, DEV_C_SHOW_GPRS_STR, DEV_C_SHOW_GPRS_OFFSET, sizeof( dev_details->packet_domain_status ), dev_details->packet_domain_status, "GPRS" );
	}

	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_C_SHOW_RSSI_STR, dev_state->resp_len ) ) != NULL )
	{
		dev_extract_text_from_buffer( temp_ptr, DEV_C_SHOW_RSSI_STR, DEV_C_SHOW_RSSI_OFFSET, sizeof( dev_details->signal_strength ), dev_details->signal_strength, "RSSI" );
	}

}

static void gr_analyze_link_show_info( DEV_STATE_STRUCT *dev_state )
{
	char *temp_ptr;
	DEV_DETAILS_STRUCT *dev_details = dev_state->dev_details;

	#ifdef DEV_DEBUG_ANALYSIS
	Alert_Message( "gr_analyze_link_show_info" );
	#endif

	// 5/6/2015 mpd : The "enable->configure->cellular->show status" response is 329 bytes with no SIM card. 
	
	// 5/7/2015 mpd : The response typically begins with "\r\r\n" which breaks all existing extraction functions. Use 
	// "find_string_in_block()" to get directly to the anchor text string. Then do the extraction.

	// 5/5/2015 mpd : Get the APN string.
	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_L_SHOW_APN_STR, dev_state->resp_len ) ) != NULL )
	{
		dev_extract_text_from_buffer( temp_ptr, DEV_L_SHOW_APN_STR, DEV_L_SHOW_APN_OFFSET, sizeof( dev_details->apn ), dev_details->apn, "APN" );
	}

}
#endif

/* ---------------------------------------------------------- */
/*  					Read and Write Lists  				  */
/* ---------------------------------------------------------- */

// 5/6/2015 mpd : Except for the SHOW commands, GR/WEN responses are very short strings preceeding the "#" prompt. The 
// termination strings have been chosen with as few unique characters as possible to reduce data space. For additional
// savings, reuse the termination string as the title of the response. 

// LANTRONIX ENTER CLI MODE INSTRUCTIONS:
// 1. Power off the device.
// 2. Press and hold down the exclamation point "!" key.
// 3. Power on the device. 
// 4. After about 1 seconds, a response containing the exclamation point will arrive.
// 5. Type "xyz" within 5 seconds to display the CLI prompt.
// 6. Type "enable" to access further command menus.

const DEV_MENU_ITEM gr_xml_read_list[] = { 
	// Response Title             Response Handler        	      Next Command            Next Termination String
	{ DEV_NO_TITLE_STR,   		  NULL,							  DEV_COMMAND_START,      DEV_TERM_START_STR },		// This first entry is used by device exchange initialization.
	{ DEV_TERM_START_STR,   	  NULL,							  DEV_COMMAND_CLI,        DEV_TERM_PROMPT_STR },	// This second entry is used by the DEV_WAITING_FOR_RESPONSE state.  
	{ DEV_TERM_PROMPT_STR,  	  NULL,					  	      DEV_COMMAND_ENABLE,     DEV_TERM_ENABLE_STR },	// Got to the ENABLE menu. 
	//{ DEV_TERM_ENABLE_STR,	  dev_wait_for_network,	 	   	  DEV_COMMAND_CR, 	      DEV_TERM_ENABLE_STR },	// Wait for a network connection. 
	{ DEV_TERM_ENABLE_STR,	  	  NULL,					 	   	  DEV_COMMAND_CR_DELAY,   DEV_TERM_ENABLE_STR },	// Give the device time to accept commands. 
	{ DEV_TERM_ENABLE_STR,		  NULL,					 	   	  DEV_COMMAND_XML, 	      DEV_TERM_XML_STR },		// Go to the XML menu.
	{ DEV_TERM_XML_STR,		  	  NULL,  						  DEV_COMMAND_X_CD_DUMP,  DEV_TERM_XML_STR },		// Get the XCR dump response. 
	{ DEV_TERM_XML_STR,			  dev_analyze_xcr_dump_device, 	  DEV_COMMAND_EXIT,       DEV_TERM_ENABLE_STR },	// Analyze the dump response. Exit XML. 
																												
	// 5/15/2015 mpd : FUTURE: This set of commands will hang until the network is connected. Handle this on the proposed 
	// "trouble shooting" screen. 
	#if 0
	{ DEV_TERM_ENABLE_STR,		  NULL,					 	   	  DEV_COMMAND_SHOW, 	  DEV_TERM_ENABLE_STR },	// Get the SHOW response.
	{ DEV_TERM_ENABLE_STR,		  dev_analyze_enable_show_ip,     DEV_COMMAND_CONFIG,     DEV_TERM_CONFIG_STR },	// Analyze the SHOW response. Go to the CONFIGURE menu. 
	{ DEV_TERM_CONFIG_STR,		  NULL,						      DEV_COMMAND_CELL,       DEV_TERM_CELL_STR },		// Go to the CELLULAR menu. 
	{ DEV_TERM_CELL_STR,		  NULL,						      DEV_COMMAND_SHOW_S,     DEV_TERM_CELL_STR },		// Get the SHOW STATUS response. 
	{ DEV_TERM_CELL_STR,		  gr_analyze_cellular_show_info,  DEV_COMMAND_EXIT,       DEV_TERM_CONFIG_STR },	// Analyze the SHOW STATUS response. Exit CELLULAR. 
	{ DEV_TERM_CONFIG_STR,		  NULL,				              DEV_COMMAND_IF_WWAN,    DEV_TERM_IF_WWAN_STR },	// Go to the WWAN0 menu. 
	{ DEV_TERM_IF_WWAN_STR,		  NULL,				              DEV_COMMAND_LINK,       DEV_TERM_LINK_STR },		// Go to the LINK menu. 
	{ DEV_TERM_LINK_STR,		  NULL,				              DEV_COMMAND_SHOW,       DEV_TERM_LINK_STR },		// Get the SHOW response. 
	{ DEV_TERM_LINK_STR,		  gr_analyze_link_show_info,      DEV_COMMAND_EXIT,       DEV_TERM_IF_WWAN_STR },	// Exit LINK. 
	{ DEV_TERM_IF_WWAN_STR,		  NULL,				              DEV_COMMAND_EXIT,       DEV_TERM_CONFIG_STR },	// Exit WWAN0. 
	{ DEV_TERM_CONFIG_STR,		  dev_final_device_analysis,      DEV_COMMAND_EXIT,       DEV_TERM_ENABLE_STR },	// Final analysis. Exit CONFIGURE. 
	#endif

	// 5/11/2015 mpd : The DTR signal is disabled in CLI mode. Exiting the CLI with the "exit" command leaves the DTR
	// disabled. Exit with the "reload" command so DTR is functional. 
	//{ DEV_TERM_ENABLE_STR,	  NULL,				              DEV_COMMAND_RELOAD,     DEV_TERM_RELOAD_STR },	// Reload (reboot). 
	//{ DEV_TERM_RELOAD_STR,	  NULL, 						  DEV_COMMAND_YES,    	  DEV_TERM_EXIT_STR }		// Confirm reload. Ignore the response to this final command.
	//{ DEV_TERM_RELOAD_STR,	  NULL, 						  DEV_COMMAND_YES,    	  DEV_TERM_REBOOT_STR },	// Confirm reload. 
	//{ DEV_TERM_REBOOT_STR,	  NULL,							  DEV_COMMAND_END,        DEV_TERM_EXIT_STR }		// End of list. 

	{ DEV_TERM_ENABLE_STR,		  dev_final_device_analysis,	  DEV_COMMAND_EXIT_FINAL, DEV_TERM_NO_RESP_STR },	// Exit ENABLE. There is no response to the final exit. 
	{ DEV_TERM_ENABLE_STR,		  dev_cli_disconnect,			  DEV_COMMAND_END, 	      DEV_TERM_NO_RESP_STR }	// Drop DTR to exit CLI. 
};

#define GR_ENABLE_LOGGING

const DEV_MENU_ITEM gr_xml_write_list[] = {  
	// 6/10/2015 mpd : FUTURE: Save this for the diagnostic screen. It is functional.
	#if 0
	// Response Title             Response Handler              Next Command           Next Termination String
	{ DEV_NO_TITLE_STR,   		  NULL, 					    DEV_COMMAND_START,       DEV_TERM_START_STR },		// This first entry is used by device exchange initialization.
	{ DEV_TERM_START_STR,   	  NULL,							DEV_COMMAND_CLI,         DEV_TERM_PROMPT_STR },		// This second entry is used by the DEV_WAITING_FOR_RESPONSE state.  
	{ DEV_TERM_PROMPT_STR, 		  NULL,				            DEV_COMMAND_ENABLE,      DEV_TERM_ENABLE_STR },		// Got to the ENABLE menu. 
	{ DEV_TERM_ENABLE_STR,	  	  NULL,					 	   	DEV_COMMAND_CR_DELAY,    DEV_TERM_ENABLE_STR },  	// Give the device time to accept commands. 
	{ DEV_TERM_ENABLE_STR,		  dev_device_write_progress,    DEV_COMMAND_CONFIG,      DEV_TERM_CONFIG_STR },		// Go to the CONFIGURE menu. 

	// 5/13/2015 mpd : The response for this setting takes close to 30 seconds with a SIM card installed.
	//{ DEV_TERM_CONFIG_STR,	  NULL,				            DEV_COMMAND_CELL,        DEV_TERM_CELL_STR },		// Go to the CELLULAR menu. 
	//{ DEV_TERM_CELL_STR,		  NULL,				            DEV_COMMAND_ANTENNA,     DEV_TERM_CELL_STR },		// Disable antenna diversity. 
	//{ DEV_TERM_CELL_STR,		  NULL,			 	            DEV_COMMAND_EXIT,        DEV_TERM_CONFIG_STR },		// Exit CELLULAR. 
																						 
	{ DEV_TERM_CONFIG_STR,		  NULL,				            DEV_COMMAND_CLOCK,       DEV_TERM_CLOCK_STR },		// Go to the CLOCK menu. 
	{ DEV_TERM_CLOCK_STR,		  NULL,				            DEV_COMMAND_SYNC,        DEV_TERM_CLOCK_STR },		// Sync the clock using network. 
	{ DEV_TERM_CLOCK_STR,		  NULL,			 	            DEV_COMMAND_EXIT,        DEV_TERM_CONFIG_STR },		// Exit CLOCK. 
																						 
	#ifdef GR_ENABLE_LOGGING                                                            
	{ DEV_TERM_CONFIG_STR,		  NULL,			 	            DEV_COMMAND_DIAG,        DEV_TERM_DIAG_STR },		// Go to the DIAGNOSTICS menu. 
	{ DEV_TERM_DIAG_STR,		  NULL,			 	            DEV_COMMAND_LOG,         DEV_TERM_LOG_STR },		// Go to the LOG menu.
	{ DEV_TERM_LOG_STR,			  NULL,			 	            DEV_COMMAND_OUTPUT,      DEV_TERM_LOG_STR },		// Set filesystem output.
	{ DEV_TERM_LOG_STR,			  NULL,			 	            DEV_COMMAND_LENGTH,      DEV_TERM_LOG_STR },		// Set max length.
	{ DEV_TERM_LOG_STR,		      NULL,			 	            DEV_COMMAND_EXIT,        DEV_TERM_DIAG_STR },		// Exit LOG. 
	{ DEV_TERM_DIAG_STR,		  NULL,			 	            DEV_COMMAND_EXIT,        DEV_TERM_CONFIG_STR },		// Exit DIAGNOSTICS. 
	#endif											               					     						
																						 
	// 5/13/2015 mpd : These settings can't be set in CLI on the target port. They drop the connection.
	//{ DEV_TERM_CONFIG_STR,	  NULL,			 	            DEV_COMMAND_EXIT,        DEV_TERM_ENABLE_STR },		// Exit CONFIGURE. 
	//{ DEV_TERM_ENABLE_STR,	  NULL,				            DEV_COMMAND_LINE,        DEV_TERM_LINE_STR },		// Go to the LINE:1 menu. 
	//{ DEV_TERM_LINE_STR,		  NULL,			            	DEV_COMMAND_BAUD,        DEV_TERM_LINE_STR },		// Set the baud rate. <-  This command hangs the interface
	//{ DEV_TERM_LINE_STR,		  NULL,			            	DEV_COMMAND_FLOW,        DEV_TERM_LINE_STR },		// Set flow control.  <-  This command hangs the interface
	//{ DEV_TERM_LINE_STR,		  NULL,  						DEV_COMMAND_EXIT,        DEV_TERM_ENABLE_STR },		// Exit LINE:1.
	//{ DEV_TERM_ENABLE_STR,	  NULL,				            DEV_COMMAND_CONFIG,      DEV_TERM_CONFIG_STR },		// Go to the CONFIGURE menu. 
																						 
	{ DEV_TERM_CONFIG_STR,		  NULL,				            DEV_COMMAND_IF_ETH,      DEV_TERM_IF_ETH_STR },		// Go to the ETH0 menu. 
	{ DEV_TERM_IF_ETH_STR,		  NULL,				            DEV_COMMAND_PRIORITY,    DEV_TERM_IF_ETH_STR },		// Set priority 2. 
	{ DEV_TERM_IF_ETH_STR,		  NULL,			            	DEV_COMMAND_WRITE,       DEV_TERM_IF_ETH_STR },		// Save the config to permanent memory.
	{ DEV_TERM_IF_ETH_STR,		  NULL,				            DEV_COMMAND_EXIT,  	     DEV_TERM_CONFIG_STR },		// Exit ETH0. 
																						 
	{ DEV_TERM_CONFIG_STR,		  NULL,				            DEV_COMMAND_IF_WWAN,     DEV_TERM_IF_WWAN_STR },	// Go to the WWAN0 menu. 
	{ DEV_TERM_IF_WWAN_STR,		  NULL,				            DEV_COMMAND_LINK,        DEV_TERM_LINK_STR },		// Go to the LINK menu. 
	{ DEV_TERM_LINK_STR,		  NULL,				            DEV_COMMAND_APN,         DEV_TERM_LINK_STR },		// Set APN. 
	{ DEV_TERM_LINK_STR,		  NULL,			            	DEV_COMMAND_WRITE,       DEV_TERM_LINK_STR },		// Save the config to permanent memory.
	{ DEV_TERM_LINK_STR,		  NULL,				            DEV_COMMAND_EXIT,        DEV_TERM_IF_WWAN_STR },	// Exit LINK. 
	{ DEV_TERM_IF_WWAN_STR,		  NULL,				            DEV_COMMAND_EXIT,        DEV_TERM_CONFIG_STR },		// Exit WWAN0. 
	{ DEV_TERM_CONFIG_STR,		  NULL,				            DEV_COMMAND_EXIT,        DEV_TERM_ENABLE_STR },		// Exit CONFIGURE. 

	{ DEV_TERM_ENABLE_STR,		  NULL,				            DEV_COMMAND_TUNNEL,      DEV_TERM_TUNNEL_STR },		// Go to TUNNEL:1 menu. 
	{ DEV_TERM_TUNNEL_STR,		  NULL,				            DEV_COMMAND_ACCEPT,      DEV_TERM_ACCEPT_STR },		// Go to ACCEPT menu. 
	{ DEV_TERM_ACCEPT_STR,		  NULL,				            DEV_COMMAND_A_MODE,      DEV_TERM_ACCEPT_STR },		// Disable accept mode. 
	{ DEV_TERM_ACCEPT_STR,		  NULL,			            	DEV_COMMAND_WRITE,       DEV_TERM_ACCEPT_STR },		// Save the config to permanent memory.
	{ DEV_TERM_ACCEPT_STR,		  NULL,				            DEV_COMMAND_EXIT,        DEV_TERM_TUNNEL_STR },		// Exit ACCEPT. 

	{ DEV_TERM_TUNNEL_STR,		  NULL,				            DEV_COMMAND_CONNECT,     DEV_TERM_CONN_STR },		// Go to CONNECT menu. 
	{ DEV_TERM_CONN_STR,		  NULL,				            DEV_COMMAND_C_MODE,      DEV_TERM_CONN_STR },		// Set connect mode always. 
	{ DEV_TERM_CONN_STR,		  NULL,			            	DEV_COMMAND_WRITE,       DEV_TERM_CONN_STR },		// Save the config to permanent memory.
	{ DEV_TERM_CONN_STR,		  NULL,				            DEV_COMMAND_HOST,        DEV_TERM_HOST_STR },		// Go to HOST:1 menu. 
	{ DEV_TERM_HOST_STR,		  NULL,				            DEV_COMMAND_ADDRESS,     DEV_TERM_HOST_STR },		// Set host IP address. 
	{ DEV_TERM_HOST_STR,		  NULL,				            DEV_COMMAND_PORT,        DEV_TERM_HOST_STR },		// Set host port. 
	{ DEV_TERM_HOST_STR,		  NULL,				            DEV_COMMAND_ALIVE,       DEV_TERM_HOST_STR },		// Set keep alive time. <- This is currently in msec. 
	{ DEV_TERM_HOST_STR,		  NULL,			            	DEV_COMMAND_WRITE,       DEV_TERM_HOST_STR },		// Save the config to permanent memory.
	{ DEV_TERM_HOST_STR,		  NULL,				            DEV_COMMAND_EXIT,        DEV_TERM_CONN_STR },		// Exit HOST:1. 
	{ DEV_TERM_CONN_STR,		  NULL,				            DEV_COMMAND_EXIT,        DEV_TERM_TUNNEL_STR },		// Exit CONNECT. 

	{ DEV_TERM_TUNNEL_STR,		  NULL,				            DEV_COMMAND_DISCONNECT,  DEV_TERM_DISCONN_STR },	// Go to DISCONNECT menu. 
	{ DEV_TERM_DISCONN_STR,		  NULL,				            DEV_COMMAND_D_MODE,	     DEV_TERM_DISCONN_STR },	// Enable modem control. 
	{ DEV_TERM_DISCONN_STR,		  NULL,			            	DEV_COMMAND_WRITE,       DEV_TERM_DISCONN_STR },	// Save the config to permanent memory.
	{ DEV_TERM_DISCONN_STR,		  NULL,				            DEV_COMMAND_EXIT,        DEV_TERM_TUNNEL_STR },		// Exit DISCONNECT. 

	{ DEV_TERM_TUNNEL_STR,		  NULL,				            DEV_COMMAND_MODEM,	     DEV_TERM_MODEM_STR },		// Go to MODEM menu. 
	{ DEV_TERM_MODEM_STR,		  NULL,				            DEV_COMMAND_ECHO,	     DEV_TERM_MODEM_STR },		// Disable echo commands. 
	{ DEV_TERM_MODEM_STR,		  NULL,				            DEV_COMMAND_VERBOSE,     DEV_TERM_MODEM_STR },		// Disable verbose response. 
	{ DEV_TERM_MODEM_STR,		  NULL,			            	DEV_COMMAND_WRITE,       DEV_TERM_MODEM_STR },		// Save the config to permanent memory.
	{ DEV_TERM_MODEM_STR,		  NULL,				            DEV_COMMAND_EXIT,        DEV_TERM_TUNNEL_STR },		// Exit MODEM. 
	{ DEV_TERM_TUNNEL_STR,		  NULL,				            DEV_COMMAND_EXIT,        DEV_TERM_ENABLE_STR },		// Exit TUNNEL. 

	// 5/13/2015 mpd : DEBUG CODE: Changing the rate of the port we are on is not working. Try the other port to see if
	// the change makes it through a reboot. It does. We could use this for manufacturing to program Line 1 from the 
	// Line 2 port. Note that the baud rate change on the other port does not survive a reboot unless a "write" command
	// preceeds the CLI exit.
	//{ DEV_TERM_ENABLE_STR,	  NULL,				            DEV_COMMAND_LINE_2,      DEV_TERM_LINE_2_STR },		// Go to the LINE:2 menu. 
	//{ DEV_TERM_LINE_2_STR,	  NULL,			            	DEV_COMMAND_BAUD,        DEV_TERM_LINE_2_STR },		// Set the baud rate. <-  This command hangs the interface
	//{ DEV_TERM_LINE_2_STR,	  dev_mid_stream_baud_change,  	DEV_COMMAND_WRITE,       DEV_TERM_LINE_2_STR },		// Save the config to permanent memory.
	//{ DEV_TERM_LINE_2_STR,	  NULL,  						DEV_COMMAND_EXIT,        DEV_TERM_ENABLE_STR },		// Exit LINE:2.
	//{ DEV_TERM_ENABLE_STR,	  NULL, 						DEV_COMMAND_EXIT,        DEV_TERM_EXIT_STR }		// Exit ENABLE. There is no response to the final exit. 

	// 5/15/2015 mpd : Baud rate changes to the GR are immediate. The UART speed must be changed to match the new device
	// speed immediately after the DEV_COMMAND_BAUD command. That's the story from Lantronix. In practice, the baud
	// chance has not even worked in PuTTY. Various speeds have been attempted with various flow control settings. No
	// luck. The device DTR signal (our DCD) goes high about 9 seconds after the baud rate command. That means we were
	// dropped out of CLI mode a few seconds earlier while trying to match the new speed with PuTTY configuration 
	// changes.
	//{ DEV_TERM_ENABLE_STR,	  NULL,				            DEV_COMMAND_LINE,	     DEV_TERM_LINE_STR },		// Go to the LINE:1 menu. 
	//{ DEV_TERM_LINE_STR,		  NULL,			            	DEV_COMMAND_BAUD,        DEV_TERM_NO_RESP_STR },	// Set the baud rate. <-  This command hangs the interface
	//{ DEV_NO_TITLE_STR,		  dev_mid_stream_baud_change,  	DEV_COMMAND_WRITE_FINAL, DEV_TERM_NO_RESP_STR },	// Use the new baud rate. Save the config to permanent memory.
	//{ DEV_NO_TITLE_STR,		  NULL,  						DEV_COMMAND_EXIT,        DEV_TERM_ENABLE_STR },		// Exit LINE:1.

	{ DEV_TERM_ENABLE_STR,		  NULL,							DEV_COMMAND_EXIT_FINAL,  DEV_TERM_NO_RESP_STR },	// Exit ENABLE. There is no response to the final exit. 
	{ DEV_NO_TITLE_STR,			  dev_cli_disconnect,			DEV_COMMAND_END, 	     DEV_TERM_NO_RESP_STR }		// Drop DTR to exit CLI. 

	// 5/14/2015 mpd : There needs to be a reboot or power cycle after this WRITE. Do it here only if the 
	// subsequent READ no longer does it.
	#endif
};

// 4/8/2015 mpd : Determine the size of the lists. These will be used later to determine if we successfully reached 
// the end.
#define GR_READ_LIST_LEN		( sizeof( gr_xml_read_list )  / sizeof( DEV_MENU_ITEM ) )
#define GR_WRITE_LIST_LEN	    ( sizeof( gr_xml_write_list ) / sizeof( DEV_MENU_ITEM ) )

extern UNS_32 gr_sizeof_read_list()
{
	return( GR_READ_LIST_LEN ); 
}

extern UNS_32 gr_sizeof_write_list()
{
	return( GR_WRITE_LIST_LEN ); 
}

/* ---------------------------------------------------------- */
/*  						  Help Function 				  */
/* ---------------------------------------------------------- */

void gr_programming_help( void ) {

//UNS_16 i = 0;

	//help.text_ptrs[i++] = "";
	//help.text_ptrs[i++] = "This screen allows you to program or";
	//help.text_ptrs[i++] = "reprogram an GR device attached to your";
	//help.text_ptrs[i++] = "controller.";
	//help.text_ptrs[i++] = "";
	//help.text_ptrs[i++] = "This feature is not recommended for";
	//help.text_ptrs[i++] = "end users. If a device needs to be";
	//help.text_ptrs[i++] = "reprogrammed, please contact your";
	//help.text_ptrs[i++] = "local Calsense Field Service";
	//help.text_ptrs[i++] = "Representative who will program the";
	//help.text_ptrs[i++] = "device for you.";
	//help.text_ptrs[i++] = "";

	//HELP_prepare_for( "GR RADIO PROGRAMMING HELP", i );
	//HELP_draw_it();								 
	
}

/* ---------------------------------------------------------- */
/*  			    Structure Initialization				  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void gr_initialize_detail_struct( DEV_STATE_STRUCT *dev_state )
{
	//Alert_Message( "gr_initialize_detail_struct" );

	dev_state->dev_details = &dev_details;

	// 4/21/2015 mpd : Clear all the string values.
	strlcpy( dev_details.ip_address,           "", sizeof( dev_details.ip_address ) );                
												
	strlcpy( dev_details.sim_status,           "", sizeof( dev_details.sim_status ) );                
	strlcpy( dev_details.network_status,       "", sizeof( dev_details.network_status ) );          
	strlcpy( dev_details.packet_domain_status, "", sizeof( dev_details.packet_domain_status ) );           
	strlcpy( dev_details.signal_strength,      "", sizeof( dev_details.signal_strength ) );   
												
	strlcpy( dev_details.apn,                  "", sizeof( dev_details.apn ) );   

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/14/2017 rmd : This first two states are no longer used.
#define	nlu_PWXC_STATE_initial_wait_while_not_powered	(1)

#define	nlu_PWXC_STATE_powerup_wait						(2)

#define	PWXC_STATE_waiting_for_connection				(3)

typedef struct
{
	UNS_32	state;
	
	UNS_32	port;
	
	UNS_32	wait_count;
	
} PWXC_control_structure;

static PWXC_control_structure	pwxc_cs;

/* ---------------------------------------------------------- */
extern void PWXC_initialize_the_connection_process( UNS_32 pport )
{
	if( pport != UPORT_A )
	{
		Alert_Message( "Why is the PremierWave XC not on PORT A?" );
	}

	// 11/11/2016 rmd : For the UDS1100 we use the CD output to determine if connected. So check
	// to make sure that function is not NULL. Otherwise we couldn't call it and the connection
	// process would error out. Over and over again.
	if( port_device_table[ config_c.port_A_device_index ].__is_connected == NULL )
	{
		Alert_Message( "Unexpected NULL: is_connected function" );

		// 2/15/2017 rmd : I think there is no point to trying to send messages anyway. They won't
		// go because we don't have a valid 'is connected' function! So do nothing in the face of
		// this error.
	}
	else
	{
		pwxc_cs.port = pport;
		
		pwxc_cs.wait_count = 0;
		
		// ----------
		
		// 4/14/2017 rmd : We can enter the connection sequence either from first boot, or because
		// of a detected disconnect, or because of a message failure. That translates to the unit
		// could be powered or not. If it is powered LEAVE IT POWERED and wait 15 seconds. Why not
		// power it down? Because the disconnect could be the result of an over the air
		// reprogramming attempt of the Lantronix XC. If we were to power down the unit, the save to
		// flash, in the XC, will fail.
		if( GENERIC_GR_card_power_is_on( pport ) )
		{
			// 4/14/2017 rmd : The unit is powered up. Wait 15 seconds. And begin looking for CD to
			// indicate a connection.
			xTimerChangePeriod( cics.process_timer, MS_to_TICKS(15000), portMAX_DELAY );
		
			pwxc_cs.state = PWXC_STATE_waiting_for_connection;
		}
		else
		{
			// 4/14/2017 rmd : The unit needs to be powered up. This is the case if this were the first
			// time trying to connect after booting, or if we powered down the device because the
			// previous connection attempt failed.
			power_up_device( pwxc_cs.port );
			
			// 4/14/2017 rmd : Wait 15 seconds. Then begin looking for CD to indicate a connection. Why
			// wait 15 seconds. To bypass any transient CD behavior during the device boot process that
			// could be picked up as a false connection. I have seen some devices do this when first
			// powered on. Don't know if this one does, but doesn't hurt to wait. Takes at least 30
			// seconds to connect anyway.
			xTimerChangePeriod( cics.process_timer, MS_to_TICKS(15000), portMAX_DELAY );
		
			pwxc_cs.state = PWXC_STATE_waiting_for_connection;
		}
	}
}

/* ---------------------------------------------------------- */
extern void PWXC_connection_processing( UNS_32 pevent )
{
	BOOL_32	lerror;
	
	lerror = (false);
	
	// ----------
	
	if( (pevent != CI_EVENT_process_string_found) && (pevent != CI_EVENT_process_timer_fired) )
	{
		// 10/29/2016 rmd : ERROR.
		Alert_Message( "Connection Process : UNXEXP EVENT" );
		
		lerror = (true);
	}
	else
	{
		switch( pwxc_cs.state )
		{
			case PWXC_STATE_waiting_for_connection:
				if((port_device_table[ config_c.port_A_device_index ].__is_connected != NULL) )
				{
					if( port_device_table[ config_c.port_A_device_index ].__is_connected( UPORT_A ) )
					{
						// 11/11/2016 rmd : Okay we're done. Device connected. Update the Comm Test string so the
						// user can see the current status. And alert.
						strlcpy( GuiVar_CommTestStatus, "Cell Connection", sizeof(GuiVar_CommTestStatus) );
					
						Alert_Message( "PremierWave XC Cell Connected" );
						
						// ----------
						
						CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities();
					}
					else
					{
						// 8/1/2018 rmd : There have been reports of this device taking 2 minutes 15 seconds to
						// connect, specifically Don Keeton says this. So make this 4 minutes to make double sure we
						// are waiting long enough before trying again.
						if( pwxc_cs.wait_count >= (4 * 60) )
						{
							// 11/11/2016 rmd : We've been waiting 4 minutes. This cell XC unit normally connects in
							// about 45 seconds. So power down the unit, and restart the connection process 2 minutes
							// from now (unless we've been trying and trying ... that 2 minutes grows).
							lerror = (true);
						}
						else
						{
							pwxc_cs.wait_count += 1;
							
							xTimerChangePeriod( cics.process_timer, MS_to_TICKS(1000), portMAX_DELAY );
						}
					}
				}
				else
				{
					// 11/11/2016 rmd : The sequence will restart and we have a test in the initialization
					// function for the NULL function.
					lerror = (true);
				}
				break;
				
		}  // of the switch statement
		
	}  // of an UNK event.


	if( lerror )
	{
		// 4/14/2017 rmd : So the connection attempt failed. This is when we power down the unit to
		// cause a cold start.
		power_down_device( pwxc_cs.port );

		// ----------
		
		// 2/3/2017 rmd : Okay well the mode is no longer 'connecting'. And it shouldn't be 'ready'.
		// So set it to 'waiting to connect'. Doing so ensures when the connection timer fires and
		// we try to start a connection sequence we really to start it. If the mode were still
		// 'connecting' it is possible to not re-start the sequence.
		cics.mode = CI_MODE_waiting_to_connect;
		
		// 10/29/2016 rmd : Send the connection error event. To start the sequence over.
		CONTROLLER_INITIATED_post_event( CI_EVENT_connection_process_error );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

