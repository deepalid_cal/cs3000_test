/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_DEVICE_GR_MULTITECH_MTSMC_LTE_H
#define INC_DEVICE_GR_MULTITECH_MTSMC_LTE_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void MTSMC_LAT_initialize_the_connection_process( UNS_32 pport );

extern void MTSMC_LAT_connection_processing( UNS_32 pevent );

extern void MTSMC_LAT_initialize_the_disconnect_process( UNS_32 pport );

extern void MTSMC_LAT_disconnect_processing( UNS_32 pevent );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

