/*  file = device_GR_AIRLINK.c                      2014  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for atoi
#include	<stdlib.h>

// 6/18/2014 ajv : Required for strlen
#include	<string.h>

#include	"device_gr_airlink.h"

#include	"gpio_setup.h"

#include	"alerts.h"

#include	"serial.h"

#include	"configuration_controller.h"

#include	"serport_drvr.h"

#include	"device_generic_gr_card.h"

#include	"comm_mngr.h"

#include	"rcvd_data.h"

#include	"cs_mem.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/18/2014 rmd : For the initialization function refer to the start define. So there is no
// confusion about where the satte machine starts.
#define		GPRS_DEVEX_read_start		(0)
#define		GPRS_DEVEX_read_model		(GPRS_DEVEX_read_start)
#define		GPRS_DEVEX_read_netphone	(1)
#define		GPRS_DEVEX_read_netip		(2)
#define		GPRS_DEVEX_read_sim			(3)
#define		GPRS_DEVEX_read_netstate	(4)
#define		GPRS_DEVEX_read_carrier		(5)
#define		GPRS_DEVEX_read_service		(6)
#define		GPRS_DEVEX_read_rssi		(7)
#define		GPRS_DEVEX_read_ecio		(8)
#define		GPRS_DEVEX_read_sernum		(9)
#define		GPRS_DEVEX_read_fwrev		(10)
#define		GPRS_DEVEX_read_finish		(11)


#define		GPRS_DEVEX_write_start		(0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void __setup_for_crlf_delimited_string_hunt( const char* pcommand_str )
{
	// 3/14/2014 rmd : This function executed within the context of the comm_mngr task. We can
	// reference and change the device exchange variables as needed.

	DATA_HANDLE	ldh;
	
	// 2/13/2014 rmd : Clean and restart the port buffer and string hunt. Note this disables
	// packet hunting mode.
	RCVD_DATA_enable_hunting_mode( comm_mngr.device_exchange_port, PORT_HUNT_FOR_CRLF_DELIMITED_STRING, WHEN_STRING_FOUND_NOTIFY_THE_COMM_MNGR_TASK );
	
	// BE 8/8/17: This is needed after modification of search function with this check. (the
	// change in the search function allows search with and without the first CRLF.
	//
	// 8/7/2018 rmd : Bashir added this line, and I don't believe it was ever tested, so this
	// may, or may not work, for the AIRLINK GR device. As of this writing though, we have ZERO
	// of these devices deployed, so I am not too concerned, and will leave this statement.
	SerDrvrVars_s[ UPORT_B ].UartRingBuffer_s.sh.find_initial_CRLF = true; 

	// ----------
	
	ldh.dptr = (UNS_8*)pcommand_str;
	
	ldh.dlen = strlen( pcommand_str );

	AddCopyOfBlockToXmitList( comm_mngr.device_exchange_port, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );
}

/* ---------------------------------------------------------- */
extern void GR_AIRLINK_initialize_device_exchange( void )
{
	// 3/14/2014 rmd : This function executed within the context of the comm_mngr task. We can
	// reference and change the device exchange variables as needed.

	// 3/14/2014 rmd : Kick off the exchange by setting the state variable and starting the
	// timer.
	if( comm_mngr.device_exchange_initial_event == COMM_MNGR_EVENT_request_read_device_settings )
	{
		comm_mngr.device_exchange_state = GPRS_DEVEX_read_start;
	}
	else
	{
		comm_mngr.device_exchange_state = GPRS_DEVEX_write_start;
	}
	
	// 3/14/2014 rmd : Start the timer so that 10ms from now the first exchange will start.
	//
	// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
	// when posting is safe. Anything on the timer queue is quickly processed and therefore the
	// queue is effectively always empty.
	xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(20), portMAX_DELAY );
}

/* ---------------------------------------------------------- */
static void process_read_state( COMM_MNGR_TASK_QUEUE_STRUCT *q_msg, char *dest, UNS_32 dest_size, char *next_string, UNS_32 next_state )
{
	if( q_msg->event == COMM_MNGR_EVENT_device_exchange_response_arrived )
	{
		// 3/21/2014 rmd : We have the case for the Raven X we want to skip of the string copy cause
		// in one case the info is not available.
		if( q_msg->dh.dptr != NULL )
		{
			strlcpy( dest, (char*)q_msg->dh.dptr, dest_size );
			
			mem_free( q_msg->dh.dptr );
		}

		// ----------

		// 3/19/2014 rmd : Because we only snatch the first CR_LF delimited part of the response
		// that comes along there is almost always more response coming. Typically CR_LF_OK_CR_LF at
		// a minimum. In some cases the response are multi part. And we need to wait till the
		// response has stopped. Otherwise if we plow ahead and restart the crlf string hunt it may
		// trip on the prior response. 20ms is plenty long. Except for in the case of the very long
		// winded software revision info. But that is the LAST query.
		vTaskDelay( MS_to_TICKS( 20 ) );
		
		// ----------
		
		// 3/21/2014 rmd : Raven X complications lead us to need the option to not fire off a new
		// string.
		if( next_string != NULL )
		{
			__setup_for_crlf_delimited_string_hunt( next_string );
		}
		
		// ----------
		
		comm_mngr.device_exchange_state = next_state;
	}
	else
	{
		// 3/19/2014 rmd : There was an error and we should stop. Not supposed to get any errors.
		COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_error );
		
		// 10/21/2014 rmd : And restore packet hunting. What the rest of the application expects!
		RCVD_DATA_enable_hunting_mode( comm_mngr.device_exchange_port, PORT_HUNT_FOR_PACKETS, NOT_STRING_HUNTING  );
	}
}

/* ---------------------------------------------------------- */
static void read_state_machine( COMM_MNGR_TASK_QUEUE_STRUCT *pq_msg )
{
	// 3/14/2014 rmd : This function executed within the context of the comm_mngr task. We can
	// reference and change the device exchange variables as needed.
	
	// ----------
	
	// 3/21/2014 rmd : RSSI value we get from the radio. Typically negative!
	INT_32	rssi;

	// 3/21/2014 rmd : ECIO value we get from the radio. Typically negative! And typically has a
	// fractional part like -2.4.
	float   ecio;

	UNS_32	timer_period_ms;
	
	// ----------
	
	// 3/21/2014 rmd : About this timeout. The Raven X delivers response to its AT commands very
	// quickly. Generally in less than 10ms. However the LS300 is significantly slower. Some of
	// it's responses are not even here after 50ms. So set this timer to 100ms. Remember this
	// will not effect how long it actually takes to complete the job. That total time is almost
	// exclusively dependant on the how fast the radio spits out the response. The second most
	// lengthy activity is the delay in the process_read_state function. As soon as the response
	// shows the state machine moves ahead with the next at command to the device.
	//
	// 3/21/2014 rmd : UPDATE - because the response timing associated with the LS300 is so
	// variable and can be unexpectedly slow, I have moved this timeout to 200ms. Remember this
	// does not mean one AT command each 200ms. It means we wait up to 200ms for the reponse
	// from an AT command. And if doesn't show within that 200ms declare an error.
	//
	// 3/21/2014 rmd : ALL RIGHT well this is just getting crazy. Now I just witnessed on the
	// scope it took 520ms for the response to an at command to appear. I'm just going to set
	// this to 2 SECONDS and be done with it. AJ is going to perhaps put up a 'retrieving please
	// wait' screen. Maybe.
	timer_period_ms = 2000;
	
	// ----------
	
	switch( comm_mngr.device_exchange_state )
	{
		
		case GPRS_DEVEX_read_model:
			// 3/18/2014 rmd : Being the first of the exchanges there is no memory to release. The timer
			// gets us here. Not a response.
			if( pq_msg->event == COMM_MNGR_EVENT_device_exchange_response_arrived )
			{
				Alert_Message( "DEVEX: unexpected event" );
			}

			__setup_for_crlf_delimited_string_hunt( "ati0\r\n" );  // get the model
			
			comm_mngr.device_exchange_state = GPRS_DEVEX_read_netphone;

			break;
			
		case GPRS_DEVEX_read_netphone:
			process_read_state( pq_msg, GuiVar_GRModel, sizeof(GuiVar_GRModel), "at*netphone?\r\n", GPRS_DEVEX_read_netip );
			break;
			
		case GPRS_DEVEX_read_netip:
			process_read_state( pq_msg, GuiVar_GRPhoneNumber, sizeof(GuiVar_GRPhoneNumber), "at*netip?\r\n", GPRS_DEVEX_read_sim );
			break;
			
		case GPRS_DEVEX_read_sim:
			process_read_state( pq_msg, GuiVar_GRIPAddress, sizeof(GuiVar_GRIPAddress), "at+iccid?\r\n", GPRS_DEVEX_read_netstate );
			break;
			
		case GPRS_DEVEX_read_netstate:
			process_read_state( pq_msg, GuiVar_GRSIMID, sizeof(GuiVar_GRSIMID), "at*netstate?\r\n", GPRS_DEVEX_read_carrier );
			break;
			
		case GPRS_DEVEX_read_carrier:
			process_read_state( pq_msg, GuiVar_GRNetworkState, sizeof(GuiVar_GRNetworkState), "at*netop?\r\n", GPRS_DEVEX_read_service );
			break;
			
		case GPRS_DEVEX_read_service:
			process_read_state( pq_msg, GuiVar_GRCarrier, sizeof(GuiVar_GRCarrier), "at*netserv?\r\n", GPRS_DEVEX_read_rssi );
			break;
			
		case GPRS_DEVEX_read_rssi:
			process_read_state( pq_msg, GuiVar_GRService, sizeof(GuiVar_GRService), "at*netrssi?\r\n", GPRS_DEVEX_read_ecio );
			break;
			
		case GPRS_DEVEX_read_ecio:
			process_read_state( pq_msg, GuiVar_GRRSSI, sizeof(GuiVar_GRRSSI), "at+ecio?\r\n", GPRS_DEVEX_read_sernum );

			// ----------

			// 3/21/2014 rmd : Enhance the signal strength value with user friendly text.
			rssi = atoi( GuiVar_GRRSSI );
			
			// 3/21/2014 rmd : If not a valid conversion returns a zero. Catch that.
			if( rssi == 0 )
			{
				// do nothing	
			}
			else
			if( rssi >= -70 )
			{
				strlcat( GuiVar_GRRSSI," (excellent)", sizeof(GuiVar_GRRSSI) );
			}
			else
			if( rssi >= -80 )
			{
				strlcat( GuiVar_GRRSSI," (good)     ", sizeof(GuiVar_GRRSSI) );
			}
			else
			if( rssi >= -85 )
			{
				strlcat( GuiVar_GRRSSI," (fair)     ", sizeof(GuiVar_GRRSSI) );
			}
			else
			if( rssi >= -95 )
			{
				strlcat( GuiVar_GRRSSI," (weak)     ", sizeof(GuiVar_GRRSSI) );
			}
			else
			{
				strlcat( GuiVar_GRRSSI," (very weak)", sizeof(GuiVar_GRRSSI) );
			}
			
			break;
			
		case GPRS_DEVEX_read_sernum:
			// 3/21/2014 rmd : Because there is no AT command to get the serial number out of the Raven
			// X special handling needed.
			if( strncmp( GuiVar_GRModel, "LS300", 5 ) == 0 )
			{
				process_read_state( pq_msg, GuiVar_GRECIO, sizeof(GuiVar_GRECIO), "at*globalid?\r\n", GPRS_DEVEX_read_fwrev );
			}
			else
			{
				process_read_state( pq_msg, GuiVar_GRECIO, sizeof(GuiVar_GRECIO), NULL, GPRS_DEVEX_read_fwrev );

				// 3/21/2014 rmd : No reason to wait the whole 200ms.
				timer_period_ms = 20;
			}

			// ----------

			// 3/21/2014 rmd : Enhance the ecio value with user friendly text.
			ecio = atof( GuiVar_GRECIO );
			
			// 3/21/2014 rmd : If could not make a valid conversion returns 0.0 which is an impossible
			// ecio so catch that.
			if( ecio == 0.0 )
			{
				// do nothing	
			}
			else
			if( ecio >= -1.5 )
			{
				strlcat( GuiVar_GRECIO," (excellent)", sizeof(GuiVar_GRECIO) );
			}
			else
			if( ecio >= -5.9 )
			{
				strlcat( GuiVar_GRECIO," (good)     ", sizeof(GuiVar_GRECIO) );
			}
			else
			if( ecio >= -9.0 )
			{
				strlcat( GuiVar_GRECIO," (marginal) ", sizeof(GuiVar_GRECIO) );
			}
			else
			{
				strlcat( GuiVar_GRECIO," (poor)     ", sizeof(GuiVar_GRECIO) );
			}
			
			break;
			
		case GPRS_DEVEX_read_fwrev:
			// 3/19/2014 rmd : It is IMPORTANT that THIS query remains the last of the queries! The
			// response from this is quite long winded and the balance of the response pouring in takes
			// more than 20 ms. If this were not the last in the list of queries (at commands) then the
			// delay in the process_read_state function would need to be even yet longer. See comment in
			// the process_read_state function.
			//
			// 3/21/2014 rmd : And skip over stuffing variable if not an LS300. Serial number not
			// available for Raven X.
			if( strncmp( GuiVar_GRModel, "LS300", 5 ) != 0 )
			{
				// 3/21/2014 rmd : Means it is a Raven X. Need to craft queue message so won't mistakenly
				// report an error cause we used the timer timeout to get here for the Raven X. Stuff the
				// Radio Ser Num string like we want for the Raven X and make so not over written by making
				// a NULL pointer.
				strncpy( GuiVar_GRRadioSerialNum, "(not avail on Raven X)", sizeof(GuiVar_GRRadioSerialNum) );
				
				pq_msg->event = COMM_MNGR_EVENT_device_exchange_response_arrived;

				pq_msg->dh.dptr = NULL;
			}

			process_read_state( pq_msg, GuiVar_GRRadioSerialNum, sizeof(GuiVar_GRRadioSerialNum), "ati1\r\n", GPRS_DEVEX_read_finish );
			break;
			
		case GPRS_DEVEX_read_finish:
		
			if( pq_msg->event == COMM_MNGR_EVENT_device_exchange_response_arrived )
			{
				strlcpy( GuiVar_GRFirmwareVersion, (char*)pq_msg->dh.dptr, sizeof(GuiVar_GRFirmwareVersion) );
				
				mem_free( pq_msg->dh.dptr );
			}

			// ----------
			
			// 10/21/2014 rmd : Restore packet hunting (what the rest of the application expects!).
			RCVD_DATA_enable_hunting_mode( comm_mngr.device_exchange_port, PORT_HUNT_FOR_PACKETS, NOT_STRING_HUNTING  );

			// 3/18/2014 rmd : Send a message to the key task.
			COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_completed_ok );
			
			// ----------
			
			break;
			
	}

	// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
	// when posting is safe. Anything on the timer queue is quickly processed and therefore the
	// queue is effectively always empty.
	xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(timer_period_ms), portMAX_DELAY );
}

/* ---------------------------------------------------------- */
static void write_state_machine( COMM_MNGR_TASK_QUEUE_STRUCT *pq_msg )
{
	// 3/14/2014 rmd : This function executed within the context of the comm_mngr task. We can
	// reference and change the device exchange variables as needed.

	UNS_32	timer_period_ms;

	timer_period_ms = 100;

	switch( comm_mngr.device_exchange_state )
	{
		
		case GPRS_DEVEX_write_start:
		
			
			break;
			
	}

}

/* ---------------------------------------------------------- */
extern void GR_AIRLINK_exchange_processing( void *pq_msg )
{
	// 3/18/2014 rmd : Note how unfortunately we had to use a parameter of type void*. We could
	// not use the COMM_MNGR_EVENT_QUEUE_EVENT_STRUCT type as it wouldn't compile. Circular
	// references. void* was the easiest way out of the problem.
	
	// 3/19/2014 rmd : Always stop the response timer for the device exchange process.
	xTimerStop( comm_mngr.timer_device_exchange, portMAX_DELAY );
	
	// 3/19/2014 rmd : During the last exchange the timer is started again and eventually fires
	// off. This is the block to prevent further unwanted processing.
	if( comm_mngr.mode == COMM_MNGR_MODE_device_exchange )
	{
		if( comm_mngr.device_exchange_initial_event == COMM_MNGR_EVENT_request_read_device_settings )
		{
			read_state_machine( (COMM_MNGR_TASK_QUEUE_STRUCT*)pq_msg );
		}
		else
		{
			write_state_machine( (COMM_MNGR_TASK_QUEUE_STRUCT*)pq_msg );
		}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

