/*  file = device_WEN_WBX2100E.h              01.22.2014  ajv */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_DEVICE_WEN_WBX2100E_H
#define INC_DEVICE_WEN_WBX2100E_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"device_common.h"	

//(xml)#xcr dump device
//<?xml version="1.0" standalone="yes"?>
//<!-- Automatically generated XML -->
//<!DOCTYPE configrecord [
//   <!ELEMENT configrecord (configgroup+)>
//   <!ELEMENT configgroup (configitem+,configgroup*)>
//   <!ELEMENT configitem (value+)>
//   <!ELEMENT value (#PCDATA)>
//   <!ATTLIST configrecord version CDATA #IMPLIED>
//   <!ATTLIST configgroup name CDATA #IMPLIED>
//   <!ATTLIST configgroup instance CDATA #IMPLIED>
//   <!ATTLIST configitem name CDATA #IMPLIED>
//   <!ATTLIST configitem instance CDATA #IMPLIED>
//   <!ATTLIST value name CDATA #IMPLIED>
//]>
//<configrecord version = "0.1.0.0T0">
//   <configgroup name = "device">
//       <configitem name = "short name">
//           <value>premierwave_xn</value>
//       </configitem>
//       <configitem name = "long name">
//           <value>Lantronix PremierWave XN</value>
//       </configitem>
//       <configitem name = "serial number">
//           <value>0080A39A2517</value>
//       </configitem>
//       <configitem name = "firmware version">
//           <value>7.9.0.0R19</value>
//       </configitem>
//   </configgroup>
//</configrecord>
//(xml)#exit
//(enable)#show
//Product Information:
//   Product Type        : Lantronix PremierWave XN (premierwave_xn)
//   FW Version / Date   : 7.9.0.0R19 / Sep 30 22:02:39 PDT 2014
//   Radio FW Version    : 3.2.12.0/1.1.6.71/6.134
//   Current Date/Time   : Thu Jan  1 00:08:40 UTC 1970
//   Temperature         : 24.0C
//   Serial Number       : 0080A39A2517
//   Uptime              : 0 days 00:08:41
//   Perm. Config        : saved
//   Region              : United States
//
//Alerts:
//   Alarm               : eth0 link state change
//   Duration            : 0 days 00:07:58
//
//   Alarm               : wlan0 link state change
//   Duration            : 0 days 00:07:58
//
//   Alarm               : backup power change
//   Duration            : 0 days 00:07:54
//
//Power Status:
//   Main Power:         : Up
//   Backup Power:       : Down
//
//Network Status:
//   Primary DNS         : <None>
//   Secondary DNS       : <None>
//
//   Interface           : eth0
//   Link                : Auto 10/100 Mbps Auto Half/Full (No link)
//   MAC Address         : 00:80:a3:9a:25:17
//   Hostname            : <None> [<DHCP>]
//   Domain              : <None> [<DHCP>]
//   IP Address          : <None> [<DHCP>]
//   Network Mask        : <None> [<DHCP>]
//   Default Gateway     : <None> [<DHCP>]
//   MTU                 : 1500
//
//   Interface           : wlan0
//   Link                : DISCONNECTED
//   MAC Address         : 00:80:a3:9a:25:18
//   Hostname            : <None> [<DHCP>]
//   Domain              : <None> [<DHCP>]
//   IP Address          : <None> [<DHCP>]
//   Network Mask        : <None> [<DHCP>]
//   Default Gateway     : <None> [<DHCP>]
//   MTU                 : 1500
//
//Line 1:
//   RS232, 115200, None, 8, 1, Hardware [CLI]
//   Tunnel Connect Mode: Waiting, Accept Mode: Waiting
//Line 2:
//   RS232, 9600, None, 8, 1, None
//   Tunnel Connect Mode: Disabled, Accept Mode: Waiting
//
//VPN Status:
//   Status              : Disabled
//   IP Address          : <None>
//(enable)#
//(enable)#configure
//(config)#?
//action                                  applications
//arp                                     bridge <instance>
//cli                                     clock
//clrscrn                                 ddns
//diagnostics                             discovery
//exit                                    ftp
//gateway                                 gre <instance>
//host <number>                           http
//icmp                                    if <instance>
//ip                                      kill ssh <session>
//kill telnet <session>                   rss
//show                                    show history
//smtp                                    snmp
//syslog                                  terminal <line>
//terminal network                        vpn <instance>
//wlan profiles                           write
//
//(config)#wlan profiles
//(config-profiles)#?
//apply wlan                              clrscrn
//create <profile name>                   delete <profile name>
//edit <profile name>                     exit
//show                                    show history
//write
//
//(config-profiles)#show
//    1) default_infrastructure_profile (Enabled)
//    2) Calsense (Enabled)
//    3) default_adhoc_profile (Disabled)
//(config-profiles)#edit calsense
//(config-profile-basic:Calsense)#?
//advanced                                apply wlan
//channel <number>                        clrscrn
//default channel                         default mode
//default topology                        exit
//mode 802.11a only                       mode 802.11abgn
//mode 802.11an                           mode 802.11b only
//mode 802.11bg                           mode 802.11bgn
//network name <text>                     no network name
//scan dfs channels disable               scan dfs channels enable
//security                                show
//show history                            state disable
//state enable                            topology adhoc
//topology infrastructure                 write
//
//(config-profile-basic:Calsense)#show
//WLAN Profile Basic Calsense Configuration:
//   Network Name     : hendrix
//   Topology         : Infrastructure
//   Mode             : 802.11bg
//   Scan DFS Channels: Enabled
//   State            : Enabled
//   Channel          : 1
//(config-profile-basic:Calsense)#
//(config)#if eth0
//(config-if:eth0)#show
//Interface eth0 Configuration:
//   State          : Enabled
//   BOOTP          : Disabled
//   DHCP           : Enabled
//   Priority       : 1
//   IP Address     : <None>
//   Default Gateway: <None>
//   Hostname       :
//   Domain         :
//   DHCP Client ID :
//   Primary DNS    : <None>
//   Secondary DNS  : <None>
//   MTU            : 1500 bytes
//   WARNING: Priority for interface eth0 and wlan0 are the same. Default priorities will be applied for all interfaces.
//(config-if:eth0)#show status
//Network Status:
//   Interface           : eth0
//   Link                : Auto 10/100 Mbps Auto Half/Full (No link)
//   MAC Address         : 00:80:a3:9a:25:17
//   Hostname            : <None> [<DHCP>]
//   Domain              : <None> [<DHCP>]
//   IP Address          : <None> [<DHCP>]
//   Network Mask        : <None> [<DHCP>]
//   Default Gateway     : <None> [<DHCP>]
//   Primary DNS         : <None>
//   Seconday DNS        : <None>
//   MTU                 : 1500
//   BOOTP               : Off
//   DHCP                : On
//   DHCP Client ID      : <None>
//Statistics:
//   Received            : 0 bytes
//   Transmitted         : 238 bytes
//(config-if:eth0)#exit
//(config)#if wlan0
//(config-if:wlan0)#show
//Interface wlan0 Configuration:
//   State          : Enabled
//   BOOTP          : Disabled
//   DHCP           : Enabled
//   Priority       : 1
//   IP Address     : <None>
//   Default Gateway: <None>
//   Hostname       :
//   Domain         :
//   DHCP Client ID :
//   Primary DNS    : <None>
//   Secondary DNS  : <None>
//   MTU            : 1500 bytes
//   WARNING: Priority for interface eth0 and wlan0 are the same. Default priorities will be applied for all interfaces.
//(config-if:wlan0)#show status
//Network Status:
//   Interface           : wlan0
//   Link                : DISCONNECTED
//   MAC Address         : 00:80:a3:9a:25:18
//   Hostname            : <None> [<DHCP>]
//   Domain              : <None> [<DHCP>]
//   IP Address          : <None> [<DHCP>]
//   Network Mask        : <None> [<DHCP>]
//   Default Gateway     : <None> [<DHCP>]
//   Primary DNS         : <None>
//   Seconday DNS        : <None>
//   MTU                 : 1500
//   BOOTP               : Off
//   DHCP                : On
//   DHCP Client ID      : <None>
//Statistics:
//   Received            : 0 bytes
//   Transmitted         : 0 bytes
//(config-if:wlan0)#
//
//(xml)#xcr dump "wlan profile:Calsense"
//<?xml version="1.0" standalone="yes"?>
//<!-- Automatically generated XML -->
//<!DOCTYPE configrecord [
//   <!ELEMENT configrecord (configgroup+)>
//   <!ELEMENT configgroup (configitem+,configgroup*)>
//   <!ELEMENT configitem (value+)>
//   <!ELEMENT value (#PCDATA)>
//   <!ATTLIST configrecord version CDATA #IMPLIED>
//   <!ATTLIST configgroup name CDATA #IMPLIED>
//   <!ATTLIST configgroup instance CDATA #IMPLIED>
//   <!ATTLIST configitem name CDATA #IMPLIED>
//   <!ATTLIST configitem instance CDATA #IMPLIED>
//   <!ATTLIST value name CDATA #IMPLIED>
//]>
//<configrecord version = "0.1.0.0T0">
//   <configgroup name = "wlan profile" instance = "Calsense">
//       <configitem name = "profile type">
//           <value>User Defined</value>
//       </configitem>
//       <configitem name = "interface">
//           <value></value>
//       </configitem>
//       <configitem name = "priority">
//           <value>0 </value>
//       </configitem>
//       <configitem name = "bssid">
//           <value></value>
//       </configitem>
//       <configitem name = "basic">
//           <value name = "network name">hendrix</value>
//           <value name = "topology">Infrastructure</value>
//           <value name = "mode">802.11bg</value>
//           <value name = "scan dfs channels">enable</value>
//           <value name = "state">enable</value>
//           <value name = "channel">1 </value>
//       </configitem>
//       <configitem name = "advanced">
//           <value name = "tx data rate maximum">MCS7</value>
//           <value name = "tx data rate">Auto-reduction</value>
//           <value name = "tx power maximum">17 dBm</value>
//           <value name = "antenna diversity">Antenna 1</value>
//           <value name = "max missed beacons">20 beacons</value>
//           <value name = "power management">disable</value>
//           <value name = "power management interval">1 beacons (100 ms each)</value>
//       </configitem>
//       <configitem name = "security">
//           <value name = "suite">None</value>
//           <value name = "key type">Passphrase</value>
//           <value name = "passphrase"></value>
//           <value name = "wep authentication">Open</value>
//           <value name = "wep key size">40</value>
//           <value name = "wep tx key index">1</value>
//           <value name = "wep key 1"></value>
//           <value name = "wep key 2"></value>
//           <value name = "wep key 3"></value>
//           <value name = "wep key 4"></value>
//           <value name = "wpax authentication">PSK</value>
//           <value name = "wpax key"></value>
//           <value name = "wpax ieee 802.1x">EAP-TTLS</value>
//           <value name = "wpax eap-ttls option">EAP-MSCHAPV2</value>
//           <value name = "wpax peap option">EAP-MSCHAPV2</value>
//           <value name = "wpax username"></value>
//           <value name = "wpax password"></value>
//           <value name = "wpax encryption">None</value>
//           <value name = "wpax validate certificate">enable</value>
//           <value name = "wpax credentials"></value>
//       </configitem>
//   </configgroup>
//</configrecord>
//(xml)#

// 5/26/2015 mpd : PuTTY Programming dump. The steps listed in the comments correspond to the DeviceInstaller steps in 
// the "http://odin.domain.calsense.com/fogbugz/default.asp?W177" document. The beginning steps are app startup. The 
// CLI path merges with DeviceInstaller at step 9.
//(enable)#configure
//(config)#clock																	// STEP 9
//(config-clock)#synchronization method sntp										// STEP 10
//(config-clock)#clock timezone PST8PDT												// STEP 13
//(config-clock)#show system clock
//Current Configuration:
//   Zone      : PST8PDT
//   UTC Offset: -0800
//   Date      : Wed 31 Dec 1969
//   Time      : 16:19:23 PST
//(config-clock)#exit
//(config)#exit
//(enable)#line 1																	// STEP 14 - 18 (some skipped)
//(line:1)#show
//Line 1 Status:
//   State       : Enabled
//   Protocol    : Tunnel
//   Baud Rate   : 115200 bits per second
//   Parity      : None
//   Data Bits   : 8
//   Stop Bits   : 1
//   Flow Control: Hardware
//   Xon Char    : <control>Q
//   Xoff Char   : <control>S
//(line:1)#exit
//(enable)#configure
//(config)#if eth0
//(config-if:eth0)#priority 2														// STEP 21
//   eth0 Priority change will take effect on the next reboot after a write command.
//(config-if:eth0)#write															// STEP 22
//(config-if:eth0)#exit																// STEP 23
//(config)#if wlan0																	// STEP 24
//(config-if:wlan0)#show
//Interface wlan0 Configuration:
//   State          : Enabled
//   BOOTP          : Disabled
//   DHCP           : Enabled														// STEP 25 (some skipped)
//   Priority       : 1
//   IP Address     : <None>
//   Default Gateway: <None>
//   Hostname       :																// Use MAC Address
//   Domain         :
//   DHCP Client ID :
//   Primary DNS    : <None>
//   Secondary DNS  : <None>
//   MTU            : 1500 bytes
//(config-if:wlan0)#show status
//Network Status:
//   Interface           : wlan0
//   Link                : DISCONNECTED
//   MAC Address         : 00:80:a3:9a:25:18
//   Hostname            : <None> [<DHCP>]
//   Domain              : <None> [<DHCP>]
//   IP Address          : <None> [<DHCP>]
//   Network Mask        : <None> [<DHCP>]
//   Default Gateway     : <None> [<DHCP>]
//   Primary DNS         : <None>
//   Seconday DNS        : <None>
//   MTU                 : 1500
//   BOOTP               : Off
//   DHCP                : On
//   DHCP Client ID      : <None>
//Statistics:
//   Received            : 0 bytes
//   Transmitted         : 0 bytes
//(config-if:wlan0)#exit
//(config)#exit
//(enable)#tunnel 1																	// STEP 27
//(tunnel:1)#accept																	// STEP 28
//(tunnel-accept:1)#show
//Tunnel Accept 1 Configuration:
//   Accept Mode          : Modem Emulation
//   Local Port           : 10001
//   Protocol             : TCP
//   Credentials          :
//   TCP Keep Alive       : 45000 milliseconds
//   AES Encrypt Key      : (Not configured)
//   AES Decrypt Key      : (Not configured)
//   Start Character      : <control>B
//   Flush Start Character: Enabled
//   Flush Serial         : Disabled
//   Block Serial         : Disabled
//   Block Network        : Disabled
//   Password             : (Not configured)
//   Password Prompt      : Disabled
//   Email Connect        : <None>
//   Email Disconnect     : <None>
//   WARNING: The port is currently in Command Mode.
//(tunnel-accept:1)#accept mode disable												// STEP 29
//(tunnel-accept:1)#write															// STEP 30
//(tunnel-accept:1)#exit
//(tunnel:1)#connect																// STEP 31
//(tunnel-connect:1)#connect mode always											// STEP 32
//   WARNING: The port is currently in Command Mode.
//   WARNING: Host 1 Address is required.
//(tunnel-connect:1)#host 1															// STEP 33
//(tunnel-connect-host:1:1)#show
//Tunnel Connect Host 1:1 Configuration:
//   Address             :
//   Port                : <None>
//   Protocol            : TCP
//   SSH Username        :
//   Credentials         :
//   Validate Certificate: Enabled
//   TCP User Timeout    : 0 milliseconds
//   TCP Keep Alive      : 45000 milliseconds
//   AES Encrypt Key     : (Not configured)
//   AES Decrypt Key     : (Not configured)
//(tunnel-connect-host:1:1)#address 64.73.242.99									// STEP 34
//   WARNING: The port is currently in Command Mode.
//   WARNING: Host 1 Port is required.
//(tunnel-connect-host:1:1)#port 16001												// STEP 35
//   WARNING: The port is currently in Command Mode.
//(tunnel-connect-host:1:1)#tcp keep alive 60000									// STEP 36
//   WARNING: The port is currently in Command Mode.
//(tunnel-connect-host:1:1)#write													// STEP 37
//(tunnel-connect-host:1:1)#show
//Tunnel Connect Host 1:1 Configuration:
//   Address             : 64.73.242.99
//   Port                : 16001
//   Protocol            : TCP
//   SSH Username        :
//   Credentials         :
//   Validate Certificate: Enabled
//   TCP User Timeout    : 0 milliseconds
//   TCP Keep Alive      : 60000 milliseconds
//   AES Encrypt Key     : (Not configured)
//   AES Decrypt Key     : (Not configured)
//(tunnel-connect-host:1:1)#exit
//(tunnel-connect:1)#exit
//(tunnel:1)#disconnect																// STEP 38
//(tunnel-disconnect:1)#show
//Tunnel Disconnect 1 Configuration:
//   Stop Character      : <None>
//   Flush Stop Character: Enabled
//   Modem Control       : Enabled
//   Timeout             : 0 milliseconds
//   Flush Serial        : Disabled
//(tunnel-disconnect:1)#modem control enable										// STEP 39
//(tunnel-disconnect:1)#write														// STEP 40
//(tunnel-disconnect:1)#exit
//(tunnel:1)#modem																	// STEP 41
//(tunnel-modem:1)#echo commands disable											// STEP 42
//   WARNING: Tunnel Connect Mode is not "Modem Emulation".
//(tunnel-modem:1)#verbose response disable											// STEP 43
//   WARNING: Tunnel Connect Mode is not "Modem Emulation".
//(tunnel-modem:1)#write															// STEP 44
//(tunnel-modem:1)#exit
//(tunnel:1)#exit
//(enable)#configure
//(config)#wlan profiles															// STEP 45
//(config-profiles)#show
//    1) default_infrastructure_profile (Enabled)
//    2) calsense_hq (Enabled)
//    3) default_adhoc_profile (Disabled)
//    4) Calsense (Enabled)
//(config-profiles)#create calsense_hq												// STEP 46
//   Created WLAN profile named "calsense_hq".
//(config-profiles)#edit calsense_hq												// STEP 47
//(config-profile-basic:calsense_hq)#network name calsense_hq						// STEP 48
//   WARNING: Changes have neither been applied nor written to Flash.
//(config-profile-basic:calsense_hq)# mode 802.11bgn								// STEP 49
//   WARNING: Changes have neither been applied nor written to Flash.
//(config-profile-basic:calsense_hq)#security
//(config-profile-security:calsense_hq)#suite wpa2									// STEP 50
//   WARNING: No security passphrase is set.
//   WARNING: Changes have neither been applied nor written to Flash.
//(config-profile-security:calsense_hq)#wpax
//(config-profile-security-wpax:calsense_hq)#authentication psk						// STEP 51
//   WARNING: No security passphrase is set.
//   WARNING: Changes have neither been applied nor written to Flash.
//(config-profile-security-wpax:calsense_hq)#write
//(config-profile-security-wpax:calsense_hq)#exit
//(config-profile-security:calsense_hq)#key type passphrase					 		// STEP 52
//   WARNING: No security passphrase is set.
//   WARNING: Changes have neither been applied nor written to Flash.
//(config-profile-security:calsense_hq)#passphrase c@153ns3							// STEP 53
//   WPA or WPA2 Key has been generated automatically.
//   WARNING: Changes have neither been applied nor written to Flash.
//(config-profile-security:calsense_hq)#wpax
//(config-profile-security-wpax:calsense_hq)#encryption tkip enable					// STEP 54
//   WARNING: Changes have neither been applied nor written to Flash.
//(config-profile-security-wpax:calsense_hq)#exit
//(config-profile-security:calsense_hq)#advanced
//(config-profile-advanced:calsense_hq)#antenna diversity antenna 1					// STEP 55
//   WARNING: Changes have neither been applied nor written to Flash.
//(config-profile-advanced:calsense_hq)#write										// STEP 56 - 59
//(config-profile-advanced:calsense_hq)#exit
//(config-profiles)#exit
//(config)#if wlan0																	// STEP 61
//(config-if:wlan0)#link															// STEP 62
//(config-wlan:wlan0)#choice 2														// STEP 63a
//(config-wlan-choice:wlan0:2)#profile calsense_hq									// STEP 63b
//(config-wlan-choice:wlan0:2)#write												// STEP 64 - 65
//(config-wlan-choice:wlan0:2)#exit
//(config-wlan:wlan0)#exit
//(config-if:wlan0)#exit
//(config)#exit																		// STEP 66
//(enable)#reload																	// STEP 67
//Are you sure (yes/no)? yes														// STEP 68
//
//Rebooting...
//waiting for reboot ...

// 5/20/2015 mpd : WEN Version:
//#define WEN_FW_VER_MIN_1			  				 	( 7 )					// This is the minimum firmware version we require (UNS_32). 
//#define WEN_FW_VER_MIN_2			  				 	( 9 )					// This is the minimum firmware version we require (UNS_32).
//#define WEN_FW_VER_MIN_3			   					( 0 )					// This is the minimum firmware version we require (UNS_32).
#define WEN_FW_VER_MIN_4			   					"0R19"					// This is the minimum firmware version we require (string).

/* 
WIFI SETTINGS BY MENU 
 
**** BASIC 
	( wen_details->radio_mode,              sizeof( wen_details->radio_mode ) );
mode 802.11a only 
mode 802.11abgn
mode 802.11an 
mode 802.11b only
mode 802.11bg 
mode 802.11bgn
 
    ( wen_details->ssid,           			sizeof( wen_details->ssid ) );
no network name 
network name <text> 
 
**** SECURITY 
	( wen_details->security,                sizeof( wen_details->security ) );
suite none
suite wep 
suite wpa
suite wpa2
 
	( wen_details->w_key_type,              sizeof( wen_details->w_key_type ) );
key type hex
key type passphrase
 
    ( wen_details->passphrase,           	sizeof( wen_details->passphrase ) );
no passphrase 
passphrase <text> 
 
**** SECURITY WEP 
	( wen_details->wep_authentication,      sizeof( wen_details->wep_authentication ) );
authentication open
authentication shared  
 
	( wen_details->wep_key_size,            sizeof( wen_details->wep_key_size ) );
key size 104
key size 40 
 
	( wen_details->wep_tx_key,              sizeof( wen_details->wep_tx_key ) );
tx key index 1
tx key index 2 
tx key index 3
tx key index 4                 
 
**** SECURITY WEP KEY X 
 
	( wen_details->key,           			sizeof( wen_details->key ) );
key <hexadecimal>
key text <text> 
 
**** SECURITY WPAX 
 
	( wen_details->wpa_authentication,      sizeof( wen_details->wpa_authentication ) );
authentication 802.1x
authentication psk 
 
	( wen_details->key,           			sizeof( wen_details->key ) );
no key 
key <hexadecimal>
key text <text> 
 
	( wen_details->wpa_eap_ttls_Option,     sizeof( wen_details->wpa_eap_ttls_Option ) );
eap-ttls option chap 
eap-ttls option eap-md5
eap-ttls option eap-mschapv2 
eap-ttls option mschap
eap-ttls option mschapv2 
eap-ttls option pap
 
	( wen_details->wpa_ieee_802_1x,         sizeof( wen_details->wpa_ieee_802_1x ) );
ieee 802.1x eap-tls
ieee 802.1x eap-ttls 
ieee 802.1x leap
ieee 802.1x peap 
 
	( wen_details->wpa_peap_option,         sizeof( wen_details->wpa_peap_option ) );
peap option eap-md5 
peap option eap-mschapv2 
 
	wen_details->wpa_eap_tls_valid_cert    
validate certificate disable
validate certificate enable 
 
	wen_details->wpa_encription_ccmp   
encryption ccmp disable 
encryption ccmp enable
 
	wen_details->wpa_encription_tkip   
encryption tkip disable 
encryption tkip enable
 
	wen_details->wpa_encription_wep  
encryption wep disable 
encryption wep enable
 
	( wen_details->wpa_eap_tls_credentials,	sizeof( wen_details->wpa_eap_tls_credentials ) );
credentials <text> 
no credentials
 
	( wen_details->password,          		sizeof( wen_details->password ) );
no password 
password <text>
 
	( wen_details->user_name,          	 	sizeof( wen_details->user_name ) );
no username 
username <text> 
 
*/  

// 5/29/2015 mpd : Possible profile item string values:
// MODE: 802.11a only, 802.11abgn, 802.11an, 802.11b only, 802.11bg, 802.11bgn.
// SUITE: None, WEP, WPA, WPA2.
// KEY TYPE: Hex, Passphrase.
// WEP AUTH: Open/None, Shared.
// KEY SIZE: 40, 104.
// WPA AUTH: PSK, RSA, XAUTH, IEEE 802.1x.
// TTLS OPTION: CHAP, EAP-MD5, EAP-MSCHAPV2, MSCHAP, MSCHAPV2, PAP.
// IEEE: EAP-TLS, EAP-TTLS, LEAP, PEAP.
// PEAP: EAPMD5, EAPMSCHAPV2
// ENCRYPTION: CCMP, TKIP, WEP, None;

// 5/27/2015 mpd : These lengths are used for READ and WRITE operations. They must exceed GuiVar text lengths by 1 byte 
// to allow for the CR during WRITE operations.
//#define WEN_KEY_LEN 						( 63 + DEV_CR_STR_LEN )
//#define WEN_MAC_ADDRESS_LEN 				( 48 + DEV_CR_STR_LEN )
//#define WEN_PASS_PHRASE_LEN 				( 63 + DEV_CR_STR_LEN )
//#define WEN_SSID_LEN 						( 32 + DEV_CR_STR_LEN )
//#define WEN_STATUS_LEN 					( 48 + DEV_CR_STR_LEN )
//#define WEN_WPA_EAP_TLS_CRED_LEN 			( 63 + DEV_CR_STR_LEN )
//#define WEN_WPA_PASSWORD_LEN 				( 63 + DEV_CR_STR_LEN )
//#define WEN_WPA_USER_NAME_LEN 			( 63 + DEV_CR_STR_LEN )
//#define WEN_MODE_LEN 						( 12 + DEV_CR_STR_LEN )	// Longest known: "802.11a only"
//#define WEN_SECURITY_LEN	 				( 4 + DEV_CR_STR_LEN )	// Longest known: "WPA2"
//#define WEN_KEY_TYPE_LEN 					( 10 + DEV_CR_STR_LEN )	// Longest known: "Passphrase"
//#define WEN_WEP_AUTH_LEN 					( 9 + DEV_CR_STR_LEN )	// Longest known: "Open/None"
//#define WEN_KEY_SIZE_LEN 					( 3 + DEV_CR_STR_LEN )	// Longest known: "104"
//#define WEN_TX_KEY_LEN  					( 1 + DEV_CR_STR_LEN )	// Longest known: "1"
//#define WEN_WPA_AUTH_LEN 					( 6 + DEV_CR_STR_LEN )	// Longest known: "802.1x"
//#define WEN_TTLS_OPT_LEN 					( 12 + DEV_CR_STR_LEN )	// Longest known: "EAP-MSCHAPV2"
//#define WEN_IEEE_LEN 						( 8 + DEV_CR_STR_LEN )	// Longest known: "EAP-TTLS"
//#define WEN_PEAP_LEN 						( 11 + DEV_CR_STR_LEN )	// Longest known: "EAPMSCHAPV2"

// 6/3/2015 mpd : WEN is adding the CR as it assembles the command. This keeps the strings going to and from the GUI in
// sync with GUI values. Comparisons do not have to strip the CR.
#define WEN_KEY_LEN 						( 64 + DEV_NULL_LEN )
#define WEN_MAC_ADDRESS_LEN 				( 48 + DEV_NULL_LEN )
#define WEN_PASS_PHRASE_LEN 				( 63 + DEV_NULL_LEN )
#define WEN_SSID_LEN 						( 32 + DEV_NULL_LEN )
#define WEN_STATUS_LEN 						( 48 + DEV_NULL_LEN )
#define WEN_WPA_EAP_TLS_CRED_LEN 			( 63 + DEV_NULL_LEN )
#define WEN_WPA_PASSWORD_LEN 				( 63 + DEV_NULL_LEN )
#define WEN_WPA_USER_NAME_LEN 				( 63 + DEV_NULL_LEN )
#define WEN_MODE_LEN 						( 12 + DEV_NULL_LEN )	// Longest known: "802.11a only"
#define WEN_SECURITY_LEN	 				( 4 + DEV_NULL_LEN )	// Longest known: "WPA2"
#define WEN_KEY_TYPE_LEN 					( 10 + DEV_NULL_LEN )	// Longest known: "Passphrase"
#define WEN_WEP_AUTH_LEN 					( 9 + DEV_NULL_LEN )	// Longest known: "Open/None"
#define WEN_KEY_SIZE_LEN 					( 3 + DEV_NULL_LEN )	// Longest known: "104"
#define WEN_TX_KEY_LEN  					( 1 + DEV_NULL_LEN )	// Longest known: "1"
#define WEN_WPA_AUTH_LEN 					( 6 + DEV_NULL_LEN )	// Longest known: "802.1x"
#define WEN_TTLS_OPT_LEN 					( 12 + DEV_NULL_LEN )	// Longest known: "EAP-MSCHAPV2"
#define WEN_IEEE_LEN 						( 8 + DEV_NULL_LEN )	// Longest known: "EAP-TTLS"
#define WEN_PEAP_LEN 						( 12 + DEV_NULL_LEN )	// Longest known: "EAP-MSCHAPV2"

// 6/9/2015 mpd : This struct needs to be named for the forward declaration (needed by DEV_STATE_STRUCT) to work.
typedef struct WEN_DETAILS_STRUCT {

	char mac_address[ WEN_MAC_ADDRESS_LEN ];
	char status[ WEN_STATUS_LEN ];
	char dhcp_name[ DEV_DHCP_NAME_LEN ];                    

	// 6/11/2015 mpd : Scenario: DHCP was enabled (a DHCP name existed), it was disabled, and it's about to be 
	// reenabled. We don't want to stomp on a long custom name if we ever knew it.
	// This flag tells us it's safe to stomp on any DHCP name that existed in the device the last time DHCP was enabled.
	// Or conversely, it prevents us from stomping on a name that was temporarily hidden from view during the current 
	// session.
	BOOL_32 dhcp_name_not_set;

	// 5/29/2015 mpd : These are in the order displayed in the "xcr dump profile" response.

	char ssid[ WEN_SSID_LEN ];
	char radio_mode[ WEN_MODE_LEN ];
	char key[ WEN_KEY_LEN ];
	char security[ WEN_SECURITY_LEN ];
	char w_key_type[ WEN_KEY_TYPE_LEN ];
	char passphrase[ WEN_PASS_PHRASE_LEN ];
	char wep_authentication[ WEN_WEP_AUTH_LEN ];
	char wep_key_size[ WEN_KEY_SIZE_LEN ];
	char wep_tx_key[ WEN_TX_KEY_LEN ];
	char wpa_authentication[ WEN_WPA_AUTH_LEN ];
	char wpa_ieee_802_1x[ WEN_IEEE_LEN ];
	char wpa_eap_ttls_option[ WEN_TTLS_OPT_LEN ];
	char wpa_peap_option[ WEN_PEAP_LEN ];
	char user_name[ WEN_WPA_USER_NAME_LEN ];
	char password[ WEN_WPA_PASSWORD_LEN ];
	//   wpa_encription_ccmp;
	//   wpa_encription_tkip;
	//   wpa_encription_wep;
	//   wpa_eap_tls_valid_cert;
	char wpa_eap_tls_credentials[ WEN_WPA_EAP_TLS_CRED_LEN ];

	BOOL_32 wpa_encription_ccmp;
	BOOL_32 wpa_encription_tkip;
	BOOL_32 wpa_encription_wep;
	BOOL_32 wpa_eap_tls_valid_cert;

	// 6/15/2015 mpd : For security reasons, several fields cannot be read from the device. That means we cannot blindly
	// write the original values back to the device whn the user hits the "Save" key. Only modify these fields if the
	// GUI explicitly tells us to do so.
	BOOL_32 user_changed_credentials;
	BOOL_32 user_changed_key;
	BOOL_32 user_changed_passphrase;
	BOOL_32 user_changed_password;

	INT_32 rssi;							// Signed INT_32

	// 9/23/2015 mpd : FogBugz #3164: The Lantronix EN UDS1100 does not use CIDR (Classless Inter Domain Routing) format
	// to define the network mask. The number of bits indicates the available bits for host addressing (e.g. A mask of 
	// "8" produces a netmask of "255.255.255.0" which has 8 bits available for hosts on the subnet. Other Lantronix
	// devices use CIDR format for programming (e.g. "192.168.254.150/24" specifies the IP address as well as a network 
	// mask of "255.255.255.0"). The CIDR mask bit value indicates the leading ones; not the trailing zeros.
	// The CS3000 GUI code was written for UDS1100. It's backwards for all newer Lantronix devices. The mask numbers are 
	// internal to CS3000 code. They become external at the GUI and in device programming. To eliminate extensive 
	// rework, the GUI side for all device types will retain pre-CIDR mask values. As mask values pass to and from the 
	// GUI code to the device code (e.g. "e_wen_programming.c" to "device_WEN_PremierWaveXN.c"), the value will be 
	// transformed to match the receiving format.  
	UNS_32 mask_bits;

	// 5/28/2015 mpd : Whole IP addresses are read from the device and used in decision making. Octets are set by 
	// individual GUI fields and are used primarily to program the device. 
	char ip_address[ DEV_IP_ADD_LEN ];  
	char ip_address_1[ DEV_IP_ADDRESS_OCTET_LEN ];            
	char ip_address_2[ DEV_IP_ADDRESS_OCTET_LEN ];            
	char ip_address_3[ DEV_IP_ADDRESS_OCTET_LEN ];            
	char ip_address_4[ DEV_IP_ADDRESS_OCTET_LEN ];            

	// 9/24/2015 mpd : FogBugz #3178: Neither the Gui side or the device side code cares about the mask string.
	// It's the mask bits that count. Remove this field. 
	//char mask[ DEV_IP_ADD_LEN ]; 
	//char mask_1[ DEV_IP_ADDRESS_OCTET_LEN ]; 
	//char mask_2[ DEV_IP_ADDRESS_OCTET_LEN ]; 
	//char mask_3[ DEV_IP_ADDRESS_OCTET_LEN ]; 
	//char mask_4[ DEV_IP_ADDRESS_OCTET_LEN ];

	char gw_address[ DEV_IP_ADD_LEN ];              
	char gw_address_1[ DEV_IP_ADDRESS_OCTET_LEN ];            
	char gw_address_2[ DEV_IP_ADDRESS_OCTET_LEN ];            
	char gw_address_3[ DEV_IP_ADDRESS_OCTET_LEN ];            
	char gw_address_4[ DEV_IP_ADDRESS_OCTET_LEN ]; 

} WEN_DETAILS_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern WEN_DETAILS_STRUCT wen_details;

extern const DEV_MENU_ITEM wen_read_list[]; 

extern const DEV_MENU_ITEM wen_write_list[]; 

extern const DEV_MENU_ITEM wen_diagnostic_write_list[];  

extern UNS_32 wen_sizeof_read_list();

extern UNS_32 wen_sizeof_write_list();

extern UNS_32 wen_sizeof_diagnostic_write_list();

extern void wen_initialize_profile_vars( DEV_STATE_STRUCT *dev_state );

extern void wen_initialize_detail_struct( DEV_STATE_STRUCT *dev_state );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

