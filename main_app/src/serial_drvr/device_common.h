/*  file = device_common.h                    05.18.2015  mpd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_DEVICE_COMMON_H
#define INC_DEVICE_COMMON_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"comm_mngr.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/8/2015 mpd : Device State Machine States.
#define DEV_WAITING_FOR_RESPONSE						( 1000 )	
#define DEV_WAITING_FOR_NETWORK							( 2000 )	
#define DEV_WAITING_FOR_READY							( 3000 )	
#define DEV_DEVEX_READ									( 4000 )
#define DEV_DEVEX_WRITE									( 5000 )
#define DEV_DEVEX_FINISH								( 6000 )

#define DEV_TICKS_PER_MS								( 5 )

// 4/23/2015 mpd : The initial large response from the GR PremierWave is 1324 bytes. At 9600 baud, that's 1.38 seconds 
// if the GR streamed like an EN. It does not. There are significant hesitations between sections of the response. Some
// WRITE steps are pushing 60 seconds before the response.  
#define DEV_COMM_TIMER_MS								( 90000 )

// 5/14/2015 mpd : The GR has some commands that will not respond (e.g. baud rate change). Set a short timer for a 
// quicker timeout.
#define DEV_NO_RESP_TIMER_MS							( 100 )

// 6/5/2015 mpd : The device must be power-cycled to enter programming mode. These defines control how long the device 
// needs to be powered-off, how often to test for power-off, and how long to wait before giving up. 
// 
// The WEN device was failing to respond for the first 2 attempts after system boot when the off time was 200ms. A 500ms 
// delay seems to have fixed the problem.
// 
// The power-down command does not always shut off power immediately if the controller has just rebooted. Test for 
// power off before starting the off timer. The test is being is executed within the context of the comm_mngr task. 
// The test time plus off time cannot exceed the 15 second watchdog!
#define DEV_POWER_OFF_TIMER_MS							( 500 )
#define DEV_POWER_TEST_TIMER_MS							( 250 )
#define DEV_POWER_TEST_MAX_MS				 			( 10000 )	
#define DEV_POWER_ON_TIMER_MS				 			( 500 )	

// 5/5/2015 mpd : We need to auto-repeat the "!" key after a power cycle until the GR responds. Microsoft does not seem
// to publish the default auto-repeat value, and the control is a slider bar with no scale. Let's guess at a reasonable
// 8 repeats a second. Set a limit on how many times we will loop through this cycle before giving up. It takes 12 - 13 
// seconds before the GR responds to the "!".
#define DEV_AUTO_REPEAT_TIMER_MS						( 125 )

// 3/29/2016 ajv : For COBOX-based -EN and -WEN devices, all it needs is "xxx" or "yyy" sent
// as single characters. It will respond with 142 characters that cannot arrive in 125ms at
// a 9600 baud rate. Slow down the send frequency so we don't chop up our response
// (recognition and extraction breaks if we do).
#define LANTRONIX_COBOX_AUTO_REPEAT_TIMER_MS	   						( 500 )

// 5/7/2015 mpd : The GR is typically responding in 10.8 to 10.9 seconds (86 to 87 counts).
#define DEV_MAX_START_LOOP_COUNT						( ( 1000 / DEV_AUTO_REPEAT_TIMER_MS ) * 15 )

// 6/5/2015 mpd : A a normal prompt is usually seen in PuTTY within 2 seconds under controller operation. There is no 
// double prompt for a human using PuTTY. 
//#define DEV_READY_REPEAT_TIMER_MS						( 250 )
//#define DEV_MAX_READY_LOOP_COUNT						( ( 1000 / DEV_READY_REPEAT_TIMER_MS ) * 15 )

#define DEV_NETWORK_TIMER_MS							( 1000 )

// 6/11/2015 mpd : Lantronix support has confirmed the need for delays between commands (this confirmation applied to 
// WEN devices). They also indicated some commands require longer times to complete than others. Apparently they return 
// a prompt when processing is not complete on their end. Sending commands with no delay can even overflow their input 
// buffer. 
// A global 1000ms separation between commands gave us 90% reliability with occasional command execution failures around 
// the end of WEP or beginning of WPA settings. The "write" command failed most often. It would simply timeout. At 
// 2000ms, only the "write" was observed failing (infrequently). Navigation commands failures have never been seen.  
// These defines allow us to modify timing based on the specific command. 
//#define DEV_COMMAND_SEPARATION_TIMER_MS				( 1000 )
#define DEV_COMMAND_SEPARATION_BASE_TIMER_MS			( 500 )
#define DEV_COMMAND_SEPARATION_NO_EXTRA_TIMER_MS		( 0 )		// 500 Total
#define DEV_COMMAND_SEPARATION_SHORT_TIMER_MS			( 500 )		// 1000 Total
#define DEV_COMMAND_SEPARATION_MEDIUM_TIMER_MS			( 1500 )	// 2000 Total
#define DEV_COMMAND_SEPARATION_LONG_TIMER_MS			( 2500 )	// 3000 Total
#define DEV_COMMAND_SEPARATION_FOR_NAVIGATION_CMD		DEV_COMMAND_SEPARATION_LONG_TIMER_MS		
#define DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD			DEV_COMMAND_SEPARATION_MEDIUM_TIMER_MS	

// 6/10/2015 mpd : The first command sent to the device after successful CLI entry has frequently had problems during
// development. This define allows debugging to focus on this point in the process. 
#define DEV_FIRST_INDEX_AFTER_ENTRY						( 2 )

// 6/4/2015 mpd : New plan. Don't wait for the network. Get all the info we can from commands that don't require a
// network connection. 
// 5/7/2015 mpd : The network is typically connecting in 30 seconds. Allow 60 to be sure. DTR (CD) is not functional in
// CLI mode, so there is no feedback on the connection status.
//#define DEV_NETWORK_LOOP_COUNT						( ( 1000 / ( DEV_NETWORK_TIMER_MS + DEV_SUBSEQUENT_CMD_TIMER ) ) * 60 )

#define DEV_PROGRAMMING_BAUD_RATE ( 9600 )
 
// 6/10/2015 mpd : The error handler stores the first error number and the text provided. These are displayed in a 
// dialog box at the end of processing.  
#define DEV_ERROR_NONE									( 0 )
#define DEV_ERROR_ENTER_MODE							( 1 )
#define DEV_ERROR_EXECUTING_COMMAND						( 2 )
#define DEV_ERROR_COMM_MNGR_TIMEOUT						( 3 )
#define DEV_ERROR_COMM_MNGR_UNKNOWN						( 4 )
#define DEV_ERROR_OLD_FIRMWARE							( 5 )
#define DEV_ERROR_XCR_DUMP_DEVICE						( 6 )

#define DEV_ERROR_LEVEL_INFO							( 1 )
#define DEV_ERROR_LEVEL_SERIOUS							( 2 )
#define DEV_ERROR_LEVEL_FATAL							( 3 )

#define DEV_NULL_LEN 									( 1 )

// 3/13/2015 mpd : These are not derived from Lantronix responses.
#define DEV_EMPTY_STR									( 0xFF ) // Special case for initial I/O cycle. 
#define DEV_EMPTY_STR_LEN								( 1 ) 	 // Length > 0 is required. 
#define DEV_CRLF_STR									( "\r\n" )
#define DEV_CRLF_STR_LEN								( 2 + DEV_NULL_LEN )
#define DEV_CR_STR										( "\r" )
#define DEV_CR_STR_LEN									( 1 + DEV_NULL_LEN )
#define DEV_LF_STR										( "\n" )
#define DEV_LF_STR_LEN									( 1 + DEV_NULL_LEN )
#define DEV_NULL_IP_ADD_STR				 				( "0.0.0.0" ) 
#define DEV_NULL_IP_ADD_LEN				 				( 7 + DEV_NULL_LEN ) 
#define DEV_IP_ADD_LEN 					 				( 15 + DEV_NULL_LEN ) 
// 9/24/2015 mpd : FogBugz #3177: Added CIDR length.
#define DEV_IP_ADD_CIDR_LEN				 				( DEV_IP_ADD_LEN + 3 ) // "xxx.xxx.xxx.xxx/24" 
#define DEV_IP_ADDRESS_OCTET_LEN						( 3 + DEV_CRLF_STR_LEN )
#define DEV_CALSENSE_IP_ADD_STR							"64.73.242.99"
#define DEV_NO_TEXT_STR									""
#define DEV_NOT_SET_STR									"Not Set"

#define DEV_PVS_TOKEN_STR							    ( "PVS" )
#define DEV_PVS_TOKEN_LEN    							( 3 + DEV_NULL_LEN )

#define DEV_IDLE_OPERATION								( 73 )					// "I"
#define DEV_READ_OPERATION								( 82 )					// "R"
#define DEV_READ_MONITOR_OPERATION						( 77 )					// "M"	Special case for EN.
#define DEV_WRITE_OPERATION								( 87 )					// "W"
#define DEV_IDLE_TEXT_STR								( "IDLE" )
#define DEV_READ_TEXT_STR								( "READ" )
#define DEV_SETUP_READ_TEXT_STR							( "S-READ" )			// 	Special case for EN.
#define DEV_MONITOR_READ_TEXT_STR						( "M-READ" )			// 	Special case for EN.
#define DEV_WRITE_TEXT_STR								( "WRITE" )
#define DEV_DHCP_WRITE_TEXT_STR							( "D-WRITE" )			// 	Special case for EN.
#define DEV_STATIC_WRITE_TEXT_STR						( "S-WRITE" )			// 	Special case for EN.
#define DEV_OPERATION_STR_LEN							( 8 )
#define DEV_INFO_TEXT_LEN								( 39 + DEV_NULL_LEN ) 	// This should match the size of GuiVar_SRInfoText.  
#define DEV_PROGRESS_TEXT_LEN							( 39 + DEV_NULL_LEN ) 	// This should match the size of GuiVar_SRProgressText.  
#define DEV_ALERT_MESSAGE_LEN							( 60 + DEV_NULL_LEN )   // 60 is about the limit of the alert screen width. More requires scrolling.  
#define DEV_FW_VER_LEN								   	( 14 + DEV_NULL_LEN )	// "ENABLE SHOW size, plus 1 extra digit in each field.

// 5/27/2015 mpd : The first 3 version fields are common for GR and WEN. A table would be an overkill at this time. 
// Work with the difference in section 4. WEN Version values are defined in "device_WEN_PremierWaveXN".

// 5/18/2015 mpd : Version testing was written for GR. Make a table if other device types need it.
//#define DEV_FW_VER_MIN_1			  				 	( 7 )					// This is the minimum firmware version we require (UNS_32). 
//#define DEV_FW_VER_MIN_2			  				 	( 9 )					// This is the minimum firmware version we require (UNS_32).
//#define DEV_FW_VER_MIN_3			   					( 0 )					// This is the minimum firmware version we require (UNS_32).
//#define DEV_FW_VER_MIN_4			   					"3A2"					// This is the minimum firmware version we require (string).
#define DEV_FW_VER_4_LEN			   					( 5 + DEV_NULL_LEN )	// Length of the 4th section plus room to grow.

//4/15/2015 mpd : Baud Rate strings.
#define DEV_BAUD_RATE_CMD_9600							( "9600\r" )
#define DEV_BAUD_RATE_CMD_19200							( "19200\r" )
#define DEV_BAUD_RATE_CMD_38400							( "38400\r" )
#define DEV_BAUD_RATE_CMD_57600							( "57600\r" )
#define DEV_BAUD_RATE_CMD_76800							( "76800\r" )
#define DEV_BAUD_RATE_CMD_115200						( "115200\r" )
#define DEV_BAUD_RATE_CMD_230400						( "230400\r" )

// 5/6/2015 mpd : The "enable->configure->cellular->show status" response is 329 bytes with no SIM card. 
	
// NO SIM RESPONSE:
//(config-cellular)#show status
//   SIM status          : connection error												<- Needed
//   IMSI                : SIM not inserted
//   Operator            : SIM not inserted
//   Network Status      : SIM not inserted												<- Needed
//   Packet Domain Status: SIM not inserted												<- Needed
//   Signal Strength     : SIM not inserted												<- Needed
//   Temperature         : 0.0C
//01234567890123456789012345678901234567890123456789012345678901234567890				<- Ruler
 
// SIM PRESENT RESPONSE:
//(config-cellular)#show status
//   SIM status          : present														<- Needed
//   IMSI                : 310410734050811
//   Operator            : AT&T
//   Network Status      : registered on home network									<- Needed
//   Packet Domain Status: registered on home network									<- Needed
//   Signal Strength     : -87 dBm														<- Needed
//   Temperature         : 33.0C
 
// 5/4/2015 mpd : GUI Var info from "login->enable->configure->cellular->show status".
// GuiVar_GRSIMState
// GuiVar_GRNetworkState
// GuiVar_GRGPRSStatus
// GuiVar_GRRSSI

// 5/4/2015 mpd : NOTE: Title and anchor strings have been chosen with as few unique characters as possible to reduce 
// data space. Lengths have also been limited.

// 5/4/2015 mpd : The response title string is the same as the first anchor text. Skip it.
//#define GR_C_SHOW_TITLE_STR		   		"SIM"						// "SIM status          :"

#define DEV_C_SHOW_SIM_STATUS_STR		   	"SIM s"						// "SIM status          :"
#define DEV_C_SHOW_SIM_STATUS_OFFSET		( 22 )
#define DEV_C_SHOW_SIM_STATUS_LEN			( 30  + DEV_NULL_LEN )		// The maximum see so far is 26

#define DEV_C_SHOW_NET_STATUS_STR		   	"Network S"					// "Network Status      :"
#define DEV_C_SHOW_NET_STATUS_OFFSET		( 22 )
#define DEV_C_SHOW_NET_STATUS_LEN		   	( 30  + DEV_NULL_LEN )		// The maximum see so far is 26

#define DEV_C_SHOW_GPRS_STR		    		"Packet D"					// "Packet Domain Status:"
#define DEV_C_SHOW_GPRS_OFFSET		    	( 22 )
#define DEV_C_SHOW_GPRS_LEN			   		( 30  + DEV_NULL_LEN )		// The maximum see so far is 26

#define DEV_C_SHOW_RSSI_STR		    		"Signal S"					// "Signal Strength     :"
#define DEV_C_SHOW_RSSI_OFFSET		    	( 22 )
#define DEV_C_SHOW_RSSI_LEN			   		( 30  + DEV_NULL_LEN )		// The maximum see so far is 26

#define DEV_C_SHOW_CONN_ERR_STR				"connection e" 				// "connection error"
#define DEV_C_SHOW_SIM_PRESENT_STR			"present"	 				// "present"
#define DEV_C_SHOW_CONN_ERR_LEN				( 12 )	 					// Length to compare

#define DEV_C_SHOW_NO_SIM_STR				"SIM not i"	 				// "SIM not inserted"
#define DEV_C_SHOW_REGISTERED_STR			"registere"	 				// "registered on home network"
#define DEV_C_SHOW_NO_SIM_LEN				( 9 )	 	 				// Length to compare

// 5/4/2015 mpd : GR PremierWave Programming per "http://odin.domain.calsense.com/fogbugz/default.asp?W150". These 
// defines also apply to WEN devices.

// 5/4/2015 mpd : These are termination strings (how RCVD_DATA recognizes the end of the expected response). They have
// been chosen with as few unique characters as possible to reduce data space. Keep the strings in alphabetic order to 
// make collisions easier to spot.
#define DEV_NO_TITLE_STR					""						   	            // Used for the initial read and write title string.
#define DEV_TERM_START_STR					"!"						   	            // "!"			<- Prompt upon power up with "!" input.
#define DEV_TERM_PROMPT_STR					">>"					   	            // ">>"			<- Prompt after "xyz" input.
																			
// 6/3/2015 mpd : This catch-all string provides no confirmation of context. Use sparingly.
#define DEV_TERM_RESP_STR					")#"									// "(enable)#" 
																					
// 6/5/2015 mpd : The device responds with a double "##" for the first few seconds after power up. It does not behave
// well during this time. 
#define DEV_TERM_NOT_READY_STR				")##"									// "(enable)##" 

#define DEV_TERM_CONN_STR					"-connect:1)#"			   	            // "(tunnel-connect:1)#"
#define DEV_TERM_ENABLE_STR					"ble)#"					   	            // "(enable)#"
#define DEV_TERM_PRO_BASIC_STR				"c:default_infrastructure_profile)#"	// "(config-profile-basic:default_infrastructure_profile)#"
#define DEV_TERM_PRO_ADV_STR				"d:default_infrastructure_profile)#"	// "(config-profile-advanced:default_infrastructure_profile)#"
#define DEV_TERM_MODEM_STR					"dem:1)#"				   	            // "(tunnel-modem:1)#"
#define DEV_TERM_IF_ETH_STR					"eth0)#"				   	            // "(config-if:eth0)#"
#define DEV_TERM_CONFIG_STR					"fig)#"					   	            // "(config)#"
#define DEV_TERM_PRO_STR					"g-profiles)#"							// "(config-profiles)#"
#define DEV_TERM_LINK_STR					"k:wwan0)#"				   	            // "(config-wwan-link:wwan0)#"
#define DEV_TERM_CELL_STR					"lar)#"					   	            // "(config-cellular)#"
#define DEV_TERM_LOG_STR					"log)#"					   	            // "(config-diagnostics-log)#"
#define DEV_TERM_TUNNEL_STR					"nel:1)#"				   	            // "(tunnel:1)#"
#define DEV_TERM_LINE_STR					"ne:1)#"				   	            // "(line:1)#"
#define DEV_TERM_LINE_2_STR					"ne:2)#"				   	            // "(line:2)#"
#define DEV_TERM_CLOCK_STR					"ock)#"					   	            // "(config-clock)#"
#define DEV_TERM_REBOOT_STR					"ot ..."				   	            // "waiting for reboot ..."
#define DEV_TERM_PRO_WEP_STR				"p:default_infrastructure_profile)#"	// "(config-profile-security-wep:default_infrastructure_profile)#"
#define DEV_TERM_ACCEPT_STR					"pt:1)#"				   	            // "(tunnel-accept:1)#"
#define DEV_TERM_RELOAD_STR					"s/no)?"				   	            // "Are you sure (yes/no)?".
#define DEV_TERM_DISCONN_STR				"sconnect:1)#"			   	            // "(tunnel-disconnect:1)#"
#define DEV_TERM_HOST_STR					"st:1:1)#"				   	            // "(tunnel-connect-host:1:1)#"
#define DEV_TERM_DIAG_STR					"tics)#"				   	            // "(config-diagnostics)#"
#define DEV_TERM_IF_WLAN_STR				"wlan0)#"				   	            // "(config-if:wlan0)#"
#define DEV_TERM_IF_WWAN_STR				"wwan0)#"				   	            // "(config-wwan:wwan0)#"
#define DEV_TERM_PRO_WPAX_STR				"x:default_infrastructure_profile)#"	// "(config-profile-security-wpax:default_infrastructure_profile)#"
#define DEV_TERM_XML_STR					"xml)#"					   	            // "(xml)#"
#define DEV_TERM_PRO_SEC_STR				"y:default_infrastructure_profile)#"	// "(config-profile-security:default_infrastructure_profile)#"
#define DEV_TERM_PRO_WEP_K1_STR				"y:default_infrastructure_profile:1)#"	// "(config-profile-security-wep-key:default_infrastructure_profile:1)#" 
#define DEV_TERM_PRO_WEP_K2_STR				"y:default_infrastructure_profile:2)#"	// "(config-profile-security-wep-key:default_infrastructure_profile:2)#" 
#define DEV_TERM_PRO_WEP_K3_STR				"y:default_infrastructure_profile:3)#"	// "(config-profile-security-wep-key:default_infrastructure_profile:3)#" 
#define DEV_TERM_PRO_WEP_K4_STR				"y:default_infrastructure_profile:4)#"	// "(config-profile-security-wep-key:default_infrastructure_profile:4)#" 
#define DEV_TERM_NO_RESP_STR				""						   	            //  No response is expected.
#define DEV_TERM_PRO_WEP_KX_STR				DEV_TERM_RESP_STR						// ")#" 
#define DEV_TERM_IF_ETH0_STR				"eth0)#"				   	            // "(config-if:eth0)#"

// "ENABLE SHOW" COMMAND - NO SIM RESPONSE:
//   Interface           : wwan0
//   Packet Domain Status: SIM not inserted
//   IP Address          : <None>														<- Needed

// "ENABLE SHOW" COMMAND - SIM PRESENT RESPONSE:
//   Interface           : wwan0
//   Packet Domain Status: registered on home network
//   IP Address          : 10.10.3.40/32												<- Needed

// 4/23/2015 mpd : The GR/WEN PremierWave response has duplicate key words for IP Address. Analyze from the "Packet 
// Domain Status:" line to extract the one associated with "wwan0". Without this, we get the "eth0" IP address.
#define DEV_E_SHOW_WWAN0_STR		    	"Packet D"					// "Packet Domain Status:"

#define DEV_E_SHOW_IP_ADD_STR			   	"IP A"						// "IP Address          :"
#define DEV_E_SHOW_IP_ADD_OFFSET		    ( 22 )
#define DEV_E_SHOW_IP_ADD_LEN			   	( 15 + DEV_NULL_LEN )		

#define DEV_E_SHOW_IP_NOT_SET_STR			"<None>"	 				// "<None> [<DHCP>]"
#define DEV_E_SHOW_IP_NOT_SET_LEN			( 6 )	 	 				// Length to compare

//  WEN "ENABLE SHOW" COMMAND:
//(enable)#show
//Product Information:
//   Product Type        : Lantronix PremierWave XN (premierwave_xn)
//   FW Version / Date   : 7.9.0.0R19 / Sep 30 22:02:39 PDT 2014
//   Radio FW Version    : 3.2.12.0/1.1.6.71/6.134
//   Current Date/Time   : Wed Dec 31 16:00:49 PST 1969
//   Temperature         : 26.0C
//   Serial Number       : 0080A39A2517
//   Uptime              : 0 days 00:00:50
//   Perm. Config        : saved
//   Region              : United States
//
//Alerts:
//   Alarm               : eth0 link state change
//   Duration            : 0 days 00:00:03
//
//   Alarm               : wlan0 link state change
//   Duration            : 0 days 00:00:03
//
//   Alarm               : backup power change
//   Duration            : 0 days 00:00:00
//
//Power Status:
//   Main Power:         : Up
//   Backup Power:       : Down
//
//Network Status:
//   Primary DNS         : <None>
//   Secondary DNS       : <None>
//
//   Interface           : eth0
//   Link                : Auto 10/100 Mbps Auto Half/Full (No link)
//   MAC Address         : 00:80:a3:9a:25:17							<- Needed
//   Hostname            : <None> [<DHCP>]
//   Domain              : <None> [<DHCP>]
//   IP Address          : <None> [<DHCP>]
//   Network Mask        : <None> [<DHCP>]
//   Default Gateway     : <None> [<DHCP>]
//   MTU                 : 1500
//
//   Interface           : wlan0
//   Link                : DISCONNECTED
//   MAC Address         : 00:80:a3:9a:25:18							<- Needed
//   Hostname            : <None> [<DHCP>]
//   Domain              : <None> [<DHCP>]
//   IP Address          : <None> [<DHCP>]
//   Network Mask        : <None> [<DHCP>]
//   Default Gateway     : <None> [<DHCP>]
//   MTU                 : 1500
//
//Line 1:
//   RS232, 115200, None, 8, 1, Hardware [CLI]
//   Tunnel Connect Mode: Waiting, Accept Mode: Disabled
//Line 2:
//   RS232, 9600, None, 8, 1, None
//   Tunnel Connect Mode: Disabled, Accept Mode: Waiting
//
//VPN Status:
//   Status              : Disabled
//   IP Address          : <None>
//(enable)#
// 5/28/2015 mpd : There are multiple "eth0" and "wlan0" strings in the response. Locate using the long string.
#define DEV_E_SHOW_ETH0_STR			    	"Interface           : eth0"	// "Interface           : eth0"
#define DEV_E_SHOW_WLAN0_STR		    	"Interface           : wlan0"	// "Interface           : wlan0"
#define DEV_E_SHOW_MAC_ADD_STR		    	"MAC Ad" 						// "MAC Address         : 00:80:a3:9a:25:18"
#define DEV_E_SHOW_MAC_ADD_OFFSET		    ( 22 )
#define DEV_E_SHOW_MAC_ADD_LEN			   	( 17 + DEV_NULL_LEN )		

//  WEN "XCR DUMP INTERFACE" COMMANDS:
//(xml)#xcr dump interface:wlan0
//<?xml version="1.0" standalone="yes"?>
//<!-- Automatically generated XML -->
//<!DOCTYPE configrecord [
//   <!ELEMENT configrecord (configgroup+)>
//   <!ELEMENT configgroup (configitem+,configgroup*)>
//   <!ELEMENT configitem (value+)>
//   <!ELEMENT value (#PCDATA)>
//   <!ATTLIST configrecord version CDATA #IMPLIED>
//   <!ATTLIST configgroup name CDATA #IMPLIED>
//   <!ATTLIST configgroup instance CDATA #IMPLIED>
//   <!ATTLIST configitem name CDATA #IMPLIED>
//   <!ATTLIST configitem instance CDATA #IMPLIED>
//   <!ATTLIST value name CDATA #IMPLIED>
//]>
//<configrecord version = "0.1.0.0T0">
//   <configgroup name = "interface" instance = "wlan0">
//       <configitem name = "state">
//           <value>enable</value>
//       </configitem>
//       <configitem name = "bootp">
//           <value>disable</value>
//       </configitem>
//       <configitem name = "dhcp">
//           <value>enable</value>
//       </configitem>
//       <configitem name = "priority">
//           <value>1 </value>
//       </configitem>
//       <configitem name = "ip address">
//           <value>&amp;#60;None&amp;#62;</value>
//       </configitem>
//       <configitem name = "default gateway">
//           <value>&amp;#60;None&amp;#62;</value>
//       </configitem>
//       <configitem name = "hostname">
//           <value></value>
//       </configitem>
//       <configitem name = "domain">
//           <value></value>
//       </configitem>
//       <configitem name = "dhcp client id">
//           <value></value>
//       </configitem>
//       <configitem name = "primary dns">
//           <value>&amp;#60;None&amp;#62;</value>
//       </configitem>
//       <configitem name = "secondary dns">
//           <value>&amp;#60;None&amp;#62;</value>
//       </configitem>
//       <configitem name = "mtu">
//           <value>1500 bytes</value>
//       </configitem>
//   </configgroup>
//</configrecord>
//(xml)#
#define DEV_X_DUMP_DHCP_STR	 		  		"dhcp"						// "<configitem name = "dhcp">"
#define DEV_X_DUMP_IP_ADD_STR		  		"ip address"				// "<configitem name = "ip address">"
#define DEV_X_DUMP_DEF_GW_STR		  		"gateway"					// "<configitem name = "default gateway">"
#define DEV_X_DUMP_HOSTNAME_STR		  		"hostname"					// "<configitem name = "hostname">"
// 9/22/2015 mpd : FogBugz #3163: Added DEV_X_DUMP_NONE_STR and DEV_X_DUMP_NONE_FIND_LEN.
#define DEV_X_DUMP_NONE_STR			  		"None"						// "<value>&amp;#60;None&amp;#62;</value>">"
#define DEV_X_DUMP_NONE_FIND_LEN	  		( 48 )						// "ip address">           <value>&amp;#60;None&a"
#define DEV_DISABLED_STR					"disable"
#define DEV_ENABLED_STR						"enable"

// 5/15/2015 mpd : These are generic for XML parsing.
#define DEV_X_DUMP_VALUE_STR			   	"<value>"					// "<value>0080A39A5CC5</value>"
#define DEV_X_DUMP_VALUE_OFFSET		   		( 7 )
#define DEV_X_DUMP_DELIMITER			   	"<"
#define DEV_X_DUMP_DELIMITER_LEN		   	( 1 ) 

// 9/24/2015 mpd : FogBugz #3177: Added the slash delimiter for CIDR IP address parsing.
#define DEV_X_DUMP_SLASH_DELIMITER			"/"

//  WEN "XCR DUMP WLAN0 PROFILE COMMANDS:
//(xml)#xcr dump " wlan profile:calsense_hq"
//<?xml version="1.0" standalone="yes"?>
//<!-- Automatically generated XML -->
//<!DOCTYPE configrecord [
//   <!ELEMENT configrecord (configgroup+)>
//   <!ELEMENT configgroup (configitem+,configgroup*)>
//   <!ELEMENT configitem (value+)>
//   <!ELEMENT value (#PCDATA)>
//   <!ATTLIST configrecord version CDATA #IMPLIED>
//   <!ATTLIST configgroup name CDATA #IMPLIED>
//   <!ATTLIST configgroup instance CDATA #IMPLIED>
//   <!ATTLIST configitem name CDATA #IMPLIED>
//   <!ATTLIST configitem instance CDATA #IMPLIED>
//   <!ATTLIST value name CDATA #IMPLIED>
//]>
//<configrecord version = "0.1.0.0T0">
//   <configgroup name = "wlan profile" instance = "calsense_hq">
//       <configitem name = "profile type">
//           <value>User Defined</value>
//       </configitem>
//       <configitem name = "interface">
//           <value></value>
//       </configitem>
//       <configitem name = "priority">
//           <value>0 </value>
//       </configitem>
//       <configitem name = "bssid">
//           <value></value>
//       </configitem>
//       <configitem name = "basic">
//           <value name = "network name">calsense_hq</value>
//           <value name = "topology">Infrastructure</value>
//           <value name = "mode">802.11bgn</value>
//           <value name = "scan dfs channels">enable</value>
//           <value name = "state">enable</value>
//           <value name = "channel">1 </value>
//       </configitem>
//       <configitem name = "advanced">
//           <value name = "tx data rate maximum">MCS7</value>
//           <value name = "tx data rate">Auto-reduction</value>
//           <value name = "tx power maximum">17 dBm</value>
//           <value name = "antenna diversity">Antenna 1</value>
//           <value name = "max missed beacons">20 beacons</value>
//           <value name = "power management">disable</value>
//           <value name = "power management interval">1 beacons (100 ms each)</value>
//       </configitem>
//       <configitem name = "security">
//           <value name = "suite">WPA2</value>
//           <value name = "key type">Passphrase</value>
//           <value name = "passphrase"><!-- configured and ignored --></value>
//           <value name = "wep authentication">Open</value>
//           <value name = "wep key size">40</value>
//           <value name = "wep tx key index">1</value>
//           <value name = "wep key 1"></value>
//           <value name = "wep key 2"></value>
//           <value name = "wep key 3"></value>
//           <value name = "wep key 4"></value>
//           <value name = "wpax authentication">PSK</value>
//           <value name = "wpax key"><!-- configured and ignored --></value>
//           <value name = "wpax ieee 802.1x">EAP-TTLS</value>
//           <value name = "wpax eap-ttls option">EAP-MSCHAPV2</value>
//           <value name = "wpax peap option">EAP-MSCHAPV2</value>
//           <value name = "wpax username"></value>
//           <value name = "wpax password"></value>
//           <value name = "wpax encryption">CCMP, TKIP, WEP</value>
//           <value name = "wpax validate certificate">enable</value>
//           <value name = "wpax credentials"></value>
//       </configitem>
//   </configgroup>
//</configrecord>
//(xml)#xcr dump " wlan profile:ccccccc"
//XML export failed
//XML export failed
//Error executing command

#define DEV_X_CP_DUMP_SSID_STR	 		  	"bssid"						// "<configitem name = "bssid">"
#define DEV_X_CP_DUMP_NET_NAME_STR	   		"network name"				// "<value name = "network name">calsense_hq</value>
#define DEV_X_CP_DUMP_MODE_STR	   			"mode"						// "<value name = "mode">802.11bgn</value>
#define DEV_X_CP_DUMP_STATE_STR	   			"state"						// "<value name = "state">enable</value>
#define DEV_X_CP_DUMP_ANT_DIV_STR	   		"diversity"					// "<value name = "antenna diversity">Antenna 1</value>
#define DEV_X_CP_DUMP_SUITE_STR	   			"suite"						// "<value name = "suite">WPA2</value>
#define DEV_X_CP_DUMP_SUITE_WEP_STR  		"WEP"						// "<value name = "suite">WEP</value>"
#define DEV_X_CP_DUMP_KEY_TYPE_STR	   		"key type"					// "<value name = "key type">Passphrase</value>
#define DEV_X_CP_DUMP_PASSPHRASE_STR	   	"passphrase"				// "<value name = "passphrase"><!-- configured and ignored --></value>
#define DEV_X_CP_DUMP_WEP_AUTH_STR	   		"p authentication"			// "<value name = "wep authentication">Open</value>
#define DEV_X_CP_DUMP_WEP_KEY_SIZE_STR	   	"p key size"				// "<value name = "wep key size">40</value>
#define DEV_X_CP_DUMP_WEP_TX_KEY_IDX_STR   	"p tx key index"			// "<value name = "wep tx key index">1</value>
#define DEV_X_CP_DUMP_WEP_KEY_STR	    	"p key "					// "<value name = "wep key 1"></value>
#define DEV_X_CP_DUMP_WEP_KEY_1_STR	    	"p key 1"					// "<value name = "wep key 1"></value>
#define DEV_X_CP_DUMP_WEP_KEY_2_STR	    	"p key 1"					// "<value name = "wep key 2"></value>
#define DEV_X_CP_DUMP_WEP_KEY_3_STR	    	"p key 1"					// "<value name = "wep key 3"></value>
#define DEV_X_CP_DUMP_WEP_KEY_4_STR	    	"p key 1"					// "<value name = "wep key 4"></value>
#define DEV_X_CP_DUMP_WPAX_AUTH_STR	    	"x authentication"			// "<value name = "wpax authentication">PSK</value>
#define DEV_X_CP_DUMP_WPAX_KEY_STR	    	"x key"						// "<value name = "wpax key"><!-- configured and ignored --></value>
#define DEV_X_CP_DUMP_WPAX_IEEE_STR	    	"ieee 802.1x"				// "<value name = "wpax ieee 802.1x">EAP-TTLS</value>
#define DEV_X_CP_DUMP_WPAX_EAP_TTLS_STR	   	"ttls option"				// "<value name = "wpax eap-ttls option">EAP-MSCHAPV2</value>
#define DEV_X_CP_DUMP_WPAX_PEAP_STR	   		"peap option"				// "<value name = "wpax peap option">EAP-MSCHAPV2</value>
#define DEV_X_CP_DUMP_WPAX_U_NAME_STR	   	"username"					// "<value name = "wpax username"></value>
#define DEV_X_CP_DUMP_WPAX_P_WORD_STR	   	"password"					// "<value name = "wpax password"></value>
#define DEV_X_CP_DUMP_WPAX_ENCRIPT_STR	   	"encryption"				// "<value name = "wpax encryption">TKIP</value>
#define DEV_X_CP_DUMP_WPAX_E_CCMP_STR  		"CCMP"						// "<value name = "wpax encryption">CCMP, TKIP, WEP</value>"
#define DEV_X_CP_DUMP_WPAX_E_TKIP_STR  		"TKIP"						// "<value name = "wpax encryption">CCMP, TKIP, WEP</value>"
#define DEV_X_CP_DUMP_WPAX_E_WEP_STR  		"WEP"						// "<value name = "wpax encryption">CCMP, TKIP, WEP</value>"
#define DEV_X_CP_DUMP_WPAX_VALID_STR	   	"certificate"				// "<value name = "wpax validate certificate">enable</value>
#define DEV_X_CP_DUMP_WPAX_CRED_STR	   		"credentials"				// "<value name = "wpax credentials"></value>
#define DEV_X_CP_DUMP_FAILURE_STR		   	"Error executing command"	// If profile does not exist: "Error executing command"
#define DEV_X_CP_DUMP_IGNORE_STR			"configured and ignored"   	// <value name = "passphrase"><!-- configured and ignored --></value>

#define DEV_X_CP_DUMP_GREATER_STR		   	">"						// "<value name = "name goes here">value follows greater symbol</value>
#define DEV_X_CP_DUMP_GREATER_OFFSET	   	( 1 )
			  
			  
//  WEN "XSR DUMP INTERFACE" COMMANDS:
//(xml)#xsr dump interface:eth0
//<?xml version="1.0" standalone="yes"?>
//<!-- Automatically generated XML -->
//<!DOCTYPE statusrecord [
//   <!ELEMENT statusrecord (statusgroup+)>
//   <!ELEMENT statusgroup (statusitem+,statusgroup*)>
//   <!ELEMENT statusitem (value+)>
//   <!ELEMENT value (#PCDATA)>
//   <!ATTLIST statusrecord version CDATA #IMPLIED>
//   <!ATTLIST statusgroup name CDATA #IMPLIED>
//   <!ATTLIST statusgroup instance CDATA #IMPLIED>
//   <!ATTLIST statusitem name CDATA #IMPLIED>
//   <!ATTLIST value name CDATA #IMPLIED>
//]>
//<statusrecord version = "0.1.0.0T0">
//   <statusgroup name = "interface" instance = "eth0">
//       <statusitem name = "generic">
//           <value name = "status">No Link</value>
//       </statusitem>
//       <statusitem name = "ip address">
//           <value></value>							<- Needed ?
//       </statusitem>
//       <statusitem name = "network mask">
//           <value></value>							<- Needed ?
//       </statusitem>
//       <statusitem name = "default gateway">
//           <value></value>							<- Needed ?
//       </statusitem>
//       <statusitem name = "Receive">
//           <value name = "bytes">0</value>
//           <value name = "packets">0</value>
//           <value name = "errs">3</value>
//           <value name = "drop">0</value>
//           <value name = "fifo">0</value>
//           <value name = "frame">0</value>
//           <value name = "compressed">0</value>
//           <value name = "multicast">0</value>
//       </statusitem>
//       <statusitem name = "Transmit">
//           <value name = "bytes">238</value>
//           <value name = "packets">3</value>
//           <value name = "errs">0</value>
//           <value name = "drop">0</value>
//           <value name = "fifo">0</value>
//           <value name = "colls">0</value>
//           <value name = "carrier">0</value>
//           <value name = "compressed">0</value>
//       </statusitem>
//   </statusgroup>
//</statusrecord>
//(xml)#xsr dump interface:wlan0
//<?xml version="1.0" standalone="yes"?>
//<!-- Automatically generated XML -->
//<!DOCTYPE statusrecord [
//   <!ELEMENT statusrecord (statusgroup+)>
//   <!ELEMENT statusgroup (statusitem+,statusgroup*)>
//   <!ELEMENT statusitem (value+)>
//   <!ELEMENT value (#PCDATA)>
//   <!ATTLIST statusrecord version CDATA #IMPLIED>
//   <!ATTLIST statusgroup name CDATA #IMPLIED>
//   <!ATTLIST statusgroup instance CDATA #IMPLIED>
//   <!ATTLIST statusitem name CDATA #IMPLIED>
//   <!ATTLIST value name CDATA #IMPLIED>
//]>
//<statusrecord version = "0.1.0.0T0">
//   <statusgroup name = "interface" instance = "wlan0">
//       <statusitem name = "generic">
//           <value name = "status">No Link</value>
//       </statusitem>
//       <statusitem name = "ip address">
//           <value></value>							<- Needed
//       </statusitem>
//       <statusitem name = "network mask">
//           <value></value>							<- Needed
//       </statusitem>
//       <statusitem name = "default gateway">
//           <value></value>							<- Needed
//       </statusitem>
//       <statusitem name = "Receive">
//           <value name = "bytes">0</value>
//           <value name = "packets">0</value>
//           <value name = "errs">0</value>
//           <value name = "drop">0</value>
//           <value name = "fifo">0</value>
//           <value name = "frame">0</value>
//           <value name = "compressed">0</value>
//           <value name = "multicast">0</value>
//       </statusitem>
//       <statusitem name = "Transmit">
//           <value name = "bytes">0</value>
//           <value name = "packets">0</value>
//           <value name = "errs">0</value>
//           <value name = "drop">6</value>
//           <value name = "fifo">0</value>
//           <value name = "colls">0</value>
//           <value name = "carrier">0</value>
//           <value name = "compressed">0</value>
//       </statusitem>
//   </statusgroup>
//</statusrecord>
//(xml)#
#define DEV_X_DUMP_IP_ADD_ITEM_STR	   		"address"					// "<statusitem name = "ip address">"
#define DEV_X_DUMP_NET_MASK_ITEM_STR	   	"mask"						// "<statusitem name = "network mask">"
#define DEV_X_DUMP_GW_ADD_ITEM_STR	   		"gateway"					// "<statusitem name = "default gateway">"

// 5/15/2015 mpd : These are generic for XML parsing of GR and WEN device responses.
//(xml)#xcr dump device
//<?xml version="1.0" standalone="yes"?>
//<!-- Automatically generated XML -->
//<!DOCTYPE configrecord [
//   <!ELEMENT configrecord (configgroup+)>
//   <!ELEMENT configgroup (configitem+,configgroup*)>
//   <!ELEMENT configitem (value+)>
//   <!ELEMENT value (#PCDATA)>
//   <!ATTLIST configrecord version CDATA #IMPLIED>
//   <!ATTLIST configgroup name CDATA #IMPLIED>
//   <!ATTLIST configgroup instance CDATA #IMPLIED>
//   <!ATTLIST configitem name CDATA #IMPLIED>
//   <!ATTLIST configitem instance CDATA #IMPLIED>
//   <!ATTLIST value name CDATA #IMPLIED>
//]>
//<configrecord version = "0.1.0.0T0">
//   <configgroup name = "device">
//       <configitem name = "short name">
//           <value>premierwave_xc_hspa</value>
//       </configitem>
//       <configitem name = "long name">
//           <value>Lantronix PremierWave XC HSPA+</value>
//       </configitem>
//       <configitem name = "serial number">
//           <value>0080A39A5CC5</value>
//       </configitem>
//       <configitem name = "firmware version">
//           <value>7.9.0.3A2</value>
//       </configitem>
//   </configgroup>
//</configrecord>
//(xml)#>!

// 5/15/2015 mpd : These anchor text strings have been chosen with as few unique characters as possible to reduce 
// data space. 
#define DEV_X_DUMP_LNAME_ITEM_STR	   		"long name"					// "<configitem name = "long name">"
#define DEV_X_DUMP_S_NUM_ITEM_STR	   		"l number"					// "<configitem name = "serial number">"
#define DEV_X_DUMP_FW_VER_ITEM_STR	   		"e version"					// "<configitem name = "firmware version">"

#define DEV_X_DUMP_LNAME_STR_LEN	   		( 31 + DEV_NULL_LEN )		// This is the value currently in use
#define DEV_X_DUMP_S_NUM_STR_LEN	   		( 12 + DEV_NULL_LEN)		// "<configitem name = "serial number">"
#define DEV_X_DUMP_FW_VER_STR_LEN	   		( 14 + DEV_NULL_LEN )		// Currently in use, plus 1 extra digit in each field.

// 5/15/2015 mpd : Skip the "Lantronix" portion of the model string.
#define DEV_X_DUMP_LNAME_STR				"Lantronix"					// "<value>Lantronix PremierWave XC HSPA+</value>"
#define DEV_X_DUMP_LNAME_OFFSET		   		( 10 )

// 5/7/2015 mpd : APN is a critical field used to validate a Calsense WRITE operation.
// COMMAND: login->enable->configure->if wwan0->link->show
//(config-wwan-link:wwan0)#show
//Cellular Link wwan0 Configuration:
//   APN          : 10429.mcs
//   Username     :
//   Password     : (Not configured)
//   Dialup String: *99#
//   Roaming      : Enabled
#define DEV_L_SHOW_APN_STR			    	"APN "					// "APN          :"
#define DEV_L_SHOW_APN_OFFSET		    	( 15 )					
#define DEV_L_SHOW_APN_LEN			    	( 9 + DEV_NULL_LEN )	// This is the value currently in use

//(config-if:eth0)#exit
//(config)#wlan profiles
//(config-profiles)#show
//    1) default_infrastructure_profile (Enabled)
//    2) calsense_hq (Enabled)
//    3) default_adhoc_profile (Disabled)
//    4) Calsense (Enabled)
//(config-profiles)#edit default_infrastructure_profile
//(config-profile-basic:default_infrastructure_profile)#?
//advanced                                apply wlan
//channel <number>                        clrscrn
//default channel                         default mode
//default topology                        exit
//mode 802.11a only                       mode 802.11abgn
//mode 802.11an                           mode 802.11b only
//mode 802.11bg                           mode 802.11bgn
//network name <text>                     no network name
//scan dfs channels disable               scan dfs channels enable
//security                                show
//show history                            state disable
//state enable                            topology adhoc
//topology infrastructure                 write
//
//(config-profile-basic:default_infrastructure_profile)#show
//WLAN Profile Basic default_infrastructure_profile Configuration:
//   Network Name     : Lantronix Initial Infra Network
//   Topology         : Infrastructure
//   Mode             : 802.11abgn
//   Scan DFS Channels: Enabled
//   State            : Enabled
//   Channel          : 1
#define DEV_P_SHOW_NET_NAME_STR			   	"Network N"				// "Network Name     : Lantronix Initial Infra Network"
#define DEV_P_SHOW_NET_NAME_OFFSET		   	( 19 )					

//(config-profile-basic:default_infrastructure_profile)#security
//(config-profile-security:default_infrastructure_profile)#?
//advanced                                apply wlan
//basic                                   clrscrn
//default key type                        default suite
//exit                                    key type hex
//key type passphrase                     no passphrase
//passphrase <text>                       show
//show history                            suite none
//suite wep                               suite wpa
//suite wpa2                              wep
//wpax                                    write
//
//(config-profile-security:default_infrastructure_profile)#wpax
//(config-profile-security-wpax:default_infrastructure_profile)#?
//apply wlan                              authentication 802.1x
//authentication psk                      clrscrn
//credentials <text>                      default authentication
//default eap-ttls option                 default encryption
//default ieee 802.1x                     default peap option
//eap-ttls option chap                    eap-ttls option eap-md5
//eap-ttls option eap-mschapv2            eap-ttls option mschap
//eap-ttls option mschapv2                eap-ttls option pap
//encryption ccmp disable                 encryption ccmp enable
//encryption tkip disable                 encryption tkip enable
//encryption wep disable                  encryption wep enable
//exit                                    ieee 802.1x eap-tls
//ieee 802.1x eap-ttls                    ieee 802.1x leap
//ieee 802.1x peap                        key <hexadecimal>
//key text <text>                         no credentials
//no key                                  no password
//no username                             password <text>
//peap option eap-md5                     peap option eap-mschapv2
//show                                    show history
//username <text>                         validate certificate disable
//validate certificate enable             write
//
//(config-profile-security-wpax:default_infrastructure_profile)#
//(config-if:eth0)#exit
//(config)#wlan profiles
//(config-profiles)#show
//    1) default_infrastructure_profile (Enabled)
//    2) calsense_hq (Enabled)
//    3) default_adhoc_profile (Disabled)
//    4) Calsense (Enabled)
//(config-profiles)#edit default_infrastructure_profile
//(config-profile-basic:default_infrastructure_profile)#?
//advanced                                apply wlan
//channel <number>                        clrscrn
//default channel                         default mode
//default topology                        exit
//mode 802.11a only                       mode 802.11abgn
//mode 802.11an                           mode 802.11b only
//mode 802.11bg                           mode 802.11bgn
//network name <text>                     no network name
//scan dfs channels disable               scan dfs channels enable
//security                                show
//show history                            state disable
//state enable                            topology adhoc
//topology infrastructure                 write
//
//(config-profile-basic:default_infrastructure_profile)#show
//WLAN Profile Basic default_infrastructure_profile Configuration:
//   Network Name     : Lantronix Initial Infra Network
//   Topology         : Infrastructure
//   Mode             : 802.11abgn
//   Scan DFS Channels: Enabled
//   State            : Enabled
//   Channel          : 1
//(config-profile-basic:default_infrastructure_profile)#security
//(config-profile-security:default_infrastructure_profile)#?
//advanced                                apply wlan
//basic                                   clrscrn
//default key type                        default suite
//exit                                    key type hex
//key type passphrase                     no passphrase
//passphrase <text>                       show
//show history                            suite none
//suite wep                               suite wpa
//suite wpa2                              wep
//wpax                                    write
//
//(config-profile-security:default_infrastructure_profile)#wpax
//(config-profile-security-wpax:default_infrastructure_profile)#?
//apply wlan                              authentication 802.1x
//authentication psk                      clrscrn
//credentials <text>                      default authentication
//default eap-ttls option                 default encryption
//default ieee 802.1x                     default peap option
//eap-ttls option chap                    eap-ttls option eap-md5
//eap-ttls option eap-mschapv2            eap-ttls option mschap
//eap-ttls option mschapv2                eap-ttls option pap
//encryption ccmp disable                 encryption ccmp enable
//encryption tkip disable                 encryption tkip enable
//encryption wep disable                  encryption wep enable
//exit                                    ieee 802.1x eap-tls
//ieee 802.1x eap-ttls                    ieee 802.1x leap
//ieee 802.1x peap                        key <hexadecimal>
//key text <text>                         no credentials
//no key                                  no password
//no username                             password <text>
//peap option eap-md5                     peap option eap-mschapv2
//show                                    show history
//username <text>                         validate certificate disable
//validate certificate enable             write
//
//(config-profile-security-wpax:default_infrastructure_profile)#

// 5/5/2015 mpd : These are all the command strings needed to read and program the EN, GR, and WEN PremierWave devices. 
// CR characters have been included where needed to simplify output to the device. 
#define DEV_MENU_SERVER_STR		       	"0\r"
#define DEV_MENU_CHANNEL_STR		   	"1\r"
#define DEV_MENU_EXPERT_STR		        "5\r"
#define DEV_MENU_SECURITY_STR		    "6\r"
#define DEV_MENU_DEFAULTS_STR		    "7\r"
#define DEV_MENU_EXIT_WO_SAVE_STR	    "8\r"
#define DEV_MENU_SAVE_AND_EXIT_STR	    "9\r"
#define DEV_TEXT_CMD_60000			    "60000\r" 
#define DEV_TEXT_CMD_1200000		    "1200000\r" 
#define DEV_TEXT_CMD_A_MODE			    "accept mode disable\r"	 
#define DEV_TEXT_CMD_ACCEPT			    "accept\r" 
#define DEV_TEXT_CMD_ADDRESS		    "address 64.73.242.99\r" 
#define DEV_TEXT_CMD_ALIVE  		    "tcp keep alive " 
#define DEV_TEXT_CMD_ANTENNA		    "antenna diversity disable\r" 	// <- This command has a 30 second response
#define DEV_TEXT_CMD_APN			    "apn 10429.mcs\r"
#define DEV_TEXT_CMD_BAUD			    "baud rate 115200\r" 			// <- This command hangs the interface
#define DEV_TEXT_CMD_C_MODE			    "connect mode always\r" 
#define DEV_TEXT_CMD_CELL			    "cellular\r" 
// 6/10/2015 mpd : Apparently the "\r" is unnecessary.
#define DEV_TEXT_CMD_CLI			    "xyz" 
//#define DEV_TEXT_CMD_CLI			    "xyz\r" 
#define DEV_TEXT_CMD_CLOCK			    "clock\r" 
#define DEV_TEXT_CMD_CLOCK_TZ		    "clock timezone PST8PDT\r" 
#define DEV_TEXT_CMD_CONFIG			    "configure\r" 
#define DEV_TEXT_CMD_CONNECT		    "connect\r"  
#define DEV_TEXT_CMD_D_MODE			    "modem control enable\r" 
#define DEV_TEXT_CMD_DHCP			    "dhcp " 
#define DEV_TEXT_CMD_DIAG			    "diagnostics\r" 
#define DEV_TEXT_CMD_DISCONNECT		    "disconnect\r" 
#define DEV_TEXT_CMD_ECHO			    "echo commands disable\r" 
#define DEV_TEXT_CMD_EDIT_DIP		    "edit default_infrastructure_profile\r"
#define DEV_TEXT_CMD_ENABLE			    "enable\r"
#define DEV_TEXT_CMD_EXIT			    "exit\r" 
#define DEV_TEXT_CMD_FLOW			    "flow control hardware\r"		// <- This command hangs the interface
#define DEV_TEXT_CMD_GATEWAY		    "default gateway " 
#define DEV_TEXT_CMD_HOST			    "host 1\r" 
#define DEV_TEXT_CMD_HOSTNAME		    "hostname " 
#define DEV_TEXT_CMD_IF_ETH			    "if eth0\r" 
#define DEV_TEXT_CMD_IF_WLAN		    "if wlan0\r" 
#define DEV_TEXT_CMD_IF_WWAN		    "if wwan0\r" 
#define DEV_TEXT_CMD_IP_CIDR		    "ip address " 
#define DEV_TEXT_CMD_LENGTH			    "max length 1000\r" 
#define DEV_TEXT_CMD_LINE			    "line 1\r" 
#define DEV_TEXT_CMD_LINE_2			    "line 2\r" 
#define DEV_TEXT_CMD_LINK			    "link\r" 
#define DEV_TEXT_CMD_LOG			    "log\r" 
#define DEV_TEXT_CMD_LOGIN			    "login\r" 
#define DEV_TEXT_CMD_MODEM			    "modem\r" 
#define DEV_TEXT_CMD_MON_CONFIG		   	"GC\r"
#define DEV_TEXT_CMD_MON_EXIT		   	"QU\r"
#define DEV_TEXT_CMD_MON_MAC_ADD		"GM\r"
#define DEV_TEXT_CMD_MON_NETWORK		"NC\r"
#define DEV_TEXT_CMD_MON_VERSION		"VS\r"
//#define DEV_TEXT_CMD_MONITOR			"zzz"
#define DEV_TEXT_CMD_N				    "N" 
#define DEV_TEXT_CMD_NO				    "no\r" 
#define DEV_TEXT_CMD_OUTPUT			    "output filesystem\r"
#define DEV_TEXT_CMD_PORT			    "port 16001\r" 
#define DEV_TEXT_CMD_PRIORITY		    "priority 2\r" 
#define DEV_TEXT_CMD_RELOAD			    "reload\r"
#define DEV_TEXT_CMD_SECURITY 		    "security\r"
//#define DEV_TEXT_CMD_SETUP				"xxx"
#define DEV_TEXT_CMD_SHOW			    "show\r" 
#define DEV_TEXT_CMD_SHOW_S			    "show status\r" 
#define DEV_TEXT_CMD_START			    "!" 
#define DEV_TEXT_CMD_START_LANTRONIX_COBOX_SU	    "x" 
#define DEV_TEXT_CMD_START_EN_MON	    "z" 
#define DEV_TEXT_CMD_SU_BAUD_RATE		"115200\r"
#define DEV_TEXT_CMD_SU_CONN_MODE	   	"C5\r"
#define DEV_TEXT_CMD_SU_DISC_MODE	   	"80\r"
#define DEV_TEXT_CMD_SU_DISC_TIME	   	"00\r"
#define DEV_TEXT_CMD_SU_FLOW_02		   	"02\r"
#define DEV_TEXT_CMD_SU_FLUSH_MODE	   	"00\r"
#define DEV_TEXT_CMD_SU_IF_MODE_4C		"4C\r"
#define DEV_TEXT_CMD_SU_PORT			"10001\r"
#define DEV_TEXT_CMD_SU_REMOTE_IP_1	   	"64\r"
#define DEV_TEXT_CMD_SU_REMOTE_IP_2	   	"73\r"
#define DEV_TEXT_CMD_SU_REMOTE_IP_3	   	"242\r"
#define DEV_TEXT_CMD_SU_REMOTE_IP_4	   	"99\r" 

// ----------

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// 4/25/2017 ajv : Temporarily changed the default TCP port used by the CS3-EN option to
// 16003 to support Scott's efforts in the City of Roseville. This allows him to use the
// controller to reprogram the customer's UDS1100 devices without needing his laptop to
// change the TCP port. Must be changed back to 16001 before official release.
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
// 11/21/2017 rmd : I added the compile by defines. I think this define is used to program
// BOTH the UDS1100 and the WEN device, but I'm not sure, it may only be used for the
// UDS1100. Another thing, there are other 16001 defines in this file, and I don't know what
// they are used for. I am only following AJ's lead, modifying this define only.

#if PROGRAM_CENTRAL_DEVICE_TO_PORT_16001

	#define DEV_TEXT_CMD_SU_REMOTE_PORT	   	"16001\r"

#elif PROGRAM_CENTRAL_DEVICE_TO_PORT_16003

	#define DEV_TEXT_CMD_SU_REMOTE_PORT	   	"16003\r"

#elif PROGRAM_CENTRAL_DEVICE_TO_PORT_16004

	#define DEV_TEXT_CMD_SU_REMOTE_PORT	   	"16004\r"

#else

	#error COMPILING CODE WITHOUT DEVICE PORT SELECTION!
	
#endif

// ----------

#define DEV_TEXT_CMD_SU_SEND_CHAR	   	"00\r"
#define DEV_TEXT_CMD_SYNC_N			    "synchronization method network\r" 
#define DEV_TEXT_CMD_SYNC_S			    "synchronization method sntp\r" 
#define DEV_TEXT_CMD_TUNNEL			    "tunnel 1\r"
#define DEV_TEXT_CMD_VERBOSE		    "verbose response disable\r" 
#define DEV_TEXT_CMD_WLAN_PRO		    "wlan profiles\r"
#define DEV_TEXT_CMD_WEP			    "wep\r"
#define DEV_TEXT_CMD_WPAX			    "wpax\r"
#define DEV_TEXT_CMD_WRITE			    "write\r" 
#define DEV_TEXT_CMD_X_CD_DUMP		    "xcr dump device\r" 
#define DEV_TEXT_CMD_X_CI_DUMP		    "xcr dump interface:wlan0\r"
#define DEV_TEXT_CMD_X_CP_DUMP		    "xcr dump \"wlan profile:default_infrastructure_profile\"\r"
#define DEV_TEXT_CMD_X_CQ_DUMP		    "xcr dump \"wlan profile:calsense_hq\"\r"
#define DEV_TEXT_CMD_X_SI_DUMP		    "xsr dump interface:wlan0\r"
#define DEV_TEXT_CMD_XML			    "xml\r" 
#define DEV_TEXT_CMD_Y				    "Y" 
#define DEV_TEXT_CMD_YES			    "yes\r" 
#define DEV_TEXT_CMD_X_ETH0_DUMP	    "xcr dump interface:eth0\r"

// 4/21/2015 mpd : These keys entered after a power cycle put the Lantronix device into specific modes.
//#define DEV_ENTER_MODE_SETUP_STR		( "xxx" )			// Key to enter SETUP MODE after a power cycle. Note: No CR or LF.
//#define DEV_ENTER_MODE_MONITOR_STR		( "zzz" )			// Key to enter MONITOR MODE after a power cycle. Note: No CR or LF.

// 6/3/2015 mpd : Strings needed for WIFI Settings. TBD: Use defines or raw text?
/* 
"no " 
"network name " 
"suite " 
"none"
"wep" 
"wpa"
"wpa2"
"key type " 
"hex"
"passphrase "
"authentication " 
"open"
"shared"  
"key size " 
"104"
"40" 
"tx key index "
"key "
"802.1x"
"psk" 
"eap-ttls option " 
"chap" 
"eap-md5"
"eap-mschapv2" 
"mschap"
"mschapv2 
"pap"
"ieee 802.1x " 
"eap-tls"
"eap-ttls" 
"leap"
"peap" 
"peap option " 
"validate certificate "
"encryption ccmp "
"encryption tkip " 
"encryption wep "
"credentials " 
"password " 
"username " 
 
*/ 
// 6/16/2015 mpd : The longest WEN/GR/EN command is "credentials <63 hex chars>\r\0". That's a total of 77 bytes 
// ( 12 + 63 + 1 + 1 = 77 ). That needs to be extended to support white space and control characters. Every character
// in the string could be a control character needing an escape character. Add 63. The entire string needs quites. 
// Add 2 more. The minimum is now 12 + 63 + 63 + 2 + 1 + 1 = 142. Let's round up to 148 for wiggle room and 32 bit 
// boundaries.
#define DEV_LONGEST_COMMAND_LEN	( 148 ) // This is the longest known GR/WEN command. Update it if a longer one is found.

// 5/5/2015 mpd : These commands apply to GR and WEN devices. Use enumerations beginning at "0" with command names in 
// alphabetic order. 
#define DEV_COMMAND_802_MODE            ( 0 )           
#define DEV_COMMAND_A_MODE	            ( 1 ) 
#define DEV_COMMAND_ACCEPT	            ( 2 ) 
#define DEV_COMMAND_ADDRESS	            ( 3 ) 
#define DEV_COMMAND_ALIVE               ( 4 ) 
#define DEV_COMMAND_ANTENNA	            ( 5 ) 
#define DEV_COMMAND_APN		            ( 6 ) 
#define DEV_COMMAND_BAUD		        ( 7 ) 
#define DEV_COMMAND_C_MODE	            ( 8 ) 
#define DEV_COMMAND_CELL		        ( 9 ) 
#define DEV_COMMAND_CLI			        ( 10 )
#define DEV_COMMAND_CLOCK	            ( 11 )
#define DEV_COMMAND_CLOCK_TZ            ( 12 )
#define DEV_COMMAND_CONFIG	            ( 13 )
#define DEV_COMMAND_CONNECT	            ( 14 )
#define DEV_COMMAND_CR_NR	            ( 15 )
#define DEV_COMMAND_CR		            ( 16 )
#define DEV_COMMAND_CR_DELAY 	        ( 17 )
#define DEV_COMMAND_D_MODE	            ( 18 )
#define DEV_COMMAND_DHCP		        ( 19 )
#define DEV_COMMAND_DIAG		        ( 20 )
#define DEV_COMMAND_DISCONNECT          ( 21 )
#define DEV_COMMAND_E_CCMP              ( 22 )
#define DEV_COMMAND_E_TKIP              ( 23 )
#define DEV_COMMAND_E_WEP               ( 24 )
#define DEV_COMMAND_EAP_TTLS            ( 25 )
#define DEV_COMMAND_ECHO		        ( 26 )
#define DEV_COMMAND_EDIT_DIP		    ( 27 )
#define DEV_COMMAND_ENABLE	            ( 28 )
#define DEV_COMMAND_END			        ( 29 )	// Special case. There is no DEV_TEXT_CMD_END.
//#define DEV_COMMAND_ENTER_MONITOR		( 30 )
//#define DEV_COMMAND_ENTER_SETUP		( 31 )
#define DEV_COMMAND_ETLS_C              ( 32 ) 
#define DEV_COMMAND_ETLS_V              ( 33 )       
#define DEV_COMMAND_EXIT		        ( 34 )
#define DEV_COMMAND_EXIT_FINAL          ( 35 )
#define DEV_COMMAND_EXIT_WO_SAVE		( 36 )
#define DEV_COMMAND_FLOW		        ( 37 )
#define DEV_COMMAND_PW_XN_GATEWAY		( 38 )
#define DEV_COMMAND_GET_AUTO_BOOL		( 39 )
#define DEV_COMMAND_GET_BAUD_RATE		( 40 )
#define DEV_COMMAND_GET_CONN_MODE		( 41 )
#define DEV_COMMAND_GET_DHCP_NAME		( 42 )
#define DEV_COMMAND_GET_DHCP_NAME_BOOL	( 43 )
#define DEV_COMMAND_GET_DISCONN_MODE	( 44 )
#define DEV_COMMAND_GET_DISCONN_TIME	( 45 )
#define DEV_COMMAND_GET_FLOW			( 46 )
#define DEV_COMMAND_GET_FLUSH_MODE		( 47 )
#define DEV_COMMAND_GET_IF_MODE			( 48 )
#define DEV_COMMAND_GET_IP_1			( 49 )
#define DEV_COMMAND_GET_IP_2			( 50 )
#define DEV_COMMAND_GET_IP_3			( 51 )
#define DEV_COMMAND_GET_IP_4			( 52 )
#define DEV_COMMAND_GET_GW_1			( 53 )
#define DEV_COMMAND_GET_GW_2			( 54 )
#define DEV_COMMAND_GET_GW_3			( 55 )
#define DEV_COMMAND_GET_GW_4			( 56 )
#define DEV_COMMAND_GET_GW_BOOL			( 57 )
#define DEV_COMMAND_GET_MASK_BITS		( 58 )
#define DEV_COMMAND_GET_PORT			( 59 )
#define DEV_COMMAND_GET_REMOTE_IP_1		( 60 )
#define DEV_COMMAND_GET_REMOTE_IP_2		( 61 )
#define DEV_COMMAND_GET_REMOTE_IP_3		( 62 )
#define DEV_COMMAND_GET_REMOTE_IP_4		( 63 )
#define DEV_COMMAND_GET_REMOTE_PORT		( 64 )
#define DEV_COMMAND_GET_SEND_BOOL		( 65 )
#define DEV_COMMAND_GET_SEND_CHAR_1		( 66 )
#define DEV_COMMAND_GET_SEND_CHAR_2		( 67 )
#define DEV_COMMAND_GET_TELNET_BOOL		( 68 )
#define DEV_COMMAND_GOTO_CHANNEL		( 69 )
#define DEV_COMMAND_GOTO_DEFAULTS		( 70 )
#define DEV_COMMAND_GOTO_EXPERT			( 71 )
#define DEV_COMMAND_GOTO_SECURITY		( 72 )
#define DEV_COMMAND_GOTO_SERVER			( 73 )
#define DEV_COMMAND_HOST		        ( 74 )
#define DEV_COMMAND_PW_XN_HOSTNAME	    ( 75 )
#define DEV_COMMAND_IEEE                ( 76 )    
#define DEV_COMMAND_IF_ETH		        ( 77 )
#define DEV_COMMAND_IF_WLAN		        ( 78 )
#define DEV_COMMAND_IF_WWAN		        ( 79 )
#define DEV_COMMAND_PW_XN_IP_CIDR		( 80 )
#define DEV_COMMAND_KEY_CLEAR_WEP       ( 81 )
#define DEV_COMMAND_KEY_CLEAR_WPA       ( 82 )
#define DEV_COMMAND_KEY_IDX             ( 83 )    
#define DEV_COMMAND_KEY_SIZE            ( 84 )    
#define DEV_COMMAND_KEY_SET_WEP         ( 85 )
#define DEV_COMMAND_KEY_SET_WPA         ( 86 )
#define DEV_COMMAND_KEY_TYPE            ( 87 )    
#define DEV_COMMAND_LENGTH	            ( 88 )
#define DEV_COMMAND_LINE		        ( 89 )
#define DEV_COMMAND_LINE_2	            ( 90 )
#define DEV_COMMAND_LINK		        ( 91 )
#define DEV_COMMAND_LOG		            ( 92 )
#define DEV_COMMAND_LOGIN	            ( 93 )
#define DEV_COMMAND_MODEM	            ( 94 )
#define DEV_COMMAND_MON_EXIT			( 95 )
#define DEV_COMMAND_MON_GET_MAC			( 96 )
#define DEV_COMMAND_MON_GET_NETWORK		( 97 )
#define DEV_COMMAND_N		            ( 98 )
#define DEV_COMMAND_NO		            ( 99 )
#define DEV_COMMAND_NONE			    ( 100 )	// Special case. There is no DEV_TEXT_CMD_NONE.
#define DEV_COMMAND_OUTPUT	            ( 101 )
#define DEV_COMMAND_PASSPHRASE          ( 102 )
#define DEV_COMMAND_PEAP                ( 103 )
#define DEV_COMMAND_PORT		        ( 104 )
#define DEV_COMMAND_PRIORITY	        ( 105 )
#define DEV_COMMAND_PWORD               ( 106 )
#define DEV_COMMAND_RELOAD	            ( 107 )
#define DEV_COMMAND_SAVE_AND_EXIT		( 108 )
#define DEV_COMMAND_SECURITY 		    ( 109 )
#define DEV_COMMAND_SHOW		        ( 110 )
#define DEV_COMMAND_SHOW_S	            ( 111 )
#define DEV_COMMAND_SSID                ( 112 )
#define DEV_COMMAND_START		        ( 113 )
#define DEV_COMMAND_SUITE               ( 114 )
#define DEV_COMMAND_SYNC		        ( 115 )
#define DEV_COMMAND_TUNNEL	            ( 116 )
#define DEV_COMMAND_UNAME               ( 117 )
#define DEV_COMMAND_VERBOSE	            ( 118 )
#define DEV_COMMAND_WEP				    ( 119 )
#define DEV_COMMAND_WEP_AUTH            ( 120 )
#define DEV_COMMAND_WEP_K1			    ( 121 )
#define DEV_COMMAND_WEP_K2			    ( 122 )
#define DEV_COMMAND_WEP_K3			    ( 123 )
#define DEV_COMMAND_WEP_K4			    ( 124 )
#define DEV_COMMAND_WEP_KX			    ( 125 )
#define DEV_COMMAND_WLAN_PRO            ( 126 )
#define DEV_COMMAND_WPA_AUTH            ( 127 )
#define DEV_COMMAND_WPAX			    ( 128 )
#define DEV_COMMAND_WRITE	            ( 129 )
#define DEV_COMMAND_WRITE_FINAL         ( 130 )
#define DEV_COMMAND_X_CD_DUMP   	    ( 131 ) 
#define DEV_COMMAND_X_CI_DUMP   	    ( 132 ) 
#define DEV_COMMAND_X_CP_DUMP   	    ( 133 ) 
#define DEV_COMMAND_X_CQ_DUMP   	    ( 134 ) 
#define DEV_COMMAND_X_SI_DUMP   	    ( 135 ) 
#define DEV_COMMAND_XML				    ( 136 ) 
#define DEV_COMMAND_Y		            ( 137 )
#define DEV_COMMAND_YES		            ( 138 )
#define	DEV_COMMAND_X_ETH0_DUMP			( 139 )
#define DEV_COMMAND_PW_XE_IP_CIDR		( 140 )
#define DEV_COMMAND_PW_XE_GATEWAY		( 141 )
#define DEV_COMMAND_PW_XE_HOSTNAME	    ( 142 )
#define DEV_COMMAND_GOTO_WLAN						( 143 )
#define DEV_COMMAND_GET_WIBOX_TOPOLOGY				( 144 )
#define DEV_COMMAND_GET_WIBOX_SSID					( 145 )
#define DEV_COMMAND_GET_WIBOX_SECURITY				( 146 )
#define DEV_COMMAND_GET_WEP_AUTH					( 147 )
#define DEV_COMMAND_GET_WEP_ENCRYPTION				( 148 )
#define DEV_COMMAND_GET_WEP_CHANGE_KEY				( 149 )
#define DEV_COMMAND_GET_WEP_DISPLAY_KEY				( 150 )
#define DEV_COMMAND_GET_WEP_KEY_TYPE				( 151 )
#define DEV_COMMAND_GET_WEP_KEY						( 152 )
#define DEV_COMMAND_GET_WEP_TX_KEY_INDEX			( 153 )
#define DEV_COMMAND_GET_WPA_CHANGE_KEY				( 154 )
#define DEV_COMMAND_GET_WPA_DISPLAY_KEY				( 155 )
#define DEV_COMMAND_GET_WPA_KEY_TYPE				( 156 )
#define DEV_COMMAND_GET_WPA_KEY						( 157 )
#define DEV_COMMAND_GET_WPA_ENCRYPTION				( 158 )
#define DEV_COMMAND_GET_WPA2_CHANGE_KEY				( 159 )
#define DEV_COMMAND_GET_WPA2_DISPLAY_KEY			( 160 )
#define DEV_COMMAND_GET_WPA2_KEY_TYPE				( 161 )
#define DEV_COMMAND_GET_WPA2_KEY					( 162 )
#define DEV_COMMAND_GET_WPA2_ENCRYPTION				( 163 )
#define DEV_COMMAND_GET_WIBOX_TX_DATA_RATE_1		( 164 )
#define DEV_COMMAND_GET_WIBOX_TX_DATA_RATE_2		( 165 )
#define DEV_COMMAND_GET_WIBOX_MIN_TX_DATA_RATE		( 166 )
#define DEV_COMMAND_GET_WIBOX_POWER_MGMT			( 167 )
#define DEV_COMMAND_GET_WIBOX_SOFT_AP_ROAMING		( 168 )
#define DEV_COMMAND_GET_WIBOX_MAX_FAILED_PACKETS	( 169 )
#define DEV_COMMAND_GET_WIBOX_NETWORK_MODE			( 170 )

									  
									  
#define DEV_DHCP_NAME_LEN  			( 17 + DEV_NULL_LEN )	// Length matches GUIVar length. 
#define DEV_LONGEST_FIELD_LEN  		( WEN_KEY_LEN )			// Extraction needs to handle the longest string. Change as needed.
									
// 5/20/2015 mpd : Timeouts on the first command to the device have been reported in the field. Add some alert messages 
// to see if field times differ from times seen during product development.
#define DEV_FIELD_DEBUGGING_ALERTS

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define		EN_PROGRAMMING_IP_ADDRESS_OCTET_MIN		(0)
#define		EN_PROGRAMMING_IP_ADDRESS_OCTET_MAX		(255)

#define		EN_PROGRAMMING_NETMASK_MIN				(0)
#define		EN_PROGRAMMING_NETMASK_MAX				(24)
// 9/23/2015 mpd : FogBugz #3164: Added default define to replace the magic number in code.
#define		EN_PROGRAMMING_NETMASK_DEFAULT			(8)

#define		EN_PROGRAMMING_GATEWAY_OCTET_MIN		(0)
#define		EN_PROGRAMMING_GATEWAY_OCTET_MAX		(255)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/23/2015 mpd : FogBugz #3164: The Lantronix EN UDS1100 does not use CIDR (Classless Inter Domain Routing) format
// to define the network mask. The number of bits indicates the available bits for host addressing (e.g. A mask of 
// "8" produces a netmask of "255.255.255.0" which has 8 bits available for hosts on the subnet. Other Lantronix
// devices use CIDR format for programming (e.g. "192.168.254.150/24" specifies the IP address as well as a network 
// mask of "255.255.255.0"). The CIDR mask bit value indicates the leading ones; not the trailing zeros.
// The CS3000 GUI code was written for UDS1100. It's backwards for all newer Lantronix devices. The mask numbers are 
// internal to CS3000 code. They become external at the GUI and in device programming. To eliminate extensive 
// rework, the GUI side for all device types will retain pre-CIDR mask values. As mask values pass to and from the 
// GUI code to the device code (e.g. "e_wen_programming.c" to "device_WEN_PremierWaveXN.c"), the value will be 
// transformed to match the receiving format.  
// 9/24/2015 mpd : FogBugz #3164: Added defines for the device side.
#define		XE_XN_DEVICE_NETMASK_MIN				(8)
#define		XE_XN_DEVICE_NETMASK_MAX				(32)
#define		XE_XN_DEVICE_NETMASK_DEFAULT			(24)

// 6/9/2015 mpd : These forward declarations are necessary due to looping struct declarations.
typedef struct DEV_MENU_ITEM				 	DEV_MENU_ITEM;
typedef struct DEV_DETAILS_STRUCT 			 	DEV_DETAILS_STRUCT;
typedef struct WEN_DETAILS_STRUCT 			 	WEN_DETAILS_STRUCT;
typedef struct EN_PROGRAMMABLE_VALUES_STRUCT 	EN_PROGRAMMABLE_VALUES_STRUCT;
typedef struct EN_DETAILS_STRUCT 			 	EN_DETAILS_STRUCT;
typedef struct PW_XE_DETAILS_STRUCT 		 	PW_XE_DETAILS_STRUCT;
typedef struct WIBOX_DETAILS_STRUCT 		 	WIBOX_DETAILS_STRUCT;
typedef struct WIBOX_PROGRAMMABLE_VALUES_STRUCT WIBOX_PROGRAMMABLE_VALUES_STRUCT;

typedef struct {

	UNS_32 error_code;
	UNS_32 error_severity;
	UNS_32 device_type;
	UNS_32 operation;
	char operation_text[ DEV_OPERATION_STR_LEN ];                
	char info_text[ DEV_INFO_TEXT_LEN ];                
	char error_text[ DEV_INFO_TEXT_LEN ];                
	UNS_32 read_list_index;
	UNS_32 write_list_index;
	BOOL_32 acceptable_version; 
	UNS_32 startup_loop_count;
	UNS_32 network_loop_count;
	BOOL_32 device_settings_are_valid;
	BOOL_32 programming_successful;
	BOOL_32 command_will_respond;

	// 6/11/2015 mpd : Lantronix support has confirmed the need for delays between commands (this confirmation applied 
	// to WEN devices). They also indicated some commands require longer times to complete than others. Apparently they 
	// return a prompt when processing is not complete on their end. Sending commands with no delay can even overflow 
	// their input buffer. This field indicates how much command separation delay (in milliseconds) is required in
	// addition to the base value applied to every command. 
	// This only applies to WEN at the moment. It may also be necessary for future devices. 
	UNS_32 command_separation;

	// 5/6/2015 mpd : WRITE operations are followed by a READ to verify the values were successfully set. The final info 
	// message after a READ or WRITE command needs to reflect what was requested, not the status of the final READ 
	// operation. Use this flag to get it right.
	BOOL_32 read_after_a_write;

	// 6/2/2015 mpd : TBD: While the WIFI Settings screen is under construction, there is excessive exiting back to the
	// WEN Programming screen. Since there is no save on the WIFI screen, any changes made to those settings would be 
	// lost if we kept refreshing values from the programming structure every time we went back in. Use this flag to 
	// only update the GuiVars if there is new data from the device.
	BOOL_32 gui_has_latest_data;

	// 3/23/2015 mpd : Centralize the response buffer pointer to make "mem_free()" easier.
	char  *resp_ptr;
	UNS_32 resp_len;

	// 5/27/2015 mpd : Use these variables to make list handling generic.
	const DEV_MENU_ITEM *list_ptr;
	UNS_32 list_len;

	// 6/9/2015 mpd : A few extra structure pointers have been added to eliminate the (void*) type. Code is much cleaner
	// if the compiler knows what fields are inside the structures.
	 
	// 5/18/2015 mpd : Pointer to the details structure for the specific device type.
	DEV_DETAILS_STRUCT *dev_details;

	// 5/27/2015 mpd : Pointer to additional details for the specific device type. WEN has additional fields beyond 
	// GR requirements.
	WEN_DETAILS_STRUCT *wen_details;

	// 6/9/2015 mpd : Pointer to EN specific details.
	EN_DETAILS_STRUCT *en_details;

	// 8/4/2015 ajv : Pointer to PremierWave XE-specific details.
	PW_XE_DETAILS_STRUCT *PW_XE_details;

	// 3/23/2016 ajv : Pointer to WiBox-specific details.
	WIBOX_DETAILS_STRUCT *WIBOX_details;

	// 6/9/2015 mpd : The "dynamic", "active", and "write" programmable values structures created for SR have been 
	// simplified within this common code module. We now have just "active" and "static" structures.

	// 6/9/2015 mpd : These are pointers to the programmable variables structures used by EN devices. READ operations 
	// load the "active_pvs" with values obtained from the device. These values are copied to the GUI for users to see. 
	// All the user variables are copied back to the "active_pvs" values when the "Save Changes" button is selected. 
	// Just before programming the device, the "active_pvs" is copied to the "static_pvs" which will be used to program
	// the device. After the WRITE, a READ is performed to verify the results (using the "active_pvs" which again gets
	// copied to the user screen).
	EN_PROGRAMMABLE_VALUES_STRUCT *en_active_pvs;
	EN_PROGRAMMABLE_VALUES_STRUCT *en_static_pvs;

	// 6/9/2015 mpd : These are pointers to the programmable variables structures used by EN devices. READ operations 
	// load the "active_pvs" with values obtained from the device. These values are copied to the GUI for users to see. 
	// All the user variables are copied back to the "active_pvs" values when the "Save Changes" button is selected. 
	// Just before programming the device, the "active_pvs" is copied to the "static_pvs" which will be used to program
	// the device. After the WRITE, a READ is performed to verify the results (using the "active_pvs" which again gets
	// copied to the user screen).
	WIBOX_PROGRAMMABLE_VALUES_STRUCT *wibox_active_pvs;
	WIBOX_PROGRAMMABLE_VALUES_STRUCT *wibox_static_pvs;

	// 5/18/2015 mpd : Every device has a Model, FW version, and Serial Number. They belongs here in device common code. 
	// Formats may vary across different devices. Use the longest length.
	char model[ DEV_X_DUMP_LNAME_STR_LEN ];  
	char firmware_version[ DEV_X_DUMP_FW_VER_STR_LEN ];  
	char serial_number[ DEV_E_SHOW_MAC_ADD_LEN ];  				// Use the ENABLE SHOW length. It's longer than XCR DUMP.

	// 6/9/2015 mpd : DHCP affects a number of values and responses. Use this flag as the ultimate decision maker. This
	// has been relocated here from WEN and EN detail structs.
	BOOL_32 dhcp_enabled;

	#ifdef DEV_FIELD_DEBUGGING_ALERTS
	//static DATE_TIME dev_first_command_time;
	UNS_32 first_command_ticks;
	#endif

	// 5/1/2015 mpd : This structure is currently ??? bytes.

} DEV_STATE_STRUCT;

// 3/17/2015 mpd : This struct contains the variables and the function pointer necessary to process each individual
// response from the device. The first 2 fields deal with the current response. The last 2 fields drive the subsequent
// device response. This struct needs to be named for the forward declaration (needed by DEV_STATE_STRUCT) to work.
typedef struct DEV_MENU_ITEM {
	// 4/8/2015 mpd : This is the text string we expect to see in the response buffer. It is a sanity check performed 
	// before attempting to parse the rest of the buffer.
	char *title_str;

	// 4/8/2015 mpd : This function pointer identifies the response handler for this specific response buffer.
	void (*dev_response_handler)( DEV_STATE_STRUCT *dev_state ); 

	// 4/8/2015 mpd : The next command is what will be sent to the device. It can be a simple ASCII character without CR
	// and LF, or a string.
	UNS_32 next_command;

	// 4/8/2015 mpd : This string tells rcvd_data what constitutes an end to the subsequent device response. The comm_mngr
	// timer will expire if this string is not received within the expected time.
	char  *next_termination_str;

} DEV_MENU_ITEM;

#define DEV_MENU_ITEM_SIZE		 sizeof( DEV_MENU_ITEM )

typedef struct
{
	// 6/10/2015 mpd : So far, only the 4th firmware version "octet" is a string. Take advantage.
	UNS_32 fw_ver_1;
	UNS_32 fw_ver_2;
	UNS_32 fw_ver_3;
	char   fw_ver_4[DEV_FW_VER_4_LEN];

    char *(*get_command_text)( DEV_STATE_STRUCT *dev_state, const UNS_32 next_command );
    void (*enter_device_mode)( DEV_STATE_STRUCT *dev_state );
    void (*exit_device_mode)();
    void (*set_read_operation)( DEV_STATE_STRUCT *dev_state, UNS_32 operation );
    void (*set_write_operation)( DEV_STATE_STRUCT *dev_state );
    void (*initialize_detail_struct)( DEV_STATE_STRUCT *dev_state );
    void (*state_machine)( DEV_STATE_STRUCT *dev_state, COMM_MNGR_TASK_QUEUE_STRUCT *pq_msg );

} DEVICE_HANDLER_STRUCT;

// 6/9/2015 mpd : This struct needs to be named for the forward declaration (needed by DEV_STATE_STRUCT) to work.
typedef struct DEV_DETAILS_STRUCT {

	// 6/11/2015 mpd : TODO: This structure has evolved into 90% GR content. Move the GR specific fields to a new GR
	// specific structure when the diagnostic screen is created.  

	// 5/15/2015 mpd : TODO: There must be a better place to get this than the large ENABLE SHOW response. 
	char ip_address[ DEV_E_SHOW_IP_ADD_LEN ];  

	// 4/20/2015 mpd : These values are pulled from the CELLULAR SHOW STATUS response. 
	char sim_status[ DEV_C_SHOW_SIM_STATUS_LEN ];  
	char network_status[ DEV_C_SHOW_NET_STATUS_LEN ];  
	char packet_domain_status[ DEV_C_SHOW_GPRS_LEN ];  
	char signal_strength[ DEV_C_SHOW_RSSI_LEN ];  
	
	// 5/7/2015 mpd : This value is pulled from the LINK SHOW response.
	char apn[ DEV_L_SHOW_APN_LEN ];  

	// 5/1/2015 mpd : This structure is currently ??? bytes.

} DEV_DETAILS_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern DEV_STATE_STRUCT *dev_state;

extern DEV_DETAILS_STRUCT dev_details;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FDTO_e_SHARED_show_obtain_ip_automatically_dropdown( void );

extern void FDTO_e_SHARED_show_subnet_dropdown( void );

extern void e_SHARED_show_keyboard( const UNS_32 px_coordinate, const UNS_32 py_coordinate, GuiConst_TEXT *guivar_ptr, const UNS_32 pmax_string_len, UNS_32 *current_cp, const UNS_32 pkeyboard_type, BOOL_32 show_current_value );

extern const char *e_SHARED_get_easyGUI_string_at_index( const UNS_32 pstructure_index_0, const UNS_32 pindex );

extern INT_32 e_SHARED_get_index_of_easyGUI_string( const char *pstr_to_find, const UNS_32 pstr_len, const UNS_32 pstructure_index_0, const UNS_32 pmin_index, const UNS_32 pmax_index );

extern void e_SHARED_start_device_communication( const UNS_32 port, UNS_32 operation, BOOL_32 *querying_device );

extern BOOL_32 e_SHARED_network_connect_delay_is_over( const UNS_32 seconds_to_wait );

extern void e_SHARED_get_info_text_from_programming_struct();

extern void e_SHARED_string_validation( char *target_string, UNS_32 string_len );

extern UNS_32 e_SHARED_index_keycode_for_gui( const UNS_32 pkeycode );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void dev_analyze_xcr_dump_device( DEV_STATE_STRUCT *dev_state );

extern void dev_analyze_enable_show_ip( DEV_STATE_STRUCT *dev_state );

extern void dev_analyze_firmware_version( DEV_STATE_STRUCT *dev_state );

extern void dev_strip_crlf_characters( char *dest_ptr, char *description );

extern void dev_extract_text_from_buffer( const char *response_buffer, const char *anchor_text, UNS_32 text_offset, size_t text_len, char *dest_ptr, char *description );

extern void dev_extract_delimited_text_from_buffer( const char *response_buffer, const char *delimiter, const char *anchor_text, UNS_32 text_offset, size_t max_text_len, char *dest_ptr, char *description );

extern void dev_extract_ip_octets( const char *ip_address, char *ip_address_1, char *ip_address_2, char *ip_address_3, char *ip_address_4 );

extern UNS_32 dev_count_true_mask_bits( const char *mask_string );

extern void dev_final_device_analysis( DEV_STATE_STRUCT *dev_state );

extern void dev_wait_for_network( DEV_STATE_STRUCT *dev_state );

extern void dev_wait_for_ready( DEV_STATE_STRUCT *dev_state );

extern void dev_device_read_progress( DEV_STATE_STRUCT *dev_state );

extern void dev_device_write_progress( DEV_STATE_STRUCT *dev_state );

extern char *dev_get_command_text( DEV_STATE_STRUCT *dev_state, const UNS_32 command );

extern void dev_cli_disconnect();

extern void dev_update_info( DEV_STATE_STRUCT *dev_state, const char *info_str, UNS_32 devex_key );

extern void DEVICE_initialize_device_exchange( void );

extern void DEVICE_exchange_processing( void *pq_msg );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

