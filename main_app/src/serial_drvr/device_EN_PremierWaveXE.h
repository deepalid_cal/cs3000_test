/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_DEVICE_EN_PWXE_H
#define INC_DEVICE_EN_PWXE_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"device_common.h"

#include	"device_WEN_PremierWaveXN.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/5/2015 ajv : I'm not thrilled with the fact that this is externally available, but
// encapsulating it would result in sigificant code space because we'd have to create GET
// and SET routines to externally access the members from device_common.c.
struct PW_XE_DETAILS_STRUCT
{
	char mac_address[ WEN_MAC_ADDRESS_LEN ];

	char status[ WEN_STATUS_LEN ];

	char dhcp_name[ DEV_DHCP_NAME_LEN ];                    

	// 6/11/2015 mpd : Scenario: DHCP was enabled (a DHCP name existed), it was disabled, and
	// it's about to be reenabled. We don't want to stomp on a long custom name if we ever knew
	// it. This flag tells us it's safe to stomp on any DHCP name that existed in the device the
	// last time DHCP was enabled. Or conversely, it prevents us from stomping on a name that
	// was temporarily hidden from view during the current session.
	BOOL_32 dhcp_name_not_set;

	// 9/23/2015 mpd : FogBugz #3164: The Lantronix EN UDS1100 does not use CIDR (Classless Inter Domain Routing) format
	// to define the network mask. The number of bits indicates the available bits for host addressing (e.g. A mask of 
	// "8" produces a netmask of "255.255.255.0" which has 8 bits available for hosts on the subnet. Other Lantronix
	// devices use CIDR format for programming (e.g. "192.168.254.150/24" specifies the IP address as well as a network 
	// mask of "255.255.255.0"). The CIDR mask bit value indicates the leading ones; not the trailing zeros.
	// The CS3000 GUI code was written for UDS1100. It's backwards for all newer Lantronix devices. The mask numbers are 
	// internal to CS3000 code. They become external at the GUI and in device programming. To eliminate extensive 
	// rework, the GUI side for all device types will retain pre-CIDR mask values. As mask values pass to and from the 
	// GUI code to the device code (e.g. "e_wen_programming.c" to "device_WEN_PremierWaveXN.c"), the value will be 
	// transformed to match the receiving format.  
	UNS_32 mask_bits;

	// 5/28/2015 mpd : Whole IP addresses are read from the device and used in decision making.
	// Octets are set by individual GUI fields and are used primarily to program the device.
	char ip_address[ DEV_IP_ADD_LEN ];  
	char ip_address_1[ DEV_IP_ADDRESS_OCTET_LEN ];            
	char ip_address_2[ DEV_IP_ADDRESS_OCTET_LEN ];            
	char ip_address_3[ DEV_IP_ADDRESS_OCTET_LEN ];            
	char ip_address_4[ DEV_IP_ADDRESS_OCTET_LEN ];            

	// 9/24/2015 mpd : FogBugz #3178: Neither the Gui side or the device side code cares about the mask string.
	// It's the mask bits that count. Remove this field. 
	//char mask[ DEV_IP_ADD_LEN ]; 
	//char mask_1[ DEV_IP_ADDRESS_OCTET_LEN ]; 
	//char mask_2[ DEV_IP_ADDRESS_OCTET_LEN ]; 
	//char mask_3[ DEV_IP_ADDRESS_OCTET_LEN ]; 
	//char mask_4[ DEV_IP_ADDRESS_OCTET_LEN ];

	char gw_address[ DEV_IP_ADD_LEN ];              
	char gw_address_1[ DEV_IP_ADDRESS_OCTET_LEN ];            
	char gw_address_2[ DEV_IP_ADDRESS_OCTET_LEN ];            
	char gw_address_3[ DEV_IP_ADDRESS_OCTET_LEN ];            
	char gw_address_4[ DEV_IP_ADDRESS_OCTET_LEN ]; 
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern const DEV_MENU_ITEM PW_XE_read_list[]; 

extern const DEV_MENU_ITEM PW_XE_write_list[]; 

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern UNS_32 PW_XE_sizeof_read_list( void );

extern UNS_32 PW_XE_sizeof_write_list( void );

// ----------

extern void PW_XE_PROGRAMMING_copy_programming_into_GuiVars( void );

extern void PW_XE_PROGRAMMING_extract_and_store_GuiVars( void );

// ----------

extern void PW_XE_initialize_detail_struct( DEV_STATE_STRUCT *dev_state );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

