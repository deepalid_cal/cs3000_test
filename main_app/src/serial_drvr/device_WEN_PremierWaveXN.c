/*  file = device_WEN_WBX2100E.c              01.22.2014  ajv */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/5/2015 mpd : Required for atoi
#include	<stdlib.h>

#include	<string.h>

#include	"device_common.h"	// Needed for extraction functions.

#include	"device_WEN_PremierWaveXN.h"

#include	"comm_mngr.h"

#include	"gpio_setup.h"

#include	"alerts.h"

#include	"serial.h"

#include	"configuration_controller.h"

#include	"serport_drvr.h"

#include	"device_generic_gr_card.h"

#include	"speaker.h"

#include	"rcvd_data.h"

#include	"cs_mem.h"

/* ---------------------------------------------------------- */
/*  			Lantronix Hardware Configuration			  */
/* ---------------------------------------------------------- */

// 4/27/2015 mpd : Development progress has been improved a great deal by the use of a 9-pin D-Sub Y-cable that 
// provides device feedback in a PuTTY window as the SC3000 controller is communitating in real time. The PuTTY
// view is a big improvement over searching RCVD_DATA response buffers in debug to see what the device is doing.
// 
// NOTE: The PremierWave XN connection requires a 9-pin null modem cable. Isolation of the 2 wires needed by PuTTY can 
// be done with the 9-pin, 2-wire (red and black) bridge connector. 
//
// Only 2 wires from the 9-pin D-Sub should connect to the PC COM port through a null modem cable:
// TX	Pin 3	Brown
// Grd	Pin 5	Green
// 
// CONNECTION->SERIAL:
//  Speed: 9600
//  Data Bits: 8
//  Stop Bits: 1
//  Parity: None
//  Flow Control: None

/* ---------------------------------------------------------- */
/*  					Global Structures   				  */
/* ---------------------------------------------------------- */

// The "WEN_DETAILS_STRUCT" struct contains fields that specifically apply only to WEN devices. 
WEN_DETAILS_STRUCT wen_details;

/* ---------------------------------------------------------- */
/*  			 String Extraction and Analysis 			  */
/* ---------------------------------------------------------- */

//#define WEN_VERBOSE_ALERTS 
//#define WEN_DEBUG_ANALYSIS		

// 6/10/2015 mpd : This function is not needed at the moment. The device data has been extracted using a response from
// a different command.
#if 0
void wen_analyze_xsr_dump_wlan0( DEV_STATE_STRUCT *dev_state )
{
	char *temp_ptr;
	WEN_DETAILS_STRUCT *wen_details = dev_state->wen_details;

	#ifdef DEV_DEBUG_ANALYSIS
	Alert_Message( "wen_analyze_xsr_dump_wlan0" );
	#endif

	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_X_DUMP_IP_ADD_ITEM_STR, dev_state->resp_len ) ) != NULL )
	{
		// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
		dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_DUMP_VALUE_STR, DEV_X_DUMP_VALUE_OFFSET, sizeof( wen_details->ip_address ), wen_details->ip_address, "IP ADD" );
	}

	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_X_DUMP_NET_MASK_ITEM_STR, dev_state->resp_len ) ) != NULL )
	{
		// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
		dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_DUMP_VALUE_STR, DEV_X_DUMP_VALUE_OFFSET, sizeof( wen_details->mask ), wen_details->mask, "MASK" );
	}

	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_X_DUMP_GW_ADD_ITEM_STR, dev_state->resp_len ) ) != NULL )
	{
		// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
		dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_DUMP_VALUE_STR, DEV_X_DUMP_VALUE_OFFSET, sizeof( wen_details->gw_address ), wen_details->gw_address, "GW ADD" );
	}

	// 5/28/2015 mpd : Use "0.0.0.0" for any addresses that are empty strings.
	if( strlen( wen_details->ip_address ) == 0 )
	{
		strlcpy( wen_details->ip_address, DEV_NULL_IP_ADD_STR, sizeof( wen_details->ip_address ) );
	}
	if( strlen( wen_details->mask ) == 0 )
	{
		strlcpy( wen_details->mask, DEV_NULL_IP_ADD_STR, sizeof( wen_details->mask ) );
	}
	if( strlen( wen_details->gw_address ) == 0 )
	{
		strlcpy( wen_details->gw_address, DEV_NULL_IP_ADD_STR, sizeof( wen_details->gw_address ) );
	}

	// 5/28/2015 mpd : Populate the octet strings. These are needed to program the device and update the GUI fields.
	dev_extract_ip_octets( wen_details->ip_address, wen_details->ip_address_1, wen_details->ip_address_2, wen_details->ip_address_3, wen_details->ip_address_4 );
	dev_extract_ip_octets( wen_details->mask,       wen_details->mask_1,       wen_details->mask_2,       wen_details->mask_3,       wen_details->mask_4 );
	dev_extract_ip_octets( wen_details->gw_address, wen_details->gw_address_1, wen_details->gw_address_2, wen_details->gw_address_3, wen_details->gw_address_4 );

	// 5/28/2015 mpd : TODO: The GUI wants an UNS_32 for the mask. Write a function to analze the octets.

}
#endif

void wen_analyze_xcr_dump_interface( DEV_STATE_STRUCT *dev_state )
{
	char *temp_ptr;
	char  temp_text[ sizeof( DEV_DISABLED_STR ) ];
	WEN_DETAILS_STRUCT *wen_details = dev_state->wen_details;

	#ifdef DEV_DEBUG_ANALYSIS
	Alert_Message( "wen_analyze_xcr_dump_interface" );
	#endif

	// 4/16/2015 mpd : Send progress info to the GUI.
	dev_update_info( dev_state, "Reading WLAN details...", DEVICE_EXCHANGE_KEY_read_settings_in_progress );

	// DHCP
	// 10/26/2015 ajv : Default DHCP enabled to TRUE unless it's explicitly disabled
	dev_state->dhcp_enabled = (true);

	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_X_DUMP_DHCP_STR, dev_state->resp_len ) ) != NULL )
	{
		// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
		dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_DUMP_VALUE_STR, DEV_X_DUMP_VALUE_OFFSET, sizeof( temp_text ), temp_text, "DHCP" );

		if( ( strncmp( temp_text, DEV_DISABLED_STR, sizeof( DEV_DISABLED_STR ) ) ) == NULL ) 
		{
			dev_state->dhcp_enabled = false;
		}
	}

	// IP ADDRESS
	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_X_DUMP_IP_ADD_STR, dev_state->resp_len ) ) != NULL )
	{
		// 9/22/2015 mpd : FogBugz #3163. The XCR dump contains "None" starting at temp_ptr + 43 if no IP address is 
		// set. Check for this before attempting to extract a legitimate IP address (which would fail due to not finding
		// the "<" delimiter within the IP address length).
		if( ( find_string_in_block( temp_ptr, DEV_X_DUMP_NONE_STR, DEV_X_DUMP_NONE_FIND_LEN ) ) == NULL ) 
		{
			// 9/22/2015 mpd : FogBugz #3163. "None" was not found. Continue normal extraction. 

			// 9/24/2015 mpd : FogBugz #3177: IP addresses in CIDR format contain up to 3 extra characters (e.g. 
			// "xxx.xxx.xxx.xxx/24") in the XCR dump response. Original XN and XE parsing code expected a delimiter of 
			// "</value>" at or before byte 15. The extra characters in the response meant that the delimiter was not 
			// found and parsing failed if the IP address contained more than 12 characters. 

			// 9/24/2015 mpd : FogBugz #3177: Provide a temporary container to hold the CIDR content.
			char temp_cidr[ DEV_IP_ADD_CIDR_LEN ];
			char *cidr_ptr;

			// 9/24/2015 mpd : FogBugz #3177: Extract to the temporary container instead of directly to the IP address.
			//dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_DUMP_VALUE_STR, DEV_X_DUMP_VALUE_OFFSET, sizeof( wen_details->ip_address ), wen_details->ip_address, "IP" );
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_DUMP_VALUE_STR, DEV_X_DUMP_VALUE_OFFSET, sizeof( temp_cidr ), temp_cidr, "CIDR" );

			// 9/24/2015 mpd : FogBugz #3177: Isolate the IP address before the "/".
			cidr_ptr = strtok ( temp_cidr, DEV_X_DUMP_SLASH_DELIMITER );

			if( cidr_ptr != NULL )
			{
				strlcpy( wen_details->ip_address, cidr_ptr, sizeof( wen_details->ip_address ) );
			}
			else 
			{
				strlcpy( wen_details->ip_address, DEV_NULL_IP_ADD_STR, sizeof( wen_details->ip_address ) );
			}

			// 9/24/2015 mpd : FogBugz #3178: Isolate the net mask bit count before the "<". A subsequent call to 
			// "strtok()" always returns a NULL, so we will have to test the result instead of the pointer for validity.
			cidr_ptr = strtok ( NULL, DEV_X_DUMP_DELIMITER );

			wen_details->mask_bits = atoi( cidr_ptr );

			if( ( wen_details->mask_bits < XE_XN_DEVICE_NETMASK_MIN ) || ( wen_details->mask_bits > XE_XN_DEVICE_NETMASK_MAX ) )
			{
				wen_details->mask_bits = XE_XN_DEVICE_NETMASK_DEFAULT;
			}
		}
		else
		{
			// 9/22/2015 mpd : FogBugz #3163. "None" was found. Skip normal extraction and assign "0.0.0.0". 
			strlcpy( wen_details->ip_address, DEV_NULL_IP_ADD_STR, sizeof( wen_details->ip_address ) );

			// 9/24/2015 mpd : FogBugz #3178: Use the default number of mask bits. 
			wen_details->mask_bits = XE_XN_DEVICE_NETMASK_DEFAULT;
		}

		dev_extract_ip_octets( wen_details->ip_address, wen_details->ip_address_1, wen_details->ip_address_2, wen_details->ip_address_3, wen_details->ip_address_4 );
	}

	// GATEWAY
	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_X_DUMP_DEF_GW_STR, dev_state->resp_len ) ) != NULL )
	{
		// 9/22/2015 mpd : FogBugz #3163. The XCR dump contains "None" starting at temp_ptr + 43 if no GW address is 
		// set. Check for this before attempting to extract a legitimate GW address (which would fail due to not finding
		// the "<" delimiter within the GW address length).
		if( ( find_string_in_block( temp_ptr, DEV_X_DUMP_NONE_STR, DEV_X_DUMP_NONE_FIND_LEN ) ) == NULL ) 
		{
			// 9/22/2015 mpd : FogBugz #3163. "None" was not found. Continue normal extraction. 
			// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_DUMP_VALUE_STR, DEV_X_DUMP_VALUE_OFFSET, sizeof( wen_details->gw_address ), wen_details->gw_address, "GW" );
		}
		else
		{
			// 9/22/2015 mpd : FogBugz #3163. "None" was found. Skip normal extraction and assign "0.0.0.0". 
			strlcpy( wen_details->gw_address, DEV_NULL_IP_ADD_STR, sizeof( wen_details->gw_address ) );
		}

		dev_extract_ip_octets( wen_details->gw_address, wen_details->gw_address_1, wen_details->gw_address_2, wen_details->gw_address_3, wen_details->gw_address_4 );
	}

	// HOSTNAME
	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_X_DUMP_HOSTNAME_STR, dev_state->resp_len ) ) != NULL )
	{
		// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
		dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_DUMP_VALUE_STR, DEV_X_DUMP_VALUE_OFFSET, sizeof( wen_details->dhcp_name ), wen_details->dhcp_name, "NAME" );

		// 6/22/2015 mpd : Determine if the DHCP name has ever been known. This flag prevents us from stomping on an 
		// existing name with the MAC address (the suggested default name).
		if( strlen( wen_details->dhcp_name ) == 0 )
		{
			wen_details->dhcp_name_not_set = true;
		}
		else
		{
			wen_details->dhcp_name_not_set = false;
		}
	}

}

extern void wen_analyze_xcr_dump_profile( DEV_STATE_STRUCT *dev_state )
{
	char *temp_ptr;
	WEN_DETAILS_STRUCT *wen_details = dev_state->wen_details;
	char  temp_text[ DEV_LONGEST_FIELD_LEN ];

	#ifdef DEV_DEBUG_ANALYSIS
	Alert_Message( "wen_analyze_xcr_dump_profile" );
	#endif

	// 4/16/2015 mpd : Send progress info to the GUI.
	dev_update_info( dev_state, "Reading WIFI details...", DEVICE_EXCHANGE_KEY_read_settings_in_progress );

	// 5/29/2015 mpd : TBD: Does every string item need to be tested for DEV_X_CP_DUMP_IGNORE_STR? Since the string 
	// begins with "<", which matches our ending delimiter, any item we don't explicitly test before extraction will 
	// have a zero length.

	// 5/29/2015 mpd : This dump depends upon the existance of the requested profile. 
	if( find_string_in_block( dev_state->resp_ptr, DEV_X_CP_DUMP_FAILURE_STR, dev_state->resp_len ) == NULL )
	{
		// 5/29/2015 mpd : The profile exists. All fields are of varying length. Some will be of zreo length.
		// Start at the beginning of the response. 
		 
		// BSSID
		// 5/29/2015 mpd : Use XML "<value>" extraction for "bssid".
		//if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_X_CP_DUMP_SSID_STR, dev_state->resp_len ) ) != NULL )
		//{
		//	// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
		//	dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_DUMP_VALUE_STR, DEV_X_DUMP_VALUE_OFFSET, sizeof( wen_details->ssid ), wen_details->ssid, "" );
		//}

		// NETWORK NAME
		// 6/18/2015 mpd : The XCR network name will contain HTML coding for special characters "&'<>"". Use the SHOW
		// command in the profile to get a clean copy.
		// 5/29/2015 mpd : The "network name" field appears to be what we want in the SSID. The "bssid" fields returned 
		// by XSR DUMP WLAN SCAN contain the MAC addresses of all our Carlsbad neighbors. NOTE: The XML leading 
		// delimiter is a single ">" for the rest of the response (including the network name).
		//if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_X_CP_DUMP_NET_NAME_STR, dev_state->resp_len ) ) != NULL )
		//{
		//	// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
		//	dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( wen_details->ssid ), wen_details->ssid, "" );
		//}

		// MODE
		// 6/18/2015 mpd : The "dev_state->resp_ptr" is used here now that NETWORK NAME is skipped. 
		if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_X_CP_DUMP_MODE_STR, dev_state->resp_len ) ) != NULL )
		{
			// 5/29/2015 mpd : Extraction code has already been proven. Pass an empty description string to save code space.  
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( wen_details->radio_mode ), wen_details->radio_mode, "" );
		}

		// 5/29/2015 mpd : The DEV_X_CP_DUMP_STATE_STR "state" XCR DUMP item is not currently being extracted.

		// 5/29/2015 mpd : The DEV_X_CP_DUMP_ANT_DIV_STR "antenna diversity" XCR DUMP item is not currently being extracted.

		// SECURITY SUITE
		// 5/29/2015 mpd : All subsequent extractions will continue where we left off. Keep these in the order of the 
		// response! 
		if( ( temp_ptr = find_string_in_block( temp_ptr, DEV_X_CP_DUMP_SUITE_STR, dev_state->resp_len ) ) != NULL )
		{
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( wen_details->security ), wen_details->security, "" );
		}

		// KEY TYPE
		if( ( temp_ptr = find_string_in_block( temp_ptr, DEV_X_CP_DUMP_KEY_TYPE_STR, dev_state->resp_len ) ) != NULL )
		{
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( wen_details->w_key_type ), wen_details->w_key_type, "" );
		}

		// PASSPHRASE
		if( ( temp_ptr = find_string_in_block( temp_ptr, DEV_X_CP_DUMP_PASSPHRASE_STR, dev_state->resp_len ) ) != NULL )
		{
			// 5/29/2015 mpd : The DEV_X_CP_DUMP_IGNORE_STR has been returned for this item in the past. Test for it. 
			if( find_string_in_block( temp_ptr, DEV_X_CP_DUMP_IGNORE_STR, dev_state->resp_len ) == NULL )
			{
				dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( wen_details->passphrase ), wen_details->passphrase, "" );
			}
			else
			{
				// 5/29/2015 mpd : TBD: Figure out what to enter for a value of "configured and ignored".
				strlcpy( wen_details->passphrase, DEV_X_CP_DUMP_IGNORE_STR, sizeof( wen_details->passphrase ) );
			}
		}

		// WEP AUTHENTICATION
		if( ( temp_ptr = find_string_in_block( temp_ptr, DEV_X_CP_DUMP_WEP_AUTH_STR, dev_state->resp_len ) ) != NULL )
		{
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( wen_details->wep_authentication ), wen_details->wep_authentication, "" );
		}

		// WEP KEY SIZE 
		if( ( temp_ptr = find_string_in_block( temp_ptr, DEV_X_CP_DUMP_WEP_KEY_SIZE_STR, dev_state->resp_len ) ) != NULL )
		{
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( wen_details->wep_key_size ), wen_details->wep_key_size, "" );
		}

		// WEP TX KEY INDEX
		if( ( temp_ptr = find_string_in_block( temp_ptr, DEV_X_CP_DUMP_WEP_TX_KEY_IDX_STR, dev_state->resp_len ) ) != NULL )
		{
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( wen_details->wep_tx_key ), wen_details->wep_tx_key, "" );
		}

		// 6/3/2015 mpd : We need a default key index if none has been set by the Gui (this happens when we were not in 
		// the WEP security suite, so the variable did not exist).
		if(  strlen( wen_details->wep_tx_key ) == 0 )
		{
			strlcpy( wen_details->wep_tx_key, "1", sizeof( wen_details->wep_tx_key ) );
		}

		// WEP KEY
		// 5/29/2015 mpd : We only store 1 key. Save this one if we are using WEP security.
		if( strstr( wen_details->security, DEV_X_CP_DUMP_SUITE_WEP_STR ) != NULL )
		{
			// 6/2/2015 mpd : Use the TX Key Index to specify which WEP Key we want to extract.
			strlcpy( temp_text, DEV_X_CP_DUMP_WEP_KEY_STR, sizeof( temp_text ) );
			strlcat( temp_text, wen_details->wep_tx_key, sizeof( temp_text ) );

			if( ( temp_ptr = find_string_in_block( temp_ptr, temp_text, dev_state->resp_len ) ) != NULL )
			{
				dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( wen_details->key ), wen_details->key, "" );
			}
		}

		// WPAX AUTHENTICATION
		if( ( temp_ptr = find_string_in_block( temp_ptr, DEV_X_CP_DUMP_WPAX_AUTH_STR, dev_state->resp_len ) ) != NULL )
		{
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( wen_details->wpa_authentication ), wen_details->wpa_authentication, "" );
		}

		// WPAX KEY
		// 5/29/2015 mpd : We only store 1 key. If we already saved the WEP key, skip this one. This value will be used
		// for all non-WEP security suites including "None". 
		if( strstr( wen_details->security, DEV_X_CP_DUMP_SUITE_WEP_STR ) == NULL )
		{
			if( ( temp_ptr = find_string_in_block( temp_ptr, DEV_X_CP_DUMP_WPAX_KEY_STR, dev_state->resp_len ) ) != NULL )
			{
				// 5/29/2015 mpd : The DEV_X_CP_DUMP_IGNORE_STR has been returned for this item in the past. Test for it. 
				if( find_string_in_block( temp_ptr, DEV_X_CP_DUMP_IGNORE_STR, dev_state->resp_len ) == NULL )
				{
					dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( wen_details->key ), wen_details->key, "" );
				}
				else
				{
					// 5/29/2015 mpd : TBD: Figure out what to enter for a value of "configured and ignored".
					strlcpy( wen_details->key, DEV_X_CP_DUMP_IGNORE_STR, sizeof( wen_details->key ) );
				}
			}
		}

		// WPAX IEEE 802.1x
		if( ( temp_ptr = find_string_in_block( temp_ptr, DEV_X_CP_DUMP_WPAX_IEEE_STR, dev_state->resp_len ) ) != NULL )
		{
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( wen_details->wpa_ieee_802_1x ), wen_details->wpa_ieee_802_1x, "" );
		}

		// WPAX EAP-TTLS OPTION
		if( ( temp_ptr = find_string_in_block( temp_ptr, DEV_X_CP_DUMP_WPAX_EAP_TTLS_STR, dev_state->resp_len ) ) != NULL )
		{
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( wen_details->wpa_eap_ttls_option ), wen_details->wpa_eap_ttls_option, "" );
		}

		// WPAX PEAP OPTION
		if( ( temp_ptr = find_string_in_block( temp_ptr, DEV_X_CP_DUMP_WPAX_PEAP_STR, dev_state->resp_len ) ) != NULL )
		{
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( wen_details->wpa_peap_option ), wen_details->wpa_peap_option, "" );
		}

		// USER NAME (used for all security suites)
		// 6/18/2015 mpd : There is a possibility of ML coding in the user name string. See the comments in this future
		// "device_common.h" function: "extern void translate_html_numbers_in_string( const char *source, char * destination )".
		if( ( temp_ptr = find_string_in_block( temp_ptr, DEV_X_CP_DUMP_WPAX_U_NAME_STR, dev_state->resp_len ) ) != NULL )
		{
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( wen_details->user_name ), wen_details->user_name, "" );
		}

		// PASSWORD (used for all security suites) 
		if( ( temp_ptr = find_string_in_block( temp_ptr, DEV_X_CP_DUMP_WPAX_P_WORD_STR, dev_state->resp_len ) ) != NULL )
		{
			// 5/29/2015 mpd : TBD: Does the password get displayed?
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( wen_details->password ), wen_details->password, "" );
		}

		// WPAX ENCRYPTION
		if( ( temp_ptr = find_string_in_block( temp_ptr, DEV_X_CP_DUMP_WPAX_ENCRIPT_STR, dev_state->resp_len ) ) != NULL )
		{
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( temp_text ), temp_text, "" );
			if( strstr( temp_text, DEV_X_CP_DUMP_WPAX_E_CCMP_STR ) != NULL )
			{
				wen_details->wpa_encription_ccmp = true;
			}
			if( strstr( temp_text, DEV_X_CP_DUMP_WPAX_E_TKIP_STR ) != NULL )
			{
				wen_details->wpa_encription_tkip = true;
			}
			if( strstr( temp_text, DEV_X_CP_DUMP_WPAX_E_WEP_STR ) != NULL )
			{
				wen_details->wpa_encription_wep = true;
			}
		}

		// WPAX VALIDAT CERTIFICATE
		if( ( temp_ptr = find_string_in_block( temp_ptr, DEV_X_CP_DUMP_WPAX_VALID_STR, dev_state->resp_len ) ) != NULL )
		{
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( temp_text ), temp_text, "" );
			if( strstr( temp_text, DEV_ENABLED_STR ) != NULL )
			{
				wen_details->wpa_eap_tls_valid_cert = true;
			}
			else
			{
				wen_details->wpa_eap_tls_valid_cert = false;
			}
		}

		// WPAX CREDENTIALS
		if( ( temp_ptr = find_string_in_block( temp_ptr, DEV_X_CP_DUMP_WPAX_CRED_STR, dev_state->resp_len ) ) != NULL )
		{
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_CP_DUMP_GREATER_STR, DEV_X_CP_DUMP_GREATER_OFFSET, sizeof( wen_details->wpa_eap_tls_credentials ), wen_details->wpa_eap_tls_credentials, "" );
		}
	}
	else
	{
		// 5/29/2015 mpd : Bad news. The profile does not exist. Set all variables to initialization values.
		wen_initialize_profile_vars( dev_state );
	}

}

extern void wen_analyze_profile_show( DEV_STATE_STRUCT *dev_state )
{
	WEN_DETAILS_STRUCT *wen_details = dev_state->wen_details;

	#ifdef DEV_DEBUG_ANALYSIS
	Alert_Message( "wen_analyze_profile_show" );
	#endif

	// NETWORK NAME
	// 6/18/2015 mpd : The XCR dump network name will contain HTML coding for special characters "&'<>"". Use this SHOW
	// command in the default profile to get a clean copy.
	// 5/29/2015 mpd : The "network name" field appears to be what we want in the SSID. 
	// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
	dev_extract_delimited_text_from_buffer( dev_state->resp_ptr, DEV_CR_STR, DEV_P_SHOW_NET_NAME_STR, DEV_P_SHOW_NET_NAME_OFFSET, sizeof( wen_details->ssid ), wen_details->ssid, "SSID" );

	// 6/18/2015 mpd : The WEN device accepts all characters except the single quote without escape
	// characters. The XCR dump we previously use to read the name is not as clean.
	// #network name "!@#$%^&*()_+-={}|[]\:;'<>?,./`~\""
	// <value name = "network name">!@#$%^&amp;#38;*()_+-={}|[]\:;&amp;#39;&amp;#60;&amp;#62;?,./`~&amp;#34;</value>
	// "&" -> &amp;#38;
	// "'" -> &amp;#39;
	// "<" -> &amp;#60;
	// ">" -> &amp;#62;
	// """ -> &amp;#34;

}

static void wen_write_progress_keys( DEV_STATE_STRUCT *dev_state )
{
	// 4/16/2015 mpd : Send progress info to the GUI.
	dev_update_info( dev_state, "Writing WEP key...", DEVICE_EXCHANGE_KEY_write_settings_in_progress );

}

static void wen_write_progress_eth0( DEV_STATE_STRUCT *dev_state )
{
	// 4/16/2015 mpd : Send progress info to the GUI.
	dev_update_info( dev_state, "Writing Ethernet settings...", DEVICE_EXCHANGE_KEY_write_settings_in_progress );

}

static void wen_write_progress_wifi( DEV_STATE_STRUCT *dev_state )
{
	// 4/16/2015 mpd : Send progress info to the GUI.
	dev_update_info( dev_state, "Writing Wi-Fi settings...", DEVICE_EXCHANGE_KEY_write_settings_in_progress );

}

static void wen_write_progress_security( DEV_STATE_STRUCT *dev_state )
{
	// 4/16/2015 mpd : Send progress info to the GUI.
	dev_update_info( dev_state, "Writing security details...", DEVICE_EXCHANGE_KEY_write_settings_in_progress );

}

static void wen_write_progress_sec_25( DEV_STATE_STRUCT *dev_state )
{
	// 4/16/2015 mpd : Send progress info to the GUI.
	dev_update_info( dev_state, "Writing security details. 25% complete.", DEVICE_EXCHANGE_KEY_write_settings_in_progress );

}

static void wen_write_progress_sec_50( DEV_STATE_STRUCT *dev_state )
{
	// 4/16/2015 mpd : Send progress info to the GUI.
	dev_update_info( dev_state, "Writing security details. 50% complete.", DEVICE_EXCHANGE_KEY_write_settings_in_progress );

}

static void wen_write_progress_sec_75( DEV_STATE_STRUCT *dev_state )
{
	// 4/16/2015 mpd : Send progress info to the GUI.
	dev_update_info( dev_state, "Writing security details. 75% complete.", DEVICE_EXCHANGE_KEY_write_settings_in_progress );

}

/* ---------------------------------------------------------- */
/*  					Read and Write Lists  				  */
/* ---------------------------------------------------------- */

// 5/6/2015 mpd : Except for the SHOW commands, GR/WEN responses are very short strings preceeding the "#" prompt. The 
// termination strings have been chosen with as few unique characters as possible to reduce data space. For additional
// savings, reuse the termination string as the title of the response. 

// LANTRONIX ENTER CLI MODE INSTRUCTIONS:
// 1. Power off the device.
// 2. Press and hold down the exclamation point "!" key ( DEV_COMMAND_START ).
// 3. Power on the device. 
// 4. After about 1 seconds, a response containing the exclamation point will arrive ( DEV_TERM_START_STR ).
// 5. Type "xyz" within 5 seconds to display the CLI prompt ( performed in the DEV_WAITING_FOR_RESPONSE state using 
//    DEV_COMMAND_CLI ).
// 6. Type "enable" to access further command menus ( DEV_COMMAND_ENABLE ).

const DEV_MENU_ITEM wen_read_list[] = { 
	// Response Title             Response Handler        	      Next Command            Next Termination String
	{ DEV_NO_TITLE_STR,   		  NULL,							  DEV_COMMAND_START,      DEV_TERM_START_STR },		// This first entry is used by device exchange initialization.  
	{ DEV_TERM_START_STR,   	  NULL,							  DEV_COMMAND_CLI,        DEV_TERM_PROMPT_STR },	// This second entry is used by the DEV_WAITING_FOR_RESPONSE state.  
	{ DEV_TERM_PROMPT_STR,   	  NULL,					  	      DEV_COMMAND_ENABLE,     DEV_TERM_ENABLE_STR },	// Got to the ENABLE menu. 
	//{ DEV_TERM_ENABLE_STR,	  dev_wait_for_network,	 	   	  DEV_COMMAND_CR, 	      DEV_TERM_ENABLE_STR },	// Wait for a network connection. 
	{ DEV_TERM_ENABLE_STR,	      NULL,					 	   	  DEV_COMMAND_CR_DELAY,   DEV_TERM_ENABLE_STR },	// Give the device time to accept commands. 
	{ DEV_TERM_ENABLE_STR,		  NULL,					 	   	  DEV_COMMAND_XML, 	      DEV_TERM_XML_STR },		// Go to the XML menu.
	{ DEV_TERM_XML_STR,		  	  NULL,  						  DEV_COMMAND_X_CD_DUMP,  DEV_TERM_XML_STR },		// Get the XCR dump response. 
																													
	// 5/28/2015 mpd : TBD: See if we can skip all commands that require a network response and still have a useful screen.
	//{ DEV_TERM_XML_STR,		  dev_analyze_xcr_dump_device, 	  DEV_COMMAND_X_SI_DUMP,  DEV_TERM_XML_STR },		// Analyze the dump response. Get the XSR dump response.
	//{ DEV_TERM_XML_STR,		  wen_analyze_xsr_dump_wlan0, 	  DEV_COMMAND_EXIT,       DEV_TERM_ENABLE_STR },	// Analyze the dump response. Exit XML. 
	{ DEV_TERM_XML_STR, 		  dev_analyze_xcr_dump_device, 	  DEV_COMMAND_X_CI_DUMP,  DEV_TERM_XML_STR },		// Analyze the dump response. Get the XCR interface dump response.
	{ DEV_TERM_XML_STR, 		  wen_analyze_xcr_dump_interface, DEV_COMMAND_X_CP_DUMP,  DEV_TERM_XML_STR },		// Analyze the dump response. Get the XCR profile dump response.
	{ DEV_TERM_XML_STR,			  wen_analyze_xcr_dump_profile,	  DEV_COMMAND_EXIT,       DEV_TERM_ENABLE_STR },	// Analyze the dump response. Exit XML. 
																												
	{ DEV_TERM_ENABLE_STR,		  NULL,					 	   	  DEV_COMMAND_SHOW, 	  DEV_TERM_ENABLE_STR },	// Get the SHOW response.
	{ DEV_TERM_ENABLE_STR,		  dev_analyze_enable_show_ip,     DEV_COMMAND_CONFIG,     DEV_TERM_CONFIG_STR },	// Analyze the SHOW response. Go to the CONFIGURE menu. 
	{ DEV_TERM_CONFIG_STR,		  NULL,				              DEV_COMMAND_WLAN_PRO,   DEV_TERM_PRO_STR },		// Go to WLAN Profiles. 
	{ DEV_TERM_PRO_STR,		 	  NULL,						      DEV_COMMAND_EDIT_DIP,   DEV_TERM_PRO_BASIC_STR },	// Edit the default profiles. 
	{ DEV_TERM_PRO_BASIC_STR,	  NULL,					 	   	  DEV_COMMAND_SHOW, 	  DEV_TERM_PRO_BASIC_STR },	// Get the SHOW response.
	{ DEV_TERM_PRO_BASIC_STR,  	  wen_analyze_profile_show,	      DEV_COMMAND_EXIT,       DEV_TERM_PRO_STR },   	// Exit the BASIC menu.  
	{ DEV_TERM_PRO_STR,		  	  NULL,				              DEV_COMMAND_EXIT,       DEV_TERM_CONFIG_STR },	// Exit the WLAN Profiles menu. 
	{ DEV_TERM_CONFIG_STR,		  dev_final_device_analysis,      DEV_COMMAND_EXIT,       DEV_TERM_ENABLE_STR },	// Final analysis. Exit CONFIGURE. 
	{ DEV_TERM_ENABLE_STR,		  NULL,							  DEV_COMMAND_EXIT_FINAL, DEV_TERM_NO_RESP_STR },	// Exit ENABLE. There is no response to the final exit. 
	{ DEV_TERM_ENABLE_STR,		  dev_cli_disconnect,			  DEV_COMMAND_END, 	      DEV_TERM_NO_RESP_STR }	// Drop DTR to exit CLI. 
};

// 6/10/2015 mpd : This was cloned from GR before the diagnostic functionality diverged from the programming screen. It 
// may save some development time when diagnostics are implemented.
const DEV_MENU_ITEM wen_diagnostic_write_list[] = {  
#if 0
	// Response Title             Response Handler              Next Command           Next Termination String
	{ DEV_NO_TITLE_STR,   		  NULL, 					    DEV_COMMAND_START,       DEV_TERM_START_STR },		// This first entry is used by device exchange initialization.  
	{ DEV_TERM_START_STR,   	  NULL,							DEV_COMMAND_CLI,         DEV_TERM_PROMPT_STR },		// This second entry is used by the DEV_WAITING_FOR_RESPONSE state.  
	{ DEV_TERM_PROMPT_STR, 		  NULL,				            DEV_COMMAND_ENABLE,      DEV_TERM_ENABLE_STR },		// Got to the ENABLE menu. 
	{ DEV_TERM_ENABLE_STR,	  	  dev_wait_for_ready,	 	   	DEV_COMMAND_CR,          DEV_TERM_ENABLE_STR },  	// Give the device time to accept commands. 
	{ DEV_TERM_ENABLE_STR,		  dev_device_write_progress,    DEV_COMMAND_CONFIG,      DEV_TERM_CONFIG_STR },		// Go to the CONFIGURE menu. 

	{ DEV_TERM_CONFIG_STR,		  NULL,				            DEV_COMMAND_CLOCK,       DEV_TERM_CLOCK_STR },		// Go to the CLOCK menu. 
	{ DEV_TERM_CLOCK_STR,		  NULL,				            DEV_COMMAND_SYNC,        DEV_TERM_CLOCK_STR },		// Sync the clock using SNTP. 
	{ DEV_TERM_CLOCK_STR,		  NULL,				            DEV_COMMAND_CLOCK_TZ,    DEV_TERM_CLOCK_STR },		// Set time zone to Pacific. 
	{ DEV_TERM_CLOCK_STR,		  NULL,			 	            DEV_COMMAND_EXIT,        DEV_TERM_CONFIG_STR },		// Exit CLOCK. 
	{ DEV_TERM_CONFIG_STR,		  NULL,				            DEV_COMMAND_EXIT,        DEV_TERM_ENABLE_STR },		// Exit CONFIGURE. 
																						 
	// 6/2/2015 mpd : TBD: Is this failure true for WEN?
	// 5/13/2015 mpd : These settings can't be set in CLI on the target port. They drop the connection.
	//{ DEV_TERM_ENABLE_STR,	  NULL,				            DEV_COMMAND_LINE,        DEV_TERM_LINE_STR },		// Go to the LINE:1 menu. 
	//{ DEV_TERM_LINE_STR,		  NULL,			            	DEV_COMMAND_BAUD,        DEV_TERM_LINE_STR },		// Set the baud rate. <-  This command hangs the interface
	//{ DEV_TERM_LINE_STR,		  NULL,			            	DEV_COMMAND_FLOW,        DEV_TERM_LINE_STR },		// Set flow control.  <-  This command hangs the interface
	//{ DEV_TERM_LINE_STR,		  NULL,  						DEV_COMMAND_EXIT,        DEV_TERM_ENABLE_STR },		// Exit LINE:1.

	{ DEV_TERM_ENABLE_STR,	      NULL,				            DEV_COMMAND_CONFIG,      DEV_TERM_CONFIG_STR },		// Go to the CONFIGURE menu. 
	{ DEV_TERM_CONFIG_STR,		  NULL,				            DEV_COMMAND_IF_E,        DEV_TERM_IF_E_STR },		// Go to the ETH0 menu. 
	{ DEV_TERM_IF_E_STR,		  NULL,				            DEV_COMMAND_PRIORITY,    DEV_TERM_IF_E_STR },		// Set priority 2. 
	{ DEV_TERM_IF_E_STR,		  NULL,			            	DEV_COMMAND_WRITE,       DEV_TERM_IF_E_STR },		// Save the config to permanent memory.
	{ DEV_TERM_IF_E_STR,		  NULL,				            DEV_COMMAND_EXIT,  	     DEV_TERM_CONFIG_STR },		// Exit ETH0. 
	{ DEV_TERM_CONFIG_STR,		  NULL,				            DEV_COMMAND_EXIT,        DEV_TERM_ENABLE_STR },		// Exit CONFIGURE. 

	{ DEV_TERM_ENABLE_STR,		  NULL,				            DEV_COMMAND_TUNNEL,      DEV_TERM_TUNNEL_STR },		// Go to TUNNEL:1 menu. 
	{ DEV_TERM_TUNNEL_STR,		  NULL,				            DEV_COMMAND_ACCEPT,      DEV_TERM_ACCEPT_STR },		// Go to ACCEPT menu. 
	{ DEV_TERM_ACCEPT_STR,		  NULL,				            DEV_COMMAND_A_MODE,      DEV_TERM_ACCEPT_STR },		// Disable accept mode. 
	{ DEV_TERM_ACCEPT_STR,		  NULL,			            	DEV_COMMAND_WRITE,       DEV_TERM_ACCEPT_STR },		// Save the config to permanent memory.
	{ DEV_TERM_ACCEPT_STR,		  NULL,				            DEV_COMMAND_EXIT,        DEV_TERM_TUNNEL_STR },		// Exit ACCEPT. 

	{ DEV_TERM_TUNNEL_STR,		  NULL,				            DEV_COMMAND_CONNECT,     DEV_TERM_CONN_STR },		// Go to CONNECT menu. 
	{ DEV_TERM_CONN_STR,		  NULL,				            DEV_COMMAND_C_MODE,      DEV_TERM_CONN_STR },		// Set connect mode always. 
	{ DEV_TERM_CONN_STR,		  NULL,			            	DEV_COMMAND_WRITE,       DEV_TERM_CONN_STR },		// Save the config to permanent memory.
	{ DEV_TERM_CONN_STR,		  NULL,				            DEV_COMMAND_HOST,        DEV_TERM_HOST_STR },		// Go to HOST:1 menu. 
	{ DEV_TERM_HOST_STR,		  NULL,				            DEV_COMMAND_ADDRESS,     DEV_TERM_HOST_STR },		// Set host IP address. 
	{ DEV_TERM_HOST_STR,		  NULL,				            DEV_COMMAND_PORT,        DEV_TERM_HOST_STR },		// Set host port. 
	{ DEV_TERM_HOST_STR,		  NULL,				            DEV_COMMAND_ALIVE,       DEV_TERM_HOST_STR },		// Set keep alive time. <- This is currently in msec. 
	{ DEV_TERM_HOST_STR,		  NULL,			            	DEV_COMMAND_WRITE,       DEV_TERM_HOST_STR },		// Save the config to permanent memory.
	{ DEV_TERM_HOST_STR,		  NULL,				            DEV_COMMAND_EXIT,        DEV_TERM_CONN_STR },		// Exit HOST:1. 
	{ DEV_TERM_CONN_STR,		  NULL,				            DEV_COMMAND_EXIT,        DEV_TERM_TUNNEL_STR },		// Exit CONNECT. 

	{ DEV_TERM_TUNNEL_STR,		  NULL,				            DEV_COMMAND_MODEM,	     DEV_TERM_MODEM_STR },		// Go to MODEM menu. 
	{ DEV_TERM_MODEM_STR,		  NULL,				            DEV_COMMAND_ECHO,	     DEV_TERM_MODEM_STR },		// Disable echo commands. 
	{ DEV_TERM_MODEM_STR,		  NULL,				            DEV_COMMAND_VERBOSE,     DEV_TERM_MODEM_STR },		// Disable verbose response. 
	{ DEV_TERM_MODEM_STR,		  NULL,			            	DEV_COMMAND_WRITE,       DEV_TERM_MODEM_STR },		// Save the config to permanent memory.
	{ DEV_TERM_MODEM_STR,		  NULL,				            DEV_COMMAND_EXIT,        DEV_TERM_TUNNEL_STR },		// Exit MODEM. 
	{ DEV_TERM_TUNNEL_STR,		  NULL,				            DEV_COMMAND_EXIT,        DEV_TERM_ENABLE_STR },		// Exit TUNNEL. 

	{ DEV_TERM_ENABLE_STR,		  NULL,							DEV_COMMAND_EXIT_FINAL,  DEV_TERM_NO_RESP_STR },	// Exit ENABLE. There is no response to the final exit. 
	{ DEV_NO_TITLE_STR,			  dev_cli_disconnect,			DEV_COMMAND_END, 	     DEV_TERM_NO_RESP_STR }		// Drop DTR to exit CLI. 

	// 5/14/2015 mpd : There needs to be a reboot or power cycle after this WRITE. Do it here only if the 
	// subsequent READ no longer does it.
#endif
};

const DEV_MENU_ITEM wen_write_list[] = {  
	// Response Title             Response Handler              Next Command           		 Next Termination String
	{ DEV_NO_TITLE_STR,   		  NULL, 					    DEV_COMMAND_START,           DEV_TERM_START_STR },		// This first entry is used by device exchange initialization.
	{ DEV_TERM_START_STR,   	  NULL,							DEV_COMMAND_CLI,             DEV_TERM_PROMPT_STR },		// This second entry is used by the DEV_WAITING_FOR_RESPONSE state.  
	{ DEV_TERM_PROMPT_STR, 		  NULL,				            DEV_COMMAND_ENABLE,          DEV_TERM_ENABLE_STR },		// Got to the ENABLE menu. 
	{ DEV_TERM_ENABLE_STR,	  	  NULL,					 	   	DEV_COMMAND_CR_DELAY,        DEV_TERM_ENABLE_STR },  	// Give the device time to accept commands. 
	{ DEV_TERM_ENABLE_STR,		  wen_write_progress_eth0,      DEV_COMMAND_CONFIG,          DEV_TERM_CONFIG_STR },		// Go to the CONFIGURE menu. 
																						   
	// 6/3/2015 mpd : WEN Programming Screen Items:                                        
	{ DEV_TERM_CONFIG_STR,		  NULL,				            DEV_COMMAND_IF_WLAN,         DEV_TERM_IF_WLAN_STR },	// Go to the WLAN0 menu. 
	{ DEV_TERM_IF_WLAN_STR,		  NULL,				            DEV_COMMAND_DHCP,            DEV_TERM_IF_WLAN_STR },	// Set DHCP based on user selection. 
	{ DEV_TERM_IF_WLAN_STR,		  NULL,				            DEV_COMMAND_PW_XN_IP_CIDR,   DEV_TERM_IF_WLAN_STR },	// Set IP/CIDR. 
	{ DEV_TERM_IF_WLAN_STR,		  NULL,				            DEV_COMMAND_PW_XN_GATEWAY,   DEV_TERM_IF_WLAN_STR },	// Set default gateway. 
	{ DEV_TERM_IF_WLAN_STR,		  NULL,				            DEV_COMMAND_PW_XN_HOSTNAME,  DEV_TERM_IF_WLAN_STR },	// Set the hostname. 
	{ DEV_TERM_IF_WLAN_STR,		  NULL,			            	DEV_COMMAND_WRITE,      	 DEV_TERM_IF_WLAN_STR },	// Save the config to permanent memory.
	{ DEV_TERM_IF_WLAN_STR,		  NULL,				            DEV_COMMAND_EXIT,  	    	 DEV_TERM_CONFIG_STR },		// Exit WLAN0. 
																													
	// 6/3/2015 mpd : WEN WIFI Settings Items:
	{ DEV_TERM_CONFIG_STR,		  NULL,				            DEV_COMMAND_WLAN_PRO,        DEV_TERM_PRO_STR },		// Go to WLAN Profiles. 
	{ DEV_TERM_PRO_STR,		 	  NULL,						    DEV_COMMAND_EDIT_DIP,        DEV_TERM_PRO_BASIC_STR },	// Edit the default profiles. 
	{ DEV_TERM_PRO_BASIC_STR,	  wen_write_progress_wifi,		DEV_COMMAND_802_MODE,        DEV_TERM_PRO_BASIC_STR },	// Set the mode. 
	{ DEV_TERM_PRO_BASIC_STR,	  NULL,				            DEV_COMMAND_SSID,            DEV_TERM_PRO_BASIC_STR },	// Set the SSID. 
	{ DEV_TERM_PRO_BASIC_STR,	  NULL,				            DEV_COMMAND_WRITE,           DEV_TERM_PRO_BASIC_STR },	// Save the config to permanent memory.
																							
	{ DEV_TERM_PRO_BASIC_STR,	  NULL,				            DEV_COMMAND_SECURITY,        DEV_TERM_PRO_SEC_STR },	// Go to the SECURITY menu. 
	{ DEV_TERM_PRO_SEC_STR,		  NULL,				            DEV_COMMAND_SUITE,           DEV_TERM_PRO_SEC_STR },	// Set the security suite. 
	{ DEV_TERM_PRO_SEC_STR,		  NULL,				            DEV_COMMAND_KEY_TYPE,        DEV_TERM_PRO_SEC_STR },	// Set the key type. 
	{ DEV_TERM_PRO_SEC_STR,		  NULL,				            DEV_COMMAND_PASSPHRASE,      DEV_TERM_PRO_SEC_STR },	// Set the passphrase. 
	{ DEV_TERM_PRO_SEC_STR,		  NULL,			            	DEV_COMMAND_WRITE,           DEV_TERM_PRO_SEC_STR },	// Save the config to permanent memory.
	
	// 6/15/2015 mpd : Original order:
	//{ DEV_TERM_PRO_SEC_STR,	  NULL,				            DEV_COMMAND_WEP,	         DEV_TERM_PRO_WEP_STR },	// Go to the WEP menu. 
	//{ DEV_TERM_PRO_WEP_STR,	  wen_write_progress_security,  DEV_COMMAND_WEP_AUTH,        DEV_TERM_PRO_WEP_STR },	// Set authentication. 
	//{ DEV_TERM_PRO_WEP_STR,	  NULL,				            DEV_COMMAND_KEY_SIZE,        DEV_TERM_PRO_WEP_STR },	// Set the key size. 
	//{ DEV_TERM_PRO_WEP_STR,	  NULL,				            DEV_COMMAND_KEY_IDX,         DEV_TERM_PRO_WEP_STR },	// Set the key index. 
	//{ DEV_TERM_PRO_WEP_STR,	  NULL,			            	DEV_COMMAND_WRITE,           DEV_TERM_PRO_WEP_STR },	// Save the config to permanent memory.
																							
	//{ DEV_TERM_PRO_WEP_STR,	  NULL,				            DEV_COMMAND_WEP_KX,          DEV_TERM_PRO_WEP_KX_STR },	// Go to the KEY X menu. 
	//{ DEV_TERM_PRO_WEP_KX_STR,  NULL,				            DEV_COMMAND_KEY_SET,         DEV_TERM_PRO_WEP_KX_STR },	// Set the key. 
	//{ DEV_TERM_PRO_WEP_KX_STR,  NULL,			            	DEV_COMMAND_WRITE,           DEV_TERM_PRO_WEP_KX_STR },	// Save the config to permanent memory.
	//{ DEV_TERM_PRO_WEP_KX_STR,  wen_write_progress_sec_25,    DEV_COMMAND_EXIT,            DEV_TERM_PRO_WEP_STR },	// Exit the Key X menu. 
	//{ DEV_TERM_PRO_WEP_STR,	  NULL,				            DEV_COMMAND_EXIT,            DEV_TERM_PRO_SEC_STR },	// Exit the WEP menu. 

	// 6/15/2015 mpd : NEW ORDER:
	{ DEV_TERM_PRO_SEC_STR,		  wen_write_progress_security,  DEV_COMMAND_WEP,	         DEV_TERM_PRO_WEP_STR },	// Go to the WEP menu. 
	{ DEV_TERM_PRO_WEP_STR,		  NULL,				            DEV_COMMAND_KEY_IDX,         DEV_TERM_PRO_WEP_STR },	// Set the key index. 
	{ DEV_TERM_PRO_WEP_STR,		  NULL,			            	DEV_COMMAND_WRITE,           DEV_TERM_PRO_WEP_STR },	// Save the config to permanent memory.
																							
	// 6/17/2015 mpd : THIS IS FRAGILE! If there are no WEP key or key length changes, we can skip the entire Key 1 - 4 
	// clearing process and save 55 seconds. Any changes to the next 16 lines will break the increment code in 
	// DEV_COMMAND_WEP_K1.
	{ DEV_TERM_PRO_WEP_STR,		  NULL,				            DEV_COMMAND_WEP_K1,          DEV_TERM_PRO_WEP_KX_STR },	// Go to the KEY 1 menu. 
	{ DEV_TERM_PRO_WEP_KX_STR,	  wen_write_progress_keys,      DEV_COMMAND_KEY_CLEAR_WEP,   DEV_TERM_PRO_WEP_KX_STR },	// Clear the current key. 
	{ DEV_TERM_PRO_WEP_KX_STR,	  NULL,			            	DEV_COMMAND_WRITE,           DEV_TERM_PRO_WEP_KX_STR },	// Save the config to permanent memory.
	{ DEV_TERM_PRO_WEP_KX_STR,	  NULL,    					    DEV_COMMAND_EXIT,            DEV_TERM_PRO_WEP_STR },	// Exit the Key 1 menu. 
																													
	{ DEV_TERM_PRO_WEP_STR,		  NULL,				            DEV_COMMAND_WEP_K2,          DEV_TERM_PRO_WEP_KX_STR },	// Go to the KEY 2 menu. 
	{ DEV_TERM_PRO_WEP_KX_STR,	  NULL,				            DEV_COMMAND_KEY_CLEAR_WEP,   DEV_TERM_PRO_WEP_KX_STR },	// Clear the current key. 
	{ DEV_TERM_PRO_WEP_KX_STR,	  NULL,			            	DEV_COMMAND_WRITE,           DEV_TERM_PRO_WEP_KX_STR },	// Save the config to permanent memory.
	{ DEV_TERM_PRO_WEP_KX_STR,	  NULL,    					    DEV_COMMAND_EXIT,            DEV_TERM_PRO_WEP_STR },	// Exit the Key 2 menu. 

	{ DEV_TERM_PRO_WEP_STR,		  NULL,						    DEV_COMMAND_WEP_K3,          DEV_TERM_PRO_WEP_KX_STR },	// Go to the KEY 3 menu. 
	{ DEV_TERM_PRO_WEP_KX_STR,	  NULL,				            DEV_COMMAND_KEY_CLEAR_WEP,   DEV_TERM_PRO_WEP_KX_STR },	// Clear the current key. 
	{ DEV_TERM_PRO_WEP_KX_STR,	  NULL,			            	DEV_COMMAND_WRITE,           DEV_TERM_PRO_WEP_KX_STR },	// Save the config to permanent memory.
	{ DEV_TERM_PRO_WEP_KX_STR,	  NULL,    					    DEV_COMMAND_EXIT,            DEV_TERM_PRO_WEP_STR },	// Exit the Key 3 menu. 

	{ DEV_TERM_PRO_WEP_STR,		  NULL,				            DEV_COMMAND_WEP_K4,          DEV_TERM_PRO_WEP_KX_STR },	// Go to the KEY 4 menu. 
	{ DEV_TERM_PRO_WEP_KX_STR,	  NULL,				            DEV_COMMAND_KEY_CLEAR_WEP,   DEV_TERM_PRO_WEP_KX_STR },	// Clear the current key. 
	{ DEV_TERM_PRO_WEP_KX_STR,	  NULL,			            	DEV_COMMAND_WRITE,           DEV_TERM_PRO_WEP_KX_STR },	// Save the config to permanent memory.
	{ DEV_TERM_PRO_WEP_KX_STR,	  NULL,    					    DEV_COMMAND_EXIT,            DEV_TERM_PRO_WEP_STR },	// Exit the Key 4 menu. 
	// 6/17/2015 mpd : END OF FRAGILE BLOCK.

	{ DEV_TERM_PRO_WEP_STR,		  NULL,							DEV_COMMAND_WEP_AUTH,        DEV_TERM_PRO_WEP_STR },	// Set authentication.
	{ DEV_TERM_PRO_WEP_STR,		  wen_write_progress_sec_25,    DEV_COMMAND_KEY_SIZE,        DEV_TERM_PRO_WEP_STR },	// Set the key size. 
	{ DEV_TERM_PRO_WEP_STR,		  NULL, 					  	DEV_COMMAND_WRITE,           DEV_TERM_PRO_WEP_STR },	// Save the config to permanent memory.
																						   
	{ DEV_TERM_PRO_WEP_STR,		  NULL,				            DEV_COMMAND_WEP_KX,          DEV_TERM_PRO_WEP_KX_STR },	// Go to the KEY X menu. 
	{ DEV_TERM_PRO_WEP_KX_STR,	  NULL,				            DEV_COMMAND_KEY_SET_WEP,     DEV_TERM_PRO_WEP_KX_STR },	// Set the key. 
	{ DEV_TERM_PRO_WEP_KX_STR,	  NULL,			            	DEV_COMMAND_WRITE,           DEV_TERM_PRO_WEP_KX_STR },	// Save the config to permanent memory.
	{ DEV_TERM_PRO_WEP_KX_STR,	  NULL,    					    DEV_COMMAND_EXIT,            DEV_TERM_PRO_WEP_STR },	// Exit the Key X menu. 
	{ DEV_TERM_PRO_WEP_STR,	 	  NULL,				            DEV_COMMAND_EXIT,            DEV_TERM_PRO_SEC_STR },	// Exit the WEP menu. 
	// 6/15/2015 mpd : END NEW ORDER.                                                        
																							 
	{ DEV_TERM_PRO_SEC_STR,		  NULL,				            DEV_COMMAND_WPAX,            DEV_TERM_PRO_WPAX_STR },	// Go to the WPAX menu. 
	{ DEV_TERM_PRO_WPAX_STR,	  wen_write_progress_sec_50,    DEV_COMMAND_WPA_AUTH,        DEV_TERM_PRO_WPAX_STR },	// Set the authentication. 
	{ DEV_TERM_PRO_WPAX_STR,	  NULL,				            DEV_COMMAND_KEY_SET_WPA,     DEV_TERM_PRO_WPAX_STR },	// Set the key. 
	{ DEV_TERM_PRO_WPAX_STR,	  NULL,				            DEV_COMMAND_EAP_TTLS,        DEV_TERM_PRO_WPAX_STR },	// Set the EAP TTLS Option. 
	{ DEV_TERM_PRO_WPAX_STR,	  NULL,				            DEV_COMMAND_IEEE,            DEV_TERM_PRO_WPAX_STR },	// Set IEEE. 
	{ DEV_TERM_PRO_WPAX_STR,	  NULL,						    DEV_COMMAND_PEAP,            DEV_TERM_PRO_WPAX_STR },	// Set the PEAP Option. 
	{ DEV_TERM_PRO_WPAX_STR,	  NULL,				            DEV_COMMAND_ETLS_V,          DEV_TERM_PRO_WPAX_STR },	// Set EAP TLS Validate Certificate. 
	{ DEV_TERM_PRO_WPAX_STR,	  NULL,				            DEV_COMMAND_E_CCMP,          DEV_TERM_PRO_WPAX_STR },	// Set CCMP Encriyption. 
	{ DEV_TERM_PRO_WPAX_STR,	  NULL,				            DEV_COMMAND_E_TKIP,          DEV_TERM_PRO_WPAX_STR },	// Set TKIP Encriyption. 
	{ DEV_TERM_PRO_WPAX_STR,	  NULL,				            DEV_COMMAND_E_WEP,           DEV_TERM_PRO_WPAX_STR },	// Set WEP Encriyption. 
	{ DEV_TERM_PRO_WPAX_STR,	  NULL,				            DEV_COMMAND_ETLS_C,          DEV_TERM_PRO_WPAX_STR },	// Set EAP TLS Credentials. 
	{ DEV_TERM_PRO_WPAX_STR,	  wen_write_progress_sec_75,    DEV_COMMAND_PWORD,           DEV_TERM_PRO_WPAX_STR },	// Set the password. 
	{ DEV_TERM_PRO_WPAX_STR,	  NULL,				            DEV_COMMAND_UNAME,           DEV_TERM_PRO_WPAX_STR },	// Set the user name. 
	{ DEV_TERM_PRO_WPAX_STR,	  NULL,			            	DEV_COMMAND_WRITE,           DEV_TERM_PRO_WPAX_STR },	// Save the config to permanent memory.
																							 
	{ DEV_TERM_PRO_WPAX_STR,	  NULL,				            DEV_COMMAND_EXIT,            DEV_TERM_PRO_SEC_STR },	// Exit the WPAX menu. 
	{ DEV_TERM_PRO_SEC_STR,		  NULL,				            DEV_COMMAND_EXIT,            DEV_TERM_PRO_STR },		// Exit the SECURITY menu. 
	{ DEV_TERM_PRO_STR,		  	  NULL,				            DEV_COMMAND_EXIT,            DEV_TERM_CONFIG_STR },		// Exit the WLAN Profiles menu. 
	{ DEV_TERM_CONFIG_STR,		  NULL,				            DEV_COMMAND_EXIT,            DEV_TERM_ENABLE_STR },		// Exit CONFIGURE. 
	{ DEV_TERM_ENABLE_STR,		  NULL,							DEV_COMMAND_EXIT_FINAL,      DEV_TERM_NO_RESP_STR },	// Exit ENABLE. There is no response to the final exit. 
	{ DEV_NO_TITLE_STR,			  dev_cli_disconnect,			DEV_COMMAND_END, 	         DEV_TERM_NO_RESP_STR }		// Drop DTR to exit CLI. 

	// 5/14/2015 mpd : There needs to be a reboot or power cycle after this WRITE. Do it here only if the 
	// subsequent READ no longer does it.
};

// 4/8/2015 mpd : Determine the size of the lists. These will be used later to determine if we successfully reached 
// the end.
#define WEN_READ_LIST_LEN	 			( sizeof( wen_read_list )	 		  / sizeof( DEV_MENU_ITEM ) )
#define WEN_WRITE_LIST_LEN	    		( sizeof( wen_write_list ) 			  / sizeof( DEV_MENU_ITEM ) )
#define WEN_DIAGNOSTIC_WRITE_LIST_LEN	( sizeof( wen_diagnostic_write_list ) / sizeof( DEV_MENU_ITEM ) )

extern UNS_32 wen_sizeof_read_list()
{
	return( WEN_READ_LIST_LEN ); 
}

extern UNS_32 wen_sizeof_write_list()
{
	return( WEN_WRITE_LIST_LEN ); 
}

extern UNS_32 wen_sizeof_diagnostic_write_list()
{
	return( WEN_DIAGNOSTIC_WRITE_LIST_LEN ); 
}

/* ---------------------------------------------------------- */
/*  			    Structure Initialization				  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void wen_initialize_profile_vars( DEV_STATE_STRUCT *dev_state )
{
	//Alert_Message( "wen_initialize_profile_vars" );

	// 5/29/2015 mpd : These are in the order displayed in the "xcr dump profile" response.
	strlcpy( wen_details.ssid,                        "", sizeof( wen_details.ssid ) );                
	strlcpy( wen_details.radio_mode,                  "", sizeof( wen_details.radio_mode ) );                
	strlcpy( wen_details.key,                         "", sizeof( wen_details.key ) );                
	strlcpy( wen_details.security,                    "", sizeof( wen_details.security ) );
	strlcpy( wen_details.w_key_type,                  "", sizeof( wen_details.w_key_type ) );
	strlcpy( wen_details.passphrase,                  "", sizeof( wen_details.passphrase ) );                
	strlcpy( wen_details.wep_authentication,          "", sizeof( wen_details.wep_authentication ) );
	strlcpy( wen_details.wep_key_size,                "", sizeof( wen_details.wep_key_size ) );
	strlcpy( wen_details.wpa_authentication,          "", sizeof( wen_details.wpa_authentication ) );
	strlcpy( wen_details.wpa_ieee_802_1x,             "", sizeof( wen_details.wpa_ieee_802_1x ) );
	strlcpy( wen_details.wpa_eap_ttls_option,         "", sizeof( wen_details.wpa_eap_ttls_option ) );
	strlcpy( wen_details.wpa_peap_option,             "", sizeof( wen_details.wpa_peap_option ) );
	strlcpy( wen_details.user_name,                   "", sizeof( wen_details.user_name ) );                
	strlcpy( wen_details.password,                    "", sizeof( wen_details.password ) );                
	// wen_details.wpa_encription_ccmp                
	// wen_details.wpa_encription_tkip                
	// wen_details.wpa_encription_wep                 
	// wen_details.wpa_eap_tls_valid_cert
	strlcpy( wen_details.wpa_eap_tls_credentials,     "", sizeof( wen_details.wpa_eap_tls_credentials ) );                

	wen_details.wpa_encription_ccmp    = false;                
	wen_details.wpa_encription_tkip    = false;                
	wen_details.wpa_encription_wep     = false;                
	wen_details.wpa_eap_tls_valid_cert = false;

}

/* ---------------------------------------------------------- */
extern void wen_initialize_detail_struct( DEV_STATE_STRUCT *dev_state )
{
	//Alert_Message( "wen_initialize_detail_struct" );

	// 5/27/2015 mpd : Initialize pointers to the device and WEN specific detail structures.
	dev_state->dev_details = &dev_details;
	dev_state->wen_details = &wen_details;

	// 4/21/2015 mpd : Clear all the string values.
    strlcpy( wen_details.mac_address,  "", sizeof( wen_details.mac_address ) );                
	strlcpy( wen_details.status,       "", sizeof( wen_details.status ) );                
	strlcpy( wen_details.dhcp_name,    "", sizeof( wen_details.dhcp_name ) );                

	// 6/22/2015 mpd : Determine if the DHCP name has ever been known. This flag prevents us from stomping on an 
	// existing name with the MAC address (the suggested default name).
	wen_details.dhcp_name_not_set = true;

	strlcpy( wen_details.ip_address,   "", sizeof( wen_details.ip_address ) );                
	strlcpy( wen_details.ip_address_1, "", sizeof( wen_details.ip_address_1 ) );                
	strlcpy( wen_details.ip_address_2, "", sizeof( wen_details.ip_address_2 ) );                
	strlcpy( wen_details.ip_address_3, "", sizeof( wen_details.ip_address_3 ) );                
	strlcpy( wen_details.ip_address_4, "", sizeof( wen_details.ip_address_4 ) );                

	// 9/24/2015 mpd : FogBugz #3178: Neither the Gui side or the device side code cares about the mask string.
	// It's the mask bits that count. Remove this field. 
	//strlcpy( wen_details.mask,         "", sizeof( wen_details.mask ) );                
	//strlcpy( wen_details.mask_1,       "", sizeof( wen_details.mask_1 ) );                
	//strlcpy( wen_details.mask_2,       "", sizeof( wen_details.mask_2 ) );                
	//strlcpy( wen_details.mask_3,       "", sizeof( wen_details.mask_3 ) );                
	//strlcpy( wen_details.mask_4,       "", sizeof( wen_details.mask_4 ) );                

	strlcpy( wen_details.gw_address,   "", sizeof( wen_details.gw_address ) );                
	strlcpy( wen_details.gw_address_1, "", sizeof( wen_details.gw_address_1 ) );                
	strlcpy( wen_details.gw_address_2, "", sizeof( wen_details.gw_address_2 ) );                
	strlcpy( wen_details.gw_address_3, "", sizeof( wen_details.gw_address_3 ) );                
	strlcpy( wen_details.gw_address_4, "", sizeof( wen_details.gw_address_4 ) );                

	wen_initialize_profile_vars( dev_state );

	wen_details.mask_bits = XE_XN_DEVICE_NETMASK_DEFAULT;
	wen_details.rssi = 0;

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

