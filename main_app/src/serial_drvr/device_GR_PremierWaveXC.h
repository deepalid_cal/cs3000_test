/*  file = device_GR_PremierWaveXC.h          08.05.2014  ajv */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_DEVICE_GR_PREMIERWAVECX_H
#define INC_DEVICE_GR_PREMIERWAVECX_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"device_common.h"	// Needed for extraction functions.

// 4/8/2015 mpd : GR State Machine States.
#define GR_WAITING_FOR_RESPONSE				(3000)	
#define GR_DEVEX_READ						(4000)
#define GR_DEVEX_WRITE						(5000)
#define GR_DEVEX_FINISH						(6000)
#define GR_WAITING_FOR_NETWORK				(7000)	

// 4/23/2015 mpd : The initial large response from the GR PremierWave is 1324 bytes. At 9600 baud, that's 1.38 seconds 
// if the GR streamed like an EN. It does not. There are significant hesitations between sections of the response. Some
// WRITE steps are pushing 10 seconds before the response.  
#define GR_COMM_TIMER_MS					( 15000 )

// 5/14/2015 mpd : The GR has some commands that will not respond (e.g. baud rate change). Set a short timer for a 
// quicker timeout.
#define GR_NO_RESP_TIMER_MS					( 100 )

// 5/5/2015 mpd : We need to auto-repeat the "!" key after a power cycle until the GR responds. Microsoft does not seem
// to publish the default auto-repeat value, and the control is a slider bar with no scale. Let's guess at a reasonable
// 8 repeats a second. Set a limit on how many times we will loop through this cycle before giving up. It takes 12 - 13 
// seconds before the GR responds to the "!".
#define GR_AUTO_REPEAT_TIMER_MS				( 125 )

// 5/7/2015 mpd : The GR is typically responding in 10.8 to 10.9 seconds (86 to 87 counts).
#define GR_MAX_START_LOOP_COUNT				( ( 1000 / GR_AUTO_REPEAT_TIMER_MS ) * 15 )

#define GR_SUBSEQUENT_CMD_TIMER				( 500 )
#define GR_NETWORK_TIMER_MS					( 1000 - GR_SUBSEQUENT_CMD_TIMER )

// 5/7/2015 mpd : The network is typically connecting in 30 seconds. Allow 60 to be sure. DTR (CD) is not functional in
// CLI mode, so there is no feedback on the connection status.
//#define GR_MAX_NETWORK_LOOP_COUNT			( ( 1000 / ( GR_NETWORK_TIMER_MS + GR_SUBSEQUENT_CMD_TIMER ) ) * 60 )
#define GR_NETWORK_LOOP_COUNT				( ( 1000 / ( GR_NETWORK_TIMER_MS + GR_SUBSEQUENT_CMD_TIMER ) ) * 60 )
#define GR_REBOOT_LOOP_COUNT				( 12 )

#define GR_PROGRAMMING_BAUD_RATE ( 9600 )
 
// 3/19/2015 mpd : TODO: Add more errors and use them!
#define GR_ERROR_NONE						( 0 )

#define GR_NULL_TERM_LEN	 				( 1	)

// 3/13/2015 mpd : These are not derived from GR PremierWave responses.
#define GR_EMPTY_STR						( 0xFF ) // Special case for initial I/O cycle. 
#define GR_EMPTY_STR_LEN					( 1 ) 	 // Length > 0 is required. 
#define GR_CRLF_STR							( "\r\n" )
#define GR_CRLF_STR_LEN						( 2 + GR_NULL_TERM_LEN )
#define GR_CR_STR							( "\r" )
#define GR_CR_STR_LEN						( 1 + GR_NULL_TERM_LEN )
#define GR_LF_STR							( "\n" )
#define GR_LF_STR_LEN						( 1 + GR_NULL_TERM_LEN )
#define GR_NULL_IP_ADD_STR				 	( "0.0.0.0" ) 
#define GR_NULL_IP_ADD_LEN				 	( 7 + GR_NULL_TERM_LEN ) 
#define GR_IP_ADD_LEN 					 	( 15 + GR_NULL_TERM_LEN ) 

#define GR_IDLE_OPERATION					( 73 )		// "I"
#define GR_READ_OPERATION					( 82 )		// "R"
#define GR_WRITE_OPERATION					( 87 )		// "W"
#define GR_IDLE_TEXT_STR					( "IDLE" )
#define GR_READ_TEXT_STR					( "READ" )
#define GR_WRITE_TEXT_STR					( "WRITE" )
#define GR_OPERATION_STR_LEN				( 6 )
#define GR_INFO_TEXT_LEN					( 39 + GR_NULL_TERM_LEN ) 	// This should match the size of GuiVar_SRInfoText.  
#define GR_PROGRESS_TEXT_LEN				( 39 + GR_NULL_TERM_LEN ) 	// This should match the size of GuiVar_SRProgressText.  
#define GR_ALERT_MESSAGE_LEN				( 60 + GR_NULL_TERM_LEN )   // 60 is about the limit of the alert screen width. More requires scrolling.  

// 4/21/2015 mpd : These keys are entered after a power cycle put the GR PremierWave into programming mode.
#define GR_ENTER_START_STR			        ( "!" )				// Note: No CR or LF.
#define GR_ENTER_CLI_STR			        ( "xyz" )			// Note: No CR or LF.
#define GR_ENTER_CLI_LEN			        ( 3 + GR_NULL_TERM_LEN )	

// 5/4/2015 mpd : GUI Vars for GR PremierWave.
// GuiVar_CommOptionInfo_Text 
// GuiVar_CommOptionProgress _Text

// 5/6/2015 mpd : The "enable->show" response is 1324 bytes with no SIM card. 
	
//(enable)#show
//Product Information:
//   Product Type        : Lantronix PremierWave XC HSPA+ (premierwave_xc_hspa)			<- Needed
//   FW Version / Date   : 7.9.0.3A2 / Apr 29 11:47:33 PDT 2015							<- Needed
//   Current Date/Time   : Mon Jan  1 00:01:12 UTC 2007
//   Temperature         : 28.0C
//   Serial Number       : 0080A39A5CC5													<- Needed
//   Uptime              : 0 days 00:01:09
//   Perm. Config        : saved
//01234567890123456789012345678901234567890123456789012345678901234567890				<- Ruler
//
//Alerts:
//   Alarm               : eth0 link state change
//   Duration            : 0 days 00:00:43
//
//   Alarm               : wwan0 link state change
//   Duration            : 0 days 00:00:43
//
//Network Status:
//   Primary DNS         : <None>
//   Secondary DNS       : <None>
//
//   Interface           : eth0
//   Link                : Auto 10/100 Mbps Auto Half/Full (No link)
//   MAC Address         : 00:80:a3:9a:5c:c5
//   Hostname            : <None> [<DHCP>]
//   Domain              : <None> [<DHCP>]
//   IP Address          : <None> [<DHCP>]												<- WRONG ADDRESS!!! WE NEED WWAN0!!!
//   Network Mask        : <None> [<DHCP>]
//   Default Gateway     : <None> [<DHCP>]
//   MTU                 : 1500
//

// NO SIM RESPONSE:
//   Interface           : wwan0
//   Packet Domain Status: SIM not inserted
//   IP Address          : <None>														<- Needed

// SIM PRESENT RESPONSE:
//   Interface           : wwan0
//   Packet Domain Status: registered on home network
//   IP Address          : 10.10.3.40/32												<- Needed

//
//Line 1:
//   RS232, 115200, None, 8, 1, Hardware [CLI]
//   Tunnel Connect Mode: Waiting, Accept Mode: Disabled
//Line 2:
//   RS232, 9600, None, 8, 1, None
//   Tunnel Connect Mode: Disabled, Accept Mode: Waiting
//
//VPN Status:
//   Status              : Disabled
//   IP Address          : <None>
 
// 5/4/2015 mpd : GUI Var info from "login->enable->show".
// GuiVar_GRModel
// GuiVar_GRFirmwareVersion
// GuiVar_GRRadioSerialNum
// GuiVar_GRIPAddress

// 5/4/2015 mpd : NOTE: Title and anchor strings have been chosen with as few unique characters as possible to reduce 
// data space. Lengths have also been limited.

//#define GR_E_SHOW_TITLE_STR		    		"Product I"					// "Product Information:"

#define GR_E_SHOW_MODEL_STR			    	"Product T"					// "Product Type        :"
#define GR_E_SHOW_MODEL_OFFSET		    	( 32 )						// Skip "Lantronix ".
#define GR_E_SHOW_MODEL_LEN			    	( 31 + GR_NULL_TERM_LEN )	// This is the value currently in use
#define GR_E_SHOW_MODEL_DELIMITER	    	"("							// "(premierwave_xc_hspa)"

#define GR_E_SHOW_FW_VER_STR			   	"FW V"						// "FW Version / Date   :"
#define GR_E_SHOW_FW_VER_OFFSET		    	( 22 )
#define GR_E_SHOW_FW_VER_LEN			   	( 13 + GR_NULL_TERM_LEN )	// Currently in use plus 1 extra digit in each field.
#define GR_E_SHOW_FW_VER_TOKEN_LEN 		   	( 4 + GR_NULL_TERM_LEN )	// Allow 4 characters per version token. 
#define GR_E_SHOW_FW_VER_DELIMITER		   	"/"							// "/ Apr 29 11:47:33 PDT 2015"
#define GR_E_SHOW_FW_VER_MIN_1			   	( 7 )						// This is the minimum firmware version we require (UNS_32). 
#define GR_E_SHOW_FW_VER_MIN_2			   	( 9 )						// This is the minimum firmware version we require (UNS_32).
#define GR_E_SHOW_FW_VER_MIN_3			   	( 0 )						// This is the minimum firmware version we require (UNS_32).
#define GR_E_SHOW_FW_VER_MIN_4			   	"3A2"						// This is the minimum firmware version we require (string).

#define GR_E_SHOW_SER_NUM_STR			   	"Ser"						// "Serial Number       :"
#define GR_E_SHOW_SER_NUM_OFFSET		   	( 22 )
#define GR_E_SHOW_SER_NUM_LEN			   	( 12 + GR_NULL_TERM_LEN)						

// 4/23/2015 mpd : The GR PremierWave response has duplicate key words for IP Address. Analyze from the "Packet Domain 
// Status:" line to extract the one associated with "wwan0". Without this, we get the "eth0" IP address.
#define GR_E_SHOW_WWAN0_STR		    		"Packet D"					// "Packet Domain Status:"

#define GR_E_SHOW_IP_ADD_STR			   	"IP A"						// "IP Address          :"
#define GR_E_SHOW_IP_ADD_OFFSET		    	( 22 )
#define GR_E_SHOW_IP_ADD_LEN			   	( 15 + GR_NULL_TERM_LEN )		

#define GR_E_SHOW_IP_NOT_SET_STR			"<None>"	 				// "<None> [<DHCP>]"
#define GR_E_SHOW_IP_NOT_SET_LEN			( 6 )	 	 				// Length to compare


// 5/6/2015 mpd : The "enable->configure->cellular->show status" response is 329 bytes with no SIM card. 
	
// NO SIM RESPONSE:
//(config-cellular)#show status
//   SIM status          : connection error												<- Needed
//   IMSI                : SIM not inserted
//   Operator            : SIM not inserted
//   Network Status      : SIM not inserted												<- Needed
//   Packet Domain Status: SIM not inserted												<- Needed
//   Signal Strength     : SIM not inserted												<- Needed
//   Temperature         : 0.0C
//01234567890123456789012345678901234567890123456789012345678901234567890				<- Ruler
 
// SIM PRESENT RESPONSE:
//(config-cellular)#show status
//   SIM status          : present														<- Needed
//   IMSI                : 310410734050811
//   Operator            : AT&T
//   Network Status      : registered on home network									<- Needed
//   Packet Domain Status: registered on home network									<- Needed
//   Signal Strength     : -87 dBm														<- Needed
//   Temperature         : 33.0C
 
// 5/4/2015 mpd : GUI Var info from "login->enable->configure->cellular->show status".
// GuiVar_GRSIMState
// GuiVar_GRNetworkState
// GuiVar_GRGPRSStatus
// GuiVar_GRRSSI

// 5/4/2015 mpd : NOTE: Title and anchor strings have been chosen with as few unique characters as possible to reduce 
// data space. Lengths have also been limited.

// 5/4/2015 mpd : The response title string is the same as the first anchor text. Skip it.
//#define GR_C_SHOW_TITLE_STR		   		"SIM"						// "SIM status          :"

#define GR_C_SHOW_SIM_STATUS_STR		   	"SIM s"						// "SIM status          :"
#define GR_C_SHOW_SIM_STATUS_OFFSET		   	( 22 )
#define GR_C_SHOW_SIM_STATUS_LEN			( 30  + GR_NULL_TERM_LEN )	// The maximum see so far is 26

#define GR_C_SHOW_NET_STATUS_STR		   	"Network S"					// "Network Status      :"
#define GR_C_SHOW_NET_STATUS_OFFSET		   	( 22 )
#define GR_C_SHOW_NET_STATUS_LEN		   	( 30  + GR_NULL_TERM_LEN )	// The maximum see so far is 26

#define GR_C_SHOW_GPRS_STR		    		"Packet D"					// "Packet Domain Status:"
#define GR_C_SHOW_GPRS_OFFSET		    	( 22 )
#define GR_C_SHOW_GPRS_LEN			   		( 30  + GR_NULL_TERM_LEN )	// The maximum see so far is 26

#define GR_C_SHOW_RSSI_STR		    		"Signal S"					// "Signal Strength     :"
#define GR_C_SHOW_RSSI_OFFSET		    	( 22 )
#define GR_C_SHOW_RSSI_LEN			   		( 30  + GR_NULL_TERM_LEN )	// The maximum see so far is 26

#define GR_C_SHOW_CONN_ERR_STR				"connection e" 				// "connection error"
#define GR_C_SHOW_SIM_PRESENT_STR			"present"	 				// "present"
#define GR_C_SHOW_CONN_ERR_LEN				( 12 )	 					// Length to compare

#define GR_C_SHOW_NO_SIM_STR				"SIM not i"	 				// "SIM not inserted"
#define GR_C_SHOW_REGISTERED_STR			"registere"	 				// "registered on home network"
#define GR_C_SHOW_NO_SIM_LEN				( 9 )	 	 				// Length to compare

// 5/4/2015 mpd : GR PremierWave Programming per "http://odin.domain.calsense.com/fogbugz/default.asp?W150".

// 5/4/2015 mpd : These are termination strings (how RCVD_DATA recognizes the end of the expected response). They have
// been chosen with as few unique characters as possible to reduce data space. Keep the strings in alphabetic order to 
// make collisions easier to spot.
#define GR_NO_TITLE_STR						""							// Used for the initial read and write title string.
#define GR_TERM_START_STR					"!"							// "!"			<- Prompt upon power up with "!" input.
#define GR_TERM_PROMPT_STR					">>"						// ">>"			<- Prompt after "xyz" input.
																		
#define GR_TERM_CONN_STR					"-connect:1)#"				// "(tunnel-connect:1)#"
#define GR_TERM_ENABLE_STR					"ble)#"						// "(enable)#"
#define GR_TERM_MODEM_STR					"dem:1)#"					// "(tunnel-modem:1)#"
#define GR_TERM_IF_E_STR					"eth0)#"					// "(config-if:eth0)#"
#define GR_TERM_CONFIG_STR					"fig)#"						// "(config)#"
#define GR_TERM_LINK_STR					"k:wwan0)#"					// "(config-wwan-link:wwan0)#"
#define GR_TERM_CELL_STR					"lar)#"						// "(config-cellular)#"
#define GR_TERM_LOG_STR						"log)#"						// "(config-diagnostics-log)#"
#define GR_TERM_IF_W_STR					"n:wwan0)#"					// "(config-wwan:wwan0)#"
#define GR_TERM_TUNNEL_STR					"nel:1)#"					// "(tunnel:1)#"
#define GR_TERM_LINE_STR					"ne:1)#"					// "(line:1)#"
#define GR_TERM_CLOCK_STR					"ock)#"						// "(config-clock)#"
#define GR_TERM_REBOOT_STR					"ot ..."					// "waiting for reboot ..."
#define GR_TERM_ACCEPT_STR					"pt:1)#"					// "(tunnel-accept:1)#"
#define GR_TERM_RELOAD_STR					"s/no)?"					// "Are you sure (yes/no)?".
#define GR_TERM_DISCONN_STR					"sconnect:1)#"				// "(tunnel-disconnect:1)#"
#define GR_TERM_HOST_STR					"st:1:1)#"					// "(tunnel-connect-host:1:1)#"
#define GR_TERM_DIAG_STR					"tics)#"					// "(config-diagnostics)#"
#define GR_TERM_XML_STR						"xml)#"						// "(xml)#"
//#define GR_TERM_EXIT_STR					""							// There is no response to the exit command.
#define GR_TERM_NO_RESP_STR					""							// No response is expected.

// DEBUG CODE:
#define GR_TERM_LINE_2_STR					"ne:2)#"					// "(line:2)#"

// COMMAND: login->enable->configure->cellular->antenna diversity disable
//(config-cellular)#antenna diversity disable
//   WARNING: SIM security features are not available.


// COMMAND: login->enable->configure->clock->synchronization method network
//(config-clock)#synchronization method network
//   NOTE: Date and Time are updated when network time is available.


// COMMAND: login->enable->configure->diagnostics->log
// COMMAND: login->enable->configure->diagnostics->log->output filesystem
// COMMAND: login->enable->configure->diagnostics->log->max length 1000
//(config)#diagnostics
//(config-diagnostics)#?
//clrscrn                                 exit
//log                                     show
//show history                            write
//
//(config-diagnostics)#log
//(config-diagnostics-log)#?
//clrscrn                                 default max length
//default output                          exit
//max length <Kbytes>                     output disable
//output filesystem                       output line <number>
//show                                    show history
//write
//
//(config-diagnostics-log)#output filesystem
//(config-diagnostics-log)#max length 1000

// COMMAND: login->enable->line 1->baud rate 115200						<- This command hangs the interface
// COMMAND: login->enable->line 1->flow control hardware				<- This command hangs the interface

// COMMAND: login->enable->configure->if eth0->priority 2
// COMMAND: login->enable->configure->if wwan0->link->apn 10429.mcs
//(config-wwan-link:wwan0)#apn 10429.mcs
//   Change in  operational State will take effect on the next reboot after a write command.

// COMMAND: login->enable->tunnel 1->accept->accept mode disable
// COMMAND: login->enable->tunnel 1->connect->connect mode always
//(tunnel-connect:1)#connect mode always
//   WARNING: The port is currently in Command Mode.
//(tunnel-connect:1)#

// COMMAND: login->enable->tunnel 1->connect->host 1
// COMMAND: login->enable->tunnel 1->connect->host 1->address 64.73.242.99
//(tunnel-connect-host:1:1)#address 64.73.242.99
//   WARNING: The port is currently in Command Mode.
//(tunnel-connect-host:1:1)#

// COMMAND: login->enable->tunnel 1->connect->host 1->port 16001
//(tunnel-connect-host:1:1)#port 16001
//   WARNING: The port is currently in Command Mode.
//(tunnel-connect-host:1:1)#

// COMMAND: login->enable->tunnel 1->connect->host 1->tcp keep alive 1200000
//(tunnel-connect-host:1:1)#tcp keep alive 1200000
//   WARNING: The port is currently in Command Mode.
//(tunnel-connect-host:1:1)#

// COMMAND: login->enable->tunnel 1->disconnect->modem control enable
// COMMAND: login->enable->tunnel 1->modem->echo commands disable
//(tunnel-modem:1)#echo commands disable
//   WARNING: Tunnel Connect Mode is not "Modem Emulation".
//(tunnel-modem:1)#

// COMMAND: login->enable->tunnel 1->modem->verbose response disable
//(tunnel-modem:1)#verbose response disable
//   WARNING: Tunnel Connect Mode is not "Modem Emulation".
//(tunnel-modem:1)#

// COMMAND: login->enable->reload
//(enable)#reload
//Are you sure (yes/no)? yes
//
//Rebooting...
//waiting for reboot ...

// 5/7/2015 mpd : APN is a critical field used to validate a Calsense WRITE operation.
// COMMAND: login->enable->configure->if wwan0->link->show
//(config-wwan-link:wwan0)#show
//Cellular Link wwan0 Configuration:
//   APN          : 10429.mcs
//   Username     :
//   Password     : (Not configured)
//   Dialup String: *99#
//   Roaming      : Enabled
#define GR_L_SHOW_APN_STR			    	"APN "					// "APN          :"
#define GR_L_SHOW_APN_OFFSET		    	( 15 )					
#define GR_L_SHOW_APN_LEN			    	( 9 + GR_NULL_TERM_LEN )	// This is the value currently in use

//(xml)#xcr dump device
//<?xml version="1.0" standalone="yes"?>
//<!-- Automatically generated XML -->
//<!DOCTYPE configrecord [
//   <!ELEMENT configrecord (configgroup+)>
//   <!ELEMENT configgroup (configitem+,configgroup*)>
//   <!ELEMENT configitem (value+)>
//   <!ELEMENT value (#PCDATA)>
//   <!ATTLIST configrecord version CDATA #IMPLIED>
//   <!ATTLIST configgroup name CDATA #IMPLIED>
//   <!ATTLIST configgroup instance CDATA #IMPLIED>
//   <!ATTLIST configitem name CDATA #IMPLIED>
//   <!ATTLIST configitem instance CDATA #IMPLIED>
//   <!ATTLIST value name CDATA #IMPLIED>
//]>
//<configrecord version = "0.1.0.0T0">
//   <configgroup name = "device">
//       <configitem name = "short name">
//           <value>premierwave_xc_hspa</value>
//       </configitem>
//       <configitem name = "long name">
//           <value>Lantronix PremierWave XC HSPA+</value>
//       </configitem>
//       <configitem name = "serial number">
//           <value>0080A39A5CC5</value>
//       </configitem>
//       <configitem name = "firmware version">
//           <value>7.9.0.3A2</value>
//       </configitem>
//   </configgroup>
//</configrecord>
//(xml)#>!

// 5/15/2015 mpd : These are XML termination strings (how RCVD_DATA recognizes the end of the expected response). They 
// have been chosen with as few unique characters as possible to reduce data space. Keep the strings in alphabetic order 
// to make collisions easier to spot.
#define GR_X_DUMP_LNAME_ITEM_STR	   		"long name"					// "<configitem name = "long name">"
#define GR_X_DUMP_S_NUM_ITEM_STR	   		"l number"					// "<configitem name = "serial number">"
#define GR_X_DUMP_FW_VER_ITEM_STR	   		"e version"					// "<configitem name = "firmware version">"

// 5/15/2015 mpd : Skip the "Lantronix" portion of the model string.
#define GR_X_DUMP_LNAME_STR			   		"Lantronix"					// "<value>Lantronix PremierWave XC HSPA+</value>"
#define GR_X_DUMP_LNAME_OFFSET		   		( 10 )

// 5/15/2015 mpd : These are generic for XML parsing.
#define GR_X_DUMP_VALUE_STR			   		"<value>"					// "<value>0080A39A5CC5</value>"
#define GR_X_DUMP_VALUE_OFFSET		   		( 7 )
#define GR_X_DUMP_DELIMITER			   		"<"
#define GR_X_DUMP_DELIMITER_LEN		   		( 1 ) 

// 5/5/2015 mpd : These are all the command strings needed to read and program the GR PremierWave device. CR 
// characters have been included where needed to simplify output to the device. 
#define GR_TEXT_CMD_A_MODE		"accept mode disable\r"	 
#define GR_TEXT_CMD_ACCEPT		"accept\r" 
#define GR_TEXT_CMD_ADDRESS		"address 64.73.242.99\r" 
#define GR_TEXT_CMD_ALIVE		"tcp keep alive 1200000\r" 
#define GR_TEXT_CMD_ANTENNA		"antenna diversity disable\r" 	// <- This command has a 30 second response
#define GR_TEXT_CMD_APN			"apn 10429.mcs\r"
#define GR_TEXT_CMD_BAUD		"baud rate 115200\r" 			// <- This command hangs the interface
#define GR_TEXT_CMD_C_MODE		"connect mode always\r" 
#define GR_TEXT_CMD_CELL		"cellular\r" 
#define GR_TEXT_CMD_CLI			"xyz\r" 
#define GR_TEXT_CMD_CLOCK		"clock\r" 
#define GR_TEXT_CMD_CONFIG		"configure\r" 
#define GR_TEXT_CMD_CONNECT		"connect\r"  
#define GR_TEXT_CMD_D_MODE		"modem control enable\r" 
#define GR_TEXT_CMD_DIAG		"diagnostics\r" 
#define GR_TEXT_CMD_DISCONNECT	"disconnect\r" 
#define GR_TEXT_CMD_ECHO		"echo commands disable\r" 
#define GR_TEXT_CMD_ENABLE		"enable\r"
#define GR_TEXT_CMD_EXIT		"exit\r" 
#define GR_TEXT_CMD_FLOW		"flow control hardware\r"		// <- This command hangs the interface
#define GR_TEXT_CMD_HOST		"host 1\r" 
#define GR_TEXT_CMD_IF_E		"if eth0\r" 
#define GR_TEXT_CMD_IF_W		"if wwan0\r" 
#define GR_TEXT_CMD_LENGTH		"max length 1000\r" 
#define GR_TEXT_CMD_LINE		"line 1\r" 
#define GR_TEXT_CMD_LINK		"link\r" 
#define GR_TEXT_CMD_LOG			"log\r" 
#define GR_TEXT_CMD_LOGIN		"login\r" 
#define GR_TEXT_CMD_MODEM		"modem\r" 
#define GR_TEXT_CMD_OUTPUT		"output filesystem\r"
#define GR_TEXT_CMD_PORT		"port 16001\r" 
#define GR_TEXT_CMD_PRIORITY	"priority 2\r" 
#define GR_TEXT_CMD_RELOAD		"reload\r"
#define GR_TEXT_CMD_SHOW		"show\r" 
#define GR_TEXT_CMD_SHOW_S		"show status\r" 
#define GR_TEXT_CMD_START		"!" 
#define GR_TEXT_CMD_SYNC		"synchronization method network\r" 
#define GR_TEXT_CMD_TUNNEL		"tunnel 1\r"
#define GR_TEXT_CMD_VERBOSE		"verbose response disable\r" 
#define GR_TEXT_CMD_WRITE		"write\r" 
#define GR_TEXT_CMD_X_DUMP_D	"xcr dump device\r" 
#define GR_TEXT_CMD_XML			"xml\r" 
#define GR_TEXT_CMD_YES			"yes\r" 

// DEBUG CODE:
#define GR_TEXT_CMD_LINE_2		"line 2\r" 

#define GR_LONGEST_COMMAND_LEN	( sizeof( GR_TEXT_CMD_SYNC ) + EN_CRLF_STR_LEN ) // This is the longest known command. Update it if a longer one is found.
	 
// 5/5/2015 mpd : Use enumerations beginning at "0". The GR may not need the flexibility of "gr_get_command_text()". 
// Incremental values from "0" will make conversion to a simpler plan much easier.
#define GR_COMMAND_A_MODE	        ( 0 ) 
#define GR_COMMAND_ACCEPT	        ( 1 ) 
#define GR_COMMAND_ADDRESS	        ( 2 ) 
#define GR_COMMAND_ALIVE	        ( 3 ) 
#define GR_COMMAND_ANTENNA	        ( 4 ) 
#define GR_COMMAND_APN		        ( 5 ) 
#define GR_COMMAND_BAUD		        ( 6 ) 
#define GR_COMMAND_C_MODE	        ( 7 ) 
#define GR_COMMAND_CELL		        ( 8 ) 
#define GR_COMMAND_CLI			    ( 9 ) 
#define GR_COMMAND_CLOCK	        ( 10 )
#define GR_COMMAND_CONFIG	        ( 11 )
#define GR_COMMAND_CONNECT	        ( 12 )
#define GR_COMMAND_CR		        ( 13 )
#define GR_COMMAND_D_MODE	        ( 14 )
#define GR_COMMAND_DIAG		        ( 15 )
#define GR_COMMAND_DISCONNECT       ( 16 )
#define GR_COMMAND_ECHO		        ( 17 )
#define GR_COMMAND_ENABLE	        ( 18 )
#define GR_COMMAND_END			    ( 19 )	// Special case. There is no GR_TEXT_CMD_END.
#define GR_COMMAND_EXIT		        ( 20 )
#define GR_COMMAND_EXIT_FINAL       ( 21 )
#define GR_COMMAND_FLOW		        ( 22 )
#define GR_COMMAND_HOST		        ( 23 )
#define GR_COMMAND_IF_E		        ( 24 )
#define GR_COMMAND_IF_W		        ( 25 )
#define GR_COMMAND_LENGTH	        ( 26 )
#define GR_COMMAND_LINE		        ( 27 )
#define GR_COMMAND_LINK		        ( 28 )
#define GR_COMMAND_LOG		        ( 29 )
#define GR_COMMAND_LOGIN	        ( 30 )
#define GR_COMMAND_MODEM	        ( 31 )
#define GR_COMMAND_NONE			    ( 32 )	// Special case. There is no GR_TEXT_CMD_NONE.
#define GR_COMMAND_OUTPUT	        ( 33 )
#define GR_COMMAND_PORT		        ( 34 )
#define GR_COMMAND_PRIORITY	        ( 35 )
#define GR_COMMAND_RELOAD	        ( 36 )
#define GR_COMMAND_SHOW		        ( 37 )
#define GR_COMMAND_SHOW_S	        ( 38 )
#define GR_COMMAND_START		    ( 39 )
#define GR_COMMAND_SYNC		        ( 40 )
#define GR_COMMAND_TUNNEL	        ( 41 )
#define GR_COMMAND_VERBOSE	        ( 42 )
#define GR_COMMAND_WRITE	        ( 43 )
#define GR_COMMAND_WRITE_FINAL      ( 44 )
#define GR_COMMAND_X_DUMP_D			( 45 ) 
#define GR_COMMAND_XML				( 46 ) 
#define GR_COMMAND_YES		        ( 47 )

// DEBUG CODE:
#define GR_COMMAND_LINE_2	        ( 48 )

#if 0
typedef struct {

	UNS_16 error_code;
	UNS_32 operation;
	char operation_text[ GR_OPERATION_STR_LEN ];                
	char info_text[ GR_INFO_TEXT_LEN ];                
	char progress_text[ GR_INFO_TEXT_LEN ];                
	UNS_32 read_list_index;
	UNS_32 write_list_index;
	BOOL_32 acceptable_version; 
	BOOL_32 device_settings_are_valid;
	BOOL_32 programming_successful;
	BOOL_32 command_will_respond;

	// 5/6/2015 mpd : WRITE operations are followed by a READ to verify the values were successfully set. The final info 
	// message after a READ or WRITE command needs to reflect what was requested, not the status of the final READ 
	// operation. Use this flag to get it right.
	BOOL_32 read_after_a_write;

	// 3/23/2015 mpd : Centralize the response buffer pointer to make "mem_free()" easier.
	char  *resp_ptr;
	UNS_32 resp_len;

	// 5/15/2015 mpd : ENABLE SHOW is not useful until the network connects. XML XCR DUMP DEVICE is available in CLI
	// immediately. That will be used for all but the IP address going forward. The XML "<" delimiter immediately 
	// following the item value pointed out a hole in "en_extract_delimited_text_from_buffer()". The delimiter gets
	// overwritten by the null terminator if there is not room for both at the end of the destination string. Add room 
	// to compensate until the extraction can be fixed and thoroughly tested on all device types.
	// 4/20/2015 mpd : These values are pulled from the ENABLE SHOW response. 
	char model[ GR_E_SHOW_MODEL_LEN + GR_X_DUMP_DELIMITER_LEN ];  
	char firmware_version[ GR_E_SHOW_FW_VER_LEN + GR_X_DUMP_DELIMITER_LEN ];  
	char serial_number[ GR_E_SHOW_SER_NUM_LEN + GR_X_DUMP_DELIMITER_LEN ];  
	// 5/15/2015 mpd : TODO: There must be a better place to get this than the large ENABLE SHOW response. 
	char ip_address[ GR_E_SHOW_IP_ADD_LEN ];  

	// 4/20/2015 mpd : These values are pulled from the CELLULAR SHOW STATUS response. 
	char sim_status[ GR_C_SHOW_SIM_STATUS_LEN ];  
	char network_status[ GR_C_SHOW_NET_STATUS_LEN ];  
	char packet_domain_status[ GR_C_SHOW_GPRS_LEN ];  
	char signal_strength[ GR_C_SHOW_RSSI_LEN ];  
	
	// 5/7/2015 mpd : This value is pulled from the LINK SHOW response.
	char apn[ GR_L_SHOW_APN_LEN ];  

	// 5/1/2015 mpd : This structure is currently ??? bytes.

} GR_STATE_STRUCT;
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern const DEV_MENU_ITEM gr_xml_read_list[]; 

extern const DEV_MENU_ITEM gr_xml_write_list[]; 

extern UNS_32 gr_sizeof_read_list();

extern UNS_32 gr_sizeof_write_list();

extern void gr_initialize_detail_struct( DEV_STATE_STRUCT *dev_state );




extern void PWXC_initialize_the_connection_process( UNS_32 pport );

extern void PWXC_connection_processing( UNS_32 pevent );




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

