/*  file = device_LR_FREEWAVE.c                           ajv */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/9/2015 mpd : Required for atoi
#include	<stdlib.h>

#include	<string.h>

#include	"device_GENERIC_FREEWAVE_card.h"

#include	"device_LR_FREEWAVE.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"cal_math.h"

#include	"comm_mngr.h"

#include	"configuration_controller.h"

#include	"controller_initiated.h"

#include	"cs_mem.h"

#include	"d_device_exchange.h"

#include	"device_common.h"

#include	"e_lr_programming_freewave.h"

#include	"rcvd_data.h"

#include	"serport_drvr.h"

#include 	"flash_storage.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	FREEWAVE_LR_STATE_wait_after_a_powerdown		(1)

#define	FREEWAVE_LR_STATE_waiting_for_connection		(2)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static LR_STATE_STRUCT state_struct;

static LR_STATE_STRUCT *LR_state = &state_struct;

// ----------

static LR_DETAILS_STRUCT_LRS lrs_struct;

// ----------

BOOL_32	LR_PROGRAMMING_querying_device;

// ----------

typedef struct
{
	// 2/8/2017 rmd : Precaution - using underscore so the 'state' variable is not confused with
	// the cics.state variable!
	UNS_32  _state;

	UNS_32  port;

	UNS_32  wait_count;

	UNS_32  connection_start_time;

} FREEWAVE_LR_control_structure;

static FREEWAVE_LR_control_structure    LR_cs;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void LR_FREEWAVE_post_device_query_to_queue( const UNS_32 pport, const UNS_32 poperation )
{
	COMM_MNGR_TASK_QUEUE_STRUCT	cmqs;

	// ----------

	LR_PROGRAMMING_querying_device = (true);

	// 3/18/2014 rmd : Post the event to cause the comm_mngr to perform the device settings
	// readout and write the results to each of the guivars.

	if( poperation == LR_WRITE )
	{
		cmqs.event = COMM_MNGR_EVENT_request_write_device_settings;
	}
	else
	{
		cmqs.event = COMM_MNGR_EVENT_request_read_device_settings;
	}

	cmqs.port = pport;

	COMM_MNGR_post_event_with_details( &cmqs );
}
	
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  			 String Extraction and Analysis 			  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void LR_analyze_main_menu( LR_STATE_STRUCT *LR_state )
{
	if( LR_state->operation == LR_READ_OPERATION )
	{
		strlcpy( GuiVar_CommOptionInfoText, "Reading device settings...", sizeof(GuiVar_CommOptionInfoText) );
	}
	else
	{
		strlcpy( GuiVar_CommOptionInfoText, "Saving device settings...", sizeof(GuiVar_CommOptionInfoText) );
	}

	dev_extract_text_from_buffer( LR_state->resp_ptr, LR_MAIN_MENU_VERSION_STR, LR_MAIN_MENU_VERSION_OFFSET, LR_MAIN_MENU_VERSION_LEN, LR_state->sw_version,    "VERSION" );

	dev_extract_text_from_buffer( LR_state->resp_ptr, LR_MAIN_MENU_SERIAL_STR,  LR_MAIN_MENU_SERIAL_OFFSET,  LR_MAIN_MENU_SERIAL_LEN,  LR_state->serial_number, "SN" );

	dev_extract_text_from_buffer( LR_state->resp_ptr, LR_MAIN_MENU_MODEL_STR,   LR_MAIN_MENU_MODEL_OFFSET,   LR_MAIN_MENU_MODEL_LEN,   LR_state->model,         "MODEL" );
}

/* ---------------------------------------------------------- */
static void LR_analyze_set_modem_mode( LR_STATE_STRUCT *LR_state )
{
	dev_extract_text_from_buffer( LR_state->resp_ptr, LR_MODE_MENU_MODE_STR, LR_MODE_MENU_MODE_OFFSET, LR_MODE_MENU_MODE_LEN, LR_state->lrs_struct->modem_mode, "MODE" );
}

/* ---------------------------------------------------------- */
static void LR_analyze_set_baud_rate( LR_STATE_STRUCT *LR_state )
{
	dev_extract_text_from_buffer( LR_state->resp_ptr, LR_BAUD_MENU_RATE_STR, LR_BAUD_MENU_RATE_OFFSET, LR_BAUD_MENU_RATE_LEN, LR_state->lrs_struct->baud_rate, "BAUD" );
}

/* ---------------------------------------------------------- */
static void LR_analyze_radio_parameters( LR_STATE_STRUCT *LR_state )
{
	dev_extract_text_from_buffer( LR_state->resp_ptr, LR_RADIO_MENU_MAX_PACKET_STR,    LR_RADIO_MENU_MAX_PACKET_OFFSET,    LR_RADIO_MENU_MAX_PACKET_LEN,    LR_state->lrs_struct->max_packet_size, "MAXP" );

	dev_extract_text_from_buffer( LR_state->resp_ptr, LR_RADIO_MENU_MIN_PACKET_STR,    LR_RADIO_MENU_MIN_PACKET_OFFSET,    LR_RADIO_MENU_MIN_PACKET_LEN,    LR_state->lrs_struct->min_packet_size, "MINP" );

	dev_extract_text_from_buffer( LR_state->resp_ptr, LR_RADIO_MENU_XMIT_RATE_STR,     LR_RADIO_MENU_XMIT_RATE_OFFSET,     LR_RADIO_MENU_XMIT_RATE_LEN,     LR_state->lrs_struct->transmit_rate,     "XRATE" );

	dev_extract_text_from_buffer( LR_state->resp_ptr, LR_RADIO_MENU_XMT_POWER_STR,     LR_RADIO_MENU_XMT_POWER_OFFSET,     LR_RADIO_MENU_XMT_POWER_LEN,     LR_state->lrs_struct->rf_xmit_power,   "XPOWER" );
}

/* ---------------------------------------------------------- */
static void LR_analyze_frequency( LR_STATE_STRUCT *LR_state )
{
	char	xmit_freq[ 6 ];

	char	recv_freq[ 6 ];

	dev_extract_text_from_buffer( LR_state->resp_ptr, "  0 ", (4), (6), xmit_freq, "XMTFREQ" );

	dev_extract_text_from_buffer( LR_state->resp_ptr, "  0 ", (11), (6), recv_freq,  "RCVFREQ" );

	snprintf( LR_state->lrs_struct->xmit_freq, sizeof(LR_state->lrs_struct->xmit_freq), "%0.04f", (435 + (atof(xmit_freq) * 0.00625F)) );

	snprintf( LR_state->lrs_struct->recv_freq, sizeof(LR_state->lrs_struct->recv_freq), "%0.04f", (435 + (atof(recv_freq) * 0.00625F)) );
}

/* ---------------------------------------------------------- */
static void LR_analyze_multipoint_parameters( LR_STATE_STRUCT *LR_state )
{
	dev_extract_text_from_buffer( LR_state->resp_ptr, LR_MULTIPOINT_MENU_NUM_REPEATERS_STR,        LR_MULTIPOINT_MENU_NUM_REPEATERS_OFFSET,        LR_MULTIPOINT_MENU_NUM_REPEATERS_LEN,        LR_state->lrs_struct->number_of_repeaters,  "NREP" );

	dev_extract_text_from_buffer( LR_state->resp_ptr, LR_MULTIPOINT_MENU_NETWORK_ID_STR,           LR_MULTIPOINT_MENU_NETWORK_ID_OFFSET,           LR_MULTIPOINT_MENU_NETWORK_ID_LEN,           LR_state->lrs_struct->network_id,           "NETID" );

	dev_extract_text_from_buffer( LR_state->resp_ptr, LR_MULTIPOINT_MENU_SUBNET_ID_STR,            LR_MULTIPOINT_MENU_SUBNET_ID_OFFSET,            LR_MULTIPOINT_MENU_SUBNET_ID_LEN,            LR_state->lrs_struct->subnet_id,            "SNID" );

	// 3/12/2015 mpd : Look for "Roaming" or "Disabled". If not there, look for "Rcv=" string.
	if( ( strncmp( LR_state->lrs_struct->subnet_id, LR_MULTIPOINT_MENU_SUBNET_ROAMING_STR, LR_MULTIPOINT_MENU_SUBNET_ROAMING_LEN ) ) == NULL )
	{
		strlcpy( LR_state->lrs_struct->subnet_rcv_id, "0", sizeof( LR_state->lrs_struct->subnet_rcv_id ) );
		strlcpy( LR_state->lrs_struct->subnet_xmt_id, "0", sizeof( LR_state->lrs_struct->subnet_xmt_id ) );
	}
	else if( ( strncmp( LR_state->lrs_struct->subnet_id, LR_MULTIPOINT_MENU_SUBNET_DISABLED_STR, LR_MULTIPOINT_MENU_SUBNET_DISABLED_LEN ) ) == NULL )
	{
		strlcpy( LR_state->lrs_struct->subnet_rcv_id, "0", sizeof( LR_state->lrs_struct->subnet_rcv_id ) );
		strlcpy( LR_state->lrs_struct->subnet_xmt_id, "F", sizeof( LR_state->lrs_struct->subnet_xmt_id ) );
	}
	else if( ( strncmp( LR_state->lrs_struct->subnet_id, LR_MULTIPOINT_MENU_SUBNET_RCV_ID_STR, LR_MULTIPOINT_MENU_SUBNET_RCV_ID_LEN ) ) == NULL )
	{
		dev_extract_text_from_buffer( LR_state->resp_ptr, LR_MULTIPOINT_MENU_SUBNET_RCV_ID_STR, LR_MULTIPOINT_MENU_SUBNET_RCV_ID_OFFSET, LR_MULTIPOINT_MENU_SUBNET_RCV_ID_LEN, LR_state->lrs_struct->subnet_rcv_id, "RCVID" );// Alternate of ROAMING, requires special handling
		dev_extract_text_from_buffer( LR_state->resp_ptr, LR_MULTIPOINT_MENU_SUBNET_XMT_ID_STR, LR_MULTIPOINT_MENU_SUBNET_XMT_ID_OFFSET, LR_MULTIPOINT_MENU_SUBNET_XMT_ID_LEN, LR_state->lrs_struct->subnet_xmt_id, "XMTID" );// Alternate of ROAMING, requires special handling
	}
	else
	{
		strlcpy( LR_state->lrs_struct->subnet_rcv_id, "0", sizeof( LR_state->lrs_struct->subnet_rcv_id ) );
		strlcpy( LR_state->lrs_struct->subnet_xmt_id, "0", sizeof( LR_state->lrs_struct->subnet_xmt_id ) );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  			  Group, Mode, and Baud Rate Handling   	  */
/* ---------------------------------------------------------- */

// 3/13/2015 mpd : "Group" is a Calsense creation. Each group is a collection of specific
// network related values for LR radio parameters. The group number is visible on the LCD.
// The internal radio parameters associated with the group are not.

// 3/13/2015 mpd : LR radio responses are string values. Stay in the world of strings for
// comparisons and display. Every field is defined as the display length plus a
// null-termination.
typedef struct
{
	char group_id[ LR_NETWORK_GROUP_ID_LEN ]; // Controller only. This field does not get sent to the radio. 

	char max_packet_size[ LR_RADIO_MENU_MAX_PACKET_LEN ];

	char min_packet_size[ LR_RADIO_MENU_MIN_PACKET_LEN ];

	char network_id[ LR_MULTIPOINT_MENU_NETWORK_ID_LEN ];

} LR_group_properties;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define LR_GROUP_DEFAULT	(0)
#define LR_GROUP_LIST_LEN	(6)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

LR_group_properties LR_group_list[ LR_GROUP_LIST_LEN ] =
{
	{ "1",  "8", "4", "0320" },
	{ "2",  "8", "5", "0340" },
	{ "3",  "8", "6", "0420" },
	{ "4",  "8", "7", "0440" },
	{ "5",  "8", "8", "0520" },
	{ "6",  "8", "9", "0540" }
};

/* ---------------------------------------------------------- */
// 3/13/2015 mpd : This function sets a group of LR radio parameters based on the
// "LR_group_list" array. The string values for the group parameters are placed in the pvs
// structure by this function. It does not write those values to the radio.
static void LR_set_network_group_values( LR_STATE_STRUCT *LR_state )
{
	UNS_32 lgroup_index;

	// 4/14/2015 mpd : Set the new group based on the "network_group_id" string. This is the
	// latest value set by the user.
	for( lgroup_index = 0; lgroup_index < LR_GROUP_LIST_LEN; ++lgroup_index )
	{
		// 4/14/2015 mpd : Compare just the frequency to identify the group.
		if( ( strncmp( LR_state->network_group_id, LR_group_list[ lgroup_index ].group_id, LR_NETWORK_GROUP_ID_LEN ) ) == NULL )
		{
			LR_state->group_index = lgroup_index;

			// 3/13/2015 mpd : Set all the LR radio parameters associated with this group.
			strlcpy( LR_state->network_group_id,             LR_group_list[ LR_state->group_index ].group_id,        sizeof( LR_state->network_group_id ) );
			strlcpy( LR_state->lrs_struct->max_packet_size,  LR_group_list[ LR_state->group_index ].max_packet_size, sizeof( LR_state->lrs_struct->max_packet_size ) );
			strlcpy( LR_state->lrs_struct->min_packet_size,  LR_group_list[ LR_state->group_index ].min_packet_size, sizeof( LR_state->lrs_struct->min_packet_size ) );
			strlcpy( LR_state->lrs_struct->network_id,       LR_group_list[ LR_state->group_index ].network_id,      sizeof( LR_state->lrs_struct->network_id ) );

			// 4/27/2017 ajv : We've found a match so no need to continue.
			break;
		}
	}

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  			  Functions That Span All Menu Items		  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void LR_final_radio_analysis( LR_STATE_STRUCT *LR_state )
{
	UNS_32 lgroup_index;

	// ----------

	strlcpy( GuiVar_CommOptionInfoText, "Verifying settings...", sizeof(GuiVar_CommOptionInfoText) );

	// 4/26/2017 ajv : Capture the Group based on the network ID and min/max packet sizes.
	for( lgroup_index = 0; lgroup_index < LR_GROUP_LIST_LEN; ++lgroup_index )
	{
		// 3/13/2015 mpd : Compare just the network ID to identify the group.
		if( ( strncmp( LR_state->lrs_struct->network_id, LR_group_list[ lgroup_index ].network_id, LR_MULTIPOINT_MENU_NETWORK_ID_LEN ) ) == NULL )
		{
			// 3/13/2015 mpd : Now verify the other fields. 
			if( ( ( strncmp( LR_state->lrs_struct->max_packet_size,  LR_group_list[ lgroup_index ].max_packet_size, LR_RADIO_MENU_MAX_PACKET_LEN ) ) == NULL ) &&
				( ( strncmp( LR_state->lrs_struct->min_packet_size,  LR_group_list[ lgroup_index ].min_packet_size, LR_RADIO_MENU_MIN_PACKET_LEN ) ) == NULL ) )
			{
				// 3/13/2015 mpd : Enough fields match. Assign the group ID using the current index.
				strlcpy( LR_state->network_group_id, LR_group_list[ lgroup_index ].group_id, sizeof( LR_state->network_group_id ) );
				LR_state->group_index = lgroup_index;
			}

			// 3/13/2015 mpd : The network ID matched a unique entry. There are no better matches. Exit
			// the loop with or without all fields matching.
			continue;
		}
	}

	// ----------

	// 4/16/2015 mpd : Send progress info to the GUI.
	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );
}

/* ---------------------------------------------------------- */
static void LR_final_radio_verification( LR_STATE_STRUCT *LR_state )
{
	strlcpy( GuiVar_CommOptionInfoText, "Verifying settings...", sizeof(GuiVar_CommOptionInfoText) );

	// 4/16/2015 mpd : Send progress info to the GUI.
	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );
}

/* ---------------------------------------------------------- */
// 4/8/2015 mpd : This function returns a pointer to an ASCII command string that will be
// sent to the LR radio. Some commands are single ASCII characters, others are strings
// terminated with CR and LF. Command strings are selected based on the active programmable
// values structure. This provided a transparent switch between dynamic radio/use command
// strings, and initialization command strings.
static char *get_command_text( LR_STATE_STRUCT *LR_state, const UNS_32 command )
{
	char *command_ptr = NULL;

	BOOL_32 send_just_cr_lf = (false);

	// 4/8/2015 mpd : Create a local space to store and append to command strings. 
	char command_buffer[ ( LR_LONGEST_COMMAND_LEN + LR_CRLF_STR_LEN ) ];

	memset( command_buffer, 0x00, sizeof( command_buffer ) );

	switch( command )
	{
		// 3/17/2015 mpd : These are single-character commands with no CR or LF attached. The
		// defines are the ASCII values of the required character.
		case  LR_MENU_CMD_STR_0:
		case  LR_MENU_CMD_STR_1:
		case  LR_MENU_CMD_STR_2:
		case  LR_MENU_CMD_STR_3:
		case  LR_MENU_CMD_STR_4:
		case  LR_MENU_CMD_STR_5:
		case  LR_MENU_CMD_STR_6:
		case  LR_MENU_CMD_STR_7:
		case  LR_MENU_CMD_STR_8:
		case  LR_MENU_CMD_STR_9:
		case  LR_MENU_CMD_STR_A:
		case  LR_MENU_CMD_STR_B:
		case  LR_MENU_CMD_STR_C:
		case  LR_MENU_CMD_STR_D:
		case  LR_MENU_CMD_STR_E:
		case  LR_MENU_CMD_STR_F:
		case  LR_MENU_CMD_STR_G:
		case  LR_MENU_CMD_STR_ESC:
			memset( command_buffer, command, 1 );
			break;

		case  LR_TEXT_CMD_NONE:
			memset( command_buffer, LR_EMPTY_STR, LR_EMPTY_STR_LEN );
			break;

		case  LR_TEXT_CMD_STR_MODE:
			strlcpy( command_buffer, LR_state->lrs_struct->modem_mode, sizeof( command_buffer ) );
			break;

		case  LR_TEXT_CMD_STR_BAUD_RATE:
			switch( port_device_table[ COMM_DEVICE_LR_FREEWAVE ].baud_rate )
			{
				case 57600:
				default:
					strlcpy( command_buffer, LR_BAUD_RATE_CMD_57600, sizeof( command_buffer ) );
					strlcpy( LR_state->lrs_struct->baud_rate, LR_BAUD_RATE_OUT_57600, sizeof( LR_state->lrs_struct->baud_rate ) );
			}
			break;

		case  LR_TEXT_CMD_STR_FREQ_CHAN:
			strlcpy( command_buffer, "0", sizeof( command_buffer ) );
			break;

		case  LR_TEXT_CMD_STR_NUM_CHAN:
			strlcpy( command_buffer, "1", sizeof( command_buffer ) );
			break;

		case  LR_TEXT_CMD_STR_XMIT_CHAN:
			snprintf( command_buffer, sizeof( command_buffer ), "%d", (UNS_32)(roundf(__round_float(atof(LR_state->lrs_struct->xmit_freq) - 435, 4) / 0.00625F)) );
			break;

		case  LR_TEXT_CMD_STR_RCV_CHAN:
			snprintf( command_buffer, sizeof( command_buffer ), "%d", (UNS_32)(roundf(__round_float(atof(LR_state->lrs_struct->recv_freq) - 435, 4) / 0.00625F)) );
			break;

		case  LR_TEXT_CMD_STR_MAX_PACKET:
			strlcpy( command_buffer, LR_state->lrs_struct->max_packet_size, sizeof( command_buffer ) );
			break;

		case  LR_TEXT_CMD_STR_MIN_PACKET:
			strlcpy( command_buffer, LR_state->lrs_struct->min_packet_size, sizeof( command_buffer ) );
			break;

			// 5/13/2015 mpd : This value should always be "1". Force it. This late-in-development shortcut skips creating
			// a PVS field, extraction from READ responses, and verification after the WRITE. That can be added when time
			// allows for full regression testing.
		case  LR_TEXT_CMD_STR_XMIT_RATE:
			strlcpy( command_buffer, LR_state->lrs_struct->transmit_rate, sizeof( command_buffer ) );
			break;

		case  LR_TEXT_CMD_STR_DATA_RATE:
			strlcpy( command_buffer, "5", sizeof( command_buffer ) );
			break;

		case  LR_TEXT_CMD_STR_XMT_POWER:
			strlcpy( command_buffer, LR_state->lrs_struct->rf_xmit_power, sizeof( command_buffer ) );
			break;

		case  LR_TEXT_CMD_STR_RTS_TO_CTS:
			strlcpy( command_buffer, "0", sizeof( command_buffer ) );
			break;

		case  LR_TEXT_CMD_STR_RETRY_TIMEOUT:
			// 4/27/2017 ajv : The ET2000e set this to 64, however ToolSuite balks at this and indicates
			// it must be between 4 and 32. Therefore, we're setting it to 32.
			strlcpy( command_buffer, "32", sizeof( command_buffer ) );
			break;

		case  LR_TEXT_CMD_STR_LOWPOWER_MODE:
			strlcpy( command_buffer, "0", sizeof( command_buffer ) );
			break;

		case  LR_TEXT_CMD_STR_NUM_REPEATERS:
			strlcpy( command_buffer, LR_state->lrs_struct->number_of_repeaters, sizeof( command_buffer ) );
			break;

		case  LR_TEXT_CMD_STR_MASTER_PACKET_REPEAT:
			strlcpy( command_buffer, "1", sizeof( command_buffer ) );
			break;

		case  LR_TEXT_CMD_STR_MAX_SLAVE_RETRY:
			strlcpy( command_buffer, "8", sizeof( command_buffer ) );
			break;

		case  LR_TEXT_CMD_STR_RETRY_ODDS:
			strlcpy( command_buffer, "0", sizeof( command_buffer ) );
			break;

		case  LR_TEXT_CMD_STR_NETWORK_ID:
			strlcpy( command_buffer, LR_state->lrs_struct->network_id, sizeof( command_buffer ) );
			break;

		case  LR_TEXT_CMD_STR_SLAVE_REPEATER:
			// 4/27/2017 ajv : This setting is only enabled on Repeaters
			if( atoi(LR_state->lrs_struct->modem_mode) == LR_MODE_REPEATER )
			{
				strlcpy( command_buffer, "1", sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, "0", sizeof( command_buffer ) );
			}
			break;

		case  LR_TEXT_CMD_STR_DIAGNOSTICS:
			strlcpy( command_buffer, "5", sizeof( command_buffer ) );
			break;

		case  LR_TEXT_CMD_STR_SUBNET_ID:
			// 3/18/2015 mpd : This field is dynamic based on MODE. 
			strlcpy( command_buffer, LR_state->lrs_struct->subnet_id, sizeof( command_buffer ) );
			break;

		case  LR_TEXT_CMD_STR_SUBNET_RCV_ID:
			// 3/18/2015 mpd : This field is dynamic based on MODE. 
			strlcpy( command_buffer, LR_state->lrs_struct->subnet_rcv_id, sizeof( command_buffer ) );
			break;

		case  LR_TEXT_CMD_STR_SUBNET_XMT_ID:
			// 3/18/2015 mpd : This field is dynamic based on MODE. 
			strlcpy( command_buffer, LR_state->lrs_struct->subnet_xmt_id, sizeof( command_buffer ) );
			break;

		case  LR_MENU_CMD_END:
		case  LR_TEXT_CMD_END:
			command_ptr = NULL;
			break;

		default:
			command_ptr = NULL;
			break;
	}

	// 3/18/2015 mpd : Is there a new command in the buffer?
	if( (command_buffer[ 0 ]) || (send_just_cr_lf) )
	{
		// 3/17/2015 mpd : All the text commands sent to the radio (LR_TEXT_CMD_...) require CR and
		// LF characters at the end of the string. This does not apply to the special case
		// LR_TEXT_CMD_NONE which is never sent to the radio.
		if( command > LR_TEXT_CMD_NONE )
		{
			strlcat( command_buffer, LR_CRLF_STR, (LR_LONGEST_COMMAND_LEN + LR_CRLF_STR_LEN) );
		}

		// 3/18/2015 mpd : Allocate memory, set the return pointer, and copy the buffer. The calling
		// function is responsible for freeing the memory.
		command_ptr = mem_malloc( strlen(command_buffer) + NULL_TERM_LEN );
		memcpy( command_ptr, command_buffer, strlen(command_buffer) );

		// 3/18/2015 mpd : Null-terminate the string.
		*(command_ptr + strlen(command_buffer)) = 0x00;
	}

	return( command_ptr );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  				   READ and WRITE Lists 				  */
/* ---------------------------------------------------------- */

// 3/17/2015 mpd : This struct contains the variables and the function pointer necessary to
// process each individual response from the LR radio. The first 2 fields deal with the
// current response. The last 2 fields drive the subsequent radio response.
typedef struct
{
	// 4/8/2015 mpd : This is the text string we expect to see in the response buffer. It is a
	// sanity check performed before attempting to parse the rest of the buffer.
	char *title_str;

	// 4/8/2015 mpd : This function pointer identifies the response handler for this specific
	// response buffer.
	void (*LR_response_handler)( LR_STATE_STRUCT *LR_state ); 

	// 4/8/2015 mpd : The next command is what will be sent to the LR radio. It can be a simple
	// ASCII character without CR and LF, or a string.
	UNS_32 next_command;

	// 4/8/2015 mpd : This string tells rcvd_data what constitutes an end to the subsequent
	// radio response. The comm_mngr timer will expire if this string is not received within the
	// expected time.
	char  *next_termination_str;

} LR_menu_item;

/* ---------------------------------------------------------- */

LR_menu_item LR_read_list[] =
{
	// Response Title           Response Handler                  Next Command             Next Termination String
	{ "not used",               NULL,                             LR_TEXT_CMD_NONE,        "Enter Choice" },	// The radio provides the first buffer without a text command. 

	{ "MAIN MENU",              LR_analyze_main_menu,             LR_MENU_CMD_STR_0,       "Enter Choice"},		// Analyze all menu values. Go to MODEM MODE menu
	{ "SET MODEM MODE",         LR_analyze_set_modem_mode,        LR_MENU_CMD_STR_ESC,     "Enter Choice"},		// Analyze all menu values. Go to MAIN MENU

	{ "MAIN MENU",              NULL,                             LR_MENU_CMD_STR_1,       "Enter Choice"},		// Go to BAUD RATE MENU
	{ "SET BAUD RATE",          LR_analyze_set_baud_rate,         LR_MENU_CMD_STR_ESC,     "Enter Choice"},		// Analyze all menu values. Go to MAIN MENU     

	{ "MAIN MENU",              NULL,                             LR_MENU_CMD_STR_3,       "Enter Choice"},		// Go to RADIO PARAMETERS MENU                                       
	{ "RADIO PARAMETERS",       LR_analyze_radio_parameters,      LR_MENU_CMD_STR_0,       "Enter New Freq"},
	{ "Enter New Freq",         NULL,						      LR_MENU_CMD_STR_F,       "  1 "},
	{ "Ch  Xmit   Rcv",			LR_analyze_frequency,		      LR_MENU_CMD_STR_ESC,     "Enter Choice"},
	{ "RADIO PARAMETERS",       NULL,      						  LR_MENU_CMD_STR_ESC,     "Enter Choice"},

	{ "MAIN MENU",              NULL,                             LR_MENU_CMD_STR_5,       "Enter Choice"},		// Go to MULTIPOINT PARAMETERS MENU                                       
	{ "MULTIPOINT PARAMETERS",  LR_analyze_multipoint_parameters, LR_MENU_CMD_STR_ESC,     "Enter Choice"},		// Analyze all menu values. Go to MAIN MENU

	{ "MAIN MENU",              LR_final_radio_analysis,          LR_TEXT_CMD_END,         ""}					// Final analysis
};

// 4/8/2015 mpd : Determine the size of the read list. This will be used later to determine if we successfully reached 
// the end.
#define LR_READ_LIST_LEN	(sizeof(LR_read_list) / sizeof(LR_menu_item))

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/16/2015 mpd : For consistency, navigate in the same menu order for WRITE as we did for
// READ. Note that WRITE requires additional analyze/hunt I/O cycles between the menu
// navigation cycles. Once on the appropriate menu, each WRITE operation involves a cycle to
// select a menu option, and another to input the new value. Also note that CR and LF
// characters ARE NOT INCLUDED ON EVERY LINE!!! The radio is very fussy regarding CR and LF.
// 
// The "analyze" functions following all WRITE operations on the menu are to capture the
// final values on that menu.
// 
// 3/16/2015 mpd : Verification of each WRITE operation can be performed on the last line
// associated with each menu. Verification of the MAIN MENU is strictly a sanity check since
// there are no writable fields on that LR radio menu.
//
// 4/13/2015 mpd : The "Response Title" requires enough characters to determine a unique
// match; not the entire line!
LR_menu_item LR_write_list[] =
{
	// Response Title           Response Handler                  Next Command                          Next Termination String
	{ "not used",               NULL,                             LR_TEXT_CMD_NONE,                     "Enter Choice" },											// The radio provides the first buffer without a text command. 

	{ "MAIN MENU",              LR_analyze_main_menu,		      LR_MENU_CMD_STR_0,                    "Enter Choice"},											// Verify all menu values. Go to MODEM MODE menu
	{ "SET MODEM MODE",         NULL,                             LR_TEXT_CMD_STR_MODE,                 "Enter Choice"},											// Set dynamic value
	{ "SET MODEM MODE",         NULL,                             LR_MENU_CMD_STR_F,                 	"IP Address"},												// Get to Ethernet/IP Radio Setup submenu
	{ "IP Radio Setup",         NULL,                             LR_MENU_CMD_STR_0,                 	"0 For Off"},												// Get to Ethernet Mode submenu
	{ "1 For Ethernet Mode",    NULL,                             LR_MENU_CMD_STR_0,                 	"IP Address"},												// Set Ethernet Mode OFF
	{ "IP Radio Setup",         NULL,                             LR_MENU_CMD_STR_ESC,                 	"Enter Choice"},											// Return to Modem Mode submenu
	{ "SET MODEM MODE",         LR_analyze_set_modem_mode,        LR_MENU_CMD_STR_ESC,                  "Enter Choice"},											// Verify all menu values. Go to MAIN MENU

	{ "MAIN MENU",              NULL,                             LR_MENU_CMD_STR_1,                    "Enter Choice"},											// Go to BAUD RATE menu
	{ "SET BAUD RATE",          NULL,                             LR_TEXT_CMD_STR_BAUD_RATE,            "Enter Choice"},											// Set rate  
	{ "SET BAUD RATE",          NULL,                             LR_MENU_CMD_STR_D,                    "3 for Both"},												// Get SETUP PORT submenu 
	{ "3 for Both",             NULL,                             LR_MENU_CMD_STR_3,                    "Enter Choice"},											// Set to BOTH 
	{ "SET BAUD RATE",          NULL,                             LR_MENU_CMD_STR_F,                    "Enter 0 for None,1 For RTS,2 for DTR"},					// Get FLOW CONTROL submenu 
	{ "Enter 0 for None",       NULL,                             LR_MENU_CMD_STR_1,                    "Enter Choice"},											// Set to RTS 
	{ "SET BAUD RATE",          LR_analyze_set_baud_rate,         LR_MENU_CMD_STR_ESC,                  "Enter Choice"},											// Verify all menu values. Go to MAIN MENU

	{ "MAIN MENU",              NULL,                             LR_MENU_CMD_STR_3,                    "Enter Choice"},											// Go to RADIO PARAMETERS menu
	{ "RADIO PARAMETERS",       NULL,                             LR_MENU_CMD_STR_1,                    "Enter Max Packet (0-9)"},									// Get MAX PACKET submenu
	{ "Enter Max Packet",       NULL,                             LR_TEXT_CMD_STR_MAX_PACKET,           "Enter Choice"},											// Set dynamic value
	{ "RADIO PARAMETERS",       NULL,                             LR_MENU_CMD_STR_2,                    "Enter Min Packet (0-9)"},									// Get MIN PACKET submenu
	{ "Enter Min Packet",       NULL,                             LR_TEXT_CMD_STR_MIN_PACKET,           "Enter Choice"},											// Set dynamic value
	{ "RADIO PARAMETERS",       NULL,                             LR_MENU_CMD_STR_3,                    "Enter New Xmit Rate (0-F)"},								// Get XMIT RATE submenu
	{ "Enter New Xmit Rate",    NULL,                             LR_TEXT_CMD_STR_XMIT_RATE,            "Enter Choice"},											// Set to 3
	{ "RADIO PARAMETERS",       NULL,                             LR_MENU_CMD_STR_4,                    "Enter New RF Data Rate (0-5)"},							// Get DATA RATE submenu
	{ "Enter New RF Data ",     NULL,                             LR_TEXT_CMD_STR_DATA_RATE,            "Enter Choice"},											// Set to 3
	{ "RADIO PARAMETERS",       NULL,                             LR_MENU_CMD_STR_5,                    "Enter New XmitPower (0-10)"},								// Get XMT POWER submenu
	{ "Enter New XmitPower",    NULL,                             LR_TEXT_CMD_STR_XMT_POWER,            "Enter Choice"},											// Set dynamic value
	{ "RADIO PARAMETERS",       NULL,                             LR_MENU_CMD_STR_7,                    "Enter 1 to allow Master RTS to Slave CTS connection, 0 otherwise"},			// Go to LP MODE submenu
	{ "Slave CTS connection",   NULL,                             LR_TEXT_CMD_STR_RTS_TO_CTS,           "Enter Choice"},											// Set to 0
	{ "RADIO PARAMETERS",       NULL,                             LR_MENU_CMD_STR_8,                    "Enter Number Retries before Timeout"},		 				// Go to LP MODE submenu
	{ "Enter Number Retries",   NULL,                             LR_TEXT_CMD_STR_RETRY_TIMEOUT,        "Enter Choice"},											// Set to 0
	{ "RADIO PARAMETERS",       NULL,                             LR_MENU_CMD_STR_9,                    "Enter LowPower Option or 0 to disable (0-31)"},			// Go to LP MODE submenu
	{ "Enter LowPower Option",  NULL,                             LR_TEXT_CMD_STR_LOWPOWER_MODE,        "Enter Choice"},											// Set to 0
	{ "RADIO PARAMETERS",       NULL,                             LR_MENU_CMD_STR_0,                    "Enter New Freq"},											// 
	{ "Enter New Freq",         NULL,                             LR_MENU_CMD_STR_F,                    "Number of Hopping Channels"},                              // 
	{ "Hop Table Size",         NULL,                             LR_MENU_CMD_STR_1,                    "Enter Frequency Channel to use"},							// 
	{ "Enter Frequency Channel",NULL,                             LR_TEXT_CMD_STR_FREQ_CHAN,     	    "Number of Hopping Channels"},								// 
	{ "Hop Table Size",         NULL,                             LR_MENU_CMD_STR_2,                    "Num Channels"},											// 
	{ "Num Channels",			NULL,                             LR_TEXT_CMD_STR_NUM_CHAN,     	    "Number of Hopping Channels"},								// 
	{ "Hop Table Size",         NULL,                             LR_MENU_CMD_STR_0,                    "Channel Number (0-15)"},									// 
	{ "Channel Number",         NULL,                             LR_TEXT_CMD_STR_FREQ_CHAN, 			"Xmit Chan (00000-05600)"},									// 
	{ "Xmit Chan",   			NULL,			                  LR_TEXT_CMD_STR_XMIT_CHAN,			"Rcv Chan (00000-05600)"},									// 
	{ "Rcv Chan",   			NULL, 			                  LR_TEXT_CMD_STR_RCV_CHAN,			    "Number of Hopping Channels"},								// 
	{ "Hop Table Size",   		NULL,			                  LR_MENU_CMD_STR_ESC, 					"Enter Choice"},											// 
	{ "RADIO PARAMETERS",       LR_analyze_radio_parameters,      LR_MENU_CMD_STR_0,                    "Enter New Freq"},
	{ "Enter New Freq",         NULL,						      LR_MENU_CMD_STR_F,                    "  1 "},
	{ "Ch  Xmit   Rcv",			LR_analyze_frequency,		      LR_MENU_CMD_STR_ESC,                  "Enter Choice"},
	{ "RADIO PARAMETERS",       NULL,      						  LR_MENU_CMD_STR_ESC,                  "Enter Choice"},

	{ "MAIN MENU",              NULL,                             LR_MENU_CMD_STR_5,                    "Enter Choice"},											// Go to MULTIPOINT PARAMETERS menu
	{ "MULTIPOINT PARAMETERS",  NULL,                             LR_MENU_CMD_STR_0,                    "Enter Number of Parallel Repeaters in Network(0-9)"},		// Get NUM REPEATERS submenu
	{ "Enter Number of Para",   NULL,                             LR_TEXT_CMD_STR_NUM_REPEATERS,        "Enter Choice"},											// Set dynamic value
	{ "MULTIPOINT PARAMETERS",  NULL,                             LR_MENU_CMD_STR_1,                    "Enter Number Times Master Repeats Packets(0-9)"},			// Get MASTER REPEAT submenu
	{ "Enter Number Times M",   NULL,                             LR_TEXT_CMD_STR_MASTER_PACKET_REPEAT, "Enter Choice"},											// Set dynamic value
	{ "MULTIPOINT PARAMETERS",  NULL,                             LR_MENU_CMD_STR_2,                    "Enter Number Times Slave Tries Before Backing Off (0-9)"},	// Get SLAVE RETRY submenu
	{ "Enter Number Times S",   NULL,                             LR_TEXT_CMD_STR_MAX_SLAVE_RETRY,      "Enter Choice"},											// Set dynamic value
	{ "MULTIPOINT PARAMETERS",  NULL,                             LR_MENU_CMD_STR_3,                    "Enter Slave Backing Off Retry Odds (0-9)"},				// Get RETRY ODDS submenu
	{ "Enter Slave Backing",    NULL,                             LR_TEXT_CMD_STR_RETRY_ODDS,           "Enter Choice"},											// Set dynamic value
	{ "MULTIPOINT PARAMETERS",  NULL,                             LR_MENU_CMD_STR_6,                    "Enter Network ID Number (0-4095)"},						// Get NETWORK ID submenu
	{ "Enter Network ID",       NULL,                             LR_TEXT_CMD_STR_NETWORK_ID,           "Enter Choice"},											// Set dynamic value
	{ "MULTIPOINT PARAMETERS",  NULL,                             LR_MENU_CMD_STR_A,                    "Enter 1 to enable Slave/Repeater or 0 for Normal"},		// Get Slave/Repeater submenu
	{ "Enter 1 to enable",      NULL,                             LR_TEXT_CMD_STR_SLAVE_REPEATER,       "Enter Choice"},											// Set dynamic value
	{ "MULTIPOINT PARAMETERS",  NULL,                             LR_MENU_CMD_STR_B,                    "Enter 1 to 129 Enable Diagnostics, 0 To Disable"},			// Get DIAG submenu
	{ "Enter 1 to 129",         NULL,                             LR_TEXT_CMD_STR_DIAGNOSTICS,          "Enter Choice"},											// Set dynamic value
	{ "MULTIPOINT PARAMETERS",  NULL,                             LR_MENU_CMD_STR_C,                    "Enter Rcv SubNetID (0-F)"},								// Get SUBNET ID submenu
	{ "Enter Rcv SubNetID",     NULL,                             LR_TEXT_CMD_STR_SUBNET_RCV_ID,        "Enter Xmit SubNetID (0-F)"},								// Set dynamic value
	{ "Enter Xmit SubNetID",    NULL,                             LR_TEXT_CMD_STR_SUBNET_XMT_ID,        "Enter Choice"},											// Set dynamic value (second submenu entry)
	{ "MULTIPOINT PARAMETERS",  LR_analyze_multipoint_parameters, LR_MENU_CMD_STR_ESC,                  "Enter Choice"},											// Verify all menu values. Go to MAIN MENU

	{ "MAIN MENU",              LR_final_radio_verification,      LR_TEXT_CMD_END,                      ""}															// Final verification
};

// 4/8/2015 mpd : Determine the size of the write list. This will be used later to determine
// if we successfully reached the end.
#define LR_WRITE_LIST_LEN	(sizeof(LR_write_list) / sizeof(LR_menu_item))

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  					 Radio Mode Functions   			  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void LR_FREEWAVE_set_radio_to_programming_mode( void )
{
	strlcpy( GuiVar_CommOptionInfoText, "Connecting to device...", sizeof(GuiVar_CommOptionInfoText) );

	// 4/16/2015 mpd : Send progress info to the GUI. Keep it simple. Use the READ key for both
	// READ and WRITE.
	if( LR_state->operation == LR_READ_OPERATION )
	{
		COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );
	}
	else
	{
		COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_write_settings_in_progress );
	}

	// ----------

	// 4/15/2015 mpd : The LR radio can only be programmed at a baud rate of 19200. Since the
	// port is at 57600 for normal communication, we have to temporarily modify the speed for
	// programming mode.
	SERIAL_set_baud_rate_on_A_or_B( comm_mngr.device_exchange_port, 19200 );

	// ----------

	// 4/15/2015 mpd : Temporarily ground from the brown interrupt wire to put the radio into
	// programming mode.
	set_reset_ACTIVE_to_serial_port_device( comm_mngr.device_exchange_port );

	vTaskDelay( MS_to_TICKS(100) );

	set_reset_INACTIVE_to_serial_port_device( comm_mngr.device_exchange_port ); 
}

/* ---------------------------------------------------------- */
static void send_no_response_esc_to_radio()
{
	DATA_HANDLE ldh;

	char command = LR_MENU_CMD_STR_ESC;

	// ----------

	// 4/9/2015 mpd : Restore packet hunting (what the rest of the application expects!).
	RCVD_DATA_enable_hunting_mode( comm_mngr.device_exchange_port, PORT_HUNT_FOR_PACKETS, NOT_STRING_HUNTING  );

	ldh.dptr = (UNS_8*)&command;

	ldh.dlen = LR_MENU_CMD_LEN;

	AddCopyOfBlockToXmitList( comm_mngr.device_exchange_port, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );
}

/* ---------------------------------------------------------- */
static void exit_radio_programming_mode()
{
	// 4/9/2015 mpd : Send a final ESC to the radio to exit MAIN MENU.
	send_no_response_esc_to_radio();

	// 4/9/2015 mpd : Remove the ground from the brown interrupt wire.
	set_reset_INACTIVE_to_serial_port_device( comm_mngr.device_exchange_port );

	// 4/15/2015 mpd : The LR radio was temporarily set to a baud rate of 19200 for programming. It's time to put it back
	// into normal communication speed.
	SERIAL_set_baud_rate_on_A_or_B( comm_mngr.device_exchange_port, port_device_table[ COMM_DEVICE_LR_FREEWAVE ].baud_rate );

	comm_mngr.device_exchange_state = LR_DEVEX_FINISH;

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  				 COMM_MNGR Command Interface			  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void setup_for_termination_string_hunt( const char* pcommand_str, char* presponse_str )
{
	// 3/14/2014 rmd : This function is executed within the context of the comm_mngr task. We
	// can reference and change the device exchange variables as needed.

	DATA_HANDLE ldh;

	// 2/13/2014 rmd : Clean and restart the port buffer and string hunt. Note this disables
	// packet hunting mode.
	RCVD_DATA_enable_hunting_mode( comm_mngr.device_exchange_port, PORT_HUNT_FOR_SPECIFIED_TERMINATION, WHEN_STRING_FOUND_NOTIFY_THE_COMM_MNGR_TASK );

	// ----------

	ldh.dptr = (UNS_8*)pcommand_str;

	ldh.dlen = strlen( pcommand_str );

	// 3/10/2015 mpd : Identify the termination string.
	SerDrvrVars_s[ comm_mngr.device_exchange_port ].UartRingBuffer_s.th.str_to_find = presponse_str;

	SerDrvrVars_s[ comm_mngr.device_exchange_port ].UartRingBuffer_s.th.chars_to_match = strlen( presponse_str );

	// ----------

	// 3/12/2015 mpd : Don't try to add a zero length command to the Xmit list. That triggers
	// alerts in "comm_mngr". Also, check for a special "LR_EMPTY_STR" value used to flag the
	// first I/O cycle.  The first read cycle sets up the hunt for an input that is triggered by
	// radio initialization hardware - not a xmit command. Sending a command here for the first
	// cycle would trigger an additional unwanted radio response.
	if( (ldh.dlen > 0) && (pcommand_str[ 0 ] != LR_EMPTY_STR) )
	{
		AddCopyOfBlockToXmitList( comm_mngr.device_exchange_port, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );
	}

}

/* ---------------------------------------------------------- */
static BOOL_32 LR_get_and_process_command( LR_STATE_STRUCT *LR_state, const UNS_32 next_command, char *next_termination_str )
{
	char *command_ptr = NULL;

	BOOL_32 command_found = (true);

	// 3/19/2015 mpd : Get the command string.
	if( ( command_ptr = get_command_text( LR_state, next_command ) ) != NULL )
	{
		// 3/10/2015 mpd : Set up the string hunt.
		setup_for_termination_string_hunt( command_ptr, next_termination_str );  

		// 3/18/2015 mpd : Free the memory obtained by "get_command_text()".
		mem_free( command_ptr );
	}
	else
	{
		command_found = (false);
	}

	return( command_found );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  			  Operation Initialization  				  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void LR_set_read_operation( LR_STATE_STRUCT *LR_state )
{
	LR_state->operation = LR_READ_OPERATION;

	// 4/13/2015 mpd : Send a status message to the key task.
	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );

	// 3/12/2015 mpd : Reset the read list to the beginning. This is used here at initialization
	// and by the read state machine to navigate through all the radio menus and process
	// responses.
	LR_state->read_list_index = 0;

	// 3/20/2015 mpd : Initialize the WRITE index even though we will not be using it at the
	// moment.
	LR_state->write_list_index = 0;

	// 3/20/2015 mpd : Setup the state machine entry point.
	comm_mngr.device_exchange_state = LR_DEVEX_READ;
}

/* ---------------------------------------------------------- */
static void LR_set_write_operation( LR_STATE_STRUCT *LR_state )
{
	LR_state->operation = LR_WRITE_OPERATION;

	// 4/13/2015 mpd : Send a status message to the key task.
	COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_write_settings_in_progress );

	// 3/12/2015 mpd : Reset the write list to the beginning. This is used here at
	// initialization and by the read state machine to navigate through all the radio menus and
	// process responses.
	LR_state->write_list_index = 0;

	// 3/20/2015 mpd : Initialize the READ index even though we will not be using it at the
	// moment.
	LR_state->read_list_index = 0;

	// ----------

	LR_set_network_group_values( LR_state );

	// ----------

	// 3/20/2015 mpd : Setup the state machine entry point.
	comm_mngr.device_exchange_state = LR_DEVEX_WRITE;
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  				  State Struct Handling 				  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void LR_set_state_struct_for_new_device_exchange( LR_STATE_STRUCT *LR_state )
{
	// 3/23/2015 mpd : These are fields that need to be set each time the LR radio programming screen is entered or a 
	// button is selected by the user.

	if( comm_mngr.device_exchange_initial_event == COMM_MNGR_EVENT_request_write_device_settings )
	{
		COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_write_settings_in_progress );

		LR_set_write_operation( LR_state );
	}
	else
	{
		COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );

		LR_set_read_operation( LR_state );
	}
}

/* ---------------------------------------------------------- */
static void LR_initialize_state_struct( LR_STATE_STRUCT *LR_state )
{
	// 3/20/2015 mpd : Set pointers to the programmable variables structs.
	LR_state->lrs_struct = &lrs_struct;

	strlcpy( LR_state->sw_version,    "", sizeof( LR_state->sw_version ) );                

	strlcpy( LR_state->serial_number, "", sizeof( LR_state->serial_number ) );            

	strlcpy( LR_state->model,         "", sizeof( LR_state->model ) );                      

	// ----------

	// 3/19/2015 mpd : These are Calsense defined groups.
	LR_state->group_index = LR_GROUP_DEFAULT;

	strlcpy( LR_state->network_group_id,  "", sizeof( LR_state->network_group_id ) );
}

/* ---------------------------------------------------------- */
static BOOL_32 LR_verify_state_struct( LR_STATE_STRUCT *LR_state )
{
	BOOL_32 valid_state_struct;

	// 3/23/2015 mpd : Verify the "dynamic_pvs" pointer.
	if( LR_state->lrs_struct != NULL )
	{
		valid_state_struct = (true);
	}
	else
	{
		valid_state_struct = (false);
	}

	return( valid_state_struct );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  	 READ/WRITE Item processing and the State Machine     */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static BOOL_32 process_list( LR_STATE_STRUCT *LR_state, COMM_MNGR_TASK_QUEUE_STRUCT *q_msg, const LR_menu_item *list_item, UNS_32 operation )
{
	char *title_ptr = NULL;
	BOOL_32 continue_cycle = (true);

	// 3/12/2015 mpd : This function performs two separate operations.
	// 
	// 1. The first operation deals with the current contents of the receive buffer. The
	// list_item struct identifies the title string and the appropriate handler for the work.
	// 
	// 2. The second operation sets up RCVD_DATA to wait for the next receive buffer. The
	// list_item struct specifies the command string needed to put the LR radio into the
	// requested menu and what string indicates the end of the radio response.

	// 3/12/2015 mpd : FIRST OPERATION: Deal with the existing receive buffer.

	if( q_msg->event == COMM_MNGR_EVENT_device_exchange_response_arrived )
	{
		// 3/23/2015 mpd : The memory handed to us by "rcvd_data" is our's to keep until we are done parsing. Better still, we
		// free it at the end of parsing before a user can create a "key timeout" by modifying a field and going home for the
		// night. Make convenient local copies of the pointer and response length so the rest of LR radio code does not
		// reference "comm_mngr" variables.
		if( ( q_msg->dh.dptr != NULL ) && ( q_msg->dh.dlen > 0 ) )
		{
			// 3/11/2015 mpd : Look for the menu title string in the data buffer.

			// 4/10/2015 mpd : Embedded '\0' characters have been seen at the beginning of LR radio response buffers. These
			// end strstr() searches. Use a custom search based on buffer length.
			if( ( title_ptr = find_string_in_block( (char*)q_msg->dh.dptr, list_item->title_str, q_msg->dh.dlen ) ) != NULL )
			{
				// 3/10/2015 mpd : The LR radio seems to add various CR and LF characters at the start of the buffer. We need a
				// predictable offset for all fields in the response. Set the menu title string as the start of the response.
				LR_state->resp_ptr = title_ptr;

				// 3/23/2015 mpd : The length may have been modified since we started at "title_ptr". Calculate how much 
				// we took off the front, then subtract from the total.
				LR_state->resp_len =  ( q_msg->dh.dlen - ( title_ptr - (char*)q_msg->dh.dptr ) );

				// 3/20/2015 mpd : Call the response handler to do something with this buffer. This is the meat!
				if( list_item->LR_response_handler != NULL )
				{
					(list_item->LR_response_handler)( LR_state );
				}
			}

			// 3/23/2015 mpd : The string we are looking for was not found in the response buffer.
			else
			{
				continue_cycle = (false);
			}

			// 3/11/2015 mpd : Free the memory obained by the receive task.
			mem_free( q_msg->dh.dptr );
		}

	} // END COMM_MNGR_EVENT_device_exchange_response_arrived

	else if( q_msg->event == COMM_MNGR_EVENT_device_exchange_response_timer_expired )
	{
		continue_cycle = (false);
	}
	else
	{
		continue_cycle = (false);
	}

	// 3/12/2015 mpd : SECOND OPERATION: Set up the COMM_MNGR to wait for the next receive buffer.

	if( continue_cycle )
	{
		// 3/20/2015 mpd : The "active_pvs" pointer determines what command set "get_command_text()"
		// will be looking at. That pointer was handled when the "LR_state->operation" was set.
		// There is no decision to be made here.

		if( !LR_get_and_process_command( LR_state, list_item->next_command, (char *)( list_item->next_termination_str ) ) )
		{
			continue_cycle = (false);
		}
	}
	else
	{
		// 4/9/2015 mpd : This condition should never happen. If it does, the radio needs to be blown out of programming mode to
		// recover. Throw multiple ESC keys at it to exit all menus.
		send_no_response_esc_to_radio();
		send_no_response_esc_to_radio();
		send_no_response_esc_to_radio();
	}

	return( continue_cycle );
}

/* ---------------------------------------------------------- */
static void LR_state_machine( LR_STATE_STRUCT *LR_state, COMM_MNGR_TASK_QUEUE_STRUCT *pq_msg )
{
	// 3/14/2014 rmd : This function executed within the context of the comm_mngr task. We can
	// reference and change the device exchange variables as needed.

	// ----------

	UNS_32  device_exchange_result;

	// ----------

	switch( comm_mngr.device_exchange_state )
	{
		case LR_DEVEX_READ:

			// 3/12/2015 mpd : Initialization used the first READ "LR_menu_list" entry. This state machine needs to begin at the
			// second entry. Bump the index here BEFORE using it.
			LR_state->read_list_index++;

			// 3/18/2015 mpd : Loop through the list until there are no more commands (return == "false").
			if( ( process_list( LR_state, pq_msg, &( LR_read_list[ LR_state->read_list_index ] ), LR_READ_OPERATION ) ) == (false) )
			{
				exit_radio_programming_mode();

				// 3/18/2015 mpd : We just finished looping through the list. Did we finist too early?
				if( LR_state->read_list_index == 1 )
				{
					// 4/16/2015 mpd : We never heard from COMM_MNGR.
					device_exchange_result = DEVICE_EXCHANGE_KEY_read_settings_error;

					strlcpy( GuiVar_CommOptionInfoText, "No response from device...", sizeof(GuiVar_CommOptionInfoText) );
				}
				else if( LR_state->read_list_index < ( LR_READ_LIST_LEN - 1 ) )
				{
					// 4/16/2015 mpd : We got part way through the list. The final count will help with debugging.
					device_exchange_result = DEVICE_EXCHANGE_KEY_read_settings_error;

					strlcpy( GuiVar_CommOptionInfoText, "Unable to read data...", sizeof(GuiVar_CommOptionInfoText) );
				}
				// 4/13/2015 mpd : We successfully got to the end of the list.
				else
				{
					// 5/12/2015 mpd : Invalid device settings are considered a configuration issue, not a READ error.
					// Return "OK" for invalid so the GUI will display values and allow access to the WRITE function.
					device_exchange_result = DEVICE_EXCHANGE_KEY_read_settings_completed_ok;
				}

				// 4/13/2015 mpd : Send the device exchange result as message to the key task.
				COMM_MNGR_device_exchange_results_to_key_process_task( device_exchange_result );
			}
			else
			{
				// 4/13/2015 mpd : There is more to do. Set the COMM_MNGR timer and wait for another response.

				// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
				// when posting is safe. Anything on the timer queue is quickly processed and therefore the
				// queue is effectively always empty.
				xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LR_COMM_TIMER_MS), portMAX_DELAY );
			}
			break;

		case LR_DEVEX_WRITE:
			// 4/9/2015 mpd : The WRITE operation uses the "LR_dynamic_pvs" as a source for programming the LR radio. Values in that
			// struct may be from the previous READ operation, or they may have been modified by a user in the GUI code. A copy is
			// made before the WRITE ("LR_dynamic_pvs" is copied to "LR_write_pvs") so the intended values can be verified against
			// what actually ended up in the radio after the WRITE ("LR_dynamic_pvs").

			// 3/12/2015 mpd : Initialization used the first WRITE "LR_menu_list" entry. This state machine needs to begin at the
			// second entry. Bump the index here BEFORE using it.
			LR_state->write_list_index++;

			// 3/18/2015 mpd : Loop through the list until there are no more commands (return == "false").
			if( ( process_list( LR_state, pq_msg, &( LR_write_list[ LR_state->write_list_index ] ), LR_WRITE_OPERATION  ) ) == (false) )
			{
				exit_radio_programming_mode();

				// 3/18/2015 mpd : We just finished looping through the list. Did we finist too early?
				if( LR_state->write_list_index == 1 )
				{
					// 4/16/2015 mpd : We never heard from COMM_MNGR.
					device_exchange_result = DEVICE_EXCHANGE_KEY_write_settings_error;

					strlcpy( GuiVar_CommOptionInfoText, "No response from device...", sizeof(GuiVar_CommOptionInfoText) );
				}
				else if( LR_state->write_list_index < ( LR_WRITE_LIST_LEN - 1 ) )
				{
					// 4/16/2015 mpd : We got part way through the list. The final count will help with debugging.
					device_exchange_result = DEVICE_EXCHANGE_KEY_write_settings_error;

					strlcpy( GuiVar_CommOptionInfoText, "Unable to write data...", sizeof(GuiVar_CommOptionInfoText) );
				}
				// 4/13/2015 mpd : We successfully got to the end of the list.
				else
				{
					// 5/12/2015 mpd : A mismatch after a WRITE is considered a configuration issue, not a WRITE error.
					// We were able to get to the end of the WRITE list. Return "OK" for invalid so the GUI will display 
					// values and allow the user to see the problem.
					device_exchange_result = DEVICE_EXCHANGE_KEY_write_settings_completed_ok;
				}

				// 4/13/2015 mpd : Send the device exchange result as message to the key task.
				COMM_MNGR_device_exchange_results_to_key_process_task( device_exchange_result );
			}
			else
			{
				// 4/13/2015 mpd : There is more to do. Set the COMM_MNGR timer and wait for another response.

				// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
				// when posting is safe. Anything on the timer queue is quickly processed and therefore the
				// queue is effectively always empty.
				xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LR_COMM_TIMER_MS), portMAX_DELAY );
			}
			break;

		case LR_DEVEX_FINISH:
		default:
			// 4/22/2015 mpd : The READ or WRITE is complete. Clean up the operation fields.
			LR_state->operation = LR_IDLE_OPERATION;
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  					Extern Functions					  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void LR_FREEWAVE_power_control( const UNS_32 pport, const BOOL_32 pon_or_off )
{
	// 11/21/2014 rmd : This function is executed within the context of the COMM_MNGR task.

	// 6/28/2013 rmd : This is an extern function, but NOT intended to be directly called. It
	// can be, but that is not its intended use. Should be referenced via the
	// config_c.port_settings.

	// 3/25/2014 rmd : On the Freewave device card the reset line is hooked up to the menu pin.
	// When momentarily driven low will put the device into menu mode.
	set_reset_INACTIVE_to_serial_port_device( pport );

	// 3/10/2014 rmd : Call the generic power control function. (false) means to power down.
	// (true) means to power up.
	GENERIC_freewave_card_power_control( pport, pon_or_off );
}

/* ---------------------------------------------------------- */
extern void LR_FREEWAVE_initialize_device_exchange( void )
{
	// 4/8/2015 mpd : Determine if the state structure has ever been initialized.
	if( !LR_verify_state_struct( LR_state ) )
	{
		// 4/8/2015 mpd : The state structure is invalid. This is normal upon first entering the LR programming screen. 
		// Handle all the first-time initialization.
		LR_initialize_state_struct( LR_state );
	}

	// 4/13/2015 mpd : Handle the state structure fields that reset on every event. 
	LR_set_state_struct_for_new_device_exchange( LR_state );

	// The LR radio menu READ process is a cycle typically triggered by RCVD_DATA when it
	// recognizes a complete buffer. The cycle starts with analyzing the current response buffer
	// from the radio, then requesting another buffer. The iterations are driven by an arrays of
	// "LR_menu_item" structures. Here at initialization, only the "hunt" half of the first READ
	// "LR_menu_item" entry is being used since there is no initial buffer to analyze. The
	// "next_termination_str" is the critical piece that informs RCVD_DATA what we are waiting
	// for. The first "next_command_str" is also a special case. It is set to a special value
	// indicating not to send a command to the radio. We need a process cycle to read the
	// contents of the first buffer.

	// 3/20/2015 mpd : Get the first command string from the list and set up the string hunt so
	// the initial response from the radio is expected, captured, and returned to the state
	// machine.
	if( LR_get_and_process_command( LR_state, LR_read_list[ LR_state->read_list_index ].next_command, LR_read_list[ LR_state->read_list_index ].next_termination_str ) )
	{
		// 3/12/2015 mpd : This triggers the LR radio to enter programming mode and send the initial
		// menu buffer.
		LR_FREEWAVE_set_radio_to_programming_mode();

		// 3/14/2014 rmd : Start the timer so that 10ms from now the first exchange will start.

		// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
		// when posting is safe. Anything on the timer queue is quickly processed and therefore the
		// queue is effectively always empty.
		xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LR_COMM_TIMER_MS), portMAX_DELAY );
	}
}

/* ---------------------------------------------------------- */
extern void LR_FREEWAVE_exchange_processing( void *pq_msg )
{
	// 3/18/2014 rmd : Note how unfortunately we had to use a parameter of type void*. We could
	// not use the COMM_MNGR_EVENT_QUEUE_EVENT_STRUCT type as it wouldn't compile. Circular
	// references. void* was the easiest way out of the problem.

	// 3/19/2014 rmd : Always stop the response timer for the device exchange process.
	xTimerStop( comm_mngr.timer_device_exchange, portMAX_DELAY );

	// 3/19/2014 rmd : During the last exchange the timer is started again and eventually fires
	// off. This is the block to prevent further unwanted processing.
	if( comm_mngr.mode == COMM_MNGR_MODE_device_exchange )
	{
		LR_state_machine( LR_state, (COMM_MNGR_TASK_QUEUE_STRUCT*)pq_msg );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void LR_FREEWAVE_initialize_the_connection_process( UNS_32 pport )
{
	if( pport != UPORT_A )
	{
		Alert_Message( "Why is the LR not on PORT A?" );
	}

	// 11/11/2016 rmd : Check to make sure the is connected function is not NULL. Otherwise we
	// couldn't call it and the connection process would error out. Over and over again.
	if( port_device_table[ config_c.port_A_device_index ].__is_connected == NULL )
	{
		Alert_Message( "Unexpected NULL is_connected function" );

		// 2/15/2017 rmd : I think there is no point to trying to send messages anyway. They won't
		// go because we don't have a valid 'is connected' function! So do nothing in the face of
		// this error.
	}
	else
	{
		LR_cs.port = pport;

		LR_cs.wait_count = 0;

		LR_cs.connection_start_time = my_tick_count;

		// 2/24/2017 rmd : We have defined the LR radio connection process as a cycling of power.
		// Followed by waiting for CD to indicate synchonization with the master. So start with a
		// power down.
		power_down_device( LR_cs.port );

		// ----------

		// 11/11/2016 rmd : First thing we do is to power down the device. And wait 5 seconds.
		xTimerChangePeriod( cics.process_timer, MS_to_TICKS(5000), portMAX_DELAY );

		LR_cs._state = FREEWAVE_LR_STATE_wait_after_a_powerdown;
	}
}

/* ---------------------------------------------------------- */
extern void LR_FREEWAVE_connection_processing( UNS_32 pevent )
{
	BOOL_32 lerror;

	lerror = (false);

	// ----------

	if( (pevent != CI_EVENT_process_string_found) && (pevent != CI_EVENT_process_timer_fired) )
	{
		// 10/29/2016 rmd : ERROR.
		Alert_Message( "Connection Process : UNXEXP EVENT" );

		lerror = (true);
	}
	else
	{
		switch( LR_cs._state )
		{
			case FREEWAVE_LR_STATE_wait_after_a_powerdown:
				power_up_device( LR_cs.port );

				// 2/24/2017 rmd : Power up the LR radio and wait 10 seconds. In case of an CD chatter that
				// may occur during power up sequence. That way we don't (false) trigger a connected state. So
				// wait 10 seconds.
				xTimerChangePeriod( cics.process_timer, MS_to_TICKS(10000), portMAX_DELAY );

				LR_cs._state = FREEWAVE_LR_STATE_waiting_for_connection;
				break;

			case FREEWAVE_LR_STATE_waiting_for_connection:
				if( (port_device_table[ config_c.port_A_device_index ].__is_connected != NULL) )
				{
					if( port_device_table[ config_c.port_A_device_index ].__is_connected( LR_cs.port ) )
					{
						// 11/11/2016 rmd : Okay we're done. Device connected. Update the Comm Test string so the
						// user can see the current status. And alert.
						strlcpy( GuiVar_CommTestStatus, "LR Connection", sizeof(GuiVar_CommTestStatus) );


						Alert_Message_va( "LR Radio sync'd to master in %u seconds", ((my_tick_count - LR_cs.connection_start_time)/200) );

						// ----------

						CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities();
					}
					else
					{
						// 1/24/2017 rmd : There is a HUGE disconnect between the device programming and the
						// controller initiated connection sequence. One is really NOT aware of the other. And they
						// do not co-exist well. For example if the connection process fails it just keeps trying
						// over and over again. And if you are performing device programming the power down-->up
						// associated with the connection process could occur right in the middle of device
						// programming. Not sure how well that would play out. And we have to allow the device
						// programming to proceed right now cause there ain't a way to stop the connection process.
						// Leaving this time large helps give the user a better chance of success while device
						// programming. A real cop-out but all I can do for right now.
						//
						// 1/24/2017 rmd : PERHAPS a mechanism to terminate the connection sequence to allow the
						// user a device programming interlude. A flag we set that the connection_processing
						// function would act upon.
						if( LR_cs.wait_count >= 120 )
						{
							// 11/11/2016 rmd : We've been waiting. LR normally connects in about 20 seconds so after 2
							// minutes of waiting let's restart the connection process.
							lerror = (true);
						}
						else
						{
							LR_cs.wait_count += 1;

							xTimerChangePeriod( cics.process_timer, MS_to_TICKS(1000), portMAX_DELAY );
						}
					}
				}
				else
				{
					// 11/11/2016 rmd : The sequence will restart and we have a test in the initialization
					// function for the NULL function.
					lerror = (true);
				}
				break;

		}  // of the switch statement

	}  // of an UNK event.


	if( lerror )
	{
		// 2/3/2017 rmd : Okay well the mode is no longer 'connecting'. And it shouldn't be 'ready'.
		// So set it to 'waiting to connect'. Doing so ensures when the connection timer fires and
		// we try to start a connection sequence we really to start it. If the mode were still
		// 'connecting' it is possible to not re-start the sequence.
		cics.mode = CI_MODE_waiting_to_connect;

		// 10/29/2016 rmd : Send the connection error event. To start the sequence over.
		CONTROLLER_INITIATED_post_event( CI_EVENT_connection_process_error );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void LR_FREEWAVE_extract_changes_from_guivars( void )
{
	snprintf( LR_state->lrs_struct->modem_mode, sizeof(LR_state->lrs_struct->modem_mode), "%d", GuiVar_LRMode );

	snprintf( LR_state->network_group_id, sizeof(LR_state->network_group_id), "%d", GuiVar_LRGroup );

	snprintf( LR_state->lrs_struct->transmit_rate, sizeof(LR_state->lrs_struct->transmit_rate), "%d", GuiVar_LRBeaconRate );

	if( GuiVar_LRMode == LR_MODE_MASTER )
	{
		strlcpy( LR_state->lrs_struct->subnet_rcv_id, "0", sizeof(LR_state->lrs_struct->subnet_rcv_id) );
		strlcpy( LR_state->lrs_struct->subnet_xmt_id, "0", sizeof(LR_state->lrs_struct->subnet_xmt_id) );
	}
	else if( GuiVar_LRMode == LR_MODE_REPEATER )
	{
		strlcpy( LR_state->lrs_struct->subnet_rcv_id, "0", sizeof(LR_state->lrs_struct->subnet_rcv_id) );
		strlcpy( LR_state->lrs_struct->subnet_xmt_id, "1", sizeof(LR_state->lrs_struct->subnet_xmt_id) );
	}
	else // GuiVar_LRMode == LR_MODE_SLAVE
	{
		snprintf( LR_state->lrs_struct->subnet_rcv_id, sizeof(LR_state->lrs_struct->subnet_rcv_id), "%d", GuiVar_LRSlaveLinksToRepeater );
		strlcpy( LR_state->lrs_struct->subnet_xmt_id, "F", sizeof(LR_state->lrs_struct->subnet_xmt_id) );
	}

	if( GuiVar_LRMode == LR_MODE_MASTER )
	{
		snprintf( LR_state->lrs_struct->number_of_repeaters, sizeof(LR_state->lrs_struct->number_of_repeaters), "%d", GuiVar_LRRepeaterInUse );
	}
	else
	{
		strlcpy( LR_state->lrs_struct->number_of_repeaters, "0", sizeof(LR_state->lrs_struct->number_of_repeaters) );
	}

	snprintf( LR_state->lrs_struct->xmit_freq, sizeof(LR_state->lrs_struct->xmit_freq), "%3d.%04d", GuiVar_LRFrequency_WholeNum, GuiVar_LRFrequency_Decimal );
	snprintf( LR_state->lrs_struct->recv_freq, sizeof(LR_state->lrs_struct->recv_freq), "%3d.%04d", GuiVar_LRFrequency_WholeNum, GuiVar_LRFrequency_Decimal );

	snprintf( LR_state->lrs_struct->rf_xmit_power, sizeof(LR_state->lrs_struct->rf_xmit_power), "%2d", GuiVar_LRTransmitPower );
}

/* ---------------------------------------------------------- */
extern void LR_FREEWAVE_copy_settings_into_guivars( void )
{
	// 9/15/2017 rmd : This function would logically be called after reading the paramemters
	// OUT OF THE RADIO, before the screen is drawn or redrawn. The read process takes place
	// upon screen entry, and AGAIN after the write process (we do a read after the write, so
	// the user can see what was written and maybe spy any errors). Therefore at the end of this
	// function is a good place to assess if the repeater setting needs to be saved to the
	// config file, and registration sent to the commserver.

	// ----------
	
	BOOL_32		save_config_file_and_send_registration_to_commserver;
	
	strlcpy( GuiVar_LRSerialNumber, LR_state->serial_number, sizeof(GuiVar_LRSerialNumber) );

	strlcpy( GuiVar_LRFirmwareVer, LR_state->sw_version, sizeof(GuiVar_LRFirmwareVer) );

	if( atoi(LR_state->lrs_struct->modem_mode) == LR_MODE_SLAVE )	// Slave
	{
		GuiVar_LRMode = LR_MODE_SLAVE;
	}
	else if( atoi(LR_state->lrs_struct->modem_mode) == LR_MODE_REPEATER )	// Repeater
	{
		GuiVar_LRMode = LR_MODE_REPEATER;
	}
	else
	{
		GuiVar_LRMode = LR_MODE_MASTER;
	}

	GuiVar_LRGroup = atoi(LR_state->network_group_id);

	if( (GuiVar_LRGroup < LR_GROUP_MIN) || (GuiVar_LRGroup > LR_GROUP_MAX) )
	{
		GuiVar_LRGroup = LR_GROUP_MIN;
	}

	GuiVar_LRBeaconRate = atoi(LR_state->lrs_struct->transmit_rate);

	GuiVar_LRSlaveLinksToRepeater = atoi(LR_state->lrs_struct->subnet_rcv_id);

	GuiVar_LRRepeaterInUse = atoi(LR_state->lrs_struct->number_of_repeaters);

	GuiVar_LRFrequency_WholeNum = (UNS_32)(atoi( strtok((char*)LR_state->lrs_struct->xmit_freq, ".") ));
	GuiVar_LRFrequency_Decimal = (UNS_32)(atoi( strtok(NULL, ".") ));

	// 4/26/2017 ajv : The LRS-455 supports 6.25 Khz steps. However, this creates a challenge
	// for us since we increment the value in 12.5 Khz steps. Therefore, round the value up to the
	// nearest 12.5 Khz step.
	if( GuiVar_LRFrequency_Decimal % 125 != 0 )
	{
		GuiVar_LRFrequency_Decimal = GuiVar_LRFrequency_Decimal - (GuiVar_LRFrequency_Decimal % 125) + 125;
	}

	GuiVar_LRTransmitPower = atoi(LR_state->lrs_struct->rf_xmit_power);
	
	// ----------
	
	save_config_file_and_send_registration_to_commserver = (false);
	
	// 9/15/2017 rmd : Note - the GuiVar_LRRepeaterInUse variable only takes on the values of 0
	// and 1, (false) and (true).
	if( comm_mngr.device_exchange_port == UPORT_A )
	{
		if( config_c.purchased_options.port_a_freewave_lr_set_for_repeater != GuiVar_LRRepeaterInUse )
		{
			config_c.purchased_options.port_a_freewave_lr_set_for_repeater = GuiVar_LRRepeaterInUse;
			
			save_config_file_and_send_registration_to_commserver = (true);
		}
	}
	else
	if( comm_mngr.device_exchange_port == UPORT_B )
	{
		if( config_c.purchased_options.port_b_freewave_lr_set_for_repeater != GuiVar_LRRepeaterInUse )
		{
			config_c.purchased_options.port_b_freewave_lr_set_for_repeater = GuiVar_LRRepeaterInUse;
			
			save_config_file_and_send_registration_to_commserver = (true);
		}
	}

	// 9/15/2017 rmd : If there were changes that require the config file to be saved and for us
	// to re-register with the commserver, do it.
	if( save_config_file_and_send_registration_to_commserver )
	{
		// 9/15/2017 rmd : Setup a request to send registration to the commserver.
		CONTROLLER_INITIATED_update_comm_server_registration_info();

		// 9/15/2017 rmd : This delay of 2 seconds isn't critical. Just picked 2 seconds.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_CONTROLLER_CONFIGURATION, 2 );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

