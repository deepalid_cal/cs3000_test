/*  file = device_LR_FREEWAVE.h                           ajv */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_DEVICE_LR_FREEWAVE_H
#define INC_DEVICE_LR_FREEWAVE_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/25/2017 ajv : LR State Machine States.
#define LR_DEVEX_READ						(4000)
#define LR_DEVEX_WRITE						(5000)
#define LR_DEVEX_FINISH						(6000)

// ----------

// 3/16/2015 mpd : Use unique identifiers "R" and "W" to define communication operations.
#define LR_READ		( 0x52 )
#define LR_WRITE	( 0x57 )

// ----------

// 6/1/2015 mpd : A 1-second COMM_MNGR timer was adequate during development of the
// FreeWave SR radios. Circumstances in the field have left support personnel stuck as
// devices timeout under different conditions. The LR timeout has been increased as a
// precaution even though no LR devices have experienced this problem.
#define LR_COMM_TIMER_MS		(10000)

// ----------

#define NULL_TERM_LEN (1)

// ----------

#define LR_IDLE_OPERATION					( 73 )		// "I"
#define LR_READ_OPERATION					( 82 )		// "R"
#define LR_WRITE_OPERATION					( 87 )		// "W"

// ----------

// 4/8/2015 mpd : MENU navigaion commands to send to the LR radio. These are direct ASCII except for the END flag.
#define LR_MENU_CMD_STR_0			            ( 0x30 )
#define LR_MENU_CMD_STR_1			            ( '1' )
#define LR_MENU_CMD_STR_2			            ( '2' )
#define LR_MENU_CMD_STR_3			            ( '3' )
#define LR_MENU_CMD_STR_4			            ( '4' )
#define LR_MENU_CMD_STR_5			            ( '5' )
#define LR_MENU_CMD_STR_6			            ( '6' )
#define LR_MENU_CMD_STR_7			            ( '7' )
#define LR_MENU_CMD_STR_8			            ( '8' )
#define LR_MENU_CMD_STR_9			            ( '9' )
#define LR_MENU_CMD_STR_A			            ( 'A' )
#define LR_MENU_CMD_STR_B			            ( 'B' )
#define LR_MENU_CMD_STR_C			            ( 'C' )
#define LR_MENU_CMD_STR_D			            ( 'D' )
#define LR_MENU_CMD_STR_E			            ( 'E' )
#define LR_MENU_CMD_STR_F			            ( 'F' )
#define LR_MENU_CMD_STR_G			            ( 'G' )		// Yes, there is a "G" on some LR menus. This is ASCII, not hex!
#define LR_MENU_CMD_STR_ESC			            ( 0x1B )
#define LR_MENU_CMD_END							( 'Z' )		// This could be any value not used by the radio.
#define LR_MENU_CMD_LEN				            ( 1 )

// ----------

// 4/8/2015 mpd : TEXT commands to send to the LR radio. These defines are used by a get
// function to retrieve the specific ASCII text that needs to be sent to the LR radio.
#define LR_TEXT_CMD_NONE						( 5000 )
#define LR_TEXT_CMD_STR_MODE					( 6000 ) 
#define LR_TEXT_CMD_STR_BAUD_RATE               ( 6001 ) 
#define LR_TEXT_CMD_STR_FREQ_CHAN               ( 6002 ) 
#define LR_TEXT_CMD_STR_NUM_CHAN                ( 6003 ) 
#define LR_TEXT_CMD_STR_XMIT_CHAN               ( 6004 ) 
#define LR_TEXT_CMD_STR_RCV_CHAN                ( 6005 ) 
#define LR_TEXT_CMD_STR_MAX_PACKET              ( 6006 ) 
#define LR_TEXT_CMD_STR_MIN_PACKET              ( 6007 ) 
#define LR_TEXT_CMD_STR_XMIT_RATE               ( 6008 ) 
#define LR_TEXT_CMD_STR_DATA_RATE               ( 6009 ) 
#define LR_TEXT_CMD_STR_XMT_POWER               ( 6010 ) 
#define LR_TEXT_CMD_STR_RTS_TO_CTS              ( 6011 ) 
#define LR_TEXT_CMD_STR_RETRY_TIMEOUT           ( 6012 ) 
#define LR_TEXT_CMD_STR_LOWPOWER_MODE           ( 6013 ) 
#define LR_TEXT_CMD_STR_NUM_REPEATERS           ( 6014 ) 
#define LR_TEXT_CMD_STR_MASTER_PACKET_REPEAT    ( 6015 ) 
#define LR_TEXT_CMD_STR_MAX_SLAVE_RETRY         ( 6016 )  
#define LR_TEXT_CMD_STR_RETRY_ODDS              ( 6017 ) 
#define LR_TEXT_CMD_STR_NETWORK_ID              ( 6018 ) 
#define LR_TEXT_CMD_STR_SLAVE_REPEATER			( 6019 )
#define LR_TEXT_CMD_STR_DIAGNOSTICS				( 6020 )
#define LR_TEXT_CMD_STR_SUBNET_ID               ( 6021 )
#define LR_TEXT_CMD_STR_SUBNET_RCV_ID           ( 6022 )
#define LR_TEXT_CMD_STR_SUBNET_XMT_ID           ( 6023 )
#define LR_TEXT_CMD_STR_LAST	                ( LR_TEXT_CMD_STR_SUBNET_XMT_ID )
#define LR_TEXT_CMD_END							( 99999 )

// ----------

// 4/15/2015 mpd : Baud Rate strings.
#define LR_BAUD_RATE_CMD_9600	( "6" )
#define LR_BAUD_RATE_CMD_19200	( "5" )
#define LR_BAUD_RATE_CMD_38400	( "4" )
#define LR_BAUD_RATE_CMD_57600	( "3" )
#define LR_BAUD_RATE_CMD_76800	( "2" )
#define LR_BAUD_RATE_CMD_115200	( "1" )
#define LR_BAUD_RATE_CMD_230400	( "0" )
#define LR_BAUD_RATE_OUT_9600	( "009600" )
#define LR_BAUD_RATE_OUT_19200	( "019200" )
#define LR_BAUD_RATE_OUT_38400	( "038400" )
#define LR_BAUD_RATE_OUT_57600	( "057600" )
#define LR_BAUD_RATE_OUT_76800	( "076800" )
#define LR_BAUD_RATE_OUT_115200	( "115200" )
#define LR_BAUD_RATE_OUT_230400	( "230400" )

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*
								MAIN MENU
				  LRS455 Version 1.77     10-21-2014

						Modem Serial Number 459-5306
						  Model Code E4M1T
(0)   Set Operation Mode
(1)   Set Baud Rate
(2)   Edit Call Book
(3)   Edit Radio Transmission Characteristics
(4)   Show Radio Statistics
(5)   Edit MultiPoint Parameters
(6)   TDMA Menu
(8)   Chg Password
(Esc) Exit Setup

Enter Choice  
*/

#define LR_MAIN_MENU_VERSION_STR		"LRS455 Version"
#define LR_MAIN_MENU_VERSION_OFFSET		( 15 )
#define LR_MAIN_MENU_VERSION_LEN		( 6 + NULL_TERM_LEN )	// Currently only 4 in use

#define LR_MAIN_MENU_SERIAL_STR			"Modem Serial Number"
#define LR_MAIN_MENU_SERIAL_OFFSET		( 20 )
#define LR_MAIN_MENU_SERIAL_LEN			( 8 + NULL_TERM_LEN )

#define LR_MAIN_MENU_MODEL_STR  		"Model Code"
#define LR_MAIN_MENU_MODEL_OFFSET  		( 11 )
#define LR_MAIN_MENU_MODEL_LEN  		( 5 + NULL_TERM_LEN )

/* ---------------------------------------------------------- */
/*
								SET MODEM MODE
						Modem Mode is   3

(0)   Point to Point Master
(1)   Point to Point Slave
(2)   Point to MultiPoint Master
(3)   Point to MultiPoint Slave
(4)   Point to Point Slave/Repeater
(5)   Point to Point Repeater
(6)   Point to Point Slave/Master Switchable
(7)   Point to MultiPoint Repeater
(F)   Ethernet Options
(Esc) Exit to Main Menu

Enter Choice  
*/

#define LR_MODE_MENU_MODE_STR  			"Modem Mode is"
#define LR_MODE_MENU_MODE_OFFSET  		( 16 )
#define LR_MODE_MENU_MODE_LEN  			( 2 + NULL_TERM_LEN ) // Leave space for the "99" invalid value.

/* ---------------------------------------------------------- */
/*
								SET BAUD RATE
						Modem Baud is  057600

(0)   230,400 
(1)   115,200
(2)   76,800
(3)   57,600
(4)   38,400
(5)   19,200
(6)   9,600
(7)   4,800
(8)   2,400
(9)   1,200
(A)   Data, Parity  0
(B)   MODBus RTU    0
(C)   RS232/485     0
(D)   Setup Port    3
(E)   TurnOffDelay  0      TurnOnDelay   0
(F)   FlowControl   1
(G)   Use break to access setup          0
(Esc) Exit to Main Menu
Enter Choice  
*/

#define LR_BAUD_MENU_RATE_STR  				"Modem Baud is"
#define LR_BAUD_MENU_RATE_OFFSET  			( 15 )
#define LR_BAUD_MENU_RATE_LEN  				( 6 + NULL_TERM_LEN )

// 4/16/2015 mpd : The command to set the baud rate is completely separate from the display
// size.
#define LR_BAUD_RATE_COMMAND_LEN 			( 1 + NULL_TERM_LEN )

/* ---------------------------------------------------------- */
/*
								RADIO PARAMETERS

WARNING: Do not change parameters without reading manual

(0)   FreqKey        Single Channel
(1)   Max Packet Size  8
(2)   Min Packet Size  4
(3)   Xmit Rate        9
(4)   RF Data Rate     5
(5)   RF Xmit Power    0
(6)   Slave Security   0
(7)   RTS to CTS       0
(8)   Retry Time Out  32
(9)   Lowpower Mode    0
(A)   High Noise       0
(B)
(C)   RemoteLED        0
(Esc) Exit to Main Menu

Enter Choice  
*/

#define LR_RADIO_MENU_MAX_PACKET_STR  		"Max Packet Size"
#define LR_RADIO_MENU_MAX_PACKET_OFFSET 	( 17 )
#define LR_RADIO_MENU_MAX_PACKET_LEN  		( 1 + NULL_TERM_LEN )

#define LR_RADIO_MENU_MIN_PACKET_STR  		"Min Packet Size"
#define LR_RADIO_MENU_MIN_PACKET_OFFSET 	( 17 )
#define LR_RADIO_MENU_MIN_PACKET_LEN  		( 1 + NULL_TERM_LEN )

#define LR_RADIO_MENU_XMIT_RATE_STR  		"Xmit Rate"
#define LR_RADIO_MENU_XMIT_RATE_OFFSET	 	( 17 )
#define LR_RADIO_MENU_XMIT_RATE_LEN  		( 1 + NULL_TERM_LEN )

#define LR_RADIO_MENU_XMT_POWER_STR  		"RF Xmit Power"
#define LR_RADIO_MENU_XMT_POWER_OFFSET	 	( 16 )
#define LR_RADIO_MENU_XMT_POWER_LEN  		( 4 + NULL_TERM_LEN ) // Leave space for the "9999" invalid value.
#define LR_RADIO_MENU_XMT_POWER_V_LEN  		( 2 )				  // The number of characters to verify after a WRITE.

/* ---------------------------------------------------------- */
/*
								MULTIPOINT PARAMETERS


(0)   Number Repeaters      0
(1)   Master Packet Repeat  1
(2)   Max Slave Retry       8
(3)   Retry Odds            0
(4)   DTR Connect           0
(5)   Repeater Frequency    0
(6)   NetWork ID         0320
(7)   Reserved 
(8)   MultiMasterSync       0
(9)   1 PPS Enable/Delay  255
(A)   Slave/Repeater        0
(B)   Diagnostics           5
(C)   SubNet ID          Rcv=0 Xmit=F
(D)   Radio ID           Not Set 
(E)   Local Access         0
(F)
(G)   Radio Name         
(Esc) Exit to Main Menu
 
	  01234567890123456789012345678901234567890	<- Ruler 
 
Enter Choice  
*/

#define LR_MULTIPOINT_MENU_NUM_REPEATERS_STR  			"Number Repeaters"
#define LR_MULTIPOINT_MENU_NUM_REPEATERS_OFFSET  		( 21 )
#define LR_MULTIPOINT_MENU_NUM_REPEATERS_LEN  			( 2 + NULL_TERM_LEN )

#define LR_MULTIPOINT_MENU_NETWORK_ID_STR  				"NetWork ID"
#define LR_MULTIPOINT_MENU_NETWORK_ID_OFFSET  			( 19 )
#define LR_MULTIPOINT_MENU_NETWORK_ID_LEN  				( 4 + NULL_TERM_LEN )

#define LR_MULTIPOINT_MENU_SUBNET_ID_STR  				"SubNet ID"
#define LR_MULTIPOINT_MENU_SUBNET_ID_OFFSET 	 		( 19 )				
#define LR_MULTIPOINT_MENU_SUBNET_ID_LEN  				( 8 + NULL_TERM_LEN )	

#define LR_MULTIPOINT_MENU_SUBNET_DISABLED_STR 			"Disabled"				// Special case for SubnetID Rcv=F, Xmt=F
#define LR_MULTIPOINT_MENU_SUBNET_DISABLED_LEN 			( 8 )					// Special case for SubnetID Rcv=F, Xmt=F
#define LR_MULTIPOINT_MENU_SUBNET_ROAMING_STR  			"Roaming"				// Special case for SubnetID Rcv=0, Xmt=0
#define LR_MULTIPOINT_MENU_SUBNET_ROAMING_LEN  			( 7 )					// Special case for SubnetID Rcv=0, Xmt=0

#define LR_MULTIPOINT_MENU_SUBNET_RCV_ID_STR  			"Rcv="					// Does not exist if DISABLED or ROAMING
#define LR_MULTIPOINT_MENU_SUBNET_RCV_ID_OFFSET 		( 4 )					
#define LR_MULTIPOINT_MENU_SUBNET_RCV_ID_LEN  			( 2 + NULL_TERM_LEN )	// Leave space for the "99" invalid value.
#define LR_MULTIPOINT_MENU_SUBNET_RCV_ID_V_LEN  		( 1 )					// The number of characters to verify after a WRITE.

#define LR_MULTIPOINT_MENU_SUBNET_XMT_ID_STR  			"Xmit="					// Does not exist if DISABLED or ROAMING
#define LR_MULTIPOINT_MENU_SUBNET_XMT_ID_OFFSET 		( 5 )					
#define LR_MULTIPOINT_MENU_SUBNET_XMT_ID_LEN  			( 2 + NULL_TERM_LEN )	// Leave space for the "99" invalid value.
#define LR_MULTIPOINT_MENU_SUBNET_XMT_ID_V_LEN  		( 1 )					// The number of characters to verify after a WRITE.

// ----------

#define LR_EMPTY_STR									( 0xFF ) // Special case for initial I/O cycle. 
#define LR_EMPTY_STR_LEN								( 1 ) 	 // Length > 0 is required. 
#define LR_CRLF_STR										"\r\n"
#define LR_CRLF_STR_LEN									( 2 )
#define LR_LONGEST_COMMAND_LEN							( 20 + NULL_TERM_LEN )	

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/13/2015 mpd : These are not derived from LR responses.
#define LR_NETWORK_GROUP_ID_LEN 						( 2 + NULL_TERM_LEN )

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// SET MODEM MODE
	char modem_mode[ LR_MODE_MENU_MODE_LEN ];

	// SET BAUD RATE
	char baud_rate[ LR_BAUD_MENU_RATE_LEN ];

	// RADIO PARAMETER
	char xmit_freq[ 10 ];
	
	char recv_freq[ 10 ];

	char max_packet_size[ LR_RADIO_MENU_MAX_PACKET_LEN ];

	char min_packet_size[ LR_RADIO_MENU_MIN_PACKET_LEN ];

	char transmit_rate[ LR_RADIO_MENU_XMIT_RATE_LEN ];

	char rf_xmit_power[ LR_RADIO_MENU_XMT_POWER_LEN ];

	// MULTIPOINT PARAMETERS
	char number_of_repeaters[ LR_MULTIPOINT_MENU_NUM_REPEATERS_LEN ];

	char network_id[ LR_MULTIPOINT_MENU_NETWORK_ID_LEN ];

	char subnet_id[ LR_MULTIPOINT_MENU_SUBNET_ID_LEN ];			// This is read-only, but needs to be with Rcv and Xmt for decisions.
																
	char subnet_rcv_id[ LR_MULTIPOINT_MENU_SUBNET_RCV_ID_LEN ];	// Alternate of ROAMING and DISABLED, requires special handling

	char subnet_xmt_id[ LR_MULTIPOINT_MENU_SUBNET_XMT_ID_LEN ];	// Alternate of ROAMING and DISABLED, requires special handling

} LR_DETAILS_STRUCT_LRS;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	UNS_32 operation;

	UNS_32 read_list_index;

	UNS_32 write_list_index;

	// 3/23/2015 mpd : Centralize the response buffer pointer to make "mem_free()" easier.
	char  *resp_ptr;

	UNS_32 resp_len;

	// 3/20/2015 mpd : Pointers to the programmable variables structs.
	LR_DETAILS_STRUCT_LRS *lrs_struct;

	// 3/19/2015 mpd : These are read-only radio parameters. They are here to avoid 3 copies.
	char sw_version[ LR_MAIN_MENU_VERSION_LEN ];

	char serial_number[ LR_MAIN_MENU_SERIAL_LEN ];

	char model[ LR_MAIN_MENU_MODEL_LEN ];

	// 3/19/2015 mpd : These are Calsense defined groups.
	UNS_32 group_index;

	char network_group_id[ LR_NETWORK_GROUP_ID_LEN ];

} LR_STATE_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern BOOL_32	LR_PROGRAMMING_querying_device;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void LR_FREEWAVE_post_device_query_to_queue( const UNS_32 pport, const UNS_32 poperation );

// ----------

extern void LR_FREEWAVE_power_control( const UNS_32 pport, const BOOL_32 pon_or_off );

extern void LR_FREEWAVE_exchange_processing( void *pq_msg );

extern void LR_FREEWAVE_initialize_device_exchange( void );

// ----------

extern void LR_FREEWAVE_initialize_the_connection_process( UNS_32 pport );

extern void LR_FREEWAVE_connection_processing( UNS_32 pevent );

// ----------

extern void LR_FREEWAVE_extract_changes_from_guivars( void );

extern void LR_FREEWAVE_copy_settings_into_guivars( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

