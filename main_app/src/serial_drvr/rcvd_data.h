/*  file = rcvd_data.h                       2009.06.23  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_RCVD_DATA_H
#define _INC_RCVD_DATA_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"general_picked_support.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/16/2015 mpd : HUNT MODE - Calsense supports a variety of third-party communication devices on the CS3000 serial
// ports. The data stream received on the serial ports is processed by functions in "rcvd_data". Since communication
// protocols vary between devices and manufacturers, "rcvd_data" functions vary in how they handle incoming data. The
// primary issue: What constitutes the end of a data stream? HUNT MODES are a generic mechanism that allows device
// handlers to specify what "rcvd_data" should search for in the incoming stream. When "rcvd_data" successfully matches
// the stream data to the search criteria, it instructs "comm_mngr" to wake the specific device handler through the
// device exchange callback function. The hunt is unsuccessful if the device exchange timer expires before the desired
// data is recognized. New hunt modes are added as necessary for new communication protocols.
// 
// PORT_HUNT_FOR_PACKETS: This is the default hunt mode. It allows the reception of raw data packets separated by
// pre-amble and post-amble delimiters. Once the end of a packet is identified, the ambles are stripped, and memory is
// allocated for the packet data using the "dh.dptr" (DATA_HANDLE pointer). The packet is then routed to a destination
// based on "routing_class". The function "Route_Incoming_Packet()" copies the packet, notifies the destination, and
// then frees the dynamic memory allocated within "rcvd_data".
// 
// PORT_HUNT_FOR_DATA: This mode transfers raw data from a source port to a destination port. The destination port is
// responsible for freeing dynamic memory.
// 
// PORT_HUNT_FOR_SPECIFIED_STRING: This mode searches the input stream for a specific string. The string to search for
// is specified in the "sh" structure. In the current implementation, the specified string will only match if it begins
// at the start of the stream. A "COMM_MNGR_EVENT_isp_expected_response_arrived" event is returned to the device handler
// upon a match. There is no memory to release since no data is returned to the device handler.
// 
// PORT_HUNT_FOR_CRLF_DELIMITED_STRING: This mode searches for a beginning CRLF and an end CRLF. Once the second CRLF is
// identified, the CRLF delimiters are stripped, and memory is allocated for the raw data string between them using the
// "dh.dptr" (DATA_HANDLE pointer). A "COMM_MNGR_EVENT_device_exchange_response_arrived" event is returned to the device
// handler upon a match. The device handler is responsible for freeing dynamic memory.
// 
// PORT_HUNT_FOR_SPECIFIED_TERMINATION: This mode searches the input stream for a specific string to signal the
// termination of the buffer. The string to search for is specified in the "th" structure. There can be any number of
// CRLF characters in the buffer before the termination string. Once the string is identified, memory is allocated for
// the entire buffer (including the termination string) using the "dh.dptr" (DATA_HANDLE pointer). A
// "COMM_MNGR_EVENT_device_exchange_response_arrived" event is returned to the device handler upon a match. The device
// handler is responsible for freeing dynamic memory.

#define PORT_HUNT_FOR_PACKETS			(100)

#define PORT_HUNT_FOR_DATA				(200)

// 3/13/2014 rmd : Meaning reutrn a queue message when a specified string is located.
#define PORT_HUNT_FOR_SPECIFIED_STRING	(300)

// 3/13/2014 rmd : Meaning find the string that is preceeded by CR LF. And terminated by CR
// LF. Grab a memory block for it. And return it via queue message.
#define PORT_HUNT_FOR_CRLF_DELIMITED_STRING	(400)


// 3/10/2015 mpd : Return a queue message when a buffer is available that is terminated by a specified string.
#define PORT_HUNT_FOR_SPECIFIED_TERMINATION	(500)


// 10/26/2016 rmd : Add task names as necessary. Used only when hunting for a specified
// string. To indicate which task gets notified.
#define		NOT_STRING_HUNTING										(0)

#define		WHEN_STRING_FOUND_NOTIFY_THE_COMM_MNGR_TASK				(100)

#define		WHEN_STRING_FOUND_NOTIFY_THE_CI_TASK					(200)

#define		WHEN_STRING_FOUND_NOTIFY_THE_CODE_DISTRIBUTION_TASK		(300)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


extern void RCVD_DATA_enable_hunting_mode( UNS_32 pport, UNS_32 prequested_hunt, UNS_32	pwho_to_notify );


extern void ring_buffer_analysis_task( void *pvParameters );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

