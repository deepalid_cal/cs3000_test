/*  file = device_EN_UDS1100.c                06.28.2013  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/12/2015 mpd : Required for isalnum
#include	<ctype.h>

// 5/5/2015 mpd : Required for atoi
#include	<stdlib.h>

#include	<string.h>

#include	"device_common.h"

#include	"device_EN_PremierWaveXE.h"

#include	"device_EN_UDS1100.h"

#include	"device_GR_PremierWaveXC.h"

#include	"device_WEN_PremierWaveXN.h"

#include	"device_WEN_WiBox.h"

#include	"gpio_setup.h"

#include	"alerts.h"

#include	"serial.h"

#include	"configuration_controller.h"

#include	"serport_drvr.h"

#include	"device_generic_gr_card.h"

#include	"speaker.h"

#include	"comm_mngr.h"

#include	"controller_initiated.h"

#include	"app_startup.h"

#include	"rcvd_data.h"

#include	"cs_mem.h"

#include	"epson_rx_8025sa.h"

#include	"e_keyboard.h"

#include	"combobox.h"

#include	"e_wen_wifi_settings.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//#define DEV_DEBUG_ANALYSIS		
//#define DEV_DEBUG_E_SHARED
//#define DEV_DEBUG_EXTRACTION
//#define DEV_DEBUG_GET_TEXT
//#define DEV_DEBUG_HUNT		
//#define DEV_FIELD_DEBUGGING_ALERTS
//#define DEV_DEBUG_NET_WAIT
//#define DEV_DEBUG_PROCESS_LIST
//#define DEV_DEBUG_STATE
//#define DEV_DEBUG_STRIP_CRLF		
//#define DEV_DEBUG_VERSIONS
//#define DEV_DEBUG_MEM_MALLOC 
//#define DEV_VERBOSE_ALERTS 

// ----------

#define DEV_ERROR_LEVEL_INFO							( 1 )
#define DEV_ERROR_LEVEL_SERIOUS							( 2 )
#define DEV_ERROR_LEVEL_FATAL							( 3 )

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// The "DEV_STATE_STRUCT" struct contains fields that apply to all radio types. Each radio type will have a detail
// struct referenced by "dev_state->type_details". These structs contain fields unique to the radio type.  
DEV_STATE_STRUCT dev_state_struct;
DEV_STATE_STRUCT *dev_state = &dev_state_struct;

// The "DEV_DETAILS_STRUCT" struct contains fields that apply to both GR and WEN devices. 
DEV_DETAILS_STRUCT dev_details;

// 6/10/2015 mpd : Forward declarations needed by device_handler[].
static void dev_enter_device_mode( DEV_STATE_STRUCT *dev_state );
static void dev_exit_device_mode();
static void dev_set_read_operation( DEV_STATE_STRUCT *dev_state, UNS_32 operation );
static void dev_set_write_operation( DEV_STATE_STRUCT *dev_state );
static void dev_state_machine( DEV_STATE_STRUCT *dev_state, COMM_MNGR_TASK_QUEUE_STRUCT *pq_msg );
 
// 6/17/2015 mpd : Forward declarations needed by dev_get_command_text().
static void dev_increment_list_item_pointer( DEV_STATE_STRUCT *dev_state, const UNS_32 count );

// 5/19/2015 mpd : TBD: This table may go away if the functions can be made generic. 
static const DEVICE_HANDLER_STRUCT device_handler[ NUMBER_OF_DEFINED_DEVICES ] =
{
    // FW Ver1,2,3,4,  command handler,      enter,                 exit,                 read,                   write,                   initialize,                   state machine
																															                    
	// NONE INSTALLED                                                                                                                      
	{ 0, 0, 0, "",     NULL,                 NULL,                  NULL,                 NULL,                   NULL,                    NULL,                         NULL }, 
																																					
	// Raveon FireLine or StingRay                                                                                                                                      
	{ 0, 0, 0, "",     NULL,                 NULL,                  NULL,                 NULL,                   NULL,                    NULL,                         NULL }, 
																																					
	// Freewave LRS455                                                                                                                                                  
	{ 0, 0, 0, "",     NULL,                 NULL,                  NULL,                 NULL,                   NULL,                    NULL,                         NULL }, 
																																					
	// Freewave FGR2                                                                                                                                                    
	{ 0, 0, 0, "",     NULL,                 NULL,                  NULL,                 NULL,                   NULL,                    NULL,                         NULL }, 
																															
	// Lantronix UDS1100-IAP                                                                                                               
	{ 6, 1, 0, "3",    dev_get_command_text, dev_enter_device_mode, dev_exit_device_mode, dev_set_read_operation, dev_set_write_operation, en_initialize_detail_struct,  dev_state_machine }, 

	// Sierra Wireless AirLink LS300
	{ 0, 0, 0, "",     NULL,                 NULL,                  NULL, 				  NULL,                   NULL,                    NULL,                         NULL }, 

	// Lantronix PremierWave XN
	{ 7, 9, 0, "0R19", dev_get_command_text, dev_enter_device_mode, dev_exit_device_mode, dev_set_read_operation, dev_set_write_operation, wen_initialize_detail_struct, dev_state_machine }, 

	// Lantronix PremierWave XC HSPA+
	{ 7, 9, 0, "0R11", dev_get_command_text, dev_enter_device_mode, dev_exit_device_mode, dev_set_read_operation, dev_set_write_operation, gr_initialize_detail_struct,  dev_state_machine },

	// Lantronix PremierWave XE
	{ 7,10, 0, "0T8",  dev_get_command_text, dev_enter_device_mode, dev_exit_device_mode, dev_set_read_operation, dev_set_write_operation, PW_XE_initialize_detail_struct, dev_state_machine }, 

	// Lantronix WiBox
	{ 6,7, 0, "0",     dev_get_command_text, dev_enter_device_mode, dev_exit_device_mode, dev_set_read_operation, dev_set_write_operation, WIBOX_initialize_detail_struct, dev_state_machine }, 

};

#ifdef DEV_DEBUG_VERSIONS
// 5/8/2015 mpd : Use a counter for testing versions.
UNS_32 dev_version_debug_count = 0;
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  			       Common GUI Functions					  */
/* ---------------------------------------------------------- */

// 5/27/2015 mpd : Functions and defines in this section should eventually move to "e_SHARED_programming.c" which 
// does not exist at the moment.

static UNS_32 common_PROGRAMMING_seconds_since_reboot = 0;

static DATE_TIME common_PROGRAMMING_reboot_time;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void FDTO_e_SHARED_show_obtain_ip_automatically_dropdown( void )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( 217, 39, GuiVar_ENObtainIPAutomatically );
}

/* ---------------------------------------------------------- */
extern void FDTO_e_SHARED_show_subnet_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxENSubnetMask_0, &FDTO_COMBOBOX_add_items, (EN_PROGRAMMING_NETMASK_MAX + 1), GuiVar_ENNetmask );
}

/* ---------------------------------------------------------- */
extern void e_SHARED_show_keyboard( const UNS_32 px_coordinate, const UNS_32 py_coordinate, GuiConst_TEXT *guivar_ptr, const UNS_32 pmax_string_len, UNS_32 *current_cp, const UNS_32 pkeyboard_type, BOOL_32 show_current_value )
{
	// 6/15/2015 ajv : Play a good key beep when the keyboard is displayed from this routine.
	good_key_beep();

	if( show_current_value )
	{
		// 6/2/2015 mpd : The keyboard routines always use GuiVar_GroupName. Therefore, copy the contents of the current
		// text item into this variable first. We'll extract it back out when we're done.
		strlcpy( GuiVar_GroupName, guivar_ptr, pmax_string_len );
	}
	else
	{
		strlcpy( GuiVar_GroupName, "", pmax_string_len );
	}

	// 6/2/2015 mpd : Blank out the item so it is no longer visible on the screen.
	memset( guivar_ptr, 0x00, pmax_string_len );

	// 6/2/2015 mpd : Save the cursor position so we know where to copy GuiVar_GroupName. 
	*current_cp = GuiLib_ActiveCursorFieldNo;

	KEYBOARD_draw_keyboard( px_coordinate, py_coordinate, pmax_string_len, pkeyboard_type );
}

/* ---------------------------------------------------------- */
extern const char *e_SHARED_get_easyGUI_string_at_index( const UNS_32 pstructure_index_0, const UNS_32 pindex )
{
    #if 0
        Usage Example:
        temp_str = e_SHARED_get_easyGUI_string_at_index( GuiStruct_itxtWENSecurity_0, 2 ); // 2 should return "WPA"
    #endif

    char *rv;

    rv = GuiLib_GetTextLanguagePtr( (UNS_16)(pstructure_index_0 + pindex), 0, GuiConst_LANGUAGE_ENGLISH );

    return( rv );
}

/* ---------------------------------------------------------- */
extern INT_32 e_SHARED_get_index_of_easyGUI_string( const char *pstr_to_find, const UNS_32 pstr_len, const UNS_32 pstructure_index_0, const UNS_32 pmin_index, const UNS_32 pmax_index )
{
    #if 0
        Usage Example:
        const char temp_str = "WEP"); // should return 1
        index = e_SHARED_get_index_of_easyGUI_string( temp_str, strlen(temp_str), GuiStruct_itxtWENSecurity_0, WEN_WIRELESS_SECURITY_MIN, WEN_WIRELESS_SECURITY_MAX );
    #endif

    INT_32  rv;
    UNS_32  i;
	char  temp_text[ DEV_LONGEST_FIELD_LEN ];

    // 5/29/2015 ajv : Set the return value to something that will always be invalid in case the
    // string can't be found.
    rv = -1;

	// 5/29/2015 mpd : Strings used to program the device will have a CR before the null-termination. That won't match
	// strings in the GuiLib. Replace the first CR with a null-terminator.
	strlcpy( temp_text, pstr_to_find, sizeof( temp_text ) );
	if( strchr( temp_text, '\r' ) != NULL )
	{
		memset( strchr( temp_text, '\r' ), 0x00, 1 );
	}

    for( i = pmin_index; i <= pmax_index; ++i )
    {
        if( strncmp( temp_text, GuiLib_GetTextLanguagePtr((UNS_16)(pstructure_index_0 + i), 0, GuiConst_LANGUAGE_ENGLISH), pstr_len ) == 0 )
        {
            rv = i;

            break;
        }
    }

	#ifdef DEV_DEBUG_E_SHARED
		if( rv == -1 )
		{
			Alert_Message_va( "GUI String: %s (struct %d)", temp_text, pstructure_index_0 );
			for( i = pmin_index; i <= pmax_index; ++i )
			{
				Alert_Message_va( "    GUI[%d]: %s", i, GuiLib_GetTextLanguagePtr( (UNS_16)( pstructure_index_0 + i ), 0, GuiConst_LANGUAGE_ENGLISH) );
			}
		}
	#endif

	return( rv );
}

/* ---------------------------------------------------------- */
extern void e_SHARED_start_device_communication( const UNS_32 port, UNS_32 operation, BOOL_32 *querying_device )
{
	*querying_device = true;

	// 3/18/2014 rmd : Post the event to cause the comm_mngr to perform the device settings
	// readout and write the results to each of the guivars.

	COMM_MNGR_TASK_QUEUE_STRUCT	cmqs;
	
	if( operation == DEV_WRITE_OPERATION )
	{
		cmqs.event = COMM_MNGR_EVENT_request_write_device_settings;
		//Alert_Message( "GUI: WRITE sent to COMM_MNGR" );
	}
	else if( operation == DEV_READ_MONITOR_OPERATION )
	{
		cmqs.event = COMM_MNGR_EVENT_request_read_monitor_device_settings;
		//Alert_Message( "GUI: READ MONITOR sent to COMM_MNGR" );
	}
	else
	{
		// 3/16/2015 mpd : Default to READ.
		cmqs.event = COMM_MNGR_EVENT_request_read_device_settings;
		//Alert_Message( "GUI: READ sent to COMM_MNGR" );
	}

	// 5/27/2015 mpd : The input param and "cmqs" use "UPORT_A" and "UPORT_B" defines.
	cmqs.port = port;

	COMM_MNGR_post_event_with_details( &cmqs );
	
	// 5/15/2015 mpd : READ and WRITE power-cycle the device to enter CLI mode. There will be a significant 
	// delay before the network reconnects and we can successfully query network settings. Track the time here 
	// on the GUI side. Don't allow the user to read network settings until the waiting period ends or the commands
	// will timeout.
	EPSON_obtain_latest_time_and_date( &common_PROGRAMMING_reboot_time );
	common_PROGRAMMING_seconds_since_reboot = 0;

}
	
/* ---------------------------------------------------------- */
extern BOOL_32 e_SHARED_network_connect_delay_is_over( const UNS_32 seconds_to_wait )
{
	BOOL_32 rv = false;
	DATE_TIME current_time;

	//Alert_Message( "e_SHARED_network_connect_delay_is_over" );

	EPSON_obtain_latest_time_and_date( &current_time );
	common_PROGRAMMING_seconds_since_reboot = current_time.T - common_PROGRAMMING_reboot_time.T;

	if( common_PROGRAMMING_seconds_since_reboot >= seconds_to_wait )
	{
		rv = true;
	}

	// 5/15/2015 mpd : DEBUG CODE. This works with KEY_PLUS. 
	//snprintf( GuiVar_CommOptionProgressText, sizeof( GuiVar_CommOptionProgressText ), "Reboot Timer(%d): %d", rv, common_PROGRAMMING_seconds_since_reboot );

	return( rv );
}

/* ---------------------------------------------------------- */
extern void e_SHARED_get_info_text_from_programming_struct()
{
	//Alert_Message( "e_SHARED_get_info_text_from_programming_struct" );

	strlcpy( GuiVar_CommOptionInfoText, dev_state->info_text, sizeof( GuiVar_CommOptionInfoText ) );

}

/* ---------------------------------------------------------- */
extern void e_SHARED_string_validation( char *target_string, UNS_32 string_len )
{
	//Alert_Message( "e_SHARED_string_validation" );

	if( strlen( target_string ) == 0 )
	{
		if( string_len > strlen( DEV_NOT_SET_STR ) )
		{
			strlcpy( target_string, DEV_NOT_SET_STR, string_len );
		}
		else
		{
			strlcpy( target_string, "NA", string_len );
		}
	}

}

/* ---------------------------------------------------------- */
extern UNS_32 e_SHARED_index_keycode_for_gui( const UNS_32 pkeycode )
{
	//Alert_Message( "e_SHARED_index_keycode_for_gui" );

	// 6/1/2015 mpd : Device exchange keycodes currently range from 0x9000 - 0x9006. EasyGUI doesn't support indexes 
	// above 99. Return a 0 - 6 index.
	return( pkeycode - DEVICE_EXCHANGE_KEY_MIN );
}

/* ---------------------------------------------------------- */
/*  			 String Extraction and Analysis 			  */
/* ---------------------------------------------------------- */

#ifdef DEV_DEBUG_VERSIONS

// 6/11/2015 mpd : NOTE: This code was written during GR development and expects a FW version of "7.9.0.3A2" in the
//  "device_handler" table as the current device minimum requirement. 
void dev_exercise_version_analysis( DEV_STATE_STRUCT *dev_state )
{
	switch( dev_version_debug_count )
	{
		case 0:
			strlcpy( dev_state->firmware_version, "7.9.0.3A2", sizeof( dev_state->firmware_version ) );		// Equal
			break;
		case 1:
			strlcpy( dev_state->firmware_version, "7.0.0.0", sizeof( dev_state->firmware_version ) );		// Down-Rev
			break;
		case 2:
			strlcpy( dev_state->firmware_version, "6.9.0.3A2", sizeof( dev_state->firmware_version ) );		// Down-Rev
			break;
		case 3:
			strlcpy( dev_state->firmware_version, "7.8.0.3A2", sizeof( dev_state->firmware_version ) );		// Down-Rev
			break;
		case 4:
			strlcpy( dev_state->firmware_version, "7.9.0.2A2", sizeof( dev_state->firmware_version ) );		// Down-Rev
			break;
		case 5:
			strlcpy( dev_state->firmware_version, "7.9.0.3A", sizeof( dev_state->firmware_version ) );		// Down-Rev
			break;
		case 6:
			strlcpy( dev_state->firmware_version, "7.9.0.3A1", sizeof( dev_state->firmware_version ) );		// Down-Rev
			break;
		case 7:
			strlcpy( dev_state->firmware_version, "7.10.0.3A2", sizeof( dev_state->firmware_version ) );	// Up-Rev
			break;
		case 8:
			strlcpy( dev_state->firmware_version, "7.9.1.3A2", sizeof( dev_state->firmware_version ) );		// Up-Rev
			break;
		case 9:
			strlcpy( dev_state->firmware_version, "7.9.0.3B2", sizeof( dev_state->firmware_version ) );		// Up-Rev
			break;
		case 10:
			strlcpy( dev_state->firmware_version, "7.9.0.3A3", sizeof( dev_state->firmware_version ) );		// Up-Rev
			break;
		case 11:
			strlcpy( dev_state->firmware_version, "7.9.0.3A21", sizeof( dev_state->firmware_version ) );	// Up-Rev
			break;
		case 12:
			strlcpy( dev_state->firmware_version, "8.0.0.0", sizeof( dev_state->firmware_version ) );		// Up-Rev
			break;
		case 13:
			strlcpy( dev_state->firmware_version, "8.9.0.3A2", sizeof( dev_state->firmware_version ) );		// Up-Rev
			break;
		default:
			break;
	}
	dev_version_debug_count++;

}

#endif // #ifdef DEV_DEBUG_VERSIONS

/* ---------------------------------------------------------- */
static void dev_handle_error( DEV_STATE_STRUCT *dev_state, const UNS_32 error, const UNS_32 severity, char *error_text )
{
	// 6/8/2015 mpd : Errors are not always fatal. Only save the first error if execution is continuing. 
	if( dev_state->error_code == DEV_ERROR_NONE )
	{
		// 6/8/2015 mpd : Save the error code and text.
		dev_state->error_code = DEV_ERROR_EXECUTING_COMMAND;
		dev_state->error_severity = severity;
		strlcpy( dev_state->error_text, error_text, sizeof( dev_state->error_text ) );

		// 6/22/2015 mpd : There are multiple READ operation defines. Test for the single WRITE define.
		// 6/8/2015 mpd : Inform the user. 
		if( dev_state->operation == DEV_WRITE_OPERATION )
		{
			dev_update_info( dev_state, error_text, DEVICE_EXCHANGE_KEY_write_settings_in_progress );
		}
		else
		{
			dev_update_info( dev_state, error_text, DEVICE_EXCHANGE_KEY_read_settings_in_progress );
		}
	}

	// 6/8/2015 mpd : Always log an alert message.
	#ifdef DEV_VERBOSE_ALERTS
		Alert_Message_va( "ERR %d: %s", error, error_text );
	#else
		Alert_Message_va( "ERR %d", error );
	#endif

}

extern void dev_analyze_xcr_dump_device( DEV_STATE_STRUCT *dev_state )
{
	char *temp_ptr;

	#ifdef DEV_DEBUG_ANALYSIS
	Alert_Message( "dev_analyze_xcr_dump_device" );
	#endif

	// 4/16/2015 mpd : Send progress info to the GUI.
	dev_update_info( dev_state, "Reading device info...", DEVICE_EXCHANGE_KEY_read_settings_in_progress );

	// 5/15/2015 mpd : Start at the "long name" configitem.
	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_X_DUMP_LNAME_ITEM_STR, dev_state->resp_len ) ) != NULL )
	{
		// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
		dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_DUMP_LNAME_STR, DEV_X_DUMP_LNAME_OFFSET, sizeof( dev_state->model ), dev_state->model, "MODEL" );
	}
	// 6/12/2015 mpd : GR has been known to fail the XCR DUMP command immediately after power-cycling. Delay timing is 
	// being addressed. This code addresses the prolem if the delay is not long enough.
	else if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_X_CP_DUMP_FAILURE_STR, dev_state->resp_len ) ) != NULL )
	{
		dev_handle_error( dev_state, DEV_ERROR_XCR_DUMP_DEVICE, DEV_ERROR_LEVEL_FATAL, "Device read failed. Retry." );
	}

	// 5/28/2015 mpd : The serial number (MAC address) for GR is available in the XML dump. WEN needs the "eth0" and 
	// "wlan0" MAC addresses from the ENABLE SHOW response.
	if( dev_state->device_type == COMM_DEVICE_GR_PREMIERWAVE )
	{
		// 5/15/2015 mpd : Go to the "serial number" configitem.
		if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_X_DUMP_S_NUM_ITEM_STR, dev_state->resp_len ) ) != NULL )
		{
			// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_DUMP_VALUE_STR, DEV_X_DUMP_VALUE_OFFSET, sizeof( dev_state->serial_number ), dev_state->serial_number, "SER NUM" );
		}
	}

	// 5/15/2015 mpd : Go to the "firmware version" configitem.
	if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_X_DUMP_FW_VER_ITEM_STR, dev_state->resp_len ) ) != NULL )
	{
		// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
		dev_extract_delimited_text_from_buffer( temp_ptr, DEV_X_DUMP_DELIMITER, DEV_X_DUMP_VALUE_STR, DEV_X_DUMP_VALUE_OFFSET, sizeof( dev_state->firmware_version ), dev_state->firmware_version, "FW VER" );
	}

	#ifdef DEV_DEBUG_VERSIONS
		dev_exercise_version_analysis( dev_state );
	#endif

}

// 5/15/2015 mpd : This is a cut down version of the ENABLE SHOW analysis function that was created for GR 
// PremierWaveXC. It applies to GR and WEN PremierWaveXN. That's why it is here in device common functions. It does not 
// apply to other device types. 
extern void dev_analyze_enable_show_ip( DEV_STATE_STRUCT *dev_state )
{
	char *temp_ptr;
	DEV_DETAILS_STRUCT *dev_details = dev_state->dev_details;
	char *p;

	#ifdef DEV_DEBUG_ANALYSIS
	Alert_Message( "dev_analyze_enable_show_ip" );
	#endif

	if( dev_state->device_type == COMM_DEVICE_GR_PREMIERWAVE )
	{
		// 4/23/2015 mpd : The GR PremierWave response has duplicate key words for IP Address. Analyze from the "Packet 
		// Domain Status:" line to extract the one associated with "wwan0". Without this, we get the "eth0" IP address.
		if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_E_SHOW_WWAN0_STR, dev_state->resp_len ) ) != NULL )
		{
			dev_extract_text_from_buffer( temp_ptr, DEV_E_SHOW_IP_ADD_STR, DEV_E_SHOW_IP_ADD_OFFSET, sizeof( dev_details->ip_address ), dev_details->ip_address, "IP ADD" );
		}
		else
		{
			strlcpy( dev_details->ip_address, DEV_NULL_IP_ADD_STR, sizeof( dev_details->ip_address ) );

			#ifdef DEV_VERBOSE_ALERTS
				Alert_Message( "Analyze IP address failure" );
			#else
				Alert_Message( "DEV1" );
			#endif
		}
	}
	else if( (dev_state->device_type == COMM_DEVICE_WEN_PREMIERWAVE) || (dev_state->device_type == COMM_DEVICE_EN_PREMIERWAVE) )
	{
		// 5/28/2015 mpd : The GR the serial number (MAC address) from the XML dump. Get the WEN "eth0" and "wlan0" MAC 
		// addresses here.

		// 5/28/2015 mpd : Use the "dev_state->serial_number" field for the WEN etho MAC address which Lantronix uses 
		// for the serial number. "Jump to the "Interface: eth0" section to get that first address.
		if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_E_SHOW_ETH0_STR, dev_state->resp_len ) ) != NULL )
		{
			// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
			dev_extract_delimited_text_from_buffer( temp_ptr, DEV_CR_STR, DEV_E_SHOW_MAC_ADD_STR, DEV_E_SHOW_MAC_ADD_OFFSET, sizeof( dev_state->serial_number ), dev_state->serial_number, "ETH0 MAC" );
		}

		// 6/22/2015 mpd : Use upper case to be consistent across all device types and extraction methods.
		for( p = dev_state->serial_number; *p != '\0'; ++p )
		{
			*p = ( char )toupper( *p );
		}

		if( dev_state->device_type == COMM_DEVICE_WEN_PREMIERWAVE )
		{
			// 5/28/2015 mpd : Jump to the "Interface: wlan0" section.
			if( ( temp_ptr = find_string_in_block( dev_state->resp_ptr, DEV_E_SHOW_WLAN0_STR, dev_state->resp_len ) ) != NULL )
			{
				// 4/22/2015 mpd : This field is of varying length. Use extraction with a delimiter.
				dev_extract_delimited_text_from_buffer( temp_ptr, DEV_CR_STR, DEV_E_SHOW_MAC_ADD_STR, DEV_E_SHOW_MAC_ADD_OFFSET, sizeof( wen_details.mac_address ), wen_details.mac_address, "WLAN0 MAC" );

				// 6/22/2015 mpd : Use upper case to be consistent across all device types and extraction methods.
				for( p = wen_details.mac_address; *p != '\0'; ++p )
				{
					*p = ( char )toupper( *p );
				}
			}
		}
	}

}

extern void dev_analyze_firmware_version( DEV_STATE_STRUCT *dev_state )
{
	char current_version[ DEV_FW_VER_LEN + 1 ];
	char section_4[ DEV_FW_VER_4_LEN ];
	char *current_token;
	char error_text[DEV_INFO_TEXT_LEN];
	UNS_32 i;
	BOOL_32 newer_version = false;

	#ifdef DEV_DEBUG_ANALYSIS
		Alert_Message( "dev_analyze_firmware_version" );
	#endif

	dev_state->acceptable_version = true; 

	// 5/5/2015 mpd : Make a local copy of the address so "strtok()" can play with it. Add a terminating "." or 
	// the last token will be skipped. This was already added to the minimum FW string in the define.
	strlcpy( current_version, dev_state->firmware_version, sizeof( current_version ) );
	strlcat( current_version, ".", sizeof( current_version ) );
	current_token =  strtok( current_version, "." );

	// 6/10/2015 mpd : EN adds a "V" to the first section "V6.1.0.3". Skip it to keep the section  all numeric.
	if( strchr( current_token, 'V' ) != NULL )
	{
		current_token++;
	}

	// 5/5/2015 mpd : The first 3 sections of the firmware are simple decimal numbers. Test them left-to-right for
	// proper column weight.
	if( device_handler[ dev_state->device_type ].fw_ver_1 != atoi( current_token ) )
	{
		if( device_handler[ dev_state->device_type ].fw_ver_1 > atoi( current_token ) )
		{
			dev_state->acceptable_version = false; 
		}
		else
		{
			newer_version = true; 
		}
	}
	else
	{
		// 5/8/2015 mpd : First section is equal. Test section 2.
		current_token =  strtok( NULL, "." );

		if( device_handler[ dev_state->device_type ].fw_ver_2 != atoi( current_token ) )
		{
			if( device_handler[ dev_state->device_type ].fw_ver_2 > atoi( current_token ) )
			{
				dev_state->acceptable_version = false; 
			}
			else
			{
				newer_version = true; 
			}
		}
		else
		{
			// 5/8/2015 mpd : First and second sections are equal. Test section 3.
			current_token =  strtok( NULL, "." );

			if( device_handler[ dev_state->device_type ].fw_ver_3 != atoi( current_token ) )
			{
				if( device_handler[ dev_state->device_type ].fw_ver_3 > atoi( current_token ) )
				{
					dev_state->acceptable_version = false; 
				}
				else
				{
					newer_version = true; 
				}
			}
		}
	}

	// 5/7/2015 mpd : Skip testing the last field if we already know the answer after testing the first 3 sections. 
	if( ( dev_state->acceptable_version ) && ( !newer_version ) )
	{
		strlcpy( section_4, device_handler[ dev_state->device_type ].fw_ver_4, sizeof( section_4 ) );

		// 5/8/2015 mpd : The first 3 sections are equal. Test section 4.
		current_token =  strtok( NULL, "." );

		// 5/5/2015 mpd : The last token can be difficult. Values such as "4", "3B", and "3A3" are greater than "3A2". 
		// Compare using the length of the minimum version. 
		for( i = 0; i < strlen( section_4 ); ++i )
		{
			if( current_token[i] != '\0' )
			{
				if( (UNS_32)current_token[i] > (UNS_32)section_4[i] )
				{
					// 5/5/2015 mpd : This character in the current token is larger than the minimum. No more searching is 
					// necessary. Exit the loop.
					break;; 
				}
				else if( (UNS_32)section_4[i] > (UNS_32)current_token[i] )
				{
					// 5/5/2015 mpd : This character in the current token is less than than the minimum. The current 
					// version is too old.
					dev_state->acceptable_version = false; 
					break;
				}
				// 5/5/2015 mpd : Implied ELSE. The characters at this position match. Keep looking until we run out of 
				// minimum version string characters (any additional characters in the current token indicate a newer 
				// version which is acceptable). Reaching the end of the string with no mismatch is a valid way out of this
				// loop.
			}
			else
			{
				// 5/5/2015 mpd : We hit the end of the current version token before it was identified as being equal or 
				// greater than the minimum. The current version is too old.
				dev_state->acceptable_version = false; 
				break;
			}
		}
	}

	if( !dev_state->acceptable_version )
	{
		// 6/11/2015 mpd : The dialog box is small. Conserve text to fit in all of the required FW version.
		//snprintf( error_text, sizeof( error_text ), "FW Version %s is below minimum %d.%d.%d.%s", 
		snprintf( error_text, sizeof( error_text ), "FW Ver: %s. Needed: %d.%d.%d.%s", 
				  dev_state->firmware_version, 
				  device_handler[ dev_state->device_type ].fw_ver_1, 
				  device_handler[ dev_state->device_type ].fw_ver_2, 
				  device_handler[ dev_state->device_type ].fw_ver_3, 
				  section_4 );

		dev_handle_error( dev_state, DEV_ERROR_OLD_FIRMWARE, DEV_ERROR_LEVEL_INFO, error_text );
	}

}

extern void dev_strip_crlf_characters( char *dest_ptr, char *description )
{
	// 5/1/2015 mpd : If a CR exists in the destination string, replace it with a null-termination.
	if( strchr( dest_ptr, '\r' ) != NULL )
	{
		memset( strchr( dest_ptr, '\r' ), 0x00, 1 );
	}

	// 5/1/2015 mpd : If a LF exists in the destination string, replace it with a null-termination. This is very 
	// unlikely since we just shortened the string if there was a CR.
	if( strchr( dest_ptr, '\n' ) != NULL )
	{
		memset( strchr( dest_ptr, '\n' ), 0x00, 1 );
	}

	#ifdef DEV_DEBUG_STRIP_CRLF
		Alert_Message_va( "%s ->%s<", description, dest_ptr );
	#endif

}

extern void dev_extract_text_from_buffer( const char *response_buffer, const char *anchor_text, UNS_32 text_offset, size_t text_len, char *dest_ptr, char *description )
{
	char *temp_ptr;

	//Alert_Message( "dev_extract_text_from_buffer" );

	// 3/11/2015 mpd : Clear the destination. 
	memset( dest_ptr, 0x00, text_len );

	if( ( temp_ptr = strstr( response_buffer, anchor_text ) ) != NULL )
	{
		// 4/22/2015 mpd : We found the anchor text. Point beyond that to the text we care about.
		temp_ptr = ( temp_ptr + text_offset );

		// 3/11/2015 mpd : The "temp_ptr" now points to the text we want. Copy every character for "text_len".
		strlcpy( dest_ptr, (char *)( temp_ptr ), text_len );

		// 5/1/2015 mpd : If a CR exists in the destination string, replace it with a null-termination.
		if( strchr( dest_ptr, '\r' ) != NULL )
		{
			memset( strchr( dest_ptr, '\r' ), 0x00, 1 );
		}

		// 5/1/2015 mpd : If a LF exists in the destination string, replace it with a null-termination. This is very 
		// unlikely since we just shortened the string if there was a CR.
		if( strchr( dest_ptr, '\n' ) != NULL )
		{
			memset( strchr( dest_ptr, '\n' ), 0x00, 1 );
		}

		#ifdef DEV_DEBUG_EXTRACTION
			Alert_Message_va( "%s ->%s<", description, dest_ptr );
		#endif
	}
	else
	{
		#ifdef DEV_VERBOSE_ALERTS
			Alert_Message( "Extract: Anchor text not found" );
		#else
			Alert_Message( "DEV2" );
		#endif
	}

}

extern void dev_extract_delimited_text_from_buffer( const char *response_buffer, const char *delimiter, const char *anchor_text, UNS_32 text_offset, size_t max_text_len, char *dest_ptr, char *description )
{
	char *temp_ptr;
	char *delimiter_ptr;
	//Alert_Message( "dev_extract_delimited_text_from_buffer" );

	// 4/22/2015 mpd : The Lantronix devices place multiple fields on a single response line with characters delimiting
	// them. Fields can also be of variable length such as the IP address which can range from "0.0.0.0" to
	// "000.000.000.000". This function copies the max text length into the buffer and then replaces the delimiter with
	// a null-termination. CR or LF will count as delimiters if the requested delimiter is not found in the maximum
	// copy length. It no delimiter, CR, or LF is found. the copied characters are replaced with nulls and an alert
	// message is posted.

	// 3/11/2015 mpd : Clear the destination.
	memset( dest_ptr, 0x00, max_text_len );

	if( ( temp_ptr = strstr( response_buffer, anchor_text ) ) != NULL )
	{
		// 4/22/2015 mpd : We found the anchor text. Point beyond that to the text we care about.
		temp_ptr = ( temp_ptr + text_offset );

		// 4/30/2015 mpd : Copy the maximum number of characters. 
		strlcpy( dest_ptr, (char *)( temp_ptr ), max_text_len );

		// 4/22/2015 mpd : Look for the delimiter in the destination.
		if( ( delimiter_ptr = strstr( dest_ptr, delimiter ) ) == NULL )
		{
			// 4/22/2015 mpd : No delimiter found. Did the line end? Accept CR as the delimiter. 
			if( ( delimiter_ptr = strstr( dest_ptr, DEV_CR_STR ) ) == NULL )
			{
				// 4/22/2015 mpd : No delimiter found. Accept LR as the delimiter. 
				if( ( delimiter_ptr = strstr( dest_ptr, DEV_LF_STR ) ) == NULL )
				{
					// 4/22/2015 mpd : No delimiter found. Was it overwritten by the null at the end of the copy? Check
					// the source. 
					if( ( * ( (char*)( temp_ptr + max_text_len - DEV_NULL_LEN ) ) ) == *delimiter )
					{
						// 5/18/2015 mpd : The delimiter was overwritten. Set the pointer to where it was and continue.
						delimiter_ptr = ( dest_ptr + max_text_len - DEV_NULL_LEN );
					}
				}
			}
		}

		// 4/22/2015 mpd : Did we find a delimiter?
		if( delimiter_ptr != NULL )
		{
			// 4/30/2015 mpd : Replace the delimiter with a null-termination.
			memset( delimiter_ptr, 0x00, 1 );

			#ifdef DEV_DEBUG_EXTRACTION
				Alert_Message_va( "%s ->%s<", description, dest_ptr );
			#endif
		}
		else
		{
			// 3/11/2015 mpd : No delimiter found. Clear the destination.
			memset( dest_ptr, 0x00, max_text_len );

			#ifdef DEV_VERBOSE_ALERTS
				Alert_Message_va( "Extract Failure: No delimiter (%s)", description );
			#else
				Alert_Message( "DEV3" );
			#endif
		}
	}
	else
	{
		Alert_Message_va( "Extract Failure: Missing anchor text - %s", description );
	}

}

extern void dev_extract_ip_octets( const char *ip_address, char *ip_address_1, char *ip_address_2, char *ip_address_3, char *ip_address_4 )
{
	char local_ip_address[ DEV_IP_ADD_LEN + 1 ];

	//Alert_Message( "dev_extract_ip_octets" );

	// 4/22/2015 mpd : Clear the destinations.
	memset( ip_address_1, 0x00, DEV_IP_ADDRESS_OCTET_LEN );
	memset( ip_address_2, 0x00, DEV_IP_ADDRESS_OCTET_LEN );
	memset( ip_address_3, 0x00, DEV_IP_ADDRESS_OCTET_LEN );
	memset( ip_address_4, 0x00, DEV_IP_ADDRESS_OCTET_LEN );

	// 4/22/2015 mpd : Make a local copy of the address so "strtok()" can play with it. Add a terminating "." or 
	// the last octet will be skipped. 
	strlcpy( local_ip_address, ip_address, sizeof( local_ip_address ) );
	strlcat( local_ip_address, ".", ( DEV_IP_ADD_LEN + 1 ) );

	// 4/22/2015 mpd : Copy each octet. 
	strlcpy( ip_address_1, strtok( local_ip_address, "." ), sizeof( ip_address_1 ) );
	strlcpy( ip_address_2, strtok( NULL,      		 "." ), sizeof( ip_address_2 ) );
	strlcpy( ip_address_3, strtok( NULL,             "." ), sizeof( ip_address_3 ) );
	strlcpy( ip_address_4, strtok( NULL,             "." ), sizeof( ip_address_4 ) );

	// 4/22/2015 mpd : Sanity check.
	if( ( ip_address_1[0] == 0x00 ) ||
		( ip_address_2[0] == 0x00 ) ||
		( ip_address_3[0] == 0x00 ) ||
		( ip_address_4[0] == 0x00 ) )
	{
		#ifdef DEV_VERBOSE_ALERTS
			Alert_Message( "IP octet extraction failed" );
		#else
			Alert_Message( "DEV5" );
		#endif
	}
}

// 9/24/2015 mpd : This function returns the number of ( true ) bits in the mask string. 
extern UNS_32 dev_count_true_mask_bits( const char *mask_string )
{
	UNS_32 bit_count = 0;
	char local_mask_string[ DEV_IP_ADD_LEN + 1 ];
	UNS_32 octet_count;
	UNS_32 octet_value;

	//Alert_Message( "dev_count_mask_bits" );

	// 4/22/2015 mpd : Make a local copy of the mask so "strtok()" can play with it. Add a terminating "." or 
	// the last octet will be skipped. 
	strlcpy( local_mask_string, mask_string, sizeof( local_mask_string ) );
	strlcat( local_mask_string, ".", ( DEV_IP_ADD_LEN + 1 ) );

	octet_value = atoi( strtok( local_mask_string, "." ) );

	// 9/24/2015 mpd : This will not be called frequently. Use brute force instead of researching elegant.
	for( octet_count = 0; octet_count < 4; ++octet_count )
	{
		switch( octet_value )
		{
			// 9/24/2015 mpd : There are intentionally no "break" statements until the end.
			case 255:
				bit_count++;
			case 254:
				bit_count++;
			case 252:
				bit_count++;
			case 248:
				bit_count++;
			case 240:
				bit_count++;
			case 224:
				bit_count++;
			case 192:
				bit_count++;
			case 128:
				bit_count++;
			case 0:
			default:
				break;
		}
		octet_value = atoi( strtok( NULL, "." ) );
	}

	return( bit_count );

}

// 3/23/2015 mpd : This function is called at the end of READ and WRITE cycles. This function gives the final
// stamp of approval for the specific operation.
extern void dev_final_device_analysis( DEV_STATE_STRUCT *dev_state )
{
	DEV_DETAILS_STRUCT *dev_details = dev_state->dev_details;

	#ifdef DEV_DEBUG_ANALYSIS
	Alert_Message( "dev_final_device_analysis" );
	#endif

	if( dev_state->read_after_a_write )
	{
		// 5/7/2015 mpd : This READ operation followed a WRITE. Handle the programming flag.

		// 6/22/2015 mpd : For GR devices, there is currently no WRITE operation. This block will not be useful until 
		// the diagnostic screen is functional. For WEN devices, we display the programmed values on the screen and let 
		// the user look for differences. NOTE: There are too few of these sold to justify all the code required for 
		// verification of every field written to the device.  
		dev_state->programming_successful = true;
	}
	else
	{
		// 5/7/2015 mpd : This is a stand-alone READ operation. It did not follow a WRITE.  Handle the settings valid flag.

		// 5/1/2015 mpd : Assume the best.
		dev_state->device_settings_are_valid = true;

		// 6/22/2015 mpd : We don't program GR devices in the field. Check the APN field as a sanity check to be sure
		// this device was programmed at Calsense.
		if( dev_state->device_type == COMM_DEVICE_GR_PREMIERWAVE )
		{
			if( strstr( dev_details->apn, DEV_TEXT_CMD_APN ) != NULL )
			{
				dev_state->programming_successful = false;
				#ifdef DEV_VERBOSE_ALERTS
					Alert_Message( "APN is invalid" );
				#else
					Alert_Message( "DEV6" );
				#endif
			}
			else
			{
				dev_state->programming_successful = true;
			}
		}

		#if 0
		// 5/5/2015 mpd : Just detect known field values for now. Do something with these in the future.
		if( strstr( dev_details->ip_address, DEV_E_SHOW_IP_NOT_SET_STR ) != NULL )
		{
			dev_state->device_settings_are_valid = false;
			#ifdef DEV_VERBOSE_ALERTS
				Alert_Message( "IP Address: None" );
			#else
				Alert_Message( "DEV7" );
			#endif
		}

		// 5/5/2015 mpd : Just detect known field values for now. Do something with these in the future.
		if( strstr( dev_details->sim_status, DEV_C_SHOW_CONN_ERR_STR ) != NULL )
		{
			dev_state->device_settings_are_valid = false;
			#ifdef DEV_VERBOSE_ALERTS
				Alert_Message( "SIM Status: Connection Error" );
			#else
				Alert_Message( "DEV8" );
			#endif
		}

		if( strstr( dev_details->network_status, DEV_C_SHOW_NO_SIM_STR ) != NULL )
		{
			dev_state->device_settings_are_valid = false;
			#ifdef DEV_VERBOSE_ALERTS
				Alert_Message( "Network Status: No SIM" );
			#else
				Alert_Message( "DEV9" );
			#endif
		}

		if( strstr( dev_details->packet_domain_status, DEV_C_SHOW_NO_SIM_STR ) != NULL )
		{
			dev_state->device_settings_are_valid = false;
			#ifdef DEV_VERBOSE_ALERTS
				Alert_Message( "GPRS Status: No SIM" );
			#else
				Alert_Message( "DEV10" );
			#endif
		}

		if( strstr( dev_details->signal_strength, DEV_C_SHOW_NO_SIM_STR ) != NULL )
		{
			dev_state->device_settings_are_valid = false;
			#ifdef DEV_VERBOSE_ALERTS
				Alert_Message( "RSSI Status: No SIM" );
			#else
				Alert_Message( "DEV11" );
			#endif
		}
		#endif

		// 6/22/2015 mpd : There is no mimimum WEN firmware version at this time.
		if( (dev_state->device_type == COMM_DEVICE_GR_PREMIERWAVE) || (dev_state->device_type == COMM_DEVICE_EN_PREMIERWAVE) )
		{
			dev_analyze_firmware_version( dev_state );

			if( !dev_state->acceptable_version )
			{
				dev_state->device_settings_are_valid = false;
			}
		}
	}

}

/* ---------------------------------------------------------- */
/*  			  Functions That Span All Menu Items		  */
/* ---------------------------------------------------------- */

#if 0
extern void dev_wait_for_network( DEV_STATE_STRUCT *dev_state )
{
	// 5/8/2015 mpd : With a SIM card in place, any command that involves WWAN0 info will timeout unless we have waited
	// 30 seconds after the power-cycle for the host to connect us to the network. COMM_MNGR can't wait 30+ seconds for
	// a response, but our state machine can send dummy commands until the network connects. The 
	// "DEV_WAITING_FOR_NETWORK" state temporarily takes us out of "DEV_DEVEX_READ". NOTE: As a response handler, this 
	// function will be called every time a response is received from the dummy command. 

	#ifdef DEV_DEBUG_NET_WAIT
		Alert_Message( "dev_wait_for_network" );
	#endif

	comm_mngr.device_exchange_state = DEV_WAITING_FOR_NETWORK;

}
#endif

// 6/11/2015 mpd : This function has been replaced by the "DEV_COMMAND_CR_DELAY" command.
#if 0
extern void dev_wait_for_ready( DEV_STATE_STRUCT *dev_state )
{
	// 6/5/2015 mpd : The device responds with a double "##" for the first few seconds after power up. No commands were 
	// sent during this period when we waited 60 seconds for a network connection. Now that the network delay is gone,
	// commands sent during the first second do not always respond. 

	#ifdef DEV_DEBUG_NET_WAIT
		Alert_Message( "dev_wait_for_ready" );
	#endif

	// 6/5/2015 mpd : The double "##" within the first second seems to be a PuTTY quirk. It does not exist in the 
	// response buffer, so we can't test responses until it goes away. It does not appear on a direct PuTTY to CLI 
	// connection on a null modem cable.

	// 6/5/2015 mpd : A state was written to loop until receiving a single "#" prompt. 
	//comm_mngr.device_exchange_state = DEV_WAITING_FOR_READY;

	// 6/5/2015 mpd : The controller is apparently sending the first command too soon. Give the device a moment to 
	// stabilize. Two seconds has been long enough for successful command responses. There are still a few extra "#"
	// characters seen in PuTTY.
	vTaskDelay( MS_to_TICKS( 2000 ) );

} 
#endif
 
extern void dev_device_read_progress( DEV_STATE_STRUCT *dev_state )
{
	// 4/16/2015 mpd : Send progress info to the GUI.
	dev_update_info( dev_state, "Reading device settings...", DEVICE_EXCHANGE_KEY_read_settings_in_progress );

}

extern void dev_device_write_progress( DEV_STATE_STRUCT *dev_state )
{
	// 4/16/2015 mpd : Send progress info to the GUI.
	dev_update_info( dev_state, "Writing device settings...", DEVICE_EXCHANGE_KEY_write_settings_in_progress );

}

// 6/18/2015 mpd : The WEN #xcr dump "wlan profile:default_infrastructure_profile" response includes HTML coding for 
// five special characters. This would only be seen in the SSID (network name), User Name, and Credentials. The Password
// Passphrase, and Key fields cannot be read, so they are not an issue. 
// A translation function is a future option. Since we can get a clean SSID using a SHOW command in the profile
// "(config-profile-basic:default_infrastructure_profile)#show", and Credentials needs to follow Linux naming 
// requirements, only the User Name could present a problem.
#if 0
static void translate_html_numbers_in_string( const char *source, char * destination )
{
	// "&" -> &amp;#38;
	// "'" -> &amp;#39;
	// "<" -> &amp;#60;
	// ">" -> &amp;#62;
	// """ -> &amp;#34;

}
#endif
 
static void dev_command_escape_char_handler( char *source_ptr, char *destination_ptr, const UNS_32 buffer_size )
{
	// 6/16/2015 mpd : Keys, Passwords, Passphrases, and Credentials can contain white space and/or control characters.
	// The device requires quotes around white space. Control characters will require escape sequences. This function
	// provides both. NOTE: The opening command is not included since it could also include white space. Only pass
	// the string that follows the reserved command word(s).

	// 6/16/2015 mpd : Do a worst-case size calculation to verify destination space or we will have to account for every
	// byte of data we add. Every character could require an escape character, plus opening/closing quotes and a CR.
	if( buffer_size >= ( ( strlen( source_ptr ) * 2 ) + 3 )  )
	{
		// 6/16/2015 mpd : Clear the destination. 
		memset( destination_ptr, 0x00, buffer_size );

		// 6/16/2015 mpd : Put an opening quote in the destination buffer. Bump the pointer for the next character.
		memcpy( destination_ptr, "\"", sizeof( char ) );
		destination_ptr++;

		// 6/16/2015 mpd : Loop through every character until the string ends. 
		while( *source_ptr != NULL )
		{
			// 6/16/2015 mpd : Handle the easy ones first. All alpha and numeric characters go directly into the 
			// destination.
			if( isalnum( *source_ptr ) )
			{
				memcpy( destination_ptr, ( char* )source_ptr, sizeof( char ) );
			}
			else
			{
				// 6/16/2015 mpd : Alphanumerics have been handled. Standard C escape sequences are handled here. Deal 
				// with any ASCII exceptions to the default handling as they crop up.
				switch( ( UNS_8 )*source_ptr )
				{
					// 6/18/2015 mpd : The WEN device accepts all characters except the single quote without escape
					// characters. The XCR dump we use to read the name is not as clean.
					// #network name "!@#$%^&*()_+-={}|[]\:;'<>?,./`~\""
					// <value name = "network name">!@#$%^&amp;#38;*()_+-={}|[]\:;&amp;#39;&amp;#60;&amp;#62;?,./`~&amp;#34;</value>
					// "&" -> &amp;#38;
					// "'" -> &amp;#39;
					// "<" -> &amp;#60;
					// ">" -> &amp;#62;
					// """ -> &amp;#34;

 					//case 0x07:	// Alarm (Beep, Bell) 
					//case 0x08:	// Backspace 
					//case 0x0C:	// Formfeed 
					//case 0x0A:	// Newline (Line Feed); see notes below 
					//case 0x0D:	// Carriage Return 
					//case 0x09:	// Horizontal Tab 
					//case 0x0B:	// Vertical Tab 
					//case 0x5C:	// Backslash 
					//case 0x27:	// Single quotation mark 
					case 0x22:	// Double quotation mark 
					//case 0x3F:	// Question mark 
						memcpy( destination_ptr, "\\", sizeof( char ) );
						destination_ptr++;
						memcpy( destination_ptr, ( char* )source_ptr, sizeof( char ) );
						break;

					// 6/16/2015 mpd : C defined octal and hex escape sequences are not being handled at this time.
					// \nnn any The character whose numerical value is given by nnn interpreted as an octal number 
					// \xhh any The character whose numerical value is given by hh interpreted as a hexadecimal number 

					// 6/16/2015 mpd : Most values such as space ' ', can go directly to the device with no special 
					// handling. That will be the default behavior.
					case ' ':
					default:
						memcpy( destination_ptr, ( char* )source_ptr, sizeof( char ) );
						break;
				}
			}
			// 6/16/2015 mpd : Single increments. Extra increments were already handled.
			source_ptr++;
			destination_ptr++;
		}

		// 6/16/2015 mpd : Put a closing quote and a NULL in the destination buffer.
		memcpy( destination_ptr, "\"", sizeof( char ) );
		memcpy( (destination_ptr + 1), 0x00, sizeof( char ) );
	}
	else
	{
		Alert_Message( "Inadequate string space" );
	}

}

// 5/19/2015 mpd : This function is common to GR and WEN.

// 4/8/2015 mpd : This function returns a pointer to an ASCII command string that will be sent to the device. Some
// commands are single ASCII characters, others are strings terminated with CR and LF. 
extern char *dev_get_command_text( DEV_STATE_STRUCT *dev_state, const UNS_32 command )
{
	char *command_ptr = NULL;
	WEN_DETAILS_STRUCT *pw_xn_details = dev_state->wen_details;
	EN_DETAILS_STRUCT  *uds1100_details  = dev_state->en_details;
	PW_XE_DETAILS_STRUCT *pw_xe_details = dev_state->PW_XE_details;
	WIBOX_DETAILS_STRUCT  *wibox_details  = dev_state->WIBOX_details;
	BOOL_32 send_command_to_device = true;
	BOOL_32 add_cr = false;

	// 4/8/2015 mpd : Create local spaces to store and append to command strings. 
	char command_buffer[ DEV_LONGEST_COMMAND_LEN ];
	char work_space[ DEV_LONGEST_COMMAND_LEN ];

	#ifdef DEV_DEBUG_GET_TEXT
		Alert_Message_va( "dev_get_command_text: %0X", command );
	#endif
 
	memset( command_buffer, 0x00, sizeof( command_buffer ) );

	// 6/11/2015 mpd : Most commands will respond. The exceptions will modify this variable.
	dev_state->command_will_respond = true;

	// 6/11/2015 mpd : Set the additional command separation value for the the majority of commands. The exceptions will 
	// modify this variable.
	dev_state->command_separation = DEV_COMMAND_SEPARATION_NO_EXTRA_TIMER_MS;

	switch( command )
	{
		case DEV_COMMAND_802_MODE:     // Set the mode. 
			strlcpy( command_buffer, "mode ", sizeof( command_buffer ) );
			strlcat( command_buffer, pw_xn_details->radio_mode, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_A_MODE:
			strlcpy( command_buffer, DEV_TEXT_CMD_A_MODE, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_ACCEPT:
			strlcpy( command_buffer, DEV_TEXT_CMD_ACCEPT, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_ADDRESS:
			strlcpy( command_buffer, DEV_TEXT_CMD_ADDRESS, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_ALIVE:
			strlcpy( command_buffer, DEV_TEXT_CMD_ALIVE, sizeof( command_buffer ) );
			if( dev_state->device_type == COMM_DEVICE_GR_PREMIERWAVE )
			{
				strlcat( command_buffer, DEV_TEXT_CMD_1200000, sizeof( command_buffer ) );
			}
			else if( (dev_state->device_type == COMM_DEVICE_WEN_PREMIERWAVE) || (dev_state->device_type == COMM_DEVICE_EN_PREMIERWAVE) )
			{
				strlcat( command_buffer, DEV_TEXT_CMD_60000, sizeof( command_buffer ) );
			}
			break;

		case DEV_COMMAND_ANTENNA:
			strlcpy( command_buffer, DEV_TEXT_CMD_ANTENNA, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_APN:
			strlcpy( command_buffer, DEV_TEXT_CMD_APN, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_BAUD:
			strlcpy( command_buffer, DEV_TEXT_CMD_BAUD, sizeof( command_buffer ) );
			dev_state->command_will_respond = false;
			break;

		case DEV_COMMAND_C_MODE:
			strlcpy( command_buffer, DEV_TEXT_CMD_C_MODE, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_CELL:
			strlcpy( command_buffer, DEV_TEXT_CMD_CELL, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_CLI:
			strlcpy( command_buffer, DEV_TEXT_CMD_CLI, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_CLOCK:
			strlcpy( command_buffer, DEV_TEXT_CMD_CLOCK, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_CLOCK_TZ:
			strlcpy( command_buffer, DEV_TEXT_CMD_CLOCK_TZ, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_CONFIG:
			strlcpy( command_buffer, DEV_TEXT_CMD_CONFIG, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_NAVIGATION_CMD;
			break;

		case DEV_COMMAND_CONNECT:
			strlcpy( command_buffer, DEV_TEXT_CMD_CONNECT, sizeof( command_buffer ) );
			break;

		case  DEV_COMMAND_CR_NR:
			// 5/22/2015 mpd : The EN needs a final CR entered to completely exit CLI mode. The CD signal will indicate
			// a disconnect. Do not "break" on this case. Continue on to DEV_COMMAND_CR;
			dev_state->command_will_respond = false;

		case DEV_COMMAND_CR:
			strlcpy( command_buffer, DEV_CR_STR, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_CR_DELAY:
			strlcpy( command_buffer, DEV_CR_STR, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_LONG_TIMER_MS;
			break;

		case DEV_COMMAND_D_MODE:
			strlcpy( command_buffer, DEV_TEXT_CMD_D_MODE, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_DHCP:
			// e.g. "dhcp enabled\r".
			strlcpy( command_buffer, DEV_TEXT_CMD_DHCP, sizeof( command_buffer ) );
			if( dev_state->dhcp_enabled )
			{
				strlcat( command_buffer, DEV_ENABLED_STR, sizeof( command_buffer ) );
			}
			else
			{
				strlcat( command_buffer, DEV_DISABLED_STR, sizeof( command_buffer ) );
			}
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_DIAG:
			strlcpy( command_buffer, DEV_TEXT_CMD_DIAG, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_DISCONNECT:
			strlcpy( command_buffer, DEV_TEXT_CMD_DISCONNECT, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_E_CCMP:       // Set CCMP Encriyption. 
			strlcpy( command_buffer, "encryption ccmp ", sizeof( command_buffer ) );
			if( pw_xn_details->wpa_encription_ccmp )
			{
				strlcat( command_buffer, DEV_ENABLED_STR, sizeof( command_buffer ) );
			}
			else
			{
				strlcat( command_buffer, DEV_DISABLED_STR, sizeof( command_buffer ) );
			}
			add_cr = true;
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			break;

		case DEV_COMMAND_E_TKIP:       // Set TKIP Encriyption. 
			strlcpy( command_buffer, "encryption tkip ", sizeof( command_buffer ) );
			if( pw_xn_details->wpa_encription_tkip )
			{
				strlcat( command_buffer, DEV_ENABLED_STR, sizeof( command_buffer ) );
			}
			else
			{
				strlcat( command_buffer, DEV_DISABLED_STR, sizeof( command_buffer ) );
			}
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_E_WEP:        // Set WEP Encriyption. 
			strlcpy( command_buffer, "encryption wep ", sizeof( command_buffer ) );
			if( pw_xn_details->wpa_encription_wep )
			{
				strlcat( command_buffer, DEV_ENABLED_STR, sizeof( command_buffer ) );
			}
			else
			{
				strlcat( command_buffer, DEV_DISABLED_STR, sizeof( command_buffer ) );
			}
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_EAP_TTLS:     // Set the EAP TTLS Option. 
			strlcpy( command_buffer, "eap-ttls option ", sizeof( command_buffer ) );
			strlcat( command_buffer, pw_xn_details->wpa_eap_ttls_option, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_ECHO:
			strlcpy( command_buffer, DEV_TEXT_CMD_ECHO, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_EDIT_DIP:     // Edit the default profiles. 
			strlcpy( command_buffer, DEV_TEXT_CMD_EDIT_DIP, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_NAVIGATION_CMD;
			break;

		case DEV_COMMAND_ENABLE:
			strlcpy( command_buffer, DEV_TEXT_CMD_ENABLE, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_NAVIGATION_CMD;
			break;

		case DEV_COMMAND_END:
			send_command_to_device = false;
			command_ptr = NULL;
			dev_state->command_will_respond = false;
			break;

		// 6/10/2015 mpd : Now using DEV_WAITING_FOR_RESPONSE for EN.
		//case  DEV_COMMAND_ENTER_MONITOR:
		//	strlcpy( command_buffer, DEV_ENTER_MODE_MONITOR_STR, sizeof( command_buffer ) );
		//	break;

		// 6/10/2015 mpd : Now using DEV_WAITING_FOR_RESPONSE for EN.
		//case  DEV_COMMAND_ENTER_SETUP:
		//	strlcpy( command_buffer, DEV_ENTER_MODE_SETUP_STR, sizeof( command_buffer ) );
		//	break;

		case DEV_COMMAND_ETLS_C:       // Set EAP TLS Credentials. 
			if( pw_xn_details->user_changed_credentials )
			{
				if( ( strlen( pw_xn_details->wpa_eap_tls_credentials ) ) && ( strncmp( pw_xn_details->wpa_eap_tls_credentials, DEV_NOT_SET_STR, sizeof( pw_xn_details->wpa_eap_tls_credentials ) ) != NULL ) )
				{
					strlcpy( command_buffer, "credentials ", sizeof( command_buffer ) );
					dev_command_escape_char_handler( pw_xn_details->wpa_eap_tls_credentials, work_space, sizeof( work_space ) );
					strlcat( command_buffer, work_space, sizeof( command_buffer ) );
				}
				else
				{
					strlcpy( command_buffer, "no credentials", sizeof( command_buffer ) );
				}
				add_cr = true;
			}
			else
			{
				// 6/15/2015 mpd : We are not changing this field. Send just a CR to skip doing anything destructive. 
				strlcpy( command_buffer, DEV_CR_STR, sizeof( command_buffer ) );
			}
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			break;

		case DEV_COMMAND_ETLS_V:       // Set EAP TLS Validate Certificate. 
			strlcpy( command_buffer, "validate certificate ", sizeof( command_buffer ) );
			if( pw_xn_details->wpa_eap_tls_valid_cert )
			{
				strlcat( command_buffer, DEV_ENABLED_STR, sizeof( command_buffer ) );
			}
			else
			{
				strlcat( command_buffer, DEV_DISABLED_STR, sizeof( command_buffer ) );
			}
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_EXIT_FINAL:
			// 5/14/2015 mpd : Exit commands return a response for all command levels except "enable" which is the final
			// exit from CLI mode. Don't expect a response from the final exit. Do not "break" on this case. Continue
			// on to DEV_COMMAND_EXIT;
			dev_state->command_will_respond = false;

		case DEV_COMMAND_EXIT:
			strlcpy( command_buffer, DEV_TEXT_CMD_EXIT, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_SHORT_TIMER_MS;
			break;

		case  DEV_COMMAND_EXIT_WO_SAVE:
			strlcpy( command_buffer, DEV_MENU_EXIT_WO_SAVE_STR, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_FLOW:
			strlcpy( command_buffer, DEV_TEXT_CMD_FLOW, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_PW_XN_GATEWAY:
			// 6/2/2015 mpd : e.g. "default gateway 192.168.254.250\r".
			strlcpy( command_buffer, DEV_TEXT_CMD_GATEWAY, sizeof( command_buffer ) );
			strlcat( command_buffer, pw_xn_details->gw_address, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			add_cr = true;
			break;

		case  DEV_COMMAND_GET_AUTO_BOOL:
			// 4/20/2015 mpd : This is always a "YES".
			strlcpy( command_buffer, DEV_TEXT_CMD_Y, sizeof( command_buffer ) );
			break;

		case  DEV_COMMAND_GET_BAUD_RATE:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->baud_rate, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->baud_rate, sizeof( command_buffer ) );
			}
			break;

		case  DEV_COMMAND_GET_CONN_MODE:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->conn_mode, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->conn_mode, sizeof( command_buffer ) );
			}
			break;

		case  DEV_COMMAND_GET_DHCP_NAME:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->dhcp_name, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->dhcp_name, sizeof( command_buffer ) );
			}

			// 4/27/2015 mpd : DHCP name is a dynamic value that does not include a CR like the rest of the EN text commands. Add a
			// CR after the name value. Note that adding a CRLF is incorrect and causes a jump to a subsequent field.
			add_cr = (true);
			break;

		case  DEV_COMMAND_GET_DHCP_NAME_BOOL:
			// 4/20/2015 mpd : Program the DHCP name if DHCP is enabled. The Calsense default name is the MAC address string.
			if( dev_state->dhcp_enabled )
			{
				strlcpy( command_buffer, DEV_TEXT_CMD_Y, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, DEV_TEXT_CMD_N, sizeof( command_buffer ) );

				// 6/12/2015 mpd : FUTURE: Both EN WRITE lists could be combined by using "dev_state->dhcp_enabled" and 
				// "dev_increment_list_item_pointer( dev_state )". We would skip this boolean response increment the 
				// list pointer +2 here to skip over DHCP name programming. This is a low priority. See 
			}
			break;

		case  DEV_COMMAND_GET_DISCONN_MODE:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->disconn_mode, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->disconn_mode, sizeof( command_buffer ) );
			}
			break;

		case  DEV_COMMAND_GET_DISCONN_TIME:
			// 4/20/2015 mpd : NOTE: Disconnect time is of the format "00:00". The ":" is provided by the device!!! This 
			// single line requires 2 inputs. Call this command twice from the WRITE list to ge the job done.
			strlcpy( command_buffer, DEV_TEXT_CMD_SU_DISC_TIME, sizeof( command_buffer ) );
			break;

		case  DEV_COMMAND_GET_FLOW:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->flow, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->flow, sizeof( command_buffer ) );
			}
			break;

		case  DEV_COMMAND_GET_FLUSH_MODE:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->flush_mode, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->flush_mode, sizeof( command_buffer ) );
			}
			break;

		case  DEV_COMMAND_GET_GW_1:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->gw_address_1, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->gw_address_1, sizeof( command_buffer ) );
			}

			// 4/27/2015 mpd : IP octets are dynamic values that do not include a CR like the rest of the EN text commands. Add a CR
			// after the octet value. Note that adding a CRLF is incorrect and causes a jump to a subsequent field.
			add_cr = (true);
			break;

		case  DEV_COMMAND_GET_GW_2:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->gw_address_2, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->gw_address_2, sizeof( command_buffer ) );
			}

			// 4/27/2015 mpd : IP octets are dynamic values that do not include a CR like the rest of the EN text commands. Add a CR
			// after the octet value. Note that adding a CRLF is incorrect and causes a jump to a subsequent field.
			add_cr = (true);
			break;

		case  DEV_COMMAND_GET_GW_3:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->gw_address_3, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->gw_address_3, sizeof( command_buffer ) );
			}

			// 4/27/2015 mpd : IP octets are dynamic values that do not include a CR like the rest of the EN text commands. Add a CR
			// after the octet value. Note that adding a CRLF is incorrect and causes a jump to a subsequent field.
			add_cr = (true);
			break;

		case  DEV_COMMAND_GET_GW_4:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->gw_address_4, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->gw_address_4, sizeof( command_buffer ) );
			}

			// 4/27/2015 mpd : IP octets are dynamic values that do not include a CR like the rest of the EN text commands. Add a CR
			// after the octet value. Note that adding a CRLF is incorrect and causes a jump to a subsequent field.
			add_cr = (true);
			break;

		case  DEV_COMMAND_GET_GW_BOOL:
			// 4/29/2015 mpd : Program the gateway IP address if DHCP is disabled.
			if( dev_state->dhcp_enabled )
			{
				// 4/27/2015 mpd : Say "No" to the change request.
				strlcpy( command_buffer, DEV_TEXT_CMD_N, sizeof( command_buffer ) );

				// 6/12/2015 mpd : FUTURE: Both EN WRITE lists could be combined by using "dev_state->dhcp_enabled" and 
				// "dev_increment_list_item_pointer( dev_state )". We send this boolean command, then increment the list 
				// pointer +4 here to skip gateway octets when DHCP is enabled. This is a low priority. See the 
				// DEV_COMMAND_WEP_K1 for an example.
			}
			else
			{
				// 4/27/2015 mpd : "Yes", we want to program the gateway IP address.
				strlcpy( command_buffer, DEV_TEXT_CMD_Y, sizeof( command_buffer ) );
			}
			break;

		case  DEV_COMMAND_GET_IF_MODE:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->if_mode, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->if_mode, sizeof( command_buffer ) );
			}
			break;

		case  DEV_COMMAND_GET_IP_1:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->ip_address_1, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->ip_address_1, sizeof( command_buffer ) );
			}

			// 4/27/2015 mpd : IP octets are dynamic values that do not include a CR like the rest of the EN text commands. Add a CR
			// after the octet value. Note that adding a CRLF is incorrect and causes a jump to a subsequent field.
			add_cr = (true);
			break;

		case  DEV_COMMAND_GET_IP_2:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->ip_address_2, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->ip_address_2, sizeof( command_buffer ) );
			}

			// 4/27/2015 mpd : IP octets are dynamic values that do not include a CR like the rest of the EN text commands. Add a CR
			// after the octet value. Note that adding a CRLF is incorrect and causes a jump to a subsequent field.
			add_cr = (true);
			break;

		case  DEV_COMMAND_GET_IP_3:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->ip_address_3, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->ip_address_3, sizeof( command_buffer ) );
			}

			// 4/27/2015 mpd : IP octets are dynamic values that do not include a CR like the rest of the EN text commands. Add a CR
			// after the octet value. Note that adding a CRLF is incorrect and causes a jump to a subsequent field.
			add_cr = (true);
			break;

		case  DEV_COMMAND_GET_IP_4:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->ip_address_4, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->ip_address_4, sizeof( command_buffer ) );
			}

			// 4/27/2015 mpd : IP octets are dynamic values that do not include a CR like the rest of the EN text commands. Add a CR
			// after the octet value. Note that adding a CRLF is incorrect and causes a jump to a subsequent field.
			add_cr = (true);
			break;

		case  DEV_COMMAND_GET_MASK_BITS:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				snprintf( command_buffer, EN_MASK_BITS_LEN, "%d\r", uds1100_details->mask_bits );
			}
			else
			{
				snprintf( command_buffer, EN_MASK_BITS_LEN, "%d\r", wibox_details->mask_bits );
			}
			break;

		case  DEV_COMMAND_GET_PORT:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->port, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->port, sizeof( command_buffer ) );
			}
			break;

		case  DEV_COMMAND_GET_REMOTE_IP_1:
			// 4/27/2015 mpd : The Remote IP octets pointing to the Calsense server are supplied by defines. They already 
			// include a CR. Don't add another.
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->remote_ip_address_1, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->remote_ip_address_1, sizeof( command_buffer ) );
			}
			break;

		case  DEV_COMMAND_GET_REMOTE_IP_2:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->remote_ip_address_2, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->remote_ip_address_2, sizeof( command_buffer ) );
			}
			break;

		case  DEV_COMMAND_GET_REMOTE_IP_3:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->remote_ip_address_3, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->remote_ip_address_3, sizeof( command_buffer ) );
			}
			break;

		case  DEV_COMMAND_GET_REMOTE_IP_4:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->remote_ip_address_4, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->remote_ip_address_4, sizeof( command_buffer ) );
			}
			break;

		case  DEV_COMMAND_GET_REMOTE_PORT:
			if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
			{
				strlcpy( command_buffer, dev_state->en_active_pvs->remote_port, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->remote_port, sizeof( command_buffer ) );
			}
			break;

		case  DEV_COMMAND_GET_SEND_BOOL:
			// 4/20/2015 mpd : This is always a "YES".
			strlcpy( command_buffer, DEV_TEXT_CMD_Y, sizeof( command_buffer ) );
			break;

		case  DEV_COMMAND_GET_SEND_CHAR_1:
			strlcpy( command_buffer, DEV_TEXT_CMD_SU_SEND_CHAR, sizeof( command_buffer ) ); 
			break; 

		case  DEV_COMMAND_GET_SEND_CHAR_2:
			strlcpy( command_buffer, DEV_TEXT_CMD_SU_SEND_CHAR, sizeof( command_buffer ) ); 
			break; 

		case  DEV_COMMAND_GET_TELNET_BOOL:
			strlcpy( command_buffer, DEV_TEXT_CMD_N, sizeof( command_buffer ) );
			break;

		case  DEV_COMMAND_GOTO_CHANNEL:
			strlcpy( command_buffer, DEV_MENU_CHANNEL_STR, sizeof( command_buffer ) );
			break;

		case  DEV_COMMAND_GOTO_DEFAULTS:
			strlcpy( command_buffer, DEV_MENU_DEFAULTS_STR, sizeof( command_buffer ) );
			break;

		case  DEV_COMMAND_GOTO_EXPERT:
			strlcpy( command_buffer, DEV_MENU_EXPERT_STR, sizeof( command_buffer ) );
			break;

		case  DEV_COMMAND_GOTO_SECURITY:
			strlcpy( command_buffer, DEV_MENU_SECURITY_STR, sizeof( command_buffer ) );
			break;

		case  DEV_COMMAND_GOTO_SERVER:
			strlcpy( command_buffer, DEV_MENU_SERVER_STR, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_HOST:
			strlcpy( command_buffer, DEV_TEXT_CMD_HOST, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_PW_XN_HOSTNAME:
			// 6/2/2015 mpd : e.g. "hostname myhost\r".
			if( ( strlen( pw_xn_details->dhcp_name ) ) && ( strncmp( pw_xn_details->dhcp_name, DEV_NOT_SET_STR, sizeof( pw_xn_details->dhcp_name ) ) != NULL ) )
			{
				strlcpy( command_buffer, DEV_TEXT_CMD_HOSTNAME, sizeof( command_buffer ) );
				strlcat( command_buffer, pw_xn_details->dhcp_name, sizeof( command_buffer ) );
			}
			else
			{
				// 6/12/2015 mpd : FUTURE: EN has a flag to stop users from stomping on a known name. Add that for WEN.


				// 6/12/2015 mpd : The ":" characters in the MAC address are not allowed in the name.
				//strlcpy( command_buffer, "no hostname", sizeof( command_buffer ) );
				snprintf( command_buffer, sizeof( command_buffer ), "%s%c%c%c%c%c%c%c%c%c%c%c%c", 
						  DEV_TEXT_CMD_HOSTNAME, 
						  pw_xn_details->mac_address[0],  pw_xn_details->mac_address[1], 
						  pw_xn_details->mac_address[3],  pw_xn_details->mac_address[4], 
						  pw_xn_details->mac_address[6],  pw_xn_details->mac_address[7],
						  pw_xn_details->mac_address[9],  pw_xn_details->mac_address[10], 
						  pw_xn_details->mac_address[12], pw_xn_details->mac_address[13],
						  pw_xn_details->mac_address[15], pw_xn_details->mac_address[16] );
			}
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_IEEE:         // Set IEEE. 
			strlcpy( command_buffer, "ieee 802.1x ", sizeof( command_buffer ) );
			strlcat( command_buffer, pw_xn_details->wpa_ieee_802_1x, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_IF_ETH:
			strlcpy( command_buffer, DEV_TEXT_CMD_IF_ETH, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_NAVIGATION_CMD;
			break;

		case DEV_COMMAND_IF_WLAN:
			strlcpy( command_buffer, DEV_TEXT_CMD_IF_WLAN, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_NAVIGATION_CMD;
			break;

		case DEV_COMMAND_IF_WWAN:
			strlcpy( command_buffer, DEV_TEXT_CMD_IF_WWAN, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_NAVIGATION_CMD;
			break;

		case DEV_COMMAND_PW_XN_IP_CIDR:
			// 6/2/2015 mpd : e.g. "ip address 192.168.1.1/24\r".
			strlcpy( command_buffer, DEV_TEXT_CMD_IP_CIDR, sizeof( command_buffer ) );
			strlcat( command_buffer, pw_xn_details->ip_address, sizeof( command_buffer ) );
			snprintf( work_space, sizeof( work_space ), "/%d\r", pw_xn_details->mask_bits );
			strlcat( command_buffer, work_space, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			break;

		case DEV_COMMAND_KEY_CLEAR_WEP:      // Clear the key. 
			// 6/15/2015 mpd : This is used to clear the WEP key. The device will error on key size changes if the
			// key value exists. Since we can't read the value due to security, don't stomp on the current value unless 
			// we really have to.
			if( ( pw_xn_details->user_changed_key ) && ( strstr( pw_xn_details->security, "WEP" ) ) )
			{
				strlcpy( command_buffer, "no key", sizeof( command_buffer ) );
				add_cr = true;
			}
			else
			{
				// 6/15/2015 mpd : We are in WPA suite, or the key is already set and we don't know what it is. Do not 
				// overwrite it. Send just a CR to skip doing anything destructive. Don't add another CR.
				strlcpy( command_buffer, DEV_CR_STR, sizeof( command_buffer ) );
			}
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			break;

		case DEV_COMMAND_KEY_CLEAR_WPA:      // Clear the key. 
			// 6/15/2015 mpd : This is used to clear the WPA key. The device will error on key size changes if the
			// key value exists. Since we can't read the value due to security, don't stomp on the current value unless 
			// we really have to.
			if( ( pw_xn_details->user_changed_key ) && ( strstr( pw_xn_details->security, "WPA" ) ) )
			{
				strlcpy( command_buffer, "no key", sizeof( command_buffer ) );
				add_cr = true;
			}
			else
			{
				// 6/15/2015 mpd : We are in WEP suite, or the key is already set and we don't know what it is. Do not 
				// overwrite it. Send just a CR to skip doing anything destructive. Don't add another CR.
				strlcpy( command_buffer, DEV_CR_STR, sizeof( command_buffer ) );
			}
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			break;

		case DEV_COMMAND_KEY_IDX:      // Set the key index. 
			strlcpy( command_buffer, "tx key index ", sizeof( command_buffer ) );
			strlcat( command_buffer, pw_xn_details->wep_tx_key, sizeof( command_buffer ) );
			add_cr = true;
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			break;

		case DEV_COMMAND_KEY_SIZE:     // Set the key size.
			// 6/17/2015 mpd : Don't allow a key size change unless the key changes too.
			if( pw_xn_details->user_changed_key )
			{
				strlcpy( command_buffer, "key size ", sizeof( command_buffer ) );
				// 6/3/2015 mpd : Truncate the text to end after the digits.
				strlcpy( work_space, pw_xn_details->wep_key_size, sizeof( work_space ) );
				strtok( work_space, " bits" );
				strlcat( command_buffer, work_space, sizeof( command_buffer ) );
				add_cr = true;
			}
			else
			{
				// 6/15/2015 mpd : We are not changing this field. Send just a CR to skip doing anything destructive. 
				strlcpy( command_buffer, DEV_CR_STR, sizeof( command_buffer ) );
			}
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			break;

		case DEV_COMMAND_KEY_SET_WEP:      // Set the WEP key. 
			// 6/15/2015 mpd : This is used to set the WEP key. Since we can't read the value due to security, don't 
			// stomp on the current value unless the user specifically askes us to.
			if( ( pw_xn_details->user_changed_key ) && ( strstr( pw_xn_details->security, "WEP" ) ) )
			{
				if( ( strlen( pw_xn_details->key ) ) && ( strncmp( pw_xn_details->key, DEV_NOT_SET_STR, sizeof( pw_xn_details->key ) ) != NULL ) )
				{
					strlcpy( command_buffer, "key ", sizeof( command_buffer ) );
					if( strncmp( pw_xn_details->w_key_type, "Passphrase", sizeof( pw_xn_details->w_key_type ) ) == NULL )
					{
						// 6/17/2015 mpd : TBD: What is needed here, the passphrase string or the key string? The CLI
						// only allows 32 characters for text. Do we have to limit this in the GUI?
						strlcat( command_buffer, "text ", sizeof( command_buffer ) );
						dev_command_escape_char_handler( pw_xn_details->key, work_space, sizeof( work_space ) );
						strlcat( command_buffer, work_space, sizeof( command_buffer ) );
					}
					else
					{
						strlcat( command_buffer, pw_xn_details->key, sizeof( command_buffer ) );
					}
				}
				else
				{
					strlcpy( command_buffer, "no key", sizeof( command_buffer ) );
				}
				add_cr = true;
			}
			else
			{
				// 6/15/2015 mpd : We are in WPA suite, or the key is already set and we don't know what it is. Do not 
				// overwrite it. Send just a CR to skip doing anything destructive. Don't add another CR.
				strlcpy( command_buffer, DEV_CR_STR, sizeof( command_buffer ) );
			}
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			break;

		case DEV_COMMAND_KEY_SET_WPA:      // Set the WPA key. 
			// 6/15/2015 mpd : This is used to set the WPA key. Since we can't read the value due to security, don't 
			// stomp on the current value unless the user specifically askes us to.
			if( ( pw_xn_details->user_changed_key ) && ( strstr( pw_xn_details->security, "WPA" ) ) )
			{
				if( ( strlen( pw_xn_details->key ) ) && ( strncmp( pw_xn_details->key, DEV_NOT_SET_STR, sizeof( pw_xn_details->key ) ) != NULL ) )
				{
					strlcpy( command_buffer, "key ", sizeof( command_buffer ) );
					if( strncmp( pw_xn_details->w_key_type, "Passphrase", sizeof( pw_xn_details->w_key_type ) ) == NULL )
					{
						// 6/17/2015 mpd : TBD: What is needed here, the passphrase string or the key string? The CLI
						// only allows 32 characters for text. Do we have to limit this in the GUI?
						strlcat( command_buffer, "text ", sizeof( command_buffer ) );
						dev_command_escape_char_handler( pw_xn_details->key, work_space, sizeof( work_space ) );
						strlcat( command_buffer, work_space, sizeof( command_buffer ) );
					}
					else
					{
						strlcat( command_buffer, pw_xn_details->key, sizeof( command_buffer ) );
					}
				}
				else
				{
					strlcpy( command_buffer, "no key", sizeof( command_buffer ) );
				}
				add_cr = true;
			}
			else
			{
				// 6/15/2015 mpd : We are in WEP suite, or the key is already set and we don't know what it is. Do not 
				// overwrite it. Send just a CR to skip doing anything destructive. Don't add another CR.
				strlcpy( command_buffer, DEV_CR_STR, sizeof( command_buffer ) );
			}
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			break;

		case DEV_COMMAND_KEY_TYPE:     // Set the key type. 
			strlcpy( command_buffer, "key type ", sizeof( command_buffer ) );
			strlcat( command_buffer, pw_xn_details->w_key_type, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_LENGTH:
			strlcpy( command_buffer, DEV_TEXT_CMD_LENGTH, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_LINE:
			strlcpy( command_buffer, DEV_TEXT_CMD_LINE, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_NAVIGATION_CMD;
			break;

		case DEV_COMMAND_LINE_2:
			strlcpy( command_buffer, DEV_TEXT_CMD_LINE_2, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_LINK:
			strlcpy( command_buffer, DEV_TEXT_CMD_LINK, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_LOG:
			strlcpy( command_buffer, DEV_TEXT_CMD_LOG, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_LOGIN:
			strlcpy( command_buffer, DEV_TEXT_CMD_LOGIN, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_MODEM:
			strlcpy( command_buffer, DEV_TEXT_CMD_MODEM, sizeof( command_buffer ) );
			break;

		case  DEV_COMMAND_MON_EXIT:
			strlcpy( command_buffer, DEV_TEXT_CMD_MON_EXIT, sizeof( command_buffer ) );
			break;

		case  DEV_COMMAND_MON_GET_MAC:
			strlcpy( command_buffer, DEV_TEXT_CMD_MON_MAC_ADD, sizeof( command_buffer ) );
			break;

		case  DEV_COMMAND_MON_GET_NETWORK:
			strlcpy( command_buffer, DEV_TEXT_CMD_MON_NETWORK, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_N:
			// 6/9/2015 mpd : NOTE: The Lantronix "Yes" and "No" programming commands are single characters with no CR or LF.
			strlcpy( command_buffer, DEV_TEXT_CMD_N, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_NO:
			strlcpy( command_buffer, DEV_TEXT_CMD_NO, sizeof( command_buffer ) );
			break;

		case  DEV_COMMAND_NONE:
			memset( command_buffer, DEV_EMPTY_STR, DEV_EMPTY_STR_LEN );
			dev_state->command_will_respond = false;
			break;

		case DEV_COMMAND_OUTPUT:
			strlcpy( command_buffer, DEV_TEXT_CMD_OUTPUT, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_PASSPHRASE:   // Set the passphrase. 
			if( pw_xn_details->user_changed_passphrase )
			{
				if( ( strlen( pw_xn_details->passphrase ) ) && ( strncmp( pw_xn_details->passphrase, DEV_NOT_SET_STR, sizeof( pw_xn_details->passphrase ) ) != NULL ) )
				{
					if( strncmp( pw_xn_details->passphrase, DEV_X_CP_DUMP_IGNORE_STR, sizeof( pw_xn_details->passphrase ) ) != NULL ) 
					{
						strlcpy( command_buffer, "passphrase ", sizeof( command_buffer ) );
						dev_command_escape_char_handler( pw_xn_details->passphrase, work_space, sizeof( work_space ) );
						strlcat( command_buffer, work_space, sizeof( command_buffer ) );
						add_cr = true;
					}
					else
					{
						// 6/3/2015 mpd : The passphrase is already set and we don't know what it is. Do not overwrite it.
						// Send just a CR to skip doing anything destructive. Don't add another.
						strlcpy( command_buffer, DEV_CR_STR, sizeof( command_buffer ) );
					}
				}
				else
				{
					strlcpy( command_buffer, "no passphrase", sizeof( command_buffer ) );
					add_cr = true;
				}
			}
			else
			{
				// 6/15/2015 mpd : We are not changing this field. Send just a CR to skip doing anything destructive. 
				strlcpy( command_buffer, DEV_CR_STR, sizeof( command_buffer ) );
			}
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			break;

		case DEV_COMMAND_PEAP:         // Set the PEAP Option. 
			strlcpy( command_buffer, "peap option ", sizeof( command_buffer ) );
			strlcat( command_buffer, pw_xn_details->wpa_peap_option, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_PORT:
			strlcpy( command_buffer, DEV_TEXT_CMD_PORT, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_PRIORITY:
			strlcpy( command_buffer, DEV_TEXT_CMD_PRIORITY, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_PWORD:        // Set the password. 
			if( pw_xn_details->user_changed_password )
			{
				if( ( strlen( pw_xn_details->password ) ) && ( strncmp( pw_xn_details->password, DEV_NOT_SET_STR, sizeof( pw_xn_details->password ) ) != NULL ) )
				{
					if( strncmp( pw_xn_details->password, DEV_X_CP_DUMP_IGNORE_STR, sizeof( pw_xn_details->password ) ) != NULL ) 
					{
						strlcpy( command_buffer, "password ", sizeof( command_buffer ) );
						dev_command_escape_char_handler( pw_xn_details->password, work_space, sizeof( work_space ) );
						strlcat( command_buffer, work_space, sizeof( command_buffer ) );
						add_cr = true;
					}
					else
					{
						// 6/3/2015 mpd : The password may be already set and we don't know what it is. Do not overwrite it.
						// Send just a CR to skip doing anything destructive. Don't add another.
						strlcpy( command_buffer, DEV_CR_STR, sizeof( command_buffer ) );
					}
				}
				else
				{
					strlcpy( command_buffer, "no password", sizeof( command_buffer ) );
					add_cr = true;
				}
			}
			else
			{
				// 6/15/2015 mpd : We are not changing this field. Send just a CR to skip doing anything destructive. 
				strlcpy( command_buffer, DEV_CR_STR, sizeof( command_buffer ) );
			}
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			break;

		case DEV_COMMAND_RELOAD:
			strlcpy( command_buffer, DEV_TEXT_CMD_RELOAD, sizeof( command_buffer ) );
			break;

		case  DEV_COMMAND_SAVE_AND_EXIT:
			strlcpy( command_buffer, DEV_MENU_SAVE_AND_EXIT_STR, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_SECURITY:     // Go to the SECURITY menu. 
			strlcpy( command_buffer, DEV_TEXT_CMD_SECURITY, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_NAVIGATION_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_SHOW:
			strlcpy( command_buffer, DEV_TEXT_CMD_SHOW, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_SHOW_S:
			strlcpy( command_buffer, DEV_TEXT_CMD_SHOW_S, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_SSID:         // Set the SSID. 
			if( ( strlen( pw_xn_details->ssid ) ) && ( strncmp( pw_xn_details->ssid, DEV_NOT_SET_STR, sizeof( pw_xn_details->ssid ) ) != NULL ) )
			{
				// 6/3/2015 mpd : Allow white space in network names.
				strlcpy( command_buffer, "network name ", sizeof( command_buffer ) );
				dev_command_escape_char_handler( pw_xn_details->ssid, work_space, sizeof( work_space ) );
				strlcat( command_buffer, work_space, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, "no network name", sizeof( command_buffer ) );
			}
			//dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			dev_state->command_separation = DEV_COMMAND_SEPARATION_LONG_TIMER_MS;
			add_cr = true;
			break;

		case DEV_COMMAND_START:
			if( (dev_state->device_type == COMM_DEVICE_EN_UDS1100) || (dev_state->device_type == COMM_DEVICE_WEN_WIBOX) )
			{
				strlcpy( command_buffer, DEV_TEXT_CMD_START_LANTRONIX_COBOX_SU, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, DEV_TEXT_CMD_START, sizeof( command_buffer ) );
			}
			break;

		case DEV_COMMAND_SUITE:        // Set the security suite. 
			strlcpy( command_buffer, "suite ", sizeof( command_buffer ) );
			strlcat( command_buffer, pw_xn_details->security, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_SYNC:
			if( dev_state->device_type == COMM_DEVICE_GR_PREMIERWAVE )
			{
				strlcpy( command_buffer, DEV_TEXT_CMD_SYNC_N, sizeof( command_buffer ) );
			}
			else if( (dev_state->device_type == COMM_DEVICE_WEN_PREMIERWAVE) || (dev_state->device_type == COMM_DEVICE_EN_PREMIERWAVE) )
			{
				strlcpy( command_buffer, DEV_TEXT_CMD_SYNC_S, sizeof( command_buffer ) );
			}
			break;

		case DEV_COMMAND_TUNNEL:
			strlcpy( command_buffer, DEV_TEXT_CMD_TUNNEL, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_UNAME:        // Set the user name. 
			if( ( strlen( pw_xn_details->user_name ) ) && ( strncmp( pw_xn_details->user_name, DEV_NOT_SET_STR, sizeof( pw_xn_details->user_name ) ) != NULL ) )
			{
				if( strncmp( pw_xn_details->user_name, DEV_X_CP_DUMP_IGNORE_STR, sizeof( pw_xn_details->user_name ) ) != NULL ) 
				{
					strlcpy( command_buffer, "username ", sizeof( command_buffer ) );
					dev_command_escape_char_handler( pw_xn_details->user_name, work_space, sizeof( work_space ) );
					strlcat( command_buffer, work_space, sizeof( command_buffer ) );
					add_cr = true;
				}
				else
				{
					// 6/3/2015 mpd : The username may be already set and we don't know what it is. Do not overwrite it.
					// Send just a CR to skip doing anything destructive. Don't add another.
					strlcpy( command_buffer, DEV_CR_STR, sizeof( command_buffer ) );
				}
			}
			else
			{
				strlcpy( command_buffer, "no username", sizeof( command_buffer ) );
				dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
				add_cr = true;
			}
			break;

		case DEV_COMMAND_VERBOSE:
			strlcpy( command_buffer, DEV_TEXT_CMD_VERBOSE, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_WEP:	         // Go to the WEP menu. 
			strlcpy( command_buffer, DEV_TEXT_CMD_WEP, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_NAVIGATION_CMD;
			break;

		case DEV_COMMAND_WEP_AUTH:     // Set authentication. 
			strlcpy( command_buffer, "authentication ", sizeof( command_buffer ) );
			// 6/3/2015 mpd : "Open/None" is not an acceptable device input. Truncate the text.
			strlcpy( work_space, pw_xn_details->wep_authentication, sizeof( work_space ) );
			strtok( work_space, "/" );
			strlcat( command_buffer, work_space, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_WEP_K1:       // Go to the KEY 1 menu. 
			if( ( pw_xn_details->user_changed_key ) && ( strstr( pw_xn_details->security, "WEP" ) ) )
			{
				strlcpy( command_buffer, "key 1", sizeof( command_buffer ) );
			}
			else
			{
				// 6/17/2015 mpd : THIS IS FRAGILE! We can skip the entire Key 1 - 4 clearing process and save 55 
				// seconds if the user is not requesting a WEP key or key size change.
				// First, set the list pointer to jump past the 16 navigate and clear steps for the 4 key indexes. Then, 
				// perform the command we would have been doing 16 items from now. The next termination string for this 
				// command needs to be generic enough to pick up either response.  
				// NOTE: Any changes to the WEN WRITE list could break this.
				dev_increment_list_item_pointer( dev_state, 16 );

				// 6/17/2015 mpd : This is an exact copy of DEV_COMMAND_WEP_AUTH.
				strlcpy( command_buffer, "authentication ", sizeof( command_buffer ) );
				// 6/3/2015 mpd : "Open/None" is not an acceptable device input. Truncate the text.
				strlcpy( work_space, pw_xn_details->wep_authentication, sizeof( work_space ) );
				strtok( work_space, "/" );
				strlcat( command_buffer, work_space, sizeof( command_buffer ) );
			}
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_NAVIGATION_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_WEP_K2:       // Go to the KEY 2 menu. 
			strlcpy( command_buffer, "key 2", sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_NAVIGATION_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_WEP_K3:       // Go to the KEY 3 menu. 
			strlcpy( command_buffer, "key 3", sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_NAVIGATION_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_WEP_K4:       // Go to the KEY 4 menu. 
			strlcpy( command_buffer, "key 4", sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_NAVIGATION_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_WEP_KX:       // Go to the KEY X menu. 
			strlcpy( command_buffer, "key ", sizeof( command_buffer ) );
			strlcat( command_buffer, pw_xn_details->wep_tx_key, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_NAVIGATION_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_WLAN_PRO:     // Go to WLAN Profiles. 
			strlcpy( command_buffer, DEV_TEXT_CMD_WLAN_PRO, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_WPA_AUTH:     // Set the authentication. 
			if( strstr( pw_xn_details->wpa_authentication, "PSK" ) )
			{
				strlcpy( command_buffer, "authentication psk", sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, "authentication 802.1x", sizeof( command_buffer ) );
			}
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_WPAX:         // Go to the WPAX menu. 
			strlcpy( command_buffer, DEV_TEXT_CMD_WPAX, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_NAVIGATION_CMD;
			break;

		case DEV_COMMAND_WRITE_FINAL:
			// 5/14/2015 mpd : The write command following a baud rate change does not respond. Do not "break" on 
			// this case. Continue on to DEV_COMMAND_WRITE;
			dev_state->command_will_respond = false;

		case DEV_COMMAND_WRITE:
			strlcpy( command_buffer, DEV_TEXT_CMD_WRITE, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_LONG_TIMER_MS;
			break;

		case DEV_COMMAND_X_CD_DUMP:
			strlcpy( command_buffer, DEV_TEXT_CMD_X_CD_DUMP, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_X_CI_DUMP:
			strlcpy( command_buffer, DEV_TEXT_CMD_X_CI_DUMP, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_X_CP_DUMP:
			strlcpy( command_buffer, DEV_TEXT_CMD_X_CP_DUMP, sizeof( command_buffer ) );

			// 5/29/2015 mpd : Use this command for the "calsense_hq" profile.
			//strlcpy( command_buffer, DEV_TEXT_CMD_X_CQ_DUMP, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_X_SI_DUMP:
			strlcpy( command_buffer, DEV_TEXT_CMD_X_SI_DUMP, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_XML:
			strlcpy( command_buffer, DEV_TEXT_CMD_XML, sizeof( command_buffer ) );

			// 6/12/2015 mpd : GR needs a little more wakeup time to respond consistently. WEN can use it too.
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_NAVIGATION_CMD;
			break;

		case DEV_COMMAND_Y:
			// 6/9/2015 mpd : NOTE: The Lantronix "Yes" and "No" programming commands are single characters with no CR or LF.
			strlcpy( command_buffer, DEV_TEXT_CMD_Y, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_YES:
			strlcpy( command_buffer, DEV_TEXT_CMD_YES, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_X_ETH0_DUMP:
			strlcpy( command_buffer, DEV_TEXT_CMD_X_ETH0_DUMP, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_PW_XE_IP_CIDR:
			// 6/2/2015 mpd : e.g. "ip address 192.168.1.1/24\r".
			strlcpy( command_buffer, DEV_TEXT_CMD_IP_CIDR, sizeof( command_buffer ) );
			strlcat( command_buffer, pw_xe_details->ip_address, sizeof( command_buffer ) );
			snprintf( work_space, sizeof( work_space ), "/%d\r", pw_xe_details->mask_bits );
			strlcat( command_buffer, work_space, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			break;

		case DEV_COMMAND_PW_XE_GATEWAY:
			// 6/2/2015 mpd : e.g. "default gateway 192.168.254.250\r".
			strlcpy( command_buffer, DEV_TEXT_CMD_GATEWAY, sizeof( command_buffer ) );
			strlcat( command_buffer, pw_xe_details->gw_address, sizeof( command_buffer ) );
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_PW_XE_HOSTNAME:
			// 6/2/2015 mpd : e.g. "hostname myhost\r".
			if( (strlen(pw_xe_details->dhcp_name) > 0) && (strncmp(pw_xe_details->dhcp_name, DEV_NOT_SET_STR, sizeof( pw_xe_details->dhcp_name)) != 0) )
			{
				strlcpy( command_buffer, DEV_TEXT_CMD_HOSTNAME, sizeof( command_buffer ) );
				strlcat( command_buffer, pw_xe_details->dhcp_name, sizeof( command_buffer ) );
			}
			else
			{
				// 6/12/2015 mpd : FUTURE: EN has a flag to stop users from stomping on a known name. Add that for WEN.


				// 6/12/2015 mpd : The ":" characters in the MAC address are not allowed in the name.
				snprintf( command_buffer, sizeof( command_buffer ), "%s%c%c%c%c%c%c%c%c%c%c%c%c", 
						  DEV_TEXT_CMD_HOSTNAME, 
						  pw_xe_details->mac_address[0],  pw_xe_details->mac_address[1], 
						  pw_xe_details->mac_address[3],  pw_xe_details->mac_address[4], 
						  pw_xe_details->mac_address[6],  pw_xe_details->mac_address[7],
						  pw_xe_details->mac_address[9],  pw_xe_details->mac_address[10], 
						  pw_xe_details->mac_address[12], pw_xe_details->mac_address[13],
						  pw_xe_details->mac_address[15], pw_xe_details->mac_address[16] );
			}
			dev_state->command_separation = DEV_COMMAND_SEPARATION_FOR_SET_VAR_CMD;
			add_cr = true;
			break;

		case DEV_COMMAND_GOTO_WLAN:
			strlcpy( command_buffer, "4\r", sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_GET_WIBOX_TOPOLOGY:
			// 3/31/2016 ajv : Always force topology to Infrastructure
			strlcpy( command_buffer, "0\r", sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_GET_WIBOX_SSID:
			strlcpy( command_buffer, dev_state->wibox_active_pvs->ssid, sizeof( command_buffer ) );
			add_cr = (true);
			break;

		case DEV_COMMAND_GET_WIBOX_SECURITY:
			snprintf( command_buffer, sizeof( command_buffer ), "%d\r", dev_state->wibox_active_pvs->security );

			// 3/31/2016 ajv : Skip over the commands that aren't relevant for the security suite the
			// user selected.
			switch( dev_state->wibox_active_pvs->security )
			{
				case 0:
					// 3/31/2016 ajv : Skip to DEV_COMMAND_GET_WIBOX_TX_DATA_RATE_1
					dev_increment_list_item_pointer( dev_state, 17 );
					break;

				case 1:
					// 3/31/2016 ajv : Move down to WIBOX_WLAN_INPUT_WEP_AUTH. Nothing needs to be done here
					// since that's the next field in the write table.
					break;

				case 2:
					// 3/31/2016 ajv : Skip to WIBOX_WLAN_INPUT_WPA_CHANGE_KEY
					dev_increment_list_item_pointer( dev_state, 7 );
					break;

				case 3:
					// 3/31/2016 ajv : Skip to WIBOX_WLAN_INPUT_WPA2_CHANGE_KEY
					dev_increment_list_item_pointer( dev_state, 12 );
					break;

			}
			break;

		case DEV_COMMAND_GET_WEP_AUTH:
			snprintf( command_buffer, sizeof( command_buffer ), "%d\r", dev_state->wibox_active_pvs->wep_authentication );
			break;

		case DEV_COMMAND_GET_WEP_ENCRYPTION:
			snprintf( command_buffer, sizeof( command_buffer ), "%d\r", (dev_state->wibox_active_pvs->wep_key_size + 1) );
			break;

		case DEV_COMMAND_GET_WEP_CHANGE_KEY:
		case DEV_COMMAND_GET_WPA_CHANGE_KEY:
		case DEV_COMMAND_GET_WPA2_CHANGE_KEY:
			if( (dev_state->wibox_active_pvs->user_changed_key) || (dev_state->wibox_active_pvs->user_changed_passphrase) )
			{
				strlcpy( command_buffer, DEV_TEXT_CMD_Y, sizeof( command_buffer ) );
			}
			else
			{
				strlcpy( command_buffer, DEV_TEXT_CMD_N, sizeof( command_buffer ) );

				// 4/1/2016 ajv : If the key hasn't changed, skip over the "Display key", "Key type" and
				// "Enter Key" fields.
				dev_increment_list_item_pointer( dev_state, 3 );
			}
			break;

		case DEV_COMMAND_GET_WEP_DISPLAY_KEY:
		case DEV_COMMAND_GET_WPA_DISPLAY_KEY:
		case DEV_COMMAND_GET_WPA2_DISPLAY_KEY:
			strlcpy( command_buffer, DEV_TEXT_CMD_N, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_GET_WEP_KEY_TYPE:
		case DEV_COMMAND_GET_WPA_KEY_TYPE:
		case DEV_COMMAND_GET_WPA2_KEY_TYPE:
			snprintf( command_buffer, sizeof( command_buffer ), "%d\r", dev_state->wibox_active_pvs->w_key_type );
			break;

		case DEV_COMMAND_GET_WEP_KEY:
		case DEV_COMMAND_GET_WPA_KEY:
		case DEV_COMMAND_GET_WPA2_KEY:
			if( dev_state->wibox_active_pvs->w_key_type == WEN_KEY_TYPE_HEX  )
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->key, sizeof( command_buffer ) );
			}
			else	 // WEN_KEY_TYPE_PASS
			{
				strlcpy( command_buffer, dev_state->wibox_active_pvs->passphrase, sizeof( command_buffer ) );
			}
			add_cr = (true);
			break;

		case DEV_COMMAND_GET_WEP_TX_KEY_INDEX:
			// 3/31/2016 ajv : Always use WEP Key Index 1.
			strlcpy( command_buffer, "1\r", sizeof( command_buffer ) );

			// 3/31/2016 ajv : Skip to DEV_COMMAND_GET_WIBOX_TX_DATA_RATE_1
			dev_increment_list_item_pointer( dev_state, 10 );
			break;

		case DEV_COMMAND_GET_WPA_ENCRYPTION:
			snprintf( command_buffer, sizeof( command_buffer ), "%d\r", dev_state->wibox_active_pvs->wpa_encryption );

			// 3/31/2016 ajv : Skip to DEV_COMMAND_GET_WIBOX_TX_DATA_RATE_1
			dev_increment_list_item_pointer( dev_state, 5 );
			break;

		case DEV_COMMAND_GET_WPA2_ENCRYPTION:
			snprintf( command_buffer, sizeof( command_buffer ), "%d\r", dev_state->wibox_active_pvs->wpa2_encryption );

			// 3/31/2016 ajv : Move down to WIBOX_WLAN_INPUT_WEP_AUTH. Nothing needs to be done here
			// since that's the next field in the write table.
			break;

		case DEV_COMMAND_GET_WIBOX_TX_DATA_RATE_1:
			// 3/31/2016 ajv : Always set TX Data rate to auto fallback
			strlcpy( command_buffer, "1\r", sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_GET_WIBOX_TX_DATA_RATE_2:
			// 3/31/2016 ajv : Always set TX Data rate to 54 Mbps
			strlcpy( command_buffer, "7\r", sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_GET_WIBOX_MIN_TX_DATA_RATE:
			// 3/31/2016 ajv : Always set TX Data rate to 1 Mbps
			strlcpy( command_buffer, "0\r", sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_GET_WIBOX_POWER_MGMT:
			// 3/31/2016 ajv : Always disable power management
			strlcpy( command_buffer, DEV_TEXT_CMD_N, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_GET_WIBOX_SOFT_AP_ROAMING:
			// 3/31/2016 ajv : Always disable soft AP roaming
			strlcpy( command_buffer, DEV_TEXT_CMD_N, sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_GET_WIBOX_MAX_FAILED_PACKETS:
			// 3/31/2016 ajv : Always set max failed packets to 6
			strlcpy( command_buffer, "6\r", sizeof( command_buffer ) );
			break;

		case DEV_COMMAND_GET_WIBOX_NETWORK_MODE:
			// 3/31/2016 ajv : Always set network mode to Wireless Only
			strlcpy( command_buffer, "1\r", sizeof( command_buffer ) );
			break;

		default:
			send_command_to_device = false;
			command_ptr = NULL;

			Alert_Message( "Unknown Device Command" );
			break;
	}

	// 3/18/2015 mpd : Was a valid command found?
	if( send_command_to_device )
	{
		// 4/20/2015 mpd : EN device programming added CR and LF here for specific commands. That is unnecessary for GR
		// and WEN programming since all command defines include CR and LF where needed. All command strings are null 
		// terminated. 

		// 6/3/2015 mpd : Many WEN GuiVars are ready-to-use strings. Just add a CR.
		if( add_cr )
		{
			strlcat( command_buffer, DEV_CR_STR, sizeof( command_buffer ) );
		}

		// 3/18/2015 mpd : Allocate memory, set the return pointer, and copy the buffer. The calling function is responsible
		// for freeing the memory.
		command_ptr = mem_malloc( strlen( command_buffer ) + DEV_NULL_LEN );
		memcpy( command_ptr, command_buffer, ( strlen( command_buffer ) + DEV_NULL_LEN ) );

		#ifdef DEV_DEBUG_GET_TEXT
			if( command == DEV_COMMAND_NONE )
			{
				Alert_Message( "CMD( 1 ): NONE" );
			}
			else if( command == DEV_COMMAND_CR )
			{
				Alert_Message_va( "CMD( 1 ): CR: %d %d", command_buffer[0], command_buffer[1] );
			}
			else 
			{
				Alert_Message_va( "CMD( %d ): %s", strlen( command_buffer ), command_ptr );
			}
		#endif
	}

	return ( command_ptr );
}

/* ---------------------------------------------------------- */
/* 				 GR/WEN Radio Mode Functions     			  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void dev_enter_device_mode( DEV_STATE_STRUCT *dev_state )
{
	UNS_32  elapsed_ms = 0;
	BOOL_32	power_is_on = true;

	//Alert_Message( "dev_enter_device_mode" );

	// 6/22/2015 mpd : There are multiple READ operation defines. Test for the single WRITE define.
	// 4/16/2015 mpd : Send progress info to the GUI. Keep it simple. Use the READ key for both READ and WRITE.
	if( dev_state->operation == DEV_WRITE_OPERATION )
	{
		strlcpy( dev_state->info_text, "Preparing to program...", sizeof( dev_state->info_text ) );                
		COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_write_settings_in_progress );
	}
	else
	{
		strlcpy( dev_state->info_text, "Preparing to read...", sizeof( dev_state->info_text ) );                
		COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );
	}

	// 4/21/2015 mpd : The GR/WEN device can only be programmed at a baud rate of 9600. Since the port is at 115200 for 
	// normal communication, we have to temporarily modify the speed for programming mode.
	SERIAL_set_baud_rate_on_A_or_B( comm_mngr.device_exchange_port, DEV_PROGRAMMING_BAUD_RATE );

	// ----------

	// 5/5/2015 mpd : The GR/WEN PremierWave devices are put into programming mode by a power cycle immediately followed 
	// by holding down the "!" key for about 10 seconds (we will simulate auto-repeat). Once it responds with a single 
	// "!", we have 5 seconds to send "xyz" out the serial port. 
 
	// 5/5/2015 mpd : Turn it off.
	power_down_device( comm_mngr.device_exchange_port );

	// ----------
	// ----------

	// 6/30/2015 rmd : The following code generated by Mike Dee is really a bunch of sillyness.
	// The is no need to test if the GPIO pins have been set to the off level. If they are not
	// at that level either we have broken hardware or the power function called just prior is
	// not properly coded. In other words, the prior function call to the table power function
	// is guaranteed 100% to have shut off the power supply to the device. The supply voltage is
	// decaying and the device will soon be off. I mean in like 50ms as the power supply caps
	// loose their energy. So the following loop waiting for the GPIO to be off immediately hits
	// the break. Every time.
	//
	// 6/30/2015 rmd : Following is the comment by Mike:
	// The power-down command does not always shut off power immediately if the controller has just rebooted. Test for 
	// power off before starting the off timer. The test is being is executed within the context of the comm_mngr task. 
	// The test time plus off time cannot exceed the 15 second watchdog!
	do
	{
		if( ( power_is_on = GENERIC_GR_card_power_is_on( comm_mngr.device_exchange_port ) ) == false ) 
		{
			break;
		}

		vTaskDelay( MS_to_TICKS( DEV_POWER_TEST_TIMER_MS ) );

		elapsed_ms += DEV_POWER_TEST_TIMER_MS;

	} while( elapsed_ms < DEV_POWER_TEST_MAX_MS );

	if( power_is_on )
	{
		dev_handle_error( dev_state, DEV_ERROR_ENTER_MODE, DEV_ERROR_LEVEL_FATAL, "Device failed to power-off" );
	}

	// ----------
	// ----------

	// 6/5/2015 mpd : Leave it off briefly.
	vTaskDelay( MS_to_TICKS( DEV_POWER_OFF_TIMER_MS ) );

	// 4/23/2015 mpd : Turn it back on.
	power_up_device( comm_mngr.device_exchange_port );

	// 6/5/2015 mpd : Give the device a moment to wakeup before throwing commands at it.
	dev_state->command_separation = DEV_POWER_ON_TIMER_MS;

	// 4/23/2015 mpd : The subsequent mode entry commands have been moved into the read list where the response can 
	// be analyzed.
	 
}

/* ---------------------------------------------------------- */
static void dev_exit_device_mode()
{
	//Alert_Message( "dev_exit_device_mode" );

	// ----------
	
	// 5/5/2015 mpd : Since WRITE sends a response following the reboot command, we can't handle it here the way we 
	// did with the no-response SR radio exit command. Reboot and exit commands have to be sent from the READ and 
	// WRITE lists. 

	// 4/15/2015 mpd : The GR/WEN device was temporarily set to a baud rate of 9600 for programming. It's time to set 
	// it back into normal communication speed.
	SERIAL_set_baud_rate_on_A_or_B( comm_mngr.device_exchange_port, port_device_table[ dev_state->device_type ].baud_rate );

	comm_mngr.device_exchange_state = DEV_DEVEX_FINISH;

	// 5/22/2015 mpd : This should have already been done. Do it again to be sure.
	dev_cli_disconnect();
}

// 5/14/2015 mpd : This function drops the DTR signal which is connected to the GR/WEN DCD input. That signals the 
// device to exit from the highest level of CLI (this is after the exit from the "enable" prompt). DTR must be set 
// active again or the radio will not maintain a network connection. Failure to completely exit CLI leaves the CD 
// signal (DTR out from the GR/WEN is connected to DCD) disconnected from the real-time network connection state.
// 	Radio			Card		Color
//  DCD	Pin 1		DTR_TO		Yellow
//	DTR Pin 4		DCD_FROM	White
/* ---------------------------------------------------------- */
extern void dev_cli_disconnect()
{
	//Alert_Message( "dev_cli_disconnect" );

	// ----------
	
	UNS_32	connect_level, disconnect_level;
	
	// ----------
	
	if( comm_mngr.device_exchange_port == UPORT_A )
	{
		connect_level = port_device_table[ config_c.port_A_device_index ].dtr_level_to_connect;

		disconnect_level = (port_device_table[ config_c.port_A_device_index ].dtr_level_to_connect ^ 1);
	}
	else
	{
		connect_level = port_device_table[ config_c.port_B_device_index ].dtr_level_to_connect;

		disconnect_level = (port_device_table[ config_c.port_B_device_index ].dtr_level_to_connect ^ 1);
	}

	SetDTR( comm_mngr.device_exchange_port, disconnect_level );

	// 5/14/2015 mpd : 250 did not work. 500 works!
	vTaskDelay( MS_to_TICKS( 500 ) );
		
	SetDTR( comm_mngr.device_exchange_port, connect_level );

}


/* ---------------------------------------------------------- */
/*  				 COMM_MNGR Command Interface			  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void dev_setup_for_termination_string_hunt( const char* pcommand_str, const char* presponse_str)
{
	// 3/14/2014 rmd : This function is executed within the context of the comm_mngr task. We can
	// reference and change the device exchange variables as needed.

	DATA_HANDLE	ldh;

	//Alert_Message( "dev_setup_for_termination_string_hunt" );

	// 5/5/2015 mpd : Are we expecting a response?
	if( dev_state->command_will_respond )
	{
		// 2/13/2014 rmd : Clean and restart the port buffer and string hunt. Note this disables packet hunting mode.
		//
		// 10/27/2016 rmd : The device exchange process is ALWAYS under control of the comm_mngr 
		RCVD_DATA_enable_hunting_mode( comm_mngr.device_exchange_port, PORT_HUNT_FOR_SPECIFIED_TERMINATION, WHEN_STRING_FOUND_NOTIFY_THE_COMM_MNGR_TASK );
	}
	else
	{
		// 5/5/2015 mpd : This final command does not generate a response. Return to default hunting. This can only be 
		// done for the final command driven by the state machine since no COMM_MNGR timer will be set when no response 
		// is expected. Any additional commands will have to be initiated by the GUI in a new device exchange. 
		RCVD_DATA_enable_hunting_mode( comm_mngr.device_exchange_port, PORT_HUNT_FOR_PACKETS, NOT_STRING_HUNTING  );
	}

	// ----------
	
	ldh.dptr = (UNS_8*)pcommand_str;
	
	ldh.dlen = strlen( pcommand_str );
	
	#ifdef DEV_DEBUG_HUNT
	Alert_Message_va( "SENDING(%d): ->%s<-", ldh.dlen, ldh.dptr );
	#endif
		
	// 3/10/2015 mpd : Identify the termination string.
	SerDrvrVars_s[ comm_mngr.device_exchange_port ].UartRingBuffer_s.th.str_to_find = ( char* )presponse_str;

	SerDrvrVars_s[ comm_mngr.device_exchange_port ].UartRingBuffer_s.th.chars_to_match = strlen( presponse_str );

	// ----------
	 
	// 3/12/2015 mpd : Don't try to add a zero length command to the Xmit list. That triggers alerts in "comm_mngr". Also,
	// check for a special "DEV_EMPTY_STR" value used to flag the first I/O cycle.  The first read cycle sets up the hunt for
	// an input that is triggered by device initialization hardware - not a xmit command. Sending a command here for the
	// first cycle would trigger an additional unwanted device response.
	if( ( ldh.dlen > 0 ) && ( pcommand_str[ 0 ] != DEV_EMPTY_STR ) )
	{
		AddCopyOfBlockToXmitList( comm_mngr.device_exchange_port, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );
	}

}

/* ---------------------------------------------------------- */
static BOOL_32 dev_get_and_process_command( DEV_STATE_STRUCT *dev_state, const UNS_32 next_command, char *next_termination_str )
{
	char *command_ptr = NULL;
	BOOL_32 command_found = true;

	//Alert_Message( "dev_get_and_process_command" );

	// 6/11/2015 mpd : Several command separation delays have been relocated to this common location for consistency and
	// maintainability. This is a delay BEFORE sending a command to allow the device time to complete whatever was 
	// initiated by the previous command. The previous command sets the additional time required beyond the base time
	// for the specific device. Ideally, we would exit the COMM_MNGR task and reenter at this point after the delay. 
	// There are not enough of these devices sold at this time to justify that coding effort.

	// 6/8/2015 mpd : This history applies to WEN commands in the WRITE list. 
	// Trial and error timing adjustment have been made to see how much time the WEN needs between commands to 
	// consistently accept and process the subsequent command. 
	// 1. Without this delay, "Error executing command" occurs 50% of the time during a WEN WRITE.
	// 2. With a delay of 500ms, there were errors. Frequency was not recorded.
	// 3. With a 2 second delay, there were no errors 10 runs in a row. Trying 1 second.
	// 4. Occasional timeouts after "write" using a 1 second delay. Changed back to 2. 
	//    Note: At 2 seconds, "write" commands seem to respond quicker.
	// Over time, see if that 2 seconds can be reduced without errors.

	// 6/12/2015 mpd : Lantronix PremiereWave have some timing issues. 
	if( (dev_state->device_type == COMM_DEVICE_WEN_PREMIERWAVE) || (dev_state->device_type == COMM_DEVICE_GR_PREMIERWAVE) || (dev_state->device_type == COMM_DEVICE_EN_PREMIERWAVE))
	{
		vTaskDelay( MS_to_TICKS( DEV_COMMAND_SEPARATION_BASE_TIMER_MS + dev_state->command_separation ) );
	}

	// 3/19/2015 mpd : Get the command string.
	if( ( command_ptr = ( device_handler[ dev_state->device_type ].get_command_text )( dev_state, next_command ) ) != NULL )
	{
		// 3/10/2015 mpd : Set up the string hunt.
		dev_setup_for_termination_string_hunt( command_ptr, next_termination_str );  

		#ifdef DEV_DEBUG_MEM_MALLOC
		Alert_Message_va( "GET Mem Freed: %p", command_ptr );
		#endif

		// 3/18/2015 mpd : Free the memory obtained by "en_get_command_text()".
		mem_free( command_ptr );
	}
	else
	{
		command_found = false;
	}

	return ( command_found );
}

/* ---------------------------------------------------------- */
extern void dev_update_info( DEV_STATE_STRUCT *dev_state, const char *info_str, UNS_32 devex_key )
{
	//Alert_Message( "dev_update_info_and_progress" );

	strlcpy( dev_state->info_text, info_str, sizeof( dev_state->info_text ) );                
	COMM_MNGR_device_exchange_results_to_key_process_task( devex_key );

}

/* ---------------------------------------------------------- */
/*  			  Operation Initialization  				  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void dev_set_read_operation( DEV_STATE_STRUCT *dev_state, UNS_32 operation )
{
	//Alert_Message( "dev_set_read_operation" );

	dev_state->operation = operation;

	strlcpy( dev_state->operation_text, DEV_READ_TEXT_STR, sizeof( dev_state->operation_text ) );                

	// 4/13/2015 mpd : Send a status message to the key task.
	dev_update_info( dev_state, "Beginning read...", DEVICE_EXCHANGE_KEY_read_settings_in_progress );

	// 3/12/2015 mpd : Reset the read list to the beginning. This is used here at initialization and by the read state
	// machine to navigate through all the device menus and process responses.
	dev_state->read_list_index = 0;

	// 3/20/2015 mpd : Initialize the WRITE index even though we will not be using it at the moment.
	dev_state->write_list_index = 0;

	// 5/5/2015 mpd : The Users cannot modify any programmable values on the GR PremierWave radio. There is no need for 
	// a "dynamic_pvs" for this device.

	// 3/20/2015 mpd : Setup the state machine entry point.
	comm_mngr.device_exchange_state = DEV_WAITING_FOR_RESPONSE;

	// 6/10/2015 mpd : Select the proper list based on the device and operation.
	if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
	{
		dev_state->list_ptr = &en_setup_read_list[0];
		dev_state->list_len = en_sizeof_setup_read_list();
	}
	else if( dev_state->device_type == COMM_DEVICE_WEN_PREMIERWAVE )
	{
		dev_state->list_ptr = &wen_read_list[0];
		dev_state->list_len = wen_sizeof_read_list();
	}
	else if( dev_state->device_type == COMM_DEVICE_EN_PREMIERWAVE )
	{
		dev_state->list_ptr = &PW_XE_read_list[0];
		dev_state->list_len = PW_XE_sizeof_read_list();
	}
	else if( dev_state->device_type == COMM_DEVICE_WEN_WIBOX )
	{
		dev_state->list_ptr = &WIBOX_setup_read_list[0];
		dev_state->list_len = WIBOX_sizeof_setup_read_list();
	}
	else // if( dev_state->device_type == COMM_DEVICE_GR_PREMIERWAVE )
	{
		dev_state->list_ptr = &gr_xml_read_list[0];
		dev_state->list_len = gr_sizeof_read_list();
	}

	// 6/2/2015 mpd : TBD: While the WIFI Settings screen is under construction, there is excessive exiting back to the
	// WEN Programming screen. Since there is no save on the WIFI screen, any changes made to those settings would be 
	// lost if we kept refreshing values from the programming structure every time we went back in. Use this flag to 
	// only update the GuiVars if there is new data from the device.
	dev_state->gui_has_latest_data = false;

}

/* ---------------------------------------------------------- */
static void dev_set_write_operation( DEV_STATE_STRUCT *dev_state )
{
	//Alert_Message( "dev_set_write_operation" );

	dev_state->operation = DEV_WRITE_OPERATION;

	strlcpy( dev_state->operation_text, DEV_WRITE_TEXT_STR, sizeof( dev_state->operation_text ) );                

	// 4/13/2015 mpd : Send a status message to the key task.
	dev_update_info( dev_state, "Beginning write...", DEVICE_EXCHANGE_KEY_write_settings_in_progress );

	// 6/9/2015 mpd : TBD: EN had multiple values. Drop them unless there is a good reason to keep them around. 
	//if( dev_state->dhcp_enabled )
	//{
	//	strlcpy( dev_state->operation_text, EN_DHCP_WRITE_TEXT_STR, sizeof( dev_state->operation_text ) );                
	//	strlcpy( dev_state->info_text, "Beginning DHCP write...", sizeof( dev_state->info_text ) );                
	//}
	//else
	//{
	//	strlcpy( dev_state->operation_text, EN_STATIC_WRITE_TEXT_STR, sizeof( dev_state->operation_text ) );                
	//	strlcpy( dev_state->info_text, "Beginning STATIC IP write...", sizeof( dev_state->info_text ) );                
	//}
		
	// 3/12/2015 mpd : Reset the write list to the beginning. This is used here at initialization and by the read state
	// machine to navigate through all the device menus and process responses.
	dev_state->write_list_index = 0;

	// 3/20/2015 mpd : Initialize the READ index even though we will not be using it at the moment.
	dev_state->read_list_index = 0;

	// 5/5/2015 mpd : The Users cannot modify any programmable values on the GR PremierWave radio. There is no need for 
	// a "dynamic_pvs", or a copy function to collect dynamic values before the WRITE operation.

	// 3/20/2015 mpd : Setup the state machine entry point.
	comm_mngr.device_exchange_state = DEV_WAITING_FOR_RESPONSE;

	// 5/6/2015 mpd : WRITE operations are followed by a READ to verify the values were successfully set. The final info 
	// message after a READ or WRITE command needs to reflect what was requested, not the status of the final READ 
	// operation. Use this flag to get it right.
	dev_state->read_after_a_write = true;

	// 6/10/2015 mpd : Select the proper list based on the device and operation.
	if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
	{
		// 4/13/2015 mpd : Copy all the active values to the static structure.
		en_copy_active_values_to_static_values( dev_state );

		// 5/20/2015 mpd : TBD: MONITOR MODE READ after every WRITE is disabled. This flag may be needed by the new 
		// diagnostics screen.
		//en_monitor_mode_after_a_write = true;

		if( dev_state->dhcp_enabled )
		{
			dev_state->list_ptr = &en_dhcp_write_list[0];
			dev_state->list_len = en_sizeof_dhcp_write_list();
		}
		else
		{
			dev_state->list_ptr = &en_static_write_list[0];
			dev_state->list_len = en_sizeof_static_write_list();
		}
	}
	else if( dev_state->device_type == COMM_DEVICE_WEN_PREMIERWAVE )
	{
		dev_state->list_ptr = &wen_write_list[0];
		dev_state->list_len = wen_sizeof_write_list();
	}
	else if( dev_state->device_type == COMM_DEVICE_EN_PREMIERWAVE )
	{
		dev_state->list_ptr = &PW_XE_write_list[0];
		dev_state->list_len = PW_XE_sizeof_write_list();
	}
	else if( dev_state->device_type == COMM_DEVICE_WEN_WIBOX )
	{
		// 3/28/2016 ajv : Copy all the active values to the static structure.
		WIBOX_copy_active_values_to_static_values( dev_state );

		if( dev_state->dhcp_enabled )
		{
			dev_state->list_ptr = &WIBOX_dhcp_write_list[0];
			dev_state->list_len = WIBOX_sizeof_dhcp_write_list();
		}
		else
		{
			dev_state->list_ptr = &WIBOX_static_write_list[0];
			dev_state->list_len = WIBOX_sizeof_static_write_list();
		}
	}
	else // if( dev_state->device_type == COMM_DEVICE_GR_PREMIERWAVE )
	{
		// 6/2/2015 mpd : FUTURE: This GR list is for the diagnostic screen.
		dev_state->list_ptr = &gr_xml_write_list[0];
		dev_state->list_len = gr_sizeof_write_list();
	}

	// 6/17/2015 mpd : TODO: This will be done by the READ following this WRITE. During development, that READ is turned 
	// off. Do it here.
	dev_state->gui_has_latest_data = false;
}

/* ---------------------------------------------------------- */
/*  				  State Struct Handling 				  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void dev_set_state_struct_for_new_device_exchange( DEV_STATE_STRUCT *dev_state )
{
	//Alert_Message( "dev_set_state_struct_for_new_device_exchange" );

	// 3/23/2015 mpd : These are fields that need to be set each time the device programming screen is entered or a 
	// button is selected by the user.

	dev_state->error_code = DEV_ERROR_NONE;

	// 4/16/2015 mpd : Clear any stale text messages.
	strlcpy( dev_state->operation_text, "", sizeof( dev_state->operation_text ) );                

	if( comm_mngr.device_exchange_initial_event == COMM_MNGR_EVENT_request_write_device_settings )
	{
		// 4/13/2015 mpd : Clear the user messages.
		dev_update_info( dev_state, "", DEVICE_EXCHANGE_KEY_write_settings_in_progress );

		( device_handler[ dev_state->device_type ].set_write_operation )( dev_state );

		//Alert_Message( "DEVEX: WRITE received from COMM_MNGR" );
	}
	else 
	{
		// 4/13/2015 mpd : Clear the user messages.
		dev_update_info( dev_state, "", DEVICE_EXCHANGE_KEY_read_settings_in_progress );

		if( comm_mngr.device_exchange_initial_event == COMM_MNGR_EVENT_request_read_monitor_device_settings )
		{
			// 5/1/2015 mpd : There is no analysis performed on the MONITOR MODE read values. Don't reset the 
			// "dev_state->device_settings_are_valid" flag here.

			( device_handler[ dev_state->device_type ].set_read_operation )( dev_state, DEV_READ_MONITOR_OPERATION );
			//Alert_Message( "DEVEX: READ MONITOR received from COMM_MNGR" );
		}
		else
		{
			// 5/1/2015 mpd : Assume the worst. Analysis after the first SETUP MODE read will set this to the correct value.
			dev_state->device_settings_are_valid = false;

			( device_handler[ dev_state->device_type ].set_read_operation )( dev_state, DEV_READ_OPERATION );

			//Alert_Message( "DEVEX: READ received from COMM_MNGR" );
		}

		// 5/13/2015 mpd : Assume the worst. Only do this for READ. The WRITE operation needs this info.
		dev_state->acceptable_version = false;
	}

	// 3/19/2015 mpd : Assume the worst. Analysis after a WRITE will set this to the correct value.
	dev_state->programming_successful = false;

	// 6/11/2015 mpd : Initialize the additional command separation value for the first command. 
	dev_state->command_separation = DEV_COMMAND_SEPARATION_NO_EXTRA_TIMER_MS;

	// 5/5/2015 mpd : Reset the loop counters; 
	dev_state->startup_loop_count = 0;
	dev_state->network_loop_count = 0;

}

/* ---------------------------------------------------------- */
static void dev_initialize_state_struct( DEV_STATE_STRUCT *dev_state )
{
	//Alert_Message( "dev_initialize_state_struct" );

	dev_state->error_code = DEV_ERROR_NONE;

	// 5/20/2015 mpd : Determine the device type.
	if( comm_mngr.device_exchange_port == UPORT_A )
	{
		dev_state->device_type = config_c.port_A_device_index;
	}
	else
	{
		dev_state->device_type = config_c.port_B_device_index;
	}

	// 5/27/2015 mpd : Clear pointers to device specific structures.
	dev_state->dev_details = NULL;
	dev_state->wen_details = NULL;
	dev_state->PW_XE_details = NULL;
	dev_state->en_details  = NULL;
	dev_state->en_active_pvs  = NULL;
	dev_state->en_static_pvs  = NULL;
	dev_state->wibox_active_pvs  = NULL;
	dev_state->wibox_static_pvs  = NULL;

	// 5/6/2015 mpd : WRITE operations are followed by a READ to verify the values were successfully set. The final info 
	// message after a READ or WRITE command needs to reflect what was requested, not the status of the final READ 
	// operation. Use this flag to get it right. The initial operation upon entering the screen is a READ that does not 
	// follow a WRITE.
	dev_state->read_after_a_write = false;

	// 4/21/2015 mpd : Set some reasonable default values.
	dev_state->dhcp_enabled = true;

	// 4/21/2015 mpd : Clear all the state struct string values.
	strlcpy( dev_state->model,            "", sizeof( dev_state->model ) ); 
	strlcpy( dev_state->firmware_version, "", sizeof( dev_state->firmware_version ) ); 
	strlcpy( dev_state->serial_number,    "", sizeof( dev_state->serial_number ) );                

	// 5/18/2015 mpd : Perform initialization unique to the device type.
	( device_handler[ dev_state->device_type ].initialize_detail_struct )( dev_state );
	           
}

/* ---------------------------------------------------------- */
static BOOL_32 dev_verify_state_struct( DEV_STATE_STRUCT *dev_state )
{
	BOOL_32 valid_state_struct = false;

	//Alert_Message( "dev_verify_state_struct" );

	if( dev_state->device_type == COMM_DEVICE_EN_UDS1100 )
	{
		// 3/23/2015 mpd : Verify the "active_pvs" pointer.
		if( dev_state->en_active_pvs != NULL )
		{
			// 3/23/2015 mpd : Verify the "pvs_token".
			if( strncmp( dev_state->en_active_pvs->pvs_token, EN_PVS_TOKEN_STR, EN_PVS_TOKEN_LEN ) == NULL )
			{
				// 3/23/2015 mpd : This is a valid "dynamic_pvs" structure from earlier excution on this screen. 

				// 3/23/2015 mpd : Verify the "static_pvs" pointer.
				if( dev_state->en_static_pvs != NULL )
				{
					// 3/23/2015 mpd : Verify the "pvs_token".
					if( strncmp( dev_state->en_static_pvs->pvs_token, EN_PVS_TOKEN_STR, EN_PVS_TOKEN_LEN ) == NULL )
					{
						valid_state_struct = true;
					}
				}
			}
		}
	}
	else if( dev_state->device_type == COMM_DEVICE_WEN_WIBOX )
	{
		// 3/29/2016 ajv : Verify the active PVS and static PVS pointers and tokens.
		if( (dev_state->wibox_active_pvs != NULL) && (strncmp(dev_state->wibox_active_pvs->pvs_token, WIBOX_PVS_TOKEN_STR, WIBOX_PVS_TOKEN_LEN) == NULL) &&
			(dev_state->wibox_static_pvs != NULL) && (strncmp(dev_state->wibox_static_pvs->pvs_token, WIBOX_PVS_TOKEN_STR, WIBOX_PVS_TOKEN_LEN) == NULL) )
		{
			valid_state_struct = (true);
		}
	}
	else // GR and WEN
	{
		// 5/5/2015 mpd : For GR and WEN, the state structure does not have valid PVS pointers or tokens to verify. Use 
		// the operation field as a sanity check. It has a limited number of legal values, and none of them are zero.
		if( ( dev_state->operation == DEV_IDLE_OPERATION ) ||
			( dev_state->operation == DEV_READ_OPERATION ) ||
			( dev_state->operation == DEV_READ_MONITOR_OPERATION ) ||
			( dev_state->operation == DEV_WRITE_OPERATION ) )
		{
			valid_state_struct = true;
		}
	}

	return ( valid_state_struct );
}

/* ---------------------------------------------------------- */
/*  	 READ/WRITE Item processing and the State Machine     */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static BOOL_32 dev_start_prompt_received( DEV_STATE_STRUCT *dev_state, COMM_MNGR_TASK_QUEUE_STRUCT *q_msg, char *prompt_str )
{
	BOOL_32 start_prompt_received = false;

	//Alert_Message( "dev_start_prompt_received" );

	if( q_msg->event == COMM_MNGR_EVENT_device_exchange_response_arrived )
	{
		if( ( q_msg->dh.dptr != NULL ) && ( q_msg->dh.dlen > 0 ) )
		{
			if( strstr( (char*)q_msg->dh.dptr, prompt_str ) != NULL ) 
			{
				start_prompt_received = true;
			}
		}
	}
	// 5/5/2015 mpd : There is nothing to do here for timeout events. The state machine will initiate another I/O
	// attempt.

	return ( start_prompt_received );
}

#if 0
/* ---------------------------------------------------------- */
static BOOL_32 dev_ready_prompt_received( DEV_STATE_STRUCT *dev_state, COMM_MNGR_EVENT_QUEUE_STRUCT *q_msg )
{
	BOOL_32 ready_prompt_received = false;

	//Alert_Message( "dev_ready_prompt_received" );

	if( q_msg->event == COMM_MNGR_EVENT_device_exchange_response_arrived )
	{
		// 6/5/2015 mpd : The double "##" within the first second seems to be a PuTTY quirk. It does not exist in the 
		// response buffer, so we can't test responses until it goes away. It does not appear on a direct PuTTY to CLI 
		// connection on a null modem cable.
		if( ( q_msg->dh.dptr != NULL ) && ( q_msg->dh.dlen > 0 ) )
		{
			// 6/5/2015 mpd : Is the device still sending "(enable)##"?
			if( strstr( (char*)q_msg->dh.dptr, DEV_TERM_NOT_READY_STR ) != NULL ) 
			{
				ready_prompt_received = false;
			}
			else
			{
				ready_prompt_received = true;
			}
		}
	}
	// 5/5/2015 mpd : There is nothing to do here for timeout events. The state machine will initiate another I/O
	// attempt.

	return ( ready_prompt_received );
}
#endif

/* ---------------------------------------------------------- */
static void dev_increment_list_item_pointer( DEV_STATE_STRUCT *dev_state, const UNS_32 count )
{
	//Alert_Message( "dev_increment_list_item_pointer" );

	// 5/27/2015 mpd : The "dev_state->list_ptr" pointer is now used to step through the GR and WEN lists ( EN and SR 
	// can migrate to this code as time allows). Incrementing the single pointer gets us to the next list item in 
	// whatever array we are dealing with. Don't ignore the old index counters. They are still useful for alert 
	// messages! 
	switch( dev_state->operation )
	{
		case DEV_READ_OPERATION:
		case DEV_READ_MONITOR_OPERATION:
			dev_state->read_list_index += count;
			dev_state->list_ptr += count;
			// 6/17/2015 mpd : WARNING: There is no protection here for counts exceeding the list size. This function
			// is called from very specific locations in the list with a constant count. It is not safe for dynamic
			// counts! 
			break;

		case DEV_WRITE_OPERATION:
			dev_state->write_list_index += count;
			dev_state->list_ptr += count;
			// 6/17/2015 mpd : WARNING: There is no protection here for counts exceeding the list size. This function
			// is called from very specific locations in the list with a constant count. It is not safe for dynamic
			// counts! 
			break;

		case DEV_IDLE_OPERATION:
		default:
			break;
	}

	// 6/18/2015 mpd : This function can be used to modify the path through the list reducing the need for multiple
	// lists. See the DEV_COMMAND_WEP_K1 for a usage example.

	// 5/27/2015 mpd : NOTE: The contents of the lists determine the end, not "dev_state->list_ptr" or 
	// "dev_state->list_len" values. Testing "dev_state->list_ptr" for overruns would require storing either a start or
	// end value to compare. 

}

/* ---------------------------------------------------------- */
static void dev_decrement_list_item_pointer( DEV_STATE_STRUCT *dev_state )
{
	//Alert_Message( "dev_decrement_list_item_pointer" );

	// 5/27/2015 mpd : The "dev_state->list_ptr" pointer is now used to step through the GR and WEN lists ( EN and SR 
	// can migrate to this code as time allows). Decrementing the single pointer gets us to the previous list item in 
	// whatever array we are dealing with. Don't ignore the old index counters. They are still useful for alert 
	// messages! 

	// 6/8/2015 mpd : This function is necessary when a device responds with something we are not expecting. If we 
	// decide we can handle it and continue, we can't use the command we were ready to send to the expected response.
	// We have to send something to complete this unexpected I/O, then deal with the current item on our list. This will
	// happen after the state machine increments the list pointer.  
	switch( dev_state->operation )
	{
		case DEV_READ_OPERATION:
		case DEV_READ_MONITOR_OPERATION:
			// 6/8/2015 mpd : Don't go negative!
			if( dev_state->read_list_index > 0 )
			{
				dev_state->read_list_index--;
				dev_state->list_ptr--;
			}
			break;

		case DEV_WRITE_OPERATION:
			if( dev_state->write_list_index > 0 )
			{
				dev_state->write_list_index--;
				dev_state->list_ptr--;
			}
			break;

		case DEV_IDLE_OPERATION:
		default:
			break;
	}

}

#ifdef EN_DEBUG_PROCESS_LIST
/* ---------------------------------------------------------- */
static void dev_process_list_alert_messages( DEV_STATE_STRUCT *dev_state, const UNS_32 *next_command, char *next_termination_str )
{
	Alert_Message( "dev_process_list_alert_messages" );

	// 6/9/2015 mpd : Handle non-ascii commands. Unprintable characters in alert messages are not helpful. 
	if( next_command != DEV_COMMAND_END )
	{
		if( next_command == DEV_COMMAND_NONE )
		{
			Alert_Message_va( "Next CMD[0]: NONE  Next Term: ->%s<-", next_termination_str );
		}
		else if( next_command == DEV_COMMAND_CONTINUE_SETUP )
		{
			Alert_Message_va( "Next CMD[0]: CR  Next Term: ->%s<-", next_termination_str );
		}
		else
		{
			Alert_Message_va( "Next CMD[0]: ->%c<-  Next Term: ->%s<-", next_command, next_termination_str );
		}
	}

}
#endif

/* ---------------------------------------------------------- */
static BOOL_32 dev_process_list_send( DEV_STATE_STRUCT *dev_state, const UNS_32 *next_command, char *next_termination_str )
{
	BOOL_32 continue_cycle = true;

	// 6/8/2015 mpd : This function is the second half of what used to be called "dev_process_list()".

	// 2. The second operation sets up RCVD_DATA to wait for the next receive buffer. The list_item struct specifies the
	// command string needed to put the EN device into the requested menu and what string indicates the end of the device
	// response.

	#ifdef EN_DEBUG_PROCESS_LIST
		dev_process_list_alert_messages( dev_state, next_command, next_termination_str );
	#endif

	// 6/11/2015 mpd : The command separation delay has been moved to "dev_get_and_process_command()".

	if( !dev_get_and_process_command( dev_state, ( const UNS_32 )next_command, next_termination_str ) )
	{
		continue_cycle = false;
	}

	return ( continue_cycle );
}

/* ---------------------------------------------------------- */
static BOOL_32 dev_handle_alternate_response( DEV_STATE_STRUCT *dev_state, COMM_MNGR_TASK_QUEUE_STRUCT *q_msg, const DEV_MENU_ITEM *list_item )
{
	BOOL_32 alternate_available = false;
	UNS_32  command;

	// 6/8/2015 mpd : We did not get the response from the device we were expecting. This function provides a way to 
	// handle specific exceptions. "Handle" involves sending a command to the device. Once handled, we can continue 
	// processing the list. To continue where we were, a pointer decrement is required since the state machine will 
	// increment the list pointer before processing the next list item. Sending a command here without the decrement 
	// would skip sending the current list item command. An increment call here would skip the current list command as
	// well as the next.     

	// 6/8/2015 mpd : LIMITATION: We will never get here if RCVD_DATA is waiting for a terminator that never arrives
	// due to an alternate response. Use the simplest "next_termination_str" value possible that guarantees the entire
	// response has arrived, but does not other possibilities. Example: Using "?\r" as the terminator completes a 
	// RCVD_DATA response of "Your Choice?" as well as "Change DHCP device name (not set) ? (N) ?" Note that in this 
	// real example, using "?" as the terminator would not work. We would not see the trailing " (N) ?" in this response 
	// because RCVD_DATA would send us the response ending with the first "?". The rest of the response would be at the 
	// beginning of the next buffer where we probably won't expect it.

	// FORMAT:
	// else if ( ( the list specifies a string we really wanted ) &&
	//           ( we received a known alternate response ) )
	// {
	//     what to do with the alternate
	// }

	// 6/12/2015 mpd : Sending a CR to skip the new question will be done in most cases.
	command = DEV_COMMAND_CR;

	// 6/12/2015 mpd : EN UDS1100 added DNS Server functionality to the server menu in V6.11.0.0. Instead of the Telnet
	// question, there will be an additional "Y/N" question for DNS Server IP programming.  
	if( ( strstr( list_item->title_str, EN_SERVER_INPUT_TELNET_YN_STR) != NULL ) &&
		( find_string_in_block( (char*)q_msg->dh.dptr, EN_SERVER_INPUT_DNS_IP_YN_STR, q_msg->dh.dlen ) != NULL ) ) 
	{
		// 6/8/2015 mpd : We received the DNS IP question. A CR response will acknowledge the default.
		if( ( alternate_available = dev_process_list_send( dev_state, (const UNS_32 *)command, EN_CHANNEL_INPUT_QUESTION_STR ) ) )
		{
			// 6/8/2015 mpd : We want to attempt the current list item on the next cycle.
			dev_decrement_list_item_pointer( dev_state );
		}
	}

	// 6/8/2015 mpd : EN UDS1100 added DHCP FQDN functionality to the server menu in V6.11.0.0. Instead of the server
	// menu response, there will be an additional FQDN "Y/N" question to handle.  
	if( ( strstr( list_item->title_str, EN_CHANGE_MENU_CHOICE_STR) != NULL ) &&
		( find_string_in_block( (char*)q_msg->dh.dptr, EN_SERVER_INPUT_FQDN_YN_STR, q_msg->dh.dlen ) != NULL ) ) 
	{
		// 6/8/2015 mpd : We received the FQDN question. A CR response will acknowledge the default.
        if( ( alternate_available = dev_process_list_send( dev_state, (const UNS_32 *)command, EN_CHANNEL_INPUT_QUESTION_STR ) ) )
		{
			// 6/8/2015 mpd : We want to attempt the current list item on the next cycle.
			dev_decrement_list_item_pointer( dev_state );
		}
	}

	// 6/12/2015 mpd : EN UDS1100 added functionality to the channel menu in V6.11.0.0. Instead of the Auto Increment
	// response, there will be an additional "Show IP addr after 'RING'" "Y/N" question to handle.  
	if( ( strstr( list_item->title_str, EN_CHANNEL_INPUT_AUTO_INCR_STR) != NULL ) &&
		( find_string_in_block( (char*)q_msg->dh.dptr, EN_CHANNEL_INPUT_SHOW_IP_RING_STR, q_msg->dh.dlen ) != NULL ) ) 
	{
		// 6/8/2015 mpd : We received the Show IP question. A CR response will acknowledge the default.
		if( ( alternate_available = dev_process_list_send( dev_state, (const UNS_32 *)command, EN_CHANNEL_INPUT_QUESTION_STR ) ) )
		{
			// 6/8/2015 mpd : We want to attempt the current list item on the next cycle.
			dev_decrement_list_item_pointer( dev_state );
		}
	}

	// 6/8/2015 mpd : State what we are testing for here.
	else if( 0 )
	{
		// 6/8/2015 mpd : Test for and handle additional exception.
	}

	return( alternate_available );

}

/* ---------------------------------------------------------- */
static BOOL_32 dev_process_list_receive( DEV_STATE_STRUCT *dev_state, COMM_MNGR_TASK_QUEUE_STRUCT *q_msg, const DEV_MENU_ITEM *list_item )
{
	char *title_ptr = NULL;
	UNS_32 list_index;
	BOOL_32 continue_cycle = true;

	#ifdef DEV_FIELD_DEBUGGING_ALERTS
	BOOL_32 timeout_on_first = false;
	#endif

	#ifdef DEV_VERBOSE_ALERTS
	char error_text[DEV_INFO_TEXT_LEN];
	#endif

	// 3/12/2015 mpd : This function performs two separate operations.
	// 
	// 1. The first operation deals with the current contents of the receive buffer. The list_item struct identifies the
	// title string and the appropriate handler for the work.
	// 
	// 2. The second operation sets up RCVD_DATA to wait for the next receive buffer. The list_item struct specifies the
	// command string needed to put the EN device into the requested menu and what string indicates the end of the device
	// response.

	#ifdef EN_DEBUG_PROCESS_LIST
	Alert_Message( "dev_process_list_receive" );
	#endif

	// 6/10/2015 mpd : There are numerous decisions based on this. Handle it once here.
	if( dev_state->operation == DEV_WRITE_OPERATION )
	{
		list_index = dev_state->write_list_index;
	}
	else
	{
		list_index = dev_state->read_list_index;
	}

	// 3/12/2015 mpd : FIRST OPERATION: Deal with the existing receive buffer.

	if( q_msg->event == COMM_MNGR_EVENT_device_exchange_response_arrived )
	{
		#ifdef DEV_FIELD_DEBUGGING_ALERTS
			if( list_index == DEV_FIRST_INDEX_AFTER_ENTRY )
			{
				Alert_Message_va( "First %s Command Delay: %d ms", dev_state->operation_text, ( ( my_tick_count - dev_state->first_command_ticks ) * DEV_TICKS_PER_MS ) );
			}
		#endif

		// 3/23/2015 mpd : The memory handed to us by "rcvd_data" is our's to keep until we are done parsing. Better still, we
		// free it at the end of parsing before a user can create a "key timeout" by modifying a field and going home for the
		// night. Make convenient local copies of the pointer and response length so the rest of EN device code does not
		// reference "comm_mngr" variables.
		if( ( q_msg->dh.dptr != NULL ) && ( q_msg->dh.dlen > 0 ) )
		{
			#ifdef EN_DEBUG_MEM_MALLOC
			Alert_Message_va( "RCVD Mem Given( %d ): %p", q_msg->dh.dlen, (char*)(q_msg->dh.dptr) );
			#endif

			//Alert_Message( list_item->title_str );

			// 3/11/2015 mpd : Look for the response title string in the data buffer.

			// 4/22/2015 mpd : SR Radios added extra '\0' characters to the response which made "strstr()" end early. A 
			// custom search based on buffer length was created to solve this problem. Continue to use it for the 
			// Lantronix device to be consistent.
			if( ( title_ptr = find_string_in_block( (char*)q_msg->dh.dptr, list_item->title_str, q_msg->dh.dlen ) ) != NULL ) 
			{
				// 5/26/2015 mpd : The original usage of "title_str" used too much data space. We can verify the correct 
				// response using the termination string or some other brief string within the response without 
				// requiring strings that match the beginning of the response. Since the characters we need can now 
				// preceed the "title_str", we can no longer start our search using the "title_ptr". Start at the 
				// beginning of the buffer. 
				dev_state->resp_ptr = (char*)q_msg->dh.dptr;
				dev_state->resp_len =  q_msg->dh.dlen;

				#ifdef DEV_DEBUG_PROCESS_LIST
					Alert_Message_va( "%s: ->%s<- found", dev_state->operation_text, list_item->title_str );
				#endif

				#ifdef DEV_VERBOSE_ALERTS 
					// 6/9/2015 mpd : WEN devices accept commands without successfully performing the requested 
					// operation. "Error executing command" is returned in the response buffer. Notify the user if this 
					// happens. Include the command that failed.
					if( (dev_state->device_type == COMM_DEVICE_WEN_PREMIERWAVE) || (dev_state->device_type == COMM_DEVICE_EN_PREMIERWAVE) )
					{
						if( ( title_ptr = find_string_in_block( (char*)q_msg->dh.dptr, DEV_X_CP_DUMP_FAILURE_STR, q_msg->dh.dlen ) ) != NULL ) 
						{
							strtok( (char*)q_msg->dh.dptr, "\r\n" );
							snprintf( error_text, sizeof( error_text ), "CMD Failed: %s", (char*)q_msg->dh.dptr );
							dev_handle_error( dev_state, DEV_ERROR_EXECUTING_COMMAND, DEV_ERROR_LEVEL_SERIOUS, error_text );
						}
					}
				#endif

				// 3/20/2015 mpd : Call the response handler to do something with this buffer. This is the meat!
				if( list_item->dev_response_handler != NULL )
				{
					( list_item->dev_response_handler )( dev_state );
				}

				// 6/8/2015 mpd : SECOND OPERATION: Start the next COMM_MNGR I/O cycle. RCVD_DATA will wait for the next 
				// receive buffer.
				continue_cycle = dev_process_list_send( dev_state, (const UNS_32 *)list_item->next_command, list_item->next_termination_str );
			}

			// 6/1/2015 mpd : Lantronix just released new EN firmware that adds a FQDN option in the middle of our
			// WRITE list. If they see no problem doing this, we need to be ready for it. At this point in code, we
			// received a response because the terminator matched, but we did not get what we expected. Go off to a
			// special handler to see if we can recover and continue.
			else if( ( continue_cycle = dev_handle_alternate_response( dev_state, q_msg, list_item ) ) )
			{
				// 6/8/2015 mpd : Nothing to do here. The "dev_handle_alternate_response()" function will start the next 
				// COMM_MNGR I/O cycle if this is something we can handle. 
			}

			// 3/23/2015 mpd : The string we are looking for was not found in the response buffer and 
			// "dev_handle_alternate_response()" is not equipped to handle what we received. Die gracefully.
			else 
			{
				#ifdef DEV_VERBOSE_ALERTS
					Alert_Message_va( "%s: ->%s<- not found", dev_state->operation_text, list_item->title_str );
				#else
					Alert_Message( "DEV13" );
				#endif
				continue_cycle = false;
			}

			// 3/11/2015 mpd : Free the memory obained by the receive task in all cases.
			#ifdef DEV_DEBUG_MEM_MALLOC
				Alert_Message_va( "RCVD Mem Freed( %d ): %p", q_msg->dh.dlen, (char*)(q_msg->dh.dptr) );
			#endif

			mem_free( q_msg->dh.dptr );
		}

	} // END COMM_MNGR_EVENT_device_exchange_response_arrived

	else if( q_msg->event == COMM_MNGR_EVENT_device_exchange_response_timer_expired )
	{
		// 5/14/2015 mpd : Some commands will not respond (e.g. GR baud rate change). Is this one of them,
		// or is it a legitimate timeout?
		if( dev_state->command_will_respond )
		{
			// 5/22/2015 mpd : This is an unexpected timeout.

			#ifdef DEV_VERBOSE_ALERTS
				// 6/10/2015 mpd : Include the READ or WRITE list index in the error message for debugging.
				snprintf( error_text, sizeof( error_text ), "COMM MNGR Timer Expired: %d", list_index );
				dev_handle_error( dev_state, DEV_ERROR_COMM_MNGR_TIMEOUT, DEV_ERROR_LEVEL_FATAL, error_text );
			#endif

			#ifdef DEV_FIELD_DEBUGGING_ALERTS
				if( list_index == DEV_FIRST_INDEX_AFTER_ENTRY )
				{
					timeout_on_first = true;
					Alert_Message_va( "First %s Command Timeout: %d ms", dev_state->operation_text, ( ( my_tick_count - dev_state->first_command_ticks ) * DEV_TICKS_PER_MS ) );
				
					// 5/20/2015 mpd : Power-cycle the device. This is a special case for timeouts on the first command 
					// after entering CLI.
					port_device_table[ dev_state->device_type ].__power_device_ptr( comm_mngr.device_exchange_port, false );
					vTaskDelay( MS_to_TICKS(250) );
					port_device_table[ dev_state->device_type ].__power_device_ptr( comm_mngr.device_exchange_port, true );
					vTaskDelay( MS_to_TICKS(2000) );
				}
			#endif
			
			continue_cycle = false;
		} 
		else
		{
			// 5/22/2015 mpd : We expected this timeout. A shorter COMM_MNGR timer was set when the command was 
			// sent. It has expired as expected. Continue the device exchange as though we received a response. 
			if( list_item->dev_response_handler != NULL )
			{
				(list_item->dev_response_handler)( dev_state );
			}

			// 6/8/2015 mpd : SECOND OPERATION: Start the next COMM_MNGR I/O cycle. RCVD_DATA will wait for the next 
			// receive buffer.
			continue_cycle = dev_process_list_send( dev_state, (const UNS_32 *)list_item->next_command, list_item->next_termination_str );
		}

	} // END OMM_MNGR_EVENT_device_exchange_response_timer_expired

	else
	{
		dev_handle_error( dev_state, DEV_ERROR_COMM_MNGR_UNKNOWN, DEV_ERROR_LEVEL_FATAL, "Unknown event" );

		#ifdef DEV_FIELD_DEBUGGING_ALERTS
			if( list_index == DEV_FIRST_INDEX_AFTER_ENTRY )
			{
				// 5/20/2015 mpd : Use tick count for better granularity.
				Alert_Message_va( "First %s Unknown Delay: %d ms", dev_state->operation_text, ( ( my_tick_count - dev_state->first_command_ticks ) * DEV_TICKS_PER_MS ) );
			}
		#endif

		continue_cycle = false;
	}

	return ( continue_cycle );
}

/* ---------------------------------------------------------- */
static void dev_state_machine( DEV_STATE_STRUCT *dev_state, COMM_MNGR_TASK_QUEUE_STRUCT *pq_msg )
{
	// 3/14/2014 rmd : This function executed within the context of the comm_mngr task. We can
	// reference and change the device exchange variables as needed.

	// ----------

	//UNS_32 	i;
	UNS_32	device_exchange_result;
	UNS_32	device_exchange_result_ok;
	UNS_32	device_exchange_error;
	UNS_32	device_exchange_progress;
	BOOL_32 list_completed = false;

	//char debug_text[64];

	// ----------

	//Alert_Message( "dev_state_machine" );

	// 6/4/2015 mpd : Save a few "if" statements by using generic response variables.
	if( ( dev_state->operation == DEV_WRITE_OPERATION ) || ( dev_state->read_after_a_write ) )
	{
		device_exchange_result_ok = DEVICE_EXCHANGE_KEY_write_settings_completed_ok;
		device_exchange_error     = DEVICE_EXCHANGE_KEY_write_settings_error;              
		device_exchange_progress  = DEVICE_EXCHANGE_KEY_write_settings_in_progress;      
	}
	else
	{
		device_exchange_result_ok = DEVICE_EXCHANGE_KEY_read_settings_completed_ok;
		device_exchange_error     = DEVICE_EXCHANGE_KEY_read_settings_error;                           
		device_exchange_progress  = DEVICE_EXCHANGE_KEY_read_settings_in_progress;                   
	}

	switch( comm_mngr.device_exchange_state )
	{
		// 5/5/2015 mpd : Some devices need special handling to get out of the starting block. This state has been added 
		// to simulate the auto-repeat keyboard function until the device makes it's initial response. That takes about 
		// 10 seconds after the power cycle. 
		case DEV_WAITING_FOR_RESPONSE:

			#ifdef DEV_DEBUG_STATE
				Alert_Message( "Waiting for response" );
			#endif

			// 6/11/2015 mpd : DEBUG CODE.
			//if( SerDrvrVars_s[ 1 ].UartRingBuffer_s.ring[0] != NULL )
			//{
			//	strlcpy(  debug_text, (char *)SerDrvrVars_s[ 1 ].UartRingBuffer_s.ring, sizeof( debug_text ) );
			//	Alert_Message_va( "RCVD: %s", debug_text );
			//}

			if( dev_start_prompt_received( dev_state, pq_msg, dev_state->list_ptr->next_termination_str ) )
			{
				// 5/7/2015 mpd : The GR is typically responding in 10.8 to 10.9 seconds (86 to 87 counts).

				#ifdef DEV_DEBUG_STATE
					Alert_Message_va( "GR responded on count: %d", dev_startup_loop_count );
				#endif

				// 6/10/2015 mpd : We are moving on. Set the next device exchange state.
				if( dev_state->operation == DEV_WRITE_OPERATION )
				{
					comm_mngr.device_exchange_state = DEV_DEVEX_WRITE;
				}
				else
				{
					comm_mngr.device_exchange_state = DEV_DEVEX_READ;
				}

				// 6/10/2015 mpd : Step to entry 1 of the list. Increment the pointer. 
				dev_increment_list_item_pointer( dev_state, 1 );

				// 6/10/2015 mpd : If necessary, do something with this buffer before moving on.
				if( dev_state->list_ptr->dev_response_handler != NULL )
				{
					dev_state->resp_ptr = (char*)pq_msg->dh.dptr;
					dev_state->resp_len =  pq_msg->dh.dlen;
					( dev_state->list_ptr->dev_response_handler )( dev_state );
				}

				// 6/5/2015 mpd : Reset the loop counter so we can reuse it for DEV_WAITING_FOR_READY.
				dev_state->startup_loop_count = 0;

				// 5/5/2015 mpd : The device has finally responded. Give it the next magic string so we can do some real work.
				dev_get_and_process_command( dev_state, dev_state->list_ptr->next_command, dev_state->list_ptr->next_termination_str );

				// 5/5/2015 mpd : We go the read or write state from here where "dev_process_list()" will handle all
				// subsequent commands. 
			}
			else
			{
				// 5/5/2015 mpd : Don't do this forever. Stop when we hit the max loop count.
				if( dev_state->startup_loop_count <= DEV_MAX_START_LOOP_COUNT )
				{
					dev_state->startup_loop_count++;

					// 6/10/2015 mpd : Stay on entry 0 of the list. Do not increment the pointer. 

					// 5/5/2015 mpd : Still waiting. Send another "!", "x", or "z".
					dev_get_and_process_command( dev_state, dev_state->list_ptr->next_command, dev_state->list_ptr->next_termination_str );

					// 5/5/2015 mpd : Set another short timer.
					if( (dev_state->device_type == COMM_DEVICE_EN_UDS1100) ||  (dev_state->device_type == COMM_DEVICE_WEN_WIBOX) )
					{
						xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LANTRONIX_COBOX_AUTO_REPEAT_TIMER_MS), portMAX_DELAY );
					}
					else
					{
						xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(DEV_AUTO_REPEAT_TIMER_MS), portMAX_DELAY );
					}
				}
				else
				{
					// 5/5/2015 mpd : We have tried long enough. Notify the user and return control of the keys.
					#ifdef DEV_VERBOSE_ALERTS
						Alert_Message_va( "Device not responding" );
					#else
						Alert_Message( "DEV14" );
					#endif

					dev_exit_device_mode();

					dev_update_info( dev_state, "Device not responding", device_exchange_error );
				}
			}
			break;

		#if 0
		case DEV_WAITING_FOR_NETWORK:

			#ifdef DEV_DEBUG_STATE
				Alert_Message( "Waiting for network" );
			#endif

			// 5/8/2015 mpd : With a SIM card in place, any command that involves WWAN0 info will timeout unless we have 
			// waited 30 seconds after the power-cycle for the host to connect us to the network. Wait here until we are 
			// connected or we hit the max wait time. NOTE: If there is no SIM card, a READ command would have responded 
			// within COMM_MNGR time limits. This implementation prevents that and makes users wait the full 
			// DEV_NETWORK_LOOP_COUNT period before acknowledging the failure. Oh well, that should be rare.
			
			// 5/8/2015 mpd : The CD signal is not functional in CLI mode. Checking for a connection is doomed. Use a
			// counter until some smarter method is discovered. 
			//if( PREMIERWAVEXC_is_connected( comm_mngr.device_exchange_port ) )

			if( dev_state->network_loop_count >= DEV_NETWORK_LOOP_COUNT )
			{
				// 5/7/2015 mpd : The network is typically connecting in 30 seconds. 
				
				#ifdef DEV_DEBUG_NET_WAIT
					Alert_Message_va( "Network connected on count: %d", dev_state->network_loop_count );
				#endif

				dev_update_info_and_progress( dev_state, "", "", device_exchange_progress );

				// 5/8/2015 mpd : The next response from the device will be handled by the DEV_DEVEX_READ state where
				// the "read_list_index" will step us through the rest of the "dev_read_list".
				//dev_state->read_list_index++;
				dev_increment_list_item_pointer( dev_state );

				#ifdef DEV_DEBUG_STATE
					Alert_Message_va( "Read List Index: %d", dev_state->read_list_index );
				#endif

				comm_mngr.device_exchange_state = DEV_DEVEX_READ;
			}
			else
			{
				// 5/8/2015 mpd : The network is still not connected. Keep the user informed.
				snprintf( dev_state->progress_text, sizeof( dev_state->progress_text ), "Seconds remaining: %d", ( DEV_NETWORK_LOOP_COUNT - dev_state->network_loop_count ) );                
				dev_update_info_and_progress( dev_state, "Waiting for network connection...", dev_state->progress_text, device_exchange_progress );

				// 5/8/2015 mpd : Delay a second before sending another CR. 
				vTaskDelay( MS_to_TICKS( DEV_NETWORK_TIMER_MS ) );

				dev_state->network_loop_count++;

				// 5/8/2015 mpd : The "read_list_index" does not get incremented in this wait state. Keep 
				// sending the same "CR" command until the network connects or we hit the max loop count.
			}

			if( dev_state->network_loop_count <= DEV_NETWORK_LOOP_COUNT )
			{
				// 5/8/2015 mpd : Process the list item. This may be the same "CR" command item 30 times, or it could be
				// the next item on the list as we transition back to the DEV_DEVEX_READ state. 
				list_completed = ( ( dev_process_list( dev_state, pq_msg, dev_state->list_ptr ) ) == false );

				// 5/8/2015 mpd : We better not hit the end. We are still near the top of the list!
				if( list_completed )
				{
					dev_exit_device_mode();

					#ifdef DEV_VERBOSE_ALERTS
						Alert_Message( "Unexpected wait loop failure." );
					#else
						Alert_Message( "DEV15" );
					#endif

					dev_update_info_and_progress( dev_state, "Wait loop fault", "", device_exchange_error );
				}
				else 
				{
					// 5/8/2015 mpd : Set the timer for another device exchange. We will return to this state.
					xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(DEV_COMM_TIMER_MS), portMAX_DELAY );
				}
			}

			#if 0
			// 5/11/2015 mpd : Save this in case DTR (CD) can be made to work in CLI mode.
			else
			{
				dev_exit_device_mode();
			
				// 5/8/2015 mpd : We have tried long enough. Notify the user and return control of the keys.
				#ifdef DEV_VERBOSE_ALERTS
					Alert_Message_va( "Network not responding" );
				#else
					Alert_Message( "DEV16" );
				#endif
			
				strlcpy( dev_state->info_text, "Network not responding", sizeof( dev_state->info_text ) );                
				strlcpy( dev_state->progress_text, "", sizeof( dev_state->progress_text ) );                
				COMM_MNGR_device_exchange_results_to_key_process_task( device_exchange_error );
			}
			#endif
			break;
		#endif

		#if 0
		case DEV_WAITING_FOR_READY:

			#ifdef DEV_DEBUG_STATE
				Alert_Message( "Waiting for ready" );
			#endif

			// 6/5/2015 mpd : The device responds with a double "##" for the first few seconds after power up. This
			// was hidden when we waited 60 seconds for a network connection. Commands sent during the first second 
			// do not always respond. Send CRs until we get a normal prompt. This is quick. No need to inform the user.

			if( dev_ready_prompt_received( dev_state, pq_msg ) )
			{
				if( dev_state->operation == DEV_WRITE_OPERATION )
				{
					comm_mngr.device_exchange_state = DEV_DEVEX_WRITE;
				}
				else
				{
					comm_mngr.device_exchange_state = DEV_DEVEX_READ;
				}

				// 6/5/2015 mpd : The device is finally ready. Send one more CR to get to the next device exchange state. 
				dev_get_and_process_command( dev_state, DEV_COMMAND_CR, DEV_TERM_RESP_STR );

				// 6/5/2015 mpd : We go the read or write state from here where "dev_process_list()" will handle all
				// subsequent commands. 
			}
			else
			{
				// 6/5/2015 mpd : Don't do this forever. Stop when we hit the max loop count.
				if( dev_state->startup_loop_count <= DEV_MAX_READY_LOOP_COUNT )
				{
					// 6/5/2015 mpd : This is different than DEV_WAITING_FOR_RESPONSE. The device is responding. There
					// will not be a timeout period we can use as our delay between I/O cycles. 
					vTaskDelay( MS_to_TICKS( DEV_READY_REPEAT_TIMER_MS ) );

					dev_state->startup_loop_count++;

					// 6/5/2015 mpd : Still waiting. Send another CR.
					dev_get_and_process_command( dev_state, DEV_COMMAND_CR, DEV_TERM_RESP_STR );

					// 6/5/2015 mpd : Set another short timer.
					xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(DEV_COMM_TIMER_MS), portMAX_DELAY );
				}
				else
				{
					// 6/5/2015 mpd : We have tried long enough. Notify the user and return control of the keys.
					#ifdef DEV_VERBOSE_ALERTS
						Alert_Message_va( "No ready prompt" );
					#else
						Alert_Message( "DEV17" );
					#endif

					dev_exit_device_mode();

					dev_update_info_and_progress( dev_state, "Device not ready", "", device_exchange_error );
				}
			}
			break;
		#endif

		case DEV_DEVEX_READ:

			#ifdef DEV_DEBUG_STATE
				Alert_Message_va( "RECEIVED %d bytes.", pq_msg->dh.dlen );
				strlcpy( debug_text, (char*)pq_msg->dh.dptr, sizeof( debug_text) );
			#endif

			// 3/12/2015 mpd : Initialization used the first two READ "DEV_MENU_ITEM" entries. This state begins at the 
			// third entry. Bump the index here BEFORE using it each I/O cycle.
			dev_increment_list_item_pointer( dev_state, 1 );

			#ifdef DEV_DEBUG_STATE
				Alert_Message_va( "Read List Index: %d", dev_state->read_list_index );
			#endif

			// 5/27/2015 mpd : Handle READ lists based on device type.
			list_completed = ( ( dev_process_list_receive( dev_state, pq_msg, dev_state->list_ptr ) ) == false );

			// 3/18/2015 mpd : Loop through the list until there are no more commands. 
			if( list_completed )
			{
				dev_exit_device_mode();

				// 3/18/2015 mpd : We just finished looping through the list. Did we finish too early?
				if( dev_state->read_list_index < ( dev_state->list_len - 1 ) )
				{
					// 4/16/2015 mpd : We got part way through the list. The final count will help with debugging.
					#ifdef DEV_VERBOSE_ALERTS
						Alert_Message_va( "Device %s ended early: %d", dev_state->operation_text, dev_state->read_list_index );
					#else
						Alert_Message_va( "DEV18 (%d)", dev_state->read_list_index );
					#endif

					strlcpy( dev_state->info_text, "Read ended early", sizeof( dev_state->info_text ) );                

					device_exchange_result = DEVICE_EXCHANGE_KEY_read_settings_error;
				}
				// 4/13/2015 mpd : We successfully got to the end of the list.
				else
				{
					// 6/8/2015 mpd : Check the "error_code" field before other possible issues. 
					if( !dev_state->error_code )
					{
						// 5/6/2015 mpd : WRITE operations are followed by a READ to verify the values were successfully 
						// set. The final info message after a READ or WRITE command needs to reflect what was 
						// requested, not the status of the final READ operation. Use this flag to get it right.
						if( dev_state->read_after_a_write )
						{
							// 5/7/2015 mpd : This READ operation followed a WRITE. Use the programming flag.

							dev_state->read_after_a_write = false;

							if( dev_state->programming_successful )
							{
								strlcpy( dev_state->info_text, "Write completed", sizeof( dev_state->info_text ) );                
								device_exchange_result = DEVICE_EXCHANGE_KEY_write_settings_completed_ok;
							}
							else
							{
								strlcpy( dev_state->info_text, "Write completed. Settings Invalid.", sizeof( dev_state->info_text ) );                
								device_exchange_result = device_exchange_error;
							}
						}
						else
						{
							// 5/7/2015 mpd : This is a stand-alone READ operation. It did not follow a WRITE.  Use the 
							// settings valid flag.

							if( dev_state->device_settings_are_valid )
							{
								strlcpy( dev_state->info_text, "Read completed", sizeof( dev_state->info_text ) );                
								//Alert_Message( "Device READ successful" );

								device_exchange_result = DEVICE_EXCHANGE_KEY_read_settings_completed_ok;
							}
							else
							{
								// 6/10/2015 mpd : Firmware problems are now reported as errors. No need for this.
								//if( !dev_state->acceptable_version )
								//{
								//	snprintf( dev_state->info_text, sizeof( dev_state->info_text ), "FW Version is below minimum 7.9.0.3A2", dev_state->firmware_version );                
								//}
								//else
								//{
									strlcpy( dev_state->info_text, "Read completed. Settings Invalid.", sizeof( dev_state->info_text ) );                
								//}
								device_exchange_result = device_exchange_error;
							}
						}
					}
					else // error_code
					{
						// 6/8/2015 mpd : An error occurred during processing. 
						strlcpy( dev_state->info_text, dev_state->error_text, sizeof( dev_state->info_text ) ); 
						switch( dev_state->error_severity )
						{
							case DEV_ERROR_LEVEL_INFO:
							case DEV_ERROR_LEVEL_SERIOUS:
								// 6/11/2015 mpd : FUTURE: A dialog box for non-fatal errors would be nice. At this 
								// time, the value is not worth the effort.
								device_exchange_result = device_exchange_result_ok;
								break;

							case DEV_ERROR_LEVEL_FATAL:
							default:
								device_exchange_result = device_exchange_error;
								break;
						}
					}
				}

				#if 0
				// 5/15/2015 mpd : Manipulating DTR allows READ to exit CLI without requiring a reboot.
				// 4/16/2015 mpd : After issuing the reboot command, wait for 12 seconds for the devices to restart
				// before confirming success to the GUI.
				
				// 5/11/2015 mpd : The user needs some feedback during this wait. Break up the time and add a progress 
				// indicator.
				for( i = 0; i < DEV_REBOOT_LOOP_COUNT; ++i ) 
				{ 
					snprintf( dev_state->progress_text, sizeof( dev_state->progress_text ), "Rebooting. Seconds remaining: %d", ( DEV_REBOOT_LOOP_COUNT - i ) );                
					COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_in_progress );
				
					vTaskDelay( MS_to_TICKS( 1000 ) );
				}
				#endif

				// 4/16/2015 mpd : The wait is over. Clear the progress text.
				dev_update_info( dev_state, dev_state->info_text, device_exchange_result );
			}
			else
			{
				// 4/13/2015 mpd : There is more to do. Set the COMM_MNGR timer and wait for another response.

				// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
				// when posting is safe. Anything on the timer queue is quickly processed and therefore the
				// queue is effectively always empty.

				// 5/14/2015 mpd : The GR has some commands that will not respond (e.g. baud rate change). Set a short 
				// timer for a quicker timeout.
				if( dev_state->command_will_respond )
				{
					xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(DEV_COMM_TIMER_MS), portMAX_DELAY );
				}
				else
				{
					xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(DEV_NO_RESP_TIMER_MS), portMAX_DELAY );
				}
			}
			break;

		case DEV_DEVEX_WRITE:
			// 3/12/2015 mpd : Initialization used the first two WRITE "DEV_MENU_ITEM" entries. This state begins at the 
			// third entry. Bump the index here BEFORE using it each I/O cycle.
			dev_increment_list_item_pointer( dev_state, 1 );

			// 4/22/2015 mpd : The "dev_process_list()" function will return "true" until the list is completed. 
			list_completed = ( dev_process_list_receive( dev_state, pq_msg, dev_state->list_ptr ) == false );

			// 3/18/2015 mpd : Loop through the list until there are no more commands. 
			if( list_completed )
			{
				dev_exit_device_mode();

				// 3/18/2015 mpd : We just finished looping through the list. Did we finish too early?
				if( dev_state->write_list_index < ( dev_state->list_len - 1 ) )
				{
					// 4/16/2015 mpd : We got part way through the list. The final count will help with debugging.
					#ifdef DEV_VERBOSE_ALERTS
						Alert_Message_va( "Device %s ended early: %d", dev_state->operation_text, dev_state->write_list_index );
					#else
						Alert_Message_va( "DEV19 (%d)", dev_state->write_list_index );
					#endif

					strlcpy( dev_state->info_text, "Write ended early", sizeof( dev_state->info_text ) );                

					device_exchange_result = DEVICE_EXCHANGE_KEY_write_settings_error;
				}

				// 4/13/2015 mpd : We successfully got to the end of the list.
				else
				{
					// 6/8/2015 mpd : Check the "error_code" field before other possible issues. 
					if( !dev_state->error_code )
					{
						// 6/8/2015 mpd : A READ will immediately follow this WRITE. A "success" message here would 
						// flash too quickly to read. Save the annoyance.
						strlcpy( dev_state->info_text, "", sizeof( dev_state->info_text ) );                
						device_exchange_result = DEVICE_EXCHANGE_KEY_write_settings_completed_ok; 
					}
					else // error_code
					{
						// 6/8/2015 mpd : An error occurred during processing. 
						strlcpy( dev_state->info_text, dev_state->error_text, sizeof( dev_state->info_text ) );                
						switch( dev_state->error_severity )
						{
							case DEV_ERROR_LEVEL_INFO:
							case DEV_ERROR_LEVEL_SERIOUS:
								// 6/11/2015 mpd : FUTURE: A dialog box for non-fatal errors would be nice. At this 
								// time, the value is not worth the effort.
								device_exchange_result = device_exchange_result_ok;
								break;

							case DEV_ERROR_LEVEL_FATAL:
							default:
								device_exchange_result = device_exchange_error;
								break;
						}
					}
				}

				// 6/8/2015 mpd : Inform the user of the result.
				dev_update_info( dev_state, dev_state->info_text, device_exchange_result );
			}
			else
			{
				// 4/13/2015 mpd : There is more to do. Set the COMM_MNGR timer and wait for another response.

				// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
				// when posting is safe. Anything on the timer queue is quickly processed and therefore the
				// queue is effectively always empty.

				// 5/14/2015 mpd : The GR has some commands that will not respond (e.g. baud rate change). Set a short 
				// timer for a quicker timeout.
				if( dev_state->command_will_respond )
				{
					xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(DEV_COMM_TIMER_MS), portMAX_DELAY );
				}
				else
				{
					xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(DEV_NO_RESP_TIMER_MS), portMAX_DELAY );
				}
			}
			break;

		case DEV_DEVEX_FINISH:
		default:
			// ----------

			//Alert_Message( "End of WRITE STATE MACHINE" );

			// 4/22/2015 mpd : The READ or WRITE is complete. Clean up the operation fields.
			dev_state->operation = DEV_IDLE_OPERATION;
			strlcpy( dev_state->operation_text, DEV_IDLE_TEXT_STR, sizeof( dev_state->operation_text ) );                

			Alert_Message_va( "Size of dev_state: %d", sizeof(dev_state) );
			break;
	}

}

/* ---------------------------------------------------------- */
/*  		Configuration Controller Functions				  */
/* ---------------------------------------------------------- */
	
/* ---------------------------------------------------------- */
extern void DEVICE_initialize_device_exchange( void )
{
	//Alert_Message( "DEVICE_initialize_device_exchange" );

	// 4/8/2015 mpd : Determine if the state structure has ever been initialized.
	if( !dev_verify_state_struct( dev_state ) )
	{
		// 4/8/2015 mpd : The state structure is invalid. This is normal upon first entering a device programming screen. 
		// Handle all the first-time initialization.
		dev_initialize_state_struct( dev_state );
	}

	// 4/13/2015 mpd : Handle the state structure fields that reset on every event. 
	dev_set_state_struct_for_new_device_exchange( dev_state );

	// The GR device READ process is a cycle typically triggered by RCVD_DATA when it recognizes a complete buffer. The
	// cycle starts with analyzing the current response buffer from the device, then requesting another buffer. The
	// iterations are driven by an arrays of "gr_menu_item" structures. Here at initialization, only the "hunt" half of the
	// first READ "gr_menu_item" entry is being used since there is no initial buffer to analyze. The string 
	// "next_termination_str" is the critical piece that informs RCVD_DATA what we are waiting for. The first
	// "next_command_str" is also a special case. It is set to a special value indicating not to send a command to the
	// device. We need a process cycle to read the contents of the first buffer.

	// 3/12/2015 mpd : This triggers the GR device to enter programming mode.
	dev_enter_device_mode( dev_state );

	if( dev_state->error_code == DEV_ERROR_NONE )
	{
		// 5/5/2015 mpd : Send the first of many "!" keys. The state machine will continue the effort.
		dev_get_and_process_command( dev_state, dev_state->list_ptr->next_command, dev_state->list_ptr->next_termination_str );

		#ifdef DEV_FIELD_DEBUGGING_ALERTS
			// 5/20/2015 mpd : Use tick count for better granularity.
			//EPSON_obtain_latest_time_and_date( &dev_state->first_command_time );
			dev_state->first_command_ticks = my_tick_count;
		#endif

		// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
		// when posting is safe. Anything on the timer queue is quickly processed and therefore the
		// queue is effectively always empty.

		if( (dev_state->device_type == COMM_DEVICE_EN_UDS1100) || (dev_state->device_type == COMM_DEVICE_WEN_WIBOX) )
		{
			xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(LANTRONIX_COBOX_AUTO_REPEAT_TIMER_MS), portMAX_DELAY );
		}
		else
		{
			xTimerChangePeriod( comm_mngr.timer_device_exchange, MS_to_TICKS(DEV_AUTO_REPEAT_TIMER_MS), portMAX_DELAY );
		}
	}

}

/* ---------------------------------------------------------- */
extern void DEVICE_exchange_processing( void *pq_msg )
{
	// 3/18/2014 rmd : Note how unfortunately we had to use a parameter of type void*. We could
	// not use the COMM_MNGR_EVENT_QUEUE_EVENT_STRUCT type as it wouldn't compile. Circular
	// references. void* was the easiest way out of the problem.

	//Alert_Message( "DEVICE_exchange_processing" );

	// 3/19/2014 rmd : Always stop the response timer for the device exchange process.
	xTimerStop( comm_mngr.timer_device_exchange, portMAX_DELAY );

	// 3/19/2014 rmd : During the last exchange the timer is started again and eventually fires
	// off. This is the block to prevent further unwanted processing.
	if( comm_mngr.mode == COMM_MNGR_MODE_device_exchange )
	{
		dev_state_machine( dev_state, (COMM_MNGR_TASK_QUEUE_STRUCT*)pq_msg );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

