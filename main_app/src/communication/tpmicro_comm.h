/* file = tpmicro_comm.h                     2012.04.02  rmd  */

/* ---------------------------------------------------------- */

#ifndef _INC_TPMICRO_COMM_H
#define _INC_TPMICRO_COMM_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
											 
#include	"cs_common.h"

#include	"cs3000_tpmicro_common.h"

#include	"data_types.h"

#include	"eeprom.h"

#include	"protocolmsgs.h"

#include	"comm_mngr.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/8/2012 rmd : At this point we are using a GUI variable to actually set the message
// rate. That should go away after development and use this define.
#define TPMICRO_NORMAL_MS_BETWEEN_MSGS		(1000)

// 2/10/2014 rmd : Dwelling for one second between exchanges seems way too long. And may
// aggrevate the LPC1763 loader program. But this needs to be from the response. Not from
// when something is sent to the tpmicro. This easily works at 50ms. So I'll make it 100. It
// seems the ISP will actually wait forever for the next command input.
#define TPMICRO_ISP_MODE_MS_BETWEEN_MSGS	(20)

// 8/8/2012 rmd : How many outgoing messages should be created without seeing an incoming
// message from the TP_MICRO? Both systems are running at a quasi unsolicited 1 hertz rate.
// So if we make 5 without seing any incoming certainly something is worng.
#define	TPMICRO_OUTGOING_BETWEEN_INCOMING_LIMIT		(5)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
typedef struct
{
	// ----------

	// 4/2/2014 rmd : Is the tpmicro responding to our messages. If not we set this flag
	// (false). Which should cause other events. Such as tokens and irrigation to stop. And
	// controllers to fall into forced. This flag latches. Can only be cleared upon a restart.
	// Maybe with the flag unset we restart at midnight.
	BOOL_32			up_and_running;
	
	// ----------

	// 2/10/2014 rmd : Are we in regular irrigaiton messaging mode or have we moved to isp
	// bootloader mode in order to update the tpmicro code.
	BOOL_32			in_ISP;
	
	// ----------
	
	// 8/8/2012 rmd : The outgoing tpmicro message rate timer.
	xTimerHandle	timer_message_rate;

	// ----------
	
	// 8/7/2012 rmd : Counts how many one hertz outgoing messages have been created and sent
	// since the last incoming message received from the tp micro. Since the whole tpmicro_comm
	// variable is held in SDRAM and is not held in a file system this variable, as weel as the
	// whole tpmicro_comm variable is initialized to zeros upon code startup.
	UNS_32			number_of_outgoing_since_last_incoming;

	// ----------

	UNS_32			isp_state;

	// ----------

	DATA_HANDLE		isp_tpmicro_file;

	// ----------

	UNS_32			isp_after_prepare_state;

	// ----------

	UNS_32			isp_where_to_in_flash;

	UNS_8			*isp_where_from_in_file;

	// ----------

	UNS_32			uuencode_checksum_line_count_20;

	UNS_32			uuencode_running_checksum_20;
	
	UNS_32			uuencode_bytes_left_in_file_to_send;

	UNS_32			uuencode_bytes_in_this_1K_block_sent;

	UNS_32			uuencode_first_1K_block_sent;

	// ----------

	// 2/18/2014 rmd : To cover the delicacies of a faulted start up and trying to find is the
	// TP Micro in ISP mode or not, we find ourselves in a spot where we don't really know for
	// sure if the ECHO OFF command has been issued or not. So we want to send it, possibly for
	// the second time, and get through that ISP entry state without the lack of a response
	// considered a failure. We then proceed eventually to the part ID state and that MUST pass
	// else we abandon the whole ISP attempt and try to execute the application. If it is even
	// there.
	BOOL_32		isp_sync_fault;

	// ----------

	// 3/4/2014 rmd : The holding location when for the tpmicro executing code revision
	// information.
	UNS_32		tpmicro_executing_code_date;

	UNS_32		tpmicro_executing_code_time;

	BOOL_32		tpmicro_executing_code_date_and_time_valid;

	BOOL_32		tpmicro_request_executing_code_date_and_time;

	// ----------

	UNS_32		file_system_code_date;

	UNS_32		file_system_code_time;

	// 3/25/2014 rmd : On startup a request is made to read the TPMicro file date and time from
	// the file system. Once that result arrives via queue message from the file system this
	// variable flips true. And effectively from that moment forth this variable and the
	// associated file system date and time are valid. As a new file is written they are
	// automatically updated.
	BOOL_32		file_system_code_date_and_time_valid;

	// ----------

	// 3/25/2014 rmd : This variable is key to blocking scan or token generation until after the
	// TPMicro code update process has fully completed. And a file version test has been fully
	// satisfied with no chance of going into ISP mode.
	BOOL_32		code_version_test_pending;

	// ----------

	// 8/8/2012 rmd : Effectively every message the wind speed is delivered. If the wind speed
	// is zero that is implicit as no information is delivered. Units are mph.
	UNS_32					wind_mph;
	

	
	// ----------

	// 10/15/2014 rmd : The rain switch, freeze switch, and wind pause indications are set with
	// EACH message from the tpmicro. Using only a bit in "pieces". All three of them start out
	// (false) when the main board code boots. But will however be made accurate on receipt of
	// the first regular full message from the the tpmicro. Which happens after the code update
	// phase has been worked through.
	BOOL_32					rain_switch_active;

	BOOL_32					freeze_switch_active;
	
	BOOL_32					wind_paused;
	
	// ----------

	// 9/20/2012 rmd : If (true) the TP MICRO has or is telling us the 24VAC fuse is blown.
	// Somehow this needs to be distributed to all irri machines when flips (true). Maybe in the
	// form of an alert line? Or some other form. Do we need an array of 12 of these to reflect
	// the state of all fuses in the network? I think so.
	BOOL_32					fuse_blown;
	
	// ----------

	// 2/6/2015 rmd : Temporary holding of whats installed as received from the TPMicro. The
	// irri side should not write the received content directly to the Chain Members structure
	// otherwise the foal side will not see the 'changes' when the content is sent to the foal
	// side as the chain members structure has already been changed. This is initialized to 0
	// like the rest of the tpmicro_comm structure. And that is fine. The is no tracking of
	// whats installed changes on the irri side.
	WHATS_INSTALLED_STRUCT	wi_holding;

} TPMICRO_COMM_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/14/2014 rmd : A regular SDRAM variable. Initialized to ZERO upon code startup by the C
// startup code. This variable is not battery backed or saved to a file.
extern TPMICRO_COMM_STRUCT		tpmicro_comm;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// This will occupy the first 10 bits of 'pieces' for this command.
#define		TPMICRO_DEBUG_MAX_LINES_OF_TEXT					(14)

#define		TPMICRO_DEBUG_TEXT_LINE_WIDTH					(64)

// 4/19/2012 rmd : Where the received data gets placed. The array is filled with spaces upon
// receipt of each screen from the tpmicro. And the message fills in lines with the data it
// provided. The +1 in the definition is to make sure we have room for a string ending NULL.
extern UNS_8	TPMICRO_debug_text[ TPMICRO_DEBUG_MAX_LINES_OF_TEXT ][ TPMICRO_DEBUG_TEXT_LINE_WIDTH ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void TP_MICRO_COMM_resync_wind_settings( void );

extern void TPMICRO_COMM_parse_incoming_message_from_tp_micro( DATA_HANDLE pdh );

extern void TPMICRO_COMM_kick_out_the_next_tpmicro_msg( COMM_MNGR_TASK_QUEUE_STRUCT *pcmqs );

extern void TPMICRO_COMM_create_timer_and_start_messaging( void );

extern void TPMICRO_make_a_copy_and_queue_incoming_packet( DATA_HANDLE pdh );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

