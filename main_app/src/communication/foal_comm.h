/*  file = foal_comm.h              10/27/00 2011.08.01  rmd  */

/* ---------------------------------------------------------- */

#ifndef _INC_FOAL_COMM_H
#define _INC_FOAL_COMM_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	"lpc_types.h"

#include	"cal_string.h"

#include	"configuration_controller.h"

#include	"cs3000_tpmicro_common.h"

#include	"foal_defs.h"

#include	"switch_input.h"

#include	"comm_utils.h"

#include	"poc.h"

#include	"lights.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/14/2012 rmd : This is the structure used to communicate the mlb information to the irri
// machines. A system may have more than one mainline break condition at a time. In other
// words one during irrigation and one during all other times. In which case the
// communicatons would send two of these records. However when the user clears the mlb, all
// three mlb possiblities in the system are cleared.
//
// 4/29/2015 rmd : These values CANNOT be more than 0xFF else the MLB alert will not work
// properly.
#define		MLB_TYPE_DURING_IRRIGATION  			(10)
#define		MLB_TYPE_DURING_MVOR					(20)
#define		MLB_TYPE_DURING_ALL_OTHER_TIMES			(30)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// The transfer of the chain configuration from the FOAL master to each irri
// machine uses this structure. We send this structure for each active
// controller in the chain. We start with an array of MAX_CHAIN_LENGTH of
// BOOL_32 which say for which controllers we are sending this info for.

typedef struct
{
	// 4/19/2017 rmd : When we rolled the registration from VERSION 5 to VERSION 6 we removed
	// the controller name field, 48 chars, that was here. It was not used, as the controller
	// names for each box is in the network file. And the network file names are the sync'd
	// variable, and therefore the ones to be used when referring to a box name.
	
	// ----------
	
	UNS_32						serial_number;  // 4 bytes
	
	PURCHASED_OPTIONS_STRUCT	purchased_options;  // 2 bytes + 2 bytes pad
	
	WHATS_INSTALLED_STRUCT		wi;  // 56 bytes	

	// 6/19/2013 rmd : Only for ports A & B. The other ports do not have
	// variable settings and behaviours.
	UNS_32						port_A_device_index;  // 4
	
	UNS_32						port_B_device_index;  // 4

	// 4/16/2014 rmd : Structure is currently at 120 bytes. If you add to this structure be sure
	// to update the box configuration load function used by the communications (located towards
	// the bottom of battery_backed_vars.c).
	
} BOX_CONFIGURATION_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// There are SIX commands that use the UNS_32 'pieces' to manage their functionality. Those
// are the TWO chain distribution commands, the TWO sync process commands, and the TWO irri
// process commands. Each of them has 32 bit positions available. And even that can be
// expanded by designating the last bit as a marker to include a second UNS_32 pieces_II if
// needed. The BIT defines range from 0 to 31.
//
// The name designation is as follows
// BIT meaning a bit number
// TI or TF meaning TO_IRRI or TO_FOAL
// C, S, I, or L meaning CHAIN, SYNC, IRRIGATION, or LIGHTS related
// and then the name


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// ******* For the MID_FOAL_TO_IRRI__clean_house_request command

#define		BIT_TI_C_clean_house_request					(2)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// ******* For the MID_FOAL_TO_IRRI__IRRI_DATA command

#define		BIT_TI_I_here_are_the_action_needed_records		(0)

#define		BIT_TI_I_stations_for_your_list					(1)

#define		BIT_TI_I_here_is_the_mlb_info					(2)

#define		BIT_TI_I_here_are_box_currents					(3)

#define		BIT_TI_I_here_is_the_terminal_short_ACK_or_no_current	(4)

#define		BIT_TI_I_here_is_real_time_weather_data_to_display		(5)

#define		BIT_TI_I_here_is_the_et_rain_tables				(6)

#define		BIT_TI_I_here_is_the_rain_table					(7)

#define		BIT_TI_I_here_is_the_system_info				(8)

#define		BIT_TI_I_here_is_the_poc_info					(9)

#define		BIT_TI_I_here_is_program_data					(10)

#define		BIT_TI_I_here_is_a_stop_command					(11)

#define		BIT_TI_I_send_me_latest_alert_timestamp			(12)

#define		BIT_TI_I_here_are_latest_alerts					(13)

#define		BIT_TI_I_here_is_the_clear_runaway_gage_command	(14)

#define		BIT_TI_I_here_is_the_chain_members_array		(15)

#define		BIT_TI_I_send_me_crc_list						(16)

#define		BIT_TI_I_rain_switch_active						(17)

#define		BIT_TI_I_freeze_switch_active					(18)

#define		BIT_TI_I_no_longer_used_19						(19)

#define		BIT_TI_I_two_wire_cable_excessive_current_ack_from_master		(20)

#define		BIT_TI_I_no_longer_used_21										(21)

#define		BIT_TI_I_two_wire_cable_overheated_ack_from_master				(22)

#define		BIT_TI_I_two_wire_cable_cooled_off_from_master					(23)

// 7/29/2015 mpd : Added bit for lights newly added to the ON list.
#define		BIT_TI_L_lights_for_on_list										(24)

// 7/29/2015 mpd : Added bit for lights newly added to the ON list.
#define		BIT_TI_L_lights_for_action_needed_list							(25)


// 8/11/2015 mpd : Note: There is a "BIT_TF_L_all_lights_off_request" bit for slaves to
// request all lights off. A corresponding "TI" bit for the master to inform the slaves DOES
// NOT exist. There is no need. Receipt of this request by the master will trigger OFF
// action needed on all lights in the system. This is the normal OFF mechanism from the
// master.

// 12/01/2015 skc : When comm server sends a new datetime, pass it along to the IRRI stations.
#define		BIT_TI_I_here_is_date_time						(26)

// 5/9/2016 ajv : Perform a 2-Wire Discovery on ALL controllers in the chain when a request
// to do is is received from Command Center Online.
#define		BIT_TI_I_perform_two_wire_discovery				(27)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// ******* For the MID_IRRI_TO_FOAL__IRRI_DATA_RESPONSE command

// 8/10/2012 rmd : As part of TOKEN_RESP we package up any and all information for each POC
// this box owns. Information that needs to make its way to the master. Such as raw flow
// readings. And MV and PUMP current measurements.
#define		BIT_TF_I_here_is_an_update_for_my_pocs				(0)

// 2012.02.15 rmd : When the STOP key is pressed we send an indication to the master. The
// takes action in his list immediately upon receipt. And on the the next TOKEN generation
// informs all the IRRI machines with a stop record.
#define		BIT_TF_I_stop_key_pressed							(1)


#define		BIT_TF_I_system_gids_to_clear_mlb_for				(2)

#define		BIT_TF_I_here_is_my_box_current_ma					(3)

#define		BIT_TF_I_reporting_a_terminal_short_or_no_current	(4)

#define		BIT_TF_I_two_wire_cable_current_measurement			(5)

#define		BIT_TF_I_test_request								(6)

#define		BIT_TF_I_reporting_et_gage_pulse_count				(7)

#define		BIT_TF_I_reporting_rain_bucket_pulse_count			(8)

#define		BIT_TF_I_reporting_runaway_gage						(9)

#define		BIT_TF_I_clear_runaway_gage_key_pressed				(10)

#define		BIT_TF_I_here_is_my_box_configuration				(11)

#define		BIT_TF_I_here_is_a_single_chain_sync_crc			(12)

#define		BIT_TF_I_manual_water_request						(13)

#define		BIT_TF_I_reporting_rain_switch_active				(14)

#define		BIT_TF_I_reporting_freeze_switch_active				(15)

#define		BIT_TF_I_reporting_two_wire_cable_excessive_current	(16)

#define		BIT_TF_I_reporting_two_wire_cable_overheated		(17)

#define		BIT_TF_I_reporting_two_wire_cable_cooled_off		(18)

#define		BIT_TF_I_reporting_decoder_faults					(19)

#define		BIT_TF_I_mvor_request								(20)

#define		BIT_TF_I_walk_thru_start_request					(21)

#define		BIT_TF_I_unused_22									(22)

#define		BIT_TF_I_reporting_wind								(23)

#define		BIT_TF_I_here_is_program_data						(24)

#define		BIT_TF_I_here_is_latest_alert_timestamp				(25)

#define		BIT_TF_I_am_request_you_start_a_scan				(26)

// 8/11/2015 mpd : Added bit for lights content. This bit indicates a request from a slave to the master to turn on a 
// light. The reason can be MANUAL or TEST. Only 1 light ON request can be included in this token.
#define		BIT_TF_L_light_on_request							(27)

// 8/10/2015 mpd : Added bit for lights content. This bit indicates a request from a slave to the master to turn off a 
// light. The reason can be MANUAL or TEST. Only 1 light OFF request can be included in this token.
#define		BIT_TF_L_light_off_request							(28)

#define		BIT_TF_I_here_is_a_moisture_reading_string			(29)


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/10/2012 rmd : When messaging between the slaves and master (and vice-a-versa) we have a
// variable content POC record that is sent. A byte within the message defines the content.
// These are the defines used to set and test the bits indicating content.
#define		POC_CONTENT_BIT_FLOW_READING_PRESENT			(0)

#define		POC_CONTENT_BIT_MILLI_AMP_MEASUREMENT_PRESENT	(1)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// The transfer of stations from the FOAL master to the irri machine just after they are
// added to the main irrigation list. This may be the closest thing we have to a 'whole'
// station transfer. Remember at certain start times there could be alot of these.
// Theoretically 768 of them. Which would make the token HUGE. Too big. We would have to
// manage that. And spread the load out over several tokens.
typedef struct
{
	// 8/27/2012 rmd : The system ID is used when there is a short on a PUMP or MV output. All
	// stations that belong to the system the MV/PUMP POC belongs to are removed from irrigation
	// when this occurs. Knowing the system GID in advance is very helpful. There may be other
	// uses.
	UNS_32				system_gid;
	
	// The total amount left to irrigate. As cycle take place this amount is decreased by the
	// cycle amount. Must be able to hold the longest amount we would ever want to irrigate for.
	// Hence it is a 32-bit long.
	UNS_32				requested_irrigation_seconds_u32;

	// 6/25/2012 rmd : Before a power fail stations may have been soaking. If they were this
	// entry will be non-zero. When we rebuild the list on the irri side if we were soaking
	// before the power fail this will reflect such after the power failure.
	UNS_32				soak_seconds_remaining;

	// 6/26/2012 rmd : The programmed (and resultant after tail-ends does its thing) cycle time.
	UNS_32				cycle_seconds;

	UNS_32				box_index_0;
	
	UNS_8				station_number_0_u8;
	
	UNS_8				reason_in_list_u8;

} STATION_FROM_MAIN_LIST_DISTRIBUTION_STRUCT;

/* ---------------------------------------------------------- */

typedef struct
{
	// 8/5/2016 rmd : To identify the POC we use these three items. The box index and the type
	// go a long way. But for the case of POC DECODERS we need the serial number. You CANNOT use
	// the POC GID as an identifier within flowsense traffic. That is forbidden because for a
	// given POC the GID's are not guaranteed to match among the controllers in a chain.

	UNS_32	box_index;
	
	UNS_32	poc_type__file_type;
	
	UNS_32	decoder_serial_number;
	
	// ----------
	
	// 6/4/2014 rmd : This is a structure to encapsulate the content for each poc that is
	// distributed from the slave for each and every known poc in the network. We always deliver
	// all 3 levels of data regardless of poc type.

	float	latest_5_second_average_gpm_foal[ POC_BYPASS_LEVELS_MAX ];
	
	UNS_32	mv_current_for_distribution_ma[ POC_BYPASS_LEVELS_MAX ];

	UNS_32	pump_current_for_distribution_ma[ POC_BYPASS_LEVELS_MAX ];

} POC_DISTRIBUTION_FROM_MASTER;


typedef struct
{
	// 6/4/2014 rmd : This is a structure to encapsulate the content for each poc that is sent
	// from the slave to the master for each of the slaves tpmicro level pocs.

	// 6/4/2014 rmd : This and the box index are all that is needed to tie the data to a
	// poc_preserves ws record. The box index is known as that is who the message is from.
	UNS_32	decoder_serial_number;

	// 6/4/2014 rmd : At this level we are sending the data for a single tpmicro poc. Can only
	// be a single poc. The tpmicro does not know about the bypass configuration.
	UNS_32	fm_accumulated_pulses;
	
	UNS_32	fm_accumulated_ms;
	
	UNS_32	fm_latest_5_second_count;
	
	UNS_32	fm_delta_between_last_two_fm_pulses_4khz;

	UNS_32  fm_seconds_since_last_pulse;
	
	// 6/4/2014 rmd : Additionally we will always include the last measured mv and pump ma.
	// Which in the case of decoder based both will be 0 because there is no mv or pump current
	// yet.
	UNS_32	last_measured_mv_ma;

	UNS_32	last_measured_pump_ma;
	
} POC_DISTRIBUTION_FROM_SLAVE_TO_MASTER;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// ----------------------------------

// ----------------------------------

// 4/26/2012 rmd : CS3000 ACTION_NEEDED REASONS.

// When the token response (from an irri machine of course) indicates a stop request has
// been made we immediately turn off the affected valves and remove them from the irrigation
// list. This reason is used as the reason for the turn off. Though not actually used in any
// special way, we wanted to be able to use the exisiting turn off function. This reason is
// not intended to end up as the reason a station is on the action_needed list. Gotta start
// somewhere. So I picked (UNS_8_MAX - 10). Do not use values greater than 255 cause we
// transmit the action_needed records with the reason as a BYTE! (for transmission
// efficiency).

// ------------------------

#define		ACTION_REASON_TURN_ON_and_clear_flow_status								(1)

#define		ACTION_REASON_TURN_ON_and_leave_flow_status								(2)

// ------------------------

#define		ACTION_REASON_TURN_OFF_cause_cycle_has_completed						(5)

// 6/27/2012 rmd : Instead of just 'flow error' we need to be specific so over on the irri
// side when we receive the action needed record we can set the station preserves flow error
// indication.
#define		ACTION_REASON_TURN_OFF_hi_flow_fault									(6)

#define		ACTION_REASON_TURN_OFF_lo_flow_fault									(7)

// 6/27/2012 rmd : When turned OFF for this reason we do not set the station preserves flow
// error indication to anything.
#define		ACTION_REASON_TURN_OFF_first_flow_fault									(8)

// ----------

#define		ACTION_REASON_TURN_OFF_pause_for_a_higher_reason_in_the_list			(12)

// 11/13/2014 rmd : This is to be used when the RRE commands a valve to pause. The user
// wants to temporarily halt it. Not yet in use in the CS3000.
#define		ACTION_REASON_TURN_OFF_pause_rre_commanded								(13)

#define		ACTION_REASON_TURN_OFF_pause_wind										(14)

// ----------

#define		ACTION_REASON_TURN_OFF_have_acquired_expected							(18)

// ------------------------

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_irrigation_has_completed				(20)

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_higher_reasons_in_the_list			(21)

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_hit_electrical_limit					(22)

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_already_one_ON_to_acquire_expected	(23)

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_irrigation_time_unexpectedly_zero		(24)

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_have_acquired_expected				(25)

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_hi_flow_fault							(26)

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_lo_flow_fault							(27)

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_hit_stop_time							(28)

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_rain_crossed_minimum_or_poll			(29)

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_rain_switch_active					(30)

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_freeze_switch_active					(31)

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_mobile_command						(32)

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_moisture_reading_hit_set_point		(33)


// ----------

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_stop_key								(100)

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_mlb									(101)

// ----------

// 11/13/2014 rmd : The are only a few true INTERNAL reason remaining. Meaning there are no
// action_needed records for this reason transferred to the irri machines.
#define		ACTION_REASON_INTERNAL_TURN_OFF_reboot_or_chain_went_down				(103)

#define		ACTION_REASON_INTERNAL_TURN_OFF_AND_REMOVE_list_dup_removal				(102)

// ----------

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_conventional_station_short			(104)
#define		ACTION_REASON_TURN_OFF_AND_REMOVE_conventional_mv_short					(105)
#define		ACTION_REASON_TURN_OFF_AND_REMOVE_conventional_pump_short				(106)

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_station_decoder_voltage_too_low		(107)
#define		ACTION_REASON_TURN_OFF_AND_REMOVE_station_decoder_solenoid_short		(108)
#define		ACTION_REASON_TURN_OFF_AND_REMOVE_station_decoder_inoperative			(109)

#define		ACTION_REASON_TURN_OFF_AND_REMOVE_poc_decoder_voltage_too_low			(110)
#define		ACTION_REASON_TURN_OFF_AND_REMOVE_poc_decoder_solenoid_short			(111)
#define		ACTION_REASON_TURN_OFF_AND_REMOVE_poc_decoder_inoperative				(112)


#define		ACTION_REASON_TURN_OFF_AND_REMOVE_controller_turned_off					(113)


#define		ACTION_REASON_TURN_OFF_AND_REMOVE_two_wire_cable_fault					(114)

// 8/3/2015 mpd : Added for Lights. Since lights actions are handled independently from stations, there is no reason to 
// use any of the 255 station action values. Start a new enum list.
#define 	ACTION_NEEDED_NONE														( 0 )
#define 	ACTION_NEEDED_TURN_ON_programmed_light									( 1 )
#define 	ACTION_NEEDED_TURN_ON_manual_light										( 2 )
#define 	ACTION_NEEDED_TURN_ON_mobile_light										( 3 )
#define 	ACTION_NEEDED_TURN_ON_test_light										( 4 )
#define		ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_output_short					( 5 )
#define		ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_hit_stop_time					( 6 )
#define		ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_manual_off						( 7 )
#define		ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_mobile_off						( 8 )
#define		ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_test_off						( 9 )
#define     ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_controller_turned_off			( 10 )
#define     ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_time_unexpectedly_zero			( 11 )

// ------------------------	

// 4/24/2012 rmd : This is what gets placed in the token. A structure keeping in mind size
// to keep transmission overhead down.
typedef struct
{
	// 6/27/2012 rmd : range 0 .. 11.
	UNS_8	box_index_0;
	
	UNS_8	station_number_0_u8;  // 0 based - max 128

	UNS_8	w_irrigation_reason_in_list_u8;

	UNS_8	sxru_xfer_reason_u8;

	// 11/13/2014 rmd : When turn ON stations this field represents for how long to run. When
	// turn OFF represents number of soak seconds. Taking this approach to manage size within
	// token of many of these records.
	INT_32	remaining_ON_or_soak_seconds;

	UNS_32	updated_requested_irrigation_seconds_ul;

} ACTION_NEEDED_XFER_RECORD;

/* ---------------------------------------------------------- */

// 4/16/2012 rmd : When a stop key indication comes in via token response we fill out this
// record and act upon it when the next token is sent.
typedef struct
{
	// ----------
	
	// 11/6/2014 rmd : For which reason and system are we stopping?
	UNS_32	stop_for_this_reason;
	
	UNS_32	stop_in_this_system_gid;
	
	// ----------
	
	// 11/6/2014 rmd : And some variables that allow us to stop at a broader level than just for
	// one reason or for one system.

	BOOL_32	stop_for_the_highest_reason_in_all_systems;
	
	BOOL_32	stop_in_all_systems;
	
	// 11/7/2014 rmd : Maybe for all reasons in a single system. Need both 'stop all systems'
	// and 'stop for all reasons' to kill ALL irrigation.
	BOOL_32	stop_for_all_reasons;

	// ----------
	
	// 11/7/2014 rmd : This variable is used when the record is returned to the irri machines.
	// So the display can be updated to reflect what was stopped. NOTE - this variable only
	// reflects the last reason for the last system that had valves stopped. And in the face of
	// multiple systems is not exatcly accurate. If that's needed we'll have to expand this
	// structures functionality.
	UNS_32	stations_removed_for_this_reason;

} STOP_KEY_RECORD_s;

/* ---------------------------------------------------------- */

// This is what gets placed in the token. Keep it small to keep the transmission overhead down.
typedef struct
{
	UNS_32	stop_datetime_t;

	UNS_16	stop_datetime_d;

	// To save space in the token, box and index values have been combined into this single UNS_8.
	UNS_8	light_index_0_47;
	
	UNS_8	action_needed;

	// This structure is 8 bytes long on 8/19/2015.

} LIGHTS_LIST_XFER_RECORD;

// ----------

typedef struct
{
	// This is small enough. Save some processing time with UNS_32.
	UNS_32	light_index_0_47;
	
	UNS_32	action_needed;

	// This structure is 8 bytes long on 8/19/2015.

} LIGHTS_ACTION_XFER_RECORD;

// ----------

// 8/12/2015 mpd : The frequency of these records in the token will be low. Use UNS_32 variables.
typedef struct
{
	// Only one light at a time can be sent to the master. This is used on the irri side to enable the key for the user. 
	// Once pressed this is ( true ) until the next token_resp is sent. The user should see a pop up indicating to 
	// try-again after their last light has turned ON. 	This flag is also used to indicate a token response is required.
	UNS_32	in_use;
	
	UNS_32	light_index_0_47;
	
	// 8/31/2015 mpd : There is only 1 reason a light request will be sent from the IRRI side: manual ON. This field is 
	// unnecessary.
	//UNS_32	reason_in_list;

	UNS_32	stop_datetime_d;
	
	// This times is in seconds.
	UNS_32	stop_datetime_t;

	// This structure is 20 bytes long on 8/19/2015.
	// This structure is 16 bytes long on 8/31/2015.

} LIGHTS_ON_XFER_RECORD;
 
// ----------

typedef struct
{
	// Only one light at a time can be sent to the master. This is used on the irri side to enable the key for the user. 
	// Once pressed this is (true) until the next token_resp is sent. The user should see a pop up indicating to 
	// try-again after their last light has turned ON. This flag is also used to indicate a token response is required.
	BOOL_32	in_use;

	UNS_32	light_index_0_47;

	// 8/31/2015 mpd : There is only 1 reason a light OFF request will be sent from the IRRI side: manual OFF. This 
	// field is unnecessary.
	//UNS_32	lights_off_for_this_reason;

	// This structure is 12 bytes long on 8/19/2015.
	// This structure is 8 bytes long on 8/31/2015.

} LIGHTS_OFF_XFER_RECORD;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


extern void FOAL_COMM_build_token__clean_house_request( void );

extern void FOAL_COMM_build_token__irrigation_token( void );



extern void FOAL_COMM_process_incoming__clean_house_request__response( COMMUNICATION_MESSAGE_LIST_ITEM *cmli );

extern void FOAL_COMM_process_incoming__irrigation_token__response( COMMUNICATION_MESSAGE_LIST_ITEM *cmli );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

