/*  file = comm_utils.h                       2011.07.25 rmd  */

/* ---------------------------------------------------------- */

#ifndef _INC_COMM_UTILS_H
#define _INC_COMM_UTILS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"packet_definitions.h"

#include	"serial.h"




/* ---------------------------------- */
/* ---------------------------------- */

// 9/21/2012 raf :  These defines are used when comparing our serial number to the serial number in a message coming to us.  The function that
// compares them requires one of these passed in depending on whether its big endian or little endian format.
#define THE_PASSED_IN_DATA_IS_BIG_ENDIAN		0	//Used for Transport
#define THE_PASSED_IN_DATA_IS_LITTLE_ENDIAN		1	//Used for Application

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// When messages come in for the various machines they are put on a list It is a list of "communication message list items"
// these are the universal (amongst the controllers various machines) means that we send a message to a machine. This list
// element contains pointers to the actual message and enough information to build a response.

typedef struct
{
	MIST_DLINK_TYPE					dl;					// list support

	DATA_HANDLE						dh;					// to the actual message

	ADDR_TYPE						from_to;			// to / from address

	UPORTS							in_port;			// port message came in from

	ROUTING_CLASS_DETAILS_STRUCT	message_class;		// and the message class as the transport determined it

} COMMUNICATION_MESSAGE_LIST_ITEM;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


extern BOOL_32 this_special_scan_addr_is_for_us( const UNS_8 *to_addr_ptr, const UNS_32 pendianness );

extern BOOL_32 this_communication_address_is_our_serial_number( const UNS_8 *to_addr_ptr, const UNS_32 pendianness );





BOOL_32 CommAddressesAreEqual( void *pa1, void *pa2);


extern void COMM_MNGR_send_outgoing_3000_message_and_take_on_memory_release_responsiblity( UPORTS p_port, DATA_HANDLE p_dh, ADDR_TYPE p_from_to, UNS_32 p_message_class );


extern void SendCommandResponseAndFree( DATA_HANDLE pdh, COMMUNICATION_MESSAGE_LIST_ITEM *pcmli, UNS_32 p_message_class );

extern void COMM_UTIL_build_and_send_a_simple_flowsense_message( COMM_COMMANDS pcommand, COMMUNICATION_MESSAGE_LIST_ITEM *pcmli, UNS_32 p_message_class );


void Roll_FIFO( void *array_ptr, int element_size, int how_many );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

