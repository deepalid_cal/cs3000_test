/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_PDATA_CHANGES_H
#define _INC_PDATA_CHANGES_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 12/1/2014 ajv : Required since this file is shared between the CS3000 and the comm server
#include	"cs3000_comm_server_common.h"

#include	"general_picked_support.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 32 bits representing whether this change has been distributed or not. The
// first 12 bits (0-11) are used for controllers A-L, respectively. The next 8
// bits are used to identify up to eight unique central computers. The final 8
// can be used to identify up to eight unique RRe-handheld radio remotes.
#define		CHANGE_BITS_32_BIT				UNS_32
#define		CHANGE_BITS_64_BIT				UNS_64

#define		CHANGE_BITS_32_BIT_ALL_SET		UNS_32_MAX
#define		CHANGE_BITS_32_BIT_ALL_CLEAR	UNS_32_MIN

#define		CHANGE_BITS_64_BIT_ALL_SET		UNS_64_MAX
#define		CHANGE_BITS_64_BIT_ALL_CLEAR	UNS_64_MIN

// 03.25.2012 ajv : Based upon the number of groups we currently have and the
// settings we know we need that aren't done yet (e.g., weather options, etc.),
// it's safe to say that we need at least 32 bits. To ensure we have room to
// expand in the future, we'll start by allocating a full 64 bits.
#define		PDATA_CHANGES_BIT_FIELD_TYPE	CHANGE_BITS_64_BIT

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/4/2016 rmd : The full pdata sync exchange between controllers has the problem of
// changes collected via the token resps of building up on the irri side of the master and
// only being distributed in the token addressed to the B controller. The other tokens to C,
// D, etc do not carry any pdata changes. The next outbound token to carry pdata changes
// depends on a token resp from the A controller. So we want that token to be able to carry
// alot of data.
//
// 8/4/2016 rmd : Addtionally we have tried the pdata content allowed in tokens to be
// limited to 1024 bytes. Well this proved painfully slow on large chains. It took maybe one
// minute to sync fully. So I've bumped that to 2048 bytes. Remember the transport is still
// at 500 byte packets.

#define		MEMORY_TO_ALLOCATE_FOR_SENDING_PDATA_BETWEEN_CONTROLLERS		(2048)

#define		MEMORY_TO_ALLOCATE_FOR_TOKEN_RESP_TO_OURSELF					(MAX_CHAIN_LENGTH * MEMORY_TO_ALLOCATE_FOR_SENDING_PDATA_BETWEEN_CONTROLLERS)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifdef _MSC_VER

	#define		DATABASE_TABLE_NAME_CONTROLLERS				"CS3000Controllers"
	#define		DATABASE_TABLE_NAME_MANUAL_PROGRAMS			"CS3000ManualPrograms"
	#define		DATABASE_TABLE_NAME_NETWORKS				"CS3000Networks"
	#define		DATABASE_TABLE_NAME_POCS					"CS3000POC"
	#define		DATABASE_TABLE_NAME_MOISTURE_SENSORS		"CS3000MoistureSensors"
	#define		DATABASE_TABLE_NAME_STATION_GROUPS			"CS3000StationGroups"
	#define		DATABASE_TABLE_NAME_STATIONS				"CS3000Stations"
	#define		DATABASE_TABLE_NAME_SYSTEMS					"CS3000Systems"
	#define		DATABASE_TABLE_NAME_WEATHER					"CS3000Weather"
	#define		DATABASE_TABLE_NAME_LIGHTS					"CS3000Lights"
	#define		DATABASE_TABLE_NAME_BUDGETS					"CS3000Budgets"
	#define		DATABASE_TABLE_NAME_WALK_THRU				"CS3000WalkThru"

	#define		DATABASE_TABLE_NAME_SYSTEM_REPORT_DATA		"CS3000SystemRecord"
	#define		DATABASE_TABLE_NAME_POC_REPORT_DATA			"CS3000POCRecord"
	#define		DATABASE_TABLE_NAME_STATION_REPORT_DATA		"CS3000StationDataRecord"
	#define		DATABASE_TABLE_NAME_STATION_HISTORY			"CS3000StationHistory"
	#define		DATABASE_TABLE_NAME_FLOW_RECORDING			"CS3000FlowRecordingTable"
	#define		DATABASE_TABLE_NAME_LIGHTS_REPORT_DATA		"CS3000LightsRecord"
	#define		DATABASE_TABLE_NAME_ET_RAIN_TABLE			"CS3000ET_RainRecord"
	#define		DATABASE_TABLE_NAME_BUDGET_REPORT_DATA		"CS3000BudgetRecord"

	#define		DATABASE_TABLE_NAME_MOISTURE_RECORDER		"CS3000MoistureRecorder"

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

DLLEXPORT UNS_32 PDATA_extract_and_store_changes( UNS_8 *pucp,
												  const UNS_32 preason_for_change,
												  const BOOL_32 pset_change_bits,
												  const UNS_32 pchanges_received_from
												  #ifdef _MSC_VER
												  , char *pSQL_statements,
												  char *pSQL_search_condition_str,
												  char *pSQL_update_str,
												  char *pSQL_insert_fields_str,
												  char *pSQL_insert_values_str,
												  char *pSQL_single_merge_statement_buf,
												  const UNS_32 pnetwork_ID
												  #endif
												);

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#ifdef _MSC_VER

DLLEXPORT INT_32 PDATA_get_change_bit_number( char *ptable_name, char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order );

#endif

// ----------

extern UNS_32 PDATA_allocate_memory_for_and_initialize_memory_for_sending_data( UNS_8 **pucp,
																				   PDATA_CHANGES_BIT_FIELD_TYPE *pbitfield_of_files_that_have_changed,
																				   UNS_8 *pbitfield_size,
																				   const UNS_32 psizeof_bitfield_size,
																				   UNS_8 **plocation_of_bitfield,
																				   const UNS_32 pmemory_to_allocate );

extern UNS_32 PDATA_allocate_space_for_num_changed_groups_in_pucp( UNS_8 **pucp,
																	  UNS_8 **llocation_of_num_changed_groups,
																	  const UNS_32 pmem_used_so_far,
																	  BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
																	  const UNS_32 pallocated_memory );

extern UNS_32 PDATA_copy_var_into_pucp( UNS_8 **pucp,
										   const void *const pvar,
										   const UNS_32 psize_of_var );

DLLEXPORT UNS_32 PDATA_copy_bitfield_info_into_pucp( UNS_8 **pucp,
													 const UNS_32 psize_of_bitfield,
													 const UNS_32 psize_of_size_of_bitfield,
													 UNS_8 **plocation_of_bitfield );

extern UNS_32 PDATA_copy_var_into_pucp_and_set_32_bit_change_bits( UNS_8 **pucp,
																	  CHANGE_BITS_32_BIT *pbitfield_of_changes_to_send,
																	  const UNS_32 pbit_to_set,
																	  CHANGE_BITS_32_BIT *pchange_bits_for_controller_ptr,
																	  CHANGE_BITS_32_BIT *ppending_change_bits_to_send_to_comm_server,
																	  void *pvar,
																	  const UNS_32 psize_of_var,
																	  const UNS_32 pmem_used_so_far,
																	  BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
																	  const UNS_32 pallocated_memory,
																	  const UNS_32 preason_data_is_being_built );

extern UNS_32 PDATA_copy_var_into_pucp_and_set_64_bit_change_bits( UNS_8 **pucp,
																	  CHANGE_BITS_64_BIT *pbitfield_of_changes_to_send,
																	  const UNS_32 pbit_to_set,
																	  CHANGE_BITS_64_BIT *pchange_bits_for_controller_ptr,
																	  CHANGE_BITS_64_BIT *ppending_change_bits_to_send_to_comm_server,
																	  void *pvar,
																	  const UNS_32 psize_of_var,
																	  const UNS_32 pmem_used_so_far,
																	  BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
																	  const UNS_32 pallocated_memory,
																	  const UNS_32 preason_data_is_being_built );

// ----------

#ifndef _MSC_VER

	#define PDATA_BUILD_RESULTS_no_memory			(0x0100)
	
	#define PDATA_BUILD_RESULTS_no_data				(0x0200)
	
	#define PDATA_BUILD_RESULTS_built_a_message		(0x0300)
	
	extern UNS_32 PDATA_build_data_to_send( DATA_HANDLE *pmsg_handle, const UNS_32 preason_data_is_being_built, const BOOL_32 this_is_to_ourself );

	extern void PDATA_update_pending_change_bits( const BOOL_32 pcomm_error_occurred );
	
#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

