/* file = commmngr.h                         2011.07.21  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_COMM_MNGR_H
#define _INC_COMM_MNGR_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
											 
#include	"cs_common.h"

#include	"packet_definitions.h"

#include	"foal_defs.h"

#include	"comm_utils.h"

#include	"serial.h"

// 9/4/2013 ajv : Necessary to give us access to the A_CONTROLLERS_CHAIN_INFO_STRUCT
// structure. Note that this structure is used in both FOAL and IRRI side
// strctures and may be better suited by being stored here, in comm_mngr.h.
#include	"foal_comm.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// definitions and externs

// This is really 1 more than the number of allowable failures i.e. a 4 means we allow 3 failures and we're still
// working him - the 4th failure means he is dead

#define COMM_MNGR_ALLOWABLE_FAILURES	(4)



typedef struct
{
	UNS_32	scan_msgs_generated;
	UNS_32	scan_msgs_rcvd;
	UNS_32	scan_msg_responses_generated;
	UNS_32	scan_msg_responses_rcvd;


	UNS_32	tokens_generated;
	UNS_32	tokens_rcvd;
	UNS_32	token_responses_generated;
	UNS_32	token_responses_rcvd;


	UNS_32	rescans;	// count of the restarts of the scan for addresses
	
} COMM_STATS;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	COMM_MNGR_EVENT_last_OM_failed									(0x0100)

#define	COMM_MNGR_EVENT_token_rate_timer_expired						(0x0200)

#define	COMM_MNGR_EVENT_attempt_to_kick_out_the_next_token				(0x0201)

#define	COMM_MNGR_EVENT_there_is_a_new_incoming_message_or_packet		(0x0300)

#define	COMM_MNGR_EVENT_message_resp_timer_expired						(0x0400)

#define	COMM_MNGR_EVENT_request_to_start_a_scan							(0x0600)

#define	COMM_MNGR_EVENT_tpmicro_msg_rate_timer_expired					(0x0700)

#define	COMM_MNGR_EVENT_tpmicro_here_is_an_incoming_packet_based_msg	(0x0800)

#define	COMM_MNGR_EVENT_isp_expected_response_arrived					(0x0900)


#define	COMM_MNGR_EVENT_tpmicro_binary_file_read_for_isp_complete		(0x0B00)


#define	COMM_MNGR_EVENT_main_code_image_version_stamp					(0x0E00)

#define	COMM_MNGR_EVENT_tpmicro_code_image_version_stamp				(0x0F00)


#define	COMM_MNGR_EVENT_request_read_device_settings					(0x1100)

// 4/29/2015 mpd : Lantronix devices have 2 modes: SETUP and MONITOR. The GUI controls which mode will be used for the 
// read operation based on the COMM_MNGR_EVENT used when the device exchange is initiated.
// SETUP MODE uses:   "COMM_MNGR_EVENT_request_read_device_settings".
// MONITOR MODE uses: "COMM_MNGR_EVENT_request_read_monitor_device_settings".
#define	COMM_MNGR_EVENT_request_read_monitor_device_settings			(0x1110)

#define	COMM_MNGR_EVENT_request_write_device_settings					(0x1200)

#define	COMM_MNGR_EVENT_device_exchange_response_arrived				(0x1300)

#define	COMM_MNGR_EVENT_device_exchange_response_timer_expired			(0x1400)

#define	COMM_MNGR_EVENT_commence										(0x1500)

#define	COMM_MNGR_EVENT_tell_master_to_scan_and_report_we_are_NEW		(0x1600)



typedef struct
{
	// To identify the event.
	UNS_32			event;

	// Used when the OM fails at the transport level. The OM itself sends sends a queue message to the comm_mngr about such.
	ADDR_TYPE		who_the_message_was_to;
	
	// Used when the OM fails at the transport level. The OM itself sends sends a queue message to the comm_mngr about such.
	UNS_32			message_class;
	
	// 4/4/2012 rmd : For incoming TP_MICRO packets. Also used to return strings during device
	// exchange sting hunt mode.
	DATA_HANDLE		dh;
	
	// 3/3/2014 rmd : Well yes i'm growing the queue message size. But the tradeoff is clarity.
	// We are returning the code image time and date.
	UNS_32			code_date;
	
	UNS_32			code_time;
	
	// 3/13/2014 rmd : Used during device programming and settings reading.
	UNS_32			port;

	UNS_32			reason_for_scan;
	
} COMM_MNGR_TASK_QUEUE_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// #ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/1/2014 rmd : As part of the boot process as the comm_mngr makes its way towards a scan
// we are intially in the tpmicro ISP mode. This mode persists until either the tpmicro is
// determined to be running and its code up to date. Or the tpmicro code is updated and a
// further verification proves the code to be up to date. ALWAYS leaves this mode to the
// scanning mode. ALWAYS!
#define COMM_MNGR_MODE_tpmicro_isp					(0)

// 6/20/2017 rmd : Hunting for the controllers on the chain.
#define COMM_MNGR_MODE_scanning						(1)

// 4/1/2014 rmd : The typical normal mode. Making tokens, if not doing a code distribution.
// Forced is a flag within this mode.
#define COMM_MNGR_MODE_operational					(2)

// 3/13/2014 rmd : Either programming to or reading from the communication device on either
// port A or port B. Once a request to enter this mode has been made (from the key
// processing task) additional requests will be ignored till the first request has
// completed.
#define	COMM_MNGR_MODE_device_exchange				(5)

// ----------

// 4/14/2014 rmd : These are STATES of the 'making_tokens' MODE.

// 4/16/2014 rmd : Tell the NEW controllers to wipe their settings. This is preparation to
// sending their what's installed, stations, & pocs to the master. And receiving all the
// settings from the master.
#define	COMM_MNGR_STATE_cleaning_house			(1111)

#define	COMM_MNGR_STATE_irrigation				(2222)

#define	COMM_MNGR_STATE_crc_cleaning_house			(3333)

// ----------

// 5/2/2014 rmd : Used to manage what the user is allowed to do following a scan (reboot or
// powerup or manually requested scan). While pdata still being distributed dont allow him
// to do much.
//
// 8/4/2016 rmd : Well that concept hasn't been fully implemented yet. Surely it is a good
// idea to block the user from editing screens while pdata is syncing. The reason being, I
// think editing while syncing opens the door to a variety of unexpected behaviors.
// Including MUTEX collision and deadlock issues. As of this writing, this count at least is
// used to prevent parsing certain parts of the token. Which is sort of along the same
// lines.
#define REQUIRED_NUMBER_OF_CLEAN_TOKENS_SINCE_SCAN			(4)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	COMM_MNGR_reason_to_scan_startup					(0)
#define	COMM_MNGR_reason_to_scan_keypad						(1)
#define	COMM_MNGR_reason_to_scan_rescan_timer				(2)
#define	COMM_MNGR_reason_to_scan_requested_by_slave			(3)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// Valid range is from 0 to (MAX_CHAIN_LENGTH-1).
	UNS_16				index;

	// Only used while building tokens.
	COMM_COMMANDS		command_to_use;
	
	DATA_HANDLE			message_handle;
	
	ADDR_TYPE			from_to;

} RECENT_CONTACT_STRUCT;

// ------------

typedef struct
{
	// ------------

	// 10/1/2012 rmd : NOTE - These two variables are initialized by default to 0. By the
	// compiler startup code. This is the recognized initialization for the entire comm_mngr
	// structure. There is no traditional init_comm_mngr function.

	// ------------

	// 10/1/2012 rmd : As changes are made and make their way to the master they
	// need to be distributed to the slaves.  It has been decided that this does
	// not need to be battery is a power failure before changes have been fully
	// distributed they will either be lost or the sync completed by a file
	// verification mechanism. Valid at the MASTER controller only.
	BOOL_32		distribute_changes_to_slave;
	
	// 10/1/2012 rmd : And this is an irri side variable indicating some sort of
	// an edit has been made. And requires transfer to the master controller. It
	// has been decided that this does not need to be battery backed. If there
	// is a power failure before changes have been fully distributed they will
	// either be lost or the sync completed by a file verification mechanism.
	// Valid at all controllers (is an IRRI side variable).
	BOOL_32		send_changes_to_master;

} CHANGE_TRACKING_FOR_COMMUNICATIONS_STRUCT;

// ------------

// 12/03/2015 skc : Structure for sending date time data in a token
typedef struct
{
	DATE_TIME dt;
	UNS_32 reason;
} DATE_TIME_TOKEN_STRUCT;

// ------------

typedef struct
{
	// We are either performing a scan or POTENTIALLY making tokens. Or neither - waiting for
	// devices to be initialized. Or waiting for TPMicro to complete it's ISP activity. Or
	// waiting for the TPMicro to report it's code version.
	UNS_32					mode;

	// 4/4/2014 rmd : The state while making tokens. Used to guide the comm_mngr through the
	// sequence between the conclusion of the scan till we are making true irrigation tokens.
	UNS_32					state;

	// ----------

	// 4/30/2014 rmd : This is the replacement flag for what used to be FORCED. Now when a slave
	// goes a certain amount of time without a token he will declare chain down. When the master
	// sees too many errors for those in the chain he will declare chain_down. Both cases result
	// in complete cancellation of any ongoing irrigation. And ignoring any further starts are
	// skipped.
	UNS_32					chain_is_down;

	// 4/1/2014 rmd : A flag for the slaves to manage a graceful transition into forced. To
	// prevent it from happening say in the middle of a device exchange. Or during a code
	// distribution for some reason. At least we'll wait till those processes have completed.
	// Masters switch into forced after the scan completes or while during normal token making
	// too many communication errors have occurred for a particular box (4 is the limit).
	BOOL_32					flag_chain_down_at_the_next_opportunity;

	// This timer gets started only when we enter forced. That means we will stay in forced for the duration of the timer.
	// Then do a scan and potentially come out of forced with a successful scan.
	xTimerHandle			timer_rescan;

	// 4/30/2014 rmd : This timer is restarted upon receipt of each token. If it expires the
	// chain_down request is made.
	xTimerHandle			timer_token_arrival;

	// 9/8/2016 rmd : This variable manages the rescan timing. It is 0 on a code restart. And
	// should also be zeroed after a successful scan (so that the timing scheme is
	// restarted).
	UNS_32					scans_while_chain_is_down;

	// 9/8/2016 rmd : We needed a scheme to clear the cumulative error counts that can occur
	// when a slave doesn't respond to his token. Clearing it at midnight is what the 2000e did
	// so we'll just continue with that.
	BOOL_32					we_have_crossed_midnight_so_clear_the_error_counts;
	
	// ----------

	BOOL_32					start_a_scan_at_the_next_opportunity;

	// 10/30/2017 rmd : Need to capture the reason to stamp when the scan ACTUALLY starts.
	// Sometimes there are timers waiting for devices to connect and such (like for SR) so we
	// submit our request to start the scan but it doesn't acually start for say 30 seconds for
	// example. When it actually starts we stamp the alert line. If not we get confused thinking
	// the scan started but in reality it has not yet started.
	UNS_32					start_a_scan_captured_reason;

	// ----------
	
	// 4/3/2014 rmd : Flags a new appearance of this controller in the chain. So that we may
	// issue the CLEAN HOUSE command to this controller. Based upon three criteria. 1) The index
	// wasn't seen previously. 2) The serial number has changed. and 3) The controller itself
	// tells us it wants to be declared new (one way this can happen is if the user changes the
	// letter at a SLAVE).
	BOOL_32					is_a_NEW_member_in_the_chain[ MAX_CHAIN_LENGTH ];

	// ----------
	
	// 5/2/2014 rmd : Four flags to manage what the user is allowed to do (screen wise)
	// following a reboot. We require a certain number of tokens and token responses that do not
	// contain pdata and chain members type data. Because that is a sign the hardware and
	// settings have not been fully gathered and distributed throughout the chain. The first two
	// are used by a master. The second two by slaves.
	
	UNS_32					since_the_scan__master__number_of_clean_tokens_made;

	UNS_32					since_the_scan__master__number_of_clean_token_responses_rcvd[ MAX_CHAIN_LENGTH ];

	UNS_32					since_the_scan__slave__number_of_clean_tokens_rcvd;

	UNS_32					since_the_scan__slave__number_of_clean_token_responses_made;

	// ----------

	// 4/29/2014 rmd : These 2 flags work together to plug an operational scenario that leads to
	// an incorrect hardware list at a SLAVE in a flowsense chain. Its annoying and doesn't
	// easily self correct so this is a direct attack on it. The file CRC test would eventually
	// correct it but I'm going after it. The scenario is this 1.Chain up and running normally
	// 2. User goes to a slave and switches to a standalone. This causes unit to hide hardware
	// at other boxes. 3. User switches back to POAFS slave. NOW the station list (and others
	// like POC list) are incorrect. If this is done quickly the master is still running along.
	// And even following a scan this scenario is NOT corrected. Because the SLAVE is not seen
	// by the master as NEW.
	//
	// The solution is the two flags that follow. The first flag forces a scan. The second flag
	// forces the master to see the slave as new which in turn triggers all hardware to be
	// distributed.
	//
	// These flags only intended for use at a slave. If this scenario is performed at a master
	// he will perform the scan when made back into a master. And ALL slaves will be seen as NEW
	// because they are appearing in the chain for the first time during the scan.
	//
	// 4/29/2014 rmd : SEE NOTE FOR SLAVES IN THE start_a_scan FUNCTION REGARDING THESE TWO
	// FLAGS. These flags only intended for use at a slave.
	BOOL_32					request_a_scan_in_the_next_token_RESP;

	BOOL_32					flag_myself_as_NEW_in_the_scan_RESP;

	// ----------

	// 11/26/2014 rmd : These two variables work together to manage the case when part of an up
	// and running chain restarts (usually due to short duration power problem at a slave). The
	// restart happens quick enough that the master does not fall into forced so tokens just
	// keep on coming. Well if there was irrigation taking place at the time we now have a
	// disjointed system. The irri list at the slave is not sync'd with the foal irri list. That
	// is because the irri side list is not battery backed.
	BOOL_32					i_am_a_slave_and_i_have_rebooted;  // this one cleared when scan received

	BOOL_32					i_am_the_master_and_i_have_rebooted;  // this one cleared when scan completed
	
	// ----------

	BOOL_32					pending_device_exchange_request;

	// ----------

	// 10/10/2018 rmd : Two counters tracking chain token errors. The first one is used to
	// declare a chain down. We need 4 errors to declare a chain down. The criteria used to be 4
	// errors within a 24 hour period, but on some SR chains (current implementation does not
	// include our transport layer) we would see say one error ever 4 hours for example, and
	// each day this would eventually lead to chain down. To avoid this, we changed the criteria
	// to need 4 errors IN A ROW to declare a chain down.
	//
	// 10/10/2018 rmd : The second counter is for informational purposes, to understand how many
	// of these 'intermittent' errors we are seeing each day. At midnight, if this value is
	// non-zero we stamp an alert line. I think this is useful to assess a chains communication
	// health.
	UNS_32					failures_to_respond_to_the_token[ MAX_CHAIN_LENGTH ];

	UNS_32					failures_to_respond_to_the_token_today[ MAX_CHAIN_LENGTH ];
	
	// ----------
	
	BOOL_32					main_app_firmware_up_to_date[ MAX_CHAIN_LENGTH ];

	BOOL_32					tpmicro_firmware_up_to_date[ MAX_CHAIN_LENGTH ];

	// 4/15/2014 rmd : A flag for use at the master. To determine if the chain members array is
	// to be distributed. This would get set if a controller delivered a
	// BOX_CONFIGURATION_STRUCT that was different than what the master had on record.
	BOOL_32					broadcast_chain_members_array;
	
	// ------------

	// 3/13/2014 rmd : Only one type of exchange on one port at a time may be pending or in
	// process. Meaning only a SINGLE programming or reading sequence can be queued or in
	// process.
	UNS_32					device_exchange_initial_event;

	UNS_32					device_exchange_port;

	UNS_32					device_exchange_state;

	UNS_32					device_exchange_device_index;

	// 3/19/2014 rmd : When we switch into the comm mngr device exchange state capture what
	// state the comm mngr was is. So after we are done with the device exchange we can restore
	// to how it was. This may no longer be necessary as we can only switch to the device
	// exchange mode when the comm mngr is in the making tokens mode.
	UNS_32					device_exchange_saved_comm_mngr_mode;

	xTimerHandle			timer_device_exchange;

	// ------------

	// Timer which decides when to give up waiting for a response from a message that the commmngr is controlling (all messages
	// are controlled by the comm manager - and there is only one active comm manager in a system of controllers. A system is
	// defined as a group of controllers that could potentially play foal.
	xTimerHandle			timer_message_resp;

	xTimerHandle			timer_token_rate_timer;

	// ------------

	BOOL_32					token_rate_timer_has_timed_out;

	BOOL_32					token_in_transit;

	// ------------

	// A list of outgoing packets that need to be synchronized with the receipt of the token. When the token itself is received
	// these packets will be sent before the token resp is generated and sent.
	MIST_LIST_HDR_TYPE		packets_waiting_for_token;
	
	// All incoming messages from the transport for this controller end up on this list. They are sorted through and processed
	// by the comm mngr. Only one list!
	MIST_LIST_HDR_TYPE		incoming_messages_or_packets;

	// ------------

	// 10/1/2012 rmd : User edits support. For distribution of changes throughout the members of
	// a chain.
	CHANGE_TRACKING_FOR_COMMUNICATIONS_STRUCT	changes;

	// ------------

	// 4/20/2015 rmd : To monitor from the commserver incoming msg receipt progress. And abandon
	// and free allocated memory if a timeout occurs.
	xTimerHandle			timer_commserver_msg_receipt_error;

	// ----------

	// 12/03/2015 skc : When the comm server (or keypad) has data for a new datetime value,
	// set this flag to true. After processing the new datetime, set the flag to false.
	BOOL_32					flag_update_date_time;

	// 12/03/2015 skc : Store the new datetime data here for the comm event loop to access in token creation.
	DATE_TIME_TOKEN_STRUCT	token_date_time;

	// ----------

	// 5/9/2016 ajv : Setting this flag (done via communication) triggers ALL of the controllers
	// in the chain to perform a 2-Wire discovery upon receipt of the next token.
	BOOL_32					perform_two_wire_discovery;

	// ----------
	
	// 7/21/2016 rmd : A timer and associated indication if the timer has expired. To postpone
	// scanning and token generation from a MASTER if needed depending on the devices seen
	// within the network.
	xTimerHandle			flowsense_device_establish_connection_timer;

	BOOL_32					flowsense_devices_are_connected;

} COMM_MNGR_STRUCT;

// ------------

typedef struct
{
	MIST_DLINK_TYPE				dl;         // list support

	DATA_HANDLE     			dh;         // to the actual message

	UPORTS						to_port;	// port packet is headed towards
	
	// Passed on to the serial driver to trigger the driver to notify the TPL_OUT task when a packet with it's status
	// request bit set has been sent.
	OUTGOING_MESSAGE_STRUCT		*pom;
	
} PACKET_ROUTING_LIST_ITEM;

// ------------

extern COMM_STATS				comm_stats;

extern COMM_MNGR_STRUCT			comm_mngr;

extern RECENT_CONTACT_STRUCT	last_contact, next_contact;

// 6/30/2017 rmd : A powerful global used to effectively idle all tasks that interface to
// serial ports A & B.
extern BOOL_32		in_device_exchange_hammer;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// function prototypes

extern void power_up_device( UNS_32 pport );

extern void power_down_device( UNS_32 pport );


extern BOOL_32 COMM_MNGR_network_is_available_for_normal_use( void );


extern UNS_32 COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members( void );

extern void COMM_MNGR_alert_if_chain_members_should_not_be_referenced( char *fil, UNS_32 lin );


extern BOOL_32 tpmicro_is_available_for_token_generation( void );


// 3/25/2014 rmd : This would be a static function but it is referenced from the
// tpmicro_comm.c source. Which is really an extention of the comm_mngr itself.
// Intentionally does not use the COMM_MNGR naming convention.
extern void setup_to_check_if_tpmicro_needs_a_code_update( void );


extern void CENT_COMM_make_copy_of_and_add_message_or_packet_to_list_of_incoming_messages( UPORTS const pport, DATA_HANDLE const pdh, ROUTING_CLASS_DETAILS_STRUCT const pmessage_class, ADDR_TYPE const pfrom_to );
	

extern void COMM_MNGR_task( void *pvParameters );


extern void COMM_MNGR_device_exchange_results_to_key_process_task( UNS_32 pdevice_exchange_key_results );


extern void COMM_MNGR_post_event_with_details( COMM_MNGR_TASK_QUEUE_STRUCT *pq_item_ptr );

extern void COMM_MNGR_post_event( UNS_32 pevent );


extern void task_control_ENTER_device_exchange_hammer( void );

extern void task_control_EXIT_device_exchange_hammer( void );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

