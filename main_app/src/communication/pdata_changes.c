/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy
#include	<string.h>

#include	"cs3000_comm_server_common.h"

#include	"pdata_changes.h"

#include	"app_startup.h"

#include	"bithacks.h"

#include	"group_base_file.h"

#include	"station_groups.h"

#include	"irrigation_system.h"

#include	"poc.h"

#include	"lights.h"

#include	"stations.h"

#include	"weather_control.h"

#include	"manual_programs.h"

#include	"configuration_network.h"

#include	"cs_mem.h"

#include	"flash_storage.h"

#include	"change.h"

#include	"alerts.h"

#include	"background_calculations.h"

#include	"flowsense.h"

#include	"screen_utils.h"

#include	"dialog.h"

#include	"moisture_sensors.h"

#include	"walk_thru.h"

#ifndef _MSC_VER

	#include	"ftimes_task.h"

	#include	"chain_sync_funcs.h"

#endif




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */

#ifndef _MSC_VER
static UNS_32 PDATA_extract_settings( UNS_8 **pucp,
									  const UNS_32 preason_for_change,
									  PDATA_CHANGES_BIT_FIELD_TYPE *pbitfield_of_rcvd_changes,
									  const UNS_32 pbit,
									  UNS_32 (*pextract_and_store_changes_func_ptr)(const UNS_8 *pucp, const UNS_32 preason_for_change, const BOOL_32 pset_change_bits, const UNS_32 pchanges_received_from),
									  void *plist_recursive_mutex,
									  const BOOL_32 pset_change_bits,
									  const UNS_32 pchanges_received_from )
#else
static UNS_32 PDATA_extract_settings( UNS_8 **pucp,
									  const UNS_32 preason_for_change,
									  PDATA_CHANGES_BIT_FIELD_TYPE *pbitfield_of_rcvd_changes,
									  const UNS_32 pbit,
									  UNS_32 (*pextract_and_store_changes_func_ptr)(const UNS_8 *pucp, const UNS_32 preason_for_change, const BOOL_32 pset_change_bits, const UNS_32 pchanges_received_from, char *pSQL_statements, char *pSQL_search_condition_str, char *pSQL_update_str, char *pSQL_insert_fields_str, char *pSQL_insert_values_str, char *pSQL_single_merge_statement_buf, const UNS_32 pnetwork_ID),
									  const BOOL_32 pset_change_bits,
									  const UNS_32 pchanges_received_from,
									  char *pSQL_statements,
									  char *pSQL_search_condition_str,
									  char *pSQL_update_str,
									  char *pSQL_insert_fields_str,
									  char *pSQL_insert_values_str,
									  char *pSQL_single_merge_statement_buf,
									  const UNS_32 pnetwork_ID )
#endif
{
	UNS_32	lsize_of_changes;

	UNS_32	rv;

	rv = 0;

	if( B_IS_SET_64( *pbitfield_of_rcvd_changes, pbit ) == (true) )
	{
		#ifndef _MSC_VER
			xSemaphoreTakeRecursive( plist_recursive_mutex, portMAX_DELAY );

			lsize_of_changes = pextract_and_store_changes_func_ptr( *pucp, preason_for_change, pset_change_bits, pchanges_received_from );

			xSemaphoreGiveRecursive( plist_recursive_mutex );

			// 4/30/2015 ajv : Since we've clear bits in the original bit field and potentially set bits
			// in the pending comm server change bit field, initiate a file save to ensure these changes
			// are stored to the file system in the event of a power fail.
			if( lsize_of_changes > 0 )
			{
				// 5/13/2015 ajv : We want to save the file in the following cases:
				// 
				//   - A message from the CommServer since we've responded that we received it and need to
				//     ensure it's saved.
				//   - A token response from the slave to the master
				//   - A token from the master to the slave
				//   
				// We DO NOT want to save the file when a token is received by the master since we've
				// already saved the file for one of the reasons above. This will otherwise result in a
				// double save for each change - once when it comes in and once when it's received when the
				// master distributes the changes back out.
				if( (pchanges_received_from == CHANGE_REASON_CENTRAL_OR_MOBILE) ||
					(pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE) ||
					((pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER) && (!FLOWSENSE_we_are_a_master_one_way_or_another())) )
				{
					FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( pbit, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
				}
				
				// ----------
				
				// 9/4/2018 rmd : Because there are changes being pushed around the network inhibit the
				// chain_sync checksum checking for a while, 16 clean tokens needed before it starts again.
				// Remember we are actually making a custom checksum of the structure in memory, not the
				// file, so any possible change should qualify to break off chain_sync testing for now. This
				// technically may be too stringent, as we only update the checksum when there is a file
				// save, but I don't want to play around squeezing out the last possible checksum test with
				// changes looming.
				CHAIN_SYNC_inhibit_crc_inclusion_in_the_token_response( pbit );
			}

		#else

			// 1/16/2015 ajv : Make sure the buffer is fully initialized to NULL since we rely on this
			// to determine whether the string is empty or not.
			memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );

			lsize_of_changes = pextract_and_store_changes_func_ptr( *pucp, preason_for_change, pset_change_bits, pchanges_received_from, pSQL_statements, pSQL_search_condition_str, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_single_merge_statement_buf, pnetwork_ID );

		#endif

		*pucp += lsize_of_changes;
		rv += lsize_of_changes;

		B_UNSET( *pbitfield_of_rcvd_changes, pbit );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Extracts the changes to program data from the token or token response and stores them.
 * 
 * @executed Executed within the context of the CommMngr task as a token or token response
 * is being parsed.
 * 
 * @param pucp A pointer to the data that was received.
 * 
 * @param pnum_groups The number of groups that were changed, as indicated by the token or
 * token response.
 * 
 * @param preason_for_change The reason for the change, such as keypad.
 * 
 * @param pset_change_bits True if this routine is called from the FOAL machine; otherwise,
 * false. This ensures the settings are redistributed after the change reaches the master.
 *
 * @author 8/29/2011 Adrianusv
 *
 * @revisions
 *    8/29/2011 Initial release
 *    3/26/2012 Added support for new data sync specification
 */
DLLEXPORT UNS_32 PDATA_extract_and_store_changes( UNS_8 *pucp,
												  const UNS_32 preason_for_change,
												  const BOOL_32 pset_change_bits,
												  const UNS_32 pchanges_received_from
												  #ifdef _MSC_VER
												  , char *pSQL_statements,
												  char *pSQL_search_condition_str,
												  char *pSQL_update_str,
												  char *pSQL_insert_fields_str,
												  char *pSQL_insert_values_str,
												  char *pSQL_single_merge_statement_buf,
												  const UNS_32 pnetwork_ID
												  #endif
												)
{
	PDATA_CHANGES_BIT_FIELD_TYPE	lbitfield_of_changes;

	UNS_8	lsize_of_bitfield;

	UNS_32	rv;

	rv = 0;

	// ----------

	// Extract out the size of the bit field. We don't currently use this, but we may use it in
	// the future if the size of the bit field grows beyond 8 bytes.
	memcpy( &lsize_of_bitfield, pucp, sizeof(lsize_of_bitfield) );
	pucp += sizeof( lsize_of_bitfield );
	rv += sizeof( lsize_of_bitfield );

	// ----------

	// Extract the bit field which identifies which groups and variables have changed.
	memcpy( &lbitfield_of_changes, pucp, sizeof(lbitfield_of_changes) );
	pucp += sizeof( lbitfield_of_changes );
	rv += sizeof( lbitfield_of_changes );

	// ----------

	// Extract the changes based upon which bits are set in the bit field of changes.
	#ifndef _MSC_VER

		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_NETWORK_CONFIGURATION, &NETWORK_CONFIG_extract_and_store_changes_from_comm, list_program_data_recursive_MUTEX, pset_change_bits, pchanges_received_from );
		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_WEATHER_CONTROL, &nm_WEATHER_extract_and_store_changes_from_comm, weather_control_recursive_MUTEX, pset_change_bits, pchanges_received_from );
		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_IRRIGATION_SYSTEM, &nm_SYSTEM_extract_and_store_changes_from_comm, list_system_recursive_MUTEX, pset_change_bits, pchanges_received_from );
		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_LANDSCAPE_DETAILS, &nm_STATION_GROUP_extract_and_store_changes_from_comm, list_program_data_recursive_MUTEX, pset_change_bits, pchanges_received_from );
		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_MANUAL_PROGRAMS, &nm_MANUAL_PROGRAMS_extract_and_store_changes_from_comm, list_program_data_recursive_MUTEX, pset_change_bits, pchanges_received_from );
		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_STATION_INFO, &nm_STATION_extract_and_store_changes_from_comm, list_program_data_recursive_MUTEX, pset_change_bits, pchanges_received_from );
		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_POC, &nm_POC_extract_and_store_changes_from_comm, list_poc_recursive_MUTEX, pset_change_bits, pchanges_received_from );
		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_LIGHTS, &nm_LIGHTS_extract_and_store_changes_from_comm, list_lights_recursive_MUTEX, pset_change_bits, pchanges_received_from );
		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_MOISTURE_SENSORS, &nm_MOISTURE_SENSOR_extract_and_store_changes_from_comm, moisture_sensor_items_recursive_MUTEX, pset_change_bits, pchanges_received_from );
		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_WALK_THRU, &nm_WALK_THRU_extract_and_store_changes_from_comm, walk_thru_recursive_MUTEX, pset_change_bits, pchanges_received_from );

		// ----------

		// 5/4/2015 ajv : The next scheduled irrigation is normally kept up-to-date by recalculating
		// every time a change is made. However, when Program Data changes are received, either via
		// a token, token response, or from Command Center Online, it's important to ensure the
		// controller determines the next scheduled irrigation based on the current programming.
		// Therefore, post a message to update it.
		postBackground_Calculation_Event( BKGRND_CALC_NEXT_SCHEDULED );

		// ----------

		// 7/19/2018 rmd : There has been a change, well at least I think if this function executes,
		// potentially a change just happened, so restart the ftimes calculation.
		FTIMES_TASK_restart_calculation();
		
		// ----------

		// 5/15/2015 ajv : If the changes came in via Command Center Online, force a full redraw of
		// the screen. This resolves an issue where changes could be lost if the user were viewing a
		// screen when changes were received because pressing BACK would revert the changes.
		// 
		// Note that this isn't extremely elegant. It doesn't retain the highlighted cursor field or
		// the highlighted menu item if on a scrolling menu. These are future enhancements we can
		// look at.
		// 
		// 6/9/2015 ajv : In the case of the main menu, there's no need to refresh the screen.
		// Ultimately, this is really intended to handle the case of a user editing a value while a
		// change comes in from Command Center Online which will never be the case on the main menu.
		if( (pchanges_received_from == CHANGE_REASON_CENTRAL_OR_MOBILE) && (GuiLib_CurStructureNdx != GuiStruct_scrMainMenu_0) )
		{
			// 6/9/2015 ajv : Make sure any dialogs that are open are closed before we redraw the
			// screen to ensure the redraw is processed properly.
			DIALOG_close_all_dialogs();

			Redraw_Screen( (true) );
		}

	#else

		memset( pSQL_statements, 0x00, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS );

		// ----------

		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_NETWORK_CONFIGURATION, &NETWORK_CONFIG_extract_and_store_changes_from_comm, pset_change_bits, pchanges_received_from, (char *)pSQL_statements, (char *)pSQL_search_condition_str, (char *)pSQL_update_str, (char *)pSQL_insert_fields_str, (char *)pSQL_insert_values_str, (char *)pSQL_single_merge_statement_buf, pnetwork_ID );
		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_WEATHER_CONTROL, &nm_WEATHER_extract_and_store_changes_from_comm, pset_change_bits, pchanges_received_from, (char *)pSQL_statements, (char *)pSQL_search_condition_str, (char *)pSQL_update_str, (char *)pSQL_insert_fields_str, (char *)pSQL_insert_values_str, (char *)pSQL_single_merge_statement_buf, pnetwork_ID );
		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_IRRIGATION_SYSTEM, &nm_SYSTEM_extract_and_store_changes_from_comm, pset_change_bits, pchanges_received_from, (char *)pSQL_statements, (char *)pSQL_search_condition_str, (char *)pSQL_update_str, (char *)pSQL_insert_fields_str, (char *)pSQL_insert_values_str, (char *)pSQL_single_merge_statement_buf, pnetwork_ID );
		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_LANDSCAPE_DETAILS, &nm_STATION_GROUP_extract_and_store_changes_from_comm, pset_change_bits, pchanges_received_from, (char *)pSQL_statements, (char *)pSQL_search_condition_str, (char *)pSQL_update_str, (char *)pSQL_insert_fields_str, (char *)pSQL_insert_values_str, (char *)pSQL_single_merge_statement_buf, pnetwork_ID );
		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_MANUAL_PROGRAMS, &nm_MANUAL_PROGRAMS_extract_and_store_changes_from_comm, pset_change_bits, pchanges_received_from, (char *)pSQL_statements, (char *)pSQL_search_condition_str, (char *)pSQL_update_str, (char *)pSQL_insert_fields_str, (char *)pSQL_insert_values_str, (char *)pSQL_single_merge_statement_buf, pnetwork_ID );
		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_STATION_INFO, &nm_STATION_extract_and_store_changes_from_comm, pset_change_bits, pchanges_received_from, (char *)pSQL_statements, (char *)pSQL_search_condition_str, pSQL_update_str, (char *)pSQL_insert_fields_str, (char *)pSQL_insert_values_str, (char *)pSQL_single_merge_statement_buf, pnetwork_ID );
		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_POC, &nm_POC_extract_and_store_changes_from_comm, pset_change_bits, pchanges_received_from, (char *)pSQL_statements, (char *)pSQL_search_condition_str, (char *)pSQL_update_str, (char *)pSQL_insert_fields_str, (char *)pSQL_insert_values_str, (char *)pSQL_single_merge_statement_buf, pnetwork_ID );
		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_LIGHTS, &nm_LIGHTS_extract_and_store_changes_from_comm, pset_change_bits, pchanges_received_from, (char *)pSQL_statements, (char *)pSQL_search_condition_str, (char *)pSQL_update_str, (char *)pSQL_insert_fields_str, (char *)pSQL_insert_values_str, (char *)pSQL_single_merge_statement_buf, pnetwork_ID );
		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_MOISTURE_SENSORS, &nm_MOISTURE_SENSOR_extract_and_store_changes_from_comm, pset_change_bits, pchanges_received_from, (char *)pSQL_statements, (char *)pSQL_search_condition_str, (char *)pSQL_update_str, (char *)pSQL_insert_fields_str, (char *)pSQL_insert_values_str, (char *)pSQL_single_merge_statement_buf, pnetwork_ID );
		rv += PDATA_extract_settings( &pucp, preason_for_change, &lbitfield_of_changes, FF_WALK_THRU, &nm_WALK_THRU_extract_and_store_changes_from_comm, pset_change_bits, pchanges_received_from, (char *)pSQL_statements, (char *)pSQL_search_condition_str, (char *)pSQL_update_str, (char *)pSQL_insert_fields_str, (char *)pSQL_insert_values_str, (char *)pSQL_single_merge_statement_buf, pnetwork_ID);

	#endif

	// ----------

	return ( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifdef _MSC_VER

/* ---------------------------------------------------------- */
/**
 * Using the passed in database table and field names, return the change bit associated with
 * the variable, as well as the size of the variable and the secondary sort order (0 if not 
 * part of an array; otherwise, the index into the array).
 */
DLLEXPORT INT_32 PDATA_get_change_bit_number( char *ptable_name, char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order )
{
	INT_32 rv;

	rv = -1;

	if( strncmp( ptable_name, DATABASE_TABLE_NAME_CONTROLLERS, NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
	}
	else if( strncmp( ptable_name, DATABASE_TABLE_NAME_MANUAL_PROGRAMS, NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MANUAL_PROGRAMS_get_change_bit_number( pfield_name, psize_of_var_ptr, psecondary_sort_order );
	}
	else if( strncmp( ptable_name, DATABASE_TABLE_NAME_NETWORKS, NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = NETWORK_CONFIG_get_change_bit_number( pfield_name, psize_of_var_ptr, psecondary_sort_order );
	}
	else if( strncmp( ptable_name, DATABASE_TABLE_NAME_POCS, NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = POC_get_change_bit_number( pfield_name, psize_of_var_ptr, psecondary_sort_order );
	}
	else if( strncmp( ptable_name, DATABASE_TABLE_NAME_STATION_GROUPS, NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_get_change_bit_number( pfield_name, psize_of_var_ptr, psecondary_sort_order );
	}
	else if( strncmp( ptable_name, DATABASE_TABLE_NAME_STATIONS, NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_get_change_bit_number( pfield_name, psize_of_var_ptr, psecondary_sort_order );
	}
	else if( strncmp( ptable_name, DATABASE_TABLE_NAME_SYSTEMS, NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = SYSTEM_get_change_bit_number( pfield_name, psize_of_var_ptr, psecondary_sort_order );
	}
	else if( strncmp( ptable_name, DATABASE_TABLE_NAME_WEATHER, NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_get_change_bit_number( pfield_name, psize_of_var_ptr, psecondary_sort_order );
	}
	else if( strncmp( ptable_name, DATABASE_TABLE_NAME_LIGHTS, NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = LIGHTS_get_change_bit_number( pfield_name, psize_of_var_ptr, psecondary_sort_order );
	}
	else if( strncmp(ptable_name, DATABASE_TABLE_NAME_MOISTURE_SENSORS, NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MOISTURE_SENSOR_get_change_bit_number( pfield_name, psize_of_var_ptr, psecondary_sort_order );
	}
	else if ( strncmp(ptable_name, DATABASE_TABLE_NAME_WALK_THRU, NUMBER_OF_CHARS_IN_A_NAME) == 0 )
	{
		rv = WALK_THRU_get_change_bit_number(pfield_name, psize_of_var_ptr, psecondary_sort_order);
	}

	return( rv );
}

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern UNS_32 PDATA_allocate_memory_for_and_initialize_memory_for_sending_data( UNS_8 **pucp,
																				   PDATA_CHANGES_BIT_FIELD_TYPE *pbitfield_of_files_that_have_changed,
																				   UNS_8 *pbitfield_size,
																				   const UNS_32 psizeof_bitfield_size,
																				   UNS_8 **plocation_of_bitfield,
																				   const UNS_32 pmemory_to_allocate )
{
	UNS_32	rv;

	rv = 0;

	// ----------

	*pucp = NULL;

	#ifndef _MSC_VER

		// 1/14/2015 ajv : Allocate the maximum amount of memory we'll allow for a token. We're
		// going to use the 'rv' value to determine where we are in relation to this and stop when
		// we reach the limit.
		//
		// 4/22/2015 rmd : In the case of a message to the commserver we are getting a VERY large
		// block. Actually the very biggest memory block in the pool. MUST be prepared that it isn't
		// available.
		if( !mem_obtain_a_block_if_available( pmemory_to_allocate, (void**)pucp ) )
		{
			// 4/22/2015 rmd : Make sure.
			*pucp = NULL;
		}

	#else

		*pucp = malloc( pmemory_to_allocate );

	#endif

	// ----------

	// Initialize the bit field
	*pbitfield_of_files_that_have_changed = 0x00;

	*pbitfield_size = sizeof( PDATA_CHANGES_BIT_FIELD_TYPE );

	// ----------

	if( *pucp != NULL )
	{
		rv = PDATA_copy_bitfield_info_into_pucp( pucp,
												 *pbitfield_size,
												 psizeof_bitfield_size,
												 plocation_of_bitfield );
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 PDATA_allocate_space_for_num_changed_groups_in_pucp(	UNS_8 **pucp,
																	UNS_8 **llocation_of_num_changed_groups,
																	const UNS_32 pmem_used_so_far,
																	BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
																	const UNS_32 pallocated_memory )
{
	UNS_32	rv;

	rv = 0;

	if( (*pmem_used_so_far_is_less_than_allocated_memory == (true)) && ((pmem_used_so_far + sizeof(UNS_32)) < pallocated_memory) )
	{
		*llocation_of_num_changed_groups = *pucp;
		*pucp += sizeof( UNS_32 );
		rv += sizeof( UNS_32 );
	}
	else
	{
		*llocation_of_num_changed_groups = NULL;

		*pmem_used_so_far_is_less_than_allocated_memory = (false);
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 PDATA_copy_var_into_pucp( UNS_8 **pucp,
										   const void *const pvar,
										   const UNS_32 psize_of_var )
{
	UNS_32	rv;

	rv = 0;

	memcpy( *pucp, pvar, psize_of_var );
	*pucp += psize_of_var;
	rv += psize_of_var;

	return( rv );
}

/* ---------------------------------------------------------- */
DLLEXPORT UNS_32 PDATA_copy_bitfield_info_into_pucp( UNS_8 **pucp,
													 const UNS_32 psize_of_bitfield,
													 const UNS_32 psize_of_size_of_bitfield,
													 UNS_8 **plocation_of_bitfield )
{
	UNS_32	rv;

	rv = 0;

	rv += PDATA_copy_var_into_pucp( pucp, &psize_of_bitfield, psize_of_size_of_bitfield );

	// Skip over the location for the bit field for now. We'll fill this back in once we've
	// filled out the bit field.
	*plocation_of_bitfield = *pucp;
	*pucp += psize_of_bitfield;
	rv += psize_of_bitfield;

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 PDATA_copy_var_into_pucp_and_set_32_bit_change_bits( UNS_8 **pucp,
																	  CHANGE_BITS_32_BIT *pbitfield_of_changes_to_send,
																	  const UNS_32 pbit_to_set,
																	  CHANGE_BITS_32_BIT *pchange_bits_for_controller_ptr,
																	  CHANGE_BITS_32_BIT *ppending_change_bits_to_send_to_comm_server,
																	  void *pvar,
																	  const UNS_32 psize_of_var,
																	  const UNS_32 pmem_used_so_far,
																	  BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
																	  const UNS_32 pallocated_memory,
																	  const UNS_32 preason_data_is_being_built )
{
	UNS_32	rv;

	rv = 0;

	// 1/12/2015 ajv : The comm server calls this routine directly for each changed variable so
	// there's no change structure to look at for changes.
	#ifndef _MSC_VER
	if( B_IS_SET( *pchange_bits_for_controller_ptr, pbit_to_set ) == (true) )
	#endif
	{
		if( (pmem_used_so_far + psize_of_var) < pallocated_memory )
		{
			rv += PDATA_copy_var_into_pucp( pucp, pvar, psize_of_var );

			B_SET( *pbitfield_of_changes_to_send, pbit_to_set );

			// 1/12/2015 ajv : Since the comm server calls this routine directly without a change bit
			// structure indicating what's changed, there's nothing to unset.
			#ifndef _MSC_VER

				// 4/30/2015 ajv : Always clear the bit in the original structure once it's been added to
				// the message. For the case of uploading to the CommServer, which requires an ACK, we're
				// also going to set the same bit in an additional structure. If we receive the ACK, we'll
				// clear the structure. If not, we'll use it to reset the bits in the original bit field so
				// the variables are resent in the next attempt.
				B_UNSET( *pchange_bits_for_controller_ptr, pbit_to_set );

				// 4/30/2015 ajv : Set the associated bit in the pending change bit field now. See the
				// comment above for more information.
				if( preason_data_is_being_built == CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER )
				{
					B_SET( *ppending_change_bits_to_send_to_comm_server, pbit_to_set );
				}

			#endif
		}
		else
		{
			*pmem_used_so_far_is_less_than_allocated_memory = (false);
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 PDATA_copy_var_into_pucp_and_set_64_bit_change_bits( UNS_8 **pucp,
																	  CHANGE_BITS_64_BIT *pbitfield_of_changes_to_send,
																	  const UNS_32 pbit_to_set,
																	  CHANGE_BITS_64_BIT *pchange_bits_for_controller_ptr,
																	  CHANGE_BITS_64_BIT *ppending_change_bits_to_send_to_comm_server,
																	  void *pvar,
																	  const UNS_32 psize_of_var,
																	  const UNS_32 pmem_used_so_far,
																	  BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
																	  const UNS_32 pallocated_memory,
																	  const UNS_32 preason_data_is_being_built )
{
	UNS_32	rv;

	rv = 0;

	// 1/12/2015 ajv : The comm server calls this routine directly for each changed variable so
	// there's no change structure to look at for changes.
	#ifndef _MSC_VER
	if( B_IS_SET_64( *pchange_bits_for_controller_ptr, pbit_to_set ) == (true) )
	#endif
	{
		if( (pmem_used_so_far + psize_of_var) < pallocated_memory )
		{
			rv += PDATA_copy_var_into_pucp( pucp, pvar, psize_of_var );

			// 11/25/2013 ajv : Since the bit field is 64-bits, we MUST use the
			// associated B_SET routine.
			B_SET_64( *pbitfield_of_changes_to_send, pbit_to_set );

			// 1/12/2015 ajv : Since the comm server calls this routine directly without a change bit
			// structure indicating what's changed, there's nothing to unset.
			#ifndef _MSC_VER

				// 4/30/2015 ajv : Always clear the bit in the original structure once it's been added to
				// the message. For the case of uploading to the CommServer, which requires an ACK, we're
				// also going to set the same bit in an additional structure. If we receive the ACK, we'll
				// clear the structure. If not, we'll use it to reset the bits in the original bit field so
				// the variables are resent in the next attempt.
				B_UNSET_64( *pchange_bits_for_controller_ptr, pbit_to_set );

				// 4/30/2015 ajv : Set the associated bit in the pending change bit field now. See the
				// comment above for more information.
				if( preason_data_is_being_built == CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER )
				{
					B_SET_64( *ppending_change_bits_to_send_to_comm_server, pbit_to_set );
				}

			#endif
		}
		else
		{
			*pmem_used_so_far_is_less_than_allocated_memory = (false);
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static void PDATA_set_bit_that_file_has_changed( const UNS_32 pfile_that_has_changed,
													PDATA_CHANGES_BIT_FIELD_TYPE *pdata_bitfield_of_files_that_have_changed )
{
	B_SET_64( *pdata_bitfield_of_files_that_have_changed, pfile_that_has_changed );
}

/* ---------------------------------------------------------- */
static void PDATA_copy_file_bitfield_into_pucp( UNS_8 **plocation_of_bitfield,
												   PDATA_CHANGES_BIT_FIELD_TYPE *pbitfield_of_files_that_have_changed,
												   const UNS_32 psize_of_bitfield )

{
	memcpy( *plocation_of_bitfield, pbitfield_of_files_that_have_changed, psize_of_bitfield );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static UNS_32 PDATA_build_setting_to_send( const UNS_32 pfile_index,
										   PDATA_CHANGES_BIT_FIELD_TYPE *pdata_bitfield_of_files_that_have_changed,
										   UNS_32 (*pbuild_data_func_ptr)(UNS_8 **pucp,
																		  const UNS_32 pmem_used_so_far,
																		  BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
																		  const UNS_32 preason_data_is_being_built,
																		  const UNS_32 pallocated_memory),
										   UNS_8 **pucp,
										   const UNS_32 pmem_used_so_far,
										   BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
										   const UNS_32 preason_data_is_being_built,
										   const UNS_32 pallocated_memory )
{
	UNS_32	rv;

	rv = 0;

	if( *pmem_used_so_far_is_less_than_allocated_memory == (true) )
	{
		rv += pbuild_data_func_ptr( pucp, pmem_used_so_far, pmem_used_so_far_is_less_than_allocated_memory, preason_data_is_being_built, pallocated_memory );

		if( rv > 0 )
		{
			PDATA_set_bit_that_file_has_changed( pfile_index, pdata_bitfield_of_files_that_have_changed );
			
			// ----------
			
			// 9/4/2018 rmd : Because there are changes being pushed around the network inhibit the
			// chain_sync checksum checking for a while, 16 clean tokens needed before it starts again.
			// Remember we are actually making a custom checksum of the structure in memory, not the
			// file, so any possible change should qualify to break off chain_sync testing for now. This
			// technically may be too stringent, as we only update the checksum when there is a file
			// save, and by the time we are here for CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER, we
			// might be able to avoid restarting the block out count, HOWEVER I don't want to play
			// around squeezing out the last possible checksum test with changes still looming.
			CHAIN_SYNC_inhibit_crc_inclusion_in_the_token_response( pfile_index );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Adds the changed data to the token. Also returns the number of bytes added which the
 * caller uses to increment the data pointer with.
 * 
 * @executed Executed within the context of the COMM_MNGR task when a token or token
 * response is being built.
 * 
 * @param pprogram_data_ptr A pointer to the data to send.
 * 
 * @return UNS_32 The number of bytes added to the passed in program data pointer.
 *
 * @author 8/29/2011 Adrianusv/BobD
 */

extern UNS_32 PDATA_build_data_to_send( DATA_HANDLE *pmsg_handle, const UNS_32 preason_data_is_being_built, const BOOL_32 this_is_to_ourself )
{
	// 4/22/2015 rmd : This function is executed within the context of the CONTROLLER_INITIATED
	// task. And within the context of the COMM_MNGR task.
	
	// 4/23/2015 rmd : Because the caller needs to know what took place here we use the return
	// value defines.
	
	// ----------
	
	PDATA_CHANGES_BIT_FIELD_TYPE	lbitfield_of_files_that_have_changed;

	UNS_8	pdata_bitfield_size;

	UNS_8	*ucp;

	UNS_8	*llocation_of_bitfield;

	UNS_32	lmessage_overhead;

	UNS_32	results;

	UNS_32	lmemory_to_allocate;
	
	BOOL_32	still_space_available;

	// ----------

	// 4/23/2015 rmd : Clean up the handle for the caller so when there is no message this can
	// be counted upon.
	pmsg_handle->dlen = 0;

	pmsg_handle->dptr = NULL;

	// ----------

	if( (preason_data_is_being_built == CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER) || (preason_data_is_being_built == CHANGE_REASON_SYNC_RECEIVING_AT_COMM_SERVER) )
	{
		// 4/22/2015 rmd : We just get the largest possible block available to build our message
		// into. When sent to the commserver the message is split up into 2K blocks that are
		// streamed out the internet connected device.
		lmemory_to_allocate = MAXIMUM_CODE_IMAGE_AND_INBOUND_OR_OUTBOUND_COMMSERVER_MESSAGE_SIZE;
	}
	else
	{
		if( this_is_to_ourself )
		{
			lmemory_to_allocate = MEMORY_TO_ALLOCATE_FOR_TOKEN_RESP_TO_OURSELF;
		}
		else
		{
			lmemory_to_allocate = MEMORY_TO_ALLOCATE_FOR_SENDING_PDATA_BETWEEN_CONTROLLERS;
		}
	}

	// ----------

	lmessage_overhead = PDATA_allocate_memory_for_and_initialize_memory_for_sending_data( &ucp,
																						  &lbitfield_of_files_that_have_changed,
																						  &pdata_bitfield_size,
																						  sizeof(pdata_bitfield_size),
																						  &llocation_of_bitfield,
																						  lmemory_to_allocate );
																						  
	if( ucp == NULL )
	{
		Alert_Message( "No memory to send program data" );
		
		results = PDATA_BUILD_RESULTS_no_memory;
	}
	else
	{
		// 5/1/2015 ajv : To ensure the pending_changes_to_send_to_comm_server flag isn't modified
		// while we're building a message to send, take the associated mutex. This way, we can
		// safely clear it when we're done and know that any changes the user made will set it again
		// and trigger another timer to start.
		xSemaphoreTakeRecursive( pending_changes_recursive_MUTEX, portMAX_DELAY );

		// ----------

		// 1/15/2015 ajv : Since the above function incremented ucp, we need to subtract the
		// overhead from ucp before we can grab a pointer to the beginning of the memory block.
		pmsg_handle->dptr = (ucp - lmessage_overhead);
	
		pmsg_handle->dlen = lmessage_overhead;
	
		// ----------
	
		still_space_available = (true);
		
		// 11/2/2018 rmd : NOBODY has noted this here, so i will. The order of the pdata content in
		// the message is critical due to the dependencies. For example the stations are assigned to
		// station groups, so the station groups had better exist and be correct, when the stations
		// are delivered. So that means the station groups MUST go before the stations! There are
		// more dependencies, for example poc's to mainlines.
		pmsg_handle->dlen += PDATA_build_setting_to_send( FF_NETWORK_CONFIGURATION, &lbitfield_of_files_that_have_changed, &NETWORK_CONFIG_build_data_to_send, &ucp, pmsg_handle->dlen, &still_space_available, preason_data_is_being_built, lmemory_to_allocate );

		pmsg_handle->dlen += PDATA_build_setting_to_send( FF_WEATHER_CONTROL, &lbitfield_of_files_that_have_changed,  &WEATHER_build_data_to_send, &ucp, pmsg_handle->dlen, &still_space_available, preason_data_is_being_built, lmemory_to_allocate );

		pmsg_handle->dlen += PDATA_build_setting_to_send( FF_IRRIGATION_SYSTEM, &lbitfield_of_files_that_have_changed, &SYSTEM_build_data_to_send, &ucp, pmsg_handle->dlen, &still_space_available, preason_data_is_being_built, lmemory_to_allocate );

		pmsg_handle->dlen += PDATA_build_setting_to_send( FF_LANDSCAPE_DETAILS, &lbitfield_of_files_that_have_changed, &STATION_GROUP_build_data_to_send, &ucp, pmsg_handle->dlen, &still_space_available, preason_data_is_being_built, lmemory_to_allocate );

		pmsg_handle->dlen += PDATA_build_setting_to_send( FF_MANUAL_PROGRAMS, &lbitfield_of_files_that_have_changed, &MANUAL_PROGRAMS_build_data_to_send, &ucp, pmsg_handle->dlen, &still_space_available, preason_data_is_being_built, lmemory_to_allocate );

		pmsg_handle->dlen += PDATA_build_setting_to_send( FF_STATION_INFO, &lbitfield_of_files_that_have_changed, &STATION_build_data_to_send, &ucp, pmsg_handle->dlen, &still_space_available, preason_data_is_being_built, lmemory_to_allocate );

		pmsg_handle->dlen += PDATA_build_setting_to_send( FF_POC, &lbitfield_of_files_that_have_changed, &POC_build_data_to_send, &ucp, pmsg_handle->dlen, &still_space_available, preason_data_is_being_built, lmemory_to_allocate );
	
		pmsg_handle->dlen += PDATA_build_setting_to_send( FF_LIGHTS, &lbitfield_of_files_that_have_changed, &LIGHTS_build_data_to_send, &ucp, pmsg_handle->dlen, &still_space_available, preason_data_is_being_built, lmemory_to_allocate );

		pmsg_handle->dlen += PDATA_build_setting_to_send( FF_MOISTURE_SENSORS, &lbitfield_of_files_that_have_changed, &MOISTURE_SENSOR_build_data_to_send, &ucp, pmsg_handle->dlen, &still_space_available, preason_data_is_being_built, lmemory_to_allocate );
		
		pmsg_handle->dlen += PDATA_build_setting_to_send( FF_WALK_THRU, &lbitfield_of_files_that_have_changed, &WALK_THRU_build_data_to_send, &ucp, pmsg_handle->dlen, &still_space_available, preason_data_is_being_built, lmemory_to_allocate );
		
		// ----------

		PDATA_copy_file_bitfield_into_pucp( &llocation_of_bitfield, &lbitfield_of_files_that_have_changed, sizeof(lbitfield_of_files_that_have_changed) );
	
		// ----------
	
		// 5/6/2014 ajv : If we've reached this point and the memory used so far only consists of
		// the overhead, it means we've entered this function with no changes. Therefore, roll back
		// the ucp and free the memory that was allocated since there's nothing to send.
		if( pmsg_handle->dlen == lmessage_overhead )
		{
			if( preason_data_is_being_built == CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER )
			{
				// 5/8/2015 rmd : A sanity check. If there was no data to send the flag should not be set.
				// Test for that. Changes and the flag should remain in sync. I think.
				if( weather_preserves.pending_changes_to_send_to_comm_server )
				{
					Alert_Message( "Pending changes flag unexpdly set" );
	
					// 5/8/2015 rmd : Clear it I suppose.
					weather_preserves.pending_changes_to_send_to_comm_server = (false);
				}
			}

			// 4/23/2015 rmd : Free the allocated memory block.
			mem_free( pmsg_handle->dptr );
	
			// ----------
			
			// 4/23/2015 rmd : Clean up the handle!
			pmsg_handle->dlen = 0;
		
			pmsg_handle->dptr = NULL;
			
			// ----------
			
			// 4/23/2015 rmd : And indicate nothing to send.
			results = PDATA_BUILD_RESULTS_no_data;
		}
		else
		{
			results = PDATA_BUILD_RESULTS_built_a_message;

			// 5/1/2015 ajv : If we're building the message for the CommServer, clear the
			// pending_changes flag. In the event of a failed communication or a power fail BEFORE the
			// message is sent, this flag will be reset when the PDATA_update_pending_change_bits
			// function is called (as that function finds the ACK bits set - which it should).
			if( preason_data_is_being_built == CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER )
			{
				weather_preserves.pending_changes_to_send_to_comm_server = (false);
			}
		}

		xSemaphoreGiveRecursive( pending_changes_recursive_MUTEX );
	}

	// ----------
	
	return( results );
}

/* ---------------------------------------------------------- */
/**
 * @task_execution Executed within the context of the CONTROLLER INITIATED task when it
 * first starts and when the waiting response flags are cleared after a communication. Also 
 * executed within the context of the COMM_MNGR task when an ACK or NAK is rcvd in response 
 * to sending the pdata to the commserver.
 */
extern void PDATA_update_pending_change_bits( const BOOL_32 pcomm_error_occurred )
{
	xSemaphoreTakeRecursive( pending_changes_recursive_MUTEX, portMAX_DELAY );

	nm_NETWORK_CONFIG_update_pending_change_bits( pcomm_error_occurred );

	nm_WEATHER_update_pending_change_bits( pcomm_error_occurred );

	nm_SYSTEM_update_pending_change_bits( pcomm_error_occurred );

	nm_STATION_GROUP_update_pending_change_bits( pcomm_error_occurred );
		
	nm_MANUAL_PROGRAMS_update_pending_change_bits( pcomm_error_occurred );
		
	nm_STATION_update_pending_change_bits( pcomm_error_occurred );

	nm_POC_update_pending_change_bits( pcomm_error_occurred );

	nm_LIGHTS_update_pending_change_bits( pcomm_error_occurred );

	nm_MOISTURE_SENSOR_update_pending_change_bits( pcomm_error_occurred );
	
	nm_WALK_THRU_update_pending_change_bits( pcomm_error_occurred );

	xSemaphoreGiveRecursive( pending_changes_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

