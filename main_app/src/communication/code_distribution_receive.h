/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_CODE_DISTRIBUTION_RECEIVE_H
#define _INC_CODE_DISTRIBUTION_RECEIVE_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"comm_mngr.h"

#include	"gpio_setup.h"

#include	"lpc_types.h"

#include	"code_distribution_task.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


extern void code_receipt_error_timer_callback( xTimerHandle pxTimer );


extern void process_incoming_packet( const CODE_DISTRIBUTION_TASK_QUEUE_STRUCT* const pcdtqs_ptr );


extern void CODE_DISTRIBUTION_make_a_copy_and_queue_incoming_packet( const UNS_32 pfrom_which_port, const DATA_HANDLE pdh, const UNS_32 pclass );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

