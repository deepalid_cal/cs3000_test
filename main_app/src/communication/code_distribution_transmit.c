/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	<stdlib.h>

#include	"code_distribution_transmit.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"cent_comm.h"

#include	"configuration_controller.h"

#include	"configuration_network.h"

#include	"cs_mem.h"

#include	"dialog.h"

#include	"flash_storage.h"

#include	"packet_definitions.h"

#include	"wdt_and_powerfail.h"

#include	"d_process.h"

#include	"packet_router.h"

#include	"controller_initiated.h"

#include	"serport_drvr.h"

#include	"e_comm_test.h"

#include	"flowsense.h"

#include	"tpmicro_comm.h"

#include	"rcvd_data.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/3/2014 rmd : If we distribute a tpmicro code image to slave(s), once received the slave
// will reboot and proceed with an ISP programming. Making that unit unavailable for some 30
// seconds. Once programmed the tpmicro will be up and running. And the unit available to
// process an incoming SCAN. By waiting here we delay our own reboot with the desired effect
// of delaying when we start the scan. Wait 45 seconds.
//
// 5/7/2015 rmd : We have still seen problems after code updates - both main app and tpmicro
// code. After the reboot the scan misses the controllers down the chain indicating they
// aren't ready. Or something isn't ready. So we are going to try lengthening this out to 60
// seconds from the 45 seconds it is now.
#define CODE_DISTRIBUTION_FLOWSENSE_TO_WAIT_BEFORE_MASTER_REBOOTS_MS	(60000)

// 9/19/2017 rmd : For the HUB, logically we should wait longer than a flowsense master, we
// have the potentially slow Freewave LR radios set up to use a repeater, and then we have a
// 15 second 'connect' sequence for the radios behind the hub. So add 30 seconds to the
// regular flowsense master delay.
#define CODE_DISTRIBUTION_HUB_TO_WAIT_FOR_QUERY_START_MS				(90000)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void code_distribution_packet_rate_timer_callback( xTimerHandle pxTimer )
{
	// 3/7/2014 ajv : This is executed within the context of the high priority
	// timer task.

	CODE_DISTRIBUTION_post_event( CODE_DISTRIBUTION_EVENT_send_next_packet );
}

/* ---------------------------------------------------------- */
static void set_transmit_bitfield_bits_for_this_many_packets( UNS_32 ptotal_packets )
{
	// 7/28/2017 rmd : Goal is to set the bits in the hub bitfield for ALL the packets that need
	// to be transmitted.

	// ----------
	
	UNS_32	whole_bytes;
	
	UNS_32	partial_bytes;

	UNS_32	bits_in_final_byte;
	
	UNS_32	iii;
	
	UNS_8	__partials[ ] = { 0x00, 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f };

	// ----------
	
	memset( cdcs.hub_packets_bitfield, 0x00, sizeof(cdcs.hub_packets_bitfield) );
	
	// ----------
	
	whole_bytes = (ptotal_packets / 8);
	
	// ----------
	
	partial_bytes = 1;

	if( (ptotal_packets % 8) == 0 )
	{
		partial_bytes = 0;
	}

	bits_in_final_byte = (ptotal_packets % 8);

	// ----------
	
	for( iii=0; iii<whole_bytes; iii++ )
	{
		cdcs.hub_packets_bitfield[ iii ] = 0xff;
	}
	
	if( partial_bytes )
	{
		cdcs.hub_packets_bitfield[ whole_bytes ] = __partials[ bits_in_final_byte ];
	}
}

/* ---------------------------------------------------------- */

// 8/22/2017 rmd : Used within this file to record the radio temperature queried from the
// radio during code distribution.
UNS_32	m7_current_temp;
	
/* ---------------------------------------------------------- */
extern void build_and_send_code_transmission_init_packet( CODE_DISTRIBUTION_TASK_QUEUE_STRUCT const *const pcdtqs_ptr )
{
	DATA_HANDLE	ldh;

	UNS_8		*ucp;
	
	FROM_COMMSERVER_PACKET_TOP_HEADER   fcph;
	
	MSG_INITIALIZATION_STRUCT   linit_message_header;
	
	CRC_BASE_TYPE   lcrc;
	
	UNS_32			packet_rate_ms;
	
	UNS_32			main_app_binary_date;

	UNS_32			main_app_binary_time;

	char			str_64[ 64 ];
	
	BOOL_32			proceed;
	
	// ----------
	
	proceed = (true);
	
	// 7/26/2017 rmd : When the QUEUE_EVENT pointer is a NULL, this means the distribution mode
	// must be a HUB based transmission, and the state must be that we are RESTARTING the
	// transmission, hopefully filling in a few missing blocks according to the bit field.
	if(	pcdtqs_ptr == NULL )
	{
		if( (cdcs.mode != CODE_DISTRIBUTION_MODE_hub_transmitting_main) && (cdcs.mode != CODE_DISTRIBUTION_MODE_hub_transmitting_tpmicro) )
		{
			proceed = (false);
		}
		
		if( cdcs.transmit_state != CODE_TRANSMITTING_STATE_restarting_transmission_for_hub )
		{
			proceed = (false);
		}

		// ----------
		
		// 7/27/2017 rmd : Check that the CRC we calculated during creation of the prior init packet
		// MATCHES the CRC as we calculate it now. This is an attempt at verifying the memory is
		// still intact.
		lcrc = CRC_calculate_32bit_big_endian( cdcs.transmit_data_start_ptr, cdcs.transmit_length );
		
		if( lcrc != cdcs.transmit_expected_crc )
		{
			proceed = (false);
		}
		
		// ----------
		
		// 9/18/2017 rmd : If it's still okay to proceed, check that we haven't been at it too long.
		// We can't just keep retrying and retrying. At least I think there should be some
		// reasonable end to it. AJ and I came up with the criteria that if we had already sent more
		// than TWICE the total packet count, well then abandon the distribution effort. Should we
		// clear the weather flag too? I think so otherwise we would reboot and just start again.
		// Let that flag get set through the normal interaction with the hub, if it needs to be set
		// it will be.
		if( proceed )
		{
			if( cdcs.transmit_packets_sent_so_far > (2 * cdcs.transmit_total_packets_for_the_binary) )
			{
				proceed = (false);
				
				Alert_Message_va( "Code Distribution exhausted: %u packets sent", cdcs.transmit_packets_sent_so_far );
	
				if( cdcs.mode == CODE_DISTRIBUTION_MODE_hub_transmitting_main )
				{
					weather_preserves.hub_needs_to_distribute_main_binary = (false);
				}
				else
				{
					weather_preserves.hub_needs_to_distribute_tpmicro_binary = (false);
				}
			}
		}
	}
	else
	{
		// 4/1/2014 rmd : The FILE READ COMPLETE can FAIL and still issue the event that triggers
		// execution of this function. The only failure I know of is couldn't get memory. Not
		// supposed to happen, but guard against here. If this does happen reboot, and try the
		// sequence again.
		if(	pcdtqs_ptr->dh.dptr == NULL )
		{
			proceed = (false);
		}
		
		if(	(cdcs.mode != CODE_DISTRIBUTION_MODE_flowsense_transmitting_main) &&
			(cdcs.mode != CODE_DISTRIBUTION_MODE_flowsense_transmitting_tpmicro) &&
			(cdcs.mode != CODE_DISTRIBUTION_MODE_hub_transmitting_main) &&
			(cdcs.mode != CODE_DISTRIBUTION_MODE_hub_transmitting_tpmicro) )
		{
			proceed = (false);
		}
	
		if( cdcs.transmit_state != CODE_TRANSMITTING_STATE_beginning_a_fresh_transmission )
		{
			proceed = (false);
		}
	}
	
	// ----------
	
	if( !proceed )
	{
		// 4/1/2014 rmd : NOTE - execution does not return from the following call.
		SYSTEM_application_requested_restart( SYSTEM_SHUTDOWN_EVENT_code_distribution_error );
	}
	else
	{
		if( cdcs.transmit_state == CODE_TRANSMITTING_STATE_restarting_transmission_for_hub )
		{

			if( cdcs.mode == CODE_DISTRIBUTION_MODE_hub_transmitting_main )
			{
				snprintf( str_64, sizeof(str_64), "MAIN CODE: re-starting hub distribution" );
			}
			else
			if( cdcs.mode == CODE_DISTRIBUTION_MODE_hub_transmitting_tpmicro )
			{
				snprintf( str_64, sizeof(str_64), "TPMICRO CODE: re-starting hub distribution" );
			}
		}
		else
		{
			// 3/31/2014 rmd : Establish all the control parameters, that this master in charge of the
			// distribution needs, to do its job. Use the data_packet_mid here.
	
			// 6/14/2017 rmd : The wierd thing about the code image memory, is that we NEVER free it. We
			// rely on a reboot to do that, at the conclusion of the distribution. That is a good thing
			// when it comes to the hub code distribution, that counts on the image staying intact, in
			// the memory.
			
			cdcs.transmit_data_start_ptr = pcdtqs_ptr->dh.dptr;
		
			cdcs.transmit_data_end_ptr = pcdtqs_ptr->dh.dptr + pcdtqs_ptr->dh.dlen;
		
			cdcs.transmit_length = pcdtqs_ptr->dh.dlen;
			
			// ----------

			cdcs.transmit_total_packets_for_the_binary = (pcdtqs_ptr->dh.dlen / DESIRED_PACKET_PAYLOAD);
		
			if( (pcdtqs_ptr->dh.dlen % DESIRED_PACKET_PAYLOAD) != 0 )
			{
				cdcs.transmit_total_packets_for_the_binary += 1;
			}
			
			set_transmit_bitfield_bits_for_this_many_packets( cdcs.transmit_total_packets_for_the_binary );
			
			// 9/18/2017 rmd : Initialized here, once. And is an on-going count, not restarted when the
			// hub performs retries. This is so we can eventually decide enough already, if the retires
			// go on and on.
			cdcs.transmit_packets_sent_so_far = 0;
	
			// ----------
			
			// 7/27/2017 rmd : Learn the CRC for use in the message below. Also this value is compared
			// against when performing a HUB re-transmission pass.
			cdcs.transmit_expected_crc = CRC_calculate_32bit_big_endian( cdcs.transmit_data_start_ptr, cdcs.transmit_length );
			
			// ----------
			
			// 8/22/2017 rmd : Initialize to a reasonable Celcius value when starting a fresh
			// distribution, not on a re-start. We have not yet read the temperature from the radio and
			// need to start somewhere.
			m7_current_temp = 30;
			
			// ----------
			
			if( cdcs.mode == CODE_DISTRIBUTION_MODE_flowsense_transmitting_main )
			{
				cdcs.transmit_packet_class = MSG_CLASS_CS3000__FROM_CONTROLLER__CODE_DISTRIBUTION;
	
				cdcs.transmit_init_mid = MID_CODE_DISTRIBUTION_main_init_packet;
	
				cdcs.transmit_data_mid = MID_CODE_DISTRIBUTION_main_data_packet;
	
				snprintf( str_64, sizeof(str_64), "MAIN CODE: starting flowsense distribution" );
	
				CODE_DOWNLOAD_draw_dialog( (true) );
			}
			else
			if( cdcs.mode == CODE_DISTRIBUTION_MODE_flowsense_transmitting_tpmicro )
			{
				cdcs.transmit_packet_class = MSG_CLASS_CS3000__FROM_CONTROLLER__CODE_DISTRIBUTION;
	
				cdcs.transmit_init_mid = MID_CODE_DISTRIBUTION_tpmicro_init_packet;
	
				cdcs.transmit_data_mid = MID_CODE_DISTRIBUTION_tpmicro_data_packet;
	
				snprintf( str_64, sizeof(str_64), "TPMICRO CODE: starting flowsense distribution" );
	
				CODE_DOWNLOAD_draw_dialog( (true) );
			}
			else
			if( cdcs.mode == CODE_DISTRIBUTION_MODE_hub_transmitting_main )
			{
				cdcs.transmit_packet_class = MSG_CLASS_CS3000__FROM_HUB__CODE_DISTRIBUTION_BROADCAST;
	
				cdcs.transmit_init_mid = MID_CODE_DISTRIBUTION_main_init_packet;
	
				cdcs.transmit_data_mid = MID_CODE_DISTRIBUTION_main_data_packet;
	
				snprintf( str_64, sizeof(str_64), "MAIN CODE: starting hub distribution" );
			}
			else
			if( cdcs.mode == CODE_DISTRIBUTION_MODE_hub_transmitting_tpmicro )
			{
				cdcs.transmit_packet_class = MSG_CLASS_CS3000__FROM_HUB__CODE_DISTRIBUTION_BROADCAST;
	
				cdcs.transmit_init_mid = MID_CODE_DISTRIBUTION_tpmicro_init_packet;
	
				cdcs.transmit_data_mid = MID_CODE_DISTRIBUTION_tpmicro_data_packet;
	
				snprintf( str_64, sizeof(str_64), "TPMICRO CODE: starting hub distribution" );
			}
		}
		
		// ----------
		
		// 7/27/2017 rmd : The string was built above in one mode or the other.
		Alert_Message_va( "%s %d bytes (%d packets)", str_64, cdcs.transmit_length, cdcs.transmit_total_packets_for_the_binary );

		// ----------
		
		cdcs.transmit_state = CODE_TRANSMITTING_STATE_building_data_packets;

		cdcs.transmit_current_packet_to_send = 1;

		// ----------
		
		// 7/18/2017 rmd : Besides the two structures we also include the date and time of the
		// binary we are distributing. This is needed for the hub, during a re-transmission, for the
		// receiving controllers, as solid verification that the binary we are continueing to
		// receive, is indeed the same one that we have been receiving.
		ldh.dlen = sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) + sizeof(MSG_INITIALIZATION_STRUCT) + (2*sizeof(UNS_32)) + sizeof(CRC_BASE_TYPE);
	
		ldh.dptr = mem_malloc( ldh.dlen );
	
		ucp = ldh.dptr;
	
		// ---------------------
	
		// 2/27/2014 ajv : Fill the header
		fcph.PID.__routing_class = MSG_CLASS_2000e_CS3000_FLAG;
	
		fcph.cs3000_msg_class = cdcs.transmit_packet_class;
	
		// 5/31/2017 rmd : These two are not used for these broadcast packets.
		fcph.to_serial_number = 0;
		fcph.to_network_id = 0;

		fcph.mid = cdcs.transmit_init_mid;
	
		memcpy( ucp, &fcph, sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) );

		ucp += sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER);
	
		// ----------
		
		// 2/27/2014 ajv : Fill the init structure
		linit_message_header.expected_crc = cdcs.transmit_expected_crc;
		
		linit_message_header.expected_size = cdcs.transmit_length;
		
		linit_message_header.expected_packets = cdcs.transmit_total_packets_for_the_binary;
		
		linit_message_header.mid = cdcs.transmit_init_mid;
	
		memcpy( ucp, &linit_message_header, sizeof(MSG_INITIALIZATION_STRUCT) );

		ucp += sizeof(MSG_INITIALIZATION_STRUCT);
	
		// ----------
		
		// 7/18/2017 rmd : Because the HUB distribution is an on-going piecemeal process (depending
		// on reception issues) we need to make sure the HUB did not receive yet another update from
		// the hub and is now transmitting it. I guess we could use a change in the expected CRC as
		// such a flag, but we'll also include the binary date and time values. We include this
		// information for any init packets: from a flowsense master or from a hub.
		if( cdcs.transmit_init_mid == MID_CODE_DISTRIBUTION_main_init_packet )
		{
			main_app_binary_date = convert_code_image_date_to_version_number( (UNS_8*)(EMC_DYCS0_BASE), CS3000_APP_FILENAME );
	
			main_app_binary_time = convert_code_image_time_to_edit_count( (UNS_8*)(EMC_DYCS0_BASE), CS3000_APP_FILENAME );
			
			memcpy( ucp, &main_app_binary_date, sizeof(main_app_binary_date) );
			ucp += sizeof(main_app_binary_date);
			
			memcpy( ucp, &main_app_binary_time, sizeof(main_app_binary_time) );
			ucp += sizeof(main_app_binary_time);
		}
		else
		if( cdcs.transmit_init_mid == MID_CODE_DISTRIBUTION_tpmicro_init_packet )
		{
			memcpy( ucp, &tpmicro_comm.file_system_code_date, sizeof(tpmicro_comm.file_system_code_date) );
			ucp += sizeof( tpmicro_comm.file_system_code_date );
			
			memcpy( ucp, &tpmicro_comm.file_system_code_time, sizeof(tpmicro_comm.file_system_code_time) );
			ucp += sizeof( tpmicro_comm.file_system_code_time );
		}

		// ----------
		
		// 3/4/2014 ajv : Calculate the CRC
		lcrc = CRC_calculate_32bit_big_endian( ldh.dptr, (ldh.dlen - sizeof(CRC_BASE_TYPE)) );
	
		// 3/4/2014 ajv : ucp already points to where the crc is stored
		memcpy( ucp, &lcrc, sizeof(CRC_BASE_TYPE) );

		ucp += sizeof(CRC_BASE_TYPE);
	
		// ----------
		
		if( cdcs.transmit_packet_class == MSG_CLASS_CS3000__FROM_CONTROLLER__CODE_DISTRIBUTION )
		{
			// 3/27/2014 rmd : We do not attempt to broadcast this packet to ourselves too. We already
			// have the code. Packet is only sent to the various ports in an effort to get it to all
			// controllers on the network.
			PACKET_ROUTER_broadcast_SCAN_TOKEN_CODE_packet_to_other_controllers_on_our_chain( ldh, NULL );
			
			packet_rate_ms = cdcs.transmit_chain_packet_rate_ms;
		}
		else
		{
			// 5/31/2017 rmd : For the HUB send to Port B. We already checked upon function entry that
			// this controller is a configured HUB.
			attempt_to_copy_packet_and_route_out_another_port( UPORT_B, ldh, NULL );
			
			// 11/7/2017 rmd : And send it a second time. WHY? Because for a hub doing a broadcast
			// distribution, if a controller behind a hub misses the first packet ALL the subsequent
			// data packets are then dropped by the receiving controller (it views them as an
			// unsolicited set of packets). And dropping all those packets is pretty ugly when it comes
			// to the main code distribution, it has to repeat the whole thing! So try to ensure the
			// init packet makes it through.
			//
			// By the way, the code fully supports the double init packets, it is simply viewed as a
			// retransmission, where during the first transmission all we got was the init and no data
			// packets.
			attempt_to_copy_packet_and_route_out_another_port( UPORT_B, ldh, NULL );
			
			// ----------
			
			packet_rate_ms = cdcs.transmit_hub_packet_rate_ms;
		}
		
		// ----------
		
		// 3/6/2014 ajv : Start the packet rate timer. When expires triggers the first packet
		// containing the actual code image to be sent.
		xTimerChangePeriod( cdcs.transmit_packet_rate_timer, MS_to_TICKS( packet_rate_ms ), portMAX_DELAY );

		// ----------
	
		// 3/27/2014 rmd : We're done with it.
		mem_free( ldh.dptr );
	}
}

/* ---------------------------------------------------------- */
static UNS_32 return_sn_for_this_item_in_the_hub_list( UNS_32 pitem_number_count )
{
	// 7/11/2017 rmd : pitem_number_count is the item index of the 3000 we are interested in.
	// Remember the list has a divider, so the number of possible 3000 choices is at most the
	// number on the queue minus 1. If there are 2000 on the list those also of course don't
	// count.
	
	// ----------
	
	UNS_32	how_many_on_the_list;
	
	UNS_32	iii;
	
	UNS_32	seen_3000s;
	
	UNS_32	sn_3000;
	
	BOOL_32	now_2000;
	
	UNS_32	rv;
	
	// 7/11/2017 rmd : ZERO indicates to users of this function there is not a 3000 that matches
	// this item number. In other works the list is too short or the caller has iterated though
	// the list.
	rv = 0;
	
	// ----------
	
	// 4/30/2017 rmd : The hub list queue is protected by a MUTEX. Always need to take before
	// working with this queue.
	xSemaphoreTake( router_hub_list_MUTEX, portMAX_DELAY );

	// ----------
	
	// 4/30/2017 rmd : The list always starts with 3000's first, till the divider is hit.
	now_2000 = (false);

	seen_3000s = 0;
	
	how_many_on_the_list = uxQueueMessagesWaiting( router_hub_list_queue );
	
	for( iii=0; iii<how_many_on_the_list; iii++ )
	{
		// 4/30/2017 rmd : If we go through the entire queue pulling from the front and writing to
		// the back the queue will remain in its original order with the same number of items. Keep
		// in mind, we ALWAYS have to run through the ENTIRE list, even if we find what we are
		// looking for part way through, to keep the list intact.
		
		// 6/19/2017 rmd : Do not wait for a queue item, there is no task adding to the queue except
		// for when a message comes from the commserver. Our goal is to pull the items that are on
		// the queue at this time. Not wait for future items that are added to the list.
		if( xQueueReceive( router_hub_list_queue, &sn_3000, 0 ) )
		{
			// 5/1/2017 rmd : This is an optimization, time wise, for this function. Once we have
			// crossed into 2000 teritory or found the item, stop testing.
			if( !now_2000 )
			{
				// 4/30/2017 rmd : Did we hit the divider between 3000 and 2000?
				if( sn_3000 == 0xffffffff )
				{
					now_2000 = (true);
				}
	
				if( !now_2000 )
				{
					seen_3000s += 1;
					
					if( seen_3000s == pitem_number_count )
					{
						rv = sn_3000;
					}
				}
			}
			
			// ----------
								
			// 7/11/2017 rmd : A sanity check, since we are counting on this functions return value of 0
			// to mean the end of the list, and also we should never see a ZERO serial number of 2000
			// comm addr in the list. No entry in the hub list should be a zero!
			if( sn_3000 == 0 )
			{
				Alert_Message( "Hub List contains a 0 entry!" );
			}
			
			// ----------
			
			// 4/30/2017 rmd : Always write them back onto the queue. Do not wait if the queue is full,
			// nothing is removing items.
			xQueueSendToBack( router_hub_list_queue, &sn_3000, 0 );
		}
		else
		{
			// 4/30/2017 rmd : This should NEVER happen .... i think.
			Alert_Message( "HUB: unexp end of list" );
		}
	}
	
	// ----------
	
	xSemaphoreGive( router_hub_list_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 build_and_send_a_hub_distribution_query_for_this_3000_sn( UNS_32 pserial_number )
{
	BOOL_32	rv;
	
	DATA_HANDLE	ldh;
	
	UNS_8	*ucp;
	
	FROM_COMMSERVER_PACKET_TOP_HEADER	fcpth;
	
	UNS_32	temp_mode;
	
	UNS_32	main_app_date;

	UNS_32	main_app_time;
	
	CRC_BASE_TYPE   lcrc;

	// ----------

	rv = (true);

	// ----------
	
	if( pserial_number != 0 )
	{
		// 7/18/2017 rmd : The header, the from serial number since that isn't in the header, the
		// code distribution mode to identify which binary, two UNS_32 for the binary date and time
		// numbers for the code we are distributing, and the CRC.
		ldh.dlen = sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) + sizeof(UNS_32) + sizeof(UNS_32) + (2 * sizeof(UNS_32)) + sizeof(CRC_BASE_TYPE);
	
		ldh.dptr = mem_malloc( ldh.dlen );
	
		ucp = ldh.dptr;
	
		// ----------
		
		// 7/18/2017 rmd : Fill the header.
		memset( &fcpth, 0x00, sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) );
		
		fcpth.PID.__routing_class = MSG_CLASS_2000e_CS3000_FLAG;
	
		fcpth.cs3000_msg_class = MSG_CLASS_CS3000__FROM_HUB__CODE_DISTRIBUTION_ADDRESSABLE;
		
		fcpth.to_serial_number = pserial_number;

		// 7/18/2017 rmd : The network ID is not used to route packets.
		fcpth.to_network_id = 0;
	
		fcpth.mid = MID_CODE_DISTRIBUTION_hub_query_packet;
		
		memcpy( ucp, &fcpth, sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) );
	
		ucp += sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER);

		// ----------
		
		// 7/18/2017 rmd : Include the from serial number, since the fcpth strucutre doesn't include
		// it, and I want the recepient to build an addressable packet as the ack, addressable to
		// me.
		memcpy( ucp, &config_c.serial_number, sizeof(config_c.serial_number) );
				
		ucp += sizeof(config_c.serial_number);

		// ----------
		
		// 7/18/2017 rmd : Include the mode so the receiving controller knows which binary we are
		// interested in having him check that he has or is in the middle of receiving. And don't
		// put OUR mode in the packet, put the mode we would expect the receiving controller to be
		// in.
		if( cdcs.mode == CODE_DISTRIBUTION_MODE_hub_transmitting_main )
		{
			temp_mode = CODE_DISTRIBUTION_MODE_receiving_from_hub_main;
		}
		else
		{
			temp_mode = CODE_DISTRIBUTION_MODE_receiving_from_hub_tpmicro;
		}
		
		memcpy( ucp, &temp_mode, sizeof(temp_mode) );
				
		ucp += sizeof(temp_mode);
		
		// ----------
		
		if( cdcs.mode == CODE_DISTRIBUTION_MODE_hub_transmitting_main )
		{
			main_app_date = convert_code_image_date_to_version_number( (UNS_8*)(EMC_DYCS0_BASE), CS3000_APP_FILENAME );
	
			main_app_time = convert_code_image_time_to_edit_count( (UNS_8*)(EMC_DYCS0_BASE), CS3000_APP_FILENAME );
			
			memcpy( ucp, &main_app_date, sizeof(main_app_date) );
			ucp += sizeof(main_app_date);
			
			memcpy( ucp, &main_app_time, sizeof(main_app_time) );
			ucp += sizeof(main_app_time);
		}
		else
		if( cdcs.mode == CODE_DISTRIBUTION_MODE_hub_transmitting_tpmicro )
		{
			memcpy( ucp, &tpmicro_comm.file_system_code_date, sizeof(tpmicro_comm.file_system_code_date) );
			ucp += sizeof( tpmicro_comm.file_system_code_date );
			
			memcpy( ucp, &tpmicro_comm.file_system_code_time, sizeof(tpmicro_comm.file_system_code_time) );
			ucp += sizeof( tpmicro_comm.file_system_code_time );
		}
		else
		{
			// 7/18/2017 rmd : Supposed to be one of those modes.
			Alert_Message( "Code Distribution Error: unexpected mode" );
			
			rv = (false);
		}
		
		// ----------
		
		// 7/18/2017 rmd : Calculate the CRC.
		lcrc = CRC_calculate_32bit_big_endian( ldh.dptr, (ldh.dlen - sizeof(CRC_BASE_TYPE)) );
	
		// 7/18/2017 rmd : And add the crc to the packet.
		memcpy( ucp, &lcrc, sizeof(CRC_BASE_TYPE) );
		ucp += sizeof(CRC_BASE_TYPE);
	
		// ----------
	
		// 7/18/2017 rmd : This function is targeted at HUB code transmission, and therefore as a
		// hub, the packet is pushed only out UPORT_B.
		attempt_to_copy_packet_and_route_out_another_port( UPORT_B, ldh, NULL );

		// ----------
		
		// 3/27/2014 rmd : We're done with it.
		mem_free( ldh.dptr );
	}
	else
	{
		// 7/18/2017 rmd : Indicate end of 3000's in the hub list was reached.
		rv = (false);
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
static void transmission_completed_clear_hub_distribution_flag_and_perform_a_restart( void )
{
	// 7/20/2017 rmd : With regards to a HUB, the transmission has been declared received by all
	// controllers in the hub list, so clear the flag that caused us to distribute.
	if( cdcs.mode == CODE_DISTRIBUTION_MODE_hub_transmitting_main )
	{
		weather_preserves.hub_needs_to_distribute_main_binary = (false);
	}
	else
	if( cdcs.mode == CODE_DISTRIBUTION_MODE_hub_transmitting_tpmicro )
	{
		weather_preserves.hub_needs_to_distribute_tpmicro_binary = (false);
	}
	
	// ----------
	
	if( cdcs.transmit_data_mid == MID_CODE_DISTRIBUTION_main_data_packet )
	{
		// 4/1/2014 rmd : NOTE - execution does not return from the following call.
		SYSTEM_application_requested_restart( SYSTEM_SHUTDOWN_EVENT_main_code_distribution_completed );
	}
	else
	if( cdcs.transmit_data_mid == MID_CODE_DISTRIBUTION_tpmicro_data_packet )
	{
		// 4/1/2014 rmd : NOTE - execution does not return from the following call.
		SYSTEM_application_requested_restart( SYSTEM_SHUTDOWN_EVENT_tpmicro_code_distribution_completed );
	}
}
	
/* ---------------------------------------------------------- */
static BOOL_32 this_transmit_bit_is_set( UNS_32 packet_number )
{
	// 7/31/2017 rmd : Is the bit set indicating we are to transmit this packet. We count on
	// packet_number to be >= 1;
	
	// ----------
	
	BOOL_32	rv;
	
	UNS_8	bit_masks[ ] = {  0x80, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40 };
	
	UNS_8	lmask;
	
	rv = (false);

	if( packet_number >= 1 )
	{
		lmask = bit_masks[ (packet_number % 8) ];
		
		if( (cdcs.hub_packets_bitfield[ (packet_number-1) / 8 ] & lmask) == lmask )
		{
			rv = (true);
		}
	}

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */

// 8/21/2017 rmd : Yeah it spits out two CR-LF pairs at the end of its response to the +++!
const char m7_ok_response_str[] = "\r\nOK\r\n\r\n";

const char m7_plus_plus_plus_str[] = "+++";

// 8/22/2017 rmd : Make sure your command strings end in only a CR. If you end them in a
// CR-LF our sequence falls apart. I think the left-over LF messes up the following command.
const char m7_read_temp_str[] = "atte\r";

// 8/21/2017 rmd : This is the string at the end of what comes back in response to the
// temperature query. The actual temperature is the 2 characters that preceeds this string.
const char m7_temp_response_str[] = "\r\nOK\r\n";

// 8/23/2017 rmd : Plain "exit" works. But does not respond with an "OK<CR><LF>"
//const char m7_exit_str[] = "exit";
// 8/23/2017 rmd : Plain "exit<CR>" works. And does respond with an "OK<CR><LF>"
//const char m7_exit_str[] = "exit\r";
// 8/23/2017 rmd : And "atcn<CR>" works. And does respond with an "OK<CR><LF>". I've settled
// on the atcn<CR> as it is more traditional in that it is an at command.
const char m7_exit_str[] = "atcn\r";

/* ---------------------------------------------------------- */
extern void perform_next_code_transmission_state_activity( CODE_DISTRIBUTION_TASK_QUEUE_STRUCT const *const pcdtqs_ptr )
{
	// 3/28/2014 rmd : This function is called within the context of the COMM_MNGR task. And the
	// controller initiated task too, when the hub is distributing code.
	
	// ----------

	DATA_HANDLE	ldh;

	UNS_8		*ucp;

	UNS_32		lamount_to_send;

	BOOL_32		last_packet_sent;

	BOOL_32		drivers_empty;
	
	FROM_COMMSERVER_PACKET_TOP_HEADER   fcph;

	MSG_DATA_PACKET_STRUCT	msg_packet_struct;
	
	CRC_BASE_TYPE   lcrc;
	
	UNS_32		packet_rate_ms;
	
	UNS_32		iii;
	
	BOOL_32		still_some;
	
	BOOL_32		one_to_transmit;
	
	char		char_array[4];
	
	UNS_32		reboot_or_query_wait_ms;
	
	// ----------
	
	// 6/20/2017 rmd : Default rate used while checking for drivers to empty and while waiting
	// to reboot.
	packet_rate_ms = 1000;
	
	// ----------
	
	// 3/28/2014 rmd : Are we still building packets or waiting for the serial drivers to empty
	// before restarting?
	switch( cdcs.transmit_state )
	{
		case CODE_TRANSMITTING_STATE_building_data_packets:

			one_to_transmit = (false);
			
			last_packet_sent = (false);
			
			if( cdcs.transmit_packet_class == MSG_CLASS_CS3000__FROM_HUB__CODE_DISTRIBUTION_BROADCAST )
			{
				// 7/28/2017 rmd : The next packet to send is determined by the next bit set in the hub bit
				// field. Look for a bit that is set.
				while( cdcs.transmit_current_packet_to_send <= cdcs.transmit_total_packets_for_the_binary )
				{
					if( this_transmit_bit_is_set( cdcs.transmit_current_packet_to_send ) )
					{
						one_to_transmit = (true);
	
						break;
					}
					else
					{
						cdcs.transmit_current_packet_to_send += 1;
					}
				}
				
				// 7/28/2017 rmd : REMEMBER during a hub retransmission we may not ACTUALLY send the last
				// packet. So we pass back through here one more time AFTER the last packet that NEEDED to
				// be sent was sent (during the last pass through).
				if( !one_to_transmit )
				{
					last_packet_sent = (true);
				}
			}
			else
			{
				if( cdcs.transmit_current_packet_to_send > cdcs.transmit_total_packets_for_the_binary )
				{
					last_packet_sent = (true);
				}
				else
				{
					one_to_transmit = (true);
				}
			}
			
			// ----------
			
			if( one_to_transmit )
			{
				// 8/7/2017 rmd : In all cases, hub or flowsense master, increment how many packets have
				// been transmitted.
				cdcs.transmit_packets_sent_so_far += 1;
				
				// ----------
				
				// 3/6/2014 ajv : Determine how much we have to send within this packet.
				if( cdcs.transmit_current_packet_to_send == cdcs.transmit_total_packets_for_the_binary )
				{
					if( (cdcs.transmit_length % DESIRED_PACKET_PAYLOAD) == 0 )
					{
						lamount_to_send = DESIRED_PACKET_PAYLOAD;
					}
					else
					{
						// 8/7/2017 rmd : Send the balance.
						lamount_to_send = (cdcs.transmit_length % DESIRED_PACKET_PAYLOAD);
					}
				}
				else
				{
					lamount_to_send = DESIRED_PACKET_PAYLOAD;
				}
			
				// ---------------------
			
				ldh.dlen = sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) + sizeof(MSG_DATA_PACKET_STRUCT) + lamount_to_send + sizeof(CRC_BASE_TYPE);
			
				ldh.dptr = mem_malloc( ldh.dlen );
			
				ucp = ldh.dptr;
			
				// ---------------------
			
				// 2/27/2014 ajv : Fill the header
			
				fcph.PID.__routing_class = MSG_CLASS_2000e_CS3000_FLAG;
		
				fcph.cs3000_msg_class = cdcs.transmit_packet_class;
		
				// 5/31/2017 rmd : These two are not used for these broadcast packets.
				fcph.to_serial_number = 0;
				fcph.to_network_id = 0;
		
				fcph.mid = cdcs.transmit_data_mid;
				
				memcpy( ucp, &fcph, sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) );
		
				ucp += sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER);
			
				// ---------------------
			
				// 3/6/2014 ajv : Fill the message packet structure
		
				msg_packet_struct.packet_number = cdcs.transmit_current_packet_to_send;
		
				msg_packet_struct.mid = cdcs.transmit_data_mid;
			
				memcpy( ucp, &msg_packet_struct, sizeof(MSG_DATA_PACKET_STRUCT) );
		
				ucp += sizeof(MSG_DATA_PACKET_STRUCT);
			
				// ----------

				// 2/27/2014 ajv : Fill the payload
				memcpy( ucp, cdcs.transmit_data_start_ptr + ((cdcs.transmit_current_packet_to_send - 1) * DESIRED_PACKET_PAYLOAD), lamount_to_send );

				ucp += lamount_to_send;
				
				// 12/20/2017 rmd : Alert used during development only, so normally commented out.
				//Alert_Message_va( "CODE: sending packet %u, size=%u", cdcs.transmit_current_packet_to_send, lamount_to_send );
				
				// ----------
				
				// 8/1/2017 rmd : Now that we've loaded the payload bump the packet counter (the count is
				// important for the non-hub transmission).
				cdcs.transmit_current_packet_to_send += 1;
			
				// ----------
				
				// 3/4/2014 ajv : Calculate the CRC
				lcrc = CRC_calculate_32bit_big_endian( ldh.dptr, (ldh.dlen - sizeof(CRC_BASE_TYPE)) );
			
				// 3/4/2014 ajv : ucp already points to where the crc is stored
				memcpy( ucp, &lcrc, sizeof(CRC_BASE_TYPE) );
				ucp += sizeof(CRC_BASE_TYPE);
			
				// ----------
			
				if( cdcs.transmit_packet_class == MSG_CLASS_CS3000__FROM_CONTROLLER__CODE_DISTRIBUTION )
				{
					// 3/27/2014 rmd : We do not attempt to broadcast this packet to ourselves too. We already
					// have the code. Packet is only sent to the various ports in an effort to get it to all
					// controllers on the network.
					PACKET_ROUTER_broadcast_SCAN_TOKEN_CODE_packet_to_other_controllers_on_our_chain( ldh, NULL );
		
					packet_rate_ms = cdcs.transmit_chain_packet_rate_ms;
				}
				else
				if( cdcs.transmit_packet_class == MSG_CLASS_CS3000__FROM_HUB__CODE_DISTRIBUTION_BROADCAST )
				{
					// 5/31/2017 rmd : For the HUB send to Port B. We already checked when we built the init
					// packet that this controller is a configured HUB.
					attempt_to_copy_packet_and_route_out_another_port( UPORT_B, ldh, NULL );
		
					// ----------
					
					packet_rate_ms = cdcs.transmit_hub_packet_rate_ms;
					
					// 8/29/2017 rmd : Through testing I found that with an ambient test chamber temperature of
					// 70 degrees C and a packet delay of 22 seconds we could stablize the radio temperature at
					// 75 degrees C. I like this. Additionally I determined we didn't have to take any action
					// until after the radio crossed 60 degrees C (or about 140F). So running the math you come
					// up with adding 1.15 seconds to the packet delay for each degree above 60C.
					if( m7_current_temp > 60 )
					{
						packet_rate_ms += ( (m7_current_temp - 60) * 1150 );
					}
					
					// 12/20/2017 rmd : Alert used during development only, so normally commented out.
					//Alert_Message_va( "packet rate = %u ms", packet_rate_ms );
					
					// ----------

					// 8/7/2017 rmd : Is it time to go read the temperature to possibly adjust the time between
					// packets? We ONLY do this for the RAVEON LR device, because with a 100% duty cycle,
					// depending on the ambient temperature, the radio may overheat.
					if( config_c.port_B_device_index == COMM_DEVICE_LR_RAVEON )
					{
						if( (cdcs.transmit_packets_sent_so_far % 10) == 0 )
						{
							// 8/7/2017 rmd : Switch to put the radio into command mode.
							cdcs.transmit_state = CODE_TRANSMITTING_STATE_sending_plus_plus_plus;

							// 8/7/2017 rmd : And even though the hub packet rate is designed to NOT build up packets in
							// the serial driver, add an extra packet delay as good measure to make sure the driver is
							// empty, SO WE GET THE REQUIRED 1/2 second of no characters sent to the radio before we
							// send the +++. There is no guarantee this works, but using an oscilloscope, I have
							// measured the transmit timing, and the packet delay define for the LR_RAVEON is crafted to
							// accomodate the FULL PACKET transmission time. So this additional buffer gives us way more
							// than needed.
							packet_rate_ms += cdcs.transmit_hub_packet_rate_ms;
						}
					}
				}
				else
				{
					Alert_Message_va( "CODE: unexpd class %u", cdcs.transmit_packet_class );
				}
				
				// ----------
				
				// 3/27/2014 rmd : We're done with it.
				mem_free( ldh.dptr );
			}
			else
			if( last_packet_sent )
			{
				if( cdcs.transmit_packet_class == MSG_CLASS_CS3000__FROM_HUB__CODE_DISTRIBUTION_BROADCAST )
				{
					if( cdcs.transmit_data_mid == MID_CODE_DISTRIBUTION_main_data_packet )
					{
						Alert_Message( "MAIN CODE: all packets sent from hub" );
					}
					else
					if( cdcs.transmit_data_mid == MID_CODE_DISTRIBUTION_tpmicro_data_packet )
					{
						Alert_Message( "TPMICRO CODE :  all packets sent from hub" );
					}
					else
					{
						Alert_Message( "UPDATE: mid error" );
					}
				}
				else
				{
					if( cdcs.transmit_data_mid == MID_CODE_DISTRIBUTION_main_data_packet )
					{
						Alert_Message( "MAIN CODE: completed distribution" );
					}
					else
					if( cdcs.transmit_data_mid == MID_CODE_DISTRIBUTION_tpmicro_data_packet )
					{
						Alert_Message( "TPMICRO CODE: completed distribution" );
					}
					else
					{
						Alert_Message( "UPDATE: mid error" );
					}
				}
		
				// ---------------------
		
				if( cdcs.transmit_packet_class == MSG_CLASS_CS3000__FROM_CONTROLLER__CODE_DISTRIBUTION )
				{
					// 3/27/2014 ajv : Pass a flag indicating that a restart is pending.
					CODE_DOWNLOAD_close_dialog( (true) );
	
					// 3/31/2014 rmd : For the controller to controller code distribution, FREE the memory where
					// we've been getting the data to send. We are done with it. mem_free has a NULL check on
					// the pointer so don't need to do here. NOTICE we do not do this for the HUB code
					// distribution, it needs to verify receipt, and possibly retransmit packets.
					mem_free( cdcs.transmit_data_start_ptr );
				}
				
				// ----------
				
				// 3/28/2014 rmd : And now we wait for the drivers to empty.
				cdcs.transmit_state = CODE_TRANSMITTING_STATE_waiting_for_serial_drivers_to_empty;
			}
			else
			{
				Alert_Message( "CODE TRANSMIT: logical error" );
			}
			break;
		
		// ----------
		
		case CODE_TRANSMITTING_STATE_sending_plus_plus_plus:

			CS_LED_RED_ON;
			
			// 8/7/2017 rmd : Clean the rcvd data buffer and set up the hunt.
			RCVD_DATA_enable_hunting_mode( UPORT_B, PORT_HUNT_FOR_SPECIFIED_STRING, WHEN_STRING_FOUND_NOTIFY_THE_CODE_DISTRIBUTION_TASK );
			
			// ----------
			
			// 10/29/2016 rmd : Define what we're huting for.
			SerDrvrVars_s[ UPORT_B ].UartRingBuffer_s.sh.str_to_find = (char*)m7_ok_response_str;
			
			SerDrvrVars_s[ UPORT_B ].UartRingBuffer_s.sh.chars_to_match = strlen( m7_ok_response_str );
		
			// 8/21/2017 rmd : The radio responds with "Raveon RV-M7<CR><LF>OK<CR><LF><CR><LF>", so we
			// need to expect the string at 12 deep into the buffer.
			SerDrvrVars_s[ UPORT_B ].UartRingBuffer_s.sh.depth_into_the_buffer_to_look = 12;
			
			// ----------
			
			ldh.dptr = (UNS_8*)m7_plus_plus_plus_str;
			
			ldh.dlen = strlen( m7_plus_plus_plus_str );
			
			AddCopyOfBlockToXmitList( UPORT_B, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );
			
			// ----------
			
			// 8/7/2017 rmd : Wait 2 seconds for that OK to appear. Observations on the scope show it
			// arrives in more than 500ms but less than 600ms.
			packet_rate_ms = (2000);
			
			cdcs.transmit_state = CODE_TRANSMITTING_STATE_reading_temperature;

			CS_LED_RED_OFF;

			break;
			
		// ----------
		
		case CODE_TRANSMITTING_STATE_reading_temperature:
			if( pcdtqs_ptr->event == CODE_DISTRIBUTION_EVENT_specified_string_found )
			{
				RCVD_DATA_enable_hunting_mode( UPORT_B, PORT_HUNT_FOR_SPECIFIED_TERMINATION, WHEN_STRING_FOUND_NOTIFY_THE_CODE_DISTRIBUTION_TASK );
				
				SerDrvrVars_s[ UPORT_B ].UartRingBuffer_s.th.str_to_find = (char*)m7_temp_response_str;

				SerDrvrVars_s[ UPORT_B ].UartRingBuffer_s.th.chars_to_match = strlen(m7_temp_response_str);

				SerDrvrVars_s[ UPORT_B ].UartRingBuffer_s.th.string_index = 0;
				
				// ----------
				
				ldh.dptr = (UNS_8*)m7_read_temp_str;
				
				ldh.dlen = strlen( m7_read_temp_str );
				
				AddCopyOfBlockToXmitList( UPORT_B, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );
				
				// ----------
				
				// 8/7/2017 rmd : Wait 1 second for the temp response to appear. Observations on the scope
				// show it has arrived in less than 20ms.
				packet_rate_ms = (1000);
				
				cdcs.transmit_state = CODE_TRANSMITTING_STATE_sending_exit;
			}
			else
			{
				// 8/7/2017 rmd : Alert and return to sending packets.
				Alert_Message( "RAVEON: failed to enter command mode" );
				
				// 8/7/2017 rmd : But do send the exit first, just in case. If not in command mode the radio
				// will transmit the 'exit' characters, which is okay, won't hurt anything.
				cdcs.transmit_state = CODE_TRANSMITTING_STATE_sending_exit;

				// 8/22/2017 rmd : Just go right there. No purpose to waiting.
				packet_rate_ms = (50);
			}
			break;
			
		// ----------
		
		case CODE_TRANSMITTING_STATE_sending_exit:
			if( pcdtqs_ptr->event == CODE_DISTRIBUTION_EVENT_specified_termination_found )
			{
				// 8/22/2017 rmd : What we know is that at positions 6 and 7 in the array is a 2 digit
				// Celcius temperature. We are going to pick it out and treat it as a string. If the string
				// length is 16 the temperature is 3 digits, which is out of control hot!
				char_array[ 0 ] = *(pcdtqs_ptr->dh.dptr + 6);
				char_array[ 1 ] = *(pcdtqs_ptr->dh.dptr + 7);

				if( pcdtqs_ptr->dh.dlen == 16 )
				{
					char_array[ 2 ] = *(pcdtqs_ptr->dh.dptr + 8);
				}
				else
				{
					char_array[ 2 ] = 0x00;
				}

				// 8/22/2017 rmd : String termination to cover the 3-digit temp.
				char_array[ 3 ] = 0x00;
				
				//Alert_Message_va( "RAVEON: found temp response string: %s", char_array );
				
				m7_current_temp = atol( char_array );
				
				// 12/20/2017 rmd : Alert used during development only, so normally commented out.
				//Alert_Message_va( "RAVEON: current temp is %u", m7_current_temp );
			}
			else
			{
				// 8/7/2017 rmd : Alert and return to sending packets.
				Alert_Message( "RAVEON: temp read error" );
			}
			
			// ----------
			
			// 8/23/2017 rmd : A NOTE about timing - the entire +++ / read temperature / exit sequence
			// takes less than 600ms, of which 500ms of is waiting after the +++. That 500ms is a guard
			// zone the radio imposes before entering its command mode.
			
			// ----------
			
			// 8/22/2017 rmd : Now we feed the exit command, and get back to sending packets.
			ldh.dptr = (UNS_8*)m7_exit_str;
			
			ldh.dlen = strlen( m7_exit_str );
			
			AddCopyOfBlockToXmitList( UPORT_B, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );
			
			// 8/7/2017 rmd : Wait 500ms for the radio to be ready to again transmit packets.
			packet_rate_ms = (500);
			
			// ----------
			
			// 10/30/2017 rmd : Re-enable PACKET HUNTING, the default normally expected mode.
			RCVD_DATA_enable_hunting_mode( UPORT_B, PORT_HUNT_FOR_PACKETS, NOT_STRING_HUNTING );
			
			// ----------
			
			cdcs.transmit_state = CODE_TRANSMITTING_STATE_building_data_packets;

			break;
			
		// ----------
		
		case CODE_TRANSMITTING_STATE_waiting_for_serial_drivers_to_empty:
			// 7/21/2016 rmd : The code distribution packet timing is designed to be such that there is
			// NO packet build up in the serial driver (because that makes timing problems down
			// complicated chains and the master no longer can know if all controllers have received the
			// full 500+ packet stream yet). However we still test that the driver is empty before
			// proceeding to the next phase.
			//
			// 7/21/2016 rmd : Being a MASTER is sending this code distribution only need to check
			// PORT_B and PORT_TP. By definition will not send to PORT_A.
			drivers_empty = (true);
			
			if( config_c.port_B_device_index )
			{
				// 4/3/2014 rmd : If still an item on the list not empty.
				if( xmit_cntrl[ UPORT_B ].xlist.count )
				{
					drivers_empty = (false);
				}
			}
	
			// 5/31/2017 rmd : Only for FLOWSENSE code distribution do we need to check the tpmciro
			// port. Not for hub distribution. Flowsense traffic could be present on the tpmicro port!
			if( cdcs.transmit_packet_class == MSG_CLASS_CS3000__FROM_CONTROLLER__CODE_DISTRIBUTION )
			{
				// 7/21/2016 rmd : PORT_TP always exists.
				if( xmit_cntrl[ UPORT_TP ].xlist.count )
				{
					drivers_empty = (false);
				}
			}
			
			// ----------
			
			if( drivers_empty )
			{
				cdcs.transmit_accumulated_reboot_ms = 0;
	
				// 4/3/2014 rmd : Now switch states to allow slaves to reboot and become available.
				cdcs.transmit_state = CODE_TRANSMITTING_STATE_waiting_for_slaves_to_reboot;
			}
			
			// 6/20/2017 rmd : Will check back in at the default 1Hz rate.

			break;
			
		// ----------
		
		case CODE_TRANSMITTING_STATE_waiting_for_slaves_to_reboot:

			// 9/19/2017 rmd : This state ticks along at the default 1 hertz rate.
			
			if( cdcs.transmit_packet_class == MSG_CLASS_CS3000__FROM_HUB__CODE_DISTRIBUTION_BROADCAST )
			{
				// 9/19/2017 rmd : Temporary alert line. Only made one time.
				if( cdcs.transmit_accumulated_reboot_ms == 0 )
				{
					Alert_Message( "HUB waiting 90 seconds to start query" );
				}

				reboot_or_query_wait_ms = CODE_DISTRIBUTION_HUB_TO_WAIT_FOR_QUERY_START_MS;
			}
			else
			{
				// 9/19/2017 rmd : Temporary alert line. Only made one time.
				if( cdcs.transmit_accumulated_reboot_ms == 0 )
				{
					Alert_Message( "FLOWSENSE waiting 60 seconds before reboot" );
				}

				reboot_or_query_wait_ms = CODE_DISTRIBUTION_FLOWSENSE_TO_WAIT_BEFORE_MASTER_REBOOTS_MS;
			}
			
			// ----------
			
			cdcs.transmit_accumulated_reboot_ms += 1000;
			
			if( cdcs.transmit_accumulated_reboot_ms >= reboot_or_query_wait_ms )
			{
				if( cdcs.transmit_packet_class == MSG_CLASS_CS3000__FROM_CONTROLLER__CODE_DISTRIBUTION )
				{
					transmission_completed_clear_hub_distribution_flag_and_perform_a_restart();
				}
				else
				if( cdcs.transmit_packet_class == MSG_CLASS_CS3000__FROM_HUB__CODE_DISTRIBUTION_BROADCAST )
				{
					// 7/11/2017 rmd : Well this is a hub distribution, so the next state is to cycle through
					// the 3000's in the hub list looking to see who received what with regards to the code
					// update. Prepare to cycle through the hub list.
		
					// 7/11/2017 rmd : Meaning transmit to the first 3000 in the list.
					cdcs.transmit_hub_query_list_count = 1;
					
					// 7/12/2017 rmd : Set up the bitfield to receive the responses from each of the controllers
					// in the hub list.
					memset( cdcs.hub_packets_bitfield, 0x00, sizeof(cdcs.hub_packets_bitfield) );
					
					Alert_Message_va( "CODE: query sn %u", return_sn_for_this_item_in_the_hub_list( cdcs.transmit_hub_query_list_count ) );

					if( build_and_send_a_hub_distribution_query_for_this_3000_sn( return_sn_for_this_item_in_the_hub_list( cdcs.transmit_hub_query_list_count ) ) )
					{
						cdcs.transmit_state = CODE_TRANSMITTING_STATE_querying_those_in_the_hub_list;
						
						cdcs.transmit_hub_query_list_count += 1;
						
						// 8/2/2017 rmd : Set a time that is plenty long to allow the repsonse to arrive. If the
						// response arrives we do not wait this entire time. We only wait this long if we don't get
						// a response back. We surely want to make sure this time iIS long enough for any reponse to
						// arrive, if it were too short we would be sending the next query before the last one
						// finished.
						packet_rate_ms = (2 * cdcs.transmit_hub_packet_rate_ms);
					}
					else
					{
						// 7/12/2017 rmd : This would mean there is nobody in the hub list and we wasted our time
						// transmitting the code binary. Or there was an error with the cdcs.mode value.
						transmission_completed_clear_hub_distribution_flag_and_perform_a_restart();
					}
				}
				else
				{
					Alert_Message( "UNEXP CODE DISTRIB CLASS" );
				}
			}
		
			// 6/20/2017 rmd : Will check back in at the default 1Hz rate.
		
			break;
			
		// ----------
		
		case CODE_TRANSMITTING_STATE_querying_those_in_the_hub_list:
		
			// 7/20/2017 rmd : So we got a response or the timer timed out. If the timer times out what
			// should we do. Well I'm not sure ... so nothing. Just proceed along.
			if( build_and_send_a_hub_distribution_query_for_this_3000_sn( return_sn_for_this_item_in_the_hub_list( cdcs.transmit_hub_query_list_count ) ) )
			{
				Alert_Message_va( "CODE: query sn %u", return_sn_for_this_item_in_the_hub_list( cdcs.transmit_hub_query_list_count ) );

				cdcs.transmit_state = CODE_TRANSMITTING_STATE_querying_those_in_the_hub_list;
				
				cdcs.transmit_hub_query_list_count += 1;
				
				// 8/2/2017 rmd : Set a time that is plenty long to allow the repsonse to arrive. If the
				// response arrives we do not wait this entire time. We only wait this long if we don't get
				// a response back. We surely want to make sure this time is long enough for any reponse to
				// arrive, if it were too short we would be sending the next query before the last one
				// finished.
				packet_rate_ms = (2 * cdcs.transmit_hub_packet_rate_ms);
			}
			else
			{
				Alert_Message( "CODE: query done" );

				// 7/20/2017 rmd : To get here, there was at least one 3000 in the list, and we have reached
				// the end of the list. So now we should go back to re-transmitting the packets indicated in
				// the bitfield. BUT FIRST test if there are any bits set within the bitfield. If not that
				// means everybody has received the binary and we are done with the transmission.
				still_some = (false);
				
				for( iii=0; iii<HUB_BITFIELD_BYTES; iii++ )
				{
					if( cdcs.hub_packets_bitfield[ iii ] )
					{
						still_some = (true);
					}
				}
				
				if( still_some )
				{
					Alert_Message( "CODE: hub needs to re-transmit some" );

					// 7/20/2017 rmd : Need to loop around and restart the transmission. Must start with the
					// INIT packet in case a controller did not receive it.
					//
					// 7/20/2017 rmd : Flag this pass through is a hub restarting so we modify some behaviors
					// when building the init packet.
					cdcs.transmit_state = CODE_TRANSMITTING_STATE_restarting_transmission_for_hub;
					
					build_and_send_code_transmission_init_packet( NULL );
				}
				else
				{
					Alert_Message( "CODE: hub sees all received ok" );

					// ----------
					
					// 9/19/2017 rmd : Clear the all powerful battery backed indication that we have a
					// distribution to perform. It has completed so clear it now else upon restart the
					// distribution will start again.
					if( cdcs.transmit_data_mid == MID_CODE_DISTRIBUTION_main_data_packet )
					{
						weather_preserves.hub_needs_to_distribute_main_binary = (false);
					}
					else
					if( cdcs.transmit_data_mid == MID_CODE_DISTRIBUTION_tpmicro_data_packet )
					{
						weather_preserves.hub_needs_to_distribute_tpmicro_binary = (false);
					}
					
					// ----------
					
					// 7/20/2017 rmd : It appears there is nothing to transmit. This would happen if they all
					// responded with nothing to retransmit. Means we're done!
					transmission_completed_clear_hub_distribution_flag_and_perform_a_restart();
				}
			}
			
			break;
			

		default:
			// 10/17/2018 rmd : Abel actually saw this alert at a HUB in Menifee. I'll enhance it to
			// include the state.
			Alert_Message_va( "CODE DISTRIB: unexpd transmit state: %u", cdcs.transmit_state );
			
	}  // of switch
	
	// ----------

	// 3/6/2014 ajv : Always restart the code update timer which will trigger the next pass
	// through this code. There is only one way out of this sequence. Send all the data, let the
	// drivers empty, wait the slave reboot time, then reboot ourselves.
	xTimerChangePeriod( cdcs.transmit_packet_rate_timer, MS_to_TICKS( packet_rate_ms ), portMAX_DELAY );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

