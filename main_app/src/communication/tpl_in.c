/*  file = tpl_in.c                          2011.07.25  rmd  */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"tpl_in.h"

#include	"tpl_out.h"

#include	"packet_router.h"

#include	"comm_mngr.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"configuration_controller.h"

#include	"timers.h"

#include	"crc.h"

#include	"cs_mem.h"

#include	"cent_comm.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

const unsigned char _Masks[ 8 ] =	{ 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 };

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define IM_ALLOWABLE_STATUS_TIMER_TIMEOUTS			(2)

#define IM_WAITING_FOR_STATUS_RQST_SECONDS			(5)

#define IM_WAITING_FOR_RESPONSE_TO_STATUS_SECONDS	(10)


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


INCOMING_STATS IncomingStats;


/*      The FOAL Transport Implementation
 *
 * Packets are found at the serial port level based on our preamble and post
 * amble sequences. They are passed up here to the transport with the ambles
 * stripped. If the CRC is good we add the packet to an incoming message. When
 * the message is complete the whole message is passed to the app.
 *
 * Incoming messages are built as a list of list of packets. What this means
 * is each incoming packet with a To address matching our own either gets added
 * to an existing Message ID list of packets or creates a new message to add
 * to. Each incoming message has a timer assoicated with it. It has a certain
 * amount of time to be completed or it will be destroyed - about 5 minutes.
 *
 *
 */


// On program start this is zero'd out so the count is zero and
MIST_LIST_HDR_TYPE  IncomingMessages;


// Though there is no message size limit,
// so we have some reasonable limit on message size we will
// flag us if we cross this arbritrary barrier

#define TPL_IN_MAX_MESSAGE_PACKETS	60

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void nm_destroy_this_IM( INCOMING_MESSAGE_STRUCT *pim )
{
	// No mutex protection on the IM list itself. We do however protect the list of packets for the particular IM.

	INCOMING_MESSAGE_PACKET_STRUCT *imps;

	if( xTimerDelete( pim->timer_existence, 0 ) == pdFAIL )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "Timer Queue Full!" );
	}

	if( xTimerDelete( pim->timer_waiting_for_status_rqst, 0 ) == pdFAIL )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "Timer Queue Full!" );
	}

	if( xTimerDelete( pim->timer_waiting_for_response_to_status, 0 ) == pdFAIL )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "Timer Queue Full!" );
	}

	// Not only delete the timers but release all memory associated with the message and take the message off the list! We're
	// DONE with it.
	
	// Start with the list of the blocks of the message.
	xSemaphoreTake( pim->list_im_packets_list_MUTEX, portMAX_DELAY );

	while( (imps = nm_ListRemoveHead( &pim->im_packets_list )) )
	{
		mem_free( imps->dh.dptr );
	
		mem_free( imps );
	}
	
	xSemaphoreGive( pim->list_im_packets_list_MUTEX );

	// Now delete the semaphore (which is implemented as a queue).
	vQueueDelete( pim->list_im_packets_list_MUTEX );
	
	// Then the message itself. Mutext protection MUST be provided external to this function.
	nm_ListRemove( &IncomingMessages, pim );
	
	mem_free( pim );
}

/* ---------------------------------------------------------- */
void TPL_IN_destroy_all_incoming_3000_scan_response_and_token_response_messages( void )
{
	INCOMING_MESSAGE_STRUCT		*im, *tmp_im;
	
	xSemaphoreTake( list_tpl_in_messages_MUTEX, portMAX_DELAY );

	im = nm_ListGetFirst( &IncomingMessages );

	while( im != NULL )
	{
		// get the next one now for the problem that will arise if we remove this below
		tmp_im = nm_ListGetNext( &IncomingMessages, im );
		
		// This function is called 2 ways. First when the application timer expires (and it
		// can only expire with a FOAL master) and second when a message unlocks the comm mngr
		// (and messages only unlock the comm mngr when for the foal master).
		if( im->msg_class.routing_class_base == ROUTING_CLASS_IS_3000_BASED )
		{
			if ( (im->msg_class.rclass == MSG_CLASS_CS3000_SCAN_RESP) || (im->msg_class.rclass == MSG_CLASS_CS3000_TOKEN_RESP) )
			{
				nm_destroy_this_IM( im );
			}
		}
		
		im = tmp_im;  // Assign the next one we acquired above - it remains correct even though we removed one in the list.
	}
	
	xSemaphoreGive( list_tpl_in_messages_MUTEX );
}

/* ---------------------------------------------------------- */
void nm_TPL_IN_destroy_all_incoming_3000_scan_and_token_messages( void )
{
	INCOMING_MESSAGE_STRUCT		*im, *tmp_im;
	
	im = nm_ListGetFirst( &IncomingMessages );

	while( im != NULL )
	{
		// get the next one now for the problem that will arise if we remove this below
		tmp_im = nm_ListGetNext( &IncomingMessages, im );
		
		// This function is called 2 ways. First when the application timer expires (and it
		// can only expire with a FOAL master) and second when a message unlocks the comm mngr
		// (and messages only unlock the comm mngr when for the foal master).
		if( im->msg_class.routing_class_base == ROUTING_CLASS_IS_3000_BASED )
		{
			if ( (im->msg_class.rclass == MSG_CLASS_CS3000_SCAN) || (im->msg_class.rclass == MSG_CLASS_CS3000_TOKEN) )
			{
				nm_destroy_this_IM( im );
			}
		}
		
		im = tmp_im;  // Assign the next one we acquired above - it remains correct even though we removed one in the list.
	}
}

/* ---------------------------------------------------------- */
void nm_TPL_IN_destroy_all_incoming_2000_comm_test_messages( void )
{
	INCOMING_MESSAGE_STRUCT		*im, *tmp_im;
	
	im = nm_ListGetFirst( &IncomingMessages );

	while( im != NULL )
	{
		// get the next one now for the problem that will arise if we remove this below
		tmp_im = nm_ListGetNext( &IncomingMessages, im );
		
		// This function is called 2 ways. First when the application timer expires (and it
		// can only expire with a FOAL master) and second when a message unlocks the comm mngr
		// (and messages only unlock the comm mngr when for the foal master).
		if( im->msg_class.routing_class_base == ROUTING_CLASS_IS_2000_BASED )
		{
			if ( (im->msg_class.rclass == MSG_CLASS_2000e_TEST_PACKET_FROM_C_OR_C) || (im->msg_class.rclass == MSG_CLASS_2000e_TEST_PACKET_ECHO_TO_C_OR_C) )
			{
				nm_destroy_this_IM( im );
			}
		}
		
		im = tmp_im;  // Assign the next one we acquired above - it remains correct even though we removed one in the list.
	}
}

/*--------------------------------------------------------------------------
	Name:           MIDOnTheListOfIncomingMessages( MID_TYPE mid)

	FileName:		C:\develop\GHS\ET2000\ETSource\B_TP_in.c

	Description:    Search through the list of IncomingMessages looking for
					one with this message ID. Return a pointer to that list
					item.
					
	Parameters:     MID of the item we're looking for
	
	Return Values:  Pointer to the matching Incoming Message, NULL if no match
					is found.
	  
	History:		date		approval	author	description
					10/20/00	--/--/--	rmd		created
					04/30/01	--/--/--	rmd		updated for code review		

 -------------------------------------------------------------------------*/
INCOMING_MESSAGE_STRUCT *nm_MIDOnTheListOfIncomingMessages( MID_TYPE mid )
{
	INCOMING_MESSAGE_STRUCT		*rv;
	
	rv = nm_ListGetFirst( &IncomingMessages );

	while( rv != NULL )
	{
		if( rv->MID.S == mid.S ) break;

		rv = nm_ListGetNext( &IncomingMessages, rv );
	}

	// We either return a NULL or a pointer to the message that matches.
	return( rv );
}

/* ---------------------------------------------------------- */
void start_existence_timer( INCOMING_MESSAGE_STRUCT *pim )
{
	// START THE EXISTENCE TIMER EACH TIME A PACKET COMES IN AND EACH TIME WE SEND A STATUS OUT FOR
	// WHAT WE HAVE RECEIVED SO FAR
	//
	// The effect is that we exist for 20 seconds after each packet comes in. When a message gives up we exist
	// for 20 seconds after it goes idle. Hopefully before the application timesout we have gone idle.
	//
	// DISCUSSION IN NON FOAL ENVIRONMENT
	// the logic behind this timer is that the im messages do not destroy themselves
	// after a sucessful status block is sent to the originating transport - they destroy
	// themselves after what we figure is long engough for the retries by the originating
	// transport to have taken place. Of course what this means is that we will have
	// completed messages on the im list just sitting there until their timer goes off - but
	// that's by design. The count of them depends on the transport message density as compared
	// to the im message lifetime - but I typically see that number ramp up to about 30.
	//

	// This message hang around time may become too large for large messages - we'll see.
	//
	// Most communciations media seem to take about 2 seconds per block WORST case  
	//	(use the WAITING_FOR_RESPONSE_TO_STATUS_SECONDS assuming its the bigger of the two
	//
	// Check this scenario out. The Foal master sends a multiple block message to a slave.
	// Each block he recieves starts this timer. Suppose we dont get the request for status
	// at the slave. That says this incoming message must wait around long enough for the outgoing
	// message at the FOAL master to timeout waiting for status and then resend the request for
	// status block. So this existence time needs to be a function of the OM wait for status time!
	//
	// There are two side to look at this. First when the IM is ACTIVE we restart this timer
	// each time we send out STATUS - we want to wait for the missing block to show. The other side of
	// the coin is that an incoming message at the slave should wait around long enough for the OM
	// at the master to get around to its retries. The OM retry time is somewhat long (currently 25 seconds
	// and set on the longest central response).
	//
	// 3/14/2014 rmd : Being that the timer task is a high priority task using portMAX_DELAY
	// when posting is safe. Anything on the timer queue is quickly processed and therefore the
	// queue is effectively always empty.
	xTimerStart( pim->timer_existence, portMAX_DELAY );
}

/* ---------------------------------------------------------- */
void process_im_existence_timer_expired( xTimerHandle pxTimer )
{
	// Remember : this function is executed in the context of the timer service task. It seems there may be a problem with
	// deleting the very timer that this callback function belongs to. Though I'm not sure why. As the delete is just
	// queued up on the timer service message queue. So defer the actual processing of this expired timer to the TPL_IN
	// task itself.

	TPL_IN_EVENT_QUEUE_STRUCT	tiqs;
	
	tiqs.event = TPL_IN_EVENT_the_existence_timer_expired;
	
	tiqs.im = (INCOMING_MESSAGE_STRUCT*)(pvTimerGetTimerID( pxTimer ));
	
	if( xQueueSend( TPL_IN_event_queue, &tiqs, 0 ) != pdPASS )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "TPL_IN queue full" );
	}
}	

/* ---------------------------------------------------------- */
void tpl_in_existence_timer_expired( INCOMING_MESSAGE_STRUCT *pim )
{
	IncomingStats.existence_timer_expired++;

	// Check im points to a message on the list - has to or something is wrong. WELL BE CAREFUL here - if the application
	// and this timer time out at the same time its possible the application already wiped this message off the list. So
	// this desn't really have to indicate something WRONG. Could use this as an exit from the routine not to flag an
	// error.
	xSemaphoreTake( list_tpl_in_messages_MUTEX, portMAX_DELAY );

	if( nm_OnList( &IncomingMessages, pim ) == FALSE )
	{
		//ALERT_MESSAGE_WITH_FILE_NAME( "IM not on list" ); 
		Alert_item_not_on_list( pim, IncomingMessages ); 
	}
	else
	{
		nm_destroy_this_IM( pim );
	}

	xSemaphoreGive( list_tpl_in_messages_MUTEX );
}

/* ---------------------------------------------------------- */
void im_StopBothPacketTimers( INCOMING_MESSAGE_STRUCT *im )
{
	portBASE_TYPE	result1, result2;
	
	result1 = xTimerStop( im->timer_waiting_for_status_rqst, portMAX_DELAY );
	
	result2 = xTimerStop( im->timer_waiting_for_response_to_status, portMAX_DELAY );

	if( (result1 == pdFAIL) || (result2 == pdFAIL) )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "Timer Queue Full" );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Proto declaration needed for compilation.
void nm_nm_build_status_and_send_it( INCOMING_MESSAGE_STRUCT *im );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void process_waiting_for_status_rqst_timer_expired( xTimerHandle pxTimer )
{
	INCOMING_MESSAGE_STRUCT		*im;

	im = (INCOMING_MESSAGE_STRUCT*)(pvTimerGetTimerID( pxTimer ));
	
	IncomingStats.status_rqst_timer_expired++;

	// Check pim points to a message on the list - has to or something is wrong.
	xSemaphoreTake( list_tpl_in_messages_MUTEX, portMAX_DELAY );

	if( nm_OnList( &IncomingMessages, im ) == FALSE )
	{
		//Alert_Message( "Status IM not on list" );
		Alert_item_not_on_list( im, IncomingMessages ); 
	} 
	else
	{
		// One of the 2 packet timers has expired. We need to make sure the other one gets stopped so that we don't get too many
		// status responses being pumped out.
		im_StopBothPacketTimers( im );
		
		if( im->timer_timeouts < IM_ALLOWABLE_STATUS_TIMER_TIMEOUTS )
		{
			im->timer_timeouts++;
		
			// The result of this timer going off is to force a status packet to be generated and sent for this im.
			xSemaphoreTake( im->list_im_packets_list_MUTEX, portMAX_DELAY );

			nm_nm_build_status_and_send_it( im );

			xSemaphoreGive( im->list_im_packets_list_MUTEX );
		}
	}

	xSemaphoreGive( list_tpl_in_messages_MUTEX );
}

/* ---------------------------------------------------------- */
void process_waiting_for_response_to_status_timer_expired( xTimerHandle pxTimer )
{
	INCOMING_MESSAGE_STRUCT		*im;

	im = (INCOMING_MESSAGE_STRUCT*)(pvTimerGetTimerID( pxTimer ));
	
	IncomingStats.status_response_timer_expired++;

	// Check pim points to a message on the list - has to or something is wrong.
	xSemaphoreTake( list_tpl_in_messages_MUTEX, portMAX_DELAY );

	if( nm_OnList( &IncomingMessages, im ) == FALSE )
	{
		//Alert_Message( "Status Response Timer: IM not on list" );
		Alert_item_not_on_list( im, IncomingMessages ); 
	}
	else
	{
		// One of the 2 packet timers has expired. We need to make sure the other one gets stopped so that we don't get too many
		// status responses being pumped out.
		im_StopBothPacketTimers( im );
		
		if ( im->timer_timeouts < IM_ALLOWABLE_STATUS_TIMER_TIMEOUTS )
		{
			im->timer_timeouts++;
		
			// the result of this timer going off is to force a status packet to be generated and sent for this im
			xSemaphoreTake( im->list_im_packets_list_MUTEX, portMAX_DELAY );

			nm_nm_build_status_and_send_it( im );

			xSemaphoreGive( im->list_im_packets_list_MUTEX );
		}
	}

	xSemaphoreGive( list_tpl_in_messages_MUTEX );
}

/* ---------------------------------------------------------- */
void nm_nm_build_status_and_send_it( INCOMING_MESSAGE_STRUCT *im )
{
	// Double nm to emphasize there are two lists used and therefore the caller should own both mutexes. First there is the list
	// of incoming messages and then the list of packets for that messages.

	INCOMING_MESSAGE_PACKET_STRUCT	*ime;
		
	TPL_STATUS_HEADER_TYPE			*sh;
	
	UNS_8							*ap, *ucp, *crc_start;
	
	DATA_HANDLE						ldh, mdh;   // mdh is for the whole message if complete
	
	UNS_16							AckBytes, i, mi;

	if( nm_OnList( &IncomingMessages, im ) == FALSE )
	{
		//ALERT_MESSAGE_WITH_FILE_NAME( "TPL_IN - IM not on list" );
		Alert_item_not_on_list( im, IncomingMessages ); 
	}
	else
	{
		// first decide if we are building an abreviated status packet or the detailed kind
		// if all the blocks are here then it's a complete message and we send a simple status
		// else we send a detailed status
		if( im->TotalBlocks == im->im_packets_list.count )
		{
			AckBytes = 0;   // signifys message is complete
		}
		else
		{
			AckBytes = ( (im->TotalBlocks-1) / 8 ) + 1; // number of ack bytes 
		}
	
		
		// 3/20/2012 rmd : For the broadcast messages that are not actually addressed to us we DO
		// NOT send the status packet. We do however (of course) give the message to the COMM_MNGR
		// if all the packets are here.
		if( im->permission_to_send_a_status )
		{
			// We need to build the status packet - without the ambles. The ambles are added just before packet is handed off to the
			// serial driver.
			ldh.dlen = sizeof( TPL_STATUS_HEADER_TYPE ) +
					   AckBytes + 
					   sizeof( CRC_BASE_TYPE );
		
			ldh.dptr = mem_malloc( ldh.dlen );
		
			ucp = ldh.dptr;
		
			crc_start = ucp;    // start of the packet
		
			sh = (TPL_STATUS_HEADER_TYPE*)ucp;
		
			
			sh->B[ 0 ] = 0;			// set the whole byte to 0
			
			sh->H.PID.STATUS = 1;	// signify status block
			
			// ----------
			
			// Assign the MSG_CLASS of the status packet based upon the class of the IM ... it should
			// be assigned the same class as the application here would assign if it were trying to
			// send an OM to the guy who sent this IM. This is important for the routing of this
			// status packet back to where it came from.
			UNS_32	status_packet_class;
			
			status_packet_class = 0;
			
			if( im->msg_class.routing_class_base == ROUTING_CLASS_IS_3000_BASED )
			{
				switch( im->msg_class.rclass )
				{
					case MSG_CLASS_CS3000_SCAN:
						//Alert_Message( "sending a scan status" );
						status_packet_class = MSG_CLASS_CS3000_SCAN_RESP;
						break;
				
					case MSG_CLASS_CS3000_SCAN_RESP:
						//Alert_Message( "sending a scan_resp status" );
						status_packet_class = MSG_CLASS_CS3000_SCAN;
						break;
				
					case MSG_CLASS_CS3000_TOKEN:
						//Alert_Message( "sending a token status" );
						status_packet_class = MSG_CLASS_CS3000_TOKEN_RESP;
						break;
				
					case MSG_CLASS_CS3000_TOKEN_RESP:
						//Alert_Message( "sending a token_resp status" );
						status_packet_class = MSG_CLASS_CS3000_TOKEN;
						break;
				
					default:
						Alert_Message( "Making IM status for unk class" );
						break;
				}
			}
			
			// 7/18/2013 rmd : When it comes to setting the message class in packet header the STATUS
			// packet and DATA packets are interchangeable. Note how we re-type sh as a DATA_HEADER.
			set_this_packets_CS3000_message_class( (TPL_DATA_HEADER_TYPE*)sh, status_packet_class );

			// ------------
			
			memcpy( &sh->H.FromTo.from, &im->from_to.to, 3 );  // Well it is from US right!

			memcpy( &sh->H.FromTo.to,   &im->from_to.from, 3 );  // We respond to the originator.
			
			// ------------
			
			sh->H.MID = im->MID;  // return the mid 
		
			sh->H.unused_in_status = 0;
		
			sh->H.AcksLength = AckBytes;   // says the message is incomplete
		
		
			// point to where the end of the status header - potentially where the CRC goes
			ucp += sizeof( TPL_STATUS_HEADER_TYPE );
			
			
			if ( AckBytes > 0 )
			{
				IncomingStats.PartialStatusSent++;
		
				// build the ack bytes
				
				ap = ucp;
		
				ime = im->im_packets_list.phead; // get the first packet in the list
		
				// integrity checker
				BOOL_32	NoBitsSet;
				
				NoBitsSet = (true);
		
				*ap = 0;    // set all bits to off - blocks not here
				
				if( ime == NULL )
				{
					// There IM must contain at least 1 packet.
					Alert_Message( "No packets in IM" );
				}
				else
				{
					for( i=1; i<=im->TotalBlocks; i++ )
					{
						// for each block get the mask index
						if ( ( i % 8)==0 ) mi=7; else mi = (i % 8) - 1;
			
						if( ime->BlockNumber == i )
						{
							NoBitsSet = (false);  // integrity check - must set at least one
			
							// the block is here - set the bit and get the next packet to look at
						
							*ap |= _Masks[ mi ];  // set the bit
			
							ime = nm_ListGetNext( &im->im_packets_list, ime );
			
							if( ime == NULL ) break;  // end of list of packets
						}
			
						if( mi == 7 )
						{
							ap++;   // next ack byte
	
							*ap=0;  // set em all off to start new ack byte
						}
					}
				}
		
				if( NoBitsSet == (true) )
				{
					Alert_Message( "No bits set in Status" );
				}
		
				ucp += AckBytes;    // point to where the CRC goes
		
			}
			else
			{
				IncomingStats.CompleteStatusSent++;
			}
		
			// ----------

			// 11/6/2012 rmd : Calculate the CRC. Which is returned in BIG ENDIAN format. Which is what
			// we need (see note in crc calc function). Then copy to ucp.
			CRC_BASE_TYPE	lcrc;
			
			lcrc = CRC_calculate_32bit_big_endian( crc_start, sizeof(TPL_STATUS_HEADER_TYPE) + AckBytes );
			
			memcpy( ucp, &lcrc, sizeof(CRC_BASE_TYPE) );
			
			// 11/6/2012 rmd : In case needed keep ucp up to snuff.
			ucp += sizeof(CRC_BASE_TYPE);
			
			// ----------

			// 3/26/2014 rmd : There are only 4 message classes that pass through the transport. SCAN,
			// SCAN_RESP, TOKEN, and TOKEN_RESP. In all cases any request for a status is to be
			// immediately send. No posting to a list waiting for the token to arrive (the BUS IS OURS
			// moment).
			Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy( im->port, ldh, NULL );
			
			// ALWAYS RELEASE THE STATUS PACKET MEMORY. A copy was made when we added the ambles to it.
			mem_free( ldh.dptr );
		}
		

		// Now pass the message to next layer - if complete
		if( AckBytes == 0 )
		{
			// message is complete
			// see if already handed to upper layer
			if( im->SentToApp == FALSE )
			{
				ime = im->im_packets_list.phead; // get the first packet in the list
	
				// figure out whole message length
				mdh.dlen = 0;
				while( ime != NULL )
				{
					mdh.dlen += ime->dh.dlen;
					ime = nm_ListGetNext( &im->im_packets_list, ime );
				}
		
				// allocate space for the whole message
				mdh.dptr = mem_malloc( mdh.dlen );
		
		
				// make a copy of the message in newly allocated memory
				ime = im->im_packets_list.phead; // get the first packet in the list
		
				ucp = mdh.dptr;
				while( ime!= NULL )
				{
					memcpy( ucp, ime->dh.dptr, ime->dh.dlen );
					
					ucp += ime->dh.dlen;
		
					ime = nm_ListGetNext( &im->im_packets_list, ime );
				}
		
				// 3/21/2012 rmd : Mark as passed to the app. Keep in mind a possible scenario. For the case
				// of a single box (or even a MASTER on a chain) the TOKEN_RESP is to ourselves. And
				// technically the TOKEN_RESP gives the COMM_MNGR the ability to generate the next TOKEN.
				// When we send the next TOKEN we clean the TPL_OUT and TPL_IN of all scan_resp and
				// token_resp messages. Therefore any further reference to the im pointer is invalid and
				// should not be attempted. Because of this we set the SENT_TO_APP indication (true) BEFORE
				// we hand the message to the comm mngr!
				im->SentToApp = TRUE;
				
				// 9/20/2012 rmd : At this point the transport is giving up the message to the application.
				// Our rule is at that layer level the to-from address must be in little endian format. So
				// fix that up here before we hand the message over.

				ADDR_TYPE temp_address;
				temp_address.from[0] = im->from_to.from[2];
				temp_address.from[1] = im->from_to.from[1];
				temp_address.from[2] = im->from_to.from[0];
				temp_address.to[0] = im->from_to.to[2];
				temp_address.to[1] = im->from_to.to[1];
				temp_address.to[2] = im->from_to.to[0];			


				// 3/21/2012 rmd : Makes a copy of the message and transfers the copy to the COMM_MNGR task.
				CENT_COMM_make_copy_of_and_add_message_or_packet_to_list_of_incoming_messages( im->port, mdh, im->msg_class, temp_address );
				
				// 3/16/2012 rmd : We're done with the assembled local copy of the message. So free it.
				mem_free( mdh.dptr );
				
				IncomingStats.MessagesToApplication++;
			}
			else
			{
				IncomingStats.SecondSubmitRequests++;
			}  // if we've given to the app already
	
		}
		else
		{
			// Restart the existence timer if we are expecting data so we are present long enough to recieve any response to this
			// status. Remember for an ACTIVE IM we may generate sveral status's to send out to the OM. We don't want to
			// be destroyed until well after the message goes idle.
			start_existence_timer( im );

			// Start the wait for packets in response to sending a STATUS indicating the message is incomplete (i.e. we haven't got all
			// the packets) ... we should only start this timer if this IM is an ACTIVE incoming message
			if( im->this_IM_is_active && im->permission_to_send_a_status )
			{
				// 3/14/2014 rmd : Being that the timer task is a high priority task using portMAX_DELAY
				// when posting is safe. Anything on the timer queue is quickly processed and therefore the
				// queue is effectively always empty.
				xTimerStart( im->timer_waiting_for_response_to_status, portMAX_DELAY );
			}
			
		}  // if message is not complete
	
	}  // of if im is not on the current list of IM's ... it should be!! 
}

/* ---------------------------------------------------------- */
/*
	Regarding Memory: The packet memory was acquired "taken" back in the rcvd_data task that found
	the packet to begin with. It will be released within this function after either a test fails OR
	we take a copy of the packet (without the header and CRC) OR we discover the packet already has
	been received. One way or another the memory must be released within this function.
*/
void add_packet_to_incoming_messages( UPORTS pport, DATA_HANDLE dh )
{
	// The dh.dptr points to a transport packet (minus the ambles). That means
	// user data wrapped up in the transport.

	TPL_DATA_HEADER_TYPE				*tdh;
	
	INCOMING_MESSAGE_STRUCT				*im;

	INCOMING_MESSAGE_PACKET_STRUCT		*me, *already_exists;  // pointers to one of the list items that make up a message
	
	DATA_HANDLE							ndh;  // new data handle for working with the data of the block to add
	
	unsigned char						*ucp;
	
	BOOL_32								insert_before, error_detected;
	
	ROUTING_CLASS_DETAILS_STRUCT		routing;
	
	char								str_64[ 64 ];
	
	UNS_32								payload_size;
	
	// ----------
	
	error_detected = FALSE;

	
	tdh = (TPL_DATA_HEADER_TYPE*)dh.dptr; // be careful to preserve tdh throughout function
	
	// ----------
	
	get_this_packets_message_class( tdh, &routing );
	
	// ----------
	
	if( routing.routing_class_base == ROUTING_CLASS_IS_2000_BASED )
	{
		// 11/28/2017 rmd : For the 2000e there are only 2 allowed inbound classes, related tot he
		// comm test.
		if( (routing.rclass != MSG_CLASS_2000e_TEST_PACKET_FROM_C_OR_C) && (routing.rclass != MSG_CLASS_2000e_TEST_PACKET_ECHO_TO_C_OR_C) )
		{
			Alert_Message( "TPL_IN unexp 2000e msg class" );
			
			error_detected = (true);
		}
	}
	
	// ----------
	
	// a sanity check - there is no real limit but make us conciously get this big
	if ( tdh->H.TotalBlocks > TPL_IN_MAX_MESSAGE_PACKETS )
	{
		// either we've got a botched packet or it really is part of a big message
		//
		sprintf( str_64, "IM Blocks=%d (too big)", tdh->H.TotalBlocks );
		Alert_Message( str_64 );
		
		error_detected = TRUE;
	}

	// ----------
	
	payload_size = dh.dlen - sizeof(TPL_DATA_HEADER_TYPE) - sizeof(CRC_TYPE);

	// test dlen here to be less than or equal to transport max user data size
	if( payload_size > TPL_USER_DATA_MAX_SIZE )
	{
		sprintf( str_64, "TPL_IN Length=%d (too big)", dh.dlen );
		Alert_Message( str_64 );
		
		error_detected = TRUE;
	}
	
	// ----------
	
	if( error_detected == FALSE )
	{
		// STEP 1: Create incoming message if doesn't already exist
		
		xSemaphoreTake( list_tpl_in_messages_MUTEX, portMAX_DELAY );

		im = nm_MIDOnTheListOfIncomingMessages( tdh->H.MID );
	
		if ( im == NULL )
		{
			// Not on the list of incoming messages - so add it.
			
			// ----------

			// 7/18/2013 rmd : The concept here is as we receive NEW incoming messages from a source or
			// device (from the central, or from an RRE for example) we should destroy the prior
			// incoming messages from the same source. The reasoning being that exchange has completed
			// and hanging on to old incoming messages only chews up memory until they are released by
			// time-out.
			
			// 2011.08.11 rmd : If we are receiving a NEW scan or token message we can destroy all the old scan or token
			// IM's that exist. This is really focused at the incoming token messages. If we make them very quickly we
			// rack up quite a store of old IM's. And that chews up alot of memory. If we are receiving a new token we
			// KNOW the FOAL master has purged himself of the old OM tokens. Because that is what we do. So why keep these
			// old IM tokens. No reason at all.
			if( routing.routing_class_base == ROUTING_CLASS_IS_3000_BASED )
			{
				if( (routing.rclass == MSG_CLASS_CS3000_SCAN) || (routing.rclass == MSG_CLASS_CS3000_TOKEN) )
				{
					nm_TPL_IN_destroy_all_incoming_3000_scan_and_token_messages();
				}
			}
			
			// ----------
			
			// 11/28/2017 rmd : To conserve resources, don't let the comm_test messages pile up. As a
			// new one is started delete all the old.
			if( routing.routing_class_base == ROUTING_CLASS_IS_2000_BASED )
			{
				if( (routing.rclass == MSG_CLASS_2000e_TEST_PACKET_FROM_C_OR_C) || (routing.rclass == MSG_CLASS_2000e_TEST_PACKET_ECHO_TO_C_OR_C) )
				{
					nm_TPL_IN_destroy_all_incoming_2000_comm_test_messages();
				}
			}
			
			// ----------

			IncomingStats.IncomingMessages++;

			// ----------

			// add it to the list of messages being built
			// allocate the memory, fill it out, and insert it
			im = mem_malloc( sizeof(INCOMING_MESSAGE_STRUCT) );
	
			// initialize it
			im->from_to = tdh->H.FromTo;

			im->port = pport; 

			im->MID = tdh->H.MID; 

			im->TotalBlocks = tdh->H.TotalBlocks;

			im->SentToApp = FALSE;

			im->list_im_packets_list_MUTEX = xSemaphoreCreateMutex();
			
			nm_ListInit( &im->im_packets_list, 0 );  // zero out the list of recieved packets
	
			// Create but do not start ALL three im timers.
			im->timer_existence = xTimerCreate(	(signed char*)"existence timer",
												MS_to_TICKS( ((5 + ((config_c.OM_Originator_Retries + 1) * config_c.OM_Seconds_for_Status_FOAL)) * 1000) ),
												FALSE,		// does not auto re-load
												im,
												process_im_existence_timer_expired );
							
	
			im->timer_waiting_for_status_rqst = xTimerCreate(	(const signed char*)"status rqst timer",
																MS_to_TICKS( ((IM_WAITING_FOR_STATUS_RQST_SECONDS) * 1000) ),
																FALSE,		// does not auto re-load
																im,
																process_waiting_for_status_rqst_timer_expired );
							

			im->timer_waiting_for_response_to_status = xTimerCreate(	(const signed char*)"status response timer",
																		MS_to_TICKS( ((IM_WAITING_FOR_RESPONSE_TO_STATUS_SECONDS) * 1000) ),
																		FALSE,		// does not auto re-load
																		im,
																		process_waiting_for_response_to_status_timer_expired );
							
			im->timer_timeouts = 0;  // initialize to zero of course
	
			// ----------

			// 6/30/2015 rmd : With the introduction of the broadcast token we have decided to NEVER
			// send a status packet. They complicate the existence of the OM considering multiple ports
			// are send the same message. Receipt of the status destroys the OM.
			im->permission_to_send_a_status = (false);
			
			/*
			// 3/20/2012 rmd : At this point the im->message_class is not set so don't use it when
			// figuring how to set the broadcast behavior. If the message is a TOKEN and it is not
			// specifically addressed to us all we should do is receive the packets. If we get the whole
			// message we will hand to the comm_mngr as an incoming message (that we won't make a RESP
			// to!).
			im->permission_to_send_a_status = (true);
															
			if( get_this_packets_message_class( tdh ) == MSG_CLASS_CS3000_TOKEN )
			{
				if( !this_communication_address_is_our_serial_number( (UNS_8 *)&tdh->H.FromTo.to, THE_PASSED_IN_DATA_IS_BIG_ENDIAN ) )
				{
					im->permission_to_send_a_status = (false);
				}
			}
			*/

			// ----------
			
			if( (routing.routing_class_base == ROUTING_CLASS_IS_3000_BASED) && (routing.rclass == MSG_CLASS_CS3000_SCAN) )
			{
				// 5/22/2015 ajv : Track every single SCAN message that comes in, regardless of whether it's
				// for us or not.
				comm_stats.scan_msgs_rcvd += 1;
			}

			// ----------

			// Now add it to the list.
			if( nm_ListInsertTail( &IncomingMessages, im ) != MIST_SUCCESS )
			{
				Alert_Message( "Failure adding new IM to list" );
			}
		}
		else
		{
			// the message ID is already on the list so stop the 2 packet timers
			//
			// The waiting_for_status_rqst_timer is stopped each time a packet comes in. It is also restarted each time a
			// packet comes in EXCEPT if its a request for a status packet.
			//
			// The waiting_for_response_to_status_timer is also stopped each time a packet comes in. It is started only when
			// we request packets to be resent.
			im_StopBothPacketTimers( im );
		}
		
		
		xSemaphoreTake( im->list_im_packets_list_MUTEX, portMAX_DELAY );

		// For each packet that comes in set this message to be active or not ... the bit should either be set for all packets or
		// not TEST for that.
		//
		// The effect of the TEST and accompanying code is to make the message active if the bit is set in ONE of the message
		// packets even if it isn't set in the others - we also throw an alert line if we see the case of a varying bit value during
		// a message - don't know why but I suppose we should know about it cause it shouldn't ever happen.
		if( im->im_packets_list.count == 0 )
		{
			// This is the first packet for this message so the im->this_IM_is_active should be set according to the bit - no test
			// possible here.
			if( tdh->H.PID.make_this_IM_active == 1 )
			{
				im->this_IM_is_active = TRUE;
			}
			else
			{
				im->this_IM_is_active = FALSE;
			}
			
			
			// ALSO extract the message classification to help with routing
			im->msg_class = routing;
		}
		else
		{
			// else there have already been packets received for this message
			if( tdh->H.PID.make_this_IM_active == 1 )
			{
				if( im->this_IM_is_active == FALSE )
				{
					Alert_Message( "IM varying active bit (0x01)" );
				}
			
			}
			else
			{
				// the bit is 0 in the packet so the message better not be set to active from other packets
				if( im->this_IM_is_active == TRUE )
				{
					Alert_Message( "IM varying active bit (0x02)" );
				}
			}
			
			// if it is set for any ONE packet the whole message is considered active - we have our
			// chance to set make_this_IM_active FALSE when the first packet comes in
			if( tdh->H.PID.make_this_IM_active == 1 )
			{
				im->this_IM_is_active = TRUE;
			}
			
			// AND do a similiar thing with the message_class to see if it is varying from packet to
			// packet which of course it shouldn't ... we don't really do anything about it if it is
			// except throw an alert ... the first packet sets the class for the whole message
			if( (im->msg_class.routing_class_base != routing.routing_class_base) || (im->msg_class.rclass != routing.rclass) )
			{
				// 7/18/2013 rmd : Should never see this. If this occurs may want to enhance to show class
				// of the incoming packet in order to learn more.
				Alert_Message( "IM varying class detected" );
			}
		}
		
		// ----------

		// STEP 2: Add this packet to the message look for packet to already exist or add to end of
		// list of packets for this message.
		
		insert_before = FALSE;
	
		already_exists = nm_ListGetFirst( &im->im_packets_list );
	
		if( already_exists != NULL )
		{
			// either we're going to insert it OR it exists already
			do {
	
				if( already_exists->BlockNumber == tdh->H.ThisBlock )
				{
					break;
				}
	
				if( already_exists->BlockNumber > tdh->H.ThisBlock )
				{
					insert_before = TRUE;
					break;
				}
	
				already_exists = nm_ListGetNext( &im->im_packets_list, already_exists );
	
			} while ( already_exists != NULL );
		}
	
	
		if( (already_exists == NULL) || (insert_before == (true)) )
		{
			// The list is empty or we didn't find a match so create the element and add it to the list. Define the data to store on the
			// list - we store ONLY the message data. That is the data the ring buffer packet detection gave to us minus the TPL HEADER
			// and CRC. We don't need those things anymore.

			ndh.dlen = payload_size;
			ndh.dptr = mem_malloc( ndh.dlen );
	
			ucp = (unsigned char*)dh.dptr;
			ucp += sizeof( TPL_DATA_HEADER_TYPE );
	
			memcpy( ndh.dptr, ucp, ndh.dlen );
	
			// create the element
			me = mem_malloc( sizeof( INCOMING_MESSAGE_PACKET_STRUCT ) );
	
			me->BlockNumber = tdh->H.ThisBlock;
	
			me->dh = ndh;
	
			// Add it to the list. Note how list insert works: it inserts BEFORE already_exists if not a NULL else it adds to the end of
			// list.
			nm_ListInsert( &im->im_packets_list, me, already_exists );
		}
		else
		{
			// already on list
			
			// do a test double sending a packet and write a 
			// message when we get here
	
	
			//DFP( 400, "PACKET ALREADY ON LIST" );
			//WaitTillKey();
		}
		
		// ----------

		// For each packet that comes in we start / restart the existence timer - it is fairly simple - when this timer expires the
		// message is destroyed. The timer start causes the timer to start its count down over.
		start_existence_timer( im );
		
		// 3/16/2012 rmd : Try to send a status. With the introduction of the broadcast TOKEN a
		// status may not actually be sent. Even when requested. But that is the correct behavior
		// and is handled deep in the function that builds the status.
		if( tdh->H.PID.request_for_status == 1 )
		{
			// Then this is a data packet with the send status flag set so build the status packet and
			// potentially send it.
			nm_nm_build_status_and_send_it( im );
		}
		else
		{
			// Every packet that comes in stops both the waiting for a status rqst packet timer and the
			// waiting for status response timer ... every time a packet comes in that is not a status
			// rqst we start the waiting for status rqst timer ... in the ideal world the last packet to
			// come in is the one that is requesting the status (that's the way they are sent) ... if
			// the order is bumped around in the medium used to move the packets (sophisticated radios
			// could do this) perhaps this scheme breaks down - BUT let's say the order stays intact ...
			// then the last packet to come in kills the timer and it ain't restarted unless the last
			// packet is missing ... and for the case of an active message that's what we want
			if( im->this_IM_is_active && im->permission_to_send_a_status )
			{
				// 3/14/2014 rmd : Being that the timer task is a high priority task using portMAX_DELAY
				// when posting is safe. Anything on the timer queue is quickly processed and therefore the
				// queue is effectively always empty.
				xTimerStart( im->timer_waiting_for_status_rqst, portMAX_DELAY );
			}
		}


		xSemaphoreGive( im->list_im_packets_list_MUTEX );

		xSemaphoreGive( list_tpl_in_messages_MUTEX );

	}  // if no error_detected

}

/* ---------------------------------------------------------- */
void TPL_IN_task( void *pvParameters )
{
	TPL_IN_EVENT_QUEUE_STRUCT	tiqs;

	// ----------
	
	// 10/23/2012 rmd : To support the WDT activity monitor.
	UNS_32	task_index;
	
	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// ----------

	memset( &IncomingStats, 0, sizeof( IncomingStats )	);

	// initialize the IN coming list of messages
	nm_ListInit( &IncomingMessages, offsetof( INCOMING_MESSAGE_STRUCT, dl ) );

	while( TRUE )
	{
		// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
		if( xQueueReceive( TPL_IN_event_queue, &tiqs, MS_to_TICKS( 500 ) ) == pdTRUE )
		{
			switch( tiqs.event )
			{
				case TPL_IN_EVENT_here_is_an_incoming_packet:

					add_packet_to_incoming_messages( tiqs.port, tiqs.dh );

					// And now release the memory the ring buffer packet hunter allocated when it found the
					// packet.
					mem_free( tiqs.dh.dptr );

					break;

				case TPL_IN_EVENT_the_existence_timer_expired:

					tpl_in_existence_timer_expired( tiqs.im );

					break ;
					
			}

		}  // of a sucessful queue item is available

		// ----------
		
		// 10/23/2012 rmd : Keep the activity indication going.
		task_last_execution_stamp[ task_index ] = xTaskGetTickCount();

		// ----------
		
	}  // of forever task loop

}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION The packet has previously been determined to be for this box. Take a peak
    in the header at the STATUS bit to decide if should be sent to the TPL_IN or TPL_OUT
    tasks. Note to keep in mind: be aware of the broadcast behaviour for the TOKEN messages
    - such packets may not be actually addressed to us - but they are for us!
	
	@CALLER_MUTEX_REQUIREMENTS (none)

    @MEMORY_RESPONSIBILITES This function makes a copy of the packet for the TPL_IN or
    TPL_OUT tasks. And the responsiblity to release the newly taken memory is transferred to
    those tasks. This function leaves the original packet memory intact and untouched. The
    caller is still responsible for its release.

    @EXECUTED Executed within the context of the RING_BUFFER_ANALYSIS_TASK. When incoming
    data is sensed and a packet is found.
	
	@RETURN (none)

	@ORIGINAL 2012.03.16 rmd

	@REVISIONS
*/
void TPL_IN_make_a_copy_and_direct_incoming_packet( UPORTS pport, DATA_HANDLE pdh )
{
	DATA_HANDLE				packet_copy;

	TPL_DATA_HEADER_TYPE	*tdh;

	// ----------

	tdh = (TPL_DATA_HEADER_TYPE*)pdh.dptr;
	
	if( tdh->H.PID.STATUS == 0 )
	{
		// ----------
		
		// 7/3/2013 rmd : DATA packet handling.
		
		// 3/19/2012 rmd : Make a copy as this function must return packet memory responsibility to
		// the caller. Not move that responsibility on.
		packet_copy.dptr = mem_malloc( pdh.dlen );

		packet_copy.dlen = pdh.dlen;
		
		memcpy( packet_copy.dptr, pdh.dptr, packet_copy.dlen );
		
		TPL_IN_EVENT_QUEUE_STRUCT	tiqs;
	
		// 3/16/2012 rmd : The packet memory release responsibility follows the packet to the TPL_IN
		// task.
		tiqs.event = TPL_IN_EVENT_here_is_an_incoming_packet;
		tiqs.dh = packet_copy;
		tiqs.port = pport;
		if( xQueueSend( TPL_IN_event_queue, &tiqs, 0 ) != pdPASS )
		{
			Alert_Message( "TPL_IN queue full" );  // TODO: include the task name within the message
		}
	}
	else
	{
		// ----------
		
		// 7/3/2013 rmd : STATUS packet handling.
		
		if ( tdh->H.PID.request_for_status == 1 )
		{
			// 3/16/2012 rmd : This bit has NEVER been intentionally set. Original design was with both
			// STATUS and SEND_ACK bit set would indicate the central is abandoning the communications
			// and to perhaps cancel any OM in process as well as any queued up to go out?
			ALERT_MESSAGE_WITH_FILE_NAME( "RCVD_DATA: UNEXP quit COMMS?" );
		}
		else
		{
			TPL_OUT_EVENT_QUEUE_STRUCT	toqs;

			// 3/19/2012 rmd : Make a copy as this function must return packet memory responsibility to
			// the caller. Not move that responsibility on.
			packet_copy.dptr = mem_malloc( pdh.dlen );
	
			packet_copy.dlen = pdh.dlen;
			
			memcpy( packet_copy.dptr, pdh.dptr, packet_copy.dlen );
			
			// 3/16/2012 rmd : The packet memory release responsibility follows the packet to the
			// TPL_OUT task. Received a status packet for OutgoingMessage list - doesn't need the port
			// info.
			toqs.event = TPL_OUT_QUEUE_COMMAND_here_is_an_incoming_status_packet;
			toqs.dh = packet_copy;
			if( xQueueSend( TPL_OUT_event_queue, &toqs, 0 ) != pdPASS )
			{
				ALERT_MESSAGE_WITH_FILE_NAME( "TPL_OUT queue full." );
			}
		}

	}  // of if not a status packet
	
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

