/*  file = cent_comm.c              2011.07.29  11/15/01  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"bithacks.h"

#include 	"budget_report_data.h"

#include	"cs_common.h"

#include	"cent_comm.h"

#include	"comm_mngr.h"

#include	"alert_parsing.h"

#include	"alerts.h"

#include	"epson_rx_8025sa.h"

#include	"change.h"

#include	"r_alerts.h"

#include	"app_startup.h"

#include	"controller_initiated.h"

#include	"configuration_network.h"

#include	"crc.h"

#include	"flash_storage.h"

#include	"flowsense.h"

#include	"packet_definitions.h"

#include	"cs_mem.h"

#include	"poc_report_data.h"

#include	"system_report_data.h"

#include	"lights_report_data.h"

#include	"foal_irri.h"

#include	"screen_utils.h"

#include	"e_factory_reset.h"

#include	"wdt_and_powerfail.h"

#include	"e_comm_test.h"

#include	"foal_lights.h"

#include	"weather_tables.h"

#include	"irri_comm.h"

#include	"dialog.h"

#include	"moisture_sensor_recorder.h"

#include	"code_distribution_task.h"




/* ---------------------------------------------------------- */
static BOOL_32 allowed_to_process_irrigation_commands_from_comm_server( void )
{
	// 3/27/2014 rmd : And we only allow irrigation commands to be acted upon when the system is
	// not in FORCED and up and running with a valid operating tpmicro present. This function
	// was created with the mobile app in mind! And possibly for other commands from the
	// comm_server.
	BOOL_32	rv;
	
	rv = ( tpmicro_is_available_for_token_generation() && (comm_mngr.mode == COMM_MNGR_MODE_operational) && !comm_mngr.chain_is_down );
	
	return( rv );
}
	
/* ---------------------------------------------------------- */
extern void CENT_COMM_build_and_send_ack_with_optional_job_number( const UNS_32 pport, const UNS_32 presponse_cmd, const BOOL_32 pinclude_job_number, const UNS_32 pjob_number )
{
	TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER	tcsuah;

	TO_COMMSERVER_MESSAGE_HEADER	tcsmh;

	UNS_32					maximum_response_size;
	
	DATA_HANDLE				ldh;

	UNS_8					*ucp;

	UNS_8					*start_of_crc;

	UNS_32					bytes_to_crc, length_reported_to_commserver;
	
	CRC_BASE_TYPE			lcrc;
	
	// ----------
	
	// 3/16/2017 rmd : Calculate packet size. Including if the packet is being generated at a
	// hub-based controller.
	maximum_response_size =	sizeof( AMBLE_TYPE ) +
							sizeof( TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER ) +
							sizeof( TO_COMMSERVER_MESSAGE_HEADER ) +
							sizeof( UNS_32 ) +
							sizeof( CRC_TYPE ) +
							sizeof( AMBLE_TYPE );

	ldh.dptr = mem_malloc( maximum_response_size );

	ucp = ldh.dptr;
	
	ldh.dlen = maximum_response_size;
	
	// 3/20/2017 rmd : The amount to CRC does not include the 2 ambles nor the CRC itself. And
	// leave out the hub portion. Will add in if included.
	bytes_to_crc = sizeof(TO_COMMSERVER_MESSAGE_HEADER) + sizeof(UNS_32);

	// 3/20/2017 rmd : And the length reported to the commserver in the 'to commserver message
	// header' in this case only includes the 'to commserver message header' size. This value
	// never includes the ambles nor the crc. And also we know the hub routing structure is
	// stripped from the message before sending to the commserver.
	length_reported_to_commserver  = sizeof(TO_COMMSERVER_MESSAGE_HEADER) + sizeof(UNS_32);
	
	// ----------
	
	if( !pinclude_job_number )
	{
		ldh.dlen -= sizeof(UNS_32);

		bytes_to_crc -= sizeof(UNS_32);

		length_reported_to_commserver -= sizeof(UNS_32);
	}

	// ----------
	
	*ucp++ = preamble._1;
	*ucp++ = preamble._2;
	*ucp++ = preamble._3;
	*ucp++ = preamble._4;

	// ----------

	start_of_crc = ucp;

	// ----------

	// 3/16/2017 rmd : So if this controller is a hub-based unit, include the 2 extra bytes to
	// enable this packet to be routed upon receipt by the hub. BUT, not if we are communicating
	// through the RRE port, cause that means we are talking DIRECTLY with the firmware upgrade
	// program and must not include this special header.
	if( CONFIG_this_controller_is_behind_a_hub() && (pport != UPORT_RRE) )
	{
		memset( &tcsuah, 0x00, sizeof(TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER) );

		tcsuah.PID.__routing_class = MSG_CLASS_2000e_CS3000_FLAG;

		tcsuah.cs3000_msg_class = MSG_CLASS_CS3000_TO_COMMSERVER_VIA_HUB;

		memcpy( ucp, &tcsuah, sizeof(TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER) );

		ucp += sizeof(TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER);

		// 3/20/2017 rmd : And update the bytes to crc. Since this is included in the message.
		bytes_to_crc += sizeof(TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER);
	}
	else
	{
		// 3/16/2017 rmd : Update the length to remove the hub-based portion that is not used.
		ldh.dlen -= sizeof( TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER );
	}
	
	// ----------
	
	memset( &tcsmh, 0x00, sizeof(TO_COMMSERVER_MESSAGE_HEADER) );

	tcsmh.from_serial_number = config_c.serial_number;

	tcsmh.from_network_id = NETWORK_CONFIG_get_network_id();

	tcsmh.length = length_reported_to_commserver;

	tcsmh.mid = presponse_cmd;

	memcpy( ucp, &tcsmh, sizeof(TO_COMMSERVER_MESSAGE_HEADER) );

	ucp += sizeof( TO_COMMSERVER_MESSAGE_HEADER );

	// ----------
	
	if( pinclude_job_number )
	{
		// 6/18/2015 rmd : Insert the special job number into the ACK so Jay can use it to clear the
		// correct blast command from the IHSFY_JOBS table at the commserver.
		memcpy( ucp, &pjob_number, sizeof(UNS_32) );
	
		ucp += sizeof(UNS_32);
	}
	
	// ----------
	
	// 11/6/2012 rmd : Calculate the CRC. Which is returned in BIG ENDIAN format. Which is what
	// we need (see note in crc calc function). Then copy to ucp.
	lcrc = CRC_calculate_32bit_big_endian( start_of_crc, bytes_to_crc );
	
	memcpy( ucp, &lcrc, sizeof(CRC_BASE_TYPE) );

	ucp += sizeof(CRC_BASE_TYPE);

	// ----------
	
	*ucp++ = postamble._1;
	*ucp++ = postamble._2;
	*ucp++ = postamble._3;
	*ucp   = postamble._4;

	// ----------
	
	// Packet is built. Send it!
	AddCopyOfBlockToXmitList( pport, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_PACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );

	// And free the memory used.
	mem_free( ldh.dptr );
}

/* ---------------------------------------------------------- */
static void terminate_ci_transaction( void )
{
	// 2/14/2017 rmd : DO NOT change the controller initiated STATE. We just received an ACK.
	// And are in the middle of a transaction. STATE should be the 'waiting for response' state.
	// Perform the housekeeping to end the transaction.
	CONTROLLER_INITIATED_post_event( CI_EVENT_wrap_up_this_transaction );
}

/* ---------------------------------------------------------- */
static void poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers( void )
{
	// 11/2/2017 rmd : There have been many occurances when the commserver sends us a message,
	// BUT we are still waiting for a response to the prior message (something went wrong
	// - and we have a MINIMUM 5 minute timeout). I think normally this isn't such a big deal,
	// but two scenarios have me concerned. One is a hub polling rate fster than the 5 minute
	// timeout. Once a message fails there is a particularly viciuous loop that seems to develop
	// that the controller doesn't seem to recover from (where new polls come in, but not
	// reacted to, until after the commserver has timed out the poll and ignores the response.
	// The other would be similar during radio remote, suppose a request for status failed
	// during that? It is very similar.
	//
	// So here I will throw a special kind of message timeout, that does not start the
	// connection sequence (that seems to be what is behind the visicious loop), but does wrap
	// up the prior message, and then process the new inbound message!
	//
	// And I've decided to do this for ALL controllers, hub related or otherwise.
	//
	// 4/26/2017 rmd : If the state isn't idle, then the state is 'waiting for response',
	// meaning we are waiting for an ACK to a message we sent to the commserver. I have thought
	// about using this inbound message to 'timeout' the waiting for an ACK. Hmmmm, I think we
	// should. It fits the overall hub scheme nicely. It means the commserver has given up on
	// the previous poll, and now enough time has elapsed that a new poll has been sent.
	if( cics.state == CI_STATE_waiting_for_response )
	{
		Alert_Message( "CI: rcvd new message, forcing prior message termination" );
		
		CONTROLLER_INITIATED_post_event( CI_EVENT_message_termination_due_to_new_inbound_message );
	}
	
	// ----------
	
	// 3/7/2017 rmd : Jay and I agreed that, for hubs and hub based controllers, following any
	// commserver initiated 'pseudo-poll' message we would send a 'no more messages' msg.
	if( CONFIG_this_controller_is_a_configured_hub() )
	{
		// 8/6/2018 rmd : This block is probably not needed here, as after a binary is received the
		// controller reboots, and when the code_distribution task is born, we clear these flags.
		// That is the real mechanism to suppressing code distribution, but this can't hurt, and
		// really makes sure we don't send the commserver the "hub busy" message.
		#if KILL_ANY_HUB_CODE_DISTRIBUTION_ATTEMPT
		
			weather_preserves.hub_needs_to_distribute_main_binary = (false);
			
			weather_preserves.hub_needs_to_distribute_tpmicro_binary = (false);
			
		#endif
		
		// ----------

		// 12/10/2018 rmd : If the HUB code distribution is underway, respond to the commserver with
		// a BUSY indication.
		if( CODE_DISTRIBUTION_hub_should_report_busy_to_the_commserver() )
		{
			// 5/25/2017 rmd : Post to the FRONT so is only message sent!
			CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_hub_is_busy_msg, NULL, CI_IF_PRESENT_REMOVE_AND_REPOST, CI_POST_TO_FRONT );
		}
		else
		{
			// 4/24/2017 rmd : We want to end up with ONE no more messages msg on the queue, at the end
			// of the queue.
			CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_no_more_messages_msg, NULL, CI_IF_PRESENT_REMOVE_AND_REPOST, CI_POST_TO_BACK );
		}
		
		// 3/7/2017 rmd : And POST to the regular CI task queue the event to cause us to build and
		// send the messages on the queue.
		CONTROLLER_INITIATED_post_event( CI_EVENT_build_and_send_the_next_queued_msg );
	}
	else
	if( CONFIG_this_controller_is_behind_a_hub() )
	{
		// 4/24/2017 rmd : We want to end up with ONE no more messages msg on the queue, at the end
		// of the queue.
		CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_no_more_messages_msg, NULL, CI_IF_PRESENT_REMOVE_AND_REPOST, CI_POST_TO_BACK );
		
		// 3/7/2017 rmd : And POST to the regular CI task queue the event to cause us to build and
		// send the messages on the queue.
		CONTROLLER_INITIATED_post_event( CI_EVENT_build_and_send_the_next_queued_msg );
	}
}

/* ---------------------------------------------------------- */
static void build_response_to_mobile_cmd_and_update_session_data( const UNS_32 presponse_cmd, const UNS_32 pjob_number )
{
	CONTROLLER_INITIATED_TASK_QUEUE_STRUCT	cieqs;

	// ----------
	
	// 9/30/2015 ajv : Build and send the ACK
	CENT_COMM_build_and_send_ack_with_optional_job_number( UPORT_A, presponse_cmd, JOB_NUMBER_INCLUDED, pjob_number );
	
	// ----------

	// 9/30/2015 ajv : Flag a new command just came in to keep the status rate up.
	CONTROLLER_INITIATED_post_event( CI_EVENT_zero_mobile_session_seconds_count );

	// ----------

	// 9/30/2015 ajv : And start the timer to send the status screen. In the maintenance
	// function where the station is actually turned ON we will send a status to show it ON.
	// Make this time 15 seconds or so after that for the routine status update series that
	// follows any new command.
	cieqs.event = CI_EVENT_restart_mobile_status_timer;

	cieqs.how_long_ms = CI_SEND_STATUS_MS_routine_update;

	CONTROLLER_INITIATED_post_event_with_details( &cieqs );
	
	// ----------
	
	// 3/7/2017 rmd : Jay and I agreed that, for hubs and hub based controllers, following any
	// commserver initiated 'pseudo-poll' message we would send a 'no more messages' msg. This
	// message falls into that category being an unsolicited messages from the commserver.
	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
}

/* ---------------------------------------------------------- */
static void update_when_last_communicated( void )
{
	// 2/20/2015 rmd : This function used to update a variable stored in the
	// NETWORK_CONFIGURATION file. We did away with that variable when we moved to perpetual
	// connections. But I left the stub in place for future use if ever needed.
}

/* ---------------------------------------------------------- */
static void process_registration_data_ack( UNS_32 pmid, UNS_32 const pto_network_id )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	// 10/9/2013 rmd : If we were not waiting for this response make an alert. We could force
	// the disconnect but I think we'll wait for the response_timeout timer to expire.
	
	// 8/15/2014 rmd : Note - do not reset the waiting_for_xxx flag here. That is the
	// responsibility of the CI task before next outbound message is kicked out.

	if( !cics.waiting_for_registration_response )
	{
		// 8/15/2014 rmd : Take no action here. Only alert and wait for the response timer to
		// expire.
		Alert_Message( "Not expecting a registration ACK" );
	}
	else
	{
		// 11/20/2015 rmd : Only if the ack mid. Remember the NAK's pass through here too.
		if( pmid == MID_FROM_COMMSERVER_REGISTRATION_DATA_full_receipt_ack )
		{
			// 8/15/2014 rmd : Clear this  flag but NOT the pending flag. That flag is used to capture
			// any requests to send the registration made mid-session. This is different from the other
			// controller initiated datas sent because we don't have a timer running this one.
			battery_backed_general_use.need_to_send_registration = (false);
			
			// ----------
	
			// 9/4/2015 rmd : Clear this flag. It has been used once. And that is the intention.
			weather_preserves.factory_reset_registration_to_tell_commserver_to_clear_pdata = (false);
			
			// ----------
			
			// 12/9/2013 rmd : So if our network ID is different from the one we received, change it? If
			// the current network ID is ZERO ... then yes change it. If non-zero do what? I guess for
			// now if non-zero we'll still accept it and change it (maybe useful during test). But
			// sooner or later that should change I suspect. So for now NO ZERO TEST!
			if( pto_network_id != NETWORK_CONFIG_get_network_id() )
			{
				lchange_bitfield_to_set = NETWORK_CONFIG_get_change_bits_ptr( CHANGE_REASON_CENTRAL_OR_MOBILE );
				
				NETWORK_CONFIG_set_network_id( pto_network_id,
											   CHANGE_generate_change_line,
											   CHANGE_REASON_CENTRAL_OR_MOBILE,
											   FLOWSENSE_get_controller_index(),
											   CHANGE_set_change_bits,
											   lchange_bitfield_to_set );
											   
				// 9/8/2015 rmd : NOTE - the file save associated with pdata changes is normally triggered
				// when a communication message containing the change (the bit field with the packed raw
				// field data) is pulled apart [in the "extract and store changes" function]. This network
				// ID change is NOT implemented that way. We side step the normal route and because of that
				// the file save must be discretly initiated here. This was an oversite for quite some time.
				FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_NETWORK_CONFIGURATION, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
			}

			// ----------
			
			// 5/8/2017 rmd : To combat a rather viscious affair, where if the registration is NAK'd, we
			// would proceed, and try to send the registration again, and it again would be NAK'd, and
			// so on...never ending, rapid fire registration attempts. So in that case, of a NAK, we
			// just won't do anything and let the 5 minute timer expire. Which yes will result in us
			// performing the connection sequence, and trying the registration again in 5 minutes, but
			// that is better than rapid fire registrations, hundreds of them! So only wrap up the
			// transaction if successful!
			terminate_ci_transaction();
		}
		
	}
}

/* ---------------------------------------------------------- */
static void process_engineering_alerts_ack( UNS_32 pmid )
{
	// 10/9/2013 rmd : If we were not waiting for this response make an alert. We could force
	// the disconnect but I think we'll wait for the response_timeout timer to expire.
	
	// 8/15/2014 rmd : Note - do not reset the waiting_for_xxx flag here. That is the
	// responsibility of the CI task before next outbound message is kicked out.

	if( !cics.waiting_for_alerts_response )
	{
		// 8/15/2014 rmd : Take no action here. Only alert and wait for the response timer to
		// expire.
		Alert_Message( "Not expecting an alerts ACK or NAK." );
	}
	else
	{
		// 11/20/2015 rmd : Only if the ack mid. Remember the NAK's pass through here too.
		if( pmid == MID_FROM_COMMSERVER_ENGINEERING_ALERTS_full_receipt_ack )
		{
			// 10/8/2013 rmd : So the comm_server has acknowledged. We need to update the alerts first
			// byte to send for the next sending of alerts. Take the MUTEX of course to assure the pile
			// remains frozen. Taking the MUTEX also prevents us from performing this operation in the
			// midst of the controller initiated task attempting to send alerts again.
			xSemaphoreTakeRecursive( alerts_pile_recursive_MUTEX, portMAX_DELAY );
			
			alerts_struct_engineering.first_to_send = alerts_struct_engineering.pending_first_to_send;
	
			xSemaphoreGiveRecursive( alerts_pile_recursive_MUTEX );
		}

		// ----------

		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
static void process_flow_recording_ack( UNS_32 pmid )
{
	// 10/9/2013 rmd : If we were not waiting for this response make an alert. We could force
	// the disconnect but I think we'll wait for the response_timeout timer to expire.
	
	// 8/15/2014 rmd : Note - do not reset the waiting_for_xxx flag here. That is the
	// responsibility of the CI task before next outbound message is kicked out.

	if( !cics.waiting_for_flow_recording_response )
	{
		// 8/15/2014 rmd : Take no action here. Only alert and wait for the response timer to
		// expire.
		Alert_Message( "Not expecting a flow recording ACK." );
	}
	else
	{
		// 11/20/2015 rmd : Only if the ack mid. Remember the NAK's pass through here too.
		if( pmid == MID_FROM_COMMSERVER_FLOW_RECORDING_full_receipt_ack )
		{
			// 10/8/2013 rmd : So the comm_server has acknowledged. We need to update the flow recording
			// first to send pointer. Take the MUTEX of course to assure the array remains frozen.
			// Taking the MUTEX also prevents us from performing this operation in the midst of the
			// controller initiated task attempting to send the flow recording lines again.
			xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
			
			if( cics.current_msg_frcs_ptr != NULL )
			{
				if( cics.current_msg_frcs_ptr->pending_first_to_send_in_use )
				{
					cics.current_msg_frcs_ptr->first_to_send = cics.current_msg_frcs_ptr->pending_first_to_send;
	
					// 10/2/2014 rmd : The first_to_send variable does not need to be saved as it is in
					// battery_backed SRAM and therefore already non-volatile. Which is irrelevant because the
					// flow recording records themsleves are held in volatile SDRAM and are lost with a power
					// failure.
				}
				else
				{
					Alert_Message( "CI Flow Recording: pending_first not in use!" );
				}
			}
			else
			{
				Alert_Message( "CI Flow Recording: current_msg ptr is NULL!" );
			}
			
			xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
		}

		// ----------

		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
static void process_moisture_sensor_recording_ack( UNS_32 pmid )
{
	// 10/9/2013 rmd : If we were not waiting for this response make an alert. We could force
	// the disconnect but I think we'll wait for the response_timeout timer to expire.
	
	// 8/15/2014 rmd : Note - do not reset the waiting_for_xxx flag here. That is the
	// responsibility of the CI task before next outbound message is kicked out.

	if( !cics.waiting_for_moisture_sensor_recording_response )
	{
		// 8/15/2014 rmd : Take no action here. Only alert and wait for the response timer to
		// expire.
		Alert_Message( "Not expecting a moisture recording ACK." );
	}
	else
	{
		// 11/20/2015 rmd : Only if the ack mid. Remember the NAK's pass through here too.
		if( pmid == MID_FROM_COMMSERVER_MOISTURE_SENSOR_RECORDER_full_receipt_ack )
		{
			// 10/8/2013 rmd : So the comm_server has acknowledged. We need to update the moisture
			// sensor recording control structure so take the MUTEX. Taking the MUTEX also prevents us
			// from performing this operation in the midst of the controller initiated task attempting
			// to send the recording lines again.
			xSemaphoreTakeRecursive( moisture_sensor_recorder_recursive_MUTEX, portMAX_DELAY );
			
			if( msrcs.pending_first_to_send_in_use )
			{
				msrcs.first_to_send = msrcs.pending_first_to_send;

				// 2/18/2016 rmd : The first to send variable is held in volatile SDRAM and is vulnerable to
				// loss with a power fail. But that is okay cause the moisture records themselves are also
				// held in volatile SDRAM and lost with a power failure.
			}
			else
			{
				Alert_Message( "CI Moisture Recording: pending_first not in use!" );
			}
			
			xSemaphoreGiveRecursive( moisture_sensor_recorder_recursive_MUTEX );
		}

		// ----------

		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
static void process_incoming_request_to_check_for_updates( const DATA_HANDLE pdh )
{
	// 3/2/2017 rmd : When considering hub based controllers this is treated as an incoming 
	// 'poll' type message. In that it is giving this controller permission to speak. And as
	// such we should queue up a 'no more messages' msg.
	//
	// 3/2/2017 rmd : There is a restriction that falls out of the specification for updating
	// code on a hub. This inbound message should not be sent, by the commserver, to controllers
	// ON the hub. Should ONLY be sent to the actual hub by the commserver. So test for that.
	if( CONFIG_this_controller_is_behind_a_hub() )
	{
		Alert_Message( "Should not receive a check for updates request" );
	}
	else
	{
		// 3/2/2017 rmd : ACK the commserver. Expected by the commserver.
		CENT_COMM_build_and_send_ack_with_optional_job_number( UPORT_A, MID_TO_COMMSERVER_check_for_updates_ack, JOB_NUMBER_NOT_INCLUDED, 0 );

		// ----------
		
		// 4/24/2017 rmd : Post the request to the queue. I think this incoming command can only be
		// initiated via the web app UI, 'check for updates' under engineering. Have no reason to
		// add to the front of the queue, so just add to the back.
		CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_check_for_updates, NULL, CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD, CI_POST_TO_BACK );
		
		// 4/24/2017 rmd : And since this is a pseudo poll message, wrap up appropriately.
		poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
	}
}

/* ---------------------------------------------------------- */
static void process_check_for_updates_ack( const UNS_32 pmessage_id )
{
	// 5/7/2014 rmd : This function executed within the context of the RING_BUFFER_ANALYSIS
	// task.
	
	// 8/15/2014 rmd : Note - do not reset the waiting_for_xxx flag here. That is the
	// responsibility of the CI task before next outbound message is kicked out.

	// ----------
	
	CI_MSGS_TO_SEND_QUEUE_STRUCT	mtsqs;
	
	BOOL_32		restart_needed;
	
	UNS_32		restart_flag;
	
	BOOL_32		an_update_is_coming;

	// ----------
	
	restart_needed = (false);
	
	restart_flag = 0;
	
	an_update_is_coming = (false);
	
	// ----------
	
	if( !cics.waiting_for_check_for_updates_response )
	{
		// 8/15/2014 rmd : Take no action here. Only alert and wait for the response timer to
		// expire.
		Alert_Message( "Not expecting a check for updates ACK." );
	}
	else
	{
		if( CONFIG_this_controller_is_behind_a_hub() )
		{
			Alert_Message( "On a HUB: should not receive a check for updates ACK" );
		}
		else
		{
			/*
			case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates:
			case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_both:
			case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_main:
			case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_tpmicro:
			case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_one_update_but_distribute_both:
			case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_one_update_available:
			case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_two_updates_available:
			*/

			// 3/2/2017 rmd : There is a marked difference between how the hub behaves and how non-hub
			// based controllers behave. So separate here.
			if( CONFIG_this_controller_is_a_configured_hub() )
			{
				// 3/3/2017 rmd : So in the case of the hub we are going to set a flag which is read upon
				// the restart so we know what code distribution effort the hub is supposed to perform.
				// Alternatively if there is no update work to do (response said 'no updates') then we have
				// no work to do here except terminate the transaction using the normal
				// 'terminate_ci_transaction' function.
				
				// ----------
				
				// 3/6/2017 rmd : Marker that we don't know yet what binary(s) to distribute. This is a
				// 'magic number' flag when set to a zero. Interpreted by the comm_mngr task after the file
				// write completes.
				cdcs.restart_info_flag_for_hub_code_distribution = 0;

				// 3/3/2017 rmd : If there is distribution work to be done we must tell the hub about it
				// here. So that following the reboot the hub knows what binary distribution work is
				// needed.
				if( pmessage_id == MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_tpmicro )
				{
					restart_needed = (true);
					
					restart_flag = SYSTEM_SHUTDOWN_EVENT_hub_to_distribute_tpmicro_binary;
				}
				else
				if( pmessage_id == MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_main )
				{
					restart_needed = (true);
					
					restart_flag = SYSTEM_SHUTDOWN_EVENT_hub_to_distribute_main_binary;
				}
				else
				if( pmessage_id == MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_both )
				{
					restart_needed = (true);
					
					restart_flag = SYSTEM_SHUTDOWN_EVENT_hub_to_distribute_both_binaries;
				}
				else
				if( pmessage_id == MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_one_update_but_distribute_both )
				{
					an_update_is_coming = (true);
					
					// 3/6/2017 rmd : A binary is coming. We don't know which one but that is okay because we
					// are going to use the hub to distribute both anyway. So set the flag here that says we
					// have to distribute both. After we receive the binary and do the restart this flag will
					// get passed along during the system restart event.
					cdcs.restart_info_flag_for_hub_code_distribution = SYSTEM_SHUTDOWN_EVENT_hub_to_distribute_both_binaries;
				}
				else
				if( pmessage_id == MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_one_update_available )
				{
					Alert_Message( "is a HUB: one update coming" );

					an_update_is_coming = (true);
					
					// 10/13/2017 rmd : But DON'T set the restart_info flag, it is set after the binary is
					// written. This HAS to be this (ugly) way BECAUSE WE DON"T KNOW WHICH BINARY IS COMING!
				}
				else
				if( pmessage_id == MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_two_updates_available )
				{
					Alert_Message( "is a HUB: two updates coming" );

					an_update_is_coming = (true);
					
					// 3/6/2017 rmd : Both binaries are coming. Set us up to distribute both following the
					// restart.
					cdcs.restart_info_flag_for_hub_code_distribution = SYSTEM_SHUTDOWN_EVENT_hub_to_distribute_both_binaries;
				}
			}
			else
			{
				// 3/3/2017 rmd : There are 4 responses from the comm server that non-hub controllers should
				// NEVER see. These are hub related only, so alert if they show.
				if( (pmessage_id == MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_tpmicro) || 
					(pmessage_id == MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_main) ||
					(pmessage_id == MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_both) ||
					(pmessage_id == MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_one_update_but_distribute_both) )
				{
					Alert_Message_va( "Not a HUB: %u should not receive this ACK", pmessage_id );
					
					// 3/3/2017 rmd : And I guess we'll let it process through, attempting to set the reboot
					// after tpmicro variable.
				}

				// ----------

				if( pmessage_id != MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates )
				{
					an_update_is_coming = (true);
				}
				
				// 10/13/2017 rmd : For the NON-HUB controllers, the restart flag is not actually important.
				// It does not control what is to be distributed (a flowsense distribution) following the
				// restart.
			}

			// ----------
			
			// 3/6/2017 rmd : We have a flag that controls when the controller restarts after receiving
			// a binary. We always reboot after receiving the main board binary. The issue becomes if we
			// are sending just the tpmicro binary, and NOT the main board binary, we want to reboot at
			// the conclusion of the tpmicro receipt. This flag, normally set to (true), controls that.
			// This applies to both hub and non-hub controllers.
			cdcs.restart_after_receiving_tp_micro_firmware = (true); 
			
			if( pmessage_id == MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_two_updates_available )
			{
				cdcs.restart_after_receiving_tp_micro_firmware = (false);
			}
			
			// ----------
			
			// 3/3/2017 rmd : For either the hub or regular non-hub controller, IF AN UPDATE IS COMING
			// we want to wipe the queue as a precautionary measure so we do not pepper the commserver
			// with additional outbound messages WHILE THE COMMSERVER IS SENDING US THE BINARY. That
			// could become problematic considering the reboot following receipt of the binary.
			if( an_update_is_coming )
			{
				// 3/3/2017 rmd : Empty the queue.
				//
				// 10/10/2017 rmd : ADDITIONALLY another IMPORTANT reason to wipe the queue is, on a HUB, to
				// wipe the NO MORE MESSAGES message so we do not send it to the commserver. We want the
				// commserver to remain in a POLL with the HUB while it is receving the binarie(s). So DO
				// NOT send the end of poll here otherwise we set loose the commserver to think it can poll
				// another (LR) controller in the city.
				while( xQueueReceive( cics.msgs_to_send_queue, &mtsqs, 0 ) );
			}
			
			// ----------
			
			// 9/28/2017 rmd : Now for all controllers kick the CI task ahead.
			//
			// For HUBS this will set the stage for the no more messages message to be sent, but we need
			// time for the CI task to run (if no binary is coming) hence the delay you see below if
			// performing an immediate reboot. If a binary is coming, during the very beginning of its
			// arrival the no more messages message will be sent.
			//
			// For all regular non-hub controllers (remember controllers behind a hub do not receive nor
			// process this ACK message) wrap up the transaction. If a binary is coming, remember we
			// wiped the queue so no more CI messages will be sent.
			terminate_ci_transaction();
			
			// ----------

			// 9/28/2017 rmd : For HUBS, for the case when no binary is coming yet the hub needs to
			// perform a hub code distribution.
			if( restart_needed )
			{
				// 9/28/2017 rmd : This is for HUBS only. Only a HUB can set the restart needed flag, and in
				// this case that a binary is not coming we still need time to send the no more messages
				// message, so make that time here. Just BEFORE the restart. I picked 4 seconds as a
				// generous amount of time for the end of poll to be sent.
				vTaskDelay( MS_to_TICKS( 4000 ) );
				
				// ----------
				
				SYSTEM_application_requested_restart( restart_flag );
			}
			
		}  // of making sure controller is not ON a hub

	}  // of not waiting for a check for updates ack

}

/* ---------------------------------------------------------- */
static void process_station_history_ack( UNS_32 pmid )
{
	// 5/7/2014 rmd : This function executed within the context of the comm_mngr.
	
	// 10/9/2013 rmd : If we were not waiting for this response make an alert. We could force
	// the disconnect but I think we'll wait for the response_timeout timer to expire.
	
	// 8/15/2014 rmd : Note - do not reset the waiting_for_xxx flag here. That is the
	// responsibility of the CI task before next outbound message is kicked out.

	if( !cics.waiting_for_station_history_response )
	{
		// 8/15/2014 rmd : Take no action here. Only alert and wait for the response timer to
		// expire.
		Alert_Message( "Not expecting a station history ACK." );
	}
	else
	{
		// 11/20/2015 rmd : Only if the ack mid. Remember the NAK's pass through here too.
		if( pmid == MID_FROM_COMMSERVER_STATION_HISTORY_full_receipt_ack )
		{
			// 8/15/2014 rmd : Make sure the station history array stays stable.
			xSemaphoreTakeRecursive( station_history_completed_records_recursive_MUTEX, portMAX_DELAY );
	
			if( station_history_completed.rdfb.pending_first_to_send_in_use )
			{
				// 8/15/2014 rmd : Update the records that need to be sent index. And save the state to the
				// file system. In this case these control variables are held within the STATION_HISTORY
				// structure and need to be saved to the file in order to save them through a power
				// failure/restart.
				station_history_completed.rdfb.first_to_send = station_history_completed.rdfb.pending_first_to_send;
				
				// ----------
				
				// 8/11/2015 rmd : In order to manage the number of station history file writes start this
				// timer with a long 2 hour delay. And only start it if not already running. Which I think
				// normally it will be because it was started when the record closed. With the potential
				// long gap in the file save (1 hour because the record send timer is 1 hour and the fle
				// save timer is 2 hours so at this point we are half way through) we are open to a power
				// failure that then would cause records to be resent. But that is the trade off to managing
				// the number of station history file writes.
				FLASH_STORAGE_if_not_running_start_file_save_timer( FF_STATION_HISTORY, STATION_HISTORY_FILE_WRITE_DELAY_SECONDS );
			}
			else
			{
				Alert_Message( "CI Station History: pending_first not in use!" );
			}
	
			xSemaphoreGiveRecursive( station_history_completed_records_recursive_MUTEX );
		}
		
		// ----------

		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
static void process_station_report_data_ack( UNS_32 pmid )
{
	// 10/9/2013 rmd : If we were not waiting for this response alert. No need to reset flag.
	// That is the responsibility of the CI task before next outbound message is kicked out.
	if( !cics.waiting_for_station_report_data_response )
	{
		Alert_Message( "Not expecting a station report data ACK." );
	}
	else
	{
		// 11/20/2015 rmd : Only if the ack mid. Remember the NAK's pass through here too.
		if( pmid == MID_FROM_COMMSERVER_STATION_REPORT_DATA_full_receipt_ack )
		{
			// 8/15/2014 rmd : Make sure the station report data array stays stable.
			xSemaphoreTakeRecursive( station_report_data_completed_records_recursive_MUTEX, portMAX_DELAY );
	
			if( station_report_data_completed.rdfb.pending_first_to_send_in_use )
			{
				// 8/15/2014 rmd : Update the records that need to be sent index. And save the state to the
				// file system. In this case these control variables are held within the STATION_REPORT_DATA
				// structure and need to be saved to the file in order to save them through a power
				// failure/restart.
				station_report_data_completed.rdfb.first_to_send = station_report_data_completed.rdfb.pending_first_to_send;
				
				// 8/27/2014 rmd : No particular reason for the 2 second delay.
				FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_STATION_REPORT_DATA, FLASH_STORAGE_seconds_to_delay_file_save_after_sending_report_data );
			}
			else
			{
				Alert_Message( "CI Station Report Data: pending_first not in use!" );
			}
	
			xSemaphoreGiveRecursive( station_report_data_completed_records_recursive_MUTEX );
		}
		
		// ----------

		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
static void process_poc_report_data_ack( UNS_32 pmid )
{
	// 10/9/2013 rmd : If we were not waiting for this response alert. No need to reset flag.
	// That is the responsibility of the CI task before next outbound message is kicked out.
	if( !cics.waiting_for_poc_report_data_response )
	{
		Alert_Message( "Not expecting a POC report data ACK." );
	}
	else
	{
		// 11/20/2015 rmd : Only if the ack mid. Remember the NAK's pass through here too.
		if( pmid == MID_FROM_COMMSERVER_POC_REPORT_DATA_full_receipt_ack )
		{
			// 10/2/2014 rmd : Make sure the poc report data stays stable.
			xSemaphoreTakeRecursive( poc_report_completed_records_recursive_MUTEX, portMAX_DELAY );
	
			if( poc_report_data_completed.rdfb.pending_first_to_send_in_use )
			{
				// 10/2/2014 rmd : Update the records that need to be sent index. And save the state to the
				// file system. In this case these control variables are held within the POC_REPORT_DATA
				// structure and need to be saved to the file in order to save them through a power
				// failure/restart.
				poc_report_data_completed.rdfb.first_to_send = poc_report_data_completed.rdfb.pending_first_to_send;
				
				// 10/2/2014 rmd : No particular reason for the 2 second delay.
				FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_POC_REPORT_DATA, FLASH_STORAGE_seconds_to_delay_file_save_after_sending_report_data );
			}
			else
			{
				Alert_Message( "CI POC Report Data: pending_first not in use!" );
			}
	
			xSemaphoreGiveRecursive( poc_report_completed_records_recursive_MUTEX );
		}

		// ----------

		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
static void process_system_report_data_ack( UNS_32 pmid )
{
	// 10/9/2013 rmd : If we were not waiting for this response alert. No need to reset flag.
	// That is the responsibility of the CI task before next outbound message is kicked out.
	if( !cics.waiting_for_system_report_data_response )
	{
		Alert_Message( "Not expecting a SYSTEM report data ACK." );
	}
	else
	{
		// 11/20/2015 rmd : Only if the ack mid. Remember the NAK's pass through here too.
		if( pmid == MID_FROM_COMMSERVER_SYSTEM_REPORT_DATA_full_receipt_ack )
		{
			// 10/2/2014 rmd : Make sure the system report data stays stable.
			xSemaphoreTakeRecursive( system_report_completed_records_recursive_MUTEX, portMAX_DELAY );
	
			if( system_report_data_completed.rdfb.pending_first_to_send_in_use )
			{
				// 10/2/2014 rmd : Update the records that need to be sent index. And save the state to the
				// file system. In this case these control variables are held within the POC_REPORT_DATA
				// structure and need to be saved to the file in order to save them through a power
				// failure/restart.
				system_report_data_completed.rdfb.first_to_send = system_report_data_completed.rdfb.pending_first_to_send;
				
				// 10/2/2014 rmd : No particular reason for the 2 second delay.
				FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_SYSTEM_REPORT_DATA, FLASH_STORAGE_seconds_to_delay_file_save_after_sending_report_data );
			}
			else
			{
				Alert_Message( "CI SYSTEM Report Data: pending_first not in use!" );
			}
	
			xSemaphoreGiveRecursive( system_report_completed_records_recursive_MUTEX );
		}

		// ----------

		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
static void process_lights_report_data_ack( UNS_32 pmid )
{
	// 10/9/2013 rmd : If we were not waiting for this response alert. No need to reset flag.
	// That is the responsibility of the CI task before next outbound message is kicked out.
	if( !cics.waiting_for_lights_report_data_response )
	{
		Alert_Message( "Not expecting a lights report data ACK." );
	}
	else
	{
		// 11/20/2015 rmd : Only if the ack mid. Remember the NAK's pass through here too.
		if( pmid == MID_FROM_COMMSERVER_LIGHTS_REPORT_DATA_full_receipt_ack )
		{
			// 8/15/2014 rmd : Make sure the station report data array stays stable.
			xSemaphoreTakeRecursive( lights_report_completed_records_recursive_MUTEX, portMAX_DELAY );
	
			if( lights_report_data_completed.rdfb.pending_first_to_send_in_use )
			{
				// 8/15/2014 rmd : Update the records that need to be sent index. And save the state to the
				// file system. In this case these control variables are held within the STATION_REPORT_DATA
				// structure and need to be saved to the file in order to save them through a power
				// failure/restart.
				lights_report_data_completed.rdfb.first_to_send = lights_report_data_completed.rdfb.pending_first_to_send;
				
				// 8/27/2014 rmd : No particular reason for the 2 second delay.
				FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_LIGHTS_REPORT_DATA, FLASH_STORAGE_seconds_to_delay_file_save_after_sending_report_data );
			}
			else
			{
				Alert_Message( "CI Station Report Data: pending_first not in use!" );
			}
	
			xSemaphoreGiveRecursive( lights_report_completed_records_recursive_MUTEX );
		}

		// ----------

		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
static void process_budget_report_data_ack( UNS_32 pmid )
{
	// 10/9/2013 rmd : If we were not waiting for this response alert. No need to reset flag.
	// That is the responsibility of the CI task before next outbound message is kicked out.
	if( !cics.waiting_for_budget_report_data_response )
	{
		Alert_Message( "Not expecting a budget report data ACK." );
	}
	else
	{
		// 11/20/2015 rmd : Only if the ack mid. Remember the NAK's pass through here too.
		if( (pmid == MID_FROM_COMMSERVER_BUDGET_REPORT_DATA_full_receipt_ack) ||
			(pmid == MID_FROM_COMMSERVER_BUDGET_REPORT_DATA_full_receipt_ack2) )
		{
			// 8/15/2014 rmd : Make sure the station report data array stays stable.
			xSemaphoreTakeRecursive( budget_report_completed_records_recursive_MUTEX, portMAX_DELAY );
	
			if( budget_report_data_completed.rdfb.pending_first_to_send_in_use )
			{
				// 8/15/2014 rmd : Update the records that need to be sent index. And save the state to the
				// file system. In this case these control variables are held within the STATION_REPORT_DATA
				// structure and need to be saved to the file in order to save them through a power
				// failure/restart.
				budget_report_data_completed.rdfb.first_to_send = budget_report_data_completed.rdfb.pending_first_to_send;
				
				// 8/27/2014 rmd : No particular reason for the 2 second delay.
				FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_BUDGET_REPORT_DATA, FLASH_STORAGE_seconds_to_delay_file_save_after_sending_report_data );
			}
			else
			{
				Alert_Message( "CI Budget Report Data: pending_first not in use!" );
			}
	
			xSemaphoreGiveRecursive( budget_report_completed_records_recursive_MUTEX );
		}

		// ----------

		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
static void process_et_rain_tables_ack( UNS_32 pmid )
{
	// 10/9/2013 rmd : If we were not waiting for this response alert. No need to reset flag.
	// That is the responsibility of the CI task before next outbound message is kicked out.
	if( !cics.waiting_for_et_rain_tables_response )
	{
		Alert_Message( "Not expecting a WEATHER_TABLES ACK." );
	}
	else
	{
		// 11/20/2015 rmd : Only if the ack mid. Remember the NAK's pass through here too.
		if( pmid == MID_FROM_COMMSERVER_ET_AND_RAIN_TABLE_full_receipt_ack )
		{
			// 10/2/2014 rmd : Make sure the structure stays stable.
			xSemaphoreTakeRecursive( weather_tables_recursive_MUTEX, portMAX_DELAY );
	
			if( weather_tables.pending_records_to_send_in_use )
			{
				// 10/2/2014 rmd : Update the records that need to be sent index. And save the state to the
				// file system since that is where it is stored and how it survives through a power
				// failure.
				xSemaphoreTakeRecursive( weather_tables_recursive_MUTEX, portMAX_DELAY );
			
				// 10/29/2015 rmd : By definition if we rcvd an ACK we have sent all that needs to be sent
				// so the new number to send is ALWAYS set to 0.
				weather_tables.records_to_send = 0;
				
				xSemaphoreGiveRecursive( weather_tables_recursive_MUTEX );
				
				// ----------
				
				// 11/2/2015 rmd : Save the records_to_send variable. No particular reason for the 2 second
				// delay.
				FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_WEATHER_TABLES, FLASH_STORAGE_seconds_to_delay_file_save_after_sending_report_data );
			}
			else
			{
				Alert_Message( "CI Weather Tables: pending_first not in use!" );
			}
	
			xSemaphoreGiveRecursive( weather_tables_recursive_MUTEX );
		}

		// ----------

		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
static void process_from_commserver_weather_data_receipt_ack( void )
{
	// 10/9/2013 rmd : If we were not waiting for this response make an alert. No need to reset
	// flag. That is the responsibility of the CI task before next outbound message is kicked
	// out.
	if( !cics.waiting_for_weather_data_receipt_response )
	{
		Alert_Message( "Not expecting a weather data ACK." );
	}
	else
	{
		// 12/1/2015 rmd : Remember both the receipt_ACK and the NAK pass through here. Processing
		// is the same for either case.
		
		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
static void process_incoming_weather_data_message( const DATA_HANDLE pdh )
{
	UNS_8		*ucp;

	UNS_32		lbox_index_0;

	UNS_8		whats_included;

	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	lchange_bitfield_to_set = NULL;

	ET_TABLE_ENTRY		et_data, original_et_table_data;

	RAIN_TABLE_ENTRY	rain_data, original_rain_table_data;

	// 3/21/2018 ajv : These are no longer used since we commented out the redundant alerts 
	// related to the receipt of an ET or rain value. 
	// const volatile float TEN_THOUSAND_POINT_O = 10000.0;
	// const volatile float ONE_HUNDRED_POINT_O = 100.0;

	// ----------

	// 6/11/2015 rmd : Send the ACK to the commserver.
	Alert_comm_command_sent( MID_TO_COMMSERVER_WEATHER_DATA_receipt_ack );
	
	CENT_COMM_build_and_send_ack_with_optional_job_number( UPORT_A, MID_TO_COMMSERVER_WEATHER_DATA_receipt_ack, JOB_NUMBER_NOT_INCLUDED, 0 );
	
	// ----------

	lbox_index_0 = FLOWSENSE_get_controller_index();

	// ----------
	
	// 5/27/2015 rmd : The data handle points to the packet start minus the preamble. So skip
	// over the top header.
	ucp = pdh.dptr;
	
	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );
	
	// ----------

	memcpy( &whats_included, ucp, sizeof(UNS_8) );
	ucp += sizeof(UNS_8);

	// ----------
	
	// 9/10/2014 ajv : Extract and store the ET if present.
	if( B_IS_SET( whats_included, WEATHER_DATA_HAS_ET ) )
	{
		memcpy( &et_data, ucp, sizeof(ET_TABLE_ENTRY) );
		ucp += sizeof(ET_TABLE_ENTRY);

		// ----------
		
		// 3/21/2018 ajv : Redundant as the changeline below triggers the creation of a formal 
		// change line. Likely left over from debugging. 
		// Alert_Message_va( "Rcvd ET: %4.2f, status %d", (et_data.et_inches_u16_10000u/TEN_THOUSAND_POINT_O), et_data.status ); 
		
		// ----------
		
		WEATHER_TABLES_get_et_table_entry_for_index( 1, &original_et_table_data );

		Alert_ChangeLine_Controller( CHANGE_ET_AND_RAIN_TABLE_SHARED_ET, lbox_index_0, &original_et_table_data.et_inches_u16_10000u, &et_data.et_inches_u16_10000u, sizeof(et_data.et_inches_u16_10000u), CHANGE_REASON_SHARING_WEATHER, lbox_index_0 );

		WEATHER_TABLES_update_et_table_entry_for_index( 1, &et_data );
	}

	// ----------

	// 9/10/2014 ajv : Extract and store the RAIN if present.
	if( B_IS_SET( whats_included, WEATHER_DATA_HAS_RAIN ) )
	{
		memcpy( &rain_data, ucp, sizeof(RAIN_TABLE_ENTRY) );
		ucp += sizeof(RAIN_TABLE_ENTRY);

		// ----------
		
		// 3/21/2018 ajv : Redundant as the changeline below triggers the creation of a formal 
		// change line. Likely left over from debugging. 
		// Alert_Message_va( "Rcvd RAIN: %4.2f, status %d", (rain_data.rain_inches_u16_100u/ONE_HUNDRED_POINT_O), rain_data.status );
		
		// ----------
		
		WEATHER_TABLES_get_rain_table_entry_for_index( 1, &original_rain_table_data );

		Alert_ChangeLine_Controller( CHANGE_ET_AND_RAIN_TABLE_SHARED_RAIN, lbox_index_0, &original_rain_table_data.rain_inches_u16_100u, &rain_data.rain_inches_u16_100u, sizeof(rain_data.rain_inches_u16_100u), CHANGE_REASON_SHARING_WEATHER, lbox_index_0 );

		WEATHER_TABLES_update_rain_table_entry_for_index( 1, &rain_data );
	}

	// ----------

	// 9/10/2014 ajv : If the Comm Server sent us either ET or Rain, make sure
	// the file is saved with the new values.
	if( B_IS_SET( whats_included, WEATHER_DATA_HAS_ET ) || B_IS_SET( whats_included, WEATHER_DATA_HAS_RAIN ) )
	{
		// 4/27/2015 rmd : Set the flag to sync the rain table. Because multiple tasks set and clear
		// the sync_the_et_table flag take the MUTEX when performing this activity.
		xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );
		
		weather_preserves.sync_the_et_rain_tables = (true);

		xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );

		// ----------
		
		// 10/28/2015 rmd : Set the index for the first_to_send to the commserver. Before the file
		// save. So it is saved to the file. We set this index in this function because of the
		// obvious reason that index 1 entries of one or the other of the tables has changed.
		xSemaphoreTakeRecursive( weather_tables_recursive_MUTEX, portMAX_DELAY );
	
		// 10/28/2015 rmd : We just recvd weather and/or rain and put it in index 1 of the table(s).
		// Therefore we should be sending 2 entries. Normally the records_to_send is already at 2 as
		// a result of the table rolling. But check here just to be sure. And if it is already
		// greater than or equal to 2 then leave it alone.
		if( weather_tables.records_to_send < 2 )
		{
			weather_tables.records_to_send = 2;
		}
	
		xSemaphoreGiveRecursive( weather_tables_recursive_MUTEX );
		
		// ----------
		
		// 9/10/2014 ajv : Trigger the weather_tables file to be saved with the new changes.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_WEATHER_TABLES, FLASH_STORAGE_seconds_to_delay_file_save_after_sending_report_data );
	}
	
	// ----------
	
	// 3/7/2017 rmd : Jay and I agreed that, for hubs and hub based controllers, following any
	// commserver initiated 'pseudo-poll' message we would send a 'no more messages' msg. This
	// message falls into that category being an unsolicited messages from the commserver.
	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
}

/* ---------------------------------------------------------- */
static void process_rain_indication_ack( void )
{
	// 10/9/2013 rmd : If we were not waiting for this response alert. No need to reset flag.
	// That is the responsibility of the CI task before next outbound message is kicked out.
	if( !cics.waiting_for_rain_indication_response )
	{
		Alert_Message( "Not expecting a rain indication ACK." );
	}
	else
	{
		// 12/1/2015 rmd : Remember both the receipt_ACK and the NAK pass through here. Processing
		// is the same for either case.
		
		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
static void process_incoming_rain_shutdown_message( const DATA_HANDLE pdh )
{
	Alert_comm_command_sent( MID_TO_COMMSERVER_RAIN_SHUTDOWN_receipt_ack );
		
	CENT_COMM_build_and_send_ack_with_optional_job_number( UPORT_A, MID_TO_COMMSERVER_RAIN_SHUTDOWN_receipt_ack, JOB_NUMBER_NOT_INCLUDED, 0 );
	
	// ----------
	
	// 5/27/2015 rmd : This blast message from the commserver contains NO data!
	
	// ----------

	xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );

	// 6/17/2015 rmd : Set the indication this command was rcvd to 2. See the variable
	// declaration for an explanation as to why.
	weather_preserves.rain.rain_shutdown_rcvd_from_commserver_uns32 = 2;

	xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );
	
	// ----------
	
	// 3/7/2017 rmd : Jay and I agreed that, for hubs and hub based controllers, following any
	// commserver initiated 'pseudo-poll' message we would send a 'no more messages' msg. This
	// message falls into that category being an unsolicited messages from the commserver.
	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
}

/* ---------------------------------------------------------- */
// WE HAVE PDATA CHANGES TO SEND TO THE COMMSERVER
/* ---------------------------------------------------------- */
static void process_verify_firmware_version_ack( const UNS_32 pmessage_id)
{
	// 5/7/2014 rmd : This function executed within the context of the comm_mngr.
	
	// 8/15/2014 rmd : Note - do not reset the waiting_for_xxx flag here. That is the
	// responsibility of the CI task before next outbound message is kicked out.

	if( !cics.waiting_for_firmware_version_check_response )
	{
		// 8/15/2014 rmd : Take no action here. Only alert and wait for the response timer to
		// expire.
		Alert_Message( "Not expecting a verify firmware version ACK." );
	}
	else
	{
		if( pmessage_id == MID_FROM_COMMSERVER_VERIFY_FIRMWARE_VERSION_ACK_FW_OUT_OF_DATE )
		{
			// 5/18/2015 rmd : AJ and I have decided if the controller tries to send pdata changes to
			// the commserver and the commserver is telling us the code is not up to date that we will
			// get the code update NOW. Instead of waiting till 3PM. Why? Because the user is sort of
			// stuck is this state. PData changes won't be sent until after the code has updated. And
			// that could be hours. This came up during test by our field service folks but could well
			// affect a user in an unexpected fashion. Getting the code update now should help this
			// quite a bit.
			//
			// 10/11/2017 rmd : The behavior is the same whether this is a regular non-hub based
			// controller or a HUB itself, for controllers behind the hub however we do want to behave
			// differently.
			if( CONFIG_this_controller_is_a_configured_hub() || !CONFIG_this_controller_is_behind_a_hub() )
			{
				// 4/24/2017 rmd : Remember regular controllers are self polling. so this message will go
				// soon. For the case of the hub the terminate_ci_transaction will cause this message to
				// immediately be sent. Should go next, so post to the front of the queue.
				CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_check_for_updates, NULL, CI_IF_PRESENT_REMOVE_AND_REPOST, CI_POST_TO_FRONT );
			}
		}
		else
		if( pmessage_id == MID_FROM_COMMSERVER_VERIFY_FIRMWARE_VERSION_ACK_FW_UP_TO_DATE )
		{
			// 2/16/2017 rmd : By pushing straight onto the msgs to send queue the sending of pdata will
			// occur immediately at the conclusion of this transaction. Notice we push this to the front
			// of the queue so it is the NEXT message to go to the commserver.
			CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_program_data_to_the_commserver, NULL, CI_IF_PRESENT_REMOVE_AND_REPOST, queueSEND_TO_FRONT );
		}

		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction. Which will
		// ultimately check to send the next queued message. Important to have already posted the
		// send program data event prior to calling this. So the events are on the CI task queue in
		// the right order.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
static void process_commserver_rcvd_program_data_from_us_ack( UNS_32 pmid )
{
	// 10/9/2013 rmd : If we were not waiting for this response alert. No need to reset flag.
	// That is the responsibility of the CI task before next outbound message is kicked out.
	if( !cics.waiting_for_pdata_response )
	{
		Alert_Message( "Not expecting a pdata ACK or NAK." );
	}
	else
	{
		if( pmid == MID_FROM_COMMSERVER_PROGRAM_DATA_receipt_ack )
		{
			// 4/27/2015 ajv : Clear the ACK change bits. Indicating there were no errors.
			PDATA_update_pending_change_bits( (false) );
		}
		else
		if( pmid == MID_FROM_COMMSERVER_PROGRAM_DATA_NAK )
		{
			// 11/20/2015 rmd : If a communication error occurred (while sending Program Data) then we
			// copy the bits of variables that were sent back into the original bit fields and restart
			// the timer to ensure the changes are again sent.
			//
			// NOTE - there is a second way this function gets called indicating there was a comm error.
			// That is when there is NO RESPONSE at all. That case is handled within the controller
			// initiated task itself.
			PDATA_update_pending_change_bits( (true) );
		}

		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
// COMMSERVER HAS PDATA FOR US
/* ---------------------------------------------------------- */
static void process_commserver_sent_us_an_ihsfy_packet( void )
{
	// 9/30/2015 rmd : Wissam and I spoke about this. What is the point of even asking if our
	// firmware is up to date if we have changes to send and we are allowed to send them. There
	// is a complication in the comm server that got us to add this if statement. The commserver
	// starts a "SESSION" of sorts. Where it is expecting certain MID's next. And this session
	// is started by asking is our firmware up to date. We've seen some difficulties with this
	// session stuff. So would rather NOT START THE SESSION if we are just going to abandon it
	// anyway. You'll notice this same test is repeated after we receive notification that our
	// firmware is up to date and thats fine. Should pass there.
	if( (weather_preserves.pending_changes_to_send_to_comm_server && !weather_preserves.factory_reset_do_not_allow_pdata_to_be_sent) )
	{
		Alert_Message( "IHSFY ignored. CHANGES need to be sent first." );

		// 4/24/2017 rmd : For both hub based and non-hub based controllers, treat this as a pseudo
		// poll message. And get the controller sending those pending pdata changes to the
		// commserver. So in turn, the commserver can try its IHSFY again. Remember the commserver
		// will try the IHSFY indefinitely.
		CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_ask_commserver_if_our_firmware_is_up_to_date, NULL, CI_IF_PRESENT_REMOVE_AND_REPOST, CI_POST_TO_FRONT );
		
		// 4/24/2017 rmd : If non-hub based self polling will commence message queue processing.
		// Otherwise we have to take this step ourselves.
		poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
	}
	else
	{
		if( CONFIG_this_controller_is_a_configured_hub() || CONFIG_this_controller_is_behind_a_hub() )
		{
			Alert_Message( "IHSFY processed (as a poll msg). Checking firmware first." );
	
			// ----------
			
			// 3/1/2017 rmd : I don't think this should happen. Kind of says the comm server hasn't
			// treated the IHSFY as a poll message. And just sent it at any time. And happens to be in
			// the middle of a standard poll with messaging taking place?
			if( cics.state != CI_STATE_idle )
			{
				Alert_Message( "CI: msg when not idle?" );
			}

			// ----------
			
			// 3/1/2017 rmd : For hub based controllers think of this as a poll message in that we need
			// to conclude our session with a 'no more messages' msg. BUT this 'session' consists of a
			// several message exchange. And ends either with the commserver telling the controller his
			// code is out of date OR ends with the receipt of pdata by the controllers. Only at these
			// places should we push the no more messages msg to the msg queue.
			//
			// In any case must post to the TASK queue the 'build and send' event to make it go.
			CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_before_pdata_rqst_check_firmware, NULL, CI_IF_PRESENT_REMOVE_AND_REPOST, CI_POST_TO_FRONT );

			CONTROLLER_INITIATED_post_event( CI_EVENT_build_and_send_the_next_queued_msg );
		}
		else
		{
			Alert_Message( "IHSFY processed. Checking firmware first." );
		
			// 3/1/2017 rmd : For the non-hub based case post to the msg queue the event to to the pre
			// program data xfer firmware check. And that is all we have to do. In 5 seconds or less a
			// 'build and send' event will be generated from the polling timer.
			CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_before_pdata_rqst_check_firmware, NULL, CI_IF_PRESENT_REMOVE_AND_REPOST, CI_POST_TO_FRONT );
		}
	}
}			

static void process_check_firmware_version_before_pdata_request_ack( const UNS_32 pmessage_id )
{
	// 5/7/2014 rmd : This function executed within the context of the comm_mngr.
	
	// 8/15/2014 rmd : Note - do not reset the waiting_for_xxx flag here. That is the
	// responsibility of the CI task before next outbound message is kicked out.

	if( !cics.waiting_for_firmware_version_check_before_asking_for_pdata_response )
	{
		// 8/15/2014 rmd : Take no action here. Only alert and wait for the response timer to
		// expire.
		Alert_Message( "Not expecting a check firmware version ACK." );
	}
	else
	{
		if( pmessage_id == MID_FROM_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_ACK_FW_OUT_OF_DATE )
		{
			// 6/8/2015 rmd : If our firmware is out of date do not ask the commserver to send the pdata
			// changes he has pending. Instead let's begin the code update process. Following completion
			// of that the commserver will again send an IHSFY and start the process over again.
			//
			// 10/11/2017 rmd : The behavior is the same whether this is a regular non-hub based
			// controller or a HUB itself, for controllers behind the hub however we do want to behave
			// differently.
			if( CONFIG_this_controller_is_a_configured_hub() || !CONFIG_this_controller_is_behind_a_hub() )
			{
				// 4/24/2017 rmd : Should go next, so post to the front of the queue.
				CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_check_for_updates, NULL, CI_IF_PRESENT_REMOVE_AND_REPOST, queueSEND_TO_FRONT );
			}
		}
		else
		if( pmessage_id == MID_FROM_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_ACK_FW_UP_TO_DATE )
		{
			// 5/1/2015 rmd : In order to ensure the commserver and the controller PDATA stay in sync
			// AJ and I have decided that if there are controller changes not yet sent to the commserver
			// and the commserver asks us to ask him for PDATA we ignore that request. This prevents the
			// possiblity of the messages 'crossing in the pipe' (ships passing in the night). Which if
			// contained changes to the same variable the possibility exists taking into account the
			// message could be built and queued on this end while the comm server data message
			// arrives.
			//
			// 5/1/2015 rmd : A NOTE ON INCLUDING THE "cics.a_pdata_message_is_on_the_list" TERM - we
			// actually do not need to include the test to see if a pdata message is all ready on the
			// list. Because if we were to proceed here and a pdata msg was indeed on the CI list our
			// request to the commserver to send his pending pdata changes would be QUEUED after our
			// pdata message. And the intended staying in sync protection would be preserved.
			// 5/8/2015 rmd : Take the MUTEX. A change could be being made I suppose. Not sure but
			// seemed like the right thing to do here.
			xSemaphoreTakeRecursive( pending_changes_recursive_MUTEX, portMAX_DELAY );
			
			// 6/8/2015 rmd : If there are changes to send and we are allowed to send the pdata OR they
			// are being sent do not ask the commserver to send his pending pdata changes.
			if( (weather_preserves.pending_changes_to_send_to_comm_server && !weather_preserves.factory_reset_do_not_allow_pdata_to_be_sent) )
			{
				Alert_Message_va( "detected pdata changes that need to send first %d %d", weather_preserves.pending_changes_to_send_to_comm_server, weather_preserves.factory_reset_do_not_allow_pdata_to_be_sent );
				
				// 6/8/2015 rmd : Nothing specific to do. The transmission of the pending pdata changes to
				// the commserver is under control of other mechanisms.
			}
			else
			if( cics.a_pdata_message_is_on_the_list )
			{
				Alert_Message( "pdata message already in process" );
				
				// 6/8/2015 rmd : Nothing specific to do. The transmission of the pending pdata changes to
				// the commserver is under control of other mechanisms.
			}
			else
			{
				// 2/16/2017 rmd : By pushing straight onto the msgs to send queue the sending of pdata will
				// occur immediately at the conclusion of this transaction. Notice we push this to the front
				// of the queue so it is the NEXT message to go to the commserver.
				CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_ask_commserver_if_there_is_program_data_to_send_to_us, NULL, CI_IF_PRESENT_REMOVE_AND_REPOST, CI_POST_TO_FRONT );
			}
			
			xSemaphoreGiveRecursive( pending_changes_recursive_MUTEX );
		}

		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
static void process_asking_commserver_if_there_is_program_data_for_us_ack( void )
{
	// 10/9/2013 rmd : If we were not waiting for this response alert. No need to reset flag.
	// That is the responsibility of the CI task before next outbound message is kicked out.
	if( !cics.waiting_for_asked_commserver_if_there_is_pdata_for_us_response )
	{
		Alert_Message( "Not expecting a program data request ACK." );
	}
	else
	{
		// 12/1/2015 rmd : Remember there are 3 possible responses from the commserver. One response
		// indicates there is data coming, the second response indicates no data coming. And the
		// third is a NAK indicating an error at the commserver. Processing is the same in all
		// cases.
		
		// ----------
		
		// 4/20/2015 rmd : The request for program data went out to the commserver. And he has ACK'd
		// it. The are two different responses. However we effectively aren't concerned with which
		// one it is. The request is really a signal to the commserver to send program data to us as
		// a wholly new inbound message from the comm server to us. And that has nothing to do with
		// this outbound message. So therefore we complete the CI transaction regardless of whether
		// data is coming or not.
		//
		// 4/20/2015 rmd : The commserver is expected to perhaps send the PROGRAM_DATA inbound
		// message with its init and data packets. Depending on if there are changes to send or not.
		//
		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
static void process_incoming_program_data_message( DATA_HANDLE const pdh )
{
	UNS_8		*ucp;

	// ----------

	// 4/20/2015 rmd : The message is delivered raw and ready to go. No headers.
	ucp = pdh.dptr;

	// ----------

	PDATA_extract_and_store_changes( ucp, CHANGE_REASON_CENTRAL_OR_MOBILE, CHANGE_set_change_bits, CHANGE_REASON_CENTRAL_OR_MOBILE );
	
	// ----------
	
	// 9/3/2015 rmd : And we rcvd pdata from the cloud. So by definition we clear the block on
	// sending pdata.
	weather_preserves.factory_reset_do_not_allow_pdata_to_be_sent = (false);

	// ----------
	
	// 3/21/2017 rmd : And for the case of the hub-based controllers (including the hub itself)
	// we now have to indicate to the commserver the end of the poll period. So send the 'no
	// more messages' msg. And make sure to post the build and send event to the task as we are
	// very likely NOT in the middle of a CI exchange. This started with the IHSFY 'pseudo-poll'
	// message from the commserver. And ends here after receipt of the pdata.
	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
}

/* ---------------------------------------------------------- */
static void process_set_time_and_date_from_commserver( const DATA_HANDLE pdh )
{
	// 6/9/2015 rmd : This function is executed within the context of the commmngr task.
	
	// 6/9/2015 rmd : There is NO ACK to the time and date command.
	
	UNS_8		*ucp;
	
	DATE_TIME	message_dt;
	
	// ----------
	
	ucp = pdh.dptr;
	
	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );
	
	memcpy( &message_dt, ucp, sizeof(DATE_TIME) );

	if( EPSON_set_date_time( message_dt, CHANGE_REASON_CENTRAL_OR_MOBILE ) )
	{
		// 12/03/2015 skc : If the datetime was actually set,
		// then prepare for sending tokens to IRRI's
		xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

		comm_mngr.token_date_time.dt = message_dt;

		comm_mngr.token_date_time.reason = CHANGE_REASON_CENTRAL_OR_MOBILE;

		comm_mngr.flag_update_date_time = true;

		xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );
	}
	
	// ----------
	
	// 3/7/2017 rmd : Jay and I agreed that, for hubs and hub based controllers, following any
	// commserver initiated 'pseudo-poll' message we would send a 'no more messages' msg. This
	// message falls into that category being an unsolicited messages from the commserver.
	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
}

/* ---------------------------------------------------------- */
static void process_request_to_send_status( void )
{
	CONTROLLER_INITIATED_TASK_QUEUE_STRUCT	citqs;
	
	// ----------
	
	Alert_comm_command_sent( MID_TO_COMMSERVER_send_a_status_screen_ack );

	CENT_COMM_build_and_send_ack_with_optional_job_number( UPORT_A, MID_TO_COMMSERVER_send_a_status_screen_ack, JOB_NUMBER_NOT_INCLUDED, 0 );
	
	// ----------
	
	// 3/7/2017 rmd : Restart the mobile session seconds.
	CONTROLLER_INITIATED_post_event( CI_EVENT_zero_mobile_session_seconds_count );
	
	// ----------
	
	// 3/7/2017 rmd : For all controllers start the timer. To send the next status 15 seconds
	// after sending the one being sent due to this command.
	citqs.event = CI_EVENT_restart_mobile_status_timer;

	citqs.how_long_ms = CI_SEND_STATUS_MS_routine_update;
	
	CONTROLLER_INITIATED_post_event_with_details( &citqs );
	
	// ----------
	
	CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_mobile_status, NULL, CI_IF_PRESENT_REMOVE_AND_REPOST, CI_POST_TO_BACK );
	
	// ----------

	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
}

/* ---------------------------------------------------------- */
static void process_ack_from_commserver_for_status_screen( void )
{
	// 10/9/2013 rmd : If we were not waiting for this response alert. No need to reset flag.
	// That is the responsibility of the CI task before next outbound message is kicked out.
	if( !cics.waiting_for_mobile_status_response )
	{
		Alert_Message( "Not expecting a mobile status screen ACK." );
	}
	else
	{
		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
static void process_incoming_mobile_station_ON( const DATA_HANDLE pdh )
{
	TEST_AND_MOBILE_KICK_OFF_STRUCT	tmkos;

	UNS_32	ljob_number;
	
	UNS_8	*ucp;
	
	// ----------
	
	// 6/18/2015 rmd : Clean.
	memset( &tmkos, 0x00, sizeof(TEST_AND_MOBILE_KICK_OFF_STRUCT) );
	
	// ----------
	
	// 6/18/2015 rmd : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;
	
	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );
	
	memcpy( &ljob_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);
	
	memcpy( &tmkos.box_index_0, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);
	
	memcpy( &tmkos.station_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);
	
	memcpy( &tmkos.time_seconds, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);
	
	// 5/9/2016 ajv : Do not set the expected
	tmkos.set_expected = (false);

	// ----------
	
	// 5/23/2017 rmd : We need to incorporate this test into many of the mobile commands from
	// the commserver. And respond with a NAK of some sort, or indicate as part of the STATUS
	// that we weren't able to fullfill your request.
	if( allowed_to_process_irrigation_commands_from_comm_server() )
	{
		FOAL_IRRI_initiate_a_station_test( &tmkos, IN_LIST_FOR_MOBILE );
	}
	
	// ----------
	
	build_response_to_mobile_cmd_and_update_session_data( MID_TO_COMMSERVER_mobile_station_ON_ack, ljob_number );
}

/* ---------------------------------------------------------- */
static void process_incoming_mobile_station_OFF( const DATA_HANDLE pdh )
{
	UNS_32	ljob_number;

	UNS_32	lbox_index;

	UNS_32	lstation_number;
	
	UNS_8	*ucp;
	
	// ----------
	
	// 6/18/2015 rmd : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;
	
	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );
	
	memcpy( &ljob_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);
	
	memcpy( &lbox_index, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);
	
	memcpy( &lstation_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);
	
	// ----------
	
	// 7/2/2015 rmd : This takes care of business NOW. AND posts to the BACK of the controller
	// initiated MESSAGE QUEUE (not the TASK queue) a status message.
	FOAL_IRRI_remove_this_station_for_this_reason_from_the_irrigation_lists( lbox_index, lstation_number, IN_LIST_FOR_MOBILE, ACTION_REASON_TURN_OFF_AND_REMOVE_mobile_command );

	// ----------
	
	build_response_to_mobile_cmd_and_update_session_data( MID_TO_COMMSERVER_mobile_station_OFF_ack, ljob_number );
}

/* ---------------------------------------------------------- */
static void process_request_factory_reset_panel_swap_for_new_panel( const DATA_HANDLE pdh )
{
	// 9/1/2015 rmd : Executed within the context of the comm_mngr task.
	
	// ----------
	
	Alert_comm_command_sent( MID_TO_COMMSERVER_panel_swap_factory_reset_to_new_panel_ack );

	CENT_COMM_build_and_send_ack_with_optional_job_number( UPORT_A, MID_TO_COMMSERVER_panel_swap_factory_reset_to_new_panel_ack, JOB_NUMBER_NOT_INCLUDED, 0 );
	
	// ----------
	
	// 9/1/2015 rmd : Now this is a tricky delay. If we proceed directly with the reset the ack
	// won't make it out the comm device. So delay this arbritrary 10 seconds to give it a
	// chance to transmit. The watchdog task allows the comm_mngr task up to 15 seconds before
	// reporting in. So this will not cause the watchdog to fire.
	vTaskDelay( MS_to_TICKS( 10000 ) );
	
	FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart( SYSTEM_SHUTDOWN_EVENT_factory_reset_via_cloud_panel_swap_for_new_panel );
	
	// ----------
	
	// 3/7/2017 rmd : Jay and I agreed that, for hubs and hub based controllers, following any
	// commserver initiated 'pseudo-poll' message we would send a 'no more messages' msg. This
	// message falls into that category being an unsolicited messages from the commserver.
	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
}

/* ---------------------------------------------------------- */
static void process_request_factory_reset_panel_swap_for_old_panel( const DATA_HANDLE pdh )
{
	// 9/1/2015 rmd : Executed within the context of the comm_mngr task.
	
	// ----------
	
	Alert_comm_command_sent( MID_TO_COMMSERVER_panel_swap_factory_reset_to_old_panel_ack );

	CENT_COMM_build_and_send_ack_with_optional_job_number( UPORT_A, MID_TO_COMMSERVER_panel_swap_factory_reset_to_old_panel_ack, JOB_NUMBER_NOT_INCLUDED, 0 );

	// ----------
	
	// 9/1/2015 rmd : Now this is a tricky delay. If we proceed directly with the reset the ack
	// won't make it out the comm device. So delay this arbritrary 10 seconds to give it a
	// chance to transmit. The watchdog task allows the comm_mngr task up to 15 seconds before
	// reporting in. So this will not cause the watchdog to fire.
	vTaskDelay( MS_to_TICKS( 10000 ) );
	
	FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart( SYSTEM_SHUTDOWN_EVENT_factory_reset_via_cloud_panel_swap_for_old_panel );
	
	// ----------
	
	// 3/7/2017 rmd : Jay and I agreed that, for hubs and hub based controllers, following any
	// commserver initiated 'pseudo-poll' message we would send a 'no more messages' msg. This
	// message falls into that category being an unsolicited messages from the commserver.
	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
}

/* ---------------------------------------------------------- */
static void process_send_all_program_data( void )
{
	// 9/1/2015 rmd : Executed within the context of the comm_mngr task.
	//
	// 4/25/2017 rmd : This function is called in response to a request, from the commserver,
	// for the controller to send ALL of its program data. Not sure how you do this in the
	// UI.

	// ----------
	
	Alert_comm_command_sent( MID_TO_COMMSERVER_set_all_bits_ack );

	CENT_COMM_build_and_send_ack_with_optional_job_number( UPORT_A, MID_TO_COMMSERVER_set_all_bits_ack, JOB_NUMBER_NOT_INCLUDED, 0 );
	
	// ----------
	
	// 4/24/2017 rmd : Need to consider what happens in the hub environment. We add the no more
	// messages msg and fire up queue processing, BUT the pdata msg is not on the queue yet as
	// it is tied to a 30 second timer. So make sure we don't use that timer but rather put the
	// msg on the queue ourselves after setting all the bits. Do save the files though to cover
	// if there is a power failure.
	COMM_TEST_send_all_program_data_to_the_cloud( SEND_PDATA_TO_CLOUD_save_the_files, SEND_PDATA_TO_CLOUD_do_not_start_the_pdata_timer );
	
	// 4/24/2017 rmd : Explicitly add per the above comment.
	CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_ask_commserver_if_our_firmware_is_up_to_date, NULL, CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD, CI_POST_TO_BACK );
	
	// ----------
	
	// 3/7/2017 rmd : Jay and I agreed that, for hubs and hub based controllers, following any
	// commserver initiated 'pseudo-poll' message we would send a 'no more messages' msg. This
	// message falls into that category being an unsolicited messages from the commserver.
	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
}

/* ---------------------------------------------------------- */
static void process_incoming_mobile_mvor_cmd( const DATA_HANDLE pdh )
{
	MVOR_KICK_OFF_STRUCT	mvor_kos;

	UNS_32	ljob_number;
	
	UNS_8	*ucp;
	
	// ----------
	
	// 9/30/2015 ajv : Clean the record first
	memset( &mvor_kos, 0x00, sizeof(MVOR_KICK_OFF_STRUCT) );
	
	// ----------
	
	// 9/30/2015 ajv : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;
	
	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );
	
	memcpy( &ljob_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	memcpy( &mvor_kos.system_gid, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);
	
	memcpy( &mvor_kos.mvor_action_to_take, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);
	
	memcpy( &mvor_kos.mvor_seconds, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);
	
	// ----------
	
	FOAL_IRRI_initiate_or_cancel_a_master_valve_override( mvor_kos.system_gid, mvor_kos.mvor_action_to_take, mvor_kos.mvor_seconds, INITIATED_VIA_COMM );

	// ----------
	
	build_response_to_mobile_cmd_and_update_session_data( MID_TO_COMMSERVER_mobile_MVOR_ack, ljob_number );
}

/* ---------------------------------------------------------- */
static void process_incoming_mobile_lights_cmd( const DATA_HANDLE pdh )
{
	LIGHTS_ON_XFER_RECORD	lonxr;

	LIGHTS_OFF_XFER_RECORD	loffxr;

	UNS_32	ljob_number;

	UNS_32	laction_needed;

	UNS_32	lbox_index;

	// 9/30/2015 ajv : Grab local copies of these variables so we can programmatically decide
	// which to use whether we're turning ON or turning OFF a light since the XFER records are
	// unique.
	UNS_32	llight_index_0_3;

	UNS_32	lseconds_to_run;
	
	DATE_TIME	ldt;

	UNS_8	*ucp;
	
	// ----------
	
	// 9/30/2015 ajv : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;
	
	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );
	
	memcpy( &ljob_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);
	
	memcpy( &laction_needed, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	memcpy( &lbox_index, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	memcpy( &llight_index_0_3, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);
	
	memcpy( &lseconds_to_run, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);
	
	// ----------
	
	if( laction_needed == ACTION_NEEDED_TURN_ON_mobile_light )
	{
		// 9/30/2015 ajv : Clean the records first
		memset( &lonxr, 0x00, sizeof(LIGHTS_ON_XFER_RECORD) );

		lonxr.in_use = (true);
		lonxr.light_index_0_47 = (lbox_index * MAX_LIGHTS_IN_A_BOX) + llight_index_0_3;

		EPSON_obtain_latest_time_and_date( &ldt );

		TDUTILS_add_seconds_to_passed_DT_ptr( &ldt, lseconds_to_run );

		lonxr.stop_datetime_d = ldt.D;
		lonxr.stop_datetime_t = ldt.T;

		FOAL_LIGHTS_initiate_a_mobile_light_ON( &lonxr );
	}
	else if( laction_needed == ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_mobile_off )
	{
		// 9/30/2015 ajv : Clean the records first
		memset( &loffxr, 0x00, sizeof(LIGHTS_OFF_XFER_RECORD) );

		loffxr.in_use = (true);
		loffxr.light_index_0_47 = (lbox_index * MAX_LIGHTS_IN_A_BOX) + llight_index_0_3;

		FOAL_LIGHTS_initiate_a_mobile_light_OFF( &loffxr );
	}
	else
	{
		Alert_Message( "Invalid lights command rcvd" );
	}

	// ----------
	
	build_response_to_mobile_cmd_and_update_session_data( MID_TO_COMMSERVER_mobile_light_cmd_ack, ljob_number );
}

/* ---------------------------------------------------------- */
static void process_incoming_mobile_turn_controller_on_off_cmd( const DATA_HANDLE pdh )
{
	UNS_32	ljob_number;

	BOOL_32	lturn_off;

	UNS_8	*ucp;
	
	// ----------
	
	// 10/5/2015 ajv : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;
	
	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );
	
	memcpy( &ljob_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	memcpy( &lturn_off, ucp, sizeof(BOOL_32) );
	ucp += sizeof(BOOL_32);
	
	// ----------
	
	// 10/5/2015 ajv : If lturn_off is TRUE, the controller/network is turned off. Otherwise,
	// it's turned on.
	NETWORK_CONFIG_set_scheduled_irrigation_off( lturn_off, CHANGE_generate_change_line, CHANGE_REASON_CENTRAL_OR_MOBILE, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, NETWORK_CONFIG_get_change_bits_ptr(CHANGE_REASON_CENTRAL_OR_MOBILE) );

	// ----------

	// 1/27/2016 ajv : Since this command comes from Command Center Online, it inherently won't
	// trigger resending Program Data. However, that means Program Data will report that the
	// controller is OFF when it's actually ON or vice versa. Therefore, force the change bits
	// to be set in the changes_to_upload_to_comm_server bit field so the changes are uploaded.
	NETWORK_CONFIG_on_all_settings_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );

	// 1/27/2016 ajv : Set our pending flag in case of a power failure. Will guarantee another
	// try when power returns if the files have been saved (remember counts on bits set in the
	// files themselves).
	weather_preserves.pending_changes_to_send_to_comm_server = (true);

	// 1/27/2016 ajv : And start the timer which triggers the data to be sent in 30 seconds.
	xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGE_MADE_MS ), portMAX_DELAY );

	// ----------
	
	build_response_to_mobile_cmd_and_update_session_data( MID_TO_COMMSERVER_mobile_turn_controller_on_off_ack, ljob_number );
}

/* ---------------------------------------------------------- */
static void process_incoming_mobile_now_days_all_stations_cmd( const DATA_HANDLE pdh )
{
	UNS_32	ljob_number;

	UNS_32	lnow_days;

	UNS_8	*ucp;
	
	// ----------
	
	// 10/5/2015 ajv : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;
	
	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );
	
	memcpy( &ljob_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	memcpy( &lnow_days, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);
	
	// ----------
	
	STATION_set_no_water_days_for_all_stations( lnow_days, CHANGE_REASON_CENTRAL_OR_MOBILE );

	// ----------
	
	build_response_to_mobile_cmd_and_update_session_data( MID_TO_COMMSERVER_mobile_now_days_all_stations_ack, ljob_number );
}

/* ---------------------------------------------------------- */
static void process_incoming_mobile_now_days_by_group_cmd( const DATA_HANDLE pdh )
{
	UNS_32	ljob_number;

	UNS_32	lnow_days;

	UNS_32	lgroup_id;

	UNS_8	*ucp;
	
	// ----------
	
	// 10/5/2015 ajv : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;
	
	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );
	
	memcpy( &ljob_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	memcpy( &lnow_days, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);
	
	memcpy( &lgroup_id, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	// ----------
	
	STATION_set_no_water_days_by_group( lnow_days, lgroup_id, CHANGE_REASON_CENTRAL_OR_MOBILE );

	// ----------
	
	build_response_to_mobile_cmd_and_update_session_data( MID_TO_COMMSERVER_mobile_now_days_by_group_ack, ljob_number );
}

/* ---------------------------------------------------------- */
static void process_incoming_mobile_now_days_by_station_cmd( const DATA_HANDLE pdh )
{
	UNS_32	ljob_number;

	UNS_32	lnow_days;

	UNS_32	lbox_index_0;

	UNS_32	lstation_number_0;

	UNS_8	*ucp;
	
	// ----------
	
	// 10/5/2015 ajv : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;
	
	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );
	
	memcpy( &ljob_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	memcpy( &lnow_days, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);
	
	memcpy( &lbox_index_0, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	memcpy( &lstation_number_0, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	// ----------
	
	STATION_set_no_water_days_by_station( lnow_days, lbox_index_0, lstation_number_0, CHANGE_REASON_CENTRAL_OR_MOBILE );

	// ----------
	
	build_response_to_mobile_cmd_and_update_session_data( MID_TO_COMMSERVER_mobile_now_days_by_station_ack, ljob_number );
}

/* ---------------------------------------------------------- */
static void process_incoming_mobile_now_days_by_box_cmd( const DATA_HANDLE pdh )
{
	UNS_32	ljob_number;

	UNS_32	lnow_days;

	UNS_32	lbox_index_0;

	UNS_8	*ucp;

	// ----------

	// 2/11/2016 wjb : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;

	ucp += sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER);

	memcpy( &ljob_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	memcpy( &lnow_days, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	memcpy( &lbox_index_0, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	// ----------

	STATION_set_no_water_days_by_box( lnow_days, lbox_index_0, CHANGE_REASON_CENTRAL_OR_MOBILE );

	// ----------

	build_response_to_mobile_cmd_and_update_session_data( MID_TO_COMMSERVER_mobile_now_days_by_box_ack, ljob_number );
}

/* ---------------------------------------------------------- */
static void process_incoming_request_to_send_registration( BOOL_32 psend_ack  )
{
	// 8/18/2016 rmd : I believe this function results in sending both the registration in 30
	// seconds and the alerts 2 minutes after that.
	
	// ----------
	
	// 10/5/2015 ajv : Get the controller ready to send registration data. Upon receipt, it's
	// the CommServer's responsibility to react accordingly. For example, it may respond with a
	// new network ID if the controller was moved from the test database to the production
	// database, or vice versa.
	CONTROLLER_INITIATED_update_comm_server_registration_info();

	// ----------
	
	// 11/6/2017 rmd : I've determined that it would be nice to not only send registration but
	// to also send alerts. For hub related controllers, for this to happen we have to get the
	// alerts on the messages queue NOW. If not the alerts won't be sent until the next poll,
	// which is typically like 15 minutes. This works well for non-hub controllers too. So don't
	// use the alerts timer, instead push directly to the back of the messages queue.
	CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_alerts, NULL, CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD, CI_POST_TO_BACK );
	
	// ----------
	
	if( psend_ack )
	{
		CENT_COMM_build_and_send_ack_with_optional_job_number( UPORT_A, MID_TO_COMMSERVER_force_registration_ack, JOB_NUMBER_NOT_INCLUDED, 0 );
	}
	
	// ----------
	
	// 3/7/2017 rmd : Jay and I agreed that, for hubs and hub based controllers, following any
	// commserver initiated 'pseudo-poll' message we would send a 'no more messages' msg. This
	// message falls into that category being an unsolicited messages from the commserver.
	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
}

/* ---------------------------------------------------------- */
static void process_incoming_enable_or_disable_FL_cmd( const DATA_HANDLE pdh )
{
	BOOL_32	lrcvd_option;

	UNS_8	*ucp;

	// ----------

	// 10/5/2015 ajv : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;

	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );

	memcpy( &lrcvd_option, ucp, sizeof(BOOL_32) );
	ucp += sizeof(BOOL_32);

	// ----------

	// 8/18/2016 rmd : A quick and dirty indication.
	if( config_c.purchased_options.option_FL != lrcvd_option )
	{
		if( lrcvd_option )
		{
			Alert_Message( "switching -FL option ON" );
		}
		else
		{
			Alert_Message( "switching -FL option OFF" );
		}
	}

	// ----------
	
	// 11/30/2015 ajv : Set the option
	config_c.purchased_options.option_FL = lrcvd_option;

	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_CONTROLLER_CONFIGURATION, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );

	// ----------

	// 12/14/2015 ajv : Redraw the screen to ensure the FLOWSENSE menu item appears or
	// disappears accordingly if the Setup menu is visible. Also make sure any dialogs that are
	// open are closed before we redraw the screen to ensure the redraw is processed properly.
	//
	// 8/18/2016 rmd : I do NOT like mingling screen work in the midst of comm_mngr work. It
	// bothers me from two points of view. The screen work is being done NOW because the display
	// task is a higher priority than the comm_mngr. And two I feel sometimes that the close all
	// dialogs and the redrawscreen confuse the UI menuing (this is just a suspicion).
	DIALOG_close_all_dialogs();

	Redraw_Screen( (true) );

	// ----------

	// 11/30/2015 ajv : Take the mutex to protect against us missing a change due to multiple
	// processes manipulating the send_box_configuration_to_master flag.
	xSemaphoreTakeRecursive( irri_comm_recursive_MUTEX, portMAX_DELAY );

	// 11/30/2015 ajv : And trigger the configuration change to be sent to the master when the
	// next token arrives.
	irri_comm.send_box_configuration_to_master = (true);

	xSemaphoreGiveRecursive( irri_comm_recursive_MUTEX );

	// 8/19/2016 rmd : KLUDGE necessary. So if the controller had -FL enabled and FLOWSENSE
	// turned on and the chain is down we are NOT making tokens. So the -FL configuration
	// information won't get carried from the irri side to the master side and inserted into
	// chain members until after a rescan. So just directly stuff it into chain members so that
	// when the registration is sent it reflects to the commserver the latest setting. This way
	// the part number in the controller details report changes to reflect the -FL change just
	// sent by the commserver.
	chain.members[ FLOWSENSE_get_controller_index() ].box_configuration.purchased_options.option_FL = lrcvd_option;
	
	// ----------

	CENT_COMM_build_and_send_ack_with_optional_job_number( UPORT_A, MID_TO_COMMSERVER_enable_or_disable_FL_option_ack, JOB_NUMBER_NOT_INCLUDED, 0 );
	
	// ----------
	
	// 8/18/2016 rmd : And since we changed or ATTEMPTED a change to part of the registration
	// data, trigger sending the registration to the comm_server so we can see updated part
	// numbers. Use function as if the comm_server asked us to do this but set the flag to ACK
	// to (false) of course. Commserver is not expecting this ACK!
	process_incoming_request_to_send_registration( (false) );
	
	// ----------
	
	// 3/7/2017 rmd : Jay and I agreed that, for hubs and hub based controllers, following any
	// commserver initiated 'pseudo-poll' message we would send a 'no more messages' msg. This
	// message falls into that category being an unsolicited messages from the commserver.
	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
}

/* ---------------------------------------------------------- */
static void process_incoming_clear_mlb_cmd( const DATA_HANDLE pdh )
{
	UNS_32	ljob_number;

	UNS_32	lsystem_GID;

	UNS_8	*ucp;

	// ----------

	// 10/5/2015 ajv : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;

	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );

	memcpy( &ljob_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	memcpy( &lsystem_GID, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	// ----------

	// 12/14/2015 ajv : Call the clear function regardless of whether there's actually a break
	// or not. It will detect whether one exists and take the appropriate action if necessary.
	SYSTEM_clear_mainline_break( lsystem_GID );

	// ----------

	// 12/14/2015 ajv : Redraw the screen to ensure the MLB flag is cleared on the Status screen
	// and Real-Time Mainline Flow screens. Also make sure any dialogs that are open are closed
	// before we redraw the screen to ensure the redraw is processed properly.
	DIALOG_close_all_dialogs();

	Redraw_Screen( (true) );

	// ----------

	build_response_to_mobile_cmd_and_update_session_data( MID_TO_COMMSERVER_clear_MLB_ack, ljob_number );
}

/* ---------------------------------------------------------- */
static void process_incoming_stop_all_irrigation_cmd( const DATA_HANDLE pdh )
{
	UNS_32	ljob_number;

	UNS_8	*ucp;

	// ----------

	// 10/5/2015 ajv : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;

	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );

	memcpy( &ljob_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	// ----------

	// 12/14/2015 ajv : Stop irrigation the same way the STOP key does - the highest type of
	// irrigation first.
	IRRI_COMM_process_stop_command( (true) );

	// ----------

	// 12/14/2015 ajv : Redraw the screen to ensure the Status screen and Irrigation Details
	// screens are updated accordingly. Also make sure any dialogs that are open are closed
	// before we redraw the screen to ensure the redraw is processed properly.
	DIALOG_close_all_dialogs();

	Redraw_Screen( (true) );

	// ----------

	build_response_to_mobile_cmd_and_update_session_data( MID_TO_COMMSERVER_stop_all_irrigation_ack, ljob_number );
}

/* ---------------------------------------------------------- */
static void process_incoming_stop_irrigation_cmd( const DATA_HANDLE pdh )
{
	UNS_32	ljob_number;

	UNS_32	lsystem_GID;

	UNS_8	*ucp;

	// ----------

	// 10/5/2015 ajv : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;

	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );

	memcpy( &ljob_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	memcpy( &lsystem_GID, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	// ----------

	// 12/14/2015 ajv : Stop irrigation the same way the STOP key does - the highest type of
	// irrigation first. This will stop the highest reason for ALL mainlines. However, we've
	// designed this feature to accept a particular system GID so we can enhance this if we
	// choose in the future.
	IRRI_COMM_process_stop_command( (false) );

	// ----------

	// 12/14/2015 ajv : Redraw the screen to ensure the Status screen and Irrigation Details
	// screens are updated accordingly. Also make sure any dialogs that are open are closed
	// before we redraw the screen to ensure the redraw is processed properly.
	DIALOG_close_all_dialogs();

	Redraw_Screen( (true) );

	// ----------

	build_response_to_mobile_cmd_and_update_session_data( MID_TO_COMMSERVER_stop_irrigation_ack, ljob_number );
}

/* ---------------------------------------------------------- */
static void process_incoming_perform_two_wire_discovery_cmd( const DATA_HANDLE pdh )
{
	UNS_32	ljob_number;

	UNS_32	lbox_index_0;

	UNS_8	*ucp;

	// ----------

	// 10/5/2015 ajv : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;

	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );

	memcpy( &ljob_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	// 5/9/2016 ajv : Extract the box index out of the message. For now, we're going to ignore
	// this and perform a discovery on ALL controllers in the chain However, this index provides
	// the ability to enhance this in the future with no changes to the CommServer.
	memcpy( &lbox_index_0, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	// ----------

	// 11/30/2015 ajv : Take the mutex to protect against us missing a change due to multiple
	// processes manipulating the perform_two_wire_discovery flag.
	xSemaphoreTakeRecursive( irri_comm_recursive_MUTEX, portMAX_DELAY );

	// 11/30/2015 ajv : Trigger every controller in the chain, including this one, to perform a
	// 2-Wire discovery when the next token arrives.
	comm_mngr.perform_two_wire_discovery = (true);

	xSemaphoreGiveRecursive( irri_comm_recursive_MUTEX );

	// ----------

	build_response_to_mobile_cmd_and_update_session_data( MID_TO_COMMSERVER_perform_two_wire_discovery_ack, ljob_number );
}

/* ---------------------------------------------------------- */
static void process_incoming_mobile_acquire_expected_flow_by_station_cmd( const DATA_HANDLE pdh )
{
	TEST_AND_MOBILE_KICK_OFF_STRUCT	tmkos;

	UNS_32	ljob_number;

	UNS_8	*ucp;

	// ----------

	// 6/18/2015 rmd : Clean.
	memset( &tmkos, 0x00, sizeof(TEST_AND_MOBILE_KICK_OFF_STRUCT) );

	// ----------

	// 6/18/2015 rmd : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;

	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );

	memcpy( &ljob_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	memcpy( &tmkos.box_index_0, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	memcpy( &tmkos.station_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	// 5/9/2016 ajv : Acquire Flow Rates should run for up to 6 minutes, so set it accordingly.
	tmkos.time_seconds = (6 * 60);

	// 5/9/2016 ajv : And ensure we set the flag to acquire the expected flow rate.
	tmkos.set_expected = (true);

	// ----------

	FOAL_IRRI_initiate_a_station_test( &tmkos, IN_LIST_FOR_MOBILE );

	// ----------

	build_response_to_mobile_cmd_and_update_session_data( MID_TO_COMMSERVER_mobile_acquire_expected_flow_by_station_ack, ljob_number );
}

/* ---------------------------------------------------------- */
static void process_incoming_enable_or_disable_HUB_cmd( const DATA_HANDLE pdh )
{
	BOOL_32	lrcvd_option;

	UNS_8	*ucp;

	// ----------

	// 10/5/2015 ajv : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;

	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );

	memcpy( &lrcvd_option, ucp, sizeof(BOOL_32) );
	ucp += sizeof(BOOL_32);

	// ----------

	// 8/18/2016 rmd : A quick and dirty indication.
	if( config_c.purchased_options.option_HUB != lrcvd_option )
	{
		if( lrcvd_option )
		{
			Alert_Message( "switching -HUB option ON" );
		}
		else
		{
			Alert_Message( "switching -HUB option OFF" );
		}
	}

	// ----------
	
	// 4/25/2017 ajv : Set the option and save the file
	config_c.purchased_options.option_HUB = lrcvd_option;

	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_CONTROLLER_CONFIGURATION, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );

	// ----------

	// 4/25/2017 ajv : Take the mutex to protect against us missing a change due to multiple
	// processes manipulating the send_box_configuration_to_master flag.
	xSemaphoreTakeRecursive( irri_comm_recursive_MUTEX, portMAX_DELAY );

	// 4/25/2017 ajv : And trigger the configuration change to be sent to the master when the
	// next token arrives.
	irri_comm.send_box_configuration_to_master = (true);

	xSemaphoreGiveRecursive( irri_comm_recursive_MUTEX );

	// ----------

	CENT_COMM_build_and_send_ack_with_optional_job_number( UPORT_A, MID_TO_COMMSERVER_enable_or_disable_HUB_option_ack, JOB_NUMBER_NOT_INCLUDED, 0 );
	
	// ----------
	
	// 4/25/2017 ajv : And since we changed or ATTEMPTED a change to part of the registration
	// data, trigger sending the registration to the comm_server so we can see updated part
	// numbers. Use function as if the comm_server asked us to do this but set the flag to ACK
	// to (false) of course. Commserver is not expecting this ACK!
	process_incoming_request_to_send_registration( (false) );
	
	// ----------
	
	// 3/7/2017 rmd : Jay and I agreed that, for hubs and hub based controllers, following any
	// commserver initiated 'pseudo-poll' message we would send a 'no more messages' msg. This
	// message falls into that category being an unsolicited messages from the commserver.
	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
}

/* ---------------------------------------------------------- */
static void process_incoming_clear_rain_all_stations_cmd( const DATA_HANDLE pdh )
{
	UNS_32	ljob_number;

	UNS_8	*ucp;
	
	// ----------
	
	// 10/5/2015 ajv : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;
	
	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );
	
	memcpy( &ljob_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	// ----------
	
	STATION_set_ignore_moisture_balance_at_next_irrigation_all_stations( CHANGE_REASON_CENTRAL_OR_MOBILE );

	// ----------
	
	build_response_to_mobile_cmd_and_update_session_data( MID_TO_COMMSERVER_clear_rain_all_stations_ack, ljob_number );
}

/* ---------------------------------------------------------- */
static void process_incoming_clear_rain_by_group_cmd( const DATA_HANDLE pdh )
{
	UNS_32	ljob_number;

	UNS_32	lgroup_id;

	UNS_8	*ucp;
	
	// ----------
	
	// 10/5/2015 ajv : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;
	
	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );
	
	memcpy( &ljob_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	memcpy( &lgroup_id, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	// ----------
	
	STATION_set_ignore_moisture_balance_at_next_irrigation_by_group( lgroup_id, CHANGE_REASON_CENTRAL_OR_MOBILE );

	// ----------
	
	build_response_to_mobile_cmd_and_update_session_data( MID_TO_COMMSERVER_clear_rain_by_group_ack, ljob_number );
}

/* ---------------------------------------------------------- */
static void process_incoming_clear_rain_by_station_cmd( const DATA_HANDLE pdh )
{
	UNS_32	ljob_number;

	UNS_32	lbox_index_0;

	UNS_32	lstation_number_0;

	UNS_8	*ucp;
	
	// ----------
	
	// 10/5/2015 ajv : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;
	
	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );
	
	memcpy( &ljob_number, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	memcpy( &lbox_index_0, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	memcpy( &lstation_number_0, ucp, sizeof(UNS_32) );
	ucp += sizeof(UNS_32);

	// ----------
	
	STATION_set_ignore_moisture_balance_at_next_irrigation_by_station( lbox_index_0, lstation_number_0, CHANGE_REASON_CENTRAL_OR_MOBILE );

	// ----------
	
	build_response_to_mobile_cmd_and_update_session_data( MID_TO_COMMSERVER_clear_rain_by_station_ack, ljob_number );
}

/* ---------------------------------------------------------- */
static void process_incoming_request_for_alerts_cmd( const DATA_HANDLE pdh )
{
	// 8/3/2017 ajv : Set the alerts timer using the HIGH PRIORITY TIMEOUT to attempt to send 
	// them in 10-seconds. 
	CONTROLLER_INITIATED_alerts_timer_upkeep( CONTROLLER_INITIATED_HI_PRIORITY_SEND_ALERTS_TIMEOUT_MS, (true) );

	CENT_COMM_build_and_send_ack_with_optional_job_number( UPORT_A, MID_TO_COMMSERVER_send_engineering_alerts_ack, JOB_NUMBER_NOT_INCLUDED, 0 ); 
}

/* ---------------------------------------------------------- */
static void process_incoming_enable_or_disable_AQUAPONICS_cmd( const DATA_HANDLE pdh )
{
	BOOL_32	lrcvd_option;

	UNS_8	*ucp;

	// ----------

	// 10/5/2015 ajv : Pick out the job number and other pertinent data.
	ucp = pdh.dptr;

	ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );

	memcpy( &lrcvd_option, ucp, sizeof(BOOL_32) );
	ucp += sizeof(BOOL_32);

	// ----------

	// 10/8/2018 ajv : A quick and dirty indication.
	if( config_c.purchased_options.option_AQUAPONICS != lrcvd_option )
	{
		if( lrcvd_option )
		{
			Alert_Message( "switching -AQUAPONICS option ON" );
		}
		else
		{
			Alert_Message( "switching -AQUAPONICS option OFF" );
		}
	}

	// ----------
	
	// 10/8/2018 ajv : Set the option and save the file
	config_c.purchased_options.option_AQUAPONICS = lrcvd_option;

	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_CONTROLLER_CONFIGURATION, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );

	// ----------

	// 10/8/2018 ajv : Take the mutex to protect against us missing a change due to multiple
	// processes manipulating the send_box_configuration_to_master flag.
	xSemaphoreTakeRecursive( irri_comm_recursive_MUTEX, portMAX_DELAY );

	// 10/8/2018 ajv : And trigger the configuration change to be sent to the master when the
	// next token arrives.
	irri_comm.send_box_configuration_to_master = (true);

	xSemaphoreGiveRecursive( irri_comm_recursive_MUTEX );

	// ----------

	CENT_COMM_build_and_send_ack_with_optional_job_number( UPORT_A, MID_TO_COMMSERVER_enable_or_disable_AQUAPONICS_option_ack, JOB_NUMBER_NOT_INCLUDED, 0 );
	
	// ----------
	
	// 10/8/2018 ajv : And since we changed or ATTEMPTED a change to part of the registration
	// data, trigger sending the registration to the comm_server so we can see updated part
	// numbers. Use function as if the comm_server asked us to do this but set the flag to ACK
	// to (false) of course. Commserver is not expecting this ACK!
	process_incoming_request_to_send_registration( (false) );
	
	// ----------
	
	// 3/7/2017 rmd : Jay and I agreed that, for hubs and hub based controllers, following any
	// commserver initiated 'pseudo-poll' message we would send a 'no more messages' msg. This
	// message falls into that category being an unsolicited messages from the commserver.
	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
}

/* ---------------------------------------------------------- */
// POLL FUNCTIONALITY
/* ---------------------------------------------------------- */
static void process_commserver_sent_us_a_poll_packet( void )
{
	// 4/21/2014 rmd : Executed within the context of the RING_BUFFER_ANALYSIS task.

	// ----------
	
	if( !CONFIG_this_controller_is_a_configured_hub() && !CONFIG_this_controller_is_behind_a_hub() )
	{
		// 2/24/2017 rmd : We are not on a hub. And we are not a hub ourselves. So why did we get a
		// poll message from the commserver?
		Alert_Message( "CI: in-appropriate poll msg rcvd" );
	}
	else
	{
		// 2/24/2017 rmd : If the CI task is not in the ready mode that means it is waiting to
		// connect, connecting, or in device exchange mode. Curious it could even receive this
		// message when in there modes.
		if( cics.mode != CI_MODE_ready )
		{
			Alert_Message_va( "CI: poll msg when not ready : mode=%u", cics.mode );
		}
		else
		{
			// 11/2/2017 rmd : NOTE - we do not send an ACK to the commserver for this inbound message.
			// We do however send at least an outbound "no more messages" message.

			// 4/26/2017 rmd : And always queue up the 'no more messages' msg. So that eventually it
			// will be sent to the commserver to end the poll period.
			poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
		}
	}
}

/* ---------------------------------------------------------- */
static void process_no_more_messages_msg_ack( UNS_32 pmid )
{
	// 10/9/2013 rmd : If we were not waiting for this response make an alert. We could force
	// the disconnect but I think we'll wait for the response_timeout timer to expire.
	
	// 8/15/2014 rmd : Note - do not reset the waiting_for_xxx flag here. That is the
	// responsibility of the CI task before next outbound message is kicked out.

	if( !cics.waiting_for_the_no_more_messages_msg_response )
	{
		// 8/15/2014 rmd : Take no action here. Only alert and wait for the response timer to
		// expire.
		Alert_Message( "Not expecting a no more messages ACK." );
	}
	else
	{
		// 3/1/2017 rmd : Set the stage if there were a NAK possible. But in this case there is no
		// NAK to the 'no more messages' msg.
		if( pmid == MID_FROM_COMMSERVER_no_more_messages_ack )
		{
			// nothing to do here
		}

		// ----------

		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
static void process_hub_is_busy_msg_ack( UNS_32 pmid )
{
	// 8/15/2014 rmd : Note - do not reset the waiting_for_xxx flag here. That is the
	// responsibility of the CI task before next outbound message is kicked out.

	if( !cics.waiting_for_hub_is_busy_msg_response )
	{
		// 8/15/2014 rmd : Take no action here. Only alert and wait for the response timer to
		// expire.
		Alert_Message( "Not expecting a hub is busy ACK." );
	}
	else
	{
		// 3/1/2017 rmd : Set the stage if there were a NAK possible. But in this case there is no
		// NAK to the 'no more messages' msg.
		if( pmid == MID_FROM_COMMSERVER_hub_is_busy_ack )
		{
			// nothing to do here
		}

		// ----------

		// 4/20/2015 rmd : Internal housekeeping to wrap up the CI transaction.
		terminate_ci_transaction();
	}
}

/* ---------------------------------------------------------- */
static void process_incoming_hub_list_from_commserver( const DATA_HANDLE pdh )
{
	UNS_8		*ucp;
	
	UNS_32		remaining_length, result;

	UNS_32		temp_item;
	
	// ----------
	
	// 4/20/2017 rmd : Only a hub should rcv this incoming message from the commserver.
	if( !CONFIG_this_controller_is_a_configured_hub() )
	{
		Alert_Message( "NON HUB: rcvd hub list from commserver!" );	
	}
	else
	{
		/*
		a fake hub list about 100 controllers long

		UNS_32	iii; 

		for( iii=0; iii<48; iii++ )
		{
			temp_item = iii;
			
			xQueueSendToBack( router_hub_list_queue, &temp_item, 0 );
		}

		temp_item = 51099;
		xQueueSendToBack( router_hub_list_queue, &temp_item, 0 );

		temp_item = 0xffffffff;
		xQueueSendToBack( router_hub_list_queue, &temp_item, 0 );

		for( iii=0; iii<50; iii++ )
		{
			temp_item = iii + 500;
			
			xQueueSendToBack( router_hub_list_queue, &temp_item, 0 );
		}
		*/
		
		// ----------
		
		ucp = pdh.dptr;
	
		ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );
	
		// ----------
		
		// 4/20/2017 rmd : Calc the payload by stripping off the other packet content.
		remaining_length = pdh.dlen - sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) - sizeof(CRC_TYPE);
		
		// 4/30/2017 rmd : The hub list is a list of 3000 controller serial numbers and 2000 comm
		// addresses, each held in a 4 byte field, with the start of the 2000 list separated from
		// the 3000 list by 0xffffffff.
		if( (remaining_length>0) && ((remaining_length % sizeof(UNS_32)) == 0) )
		{
			Alert_Message_va( "HUB: rcvd hub list as %u bytes", remaining_length );
			
			// ----------
			
			// 4/30/2017 rmd : Maybe the list is shared between two tasks.
			xSemaphoreTake( router_hub_list_MUTEX, portMAX_DELAY );
			
			// ----------
			
			// 5/25/2017 rmd : Every time we connect we receive the hub list, so first empty it out so
			// we don't keep adding and adding to it for each connection. Be careful to only wait the 0
			// amount of time. Imagine what would happen if you waited portMAX_DELAY, app would crash!
			while( xQueueReceive( router_hub_list_queue, &temp_item, 0 ) );
			
			// ----------
			
			do
			{
				memcpy( &temp_item, ucp, sizeof(UNS_32) );
				
				ucp += sizeof(UNS_32);
				
				remaining_length -= sizeof(UNS_32);

				result = xQueueSendToBack( router_hub_list_queue, &temp_item, 0 );
			
				// 4/30/2017 rmd : Returns (true) if successful.
				if( result != pdTRUE )
				{
					Alert_Message( "HUB: hub list queue FULL" );
				}

			} while( (remaining_length > 0) && result );
			
			// ----------
			
			xSemaphoreGive( router_hub_list_MUTEX );
		}
		else
		{
			Alert_Message_va( "HUB ERROR: rcvd hub list as %u bytes", remaining_length );	
		}
		
		// ----------
	
		CENT_COMM_build_and_send_ack_with_optional_job_number( UPORT_A, MID_TO_COMMSERVER_here_is_the_hub_list_ack, JOB_NUMBER_NOT_INCLUDED, 0 );
		
		// ----------

		// 12/10/2018 rmd : Indicate we have a hub list to the code distribution task. Having a HUB
		// list in place is part of the criteria to begin a HUB code distribution.
		CODE_DISTRIBUTION_post_event( CODE_DISTRIBUTION_EVENT_hub_list_arrived );
		
		// ----------
		
		// 3/7/2017 rmd : Jay and I agreed that, for hubs and hub based controllers, following any
		// commserver initiated 'pseudo-poll' message we would send a 'no more messages' msg. This
		// message falls into that category being an unsolicited messages from the commserver.
		poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
	}
}

/* ---------------------------------------------------------- */
static void process_incoming_stop_and_cancel_hub_code_distribution( const DATA_HANDLE pdh )
{
	// 4/20/2017 rmd : Only a hub should rcv this incoming message from the commserver.
	if( !CONFIG_this_controller_is_a_configured_hub() )
	{
		Alert_Message( "NON HUB: rcvd stop and cancel hub distribution from commserver!" );	
	}
	else
	{
		// 12/10/2018 rmd : There is no data associated with this command. The needed action is to
		// stop and cancel the hub code distribution, and signal the comm server this hub is no
		// longer busy by sending a standard end of poll reponse.
		
		// 12/10/2018 rmd : STOP AND CANCEL HERE ...

		// ----------
	
		CENT_COMM_build_and_send_ack_with_optional_job_number( UPORT_A, MID_TO_COMMSERVER_stop_and_cancel_hub_code_distribution_ack, JOB_NUMBER_NOT_INCLUDED, 0 );
		
		// ----------
		
		// 3/7/2017 rmd : Jay and I agreed that, for hubs and hub based controllers, following any
		// commserver initiated 'pseudo-poll' message we would send a 'no more messages' msg. This
		// message falls into that category being an unsolicited messages from the commserver.
		poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
	}
}

/* ---------------------------------------------------------- */
static void process_incoming_start_the_hub_code_distribution( const DATA_HANDLE pdh )
{
	UNS_8		*ucp;
	
	UNS_32		duration_minutes;
	
	// ----------
	
	// 4/20/2017 rmd : Only a hub should rcv this incoming message from the commserver.
	if( !CONFIG_this_controller_is_a_configured_hub() )
	{
		Alert_Message( "NON HUB: rcvd start hub distribution from commserver!" );	
	}
	else
	{
		ucp = pdh.dptr;
	
		ucp += sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );
	
		// ----------
		
		// 12/10/2018 rmd : Pick out the permissible distribution duration.
		memcpy( &duration_minutes, ucp, sizeof(UNS_32) );

		// 12/10/2018 rmd : Try to start the hub code distribution. If eligible the code
		// distribution mode will be set, and this in turn will trigger the BUSY end of poll
		// to be sent following the processing of this incoming command. This is the ONLY place the
		// code distribution start is actually attempted.
		CODE_DISTRIBUTION_try_to_start_a_code_distribution( CODE_DISTRIBUTION_MODE_hub_transmitting_main );

		// ----------
	
		CENT_COMM_build_and_send_ack_with_optional_job_number( UPORT_A, MID_TO_COMMSERVER_start_the_hub_code_distribution_ack, JOB_NUMBER_NOT_INCLUDED, 0 );
		
		// ----------
		
		// 3/7/2017 rmd : Jay and I agreed that, for hubs and hub based controllers, following any
		// commserver initiated 'pseudo-poll' message we would send a 'no more messages' msg. This
		// message falls into that category being an unsolicited messages from the commserver.
		poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers();
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void process_packet_from_commserver( const DATA_HANDLE pdh )
{
	// 9/27/2017 rmd : Executed within the context of the RING_BUFFER_ANALYSIS task.
	//
	// 4/20/2015 rmd : The DATA_HANDLE contained in the pcmli parameter points to the message
	// itself. Not to any packet header structure. For the case of the ACK mids this DATA_HANDLE
	// is not relevant and should NOT BE USED. The rest of the PACKET_TOP_HEADER is valid. As is
	// the pcmli content.

	// ----------

	FROM_COMMSERVER_PACKET_TOP_HEADER	fcpth;
	
	BOOL_32		mid_processed;
	
	// ----------
	
	// 11/20/2015 rmd : Initialized to the normal case.
	mid_processed = (true);

	// 5/27/2015 rmd : All messages begin with the FROM_COMMSERVER_PACKET_TOP_HEADER. Pick it
	// off and use to see the mid.
	memcpy( &fcpth, pdh.dptr, sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) );

	// ----------

	switch( fcpth.mid )
	{
		case MID_FROM_COMMSERVER_REGISTRATION_DATA_full_receipt_ack:
		case MID_FROM_COMMSERVER_REGISTRATION_DATA_NAK:
			process_registration_data_ack( fcpth.mid, fcpth.to_network_id );
			break;

		case MID_FROM_COMMSERVER_ENGINEERING_ALERTS_full_receipt_ack:
		case MID_FROM_COMMSERVER_ENGINEERING_ALERTS_NAK:
			process_engineering_alerts_ack( fcpth.mid );
			break;

		case MID_FROM_COMMSERVER_FLOW_RECORDING_full_receipt_ack:
		case MID_FROM_COMMSERVER_FLOW_RECORDING_NAK:
			process_flow_recording_ack( fcpth.mid );
			break;

		case MID_FROM_COMMSERVER_MOISTURE_SENSOR_RECORDER_full_receipt_ack:
		case MID_FROM_COMMSERVER_MOISTURE_SENSOR_RECORDER_NAK:
			process_moisture_sensor_recording_ack( fcpth.mid );
			break;

		case MID_FROM_COMMSERVER_STATION_HISTORY_full_receipt_ack:
		case MID_FROM_COMMSERVER_STATION_HISTORY_NAK:
			process_station_history_ack( fcpth.mid );
			break;

		case MID_FROM_COMMSERVER_STATION_REPORT_DATA_full_receipt_ack:
		case MID_FROM_COMMSERVER_STATION_REPORT_DATA_NAK:
			process_station_report_data_ack( fcpth.mid );
			break;

		case MID_FROM_COMMSERVER_POC_REPORT_DATA_full_receipt_ack:
		case MID_FROM_COMMSERVER_POC_REPORT_DATA_NAK:
			process_poc_report_data_ack( fcpth.mid );
			break;

		case MID_FROM_COMMSERVER_SYSTEM_REPORT_DATA_full_receipt_ack:
		case MID_FROM_COMMSERVER_SYSTEM_REPORT_DATA_NAK:
			process_system_report_data_ack( fcpth.mid );
			break;

		case MID_FROM_COMMSERVER_LIGHTS_REPORT_DATA_full_receipt_ack:
		case MID_FROM_COMMSERVER_LIGHTS_REPORT_DATA_NAK:
			process_lights_report_data_ack( fcpth.mid );
			break;

		case MID_FROM_COMMSERVER_BUDGET_REPORT_DATA_full_receipt_ack:
		case MID_FROM_COMMSERVER_BUDGET_REPORT_DATA_NAK:
		case MID_FROM_COMMSERVER_BUDGET_REPORT_DATA_full_receipt_ack2:
		case MID_FROM_COMMSERVER_BUDGET_REPORT_DATA_NAK2:
			process_budget_report_data_ack( fcpth.mid );
			break;

		case MID_FROM_COMMSERVER_ET_AND_RAIN_TABLE_full_receipt_ack:
		case MID_FROM_COMMSERVER_ET_AND_RAIN_TABLE_NAK:
			process_et_rain_tables_ack( fcpth.mid );
			break;
			
		case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates:
		case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_both:
		case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_main:
		case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_tpmicro:
		case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_one_update_available:
		case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_one_update_but_distribute_both:
		case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_two_updates_available:
			process_check_for_updates_ack( fcpth.mid );
			break;

		// 6/11/2015 rmd : The ACK from the commserver for the ET and RAIN message we sent there.
		case MID_FROM_COMMSERVER_WEATHER_DATA_receipt_ack:
		case MID_FROM_COMMSERVER_WEATHER_DATA_NAK:
			process_from_commserver_weather_data_receipt_ack();
			break;

		// 6/11/2015 rmd : The ACK from the commserver for the RAIN_INDICATION message we sent
		// there.
		case MID_FROM_COMMSERVER_RAIN_INDICATION_receipt_ack:
		case MID_FROM_COMMSERVER_RAIN_INDICATION_NAK:
			process_rain_indication_ack();
			break;

		// ----------
		
		case MID_FROM_COMMSERVER_VERIFY_FIRMWARE_VERSION_ACK_FW_UP_TO_DATE:
		case MID_FROM_COMMSERVER_VERIFY_FIRMWARE_VERSION_ACK_FW_OUT_OF_DATE:
			process_verify_firmware_version_ack( fcpth.mid );
			break;

		case MID_FROM_COMMSERVER_PROGRAM_DATA_receipt_ack:
		case MID_FROM_COMMSERVER_PROGRAM_DATA_NAK:
			process_commserver_rcvd_program_data_from_us_ack( fcpth.mid );
			break;

		// ----------
		
		case MID_FROM_COMMSERVER_IHSFY_program_data:
			process_commserver_sent_us_an_ihsfy_packet();
			break;
			
		case MID_FROM_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_ACK_FW_UP_TO_DATE:
		case MID_FROM_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_ACK_FW_OUT_OF_DATE:
			process_check_firmware_version_before_pdata_request_ack( fcpth.mid );
			break;

		case MID_FROM_COMMSERVER_PROGRAM_DATA_REQUEST_ACK_NO_CHANGES:
		case MID_FROM_COMMSERVER_PROGRAM_DATA_REQUEST_ACK_CHANGES_AVAILABLE:
		case MID_FROM_COMMSERVER_PROGRAM_DATA_REQUEST_NAK:
			process_asking_commserver_if_there_is_program_data_for_us_ack();
			break;

		// ----------

		case MID_FROM_COMMSERVER_set_your_time_and_date:
			process_set_time_and_date_from_commserver( pdh );
			break;

		// ----------

		case MID_FROM_COMMSERVER_WEATHER_DATA_packet:
			process_incoming_weather_data_message( pdh );
			break;

		case MID_FROM_COMMSERVER_RAIN_SHUTDOWN_packet:
			process_incoming_rain_shutdown_message( pdh );
			break;
			
		// ----------
		
		case MID_FROM_COMMSERVER_send_a_status_screen:
			process_request_to_send_status();
			break;
			
		case MID_FROM_COMMSERVER_status_screen_ack_packet:
			process_ack_from_commserver_for_status_screen();
			break;
			
		case MID_FROM_COMMSERVER_mobile_station_ON:
			process_incoming_mobile_station_ON( pdh );
			break;
			
		case MID_FROM_COMMSERVER_mobile_station_OFF:
			process_incoming_mobile_station_OFF( pdh );
			break;
			
		// ----------

		case MID_FROM_COMMSERVER_panel_swap_factory_reset_to_new_panel:
			process_request_factory_reset_panel_swap_for_new_panel( pdh );
			break;

		case MID_FROM_COMMSERVER_panel_swap_factory_reset_to_old_panel:
			process_request_factory_reset_panel_swap_for_old_panel( pdh );
			break;

		case MID_FROM_COMMSERVER_set_all_bits:
			process_send_all_program_data();
			break;
			
		// ----------

		case MID_FROM_COMMSERVER_mobile_MVOR:
			process_incoming_mobile_mvor_cmd( pdh );
			break;

		// ----------

		case MID_FROM_COMMSERVER_mobile_light_cmd:
			process_incoming_mobile_lights_cmd( pdh );
			break;
			
		// ----------

		case MID_FROM_COMMSERVER_mobile_turn_controller_on_off:
			process_incoming_mobile_turn_controller_on_off_cmd( pdh );
			break;

		// ----------

		case MID_FROM_COMMSERVER_mobile_now_days_all_stations:
			process_incoming_mobile_now_days_all_stations_cmd( pdh );
			break;

		case MID_FROM_COMMSERVER_mobile_now_days_by_group:
			process_incoming_mobile_now_days_by_group_cmd( pdh );
			break;

		case MID_FROM_COMMSERVER_mobile_now_days_by_station:
			process_incoming_mobile_now_days_by_station_cmd( pdh );
			break;

		case MID_FROM_COMMSERVER_mobile_now_days_by_box:
			process_incoming_mobile_now_days_by_box_cmd( pdh );
			break;

		// ----------

		case MID_FROM_COMMSERVER_force_registration:
			process_incoming_request_to_send_registration( (true) );
			break;

		// ----------
					
		case MID_FROM_COMMSERVER_enable_or_disable_FL_option:
			process_incoming_enable_or_disable_FL_cmd( pdh );
			break;

		// ----------

		case MID_FROM_COMMSERVER_clear_MLB:
			process_incoming_clear_mlb_cmd( pdh );
			break;

		// ----------

		case MID_FROM_COMMSERVER_stop_all_irrigation:
			process_incoming_stop_all_irrigation_cmd( pdh );
			break;

		// ----------

		case MID_FROM_COMMSERVER_stop_irrigation:
			process_incoming_stop_irrigation_cmd( pdh );
			break;

		// ----------

		case MID_FROM_COMMSERVER_check_for_updates:
			process_incoming_request_to_check_for_updates( pdh );
			break;

		// ----------

		case MID_FROM_COMMSERVER_perform_two_wire_discovery:
			process_incoming_perform_two_wire_discovery_cmd( pdh );
			break;

		// ----------

		case MID_FROM_COMMSERVER_mobile_acquire_expected_flow_by_station:
			process_incoming_mobile_acquire_expected_flow_by_station_cmd( pdh );
			break;

		// ----------

		case MID_FROM_COMMSERVER_enable_or_disable_HUB_option:
			process_incoming_enable_or_disable_HUB_cmd( pdh );
			break;

		// ----------

		case MID_FROM_COMMSERVER_clear_rain_all_stations:
			process_incoming_clear_rain_all_stations_cmd( pdh );
			break;

		case MID_FROM_COMMSERVER_clear_rain_by_group:
			process_incoming_clear_rain_by_group_cmd( pdh );
			break;

		case MID_FROM_COMMSERVER_clear_rain_by_station:
			process_incoming_clear_rain_by_station_cmd( pdh );
			break;

		// ----------

		case MID_FROM_COMMSERVER_send_engineering_alerts:
			process_incoming_request_for_alerts_cmd( pdh );
			break;

		// ----------

		case MID_FROM_COMMSERVER_enable_or_disable_AQUAPONICS_option:
			process_incoming_enable_or_disable_AQUAPONICS_cmd( pdh );
			break;

		// ----------

		case MID_FROM_COMMSERVER_go_ahead_send_your_messages:
			process_commserver_sent_us_a_poll_packet();
			break;
			
		case MID_FROM_COMMSERVER_no_more_messages_ack:
			process_no_more_messages_msg_ack( fcpth.mid );
			break;

		case MID_FROM_COMMSERVER_hub_is_busy_ack:
			process_hub_is_busy_msg_ack( fcpth.mid );
			break;

		// ----------
		
		case MID_FROM_COMMSERVER_here_is_the_hub_list:
			process_incoming_hub_list_from_commserver( pdh );
			break;
			
		case MID_FROM_COMMSERVER_stop_and_cancel_hub_code_distribution:
			process_incoming_stop_and_cancel_hub_code_distribution( pdh );
			break;
			
		case MID_FROM_COMMSERVER_start_the_hub_code_distribution:
			process_incoming_start_the_hub_code_distribution( pdh );
			break;
			
		// ----------

		// 3/27/2014 rmd : NOTE - as you add commands make sure you CONSIDER if they should be
		// processed. Particularly the irrigation activity commands. Such as turning on a valve for
		// the mobile app! For example if the chain is down those should not be acted upon.

		default:
			// 11/20/2015 rmd : An unknown mid. Flag not processed and make an alert.
			mid_processed = (false);

			Alert_Message_va( "Unhandled MID %d from COMMSERVER", fcpth.mid );
			break;
	}

	// ----------
	
	if( mid_processed )
	{
		// 12/22/2014 ajv : Update the Comm Test string so the user can see that the last command
		// was successful.
		ALERTS_parse_comm_command_string( fcpth.mid, GuiVar_CommTestStatus, sizeof(GuiVar_CommTestStatus) );
	
		// 4/17/2015 ajv : If the user is viewing the Communication Test screen, we should refresh
		// the cursors to show the updated text.
		if( GuiLib_CurStructureNdx == GuiStruct_scrCommTest_0 )
		{
			Refresh_Screen();
		}
	}
}

/* ---------------------------------------------------------- */
typedef struct
{
	MSG_INITIALIZATION_STRUCT	init_struct;

	UNS_32	next_expected_packet;

	UNS_32	bytes_rcvd_so_far;

	UNS_8	*ptr_to_next_packet_destination;

	UNS_8	*ptr_to_msg_being_built;

} COMMSERVER_MSG_RECEIPT_CONTROL_STRUCT;

static	COMMSERVER_MSG_RECEIPT_CONTROL_STRUCT	msg_being_built;

/* ---------------------------------------------------------- */
// 4/20/2015 rmd : Allow 30 seconds for the next data packet to show. Just picked this
// length. Seemed reasonable.
#define COMMSERVER_MSG_RECEIPT_ERROR_TIMER_MS		(30000)

extern void commserver_msg_receipt_error_timer_callback( xTimerHandle pxTimer )
{
	// 4/22/2014 rmd : Executed within the context of the high priority timer task.
	// 4/20/2015 rmd : Free any alocated memory. And zero the msg_being_built structure to
	// indicate nothing being received.
	if( msg_being_built.ptr_to_msg_being_built )
	{
		mem_free( msg_being_built.ptr_to_msg_being_built );	
	}
	
	memset( &msg_being_built, 0x00, sizeof(msg_being_built) );
}

/* ---------------------------------------------------------- */
static void process_incoming_commserver_init_packet( const DATA_HANDLE pdh )
{ 
	UNS_8		*ucp;
	
	// ----------

	// 4/20/2015 rmd : Test for any prior allocations. There shouldn't be one. But could be I
	// suppose if a msg was being rcvd and an init packet showed in error BEFORE the msg receipt
	// error timer expired.
	if( msg_being_built.ptr_to_msg_being_built )
	{
		Alert_Message( "Commserver msg receipt unexpected allocation" );
		
		mem_free( msg_being_built.ptr_to_msg_being_built );	
	}
	
	// ----------

	// 3/31/2014 rmd : Get to the initialization info in the incoming message.
	ucp = pdh.dptr + sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER);

	// 3/31/2014 rmd : And copy it to our control structure.
	memcpy( &msg_being_built.init_struct, ucp, sizeof(MSG_INITIALIZATION_STRUCT) );

	// ----------
	
	// 4/20/2015 rmd : Debug alerts.
	//#if 0
	Alert_inbound_message_size( msg_being_built.init_struct.mid, msg_being_built.init_struct.expected_size, msg_being_built.init_struct.expected_packets );

	//Alert_Message_va( "inbound msg    size: %d", msg_being_built.init_struct.expected_size );
	//Alert_Message_va( "inbound msg     crc: 0x%08lX", msg_being_built.init_struct.expected_crc );
	//Alert_Message_va( "inbound msg packets: %d", msg_being_built.init_struct.expected_packets );
	//Alert_Message_va( "inbound msg     mid: %d", msg_being_built.init_struct.mid );
	//#endif

	// ----------
	
	msg_being_built.bytes_rcvd_so_far = 0;

	msg_being_built.next_expected_packet = 1;

	msg_being_built.ptr_to_next_packet_destination = NULL;

	// ----------
	
	// 4/20/2015 rmd : Is the memory available to receive the indicated message? If so allocate
	// it and send the ACK to the init packet.
	if( !mem_obtain_a_block_if_available( msg_being_built.init_struct.expected_size, (void**)&msg_being_built.ptr_to_msg_being_built ) )
	{
		Alert_Message( "Commserver msg receipt memory unavailable" );
		
		// 8/21/2014 rmd : Make SURE the memory ptr is NULL! So if we rcv a data packet we do not
		// try to store it somewhere. We check for this NULL ptr there. Zero the whole control
		// sturcutre to indicate no msg being rcvd.
		memset( &msg_being_built, 0x00, sizeof(msg_being_built) );
	}
	else
	{
		// 3/31/2014 rmd : NOTE - the allocated memory is freed if there is an error during receipt
		// of the data packets. If receipt is sucessful however, the memory is freed after the
		// message is processed.

		// ----------

		msg_being_built.ptr_to_next_packet_destination = msg_being_built.ptr_to_msg_being_built;
		
		// ----------
			
		// 5/14/2015 rmd : The program data init packet is ACK'd. And we need to set the mid to the
		// expected data packet value.
		if( msg_being_built.init_struct.mid == MID_FROM_COMMSERVER_PROGRAM_DATA_init_packet )
		{
			msg_being_built.init_struct.mid = MID_FROM_COMMSERVER_PROGRAM_DATA_data_packet;

			// 6/4/2015 rmd : Send the ACK response. The PDATA init packet is ACK'd. We likely wouldn't
			// ACK this packet if we were to design the messaging scheme from fresh. But we followed
			// suit with how the code update message was received. In that case checking for the memory
			// and not sending the message if the memory is not available is a cell data plan
			// consideration. Not really needed for pdata but it is in place now.
			CENT_COMM_build_and_send_ack_with_optional_job_number( UPORT_A, MID_TO_COMMSERVER_PROGRAM_DATA_init_ack, JOB_NUMBER_NOT_INCLUDED, 0 );
		}
		else
		{
			Alert_Message( "From commserver unexpected mid" );
			
			// 4/20/2015 rmd : Since we are going to start the timer the memory will be freed upon that
			// timeout.
		}
		
		// ----------
		
		// 4/20/2015 rmd : And start the code receipt error timer to guard the process.
		xTimerChangePeriod( comm_mngr.timer_commserver_msg_receipt_error, MS_to_TICKS(COMMSERVER_MSG_RECEIPT_ERROR_TIMER_MS), portMAX_DELAY );
	}
}

/* ---------------------------------------------------------- */
static void process_incoming_commserver_data_packet( const DATA_HANDLE pdh )
{ 
	UNS_8	*ucp;

	MSG_DATA_PACKET_STRUCT	packet_struct;

	UNS_32	packet_data_size;
	
	BOOL_32	msg_error;

	CRC_BASE_TYPE	lcrc;
	
	DATA_HANDLE		ldh;
	
	// ----------
	
	// 4/20/2015 rmd : We've got control right now. So stop the timer. If there are any errors
	// we will free the memory and wipe the control structure within this function.
	xTimerStop( comm_mngr.timer_commserver_msg_receipt_error, portMAX_DELAY );
	
	// ----------

	ucp = pdh.dptr;
	
	ucp += sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER);

	memcpy( &packet_struct, ucp, sizeof(MSG_DATA_PACKET_STRUCT) );

	ucp += sizeof(MSG_DATA_PACKET_STRUCT);

	// ----------
	
	msg_error = (true);
	
	// 4/20/2015 rmd : Are the memory ptrs non-NULL?
	if( msg_being_built.ptr_to_msg_being_built && msg_being_built.ptr_to_next_packet_destination )
	{
		if( msg_being_built.init_struct.mid == packet_struct.mid )
		{
			if( (msg_being_built.next_expected_packet == packet_struct.packet_number) && (msg_being_built.init_struct.expected_packets >= packet_struct.packet_number) )
			{
				// 4/20/2015 rmd : Calc the payload by stripping off the other packet content.
				packet_data_size = (pdh.dlen - sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) - sizeof(MSG_DATA_PACKET_STRUCT) - sizeof(CRC_TYPE));
		
				// 4/20/2015 rmd : Test we won't blow the memory.
				if( (msg_being_built.bytes_rcvd_so_far + packet_data_size) <= msg_being_built.init_struct.expected_size )
				{
					// 4/20/2015 rmd : Made it through all the tests.
					msg_error = (false);
					
					// ----------

					// 4/20/2015 rmd : Take the packet content.
					memcpy( msg_being_built.ptr_to_next_packet_destination, ucp, packet_data_size );
		
					msg_being_built.bytes_rcvd_so_far += packet_data_size;
		
					// ----------
					
					// 9/27/2017 rmd : Is the whole message here yet?
					if( (msg_being_built.init_struct.expected_packets == packet_struct.packet_number) && (msg_being_built.bytes_rcvd_so_far == msg_being_built.init_struct.expected_size) )
					{
						lcrc = CRC_calculate_32bit_big_endian( msg_being_built.ptr_to_msg_being_built, msg_being_built.init_struct.expected_size );
						
						// 4/20/2015 rmd : Does it match?
						if( lcrc == msg_being_built.init_struct.expected_crc )
						{
							// 4/20/2015 rmd : Okay the message is good. Get it into a data handle.
							ldh.dptr = msg_being_built.ptr_to_msg_being_built;
							
							ldh.dlen = msg_being_built.init_struct.expected_size;
							
							// ----------
							
							if( msg_being_built.init_struct.mid == MID_FROM_COMMSERVER_PROGRAM_DATA_data_packet )
							{
								Alert_comm_command_sent( MID_TO_COMMSERVER_PROGRAM_DATA_receipt_ack );
								
								CENT_COMM_build_and_send_ack_with_optional_job_number( UPORT_A, MID_TO_COMMSERVER_PROGRAM_DATA_receipt_ack, JOB_NUMBER_NOT_INCLUDED, 0 );

								process_incoming_program_data_message( ldh );
							}
							else
							{
								Alert_Message( "From commserver: rcvd UNKNOWN msg" );
							}
						}
						else
						{
							// 8/21/2014 rmd : If CRC fails make an alert message.
							//Alert_Message_va( "Commserver inbound msg CRC failed! (saw %08x, exp %08x)", lcrc, msg_being_built.init_struct.expected_crc );
							Alert_Message( "Commserver inbound msg CRC failed!"  );
						}

						// ----------
						
						// 4/20/2015 rmd : So whether the CRC failed or not free the memory and wipe the control
						// structure.
						if( msg_being_built.ptr_to_msg_being_built )
						{
							mem_free( msg_being_built.ptr_to_msg_being_built );
						}
						
						memset( &msg_being_built, 0x00, sizeof(msg_being_built) );
					}
					else
					{
						// 4/22/2014 rmd : Restart the error timer. We rcvd an acceptable packet.
						xTimerChangePeriod( comm_mngr.timer_commserver_msg_receipt_error, MS_to_TICKS(COMMSERVER_MSG_RECEIPT_ERROR_TIMER_MS), portMAX_DELAY );
	
						// 4/20/2015 rmd : Wasn't the last packet. More to come. Update accumulators to recieve
						// more.
						msg_being_built.next_expected_packet += 1;
		
						msg_being_built.ptr_to_next_packet_destination += packet_data_size;
					}

				}  // too much for allocated memory
				else
				{
					Alert_Message( "Commserver inbound msg bigger than expected!"  );
				}
			}
			else
			{
				//Alert_Message_va( "CODE RECEIPT: unexp packet number (saw %d; exp %d)", packet_struct.packet_number, dss.next_expected_packet );
				Alert_Message( "Commserver inbound msg unexp packet!"  );
			}
		}
		else
		{
			//Alert_Message_va( "CODE RECEIPT: unexp MID (saw %d; exp %d)", packet_struct.mid, dss.init_struct.mid );
			Alert_Message( "Commserver inbound msg unexp MID!"  );
		}

	}  // of no memory allocated

	// ----------
	
	if( msg_error )
	{
		if( msg_being_built.ptr_to_msg_being_built )
		{
			mem_free( msg_being_built.ptr_to_msg_being_built );
		}
		
		memset( &msg_being_built, 0x00, sizeof(msg_being_built) );
	}
}	

/* ---------------------------------------------------------- */
extern void CENT_COMM_process_incoming_packet_from_comm_server( const UNS_32 pfrom_which_port, const DATA_HANDLE pdh, const UNS_32 pclass )
{
	// 4/21/2014 rmd : Executed within the context of the RING_BUFFER_ANALYSIS task.

	// 4/15/2015 rmd : Some packet MID's are the ENTIRE message. These are typically any ACKs to
	// data sent from the controller to the comm server. But also include the IHSFY (I Have
	// Something For You) packet type that the commserver sends to us. These single packet
	// messages will be picked out and processed as complete messages right away. The rest of
	// the MIDs will be init packets and data packets for a variety of MID's (so far only
	// program data). They are also processed within this context.
	
	// ----------
	
	// 3/31/2014 rmd : We need to find which incoming command we need to process. All packets
	// from the commserver start with the FROM_COMMSERVER_MESSAGE_HEADER.
	FROM_COMMSERVER_PACKET_TOP_HEADER	fcpth;

	// ----------
	
	// 5/23/2017 rmd : By definition only packets that arrived on PORT_A and have the
	// MSG_CLASS_CS3000_FROM_COMMSERVER should pass through this function.
	if( (pfrom_which_port == UPORT_A) && (pclass == MSG_CLASS_CS3000_FROM_COMMSERVER) )
	{
		// 5/23/2017 rmd : All packets have this header on them. So load it so we can see the mid.
		memcpy( &fcpth, pdh.dptr, sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) );
	
		// ----------
		
		// 4/20/2015 rmd : Regardless of mid, valid or not, set this. As it turns out the last
		// communicated is not used at the moment. To satify the WATER SENSE requirement to show if
		// comms are up we can use the connected function for the internet connected device.
		update_when_last_communicated();
		
		// ----------
		
		// 8/23/2013 rmd : Generate an alert for the incoming command. Exempting commands that
		// repeat frequently. Exclude the DATA packets.
		if( (fcpth.mid != MID_FROM_COMMSERVER_PROGRAM_DATA_data_packet) &&
			(fcpth.mid != MID_FROM_COMMSERVER_status_screen_ack_packet)
		  )
		{
			Alert_comm_command_received( fcpth.mid );
		}
	
		// ----------
		
		if( fcpth.mid == MID_FROM_COMMSERVER_PROGRAM_DATA_init_packet )
		{
			// 4/17/2015 rmd : We allow the receipt of init packets and data packets from the commserver
			// at all times. The acknowledge of and processing of the final message will be qualified by
			// the 'allowed_to_process_irrigation_commands' parameter.
			process_incoming_commserver_init_packet( pdh );
		}
		else
		if( fcpth.mid == MID_FROM_COMMSERVER_PROGRAM_DATA_data_packet )
		{
			// 4/17/2015 rmd : We allow the receipt of init packets and data packets from the commserver
			// at all times. The acknowledge of and processing of the final message will be qualified by
			// the 'allowed_to_process_irrigation_commands' parameter.
			process_incoming_commserver_data_packet( pdh );
		}
		else
		{
			process_packet_from_commserver( pdh );
		}
	}
	else
	{
		Alert_Message_va( "ROUTER: attempt to process unexp packet, port %s, class %u", port_names[ pfrom_which_port ], pclass );
	}

	// ----------
	
	// 3/27/2014 rmd : NOTE - as you add commands make sure you CONSIDER if they should be
	// processed. Particularly the irrigation activity commands. Such as turning on a valve for
	// the mobile app! For example if the chain is down those should not be acted upon.
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

