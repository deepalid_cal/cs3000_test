/*  file = irri_comm.c                       2011.08.01  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy
#include	<string.h>

#include	"irri_comm.h"

#include	"comm_mngr.h"

#include	"app_startup.h"

#include	"alert_parsing.h"

#include	"alerts.h"

#include	"bithacks.h"

#include	"flash_storage.h"

#include	"irri_flow.h"

#include	"irri_irri.h"

#include	"irri_lights.h"

#include	"irri_subs.h"

#include	"foal_irri.h"

#include	"foal_flow.h"

#include	"foal_comm.h"

#include	"foal_lights.h"

#include	"packet_router.h"

#include	"pdata_changes.h"

#include	"configuration_controller.h"

#include	"td_check.h"

#include	"r_alerts.h"

#include	"change.h"

#include	"tpmicro_data.h"

#include	"station_groups.h"

#include	"manual_programs.h"

#include	"flowsense.h"

#include	"cs_mem.h"

#include	"screen_utils.h"

#include	"weather_control.h"

#include	"epson_rx_8025sa.h"

#include	"moisture_sensors.h"

#include	"two_wire_utils.h"

#include	"walk_thru.h"

#include	"chain_sync_funcs.h"

#include	"chain_sync_vars.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

IRRI_COMM		irri_comm;	// all 0 on program start by compiler load

/* ---------------------------------------------------------- */
extern BOOL_32 IRRI_COMM_if_any_2W_cable_is_over_heated( void )
{
	BOOL_32	rv;
	
	UNS_32	i;
	
	rv = (false);
	
	for( i=0; i<MAX_CHAIN_LENGTH; i++ )
	{
		if( irri_comm.two_wire_cable_overheated[ i ] )
		{
			rv = (true);
		}
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern void IRRI_COMM_process_incoming__clean_house_request( COMMUNICATION_MESSAGE_LIST_ITEM *cmli )
{
	// 4/15/2014 rmd : This function, executed within the context of the comm_mngr must do some
	// serious house cleaning work as this controller has been determined to be a new member of
	// the chain. That is based upon this controller index was not present after past scans. A
	// serial number match IS a criteria for the NEW declaration.

	// ----------
	
	// 3/21/2012 rmd : Remember tokens are BROADCAST. Only respond if to us.
	if( this_communication_address_is_our_serial_number( (UNS_8 *)&(cmli->from_to.to), THE_PASSED_IN_DATA_IS_LITTLE_ENDIAN  ) )
	{
		// 8/8/2016 rmd : NOTE - I DO QUESTION why the CLEAN HOUSE command isn't broadcast. Meaning
		// why doesn't everyone (except 'A' controller of course) act upon it. Well it is this way
		// for now and I'm not going to change for fear of breaking something.

		// ----------
		
		Alert_Message( "Clean House request received" );
		
		// ----------

		// 4/16/2014 rmd : Note there is no data accompanying this incoming command. And we have
		// already seen the command. Just process and respond.
	
		// 4/15/2014 rmd : Step ONE - we want to roll through the station list deleting all the
		// stations. Stations could be physically at this controller or a different controller. They
		// all go. Then we add back in the 'dummy' hidden station needed to avoid empty list
		// problems.
		STATION_clean_house_processing();
		
		// ----------
		
		// 4/15/2014 rmd : Step TWO - we want to roll through the poc list deleting all the pocs.
		// POCs could be physically at this controller or a different controller. They all
		// go. Then we add back in the 'dummy' hidden POC needed to avoid empty list problems.
		POC_clean_house_processing();
		
		// ----------

		// 2/5/2016 rmd : Roll through the moisture sensor list deleting all of them. And then add
		// back in the 'dummy' hidden sensor to avoid empty list problems.
		MOISTURE_SENSOR_clean_house_processing();
		
		// ----------

		// 5/13/2014 ajv : Clean the rest of the groups. Specifically,
		// Irrigation Systems, Manual Programs, and Station Groups.

		SYSTEM_clean_house_processing();
		
		MANUAL_PROGRAMS_clean_house_processing();
		
		STATION_GROUP_clean_house_processing();
		
		LIGHTS_clean_house_processing();
		
		WALK_THRU_clean_house_processing();
		
		// ----------

		// 4/25/2014 rmd : NO need to wipe the chain members copy of whats installed to trigger the
		// re-creation of the stations. Ask the tpmicro to send the whats_installed. When it arrives
		// the content is compared to the various hardware GROUP FILES. And if say a station can't
		// be found it is created. This has NOTHING to do with the content of the chain_members!
		tpmicro_data.request_whats_installed_from_tp_micro = (true);
		
		// ----------
	
		COMM_UTIL_build_and_send_a_simple_flowsense_message( MID_IRRI_TO_FOAL__CLEAN_HOUSE_REQUEST_RESP, cmli, MSG_CLASS_CS3000_TOKEN_RESP );
		
	}  // of if the message was actually addressed to us
	
	// ----------
	
	// 11/26/2014 rmd : Because a CLEAN_HOUSE command was sent to someone in the chain it means
	// the chain has fundamentally changed. As such we are going to wipe all irrigation. We are
	// intentionally doing this outside of the test if the CLEAN HOUSE message is for us or not.
	// We want ALL controllers in the chain to wipe their irrigation if there are CLEAN HOUSE
	// requests being issued.
	//
	// 11/26/2014 rmd : Originally AJ and I thought that we should only wipe the IRRI side
	// irrigation here. But I see no harm in also going after the FOAL side irrigation. Slaves
	// shouldn't have any. And masters should have already done this when a NEW controller was
	// detected!
	FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation();

	FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights();
}

/* ---------------------------------------------------------- */
extern void IRRI_COMM_process_incoming__crc_error_clean_house_request( COMMUNICATION_MESSAGE_LIST_ITEM *cmli )
{
	// 11/29/2018 dmd : This function is very similar to the above clean house function. It is
	// executed within the context of the comm_mngr when a specific file clean up command is
	// sent. The function extracts the file index from the message and does a house cleaning
	// work as this controller has been determined to have a corrupted file.
	
	UNS_32 ff_index;
		
	UNS_8 *temp_dptr;

	// ----------
	
	// 3/21/2012 rmd : Remember tokens are BROADCAST. Only respond if to us.
	if( this_communication_address_is_our_serial_number( (UNS_8 *)&(cmli->from_to.to), THE_PASSED_IN_DATA_IS_LITTLE_ENDIAN  ) )
	{
		// 8/8/2016 rmd : NOTE - I DO QUESTION why the CLEAN HOUSE command isn't broadcast. Meaning
		// why doesn't everyone (except 'A' controller of course) act upon it. Well it is this way
		// for now and I'm not going to change for fear of breaking something.

		// ----------
		
		Alert_Message( "Clean House request received" );
		
		// ----------
		
		temp_dptr = cmli->dh.dptr;
		
		memcpy ( &ff_index, (temp_dptr + sizeof (COMM_COMMANDS)), sizeof(UNS_32));
		
		chain_sync_file_pertinants [ ff_index ].__clean_house_processing();

		// ----------

		// 4/25/2014 rmd : NO need to wipe the chain members copy of whats installed to trigger the
		// re-creation of the stations. Ask the tpmicro to send the whats_installed. When it arrives
		// the content is compared to the various hardware GROUP FILES. And if say a station can't
		// be found it is created. This has NOTHING to do with the content of the chain_members!
		tpmicro_data.request_whats_installed_from_tp_micro = (true);
		
		// ----------
	
		COMM_UTIL_build_and_send_a_simple_flowsense_message( MID_IRRI_TO_FOAL__CLEAN_HOUSE_REQUEST_RESP, cmli, MSG_CLASS_CS3000_TOKEN_RESP );
		
	}  // of if the message was actually addressed to us
	
	// ----------
	
	// 11/26/2014 rmd : Because a CLEAN_HOUSE command was sent to someone in the chain it means
	// the chain has fundamentally changed. As such we are going to wipe all irrigation. We are
	// intentionally doing this outside of the test if the CLEAN HOUSE message is for us or not.
	// We want ALL controllers in the chain to wipe their irrigation if there are CLEAN HOUSE
	// requests being issued.
	//
	// 11/26/2014 rmd : Originally AJ and I thought that we should only wipe the IRRI side
	// irrigation here. But I see no harm in also going after the FOAL side irrigation. Slaves
	// shouldn't have any. And masters should have already done this when a NEW controller was
	// detected!
	FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation();

	FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights();
}

/* ---------------------------------------------------------- */
extern void IRRI_COMM_process_stop_command( const BOOL_32 pstop_all_irrigation )
{
	irri_comm.send_stop_key_record = (true);

	// ----------

	// 11/6/2014 rmd : Filling out the stop key instructions record could get complicated. For
	// example one might have to pass through the irri irrigation list and find the highest
	// reason in the list. Then allow the user to choose what he wants to stop. For now we will
	// just stop all reasons in all system.
	memset( &irri_comm.stop_key_record, 0x00, sizeof(STOP_KEY_RECORD_s) );

	if( pstop_all_irrigation )
	{
		irri_comm.stop_key_record.stop_in_all_systems = (true);
		irri_comm.stop_key_record.stop_for_all_reasons = (true);
	}
	else
	{
		irri_comm.stop_key_record.stop_for_the_highest_reason_in_all_systems = (true);
	}
}

/* ---------------------------------------------------------- */
static BOOL_32 extract_chain_members_array_from_the_incoming_flowsense_token( UNS_8 **pucp )
{
	// 5/1/2014 rmd : Take the mutex to protect against us missing a change due to multiple
	// processes manipulating the send_box_configuration_to_master flag.
	xSemaphoreTakeRecursive( irri_comm_recursive_MUTEX, portMAX_DELAY );

	// ----------

	// 4/25/2014 rmd : Hey guess what. If we are going to send our box_configuration, part of
	// which is held in OUR COPY of chain members, DO NOT accept this incoming version of
	// chain_members. We would just overwrite what we want to send to the master. This is safe
	// to do because our sending of our box_configuration will trigger another broadcast of the
	// chain members.
	if( irri_comm.send_box_configuration_to_master )
	{
		// 4/25/2014 rmd : A debug line.
		// 8/18/2014 ajv : Suppressed for release to Bakersfield
		//Alert_Message( "Chain Members dropped" );
	}
	else
	{
		// 4/25/2014 rmd : A debug line.
		// 8/18/2014 ajv : Suppressed for release to Bakersfield
		//Alert_Message( "Chain Members received" );

		// ----------
		
		xSemaphoreTakeRecursive( chain_members_recursive_MUTEX, portMAX_DELAY );
	
		// ----------

		// 2/6/2015 rmd : Update chain_members with the latest NETWORK copy.
		memcpy( &chain.members, *pucp, sizeof(chain.members) );
	
		// ----------
	
		// 2/6/2015 rmd : And now cause AGAIN the BOX_CONFIGURATION structure to be sent to the
		// master. Whenever it is received from the tpmicro it is sent to the master. Where upon
		// receipt it will be compared to it's copy. If there are any changes detected the process
		// of redistribution REPEATS. And we AGAIN will find ourselves here. The process repeats
		// until the masters copy matches what every controller is independently putting together
		// and sending. Meaning the controllers send the BOX_CONFIGURATION not by pulling it from
		// chain members.
		tpmicro_data.request_whats_installed_from_tp_micro = (true);
		
		// ----------
	
		// 2/6/2015 rmd : When the wi is received from the tpmicro the ENTIRE box configuration
		// structure (wi plus other options) will be sent to the master. And compared to its copy of
		// the configuration the master retains within its chain members structure. Therefore we can
		// be assured that all fields will be tested to be in sync. No further testing is required
		// here.
		
		// ----------

		xSemaphoreGiveRecursive( chain_members_recursive_MUTEX );
	}

	// ----------
	
	xSemaphoreGiveRecursive( irri_comm_recursive_MUTEX );

	// ----------
	
	// 4/25/2014 rmd : Regardless of if we accepted the chain_members array or not bump the
	// pointer.
	*pucp += sizeof(chain.members);

	// ----------
	
	// Always return true.
	return( (true) );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Within the token we see stations to add to our main irrigation list. This
    indicates these stations were just added to the FOAL main irrigation list as well. As
    the only time we do this is when the stations are initially added.
	
    Scroll through the list first looking for a match. And delete them. Finding and deleting
    a match is not considered an error. That condition can happen normally for TEST and
    MANUAL PROGRAM when there are multiple starts with the same time.
	
    @EXECUTED Executed within the context of the COMM_MNGR task.

	@AUTHOR 2011.10.11 rmd
*/ 
static void irri_add_to_main_list( STATION_FROM_MAIN_LIST_DISTRIBUTION_STRUCT *sds_ptr )
{
	IRRI_MAIN_LIST_OF_STATIONS_STRUCT	*liml_ptr, *tmp_liml_ptr;

	char	str_8[ 8 ];
	
	BOOL_32	add_to_the_list, delete_this_station;
	
	// ----------

	// 10/11/2012 rmd : If the station is in the list for MANUAL_PROGRAM or belongs to an
	// inactive box we will we not add to the list.
	add_to_the_list = (true);

	// ----------

	// We are modifying the irri irrigation list. So better had take the MUTEX.
	xSemaphoreTakeRecursive( irri_irri_recursive_MUTEX, portMAX_DELAY );

	// ----------

	// 8/31/2015 rmd : First go through the list looking for duplicates and stations in the list
	// for manual program. In those cases we either remove the duplicate or modify the list
	// item.
	liml_ptr = nm_ListGetFirst( &irri_irri.list_of_irri_all_irrigation );

	while( liml_ptr != NULL )
	{
		delete_this_station = (false);

		// ----------
		
		// And now for the REAL reason we are looking through the list. If the station is already in
		// the list delete it. Or in the case of MANUAL PROGRAM adjust the remaining time.
		if( (liml_ptr->box_index_0 == sds_ptr->box_index_0) && (liml_ptr->station_number_0_u8 == sds_ptr->station_number_0_u8) && (liml_ptr->ibf.w_reason_in_list == sds_ptr->reason_in_list_u8) )
		{
			// 10/11/2012 rmd : This is not an error. Can occur normally when say the user requests TEST
			// while already testing this station. What about PROGRAMMED IRRIGATION start hit while it
			// is still running? And additionally the case of MANUAL_PROGRAM hitting a new start while
			// still working off the prior one. In that case we want to adjust the existing record.
			if( liml_ptr->ibf.w_reason_in_list == IN_LIST_FOR_MANUAL_PROGRAM )
			{
				// 10/12/2012 rmd : It is already in the list and all we are going to do is adjust the
				// remaining time.
				add_to_the_list = (false);
				
				// 10/11/2012 rmd : For the case of MP set the programmed time equal to what was carried in
				// the sds record. The interesting thing is this station could be ON at this time. And
				// that's okay. The actions taken here will not affect it being ON and the remaining time ON
				// for the cycle and overall remaining time will be correct.
				liml_ptr->requested_irrigation_seconds_u32 = sds_ptr->requested_irrigation_seconds_u32;
			}
			else
			{
				// 10/12/2012 rmd : Squash duplicates.
				delete_this_station = (true);
			}
			
		}
		
		if( delete_this_station )
		{
			// Okay so it is in the list. Delete the item.
			tmp_liml_ptr = liml_ptr;  // Save. THIS IS THE ONE WE ARE TO DELETE!
			
			liml_ptr = nm_ListGetNext( &irri_irri.list_of_irri_all_irrigation, liml_ptr );
			
			// Any housekeeping to do here? We are abruptly removing from the list. So far not. We count
			// on duplicate behaviour here in this irri list as in the master foal list. For example for
			// the case of MANUAL_PROGRAM with additional starts while still working off the prior one.
			// Both side modify their existing list record.
			
			if( nm_ListRemove( &irri_irri.list_of_irri_all_irrigation, tmp_liml_ptr ) != MIST_SUCCESS )
			{
				ALERT_MESSAGE_WITH_FILE_NAME( "Error removing from list" );
			}
			
			// And do not forget to free the associated memory.
			mem_free( tmp_liml_ptr );
		}
		else
		{
			liml_ptr = nm_ListGetNext( &irri_irri.list_of_irri_all_irrigation, liml_ptr );
		}

	}

	// ----------

	// 8/31/2015 rmd : Sanity Check - this is a test to make sure we aren't receiving stations
	// for boxes that our chain members don't show as active and part of the chain. [This did
	// catch an error on the foal side where we weren't checking if stations were in use when
	// trying to irrigate them during a manual program start.]
	if( !chain.members[ sds_ptr->box_index_0 ].saw_during_the_scan )
	{
		// 8/31/2015 rmd : Not going to add this one to the list. Alert and drop it.
		add_to_the_list = (false);
		
		Alert_Message_va( "Irri: box not active %s", ALERTS_get_station_number_with_or_without_two_wire_indicator( liml_ptr->box_index_0, liml_ptr->station_number_0_u8, str_8, sizeof(str_8) ) );
	}
	
	// ----------

	// So NOW the list is CLEAN. Let's check the new record to add. If it's index cannot be
	// found we won't be adding it.
	if( add_to_the_list )
	{
		// So now the list is clean. And we have the index for the sds to add and we can add the new
		// record. Sorted by reason in list, then index, then station number. Scroll through the
		// list till we encounter a station that 'weighs' more than the one we're trying to insert.
		// Then use the insert function to insert before that one.
		liml_ptr = nm_ListGetFirst( &irri_irri.list_of_irri_all_irrigation );
		
		while( liml_ptr != NULL )
		{
			// If the item to be added has a higher reason then that's that. We must stop.
			if( sds_ptr->reason_in_list_u8 > liml_ptr->ibf.w_reason_in_list )
			{
				break;
			}
			else
			if( sds_ptr->reason_in_list_u8 == liml_ptr->ibf.w_reason_in_list )
			{
				// If the reason is equal then lets look at the index.
				if( sds_ptr->box_index_0 < liml_ptr->box_index_0 )
				{
					break;	
				}
				else
				if( sds_ptr->box_index_0 == liml_ptr->box_index_0 )
				{
					if( sds_ptr->station_number_0_u8 < liml_ptr->station_number_0_u8 )
					{
						break;
					}
				}
			}
			
			liml_ptr = nm_ListGetNext( &irri_irri.list_of_irri_all_irrigation, liml_ptr );
		}

		tmp_liml_ptr = mem_malloc( sizeof(IRRI_MAIN_LIST_OF_STATIONS_STRUCT) );

		// Fill out the new list item in its entirety.
		tmp_liml_ptr->system_gid = sds_ptr->system_gid;
		tmp_liml_ptr->requested_irrigation_seconds_u32 = sds_ptr->requested_irrigation_seconds_u32;
		tmp_liml_ptr->box_index_0 = sds_ptr->box_index_0;
		tmp_liml_ptr->station_number_0_u8 = sds_ptr->station_number_0_u8;
		tmp_liml_ptr->ibf.w_reason_in_list = sds_ptr->reason_in_list_u8;
		tmp_liml_ptr->cycle_seconds = sds_ptr->cycle_seconds;

		// 6/26/2012 rmd : When a station is added to the list it is OFF. It may however be soaking.
		// For example if soaking and there was a power failure it would still be soaking. If the
		// station was ON and there was a power failure it would now be OFF and soaking.
		tmp_liml_ptr->ibf.station_is_ON = (false);

		tmp_liml_ptr->remaining_ON_or_soak_seconds = sds_ptr->soak_seconds_remaining;
		
		// ----------
		
		// Now insert the new element BEFORE the list item we stopped at. A NULL puts us at the end
		// of the list. Or in the case of an empty list at the head.
		nm_ListInsert( &irri_irri.list_of_irri_all_irrigation, tmp_liml_ptr, liml_ptr );
	}
	
	xSemaphoreGiveRecursive( irri_irri_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This is an external entry point to add a station to the IRRI list from the TEST SEQUENTIAL screen. It
	is not for general use.
	
    @EXECUTED Executed within the context of the COMM_MNGR task.

	@AUTHOR 07/09/2015 mpd
*/ 
extern void irri_add_to_main_list_for_test_sequential( STATION_FROM_MAIN_LIST_DISTRIBUTION_STRUCT *sds_ptr )
{
	irri_add_to_main_list( sds_ptr );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Pull the action_needed records coming from the master. And take action
    based on the xfer reason for each one.
	
	@CALLER MUTEX REQUIREMENTS

    @EXECUTED Executed within the context of the COMM_MNGR task during the token processing
    function.

    @RETURN The number of bytes pulled from the message. If the number of indicated records
    is out of range returns ZERO. Which might be taken as an error. And an indication to
    abandon any further parsing of the token as could lead to unpredictable problems.

	@AUTHOR 2012.02.06 rmd
*/
static INT_32 __extract_action_needed_records_from_the_incoming_flowsense_token( unsigned char *ptr_to_ucp )
{
	INT_32  i, rv;

	rv = 0;

	UNS_16						lhow_many_records;

	ACTION_NEEDED_XFER_RECORD	lanxr;

	// Pull the count of how many items
	memcpy( &lhow_many_records, ptr_to_ucp, sizeof( UNS_16 ) );
	
	if( (lhow_many_records == 0) || (lhow_many_records > MAX_STATIONS_PER_NETWORK) )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "Irri: range check error on number of records" );
	}
	else
	{
		// If the number of records indicated is out of range we return a 0. Could be the right
		// thing to do or the wrong thing to do. Probably the safest thing to do would be, upon
		// return from this function, to abandon parsing anymore of the rcvd message all together.
		rv = sizeof( UNS_16 );

		ptr_to_ucp += sizeof( UNS_16 );

		for( i=0; i<lhow_many_records; i++ )
		{
			memcpy( &lanxr, ptr_to_ucp, sizeof( ACTION_NEEDED_XFER_RECORD ) );
	
			ptr_to_ucp += sizeof( ACTION_NEEDED_XFER_RECORD );

			rv += sizeof( ACTION_NEEDED_XFER_RECORD );
			
			IRRI_process_rcvd_action_needed_record( &lanxr );
		}
		
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Pick off the STOP_COMMAND_RECORD from the message pointer. And increment
    the message content pointer by the STOP_COMMAND_RECORD amount. And take the STOP command
    action by lookingthrough the IRRI irrigation list and removing all the stations in the
    list for the associated reason to stop.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task when parsing the rcvd token
    from the FOAL master.
	
	@RETURN (none)

	@ORIGINAL 2012.04.16 rmd

	@REVISIONS
*/
static void __extract_stop_key_record_from_the_incoming_flowsense_token( UNS_8 **pucp_ptr )
{
	STOP_KEY_RECORD_s	key_record;

	// ----------

	memcpy( &key_record, *pucp_ptr, sizeof(STOP_KEY_RECORD_s) );
	
	*pucp_ptr += sizeof(STOP_KEY_RECORD_s);

	// ----------

	// 11/7/2014 rmd : Free the ui to press again.
	GuiVar_StopKeyPending = (false);

	// 2012.06.21 ajv : If we're viewing the stop dialog, update the display.
	if( GuiLib_CurStructureNdx == GuiStruct_dlgStop_0 )
	{
		GuiVar_StopKeyReasonInList = key_record.stations_removed_for_this_reason;

		Refresh_Screen();
	}
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Upon receipt of the token the mlb info bit is set. Indicating the message
    contains all the mlb instances in effect at this moment. The absence of information
    means the mlb case is not in effect. So ALL mlb settings are either cleared or set. In
    the message itself if the mlb_info bit is not set ALL mlb instances are cleared.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task upon receipt of the
    token.
	
    @RETURN If the extraction and parsing went error free we return a (true), else (false).

	@ORIGINAL 2012.05.09 rmd

	@REVISIONS (none)
*/

static BOOL_32 __extract_mlb_info_from_the_incoming_flowsense_token( UNS_8 **pucp_ptr, UNS_32 pfrom_serial_number, UNS_32 ppieces )
{
	INT_32	i;

	BY_SYSTEM_RECORD	*bsr;
			
	BOOL_32	rv;
	
	rv = (true);
	
	// 5/15/2012 rmd : Because we are potentially changing the mlb settings in the
	// system_preserves, take this MUTEX.
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	// 5/15/2012 rmd : If the message is from us there is no need to overwrite the
	// system_preserves with the message content. As it was the system_preserves where the
	// information came from in the first place. For the case of mlb we do not have FOAL and
	// IRRI side variables. I'm experimenting with this approach. It makes it easier for
	// 'conditions' that need to persist through 'events' such as the chain going down. We don't
	// have to keep passing between FOAL to IRRI and IRRI back to FOAL as events occur. BUT we
	// still have to pick the info out of the message to keep ucp pointed correctly.
	
	// 5/15/2012 rmd : Now that we have the system_preserves MUTEX, clear all
	// the mlb settings in all the systems (whether they're in use or not). We
	// always do this. Whether or not the message contains mlb_info. This is our
	// mechanism for clearing the mlb condition. If the TOKEN doesn't explicitly
	// contain the mlb data there isn't a mlb!
	for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
	{
		system_preserves.system[ i ].delivered_mlb_record.there_was_a_MLB_during_irrigation = (false);
		system_preserves.system[ i ].delivered_mlb_record.there_was_a_MLB_during_mvor_closed = (false);
		system_preserves.system[ i ].delivered_mlb_record.there_was_a_MLB_during_all_other_times = (false);
	}
	
	if( B_IS_SET( ppieces, BIT_TI_I_here_is_the_mlb_info ) )
	{
		UNS_8	record_count;
		
		// 5/15/2012 rmd : No alignment concerns. Direct read the byte.
		record_count = *(*pucp_ptr);
		
		*pucp_ptr += sizeof( UNS_8 );
		
		for( i=0; i<record_count; i++ )
		{
			UNS_32	lsystem_gid;
			
			memcpy( &lsystem_gid, *pucp_ptr, sizeof(UNS_32) );
	
			*pucp_ptr += sizeof(UNS_32);
			
			bsr = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( lsystem_gid );
	
			if( bsr != NULL )
			{
				memcpy( &bsr->delivered_mlb_record, *pucp_ptr, sizeof(SYSTEM_MAINLINE_BREAK_RECORD) );

				*pucp_ptr += sizeof(SYSTEM_MAINLINE_BREAK_RECORD);
			}
			else
			{
				// 5/15/2012 rmd : If there was an error git out of here. Returning a NULL which in all
				// likely hood will trigger the rest of the TOKEN message to be abandoned.
				ALERT_MESSAGE_WITH_FILE_NAME( "IRRI_COMM : mlb content error." );

				rv = (false);

				break;
	
			}  // of if we found the system_preserves record for the gid
			
		}  // for each record in the message
		
	}  // of if the message contains mlb_info content

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
	
	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_8* build_poc_update_to_ship_to_master( UNS_32* phow_many_bytes )
{
	UNS_8	*rv;
	
	UNS_32	p, f;

	UNS_8	number_of_pocs;

	UNS_8	*ptr_to_the_number_of_pocs;

	UNS_8	*ucp;
	
	POC_DISTRIBUTION_FROM_SLAVE_TO_MASTER	pdfs;
	
	// ----------
	
	// 6/4/2014 rmd : Pad the calculated size with 16 and get the largest possible block
	// needed. Remember as far as the tpmciro is concerned we can only have 12 pocs at this box.
	// For example 1 TERMINAL and 11 DECODER based. Also remember for this transfer the bypass
	// concept does not exist.
	ucp = mem_malloc( 16 + sizeof(UNS_8) + (MAX_POCS_IN_NETWORK * sizeof(POC_DISTRIBUTION_FROM_SLAVE_TO_MASTER)) );
	
	rv = ucp;
	
	// ----------

	// 4/6/2012 rmd : Because we won't know how many poc's are in the message till after the
	// fact get a marker to where to put that byte.
	ptr_to_the_number_of_pocs = ucp;
		
	number_of_pocs = 0;
	
	// 8/10/2012 rmd : Jump over the number of pocs location.
	ucp += sizeof(UNS_8);

	// 6/4/2014 rmd : Start off the returned size at 1.
	*phow_many_bytes = sizeof(UNS_8);

	// ----------
	
	// Take the poc_preserves mutex as we are looking through the list and counting on it to be
	// stable else we may get unexpected results.
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	for( p=0; p<MAX_POCS_IN_NETWORK; p++ )
	{
		// 4/6/2012 rmd : When building a token response we only send the poc data for the active
		// poc's that this box owns.
		if( (poc_preserves.poc[ p ].poc_gid != 0) && (poc_preserves.poc[ p ].box_index == FLOWSENSE_get_controller_index()) )
		{
			for( f=0; f<POC_BYPASS_LEVELS_MAX; f++ )
			{
				if( poc_preserves.poc[ p ].ws[ f ].pbf.there_is_flow_meter_count_data_to_send_to_the_master || poc_preserves.poc[ p ].ws[ f ].pbf.send_mv_pump_milli_amp_measurements_to_the_master )
				{
					number_of_pocs += 1;

					// ----------
					
					memset( &pdfs, 0x00, sizeof(POC_DISTRIBUTION_FROM_SLAVE_TO_MASTER) );

					// 6/4/2014 rmd : All we need at the master to identify a particular tpmicro level poc is
					// the box index and decoder serial number. The master has the box index by virtue of who
					// the message is from (us) so only need to indclude the decoder serial number here.
					pdfs.decoder_serial_number = poc_preserves.poc[ p ].ws[ f ].decoder_serial_number;
					
					pdfs.fm_accumulated_pulses = poc_preserves.poc[ p ].ws[ f ].fm_accumulated_pulses_irri;

					pdfs.fm_accumulated_ms = poc_preserves.poc[ p ].ws[ f ].fm_accumulated_ms_irri;

					pdfs.fm_latest_5_second_count = poc_preserves.poc[ p ].ws[ f ].fm_latest_5_second_pulse_count_irri;

					pdfs.fm_delta_between_last_two_fm_pulses_4khz = poc_preserves.poc[ p ].ws[ f ].fm_delta_between_last_two_fm_pulses_4khz_irri;

					pdfs.fm_seconds_since_last_pulse = poc_preserves.poc[ p ].ws[ f ].fm_seconds_since_last_pulse_irri;

					pdfs.last_measured_mv_ma = poc_preserves.poc[ p ].ws[ f ].mv_last_measured_current_from_the_tpmicro_ma;

					pdfs.last_measured_pump_ma = poc_preserves.poc[ p ].ws[ f ].pump_last_measured_current_from_the_tpmicro_ma;
					
					// ----------
					
					memcpy( ucp, &pdfs, sizeof(POC_DISTRIBUTION_FROM_SLAVE_TO_MASTER) );

					ucp += sizeof(POC_DISTRIBUTION_FROM_SLAVE_TO_MASTER);

					*phow_many_bytes += sizeof(POC_DISTRIBUTION_FROM_SLAVE_TO_MASTER);

					// ----------

					// 6/5/2014 rmd : And when we send the accumulated pulses and accumulated ms to the master
					// we ZERO them here as they've been accounted for.
					poc_preserves.poc[ p ].ws[ f ].fm_accumulated_pulses_irri = 0;

					poc_preserves.poc[ p ].ws[ f ].fm_accumulated_ms_irri = 0;
					
					// ----------
					
					poc_preserves.poc[ p ].ws[ f ].pbf.there_is_flow_meter_count_data_to_send_to_the_master = (false);

					poc_preserves.poc[ p ].ws[ f ].pbf.send_mv_pump_milli_amp_measurements_to_the_master = (false);
				}
			}
		}
	}		

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
			
	// ----------
	
	// 8/10/2012 rmd : Use the number of pocs. NOT message size. Because message size is always
	// at least 1.
	if( number_of_pocs > 0 )
	{
		// 4/6/2012 rmd : Stuff in the number of pocs in the message.
		*ptr_to_the_number_of_pocs = number_of_pocs;
	}
	else
	{
		// 4/6/2012 rmd : Ain't no message.
		mem_free( rv );
		
		rv = NULL;
		
		// 4/6/2012 rmd : Tell the calling function how big.
		*phow_many_bytes = 0;
	}

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 load_mvor_request_to_ship_to_master( UNS_8 **mvor_request_ptr )
{
	UNS_32	rv;
	
	rv = 0;
	
	*mvor_request_ptr = NULL;
	
	if( irri_comm.mvor_request.in_use )
	{
		*mvor_request_ptr = mem_malloc( sizeof(MVOR_KICK_OFF_STRUCT) );

		memcpy( *mvor_request_ptr, &irri_comm.mvor_request, sizeof(MVOR_KICK_OFF_STRUCT) );
		
		// 8/26/2015 ajv : And free the irri_comm request holding structure to accept another
		// request.
		irri_comm.mvor_request.in_use = (false);
		
		rv = sizeof(MVOR_KICK_OFF_STRUCT);
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION ...allocate memory for, fill, and return the size of and ptr to. If not
    return 0 size and NULL ptr.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task.
	
	@RETURN (see function description)

	@ORIGINAL 2012.05.09 rmd 

	@REVISIONS (none)
*/
static UNS_32 __load_system_gids_to_clear_mlb_for( UNS_8 **pclear_mlb_data_ptr )
{
	UNS_32	rv;
	
	rv = 0;
	
	*pclear_mlb_data_ptr = NULL;
	
	// --------------------------------
	
	UNS_8	record_count;
	
	record_count = 0;

	INT_32	i;
		
	// 5/15/2012 rmd : So we don't have to go through the whole mem_malloc and mem_free process
	// when 99.9999% of the time there is nothing to send, let's check the array first.
	for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
	{
		if( irri_comm.system_gids_to_clear_mlbs_for[ i ] != 0 )
		{
			record_count += 1;
		}
	}
	
	// --------------------------------
	
	if( record_count > 0 )
	{
		// 5/16/2012 rmd : A sanity test variable.
		INT_32	count_copy;
		
		count_copy = record_count;

		// 5/15/2012 rmd : Obtain the necessary memory. Remember the size is the return value.
		rv = (1 + (record_count * sizeof(UNS_16)) );
		
		*pclear_mlb_data_ptr = mem_malloc( rv );
		
		UNS_8	*ucp;
	
		// 5/15/2012 rmd : This is where the record_count goes. Also use this variable to release
		// the memory if needed.
		ucp = *pclear_mlb_data_ptr;
		
		// 5/16/2012 rmd : Place in the record count in bump pointer.
		*ucp = record_count;
	
		// 5/15/2012 rmd : Bump over where the record count goes.
		ucp += sizeof( UNS_8 );
		
		for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
		{
			if( irri_comm.system_gids_to_clear_mlbs_for[ i ] != 0 )
			{
				count_copy -= 1;
				
				memcpy( ucp, &(irri_comm.system_gids_to_clear_mlbs_for[ i ]), sizeof(UNS_16) );
				
				ucp += sizeof(UNS_16);

				// 5/16/2012 rmd : And reset the indication to clear.
				irri_comm.system_gids_to_clear_mlbs_for[ i ] = 0;
			}
		}
		
		if( count_copy != 0 )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "IRRI_COMM : unexp outcome." );

			// 5/14/2012 rmd : Free the memory and set the ptr to NULL to signify nothing to send in the
			// message.
			mem_free( *pclear_mlb_data_ptr );
			
			*pclear_mlb_data_ptr = NULL;
			
			rv = 0;
		}

	}  // of there were records to send

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION If the box current is marked to ship to the master allocate memory for,
    fill, and return the size of and ptr to. If not return 0 size and NULL ptr.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task.
	
	@RETURN (see function description)

	@ORIGINAL 2012.08.16 rmd

	@REVISIONS (none)
*/
static UNS_32 __load_box_current_to_ship_to_master( UNS_8 **box_current_ptr )
{
	UNS_32	rv;
	
	rv = 0;
	
	*box_current_ptr = NULL;
	
	// 8/16/2012 rmd : Query the irri side flag to send or not. If we send it is only a single
	// UNS_32 that is sent.
	if( tpmicro_data.as_rcvd_from_tp_micro.current_needs_to_be_sent == (true) )
	{
		*box_current_ptr = mem_malloc( sizeof(UNS_32) );
		
		// 8/16/2012 rmd : Take a copy of the measurment and place into the allocated memory.
		memcpy( *box_current_ptr, &tpmicro_data.as_rcvd_from_tp_micro.measured_ma_current, sizeof(UNS_32) );

		// 8/16/2012 rmd : And clear the flag.
		tpmicro_data.as_rcvd_from_tp_micro.current_needs_to_be_sent = (false);

		rv = sizeof(UNS_32);
	}
	
	return( rv );
}

static BOOL_32 __extract_the_box_currents_from_the_incoming_flowsense_token( UNS_8 **pucp )
{
	BOOL_32	rv;
	
	rv = (true);
	
	UNS_8	number_present;

	number_present = **pucp;
	*pucp += sizeof(UNS_8);
	
	if( number_present > MAX_CHAIN_LENGTH )
	{
		rv = (false);
	}
	else
	{
		UNS_32	i;
		
		for( i=0; i<number_present; i++ )
		{
			UNS_8	index;
			
			index = **pucp;
			*pucp += sizeof(UNS_8);
		
			if( index >= MAX_CHAIN_LENGTH )
			{
				rv = (false);
				
				break;
			}
			else
			{
				UNS_32	measurement;
				
				memcpy( &measurement, *pucp, sizeof(UNS_32) );
				*pucp += sizeof(UNS_32);
				
				if( measurement > MAXIMUM_BOX_MILLI_AMPS )
				{
					rv = (false);
					
					break;
				}
				else
				{
					tpmicro_data.measured_ma_current_as_rcvd_from_master[ index ] = measurement;
				}
			}
			
		}
		
	}
	
	if( rv == (false) )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "IRRI_COMM: box current problem." );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION If there has been a short reported send the details to the master and
    indicate it has been done.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED 
	
	@RETURN (see function description)

	@ORIGINAL 2012.08.16 rmd

	@REVISIONS (none)
*/
static UNS_32 load_terminal_short_or_no_current_to_ship_to_master( UNS_8 **pterminal_short_ptr )
{
	// 9/25/2015 rmd : Executed within the context of the COMM_MNGR task. If the tpmicro data
	// indicates a short or no current record has arrived from the tpmicro include it in the
	// token response and send it on to the master.

	// ----------
	
	UNS_32	rv;
	
	rv = 0;
	
	*pterminal_short_ptr = NULL;
	
	if( tpmicro_data.terminal_short_or_no_current_state == TERMINAL_SONC_STATE_send_to_master )
	{
		// 9/24/2013 rmd : We are going to send the short report record.
		if( mem_obtain_a_block_if_available( sizeof(TERMINAL_SHORT_OR_NO_CURRENT_STRUCT), (void**)pterminal_short_ptr ) )
		{
			memcpy( *pterminal_short_ptr, &tpmicro_data.terminal_short_or_no_current_report, sizeof(TERMINAL_SHORT_OR_NO_CURRENT_STRUCT) );
			
			rv += sizeof(TERMINAL_SHORT_OR_NO_CURRENT_STRUCT);
			
			// ----------
			
			// 9/29/2015 rmd : Now indicate we are waiting to hear back from the master.
			tpmicro_data.terminal_short_or_no_current_state = TERMINAL_SONC_STATE_waiting_for_response_from_master;
		}	
	}
	
	// ----------
	
	return( rv );
}
	
static UNS_32 load_two_wire_cable_current_measurement_to_ship_to_master( UNS_8 **twci_ptr )
{
	UNS_32	rv;
	
	rv = 0;
	
	if( mem_obtain_a_block_if_available( sizeof(TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT), (void**)twci_ptr ) )
	{
		memcpy( *twci_ptr, &tpmicro_data.twccm, sizeof(TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT) );
		
		rv += sizeof(TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT);
	}	
	
	return( rv );
}

static UNS_32 load_test_request_to_ship_to_master( UNS_8 **test_request_ptr )
{
	UNS_32	rv;
	
	rv = 0;
	
	*test_request_ptr = NULL;
	
	if( irri_comm.test_request.in_use )
	{
		if( mem_obtain_a_block_if_available( sizeof(TEST_AND_MOBILE_KICK_OFF_STRUCT), (void**)test_request_ptr ) )
		{
			memcpy( *test_request_ptr, &irri_comm.test_request, sizeof(TEST_AND_MOBILE_KICK_OFF_STRUCT) );
			
			// 8/31/2012 rmd : And free the irri_comm request holding structure to accept another
			// request.
			irri_comm.test_request.in_use = (false);
			
			rv = sizeof(TEST_AND_MOBILE_KICK_OFF_STRUCT);
		}
	}
	
	return( rv );
}
	
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION If there has been 1 or more et gage pulses reported by the tp micro we feed
    them up to the master here.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task when building a token
    response.
	
	@RETURN Size of message content.

	@ORIGINAL 2012.09.18 rmd

	@REVISIONS (none)
*/
static UNS_32 load_et_gage_count_to_ship_to_master( UNS_8 **pet_gage_count_ptr )
{
	UNS_32	rv;
	
	rv = 0;
	
	*pet_gage_count_ptr = NULL;
	
	// 9/18/2012 rmd : Take the MUTEX to avoid conflict when evaluating and reseting the count
	// while building a token response. Even though I believe this variable is only accessed by
	// the COMM_MNGR task.
	xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );

	if( tpmicro_data.et_gage_pulse_count_to_send_to_the_master > 0 )
	{
		*pet_gage_count_ptr = mem_malloc( sizeof(UNS_32) );

		memcpy( *pet_gage_count_ptr, &(tpmicro_data.et_gage_pulse_count_to_send_to_the_master), sizeof(UNS_32) );
		
		// 9/18/2012 rmd : And clear the pending count(s).
		tpmicro_data.et_gage_pulse_count_to_send_to_the_master = 0;
		
		rv = sizeof(UNS_32);
	}
	
	xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );

	return( rv );
}
	
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION If there has been 1 or more rain bucket pulses reported by the tp micro we
    feed them up to the master here.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task when building a token
    response.
	
	@RETURN Size of message content.

	@ORIGINAL 2012.09.18 rmd

	@REVISIONS (none)
*/
static UNS_32 load_rain_bucket_count_to_ship_to_master( UNS_8 **prain_bucket_count_ptr )
{
	UNS_32	rv;
	
	rv = 0;
	
	*prain_bucket_count_ptr = NULL;
	
	// 9/18/2012 rmd : Take the MUTEX to avoid conflict when evaluating and reseting the count
	// while building a token response. Even though I believe this variable is only accessed by
	// the COMM_MNGR task.
	xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );

	if( tpmicro_data.rain_bucket_pulse_count_to_send_to_the_master > 0 )
	{
		*prain_bucket_count_ptr = mem_malloc( sizeof(UNS_32) );

		memcpy( *prain_bucket_count_ptr, &(tpmicro_data.rain_bucket_pulse_count_to_send_to_the_master), sizeof(UNS_32) );
		
		// 9/18/2012 rmd : And clear the pending count(s).
		tpmicro_data.rain_bucket_pulse_count_to_send_to_the_master = 0;
		
		rv = sizeof(UNS_32);
	}
	
	xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );

	return( rv );
}
	
/* ---------------------------------------------------------- */
static UNS_32 load_wind_to_ship_to_master( UNS_8 **pwind_ptr )
{
	UNS_32	rv;

	UNS_8	*ucp;
	
	rv = 0;
	
	*pwind_ptr = NULL;
	
	// 10/13/2014 ajv : Always send wind if this controller has the Wind Gage
	// and it's in use.
	if( (WEATHER_get_wind_gage_in_use()) && (WEATHER_get_wind_gage_box_index() == FLOWSENSE_get_controller_index()) )
	{
		ucp = mem_malloc( sizeof(UNS_32) + sizeof(BOOL_32) );

		*pwind_ptr = ucp;

		memcpy( ucp, &tpmicro_comm.wind_mph, sizeof(UNS_32) );
		ucp += sizeof(UNS_32);
		rv += sizeof(UNS_32);

		memcpy( ucp, &tpmicro_comm.wind_paused, sizeof(BOOL_32) );
		rv += sizeof(BOOL_32);
	}

	return( rv );
}
	
/* ---------------------------------------------------------- */
static UNS_32 load_box_configuration_to_ship_to_master( UNS_8 **box_config_ptr )
{
	UNS_32	rv;
	
	// 2/6/2015 rmd : Use a local BOX_CONFIG structure in case of an alignment issue. Meaning
	// you cannot write the structure variable by variable directly into the message memory.
	BOX_CONFIGURATION_STRUCT	lbox;

	rv = 0;
	
	*box_config_ptr = NULL;
	
	// ----------
	
	// 5/1/2014 rmd : Take the mutex to protect against us missing a change due to multiple
	// processes manipulating the send_box_configuration_to_master flag.
	xSemaphoreTakeRecursive( irri_comm_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	if( irri_comm.send_box_configuration_to_master )
	{
		if( mem_obtain_a_block_if_available( sizeof(BOX_CONFIGURATION_STRUCT), (void**)box_config_ptr ) )
		{
			// 4/16/2014 rmd : And clear the detected change flag.
			irri_comm.send_box_configuration_to_master = (false);
	
			// ----------
	
			// 4/24/2015 rmd : First ZERO the variable out so we have a consistent starting point.
			// Without doing this to this stack variable we risk repetitively sending a varying
			// configuration to the master. And it keeps looking different. Because the PAD bytes within
			// the structure are inconsistent. AND INDEED THIS HAPPENS.
			memset( &lbox, 0x00, sizeof(BOX_CONFIGURATION_STRUCT) );

			// 2/6/2015 rmd : Load first into a variable then memcpy into the pointer location. To be
			// extra careful there aren't any alignment issues with the returned mem block pointer.
			// Though I don't think there are any cause our memory subsytem returns WORD 4-byte aligned
			// pointers.
			load_this_box_configuration_with_our_own_info( &lbox );
			
			memcpy( *box_config_ptr, &lbox, sizeof(BOX_CONFIGURATION_STRUCT) );
	
			// ----------
			
			rv = sizeof(BOX_CONFIGURATION_STRUCT);
		}
	}

	// ----------
	
	xSemaphoreGiveRecursive( irri_comm_recursive_MUTEX );

	// ----------
	
	return( rv );
}
	
/* ---------------------------------------------------------- */
static UNS_32 load_manual_water_request_to_ship_to_master( UNS_8 **manual_water_request_ptr )
{
	UNS_32	rv;
	
	rv = 0;
	
	*manual_water_request_ptr = NULL;
	
	if( irri_comm.manual_water_request.in_use == (true) )
	{
		*manual_water_request_ptr = mem_malloc( sizeof(MANUAL_WATER_KICK_OFF_STRUCT) );

		memcpy( *manual_water_request_ptr, &irri_comm.manual_water_request, sizeof(MANUAL_WATER_KICK_OFF_STRUCT) );
		
		// 4/4/2013 ajv : And free the irri_comm request holding structure to
		// accept another request.
		irri_comm.manual_water_request.in_use = (false);
		
		rv = sizeof(MANUAL_WATER_KICK_OFF_STRUCT);
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 load_decoder_faults_to_ship_to_master( UNS_8 **decoder_faults_ptr )
{
	UNS_32	i;

	BOOL_32	send_decoder_faults;

	UNS_32	rv;

	rv = 0;

	send_decoder_faults = (false);

	*decoder_faults_ptr = NULL;

	for( i=0; i<TWO_WIRE_DECODER_FAULTS_TO_BUFFER; i++ )
	{
		if( tpmicro_data.decoder_faults[ i ].fault_type_code != DECODER_FAULT_CODE_not_in_use )
		{
			send_decoder_faults = (true);

			break;
		}
	}

	if( send_decoder_faults )
	{
		// 10/23/2014 rmd : Looks for at least one, and copies the whole array in case there is more
		// than one.
		*decoder_faults_ptr = mem_malloc( sizeof(tpmicro_data.decoder_faults) );

		memcpy( *decoder_faults_ptr, &tpmicro_data.decoder_faults, sizeof(tpmicro_data.decoder_faults) );

		rv = sizeof(tpmicro_data.decoder_faults);

		// ----------
		
		// 4/4/2013 ajv : And reset the tpmicro_data structure to accept new
		// shorts from the TP Micro.
		memset( &tpmicro_data.decoder_faults, 0x00, sizeof(tpmicro_data.decoder_faults) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 extract_a_terminal_short_acknowledge_from_the_incoming_flowsense_token( UNS_8 **pucp )
{
	// 9/25/2013 rmd : Executed within the context of the COMM_MNGR task. Extract the short
	// record. Analyze for necessary actions needed to the irri list. And potential ACK to the
	// tp micro if this is our own record that made the journey from us to the master and back.
	// Additionally for display purposes set the CURRENT flags in the station preserves bit
	// field.
	
	// 9/25/2013 rmd : Returns (true) if received data passes range checks.

	// ----------
	
	FOAL_TO_IRRI_TERMINAL_SHORT_STRUCT			ftiss;
	
	UNS_32										lstation_preserves_index, this_decoders_level;
	
	POC_DECODER_OR_TERMINAL_WORKING_STRUCT		*ws_ptr;
	
	BY_POC_RECORD								*bpr_ptr;
	
	BOOL_32										rv;
	
	rv = (true);
	
	// ----------
	
	memcpy( &ftiss, *pucp, sizeof(FOAL_TO_IRRI_TERMINAL_SHORT_STRUCT) );
	
	*pucp += sizeof(FOAL_TO_IRRI_TERMINAL_SHORT_STRUCT);
	
	// -----------

	// 9/24/2013 rmd : Range checking.

	if( (ftiss.srs.result != SHORT_OR_NO_CURRENT_RESULT_short_and_found) &&
		(ftiss.srs.result != SHORT_OR_NO_CURRENT_RESULT_short_but_hunt_was_unsuccessful) &&
		(ftiss.srs.result != SHORT_OR_NO_CURRENT_RESULT_no_current) )
	{
		rv = (false);	
	}

	if( (ftiss.srs.terminal_type != OUTPUT_TYPE_TERMINAL_MASTER_VALVE) &&
		(ftiss.srs.terminal_type != OUTPUT_TYPE_TERMINAL_PUMP) &&
		(ftiss.srs.terminal_type != OUTPUT_TYPE_TERMINAL_STATION) &&
		(ftiss.srs.terminal_type != OUTPUT_TYPE_TERMINAL_LIGHT) )
	{
		rv = (false);	
	}
	
	// ----------

	if( !rv )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "IRRI_COMM: short struct out of range!" );
	}
	else
	{
		if( ftiss.srs.result == SHORT_OR_NO_CURRENT_RESULT_no_current )
		{
			// 11/3/2014 rmd : NOTE - for the case of NO_CURRENT we cannot use the action_needed record
			// (like we do with a SHORT). A no current is passive so there is no resultant action needed
			// hence no action needed record.
			
			// ----------
			
			if( (ftiss.srs.terminal_type == OUTPUT_TYPE_TERMINAL_MASTER_VALVE) || (ftiss.srs.terminal_type == OUTPUT_TYPE_TERMINAL_PUMP) )
			{
				// 9/25/2013 rmd : Prepare to change the POC Preserves.
				xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

				// 9/25/2013 rmd : The goal is to set the NO_CURRENT flag for the MV or PUMP.
				ws_ptr = POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters( ftiss.box_index_0, 0, &bpr_ptr, &this_decoders_level );

				if( ws_ptr != NULL )
				{
					// 9/29/2015 rmd : These flags are intended for user information purposes only (screen
					// work). They are cleared when the MV or PUMP output transitions from not energized to
					// energized.
					if( ftiss.srs.terminal_type == OUTPUT_TYPE_TERMINAL_MASTER_VALVE )
					{
						ws_ptr->pbf.no_current_mv = (true);	
					}
					else
					{
						ws_ptr->pbf.no_current_pump = (true);	
					}
				}
				
				xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
			}
			else
			if( ftiss.srs.terminal_type == OUTPUT_TYPE_TERMINAL_STATION )
			{
				// 9/25/2013 rmd : Well we need to find the STATION_PRESERVES index and set the NO_CURRENT
				// flag for this station.
				if( STATION_PRESERVES_get_index_using_box_index_and_station_number( ftiss.box_index_0, ftiss.srs.station_or_light_number_0, &lstation_preserves_index ) == (true) )
				{
					// 8/17/2012 rmd : Manipulating a bit field. NOT atomic! So take the MUTEX.
					xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );
	
					// 8/21/2012 rmd : Mark the station as NO CURRENT. For display purposes only. Note - the
					// action needed record cannot perform this flag setting operation like with a short cause
					// there is NO action needed record for a NO_CURRENT.
					station_preserves.sps[ lstation_preserves_index ].spbf.i_status = STATION_PRESERVES_I_STATUS_conventional_no_current;
	
					xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
				}
			}
			else
			if( ftiss.srs.terminal_type == OUTPUT_TYPE_TERMINAL_LIGHT )
			{
				// 9/29/2015 rmd : Find the lights preserves index and set the no current flag.
				if( (ftiss.box_index_0 < MAX_CHAIN_LENGTH) && (ftiss.srs.station_or_light_number_0 < MAX_LIGHTS_IN_A_BOX ) )
				{
					lights_preserves.lbf[ LIGHTS_get_lights_array_index( ftiss.box_index_0, ftiss.srs.station_or_light_number_0 ) ].no_current_output = (true);
				}
			}
		}
		else
		if( ftiss.srs.result == SHORT_OR_NO_CURRENT_RESULT_short_and_found )
		{
			if( (ftiss.srs.terminal_type == OUTPUT_TYPE_TERMINAL_MASTER_VALVE) || (ftiss.srs.terminal_type == OUTPUT_TYPE_TERMINAL_PUMP) )
			{
				// 9/25/2013 rmd : Prepare to change the POC Preserves.
				xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

				// 9/25/2013 rmd : The goal is to set the SHORTED flag for the MV or PUMP. Find the TERMINAL
				// poc working details. Meaning decoder serial number is ZERO.
				ws_ptr = POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters( ftiss.box_index_0, 0, &bpr_ptr, &this_decoders_level );

				if( ws_ptr != NULL )
				{
					// 9/29/2015 rmd : These flags are intended for user information purposes only (screen
					// work). They are cleared when the MV or PUMP output transitions from not energized to
					// energized.
					if( ftiss.srs.terminal_type == OUTPUT_TYPE_TERMINAL_MASTER_VALVE )
					{
						ws_ptr->pbf.shorted_mv = (true);	
					}
					else
					{
						ws_ptr->pbf.shorted_pump = (true);	
					}
				}

				xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
			}
			
			// 11/3/2014 rmd : NOTE - for the case of a STATION SHORT or a LIGHTS SHORT the flag is set
			// when the irri machine receives the ACTION_NEEDED record requesting the turn OFF.

		}
		
		// ----------

		// 9/30/2015 rmd : ONLY if this record originally came from us do we acknowledge. This
		// supports two shorts at the exact same time on two different boxes on a chain. Each token
		// response to the master delivers the information for a single short. And the NEXT token
		// contains the ACTION NEEDED turn OFF record and this short acknowledge content. Allowing
		// us to process and indicate to the TPMicro only after we have rcvd OUR record. Remember
		// ... while the TPMICRO is waiting for this ACK no outputs will be turned ON.
		if( ftiss.box_index_0 == FLOWSENSE_get_controller_index() )
		{
			// 8/21/2012 rmd : The flags should make sense else we have a logic breakdown!
			if( (tpmicro_data.terminal_short_or_no_current_state != TERMINAL_SONC_STATE_waiting_for_response_from_master) ||
				(memcmp( &tpmicro_data.terminal_short_or_no_current_report, &ftiss.srs, sizeof(TERMINAL_SHORT_OR_NO_CURRENT_STRUCT) ) != 0) )
			{
				ALERT_MESSAGE_WITH_FILE_NAME( "IRRI_COMM: short unexp state!" );
			}
			
			// 8/21/2012 rmd : Okay serial numbers match so this is our report. Set the flag about
			// rcving the ack. Regardless of if logic test above passed.
			tpmicro_data.terminal_short_or_no_current_state = TERMINAL_SONC_STATE_ack_to_tpmicro;

		}  // of if this is our serial number
		
		// ----------

	}  // of if the intiial range check passed
	
	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 extract_a_two_wire_cable_anomaly_from_the_incoming_flowsense_token( UNS_8 **pucp, UNS_32 *pstate_ptr, BOOL_32 *flag_ptr )
{
	// 9/25/2013 rmd : Executed within the context of the COMM_MNGR task. Extract the record.
	// Looking if we send the ACK to the tp micro if this is our own record that made the
	// journey from us to the master and back. This function always returns (true).
	
	BOOL_32		rv;
		
	UNS_32		lbox_index_0;
	
	// 11/11/2014 rmd : Indicate failure.
	rv = (false);
	
	// ----------
	
	memcpy( &lbox_index_0, *pucp, sizeof(UNS_32) );
	
	*pucp += sizeof(UNS_32);
	
	// -----------

	if( lbox_index_0 < MAX_CHAIN_LENGTH )
	{
		// 11/11/2014 rmd : Always set the display flag for the box index slot.
		*(flag_ptr + lbox_index_0) = (true);
		
		// 11/11/2014 rmd : And if the index is our own flag the tp_micro to complete the process
		// because apparently the anomoly originated at our box.
		if( lbox_index_0 == FLOWSENSE_get_controller_index() )
		{
			// 11/12/2014 rmd : ONLY for the case of the overheated FET's do we tell the tpmicro that we
			// heard back from the master. In the case of the overheated telling the tpmicro only makes
			// sure all decoder commands are cleared. It does not clear the overheated state that
			// happens autonomously when the fet's cool down. For the case of EXCESSIVE current it is
			// most important that we do not tell the tpmicro we heard back from the master. Becuase
			// that indication actually clears the 2W cable excessive current indication which would
			// allow the cable to power up (if POC decoder present) and perhaps repeat the whole cable
			// excessive current process again.

			// 11/12/2014 rmd : Check the xmission state makes sense.
			if( *pstate_ptr != TWO_WIRE_CABLE_ANOMALY_STATE_waiting_for_reponse_from_master )
			{
				Alert_Message( "irri: two wire cable unexp state" );
			}
		
			// 11/12/2014 rmd : SEE NOTE ABOVE. Only for the overheated state do we flag to tell the
			// tpmicro.
			if( pstate_ptr == &tpmicro_data.two_wire_cable_over_heated_xmission_state )
			{
				*pstate_ptr = TWO_WIRE_CABLE_ANOMALY_STATE_send_ack_to_tpmicro;
			}
		}

		rv = (true);
	}
	else
	{
		Alert_Message( "irri: cable anomoly box_index bad" );
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 extract_real_time_weather_data_out_of_msg_from_main( UNS_8 **pucp, const UNS_32 pfrom_serial_number )
{
	BOOL_32	rv;

	rv = (true);

	// ----------

	// 10/13/2014 ajv : If it came from us, simply skip over the all of the
	// variables since we've already set our local preserves when we originally
	// received the data in a token response.
	// 
	// Otherwise, set the appropriate weather_preserves and GuiVar variables.
	if( config_c.serial_number == pfrom_serial_number )
	{
		*pucp += sizeof(ET_TABLE_ENTRY) +
				 sizeof(UNS_32) +
				 sizeof(BOOL_32) +
				 sizeof(RAIN_TABLE_ENTRY) +
				 sizeof(UNS_32) +
				 sizeof(BOOL_32) +
				 sizeof(BOOL_32) +
				 sizeof(UNS_32) +
				 sizeof(BOOL_32);
	}
	else
	{
		memcpy( &weather_preserves.et_rip, *pucp, sizeof(ET_TABLE_ENTRY) );
		*pucp += sizeof(ET_TABLE_ENTRY);

		memcpy( &weather_preserves.remaining_gage_pulses, *pucp, sizeof(UNS_32) );
		*pucp += sizeof(UNS_32);

		memcpy( &weather_preserves.dont_use_et_gage_today, *pucp, sizeof(BOOL_32) );
		*pucp += sizeof(BOOL_32);

		memcpy( &weather_preserves.rain.rip, *pucp, sizeof(RAIN_TABLE_ENTRY) );
		*pucp += sizeof(RAIN_TABLE_ENTRY);

		// 4/28/2015 rmd : This used to be the roll time to roll time pulse count. I switched to
		// mid-night to mid-night. That seems to relate better to the user.
		memcpy( &weather_preserves.rain.midnight_to_midnight_raw_pulse_count, *pucp, sizeof(UNS_32) );
		*pucp += sizeof(UNS_32);

		memcpy( &weather_preserves.rain_switch_active, *pucp, sizeof(BOOL_32) );
		*pucp += sizeof(BOOL_32);

		memcpy( &weather_preserves.freeze_switch_active, *pucp, sizeof(BOOL_32) );
		*pucp += sizeof(BOOL_32);

		memcpy( &GuiVar_StatusWindGageReading, *pucp, sizeof(UNS_32) );
		*pucp += sizeof(UNS_32);

		memcpy( &GuiVar_StatusWindPaused, *pucp, sizeof(BOOL_32) );
		*pucp += sizeof(BOOL_32);
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 load_light_on_request_to_ship_to_master( UNS_8 **light_on_request_ptr )
{
	UNS_32	rv;
	
	// ----------
	
	rv = 0;
	
	*light_on_request_ptr = NULL;
	
	// ----------
	
	// This flag is used to block multiple requests until the token response has been handled, and to indicate a 
	// response is required.
	if( irri_comm.light_on_request.in_use == (true) )
	{
		*light_on_request_ptr = mem_malloc( sizeof( LIGHTS_ON_XFER_RECORD ) );

		memcpy( *light_on_request_ptr, &irri_comm.light_on_request, sizeof( LIGHTS_ON_XFER_RECORD ) );

		// We have copied the ON request data into the allocated memory. Now clean up the static fields in the 
		// "irri_comm" structure for next time.

		// Free the irri_comm request holding structure to accept another request.
		irri_comm.light_on_request.in_use = ( false );

		// 8/17/2015 mpd : TODO: Find a better INVALID number.
		irri_comm.light_on_request.light_index_0_47 = 99;
		irri_comm.light_on_request.stop_datetime_d = 0;
		irri_comm.light_on_request.stop_datetime_t = 0;

		rv = sizeof( LIGHTS_ON_XFER_RECORD );
	}

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 load_light_off_request_to_ship_to_master( UNS_8 **light_off_request_ptr )
{
	UNS_32	rv;
	
	rv = 0;
	
	*light_off_request_ptr = NULL;
	
	// This flag is used to block multiple requests until the token response has been handled, and to indicate a 
	// response is required.
	if( irri_comm.light_off_request.in_use == (true) )
	{
		*light_off_request_ptr = mem_malloc( sizeof( LIGHTS_OFF_XFER_RECORD ) );

		memcpy( *light_off_request_ptr, &irri_comm.light_off_request, sizeof( LIGHTS_OFF_XFER_RECORD ) );

		// We have copied the OFF request data into the allocated memory. Now clean up the static fields in the 
		// "irri_comm" structure for next time.

		// Free the irri_comm request holding structure to accept another request.
		irri_comm.light_off_request.in_use = ( false );

		// 8/17/2015 mpd : TODO: Find a better INVALID number.
		irri_comm.light_off_request.light_index_0_47 = 99;

		rv = sizeof( LIGHTS_OFF_XFER_RECORD );
	}

	return( rv );
}

/* ---------------------------------------------------------- */

// 2011.12.01 rmd : For debug purposes to measure the worst case time to pull apart a token
// and make the response. But what does the so called WORST case time mean if we were
// pre-empted by say the TD_CHECK task and we had to draw a screen. All higher priority
// activites than parsing this token. So keep that in mind. But I think those activities do
// not appreciably change the fact that it can take 1.8 SECONDS to parse a token holding 768
// stations.  On that note you may want to suspend the scheduler to improve the accuracy of
// the measured time. But I've found it to make little difference. Maybe we shave 100ms off
// this otherwise very large time of 1.7 or so seconds.
static UNS_32	__largest_token_parsing_delta;

static UNS_32	__largest_token_resp_size;

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Incoming irri data token. This triggers us to flag the sync as complete! By
    definition if we get one of these the sync process is done!

	@EXECUTED Executed within the context of the COMM_MNGR task. 
	
	@RETURN (none)

	@AUTHOR 2011.10.26 rmd
*/
extern void IRRI_COMM_process_incoming__irrigation_token( COMMUNICATION_MESSAGE_LIST_ITEM *cmli )
{
	DATA_HANDLE 	ldh;	// for the new outgoing message
	
	FOAL_COMMANDS	*icm, *rm;	// incoming message & return message
	
	UNS_8			*ucp;
	
	UNS_32			i;

	BOOL_32			is_clean;

	// ----------

	// 2011.12.01 rmd : For debug purposes to measure how long it takes to do the processing.
	UNS_32	__this_time_delta;

	__this_time_delta = my_tick_count;

	// ----------

	// 4/12/2012 rmd : If we suffer a range check or other content error on a piece of data
	// within the message abandon any further processing. Drop it!
	BOOL_32	content_ok;
	
	content_ok = (true);

	// ----------

	BOOL_32		lnetwork_is_ready;

	// 5/8/2014 ajv : Make a local copy of whether the network is available
	// rather than using the GuiVar.
	lnetwork_is_ready = COMM_MNGR_network_is_available_for_normal_use();

	// ----------

	// 2011.11.21 rmd : There is not a good reason to take the irri_irri list mutex and hold it
	// for the entire time we parse the token and make the response. There are certain times
	// throughout that process we will take the list_irri_irri_recursive_mutex. But only as
	// needed. NOTE: we did run into a problem taking and holding this mutex during program
	// starts when it does take some time to process this incoming token loaded with stations to
	// add to the list. This was the case for 768 stations all starting simultaneously. I don't
	// fully understand all that was happening but between TD_CHECK, COMM_MNGR, and the DISPLAY
	// task all vying for the mutex when show irri_details we had troubles.

	// ALL messages start with the 2 byte part of this structure
	icm = (FOAL_COMMANDS*)cmli->dh.dptr;

	// create a ptr to the first possible piece of data coming to us
	ucp = cmli->dh.dptr;
	ucp += sizeof( FOAL_COMMANDS );

	if( icm->mid != MID_FOAL_TO_IRRI__TOKEN )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "Unexpected command" );
	}
	else
	{
		// 8/4/2016 rmd : We have seen a MUTEX DEADLOCK condition occur while parsing the TOKEN. It
		// occurs if the token contains SYSTEM pdata content. And while parsing that content if the
		// TDCHECK one second interrupt occurs. TDCHECK being a higher priority begins execution.
		// And one of the things it does is to sync the system preserves. And it does this
		// regardless of the NETWORK_READY flag you'll see in there. The if( NETWORK_READY ) syncs
		// the system_preserves. AND SO DOES the ELSE. It is the system_preserves sync that leads to
		// the deadlock condition.
		//
		// 8/4/2016 rmd : So what do we KNOW? We know that TDCHECK is NOT executing. Why because we
		// are a lower priority task and we are executing. So by defintion TDCHECK cannot. So we can
		// boost our priority to above that of TDCHECK and be guaranteed that TDCHECK cannot
		// interrupt the parsing of the token. Even if TDCHECK begins execution on the 'next
		// instruction' before I boost the priority of comm_mngr its okay. BECAUSE tdcheck will
		// finish what it is doing before the we the comm_mngr continue.
		vTaskPrioritySet( NULL, PRIORITY_LEVEL_COMM_MNGR_BOOSTED );
		
		// ----------
		
		// for engineering observation
		//rcvd_token_size = cmli->dh.dlen;

		comm_stats.tokens_rcvd += 1;
		
		// ----------

		UNS_32	from_serial_number;
		
		// Must 0 out first to get the 4th byte set to 0.
		from_serial_number = 0;
		
		memcpy( &from_serial_number, &(cmli->from_to.from), 3 );


		UNS_32	to_serial_number;
		
		// Must 0 out first to get the 4th byte set to 0.
		to_serial_number = 0;
		
		memcpy( &to_serial_number, &(cmli->from_to.to), 3 );
		
		// ----------
		
		// 5/5/2014 rmd : Initialize for the network available logic.
		is_clean = (true);
		
		// ----------
		
		// 8/8/2016 rmd : For debug only.
		//Alert_Message_va( "token rcvd to %u", to_serial_number );

		if( ( content_ok ) && (B_IS_SET( icm->pieces, BIT_TI_I_here_is_program_data ) == (true)) )
		{
			// 8/8/2016 rmd : For debug only.
			//Alert_Message_va( "has pdata content" );

			ucp += PDATA_extract_and_store_changes( ucp, CHANGE_REASON_DONT_SHOW_A_REASON, (false), CHANGE_REASON_DISTRIBUTED_BY_MASTER );

			// 5/5/2014 rmd : Content indicates network still learning about itself.
			is_clean = (false);
		}
		
		// ----------

		if( (content_ok) && (B_IS_SET( icm->pieces, BIT_TI_I_here_is_the_chain_members_array)) )
		{
			content_ok = extract_chain_members_array_from_the_incoming_flowsense_token( &ucp );

			// 5/5/2014 rmd : Content indicates network still learning about itself.
			is_clean = (false);
		}

		// ----------

		if( is_clean )
		{
			// 10/22/2014 ajv : If we set the flag to notify the master we're new,
			// clear it now. Doing so here protects against some other event
			// interrupting the process before the master is able to parse the
			// information it receives from new controllers.
			// 
			// However, ONLY do so if we're not requesting a new scan. These two
			// flags are set by the tell_master_to_scan_and_report_we_are_NEW comm
			// mngr event in certain situations so we want to make sure we don't
			// disrupt that.
			if( (!comm_mngr.request_a_scan_in_the_next_token_RESP) && (comm_mngr.flag_myself_as_NEW_in_the_scan_RESP) )
			{
				comm_mngr.flag_myself_as_NEW_in_the_scan_RESP = (false);
			}

			// ----------

			comm_mngr.since_the_scan__slave__number_of_clean_tokens_rcvd += 1;
			
			// ----------
			
			// 9/4/2018 rmd : And for the chain sync control, since we got a clean token count increment
			// any clean token counts that need it. These counts are set to 0 when the token contains
			// program data changes, or when we have program data changes to send.
			CHAIN_SYNC_increment_clean_token_counts();
		}
		else
		{
			// 5/5/2014 rmd : If we haven't yet reached the criteria level keep reseting our token rcvd
			// count and token made count. Has to be latching, once reaches limit stays above limit, as
			// the pdata content appears during normal real-time user editing activity.
			if( comm_mngr.since_the_scan__slave__number_of_clean_tokens_rcvd < REQUIRED_NUMBER_OF_CLEAN_TOKENS_SINCE_SCAN )
			{
				comm_mngr.since_the_scan__slave__number_of_clean_tokens_rcvd = 0;

				// 5/5/2014 rmd : And clear the clean token_resp count too. It is not allowed to progress
				// while we keep rcving pdata type tokens.
				comm_mngr.since_the_scan__slave__number_of_clean_token_responses_made = 0;
			}
		}
		
		// ----------

		if( ( content_ok ) && ( B_IS_SET( icm->pieces, BIT_TI_I_stations_for_your_list ) ) )
		{
			UNS_16	number_of_stations;
			
			memcpy( &number_of_stations, ucp, sizeof(UNS_16) );
			
			ucp += sizeof( UNS_16 );
			
			STATION_FROM_MAIN_LIST_DISTRIBUTION_STRUCT	sds;
			
			// 2011.12.01 rmd : Have seen this take 1.75 seconds for 768 stations. But that is not the
			// sheer volume of work that has to be done here. By far almost ALL of that time has to do
			// with the fact that this task (COMM_MNGR) is a lower priority than TD_CHECK. So if a
			// second ticks off we check for starts, do irrigation list maintenance, and likely some
			// display work. If we suspend the scheduler that time drops well below 250ms. I didn't
			// measure any tighter than that. Hell it could be 10ms. I don't know.
			for( i=0; i<number_of_stations; i++ )
			{
				memcpy( &sds, ucp, sizeof(STATION_FROM_MAIN_LIST_DISTRIBUTION_STRUCT) );
				
				ucp += sizeof( STATION_FROM_MAIN_LIST_DISTRIBUTION_STRUCT );
				
				irri_add_to_main_list( &sds );
			}
			
		}
		
		// -------------

		if( ( content_ok ) && ( B_IS_SET( icm->pieces, BIT_TI_I_here_are_the_action_needed_records ) ) )
		{
			ucp += __extract_action_needed_records_from_the_incoming_flowsense_token( ucp );
		}
		
		// -------------

		if( ( content_ok ) && ( B_IS_SET( icm->pieces, BIT_TI_I_here_is_a_stop_command ) ) )
		{
			__extract_stop_key_record_from_the_incoming_flowsense_token( &ucp );
		}
		
		// -------------

		// 5/16/2012 rmd : NOTE - for the mlb we moved the BIT test to inside the process_mlb_info
		// function.
		if( content_ok )
		{
			// 5/15/2012 rmd : The BIT test is incorporated within the mlb_info function. If there is an
			// error returns a NULL. This was done to consolidate the code. The absence of the BIT being
			// set is our way of clearing the mlbs and if we didn't do it in the function we'd be doing
			// that here. And I want to keep the clutter down in the main message parsing function.
			content_ok = __extract_mlb_info_from_the_incoming_flowsense_token( &ucp, from_serial_number, icm->pieces );
		}
		
		// -------------

		if( ( content_ok ) && B_IS_SET( icm->pieces, BIT_TI_I_here_are_box_currents ) )
		{
			content_ok = __extract_the_box_currents_from_the_incoming_flowsense_token( &ucp );
		}
		
		// -------------

		if( ( content_ok ) && B_IS_SET( icm->pieces, BIT_TI_I_here_is_the_terminal_short_ACK_or_no_current ) )
		{
			content_ok = extract_a_terminal_short_acknowledge_from_the_incoming_flowsense_token( &ucp );
		}
		
		// -------------

		if( ( content_ok ) && B_IS_SET( icm->pieces, BIT_TI_I_two_wire_cable_excessive_current_ack_from_master ) )
		{
			content_ok = extract_a_two_wire_cable_anomaly_from_the_incoming_flowsense_token( &ucp, &tpmicro_data.two_wire_cable_excessive_current_xmission_state, irri_comm.two_wire_cable_excessive_current );
		}
		
		// -------------

		if( ( content_ok ) && B_IS_SET( icm->pieces, BIT_TI_I_two_wire_cable_overheated_ack_from_master ) )
		{
			content_ok = extract_a_two_wire_cable_anomaly_from_the_incoming_flowsense_token( &ucp, &tpmicro_data.two_wire_cable_over_heated_xmission_state, irri_comm.two_wire_cable_overheated );
		}
		
		// -------------

		if( ( content_ok ) && B_IS_SET( icm->pieces, BIT_TI_I_two_wire_cable_cooled_off_from_master ) )
		{
			UNS_32		box_index_0;

			memcpy( &box_index_0, ucp, sizeof(UNS_32) );
			
			ucp += sizeof(UNS_32);
			
			// 11/11/2014 rmd : Range check before use and to prohibit any further message parsing.
			content_ok = (box_index_0 < MAX_CHAIN_LENGTH);
			
			if( content_ok )
			{
				// 11/11/2014 rmd : Always set the display flag for the box index slot.
				irri_comm.two_wire_cable_overheated[ box_index_0 ] = (false);
			}
		}

		// -------------

		if( ( content_ok ) && B_IS_SET( icm->pieces, BIT_TI_I_here_is_real_time_weather_data_to_display ) )
		{
			// 10/13/2014 ajv : This function uses the message FROM serial
			// number to know to drop the values if the message is from
			// ourselves.
			content_ok = extract_real_time_weather_data_out_of_msg_from_main( &ucp, from_serial_number );
		}

		// -------------

		if( ( content_ok ) && B_IS_SET( icm->pieces, BIT_TI_I_here_is_the_et_rain_tables ) )
		{
			// This function uses the message FROM serial number to know to drop the table if it is from
			// ourselves.
			content_ok = WEATHER_TABLES_extract_et_rain_tables_out_of_msg_from_main( &ucp, from_serial_number );
		}
		
		// -------------

		if( (content_ok) && ( B_IS_SET( icm->pieces, BIT_TI_I_rain_switch_active ) ) )
		{
			GuiVar_StatusRainSwitchState = (true);
		}
		else
		{
			GuiVar_StatusRainSwitchState = (false);
		}
				
		// -------------

		if( (content_ok) && ( B_IS_SET( icm->pieces, BIT_TI_I_freeze_switch_active ) ) )
		{
			GuiVar_StatusFreezeSwitchState = (true);
		}
		else
		{
			GuiVar_StatusFreezeSwitchState = (false);
		}

		// -------------

		if( (content_ok) && (B_IS_SET( icm->pieces, BIT_TI_I_here_is_the_clear_runaway_gage_command)) )
		{
			// 2/7/2013 ajv : Although one may expect to clear the weather_
			// preserves.run_away_gage flag here, we have to wait until the
			// message to clear the runaway gage has been passed to the TP
			// Micro. Therefore, instead, we're going to set a flag which
			// indicates that, on the next incoming message from the TP Micro,
			// clear the flag and ignore whatever the runaway gage setting is
			// that's received.
			tpmicro_data.et_gage_clear_runaway_gage_being_processed = (true);

			// Set the flag for the TP Micro to clear it's flag as well.
			tpmicro_data.et_gage_clear_runaway_gage = (true);
		}

		// -------------

		if( (content_ok) && (B_IS_SET( icm->pieces, BIT_TI_I_send_me_crc_list)) )
		{
			// Set the flag to notify the IRRI machine to pass along it's CRC
			// list for comparison to the master.
			irri_comm.send_crc_list = (true);
		}

		// -------------

		// 5/8/2014 ajv : If the network isn't ready, ignore any incoming system
		// and POC flow information.
		if( lnetwork_is_ready )
		{
			if( ( content_ok ) && ( B_IS_SET( icm->pieces, BIT_TI_I_here_is_the_system_info ) ) )
			{
				// This function uses the message FROM serial number to know NOT to overwrite certain parts
				// of the system preserves.
				content_ok = IRRI_FLOW_extract_system_info_out_of_msg_from_main( &ucp, from_serial_number );
			}

			// ----------

			if( ( content_ok ) && ( B_IS_SET( icm->pieces, BIT_TI_I_here_is_the_poc_info ) ) )
			{
				// Extract the sent poc information.
				content_ok = IRRI_FLOW_extract_the_poc_info_from_the_token( &ucp );
			}
		}

		// ----------

		// 7/29/2015 mpd : Added Lights ON list data to token.
		if( ( content_ok ) && ( B_IS_SET( icm->pieces, BIT_TI_L_lights_for_on_list ) ) )
		{
			UNS_16	number_of_lights;

			memcpy( &number_of_lights, ucp, sizeof(UNS_16) );

			ucp += sizeof( UNS_16 );

			LIGHTS_LIST_XFER_RECORD	llxr;

			for( i = 0; i < number_of_lights; i++ )
			{
				memcpy( &llxr, ucp, sizeof( LIGHTS_LIST_XFER_RECORD ) );

				ucp += sizeof( LIGHTS_LIST_XFER_RECORD );

				IRRI_LIGHTS_process_rcvd_lights_on_list_record( &llxr );
			}
		}

		// ----------

		// 8/4/2015 mpd : Added Lights action needed list data to token.
		if( ( content_ok ) && ( B_IS_SET( icm->pieces, BIT_TI_L_lights_for_action_needed_list ) ) )
		{
			UNS_16	number_of_lights;

			memcpy( &number_of_lights, ucp, sizeof(UNS_16) );

			ucp += sizeof( UNS_16 );

			LIGHTS_ACTION_XFER_RECORD	laxr;

			for( i = 0; i < number_of_lights; i++ )
			{
				memcpy( &laxr, ucp, sizeof( LIGHTS_ACTION_XFER_RECORD ) );

				ucp += sizeof( LIGHTS_ACTION_XFER_RECORD );

				IRRI_LIGHTS_process_rcvd_lights_action_needed_record( &laxr );
			}
		}

		// ----------

		// 12/01/2015 skc : Add ability to send date time data to IRRI's
		if( (content_ok) && ( B_IS_SET( icm->pieces, BIT_TI_I_here_is_date_time ) ) )
		{
			DATE_TIME_TOKEN_STRUCT datetime;

			memcpy( &datetime, ucp, sizeof(DATE_TIME_TOKEN_STRUCT) );

			ucp += sizeof( DATE_TIME_TOKEN_STRUCT );

			// 12/06/2015 skc : Don't process if message is from ourself. We already set the time
			if( from_serial_number != config_c.serial_number )
			{
				EPSON_set_date_time( datetime.dt, datetime.reason );
			}
		}

		// ----------

		// 5/9/2016 ajv : Parse the Perform 2-Wire Discovery message
		if( (content_ok) && ( B_IS_SET( icm->pieces, BIT_TI_I_perform_two_wire_discovery ) ) )
		{
			// 5/9/2016 ajv : Trigger the discovery now. If the network isn't ready yet, or if this
			// controller doesn't have a 2-Wire module, the process will be skipped.
			TWO_WIRE_perform_discovery_process( (true), (false) );
		}

		// ----------

		if( ( content_ok ) && (B_IS_SET( icm->pieces, BIT_TI_I_here_are_latest_alerts ) == (true)) )
		{
			// 11/25/2014 ajv : This routine will return FALSE if the size of the incoming alerts, as
			// specified in the message content, is larger than the pile can handle. The only time we
			// should see this is if the data has somehow become trashed prior to landing here.
			content_ok = ALERTS_extract_and_store_alerts_from_comm( &ucp, &alerts_struct_user );
		}
		
		// ----------
		// ----------
		
		// RESPONSE ... RESPONSE ... RESPONSE ... RESPONSE ... RESPONSE ... RESPONSE ... RESPONSE
		// RESPONSE ... RESPONSE ... RESPONSE ... RESPONSE ... RESPONSE ... RESPONSE ... RESPONSE
		// RESPONSE ... RESPONSE ... RESPONSE ... RESPONSE ... RESPONSE ... RESPONSE ... RESPONSE

		// ----------
		// ----------

		// 3/21/2012 rmd : With the introduction of the broadcast TOKEN we only respond if the token
		// was actually to us.
		if( to_serial_number == config_c.serial_number )
		{
			// ----------
		
			// ================ THIS IS THE MOMENT THE BUS IS OURS ==================

			// 3/26/2014 rmd : Only if the message is for us! But commented out since we have no such
			// packets as these at this time. Mainly because slaves do not originate central responses.
			// So far central traffic only stays within the master.
			//Xmit_All_Packets_Waiting_For_Token();

			// ----------
		
			// BUILD THE RESPONSE
			comm_stats.token_responses_generated += 1;

			ldh.dlen =	sizeof( FOAL_COMMANDS );

			// ----------
		
			// 10/2/2012 rmd : The comm_mngr structure can be read or written to by several tasks. Take
			// the change flags about the need to distribute an edit for example. They are written to by
			// the key processing task and read and written to by the comm_mngr task. In this particular
			// instance we are looking at the variable set to cause changes to be sent to the master. It
			// is held within the comm_mngr struct.
			xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );
		
			// ----------
			
			is_clean = (true);
			
			// ----------
			
			DATA_HANDLE		pdata_handle;
			
			UNS_32			build_results;
			
			// This function will set the number of bytes to xfer and allocate and load memory with
			// them.
			if( comm_mngr.changes.send_changes_to_master == (true) )
			{
				// 8/4/2016 rmd : If the from serial number equals the to serial number then we are the
				// master and this token_resp is being built for ourselves. Which means the message will not
				// pass through the transport or any serial drivers. It is placed directly, as a whole
				// message, onto our incoming message list. To solve a syncing problem where the collected
				// data builds up in the IRRI side of the MASTER allow a large token_resp to be
				// built.
				build_results = PDATA_build_data_to_send( &pdata_handle, CHANGE_REASON_SYNC_SENDING_TO_MASTER, (to_serial_number == from_serial_number) );

				// 4/23/2015 rmd : We CAN COUNT ON the handle being NULL'd and .dlen zeroed by the
				// preceeding function if no message to send!
		
				// ----------
				
				if( build_results == PDATA_BUILD_RESULTS_built_a_message )
				{
					// 4/23/2015 rmd : Message built. Nothing to do.
				}
				else
				if( build_results == PDATA_BUILD_RESULTS_no_data )
				{
					// 9/19/2013 ajv : Only reset the changes_made_on_this_controller flag if we've sent
					// everything there is to send.
		
					// 10/2/2012 rmd : MUTEX protected edit of this variable. So even if user is in the midst of
					// editing integrity is upheld.
					comm_mngr.changes.send_changes_to_master = (false);
				}
				else
				if( build_results == PDATA_BUILD_RESULTS_no_memory )
				{
					// 4/23/2015 rmd : No memory right now. Nothing to do. Wait for next token to try again.
				}
			}
			else
			{
				pdata_handle.dlen = 0;

				pdata_handle.dptr = NULL;
			}

			ldh.dlen += pdata_handle.dlen;

			// 5/5/2014 rmd : Test to be non-zero (carry content). If so not clean by our definition.
			if( pdata_handle.dlen )
			{
				is_clean = (false);	
			}

			// ----------

			UNS_32		box_configuration_bytes;

			UNS_8		*box_configuration_ptr;

			// This function will set the number of bytes to xfer and allocate and load memory with
			// them.
			box_configuration_bytes = load_box_configuration_to_ship_to_master( &box_configuration_ptr );

			ldh.dlen += box_configuration_bytes;

			// 5/5/2014 rmd : Test to be non-zero (carry content). If so not clean by our definition.
			if( box_configuration_bytes )
			{
				is_clean = (false);	
			}

			// ----------

			if( irri_comm.send_stop_key_record )
			{
				ldh.dlen += sizeof( STOP_KEY_RECORD_s );
			}
			
			// ----------
			
			// 5/5/2014 rmd : If clean bump our criteria count.
			if( is_clean )
			{
				comm_mngr.since_the_scan__slave__number_of_clean_token_responses_made += 1;
			}
			else
			{
				// 5/5/2014 rmd : If we haven't yet reached the criteria level keep reseting our token rcvd
				// count and token made count. Has to be latching, once reaches limit stays above limit, as
				// the pdata content appears during normal real-time user editing activity.
				if( comm_mngr.since_the_scan__slave__number_of_clean_token_responses_made < REQUIRED_NUMBER_OF_CLEAN_TOKENS_SINCE_SCAN )
				{
					comm_mngr.since_the_scan__slave__number_of_clean_token_responses_made = 0;
	
					// 5/5/2014 rmd : And clear the clean token rcvd count too. Creating a more strict criteria.
					comm_mngr.since_the_scan__slave__number_of_clean_tokens_rcvd = 0;
				}
			}
			
			// ------------

			UNS_32		mlb_system_gids_to_clear_bytes;

			UNS_8		*mlb_system_gids_to_clear_ptr;
		
			// This function will set the number of bytes to xfer and allocate and load memory with
			// them.
			mlb_system_gids_to_clear_bytes = __load_system_gids_to_clear_mlb_for( &mlb_system_gids_to_clear_ptr );
			
			ldh.dlen += mlb_system_gids_to_clear_bytes;
			
			// ------------

			UNS_32		box_current_bytes;

			UNS_8		*box_current_ptr;
		
			// This function will set the number of bytes to xfer and allocate and load memory with
			// them.
			box_current_bytes = __load_box_current_to_ship_to_master( &box_current_ptr );
			
			ldh.dlen += box_current_bytes;
			
			// ------------

			UNS_32		terminal_short_bytes;

			UNS_8		*terminal_short_ptr;
		
			// This function will set the number of bytes to xfer and allocate and load memory with
			// them.
			terminal_short_bytes = load_terminal_short_or_no_current_to_ship_to_master( &terminal_short_ptr );
			
			ldh.dlen += terminal_short_bytes;
			
			// ------------

			UNS_32		two_wire_cable_info_bytes;

			UNS_8		*two_wire_cable_info_ptr;
		
			// This function will set the number of bytes to xfer and allocate and load memory with
			// them.
			two_wire_cable_info_bytes = load_two_wire_cable_current_measurement_to_ship_to_master( &two_wire_cable_info_ptr );
			
			ldh.dlen += two_wire_cable_info_bytes;
			
			// ------------

			UNS_32		test_request_bytes;

			UNS_8		*test_request_ptr;
		
			// This function will set the number of bytes to xfer and allocate and load memory with
			// them.
			test_request_bytes = load_test_request_to_ship_to_master( &test_request_ptr );
			
			ldh.dlen += test_request_bytes;
			
			// ------------

			UNS_32		et_gage_count_bytes;

			UNS_8		*et_gage_count_ptr;
		
			// This function will set the number of bytes to xfer and allocate and load memory with
			// them.
			et_gage_count_bytes = load_et_gage_count_to_ship_to_master( &et_gage_count_ptr );
			
			ldh.dlen += et_gage_count_bytes;
			
			// ------------

			UNS_32		rain_bucket_count_bytes;

			UNS_8		*rain_bucket_count_ptr;
		
			// This function will set the number of bytes to xfer and allocate and load memory with
			// them.
			rain_bucket_count_bytes = load_rain_bucket_count_to_ship_to_master( &rain_bucket_count_ptr );
			
			ldh.dlen += rain_bucket_count_bytes;
			
			// ------------

			UNS_32		wind_bytes;

			UNS_8		*wind_ptr;

			// This function will set the number of bytes to xfer and allocate and load memory with
			// them.
			wind_bytes = load_wind_to_ship_to_master( &wind_ptr );

			ldh.dlen += wind_bytes;

			// ------------

			UNS_32		manual_water_request_bytes;

			UNS_8		*manual_water_request_ptr;

			// This function will set the number of bytes to xfer and allocate and load memory with
			// them.
			manual_water_request_bytes = load_manual_water_request_to_ship_to_master( &manual_water_request_ptr );

			ldh.dlen += manual_water_request_bytes;

			// ------------

			UNS_32		decoder_faults_bytes;

			UNS_8		*decoder_faults_ptr;

			decoder_faults_bytes = load_decoder_faults_to_ship_to_master( &decoder_faults_ptr );

			ldh.dlen += decoder_faults_bytes;

			// ----------

			UNS_32		poc_update_bytes;

			UNS_8		*poc_update_ptr;

			if( lnetwork_is_ready )
			{
				// This function will set the number of bytes to xfer and allocate and load memory with
				// them.
				poc_update_ptr = build_poc_update_to_ship_to_master( &poc_update_bytes );
			}
			else
			{
				poc_update_bytes = 0;

				poc_update_ptr = NULL;
			}

			ldh.dlen += poc_update_bytes;

			// ----------

			UNS_32		moisture_sensor_reading_bytes;

			UNS_8		*moisture_sensor_reading_ptr;

			if( lnetwork_is_ready )
			{
				// 2/25/2016 rmd : This function will set the number of bytes to xfer and allocate and load
				// memory with them.
				moisture_sensor_reading_ptr = MOISTURE_SENSOR_find_a_sensor_reading_to_send_to_the_master( &moisture_sensor_reading_bytes );
			}
			else
			{
				moisture_sensor_reading_bytes = 0;

				moisture_sensor_reading_ptr = NULL;
			}

			ldh.dlen += moisture_sensor_reading_bytes;

			// ----------

			// 8/12/2015 mpd : Added light ON request to the token response.
			UNS_32		light_on_request_bytes;

			UNS_8		*light_on_request_ptr;

			// This function will set the number of bytes to xfer and allocate and load memory with
			// them.
			light_on_request_bytes = load_light_on_request_to_ship_to_master( &light_on_request_ptr );

			ldh.dlen += light_on_request_bytes;

			// ------------

			// 8/12/2015 mpd : Added light OFF request to the token response.
			UNS_32		light_off_request_bytes;

			UNS_8		*light_off_request_ptr;

			// This function will set the number of bytes to xfer and allocate and load memory with
			// them.
			light_off_request_bytes = load_light_off_request_to_ship_to_master( &light_off_request_ptr );

			ldh.dlen += light_off_request_bytes;

			// ------------

			UNS_32		mvor_request_bytes;

			UNS_8		*mvor_request_ptr;

			mvor_request_bytes = load_mvor_request_to_ship_to_master( &mvor_request_ptr );

			ldh.dlen += mvor_request_bytes;

			// ------------

			UNS_32		walk_thru_bytes;

			UNS_8		*walk_thru_ptr;

			// This function will set the number of bytes to xfer and allocate and load memory with
			// them.
			walk_thru_bytes = WALK_THRU_load_walk_thru_gid_for_token_response( &walk_thru_ptr );

			ldh.dlen += walk_thru_bytes;

			// ------------

			UNS_32		chain_sync_bytes;

			UNS_8		*chain_sync_ptr;

			// This function will set the number of bytes to xfer and allocate and load memory with
			// them.
			chain_sync_bytes = CHAIN_SYNC_load_a_chain_sync_crc_into_the_token_response( &chain_sync_ptr );

			ldh.dlen += chain_sync_bytes;

			// ----------

			ALERTS_DISPLAY_STRUCT	*ldisplay_struct;

			DATE_TIME	latest_alert_timestamp;

			UNS_32	lindex_of_start_of_alert;

			// Check whether the FOAL master has requested our latest alert
			// time stamp.
			if( B_IS_SET( icm->pieces, BIT_TI_I_send_me_latest_alert_timestamp ) == (true) )
			{
				ldisplay_struct = ALERTS_get_display_structure_for_pile( &alerts_struct_user );

				lindex_of_start_of_alert = ldisplay_struct->all_start_indicies[ 0 ];

				// 10/31/2012 rmd : Just skip over the alert id. We don't specifically need it.
				ALERTS_inc_index( &alerts_struct_user, &lindex_of_start_of_alert, sizeof(UNS_16) );
				
				// 10/31/2012 rmd : Now get what we're after - the time stamp.
				ALERTS_pull_object_off_pile( &alerts_struct_user, &latest_alert_timestamp, &lindex_of_start_of_alert, sizeof(latest_alert_timestamp) );

				ldh.dlen += sizeof( DATE_TIME );
			}

			// ----------
			// ----------

			// 4/30/2014 rmd : DONE figuring the size. Build the message and send it.
			
			// ----------
			
			if( ldh.dlen > __largest_token_resp_size )
			{
				__largest_token_resp_size = ldh.dlen;
				
				Alert_largest_token_size( __largest_token_resp_size, (true) );
			}

			// ----------
			
			ldh.dptr = mem_malloc( ldh.dlen );
	
			ucp = ldh.dptr;
	
			// THE HEADER
			rm = (FOAL_COMMANDS*)ucp;
	
			rm->mid = MID_IRRI_TO_FOAL__TOKEN_RESP;
			rm->pieces = 0;  // clean
	
	
			ucp += sizeof( FOAL_COMMANDS );
	
			// ------------

			if( (pdata_handle.dlen > 0) && (pdata_handle.dptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_I_here_is_program_data );

				memcpy( ucp, pdata_handle.dptr, pdata_handle.dlen );

				ucp += pdata_handle.dlen;

				mem_free( pdata_handle.dptr );
			}
	
			// ------------

			if( (box_configuration_bytes > 0) && (box_configuration_ptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_I_here_is_my_box_configuration );

				memcpy( ucp, box_configuration_ptr, box_configuration_bytes );

				ucp += box_configuration_bytes;

				mem_free( box_configuration_ptr );
			}

			// ------------

			// 4/29/2014 rmd : Are we to request the master to start a scan? If so do it. Adds ZERO
			// bytes to the message.
			if( comm_mngr.request_a_scan_in_the_next_token_RESP || comm_mngr.i_am_a_slave_and_i_have_rebooted )
			{
				B_SET( rm->pieces, BIT_TF_I_am_request_you_start_a_scan );
				
				// 4/29/2014 rmd : And NOTE we do not clear either of the flags here. We clear then when our
				// scan message comes in. I think this works fine. I mean if we keep getting tokens why not
				// keep requesting till we see an incoming scan message. Which actually this should be the
				// last token generated. The comm_mngr will respond to the scan request immediately.
			}
	
			// ------------

			// Flag if the STOP key was pressed. This adds ZERO bytes to the message.
			if( irri_comm.send_stop_key_record )
			{
				B_SET( rm->pieces, BIT_TF_I_stop_key_pressed );
				
				memcpy( ucp, &irri_comm.stop_key_record, sizeof(STOP_KEY_RECORD_s) );
				
				ucp += sizeof(STOP_KEY_RECORD_s);
				
				// 11/6/2014 rmd : And clear the need to send the record.
				irri_comm.send_stop_key_record = (false);
			}
	
			// ------------

			// Add the mlb clearing info.
			if( (mlb_system_gids_to_clear_bytes > 0) && (mlb_system_gids_to_clear_ptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_I_system_gids_to_clear_mlb_for );
				
				memcpy( ucp, mlb_system_gids_to_clear_ptr, mlb_system_gids_to_clear_bytes );
				
				ucp += mlb_system_gids_to_clear_bytes;
				
				mem_free( mlb_system_gids_to_clear_ptr );
			}
	
			// ------------

			// Add the mlb clearing info.
			if( (box_current_bytes > 0) && (box_current_ptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_I_here_is_my_box_current_ma );
				
				memcpy( ucp, box_current_ptr, box_current_bytes );
				
				ucp += box_current_bytes;
				
				mem_free( box_current_ptr );
			}
	
			// ------------

			if( (terminal_short_bytes > 0) && (terminal_short_ptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_I_reporting_a_terminal_short_or_no_current );
				
				memcpy( ucp, terminal_short_ptr, terminal_short_bytes );
				
				ucp += terminal_short_bytes;
				
				mem_free( terminal_short_ptr );
			}
	
			// ------------

			if( tpmicro_data.two_wire_cable_excessive_current_xmission_state == TWO_WIRE_CABLE_ANOMALY_STATE_send_to_master )
			{
				B_SET( rm->pieces, BIT_TF_I_reporting_two_wire_cable_excessive_current );
				
				tpmicro_data.two_wire_cable_excessive_current_xmission_state = TWO_WIRE_CABLE_ANOMALY_STATE_waiting_for_reponse_from_master;
			}

			// ------------

			if( tpmicro_data.two_wire_cable_over_heated_xmission_state == TWO_WIRE_CABLE_ANOMALY_STATE_send_to_master )
			{
				B_SET( rm->pieces, BIT_TF_I_reporting_two_wire_cable_overheated );
				
				tpmicro_data.two_wire_cable_over_heated_xmission_state = TWO_WIRE_CABLE_ANOMALY_STATE_waiting_for_reponse_from_master;
			}

			// ------------

			if( tpmicro_data.two_wire_cable_cooled_off_xmission_state == TWO_WIRE_CABLE_ANOMALY_STATE_send_to_master )
			{
				B_SET( rm->pieces, BIT_TF_I_reporting_two_wire_cable_cooled_off );

				tpmicro_data.two_wire_cable_cooled_off_xmission_state = TWO_WIRE_CABLE_ANOMALY_STATE_waiting_for_reponse_from_master;
			}

			// ------------

			if( (two_wire_cable_info_bytes > 0) && (two_wire_cable_info_ptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_I_two_wire_cable_current_measurement );
				
				memcpy( ucp, two_wire_cable_info_ptr, two_wire_cable_info_bytes );
				
				ucp += two_wire_cable_info_bytes;
				
				mem_free( two_wire_cable_info_ptr );
			}
	
			// ------------

			if( (test_request_bytes > 0) && (test_request_ptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_I_test_request );
				
				memcpy( ucp, test_request_ptr, test_request_bytes );
				
				ucp += test_request_bytes;
				
				mem_free( test_request_ptr );
			}
	
			// ------------

			if( (et_gage_count_bytes > 0) && (et_gage_count_ptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_I_reporting_et_gage_pulse_count );
				
				memcpy( ucp, et_gage_count_ptr, et_gage_count_bytes );
				
				ucp += et_gage_count_bytes;
				
				mem_free( et_gage_count_ptr );
			}
	
			// ------------

			if( (rain_bucket_count_bytes > 0) && (rain_bucket_count_ptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_I_reporting_rain_bucket_pulse_count );
				
				memcpy( ucp, rain_bucket_count_ptr, rain_bucket_count_bytes );
				
				ucp += rain_bucket_count_bytes;
				
				mem_free( rain_bucket_count_ptr );
			}
	
			// ------------

			if( (wind_bytes > 0) && (wind_ptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_I_reporting_wind );

				memcpy( ucp, wind_ptr, wind_bytes );

				ucp += wind_bytes;

				mem_free( wind_ptr );
			}

			// ------------

			if( tpmicro_comm.rain_switch_active == (true) )
			{
				B_SET( rm->pieces, BIT_TF_I_reporting_rain_switch_active );
			}

			// ------------

			if( tpmicro_comm.freeze_switch_active == (true) )
			{
				B_SET( rm->pieces, BIT_TF_I_reporting_freeze_switch_active );
			}

			// ------------

			// Flag if the TP Micro reported a runaway gage. This adds ZERO
			// bytes to the message.
			if( tpmicro_data.et_gage_runaway_gage_in_effect_to_send_to_master == (true) )
			{
				B_SET( rm->pieces, BIT_TF_I_reporting_runaway_gage );

				tpmicro_data.et_gage_runaway_gage_in_effect_to_send_to_master = (false);
			}

			// ------------

			// Flag if the Clear Runaway Gage key was pressed. This adds ZERO
			// bytes to the message.
			if( irri_comm.clear_runaway_gage_pressed == (true) )
			{
				B_SET( rm->pieces, BIT_TF_I_clear_runaway_gage_key_pressed );

				irri_comm.clear_runaway_gage_pressed = (false);
			}

			// ------------

			if( (manual_water_request_bytes > 0) && (manual_water_request_ptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_I_manual_water_request );

				memcpy( ucp, manual_water_request_ptr, manual_water_request_bytes );

				ucp += manual_water_request_bytes;

				mem_free( manual_water_request_ptr );
			}

			// ------------

			if( (decoder_faults_bytes > 0) && (decoder_faults_ptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_I_reporting_decoder_faults );

				memcpy( ucp, decoder_faults_ptr, decoder_faults_bytes );

				ucp += decoder_faults_bytes;

				mem_free( decoder_faults_ptr );
			}

			// ------------

			// 8/12/2015 mpd : Added for lights.
			if( (light_on_request_bytes > 0) && (light_on_request_ptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_L_light_on_request );

				memcpy( ucp, light_on_request_ptr, light_on_request_bytes );

				ucp += light_on_request_bytes;

				mem_free( light_on_request_ptr );
			}

			// ----------

			// 8/12/2015 mpd : Added for lights.
			if( (light_off_request_bytes > 0) && (light_off_request_ptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_L_light_off_request );

				memcpy( ucp, light_off_request_ptr, light_off_request_bytes );

				ucp += light_off_request_bytes;

				mem_free( light_off_request_ptr );
			}

			// ----------

			if( (poc_update_bytes > 0) && (poc_update_ptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_I_here_is_an_update_for_my_pocs );

				memcpy( ucp, poc_update_ptr, poc_update_bytes );

				ucp += poc_update_bytes;

				mem_free( poc_update_ptr );
			}

			// ----------

			if( (moisture_sensor_reading_bytes > 0) && (moisture_sensor_reading_ptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_I_here_is_a_moisture_reading_string );

				memcpy( ucp, moisture_sensor_reading_ptr, moisture_sensor_reading_bytes );

				ucp += moisture_sensor_reading_bytes;

				mem_free( moisture_sensor_reading_ptr );
			}

			// ----------

			if( (mvor_request_bytes > 0) && (mvor_request_ptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_I_mvor_request );

				memcpy( ucp, mvor_request_ptr, mvor_request_bytes );

				ucp += mvor_request_bytes;

				mem_free( mvor_request_ptr );
			}

			// ----------

			if( (walk_thru_bytes > 0) && (walk_thru_ptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_I_walk_thru_start_request );

				memcpy( ucp, walk_thru_ptr, walk_thru_bytes );

				ucp += walk_thru_bytes;

				mem_free( walk_thru_ptr );
			}

			// ------------

			if( (chain_sync_bytes > 0) && (chain_sync_ptr != NULL) )
			{
				B_SET( rm->pieces, BIT_TF_I_here_is_a_single_chain_sync_crc );

				memcpy( ucp, chain_sync_ptr, chain_sync_bytes );

				ucp += chain_sync_bytes;

				mem_free( chain_sync_ptr );
			}

			// ------------

			if( B_IS_SET( icm->pieces, BIT_TI_I_send_me_latest_alert_timestamp ) )
			{
				B_SET( rm->pieces, BIT_TF_I_here_is_latest_alert_timestamp );

				memcpy( ucp, &latest_alert_timestamp, sizeof(latest_alert_timestamp) );

				ucp += sizeof( latest_alert_timestamp );
			}
	
			// ------------

			xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );
		
			// ------------
			
			// SEND THE RESPONSE !!!!
			SendCommandResponseAndFree( ldh, cmli, MSG_CLASS_CS3000_TOKEN_RESP );
			
			// ------------

		}  // of if the message was addressed to us
		
		// ----------
		
		// 8/4/2016 rmd : RESTORE comm_mngr priority back to the original lower than TDCHECK
		// priority. DO NOT get tempted to leave comm_mngr at the higher priority. That will
		// potentially break the MUTEX DEADLOCK protection scheme we have in place here. Doing that
		// would just simply reverse the roles of the task in the deadlock and that deadlock or
		// another would then be able to occur.
		vTaskPrioritySet( NULL, PRIORITY_LEVEL_COMM_MNGR_TASK );
		
	}
	
	__this_time_delta = (my_tick_count - __this_time_delta);
	
	if( __this_time_delta > __largest_token_parsing_delta )
	{
		__largest_token_parsing_delta = __this_time_delta;
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

