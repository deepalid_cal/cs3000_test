/*  file = tpmicro_isp_mode.c      rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"tpmicro_isp_mode.h"

#include	"tpmicro_comm.h"

#include	"rcvd_data.h"

#include	"serport_drvr.h"

#include	"alerts.h"

#include	"flash_storage.h"

#include	"app_startup.h"

#include	"code_distribution_task.h"

#include	"cs_mem.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// BASE ASSUMPTIONS AND IMPORTANT NOTES

// 1.	Assuming the file is larger than 1K. The math that achieves sending the first block
// 		last makes this assumption.
//
// 2.	We send the FISRT 1K block LAST to the micro. Why? Well that's the block that
//  	contains the marker of valid user code present. So if we sent and wrote to flash
//  	first and then there was a power failure during the last 50+ remaining 1K blocks we
//  	would be in a state where the resident bootloader thought there was valid code. BUT
//  	in actuality there was not. The bootloader would turn execution over to this broken
//  	code image. I have seen where the micro is then lost in that junk code. Which may
//  	result in an 'unreachable micro'. Meaning you cannot without the JTAG get the micro
//  	back into ISP mode. A real problem in the field. To MINIMIZE this hole to prepare
//  	for programming the first thing we do is to erase the entire flash (destroying the
//  	valid code marker). And the last thing we do is write the first 1K block to flash -
//  	restoring the valid code marker. It takes about 8ms to write 1K bytes to the flash.
//  	So I think there a small window where if power were to be removed we could get a
//  	junk code image in flash that looked good to the bootloader based upon that valid
//  	code marker.
//
// 3.	If one wanted to tighten the chance of the described unusable state there are
//  	probably several schemes available. One would be to permanately install J2 on the
//  	TPMicro card. This would force the bootloader into ISP mode on each cold start. The
//  	CS3000 main board could get ahold of it and read out the code image and verify
//  	against the file stored in the flash file system. This would be a 20 some second
//  	delay upon system power up.
//
// 4.	Another scheme might be to write a small program that resided in the first sector of
//  	flash (4Kbyte). This program would never change or be erased. It could in turn check
//  	the real TPMicro application in the rest of flash - say sectors 2 through 21. If it
//  	liked what it saw based upon some scheme it could transfer control. If not invoke
//  	the ISP.
//
// 5.	There are probably other crafty schemes one could devise. We will wait and see if
//		our write the first block last scheme is good enough.
 
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	UUENCODE_LINE_BYTE_COUNT_LIMIT		(45)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


static void __uuencode_and_send_up_to_next_45_bytes( void )
{
	// 2/26/2014 rmd : Look at how many bytes remain in the file. Send up to 45 data bytes. If
	// less than 45 bytes remaining pad up to a multiple of three to send. Add to running
	// checksum. Bump line count.
	
	UNS_32		bytes_in_this_line, encode_index, xmit_index;
	
	UNS_8		bytes_to_encode[ UUENCODE_LINE_BYTE_COUNT_LIMIT ];

	UNS_8		xmit_line[ 64 ];  // an actual line sent is 61 + CR + LF = 63 bytes maximum
	
	DATA_HANDLE	ldh;

	UNS_8		ch1, ch2, ch3, n1, n2, n3, n4;

	UNS_32		remaining_to_send, bytes_left_in_1K_block;

	// ----------

	// 2/26/2014 rmd : Clear so any pad bytes are already 0.
	memset( bytes_to_encode, 0x00, UUENCODE_LINE_BYTE_COUNT_LIMIT );

	// ----------

	bytes_left_in_1K_block = (1024 - tpmicro_comm.uuencode_bytes_in_this_1K_block_sent);
	

	if( tpmicro_comm.uuencode_bytes_left_in_file_to_send <= bytes_left_in_1K_block )
	{
		remaining_to_send = tpmicro_comm.uuencode_bytes_left_in_file_to_send;
	}
	else
	{
		remaining_to_send = bytes_left_in_1K_block;
	}
	
	
	if( remaining_to_send >= UUENCODE_LINE_BYTE_COUNT_LIMIT )
	{
		bytes_in_this_line = UUENCODE_LINE_BYTE_COUNT_LIMIT;
	}
	else
	{
		bytes_in_this_line = remaining_to_send;
	}

	// ----------

	memcpy( bytes_to_encode, tpmicro_comm.isp_where_from_in_file, bytes_in_this_line );
		
	tpmicro_comm.uuencode_bytes_left_in_file_to_send -= bytes_in_this_line;

	tpmicro_comm.isp_where_from_in_file += bytes_in_this_line;

	tpmicro_comm.uuencode_bytes_in_this_1K_block_sent += bytes_in_this_line;

	// ----------

	// 2/26/2014 rmd : At this point bytes_in_this_line reflects the TRUE user payload. Which is
	// what the first byte is to represent. So load that first byte of the line now.
	xmit_index = 0;

	// 2/26/2014 rmd : The sent line length is the byte count + 32 per the specificiation.
	xmit_line[ xmit_index++ ] = bytes_in_this_line + 32;

	// ----------

	// 2/26/2014 rmd : Pad till a multiple of 3. UUencode requirement. For the case of a full
	// line nothing happens here. For the partial we pad.
	while( (bytes_in_this_line % 3) != 0 )
	{
		bytes_in_this_line += 1;
	}

	// ----------

	// 2/26/2014 rmd : Prepare the handle for transmission now.
	ldh.dptr = xmit_line;
	
	// 2/26/2014 rmd : The length byte + (data_bytes/3*4) + CR + LF.
	ldh.dlen = 1 + ((bytes_in_this_line/3)*4) + 2;
	
	// ----------
	
	encode_index = 0;

	while( bytes_in_this_line )
	{
		// ----------

		// 2/26/2014 rmd : Always 3 at a time. And total is for sure a multiple of three!
		bytes_in_this_line -= 3;

		// ----------

		ch1 = bytes_to_encode[ encode_index++ ];
		ch2 = bytes_to_encode[ encode_index++ ];
		ch3 = bytes_to_encode[ encode_index++ ];

		n1 = 0;
		n2 = 0;
		n3 = 0;
		n4 = 0;

		// ----------

		// 2/26/2014 rmd : Update checksum.
		tpmicro_comm.uuencode_running_checksum_20 += (ch1 + ch2 + ch3);
		
		// ----------

		// 2/26/2014 rmd : Perform the actual encoding in an explicit manner. Not very compact but
		// easy to see the actual process.
		if ((ch1-128)>=0)   {ch1-=128;  n1+=32;}
		if ((ch1-64)>=0)    {ch1-=64;   n1+=16;}
		if ((ch1-32)>=0)    {ch1-=32;   n1+=8;}
		if ((ch1-16)>=0)    {ch1-=16;   n1+=4;}
		if ((ch1-8)>=0)     {ch1-=8;    n1+=2;}
		if ((ch1-4)>=0)     {ch1-=4;    n1+=1;}
		
		if ((ch1-2)>=0)     {ch1-=2;    n2+=32;}
		if ((ch1-1)>=0)     {ch1-=1;    n2+=16;}
		if ((ch2-128)>=0)   {ch2-=128;  n2+=8;}
		if ((ch2-64)>=0)    {ch2-=64;   n2+=4;}
		if ((ch2-32)>=0)    {ch2-=32;   n2+=2;}
		if ((ch2-16)>=0)    {ch2-=16;   n2+=1;}
		
		if ((ch2-8)>=0)     {ch2-=8;    n3+=32;}
		if ((ch2-4)>=0)     {ch2-=4;    n3+=16;}
		if ((ch2-2)>=0)     {ch2-=2;    n3+=8;}
		if ((ch2-1)>=0)     {ch2-=1;    n3+=4;}
		if ((ch3-128)>=0)   {ch3-=128;  n3+=2;}
		if ((ch3-64)>=0)    {ch3-=64;   n3+=1;}
		
		if ((ch3-32)>=0)    {ch3-=32;   n4+=32;}
		if ((ch3-16)>=0)    {ch3-=16;   n4+=16;}
		if ((ch3-8)>=0)     {ch3-=8;    n4+=8;}
		if ((ch3-4)>=0)     {ch3-=4;    n4+=4;}
		if ((ch3-2)>=0)     {ch3-=2;    n4+=2;}
		if ((ch3-1)>=0)     {ch3-=1;    n4+=1;}
		
		if (n1 == 0x00) n1=0x60;
		else n1+=0x20;
		if (n2 == 0x00) n2=0x60;
		else n2+=0x20;
		if (n3 == 0x00) n3=0x60;
		else n3+=0x20;
		if (n4 == 0x00) n4=0x60;
		else n4+=0x20;

		// ----------

		xmit_line[ xmit_index++ ] = n1;
		xmit_line[ xmit_index++ ] = n2;
		xmit_line[ xmit_index++ ] = n3;
		xmit_line[ xmit_index++ ] = n4;

		// ----------

	}
	
	// ----------

	xmit_line[ xmit_index++ ] = '\r';
	xmit_line[ xmit_index++ ] = '\n';
	
	// ----------

	if( xmit_index == ldh.dlen )
	{
		AddCopyOfBlockToXmitList( UPORT_TP, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );
		
		// 2/26/2014 rmd : When we reach 20 lines we have to send the checksum.
		tpmicro_comm.uuencode_checksum_line_count_20 += 1;
	}
	else
	{
		Alert_Message( "UUENCODE length mismatch" );
	}
	
}

/* ---------------------------------------------------------- */
/*
void create_valid_checksum_for_first_8_words( void )
{
	// 2/25/2014 rmd : This function works. But as it turns out is not needed cause the binary
	// image produced by Crossworks already has the correct 2's complement in place.
	//
	// 2/25/2014 rmd : The 8th word is to contain the 2s complement of the sum of the first 7
	// words. That way the checksum of the first 8 words is zero. And that is what the
	// bootloader use to decide if the flash contains valid user code to run.
	
	UNS_32	*vector_ptr;

	UNS_32	iii, lsum;
	
	vector_ptr = (UNS_32*)tpmicro_comm.isp_tpmicro_file.dptr;
	
	lsum = 0;
	
	for( iii=0; iii<7; iii++ )
	{
		lsum += *vector_ptr;
		
		vector_ptr += 1;
	}
	
	// 2/25/2014 rmd : Now take the 2's complement of the sum. Which is actually the negative of
	// the sum. So that when the first 8 location are added together their sum is 0.
	lsum = ~lsum + 1;
	
	// 2/25/2014 rmd : And write into the code image.
	*vector_ptr = lsum;
}
*/

/* ---------------------------------------------------------- */
static void __setup_for_normal_string_exchange( const char* pcommand_str, const char* presponse_str )
{
	DATA_HANDLE	ldh;
	
	// 2/13/2014 rmd : Clean and restart the port buffer and string hunt.
	RCVD_DATA_enable_hunting_mode( UPORT_TP, PORT_HUNT_FOR_SPECIFIED_STRING, WHEN_STRING_FOUND_NOTIFY_THE_COMM_MNGR_TASK );
	
	// ----------
	
	ldh.dptr = (UNS_8*)pcommand_str;
	
	ldh.dlen = strlen( pcommand_str );
	
	// ----------
	
	// 2/12/2014 rmd : The response to the syschronized string is itself plus an OK.
	SerDrvrVars_s[ UPORT_TP ].UartRingBuffer_s.sh.str_to_find = (char*)presponse_str;
	
	SerDrvrVars_s[ UPORT_TP ].UartRingBuffer_s.sh.chars_to_match = strlen( presponse_str );
	
	// 10/30/2016 rmd : Mean the string must be present starting with the first char of the
	// buffer.
	SerDrvrVars_s[ UPORT_TP ].UartRingBuffer_s.sh.depth_into_the_buffer_to_look = 0;

	// ----------
	
	AddCopyOfBlockToXmitList( UPORT_TP, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );
}

/* ---------------------------------------------------------- */

const char question_mark_str[] = "?";

const char question_mark_str_response[] = "Synchronized\r\n";

// ----------

const char synchronized_str[] = "Synchronized\r\n";

// 2/18/2014 rmd : Note that the real response is only the OK. But because the ISP echos
// its incoming commands at this point (prior to use turning off the echo) the expected
// response includes the sync string itself plus the OK response.
const char synchronized_str_response[] = "Synchronized\r\nOK\r\n";

// ----------

const char frequency_str[] = "12000\r\n";

const char frequency_str_response[] = "12000\r\nOK\r\n";

// ----------

const char echo_off_str[] = "A 0\r\n";

// 2/19/2014 rmd : For some reason this command returns an OK and not a 0 to signal command
// success? Is this because the frequency is missing? YES it is. If you skip the frequency
// part the ISP loader is in a kind of EARLY STAGE mode where it answers with OK's instead
// of 0's.
const char echo_off_str_response[] = "A 0\r\n0\r\n";

// ----------

const char part_id_str[] = "J\r\n";

// 2/18/2014 rmd : Normal commands return a command success "0". Note the Part ID for the
// LPC1763 (from the manual).
//const char part_id_str_response[] = "J\r\n0\r\n637607987\r\n";
const char part_id_str_response[] = "0\r\n637607987\r\n";

// ----------

const char ok_str[] = "OK\r\n";

// ----------

const char esc_str[] = "\e";

// ----------

const char unlock_str[] = "U 23130\r\n";

// ----------

const char prepare_str[] = "P 0 21\r\n";

// ----------

const char erase_str[] = "E 0 21\r\n";

// ----------

const char command_success_str[] = "0\r\n";

// ----------

const char go_str[] = "G 0 T\r\n";

// ----------

// 2/25/2014 rmd : The LPC1763 has flash sector 0 through 21. For a total of 256K of flash
// code space.
#define		LPC1763_LAST_SECTOR		(21)

#define		SRAM_BUFFER_START_ADDR	(0x10000200)

#define		SRAM_TEST_AMOUNT		(2048)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void TPMICRO_COMM_kick_out_the_next_isp_tpmicro_msg( COMM_MNGR_TASK_QUEUE_STRUCT *pcmqs )
{
	DATA_HANDLE	ldh;
	
	UNS_32		next_command_ms;
	
	BOOL_32		start_timer;
	
	char		lstr_24[ 24 ];
	
	// ----------

	// 2/13/2014 rmd : Either the response came in or not. In either case we don't need the
	// timer to continue running which it would be if the correct response is the reason we are
	// here.
	xTimerStop( tpmicro_comm.timer_message_rate, portMAX_DELAY );
	
	// 2/21/2014 rmd : For some states we intentionally set this FALSE.
	start_timer = (true);

	// 2/19/2014 rmd : The normal delay between msgs to the tp micro ISP loader. This only comes
	// into play when the expected response does not arrive. Also when we issue the GO command
	// we need to wait a bit ... say 100 ms before sending the first application level packet
	// message.
	next_command_ms = TPMICRO_ISP_MODE_MS_BETWEEN_MSGS;
	
	// ----------

	switch( tpmicro_comm.isp_state )
	{
		case ISP_STATE_question_mark:

			// 2/18/2014 rmd : This is the first of a sequence of steps achieving synchronization with
			// the LPC1763 resident loader (ISP).One way to arrive here is because we (the main board
			// code) commanded the TPMicro to enter the ISP mode via an IAP command. The more common way
			// to fall through here is on startup.
			//
			// The scheme is complicated to think about. But then again not. There may be obscure
			// sequences, including power failures or brown outs, that I suppose could leave the TPMicro
			// in its ISP mode but the main board doesn't realize that. Additionally if an unprogrammed
			// TPMicro is installed it will be running the ISP on power up. And this could happen if
			// there were a power failure mid-stream TPMicro code download (mid-stream between TPMicro
			// and main board).
			//
			// For this reason I have decided the first communication sequence the Main Board code will
			// do is to test for responses from the resident TPMicro ISP loader. It will do this on each
			// power up. Also can and will do this if we request this ourselves after seeing we have a
			// newer code file than the TPMicro is running.
			//
			// If we determine we are in ISP mode based on the responses seen we WILL attempt to load
			// the code.
			//
			// The first few steps of this entry sequence proceed in the blind. Meaning we do not try to
			// find the response. This is because the ISP program ECHOES all incoming characters. And to
			// ease string detection complication I decided to wait till after we get past the autobaud
			// section and issue the first echo off command before we start looking for reponses to
			// confirm we are indeed sync'd up to the ISP bootloader.
			if( pcmqs->event == COMM_MNGR_EVENT_tpmicro_msg_rate_timer_expired )
			{
				// 3/5/2014 rmd : Because of disturbances to timing of the delicate ISP serial exchanges we
				// temporarily boost the priorities of the tasks involved. This could have been fixed maybe
				// by just lowering the display write task priority but then that posses other potential
				// problem if there are keypad activites (queued keys). We perhaps also could have lengthned
				// the ISP expected communication response timeouts but that also could make trouble.
				// Particularly during our ISP probe that takes place during each code startup. Certainly
				// would have lengthened that activity.
				elevate_priority_of_ISP_related_code_update_tasks();

				// ----------
	
				__setup_for_normal_string_exchange( question_mark_str, question_mark_str_response );
				
				// ----------
				
				// 2/19/2014 rmd : We're just starting so no fault yet.
				tpmicro_comm.isp_sync_fault = (false);

				tpmicro_comm.isp_state = ISP_STATE_synchronized;
			}
			else
			{
				// 2/18/2014 rmd : This should never happen. The timer will be restarted, and by not
				// changing state or mode here we'll end up right back through this case.
				ALERT_MESSAGE_WITH_FILE_NAME( "TPMicro comm unexpd event" );
			}

			break;

		// ----------

		case ISP_STATE_synchronized:

			if( pcmqs->event == COMM_MNGR_EVENT_isp_expected_response_arrived )
			{
				// ----------
	
				__setup_for_normal_string_exchange( synchronized_str, synchronized_str_response );
				
				// ----------
				
				tpmicro_comm.isp_state = ISP_STATE_frequency;
			}
			else
			if( pcmqs->event == COMM_MNGR_EVENT_tpmicro_msg_rate_timer_expired )
			{
				// 2/13/2014 rmd : Well the timer expired for one of two reasons. Either we are already in
				// ISP mode past the synchronization or the TPMicro application is running. If ISP is
				// already running, because of a major anomoly (like a fault or power disruption during a
				// prior ISP session) we must try our best to get a hold of it. We'll march through the
				// sequence without looking for any responses. Till we get to the PART ID request. If that
				// fails we abandon. But the first thing we should do is send an ESC to clear the "?" sent
				// so far. This is sort of a give one more try to find out if we are in ISP mode or not. See
				// if we can get a hold of it.
				tpmicro_comm.isp_sync_fault = (true);

				tpmicro_comm.isp_state = ISP_STATE_esc_to_clear;
			}
			
			break;

		// ----------

		case ISP_STATE_esc_to_clear:

			// 2/19/2014 rmd : Clean up string hunting mode. Leaves str_to_find NULL which means no
			// string search takes place.
			RCVD_DATA_enable_hunting_mode( UPORT_TP, PORT_HUNT_FOR_PACKETS, NOT_STRING_HUNTING  );

			// ----------
			
			ldh.dptr = (UNS_8*)esc_str;

			ldh.dlen = strlen( esc_str );

			// ----------
			
			AddCopyOfBlockToXmitList( UPORT_TP, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );

			// 2/19/2014 rmd : Always move to the frequency mode.
			tpmicro_comm.isp_state = ISP_STATE_frequency;

			break;


		// ----------

		case ISP_STATE_frequency:

			if( pcmqs->event == COMM_MNGR_EVENT_isp_expected_response_arrived )
			{
				// ----------
	
				__setup_for_normal_string_exchange( frequency_str, frequency_str_response );
				
				// ----------
				
				tpmicro_comm.isp_state = ISP_STATE_echo_off;
			}
			else
			if( pcmqs->event == COMM_MNGR_EVENT_tpmicro_msg_rate_timer_expired )
			{
				if( tpmicro_comm.isp_sync_fault )
				{
					// 2/19/2014 rmd : Sync Fault already seen. So we were not expecting a response and this is
					// the right place to be. Yes we saw an ealier error and now we are progressing blindly for
					// a few commands. Until finally when we send the PART_ID command we had better see the
					// correct response or it's time to abandon ISP mode and go run the tpmicro application.

					// Fault mode. No response expected.
					__setup_for_normal_string_exchange( frequency_str, NULL );
					
					// ----------
					
					tpmicro_comm.isp_state = ISP_STATE_echo_off;
				}
				else
				{
					// 2/19/2014 rmd : Soooo. We didn't get our expected response. We send the esc and complete
					// the ISP entry sequence in the blind. Meaning not looking for any responses. Till we ask
					// for the PART_ID. If that passes we'll load code.
					tpmicro_comm.isp_sync_fault = (true);
					
					tpmicro_comm.isp_state = ISP_STATE_esc_to_clear;
				}
			}
			
			break;


		// ----------

		case ISP_STATE_echo_off:

			if( pcmqs->event == COMM_MNGR_EVENT_isp_expected_response_arrived )
			{
				// ----------
	
				__setup_for_normal_string_exchange( echo_off_str, echo_off_str_response );
	
				// ----------
				
				tpmicro_comm.isp_state = ISP_STATE_part_id;
			}
			else
			if( pcmqs->event == COMM_MNGR_EVENT_tpmicro_msg_rate_timer_expired )
			{
				if( tpmicro_comm.isp_sync_fault )
				{
					// 2/19/2014 rmd : Sync Fault already seen. So we were not expecting a response and this is
					// the right place to be. Yes we saw an ealier error and now we are progressing blindly for
					// a few commands. Until finally when we send the PART_ID command we had better see the
					// correct response or it's time to abandon ISP mode and go run the tpmicro application.

					// Fault mode. No response expected.
					__setup_for_normal_string_exchange( echo_off_str, NULL );
					
					// ----------
					
					tpmicro_comm.isp_state = ISP_STATE_part_id;
				}
				else
				{
					// 2/19/2014 rmd : Soooo. We didn't get our expected response. We send the esc and complete
					// the ISP entry sequence in the blind. Meaning not looking for any responses. Till we ask
					// for the PART_ID. If that passes we'll load code.
					tpmicro_comm.isp_sync_fault = (true);
					
					tpmicro_comm.isp_state = ISP_STATE_esc_to_clear;
				}
			}
			
			break;


		// ----------

		case ISP_STATE_part_id:

			// 2/19/2014 rmd : OKAY! No matter how we arrived here we are going to feed the request for
			// part ID and look for it's response. And it had better work or we are going to abandon the
			// attempt to communicate with the ISP!

			// ----------

			__setup_for_normal_string_exchange( part_id_str, part_id_str_response );

			// ----------
			
			tpmicro_comm.isp_state = ISP_STATE_read_file;

			break;

		// ----------

		case ISP_STATE_read_file:

			if( pcmqs->event == COMM_MNGR_EVENT_isp_expected_response_arrived )
			{
				// 2/24/2014 rmd : Yeah! This means we are on. We got the expected part ID response. And
				// consider the ISP exchanges to be working properly. Let the user know what is going on
				// during the apparent standstill.
				Alert_message_on_tpmicro_pile_T( "TPMicro ISP executing ... " );	

				// 9/23/2014 rmd : User notification on the CS3000 main display.
				CODE_DOWNLOAD_draw_tp_micro_update_dialog();

				// ----------

				// 2/21/2014 rmd : We are going to break now from the typical response - message progress.
				// And request the tpmicro file be read into memory. When that file read activity has
				// completed, the file system send us a queue message. Don't need the timer. Am waiting on
				// queue message.
				start_timer = (false);
				
				// 2/24/2014 rmd : Note it takes about 40ms to read the tpmicro file. At it's present size
				// of about 60K. It doesn't matter much cause we just wait for the flash file system to
				// report back to us via a queue message.
				FLASH_STORAGE_request_code_image_file_read( TPMICRO_APP_FILENAME, FLASH_TASK_EVENT_read_code_image_file_to_memory_for_tpmicro_isp_mode );
				
				// ----------
				
				tpmicro_comm.isp_state = ISP_STATE_unlock;
			}
			else
			{
				// 2/19/2014 rmd : Okay so after our best effort, in the face of a faulted response and
				// desparate attempt at recovery, we must give up.
				tpmicro_comm.isp_state = ISP_STATE_abandon;
			}
			
			break;

		// ----------

		case ISP_STATE_unlock:

			// 2/24/2014 rmd : Should be the file read completion event.
			if( pcmqs->event == COMM_MNGR_EVENT_tpmicro_binary_file_read_for_isp_complete )
			{
				// 2/24/2014 rmd : Did the file read?
				if( pcmqs->dh.dptr == NULL )
				{
					// 2/24/2014 rmd : File read incomplete. Can be for two primary reasons. First is file not
					// found. And second is there is SHUTDOWN impending. First of all the file should always be
					// there (after first written). And second if a shutdown is impending this task should not
					// be running.
					Alert_Message( "Unexp'd file read result." );
	
					// 2/24/2014 rmd : Quit - not working right.
					tpmicro_comm.isp_state = ISP_STATE_abandon;
				}
				else
				{
					// 2/24/2014 rmd : File in place. With the correct 2's complement checksum in the 8th word
					// location. Marking the binary as valid user code. The 2's complement value is calculated
					// by Crossworks.
					//
					// 2/21/2014 rmd : Capture the memory handle. Remember we are taking on memory release
					// responsibility.
					tpmicro_comm.isp_tpmicro_file = pcmqs->dh;
					
					// ----------
					
					// 2/20/2015 rmd : Have we been flagged to REWRITE the tpmicro code image to the file
					// system. This is needed to fix a quirk that occurs during the file upgrade process. And
					// will only be used ONCE. The quirk is that because the tpmicro file was written in the old
					// format it repeatedly thinks the tpmicro is not running the latest code and repeats the
					// ISP process. Over and over again. Until the tpmicro file is re-written in our new
					// format.
					//
					// 2/20/2015 rmd : This variable and the associated code within this IF can be REMOVED once
					// all controllers out there have been updated to this new file format. That would be after
					// say MARCH 1, 2015.
					if( weather_preserves.write_tpmicro_file_in_new_file_format )
					{
						// 2/20/2015 rmd : We NEED a new block of memory because the flash file system write takes
						// control of the memory block for the special case of writing a code image file. Even
						// though the function NAME implies differently.
						UNS_8	*tpmicro_code_image_ptr;
						
						if( mem_obtain_a_block_if_available( pcmqs->dh.dlen, (void**)&tpmicro_code_image_ptr ) )
						{
							// 2/20/2015 rmd : Put the code image into place in the new memory.
							memcpy( tpmicro_code_image_ptr, pcmqs->dh.dptr, pcmqs->dh.dlen );
							
							// 2/20/2015 rmd : Clear the battery backed variable. Which is ONLY set upon the file
							// upgrade to the new DIR_ENTRY definition update.
							weather_preserves.write_tpmicro_file_in_new_file_format = (false);
		
							// 2/20/2015 rmd : Don't let this SPECIAL code image write trigger a reboot. Not needed and
							// may cause confusion.
							cdcs.restart_after_receiving_tp_micro_firmware = (false);
		
							FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file( FLASH_INDEX_0_BOOTCODE_AND_EXE,
																					TPMICRO_APP_FILENAME,
																					// 2/20/2015 rmd : The version value is 0 here. And is figured out from the actual code
																					// image in memory at the actual write time.
																					0,
																					// 2/20/2015 rmd : Be sure to use the block obtained to support this file write.
																					tpmicro_code_image_ptr,
																					pcmqs->dh.dlen,
																					NULL,
																					// 2/20/2015 rmd : The file name 'number' is not used for code image files.
																					0 );
						}
					}
					
					// ----------

					// 2/27/2014 rmd : Notice we skip over the first 1K block. This math is assuming the file is
					// larger than 1K and I think that's fair considering it is at 50K now. This is our scheme
					// to write the FIRST block LAST to protect against partial yet valid looking code image
					// existing in flash. Actually all the bootloader needs to declare the code in flash valid
					// is the first 8 locations. But we'll leave the whole first 1K for last. That's just
					// easier. The smallest amount we could skip over would be 256 bytes (the smallest flash
					// write allowed).
					tpmicro_comm.isp_where_from_in_file = (tpmicro_comm.isp_tpmicro_file.dptr + 1024);

					tpmicro_comm.isp_where_to_in_flash = 1024;

					tpmicro_comm.uuencode_bytes_left_in_file_to_send = (pcmqs->dh.dlen - 1024);
					
					tpmicro_comm.uuencode_first_1K_block_sent = (false);
					
					// ----------

					// 2/27/2014 rmd : Because the amount written to SRAM must be a multiple of 4 (ISP W command
					// requirement) fix the length if not a multiple of 4. Remember we just setting how many
					// bytes we read from the code image memory. It is okay to go past the end of the actual
					// code image as that wont affect execution. Just writes 'junk' into flash at the end of the
					// image. Which is okay. As a matter of fact we are probably writing lots of 'junk' into
					// flash at the end of image because we ALWAYS copy/write to flash 1024 bytes at a time. So
					// the last 1K block will likely contain 'junk'.
					while( tpmicro_comm.uuencode_bytes_left_in_file_to_send % 4 != 0 )
					{
						tpmicro_comm.uuencode_bytes_left_in_file_to_send += 1;
					}
					
					// ----------
	
					tpmicro_comm.uuencode_running_checksum_20 = 0;

					tpmicro_comm.uuencode_checksum_line_count_20 = 0;
					
					tpmicro_comm.uuencode_bytes_in_this_1K_block_sent = 0;
					
					// ----------
	
					// 2/25/2014 rmd : Right now we are passing through the initial UNLOCK string. Followed by a
					// PREPARE string so we can then ERASE the entire flash. So following the prepare we go to
					// erase.
					tpmicro_comm.isp_after_prepare_state = ISP_STATE_erase;
					
					// ----------
	
					__setup_for_normal_string_exchange( unlock_str, command_success_str );
	
					// ----------
					
					tpmicro_comm.isp_state = ISP_STATE_prepare;
				}
			}
			else
			{
				// 2/24/2014 rmd : Should never see this alert. Should only arrive in this state by the file
				// read event message.
				Alert_Message( "File read problem." );

				// 2/24/2014 rmd : Quit - not working right.
				tpmicro_comm.isp_state = ISP_STATE_abandon;
			}
			
			break;

		// ----------

		case ISP_STATE_prepare:

			if( pcmqs->event == COMM_MNGR_EVENT_isp_expected_response_arrived )
			{
				// ----------

				__setup_for_normal_string_exchange( prepare_str, command_success_str );

				// ----------
				
				tpmicro_comm.isp_state = tpmicro_comm.isp_after_prepare_state;
			}
			else
			{
				// 2/19/2014 rmd : Okay so after all that, in the face of a faulted response and desparate
				// attempt at recovery, we must give up.
				tpmicro_comm.isp_state = ISP_STATE_abandon;
			}
			
			break;

		// ----------

		case ISP_STATE_erase:

			if( pcmqs->event == COMM_MNGR_EVENT_isp_expected_response_arrived )
			{
				// ----------

				__setup_for_normal_string_exchange( erase_str, command_success_str );

				// ----------

				// 2/25/2014 rmd : The FLASH erase activity takes about 100ms to complete. So we need to
				// wait much longer than normal for the response.
				next_command_ms = 250;

				// ----------
				
				tpmicro_comm.isp_state = ISP_STATE_write_to_sram;
			}
			else
			{
				// 2/25/2014 rmd : Unexpected response, we must give up.
				tpmicro_comm.isp_state = ISP_STATE_abandon;
			}
			
			break;

		// ----------

		case ISP_STATE_write_to_sram:

			if( pcmqs->event == COMM_MNGR_EVENT_isp_expected_response_arrived )
			{
				if( tpmicro_comm.uuencode_bytes_left_in_file_to_send > 1024 )
				{
					snprintf( lstr_24, sizeof(lstr_24), "W %d 1024\r\n", 0x10000200 );
				}
				else
				{
					snprintf( lstr_24, sizeof(lstr_24), "W %d %d\r\n", SRAM_BUFFER_START_ADDR, tpmicro_comm.uuencode_bytes_left_in_file_to_send );
				}

				// ----------

				__setup_for_normal_string_exchange( lstr_24, command_success_str );

				// ----------
				
				tpmicro_comm.isp_state = ISP_STATE_uuencode_and_send_one_line;
			}
			else
			{
				// 2/25/2014 rmd : Unexpected response, we must give up.
				tpmicro_comm.isp_state = ISP_STATE_abandon;
			}
			
			break;

		// ----------

		case ISP_STATE_uuencode_and_send_one_line:

			// 2/27/2014 rmd : For the case we are sending uuencoded line after uuencoded line, up till
			// 20 of them in a row, there is no response from the LPC between the lines. So we ignore if
			// we got a response or not. We could be looking for a response, but in very few cases.
			// Primarily only the first case after we issue the W to SRAM command.
			__uuencode_and_send_up_to_next_45_bytes();
			
			if( tpmicro_comm.uuencode_bytes_left_in_file_to_send == 0 )
			{
				tpmicro_comm.isp_state = ISP_STATE_uuencode_send_checksum;
			}
			else
			if( tpmicro_comm.uuencode_checksum_line_count_20 == 20 )
			{
				tpmicro_comm.isp_state = ISP_STATE_uuencode_send_checksum;
			}
			else
			if( tpmicro_comm.uuencode_bytes_in_this_1K_block_sent == 1024 )
			{
				tpmicro_comm.isp_state = ISP_STATE_uuencode_send_checksum;
			}
			else
			{
				// No state change. Send another line in 20ms.
			}
			
			break;

		// ----------

		case ISP_STATE_uuencode_send_checksum:
		
			// 2/26/2014 rmd : Its just a timer that gets us here. No reponse expected.
			
			snprintf( lstr_24, sizeof(lstr_24), "%d\r\n", tpmicro_comm.uuencode_running_checksum_20 );

			// 2/26/2014 rmd : Oddly enough the response to the checksum is an "OK" (if all good). If
			// not we quit.
			__setup_for_normal_string_exchange( lstr_24, ok_str );
					
			// ----------

			// 2/26/2014 rmd : We sent the checksum. These are always restarted.
			tpmicro_comm.uuencode_running_checksum_20 = 0;

			tpmicro_comm.uuencode_checksum_line_count_20 = 0;
					
			// ----------

			if( (tpmicro_comm.uuencode_bytes_left_in_file_to_send == 0) || (tpmicro_comm.uuencode_bytes_in_this_1K_block_sent == 1024) )
			{
				// 2/26/2014 rmd : We want to copy from the SRAM to the FLASH. But before we can do that we
				// must do a PREPARE command to allow the flash write.
				tpmicro_comm.isp_state = ISP_STATE_prepare;

				tpmicro_comm.isp_after_prepare_state = ISP_STATE_copy_to_flash;
			}
			else
			{
				tpmicro_comm.isp_state = ISP_STATE_uuencode_and_send_one_line;
			}
			
			break;

		// ----------

		case ISP_STATE_copy_to_flash:

			if( pcmqs->event == COMM_MNGR_EVENT_isp_expected_response_arrived )
			{
				// 2/26/2014 rmd : Now I'm going to do something really strange here. The flash write
				// sub-system can only copy data from sram to flash in 256 byte chunks. Of course our
				// standard 1024 chunks are fine and dandy. But the last one could be any size up to 1024.
				// But must be a multiple of 256. So for the case of the short last block, even though the
				// SRAM likely contains left over data from the prior write to flash, bump our length up so
				// we always write a full 1024 bytes. When the code executes properly the bytes on the end
				// will be of no consequence.
				//
				// 2/26/2014 rmd : NOTE the 1024 byte copy/write to flash takes 5ms to 10ms (measured 8ms).
				// So our usual 20ms dwell between commands should suffice.
				snprintf( lstr_24, sizeof(lstr_24), "C %d %d 1024\r\n", tpmicro_comm.isp_where_to_in_flash, SRAM_BUFFER_START_ADDR );
	
				// 2/26/2014 rmd : And now the response to this is the 0 success string.
				__setup_for_normal_string_exchange( lstr_24, command_success_str );

				// ----------
				
				tpmicro_comm.isp_where_to_in_flash += 1024;

				tpmicro_comm.uuencode_bytes_in_this_1K_block_sent = 0;

				// ----------
				
				if( tpmicro_comm.uuencode_bytes_left_in_file_to_send == 0 )
				{
					if( tpmicro_comm.uuencode_first_1K_block_sent == (false) )
					{
						// 2/27/2014 rmd : Restore what we need to write the first block now.
						tpmicro_comm.isp_where_from_in_file = tpmicro_comm.isp_tpmicro_file.dptr;
						
						tpmicro_comm.isp_where_to_in_flash = 0;
	
						tpmicro_comm.uuencode_bytes_left_in_file_to_send = 1024;
						
						tpmicro_comm.uuencode_first_1K_block_sent = (true);
						
						// 2/27/2014 rmd : Now go back and write this last block.
						tpmicro_comm.isp_state = ISP_STATE_write_to_sram;
					}
					else
					{
						// 2/26/2014 rmd : Done writing the file. Go seek response to this write and execute the GO
						// command.
						tpmicro_comm.isp_state = ISP_STATE_go_execute;
					}
				}
				else
				{
					// 2/26/2014 rmd : Work on writing some more data to SRAM for transfer to flash.
					tpmicro_comm.isp_state = ISP_STATE_write_to_sram;
				}
			}
			else
			{
				// 2/25/2014 rmd : Unexpected response, we must give up.
				tpmicro_comm.isp_state = ISP_STATE_abandon;
			}
			
			break;

		// ----------

		case ISP_STATE_go_execute:

			if( pcmqs->event == COMM_MNGR_EVENT_isp_expected_response_arrived )
			{
				// 12/8/2014 rmd : Be sure to disable the string hunting NOW. Before we send the go_str to
				// the tpmicro. Because the go_str responds with the SUCCESS string and that will tigger an
				// event for the comm_mngr. The consequence of which is we will NOT WAIT FOR THE TIMER
				// before we send the FIRST message to the tpmicro. And it will miss that message cause it
				// ain't fully booted. Which screws up the startup exchange sequence between the main board
				// and the tpmicro.
				RCVD_DATA_enable_hunting_mode( UPORT_TP, PORT_HUNT_FOR_PACKETS, NOT_STRING_HUNTING  );
				
				// 2/19/2014 rmd : We have issued the go command. Switch to the normal messaging mode.
				tpmicro_comm.in_ISP = (false);
				
				// 4/1/2014 rmd : When we leave ISP, wether doing an actual update or an abandoned initial
				// attempt on startup (both occur on startup actually) we always setup an exchange with the
				// tpmicro to make sure the code time and date matches our file. See the startup sequence
				// chart. Upon recognition that the running tpmicro code is in sync with our file copy, only
				// then will a scan be allowed to commence.
				setup_to_check_if_tpmicro_needs_a_code_update();
				
				// 2/19/2014 rmd : Give time for the tpmicro application to actually startup. To get into a
				// position to receive the first application level packet message. This takes about 45ms. So
				// we'll allow 120ms.
				//
				// 12/8/2014 rmd : It seems this doesn't work with such a short delay. Not exactly sure why.
				// But behaved as if the tpmicro was missing the first message. I have seen it work at
				// 1000ms. So increasing to 1500 ms. Have tested with about 12 unprogrammed tpmicros and all
				// passed. (This isn't like me to not know EXACTLY why the shorter delay doesn't work. But I
				// feel very confident in what I'm doing here. In between programming the tpmicro and before
				// starting the scan. Even if the scan is not real [standalone]. So I'm mostly comfortable
				// leaving this as is.)
				next_command_ms = 1500;

				// ----------
				
				ldh.dlen = strlen( go_str );
				
				ldh.dptr = (UNS_8*)go_str;
				
				AddCopyOfBlockToXmitList( UPORT_TP, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );

				// ----------
	
				// 2/24/2014 rmd : If the block was allocated free it.
				if( tpmicro_comm.isp_tpmicro_file.dptr != NULL )
				{
					mem_free( tpmicro_comm.isp_tpmicro_file.dptr );
					
					// 2/24/2014 rmd : And show it as unallocated.
					tpmicro_comm.isp_tpmicro_file.dptr = NULL;
				}
				
				// ----------
	
				// 3/5/2014 rmd : Bring the ISP related task back down to normal priority.
				restore_priority_of_ISP_related_code_update_tasks();
				
				// ----------

				CODE_DOWNLOAD_close_dialog( (false) );

				// ----------

				Alert_Message( "TPMicro code ISP update completed" );	

				Alert_message_on_tpmicro_pile_M( "TPMicro code ISP update completed" );
			}
			else
			{
				// 2/25/2014 rmd : Unexpected response, we must give up.
				tpmicro_comm.isp_state = ISP_STATE_abandon;
			}
			
			break;

		// ----------

		case ISP_STATE_abandon:

			// 2/24/2014 rmd : If the block was allocated free it.
			if( tpmicro_comm.isp_tpmicro_file.dptr != NULL )
			{
				mem_free( tpmicro_comm.isp_tpmicro_file.dptr );
				
				// 2/24/2014 rmd : And show it as unallocated.
				tpmicro_comm.isp_tpmicro_file.dptr = NULL;
			}
			
			// ----------

			// 3/5/2014 rmd : Bring the ISP related task back down to normal priority.
			restore_priority_of_ISP_related_code_update_tasks();
			
			// ----------

			// 2/13/2014 rmd : Disables string mode and enables packet mode.
			RCVD_DATA_enable_hunting_mode( UPORT_TP, PORT_HUNT_FOR_PACKETS, NOT_STRING_HUNTING  );

			// 2/19/2014 rmd : Leave ISP. Switch to the normal packetized messaging mode.
			tpmicro_comm.in_ISP = (false);
			
			// 4/1/2014 rmd : When we leave ISP, wether doing an actual update or an abandoned initial
			// attempt on startup (both occur on startup actually) we always setup an exchange with the
			// tpmicro to make sure the code time and date matches our file. See the startup sequence
			// chart. Upon recognition that the running tpmicro code is in sync with our file copy, only
			// then will a scan be allowed to commence.
			setup_to_check_if_tpmicro_needs_a_code_update();
			
			break;

	}

	// ----------

	// 2/25/2014 rmd : When we read the file in we do not need to start this timer. Cause we are
	// waiting for a queue message from the flash file system. Which will show for sure. File
	// read failure or not. If doesn't show major system malfunction taking place.
	if( start_timer )
	{
		// 2/13/2014 rmd : We set the timer based upon the context. And are counting on the fact
		// that the response comes in well before that time. So we can stop the timer before it goes
		// off.
		//
		// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
		// when posting is safe. Anything on the timer queue is quickly processed and therefore the
		// queue is effectively always empty.
		xTimerChangePeriod( tpmicro_comm.timer_message_rate, MS_to_TICKS( next_command_ms ), portMAX_DELAY );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

