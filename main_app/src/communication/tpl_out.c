/* file = tpl_out.c                          2011.07.19  rmd  */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for rand
#include	<stdlib.h>

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"tpl_out.h"

#include	"crc.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"configuration_controller.h"

#include	"packet_router.h"

#include	"tpl_in.h"

#include	"comm_mngr.h"

#include	"flowsense.h"

#include	"cs_mem.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Manages the outgoing TPL messages. Messages are built and thrown to the Port output xmit list. We must preserve the
// message though as the receipiant may ask for any of the packets to be resent. When we receive the status packet that says
// all packets have been received we may destroy the message.
//
// Designed to handle multiple outgoing messages at once.
//
// Can send messages to another process operating on this hardware - such as the FOAL machine - by directly placing message
// on the incoming ring buffer. A very powerful and clean way to send messages to ourselve. It is actually the default
// communications mode for flow on a loop standalone controllers - that is to send to ourselves.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/28/2014 rmd : The next OM mid to use. Since in the CS3000 we no longer use the
// TRANSPORT format when communicating to the comm_server the next mid selection is much
// less complicated. There were issues between the 2000e and CC4 I'd rather not get into
// here. But that is all history now. So the next mid is SIMPLY the next sequential mid! But
// we do seed the count to start. I suppose just so on a chain the controllers are throwing
// around different mids? It just felt better to do.
static UNS_16	next_mid;


OUTGOING_STATS OutgoingStats;


// Must be initialized on startup as it posseses dynamic allocations.
MIST_LIST_HDR_TYPE OutgoingMessages;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

char const * const cs3000_msg_class_text[ MSG_CLASS_total_number_of_classes ] =
{
	"nlu 0",

	"CS3000_TO_COMMSERVER_VIA_HUB",

	"CS3000_FROM_COMMSERVER",

	"CS3000_SCAN",
	"CS3000_SCAN_RESP",

	"CS3000_TOKEN",
	"CS3000_TOKEN_RESP",

	"CS3000_TO_TPMICRO",
	"CS3000_FROM_TPMICRO",

	"nlu 9",

	"nlu 10",

	"CS3000_FROM_COMMSERVER_CODE_DIST",
	"CS3000_FROM_CONTROLLER_CODE_DIST",
	"CS3000_FROM_HUB_CODE_DIST",
	"CS3000_FROM_HUB_CODE_DIST_ADDR",
	"CS3000_TO_HUB_CODE_DIST",

	"CS3000_TEST_PACKET",
	"CS3000_TEST_PACKET_ECHO"
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/*--------------------------------------------------------------------------
	Name:           CalcCRCForAllPacketsOfOutgoingMessage( LIST_OF_OUTGOING_MESSAGES_ELEMENT *lome )

	Description:    Used when we are done building a new message or preparing for re-send an existing
					message to calculate and insert the CRC into each packet.

	Parameters:     pom     passed pointer to the OM whose packets we are going to calc the CRC for

	Return Values:  None

	History:        10/25/00    --------    rmd     created
					04/12/01    --------    rmd     added "offsetof" to ListInit call


 -------------------------------------------------------------------------*/
void nm_nm_CalcCRCForAllPacketsOfOutgoingMessage( OUTGOING_MESSAGE_STRUCT *pom )
{
	// No mutex accomodations within this function for either the outgoing message list of the list of packets for the message.

	OUTGOING_MESSAGE_PACKET		*omp;

	UNS_8						*ucp;

	UNS_32						crc_len;

	omp = nm_ListGetFirst( &pom->om_packets_list );

	while( omp != NULL )
	{
		// Remember the packets at this point are everything but the pre and post ambles. And therefore we CRC the entire packet
		// except the CRC bytes themselves.
		crc_len = omp->dh.dlen - sizeof(CRC_BASE_TYPE);

		ucp = omp->dh.dptr;

		// ----------

		// 11/6/2012 rmd : Calculate the CRC. Which is returned in BIG ENDIAN format. Which is what
		// we need (see note in crc calc function). Then copy to ucp.
		CRC_BASE_TYPE	lcrc;
		
		lcrc = CRC_calculate_32bit_big_endian( ucp, crc_len );
		
		ucp += crc_len;  // now points to where the crc is stored

		memcpy( ucp, &lcrc, sizeof(CRC_BASE_TYPE) );
		
		// ----------

		omp = nm_ListGetNext( &pom->om_packets_list, omp );
	}
}

/*--------------------------------------------------------------------------
	Name:           DestroyOM( OUTGOING_MESSAGE *pom )

	Description:    This function will destroy (free all memory associated with) an Outgoing Message.
					The pointer to the message is no longer valid after this routine is called.

					A note for RTOS implementation: This function as well as others should make use of the
					"in use" flag. Such a flag would indicate we are working with this outgoing message and to
					destroy would cause the other function ("thread") start using dereferenced pointers.

	Parameters:     pom     a pointer to the OM to destroy

	Return Values:  None

	History:        04/12/01    --------    rmd     created

 -------------------------------------------------------------------------*/
void nm_destroy_OM( OUTGOING_MESSAGE_STRUCT *pom )
{
	// No mutex protection on the IM list itself. We do however protect the list of packets for the particular IM.

	// steps include    1) free the status timer
	//					2) free the exist timer
	//                  2) free each packet - both the list item and the packet itself
	//                  3) remove the message from the list of OM's
	//                  4) free the memory associated with the OM

	OUTGOING_MESSAGE_PACKET *omp;

	// Stop the timer and free its memory.
	xTimerDelete( pom->timer_waiting_for_status_resp, portMAX_DELAY );

	// Stop the timer and free its memory.
	xTimerDelete( pom->timer_exist, portMAX_DELAY );

	// Free the packets and their associated list item memory.
	xSemaphoreTake( pom->list_om_packets_list_MUTEX, portMAX_DELAY );

	while( (omp = nm_ListRemoveHead( &pom->om_packets_list )) )
	{  // removes it from the list
		mem_free( omp->dh.dptr );

		mem_free( omp );
	}

	xSemaphoreGive( pom->list_om_packets_list_MUTEX );

	// Now delete the semaphore (which is implemented as a queue).
	vQueueDelete( pom->list_om_packets_list_MUTEX );

	// then the message element itself
	nm_ListRemove( &OutgoingMessages, pom );

	mem_free( pom );
}

/* ---------------------------------------------------------- */
void TPL_OUT_destroy_all_outgoing_3000_scan_and_token_messages( void )
{
	// original comment: Part of scheme for controlling OM's is to have the application destroy all outgoing messages of ORIGIN
	// type. Here's the scenario. The foal machine originates a message. For some reason the status doesn't come back and that
	// OM is unhappy and authorized to do retries. Lets say that the RESP however comes back to the foal machine (the app). Now
	// the app is going to move forward and talk to the next controller however the transport OM for the previous controller is
	// not happy and could do a retry. Potential collision scenario. Solution is to have the application (the comm mngr) destroy
	// the ORIGIN OM's when he receives the RESP.
	//
	// updated comment 2006.10.25 rmd This really has to do with the FlowSense traffic - the commmngr generates tokens and scan
	// messages which of course cause the commmngr to 'lock up' ie message in process is TRUE - the commmngr is locked up while
	// we wait for a message response - when we receive that response we want to release the commmngr so it is free to generate
	// a subsequent message - generally to the next guy in the chain. In an effort to squash the anomaly where the OM transport
	// doesn't receive his status but the RESP message comes through as an IM - when we route the IM we destroy all active OM's
	// that are of the type that triggered the IM to be sent. This also has the added benefit of reducing memory use as the
	// token OM's are generated in rapid succession.

	OUTGOING_MESSAGE_STRUCT *om;

	xSemaphoreTake( list_tpl_out_messages_MUTEX, portMAX_DELAY );

	om = nm_ListGetFirst( &OutgoingMessages );

	while( om != NULL )
	{
		// The only outgoing message that is considered active is the OM_CLASS_TOKEN - the SCAN class
		// is active in a funny way in that it could time out from lack of a status AFTER the RESP
		// message came in and send up a message to the transport ... so delete their kind too
		if( om->msg_class.routing_class_base == ROUTING_CLASS_IS_3000_BASED )
		{
			if ( (om->msg_class.rclass == MSG_CLASS_CS3000_TOKEN) || (om->msg_class.rclass == MSG_CLASS_CS3000_SCAN) )
			{
				nm_destroy_OM( om );
	
				// since we messed with the list start over
				om = nm_ListGetFirst( &OutgoingMessages );
	
			}
			else
			{
				om = nm_ListGetNext( &OutgoingMessages, om );
			}
		}
	}

	xSemaphoreGive( list_tpl_out_messages_MUTEX );
}

/* ---------------------------------------------------------- */
void TPL_OUT_queue_a_message_to_start_resp_timer( OUTGOING_MESSAGE_STRUCT *pom )
{
	// 6/30/2015 rmd : We no longer EVER send any status back. On any messages.
	Alert_Message( "OM: should not be starting status timer!" );



	// test it just in case
	if( pom != NULL )
	{
		TPL_OUT_EVENT_QUEUE_STRUCT		toqs;

		toqs.event = TPL_OUT_QUEUE_COMMAND_start_waiting_for_status_resp_timer;

		toqs.om = pom;

		if( xQueueSend( TPL_OUT_event_queue, &toqs, 0 ) != pdPASS )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "TPL_OUT queue full!" );
		}
	}
}

/* ---------------------------------------------------------- */
void __start_waiting_for_status_resp_timer( OUTGOING_MESSAGE_STRUCT *pom )
{
	xSemaphoreTake( list_tpl_out_messages_MUTEX, portMAX_DELAY );

	// check lom points to a message on the list
	if( nm_OnList( &OutgoingMessages, pom ) == TRUE )
	{
		// ONLY start the timer if we are an active message. That is we have a time to run.
		if( pom->seconds_to_wait_for_status_resp > 0 )
		{
			// The timer function to change the period actually changes the period and STARTS the timer!
			//
			// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
			// when posting is safe. Anything on the timer queue is quickly processed and therefore the
			// queue is effectively always empty.
			xTimerChangePeriod( pom->timer_waiting_for_status_resp, MS_to_TICKS(pom->seconds_to_wait_for_status_resp * 1000), portMAX_DELAY );
		}
	}
	else
	{
		// This could happen I suppose. Not sure how right now.
		//ALERT_MESSAGE_WITH_FILE_NAME( "OM not on list." );
		Alert_item_not_on_list( pom, OutgoingMessages );
	}

	xSemaphoreGive( list_tpl_out_messages_MUTEX );
}

/* ---------------------------------------------------------- */
/*--------------------------------------------------------------------------
	Name:           Process_OM_StatusTimerExpired( void *pptr )

	Description:    The deferred event created when the OM status expires calls this function.
					It carries a pointer to the OM. Our job here is to look at how many times
					the status has expired and either (1) tell the originator the message failed and
					then destroy the message OR (2) set up the last block sent with a status request
					to be sent again.

	Parameters:     pptr    pointer to the OM whose status timer expired

	Return Values:  None

	History:        10/25/00    --------    rmd     created
					04/12/01    --------    rmd     Comment header updated to reflect function.
													Reduced function complexity to make easier to understand.
													Removed the OM destroy steps to a separate function
													(called DestroyOM) so that function is available to call
													from either inside this function when the status timeouts
													reaches its limit or from the function that pulls apart
													the status when it has recieved a complete status.

 -------------------------------------------------------------------------*/
static void process_waiting_for_status_resp_timer_expired( xTimerHandle pxTimer )
{
	// This is NOT an interrupt. But rather executed within the context of the timer task.

	OUTGOING_MESSAGE_STRUCT			*om;

	OUTGOING_MESSAGE_PACKET			*omp;

	TPL_DATA_HEADER_TYPE			*tdh;

	UNS_32							i;

	unsigned char					*ucp;

	BOOL_32							FoundOneToSend;

	TPL_OUT_EVENT_QUEUE_STRUCT		toqs;

	COMM_MNGR_TASK_QUEUE_STRUCT		cmqs;

	om = (OUTGOING_MESSAGE_STRUCT*)(pvTimerGetTimerID( pxTimer ));

	xSemaphoreTake( list_tpl_out_messages_MUTEX, portMAX_DELAY );

	// check lom points to a message on the list
	if( nm_OnList( &OutgoingMessages, om ) == (false) )
	{
		// Check this scenario out we send a message of a size that is so long that the status timer
		// times out (when the status timer is set to 2 seconds that size happens to be a program
		// start for 18 stations).
		//
		// What can happen is that the timer can time out and build an deferred event about that -
		// and at about the same time the status block could show up Well when the status block
		// shows up the outgoing message is removed from the list. Then the deferred event finally
		// gets around to firing and guess what the message is gone so it doesn't have to remove
		// it.
		//
		// All pointers involved in this scenario appear to be so well behaved that I could make it
		// so this was not an error. I could also make the message status timeout period a function
		// of the number of blocks being sent.
		//
		// In summary, the status timeout needs to be adjusted based on message length.
		//

		// 3/20/2012 rmd : It has been reported (with the 2000e) that this alert can show during
		// heavy RRe-TRAN usage (I suppose coupled with degraded coverage).
		if ( config_c.debug.transport_om_show_unexpected_behavior_alerts == (true) )
		{
			//ALERT_MESSAGE_WITH_FILE_NAME( "OM not on list" );
			Alert_item_not_on_list( om, OutgoingMessages );
		}
	}
	else
	{
		// 11/28/2017 rmd : The timer should not ever be started for the comm_test 2000e messages
		// that pass through this transport. Those are the only 2000e messages we handle, and they
		// don't use the timer. So screen for them to be sure the unexpected isn't occuring.
		if( om->msg_class.routing_class_base == ROUTING_CLASS_IS_2000_BASED )
		{
			Alert_Message( "TPL_OUT : unexp status timer fired" );
		}
		else
		{
			// 11/28/2017 rmd : THIS MUST THEN BE A 3000 CLASS MESSAGE.

			// 10/10/2013 rmd : This timer expiration is a normal event when scanning, it means the
			// controller is not there, so do not show an alert.
			if( om->msg_class.rclass != MSG_CLASS_CS3000_SCAN )
			{
				// 10/10/2013 rmd : And further more if the debug bit doesn't allow don't show.
				if( config_c.debug.transport_om_show_status_timer_expired_alert == (true) )
				{
					// 10/10/2013 rmd : A bit of information. Not that it is all relevant here but is helpful as
					// a refresher. In a packet header, the ONLY time the 3 byte 'address' is not a SERIAL
					// NUMBER is in the outbound CS3000 SCAN packet. And even in that, ONLY the TO ADDRESS is
					// not a serial number. The FROM address is the serial number of the MASTER. The SCAN_RESP
					// packet contains serial numbers as normal.
					//
					// As you can see here we are not attempting to alert about this lack of a status for the
					// case of the SCAN so all adresses are in fact serial numbers. AND they are in BIG ENDIAN
					// format because we are at the transport level.
					
					// ----------
	
					/*
					UNS_32	serial_number;
				
					char	str_4[ 4 ];
	
					// 10/11/2013 rmd : et the 4-byte serial number to 0 in order to 0 all 4 bytes. Build up a
					// little endian version of the 3-byte TO address. Then copy the 3 bytes in little endian
					// format to the serial number. The result is a little endian UNS_32 serial number for the
					// alert lines.
					serial_number = 0;
	
					str_4[ 0 ] = om->from_to.to[ 2 ];
					str_4[ 1 ] = om->from_to.to[ 1 ];
					str_4[ 2 ] = om->from_to.to[ 0 ];
					
					memcpy( &serial_number, str_4, 3 );
					*/
					
					// ----------
					
					// 10/11/2013 rmd : For the case of a flowsense packet generate an alert that ends up on the
					// USER pile. The format is less cryptic than the ENGINEERING version. For the user a
					// missing status event shows as a FLOWSENSE communication retry. And should be viewed as an
					// indication of a poor communication path.
					if( om->msg_class.rclass == MSG_CLASS_CS3000_TOKEN )
					{
						Alert_status_timer_expired_idx( last_contact.index );
					}
					else
					{
						Alert_Message_va( "TPL_OUT: status timer expired : msg_class=%s", cs3000_msg_class_text[ om->msg_class.rclass ] );
					}
				}
			}
	
			// ----------
	
			// If we've reached the limit on the number of tries for status tell the originator and destroy the message.
			if( om->status_timeouts >= om->allowable_timeouts )
			{
				OutgoingStats.NO_Failures++;
	
				// Our goal here is to get a message to the comm mngr about this failed message.
				//
				// We dont necessarily have to tell the originating machine ie the foal machine about the failure as the comm mngr takes
				// care of that via the LiveOnes array. When the number of Live Ones doesn't match the "Number In Chain" setting the foal
				// machine will cease generating messages in the foal mode. It will wait until the irri machine timer times out and sets the
				// lets play forced switch.
	
				// Failures of messages that originated in this controller ie IRRI or CENT messages do not generate a failure message for
				// anybody; that's because those kind of messages do not lock up the comm mngr and the point of this message from the
				// Transport layer is to free up the comm mngr.
	
				// Messages that originated in either the FOAL machine or the comm mngr(scanning) can time out back to the comm mngr. The
				// point of this timeout is to unlock the commmngr and this is how its done.
				if( (om->msg_class.rclass == MSG_CLASS_CS3000_SCAN) || (om->msg_class.rclass == MSG_CLASS_CS3000_TOKEN) )
				{
					// 3/21/2013 rmd : In a sense what we are doing here is indicating to the comm_mngr that
					// this queue posting is a message from the serial number we were trying to send to. When
					// the comm_mngr receives this queue message a test is made between this transport message
					// address and who the comm_mngr last sent a message to. It is a verification that the
					// transport and the comm_mngr are in sync; that the relative timeouts within each are
					// appropriate. But ahhh....don't forget at the transport layer level the addresses are in
					// big endian format. And the comm_mngr works with little endian format. So we have to dance
					// the bytes around.
					cmqs.who_the_message_was_to.from[ 0 ] = om->from_to.to[ 2 ];
					cmqs.who_the_message_was_to.from[ 1 ] = om->from_to.to[ 1 ];
					cmqs.who_the_message_was_to.from[ 2 ] = om->from_to.to[ 0 ];
					
					// And provide the message class so the comm_mngr can further identify this failed message. And we can make a sanity check
					// in the comm_mngr to make sure tasks and messages are in sync like we think they should be.
					cmqs.message_class = om->msg_class.rclass;
	
					cmqs.event = COMM_MNGR_EVENT_last_OM_failed;
	
					COMM_MNGR_post_event_with_details( &cmqs );
				}
				else
				{
					// do not signal upper layer ie commmngr about these outgoing message failures
	
					// do nothing for all of our normally outgoing messages ... these include
					//	* messages from our irri_machine (presumably to the foal_machine_master)
					//  * messages from our central_machine (presumably to central_primary or central_secondary)
					//  * messages to the RRe handheld
	
					// This is the HOLE where we could loose IRRIGATION if the OM to the foal message failed and the irri machine
					// assumed it was sucessful - at least I think we could loose irrigation  - maybe not - at least if the
					// communications kept going the irrigation at a minimum would NEVER START (that's as good as lost I would say).
				}
	
				// too many status timeout so destroy the message
				nm_destroy_OM( om );
	
			} else {
	
				// We are going to RETRY - so bump the counter.
				om->status_timeouts++;
	
	
				xSemaphoreTake( om->list_om_packets_list_MUTEX, portMAX_DELAY );
	
				// last status request block sent needs to be tried again
				omp = nm_ListGetFirst( &om->om_packets_list );
	
				FoundOneToSend = FALSE;
	
				i = 0;
	
				while( omp != NULL )
				{
					i++;
	
					if( i == om->last_status )
					{
						ucp = omp->dh.dptr;
						tdh = (TPL_DATA_HEADER_TYPE*)ucp;
	
						// set the request for status bit
						tdh->H.PID.request_for_status = 1;
	
						// mark the block to send
						omp->NeedToSend = TRUE;
	
						FoundOneToSend = TRUE;
	
						OutgoingStats.NO_PacketsResent++;
	
						break;  // out of the while loop
					}
	
					omp = nm_ListGetNext( &om->om_packets_list, omp );
				}
	
				if( FoundOneToSend == TRUE )
				{
					// Since we know we are going to send one and we're done massaging the packets calc the CRC for the OM.
					nm_nm_CalcCRCForAllPacketsOfOutgoingMessage( om );
	
					// Now we're ready to let the blocks out the door so notify the task to do so.
					toqs.event = TPL_OUT_QUEUE_COMMAND_send_packets;
					toqs.om = om;
					if( xQueueSend( TPL_OUT_event_queue, &toqs, 0 ) != pdPASS )
					{
						ALERT_MESSAGE_WITH_FILE_NAME( "TPL_OUT queue full!" );
					}
				}
				else
				{
					Alert_Message( "Didn't find OM packet to send" );
				}
	
				xSemaphoreGive( om->list_om_packets_list_MUTEX );
	
			}  // of status timeout count allows another retry
		}  // of this was a 3000 based message class
	}

	xSemaphoreGive( list_tpl_out_messages_MUTEX );

}  // of function

/* ---------------------------------------------------------- */
static void process_om_existence_timer_expired( xTimerHandle pxTimer )
{
	// Remember! This is executed within the context of the timer task.

	OUTGOING_MESSAGE_STRUCT		*om;

	om = (OUTGOING_MESSAGE_STRUCT*)(pvTimerGetTimerID( pxTimer ));

	xSemaphoreTake( list_tpl_out_messages_MUTEX, portMAX_DELAY );

	// The exist timer is only set for RESP type messages. When it goes off there is no body to tell about it just
	// delete the message. The message is hanging around so that the guy we sent the message to can ask for pieces
	// of the message (triggered by the request for status or possibly the IM status timers themselves).

	// check om points to a message on the list
	if( nm_OnList( &OutgoingMessages, om ) == FALSE )
	{
		// check this scenario out we send a message of a size that is so
		// long that the status timer times out (when the status timer is set to 2
		// seconds that size happens to be a program start for 18 stations)
		//
		// what can happen is that the timer can time out and build an deferred event
		// about that - and at about the same time the status block could show up
		// Well when the status block shows up the outgoing message is removed from the
		// list. Then the deferred event finally gets around to firing and guess what
		// the message is gone so it doesn't have to remove it.
		//
		// All pointers involved in this scenario appear to be so well behaved that
		// I could make it so this was not an error. I could also make the message status
		// timeout period a function of the number of blocks being sent.
		//
		// In summary, the status timeout needs to be adjusted based on message length.

		// 3/20/2012 rmd : It has been reported (with the 2000e) that this alert can show during
		// heavy RRe-TRAN usage (I suppose coupled with degraded coverage).
		if ( config_c.debug.transport_om_show_unexpected_behavior_alerts == (true) )
		{
			//ALERT_MESSAGE_WITH_FILE_NAME( "OM not on list" );
			Alert_item_not_on_list( om, OutgoingMessages );
		}
	}
	else
	{
		// this should be considered a failure cause we never received a good status
		OutgoingStats.NO_Failures++;

		// the exist timer has fired!
		nm_destroy_OM( om );
	}

	xSemaphoreGive( list_tpl_out_messages_MUTEX );
}

/* ---------------------------------------------------------- */
/*--------------------------------------------------------------------------
	Name:           here_is_an_incoming_status( DATA_HANDLE dh )

	Description:    Called when by the TPL_OUT_task when it receives an appropriate queue message.

					The packet memory must be released here within this function as it was acquired
					by the packet hunter and then reference to it passed on the queue. After we have
					worked with the packet here we can FREE it's associated memory.

	Parameters:     pdh points to the status packet itself - no ambles.

	Return Values:  None

 -------------------------------------------------------------------------*/
static void __here_is_an_incoming_status( DATA_HANDLE pdh )
{
	OUTGOING_MESSAGE_STRUCT		*om;

	OUTGOING_MESSAGE_PACKET		*omp;

	TPL_DATA_HEADER_TYPE		*tdh;   // "transport data header" pointer
	TPL_STATUS_HEADER_TYPE		*tsh;   // "transport status header" pointer

	unsigned char				*abp, *ucp;       // "ack byte pointer" and an "unsigned char pointer"

	unsigned short				HighestToResend, i, mi;

	TPL_OUT_EVENT_QUEUE_STRUCT		toqs;

	// Packet is delivered to this function WITHOUT the ambles. They are stripped during the ring buffer packet detection
	// process.

	// Take a look at the status header if it indicates message is complete we can call the timer expired routine to destroy the
	// outgoing message.

	// Find the outgoing message whose mid matches that of this status block.

	ucp = pdh.dptr;

	tsh = (TPL_STATUS_HEADER_TYPE*)pdh.dptr;

	xSemaphoreTake( list_tpl_out_messages_MUTEX, portMAX_DELAY );

	om = nm_ListGetFirst( &OutgoingMessages );

	while( om != NULL )
	{
		if ( om->mid.S == tsh->H.MID.S ) break;

		om = nm_ListGetNext( &OutgoingMessages, om );
	}

	if( om == NULL )
	{
		// Be on the look out for this error. And try to note here what situations cause it to occur.

		// ----------

		UNS_32	serial_number;
	
		char	str_4[ 4 ];

		// 10/11/2013 rmd : et the 4-byte serial number to 0 in order to 0 all 4 bytes. Build up a
		// little endian version of the 3-byte TO address. Then copy the 3 bytes in little endian
		// format to the serial number. The result is a little endian UNS_32 serial number for the
		// alert lines.
		serial_number = 0;

		str_4[ 0 ] = tsh->H.FromTo.from[ 2 ];
		str_4[ 1 ] = tsh->H.FromTo.from[ 1 ];
		str_4[ 2 ] = tsh->H.FromTo.from[ 0 ];
		
		memcpy( &serial_number, str_4, 3 );
		
		Alert_Message_va( "TPL_OUT: no OM for incoming status found, from sn=%u", serial_number );
		
		// ----------

		OutgoingStats.NO_UnexpectedStatus++;
	}
	else
	{
		// So the status packet is for an OM - stop the status timer NOW else it is possible for it to expire and create a timer
		// expired event to process after we either have destroyed the message or already processed the status and marked blocks for
		// re-send.
		xTimerStop( om->timer_waiting_for_status_resp, portMAX_DELAY );

		if( tsh->H.AcksLength == 0 )
		{
			// This means message is complete - may destroy.

			// successful message
			OutgoingStats.NO_Successes++;

			nm_destroy_OM( om );
		}
		else
		{
			// Resending a block because of the status is saying to do so.

			if( config_c.debug.transport_om_show_status_timer_expired_alert == 1 )
			{
				// ----------
		
				UNS_32	serial_number;
			
				char	str_4[ 4 ];
		
				// 10/11/2013 rmd : et the 4-byte serial number to 0 in order to 0 all 4 bytes. Build up a
				// little endian version of the 3-byte TO address. Then copy the 3 bytes in little endian
				// format to the serial number. The result is a little endian UNS_32 serial number for the
				// alert lines.
				serial_number = 0;
		
				str_4[ 0 ] = om->from_to.to[ 2 ];
				str_4[ 1 ] = om->from_to.to[ 1 ];
				str_4[ 2 ] = om->from_to.to[ 0 ];
				
				memcpy( &serial_number, str_4, 3 );
				
				Alert_Message_va( "TPL_OUT: status packet rqsting resend : msg_class=%s, to ser num=%u", cs3000_msg_class_text[ om->msg_class.rclass ], serial_number );
			}

			// received an incomplete message status packet
			OutgoingStats.NO_IncompleteStatusRcvd++;

			// This is our indication to the packet send routine that these are resent due to a recieved status
			// When they are sent they must go on a list and go out with the status's (when the token arrives).
			om->status_resend_count++;

			// so we've got a status packet with some bits set and some not set

			xSemaphoreTake( om->list_om_packets_list_MUTEX, portMAX_DELAY );

			// get pointed to the first packet of the outgoing message
			omp = nm_ListGetFirst( &om->om_packets_list );

			if( omp == NULL )
			{
				ALERT_MESSAGE_WITH_FILE_NAME( "OM has no blocks" );
			}
			else
			{
				// get pointed to the first byte of the ack bytes
				abp = ucp + sizeof( TPL_STATUS_HEADER_TYPE );

				//DFP( 400, "first ack byte is %02x", *ap );
				//display_comm_stats();
				//WaitTillKey();
				//DFP( 400, (char*)BlankLine );

				i = 0;

				HighestToResend = 0;

				while( omp != NULL )
				{
					i++;

					// for reference - the masks definition
					// const unsigned char _Masks[8] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};
					//
					// for each block get the mask index - here's a table of what we need
					//
					//   i  i%8   mi
					// --------------
					//   1   1    0
					//   2   2    1
					//   3   3    2
					//   4   4    3
					//   5   5    4
					//   6   6    5
					//   7   7    6
					//   8   0    7
					//   9   1    0
					//  10   2    1
					//  11   3    2
					//  12   4    3
					//  13   5    4
					//  14   6    5
					//  15   7    6
					//  16   0    7
					//  17   1    0
					//   .   .
					//   .   .
					//   .   .
					//
					if ( ( i % 8)==0 ) mi=7; else mi = (i % 8) - 1;


					//DFP( 400, "testing bit %02d in %02x", i, *ap );
					//WaitTillKey();


					// If the bit is not there the block needs to be sent.
					if ( ( *abp & _Masks[mi] ) == 0 ) {

						//DFP( 320, "block %02d missing - gonna resend", i );
						//WaitTillKey();

						// block needs to be retransmitted
						omp->NeedToSend = TRUE;

						// keep track of the highest so we know which block to set the
						// send status bit in
						//
						HighestToResend = i;

						// 607 ajv
						//
						// Removed based upon feedback from customers and Field Reps.  In situations
						// where communications was struggling significantly, this alert could be
						// stamped for every packet that failed, rolling out other alerts which may
						// have been more important to the user.
						//
						/*
						if ( ConfigTable.Items.Debug.b.enable_status_timer_expired_alert == 1 ) {

							sprintf( str_64, "packet # %d ", i );
							Alert_Message( str_64 );

						}
						*/

						OutgoingStats.NO_PacketsResent++;

					}

					omp = nm_ListGetNext( &om->om_packets_list, omp );

					// get looking at the next ack byte when time to
					if( mi == 7 ) abp++;

				}

				// now set the request for status bit in the highest block to resend
				if( HighestToResend > 0 )
				{
					omp = nm_ListGetFirst( &om->om_packets_list );

					i=0;

					while( omp != NULL ) {

						i++;

						if( i == HighestToResend ) {

							// get tdh pointed to the header of the packet
							tdh = (TPL_DATA_HEADER_TYPE*)(omp->dh.dptr);

							// integrity check
							if( tdh->H.ThisBlock != i )
							{
								Alert_Message( "OM alignment off" );
							}

							tdh->H.PID.request_for_status = 1;  // set the status request bit

							break;  // from the while loop
						}

						omp = nm_ListGetNext( &om->om_packets_list, omp );
					}

					nm_nm_CalcCRCForAllPacketsOfOutgoingMessage( om );

					// now we're ready to let the blocks out the door so signal the task
					toqs.event = TPL_OUT_QUEUE_COMMAND_send_packets;

					toqs.om = om;

					if( xQueueSend( TPL_OUT_event_queue, &toqs, 0 ) != pdPASS )
					{
						ALERT_MESSAGE_WITH_FILE_NAME( "TPL_OUT queue full" );
					}
				}
				else
				{
					// The status told us in a round about way that all the blocks are there (something is not right for a status packet to be
					// this way but...) so destroy the OM.
					//
					// But give back the list MUTEX first!  And then take it back when done. So the Takes and Gives match within this function.
					xSemaphoreGive( om->list_om_packets_list_MUTEX );

					nm_destroy_OM( om );

					xSemaphoreTake( om->list_om_packets_list_MUTEX, portMAX_DELAY );

					ALERT_MESSAGE_WITH_FILE_NAME( "Abnormal OM STATUS rcvd" );  // In all my years I have NEVER seen this.
				}

			}  // of if the OM has no packets on the packets list

			xSemaphoreGive( om->list_om_packets_list_MUTEX );

		}  // of if we received an incomplete status

	}

	xSemaphoreGive( list_tpl_out_messages_MUTEX );
}

/* ---------------------------------------------------------- */
static void __check_a_message_for_outgoing_packets_to_send( OUTGOING_MESSAGE_STRUCT *ptr_outgoing_message )
{
	OUTGOING_MESSAGE_PACKET	*omp;

	TPL_DATA_HEADER_TYPE	*tdh;

	// 2011.07.27 rmd We ONLY deal with one message at a time here ... NOT the whole list of messages. This function is invoked
	// on demand and not on a periodic basis.

	xSemaphoreTake( list_tpl_out_messages_MUTEX, portMAX_DELAY );

	// check p_om is on the list
	if( nm_OnList( &OutgoingMessages, ptr_outgoing_message ) == (false) )
	{
		//ALERT_MESSAGE_WITH_FILE_NAME( "OM not on list" );
		Alert_item_not_on_list( ptr_outgoing_message, OutgoingMessages );
	}
	else
	{
		xSemaphoreTake( ptr_outgoing_message->list_om_packets_list_MUTEX, portMAX_DELAY );

		// loop on the packets for this message
		omp = nm_ListGetFirst( &(ptr_outgoing_message->om_packets_list) );

		while( omp != NULL )
		{
			if( omp->NeedToSend == TRUE )
			{
				// Whatever this uses to "send" the data it must take a copy of the data and leave the
				// original message intact.

				// Since the message FROM_CENTRAL generally arrives asynchronously to when we receive our
				// token the TO_CENTRAL response packets MUST be put on a list of packet to send when the
				// token arrives. Unless the port the message came in on cannot possibly have any FLOWSENSE
				// traffic on it. Examples of such would be the USB port.

				// SCAN, SCAN_RESP, TOKEN, and TOKEN_RESP packets should always be sent right away. For the
				// SCAN and TOKEN messages this is pretty clear. The A controller is in change and he
				// generated these messages. And he wants them sent now. The same train of thought is
				// extended to the SCAN_RESP and TOKEN_RESP messages. The master sent them and is awaiting
				// the reponse so send them packets NOW!

				// Resends follow the same rules about if the packets can be sent now or have to wait
				// for the next token to us. Two ways resends can occur. FIRST is an OM's status timer
				// elapsed and we want to send the message again...this can ONLY happen for messages that
				// originate at the FOAL machine and SCAN machine (CommMngr)...we dont set the timer for any
				// other originating messages...when that timer fires it okay to resend those packets
				// immediately cause remember it is the FOAL machine who is in charge of the bus and if he
				// is trying to get a message through he wants his packets now. SECOND is a status comes in
				// that says packets are missing...please send them. Again we just follow the rules by
				// packet type.

				// In the controllers the only OM's that trigger their own resend (status timer enabled) are
				// the SCAN and TOKEN messages. All other messages must receive a status packet that causes
				// the resend.

				// 2011.07.27 rmd  We would like a reasonably precise indication of when a packet with a
				// request for status ACTUALLY goes out the door. In the case of long 10+ packet messages
				// the serial driver may take quite some time to actually send all those bytes. Especially
				// if CTS starts doing it's thing. The OM wants to start a timer so it can decide when to
				// ask again for a status. This is the means by which the message "FORCES" itself upon the
				// receiver. If we set the timer too early we can deceive ourselves and report errors that
				// aren't.
				void	*serial_driver_should_signal_us_to_start_timer;

				// Take a look at the status request to decide if the serial driver should signal us.
				tdh = (TPL_DATA_HEADER_TYPE*)(omp->dh.dptr);

				serial_driver_should_signal_us_to_start_timer = NULL;  // the default is NO!

				if ( tdh->H.PID.request_for_status == 1 )
				{
					// only start the timer for messages that are supposed to be active i.e. ONLY start the timer if there is a TIME set
					if ( ptr_outgoing_message->seconds_to_wait_for_status_resp > 0 )
					{
						// When the serial driver completes this packet it will post a message to our queue.
						serial_driver_should_signal_us_to_start_timer = ptr_outgoing_message;
					}
				}

				// ----------
				
				if( ptr_outgoing_message->msg_class.routing_class_base == ROUTING_CLASS_IS_2000_BASED )
				{
					if( (ptr_outgoing_message->msg_class.rclass == MSG_CLASS_2000e_TEST_PACKET_FROM_C_OR_C) || (ptr_outgoing_message->msg_class.rclass == MSG_CLASS_2000e_TEST_PACKET_ECHO_TO_C_OR_C) )
					{
						// 11/27/2017 rmd : For the 2000e comm_test packets send 'em right away to the specified
						// port.
						//Alert_Message_va( "test packet to 2000e %u bytes to %c%c%c", omp->dh.dlen, tdh->H.FromTo.to[0], tdh->H.FromTo.to[1], tdh->H.FromTo.to[2] );

						Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy( ptr_outgoing_message->port, omp->dh, NULL );
					}
					else
					{
						Alert_Message( "TPL_OUT: unexp 2000e msg class" );
					}
				}
				else
				{
					// 11/28/2017 rmd : This is assumed to be a 3000 based message.

					switch( ptr_outgoing_message->msg_class.rclass )
					{
						case MSG_CLASS_CS3000_SCAN:
						case MSG_CLASS_CS3000_TOKEN:
							// 3/15/2012 rmd : Implement the SCATTER behavior for both the SCAN packet and TOKEN
							// packets. As we are the master and we've decided to build and send this message NOW, send
							// the packets right away.
							//
							// 3/15/2012 rmd : If you look at the serial_driver_should_signal_us_to_start_timer scheme
							// you see for the case of SCATTER messages starting the timer is not so clean. And even for
							// the case of sending to the M ports. Cause the serial driver is actually only sending the
							// packets through UPORT_TP. And we really do not know when the packet with the status
							// request goes out the door. So we have the situation where we'd like to know who is the
							// slowest driver to send out that particular packet. And the last guy to send it should be
							// the one to start the timer. That could be implemented. However I think I'll wait to see.
							// It may not be a problem as is. The current scheme only falls apart for the SCATTER
							// message packets. And likely will work with this defect!
	
							// ----------
	
							// 3/15/2012 rmd : For each driver to signal this message to start the timer (and could
							// happen multiple times due to scatter) the timer resets its time out and starts the tiemr
							// again. And this is okay. As long as it didn't expire before all drivers sent the status
							// request packet the last one to send the status rqst packet is the one who finally sets
							// the timer. And that would be the desired operation!
							PACKET_ROUTER_broadcast_SCAN_TOKEN_CODE_packet_to_other_controllers_on_our_chain( omp->dh, serial_driver_should_signal_us_to_start_timer );
							
							break;
	
						case MSG_CLASS_CS3000_SCAN_RESP:
							if( ptr_outgoing_message->status_resend_count == 0 )
							{
								Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy( ptr_outgoing_message->port, omp->dh, serial_driver_should_signal_us_to_start_timer );
							}
							else
							{
								// These are UNEXPECTED cause we don't do retries on the scan packets ... what to do? flag as an error
								Alert_Message( "UNXP resend: scan packet" );
							}
							break;
	
	
						case MSG_CLASS_CS3000_TOKEN_RESP:
							// Picture a slave controller on a chain. That has received a scan message. It's response
							// ONLY needs to go back out the port it came in. So that's what we'll do. And it is sent
							// right away as the bus is ours.
							Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy( ptr_outgoing_message->port, omp->dh, serial_driver_should_signal_us_to_start_timer );
							break;
	
						default:
							ALERT_MESSAGE_WITH_FILE_NAME( "OM UNK Message Class" );
							break;
	
					}
				}


				// one got sent
				OutgoingStats.NO_PacketsSent++;


				// If the status request we need to 1) reset the bit 2) capture the block number for timer retries that may be necessary.
				if( tdh->H.PID.request_for_status == 1 )
				{
					// Set the request bit for status OFF - this will invalidate the CRC but we're done sending unless a status response or
					// status timer tells us to resend in which case we will mark the Status Request bit and recalc the CRC at that time.
					tdh->H.PID.request_for_status = 0;

					// Capture the block number of the last packet sent with the status request bit set.
					ptr_outgoing_message->last_status = tdh->H.ThisBlock;
				}


				// we've sent it so reset the to send flag
				omp->NeedToSend = FALSE;

			}  // of if this packet needs to be sent


			// look at next packet in message
			omp = nm_ListGetNext( &ptr_outgoing_message->om_packets_list, omp );

		}  // of while looping on packets of the message AND packet_sent is TRUE


		xSemaphoreGive( ptr_outgoing_message->list_om_packets_list_MUTEX );

		// ----------
		
		// 11/27/2017 rmd : These test messages are a fire one time and forget affair, with no
		// status coming back. So being that we are done with it after a single transmission free
		// all assets and delete it.
		if( ptr_outgoing_message->msg_class.routing_class_base == ROUTING_CLASS_IS_2000_BASED )
		{
			if( (ptr_outgoing_message->msg_class.rclass == MSG_CLASS_2000e_TEST_PACKET_FROM_C_OR_C) || (ptr_outgoing_message->msg_class.rclass == MSG_CLASS_2000e_TEST_PACKET_ECHO_TO_C_OR_C) )
			{
				nm_destroy_OM( ptr_outgoing_message );
			}
		}
	}

	xSemaphoreGive( list_tpl_out_messages_MUTEX );
}

/* ---------------------------------------------------------- */
static void build_outgoing_message( UPORTS pport, DATA_HANDLE pdh, ADDR_TYPE pFromTo, ROUTING_CLASS_DETAILS_STRUCT pmsg_class )
{

	unsigned char				*ucp;

	OUTGOING_MESSAGE_STRUCT		*om;

	OUTGOING_MESSAGE_PACKET		*omp;

	TPL_DATA_HEADER_TYPE		*tdh;

	unsigned short				dlen, packet_size, TotalBlocks, ThisBlock;

	BOOL_32						l_receiving_party_should_make_active;

	unsigned short				l_max_packet_size;

	xSemaphoreTake( list_tpl_out_messages_MUTEX, portMAX_DELAY );

	// new message to send
	OutgoingStats.NO_Messages++;

	// We don't want to allow more than 10 outgoing messages to exist at any given time.
	// Our scheme for destroying OM's leaves the failed RESP messages (resp messages that
	// never saw their status come back) on the list for the exist time. So actually what
	// could most likely end up on the list would be RESP messages since ORIGIN messages
	// do a retry and timeout on their own.
	//
	// Start with the oldest first ... that would be the first message on the list
	while( OutgoingMessages.count >= 10 )
	{
		// destroy the OLDEST on the list
		nm_destroy_OM( nm_ListGetFirst( &OutgoingMessages ) );
	}

	// Get the memory for the new message now but don't add to the list till
	// we're done. In a multi-tasking environment we don't want a partially built
	// message on the list.
	om = mem_malloc( sizeof( OUTGOING_MESSAGE_STRUCT ) );

	// ----------
	
	// 3/21/2013 rmd : The TO FROM addresses up to this point are in little endian format. Our
	// convention (for compatibility with Command Center) is that addresses within packets are
	// big endian format. So we must swap them around. The end result is the middle byte stays
	// and the first and lst bytes trade places.
	om->from_to.from[ 0 ] = pFromTo.from[ 2 ];
	om->from_to.from[ 1 ] = pFromTo.from[ 1 ];
	om->from_to.from[ 2 ] = pFromTo.from[ 0 ];

	om->from_to.to[ 0 ] = pFromTo.to[ 2 ];
	om->from_to.to[ 1 ] = pFromTo.to[ 1 ];
	om->from_to.to[ 2 ] = pFromTo.to[ 0 ];

	// ----------
	
	om->port = pport;

	om->status_timeouts = 0;  // this is how many retries have been tried so far - of course none at this point

	om->status_resend_count = 0;  // this is how many retries have been tried so far - of course none at this point

	om->msg_class = pmsg_class;  // capture the message class for use during the messages lifespan


	om->list_om_packets_list_MUTEX = xSemaphoreCreateMutex();  // DON'T forget to create the MUTEX!


	// Create the existence timer!
	om->timer_exist = xTimerCreate( (const signed char*)"OM exist timer", MS_to_TICKS(config_c.OM_Minutes_To_Exist * 60 * 1000), FALSE, (void*)om, process_om_existence_timer_expired );

	if( om->timer_exist == NULL )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "Timer NOT CREATED" );
	}

	// I've decided it better to always make the timer. Even though for most messages this timer is not started. We
	// always start it with the xTimerChangePeriod function call. When we delete an OM we can always delete the
	// timer. Cause it ALWAYS exists. Make it with just 30 seconds. Can't use a period of 0 as it will not be made.
	om->timer_waiting_for_status_resp = xTimerCreate( (const signed char*)"OM status resp timer", MS_to_TICKS(30 * 1000), FALSE, (void*)om, process_waiting_for_status_resp_timer_expired );

	if( om->timer_waiting_for_status_resp == NULL )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "Timer NOT CREATED" );
	}

	// based on the message CLASS set the following
	//		> allowable_timeouts
	//		> seconds_to_wait_for_status
	//		> receiving_party_should_make_active
	if( om->msg_class.routing_class_base == ROUTING_CLASS_IS_2000_BASED )
	{
		if( (om->msg_class.rclass == MSG_CLASS_2000e_TEST_PACKET_FROM_C_OR_C) || (om->msg_class.rclass == MSG_CLASS_2000e_TEST_PACKET_ECHO_TO_C_OR_C) )
		{
			// 11/27/2017 rmd : For the case of a 2000e test packet response to a 2000e the message is
			// completely passive, no retries or status expected. After sending message should be
			// destroyed.
			l_receiving_party_should_make_active = FALSE;
						
			om->allowable_timeouts = 0;

			om->seconds_to_wait_for_status_resp = 0;
		}
		else
		{
			Alert_Message( "TPLOUT unexp 2000 class" );
		}
	}
	else
	{
		// 11/28/2017 rmd : Then it must be a 3000 based class.
		
		switch ( om->msg_class.rclass )
		{
			case MSG_CLASS_CS3000_SCAN:
				// The scan is a passive-on-both-ends affair.
				l_receiving_party_should_make_active = FALSE;
	
				// We don't want to prolong scan and if communciations is that bad that we need a retry I
				// don't want to play foal anyway.
				om->allowable_timeouts = 0;  // no re-tries during scan
	
				// In order to keep the scan tight and sweet we need to use a short timeout number. This
				// should be the longest it takes for the ping answer to come back. Lets use 3 seconds to
				// cover radio applications (hardwire can be set to as low as 1 and it works) watch out 3
				// may be tight for some radio applications (multiple repeators perhaps - AJ needs to test)
				//
				// For the SCAN_OR_PING class having the timeout inconjunction with setting 0 timeouts has
				// the effect of when the status doesn't come back within 3 seconds a message is passed up
				// to the transport about the failure
	
				// 6/30/2015 rmd : Even the scan doesn't receive a status back from the recepient.
				om->seconds_to_wait_for_status_resp = 0;
				//om->seconds_to_wait_for_status_resp = OM_SECONDS_TO_WAIT_FOR_STATUS_DURING_SCAN_OR_PING;
	
				break;
	
			case MSG_CLASS_CS3000_SCAN_RESP:
				// The SCAN response is passive all the way around with no timer and no retries
				l_receiving_party_should_make_active = FALSE;
				om->allowable_timeouts = 0;
				om->seconds_to_wait_for_status_resp = 0;
				break;
	
	
			case MSG_CLASS_CS3000_TOKEN:
				// The outgoing TOKEN message is an active affair, the OM is active and has retries. The
				// receiving party is passive. If the transport unsuccesfully exhausts its retries a message
				// is genreated to the COMM_MNGR - in order to report the error and allow the FOAL master to
				// generate a token for the next controller.
				l_receiving_party_should_make_active = FALSE;
	
				om->allowable_timeouts = 0;
	
				om->seconds_to_wait_for_status_resp = 0;
	
				/*
				om->allowable_timeouts = config_c.OM_Originator_Retries;  // the default is 1
	
				om->seconds_to_wait_for_status_resp = config_c.OM_Seconds_for_Status_FOAL;  // the default is 25 seconds
				*/
				break;
	
			case MSG_CLASS_CS3000_TOKEN_RESP:
				// 7/18/2013 rmd : The message generated by the IRRI machine is passive - the receiving
				// party needs to make active though! - the receiving party is the FOAL master for this
				// message type.
				l_receiving_party_should_make_active = (false);
	
				om->allowable_timeouts = 0;
	
				om->seconds_to_wait_for_status_resp = 0;
	
				break;
	
	
			// NOTE: if	we add a CLASS that generates active OM's you should look at including that msg
			//       type in the function OM_DestroyAll_MESSAGE_TYPE_ORIGIN_MESSAGES()
	
			default:
				// should NEVER get this
				Alert_Message( "OM class unknown" );
	
				// well in the case wipeout let the message go without either side active
				l_receiving_party_should_make_active = FALSE;
				om->allowable_timeouts = 0;
				om->seconds_to_wait_for_status_resp = 0;
				break;
		}
	}

	// ----------
	
	next_mid += 1;
	
	om->mid.S = next_mid;
	
	// ----------
	
	l_max_packet_size = TPL_USER_DATA_MAX_SIZE;
	
	// Get total blocks so we can set this in each packet of the message.
	TotalBlocks = ( pdh.dlen / l_max_packet_size );

	// And test for any remainder.
	if ( ( pdh.dlen % l_max_packet_size ) != 0 )
	{
		TotalBlocks += 1;
	}

	ThisBlock = 0;

	// ----------
	
	xSemaphoreTake( om->list_om_packets_list_MUTEX, portMAX_DELAY );

	// Initialize the message packet list.
	nm_ListInit( &om->om_packets_list, offsetof( OUTGOING_MESSAGE_PACKET, dl )  );

	while( pdh.dlen > 0 )
	{
		// STEP 1: Calculate a few useful numbers

		// dlen is how much of the user data goes on this packet
		if( pdh.dlen > l_max_packet_size ) dlen = l_max_packet_size; else dlen = pdh.dlen;

		// set how much left (after this packet) NOW for use in request for status bit setting below
		pdh.dlen -= dlen;

		// The ambles are ALWAYS added when the packet is sent. So this is the entire PACKET payload. That is we build the packet
		// minus the pre and post ambles.
		packet_size = sizeof( TPL_DATA_HEADER_TYPE ) +
					  dlen +
					  sizeof( CRC_TYPE );


		// STEP 2: allocate memory for the actual packet data
		ucp = mem_malloc( packet_size );


		// STEP 3: allocate space for and add the list item corresponding to the packet
		omp = mem_malloc( sizeof( OUTGOING_MESSAGE_PACKET ) );

		omp->dh.dlen = packet_size;
		omp->dh.dptr = ucp;
		omp->NeedToSend = TRUE;   // mark all packets to be sent


		// STEP 4: fill out the packet itself
		tdh = (TPL_DATA_HEADER_TYPE*)ucp;


		tdh->B[ 0 ] = 0;			// set whole PID byte to 0
		tdh->H.PID.STATUS = 0;		// this is data not status

		// if there is more data remaining (for more packets in the message) dont make a request for status
		if( pdh.dlen > 0 ) tdh->H.PID.request_for_status = 0; else tdh->H.PID.request_for_status = 1;


		// should the receiving party make this an active incoming message -
		if( l_receiving_party_should_make_active == (true) )
		{
			tdh->H.PID.make_this_IM_active = 1;
		}
		else
		{
			tdh->H.PID.make_this_IM_active = 0;
		}
		
		// ----------

		if( om->msg_class.routing_class_base == ROUTING_CLASS_IS_2000_BASED )
		{
			// 11/27/2017 rmd : For these special 2000e packets that run through here avoid the 3000
			// packet class scheme. These are the only 2000e packets that pass through this transport.
			if( (om->msg_class.rclass == MSG_CLASS_2000e_TEST_PACKET_FROM_C_OR_C) || (om->msg_class.rclass == MSG_CLASS_2000e_TEST_PACKET_ECHO_TO_C_OR_C) )
			{
				tdh->H.PID.__routing_class = om->msg_class.rclass;
				
				if( om->msg_class.rclass == MSG_CLASS_2000e_TEST_PACKET_ECHO_TO_C_OR_C )
				{
					// 11/27/2017 rmd : For the case of the 2000e, when echoing the received test message, we
					// took a copy of the to-from address as it appeared in the received message. It's byte
					// ordering should be correct, but we do need to reverse the to and the from so it is
					// addressed to who it came from. BUT using the passed in paramter, not only do we need to
					// reverse the from to, we HAVE to do the LITTLE ENDIAN swap too.
					tdh->H.FromTo.from[0] = pFromTo.to[2];
					tdh->H.FromTo.from[1] = pFromTo.to[1];
					tdh->H.FromTo.from[2] = pFromTo.to[0];
					tdh->H.FromTo.to[0] = pFromTo.from[2];
					tdh->H.FromTo.to[1] = pFromTo.from[1];
					tdh->H.FromTo.to[2] = pFromTo.from[0];
				}
				else
				{
					// 12/11/2017 rmd : As the originator all we have to do is swap the ENDIANS, don't need to
					// swap the to-from.
					tdh->H.FromTo.from[0] = pFromTo.from[2];
					tdh->H.FromTo.from[1] = pFromTo.from[1];
					tdh->H.FromTo.from[2] = pFromTo.from[0];
					tdh->H.FromTo.to[0] = pFromTo.to[2];
					tdh->H.FromTo.to[1] = pFromTo.to[1];
					tdh->H.FromTo.to[2] = pFromTo.to[0];
				}
			}
		}
		else
		{
			// 11/29/2017 rmd : Then it is a 3000 based class.

			// 7/18/2013 rmd : Using the om message class set the class for each packet.
			set_this_packets_CS3000_message_class( tdh, om->msg_class.rclass );

			// Because the ARM chip we are using is little endian, we need to swap the bytes in the
			// address to comply with our legacy transport which is big endian.  The middle byte stays
			// the same, the 1st and last byte need to swap though
			tdh->H.FromTo.from[0] = pFromTo.from[2];
			tdh->H.FromTo.from[1] = pFromTo.from[1];
			tdh->H.FromTo.from[2] = pFromTo.from[0];
			tdh->H.FromTo.to[0] = pFromTo.to[2];
			tdh->H.FromTo.to[1] = pFromTo.to[1];
			tdh->H.FromTo.to[2] = pFromTo.to[0];
		}
		
		// ----------
		
		// 7/18/2013 rmd : Safer to use memcpy since we are dealing with packed structures within
		// other structures. We may see a data abort error.
		memcpy( &tdh->H.MID, &om->mid, sizeof(UNS_16) );
		
		// ----------

		tdh->H.TotalBlocks = TotalBlocks;
		tdh->H.ThisBlock = ++ThisBlock;
		
		// ----------

		// initialize the last_status to the highest block by setting it equal to current
		// block number for each block made. Should intialize in case it is tried to be
		// used before the message is actually sent - something that shouldn't happen but
		// just in case its initialized.
		om->last_status = tdh->H.ThisBlock;


		// push ucp up to where user data is to be inserted
		ucp += sizeof( TPL_DATA_HEADER_TYPE );

		memcpy( ucp, pdh.dptr, dlen );

		// push dh.dptr to where we pull data from next
		pdh.dptr += dlen;

		// point to where the CRC must go
		ucp += dlen;

		ucp += 4;  // jump over CRC spot

		// STEP 5: add the packet to the list of packets for this message
		nm_ListInsertTail( &om->om_packets_list, omp );
	}

	nm_nm_CalcCRCForAllPacketsOfOutgoingMessage( om );


	// Add it to the list of messages.
	nm_ListInsertTail( &OutgoingMessages, om );


	// Start the exist timer!
	//
	// 3/14/2014 rmd : Being that the timer task is a high priority task using portMAX_DELAY
	// when posting is safe. Anything on the timer queue is quickly processed and therefore the
	// queue is effectively always empty.
	xTimerStart( om->timer_exist, portMAX_DELAY );


	xSemaphoreGive( om->list_om_packets_list_MUTEX );

	xSemaphoreGive( list_tpl_out_messages_MUTEX );


	// It's done and we know all packets need to be xmitted.
	__check_a_message_for_outgoing_packets_to_send( om );
}

/* ---------------------------------------------------------- */
void TPL_OUT_task( void *pvParameters )
{
	TPL_OUT_EVENT_QUEUE_STRUCT	toqs;

	BOOL_32		random_number_generator_seeded;
	
	// ----------
	
	// 10/23/2012 rmd : To support the WDT activity monitor.
	UNS_32	task_index;
	
	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// ----------

	random_number_generator_seeded = (false);
	
	// ----------
	
	// the TPL_OUT_event_queue is created in startup.c

	memset( &OutgoingStats, 0, sizeof( OutgoingStats )	);

	// initialize the OUT going list of messages
	xSemaphoreTake( list_tpl_out_messages_MUTEX, portMAX_DELAY );

	nm_ListInit( &OutgoingMessages, offsetof( OUTGOING_MESSAGE_STRUCT, dl ) );

	xSemaphoreGive( list_tpl_out_messages_MUTEX );


	while( (true) )
	{
		// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
		if( xQueueReceive( TPL_OUT_event_queue, &toqs, MS_to_TICKS( 500 ) ) == pdTRUE )
		{
			switch( toqs.event )
			{
				case TPL_OUT_QUEUE_COMMAND_here_is_an_outgoing_message:

					if( !random_number_generator_seeded )
					{
						// 10/28/2014 rmd : Seed the random number generator and intialize the next_mid.
						srand( xTaskGetTickCount() );
						
						next_mid = rand();
						
						random_number_generator_seeded = (true);
					}
					
					// ----------
					
					build_outgoing_message( toqs.port, toqs.dh, toqs.from_to, toqs.msg_class );
					
					// 7/25/2013 rmd : So now finally we can release the message memory that another process
					// malloc'ed when building this message originally.
					mem_free( toqs.dh.dptr );

					break;

				
				case TPL_OUT_QUEUE_COMMAND_send_packets:

					__check_a_message_for_outgoing_packets_to_send( toqs.om );

					break;

				
				case TPL_OUT_QUEUE_COMMAND_here_is_an_incoming_status_packet:

					__here_is_an_incoming_status( toqs.dh );
					// And now release the memory the ring buffer packet hunter allocated when it found the
					// packet.
					mem_free( toqs.dh.dptr );

					break;

				
				case TPL_OUT_QUEUE_COMMAND_start_waiting_for_status_resp_timer:

					// This message actually comes from the SERIAL DRIVER. Indicating it just finsihed transmitting a
					// packet with its rqst for status bit set. The driver isn't actually looking for this. But the
					// request to send us a message is passed to the driver with the block.
					__start_waiting_for_status_resp_timer( toqs.om );

					break;

			}

		}  // of a sucessful queue item is available

		// ----------
		
		// 10/23/2012 rmd : Keep the activity indication going.
		task_last_execution_stamp[ task_index ] = xTaskGetTickCount();

		// ----------

	}  // of forever task loop
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

