/*  file = tpl_in.h                           2011.07.25 rmd  */

/* ---------------------------------------------------------- */

#ifndef _INC_TPL_IN_H
#define _INC_TPL_IN_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"lpc_types.h"

#include	"packet_definitions.h"

#include	"serial.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern const unsigned char _Masks[8];


// The following extern and typedef definitions are currently here so that we can display statistics about the tpl.

extern MIST_LIST_HDR_TYPE  IncomingMessages;


// These are the elements on the incoming messages list each element represents a new MID (message ID) these are not the
// packets themselves - they reside on the list MessageHdr points to.
typedef struct
{
	MIST_DLINK_TYPE     dl;					// for list control of this list

	MID_TYPE            MID;				// ID of message this element represents

	ADDR_TYPE           from_to;			// to / from address

	UPORTS				port;				// port the message came in on

	unsigned char       TotalBlocks;		// total blocks in this message

	BOOL_32				SentToApp;			// so we only send it once

	MIST_LIST_HDR_TYPE	im_packets_list;	// This is the linked list of the actual incoming packets.
	
	xSemaphoreHandle	list_im_packets_list_MUTEX;
	
	// the timer which controls how long this IM will exist. Incoming messages exist as
	// long as this timer - and they are passive (that is they don't request retries to
	// make themselves complete). Except for messages recieved from the IRRI MACHINE (another
	// way to say this is except for FOAL messages received by the foal master). These kind
	// of incoming messages do ask for retries if they aren't complete. They go passive when
	// a complete message has been received.
	//
	// this is NOT a pointer to the timer structure it is the structure itself - otherwise we'd
	// have to allocate the memory for the structure anyway so might as well get it now when we make
	// the new incoming message
	xTimerHandle		timer_existence;
	
	// There are 2 more timers associated with incoming messages. They are only active for
	// messages TO the FOAL MASTER MACHINE ( 33A ) or the COMM MNGR of the master ( 11A ). The
	// first timer is a timer looking for the status request packet. It is stopped each time
	// a packet comes in and is set if that packet is not a status packet. The second timer
	// is also stopped each time a packet comes in and is set when sending a status that
	// indicates an incomplete incoming message ie packets are missing.
	xTimerHandle		timer_waiting_for_status_rqst;

	xTimerHandle		timer_waiting_for_response_to_status;


	unsigned short		timer_timeouts;


	// a convenience variable to help us decide to start the status request timers or not
	// this is set with info derived from the header of any packet and should remain fixed
	// throughout all incoming packets for a given message ID
	//
	// currently used by the RRe only but I think it is an improvement over the current scheme which
	// uses addresses to make the decision if an IM is active or not
	//
	// now integrated throughout the 500 / 600 / RRe / CC4 environment ... with regards to CC4 this change did
	// not result in any changes to the CC4 code because all the OM messages it sends which are IM messages to
	// us are passive i.e. the bit is NOT set and that was the way CC4 set the header already - with the bit of
	// interest set to a 0
	BOOL_32				this_IM_is_active;

	// this can now be extracted from each packet ... it is assigned when the sender of the message
	// makes the OM ... it is available in each packet that comes in
	ROUTING_CLASS_DETAILS_STRUCT	msg_class;

	// 3/16/2012 rmd : With the introduction of the broadcast TOKEN, there are messages incoming
	// to us that aren't addressed to us. The TPL_IN should ONLY send out a STATUS or start the
	// timer waiting for a status rqst if the message is actually to us. The part about not
	// starting the timer I'm not sure about. But I suppose it makes sense. For a broadcast
	// message (TOKEN) why would we start the timer which is waiting for a status? If the status
	// doesn't show we aren't supposed to do anything. As an 'un-addressed' broadcast receipient
	// we are supposed to play 'dead' sort of speak.
	BOOL_32				permission_to_send_a_status;
	
	
} INCOMING_MESSAGE_STRUCT;



// These are the elements of the message itself a translation of one per packet received.
typedef struct {

	MIST_DLINK_TYPE     dl;             // for list control
	unsigned char       BlockNumber;    // which block is this
	DATA_HANDLE         dh;             // pointer to and length of the USER DATA

} INCOMING_MESSAGE_PACKET_STRUCT;



typedef struct {

	unsigned long   IncomingMessages;
	unsigned long   CompleteStatusSent;
	unsigned long   PartialStatusSent;
	unsigned long   MessagesToApplication;
	unsigned long   SecondSubmitRequests;

	unsigned long	existence_timer_expired;
	unsigned long	status_rqst_timer_expired;
	unsigned long	status_response_timer_expired;
	
} INCOMING_STATS;



extern INCOMING_STATS IncomingStats;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#define	TPL_IN_EVENT_here_is_an_incoming_packet				0x0400

#define	TPL_IN_EVENT_the_existence_timer_expired			0x0500


typedef struct
{
	// To identify the event.
	unsigned short				event;
	
	// This is needed when a timer expires so we know what IM message the command is for. Especially when a timer
	// expires.
	INCOMING_MESSAGE_STRUCT		*im;
	
	// This would be used to hand any incoming packet to the list of IM's.
	DATA_HANDLE					dh;

	UPORTS						port;

} TPL_IN_EVENT_QUEUE_STRUCT;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


void TPL_IN_destroy_all_incoming_3000_scan_response_and_token_response_messages( void );


void TPL_IN_task( void *pvParameters );


void TPL_IN_make_a_copy_and_direct_incoming_packet( UPORTS pport, DATA_HANDLE pdh );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

