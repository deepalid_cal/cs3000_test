/*  file = comm_utils.c                      2011.07.25  rmd  */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy
#include	<string.h>

#include	"comm_utils.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"comm_mngr.h"

#include	"configuration_controller.h"

#include	"cs_mem.h"

#include	"flowsense.h"

#include	"tpl_in.h"

#include	"cent_comm.h"




/* ---------------------------------------------------------- */
/**
 * Using the TO address of a scan message, determine whether the packet is
 * destined for this controller or not. Scan messages, unlike tokens, use the
 * scan character, rather than the controller's serial number.
 * 
 * This is ONLY used to determine if an incoming packet of type MSG_CLASS_SCAN
 * is for us using the special scan-encoded addressing.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the COMM_MNGR task when
 * building an outgoing scan message and when an incoming scan message is
 * received.
 * 
 * @param to_addr_ptr A pointer to the transport packet header's to address. It may be
 * either little or big endian, as identified by the pendianess parameter.
 * 
 * @param pendianness Whether the data is big endian or little endian. When
 * processed while building an outgoing message, the value is already little
 * endian since it hasn't gone through the transport yet. When called after an
 * incoming scan packet is received, the value is big endian and must be swapped
 * to little endian to perform the comparison. Valid values are:
 *   - THE_PASSED_IN_DATA_IS_BIG_ENDIAN
 *   - THE_PASSED_IN_DATA_IS_LITTLE_ENDIAN
 * 
 * @return BOOL_32 True if the scan character in the To address matches our scan
 * character; otherwise, false.
 *
 * @author 2/28/2013 Adrianusv
 *
 * @revisions (none)
 */
extern BOOL_32 this_special_scan_addr_is_for_us( const UNS_8 *to_addr_ptr, const UNS_32 pendianness )
{
	UNS_32	packet_scan_char;

	UNS_8	temp_address[ 3 ];

	// 9/20/2012 rmd : to_addr_ptr can point to either a little endian or big
	// endian format. We want it as little endian to do our comparison. That is
	// why we swap them around here. So in the case it already points to a
	// little endian format there is really nothing to do but we do move the
	// address into the temp_address location. If that pendianess argument
	// indicated it is pointing to a big endian format we swap around the bytes.

	// 9/20/2012 rmd : This middle byte always stays the same.
	temp_address[ 1 ] = to_addr_ptr[ 1 ];

	if( pendianness == THE_PASSED_IN_DATA_IS_BIG_ENDIAN )
	{
		temp_address[ 0 ] = to_addr_ptr[ 2 ];
		temp_address[ 2 ] = to_addr_ptr[ 0 ];
	}
	else
	{
		temp_address[ 0 ] = to_addr_ptr[ 0 ];
		temp_address[ 2 ] = to_addr_ptr[ 2 ];
	}

	packet_scan_char = 0;	// Set all 4 bytes to 0
	memcpy( (void*)&packet_scan_char, temp_address, 3 );

	return( packet_scan_char == FLOWSENSE_get_controller_letter() );
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION  Compares our known serial number to the address contained in the memory pass in.

	@PARAMETERS

		*vptr: A pointer to our serial number, could be little endian or big endian, we know from the 2nd paramater.

		endianness: either ENDIAN_BIG or ENDIAN_LITTLE depending if its from the transport or application, respectively 

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITIES(none)

	@EXECUTED 

	@RETURN True if the serial matches our serial number

	@ORIGINAL 9/19/2012 raf

	@REVISIONS
*/

extern BOOL_32 this_communication_address_is_our_serial_number( const UNS_8 *to_addr_ptr, const UNS_32 pendianness )
{
	// Used for all general communication packets. With the exception of the SCAN packets.
	UNS_32	packet_to_serial_number;

	// 9/19/2012 raf :  We need to swap our bytes because we need to support legacy communications, which is big endian.
	UNS_8	temp_address[ 3 ];

	// 9/20/2012 rmd : vptr can point to either a little endian or big endian format. We want it
	// as little endian to do our comparison. That is why we swap them around here. So in the
	// case it already points to a little endian format there is really nothing to do but we do
	// move the address into the temp_address location. If that pendianess argument indicated it
	// is pointing to a big endian format we swap around the bytes.

	// 9/20/2012 rmd : This middle byte always stays the same.
	temp_address[ 1 ] = to_addr_ptr[ 1 ];
	
	if( pendianness == THE_PASSED_IN_DATA_IS_BIG_ENDIAN )
	{
		// 9/19/2012 raf :  we need to swap 0 and 2 bytes
		temp_address[ 0 ] = to_addr_ptr[ 2 ];
		temp_address[ 2 ] = to_addr_ptr[ 0 ];
	}
	else
	{
		temp_address[ 0 ] = to_addr_ptr[ 0 ];
		temp_address[ 2 ] = to_addr_ptr[ 2 ];
	}

	packet_to_serial_number = 0;  // Set all 4 bytes to 0.
	memcpy( &packet_to_serial_number, temp_address, 3 );  // Pick out the serial number.
																																	
	return( config_c.serial_number == packet_to_serial_number );
}

/* ---------------------------------------------------------- */
/*--------------------------------------------------------------------------
    Name:           CommAddressesAreEqual(char *a1, char *a2)

    Description:    Pass this routine the pointers to the start of the 2
                    three digit communication addresses you want to compare

    Parameters:     a1 and a2 are char pointers

    Return Values:  BOOLEAN TRUE or FALSE

    Date:           rmd 10/19/00

 -------------------------------------------------------------------------*/
BOOL_32	CommAddressesAreEqual( void *pa1, void *pa2)
{
	BOOL_32	rv;

	rv = (true);

	unsigned short i;
	unsigned char *a1, *a2;

	a1 = pa1;
    a2 = pa2;

    for( i=0; i<3; i++ )
	{
		if( *a1++ != *a2++ )
		{
			rv = (false);
		}
    }

    return( rv );
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION This function handles OM's. These are messages that were created within
	this controller. And that is by the definition of an OM. Most OM's are triggered into
	existence by an incoming message (an IM). And they go back out the port they came in.
	There are TWO exceptions to this rule - the SCAN and the TOKEN messages. They are
	distributed to all ports that will receive them. This behavior is accomplished within
	the TPL_OUT task as the message packets are sent.

	@MEMORY_RESPONSIBILITES The message memory responsiblity is passed into this function.
	The caller has allocated the message memory. But does not have to free it. Ultimately
	the message memory is freed within the TPL_OUT task when it adds the message to the list
	of OM's. So the message is passed by reference via a queue to the TPL_OUT task.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@EXECUTED Executed within the context of the COMM_MNGR task as we send out SCAN and
	TOKEN messages AND as part of the processing of incoming messages. All of which is
	performed under control of the COMM_MNGR task.
	
	@RETURN (none)

	@AUTHOR 2012.03.15 rmd
*/
extern void COMM_MNGR_send_outgoing_3000_message_and_take_on_memory_release_responsiblity( UPORTS p_port, DATA_HANDLE p_dh, ADDR_TYPE p_from_to, UNS_32 p_message_class )
{
	// Sanity check. Range check the serial port.
	if( p_port > UPORT_M1 )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "Port out of range." );

		// 3/16/2012 rmd : The memory freeing responsibility was passwed into this function.
		mem_free( p_dh.dptr );
	}
	else
	{
		if( (p_message_class == MSG_CLASS_CS3000_SCAN) || (p_message_class == MSG_CLASS_CS3000_TOKEN) )
		{
			// 2011.08.11 rmd : For the outgoing SCAN messages and TOKEN messages there is no reason to
			// keep the previous outgoing scan or token message. As a matter of fact an outgoing token
			// is an active message and will autonomiously generate requests for status until it sees
			// the receiving transport (where-ever that may be) indicate the message was received. And
			// it is possible this status from the remote IM to our local OM never makes it. Yet the
			// remote IM was complete and passed to the app and the app responded and our local IM
			// received the response and the comm mngr is moving on to the next OM token. Se clean the
			// slate. We don't need the old OM scan or token messages.
			TPL_OUT_destroy_all_outgoing_3000_scan_and_token_messages();

			// 2011.08.11 rmd : A similar argument holds for the IM token RESP. It is an ACTIVE message.
			// That is if it does not get a request for a status it will autonimously ask for one. Well
			// if we actually declared a failure for the last toekn then we certainly do not want a
			// 'broken' IM resp for that token to be alive and attempting to receive the entire message.
			// So kill all the IM scan and token responses.
			TPL_IN_destroy_all_incoming_3000_scan_response_and_token_response_messages();
		}

		BOOL_32 send_to_tpl_out, send_to_incoming_msgs;

		send_to_tpl_out = (false);

		send_to_incoming_msgs = (false);

		if( p_message_class == MSG_CLASS_CS3000_SCAN )
		{
			// 3/20/2012 rmd : Because the scan message uses the communication CHAR as the TO address we
			// must look at the to address that as a scan character to see if it is to us.
			if( this_special_scan_addr_is_for_us( (UNS_8*)&p_from_to.to, THE_PASSED_IN_DATA_IS_LITTLE_ENDIAN ) == (true) )
			{
				send_to_incoming_msgs = (true);
			}
			else
			{
				// 7/25/2013 rmd : SCAN message is not broadcast. So it is either to us or passes through
				// the transport.
				send_to_tpl_out = (true);
			}
		}
		else
		if( p_message_class == MSG_CLASS_CS3000_TOKEN )
		{
			// 3/15/2012 rmd : There is ONLY one class of OM that is truly a BROADCAST message. Meaning
			// no matter the TO address we are going to pass this message to the TPL_IN task. And that
			// is the TOKEN.
			send_to_incoming_msgs = (true);

			// 4/30/2014 rmd : If we are the master of a REAL chain and we made a token BROADCAST it.
			if( FLOWSENSE_we_are_the_master_of_a_multiple_controller_network() )
			{
				send_to_tpl_out = (true);
			}
		}
		else
		{
			// 3/15/2012 rmd : All other message classes should have their TO address serial number
			// checked. If the message is to us it goes ONLY to our incoming message list. Else it goes
			// ONLY out the port it came in.
			if( this_communication_address_is_our_serial_number( (UNS_8 *)&p_from_to.to, THE_PASSED_IN_DATA_IS_LITTLE_ENDIAN  ) )
			{
				send_to_incoming_msgs = (true);
			}
			else
			{
				send_to_tpl_out = (true);
			}
		}

		// ----------
		
		if( send_to_incoming_msgs == (true) )
		{
			// Hand the message to ourselves. Which allocates new memory into which a copy of the
			// message is placed. This new memory will not be freed until the message has been pulled
			// off the list and processed. Then it is freed.
            ROUTING_CLASS_DETAILS_STRUCT    rcds;
			
			rcds.routing_class_base = ROUTING_CLASS_IS_3000_BASED;
			rcds.rclass = p_message_class;
			
			CENT_COMM_make_copy_of_and_add_message_or_packet_to_list_of_incoming_messages( p_port, p_dh, rcds, p_from_to );
		}

		// ----------
		
		if( send_to_tpl_out == (true) )
		{
			TPL_OUT_EVENT_QUEUE_STRUCT  toqs;

			// The message is going out one or more of the serial ports. For the case of the SCAN and
			// TOKEN being sent out all FLOWSENSE enbled ports, this behavior is accomplished at the
			// TPL_OUT level. Right where packets are sent. For all other message classes, again the
			// decision is made within the TPL_OUT when the packets are sent but the bottom line is the
			// outgoing message is a response and is sent ONLY out the port from which it was received.
			// Which is the p_port here.
			//
			// Build an outgoing message. The message memory WILL NOT be freed until the TPL_OUT queue
			// message is processed in it's task and the OM has been created. Then it is freed.

			toqs.event = TPL_OUT_QUEUE_COMMAND_here_is_an_outgoing_message;
			toqs.dh = p_dh;
			toqs.port = p_port;
			toqs.from_to = p_from_to;
			toqs.msg_class.routing_class_base = ROUTING_CLASS_IS_3000_BASED;
			toqs.msg_class.rclass = p_message_class;

			if( xQueueSend( TPL_OUT_event_queue, &toqs, 0 ) != pdPASS )
			{
				ALERT_MESSAGE_WITH_FILE_NAME( "TPL_OUT queue full" );
			}
		}
		else
		{
			// 3/16/2012 rmd : This reveals the message memory take and free scheme. The caller has
			// already obtained memory and built the message. If the message needed to go to ourselves a
			// copy was made and the copy was added to the incoming message list. Then if we needed to
			// send the message out serial ports it was handed to TPL_OUT and along with it the memory
			// freeing responsibilites. If the message doesn't go to TPL_OUT we must free the memory
			// here.
			mem_free( p_dh.dptr );
		}

	}  // of if the p_port is not out of range
}

/* ---------------------------------------------------------- */
extern void SendCommandResponseAndFree( DATA_HANDLE pdh, COMMUNICATION_MESSAGE_LIST_ITEM *pcmli, UNS_32 p_message_class )
{
	ADDR_TYPE	from_to;

	// It is from us.
	memcpy( &(from_to.from), &(config_c.serial_number), 3 );

	// And it is to who it is from.
	memcpy( &(from_to.to), &(pcmli->from_to.from), 3 );

	// Send it and relinquish control over the associated memory. If we are sending a command response typically the machine we
	// are sending to is in charge of communications and should make the message active on his own - in the ET2000e this whole
	// OM_receiving_party_SHOULD_make_active scheme is ONLY used by the RRe communications (the ET2000e on messages received
	// from the RRe_MACHINE) so it doesn't matter what we set this to - well maybe it does it this is a RESP for an RRe message
	// in which case we want the receiving guy to make the message active.
	COMM_MNGR_send_outgoing_3000_message_and_take_on_memory_release_responsiblity( pcmli->in_port, pdh, from_to, p_message_class );
}

/* ---------------------------------------------------------- */
extern void COMM_UTIL_build_and_send_a_simple_flowsense_message( COMM_COMMANDS pcommand, COMMUNICATION_MESSAGE_LIST_ITEM *pcmli, UNS_32 p_message_class )
{
	DATA_HANDLE		ldh;
	
	// build a simple command only RESP type response
	ldh.dlen = sizeof( COMM_COMMANDS );
	ldh.dptr = mem_malloc( ldh.dlen );

	*(COMM_COMMANDS*)(ldh.dptr) = pcommand;
	
	SendCommandResponseAndFree( ldh, pcmli, p_message_class );
}

/*--------------------------------------------------------------------------
    Name:           Roll_FIFO( void *array_ptr, int element_size, int how_many )

	FileName:		C:\develop\GHS\ET2000\ETSource\etcommon.c

    Description:    A function developed to perform the fifo roll that takes place when
					adding an item to a fifo array list of items. We keep many fifo lists in
					memory with permantely allocated space for them i.e. we dont use the krnllist
					functions for them. These list include diagnostics, flow readings, transport mid's to
					name a few.

					We roll the oldest item out the top and the newest item is to be added to the bottom.

    Parameters:     void *array_ptr		pointer to the start of the array
    				int element_size	size of each element in the array
    				int how_many		number of items in the array

    Return Values:  None

    History:		date	approval	author	description
					4/30/01	--/--/--	rmd

 -------------------------------------------------------------------------*/
void Roll_FIFO( void *array_ptr, int element_size, int how_many )
{
	char *to_ptr, *from_ptr;

	unsigned short i;

	if( how_many < 2 )
	{
		Alert_Message( "FIFO too small" );
	}
	else
	{
		// form the to and from ptrs
		to_ptr = array_ptr;
		to_ptr += (how_many-1) * element_size;

		from_ptr = array_ptr;
		from_ptr += (how_many-2) * element_size;

		for ( i=0; i<how_many - 1; i++ ) {

			memcpy( to_ptr, from_ptr, element_size );

			to_ptr -= element_size;
			from_ptr -= element_size;

		}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

