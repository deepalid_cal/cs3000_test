/*  file = irri_comm.h              10/27/00 2011.08.01  rmd  */

/* ---------------------------------------------------------- */

#ifndef _INC_IRRI_COMM_H
#define _INC_IRRI_COMM_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"lpc_types.h"

#include	"comm_utils.h"

#include	"irrigation_system.h"

#include	"foal_irri.h"

#include	"foal_lights.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
typedef struct
{
	// --------

	// 8/31/2012 rmd : When the use requests. This is filled out. I suppose could also be
	// requested via the communications while parsing a message from the central.
	TEST_AND_MOBILE_KICK_OFF_STRUCT	test_request;
	
	// --------

	// 4/4/2013 ajv : When the user requests manual watering, this is filled
	// out. I suppose could also be requested via the communications while
	// parsing a message from the central.
	MANUAL_WATER_KICK_OFF_STRUCT	manual_water_request;

	// 8/12/2015 mpd : Lights can only be turned on from slaves as MANUAL. MOBILE is not handled here since all 
	// communications from mobile devices communicate with the master (FOAL side).
	LIGHTS_ON_XFER_RECORD			light_on_request;

	// 8/17/2015 mpd : TBD: Why take up permanent space in the "irri_comm" when we can gather all the needed info in a 
	// local var?

	// 8/12/2015 mpd : Used to send output current info (ma, shorts, and opens) to the master. All 4 outputs are in the 
	// struct. There is no "InUse" byte since all outputs are handled in a single token.
	//LIGHTS_INFO_TF_XFER_RECORD			lights_info;

	// 8/12/2015 mpd : Lights can be turned off from slaves as MANUAL or TEST. This structure handles both cases. MOBILE
	// is not handled here since all communications from mobile devices communicate with the master (FOAL side).
	LIGHTS_OFF_XFER_RECORD			light_off_request;

	// --------
	
	// 2012.02.15 rmd : This is set (true) when the STOP key is pressed. Setting (true) causes
	// the token response to tell the foal master about this key press.
	BOOL_32				send_stop_key_record;
	
	STOP_KEY_RECORD_s	stop_key_record;
	
	// 11/11/2014 rmd : These flags are SET at each irri machine in the chain when the two_wire
	// cable excessive current is acknowledged by the foal side. If we are the originating
	// controller we also flag the tp micro to clean the slate of decoder commands and to all
	// new incoming decoder commands. This flag from that point forward is nothing more than a
	// status flag for the display. And it is cleared when any station turn ON action_needed
	// record is received for a station on that box (which may not mean the cable is energized
	// cause it is not a two-wire station but probably is good enough - a more precise
	// determination of if the box had a poc_decoder for the system the station belongs to or if
	// the station was a decoder based station at the box would be needed).
	BOOL_32		two_wire_cable_excessive_current[ MAX_CHAIN_LENGTH ];

	// 11/11/2014 rmd : This flag is SET at each irri machine in the chain when the two_wire
	// cable overheated is acknowledged by the foal side. When that happens we also flag the tp
	// micro to clean the slate of decoder commands and to all new incoming decoder commands.
	// This flag at that point is nothing more than a status flag. And it is cleared when the
	// TPMICRO has determined the thermistors have cooled down and have signaled the master of
	// such.
	BOOL_32		two_wire_cable_overheated[ MAX_CHAIN_LENGTH ];

	// --------
	
	// 5/15/2012 rmd : On program startup these are initialized to 0. Which is the indication
	// that there is no system to send the clear mlb command to the master for. Remember the
	// scheme is we clear all three kinds of mlb's with the single clear command. Since the
	// gid's are stored as UNS-16 this can be an array of such.
	UNS_16		system_gids_to_clear_mlbs_for[ MAX_POSSIBLE_SYSTEMS ];

	// --------
	
	// 2/7/2013 ajv : This is set TRUE when the user presses the Clear Runaway
	// Gage button on the ET Gage screen. When this is true, the token response
	// tells the FOAL master about this key press.
	BOOL_32		clear_runaway_gage_pressed;

	// 2/26/2013 ajv : If the controller's local TP Micro detects a change to
	// what's installed, the controller needs to pass it to the master to update
	// the records throughout the chain.
	BOOL_32		send_box_configuration_to_master;

	// 4/3/2013 ajv : When the master decides it's time to verify the chain's
	// file CRCs, it sends a command to the slaves to send their individual CRC
	// list. This irri_comm flag is set at the slave to notify it to add the
	// information when it builds the token response.
	BOOL_32		send_crc_list;

	// ----------

	MVOR_KICK_OFF_STRUCT	mvor_request;

	// ----------
	
	// 3/21/2018 rmd : After a walk thru delay reaches 0, and all walk thru changes have made it
	// to the master, we send the master the gid of the walk thru sequence we want to start.
	BOOL_32		walk_thru_need_to_send_gid_to_master;
	
	UNS_32		walk_thru_gid_to_send;
	
} IRRI_COMM;

// 9/28/2012 rmd : Note this structure is not in battery backed SRAM. 
extern IRRI_COMM	irri_comm;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern BOOL_32 IRRI_COMM_if_any_2W_cable_is_over_heated( void );

// ----------

extern void IRRI_COMM_process_incoming__clean_house_request( COMMUNICATION_MESSAGE_LIST_ITEM *cmli );

extern void IRRI_COMM_process_incoming__crc_error_clean_house_request( COMMUNICATION_MESSAGE_LIST_ITEM *cmli );

extern void IRRI_COMM_process_stop_command( const BOOL_32 pstop_all_irrigation );

// ----------

extern void irri_add_to_main_list_for_test_sequential( STATION_FROM_MAIN_LIST_DISTRIBUTION_STRUCT *sds_ptr );

extern void IRRI_COMM_process_incoming__irrigation_token( COMMUNICATION_MESSAGE_LIST_ITEM *cmli );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

