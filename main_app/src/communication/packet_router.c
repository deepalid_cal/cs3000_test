/*  file = packet_router.c      02.13.06 ajv : 2011.07.20 rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"packet_router.h"

#include	"crc.h"

#include	"comm_utils.h"

#include	"tpl_in.h"

#include	"alerts.h"

#include	"td_check.h"

#include	"comm_mngr.h"

#include	"foal_comm.h"

#include	"rcvd_data.h"

#include	"app_startup.h"

#include	"configuration_controller.h"

#include	"configuration_network.h"

#include	"tpmicro_comm.h"

#include	"controller_initiated.h"

#include	"cent_comm.h"

#include	"flowsense.h"

#include	"code_distribution_receive.h"

#include	"cs_mem.h"

#include	"gpio_setup.h"

#include	"radio_test.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*

COMMUNICATIONS RESTRICTIONS

- If a -M-M or a -M-SR is connected to a Central, it must be the FLOWSENSE
  master.

- If communicating via Port C or an LA2, there cannot be another controller
  on the chain with a device on Port C.

- In order for SR rerouting to work, SR master must be an ET2000e (slaves
  can be ET2000 (500 series) or ET2000e).

- There may only be ONE SR Master within an SR chain.

- If a -SR is a FLOWSENSE master, SR Radio must be the SR master radio.

- An SR slave cannot be a FLOWSENSE master.

*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION One of the arguments is a block of data. Allcoate new space for that data
    plus the ambles. Add the ambles and send to the serial port driver for the specified
    port.

    The primary use of this function is by the outgoing transport. And it wants to know when
    a block is actually transmitted. For the purpose of starting the status response timer.
    You see with slow radios and the action of CTS combined with long messages (15 packets
    say) the time delta from wehn the packet is given to the serial port till when it is
    actually transmitted can be 20 plus seconds.

    Set pom to NULL if not needed. Such as for the messages to the tpmicro which are
    actually a single packet non-transport message.

	@CALLER_MUTEX_REQUIREMENTS (none)

    @MEMORY_RESPONSIBILITES Do not release the pdh memory. Allocate our own and release
    after given to the serial driver.

    @EXECUTED Executed within the context of the TPL_OUT task. And the COMM_MNGR when
    sending messages to the tpmicro.

	@RETURN (none)

	@ORIGINAL 2011 rmd

	@REVISIONS
*/
extern void Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy( const UPORTS pto_which_port, DATA_HANDLE pdh, OUTGOING_MESSAGE_STRUCT *pom )
{
	unsigned char	*ucp;

	DATA_HANDLE		ldh;

	// ----------

	// Because we have to add the pre and post ambles to define the packet it size is + 2*sizeof(AMBLE_TYPE).
	ldh.dlen = pdh.dlen + ( 2 * sizeof(AMBLE_TYPE) );

	// Allocate space for it.
	ldh.dptr = mem_malloc( ldh.dlen );

	ucp = ldh.dptr;

	// ----------

	// Put the preamble in place.
	*ucp++ = preamble._1;
	*ucp++ = preamble._2;
	*ucp++ = preamble._3;
	*ucp++ = preamble._4;

	// ----------

	// Copy the data in place.
	memcpy( ucp, pdh.dptr, pdh.dlen );

	ucp += pdh.dlen;  // hop over data

	// ----------

	// Put the postamble in place.
	*ucp++ = postamble._1;
	*ucp++ = postamble._2;
	*ucp++ = postamble._3;
	*ucp   = postamble._4;

	// ----------

	// Pass it to the serial driver.
	AddCopyOfBlockToXmitList( pto_which_port, ldh, pom, SERIAL_DRIVER_HANDLING_SENDING_PACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );

	// We are done with our local packet. A copy now exists in the serial driver xmit list.
	mem_free( ldh.dptr );
}

/* ---------------------------------------------------------- */
/*
void Copy_And_Add_To_List_Of_Packets_Waiting_For_The_Token( UPORTS pto_port, DATA_HANDLE pdh, OUTGOING_MESSAGE_STRUCT *pom )
{
	// POM is the indication passed onto the serial driver about if it needs to signal the TPL_OUT task that it just sent a
	// packet asking for a status. TPL_OUT starts a timer waiting for the status when it receives this notification.

	// MUST take a copy of the packet. For example the TPL_OUT task resets the rqst for status bits within the packet after
	// handing the packet to this function.

	PACKET_ROUTING_LIST_ITEM	*prli;

	DATA_HANDLE					ldh;

	// allocate and take a copy of the packet
	ldh.dlen = pdh.dlen;

	ldh.dptr = mem_malloc( pdh.dlen );

	memcpy( ldh.dptr, pdh.dptr, pdh.dlen );


	prli = mem_malloc( sizeof(PACKET_ROUTING_LIST_ITEM) );

	prli->dh = ldh;

	prli->to_port = pto_port;

	prli->pom = pom;

	xSemaphoreTake( list_packets_waiting_for_token_MUTEX, portMAX_DELAY );

	nm_ListInsertTail( &comm_mngr.packets_waiting_for_token, prli );

	xSemaphoreGive( list_packets_waiting_for_token_MUTEX );
}
*/

/* ---------------------------------------------------------- */
/*
void Xmit_All_Packets_Waiting_For_Token( void )
{
	PACKET_ROUTING_LIST_ITEM *prli;

	xSemaphoreTake( list_packets_waiting_for_token_MUTEX, portMAX_DELAY );

	while( (prli = nm_ListRemoveHead( &comm_mngr.packets_waiting_for_token )) != NULL )
	{
		// REMOVE it and process it - when done release the memory associated with the item.

		// whatever this uses to "send" the data it must take a copy of
		// the data and leave the original message intact
		Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy( prli->to_port, prli->dh, prli->pom );

		// message has been processed so free it from the list
		// free the data of the message itself
		mem_free( prli->dh.dptr );

		// free the list item for the message
		mem_free( prli );
	}

	xSemaphoreGive( list_packets_waiting_for_token_MUTEX );
}
*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void attempt_to_copy_packet_and_route_out_another_port( UPORTS pto_who, DATA_HANDLE pdh, OUTGOING_MESSAGE_STRUCT *pom )
{
	// 4/2/2014 rmd : The pom parameter is a TRANSPORT related start the staus timer affair. If
	// not needed (only needed when called from the trnasport) set pom to a NULL.
	
	// 3/26/2014 rmd : This function is called from both the comm_mngr and the ring_buffer
	// tasks. I don't think there are any multi-tasking or memory implications to discuss. The
	// pdh DATA_HANDLE memory must be freed by the caller. No transfer of memory
	// responsibilities takes place.
	
	// 3/26/2014 rmd : NOTE - not intended to be called with the port parameter set to UPORT_TP.
	// That just doesn't make sense from the point of view of the application (the caller of
	// this function).
	
	// 3/26/2014 rmd : Furthermore an added test on eligibility to route the packet through this
	// port should include the device type on the port. When it comes to ports A & B.
	
	// ----------

	UPORTS	new_port;
	
	BOOL_32	allowed;

	// ----------

	if( pto_who == UPORT_TP )
	{
		Alert_Message( "illogical port choice" );
	}
	else
	{
		// 3/26/2014 rmd : Set the default (true) which covers for UPORT_M1 in the following testing
		// that follows. We can ALWAYS send the packet out the M. Dash-M card or no card.
		allowed = (true);
		
		// 7/21/2016 rmd : This function is called by masters to route packets to other controllers
		// (flowsense and code distribution packets). For the MASTER we only call this function
		// asking to try PORT_B and PORT_TP. Which is by definition because any device on the MASTER
		// PORT_A passes traffic back to the central.
		//
		// 7/21/2016 rmd : For the SLAVES we can end up testing any of the 3 ports - depending on
		// where the packet arrived from. But the answer is always the same - if there is a device
		// on the port send the packet.
		if( pto_who == UPORT_A )
		{
			allowed = (config_c.port_A_device_index != COMM_DEVICE_NONE_PRESENT);
		}
		else
		if( pto_who == UPORT_B )
		{
			allowed = (config_c.port_B_device_index != COMM_DEVICE_NONE_PRESENT);
		}
		
		// ----------

		// 4/2/2014 rmd : So the scenario is that a slave tpmicro has just started to do an ISP
		// update to its tpmicro. Say the controller has an SR radio on it to. A scan packet arrives
		// via the SR radio. The router attempts to push it out the M port - like it should. However
		// if we send a packet to the tpmicro mid ISP stream we botch the programming. This prevents
		// that from occurring. This is only a phenomenon at the slaves as the master has its own
		// protection waiting to start the scan till after its own ISP related activity has
		// completed.
		if( pto_who == UPORT_M1 )
		{
			allowed = !tpmicro_comm.in_ISP && !tpmicro_comm.code_version_test_pending && tpmicro_comm.up_and_running;
		}
		
		// ----------

		if( allowed )
		{
			// 3/26/2014 rmd : If we concluded that port M1 is to accept this packet we must translate
			// the port to the TP port. So the packet arrives where the physical M1 port is. The TP
			// Micro will take the packet and relay it to port M1.
			if( pto_who == UPORT_M1 )
			{
				new_port = UPORT_TP;
			}
			else
			{
				new_port = pto_who;
			}
			
			// 3/26/2014 rmd : Send to the translated port.
			Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy( new_port, pdh, pom );
		}

	}
}

/* ---------------------------------------------------------- */
static BOOL_32 strip_hub_routing_header_from_packet( DATA_HANDLE *pdh )
{
	DATA_HANDLE			new_block;
	
	UNS_8				*from_ucp, *to_ucp, *start_of_crc;

	UNS_32				bytes_to_crc;
	
	CRC_BASE_TYPE		lcrc;
	
	BOOL_32				rv;
	
	rv = (false);
	
	// ----------
	
	// 3/21/2017 rmd : The packet has arrived here with the ambles stripped. The length includes
	// the CRC. We need to remove the special 2 byte hub routing structure that is up front.
	// Then recompute the crc. And return the packet in the original memory block. With the
	// packet length appropriately adjusted. And yes with the ambles still not in place.
	if( mem_obtain_a_block_if_available( pdh->dlen, (void**)&(new_block.dptr) ) )
	{
		// 3/21/2017 rmd : Nothing can go wrong now so return (true).
		rv = (true);
		
		// 3/29/2017 rmd : Where to put data as we are building the new block.
		to_ucp = new_block.dptr;

		// 3/29/2017 rmd : Where we get the data from - skipping over the header we are
		// stripping.
		from_ucp = pdh->dptr + sizeof(TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER);
		
		// 3/22/2017 rmd : This will be the FINAL length.
		new_block.dlen = pdh->dlen - sizeof(TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER);
		
		// ----------
		
		// 3/22/2017 rmd : We are building up the final packet into the new_block memory.
		start_of_crc = to_ucp;

		bytes_to_crc = new_block.dlen - sizeof(CRC_TYPE);
		
		// ----------
		
		// 3/22/2017 rmd : Fill in the new block with the content - except for the CRC at the end.
		// We are DONE with the from_ucp pointer at this point.
		memcpy( to_ucp, from_ucp, bytes_to_crc );
		
		// 3/22/2017 rmd : Advance to where the crc sits.
		to_ucp += bytes_to_crc;
		
		// ----------
		
		// 3/22/2017 rmd : Calculate the CRC. Which is returned in BIG ENDIAN format. Which is what
		// we need (see note in crc calc function). Then copy to ucp.
		lcrc = CRC_calculate_32bit_big_endian( start_of_crc, bytes_to_crc );
		
		memcpy( to_ucp, &lcrc, sizeof(CRC_BASE_TYPE) );
	
		// ----------
		
		// 3/22/2017 rmd : Okay new packet has been formed and sits in the new_block memory. Copy it
		// back to the original memory location. And adjust the original data handle length. This is
		// what the caller is to work with.
		memcpy( pdh->dptr, new_block.dptr, new_block.dlen );
		
		pdh->dlen = new_block.dlen;
		
		// ----------
		
		// 3/22/2017 rmd : And free that new_block memory that we are now done with.
		mem_free( new_block.dptr );
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 this_2000_packet_from_the_commserver_is_on_the_hub_list( TPL_DATA_HEADER_TYPE *ptpldh_ptr )
{
	BOOL_32	rv, now_2000;
	
	UNS_32	how_many, iii;
	
	HUB_LIST_ADDR_UNION	hlau;
	
	// ----------
	
	rv = (false);
	
	// ----------
	
	// 4/30/2017 rmd : The hub list queue is protected by a MUTEX. Always need to take before
	// working with this queue.
	xSemaphoreTake( router_hub_list_MUTEX, portMAX_DELAY );

	// ----------
	
	//CS_LED_RED_ON;


	// 4/30/2017 rmd : The list always starts with 3000's first, till the divider is hit.
	now_2000 = (false);
	
	how_many = uxQueueMessagesWaiting( router_hub_list_queue );
	
	for( iii=0; iii<how_many; iii++ )
	{
		// 4/30/2017 rmd : If we go through the entire queue pulling from the front and writing to
		// the back the queue will remain in its original order witht he same number of items. Keep
		// in mind, we ALWAYS have to run through the ENTIRE list, even if we find what we are
		// looking for part way through, to keep the list intact.
		
		// 6/19/2017 rmd : Do not wait for a queue item, there is no task adding to the queue except
		// for when a message comes from the commserver. Our goal is to pull the items that are on
		// the queue at this time. Not wait for future items that are added to the list.
		if( xQueueReceive( router_hub_list_queue, &hlau, 0 ) )
		{
			// 5/1/2017 rmd : This is an optimization, time wise, for this function. Once we have found
			// a match, stop testing.
			if( !rv )
			{
				// 4/30/2017 rmd : Did we hit the divider between 3000 and 2000?
				if( hlau.sn_3000 == 0xffffffff )
				{
					now_2000 = (true);
				}
				else
				if( now_2000 )
				{
					if( (hlau.list_addr[0] == ptpldh_ptr->H.FromTo.to[0]) && (hlau.list_addr[1] == ptpldh_ptr->H.FromTo.to[1]) && (hlau.list_addr[2] == ptpldh_ptr->H.FromTo.to[2]) )
					{
						rv = (true);
					}
				}
			}

			// 4/30/2017 rmd : Always write them back onto the queue. Do not wait if the queue is full,
			// nothing is removing items.
			xQueueSendToBack( router_hub_list_queue, &hlau, 0 );
		}
		else
		{
			// 4/30/2017 rmd : This should NEVER happen .... i think.
			Alert_Message( "HUB: unexp hub list queue ERROR" );
		}
	}
		
	// ----------
	
	//CS_LED_RED_OFF;


	xSemaphoreGive( router_hub_list_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 this_2000_packet_is_from_a_controller_on_the_hub_list( TPL_DATA_HEADER_TYPE *ptpldh_ptr )
{
	BOOL_32	rv, now_2000;
	
	UNS_32	how_many, iii;
	
	HUB_LIST_ADDR_UNION	hlau;
	
	// ----------
	
	rv = (false);
	
	// ----------
	
	// 4/30/2017 rmd : The hub list queue is protected by a MUTEX. Always need to take before
	// working with this queue.
	xSemaphoreTake( router_hub_list_MUTEX, portMAX_DELAY );

	// ----------
	
	//CS_LED_RED_ON;


	// 4/30/2017 rmd : The list always starts with 3000's first, till the divider is hit.
	now_2000 = (false);
	
	how_many = uxQueueMessagesWaiting( router_hub_list_queue );
	
	for( iii=0; iii<how_many; iii++ )
	{
		// 4/30/2017 rmd : If we go through the entire queue pulling from the front and writing to
		// the back the queue will remain in its original order witht he same number of items. Keep
		// in mind, we ALWAYS have to run through the ENTIRE list, even if we find what we are
		// looking for part way through, to keep the list intact.
		
		// 6/19/2017 rmd : Do not wait for a queue item, there is no task adding to the queue except
		// for when a message comes from the commserver. Our goal is to pull the items that are on
		// the queue at this time. Not wait for future items that are added to the list.
		if( xQueueReceive( router_hub_list_queue, &hlau, 0 ) )
		{
			// 5/1/2017 rmd : This is an optimization, time wise, for this function. Once we have found
			// a match, stop testing.
			if( !rv )
			{
				// 4/30/2017 rmd : Did we hit the divider between 3000 and 2000?
				if( hlau.sn_3000 == 0xffffffff )
				{
					now_2000 = (true);
				}
				else
				if( now_2000 )
				{
					if( (hlau.list_addr[0] == ptpldh_ptr->H.FromTo.from[0]) && (hlau.list_addr[1] == ptpldh_ptr->H.FromTo.from[1]) && (hlau.list_addr[2] == ptpldh_ptr->H.FromTo.from[2]) )
					{
						rv = (true);
					}
				}
			}

			// 4/30/2017 rmd : Always write them back onto the queue. Do not wait if the queue is full,
			// nothing is removing items.
			xQueueSendToBack( router_hub_list_queue, &hlau, 0 );
		}
		else
		{
			// 4/30/2017 rmd : This should NEVER happen .... i think.
			Alert_Message( "HUB: unexp hub list queue ERROR" );
		}
	}
		
	// ----------
	
	//CS_LED_RED_OFF;


	xSemaphoreGive( router_hub_list_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 this_3000_packet_from_the_commserver_is_on_the_hub_list( FROM_COMMSERVER_PACKET_TOP_HEADER *pfcpth_ptr )
{
	BOOL_32	rv, now_2000;
	
	UNS_32	how_many, iii;
	
	UNS_32	sn_3000;
	
	rv = (false);
	
	// ----------
	
	// 4/30/2017 rmd : The hub list queue is protected by a MUTEX. Always need to take before
	// working with this queue.
	xSemaphoreTake( router_hub_list_MUTEX, portMAX_DELAY );

	// ----------
	
	// 4/30/2017 rmd : TIMING NOTES - built a hub list of 100 long, 50 3000 serial number and 50
	// 2000 serial numbers, this function took less than 400usec to execute! With DEBUG code.
	// Blazing! Release code will be faster.
	//CS_LED_RED_ON;

	// ----------
	
	// 4/30/2017 rmd : The list always starts with 3000's first, till the divider is hit.
	now_2000 = (false);
	
	how_many = uxQueueMessagesWaiting( router_hub_list_queue );
	
	for( iii=0; iii<how_many; iii++ )
	{
		// 4/30/2017 rmd : If we go through the entire queue pulling from the front and writing to
		// the back the queue will remain in its original order witht he same number of items. Keep
		// in mind, we ALWAYS have to run through the ENTIRE list, even if we find what we are
		// looking for part way through, to keep the list intact.
		
		// 6/19/2017 rmd : Do not wait for a queue item, there is no task adding to the queue except
		// for when a message comes from the commserver. Our goal is to pull the items that are on
		// the queue at this time. Not wait for future items that are added to the list.
		if( xQueueReceive( router_hub_list_queue, &sn_3000, 0 ) )
		{
			// 5/1/2017 rmd : This is an optimization, time wise, for this function. Once we have found
			// a match, stop testing.
			if( !rv )
			{
				// 4/30/2017 rmd : Did we hit the divider between 3000 and 2000?
				if( sn_3000 == 0xffffffff )
				{
					now_2000 = (true);
				}
	
				if( !now_2000 )
				{
					if( sn_3000 == pfcpth_ptr->to_serial_number )
					{
						rv = (true);
					}
				}
			}

			// 4/30/2017 rmd : Always write them back onto the queue. Do not wait if the queue is full,
			// nothing is removing items.
			xQueueSendToBack( router_hub_list_queue, &sn_3000, 0 );
		}
		else
		{
			// 4/30/2017 rmd : This should NEVER happen .... i think.
			Alert_Message( "HUB: unexp hub list queue ERROR" );
		}
	}
		
	// ----------
	
	//CS_LED_RED_OFF;
	
	// ----------
	
	xSemaphoreGive( router_hub_list_MUTEX );

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 this_3000_packet_to_the_commserver_is_on_the_hub_list( TO_COMMSERVER_MESSAGE_HEADER *ptcsmh_ptr )
{
	BOOL_32	rv, now_2000;
	
	UNS_32	how_many, iii;
	
	UNS_32	sn_3000;
	
	rv = (false);
	
	// ----------
	
	// 4/30/2017 rmd : The hub list queue is protected by a MUTEX. Always need to take before
	// working with this queue.
	xSemaphoreTake( router_hub_list_MUTEX, portMAX_DELAY );

	// ----------
	
	// 4/30/2017 rmd : TIMING NOTES - built a hub list of 100 long, 50 3000 serial number and 50
	// 2000 serial numbers, this function took less than 400usec to execute! With DEBUG code.
	// Blazing! Release code will be faster.
	//CS_LED_RED_ON;

	// ----------
	
	// 4/30/2017 rmd : The list always starts with 3000's first, till the divider is hit.
	now_2000 = (false);
	
	how_many = uxQueueMessagesWaiting( router_hub_list_queue );
	
	for( iii=0; iii<how_many; iii++ )
	{
		// 4/30/2017 rmd : If we go through the entire queue pulling from the front and writing to
		// the back the queue will remain in its original order witht he same number of items. Keep
		// in mind, we ALWAYS have to run through the ENTIRE list, even if we find what we are
		// looking for part way through, to keep the list intact.
		
		// 6/19/2017 rmd : Do not wait for a queue item, there is no task adding to the queue except
		// for when a message comes from the commserver. Our goal is to pull the items that are on
		// the queue at this time. Not wait for future items that are added to the list.
		if( xQueueReceive( router_hub_list_queue, &sn_3000, 0 ) )
		{
			// 5/1/2017 rmd : This is an optimization, time wise, for this function. Once we have found
			// a match, stop testing.
			if( !rv )
			{
				// 4/30/2017 rmd : Did we hit the divider between 3000 and 2000?
				if( sn_3000 == 0xffffffff )
				{
					now_2000 = (true);
				}
	
				if( !now_2000 )
				{
					if( sn_3000 == ptcsmh_ptr->from_serial_number )
					{
						rv = (true);
					}
				}
			}

			// 4/30/2017 rmd : Always write them back onto the queue. Do not wait if the queue is full,
			// nothing is removing items.
			xQueueSendToBack( router_hub_list_queue, &sn_3000, 0 );
		}
		else
		{
			// 4/30/2017 rmd : This should NEVER happen .... i think.
			Alert_Message( "HUB: unexp hub list queue ERROR" );
		}
	}
		
	// ----------
	
	//CS_LED_RED_OFF;
	
	xSemaphoreGive( router_hub_list_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
static void handle_slave_packet_routing( UNS_32 const pfrom_which_port, ROUTING_CLASS_DETAILS_STRUCT const prouting, DATA_HANDLE pdh, BOOL_32 *need_to_scatter )
{
	// 5/22/2017 rmd : Executed in the context of the RING_BUFFER_ANALYSIS task.
	
	// ----------
	
	TPL_DATA_HEADER_TYPE	tplh;

	// ----------
	
	// 4/13/2017 rmd : Relying on the common packet header, stuff a local header from the packet
	// memory.
	memcpy( &tplh, pdh.dptr, sizeof(TPL_DATA_HEADER_TYPE) );

	// ----------
	
	if( prouting.rclass == MSG_CLASS_CS3000__FROM_CONTROLLER__CODE_DISTRIBUTION )
	{
		// 2/10/2017 rmd : Then the packet is for us - a regular, non hub-based, slave controller in
		// a chain, and we are receiving a binary. Forward a copy of the packet to the code
		// distribution task, to be post processed (meaning processed when that task runs).Code
		// distribution down a chain is broadcast, so process the packet ourselves, and scatter this
		// packet.
		CODE_DISTRIBUTION_make_a_copy_and_queue_incoming_packet( pfrom_which_port, pdh, prouting.rclass );
		
		// 4/13/2017 rmd : And being broadcast ... scatter it.
		*need_to_scatter = (true);
	}
	else
	if( prouting.rclass == MSG_CLASS_CS3000_SCAN )
	{
		// 2011.08.18 rmd : When the scan is sent out it uses a scan_index based address. The
		// scan_resp is a serial number based message. When the receiving transport receives in the
		// scan_resp message it send back a status packet to the sender. That packet simply reverses
		// the to-from addresses and re-assigns its class as a MSG_CLASS_SCAN packet (to ensure
		// proper routing). So then the MSG_CLASS_SCAN packets with their status bit set (meaning
		// they are a status packet) use the true serial number based addressing.
		if( tplh.H.PID.STATUS == 0 )
		{
			if( this_special_scan_addr_is_for_us( tplh.H.FromTo.to, THE_PASSED_IN_DATA_IS_BIG_ENDIAN ) == (true) )
			{
				TPL_IN_make_a_copy_and_direct_incoming_packet( pfrom_which_port, pdh );
			}
			else
			{
				*need_to_scatter = (true);
			}
		}
		else
		{
			// 3/12/2012 rmd : Otherwise this packet is actually the ACK packet at the transport level,
			// FROM THE MASTER. That is the ACK packet to the SCAN_RESP message sent by the slave. And
			// it has normal addressing. It could be for us. If not, scatter it.
			if( this_communication_address_is_our_serial_number( tplh.H.FromTo.to, THE_PASSED_IN_DATA_IS_BIG_ENDIAN ) == (true) )
			{
				TPL_IN_make_a_copy_and_direct_incoming_packet( pfrom_which_port, pdh );
			}
			else
			{
				*need_to_scatter = (true);
			}
		}
	}
	else
	if( prouting.rclass == MSG_CLASS_CS3000_TOKEN )
	{
		if( tplh.H.PID.STATUS == 0 )
		{
			// 3/16/2012 rmd : The packet is not a STATUS packet. Rather part of a regular actual TOKEN
			// message. And we ALWAYS give these incoming packets to the TPL_IN task. This is the
			// BROADCAST implementation.
			TPL_IN_make_a_copy_and_direct_incoming_packet( pfrom_which_port, pdh );
	
			// 3/19/2012 rmd : And now let it scatter!
			*need_to_scatter = (true);
		}
		else
		{
			// 4/13/2017 rmd : This is the status packet, from the MASTER when it received a TOKEN_RESP
			// from a slave. See if it is to us, if not scatter it.
			if( this_communication_address_is_our_serial_number( tplh.H.FromTo.to, THE_PASSED_IN_DATA_IS_BIG_ENDIAN ) == (true) )
			{
				// 4/13/2017 rmd : If to us, send to the TPL_OUT task (via TPL_IN function call).
				TPL_IN_make_a_copy_and_direct_incoming_packet( pfrom_which_port, pdh );
			}
			else
			{
				*need_to_scatter = (true);
			}
		}
	}
	else
	if( (prouting.rclass == MSG_CLASS_CS3000_SCAN_RESP) || (prouting.rclass == MSG_CLASS_CS3000_TOKEN_RESP) )
	{
		// 4/13/2017 rmd : SCAN_RESP packets are always headed for the master of a chain. And since
		// we are NOT the master scatter these.
		*need_to_scatter = (true);
	}
}

/* ---------------------------------------------------------- */
static void if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task( DATA_HANDLE pdh, UNS_32 const pport, UNS_32 const pevent_name )
{
	FROM_CONTROLLER_PACKET_TOP_HEADER	*fcpth;

	RADIO_TEST_TASK_QUEUE_STRUCT		radio_test_queue_item;
	
	DATA_HANDLE							packet_copy;
	
	// ----------
	
	fcpth = (FROM_CONTROLLER_PACKET_TOP_HEADER*)pdh.dptr;
	
	if( fcpth->to_serial_number == config_c.serial_number )
	{
		if( mem_obtain_a_block_if_available( pdh.dlen, (void**)&(packet_copy.dptr) ) )
		{
			// 12/18/2017 rmd : We got the memory now fill it.
			memcpy( packet_copy.dptr, pdh.dptr, pdh.dlen );
		
			packet_copy.dlen = pdh.dlen;
			
			radio_test_queue_item.event = pevent_name;
		
			radio_test_queue_item.dh = packet_copy;

            radio_test_queue_item.from_port = pport;
			
			RADIO_TEST_post_event_with_details( &radio_test_queue_item );
		}
	}
}

/* ---------------------------------------------------------- */
extern void Route_Incoming_Packet( const UPORTS pfrom_which_port, DATA_HANDLE pdh )
{
	// 3/29/2017 rmd : This function is called within the context of the "ring buffer analysis"
	// task, as soon as a complete packet in the ring buffer has been detected . The packet is
	// delivered with the pre and post ambles stripped. And the CRC has not been checked yet.
	
	// 3/29/2017 rmd : This function is responsible for RELEASING the DATA_HANDLE memory after
	// the routing activities have been complete. So at the end of the function should see the
	// free.
	

	// ----------
	
	BOOL_32								error_detected, need_to_scatter;
	
	ROUTING_CLASS_DETAILS_STRUCT		routing;
	
	FROM_COMMSERVER_PACKET_TOP_HEADER	fcpth;
	
	TO_COMMSERVER_MESSAGE_HEADER		tcsmh;
	
	TPL_DATA_HEADER_TYPE				tplh;
	
	// ----------

	need_to_scatter = (false);

	error_detected = (false);
	
	// ----------

	if( !error_detected )
	{
		if( pfrom_which_port > UPORT_RRE )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "SERIAL: uport out of range." );

			// 4/4/2012 rmd : Prevent further analysis. Drop the packet.
			error_detected = (true);
		}
	}
	
	// ----------

	if( !error_detected )
	{
		if( CRCIsOK( pdh ) == (false) )
		{
			// 2012.03.07 rmd: The CRC is not OK. Generate an alert line if the port allows it. For
			// example, LR units can sometimes hear each other, but are actually out of range. And that
			// can generate nusiance CRC errors. We suppress the alert with a port setting.
			if( config_c.port_settings[ pfrom_which_port ].alert_about_crc_errors )
			{
				if( CONFIG_this_controller_is_a_configured_hub() )
				{
					if( pfrom_which_port == UPORT_B )
					{
						// 8/9/2018 rmd : When using an RAVEON LR RADIO, hearing about CRC errors on port B of a
						// HUB, is kind of a nuisance. It is usually traffic over heard from either another hub in
						// the city, or from controllers on another hub when they are responding to the commserver.
						// There is a place for them, and we probably need a way to turn these on and off on demand
						// with a user setting, but for now suppress as they can flood the alerts.
						if( config_c.port_B_device_index == COMM_DEVICE_LR_RAVEON )
						{
							// 8/9/2018 rmd : Do nothing.
						}
						else
						{
							// 8/9/2018 rmd : Not a Raveon LR, so flag about it.
							Alert_comm_crc_failed( pfrom_which_port );
						}
					}
					else
					{
						// 8/9/2018 rmd : On any other port besides port B let's hear about it.
						Alert_comm_crc_failed( pfrom_which_port );
					}
				}
				else
				{
					// 8/9/2018 rmd : Not a HUB so tell about it.
					Alert_comm_crc_failed( pfrom_which_port );
				}
			}

			// ----------
			
			// 12/12/2017 rmd : Signal the link test task so if it is running it can bump failed packet
			// counter.
			RADIO_TEST_post_event( RADIO_TEST_EVENT_bad_crc );
			
			// ----------
			
			// 4/4/2012 rmd : Prevent further analysis. Drop the packet.
			error_detected = (true);
		}
	}
	
	// ----------

	// 7/12/2013 rmd : Don't attempt to deduce the msg class if we have an error. Data may be
	// invalid!
	if( !error_detected )
	{
		// 7/11/2017 rmd : Packets from a master controller sent DIRECTLY to the commserver, meaning
		// given directly to the device that has established the socket to the commserver, are NOT
		// handled by this router, and are not considered 3000 routable packets.
		// 
		// ROUTABLE packets, whether originating from a 2000 or 3000, all begin with the same
		// 2-bytes, that is those first two bytes have a common definition. This would include any
		// packet that originates from a 2000, any 3000 SCAN, SCAN_RESP, TOKEN, and TOKEN_RESP. It
		// also include packets FROM the commserver to a 3000. But as we said in the first
		// paragraph, NOT packets TO the commserver.
		//
		// Since packets TO the commserver are different, for the case of a controller behind a hub,
		// packets it originates TO the commserver are encapsulated in a wrapper to make them
		// 'routable' packets, so they can pass through the hub. When the router learns they are for
		// the commserver we strip this wrapper, and send them on their way to the commserver.
		//
		// When it comes to code distribution, packets that orignate at the controller behind the
		// hub, that are for the hub itself, in response to a code distribution follow up query, are
		// routable packets.
		get_this_packets_message_class( (TPL_DATA_HEADER_TYPE*)pdh.dptr, &routing );

		// ----------

		// 3/31/2014 rmd : Stuff the various header possiblities for packet identification, relying
		// on the common packet header.
		memcpy( &tplh, pdh.dptr, sizeof(TPL_DATA_HEADER_TYPE) );
	
		memcpy( &fcpth, pdh.dptr, sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) );
	}
	
	// ----------

	if( !error_detected )
	{
		// 3/19/2012 rmd : A sanity and packet managment type test. If we are getting these types of
		// packets it means we are on a chain. And we had better be eligible to be so.
		// 
		// 2/12/2014 ajv : Unfortunately, this results in all of the alerts being rolled out. We
		// should enhance this to only stamp perhaps each time a scan packet is received as only
		// once per day for tokens? Requires some thought.
		if( routing.routing_class_base == ROUTING_CLASS_IS_3000_BASED )
		{
			if( (routing.rclass == MSG_CLASS_CS3000_SCAN) || (routing.rclass == MSG_CLASS_CS3000_TOKEN) || (routing.rclass == MSG_CLASS_CS3000_SCAN_RESP) || (routing.rclass == MSG_CLASS_CS3000_TOKEN_RESP) )
			{
				if( FLOWSENSE_we_are_poafs() == (false) )
				{
					if( config_c.purchased_options.option_FL == (false) )
					{
						Alert_token_rcvd_with_no_FL();
					}
					else
					{
						Alert_token_rcvd_with_FL_turned_off();
					}
	
					// 4/4/2012 rmd : Prevent further analysis. Drop the packet.
					error_detected = (true);
				}
			}
		}
	}
	
	// ----------

	if( !error_detected )
	{
		// 3/19/2012 rmd : Another sanity check here. To see that packets are not flying all over
		// the network out of control. If we are the master we should never see TOKEN or SCAN
		// packets as incoming packets. If so that would mean they are making full circle, sort of
		// speak, and being sent back unecessairly to where they originated from.
		if( routing.routing_class_base == ROUTING_CLASS_IS_3000_BASED )
		{
			if( (routing.rclass == MSG_CLASS_CS3000_SCAN) || (routing.rclass == MSG_CLASS_CS3000_TOKEN) )
			{
				if( FLOWSENSE_we_are_the_master_of_a_multiple_controller_network() )
				{
					// 3/19/2012 rmd : Shouldn't happen. Alert about this. And don't check if they are to us.
					// And prevent any further scattering. I'd imagine they would come back again!
					Alert_token_rcvd_by_FL_master();
	
					// 4/4/2012 rmd : Prevent further analysis. Drop the packet.
					error_detected = (true);
				}
			}
		}
	}
	
	// ----------

	// 4/4/2012 rmd : If no errors proceed.
	if( !error_detected )
	{
		// 2/10/2017 rmd : Split up the router into 3 independent handling sections. This will lead
		// to some duplicate code but a much higher level of maintainability! Can change behavior
		// for one type of controller without affecting the others. So, one set of rules for the
		// hub, one for controllers on a hub, and one set for non-hub based controllers.
		//
		// 4/13/2017 rmd : Pull out one exception, for packets that arrived on the RRE port. They
		// should ONLY be from the firmware upgrade program. And regardless of all other controller
		// option always handled the same.
		if( pfrom_which_port == UPORT_RRE )
		{
			if( (routing.routing_class_base == ROUTING_CLASS_IS_3000_BASED) && (routing.rclass == MSG_CLASS_CS3000__FROM_COMMSERVER__CODE_DISTRIBUTION) )
			{
				// 2/14/2017 rmd : If we are getting code packets through the rre port accept them as ours.
				// That way the 'firmware upgrade' program operator needn't worry about the NID or Serial
				// Number when using the Firmware Upgrade program. Forward a copy of the packet to the code
				// distribution task, to be post processed (meaning processed when that task runs).
				CODE_DISTRIBUTION_make_a_copy_and_queue_incoming_packet( pfrom_which_port, pdh, routing.rclass );
			}
			else
			{
				// 2/10/2017 rmd : This is an error. Should only receive code packets via the rre port. Drop
				// the packet.
				Alert_router_rcvd_unexp_class( ROUTER_TYPE_RRE, pfrom_which_port, routing.routing_class_base, routing.rclass );
			}
		}
		else
		if( CONFIG_this_controller_is_a_configured_hub() )
		{
			if( pfrom_which_port == UPORT_A )
			{
				// 4/13/2017 rmd : PACKET HANDLING FOR A HUB
				
				// 2/10/2017 rmd : The only packets we should see from Port A are packets DIRECTLY from the
				// commserver. Being a hub they could be for us or for another controller. And they could
				// also be 2000 packets.
				if( routing.routing_class_base == ROUTING_CLASS_IS_2000_BASED )
				{
					if( routing.rclass == MSG_CLASS_2000e_FROM_CENTRAL )
					{
						#if SHOW_PACKET_ROUTER_ALERTS
							// 4/13/2017 rmd : This is not an error. Just an FYI for now. To be commented out later.
							Alert_hub_rcvd_packet( ROUTER_TYPE_HUB, pfrom_which_port, routing.routing_class_base, pdh.dlen );
						#endif	

						// ----------
						
						// 5/10/2017 rmd : A qualifying packet demonstrating activity from the socket.
						CONTROLLER_INITIATED_post_event( CI_EVENT_restart_hub_packet_activity_timer );

						// ----------
						
						// 3/15/2017 rmd : There is only one thing to do with packets from the central for a 2000.
						// BUT, don't do this if we are in device exchange mode. We could be performing a device
						// programming on the B port device, and the A port is still able to receive packets from
						// the commserver. But in the event a packet does arrive, we must not forward it to the B
						// port, that will disrupt the device programming.
						if( !in_device_exchange_hammer )
						{
							// 8/17/2018 rmd : This test is good to keep in place. We should NEVER see this alert. If we
							// do it means either the hub list in the controller has been destroyed, or the commserver
							// is using the wrong hub for this controller. The commserver using the wrong hub could be
							// caused by a number of issues ranging from a flat out bug, to after a user made a hub
							// assignment change the commserver failed to update the hub list in the controller, to
							// more. I've decided not to be on the conservative side here, and to NOT drop the packet in
							// this case. We'll still forward it, but certainly alert about this condition. NOTE: if the
							// controller receives this packet, and responds, those returning packets will also be found
							// to not be on the hub list and WILL be dropped.
							if( !this_2000_packet_from_the_commserver_is_on_the_hub_list( &tplh ) )
							{
								Alert_Message_va( "HUB: 2000 from commserver, not on list, to addr %c%c%c", tplh.H.FromTo.to[0], tplh.H.FromTo.to[1], tplh.H.FromTo.to[2] );
							}
							
							// ----------
							
							// 8/15/2018 rmd : And forward it regardless if on hub list or not.
							attempt_to_copy_packet_and_route_out_another_port( UPORT_B, pdh, NULL );
						}
					}
					else
					{
						// 2/10/2017 rmd : This is an error. The packet should be dropped.
						Alert_router_rcvd_unexp_class( ROUTER_TYPE_HUB, pfrom_which_port, routing.routing_class_base, routing.rclass );
					}
				}
				else
				if( routing.routing_class_base == ROUTING_CLASS_IS_3000_BASED )
				{
					if( routing.rclass == MSG_CLASS_CS3000_FROM_COMMSERVER )
					{
						// 5/10/2017 rmd : A qualifying packet demonstrating activity from the socket.
						CONTROLLER_INITIATED_post_event( CI_EVENT_restart_hub_packet_activity_timer );

						// ----------
						
						if( fcpth.to_serial_number == config_c.serial_number )
						{
							// 2/10/2017 rmd : Then the packet is for us - the hub. Processed inline, within the context
							// of this task, the RING_BUFFER_ANALYSIS task.
							CENT_COMM_process_incoming_packet_from_comm_server( pfrom_which_port, pdh, routing.rclass );
						}
						else
						{
							#if SHOW_PACKET_ROUTER_ALERTS
								Alert_hub_forwarding_packet( ROUTER_TYPE_HUB, pfrom_which_port, routing.routing_class_base, pdh.dlen );
							#endif	
							
							// 11/16/2018 rmd : So here we are a hub, and packets are coming from the commserver, for
							// another controller. We used to check if the HUB had a code distribution to do, and if so
							// drop this packet. But now that the HUB code distribution can be pending until the start
							// command arrives, let's just do as normal, and forward this packet out the B port.
							//
							// 2/10/2017 rmd : The packet is to be forwarded out the hub device on port B. That's the
							// only place it can go. BUT, don't do this if we are in device exchange mode. We could be
							// performing a device programming on the B port device, and the A port is still able to
							// receive packets from the commserver. But in the event a packet does arrive, we must not
							// forward it to the B port, that will likely disrupt the device programming.
							if( !in_device_exchange_hammer )
							{
								// 8/17/2018 rmd : This test is good to keep in place. We should NEVER see this alert. If we
								// do it means either the hub list in the controller has been destroyed, or the commserver
								// is using the wrong hub for this controller. The commserver using the wrong hub could be
								// caused by a number of issues ranging from a flat out bug, to after a user made a hub
								// assignment change the commserver failed to update the hub list in the controller, to
								// more. I've decided to be on the conservative side here, and to NOT drop the packet in
								// this case. We'll still forward it, but certainly alert about this condition. NOTE: if the
								// controller receives this packet, and responds, those returning packets will also be found
								// to not be on the hub list and WILL be dropped.
								if( !this_3000_packet_from_the_commserver_is_on_the_hub_list( &fcpth ) )
								{
									Alert_Message_va( "HUB: 3000 from commserver, not on list, to sn %u", fcpth.to_serial_number );
								}
								
								// ----------
								
								// 8/15/2018 rmd : And forward it regardless if on hub list or not.
								attempt_to_copy_packet_and_route_out_another_port( UPORT_B, pdh, NULL );
							}
						}
					}
					else
					if( routing.rclass == MSG_CLASS_CS3000__FROM_COMMSERVER__CODE_DISTRIBUTION )
					{
						if( fcpth.to_serial_number == config_c.serial_number )
						{
							// 5/10/2017 rmd : A qualifying packet demonstrating activity from the socket.
							CONTROLLER_INITIATED_post_event( CI_EVENT_restart_hub_packet_activity_timer );
	
							// ----------
							
							// 6/7/2017 rmd : Forward a copy of the packet to the code distribution task, to be post
							// processed (meaning processed when that task runs).
							CODE_DISTRIBUTION_make_a_copy_and_queue_incoming_packet( pfrom_which_port, pdh, routing.rclass );
						}
						else
						{
							// 2/10/2017 rmd : This is an error. The packet should be dropped.
							Alert_router_rcvd_packet_not_for_us( ROUTER_TYPE_HUB, pfrom_which_port, routing.routing_class_base );
						}
					}
					else
					{
						// 2/10/2017 rmd : This is an error. The packet should be dropped.
						Alert_router_rcvd_unexp_class( ROUTER_TYPE_HUB, pfrom_which_port, routing.routing_class_base, routing.rclass );
					}
				}
				else
				{
					Alert_router_rcvd_unexp_base_class( ROUTER_TYPE_HUB, pfrom_which_port, routing.routing_class_base );
				}
			}
			else
			if( pfrom_which_port == UPORT_B )
			{
				// 4/13/2017 rmd : PACKET HANDLING FOR A HUB
				
				// 3/15/2017 rmd : The only packets we should see here are from other controllers headed
				// back to the central.
				if( routing.routing_class_base == ROUTING_CLASS_IS_2000_BASED )
				{
					if( routing.rclass == MSG_CLASS_2000e_TO_CENTRAL )
					{
						#if SHOW_PACKET_ROUTER_ALERTS
							// 4/13/2017 rmd : This is not an error. Just an FYI for now. To be commented out later.
							Alert_hub_rcvd_packet( ROUTER_TYPE_HUB, pfrom_which_port, routing.routing_class_base, pdh.dlen );
						#endif
						
						// 4/30/2017 rmd : Is this FROM a controller on our hub list?
						if( this_2000_packet_is_from_a_controller_on_the_hub_list( &tplh ) )
						{
							// 3/15/2017 rmd : There is only one thing to do with packets from the central for a 2000.
							// Send to port A.
							attempt_to_copy_packet_and_route_out_another_port( UPORT_A, pdh, NULL );
						}
						else
						{
							#if SHOW_PACKET_ROUTER_NOT_ON_HUB_LIST_ALERTS
								// 5/1/2017 rmd : Mean this hub is overhearing other controllers not assigned to this hub.
								Alert_router_rcvd_packet_not_on_hub_list_with_sn( ROUTER_TYPE_HUB, pfrom_which_port, routing.routing_class_base, tcsmh.from_serial_number );
							#endif
						}
					}
					else
					if( (routing.rclass == MSG_CLASS_2000e_TEST_PACKET_FROM_C_OR_C) || (routing.rclass == MSG_CLASS_2000e_TEST_PACKET_ECHO_TO_C_OR_C) )
					{
						#if SHOW_PACKET_ROUTER_ALERTS
							// 4/13/2017 rmd : This is not an error. Just an FYI for now. To be commented out later.
							Alert_Message_va( "Hub ROUTER: 2000 test packet in port B %u bytes", pdh.dlen );
						#endif
						
						// 11/27/2017 rmd : If the packet is on our hub list we will echo it if its TO address is
						// "HUB".
						if( this_2000_packet_is_from_a_controller_on_the_hub_list( &tplh )  )
						{
							// 11/27/2017 rmd : So this packet is on our hub list, check the TO address to be the  
							// special HUB address and if so then echo the packet back to where it came from.
							RADIO_TEST_if_addressed_to_the_hub_pass_packet_on_to_the_inbound_transport( UPORT_B, pdh );
						}
						else
						{
							// 3/20/2018 rmd : I almost left this alert showing. It might be useful during radio test to
							// see which HUBS can hear a controller.
							#if SHOW_PACKET_ROUTER_NOT_ON_HUB_LIST_ALERTS
								// 5/1/2017 rmd : Mean this hub is overhearing other controllers not assigned to this hub.
								Alert_router_rcvd_packet_not_on_hub_list( ROUTER_TYPE_HUB, pfrom_which_port, routing.routing_class_base );
							#endif
						}
					}
					else
					{
						// 2/10/2017 rmd : This can happen if our hub, overhears another hub, sending a packet to a
						// controller (packet class 0). The packet should be dropped.
						#if SHOW_PACKET_ROUTER_hubs_overhearing_one_another
							Alert_router_rcvd_unexp_class( ROUTER_TYPE_HUB, pfrom_which_port, routing.routing_class_base, routing.rclass );
						#endif
					}
				}
				else
				if( routing.routing_class_base == ROUTING_CLASS_IS_3000_BASED )
				{
					if( routing.rclass == MSG_CLASS_CS3000_TO_COMMSERVER_VIA_HUB )
					{ 
						#if SHOW_PACKET_ROUTER_ALERTS
							Alert_hub_rcvd_packet( ROUTER_TYPE_HUB, pfrom_which_port, routing.routing_class_base, pdh.dlen );
						#endif	
	
						// 3/21/2017 rmd : This packet came from a 3000 that, was within range of, or connected to
						// this hub, in some manner. Such a packet can only arrive to a hub on port B. From any
						// other port is an error. We need to strip the special routing header, recalculate the crc,
						// and send on to the commserver.
						strip_hub_routing_header_from_packet( &pdh );
						
						// 6/19/2017 rmd : Load the header structure, now that we have removed the packet wrapper.
						memcpy( &tcsmh, pdh.dptr, sizeof(TO_COMMSERVER_MESSAGE_HEADER) );

						// 4/30/2017 rmd : Is this from a controller on our hub list?
						if( this_3000_packet_to_the_commserver_is_on_the_hub_list( &tcsmh ) )
						{
							attempt_to_copy_packet_and_route_out_another_port( UPORT_A, pdh, NULL );
						}
						else
						{
							#if SHOW_PACKET_ROUTER_NOT_ON_HUB_LIST_ALERTS
								Alert_router_rcvd_packet_not_on_hub_list_with_sn( ROUTER_TYPE_HUB, pfrom_which_port, routing.routing_class_base, tcsmh.from_serial_number );
							#endif
						}
					}
					else
					if( routing.rclass == MSG_CLASS_CS3000__TO_HUB__CODE_DISTRIBUTION_ADDRESSABLE )
					{
						// 7/12/2017 rmd : These packets have a TO address, and we should check that it is ours. The
						// packet coming from the controller behind the hub builds the packet starting with the FROM
						// COMMSERVER PACKET HEADER.
						if( fcpth.to_serial_number == config_c.serial_number )
						{
							// 7/12/2017 rmd : Okay it is for use, give to the code distribution task.
							CODE_DISTRIBUTION_make_a_copy_and_queue_incoming_packet( pfrom_which_port, pdh, routing.rclass );
						}
					}
					else
					if( routing.rclass == MSG_CLASS_CS3000__TO_CONTROLLER__TEST_PACKET )
					{
                        // 1/16/2018 Ryan : This would be if another controller originated the test. We are going to
                        // get a copy of the message because the message memory is freed at the end of this function
                        // and we want to make sure the content of message is intact when the radio test task
                        // processes it. We include the port and the event in the parameters so that the function
                        // has the information required to send the message to the right case in the radio test task
                        // and to echo the message back to the controller of origin.
            			if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task( pdh, pfrom_which_port, RADIO_TEST_EVENT_here_is_an_inbound_message_to_echo );
					}
					else
					if( routing.rclass == MSG_CLASS_CS3000__TO_CONTROLLER__TEST_PACKET_ECHO )
					{
                        // 12/18/2017 rmd : This would be if we originated the test. MAKE SURE WE GET A COPY OF THE
                        // MESSAGE as the message memory is freed at the end of this function, and we are only
                        // handing this message memory over to another task for subsequent processing. Depending on
                        // the task priority we do not know if the message memory has been freed or not by the time
                        // we get around to processing the message in the radio_test task.

                        // 1/16/2018 Ryan : We include the event and the port in the parameters now, so the function
                        // can also be used to echo packets, as well as recieve echoed packages, depending on the
                        // event.
						if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task( pdh, pfrom_which_port, RADIO_TEST_EVENT_here_is_an_inbound_echo_to_process );
					}
					else
					{
						// 5/2/2017 rmd : Packet class 2, this happens when this hub overhears another hub
						// forwarding the packets it received from the commserver.
						#if SHOW_PACKET_ROUTER_hubs_overhearing_one_another
							Alert_Message_va( "Hub Router: port B dropped 3000 packet, class %u", routing.rclass );
						#endif
					}
				}
				else
				{
					Alert_router_rcvd_unexp_base_class( ROUTER_TYPE_HUB, pfrom_which_port, routing.routing_class_base );
				}
			}
			else	
			if( pfrom_which_port == UPORT_TP )
			{
				// 4/13/2017 rmd : PACKET HANDLING FOR A HUB
				
				if( routing.routing_class_base == ROUTING_CLASS_IS_2000_BASED )
				{
					Alert_router_rcvd_unexp_class( ROUTER_TYPE_HUB, pfrom_which_port, routing.routing_class_base, routing.rclass );
				}
				else
				if( routing.routing_class_base == ROUTING_CLASS_IS_3000_BASED )
				{
					if( routing.rclass == MSG_CLASS_CS3000_FROM_TP_MICRO )
					{
						if( this_communication_address_is_our_serial_number( tplh.H.FromTo.to, THE_PASSED_IN_DATA_IS_BIG_ENDIAN ) )
						{
							// 11/15/2012 rmd : The packet length still includes the CRC. Since it is not of any use
							// when parsing the message, remove it by reducing the length.
							pdh.dlen -= sizeof(CRC_BASE_TYPE);
							
							TPMICRO_make_a_copy_and_queue_incoming_packet( pdh );
						}
						else
						{
							Alert_router_unexp_to_addr_port( ROUTER_TYPE_HUB, pfrom_which_port );
						}
					}
					else
					if( (routing.rclass == MSG_CLASS_CS3000_TOKEN_RESP) || (routing.rclass == MSG_CLASS_CS3000_SCAN_RESP) )
					{
						// 3/12/2012 rmd : The SCAN_RESP messages are always true serial number based packet.
						if( this_communication_address_is_our_serial_number( tplh.H.FromTo.to, THE_PASSED_IN_DATA_IS_BIG_ENDIAN ) )
						{
							TPL_IN_make_a_copy_and_direct_incoming_packet( pfrom_which_port, pdh );
						}
						else
						{
							// 2/14/2017 rmd : The hub is the MASTER controller therefore these scan resp and token resp
							// packets had better be for us. If not this as an error.
							Alert_router_received_unexp_token_resp( ROUTER_TYPE_HUB, pfrom_which_port );
						}
					}
					else
					{
						// 2/10/2017 rmd : This is an error. The packet should be dropped.
						Alert_router_rcvd_unexp_class( ROUTER_TYPE_HUB, pfrom_which_port, routing.routing_class_base, routing.rclass );
					}
				}
				else
				{
					Alert_router_rcvd_unexp_base_class( ROUTER_TYPE_HUB, pfrom_which_port, routing.routing_class_base );
				}
			}
			else
			{
				// 2/10/2017 rmd : This is an error. The packet should be dropped.
				Alert_router_unk_port( ROUTER_TYPE_HUB );
			}
		}
		else
		if( CONFIG_this_controller_is_behind_a_hub() )
		{
			if( pfrom_which_port == UPORT_A )
			{
				// 4/13/2017 rmd : PACKET HANDLING FOR CONTROLLERS ON A HUB
				
				// 2/10/2017 rmd : The only packets we should see from Port A are from_commserver packets
				// that have been forwarded to us by the hub. With us hanging on a hub, and such packets
				// broadcast, they could be for us, or for another controller. And they could also be 2000
				// packets.
				if( routing.routing_class_base == ROUTING_CLASS_IS_2000_BASED )
				{
					// 4/13/2017 rmd : For controllers on a hub, if there is also a 2000 behind this hub, we
					// would see that traffic too. But should only see the packets TO and FROM the
					// commserver.
					if( (routing.rclass == MSG_CLASS_2000e_FROM_CENTRAL) || (routing.rclass == MSG_CLASS_2000e_TO_CENTRAL) )
					{
						#if SHOW_PACKET_ROUTER_ALERTS
							Alert_Message( "On a Hub Router: dropping 2000 packet in port A (not an error)" );
						#endif	
					}
					else
					{
						// 4/13/2017 rmd : Unexpected. Why did we see this class type?
						Alert_router_rcvd_unexp_class( ROUTER_TYPE_BEHIND_HUB, pfrom_which_port, routing.routing_class_base, routing.rclass );
					}
				}
				else
				if( routing.routing_class_base == ROUTING_CLASS_IS_3000_BASED )
				{
					if( routing.rclass == MSG_CLASS_CS3000_FROM_COMMSERVER )
					{
						if( fcpth.to_serial_number == config_c.serial_number )
						{
							// 2/10/2017 rmd : Then the packet is for us - a controller BEHIND the hub. Processed
							// inline, within the context of this task, the RING_BUFFER_ANALYSIS task.
							CENT_COMM_process_incoming_packet_from_comm_server( pfrom_which_port, pdh, routing.rclass );
						}
						else
						{
							#if SHOW_PACKET_ROUTER_ALERTS
								Alert_router_rcvd_packet_not_for_us( ROUTER_TYPE_BEHIND_HUB, pfrom_which_port, routing.routing_class_base );
							#endif
						}
					}
					else
					if( routing.rclass == MSG_CLASS_CS3000__FROM_HUB__CODE_DISTRIBUTION_BROADCAST )
					{
						// 6/6/2017 rmd : These packets are broadcast and we should always accept them for
						// processing. Forward a copy of the packet to the code distribution task, to be post
						// processed (meaning processed when that task runs).
						CODE_DISTRIBUTION_make_a_copy_and_queue_incoming_packet( pfrom_which_port, pdh, routing.rclass );
					}
					else
					if( routing.rclass == MSG_CLASS_CS3000__FROM_HUB__CODE_DISTRIBUTION_ADDRESSABLE )
					{
						// 7/12/2017 rmd : Check that this packet is to us. It is the addresable class.
						if( fcpth.to_serial_number == config_c.serial_number )
						{
							// 7/12/2017 rmd : Give to the code distribution task.
							CODE_DISTRIBUTION_make_a_copy_and_queue_incoming_packet( pfrom_which_port, pdh, routing.rclass );
						}
					}
					else
					if( routing.rclass == MSG_CLASS_CS3000_TO_COMMSERVER_VIA_HUB )
					{
						// 4/13/2017 rmd : In an LR system we could see the packets transmitted from other
						// controllers TO THE COMMSERVER.
						#if SHOW_PACKET_ROUTER_ALERTS
							Alert_Message( "On a Hub Router: dropping 3000 packet TO_CENTRAL in port A (not an error)" );
						#endif
					}
					else
					if( routing.rclass == MSG_CLASS_CS3000__TO_CONTROLLER__TEST_PACKET )
					{
                        // 1/16/2018 Ryan : This would be if another controller originated the test. We are going to
                        // get a copy of the message because the message memory is freed at the end of this function
                        // and we want to make sure the content of message is intact when the radio test task
                        // processes it. We include the port and the event in the parameters so that the function
                        // has the information required to send the message to the right case in the radio test task
                        // and to echo the message back to the controller of origin.
            			if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task( pdh, pfrom_which_port, RADIO_TEST_EVENT_here_is_an_inbound_message_to_echo );
					}
					else
					if( routing.rclass == MSG_CLASS_CS3000__TO_CONTROLLER__TEST_PACKET_ECHO )
					{
                        // 12/18/2017 rmd : This would be if we originated the test. MAKE SURE WE GET A COPY OF THE
                        // MESSAGE as the message memory is freed at the end of this function, and we are only
                        // handing this message memory over to another task for subsequent processing. Depending on
                        // the task priority we do not know if the message memory has been freed or not by the time
                        // we get around to processing the message in the radio_test task.

                        // 1/16/2018 Ryan : We include the event and the port in the parameters now, so the function
                        // can also be used to echo packets, as well as recieve echoed packages, depending on the
                        // event.
						if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task( pdh, pfrom_which_port, RADIO_TEST_EVENT_here_is_an_inbound_echo_to_process );
					}
					else
					{
						// 2/10/2017 rmd : This is an unexpected. The packet should be dropped.
						Alert_router_rcvd_unexp_class( ROUTER_TYPE_BEHIND_HUB, pfrom_which_port, routing.routing_class_base, routing.rclass );
					}
				}
				else
				{
					Alert_router_rcvd_unexp_base_class( ROUTER_TYPE_BEHIND_HUB, pfrom_which_port, routing.routing_class_base );
				}
			}
			else
			if( pfrom_which_port == UPORT_B )
			{
				// 4/13/2017 rmd : PACKET HANDLING FOR CONTROLLERS ON A HUB
				
				// 4/13/2017 rmd : For controllers that flag as 'being on a hub', they are a MASTER, and
				// therefore the only thing coming in Port B should be scan resp or token resp.
				if( routing.routing_class_base == ROUTING_CLASS_IS_2000_BASED )
				{
					// 2/10/2017 rmd : This is an error. The packet should be dropped.
					Alert_router_rcvd_unexp_class( ROUTER_TYPE_BEHIND_HUB, pfrom_which_port, routing.routing_class_base, routing.rclass );
				}
				else
				if( routing.routing_class_base == ROUTING_CLASS_IS_3000_BASED )
				{
					if( (routing.rclass == MSG_CLASS_CS3000_TOKEN_RESP) || (routing.rclass == MSG_CLASS_CS3000_SCAN_RESP) )
					{
						// 3/12/2012 rmd : The SCAN_RESP messages are always true serial number based packet.
						if( this_communication_address_is_our_serial_number( tplh.H.FromTo.to, THE_PASSED_IN_DATA_IS_BIG_ENDIAN ) )
						{
							TPL_IN_make_a_copy_and_direct_incoming_packet( pfrom_which_port, pdh );
						}
						else
						{
							// 2/14/2017 rmd : The hub is the MASTER controller therefore these scan resp and token resp
							// packets had better be for us. If not this as an error.
							Alert_router_received_unexp_token_resp( ROUTER_TYPE_BEHIND_HUB, pfrom_which_port );
						}
					}
					else if( routing.rclass == MSG_CLASS_CS3000__TO_CONTROLLER__TEST_PACKET )
					{
                        // 1/16/2018 Ryan : This would be if another controller originated the test. We are going to
                        // get a copy of the message because the message memory is freed at the end of this function
                        // and we want to make sure the content of message is intact when the radio test task
                        // processes it. We include the port and the event in the parameters so that the function
                        // has the information required to send the message to the right case in the radio test task
                        // and to echo the message back to the controller of origin.
            			if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task( pdh, pfrom_which_port, RADIO_TEST_EVENT_here_is_an_inbound_message_to_echo );
					}
					else if( routing.rclass == MSG_CLASS_CS3000__TO_CONTROLLER__TEST_PACKET_ECHO )
					{
                        // 12/18/2017 rmd : This would be if we originated the test. MAKE SURE WE GET A COPY OF THE
                        // MESSAGE as the message memory is freed at the end of this function, and we are only
                        // handing this message memory over to another task for subsequent processing. Depending on
                        // the task priority we do not know if the message memory has been freed or not by the time
                        // we get around to processing the message in the radio_test task.

                        // 1/16/2018 Ryan : We include the event and the port in the parameters now, so the function
                        // can also be used to echo packets, as well as recieve echoed packages, depending on the
                        // event.
						if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task( pdh, pfrom_which_port, RADIO_TEST_EVENT_here_is_an_inbound_echo_to_process );
					}
					else
					{
						Alert_router_rcvd_unexp_class( ROUTER_TYPE_BEHIND_HUB, pfrom_which_port, routing.routing_class_base, routing.rclass );
					}
				}
				else
				{
					Alert_router_rcvd_unexp_base_class( ROUTER_TYPE_BEHIND_HUB, pfrom_which_port, routing.routing_class_base );
				}
			}
			else	
			if( pfrom_which_port == UPORT_TP )
			{
				// 4/13/2017 rmd : PACKET HANDLING FOR CONTROLLERS ON A HUB
				
				if( routing.routing_class_base == ROUTING_CLASS_IS_2000_BASED )
				{
					Alert_router_rcvd_unexp_class( ROUTER_TYPE_BEHIND_HUB, pfrom_which_port, routing.routing_class_base, routing.rclass );
				}
				else
				if( routing.routing_class_base == ROUTING_CLASS_IS_3000_BASED )
				{
					if( routing.rclass == MSG_CLASS_CS3000_FROM_TP_MICRO )
					{
						if( this_communication_address_is_our_serial_number( tplh.H.FromTo.to, THE_PASSED_IN_DATA_IS_BIG_ENDIAN ) )
						{
							// 11/15/2012 rmd : The packet length still includes the CRC. Since it is not of any use
							// when parsing the message, remove it by reducing the length.
							pdh.dlen -= sizeof(CRC_BASE_TYPE);
							
							TPMICRO_make_a_copy_and_queue_incoming_packet( pdh );
						}
						else
						{
							Alert_router_unexp_to_addr_port( ROUTER_TYPE_BEHIND_HUB, pfrom_which_port );
						}
					}
					else
					if( (routing.rclass == MSG_CLASS_CS3000_TOKEN_RESP) || (routing.rclass == MSG_CLASS_CS3000_SCAN_RESP) )
					{
						// 3/12/2012 rmd : The SCAN_RESP messages are always true serial number based packet.
						if( this_communication_address_is_our_serial_number( tplh.H.FromTo.to, THE_PASSED_IN_DATA_IS_BIG_ENDIAN ) )
						{
							TPL_IN_make_a_copy_and_direct_incoming_packet( pfrom_which_port, pdh );
						}
						else
						{
							// 2/14/2017 rmd : The controller behind the HUB is the MASTER, therefore these scan resp
							// and token resp packets had better be for us. If not this as an error.
							Alert_router_received_unexp_token_resp( ROUTER_TYPE_BEHIND_HUB, pfrom_which_port );
						}
					}
					else
					{
						// 2/10/2017 rmd : This is an error. The packet should be dropped.
						Alert_router_rcvd_unexp_class( ROUTER_TYPE_BEHIND_HUB, pfrom_which_port, routing.routing_class_base, routing.rclass );
					}
				}
				else
				{
					Alert_router_rcvd_unexp_base_class( ROUTER_TYPE_BEHIND_HUB, pfrom_which_port, routing.routing_class_base );
				}
			}
			else
			{
				// 2/10/2017 rmd : This is an error. The packet should be dropped.
				Alert_router_unk_port( ROUTER_TYPE_BEHIND_HUB );
			}
		}
		else
		if( FLOWSENSE_we_are_a_master_one_way_or_another() )
		{
			// 4/13/2017 rmd : PACKET HANDLING FOR NON-HUB BASED MASTERS.

			// 4/10/2017 rmd : Non hub controllers should NEVER receive a packet of 2000 based routing
			// class. So screen for that up front.
			if( routing.routing_class_base == ROUTING_CLASS_IS_2000_BASED )
			{
				Alert_Message_va( "non-hub master ROUTER: 3000 rcvd 2000 packet class %u, port %s, len %u", routing.rclass, port_names[ pfrom_which_port ], pdh.dlen );
			}
			else
			if( routing.routing_class_base == ROUTING_CLASS_IS_3000_BASED )
			{
				if( pfrom_which_port == UPORT_A )
				{
					// 4/13/2017 rmd : PACKET HANDLING FOR NON-HUB BASED MASTERS.
					
					if( routing.rclass == MSG_CLASS_CS3000_FROM_COMMSERVER )
					{
						if( fcpth.to_serial_number == config_c.serial_number )
						{
							// 2/10/2017 rmd : Then the packet is for us - a regular, non hub-based, master controller.
							// Processed inline, within the context of this task, the RING_BUFFER_ANALYSIS task.
							CENT_COMM_process_incoming_packet_from_comm_server( pfrom_which_port, pdh, routing.rclass );
						}
						else
						{
							Alert_Message( "non-hub master ROUTER: port A dropping 3000 packet FROM_CENTRAL (error)" );
						}
					}
					else if( routing.rclass == MSG_CLASS_CS3000__FROM_COMMSERVER__CODE_DISTRIBUTION )
					{
						if( fcpth.to_serial_number == config_c.serial_number )
						{
							// 2/10/2017 rmd : Then the packet is for us - a regular, non hub-based, master controller,
							// and we are receiving a binary through our central device on port A. Forward a copy of the
							// packet to the code distribution task, to be post processed (meaning processed when that
							// task runs).
							CODE_DISTRIBUTION_make_a_copy_and_queue_incoming_packet( pfrom_which_port, pdh, routing.rclass );
						}
						else
						{
							// 2/10/2017 rmd : This is an error. The packet should be dropped.
							Alert_Message( "non-hub master ROUTER: port A dropping 3000 code packet (error)" );
						}
					}
					else if( routing.rclass == MSG_CLASS_CS3000__TO_CONTROLLER__TEST_PACKET )
					{
                        // 1/16/2018 Ryan : This would be if another controller originated the test. We are going to
                        // get a copy of the message because the message memory is freed at the end of this function
                        // and we want to make sure the content of message is intact when the radio test task
                        // processes it. We include the port and the event in the parameters so that the function
                        // has the information required to send the message to the right case in the radio test task
                        // and to echo the message back to the controller of origin.
            			if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task( pdh, pfrom_which_port, RADIO_TEST_EVENT_here_is_an_inbound_message_to_echo );
					}
					else if( routing.rclass == MSG_CLASS_CS3000__TO_CONTROLLER__TEST_PACKET_ECHO )
					{
                        // 12/18/2017 rmd : This would be if we originated the test. MAKE SURE WE GET A COPY OF THE
                        // MESSAGE as the message memory is freed at the end of this function, and we are only
                        // handing this message memory over to another task for subsequent processing. Depending on
                        // the task priority we do not know if the message memory has been freed or not by the time
                        // we get around to processing the message in the radio_test task.

                        // 1/16/2018 Ryan : We include the event and the port in the parameters now, so the function
                        // can also be used to echo packets, as well as recieve echoed packages, depending on the
                        // event.
						if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task( pdh, pfrom_which_port, RADIO_TEST_EVENT_here_is_an_inbound_echo_to_process );
					}
					else
					{
						// 2/10/2017 rmd : This is an unexpected. The packet should be dropped.
						Alert_router_rcvd_unexp_class( ROUTER_TYPE_FL_MASTER, pfrom_which_port, routing.routing_class_base, routing.rclass ); 
					}
				}
				else
				if( pfrom_which_port == UPORT_B )
				{
					// 4/13/2017 rmd : PACKET HANDLING FOR NON-HUB BASED MASTERS.
					
					if( (routing.rclass == MSG_CLASS_CS3000_TOKEN_RESP) || (routing.rclass == MSG_CLASS_CS3000_SCAN_RESP) )
					{
						// 3/12/2012 rmd : The SCAN_RESP messages are always true serial number based packet.
						if( this_communication_address_is_our_serial_number( tplh.H.FromTo.to, THE_PASSED_IN_DATA_IS_BIG_ENDIAN ) )
						{
							TPL_IN_make_a_copy_and_direct_incoming_packet( pfrom_which_port, pdh );
						}
						else
						{
							// 2/14/2017 rmd : As a MASTER these scan resp and token resp packets had better be for us.
							// If not, they've reached the end of the line, and this as an error.
							Alert_router_received_unexp_token_resp( ROUTER_TYPE_FL_MASTER, pfrom_which_port );
						}
					}
					else
					if( routing.rclass == MSG_CLASS_CS3000__TO_CONTROLLER__TEST_PACKET )
					{
                        // 1/16/2018 Ryan : This would be if another controller originated the test. We are going to
                        // get a copy of the message because the message memory is freed at the end of this function
                        // and we want to make sure the content of message is intact when the radio test task
                        // processes it. We include the port and the event in the parameters so that the function
                        // has the information required to send the message to the right case in the radio test task
                        // and to echo the message back to the controller of origin.
            			if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task( pdh, pfrom_which_port, RADIO_TEST_EVENT_here_is_an_inbound_message_to_echo );
					}
					else
					if( routing.rclass == MSG_CLASS_CS3000__TO_CONTROLLER__TEST_PACKET_ECHO )
					{
                        // 12/18/2017 rmd : This would be if we originated the test. MAKE SURE WE GET A COPY OF THE
                        // MESSAGE as the message memory is freed at the end of this function, and we are only
                        // handing this message memory over to another task for subsequent processing. Depending on
                        // the task priority we do not know if the message memory has been freed or not by the time
                        // we get around to processing the message in the radio_test task.

                        // 1/16/2018 Ryan : We include the event and the port in the parameters now, so the function
                        // can also be used to echo packets, as well as recieve echoed packages, depending on the
                        // event.
						if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task( pdh, pfrom_which_port, RADIO_TEST_EVENT_here_is_an_inbound_echo_to_process );
					}
					else
					{
						Alert_router_rcvd_unexp_class( ROUTER_TYPE_FL_MASTER, pfrom_which_port, routing.routing_class_base, routing.rclass );
					}
				}
				else	
				if( pfrom_which_port == UPORT_TP )
				{
					// 4/13/2017 rmd : PACKET HANDLING FOR NON-HUB BASED MASTERS.
					
					if( routing.rclass == MSG_CLASS_CS3000_FROM_TP_MICRO )
					{
						if( this_communication_address_is_our_serial_number( tplh.H.FromTo.to, THE_PASSED_IN_DATA_IS_BIG_ENDIAN ) )
						{
							// 11/15/2012 rmd : The packet length still includes the CRC. Since it is not of any use
							// when parsing the message, remove it by reducing the length.
							pdh.dlen -= sizeof(CRC_BASE_TYPE);
							
							TPMICRO_make_a_copy_and_queue_incoming_packet( pdh );
						}
						else
						{
							Alert_router_unexp_to_addr_port( ROUTER_TYPE_FL_MASTER, pfrom_which_port );
						}
					}
					else
					if( (routing.rclass == MSG_CLASS_CS3000_TOKEN_RESP) || (routing.rclass == MSG_CLASS_CS3000_SCAN_RESP) )
					{
						// 3/12/2012 rmd : The SCAN_RESP messages are always true serial number based packet.
						if( this_communication_address_is_our_serial_number( tplh.H.FromTo.to, THE_PASSED_IN_DATA_IS_BIG_ENDIAN ) )
						{
							TPL_IN_make_a_copy_and_direct_incoming_packet( pfrom_which_port, pdh );
						}
						else
						{
							// 2/14/2017 rmd : The hub is the MASTER controller therefore these scan resp and token resp
							// packets had better be for us. If not this as an error.
							Alert_router_received_unexp_token_resp( ROUTER_TYPE_FL_MASTER, pfrom_which_port );
						}
					}
					else
					{
						// 2/10/2017 rmd : This is an error. The packet should be dropped.
						Alert_router_rcvd_unexp_class( ROUTER_TYPE_FL_MASTER, pfrom_which_port, routing.routing_class_base, routing.rclass );
					}
				}
				else
				{
					// 2/10/2017 rmd : This is an error. The packet should be dropped.
					Alert_router_unk_port( ROUTER_TYPE_FL_MASTER );
				}
			}
			else
			{
				// 5/16/2017 rmd : We had better see a know CLASS BASE.
				Alert_router_rcvd_unexp_base_class( ROUTER_TYPE_FL_MASTER, pfrom_which_port, routing.routing_class_base );
			}
			
		}
		else
		{
			// 4/13/2017 rmd : PACKET HANDLING FOR SLAVES ON A CHAIN.

			// 4/10/2017 rmd : Non hub controllers should NEVER receive a packet of 2000 based routing
			// class. So screen for that up front.
			if( routing.routing_class_base == ROUTING_CLASS_IS_2000_BASED )
			{
				Alert_Message_va( "slave ROUTER: 3000 rcvd 2000 packet class %u, port %s, len %u", routing.rclass, port_names[ pfrom_which_port ], pdh.dlen );
			}
			else
			if( routing.routing_class_base == ROUTING_CLASS_IS_3000_BASED )
			{
				need_to_scatter = (false);
			
				if( pfrom_which_port == UPORT_A )
				{
					// 4/13/2017 rmd : PACKET HANDLING FOR SLAVES ON A CHAIN.
					
					// 5/16/2017 rmd : Being a slave, Port A should only see controller-to-controller type
					// packets.
					if( (routing.rclass == MSG_CLASS_CS3000__FROM_CONTROLLER__CODE_DISTRIBUTION) ||
						(routing.rclass == MSG_CLASS_CS3000_SCAN) ||
						(routing.rclass == MSG_CLASS_CS3000_TOKEN) ||
						(routing.rclass == MSG_CLASS_CS3000_SCAN_RESP) ||
						(routing.rclass == MSG_CLASS_CS3000_TOKEN_RESP)
					  )
					{
						handle_slave_packet_routing( pfrom_which_port, routing, pdh, &need_to_scatter );
					}
					else
					if( routing.rclass == MSG_CLASS_CS3000__TO_CONTROLLER__TEST_PACKET )
					{
                        // 1/16/2018 Ryan : This would be if another controller originated the test. We are going to
                        // get a copy of the message because the message memory is freed at the end of this function
                        // and we want to make sure the content of message is intact when the radio test task
                        // processes it. We include the port and the event in the parameters so that the function
                        // has the information required to send the message to the right case in the radio test task
                        // and to echo the message back to the controller of origin.
            			if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task( pdh, pfrom_which_port, RADIO_TEST_EVENT_here_is_an_inbound_message_to_echo );
					}
					else
					if( routing.rclass == MSG_CLASS_CS3000__TO_CONTROLLER__TEST_PACKET_ECHO )
					{
                        // 12/18/2017 rmd : This would be if we originated the test. MAKE SURE WE GET A COPY OF THE
                        // MESSAGE as the message memory is freed at the end of this function, and we are only
                        // handing this message memory over to another task for subsequent processing. Depending on
                        // the task priority we do not know if the message memory has been freed or not by the time
                        // we get around to processing the message in the radio_test task.

                        // 1/16/2018 Ryan : We include the event and the port in the parameters now, so the function
                        // can also be used to echo packets, as well as recieve echoed packages, depending on the
                        // event.
						if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task( pdh, pfrom_which_port, RADIO_TEST_EVENT_here_is_an_inbound_echo_to_process );
					}
					else
					{
						// 2/10/2017 rmd : This is an unexpected. The packet should be dropped.
						Alert_router_rcvd_unexp_class( ROUTER_TYPE_FL_SLAVE, pfrom_which_port, routing.routing_class_base, routing.rclass );
					}
				}
				else
				if( pfrom_which_port == UPORT_B )
				{
					// 4/13/2017 rmd : PACKET HANDLING FOR SLAVES ON A CHAIN.
					
					if( (routing.rclass == MSG_CLASS_CS3000__FROM_CONTROLLER__CODE_DISTRIBUTION) ||
						(routing.rclass == MSG_CLASS_CS3000_SCAN) ||
						(routing.rclass == MSG_CLASS_CS3000_TOKEN) ||
						(routing.rclass == MSG_CLASS_CS3000_SCAN_RESP) ||
						(routing.rclass == MSG_CLASS_CS3000_TOKEN_RESP)
					  )
					{
						handle_slave_packet_routing( pfrom_which_port, routing, pdh, &need_to_scatter );
					}
					else
					if( routing.rclass == MSG_CLASS_CS3000__TO_CONTROLLER__TEST_PACKET )
					{
                        // 1/16/2018 Ryan : This would be if another controller originated the test. We are going to
                        // get a copy of the message because the message memory is freed at the end of this function
                        // and we want to make sure the content of message is intact when the radio test task
                        // processes it. We include the port and the event in the parameters so that the function
                        // has the information required to send the message to the right case in the radio test task
                        // and to echo the message back to the controller of origin.
            			if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task( pdh, pfrom_which_port, RADIO_TEST_EVENT_here_is_an_inbound_message_to_echo );
					}
					else
					if( routing.rclass == MSG_CLASS_CS3000__TO_CONTROLLER__TEST_PACKET_ECHO )
					{
                        // 12/18/2017 rmd : This would be if we originated the test. MAKE SURE WE GET A COPY OF THE
                        // MESSAGE as the message memory is freed at the end of this function, and we are only
                        // handing this message memory over to another task for subsequent processing. Depending on
                        // the task priority we do not know if the message memory has been freed or not by the time
                        // we get around to processing the message in the radio_test task.

                        // 1/16/2018 Ryan : We include the event and the port in the parameters now, so the function
                        // can also be used to echo packets, as well as recieve echoed packages, depending on the
                        // event.
						if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task( pdh, pfrom_which_port, RADIO_TEST_EVENT_here_is_an_inbound_echo_to_process );
					}
					else
					{
						// 2/10/2017 rmd : This is an unexpected. The packet should be dropped.
						Alert_router_rcvd_unexp_class( ROUTER_TYPE_FL_SLAVE, pfrom_which_port, routing.routing_class_base, routing.rclass );
					}
				}
				else	
				if( pfrom_which_port == UPORT_TP )
				{
					// 4/13/2017 rmd : PACKET HANDLING FOR SLAVES ON A CHAIN.
					
					if( routing.rclass == MSG_CLASS_CS3000_FROM_TP_MICRO )
					{
						if( this_communication_address_is_our_serial_number( tplh.H.FromTo.to, THE_PASSED_IN_DATA_IS_BIG_ENDIAN ) )
						{
							// 11/15/2012 rmd : The packet length still includes the CRC. Since it is not of any use
							// when parsing the message, remove it by reducing the length.
							pdh.dlen -= sizeof(CRC_BASE_TYPE);
							
							TPMICRO_make_a_copy_and_queue_incoming_packet( pdh );
						}
						else
						{
							Alert_router_unexp_to_addr_port( ROUTER_TYPE_FL_SLAVE, pfrom_which_port );
						}
					}
					else
					if( (routing.rclass == MSG_CLASS_CS3000__FROM_CONTROLLER__CODE_DISTRIBUTION) ||
						(routing.rclass == MSG_CLASS_CS3000_SCAN) ||
						(routing.rclass == MSG_CLASS_CS3000_TOKEN) ||
						(routing.rclass == MSG_CLASS_CS3000_SCAN_RESP) ||
						(routing.rclass == MSG_CLASS_CS3000_TOKEN_RESP)
					  )
					{
						handle_slave_packet_routing( pfrom_which_port, routing, pdh, &need_to_scatter );
					}
					else
					{
						// 2/10/2017 rmd : This is an error. The packet should be dropped.
						Alert_router_rcvd_unexp_class( ROUTER_TYPE_FL_SLAVE, pfrom_which_port, routing.routing_class_base, routing.rclass );
					}
				}
				else
				{
					// 2/10/2017 rmd : This is an error. The packet should be dropped.
					Alert_router_unk_port( ROUTER_TYPE_FL_SLAVE );
				}
				
				// ----------
	
				// 3/19/2012 rmd : Okay they've been screened pretty heavily so far. This controller is
				// allowed to receive these packet types. They are not to us (with the exception of the
				// TOKEN!). And we are not the master controller on the chain. So scatter them.
				if( need_to_scatter )
				{
					switch( routing.rclass )
					{
						case MSG_CLASS_CS3000_SCAN:
						case MSG_CLASS_CS3000_SCAN_RESP:
						case MSG_CLASS_CS3000_TOKEN:
						case MSG_CLASS_CS3000_TOKEN_RESP:
						case MSG_CLASS_CS3000__FROM_CONTROLLER__CODE_DISTRIBUTION:
							if( pfrom_which_port == UPORT_TP )
							{
								// 5/16/2017 rmd : Then we know this packet came in the M port, and we need to check to see
								// about pushing it out port A and/or B. BUT make sure the device is appropriate. We want to
								// guard against inappropriate hardware on the ports for this kind of traffic. So far we can
								// only forward these packet CLASSES via a freewave SR radio.
								if( CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets( UPORT_A ) )
								{
									attempt_to_copy_packet_and_route_out_another_port( UPORT_A, pdh, NULL );
								}
		
								if( CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets( UPORT_B ) )
								{
									attempt_to_copy_packet_and_route_out_another_port( UPORT_B, pdh, NULL );
								}
							}
							else
							if( pfrom_which_port == UPORT_A )
							{
								if( CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets( UPORT_B ) )
								{
									attempt_to_copy_packet_and_route_out_another_port( UPORT_B, pdh, NULL );
								}
		
								attempt_to_copy_packet_and_route_out_another_port( UPORT_M1, pdh, NULL );
							}
							else
							if( pfrom_which_port == UPORT_B )
							{
								if( CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets( UPORT_A ) )
								{
									attempt_to_copy_packet_and_route_out_another_port( UPORT_A, pdh, NULL );
								}
		
								attempt_to_copy_packet_and_route_out_another_port( UPORT_M1, pdh, NULL );
							}
							break;
		
						
						default:
							// 3/9/2012 rmd : We should not see this packet class This shouldn't ha
							Alert_Message( "ROUTER: surprise class to scatter" );
							break ;
					}
		
				}  // of still need to scatter
			}
			else
			{
				// 5/16/2017 rmd : We had better see a know CLASS BASE.
				Alert_router_rcvd_unexp_base_class( ROUTER_TYPE_FL_SLAVE, pfrom_which_port, routing.routing_class_base );
			}

		}  // of handling for the different controller types
		
	}  // of no errors
	
	// ----------
	
	// 3/19/2012 rmd : In ALL cases we are done. Copies of the packet have been made as needed.
	// Release the packet memory.
	mem_free( pdh.dptr );
}

/* ---------------------------------------------------------- */
/*
#include "screen_utils.h"
#include "d_process.h"


extern void FDTO_show_buffered_packets( void )
{
	FDTO_DrawStr( 2, 16, GuiLib_PS_ON, "Buffered: Port B %u, Port TP %u, Mode %u ", xmit_cntrl[ UPORT_B ].xlist.count, xmit_cntrl[ UPORT_TP ].xlist.count, comm_mngr.mode );
}
*/

extern void PACKET_ROUTER_broadcast_SCAN_TOKEN_CODE_packet_to_other_controllers_on_our_chain( DATA_HANDLE pdh, OUTGOING_MESSAGE_STRUCT *pom )
{
	// 7/21/2016 rmd : This function is called by the MASTER in a chain to distribute the packet
	// to all controllers in the chain. We ONLY have to think about this from the point of view
	// of the MASTER. The SLAVES handle their receipt and re-transmission in the primitives of
	// the packet routing. Whereas this function is called from the TPL_OUT and
	// CODE_DISTRIBUTION modules.
	//
	// So this function is ONLY used to scatter SCAN, TOKEN, and CODE_DISTRIBUTION packets. All
	// generated by the MASTER, and as such we do not conider PORT A as one of the ports to send
	// out to, because by definition any PORT_A device on a MASTER is NOT TO BE USED FOR
	// CONTROLLER-TO-CONTROLLER TRAFFIC! That PORT_A device leads back to the central.
	//
	// 3/24/2017 rmd : And futhermore if this controller is a HUB we should skip port B. Since
	// that port is the hub transmission device. By definition.
	
	// ----------
	
	// 5/16/2017 rmd : Cover the conditions that make it unsafe to send out PORT B.

	BOOL_32		okay_to_send_out_B;

	okay_to_send_out_B = (true);
	
	// 3/24/2017 rmd : If this controller is a HUB we would never want to try port B.
	if( CONFIG_this_controller_is_a_configured_hub() )
	{
		okay_to_send_out_B = (false);
	}
	
	// 5/16/2017 rmd : only ONE device on port B is considered safe at this time, a Freewave SR
	// radio.
	if( config_c.port_B_device_index != COMM_DEVICE_SR_FREEWAVE )
	{
		okay_to_send_out_B = (false);
	}
	
	if( okay_to_send_out_B )
	{
		attempt_to_copy_packet_and_route_out_another_port( UPORT_B, pdh, pom );
	}
	
	// ----------
	
	// 5/16/2017 rmd : Always okay to send out the M port.
	attempt_to_copy_packet_and_route_out_another_port( UPORT_M1, pdh, pom );
	
	// ----------
	
	/*
	// 7/14/2016 rmd : DEBUG CODE
	DISPLAY_EVENT_STRUCT	lde;

	lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
	lde._04_func_ptr = (void*)&FDTO_show_buffered_packets;
	Display_Post_Command( &lde );
	*/
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

