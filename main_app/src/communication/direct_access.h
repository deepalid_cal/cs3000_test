/* file = direct_access.h  10/15/02  rmd  */

/* ---------------------------------- */

#ifndef _INC_DIRECT_ACCESS_H
#define _INC_DIRECT_ACCESS_H

/* ---------------------------------- */
/* ---------------------------------- */

#include	"comm_utils.h"

#include	"lpc_types.h"




/* ---------------------------------- */
/* ---------------------------------- */

typedef struct {

	// when TRUE (0x01) screen dumps will be happening
	BOOL_32	active;

	UNS_32	nlu_milliseconds_between_updates;

} DIRECT_ACCESS_TCB_STRUCTURE;


extern DIRECT_ACCESS_TCB_STRUCTURE direct_access_tcb;




typedef struct {

	// when we send out the screen updates we have to know who to send them too
	// so this is captured when direct access is initiated
	COMMUNICATION_MESSAGE_LIST_ITEM	cmli;

	// after certain occurances we need to send a complete display update
	BOOL_32	send_whole_display;

} DIRECT_ACCESS_PRIVATE_STRUCT;


extern DIRECT_ACCESS_PRIVATE_STRUCT direct_access_private;



/* ---------------------------------- */
/* ---------------------------------- */


void init_direct_access( void );


unsigned short assemble_display_update( unsigned char *pwhereto, unsigned short pstart, unsigned short plength );

void DIRECT_ACCESS_intelligent_update( void );



/* ---------------------------------- */
/* ---------------------------------- */

#endif

/* ---------------------------------- */
/* ---------------------------------- */
/* ---------------------------------- */
/* ---------------------------------- */
