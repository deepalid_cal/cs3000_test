/*  file = packet_router.h      02.13.06 ajv : 2011.07.20 rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_PACKET_ROUTER_H
#define _INC_PACKET_ROUTER_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"general_picked_support.h"

#include	"gpio_setup.h"

#include	"tpl_out.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/6/2018 ajv : Router types are used when parsing alerts to indicate which area of the 
// router the alert is being generated for. 
#define	ROUTER_TYPE_HUB				(0)
#define ROUTER_TYPE_BEHIND_HUB		(1)
#define ROUTER_TYPE_FL_MASTER		(2)
#define ROUTER_TYPE_FL_SLAVE		(3)
#define ROUTER_TYPE_RRE				(4)


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef union
{
	UNS_8	list_addr[ 4 ];
	
	UNS_32	sn_3000;

} HUB_LIST_ADDR_UNION;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Packet Routing Functions

extern void Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy( const UPORTS pto_which_port, DATA_HANDLE pdh, OUTGOING_MESSAGE_STRUCT *pom );

extern void attempt_to_copy_packet_and_route_out_another_port( UPORTS pto_who, DATA_HANDLE pdh, OUTGOING_MESSAGE_STRUCT *pom );

extern void Route_Incoming_Packet( const UPORTS pfrom_which_port, DATA_HANDLE pdh );

extern void PACKET_ROUTER_broadcast_SCAN_TOKEN_CODE_packet_to_other_controllers_on_our_chain( DATA_HANDLE pdh, OUTGOING_MESSAGE_STRUCT *pom );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

