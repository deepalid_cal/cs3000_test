/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_CODE_DISTRIBUTION_TRANSMIT_H
#define _INC_CODE_DISTRIBUTION_TRANSMIT_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"comm_mngr.h"

#include	"gpio_setup.h"

#include	"lpc_types.h"

#include	"code_distribution_task.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void code_distribution_packet_rate_timer_callback( xTimerHandle pxTimer );

extern void build_and_send_code_transmission_init_packet( CODE_DISTRIBUTION_TASK_QUEUE_STRUCT const *const pcdtqs_ptr );

extern void perform_next_code_transmission_state_activity( CODE_DISTRIBUTION_TASK_QUEUE_STRUCT const *const pcdtqs_ptr );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

