/*  file = foal_comm.c                10.27.00 2011.08.01 rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy
#include	<string.h>

#include	"foal_comm.h"

#include	"app_startup.h"

#include	"bithacks.h"

#include	"comm_mngr.h"

#include	"flash_storage.h"

#include	"foal_flow.h"

#include	"irri_irri.h"

#include	"irri_comm.h"

#include	"foal_irri.h"

#include	"foal_lights.h"

#include	"alerts.h"

#include	"td_check.h"

#include	"pdata_changes.h"

#include	"configuration_controller.h"

#include	"r_alerts.h"

#include	"change.h"

#include	"tpmicro_data.h"

#include	"weather_control.h"

#include	"group_base_file.h"

#include	"station_groups.h"

#include	"manual_programs.h"

#include	"flowsense.h"

#include	"cs_mem.h"

#include	"controller_initiated.h"

#include	"cs3000_tpmicro_common.h"

#include	"screen_utils.h"

#include	"moisture_sensors.h"

#include	"chain_sync_funcs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/24/2014 rmd : A variable local to this file. Used to receive the decoder fault
// information from the incoming token_resp. And immediately process it. Declaring as a
// static here keeps this variable off the stack. Though does consume the space in SDRAM
// full time. Seemed better than on the stack though.
static DECODER_FAULTS_ARRAY_TYPE		decoder_faults;

// 4/16/2012 rmd : See structure definition for some explanation. This variable does NOT
// have to be battery backed. And is only valid in a fleeting way. That is between the
// receipt of a token response and the generation of the next token. The token response
// loads it. And then it is always sent with the next outgoing token to all controllers.
static STOP_KEY_RECORD_s	stop_key_record;

// 11/7/2014 rmd : C startup code initializes to 0 (false).
static BOOL_32	send_stop_key_record_in_next_token;

// ----------

// 11/1/2013 rmd : These terminal short and two-wire cable variables DO NOT have to be by
// controller (ie an array of MAX_CHAIN_LENGTH). They are valid only during the time between
// receipt of a token_resp and creation of the next outgoing token. And that outgoing token
// will contain the information of these variables if appropriate.
//
// 11/10/2014 rmd : These flags do not need to be BATTERY BACKED. On a startup the state is
// configured such that the TPMicro receives the ACK to these events. Thereby potentially
// clearing itself of the states. Remember some of these event chains prohibit further turn
// ON's.
static UNS_32			two_wire_cable_excessive_current_index;
static BOOL_32			two_wire_cable_excessive_current_needs_distribution;

// 11/10/2014 rmd : Note the excessive current flag is cleared when any action needed record
// requesting a turn ON is sent to the irri machines.

static UNS_32			two_wire_cable_overheated_index;
static BOOL_32			two_wire_cable_overheated_needs_distribution;

static UNS_32			two_wire_cable_cooled_off_index;
static BOOL_32			two_wire_cable_cooled_off_needs_distribution;

// ----------

// 8/23/2012 rmd : As shorts come in from the tpmicro they are recorded here. For
// re-distribution back out in the TOKEN. So all slaves know about them. When the slaves rcv
// the short indication flags are set and the TPMicro is notified of such so that it is
// allowed to once again turn valves ON. All the station turn OFFs take place on the FOAL
// side. And action_needed records are generated. This variable is only valid for the time
// between receving a token_resp and when the next outgoing token is generated.
FOAL_TO_IRRI_TERMINAL_SHORT_STRUCT		terminal_short_record;

// 9/24/2013 rmd : Flag to include in next outgoing TOKEN to the irri machines. Intialized
// (false) on startup by C startup code.
BOOL_32									terminal_short_needs_distribution_to_irri_machines;
	
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static BOOL_32 nm_using_FROM_ADDRESS_to_find_comm_mngr_members_index( void *vptr, UNS_32 *ptr_to_index )
{
	// 5/1/2014 rmd : Designed for local foal_comm use. References chain.members without
	// checking if network avaiable. Which is okay in this case cause this is called where we
	// pull apart a token_resp so the pieces we are referencing are supposed to be valid. Also
	// don't use the comm_mngr_MUTEX. But that is covered by the nm_ function designation.
	
	// ----------
	
	BOOL_32	rv;

	rv = (false);

	// ----------

	// 8/23/2012 rmd : A default (yet valid) value.
	*ptr_to_index = 0;

	UNS_32	i, local_ser_num;

	local_ser_num = 0; // Must 0 first to get the 4th byte set to zero!
	memcpy( &local_ser_num, vptr, 3 );

	for( i=0; i<MAX_CHAIN_LENGTH; i++ )
	{
		if( chain.members[ i ].saw_during_the_scan )
		{
			if( chain.members[ i ].box_configuration.serial_number == local_ser_num )
			{
				*ptr_to_index = i;

				rv = (true);

				break;
			}
		}
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
extern void FOAL_COMM_process_incoming__clean_house_request__response( COMMUNICATION_MESSAGE_LIST_ITEM *cmli )
{
	// 4/24/2014 rmd : Executed within the context of the comm_mngr. We have already performed
	// an integrity test to make sure the responder matches the last_contact. And we know the
	// command is the clean house RESP. This reponse carries NO data. Only the MID. Which we
	// already looked at prior.
	
	// ----------
	
	// 4/15/2014 rmd : So this controller saw our CLEAN_HOUSE request. Clear his flag so
	// potentially the comm_mngr will move on to making tokens - if not another NEW controller.
	comm_mngr.is_a_NEW_member_in_the_chain[ last_contact.index ] = (false);
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Pull the box current from the msg. Move the msg pointer along to the next
    piece of msg content. Return (false) if value is out of range. Otherwise return (true).
	
	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task upon receipt of a TOKEN_RESP
    message.

	@RETURN (see function description)

	@ORIGINAL 2012.08.08 rmd

	@REVISIONS (none)
*/
static BOOL_32 __extract_box_current_from_token_response( UNS_8 **pucp, UNS_32 pcontroller_index )
{
	BOOL_32	rv;
	
	rv = (true);
	
	// 8/16/2012 rmd : Extract the value from the message. Then indicate if this newly rcvd
	// value should be distributed in the next outgoing token.
	UNS_32	new_current_ma, *ptr_32;
	
	ptr_32 = (UNS_32*)*pucp;

	// 8/16/2012 rmd : No guarantee the ptr is on a 4-byte align. Use memcpy.
	memcpy( &new_current_ma, *pucp, sizeof(UNS_32) );
	*pucp += sizeof(UNS_32);
	
	if( new_current_ma > MAXIMUM_BOX_MILLI_AMPS )
	{
		Alert_Message( "FOAL_COMM: current out of range." ); 
		
		rv = (false);
	}
	else
	{
		// 8/16/2012 rmd : Only if the rcvd value is different than what we have do we distribute.
		// Only change the flag to (true) here. And set to (false) when sent.
		if( new_current_ma != tpmicro_data.as_rcvd_from_slaves[ pcontroller_index ].measured_ma_current )
		{
			// 8/16/2012 rmd : Set if to be distributed.
			tpmicro_data.as_rcvd_from_slaves[ pcontroller_index ].current_needs_to_be_sent = (true);
	
			// 8/16/2012 rmd : And update.
			tpmicro_data.as_rcvd_from_slaves[ pcontroller_index ].measured_ma_current = new_current_ma;
		}

	}
		
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION A STOP request has been generated at a controller and has arrived here at
    the master via the TOKEN_RESP. We reference the highest priority kind of irrigation
    presently in the list. And set the flags so that when the next TOKEN is made (remember
    broadcast) we go about the process of taking place and turn OFF those stations (if any
    ON) and remove all the stations for that reason from the list.
	
	What's missing : what system was this for?
	
	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the COMM_MNGR task when the token response is
    being pulled apart and we recognize the irri machine has indicated the STOP was pressed.
    This function may also end up being called via a 'communicated' STOP key command.
    Meaning via the radio remote or central?

	@RETURN (none)

	@AUTHOR 2012.02.21 rmd
*/
extern BOOL_32 extract_stop_key_record_from_token_response( UNS_8 **pucp, UNS_32 pcontroller_index )
{
	BOOL_32		rv;
	
	UNS_32		sss;
	
	// ----------

	rv = (true);
	
	// -----------

	// 9/24/2013 rmd : We are expecting to see the entire SHORT_OR_NO_CURRENT_REPORT_STRUCT. So
	// get it.
	memcpy( &stop_key_record, *pucp, sizeof(STOP_KEY_RECORD_s) );
	
	*pucp += sizeof(STOP_KEY_RECORD_s);
	
	// ----------
	
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	// 11/7/2014 rmd : Initialize indicating nothing stopped.
	stop_key_record.stations_removed_for_this_reason = nlu_IN_LIST_FOR_not_in_use;

	if( stop_key_record.stop_in_all_systems && stop_key_record.stop_for_all_reasons )
	{
		// 11/7/2014 rmd : Don't use the remove_all_stations function. We need the feedback for when
		// we hit a system that has stations in the list. So we can capture the highest reason for
		// the user to see on the irri side.
		for( sss=0; sss<MAX_POSSIBLE_SYSTEMS; sss++ )
		{
			// 11/7/2014 rmd : If the gid is non-zero the system is in use.
			if( system_preserves.system[ sss ].system_gid )
			{
				// 11/7/2014 rmd : And used for irrigation.
				if( SYSTEM_get_used_for_irri_bool( system_preserves.system[ sss ].system_gid ) )
				{
					// 11/7/2014 rmd : Remove all stations in this system. If there are any.
					if( FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists( &system_preserves.system[ sss ], 0, REMOVE_FOR_ALL_REASONS, ACTION_REASON_TURN_OFF_AND_REMOVE_stop_key ) )
					{
						// 11/7/2014 rmd : If returned true at least one station was removed from the list. NOTE
						// highest reason inlist is not affected by removing all stations for that reason. That
						// happens the next time foal_irri maintenance function runs. Which won't be until after the
						// next token is sent. Because it is blocked from running by the
						// send_stop_key_record_in_next_token variable being (true).
						
						// 11/7/2014 rmd : And while there may have been station in the list for multiple reason
						// this is good enough to catch the highest reason.
						stop_key_record.stations_removed_for_this_reason = system_preserves.system[ sss ].highest_reason_in_list;

						Alert_stop_key_pressed_idx( system_preserves.system[ sss ].system_gid, system_preserves.system[ sss ].highest_reason_in_list, pcontroller_index );
					}
				}
			}
		}
	}
	else
	if( stop_key_record.stop_for_the_highest_reason_in_all_systems )
	{
		for( sss=0; sss<MAX_POSSIBLE_SYSTEMS; sss++ )
		{
			// 11/7/2014 rmd : If the gid is non-zero the system is in use.
			if( system_preserves.system[ sss ].system_gid )
			{
				// 11/7/2014 rmd : And used for irrigation.
				if( SYSTEM_get_used_for_irri_bool( system_preserves.system[ sss ].system_gid ) )
				{
					// 11/7/2014 rmd : Get the highest reason ON and go remove all stations in the list for that
					// reason.
					if( FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists( &system_preserves.system[ sss ], system_preserves.system[ sss ].highest_reason_in_list, REMOVE_ONLY_FOR_THE_REASON_SPECIFIED, ACTION_REASON_TURN_OFF_AND_REMOVE_stop_key ) )
					{
						// 11/7/2014 rmd : If returned true at least one station was removed from the list. NOTE
						// highest reason inlist is not affected by removing all stations for that reason. That
						// happens the next time foal_irri maintenance function runs. Which won't be until after the
						// next token is sent. Because it is blocked from running by the
						// send_stop_key_record_in_next_token variable being (true).
						stop_key_record.stations_removed_for_this_reason = system_preserves.system[ sss ].highest_reason_in_list;

						Alert_stop_key_pressed_idx( system_preserves.system[ sss ].system_gid, system_preserves.system[ sss ].highest_reason_in_list, pcontroller_index );
					}
				}
			}
		}
	}
	else
	{
		Alert_Message( "Unhandled STOP KEY request" );
	}

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	// ----------
	
	// 4/26/2012 rmd : Setting this true cause us to signal the world in the next outgoing
	// token. It also prevents the foal irrigation list maintenance function from running until
	// the stop indication has been distributed.
	send_stop_key_record_in_next_token = (true);
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Extract the terminal short information from the token response delivered
    from an irri machine. Move the msg pointer along to the next piece of msg content.
    Return (false) if value is out of range. Otherwise return (true).
	
    @CALLER_MUTEX_REQUIREMENTS Caller to be holding the comm_mngr MUTEX. We reference the
    comm_mngr structure in several places within.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task upon receipt of a TOKEN_RESP
    message. In other words only executed at the master controller.
	
	@RETURN (see function description)

	@ORIGINAL 2012.08.27 rmd

	@REVISIONS (none)
*/
static BOOL_32 nm_extract_terminal_short_or_no_current_from_token_response( UNS_8 **pucp, UNS_32 pcontroller_index )
{
	BOOL_32		rv, found_preserves;
	
	UNS_32		ppp;

	TERMINAL_SHORT_OR_NO_CURRENT_STRUCT	srs;
	
	UNS_32		laction_reason;

	// ----------

	rv = (true);
	
	// -----------

	// 9/24/2013 rmd : We are expecting to see the entire SHORT_OR_NO_CURRENT_REPORT_STRUCT. So
	// get it.
	memcpy( &srs, *pucp, sizeof(TERMINAL_SHORT_OR_NO_CURRENT_STRUCT) );
	
	*pucp += sizeof(TERMINAL_SHORT_OR_NO_CURRENT_STRUCT);
	
	// -----------

	// 9/24/2013 rmd : Range checking.

	if( (srs.result != SHORT_OR_NO_CURRENT_RESULT_short_and_found) &&
		(srs.result != SHORT_OR_NO_CURRENT_RESULT_short_but_hunt_was_unsuccessful) &&
		(srs.result != SHORT_OR_NO_CURRENT_RESULT_no_current) )
	{
		rv = (false);	
	}

	if( (srs.terminal_type != OUTPUT_TYPE_TERMINAL_MASTER_VALVE) &&
		(srs.terminal_type != OUTPUT_TYPE_TERMINAL_PUMP) &&
		(srs.terminal_type != OUTPUT_TYPE_TERMINAL_STATION) &&
		(srs.terminal_type != OUTPUT_TYPE_TERMINAL_LIGHT) )
	{
		rv = (false);	
	}

	// -----------

	if( rv )
	{
		if( srs.result == SHORT_OR_NO_CURRENT_RESULT_short_and_found )
		{
			if( (srs.terminal_type == OUTPUT_TYPE_TERMINAL_MASTER_VALVE) || (srs.terminal_type == OUTPUT_TYPE_TERMINAL_PUMP) )
			{
				// 8/21/2012 rmd : For a SHORT on a MV or PUMP cancel all irrigation in the list for the
				// SYSTEM the POC or MV belongs to.
				
				xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );
			
				found_preserves = (false);

				for( ppp=0; ppp<MAX_POCS_IN_NETWORK; ppp++ )
				{
					if( (poc_preserves.poc[ ppp ].poc_gid != 0) )
					{
						// 6/4/2014 rmd : If preserves is for the box the message is from and is of type terminal
						// we've got a match.
						if( (poc_preserves.poc[ ppp ].box_index == pcontroller_index) && (poc_preserves.poc[ ppp ].poc_type__file_type == POC__FILE_TYPE__TERMINAL) )
						{
							found_preserves = (true);
							
							// ----------
							
							if( srs.terminal_type == OUTPUT_TYPE_TERMINAL_MASTER_VALVE )
							{
								laction_reason = ACTION_REASON_TURN_OFF_AND_REMOVE_conventional_mv_short;
								
								Alert_conventional_mv_short_idx( pcontroller_index );
							}
							else
							{
								laction_reason = ACTION_REASON_TURN_OFF_AND_REMOVE_conventional_pump_short;
								
								Alert_conventional_pump_short_idx( pcontroller_index );
							}
							
							// ----------
							
							// 6/4/2014 rmd : Remove all stations in the system that owns this poc. For all reasons.
							FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists( poc_preserves.poc[ ppp ].this_pocs_system_preserves_ptr, 0, REMOVE_FOR_ALL_REASONS, laction_reason );
							
							// 9/30/2015 rmd : If the MV output was energized because of MVOR we need to cancel that
							// MVOR.
							if( srs.terminal_type == OUTPUT_TYPE_TERMINAL_MASTER_VALVE )
							{
								FOAL_IRRI_initiate_or_cancel_a_master_valve_override( poc_preserves.poc[ ppp ].this_pocs_system_preserves_ptr->system_gid, MVOR_ACTION_CANCEL_MASTER_VALVE_OVERRIDE, 0, INITIATED_VIA_MVSHORT );
							}
							
						}
					}
				}	

				xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
				
				// ----------

				if( !found_preserves )
				{
					Alert_Message( "FOAL_COMM: shorted poc not found." );
				}
		
			}
			else
			if( srs.terminal_type == OUTPUT_TYPE_TERMINAL_STATION )
			{
				// 8/21/2012 rmd : Find and remove the offending irrigation station from the ON list and
				// resume irrigation without this station.
				FOAL_IRRI_remove_all_instances_of_this_station_from_the_irrigation_lists( pcontroller_index, srs.station_or_light_number_0, ACTION_REASON_TURN_OFF_AND_REMOVE_conventional_station_short );
				
				Alert_conventional_station_short_idx( pcontroller_index, srs.station_or_light_number_0 );
			}
			else
			if( srs.terminal_type == OUTPUT_TYPE_TERMINAL_LIGHT )
			{
				// 9/30/2015 rmd : Get the lights output removed from the ON list.
				FOAL_LIGHTS_turn_off_this_lights_output( pcontroller_index, srs.station_or_light_number_0, ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_output_short, LIGHTS_TEXT_OUTPUT_SHORT_DETECTED );
			}
		}
		else
		if( srs.result == SHORT_OR_NO_CURRENT_RESULT_short_but_hunt_was_unsuccessful )
		{
			// 9/25/2013 rmd : In this case we want to turn OFF the outputs presently ON. If there is
			// more than ONE on at the box flag them to irrigate one at a time. If there is only one on
			// KILL him as a suspected hi current. If there are none ON, meaning MV and PMP likely,
			// cancel all of irrigation.
			
			// 9/25/2013 rmd : NOPE changed my mind. First I'm going to see if there are any ON at the
			// box that reported the short. If so all those stations will be cancelled. If none are ON
			// then I will indeed cancel the irrigation for the SYSTEM that boxes POC belongs to.

			// ----------

			// 9/25/2013 rmd : First the general alert. If there are station ON we follow with a short
			// alert about each station being turned OFF [what about the MV and PUMP? - no alerts for
			// them and probably should be but that may be tricky to do]. If none ON then an alert about
			// all irrigation being cancelled.
			Alert_conventional_output_unknown_short_idx( pcontroller_index );
			
			// ----------
			
			// 9/25/2013 rmd : Then we want to turn OFF and cancel all the stations we find ON at this
			// box.
			FOAL_IRRI_remove_all_stations_ON_at_this_box_from_the_irrigation_lists( pcontroller_index, ACTION_REASON_TURN_OFF_AND_REMOVE_conventional_station_short );
			
			// ----------
			
			// 9/30/2015 rmd : If there is a MVOR in force cancel it as the MV could be the reason for
			// the short. But only for the MAINLINE that owns the terminal POC at the box this message
			// came from.
			xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );
		
			for( ppp=0; ppp<MAX_POCS_IN_NETWORK; ppp++ )
			{
				if( (poc_preserves.poc[ ppp ].poc_gid != 0) )
				{
					// 9/30/2015 rmd : If preserves is for the box the message is from and is of type terminal
					// we've got a match.
					if( (poc_preserves.poc[ ppp ].box_index == pcontroller_index) && (poc_preserves.poc[ ppp ].poc_type__file_type == POC__FILE_TYPE__TERMINAL) )
					{
						FOAL_IRRI_initiate_or_cancel_a_master_valve_override( poc_preserves.poc[ ppp ].this_pocs_system_preserves_ptr->system_gid, MVOR_ACTION_CANCEL_MASTER_VALVE_OVERRIDE, 0, INITIATED_VIA_MVSHORT );
					}
				}
			}	

			xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
			
			// ----------
			
			// 9/30/2015 rmd : And kill all the lights ON at this box.
			FOAL_LIGHTS_turn_off_all_lights_at_this_box( pcontroller_index, ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_output_short, LIGHTS_TEXT_OUTPUT_SHORT_DETECTED );
		}
		else
		if( srs.result == SHORT_OR_NO_CURRENT_RESULT_no_current )
		{
			if( srs.terminal_type == OUTPUT_TYPE_TERMINAL_STATION )
			{
				// 9/25/2013 rmd : Well we need to make an alert line about the no current. And see if we
				// should set the STATION_HISTORY flag for NO_CURRENT. When the record is re-sent to the
				// irri side (with the outgoing token from the master) the NO_CURRENT flag in the battery
				// backed station preserves is set.
				Alert_conventional_station_no_current_idx( pcontroller_index, srs.station_or_light_number_0 );
				
				FOAL_IRRI_set_no_current_flag_in_station_history_line_if_appropriate( pcontroller_index, srs.station_or_light_number_0 );
			}
			else
			if( srs.terminal_type == OUTPUT_TYPE_TERMINAL_MASTER_VALVE )
			{
				Alert_conventional_mv_no_current_idx( pcontroller_index );
			}
			else
			if( srs.terminal_type == OUTPUT_TYPE_TERMINAL_PUMP )
			{
				Alert_conventional_pump_no_current_idx( pcontroller_index );
			}
			else
			if( srs.terminal_type == OUTPUT_TYPE_TERMINAL_LIGHT )
			{
				Alert_light_ID_with_text( pcontroller_index, srs.station_or_light_number_0, LIGHTS_TEXT_OUTPUT_NO_CURRENT_DETECTED );
			}
		}
		
		// ----------
	
		// 9/24/2013 rmd : The record proved to be hold valid data therefore distribute it to all
		// the irri machines. In all cases it needs to be distributed. Include the original short
		// record, the serial number where the short occurred, and set the distribution flag.
		terminal_short_record.srs = srs;
		
		terminal_short_record.box_index_0 = pcontroller_index;
		
		terminal_short_needs_distribution_to_irri_machines = (true);
		
		// ----------
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION A two_wire cable anomaly has been reported to the master. We kill all
    irrigation right now. A better analysis would be to see which POC's, if any, are
    involved and shut down particular systems.

    @CALLER_MUTEX_REQUIREMENTS Caller to be holding the comm_mngr MUTEX. As we reference the
    comm_mngr structure.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task upon receipt of a TOKEN_RESP
    message. In other words only executed at the master controller.
	
	@RETURN (none)

	@ORIGINAL 2013.11.01 rmd

	@REVISIONS (none)
*/
static void _nm_respond_to_two_wire_cable_fault( UNS_32 pcontroller_index, UNS_32 *pbox_index_ptr, BOOL_32 *pneeds_distribution_ptr )
{
	UNS_32	ppp;
	

	// 11/1/2013 rmd : We are changing the foal_irri list. Important to take the irrigation list
	// MUTEX because we are going to remove stations from the list. And we wouldn't want to do
	// that to some poor other task that was iterating through the list. AND EVEN MORE SO, by us
	// having the foal_irri MUTEX it means the list maintenance function is not running.
	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );
	
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	// 10/30/2014 rmd : Look through the poc_preserves to see if there are any decoder based
	// pocs at this box. If so all stations for the system that owns that poc must be removed
	// from the irrigation list.
	for( ppp=0; ppp<MAX_POCS_IN_NETWORK; ppp++ )
	{
		// 10/30/2014 rmd : If the poc_gid is non-zero it means the poc is in use.
		if( poc_preserves.poc[ ppp ].poc_gid && (poc_preserves.poc[ ppp ].box_index == pcontroller_index) )
		{
			if( (poc_preserves.poc[ ppp ].poc_type__file_type == POC__FILE_TYPE__DECODER_SINGLE) || (poc_preserves.poc[ ppp ].poc_type__file_type == POC__FILE_TYPE__DECODER_BYPASS) )
			{
				// 10/30/2014 rmd : Well then it is a decoder based poc at this box with the cable problem.
				FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists( poc_preserves.poc[ ppp ].this_pocs_system_preserves_ptr, 0, REMOVE_FOR_ALL_REASONS, ACTION_REASON_TURN_OFF_AND_REMOVE_two_wire_cable_fault );
			}
		}
	}
	
	// ----------
	
	// 10/30/2014 rmd : Now that poc hunt may or may not have removed valves from the irrigation
	// list. So we still need to check for valves at this box and remove them.
	FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists( pcontroller_index, ACTION_REASON_TURN_OFF_AND_REMOVE_two_wire_cable_fault );
	
	// ----------

	// 11/1/2013 rmd : Set up for transmission to the irri side.
	*pbox_index_ptr = pcontroller_index;
	
	*pneeds_distribution_ptr = (true);
	
	// ----------

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static BOOL_32 __extract_two_wire_cable_current_measurement_from_token_response( UNS_8 **pucp, UNS_32 pcontroller_index )
{
	BOOL_32	rv;
	
	rv = (true);
	 
	// ----------

	memcpy( &foal_irri.twccm[ pcontroller_index ], *pucp, sizeof(TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT) );
	
	*pucp += sizeof(TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT);
	 
	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 __extract_test_request_from_token_response( UNS_8 **pucp )
{
	BOOL_32	rv;
	
	rv = (true);  // this always returns true
	
	TEST_AND_MOBILE_KICK_OFF_STRUCT	tkos;
	
	memcpy( &tkos, *pucp, sizeof(TEST_AND_MOBILE_KICK_OFF_STRUCT) );
	*pucp += sizeof(TEST_AND_MOBILE_KICK_OFF_STRUCT);
	
	FOAL_IRRI_initiate_a_station_test( &tkos, IN_LIST_FOR_TEST );
	
	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 __extract_et_gage_pulse_count_from_token_response( UNS_8 **pucp_ptr, UNS_32 const pcontroller_index )
{
	UNS_32	pulse_count;

	BOOL_32	rv;
	
	// 9/19/2012 rmd : We could check the pulse count to not be extreme. And if so return
	// (false).
	rv = (true);
	
	// 10/17/2014 ajv : Always extract the data from the response, regardless of
	// whether the controller who sent it is specified as the controller
	// connected to the gage or not. This allows us to continue processing the
	// rest of the token.
	memcpy( &pulse_count, *pucp_ptr, sizeof(UNS_32) );
	*pucp_ptr += sizeof( UNS_32 );

	if( WEATHER_get_et_gage_is_in_use() == (true) )
	{
		// 12/9/2013 ajv : We are testing the box index here to ensure we don't get pulses from two
		// different boxes and count them both.
		if( WEATHER_get_et_gage_box_index() == pcontroller_index )
		{
			// 9/19/2012 rmd : Arbitrary limit I picked. Should actually never see more than 1. But
			// we'll allow 2.
			if( (pulse_count > 0) && (pulse_count <= 2) )
			{
				xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );
				
				// 9/19/2012 rmd : If we are still using this location to count up gage pulses can add to
				// it. Meaning has the user decided to hand edit? If so want to leave the value in place now
				// alone.
				if( weather_preserves.et_rip.status == ET_STATUS_FROM_ET_GAGE )
				{
					weather_preserves.et_rip.et_inches_u16_10000u += (pulse_count * 100);

					if( WEATHER_get_et_gage_log_pulses() == (true) )
					{
						Alert_ETGage_Pulse( (weather_preserves.et_rip.et_inches_u16_10000u / 100) );
					}
				}

				xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );
			}
			else
			{
				// 9/19/2012 rmd : Alert pulse count out of range.
				Alert_Message( "Gage pulse count o.o.r." );
			}
		}
		else
		{
			Alert_Message_va( "ET rcvd from %c, gage at %c", pcontroller_index + 'A', WEATHER_get_et_gage_box_index() + 'A' );
		}
	}
	else
	{
		// 02/07/2013 ajv : If an ET Gage pulse is received but no gages are
		// in use the weather_control structure has become disjointed with
		// what's actually installed.
		Alert_et_gage_pulse_but_no_gage_in_use();
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 extract_rain_bucket_pulse_count_from_token_response( UNS_8 **pucp_ptr, UNS_32 const pcontroller_index )
{
	UNS_32	pulse_count, sss;

	BOOL_32	rv;
	
	// 9/19/2012 rmd : We could check the pulse count to not be extreme. And if so return
	// (false).
	rv = (true);

	// 10/17/2014 ajv : Always extract the data from the response, regardless of whether the
	// controller who sent it is specified as the controller connected to the gage or not. This
	// allows us to continue processing the rest of the token.
	memcpy( &pulse_count, *pucp_ptr, sizeof(UNS_32) );
	*pucp_ptr += sizeof( UNS_32 );
	
	if( WEATHER_get_rain_bucket_is_in_use() == (true) )
	{
		// 12/9/2013 ajv : We are testing the serial number here to ensure we don't get pulses from
		// two different serial numbers and count them both.
		if( WEATHER_get_rain_bucket_box_index() == pcontroller_index )
		{
			// 9/19/2012 rmd : Arbitrary limit I picked. Should actually never see more than 1. But
			// we'll allow 2.
			if( (pulse_count > 0) && (pulse_count <= 2) )
			{
				// 4/27/2015 rmd : Add to the system report data rip.
				xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
			
				for( sss=0; sss<MAX_POSSIBLE_SYSTEMS; sss++ )
				{
					// If the record is in use.
					if( system_preserves.system[ sss ].system_gid != 0 )
					{
						// 4/27/2015 rmd : Each bucket pulse is 0.01 inches.
						system_preserves.system[ sss ].rip.rainfall_raw_total_100u += pulse_count;
					}
				}
		
				xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
				
				// ----------
				
				xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );
				
				// 9/21/2012 rmd : We always add to the roll time to roll time pulse count.
				weather_preserves.rain.roll_to_roll_raw_pulse_count += pulse_count;

				// 9/21/2012 rmd : We always add to the midnight to midnight pulse count.
				weather_preserves.rain.midnight_to_midnight_raw_pulse_count += pulse_count;
				
				// ----------
				
				// 9/24/2012 rmd : We add to the hourly total here. And make sure we haven't exceeded the
				// hourly total. And if we see the hourly total has made it's way past the minimum we will
				// set the flag to trigger the end of and prevent further irrigation. We do this regardless
				// of if the user has edited the rip or not.
				weather_preserves.rain.hourly_total_inches_100u += pulse_count;
				
				// -------------

				// 9/24/2012 rmd : Limit the hourly total as needed.
				UNS_32	max_hourly, minimum_to_inhibit_irrigation;
				
				max_hourly = WEATHER_get_rain_bucket_max_hourly_inches_100u();

				minimum_to_inhibit_irrigation = WEATHER_get_rain_bucket_minimum_inches_100u();
				
				// -------------

				if( weather_preserves.rain.hourly_total_inches_100u > max_hourly )
				{
					weather_preserves.rain.hourly_total_inches_100u = max_hourly;
				}
				
				// 9/19/2012 rmd : If the user has not edited the rip check to see if the new hourly total
				// plus the total already in the table exceed the minimum. If the user has edited the rip is
				// no longer valid for this check.
				if( (weather_preserves.rain.rip.status == RAIN_STATUS_FROM_RAIN_BUCKET_M) )
				{
					// 9/24/2012 rmd : If at any point the accumulated hourly is more than the minimum then
					// inhibit irrigation. We don't need to check if the rip is already an R or if the flag is
					// already set. Whether the amount is an 'M' or an 'R' it is okay to make this check on the
					// amount in the rip and potentially set the flag. Even though we may have already crossed
					// the minimum and the flag is already set.
					if( (weather_preserves.rain.rip.rain_inches_u16_100u + weather_preserves.rain.hourly_total_inches_100u) >= minimum_to_inhibit_irrigation )
					{
						// 9/16/2014 ajv : The first time we flag that we have crossed the minimum, notify Command
						// Center Online for the purpose of rain polling.
						if( weather_preserves.rain.inhibit_irrigation_have_crossed_the_minimum == (false) )
						{
							CONTROLLER_INITIATED_post_event( CI_EVENT_start_send_rain_indication_timer );
						}

						weather_preserves.rain.inhibit_irrigation_have_crossed_the_minimum = (true);
					}
					
				}
				
				xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );
			}
			else
			{
				// 9/19/2012 rmd : Alert pulse count out of range.
				Alert_Message( "RB pulse count o.o.r." );
			}
			
		}
		else
		{
			Alert_Message_va( "Rain rcvd from %c, bucket at %c", pcontroller_index + 'A', WEATHER_get_rain_bucket_box_index() + 'A' );
		}
	}
	else
	{
		// 02/07/2013 ajv : If an Rain Bucket pulse is received but no
		// buckets are in use, the weather_control structure has become
		// disjointed with what's actually installed.
		Alert_rain_pulse_but_no_bucket_in_use();
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 extract_wind_from_token_response( UNS_8 **pucp_ptr, UNS_32 const pcontroller_index, UNS_32 const ppieces )
{
	UNS_32	lwind_reading;

	BOOL_32	lwind_paused;

	BOOL_32	make_assignment;

	BOOL_32	rv;

	// 10/13/2014 ajv : So far, we always return true. However, we could modify
	// this to range check the wind value to make sure it's within reasonable
	// bounds and return false in such a case.
	rv = (true);

	make_assignment = (false);

	if( B_IS_SET( ppieces, BIT_TF_I_reporting_wind ) )
	{
		// 11/12/2014 rmd : Always extract the token content to keep the message square.
		memcpy( &lwind_reading, *pucp_ptr, sizeof(UNS_32) );
		*pucp_ptr += sizeof(UNS_32);
	
		memcpy( &lwind_paused, *pucp_ptr, sizeof(BOOL_32) );
		*pucp_ptr += sizeof(BOOL_32);
		
		// ----------

		// 11/12/2014 rmd : Now based on user settings possibly override what we just received. In
		// order to keep clean values if the user takes out of service with wind and perhaps in the
		// paused state.
		if( WEATHER_get_wind_gage_in_use() )
		{
			// 11/12/2014 rmd : Check we just got wind from the controller registered to have the gage.
			if( WEATHER_get_wind_gage_box_index() != pcontroller_index )
			{
				Alert_Message_va( "Wind rcvd from %c, gage at %c", pcontroller_index + 'A', WEATHER_get_wind_gage_box_index() + 'A' );

				lwind_reading = 0;
	
				lwind_paused = (false);
			}
		}
		else
		{
			// 02/07/2013 ajv : If a Wind Gage reading is received but no gages are in use, the
			// weather_control structure has become disjointed with what's actually installed.
			Alert_wind_detected_but_no_gage_in_use();

			lwind_reading = 0;

			lwind_paused = (false);
		}

		// 11/14/2014 rmd : For all cases if we received make the assignement to the Gui Vars.
		make_assignment = (true);
	}
	else
	{
		// 11/12/2014 rmd : So in this token response we didn't get wind. We were we supposed to!
		if( WEATHER_get_wind_gage_in_use() && (WEATHER_get_wind_gage_box_index() == pcontroller_index) )
		{
			// 11/12/2014 rmd : Didn't see out wind. Something wrong. Clear state.
			lwind_reading = 0;

			lwind_paused = (false);

			// 11/14/2014 rmd : And make the assignement.
			make_assignment = (true);
		}
		
		// 11/12/2014 rmd : Is wind gage not in use?
		if( !WEATHER_get_wind_gage_in_use() )
		{
			// 11/12/2014 rmd : In this case we weren't supposed to receive wind. But clear the states
			// to make sure the user didn't take the gage out of use while paused!
			lwind_reading = 0;

			lwind_paused = (false);

			// 11/14/2014 rmd : And make the assignement.
			make_assignment = (true);
		}
	}

	// ----------
	
	if( make_assignment )
	{
		// 10/13/2014 ajv : Directly set the GuiVar used to display this variable. Since the wind
		// speed is simply an in-memory variable, why declare another global when we already have
		// one?
		GuiVar_StatusWindGageReading = lwind_reading;
		
		GuiVar_StatusWindPaused = lwind_paused;
		
		// 10/15/2014 ajv : Also set the foal_irri variable which is used to control irrigation. No
		// mutex is necessary since this is an atomic operation.
		foal_irri.wind_paused = lwind_paused;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Processes what the TP Micro reports is connected regarding each station
 * module. If the terminal and card are both connected, the stations are either
 * added if they don't exist or flagged as connected. If one of the required
 * components is not connected, the appropriate flag is set false to prevent the
 * station from being used.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the COMM MNGR task when an
 * updated 'what's installed' structure is received from the controller's TP
 * Micro.
 * 
 * @param new_whats_installed A pointer to the newly received what's installed
 * structure.
 * 
 * @param pmodule_number The card number (0-6) which indicates which stations to
 * enable or disable. Card 0 holds stations 1-8, card 2 holds 9-16, and so on.
 * 
 * @param saw_a_change A pointer to the calling function's 'saw_a_change'
 * variable. This is set TRUE if a change was detected and triggers the TP Micro
 * file to be saved once all of the components have been processed.
 *
 * @author 8/21/2012 Adrianusv
 *
 * @revisions
 *  2/11/2013 rmd : Modified routine to simplify processing. Also removed the
 *  removal of stations if the module is completely removed from the TP Micro.
 * 
 *  7/26/2013 ajv: Added explicit checking for what has changed and alert
 *  generation based upon what has been added or removed.
 */
static void process_what_is_installed_for_stations( const WHATS_INSTALLED_STRUCT *const new_whats_installed, const UNS_32 pmodule_number, const UNS_32 pbox_index_0, BOOL_32 *saw_a_change )
{
	// 4/24/2014 rmd : Executed within the context of the comm_mngr task when a whats installed
	// structure is received from the local tpmicro. Caller to be holding both the chain_members
	// and the irri_comm recursive MUTEXES.
	//
	// Compare the received whats installed to the chain.members copy for this controller to
	// determine if a change is to be flagged and processed. On a chain a change causes
	// transmission of the box configuration to the master. Which is turn broadcasts the chain
	// member array. Which in turn causes us to re-request the tpmicro to send the whats
	// installed. So that we double check we are indeed in sync.

	// 2/5/2015 rmd : This function is allowed to set saw_a_change (true) if a configuration
	// change has been detected. If no change leaves the variable alone.

	// ----------
	
	STATION_STRUCT		*lstation;
	
	UNS_32	i, station;
	
	// ----------
	
	if( chain.members[ pbox_index_0 ].box_configuration.wi.stations[ pmodule_number ].card_present != new_whats_installed->stations[ pmodule_number ].card_present )
	{
		Alert_station_card_added_or_removed_idx( pbox_index_0, pmodule_number, new_whats_installed->stations[ pmodule_number ].card_present );

		*saw_a_change = (true);
	}

	if( chain.members[ pbox_index_0 ].box_configuration.wi.stations[ pmodule_number ].tb_present != new_whats_installed->stations[ pmodule_number ].tb_present )
	{
		Alert_station_terminal_added_or_removed_idx( pbox_index_0, pmodule_number, new_whats_installed->stations[ pmodule_number ].tb_present );

		*saw_a_change = (true);
	}

	// ----------

	// 4/9/2013 rmd : ALWAYS check that the station file reflects the installed hardware. This
	// is done whenever the whats_installed structure is delivered from the TP Micro to ensure
	// the station file is indeed in sync with the installed hardware. We used to only look
	// through the stations if the deleivered wi didn't match the chain members copy. But that
	// approach doesn't guarantee the station file is in sync with the wi. And I view the
	// station file as the bottom line.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// 2/11/2013 rmd : Either they are both installed or we behave as if neither is.
	if( new_whats_installed->stations[ pmodule_number ].card_present && new_whats_installed->stations[ pmodule_number ].tb_present )
	{
		// A new station card and terminal board was installed. Therefore, add
		// the appropriate stations to this controller.
		for( i = 0; i < 8; ++i )
		{
			station = (pmodule_number*8) + i;
			
			lstation = nm_STATION_get_pointer_to_station( pbox_index_0, station );

			// Only create the station if it doesn't already exist.
			if( lstation == NULL )
			{
				lstation = nm_STATION_create_new_station( pbox_index_0, station, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED );  // station ends up in use

				// 8/18/2014 ajv : Suppressed for release to Bakersfield.
				//
				// 2/5/2015 rmd : I believe he meant to do this permanently. So comment should say more
				// along the lines of suppressed after debugged.
				Alert_Message_va( "TPmicro: station %c%u created", ('A' + pbox_index_0), (station + 1) ); 
			}

			// ----------

			// 7/7/2014 rmd : We want to set the station as physically available here. Even if just
			// created we must do this as the station is created with physically available set to
			// (false).
			nm_STATION_set_physically_available( lstation, (true), CHANGE_do_not_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, pbox_index_0, CHANGE_set_change_bits, STATION_get_change_bits_ptr(lstation, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED ) );
		}
	}
	else
	{
		// 2/11/2013 rmd : One or both pieces of hardware are missing. Set the flags appropriately.
		// But do not delete the station or any of its associated data.
		for( i = 0; i < 8; ++i )
		{
			station = (pmodule_number*8) + i;
			
			lstation = nm_STATION_get_pointer_to_station( pbox_index_0, station );

			// 2/11/2013 rmd : Set flags if the station already exists. If doesn't exist - do nothing.
			if( lstation != NULL )
			{
				nm_STATION_set_physically_available( lstation, (false), (false), CHANGE_REASON_TERMINAL_CARD_OR_DECODER_REMOVED, pbox_index_0, (true), STATION_get_change_bits_ptr(lstation, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_REMOVED ) );
			}
		}
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Processes what the TP Micro reports is connected regarding the lights module.
 * If the terminal and card are both connected, the lights module is flagged as
 * connected. If one of the required components is not connected, the
 * appropriate flag is set false to prevent the lights from being used.
 * 
 * @mutex_requirements Caller to be holding both the chain_members and the
 * irri_comm recursive MUTEXES.
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the COMM MNGR task when an
 * updated 'what's installed' structure is received from the controller's TP
 * Micro.
 * 
 * @param new_whats_installed A pointer to the newly received what's installed
 * structure.
 * 
 * @param pbox_index_0 The 0 - 11 box index.
 *
 * @param poutput_index_0 The 0 - 3 output index.
 *
 * @param saw_a_change A pointer to the calling function's 'saw_a_change'
 * variable. This is set TRUE if a change was detected and triggers the TP Micro
 * file to be saved once all of the components have been processed.
 *
 * @author 7/13/2015 mpd
 */
static void process_what_is_installed_for_lights( const WHATS_INSTALLED_STRUCT *const new_whats_installed, const UNS_32 pbox_index_0, BOOL_32 *saw_a_change )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	LIGHT_STRUCT	*llights;

	UNS_32 loutput_index;

	// 4/24/2014 rmd : Executed within the context of the comm_mngr task when a whats installed
	// structure is received from the local tpmicro. Caller to be holding both the chain_members
	// and the irri_comm recursive MUTEXES.

	// 2/5/2015 rmd : This function is allowed to set saw_a_change (true) if a configuration
	// change has been detected. If no change leaves the variable alone.

	// ----------
	
	// 2/11/2013 rmd : Has anything changed?
	if( chain.members[ pbox_index_0 ].box_configuration.wi.lights.card_present != new_whats_installed->lights.card_present )
	{
		Alert_lights_card_added_or_removed_idx( pbox_index_0, new_whats_installed->lights.card_present );

		*saw_a_change = (true);
	}

	if( chain.members[ pbox_index_0 ].box_configuration.wi.lights.tb_present != new_whats_installed->lights.tb_present )
	{
		Alert_lights_terminal_added_or_removed_idx( pbox_index_0, new_whats_installed->lights.tb_present );

		*saw_a_change = (true);
	}

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	// 2/12/2013 rmd : What we do for light goes here.
	if( new_whats_installed->lights.card_present && new_whats_installed->lights.tb_present )
	{
		// 7/13/2015 mpd : Create a group for each of the outputs on the LIGHTS board.
		for( loutput_index = 0; loutput_index < MAX_LIGHTS_IN_A_BOX; ++loutput_index )
		{
			llights = nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index( pbox_index_0, loutput_index );

			// 7/26/2013 ajv : Both components are present, therefore take the
			// steps necessary to create or enable lights in the firmware.
			if( llights == NULL )
			{
				llights = nm_LIGHTS_create_new_group( pbox_index_0, loutput_index );

                lchange_bitfield_to_set = LIGHTS_get_change_bits_ptr( llights, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED );

				nm_LIGHTS_set_box_index( llights, pbox_index_0, CHANGE_do_not_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, pbox_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );

				nm_LIGHTS_set_output_index( llights, loutput_index, CHANGE_do_not_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, pbox_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
			}

			// ----------

			// 9/29/2015 ajv : By default, the light is created with the connected flags set to false.
			// Therefore, if a new POC was created or another change was detected, set them true now.
			nm_LIGHTS_set_physically_available( llights, (true), CHANGE_do_not_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, pbox_index_0, CHANGE_set_change_bits, LIGHTS_get_change_bits_ptr( llights, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED ) );
		}
	}
	else
	{
		for(  loutput_index = 0; loutput_index < MAX_LIGHTS_IN_A_BOX; ++loutput_index )
		{
			llights = nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index( pbox_index_0, loutput_index );

			// 9/29/2015 ajv : One or both of the components are missing, therefore take the steps
			// necessary to disable lights in the firmware. If it had existed set flags indicating what
			// is missing.
			if( llights != NULL )
			{
				nm_LIGHTS_set_physically_available( llights, (false), CHANGE_do_not_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_REMOVED, pbox_index_0, CHANGE_set_change_bits, LIGHTS_get_change_bits_ptr( llights, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_REMOVED ) );
			}
		}
	}

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
/**
 * Processes what the TP Micro reports is connected regarding the on-board POC
 * module. If the terminal and board are both connected, the POC is added (if
 * it doesn't already exist) and set in use; if both are disconnected, the POC
 * is removed; if only one of the required components is connected, the POC is
 * added (if it doesn't exist) and set to be not in use.
 *
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the COMM MNGR task when an
 * updated 'what's installed' structure is received from the controller's TP
 * Micro.
 * 
 * @param new_whats_installed A pointer to the newly received what's installed
 * structure.
 * 
 * @param been_a_change A pointer to the calling function's 'been_a_change'
 * variable. This is set TRUE if a change was detected and triggers the TP Micro
 * file to be saved once all of the components have been processed.
 *
 * @author 8/21/2012 Adrianusv
 *
 * @revisions (none)
 *  2/11/2013 rmd : Modified routine to simplify processing. Also removed the
 *  removal of the POC if the module is completely removed from the TP Micro.
 * 
 *  7/26/2013 ajv: Added explicit checking for what has changed and alert
 *  generation based upon what has been added or removed.
 */
static void process_what_is_installed_for_POCs( const WHATS_INSTALLED_STRUCT *const new_whats_installed, const UNS_32 pbox_index_0, BOOL_32 *saw_a_change )
{
	POC_GROUP_STRUCT	*lpoc;

	char	str_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	// ----------
	
	// 4/24/2014 rmd : Executed within the context of the comm_mngr task when a whats installed
	// structure is received from the local tpmicro. Caller to be holding both the chain_members
	// and the irri_comm recursive MUTEXES. This function will at the most create a TERMINAL
	// POC. That is the only kind of POC that is associated with the whats installed data.

	// 2/5/2015 rmd : This function is allowed to set saw_a_change (true) if a configuration
	// change has been detected. If no change leaves the variable alone.

	// ----------
	
	// 2/11/2013 rmd : Has anything changed?
	if( chain.members[ pbox_index_0 ].box_configuration.wi.poc.card_present != new_whats_installed->poc.card_present )
	{
		Alert_poc_card_added_or_removed_idx( pbox_index_0, new_whats_installed->poc.card_present );

		*saw_a_change = (true);
	}

	if( chain.members[ pbox_index_0 ].box_configuration.wi.poc.tb_present != new_whats_installed->poc.tb_present )
	{
		Alert_poc_terminal_added_or_removed_idx( pbox_index_0, new_whats_installed->poc.tb_present );

		*saw_a_change = (true);
	}

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	// 6/3/2016 rmd : Find POC in file. Whether to show or not.
	lpoc = nm_POC_get_pointer_to_terminal_poc_with_this_box_index_0( pbox_index_0 );

	// 2/11/2013 rmd : Either they are both installed or we behave as if neither is.
	if( new_whats_installed->poc.card_present && new_whats_installed->poc.tb_present )
	{
		// 2/11/2013 rmd : If not found create. Otherwise, set the card and
		// terminal board connected flags.
		if( lpoc == NULL )
		{
			lpoc = nm_POC_create_new_group( pbox_index_0, POC__FILE_TYPE__TERMINAL, 0 );

			nm_POC_set_box_index( lpoc, pbox_index_0, CHANGE_do_not_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, pbox_index_0, CHANGE_set_change_bits, POC_get_change_bits_ptr(lpoc, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED ) );

			snprintf( str_48, NUMBER_OF_CHARS_IN_A_NAME, "Terminal Strip @ %c", pbox_index_0 + 'A' );
			nm_POC_set_name( lpoc, str_48, CHANGE_do_not_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, pbox_index_0, CHANGE_set_change_bits, POC_get_change_bits_ptr(lpoc, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED ) );

			// 8/18/2014 ajv : Suppressed for release to Bakersfield
			//Alert_Message_va( "TPMicro: POC %u created", nm_GROUP_get_group_ID(lpoc) );
		}

		// ----------

		// 6/6/2016 rmd : By default, the POC is created with the "show" flag set false.
		// Additionally if the user had previously set this flag to hide this POC well its back
		// again so show it.
		nm_POC_set_show_this_poc( lpoc, (true), CHANGE_do_not_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, pbox_index_0, CHANGE_set_change_bits, POC_get_change_bits_ptr(lpoc, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED ) );

		// 5/7/2014 ajv : Instead of waiting for the routine 1hz call to this sync function, force
		// it now. Since we've added a POC or changed the phyiscally available attribute, force a
		// sync with the preserves to prevent the "TP_COMM: poc not found" alert.
		POC_PRESERVES_synchronize_preserves_to_file();
	}
	else
	{
		// 6/6/2016 rmd : Else do NOTHING. Just because the tpmicro reported the poc missing we
		// cannot mark this poc as missing by setting the 'show' flag (false). If we did that and
		// this was just a temporary condition (user messing around) the poc preserves would remove
		// this poc and destroy the budget accumulators in the process. If poc then re-added budgets
		// is ruined for this billing cycle. NO BUENO.
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Processes what the TP Micro reports is installed related to the Weather
 * (model -W) module. If it reports the card and terminal board are both
 * present, the -W setting is enabled. If either the card and terminal board are
 * disconnected, the -W setting is disabled.
 *
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the COMM MNGR task when an
 * updated 'what's installed' structure is received from the controller's TP
 * Micro.
 * 
 * @param new_whats_installed A pointer to the newly received what's installed
 * structure.
 * 
 * @param been_a_change A pointer to the calling function's 'been_a_change'
 * variable. This is set TRUE if a change was detected and triggers the TP Micro
 * file to be saved once all of the components have been processed.
 *
 * @author 8/21/2012 Adrianusv
 *
 * @revisions
 *  2/11/2013 rmd : Modified routine to simplify processing.
 * 
 *  7/26/2013 ajv: Added explicit checking for what has changed and alert
 *  generation based upon what has been added or removed.
 */
static void process_what_is_installed_for_weather( const WHATS_INSTALLED_STRUCT *const new_whats_installed, const UNS_32 pbox_index_0, BOOL_32 *saw_a_change )
{
	// 4/24/2014 rmd : Executed within the context of the comm_mngr task when a whats installed
	// structure is received from the local tpmicro. Caller to be holding both the chain_members
	// and the irri_comm recursive MUTEXES.

	// 2/5/2015 rmd : This function is allowed to set saw_a_change (true) if a configuration
	// change has been detected. If no change leaves the variable alone.

	// ----------
	
	if( chain.members[ pbox_index_0 ].box_configuration.wi.weather_card_present != new_whats_installed->weather_card_present )
	{
		Alert_weather_card_added_or_removed_idx( pbox_index_0, new_whats_installed->weather_card_present );

		*saw_a_change = (true);
	}

	if( chain.members[ pbox_index_0 ].box_configuration.wi.weather_terminal_present != new_whats_installed->weather_terminal_present )
	{
		Alert_weather_terminal_added_or_removed_idx( pbox_index_0, new_whats_installed->weather_terminal_present );

		*saw_a_change = (true);
	}
}

/* ---------------------------------------------------------- */
/**
 * Processes what the TP Micro reports is installed related to the
 * Communications (model -M) module.
 *
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the COMM MNGR task when an
 * updated 'what's installed' structure is received from the controller's TP
 * Micro.
 * 
 * @param new_whats_installed A pointer to the newly received what's installed
 * structure.
 * 
 * @param been_a_change A pointer to the calling function's 'been_a_change'
 * variable. This is set TRUE if a change was detected and triggers the TP Micro
 * file to be saved once all of the components have been processed.
 *
 * @author 8/21/2012 Adrianusv
 *
 * @revisions
 *  7/26/2013 ajv: Added explicit checking for what has changed and alert
 *  generation based upon what has been added or removed.
 */
static void process_what_is_installed_for_comm( const WHATS_INSTALLED_STRUCT *const new_whats_installed, const UNS_32 pbox_index_0, BOOL_32 *saw_a_change )
{
	// 4/24/2014 rmd : Executed within the context of the comm_mngr task when a whats installed
	// structure is received from the local tpmicro. Caller to be holding both the chain_members
	// and the irri_comm recursive MUTEXES.

	// 2/5/2015 rmd : This function is allowed to set saw_a_change (true) if a configuration
	// change has been detected. If no change leaves the variable alone.

	// ----------
	
	if( chain.members[ pbox_index_0 ].box_configuration.wi.dash_m_card_present != new_whats_installed->dash_m_card_present )
	{
		Alert_communication_card_added_or_removed_idx( pbox_index_0, new_whats_installed->dash_m_card_present );

		*saw_a_change = (true);
	}

	if( chain.members[ pbox_index_0 ].box_configuration.wi.dash_m_terminal_present != new_whats_installed->dash_m_terminal_present )
	{
		Alert_communication_terminal_added_or_removed_idx( pbox_index_0, new_whats_installed->dash_m_terminal_present );

		*saw_a_change = (true);
	}

	// 2/12/2013 rmd : What we do for Dash M card and terminal goes here.
	if( new_whats_installed->dash_m_card_present && new_whats_installed->dash_m_terminal_present )
	{
		// 7/26/2013 ajv : Both components are present, therefore take the
		// steps necessary to enable the -M in the firmware.
	}
	else
	{
		// 7/26/2013 ajv : One or both of the components are missing,
		// therefore take the steps necessary to disable the -M in the
		// firmware.
	}
}

/* ---------------------------------------------------------- */
/**
 * Processes what the TP Micro reports is installed related to the Two-Wire
 * module.
 *
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the COMM MNGR task when an
 * updated 'what's installed' structure is received from the controller's TP
 * Micro.
 * 
 * @param new_whats_installed A pointer to the newly received what's installed
 * structure.
 * 
 * @param been_a_change A pointer to the calling function's 'been_a_change'
 * variable. This is set TRUE if a change was detected and triggers the TP Micro
 * file to be saved once all of the components have been processed.
 *
 * @author 8/21/2012 Adrianusv
 *
 * @revisions (none)
 */
static void process_what_is_installed_for_two_wire( const WHATS_INSTALLED_STRUCT *const new_whats_installed, const UNS_32 pbox_index_0, BOOL_32 *saw_a_change )
{
	// 4/24/2014 rmd : Executed within the context of the comm_mngr task when a whats installed
	// structure is received from the local tpmicro. Caller to be holding both the chain_members
	// and the irri_comm recursive MUTEXES.

	// 2/5/2015 rmd : This function is allowed to set saw_a_change (true) if a configuration
	// change has been detected. If no change leaves the variable alone.

	// ----------
	

	// 2/11/2013 rmd : There is no two-wire card. Only a terminal block.
	if( chain.members[ pbox_index_0 ].box_configuration.wi.two_wire_terminal_present != new_whats_installed->two_wire_terminal_present )
	{
		// 7/19/2013 rmd : So far all we do is make an alert.
		Alert_two_wire_terminal_added_or_removed_idx( pbox_index_0, new_whats_installed->two_wire_terminal_present );

		*saw_a_change = (true);
	}
}

/* ---------------------------------------------------------- */
static BOOL_32 extract_box_configuration_from_irrigation_token_resp( UNS_8 **pucp, const UNS_32 pcontroller_index )
{
	BOX_CONFIGURATION_STRUCT	lbox_config;

	UNS_32	card;

	BOOL_32	saw_a_change;

	BOOL_32	rv;
	
	rv = (true);  // this always returns true

	// ----------

	xSemaphoreTakeRecursive( chain_members_recursive_MUTEX, portMAX_DELAY );

	// ----------

	// 11/21/2014 ajv : Take a copy of the box configuration so we can build up the necessary
	// stations, POCs and so on at the master for distribution back to the slaves.
	memcpy( &lbox_config, *pucp, sizeof(BOX_CONFIGURATION_STRUCT) );

	*pucp += sizeof(BOX_CONFIGURATION_STRUCT);

	// ----------

	// 2/5/2015 rmd : Initialize. The process functions can only set this variable (true).
	saw_a_change = (false);

	if( memcmp( &chain.members[ pcontroller_index ].box_configuration, &lbox_config, sizeof(BOX_CONFIGURATION_STRUCT) ) != 0 )
	{
		saw_a_change = (true);
	}
	
	// ----------
	
	// 2/6/2015 rmd : NOTE - further inclusion of the saw_a_change term is really not necessary
	// in the following process functions. The preceeding MEMCMP covers it all. However it is
	// already coded and in place so I'll leave it.
	
	// 11/21/2014 ajv : Here is where specific differences are detected and acted upon. For
	// example if stations have been removed mark the appropriate flags as not connected.

	// 2/2/2016 rmd : THIS ONLY APPLIES TO TERMINAL BASED ENTITIES. Decoder based items
	// (stations, POC's, and moisture sensors) have their physically available set at the
	// conclusion of the discovery if they are missing from the discovery. And stations and
	// POC's are CREATED when the user makes a station assignment or indicates a POC is in use
	// or builds a bypass.
	
	for( card = 0; card < STATION_CARD_COUNT; card++ )
	{
		process_what_is_installed_for_stations( &lbox_config.wi, card, pcontroller_index, &saw_a_change );
	}

	process_what_is_installed_for_lights( &lbox_config.wi, pcontroller_index, &saw_a_change );

	process_what_is_installed_for_POCs( &lbox_config.wi, pcontroller_index, &saw_a_change );

	process_what_is_installed_for_weather( &lbox_config.wi, pcontroller_index, &saw_a_change );

	process_what_is_installed_for_comm( &lbox_config.wi, pcontroller_index, &saw_a_change );

	process_what_is_installed_for_two_wire( &lbox_config.wi, pcontroller_index, &saw_a_change );

	// ----------
	
	// 4/24/2014 rmd : Overwrite what was in the chain members array for this controller. And
	// set the flag to cause distribution to all in the chain.
	memcpy( &chain.members[ pcontroller_index ].box_configuration, &lbox_config, sizeof(BOX_CONFIGURATION_STRUCT) );

	// ----------

	xSemaphoreGiveRecursive( chain_members_recursive_MUTEX );

	// ----------

	// 2/5/2015 rmd : BUT only re-register with the central if there was a change detected. Why?
	// Because otherwise on every program restart (power-failure) we would send the registration
	// data at the first request to communicate. A simple power fail triggering a
	// re-registration is uncalled for. So use saw_a_change to suppress that.
	if( saw_a_change)
	{
		Alert_Message( "DISTRIB & RE-REG: wi change detected" );

		// ----------
	
		// 2/6/2015 rmd : ONLY set to re-distribute. Because the receipt over at the IRRI side once
		// again causes all controllers to send their wi AGAIN. This circle continues until there
		// are no changes detected. Ensuring all box_configuration elements of the chain member
		// array are in sync.
		comm_mngr.broadcast_chain_members_array = (true);
		
		// ----------
		
		// 10/23/2014 ajv : Force another registration to make sure we update the configuration held
		// by Command Center Online.
		CONTROLLER_INITIATED_update_comm_server_registration_info();

		// ----------
	
		// 8/22/2012 ajv : Redraw the screen we're on in case something changed
		// on the screen the user was viewing.
		//
		// TODO: What if the user is viewing, say, the Stations screen and the
		// station that's up is removed? We need to ensure the routines used to
		// display the data properly handle the case where the data has changed.
	
		// 11/8/2013 ajv : For now, redraw the Main Menu if that's the screen the
		// user's viewing since we manage what menu items are displayed based upon
		// what's installed.
		if( GuiLib_CurStructureNdx == GuiStruct_scrMainMenu_0 )
		{
			Redraw_Screen( (false) );
		}
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 __extract_manual_water_request_from_token_response( UNS_8 **pucp )
{
	BOOL_32	rv;
	
	rv = (true);  // this always returns true
	
	MANUAL_WATER_KICK_OFF_STRUCT	mwkos;
	
	memcpy( &mwkos, *pucp, sizeof(MANUAL_WATER_KICK_OFF_STRUCT) );
	*pucp += sizeof(MANUAL_WATER_KICK_OFF_STRUCT);
	
	FOAL_IRRI_initiate_manual_watering( &mwkos );
	
	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 extract_mvor_request_from_token_response( UNS_8 **pucp )
{
	BOOL_32	rv;

	MVOR_KICK_OFF_STRUCT	mvorkos;
	
	// ----------
	
	rv = (true);  // this always returns true
	
	memcpy( &mvorkos, *pucp, sizeof(MVOR_KICK_OFF_STRUCT) );
	*pucp += sizeof(MVOR_KICK_OFF_STRUCT);
	
	FOAL_IRRI_initiate_or_cancel_a_master_valve_override( mvorkos.system_gid, mvorkos.mvor_action_to_take, mvorkos.mvor_seconds, mvorkos.initiated_by );

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 extract_walk_thru_start_request_from_token_response( UNS_8 **pucp )
{
	BOOL_32	rv;

	UNS_32	walk_thru_gid;
	
	// ----------
	
	rv = (true);  // this always returns true
	
	memcpy( &walk_thru_gid, *pucp, sizeof(UNS_32) );
	
	*pucp += sizeof(UNS_32);
	
	FOAL_IRRI_start_a_walk_thru( walk_thru_gid );
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 extract_chain_sync_crc_from_token_response( UNS_8 **pucp, UNS_32 const pfrom_controller_index )
{
	BOOL_32	rv;

	UNS_32			extracted_ff_name;
	
	CRC_BASE_TYPE	extracted_crc;
	
	// ----------
	
	rv = (true);  // this always returns true
	
	// ----------
	
	memcpy( &extracted_ff_name, *pucp, sizeof(UNS_32) );
	
	*pucp += sizeof(UNS_32);
	
	memcpy( &extracted_crc, *pucp, sizeof(CRC_BASE_TYPE) );
	
	*pucp += sizeof(CRC_BASE_TYPE);
	
	// ----------
	
	CHAIN_SYNC_test_deliverd_crc( extracted_ff_name, extracted_crc, pfrom_controller_index );
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 __station_decoder_fault_support( UNS_32 pcontroller_index, DECODER_FAULT_BASE_TYPE *pfault_ptr, UNS_32 paction_reason )
{
	// 10/24/2014 rmd : Find and turn OFF the station associated with the function parameters.
	// In some cases there may not be a station found and that is okay. Make alert lines as
	// appropriate.
	
	// 10/27/2014 rmd : Return (true) if a station is found. This helps us with the inoperative
	// decoder alert for the case when the decoder doesn't have a station specified for the
	// output we made the call with.

	// ----------
	
	BOOL_32		rv;
	
	STATION_STRUCT	*lstation;

	UNS_32	lstation_number_0;

	rv = (false);

	// 10/24/2014 rmd : Bar list from changing.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// 10/24/2014 rmd : Locate the station. Use the passed controller index indicating who the
	// message is from. Cause can only report about decoder stations physically at his box.
	lstation = nm_STATION_get_pointer_to_decoder_based_station( pcontroller_index, pfault_ptr->decoder_sn, pfault_ptr->afflicted_output );
	
	if( lstation )
	{
		// 10/24/2014 rmd : Remove the station from all lists. For all reasons.
		lstation_number_0 = nm_STATION_get_station_number_0( lstation );
		
		// ----------
		
		FOAL_IRRI_remove_all_instances_of_this_station_from_the_irrigation_lists( pcontroller_index, lstation_number_0, paction_reason );

		// ----------
		
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_station_decoder_solenoid_short )
		{
			Alert_two_wire_station_decoder_fault_idx( STATION_DECODER_FAULT_ALERT_CODE_solenoid_short, pcontroller_index, pfault_ptr->decoder_sn, lstation_number_0 );
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_station_decoder_voltage_too_low )
		{
			Alert_two_wire_station_decoder_fault_idx( STATION_DECODER_FAULT_ALERT_CODE_voltage_too_low, pcontroller_index, pfault_ptr->decoder_sn, lstation_number_0 );
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_station_decoder_inoperative )
		{
			Alert_two_wire_station_decoder_fault_idx( STATION_DECODER_FAULT_ALERT_CODE_inoperative, pcontroller_index, pfault_ptr->decoder_sn, lstation_number_0 );
		}
		
		// ----------
		
		rv = (true);
	}
	else
	{
		// 10/22/2014 rmd : Tell about this! However note the inoperative decoder may not have
		// stations assigned to both outputs. Or any outputs for that matter. But decoder solenoid
		// shorts and low voltage conditions only occur during an attempt to turn ON which implies
		// the output is in use and assigned to a station.
		if( paction_reason != ACTION_REASON_TURN_OFF_AND_REMOVE_station_decoder_inoperative )
		{
			Alert_Message_va( "Decoder SN %d, output %d not found in station list", pfault_ptr->decoder_sn, pfault_ptr->afflicted_output );
		}
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	
	return( rv );
}

/* ---------------------------------------------------------- */
static void __poc_decoder_fault_support( UNS_32 pcontroller_index, DECODER_FAULT_BASE_TYPE *pfault_ptr, UNS_32 paction_reason )
{
	BY_SYSTEM_RECORD	*lbsr_ptr;

	POC_GROUP_STRUCT	*lpoc;

	char	lpoc_name[ NUMBER_OF_CHARS_IN_A_NAME ];
	
	// ----------
	
	// 11/6/2014 rmd : We are here because a poc decoder has either a low voltage condition or a
	// solenoid short or the decoder was flagged inoperative.
	
	// 12/10/2014 rmd : Take and hold the MUTEX surrounding all the calls involving the poc and
	// poc group file.
	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );
	
	// 6/2/2016 rmd : Should be able to find a POC set to SHOW. Could be a BYPASS or SINGLE.
	lpoc = nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn( pcontroller_index, POC__FILE_TYPE__DECODER_SINGLE, pfault_ptr->decoder_sn, POC_SET_TO_SHOW_ONLY );

	if( !lpoc )
	{
		// 6/10/2016 rmd : Then by default in the blind we will tie this error to the bypass (if
		// there is one). Remember when we find the bypass the serial number is not used - only the
		// box_index and the poc_type. But hey ... we got the error and it wasn't for a SINGLE so it
		// must be coming from a BYPASS.
		lpoc = nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn( pcontroller_index, POC__FILE_TYPE__DECODER_BYPASS, 0, POC_SET_TO_SHOW_ONLY );
	}

	// ----------
	
	if( lpoc )
	{
		lbsr_ptr = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( POC_get_GID_irrigation_system( lpoc ) );
	
		// ----------
		
		FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists( lbsr_ptr, 0, REMOVE_FOR_ALL_REASONS, paction_reason );
	
		// ----------
		
		strlcpy( lpoc_name, nm_GROUP_get_name(lpoc), sizeof(lpoc_name) );
		
		// ----------
		
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_poc_decoder_voltage_too_low )
		{
			Alert_two_wire_poc_decoder_fault_idx( POC_DECODER_FAULT_ALERT_CODE_voltage_too_low, pcontroller_index, pfault_ptr->decoder_sn, (char*)lpoc_name );
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_poc_decoder_solenoid_short )
		{
			// 11/6/2014 rmd : Sanity check.
			if( pfault_ptr->afflicted_output != DECODER_OUTPUT_A_BLACK )
			{
				Alert_Message( "POC decoder short unexp output" );
			}
	
			Alert_two_wire_poc_decoder_fault_idx( POC_DECODER_FAULT_ALERT_CODE_solenoid_short, pcontroller_index, pfault_ptr->decoder_sn, (char*)lpoc_name );
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_poc_decoder_inoperative )
		{
			Alert_two_wire_poc_decoder_fault_idx( POC_DECODER_FAULT_ALERT_CODE_inoperative, pcontroller_index, pfault_ptr->decoder_sn, (char*)lpoc_name );
		}
		
	}
	else
	{
		// 12/10/2014 rmd : In the case of a POC decoder we could suffer a fault (DECODER INOP say)
		// before this decoder has been selected IN_USE by the user (and subsequently added to the
		// file). If that were to happen the poc would not be in the file. And the prior call would
		// return a NULL. If it did we do not want to use that value in the following function
		// calls. Doing such would generate 3 error condition alert lines.
		Alert_Message_va( "POC Decoder Fault: s/n %u not found in file", pfault_ptr->decoder_sn );
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static BOOL_32 __extract_decoder_faults_from_token_response_and_stop_affected_stations( UNS_8 **pucp, const UNS_32 pcontroller_index )
{
	UNS_32		i;
	
	BOOL_32		station_found;
	
	// ----------
	
	memcpy( &decoder_faults, *pucp, sizeof(DECODER_FAULTS_ARRAY_TYPE) );

	*pucp += sizeof(DECODER_FAULTS_ARRAY_TYPE);

	// ----------
	
	for( i=0; i<TWO_WIRE_DECODER_FAULTS_TO_BUFFER; i++ )
	{
		// 10/24/2014 rmd : Remember it is legitimate that some entries in the array hold the fault
		// code of 0. Meaning not in use. As a matter of fact the typical case is only ONE entry is
		// active. However we scan through them all.
		if( decoder_faults[ i ].fault_type_code != DECODER_FAULT_CODE_not_in_use )
		{
			// 10/24/2014 rmd : Using this construct so we only have to test for and alert about invalid
			// decoder type once.
			if( (decoder_faults[ i ].id_info.decoder_type__tpmicro_type != DECODER__TPMICRO_TYPE__2_STATION) && (decoder_faults[ i ].id_info.decoder_type__tpmicro_type != DECODER__TPMICRO_TYPE__POC) )
			{
				Alert_Message( "Unexpected decoder type!" );
			}
			else
			{
				if( decoder_faults[ i ].fault_type_code == DECODER_FAULT_CODE_solenoid_output_short )
				{
					if( decoder_faults[ i ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__2_STATION )
					{
						__station_decoder_fault_support( pcontroller_index, &decoder_faults[ i ], ACTION_REASON_TURN_OFF_AND_REMOVE_station_decoder_solenoid_short );
					}
					else
					if( decoder_faults[ i ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__POC )
					{
						__poc_decoder_fault_support( pcontroller_index, &decoder_faults[ i ], ACTION_REASON_TURN_OFF_AND_REMOVE_poc_decoder_solenoid_short );
					}
				}
				else
				if( decoder_faults[ i ].fault_type_code == DECODER_FAULT_CODE_low_voltage_detected )
				{
					if( decoder_faults[ i ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__2_STATION )
					{
						__station_decoder_fault_support( pcontroller_index, &decoder_faults[ i ], ACTION_REASON_TURN_OFF_AND_REMOVE_station_decoder_voltage_too_low );
					}
					else
					if( decoder_faults[ i ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__POC )
					{
						__poc_decoder_fault_support( pcontroller_index, &decoder_faults[ i ], ACTION_REASON_TURN_OFF_AND_REMOVE_poc_decoder_voltage_too_low );
					}
				}
				else
				if( decoder_faults[ i ].fault_type_code == DECODER_FAULT_CODE_solenoid_output_no_current )
				{
					// not yet generated by the tpmicro and decoder code	
				}
				else
				if( decoder_faults[ i ].fault_type_code == DECODER_FAULT_CODE_inoperative_decoder )
				{
					if( decoder_faults[ i ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__2_STATION )
					{
						// 10/24/2014 rmd : For the case of an inoperative decoder, which can occur at ANY time,
						// check for and remove stations in the list for the decoder on either output. Should remove
						// for any and all reasons in the list.

						// 10/29/2014 rmd : Since the inoperative decoder case can occur even though no stations are
						// assigned to this decoder we may not generate any alert lines. So handle specially to
						// decider if we force an alert.
						station_found = (false);

						// 10/29/2014 rmd : Force the support function to look for stations using the BLACK output.
						decoder_faults[ i ].afflicted_output = DECODER_OUTPUT_A_BLACK;
						
						if( __station_decoder_fault_support( pcontroller_index, &decoder_faults[ i ], ACTION_REASON_TURN_OFF_AND_REMOVE_station_decoder_inoperative ) )
						{
							station_found = (true);
						}

						// 10/29/2014 rmd : Force the support function to look for stations using the ORANGE output.
						decoder_faults[ i ].afflicted_output = DECODER_OUTPUT_B_ORANGE;
						
						if( __station_decoder_fault_support( pcontroller_index, &decoder_faults[ i ], ACTION_REASON_TURN_OFF_AND_REMOVE_station_decoder_inoperative ) )
						{
							station_found = (true);
						}
						
						if( !station_found )
						{
							// 10/29/2014 rmd : With the box index and station number maxed out we parse the line to
							// indicate no stations assigned to this decoder.
							Alert_two_wire_station_decoder_fault_idx( STATION_DECODER_FAULT_ALERT_CODE_inoperative, UNS_32_MAX, decoder_faults[ i ].decoder_sn, UNS_32_MAX );
						}
					}
					else
					if( decoder_faults[ i ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__POC )
					{
						__poc_decoder_fault_support( pcontroller_index, &decoder_faults[ i ], ACTION_REASON_TURN_OFF_AND_REMOVE_poc_decoder_inoperative );
					}
				}
				
			}  // of invalid decoder type

		}  // of entry is in use

	}  // of for each entry in the fault array

	// ----------

	// 10/24/2014 rmd : Always returns (true).
	return( (true) );

}

/* ---------------------------------------------------------- */
static BOOL_32 extract_lights_on_from_token_response( UNS_8 **pucp, UNS_32 pcontroller_index )
{
	BOOL_32	rv;
	
	rv = (true);  // this always returns true
	
	LIGHTS_ON_XFER_RECORD	loxr;
	
	memcpy( &loxr, *pucp, sizeof(LIGHTS_ON_XFER_RECORD) );
	*pucp += sizeof(LIGHTS_ON_XFER_RECORD);
	
	// 8/11/2015 mpd : TODO: Turn on light.
	FOAL_LIGHTS_process_light_on_from_token_response( &loxr );
	
	return( rv );

}

/* ---------------------------------------------------------- */
static BOOL_32 extract_lights_off_from_token_response( UNS_8 **pucp, UNS_32 pcontroller_index )
{
	BOOL_32	rv;
	
	rv = (true);  // this always returns true
	
	LIGHTS_OFF_XFER_RECORD	loxr;
	
	memcpy( &loxr, *pucp, sizeof(LIGHTS_OFF_XFER_RECORD) );
	*pucp += sizeof(LIGHTS_OFF_XFER_RECORD);
	
	// 8/11/2015 mpd : TODO: Turn off light.
	FOAL_LIGHTS_process_light_off_from_token_response( &loxr );
	
	return( rv );

}

/* ---------------------------------------------------------- */
// This function is executed within the context of the comm_mngr task.
void FOAL_COMM_process_incoming__irrigation_token__response( COMMUNICATION_MESSAGE_LIST_ITEM *cmli )
{
	unsigned char	*ucp;
	
	FOAL_COMMANDS	fc;  // incoming message
	
	BOOL_32		clean_token_resp;

	// ------------

	// 10/2/2012 rmd : The comm_mngr structure can be read or written to by several tasks. Take
	// the change flags about the need to distribute an edit for example. They are written to
	// by the key processing task and read and written to by the comm_mngr task.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	// ------------

	// You have to be very careful about using the ucp pointer as a literal pointer to a
	// structure such as a STATION_XFER_RECORD. The reason is ALIGNMENT - you really don't know
	// if ucp is on an ODD or EVEN boundary - I suspect if it is on an ODD boundary you will get
	// exception errors when trying to access a short. Because it too would end up on an odd
	// boundary. It is MUCH safer to first copy the data to an acceptable location. Remember
	// however this structure goes on the stack and chews up sizeof(STATION_XFER_RECORD) which
	// is 54 bytes of stack space - not too bad. This same argument holds for the ILC that is
	// xferred in this message. That structure is 92 bytes.

	ucp = cmli->dh.dptr;

	// ALL messages start with the FOAL_COMMANDS structure.
	memcpy( &fc, ucp, sizeof(FOAL_COMMANDS) );
	
	ucp += sizeof(FOAL_COMMANDS);
	

	// Pull apart the response!	
	comm_stats.token_responses_rcvd += 1;

	UNS_32	controller_index;

	// 2/21/2013 rmd : Find the index into the comm_mngr members array.
	if( nm_using_FROM_ADDRESS_to_find_comm_mngr_members_index( &(cmli->from_to.from), &controller_index ) == (false) )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "Token Resp : can't find index" );	
	}
	else
	{
		if( last_contact.index != controller_index )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "Response from unexp index" );
		}

		// ----------

		// 4/12/2012 rmd : If there is a problem with any part of the data while extracting we'll
		// alert and abandon the rest of the message. To avoid potentially crashing.
		BOOL_32	content_ok;
		
		content_ok = (true);

		clean_token_resp = (true);

		// ----------

		// 8/8/2016 rmd : For debug only.
		//Alert_Message_va( "token resp rcvd from %c, %u bytes", controller_index+'A', cmli->dh.dlen );
		
		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_here_is_program_data )) )
		{
			// 8/8/2016 rmd : For debug only.
			//Alert_Message_va( "   holds pdata content", controller_index+'A' );

			// 8/9/2016 rmd : NOTE THIS COMMENT
			// 8/9/2016 rmd : I THINK it would not be a bad idea to boost the priority of the comm_mngr
			// during the pdata parsing. Do it here. This accomplishes two things. If you look at how we
			// boost the priority of the comm_mngr in irri_comm while parsing the token there is an
			// argument about a MUTEX deadlock condition. Seems it could occur here. ALSO boosting
			// prevents the display from being redrawn in the case that there is a lot of change lines
			// being made and the user is looking at the change lines screen (or engineering alerts).

			// 8/9/2016 rmd : AJ's comment - Since the change was received via a token response, we know
			// the change came from the keypad. Therefore, force the reason to that.
			ucp += PDATA_extract_and_store_changes( ucp, CHANGE_REASON_CHANGED_AT_SLAVE, CHANGE_set_change_bits, CHANGE_REASON_CHANGED_AT_SLAVE );

			// 5/2/2014 rmd : Well there was pdata content from the SLAVE. Does not qualify for the
			// network available criteria.
			clean_token_resp = (false);
		}

		// ----------

		if( (content_ok) && (B_IS_SET(fc.pieces, BIT_TF_I_here_is_my_box_configuration)) )
		{
			// 8/8/2016 rmd : For debug only.
			//Alert_Message_va( "   holds wi content", controller_index+'A' );

			content_ok = extract_box_configuration_from_irrigation_token_resp( &ucp, controller_index );

			// 5/2/2014 rmd : Well there was box config content from the SLAVE. Does not qualify for the
			// network available criteria.
			clean_token_resp = (false);
		}

		// ----------

		if( clean_token_resp )
		{
			comm_mngr.since_the_scan__master__number_of_clean_token_responses_rcvd[ controller_index ] += 1;
		}
		else
		{
			// 5/5/2014 rmd : If we haven't yet reached the limit then clear back to 0. So the criteria
			// is "x clean tokens rcvd IN A ROW". Has to be latching, once reaches limit stays above
			// limit, as the pdata content appears during normal real-time user editing activity.
			if( comm_mngr.since_the_scan__master__number_of_clean_token_responses_rcvd[ controller_index ] < REQUIRED_NUMBER_OF_CLEAN_TOKENS_SINCE_SCAN )
			{
				// 5/5/2014 rmd : But so we aren't unecessarily strict only clear for the controller
				// involved here.
				comm_mngr.since_the_scan__master__number_of_clean_token_responses_rcvd[ controller_index ] = 0;

				// 5/5/2014 rmd : And clean the clean token made count too. So that actually while the token
				//  responses keep carrying this type of content we don't let the master clean tokens made
				//  count advance.
				comm_mngr.since_the_scan__master__number_of_clean_tokens_made = 0;
			}
		}
	
		
		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_am_request_you_start_a_scan )) )
		{
			// 10/30/2017 rmd : Capture the reason for the scan. The alert line is made when the scan
			// actually starts.
			comm_mngr.start_a_scan_captured_reason = COMM_MNGR_reason_to_scan_requested_by_slave;
			
			// 4/29/2014 rmd : By design legit to directly set this.
			comm_mngr.start_a_scan_at_the_next_opportunity = (true);
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_stop_key_pressed )) )
		{
			content_ok = extract_stop_key_record_from_token_response( &ucp, controller_index );
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_system_gids_to_clear_mlb_for )) )
		{
			content_ok = FOAL_extract_clear_mlb_gids( &ucp );
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_here_is_my_box_current_ma )) )
		{
			content_ok = __extract_box_current_from_token_response( &ucp, controller_index );
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_reporting_a_terminal_short_or_no_current )) )
		{
			content_ok = nm_extract_terminal_short_or_no_current_from_token_response( &ucp, controller_index );
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_reporting_two_wire_cable_excessive_current )) )
		{
			xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

			Alert_two_wire_cable_excessive_current_idx( controller_index );

			xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX);

			// 11/1/2013 rmd : No need to evaluate to 'content ok' cause there is no accompanying data
			// to this bit of pieces to range check.
			_nm_respond_to_two_wire_cable_fault( controller_index, &two_wire_cable_excessive_current_index, &two_wire_cable_excessive_current_needs_distribution );
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_reporting_two_wire_cable_overheated )) )
		{
			xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

			Alert_two_wire_cable_over_heated_idx( controller_index );

			xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX);

			// 11/1/2013 rmd : No need to evaluate to 'content ok' cause there is no accompanying data
			// to this bit of pieces to range check.
			_nm_respond_to_two_wire_cable_fault( controller_index, &two_wire_cable_overheated_index, &two_wire_cable_overheated_needs_distribution );
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_reporting_two_wire_cable_cooled_off )) )
		{
			xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

			Alert_two_wire_cable_cooled_off_idx( controller_index );

			xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX);

			// 11/11/2014 rmd : Setup the box_index to be distributed.
			two_wire_cable_cooled_off_index = controller_index;

			two_wire_cable_cooled_off_needs_distribution = (true);
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_two_wire_cable_current_measurement )) )
		{
			content_ok = __extract_two_wire_cable_current_measurement_from_token_response( &ucp, controller_index );
		}

		// ----------

		// 6/24/2015 mpd : The "Test Stations Sequential" screen depends on "BIT_TF_I_stop_key_pressed" being handled
		// before this. Don't change the order.
		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_test_request )) )
		{
			content_ok = __extract_test_request_from_token_response( &ucp );
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_reporting_et_gage_pulse_count )) )
		{
			content_ok = __extract_et_gage_pulse_count_from_token_response( &ucp, controller_index );
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_reporting_rain_bucket_pulse_count )) )
		{
			content_ok = extract_rain_bucket_pulse_count_from_token_response( &ucp, controller_index );
		}

		// ----------

		if( content_ok )
		{
			// 11/12/2014 rmd : Always attempt to extract wind from the token_response. So that we have
			// the opportunity to clean up the control variables if the user has said he doesn't want to
			// use wind anymore. Suppose he made that decision in the paused state.
			content_ok = extract_wind_from_token_response( &ucp, controller_index, fc.pieces );
		}

		// ----------

		if( content_ok )
		{
			// 3/17/2016 rmd : If the bit is set that indicates the rain switch 'has rain'. If the bit
			// is not set (missing from the message that is) that indicates no rain. When we see the bit
			// we start a 20 second timer. And everytime we rcv a msg with the bit set we RESTART that
			// timer. As msgs arrive without the bit set the timer count down. After 20 seconds of not
			// seeing a bit set from ANYBODY in the chain the timer expires and the rain_switch_active
			// indication is set (false). See the timer call back function and you'll see this.
			//
			// TODO
			// 3/17/2016 rmd : Mike B. and I recognize that the 20 seconds may just not be long enough
			// on an elaborately configured chain (radios and hardwire). Either the time will have to be
			// lengtheded or the scheme redesigned. This needs to be tested.
			if( B_IS_SET( fc.pieces, BIT_TF_I_reporting_rain_switch_active ) )
			{
				xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );

				if( weather_preserves.rain_switch_active == (false) )
				{
					weather_preserves.rain_switch_active = (true);

					Alert_rain_switch_active();
				}

				// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
				// when posting is safe. Anything on the timer queue is quickly processed and therefore the
				// queue is effectively always empty.
				xTimerStart( foal_irri.timer_rain_switch, portMAX_DELAY );

				xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );
			}
		}

		// ----------

		if( content_ok )
		{
			if( B_IS_SET(fc.pieces, BIT_TF_I_reporting_freeze_switch_active) )
			{
				xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );

				if( weather_preserves.freeze_switch_active == (false) )
				{
					weather_preserves.freeze_switch_active = (true);

					Alert_freeze_switch_active();
				}

				// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
				// when posting is safe. Anything on the timer queue is quickly processed and therefore the
				// queue is effectively always empty.
				xTimerStart( foal_irri.timer_freeze_switch, portMAX_DELAY );

				xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );
			}
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_reporting_runaway_gage)) )
		{
			xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );

			if( weather_preserves.run_away_gage == (false) )
			{
				weather_preserves.run_away_gage = (true);

				Alert_ETGage_RunAway();
			}
			
			xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_clear_runaway_gage_key_pressed)) )
		{
			// 2/7/2013 ajv : Set the flag to distribute the clear command
			// to all of the IRRI machines.
			weather_preserves.clear_runaway_gage = (true);

			Alert_ETGage_RunAway_Cleared();
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_manual_water_request )) )
		{
			content_ok = __extract_manual_water_request_from_token_response( &ucp );
		}

		// ----------

		if( (content_ok) && (B_IS_SET(fc.pieces, BIT_TF_I_reporting_decoder_faults)) )
		{
			content_ok = __extract_decoder_faults_from_token_response_and_stop_affected_stations( &ucp, controller_index );
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_L_light_on_request )) )
		{
			content_ok = extract_lights_on_from_token_response( &ucp, controller_index );
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_L_light_off_request )) )
		{
			content_ok = extract_lights_off_from_token_response( &ucp, controller_index );
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_here_is_an_update_for_my_pocs )) )
		{
			// 2012.01.27 rmd : Because this is the COMM_MNGR task we can reference the contact status
			// without the use of a MUTEX. NO other task is allowed to reference the contact status
			// array.
			content_ok = FOAL_FLOW_extract_poc_update_from_token_response( &ucp, controller_index );
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_here_is_a_moisture_reading_string )) )
		{
			content_ok = MOISTURE_SENSOR_extract_moisture_reading_from_token_response( &ucp, controller_index );
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_mvor_request )) )
		{
			content_ok = extract_mvor_request_from_token_response( &ucp );
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_walk_thru_start_request )) )
		{
			content_ok = extract_walk_thru_start_request_from_token_response( &ucp );
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_here_is_a_single_chain_sync_crc )) )
		{
			content_ok = extract_chain_sync_crc_from_token_response( &ucp, controller_index );
		}

		// ----------

		if( (content_ok) && (B_IS_SET( fc.pieces, BIT_TF_I_here_is_latest_alert_timestamp) == (true)) )
		{
			content_ok = ALERTS_store_latest_timestamp_for_this_controller( &ucp, controller_index );
		}

		// ----------

		if( !content_ok )
		{
			Alert_token_resp_extraction_error();
		}
		
	}  // Of if we found a valid index.

	// ------------

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Builds a special type of token message which requests the controller's
 * information (name, serial number, list of purchased options, and what's
 * installed structure), as well as a list of the stations and points of
 * connection connected to the controller.
 * 
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the COMM_MNGR task when
 * building the first token after the FLOWSENSE chain comes online.
 *
 * @author 9/4/2013 AdrianusV
 *
 * @revisions (none)
 */
extern void FOAL_COMM_build_token__clean_house_request( void )
{
	DATA_HANDLE						ldh;

	// ------------

	ldh.dlen = sizeof( COMM_COMMANDS );

	ldh.dptr = mem_malloc( ldh.dlen );

	next_contact.message_handle = ldh;


	*(COMM_COMMANDS*)(ldh.dptr) = MID_FOAL_TO_IRRI__CLEAN_HOUSE_REQUEST;
	
	// ------------

	// 4/15/2014 rmd : There is no message content. The COMMAND mid says it all.

	// ------------
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION When the TOKEN is being built this function is called to test if we should
    include the STOP COMMAND structure within the TOKEN. If not we set the pointer argument
    to NULL and return a 0 length. The action to remove these stations we previously taken
    when the stop_command_pending flag was set.
	
	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task.
	
    @RETURN The length of this piece of the message. Zero if not to include within the
    message.

	@ORIGINAL 2012.03.12 rmd

	@REVISIONS
*/
static UNS_32 load_stop_key_record_into_outgoing_token( UNS_8 **stop_command_data_ptr )
{
	UNS_32	rv;
	
	if( send_stop_key_record_in_next_token )
	{
		*stop_command_data_ptr = mem_malloc( sizeof(STOP_KEY_RECORD_s) );

		memcpy( *stop_command_data_ptr, &stop_key_record, sizeof(STOP_KEY_RECORD_s) );

		rv = sizeof(STOP_KEY_RECORD_s);
		
		// 4/17/2012 rmd : And clear the setting. With the broadcast token we send this out ONCE.
		send_stop_key_record_in_next_token = (false);
	}
	else
	{
		rv = 0;
		
		*stop_command_data_ptr = NULL;	
	}
				
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION When the TOKEN is being built this function is called insert the box
    current into the outgoing msg so all irri machines can display it.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task.
	
    @RETURN The length of this piece of the message. Zero if not to include within the
    message. Also the ptr is set to NULL if no content.

	@ORIGINAL 2012.05.14 rmd

	@REVISIONS
*/
static UNS_32 load_box_current_into_outgoing_token( UNS_8 **pbox_current_ptr )
{
	UNS_32	rv;
	
	rv = 0;
	
	*pbox_current_ptr = NULL;

	// -----------

	UNS_8	*ucp, how_many, *how_many_ptr;
	
	how_many = 0;
	
	// 8/17/2012 rmd : Keep compiler happy regarding use of an uninitialized variable.
	ucp = NULL;
	
	UNS_32	i;

	for( i=0; i<MAX_CHAIN_LENGTH; i++ )
	{
		if( tpmicro_data.as_rcvd_from_slaves[ i ].current_needs_to_be_sent == (true) )
		{
			if( rv == 0 )
			{
				// 8/16/2012 rmd : Get the maximum amount this could ever be. We're sending the count of
				// records plus for each record the index of the record and the current reading.
				ucp = mem_malloc( (sizeof(UNS_8) + (MAX_CHAIN_LENGTH*( sizeof(UNS_8) + sizeof(UNS_32) )) ) );
				
				how_many_ptr = ucp;
				
				*pbox_current_ptr = ucp;
				
				// 8/16/2012 rmd : Skip over the how_many byte location.
				ucp += sizeof(UNS_8);
				rv += sizeof(UNS_8);
			}
			
			// -----------

			how_many += 1;
			
			// -----------
			
			// 8/16/2012 rmd : Yeah i know. An UNS_32 written to a byte location. Works for two reasons.
			// First little endian machine. Second because i < MAX_CHAIN_LENGTH. But not portable code.
			*ucp = i;
			ucp += sizeof(UNS_8);
			rv += sizeof(UNS_8);
			
			memcpy( ucp, &(tpmicro_data.as_rcvd_from_slaves[ i ].measured_ma_current), sizeof(UNS_32) );
			ucp += sizeof(UNS_32);
			rv += sizeof(UNS_32);

			// 8/16/2012 rmd : Clear the flag.
			tpmicro_data.as_rcvd_from_slaves[ i ].current_needs_to_be_sent = (false);
		}

	}
	
	if( how_many != 0 )
	{
		// 8/16/2012 rmd : Direct byte writes are always safe. No ptr alignment issue.
		*how_many_ptr = how_many;
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 _nm_load_terminal_short_or_no_current_record_into_outgoing_token( UNS_8 **pterminal_short_ptr )
{
	UNS_32	rv;

	rv = 0;
	
	*pterminal_short_ptr = NULL;

	// -----------

	if( terminal_short_needs_distribution_to_irri_machines )
	{
		// 9/25/2013 rmd : Get the memory for the record we're going to send.
		if( mem_obtain_a_block_if_available( sizeof(FOAL_TO_IRRI_TERMINAL_SHORT_STRUCT), (void**)pterminal_short_ptr ) )
		{
			memcpy( *pterminal_short_ptr, &terminal_short_record, sizeof(FOAL_TO_IRRI_TERMINAL_SHORT_STRUCT) );
	
			rv += sizeof(FOAL_TO_IRRI_TERMINAL_SHORT_STRUCT);
	
			// -----------
			
			// 8/23/2012 rmd : Clear the flag.
			terminal_short_needs_distribution_to_irri_machines = (false);
		}

	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 _nm_load_two_wire_cable_excessive_current_into_outgoing_token( UNS_8 **ptwca_ptr )
{
	UNS_32	rv;
	
	rv = 0;
	
	*ptwca_ptr = NULL;

	// ----------
	
	if( two_wire_cable_excessive_current_needs_distribution )
	{
		// 9/25/2013 rmd : Get the memory for the record we're going to send.
		if( mem_obtain_a_block_if_available( sizeof(UNS_32), (void**)ptwca_ptr ) )
		{
			memcpy( *ptwca_ptr, &two_wire_cable_excessive_current_index, sizeof(UNS_32) );
	
			rv += sizeof(UNS_32);
	
			// -----------
			
			// 8/23/2012 rmd : Clear the flag.
			two_wire_cable_excessive_current_needs_distribution = (false);
		}

	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 _nm_load_two_wire_cable_overheated_into_outgoing_token( UNS_8 **ptwca_ptr )
{
	UNS_32	rv;
	
	rv = 0;
	
	*ptwca_ptr = NULL;

	// -----------

	if( two_wire_cable_overheated_needs_distribution )
	{
		// 9/25/2013 rmd : Get the memory for the record we're going to send.
		if( mem_obtain_a_block_if_available( sizeof(UNS_32), (void**)ptwca_ptr ) )
		{
			memcpy( *ptwca_ptr, &two_wire_cable_overheated_index, sizeof(UNS_32) );
	
			rv += sizeof(UNS_32);
	
			// -----------
			
			// 8/23/2012 rmd : Clear the flag.
			two_wire_cable_overheated_needs_distribution = (false);
		}

	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 _nm_load_two_wire_cable_cooled_down_into_outgoing_token( UNS_8 **ptwca_ptr )
{
	UNS_32	rv;
	
	rv = 0;
	
	*ptwca_ptr = NULL;

	// -----------

	if( two_wire_cable_cooled_off_needs_distribution )
	{
		// 9/25/2013 rmd : Get the memory for the record we're going to send.
		if( mem_obtain_a_block_if_available( sizeof(UNS_32), (void**)ptwca_ptr ) )
		{
			memcpy( *ptwca_ptr, &two_wire_cable_cooled_off_index, sizeof(UNS_32) );
	
			rv += sizeof(UNS_32);
	
			// -----------
			
			// 8/23/2012 rmd : Clear the flag.
			two_wire_cable_cooled_off_needs_distribution = (false);
		}

	}

	return( rv );
}

/* ---------------------------------------------------------- */
// 12/03/2015 skc : Copy comm_mngr.token_date_time data into dt_ptr.
// Returns number of bytes allocated.
static UNS_32 FOAL_date_time_into_outgoing_token( UNS_8 **dt_ptr )
{
	UNS_32 rv = 0;

	*dt_ptr = NULL;

	if( comm_mngr.flag_update_date_time )
	{
		if(  mem_obtain_a_block_if_available( sizeof(DATE_TIME_TOKEN_STRUCT), (void**)dt_ptr ) )
		{
			// There's a small chance the keypress task is changing comm_mngr.
			xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

			memcpy( *dt_ptr, &comm_mngr.token_date_time, sizeof(DATE_TIME_TOKEN_STRUCT) );

			rv += sizeof(DATE_TIME_TOKEN_STRUCT);

			// clear the flag
			comm_mngr.flag_update_date_time = false;

			xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );
		}
	}

	return rv;
}
	
/* ---------------------------------------------------------- */
static UNS_32 load_real_time_weather_data_into_outgoing_token( UNS_8 **pweather_data_ptr )
{
	UNS_8	*ucp;

	UNS_32	rv;

	rv = 0;

	*pweather_data_ptr = NULL;

	// ----------

	if( WEATHER_get_et_gage_is_in_use() || WEATHER_get_rain_bucket_is_in_use() || WEATHER_get_wind_gage_in_use() )
	{
		if( mem_obtain_a_block_if_available( (sizeof(ET_TABLE_ENTRY) + sizeof(UNS_32) + sizeof(BOOL_32) + sizeof(RAIN_TABLE_ENTRY) + sizeof(UNS_32) + sizeof(BOOL_32) + sizeof(BOOL_32) + sizeof(UNS_32) + sizeof(BOOL_32)), (void**)pweather_data_ptr ) )
		{
			ucp = *pweather_data_ptr;

			memcpy( ucp, &weather_preserves.et_rip, sizeof(ET_TABLE_ENTRY) );
			ucp += sizeof(ET_TABLE_ENTRY);
			rv += sizeof(ET_TABLE_ENTRY);

			memcpy( ucp, &weather_preserves.remaining_gage_pulses, sizeof(UNS_32) );
			ucp += sizeof(UNS_32);
			rv += sizeof(UNS_32);

			memcpy( ucp, &weather_preserves.dont_use_et_gage_today, sizeof(BOOL_32) );
			ucp += sizeof(BOOL_32);
			rv += sizeof(BOOL_32);

			memcpy( ucp, &weather_preserves.rain.rip, sizeof(RAIN_TABLE_ENTRY) );
			ucp += sizeof(RAIN_TABLE_ENTRY);
			rv += sizeof(RAIN_TABLE_ENTRY);

			// 4/28/2015 rmd : This used to be the roll time to roll time pulse count. I switched to
			// mid-night to mid-night. That seems to relate better to the user.
			memcpy( ucp, &weather_preserves.rain.midnight_to_midnight_raw_pulse_count, sizeof(UNS_32) );
			ucp += sizeof(UNS_32);
			rv += sizeof(UNS_32);

			memcpy( ucp, &weather_preserves.rain_switch_active, sizeof(BOOL_32) );
			ucp += sizeof(BOOL_32);
			rv += sizeof(BOOL_32);

			memcpy( ucp, &weather_preserves.freeze_switch_active, sizeof(BOOL_32) );
			ucp += sizeof(BOOL_32);
			rv += sizeof(BOOL_32);
   		
			// 10/13/2014 ajv : The Wind Gage reading is an in-memory variable
			// which is set at the master by an incoming token response from a
			// controller with a -W option and a wind gage.
			memcpy( ucp, &GuiVar_StatusWindGageReading, sizeof(UNS_32) );
			ucp += sizeof(UNS_32);
			rv += sizeof(UNS_32);

			// 10/13/2014 ajv : The Wind Gage state is an in-memory variable
			// which is set at the master by an incoming token response from a
			// controller with a -W option and a wind gage.
			memcpy( ucp, &GuiVar_StatusWindPaused, sizeof(BOOL_32) );
			ucp += sizeof(BOOL_32);
			rv += sizeof(BOOL_32);
		}
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 __load_chain_members_array_into_outgoing_token( UNS_8 **pchain_members_ptr )
{
	UNS_32	rv;

	// ----------
	
	rv = 0;

	*pchain_members_ptr = NULL;
	
	// ----------
	
	if( comm_mngr.broadcast_chain_members_array )
	{
		*pchain_members_ptr = mem_malloc( sizeof(chain.members) );
		
		rv = sizeof(chain.members);

		comm_mngr.broadcast_chain_members_array = (false);

		// ----------

		xSemaphoreTakeRecursive( chain_members_recursive_MUTEX, portMAX_DELAY );
		
		memcpy( *pchain_members_ptr, &chain.members, sizeof(chain.members) );

		xSemaphoreGiveRecursive( chain_members_recursive_MUTEX );
	}

	return( rv );
}

/* ---------------------------------------------------------- */

// 2011.12.01 rmd : For debug purposes to measure the worst case time to make a token. Like
// when all 768 stations are to be xferred because we just hit their start time. The
// measured time seems to be VERY reasonable and clocks in under 10ms.
static UNS_32	__largest_token_generation_delta;

static UNS_32	__largest_token_size;

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Create the broadcast token. Sent out the flowsense chain for all slaves to
    to all slaves (irri machines) to receive. I believe there is always something to
    send. After all the slaves are waiting to hear from the master and if they don't will
    enter FORCED. We test for a zero length message.
	
	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task when kicking out the next
    token.

	@RETURN (none)

	@ORIGINAL 2012.08.08 rmd

	@REVISIONS (none)
*/
extern void FOAL_COMM_build_token__irrigation_token( void )
{
	BOOL_32		lnetwork_is_ready;

	// 5/8/2014 ajv : Make a local copy of whether the network is available
	// rather than using the GuiVar. Otherwise, if the state of the GuiVar
	// changed mid-stream, we would start sending data which the slave may not
	// be ready for.
	lnetwork_is_ready = COMM_MNGR_network_is_available_for_normal_use();

	// ----------

	// 2011.12.01 rmd : For debug purposes to measure how long it takes to do the processing.
	UNS_32	__this_time_delta;

	__this_time_delta = my_tick_count;

	// ----------

	// 10/2/2012 rmd : The comm_mngr structure can be read or written to by several tasks. Take
	// the change flags about the need to distribute an edit for example. They are written to by
	// the key processing task and read and written to by the comm_mngr task.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	// ----------

	UNS_32	content_length;
	
	content_length = 0;
	
	// ----------

	DATA_HANDLE	pdata_handle;
	
	UNS_32	build_results;
	
	if( comm_mngr.changes.distribute_changes_to_slave == (true) )
	{
		// 8/4/2016 rmd : Yes there are tokens to ourself, but tokens are broadcast so they don't
		// qualify for the large memory message buffer. Hence the (false) in the function call.
		build_results = PDATA_build_data_to_send( &pdata_handle, CHANGE_REASON_SYNC_DISTRIBUTING_TO_SLAVES, (false) );

		// 4/23/2015 rmd : !We can count on the handle being NULL'd and zeroed by the preceeding
		// function if no message to send!

		if( build_results == PDATA_BUILD_RESULTS_built_a_message )
		{
			// 4/23/2015 rmd : Message built. Nothing to do.
		}
		else
		if( build_results == PDATA_BUILD_RESULTS_no_data )
		{
			// 9/19/2013 ajv : Only reset the distribute_changes_to_slave flag
			// if we've sent everything there is to send.

			// 10/2/2012 rmd : MUTEX protected edit of this variable. So even if user is in the midst of
			// editing integrity is upheld.
			comm_mngr.changes.distribute_changes_to_slave = (false);
		}
		else
		if( build_results == PDATA_BUILD_RESULTS_no_memory )
		{
			// 4/23/2015 rmd : No memory right now. Nothing to do. Wait for next token to try again.
		}
	}
	else
	{
		pdata_handle.dlen = 0;
		
		pdata_handle.dptr = NULL;
	}

	content_length += pdata_handle.dlen;
	
	// ----------

	UNS_8	*chain_members_data_ptr;

	UNS_32	chain_members_data_length;

	chain_members_data_length = __load_chain_members_array_into_outgoing_token( &chain_members_data_ptr );

	content_length += chain_members_data_length;

	// ----------

	// 5/2/2014 rmd : And for the network availability criteria we add to our clean token count
	// if indeed we are not sending any pdata content or any chain members content. If none of
	// those bump the count. DO NOT set the count back to ZERO if there is some of that content.
	// Remember those things can show up during normal user editing at any time. This criteria
	// is supposed to manage the user activity only when coming out of a scan.
	if( !pdata_handle.dlen && !chain_members_data_length )
	{
		comm_mngr.since_the_scan__master__number_of_clean_tokens_made += 1;
	}
	else
	{
		// 5/5/2014 rmd : If we haven't yet reached the limit then clear back to 0. So the criteria
		// is "4 clean tokens IN A ROW". Has to be latching, once reaches limit stays above limit,
		// as the pdata content appears during normal real-time user editing activity.
		if( comm_mngr.since_the_scan__master__number_of_clean_tokens_made < REQUIRED_NUMBER_OF_CLEAN_TOKENS_SINCE_SCAN )
		{
			comm_mngr.since_the_scan__master__number_of_clean_tokens_made = 0;
			
			// 5/5/2014 rmd : And clean the clean the whole clean token responses rcvd array. So that
			// actually while the token keeps carry this type of content we don't let the slaves
			// token_resp count increment yet.
			memset( &comm_mngr.since_the_scan__master__number_of_clean_token_responses_rcvd, 0x00, sizeof( comm_mngr.since_the_scan__master__number_of_clean_token_responses_rcvd ) );
		}
	}

	// ----------

	UNS_8	*sfml_data_ptr;  // sfml = Station From Main List
	
	UNS_32	sfml_data_length;
	
	sfml_data_length = FOAL_IRRI_load_sfml_for_distribution_to_slaves( next_contact.index, &(sfml_data_ptr) );

	content_length += sfml_data_length;
	
	// ----------------	

	UNS_8	*action_needed_data_ptr;  // sxru = Station Xfer Record Union
	
	UNS_32	action_needed_data_length;
	
	action_needed_data_length = FOAL_IRRI_buildup_action_needed_records_for_token( &(action_needed_data_ptr) );

	content_length += action_needed_data_length;
	
	// ----------------	

	UNS_8	*stop_command_data_ptr;
	
	UNS_32	stop_command_data_length;
	
	stop_command_data_length = load_stop_key_record_into_outgoing_token( &stop_command_data_ptr );

	content_length += stop_command_data_length;
	
	// ----------------	

	UNS_8	*mlb_info_data_ptr;
	
	UNS_32	mlb_info_data_length;
	
	mlb_info_data_length = FOAL_IRRI_load_mlb_info_into_outgoing_token( &mlb_info_data_ptr );

	content_length += mlb_info_data_length;
	
	// ----------------	

	UNS_8	*box_current_ptr;
	
	UNS_32	box_current_length;
	
	box_current_length = load_box_current_into_outgoing_token( &box_current_ptr );

	content_length += box_current_length;
	
	// ----------------	

	UNS_8	*terminal_short_ptr;
	
	UNS_32	terminal_short_length;
	
	terminal_short_length = _nm_load_terminal_short_or_no_current_record_into_outgoing_token( &terminal_short_ptr );

	content_length += terminal_short_length;
	
	// ----------------	

	UNS_8	*twca_excessive_current_ptr;
	
	UNS_32	twca_excessive_current_length;
	
	twca_excessive_current_length = _nm_load_two_wire_cable_excessive_current_into_outgoing_token( &twca_excessive_current_ptr );

	content_length += twca_excessive_current_length;
	
	// ----------------	

	UNS_8	*twca_overheated_ptr;
	
	UNS_32	twca_overheated_length;
	
	twca_overheated_length = _nm_load_two_wire_cable_overheated_into_outgoing_token( &twca_overheated_ptr );

	content_length += twca_overheated_length;
	
	// ----------------	

	UNS_8	*twca_cooled_ptr;
	
	UNS_32	twca_cooled_length;
	
	twca_cooled_length = _nm_load_two_wire_cable_cooled_down_into_outgoing_token( &twca_cooled_ptr );

	content_length += twca_cooled_length;
	
	// ----------------	

	UNS_8	*real_time_weather_data_ptr;

	UNS_32	real_time_weather_data_length;

	real_time_weather_data_length = load_real_time_weather_data_into_outgoing_token( &real_time_weather_data_ptr );

	content_length += real_time_weather_data_length;

	// ----------------	

	UNS_8	*et_rain_tables_data_ptr;
	
	UNS_32	et_rain_tables_data_length;
	
	et_rain_tables_data_length = WEATHER_TABLES_load_et_rain_tables_into_outgoing_token( &et_rain_tables_data_ptr );

	content_length += et_rain_tables_data_length;
	
	// ------------

	UNS_8	*system_data_ptr;

	UNS_32	system_data_length;


	// 5/8/2014 ajv : If the network isn't ready yet, we should not try to
	// distribute system and POC flow information because the associated
	// preserves may not be sync'd yet.
	if( lnetwork_is_ready )
	{
		// 2012.02.15 rmd : We've got to distribute both the system and the poc flow rate
		// information. Maybe it's more than just flow rate information. How about system
		// information about number of flow meters, stable_flow or not, an indication of if the
		// system flow number is actual or simply sum of expecteds. And more stuff as needed.
		//
		// So for each system there may or may not be flow information. When would there not be?
		// Hmmmm. I think the answer to that is never. Because we always at least have the expected
		// flow rate. When stations are ON their individual flow rates are sent separetly.
		system_data_length = FOAL_FLOW_load_system_info_for_distribution_to_slaves( &(system_data_ptr) );
	}
	else
	{
		system_data_length = 0;

		system_data_ptr = NULL;
	}

	content_length += system_data_length;

	// ----------------	

	UNS_8	*poc_data_ptr;

	UNS_32	poc_data_length;

	// 5/8/2014 ajv : If the network isn't ready yet, we should not try to
	// distribute system and POC flow information because the associated
	// preserves may not be sync'd yet.
	if( lnetwork_is_ready )
	{
		poc_data_length = FOAL_FLOW_load_poc_info_for_distribution_to_slaves( &(poc_data_ptr) );
	}
	else
	{
		poc_data_length = 0;

		poc_data_ptr = NULL;
	}

	content_length += poc_data_length;

	// ----------------	

	// 7/29/2015 mpd : Added Lights ON list data to token.
	UNS_8	*lights_on_list_data_ptr;  

	UNS_32	lights_on_list_data_length;

	lights_on_list_data_length = FOAL_LIGHTS_load_lights_on_list_into_outgoing_token( &(lights_on_list_data_ptr) );

	content_length += lights_on_list_data_length;

	// ----------------	

	// 8/4/2015 mpd : Added Lights action needed list data to token.
	UNS_8	*lights_action_needed_list_data_ptr;  

	UNS_32	lights_action_needed_list_data_length;

	lights_action_needed_list_data_length = FOAL_LIGHTS_load_lights_action_needed_list_into_outgoing_token( &( lights_action_needed_list_data_ptr ) );

	content_length += lights_action_needed_list_data_length;

	// ----------------	

	// 11/30/2015 skc : Append new date time data to token.
	UNS_8	*date_time_data_ptr;

	UNS_32	date_time_data_length;

	date_time_data_length = FOAL_date_time_into_outgoing_token( &date_time_data_ptr );

	content_length += date_time_data_length;

	// ----------------	

	UNS_8	*alert_data_ptr;

	UNS_32	alert_data_length;

	DATE_TIME	ldt;

	alert_data_ptr = NULL;

	alert_data_length = 0;

	if( (ALERTS_need_to_sync == (true)) && (ALERTS_timestamps_are_synced() == (true)) )
	{
		ldt = ALERTS_get_oldest_timestamp();

		if( ldt.D > DATE_MINIMUM )
		{
			// 2012.07.26 ajv : Since using mem_malloc is blocked in alerts.c, we need to allocate the
			// memory here. If the memory's not available, skip the process this token.
			if( mem_obtain_a_block_if_available( ALERT_PILE_BYTES, (void**)&alert_data_ptr ) == (true) )
			{
				// 12/5/2012 rmd : Take the alerts MUTEX now. To envelope the process of choosing which
				// alerts to send and resetting the flag indicating a sync is required. While holding the
				// MUTEX another alert cannot be added.
				xSemaphoreTakeRecursive( alerts_pile_recursive_MUTEX, portMAX_DELAY );

				// 12/5/2012 rmd : Load the data where alert_data_ptr points to.
				alert_data_length = ALERTS_for_controller_to_controller_sync_return_size_of_and_build_alerts_to_send( alert_data_ptr, &alerts_struct_user, ldt );

				// 2012.07.26 ajv - While we still hold the MUTEX preventing the addition of another alert
				// clear the sync flag.
				ALERTS_need_to_sync = (false);
		
				xSemaphoreGiveRecursive( alerts_pile_recursive_MUTEX );
				
				// ----------
				
				content_length += alert_data_length;

				if( alert_data_length == 0 )
				{
					// 2012.07.26 ajv : Free the memory and set the ptr to NULL to signify nothing to send in
					// the message.
					mem_free( alert_data_ptr );

					alert_data_ptr = NULL;
				}
				else
				{
					// 2012.07.30 ajv : Include an additional UNS_32 to indicate the size of the alerts being
					// sent.
					content_length += sizeof(UNS_32);
				}
			}
			else
			{
				Alert_Message( "Unable to allocate memory for alerts" );
			}
		}
		else
		{
			// 2012.07.26 ajv : Don't do anything. Since we don't have the latest time stamp from this
			// controller, we'll request it when we build the actual token further down in this
			// function.
		}
	}
	
	// ----------------	

	#if 0

	// Now for the count of how many station flow records to send (the ones with the station's
	// share of the total flow) - the count is always there - it could be 0 however
	ldh.dlen += sizeof( stations_ON_uc );   // for the station by station numbers - it could be a 0 but its always included
	
	ldh.dlen += foal_irri.list_of_those_ON_by_controller[ next_contact.index ].count * sizeof( STATION_FLOW_RATE_TO_IRRI );

	// SWITCH INPUT CONTROL contribution
	l_sic_length_us = FOAL_analyze_switch_input_records_for_irri_machine_xfer( NULL, SIC_CMD_COUNT_ONLY );

	if ( l_sic_length_us > 0 ) {

		l_sic_send_a_record_uc = TRUE;
		
		ldh.dlen += l_sic_length_us;

	} else {

		l_sic_send_a_record_uc = FALSE;

	}


	if ( FOAL_rain_xmin_to_irri[ next_contact.index ].need_to_send == TRUE )
	{
		ldh.dlen += sizeof( RAIN_STRUCT_FOR_XMIN_TRANSPORT );
	}
	
	if ( FOAL_rain_slot1_to_irri[ next_contact.index ].need_to_send == TRUE )
	{
		ldh.dlen += sizeof( RAIN_STRUCT_FOR_SLOT1_TRANSPORT );
	}
	
	
	// now the contribution from wind
	if( WIND_number_of_wgs_on_the_chain() > 0 )
	{
		ldh.dlen += sizeof( WIND_GAGE_COMMUNICATIONS_RECORD );
	}
	#endif
	
	/* ---------------------------------------------------------- */
	/* ---------------------------------------------------------- */

	// 5/8/2014 rmd : ALWAYS build the token. Even if NO bits are set in pieces and/or there is
	// NO payload to the message besides the FOAL_COMMAND structure. Why? So the slave always
	// receives a token and we always get a response. Otherwise we need to rethink several
	// issues least of which is starting the response timer. We have no implementation to SKIP
	// over a token just because it is empty.
	// Maintain accumulators.

	comm_stats.tokens_generated += 1;

	// -----------
	
	UNS_8			*ucp;

	content_length += sizeof( FOAL_COMMANDS );
	
	// 8/14/2012 rmd : Allocate it!
	ucp = mem_malloc( content_length );

	// -----------

	if( content_length > __largest_token_size )
	{
		__largest_token_size = content_length;
		
		Alert_largest_token_size( __largest_token_size, (false) );
	}

	// -----------
	
	// 8/14/2012 rmd : Set the HANDLE the COMM_MNGR uses to actually transmit the message.
	next_contact.message_handle.dptr = ucp;

	next_contact.message_handle.dlen = content_length;

	// -----------
	
	// 4/11/2012 rmd : Because of potential alignment issues do not write directly to the
	// message area. Always use memcpy. So you see this for the FOAL_COMMANDS structure. We'll
	// actually put fc into place last thing, after the message is built.
	FOAL_COMMANDS	fc, *fc_ptr;
	
	fc.mid = next_contact.command_to_use;
	fc.pieces = 0;    // start clean

	// 4/12/2012 rmd : Capture where fc needs to be placed after building the message.
	fc_ptr = (FOAL_COMMANDS*)ucp;
	
	ucp += sizeof( FOAL_COMMANDS );  // point to the first place to put data after the header

	// ----------
	
	if( (pdata_handle.dlen > 0) && (pdata_handle.dptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_I_here_is_program_data );

		memcpy( ucp, pdata_handle.dptr, pdata_handle.dlen );

		ucp += pdata_handle.dlen;

		mem_free( pdata_handle.dptr );
	}

	// ----------

	if( (chain_members_data_length > 0) && (chain_members_data_ptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_I_here_is_the_chain_members_array );

		memcpy( ucp, chain_members_data_ptr, chain_members_data_length );

		ucp += chain_members_data_length;

		mem_free( chain_members_data_ptr );
	}			

	// ----------
	
	// Send the stations newly added to the main irrigation list.
	if( (sfml_data_length > 0) && (sfml_data_ptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_I_stations_for_your_list );

		memcpy( ucp, sfml_data_ptr, sfml_data_length );
		
		ucp += sfml_data_length;
		
		mem_free( sfml_data_ptr );
	}			

	// -----------
	
	if( (action_needed_data_length > 0) && (action_needed_data_ptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_I_here_are_the_action_needed_records );

		memcpy( ucp, action_needed_data_ptr, action_needed_data_length );
		
		ucp += action_needed_data_length;
		
		mem_free( action_needed_data_ptr );
	}			

	// -----------
	
	if( (stop_command_data_length > 0) && (stop_command_data_ptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_I_here_is_a_stop_command );

		memcpy( ucp, stop_command_data_ptr, stop_command_data_length );
		
		ucp += stop_command_data_length;
		
		mem_free( stop_command_data_ptr );
	}			

	// -----------
	
	if( (mlb_info_data_length > 0) && (mlb_info_data_ptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_I_here_is_the_mlb_info );

		memcpy( ucp, mlb_info_data_ptr, mlb_info_data_length );
		
		ucp += mlb_info_data_length;
		
		mem_free( mlb_info_data_ptr );
	}			

	// -----------
	
	if( (box_current_length > 0) && (box_current_ptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_I_here_are_box_currents );

		memcpy( ucp, box_current_ptr, box_current_length );
		
		ucp += box_current_length;
		
		mem_free( box_current_ptr );
	}			

	// ----------------
	
	if( (terminal_short_length > 0) && (terminal_short_ptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_I_here_is_the_terminal_short_ACK_or_no_current );

		memcpy( ucp, terminal_short_ptr, terminal_short_length );
		
		ucp += terminal_short_length;
		
		mem_free( terminal_short_ptr );
	}			

	// ----------------
	
	if( (twca_excessive_current_length > 0) && (twca_excessive_current_ptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_I_two_wire_cable_excessive_current_ack_from_master );

		memcpy( ucp, twca_excessive_current_ptr, twca_excessive_current_length );
		
		ucp += twca_excessive_current_length;

		mem_free( twca_excessive_current_ptr );
	}			

	// ----------------
	
	if( (twca_overheated_length > 0) && (twca_overheated_ptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_I_two_wire_cable_overheated_ack_from_master );

		memcpy( ucp, twca_overheated_ptr, twca_overheated_length );
		
		ucp += twca_overheated_length;
		
		mem_free( twca_overheated_ptr );
	}

	// ----------------
	
	if( (twca_cooled_length > 0) && (twca_cooled_ptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_I_two_wire_cable_cooled_off_from_master );

		memcpy( ucp, twca_cooled_ptr, twca_cooled_length );
		
		ucp += twca_cooled_length;
		
		mem_free( twca_cooled_ptr );
	}

	// ----------------

	if( (real_time_weather_data_length > 0) && (real_time_weather_data_ptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_I_here_is_real_time_weather_data_to_display );

		memcpy( ucp, real_time_weather_data_ptr, real_time_weather_data_length );

		ucp += real_time_weather_data_length;

		mem_free( real_time_weather_data_ptr );
	}			

	// ----------------

	if( (et_rain_tables_data_length > 0) && (et_rain_tables_data_ptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_I_here_is_the_et_rain_tables );

		memcpy( ucp, et_rain_tables_data_ptr, et_rain_tables_data_length );
		
		ucp += et_rain_tables_data_length;
		
		mem_free( et_rain_tables_data_ptr );
	}			

	// ----------------

	if( weather_preserves.rain_switch_active == (true) )
	{
		B_SET( fc.pieces, BIT_TI_I_rain_switch_active );
	}

	// ----------------

	if( weather_preserves.freeze_switch_active == (true) )
	{
		B_SET( fc.pieces, BIT_TI_I_freeze_switch_active );
	}

	// ----------------

	if( weather_preserves.clear_runaway_gage == (true) )
	{
		B_SET( fc.pieces, BIT_TI_I_here_is_the_clear_runaway_gage_command );

		weather_preserves.clear_runaway_gage = (false);
	}

	// ----------------

	// 4/3/2013 ajv : TODO We need to build a timer which triggers the CRC
	// verification process.
	BOOL_32	temp;

	temp = (false);

	if( temp == (true) )
	{
		B_SET( fc.pieces, BIT_TI_I_send_me_crc_list );

		temp = (false);
	}

	// -----------
	
	// Plunk in the information for all systems.
	if( (system_data_length > 0) && (system_data_ptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_I_here_is_the_system_info );

		memcpy( ucp, system_data_ptr, system_data_length );

		ucp += system_data_length;

		mem_free( system_data_ptr );
	}

	// -----------

	// Plunk in the information for all pocs.
	if( (poc_data_length > 0) && (poc_data_ptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_I_here_is_the_poc_info );

		memcpy( ucp, poc_data_ptr, poc_data_length );

		ucp += poc_data_length;

		mem_free( poc_data_ptr );
	}

	// ----------

	// 7/29/2015 mpd : Send the lights newly added to the ON list.
	if( (lights_on_list_data_length > 0) && (lights_on_list_data_ptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_L_lights_for_on_list );

		memcpy( ucp, lights_on_list_data_ptr, lights_on_list_data_length );

		ucp += lights_on_list_data_length;

		mem_free( lights_on_list_data_ptr );
	}			

	// -----------

	// 7/29/2015 mpd : Send lights on the action needed list.
	if( (lights_action_needed_list_data_length > 0) && (lights_action_needed_list_data_ptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_L_lights_for_action_needed_list );

		memcpy( ucp, lights_action_needed_list_data_ptr, lights_action_needed_list_data_length );

		ucp += lights_action_needed_list_data_length;

		mem_free( lights_action_needed_list_data_ptr );
	}			

	// -----------

	if( (date_time_data_length > 0) && (date_time_data_ptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_I_here_is_date_time );

		memcpy( ucp, date_time_data_ptr, date_time_data_length );

		ucp += date_time_data_length;

		mem_free( date_time_data_ptr );
	}

	if( comm_mngr.perform_two_wire_discovery )
	{
		// 5/9/2016 ajv : Currently, no data is associated with this message. However, we may opt to
		// include a particular controller's box index in the future. Command Center Online already
		// includes this so it should be straightforward to implement if desired later.
		B_SET( fc.pieces, BIT_TI_I_perform_two_wire_discovery );

		// 5/9/2016 ajv : And clear the flag.
		comm_mngr.perform_two_wire_discovery = (false);
	}

	// -----------

	if( (alert_data_length > 0) && (alert_data_ptr != NULL) )
	{
		B_SET( fc.pieces, BIT_TI_I_here_are_latest_alerts );

		// First, add the actual length of the data to simplify the parsing
		// on the receiving end.
		memcpy( ucp, &alert_data_length, sizeof(alert_data_length) );
		ucp += sizeof(alert_data_length);

		// Then add the actual alerts, ordered oldest to newest.
		memcpy( ucp, alert_data_ptr, alert_data_length );
		ucp += alert_data_length;

		mem_free( alert_data_ptr );
	}
	else
	if( (ALERTS_need_to_sync == (true)) && (ALERTS_need_latest_timestamp_for_this_controller( next_contact.index ) == (true)) )
	{
		B_SET( fc.pieces, BIT_TI_I_send_me_latest_alert_timestamp );
	}

	// -----------
	
	#if 0
	// 608.rmd
	// now we send the CONTROLLER_INFO_IN_THE_TOKEN on a case by case basis if the controller is part of the chain
	// this effort dramatically reduces the size of the token
	//
	// put in the count of how many CONTROLLER_INFO_IN_THE_TOKEN records are coming
	//
	memcpy( ucp, &controller_info_count_uc, sizeof( controller_info_count_uc ) );
	ucp += sizeof( controller_info_count_uc );

	for ( i=0; i<MAX_CHAIN_LENGTH; i++ )
	{
		if( iccc.pci.whose_active[ i ] == (true) )
		{
			// set the INDEX - can't forget to do this at some point - I know once it is done we
			// technically never have to do it again [could do this once at startup init I suppose] BUT
			// do it here to make the point
			//
			FoalFlow.controller_info[ i ].info.index = i;

			memcpy( ucp, &FoalFlow.controller_info[ i ], sizeof( CONTROLLER_INFO_IN_THE_TOKEN ) );
			ucp += sizeof( CONTROLLER_INFO_IN_THE_TOKEN );

			// a sanity check is to decrement this then test for 0 when done
			controller_info_count_uc -= 1;
		}
	}

	// A sanity check.
	if( controller_info_count_uc != 0 )
	{
		Alert_Message( "FOAL_COMM: controller_info NOT 0!" );
	}

	#endif
	



	#if 0
	///////// STEP: ADD the records of proportioned flow rate for those valves ON

	stations_on_uc = foal_irri.list_of_those_ON_by_controller[ next_contact.index ].count;

	// tell the irri machine how many records are coming
	memcpy( ucp, &stations_on_uc, sizeof( stations_on_uc ) );
	ucp += sizeof( stations_on_uc );

	flow_ilc = nm_ListGetFirst( &foal_irri.list_of_those_ON_by_controller[ next_contact.index ] );

	while( flow_ilc != NULL )
	{
		sfrti.snumber = flow_ilc->station_number_0_u8;
		
		sfrti.sflow_fl = flow_ilc->share_of_real_flow_rate_fl;
		
		memcpy( ucp, &sfrti, sizeof( STATION_FLOW_RATE_TO_IRRI ) );
		ucp += sizeof( STATION_FLOW_RATE_TO_IRRI );
		
		flow_ilc = nm_ListGetNext( &foal_irri.list_of_those_ON_by_controller[ next_contact.index ], flow_ilc );
	}

	#endif


	#if 0
	//////// STEP 6: ADD THE PUMP CONTROL
	if( FoalFlow.PumpShouldBeOn == TRUE ) {
	
		// If we don't test against the system level short for the MV or PUMP we will continue
		// to request the pump output at the irri machine be energized. This is mostly obvious
		// when 1 station is on, we short the MV. The irri machine tells the foal machine about
		// the short and then the irri machine in turn tells the foal machine to kill all the
		// stations in the list. Well there is a 5 second timer that keeps the MV and PUMP going
		// even after all the stations are gone and off (to prevent MV chatter during station
		// changes). We could kill the MV when the FOAL machine receives notice of the short except
		// it may reopen before we kill the stations - seperate thread controls opening it based
		// on who's on. We could kill it when we get the KILL messages for the stations but that
		// is an ackward thing to do. No...this is the best way to cover this issue.
		//
		// Test for the short...
		//
		if ( (foal_comm.SHORT_SYSTEM_MV == FALSE) && (foal_comm.SHORT_SYSTEM_PUMP == FALSE) ) {

			// then tell the IRRI machines the pump should be on
			//
			B_SET( fc->pieces, BIT_TI_I_turn_on_pump);  // set this bit

		}

	}


	//////// STEP 7: ADD THE SWITCH INPUT CONTROL RECORD (THIS INCLUDES RAIN SWITCH) TO THE IRRI MACHINES
	if ( l_sic_send_a_record_uc == TRUE ) {

		// use the local variable to be sure we actuall allocated the space (in case somehow the foal_irri.sic_records changed between then and now ie RTOS)

		// the rain switch works like this - first the irri controller with the switch
		// (or multiple controllers - doesn't matter) tell the foal machine the rain switch
		// is active. The foal machine keeps a timer that is set restarted
		// each time an irri machine has the bit set - when the timer expires it sets 
		// foalirri.RainSwitchSaysRain inactive. (SIC works this way too)
		//
		// Each contact the foal machine simply passes the rain switch status to the irri machine
		// which then checks its irri list for any removals necessary and also sets its own internal
		// rain switch status which is used during start time detection.
		//
		// To the irri machine absence of this record means NO SWITCH COMMAND (NO RAIN included)
		//
		// it's okay to allocate space for and not use so test again (in case it changed ie RTOS)
		//
		if ( FOAL_analyze_switch_input_records_for_irri_machine_xfer( NULL, SIC_CMD_COUNT_ONLY ) > 0 ) {

			B_SET( fc->pieces, BIT_TI_I_here_is_a_switch_control_record );  // set this bit

			FOAL_analyze_switch_input_records_for_irri_machine_xfer( &ucp, SIC_CMD_COUNT_AND_MOVE );

		}

	}
	#endif


	#if 0
	
	if ( FOAL_rain_xmin_to_irri[ next_contact.index ].need_to_send == TRUE ) {
	
		B_SET( fc->pieces, BIT_TI_I_crossed_minimum_rain );
		
		memcpy( ucp, &FOAL_rain_xmin_to_irri[ next_contact.index ], sizeof( RAIN_STRUCT_FOR_XMIN_TRANSPORT ) );
		
		ucp += sizeof( RAIN_STRUCT_FOR_XMIN_TRANSPORT );
		
		
		FOAL_rain_xmin_to_irri[ next_contact.index ].need_to_send = FALSE;
		
	}
	
	if ( FOAL_rain_slot1_to_irri[ next_contact.index ].need_to_send == TRUE ) {
	
		B_SET( fc->pieces, BIT_TI_I_here_is_rain_1 );
		
		memcpy( ucp, &FOAL_rain_slot1_to_irri[ next_contact.index ], sizeof( RAIN_STRUCT_FOR_SLOT1_TRANSPORT ) );
		
		ucp += sizeof( RAIN_STRUCT_FOR_SLOT1_TRANSPORT );
		
		
		FOAL_rain_slot1_to_irri[ next_contact.index ].need_to_send = FALSE;
		
	}
	
	if ( FOAL_et_slot1_to_irri[ next_contact.index ].need_to_send == TRUE ) {
	
		B_SET( fc->pieces, BIT_TI_I_here_is_et_1 );
		
		memcpy( ucp, &FOAL_et_slot1_to_irri[ next_contact.index ], sizeof( ET_STRUCT_FOR_SLOT1_TRANSPORT ) );
		
		ucp += sizeof( ET_STRUCT_FOR_SLOT1_TRANSPORT );
		
		
		FOAL_et_slot1_to_irri[ next_contact.index ].need_to_send = FALSE;
		
	}
	

	// ADD THE CONTRIBUTION FROM WIND if any
	if ( WIND_number_of_wgs_on_the_chain() > 0 )
	{
		B_SET( fc->pieces, BIT_TI_I_wind_speed_record );  // set this bit
		
		WIND_set_the_comm_record_up( &wgcr );
		
		memcpy( ucp, &wgcr, sizeof( wgcr ) );
		
		ucp += sizeof( wgcr );
	}
	
	#endif

	// ----------
	
	// 4/11/2012 rmd : And finally put fc into place.
	memcpy( fc_ptr, &fc, sizeof(FOAL_COMMANDS) );
	
	// ----------

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );

	// ----------
		
	__this_time_delta = (my_tick_count - __this_time_delta);
	
	if( __this_time_delta > __largest_token_generation_delta )
	{
		__largest_token_generation_delta = __this_time_delta;
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

