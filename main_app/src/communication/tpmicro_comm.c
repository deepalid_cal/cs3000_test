/*  file = tpmicro_comm.c                      2012.04.02 rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"tpmicro_comm.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"bithacks.h"

#include	"irri_irri.h"

#include	"crc.h"

#include	"comm_mngr.h"

#include	"battery_backed_vars.h"

#include	"r_alerts.h"

#include	"configuration_controller.h"

#include	"configuration_network.h"

#include	"tpmicro_data.h"

#include	"packet_router.h"

#include	"flash_storage.h"

#include	"change.h"

#include	"irri_comm.h"

#include	"weather_control.h"

#include	"serport_drvr.h"

#include	"dialog.h"

#include	"two_wire_utils.h"

#include	"rcvd_data.h"

#include	"tpmicro_isp_mode.h"

#include	"flowsense.h"

#include	"code_distribution_task.h"

#include	"d_two_wire_discovery.h"

#include	"cs_mem.h"

#include	"d_process.h"

#include	"bypass_operation.h"

#include	"station_groups.h"

#include	"irri_lights.h"

#include	"moisture_sensors.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/19/2012 rmd : Where the received data gets placed. The array is filled with spaces upon
// receipt of each screen from the tpmicro. And the message fills in lines with the data it
// provided. The +1 in the definition is to make sure we have room for a string ending NULL.
UNS_8	TPMICRO_debug_text[ TPMICRO_DEBUG_MAX_LINES_OF_TEXT ][ TPMICRO_DEBUG_TEXT_LINE_WIDTH ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/8/2012 rmd : Contains the data managing the dynamic communications link between the
// main board and the tp_micro. Does not need to be battery backed.
TPMICRO_COMM_STRUCT		tpmicro_comm;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static BOOL_32 extract_flow_meter_readings_out_of_msg_from_tpmicro( unsigned char **pucp )
{
	// 5/27/2014 rmd : Executed within the context of the comm_mngr task. First byte contains
	// how many records. Each record owner should be able to be found within the poc_preserves.

	// ----------
	
	BOOL_32	rv;

	// 4/5/2012 rmd : The first byte is the number of terminal or deocders.
	UNS_8	number_in_the_message;

	UNS_32	ppp, this_decoders_level;
	
	FLOW_COUNT_XFER_FROM_THE_TPMICRO_STRUCT		fcxm;

	POC_DECODER_OR_TERMINAL_WORKING_STRUCT		*wds;

	BY_POC_RECORD	*lbpr_ptr;
	
	BYPASS_EVENT_QUEUE_STRUCT	beqs;

	// ----------
	
	rv = (true);  // assume success

	number_in_the_message = *(*pucp);

	*pucp += sizeof(UNS_8);

	// ----------

	// 7/10/2014 rmd : This could actually fail if we had a bypass and 11 other single story
	// pocs. As that would deliver 14 flow meter readings. But I'll leave as that sure is one
	// heck of an extreme case.
	if( number_in_the_message > MAX_POCS_IN_NETWORK )
	{
		Alert_Message( "TP COMM: msg contains too many pocs" );

		rv = (false);
	}
	else
	{
		// 4/5/2012 rmd : Take the poc_preserves mutex as we are manipulating the rcvd flow values.
		xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

		for( ppp=0; ppp<number_in_the_message; ppp++ )
		{
			memcpy( &fcxm, *pucp, sizeof(FLOW_COUNT_XFER_FROM_THE_TPMICRO_STRUCT) );
			
			*pucp += sizeof(FLOW_COUNT_XFER_FROM_THE_TPMICRO_STRUCT);
			
			// ----------
			
			lbpr_ptr = NULL;

			wds = POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters( FLOWSENSE_get_controller_index(), fcxm.serial_number, &lbpr_ptr, &this_decoders_level );

			if( !wds || !lbpr_ptr )
			{
				// 6/5/2014 rmd : It is possible to have decoder based POC's that have not been assigned to
				// a poc file 'level' poc. They may be on the cable but not used. So this is really not an
				// error. So we should be careful with this alert as it could become a nuisance. But one
				// particular case of this we should always alert for. If the UNASSIGNED decoder if
				// reporting non-zero flow! That would mean a flow meter was wired up. Why would that be if
				// not being used?
				if( fcxm.fm_accumulated_pulses )
				{
					ALERT_MESSAGE_WITH_FILE_NAME( "TP_COMM: poc not found in preserves" );
	
					Alert_Message_va( "sn: %u , count/ms: %u / %u , five: %u", fcxm.serial_number, fcxm.fm_accumulated_pulses, fcxm.fm_accumulated_ms, fcxm.fm_latest_5_second_count );
				}
				
				// 7/14/2014 rmd : I don't think this should be flagged as a message failure. Meaning do not
				// set rv (false). That would cause the abandonment of the balance of the message from the
				// tpmicro. Perhaps with a resultant functional issue of the system. Just cause we can't
				// find the POC in the preserves.
			}
			else
			{
				// 2/12/2015 rmd : alert message for debug
				//Alert_Message_va( "sn: %u , count/ms: %u / %u , five: %u", fcxm.serial_number, fcxm.fm_accumulated_pulses, fcxm.fm_accumulated_ms, fcxm.fm_latest_5_second_count );

				// 6/4/2014 rmd : We don't know if more than ONE message will arrive from the tpmicro
				// carrying flow data for each token response that we build. So we must add to the collected
				// information for the pulses. For the 5 second overwrite the prior value. But only add to
				// the ms if there are pulses otherwise we would be gathering ms without an accompanying
				// count. And if subsequently count came in the next delivery (before a token response was
				// made) we end up in a state where there are MORE ms than there should be for the count. So
				// test if there is a count before adding.
				if( fcxm.fm_accumulated_pulses )
				{
					wds->fm_accumulated_pulses_irri += fcxm.fm_accumulated_pulses;
	
					wds->fm_accumulated_ms_irri += fcxm.fm_accumulated_ms;
				}
				
				// 6/4/2014 rmd : And always overwrite the 5 second pulse tally to reflect the latest.
				wds->fm_latest_5_second_pulse_count_irri = fcxm.fm_latest_5_second_count;
				
				// 1/15/2015 rmd : And overwrite the delta and seconds since. The result is we are using the
				// latest representation of flow.
				wds->fm_delta_between_last_two_fm_pulses_4khz_irri = fcxm.fm_delta_between_last_two_fm_pulses_4khz;

				wds->fm_seconds_since_last_pulse_irri = fcxm.fm_seconds_since_last_pulse;
				
				// ----------
				
				// 7/2/2014 rmd : alert line for development only
				//Alert_Message_va( "sn: %u , count/ms: %u / %u , five: %u", fcxm.serial_number, fcxm.accumulated_pulses, fcxm.accumulated_ms, fcxm.latest_5_second_count );
				
				// ----------
				
				// 6/4/2014 rmd : And flag to move to master. Data arrived from the tpmicro so we should set
				// the falg to send to the master when building the next token response.
				wds->pbf.there_is_flow_meter_count_data_to_send_to_the_master = (true);
				
				// ----------

				// 7/10/2014 rmd : If this is part of a BYPASS we need to feed the bypass task this
				// information.
				if( lbpr_ptr->poc_type__file_type == POC__FILE_TYPE__DECODER_BYPASS )
				{
					beqs.event = BYPASS_EVENT_flow_reading_update;
					
					beqs.decoder_serial_number = fcxm.serial_number;

					beqs.fm_latest_5_second_count = fcxm.fm_latest_5_second_count;

					beqs.fm_delta_between_last_two_fm_pulses_4khz = fcxm.fm_delta_between_last_two_fm_pulses_4khz;

					beqs.fm_seconds_since_last_pulse = fcxm.fm_seconds_since_last_pulse;
					
					if( xQueueSendToBack( BYPASS_event_queue, &beqs, 0 ) != pdPASS )
					{
						ALERT_MESSAGE_WITH_FILE_NAME( "BYPASS queue full" );
					}
				}  // of it is a bypass poc

			}  // of we found the poc in the preserves

		}
		
		// ----------
		
		xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

	}  // of the number of pocs is within range

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 extract_moisture_reading_out_of_msg_from_tpmicro( unsigned char **pucp )
{
	// 5/27/2014 rmd : Executed within the context of the comm_mngr task. There is just a single
	// moisture reading. Which we need to find and attach to the decoder it came from.

	// ----------
	
	BOOL_32	rv;
	
	UNS_32	decoder_serial_number;

	MOISTURE_SENSOR_DECODER_RESPONSE	reading;
	
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;
	
	// ----------
	
	rv = (true);  // assume success

	// ----------
	
	memcpy( &decoder_serial_number, *pucp, sizeof(UNS_32) );

	*pucp += sizeof(UNS_32);


	memcpy( &reading, *pucp, sizeof(MOISTURE_SENSOR_DECODER_RESPONSE) );

	*pucp += sizeof(MOISTURE_SENSOR_DECODER_RESPONSE);

	// ----------

	// 2/22/2016 rmd : So this message came from the local TPMicro. We now need to get this
	// reading to the master (FOAL side). First let's see if we can find it in OUR file list.
	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	lmois = nm_MOISTURE_SENSOR_get_pointer_to_file_moisture_sensor_with_this_box_index_and_decoder_serial_number( FLOWSENSE_get_controller_index(), decoder_serial_number );
	
	if( lmois != NULL )
	{
		nm_MOISTURE_SENSOR_set_new_reading_slave_side_variables( &reading, lmois );
	}
	else
	{
		Alert_Message( "MOISTURE group not found!" );
	}
	
	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @EXECUTED Executed within the context of the COMM_MNGR task. When parsing the tp_micro
    incoming message.

	@RETURN (false) if the data was determined to be out of range. Otherwise (true).

	@ORIGINAL 2012.08.08 rmd

	@REVISIONS (none)
*/
static BOOL_32 extract_ma_readings_out_of_msg_from_tpmicro( unsigned char **pucp )
{
/*
  ----------------------------------------------------------
  Current Data Packet Format
  ----------------------------------------------------------
    | # number of device types							//U8
	|
	| |----------------------
	| | type of Device  								//U8
	| |
	| | |------------------
	| | | # of readings for device  					//U8
	| | |
	| | | |------------------
	| | | | index of device 							//U8
	| | | |
	| | | | (reading)   								//U32
	| | | | ...
	| | | | ...
	| | | | ...
	| | | | index of device 							//U8
	| | | |
	| | | | (reading)   								//U32
	| | | | ...
	| | | | ...
	| | | | ...
	| | | | index of device 							//U8
	| | | |
	| | | | (reading)   								//U32
	| | | |------------------
	| | |
	| | |--------------------
    | |
	| |----------------------
	|
	-------------------------
*/
	// ----------------------

	POC_DECODER_OR_TERMINAL_WORKING_STRUCT	*ws_ptr;
	
	BOOL_32	rv;

	UNS_8	number_of_device_types;

	UNS_8	device_type, device_readings;
	
	UNS_32	this_decoders_level;
	
	BY_POC_RECORD	*bpr_ptr;

	// ----------

	rv = (true);  // assume success

	number_of_device_types = *(*pucp);
	(*pucp)++;

	while( (number_of_device_types > 0) && (rv == (true)) )
	{
		device_type = *(*pucp);
		(*pucp)++;

		device_readings = *(*pucp);
		(*pucp)++;

		if( device_type == OUTPUT_TYPE_TERMINAL_STATION )
		{
			while( (device_readings > 0) && (rv == (true)) )
			{
				UNS_8	station_number_0_based;

				station_number_0_based = *(*pucp);
				(*pucp)++;

				UNS_32	milli_amps;

				// 8/8/2012 rmd : To avoid potential data abort exception use memcpy. It is the safe way to
				// get multi-byte data out of the message.
				memcpy( &milli_amps, *pucp, sizeof(UNS_32) );
				*pucp += sizeof(UNS_32);

				if( station_number_0_based > MAX_CONVENTIONAL_STATIONS )
				{
					rv = (false);
				}
				else
				{
					UNS_32	preserves_index;
					
					UNS_16	ma_uns_16;
					
					// 8/8/2012 rmd : The station must be for this box and therefore use our own box index.
					if( STATION_PRESERVES_get_index_using_box_index_and_station_number( FLOWSENSE_get_controller_index(), station_number_0_based, &preserves_index ) == (true) )
					{
						// 8/8/2012 rmd : As we are going to write to the preserves lock them.
						xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

						// 5/6/2014 rmd : Because ma is an UNS_16 in the station preserves make sure the tests and
						// assignement go well using this intermediate variable.
						ma_uns_16 = milli_amps;

						// 8/8/2012 rmd : In order to prevent unecessary network traffic only distribute the ma
						// readings if it is different.
						if( station_preserves.sps[ preserves_index ].last_measured_current_ma != ma_uns_16 )
						{
							station_preserves.sps[ preserves_index ].last_measured_current_ma = ma_uns_16;

							station_preserves.sps[ preserves_index ].spbf.distribute_last_measured_current_ma = (true);
						}

						xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
					}
					else
					{
						rv = (false);
					}

				}  // of if the station number is within range

				device_readings -= 1;

			}  // of while more readings and rv still (true)

		}
		else
		if( device_type == OUTPUT_TYPE_TERMINAL_LIGHT )
		{
			while( (device_readings > 0) && (rv == (true)) )
			{
				UNS_8	lights_number;

				lights_number = *(*pucp);
				(*pucp)++;

				UNS_32	milli_amps;

				// 8/8/2012 rmd : To avoid potential data abort exception must use memcpy. It is the safe
				// way to get multi-byte data out of the message.
				memcpy( &milli_amps, *pucp, sizeof(UNS_32) );
				*pucp += sizeof(UNS_32);

				// 8/8/2012 rmd : Right now we're not doing anything with the lights current draw readings.

				device_readings -= 1;

			}  // of while more readings and rv still (true)

		}
		else
		if( device_type == OUTPUT_TYPE_TERMINAL_MASTER_VALVE )
		{
			// 8/9/2012 rmd : For the terminal MV the count of the device readings is present within the
			// message and MUST be 1. Which we'll test for. The so called 'index' is not present for the
			// terminal MV (or the terminal pump).
			if( device_readings != 1 )
			{
				rv = (false);
			}
			else
			{
				UNS_32	milli_amps;

				// 8/8/2012 rmd : To avoid potential data abort exception must use memcpy. It is the safe
				// way to get multi-byte data out of the message.
				memcpy( &milli_amps, *pucp, sizeof(UNS_32) );

				*pucp += sizeof(UNS_32);

				// 8/8/2012 rmd : Now roll through the poc_preserves looking for the terminal type poc to
				// set its last measured current. Take the MUTEX to assure stability.
				xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

				ws_ptr = POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters( FLOWSENSE_get_controller_index(), 0, &bpr_ptr, &this_decoders_level );
				
				if( ws_ptr != NULL )
				{
					// 8/9/2012 rmd : If the measurement has changed set the flag and assign it.
					if( milli_amps != ws_ptr->mv_last_measured_current_from_the_tpmicro_ma )
					{
						ws_ptr->mv_last_measured_current_from_the_tpmicro_ma = milli_amps;

						ws_ptr->pbf.send_mv_pump_milli_amp_measurements_to_the_master = (true);
					}
				}
				else
				{
					ALERT_MESSAGE_WITH_FILE_NAME( "poc preserves not found" );
				}
				
				xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

			}  // of the device_readings is indeed 1

		}
		else
		if( device_type == OUTPUT_TYPE_TERMINAL_PUMP )
		{
			// 8/9/2012 rmd : For the terminal PUMP the count of the device readings is present within
			// the message and MUST be 1. Which we'll test for. The so called 'index' is not present for
			// the terminal PUMP.
			if( device_readings != 1 )
			{
				rv = (false);
			}
			else
			{
				UNS_32	milli_amps;

				// 8/8/2012 rmd : To avoid potential data abort exception must use memcpy. It is the safe
				// way to get multi-byte data out of the message.
				memcpy( &milli_amps, *pucp, sizeof(UNS_32) );
				*pucp += sizeof(UNS_32);

				// 8/8/2012 rmd : Now roll through the poc_preserves looking for the terminal type poc to
				// set its last measured current. Take the MUTEX to assure stability.
				xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

				ws_ptr = POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters( FLOWSENSE_get_controller_index(), 0, &bpr_ptr, &this_decoders_level );
				
				if( ws_ptr != NULL )
				{
					// 8/9/2012 rmd : If the measurement has changed set the flag and assign it.
					if( milli_amps != ws_ptr->pump_last_measured_current_from_the_tpmicro_ma )
					{
						ws_ptr->pump_last_measured_current_from_the_tpmicro_ma = milli_amps;

						ws_ptr->pbf.send_mv_pump_milli_amp_measurements_to_the_master = (true);
					}
				}
				else
				{
					ALERT_MESSAGE_WITH_FILE_NAME( "poc preserves not found" );
				}

				xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

			}  // of the device_readings is indeed 1

		}
		else
		{
			rv = (false);
		}

		number_of_device_types -= 1;
	}

	if( rv == (false) )
	{
		Alert_message_on_tpmicro_pile_M( "rcvd current draw content out of range." );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Pulls the what is installed structure from the incoming tpmicro
    message.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task. When the tp_micro incoming
    message delivers the structure depicting what is installed.

	@RETURN (none)

	@ORIGINAL 2012.08.08 rmd

	@REVISIONS (none)
*/
static BOOL_32 extract_whats_installed_out_of_msg_from_tpmicro( unsigned char **pucp )
{
	BOOL_32	rv;

	UNS_32	our_box_index_0;

	// ----------
	
	// 8/7/2012 rmd : This function always returns true.
	rv = (true);

	// ----------

	WHATS_INSTALLED_STRUCT	lwi;

	// 8/7/2012 rmd : get the data.
	memcpy( &lwi, *pucp, sizeof(WHATS_INSTALLED_STRUCT) );

	// 8/7/2012 rmd : Bump the message ptr.
	*pucp += sizeof( WHATS_INSTALLED_STRUCT );

	// ----------

	// 8/3/2012 rmd : Prepare to write to the tpmicro_data structure. A shared and mutex
	// protected structure.
	xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );

	// 5/1/2014 rmd : Take the mutex to protect against us missing a change due to multiple
	// processes manipulating the send_box_configuration_to_master flag. Each of the functions
	// that evaluate the new wi potentiall set the send_box_configuration flag.
	xSemaphoreTakeRecursive( irri_comm_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	// 4/24/2014 rmd : So we know which chain members index to compare against. And if there is
	// a change where to write the received data.
	our_box_index_0 = FLOWSENSE_get_controller_index();

	// ----------

	// 2/6/2015 rmd : Copy to the temporary holding location. Waiting till we build a token
	// response. We are using the tpmicro_data MUTEX to guard this holding what's installed
	// structure against access during this copy. And again same MUTEX when we read this
	// variable.
	tpmicro_comm.wi_holding = lwi;

	// ----------
	
	// 2/6/2015 rmd : Always send to the master. If there has been a change it will be detected
	// there on the foal side. If not no harm in sending. A detected change triggers a
	// re-registration with the central. And stations made or hidden, etc.
	irri_comm.send_box_configuration_to_master = (true);

	// ----------
	
	// 4/1/2014 rmd : Flag that we've seen this data. Allows the comm_mngr to begin making scan
	// and token messages. Also allows us to process incoming SCAN and TOKENS. In a sense this
	// crosses the foal-irri barrier. However in this case we must. So that the 'master' or
	// 'standalone' begins making tokens following scan completion. Which is HOW the
	// whats_installed information makes its way back to the foal side or master in the case of
	// a chain.
	tpmicro_data.whats_installed_has_arrived_from_the_tpmicro = (true);

	// ----------
	
	Alert_Message( "Whats Installed rcvd from TPMicro" );

	// ----------

	xSemaphoreGiveRecursive( irri_comm_recursive_MUTEX );

	xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Pulls the box level measured current from the incoming tpmicro message. If
    the value is different than we have on record the flag is set to send it to the master
    with the next token resp.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task. When the tp_micro incoming
    message is pulled apart.

	@RETURN (none)

	@ORIGINAL 2012.08.08 rmd

	@REVISIONS (none)
*/
static BOOL_32 extract_box_level_measured_current_out_of_msg_from_tpmicro( unsigned char **pucp )
{
	BOOL_32	rv;

	rv = (true);

	// ------------

	UNS_32	newly_measured_current_ma;

	// 8/7/2012 rmd : get the data.
	memcpy( &(newly_measured_current_ma), *pucp, sizeof(UNS_32) );

	// 8/15/2012 rmd : Move ptr to next msg content.
	*pucp += sizeof(UNS_32);

	// ------------

	if( newly_measured_current_ma > MAXIMUM_BOX_MILLI_AMPS )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "TPMICRO: box current error." );

		rv = (false);
	}
	else
	{
		// 8/16/2012 rmd : If it differs from what we have on record.
		if( newly_measured_current_ma != tpmicro_data.as_rcvd_from_tp_micro.measured_ma_current )
		{
			// 8/15/2012 rmd : If the new current is different route it to the master. No MUTEX needed
			// as commentted about in the variable declaration description.
			tpmicro_data.as_rcvd_from_tp_micro.current_needs_to_be_sent = (true);

			// 8/15/2012 rmd : And update the recorded value.
			tpmicro_data.as_rcvd_from_tp_micro.measured_ma_current = newly_measured_current_ma;
		}
	}

	// ------------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION When the TPMICRO sees a short an attempt to find the triac output with the
    excessive current is made. The results of the hunt are reported in this msg content.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task. When the tp_micro incoming
    message is pulled apart.

    @RETURN (false) if there is an out-of-range error, otherwise (true). Also moves the pucp
    ptr along to the next piece of msg content.

	@ORIGINAL 2012.08.08 rmd
*/
static BOOL_32 extract_terminal_short_report_out_of_msg_from_tpmicro( unsigned char **pucp )
{
	TERMINAL_SHORT_OR_NO_CURRENT_STRUCT		srs;
	
	BOOL_32	rv;

	// ------------

	rv = (true);

	// ------------

	// 9/18/2013 rmd : The message contains the SHORT_OR_NO_CURRENT_REPORT_STRUCT. So get it and
	// analyze.
	memcpy( &srs, *pucp, sizeof(TERMINAL_SHORT_OR_NO_CURRENT_STRUCT) );
	
	*pucp += sizeof(TERMINAL_SHORT_OR_NO_CURRENT_STRUCT);
	
	// ------------
	
	// 9/24/2013 rmd : These are the ONLY 3 result reasons the record can be set for.
	if( (srs.result != SHORT_OR_NO_CURRENT_RESULT_short_and_found) &&
		(srs.result != SHORT_OR_NO_CURRENT_RESULT_short_but_hunt_was_unsuccessful) &&
		(srs.result != SHORT_OR_NO_CURRENT_RESULT_no_current) )
	{
		Alert_message_on_tpmicro_pile_M( "rcvd short result - out_of_range." );

		rv = (false);
	}

	if( (srs.terminal_type != OUTPUT_TYPE_TERMINAL_MASTER_VALVE) &&
		(srs.terminal_type != OUTPUT_TYPE_TERMINAL_PUMP) &&
		(srs.terminal_type != OUTPUT_TYPE_TERMINAL_STATION) &&
		(srs.terminal_type != OUTPUT_TYPE_TERMINAL_LIGHT) )
	{
		Alert_message_on_tpmicro_pile_M( "rcvd short terminal - out_of_range." );

		rv = (false);
	}
	
	// ------------
	
	if( rv )
	{
		// 8/20/2012 rmd : First a sanity check. We should not be seeing a short or no current
		// report with any of our flags already set.
		if( tpmicro_data.terminal_short_or_no_current_state != TERMINAL_SONC_STATE_idle )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "TP MICRO : unexp state." );
		}
		
		// ----------
		
		// 9/24/2013 rmd : Prepare for transfer of the short report to the master.
		tpmicro_data.terminal_short_or_no_current_report = srs;
		
		// ----------

		// 9/30/2015 rmd : Move to the next state.
		tpmicro_data.terminal_short_or_no_current_state = TERMINAL_SONC_STATE_send_to_master;
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 extract_decoder_fault_content_out_of_msg_from_tpmicro( UNS_8 **pucp )
{
	UNS_32	i;

	BOOL_32	rv;

	rv = (false);

	// 11/7/2013 ajv : Next, find a location within a local array to store it so
	// the data can be based up to the FOAL master.

	// 11/6/2013 ajv : Since we're writing to a non-atomic variable which is
	// reset when a token response is being built, take the tpmicro_data mutex
	// to ensure no one messes with the data while we're working with it.
	xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );

	for( i = 0; i < TWO_WIRE_DECODER_FAULTS_TO_BUFFER; ++i )
	{
		// 11/6/2013 ajv : If we've found an available slot within the array,
		// extract the data out of the pointer and break out of the loop.
		if( tpmicro_data.decoder_faults[ i ].fault_type_code == DECODER_FAULT_CODE_not_in_use )
		{
			memcpy( &tpmicro_data.decoder_faults[ i ], *pucp, sizeof(DECODER_FAULT_BASE_TYPE) );
		
			*pucp += sizeof(DECODER_FAULT_BASE_TYPE);
		
			// 11/6/2013 ajv : Since we successfully extracted data out of the
			// pointer, make sure we return true to keep content_good intact.
			rv = (true);

			// 11/7/2013 ajv : We've extracted and stored the message content. So we are done.
			break;
		}
	}

	xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 extract_executing_code_date_and_time_out_of_msg_from_tpmicro( unsigned char **pucp )
{
	BOOL_32	rv;

	rv = (true);

	// ------------

	memcpy( &(tpmicro_comm.tpmicro_executing_code_date), *pucp, sizeof(UNS_32) );

	*pucp += sizeof(UNS_32);

	memcpy( &(tpmicro_comm.tpmicro_executing_code_time), *pucp, sizeof(UNS_32) );

	*pucp += sizeof(UNS_32);

	// 3/4/2014 rmd : No particular range check I could think of. So we are not doing one.
	// Function always returns true.
	
	// ------------

	return( rv );
}

/* ---------------------------------------------------------- */
extern void TP_MICRO_COMM_resync_wind_settings( void )
{
	// 5/9/2014 rmd : Executed within the context of the comm_mngr when parsing an incoming
	// message from the tpmicro. May also need to execute within the context of the keyprocess
	// task as when wind settings are changed by the user. Atomic operation. No MUTEX needed.
	tpmicro_data.send_wind_settings_structure_to_the_tpmicro = (true);
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Pulls apart all message to us from the tp micro.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task.

	@RETURN (none)

	@ORIGINAL 2012.08.08 rmd

	@REVISIONS (none)
*/
extern void TPMICRO_COMM_parse_incoming_message_from_tp_micro( DATA_HANDLE pdh )
{
	DISPLAY_EVENT_STRUCT	lde;

	UNS_32	lmsg_bytes_remaining;

	UNS_8	how_many, sw1_status;

	unsigned char	*ucp;

	char	str_128[ ALERTS_MAX_STORAGE_OF_TEXT_CHARS ];
	
	char	running_str[ 32 ];

	DATE_TIME	running_dt;

	UNS_32	decoder_count, ddd;
	
	UNS_32	nu_wind;
	
	FOAL_COMMANDS	fc;

	// 4/5/2012 rmd : If the functions performing the extraction of data flag an error we
	// abandon the rest of the message.
	BOOL_32		content_good;

	// ----------

	// 11/15/2012 rmd : The pdh.dlen includes the packet HEADER but does not include the packet
	// CRC. That has been stripped from the packet by the time it reaches here.
	
	// 4/5/2012 rmd : At the top of the message is the TPL_DATA_HEADER_TYPE structure. Which we
	// do not care about at this point. The CRC has been checked. The port the packet came in on
	// has been checked. The message class has been checked. And the TO address has already been
	// checked.
	ucp = pdh.dptr;
	lmsg_bytes_remaining = pdh.dlen;

	ucp += sizeof( TPL_DATA_HEADER_TYPE );
	lmsg_bytes_remaining -= sizeof( TPL_DATA_HEADER_TYPE );
	
	// ----------

	// 4/5/2012 rmd : ucp now points at the fc structure for the message.
	memcpy( &fc, ucp, sizeof(FOAL_COMMANDS) );

	ucp += sizeof(FOAL_COMMANDS);
	lmsg_bytes_remaining -= sizeof( FOAL_COMMANDS );
	
	// ----------

	content_good = (true);
	
	// ----------

	if( fc.mid == MID_TPMICRO_TO_MAIN_PRIMARY_MESSAGE )
	{
		// 8/8/2012 rmd : We count the number of incoming from the TPMICRO between each of our
		// outgoing. We are both running at a quasi 1 hertz rate. Quasi because actually all
		// messages between are unsolicited. Meaning they can be sent at any time. There is not a
		// formal token - token_resp arrangement.
		tpmicro_comm.number_of_outgoing_since_last_incoming = 0;

		// ----------

		if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_executing_code_date_and_time ) )
		{
			// 3/4/2014 rmd : Note function always returns true as there is no range check on the data
			// performed.
			content_good = extract_executing_code_date_and_time_out_of_msg_from_tpmicro( &ucp );
			
			if( content_good )
			{
				// 3/4/2014 rmd : Flag available for use.
				tpmicro_comm.tpmicro_executing_code_date_and_time_valid = (true);

				// ----------
				
				// 2/20/2015 rmd : Informative alert.
				running_dt.D = tpmicro_comm.tpmicro_executing_code_date;

				running_dt.T = tpmicro_comm.tpmicro_executing_code_time;
				
				snprintf( str_128, sizeof(str_128), "TPMicro is running %s", DATE_TIME_to_DateTimeStr_32( running_str, sizeof(running_str), running_dt ) );

				Alert_Message( str_128 );

				//Alert_message_on_tpmicro_pile_M( str_128 );
			}
			
			// ----------
			
			// 10/14/2014 rmd : Because the code time and date content indicates this is the first
			// message from the tpmicro received since the MAIN BOARD booted we set the request to send
			// the wind structure and to clear the tpmicro rcvd_errors bit field to match the tp micros
			// starting condition for the error flag. (The error flag should be removed some day
			// - seems like clutter.)
			tpmicro_data.rcvd_errors.errorBitField = 0;
			
			// 10/14/2014 rmd : Always send wind to start.
			TP_MICRO_COMM_resync_wind_settings();
		}
		else
		{
			// 10/14/2014 rmd : Up until the point where further message parsing is qualified by an "up
			// and running NETWORK", there is only one EXTRACTION of message content in the design at
			// this time - the whats installed.

			if( content_good )
			{
				if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_here_is_whats_installed ) )
				{
					content_good = extract_whats_installed_out_of_msg_from_tpmicro( &ucp );
				}
			}
	
			// ----------
	
			if( content_good )
			{
				if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_wind_requesting_settings ) )
				{
					// 10/14/2014 rmd : BIT ONLY. NO DATA INCLUDED.

					TP_MICRO_COMM_resync_wind_settings();
				}
			}
	
			// ----------
	
			if( content_good )
			{
				if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_two_wire_cable_excessive_current_marker ) )
				{
					// 10/14/2014 rmd : BIT ONLY. NO DATA INCLUDED.

					// 10/31/2013 rmd : A true unexpected state. Should never happen.
					if( tpmicro_data.two_wire_cable_excessive_current_xmission_state != TWO_WIRE_CABLE_ANOMALY_STATE_idle )
					{
						Alert_Message( "Unexpected 2W cable I state" );
					}
	
					tpmicro_data.two_wire_cable_excessive_current_xmission_state = TWO_WIRE_CABLE_ANOMALY_STATE_send_to_master;
	
					// 11/8/2013 ajv : If a discovery is in progress and the cable
					// short is detected, notify the user with a dialog.
					if( GuiLib_CurStructureNdx == GuiStruct_dlgDiscoveringDecoders_0 )
					{
						lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
						lde._04_func_ptr = &FDTO_TWO_WIRE_update_discovery_dialog;
						lde._06_u32_argument1 = (true);
						Display_Post_Command( &lde );
					}
					else
					{
						// 1/28/2014 ajv : If the dialog is already closed, we still
						// need to reset the UI variable because otherwise, we'll
						// restart the discovery process when we go to the Station
						// Assignment screen since it looks at this variable to
						// determine whether to show the discovery dialog or not.
						GuiVar_TwoWireDiscoveryState = TWO_WIRE_DISCOVERY_DIALOG_STATE_COMPLETE;
					}
				}
			}
	
			// ----------
	
			if( content_good )
			{
				if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_two_wire_cable_overheated_marker ) )
				{
					// 10/14/2014 rmd : BIT ONLY. NO DATA INCLUDED.

					// 10/31/2013 rmd : A true unexpected state. Should never happen.
					if( tpmicro_data.two_wire_cable_over_heated_xmission_state != TWO_WIRE_CABLE_ANOMALY_STATE_idle )
					{
						Alert_Message( "Unexpected 2W cable H state" );
					}
	
					tpmicro_data.two_wire_cable_over_heated_xmission_state = TWO_WIRE_CABLE_ANOMALY_STATE_send_to_master;
	
					// 11/8/2013 ajv : If a discovery is in progress and the cable
					// overheats, notify the user with a dialog.
					if( GuiLib_CurStructureNdx == GuiStruct_dlgDiscoveringDecoders_0 )
					{
						lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
						lde._04_func_ptr = &FDTO_TWO_WIRE_update_discovery_dialog;
						lde._06_u32_argument1 = (true);
						Display_Post_Command( &lde );
					}
					else
					{
						// 1/28/2014 ajv : If the dialog is already closed, we still
						// need to reset the UI variable because otherwise, we'll
						// restart the discovery process when we go to the Station
						// Assignment screen since it looks at this variable to
						// determine whether to show the discovery dialog or not.
						GuiVar_TwoWireDiscoveryState = TWO_WIRE_DISCOVERY_DIALOG_STATE_COMPLETE;
					}
				}
			}
	
			// ----------
	
			if( content_good )
			{
				if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_two_wire_cable_cooled_off_marker ) )
				{
					// 10/14/2014 rmd : BIT ONLY. NO DATA INCLUDED.

					// 10/31/2013 rmd : A true unexpected state. Should never happen.
					if( tpmicro_data.two_wire_cable_cooled_off_xmission_state != TWO_WIRE_CABLE_ANOMALY_STATE_idle )
					{
						Alert_Message( "Unexpected 2W cable C-O state" );
					}
	
					tpmicro_data.two_wire_cable_cooled_off_xmission_state = TWO_WIRE_CABLE_ANOMALY_STATE_send_to_master;
				}
			}
	
			// ----------
	
			// 5/8/2014 rmd : From this point forward within the message FROM the tpmicro if the
			// comm_mngr isn't ready (for that matter if the network isn't available) drop the rest of
			// the content from the tpmicro. WHY DO THIS? Well it gets at hardware being available
			// within the main board files prior to the tp_micro sending message content for such
			// hardware. For example the tp_micro will send POC flow content within its first message to
			// the main board when in fact the main board has yet to receive the whats installed
			// (actually can only if the main board boots but the tp micro doesn't). Another case is
			// during code distribution or receipt. The main board is not making tokens (to move the
			// flow readings around) YET the tpmicro continues to send flow readings. From a
			// philosophical point of view when we are not ready for or able to properly handle such
			// content why try. So the decision was made (Bob & AJ) to drop such content during these
			// transitional phases.
			if( COMM_MNGR_network_is_available_for_normal_use() )
			{
				// 10/14/2014 rmd : NOTE the first item extracted inside of the "up and running netwrok
				// section" is the WIND SPEED. If there is wind this content must be present otherwise the
				// wind is taken to be ZERO. Therefore we include the wind up front in the message to be
				// sure there is room for this data. And not excluded by the message size restriction when
				// the message is being built.
				if( content_good )
				{
					if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_we_have_wind_speed ) )
					{
						// 10/14/2014 rmd : THERE is data with this one. The wind speed.
						memcpy( &nu_wind, ucp, sizeof(UNS_32) );
		
						ucp += sizeof(UNS_32);
					}
					else
					{
						// 10/13/2014 rmd : With the bit not set that is the way the tpmicro tells us the wind speed
						// is zero.
						nu_wind = 0;
					}
	
					/*
					// 10/15/2014 rmd : Should comment out this alert. Will become a nuisance in the field.
					// Everytime the wind speed changes!
					if( nu_wind != tpmicro_comm.wind_mph )
					{
						Alert_Message_va( "wind speed is %u mph", nu_wind );
					}
					*/
					
					tpmicro_comm.wind_mph = nu_wind;
				}
		
				// ----------
				
				if( content_good )
				{
					xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );
		
					if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_it_is_currently_raining ) )
					{
						// 8/7/2012 rmd : BIT ONLY. No data accompanies this indication.
						if((WEATHER_get_rain_switch_in_use() == (true)) && (WEATHER_get_rain_switch_connected_to_this_controller( FLOWSENSE_get_controller_index() ) == (true)) )
						{
							tpmicro_comm.rain_switch_active = (true);
						}
						else
						{
							tpmicro_comm.rain_switch_active = (false);
						}
					}
					else
					{
						tpmicro_comm.rain_switch_active = (false);
					}
		
					xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );
				}
		
				// ----------

				if( content_good )
				{
					xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );
		
					if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_it_is_currently_freezing ) )
					{
						// 8/7/2012 rmd : BIT ONLY. No data accompanies this indication.

						if( (WEATHER_get_freeze_switch_in_use() == (true)) && (WEATHER_get_freeze_switch_connected_to_this_controller( FLOWSENSE_get_controller_index() ) == (true)) )
						{
							tpmicro_comm.freeze_switch_active = (true);
						}
						else
						{
							tpmicro_comm.freeze_switch_active = (false);
						}
					}
					else
					{
						tpmicro_comm.freeze_switch_active = (false);
					}
		
					xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );
				}
		
				// ----------

				if( content_good )
				{
					if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_we_had_an_et_gage_pulse ) == (true) )
					{
						// 8/7/2012 rmd : BIT ONLY. No data accompanys this indication.
		
						// 3/21/2018 ajv : Redundant. When Log Pulses is enabled, this is generated as a formal 
						// alert. 
						// Alert_Message( "tpmicro_comm: et gage pulse" ); 
		
						// 9/18/2012 rmd : Take the MUTEX to avoid conflict when evaluating and reseting the count
						// while building a token response.
						xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );
		
						tpmicro_data.et_gage_pulse_count_to_send_to_the_master += 1;
						
						xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );
					}
				}
		
				// ----------

				if( content_good )
				{
					if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_we_had_a_rain_bucket_pulse ) == (true) )
					{
						// 8/7/2012 rmd : BIT ONLY. No data accompanies this indication.
		
						Alert_Message( "tpmicro_comm: rain bucket pulse" );
		
						// 9/18/2012 rmd : Take the MUTEX to avoid conflict when evaluating and reseting the count
						// while building a token response.
						xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );
		
						tpmicro_data.rain_bucket_pulse_count_to_send_to_the_master += 1;
						
						xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );
					}
				}
		
				// ----------

				if( content_good )
				{
					if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_here_are_the_poc_flow_meter_counts ) )
					{
						content_good = extract_flow_meter_readings_out_of_msg_from_tpmicro( &ucp );
					}
				}
		
				// ----------

				if( content_good )
				{
					if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_here_is_a_moisture_reading ) )
					{
						content_good = extract_moisture_reading_out_of_msg_from_tpmicro( &ucp );
					}
				}
		
				// ----------
		
				if( content_good )
				{
					if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_measured_currents_for_the_stations_ON ) )
					{
						content_good = extract_ma_readings_out_of_msg_from_tpmicro( &ucp );
					}
				}
		
				// ----------
		
				if( content_good )
				{
					if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_terminal_short_report ) )
					{
						content_good = extract_terminal_short_report_out_of_msg_from_tpmicro( &ucp );
					}
				}
		
				if( content_good )
				{
					if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_fuse_is_blown ) == (true) )
					{
						// 8/7/2012 rmd : BIT ONLY. No data accompanies this indication.
		
						if( !tpmicro_comm.fuse_blown )
						{
							Alert_fuse_blown_idx( FLOWSENSE_get_controller_index() );
		
							tpmicro_comm.fuse_blown = (true);
						}
					}
					else
					{
						if( tpmicro_comm.fuse_blown )
						{
							Alert_fuse_replaced_idx( FLOWSENSE_get_controller_index() );
		
							tpmicro_comm.fuse_blown = (false);
						}
					}
				}
		
				// ----------
		
				if( content_good && B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_here_is_the_1s_current_draw ) )
				{
					// 8/10/2012 rmd : The latest current as measured within the tpmicro.
					content_good = extract_box_level_measured_current_out_of_msg_from_tpmicro( &ucp );
				}
		
				// ----------
		
				if( content_good )
				{
					if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_wind_is_in_paused_state ) )
					{
						// 8/7/2012 rmd : BIT ONLY. No data accompanies this indication.
						
						// 10/15/2014 rmd : Only alert at change of state.
						if( !tpmicro_comm.wind_paused )
						{
							Alert_Message_va( "wind speed is %u mph", tpmicro_comm.wind_mph );

							Alert_wind_paused( tpmicro_comm.wind_mph );
						}
						
						tpmicro_comm.wind_paused = (true);
					}
					else
					{
						if( tpmicro_comm.wind_paused )
						{
							Alert_Message_va( "wind speed is %u mph", tpmicro_comm.wind_mph );

							Alert_wind_resumed( tpmicro_comm.wind_mph );
						}
						
						tpmicro_comm.wind_paused = (false);
					}
				}
		
		
				// ----------
		
				if( content_good )
				{
					if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_decoders_discovered_so_far ) )
					{
						// 10/16/2013 rmd : An UNS_32 accompanies this bit. Get it into a temporary variable to
						// guard against the display process attempting to display during partial memcpy completion.
						// MEMCPY is of course not atomic.
						memcpy( &ddd, ucp, sizeof(UNS_32) );
						ucp += sizeof(UNS_32);
						
						// 10/16/2013 rmd : Now perform an atomic copy.
						tpmicro_data.decoders_discovered_so_far = ddd;
		
						// 10/17/2013 ajv : Update the easyGUI variable as well to
						// ensure the screen is updated properly. The screen will
						// reflect the newly discovered decoder the next time TD CHECK
						// updates the dialog.
						GuiVar_TwoWireNumDiscoveredStaDecoders = tpmicro_data.decoders_discovered_so_far;
					}
				}
		
				// ----------
		
				if( content_good )
				{
					if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_sending_discovered_decoders ) == (true) )
					{
						// 2/5/2013 rmd : This signifies that we are being handed ALL the decoders on the chain.
						// Implying that before the ENQUIRY process commenced, we issued the broadcast command to
						// reset each decoders discovered status.
						BOOL_32	complete_list;
						
						memcpy( &complete_list, ucp, sizeof(BOOL_32) );
						ucp += sizeof(BOOL_32);
		
						memcpy( &decoder_count, ucp, sizeof(UNS_32) );
						ucp += sizeof(UNS_32);
						
						if( decoder_count <= MAX_DECODERS_IN_A_BOX )
						{
							xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );
	
							// ----------
	
							if( complete_list )
							{
								// 2/1/2013 rmd : Since we don't know what was in the list of decoders, and if a new decoder
								// was added in the field serial number ordering shifts around, we must empty the list to
								// start.
								memset( &tpmicro_data.decoder_info, 0x00, sizeof(tpmicro_data.decoder_info) );
							}
							
							for( ddd=0; ddd<decoder_count; ddd++ )
							{
								// 4/9/2013 rmd : Pick out the serial number.
								memcpy( &tpmicro_data.decoder_info[ ddd ].sn, ucp, sizeof(UNS_32) );
								ucp += sizeof(UNS_32);
								
								// 4/9/2013 rmd : Pick out the ID info.
								memcpy( &tpmicro_data.decoder_info[ ddd ].id_info, ucp, sizeof(ID_REQ_RESP_s) );
								ucp += sizeof(ID_REQ_RESP_s);
							}
		
							// ----------
	
							// 7/8/2014 rmd : Now cover the case where there are decoder based stations in the station
							// file that no longer have a matching decoder showing during the discovery. THIS DOES NOT
							// CREATE NEW STATIONS. Only finds existing stations and setas their PHYSICALLY AVAILABLE
							// either (true) or (false).
							STATION_for_stations_on_this_box_set_physically_available_based_upon_discovery_results();
	
							// 7/8/2014 rmd : See if the decoders we found match up to exisiting POC's. Includes
							// multiple stage bypass pocs.
							//
							// 6/10/2014 rmd : DO NOT ADJUST THE "SHOW" setting based upon discovery results. If there is a wiring
							// problem and a decoder is not seen and we set SHOW (false) that POC would unsync from the
							// poc_preserves. And that would take the BUDGETS accumulators for the current billing period with it.
							// After fixing the wiring problem BUDGETS would be WHACKED! No use since start of billing cycle. Very
							// bad and unfortunate scenario. COMMENTING OUT THE FOLLOWING LINE FIXES THIS.
							//POC_for_pocs_on_this_box_set_physically_available_based_upon_discovery_results();

							// 2/2/2016 rmd : And same for any moisture sensor decoders.
							MOISTURE_SENSOR_set_physically_available_or_create_new_moisture_sensors_as_needed_based_upon_the_discovery_results();
							
							// ----------
							
							xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );
		
							// ----------
							
							// 2/5/2013 rmd : Request a time delayed file save. I don't think we really need a time
							// delay here. But this is a convenient way to trigger the file save. And if the incoming
							// message causes a file save for another reason this avoids double writes to the file
							// system.
							FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_TPMICRO_DATA, 1 );
		
							if( GuiLib_CurStructureNdx == GuiStruct_dlgDiscoveringDecoders_0 )
							{
								lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
								lde._04_func_ptr = &FDTO_TWO_WIRE_update_discovery_dialog;
								lde._06_u32_argument1 = (true);
								Display_Post_Command( &lde );
							}
							else
							{
								// 1/28/2014 ajv : If the dialog is already closed, we still
								// need to reset the UI variable because otherwise, we'll
								// restart the discovery process when we go to the Station
								// Assignment screen since it looks at this variable to
								// determine whether to show the discovery dialog or not.
								GuiVar_TwoWireDiscoveryState = TWO_WIRE_DISCOVERY_DIALOG_STATE_COMPLETE;
							}
						}
						else
						{
							content_good = (false);
							
							snprintf( str_128, sizeof(str_128), "too many discovered decoders - %d", decoder_count );
							Alert_message_on_tpmicro_pile_M( str_128 );
						}
					}
				}
		
				// ----------
		
				if( content_good )
				{
					if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_sending_decoder_statistics ) == (true) )
					{
						UNS_32	decoder_count, ddd, ser_no;
		
						memcpy( &decoder_count, ucp, sizeof(UNS_32) );
						ucp += sizeof(UNS_32);
						
						snprintf( str_128, sizeof(str_128), "stats for %d decoders", decoder_count );
						Alert_message_on_tpmicro_pile_M( str_128 );
		
						if( decoder_count <= MAX_DECODERS_IN_A_BOX )
						{
							for( ddd=0; ddd<decoder_count; ddd++ )
							{
								memcpy( &ser_no, ucp, sizeof(UNS_32) );
								ucp += sizeof(UNS_32);
		
								CS3000_DECODER_INFO_STRUCT	*dis;
								
								dis = find_two_wire_decoder( ser_no );
								
								if( dis != NULL )
								{
									memcpy( &dis->decoder_statistics, ucp, sizeof(DECODER_STATS_s) );
									ucp += sizeof(DECODER_STATS_s);
								}
								else
								{
									snprintf( str_128, sizeof(str_128), "statistics decoder not found - %07d", ser_no );
									Alert_message_on_tpmicro_pile_M( str_128 );
		
		
									content_good = (false);
									
									break;
								}
							}
							
							// 2/5/2013 rmd : Request a time delayed file save. I don't think we really need a time
							// delay here. But this is a convenient way to trigger the file save. And if the incoming
							// message causes a file save for another reason this avoids double writes to the file
							// system.
							FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_TPMICRO_DATA, 1 );
						}
						else
						{
							content_good = (false);
							
							snprintf( str_128, sizeof(str_128), "stats for too many decoders - %d", decoder_count );
							Alert_message_on_tpmicro_pile_M( str_128 );
						}
					}  // if the bit is set
				}  // if the content is good so far	
		
				// ----------
		
				// 2/7/2013 : Check for runaway gage.
				if( content_good )
				{
					if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_run_away_gage_state_latched ) )
					{
						// 2/7/2013 ajv : If we're preparing to send (or have sent) a
						// request to the TP Micro to clear the runaway gage, ignore the
						// runaway gage bit until it's reset.
						if( tpmicro_data.et_gage_clear_runaway_gage_being_processed == (false) )
						{
							tpmicro_data.et_gage_runaway_gage_in_effect_to_send_to_master = (true);
						}
					}
					else
					{
						// 2/7/2013 ajv : If the runaway gage bit is cleared but we
						// haven't reset the "being processed" flag yet, do so now.
						// Additionally, reset the runaway gage flag in
						// weather_preserves and reset the pulse count to prevent an
						// abnormally high pulse count from getting into the table.
						if( tpmicro_data.et_gage_clear_runaway_gage_being_processed )
						{
							weather_preserves.run_away_gage = (false);
		
							weather_preserves.et_rip.et_inches_u16_10000u = 0;
		
							tpmicro_data.et_gage_clear_runaway_gage_being_processed = (false);
						}
					}
				}
		
				// ----------
		
				if( content_good )
				{
					if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_2W_cable_current_measurement ) )
					{
						memcpy( &tpmicro_data.twccm, ucp, sizeof(TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT) );
		
						ucp += sizeof(TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT);
					}
				}
				
				// ----------
		
				if( (content_good) && (B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_two_wire_decoder_output_short )) )
				{
					content_good = extract_decoder_fault_content_out_of_msg_from_tpmicro( &ucp );
				}
		
				// ----------
		
				if( (content_good) && (B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_two_wire_decoder_output_voltage_too_low)) )
				{
					content_good = extract_decoder_fault_content_out_of_msg_from_tpmicro( &ucp );
				}
		
				// ----------
		
				if( (content_good) && (B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_two_wire_decoder_inoperative )) )
				{
					content_good = extract_decoder_fault_content_out_of_msg_from_tpmicro( &ucp );
				}

				// ----------
				
				if( content_good )
				{
					// 3/10/2016 mjb : First we check if the bit is set from main to indicate that SW1 message is included.
					if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_status_sw1_status_in_message ) )
					{
						// 3/10/2016 mjb : Now lets get the actual sw1 switch state out of the message from main.
						memcpy( &sw1_status, ucp, sizeof(UNS_8) );
						ucp += sizeof(UNS_8);

						// ----------
						
						// 6/7/2016 rmd : Only honor the status of SW1 if the user has us set up to do so.
						if( WEATHER_sw1_is_available_and_enabled_as_a_rain_switch_at_this_controller( FLOWSENSE_get_controller_index() ) )
						{
							// 3/10/2016 mjb : Based on the state of the sw1 switch when it is used as a rain switch, as
							// is the case here, we are going to simply "piggyback" off of the existing rain switch
							// flags and set the flags accordingly.
							if( sw1_status )
							{
								// 6/7/2016 rmd : No MUTEX needed - BOOL_32 is an atomic variable type.
								tpmicro_comm.rain_switch_active = (true);
							}
							else
							{
								tpmicro_comm.rain_switch_active = (false);
							}
						}
						else
						{
							// 6/7/2016 rmd : Always clean up. If no longer enabled must be (false).
							tpmicro_comm.rain_switch_active = (false);
						}
					}
				}
				
				// ----------
	
				// 4/23/2012 rmd : The TPMicro has agreed to always include the error
				// structure last in the message. If there is an error.
				if( content_good )
				{
					if( B_IS_SET( fc.pieces, BIT_TTMB_PRIMARY_we_have_errors_to_report ) )
					{
						ucp += TPMICRO_DATA_extract_reported_errors_out_of_msg_from_tpmicro( &ucp );
					}
				}
		
			}  // of if network is available
			
		}
		// ---------

		// 4/23/2012 rmd : This message ID is done!

	}
	else
	if( fc.mid == MID_TPMICRO_TO_MAIN_SECONDARY_MESSAGE )
	{
		// 10/25/2013 rmd : The interesting thing about this bit is that it only occurs in messages
		// with NO OTHER BITS SET. In other words the alerts come in a message all by themselves.
		// This is by design so the tpmicro can create and send an 'alert' message anytime it wants
		// to. Without waiting for the one hertz mark.
		if( B_IS_SET( fc.pieces, BIT_TTMB_SECONDARY_alert_message_string ) )
		{
			memcpy( &how_many, ucp, sizeof( UNS_8 ) );

			ucp += sizeof( UNS_8 );

			if( how_many <= ALERTS_MAX_STORAGE_OF_TEXT_CHARS )
			{
				// Initialize the buffer to all NULLs
				memset( &str_128, 0x00, sizeof(str_128) );

				// Copy the appropriate number of characters into the buffer
				memcpy( &str_128, ucp, how_many );

				ucp += how_many;

				Alert_message_on_tpmicro_pile_T( str_128 );
			}
			else
			{
				snprintf( str_128, sizeof(str_128), "alert message length out of range (%d)", how_many );
				Alert_message_on_tpmicro_pile_M( str_128 );
				
				// 4/8/2013 rmd : Stop pulling apart this message.
				content_good = (false);
			}
		}
		else
		{
			// 10/14/2014 rmd : The convention is that if the tpmicro sets and sends an alert message
			// that is the ONLY content in the message. Hence we can use the else construct you see
			// here.
			

		}
	}
	else
	if( fc.mid == MID_TPMICRO_TO_MAIN_DEBUG_DISPLAY )
	{
		// 3/22/2013 rmd : For this message type pieces contains a number. The line number on the
		// display! It is not our traditional bit field. And each message carries with it ONLY ONE
		// line. Could be more efficient but is this way.
		if( (fc.pieces < TPMICRO_DEBUG_MAX_LINES_OF_TEXT) && (lmsg_bytes_remaining <= TPMICRO_DEBUG_TEXT_LINE_WIDTH) )
		{
			// 11/15/2012 rmd : Fill with line terminators.
			memset( &TPMICRO_debug_text[ fc.pieces ], 0x00, TPMICRO_DEBUG_TEXT_LINE_WIDTH );
			
			memcpy( &TPMICRO_debug_text[ fc.pieces ], ucp, lmsg_bytes_remaining );
		}
		else
		{
			snprintf( (char*)(&TPMICRO_debug_text[ 0 ]), TPMICRO_DEBUG_TEXT_LINE_WIDTH, "PROBLEM WITH DATA" );
		}

		// 4/19/2012 rmd : The display is automatically drawn 4X per second. If you are on the
		// screen. So nothing special to do here to cause the new information to be displayed.
	}
	else
	if( fc.mid == MID_TPMICRO_TO_MAIN_SW_HANDSHAKE )
	{
		// 3/13/2013 rmd : The TP Micro is telling us to send more data his way. So signal the
		// serial driver it is okay to begin sending the next block (if it has one to send).
		postSerportDrvrEvent( UPORT_TP, UPEV_FLOW_CONTROL_GOAHEAD );
	}
	else
	{
		Alert_message_on_tpmicro_pile_M( "unexp command from tpmicro." );
	}

	// 2012.06.07 ajv : We are done with this message and it's associated
	// memory.
	mem_free( pdh.dptr );
}

/* ---------------------------------------------------------- */
static void msg_rate_timer_callback( xTimerHandle pxTimer )
{
	(void)pxTimer;

	// Remember this is executed within the context of the high priority timer task.

	COMM_MNGR_post_event( COMM_MNGR_EVENT_tpmicro_msg_rate_timer_expired );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Looks for stations ON. If any returns the size of the message content. Also
    sets the ptr-to-the-ptr to where the message content is. Format is first one byte for
    the number of stations then one byte for each station number.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task when building an outgoing
    message for the tpmicro. Currently at a 1 hertz rate.

    @RETURN The number of bytes of message content generated. Also sets the ptr-to-a-ptr to
    where the content is.

	@ORIGINAL 2012.08.14 rmd

	@REVISIONS
*/
static UNS_32 build_stations_ON_for_tpmicro_msg( UNS_8 **son_ptr )
{
	UNS_32	rv;

	BOOL_32	have_seen_an_error;

	UNS_8	*ucp;

	UNS_8	station_count, *station_count_ptr;

	STATION_STRUCT	*lstation;
	
	STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT		sos;
		
	// ----------

	rv = 0;

	// ---------------

	have_seen_an_error = (false);

	// ---------------

	// 4/3/2012 rmd : Because we are looking at the list and we require it to be
	// stable.
	xSemaphoreTakeRecursive( irri_irri_recursive_MUTEX, portMAX_DELAY );

	// 8/14/2012 rmd : Based on message format get memory. NEVER returns NULL.
	ucp = mem_malloc( (sizeof(UNS_8) + (MAX_NUMBER_OF_TERMINAL_AND_DECODER_BASED_OUTPUTS_ON_AT_A_TIME * sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT))) );

	*son_ptr = ucp;

	// ------------

	station_count = 0;

	station_count_ptr = ucp;

	ucp += sizeof(UNS_8);

	rv += sizeof(UNS_8);

	// ------------

	IRRI_MAIN_LIST_OF_STATIONS_STRUCT	*imlss;
	
	UNS_32		lindex;
	
	// 4/18/2014 rmd : So we don't have to execute this somewhat heavy function over and over
	// again.
	lindex = FLOWSENSE_get_controller_index();

	// 5/16/2014 rmd : Remember this is the irri list. There is no direct list of those ON.
	imlss = nm_ListGetFirst( &irri_irri.list_of_irri_all_irrigation );

	while( imlss != NULL )
	{
		// 4/4/2012 rmd : Only for OUR stations. We are building up a message for our TPMicro to
		// turn on the stations physically located at this box.
		if( (imlss->box_index_0 == lindex) && (imlss->ibf.station_is_ON == (true)) )
		{
			// 8/14/2012 rmd : If we're already at the limit git.
			if( station_count >= NETWORK_CONFIG_get_electrical_limit( lindex ) )
			{
				Alert_message_on_tpmicro_pile_M( "too many ON for msg to TPMicro." );

				// 8/14/2012 rmd : Indicate no message.
				have_seen_an_error = (true);

				break;
			}
			else
			{
				// 4/10/2013 rmd : Find the station in the station file so we can pick out the decoder
				// serial number and output A/B information. Must take program_data MUTEX to access the
				// station file.
				xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

				// 4/18/2014 rmd : Find the station based upon our box index and the station number.
				lstation = nm_STATION_get_pointer_to_station( lindex, imlss->station_number_0_u8 );
				
				// 4/10/2013 rmd : Only continue if we found the station.
				if( lstation != NULL )
				{
					sos.terminal_number_0 = nm_STATION_get_station_number_0( lstation );

					sos.decoder_serial_number = nm_STATION_get_decoder_serial_number( lstation );

					sos.decoder_output = nm_STATION_get_decoder_output( lstation );
					
					// ----------
					
					memcpy( ucp, &sos, sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT) );
					
					ucp += sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT);

					rv += sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT);
	
					station_count += 1;
				}
				else
				{
					Alert_message_on_tpmicro_pile_M( "Msg to TPMicro: station ON not found." );
	
					// 8/14/2012 rmd : Indicate no message. rv is already 0.
					have_seen_an_error = (true);
	
					break;
				}
				
				xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

			}  // of if we haven't hit the count limit yet

		}  // of a station at this box that is ON

		imlss = nm_ListGetNext( &irri_irri.list_of_irri_all_irrigation, imlss );
	}

	xSemaphoreGiveRecursive( irri_irri_recursive_MUTEX );

	// ------------

	// 9/24/2015 rmd : Regardless of value make place the count in the message.
	*station_count_ptr = station_count;
	
	// ------------

	if( have_seen_an_error || (station_count == 0) )
	{
		mem_free( *son_ptr );

		*son_ptr = NULL;

		rv = 0;
	}

	// ------------

	return( rv );
}

/* ---------------------------------------------------------- */
// 10/12/2012 rmd : This is not part of the anti_chatter mechanism. But rather a safety
// catch for communication errors to prevent the MV from being OPEN or the PUMP running with
// no valves ON. The ALERT limit MUST be bigger than the MV_PUMP limit. The delta is how
// many alerts we will make about the situation.
//
// But how long should the MV_PUMP limit be. Well first of all this condition should never
// exist. And actually this time out should be say twice the token rate. But the token rate
// is CHAIN dependant. HEY! Here's an idea. Have the IRRI side learn it's token rate. And
// then set these and other timeouts accordingly. Such as how long before we fall into
// forced. Well for now I will make this 30 counts. And what are these counts. They are
// messages to the tp micro! Which are supposed to occur at a 1Hz rate.
//
// 9/12/2014 rmd : NOTE how we need to use the SLOW CLOSING VALVE maximum time as our limit
// to tolerate the alarming condition of no valves ON with MV or PUMP active. This is
// because this condition can actually occur when the next turn ON is inhibited. EXCEPT for
// the case of the pump. It cannot. Because we do not honor the slow closing turn on inhibit
// if the pump is running and we just turned off the last valve. So that limit is 30 seconds
// only. Before we take over control.
#define	POC_MSGS_WITH_NO_VALVES_ON_MV_LIMIT		(DELAY_BETWEEN_VALVES_MAX + 10)
#define	POC_MSGS_WITH_NO_VALVES_ON_PUMP_LIMIT	(30)

// 10/12/2012 rmd : By making 5 more we get 5 alerts when the condition occurs. Hopefully
// never.
#define	POC_MSGS_WITH_NO_VALVES_ON_MV_ALERT_LIMIT	(POC_MSGS_WITH_NO_VALVES_ON_MV_LIMIT + (5))
#define	POC_MSGS_WITH_NO_VALVES_ON_PUMP_ALERT_LIMIT	(POC_MSGS_WITH_NO_VALVES_ON_PUMP_LIMIT + (5))


static UNS_32 build_poc_output_activation_for_tpmicro_msg( UNS_8 **pon_ptr )
{
	// 7/9/2014 rmd : Executed within the context of the comm_mngr task. Think about this as
	// executing on the IRRI side of the world.
	
	// ----------
	
	UNS_32	ppp, i, levels;

	UNS_8	number_of_pocs, *number_of_pocs_ptr;

	UNS_32	rv;

	UNS_8	*ucp;
	
	POC_ON_FOR_TPMICRO_STRUCT	pos;

	BOOL_32	have_seen_an_error;
	
	void	*lpoc;

	BOOL_32		at_least_one_station_in_this_pocs_mainline_is_open;

	IRRI_MAIN_LIST_OF_STATIONS_STRUCT	*imlss;
	
	BOOL_32		there_is_a_mlb, its_a_bypass;

	BOOL_32	include_me;

	// ----------

	rv = 0;

	have_seen_an_error = (false);

	// ----------

	// 8/14/2012 rmd : Based on message format get memory. NEVER returns NULL. Notice we only
	// allow for 6 ON at a time. And this include the TERMINAL and DECODER combination. And as a
	// matter of fact a BYPASS counts as THREE.
	ucp = mem_malloc( (sizeof(UNS_8) + (MAX_NUMBER_OF_POCS_ON_AT_A_TIME * sizeof(POC_ON_FOR_TPMICRO_STRUCT))) );

	*pon_ptr = ucp;
	
	// ----------

	number_of_pocs = 0;

	number_of_pocs_ptr = ucp;
	
	ucp += sizeof(UNS_8);

	rv += sizeof(UNS_8);
	
	// ----------

	// 8/14/2012 rmd : Since we are going to sift through them.
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	for( ppp=0; ppp<MAX_POCS_IN_NETWORK; ppp++ )
	{
		// 10/23/2015 rmd : We are going to loop through ALL poc's in the network. Setting the mv
		// and pump energized bits in each poc. Only the POC's at this box get included in the
		// message to the TPMicro. We set the bits in ALL poc's however for user display purposes so
		// he can see whats active around the network.
		
		// ----------
		
		if( poc_preserves.poc[ ppp ].poc_gid != 0 )
		{
			lpoc = nm_POC_get_pointer_to_poc_with_this_gid_and_box_index( poc_preserves.poc[ ppp ].poc_gid, poc_preserves.poc[ ppp ].box_index );
			
			if( lpoc == NULL )
			{
				// 7/9/2014 rmd : This is an error. Alert and set error flag.
				ALERT_MESSAGE_WITH_FILE_NAME( "file POC not found" );

				have_seen_an_error = (true);
			}
			else
			{
				// 10/23/2015 rmd : Our job here is to set the MV energized and PUMP energized bits to
				// reflect the various system status possible (MLB, MVOR, irrigating). There are VERY
				// important SANITY checks we do here. To make sure things like running the pump with no MV
				// open do not occur.
				//
				// 10/23/2015 rmd : You will see we also go to very elaborate lengths to make sure we don't
				// keep MV's open or PUMP's running when we are not irrigating. Like could happen during
				// communication failures.
				
				// ----------
				
				// 9/26/2013 rmd : Capture setting prior to turn ON to see if we should clear the current
				// flags.
				BOOL_32		pre_mv, pre_pump;
				
				pre_mv = poc_preserves.poc[ ppp ].ws[ 0 ].pbf.master_valve_energized_irri;
				
				pre_pump = poc_preserves.poc[ ppp ].ws[ 0 ].pbf.pump_energized_irri;
									
				// ----------
				
				at_least_one_station_in_this_pocs_mainline_is_open = (false);
				
				xSemaphoreTakeRecursive( irri_irri_recursive_MUTEX, portMAX_DELAY );
			
				imlss = nm_ListGetFirst( &irri_irri.list_of_irri_all_irrigation );
				
				while( imlss != NULL )
				{
					if( imlss->ibf.station_is_ON )
					{
						if( (imlss->system_gid) == (poc_preserves.poc[ ppp ].this_pocs_system_preserves_ptr->system_gid) )
						{
							at_least_one_station_in_this_pocs_mainline_is_open = (true);
							
							break;  // we can stop now
						}
					}
			
					imlss = nm_ListGetNext( &irri_irri.list_of_irri_all_irrigation, imlss );
				}
			
				xSemaphoreGiveRecursive( irri_irri_recursive_MUTEX );
	
				// ----------
				
				// 9/29/2015 rmd : Reset our messages with no valves ON logic.
				if( at_least_one_station_in_this_pocs_mainline_is_open )
				{
					poc_preserves.poc[ ppp ].msgs_to_tpmicro_with_no_valves_ON = 0;
				}
				else
				{
					// 9/12/2014 rmd : Do not need a test on the roll-over. It could increment safely for 136
					// years!. And each time a valve comes ON goes back to zero.
					poc_preserves.poc[ ppp ].msgs_to_tpmicro_with_no_valves_ON += 1;
				}
	
				// ----------
				
				// 7/17/2014 rmd : At this point the outputs have been set to energize based upon either the
				// last received token or the management of the bypass task if the decoder belongs to a
				// bypass. We still need to incorporate the MAINLINE BREAK status and the MVOR status.
				// Potentially overriding the requested state up to this point.

				there_is_a_mlb = SYSTEM_PRESERVES_there_is_a_mlb( &(poc_preserves.poc[ ppp ].this_pocs_system_preserves_ptr->delivered_mlb_record) );

				// ----------
				
				// 10/23/2015 rmd : Identify this POC as a BYPASS for special handling.
				if( POC_get_type_of_poc( poc_preserves.poc[ ppp ].poc_gid ) == POC__FILE_TYPE__DECODER_BYPASS )
				{
					its_a_bypass = (true);

					// 7/8/2014 rmd : Use the formal GET function so value is range checked.
					levels = POC_get_bypass_number_of_levels( lpoc );
				}
				else
				{
					its_a_bypass = (false);

					levels = 1;
				}
				
				// ----------
	
				// 10/23/2015 rmd : PUMP HANDLING - handle the PUMP energize bit separately from the MV
				// logic below. Too complicated otherwise.
				if( poc_preserves.poc[ ppp ].this_pocs_system_preserves_ptr->sbf.mv_open_for_irrigation )
				{
					// 10/2/2015 rmd : At this time ONLY terminal based POC's have a pump output. If you allow a
					// decoder based POC to set its pump output the TPMicro will balk about this unexpected
					// setting. If we ever introduce a PUMP output POC decoder we will need to relax this code.
					if( poc_preserves.poc[ ppp ].poc_type__file_type == POC__FILE_TYPE__TERMINAL )
					{
						// 10/8/2018 ajv : We've added a new flag that allows the user to specify whether a pump is 
						// physically attached to the POC terminal or not. If the flag is false for this POC, don't 
						// energize the pump. 
						if( POC_get_has_pump_attached( lpoc ) == (true) )
						{
							if( poc_preserves.poc[ ppp ].this_pocs_system_preserves_ptr->sbf.pump_activate_for_irrigation )
							{
								poc_preserves.poc[ ppp ].ws[ 0 ].pbf.pump_energized_irri = (true);
							}
							else
							{
								poc_preserves.poc[ ppp ].ws[ 0 ].pbf.pump_energized_irri = (false);
							}
						}
						else
						{
							poc_preserves.poc[ ppp ].ws[ 0 ].pbf.pump_energized_irri = (false);
						}
					}
					else
					{
						poc_preserves.poc[ ppp ].ws[ 0 ].pbf.pump_energized_irri = (false);
					}
				}
				else
				{
					poc_preserves.poc[ ppp ].ws[ 0 ].pbf.pump_energized_irri = (false);
				}

				// ----------
				
				// 10/23/2015 rmd : Remember when opening MV's for the case of a BYPASS you do NOT directly
				// manipulate the mv_energized bits. That is done in the BYPASS ENGINE.
				
				if( there_is_a_mlb || poc_preserves.poc[ ppp ].this_pocs_system_preserves_ptr->sbf.delivered_MVOR_in_effect_closed )
				{
					// 10/23/2015 rmd : There is a MLB! Or the MVOR is calling for the master valves to be
					// closed. Make sure the BYPASS is de-activated and directly manipulate all levels to make
					// sure that ALL master valves are closed. And ALSO that the PUMP is not running!

					poc_preserves.poc[ ppp ].bypass_activate = (false);
					
					// 10/23/2015 rmd : And whether a BYPASS or not go through and take direct control over each
					// master valve. Closing them ALL.
					for( i=0; i<levels; i++ )
					{
						POC_PRESERVES_set_master_valve_energized_bit( &(poc_preserves.poc[ ppp ].ws[ i ]), MASTER_VALVE_ACTION_CLOSE );

						// 10/23/2015 rmd : And of course the pump shouldn't be running during a MLB.
						poc_preserves.poc[ ppp ].ws[ i ].pbf.pump_energized_irri = (false);
					}
				}
				else
				if( poc_preserves.poc[ ppp ].this_pocs_system_preserves_ptr->sbf.delivered_MVOR_in_effect_opened || poc_preserves.poc[ ppp ].this_pocs_system_preserves_ptr->sbf.mv_open_for_irrigation )
				{
					// 10/23/2015 rmd : For the case of MVOR OPENED or IRRIGATION we handle a bypass by only
					// setting its activate bit. For non bypass we set level 0 MV opened. And we do not mess
					// with the PUMP setting. That is managed by the open_for_irrigation flag in the prior
					// section.
					if( its_a_bypass )
					{
						poc_preserves.poc[ ppp ].bypass_activate = (true);
					}
					else
					{
						// 10/23/2015 rmd : For the case of a regular POC only need to manipulate level 0.
						POC_PRESERVES_set_master_valve_energized_bit( &(poc_preserves.poc[ ppp ].ws[ 0 ]), MASTER_VALVE_ACTION_OPEN );
					}
				}
				else
				{
					// 8/8/2012 rmd : The MV's are to be IDLED. Meaning not energized. Irrigation has completed
					// and therefore we de-energize the valves. Or de-activate the BYPASS.
					//
					// 10/22/2015 rmd : This clearing of the energize bit UNDOES the MLB action taken for the
					// MV. What that means for a NC MV is nothing. For a NO MV however it means it OPENS the
					// valve when the MLB is CLEARED. This may be controversial. We may want to do nothing till
					// we irrigate so as not to suprise the operator when he clears the MLB. On the other hand
					// maybe it is better to show him what will happen when irrigation occurs? In any case when
					// it finally does OPEN it takes 150 seconds before another MLB can be detected due to our
					// block out timer after the MV just opens!
					//
					// 10/22/2015 rmd : This also serves to de-energize the MV output when MVOR is no longer in
					// effect.
					if( its_a_bypass )
					{
						poc_preserves.poc[ ppp ].bypass_activate = (false);
					}
					else
					{
						poc_preserves.poc[ ppp ].ws[ 0 ].pbf.master_valve_energized_irri = (false);
					}

					// 8/14/2012 rmd : And the FAIL-SAFE against energizing the pump with the MV closed (well
					// its only closed for the case of a NC MV). No matter what the system is asking for. As a
					// matter of fact it is an error if it is asking for the PUMP to be energized without a
					// request to open the MV.
					poc_preserves.poc[ ppp ].ws[ 0 ].pbf.pump_energized_irri = (false);

					if( poc_preserves.poc[ ppp ].this_pocs_system_preserves_ptr->sbf.pump_activate_for_irrigation )
					{
						ALERT_MESSAGE_WITH_FILE_NAME( "IRRI_FLOW: unexp pump rqst!" );
					}
				}

				// ----------
				
				// 10/23/2015 rmd : This is a different kind of a test. We are using the IRRI irrigation
				// list to see if we are irrigating. If not and we don't have a MVOR opened or closed
				// condition I don't expect to see any MV's energized! We are double checking the system
				// mv_open_for_irrigation variable setting to make sense.
				if( !there_is_a_mlb && 
				    !(poc_preserves.poc[ ppp ].this_pocs_system_preserves_ptr->sbf.delivered_MVOR_in_effect_closed) && 
					!(poc_preserves.poc[ ppp ].this_pocs_system_preserves_ptr->sbf.delivered_MVOR_in_effect_opened) )
				{
					if( poc_preserves.poc[ ppp ].msgs_to_tpmicro_with_no_valves_ON >= POC_MSGS_WITH_NO_VALVES_ON_MV_LIMIT )
					{
						// 7/17/2014 rmd : Let's see if we ever run into this problem. Where we want to energize for
						// apparently no good reason.
						for( i=0; i<levels; i++ )
						{
							if( poc_preserves.poc[ ppp ].ws[ i ].pbf.master_valve_energized_irri )
							{
								// 10/12/2012 rmd : We use the ALERT LIMIT to manage the number of alerts generated during
								// this condition. So we don't end up rolling out alerts that may be important to
								// us.
								if( poc_preserves.poc[ ppp ].msgs_to_tpmicro_with_no_valves_ON < POC_MSGS_WITH_NO_VALVES_ON_MV_ALERT_LIMIT )
								{
									// 2/24/2015 rmd : Should never see this line.
									Alert_Message( "TPMICRO COMM: MV energized w/ no valves ON!" );
								}
							}
		
							// 7/17/2014 rmd : Whatever the reason or type of valve (NO or NC) do not energize the MV
							// when there are no valves ON and the other reasons to energize aren't in play.
							poc_preserves.poc[ ppp ].ws[ i ].pbf.master_valve_energized_irri = (false);
						}


					}
				}
				
				// ----------
				
				// 9/29/2015 rmd : PUMP - the final logic to protect and allow operation of the PUMP.
				// Remember in the case of a MVOR opened we could have valves irrigating also and therefore
				// valid to have the pump energized. So cannot include that term in the following
				// if.
				if( there_is_a_mlb || 
					(poc_preserves.poc[ ppp ].msgs_to_tpmicro_with_no_valves_ON >= POC_MSGS_WITH_NO_VALVES_ON_PUMP_LIMIT) ||
					(poc_preserves.poc[ ppp ].this_pocs_system_preserves_ptr->sbf.delivered_MVOR_in_effect_closed) )
				{
					// 7/17/2014 rmd : Let's see if we ever run into this problem. Where we want to energize for
					// apparently no good reason. Possibly a dangerous reason in the case of the pump!
					for( i=0; i<levels; i++ )
					{
						if( poc_preserves.poc[ ppp ].ws[ i ].pbf.pump_energized_irri )
						{
							if( there_is_a_mlb )
							{
								Alert_Message( "TPMICRO COMM: PUMP energized w/ MLB!" );
							}
							else
							if( poc_preserves.poc[ ppp ].msgs_to_tpmicro_with_no_valves_ON < POC_MSGS_WITH_NO_VALVES_ON_PUMP_ALERT_LIMIT )
							{
								Alert_Message( "TPMICRO COMM: PUMP energized w/ no valves ON!" );
							}
							else
							if( poc_preserves.poc[ ppp ].this_pocs_system_preserves_ptr->sbf.delivered_MVOR_in_effect_closed )
							{
								Alert_Message( "TPMICRO COMM: PUMP energized w/ MVOR closed" );
							}
						}

						// 9/29/2015 rmd : For the reasons of the if statement do not energize the PUMP.
						poc_preserves.poc[ ppp ].ws[ i ].pbf.pump_energized_irri = (false);
					}

				}
				
				// ----------
				
				// 10/26/2015 rmd : If has just been energized clear flags. These flags are for display
				// purposes only (i think).
				if( !pre_mv && poc_preserves.poc[ ppp ].ws[ 0 ].pbf.master_valve_energized_irri )
				{
					poc_preserves.poc[ ppp ].ws[ 0 ].pbf.no_current_mv = (false);

					poc_preserves.poc[ ppp ].ws[ 0 ].pbf.shorted_mv = (false);
				}

				if( !pre_pump && poc_preserves.poc[ ppp ].ws[ 0 ].pbf.pump_energized_irri )
				{
					poc_preserves.poc[ ppp ].ws[ 0 ].pbf.no_current_pump = (false);

					poc_preserves.poc[ ppp ].ws[ 0 ].pbf.shorted_pump = (false);
				}

				// ----------
				
				// 10/23/2015 rmd : Now if this POC belongs to this box consider sending to the TPMicro if
				// there is work for the TPMicro to do. Remember the absense of a POC is our indication to
				// the tpmicro NOT to enrgize the output.
				if( poc_preserves.poc[ ppp ].box_index == FLOWSENSE_get_controller_index() )
				{
					for( i=0; i<levels; i++ )
					{
						// 10/26/2015 rmd : If mv energized to be included.
						include_me = poc_preserves.poc[ ppp ].ws[ i ].pbf.master_valve_energized_irri;

						if( (i==0) && poc_preserves.poc[ ppp ].ws[ i ].pbf.pump_energized_irri )
						{
							include_me = (true);
						}

						if( include_me )
						{
							if( number_of_pocs < MAX_NUMBER_OF_POCS_ON_AT_A_TIME )
							{
								number_of_pocs += 1;
					
								// ----------
								
								// 5/16/2014 rmd : Zero out for next. Making sure mv and pump off.
								memset( &pos, 0x00, sizeof( POC_ON_FOR_TPMICRO_STRUCT ) );
					
								// 5/16/2014 rmd : Get the serial number. Which if 0 indicates the terminal poc.
								pos.decoder_serial_number = poc_preserves.poc[ ppp ].ws[ i ].decoder_serial_number;
					
								// ----------
					
								if( poc_preserves.poc[ ppp ].ws[ i ].pbf.master_valve_energized_irri )
								{
									B_SET( pos.pump_and_mv_control, POC_CONTENT_TO_TP_MICRO_energize_mv );
								}
					
								if( poc_preserves.poc[ ppp ].ws[ i ].pbf.pump_energized_irri )
								{
									B_SET( pos.pump_and_mv_control, POC_CONTENT_TO_TP_MICRO_energize_pump );
								}
		
								// ----------
								
								memcpy( ucp, &pos, sizeof( POC_ON_FOR_TPMICRO_STRUCT ) );
											
								ucp += sizeof(POC_ON_FOR_TPMICRO_STRUCT);
						
								rv += sizeof(POC_ON_FOR_TPMICRO_STRUCT);
							}
							else
							{
								// 9/29/2015 rmd : We are SKIPPING energizing some POC's. Otherwise we could have a memory
								// overflow as the message memory space won't hold them. Additionally there is an electrical
								// limit that has to come into play eventually. We are limiting to 6 energized [well the
								// trminal can sneak in the pump output for a total of 7 the way this function logic is
								// coded].
								Alert_Message( "POC ON hit the limit! Cannot energize them all!!" );
							}

						}  // if included
						
					}  // for each level
					
				}  // of POC is at this box
					
			}  // couldn't find the poc in the file

		}  // of if this poc is in use
		
	}  // for all the possible pocs
	
	// ----------

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

	// ----------

	// 9/24/2015 rmd : Regardless of value make place the count in the message.
	*number_of_pocs_ptr = number_of_pocs;
	
	// ----------

	if( have_seen_an_error || (number_of_pocs == 0) )
	{
		mem_free( *pon_ptr );

		*pon_ptr = NULL;

		rv = 0;
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 build_lights_ON_for_tpmicro_msg( UNS_8 **lon_ptr )
{
	UNS_32	rv;

	BOOL_32	have_seen_an_error; 

	UNS_8	*ucp;

	UNS_8	light_count, *light_count_ptr;

	STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT		los;

	IRRI_LIGHTS_LIST_COMPONENT	*illc;
	
	UNS_32		lindex;
	
	// ----------

	rv = 0;

	// ---------------

	have_seen_an_error = (false);

	// ---------------

	// 4/3/2012 rmd : Because we are looking at the list and we require it to be
	// stable.
	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	// 8/14/2012 rmd : Based on message format get memory. NEVER returns NULL.
	ucp = mem_malloc( sizeof(UNS_8) + (MAX_LIGHTS_IN_A_BOX * sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT)) );

	*lon_ptr = ucp;

	// ------------

	light_count = 0;

	// 9/24/2015 rmd : Initialize count and record location of count in message (written into
	// message later).
	light_count_ptr = ucp;

	ucp += sizeof(UNS_8);

	rv += sizeof(UNS_8);

	// ------------

	// 4/18/2014 rmd : So we don't have to execute this somewhat heavy function over and over
	// again.
	lindex = FLOWSENSE_get_controller_index();

	// 5/16/2014 rmd : Remember this is the irri list.
	illc = nm_ListGetFirst( &irri_lights.header_for_irri_lights_ON_list );

	while( illc != NULL )
	{
		// 9/24/2015 rmd : Only for OUR outputs. We are building up a message for our TPMicro to
		// turn on the light outputs physically located at this box.
		if( (illc->box_index_0 == lindex) && (illc->light_is_energized == (true)) )
		{
			// 8/14/2012 rmd : If we're already at the limit git.
			if( light_count >= MAX_LIGHTS_IN_A_BOX )
			{
				Alert_message_on_tpmicro_pile_M( "too many LIGHTS ON" );

				// 8/14/2012 rmd : Indicate no message.
				have_seen_an_error = (true);

				break;
			}
			else
			{
				los.terminal_number_0 = illc->output_index_0;

				los.decoder_serial_number = DECODER_SERIAL_NUM_DEFAULT;

				// 9/24/2015 rmd : This is irrelevant but I set for cleanlyness.
				los.decoder_output = DECODER_OUTPUT_A_BLACK;
				
				// ----------
				
				memcpy( ucp, &los, sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT) );
				
				ucp += sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT);

				rv += sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT);

				light_count += 1;
				
			}  // of if we haven't hit the count limit yet

		}  // of a output at this box and is ON

		illc = nm_ListGetNext( &irri_lights.header_for_irri_lights_ON_list, illc );
	}

	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

	// ------------

	// 9/24/2015 rmd : Regardless of value make place the count in the message.
	*light_count_ptr = light_count;
	
	// ------------

	if( have_seen_an_error || (light_count == 0) )
	{
		mem_free( *lon_ptr );

		*lon_ptr = NULL;

		rv = 0;
	}

	// ------------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION When the tp micro message timer expires a queue message is pushed onto the
    main COMM_MNGR task queue. When that queue item is pulled this function is directly
    called. The message rate is currently set to 1 hertz.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task when the tpmicro message
    rate timer expires.

	@RETURN (none)

	@ORIGINAL 2012.08.08 rmd

	@REVISIONS (none)
*/

// 1/8/2013 rmd : So we don't flood the entire pile with the 'no msgs from TPmicro'.
#define	TPMICRO_MAX_NUMBER_OF_MISSING_MSG_ALERTS	(5)

static void kick_out_the_next_normal_tpmicro_msg( COMM_MNGR_TASK_QUEUE_STRUCT *pcmqs )
{
	// 4/2/2014 rmd : Executes within the context of the comm_mngr.
	
	// ----------

	DATA_HANDLE								ldh;

	UNS_8									*ucp;

	UNS_32									block_size, remaining_bytes;	
	
	UNS_32									piece_size;
		
	WIND_SETTINGS_STRUCT					wss;
	
	UNS_32									ms_between_messages;

	char									from_str[ 32 ], to_str[ 32 ], str_64[ 64 ];

	DATE_TIME								from_dt, to_dt;
	
	// 9/23/2014 rmd : To be set (true) means the TPMicro is already running our application. As
	// it had to deliver up its executing code version information.
	BOOL_32									isp_entry_requested;
	
	// ----------

	if( pcmqs->event != COMM_MNGR_EVENT_tpmicro_msg_rate_timer_expired )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "TPMicro comm unexpd event" );
	}
	else
	{
		if( tpmicro_comm.number_of_outgoing_since_last_incoming < (TPMICRO_OUTGOING_BETWEEN_INCOMING_LIMIT + TPMICRO_MAX_NUMBER_OF_MISSING_MSG_ALERTS) )
		{
			tpmicro_comm.number_of_outgoing_since_last_incoming += 1;
	
			if( tpmicro_comm.number_of_outgoing_since_last_incoming > TPMICRO_OUTGOING_BETWEEN_INCOMING_LIMIT )
			{
				if( tpmicro_comm.number_of_outgoing_since_last_incoming == (TPMICRO_OUTGOING_BETWEEN_INCOMING_LIMIT + TPMICRO_MAX_NUMBER_OF_MISSING_MSG_ALERTS) )
				{
					Alert_message_on_tpmicro_pile_M( "last warning, no msgs from TPMicro" );
					
					// 4/2/2014 rmd : So if the TPMICRO is not responding what is wrong. Did it perhaps fail to
					// successfully perform a code update. And we need to reboot to try again. That is a real
					// possibility. I have seen it under conditions that have now been corrected for. TODO
					// reboot whole controller once every 30 minutes when in this mode? I choose 30 minutes to
					// allow time for the controller initiated alerts to get to the comm_server telling about
					// this condition (if any).
					tpmicro_comm.up_and_running = (false);
				}
				else
				{
					snprintf( str_64, sizeof(str_64), "%u seconds since last msg from TPmicro", tpmicro_comm.number_of_outgoing_since_last_incoming );
	
					Alert_message_on_tpmicro_pile_M( str_64 );
				}
			}
		}
	
		// ----------

		// 3/4/2014 rmd : This is THE place to test if the tpmicro code need updating. The two flags
		// become true only after the information was setup to be requested and it the information
		// is sucessfully in place. The test pending flag is a critical flag used when deciding if
		// the tpmicro is ready to send scan or token messages.
		isp_entry_requested = (false);

		if( tpmicro_comm.code_version_test_pending && tpmicro_comm.file_system_code_date_and_time_valid && tpmicro_comm.tpmicro_executing_code_date_and_time_valid )
		{
			// 2/11/2015 rmd : Previosuly we checked here if the file system had NEWER code than the TP
			// Micro. Well this leaves the MATCHED PAIR problem door open. That is the TP Micro has
			// newer code that is not compatible with the main app itself (say the user plugged in a
			// TPMicro that held new code). When this condition exists the messaging between the main
			// board and tpmicro may be incompatible - leading to a potentially non-functional state
			// (can't irrigate). AJ and I discussed this and decided the best course of action was to
			// assume what was held in the file system was indeed a MATCHED PAIR and that the tpmicro
			// should be running what the file system holds. Nothing else. So here we use the != to make
			// the tests.
			if( (tpmicro_comm.file_system_code_date != tpmicro_comm.tpmicro_executing_code_date) || (tpmicro_comm.file_system_code_time != tpmicro_comm.tpmicro_executing_code_time) )
			{
				// 3/4/2014 rmd : This IS the flag that will cause the code to be updated.
				isp_entry_requested = (true);
			}

			if( isp_entry_requested )
			{
				from_dt.D = tpmicro_comm.tpmicro_executing_code_date;		
				from_dt.T = tpmicro_comm.tpmicro_executing_code_time;		

				to_dt.D = tpmicro_comm.file_system_code_date;		
				to_dt.T = tpmicro_comm.file_system_code_time;		

				// ----------

				// 9/23/2014 rmd : RECOGNIZE - there are two ways the TPMicro ISP mode can be running. One
				// way is here by us requesting the TPMicro application (our application that is) to enter
				// the mode. The alternate means is that there is NO tpmicro application (our application)
				// in the tpmicro flash. And therefore when the tpmicro powers up it automatically runs the
				// ISP bootloader program.
				//
				// 9/23/2014 rmd : Because of these two ways to enter we put up the CS3000 main board
				// display user notification at a common point where either entry means converge. However
				// alert lines unique to this reason for ISP entry remain here.
				Alert_Message( "TPMicro code ISP update starting" );	

				Alert_message_on_tpmicro_pile_M( "TPMicro code ISP update starting" );	

				snprintf( str_64, sizeof(str_64), "... from %s to %s", DATE_TIME_to_DateTimeStr_32( from_str, sizeof(from_str), from_dt ), DATE_TIME_to_DateTimeStr_32( to_str, sizeof(to_str), to_dt ) );

				Alert_Message( str_64 );

				Alert_message_on_tpmicro_pile_M( str_64 );
			}
			else
			{
				// 10/24/2017 rmd : It is IMPORTANT to understand that this part of the 'if' is ALWAYS run
				// through, eventually, following a reboot. So even if there is a tpmicro binary update to
				// perform, AFTER that we restart the ISP checking process again, and then it passes the
				// second time, and we pass through this code, beginning the SCAN.

				// 10/30/2017 rmd : Capture the reason for the scan. The alert line is made when the scan
				// actually starts.
				comm_mngr.start_a_scan_captured_reason = COMM_MNGR_reason_to_scan_startup;
				
				// 7/21/2016 rmd : Okay so finally during the startup sequence we begin the scan. Both
				// Master or a Slave - this is to be done. After we set it operational below at the next
				// token rate timer timeout we will attempt to kick out the next token and see the scan
				// request flag set. And the scan will start. BUT only after a whole variety of criteria are
				// met including the Flowsense device POWERUP criteria.
				comm_mngr.start_a_scan_at_the_next_opportunity = (true);
				
				comm_mngr.mode = COMM_MNGR_MODE_operational;
			}

			// ----------

			// 3/4/2014 rmd : We've made the evaluation. Set flag back to (false).
			tpmicro_comm.code_version_test_pending = (false);
		}
		
		// ----------
	
		// 2/10/2014 rmd : Setup the normal timer delay of 1 second. Only one message, requesting
		// the ISP mode, increases the timer frequency.
		ms_between_messages = TPMICRO_NORMAL_MS_BETWEEN_MSGS;
		
		// ----------
	
		// 3/22/2013 rmd : Get the agreed upon maximum packet size for the TP MICRO. In our case
		// here the size we are working with does not include the pre and post ambles. Those are
		// added on to the message in a bigger memory block after we are done in this function. So
		// not required to include them in the memory block size needed here in this function to
		// build the message in.
		block_size = ( TPMICRO_MAX_AMBLE_TO_AMBLE_PACKET_SIZE - (2 * sizeof(AMBLE_TYPE)) );
		
		ldh.dptr = mem_malloc( block_size );
	
		ucp = ldh.dptr;
	
		ldh.dlen = 0;
		
		// ----------
	
		// 1/31/2014 rmd : Since the CRC is to be included in this functions local memory block back
		// its size out now. So the rest of the message leaves space for the crc.
		remaining_bytes = block_size - sizeof(CRC_TYPE);
		
		// ----------
	
		// 4/3/2012 rmd : Fill out the TPL DATA HEADER as needed.
		TPL_DATA_HEADER_TYPE	tdh;
	
		memset( &tdh, 0x00, sizeof(TPL_DATA_HEADER_TYPE) );
		
		// ----------
	
		set_this_packets_CS3000_message_class( &tdh, MSG_CLASS_CS3000_TO_TP_MICRO );
		
		// ----------
	
		// 4/3/2012 rmd : We set the FROM address with our serial number.
		memcpy( &(tdh.H.FromTo.from), &(config_c.serial_number), 3 );
		
		// ----------
	
		// 3/12/2013 rmd : NOTE - the TPMicro DOES NOT check against the TO address. The criteria at
		// the TP Micro to accept an incoming packet as a message to it, is that the packet came in
		// on the MAIN port and the class is MSG_CLASS_TO_TPMICRO. And thats all (of course the CRC
		// has to be good too).
	
		// 4/4/2012 rmd : Plunk it into the packet we're building.
		memcpy( ucp, &tdh, sizeof(TPL_DATA_HEADER_TYPE) );
	
		ucp += sizeof( TPL_DATA_HEADER_TYPE );
	
		ldh.dlen += sizeof( TPL_DATA_HEADER_TYPE );
	
		remaining_bytes -= sizeof( TPL_DATA_HEADER_TYPE );
		
		// ----------
	
		// 4/3/2012 rmd : The fc structure location within the message needs to be filled out. And
		// the bits set as we build the message. To be EXTRA safe you should not simply declare a
		// ptr to a FOAL_COMMANDS structure at the current value of ucp. If ucp turned out to not be
		// ALIGNED on a word boundary and you tried to write to fc->pieces would we get an ADDRESS
		// EXCEPTION???? I do not want to find out.
		FOAL_COMMANDS	fc;
	
		unsigned char	*where_to_place_fc;
	
		where_to_place_fc = ucp;
	
		fc.mid = MID_MAIN_TO_TPMICRO;
		fc.pieces = 0x00;  // ALL bits cleared to start.
	
		ucp += sizeof( FOAL_COMMANDS );
	
		ldh.dlen += sizeof( FOAL_COMMANDS );
	
		remaining_bytes -= sizeof( FOAL_COMMANDS );
	
		// ----------
	
		if( tpmicro_comm.tpmicro_request_executing_code_date_and_time )
		{
			// 4/1/2014 rmd : If we are requesting the executing code date and time our convention is
			// not to add message content beyond the request. I view it as just asking for trouble. I
			// think it better to make it through the executing code test. And then build up REGULAR
			// messages. This flag means we are still not past the executing code check.
			B_SET( fc.pieces, BIT_TTPM_request_executing_code_date_and_time );
			
			// 12/8/2014 rmd : I don't particularly like clearing this flag here. If I had to do again I
			// would clear it when I received the data. To cover if the message is missed by the
			// tpmicro. But it is all tested now. So I'm not inclined to move it. If we have trouble
			// seeing the 'Y' on the main screen this could be a contributing factor!
			tpmicro_comm.tpmicro_request_executing_code_date_and_time = (false);
		}
		else
		if( isp_entry_requested )
		{
			// 3/27/2014 rmd : NOTE - when the message contains an ISP request it logically makes no
			// sense to include further message content because the first bit processed by the tpmicro
			// is the ISP request bit. And that processing causes a code restart and the rest of the
			// message to be dropped. FUTHERMORE there is a bug introduced if we were to include further
			// message content. Specifically the with clearing the request for the executing code data
			// and time when combined with the ISP request. Because recognize with the ISP request the
			// tpmicro will not be responding with the code data & time!

			// ----------
			
			isp_entry_requested = (false);
	
			// ----------

			// 2/10/2014 rmd : After we set this bit in the message should we build out the rest of the
			// message? When this bit is processed the microcontroller resident ISP bootloader will
			// execute. Our user application is done. There really is no point to including more in the
			// message.
			B_SET( fc.pieces, BIT_TTPM_isp_entry_requested );
			
			tpmicro_comm.in_ISP = (true);
	
			tpmicro_comm.isp_state = ISP_STATE_question_mark;  // start at the beginning.
	
			// 2/19/2014 rmd : What about this delay. This is the delay from when we send this message,
			// which may take about 5 ms to send, for the tpmicro to receive it. Process it. Set itself
			// up for a restart. Restart. Enter the ISP mode and be ready for the "?". It does all seem
			// to happen within 10ms. But what's the hurry. I'm going to hard code this UNIQUE delay to
			// 40ms so we are sure the tpmicro is waiting in ISP mode to recevice the "?".
			ms_between_messages = 40;
		}
		else
		{
			// 4/1/2014 rmd : SHOULD we wait till the scan has completed? We are not at this time. And I
			// don't know of a reason why. It was just a thought. One complication is after a scan we
			// are counting on the what's installed to be in place. In the case of a standalone I think
			// there is a RACE going on between the TPMICRO messages and proceeding towards the
			// COMM_MNGR generating tokens. The what's installed information from the tpmicro is to be
			// available. How to guarantee?

			// ----------
		
			if( tpmicro_data.request_whats_installed_from_tp_micro )
			{
				// 3/25/2013 rmd : Setting this bit causes the TP Micro to send the what's installed info.
				B_SET( fc.pieces, BIT_TTPM_request_whats_installed );
				
				// 12/8/2014 rmd : I don't particularly like clearing this flag here. If I had to do again I
				// would clear it when I received the data. To cover if the message is missed by the
				// tpmicro. But it is all tested now. So I'm not inclined to move it. If we have trouble
				// seeing the 'Y' on the main screen this could be a contributing factor!
				tpmicro_data.request_whats_installed_from_tp_micro = (false);
			}
			
			// ----------
		
			// 9/27/2013 rmd : DO THIS FIRST before we test to send output, MV, and PUMP state
			// information. Because once we ACK the short or no_current the message we send should
			// include updated station, MV, and PUMP output states.
			if( tpmicro_data.terminal_short_or_no_current_state == TERMINAL_SONC_STATE_ack_to_tpmicro )
			{
				// 8/20/2012 rmd : Tell the tp micro our process has completed thereby enabling the tpmicro
				// to once again accept TERMINAL turn ON commands.
				B_SET( fc.pieces, BIT_TTPM_short_ack_terminal );
		
				tpmicro_data.terminal_short_or_no_current_state = TERMINAL_SONC_STATE_idle;
			}
		
			// ----------
		
			// 10/31/2013 rmd : Before we send the station and POC turn ON information check to clear
			// the two wire cable anomaly.
			if( tpmicro_data.two_wire_cable_excessive_current_xmission_state == TWO_WIRE_CABLE_ANOMALY_STATE_send_ack_to_tpmicro )
			{
				B_SET( fc.pieces, BIT_TTPM_clear_two_wire_cable_excessive_current );
		
				tpmicro_data.two_wire_cable_excessive_current_xmission_state = TWO_WIRE_CABLE_ANOMALY_STATE_idle;
			}
			
			// ----------
		
			// 10/31/2013 rmd : Before we send the station and POC turn ON information check to clear
			// the two wire cable anomaly.
			if( tpmicro_data.two_wire_cable_over_heated_xmission_state == TWO_WIRE_CABLE_ANOMALY_STATE_send_ack_to_tpmicro )
			{
				B_SET( fc.pieces, BIT_TTPM_ack_two_wire_cable_overheated );
		
				tpmicro_data.two_wire_cable_over_heated_xmission_state = TWO_WIRE_CABLE_ANOMALY_STATE_idle;
			}
			
			// ----------
		
			// 10/31/2013 rmd : INCLUDE THE STATIONS ON. Don't be tempted to not send the station
			// information just because there is a terminal short condition being processed. Remember
			// the station info we send is a combination of both terminal and 2W cable based stations.
			// And the absence of the two wire stations just because we were processing a terminal based
			// short would cause all the 2W stations to start turning OFF. In the TP Micro the incoming
			// terminal station requests are dropped if we are in the midst of completing the terminal
			// short processing sequence.
			UNS_32	stations_ON_bytes;
		
			UNS_8	*stations_ON_ptr;
		
			stations_ON_bytes = build_stations_ON_for_tpmicro_msg( &stations_ON_ptr );
		
			if( stations_ON_ptr != NULL )
			{
				if( remaining_bytes >= stations_ON_bytes )
				{
					// 8/9/2012 rmd : Note - when there are ZERO stations ON we do not set the bit in pieces.
					// And YES that is the indication to the tp micro to turn off all STATIONS. Remember this
					// bit is for the stations. Not the MVs, PUMPs, or LIGHTs. [ Note - with 1 on at a time we
					// likely will send a message to the tp micro without this bit set in pieces at each station
					// transition. And that's fine. The stations turned OFF. Not the MV and PUMPS. ]
					if( stations_ON_bytes > 0 )
					{
						// 4/4/2012 rmd : Set the bit indicating this data type is present within the message.
						B_SET( fc.pieces, BIT_TTPM_here_are_the_stations_to_turn_on );
			
						memcpy( ucp, stations_ON_ptr, stations_ON_bytes );
			
						ucp += stations_ON_bytes;
			
						ldh.dlen += stations_ON_bytes;
			
						remaining_bytes -= stations_ON_bytes;
					}
				}
		
				// 8/14/2012 rmd : And free the memory block. We're done with it. You might question why we
				// get the memory in the function that builds this part of the content and then immediately
				// release it. Well it has to do with the format we are moving to when building messages.
				// Build each piece up front and place into a block of memory. Then once we know the total
				// message size get the memory. And move the previously defined content into place.
				mem_free( stations_ON_ptr );
			}
		
			// ----------
		
			// 10/31/2013 rmd : Always include the POC's ON. See the note above regarding the station ON
			// for more information why we ALWAYS include the POC's ON information.
			UNS_32	poc_info_bytes;
		
			UNS_8	*poc_info_ptr;
		
			poc_info_bytes = build_poc_output_activation_for_tpmicro_msg( &poc_info_ptr );
		
			if( poc_info_ptr != NULL )
			{
				if( remaining_bytes >= poc_info_bytes )
				{
					// 8/9/2012 rmd : Note - when there are ZERO stations ON we do not set the bit in pieces.
					// And YES that is the indication to the tp micro to turn off all STATIONS. Remember this
					// bit is for the stations. Not the MVs, PUMPs, or LIGHTs. [ Note - with 1 on at a time we
					// likely will send a message to the tp micro without this bit set in pieces at each station
					// transition. And that's fine. The stations turned OFF. Not the MV and PUMPS. ]
					if( poc_info_bytes > 0 )
					{
						// 4/4/2012 rmd : Set the bit indicating this data type is present within the message.
						B_SET( fc.pieces, BIT_TTPM_here_are_the_pocs_to_turn_on );
			
						memcpy( ucp, poc_info_ptr, poc_info_bytes );
		
						ucp += poc_info_bytes;
		
						ldh.dlen += poc_info_bytes;
			
						remaining_bytes -= poc_info_bytes;
					}
				}
		
				// 8/14/2012 rmd : And free the memory block. We're done with it. You might question why we
				// get the memory in the function that builds this part of the content and then immediately
				// release it. Well it has to do with the format we are moving to when building messages.
				// Build each piece up front and place into a block of memory. Then once we know the total
				// message size get the memory. And move the previously defined content into place.
				mem_free( poc_info_ptr );
			}		
		
			// ----------
		
			// 9/24/2015 rmd : Include the LIGHTS that should be ON.
			UNS_32	lights_ON_bytes;
		
			UNS_8	*lights_ON_ptr;
		
			lights_ON_bytes = build_lights_ON_for_tpmicro_msg( &lights_ON_ptr );
		
			if( lights_ON_ptr != NULL )
			{
				if( remaining_bytes >= lights_ON_bytes )
				{
					// 9/24/2015 rmd : Note - when there are no lights putputs ON we do not set the bit in
					// pieces. And that is the indication to the tp micro to turn off all the lights outputs.
					if( lights_ON_bytes > 0 )
					{
						// 4/4/2012 rmd : Set the bit indicating this data type is present within the message.
						B_SET( fc.pieces, BIT_TTPM_here_are_the_lights_to_turn_on );
			
						memcpy( ucp, lights_ON_ptr, lights_ON_bytes );
			
						ucp += lights_ON_bytes;
			
						ldh.dlen += lights_ON_bytes;
			
						remaining_bytes -= lights_ON_bytes;
					}
				}
		
				mem_free( lights_ON_ptr );
			}
		
			// ----------
		
			if( tpmicro_data.send_wind_settings_structure_to_the_tpmicro )
			{
				if( remaining_bytes >= sizeof(WIND_SETTINGS_STRUCT) )
				{
					B_SET( fc.pieces, BIT_TTPM_here_are_the_wind_settings );
					
					WEATHER_get_a_copy_of_the_wind_settings( &wss );
					
					memcpy( ucp, &wss, sizeof(WIND_SETTINGS_STRUCT) );
		
					ucp += sizeof(WIND_SETTINGS_STRUCT);
					
					ldh.dlen += sizeof(WIND_SETTINGS_STRUCT);
					
					remaining_bytes -= sizeof(WIND_SETTINGS_STRUCT);
		
					tpmicro_data.send_wind_settings_structure_to_the_tpmicro = (false);
				}
			}
		
			// ----------
		
			if( tpmicro_data.et_gage_clear_runaway_gage )
			{
				B_SET( fc.pieces, BIT_TTPM_clear_run_away_gage_state );
		
				tpmicro_data.et_gage_clear_runaway_gage = (false);
			}
		
			// -----------------
		
			if( tpmicro_data.two_wire_cable_power_operation.send_command )
			{
				piece_size = sizeof( BOOL_32 );
				
				if( remaining_bytes >= piece_size )
				{
					B_SET( fc.pieces, BIT_TTPM_two_wire_control_bus_power );
					
					memcpy( ucp, &tpmicro_data.two_wire_cable_power_operation.on, sizeof(BOOL_32) );
					ucp += sizeof(BOOL_32);
		
					ldh.dlen +=  sizeof(BOOL_32);
		
					remaining_bytes -= sizeof(BOOL_32);
					
					tpmicro_data.two_wire_cable_power_operation.send_command = (false);
				}	
			}
		
			// -----------------
		
			if( tpmicro_data.two_wire_perform_discovery )
			{
				// 5/9/2016 rmd : Only if this controller has a two wire terminal installed. Surprisingly I
				// did not see any test in the TPMicro for this. The TPMicro apparently will do the
				// discovery two-wire terminal or not. I think that's probably okay in that it doesn't hurt
				// anything hardware wise and the result will for sure be no decoders found. But I don't
				// like it. So will block here.
				//
				// 5/11/2016 rmd : Additionally if someone gets tempted to set the perform_discovery
				// variable say using a command from the web this will block the tpmicro from attempting the
				// discovery.
				if( chain.members[ FLOWSENSE_get_controller_index() ].box_configuration.wi.two_wire_terminal_present )
				{
					B_SET( fc.pieces, BIT_TTPM_two_wire_perform_discovery_process );
				}
				
				// 5/11/2016 rmd : Always clear the flag. Whether we set the bit or not. Don't want the flag
				// hanging around 'set'. Remember this is normally set via keypad or web interface for
				// immediate gratification. And the assumption is the unit has been running for a while.
				tpmicro_data.two_wire_perform_discovery = (false);
			}
		
			// 5/11/2016 rmd : Here is a second flag that can trigger a discovery. It only gets set when
			// a factory reset is commanded either via the keypad or the web.
			if( weather_preserves.perform_a_discovery_following_a_FACTORY_reset )
			{
				// 5/11/2016 rmd : So it's complicated. The unit HAS NOT been up and running for a while.
				// And it takes some time (exchanges between the main board and tpmicro) before the whats
				// installed is in place. And then more time until the chain.members is distributed and in
				// place. So this flag is set and stays set UNTIL a 2W Terminal shows up in the
				// configuration and then a discovery is kicked off automatically. REMEMBER this flag isn't
				// set following a first time start up. It is set when the WEB commands a factory reset
				// (part of a panel swap) or the user using the keypad does a factory reset. AND will remain
				// until a two wire terminal shows up. Which could be never.
				//
				// 5/9/2016 rmd : Only if this controller has a two wire terminal installed. Surprisingly I
				// did not see any test in the TPMicro for this. The TPMicro apparently will do the
				// discovery two-wire terminal or not. I think that's probably okay in that it doesn't hurt
				// anything hardware wise and the result will for sure be no decoders found.
				if( chain.members[ FLOWSENSE_get_controller_index() ].box_configuration.wi.two_wire_terminal_present )
				{
					B_SET( fc.pieces, BIT_TTPM_two_wire_perform_discovery_process );

					// 5/11/2016 rmd : And indeed ONLY clear this flag if we set the bit in the message.
					// Otherwise this mechanism of kicking off a discovery WILL NOT WORK!
					weather_preserves.perform_a_discovery_following_a_FACTORY_reset = (false);
				}
			}
		
			// -----------------
		
			if( tpmicro_data.two_wire_clear_statistics_at_all_decoders )
			{
				B_SET( fc.pieces, BIT_TTPM_two_wire_clear_statistics_at_all_decoders );
				
				tpmicro_data.two_wire_clear_statistics_at_all_decoders = (false);
			}
		
			// -----------------
		
			if( tpmicro_data.two_wire_request_statistics_from_all_decoders )
			{
				B_SET( fc.pieces, BIT_TTPM_two_wire_request_statistics_from_all_decoders );
				
				tpmicro_data.two_wire_request_statistics_from_all_decoders = (false);
			}
		
			// -----------------
		
			if( tpmicro_data.two_wire_start_all_decoder_loopback_test )
			{
				B_SET( fc.pieces, BIT_TTPM_two_wire_start_all_decoder_loopback_test );
				
				tpmicro_data.two_wire_start_all_decoder_loopback_test = (false);
			}
		
			// -----------------
		
			if( tpmicro_data.two_wire_stop_all_decoder_loopback_test )
			{
				B_SET( fc.pieces, BIT_TTPM_two_wire_stop_all_decoder_loopback_test );
				
				tpmicro_data.two_wire_stop_all_decoder_loopback_test = (false);
			}
		
			// ----------
		
			if( tpmicro_data.send_2w_solenoid_location_on_off_command )
			{
				piece_size = sizeof( tpmicro_data.two_wire_solenoid_location_on_off_command );
				
				if( remaining_bytes >= piece_size )
				{
					B_SET( fc.pieces, BIT_TTPM_two_wire_decoder_solenoid_on_off );
					
					memcpy( ucp, &tpmicro_data.two_wire_solenoid_location_on_off_command, sizeof(tpmicro_data.two_wire_solenoid_location_on_off_command) );

					ucp += sizeof( tpmicro_data.two_wire_solenoid_location_on_off_command );
		
					ldh.dlen += piece_size;
		
					remaining_bytes -= piece_size;
					
					tpmicro_data.send_2w_solenoid_location_on_off_command = (false);
				}
			}
		
			// ----------
		
			if( tpmicro_data.two_wire_set_decoder_sn )
			{
				piece_size = sizeof( UNS_32 );
				
				if( remaining_bytes >= piece_size )
				{
					B_SET( fc.pieces, BIT_TTPM_two_wire_set_serial_number );
					
					memcpy( ucp, &tpmicro_data.sn_to_set, sizeof(UNS_32) );
		
					ucp += sizeof( UNS_32 );
		
					ldh.dlen += piece_size;
		
					remaining_bytes -= piece_size;
					
					tpmicro_data.two_wire_set_decoder_sn = (false);
				}
			}
		
		}  // end of adding to the message in the absence of ISP or executing code date and time request
		
		// ----------
	
		// 4/3/2012 rmd : Done adding content. NOW put the fc structure into place.
		memcpy( where_to_place_fc, &fc, sizeof(FOAL_COMMANDS) );
	
		// ----------
	
		// 11/6/2012 rmd : Calculate the CRC. Which is returned in BIG ENDIAN format. Which is what
		// we need (see note in crc calc function). Then copy to ucp.
		CRC_BASE_TYPE	lcrc;
		
		lcrc = CRC_calculate_32bit_big_endian( ldh.dptr, ldh.dlen );
		
		memcpy( ucp, &lcrc, sizeof(CRC_BASE_TYPE) );
		
		// 11/6/2012 rmd : In case needed keep ucp up to snuff.
		ucp += sizeof(CRC_BASE_TYPE);
		
		ldh.dlen += 4;  // Hey the message block to send just got 4 bigger.
	
		// ----------
		
		// 3/25/2013 rmd : And tack on the PRE and POST ambles.
		Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy( UPORT_TP, ldh, NULL ); // NULL signifies not from TPL_OUT and no status timer involved
	
		// -------------------------------------
	
		// 4/5/2012 rmd : Done with the allocated message space. Free it.
		mem_free( ldh.dptr );
	
		// -------------------------------------
	
		// 4/9/2012 rmd : To allow us to change the msg frequency from the UI we call
		// xTimerChangePeriod (and this function also starts the timer) with a variable as the time.
		// Variable since deleted.
		//
		// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
		// when posting is safe. Anything on the timer queue is quickly processed and therefore the
		// queue is effectively always empty.
		xTimerChangePeriod( tpmicro_comm.timer_message_rate, MS_to_TICKS( ms_between_messages ), portMAX_DELAY );

	}  // of building a new msg for the tpmicro
	
}

/* ---------------------------------------------------------- */
extern void TPMICRO_COMM_kick_out_the_next_tpmicro_msg( COMM_MNGR_TASK_QUEUE_STRUCT *pcmqs )
{
	// 2/10/2014 rmd : This function is executed within the context of the comm_mngr task. At a
	// periodic rate tied to the tpmicro msg timer. The rate varies depending if in ISP or not.
	if( tpmicro_comm.in_ISP )
	{
		TPMICRO_COMM_kick_out_the_next_isp_tpmicro_msg( pcmqs );
	}
	else
	{
		kick_out_the_next_normal_tpmicro_msg( pcmqs );
	}
}

/* ---------------------------------------------------------- */
extern void TPMICRO_COMM_create_timer_and_start_messaging( void )
{
	// 8/8/2012 rmd : The whole tpmicro_comm structure is filled with zeros at this point per
	// the C startup code. Therefore we do not zero out the counters within the tpmicro_comm
	// structure.

	// ----------

	// 8/8/2012 rmd : Create the MESSAGE RATE timer.
	tpmicro_comm.timer_message_rate = xTimerCreate( (const signed char*)"TP msg timer", MS_to_TICKS( TPMICRO_NORMAL_MS_BETWEEN_MSGS ), FALSE, NULL, msg_rate_timer_callback );
	
	// 4/4/2012 rmd : Now start the timer!
	//
	// 3/14/2014 rmd : Being that the timer task is a high priority task using portMAX_DELAY
	// when posting is safe. Anything on the timer queue is quickly processed and therefore the
	// queue is effectively always empty.
	xTimerStart( tpmicro_comm.timer_message_rate, portMAX_DELAY );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION The CRC of the packet has been checked. The UPORT the packet came in has
    been verified. The message class has been checked. And the TO address has been checked.
    In this function we make a copy of the incoming packet. The memory responsiblity for the
    newly allocated memory is transferred via the queue to the TPMicro Task.

	@CALLER_MUTEX_REQUIREMENTS (none)

    @MEMORY_RESPONSIBILITES (none) This function must not release the memory associated with
    the pdh argument. Additionally the responsibility for the memory newly allocated within
    this function is passed on via the queue message.

    @EXECUTED Executed within the context of the RING_BUFFER_ANALYSIS_TASK. When a incoming
    packet has been found and determined to be from the TP_MICRO.

	@RETURN (none)

	@ORIGINAL 2012.04.04 rmd

	@REVISIONS
*/
void TPMICRO_make_a_copy_and_queue_incoming_packet( DATA_HANDLE pdh )
{
	DATA_HANDLE		packet_copy;

	COMM_MNGR_TASK_QUEUE_STRUCT	cmeq;

	// 4/4/2012 rmd : Make a copy. The responsiblity to free is passed to the TPMicro task via
	// the queue event.
	packet_copy.dptr = mem_malloc( pdh.dlen );

	packet_copy.dlen = pdh.dlen;

	memcpy( packet_copy.dptr, pdh.dptr, pdh.dlen );

	cmeq.event = COMM_MNGR_EVENT_tpmicro_here_is_an_incoming_packet_based_msg;

	cmeq.dh = packet_copy;

	COMM_MNGR_post_event_with_details( &cmeq );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	OM_TEST_SIZE	(1000)

/* ---------------------------------------------------------- */
extern void TPMICRO_COMM_kick_out_test_message( void )
{
	COMMUNICATION_MESSAGE_LIST_ITEM		cmli;
	
	DATA_HANDLE							om;
	
	om.dlen = OM_TEST_SIZE;
	
	om.dptr = mem_malloc( om.dlen );
	
	// 3/12/2013 rmd : We don't much care about the content!
	
	// ----------
	
	cmli.in_port = UPORT_TP;
	
	cmli.from_to.from[ 0 ] = 0x00;
	cmli.from_to.from[ 1 ] = 0x11;
	cmli.from_to.from[ 2 ] = 0x22;
	
	// ----------

	SendCommandResponseAndFree( om, &cmli, MSG_CLASS_CS3000_TOKEN_RESP );
	
	// ----------

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

