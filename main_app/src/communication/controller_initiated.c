/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"controller_initiated.h"

#include	"gpio_setup.h"

#include	"configuration_controller.h"

#include	"serial.h"

#include	"alerts.h"

#include	"change.h"

#include	"crc.h"

#include	"comm_mngr.h"

#include	"cent_comm.h"

#include	"serport_drvr.h"

#include	"app_startup.h"

#include	"configuration_network.h"

#include	"epson_rx_8025sa.h"

#include	"r_alerts.h"

#include	"flowsense.h"

#include	"df_storage_mngr.h"

#include	"alert_parsing.h"

#include	"cs_mem.h"

#include	"tpmicro_comm.h"

#include	"poc_report_data.h"

#include	"budget_report_data.h"

#include	"lights_report_data.h"

#include	"system_report_data.h"

#include	"weather_control.h"

#include	"bithacks.h"

#include	"foal_irri.h"

#include	"screen_utils.h"

#include	"moisture_sensor_recorder.h"

#include	"lpc3250_chip.h"

#include	"code_distribution_task.h"

#include	"device_LR_RAVEON.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#define		CI_RESULT_INFRASTRUCTURE_IN_USE		(0x0401)

#define		CI_RESULT_NO_DATA_TO_SEND			(0x0402)

#define		CI_RESULT_NO_MEMORY					(0x0403)

#define		CI_RESULT_ERROR_BUILDING_MESSAGE	(0x0404)

#define		CI_RESULT_MESSAGE_SUCCESSFUL		(0x0405)


// 8/14/2013 rmd : The controller initiated control structure.
CONTROLLER_INITIATED_CONTROL_STRUCT     cics;

// ----------

#if INCLUDE_LANTRONIX_MSG_TRANSACTION_TIME_CODE

	// 10/20/2015 rmd : LANTRONIX TEST VARIABLE to measure the delta between message sent and
	// ACK rcvd.
	DATE_TIME	last_message_sent_dt_stamp;
	
#endif

// ----------

// 10/7/2013 rmd : We are allowing 3 minutes for the message to make it to the comm_server,
// be processed, and then the return ACK to make it to us. The reason so long is we may have
// to allow for token delays if the internet connected device is at another box. As well as
// comm_server time to write to the databases.
//
// 8/11/2014 rmd : UPDATE - well at this point in time we do not support controller
// initiated message origination at a box other than the box witht he central device so the
// token delay consideration isn't valid. So I'm decreasing to 2 minutes. This is a
// universal time used by all messages waiting for a response. The controller initiated task
// will wait this long for either the comm server to respond or the response timer fires
// indicating a problem.
//
// 4/24/2015 rmd : UPDATE AGAIN - 2 minutes seems excessive. And when there is an error us
// poor engineering folks had to wait around to watch the behavior. When the connection is
// there and the commserver is working I have never seen it take more than 15 seconds for a
// response. Soooo I'll make this 1 minute now.
//
// 6/11/2015 rmd : Moved to 4 minutes to try and track down 'no response' errors we keep
// seeing. Some appear associated with data arriving at the commserver slowly. And others
// with a big slowdown in the advantage database engine.
//
// 6/24/2015 rmd : Seems we got to the bottom of the slow responses from the commserver. A
// database backup issue that was running. Moving back to our 'normal' 60 seconds
// timeout.
//
// 10/16/2015 rmd : WELL once again we are having issues. And it is probably the very same
// issue we had in June! Seems we may have now learned it is related to the AT&T tunnel. For
// diagnostic purposes we are moving this time out to 5 minutes for now. To help get to the
// bottom of the continuing 'no response' issue. Added the LANTRONIX_CODE ifdef.
//
// 2/1/2017 rmd : Well we have learned an awful lot recently. Associated with the rollout of
// the new commserver we learned that the reason for the lengthy time before response to the
// controller delivering data was associated with the DATABASE. Wissam still believes there
// is more to the story. Such as CPU loading and our network congestion (not the cell
// network). He believe this considering the 12:12AM report in time of so many controllers
// at once. We should stagger this timing of such based upon controller serial number. All
// in all I have decided to make this application repsonse time 5 minutes for all messages
// for all controllers. Even with the improvements Wissam made (using INSERT statements
// instead of MERGE statements) we still see 90+ seconds transaction times.
#define		CONTROLLER_INITIATED_DATABASE_RECORD_INSERT_TIME_MS		(5*60*1000)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void restart_hub_packet_activity_timer( void )
{
	// 5/10/2017 rmd : We allow up to 30 minutes with not a single packet from the commserver to
	// arrive on port A of the hub. If that time elapses we start the connection process.
	//
	// 6/6/2017 rmd : Significant change needed - what if a city has a few hubs and a couple
	// hundred 2000 controllers. It might be possible a job time (5AM) that the commserver is
	// busy with controllers on a hub or two for quite some time, and just not get around to
	// polling one of the hubs within the city. Also remember, this is a cover your ass strange
	// situation has occurred where the device indicates it is connected but perhaps it is not.
	// So make this timer long, say 4 hours.
	//
	// 6/7/2017 rmd : Another reason we might want this timeout very long, is the hub code
	// transmission effort, in a city with multiple hubs, during which the commserver sees one
	// hub busy distributing code, and the commserver just keeps pecking at that hub waiting for
	// him to finish, and the commserver doesn't poll any other hubs while one hub indicates it
	// is busy.
	xTimerChangePeriod( cics.hub_packet_activity_timer, MS_to_TICKS(4*60*60*1000), portMAX_DELAY );
}
	
/* ---------------------------------------------------------- */
static void hub_packet_activity_time_callback( xTimerHandle pxTimer )
{
	// 5/10/2017 rmd : Remember this is executed within the context of the high priority timer
	// task. Post an event to the CI task to perform the work. But only for the hub itself
	// should this activity be taking place.
	if( CONFIG_this_controller_is_a_configured_hub() )
	{
		Alert_Message( "Hub Packet Activity timer EXPIRED" );
		
		CONTROLLER_INITIATED_post_event( CI_EVENT_start_the_connection_process );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void start_the_polling_timer( UNS_32 pseconds )
{
	// 3/1/2017 rmd : Following a connection, controllers that are not hub-based, generate their
	// own 'build and send' event. Once every 5 seconds. This IS how messages are sent.
	xTimerChangePeriod( cics.queued_msgs_polling_timer, MS_to_TICKS(pseconds*1000), portMAX_DELAY );
}
	
/* ---------------------------------------------------------- */
static void ci_queued_msgs_polling_timer_callback( xTimerHandle pxTimer )
{
	// 10/21/2013 rmd : Remember this is executed within the context of the high priority timer
	// task. Post an event to the CI task to perform the work.
	CONTROLLER_INITIATED_post_event( CI_EVENT_build_and_send_the_next_queued_msg );
	
	// 2/15/2017 rmd : Upon completion of the connection process the timer is started causing
	// all controllers eligible to do so to build and send the first message. In the case of
	// non-hub based controllers we keep the polling timer going. This is the mechanism that
	// replaces the 'poll' message that comes from the commserver for the case of hub based
	// controllers.
	if( !CONFIG_this_controller_is_a_configured_hub() && !CONFIG_this_controller_is_behind_a_hub() )
	{
		// 2/23/2017 rmd : If we are waiting to begin the connection process or in the middle of
		// connecting do not restart the timer. Upon completion of the connection process we will
		// queue up some messages and start the timer.
		if( cics.mode == CI_MODE_ready )
		{
			// 10/11/2017 rmd : The normal rate we are using is to check once every 5 seconds if there
			// is something queued to send. I just picked this as a reasonable value, no particular
			// reason.
			start_the_polling_timer( 5 );
		}
	}
}
	
/* ---------------------------------------------------------- */
extern void CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities( void )
{
	// 2/7/2017 rmd : Clear the connection failure count. So that a connection retry is tried
	// again without undo delay.
	cics.connection_process_failures = 0;

	// ----------
	
	// 2/23/2017 rmd : Set the CI mode so that it may build and send messages.
	cics.mode = CI_MODE_ready;

	cics.state = CI_STATE_idle;
	
	// ----------

	// 5/25/2017 rmd : And indicate we have not yet received the hub list associated with this
	// connection. Remember this list is dynamic, and the commserver intentionally disconnects
	// us when a controller hub association is changed. It's arrival is required prior to
	// beginning a hub code distribution.
	cdcs.hub_code__list_received = (false);
	
	// 6/1/2017 rmd : NOTE - do not clear the cics.hub_code__comm_mngr_ready variable here. That
	// variable is cleared upon program boot, and set by the commngr. It is not related to the
	// connection process, and therefore should not be cleared here. If you did, you possibly
	// would never start the hub code distribution process.

	// ----------

	// 3/23/2017 rmd : Update the gui var which manages the 'connected' symbol shown in the
	// right hand side of the top screen bar.
	GuiVar_LiveScreensCommCentralConnected = (true);
	
	// ----------
	
	if( !CONFIG_this_controller_is_behind_a_hub() )
	{
		// 3/22/2017 rmd : So in this section falls hubs and all regular non-hub based controllers.

		// 10/11/2017 rmd : When starting the commserver service, many controllers can connect at
		// roughly the same time, all sending alerts at the same time. So to distribute the database
		// loading in that scenario, spread out when alerts are sent following a connection using
		// the serial number based time calculation. This spreads out the sending of the alerts over
		// a 4 minute window. And force the timer to be loaded with the (true) parameter.
		CONTROLLER_INITIATED_alerts_timer_upkeep( CONTROLLER_INITIATED_ms_after_connection_to_send_alerts( config_c.serial_number ), (true) );
		
		// ----------
		
		// 10/11/2017 rmd : If this is a HUB, the important thing to do is to send the commserver an
		// END OF POLL message. This is the agreement Jay and I have. This will identify the socket
		// for the commserver, the commserver then knows this is a hub by looking at this NID
		// settings in the database, and will subsequent to this END OF POLL, send the hub list to
		// the hub. Sending the hub list is a REQUIREMENT for the hub to function.
		if( CONFIG_this_controller_is_a_configured_hub() )
		{
			// 5/10/2017 rmd : Okay we've connected, and we are a hub, so should start the packet
			// activity timer here. From this point forward each packet that arrives will ALSO restart
			// this timer. It is important to start it here in case we never receive even a SINGLE
			// packet from the commserver, so at least the timer is running. On a reboot this would be
			// the first time the timer is started.
			restart_hub_packet_activity_timer();
			
			// ----------
			
			// 5/8/2017 rmd : And then the important step we do, send ONLY the no more messages message
			// upon the initial HUB connection, and then wait for a poll or pseudo poll message. What
			// should happen is that we see the hub list as our first message, which would then allow
			// any other queued messages to be sent. THIS STEP of only sending the no more messages msg
			// is how we SYNCHRONIZE a new hub into the company polling that may be taking place.
			// Remember a hub connecting shouldn't just take control of the polling - which would happen
			// if we let it rattle off a bunch of messages. The commserver very carefully handles just
			// this first 'end of poll' message. And then waits until legimately this hub can be polled,
			// taking into account the LR condition within the city. Posted to the FRONT so this is the
			// FIRST and only message to go following a connection.
			//
			// 9/21/2017 rmd : Not always true that the no more messages is the first and only message
			// to go. If the registration needs to be sent, it will go first, followed by the no more
			// messages message. That doesn't seem to bother anything though.
			CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_no_more_messages_msg, NULL, CI_IF_PRESENT_REMOVE_AND_REPOST, CI_POST_TO_FRONT );
		}

		// ----------
		
		// 3/1/2017 rmd : Then for both HUBS and and non-hub based controllers start the polling
		// timer. For the HUB in 3 seconds the socket identification message (END OF POLL), that we
		// just queued on the msg queue, will we sent. We want 3 seconds so that it is timely, and I
		// know it goes ahead of alerts (it will even if the alerts timer fires, cause the END OF
		// POLL is queued to the front of the message queue). For the case of non-hub based this is
		// a critical step to start the polling timer, so that self generated polls will be
		// generated from this point forth.
		start_the_polling_timer( 3 );
	}
}
					
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void ci_start_the_waiting_to_start_the_connection_process_timer_seconds( UNS_32 pseconds )
{
	// 3/28/2017 rmd : Start the timer so we try the start teh connection process again in the
	// specified amount of time.
	xTimerChangePeriod( cics.waiting_to_start_the_connection_process_timer, MS_to_TICKS(pseconds*1000), portMAX_DELAY );
}

/* ---------------------------------------------------------- */
static void waiting_to_start_the_connection_process_timer_callback( xTimerHandle pxTimer )
{
	// 10/21/2013 rmd : Remember this is executed within the context of the high priority timer
	// task. DO NOT run through some long winded processing here i.e. disconnect sequence. Post
	// an event to the CI task to perform the work.
	CONTROLLER_INITIATED_post_event( CI_EVENT_start_the_connection_process );
}

/* ---------------------------------------------------------- */
static void ci_start_the_connection_process( void )
{
	// 6/16/2017 rmd : By definition the connection process is ONLY associated with
	// communications to the commserver. Therefore suppress for all other configurations.
	if( CONFIG_this_controller_originates_commserver_messages() )
	{
		// 10/31/2016 rmd : Make sure we are NOT waiting for an actual msg response from the
		// commserver. And make sure we are not already in the middle of the connection process. If
		// we are waiting for a response let that play out. If we show as not connected or the
		// reponse fails to show we will start the connection process then. Also if we are already
		// in the midst of the connection process let it play out too.
		//
		// 3/28/2017 rmd : Another consideration is to ensure we are not in the device exchange MODE
		// here. If we were we want that to gracefully exit. At which time an attempt to start the
		// connection process is made (when leaving the screen).
		if( ((cics.mode == CI_MODE_ready) && (cics.state == CI_STATE_idle)) || (cics.mode == CI_MODE_waiting_to_connect) )
		{
			if( port_device_table[ config_c.port_A_device_index ].on_port_A_enables_controller_to_make_CI_messages )
			{
				if( (port_device_table[ config_c.port_A_device_index ].__initialize_the_connection_process != NULL) && (port_device_table[ config_c.port_A_device_index ].__connection_processing != NULL) )
				{
					Alert_ci_starting_connection_process();

					// ----------
					
					// 3/29/2017 rmd : Let's assume the unit is not connected and/or that the initialization
					// process indeed will disconnect the unit. So indicate so on the screen. As the connection
					// process does it thing, it will reach the point where is hopefully connects, and this GUI
					// flag will be set true then.
					GuiVar_LiveScreensCommCentralConnected = (false);
					
					// ----------
					
					cics.mode = CI_MODE_connecting;
					
					// 10/26/2016 rmd : By definition the device is on Port A. Setup the process state machine.
					port_device_table[ config_c.port_A_device_index ].__initialize_the_connection_process( UPORT_A );
				}
				else
				{
					Alert_Message( "Port A: NULL device connection function(s)" );
	
					// 1/27/2017 rmd : I think this is an INVALID configuration. And we should not keep testing
					// to connect. So do nothing. Do not restart the timer. Effectively shutdown the CI task by
					// setting the mode to waiting to connect.
					cics.mode = CI_MODE_waiting_to_connect;
				}
			}
			else
			{
				// 1/27/2017 rmd : The device on Port A is not allowed to be used for commserver messages.
				// Do not alert about such. And do not retry an attempt to connect. The device cannot be
				// changed until power down (is not supposed to be - ELECTRICALLY UNSAFE TO DO SO ) . So no
				// sense retrying to connect. Make sure mode is set to 'trying to connect', effectively
				// shutting down the CI task.
				cics.mode = CI_MODE_waiting_to_connect;
			}
			
		}  // already connecting or waiting for a commserver response so nothing to do

	}
	else
	{
		// 1/27/2017 rmd : I don't think we need to say anything alert wise. Lots of controllers
		// aren't masters. If it had a device on port A but the letter was set to a 'B' we would not
		// be a master. But do want to keep trying to connect to cover the case when the user sets
		// the letter to an 'A'. So I think once a minute we'll test to see if appropriate to
		// connect and ACTIVATE this controller initiated task.
		//
		// 3/28/2017 rmd : Do make sure the mode is appropriate for a non-master controller. Think
		// if we were a master an mode was READY and now we're the B controller. Now we should be
		// waiting to connect...forever.
		cics.mode = CI_MODE_waiting_to_connect;
		
		ci_start_the_waiting_to_start_the_connection_process_timer_seconds( 60 );
	}
}

/* ---------------------------------------------------------- */
static void connection_process_timer_callback( xTimerHandle pxTimer )
{
	// 10/21/2013 rmd : Remember this is executed within the context of the high priority timer
	// task. DO NOT run through some long winded processing here i.e. disconnect sequence. Post
	// an event to the CI task to perform the work.
	CONTROLLER_INITIATED_post_event( CI_EVENT_process_timer_fired );
}

/* ---------------------------------------------------------- */
static void process_during_connection_process_timer_fired( void )
{
	// 5/8/2015 rmd : Executed soley within the context of the CI task. From no where else.

	// 2/3/2017 rmd : We should be in the 'connecting' mode. And the function pointer should not
	// be NULL. If not try to start the connection process.
	if( (cics.mode == CI_MODE_connecting) && (port_device_table[ config_c.port_A_device_index ].__connection_processing != NULL) )
	{
		// 10/29/2016 rmd : We are in the connection process. And are waiting for either this timer
		// to fire or a string to arrive (which didn't). Like after a RESET we want to wait 30
		// seconds before tapping the unit with an 'AT' command.
		port_device_table[ config_c.port_A_device_index ].__connection_processing( CI_EVENT_process_timer_fired );
	}
	else
	{
		// 2/3/2017 rmd : This should NEVER happen. We test when we begin the connection sequence
		// that the function is not NULL. And we should in 'connecting' mode.
		//
		// 6/30/2017 rmd : Ahh, but it can happen. When we enter the device exchange mode and
		// clobber the controller initiated MODE. So don't alert if in device exchange mode.
		if( !in_device_exchange_hammer )
		{
			Alert_Message_va( "Connection Sequence ERROR mode=%u", cics.mode );

			// 2/3/2017 rmd : Try to start it. If not eligible won't. There are quite a few safeguards
			// in place so directly calling the start function with no checking here is safe to do.
			ci_start_the_connection_process();
		}
	}
}

/* ---------------------------------------------------------- */
static void process_during_connection_process_string_found( void )
{
	// 5/8/2015 rmd : Executed soley within the context of the CI task. From no where else.

	// 10/30/2016 rmd : Since we found the string STOP the timer so it doesn't fire too. If it
	// did we would have that event to process too. And get all confused.
	xTimerStop( cics.process_timer, portMAX_DELAY );
	
	// ----------
	
	// 2/3/2017 rmd : We should be in the 'connecting' mode. And the function pointer should not
	// be NULL. If not try to start the connection process.
	if( (cics.mode == CI_MODE_connecting) && (port_device_table[ config_c.port_A_device_index ].__connection_processing != NULL) )
	{
		// 10/29/2016 rmd : We are in the connection process. And are waiting for either a certain
		// string or a timer to timeout. Well the string arrived.
		port_device_table[ config_c.port_A_device_index ].__connection_processing( CI_EVENT_process_string_found );
	}
	else
	{
		// 2/3/2017 rmd : This should NEVER happen. We test when we begin the connection sequence
		// that the function is not NULL. And we should in 'connecting' mode.
		//
		// 6/30/2017 rmd : Ahh, but it can happen. When we enter the device exchange mode and
		// clobber the controller initiated MODE. So don't alert if in device exchange mode.
		if( !in_device_exchange_hammer )
		{
			Alert_Message( "Connection Sequence ERROR" );
	
			// 2/3/2017 rmd : Try to start it. If not eligible won't. There are quite a few safeguards
			// in place so directly calling the start function with no checking here is safe to do.
			ci_start_the_connection_process();
		}
	}
}

/* ---------------------------------------------------------- */
static void __clear_all_waiting_for_response_flags( const BOOL_32 pcomm_error_occurred, const BOOL_32 pstamp_divider_and_transaction_time )
{
	// 8/15/2014 rmd : A functional NOTE. This function MUST be called between CI messages. To
	// clear not only our cics 'waiting_for' flag. But ALSO the data specific PENDING flag must
	// be cleared. If we didn't clear the data specific flag here then that data would NEVER try
	// again. FURTHERMORE the reason those data specific flags are cleared here and NOT where
	// the ACK is received is exactly this reason. If the ACK is never received the flag will
	// not be cleared and the data won't try again!

	// ----------

	CONTROLLER_INITIATED_TASK_QUEUE_STRUCT	cieqs;

	// ----------
	
	#if INCLUDE_LANTRONIX_MSG_TRANSACTION_TIME_CODE

		//#warning *** compiling LANTRONIX MSG transaction time code *** 

		// 1/18/2017 rmd : You get a funny in that when we suppress the normal transaction alert
		// lines that depict a transaction is taking place (like we do for the mobile status
		// update), all we get are the msg transaction time lines. One for each mobile screen
		// update. Looks wierd in the alerts. And generally we are interested in the transaction
		// time for report data transactions since those occur a the same time of day and we are
		// investigating a commserver/database loading issue. So skip for mobile status update.
		if( !cics.waiting_for_mobile_status_response && pstamp_divider_and_transaction_time )
		{
			// 10/20/2015 rmd : LANTRONIX TEST.
			DATE_TIME	ldt;
			
			EPSON_obtain_latest_time_and_date( &ldt );
			
			if( ldt.T >= last_message_sent_dt_stamp.T )
			{
				Alert_msg_transaction_time( (ldt.T - last_message_sent_dt_stamp.T) ); 
			}
			else
			{
				Alert_Message( "msg spanned midnight" );
			}
		}
		
	#endif
	
	// ----------
	
	// 2/17/2017 rmd : Make the divider here after we've stamped the transaction time. But only
	// when asked to do so.
	//
	// 8/19/2016 rmd : Not for the mobile status command. We don't make any alerts for it! It
	// was quite a nuisance when we did.
	if( !cics.waiting_for_mobile_status_response && pstamp_divider_and_transaction_time )
	{
		// 8/22/2013 rmd : This next line is a divider for alerts readability. Eventually should be
		// commented out.
		Alert_divider_small();
	}

	// ----------

	if( cics.waiting_for_registration_response )
	{
		// 4/28/2015 rmd : We have a lock flag to prevent multiple registrations from being posted
		// to the CI message list. Once one has been posted this flag is set true. At the conclusion
		// of when the message is actually transmitted (or fails) we clear this flag. And that would
		// be here.
		cics.a_registration_message_is_on_the_list = (false);

		// 8/15/2014 rmd : Because registration data always sends all of itself there are no data
		// specific tracking flags to reset. Only the cics flag.
		cics.waiting_for_registration_response = (false);
	}

	// ----------

	// 10/9/2013 rmd : For the alerts if we were waiting for the alerts response then clear the
	// fact that the alerts pointers were in use. If that is what we were waiting for either the
	// ACK succesfully arrived or the response timer timed out. Either way that transaction is
	// over. If the response arrived the first byte to send pointer was bumped. If not it
	// wasn't. Again, either way we're free to try again.
	if( cics.waiting_for_alerts_response )
	{
		xSemaphoreTakeRecursive( alerts_pile_recursive_MUTEX, portMAX_DELAY );

		// 8/15/2014 rmd : Can ONLY clear the one that the session we are wrapping up is for.
		// Because we could have multiple set simultaneously if we were sending different alerts
		// piles at the same time. Meaning if different alerts piles message were queued up at the
		// same time on the ci msg list. Right now only doing engineering so only need to clear that
		// one. This will need extra support within the cics structure. See how it's done with flow
		// recording mananging the multiple systems.
		alerts_struct_engineering.pending_first_to_send_in_use = (false);

		xSemaphoreGiveRecursive( alerts_pile_recursive_MUTEX );

		cics.waiting_for_alerts_response = (false);
	}


	// ----------

	// 10/21/2013 rmd : Read the comments for alerts above. Same applies for flow_recording.
	if( cics.waiting_for_flow_recording_response )
	{
		xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

		// 8/15/2014 rmd : Clear only the flag for the system whose message we are processing.
		if( cics.current_msg_frcs_ptr != NULL )
		{
			if( cics.current_msg_frcs_ptr->pending_first_to_send_in_use )
			{
				cics.current_msg_frcs_ptr->pending_first_to_send_in_use = (false);
			}
			else
			{
				Alert_Message( "flow: pending_first not in use!" );
			}
		}
		else
		{
			Alert_Message( "CICS: current_msg ptr is NULL!" );
		}

		xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

		cics.waiting_for_flow_recording_response = (false);
	}


	// ----------

	// 10/21/2013 rmd : Read the comments for alerts above. Same applies here.
	if( cics.waiting_for_moisture_sensor_recording_response )
	{
		xSemaphoreTakeRecursive( moisture_sensor_recorder_recursive_MUTEX, portMAX_DELAY );

		if( msrcs.pending_first_to_send_in_use )
		{
			msrcs.pending_first_to_send_in_use = (false);
		}
		else
		{
			Alert_Message( "mois: pending_first not in use!" );
		}

		xSemaphoreGiveRecursive( moisture_sensor_recorder_recursive_MUTEX );

		cics.waiting_for_moisture_sensor_recording_response = (false);
	}


	// ----------

	// 8/15/2014 rmd : Because updates sends no data there are no data specific tracking flags
	// to reset. Only the cics response flag.
	cics.waiting_for_check_for_updates_response = (false);

	// ----------

	if( cics.waiting_for_station_history_response )
	{
		// 8/15/2014 rmd : Always clear these types of PENDING variables here in this function cause
		// if we didn't get the response we want to be free to try again. If we didn't clear these
		// here we wouldn't try again.
		station_history_completed.rdfb.pending_first_to_send_in_use = (false);

		cics.waiting_for_station_history_response = (false);
	}

	// ----------

	if( cics.waiting_for_station_report_data_response )
	{
		// 8/15/2014 rmd : Always clear these types of PENDING variables here in this function cause
		// if we didn't get the response we want to be free to try again. If we didn't clear these
		// here we wouldn't try again.
		station_report_data_completed.rdfb.pending_first_to_send_in_use = (false);

		cics.waiting_for_station_report_data_response = (false);
	}

	// ----------

	if( cics.waiting_for_poc_report_data_response )
	{
		// 8/15/2014 rmd : Always clear these types of PENDING variables here in this function cause
		// if we didn't get the response we want to be free to try again. If we didn't clear these
		// here we wouldn't try again.
		poc_report_data_completed.rdfb.pending_first_to_send_in_use = (false);

		cics.waiting_for_poc_report_data_response = (false);
	}

	// ----------

	if( cics.waiting_for_budget_report_data_response )
	{
		// 02/22/2016 skc : Always clear these types of PENDING variables here in this function cause
		// if we didn't get the response we want to be free to try again. If we didn't clear these
		// here we wouldn't try again.
		budget_report_data_completed.rdfb.pending_first_to_send_in_use = (false);

		cics.waiting_for_budget_report_data_response = (false);
	}

	// ----------

	if( cics.waiting_for_lights_report_data_response )
	{
		// 8/15/2014 rmd : Always clear these types of PENDING variables here in this function cause
		// if we didn't get the response we want to be free to try again. If we didn't clear these
		// here we wouldn't try again.
		lights_report_data_completed.rdfb.pending_first_to_send_in_use = (false);

		cics.waiting_for_lights_report_data_response = (false);
	}

	// ----------

	if( cics.waiting_for_system_report_data_response )
	{
		// 8/15/2014 rmd : Always clear these types of PENDING variables here in this function cause
		// if we didn't get the response we want to be free to try again. If we didn't clear these
		// here we wouldn't try again.
		system_report_data_completed.rdfb.pending_first_to_send_in_use = (false);

		cics.waiting_for_system_report_data_response = (false);
	}

	// ----------

	if( cics.waiting_for_et_rain_tables_response )
	{
		// 8/15/2014 rmd : Always clear these types of PENDING variables here in this function cause
		// if we didn't get the response we want to be free to try again. If we didn't clear these
		// here we wouldn't try again.
		weather_tables.pending_records_to_send_in_use = (false);
		
		cics.waiting_for_et_rain_tables_response = (false);
	}

	// ----------

	if( cics.waiting_for_weather_data_receipt_response )
	{
		cics.waiting_for_weather_data_receipt_response = (false);

		// 6/16/2015 rmd : If there was an error try again. This is important to get through. We
		// keep trying until 8:10pm.
		if( pcomm_error_occurred )
		{
			CONTROLLER_INITIATED_post_event( CI_EVENT_start_send_weather_data_timer );
		}
	}

	// ----------

	if( cics.waiting_for_rain_indication_response )
	{
		cics.waiting_for_rain_indication_response = (false);

		// 6/16/2015 rmd : If there was an error try again. This is important to get through. We
		// keep trying until the next time the table rolls.
		if( pcomm_error_occurred )
		{
			CONTROLLER_INITIATED_post_event( CI_EVENT_start_send_rain_indication_timer );
		}
	}

	// ----------

	if( cics.waiting_for_mobile_status_response )
	{
		cics.waiting_for_mobile_status_response = (false);

		if( pcomm_error_occurred )
		{
			// 6/24/2015 rmd : Start the timer setting the time so that we send the status immediately.
			cieqs.event = CI_EVENT_restart_mobile_status_timer;
		
			cieqs.how_long_ms = CI_SEND_STATUS_MS_after_attempt_error;
			
			CONTROLLER_INITIATED_post_event_with_details( &cieqs );
		}
	}

	// ----------

	// 5/12/2015 rmd : Prior to sending pdata to the commserver we check if the controller
	// firmware is up to date (compared to the files the commserver has). If so we go ahead and
	// send our pdata changes to the commserver. If we DO NOT get the response to this firmware
	// version inquiry then it is IMPORTANT to RESTART the pdata timer to start the process over
	// trying to send our pdata to the controller. If we didn't restart the timer here then, for
	// one, the comm server cannot send any changes it has to us (as we will ignore the IHSFY
	// packet from the commserver). Our pending pdata changes would eventually get sent but that
	// would be hinged on the next time the timer was started.
	if( cics.waiting_for_firmware_version_check_response )
	{
		cics.waiting_for_firmware_version_check_response = (false);

		if( pcomm_error_occurred )
		{
			xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGES_NOT_SENT_MS ), portMAX_DELAY );
		}
	}

	// ----------

	// 5/12/2015 rmd : When sending PDATA CHANGES TO THE COMMSERVER.
	if( cics.waiting_for_pdata_response )
	{
		if( pcomm_error_occurred )
		{
			// 11/20/2015 rmd : For the case of the RESPONSE TIMEOUT (meaning no response rcvd) we have
			// some housekeeping to do for the pdata change tracking bits. If a successful acknoledge is
			// received then the change bits are updated then as part of that processing.
			//
			// 4/27/2015 ajv : If a communication error occurred (while sending Program Data) then we
			// copy the bits of variables that were sent back into the original bit fields and restart
			// the timer to ensure the changes are again sent.
			PDATA_update_pending_change_bits( (true) );
		}

		// 4/30/2015 rmd : And clear the lock flag indicating there was a pdata message for the
		// commserver on the CI list.
		cics.a_pdata_message_is_on_the_list = (false);

		cics.waiting_for_pdata_response = (false);
	}

	// ----------

	// 5/12/2015 rmd : In response to an IHSFY packet from the commserver, if we do not have any
	// pdata changes of our own to send to the commserver, we ask the commserver to send its
	// pdata changes to us. There is an ACK to that I think. But we actually completely ignore
	// that ACK. We benignly expect the commserver to begin an incoming pdata message to us.
	cics.waiting_for_asked_commserver_if_there_is_pdata_for_us_response = (false);

	// ----------

	// 2/24/2017 rmd : Because the 'no more messages' message sends no data there are no data
	// specific tracking flags to reset. Only the cics response flag.
	cics.waiting_for_the_no_more_messages_msg_response = (false);

	// 2/24/2017 rmd : Because the 'hub is busy' message sends no data there are no data
	// specific tracking flags to reset. Only the cics response flag.
	cics.waiting_for_hub_is_busy_msg_response = (false);
}

/* ---------------------------------------------------------- */
static void nm_build_init_packet_and_send_it( UNS_8* pdata_ptr, const UNS_32 pdata_len, const UNS_32 pmessage_id, const UNS_32 pport )
{
	TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER	tcsuah;

	TO_COMMSERVER_MESSAGE_HEADER        tcsmh;

	MSG_INITIALIZATION_STRUCT   init_struct;

	UNS_32					maximum_response_size;
	
	DATA_HANDLE				ldh;

	UNS_8					*ucp;

	UNS_8					*start_of_crc;

	UNS_32					bytes_to_crc, length_reported_to_commserver;
	
	CRC_BASE_TYPE			lcrc;
	
	// ----------

	//  					4 + 2 + 14 + 12 + 4 + 4 = 40
	maximum_response_size =	sizeof( AMBLE_TYPE ) +
							sizeof( TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER ) +
							sizeof( TO_COMMSERVER_MESSAGE_HEADER ) +
							sizeof( MSG_INITIALIZATION_STRUCT ) +
							sizeof( CRC_BASE_TYPE ) +
							sizeof( AMBLE_TYPE );

	if( mem_obtain_a_block_if_available( maximum_response_size, (void**)&(ldh.dptr) ) )
	{
		ucp = ldh.dptr;
	
		ldh.dlen = maximum_response_size;
	
		// 3/20/2017 rmd : The amount to CRC does not include the 2 ambles nor the CRC itself. And
		// leave out the hub portion. Will add in if included.
		bytes_to_crc = sizeof(TO_COMMSERVER_MESSAGE_HEADER) + sizeof( MSG_INITIALIZATION_STRUCT );
	
		// 3/20/2017 rmd : And the length reported to the commserver in the 'to commserver message
		// header' includes only the 'to commserver message header' and 'msg_initialization_struct'
		// sizes. This value never includes the ambles nor the crc. And also we know the hub routing
		// structure is stripped from the message before sending to the commserver.
		length_reported_to_commserver  = sizeof(TO_COMMSERVER_MESSAGE_HEADER) + sizeof( MSG_INITIALIZATION_STRUCT );
		
		// ----------

		*ucp++ = preamble._1;
		*ucp++ = preamble._2;
		*ucp++ = preamble._3;
		*ucp++ = preamble._4;

		// ----------

		start_of_crc = ucp;

		// ----------

		// 3/16/2017 rmd : So if this controller is a hub-based unit include the 2 extra bytes to
		// enable this packet to be routed upon receipt by the hub.
		if( CONFIG_this_controller_is_behind_a_hub() )
		{
			// 3/16/2017 rmd : And note this part of the packet will get stripped off prior to passing
			// on to the commserver. The commserver is not expecting to see this portion.

			memset( &tcsuah, 0x00, sizeof(TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER) );

			tcsuah.PID.__routing_class = MSG_CLASS_2000e_CS3000_FLAG;

			tcsuah.cs3000_msg_class = MSG_CLASS_CS3000_TO_COMMSERVER_VIA_HUB;

			memcpy( ucp, &tcsuah, sizeof(TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER) );
	
			ucp += sizeof(TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER);
			
			// 3/20/2017 rmd : And update the bytes to crc. Since this is included in the message.
			bytes_to_crc += sizeof(TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER);
		}
		else
		{
			// 3/16/2017 rmd : Update the length to remove the hub-based portion that is not used.
			ldh.dlen -= sizeof( TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER );
		}
		
		// ----------

		// 12/4/2013 rmd : Set the network ID. Should have already been checked to
		// be non-zero prior to getting this far into sending the data.
		tcsmh.from_network_id = NETWORK_CONFIG_get_network_id();

		// 8/23/2013 rmd : Build up part of the header.
		tcsmh.from_serial_number = config_c.serial_number;

		// 3/16/2017 rmd : This length only includes the content that goes to the commserver.
		tcsmh.length = length_reported_to_commserver;

		tcsmh.mid = (UNS_16)pmessage_id;

		memcpy( ucp, &tcsmh, sizeof(TO_COMMSERVER_MESSAGE_HEADER) );

		ucp += sizeof(TO_COMMSERVER_MESSAGE_HEADER);

		// ----------

		init_struct.expected_size = pdata_len;

		init_struct.expected_crc = CRC_calculate_32bit_big_endian( pdata_ptr, pdata_len );

		init_struct.expected_packets = (pdata_len / DESIRED_PACKET_PAYLOAD);

		if( (pdata_len % DESIRED_PACKET_PAYLOAD) > 0 )
		{
			init_struct.expected_packets += 1;
		}

		init_struct.mid = (UNS_16)pmessage_id;

		// ----------

		// 8/19/2016 rmd : SUPPRESS the outbound message size alert for the mobile screen
		// update. Its a repetitive nuisance! But DO ADD to its daily accumulator.
		if( pmessage_id == MID_TO_COMMSERVER_status_screen_init_packet )
		{
			SerDrvrVars_s[ pport ].stats.mobile_status_updates_bytes += (ldh.dlen + init_struct.expected_size);
		}
		else
		{
			Alert_outbound_message_size( init_struct.mid, init_struct.expected_size, init_struct.expected_packets );
		}

		// ----------

		memcpy( ucp, &init_struct, sizeof(MSG_INITIALIZATION_STRUCT) );

		ucp += sizeof(MSG_INITIALIZATION_STRUCT);

		// ----------

		// 3/16/2017 rmd : Calulate the CRC for the full packet. Including the LR HUB routing header
		// if present. But not including the two ambles and the crc itself.
		lcrc = CRC_calculate_32bit_big_endian( start_of_crc, bytes_to_crc );

		memcpy( ucp, &lcrc, sizeof(CRC_BASE_TYPE) );

		ucp += sizeof(CRC_BASE_TYPE);

		// ----------

		*ucp++ = postamble._1;
		*ucp++ = postamble._2;
		*ucp++ = postamble._3;
		*ucp   = postamble._4;

		// ----------

		// 10/27/2016 rmd : Send packet to the serial driver for transmission. The serial driver
		// will make a final test for CTS and a connection before sending the data to the device.
		AddCopyOfBlockToXmitList( pport, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_PACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );

		// 8/4/2014 ajv : Free the memory we allocated earlier. The serial driver already took a
		// copy of the data, so there's no reason for us to hold on to it any longer.
		mem_free( ldh.dptr );
	}
	else
	{
		Alert_Message( "No Memory to send init packet" );
	}
}

/* ---------------------------------------------------------- */
static void nm_build_data_packets_and_send_them( UNS_8* pdata_ptr, const UNS_32 pdata_len, const UNS_32 pmessage_id, const UNS_32 pport )
{
	TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER	tcsuah;

	TO_COMMSERVER_MESSAGE_HEADER    tcsmh;

	MSG_DATA_PACKET_STRUCT  data_struct;

	UNS_32					maximum_response_size;
	
	DATA_HANDLE				ldh;

	UNS_8					*start_of_crc;

	UNS_32					bytes_to_crc, length_reported_to_commserver;
	
	CRC_BASE_TYPE			lcrc;
	
	UNS_32  total_packets;

	UNS_32  amount_to_send;

	UNS_32  i;

	UNS_8   *ucp, *tcsmh_ptr;

	// ----------

	//  					4 + 2 + 14 + 4 + 2048 + 4 + 4 = 2080
	maximum_response_size =	sizeof( AMBLE_TYPE ) +
							sizeof( TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER ) +
							sizeof( TO_COMMSERVER_MESSAGE_HEADER ) +
							sizeof( MSG_DATA_PACKET_STRUCT ) +
							DESIRED_PACKET_PAYLOAD +
							sizeof( CRC_BASE_TYPE ) +
							sizeof( AMBLE_TYPE );

	if( mem_obtain_a_block_if_available( maximum_response_size, (void**)&(ldh.dptr) ) )
	{
		total_packets = (pdata_len / DESIRED_PACKET_PAYLOAD);
	
		if( (pdata_len % DESIRED_PACKET_PAYLOAD) > 0 )
		{
			total_packets += 1;
		}
	
		// ----------
		
		for( i=1; i<=total_packets; i++ )
		{
			ucp = ldh.dptr;
		
			ldh.dlen = maximum_response_size;
		
			// 3/20/2017 rmd : The amount to CRC does not include the 2 ambles nor the CRC itself. And
			// leave out the hub portion. Will add in if included.
			bytes_to_crc = sizeof(TO_COMMSERVER_MESSAGE_HEADER) + sizeof(MSG_DATA_PACKET_STRUCT) + DESIRED_PACKET_PAYLOAD;
		
			// 3/20/2017 rmd : And the length reported to the commserver in the 'to commserver message
			// header' includes only the 'to commserver message header' and 'msg_initialization_struct'
			// sizes. This value never includes the ambles nor the crc. And also we know the hub routing
			// structure is stripped from the message before sending to the commserver.
			length_reported_to_commserver  = sizeof(TO_COMMSERVER_MESSAGE_HEADER) + sizeof(MSG_DATA_PACKET_STRUCT) + DESIRED_PACKET_PAYLOAD;
			
			// ----------
	
			*ucp++ = preamble._1;
			*ucp++ = preamble._2;
			*ucp++ = preamble._3;
			*ucp++ = preamble._4;
	
			// ----------
	
			start_of_crc = ucp;
			
			// ----------
	
			// 3/16/2017 rmd : So if this controller is a hub-based unit include the 2 extra bytes to
			// enable this packet to be routed upon receipt by the hub.
			if( CONFIG_this_controller_is_behind_a_hub() )
			{
				// 3/16/2017 rmd : And note this part of the packet will get stripped off prior to passing
				// on to the commserver. The commserver is not expecting to see this portion.
	
				memset( &tcsuah, 0x00, sizeof(TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER) );
	
				tcsuah.PID.__routing_class = MSG_CLASS_2000e_CS3000_FLAG;
	
				tcsuah.cs3000_msg_class = MSG_CLASS_CS3000_TO_COMMSERVER_VIA_HUB;
	
				memcpy( ucp, &tcsuah, sizeof(TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER) );
		
				ucp += sizeof(TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER);
				
				// 3/20/2017 rmd : And update the bytes to crc. Since this is included in the message.
				bytes_to_crc += sizeof(TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER);
			}
			else
			{
				// 3/16/2017 rmd : Update the length to remove the hub-based portion that is not used.
				ldh.dlen -= sizeof( TO_COMMSERVER_USING_A_HUB_MESSAGE_HEADER );
			}
			
			// ----------

			// 12/4/2013 rmd : Set the network ID. Should have already been checked to
			// be non-zero prior to getting this far into sending the data.
			tcsmh.from_network_id = NETWORK_CONFIG_get_network_id();

			// 8/23/2013 rmd : Build up part of the header.
			tcsmh.from_serial_number = config_c.serial_number;

			tcsmh.mid = (UNS_16)pmessage_id;

			// 11/10/2016 rmd : DO NOT COPY TCSMH INTO UCP YET. WE NEED TO DETERMINE THE SIZE OF THE
			// PAYLOAD SO WE CAN PROPERLY SET THE TCSMH.LENGTH VALUE. CAPTURE UCP.
			tcsmh_ptr = ucp;
			
			ucp += sizeof(TO_COMMSERVER_MESSAGE_HEADER);

			// ----------

			data_struct.packet_number = (UNS_16)i;

			data_struct.mid = (UNS_16)pmessage_id;

			memcpy( ucp, &data_struct, sizeof(MSG_DATA_PACKET_STRUCT) );

			ucp += sizeof(MSG_DATA_PACKET_STRUCT);

			// ----------

			// 7/14/2014 ajv : Determine how much we have to send within this packet.
			if( i == total_packets )
			{
				// 7/14/2014 ajv : Since this is the last packet, it's likely it's not going to require the
				// full 2k block that we have available. Therefore, calculate exactly how much data is left.
				amount_to_send = pdata_len - (DESIRED_PACKET_PAYLOAD * (i-1));

				// 3/21/2017 rmd : DO THIS MATH CAREFULLY. WE ARE WORKING WITH UNSIGNED VARIABLES. AND YOU
				// COULD UNDERFLOW A VALUE WITH THE WRONG PARENTHESIS.
				//
				// 3/21/2017 rmd : And adjust the bytes to crc and the length reported in tcsmh accordingly.
				bytes_to_crc = (bytes_to_crc - DESIRED_PACKET_PAYLOAD) + amount_to_send;

				length_reported_to_commserver = (length_reported_to_commserver - DESIRED_PACKET_PAYLOAD) + amount_to_send;
				
				// 3/21/2017 rmd : And adjust the number of bytes transmitted.
				ldh.dlen = (ldh.dlen - DESIRED_PACKET_PAYLOAD) + amount_to_send;
			}
			else
			{
				amount_to_send = DESIRED_PACKET_PAYLOAD;
			}

			memcpy( ucp, pdata_ptr, amount_to_send );

			ucp += amount_to_send;

			// 7/14/2014 ajv : Increment the passed-in pointer so the next
			// iteration of the loop starts at the correct point.
			pdata_ptr += amount_to_send;

			// ----------
			
			// 3/16/2017 rmd : Set the tcsmg.length variable now that all content length is known.
			tcsmh.length = length_reported_to_commserver;
			
			memcpy( tcsmh_ptr, &tcsmh, sizeof(TO_COMMSERVER_MESSAGE_HEADER) );

			// ----------

			// 3/16/2017 rmd : Calculate the CRC for the packet. Including the LR HUB routing header if
			// present.
			lcrc = CRC_calculate_32bit_big_endian( start_of_crc, bytes_to_crc );

			memcpy( ucp, &lcrc, sizeof(CRC_BASE_TYPE) );

			ucp += sizeof(CRC_BASE_TYPE);
			
			// ----------

			*ucp++ = postamble._1;
			*ucp++ = postamble._2;
			*ucp++ = postamble._3;
			*ucp   = postamble._4;

			// ----------

			// 10/27/2016 rmd : Send packet to the serial driver for transmission. The serial driver
			// will make a final test for CTS and a connection before sending the data to the device.
			AddCopyOfBlockToXmitList( pport, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_PACKETIZED_DATA, SERIAL_DRIVER_DO_NOT_POST_NEW_TX_BLK_AVAIL );
		}

		// 8/4/2014 ajv : Since the AddCopyOfBlockToXmitList routine DID NOT post an event to the
		// serial port driver to ensure we don't overflow the event queue, do so now. At this point,
		// all of the data packets are pending on the xmit list.
		postSerportDrvrEvent( pport, UPEV_NEW_TX_BLK_AVAIL );

		// 8/4/2014 ajv : Free the memory we allocated earlier. The serial driver already took a
		// copy of the data, so there's no reason for us to hold on to it any longer.
		mem_free( ldh.dptr );
	}
	else
	{
		Alert_Message( "No Memory to send data packet" );
	}
}

/* ---------------------------------------------------------- */
static void ci_response_timer_callback( xTimerHandle pxTimer )
{
	// 10/21/2013 rmd : Remember this is executed within the context of the high priority timer
	// task. This is executed when we've waited and no response to the outgoing CI message was
	// ever received. This timeout is 5 minutes, and was made sooooo long to accomodate
	// unpredicatably long database write durations.
	CONTROLLER_INITIATED_post_event( CI_EVENT_message_termination_due_to_time );
}

/* ---------------------------------------------------------- */
static void ci_send_the_just_built_message( void )
{
	UNS_32	response_timeout_ms;
	
	float	freewave_bit_rate_fl;
	
	// 2/23/2017 rmd : This function is called directly after a message is built. And we assume
	// the message is sent to the device. All checks to ensure we should be sending a message
	// are made prior to getting to this point. Upon entry to this function we ASSUME the
	// now_xmitting structure is filled out and valid.
	
	// 6/17/2015 rmd : The ci_and_comm_mngr MUTEX should be held to protect the comm_mngr
	// pending exchange request variable. This may sound crazy but that particular variable can
	// only be set (true) when the comm_mngr holds this same MUTEX. This is a locking mechanism
	// to prevent the possiblity of a device programming occurring while there is a controller
	// initiated session underway. And vice-a-versa!
	//
	// The comm_mngr also takes this MUTEX when testing to enter device exchange mode. What he
	// tests is that the CI task is IDLE. And by taking this MUTEX if he is IDLE when the
	// comm_mngr tests he is not processing a message.
	xSemaphoreTakeRecursive( ci_and_comm_mngr_activity_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	// 9/17/2014 rmd : Always start out clearing these flags.
	cics.last_message_concluded_with_a_response_timeout = (false);

	cics.last_message_concluded_with_a_new_inbound_message = (false);

	// ----------

	#if INCLUDE_LANTRONIX_MSG_TRANSACTION_TIME_CODE

		//#warning *** compiling LANTRONIX MSG transaction time code *** 

		// 10/20/2015 rmd : LANTRONIX TEST
		EPSON_obtain_latest_time_and_date( &last_message_sent_dt_stamp );
		
	#endif
	
	// ----------
	
	// 8/19/2016 rmd : SUPPRESS for the mobile screen update. Its a repetitive nuisance!
	if( cics.now_xmitting.init_packet_message_id != MID_TO_COMMSERVER_status_screen_init_packet )
	{
		// 8/22/2013 rmd : This next line is divider for alerts readability. Likely eventually
		// should be commented out.
		Alert_divider_small();

		// 9/16/2014 ajv : The alerts that indicated the activity (stamped in CONTROLLER_INITIATED_t
		// ask) were Alert_Messages, which means they not only took up valuable alert pile space,
		// but also could not be exposed to the user now or in the future without significant
		// rework. Therefore, we've removed the the alert messages and replaced it with this single
		// call which places just the message ID on the pile.
		Alert_comm_command_sent( cics.now_xmitting.init_packet_message_id );
	}
	
	// ----------

	// 10/9/2013 rmd : Test for safety.
	if( cics.now_xmitting.activity_flag_ptr != NULL )
	{
		// 10/11/2013 rmd : Set the flag associated with this command (true). This is in effect
		// letting the incoming ACK know that indeed we are waiting for that specific ACK. Instead
		// of waiting for the command we look at this flag when we receive the command to know to
		// accept it or not.
		*(cics.now_xmitting.activity_flag_ptr) = (true);
	}

	// 8/15/2014 rmd : And regardless of its value assign the flow_recording_control_structure
	// ptr so we can track for which system the response is for (same as which system the
	// message is form). [[[[[[[ Should do the same for alerts. ]]]]]]]
	cics.current_msg_frcs_ptr = cics.now_xmitting.frcs_ptr;

	// ----------

	// 10/27/2016 rmd : If there is an error (lack of ACK, CTS timeout) we do attempt a reset /
	// disconnect / reconnect sequence on the radio.
	cics.state = CI_STATE_waiting_for_response;

	// ----------

	// 8/11/2014 rmd : Take the message and craft the format the comm server is expecting. That
	// is an init packet followed by the 2K data packets. We carefully load up the serial driver
	// packet list. Making sure to manage how many times we feed the serial driver the
	// NEW_BLK_AVAIL event. If not careful we would overflow the serial driver queue with the
	// new blk event.
	nm_build_init_packet_and_send_it( cics.now_xmitting.message.dptr, cics.now_xmitting.message.dlen, cics.now_xmitting.init_packet_message_id, UPORT_A );

	nm_build_data_packets_and_send_them( cics.now_xmitting.message.dptr, cics.now_xmitting.message.dlen, cics.now_xmitting.data_packet_message_id, UPORT_A );
	
	// ----------
	
	// 5/6/2014 rmd : And free the message memory now. The serial driver has a copy and we don't
	// need it anymore.
	mem_free( cics.now_xmitting.message.dptr );

	// 5/6/2014 rmd : But don't free the cili_ptr memory itself. We need that intact when
	// processing the response.

	// ----------
	
	// 7/28/2017 rmd : Now start the response timer. This needs to be a SMART timeout period,
	// especially when it comes to controllers behind a hub. The radio devices used on a hub are
	// slow, around 4.5KBaud. A big 500K station report data message would take 18.5 minutes at
	// that rate with 100% duty cycle, BUT we cannot do a 100% DUTY CYCLE! So WOW, it may take
	// double or triple that to get such a message through. We may have to limit the number of
	// records we send where the message is built for the case of RAVEON LR radios.

	// 8/4/2017 rmd : NOTE - the time calculated needs to be thought about as a TRANSMISSION
	// TIME + DATABASE TIME. Unfortunately our database exchanges can be quite lengthy, upwards
	// of 5 minutes. We are hoping when we switch from the Advantage database to MS Squeal
	// Server the performance will improve by an order of magnitude. REGARDLESS after computing
	// the transmission time add our 5 minute database timeout.

	// 8/4/2017 rmd : Start with a default 2 minute transmission time. This covers all messages
	// for -GR, -EN, -WEN devices. I picked this time based upon observation. We've been running
	// with a 5 minute timeout for some time now, succesfully. Now that we understand more about
	// the needs of the time needed, we are formally coming up with a transmission time and
	// database time.
	response_timeout_ms = (2*60*1000);
	
	// 8/4/2017 rmd : For special cases the transmission time can be significantly longer than
	// our default. Find those cases and adjust.
	if( CONFIG_this_controller_is_behind_a_hub() )
	{
		if( config_c.port_A_device_index == COMM_DEVICE_SR_FREEWAVE )
		{
			// 7/28/2017 rmd : The effective over the air data rate is 4.5KBaud. And the units can run
			// at a 100% duty cycle. So take the length of the message and figure how long it should
			// take, and add a 20% safety margin. Remember, baud is bits per second, so multiply the
			// byte length by 10 - the number of uart bits per byte.
			if( config_c.purchased_options.port_a_freewave_sr_set_for_repeater )
			{
				freewave_bit_rate_fl = CODE_DISTRIBUTION_EFFECTIVE_OVER_THE_AIR_BIT_RATE_FOR_FREEWAVE_SR_WITH_REPEATER;
			}
			else
			{
				freewave_bit_rate_fl = CODE_DISTRIBUTION_EFFECTIVE_OVER_THE_AIR_BIT_RATE_FOR_FREEWAVE_SR_NO_REPEATER;
			}
			
			response_timeout_ms = ( 1.2 * (1000 * ((cics.now_xmitting.message.dlen * 10) / freewave_bit_rate_fl)) );
		}
		else
		if( config_c.port_A_device_index == COMM_DEVICE_LR_FREEWAVE )
		{
			if( config_c.purchased_options.port_a_freewave_lr_set_for_repeater )
			{
				freewave_bit_rate_fl = CODE_DISTRIBUTION_EFFECTIVE_OVER_THE_AIR_BIT_RATE_FOR_FREEWAVE_LR_WITH_REPEATER;
			}
			else
			{
				freewave_bit_rate_fl = CODE_DISTRIBUTION_EFFECTIVE_OVER_THE_AIR_BIT_RATE_FOR_FREEWAVE_LR_NO_REPEATER;
			}
			
			// 7/28/2017 rmd : Math explained in the SR case above.
			response_timeout_ms = ( 1.2 * (1000 * ((cics.now_xmitting.message.dlen * 10) / freewave_bit_rate_fl)) );
		}
		else
		if( config_c.port_A_device_index == COMM_DEVICE_LR_RAVEON )
		{
			// 7/28/2017 rmd : Math explained in the SR case above.
			response_timeout_ms = ( 1.2 * (1000 * ((cics.now_xmitting.message.dlen * 10) / CODE_DISTRIBUTION_EFFECTIVE_OVER_THE_AIR_BIT_RATE_FOR_RAVEON_LR)) );
		}
		else
		{
			Alert_Message( "CI: unexpected device index" );
		}
	}
	
	// ----------
	
	// 8/4/2017 rmd : Now add in the database transmission time. Wissam and I agreed 5 minutes
	// for this component of the timeout should work. We have seen up to 3 minute database
	// exchanges.
	response_timeout_ms += CONTROLLER_INITIATED_DATABASE_RECORD_INSERT_TIME_MS;
	
	// 9/9/2013 rmd : Being that the timer task is a high priority task using portMAX_DELAY when
	// posting is safe. Anything on the timer queue is quickly processed and therefore the queue
	// is effectively always empty.
	xTimerChangePeriod( cics.response_timer, MS_to_TICKS( response_timeout_ms ), portMAX_DELAY );

	// ----------
	
	xSemaphoreGiveRecursive( ci_and_comm_mngr_activity_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void ci_waiting_for_response_state_processing( UNS_32 pevent )
{
	switch( pevent )
	{
		case CI_EVENT_wrap_up_this_transaction:
			// 5/14/2015 rmd : Because we are going to change the CI state take this MUTEX to be safe. I
			// actually think this is not required here.
			xSemaphoreTakeRecursive( ci_and_comm_mngr_activity_recursive_MUTEX, portMAX_DELAY );

			// 8/21/2013 rmd : Well we received a response. Or maybe the timeout timer already expired.
			// In either case stop the timer.
			xTimerStop( cics.response_timer, portMAX_DELAY );

			// ----------

			// 2/23/2017 rmd : Zero the now_xmitting structure. We test now_xmitting to be all zeros
			// when the state is IDLE and we go to build another message.
			memset( &cics.now_xmitting, 0x00, sizeof( CONTROLLER_INITIATED_MESSAGE_TRANSMITTING ) );
			
			// ----------

			// 2/24/2017 rmd : Before we clear the flags decide should we post an event to OUR queue
			// (yes this function is executing in the context of the CI task so any event posted to our
			// queue cannot be acted upon till this function is done. Furthermore when we finally do act
			// upon this event, if the connection process is underway we will not build and send a new
			// message.
			//
			// 2/24/2017 rmd : But WHOA! Hold your horses. If the last message sent was the 'no more
			// messages' msg DO NOT proceed. And this should only be being sent for controllers that are
			// either a hub, or are on a hub. So make that sanity check too.
			if( CONFIG_this_controller_is_a_configured_hub() || CONFIG_this_controller_is_behind_a_hub() )
			{
				if( cics.last_message_concluded_with_a_new_inbound_message )
				{
					// 11/2/2017 rmd : If it was a NEW inbound message that terminated the last message, then
					// the commserver is expecting us to respond, so by all means march ahead.
					CONTROLLER_INITIATED_post_event( CI_EVENT_build_and_send_the_next_queued_msg );
				}
				else
				if( cics.waiting_for_the_no_more_messages_msg_response || cics.waiting_for_hub_is_busy_msg_response )
				{
					// 2/27/2017 rmd : And do not post the event to build and send the next message if we just
					// sent the 'no more messages' msg, or we sent the 'hub is busy' msg.
					//Alert_Message( "CI: poll period over" );  nuisance alert
				}
				else
				{
					CONTROLLER_INITIATED_post_event( CI_EVENT_build_and_send_the_next_queued_msg );
				}
			}
			else
			{
				// 2/27/2017 rmd : Sanity check.
				if( cics.waiting_for_the_no_more_messages_msg_response )
				{
					Alert_Message( "CI: why did non-hub controller send this msg?" );
				}
			
				// 2/27/2017 rmd : For regular controllers always plow ahead with an attempt to send the
				// next message.
				CONTROLLER_INITIATED_post_event( CI_EVENT_build_and_send_the_next_queued_msg );
			}
			
			// ----------
			
			// 3/27/2017 rmd : And clear all the waiting flags. Do not move this function call in
			// relationship to other calls within this case statement processing. The flags need to be
			// set for the above logic to decide should we post the event to build another message.
			__clear_all_waiting_for_response_flags( (cics.last_message_concluded_with_a_response_timeout || cics.last_message_concluded_with_a_new_inbound_message), (true) );

			// ----------

			// 2/23/2017 rmd : Switch states to allow the connection process to start, if need be. If
			// state is still 'waiting for response' connection process will not begin.
			cics.state = CI_STATE_idle;

			// ----------
			
			// 11/2/2017 rmd : Only if the error was a TIMEOUT error, or we see the device isn't
			// connected, then we should go ahead and start the connection process. If the error was a
			// new inbound message came in, THEN the assumption is the socket is good, so leave it
			// alone, and let the new inbound message process!
			if( cics.last_message_concluded_with_a_response_timeout || !CONFIG_is_connected() )
			{
				// 10/29/2016 rmd : Depending on the device, the connection process starts with a RESET or
				// cycling of power. So if connected will be disconnected.
				//
				// 2/3/2017 rmd : Try to start it. If not eligible won't. There are quite a few safeguards
				// in place so directly calling the start function with no checking here is safe to do.
				ci_start_the_connection_process();
			}

			// ----------

			xSemaphoreGiveRecursive( ci_and_comm_mngr_activity_recursive_MUTEX );

			break;


		case CI_EVENT_message_termination_due_to_time:
		case CI_EVENT_message_termination_due_to_new_inbound_message:

			// 9/17/2014 ajv : Generate an alert which can be filtered to be displayed for the user.
			Alert_comm_command_failure( pevent );

			// ----------
			
			// 12/22/2014 ajv : Update the Comm Test string so the user can see the current status
			strlcpy( GuiVar_CommTestStatus, "Communication failed: No response", sizeof(GuiVar_CommTestStatus) );

			// 4/17/2015 ajv : If the user is viewing the Communication Test screen, we should refresh
			// the cursors to show the updated text.
			if( GuiLib_CurStructureNdx == GuiStruct_scrCommTest_0 )
			{
				Refresh_Screen();
			}

			// ----------

			// 11/2/2017 rmd : Track what terminated the message, so we know to restart the connection
			// or not.
			if( pevent == CI_EVENT_message_termination_due_to_time )
			{
				// 9/16/2014 rmd : And since we suffered an error try to straighten away the device if that
				// is what is behind the error.
				cics.last_message_concluded_with_a_response_timeout = (true);
			}
			else
			{
				cics.last_message_concluded_with_a_new_inbound_message = (true);
			}
			
			// ----------

			// 11/11/2016 rmd : NOTICE we DO NOT change the state. So when we post this event we execute
			// the case above.
			CONTROLLER_INITIATED_post_event( CI_EVENT_wrap_up_this_transaction );

			break;

	}  // of switch
}

/* ---------------------------------------------------------- */
extern void CONTROLLER_INITIATED_force_end_of_transaction( void )
{
	// 5/8/2017 rmd : Use very carefully, only to, in a brute force manner, end a controller
	// initiated message exchange. Will usually result in the message or message contect being
	// resent at a later time. Used, for example, when a disconnect is seen while we are not in
	// the middle of the connection process. Also used upon entry to the device exchange mode.
	if( (cics.mode == CI_MODE_ready) && (cics.state == CI_STATE_waiting_for_response) )
	{
		Alert_Message( "CI: forcing end of message exchange" );
		
		xTimerStop( cics.response_timer, portMAX_DELAY );

		memset( &cics.now_xmitting, 0x00, sizeof( CONTROLLER_INITIATED_MESSAGE_TRANSMITTING ) );
		
		// 3/28/2017 rmd : Pretend there was an error so we restart particular timers. And stamp the
		// divider line too.
		__clear_all_waiting_for_response_flags( (true), (true) );

		cics.state = CI_STATE_idle;
	}
}	

/* ---------------------------------------------------------- */
static void set_now_xmitting_and_send_the_msg(	DATA_HANDLE                     pmessage_handle,
												BOOL_32                         *presponse_flag_ptr,
												FLOW_RECORDING_CONTROL_STRUCT   *pfrcs_ptr,
												const UNS_32                    pinit_packet_mid,
												const UNS_32                    pdata_packet_mid )
{
	// 8/20/2013 rmd : The payload at this point is the message. INCLUDING the payload header.

	// 8/14/2013 rmd : The caller allocates the payload memory. But the caller is NOT
	// responsible for the release of said memory. That responsiblity is passed onto the message
	// processing functions. When the message is sent to the serial drivers the memory is
	// freed.

	// ----------

	// 2/23/2017 rmd : Sanity check for 0 byte messages. This should never happen.
	if( pmessage_handle.dlen == 0 )
	{
		Alert_Message( "CI : trying send a 0 byte msg" );
	}
	else
	{
		cics.now_xmitting.message = pmessage_handle;
		
		cics.now_xmitting.activity_flag_ptr = presponse_flag_ptr;
		
		cics.now_xmitting.frcs_ptr = pfrcs_ptr;
		
		cics.now_xmitting.init_packet_message_id = pinit_packet_mid;
		
		cics.now_xmitting.data_packet_message_id = pdata_packet_mid;
		
		// ----------
		
		// 2/23/2017 rmd : Now SEND the message!
		ci_send_the_just_built_message();
	}
}

/* ---------------------------------------------------------- */
static void check_if_we_need_to_post_a_build_and_send_event( UNS_32 pbuild_result )
{
	if( pbuild_result != CI_RESULT_MESSAGE_SUCCESSFUL )
	{
		if( CONFIG_this_controller_is_a_configured_hub() || CONFIG_this_controller_is_behind_a_hub() )
		{
			// 3/1/2017 rmd : Picture this for a hub based controller: we receive a poll message and put
			// the 'no more messages' msg on the msg queue. And the message we just tried to build
			// is the only thing ahead of it. Well we count on the msg transaction to trigger the next
			// message to be built and sent. In this case we didn't build a message (the build failed
			// for some reason or another). So the msg transaction is dead here. We MUST (for hub based)
			// post our own 'build and send' event to the task to get the 'no more messages' msg (or
			// possibly other msgs) to continue to spew out and for the poll period to be gracefully
			// ended.
			CONTROLLER_INITIATED_post_event( CI_EVENT_build_and_send_the_next_queued_msg );
		}
	}
}
	
/* ---------------------------------------------------------- */
static BOOL_32 sending_registration_instead( UNS_32 pevent, BY_SYSTEM_RECORD *pbsr_ptr )
{
	// 12/8/2014 rmd : Executed within the context of the controller initiated task. This
	// function should only be called after the specific controller initiated timer has fired
	// (ie alerts, station history, etc). And in the timer call back we check if the user has
	// supressed sending. And after we have checked we are a master and have an internet device.
	// And after we have verified the comm_mngr is not in the midst of a device exchange
	// session.
	//
	// Returns true if we are sending the registration instead. And generally the design is to
	// restart the callers timer to try again after the registration has been sent. WE DO NOT
	// WANT TO QUEUE the callers intended CI message. Because if the registration connection
	// fails for some reason we would be sending data to the comm server from an UNREGISTERED
	// CONTROLLER!

	BOOL_32		rv;

	// ----------
	
	rv = (false);

	// ----------

	// 12/8/2014 rmd : If networkID is 0 send the registration data.
	//
	// 10/25/2017 rmd : Today I am introducing the restriction that the network has to be up and
	// running, and fully synced, and the chain not down, and the comm_mngr past the ISP phase
	// in order to be allowed to send the registration. This is embodied in the function call
	// COMM_MNGR_network_is_available_for_normal_use. I guess the justification for this
	// allowing sending of the registration is that with a netowrk ID of 0, this unit has never
	// registered so get this one time event done.
	if( NETWORK_CONFIG_get_network_id() == 0 )
	{
		rv = (true);
	}

	// ----------

	// 8/14/2014 rmd : And check the pending to send registration variable. If set we set the
	// other flag to send the registration. The pending flag is cleared when we build the
	// message. And the other flag is cleared when the ACK is received. This way if we don't
	// receive the ACK and timeout we will try again. And if the application makes a second
	// request while mid session sending the registration we will also send again. Notice the
	// use of the MUTEX when the pending flag is set. This assures us when we clear it (when
	// message is built) there wasn't an outstanding user request we just erased.
	if( battery_backed_general_use.pending_request_to_send_registration || battery_backed_general_use.need_to_send_registration )
	{
		// 10/25/2017 rmd : We have introduced this strict criteria on the ability to send the
		// registration after we observed it being sent pretty much whenever there was a pending
		// reason without consideration for system startup. So, for example, following a tpmicro
		// update, with an EN device (fast connect), it was possible to connect and send
		// registration even before the tpmicro was running. What's wrong with that? Well the
		// tpmicro delivers content, the what's installed, that ends up in the chain_members
		// structure that is sent as part of the registration message, so the registration stood a
		// chance of being non-representative of the hardware. There are other circustances too, for
		// example after a failed scan. And what we saw was sometimes a customer on CCO would
		// experience varying configurations, with controllers coming and going. And that was
		// confusing. This will help.
		if( COMM_MNGR_network_is_available_for_normal_use() )
		{
			// 12/8/2014 rmd : Tell the caller to postpone his message, we are sending the
			// registration.
			rv = (true);
	
			// 8/14/2014 rmd : This may look faulty setting the need_to_send here but it is intentional
			// and well thought out. Plays into the functionality and interaction of the two flags and
			// the MUTEX.
			battery_backed_general_use.need_to_send_registration = (true);
		}
	}

	// ----------

	if( rv )
	{
		// 2/15/2017 rmd : So apparently we have tried to send a message to the commserver and
		// determined that we need to send the registration first. How to do that? Well we have to
		// post to the queued message queue in an order so that we end up with the registration
		// first and then the callers message request re-posted. AS THE FIRST TWO MSGS TO
		// SEND. So put them on in reverse order posting to the front of the queue.
		
		// 2/15/2017 rmd : Repost the callers attempted message.
		CONTROLLER_INITIATED_post_to_messages_queue( pevent, pbsr_ptr, CI_IF_PRESENT_REMOVE_AND_REPOST, CI_POST_TO_FRONT );

		// 2/15/2017 rmd : Put the registration in place ahead of that.
		CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_registration, NULL, CI_IF_PRESENT_REMOVE_AND_REPOST, CI_POST_TO_FRONT );
		
		// 2/15/2017 rmd : And now kick it all off. And this can be posted to the back of the ci
		// queue. That's okay. It'll get hit quickly.
		CONTROLLER_INITIATED_post_event( CI_EVENT_build_and_send_the_next_queued_msg );
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ------------ R E G I S T R A T I O N --------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void CONTROLLER_INITIATED_update_comm_server_registration_info( void )
{
	// 8/14/2014 rmd : Called by any task when a change is made that effects the registration
	// content (and therefore the comm_server should know about that). Not to called by the CI
	// task itself. As that could potentially break the 'pending' variable functionality.
	//
	// Note - we take the ci activity MUTEX. That is important to the overall scheme. We also
	// hold the MUTEX when the registration message is built and this flag is cleared! And that
	// is key to plugging any holes that could result in a registration data change that is not
	// sent tot he comm_server. That would be bad!
	xSemaphoreTakeRecursive( ci_and_comm_mngr_activity_recursive_MUTEX, portMAX_DELAY );

	battery_backed_general_use.pending_request_to_send_registration = (true);

	xSemaphoreGiveRecursive( ci_and_comm_mngr_activity_recursive_MUTEX );
}

static UNS_32 send_ci_registration_data( void )
{
	// 6/17/2015 rmd : This function executed within the context of the controller_initiated
	// task only. There are no mutex considerations for the caller.

	// ----------

	// 8/14/2014 rmd : We do however need to be holding the ci_and_comm_mngr_activity_MUTEX when
	// we build the message and clear the pending flag. Otherwise the user may be able to set it
	// only to have it cleared without the data actually being sent to the comm server!
	xSemaphoreTakeRecursive( ci_and_comm_mngr_activity_recursive_MUTEX, portMAX_DELAY );

	// ----------

	UNS_32			rv;

	DATA_HANDLE		msg_handle;

	// 8/23/2013 rmd : Work with a ptr to the registration struct to keep the structure itself
	// off the stack. In case it grows large as we add to it.
	COMM_SERVER_REGISTRATION_STRUCT		*csrs_ptr;

	// ----------

	rv = CI_RESULT_MESSAGE_SUCCESSFUL;

	// ----------

	// 4/28/2015 rmd : Only if it hasn't already been built.
	if( !cics.a_registration_message_is_on_the_list )
	{
		// 8/23/2013 rmd : Get the memory for and fill out the registration structure. We get a
		// seperate block like this to keep the actual registration structure off the stack. I have
		// a feeling the size of the registration structure is going to grow substantially over
		// time.
		if( mem_obtain_a_block_if_available( sizeof(COMM_SERVER_REGISTRATION_STRUCT), (void**)&csrs_ptr ) )
		{
			// 8/15/2014 rmd : Clean it first then fill out the registration structure.
			memset( csrs_ptr, 0x00, sizeof(COMM_SERVER_REGISTRATION_STRUCT) );

			csrs_ptr->structure_version = REGISTRATION_STRUCTURE_REVISION;

			EPSON_obtain_latest_time_and_date( &(csrs_ptr->controller_date_time) );


			snprintf( csrs_ptr->main_code_revision_string, sizeof(csrs_ptr->main_code_revision_string), "%s", restart_info.main_app_code_revision_string );

			// 3/4/2015 ajv : Get the TP Micro version (if it's available, which it should be at this
			// point)
			if( tpmicro_comm.file_system_code_date_and_time_valid )
			{
				UNS_32 lday, lmonth, lyear, ldow, lhour, lmin, lsec;

				DateToDMY( tpmicro_comm.file_system_code_date, &lday, &lmonth, &lyear, &ldow );
				TimeToHMS( tpmicro_comm.file_system_code_time, &lhour, &lmin, &lsec );

				// 3/4/2015 ajv : The format of the TP Micro string should match that of the controller. For
				// example, "Mar  4 2015 @ 14:19:45"
				snprintf( csrs_ptr->tp_micro_code_revision_string, sizeof(csrs_ptr->tp_micro_code_revision_string), "%s %2d %4d @ %02d:%02d:%02d", GetMonthShortStr(lmonth-1), lday, lyear, lhour, lmin, lsec );
			}
			else
			{
				Alert_Message( "TP Micro date/time not valid yet" );
			}

			// ----------
			
			// 3/4/2015 ajv : There was a comment here prompting why we aren't sending the network ID as
			// well. One reason is the message header already includes the network ID. This is actually
			// what the Comm Server uses to identify whether it needs to assign a new network ID to the
			// controller in the case where the Network ID is 0 or the provided Network ID doesn't
			// exist within the database yet. Adding it here would just end up being redundant.

			// ----------
			
			// 10/13/2017 rmd : Now copy chain members to the message we are building.
			memcpy( &(csrs_ptr->members), &(chain.members), sizeof(chain.members) );

			// ----------

			// 10/13/2017 rmd : Chain members is fraught with problems in that it is only accurate when
			// the chain is up, and static. Changes that take place to what's installed and purchased
			// option aren't necessarily written into the chain members. THIS IS BAD. Also using chain
			// members to draw the back plane picture is problems some. And to display the controller
			// details screen is problemsome. This is a KLUDGE to patch at least what we are sending for
			// for this controller. But what about the rest of them on the chain?
			// Maybe ok ... not sure.
			load_this_box_configuration_with_our_own_info( &(csrs_ptr->members[ FLOWSENSE_get_controller_index() ].box_configuration) );
			
			// 6/28/2017 rmd : Here is where we modify the purchased HUB OPTION bit to reflect if the
			// overall controller configuration is that of a true HUB, comm devices and all. Note it is
			// the A box that matters, that afterall IS THE HUB, hence using index 0 of the members
			// array.
			csrs_ptr->members[ 0 ].box_configuration.purchased_options.option_HUB = CONFIG_this_controller_is_a_configured_hub();
			
			// ----------

			// 9/4/2015 rmd : Set indication to clear pdata or not. We clear the weather preserves
			// variable when the ack is rcvd.
			csrs_ptr->commserver_to_clear_data = weather_preserves.factory_reset_registration_to_tell_commserver_to_clear_pdata;
			
			Alert_Message_va( "Cloud PDATA to be deleted: %s", GetNoYesStr( csrs_ptr->commserver_to_clear_data ) );

			// ----------

			// 8/23/2013 rmd : Make sure the msg handle reflects the proper length.
			msg_handle.dlen = sizeof(COMM_SERVER_REGISTRATION_STRUCT);

			msg_handle.dptr = (void*)csrs_ptr;

			set_now_xmitting_and_send_the_msg( msg_handle, &cics.waiting_for_registration_response, NULL, MID_TO_COMMSERVER_REGISTRATION_DATA_init_packet, MID_TO_COMMSERVER_REGISTRATION_DATA_data_packet );

			// ----------

			// 4/28/2015 rmd : Mark that a registration message is on the pending message list.
			cics.a_registration_message_is_on_the_list = (true);

			// 8/14/2014 rmd : And now clear the pending flag. After we know we got a copy of the latest
			// registration data and we are protected by the ci_and_comm_mngr_activity_MUTEX. Protected
			// in that if from the time we took the copy till we cleared it another higher level task
			// actually is trying to set it.
			battery_backed_general_use.pending_request_to_send_registration = (false);
		}
		else
		{
			Alert_Message( "No memory for registration msg?" );

			rv = CI_RESULT_NO_MEMORY;
		}
	}
	else
	{
		Alert_Message( "Registration already being sent?" );

		rv = CI_RESULT_INFRASTRUCTURE_IN_USE;
	}
	

	// ----------

	xSemaphoreGiveRecursive( ci_and_comm_mngr_activity_recursive_MUTEX );

	// ----------

	return( rv );
}

static void send_registration_packet( void )
{
	UNS_32	ci_result;
	
	ci_result = send_ci_registration_data();

	// 8/14/2014 rmd : The only reason would be no memory available to build the message in. In
	// which case the 'pending' flag would still be set (or the 'need_to_send' flag is set if we
	// didn't get a response to a prior attempt to send the reg data). So nothing to do here. If
	// we do not get an ACK response the next attempt to send any other type of CI data will
	// result in sending the registration again instead.
	
	// ----------
	
	// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
	// controllers working on the next msg in the msg queue.
	check_if_we_need_to_post_a_build_and_send_event( ci_result );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* --------------------- A L E R T S ------------------------ */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void ci_alerts_timer_callback( xTimerHandle pxTimer )
{
	// 10/21/2013 rmd : Remember this is executed within the context of the high priority timer
	// task. And is executed when it it time to send alerts. Which is normally 15 minutes after
	// an alert is made.
	CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_alerts, NULL, CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD, CI_POST_TO_BACK );
}

extern void CONTROLLER_INITIATED_alerts_timer_upkeep( UNS_32 timer_duration_ms, BOOL_32 pforce_timer_to_load )
{
	// 10/8/2013 rmd : This function is called when an alert is made. And therefore can be
	// called from a multitude of tasks. Any task that generates an alert. Which is likely all
	// and any task!
	//
	// If the timer is already running we do not restart it. If we did and an alert was
	// generated more frequently than the timeout we would never send them. However for the
	// SPECIAL case of the HI_PRIORITY duration we always restart. This ASSUMES that such an
	// alert will not continue being made once every 15 seconds. I think that is a safe bet. BUT
	// more importantly the reason to restart is to wipe likely the default duration from the
	// timer and set it up for the priority.

	// 10/11/2013 rmd : Test to make sure the timer exists. So we are not making calls with a
	// NULL timer. This should NEVER happen as the timer is made before the alerts piles are
	// initiated.
	if( cics.alerts_timer != NULL )
	{
		// 10/8/2013 rmd : For a routine alert start the timer. BUT only if it is NOT already
		// running.
		if( (xTimerIsTimerActive( cics.alerts_timer ) == pdFALSE) || pforce_timer_to_load )
		{
			// 10/8/2013 rmd : Use the change period function as the use of the timer reset function may
			// reset it with the HI PRIORITY value if that was last used.
			//
			// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
			// when posting is safe. Anything on the timer queue is quickly processed and therefore the
			// queue is effectively always empty.
			xTimerChangePeriod( cics.alerts_timer, MS_to_TICKS( timer_duration_ms ), portMAX_DELAY );
		}
	}
}

static UNS_32 send_ci_engineering_alerts( void )
{
	// 10/9/2013 rmd : Executed within the CONTROLLER_INITIATED task when the send alerts tiemr
	// expires. RETURNS true if the message is under way. RETURNS false if alerts not sent. And
	// that can happen for a number of reasons. In all cases our intent is to use the return
	// value to restart send alerts timer.

	// ----------

	DATA_HANDLE     msg_handle;

	ALERTS_PILE_STRUCT  *lpile;

	UNS_32      alert_length, iii;

	UNS_32      size_limit;

	UNS_8       *ucp, *pile_start;

	BOOL_32     rv;

	// ----------

	rv = CI_RESULT_MESSAGE_SUCCESSFUL;

	// ----------

	// 8/30/2013 rmd : Block OTHER tasks from making alerts. Within this function error
	// conditions may trigger the generation of an alert with no harm. In those cases we are
	// abandoning the send attempt and allowed to change the pile.
	xSemaphoreTakeRecursive( alerts_pile_recursive_MUTEX, portMAX_DELAY );

	// ----------

	// 8/30/2013 rmd : We're pulling the alerts from the engineering pile.
	lpile = &alerts_struct_engineering;

	pile_start = alert_piles[ lpile->alerts_pile_index ].addr;

	// ----------

	// 8/25/2014 rmd : If the pile is available we can take a closer look. If not git.
	if( !lpile->ready_to_use_bool )
	{
		Alert_Message( "Alerts pile not ready." );

		rv = CI_RESULT_NO_DATA_TO_SEND;
	}
	else
	{
		// 10/9/2013 rmd : If the pile is ready and we don't have a message in the works go ahead
		// and try a new sending. The in_use flag indicates a message is already queued up. It may
		// be in the midst of processes or simply waiting on the queue to go out. Either way the
		// flag is (true).
		if( lpile->pending_first_to_send_in_use )
		{
			Alert_Message( "Attempt to send alerts while in process." );

			rv = CI_RESULT_INFRASTRUCTURE_IN_USE;
		}
		else
		{
			// 8/30/2013 rmd : We won't need to put more than this many bytes into the memory block
			// while pulling the alerts. If so we abandon and declare something wrong.
			size_limit = alert_piles[ lpile->alerts_pile_index ].pile_size;

			// ----------

			if( !mem_obtain_a_block_if_available( size_limit, (void**)&(msg_handle.dptr) ) )
			{
				Alert_Message( "No Memory to send alerts" );

				rv = CI_RESULT_NO_MEMORY;
			}
			else
			{
				ucp = msg_handle.dptr;

				msg_handle.dlen = 0;

				// ----------

				// 8/30/2013 rmd : Because of the way we increment the pointers (indicies) the next
				// available location will never be the same as the first OR first_to_send. That is, in a
				// protected fashion, we bump next, then test if it has landed on the first, and if so then
				// bump the first. Both the usual FIRST and the NEW_FIRST if needed.
				//
				// Therefore if the first_to_send is not equal to the next then there are alerts to send!
				if( lpile->first_to_send == lpile->next )
				{
					mem_free( msg_handle.dptr );

					rv = CI_RESULT_NO_DATA_TO_SEND;
				}
				else
				{
					// 8/30/2013 rmd : Be sure not to move the first_byte_to_send until the communication
					// transaction is succesful. That is it receives a response from the comm server.
					lpile->pending_first_to_send = lpile->first_to_send;

					while( (lpile->pending_first_to_send != lpile->next) && (rv == CI_RESULT_MESSAGE_SUCCESSFUL) )
					{
						// 8/30/2013 rmd : Obtain the length of the alert to pull.
						alert_length = nm_ALERTS_parse_alert_and_return_length( lpile, PARSE_FOR_LENGTH_ONLY, NULL, 0, lpile->pending_first_to_send );

						for( iii=0; iii<alert_length; iii++ )
						{
							// ----------

							// 8/30/2013 rmd : See if we've already filled the block. If so it is an error.
							if( msg_handle.dlen >= size_limit )
							{
								// 8/30/2013 rmd : There's been an error. Free the message memory and exit without sending.
								mem_free( msg_handle.dptr );

								// 8/30/2013 rmd : It is okay to make an alert since we are going to ABANDON this effort.
								// Remember there is nothing for us to restore. We left the orignal pile state alone.
								Alert_Message( "CI: alerts to commserver - too many!" );

								rv = CI_RESULT_ERROR_BUILDING_MESSAGE;

								break;
							}

							// ----------

							// 8/30/2013 rmd : Pull the raw bytes and add to the message. And update all counts and
							// pointers.
							*ucp = *(pile_start + lpile->pending_first_to_send);

							ucp += 1;

							msg_handle.dlen += 1;

							ALERTS_inc_index( lpile, &lpile->pending_first_to_send, 1 );

							// ----------
						}

					}

					if( rv == CI_RESULT_MESSAGE_SUCCESSFUL )
					{
						// 8/30/2013 rmd : Set the pending NEW first. And mark it as in use to prevent another pass
						// through this function prior to the message actually being sent. We wait till we see the
						// acknowledge before we update the actual first_byte_to_send variable.
						lpile->pending_first_to_send_in_use = (true);

						// ----------

						// 8/30/2013 rmd : Send it!
						set_now_xmitting_and_send_the_msg( msg_handle, &cics.waiting_for_alerts_response, NULL, MID_TO_COMMSERVER_ENGINEERING_ALERTS_init_packet, MID_TO_COMMSERVER_ENGINEERING_ALERTS_data_packet );
					}

				}  // of if there is data to send

			}  // of if memory available

		}  // message already being sent

	}  // of if pile is ready to use

	xSemaphoreGiveRecursive( alerts_pile_recursive_MUTEX );

	// ----------

	return( rv );
}

static void send_engineering_alerts( void )
{
	UNS_32  ci_result;

	// ----------

	if( !sending_registration_instead( CI_MESSAGE_send_alerts, NULL ) )
	{
		// 8/25/2014 rmd : Attempt to send. Then evaluate the function return value to determine if
		// timer should be restarted.
		ci_result = send_ci_engineering_alerts();

		// 8/25/2014 rmd : The only time we should restart the timer would be if the infrastructure
		// was in use. This would mean the timer fired (or a manual attempt was initiated) while an
		// ALERT message was either on the ci msg queue or actually in process. Meaning a message
		// was made and it has not yet completed. All the other reasons (except when successful) are
		// errors. And I don't want to fill the alerts with error after error. Once is good enough.
		// And it won't be once. The next time the timer fires cause a new line is made the error
		// may happen again.
		//
		// 9/11/2014 rmd : There should always be memory to send alerts. They aren't that big.
		if( (ci_result == CI_RESULT_INFRASTRUCTURE_IN_USE) || (ci_result == CI_RESULT_NO_MEMORY) )
		{
			// 10/11/2013 rmd : The most likely cause is there is an alerts exchange already queued
			// up. And this can only happen if the normal 15 minute timer fired, a ci alerts message has
			// been created and queued, and then a high priority alert was added to the pile and its
			// consequent timer fired before the previously queued alerts message completed.
			Alert_Message( "CI busy - alerts timer restarted" );

			// ----------

			// 8/25/2014 rmd : Restart the alerts timer at the regular value. 15 minutes. No reason to
			// use anything different.
			xTimerChangePeriod( cics.alerts_timer, MS_to_TICKS( CONTROLLER_INITIATED_DEFAULT_SEND_ALERTS_TIMEOUT_MS ), portMAX_DELAY );
		}
		
		// ----------
		
		// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
		// controllers working on the next msg in the msg queue.
		check_if_we_need_to_post_a_build_and_send_event( ci_result );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* -------------- F L O W   R E C O R D I N G --------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/14/2014 rmd : NOTE - the flow recording timer and its associated callback functions are
// located in the flow recording file.

static UNS_32 send_ci_flow_recording( BY_SYSTEM_RECORD *pbsr_ptr )
{
	// 10/9/2013 rmd : Executed within the CONTROLLER_INITIATED task context when the send flow
	// recording timer expires. RETURNS true if a message is built and posted to the CI task.
	// RETURNS false if a message was not built. And that can happen for a number of reasons.

	// ----------

	DATA_HANDLE     msg_handle;

	UNS_32          size_limit;

	UNS_8           *ucp;

	UNS_32          rv;

	rv = CI_RESULT_MESSAGE_SUCCESSFUL;

	// ----------

	// 10/22/2013 rmd : Block any changes to the flow_recording data by taking the
	// system_preserves MUTEX.
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	// ----------

	// 8/15/2014 rmd : If THIS SYSTEM already has a message pending do not queue another message
	// onto the ci queue. The tracking of what has been sent would lead us to queueing the same
	// message twice. I also think it opens the door to some kind of a hole when the ACK is
	// received and the next to send pointer is adjusted.
	if( pbsr_ptr->frcs.pending_first_to_send_in_use )
	{
		// 8/15/2014 rmd : Not sure if this would ever occur. Would think not when thinking about
		// the timers but I suppose manually triggered could do this.
		Alert_Message( "Flow Recording: message in process." );

		rv = CI_RESULT_INFRASTRUCTURE_IN_USE;
	}
	else
	{
		// 10/24/2013 rmd : If the frcs has been initialized and there are lines proceed. Because of
		// the way we increment the pointers, when there is a new line, the next_available ptr will
		// NOT be the same as the first_to_send pointer. Therefore if the first_to_send is not equal
		// to the next then there are records to send!
		if( (pbsr_ptr->frcs.original_allocation == NULL) || (pbsr_ptr->frcs.first_to_send == pbsr_ptr->frcs.next_available) )
		{
			Alert_Message( "CI: no flow recording to send" );

			rv = CI_RESULT_NO_DATA_TO_SEND;
		}
		else
		{
			// 10/22/2013 rmd : Figure the maximum size. We are going to preceed the records with the
			// header and the system_gid.
			size_limit = sizeof(UNS_32) + (FLOW_RECORDER_MAX_RECORDS * sizeof(CS3000_FLOW_RECORDER_RECORD));

			// ----------

			if( !mem_obtain_a_block_if_available( size_limit, (void**)&(msg_handle.dptr) ) )
			{
				Alert_Message( "No Memory to send flow_recording" );

				rv = CI_RESULT_NO_MEMORY;
			}
			else
			{
				// ----------

				ucp = msg_handle.dptr;

				msg_handle.dlen = 0;

				// ----------

				// 10/22/2013 rmd : Place in the system gid that the records come from.
				memcpy( ucp, &(pbsr_ptr->system_gid), sizeof(UNS_32) );

				ucp += sizeof(UNS_32);

				msg_handle.dlen += sizeof(UNS_32);

				// ----------

				// 8/30/2013 rmd : Don't want to move the actual first_byte_to_send until the communication
				// transaction is succesful (receives a response from the comm server). So use a copy.
				pbsr_ptr->frcs.pending_first_to_send = pbsr_ptr->frcs.first_to_send;

				while( (pbsr_ptr->frcs.pending_first_to_send != pbsr_ptr->frcs.next_available) && (rv == CI_RESULT_MESSAGE_SUCCESSFUL) )
				{
					// 10/22/2013 rmd : Can we fit one more?
					if( (msg_handle.dlen + sizeof(CS3000_FLOW_RECORDER_RECORD)) > size_limit )
					{
						rv = CI_RESULT_ERROR_BUILDING_MESSAGE;

						// 10/22/2013 rmd : There's been an error. Free the message memory and exit without sending.
						mem_free( msg_handle.dptr );

						// 10/22/2013 rmd : This should never happen. Make an alert line.
						Alert_Message( "CI: flow recording to commserver - too many!" );
					}
					else
					{
						// 8/30/2013 rmd : Copy the record at the pointer.
						memcpy( ucp, pbsr_ptr->frcs.pending_first_to_send, sizeof(CS3000_FLOW_RECORDER_RECORD) );

						ucp += sizeof(CS3000_FLOW_RECORDER_RECORD);

						msg_handle.dlen += sizeof(CS3000_FLOW_RECORDER_RECORD);

						// ----------

						// 10/22/2013 rmd : Bump to the next line to send.
						nm_flow_recorder_inc_pointer( &(pbsr_ptr->frcs.pending_first_to_send), pbsr_ptr->frcs.original_allocation );
					}

				}   

				if( rv == CI_RESULT_MESSAGE_SUCCESSFUL )
				{
					// 10/22/2013 rmd : Mark the pending first to send to prevent another pass through this
					// function prior to the message actually being sent. We wait till we see the acknowledge
					// before we update the actual first_byte_to_send variable.
					pbsr_ptr->frcs.pending_first_to_send_in_use = (true);

					// ----------

					// 8/30/2013 rmd : Send it!
					set_now_xmitting_and_send_the_msg( msg_handle, &cics.waiting_for_flow_recording_response, &pbsr_ptr->frcs, MID_TO_COMMSERVER_FLOW_RECORDING_init_packet, MID_TO_COMMSERVER_FLOW_RECORDING_data_packet );
				}

			}  // of if no memory

		}  // of if no data to send

	}  // of if message in process

	// ----------

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	// ----------

	return( rv );
}

static void send_flow_recording( BY_SYSTEM_RECORD *bsr_ptr )
{
	UNS_32  ci_result;

	// ----------

	if( !sending_registration_instead( CI_MESSAGE_send_flow_recording, bsr_ptr ) )
	{
		// 8/25/2014 rmd : Attempt to send. Then evaluate the function return value to determine if
		// timer should be restarted.
		ci_result = send_ci_flow_recording( bsr_ptr );

		// 8/25/2014 rmd : The only time we should restart the timer would be if the infrastructure
		// was in use. This would mean the timer fired (or a manual attempt was initiated) while a
		// message was either on the ci msg queue or actually in process. Meaning a message was made
		// and it has not yet completed. All the other reasons (except when successful) are errors.
		// And I don't want to fill the alerts with error after error. Once is good enough. And it
		// won't be once. The next time the timer fires cause a new line is made the error may
		// happen again.
		//
		// 9/11/2014 rmd : There should always be memory for flow recording. It isn't that big.
		if( (ci_result == CI_RESULT_INFRASTRUCTURE_IN_USE) || (ci_result == CI_RESULT_NO_MEMORY) )
		{
			// 8/15/2014 rmd : The most likely cause is this system already has a msg queued up on the
			// cics list (his pending flag is set).
			Alert_Message( "CI busy - flow recording timer restarted" );

			// 8/12/2014 rmd : And restart the flow recording timer to its normal 15 minute period. I
			// don't have a reason to do otherwise (use some other amount of time).
			//
			// 8/12/2014 rmd : And since the flow recording control structure may be uninitialized make
			// this simple test. FRCS should be initialized but is possible it isn't if the
			// flow_recorder_verify_allocation function has not yet been called.
			if( bsr_ptr->frcs.original_allocation != NULL )
			{
				xTimerChangePeriod( bsr_ptr->frcs.when_to_send_timer, MS_to_TICKS( CONTROLLER_INITIATED_FLOW_RECORDING_TRANSMISSION_TIMER_MS ), portMAX_DELAY );
			}
		}
		
		// ----------
		
		// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
		// controllers working on the next msg in the msg queue.
		check_if_we_need_to_post_a_build_and_send_event( ci_result );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------- M O I S T U R E   R E C O R D I N G ----------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/18/2016 rmd : NOTE - the MOISTURE RECORDER timer and its associated callback functions
// are located in the moisture sensor recorder file.

static UNS_32 send_ci_moisture_sensor_recorder_records( void )
{
	// 10/9/2013 rmd : Executed within the CONTROLLER_INITIATED task context when the send flow
	// recording timer expires. RETURNS true if a message is built and posted to the CI task.
	// RETURNS false if a message was not built. And that can happen for a number of reasons.

	// ----------

	DATA_HANDLE     msg_handle;

	UNS_32          size_limit;

	UNS_8           *ucp;

	UNS_32          rv;

	rv = CI_RESULT_MESSAGE_SUCCESSFUL;

	// ----------

	// 10/22/2013 rmd : Block any changes to the flow_recording data by taking the
	// system_preserves MUTEX.
	xSemaphoreTakeRecursive( moisture_sensor_recorder_recursive_MUTEX, portMAX_DELAY );

	// ----------

	// 8/15/2014 rmd : If THIS SYSTEM already has a message pending do not queue another message
	// onto the ci queue. The tracking of what has been sent would lead us to queueing the same
	// message twice. I also think it opens the door to some kind of a hole when the ACK is
	// received and the next to send pointer is adjusted.
	if( msrcs.pending_first_to_send_in_use )
	{
		// 8/15/2014 rmd : Not sure if this would ever occur. Would think not when thinking about
		// the timers but I suppose manually triggered could do this.
		Alert_Message( "Moisture Recording: message in process." );

		rv = CI_RESULT_INFRASTRUCTURE_IN_USE;
	}
	else
	{
		// 10/24/2013 rmd : If the frcs has been initialized and there are lines proceed. Because of
		// the way we increment the pointers, when there is a new line, the next_available ptr will
		// NOT be the same as the first_to_send pointer. Therefore if the first_to_send is not equal
		// to the next then there are records to send!
		if( (msrcs.original_allocation == NULL) || (msrcs.first_to_send == msrcs.next_available) )
		{
			Alert_Message( "CI: no moisture recording to send" );

			rv = CI_RESULT_NO_DATA_TO_SEND;
		}
		else
		{
			// 2/18/2016 rmd : Figure the maximum size. Only the raw records are sent. The server must
			// assume it is just record after record till the end of the message. Doesn't need to know
			// how many.
			size_limit = (MOISTURE_SENSOR_RECORDER_MAX_RECORDS * sizeof(MOISTURE_SENSOR_RECORDER_RECORD));

			// ----------

			if( !mem_obtain_a_block_if_available( size_limit, (void**)&(msg_handle.dptr) ) )
			{
				Alert_Message( "No Memory to send moisture recording" );

				rv = CI_RESULT_NO_MEMORY;
			}
			else
			{
				// ----------

				ucp = msg_handle.dptr;

				msg_handle.dlen = 0;

				// ----------

				// 8/30/2013 rmd : Don't want to move the actual first_byte_to_send until the communication
				// transaction is succesful (receives a response from the comm server). So use a copy.
				msrcs.pending_first_to_send = msrcs.first_to_send;

				while( (msrcs.pending_first_to_send != msrcs.next_available) && (rv == CI_RESULT_MESSAGE_SUCCESSFUL) )
				{
					// 10/22/2013 rmd : Can we fit one more?
					if( (msg_handle.dlen + sizeof(MOISTURE_SENSOR_RECORDER_RECORD)) > size_limit )
					{
						rv = CI_RESULT_ERROR_BUILDING_MESSAGE;

						// 10/22/2013 rmd : There's been an error. Free the message memory and exit without sending.
						mem_free( msg_handle.dptr );

						// 10/22/2013 rmd : This should never happen. Make an alert line.
						Alert_Message( "CI: moisture recording to commserver - too many!" );
					}
					else
					{
						// 8/30/2013 rmd : Copy the record at the pointer.
						memcpy( ucp, msrcs.pending_first_to_send, sizeof(MOISTURE_SENSOR_RECORDER_RECORD) );

						ucp += sizeof(MOISTURE_SENSOR_RECORDER_RECORD);

						msg_handle.dlen += sizeof(MOISTURE_SENSOR_RECORDER_RECORD);

						// ----------

						// 10/22/2013 rmd : Bump to the next line to send.
						nm_MOISTURE_SENSOR_RECORDER_inc_pointer( &(msrcs.pending_first_to_send), msrcs.original_allocation );
					}

				}   

				if( rv == CI_RESULT_MESSAGE_SUCCESSFUL )
				{
					// 10/22/2013 rmd : Mark the pending first to send to prevent another pass through this
					// function prior to the message actually being sent. We wait till we see the acknowledge
					// before we update the actual first_byte_to_send variable.
					msrcs.pending_first_to_send_in_use = (true);

					// ----------

					// 8/30/2013 rmd : Send it!
					set_now_xmitting_and_send_the_msg( msg_handle, &cics.waiting_for_moisture_sensor_recording_response, NULL, MID_TO_COMMSERVER_MOISTURE_SENSOR_RECORDER_init_packet, MID_TO_COMMSERVER_MOISTURE_SENSOR_RECORDER_data_packet );
				}

			}  // of if no memory

		}  // of if no data to send

	}  // of if message in process

	// ----------

	xSemaphoreGiveRecursive( moisture_sensor_recorder_recursive_MUTEX );

	// ----------

	return( rv );
}

static void send_moisture_sensor_recording( void )
{
	UNS_32  ci_result;

	// ----------

	if( !sending_registration_instead( CI_MESSAGE_send_moisture_sensor_recorder_records, NULL ) )
	{
		// 8/25/2014 rmd : Attempt to send. Then evaluate the function return value to determine if
		// timer should be restarted.
		ci_result = send_ci_moisture_sensor_recorder_records();

		// 8/25/2014 rmd : The only time we should restart the timer would be if the infrastructure
		// was in use. This would mean the timer fired (or a manual attempt was initiated) while a
		// message was either on the ci msg queue or actually in process. Meaning a message was made
		// and it has not yet completed. All the other reasons (except when successful) are errors.
		// And I don't want to fill the alerts with error after error. Once is good enough. And it
		// won't be once. The next time the timer fires cause a new line is made the error may
		// happen again.
		//
		// 9/11/2014 rmd : There should always be memory for flow recording. It isn't that big.
		if( (ci_result == CI_RESULT_INFRASTRUCTURE_IN_USE) || (ci_result == CI_RESULT_NO_MEMORY) )
		{
			// 8/15/2014 rmd : The most likely cause is this system already has a msg queued up on the
			// cics list (his pending flag is set).
			Alert_Message( "CI busy - moisture recorder timer restarted" );

			// 8/12/2014 rmd : And restart the flow recording timer to its normal 15 minute period. I
			// don't have a reason to do otherwise (use some other amount of time).
			//
			// 8/12/2014 rmd : And since the flow recording control structure may be uninitialized make
			// this simple test. FRCS should be initialized but is possible it isn't if the
			// flow_recorder_verify_allocation function has not yet been called.
			if( msrcs.original_allocation != NULL )
			{
				xTimerChangePeriod( msrcs.when_to_send_timer, MS_to_TICKS( CI_MOISTURE_SENSOR_RECORDER_TRANSMISSION_TIMER_MS ), portMAX_DELAY );
			}
		}
		
		// ----------
		
		// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
		// controllers working on the next msg in the msg queue.
		check_if_we_need_to_post_a_build_and_send_event( ci_result );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------- C O D E   U P D A T E ------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32 send_ci_check_for_updates( UNS_32 const pinit_mid, UNS_32 const pdata_mid, BOOL_32 * const pwaiting_for_flag_ptr )
{
	// 6/5/2015 rmd : This common function is used for three messages that all have the same
	// format but different mid's which in turn trigger different ACK mid's from the
	// commserver.

	DATA_HANDLE		msg_handle;

	UNS_32		main_code_date, main_code_time;

	UNS_8		*ucp;

	UNS_32		rv;

	// ----------

	rv = CI_RESULT_MESSAGE_SUCCESSFUL;

	// 5/6/2014 ajv : Since we're sending the TP Micro revision date and time,
	// it's important that we ensure the values are valid. If not, skip this
	// process and flag with an alert.
	if( tpmicro_comm.file_system_code_date_and_time_valid )
	{
		if( mem_obtain_a_block_if_available( (2 * (sizeof(16) + sizeof(32))), (void**)&(msg_handle.dptr) ) )
		{
			ucp = msg_handle.dptr;

			msg_handle.dlen = 0;

			// ----------

			// 5/6/2014 ajv : Add the main code date and time.
			main_code_date = convert_code_image_date_to_version_number( (UNS_8*)(EMC_DYCS0_BASE), CS3000_APP_FILENAME );
			memcpy( ucp, &main_code_date, sizeof(UNS_16) );
			ucp += sizeof(UNS_16);
			msg_handle.dlen += sizeof(UNS_16);

			main_code_time = convert_code_image_time_to_edit_count( (UNS_8*)(EMC_DYCS0_BASE), CS3000_APP_FILENAME );
			memcpy( ucp, &main_code_time, sizeof(UNS_32) );
			ucp += sizeof(UNS_32);
			msg_handle.dlen += sizeof(UNS_32);

			// ----------

			// 5/6/2014 ajv : Add the TP Micro date and time.
			memcpy( ucp, &tpmicro_comm.file_system_code_date, sizeof(UNS_16) );
			ucp += sizeof(UNS_16);
			msg_handle.dlen += sizeof(UNS_16);

			memcpy( ucp, &tpmicro_comm.file_system_code_time, sizeof(UNS_32) );
			ucp += sizeof(UNS_32);
			msg_handle.dlen += sizeof(UNS_32);

			// ----------

			set_now_xmitting_and_send_the_msg( msg_handle, pwaiting_for_flag_ptr, NULL, pinit_mid, pdata_mid );
		}
		else
		{
			Alert_Message( "No memory to check for updates" );

			rv = CI_RESULT_NO_MEMORY;
		}
	}
	else
	{
		Alert_Message( "Trying to check for updates with invalid date and time" );

		rv = CI_RESULT_INFRASTRUCTURE_IN_USE;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static void send_check_for_updates( void )
{
	UNS_32	ci_result;
	
	if( !sending_registration_instead( CI_MESSAGE_send_check_for_updates, NULL ) )
	{
		ci_result = send_ci_check_for_updates( MID_TO_COMMSERVER_CHECK_FOR_UPDATES_init_packet, MID_TO_COMMSERVER_CHECK_FOR_UPDATES_data_packet, &cics.waiting_for_check_for_updates_response );

		// ----------
		
		// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
		// controllers working on the next msg in the msg queue.
		check_if_we_need_to_post_a_build_and_send_event( ci_result );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* -------------- S T A T I O N   H I S T O R Y ------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/15/2014 rmd : NOTE - the associated timer and call back functions are located in the
// report_data structure and file.

static UNS_32 send_ci_station_history( void )
{
	// 10/9/2013 rmd : Executed within the CONTROLLER_INITIATED task context.

	// ----------

	DATA_HANDLE     msg_handle;

	UNS_32          size_limit;

	UNS_8           *ucp;

	UNS_32          rv;

	rv = CI_RESULT_MESSAGE_SUCCESSFUL;

	// ----------

	// 8/15/2014 rmd : Make sure the station history array stays stable.
	xSemaphoreTakeRecursive( station_history_completed_records_recursive_MUTEX, portMAX_DELAY );

	// ----------

	// 8/15/2014 rmd : If a station history message has already been prepared we will not make a
	// second one. To avoid complications and problems tracking what has been sent or not. At
	// the least we end up sending dups. At the worst we fail to send some data to the
	// comm_server.
	if( station_history_completed.rdfb.pending_first_to_send_in_use )
	{
		// 8/15/2014 rmd : Not sure if this would ever occur. Would think not when thinking about
		// the timers but I suppose manually triggered could do this.
		Alert_Message( "Station History: message in process." );

		rv = CI_RESULT_INFRASTRUCTURE_IN_USE;
	}
	else
	{
		// 8/15/2014 rmd : Are there records to send? Because of the way we increment the pointers,
		// when there is a new line, the next_available ptr will NEVER be the same as the
		// first_to_send pointer. Therefore if the first_to_send is not equal to the next then there
		// are records to send!
		if( station_history_completed.rdfb.first_to_send == station_history_completed.rdfb.index_of_next_available )
		{
			Alert_Message( "CI: no station history to send" );

			rv = CI_RESULT_NO_DATA_TO_SEND;
		}
		else
		{
			size_limit = (STATION_HISTORY_MAX_RECORDS * sizeof(STATION_HISTORY_RECORD) );

			// ----------

			if( !mem_obtain_a_block_if_available( size_limit, (void**)&(msg_handle.dptr) ) )
			{
				Alert_Message( "No Memory to send station history" );

				rv = CI_RESULT_NO_MEMORY;
			}
			else
			{
				ucp = msg_handle.dptr;

				msg_handle.dlen = 0;

				// ----------

				// 8/15/2014 rmd : Do not change first_to_send within this function. That remains unchanged
				// till we see the ACK. Safe to work with the pending_first here within this function. MUTEX
				// allows us to do this.
				station_history_completed.rdfb.pending_first_to_send = station_history_completed.rdfb.first_to_send;

				while( (station_history_completed.rdfb.pending_first_to_send != station_history_completed.rdfb.index_of_next_available) && (rv == CI_RESULT_MESSAGE_SUCCESSFUL) )
				{
					// 10/22/2013 rmd : Can we fit one more?
					if( (msg_handle.dlen + sizeof(STATION_HISTORY_RECORD)) > size_limit )
					{
						rv = CI_RESULT_ERROR_BUILDING_MESSAGE;

						// 10/22/2013 rmd : There's been an error. Free the message memory and exit without sending.
						mem_free( msg_handle.dptr );

						// 10/22/2013 rmd : This should never happen. Make an alert line.
						Alert_Message( "CI: station history to commserver - too many!" );
					}
					else
					{
						// 8/30/2013 rmd : Copy the record at the pointer.
						memcpy( ucp, &(station_history_completed.shr[ station_history_completed.rdfb.pending_first_to_send ]), sizeof(STATION_HISTORY_RECORD) );

						ucp += sizeof(STATION_HISTORY_RECORD);

						msg_handle.dlen += sizeof(STATION_HISTORY_RECORD);

						// ----------

						// 10/22/2013 rmd : Bump to the next line to send.
						nm_STATION_HISTORY_inc_index( &station_history_completed.rdfb.pending_first_to_send );
					}

				}   

				if( rv == CI_RESULT_MESSAGE_SUCCESSFUL )
				{
					// 10/22/2013 rmd : Mark the pending first to send to prevent another pass through this
					// function prior to the message actually being sent. We wait till we see the acknowledge
					// before we update the actual first_byte_to_send variable.
					station_history_completed.rdfb.pending_first_to_send_in_use = (true);

					// ----------

					// 8/30/2013 rmd : Send it!
					set_now_xmitting_and_send_the_msg( msg_handle, &cics.waiting_for_station_history_response, NULL, MID_TO_COMMSERVER_STATION_HISTORY_init_packet, MID_TO_COMMSERVER_STATION_HISTORY_data_packet );
				}

			}  // of no memory available

		}  // of there is data to send

	}  // of infrastructure not in use

	// ----------

	xSemaphoreGiveRecursive( station_history_completed_records_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static void send_station_history( void )
{
	UNS_32  ci_result;

	// ----------

	if( !sending_registration_instead( CI_MESSAGE_send_station_history, NULL ) )
	{
		// 8/25/2014 rmd : Attempt to send. Then evaluate the function return value to determine if
		// timer should be restarted.
		ci_result = send_ci_station_history();

		// 8/25/2014 rmd : The only time we should restart the timer would be if the infrastructure
		// was in use. This would mean the timer fired (or a manual attempt was initiated) while a
		// message was either on the ci msg queue or actually in process. Meaning a message was made
		// and it has not yet completed. All the other reasons (except when successful) are errors.
		// And I don't want to fill the alerts with error after error. Once is good enough. And it
		// won't be once. The next time the timer fires cause a new line is made the error may
		// happen again.
		//
		// 9/11/2014 rmd : This can also happen cause station history and station report data go
		// after the same memory block. Only one can use it at a time. So one of the two must wait
		// until the other is done.
		if( (ci_result == CI_RESULT_INFRASTRUCTURE_IN_USE) || (ci_result == CI_RESULT_NO_MEMORY) )
		{
			// 8/15/2014 rmd : The most cause of this system already has a msg queued up on the cics
			// list (his pending flag is set).
			Alert_Message( "station history timer restarted" );

			// 8/15/2014 rmd : Restart the station history timer.
			STATION_HISTORY_start_ci_timer_if_it_is_not_running__seconds( (15*60) );
		}

		// ----------
		
		// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
		// controllers working on the next msg in the msg queue.
		check_if_we_need_to_post_a_build_and_send_event( ci_result );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------- S T A T I O N  R E P O R T  D A T A ----------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/15/2014 rmd : NOTE - the associated timer and call back functions are located in the
// report_data structure and file.

static UNS_32 send_ci_station_report_data( void )
{
	// 10/9/2013 rmd : Executed within the CONTROLLER_INITIATED task context.

	// ----------

	DATA_HANDLE     msg_handle;

	UNS_32          size_limit;

	UNS_8           *ucp;

	UNS_32          rv;

	rv = CI_RESULT_MESSAGE_SUCCESSFUL;

	// ----------

	// 8/15/2014 rmd : Make sure the station history array stays stable.
	xSemaphoreTakeRecursive( station_report_data_completed_records_recursive_MUTEX, portMAX_DELAY );

	// ----------

	// 8/15/2014 rmd : If a station report_data message has already been prepared we will not
	// make a second one. To avoid complications and problems tracking what has been sent or
	// not. At the least we end up sending dups. At the worst we fail to send some data to the
	// comm_server.
	if( station_report_data_completed.rdfb.pending_first_to_send_in_use )
	{
		// 8/15/2014 rmd : Not sure if this would ever occur. Would think not when thinking about
		// the timers but I suppose manually triggered could do this.
		Alert_Message( "Station Report Data: message in process." );

		rv = CI_RESULT_INFRASTRUCTURE_IN_USE;
	}
	else
	{
		// 8/15/2014 rmd : Are there records to send? Because of the way we increment the pointers,
		// when there is a new line, the next_available ptr will NEVER be the same as the
		// first_to_send pointer. Therefore if the first_to_send is not equal to the next then there
		// are records to send!
		if( station_report_data_completed.rdfb.first_to_send == station_report_data_completed.rdfb.index_of_next_available )
		{
			Alert_Message( "CI: no station report data to send" );

			rv = CI_RESULT_NO_DATA_TO_SEND;
		}
		else
		{
			size_limit = (STATION_REPORT_DATA_MAX_RECORDS * sizeof(STATION_REPORT_DATA_RECORD) );

			// ----------

			if( !mem_obtain_a_block_if_available( size_limit, (void**)&(msg_handle.dptr) ) )
			{
				Alert_Message( "No Memory to send station report data" );

				rv = CI_RESULT_NO_MEMORY;
			}
			else
			{
				ucp = msg_handle.dptr;

				msg_handle.dlen = 0;

				// ----------

				// 8/15/2014 rmd : Do not change first_to_send within this function. That remains unchanged
				// till we see the ACK. Safe to work with the pending_first here within this function. MUTEX
				// allows us to do this.
				station_report_data_completed.rdfb.pending_first_to_send = station_report_data_completed.rdfb.first_to_send;

				while( (station_report_data_completed.rdfb.pending_first_to_send != station_report_data_completed.rdfb.index_of_next_available) && (rv == CI_RESULT_MESSAGE_SUCCESSFUL) )
				{
					// 10/22/2013 rmd : Can we fit one more?
					if( (msg_handle.dlen + sizeof(STATION_REPORT_DATA_RECORD)) > size_limit )
					{
						rv = CI_RESULT_ERROR_BUILDING_MESSAGE;

						// 10/22/2013 rmd : There's been an error. Free the message memory and exit without sending.
						mem_free( msg_handle.dptr );

						// 10/22/2013 rmd : This should never happen. Make an alert line.
						Alert_Message( "CI: station report data to commserver - too many!" );
					}
					else
					{
						// 8/30/2013 rmd : Copy the record at the pointer.
						memcpy( ucp, &(station_report_data_completed.srdr[ station_report_data_completed.rdfb.pending_first_to_send ]), sizeof(STATION_REPORT_DATA_RECORD) );

						ucp += sizeof(STATION_REPORT_DATA_RECORD);

						msg_handle.dlen += sizeof(STATION_REPORT_DATA_RECORD);

						// ----------

						// 10/22/2013 rmd : Bump to the next line to send.
						nm_STATION_REPORT_DATA_inc_index( &station_report_data_completed.rdfb.pending_first_to_send );
					}

				}   

				if( rv == CI_RESULT_MESSAGE_SUCCESSFUL )
				{
					// 10/22/2013 rmd : Mark the pending first to send to prevent another pass through this
					// function prior to the message actually being sent. We wait till we see the acknowledge
					// before we update the actual first_byte_to_send variable.
					station_report_data_completed.rdfb.pending_first_to_send_in_use = (true);

					// ----------

					// 8/30/2013 rmd : Send it!
					set_now_xmitting_and_send_the_msg( msg_handle, &cics.waiting_for_station_report_data_response, NULL, MID_TO_COMMSERVER_STATION_REPORT_DATA_init_packet, MID_TO_COMMSERVER_STATION_REPORT_DATA_data_packet );
				}

			}  // of no memory available

		}  // of there is data to send

	}  // of infrastructure not in use

	// ----------

	xSemaphoreGiveRecursive( station_report_data_completed_records_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static void send_station_report_data( void )
{
	UNS_32  ci_result;

	if( !sending_registration_instead( CI_MESSAGE_send_station_report_data, NULL ) )
	{
		// 8/25/2014 rmd : Attempt to send. Then evaluate the function return value to determine if
		// timer should be restarted.
		ci_result = send_ci_station_report_data();

		// 8/25/2014 rmd : The only time we should restart the timer would be if the infrastructure
		// was in use. This would mean the timer fired (or a manual attempt was initiated) while a
		// message was either on the ci msg queue or actually in process. Meaning a message was made
		// and it has not yet completed. All the other reasons (except when successful) are errors.
		// And I don't want to fill the alerts with error after error. Once is good enough. And it
		// won't be once. The next time the timer fires cause a new line is made the error may
		// happen again.
		//
		// 9/11/2014 rmd : This can also happen cause station history and station report data go
		// after the same memory block. Only one can use it at a time. So one of the two must wait
		// until the other is done.
		if( (ci_result == CI_RESULT_INFRASTRUCTURE_IN_USE) || (ci_result == CI_RESULT_NO_MEMORY) )
		{
			// 8/15/2014 rmd : The most cause of this system already has a msg queued up on the cics
			// list (his pending flag is set).
			Alert_Message( "station report data timer restarted" );

			// 8/15/2014 rmd : Restart the station history timer.
			STATION_REPORT_DATA_start_the_ci_timer_if_it_is_not_running();
		}

		// ----------
		
		// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
		// controllers working on the next msg in the msg queue.
		check_if_we_need_to_post_a_build_and_send_event( ci_result );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* -------------- P O C  R E P O R T  D A T A --------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/15/2014 rmd : NOTE - the associated timer and call back functions are located in the
// poc_report_data structure and file.

static UNS_32 send_ci_poc_report_data( void )
{
	// 10/9/2013 rmd : Executed within the CONTROLLER_INITIATED task context.

	// ----------

	DATA_HANDLE     msg_handle;

	UNS_32          size_limit;

	UNS_8           *ucp;

	UNS_32          rv;

	rv = CI_RESULT_MESSAGE_SUCCESSFUL;

	// ----------

	// 10/2/2014 rmd : Make sure the poc_report_data array stays stable.
	xSemaphoreTakeRecursive( poc_report_completed_records_recursive_MUTEX, portMAX_DELAY );

	// ----------

	// 10/2/2014 rmd : If a poc_report_data message has already been prepared we will not make a
	// second one. To avoid complications and problems tracking what has been sent or not. At
	// the least we end up sending dups. At the worst we fail to send some data to the
	// comm_server.
	if( poc_report_data_completed.rdfb.pending_first_to_send_in_use )
	{
		// 10/2/2014 rmd : Not sure if this would ever occur. Would think not when thinking about
		// the timers but I suppose manually triggered could do this.
		Alert_Message( "POC Report Data: message in process." );

		rv = CI_RESULT_INFRASTRUCTURE_IN_USE;
	}
	else
	{
		// 10/2/2014 rmd : Are there records to send? Because of the way we increment the pointers,
		// when there is a new line, the next_available ptr will NEVER be the same as the
		// first_to_send pointer. Therefore if the first_to_send is not equal to the next then there
		// are records to send!
		if( poc_report_data_completed.rdfb.first_to_send == poc_report_data_completed.rdfb.index_of_next_available )
		{
			Alert_Message( "CI: no poc report data to send" );

			rv = CI_RESULT_NO_DATA_TO_SEND;
		}
		else
		{
			size_limit = (POC_REPORT_RECORDS_TO_KEEP * sizeof(POC_REPORT_RECORD) );

			// ----------

			if( !mem_obtain_a_block_if_available( size_limit, (void**)&(msg_handle.dptr) ) )
			{
				Alert_Message( "No Memory to send poc report data" );

				rv = CI_RESULT_NO_MEMORY;
			}
			else
			{
				ucp = msg_handle.dptr;

				msg_handle.dlen = 0;

				// ----------

				// 10/2/2014 rmd : Do not change first_to_send within this function. That remains unchanged
				// till we see the ACK. Safe to work with the pending_first here within this function. MUTEX
				// allows us to do this.
				poc_report_data_completed.rdfb.pending_first_to_send = poc_report_data_completed.rdfb.first_to_send;

				while( (poc_report_data_completed.rdfb.pending_first_to_send != poc_report_data_completed.rdfb.index_of_next_available) && (rv == CI_RESULT_MESSAGE_SUCCESSFUL) )
				{
					// 10/22/2013 rmd : Can we fit one more?
					if( (msg_handle.dlen + sizeof(POC_REPORT_RECORD)) > size_limit )
					{
						rv = CI_RESULT_ERROR_BUILDING_MESSAGE;

						// 10/22/2013 rmd : There's been an error. Free the message memory and exit without sending.
						mem_free( msg_handle.dptr );

						// 10/22/2013 rmd : This should never happen. Make an alert line.
						Alert_Message( "CI: poc_report_data to commserver - too many!" );
					}
					else
					{
						// 10/2/2014 rmd : Copy the record at the pointer.
						memcpy( ucp, &(poc_report_data_completed.prr[ poc_report_data_completed.rdfb.pending_first_to_send ]), sizeof(POC_REPORT_RECORD) );

						ucp += sizeof(POC_REPORT_RECORD);

						msg_handle.dlen += sizeof(POC_REPORT_RECORD);

						// ----------

						// 10/22/2013 rmd : Bump to the next line to send.
						nm_POC_REPORT_DATA_inc_index( &poc_report_data_completed.rdfb.pending_first_to_send );
					}

				}   

				if( rv == CI_RESULT_MESSAGE_SUCCESSFUL )
				{
					// 10/2/2014 rmd : Mark the pending first to send to prevent another pass through this
					// function prior to the message actually being sent. We wait till we see the acknowledge
					// before we update the actual first_byte_to_send variable.
					poc_report_data_completed.rdfb.pending_first_to_send_in_use = (true);

					// ----------

					// 10/2/2014 rmd : Send it!
					set_now_xmitting_and_send_the_msg( msg_handle, &cics.waiting_for_poc_report_data_response, NULL, MID_TO_COMMSERVER_POC_REPORT_DATA_init_packet, MID_TO_COMMSERVER_POC_REPORT_DATA_data_packet );
				}

			}  // of no memory available

		}  // of there is data to send

	}  // of infrastructure not in use

	// ----------

	xSemaphoreGiveRecursive( poc_report_completed_records_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static void send_poc_report_data( void )
{
	UNS_32  ci_result;
	
	if( !sending_registration_instead( CI_MESSAGE_send_poc_report_data, NULL ) )
	{
		// 8/25/2014 rmd : Attempt to send. Then evaluate the function return value to determine if
		// timer should be restarted.
		ci_result = send_ci_poc_report_data();

		// 8/25/2014 rmd : The only time we should restart the timer would be if the infrastructure
		// was in use. This would mean the timer fired (or a manual attempt was initiated) while a
		// message was either on the ci msg queue or actually in process. Meaning a message was made
		// and it has not yet completed. All the other reasons (except when successful) are errors.
		// And I don't want to fill the alerts with error after error. Once is good enough. And it
		// won't be once. The next time the timer fires cause a new line is made the error may
		// happen again.
		//
		// 9/11/2014 rmd : This can also happen cause station history and station report data go
		// after the same memory block. Only one can use it at a time. So one of the two must wait
		// until the other is done.
		if( (ci_result == CI_RESULT_INFRASTRUCTURE_IN_USE) || (ci_result == CI_RESULT_NO_MEMORY) )
		{
			// 8/15/2014 rmd : The most cause of this system already has a msg queued up on the cics
			// list (his pending flag is set).
			Alert_Message( "poc report data timer restarted" );

			// 8/15/2014 rmd : Restart the station history timer.
			POC_REPORT_DATA_start_the_ci_timer_if_it_is_not_running();
		}

		// ----------
		
		// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
		// controllers working on the next msg in the msg queue.
		check_if_we_need_to_post_a_build_and_send_event( ci_result );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ----------- B U D G E T  R E P O R T  D A T A ------------ */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 02/22/2016 skc : NOTE - the associated timer and call back functions are located in the
// budget_report_data structure and file.

static UNS_32 send_ci_budget_report_data( void )
{
	// 02/22/2016 skc : Executed within the CONTROLLER_INITIATED task context.

	DATA_HANDLE     msg_handle;
	UNS_32          size_limit;
	UNS_8           *ucp;
	UNS_32          rv;

	rv = CI_RESULT_MESSAGE_SUCCESSFUL;

	// ----------

	// 02/22/206 skc : Make sure the budget_report_data array stays stable.
	xSemaphoreTakeRecursive( budget_report_completed_records_recursive_MUTEX, portMAX_DELAY );

	// ----------

	// 02/22/2016 skc : If a budget_report_data message has already been prepared we will not make a
	// second one. To avoid complications and problems tracking what has been sent or not. At
	// the least we end up sending dups. At the worst we fail to send some data to the
	// comm_server.
	if( budget_report_data_completed.rdfb.pending_first_to_send_in_use )
	{
		// 02/22/2016 skc : Not sure if this would ever occur. Would think not when thinking about
		// the timers but I suppose manually triggered could do this.
		Alert_Message( "Budget Report Data: message in process." );

		rv = CI_RESULT_INFRASTRUCTURE_IN_USE;
	}
	else
	{
		// 02/22/2016 skc : Are there records to send? Because of the way we increment the pointers,
		// when there is a new line, the next_available ptr will NEVER be the same as the
		// first_to_send pointer. Therefore if the first_to_send is not equal to the next then there
		// are records to send!
		if( budget_report_data_completed.rdfb.first_to_send == budget_report_data_completed.rdfb.index_of_next_available )
		{
			Alert_Message( "CI: no budget report data to send" );

			rv = CI_RESULT_NO_DATA_TO_SEND;
		}
		else
		{
			size_limit = (BUDGET_REPORT_RECORDS_TO_KEEP * sizeof(BUDGET_REPORT_RECORD) );

			// ----------

			if( !mem_obtain_a_block_if_available( size_limit, (void**)&(msg_handle.dptr) ) )
			{
				Alert_Message( "No Memory to send budget report data" );

				rv = CI_RESULT_NO_MEMORY;
			}
			else
			{
				ucp = msg_handle.dptr;

				msg_handle.dlen = 0;

				// ----------

				// 02/22/2016 skc : Do not change first_to_send within this function. That remains unchanged
				// till we see the ACK. Safe to work with the pending_first here within this function. MUTEX
				// allows us to do this.
				budget_report_data_completed.rdfb.pending_first_to_send = budget_report_data_completed.rdfb.first_to_send;

				while( (budget_report_data_completed.rdfb.pending_first_to_send != budget_report_data_completed.rdfb.index_of_next_available) && 
					   (rv == CI_RESULT_MESSAGE_SUCCESSFUL) )
				{
					// 02/22/2016 skc : Can we fit one more?
					if( (msg_handle.dlen + sizeof(BUDGET_REPORT_RECORD)) > size_limit )
					{
						rv = CI_RESULT_ERROR_BUILDING_MESSAGE;

						// 10/22/2013 rmd : There's been an error. Free the message memory and exit without sending.
						mem_free( msg_handle.dptr );

						// 02/22/2016 skc : This should never happen. Make an alert line.
						Alert_Message( "CI: budget_report_data to commserver - too many!" );
					}
					else
					{
						// 02/22/2016 skc : Copy the record at the pointer.
						memcpy( ucp, &(budget_report_data_completed.brr[ budget_report_data_completed.rdfb.pending_first_to_send ]), sizeof(BUDGET_REPORT_RECORD) );

						ucp += sizeof(BUDGET_REPORT_RECORD);

						msg_handle.dlen += sizeof(BUDGET_REPORT_RECORD);

						// ----------

						// 02/22/2016 skc : Bump to the next line to send.
						nm_BUDGET_REPORT_DATA_inc_index( &budget_report_data_completed.rdfb.pending_first_to_send );
					}

				}   

				if( rv == CI_RESULT_MESSAGE_SUCCESSFUL )
				{
					// 02/22/2016 skc : Mark the pending first to send to prevent another pass through this
					// function prior to the message actually being sent. We wait till we see the acknowledge
					// before we update the actual first_byte_to_send variable.
					budget_report_data_completed.rdfb.pending_first_to_send_in_use = (true);

					// ----------

					// 10/2/2014 rmd : Send it!
					set_now_xmitting_and_send_the_msg(	msg_handle,
														&cics.waiting_for_budget_report_data_response, NULL, 
														MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet, 
														MID_TO_COMMSERVER_BUDGET_REPORT_DATA_data_packet );
				}

			}  // of no memory available

		}  // of there is data to send

	}  // of infrastructure not in use

	// ----------

	xSemaphoreGiveRecursive( budget_report_completed_records_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static void send_budget_report_data( void )
{
	UNS_32  ci_result;
	
	if( !sending_registration_instead( CI_MESSAGE_send_budget_report_data, NULL ) )
	{
		// 02/22/2016 skc : Attempt to send. Then evaluate the function return value to determine if
		// timer should be restarted.
		ci_result = send_ci_budget_report_data();

		// 8/25/2014 rmd : The only time we should restart the timer would be if the infrastructure
		// was in use. This would mean the timer fired (or a manual attempt was initiated) while a
		// message was either on the ci msg queue or actually in process. Meaning a message was made
		// and it has not yet completed. All the other reasons (except when successful) are errors.
		// And I don't want to fill the alerts with error after error. Once is good enough. And it
		// won't be once. The next time the timer fires cause a new line is made the error may
		// happen again.
		//
		// 9/11/2014 rmd : This can also happen cause station history and station report data go
		// after the same memory block. Only one can use it at a time. So one of the two must wait
		// until the other is done.
		if( (ci_result == CI_RESULT_INFRASTRUCTURE_IN_USE) || (ci_result == CI_RESULT_NO_MEMORY) )
		{
			// 02/22/2016 skc rmd : The most cause of this system already has a msg queued up on the cics
			// list (his pending flag is set).
			Alert_Message( "budget report data timer restarted" );

			// 8/15/2014 rmd : Restart the station history timer.
			BUDGET_REPORT_DATA_start_the_ci_timer_if_it_is_not_running();
		}

		// ----------
		
		// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
		// controllers working on the next msg in the msg queue.
		check_if_we_need_to_post_a_build_and_send_event( ci_result );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ------------ S Y S T E M  R E P O R T  D A T A ----------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/15/2014 rmd : NOTE - the associated timer and call back functions are located in the
// system_report_data structure and file.

static UNS_32 send_ci_system_report_data( void )
{
	// 10/9/2013 rmd : Executed within the CONTROLLER_INITIATED task context.

	// ----------

	DATA_HANDLE     msg_handle;

	UNS_32          size_limit;

	UNS_8           *ucp;

	UNS_32          rv;

	rv = CI_RESULT_MESSAGE_SUCCESSFUL;

	// ----------

	// 10/2/2014 rmd : Make sure the system_report_data array stays stable.
	xSemaphoreTakeRecursive( system_report_completed_records_recursive_MUTEX, portMAX_DELAY );

	// ----------

	// 10/2/2014 rmd : If a system_report_data message has already been prepared we will not
	// make a second one. To avoid complications and problems tracking what has been sent or
	// not. At the least we end up sending dups. At the worst we fail to send some data to the
	// comm_server.
	if( system_report_data_completed.rdfb.pending_first_to_send_in_use )
	{
		// 10/2/2014 rmd : Not sure if this would ever occur. Would think not when thinking about
		// the timers but I suppose manually triggered could do this.
		Alert_Message( "SYSTEM Report Data: message in process." );

		rv = CI_RESULT_INFRASTRUCTURE_IN_USE;
	}
	else
	{
		// 10/2/2014 rmd : Are there records to send? Because of the way we increment the pointers,
		// when there is a new line, the next_available ptr will NEVER be the same as the
		// first_to_send pointer. Therefore if the first_to_send is not equal to the next then there
		// are records to send!
		if( system_report_data_completed.rdfb.first_to_send == system_report_data_completed.rdfb.index_of_next_available )
		{
			Alert_Message( "CI: no system report data to send" );

			rv = CI_RESULT_NO_DATA_TO_SEND;
		}
		else
		{
			size_limit = (SYSTEM_REPORT_RECORDS_TO_KEEP * sizeof(SYSTEM_REPORT_RECORD) );

			// ----------

			if( !mem_obtain_a_block_if_available( size_limit, (void**)&(msg_handle.dptr) ) )
			{
				Alert_Message( "No Memory to send system report data" );

				rv = CI_RESULT_NO_MEMORY;
			}
			else
			{
				ucp = msg_handle.dptr;

				msg_handle.dlen = 0;

				// ----------

				// 10/2/2014 rmd : Do not change first_to_send within this function. That remains unchanged
				// till we see the ACK. Safe to work with the pending_first here within this function. MUTEX
				// allows us to do this.
				system_report_data_completed.rdfb.pending_first_to_send = system_report_data_completed.rdfb.first_to_send;

				while( (system_report_data_completed.rdfb.pending_first_to_send != system_report_data_completed.rdfb.index_of_next_available) && (rv == CI_RESULT_MESSAGE_SUCCESSFUL) )
				{
					// 10/22/2013 rmd : Can we fit one more?
					if( (msg_handle.dlen + sizeof(SYSTEM_REPORT_RECORD)) > size_limit )
					{
						rv = CI_RESULT_ERROR_BUILDING_MESSAGE;

						// 10/22/2013 rmd : There's been an error. Free the message memory and exit without sending.
						mem_free( msg_handle.dptr );

						// 10/22/2013 rmd : This should never happen. Make an alert line.
						Alert_Message( "CI: system_report_data to commserver - too many!" );
					}
					else
					{
						// 10/2/2014 rmd : Copy the record at the pointer.
						memcpy( ucp, &(system_report_data_completed.srr[ system_report_data_completed.rdfb.pending_first_to_send ]), sizeof(SYSTEM_REPORT_RECORD) );

						ucp += sizeof(SYSTEM_REPORT_RECORD);

						msg_handle.dlen += sizeof(SYSTEM_REPORT_RECORD);

						// ----------

						// 10/22/2013 rmd : Bump to the next line to send.
						nm_SYSTEM_REPORT_DATA_inc_index( &system_report_data_completed.rdfb.pending_first_to_send );
					}

				}   

				if( rv == CI_RESULT_MESSAGE_SUCCESSFUL )
				{
					// 10/2/2014 rmd : Mark the pending first to send to prevent another pass through this
					// function prior to the message actually being sent. We wait till we see the acknowledge
					// before we update the actual first_byte_to_send variable.
					system_report_data_completed.rdfb.pending_first_to_send_in_use = (true);

					// ----------

					// 10/2/2014 rmd : Send it!
					set_now_xmitting_and_send_the_msg( msg_handle, &cics.waiting_for_system_report_data_response, NULL, MID_TO_COMMSERVER_SYSTEM_REPORT_DATA_init_packet, MID_TO_COMMSERVER_SYSTEM_REPORT_DATA_data_packet );
				}

			}  // of no memory available

		}  // of there is data to send

	}  // of infrastructure not in use

	// ----------

	xSemaphoreGiveRecursive( system_report_completed_records_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static void send_system_report_data( void )
{
	UNS_32  ci_result;

	if( !sending_registration_instead( CI_MESSAGE_send_system_report_data, NULL ) )
	{
		// 8/25/2014 rmd : Attempt to send. Then evaluate the function return value to determine if
		// timer should be restarted.
		ci_result = send_ci_system_report_data();

		// 8/25/2014 rmd : The only time we should restart the timer would be if the infrastructure
		// was in use. This would mean the timer fired (or a manual attempt was initiated) while a
		// message was either on the ci msg queue or actually in process. Meaning a message was made
		// and it has not yet completed. All the other reasons (except when successful) are errors.
		// And I don't want to fill the alerts with error after error. Once is good enough. And it
		// won't be once. The next time the timer fires cause a new line is made the error may
		// happen again.
		//
		// 9/11/2014 rmd : This can also happen cause station history and station report data go
		// after the same memory block. Only one can use it at a time. So one of the two must wait
		// until the other is done.
		if( (ci_result == CI_RESULT_INFRASTRUCTURE_IN_USE) || (ci_result == CI_RESULT_NO_MEMORY) )
		{
			// 8/15/2014 rmd : The most cause of this system already has a msg queued up on the cics
			// list (his pending flag is set).
			Alert_Message( "system report data timer restarted" );

			// 8/15/2014 rmd : Restart the station history timer.
			SYSTEM_REPORT_DATA_start_the_ci_timer_if_it_is_not_running();
		}

		// ----------
		
		// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
		// controllers working on the next msg in the msg queue.
		check_if_we_need_to_post_a_build_and_send_event( ci_result );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ------------ E T  AND  R A I N  T A B L E S -------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static UNS_32 send_ci_et_rain_tables( void )
{
	// 10/9/2013 rmd : Executed within the CONTROLLER_INITIATED task context.

	// ----------

	DATA_HANDLE     msg_handle;

	UNS_32          size_limit;

	UNS_8           *ucp;

	UNS_32          rv;

	UNS_32			i;

	rv = CI_RESULT_MESSAGE_SUCCESSFUL;

	// ----------

	// 10/2/2014 rmd : Make sure the system_report_data array stays stable.
	xSemaphoreTakeRecursive( weather_tables_recursive_MUTEX, portMAX_DELAY );

	// ----------

	// 10/28/2015 rmd : Not allowed to send if one already in process.
	if( weather_tables.pending_records_to_send_in_use )
	{
		Alert_Message( "ET/RAIN Tables: message in process." );

		rv = CI_RESULT_INFRASTRUCTURE_IN_USE;
	}
	else
	{
		// 10/28/2015 rmd : Are there table entries to send?
		if( weather_tables.records_to_send == 0 )
		{
			Alert_Message( "CI: no et/rain tables entries to send" );

			rv = CI_RESULT_NO_DATA_TO_SEND;
		}
		else
		{
			// 10/29/2015 rmd : We send the number of records UNS_32 + the date UNS_32 + the records.
			size_limit = sizeof(UNS_32) + sizeof(UNS_32) + ( weather_tables.records_to_send * (sizeof(ET_TABLE_ENTRY) + sizeof(RAIN_TABLE_ENTRY)) );

			// ----------

			if( !mem_obtain_a_block_if_available( size_limit, (void**)&(msg_handle.dptr) ) )
			{
				Alert_Message( "No Memory to send et/rain table" );

				rv = CI_RESULT_NO_MEMORY;
			}
			else
			{
				ucp = msg_handle.dptr;

				msg_handle.dlen = 0;

				// ----------

				// 10/28/2015 rmd : Place in the count and the table date.
				memcpy( ucp, &weather_tables.records_to_send, sizeof(UNS_32) );
				ucp += sizeof(UNS_32);
				msg_handle.dlen += sizeof(UNS_32);
				
				memcpy( ucp, &weather_tables.et_rain_table_date, sizeof(UNS_32) );
				ucp += sizeof(UNS_32);
				msg_handle.dlen += sizeof(UNS_32);
				
				for( i=0; i<weather_tables.records_to_send; i++ )
				{
					memcpy( ucp, &weather_tables.et_table[ i ], sizeof(ET_TABLE_ENTRY) );
					ucp += sizeof(ET_TABLE_ENTRY);
					msg_handle.dlen += sizeof(ET_TABLE_ENTRY);

					memcpy( ucp, &weather_tables.rain_table[ i ], sizeof(RAIN_TABLE_ENTRY) );
					ucp += sizeof(RAIN_TABLE_ENTRY);
					msg_handle.dlen += sizeof(RAIN_TABLE_ENTRY);
				}
				
				// ----------
				
				if( rv == CI_RESULT_MESSAGE_SUCCESSFUL )
				{
					// 10/2/2014 rmd : Mark the pending first to send to prevent another pass through this
					// function prior to the message actually being sent. We wait till we see the acknowledge
					// before we update the actual first_byte_to_send variable.
					weather_tables.pending_records_to_send_in_use = (true);
					
					// ----------

					// 10/2/2014 rmd : Send it!
					set_now_xmitting_and_send_the_msg( msg_handle, &cics.waiting_for_et_rain_tables_response, NULL, MID_TO_COMMSERVER_ET_AND_RAIN_TABLE_init_packet, MID_TO_COMMSERVER_ET_AND_RAIN_TABLE_data_packet );
				}

			}  // of no memory available

		}  // of there is data to send

	}  // of infrastructure not in use

	// ----------

	xSemaphoreGiveRecursive( weather_tables_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static void send_et_rain_tables( void )
{
	UNS_32  ci_result;

	// ----------

	if( !sending_registration_instead( CI_MESSAGE_send_et_rain_tables, NULL ) )
	{
		// 10/28/2015 rmd : Attempt to send the requested et and rain table entries.
		ci_result = send_ci_et_rain_tables();

		// 10/29/2015 rmd : The NO_DATA_TO_SEND condition is already alerted about in the msg build
		// function. Only test for IN-USE or NO_MEMORY conditions here.
		if( (ci_result == CI_RESULT_INFRASTRUCTURE_IN_USE) || (ci_result == CI_RESULT_NO_MEMORY) )
		{
			// 8/15/2014 rmd : The most cause of this system already has a msg queued up on the cics
			// list (his pending flag is set).
			Alert_Message( "couldn't send et/rain table entries" );
		}

		// ----------
		
		// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
		// controllers working on the next msg in the msg queue.
		check_if_we_need_to_post_a_build_and_send_event( ci_result );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* --------------- W E A T H E R   D A T A ------------------ */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void send_weather_data_timer_callback( xTimerHandle pxTimer )
{
	// 10/2/2014 rmd : Remember this is executed within the context of the high priority timer
	// task. And is executed generally shortly after the ET and RAIN tables roll (8pm).

	// 6/16/2015 rmd : In the face of an error we end up retrying. If however it is past 8:10pm
	// it is not worth trying anymore. So do not post the event.
	DATE_TIME   ldt;

	EPSON_obtain_latest_time_and_date( &ldt );

	// 6/16/2015 rmd : Is it less than 8:10PM? Why 8:10? Because that is when the commserver
	// shares weather. And if this hasn't arrived by then it won't do any good anyway.
	if( ldt.T < 72600 )
	{
		// 9/5/2017 rmd : Remember this is executed within the context of the high priority timer
		// task. And is executed when it it time to send the et and rain data.
		CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_weather_data, NULL, CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD, CI_POST_TO_BACK );
	}
}

static void start_send_weather_data_timer_if_it_is_not_running( void )
{
	// 6/16/2015 rmd : If it exists do what we came for. Start the timer.
	if( cics.send_weather_data_timer != NULL )
	{
		// 6/16/2015 rmd : Only if it is not running. So we can't possibly postpone by
		// restarting.
		if( xTimerIsTimerActive( cics.send_weather_data_timer ) == pdFALSE )
		{
			// 6/16/2015 rmd : Start the 15 second timer to trigger the controller initiated send.
			xTimerStart( cics.send_weather_data_timer, portMAX_DELAY );
		}
	}
}

static UNS_32 send_ci_weather_data( void )
{
	DATA_HANDLE         msg_handle;

	ET_TABLE_ENTRY      et_data;

	RAIN_TABLE_ENTRY    rain_data;

	UNS_8       whats_included;

	UNS_8       *ucp;

	UNS_32		rv;
	
	rv = CI_RESULT_MESSAGE_SUCCESSFUL;
	
	// 9/2/2014 ajv : Each data packet contains the an 8-bit bitfield which
	// indicates what type of data is being sent, ET inches, ET status, rain
	// inches, and rain status.
	if( mem_obtain_a_block_if_available( (sizeof(UNS_8) + sizeof(ET_TABLE_ENTRY) + sizeof(RAIN_TABLE_ENTRY)), (void**)&(msg_handle.dptr) ) )
	{
		ucp = msg_handle.dptr;

		msg_handle.dlen = 0;

		// ----------

		// 9/26/2014 ajv : Initialize whats_included to ensure it's all 0's
		// before we set any bits.
		whats_included = 0x00;

		// ----------

		// 9/15/2014 ajv : Determine what data is going to be included - it may
		// be ET data, rain data, both, or neither.
		if( WEATHER_get_et_gage_is_in_use() )
		{
			B_SET( whats_included, WEATHER_DATA_HAS_ET );
		}

		if( WEATHER_get_rain_bucket_is_in_use() )
		{
			B_SET( whats_included, WEATHER_DATA_HAS_RAIN );
		}

		memcpy( ucp, &whats_included, sizeof(UNS_8) );
		ucp += sizeof(UNS_8);
		msg_handle.dlen += sizeof(UNS_8);

		if( B_IS_SET( whats_included, WEATHER_DATA_HAS_ET ) )
		{
			WEATHER_TABLES_get_et_table_entry_for_index( 1, &et_data );
			
			// ----------
			
			// 7/22/2015 rmd : If the value was succesfully developed using the gage then set the status
			// to SHARED so that when the commserver distributes to other controllers this will be the
			// status setting. If not from the gage we leave the status alone. And is what the
			// controllers shared to will receieve. The comm_server acts as only a pass through
			// entitity.
			if( et_data.status == ET_STATUS_FROM_ET_GAGE )
			{
				et_data.status = ET_STATUS_SHARED_FROM_THE_CENTRAL;
			}

			// ----------
			
			memcpy( ucp, &et_data, sizeof(ET_TABLE_ENTRY) );
			ucp += sizeof(ET_TABLE_ENTRY);
			msg_handle.dlen += sizeof(ET_TABLE_ENTRY);
		}

		if( B_IS_SET( whats_included, WEATHER_DATA_HAS_RAIN ) )
		{
			WEATHER_TABLES_get_rain_table_entry_for_index( 1, &rain_data );

			// ----------
			
			// 7/22/2015 rmd : If the rain has crossed the minimum (is an R) then flip the status to
			// SHARED. If it is below the minimum we must leave the status alone. Why? Because when the
			// status is BELOW_MIN and there is a number there (some rain fell) if we were to flip the
			// status to SHARED we would take that as RAIN when reveived by a controller. We in effect
			// lose the ability to distinguish the amount as below minimum or above. So the funny thing
			// about this is even though a 0 BELOW MINIMUM value is shared it shows in the receiving
			// controller as just that - 0 BELOW_MINIMUM even though it is actually a shared value.
			// (Remember - the comm server will share whatever status we deliver to it.)
			if( rain_data.status == RAIN_STATUS_FROM_RAIN_BUCKET_R )
			{
				rain_data.status = RAIN_STATUS_SHARED_FROM_THE_CENTRAL;
			}

			// ----------
			
			memcpy( ucp, &rain_data, sizeof(RAIN_TABLE_ENTRY) );
			ucp += sizeof(RAIN_TABLE_ENTRY);
			msg_handle.dlen += sizeof(RAIN_TABLE_ENTRY);
		}

		// ----------

		set_now_xmitting_and_send_the_msg( msg_handle, &cics.waiting_for_weather_data_receipt_response, NULL, MID_TO_COMMSERVER_WEATHER_DATA_init_packet, MID_TO_COMMSERVER_WEATHER_DATA_data_packet );
	}
	else
	{
		Alert_Message( "No memory to send weather data" );

		rv = CI_RESULT_NO_MEMORY;
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
static void send_weather_data( void )
{
	UNS_32	ci_result;
	
	if( !sending_registration_instead( CI_MESSAGE_send_weather_data, NULL ) )
	{
		ci_result = send_ci_weather_data();
		
		// 3/1/2017 rmd : The only thing that can go wrong is no memory.
		if( ci_result == CI_RESULT_NO_MEMORY )
		{
			// 6/16/2015 rmd : Try again later.
			CONTROLLER_INITIATED_post_event( CI_EVENT_start_send_weather_data_timer );
		}
		
		// ----------
		
		// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
		// controllers working on the next msg in the msg queue.
		check_if_we_need_to_post_a_build_and_send_event( ci_result );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ------------- R A I N   I N D I C A T I O N -------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void send_rain_indication_timer_callback( xTimerHandle pxTimer )
{
	// 10/2/2014 rmd : Remember this is executed within the context of the high priority timer
	// task. And is executed shortly after the minimum rain level has been crossed.

	// 6/16/2015 rmd : If however our rain table has rolled cancel our effort to send this.
	// Remember the way this CI message is structured if the message is not sucessful for
	// whatever reason we will start the timer and try again. The only way to stop that effort
	// is either a successful ACK from the commserver or when the table rolls which clears this
	// flag. So we test the flag here.
	if( weather_preserves.rain.inhibit_irrigation_have_crossed_the_minimum )
	{
		CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_rain_indication, NULL, CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD, CI_POST_TO_BACK );
	}
}

static void start_send_rain_indication_timer_if_it_is_not_running( void )
{
	// 6/16/2015 rmd : If it exists do what we came for. Start the timer.
	if( cics.send_rain_indication_timer != NULL )
	{
		// 6/16/2015 rmd : Only if it is not running. So we can't possibly postpone by
		// restarting.
		if( xTimerIsTimerActive( cics.send_rain_indication_timer ) == pdFALSE )
		{
			// 6/16/2015 rmd : Start the 15 second timer to trigger the controller initiated send.
			xTimerStart( cics.send_rain_indication_timer, portMAX_DELAY );
		}
	}
}

static UNS_32 send_ci_rain_indication( void )
{
	DATA_HANDLE     msg_handle;

	DATE_TIME       ldt;

	UNS_8           *ucp;

	UNS_32			rv;
	
	rv = CI_RESULT_MESSAGE_SUCCESSFUL;
	
	// 9/16/2014 ajv : The command simply includes the date the rain occurred.
	if( mem_obtain_a_block_if_available( sizeof(UNS_16), (void**)&(msg_handle.dptr) ) )
	{
		ucp = msg_handle.dptr;

		msg_handle.dlen = 0;

		// ----------

		// 5/19/2015 rmd : Include some data. Not sure how a 0 length data section would pass
		// through the functions. Including on the commserver side. The content is not used at all.
		EPSON_obtain_latest_time_and_date( &ldt );

		memcpy( ucp, &ldt.D, sizeof(UNS_16) );
		ucp += sizeof(UNS_16);
		msg_handle.dlen += sizeof(UNS_16);

		// ----------

		set_now_xmitting_and_send_the_msg( msg_handle, &cics.waiting_for_rain_indication_response, NULL, MID_TO_COMMSERVER_RAIN_INDICATION_init_packet, MID_TO_COMMSERVER_RAIN_INDICATION_data_packet );
	}
	else
	{
		Alert_Message( "No memory to send rain indication" );
		
		rv = CI_RESULT_NO_MEMORY;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static void send_rain_indication( void )
{
	UNS_32	ci_result;
	
	if( !sending_registration_instead( CI_MESSAGE_send_rain_indication, NULL ) )
	{
		ci_result = send_ci_rain_indication();
		
		if( ci_result == CI_RESULT_NO_MEMORY )
		{
			// 6/16/2015 rmd : Try again later.
			CONTROLLER_INITIATED_post_event( CI_EVENT_start_send_rain_indication_timer );
		}
		
		// ----------
		
		// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
		// controllers working on the next msg in the msg queue.
		check_if_we_need_to_post_a_build_and_send_event( ci_result );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* --------------- M O B I L E   S T A T U S ---------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void restart_mobile_status_timer( UNS_32 phow_long_ms )
{
	// 6/16/2015 rmd : If it exists do what we came for. Start the timer.
	if( cics.send_mobile_status_timer != NULL )
	{
		// 7/2/2015 rmd : We want to restart the timer. Status sends are embedded in the foal
		// irrigation management for mobile stations at the turn ON and turn OFF points. Those
		// status are sent immediately at those spots.
		xTimerChangePeriod( cics.send_mobile_status_timer, MS_to_TICKS( phow_long_ms ), portMAX_DELAY );
	}
}

static void mobile_status_timer_callback( xTimerHandle pxTimer )
{
	// 10/2/2014 rmd : Remember this is executed within the context of the high priority timer
	// task. And is executed shortly after the minimum rain level has been crossed.

	// ----------
	
	// 6/24/2015 rmd : I've decided not to use a MUTEX for the
	// cics.mobile_seconds_since_last_command. We are only reading it here. As atomic reads. So
	// the value will be valid. And either it is past a certain limit or not. And that is not
	// critical.
	
	BOOL_32		send_status;
	
	send_status = (true);
	
	// ----------
	
	// 6/24/2015 rmd : It is here where we implement the self perpetuating status screens. We
	// restart the timer with varying screen update rates. Depending on how long we have been
	// doing this with no new commands arriving.
	if( cics.mobile_seconds_since_last_command >= CI_MOBILE_SESSION_idle_limit_seconds )
	{
		// 6/24/2015 rmd : Session over. Do not restart the status timer. Do not send the status.
		send_status = (false);
	}
	else
	if( cics.mobile_seconds_since_last_command > CI_MOBILE_SESSION_idle_throttle_down_seconds )
	{
		// 6/24/2015 rmd : Slow down a bit. It has been a while since we've seen a command.
		restart_mobile_status_timer( CI_SEND_STATUS_MS_reduced_rate_update );
	}
	else
	{
		// 6/24/2015 rmd : Slow down a bit. It has been a while since we've seen a command.
		restart_mobile_status_timer( CI_SEND_STATUS_MS_routine_update );
	}

	// ----------
	
	if( send_status )
	{
		// 3/7/2017 rmd : Why send to the front? I don't think we need to. The commserver uses a
		// routine poll message to acquire a mobile status. He is expecting one to be there that we
		// deliver.
		CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_mobile_status, NULL, CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD, CI_POST_TO_BACK );
	}
}

// ----------

typedef struct
{
	UNS_8	revision;

	// ----------
	
	// 6/22/2015 rmd : The mlb_record is only present if we have a mlb.
	UNS_8	mainline_break;

	// 6/22/2015 rmd : 16 bytes if present
	SYSTEM_MAINLINE_BREAK_RECORD	mlb_record;
	
	// ----------
	
	UNS_8	has_et_gage;
	
	UNS_8	et_gage_pulses;
	

	UNS_8	has_rain_bucket;
	
	UNS_8	rain_bucket_pulses;
	
	// ----------
	
	UNS_8	number_of_systems;
	
	UNS_16	flow_rate_1_gpm;
	UNS_16	flow_rate_2_gpm;
	UNS_16	flow_rate_3_gpm;
	UNS_16	flow_rate_4_gpm;
	
	// ----------
	
	UNS_8	number_of_stations_on;
	
	UNS_8	reason_on;
	
	// ----------
	
	// 6/22/2015 rmd : Worst case can reflect up to 6 station on. Only included if on for
	// mobile.
	UNS_8	box_index1;
	UNS_8	station_number1;
	UNS_16	remaining_seconds1;
	
	UNS_8	box_index2;
	UNS_8	station_number2;
	UNS_16	remaining_seconds2;
	
	UNS_8	box_index3;
	UNS_8	station_number3;
	UNS_16	remaining_seconds3;
	
	UNS_8	box_index4;
	UNS_8	station_number4;
	UNS_16	remaining_seconds4;
	
	UNS_8	box_index5;
	UNS_8	station_number5;
	UNS_16	remaining_seconds5;
	
	UNS_8	box_index6;
	UNS_8	station_number6;
	UNS_16	remaining_seconds6;
	
	// ----------
	
} __attribute__((packed)) MOBILE_STATUS_PACKET_STRUCT;

static DATA_HANDLE		mobile_handle;

static UNS_8			*mobile_ucp;

static void mobile_copy_util( void *pfrom, UNS_32 plength )
{
	memcpy( mobile_ucp, pfrom, plength );
	
	mobile_ucp += plength;
	
	mobile_handle.dlen += plength;
}

static UNS_32 send_ci_mobile_status( void )
{
	UNS_32			sss, number_of_systems;

	UNS_8			temp_8, number_ON_for_mobile, highest_reason_ON;

	UNS_16			temp_16;
	
	BOOL_32			found_a_mlb;

	IRRIGATION_LIST_COMPONENT	*ilc;
	
	UNS_32	rv;
	
	// ----------

	rv = CI_RESULT_MESSAGE_SUCCESSFUL;
	
	// 6/22/2015 rmd : Get memory for the largest possible message content.
	if( mem_obtain_a_block_if_available( sizeof(MOBILE_STATUS_PACKET_STRUCT), (void**)&(mobile_handle.dptr) ) )
	{
		mobile_handle.dlen = 0;
		
		mobile_ucp = mobile_handle.dptr;
		
		// ----------

		temp_8 = MOBILE_STATUS_REVISION_CODE;

		mobile_copy_util( &temp_8, sizeof(UNS_8) );
		
		// ----------

		// 6/22/2015 rmd : Find and deliver any MLB indication. We'll just show the first one we
		// see. That's good enough to alert him. He can't perform any mobile functionality with a
		// MLB. And MUST go to the controller to clear.
		found_a_mlb = (false);

		number_of_systems = 0;

		xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

		// 5/3/2012 rmd : Check for MLB one system at a time.
		for( sss=0; sss<MAX_POSSIBLE_SYSTEMS; sss++ )
		{
			// 5/3/2012 rmd : Only if this preserves record is in use (registered).
			if( system_preserves.system[ sss ].system_gid != 0 )
			{
				number_of_systems += 1;
				
				if( !found_a_mlb && SYSTEM_PRESERVES_there_is_a_mlb( &system_preserves.system[ sss ].delivered_mlb_record ) )
				{
					found_a_mlb = (true);
					
					temp_8 = (true);
					mobile_copy_util( &temp_8, sizeof(UNS_8) );

					mobile_copy_util( &system_preserves.system[ sss ].delivered_mlb_record, sizeof(SYSTEM_MAINLINE_BREAK_RECORD) );
				}
			}
		}

		xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

		if( !found_a_mlb )
		{
			temp_8 = (false);
			mobile_copy_util( &temp_8, sizeof(UNS_8) );
		}

		if( !number_of_systems )
		{
			Alert_Message( "CI: no systems!" );	
		}

		// ----------

		if( WEATHER_get_et_gage_is_in_use() )
		{
			temp_8 = (true);
			mobile_copy_util( &temp_8, sizeof(UNS_8) );

			temp_8 = (weather_preserves.et_rip.et_inches_u16_10000u/100);
			mobile_copy_util( &temp_8, sizeof(UNS_8) );
		}
		else
		{
			temp_8 = (false);
			mobile_copy_util( &temp_8, sizeof(UNS_8) );
		}

		// ----------
		
		if( WEATHER_get_rain_bucket_is_in_use() )
		{
			temp_8 = (true);
			mobile_copy_util( &temp_8, sizeof(UNS_8) );

			temp_8 = weather_preserves.rain.midnight_to_midnight_raw_pulse_count;
			mobile_copy_util( &temp_8, sizeof(UNS_8) );
		}
		else
		{
			temp_8 = (false);
			mobile_copy_util( &temp_8, sizeof(UNS_8) );
		}

		// ----------

		// 7/2/2015 rmd : FLOW RATE

		temp_8 = number_of_systems;
		mobile_copy_util( &temp_8, sizeof(UNS_8) );

		for( sss=0; sss<number_of_systems; sss++ )
		{
			temp_16 = system_preserves.system[sss].system_rcvd_most_recent_token_5_second_average;
			mobile_copy_util( &temp_16, sizeof(UNS_16) );
		}

		// ----------
		
		// 6/22/2015 rmd : Poke around in the ON list. Remember we are at the master box potentially
		// with a central device so this is valid to do. A bit bold yet valid.
		xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );
		
		// 7/2/2015 rmd : First go through and find if ANY stations ON for radio remote. Remember
		// this list contains valves from multiple systems. So you can indeed have valves ON in one
		// system for radio remote. And ON in another system for say programmed irrigation. And the
		// order they appear in the on list is unknown.
		//
		// I have decided if there is a valve on for radio remote somewhere in the site the status
		// will reflect ONLY valves on for radio remote. To keep the status manageable. So his
		// mobile will only reflect the radio remote valves not those potentially ON for another
		// reason.
		
		// 7/2/2015 rmd : First loop through and find how many ON for radio remote. And what the
		// highest priority reason ON is.
		number_ON_for_mobile = 0;
		
		highest_reason_ON = 0;
				
		ilc = nm_ListGetFirst( &foal_irri.list_of_foal_stations_ON );
		
		while( ilc )
		{
			if( ilc->bbf.w_reason_in_list > highest_reason_ON )
			{
				highest_reason_ON = ilc->bbf.w_reason_in_list;
			}

			if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_MOBILE )
			{
				number_ON_for_mobile += 1;
			}
			
			ilc = nm_ListGetNext( &foal_irri.list_of_foal_stations_ON, ilc );
		}
 
		// ----------
		
		// 7/2/2015 rmd : If something ON range check reason ON.
		if( foal_irri.list_of_foal_stations_ON.count )
		{
			if( !((highest_reason_ON > nlu_IN_LIST_FOR_not_in_use) && (highest_reason_ON <= IN_LIST_FOR_MOBILE)) )
			{
				Alert_Message( "CI: reason out of range!" );
				
				highest_reason_ON = IN_LIST_FOR_PROGRAMMED_IRRIGATION;
			}
		}

		// 7/2/2015 rmd : Put in how many ON and for what reason.
		if( number_ON_for_mobile )
		{
			if( highest_reason_ON != IN_LIST_FOR_MOBILE )
			{
				Alert_Message( "CI: not for mobile!" );
				
				highest_reason_ON = IN_LIST_FOR_MOBILE;
			}

			if( number_ON_for_mobile > 6 )
			{
				Alert_Message( "CI: too many mobile ON!" );
				
				number_ON_for_mobile = 6;
			}

			mobile_copy_util( &number_ON_for_mobile, sizeof(UNS_8) );

			mobile_copy_util( &highest_reason_ON, sizeof(UNS_8) );

			// ----------
			
			// 7/2/2015 rmd : Now insert the station details protecting against exceeding 6.
			sss = 0;
			
			ilc = nm_ListGetFirst( &foal_irri.list_of_foal_stations_ON );
	
			while( ilc )
			{
				if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_MOBILE )
				{
					// 7/2/2015 rmd : Protect against delivering too many. Shouldn't happen but...
					if( sss < 6 )
					{
						// 6/22/2015 rmd : box index
						temp_8 = ilc->box_index_0;
						mobile_copy_util( &temp_8, sizeof(UNS_8) );
						
						// 6/22/2015 rmd : station number
						temp_8 = ilc->station_number_0_u8;
						mobile_copy_util( &temp_8, sizeof(UNS_8) );
						
						// 6/22/2015 rmd : remiaing seconds
						temp_16 = ilc->remaining_seconds_ON;
						mobile_copy_util( &temp_16, sizeof(UNS_16) );
					}

					sss += 1;
				}

				ilc = nm_ListGetNext( &foal_irri.list_of_foal_stations_ON, ilc );
			}
	
		}
		else
		{
			// 6/22/2015 rmd : Put in how many ON.
			temp_8 = foal_irri.list_of_foal_stations_ON.count;
			mobile_copy_util( &temp_8, sizeof(UNS_8) );
			
			// 7/2/2015 rmd : If some ON put in the highest priority reason why that we came up with. If
			// nothing ON skip putting in the reason.
			if( temp_8 )
			{
				// 7/2/2015 rmd : Reason ON has already been sanity checked.
				mobile_copy_util( &highest_reason_ON, sizeof(UNS_8) );
			}
		}

		xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );

		// ----------

		set_now_xmitting_and_send_the_msg( mobile_handle, &cics.waiting_for_mobile_status_response, NULL, MID_TO_COMMSERVER_status_screen_init_packet, MID_TO_COMMSERVER_status_screen_data_packet );
	}
	else
	{
		Alert_Message( "No memory to send mobile status" );

		rv = CI_RESULT_NO_MEMORY;
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
static void send_mobile_status( void )
{
	UNS_32	ci_result;
	
	CONTROLLER_INITIATED_TASK_QUEUE_STRUCT	cieqs;
	
	if( !sending_registration_instead( CI_MESSAGE_send_mobile_status, NULL ) )
	{
		ci_result = send_ci_mobile_status();
		
		if( ci_result == CI_RESULT_NO_MEMORY )
		{
			// 6/16/2015 rmd : Try again shortly.
			cieqs.event = CI_EVENT_restart_mobile_status_timer;
		
			cieqs.how_long_ms = CI_SEND_STATUS_MS_after_attempt_error;
			
			CONTROLLER_INITIATED_post_event_with_details( &cieqs );
		}
		
		// ----------
		
		// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
		// controllers working on the next msg in the msg queue.
		check_if_we_need_to_post_a_build_and_send_event( ci_result );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ------------ SENDING PDATA TO THE COMMSERVER ------------- */
/* --------------- P R O G R A M   D A T A ------------------ */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void ci_pdata_timer_callback( xTimerHandle pxTimer )
{
	// 4/23/2015 rmd : Time to start the process to send program data to the commserver. The
	// result of which is to build and send the pdata outbound message - if our firmware is up
	// to date.
	CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_ask_commserver_if_our_firmware_is_up_to_date, NULL, CI_IF_PRESENT_REMOVE_AND_REPOST, CI_POST_TO_FRONT );
}

// ----------
static void send_verify_firmware_version_before_sending_program_data( void )
{
	UNS_32	ci_result;
	
	if( !sending_registration_instead( CI_MESSAGE_ask_commserver_if_our_firmware_is_up_to_date, NULL ) )
	{
		ci_result = send_ci_check_for_updates( MID_TO_COMMSERVER_VERIFY_FIRMWARE_VERSION_init_packet, MID_TO_COMMSERVER_VERIFY_FIRMWARE_VERSION_data_packet, &cics.waiting_for_firmware_version_check_response );

		if( ci_result != CI_RESULT_MESSAGE_SUCCESSFUL )
		{
			// 4/23/2015 rmd : The process to send pdata involves first checking our code is up to date.
			// Because we are trying to do this we should assume there are changes to send. And it is
			// vital they are sent to the commserver. Restart the pdata timer to restart the process
			// since we couldn't do it now.
			xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGES_NOT_SENT_MS ), portMAX_DELAY );
		}
		
		// ----------
		
		// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
		// controllers working on the next msg in the msg queue.
		check_if_we_need_to_post_a_build_and_send_event( ci_result );
	}
}

// ----------
static UNS_32 build_and_send_program_data_primitive( void )
{
	// 4/22/2015 rmd : This function executes only within the context of the CONTROLLER
	// INITIATED task.

	// ----------

	DATA_HANDLE     msg_handle;

	UNS_32          build_results;
	
	UNS_32			rv;
	
	// ----------

	rv = CI_RESULT_MESSAGE_SUCCESSFUL;
	
	// 4/30/2015 rmd : Test if the lock flag is sent protecting us from generating a second
	// pdata message before a prior one has completed. This lock flag preserves the
	// functionality of the ACK bits.
	if( cics.a_pdata_message_is_on_the_list )
	{
		// 4/30/2015 rmd : One is already on the list. This isn't really an error. Just an
		// interesting occurance. Alert and restart the timer.
		Alert_Message( "PDATA: already a msg" );

		rv = CI_RESULT_INFRASTRUCTURE_IN_USE;
	}
	else
	{
		// 9/3/2015 rmd : If this panel is in the midst of a PANEL SWAP procedure do not allow the
		// program data to xmit to the commserver at this time. It is the factory default data and
		// will destroy the database table copy of the pdata wthe web is trying to send to this
		// panel. If we allowed this default data to be xmitted the only remiaing copy of the
		// program data would be held in Wissam's TEMP table. And if the user restarted the swap
		// procedure we would lose that! This block is celared once we have rcvd pdata from the
		// cloud.
		if( !weather_preserves.factory_reset_do_not_allow_pdata_to_be_sent )
		{
			build_results = PDATA_build_data_to_send( &msg_handle, CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER, (false) );
	
			if( build_results == PDATA_BUILD_RESULTS_no_data )
			{
				// 4/23/2015 rmd : This actually shouldn't happen as we only start the timer if there has
				// been a change made.
				Alert_Message( "PDATA: no data!" );
	
				rv = CI_RESULT_NO_DATA_TO_SEND;
				
				// 4/23/2015 rmd : Why should we start the timer again? It's being reported there are no
				// changes to send. If we start the timer again we would end up back here again. And
				// again. Never ending process.
			}
			else
			if( build_results == PDATA_BUILD_RESULTS_built_a_message )
			{
				// 4/23/2015 rmd : Send the data and get back here to try again in 30 seconds. But now that
				// we are taking the largest memory block and sending ALL the changes in one shot the next
				// time we pass through here should result with no data being sent. Unless of course more
				// changes were made.
				set_now_xmitting_and_send_the_msg( msg_handle, &cics.waiting_for_pdata_response, NULL, MID_TO_COMMSERVER_PROGRAM_DATA_init_packet, MID_TO_COMMSERVER_PROGRAM_DATA_data_packet );
	
				// 4/30/2015 rmd : Set the lock flag to prevent a second pdata message being generated.
				cics.a_pdata_message_is_on_the_list = (true);
	
				// 4/23/2015 rmd : To start the timer just to check if there are further changes DOES impact
				// our cell usage because the process only arrives here after verifying that our code is up
				// to date. Plus we now send the entire message in one shot. So I'm not going to start the
				// timer here. We shouldn't have to.
			}
			else
			if( build_results == PDATA_BUILD_RESULTS_no_memory )
			{
				rv = CI_RESULT_NO_MEMORY;
			}
			else
			{
				Alert_Message( "PDATA unexp result" );

				rv = CI_RESULT_ERROR_BUILDING_MESSAGE;
			}
		}
		else
		{
			Alert_Message( "PDATA to cloud blocked - unit swap underway" );

			rv = CI_RESULT_INFRASTRUCTURE_IN_USE;
		}
	}
	
	return( rv );
}

static void build_and_send_program_data( void )
{
	UNS_32	ci_result;
	
	ci_result = build_and_send_program_data_primitive();
	
	if( (ci_result == CI_RESULT_NO_MEMORY) || (ci_result == CI_RESULT_INFRASTRUCTURE_IN_USE) )
	{
		// 4/23/2015 rmd : Already sending or couldn't get the memory block. Already an alert line
		// made within the function that attempted the pdata build. Maybe we're receiving a code
		// update? Or saving some big file? Anyway restart the timer for 30 seconds to try again
		// then.
		xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGES_NOT_SENT_MS ), portMAX_DELAY );
	}
	
	// ----------
	
	// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
	// controllers working on the next msg in the msg queue.
	check_if_we_need_to_post_a_build_and_send_event( ci_result );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---- A S K   C O M M S E R V E R   F O R   P D A T A ----- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void send_verify_firmware_version_before_asking_for_program_data( void )
{
	// 6/5/2015 rmd : The commserver has sent us an IHSFY for pdata to send to us. We check the
	// firmware version first.

	UNS_32	ci_result;
	
	if( !sending_registration_instead( CI_MESSAGE_before_pdata_rqst_check_firmware, NULL ) )
	{
		ci_result = send_ci_check_for_updates( MID_TO_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_init_packet, MID_TO_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_data_packet, &cics.waiting_for_firmware_version_check_before_asking_for_pdata_response );

		if( ci_result != CI_RESULT_MESSAGE_SUCCESSFUL )
		{
			// 6/5/2015 rmd : Nothing to do. Jay will send another IHSFY in 30 seconds if we don't get
			// around to asking for the program data from him.
		}
		
		// ----------
		
		// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
		// controllers working on the next msg in the msg queue.
		check_if_we_need_to_post_a_build_and_send_event( ci_result );
	}
}

// ----------

static UNS_32 send_program_data_request_primitive( void )
{
	DATA_HANDLE     msg_handle;

	UNS_8       program_data_to_request;

	UNS_8       *ucp;
	
	UNS_32		rv;
	
	// ----------

	rv = CI_RESULT_MESSAGE_SUCCESSFUL;
	
	// 2/19/2015 ajv : Since we can't have a message of 0 length, just grab 8 bits.
	if( mem_obtain_a_block_if_available( sizeof(UNS_8), (void**)&(msg_handle.dptr) ) )
	{
		ucp = msg_handle.dptr;

		msg_handle.dlen = 0;

		// ----------

		// 9/26/2014 ajv : Initialize program_data_to_request to ensure it's all 0's before we set
		// any bits.
		program_data_to_request = 0x00;

		// ----------

		memcpy( ucp, &program_data_to_request, sizeof(UNS_8) );
		ucp += sizeof(UNS_8);
		msg_handle.dlen += sizeof(UNS_8);

		// ----------

		set_now_xmitting_and_send_the_msg( msg_handle, &cics.waiting_for_asked_commserver_if_there_is_pdata_for_us_response, NULL, MID_TO_COMMSERVER_PROGRAM_DATA_REQUEST_init_packet, MID_TO_COMMSERVER_PROGRAM_DATA_REQUEST_data_packet );
	}
	else
	{
		Alert_Message( "No memory to send program request" );

		rv = CI_RESULT_NO_MEMORY;
	}
	
	return( rv );
}

static void send_program_data_request( void )
{
	UNS_32	ci_result;
	
	ci_result = send_program_data_request_primitive();
	
	if( ci_result == CI_RESULT_NO_MEMORY )
	{
		// 4/24/2015 rmd : Nothing to do. Jay will indicate to us again that we need to ask for
		// program data by sending the IHSFY packet.
	}
	
	// ----------
	
	// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
	// controllers working on the next msg in the msg queue.
	check_if_we_need_to_post_a_build_and_send_event( ci_result );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ----------- L I G H T S  R E P O R T  D A T A ------------ */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/15/2014 rmd : NOTE - the associated timer and call back functions are located in the
// lights_report_data structure and file.

static UNS_32 send_ci_lights_report_data( void )
{
	// 10/9/2013 rmd : Executed within the CONTROLLER_INITIATED task context.

	// ----------

	DATA_HANDLE     msg_handle;

	UNS_32          size_limit;

	UNS_8           *ucp;

	UNS_32          rv;

	rv = CI_RESULT_MESSAGE_SUCCESSFUL;

	// ----------

	// 10/2/2014 rmd : Make sure the lights_report_data array stays stable.
	xSemaphoreTakeRecursive( lights_report_completed_records_recursive_MUTEX, portMAX_DELAY );

	// ----------

	// 10/2/2014 rmd : If a lights_report_data message has already been prepared we will not make a
	// second one. To avoid complications and problems tracking what has been sent or not. At
	// the least we end up sending dups. At the worst we fail to send some data to the
	// comm_server.
	if( lights_report_data_completed.rdfb.pending_first_to_send_in_use )
	{
		// 10/2/2014 rmd : Not sure if this would ever occur. Would think not when thinking about
		// the timers but I suppose manually triggered could do this.
		Alert_Message( "LIGHTS Report Data: message in process." );

		rv = CI_RESULT_INFRASTRUCTURE_IN_USE;
	}
	else
	{
		// 10/2/2014 rmd : Are there records to send? Because of the way we increment the pointers,
		// when there is a new line, the next_available ptr will NEVER be the same as the
		// first_to_send pointer. Therefore if the first_to_send is not equal to the next then there
		// are records to send!
		if( lights_report_data_completed.rdfb.first_to_send == lights_report_data_completed.rdfb.index_of_next_available )
		{
			Alert_Message( "CI: no lights report data to send" );

			rv = CI_RESULT_NO_DATA_TO_SEND;
		}
		else
		{
			size_limit = (LIGHTS_REPORT_DATA_MAX_RECORDS * sizeof(LIGHTS_REPORT_RECORD) );

			// ----------

			if( !mem_obtain_a_block_if_available( size_limit, (void**)&(msg_handle.dptr) ) )
			{
				Alert_Message( "No Memory to send lights report data" );

				rv = CI_RESULT_NO_MEMORY;
			}
			else
			{
				ucp = msg_handle.dptr;

				msg_handle.dlen = 0;

				// ----------

				// 10/2/2014 rmd : Do not change first_to_send within this function. That remains unchanged
				// till we see the ACK. Safe to work with the pending_first here within this function. MUTEX
				// allows us to do this.
				lights_report_data_completed.rdfb.pending_first_to_send = lights_report_data_completed.rdfb.first_to_send;

				while( (lights_report_data_completed.rdfb.pending_first_to_send != lights_report_data_completed.rdfb.index_of_next_available) && (rv == CI_RESULT_MESSAGE_SUCCESSFUL) )
				{
					// 10/22/2013 rmd : Can we fit one more?
					if( (msg_handle.dlen + sizeof(LIGHTS_REPORT_RECORD)) > size_limit )
					{
						rv = CI_RESULT_ERROR_BUILDING_MESSAGE;

						// 10/22/2013 rmd : There's been an error. Free the message memory and exit without sending.
						mem_free( msg_handle.dptr );

						// 10/22/2013 rmd : This should never happen. Make an alert line.
						Alert_Message( "CI: lights_report_data to commserver - too many!" );
					}
					else
					{
						// 10/2/2014 rmd : Copy the record at the pointer.
						memcpy( ucp, &(lights_report_data_completed.lrr[ lights_report_data_completed.rdfb.pending_first_to_send ]), sizeof(LIGHTS_REPORT_RECORD) );

						ucp += sizeof(LIGHTS_REPORT_RECORD);

						msg_handle.dlen += sizeof(LIGHTS_REPORT_RECORD);

						// ----------

						// 10/22/2013 rmd : Bump to the next line to send.
						nm_LIGHTS_REPORT_DATA_inc_index( &lights_report_data_completed.rdfb.pending_first_to_send );
					}

				}   

				if( rv == CI_RESULT_MESSAGE_SUCCESSFUL )
				{
					// 10/2/2014 rmd : Mark the pending first to send to prevent another pass through this
					// function prior to the message actually being sent. We wait till we see the acknowledge
					// before we update the actual first_byte_to_send variable.
					lights_report_data_completed.rdfb.pending_first_to_send_in_use = (true);

					// ----------

					// 10/2/2014 rmd : Send it!
					set_now_xmitting_and_send_the_msg(	msg_handle,
														&cics.waiting_for_lights_report_data_response, 
														NULL, 
														MID_TO_COMMSERVER_LIGHTS_REPORT_DATA_init_packet, 
														MID_TO_COMMSERVER_LIGHTS_REPORT_DATA_data_packet );
				}

			}  // of no memory available

		}  // of there is data to send

	}  // of infrastructure not in use

	// ----------

	xSemaphoreGiveRecursive( lights_report_completed_records_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static void send_lights_report_data( void )
{
	UNS_32  ci_result;

	// ----------
	
	if( !sending_registration_instead( CI_MESSAGE_send_lights_report_data, NULL ) )
	{
		// 8/25/2014 rmd : Attempt to send. Then evaluate the function return value to determine if
		// timer should be restarted.
		ci_result = send_ci_lights_report_data();

		// 8/25/2014 rmd : The only time we should restart the timer would be if the infrastructure
		// was in use. This would mean the timer fired (or a manual attempt was initiated) while a
		// message was either on the ci msg queue or actually in process. Meaning a message was made
		// and it has not yet completed. All the other reasons (except when successful) are errors.
		// And I don't want to fill the alerts with error after error. Once is good enough. And it
		// won't be once. The next time the timer fires cause a new line is made the error may
		// happen again.
		//
		// 9/11/2014 rmd : This can also happen cause station history and station report data go
		// after the same memory block. Only one can use it at a time. So one of the two must wait
		// until the other is done.
		if( (ci_result == CI_RESULT_INFRASTRUCTURE_IN_USE) || (ci_result == CI_RESULT_NO_MEMORY) )
		{
			// 8/15/2014 rmd : The most cause of this system already has a msg queued up on the cics
			// list (his pending flag is set).
			Alert_Message( "lights report data timer restarted" );

			// 8/15/2014 rmd : Restart the station history timer.
			LIGHTS_REPORT_DATA_start_the_ci_timer_if_it_is_not_running();
		}

		// ----------
		
		// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
		// controllers working on the next msg in the msg queue.
		check_if_we_need_to_post_a_build_and_send_event( ci_result );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ------------ N O   M O R E   M E S S A G E S ------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static UNS_32 build_and_send_the_no_more_messages_msg( void )
{
	DATA_HANDLE     msg_handle;

	DATE_TIME       ldt;

	UNS_8           *ucp;
	
	UNS_32			rv;
	
	rv = CI_RESULT_MESSAGE_SUCCESSFUL;

	// 2/24/2017 rmd : We include a 2 byte date so we do not have a zero byte message. There
	// really is no data with this message.
	if( mem_obtain_a_block_if_available( sizeof(UNS_16), (void**)&(msg_handle.dptr) ) )
	{
		ucp = msg_handle.dptr;

		msg_handle.dlen = 0;

		// ----------

		// 5/19/2015 rmd : Include some data. Not sure how a 0 length data section would pass
		// through the functions. Including on the commserver side. The content is not used at all.
		EPSON_obtain_latest_time_and_date( &ldt );

		memcpy( ucp, &ldt.D, sizeof(UNS_16) );
		ucp += sizeof(UNS_16);
		msg_handle.dlen += sizeof(UNS_16);

		// ----------

		set_now_xmitting_and_send_the_msg( msg_handle, &cics.waiting_for_the_no_more_messages_msg_response, NULL, MID_TO_COMMSERVER_no_more_messages_init_packet, MID_TO_COMMSERVER_no_more_messages_data_packet );
	}
	else
	{
		Alert_Message( "No memory to send No More Msgs msg" );
		
		rv = CI_RESULT_NO_MEMORY;
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern void CONTROLLER_INITIATED_attempt_to_build_and_send_the_no_more_messages_msg( BOOL_32 ptest_if_we_should_send_registration_first )
{
	UNS_32	ci_result;
	
	BOOL_32	build_and_send_it;
	
	// 10/5/2017 rmd : There is a use of this function when we want to force the END OF POLL to
	// definately be sent, and not registration. This functionality is used when receiving a
	// binary from the commserver.
	if( ptest_if_we_should_send_registration_first )
	{
		build_and_send_it = !sending_registration_instead( CI_MESSAGE_send_no_more_messages_msg, NULL );
	}
	else
	{
		build_and_send_it = (true);
	}
	
	if( build_and_send_it )
	{
		ci_result = build_and_send_the_no_more_messages_msg();
		
		if( ci_result == CI_RESULT_NO_MEMORY )
		{
			// 3/1/2017 rmd : If there is no memory should we try again? Probably. There should be
			// memory!
			CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_no_more_messages_msg, NULL, CI_IF_PRESENT_REMOVE_AND_REPOST, CI_POST_TO_BACK );
		}

		// ----------
		
		// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
		// controllers working on the next msg in the msg queue.
		check_if_we_need_to_post_a_build_and_send_event( ci_result );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* --------------- H U B   I S   B U S Y  ------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static UNS_32 build_and_send_the_hub_is_busy_msg( void )
{
	DATA_HANDLE     msg_handle;

	DATE_TIME       ldt;

	UNS_8           *ucp;
	
	UNS_32			rv;
	
	rv = CI_RESULT_MESSAGE_SUCCESSFUL;

	// 2/24/2017 rmd : We include a 2 byte date so we do not have a zero byte message. There
	// really is no data with this message.
	if( mem_obtain_a_block_if_available( sizeof(UNS_16), (void**)&(msg_handle.dptr) ) )
	{
		ucp = msg_handle.dptr;

		msg_handle.dlen = 0;

		// ----------

		// 5/19/2015 rmd : Include some data. Not sure how a 0 length data section would pass
		// through the functions. Including on the commserver side. The content is not used at all.
		EPSON_obtain_latest_time_and_date( &ldt );

		memcpy( ucp, &ldt.D, sizeof(UNS_16) );
		ucp += sizeof(UNS_16);
		msg_handle.dlen += sizeof(UNS_16);

		// ----------

		set_now_xmitting_and_send_the_msg( msg_handle, &cics.waiting_for_hub_is_busy_msg_response, NULL, MID_TO_COMMSERVER_hub_is_busy_init_packet, MID_TO_COMMSERVER_hub_is_busy_data_packet );
	}
	else
	{
		Alert_Message( "No memory to send Hub Is Busy msg" );
		
		rv = CI_RESULT_NO_MEMORY;
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
static void attempt_to_build_and_send_the_hub_is_busy_msg( void )
{
	UNS_32	ci_result;
	
	if( !sending_registration_instead( CI_MESSAGE_send_hub_is_busy_msg, NULL ) )
	{
		ci_result = build_and_send_the_hub_is_busy_msg();
		
		if( ci_result == CI_RESULT_NO_MEMORY )
		{
			// 3/1/2017 rmd : If there is no memory should we try again? Probably. There should be
			// memory! Post to the front in the spirit of the hub is busy message, which is supposed to
			// be the only message sent (when the hub is busy).
			CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_hub_is_busy_msg, NULL, CI_IF_PRESENT_REMOVE_AND_REPOST, CI_POST_TO_FRONT );
		}

		// ----------
		
		// 3/1/2017 rmd : Test if the message was successful. If not we need to get hub based
		// controllers working on the next msg in the msg queue.
		check_if_we_need_to_post_a_build_and_send_event( ci_result );
	}
}

/* ---------------------------------------------------------- */
extern void CONTROLLER_INITIATED_post_to_messages_queue( UNS_32 pevent, BY_SYSTEM_RECORD *pbsr_ptr, UNS_32 pbehavior, UNS_32 psend_to_front )
{
	CI_MSGS_TO_SEND_QUEUE_STRUCT	mtsqs;
	
	UNS_32	result;
	
	UNS_32	how_many, iii;
	
	BOOL_32	proceed;

	// ----------
	
	// 4/24/2017 rmd : Protect the queue from other tasks adding while we are scrolling through
	// the queue list during filtering. Take the scenario where a timer fires off and wants to
	// post to the message queue. That could be done within the ocntext of the high priority
	// timer task if this function were called within the timer callback function (and this does
	// happen!). Wait forever for the MUTEX.
	xSemaphoreTake( ci_message_list_MUTEX, portMAX_DELAY );
	
	// ----------
	
	proceed = (true);
	
	// 3/28/2017 rmd : What is the sense of queueing messages if we are not allowed to be
	// sending them in the first place? Block queueing if we are not eligible.
	if( CONFIG_this_controller_originates_commserver_messages() )
	{
		// 3/7/2017 rmd : If the event to be posted to the queue is a status screen, we want to post
		// it to the front of the queue. Also Jay and I reasoned that there should only be ONE
		// status screen msg on the queue. Doesn't make sense to spit out multiple status at the
		// same time.

		// 4/24/2017 rmd : QUESTION - is there any risk to pulling these off. And placing at the
		// back. The OS protects against the queue becoming corrupt by using critical sections. What
		// about the loss of an event while doing this. Don't think that can happen either even if a
		// high priority task cuts in (timer task for example) and posts to the queue.
		how_many = uxQueueMessagesWaiting( cics.msgs_to_send_queue );

		if( pbehavior == CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD )
		{
			for( iii=0; iii<how_many; iii++ )
			{
				// 4/24/2017 rmd : If we go through the entire queue pulling from the front and writing to
				// the back the queue will remain in its original order.
				if( xQueueReceive( cics.msgs_to_send_queue, &mtsqs, 0 ) )
				{
					if( mtsqs.event == pevent )
					{
						proceed = (false);
					}

					// 4/24/2017 rmd : Always write them back onto the queue.
					xQueueSendToBack( cics.msgs_to_send_queue, &mtsqs, 0 );
				}
			}
		}
		else
		if( pbehavior == CI_IF_PRESENT_REMOVE_AND_REPOST )
		{
			for( iii=0; iii<how_many; iii++ )
			{
				// 4/24/2017 rmd : If we go through the entire queue pulling from the front and writing to
				// the back the queue will remain in its original order.
				if( xQueueReceive( cics.msgs_to_send_queue, &mtsqs, 0 ) )
				{
					// 4/24/2017 rmd : If already present remove the item. Then add it back to the queue
					// following the callers to front or to back parameter.
					if( mtsqs.event != pevent )
					{
						xQueueSendToBack( cics.msgs_to_send_queue, &mtsqs, 0 );
					}	
				}
			}
		}
		else
		{
			Alert_Message_va( "CI MSG QUEUE: unk behavior %u", pbehavior );
		}
		

		// ----------
		
		if( proceed )
		{
			mtsqs.event = pevent;
		
			mtsqs.bsr_ptr = pbsr_ptr;
			
			// 2/15/2017 rmd : Post to the front or back appropriately. Do not wait any ticks.
			if( psend_to_front == CI_POST_TO_FRONT )
			{
				result = xQueueSendToFront( cics.msgs_to_send_queue, &mtsqs, 0 );
			}
			else
			{
				result = xQueueSendToBack( cics.msgs_to_send_queue, &mtsqs, 0 );
			}
			
			// ----------
			
			if( result )
			{
				// 4/24/2018 rmd : Suppressed this alert after many months of observation. Seems to be
				// working A okay.
				//Alert_ci_queued_msg( psend_to_front, mtsqs.event );
			}
			else
			{
				Alert_Message( "CI msgs to send queue overflow!" );
			}
		}
	}
	
	// ----------
	
	xSemaphoreGive( ci_message_list_MUTEX );
}

/* ---------------------------------------------------------- */
static void build_and_send_this_queued_up_msg( CI_MSGS_TO_SEND_QUEUE_STRUCT *mtsqs_ptr )
{
	switch( mtsqs_ptr->event )
	{
		case CI_MESSAGE_send_registration:
		
			send_registration_packet();
		
			break;
		
		case CI_MESSAGE_send_alerts:
		
			send_engineering_alerts();
		
			break;
		
		case CI_MESSAGE_send_station_history:
		
			send_station_history();
		
			break;
		
		case CI_MESSAGE_send_station_report_data:
		
			send_station_report_data();
		
			break;
		
		case CI_MESSAGE_send_system_report_data:
		
			send_system_report_data();
		
			break;
		
		case CI_MESSAGE_send_poc_report_data:
		
			send_poc_report_data();
		
			break;
		

		case CI_MESSAGE_send_lights_report_data:
		
			send_lights_report_data();
		
			break;
						

		case CI_MESSAGE_send_budget_report_data:

			send_budget_report_data();

			break;
		

		case CI_MESSAGE_send_et_rain_tables:
		
			send_et_rain_tables();
		
			break;
		

		case CI_MESSAGE_send_flow_recording:
		
			send_flow_recording( mtsqs_ptr->bsr_ptr );
		
			break;
		

		case CI_MESSAGE_send_moisture_sensor_recorder_records:
		
			send_moisture_sensor_recording();
		
			break;
		

		case CI_MESSAGE_send_weather_data:
		
			send_weather_data();
		
			break;
		
		case CI_MESSAGE_send_rain_indication:
		
			send_rain_indication();
		
			break;
		
		case CI_MESSAGE_send_mobile_status:
		
			send_mobile_status();
		
			break;
		
		// ----------
		
		case CI_MESSAGE_send_check_for_updates:
		
			send_check_for_updates();
		
			break;
		
		// ----------
		
		case CI_MESSAGE_ask_commserver_if_our_firmware_is_up_to_date:
		
			send_verify_firmware_version_before_sending_program_data();
		
			break;
		
		case CI_MESSAGE_send_program_data_to_the_commserver:
		
			// 4/21/2015 rmd : Only after the commserver has responded to our request to check our
			// firmware version and giving us permission to send our program data do we do so. NOTE -
			// this is a direct call to the function to build and send the pdata. Doesn't test if we
			// need to send registration. That test was already made just before we checked if our
			// firmware was up to date.
			build_and_send_program_data();
			
			break;
		
		// ----------
		
		case CI_MESSAGE_before_pdata_rqst_check_firmware:
		
			send_verify_firmware_version_before_asking_for_program_data();
		
			break;
		
		case CI_MESSAGE_ask_commserver_if_there_is_program_data_to_send_to_us:
		
			send_program_data_request();
		
			break;
		
		// ----------
		
		case CI_MESSAGE_send_no_more_messages_msg:
		
			// 10/5/2017 rmd : And YES, test if we should send registration first, hence the (true)
			// parameter.
			CONTROLLER_INITIATED_attempt_to_build_and_send_the_no_more_messages_msg( (true) );
		
			break;
		
		case CI_MESSAGE_send_hub_is_busy_msg:
		
			attempt_to_build_and_send_the_hub_is_busy_msg();
		
			break;

		// ----------
		
		default:
			Alert_Message_va( "CI: unhandled message to send %u", mtsqs_ptr->event );
			break;
						
	}
}

/* ---------------------------------------------------------- */
extern void CONTROLLER_INITIATED_task( void *pvParameters )
{
	// 8/21/2013 rmd : RECOGNIZE this task is active at the controller (the box) that originates
	// the message. This way multiple boxes could trigger to send controller initiated messages
	// at the same time. The SERIAL DRIVER task however should then allow transmission for the
	// case when it is supposed establish the connection but the device is already connected.
	// Which it does. If already connected the serial driver simply goes ahead and sends the
	// message.

	// ----------

	UNS_32							task_index;

	CONTROLLER_INITIATED_TASK_QUEUE_STRUCT	citqs;

	CI_MSGS_TO_SEND_QUEUE_STRUCT	mtsqs;
	
	UNS_32							seconds_to_wait_before_trying_to_connect_again;
	
	// ----------
	
	// 10/23/2012 rmd : Passed parameter is the address to the Task_Table entry used to create
	// this task. So we can figure out it's index to use when keeping the watchdog activity
	// monitor moving. And extract the port this task is responsible for.
	TASK_ENTRY_STRUCT   *tt_ptr;

	tt_ptr = pvParameters;

	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// ----------
	
	cics.msgs_to_send_queue = xQueueCreate( CI_NUMBER_OF_MSGS_TO_ALLOWED_TO_BE_QUEUED, sizeof(CI_MSGS_TO_SEND_QUEUE_STRUCT) );

	cics.queued_msgs_polling_timer = xTimerCreate( (const signed char*)"", MS_to_TICKS(10000), FALSE, NULL, ci_queued_msgs_polling_timer_callback );

	// 5/8/2015 rmd : The __clear_all_waiting_for_response_flags function and the
	// PDATA_update_pending_change_bits function depend on this timer being created. We do not
	// test for the timer to be a NULL when using within those functions!
	cics.pdata_timer = xTimerCreate( (const signed char*)"", MS_to_TICKS(CI_PDATA_TIMER_CHANGE_MADE_MS), FALSE, NULL, ci_pdata_timer_callback );

	// 10/7/2013 rmd : Create the application level message response timer.
	cics.response_timer = xTimerCreate( (const signed char*)"", MS_to_TICKS(CONTROLLER_INITIATED_DATABASE_RECORD_INSERT_TIME_MS), FALSE, NULL, ci_response_timer_callback );

	cics.waiting_to_start_the_connection_process_timer = xTimerCreate( (const signed char*)"", MS_to_TICKS(1000), FALSE, NULL, waiting_to_start_the_connection_process_timer_callback );

	cics.hub_packet_activity_timer = xTimerCreate( (const signed char*)"", MS_to_TICKS(1000), FALSE, NULL, hub_packet_activity_time_callback );

	cics.process_timer = xTimerCreate( (const signed char*)"", MS_to_TICKS(1000), FALSE, NULL, connection_process_timer_callback );

	// ----------
	
	// 6/16/2015 rmd : This should be a 30 second timer. So that 30 seconds after 8PM the
	// weather data is sent to the commserver. And if that fails we quickly get in another try.
	// The commserver will be sending out all its weather starting at around 8:05pm. So If it is
	// past 8:05 sending our weather data is for naught.
	//
	// 9/5/2017 rmd : Things got more complicated when the hub functionality was added. For that
	// it requires a poll from the commserver to send the message - not just the 30 second timer
	// to expire. Well in that scenario waiting the 30 seconds seemed like just toying with the
	// devil in that if the message wasn't queued when the poll came that was it - the weather
	// wasn't going to be shared. So we shaved 15 seconds off this time. Was leary of driving it
	// closer to zero for the non-hub retry functionality. NOTE - I don't think retries work for
	// the hub based controllers as the poll is done and likely not another poll for some 15
	// minutes.
	cics.send_weather_data_timer = xTimerCreate( (const signed char*)"", MS_to_TICKS(15*1000), FALSE, (void*)NULL, send_weather_data_timer_callback );
	
	// 6/16/2015 rmd : This should be a 30 second timer. So that 30 seconds after we see the
	// minimum rain that indication is sent to the commserver. And if that fails we quickly get
	// in another try. We will try again and again until either success or the table rolls.
	//
	// 9/5/2017 rmd : See note above regarding weather data timer change to 15 seconds. Same
	// holds here for the rain indication timer.
	cics.send_rain_indication_timer = xTimerCreate( (const signed char*)"", MS_to_TICKS(15*1000), FALSE, (void*)NULL, send_rain_indication_timer_callback );
	
	// ----------
	
	cics.send_mobile_status_timer = xTimerCreate( (const signed char*)"", MS_to_TICKS(5*1000), FALSE, (void*)NULL, mobile_status_timer_callback );
	
	// ----------
	
	// 9/7/2017 rmd : IMPORTANT to start off in the correct mode. We ALWAYS will start making an
	// attempt to ID any raveon radios that are present. It is vital to place the CI task into
	// the correct mode in order to direct the timer and string hunt events produced during the
	// process.
	cics.mode =  CI_MODE_raveon_radio_id;
	
	cics.state = CI_STATE_idle;

	memset( &cics.now_xmitting, 0x00, sizeof( CONTROLLER_INITIATED_MESSAGE_TRANSMITTING ) );

	cics.connection_process_failures = 0;

	// 3/23/2017 rmd : And set this guivar appropriately (false) to start.
	GuiVar_LiveScreensCommCentralConnected = (false);
	
	// ----------

	// 10/11/2013 rmd : And clear all the flags to indicate no exchange is in process.
	__clear_all_waiting_for_response_flags( (false), (false) );

	// ----------

	// 9/17/2014 rmd : No errors yet so no don't cycle power after disconnect.
	cics.last_message_concluded_with_a_response_timeout = (false);

	// ----------

	// 4/30/2015 rmd : Here, in the context of a startup, if ACK change bits are set it
	// indicates there was a code restart somewhere from the time the pdata message was built
	// and that message being allowed to complete transmission sequence. Call our ACK bit
	// management function indicating there was an error so that the original change bits are
	// restored and the pdata timer is started. NOTE - must call after pdata timer has been
	// created.
	PDATA_update_pending_change_bits( (true) );

	// 4/24/2015 rmd : Using our battery backed indication of changes that need to be sent to
	// the commserver, if there was a power failure before changes had a chance to be sent to
	// the comm_server, start the pdata timer.
	if( weather_preserves.pending_changes_to_send_to_comm_server == (true) )
	{
		Alert_Message( "Pending PData changes to be sent." );

		// 11/9/2017 rmd : Important - use the AFTER_CONNECTION timer scheme to spread the load when
		// the comm server is started and all controllers connect at roughly the same time. Or after
		// a broad code update, all controllers reboot and send their pdata.
		xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CONTROLLER_INITIATED_ms_after_connection_to_send_alerts( config_c.serial_number ) ), portMAX_DELAY );
	}

	// ----------

	while( (true) )
	{
		// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
		if( xQueueReceive( CONTROLLER_INITIATED_task_queue, &citqs, MS_to_TICKS( 500 ) ) )
		{
			switch( citqs.event )
			{
				case CI_EVENT_commence:
					// 9/6/2017 rmd : If there are Raveon radios present we will power them up, feed them a
					// '+++' and determine if we are working with a Stingray(M7), Fireline(M5), or Sonik
					// variety. At the conclusion of this process we will 'start the CI connection' process, AND
					// send the COMMENCE event to the comm_mngr so it may begin running. NOTE - if no raveon
					// radios are present this process very quickly moves through (total 50ms) and starts the
					// connection process.
					RAVEON_id_start_the_process();
					break;
					
				// ----------
				
				case CI_EVENT_start_send_weather_data_timer:
					start_send_weather_data_timer_if_it_is_not_running();
					break;
					
				case CI_EVENT_start_send_rain_indication_timer:
					start_send_rain_indication_timer_if_it_is_not_running();
					break;
				
				case CI_EVENT_start_timer_to_send_pdata_change_to_commserver:
					xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGE_MADE_MS ), portMAX_DELAY );
					break;

				// ----------
				
				case CI_EVENT_wrap_up_this_transaction:
				case CI_EVENT_message_termination_due_to_new_inbound_message:
				case CI_EVENT_message_termination_due_to_time:

					if( (cics.mode == CI_MODE_ready) && (cics.state == CI_STATE_waiting_for_response) )
					{
						ci_waiting_for_response_state_processing( citqs.event );
					}
					else
					{
						// 2/23/2017 rmd : This shouldn't happen. These events are only applicable if we are in the
						// middle of sending a message. Shouldn't see them otherwise.
						Alert_Message( "CI: waiting events while not ready & waiting" );
					}
					break;
					

				case CI_EVENT_build_and_send_the_next_queued_msg:
					// 2/23/2017 rmd : First test can we BUILD and SEND any queued messages? If not we count on
					// whatever it preventing us from doing so to once again eventually issue a
					// 'send_next_queued_msg' event.
					//
					// 6/14/2017 rmd : Do we want to allow messages to the commserver if the chain is down?
					// Maybe we need to block the sending of PROGRAM DATA changes when that occurs. You would
					// not block that here. Go find where the pdata message is bing built and prevent it.
					//
					// 6/14/2017 rmd : Do block if a device exchange is pending or happening. You can imagine
					// that wouldn't be good if we were trying to use the device while programming it!
					if( !in_device_exchange_hammer )
					{
						if( CONFIG_this_controller_originates_commserver_messages() )
						{
							if( (cics.mode == CI_MODE_ready) && CONFIG_is_connected() )
							{
								if( cics.state == CI_STATE_idle)
								{
									// 2/23/2017 rmd : Sanity check - spot some of the fields of now_xmitting. They had better
									// be zeros.
									if( (cics.now_xmitting.message.dptr != NULL) || (cics.now_xmitting.message.dlen != 0) || (cics.now_xmitting.init_packet_message_id != 0) || (cics.now_xmitting.activity_flag_ptr != NULL) )
									{
										// 8/20/2013 rmd : ERROR. A message is apparently in progress. Free memory if needed. And
										// clean up. Being at the IDLE state with now_xmitting not a NULL should be considered an
										// ERROR. This cannot happen.
										Alert_Message( "CI: During IDLE now_xmitting is non-zero" );
										
										// 2/23/2017 rmd : This could result in a memory leak if memory is allocated for the
										// message. But the alert should alert us to this condition that should never happen. Zero
										// the structure and build a new message.
										memset( &cics.now_xmitting, 0x00, sizeof( CONTROLLER_INITIATED_MESSAGE_TRANSMITTING ) );
									}
									
									// ----------
									
									// 2/23/2017 rmd : In any case build and commence sending the new message.
									if( xQueueReceive( cics.msgs_to_send_queue, &mtsqs, 0 ) )
									{
										// 10/26/2017 rmd : Alert line used only during development.
										//Alert_Message_va( "CI pulled msg %u", mtsqs.event );
				
										build_and_send_this_queued_up_msg( &mtsqs );
									}
									else
									{
										// 2/23/2017 rmd : Nuisance alert. Only for developer. Normally commented out.
										//Alert_Message( "CI msgs: nothing queued" );
									}
	
	
								}  // of we are not waiting for a response
								
								// 2/23/2017 rmd : When waiting for a response that state always ends by generating another
								// 'send_next_queued_msg' event.
							}
							else
							{
								// 4/24/2015 rmd : We shall just alert. And leave the message on the list. When the
								// connection occurs another 'new_message_available' event will be generated.
								// 3/20/2018 rmd : Commented out to manage alerts.
								// Alert_ci_waiting_for_device();
								
								// 2/3/2017 rmd : Try to start it. If not eligible won't. There are quite a few safeguards
								// in place, so directly calling the start function without checking if the connection
								// process is already underway is safe to do. Keep in mind when we connect we generate
								// another 'send_next_queued_msg' event.
								ci_start_the_connection_process();
							}

						}  // of is allowed to build and send messages to the commserver
						
					}  // of comm_mngr prevented msg from being built - do nothing
					break;

				// ----------
				
				case CI_EVENT_bump_mobile_session_seconds_count:

					if( cics.mobile_seconds_since_last_command < CI_MOBILE_SESSION_idle_limit_seconds )
					{
						cics.mobile_seconds_since_last_command += 1;
					}

					break;

				case CI_EVENT_zero_mobile_session_seconds_count:

					cics.mobile_seconds_since_last_command = 0;

					break;

				case CI_EVENT_restart_mobile_status_timer:

					restart_mobile_status_timer( citqs.how_long_ms );

					break;

				// ----------

				case CI_EVENT_restart_hub_packet_activity_timer:
					// 5/10/2017 rmd : A qualifying packet has arrived from the commserver, so restart the
					// timer.
					restart_hub_packet_activity_timer();
					break;

				case CI_EVENT_start_the_connection_process:
					// 2/3/2017 rmd : Try to start it. If not eligible won't. There are quite a few safeguards
					// in place so directly calling the start function with no checking here is safe to do.
					ci_start_the_connection_process();
					break;

				case CI_EVENT_process_timer_fired:
					if( cics.mode == CI_MODE_connecting )
					{
						process_during_connection_process_timer_fired();
					}
					else
					if( cics.mode == CI_MODE_raveon_radio_id )
					{
						RAVEON_id_event_processing( &citqs );
					}
					else
					{
						Alert_Message( "CI: timer fired unexpectedly" );
					}

					break;

				case CI_EVENT_process_string_found:
					if( cics.mode == CI_MODE_connecting )
					{
						process_during_connection_process_string_found();
					}
					else
					if( cics.mode == CI_MODE_raveon_radio_id )
					{
						RAVEON_id_event_processing( &citqs );
					}
					else
					{
						Alert_Message( "CI: string found unexpectedly" );
					}

					break;

				case CI_EVENT_connection_process_error:
					// 9/17/2014 ajv : Generate an alert which can be filtered to be displayed for the user.
					Alert_comm_command_failure( CI_EVENT_connection_process_error );
		
					// ----------
					
					// 12/22/2014 ajv : Update the Comm Test string so the user can see the current status
					strlcpy( GuiVar_CommTestStatus, "Communication failed: Unable to Connect", sizeof(GuiVar_CommTestStatus) );

					// 4/17/2015 ajv : If the user is viewing the Communication Test screen, we should refresh
					// the cursors to show the updated text.
					if( GuiLib_CurStructureNdx == GuiStruct_scrCommTest_0 )
					{
						Refresh_Screen();
					}

					// ----------
					
					cics.mode = CI_MODE_waiting_to_connect;
					
					// ----------
					
					// 2/7/2017 rmd : Based upon how many times we have tried limit the frequency of retries.
					// Mostly this is to avoid filling the alerts. Otherwise what would be the harm to trying
					// again and again?
					//
					// 2/7/2017 rmd : Also note: the delay gives the user the opportunity to invoke the device
					// programming. THOUGH THIS IS NOT PROPERLY INTEGRATED WITHIN THE CI TASK!
					cics.connection_process_failures += 1;
					
					if( cics.connection_process_failures <= 3 )
					{
						// 2/7/2017 rmd : Once every 2 minutes to start.
						seconds_to_wait_before_trying_to_connect_again = (2 * 60);
					}
					else
					if( cics.connection_process_failures <= 5 )
					{
						// 2/7/2017 rmd : Then once every 10 minutes. etc ...
						seconds_to_wait_before_trying_to_connect_again = (10 * 60);
					}
					else
					if( cics.connection_process_failures <= 7 )
					{
						seconds_to_wait_before_trying_to_connect_again = (60 * 60);
					}
					else
					{
						seconds_to_wait_before_trying_to_connect_again = (4 * 60 * 60);
					}

					ci_start_the_waiting_to_start_the_connection_process_timer_seconds( seconds_to_wait_before_trying_to_connect_again );

					break;
					
				// ----------
				
				case CI_EVENT_disconnected:
					// 6/16/2017 rmd : By definition the connect/disconnect activity is ONLY associated with
					// communication devices linked to the commserver. Therefore suppress for all other
					// configurations.
					if( CONFIG_this_controller_originates_commserver_messages() )
					{
						if( config_c.port_A_device_index == COMM_DEVICE_LR_RAVEON )
						{
							// 4/25/2017 rmd : The Raveon unit CD output is not good for much. It chatters all the time
							// as it picks up pieces of the carrier and perhaps even noise. All we do to this device is
							// to power it ONCE and live life in bliss. We never cycle power again. The whole concept of
							// are we 'connected' is a farce for this device. Furthermore the FIRELINE port driver turns
							// itself off after 30 seconds of inactivity, as a 'feature' (a very bad feature as you
							// cannot disable this behavior), and when the driver turns itself off the CD line
							// transitions. Regarding CTS, luckily when the radio receives data the driver turns back on
							// and CTS is active so we can send a response via the radio. SO HERE, we just do nothing.
						}
						else
						{
							// 3/23/2017 rmd : During the connection process, most specifically when we cycle the power
							// up front, we see this line (associated with when the device is poered back on). It's kind
							// of a nuisance. Let's hide it in that case and see if we miss it or need it to realize
							// something strange is going on.
							if( cics.mode != CI_MODE_connecting )
							{
								Alert_Message( "Port A: Disconnected" );
								
								// 3/23/2017 rmd : Update the gui var which manages the 'connected' symbol shown in the
								// right hand side of the top screen bar.
								GuiVar_LiveScreensCommCentralConnected = (false);
	
								// 3/27/2017 rmd : If we are in the middle of a message take the brute force steps needed to
								// exit that mode.
								CONTROLLER_INITIATED_force_end_of_transaction();
							}
							
							// 2/3/2017 rmd : Try to start it. Only if eligible will the process start. If on-going will
							// NOT restart. There are quite a few safeguards in place so calling the start function
							// here, even if already in the connection process, is safe to do.
							ci_start_the_connection_process();
						}
					}
					break;

				case CI_EVENT_connected:
					// 6/16/2017 rmd : By definition the connect/disconnect activity is ONLY associated with
					// communication devices linked to the commserver. Therefore suppress for all other
					// configurations.
					if( CONFIG_this_controller_originates_commserver_messages() )
					{
						if( config_c.port_A_device_index == COMM_DEVICE_LR_RAVEON )
						{
							// 4/25/2017 rmd : The Raveon unit CD output is not good for much. It chatters all the time
							// as it picks up pieces of the carrier and perhaps even noise. All we do to this device is
							// to power it ONCE and live life in bliss. We never cycle power again. The whole concept of
							// are we 'connected' or not is a farce for this device. So think here we just do nothing.
						}
						else
						{
							// 3/23/2017 rmd : This event is only interesting outside of the connection process. During
							// the connection process CD can transition as a result of power cycling and/or device reset
							// activity. And during that time we do not want to see alert lines about such. They are
							// confusing. And the connection porcess makes it OWN connect alert line. So during the
							// connect process we don't need one.
							if( cics.mode != CI_MODE_connecting )
							{
								// 3/23/2017 rmd : I really don't ever expect this to happen outside of the connection
								// process. And therefore we should never see this line. Hence the question mark.
								Alert_Message( "Port A: Connected?" );
								
								// ----------
								
								// 3/23/2017 rmd : Update the gui var which manages the 'connected' symbol shown in the
								// right hand side of the top screen bar.
								GuiVar_LiveScreensCommCentralConnected = (true);
								
								// ----------
								
								// 3/29/2017 rmd : And we should start the connection process. Take the following scenario.
								// A connection attempt was tried. But failed . Repeatedly. So now we are in the mode where
								// we attempt a connection only once every 4 hours. There are some devices the connection
								// process is device controlled (meaning we don't actively cause it to connection, the
								// device is programmed to do so on its own). Well if the reason it couldn't connect was say
								// a wifi router was unplugged, or an SR radio master was powered down or not programmed
								// correctly, then when the condition is corrected we need to snap into action and run the
								// connection process. SO THIS CONTROLLER can begin responding to polls.
								ci_start_the_connection_process();
							}
						}

						// 3/23/2017 rmd : Nothing else to do. The connection recognition is managed and acted upon
						// during the connection process. NOT HERE!
					}
					break;
					
				// ----------
				
				default:
					Alert_Message( "CI : unk event" );
					break;
			}

		}  // of if queue rcvd (true)

		// 10/23/2012 rmd : Keep the activity indication going.
		task_last_execution_stamp[ task_index ] = xTaskGetTickCount();
	}
}

/* ---------------------------------------------------------- */
extern void CONTROLLER_INITIATED_post_event_with_details( CONTROLLER_INITIATED_TASK_QUEUE_STRUCT *pq_item_ptr )
{
	// 6/24/2015 rmd : Executed within the context of a wide variety of tasks.

	//  WARNING: DO NOT CALL THIS FUNCTION FROM AN INTERRUPT because it is not using the fromISR
	//  xQueue function. Executed from a wide variety of tasks. Including the timer task,
	//  comm_mngr task, and the controller initiated task.

	// ----------
	
	if( xQueueSendToBack( CONTROLLER_INITIATED_task_queue, pq_item_ptr, 0 ) != pdPASS )	 // do not wait for queue space ... flag an error
	{
		Alert_Message( "CI queue overflow!" );
	}
}

/* ---------------------------------------------------------- */
extern void CONTROLLER_INITIATED_post_event( UNS_32 pevent )
{
	// 6/24/2015 rmd : Executed within the context of a wide variety of tasks.

	//  WARNING: DO NOT CALL THIS FUNCTION FROM AN INTERRUPT because it is not using the fromISR
	//  xQueue function. Executed from a wide variety of tasks. Including the timer task,
	//  comm_mngr task, and the controller initiated task.

	// ----------
	
	CONTROLLER_INITIATED_TASK_QUEUE_STRUCT	cieqs;
	
	cieqs.event = pevent;
	
	CONTROLLER_INITIATED_post_event_with_details( &cieqs );
}

/* ---------------------------------------------------------- */
extern UNS_32 CONTROLLER_INITIATED_ms_after_midnight_to_send_this_controllers_daily_records( UNS_32 pserial_number )
{
	// 7/13/2017 rmd : This function returns the number of ms to wait, after midnight, based
	// upon this controllers serial number, before sending the ET/RAIN record, the poc report
	// data, the lights report data, the station report data, and the system report data. This
	// is done in an effort to distibute the load on the database. Imagine if all controllers
	// hit the database at the same time. Well our old shitty advantage database has a probelm
	// with this. So this function was born.
	
	// 7/13/2017 rmd : Serial number begin at 50000 and continue up to say 99999. Well i guess
	// it doesn't matter how high up they go. The goal of my algorithm here is to spread out the
	// communication from 12:15 to 1:15. Over an hour window. An hour is 3600 seconds. So if I
	// just mod the serial number by 3600 I should get a number of seconds, that I can add to
	// 12:15.
	
	UNS_32	rv;
	
	UNS_32	remainder_seconds;
	
	// 7/13/2017 rmd : Send the report data sometime within the next hour.
	remainder_seconds = ( pserial_number % 3600 );
	
	rv = (15 * 60000) + ( remainder_seconds * 1000 );
	
	return( rv );
}

extern UNS_32 CONTROLLER_INITIATED_ms_after_connection_to_send_alerts( UNS_32 pserial_number )
{
	UNS_32	rv;
	
	UNS_32	remainder_seconds;
	
	// 7/13/2017 rmd : Send the alerts sometime in the first 4 minutes.
	remainder_seconds = ( pserial_number % 240 );
	
	// 7/13/2017 rmd : Handle 0, just to have some delay.
	if( !remainder_seconds )
	{
		// 10/11/2017 rmd : Do not change this. It provides a minimum of 5 seconds before the timer
		// fires. This way the hub END OF POLL can be sent as the very first message.
		remainder_seconds = 5;
	}
	
	rv = ( remainder_seconds * 1000 );
	
	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

