/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"code_distribution_receive.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"cent_comm.h"

#include	"configuration_controller.h"

#include	"configuration_network.h"

#include	"cs_mem.h"

#include	"dialog.h"

#include	"flash_storage.h"

#include	"packet_definitions.h"

#include	"wdt_and_powerfail.h"

#include	"d_process.h"

#include	"packet_router.h"

#include	"controller_initiated.h"

#include	"serport_drvr.h"

#include	"e_comm_test.h"

#include	"flowsense.h"

#include	"tpmicro_comm.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/19/2017 rmd : For the FLOWSENSE or COMMSERVER distributions the packets arrive is a
// steady stream. We do not expect, no accept reception problems. If we don't see a packet
// within 30 seconds, something is wrong.
#define CODE_RECEIPT_ERROR_TIMER_BETWEEN_INIT_AND_FIRST_DATA_PACKET_FOR_FLOWSENSE_OR_COMMSERVER_DISTRIBUTION_MS		(30000)

// 9/19/2017 rmd : When it comes to a HUB the data packet flow is unpredictable due to
// reception issues. And for the HUB it is actually acceptable to miss several packets, the
// ditribution continues, and retries take palce after a QUERY to the recepients. So be
// fairly lenient here. I would say wait 5 minutes with no packets before discarding the
// attempt and rebooting.
#define CODE_RECEIPT_ERROR_TIMER_BETWEEN_INIT_AND_FIRST_DATA_PACKET_FOR_HUB_DISTRIBUTION_MS							(5*60*1000)


// 9/19/2017 rmd : For a FLOWSENSE or COMMSERVER distribution packets should be flowing in
// at a regular rate. We have no retry scheme. We certainly expect to see one packet every
// 60 seconds.
#define CODE_RECEIPT_ERROR_TIMER_BETWEEN_DATA_PACKETS_FOR_FLOWSENSE_OR_COMMSERVER_DISTRIBUTION_MS					(1*60*1000)

// 9/19/2017 rmd : For HUB code distribution the error timer has to be MUCH longer, because
// if there are missing packets (reception problems) we don't really know when we received
// the last packet. And then we have to wait 90 seconds for the QUERY to begin, and then we
// have to wait for the QUERY TO FINISH before we possibly will see another data packet. The
// BIG PROBLEM is we don't know how long the query will take. If there are a lot of
// controllers in the hub list it could be a while, especially if there are errors. So wow,
// I'm going to make this time 30 minutes!
#define CODE_RECEIPT_ERROR_TIMER_BETWEEN_DATA_PACKETS_FOR_HUB_DISTRIBUTION_MS										(30*60*1000)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void code_receipt_error_timer_callback( xTimerHandle pxTimer )
{
	// 4/22/2014 rmd : Executed within the context of the high priority timer task.

	// 4/22/2014 rmd : The code receipt error timer expired. Indicating something went wrong
	// during the code receipt process. This should be a very rare event. And as such we decided
	// the safest way to recover from this event would be to restart. I think we could simply
	// change the comm_mngr mode to making_tokens. But I think there may be some risk involved
	// with that. For example, was the mode making tokens prior to switching into code_receipt
	// mode?
	
	// 6/8/2015 rmd : We count on this restart to clear the cics.code_receipt_underway flag
	// too.
	SYSTEM_application_requested_restart( SYSTEM_SHUTDOWN_EVENT_code_receipt_error );
}

/* ---------------------------------------------------------- */

#define	HUB_CODE_RECEIPT_PROGRESS_none_received		(0)
#define	HUB_CODE_RECEIPT_PROGRESS_partial_received	(1)
#define	HUB_CODE_RECEIPT_PROGRESS_fully_received	(2)

/* ---------------------------------------------------------- */
static void process_and_respond_to_incoming_hub_distribution_query_packet( const CODE_DISTRIBUTION_TASK_QUEUE_STRUCT* const pcdtqs_ptr )
{
	UNS_8	*ucp;

	FROM_COMMSERVER_PACKET_TOP_HEADER	fcpth;
	
	UNS_32	hub_serial_number;
	
	UNS_32	distribution_mode;
	
	UNS_32	query_binary_date;
	UNS_32	query_binary_time;
	
	UNS_32	executing_binary_date;
	UNS_32	executing_binary_time;
	
	UNS_32	reportable_progress;
	
	DATA_HANDLE	ldh;
	
	CRC_BASE_TYPE   lcrc;
	
	// ----------
	
	ucp = pcdtqs_ptr->dh.dptr;
	
	memcpy( &fcpth, ucp, sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) );

	ucp += sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER);

	// ----------
	
	memcpy( &hub_serial_number, ucp, sizeof(hub_serial_number) );
	ucp += sizeof(hub_serial_number);
	
	// ----------
	
	memcpy( &distribution_mode, ucp, sizeof(distribution_mode) );
	ucp += sizeof(distribution_mode);
	
	// ----------
	
	memcpy( &query_binary_date, ucp, sizeof(query_binary_date) );
	ucp += sizeof(query_binary_date);
	
	memcpy( &query_binary_time, ucp, sizeof(query_binary_time) );
	ucp += sizeof(query_binary_time);
	
	// ----------
	
	// 7/20/2017 rmd : Free the memory the packet was delivered in.
	mem_free( pcdtqs_ptr->dh.dptr );

	// ----------
	
	// 7/18/2017 rmd : Okay we've extracted all the delivered content from the packet. Prepare a
	// response.
	
	// 7/18/2017 rmd : Default is worst case.
	reportable_progress = HUB_CODE_RECEIPT_PROGRESS_none_received;
	
	// ----------
	
	// 11/2/2017 rmd : Using the distribution mode delivered to us, determine if we are already
	// running the code. If we already have the desired code, it doesn't matter if we have
	// partially received it or not, report that we have fully received it.
	if( distribution_mode == CODE_DISTRIBUTION_MODE_receiving_from_hub_main )
	{
		executing_binary_date = convert_code_image_date_to_version_number( (UNS_8*)(EMC_DYCS0_BASE), CS3000_APP_FILENAME );

		executing_binary_time = convert_code_image_time_to_edit_count( (UNS_8*)(EMC_DYCS0_BASE), CS3000_APP_FILENAME );
		
		if( (executing_binary_date == query_binary_date) && (executing_binary_time == query_binary_time) )
		{
			// 7/18/2017 rmd : So we are running what the hub is trying to send.
			Alert_Message( "HUB QUERY: already running main code" );
			
			reportable_progress = HUB_CODE_RECEIPT_PROGRESS_fully_received;
		}
	}
	else
	if( distribution_mode == CODE_DISTRIBUTION_MODE_receiving_from_hub_tpmicro )
	{
		if( (tpmicro_comm.file_system_code_date == query_binary_date) && (tpmicro_comm.file_system_code_time == query_binary_time) )
		{
			// 7/18/2017 rmd : So we are running what the hub is trying to send.
			Alert_Message( "HUB QUERY: already running tpmicro code" );
			
			reportable_progress = HUB_CODE_RECEIPT_PROGRESS_fully_received;
		}
	}
	else
	{
		Alert_Message( "QUERY: delivered UNEXP mode" );
	}
	
	// ----------
	
	// 11/2/2017 rmd : Okay so we must not be running the what the hub is distributing, so dig
	// deeper.
	if( reportable_progress == HUB_CODE_RECEIPT_PROGRESS_none_received )
	{
		// 7/18/2017 rmd : Test the mode to see if we are currently receiving what the hub thinks we
		// should be in. If so that would indicate we have a partial receipt to report.
		if( distribution_mode == cdcs.mode )
		{
			// 7/18/2017 rmd : So this means we are in the middle of the receipt, and have some missing
			// packets, that is why we didn't reboot. Let's flag that. We'll deal with the bitfield to
			// put into the returning message below.
			reportable_progress = HUB_CODE_RECEIPT_PROGRESS_partial_received;
		}
	}
	
	// ----------
	
	// 7/18/2017 rmd : BUILD A RESPONSE TO THE QUERY.

	// 7/18/2017 rmd : The header, the reportable_progress UNS_32, the bitfield if needed,
	// and the CRC.
	ldh.dlen = sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) + sizeof(UNS_32) + sizeof(cdcs.hub_packets_bitfield) + sizeof(CRC_BASE_TYPE);

	ldh.dptr = mem_malloc( ldh.dlen );

	ucp = ldh.dptr;

	// ----------
	
	// 7/18/2017 rmd : Fill the header.
	memset( &fcpth, 0x00, sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) );
	
	fcpth.PID.__routing_class = MSG_CLASS_2000e_CS3000_FLAG;

	fcpth.cs3000_msg_class = MSG_CLASS_CS3000__TO_HUB__CODE_DISTRIBUTION_ADDRESSABLE;
	
	fcpth.to_serial_number = hub_serial_number;

	// 7/18/2017 rmd : The network ID is not used to route packets.
	fcpth.to_network_id = 0;

	fcpth.mid = MID_CODE_DISTRIBUTION_hub_query_packet_ack;
	
	memcpy( ucp, &fcpth, sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) );

	ucp += sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER);

	// ----------
	
	// 7/18/2017 rmd : Now include in the progress indication.
	memcpy( ucp, &reportable_progress, sizeof(reportable_progress) );
			
	ucp += sizeof(reportable_progress);

	// ----------
	
	if( reportable_progress == HUB_CODE_RECEIPT_PROGRESS_none_received )
	{
		// 9/20/2017 rmd : The case of NONE received could mean this controller missed the original
		// init packet. Or the controller had a power failure mid-distribution.
		Alert_Message( "QUERY: received none of it - including bitfield" );
		
		// 11/2/2017 rmd : Remember in this case the bitfield should be all 0's, and I'm going to
		// make sure it is, cause maybe there was a prior attempted distribution???
		memset( cdcs.hub_packets_bitfield, 0x00, sizeof(cdcs.hub_packets_bitfield) );

		// 11/2/2017 rmd : And include it.
		memcpy( ucp, &cdcs.hub_packets_bitfield, sizeof(cdcs.hub_packets_bitfield) );

		ucp += sizeof(cdcs.hub_packets_bitfield);
	}
	else
	if( reportable_progress == HUB_CODE_RECEIPT_PROGRESS_partial_received )
	{
		Alert_Message( "QUERY: partial received - including bitfield" );
		
		// 7/18/2017 rmd : Then include the bitfield in the returned message.
		memcpy( ucp, &cdcs.hub_packets_bitfield, sizeof(cdcs.hub_packets_bitfield) );

		ucp += sizeof(cdcs.hub_packets_bitfield);
	}
	else
	{
		Alert_Message( "QUERY: fully received" );

		// 7/18/2017 rmd : We are not including the bitfield, so reduce the message size so we don't
		// send useless bytes.
		ldh.dlen -= sizeof(cdcs.hub_packets_bitfield);
		
		// 11/2/2017 rmd : Test if we did however receive some of it.
		if( (cdcs.mode == CODE_DISTRIBUTION_MODE_receiving_from_hub_main) || (cdcs.mode == CODE_DISTRIBUTION_MODE_receiving_from_hub_tpmicro) )
		{
			// 11/2/2017 rmd : This is a bit of a strange mode, we are already running the correct code,
			// but the hub is distributing to update some others on the hub. We'll keep receiving
			// partials of the distribution, or perhaps the whole thing. But if we received only some of
			// the packets we should clean our code distribution state machine and free our memory
			// allocs up. So we are able to receive again without rebooting (particularly important for
			// the main binary). If we didn't do this and the hub stopped distributing (cause everyone
			// else was happy) this controller would reboot 30 minutes from now!
			CODE_DISTRIBUTION_stop_and_cleanup();
		}
	}
	
	// ----------
	
	// 7/18/2017 rmd : Calculate the CRC.
	lcrc = CRC_calculate_32bit_big_endian( ldh.dptr, (ldh.dlen - sizeof(CRC_BASE_TYPE)) );

	// 7/18/2017 rmd : And add the crc to the packet.
	memcpy( ucp, &lcrc, sizeof(CRC_BASE_TYPE) );
	ucp += sizeof(CRC_BASE_TYPE);

	// 8/2/2017 rmd : Send the response to the port where the query came in on.
	attempt_to_copy_packet_and_route_out_another_port( pcdtqs_ptr->from_port, ldh, NULL );

	// ----------
	
	// 3/27/2014 rmd : We're done with it.
	mem_free( ldh.dptr );
}

/* ---------------------------------------------------------- */
static void process_incoming_hub_distribution_query_packet_ack( const CODE_DISTRIBUTION_TASK_QUEUE_STRUCT* const pcdtqs_ptr )
{
	// 5/22/2017 rmd : Executed within the context of this task, the CODE_DISTRIBUTION task.
	
	// ----------
	
	UNS_8	*ucp;

	FROM_COMMSERVER_PACKET_TOP_HEADER	fcpth;
	
	UNS_32	reportable_progress;
	
	UNS_8	delivered_bit_field[ HUB_BITFIELD_BYTES ];
	
	UNS_32	iii;
	
	// ----------

	// 8/2/2017 rmd : Stop the packet rate timer since we are not going to wait for it to expire
	// to trigger the transmission of the next query packet. And we can immediately issue the
	// event to send the next packet cause this function itself is called via an event on the
	// same task queue. So the send next packet event WILL NOT be processed UNTIL this function
	// ends and we return to looking at the task queue.
	xTimerStop( cdcs.transmit_packet_rate_timer, portMAX_DELAY );

	CODE_DISTRIBUTION_post_event( CODE_DISTRIBUTION_EVENT_send_next_packet );

	// ----------
	
	ucp = pcdtqs_ptr->dh.dptr;
	
	memcpy( &fcpth, ucp, sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) );

	ucp += sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER);

	// ----------
	
	memcpy( &reportable_progress, ucp, sizeof(reportable_progress) );
	ucp += sizeof(reportable_progress);
	
	// ----------
	
	if( (reportable_progress == HUB_CODE_RECEIPT_PROGRESS_none_received) || (reportable_progress == HUB_CODE_RECEIPT_PROGRESS_partial_received) )
	{
		if( reportable_progress == HUB_CODE_RECEIPT_PROGRESS_none_received )
		{
			Alert_Message( "reports none rcvd" );
		}
		else
		{
			Alert_Message( "reports partial received" );
		}
		
		memcpy( &delivered_bit_field, ucp, sizeof(delivered_bit_field) );
		ucp += sizeof(delivered_bit_field);

		// ----------
		
		// 7/18/2017 rmd : Okay we've extracted all the delivered content from the packet.
		for( iii=0; iii<HUB_BITFIELD_BYTES; iii++ )
		{
			// 7/20/2017 rmd : In the hub bitfield, set the bits for the packets this controller says it
			// did NOT receive.
			cdcs.hub_packets_bitfield[ iii ] |= ~delivered_bit_field[ iii ];
		}
	}
	else
	if( reportable_progress == HUB_CODE_RECEIPT_PROGRESS_fully_received )
	{
		Alert_Message( "reports fully rcvd" );
	}
	else
	{
		Alert_Message( "reports UNK??" );
	}
	
	// 9/20/2017 rmd : If the controller indicates it hasn't received any of it, we treat that
	// here as if it is running the correct code. Why? Well that controller needs all of the
	// packets, so just let the controller check for an update, eventually, and the hub code
	// distribution will start again later.
	
	// ----------
	
	// 6/20/2017 rmd : Regardless of all that happened here within this function, we must free
	// the memory the packet was delivered in.
	mem_free( pcdtqs_ptr->dh.dptr );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static BOOL_32 process_firmware_upgrade_after_last_packet_received( const CODE_DISTRIBUTION_TASK_QUEUE_STRUCT* const pcdtqs_ptr )
{
	CRC_BASE_TYPE	lcrc;
	
	BOOL_32		rv;
	
	// ----------
	
	rv = (false);
	
	// 8/12/2014 rmd : Check it contains one of the two valid possible MID's. Main code or
	// TPMicro code.
	if( cdcs.receive_mid == MID_CODE_DISTRIBUTION_main_data_packet )
	{
		lcrc = CRC_calculate_32bit_big_endian( cdcs.receive_ptr_to_main_app_memory, cdcs.receive_expected_size );
		
		// 2/14/2014 ajv : Verify the CRC matches what the CommServer sent us.
		if( lcrc == cdcs.receive_expected_crc )
		{
			rv = (true);
			
			// ----------
			
			// 5/8/2017 rmd : Set the flag to protect the memory space holding the binary image from
			// being freed in the case the commserver starts a second main board binary transmission
			// before the file write has completed.
			cdcs.main_board_already_received = (true);
			
			// ----------
			
			// 2/20/2014 rmd : When the write has completed a message is sent to the COMM_MNGR task
			// which triggers a main app code restart via a hard reset. We 'send' the response here. BUT
			// THE ACTUAL TRANSMISSION IS NOT GUARANTEED. It only occurs by virtue of the fact that the
			// file write is a relatively long process and performed in a low priority task. Allow the
			// serial driver to complete its quick and higher priority transmission. I say its a very
			// sure bet it will be sent by the time the file write completes.
			//
			// 3/17/2014 ajv : When using the RRE port, send the response immediately by building the
			// response and adding it to the trasmit list. This allows the Firmware Upgrade Utility to
			// close the COM port.
			//
			// 3/20/2017 rmd : And if binary arrived on the A port and this is not a controller on a
			// hub, that means the data came direct from the commserver and is to be ack'd.
			//
			// 3/20/2017 rmd : Bottom line is only confirm receipt if data came direct from the
			// commserver. Other distributions (via hub or master down a chain) are broadcast and not
			// acknowledged in this way. The code receipt is tested other ways (during scan and
			// explicitly by the hub).
			if( pcdtqs_ptr->message_class == MSG_CLASS_CS3000__FROM_COMMSERVER__CODE_DISTRIBUTION )
			{
				CENT_COMM_build_and_send_ack_with_optional_job_number( pcdtqs_ptr->from_port, MID_CODE_DISTRIBUTION_main_full_receipt_ack, JOB_NUMBER_NOT_INCLUDED, 0 );
				
				// 10/5/2017 rmd : This is the last binary received BEFORE the restart. After the last
				// binary, if this is a HUB, the commserver wants to see an END OF POLL message.
				if( CONFIG_this_controller_is_a_configured_hub() )
				{
					// 10/5/2017 rmd : MUST use a brute force method to directly FORCE the END OF POLL to be
					// sent. If we go through traditional queueing channels, the registration will be sent
					// first, and by the time that is saved to the database the controller may have rebotted
					// after savng the binary. That is no good because the commserver is really counting on this
					// END OF POLL message.
					CONTROLLER_INITIATED_attempt_to_build_and_send_the_no_more_messages_msg( (false) );
				}
			}

			// ----------
			
			// 2/25/2015 rmd : We have decided whenever a new code image is received that the unit will
			// re-register with the web server. This is the mechanism by which the database field
			// reflecting the code revision the unit is running is updated. And more so it just seemed
			// like a good practice going forward. We'll do this for both the main code binary and the
			// tpmicro code binary even though I think only the main code revision date and time are
			// held in the web server database.
			//
			// 2/25/2015 rmd : Be AWARE. This is not guaranteed to work on all code updates. This
			// request is stored in battery backed SRAM. If the code update clears the battery backed
			// variables (rarely but does happen) then this request could be lost. On the plus side
			// regarding that is that if the chain members battery backed variable was also cleared then
			// a re-registration would be requested when it was rebuilt. So just be aware if
			// re-registration is needed. Will your code update result in one?
			CONTROLLER_INITIATED_update_comm_server_registration_info();
			
			// ----------
			
			// 11/9/2015 rmd : Why do we set ALL the bits to send the pdata to the cloud here? Well we
			// discovered a problem when the code update introduces new variables AND WE ARE PART OF A
			// CHAIN. When the code runs the new variables are SET properly via the updater. The file is
			// also saved via the updater but the appropriate bits to send the changes to the cloud are
			// NOT set yet. They get set when the changes are delivered to the FOAL side. Then the SCAN
			// occurs followed by the code distribution to the slaves (immedia following the scan). Then
			// the code distribution triggers a controller RESTART before the tokens begin (remember
			// tokens are needed to deliver the pdata changes to the foal side). The slaves run the new
			// executable and update their own variables and files. Keeping master and slave variables
			// in sync. The slaves send the changes to the master but because the master detects no
			// change the bits to send to the cloud are not set at that point. And THAT IS the problem
			// we are fixing here. We agreed to always send ALL the PDATA following a main code
			// update.
			//
			// 11/16/2015 rmd : So save the files but do not start the timer. Remember we are also going
			// to queue the main board code file write too. Following the completion of the main board
			// code file write the COMM_MNGR task performs a reboot! So no reason to start the timer to
			// send the data to the cloud here. That will happen as part of the restart code.
			COMM_TEST_send_all_program_data_to_the_cloud( SEND_PDATA_TO_CLOUD_save_the_files, SEND_PDATA_TO_CLOUD_do_not_start_the_pdata_timer );
			
			// ----------
			
			// 2/14/2014 ajv : Write the binary to the flash file system.
			FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file( FLASH_INDEX_0_BOOTCODE_AND_EXE,
																	CS3000_APP_FILENAME,
																	// 2/20/2014 rmd : We use a version value of 0 here because the version value is picked from
																	// the code image during the actual write process. As is the edit_count value. These two
																	// values represent the code image time & date.
																	0,
																	cdcs.receive_ptr_to_main_app_memory,
																	cdcs.receive_expected_size,
																	NULL,
																	// 2/20/2014 rmd : The file name 'number' is not used for code image files.
																	0 );
		}
		else
		{
			// 8/21/2014 rmd : In this rare case the memory will be freed upon the arrival of the next
			// init packet or a power failure.
			Alert_Message_va( "Main App Firmware CRC failed! (saw %08x, exp %08x)", lcrc, cdcs.receive_expected_crc );
		}
	}
	else
	if( cdcs.receive_mid == MID_CODE_DISTRIBUTION_tpmicro_data_packet )
	{
		lcrc = CRC_calculate_32bit_big_endian( cdcs.receive_ptr_to_tpmicro_memory, cdcs.receive_expected_size );
		
		// 2/14/2014 ajv : Verify the CRC matches what the CommServer sent us.
		if( lcrc == cdcs.receive_expected_crc )
		{
			rv = (true);
			
			// 3/17/2014 ajv : When using the RRE port, send the response immediately by building the
			// response and adding it to the trasmit list. This allows the Firmware Upgrade Utility to
			// close the COM port.
			//
			// 3/20/2017 rmd : And if binary arrived on the A port and this is not a controller on a
			// hub, that means the data came direct from the commserver and is to be ack'd.
			//
			// 3/20/2017 rmd : Bottom line is only confirm receipt if data came direct from the
			// commserver. Other distributions (via hub or master down a chain) are broadcast and not
			// acknowledged in this way. The code receipt is tested other ways (during scan and
			// explicitly by the hub).
			if( pcdtqs_ptr->message_class == MSG_CLASS_CS3000__FROM_COMMSERVER__CODE_DISTRIBUTION )
			{
				// 10/12/2017 rmd : Do not send completed firmware receipt ACK now. If we do that the
				// commserver will immediatly begin sending the next binary (if there is one) and we will
				// ignore the init packet because the code receipt utility is occupied, still saving the
				// tpmicro bianry. Wait until the file save has completed, where we will set the cdcs mode
				// to IDLE, send the ACK, and for a HUB possibly send the END OF POLL message. So set up
				// here here to make all that occur.
				cdcs.receive_tpmicro_binary_came_from_commserver = (true);

				cdcs.receive_tpmicro_binary_came_from_port = pcdtqs_ptr->from_port;
			}
			
			// ----------
			
			// 2/25/2015 rmd : We have decided whenever a new code image is received that the unit will
			// re-register with the web server. This is the mechanism by which the database field
			// reflecting the code revision the unit is running is updated. And more so it just seemed
			// like a good practice going forward. We'll do this for both the main code binary and the
			// tpmicro code binary even though I think only the main code revision date and time are
			// held in the web server database.
			//
			// 2/25/2015 rmd : Be AWARE. This is not guaranteed to work on all code updates. This
			// request is stored in battery backed SRAM. If the code update clears the battery backed
			// variables (sometimes happens) then this request could be lost. On the plus side regarding
			// that is that if the chain members battery backed variable was also cleared then a
			// re-registration would be requested when it was rebuilt. So just be aware if
			// re-registration is needed. Will your code update result in one?
			CONTROLLER_INITIATED_update_comm_server_registration_info();
			
			// ----------
			
			// 5/8/2017 rmd : Set the flag to protect the memory space holding the binary image from
			// being freed in the case the commserver starts a second tp micro binary transmission.
			cdcs.tp_micro_already_received = (true);
			
			// 2/14/2014 ajv : Write the binary to the flash file system.
			FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file( FLASH_INDEX_0_BOOTCODE_AND_EXE,
																	TPMICRO_APP_FILENAME,
																	// 2/20/2014 rmd : We use a version value of 0 here because the version value is picked from
																	// the code image during the actual write process. As is the edit_count value. These two
																	// values represent the code image time & date.
																	0,
																	cdcs.receive_ptr_to_tpmicro_memory,
																	cdcs.receive_expected_size,
																	NULL,
																	// 2/20/2014 rmd : The file name 'number' is not used for code image files.
																	0 );
		}
		else
		{
			// 8/21/2014 rmd : In this rare case the memory will be freed upon the arrival of the next
			// init packet or a power failure.
			Alert_Message_va( "TP Micro Firmware CRC failed! (saw %08x, exp %08x)", lcrc, cdcs.receive_expected_crc );
		}
	}
	else
	{
		Alert_Message( "Invalid MID in DSS" );
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
static void free_and_step_on( void )
{
	// 8/21/2014 rmd : Called during data packet receipt error, or a CRC error. To free up
	// memory and assure the next data packet will be rejected. Takes a new init packet to again
	// rcv data packets. Free anything we can find.
	//
	// 10/16/2017 rmd : Another thing to note here, if the memory had already been freed (the
	// file save activity frees the memory!) the pointer won't be NULL, the mem_free function
	// does not NULL the pointer (I wish it would), but the result here will be if we tried to
	// previously receive a main app or tp_micro and never rebooted, but are here again trying
	// to receive and something went wrong, and pointer had been freed, this attempted mem_free
	// will generate a forced restart with pointer over/under run error. Which is okay by me, if
	// things are that messed up I want to restart.
	
	// 10/16/2017 rmd : Flawed technique - see note above.
	if( cdcs.receive_ptr_to_main_app_memory )
	{
		mem_free( cdcs.receive_ptr_to_main_app_memory );
	}

	cdcs.receive_ptr_to_main_app_memory = NULL;

	// ----------
	
	// 10/16/2017 rmd : Flawed technique - see note above.
	if( cdcs.receive_ptr_to_tpmicro_memory )
	{
		mem_free( cdcs.receive_ptr_to_tpmicro_memory );
	}

	cdcs.receive_ptr_to_tpmicro_memory = NULL;
	
	// ----------
	
	// 7/6/2017 rmd : NULL out some of the infrastructure variables.
	cdcs.receive_ptr_to_next_packet_destination = NULL;
	
	cdcs.receive_start_ptr_for_active_hub_distribution = NULL;
	
	// 9/20/2017 rmd : And set the mode to IDLE so that it doesn't appear we are in the process
	// of receiving anything.
	cdcs.mode = CODE_DISTRIBUTION_MODE_idle;
}

/* ---------------------------------------------------------- */
static void process_incoming_code_download_init_packet( const CODE_DISTRIBUTION_TASK_QUEUE_STRUCT* const pcdtqs_ptr )
{
	UNS_8								*ucp;
	
	BOOL_32								memory_available;

	FROM_COMMSERVER_PACKET_TOP_HEADER	fcpth;

	MSG_INITIALIZATION_STRUCT			lmis;
	
	UNS_32								received_code_binary_date;

	UNS_32								received_code_binary_time;
	
	BOOL_32								proceed;
	
	BOOL_32								re_transmitting;

	UNS_8								*image_ptr;
	
	// ----------

	// 6/4/2015 rmd : Keep the code receipt tally up. These bytes have already been added into
	// the rcvd_bytes total via the UART isr.
	//
	// 5/23/2017 rmd : The focus of these lines is to monitor GR data usage. In that case we
	// view these lines at the WEB, and only for the A controller, and by definition that
	// central traffic comes in via UPORT_A. So if we always tally to that port, then for the
	// -GR controller, the daily/monthly alert lines will be correct.
	SerDrvrVars_s[ UPORT_A ].stats.code_receipt_bytes += pcdtqs_ptr->dh.dlen;
	
	// ----------

	ucp = pcdtqs_ptr->dh.dptr;

	// ----------
	
	// 7/27/2017 rmd : Snatch the top header structure.
	memcpy( &fcpth, ucp, sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) );
	
	// 3/31/2014 rmd : Get to the initialization info in the incoming message.
	ucp += sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER);
	
	// ----------
	
	// 7/27/2017 rmd : Snatch the msg initialization structure.
	memcpy( &lmis, ucp, sizeof(MSG_INITIALIZATION_STRUCT) );
	
	// 3/31/2014 rmd : Bump ahead to the code revision date and time.
	ucp += sizeof(MSG_INITIALIZATION_STRUCT);
	
	// ----------
	
	// 7/18/2017 rmd : And all init packets have a code revision date and time.
	memcpy( &received_code_binary_date, ucp, sizeof(received_code_binary_date) );

	memcpy( &received_code_binary_time, ucp, sizeof(received_code_binary_time) );
	
	// ----------
	
	// 7/27/2017 rmd : A basic sanity check.
	if( fcpth.mid != lmis.mid )
	{
		Alert_Message( "CDCS: mids don't match?" );	
	}

	// 7/27/2017 rmd : A basic sanity check.
	if( pcdtqs_ptr->message_class != fcpth.cs3000_msg_class )
	{
		Alert_Message( "CDCS: classes don't match?" );	
	}

	// ----------
	
	proceed = (true);
	
	re_transmitting = (true);
	
	// ----------
	
	// 7/27/2017 rmd : Test for an expected mid.
	if( (lmis.mid != MID_CODE_DISTRIBUTION_main_init_packet) && (lmis.mid != MID_CODE_DISTRIBUTION_tpmicro_init_packet) )
	{
		Alert_Message( "CODE: unexpected mid" );
		
		proceed = (false);
	}
	
	// ----------
	
	// 7/27/2017 rmd : Here's the deal about behavior. If the message class is FROM_HUB then we
	// could be re-transmitting the code to fill in missing packets, for ourselves or others.
	// Make a variety of checks to see if that is the case.
	if( pcdtqs_ptr->message_class == MSG_CLASS_CS3000__FROM_HUB__CODE_DISTRIBUTION_BROADCAST )
	{
		// 10/18/2017 rmd : In the packet router this controller has already been screened as being
		// allowed to receive this CLASS, in other words this is a controller behind a hub.

		// ----------
		
		// 10/18/2017 rmd : As a controller behind a hub, the binary distributions are
		// binary-by-binary with a reboot in between. So this must ALWAYS be true.
		cdcs.restart_after_receiving_tp_micro_firmware = (true);
		
		// ----------
		
		// 7/27/2017 rmd : A new init pacekt could be a re-transmission effort, or not. It could be
		// the MAIN code after the TPMICRO. It could be this controller never fully received a prior
		// attempted distribution, and now the hub is sending a DIFFERENT binary. Remember past
		// attempts will stay resident in memory UNTIL a restart. That could be days or more!
		
		// 7/27/2017 rmd : If the mode does not indicate a receipt underway then we are not
		// retransmitting.
		if( (cdcs.mode == CODE_DISTRIBUTION_MODE_receiving_from_hub_main) || (cdcs.mode == CODE_DISTRIBUTION_MODE_receiving_from_hub_tpmicro) )
		{
			// 10/24/2017 rmd : Testing the MID is a bit tricky because we flip them from INIT to DATA
			// towards the end of our processing of this init packet.
			if( cdcs.receive_mid == MID_CODE_DISTRIBUTION_main_data_packet )
			{
				if( lmis.mid != MID_CODE_DISTRIBUTION_main_init_packet )
				{
					Alert_Message( "not re-transmitting: main mid wrong" );
					
					re_transmitting = (false);
				}
			}
			else
			if( cdcs.receive_mid == MID_CODE_DISTRIBUTION_tpmicro_data_packet )
			{
				if( lmis.mid != MID_CODE_DISTRIBUTION_tpmicro_init_packet )
				{
					Alert_Message( "not re-transmitting: tpmicro mid wrong" );
					
					re_transmitting = (false);
				}
			}
			else
			{
				Alert_Message( "not re-transmitting: saved mid corrupt" );
				
				re_transmitting = (false);
			}
			
			// ----------
			
			// 7/27/2017 rmd : If the saved binary date and time don't match we are not retransmitting.
			if( (cdcs.receive_code_binary_date != received_code_binary_date) || (cdcs.receive_code_binary_time != received_code_binary_time) )
			{
				Alert_Message( "not re-transmitting: date and time don't match" );
				
				re_transmitting = (false);
			}
			
			if( (cdcs.receive_expected_size != lmis.expected_size) || (cdcs.receive_expected_packets != lmis.expected_packets) )
			{
				Alert_Message( "not re-transmitting: expected size & packets don't match" );
				
				re_transmitting = (false);
			}
			
			// ----------
			
			// 7/27/2017 rmd : Check the receive pointer is not NULL.
			if( cdcs.mode == CODE_DISTRIBUTION_MODE_receiving_from_hub_main )
			{
				image_ptr = cdcs.receive_ptr_to_main_app_memory;
			}
			else
			{
				image_ptr = cdcs.receive_ptr_to_tpmicro_memory;
			}
				
			// 7/27/2017 rmd : Test that it is not NULL, which would be an error!
			if( image_ptr == NULL )
			{
				Alert_Message( "not re-transmitting: memory ptr is NULL" );
				
				re_transmitting = (false);
			}
			
			// ----------
			
			// 7/27/2017 rmd : Check the anticipated CRC's match.
			if( lmis.expected_crc != cdcs.receive_expected_crc )
			{
				Alert_Message( "not re-transmitting: crcs don't match" );
				
				re_transmitting = (false);
			}
		}
		else
		{
			Alert_Message( "not a HUB re-transmission" );
			
			re_transmitting = (false);
		}
	}
	else
	if( pcdtqs_ptr->message_class == MSG_CLASS_CS3000__FROM_COMMSERVER__CODE_DISTRIBUTION )
	{
		re_transmitting = (false);

		// 10/23/2017 rmd : NOTE - DO NOT set the "cdcs.restart_after_receiving_tp_micro_firmware"
		// variable here. In this case that variable is set when processing the "check for updates
		// ACK" in cent_comm.c. If two updates are coming we do not reboot after the tpmicro
		// arrives.
	}
	else
	if( pcdtqs_ptr->message_class == MSG_CLASS_CS3000__FROM_CONTROLLER__CODE_DISTRIBUTION )
	{
		re_transmitting = (false);

		// 10/23/2017 rmd : Anytime a controller on a chain receives the tpmicro binary it reboots.
		// So make sure to set that up to occur.
		cdcs.restart_after_receiving_tp_micro_firmware = (true);
	}
	else
	{
		proceed = (false);
	}
	
	// ----------
	
	memory_available = (false);
	
	if( proceed )
	{
		if( re_transmitting )
		{
			Alert_Message( "is a HUB re-transmission" );

			// 7/27/2017 rmd : Remember it was previously obtained.
			memory_available = (true);

			// ----------
			
			// 10/24/2017 rmd : Need to restore this one for the following section. Remember it flips
			// between the INIT packet to the DATA packet, and in the case we are re-transmitting it is
			// set to the data packet right now. Restore to INIT so next use is an expected value.
			cdcs.receive_mid = lmis.mid;
		}
		else
		{
			// 7/27/2017 rmd : Not retransmitting. That means we have to get memory and initialize all
			// the tracking variables.

			cdcs.receive_expected_size = lmis.expected_size;
		
			cdcs.receive_expected_crc = lmis.expected_crc;
		
			cdcs.receive_expected_packets = lmis.expected_packets;
			
			cdcs.receive_mid = lmis.mid;
			
			// ----------
			
			cdcs.receive_bytes_rcvd_so_far = 0;
		
			cdcs.receive_next_expected_packet = 1;
		
			cdcs.receive_ptr_to_next_packet_destination = NULL;
		
			// ----------
			
			cdcs.receive_code_binary_date = received_code_binary_date;
			
			cdcs.receive_code_binary_time = received_code_binary_time;
			
			// ----------
			
			// 7/27/2017 rmd : Since this is NOT a re-transmission clear out the received packets
			// bitfield.
			memset( cdcs.hub_packets_bitfield, 0x00, sizeof(cdcs.hub_packets_bitfield) );
			
			// ----------
			
			// 8/21/2014 rmd : First check to see if the memory was already allocated. This is being
			// done primarily because the main code image is so big and has a special allocation for it
			// to ask for another would result in no memory perhaps. Additionally this is considered
			// abnormal. Why are we receiving another init packet when the prior sequence is apparently
			// still on-going. And finally under all normal circumstance the memory should not be in use
			// when we recieve an init packet.
			if( cdcs.receive_mid == MID_CODE_DISTRIBUTION_main_init_packet )
			{
				if( !cdcs.main_board_already_received )
				{
					memory_available = mem_obtain_a_block_if_available( cdcs.receive_expected_size, (void**)&cdcs.receive_ptr_to_main_app_memory );
				}
	
				if( !memory_available )
				{
					// 8/21/2014 rmd : Make SURE the memory ptr is NULL! So if we rcv a data packet we do not
					// try to store it somewhere. We check for this NULL ptr there.
					cdcs.receive_ptr_to_main_app_memory = NULL;
				}
			}
			else
			{
				if( !cdcs.tp_micro_already_received )
				{
					memory_available = mem_obtain_a_block_if_available( cdcs.receive_expected_size, (void**)&cdcs.receive_ptr_to_tpmicro_memory );
				}
	
				if( !memory_available )
				{
					// 8/21/2014 rmd : Make SURE the memory ptr is NULL! So if we rcv a data packet we do not
					// try to store it somewhere. We check for this NULL ptr there.
					cdcs.receive_ptr_to_tpmicro_memory = NULL;
				}
			}
			
			// ----------
			
			if( !memory_available )
			{
				Alert_Message( "Code receipt memory block not available" );
				
				// 10/24/2017 rmd : This is a pretty fatal error, and we should reboot, to free the memory.
				// Likely the scenario is we are trying to receive a main board binary, have taken memory,
				// but for some reason didn't recognize the second INIT packet as a re-transmission. We will
				// never be able to get the memory again without a reboot, because there is only ONE of
				// those partitions in the system. It may be our 30 minute code receipt packet timer would
				// eventually get us, but for this confused state I feel better rebooting now.
				SYSTEM_application_requested_restart( SYSTEM_SHUTDOWN_EVENT_code_distribution_error );
			}
			
			// ----------
			
			// 6/16/2017 rmd : Set the mode based upon the packet class.
			if( pcdtqs_ptr->message_class == MSG_CLASS_CS3000__FROM_COMMSERVER__CODE_DISTRIBUTION )
			{
				cdcs.mode = CODE_DISTRIBUTION_MODE_receiving_from_commserver;
			}
			else
			if( pcdtqs_ptr->message_class == MSG_CLASS_CS3000__FROM_CONTROLLER__CODE_DISTRIBUTION )
			{
				cdcs.mode = CODE_DISTRIBUTION_MODE_receiving_from_controller;
			}
			else
			if( CONFIG_this_controller_is_behind_a_hub() && (pcdtqs_ptr->message_class == MSG_CLASS_CS3000__FROM_HUB__CODE_DISTRIBUTION_BROADCAST) )
			{
				if( cdcs.receive_mid == MID_CODE_DISTRIBUTION_main_init_packet )
				{
					cdcs.mode = CODE_DISTRIBUTION_MODE_receiving_from_hub_main;
				}
				else
				{
					cdcs.mode = CODE_DISTRIBUTION_MODE_receiving_from_hub_tpmicro;
				}
			}
			else
			{
				proceed = (false);
				
				Alert_Message_va( "Code Receipt: unexp packet class %u", pcdtqs_ptr->message_class );
			}
		}
	}
	else
	{
		// 7/27/2017 rmd : Attempt some clean up. We won't be doing a restart so try to free any
		// memory that may have been taken.
		//
		// 10/16/2017 rmd : Another thing to note here, if the memory had already been freed the
		// pointer won't be NULL, the mem_free function does not NULL the pointer (I wish it would),
		// but the result here will be if we tried to previously receive a main app or tp_micro and
		// never rebooted, but are here again trying to receive and something went wrong, and
		// pointer had been freed, this attempted mem_free will generate a forced restart with
		// pointer over/under run error. Which is okay by me, if things are that messed up I want to
		// restart.
		if( cdcs.receive_ptr_to_main_app_memory != NULL )
		{
			mem_free( cdcs.receive_ptr_to_main_app_memory );
		}

		if( cdcs.receive_ptr_to_tpmicro_memory != NULL )
		{
			mem_free( cdcs.receive_ptr_to_tpmicro_memory );
		}
	}
	
	// ----------
	
	// 8/15/2014 ajv : Alerts used to help debug comm server issues. Suppressed
	// in release.
	/*
	Alert_Message_va( "init_struct size: %d", cdcs.receive_expected_size );
	Alert_Message_va( "init_struct crc: 0x%08lX", cdcs.receive_expected_crc );
	Alert_Message_va( "init_struct packets: %d", cdcs.receive_expected_packets );
	Alert_Message_va( "init_struct mid: %d", cdcs.receive_mid );
	*/

	// ----------
	
	if( proceed && memory_available )
	{
		// 3/31/2014 rmd : NOTE - the allocated memory is freed if there is an error during receipt
		// of the data packets. If receipt is sucessful however, control of this block is given to
		// the file system when it write the code image file.
		
		// ----------
		
		if( cdcs.receive_mid == MID_CODE_DISTRIBUTION_main_init_packet )
		{
			if( re_transmitting )
			{
				Alert_Message( "MAIN CODE: receiving fill-in packets" );
			}
			else
			{
				Alert_Message_va( "MAIN CODE: receiving %d bytes (%d packets)", cdcs.receive_expected_size, cdcs.receive_expected_packets );
			}
		
			// ----------
			
			cdcs.receive_ptr_to_next_packet_destination = cdcs.receive_ptr_to_main_app_memory;
		
			cdcs.receive_start_ptr_for_active_hub_distribution = cdcs.receive_ptr_to_main_app_memory;
		
			// ----------
			
			// 6/7/2017 rmd : There are three ways code distribution takes place. Via the commserver,
			// via the master of a chain, and via the hub. Only via the commserver is addressed to a
			// specific controller, the other two ways are broadcast and therefore we cannot respond to
			// the init packet.
			if( pcdtqs_ptr->message_class == MSG_CLASS_CS3000__FROM_COMMSERVER__CODE_DISTRIBUTION )
			{
				CENT_COMM_build_and_send_ack_with_optional_job_number( pcdtqs_ptr->from_port, MID_CODE_DISTRIBUTION_main_init_ack, JOB_NUMBER_NOT_INCLUDED, 0 );
			}
		
			// ----------
			
			// 3/31/2014 rmd : Flip the mid to the DATA mid so we can test against as the data packets
			// arrive.
			cdcs.receive_mid = MID_CODE_DISTRIBUTION_main_data_packet;
		}
		else
		if( cdcs.receive_mid == MID_CODE_DISTRIBUTION_tpmicro_init_packet )
		{
			if( re_transmitting )
			{
				Alert_Message( "TPMICRO CODE: receiving fill-in packets" );
			}
			else
			{
				Alert_Message_va( "TPMICRO CODE: receiving %d bytes (%d packets)", cdcs.receive_expected_size, cdcs.receive_expected_packets );
			}
		
			// ----------
			
			cdcs.receive_ptr_to_next_packet_destination = cdcs.receive_ptr_to_tpmicro_memory;
		
			cdcs.receive_start_ptr_for_active_hub_distribution = cdcs.receive_ptr_to_tpmicro_memory;
		
			// ----------
			
			// 4/23/2014 rmd : Let the comm_server know we are available to receive the data packets.
			// However, ONLY respond if it came from the comm_server. If it's being locally distributed
			// by the master, it's being broadcast so no ACK is necessary.
			if( pcdtqs_ptr->message_class == MSG_CLASS_CS3000__FROM_COMMSERVER__CODE_DISTRIBUTION )
			{
				// 10/13/2017 rmd : ACK the INIT packet.
				CENT_COMM_build_and_send_ack_with_optional_job_number( pcdtqs_ptr->from_port, MID_CODE_DISTRIBUTION_tpmicro_init_ack, JOB_NUMBER_NOT_INCLUDED, 0 );
			}
		
			// ----------
			
			// 3/31/2014 rmd : Flip the mid to the DATA mid so we can test against as the data packets
			// arrive.
			cdcs.receive_mid = MID_CODE_DISTRIBUTION_tpmicro_data_packet;
		}

		// ----------
		
		if( (cdcs.mode != CODE_DISTRIBUTION_MODE_receiving_from_hub_main) && (cdcs.mode != CODE_DISTRIBUTION_MODE_receiving_from_hub_tpmicro) )
		{
			// 9/19/2017 rmd : For a FLOWSENSE or COMMSERVER distribution, packets are expected to
			// arrive regularly, so we wait 30 seconds for the first data packet to arrive, before
			// declaring a mistrial and rebooting.
			xTimerChangePeriod( cdcs.receive_error_timer, MS_to_TICKS(CODE_RECEIPT_ERROR_TIMER_BETWEEN_INIT_AND_FIRST_DATA_PACKET_FOR_FLOWSENSE_OR_COMMSERVER_DISTRIBUTION_MS), portMAX_DELAY );

			// 7/10/2017 rmd : AJ and I decided to not draw the dialog when a controller behind a hub is
			// receiving a code distribution. The receipt activity is designed to be wholly a background
			// activity so this should be fine. It is a sort of experiment, and we'll see if there is
			// any downside to letting the user push keys/make changes during a code receipt.
			CODE_DOWNLOAD_draw_dialog( (false) );
		}
		else
		{
			// 9/19/2017 rmd : For the HUB distribution case we are fairly lenient, allowing 5 minutes
			// for the first data packet to arrive before declaring a mistrial and rebooting.
			xTimerChangePeriod( cdcs.receive_error_timer, MS_to_TICKS(CODE_RECEIPT_ERROR_TIMER_BETWEEN_INIT_AND_FIRST_DATA_PACKET_FOR_HUB_DISTRIBUTION_MS), portMAX_DELAY );
		}
	}
	
	// ----------
	
	// 6/20/2017 rmd : Regardless of all that happened here within this function, we must free
	// the memory the packet was delivered in.
	mem_free( pcdtqs_ptr->dh.dptr );
}

/* ---------------------------------------------------------- */
static void set_bit_for_rcvd_packet( UNS_32 ppacket_number_1 )
{
	// 7/7/2017 rmd : ppacket_number_1 is a 1 based number.
	
	// ----------
	
	UNS_32	packet_number_0;
	
	// 7/5/2017 rmd : Make the packet number a 0-based count.
	packet_number_0 = ppacket_number_1 - 1;
	
	// 7/5/2017 rmd : Remember for the HUB distribution, we could be receiving the same packet
	// more than once. And every time a packet comes in we should check to see if we have
	// received all the packets. So set the bit for this packet, and then check.
	cdcs.hub_packets_bitfield[ (packet_number_0/8) ] |= (1 << (packet_number_0 % 8));
}

/* ---------------------------------------------------------- */
static BOOL_32 all_hub_code_distribution_packets_received( UNS_32 pexpected_packets_1 )
{
	// 7/5/2017 rmd : The pexpected_packets_1 is a 1 based number. And is the total packet
	// count.
	
	// ----------

	UNS_32	whole_bytes, array_index;
	
	UNS_8	partial_mask;
	
	BOOL_32	includes_a_partial;
	
	BOOL_32	rv;
	
	UNS_8	cdcs_partials[ ] = { 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff };

	// ----------
	
	// 7/5/2017 rmd : Figure how many bytes in the array should have all their bits set.
	whole_bytes = (pexpected_packets_1 / 8);
	
	// ----------
	
	// 7/6/2017 rmd : Is there a partial byte in the bitfield?
	includes_a_partial = (true);
	
	if( (pexpected_packets_1 % 8) == 0 )
	{
		includes_a_partial = (false);	
	}

	// ----------
	
	rv = (true);
	
	if( whole_bytes >= 1 )
	{
		// 7/6/2017 rmd : Then we have at least 1 whole byte to look at.
		array_index = 0;
		
		do
		{
			// 7/6/2017 rmd : Test the whole bytes.
			if( cdcs.hub_packets_bitfield[ array_index ] != 0xff )
			{
				rv = (false);
			}

			array_index += 1;
			
		} while( rv && (array_index < whole_bytes) );
	}
	
	// ----------
	
	if( includes_a_partial )
	{
		// 7/5/2017 rmd : And figure out what the partial mask is to use.
		partial_mask = cdcs_partials[ ((pexpected_packets_1 - 1) % 8) ];
		
		// 7/6/2017 rmd : Test the partial to be exactly correct.
		if( cdcs.hub_packets_bitfield[ whole_bytes ] != partial_mask )
		{
			rv = (false);
		}
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
static void process_incoming_code_download_data_packet( const CODE_DISTRIBUTION_TASK_QUEUE_STRUCT* const pcdtqs_ptr )
{
	UNS_8	*ucp;

	FROM_COMMSERVER_PACKET_TOP_HEADER	fcpth;

	BOOL_32		memory_available, class_integrity_ok;
	
	MSG_DATA_PACKET_STRUCT	packet_struct;

	UNS_32	packet_data_size;

	// ----------

	// 6/4/2015 rmd : Keep the code receipt tally up. These bytes have already been added into
	// the rcvd_bytes total via the UART isr.
	//
	// 5/23/2017 rmd : The focus of these lines is to monitor GR data usage. In that case we
	// view these lines at the WEB, and only for the A controller, and by definition that
	// central traffic comes in via UPORT_A. So if we always tally to that port, then for the
	// -GR controller, the alert lines will be correct.
	SerDrvrVars_s[ UPORT_A ].stats.code_receipt_bytes += pcdtqs_ptr->dh.dlen;
	
	// ----------

	// 8/2/2017 rmd : It is possible the controller missed the init packet or was powered down
	// for part of the distribution. In this case if we were to let this packet through for
	// processing one of the tests would fail (likely the integrity test) and make an alert line
	// for EACH packet received. For a large distribution such as the main binary this would
	// flood the alerts. So screen if we are even in the mode before further processing.
	if( (cdcs.mode == CODE_DISTRIBUTION_MODE_receiving_from_commserver) ||
		(cdcs.mode == CODE_DISTRIBUTION_MODE_receiving_from_controller) ||	
		(cdcs.mode == CODE_DISTRIBUTION_MODE_receiving_from_hub_main) ||	
		(cdcs.mode == CODE_DISTRIBUTION_MODE_receiving_from_hub_tpmicro)
	  )
	{
		ucp = pcdtqs_ptr->dh.dptr;
		
		memcpy( &fcpth, ucp, sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) );
	
		ucp += sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER);
	
		memcpy( &packet_struct, ucp, sizeof(MSG_DATA_PACKET_STRUCT) );
	
		ucp += sizeof(MSG_DATA_PACKET_STRUCT);
		
		// 6/30/2017 rmd : ucp now points to the payload.
		
		// ----------
		
		memory_available = (false);
		
		if( cdcs.receive_mid == MID_CODE_DISTRIBUTION_main_data_packet )
		{
			// 5/8/2017 rmd : Prevent further receipt if we've already received once.
			if( !cdcs.main_board_already_received )
			{
				if( cdcs.receive_ptr_to_main_app_memory && cdcs.receive_ptr_to_next_packet_destination )
				{
					memory_available = (true);
				}
			}
		}
		else
		if( cdcs.receive_mid == MID_CODE_DISTRIBUTION_tpmicro_data_packet )
		{
			// 5/8/2017 rmd : Prevent further receipt if we've already received once.
			if( !cdcs.tp_micro_already_received )
			{
				if( cdcs.receive_ptr_to_tpmicro_memory && cdcs.receive_ptr_to_next_packet_destination )
				{
					memory_available = (true);
				}
			}
		}
		
		// ----------
	
		class_integrity_ok = (false);
		
		if( cdcs.mode == CODE_DISTRIBUTION_MODE_receiving_from_commserver)
		{
			if( pcdtqs_ptr->message_class == MSG_CLASS_CS3000__FROM_COMMSERVER__CODE_DISTRIBUTION )
			{
				class_integrity_ok = (true);
			}
		}
		else
		if( cdcs.mode == CODE_DISTRIBUTION_MODE_receiving_from_controller)
		{
			if( pcdtqs_ptr->message_class == MSG_CLASS_CS3000__FROM_CONTROLLER__CODE_DISTRIBUTION )
			{
				class_integrity_ok = (true);
			}
		}
		else
		if( (cdcs.mode == CODE_DISTRIBUTION_MODE_receiving_from_hub_main) || (cdcs.mode == CODE_DISTRIBUTION_MODE_receiving_from_hub_tpmicro) )
		{
			// 6/16/2017 rmd : Only a controller on a hub should be seeing these packets, so make that
			// check too.
			if( CONFIG_this_controller_is_behind_a_hub() && (pcdtqs_ptr->message_class == MSG_CLASS_CS3000__FROM_HUB__CODE_DISTRIBUTION_BROADCAST) )
			{
				class_integrity_ok = (true);
			}
		}
		
		if( !class_integrity_ok )
		{
			Alert_Message( "Code DISTRIB: class integrity failed" );
		}
	
		// ----------
		
		if( memory_available && class_integrity_ok )
		{
			// 2/12/2014 ajv : Calculate the data size by stripping off the header, packet struct and
			// CRC. The pre- and post-ambles have already been removed.
			packet_data_size = (pcdtqs_ptr->dh.dlen - sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) - sizeof(MSG_DATA_PACKET_STRUCT) - sizeof(CRC_TYPE));
	
			if( packet_struct.mid == cdcs.receive_mid )
			{
				if( (cdcs.mode == CODE_DISTRIBUTION_MODE_receiving_from_hub_main) || (cdcs.mode == CODE_DISTRIBUTION_MODE_receiving_from_hub_tpmicro) )
				{
					// 6/16/2017 rmd : This would mean we are a controller ON A HUB, which we have already
					// tested for.
					
					// 6/16/2017 rmd : Calculate the destination for this packet.
					if( packet_struct.packet_number <= cdcs.receive_expected_packets )
					{
						// 6/30/2017 rmd : Packet numbers are 1 based. We need to develop a pointer to where this
						// packet goes. Remember for the case of the hub we could be resending any packet, in any
						// order.
						if( cdcs.receive_start_ptr_for_active_hub_distribution )
						{
							// 9/1/2017 rmd : This alert line is for development only and will definitely have to be
							// commented out.
							//Alert_Message_va( "CODE PACKET RCVD : number %u", packet_struct.packet_number );

							// 7/5/2017 rmd : Figure a ptr where to put the packet.
							cdcs.receive_ptr_to_next_packet_destination = cdcs.receive_start_ptr_for_active_hub_distribution + ((packet_struct.packet_number-1) * DESIRED_PACKET_PAYLOAD);
	
							memcpy( cdcs.receive_ptr_to_next_packet_destination, ucp, packet_data_size );
				
							// 7/5/2017 rmd : Remember for the HUB distribution, we could be receiving the same packet
							// more than once. And every time a packet comes in we should check to see if we have
							// received all the packets. So set the bit for this packet, and then check if all the
							// packets have arrived.
							set_bit_for_rcvd_packet( packet_struct.packet_number );
							
							// ----------
							
							if( all_hub_code_distribution_packets_received( cdcs.receive_expected_packets ) )
							{
								if( cdcs.receive_mid == MID_CODE_DISTRIBUTION_main_data_packet )
								{
									Alert_Message( "MAIN CODE: reception from hub completed" );
								}
								else
								{
									Alert_Message( "TPMICRO: reception from hub completed" );
								}
				
								// ----------
				
								// 2/14/2014 ajv : We've received all of the packets, so it's time to process the assembled
								// message and store it. If the CRC fails this function returns (false).
								if( !process_firmware_upgrade_after_last_packet_received( pcdtqs_ptr ) )
								{
									// 9/20/2017 rmd : If the crc failed wipe out any receipt in progress. We have to start
									// over.
									free_and_step_on();
								}
							}
							else
							{
								// 9/19/2017 rmd : Even though receiving code from a hub is designed to be a persistent
								// process, we do not allow it to persist indefintely. So we do have a generous error timer
								// set to 30 minutes waiting for another data packet, if we don't see one within that time
								// we will reboot and start clean.
								xTimerChangePeriod( cdcs.receive_error_timer, MS_to_TICKS(CODE_RECEIPT_ERROR_TIMER_BETWEEN_DATA_PACKETS_FOR_HUB_DISTRIBUTION_MS), portMAX_DELAY );
								
								// 7/10/2017 rmd : Also note, for the case of receipt from a hub there are no counter
								// updates, or pointer updates to be made, since packets can end up being duplicated on
								// retires. What matters is the bits set in the bit field, indicating which packets have
								// arrived. There is no 'next expected packet' number or pointer.
							}
						}
						else
						{
							Alert_Message( "HUB CODE RECEIPT: rcv start ptr NULL" );
				
							free_and_step_on();
						}
					}
					else
					{
						Alert_Message( "HUB CODE RECEIPT: packet # rcvd > expected" );
			
						free_and_step_on();
					}
				}
				else
				{
					// 6/16/2017 rmd : This is for code being received from the COMMSERVER or from the master
					// controller on a FLOWSENSE chain.
					if( packet_struct.packet_number == cdcs.receive_next_expected_packet )
					{
						memcpy( cdcs.receive_ptr_to_next_packet_destination, ucp, packet_data_size );
			
						cdcs.receive_bytes_rcvd_so_far += packet_data_size;
			
						// ---------------------
			
						if( (packet_struct.packet_number == cdcs.receive_expected_packets) && (cdcs.receive_bytes_rcvd_so_far == cdcs.receive_expected_size) )
						{
							// 4/22/2014 rmd : Stop the error timer. We are done.
							xTimerStop( cdcs.receive_error_timer, portMAX_DELAY );
							
							// ----------
							
							if( cdcs.receive_mid == MID_CODE_DISTRIBUTION_main_data_packet )
							{
								Alert_Message( "MAIN CODE: reception completed" );
							}
							else
							{
								Alert_Message( "TPMICRO: reception completed" );
							}
			
							// ----------
			
							// 3/27/2014 ajv : Pass a flag indicating that a restart is
							// pending. This will change the state and display a message to
							// user that the update is being applied.
							CODE_DOWNLOAD_close_dialog( (true) );
			
							// ----------
			
							// 2/14/2014 ajv : We've received all of the packets, so it's time to process the assembled
							// message and store it.
							if( !process_firmware_upgrade_after_last_packet_received( pcdtqs_ptr ) )
							{
								// 9/20/2017 rmd : If the crc failed wipe out any receipt in progress. We have to start
								// over.
								free_and_step_on();
							}
						}
						else
						{
							// 4/22/2014 rmd : Restart the error timer. We rcvd an acceptable packet.
							xTimerChangePeriod( cdcs.receive_error_timer, MS_to_TICKS(CODE_RECEIPT_ERROR_TIMER_BETWEEN_DATA_PACKETS_FOR_FLOWSENSE_OR_COMMSERVER_DISTRIBUTION_MS), portMAX_DELAY );
		
							// 2/5/2014 rmd : Wasn't the last packet. More to come. Update accumulators to recieve more.
							cdcs.receive_next_expected_packet += 1;
			
							cdcs.receive_ptr_to_next_packet_destination += packet_data_size;
						}
					}
					else
					{
						Alert_Message_va( "CODE RECEIPT: unexp packet number (saw %d; exp %d)", packet_struct.packet_number, cdcs.receive_next_expected_packet );
			
						free_and_step_on();
			
						// 4/22/2014 rmd : And let the code receipt error timer expire.
					}
				}
			}
			else
			{
				Alert_Message_va( "CODE RECEIPT: unexp MID (saw %d; exp %d)", packet_struct.mid, cdcs.receive_mid );
	
				free_and_step_on();
				
				// 4/22/2014 rmd : And let the code receipt error timer expire.
			}
	
		}  // of no memory allocated (not sure how that can happen) ... or receipt not allowed (already rcvd once)
		
	}  // of not in a receiving mode
	
	// ----------
	
	// 6/20/2017 rmd : Regardless of all that happened here within this function, we must free
	// the memory the packet was delivered in.
	mem_free( pcdtqs_ptr->dh.dptr );
}

/* ---------------------------------------------------------- */
extern void process_incoming_packet( const CODE_DISTRIBUTION_TASK_QUEUE_STRUCT* const pcdtqs_ptr )
{
	// 5/22/2017 rmd : Executed within the context of this task, the CODE_DISTRIBUTION task.
	
	// ----------
	
	// 3/31/2014 rmd : We need to find which incoming command we need to process.
	FROM_COMMSERVER_PACKET_TOP_HEADER	fcpth;
	
	memcpy( &fcpth, pcdtqs_ptr->dh.dptr, sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) );

	// ----------
	
	// 6/7/2017 rmd : Whether the packet is coming from the commserver, or a master of a chain,
	// or the hub, all code distributions use the SAME mid's. We can however use the MSG_CLASS
	// to distinguish which of the 5 activities is taking place.
	
	if( (fcpth.mid == MID_CODE_DISTRIBUTION_main_init_packet) || (fcpth.mid == MID_CODE_DISTRIBUTION_tpmicro_init_packet) )
	{
		// 6/7/2017 rmd : All FIVE distribution activities (receipt from the commserver, receipt
		// from a flowsense master, receipt from a hub, transmission by a hub, and transmission by a
		// flowsense master) END WITH A REBOOT. So if there is a code reception error, we reboot.
		// When transmission ends normally, we reboot. A sucessful code receipt, we reboot. The
		// point being, any one of these activities should ONLY START when the code distribution
		// mode is IDLE.
		//
		// 6/7/2017 rmd : We restrict code distrbution to only one activity at a time to avoid any
		// complicated resource (memory) or task interaction problems. This seems like a reasonable
		// restriction given code updates aren't supposed to happen too often.
		//
		// 6/7/2017 rmd : Also it would be pretty weird to be a hub or flowsense master distributing
		// code, and then start receiving code from the commserver. I'd just prefer to block that
		// scenario.
		//
		// 10/18/2017 rmd : However if this controller is BEHIND A HUB it needs to be able to accept
		// a retransmission, including the init packet. Allow the init packet so we can make
		// comparisons to on-going reception, to make sure we are still working on the same
		// reception.
		if( CONFIG_this_controller_is_behind_a_hub() )
		{
			// 11/2/2017 rmd : If the controller is distributing down a chain, I don't want to accept a
			// new init packet. That CAN CHANGE the code distribution MODE. Ignore any new distributions
			// from the hub. So only accept if we are idle or already receiving from the hub, thereby
			// blocking if we are distributing down our own FLOWSENSE chain!.
			if( (cdcs.mode == CODE_DISTRIBUTION_MODE_idle) || (cdcs.mode == CODE_DISTRIBUTION_MODE_receiving_from_hub_main) || (cdcs.mode == CODE_DISTRIBUTION_MODE_receiving_from_hub_tpmicro) )
			{
				process_incoming_code_download_init_packet( pcdtqs_ptr );
			}
		}
		else
		if( cdcs.mode == CODE_DISTRIBUTION_MODE_idle )
		{
			process_incoming_code_download_init_packet( pcdtqs_ptr );
		}
		else
		{
			// 6/30/2017 rmd : This likely means we were distributing code when the commserver tried to
			// send us another code update. We just drop it. It'll have to happen later.
			Alert_Message( "CODE RECEIPT: init packet ignored, already active" );
		}
	}
	else
	if( (fcpth.mid == MID_CODE_DISTRIBUTION_main_data_packet) || (fcpth.mid == MID_CODE_DISTRIBUTION_tpmicro_data_packet) )
	{
		// 6/7/2017 rmd : Take the HUB code transmission, if we missed the init packet, yet am
		// receiving a data packet, well I think the bottom line is we will drop it. Go to process
		// to find out more about what state we are in and what to do.
		process_incoming_code_download_data_packet( pcdtqs_ptr );
	}
	else
	if( fcpth.mid == MID_CODE_DISTRIBUTION_hub_query_packet )
	{
		process_and_respond_to_incoming_hub_distribution_query_packet( pcdtqs_ptr );
	}
	else
	if( fcpth.mid == MID_CODE_DISTRIBUTION_hub_query_packet_ack )
	{
		process_incoming_hub_distribution_query_packet_ack( pcdtqs_ptr );
	}
	else
	{
		// 3/31/2014 rmd : Drop it no processing. Message memory freed by calling function.

		Alert_Message( "UNK code distribution MID" );
	}
}

/* ---------------------------------------------------------- */
extern void CODE_DISTRIBUTION_make_a_copy_and_queue_incoming_packet( const UNS_32 pfrom_which_port, const DATA_HANDLE pdh, const UNS_32 pclass )
{
	CODE_DISTRIBUTION_TASK_QUEUE_STRUCT	cdtqs;

	DATA_HANDLE		packet_copy;

	// ----------
	
	// 6/7/2017 rmd : The calling task, the RCVD_DATA task, will free the packet memory, we must
	// obtain our own copy.
	packet_copy.dptr = mem_malloc( pdh.dlen );

	packet_copy.dlen = pdh.dlen;

	memcpy( packet_copy.dptr, pdh.dptr, pdh.dlen );

	// ----------
	
	cdtqs.event = CODE_DISTRIBUTION_EVENT_incoming_packet;
	
	cdtqs.message_class = pclass;
	
	cdtqs.dh = packet_copy;

	cdtqs.from_port = pfrom_which_port;

	if( xQueueSend( CODE_DISTRIBUTION_task_queue, &cdtqs, 0 ) != pdPASS )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "CODE DISTRIB queue full!" );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

