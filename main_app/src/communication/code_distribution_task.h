/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_CODE_DISTRIBUTION_TASK_H
#define _INC_CODE_DISTRIBUTION_TASK_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"comm_mngr.h"

#include	"gpio_setup.h"

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CODE_DOWNLOAD_STATE_IDLE					(0)
#define CODE_DOWNLOAD_STATE_DOWNLOADING_UPDATE		(1)
#define CODE_DOWNLOAD_STATE_DISTRIBUTING_UPDATE		(2)
#define CODE_DOWNLOAD_STATE_APPLYING_UPDATE			(3)
#define CODE_DOWNLOAD_STATE_DISTRIBUTION_COMPLETE	(4)
#define CODE_DOWNLOAD_STATE_TP_MICRO_UPDATING		(5)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/28/2014 rmd : MANY CONSIDERATIONS went into choosing the packet rate. First off this
// rate must be slower than the time it takes to transmit the 2K packet down the 38.4K baud
// dash M cable. So that no packets build at the UPORT_TP. This would be a memory
// consideration. Also we don't test if this driver is empty before rebooting. Another key
// factor in this rate is the transmission speed for the SR radios. With 3 retries at the
// master it takes a bit over 2 seconds to send the 2K over the air. (At 5 retries its 3
// some seconds.) We are now using 3 retries to manage the download time. The SR's take
// almost 20 minutes to send an 800K main code image.
//
// Memory use is also very important. If a device cannot accept the data the 2K packets at
// this rate they pile up in the serial driver. I increased the memory partition to handle
// these packets to 384. And removed the serial driver max number of queued packet
// limitiation. It is inherently limited by the memory pool.

// ----------

// Remember when you introduce a repeator at an SR radio, the over the air throughput is cut
// in half.
//
// 7/20/2016 rmd : Additionally is has been learned that the different GROUPS (min and max
// packet sizes) affects the over the air throughput. To avoid the slowest radio link from
// building up packets at a serial driver we've timed this RATE for the slowest group. That
// is group 10 because it has the smallest MIN and MAX packet sizes. It is really a problem
// if the packet rate is such that packets build up at a serial driver feeding a link.
// Picture a master feeding a slave. That then has some M's. And then finally a second
// master feeding a final slave. If that second master was on Group 10 and the first master
// was on Group say 3. Well Group 3 has a HIGHER throughput than Group 10. So if we feed the
// data faster than the Group 10 master could deliver it to the Slave the packets would pile
// up in the serial driver feeding the Group 10 master. The pile up is functionally okay.
// Where the problem lies is when the Group 3 master (the A controller) is done with the
// distribution the Group 10 master is not. However the group 10 master panel has received
// the full distribution and does a restart. So the final slave never gets the full
// distribution. We could wait for the restart but what is the point. The best and easiest
// solution I came up with is to slow the packet rate so no build up occurs even for the
// slowest SR group. UNFORTUNATELY the distribution takes 45 minutes with this 4.65 second
// delay between 2K packets.
//
// 6/21/2017 rmd : I have reviewed the timing and this 4650 number is correct, for group 10.
// The effective over-the-air data rate is about 4.5k baud. And these units can run at a
// 100% duty cycle.
//
// 9/15/2017 rmd : NOTE - we (aj and i) have remembered that we ALWAYS force the radio to be
// programmed as if there is a repeater, whether there is one or not. This will be a future
// effort, to re-introduce a user setting to use a repeater, because I think we are
// needlessly throwing away half our over the air throughput.
//
// 9/18/2017 rmd : NOTE - THE TIMING WITHOUT A REPEATER IS JUST A GUESS!!! Still needs to be
// measured with a scope once the user is allowed to program the radio without the use of a
// repeater set.
#define CODE_DISTRIBUTION_MS_BETWEEN_PACKETS_FOR_FREEWAVE_SR_NO_REPEATER					(2350)
#define CODE_DISTRIBUTION_MS_BETWEEN_PACKETS_FOR_FREEWAVE_SR_WITH_REPEATER					(4650)

#define CODE_DISTRIBUTION_EFFECTIVE_OVER_THE_AIR_BIT_RATE_FOR_FREEWAVE_SR_NO_REPEATER		(8800)
#define CODE_DISTRIBUTION_EFFECTIVE_OVER_THE_AIR_BIT_RATE_FOR_FREEWAVE_SR_WITH_REPEATER		(4400)

// ----------

// 7/20/2016 rmd : -M cable runs at 38400. This rate feeds that cable close to full time
// without accumulating packets in the TP port serial driver buffer. A value of 950ms
// accumulates packets at the main board PORT_TP serial driver. Using 1000ms does not.
#define CODE_DISTRIBUTION_MS_BETWEEN_PACKETS_FOR_M					(1000)

// ----------

// 9/15/2017 rmd : FREEWAVE LR - Speed varies with the group setting and the use of a
// repeater. As a matter of fact with a repeater in use the effective over-the-air baud rate
// is halved! The fastest is group 1, that allows us to set 7.5 seconds between packets. The
// slowest is group 6, which makes us have to set the gap between packets to 8.8 seconds.
// Remember what we are doing here is avoiding packets stacking up in the serial driver, as
// that has potential memory and device implications. I'm going to not incorporate the GROUP
// choice factor, use the the worst case group 6 timing. We will however capture when
// programming if the repeater is set in use or not.
#define CODE_DISTRIBUTION_MS_BETWEEN_PACKETS_FOR_FREEWAVE_LR_NO_REPEATER	(8800)
#define CODE_DISTRIBUTION_MS_BETWEEN_PACKETS_FOR_FREEWAVE_LR_WITH_REPEATER	(17600)

#define CODE_DISTRIBUTION_EFFECTIVE_OVER_THE_AIR_BIT_RATE_FOR_FREEWAVE_LR_NO_REPEATER	(2300)
#define CODE_DISTRIBUTION_EFFECTIVE_OVER_THE_AIR_BIT_RATE_FOR_FREEWAVE_LR_WITH_REPEATER	(1150)

// ----------

// 6/21/2017 rmd : Using two Raveon M7 STINGRAY radios, I measured our 2076 byte packets,
// take right about 4.6 seconds to come out of the receiving radio, putting the effective
// over the air data rate at about the same as the Freewave SR radio. But I want a bit of
// margin, so am setting this at 4.75 seconds, and this puts the effective over-the-air data
// rate at about 4.4KBaud.
//
// 6/30/2017 rmd : Ahhh, but at this rate, for alot of data, the radio gets very hot. So I
// suspect we need to be smarter about it. Probably a rate that depends on the amount of
// data that we need to send.
#define CODE_DISTRIBUTION_MS_BETWEEN_PACKETS_FOR_RAVEON_LR			(4750)

#define CODE_DISTRIBUTION_EFFECTIVE_OVER_THE_AIR_BIT_RATE_FOR_RAVEON_LR	(4400)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CODE_DISTRIBUTION_MODE_idle		 						(0)

#define CODE_DISTRIBUTION_MODE_receiving_from_commserver		(1)

#define CODE_DISTRIBUTION_MODE_receiving_from_controller		(2)

#define CODE_DISTRIBUTION_MODE_not_used_3						(3)

#define CODE_DISTRIBUTION_MODE_flowsense_transmitting_main		(4)

#define CODE_DISTRIBUTION_MODE_flowsense_transmitting_tpmicro	(5)

#define CODE_DISTRIBUTION_MODE_hub_transmitting_main			(6)

#define CODE_DISTRIBUTION_MODE_hub_transmitting_tpmicro			(7)

#define CODE_DISTRIBUTION_MODE_receiving_from_hub_main			(8)

#define CODE_DISTRIBUTION_MODE_receiving_from_hub_tpmicro		(9)

// ----------

#define CODE_TRANSMITTING_STATE_promised										(0x0010)

#define CODE_TRANSMITTING_STATE_beginning_a_fresh_transmission					(0x0020)

#define CODE_TRANSMITTING_STATE_restarting_transmission_for_hub					(0x0030)

#define CODE_TRANSMITTING_STATE_building_data_packets							(0x0040)

#define CODE_TRANSMITTING_STATE_sending_plus_plus_plus							(0x0050)

#define CODE_TRANSMITTING_STATE_reading_temperature								(0x0051)

#define CODE_TRANSMITTING_STATE_sending_exit									(0x0052)

#define CODE_TRANSMITTING_STATE_waiting_return_to_normal_operation				(0x0053)

#define CODE_TRANSMITTING_STATE_waiting_for_serial_drivers_to_empty				(0x0060)

#define CODE_TRANSMITTING_STATE_waiting_for_slaves_to_reboot					(0x0070)

#define CODE_TRANSMITTING_STATE_querying_those_in_the_hub_list					(0x0080)


// ----------

// 6/16/2017 rmd : For the HUB, each byte handles 8 packets, each packet is 2048 bytes of
// payload, so the 128 byte array handles a 2MByte code image binary.
#define	HUB_BITFIELD_BYTES	(128)

// ----------

typedef struct
{
	// 6/7/2017 rmd : Are we receiving, or transmitting, or idle.
	UNS_32	mode;
	
	// ----------
	
	// 6/7/2017 rmd : RECEIVING CONTROL VARIABLES
	
	// 5/8/2017 rmd : After receipt we want to PROTECT the memory holding the binary image from
	// an accidental/additional init or data packet receipt, which would result in corruption of
	// the image we are about to save. This scenario occurred: we received the full main board
	// binary from the commserver, then not clear why, but, the commserver immediately began a
	// second distribution, and when the init packet was received we FREED the memory, as part
	// of preparing to receive a new binary. Well, freeing the memory results in over-writing
	// the memory space (that what we do when freeing), so the binary the was written was
	// CORRUPT and NON-BOOTABLE. I actually saw this happen! Hence the introduction of these
	// flags to protect against a second receipt of either the main board or the tp micro
	// binary. We can only receive it once, and then MUST reboot to be able to recevice again.

	BOOL_32			tp_micro_already_received;
	
	BOOL_32			main_board_already_received;
	

	// 7/11/2017 rmd : For controllers receiving a distribution from their hub, this bitfield
	// serves to mark the received packets, one bit per packet of course. For the hub itself
	// this bitfield controls which packets NEED TO BE SENT. Of course when the distribution
	// first begins all the bits for the number of packets to be sent are set. Then we take an
	// assesment of who still needs what packets, and this bit field is used to collect that
	// information, so that once again this bitfield can manage which packets need to be sent.
	UNS_8			hub_packets_bitfield[ HUB_BITFIELD_BYTES ];

	UNS_32			receive_expected_size;

					// 2/4/2014 rmd : The full crc of the binary code image.
	UNS_32			receive_expected_crc;
	
	// 7/18/2017 rmd : The binary revision info. For the binary currently being received.
	// Introduced to support the hub transmission process.
	UNS_32			receive_code_binary_date;

	UNS_32			receive_code_binary_time;


	UNS_16			receive_expected_packets;

	UNS_16			receive_mid;
	

	
	// 6/16/2017 rmd : Starts at 1, so a 1-based count, not 0-based.
	UNS_32			receive_next_expected_packet;

	UNS_32			receive_bytes_rcvd_so_far;

	UNS_8			*receive_ptr_to_next_packet_destination;

					// 8/21/2014 rmd : We need pointers for each the tpmicro and the main board code images. To
					// support back-to-back downloads from the comm_server. Meaning when the tpmicro code comes
					// first and then the main board code immediately behind it. In that case we need the
					// tpmicro pointer and memory to remain intact until the file write has completed.
	UNS_8			*receive_ptr_to_tpmicro_memory;

	UNS_8			*receive_ptr_to_main_app_memory;
	
					// 6/30/2017 rmd : Used to mark the beginning of the memory block a hub distribution is
					// being received into.
	UNS_8			*receive_start_ptr_for_active_hub_distribution;


					// 4/22/2014 rmd : If during the course of code receipt the packets fail to arrive, go out
					// of order, or the message suffers some other type of error this timer will fire and
					// restart the controller. The restart is our means of error recovery. This timer will be
					// set to 30 seconds and restarted at each packet receipt.
	xTimerHandle	receive_error_timer;

					// 6/2/2014 ajv : When the controller checks for updates, the CommServer
					// checks both the CS3000 and TP Micro firmware files for updates. If both
					// are identified to be new, the TP Micro file is sent and is followed up by
					// the CS3000 file. To accomplish this, we'll suppress the restart once the
					// TP Micro is saved to the file. That way, the connection stays open to
					// receive the CS3000 firmware and, once received, the controller restarts,
					// updating the TP Micro on power-up.
	BOOL_32			restart_after_receiving_tp_micro_firmware;
	
					// 10/12/2017 rmd : Used to determine if after the TPMICRO file save we need to send an ACK,
					// and possibly an END OF POLL. The port is always known to be UPORT_A, but to be formal
					// capture the port too.
	BOOL_32			receive_tpmicro_binary_came_from_commserver;
	UNS_32			receive_tpmicro_binary_came_from_port;
	
	// ----------
	
	// 6/7/2017 rmd : TRANSMITTING CONTROL VARIABLES
	
					// 5/31/2017 rmd : So we can track are we building for the hub or for the comm_mngr.
	UNS_32			transmit_packet_class;
	
	UNS_8			*transmit_data_start_ptr;

	UNS_8			*transmit_data_end_ptr;

	UNS_32			transmit_total_packets_for_the_binary;

	UNS_32			transmit_current_packet_to_send;

					// 8/7/2017 rmd : An on-going count, used to count packets for the hub, so we know when to
					// go read the temperature. We read the temp every XXth packet. Being an ongoing count, also
					// used during hub RETRANSMISSION to decide should we give up after xxx packets.
	UNS_32			transmit_packets_sent_so_far;
	
					// 7/27/2017 rmd : For checking the binary image is still intact, when a hub is coming
					// around to re-transmit some blocks.
	UNS_32			transmit_expected_crc;

					// 7/27/2017 rmd : Introduced to support hub re-transmission logic. Needed to know the
					// length. So we capture it upon the initial transmission attempt.
	UNS_32			transmit_length;

					// 3/31/2014 rmd : Used to guide the behavior from the init state, through the data transfer
					// state, to finally waiting for the serial driver to empty.
	UNS_32			transmit_state;

					// 3/31/2014 rmd : Set when building the init packet. Used during data packet construction.
	UNS_32			transmit_init_mid;

	UNS_32			transmit_data_mid;

	UNS_32			transmit_accumulated_reboot_ms;
	
					// 7/20/2016 rmd : This value controls the delay between 2Kbyte packets during the code
					// distribution down the chain. During the scan, any controller with an SR, will be
					// recognized when looking at their scan_resp, and that will cause us to adjust this to the
					// SR value which is MUCH longer. This is initialized when the scan begins and potentially
					// adjusted as the scan responses arrive.
	UNS_32			transmit_chain_packet_rate_ms;

					// 6/20/2017 rmd : One rate variable for each of the two transmit processes, to allow
					// setting of one without affecting the other, which might happen if say a scan was
					// attempted while hub was distributing.
	UNS_32			transmit_hub_packet_rate_ms;
	
					// 7/11/2017 rmd : A counter to which 3000 controller in the hub list we are querying about
					// their receipt of the code distribution.
	UNS_32			transmit_hub_query_list_count;
	
	// ----------

					// 5/25/2017 rmd : Timer used to manage the packet transfer rate, then to wait for
					// controllers to restart, then to manange querying the controllers, and then to manage
					// repeat all but the first step as needed.
	xTimerHandle	transmit_packet_rate_timer;
	
	// ----------

	// 5/25/2017 rmd : HUB Code distribution related variables.
	
	// 9/26/2017 rmd : Need the hub list to perform a hub code distribution. Used to check each
	// controller to see who needs what packets.
	BOOL_32		hub_code__list_received;
	
	// 9/26/2017 rmd : Before a hub code distribution is allowed to start, allow the comm
	// manager to get the chain up and running, and signal there is no code distribution taking
	// place down the chain.
	BOOL_32		hub_code__comm_mngr_ready;
	
	// ----------
	
	// 3/6/2017 rmd : Temporary storage for the hub, to support the somewhat complicated process
	// of performing a restart and knowing what code distribution(s) are needed.
	UNS_32		restart_info_flag_for_hub_code_distribution;
	
} CODE_DISTRIBUTION_CONTROL_STRUCT;


extern CODE_DISTRIBUTION_CONTROL_STRUCT		cdcs;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CODE_DISTRIBUTION_EVENT_incoming_packet								(0x1111)

#define	CODE_DISTRIBUTION_EVENT_main_binary_file_read_complete				(0x1122)

#define	CODE_DISTRIBUTION_EVENT_tpmicro_binary_file_read_complete			(0x1133)

#define	CODE_DISTRIBUTION_EVENT_tpmicro_binary_file_read_for_isp_complete	(0x1144)

#define	CODE_DISTRIBUTION_EVENT_main_binary_file_write_complete				(0x1155)

#define	CODE_DISTRIBUTION_EVENT_tpmicro_binary_file_write_complete			(0x1166)

#define	CODE_DISTRIBUTION_EVENT_send_next_packet							(0x1177)

#define	CODE_DISTRIBUTION_EVENT_set_chain_distribution_packet_rate			(0x1188)

#define	nlu_CODE_DISTRIBUTION_EVENT_set_hub_distribution_packet_rate		(0x1199)

#define CODE_DISTRIBUTION_EVENT_hub_list_arrived							(0x11aa)

#define CODE_DISTRIBUTION_EVENT_commmngr_ready								(0x11bb)

#define	CODE_DISTRIBUTION_EVENT_specified_string_found						(0x11cc)

#define	CODE_DISTRIBUTION_EVENT_cr_lf_string_found							(0x11dd)

#define	CODE_DISTRIBUTION_EVENT_specified_termination_found					(0x11ee)

typedef struct
{
	// To identify the event.
	UNS_32			event;

	// 6/7/2017 rmd : Include the routing class when used to deliver packets to the task.
	UNS_32			message_class;
	
	// 6/7/2017 rmd : To provide a handle to packets delivered to the task. Or a handle to a
	// code binary read from the file system.
	DATA_HANDLE		dh;
	
	// 6/16/2017 rmd : Port the packet arrived on.
	UNS_32			from_port;

	// 6/14/2017 rmd : Used to set the code distribution packet rate.
	UNS_32			packet_rate_ms;

} CODE_DISTRIBUTION_TASK_QUEUE_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void CODE_DOWNLOAD_draw_tp_micro_update_dialog( void );

extern void CODE_DOWNLOAD_draw_dialog( const BOOL_32 puploading_firmware );

extern void FDTO_CODE_DOWNLOAD_update_dialog( void );

extern void CODE_DOWNLOAD_close_dialog( const BOOL_32 prestart_pending );

// ----------

extern BOOL_32 CODE_DISTRIBUTION_there_are_3000s_on_the_hub_list( void );

// ----------

extern BOOL_32 CODE_DISTRIBUTION_comm_mngr_should_be_idle( void );

// ----------

extern BOOL_32 CODE_DISTRIBUTION_hub_should_report_busy_to_the_commserver( void );

extern BOOL_32 CODE_DISTRIBUTION_try_to_start_a_code_distribution( UNS_32 prequested_mode );


extern void CODE_DISTRIBUTION_task( void *pvParameters );

// ----------

extern void CODE_DISTRIBUTION_post_event_with_details( CODE_DISTRIBUTION_TASK_QUEUE_STRUCT *pq_item_ptr );

extern void CODE_DISTRIBUTION_post_event( UNS_32 pevent );


extern void CODE_DISTRIBUTION_stop_and_cleanup( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

