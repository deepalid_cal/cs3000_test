/* ---------------------------------------------------------- */

#ifndef _INC_CENT_COMM_H
#define _INC_CENT_COMM_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	"battery_backed_vars.h"

#include	"cal_td_utils.h"

#include	"foal_defs.h"

#include	"lpc_types.h"

#include	"comm_utils.h"

#include	"gpio_setup.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/10/2014 ajv : Bits used to identify which types of weather data was
// sent to and received from the comm server during weather sharing.
#define WEATHER_DATA_HAS_ET		(0)
#define WEATHER_DATA_HAS_RAIN	(1)

// ----------

// 9/4/2015 rmd : Revision history.
//
// 9/4/2015 rmd : Moving from 4 to 5. Adds the clear pdata varaible to the strucutre.
//
// 4/19/2017 rmd : Moving from 5 to 6. In all previous structure versions we included the 48
// character controller name field, in the CHAIN_MEMBERS_SHARED_STRUCT portion (in the
// BOX_CONFIGURATION_STRUCT), this field was added way back during early development. This
// has been unnecessary for some time, since we added the controller names array, all 12, to
// the network file. And that is the actual sync'd variable that should, and is broadly
// referenced in the code when you want a box name.
#define	REGISTRATION_STRUCTURE_REVISION		(6)

// ----------

typedef struct
{
	// 8/23/2013 rmd : Provide a structure version number so comm_server knows how to parse.
	UNS_16					structure_version;
	
	// ----------

	DATE_TIME				controller_date_time;
	
	// ----------

	char					main_code_revision_string[ 48 ];
	
	// ----------

	char					tp_micro_code_revision_string[ 48 ];

	// ----------

	CHAIN_MEMBERS_SHARED_STRUCT		members[ MAX_CHAIN_LENGTH ];
	
	// ----------
	
	// 9/4/2015 rmd : Indication to comm server to clear the pdata associated with this
	// controller. Maybe also clears report data. Added in the transition from revision 4 to
	// 5.
	UNS_8					commserver_to_clear_data;
	
} COMM_SERVER_REGISTRATION_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

	#define		JOB_NUMBER_INCLUDED			(true)
	#define		JOB_NUMBER_NOT_INCLUDED		(false)
	extern void CENT_COMM_build_and_send_ack_with_optional_job_number( const UNS_32 pport, const UNS_32 presponse_cmd, const BOOL_32 pinclude_job_number, const UNS_32 pjob_number );
	
	extern void commserver_msg_receipt_error_timer_callback( xTimerHandle pxTimer );
	
	extern void CENT_COMM_process_incoming_packet_from_comm_server( const UNS_32 pfrom_which_port, const DATA_HANDLE pdh, const UNS_32 pclass );
	
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

