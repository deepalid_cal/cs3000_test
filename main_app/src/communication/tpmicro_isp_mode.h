/* file = tpmicro_isp_mode.h      rmd  */

/* ---------------------------------------------------------- */

#ifndef _INC_TPMICRO_ISP_MODE_H
#define _INC_TPMICRO_ISP_MODE_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
											 
#include	"comm_mngr.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define ISP_STATE_question_mark						(10)

#define ISP_STATE_synchronized						(20)

#define ISP_STATE_esc_to_clear						(30)

#define ISP_STATE_frequency							(40)

#define ISP_STATE_echo_off							(50)

#define ISP_STATE_part_id							(60)

#define ISP_STATE_read_file							(70)

#define ISP_STATE_unlock							(80)

#define ISP_STATE_prepare							(90)

#define ISP_STATE_erase								(100)

#define ISP_STATE_write_to_sram						(110)

#define ISP_STATE_uuencode_and_send_one_line		(120)

#define ISP_STATE_uuencode_send_checksum			(130)

#define ISP_STATE_copy_to_flash						(140)

#define ISP_STATE_go_execute						(150)



#define ISP_STATE_abandon							(900)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */




extern void TPMICRO_COMM_kick_out_the_next_isp_tpmicro_msg( COMM_MNGR_TASK_QUEUE_STRUCT *pcmqs );




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

