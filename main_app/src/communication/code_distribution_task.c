/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"code_distribution_task.h"

#include	"code_distribution_transmit.h"

#include	"code_distribution_receive.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"cent_comm.h"

#include	"configuration_controller.h"

#include	"configuration_network.h"

#include	"cs_mem.h"

#include	"dialog.h"

#include	"flash_storage.h"

#include	"packet_definitions.h"

#include	"wdt_and_powerfail.h"

#include	"d_process.h"

#include	"packet_router.h"

#include	"controller_initiated.h"

#include	"serport_drvr.h"

#include	"e_comm_test.h"

#include	"flowsense.h"

#include	"tpmicro_comm.h"

#include	"device_LR_RAVEON.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


CODE_DISTRIBUTION_CONTROL_STRUCT	cdcs;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CODE_DOWNLOAD_PROGRESS_BAR_MIN		(0)
#define	CODE_DOWNLOAD_PROGRESS_BAR_MAX		(200)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void CODE_DOWNLOAD_draw_tp_micro_update_dialog( void )
{
	GuiVar_CodeDownloadPercentComplete = 100;

	GuiVar_CodeDownloadProgressBar = CODE_DOWNLOAD_PROGRESS_BAR_MAX;

	// ---------------------

	GuiVar_CodeDownloadUpdatingTPMicro = (true);

	// ---------------------

	GuiVar_CodeDownloadState = CODE_DOWNLOAD_STATE_TP_MICRO_UPDATING;

	DIALOG_draw_ok_dialog( GuiStruct_dlgDownloadingFirmware_0 );
}

/* ---------------------------------------------------------- */
extern void CODE_DOWNLOAD_draw_dialog( const BOOL_32 puploading_firmware )
{
	#if CODE_DOWNLOAD_SHOW_PROGRESS_DIALOG

		GuiVar_CodeDownloadPercentComplete = 0;
	
		GuiVar_CodeDownloadProgressBar = CODE_DOWNLOAD_PROGRESS_BAR_MIN;
	
		// ---------------------
	
		GuiVar_CodeDownloadUpdatingTPMicro = (false);
	
		// ---------------------
	
		if( puploading_firmware == (true) )
		{
			GuiVar_CodeDownloadState = CODE_DOWNLOAD_STATE_DISTRIBUTING_UPDATE;
		}
		else
		{
			GuiVar_CodeDownloadState = CODE_DOWNLOAD_STATE_DOWNLOADING_UPDATE;
		}
	
		DIALOG_draw_ok_dialog( GuiStruct_dlgDownloadingFirmware_0 );

	#elif defined NDEBUG
	
		// 5/16/2016 rmd : To prevent accidental build of release with code distribution dialog
		// disabled.
		#if !CODE_DOWNLOAD_DEFEAT_PROGRESS_DIALOG_RELEASE_BUILD_SAFETY
			#error COMPILING RELEASE CODE WITH CODE DISTRIBUTION DIALOG DISABLED! NOT ALLOWED!
		#endif
		
	#endif
}

/* ---------------------------------------------------------- */
extern void FDTO_CODE_DOWNLOAD_update_dialog( void )
{
	#if CODE_DOWNLOAD_SHOW_PROGRESS_DIALOG

		GuiVar_SpinnerPos = ((GuiVar_SpinnerPos + 1) % 4);
	
		// ---------------------
	
		if( GuiVar_CodeDownloadState == CODE_DOWNLOAD_STATE_DISTRIBUTING_UPDATE )
		{
			GuiVar_CodeDownloadPercentComplete = ((cdcs.transmit_current_packet_to_send * 100) / cdcs.transmit_total_packets_for_the_binary);
		}
		else if( GuiVar_CodeDownloadState == CODE_DOWNLOAD_STATE_DOWNLOADING_UPDATE )
		{
			GuiVar_CodeDownloadPercentComplete = ((cdcs.receive_next_expected_packet * 100) / cdcs.receive_expected_packets);
		}
	
		GuiVar_CodeDownloadProgressBar = (2 * GuiVar_CodeDownloadPercentComplete);
	
		// ---------------------
	
		FDTO_DIALOG_redraw_ok_dialog();

	#elif defined NDEBUG
	
		// 5/16/2016 rmd : To prevent accidental build of release with code distribution dialog
		// disabled.
		#if !CODE_DOWNLOAD_DEFEAT_PROGRESS_DIALOG_RELEASE_BUILD_SAFETY
			#error COMPILING RELEASE CODE WITH CODE DISTRIBUTION DIALOG DISABLED! NOT ALLOWED!
		#endif
		
	#endif
}

/* ---------------------------------------------------------- */
extern void CODE_DOWNLOAD_close_dialog( const BOOL_32 prestart_pending )
{
	#if CODE_DOWNLOAD_SHOW_PROGRESS_DIALOG

		GuiVar_CodeDownloadPercentComplete = 100;
	
		GuiVar_CodeDownloadProgressBar = CODE_DOWNLOAD_PROGRESS_BAR_MAX;
	
		// ---------------------
	
		switch( GuiVar_CodeDownloadState )
		{
			case CODE_DOWNLOAD_STATE_DOWNLOADING_UPDATE:
				GuiVar_CodeDownloadState = CODE_DOWNLOAD_STATE_APPLYING_UPDATE;
				break;
	
			case CODE_DOWNLOAD_STATE_DISTRIBUTING_UPDATE:
				GuiVar_CodeDownloadState = CODE_DOWNLOAD_STATE_DISTRIBUTION_COMPLETE;
				break;
	
			default:
				GuiVar_CodeDownloadState = CODE_DOWNLOAD_STATE_IDLE;
		}
	
		// ---------------------
	
		if( prestart_pending == (true) )
		{
			DISPLAY_EVENT_STRUCT	lde;
	
			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = (void*)&FDTO_DIALOG_redraw_ok_dialog;
			Display_Post_Command( &lde );
		}
		else
		{
			DIALOG_close_ok_dialog();
		}
		
	#elif defined NDEBUG
	
		// 5/16/2016 rmd : To prevent accidental build of release with code distribution dialog
		// disabled.
		#if !CODE_DOWNLOAD_DEFEAT_PROGRESS_DIALOG_RELEASE_BUILD_SAFETY
			#error COMPILING RELEASE CODE WITH CODE DISTRIBUTION DIALOG DISABLED! NOT ALLOWED!
		#endif
		
	#endif
}

/* ---------------------------------------------------------- */
extern BOOL_32 CODE_DISTRIBUTION_there_are_3000s_on_the_hub_list( void )
{
	BOOL_32	rv, now_2000;
	
	UNS_32	how_many, iii;
	
	UNS_32	sn_3000;
	
	rv = (false);
	
	// ----------
	
	// 4/30/2017 rmd : The hub list queue is protected by a MUTEX. Always need to take before
	// working with this queue.
	xSemaphoreTake( router_hub_list_MUTEX, portMAX_DELAY );

	// ----------
	
	// 4/30/2017 rmd : The list always starts with 3000's first, till the divider is hit.
	now_2000 = (false);
	
	how_many = uxQueueMessagesWaiting( router_hub_list_queue );
	
	for( iii=0; iii<how_many; iii++ )
	{
		// 4/30/2017 rmd : If we go through the entire queue pulling from the front and writing to
		// the back the queue will remain in its original order witht he same number of items. Keep
		// in mind, we ALWAYS have to run through the ENTIRE list, even if we find what we are
		// looking for part way through, to keep the list intact.
		
		// 6/19/2017 rmd : Do not wait for a queue item, there is no task adding to the queue except
		// for when a message comes from the commserver. Our goal is to pull the items that are on
		// the queue at this time. Not wait for future items that are added to the list.
		if( xQueueReceive( router_hub_list_queue, &sn_3000, 0 ) )
		{
			// 5/1/2017 rmd : This is an optimization, time wise, for this function. Once we have found
			// a match, stop testing.
			if( !rv )
			{
				// 4/30/2017 rmd : Did we hit the divider between 3000 and 2000?
				if( sn_3000 == 0xffffffff )
				{
					now_2000 = (true);
				}
	
				if( !now_2000 )
				{
					// 9/26/2017 rmd : If we get here there is at least ONE 3000 on the list, and that is all we
					// care about.
					rv = (true);
				}
			}

			// 4/30/2017 rmd : Always write them back onto the queue. Do not wait if the queue is full,
			// nothing is removing items.
			xQueueSendToBack( router_hub_list_queue, &sn_3000, 0 );
		}
		else
		{
			// 4/30/2017 rmd : This should NEVER happen .... i think.
			Alert_Message( "CODE DIST: hub list ERROR" );
		}
	}
		
	// ----------
	
	xSemaphoreGive( router_hub_list_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
BOOL_32 CODE_DISTRIBUTION_try_to_start_a_code_distribution( UNS_32 prequested_mode )
{
	// 12/10/2018 rmd : Executes in the context of the comm_mngr task, when procesing incoming
	// message from the commserver, and also following a scan to attempt a flowsense chain
	// distribution.

	// ----------
	
	BOOL_32	rv;
	
	rv = (false);
	
	xSemaphoreTakeRecursive( code_distribution_control_structure_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	// 5/31/2017 rmd : DO NOT try to start a FLOWSENSE code distribution, or a HUB code
	// distribution, if some other code distribution activity is taking place. As we share
	// memory and control structures, we are keeping all code distribution activities mutually
	// exclusive to reduce complexity.
	if( cdcs.mode == CODE_DISTRIBUTION_MODE_idle && !in_device_exchange_hammer )
	{
		// 7/20/2017 rmd : For all modes, flowsense or hub, this function is used only for the fresh
		// transmission, that is when we are starting a code distribution from scratch.
		cdcs.transmit_state = CODE_TRANSMITTING_STATE_beginning_a_fresh_transmission;
		
		// ----------
		
		switch( prequested_mode )
		{
			case CODE_DISTRIBUTION_MODE_flowsense_transmitting_main:

				rv = (true);

				cdcs.mode = CODE_DISTRIBUTION_MODE_flowsense_transmitting_main;
				
				FLASH_STORAGE_request_code_image_file_read( CS3000_APP_FILENAME, FLASH_TASK_EVENT_read_code_image_file_to_memory_for_code_transmit );

				break;
				

			case CODE_DISTRIBUTION_MODE_flowsense_transmitting_tpmicro:

				rv = (true);

				cdcs.mode = CODE_DISTRIBUTION_MODE_flowsense_transmitting_tpmicro;
				
				FLASH_STORAGE_request_code_image_file_read( TPMICRO_APP_FILENAME, FLASH_TASK_EVENT_read_code_image_file_to_memory_for_code_transmit );
				
				break;
				

			case CODE_DISTRIBUTION_MODE_hub_transmitting_main:
			case CODE_DISTRIBUTION_MODE_hub_transmitting_tpmicro:

				if( CONFIG_this_controller_is_a_configured_hub() )
				{
					// 6/1/2017 rmd : If the comm_mngr has given the go ahead, and the hub list has been
					// received, THEN we may begin any pending hub code distribution.
					if( cdcs.hub_code__comm_mngr_ready && cdcs.hub_code__list_received )
					{
						// 11/1/2017 rmd : Check if it is RAVEON that the hub has the correct M7 radio.
						if( (config_c.port_B_device_index == COMM_DEVICE_LR_RAVEON) && (config_c.purchased_options.port_b_raveon_radio_type != RAVEON_RADIO_ID_M7) )
						{
							// 11/1/2017 rmd : OKAY so ... this is a HUB, running RAVEON LR radios, and the radio is NOT
							// AN M7. That is not a supported configuration because the serial port on the stupid M5
							// times out and floats the CTS output, so we as a hub cannot forward the polls out the
							// radio! Also, during the code distribution, we have to read the temperature during the
							// 1Mbyte transmission. However do not clear the flags that indicate we need to do the
							// distribution, just alert. By not clearing the flags when the condition is corrected the
							// distribution will begin.
							Alert_Message_va( "HUB WARNING: WITHOUT AN M7 - WILL NOT WORK!, %u, %u", config_c.port_B_device_index, config_c.purchased_options.port_b_raveon_radio_type );

							// ----------
							
							// 8/6/2018 rmd : If we do not have the proper LR device, we want to clear the flags and
							// abandon any pending code distribution. This was seen to be a tremendous problem at San
							// Ramon. We had a bug where the radios were not properly being detected as M7 radios, and
							// this led to a stuck state where the distribution could not proceed because of the
							// incorrect radio, yet the controller kept reporting to the hub it was busy. It just sat
							// there reporting busy forever! Locked up the whole city! This prevents that state.
							weather_preserves.hub_needs_to_distribute_main_binary = (false);

							weather_preserves.hub_needs_to_distribute_tpmicro_binary = (false);
						}
						else
						{
							// 5/26/2017 rmd : Then, if there are 3000's on the hub list, we are going to START the hub
							// code distribution!
							if( CODE_DISTRIBUTION_there_are_3000s_on_the_hub_list() )
							{
								// 12/10/2018 rmd : Figure out what packet repeat rate to use.
								if( config_c.port_B_device_index == COMM_DEVICE_LR_FREEWAVE )
								{
									if( config_c.purchased_options.port_b_freewave_lr_set_for_repeater )
									{
										cdcs.transmit_hub_packet_rate_ms = CODE_DISTRIBUTION_MS_BETWEEN_PACKETS_FOR_FREEWAVE_LR_WITH_REPEATER;
									}
									else
									{
										cdcs.transmit_hub_packet_rate_ms = CODE_DISTRIBUTION_MS_BETWEEN_PACKETS_FOR_FREEWAVE_LR_NO_REPEATER;
									}
								}
								else
								if( config_c.port_B_device_index == COMM_DEVICE_LR_RAVEON )
								{
									cdcs.transmit_hub_packet_rate_ms = CODE_DISTRIBUTION_MS_BETWEEN_PACKETS_FOR_RAVEON_LR;	
								}
								else
								{
									if( config_c.purchased_options.port_b_freewave_sr_set_for_repeater )
									{
										cdcs.transmit_hub_packet_rate_ms = CODE_DISTRIBUTION_MS_BETWEEN_PACKETS_FOR_FREEWAVE_SR_WITH_REPEATER;
									}
									else
									{
										cdcs.transmit_hub_packet_rate_ms = CODE_DISTRIBUTION_MS_BETWEEN_PACKETS_FOR_FREEWAVE_SR_NO_REPEATER;
									}
								}

								// ----------
								
								// 12/10/2018 rmd : Read the appropriate binary file into memory.
								if( weather_preserves.hub_needs_to_distribute_main_binary )
								{
									rv = (true);
		
									// 6/14/2017 rmd : Correct the mode, as the calling function just uses a generic main_binary
									// mode to get things going. Now we specifically know what we are going to distribute.
									cdcs.mode = CODE_DISTRIBUTION_MODE_hub_transmitting_main;
									
									FLASH_STORAGE_request_code_image_file_read( CS3000_APP_FILENAME, FLASH_TASK_EVENT_read_code_image_file_to_memory_for_code_transmit );
								}
								else
								if( weather_preserves.hub_needs_to_distribute_tpmicro_binary )
								{
									rv = (true);
		
									// 6/14/2017 rmd : Correct the mode, as the calling function just uses a generic main_binary
									// mode to get things going. Now we specifically know what we are going to distribute.
									cdcs.mode = CODE_DISTRIBUTION_MODE_hub_transmitting_tpmicro;
									
									FLASH_STORAGE_request_code_image_file_read( TPMICRO_APP_FILENAME, FLASH_TASK_EVENT_read_code_image_file_to_memory_for_code_transmit );
								}
							}
							else
							{
								// 9/26/2017 rmd : The hub list is empty, so no need to distribute. The flags could be set
								// however, as the update process proceeds through normally, as if there is a hub list and a
								// needed distribution. This is where we end it though.
								weather_preserves.hub_needs_to_distribute_main_binary = (false);
	
								weather_preserves.hub_needs_to_distribute_tpmicro_binary = (false);
							}

						}  // of if the proper device has been reported

					}  // of not ready yet
					
				}
				else
				{
					// 6/1/2017 rmd : Cleanup any residual parameters set if maybe this WAS ONCE a hub.
					cdcs.mode = CODE_DISTRIBUTION_MODE_idle;
					
					weather_preserves.hub_needs_to_distribute_main_binary = (false);
			
					weather_preserves.hub_needs_to_distribute_tpmicro_binary = (false);
				}
				break;
				
		}  // of switch of requested mode
		
	}  // of if idle

	// ----------

	xSemaphoreGiveRecursive( code_distribution_control_structure_recursive_MUTEX );
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
BOOL_32 CODE_DISTRIBUTION_hub_should_report_busy_to_the_commserver( void )
{
	BOOL_32	rv;
	
	rv = (false);
	
	if( (cdcs.mode == CODE_DISTRIBUTION_MODE_hub_transmitting_main) || (cdcs.mode == CODE_DISTRIBUTION_MODE_hub_transmitting_tpmicro) )
	{
		rv = (true);	
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
BOOL_32 CODE_DISTRIBUTION_comm_mngr_should_be_idle( void )
{
	BOOL_32	rv;
	
	rv = (false);
	
	// ----------
	
	// 6/8/2017 rmd : Code distribution, transmitting or recieving, is not to affect the
	// comm_mngr task. However there are two expceptions. FIRST is, if this controller is a
	// master on a chain, if it is broadcasting the binary down the chain using the flowsense
	// communication devices, we should NOT be making tokens!
	if( FLOWSENSE_we_are_the_master_of_a_multiple_controller_network() )
	{
		if( (cdcs.mode == CODE_DISTRIBUTION_MODE_flowsense_transmitting_main) || (cdcs.mode == CODE_DISTRIBUTION_MODE_flowsense_transmitting_tpmicro) )
		{
			rv = (true);
		}
	}

	// ----------
	
	// 6/13/2017 rmd : SECOND is, if we are a slave on a chain, during code reception we are not
	// getting a token, and should prevent ourselves from falling into forced, because that is
	// unnecessary complication.
	if( FLOWSENSE_we_are_poafs() && (FLOWSENSE_get_controller_letter() != 'A') )
	{
		if( cdcs.mode == CODE_DISTRIBUTION_MODE_receiving_from_controller )
		{
			rv = (true);
		}
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/7/2017 rmd : A discussion about the interaction between the 5 code distribution modes,
// and the COMM_MNGR and the CI (controller initiated) task. Assume the controller always
// has a means of connection to the commserver, hub related or otherwise.
//
// Realization: receiving code is benign, and does not affect the comm_mngr nor ci tasks
// 				while it is taking place..
//
// CASE 1: (mode=RECEIVING_FROM_COMMSERVER) A single controller, receiving code from the
// COMMSERVER. The commngr keeps on making tokens to ourselves. And the CI task
// msgs_to_send_queue is wiped, when we realize a binary is coming, so the CI task is
// inactive. Bottom line, no impact to the comm_mngr task, nor the ci task.
//
// CASE 2: (mode=RECEIVING_FROM_COMMSERVER) A master controller of a chain, receiving code
// from the COMMSERVER. The controller can continue to make tokens and operate the chain.
// The CI task msgs_to_send_queue is wiped, when we realize a binary is coming, so the CI
// task is inactive. Bottom line, no impact to the comm_mngr task, nor the ci task.
//
// CASE 3: (mode=RECEIVING_FROM_CONTROLLER) A slave controller in a chain, receiving code
// from the CHAIN MASTER. The comm_mngr task is waiting for a scan or token from the master
// which it doesn't receive, is inherently inactive. The ci task of a slave is never active.
// Bottom line, no impact to the comm_mngr task, nor the ci task.
//
// CASE 4: (mode=RECEIVING_FROM_COMMSERVER) A hub, receiving code from the COMMSERVER. The
// controller can continue to make tokens and operate the chain. And the CI task
// msgs_to_send_queue is wiped, when we realize a binary is coming. Bottom line, no impact
// to the comm_mngr task, nor the ci task.
//
// CASE 5: (mode=RECEIVING_FROM_HUB) A controller behind a hub, receiving code from the HUB.
// The controller can continue to make tokens and operate the chain. And the CI task has not
// rcvd permission from the commserver to send any messages, so it is inactive. Bottom line,
// no impact to the comm_mngr task, nor the ci task.
//
// CASE 6: (mode=TRANSMITTING_FROM_CONTROLLER) A master controller of a chain, transmitting
// a binary down the chain. The comm_mngr task must stay idle during this activity, so that
// only packets containing the binary are sent down the chain. The CI task should be
// unaffected.
//
// CASE 7: (mode=TRANSMITTING_FROM_HUB) A hub, transmitting a binary to the controllers
// behind the hub. The comm_mngr task should be unaffected, the chain can remain up, and
// scans and tokens continue to be made. The CI task will only send the 'hub is busy'
// message in response to polls from the commserver. So must be allowed to 'send the next
// queued message', but that message will always be the 'hub is busy' message.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void CODE_DISTRIBUTION_task( void *pvParameters )
{
	CODE_DISTRIBUTION_TASK_QUEUE_STRUCT		cdtqs;
	
	// 10/23/2012 rmd : To support the WDT activity monitor.
	UNS_32		task_index;
	
	COMM_MNGR_TASK_QUEUE_STRUCT	cmeqs;

	// ----------

	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// ----------

	// 6/7/2017 rmd : Be explicit. Always start in the idle mode.
	cdcs.mode = CODE_DISTRIBUTION_MODE_idle;
	
	// ----------

	cdcs.hub_code__list_received = (false);

	cdcs.hub_code__comm_mngr_ready = (false);
	
	// ----------
	
	// 10/23/2017 rmd : Initialize to (true), the default. The ONLY time this variable should be
	// set (false) is when receiving TWO updates from the commserver. In that case we do not
	// reboot between them.
	cdcs.restart_after_receiving_tp_micro_firmware = (true);

	cdcs.receive_tpmicro_binary_came_from_commserver = (false);
	
	// ----------

	// 7/20/2016 rmd : Initialize the delay between packet to the smaller M only value. During
	// the scan, any controller seen with an SR will cause us to adjust this value to the SR
	// value, which is MUCH longer.
	cdcs.transmit_chain_packet_rate_ms = CODE_DISTRIBUTION_MS_BETWEEN_PACKETS_FOR_M;

	cdcs.transmit_hub_packet_rate_ms = CODE_DISTRIBUTION_MS_BETWEEN_PACKETS_FOR_RAVEON_LR;

	// 4/22/2014 rmd : Create the transmit PACKET RATE timer.
	cdcs.transmit_packet_rate_timer = xTimerCreate( NULL, MS_to_TICKS(cdcs.transmit_chain_packet_rate_ms), (false), NULL, code_distribution_packet_rate_timer_callback );

	// ----------

	// 4/22/2014 rmd : Create the CODE RECEIPT ERROR timer. The timer period is not important
	// here, when we use the timer we start it with the ChangePeriod function.
	cdcs.receive_error_timer = xTimerCreate( NULL, MS_to_TICKS(30000), (false), NULL, code_receipt_error_timer_callback );

	// ----------
	
	// 10/30/2017 rmd : These two, when active, DISABLE the hub code distribution. I used them
	// during development. THE DEFINE MUST BE SET TO 0 FOR RELEASE!
	#if KILL_ANY_HUB_CODE_DISTRIBUTION_ATTEMPT
	
		weather_preserves.hub_needs_to_distribute_main_binary = (false);
		
		weather_preserves.hub_needs_to_distribute_tpmicro_binary = (false);
		
	#endif
		
	// ----------
	
	while( (true) )
	{
		// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
		if( xQueueReceive( CODE_DISTRIBUTION_task_queue, &cdtqs, MS_to_TICKS( 500 ) ) == pdTRUE )
		{
			xSemaphoreTakeRecursive( code_distribution_control_structure_recursive_MUTEX, portMAX_DELAY );

			// ----------
			
			switch( cdtqs.event )
			{
				case CODE_DISTRIBUTION_EVENT_incoming_packet:

					process_incoming_packet( &cdtqs );
					
					break;
					
				// ----------
				
				case CODE_DISTRIBUTION_EVENT_set_chain_distribution_packet_rate:

					cdcs.transmit_chain_packet_rate_ms = cdtqs.packet_rate_ms;

					break;
					
				// ----------
				
				case CODE_DISTRIBUTION_EVENT_commmngr_ready:
					// 5/31/2017 rmd : This event occurs following every scan that does NOT result in a
					// FLOWSENSE code distribution by the commmngr. So even after a failed scan. try to start
					// any needed hub code distribution process. In other words we are giving any needed
					// FLOWSENSE chain distribution priority over any needed hub distribution.
					cdcs.hub_code__comm_mngr_ready = (true);

					break;
					
				case CODE_DISTRIBUTION_EVENT_hub_list_arrived:
					// 12/10/2018 rmd : Set the hub list has arrived indication the code distribution task
					// references.
					cdcs.hub_code__list_received = (true);

					break;
					
				// ----------

				case CODE_DISTRIBUTION_EVENT_tpmicro_binary_file_read_for_isp_complete:

					// 6/14/2017 rmd : The comm_mngr manages the isp mode, let him know.
					cmeqs.event = COMM_MNGR_EVENT_tpmicro_binary_file_read_for_isp_complete;
					cmeqs.dh = cdtqs.dh;
					COMM_MNGR_post_event_with_details( &cmeqs );

					break;
					
				case CODE_DISTRIBUTION_EVENT_main_binary_file_read_complete:
				case CODE_DISTRIBUTION_EVENT_tpmicro_binary_file_read_complete:

					// 6/30/2017 rmd : Don't try if the user has entered device exchange mode.
					if( !in_device_exchange_hammer )
					{
						// 4/1/2014 rmd : Main code file read completed event. Attempt to commence distribution.
						build_and_send_code_transmission_init_packet( &cdtqs );
					}
					else
					{
						// 6/30/2017 rmd : Well the file read may have happened, if so free the memory.
						if( cdtqs.dh.dptr )
						{
							mem_free( cdtqs.dh.dptr );
						}
					}
					
					break;

				case CODE_DISTRIBUTION_EVENT_send_next_packet:

					// 6/30/2017 rmd : Don't try if the user has entered device exchange mode.
					if( !in_device_exchange_hammer )
					{
						// 8/7/2017 rmd : The send next packet event is a general go ahead that results generally
						// from the packet rate timer expiring. This event is also generated when the response to
						// the query is received.
						//
						// 8/7/2017 rmd : The parameter indicating a string found is false, as during string hunting
						// when reading the TEMPERATURE out of the RAVEON radio, if this event is generated it means
						// the timer expired meaning NO STRING FOUND.
						perform_next_code_transmission_state_activity( &cdtqs );
					}

					break;

				case CODE_DISTRIBUTION_EVENT_cr_lf_string_found:
				case CODE_DISTRIBUTION_EVENT_specified_string_found:
				case CODE_DISTRIBUTION_EVENT_specified_termination_found:

					// 6/30/2017 rmd : Don't try if the user has entered device exchange mode.
					if( !in_device_exchange_hammer )
					{
						// 8/7/2017 rmd : String was found so set parameter true.
						perform_next_code_transmission_state_activity( &cdtqs );
					}

					break;

				// ----------
				
				case CODE_DISTRIBUTION_EVENT_main_binary_file_write_complete:

					// 8/18/2014 rmd : In the odd case that the comm_server app sends a file with the same
					// version time and date as the one the file system already has, the file system will not
					// make the file write (doesn't write duplicates). In that case we would arrive here and
					// trigger the restart so quickly the binary receipt ACK may not have time to reach the
					// communication device and be successfully transmitted to the comm_server. As it turns out,
					// we are indeed counting on the file write time, to allow the response to make it to the
					// comm_server app. Intentionally add time here, to account for the 'same' file being sent,
					// give an extra 4 seconds.
					vTaskDelay( MS_to_TICKS( 4000 ) );
					
					// ----------
					
					if( CONFIG_this_controller_is_a_configured_hub() )
					{
						if( cdcs.restart_info_flag_for_hub_code_distribution == 0 )
						{
							// 3/6/2017 rmd : Okay for the case it has not yet been set we now KNOW what to set it to
							// now. We are receiving ONLY the main board binary (if we received both the flag would have
							// already been set).
							cdcs.restart_info_flag_for_hub_code_distribution = SYSTEM_SHUTDOWN_EVENT_hub_to_distribute_main_binary;
						}
						
						SYSTEM_application_requested_restart( cdcs.restart_info_flag_for_hub_code_distribution );
					}
					else
					{
						// 3/6/2017 rmd : For non-hub based always after receipt of the main board we start the
						// reboot process. NOTE - the alert is not entirely accurate as we may have actually
						// received both binaries. But that's okay because the event doesn't control behavior like
						// it does with the hub.
						SYSTEM_application_requested_restart( SYSTEM_SHUTDOWN_EVENT_main_code_file_updated );
					}
					
					break;

				case CODE_DISTRIBUTION_EVENT_tpmicro_binary_file_write_complete:

					// 6/2/2014 ajv : If both the CS3000 and TP Micro firmware are newer, we receive the TP
					// Micro file first and, instead of restarting immediately, wait until the restart is
					// triggered by saving the mainboard file.
					
					// 10/13/2017 rmd : If received from the commserver, meaning not a flowsense distrbituion,
					// need to send the ACK. And if the main board code is not following, and we are a HUB, must
					// send the end of poll.
					if( cdcs.receive_tpmicro_binary_came_from_commserver )
					{
						// 10/18/2017 rmd : Suppose the main board binary needs to be sent, as soon as the ACK to
						// the tpmicro binary is received by the commserver, the init packet for the main board
						// binary will be sent. In order that the code distribution incoming packet processing
						// accepts this new init packet the cdcs mode MUST BE IDLE. So being we are done with the
						// tpmicro, set that here. It is okay to set whether the main board binary is coming or not.
						// But make sure to do this BEFORE the ACK is sent!
						cdcs.mode = CODE_DISTRIBUTION_MODE_idle;
						
						// 10/13/2017 rmd : It came from the commserver, always ACK.
						CENT_COMM_build_and_send_ack_with_optional_job_number( cdcs.receive_tpmicro_binary_came_from_port, MID_CODE_DISTRIBUTION_tpmicro_full_receipt_ack, JOB_NUMBER_NOT_INCLUDED, 0 );

						// ----------
						
						if( CONFIG_this_controller_is_a_configured_hub() )
						{
							// 10/5/2017 rmd : If two binaries are coming the TPMICRO is sent first, followed by the
							// MAIN binary. After the last binary, if this is a HUB, the commserver wants to see an
							// END OF POLL message. So if only sending the TPMICRO send the END OF POLL.
							if( cdcs.restart_after_receiving_tp_micro_firmware )
							{
								// 10/5/2017 rmd : MUST use a brute force method to directly FORCE the END OF POLL to be
								// sent. If we go through traditional queueing channels, the registration will be sent
								// first, and by the time that is saved to the cloud database the controller may have
								// rebooted after saving the binary. That is no good because the commserver is really
								// counting on this END OF POLL message. The (false) parameter ensures we won't send
								// registration first.
								CONTROLLER_INITIATED_attempt_to_build_and_send_the_no_more_messages_msg( (false) );
							}
						}

						// ----------
						
						// 10/13/2017 rmd : If we are going to restart, wait for the transmissions to occur. The
						// commserver needs them. If we restart too soon they would get chopped.
						if( cdcs.restart_after_receiving_tp_micro_firmware )
						{
							// 10/13/2017 rmd : Four seconds seems to work fine, never seen it not work.
							vTaskDelay( MS_to_TICKS( 4000 ) );
						}
					}
					
					// ----------
					
					if( cdcs.restart_after_receiving_tp_micro_firmware )
					{
						Alert_Message( "CODE RECEIPT: after tpmicro rebooting" );

						if( CONFIG_this_controller_is_a_configured_hub() )
						{
							if( cdcs.restart_info_flag_for_hub_code_distribution == 0 )
							{
								// 3/6/2017 rmd : Okay for the case it has not yet been set we now KNOW what to set it to
								// now. We are receiving ONLY the tpmicro binary. And only need to distribute that binary on
								// the other side of the restart.
								cdcs.restart_info_flag_for_hub_code_distribution = SYSTEM_SHUTDOWN_EVENT_hub_to_distribute_tpmicro_binary;
							}
							
							SYSTEM_application_requested_restart( cdcs.restart_info_flag_for_hub_code_distribution );
						}
						else
						{
							// 3/6/2017 rmd : For non-hub based if we're set to reboot after receipt of the tpmicro
							// binary start the reboot process.
							SYSTEM_application_requested_restart( SYSTEM_SHUTDOWN_EVENT_tpmicro_code_file_updated );
						}
					}
					else
					{
						// 10/18/2017 rmd : Not rebooting, waiting for main board binary.
						Alert_Message( "CODE RECEIPT: after tpmicro not rebooting" );
					}

					break;
					
				default:
					Alert_Message( "unhandled code distrib event" );
					
			}  // of event processing switch statement

			// ----------
			
			xSemaphoreGiveRecursive( code_distribution_control_structure_recursive_MUTEX );
			
		}  // of a sucessful queue item is available

		// ----------

		// 10/23/2012 rmd : Keep the activity indication going.
		task_last_execution_stamp[ task_index ] = xTaskGetTickCount();

	}  // of forever task loop
}

/* ---------------------------------------------------------- */
extern void CODE_DISTRIBUTION_post_event_with_details( CODE_DISTRIBUTION_TASK_QUEUE_STRUCT *pq_item_ptr )
{
	// 6/24/2015 rmd : Can be executed within the context of a wide variety of tasks.

	//  WARNING: DO NOT CALL THIS FUNCTION FROM AN INTERRUPT because it is not using the fromISR
	//  xQueue function.

	if( xQueueSendToBack( CODE_DISTRIBUTION_task_queue, pq_item_ptr, 0 ) != pdPASS )	 // do not wait for queue space ... flag an error
	{
		Alert_Message( "Code Distrib queue overflow!" );
	}
}

/* ---------------------------------------------------------- */
extern void CODE_DISTRIBUTION_post_event( UNS_32 pevent )
{
	// 6/24/2015 rmd : Can be executed within the context of a wide variety of tasks.

	//  WARNING: DO NOT CALL THIS FUNCTION FROM AN INTERRUPT because it is not using the fromISR
	//  xQueue function.

	CODE_DISTRIBUTION_TASK_QUEUE_STRUCT		cdtqs;
	
	cdtqs.event = pevent;
	
	CODE_DISTRIBUTION_post_event_with_details( &cdtqs );
}

/* ---------------------------------------------------------- */
extern void CODE_DISTRIBUTION_stop_and_cleanup( void )
{
	// 6/30/2017 rmd : Try to destroy what the code distribution machine may have been
	// doing.

	// 11/3/2017 rmd : Definately stop this timer otherwise the controller will reboot 30
	// minutes from now.
	xTimerStop( cdcs.receive_error_timer, portMAX_DELAY );
	
	if( cdcs.receive_ptr_to_main_app_memory )
	{
		// 11/2/2017 rmd : If the memory has already been freed, this will trigger a REBOOT!
		mem_free( cdcs.receive_ptr_to_main_app_memory );

		cdcs.receive_ptr_to_main_app_memory = NULL;
	}

	if( cdcs.receive_ptr_to_tpmicro_memory )
	{
		// 11/2/2017 rmd : If the memory has already been freed, this will trigger a REBOOT!
		mem_free( cdcs.receive_ptr_to_tpmicro_memory );
		
		cdcs.receive_ptr_to_tpmicro_memory = NULL;
	}

	if( cdcs.transmit_data_start_ptr )
	{
		// 11/2/2017 rmd : If the memory has already been freed, this will trigger a REBOOT!
		mem_free( cdcs.transmit_data_start_ptr );
		
		cdcs.transmit_data_start_ptr = NULL;
	}
	
	// ----------
	
	cdcs.mode = CODE_DISTRIBUTION_MODE_idle;
	cdcs.receive_mid = 0;
	cdcs.transmit_init_mid = 0;
	cdcs.transmit_state = 0;
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

