/*  file = comm_mngr.c                         2011.07.20 rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"comm_mngr.h"

#include	"comm_utils.h"

#include	"foal_comm.h"

#include	"alerts.h"

#include	"packet_router.h"

#include	"tpl_in.h"

#include	"tpl_out.h"

#include	"app_startup.h"

#include	"cent_comm.h"

#include	"irri_comm.h"

#include	"irri_irri.h"

#include	"foal_irri.h"

#include	"configuration_controller.h"

#include	"tpmicro_comm.h"

#include	"df_storage_mngr.h"

#include	"wdt_and_powerfail.h"

#include	"tpmicro_isp_mode.h"

#include	"flash_storage.h"

#include	"configuration_network.h"

#include	"controller_initiated.h"

#include	"flowsense.h"

#include	"code_distribution_task.h"

#include	"weather_control.h"

#include	"manual_programs.h"

#include	"station_groups.h"

#include	"cs_mem.h"

#include	"rcvd_data.h"

#include	"background_calculations.h"

#include	"dialog.h"

#include	"moisture_sensors.h"

#include	"device_LR_RAVEON.h"

#include	"radio_test.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/*

	The CommMngr function set operates between the lower layer transport and the upper
	layer applications. It is what makes all the communications go. It contains functions
	that can be polled by the FOAL machine to tell about the chain health. The comm manager
	main function queries the various machine to see if they have any communications they
	wish to do.
	
	This layer is responsible for all the message routing. All messages pass through this
	layer.
	
		
*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*
*** THE CommVer NUMBER NOTES THAT DESCRIBE WHEN AND WHY COMM VERSION ROLLED *** 

602.a.4-001	02.02.2006 ajv

	* Added new "CommVer" string which tracks what version of the communications
	  infrastructure that a controller is running. This will not only allow
	  ET2000e controllers to communicate with 500 series ET2000 controllers, but
	  also allow two controllers with different ROM versions to communicate if
	  the revisions between them did not modify communications. Each time any
	  file is modified which may break communications between two revisions, the
	  communication version number should be rolled.
	
602.b-002 03.30.2006 ajv

	* Rolled CommVer to 002. Although it was not an absolute necessity, due to
	  the change affecting communications with older controllers, I felt it wise
	  to do so to ensure that ET2000e and ET2000 (500 series) ROMs are swapped
	  out at the same time.

603.a.1-003	04.16.2006 ajv

	* Rolled CommVer to 003. This was required because an unsigned char
	  program_priority was added to the ilc definition, effectively breaking
	  communications with controllers without this variable.

608.a 02.2008 rmd

	* Rolled CommVer to 004. We wanted to start providing better chain awareness
	  i.e. what was in the FOAL masters irrigation list and what was ON. Wanted
	  this information so the RRE could better inform the user about what was ON
	  in his SYSTEM. Additionally we discovered a significant inefficiency in
	  the CONTROLLER_FLOWDISPLAY_INFO structure that accompanies each TOKEN.
	  There were at least 5 BOOLEANS that could be converted to bits - there are
	  also unsigned shorts that only need to be char because of the range used -
	  I estimated about 10 byte savings times 12 controllers in the chain = 120
	  bytes per token! Before we started this effort the token was at 326 bytes
	  [typically - it does vary as things take place]. That is a 36% size
	  reduction. Worth the effort I'd say as it will help the system run faster.
	  Since the tokens are soooooo different now we accompanied this change with
	  changing MID_FOAL_TO_IRRI_NORMAL and MID_FOAL_TO_IRRI_FORCED values so
	  there is NO chance an older prior irri machine could receive and process
	  this new token and produce unpredictable results i.e. turn ON all
	  the valves or something nice like that.

	  UPDATE: when all said and done new token is now 40 bytes [was 326!].
	  No loss of information.

*/
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/20/2016 rmd : So that we may modify the SCAN and SCAN_RESP message content as time goes
// on. The version number is included in the message as a single character.
#define	SCAN_VERSION		(1)

// 7/20/2016 rmd : VERSION 2 - for the scan response think of version 2 as the baseline
// version. It includes the two bytes indicating to flag_myself_as_new and
// i_am_a_slave_and_have_rebooted.
//
// 7/20/2016 rmd : VERSION 3 - includes the byte indicating there is an SR radio used for
// FLOWSENSE traffic at the responding controller.
#define	SCAN_RESP_VERSION	(3)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/13/2015 rmd : This is the smallest number of MS between token. Therefore setting the
// maximum token rate. If the actual communications (token - token_resp cycle) takes longer
// than this amount of time, well then the tokens will be slower than this setting.
#define COMM_MNGR_MINIMUM_MS_BETWEEN_TOKENS					(700)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/3/2014 rmd : Initialized to 0 on code boot by C.
COMM_STATS                      comm_stats;

// 4/3/2014 rmd : NOT a battery backed structure. Initialized to 0 on boot.
COMM_MNGR_STRUCT                comm_mngr;

// 4/3/2014 rmd : Not a battery backed structure. Initialized to 0 on boot.
RECENT_CONTACT_STRUCT           last_contact, next_contact;

// 6/30/2017 rmd : A powerful global used to effectively idle all tasks that interface to
// serial ports A & B.
BOOL_32		in_device_exchange_hammer;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/1/2012 rmd : NOTE: since the comm_mngr is not battery backed there is no specific
// 'init' function. The compiler has initialized the variable to 0. We wanted to have an
// init function to use elsewhere in the code. For exampe when starting a scan. However the
// comm_mngr structure is fully of list headers and FreeRTOS timer task handles. They
// complicate any 'start' over so I've decided to just handle the required initializations
// as needed throughout the code.

/* ---------------------------------------------------------- */
extern void power_up_device( UNS_32 pport )
{
	if( pport == UPORT_A )
	{
		if( port_device_table[ config_c.port_A_device_index ].__power_device_ptr != NULL )
		{
			port_device_table[ config_c.port_A_device_index ].__power_device_ptr( pport, (true) );
		}
	}
	else
	if( pport == UPORT_B )
	{
		if( port_device_table[ config_c.port_B_device_index ].__power_device_ptr != NULL )
		{
			port_device_table[ config_c.port_B_device_index ].__power_device_ptr( pport, (true) );
		}	
	}
}

/* ---------------------------------------------------------- */
extern void power_down_device( UNS_32 pport )
{
	if( pport == UPORT_A )
	{
		if( port_device_table[ config_c.port_A_device_index ].__power_device_ptr != NULL )
		{
			port_device_table[ config_c.port_A_device_index ].__power_device_ptr( pport, (false) );
		}	
	}
	else
	if( pport == UPORT_B )
	{
		if( port_device_table[ config_c.port_B_device_index ].__power_device_ptr != NULL )
		{
			port_device_table[ config_c.port_B_device_index ].__power_device_ptr( pport, (false) );
		}	
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern BOOL_32 COMM_MNGR_network_is_available_for_normal_use( void )
{
	// 5/1/2014 rmd : This function called from various tasks. No MUTEX used as all the reads
	// are atomic.
	
	// ----------
	
	BOOL_32		rv;
	
	UNS_32		i;

	// ----------

	rv = (true);

	// ----------
	
	if( !((comm_mngr.mode == COMM_MNGR_MODE_operational) && (comm_mngr.state == COMM_MNGR_STATE_irrigation)) )
	{
		rv = (false);	
	}

	// ----------
	
	if( comm_mngr.chain_is_down )
	{
		rv = (false);	
	}

	// ----------

	if( FLOWSENSE_we_are_a_master_one_way_or_another() )
	{
		// 5/2/2014 rmd : For the MASTER two criteria must be satified. First of all for each
		// controller seen in the scan we would like to receive at least one token_resp free of any
		// pdata, deleted_groups, or box_configuration content.
		for( i=0; i<MAX_CHAIN_LENGTH; i++ )
		{
			if( chain.members[ i ].saw_during_the_scan )
			{
				if( comm_mngr.since_the_scan__master__number_of_clean_token_responses_rcvd[ i ] < REQUIRED_NUMBER_OF_CLEAN_TOKENS_SINCE_SCAN )
				{
					rv = (false);	
				}
			}
		}

		// 5/2/2014 rmd : Secondary test is that we made at least 4 tokens in a row that did not
		// contain any pdata or chain_members content.
		if( comm_mngr.since_the_scan__master__number_of_clean_tokens_made < REQUIRED_NUMBER_OF_CLEAN_TOKENS_SINCE_SCAN )
		{
			rv = (false);	
		}
	}
	else
	{
		// 5/2/2014 rmd : For SLAVES they need to receive a number of clean tokens. And make a
		// number of clean tokens.
		if( comm_mngr.since_the_scan__slave__number_of_clean_tokens_rcvd < REQUIRED_NUMBER_OF_CLEAN_TOKENS_SINCE_SCAN )
		{
			rv = (false);	
		}

		if( comm_mngr.since_the_scan__slave__number_of_clean_token_responses_made < REQUIRED_NUMBER_OF_CLEAN_TOKENS_SINCE_SCAN )
		{
			rv = (false);	
		}
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
static void bump_number_of_failures_to_respond_to_token( UNS_32 const pindex )
{
	// 4/2/2014 rmd : Bump the number of errors. The decision to fall into forced takes place
	// when the attempt to kick out the next token is made.
	comm_mngr.failures_to_respond_to_the_token[ pindex ] += 1;
	
	// 09/06/2018 dmd: In addition, bump the total number of errors. This keeps track of the
	// token response errors that happened in the past 24 hours.
	comm_mngr.failures_to_respond_to_the_token_today[ pindex ] += 1;
}

/* ---------------------------------------------------------- */
static UNS_32 nm_number_of_live_ones( void )
{
	UNS_32  i, rv;

	rv = 0;

	// ------------

	for( i=0; i<MAX_CHAIN_LENGTH; i++ )
	{
		if( chain.members[ i ].saw_during_the_scan )
		{
			if( comm_mngr.failures_to_respond_to_the_token[ i ] < COMM_MNGR_ALLOWABLE_FAILURES )
			{
				rv += 1;
			}
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
UNS_32 COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members( void )
{
	// 10/29/2018 rmd : Since chain members is shared down the chain, this function is useful
	// and valid to use at MASTER and SLAVE controllers. This means it can be used for UI work.
	// WARNING however, this function can be misleading, for example during the chain down
	// state, it may not be accurate. See how the
	// COMM_MNGR_alert_if_chain_members_should_not_be_referenced function below is used to
	// protect various UI work.
	//
	// 10/29/2018 rmd : NOTE - you cannot refer to the fl_struct.NOCIC variable at slave
	// controllers, because it is NOT a sync'd pdata variable. Therefore it is not valid at the
	// slaves in a chain, hence the need for this function.
	UNS_32  i, rv;

	rv = 0;

	// ------------

	// 10/29/2018 rmd : Take the MUTEX protecting the chain_members structure.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	for( i=0; i<MAX_CHAIN_LENGTH; i++ )
	{
		if( chain.members[ i ].saw_during_the_scan )
		{
			rv += 1;
		}
	}

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern void COMM_MNGR_alert_if_chain_members_should_not_be_referenced( char *fil, UNS_32 lin )
{
	// 5/1/2014 rmd : This function called from various tasks.

	if( !COMM_MNGR_network_is_available_for_normal_use() )
	{
		Alert_Message_va( "SHOULD NOT BE USING CHAIN_MEMBERS : %s, %u", CS_FILE_NAME( fil ), lin );
	}
}

/* ---------------------------------------------------------- */
static BOOL_32 an_FL_slave_has_a_central_device( UNS_32 *pbox_index )
{
	BOOL_32 rv;

	UNS_32	i;

	// ----------

	rv = (false);

	// 5/9/2017 ajv : Initialize the box index to 'A'.
	*pbox_index = 0;

	// ----------

	if( FLOWSENSE_we_are_the_master_of_a_multiple_controller_network() )
	{
		xSemaphoreTakeRecursive( chain_members_recursive_MUTEX, portMAX_DELAY );

		// 5/9/2017 ajv : Start with controller 'B' and look whether there's a central-only device
		// attached to Port A.
		for( i = 1; i < MAX_CHAIN_LENGTH; ++i )
		{
			if( (chain.members[ i ].saw_during_the_scan) && 
				( (chain.members[ i ].box_configuration.port_A_device_index == COMM_DEVICE_EN_UDS1100) || 
				  (chain.members[ i ].box_configuration.port_A_device_index == COMM_DEVICE_GR_AIRLINK) ||
				  (chain.members[ i ].box_configuration.port_A_device_index == COMM_DEVICE_WEN_PREMIERWAVE) ||
				  (chain.members[ i ].box_configuration.port_A_device_index == COMM_DEVICE_GR_PREMIERWAVE) ||
				  (chain.members[ i ].box_configuration.port_A_device_index == COMM_DEVICE_EN_PREMIERWAVE) ||
				  (chain.members[ i ].box_configuration.port_A_device_index == COMM_DEVICE_WEN_WIBOX) ||
				  (chain.members[ i ].box_configuration.port_A_device_index == COMM_DEVICE_MULTITECH_LTE) ) )
			{
				rv = (true);

				// 5/9/2017 ajv : Return which controller index has the central communication option.
				*pbox_index = i;

				// 5/9/2017 ajv : We found one, so break out.
				break;
			}

		}

		xSemaphoreGiveRecursive( chain_members_recursive_MUTEX );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static void transition_to_chain_down( void )
{
	// 4/30/2014 rmd : If already down do nothing.
	if( !comm_mngr.chain_is_down )
	{
		Alert_entering_forced();

		// ----------
		
		FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation();
		
		FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights();

		// ----------
		
		comm_mngr.chain_is_down = (true);
	}
}

/* ---------------------------------------------------------- */
static void flowsense_device_connection_timer_callback( xTimerHandle pxTimer )
{
	// 4/30/2014 rmd : This function is executed within the context of the high priority timer
	// task.
	
	// 7/21/2016 rmd : Just set this flag (true) indicating the timer has expired. The token
	// rate timer is already running and will expire and then the scan will commence!
	comm_mngr.flowsense_devices_are_connected = (true);
}

/* ---------------------------------------------------------- */
static UNS_32 contact_timer_milliseconds( void )
{
	// 7/2/2015 rmd : How tight can this timer be. Considerations are how long does it take on a
	// full 12 controller chain for the token to make it all the way around. Well with a
	// broadcast token that time is short. Only the time to the next token. And that may only be
	// seconds. Let's call it 15.
	//
	// It turns out the real issue that we want to manage is falling into forced during a code
	// delivery. Not sure of the harm in doing so but doesn't seem good. When code recipt starts
	// the commmngr stops making tokens. Code receipt through a GR is probably the slowest at
	// about 1 minute. Plus ten second to save the file. Plus restart. Then go through the scan
	// before beginning to rcv the code distribution. The scan restarts the timer. The code
	// distribution STOPS the timer. So the slave must tolerate the receipt time plus say one
	// minute. So set this timer to 2.5 minutes.

	return( 150 * 1000 );
}

/* ---------------------------------------------------------- */
static void token_arrival_timer_callback( xTimerHandle pxTimer )
{
	// 4/30/2014 rmd : This function is executed within the context of the high priority timer
	// task.

	// 4/30/2014 rmd : It is okay to set this atomic variable directly from the timer task
	// context. Slaves/masters/standalones all make this request. Though it is only meaningful
	// for the slaves.
	comm_mngr.flag_chain_down_at_the_next_opportunity = (true);
}

/* ---------------------------------------------------------- */
static void token_rate_timer_callback( xTimerHandle pxTimer )
{
	// 4/30/2014 rmd : This function is executed within the context of the high priority timer
	// task.
	COMM_MNGR_post_event( COMM_MNGR_EVENT_token_rate_timer_expired );
}

/* ---------------------------------------------------------- */
static void message_rescan_timer_callback( xTimerHandle pxTimer )
{
	// Remember this is executed within the context of the high priority timer task.
	COMM_MNGR_TASK_QUEUE_STRUCT cmqs;

	cmqs.event = COMM_MNGR_EVENT_request_to_start_a_scan;

	cmqs.reason_for_scan = COMM_MNGR_reason_to_scan_rescan_timer;

	COMM_MNGR_post_event_with_details( &cmqs );
}

/* ---------------------------------------------------------- */
static void rescan_timer_maintenance( void )
{
	// This function is called each time a token is kicked out. Whether that be in forced or what. IF we are in forced AND
	// we are the master of a real multiple controller chain then start the timer.

	UNS_32	timer_seconds;

	// Only masters of TRUE FlowSense multiple controller systems need to have this timer running.
	if( FLOWSENSE_we_are_the_master_of_a_multiple_controller_network() )
	{
		if( comm_mngr.chain_is_down )
		{
			// Okay we are allowed to start the timer. But only if it is not already running.
			if( xTimerIsTimerActive( comm_mngr.timer_rescan ) == pdFALSE )
			{
				// 9/8/2016 rmd : The profile should be the first 5 scans are 5 minutes apart. The next 5
				// scans are 15 minutes apart. The next 5 scans are 1 hour apart. And finally once every 4
				// hours. Originally this scheme was devised to prevent rolling out the 2000e alerts. That
				// really isn't the case for the 3000 - but still seems like a reasonble scheme to back off
				// how frequently we try.
				if( comm_mngr.scans_while_chain_is_down < 5 )
				{
					timer_seconds = (5 * 60);
				}
				else
				if( comm_mngr.scans_while_chain_is_down < 10 )
				{
					timer_seconds = (15 * 60);
				}
				else
				if( comm_mngr.scans_while_chain_is_down < 15 )
				{
					timer_seconds = (60 * 60);
				}
				else
				{
					timer_seconds = (4 * 60 * 60);
				}

				// The timer function to change the period actually changes the period and STARTS the timer!
				//
				// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
				// when posting is safe. Anything on the timer queue is quickly processed and therefore the
				// queue is effectively always empty.
				xTimerChangePeriod( comm_mngr.timer_rescan, MS_to_TICKS( (timer_seconds * 1000) ), portMAX_DELAY );
			}
		}
		else
		{
			xTimerStop( comm_mngr.timer_rescan, portMAX_DELAY );
		}
	}
	else
	{
		xTimerStop( comm_mngr.timer_rescan, portMAX_DELAY );
	}
}

/* ---------------------------------------------------------- */
static void message_resp_timer_callback( xTimerHandle pxTimer )
{
	// Remember this is executed within the context of the high priority timer task.

	COMM_MNGR_post_event( COMM_MNGR_EVENT_message_resp_timer_expired );
}

/* ---------------------------------------------------------- */
static void start_message_resp_timer( INT_32 for_how_many_milliseconds )
{
	// The timer function to change the period actually changes the period and STARTS the timer!
	//
	// 3/14/2014 rmd : Being that the timer task is a high priority task using portMAX_DELAY
	// when posting is safe. Anything on the timer queue is quickly processed and therefore the
	// queue is effectively always empty.

	xTimerChangePeriod( comm_mngr.timer_message_resp, MS_to_TICKS( for_how_many_milliseconds ), portMAX_DELAY );
}

/* ---------------------------------------------------------- */
static void stop_message_resp_timer( void )
{
	// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
	// when posting is safe. Anything on the timer queue is quickly processed and therefore the
	// queue is effectively always empty.

	xTimerStop( comm_mngr.timer_message_resp, portMAX_DELAY );
}

/* ---------------------------------------------------------- */
static void device_exchange_timer_callback( xTimerHandle pxTimer )
{
	// 3/14/2014 rmd : Remember executed within the context of the high priority timer task.

	COMM_MNGR_post_event( COMM_MNGR_EVENT_device_exchange_response_timer_expired );
}

/* ---------------------------------------------------------- */
static void post_attempt_to_send_next_token( void )
{
	// 3/6/2014 rmd : This function is called as the various events occur that may indicate it
	// is time to send the next token. Such as the token rate timer expired. Or such as a
	// response came in. Recognize there are a series of conditions that must be satisfied for
	// the token to actually go out. Normally it is the timer has expired and a response has
	// come in. Being the timer is a 700ms timer in many cases the response arrives before the
	// timer expires. But this is communications path dependant.

	COMM_MNGR_post_event( COMM_MNGR_EVENT_attempt_to_kick_out_the_next_token );
}

/* ---------------------------------------------------------- */
static void build_and_send_a_scan_resp( COMMUNICATION_MESSAGE_LIST_ITEM *pcmli )
{
	DATA_HANDLE     ldh;

	UNS_8           *ucp;

	COMM_COMMANDS	lcommand;

	UNS_32			scan_char;

	UNS_32          main_code_date, main_code_time;
	
	UNS_8           has_an_sr;

	// 3/25/2014 rmd : The SCAN RESP expects the controller letter.
	scan_char = FLOWSENSE_get_controller_letter();

	// 2011.08.17 rmd : Build a response that includes our scan_index value. That way the
	// receiving comm_mngr can do a special check on this response to verify it came from the
	// controller we think. This is desireable because the response otherwise would contain
	// only the serial number and we would not really know it came from the 'addressed'
	// controller. Remember during the scan the addressed controller is an address made up of
	// the contact index. Not the serial number.
	//
	// 11/26/2014 rmd : Added the reboot indication as another byte.
	//
	// 7/20/2016 rmd : Added VERSION 3 content as one more byte.
	ldh.dlen = sizeof(COMM_COMMANDS) + sizeof(scan_char) + sizeof(main_code_date) + sizeof(main_code_time) +
	           sizeof(tpmicro_comm.file_system_code_date) + sizeof(tpmicro_comm.file_system_code_time) +
			   (4 * sizeof(UNS_8));
			   
	ldh.dptr = mem_malloc( ldh.dlen );

	ucp = ldh.dptr;

	// -----------

	// 4/3/2014 rmd : Always send this command.
	lcommand = MID_SCAN_TO_COMMMNGR_RESP;

	memcpy( ucp, &lcommand, sizeof(COMM_COMMANDS) );	// first piece of the response
	ucp += sizeof(COMM_COMMANDS);

	memcpy( ucp, &scan_char, sizeof(UNS_32) );  // second piece of the response
	ucp += sizeof(UNS_32);

	// -----------

	// 2/18/2014 ajv : Add the firmware version the controller is currently running as a version
	// number and edit count. This will be used by the FLOWSENSE master to determine whether
	// there are controllers which are running a different version of firmware.
	main_code_date = convert_code_image_date_to_version_number( (UNS_8*)(EMC_DYCS0_BASE), CS3000_APP_FILENAME );
	memcpy( ucp, &main_code_date, sizeof(main_code_date) );	 // third piece of the response
	ucp += sizeof( main_code_date );

	main_code_time = convert_code_image_time_to_edit_count( (UNS_8*)(EMC_DYCS0_BASE), CS3000_APP_FILENAME );
	memcpy( ucp, &main_code_time, sizeof(main_code_time) );  // fourth piece of the response
	ucp += sizeof( main_code_time );

	// -----------

	// 4/2/2014 rmd : Include the tpmicro file system time and date. Which is guaranteed to be
	// valid if we are responding to the token! See where this function is callled.
	memcpy( ucp, &tpmicro_comm.file_system_code_date, sizeof(tpmicro_comm.file_system_code_date) );	 // fifth piece of the response
	ucp += sizeof( tpmicro_comm.file_system_code_date );

	memcpy( ucp, &tpmicro_comm.file_system_code_time, sizeof(tpmicro_comm.file_system_code_time) );  // sixth piece of the response
	ucp += sizeof( tpmicro_comm.file_system_code_time );

	// -----------

	// 4/29/2014 rmd : A single byte indicating our comm version.
	*ucp++ = SCAN_RESP_VERSION;

	// ----------
	
	// 4/29/2014 rmd : And a single byte flag to force me to be seen as NEW by the receiving
	// master. Then clear flag.
	if( comm_mngr.flag_myself_as_NEW_in_the_scan_RESP )
	{
		*ucp++ = (true);
	}
	else
	{
		*ucp++ = (false);
	}

	// 10/22/2014 ajv : DO NOT clear the flag_myself_as_NEW_in_the_scan_RESP
	// here. Instead, clear the flag after we receive a clean house token.
	// Otherwise, we run the risk of not syncing properly in cases such as:
	// 
	//   - User starts a new scan before the initial scan is complete, resulting
	//     in the master never processing the NEW flag
	//
	//   - A code update is distributed after the scan completes, preventing the
	//     NEW flag from being processed.

	// ----------
	
	// 11/26/2014 rmd : This is the flag that tells the master this controller has rebooted. If
	// the master has also not rebooted the scan is terminated and the master WILL REBOOT too.
	// See this variable declaration to get a description of why this behavior was implemented.
	if( comm_mngr.i_am_a_slave_and_i_have_rebooted )
	{
		*ucp++ = (true);
	}
	else
	{
		*ucp++ = (false);
	}

	// 11/26/2014 rmd : And clear this flag too. We have received a scan message and told the
	// master about this. Suppose the master doesn't get this scan response holding this reboot
	// information? Well then I am assuming the scan will fail cause this controller did not
	// show up in the scan. And that will cause the master to eventually rescan. This should all
	// play out.
	comm_mngr.i_am_a_slave_and_i_have_rebooted = (false);
		
	// ----------

	// 7/20/2016 rmd : VERSION 3 CONTENT
	
	// 7/20/2016 rmd : Add the byte indicating this controller has an SR radio used for
	// Flowsense traffic. This byte will in turn control the code distribution packet rate. Slow
	// for SR, faster if not.
	has_an_sr = (false);
	
	// 7/20/2016 rmd : If the A controller has an SR on port A it is NOT used for Flowsense
	// traffic.
	if( (scan_char != 'A') && (config_c.port_A_device_index == COMM_DEVICE_SR_FREEWAVE) )
	{
		has_an_sr = (true);	
	}
	
	// 7/20/2016 rmd : An SR on port B is always used for Flowsense traffic.
	if( config_c.port_B_device_index == COMM_DEVICE_SR_FREEWAVE )
	{
		has_an_sr = (true);	
	}
	
	*ucp++ = has_an_sr;
	
	// ----------
	
	ADDR_TYPE       laddr;

	// 2011.11.18 rmd : This only works cause this is a little endian machine (ARM). A big
	// endian machine would pick up the wrong end of the serial number.
	memcpy( &laddr.from, &config_c.serial_number, 3 );

	memcpy( &laddr.to, &pcmli->from_to.from, 3 );

	// -----------

	// Maintain accumulators.
	comm_stats.scan_msg_responses_generated += 1;

	// ----------

	// Send it. Responsiblity for the message memory is transferred.
	COMM_MNGR_send_outgoing_3000_message_and_take_on_memory_release_responsiblity( pcmli->in_port, ldh, laddr, MSG_CLASS_CS3000_SCAN_RESP );
}

/* ---------------------------------------------------------- */
static void process_incoming_scan( COMMUNICATION_MESSAGE_LIST_ITEM *cmli )
{
	// Think of this as the IRRI side executing this. Even though it is executed within the
	// context of the COMM_MNGR task. As with all incoming messages.

	COMM_COMMANDS   cc;

	UNS_8           *ucp, scan_version;

	// ----------
	
	// ALL messages start with the 2 byte part message ID.
	cc = *((COMM_COMMANDS*)cmli->dh.dptr);

	// Generate a ptr to the start of any accompanying data.
	ucp = cmli->dh.dptr;

	ucp += sizeof( COMM_COMMANDS );

	// ----------
	
	if( cc == MID_SCAN_TO_COMMMNGR )
	{
		// 7/20/2016 rmd : Not doing anything with this yet.
		scan_version = *ucp;

		// ----------
		
		// 4/29/2014 rmd : Here is a scan message to us. We no longer need to ask for a scan.
		comm_mngr.request_a_scan_in_the_next_token_RESP = (false);
		
		// ----------
		
		build_and_send_a_scan_resp( cmli );

		// ----------
		
		// 5/2/2014 rmd : And clear the network available criteria flags. Even if this is a MASTER
		// this is okay to do as in that case they won't be evaluated as part of the criteria. If
		// this is a SLAVE however this IS the place the counts are cleared.
		comm_mngr.since_the_scan__slave__number_of_clean_tokens_rcvd = 0;
	
		comm_mngr.since_the_scan__slave__number_of_clean_token_responses_made = 0;
	}
	else
	{
		Alert_Message( "UNK command in scan msg" );
	}
}

/* ---------------------------------------------------------- */
static void process_incoming_scan_RESP( COMMUNICATION_MESSAGE_LIST_ITEM *cmli )
{
	UNS_8		*ucp;
	
	UNS_32		responders_char, responders_index, responders_serial_number;

	UNS_32		masters_main_code_date, masters_main_code_time;

	UNS_32		delivered_main_code_date, delivered_main_code_time;

	UNS_32		delivered_tpmicro_code_date, delivered_tpmicro_code_time;
	
	UNS_8		scan_resp_version;

	UNS_8		is_NEW;

	UNS_8		has_rebooted;
	
	UNS_8		has_an_sr;

	BOOL_32		range_error;
	
	CODE_DISTRIBUTION_TASK_QUEUE_STRUCT		cdtqs;
	
	BOOL_32 	set_it;

	// ----------

	// 10/2/2012 rmd : The comm_mngr structure can be read or written to by several tasks. Take
	// the change flags about the need to distribute an edit for example. They are written to
	// by the key processing task and read and written to by the comm_mngr task.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	range_error = (false);
	
	// ----------

	if( !tpmicro_comm.file_system_code_date_and_time_valid )
	{
		// 3/27/2014 rmd : Alert about this as we use it within this function. Should NEVER see this
		// alert. Take no action as should never see. If we see need to understand and fix how this
		// is possible.
		Alert_Message( "Scanning w/o valid code d&t!" );
	}

	// ------------

	// 11/26/2014 rmd : Extract the serial number of who the message was from and record it.
	// This IS the only way we get the serial number from the controller. Zero it out first
	// though to get the FOURTH byte zeroed.
	responders_serial_number = 0;
	
	memcpy( &responders_serial_number, &cmli->from_to.from, 3 );

	// ----------
	
	// 4/17/2014 rmd : Skip over the message ID.
	ucp = cmli->dh.dptr;

	ucp += sizeof( COMM_COMMANDS );

	// ----------
	
	// 11/26/2014 rmd : Extract the controller letter.
	memcpy( &responders_char, ucp, sizeof(UNS_32) );

	ucp += sizeof(UNS_32);
	
	// 12/1/2014 rmd : And convert to an index.
	responders_index = responders_char - 'A';
	
	// ----------
	
	// 12/1/2014 rmd : RANGE check these values as we are going to use them as an array index.
	if( last_contact.index > BOX_INDEX_MAXIMUM )
	{
		range_error = (true);
		
		ALERT_MESSAGE_WITH_FILE_NAME( "Scan resp: contact out of range" );
		
		// 12/1/2014 rmd : Hmmm. Let the function that builds up the next scan message correct this
		// index. It contains further range checking on the last_contact.index value..
	}

	if( responders_index > BOX_INDEX_MAXIMUM )
	{
		range_error = (true);
		
		ALERT_MESSAGE_WITH_FILE_NAME( "Scan resp: char out of range" );
		
		// 12/1/2014 rmd : Something potentially bad is occuring. Let's attempt to force NEW
		// controllers found on a following scan.
		chain.members[ last_contact.index ].saw_during_the_scan = (false);
	}

	// ----------
	
	if( !range_error )
	{
		if( last_contact.index != responders_index )
		{
			// 11/26/2014 rmd : This is an error. Well this means we've received a response back from a
			// controller that we didn't think would be responding.
			ALERT_MESSAGE_WITH_FILE_NAME( "Unexpected scan response" );
	
			// 11/26/2014 rmd : Somehow a controller response came in that was not from the controller
			// we were interrogating. or the Something is up with the chain. Make sure we clear this to
			// cause a NEW indication during a subsequent scan. Just seems like the right thing to do.
			chain.members[ last_contact.index ].saw_during_the_scan = (false);

			// 12/1/2014 rmd : Try to force a new scan and NEW controllers to be found as something is
			// not right. Clearing both the last contact and responders reported indexes.
			chain.members[ responders_index ].saw_during_the_scan = (false);
		}
		else
		{
			if( responders_serial_number != chain.members[ last_contact.index ].box_configuration.serial_number )
			{
				// 4/17/2014 rmd : It is a new serial number! Flag this controller as NEW. So he cleans
				// house.
				comm_mngr.is_a_NEW_member_in_the_chain[ last_contact.index ] = (true);
			}
		
			// ------------
		
			// 4/17/2014 rmd : If we didn't see this controller during the last scan flag as NEW so he
			// will be asked to clean house.
			if( !chain.members[ last_contact.index ].saw_during_the_scan )
			{
				// 4/17/2014 rmd : Didn't see last time. Flag this controller as NEW. So he cleans house.
				comm_mngr.is_a_NEW_member_in_the_chain[ last_contact.index ] = (true);
			}
			
			// ----------
			
			// 4/25/2014 rmd : Set these two. Even if already correct.
			chain.members[ last_contact.index ].saw_during_the_scan = (true);
		
			chain.members[ last_contact.index ].box_configuration.serial_number = responders_serial_number;
		
			// ----------
		
			// 4/17/2014 rmd : NOTE - there is a THIRD way a controller can be flagged as NEW. Included
			// in the SCAN_RESP will be a flag from controllers that have had settings edited while they
			// were operating as a standalone. When they are made to be a slave we want to clean them in
			// this case. This covers the scenario where a chain is up and running. Then power fails at
			// the master for say days. A user sets one of the slaves as a standalone and actually uses
			// it - perhaps changing settings. Then set back to a slave. And the master re-powered. Well
			// the chain looks totally intact. BUT the file system is out of sync and needs to be
			// distributed from the master to this NEW slave.
		
			// ------------
		
			// 11/26/2014 rmd : Retrieve main code date and time. So we can compare it against what we
			// have to see if the controller need to be updated.
			memcpy( &delivered_main_code_date, ucp, sizeof(UNS_32) );
			ucp += sizeof(UNS_32);
		
			memcpy( &delivered_main_code_time, ucp, sizeof(UNS_32) );
			ucp += sizeof(UNS_32);
		
			masters_main_code_date = convert_code_image_date_to_version_number( (UNS_8*)(EMC_DYCS0_BASE), CS3000_APP_FILENAME );
		
			masters_main_code_time = convert_code_image_time_to_edit_count( (UNS_8*)(EMC_DYCS0_BASE), CS3000_APP_FILENAME );
		
			// ------------
		
			if( (masters_main_code_date == delivered_main_code_date) && (masters_main_code_time == delivered_main_code_time) )
			{
				comm_mngr.main_app_firmware_up_to_date[ last_contact.index ] = (true);
			}
			else
			{
				comm_mngr.main_app_firmware_up_to_date[ last_contact.index ] = (false);
		
				Alert_main_code_needs_updating_at_slave_idx( last_contact.index );
			}
		
			// ------------
		
			// 11/26/2014 rmd : Retrieve tpmicro code date and time. So we can compare it against what
			// we have to see if the controller need to be updated.
			memcpy( &delivered_tpmicro_code_date, ucp, sizeof(UNS_32) );
			ucp += sizeof(UNS_32);
		
			memcpy( &delivered_tpmicro_code_time, ucp, sizeof(UNS_32) );
			ucp += sizeof(UNS_32);
		
			if( (delivered_tpmicro_code_date == tpmicro_comm.file_system_code_date) && (delivered_tpmicro_code_time == tpmicro_comm.file_system_code_time) )
			{
				comm_mngr.tpmicro_firmware_up_to_date[ last_contact.index ] = (true);
			}
			else
			{
				comm_mngr.tpmicro_firmware_up_to_date[ last_contact.index ] = (false);
		
				Alert_tpmicro_code_needs_updating_at_slave_idx( last_contact.index );
			}
		
			// ------------
		
			// 11/26/2014 rmd : Obtain the scan response version so we know the content.
			scan_resp_version = *ucp;
			
			ucp += sizeof(UNS_8);
			
			// ----------
			
			// 4/29/2014 rmd : And now pick out our single byte flag to see if we should force the NEW
			// indication as requested by the SLAVE.
			is_NEW = *ucp;
		
			ucp += sizeof(UNS_8);
			
			if( is_NEW )
			{
				comm_mngr.is_a_NEW_member_in_the_chain[ last_contact.index ] = (true);
			}
		
			// ----------
		
			if( comm_mngr.is_a_NEW_member_in_the_chain[ last_contact.index ] )
			{
				// 4/25/2014 rmd : Just a diagnostic alert.
				Alert_Message_va( "Controller %c is a NEW chain member", ('A'+last_contact.index) );
				
				// ----------
				
				// 10/23/2014 ajv : Set a flag to make sure that Command Center Online
				// is updated since this controller was previously not seen by the scan,
				// but it is now.
				CONTROLLER_INITIATED_update_comm_server_registration_info();
				
				// ----------
				
				// 11/26/2014 rmd : Because the chain has a configuration change we have decided all
				// irrigation is to be wiped. To keep strange behaviors from occuring.
				FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation();

				FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights();
			}
			
			// ----------
			
			// 11/26/2014 rmd : Now for content that only exists in version 2 and beyond.
			if( scan_resp_version >= 2 )
			{
				// 11/26/2014 rmd : Extract the byte telling us if the slave has rebooted.
				has_rebooted = *ucp;
			
				ucp += sizeof(UNS_8);
				
				#if DURING_SCAN_ALLOW_MASTER_TO_REBOOT_IF_SLAVE_HAS_REBOOTED

				if( has_rebooted )
				{
					// 11/26/2014 rmd : This flag is telling us the slave has recently rebooted and not been
					// scanned until now (say a brown out). In this brown out type case the irri list in the
					// slave is gone and we need to get everybody in the chain on the same page. The easiest way
					// to do this is to reboot the master too. BUT only if the master himself hasn't rebooted.
					//
					// 11/26/2014 rmd : Further note we do not want to wipe irrigation in this case. Just resync
					// irrigation. Which rebooting the master will do cause that will trigger the transmission
					// of the foal irri list to all controllers.
					if( !comm_mngr.i_am_the_master_and_i_have_rebooted )
					{
						// 11/26/2014 rmd : If we are going to reboot AND if this responding controller was
						// determined to be new we need to set the stage so that he will once again be seen as new
						// following the reboot and subsequent scan. To do that wipe his saw_during_scan flag. It is
						// soooo important to make sure he is again seen as NEW so that he receives his CLEAN HOUSE
						// command.
						if( comm_mngr.is_a_NEW_member_in_the_chain[ last_contact.index ] )
						{
							chain.members[ last_contact.index ].saw_during_the_scan = (false);  // force to be seen NEW during next scan
						}

						DIALOG_draw_ok_dialog( GuiStruct_dlgControllerRebooting_0 );

						// 2/10/2015 ajv : Wait a few seconds before triggering the restart to allow for any pending
						// delayed file saves to complete.
						vTaskDelay( MS_to_TICKS(4000) );
						
						SYSTEM_application_requested_restart( SYSTEM_SHUTDOWN_EVENT_one_of_the_slaves_rebooted );
					}
				}

				#elif defined NDEBUG
				
					// 5/16/2016 rmd : To prevent accidental build of release with code distribution disabled.
					// Which could be a disaster on a chain.
					#error COMPILING RELEASE CODE WITH MASTER REBOOT DISABLED! NOT ALLOWED!
					
				#endif
			}
			
			// ----------
			
			// 7/20/2016 rmd : Now for content that only exists in version 3 and beyond. If we see an SR
			// bump up the time to the SR packet rate. At the start of the scan the rate is set to
			// the faster M only rate. Also if older code in place assume SR's are present.
			set_it = (false);
			
			if( scan_resp_version >= 3 )
			{
				has_an_sr = *ucp;
				ucp += sizeof(UNS_8);

				if( has_an_sr )
				{
					set_it = (true);
				}
			}
			else
			{
				// 7/20/2016 rmd : Because we've received a scan response from older code we MUST assume
				// (because the scan_resp from older code doesn't hold this information) there are SR radios
				// present and use the SR radio packet delay.
				set_it = (true);
			}
			
			if( set_it )
			{
				cdtqs.event = CODE_DISTRIBUTION_EVENT_set_chain_distribution_packet_rate;
				
				// 9/18/2017 rmd : Because we are not yet taking into account if there is a repeater or not
				// we must use the slower 'as if there is a repeater' rate ... for now.
				cdtqs.packet_rate_ms = CODE_DISTRIBUTION_MS_BETWEEN_PACKETS_FOR_FREEWAVE_SR_WITH_REPEATER;
				
				CODE_DISTRIBUTION_post_event_with_details( &cdtqs );
			}
			
			// ----------
			
		}  // of correct last contact
		
	}  // of not a range error
		
	// ------------

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void test_last_incoming_resp_integrity( ADDR_TYPE to_from_addr, const char *type_string )
{
	// 4/23/2014 rmd : Only useful at the master for the token response. See if the last to
	// address is equal to the incoming message from address. We only alert if so. No action is
	// taken. This is a system sanity check to make sure messaging and responses are in
	// sync.
	if( CommAddressesAreEqual( &to_from_addr.from, &last_contact.from_to.to ) == (false) )
	{
		// 3/14/2013 ajv : Since the to and from addresses are stored as three- byte arrays to
		// support legacy communications, copy them into integers to properly display the values.
		UNS_32  lto_serialnumber, lfrom_serialnumber;

		lto_serialnumber = 0;

		lfrom_serialnumber = 0;

		memcpy( &lto_serialnumber, last_contact.from_to.to, 3 );

		memcpy( &lfrom_serialnumber, to_from_addr.from, 3 );

		Alert_Message_va( "COMM MNGR %s address discrepancy; to: %d from: %d", type_string, lto_serialnumber, lfrom_serialnumber );
	}
}

/* ---------------------------------------------------------- */
extern UNS_32 COMM_MNGR_token_timeout_seconds( void )
{
	UNS_32  rv;

	// 6/30/2015 rmd : In the CS3000 with broadcasting, the transport sends the message and is
	// not waiting for a status. It is done. There are no retries. And no message from the
	// TPL_OUT task about failure. It is up to us here. Wait 15 seconds for a token response.
	// That is plenty. (We used to wait 90 seconds in the 2000e world. But that had to do with
	// central traffic pent up to send before the token response.)
	//
	// 8/8/2016 rmd : So in certain scenarios we push right up against 15 seconds. If you
	// involve SR radios with a repeator and big tokens during pdata changes (2K bytes plus) I
	// have seen us timeout and then the token response show. So I've extended this out to 25
	// seconds.
	//
	// 8/9/2016 rmd : And then on subsequent large USER alert syncs (we were doing them by
	// accident actually). But anyway we still were nt waiting long enough. So now I've given
	// another 10 seconds. Remember SR radios move about 2K bytes in 5 seconds. I saw an 6K byte
	// plus token. That would be 15 seconds just to get it through the radios. Still not exactly
	// sure why need more than the 25 seconds but I saw the timer expire. So moved to 35
	// seconds.

	rv = 35;
	
	return( rv );
}

/* ---------------------------------------------------------- */
static void build_up_the_next_scan_message( void )
{
	COMM_COMMANDS	*cc;

	UNS_8	*ucp;

	// ----------
	
	next_contact.message_handle.dlen = sizeof(COMM_COMMANDS) + sizeof(UNS_8);

	next_contact.message_handle.dptr = mem_malloc( next_contact.message_handle.dlen );

	// ----------
	
	ucp = next_contact.message_handle.dptr;

	cc = (COMM_COMMANDS*)next_contact.message_handle.dptr;

	*cc = MID_SCAN_TO_COMMMNGR;

	ucp += sizeof( COMM_COMMANDS );

	// ----------
	
	// 4/29/2014 rmd : Not yet used upon receipt.
	*ucp = SCAN_VERSION;

	// ----------
	
	// For the LPC3250 the scan to address is a special form of the serial number. Along the lines of 0x000041, 0x000042,
	// 0x000043, etc. To reflect the A controller, the B controller, the C controller, etc.
	next_contact.from_to.to[ 0 ] = 'A' + next_contact.index;
	next_contact.from_to.to[ 1 ] = 0;
	next_contact.from_to.to[ 2 ] = 0;

	memcpy( &next_contact.from_to.from, &config_c.serial_number, 3 );
}

/* ---------------------------------------------------------- */
static BOOL_32 nm_set_next_contact_index_and_command( void )
{
	// 4/4/2014 rmd : Local function called within context of comm_mngr only.
	
	// ----------

	UNS_32  rv, i;

	// ----------

	// 4/4/2014 rmd : Means no command to send.
	rv = (false);

	// ----------
	
	// A sanity check
	if( last_contact.index >= MAX_CHAIN_LENGTH )
	{
		Alert_Message( "last contact index out of range" );

		// Do something - fix it at least.
		last_contact.index = 0;
	}

	// ----------
	
	switch( comm_mngr.mode )
	{
		case COMM_MNGR_MODE_scanning:
			if( last_contact.index < BOX_INDEX_MAXIMUM )
			{
				next_contact.index = (last_contact.index + 1);
				
				rv = (true);
			}
			break;
		
		case COMM_MNGR_MODE_operational:
			switch( comm_mngr.state )
			{
				case COMM_MNGR_STATE_crc_cleaning_house:

					for( i=0; i<MAX_CHAIN_LENGTH; i++ )
					{
						if( comm_mngr.is_a_NEW_member_in_the_chain[ i ] )
						{
							next_contact.index = i;
							
							next_contact.command_to_use = MID_FOAL_TO_IRRI__CRC_ERROR_CLEAN_HOUSE_REQUEST;
							
							rv = (true);

							break;	// DONE!
						}
					}
					
					if( i == MAX_CHAIN_LENGTH )
					{
						// 4/4/2014 rmd : So no more. Change states.
						comm_mngr.state = COMM_MNGR_STATE_irrigation;
						
						// 4/4/2014 rmd : Returns false. So next token time slot will be skipped. We'll return to
						// this function in 700ms to process the new state.
					}
					break;
					
				case COMM_MNGR_STATE_cleaning_house:

					for( i=0; i<MAX_CHAIN_LENGTH; i++ )
					{
						if( comm_mngr.is_a_NEW_member_in_the_chain[ i ] )
						{
							next_contact.index = i;
							
							next_contact.command_to_use = MID_FOAL_TO_IRRI__CLEAN_HOUSE_REQUEST;
							
							rv = (true);

							break;	// DONE!
						}
					}
					
					if( i == MAX_CHAIN_LENGTH )
					{
						// 4/4/2014 rmd : So no more. Change states.
						comm_mngr.state = COMM_MNGR_STATE_irrigation;
						
						// 4/4/2014 rmd : Returns false. So next token time slot will be skipped. We'll return to
						// this function in 700ms to process the new state.
					}

					break;
				
				// ----------

				case COMM_MNGR_STATE_irrigation:
				
					// 4/16/2014 rmd : Well we sure had better see ONE that was seen during the scan.
					rv = (true);

					next_contact.index = last_contact.index;
	
					do
					{
						next_contact.index += 1;
		
						if( next_contact.index >= MAX_CHAIN_LENGTH )
						{
							next_contact.index = 0;
						}
					} while( !chain.members[ next_contact.index ].saw_during_the_scan );

					next_contact.command_to_use = MID_FOAL_TO_IRRI__TOKEN;

					break;
				
			}
			break;
		
		// ----------

		default:
			Alert_Message( "Error calling next contact" );
			
	}

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
static void hide_hardware_based_upon_scan_results( void )
{
	// 4/17/2014 rmd : Executed within the context of the comm_mngr. At masters of a chain. And
	// by standalones too after their decision is made to skip around the scan.
	//
	// Job here is to hide all the stations and pocs for letters that did not respond during the
	// scan.

	// ----------

	// 4/17/2014 rmd : Roll through the stations list and hide or reveal stations based upon if
	// their index responded to the scan.
	STATION_set_not_physically_available_based_upon_communication_scan_results();

	// ----------

	// 6/6/2016 rmd : For the case of POCs and MOISTURE_SENSORS do not disable their file
	// records by setting say the POC to not show or the moisture sensor to not physically
	// available. If we were to do this hiding of POC's or MOISTURE SENSORS, if the user were
	// debugging a communication issue and a controller didn't show in the SCAN, he would end up
	// with the moisture sensor hidden. Or the POC set not to 'show'. For the case of the POC
	// this has the profund effect of destroying the poc preserves record including the recorded
	// BUDGET USE to date in the billing period. Just cause I did a scan with a broken chain! WE
	// MUST PREVENT THIS. I think.
	
	// 7/8/2015 mpd : Same for LIGHTS.
	LIGHTS_set_not_physically_available_based_upon_communication_scan_results();

	// ----------
}

/* ---------------------------------------------------------- */
static void if_any_NEW_in_the_chain_then_distribute_all_groups( void )
{
	// 4/17/2014 rmd : Executed within the context of the comm_mngr. Only at the master of a
	// chain. Not by standalones.
	//
	// 4/25/2014 rmd : Need to send, to the NEW controllers in the chain, all exisiting
	// stations, pocs, hardware in general, and all program data settings. These pieces of data
	// may be from the master, other slaves, and even from the NEW controller index itself.

	// ----------

	UNS_32	i;
	
	for( i=0; i<MAX_CHAIN_LENGTH; i++ )
	{
		if( comm_mngr.is_a_NEW_member_in_the_chain[ i ] )
		{
			STATION_set_bits_on_all_stations_to_cause_distribution_in_the_next_token();

			POC_set_bits_on_all_pocs_to_cause_distribution_in_the_next_token();

			MOISTURE_SENSOR_set_bits_on_all_moisture_sensors_to_cause_distribution_in_the_next_token();

			WEATHER_CONTROL_set_bits_on_all_settings_to_cause_distribution_in_the_next_token();

			NETWORK_CONFIG_set_bits_on_all_settings_to_cause_distribution_in_the_next_token();

			IRRIGATION_SYSTEM_set_bits_on_all_systems_to_cause_distribution_in_the_next_token();

			MANUAL_PROGRAMS_set_bits_on_all_groups_to_cause_distribution_in_the_next_token();

			STATION_GROUP_set_bits_on_all_groups_to_cause_distribution_in_the_next_token();

			LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token();
			
			WALK_THRU_set_bits_on_all_groups_to_cause_distribution_in_the_next_token();

			// ----------
			
			// 4/25/2014 rmd : All done. We've done it once and that's all that is required.
			break;
		}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void kick_out_the_next_scan_message( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void COMM_MNGR_start_a_scan( void )
{
	// 4/1/2014 rmd : Executed within context of the COMM_MNGR task. Either when the tpmicro
	// code version test passes as part of each boot cycle. Or indirectly by outside request
	// (keypad / re-scan timer). Note on reboot all controllers (MASTERS, SLAVES, and
	// STANDALONES) pass through this start_a_scan function. Good place to do housekeeping.

	// ----------

	UNS_32  i;

	CODE_DISTRIBUTION_TASK_QUEUE_STRUCT		cdtqs;

	// ----------
	
	// 10/30/2017 rmd : Now that the scan is actually starting stamp the alert.
	Alert_starting_scan_with_reason( comm_mngr.start_a_scan_captured_reason );

	// ------------

	// 6/19/2017 rmd : Restore the default code distribution packet rate to the FASTER M rate.
	// As scan responses arrive, if an SR is detected, we decrease the rate to the slower SR
	// rate.
	cdtqs.event = CODE_DISTRIBUTION_EVENT_set_chain_distribution_packet_rate;
	
	cdtqs.packet_rate_ms = CODE_DISTRIBUTION_MS_BETWEEN_PACKETS_FOR_M;
	
	CODE_DISTRIBUTION_post_event_with_details( &cdtqs );

	// ----------
	
	// 10/2/2012 rmd : The comm_mngr structure can be read or written to by several tasks. Take
	// the change flags about the need to distribute an edit for example. They are written to
	// by the key processing task and read and written to by the comm_mngr task.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	// ------------

	// 4/1/2014 rmd : Clear the flag that brought us to this function.
	comm_mngr.start_a_scan_at_the_next_opportunity = (false);

	// ----------
	
	// 4/30/2014 rmd : IF the chain is down we do want to bump the count of scans while the
	// chain is down. Otherwise zero that count. Incidentally, if the chain is down and we are
	// scanning I believe this controller MUST be a master as only masters perform a scan.
	if( comm_mngr.chain_is_down )
	{
		comm_mngr.scans_while_chain_is_down += 1;

		// 5/5/2014 rmd : For MASTERS only - clear the saw_during_the_scan indication for each box.
		// This is to force a complete file sync when the chain comes back up. One reason for this
		// is while the chain is down we do update a few things. For example the station NO Water
		// Days. And that is stored in its file and the change is managed under the usual change
		// distribution method. However without a token (and if following the change there is a
		// power failure) the indication of the change is lost. And we really don't want the file
		// CRC test to catch this. The philosophy on that is a failed file CRC check means a hole in
		// our file sync process.
		for( i=0; i<MAX_CHAIN_LENGTH; i++ )
		{
			chain.members[ i ].saw_during_the_scan = (false);
		}
	}

	// 4/30/2014 rmd : And NOW clear the chain_is_down flag. For the case of a MASTER it will be
	// set again quickly following the scan if the number of controllers found doesn't match the
	// NOCIC.
	comm_mngr.chain_is_down = (false);

	// ----------

	// 4/25/2014 rmd : Note - DO NOT clear the saw_during_the_scan variable for all indicies.
	// Except for 'A' which is set just below. We count on their present value of a record of
	// what was when determining NEW status or not.
	//
	// 4/25/2014 rmd : DO set all NEW to false. Must do this to ensure at the conclusion of the
	// scan only those who responded and we determined to be new are flagged as new.
	memset( &(comm_mngr.is_a_NEW_member_in_the_chain), 0x00, sizeof( comm_mngr.is_a_NEW_member_in_the_chain ) );
	
	// 4/25/2014 rmd : Clear the error count for all indicies. Regardless of master, slave,
	// standalond status. Makes sense as housekeeping.
	memset( &(comm_mngr.failures_to_respond_to_the_token), 0x00, sizeof( comm_mngr.failures_to_respond_to_the_token ) );

	// ----------

	// 5/2/2014 rmd : Regardless of master slave standalone clear the network available criteria
	// flags and accumulators.
	comm_mngr.since_the_scan__master__number_of_clean_tokens_made = 0;

	memset( &comm_mngr.since_the_scan__master__number_of_clean_token_responses_rcvd, 0x00, sizeof( comm_mngr.since_the_scan__master__number_of_clean_token_responses_rcvd ) );

	// 5/2/2014 rmd : It actually doesn't serve any purpose to clear the slave values here. They
	// are already 0 on reboot. They do need to be cleared however when a scan message is rcvd
	// to cover the case of scan initiated at master without slaves knowledge (local power
	// failure at master).

	// ----------
	
	// If we are part of a larger chain AND we are the master 'A' controller.
	if( FLOWSENSE_we_are_the_master_of_a_multiple_controller_network() )
	{
		// 4/30/2014 rmd : Is zero on reboot.
		comm_stats.rescans += 1;

		// ----------

		comm_mngr.mode = COMM_MNGR_MODE_scanning;

		// ----------
		
		// 4/17/2014 rmd : Setting this to 0, or the A controller, means the scan will actually
		// start with the B controller. On startup remember our OWN tpmicro code version has been
		// already checked before the comm_mngr gets to the point where it is attempting this scan.
		// So there really is no point to scanning ourselves. Additionally we can't be NEW.
		last_contact.index = 0;

		// ----------
		
		// 4/17/2014 rmd : Fix everything up as if we scanned 'A'.

		chain.members[ 0 ].saw_during_the_scan = (true);

		chain.members[ 0 ].box_configuration.serial_number = config_c.serial_number;

		comm_mngr.main_app_firmware_up_to_date[ 0 ] = (true);  // must be - we booted
	
		comm_mngr.tpmicro_firmware_up_to_date[ 0 ] = (true);  // must be - went through comm_mngr startup

		// 5/22/2015 ajv : Since we're starting the scan with the "B" controller, make sure the
		// accumulators take into account that "A"s response was generated and received.
		comm_stats.scan_msg_responses_generated += 1;
		comm_stats.scan_msgs_rcvd += 1;
		comm_stats.scan_msg_responses_rcvd += 1;
		
		// 11/26/2014 rmd : And clear the flag that is requesting a scan. This flag exists in a
		// sense at each and every irri machine. Including the masters. Because we pseudo scan A we
		// must clear it here otherwise we will keep triggering a scan over and over again.
		comm_mngr.i_am_a_slave_and_i_have_rebooted = (false);
		
		// ----------
		
		// Since we are executing at the task level it is safe to call this other task level function.
		kick_out_the_next_scan_message();
	}
	else
	if( !FLOWSENSE_we_are_poafs() )
	{
		// 4/17/2014 rmd : STANDALONES fall through to here.
		
		// 4/17/2014 rmd : YOU know - for standalones we need to set the saw_during_scan
		// appropriately so that we hide any stations, pos, or other hardware appropriately. We
		// don't know if this controller was a slave in a chain and somebody just switched it to a
		// standalone. If they did this it's file system would contain stations etc from the chain.
		// And they need to be hidden.
		//
		// 4/29/2014 rmd : This is ALSO very important at a master wen it is turned into a
		// standalone and then back into a master again. With all the saw_during_the_scan set to
		// FALSE that forces us to see each slave that reports during the scan as NEW. Which is
		// important so we get a hardware report from each of those slaves. Which in turn allows the
		// master to UNHIDE the hardware that gets hidden when he is a standalone.
		for( i=0; i<MAX_CHAIN_LENGTH; i++ )
		{
			chain.members[ i ].saw_during_the_scan = (false);
		}

		chain.members[ FLOWSENSE_get_controller_index() ].saw_during_the_scan = (true);

		// 4/17/2014 rmd : And make sure his serial number is correct. You know this could be the
		// first time this controller ever ran.
		chain.members[ FLOWSENSE_get_controller_index() ].box_configuration.serial_number = config_c.serial_number;
		
		// 11/26/2014 rmd : And clear the flag that is requesting a scan. This flag exists in a
		// sense at each and every irri machine. Including the masters. Because we pseudo scan A we
		// must clear it here otherwise we will keep triggering a scan over and over again.
		comm_mngr.i_am_a_slave_and_i_have_rebooted = (false);
		
		// 7/13/2015 ajv : It turns out we need to clear BOTH flags that can request a scan. This
		// can get set if a controller is set to be POAFS, the chain goes down, and then is no
		// longer made to be POAFS. In this situation, this flag is set but, since the controller
		// never sends an actual scan message, it's never cleared. Therefore, we need to clear it
		// manually.
		comm_mngr.request_a_scan_in_the_next_token_RESP = (false);

		// ----------

		hide_hardware_based_upon_scan_results();

		// ----------
		
		// 4/24/2014 rmd : This will cause us to send the chain members array to ourselves. Which
		// the receipt of will generate a request for the tpmicro to again send the 'irri' machine
		// the whats installed information. Which in turn causes a review of the hardware versus the
		// file records! A good thing to make sure all is in sync.
		comm_mngr.broadcast_chain_members_array = (true);

		// ----------
		
		// 4/17/2014 rmd : And move straight to operational mode. And skip the HOUSE_CLEANING state.
		// No house cleaning is needed for a standalone because we cleaned in a sense its files by
		// hiding hardware.
		comm_mngr.mode = COMM_MNGR_MODE_operational;
		
		comm_mngr.state = COMM_MNGR_STATE_irrigation;

		// ----------
		
		// 4/17/2015 ajv : The next scheduled irrigation is normally kept up-to-date by
		// recalculating every time a change is made. However, when the CommMngr transitions to 
		// operational and irrigating (i.e., no longer cleaning house), it's important to ensure the
		// controller determines the next scheduled irrigation based on the current programming.
		// Therefore, post a message to update the GuiVars used to display the next scheduled
		// irrigation.
		postBackground_Calculation_Event( BKGRND_CALC_NEXT_SCHEDULED );

		// ----------

		// 4/17/2014 rmd : And make sure token can move out.
		comm_mngr.token_in_transit = (false);
	}
	else
	{
		// 4/17/2014 rmd : SLAVES fall through to here. In order to process an incoming scan or
		// token the comm_mngr must be in its operational mode.
		comm_mngr.mode = COMM_MNGR_MODE_operational;
		
		comm_mngr.state = COMM_MNGR_STATE_irrigation;

		// 4/29/2014 rmd : It seems we can't really do anything to the stations, pocs, etc here. If
		// they are correct we want to leave them alone. If not we, as a slave, don't may or may not
		// have the knowledge to correct hide or reveal items.

		// 4/29/2014 rmd : If here we were to set our two flags
		// request_a_scan_in_the_next_token and flag_myself_as_NEW_in_the_scan_RESP we would ALWAYS
		// cause a full distribution of the hardware list on a scan. We shall see if this can be
		// avoided. If not this is how to cause that!! UPDATE so far we do not need to do this.

		// ----------

		// 4/17/2015 ajv : The next scheduled irrigation is normally kept up-to-date by
		// recalculating every time a change is made. However, when the CommMngr transitions to 
		// operational and irrigating (i.e., no longer cleaning house), it's important to ensure the
		// controller determines the next scheduled irrigation based on the current programming.
		// Therefore, post a message to update the GuiVars used to display the next scheduled
		// irrigation.
		postBackground_Calculation_Event( BKGRND_CALC_NEXT_SCHEDULED );
	}
	
	// ------------

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern BOOL_32 tpmicro_is_available_for_token_generation( void )
{
	// 3/6/2014 rmd : First let's think about the startup conditions. We cannot start pushing
	// out scan packets till we know the TPMicro is up and running. And that includes the
	// possiblity a code download to it has to happen. In this case not only do we watch the
	// mode but for the first message to arrive.
	//
	// Second to think about is just in the middle of the application running, if we happen to
	// be in ISP mode we cannot attempt a scan. We have to wait.

	// 3/25/2014 rmd : Because the scan response from a slave contains the tpmicro code version
	// (of the controller sending the SCAN_RESP), and we compare that code version in the
	// response to our own local tpmicro code file version we want the comm mngr to have a valid
	// file system tpmicro code version before the scan is allowed to proceed. So you see the
	// term about such included below.
	return(	
				// 3/25/2014 rmd : Test TP Micro is not in ISP mode.
				!tpmicro_comm.in_ISP &&

						// 3/25/2014 rmd : If there is a code version test pending the tpmicro is not ready. This
						// one variable indicates a full exchange between main board and tpmciro has taken place and
						// if the tpmicro needs a code update the tpmicro_comm.mode will not be NORMAL. Additionally
						// his ONE variable when set (false) also indicates the file system code date and time is
						// also valid. But we'll explicitly mention that term for clarity.
						!tpmicro_comm.code_version_test_pending &&
						
									// 3/25/2014 rmd : Ensure tpmicro code version stamp from file system is valid. The scan
									// response logic depends upon it to decide if tpmicro code needs distribution.
									tpmicro_comm.file_system_code_date_and_time_valid &&
									
												// 2/6/2015 rmd : And that the whats installed is valid. We want the what's installed
												// available BEFORE the comm_mngr makes the first token and the irri side responds with a
												// token response. Especially in a standalone since it gets to TOKEN makin' quickly as
												// compared to a chain which has to go through the scan process first. So in the standalone
												// we want the tpmicro what's installed available for the first token_resp built. And this
												// term manages that.
												tpmicro_data.whats_installed_has_arrived_from_the_tpmicro
		  );
}

/* ---------------------------------------------------------- */
static void attempt_to_kick_out_the_next_token( void )
{
	// 10/2/2012 rmd : The comm_mngr structure can be read or written to by several tasks. Take
	// the change flags about the need to distribute an edit for example. They are written to by
	// the key processing task and read and written to by the comm_mngr task.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	// ------------

	// We need this test because under normal operations the token response could come in before
	// or after the token timer has timed out. This is the mechanism that limits the token rate
	// to no faster than the time rate. The token can be slower than the timer rate however
	// depending on the communication method (radio for example). So only if the timer has timed
	// out and the last token has been processed in one way or another (TPL message, RESP timer
	// timed out, or an actual token RESP came in) do we go ahead and produce the next token.
	if( comm_mngr.token_rate_timer_has_timed_out == (true) )
	{
		if( comm_mngr.mode == COMM_MNGR_MODE_operational )
		{
			// 7/21/2016 rmd : And the flowsense powerup device connection timer has elapsed. This is
			// ONLY (false) during application startup. Probably should be set (false) and the timer
			// started coming in and out of device programming!
			if( comm_mngr.flowsense_devices_are_connected )
			{
				// 4/1/2014 rmd : In the case of a standalone this test may cause us to not make a token or
				// two on startup UNTIL the what's installed information has arrived from the tpmicro. And
				// that's a good thing. So we don't report system 'changes' when there weren't any!
				if( tpmicro_is_available_for_token_generation() )
				{
					// 4/1/2014 rmd : Has the last exchange completed?
					if( !comm_mngr.token_in_transit )
					{
						// 3/26/2014 rmd : Both master and slaves ALWAYS execute up to this point in this function.
						
						if( CODE_DISTRIBUTION_comm_mngr_should_be_idle() )
						{
							// 6/20/2017 rmd : Nothing to do. We are either distributing code down a chain, or as a
							// slave we are receiving a chain distribution. And the only way out of such a mode is an
							// eventual reboot.
						}
						else
						if( comm_mngr.start_a_scan_at_the_next_opportunity )
						{
							// 3/6/2014 rmd : We've been asked to start a scan and the comm_mngr is BETWEEN messages.
							// This is a graceful time to change states and start it.
							COMM_MNGR_start_a_scan();
						}
						else
						if( comm_mngr.pending_device_exchange_request )
						{
							// 3/17/2014 rmd : Process the request. If the device does not have an
							// initialize_device_exchange function nothing will happen. And the request will be set
							// (false).
							// 3/13/2014 rmd : If the function is not NULL safe to execute.
							if( port_device_table[ comm_mngr.device_exchange_device_index ].__initialize_device_exchange != NULL )
							{
								comm_mngr.device_exchange_saved_comm_mngr_mode = comm_mngr.mode;
								
								// 3/13/2014 rmd : Change the comm_mngr state and initialize the programming sequence for
								// the particular device.
								comm_mngr.mode = COMM_MNGR_MODE_device_exchange;
				
								port_device_table[ comm_mngr.device_exchange_device_index ].__initialize_device_exchange();
				
								// 4/1/2014 rmd : And stop our contact timer. While in this mode may not receive any tokens.
								// And user may stay in this mode for an extended period?
								xTimerStop( comm_mngr.timer_token_arrival, portMAX_DELAY );
							}
							
							// 3/13/2014 rmd : Always clear the pending flag. Request filled or not. We gave it a shot.
							comm_mngr.pending_device_exchange_request = (false);
						}
						else
						if( in_device_exchange_hammer )
						{
							// 6/30/2017 rmd : In device exchange, nothing to do.
						}
						else
						{
							// 4/30/2014 rmd : So here we are. At the core intention of this function. To make a token.
							// But we're not there yet!
				
							// ----------
							
							// 4/30/2014 rmd : Start or stop the rescan timer as needed.
							rescan_timer_maintenance();
								
							// ----------
							
							// 11/26/2014 rmd : Since this flag is only used during processing of scan responses and
							// since the scan has completed we should clear the flag here. The reason I decided to clear
							// it here instead of within the function that manages the next scan message is that slaves
							// and standalones don't actually SCAN. But both do arrive here. And I wanted the flag
							// cleared for all controllers. To avoid any funnys from cropping up if a controller is
							// switched from a standalone to a master or slave to a master. With this flag set (false)
							// during any subsequent scans if any slave lets us know he rebooted then we'll reboot too.
							// And that is the behavior we desire.
							comm_mngr.i_am_the_master_and_i_have_rebooted = (false);
							
							// ----------
							
							// 4/2/2014 rmd : For MASTERS only. Evaluate should we keep trying to communicate based upon
							// the number of failures to respond. If too many errors set the fall into forced flag.
							if( FLOWSENSE_we_are_the_master_of_a_multiple_controller_network() )
							{
								// 4/2/2014 rmd : And don't keep doing it over and over again. Once the chain is declared
								// DOWN, the only way out for a master is through the scan.
								if( !comm_mngr.chain_is_down )
								{
									comm_mngr.flag_chain_down_at_the_next_opportunity = ( nm_number_of_live_ones() != FLOWSENSE_get_num_controllers_in_chain() );
								}
							}
				
							// ----------
				
							// 4/1/2014 rmd : Process the request to fall into forced. Both masters and slaves make the
							// switch at this point. But follow two different paths to get here. MASTERS make their
							// request just above as you can see. And slaves when their contact timer fires.
							if( comm_mngr.flag_chain_down_at_the_next_opportunity )
							{
								transition_to_chain_down();
							
								// 4/1/2014 rmd : And clear the flag that caused the switch.
								comm_mngr.flag_chain_down_at_the_next_opportunity = (false);
							}
				
							// ----------
							
							if( FLOWSENSE_we_are_a_master_one_way_or_another() && !comm_mngr.chain_is_down )
							{
								// 4/4/2014 rmd : There are RARE times, particularly when we switch states with the token
								// making mode, where we might not actually skip a time slot to send a token out. Just the
								// way the logic works out.
								if( nm_set_next_contact_index_and_command() )
								{
									// 10/10/2018 rmd : At midnight, take a look at the accumulated, daily, token response,
									// error counts. For informational purposes, to understand how many errors we are seeing
									// each day (errors that may or may NOT have led to a chain down), make an alert showing
									// non-zero counts by controller index. I think this is useful to assess a chains
									// communication health.
									if( comm_mngr.we_have_crossed_midnight_so_clear_the_error_counts )
									{
										UNS_32 iii;
										
										for( iii=0; iii<MAX_CHAIN_LENGTH; iii++ )
										{
											if( chain.members[ iii ].saw_during_the_scan )
											{
												if( comm_mngr.failures_to_respond_to_the_token_today[ iii ] != 0 )
												{
													Alert_Message_va( "Daily Token Response Errors: for %c, %u errors", ('A' + iii), comm_mngr.failures_to_respond_to_the_token_today[ iii ] );
												}
											}
										}

										// 9/7/2018 rmd : Clear the 24hr error accumulator.
										memset( &(comm_mngr.failures_to_respond_to_the_token_today), 0x00, sizeof( comm_mngr.failures_to_respond_to_the_token_today ) );
										
										comm_mngr.we_have_crossed_midnight_so_clear_the_error_counts = (false);
									}
									
									// ----------

									// The token is from us. Only works for little endian!
									memcpy( &(next_contact.from_to.from), &(config_c.serial_number), 3 );
				
									// 2011.11.18 rmd : And it is to the serial number on record. This only works cause this is
									// a little endian machine (ARM). A big endian machine would pick up the wrong end of the
									// serial number. We WANT to use the least significant bytes of it.
									memcpy( &(next_contact.from_to.to), &(chain.members[ next_contact.index ].box_configuration.serial_number), 3 );
				
									// 8/8/2016 rmd : For debug only.
									//Alert_Message_va( "building token to %u", chain.members[next_contact.index].box_configuration.serial_number );
									
									// ----------	
				
									if( next_contact.command_to_use == MID_FOAL_TO_IRRI__CLEAN_HOUSE_REQUEST )
									{
										FOAL_COMM_build_token__clean_house_request();
									}
									else if( next_contact.command_to_use == MID_FOAL_TO_IRRI__TOKEN )
									{
										FOAL_COMM_build_token__irrigation_token();                   
									}
									else if( next_contact.command_to_use == MID_FOAL_TO_IRRI__CRC_ERROR_CLEAN_HOUSE_REQUEST )
									{
										//do nothing.
									}
									else
									{
										Alert_Message( "COMM_MNGR: unk command to use" );               
									}
				
									// ----------	
				
									start_message_resp_timer( (COMM_MNGR_token_timeout_seconds() * 1000) );
				
									// ----------	
				
									// Update to reflect the new about to be last contact.
									last_contact = next_contact;
				
									// ----------	
				
									// 4/3/2014 rmd : Being a token this message is to go out all non-internet connected ports.
									// So the port to send is rendered irrelevant in the following call by the msg class. Hence
									// the 0 for a port number.
									COMM_MNGR_send_outgoing_3000_message_and_take_on_memory_release_responsiblity( 0, next_contact.message_handle, next_contact.from_to, MSG_CLASS_CS3000_TOKEN );
				
									// ----------	
				
									// Flag a token has been sent.
									comm_mngr.token_in_transit = (true);
				
									// ----------	
				
									// And this may seem all wrong setting resetting the token_rate_timer_has_timed_out here. As
									// opposed to setting it FALSE when we actually start the timer. But there are several
									// scenarios to account for:
									//
									// 1. The token response comes in before the rate timer times out. We find ourselves in this
									// function trying to make another token but the timer hasn't timed out. So we leave.
									// Without starting the timer again. When the timer times out we are back here again. And
									// that time we make a token. Right on time.
									//
									// 2. The rate timer times out before the token response comes in. We find our selves here
									// in this function. We start the token rate timer again but do not reset the flag. This way
									// when the token resp does comes in and we again find ourselves in this function we will
									// send the next token without ONCE again waiting for the timer to timeout. THIS IS THE KEY
									// REASON WE SET IT HERE.
									//
									// 3. The rate timer times out and the token response never does show. We keep restarting
									// the token rate timer as our means to keep us pecking at this function trying to send
									// another token. Finally when the resp timer times out and clears the token in transit flag
									// we go ahead and send another token.
									//
									// 4. As we are waiting to fall into forced the situation is very similiar to 3 above except
									// that we can't send another token till we drop into forced.
									comm_mngr.token_rate_timer_has_timed_out = (false);
								}
							}
							else
							{
								// 3/27/2014 rmd : SLAVES always fall through to here. So do masters when the chain is down.
								
								// 4/30/2014 rmd : If we are a slave and the chain is NOT down, start the token arrival
								// timer.
								if( !FLOWSENSE_we_are_a_master_one_way_or_another() && !comm_mngr.chain_is_down )
								{
									// 4/1/2014 rmd : And for a slave make sure the contact timer is running. This would be for
									// the case we never got even a single token.
									if( xTimerIsTimerActive( comm_mngr.timer_token_arrival ) == pdFALSE )
									{
										// Resets and starts the timer countdown.
										xTimerStart( comm_mngr.timer_token_arrival, portMAX_DELAY );
									}
								}
							}
				
						}  // of we are going to see if we should be making tokens
						  
					}  // of last token response arrived or timeout occurred
				
				}  // of tpmicro or devices aint ready yet
				else
				{
					// debug message Alert_Message( "tpmicro ain't ready yet" );
				}
			}
			else
			{
				// debug message Alert_Message( "devices powerup delay not completed" );
			}

		}
		else
		if( comm_mngr.mode == COMM_MNGR_MODE_scanning )
		{
			// 4/30/2014 rmd : Nothing to do.
		}
		else
		if( comm_mngr.mode == COMM_MNGR_MODE_tpmicro_isp )
		{
			// 4/30/2014 rmd : Nothing to do.
		}
		else
		if( comm_mngr.mode == COMM_MNGR_MODE_device_exchange )
		{
			if( comm_mngr.pending_device_exchange_request )
			{
				// 3/17/2014 rmd : Process the request. If the device does not have an
				// initialize_device_exchange function nothing will happen. And the request will be set
				// (false).
				// 3/13/2014 rmd : If the function is not NULL safe to execute.
				if( port_device_table[ comm_mngr.device_exchange_device_index ].__initialize_device_exchange != NULL )
				{
					port_device_table[ comm_mngr.device_exchange_device_index ].__initialize_device_exchange();
				}
				
				// 3/13/2014 rmd : Always clear the pending flag. Request filled or not. We gave it a shot.
				comm_mngr.pending_device_exchange_request = (false);
			}
		}
		else
		{
			// 3/6/2014 rmd : Should never happen.
			Alert_Message( "Unexpd COMM_MNGR state!" );
		}


		// ----------

		// ALWAYS start the token timer here. Even though we may not have sent a token. This is to
		// cause us to try again and again to generate a token as we are waiting to fall into
		// forced. Or we are waiting for the resp timer to time out. (There is that limbo period
		// where we don't send any tokens until the CONTACT_TIMER times out and puts us in FORCED.
		// Then we send tokens to ourselves.)
		//
		// 3/14/2014 rmd : Being that the timer task is a high priority task using portMAX_DELAY
		// when posting is safe. Anything on the timer queue is quickly processed and therefore the
		// queue is effectively always empty.
		xTimerStart( comm_mngr.timer_token_rate_timer, portMAX_DELAY );

	}  // of if the token rate timer has timed out

	// ------------

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );

	// ------------
}

/* ---------------------------------------------------------- */
static void kick_out_the_next_scan_message( void )
{
	BOOL_32 at_least_one_controller_has_older_main_code;

	BOOL_32 at_least_one_controller_has_older_tpmicro_code;

	UNS_32  i;

	// ----------
	
	// 10/2/2012 rmd : The comm_mngr structure can be read or written to by several tasks. Take
	// the change flags about the need to distribute an edit for example. They are written to
	// by the key processing task and read and written to by the comm_mngr task.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	// ------------

	// Maintain accumulators.
	comm_stats.scan_msgs_generated += 1;

	// ----------

	if( comm_mngr.start_a_scan_at_the_next_opportunity )
	{
		// 3/6/2014 rmd : We've been asked to start a scan and the comm_mngr is already scanning.
		// Restart the scan.
		COMM_MNGR_start_a_scan();
	}
	else
	{
		// 4/4/2014 rmd : Is there another letter to scan?
		if( nm_set_next_contact_index_and_command() )
		{
			// Need to complete the scan - there must be another to go. Build
			// the message and load up the message ptr and FromTo address
			build_up_the_next_scan_message();

			// Update to reflect the new about to be last contact.
			last_contact = next_contact;

			// ----------
			
			// 6/30/2015 rmd : This is the SCAN timeout. The time between scan messages when there is no
			// response. The transport plays no role in this anymore as there is no status expected.
			//
			// 7/21/2016 rmd : We used to wait here 3000ms. But I observed that was way longer than
			// needed. Even on a complicated chain with M's and SR's with multiple repeaters and M chain
			// pieces the return of the SCAN_RESP from a controller that is there was impressively fast.
			// Well under one second. So I am going to tighten this up making it 60% of the orignal
			// value - tightening but being conservative about by how much. So now moved to 1800ms.
			start_message_resp_timer( 1800 );

			// ----------
			
			// 3/15/2012 rmd : The output port indicated for this message (UPORT_TP) is not used. If
			// this message is to us it does not go out any serial ports. If not to us it is pumped out
			// all available FLOWSENSE ports.
			COMM_MNGR_send_outgoing_3000_message_and_take_on_memory_release_responsiblity( UPORT_TP, next_contact.message_handle, next_contact.from_to, MSG_CLASS_CS3000_SCAN );
		}
		else
		{
			// 4/17/2014 rmd : SCAN COMPLETED.

			// ----------
			
			// 4/17/2014 rmd : If the correct number are in the chain it is appropriate to proceed. If
			// not, we restrict much user screen navigation, do not send any tokens even to ourselves,
			// and effectively become a brick. Waiting to scan again.
			if( nm_number_of_live_ones() == FLOWSENSE_get_num_controllers_in_chain() )
			{
				// 4/17/2014 rmd : The number of controllers seen matches the number in the chain. Looks
				// like a viable chain!

				// ----------

				// 5/18/2015 ajv : Notify the user that the chain has come up successfully.
				Alert_leaving_forced();

				// ---------
				
				// 9/8/2016 rmd : At this point since we got a good scan clear the number of rescans count
				// so that we reset the scan timing logic. If this chain goes down again we don't want to
				// wait 4 hours for another scan!
				comm_mngr.scans_while_chain_is_down = 0;

				// ----------
				
				at_least_one_controller_has_older_main_code = (false);
	
				#if CODE_DOWNLOAD_ALLOW_MASTER_TO_DISTRIBUTE_CODE

					for( i=0; i<MAX_CHAIN_LENGTH; ++i )
					{
						if( chain.members[ i ].saw_during_the_scan && !comm_mngr.main_app_firmware_up_to_date[ i ] )
						{
							at_least_one_controller_has_older_main_code = (true);
		
							Alert_Message_va( "Controller %c has older MAIN CODE", (i + 'A') );
		
							// 3/5/2014 ajv : Since we found one, we can technically skip processing the rest. However,
							// it may be nice to identify all of the controllers which have out-dated firmware, so let's
							// continue processing the rest.
						}
					}

				#elif defined NDEBUG
				
					// 5/16/2016 rmd : To prevent accidental build of release with code distribution disabled!
					#error COMPILING RELEASE CODE WITH CODE DISTRIBUTION DISABLED! NOT ALLOWED!
					
				#endif
				
				// ----------
				
				at_least_one_controller_has_older_tpmicro_code = (false);
	
				#if CODE_DOWNLOAD_ALLOW_MASTER_TO_DISTRIBUTE_CODE

					for( i=0; i<MAX_CHAIN_LENGTH; ++i )
					{
						if( chain.members[ i ].saw_during_the_scan && !comm_mngr.tpmicro_firmware_up_to_date[ i ] )
						{
							at_least_one_controller_has_older_tpmicro_code = (true);
		
							Alert_Message_va( "Controller %c has older TPMICRO CODE", (i + 'A') );
		
							// 3/5/2014 ajv : Since we found one, we can technically skip processing the rest. However,
							// it may be nice to identify all of the controllers which have out-dated firmware, so let's
							// continue processing the rest.
						}
					}

				#elif defined NDEBUG
				
					// 5/16/2016 rmd : To prevent accidental build of release with code distribution disabled.
					// Which could be a disaster on a chain.
					#error COMPILING RELEASE CODE WITH CODE DISTRIBUTION DISABLED! NOT ALLOWED!
					
				#endif
				
				// ----------
				
				// 11/25/2014 rmd : If during initial chain construction we determine there needs to be a
				// code update we have to take precaution to preserve any controllers flagged as NEW. The
				// approach we take is to make sure subsequent to the code update & controller restart that
				// they are once again flagged as NEW.
				if( at_least_one_controller_has_older_main_code || at_least_one_controller_has_older_tpmicro_code )
				{
					// 11/14/2014 ajv : Clear the parameter that originally triggered the NEW flag to be set.
					// This can either be the serial number or the "saw_during_the_scan" flag. Rather than muck
					// with serial numbers, we're going to clear the saw_during_the_scan flag for any
					// controllers which are currently marked as new. That way they will again be marked as NEW
					// when the scan takes place again after the code update.
					for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
					{
						if( comm_mngr.is_a_NEW_member_in_the_chain[ i ] )
						{
							chain.members[ i ].saw_during_the_scan = (false);

							// 11/14/2014 ajv : No need to reset the NEW_member flag since this is automatically done on
							// the next scan anyway.
						}
					}
				}

				// ----------
				
				if( at_least_one_controller_has_older_main_code )
				{
					if( !CODE_DISTRIBUTION_try_to_start_a_code_distribution( CODE_DISTRIBUTION_MODE_flowsense_transmitting_main ) )
					{
						// 6/1/2017 rmd : And make it so the chain is down, so while we wait for another scan
						// attempt, we won't go irrigating (operational) with the older code.
						for( i=0; i<MAX_CHAIN_LENGTH; i++ )
						{
							chain.members[ i ].saw_during_the_scan = (false);
						}
					}

					// 6/20/2017 rmd : Whether we started a code distribution or not, set the comm_mngr
					// operational/irrigation. If the code distribution is taking place we will not be making
					// make tokens, this is picked up when we try to make the next token. Eventually the scan
					// timer will timeout and another scan will be attempted.
					comm_mngr.mode = COMM_MNGR_MODE_operational;

					comm_mngr.state = COMM_MNGR_STATE_irrigation;
				}
				else
				if( at_least_one_controller_has_older_tpmicro_code )
				{
					if( !CODE_DISTRIBUTION_try_to_start_a_code_distribution( CODE_DISTRIBUTION_MODE_flowsense_transmitting_tpmicro ) )
					{
						// 6/1/2017 rmd : And make it so the chain is down, so while we wait for another scan
						// attempt, we won't go irrigating (operational) with the older code.
						for( i=0; i<MAX_CHAIN_LENGTH; i++ )
						{
							chain.members[ i ].saw_during_the_scan = (false);
						}
					}

					// 6/20/2017 rmd : Whether we started a code distribution or not, set the comm_mngr
					// operational/irrigation. If the code distribution is taking place we will not be making
					// make tokens, this is picked up when we try to make the next token. Eventually the scan
					// timer will timeout and another scan will be attempted.
					comm_mngr.mode = COMM_MNGR_MODE_operational;

					comm_mngr.state = COMM_MNGR_STATE_irrigation;
				}
				else
				{
					// 4/17/2014 rmd : If the correct number are in the chain it is appropriate to hide hardware
					// based upon those who didn't respond. And to process the NEW controllers by passing
					// through the cleaning house state.
					hide_hardware_based_upon_scan_results();
	
					// 4/25/2014 rmd : Need to send to the NEW controllers in the chain all exisiting stations,
					// pocs, hardware in general, and all program data settings. From all controllers in the
					// chain. In other words trigger the group-by-group broadcast of all the file data.
					if_any_NEW_in_the_chain_then_distribute_all_groups();

					// ----------
					
					comm_mngr.mode = COMM_MNGR_MODE_operational;
	
					// 4/16/2014 rmd : Always set to pass through the cleaning house state. If none to clean
					// will just pass through this state to the irrigation state.
					comm_mngr.state = COMM_MNGR_STATE_cleaning_house;
					
					// 4/24/2014 rmd : This is a good idea after a scan. The receipt of which at the slaves will
					// cause another read of its own what's installed from its tpmicro. If any differences are
					// detected compared to the chain members array this flag causes us to send, that slave will
					// send his box configuration which will in turn cause us to set this falg again. The circle
					// won't stop until all in sync.
					comm_mngr.broadcast_chain_members_array = (true);
					
					// 4/1/2014 rmd : And now wait for the token rate timer to expire! Which it will shortly as
					// the token rate timer runs for the life of the application.
				}

			}  // of if we found the correct number of controllers
			else
			{
				comm_mngr.mode = COMM_MNGR_MODE_operational;
			
				// 4/17/2014 rmd : Well the scan failed to find the expected number of controllers. Do not
				// attempt to distribute any code updates to the ones we did find. Do not process the CLEAN
				// HOUSE state. When we go to kick out the next token will be flagged as 'chain down'. Any
				// ongoing irrigation will be destroyed and keypad/screen activity severely restricted.
				comm_mngr.state = COMM_MNGR_STATE_irrigation;
			}
			
			// ----------
			
			// 6/20/2017 rmd : Regardless of if we are distributing code down the chain or not, or
			// cleaning house, or if the scan failed, give the hub the go ahead to try and distribute
			// code itself. If we are already distributing code down the chain, the hub distribution
			// will be blocked, because we only allow one distribution at a time.
			CODE_DISTRIBUTION_post_event( CODE_DISTRIBUTION_EVENT_commmngr_ready );
			
			// ----------

			// 4/17/2014 rmd : Regardless how the scan turned out, housekeeping to make sure token state
			// machine can advance when the timer fires.
			comm_mngr.token_in_transit = (false);
	
		}  // end of scan completed
		
	}  // of scan restarted

	// ------------

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void process_message_resp_timer_expired( void )
{
	// 10/1/2012 rmd : Scanning aside, I believe normally we only come here when the OM has been
	// satisfied but there is no IM response to that OM - a kind of abnormal yet possible
	// occurance. We should normally see instances of when the OM status timer expires and we
	// then post a queue message from the TPL to the COMM MNGR to handle that.

	// ------------

	// 10/2/2012 rmd : The comm_mngr structure can be read or written to by several tasks. Take
	// the change flags about the need to distribute an edit for example. They are written to by
	// the key processing task and read and written to by the comm_mngr task.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	// ------------

	// A sanity check
	if( last_contact.index >= MAX_CHAIN_LENGTH )
	{
		Alert_Message( "last contact index out of range" );

		// Do something - fix it at least.
		last_contact.index = 0;
	}

	// ------------

	if( comm_mngr.mode == COMM_MNGR_MODE_scanning )
	{
		// 4/17/2014 rmd : Mark as not present.
		chain.members[ last_contact.index ].saw_during_the_scan = (false);
		
		// 4/17/2014 rmd : Because we are executing this function within the context of the
		// comm_mngr task this is perfectly safe to do.
		kick_out_the_next_scan_message();
	}
	else
	{
		Alert_msg_response_timeout_idx( last_contact.index );

		// ----------
		
		// 10/10/2018 rmd : Increment the error tracking variables. These variables are used to
		// decide when we've had enough, and declare 'chain_down'.
		bump_number_of_failures_to_respond_to_token( last_contact.index );
		
		// ----------
		
		// 3/6/2014 rmd : We indicate the ongoing transaction is done. One of the criteria to begin
		// the next token exchange.
		comm_mngr.token_in_transit = (false);

		post_attempt_to_send_next_token();
	}

	// ------------

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void transport_says_last_OM_failed( COMM_MNGR_TASK_QUEUE_STRUCT cmqs )
{
	// 6/30/2015 rmd : Because we are not waiting for a status in the OM this should never
	// happen.
	Alert_Message( "OM say status didn't arrive!" );
	
	// ----------
	
	// 10/2/2012 rmd : The comm_mngr structure can be read or written to by several tasks. Take
	// the change flags about the need to distribute an edit for example. They are written to by
	// the key processing task and read and written to by the comm_mngr task.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	// ----------

	stop_message_resp_timer();

	// ----------

	// A sanity check
	if( last_contact.index >= MAX_CHAIN_LENGTH )
	{
		Alert_Message( "last contact index out of range" );

		// Do something - fix it at least.
		last_contact.index = 0;
	}

	// ----------
	
	if( cmqs.message_class == MSG_CLASS_CS3000_SCAN )
	{
		// A sanity check.
		if( comm_mngr.mode != COMM_MNGR_MODE_scanning )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "CommMngr : conflicting state" );
		}

		// ----------

		chain.members[ last_contact.index ].saw_during_the_scan = (false);

		// Since we are executing at the task level it is safe to call this other task level function.
		kick_out_the_next_scan_message();

		// ----------
	}
	else
	if( cmqs.message_class == MSG_CLASS_CS3000_TOKEN )
	{
		// A sanity check. Hmmmm. A token to ourselves should NEVER fail. So this must be a token
		// sent to another controller and we are the master. Must be in the normal state.
		if( comm_mngr.mode != COMM_MNGR_MODE_operational )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "CommMngr : conflicting state" );
		}

		// ----------

		// Because these are the incoming messages that 'drive' the comm manager we have some special things to do.
		test_last_incoming_resp_integrity( cmqs.who_the_message_was_to, "transport msg" );

		// ----------
		
		// 10/10/2018 rmd : Increment the error tracking variables. These variables are used to
		// decide when we've had enough, and declare 'chain_down'.
		bump_number_of_failures_to_respond_to_token( last_contact.index );
		
		// ----------

		// 3/6/2014 rmd : We indicate the ongoing transaction is done. One of the criteria to begin
		// the next token exchange.
		comm_mngr.token_in_transit = (false);

		post_attempt_to_send_next_token();
	}
	else
	{
		Alert_Message( "COMM_MNGR : unexpected msg_class from tpl" );
	}

	// ----------

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION Think of this function of the incoming message interface for the transport.
	The message at the packet level has been determined to be for us. So it was assembled by
	the transport and finally when complete this function is called. Another way this
	function might get called is by the various message generators physically within this
	controller. For example the comm_mngr itself will generate tokens for our self (our
	serial number). And those must be received. The outgoing message sending will intercept
	such outgoing messages and directly have them added to the list of incoming messages.
	And this function is called.

	@CALLER_MUTEX_REQUIREMENTS (none by the caller)

	@MEMORY_RESPONSIBILITES Because TOKENS are broadcast we find ourselves running TOKEN
	messages through this function to ourselves and additionally routed out all accepting
	serial ports (given to TPL_OUT). Because of such the message memory must remain intact
	throughout both operations. And therefore we must take a copy of the message memory and
	reference the copy on list.

	@EXECUTED Executed within the context of the COMM_MNGR task. When an outgoing message (
	(a TOKEN) is determined to be for ourselves. Additionally called from within the TPL_IN
	task after a complete message to us has been assembled.
	
	@RETURN (none)

	@AUTHOR 2012.03.15 rmd
*/
extern void CENT_COMM_make_copy_of_and_add_message_or_packet_to_list_of_incoming_messages( UPORTS const pport, DATA_HANDLE const pdh, ROUTING_CLASS_DETAILS_STRUCT const pmessage_class, ADDR_TYPE const pfrom_to )
{
	// 11/17/2014 rmd : Called within the context of the RING_BUFFER analysis task. Also called
	// from within the context of the TPL_IN task when a completed message has been rcvd.

	// 11/17/2014 rmd : PACKETS that come from either the comm_server or are part of the code
	// distribution are MESSAGES in their own right. So here you'll see the terminology crossing
	// over from packets to messages. And that's okay.

	// 11/17/2014 rmd : We must make a copy of this packet here as we still need the packet
	// intact for SCATTERING. And since we are placing this packet in a list and signaling the
	// COMM_MNGR about this packet(message) it will be processed before we have a chance to
	// scatter it GIVEN that the comm_mngr task is at a higher priority task than the
	// ring_buffer task that is executing this function.

	COMMUNICATION_MESSAGE_LIST_ITEM		*cmli;
	
	COMM_MNGR_TASK_QUEUE_STRUCT			cmqs;
	
	DATA_HANDLE							ldh;

	// ----------

	// 11/17/2014 rmd : Get new memory and make a copy of the packet to be associated with the
	// list item.
	ldh.dptr = mem_malloc( pdh.dlen );

	memcpy( ldh.dptr, pdh.dptr, pdh.dlen );
	
	ldh.dlen = pdh.dlen;
	
	// ----------

	// 3/16/2012 rmd : Create the message list item.
	cmli = mem_malloc( sizeof(COMMUNICATION_MESSAGE_LIST_ITEM) );
	
	// ----------

	cmli->dh = ldh;

	cmli->from_to = pfrom_to;
	
	cmli->in_port = pport;

	cmli->message_class = pmessage_class;
	
	// ----------

	xSemaphoreTakeRecursive( list_incoming_messages_recursive_MUTEX, portMAX_DELAY );

	nm_ListInsertTail( &comm_mngr.incoming_messages_or_packets, cmli );
	
	xSemaphoreGiveRecursive( list_incoming_messages_recursive_MUTEX );
	
	// ----------

	cmqs.event = COMM_MNGR_EVENT_there_is_a_new_incoming_message_or_packet;
	
	COMM_MNGR_post_event_with_details( &cmqs );
}

/* ---------------------------------------------------------- */
static void process_incoming_messages_list( void )
{
	// 3/27/2014 rmd : This function executed within the context of the comm_mngr task.
	
	// ------------

	COMMUNICATION_MESSAGE_LIST_ITEM     *cmli;
	
	DATA_HANDLE							message_copy;
	
	RADIO_TEST_TASK_QUEUE_STRUCT		radio_test_queue_item;

	// 4/1/2014 rmd : These flags help us to understand system timing. Particularly between
	// master and slaves in the face of tpmicro ISP updates. Do we ever receive scan messages
	// that are dropped because the tpmicro has not made enough exchanges? These are also a form
	// of a SANITY check. Should we be receiving a SCAN_RESP message if we are not SCANNING? I'd
	// say not. And these variables let us see things like that.
	BOOL_32		allowed_to_process_scan, allowed_to_process_scan_resp;
	
	BOOL_32		allowed_to_process_token, allowed_to_process_token_resp;
	
	// ----------
	
	// 3/27/2014 rmd : Well the world has to be pretty much up to snuf to process these
	// messages. TPMicro happy. Main board happy. No code distribution or update issues. One
	// implication of this term is if a tpmicro is BROKEN or physically not present at a slave
	// the slave will not respond to the scan. Which is fine.

	// 4/1/2014 rmd : SCAN messages need a tpmicro out of ISP mode. Which I guess is a given if
	// the incoming scan message came down an M cable. But a given when we send the scan to
	// oursleves. Additionally we need to file system tpmicro code time and date to be valid.
	// BECAUSE we include that in the scan response.
	//
	// 4/14/2014 rmd : Slaves are in the 'making_tokens' MODE when they receive their incoming
	// scan message. Therefore the 'making_tokens' term that follows.
	allowed_to_process_scan = ( (comm_mngr.mode == COMM_MNGR_MODE_scanning) || (comm_mngr.mode == COMM_MNGR_MODE_operational) ) && !tpmicro_comm.in_ISP && tpmicro_comm.file_system_code_date_and_time_valid;

	allowed_to_process_scan_resp = ( comm_mngr.mode == COMM_MNGR_MODE_scanning );

	allowed_to_process_token = ( tpmicro_is_available_for_token_generation() && (comm_mngr.mode == COMM_MNGR_MODE_operational) );

	allowed_to_process_token_resp = ( tpmicro_is_available_for_token_generation() && (comm_mngr.mode == COMM_MNGR_MODE_operational) );

	// ----------
	
	xSemaphoreTakeRecursive( list_incoming_messages_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	// Process ALL incoming messages that are on the list. As each one is completed release the message memory.
	cmli = nm_ListRemoveHead( &comm_mngr.incoming_messages_or_packets );

	while( cmli != NULL )
	{
		if( cmli->message_class.routing_class_base == ROUTING_CLASS_IS_2000_BASED )
		{
			// 11/28/2017 rmd : This is for handling the 2 2000e based comm_test messages that could
			// come at us (only if we are a HUB though does the packet router let them through).
			if( cmli->message_class.rclass == MSG_CLASS_2000e_TEST_PACKET_FROM_C_OR_C )
			{
				// 11/28/2017 rmd : This would be if the 2000e was generating the test messages.
                Alert_Message_va( "Test Message rcvd from 2000e %c%c%c", cmli->from_to.from[2], cmli->from_to.from[1], cmli->from_to.from[0] );

				RADIO_TEST_echo_inbound_2000e_test_message_back_to_where_it_came_from( cmli->from_to, cmli->in_port, cmli->dh );
			}
			else
			if( cmli->message_class.rclass == MSG_CLASS_2000e_TEST_PACKET_ECHO_TO_C_OR_C )
			{
				// 11/28/2017 rmd : This would be if we (this 3000 hub or otherwise?) originated the test.
				// MAKE SURE WE GET A COPY OF THE MESSAGE as the message memory is freed at the end of this
				// function, and we are only handing this message memory overto another task for subsequent
				// processing. Depending on the task priority we do not know if the message memory has been
				// freed or not by the time we get around to processing the message in the link_test task.
				if( mem_obtain_a_block_if_available( cmli->dh.dlen, (void**)&(message_copy.dptr) ) )
				{
					// 12/12/2017 rmd : We got the memory now fill it.
					memcpy( message_copy.dptr, cmli->dh.dptr, cmli->dh.dlen );

					message_copy.dlen = cmli->dh.dlen;
					
					radio_test_queue_item.event = RADIO_TEST_EVENT_here_is_an_inbound_echo_to_process;
					radio_test_queue_item.dh = message_copy;
					radio_test_queue_item.from_port = cmli->in_port;
					radio_test_queue_item.message_class = cmli->message_class;
					
					RADIO_TEST_post_event_with_details( &radio_test_queue_item );
				}
			}
			else
			{
				Alert_Message( "Incoming 2000e based message not processed" );
			}
		}
		else
		if( cmli->message_class.routing_class_base == ROUTING_CLASS_IS_3000_BASED )
		{
			// 3/27/2014 rmd : If 3000 messages are not allowed to be processed they are thrown away.
			
			switch( cmli->message_class.rclass )
			{
				case MSG_CLASS_CS3000_SCAN:
					// 4/29/2014 rmd : In the packet router this has been tested to be for us.
					if( allowed_to_process_scan )
					{
						// 7/2/2015 rmd : Well the master is talking to us so restart the contact timer to help
						// prevent from falling into forced during code receipt at the master. During the code
						// distribution the timer is stopped.
						xTimerStart( comm_mngr.timer_token_arrival, portMAX_DELAY );
	
						process_incoming_scan( cmli );
					}
					else
					{
						Alert_Message( "Incoming SCAN not processed" );
					}
					break;
	
	
				case MSG_CLASS_CS3000_SCAN_RESP:
					// Maintain accumulators.
					comm_stats.scan_msg_responses_rcvd += 1;
	
					// 4/29/2014 rmd : In the packet router this has been tested to be for us.
					if( allowed_to_process_scan_resp )
					{
						stop_message_resp_timer();
		
						process_incoming_scan_RESP( cmli );
		
						// Since we are executing within the context of this task it is safe to call this other task level function.
						kick_out_the_next_scan_message();
					}
					else
					{
						Alert_Message( "Incoming SCAN_RESP not processed" );
					}
					break;
	
	
				case MSG_CLASS_CS3000_TOKEN:
					// 4/29/2014 rmd : In the packet router this has NOT been tested to be for us. All TOKENs
					// are received based only upon the message CLASS. Not the TO address.
					if( allowed_to_process_token )
					{
						// 4/30/2014 rmd : Well for the case of SLAVES they have no way to clear their chain_down
						// flag. And there really is nothing to do regarding that. Unlike the 2000e where we had to
						// go through a complicated process. But since there is no ongoing irrigation when the chain
						// is down all we do is flip the flag. Note - MASTERS change their chain_down setting as
						// part of the scan startup function.
						if( comm_mngr.chain_is_down )
						{
							Alert_leaving_forced();
	
							comm_mngr.chain_is_down = (false);
						}
						
						// ----------
						
						// Restart the contact timer : resets and starts the timer countdown.
						xTimerStart( comm_mngr.timer_token_arrival, portMAX_DELAY );
						
						// ----------
						
						if( *((COMM_COMMANDS*)cmli->dh.dptr) == MID_FOAL_TO_IRRI__CLEAN_HOUSE_REQUEST )
						{
							IRRI_COMM_process_incoming__clean_house_request( cmli );
						}
						else if( *((COMM_COMMANDS*)cmli->dh.dptr) == MID_FOAL_TO_IRRI__TOKEN )
						{
							IRRI_COMM_process_incoming__irrigation_token( cmli );
						}
						else if( *((COMM_COMMANDS*)cmli->dh.dptr) == MID_FOAL_TO_IRRI__CRC_ERROR_CLEAN_HOUSE_REQUEST )
						{
							IRRI_COMM_process_incoming__crc_error_clean_house_request( cmli );
						}
						else
						{
							ALERT_MESSAGE_WITH_FILE_NAME( "Unknown command" );
						}
					}
					else
					{
						Alert_Message( "Incoming TOKEN not processed" );
					}
					break;
	
	
				case MSG_CLASS_CS3000_TOKEN_RESP:
					// 4/29/2014 rmd : In the packet router this has been tested to be for us.
					if( allowed_to_process_token_resp )
					{
						stop_message_resp_timer();
		
						// Presumably the reason 'we' are receiving this message is because we are the master. 'We' could be a single controller or
						// the master on a chain of controllers. This type of incoming message is the response the commmngr is looking for. And as
						// these are the incoming messages that 'drive' the comm manager we have some special things to do.
						test_last_incoming_resp_integrity( cmli->from_to, "token resp" );
						
						// ----------

						// 10/10/2018 rmd : We require 4 errors to declare a chain down. The criteria used to be 4
						// errors within a 24 hour period, but on some SR chains (current implementation does not
						// include our transport layer) we would see say one error ever 4 hours for example, and
						// each day this would eventually lead to chain down. To avoid this, we changed the criteria
						// to need 4 errors IN A ROW to declare a chain down. So each time we successfully got a
						// token response we restart the error count, and this is how we implement the '4-in-a-row'
						// criteria.
						comm_mngr.failures_to_respond_to_the_token[ last_contact.index ] = 0;
						
						// ----------
						
						if( *((COMM_COMMANDS*)cmli->dh.dptr) == MID_IRRI_TO_FOAL__CLEAN_HOUSE_REQUEST_RESP )
						{
							FOAL_COMM_process_incoming__clean_house_request__response( cmli );
						}
						else if( *((COMM_COMMANDS*)cmli->dh.dptr) == MID_IRRI_TO_FOAL__TOKEN_RESP )
						{
							FOAL_COMM_process_incoming__irrigation_token__response( cmli );
						}
						else
						{
							ALERT_MESSAGE_WITH_FILE_NAME( "Unknown command" );
						}
		
						// ----------
		
						// 3/6/2014 rmd : We indicate the ongoing transaction is done. One of the criteria to begin
						// the next token exchange.
						comm_mngr.token_in_transit = (false);
		
						post_attempt_to_send_next_token();
					}
					else
					{
						Alert_Message( "Incoming TOKEN_RESP not processed" );
					}
					break;
	
				default:
					Alert_Message( "UNK incoming 3000 message class to process" );
					break;
	
			}  // of switch of message class
		}


		// ----------		

		// Free the message memory and the list item memory.
		mem_free( cmli->dh.dptr );

		mem_free( cmli );

		// Take the next one. If there is one.
		cmli = nm_ListRemoveHead( &comm_mngr.incoming_messages_or_packets );
	}

	// ----------
	
	xSemaphoreGiveRecursive( list_incoming_messages_recursive_MUTEX );
}

/* ---------------------------------------------------------- */

// 3/25/2014 rmd : This would be a static function but it is referenced from the
// tpmicro_comm.c source. Which is really an extention of the comm_mngr itself.
// Intentionally does not use the COMM_MNGR naming convention.
extern void setup_to_check_if_tpmicro_needs_a_code_update( void )
{
	// 3/4/2014 rmd : This function is to be called in two places. Once when the comm_mngr task
	// starts. And when a new tpmicro file is written to the file system. In both instances the
	// end result is a decision will be made to enter ISP mode and update the tpmicro code or
	// not.

	// ----------

	// 3/25/2014 rmd : Set the flag that holds off scans and tokens till potnetial ISP code
	// download and file version test completed.
	tpmicro_comm.code_version_test_pending = (true);

	// ----------

	// 2/20/2014 rmd : Set the flags that cause a comparison of what the tpmicro is running to
	// what we have on file here. If what is on file is newer a tpmicro code update is to occur.
	// When both flags are (true) we make the comparison and decode if a tpmicro code update is
	// needed.
	tpmicro_comm.tpmicro_executing_code_date_and_time_valid = (false);

	tpmicro_comm.tpmicro_request_executing_code_date_and_time = (true);

	// ----------

	tpmicro_comm.file_system_code_date_and_time_valid = (false);

	// 3/4/2014 rmd : This will return a comm_mngr queue message with the answer. At which time
	// we can extract the date and time and set the flag true.
	FLASH_STORAGE_request_code_image_version_stamp( TPMICRO_APP_FILENAME );
}


/* ---------------------------------------------------------- */
extern void COMM_MNGR_task( void *pvParameters )
{
	COMM_MNGR_TASK_QUEUE_STRUCT    cmqs;

	// 10/23/2012 rmd : To support the WDT activity monitor.
	UNS_32		task_index;
	
	BOOL_32		start_the_FlowSense_SR_radio_connection_timer;

	UNS_32		iii;
	
	// ----------

	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// ----------

	// 6/30/2017 rmd : Our brute force variable, protecting the serial ports from task
	// activities during the device exchange mode.
	in_device_exchange_hammer = (false);
	
	// ----------

	xSemaphoreTakeRecursive( list_incoming_messages_recursive_MUTEX, portMAX_DELAY );

	nm_ListInit( &comm_mngr.incoming_messages_or_packets, 0 );

	xSemaphoreGiveRecursive( list_incoming_messages_recursive_MUTEX );


	xSemaphoreTake( list_packets_waiting_for_token_MUTEX, portMAX_DELAY );

	nm_ListInit( &comm_mngr.packets_waiting_for_token, 0 );

	xSemaphoreGive( list_packets_waiting_for_token_MUTEX );

	// ----------
	
	// 11/26/2014 rmd : Set both flags (true). Regardless of if we are a master of slave. The
	// master will receive his own token response alert him that he himself has rebooted. But
	// will ignore that flag cause the master rebooted flag is also set.
	comm_mngr.i_am_the_master_and_i_have_rebooted = (true);

	comm_mngr.i_am_a_slave_and_i_have_rebooted = (true);

	// ----------
	
	// 4/30/2014 rmd : When this timer expires a slave will set chain_down (true).
	comm_mngr.timer_token_arrival = xTimerCreate( NULL, MS_to_TICKS((contact_timer_milliseconds())), FALSE, NULL, token_arrival_timer_callback );

	// ----------
	
	// Create the RESCAN timer. Actual timer period is set when timer is started.
	comm_mngr.timer_rescan = xTimerCreate( NULL, MS_to_TICKS(1000), FALSE, NULL, message_rescan_timer_callback );

	// ----------
	
	// Create the TOKEN RATE timer. Note that the period of this timer (minimum ms between
	// tokens) sets the fastest token rate. If communications delays take more than this timer
	// is set for then when the token reponse arrives and is processed, another token is
	// immediately sent.
	comm_mngr.timer_token_rate_timer = xTimerCreate( NULL, MS_to_TICKS( COMM_MNGR_MINIMUM_MS_BETWEEN_TOKENS ), FALSE, NULL, token_rate_timer_callback );

	// ----------
	
	// Create the message RESP timer. Actual timer period is set when timer is started.
	comm_mngr.timer_message_resp = xTimerCreate( NULL, MS_to_TICKS(1000), FALSE, NULL, message_resp_timer_callback );

	// ----------
	
	// 4/20/2015 rmd : Create the COMMSERVER MSG RECEIPT ERROR timer. The timer period is not
	// important here as we start the timer with the ChangePeriod function.
	comm_mngr.timer_commserver_msg_receipt_error = xTimerCreate( NULL, MS_to_TICKS(30000), (false), NULL, commserver_msg_receipt_error_timer_callback );

	// ----------
	
	// 4/30/2014 rmd : Create the device exchange (programming and reading) timer.
	comm_mngr.timer_device_exchange = xTimerCreate( NULL, MS_to_TICKS(100), FALSE, NULL, device_exchange_timer_callback );

	// 4/30/2014 rmd : And test that this timer created. We only test this timer create. Our
	// logic being by keeping this timer as the LAST timer to create, if this one creates, then
	// there must have been memory to create the others. Saving the code space for the extra
	// ALERT lines. And no memory is about the only thing that can go wrong.
	if( comm_mngr.timer_device_exchange == NULL )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "Timer NOT CREATED" );
	}

	// ----------

	// 4/1/2014 rmd : ALWAYS starts in tpmicro ISP mode. To make sure the tpmicro is running and
	// its code is up to date as compared to our file system. Not until that has been verified
	// will we proceed to the scan. Which is ALWAYS next.
	comm_mngr.mode = COMM_MNGR_MODE_tpmicro_isp;

	comm_mngr.chain_is_down = (false);

	comm_mngr.flag_chain_down_at_the_next_opportunity = (false);

	// 3/6/2014 rmd : DO NOT start the scan yet. The scan will automatically commence after
	// tpmicro code verification mode completes.
	comm_mngr.start_a_scan_at_the_next_opportunity = (false);

	// 3/13/2014 rmd : This request is set (true) via a comm mngr queue message. And is only
	// acted upon when the comm_mngr is making tokens. No particular reason that it is only
	// honored then, except for it keeps the code less complicated.
	comm_mngr.pending_device_exchange_request = (false);
	
	comm_mngr.token_rate_timer_has_timed_out = (true);	// set true to allow message generation

	comm_mngr.token_in_transit = (false);

	// 3/6/2014 rmd : Start this timer. It is meant to run for the lifetime of the application.
	//
	// 3/14/2014 rmd : Being that the timer task is a high priority task using portMAX_DELAY
	// when posting is safe. Anything on the timer queue is quickly processed and therefore the
	// queue is effectively always empty.
	xTimerStart( comm_mngr.timer_token_rate_timer, portMAX_DELAY );
	
	// ------------

	// 3/26/2013 rmd : After the startup task has created all the other tasks it posts a message
	// to this task, the comm_mngr, to start a scan. We used to just start the scan here by
	// calling __start_a_scan. But the serial drivers aren't yet initialized so the outgoing
	// message packet that tries to add itself to the xmit list fails. As the list does not yet
	// exist.

	// ------------

	// 4/2/2014 rmd : This flag always start out true. Will flip false if the tpmicro is not
	// running properly. Even if the ISP is botched. Or no matter. If the tpmicro is not
	// responding it flips false. Used to prevent further packets to the tp micro - which fill
	// the alerts because we don't see the flow control packet arrive. And can be used to
	// attempt a restart at say mid-night to allow the tpmicro to try again.
	tpmicro_comm.up_and_running = (true);

	// 2/12/2014 rmd : For the TPMicro we start off doing a quick test to see if the ISP is
	// running. If we conclude it is not running it is assumed our application is running. Which
	// is what we want. We WANT THE INTIAL test for ISP to fail. It normally should. If the ISP
	// is running we then download the latest code file we have here in our main board file
	// system. The reason being we assume if the ISP is running either a prior code update
	// sequence to the TP Micro failed (power failure could cause this) OR there never was any
	// code in the TP Micro. The whole sequence to test for ISP and then gracefully exit takes
	// about 150ms. At about 170ms we send our first regular message to the tp micro (given that
	// the ISP is not running). If the ISP is running it going to be about 40 seconds till
	// regular messaging begins.
	tpmicro_comm.in_ISP = (true);

	// 2/19/2014 rmd : The question mark state is the first step in communicating with the ISP.
	// If it is running!
	tpmicro_comm.isp_state = ISP_STATE_question_mark;

	// ----------

	// 5/9/2016 ajv : Explicitly ensure the perform 2-Wire discovery flag is not set upon
	// startup.
	comm_mngr.perform_two_wire_discovery = (false);

	// ----------
	
	// 7/21/2016 rmd : Create the flowsense device connection timer and set the flag (false) to
	// prevent the comm_mngr from scanning or generating tokens till the timer has expired and
	// the flag is set (true).
	//
	// 7/21/2016 rmd : Presently set to 30 seconds. The time I have observed needed to allow
	// various SR radio topologies to sync up to one another.
	comm_mngr.flowsense_device_establish_connection_timer = xTimerCreate( NULL, MS_to_TICKS( 30000 ), FALSE, NULL, flowsense_device_connection_timer_callback );

	comm_mngr.flowsense_devices_are_connected = (false);
	
	// ----------
	
	// 5/8/2017 ajv : Check for invalid communication configurations here (e.g., central device
	// attached to a FLOWSENSE slave) using chain members.
	UNS_32	pindex_of_controller_with_central_device;

	if( an_FL_slave_has_a_central_device( &pindex_of_controller_with_central_device ) )
	{
		Alert_Message_va( "Central device connected to FL slave %c", pindex_of_controller_with_central_device + 'A' );	
	}

	// ----------
		
	while( (true) )
	{
		// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
		if( xQueueReceive( COMM_MNGR_task_queue, &cmqs, MS_to_TICKS( 500 ) ) == pdTRUE )
		{
			switch( cmqs.event )
			{
				case COMM_MNGR_EVENT_commence:
				
					// 3/25/2014 rmd : This event is posted to us from the app_startup task. Indicating all
					// subsytems/tasks are ready to go. So for example we can use the serial drivers and KNOW
					// those tasks are ready for us to use.
					//
					// 3/25/2014 rmd : The way the startup now works is the first thing that happens is the
					// TPMICRO tests if the ISP bootloader is running. This would indicate the TPMICRO had no
					// code in it (possibly from a failed code update session!). If the ISP is running a tpmicro
					// code update session takes place. If not the tpmicro code is assummed to be running.
					//
					// After the ISP query we go through some exchange with the tpmicro (including a code
					// version test). Following a sucessful code version test we are ready to begin a scan. The
					// ISP conclusion puts the comm_mngr into NORMAL mode. Which upon creation of the first
					// token flips to do a scan.

					// ----------
					
					// 11/11/2016 rmd : If the device on PORT_A is used for controller initiated messages to the
					// commserver it's power up sequence is handled within the CONTROLLER_INITIATED task so
					// don't do it here. If there is device however and we're not using it for controller
					// initiated, which would mean we are using it for flowsense, power it up here.
					if( !CONFIG_this_controller_originates_commserver_messages() && (config_c.port_A_device_index != 0) )
					{
						power_up_device( UPORT_A );
					}
					
					// 9/7/2017 rmd : Generally speaking any device on PORT B is powered up here. HOWEVER there
					// is a special case for the RAVEON LR devices. In that case the CI task is considered to
					// 'own' the device, and actually takes responsibility for powereing up and performing the
					// IDENTIFICATION process (that is to identify between sonik, fireline, and stingray). If it
					// is a RAVEON device on port B it is already powered by the time this code executes.
					if( (config_c.port_B_device_index != 0) && (config_c.port_B_device_index != COMM_DEVICE_LR_RAVEON) )
					{
						power_up_device( UPORT_B );
					}
					
					// ----------
					
					start_the_FlowSense_SR_radio_connection_timer = (false);
					
					// 7/21/2016 rmd : Keep in mind this event (COMM_MNGR_EVENT_commence) only executes once
					// following a code restart. Our job here is to decode under what condition we want to start
					// a device connection timer for Flowsense traffic. SR is the only device at this time. In
					// the various network topologies (master, slaves, repeaters) it takes time for the radios
					// to find each other. So we want to hold off starting the SCAN until the devices have
					// connected.
					if( FLOWSENSE_we_are_the_master_of_a_multiple_controller_network() )
					{
						// 7/22/2016 rmd : So ONLY for MASTERS do we possibly start this timer. And the philosophy
						// is if there is any doubt about starting it ... start it. In other words if there might be
						// SR's out there start it.

						// 7/22/2016 rmd : This logic could mistakenly NOT start the timer when it is needed. But
						// the worst that happens is the SCAN fails and automatically rescans some 5 minutes later.
						
						// 7/22/2016 rmd : If we have an SR on Port B invoke the delay. Except if this is a hub
						// because hubs do not use port B for flowsense traffic.
						if( (config_c.port_B_device_index == COMM_DEVICE_SR_FREEWAVE) && !CONFIG_this_controller_is_a_configured_hub() )
						{
							start_the_FlowSense_SR_radio_connection_timer = (true);
						}
						
						// 7/22/2016 rmd : If chain members doesn't reflect the chain members.
						if( nm_number_of_live_ones() == FLOWSENSE_get_num_controllers_in_chain() )
						{
							// 7/22/2016 rmd : Starting with the B controller see if there are ANY SR's in the chain
							// somewhere.
							for( iii=1; iii<MAX_CHAIN_LENGTH; iii++ )
							{
								if( chain.members[ iii ].saw_during_the_scan )
								{
									if( chain.members[ iii ].box_configuration.port_A_device_index == COMM_DEVICE_SR_FREEWAVE )
									{
										start_the_FlowSense_SR_radio_connection_timer = (true);
									}
									
									if( chain.members[ iii ].box_configuration.port_B_device_index == COMM_DEVICE_SR_FREEWAVE )
									{
										start_the_FlowSense_SR_radio_connection_timer = (true);
									}
								}
							}
						}
						else
						{
							// 7/22/2016 rmd : So chain members does not reflect how many are in the chain. Start the
							// connection timer cause we don't know what is out there.
							start_the_FlowSense_SR_radio_connection_timer = (true);
						}
					}
					
					// ----------

					if( start_the_FlowSense_SR_radio_connection_timer )
					{
						Alert_Message( "delaying startup scan by 30 seconds" );

						// 7/22/2016 rmd : The connection criteria is a blind 30 second delay. We hope the devices
						// have connected following the 30 seconds. We may be able to be smarter by watching one of
						// the lines out of the master but for now it is this way.
						xTimerStart( comm_mngr.flowsense_device_establish_connection_timer, portMAX_DELAY );
					}
					else
					{
						// 7/21/2016 rmd : No devices to worry about so cancel the BLOCKING power up delay.
						comm_mngr.flowsense_devices_are_connected = (true);
					}

					// ----------
					
					// 3/25/2014 rmd : And start the ISP exchange.
					TPMICRO_COMM_create_timer_and_start_messaging();
					
					break;

				// ----------

				case COMM_MNGR_EVENT_there_is_a_new_incoming_message_or_packet:
					process_incoming_messages_list();
					break;

				// ----------

				case COMM_MNGR_EVENT_token_rate_timer_expired:
					// 3/5/2014 rmd : This is part of the criteria to generate the next token. This timer must
					// be satisfied ie it timed out. If the token resp isn't back yet we'll wait for it.
					comm_mngr.token_rate_timer_has_timed_out = (true);

					post_attempt_to_send_next_token();
					break;

				case COMM_MNGR_EVENT_attempt_to_kick_out_the_next_token:
					attempt_to_kick_out_the_next_token();
					break;

				case COMM_MNGR_EVENT_last_OM_failed:
					transport_says_last_OM_failed( cmqs );
					break;

				case COMM_MNGR_EVENT_message_resp_timer_expired:
					process_message_resp_timer_expired();
					break;

				// ----------

				case COMM_MNGR_EVENT_request_to_start_a_scan:
					// 11/26/2014 rmd : Only masters or standalones should ever post this event.
					if( !FLOWSENSE_we_are_a_master_one_way_or_another() )
					{
						Alert_Message( "slave rqsted scan" );
					}

					comm_mngr.start_a_scan_captured_reason = cmqs.reason_for_scan;
					
					comm_mngr.start_a_scan_at_the_next_opportunity = (true);

					break;

				// ----------

				case COMM_MNGR_EVENT_tell_master_to_scan_and_report_we_are_NEW:
					// 4/29/2014 rmd : Only a slave should ever be trying to use these flags. Let's test for
					// that.
					if( FLOWSENSE_we_are_poafs() && (FLOWSENSE_get_controller_index() != 0) )
					{
						comm_mngr.request_a_scan_in_the_next_token_RESP = (true);
	
						comm_mngr.flag_myself_as_NEW_in_the_scan_RESP = (true);

						// 5/8/2014 rmd : This event is thrown when the flowsense chain parameters are changed. For
						// the case of a slave we do not want the network to be flagged as available until after the
						// scan and we have received the required number of tokens.
						comm_mngr.since_the_scan__slave__number_of_clean_tokens_rcvd = 0;

						comm_mngr.since_the_scan__slave__number_of_clean_token_responses_made = 0;
					}
					else
					{
						Alert_Message( "Why are we using these flags?");
					}
					break;

				// ----------
				
				// 4/17/2012 rmd : We used to have a separate task for the TP_MICRO communications
				// activites. But when it came to the STOP key processing I realized that we wanted to
				// sequentially process both the incoming TOKENS and generate the outgoing TP_MICRO
				// messages. This meant either using a MUTEX or task priorities to control this. Or if all
				// this was in the same task we could manage it that way. Without the clutter and possible
				// oversite of using a MUTEX.
				//
				// An example of why we want to sequentially handle messages would be an incoming TOKEN with
				// both a request to turn stations ON and to STOP the stations. Would like to fully parse
				// the TOKEN before making the TP_MICRO message. (It may be the FOAL side doesn't allow this
				// particular combination of ON and STOP - but I use it as an example.)

				case COMM_MNGR_EVENT_tpmicro_msg_rate_timer_expired:
				case COMM_MNGR_EVENT_isp_expected_response_arrived:
					TPMICRO_COMM_kick_out_the_next_tpmicro_msg( &cmqs );
					break;

				case COMM_MNGR_EVENT_tpmicro_here_is_an_incoming_packet_based_msg:
					TPMICRO_COMM_parse_incoming_message_from_tp_micro( cmqs.dh );
					break;

				// ----------
				
				case COMM_MNGR_EVENT_tpmicro_binary_file_read_for_isp_complete:
					// 6/14/2017 rmd : The code distribution task forwards this event to us when it is
					// recognized that the file read was for the tpmicro ISP purpose. The SCAN process cannot
					// commence until we have finished programming the tpmicro.
					if( tpmicro_comm.in_ISP )
					{
						TPMICRO_COMM_kick_out_the_next_isp_tpmicro_msg( &cmqs );
					}
					else
					{
						Alert_Message( "COMM MNGR unexpected isp file read event" );
					}
					break;

				// ----------

				case COMM_MNGR_EVENT_tpmicro_code_image_version_stamp:
					// 4/1/2014 rmd : Sanity check.
					if( !tpmicro_comm.code_version_test_pending && !tpmicro_comm.in_ISP )
					{
						Alert_Message( "Unexp TPMICRO file version event" );
					}

					// 1/27/2015 rmd : Okay so the file system code date and time is here. Capture it. And set
					// the valid flag so we know we can use them. Just take it at face value. No range checking.
					// We used to test for the date to be non-zero. But when we fashioned the structure and file
					// update scheme the tpmicro file date actually went to 0.
					tpmicro_comm.file_system_code_date = cmqs.code_date;

					tpmicro_comm.file_system_code_time = cmqs.code_time;

					tpmicro_comm.file_system_code_date_and_time_valid = (true);

					break;

				case COMM_MNGR_EVENT_request_read_device_settings:
				case COMM_MNGR_EVENT_request_read_monitor_device_settings:
				case COMM_MNGR_EVENT_request_write_device_settings:
					// 5/14/2015 rmd : To coordinate with the CI task take this MUTEX. Which blocks any new
					// messages from being started or made and added to the list till we are done here and
					// release it.
					xSemaphoreTakeRecursive( ci_and_comm_mngr_activity_recursive_MUTEX, portMAX_DELAY );

					if( (cmqs.port == UPORT_A) || (cmqs.port == UPORT_B))
					{
						if( cmqs.port == UPORT_A )
						{
							comm_mngr.device_exchange_device_index = config_c.port_A_device_index;
						}
						else
						{
							comm_mngr.device_exchange_device_index = config_c.port_B_device_index;
						}
					}
					else
					{
						// 3/19/2014 rmd : Setting the device index to 0 will ensure the function pointers test that
						// follows will fail. The 0 index contains NULL funtions.
						comm_mngr.device_exchange_device_index = 0;
					}
						
					// 3/14/2014 rmd : Test that we don't already have a pending device exchange request.
					// 
					// 4/29/2015 ajv : We've removed the _is_connected condition now that our central
					// communication devices maintain a persistant connection. The _is_idle_with_no_work_pending
					// routine will protect us from trying to enter device exchange mode while we're actively
					// communicating.
					if( (port_device_table[ comm_mngr.device_exchange_device_index ].__initialize_device_exchange != NULL) &&
					    (port_device_table[ comm_mngr.device_exchange_device_index ].__device_exchange_processing != NULL) )
					{
						comm_mngr.device_exchange_port = cmqs.port;

						comm_mngr.device_exchange_initial_event = cmqs.event;
						
						comm_mngr.pending_device_exchange_request = (true);
					}
					else
					{
						// 3/13/2014 rmd : With a queue message respond to the key processing task the request has
						// been denied. Because the comm mngr is busy at the moment. Or that the functions are not
						// in place. Or that the device is busy. Try again later.

						COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_busy_try_again_later );
					}
					
					xSemaphoreGiveRecursive( ci_and_comm_mngr_activity_recursive_MUTEX );

					break;

				case COMM_MNGR_EVENT_device_exchange_response_arrived:
				case COMM_MNGR_EVENT_device_exchange_response_timer_expired:
				
					if( port_device_table[ comm_mngr.device_exchange_device_index ].__device_exchange_processing != NULL )
					{
						port_device_table[ comm_mngr.device_exchange_device_index ].__device_exchange_processing( &cmqs );
					}
					else
					{
						// 3/19/2014 rmd : Well gee. This should NEVER HAPPEN. Make alert. Signal keyprocess task.
						// And restore comm mngr to original mode.
						Alert_Message( "No device exchange process function" );
						
						if( ( comm_mngr.device_exchange_initial_event == COMM_MNGR_EVENT_request_read_device_settings ) ||
							( comm_mngr.device_exchange_initial_event == COMM_MNGR_EVENT_request_read_monitor_device_settings ) )
						{
							COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_read_settings_error );
						}
						else
						{
							COMM_MNGR_device_exchange_results_to_key_process_task( DEVICE_EXCHANGE_KEY_write_settings_error );
						}
						
						// 3/19/2014 rmd : Restore.
						comm_mngr.mode = comm_mngr.device_exchange_saved_comm_mngr_mode;

						// 10/21/2014 rmd : And restore packet hunting. What the rest of the application expects!
						RCVD_DATA_enable_hunting_mode( comm_mngr.device_exchange_port, PORT_HUNT_FOR_PACKETS, NOT_STRING_HUNTING  );
					}

					break;
					
			}  // of event processing

		}  // of a sucessful queue item is available

		// ----------

		// 10/23/2012 rmd : Keep the activity indication going.
		task_last_execution_stamp[ task_index ] = xTaskGetTickCount();

		// ----------

	}  // of forever task loop

}

/* ---------------------------------------------------------- */
extern void COMM_MNGR_device_exchange_results_to_key_process_task( UNS_32 pdevice_exchange_key_results )
{
	KEY_TO_PROCESS_QUEUE_STRUCT		ktpqs;
					
	ktpqs.keycode = pdevice_exchange_key_results;

	ktpqs.repeats = 0;

	xQueueSendToBack( Key_To_Process_Queue, &ktpqs, portMAX_DELAY );
}

/* ---------------------------------------------------------- */
extern void COMM_MNGR_post_event_with_details( COMM_MNGR_TASK_QUEUE_STRUCT *pq_item_ptr )
{
	// 6/24/2015 rmd : Can be executed within the context of a wide variety of tasks.

	//  WARNING: DO NOT CALL THIS FUNCTION FROM AN INTERRUPT because it is not using the fromISR
	//  xQueue function.

	if( xQueueSendToBack( COMM_MNGR_task_queue, pq_item_ptr, 0 ) != pdPASS )	 // do not wait for queue space ... flag an error
	{
		Alert_Message( "CommMngr queue overflow!" );
	}
}

/* ---------------------------------------------------------- */
extern void COMM_MNGR_post_event( UNS_32 pevent )
{
	// 6/24/2015 rmd : Can be executed within the context of a wide variety of tasks.

	//  WARNING: DO NOT CALL THIS FUNCTION FROM AN INTERRUPT because it is not using the fromISR
	//  xQueue function.

	COMM_MNGR_TASK_QUEUE_STRUCT	cmeqs;
	
	cmeqs.event = pevent;
	
	COMM_MNGR_post_event_with_details( &cmeqs );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/30/2017 rmd : The whole device exchange implementation needs to be re-done. It is not
// well integrated with the serial drivers, with the controller initiated task, with the
// code distribution task, with the rcvd data task (packet router), and with the comm_mngr
// task. But for now we can limp along until I restructure the interaction. And this
// function helps us control the other tasks.

extern void task_control_ENTER_device_exchange_hammer( void )
{
	// 6/30/2017 rmd : Set the global BRUTE FORCE variable we can reference to quench any
	// attempts to send data to the serial port. Only should allow device exchange data.
	in_device_exchange_hammer = (true);
	
	// ----------
	
	// 6/30/2017 rmd : For CONTROLLER INITIATED do these things.

	// 3/27/2017 rmd : If we are in the middle of a message take the brute force steps needed to
	// exit that mode.
	CONTROLLER_INITIATED_force_end_of_transaction();

	// 3/27/2017 rmd : Set the mode that blocks any further CI task activity. If in the middle
	// of the CONNECTION process that will end because of the mode change.
	cics.mode = CI_MODE_in_device_exchange;
	
	// ----------
	
	CODE_DISTRIBUTION_stop_and_cleanup();
	
	// ----------
	
	// 6/30/2017 rmd : The COMM_MNGR goes through an elaborate entrance to device programming.
	// Sort of independantly from when the user enters it with the keyboard. This has to be
	// re-worked someday.
}

extern void task_control_EXIT_device_exchange_hammer( void )
{
	// 6/30/2017 rmd : If we are leaving a device exchange screen (leaving the device exchange
	// mode) need to do some task maintenance.
	if( in_device_exchange_hammer )
	{
		// 3/27/2017 rmd : If, and only if, the controller initiated task mode has been set to
		// indicate it is in the device exchange mode (indicating we just executed a device
		// exchange) should we exit that mode and start the connection process.
		if( cics.mode == CI_MODE_in_device_exchange )
		{
			cics.mode = CI_MODE_waiting_to_connect;
			
			CONTROLLER_INITIATED_post_event( CI_EVENT_start_the_connection_process );
		}
		
		// ----------
		
		// 6/30/2017 rmd : The comm_mngr kind of does some things on its own.
		if( comm_mngr.mode == COMM_MNGR_MODE_device_exchange )
		{
			comm_mngr.mode = comm_mngr.device_exchange_saved_comm_mngr_mode;
		}
		
		// ----------
		
		// 6/30/2017 rmd : There is an attempt to cover this in all the individual device
		// programmers, but place one global one here. Restore packet hunting. What the rest of the
		// application expects!
		RCVD_DATA_enable_hunting_mode( comm_mngr.device_exchange_port, PORT_HUNT_FOR_PACKETS, NOT_STRING_HUNTING  );
	}

	// ----------
	
	in_device_exchange_hammer = (false);
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

