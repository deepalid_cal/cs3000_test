/* file = main.c                             01.25.2011  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/*
Pasting Tokens
Each argument passed to a macro is a token, and sometimes it might be expedient to paste arguments together to form a new token.
This could come in handy if you have a complicated structure and you'd like to debug your program by printing out different fields. Instead of
writing out the whole structure each time, you might use a macro to pass in the field of the structure to print.

To paste tokens in a macro, use ## between the two things to paste together.

For instance #define BUILD_FIELD(field) my_struct.inner_struct.union_a.##field

Now, when used with a particular field name, it will expand to something like my_struct.inner_struct.union_a.field1

The tokens are literally pasted together.

// ----------

// 7/30/2013 rmd : Here is a cryptic example of pasting. This thing works but I don't like
// using it. So very cryptic! Shown here for reference. A useful macro taking advantage of
// PASTING tokens within.
#define READ_AND_EVALUATE_CTS( port ) SerDrvrVars_s[ UPORT_##port ].modem_control_line_status.i_cts = config_c.port_settings_##port.cts_when_OK_to_send ? PORT_##port##_CTS_is_HIGH : PORT_##port##_CTS_is_LOW

use:
READ_AND_EVALUATE_CTS( A );

// ----------

String-izing Tokens
Another potentially useful macro option is to turn a token into a string containing the literal text of the token. This might be useful
for printing out the token. The syntax is simple--simply prefix the token with a pound sign (#). #define PRINT_TOKEN(token) printf(#token " is %d", token)

For instance, PRINT_TOKEN(foo) would expand to printf("<foo>" " is %d" <foo>)

(Note that in C, string literals next to each other are concatenated, so something like "token" " is " " this " will effectively become
"token is this". This can be useful for formatting printf statements.)

For instance, you might use it to print the value of an expression as well as the expression itself (for debugging purposes).
*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/*
NEED TO FIX ON NEXT REVISION

	NEGATIVE HOLD OVER BEHAVIOR
	Choice on what -ve HO affects. IE Manual Programs?



KNOWN ISSUES AND FUTURE IDEAS
 
  FLOWSENSE TRAFFIC (FogBugz Case 1438)
    Bill discovered that the FLOWSENSE scan packet is not restricted to only -M
    and -SR communication options. He noticed this in Palm Desert where the LR
    transmit light would light up 11 times during the scan process. This doesn't
    appear to be causing any problems in the field, but it could theoretically
    if a scan is performed while LR communications are going.
 
  RADIO REMOTE COMMUNICATION (FogBugz Case 1439)
    There is a discrepancy when a station is paused from the Radio Remote and
    never turned off. When the system times out, the Radio Remote issues a
    command to turn off all the stations that were on or paused via the hand-
    held. Now that the pause alert is suppressed, there's no notification that
    the station was paused and never resumed. This could cause someone to
    assume the station was running until the system timed out. A fix would be
    to analyze which stations were on and which were paused and create unique
    alerts for each when the timeout occurs.
 
  LEARNING EXPECTEDS (FogBugz Case 1440)
    Currently, there is no way to use the Test key to learn more than one
    station at a time. This means that the user must either learn each station
    on-by-one or learn them through the course of normal irrigation. We may want
    to consider adding the ability to learn by program or for all programs using
    the test key. This would give the end-user the opportunity to learn when it
    was convenient for them.
 
*/
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*
NOTES ON CROSSWORKS

* When compiling you may notice an error message regarding an include directory that is not found.
  Go to the top level of the solution and look at the solution 'common' properties. Look for
  'SYSTEM INLCUDE DIRECTORIES". You will see two entries in there. Hopefully the offending
  one which I have always found to be the <package directory> entry. Remove it.

* 6/15/2012 rmd : To compile in verbose mode. Meaning to see the full gnu c compiler command
  line right click in the 'output' window where the compiling process messages are
  displayed. There is an option there to 'echo build command lines'.

* 6/15/2012 rmd : Regarding ARM versus THUMB. To compile thumb code THUMB_INTERWORK should 
  be defined for freeRTOS. This is used with the enable and disable interrupts macros
  (functions). Though actually I saw thumb code run with or without this define. But seems
  to be the safe thing to do since it is documented to define it when building thumb code. I
  did some quick code comparisons between THUMB and ARM code:

					    code size			test code execution speed
  ARM debug				650K				3.28ms
 
  ARM release			409K				1.72ms

  THUMB debug			440K				7.04ms
  
  THUMB release			280K				3.82ms
  
  After taking this data I decided we would stick with the ARM code. Roughly in this example
  it costs 120K of code to improve execution speed by better than half. I'm not exactly sure
  why the thumb code performs so poorly. But I think this is a complicated matter. Involving
  the on-chip caches, the external SDRAM bus width, and perhaps other factors.
*/ 


/*
NOTES ON UPGRADING freeRTOS

* We have modified TASKS.C at lines 1350 and 2360. This was to format the OS Stats reporting
  to a liking of ours.
  
* We have modified the xSemaphoreTakeRecursive function to a debug version that always sets 
  the block time to 2000ms. And if the TAKE fails makes an alert line. We did this so we
  could have an indication of any MUTEX deadlock conditions that form. This modification
  affects 3 files. "semphr.h' line 300, "queue.c" line 560, and "queue.h" line 1250.
  
* We have modified the xTimerGenericCommand function code in TIMERS.C line 265 and 305. To 
  include protection against a timer call with a NULL timer. Doing such will crash the OS
  and there was no protection against this.
*/ 

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*
V A R I A B L E   D E C L A R A T I O N   G U I D E L I N E S
*/
/*
typedef struct {

	unsigned long	s_long;

	unsigned char	test_str[ 64 ];

} TEST_STRUCT;  // demo structure for use below


// .sdram_init_vars
// Initialized variables in off-board SDRAM: RULES ARE
// 1. Wether you explicitly intialize or not, all variables in this section take up FLASH space [code space]. So if this is a large variable
//    it may be better off in the .sdram_bss_vars section which does NOT consume code space. You could initialize such with your own code.
// 2. If you do not explicitly initialize a variable in this section the linker will do it for you to 0's. There are no known problems
//    of not being explicit EXCEPT you might as well place it in the .sdram_bss_vars section as you are just wasting code space.
// 3. Do not place large arrays or other structures here due to the code space implications.
//
TEST_STRUCT test_struct_init __attribute__((section(".sdram_init_vars"))) = { 

	.s_long = 0x12345678,
	.test_str = "This is my test string!!!!!!...hope it works." 

};

// .sdram_bss_vars
// Variables in off-board SDRAM which are zeroed on program startup: RULES ARE
// 1. These declarations do NOT consume any code space as opposed to the .sdram_init_vars ones.
// 2. You can attempt to explicitly initialize one of these variables and no harm will come of it. There will be no compiler warnings.
//    But your variable will be all zeros on program startup.
// 3. Suitable for very large arrays or sturctures.
//
unsigned char test_array_uc[ 0x1000 ] __attribute__((section(".sdram_bss_vars")));

// .battery_backed_vars
// Variables stored in off-board battery backed SRAM: RULES ARE
// 1. These declarations do NOT consume any code space.
// 2. These variables are not initialized in any way on program startup. Not set to zero. Not touched at all.
// 3. Only variables that MUST be here should be in this section. Ones that must persist throughout a power fail.
// 4. More rules to come as needed.
//
TEST_STRUCT test_struct_battery __attribute__((section(".battery_backed_vars")));


// --------------------------------------------
// --------------------------------------------

INFORMATION ON SOME OTHER CLASSIC SECTION NAMES
You will see these named in the linker output map but it's not obvious what they are.
This is a partial guide to the major section names you'll seems.

SECTION			LOCATION		COMMENT
.text			FLASH			code
.rodata			FLASH			initialized constants
.data			INTRAM			initialized variables (by setting them equal to something in their declaration)
.bss			INTRAM			initialized to 0 by compiler on startup (stands for BLOCK STARTING WITH SYMBOL)

*/
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/*

static void HardFault_Handler(void)
{
	__asm volatile
	(
		" tst lr, #4										\n"
		" ite eq											\n"
		" mrseq r0, msp										\n"
		" mrsne r0, psp										\n"
		" ldr r1, [r0, #24]									\n"
		" ldr r2, handler2_address_const						\n"
		" bx r2												\n"
		" handler2_address_const: .word pop_registers_from_fault_stack	\n"
	);
}



This code just sets up everything ready, then jumps to a function called pop_registers_from_fault_stack - which is defined below:

void pop_registers_from_fault_stack(unsigned int * hardfault_args)
{
unsigned int stacked_r0;
unsigned int stacked_r1;
unsigned int stacked_r2;
unsigned int stacked_r3;
unsigned int stacked_r12;
unsigned int stacked_lr;
unsigned int stacked_pc;
unsigned int stacked_psr;

	stacked_r0 = ((unsigned long) hardfault_args[0]);
	stacked_r1 = ((unsigned long) hardfault_args[1]);
	stacked_r2 = ((unsigned long) hardfault_args[2]);
	stacked_r3 = ((unsigned long) hardfault_args[3]);

	stacked_r12 = ((unsigned long) hardfault_args[4]);
	stacked_lr = ((unsigned long) hardfault_args[5]);
	stacked_pc = ((unsigned long) hardfault_args[6]);
	stacked_psr = ((unsigned long) hardfault_args[7]);

    // Inspect stacked_pc to locate the offending instruction.
	for( ;; );
}

*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcat
#include	<string.h>

#include	"cs_common.h"

#include	"app_startup.h"

#include	"battery_backed_vars.h"

#include	"key_scanner.h"

#include	"lpc32xx_chip.h"

#include	"lpc32xx_clkpwr_driver.h"

#include	"lpc32xx_gpio.h"

#include	"lpc3200_intc.h"

#include	"lpc3250_chip.h"

#include	"lpc_arm922t_cp15_driver.h"

#include	"gpio_setup.h"

#include	"epson_rx_8025sa.h"

#include	"s1l_memtests.h"

#include	"df_storage_mngr.h"

#include	"spi_flash_driver.h"

#include	"wdt_and_powerfail.h"

#include	"flash_storage.h"

#include	"serial.h"

#include	"lpc32xx_timer.h"

#include	"alerts.h"

#include	"lpc32xx_clcdc.h"

#include	"lcd_init.h"

#include	"r_memory_use.h"

#include	"guilib.h"

#include	"serport_drvr.h"

#include	"rcvd_data.h"

#include	"d_process.h"

#include	"comm_mngr.h"

#include	"cs_mem_part.h"

#include	"cs_mem.h"

#include	"tpmicro_data.h"

// 8/19/2015 mpd : Temporary to obtain "sizeof()" for lights structures. 
//#include	"foal_lights.h"
//#include	"irri_lights.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*
static void disable_dcache_caution( void )
{
	// Be VERY careful how you use this and the accompanying enable_dcache function. So far they only exist to be used during memory tests
	// so we are sure we are actually reading and writing to the memory under test. The safest way I've found to use them is to first suspend
	// ALL other tasks (vTaskSuspendAll) then diasble the dcache (disable_dcache). The do your memory tests or whatever. Then enable the dcache
	// (enable_dcache) the resume the OS (xTaskResumeAll)

	void dcache_flush( void );  // defined in LPC3200_Startup.s
	dcache_flush();

	cp15_set_dcache( 0 );
}
*/ 
 
/*-----------------------------------------------------------*/
/*
static void enable_dcache_caution( void )
{
	cp15_invalidate_cache();

	cp15_set_dcache( 1 );
}
*/
/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/


/* 
volatile UNS_16 sram_test_array[ 64*1024 ] __attribute__(BATTERY_BACKED_VARS);

volatile UNS_16 sdram_test_array[ 64*1024 ];

volatile UNS_16 iram_test_array[ 64*1024 ] __attribute__(APPLICATION_IRAM_VARS);
*/


/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/

//#define DF_ARRAY_LENGTH_32	(DF_PAGE_SIZE_IN_BYTES/sizeof(UNS_32))

/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
static UNS_32	lcounter;

static void toggle_LED( UNS_32 pled )
{
	if( pled == 1 )
	{
		if( P0_OUTP_STATE & CS_LED_YELLOW )
		{
			CS_LED_YELLOW_OFF;	
		}
		else
		{
			CS_LED_YELLOW_ON;
		}
		
		/*
		#define TEST_SIZE	(100)
		DATA_HANDLE ldh;
		ldh.dptr = mem_malloc( TEST_SIZE );
		if( ldh.dptr != NULL )
		{
			ldh.dlen = TEST_SIZE;
			UNS_32 i; 
			for( i=0; i<TEST_SIZE; i++ ) 
			{ 
				*((ldh.dptr) + i) = i;
			}

			if( SerDrvrVars_s[ UPORT_TP ].SerportTaskState != UPORT_STATE_DISABLED )
			{
				//while( xmit_cntrl[ UPORT_TP ].xlist.count < 10 )
				{
					AddCopyOfBlockToXmitList( UPORT_TP, ldh );
				}
			}
			
			mem_free( ldh.dptr );
		} 
		*/
		
		lcounter += 1;

		//Alert_Message_va( "%u   LED 1 %s %u", lcounter, __FILE__, __LINE__ );
	}
	else
	if( pled == 2 )
	{
		if( P0_OUTP_STATE & CS_LED_RED )
		{
			CS_LED_RED_OFF;	
		}
		else
		{
			CS_LED_RED_ON;
		}

		//Alert_Message_va( "LED 2 %s %d", __FILE__, __LINE__ );
		//Alert_Message_va( "LED 2 %s %d", __FILE__, __LINE__ );
		//Alert_Message_va( "LED 2 %s %d", __FILE__, __LINE__ );
		//Alert_Message_va( "LED 2 %s %d", __FILE__, __LINE__ );
	}
}

/*
static void my_timer_callback( xTimerHandle pxTimer )
{
	toggle_LED( (UNS_32)(pvTimerGetTimerID( pxTimer )) );
}
*/

extern void vled_TASK( void *pvParameters )
{
	UNS_32			task_param;
	
	task_param = (UNS_32)pvParameters;



	/*
	xTimerHandle	ltimer;

	ltimer = xTimerCreate( (const signed char*)"Timer", MS_to_TICKS( (task_param * 1000) ), TRUE, (void*)task_param, my_timer_callback );
			
	if( ltimer == NULL )
	{
		Alert_Message_va( "Timer %d NOT CREATED!", task_param );
	}
	else
	if( xTimerStart( ltimer, 0 ) != pdPASS )
	{
		Alert_Message_va( "Timer %d NOT STARTED!", task_param );
	}
	*/
	
	/*
	if( task_param == 1 )
	{
		//#define TEST_SIZE	(64*6)
		#define TEST_SIZE	(100)
		DATA_HANDLE ldh;
		ldh.dptr = mem_malloc( TEST_SIZE );
		if( ldh.dptr != NULL )
		{
			ldh.dlen = TEST_SIZE;
			UNS_32 i; 
			for( i=0; i<TEST_SIZE; i++ ) 
			{ 
				*((ldh.dptr) + i) = (UNS_8)i;
			}
	
			#if 0
			if( SerDrvrVars_s[ UPORT_TP ].SerportTaskState != UPORT_STATE_DISABLED )
			{
				{
					AddCopyOfBlockToXmitList( UPORT_TP, ldh );
				}
			}
			mem_free( ldh.dptr ); 
			#endif
			
			__test_packet_echo( UPORT_A, ldh );
		} 
	}
	*/
	
 

	volatile float		sf1, sf2;
	
	volatile double		df1, df2;
	

	#define NUMBER_OF_MALLOC_BLOCKS		16

	#define TEST_SIZE_1					256
	#define TEST_SIZE_2					320
	#define TEST_SIZE_3					384
	#define TEST_SIZE_4					512

	DATA_HANDLE		dh1[ NUMBER_OF_MALLOC_BLOCKS ];
	DATA_HANDLE		dh2[ NUMBER_OF_MALLOC_BLOCKS ];
	DATA_HANDLE		dh3[ NUMBER_OF_MALLOC_BLOCKS ];
	DATA_HANDLE		dh4[ NUMBER_OF_MALLOC_BLOCKS ];

	DATA_HANDLE		*dh_ptr;

	UNS_32			i, j, k, *uns32_ptr, *tmp32_ptr, *dptr, test_size, list_length, list_element_size;
	

	#define NUMBER_OF_LISTS				4
	
	#define LIST_LENGTH_1				24
	#define LIST_ELEMENT_SIZE_1			256

	#define LIST_LENGTH_2				16
	#define LIST_ELEMENT_SIZE_2			320

	#define LIST_LENGTH_3				32
	#define LIST_ELEMENT_SIZE_3			896
	
	#define LIST_LENGTH_4				64
	#define LIST_ELEMENT_SIZE_4			1104
	
	MIST_LIST_HDR_TYPE		tlist_hdr[ NUMBER_OF_LISTS ];
	
	for( ;; )
	{
		//vTaskDelay( MS_to_TICKS( 10 ) );

		toggle_LED( 2 );

		// Settings for the default. Use this construct to avoid uninitialized variable warning below. So these will be assumed for
		// task_param == 1. As opposed to explicitly specifying.
		dh_ptr = &dh1[ 0 ];

		test_size = TEST_SIZE_1;

		list_length = LIST_LENGTH_1;

		list_element_size = LIST_ELEMENT_SIZE_1;


		if (task_param == 2)
		{
			dh_ptr = &dh2[ 0 ];

			test_size = TEST_SIZE_2;

			list_length = LIST_LENGTH_2;

			list_element_size = LIST_ELEMENT_SIZE_2;
		}
		else
		if (task_param == 3)
		{
			dh_ptr = &dh3[ 0 ];

			test_size = TEST_SIZE_3;

			list_length = LIST_LENGTH_3;

			list_element_size = LIST_ELEMENT_SIZE_3;
		}
		else
		if (task_param == 4)
		{
			dh_ptr = &dh4[ 0 ];

			test_size = TEST_SIZE_4;

			list_length = LIST_LENGTH_4;

			list_element_size = LIST_ELEMENT_SIZE_4;
		}
		
		// --------------------------------------------------------------
		// LIST TESTS
		// --------------------------------------------------------------

		for( i=0; i<NUMBER_OF_LISTS; i++ )
		{
			nm_ListInit( &tlist_hdr[ i ], 0 );

			for( j=0; j<list_length; j++ )
			{
				uns32_ptr = mem_malloc( list_element_size );

				dptr = uns32_ptr + (sizeof(MIST_DLINK_TYPE)/sizeof(UNS_32));

				for( k=0; k<((list_element_size-sizeof(MIST_DLINK_TYPE))/sizeof(UNS_32)); k++ )
				{
					// Write the address of the location modified by the task param.
					*dptr = (((UNS_32)dptr) - task_param);
					
					dptr += 1;
				}
				
				nm_ListInsertTail( &tlist_hdr[ i ], uns32_ptr );
			}
		}

		for( i=0; i<NUMBER_OF_LISTS; i++ )
		{
			uns32_ptr = nm_ListGetFirst( &tlist_hdr[ i ] );
			
			j = 0;

			while( uns32_ptr != NULL )
			{
				j += 1;
				
				dptr = uns32_ptr + (sizeof(MIST_DLINK_TYPE)/sizeof(UNS_32));
	
				for( k=0; k<((list_element_size-sizeof(MIST_DLINK_TYPE))/sizeof(UNS_32)); k++ )
				{
					// Read the address of the location modified by the task param.
					if( *dptr != (((UNS_32)dptr) - task_param) )
					{
						for(;;);
					}
						
					dptr += 1;
				}
				
				tmp32_ptr = uns32_ptr;
				
				uns32_ptr = nm_ListGetNext( &tlist_hdr[ i ], uns32_ptr );
				
				nm_ListRemove( &tlist_hdr[ i ], tmp32_ptr );
				
				mem_free( tmp32_ptr );
			}
			
			if( j != list_length )
			{
				for(;;);
			}
		}


		// --------------------------------------------------------------
		// MEM_MALLOC TESTS
		// --------------------------------------------------------------

		for( i=0; i<NUMBER_OF_MALLOC_BLOCKS; i++ )
		{
			dh_ptr->dptr = mem_malloc( test_size );
			
			uns32_ptr = (UNS_32*)dh_ptr->dptr;
			
			for( j=0; j<(test_size/sizeof(UNS_32)); j++ )
			{
				// Write the address of the location modified by the task param.
				*uns32_ptr = (((UNS_32)uns32_ptr) + task_param);
				
				uns32_ptr += 1;
			}

			dh_ptr += 1;
		}


		if( task_param == 1 )
		{
			dh_ptr = &dh1[ 0 ];
		}
		else
		if (task_param == 2)
		{
			dh_ptr = &dh2[ 0 ];
		}
		else
		if (task_param == 3)
		{
			dh_ptr = &dh3[ 0 ];
		}
		else
		if (task_param == 4)
		{
			dh_ptr = &dh4[ 0 ];
		}
		
		for( i=0; i<NUMBER_OF_MALLOC_BLOCKS; i++ )
		{
			uns32_ptr = (UNS_32*)dh_ptr->dptr;

			for( j=0; j<(test_size/sizeof(UNS_32)); j++ )
			{
				// Read the address of the location modified by the task param.
				if( *uns32_ptr != (((UNS_32)uns32_ptr) + task_param) )
				{
					for(;;);
				}

				uns32_ptr += 1;
			}

			mem_free( dh_ptr->dptr );

			dh_ptr += 1;
		}



		// --------------------------------------------------------------
		// FLOATING POINT TESTS
		// --------------------------------------------------------------

		if( task_param == 1 ) 
		{ 
			toggle_LED( 1 );
			
			sf1 = (float)1.0 + ((float)22.0/(float)3.0);
			
			sf2 = (float)1.0 + (float)(0.75);
			
			sf2 = ((sf1 * sf2)/sf2);
			
	
			df1 = (double)1.0 + ((double)22.0/(double)3.0);
	
			df2 = (double)1.0 + (double)(0.75);
	
			df2 = ((df1 * df2)/df2);
			
			if( sf2 != sf1 )
			{
				for(;;);
			}
			
			if( df2 != df1 )
			{
				for(;;);
			}

			toggle_LED( 1 );
		}
		else
		if (task_param == 2)
		{
			sf1 = (float)2.0 + ((float)22.0/(float)3.0);

			sf2 = (float)2.0 + (float)(0.75);

			sf2 = ((sf1 * sf2)/sf2);


			df1 = (double)2.0 + ((double)22.0/(double)3.0);

			df2 = (double)2.0 + (double)(0.75);

			df2 = ((df1 * df2)/df2);

			if( sf2 != sf1 )
			{
				for(;;);
			}

			if( df2 != df1 )
			{
				for(;;);
			}
		}
	}
}




/* 
  void vled_TASK_1( void *pvParameters )
{
	vTaskDelay( 2000 );  // wait for tasks to settle (including serial driver)

	file_system_test( 0 );
	
	for( ;; )
	{
		vTaskDelay( 2000 );
	}
*/

	/* 
	volatile UNS_32 df_test_write_array_0[ DF_ARRAY_LENGTH_32 ];
	volatile UNS_32 df_test_read_array_0[ DF_ARRAY_LENGTH_32 ];
	
	vTaskDelay( 2000 );  // wait for tasks to settle (including serial driver)


	UNS_32	value_32, iii;
	
	value_32 = 0x00000000;
	
	for( ;; )
	{
		for( iii=0; iii<DF_ARRAY_LENGTH_32; iii++ )
		{
			df_test_write_array_0[ iii ] = value_32++;
		}

		CS_LED_RED_ON;

		DF_write_page( 0, DF_MASTER_RECORD_PAGE, (void*)&df_test_write_array_0, DF_PAGE_SIZE_IN_BYTES );

		DF_read_page( 0, DF_MASTER_RECORD_PAGE, (void*)&df_test_read_array_0, DF_PAGE_SIZE_IN_BYTES );

		CS_LED_RED_OFF;
		
		for( iii=0; iii<DF_ARRAY_LENGTH_32; iii++ )
		{
			if( df_test_read_array_0[ iii ] != df_test_write_array_0[ iii ] )
			{
				if( !restart_info.POWER_FAIL_impending ) Alert_Message( "Flash 0 failure!" );
			}
		} 

		//vTaskDelay( 200 );
	} 
	*/
	 
//}

/* 
  void vled_TASK_2( void *pvParameters )
{
	vTaskDelay( 2000 );  // wait for tasks to settle (including serial driver)

	file_system_test( 1 );

	for( ;; )
	{
		vTaskDelay( 200 );
	}

    */

	/* 
	volatile UNS_32 df_test_write_array_1[ DF_ARRAY_LENGTH_32 ];
	volatile UNS_32 df_test_read_array_1[ DF_ARRAY_LENGTH_32 ];

	vTaskDelay( 2000 );  // wait for tasks to settle (including serial driver)


	UNS_32	value_32, iii;

	value_32 = 0xFFFFFFFF;

	for( ;; )
	{
		for( iii=0; iii<DF_ARRAY_LENGTH_32; iii++ )
		{
			df_test_write_array_1[ iii ] = value_32--;
		}

		CS_LED_YELLOW_ON;

		DF_write_page( 1, DF_MASTER_RECORD_PAGE, (void*)&df_test_write_array_1, DF_PAGE_SIZE_IN_BYTES );

		DF_read_page( 1, DF_MASTER_RECORD_PAGE, (void*)&df_test_read_array_1, DF_PAGE_SIZE_IN_BYTES );

		CS_LED_YELLOW_OFF;

		for( iii=0; iii<DF_ARRAY_LENGTH_32; iii++ )
		{
			if( df_test_read_array_1[ iii ] != df_test_write_array_1[ iii ] )
			{
				if( !restart_info.POWER_FAIL_impending ) Alert_Message( "Flash 1 failure!" );
			}
		} 

		//vTaskDelay( 200 );
	}
	*/
	

	 
	 
	 
	//UNS_32 iters, hexaddr, bytes, width, tstnum;
	//UNS_32 failure_count;


	/*
	UNS_32 hexaddr, bytes, width, tstnum, iters;
	hexaddr = EMC_CS1_BASE;
	bytes = 512 * 1024;
	width = 1;
	tstnum = MTST_ALL;
	iters = 2;
	// memtst [hex address][bytes to test][1, 2, or 4 bytes][0(all) - 5][iterations]
	memory_test( hexaddr, bytes, width, tstnum, iters);
	width = 2;
	memory_test( hexaddr, bytes, width, tstnum, iters);
	width = 4;
	memory_test( hexaddr, bytes, width, tstnum, iters);
	*/

	/*
	for( ;; )
	{
		//vTaskDelay( 100 );

		//memory_test( EMC_CS1_BASE + (1024*100), (64 * 1024), 2, MTST_WALKING1, 1 );
		//memory_test( EMC_DYCS0_BASE + (4*1024*1024), (64 * 1024), 2, MTST_WALKING1, 1 );

		//disable_dcache_caution();

		//UNS_32 jjj;
		//for( jjj=0; jjj<20; jjj++ )
		//{
			UNS_32 iii;
			CS_LED_GREEN_ON;
			//memmove( (void*)&sram_test_array[ 0 ], (const void*)&sram_test_array[(32*1024)], (64*1024) );
			for( iii=0; iii<(32*1024); iii++ )
			{
				sram_test_array[ iii ] = sram_test_array[ iii + (32*1024) ];
			}
			CS_LED_GREEN_OFF;
			
			CS_LED_RED_ON;
			//memmove( (void*)&sdram_test_array[ 0 ], (const void*)&sdram_test_array[(32*1024)], (64*1024) );
			for( iii=0; iii<(32*1024); iii++ )
			{
				sdram_test_array[ iii ] = sdram_test_array[ iii + (32*1024) ];
			}
			CS_LED_RED_OFF;
	
			CS_LED_YELLOW_ON;
			//memmove( (void*)&iram_test_array[ 0 ], (const void*)&iram_test_array[(32*1024)], (64*1024) );
			for( iii=0; iii<(32*1024); iii++ )
			{
				iram_test_array[ iii ] = iram_test_array[ iii + (32*1024) ];
			}
			CS_LED_YELLOW_OFF;
		//}

		//enable_dcache_caution();

	}
	*/



	/*
	hexaddr = 0x80000000;  // This is EMC SDRAM.
	//hexaddr = 0x08034000;  // this is IRAM - be careful to step around this program or other used spots - can't test much
	bytes = 0x800000;
	width = 4;
	tstnum = MTST_ALL;
	iters = 2;
	// memtst [hex address][bytes to test][1, 2, or 4 bytes][0(all) - 5][iterations]
	memory_test( hexaddr, bytes, width, tstnum, iters);
	*/
	


	
	//vTaskDelay( 2000 );

	// memtst [hex address][bytes to test][1, 2, or 4 bytes][0(all) - 5][iterations]
	//hexaddr = 0x80000000;  // This is EMC SDRAM.
	//bytes = 0x800000;
	//bytes = 0x8000;
	//width = 1;
	//tstnum = MTST_ALL;
	//iters = 1;

	/*
	for(;;)
	{
		term_dat_out( "MEM TEST w/ data cache ENABLED\n\r");
        CS_LED_RED_ON;

		memset( (void*)hexaddr, 0xAA55AA55, bytes );  // just to clear it out prior to the test
		//memory_test( hexaddr, bytes, width, tstnum, iters);

		CS_LED_RED_OFF;




		term_dat_out( "MEM TEST w/ data cache DISABLED\n\r");
		CS_LED_GREEN_ON;
		disable_dcache();

		memset( (void*)hexaddr, 0xAA55AA55, bytes );  // just to clear it out prior to the test
		//memory_test( hexaddr, bytes, width, tstnum, iters);

		enable_dcache();
		CS_LED_GREEN_OFF;
		



		term_dat_out( "MICHAEL BARR's memory test w/ data cache DISABLED\n\r");
		CS_LED_YELLOW_ON;
		disable_dcache();

		memset( (void*)hexaddr, 0xAA55AA55, bytes );  // just to clear it out prior to the test
		#define BASE_ADDRESS  (volatile datum *) 0x80000000
		#define NUM_BYTES     (1024 * 1024 * 8)

		if ( memTestDataBus( BASE_ADDRESS ) == 0 )
		{
			term_dat_out( "Data Bus Test PASSED!\n\r");
		}
		else
		{
			term_dat_out( "Data Bus Test FAILED!!!!\n\r");
		}
		
		if ( memTestAddressBus( BASE_ADDRESS, NUM_BYTES ) == NULL )
		{
			term_dat_out( "Address Bus Test PASSED!\n\r");
		}
		else
		{
			term_dat_out( "Address Bus Test FAILED!!!!\n\r");
		}

		if ( memTestDevice( BASE_ADDRESS, NUM_BYTES ) == NULL )
		{
			term_dat_out( "Device Test PASSED!\n\n\r");
		}
		else
		{
			term_dat_out( "Device Test FAILED!!!!\n\n\r");
		}

		enable_dcache();

	}
	*/
	
	/*
	for(;;)
	{
		P3_OUTP_SET = P3_STATE_GPO( 14 );  // on both the phytec and embedded artist boards this is an LED

		vTaskDelay( led_task_delay );

		P3_OUTP_CLR = P3_STATE_GPO( 14 );

		vTaskDelay( led_task_delay );

		//vTestSetLED( 2, 1 );

		//timer_wait_ms( TIMER_CNTR0, 25 );

		//vTestSetLED( 2, 0 );

		//timer_wait_ms( TIMER_CNTR0, 25 );
	}
	*/
	
//}


/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
int main( void )
{
	//UNS_32	lsize;
	
	//lsize = sizeof( DATE_TIME );
	//lsize = sizeof( ET_TABLE_ENTRY );
	//lsize = sizeof( RAIN_TABLE_ENTRY );
	//lsize = sizeof( FROM_COMMSERVER_PACKET_TOP_HEADER );

	//lsize = sizeof(STATION_PRESERVES_RECORD);
	//lsize = sizeof(STATION_HISTORY_RECORD);
	//lsize = sizeof(IRRIGATION_LIST_COMPONENT);
	//lsize = sizeof( comm_mngr.since_the_scan__master__number_of_clean_token_responses_rcvd );
	//lsize = sizeof(STATION_REPORT_DATA_RECORD);
	//lsize = sizeof(STATION_HISTORY_RECORD);
	//lsize = sizeof(CHAIN_MEMBERS_SHARED_STRUCT);

	//lsize = sizeof(chain.members);
	
	//lsize = sizeof(tpmicro_data);

	// 8/19/2015 mpd : Ran lights structure sizes.
	//lsize = sizeof( LIGHTS_INFO_TI_XFER_RECORD );				//   192
	//lsize = sizeof( LIGHTS_LIST_XFER_RECORD );				//     8
	//lsize = sizeof( LIGHTS_ACTION_XFER_RECORD );				//     8
	//lsize = sizeof( LIGHTS_ON_XFER_RECORD );					//    20
	//lsize = sizeof( LIGHT_OUTPUT_CURRENT_RECORD );			//     4
	//lsize = sizeof( LIGHTS_INFO_TF_XFER_RECORD );				//    20
	//lsize = sizeof( LIGHTS_OFF_XFER_RECORD );					//    12
	//lsize = sizeof( LIGHTS_BIT_FIELD_STRUCT );				//     1
	//lsize = sizeof( LIGHTS_SCHEDULE_STRUCT );					//     8
	//lsize = sizeof( LIGHTS_REPORT_RECORD );					//    16
	//lsize = sizeof( BY_LIGHTS_RECORD );						//    40 
	//lsize = sizeof( LIGHTS_BB_STRUCT );						//  1988 
	//lsize = sizeof( FOAL_LIGHTS_BATTERY_BACKED_STRUCT );		//  1928 
	//lsize = sizeof( IRRI_LIGHTS_BIT_FIELD_STRUCT );			//     4 
	//lsize = sizeof( IRRI_LIGHTS_LIST_COMPONENT );				//    36
	//lsize = sizeof( IRRI_LIGHTS );							//  1748
	//lsize = sizeof( COMPLETED_LIGHTS_REPORT_DATA_STRUCT ); 	// 10812
	//lsize = sizeof( LIGHTS_LIST_COMPONENT );			 		//    36
	//lsize = sizeof( LIGHT_STRUCT );					 		//   324 (this was obtained in lights.c due to compile issues)
	// This structure is xx bytes long on 8/19/2015.

	// ----------

	/*
	UNS_64	uns_64_var;
	UNS_32	uns_32_var;
	UNS_16	uns_16_var;

	uns_64_var = 0x7766554433221100;

	uns_32_var = uns_64_var;

	uns_16_var = uns_32_var;
	
	
	swap_16( uns_16_var );
	
	swap_32( uns_32_var );

	swap_64( uns_64_var );
	*/
	
	// ----------

	/*
	// 10/22/2012 rmd : Produces a data abort error.
	UNS_8	*ucp;
	UNS_32	*ulp, dummy;
	
	ucp = (UNS_8*)0x1000;

	ucp += 1;
	
	ulp = (UNS_32*)ucp;
	
	dummy = *ulp;
	*/
	
	// ----------

	/*
	// 10/22/2012 rmd : Produces a floating point divide by zero. Which is not an exception. Not
	// sure right now about floating point exception handling.
	volatile float	num1, num2, num3;

	num1 = 0.0;

	num2 = 0.0;

	num3 = (num1 / num2);
	
	snprintf( restart_info.exception_string, sizeof(restart_info.exception_string), "show that NaN: %10.5f", num3 );

	num3 *= 1.20005;
	
	snprintf( restart_info.exception_string, sizeof(restart_info.exception_string), "show that NaN: %10.5f", num3 );
	*/
	
	// ----------

	/*
	volatile UNS_32	clk_rate;

	// 10/15/2012 rmd : Answer is 208MHz.
	clk_rate = clkpwr_get_base_clock_rate( CLKPWR_ARM_CLK );

	// 10/15/2012 rmd : Answer is 104MHz.
	clk_rate = clkpwr_get_base_clock_rate( CLKPWR_HCLK );

	// 10/15/2012 rmd : Answer is 13MHz.
	clk_rate = clkpwr_get_base_clock_rate( CLKPWR_PERIPH_CLK );


	clk_rate = clkpwr_get_base_clock_rate( CLKPWR_PERIPH_CLK );
	*/
	
	// ----------

	/*
	// 1/13/2015 rmd : With the following you can turn on TP9 and output to it the various 
	clocks.

	clkpwr_set_mux( CLKPWR_TEST_CLK1, 1 ); 

	clkpwr_set_mux_state( CLKPWR_TEST_CLK1, CLKPWR_TESTCLK1_SEL_MOSC );

	// 1/13/2015 rmd : These calls diddle the XTAL capacitance.
	clkpwr_mainosc_setup( 0, 1 );

	clkpwr_mainosc_setup( 0x7F, 1 );

	clkpwr_set_mux_state( CLKPWR_TEST_CLK1, CLKPWR_TESTCLK1_SEL_PERCLK );

	clkpwr_set_mux_state( CLKPWR_TEST_CLK1, CLKPWR_TESTCLK1_SEL_RTC );

	// 1/13/2015 rmd : TP8 output.
	//clkpwr_set_mux( CLKPWR_TEST_CLK2, 1 );
	*/
	
	// ----------

	// UNTIL alerts have been checked out they cannot be used. Because this variable is a
	// battery-backed variable it is not initialized on startup like the .bss vars are. So we
	// explicitly set.
	// 
	// Additionally DO NOT be tempted to move the alerts pile initialization from the startup
	// task to here. We are counting on the scheduler to be running when making alerts as we are
	// taking and giving a mutex with the MAXIMUM delay time set. In other words we could
	// attempt a block and task switch w/o the scheduler running. So wait till the startup task
	// to init alerts.
	alerts_struct_user.ready_to_use_bool = (false);  // battery_backed variable ... MUST explicitly set

	alerts_struct_changes.ready_to_use_bool = (false);

	alerts_struct_tp_micro.ready_to_use_bool = (false);

	alerts_struct_engineering.ready_to_use_bool = (false);

	// ----------

	// Create the memory partitions
	//
	// 03.27.2009 rmd ... The memory MUST be initialized BEFORE any freeRTOS calls because we have
	// directed the freeRTOS heap calls to use our heap management system.
	// 
	// There are NO alert lines attempted during this intialization! The pile has not yet been checked and initialized.
	// We have protection against such if tried though.
	init_mem_partitioning();

	init_mem();  // simply prepares memory use stats variables

	// ----------

	// INTERRUPTS ARE NOT ENABLED AT THIS POINT. Apparently OpenRTOS does not want interrupts
	// enabled during the scheduler initialization. They make particular note of this at the
	// declaration of the ulCriticalNesting variable.
	// 
	// Regarding any hardware initializations that require interrupts enabled ... perform them
	// in a task. For example the lcd task performs the i2c DAC initialization. And this
	// requires interrupts to be enabled when operating the i2c channel. And therefore cannot be
	// done in main.

	// STACK: Set to 4K bytes - quite large - why not ... we have the room and this task will be
	// deleted and this memory freed

	xTaskCreate( system_startup_task, (signed char *)"Startup", 0x1000/4, NULL, PRIORITY_LEVEL_STARTUP_TASK_1, &startup_task_handle );

	OS_make_task_VFP_capable( startup_task_handle, (void*)&startup_task_FLOP_registers );

	// ----------

	vTaskStartScheduler( );

	// ----------

	// Will not get here unless scheduler initialization fails.
	for(;;);

	// ----------

	return( 0 ); 
}

/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
extern void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed char *pcTaskName )
{
	// This function will get called if a task overflows its stack.

	// 6/4/2015 rmd : Well we have seen this work and make an alert line. It is interesting to
	// think about though. If the stack is blown I suppose there is a big chance this function
	// cannot run. But more likely is as this function runs we are just destroying the stack
	// further? Haven't investigated. And can't be diverted to do so at this time.
	
	// ----------
	
	// Just to stop warning messages.
	( void ) pxTask;
	( void ) pcTaskName;

	// This seems like what we should do.
	vTaskSuspendAll();

	#ifndef NDEBUG
		for(;;);
	#else
		// Trying to get an alert line generated and kill the task. Maybe this works? Not sure.
		char	str_64[ 64 ];
		snprintf( str_64, sizeof(str_64), "Stack Overflow: %s", pcTaskName );
		Alert_Message( str_64 );
		for(;;) vTaskDelay(100);
	#endif
}

/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/

// configUSE_TICK_HOOK
/*
static void vApplicationTickHook( void )
{
  // Can be used as specified in the OpenRTOS user manual.
}
*/

/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/

// sample idle hook function - see macro definition configUSE_IDLE_HOOK
/*
static void vApplicationIdleHook( void )
{
  // Can be used as specified in the OpenRTOS user manual.
}
*/

/*-----------------------------------------------------------*/

extern void vApplicationMallocFailedHook( void )
{
	for(;;);
}

/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
void vConfigureTimerForRunTimeStats( void )
{
	TIMER_CNTR_REGS_T	*ltimer;

	ltimer = FreeRTOS_RUN_TIME_STATS_TIMER;


	clkpwr_clk_en_dis( FreeRTOS_RUN_TIME_STATS_TIMER_CLK_EN, 1 );

	// Setup default timer state as standard timer mode, timer disabled and all match and counters disabled.
	ltimer->tcr = 0;  // timer control register - disabled
	ltimer->ctcr = TIMER_CNTR_SET_MODE(TIMER_CNTR_CTCR_TIMER_MODE);  // timer mode
	ltimer->ccr = 0;  // capture control register fully disabled
	ltimer->emr = (TIMER_CNTR_EMR_EMC_SET(0, TIMER_CNTR_EMR_NOTHING) | TIMER_CNTR_EMR_EMC_SET(1, TIMER_CNTR_EMR_NOTHING) | TIMER_CNTR_EMR_EMC_SET(2, TIMER_CNTR_EMR_NOTHING) | TIMER_CNTR_EMR_EMC_SET(3, TIMER_CNTR_EMR_NOTHING));

	// Clear pending interrupts and reset counts.
	ltimer->tc = 0;  // the count register itself
	ltimer->pc = 0;  // prescale counter register

	ltimer->mr[ 0 ] = 0;  // Zero all the match registers.
	ltimer->mr[ 1 ] = 0;
	ltimer->mr[ 2 ] = 0;
	ltimer->mr[ 3 ] = 0;

	ltimer->pr = 650;		// Set to count the main counter at 20kHz.
	ltimer->mcr = 0x00;		// Just count.

	ltimer->ir = 0xFF;  // Clear ALL pending interrupts. TODO set the 32 bit value!

	ltimer->tcr = TIMER_CNTR_TCR_EN;  // Enable the timer/counter.
}

unsigned long return_run_timer_counter_value( void )
{
	TIMER_CNTR_REGS_T	*ltimer;
	
	ltimer = FreeRTOS_RUN_TIME_STATS_TIMER;
	
	return( ltimer->tc );
}

/*-----------------------------------------------------------*/

/*
 * Check task function.
 */
#if 0
static void vCheckTask( void *pvParameters )
{
long ulTicksToWait = mainCHECK_NORMAL_DELAY;
portTickType		xLastExecutionTime;
UNS_32				block_line_count_ui;

/*
UNS_32				dura_ui, iii;
char		        str_128[ 128 ];
DATA_HANDLE			ldh;
*/

	xLastExecutionTime = xTaskGetTickCount();

	block_line_count_ui = 0;

//	dura_ui = 0;
    
	for( ;; )
	{
		/* Perform this check every mainCHECK_NORMAL_DELAY milliseconds 
		if no error is detected otherwise check every mainERROR_PERIOD 
		milliseconds */
		vTaskDelayUntil( &xLastExecutionTime, ulTicksToWait );


		/*
		for( iii = 0; iii<120; iii++ )
		{
			dura_ui += 1;
			snprintf( str_128,  sizeof(str_128),  "%4d 1234567890123456789012345678901234567890123456789012345678901234567890\r\n", dura_ui );
			ldh.dlen = strlen( str_128 );
			ldh.dptr = (unsigned char*)&str_128;
			AddCopyOfBlockToXmitList( UPORT_RRE, ldh );
		}
		*/
		
        
		/*
		// Has an error been found in any task?
		if( xAreBlockingQueuesStillRunning( ) != pdTRUE )
		{
			debug_printf( "Queues Failed \r\n" );
			lStatus = pdFAIL;
		}

		if( xAreBlockTimeTestTasksStillRunning( ) != pdTRUE )
		{
			block_line_count_ui += 1;
			debug_printf( "Block Time Tests Failed %d \r\n", block_line_count_ui );
			lStatus = pdFAIL;
		}

		if( xAreSemaphoreTasksStillRunning( ) != pdTRUE )
		{
			debug_printf( "Semaphores Failed \r\n" );
			lStatus = pdFAIL;
		}

		if( xArePollingQueuesStillRunning( ) != pdTRUE )
		{
			debug_printf( "Polling Queues Failed \r\n" );
			lStatus = pdFAIL;
		}

#if ( configUSE_VFP == 1 )
		if( xAreMathsTaskStillRunning( ) != pdTRUE )
		{
			debug_printf( "Math Task Failed \r\n" );
			lStatus = pdFAIL;
		}

		if( xAreFlopRegisterTestsStillRunning( ) != pdTRUE )
		{
			debug_printf( "FLOP Register Failed \r\n" );
			lStatus = pdFAIL;
		}
#endif
        */


		
		/* If an error has been found then increase our cycle rate, and in so
		going increase the rate at which the check task LED toggles. */
		if( lStatus != pdPASS )
		{
			ulTicksToWait = mainERROR_PERIOD;
		}
		
		/* Toggle the user LED on the board. */
		vParTestToggleLED( mainUSER_LED );
	}
}
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

