/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cal_math.h"

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for modf
#include	<math.h>

// 10/12/2015 ajv : Required for uint32_t to limit the amount of edits to NewLib's
// implemtation of the roundf function.
#include	<stdint.h>




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Accepts a floating-point value and returns the integer portion rounded to the
 * nearest whole number.
 * 
 * @param pfl The floating-point value to round.
 * 
 * @return UNS_16 The integer portion of the floating-point number round to the
 *                nearest whole number.
 *
 * @author rmd
 * 
 * @revisions
 *   4/27/2012 rmd : Copied original code from ET2000e. We want UNS_16 as the
 *             return type. This is used when loading the flow recording lines.
 */
extern UNS_16 __round_UNS16( const float pfl )
{
	double  lfrac;

	double  lint;

	UNS_16  rv;

	// modf returns the fractional part of the pfl; it also puts the integral
	// part into lint
	lfrac = modf( pfl, &lint );

	if( lfrac >= 0.5 )
	{
		rv = (UNS_16)(lint + 1);
	}
	else
	{
		rv = (UNS_16)lint;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/12/2015 ajv : Support for C99 version of roundf routine. Extracted from NewLib source
// since it's not included in Rowley's
// library.
// 
// Source: https://github.com/eblot/newlib/blob/master/newlib/libm/common/fdlibm.h

/* A union which permits us to convert between a float and a 32-bit int. */
typedef union
{
	float value;

	uint32_t word;

} ieee_float_shape_type;

// ----------

/* Get a 32-bit int from a float.  */
#define GET_FLOAT_WORD( i, d )		\
do									\
{									\
  ieee_float_shape_type gf_u;		\
  gf_u.value = (d);					\
  (i) = gf_u.word;					\
} while (0)

// ----------

/* Set a float from a 32-bit int.  */
#define SET_FLOAT_WORD( d, i )		\
do									\
{									\
  ieee_float_shape_type sf_u;		\
  sf_u.word = (i);					\
  (d) = sf_u.value;					\
} while (0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
// 10/12/2015 ajv : C99 version of roundf routine. Extracted from NewLib source since it's
// not included in Rowley's library.
// 
// Source: https://github.com/eblot/newlib/blob/master/newlib/libm/common/sf_round.c
extern float roundf( float x )
{
	uint32_t w;

	/* Most significant word, least significant word. */
	int exponent_less_127;

	// ----------

	GET_FLOAT_WORD( w, x );

	// ----------

	/* Extract exponent field. */
	exponent_less_127 = (int)((w & 0x7f800000) >> 23) - 127;

	if( exponent_less_127 < 23 )
	{
		if( exponent_less_127 < 0 )
		{
			w &= 0x80000000;

			if( exponent_less_127 == -1 )
			{
				/* Result is +1.0 or -1.0. */
				w |= ((uint32_t)127 << 23);
			}
		}
		else
		{
			unsigned int exponent_mask = 0x007fffff >> exponent_less_127;

			if( (w & exponent_mask) == 0 )
			{
				/* x has an integral value. */
				return( x );
			}

			w += 0x00400000 >> exponent_less_127;
			w &= ~exponent_mask;
		}
	}
	else
	{
		if( exponent_less_127 == 128 )
		{
			/* x is NaN or infinite. */
			return( x + x );
		}
		else
		{
			return( x );
		}
	}

	// ----------

	SET_FLOAT_WORD( x, w );

	// ----------
	
	return( x );
}

/* ---------------------------------------------------------- */
// 10/12/2015 ajv : Round a floating-point value to the number of specified decimals.
extern float __round_float( const float pfl, const UNS_32 pprecision )
{
	float   rv;

	rv = roundf(pfl * pow(10, pprecision)) / pow(10, pprecision);

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

