/*  file = cal_string.h   2009.05.27    rmd                   */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_CAL_STRING_H
#define _INC_CAL_STRING_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

// 6/18/2014 ajv : Required for size_t definition
#include	<stdio.h>

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// This length applies to virtually ALL the name strings in the controller.
#define NUMBER_OF_CHARS_IN_A_NAME	(48)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern const char *GetAlertActionStr( const UNS_32 pAction );

extern const char *GetBooleanStr( const BOOL_32 pBool );

extern const char *GetBudgetModeStr( const UNS_32 pmode );

extern const char *GetChangeReasonStr( const UNS_32 pReason );

extern const char *GetDayLongStr( const UNS_32 pDay );

extern const char *GetDayShortStr( const UNS_32 pDay );

extern const char *GetExposureStr( const UNS_32 pexposure );

extern const char *GetFlowMeterStr( const UNS_32 pFlowMeterType );

extern const char *GetFlowTypeStr( const UNS_32 pFlowTypeType );

extern const char *GetHeadTypeStr( const UNS_32 phead_type );

extern const char *GetMasterValveStr( const UNS_32 pMasterValveType );

extern const char *GetMonthLongStr( const UNS_32 pMonth );

extern const char *GetMonthShortStr( const UNS_32 pMonth );

extern const char *GetNoYesStr( const BOOL_32 pBool );

extern const char *GetOffOnStr( const BOOL_32 pBool );

extern const char *GetPlantTypeStr( const UNS_32 pplant_type );

extern const char *GetPOCBudgetEntryStr( const UNS_32 pEntryType );

extern const char *GetPOCUsageStr( const UNS_32 pPOCUsage );

extern const char *GetPriorityLevelStr( const UNS_32 pPriorityLevel );

extern const char *GetReasonOnStr( const UNS_32 pReason ); 

extern const char *GetReedSwitchStr( const UNS_32 preed_switch_type );

extern const char *GetScheduleTypeStr( const UNS_32 pType );

extern const char *GetSlopePercentageStr( const UNS_32 pslope_percentage_index );

extern const char *GetSoilTypeStr( const UNS_32 psoil_type );

DLLEXPORT const char *GetTimeZoneStr( const UNS_32 pTimeZone );

extern const char *GetWaterUnitsStr( const UNS_32 punits );


extern const char *Get_MoistureSensor_MoistureControlMode_Str( const UNS_32 pcontrol_mode );

extern const char *Get_MoistureSensor_HighTemperatureAction_Str( const UNS_32 paction );

extern const char *Get_MoistureSensor_LowTemperatureAction_Str( const UNS_32 paction );

extern const char *Get_MoistureSensor_HighECAction_Str( const UNS_32 paction );

extern const char *Get_MoistureSensor_LowECAction_Str( const UNS_32 paction );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern char *sp_strlcat( char *dest, size_t pdest_size, char* fmt, ... );

extern char *ShaveLeftPad( char *pbuf, const char *pstr );

#ifdef _MSC_VER

	// 2012.02.23 rmd : So now with CrossWorks version 2.2 they have included
	// definitions for the strlcpy and strlcat functions. So we comment out our
	// copies of thse functions. By the way I pulled this source code from the
	// RedHat Newlib library source.

	// 6/23/2014 ajv : However, Visual Studio does not include these
	// definitions. Therefore, since this file is shared with the comm_server,
	// allow these to be access by the comm server without having to redefine
	// them elsewhere.

	extern size_t strlcpy( char *dst, const char *src, const size_t siz );

	extern size_t strlcat( char *dst, const char *src, const size_t siz );

	extern char *strnstr( const char *s, const char *find, size_t slen );

#else

	//----------

	extern char *trim_white_space( char *pstr );

	extern char *RemovePathFromFileName( char *str1 );

	/* ---------------------------------------------------------- */
	// 2012.02.10 ajv : Some of the following are depracated 
	// functions which may be able to be removed after legacy code
	// has been removed.
	/* ---------------------------------------------------------- */

	extern char *ShaveRightPad_63max( char *pbuf, const char *pstr );

	extern char *CenterDisplayString( char *pbuf, char *ptext );

	extern char *PadLeft(char *pbuf, const UNS_32 psize);

	extern char *PadRight( char *pbuf, const UNS_32 psize );

	//----------

	extern char *format_binary_32( char *pbuf, const void *pvalue );

	extern char* find_string_in_block( const char *block_ptr, const char *string_to_find_ptr, size_t block_length );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

