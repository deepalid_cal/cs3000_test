/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* file = cal_string.c   2009.05.18  rmd  */

// A collection of general purpose string functions primarily for lcd support.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

// 6/18/2014 ajv : Required for va_list variable type and va_ routines
#include	<stdarg.h>

// 6/18/2014 ajv : Required for memset and strlcat
#include	<string.h>

#include	"cal_string.h"

#include	"cal_td_utils.h"

#include	"configuration_network.h"

// Necessary to utilize the isspace macro.
#include	"ctype.h"

#include	"change.h"

#include	"guilib.h"

#include	"irrigation_system.h"

#include	"poc.h"

#include	"station_groups.h"

#include	"moisture_sensors.h"

#ifdef _MSC_VER

	#include	"sql_merge.h"

#endif



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define LINE_LEN		(53)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static char UNKNOWN_STR[] = "UNKNOWN";

static char MANPROG_STR[] = "Manual Program";

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in alert action. If 
 * the current language is English, a pointer to the English string is returned.
 * Similarly, if the current language is Spanish, a pointer to the Spanish 
 * string is returned. 
 * 
 * @param pAction The 0-based alert action.
 * 
 * @return char* A pointer to the text string based upon the passed in value and 
 *  	   the current language.
 *  
 * @author 7/19/2011 Adrianusv
 *  
 * @revisions 
 *    2/7/2012 Modified passed in string to be a 32-bit unsigned integer
 */
extern const char *GetAlertActionStr( const UNS_32 pAction )
{
	char *rv;

	// 6/1/2015 ajv : Note that the >= comparison always evaluates to true since pAction is
	// unsigned and ALERT_ACTIONS_NONE is 0. Based on this, the condition could be eliminated.
	// However, if the low-end value ever changes to something other than 0, we want to make
	// sure the condition checks that. Therefore, leave the check in place even though it's
	// (currently) unnecessary.
	if( (pAction >= ALERT_ACTIONS_NONE) && (pAction <= ALERT_ACTIONS_ALERT_SHUTOFF) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtAlertActions_0 + pAction), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in Boolean value. If 
 * the current language is English, a pointer to the English string is returned.
 * Similarly, if the current language is Spanish, a pointer to the Spanish 
 * string is returned. 
 * 
 * @param pBool True or false.
 * 
 * @return char* A pointer to the text string based upon the passed in value and 
 *  	   the current language.
 *  
 * @author 8/30/2011 Adrianusv
 *  
 * @revisions 
 *    2/7/2012 Modified passed in string to be a 32-bit unsigned integer
 */
extern const char *GetBooleanStr( const BOOL_32 pBool )
{
	char *rv;

	if( (pBool == (false)) || (pBool == (true)) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtFalseTrue_0 + pBool), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in budget mode 
 * 
 * @param pmode The 0-based budget mode.
 * 
 * @return char* A pointer to a string.
 *  
 * @author 5/24/2016 skc
 *  
 * @revisions 
 *    
 */
extern const char *GetBudgetModeStr( const UNS_32 pmode )
{
	char *rv;

	if( (pmode >= IRRIGATION_SYSTEM_BUDGET_MODE_MIN) && (pmode <= IRRIGATION_SYSTEM_BUDGET_MODE_MAX) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtBudgetMode_0 + pmode), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );

}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in change reason.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task 
 * when the alerts report is generated.
 * 
 * @param pReason The reason for the change, of type UNS_32. 
 *  
 * @return char* A pointer to the text string based upon the passed in value and
 * the current language.
 * 
 * @author 5/20/2011 Adrianusv
 *
 * @revisions (none)
 */
extern const char *GetChangeReasonStr( const UNS_32 pReason )
{
	char *rv;

	// 6/1/2015 ajv : Note that the >= comparison always evaluates to true since pReason is
	// unsigned and CHANGE_REASON_CENTRAL_OR_MOBILE is 0. Based on this, the condition could be
	// eliminated. However, if the low-end value ever changes to something other than 0, we want
	// to make sure the condition checks that. Therefore, leave the check in place even though
	// it's (currently) unnecessary.
	if( (pReason >= CHANGE_REASON_CENTRAL_OR_MOBILE) && (pReason <= CHANGE_REASON_MAX_REASON) )
	{
		rv = GuiLib_GetTextLanguagePtr( (UNS_16)GuiStruct_lstChangeReasons_0, (UNS_16)(pReason-1), GuiConst_LANGUAGE_ENGLISH );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in day of the week. 
 * If the current language is English, a pointer to the English string is 
 * returned. Similarly, if the current language is Spanish, a pointer to the 
 * Spanish string is returned.
 * 
 * @param pDay The 0-based day.
 * 
 * @return char* A pointer to the text string based upon the passed in value and 
 *  	   the current language.
 *  
 * @author 8/30/2011 Adrianusv
 *  
 * @revisions 
 *    2/7/2012 Modified passed in string to be a 32-bit unsigned integer
 */
extern const char *GetDayLongStr( const UNS_32 pDay )
{
	char *rv;

	if( pDay < DAYS_IN_A_WEEK )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtDayOfWeek_0 + pDay), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in day of the week. 
 * If the current language is English, a pointer to the English string is 
 * returned. Similarly, if the current language is Spanish, a pointer to the 
 * Spanish string is returned.
 * 
 * @param pDay The 0-based day.
 * 
 * @return char* A pointer to the text string based upon the passed in value and 
 *  	   the current language.
 *  
 * @author 8/30/2011 Adrianusv
 *  
 * @revisions 
 *    2/7/2012 Modified passed in string to be a 32-bit unsigned integer
 */
extern const char *GetDayShortStr( const UNS_32 pDay )
{
	char *rv;

	if( pDay < DAYS_IN_A_WEEK )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtDayOfWeek_abbrev_0 + pDay), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in exposure index. 
 * If the current language is English, a pointer to the English string is 
 * returned. Similarly, if the current language is Spanish, a pointer to the 
 * Spanish string is returned. 
 * 
 * @param pplant_type The 0-based plant type index.
 * 
 * @return char* A pointer to the text string based upon the passed in value and 
 *  	   the current language.
 *  
 * @author 7/2/2012 Adrianusv
 *  
 * @revisions 
 *    7/2/2012 Initial release
 */
extern const char *GetExposureStr( const UNS_32 pexposure )
{
	char *rv;

	// 6/1/2015 ajv : Note that the >= comparison always evaluates to true since pexposure is
	// unsigned and STATION_GROUP_EXPOSURE_MIN is 0. Based on this, the condition could be
	// eliminated. However, if the low-end value ever changes to something other than 0, we want
	// to make sure the condition checks that. Therefore, leave the check in place even though
	// it's (currently) unnecessary.
	if( (pexposure >= STATION_GROUP_EXPOSURE_MIN) && (pexposure <= STATION_GROUP_EXPOSURE_MAX) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtExposure_0 + pexposure), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in flow meter type. 
 * If the current language is English, a pointer to the English string is 
 * returned. Similarly, if the current language is Spanish, a pointer to the 
 * Spanish string is returned. 
 * 
 * @param pFlowMeterType The 0-based flow meter type.
 * 
 * @return char* A pointer to the text string based upon the passed in value and 
 *  	   the current language.
 *  
 * @author 8/31/2011 Adrianusv
 *  
 * @revisions 
 *    2/7/2012 Modified passed in string to be a 32-bit unsigned integer
 */
extern const char *GetFlowMeterStr( const UNS_32 pFlowMeterType )
{
	char *rv;

	// 6/1/2015 ajv : Note that the >= comparison always evaluates to true since pFlowMeterType
	// is unsigned and POC_FM_CHOICE_MIN is 0. Based on this, the condition could be eliminated.
	// However, if the low-end value ever changes to something other than 0, we want to make
	// sure the condition checks that. Therefore, leave the check in place even though it's
	// (currently) unnecessary.
	if( (pFlowMeterType >= POC_FM_CHOICE_MIN) && (pFlowMeterType <= POC_FM_CHOICE_MAX) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtFlowMeters_0 + pFlowMeterType), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in irrigation flow type used to 
 * create database column names.
 * 
 * @param pFlowTypeType The 0-based irrigation flow type.
 * 
 * @return char* A pointer to a string.
 *  
 * @author 5/10/2016 skc
 *  
 * @revisions 
 *    
 */
extern const char *GetFlowTypeStr( const UNS_32 pFlowTypeType )
{
	char *rv;

	switch( pFlowTypeType )
	{
	case FLOW_TYPE_IRRIGATION:
		rv = "Irrigation";
		break;
	case IN_LIST_FOR_PROGRAMMED_IRRIGATION:
		rv = "Program";
		break;
	case IN_LIST_FOR_MANUAL_PROGRAM:
		rv = "ManualProgram";
		break;
	case IN_LIST_FOR_no_longer_used:
		rv = "NA3";
		break;
	case IN_LIST_FOR_MANUAL:
		rv = "Manual";
		break;
	case IN_LIST_FOR_WALK_THRU:
		rv = "WalkThru";
		break;
	case IN_LIST_FOR_TEST:
		rv = "Test";
		break;
	case IN_LIST_FOR_MOBILE:
		rv = "Mobile";
		break;
	case FLOW_TYPE_MVOR:
		rv = "MVOR";
		break;
	case FLOW_TYPE_NON_CONTROLLER:
		rv = "NonController";
		break;
	default:
		rv = UNKNOWN_STR;
		break;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in head type. If the 
 * current language is English, a pointer to the English string is returned. 
 * Similarly, if the current language is Spanish, a pointer to the Spanish 
 * string is returned. 
 * 
 * @param phead_type The 0-based head type.
 * 
 * @return char* A pointer to the text string based upon the passed in value and 
 *  	   the current language.
 *  
 * @author 7/2/2012 Adrianusv
 *  
 * @revisions 
 *    7/2/2012 Initial release
 */
extern const char *GetHeadTypeStr( const UNS_32 phead_type )
{
	char *rv;

	// 6/1/2015 ajv : Note that the >= comparison always evaluates to true since phead_type is
	// unsigned and STATION_GROUP_HEAD_TYPE_MIN is 0. Based on this, the condition could be
	// eliminated. However, if the low-end value ever changes to something other than 0, we want
	// to make sure the condition checks that. Therefore, leave the check in place even though
	// it's (currently) unnecessary.
	if( (phead_type >= STATION_GROUP_HEAD_TYPE_MIN) && (phead_type <= STATION_GROUP_HEAD_TYPE_MAX) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtHeadTypes_0 + phead_type), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in master valve type.
 * If the current language is English, a pointer to the English string is 
 * returned. Similarly, if the current language is Spanish, a pointer to the 
 * Spanish string is returned. 
 * 
 * @param pMasterValveType The 0-based master valve type.
 * 
 * @return char* A pointer to the text string based upon the passed in value and 
 *  	   the current language.
 *  
 * @author 8/31/2011 Adrianusv
 *  
 * @revisions 
 *    2/7/2012 Modified passed in string to be a 32-bit unsigned integer
 */
extern const char *GetMasterValveStr( const UNS_32 pMasterValveType )
{
	char *rv;

	if( (pMasterValveType == POC_MV_TYPE_NORMALLY_OPEN) || (pMasterValveType == POC_MV_TYPE_NORMALLY_CLOSED) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtMVState_0 + pMasterValveType), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in month. If the 
 * current language is English, a pointer to the English string is returned. 
 * Similarly, if the current language is Spanish, a pointer to the Spanish string 
 * is returned. 
 * 
 * @param pMonth The 0-based month.
 * 
 * @return char* A pointer to the text string based upon the passed in value and 
 *  	   the current language.
 *  
 * @author 8/30/2011 Adrianusv
 *  
 * @revisions 
 *    2/7/2012 Modified passed in string to be a 32-bit unsigned integer
 */
extern const char *GetMonthLongStr( const UNS_32 pMonth )
{
	char *rv;

	if( pMonth < MONTHS_IN_A_YEAR )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtMonths_1 + pMonth), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in month. If the 
 * current language is English, a pointer to the English string is returned. 
 * Similarly, if the current language is Spanish, a pointer to the Spanish string 
 * is returned. 
 * 
 * @param pMonth The 0-based month.
 * 
 * @return char* A pointer to the text string based upon the passed in value and 
 *  	   the current language.
 *  
 * @author 8/30/2011 Adrianusv
 *  
 * @revisions 
 *    2/7/2012 Modified passed in string to be a 32-bit unsigned integer
 */
extern const char *GetMonthShortStr( const UNS_32 pMonth )
{
	char *rv;

	if( pMonth < MONTHS_IN_A_YEAR )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtMonths_abbrev_1 + pMonth), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}


	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in Boolean value as 
 * either NO or YES. If the current language is English, a pointer to the 
 * English string is returned. Similarly, if the current language is Spanish, a 
 * pointer to the Spanish string is returned. 
 * 
 * @param pBool True to display YES; otherwise, false.
 * 
 * @return char* A pointer to the text string based upon the passed in value and 
 *  	   the current language.
 *  
 * @author 9/14/2011 Adrianusv
 *  
 * @revisions 
 *    2/7/2012 Modified passed in string to be a 32-bit unsigned integer
 */
extern const char *GetNoYesStr( const BOOL_32 pBool )
{
	char *rv;

	if( pBool == (true) )
	{
		rv = GuiLib_GetTextPtr( GuiStruct_txtYes_0, 0 );
	}
	else if( pBool == (false) )
	{
		rv = GuiLib_GetTextPtr( GuiStruct_txtNo_0, 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in Boolean value as 
 * either OFF or ON. If the current language is English, a pointer to the 
 * English string is returned. Similarly, if the current language is Spanish, a 
 * pointer to the Spanish string is returned. 
 * 
 * @param pBool True to display ON; otherwise, false.
 * 
 * @return char* A pointer to the text string based upon the passed in value and 
 *  	   the current language.
 *  
 * @author 8/30/2011 Adrianusv
 *  
 * @revisions 
 *    2/7/2012 Modified passed in string to be a 32-bit unsigned integer
 */
extern const char *GetOffOnStr( const BOOL_32 pBool )
{
	char *rv;

	if( (pBool == (false)) || (pBool == (true)) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtOffOn_0 + pBool), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in plant type index. 
 * If the current language is English, a pointer to the English string is 
 * returned. Similarly, if the current language is Spanish, a pointer to the 
 * Spanish string is returned. 
 * 
 * @param pplant_type The 0-based plant type index.
 * 
 * @return char* A pointer to the text string based upon the passed in value and 
 *  	   the current language.
 *  
 * @author 7/2/2012 Adrianusv
 *  
 * @revisions 
 *    7/2/2012 Initial release
 */
extern const char *GetPlantTypeStr( const UNS_32 pplant_type )
{
	char *rv;

	// 6/1/2015 ajv : Note that the >= comparison always evaluates to true since pplant_type is
	// unsigned and STATION_GROUP_PLANT_TYPE_MIN is 0. Based on this, the condition could be
	// eliminated. However, if the low-end value ever changes to something other than 0, we want
	// to make sure the condition checks that. Therefore, leave the check in place even though
	// it's (currently) unnecessary.
	if( (pplant_type >= STATION_GROUP_PLANT_TYPE_MIN) && (pplant_type <= STATION_GROUP_PLANT_TYPE_MAX) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtPlantTypes_0 + pplant_type), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in value. If the 
 * current language is English, a pointer to the English string is returned. 
 * Similarly, if the current language is Spanish, a pointer to the Spanish string 
 * is returned. 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param pPOCUsage The 0-based Budget Entry Option
 * 
 * @return const char* A pointer to the text string based upon the passed in
 * value and the current language.
 *
 * @author 04/05/2012 skc
 *
 * @revisions (none)
 */
extern const char *GetPOCBudgetEntryStr( const UNS_32 pEntryType )
{
	char *rv;

	if( (pEntryType >= POC_BUDGET_GALLONS_ENTRY_OPTION_MIN) && (pEntryType <= POC_BUDGET_GALLONS_ENTRY_OPTION_MAX) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtBudgetOptions_0 + pEntryType), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );

}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in value. If the 
 * current language is English, a pointer to the English string is returned. 
 * Similarly, if the current language is Spanish, a pointer to the Spanish string 
 * is returned. 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param pPOCUsage The 0-based POC usage.
 * 
 * @return const char* A pointer to the text string based upon the passed in
 * value and the current language.
 *
 * @author 10/16/2012 Adrianusv
 *
 * @revisions (none)
 */
extern const char *GetPOCUsageStr( const UNS_32 pPOCUsage )
{
	char *rv;

	// 6/1/2015 ajv : Note that the >= comparison always evaluates to true since pPOCUsage is
	// unsigned and POC_USAGE_MIN is 0. Based on this, the condition could be eliminated.
	// However, if the low-end value ever changes to something other than 0, we want to make
	// sure the condition checks that. Therefore, leave the check in place even though it's
	// (currently) unnecessary.
	if( (pPOCUsage >= POC_USAGE_MIN) && (pPOCUsage <= POC_USAGE_MAX) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtPOCUsages_0 + pPOCUsage), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in priority level. If
 * the current language is English, a pointer to the English string is 
 * returned. Similarly, if the current language is Spanish, a pointer to the 
 * Spanish string is returned.
 * 
 * @param pPriorityLevel The 1-based priority level.
 * 
 * @return char* A pointer to the text string based upon the passed in value and 
 *  	   the current language.
 *  
 * @author 5/20/2011 Adrianusv
 *  
 * @revisions 
 *    2/7/2012 Modified passed in string to be a 32-bit unsigned integer
 */
extern const char *GetPriorityLevelStr( const UNS_32 pPriorityLevel )
{
	char *rv;

	if( (pPriorityLevel >= PRIORITY_LOW) && (pPriorityLevel <= PRIORITY_HIGH) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtPriorityLevels_1 + (pPriorityLevel-1)), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in reason in the
 * list. If the current language is English, a pointer to the English string is
 * returned. Similarly, if the current language is Spanish, a pointer to the
 * Spanish string is returned.
 * 
 * @param pReason The 0-based reason in the list.
 * 
 * @return const char* A pointer to the text string based upon the passed in
 *                     value and the current language.
 *
 * @author 5/22/2012 Adrianusv
 *
 * @revisions
 *    5/22/2012 Initial release
 */
extern const char *GetReasonOnStr( const UNS_32 pReason )
{
	char *rv;

	// 6/1/2015 ajv : Note that the >= comparison always evaluates to true since pReason is
	// unsigned and IN_LIST_FOR_LOWEST_WEIGHTED_REASON is 0. Based on this, the condition could
	// be eliminated. However, if the low-end value ever changes to something other than 0, we
	// want to make sure the condition checks that. Therefore, leave the check in place even
	// though it's (currently) unnecessary.
	//
	// 7/20/2015 rmd : Updated to only look at currently used irrigation reasons. I'm going to
	// use direct values. Knowing that they will only change if hell rose from the depths. So
	// many things work according to their values.
	if( (pReason >= IN_LIST_FOR_PROGRAMMED_IRRIGATION) && (pReason <= IN_LIST_FOR_MOBILE) )
	{
		// 7/20/2015 rmd : Updated to make an exception for no longer used (old MANPROG_1) and for
		// the rejiggered MAN_PROG as easy gui isn't able to be updated at this moment.
		if( pReason == IN_LIST_FOR_no_longer_used )
		{
			rv = UNKNOWN_STR;
		}
		else
		if( pReason == IN_LIST_FOR_MANUAL_PROGRAM )
		{
			rv = MANPROG_STR;
		}
		else
		{
			rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtReasonInList_0 + pReason), 0 );
		}
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern const char *GetReedSwitchStr( const UNS_32 preed_switch_type )
{
	char *rv;

	// 6/1/2015 ajv : Note that the >= comparison always evaluates to true since
	// preed_switch_type is unsigned and POC_FM_REED_SWITCH_MIN is 0. Based on this, the
	// condition could be eliminated. However, if the low-end value ever changes to something
	// other than 0, we want to make sure the condition checks that. Therefore, leave the check
	// in place even though it's (currently) unnecessary.
	if( (preed_switch_type >= POC_FM_REED_SWITCH_MIN) && (preed_switch_type <= POC_FM_REED_SWITCH_MAX) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtReedSwitchType_0 + preed_switch_type), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in schedule type. If 
 * the current language is English, a pointer to the English string is returned. 
 * Similarly, if the current language is Spanish, a pointer to the Spanish 
 * string is returned. 
 * 
 * @param pType The 0-based schedule type.
 * 
 * @return char* A pointer to the text string based upon the passed in value and 
 *  	   the current language.
 *  
 * @author 8/16/2011 Adrianusv
 *  
 * @revisions 
 *    2/7/2012 Modified passed in string to be a 32-bit unsigned integer
 */
extern const char *GetScheduleTypeStr( const UNS_32 pType )
{
	char *rv;

	// 6/1/2015 ajv : Note that the >= comparison always evaluates to true since pType is
	// unsigned and SCHEDULE_TYPE_MIN is 0. Based on this, the condition could be eliminated.
	// However, if the low-end value ever changes to something other than 0, we want to make
	// sure the condition checks that. Therefore, leave the check in place even though it's
	// (currently) unnecessary.
	if( (pType >= SCHEDULE_TYPE_MIN) && (pType <= SCHEDULE_TYPE_MAX) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtScheduleType_0 + pType), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern const char *GetSlopePercentageStr( const UNS_32 pslope_percentage_index )
{
	char *rv;

	// 6/1/2015 ajv : Note that the >= comparison always evaluates to true since
	// pslope_percentage_index is unsigned and STATION_GROUP_SLOPE_PERCENTAGE_MIN is 0. Based on
	// this, the condition could be eliminated. However, if the low-end value ever changes to
	// something other than 0, we want to make sure the condition checks that. Therefore, leave
	// the check in place even though it's (currently) unnecessary.
	if( (pslope_percentage_index >= STATION_GROUP_SLOPE_PERCENTAGE_MIN) && (pslope_percentage_index <= STATION_GROUP_SLOPE_PERCENTAGE_MAX) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtSlopePercentage_0 + pslope_percentage_index), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}


/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in soil type. If the 
 * current language is English, a pointer to the English string is returned. 
 * Similarly, if the current language is Spanish, a pointer to the Spanish 
 * string is returned. 
 * 
 * @param psoil_type The 0-based soil type.
 * 
 * @return char* A pointer to the text string based upon the passed in value and 
 *  	   the current language.
 *  
 * @author 7/2/2012 Adrianusv
 *  
 * @revisions 
 *    7/2/2012 Initial release
 */
extern const char *GetSoilTypeStr( const UNS_32 psoil_type )
{
	char *rv;

	// 6/1/2015 ajv : Note that the >= comparison always evaluates to true since psoil_type is
	// unsigned and STATION_GROUP_SOIL_TYPE_MIN is 0. Based on this, the condition could be
	// eliminated. However, if the low-end value ever changes to something other than 0, we want
	// to make sure the condition checks that. Therefore, leave the check in place even though
	// it's (currently) unnecessary.
	if( (psoil_type >= STATION_GROUP_SOIL_TYPE_MIN) && (psoil_type <= STATION_GROUP_SOIL_TYPE_MAX) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtSoilTypes_0 + psoil_type), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the text string equivalent of the passed in time zone. For
 * instance, if tzPACIFIC_STANDAR_TIME (value 2) is passed in, "Pacific Standard Time" is 
 * returned. If the current language is English, a pointer to the English string is 
 * returned. Similarly, if the current language is Spanish, a pointer to the Spanish string 
 * is returned. 
 * 
 * @param pTimeZone The 0-based time zone.
 * 
 * @return char* A pointer to the text string.
 *  
 * @author 8/23/2011 Adrianusv
 *  
 * @revisions 
 *    2/7/2012 Modified passed in string to be a 32-bit unsigned integer
 */
DLLEXPORT const char *GetTimeZoneStr( const UNS_32 pTimeZone )
{
	char *rv;

	// 6/1/2015 ajv : Note that the >= comparison always evaluates to true since pTimeZone is
	// unsigned and tzHAWAII_STANDARD_TIME is 0. Based on this, the condition could be
	// eliminated. However, if the low-end value ever changes to something other than 0, we want
	// to make sure the condition checks that. Therefore, leave the check in place even though
	// it's (currently) unnecessary.
	if( (pTimeZone >= tzHAWAIIAN_STANDARD_TIME) && (pTimeZone <= tzUS_EASTERN_STANDARD_TIME) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtTimeZones_0 + pTimeZone), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return ( rv  );
}

/* ---------------------------------------------------------- */
/**
 * Returns the text string equivalent of the passed in water units display type.
 * 
 * @param punits The 0-based water units string.
 * 
 * @return char* A pointer to the text string.
 *  
 * @author 06/08/2016 skc
 *  
 * @revisions 
 */
extern const char *GetWaterUnitsStr( const UNS_32 punits )
{
	char *rv;

	if( punits == NETWORK_CONFIG_WATER_UNIT_GALLONS )
	{
		rv = GuiLib_GetTextPtr( GuiStruct_txtGallons_0, 0 );
	}
	else if( punits == NETWORK_CONFIG_WATER_UNIT_HCF )
	{
		rv = GuiLib_GetTextPtr( GuiStruct_txtHCF_0, 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern const char *Get_MoistureSensor_MoistureControlMode_Str( const UNS_32 pcontrol_mode )
{
	char *rv;
	
	// 6/1/2015 ajv : Note that the >= comparison always evaluates to true since the variable is
	// unsigned and the MINIMUM is 0. Based on this, the condition could be eliminated. However,
	// if the MINIMUM ever changes to something other than 0, we want to make sure the condition
	// checks that. Therefore, leave the check in place even though it's (currently)
	// unnecessary.
	
	if( (pcontrol_mode >= MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_MIN) && (pcontrol_mode <= MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_MAX) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtMoisControlMode_0 + pcontrol_mode), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern const char *Get_MoistureSensor_HighTemperatureAction_Str( const UNS_32 paction )
{
	char *rv;
	
	// 10/9/2018 ajv : Note that the >= comparison always evaluates to true since the variable 
	// is unsigned and the MINIMUM is 0. Based on this, the condition could be eliminated. 
	// However, if the MINIMUM ever changes to something other than 0, we want to make sure the 
	// condition checks that. Therefore, leave the check in place even though it's (currently) 
	// unnecessary. 
	
	if( (paction >= MOISTURE_SENSOR_HIGH_TEMP_ACTION_MIN) && (paction <= MOISTURE_SENSOR_HIGH_TEMP_ACTION_MAX) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtMoisControlMode_0 + paction), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern const char *Get_MoistureSensor_LowTemperatureAction_Str( const UNS_32 paction )
{
	char *rv;
	
	// 10/9/2018 ajv : Note that the >= comparison always evaluates to true since the variable 
	// is unsigned and the MINIMUM is 0. Based on this, the condition could be eliminated. 
	// However, if the MINIMUM ever changes to something other than 0, we want to make sure the 
	// condition checks that. Therefore, leave the check in place even though it's (currently) 
	// unnecessary. 
	
	if( (paction >= MOISTURE_SENSOR_LOW_TEMP_ACTION_MIN) && (paction <= MOISTURE_SENSOR_LOW_TEMP_ACTION_MAX) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtMoisControlMode_0 + paction), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern const char *Get_MoistureSensor_HighECAction_Str( const UNS_32 paction )
{
	char *rv;
	
	// 10/9/2018 ajv : Note that the >= comparison always evaluates to true since the variable 
	// is unsigned and the MINIMUM is 0. Based on this, the condition could be eliminated. 
	// However, if the MINIMUM ever changes to something other than 0, we want to make sure the 
	// condition checks that. Therefore, leave the check in place even though it's (currently) 
	// unnecessary. 
	
	if( (paction >= MOISTURE_SENSOR_HIGH_EC_ACTION_MIN) && (paction <= MOISTURE_SENSOR_HIGH_EC_ACTION_MAX) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtMoisControlMode_0 + paction), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern const char *Get_MoistureSensor_LowECAction_Str( const UNS_32 paction )
{
	char *rv;
	
	// 10/9/2018 ajv : Note that the >= comparison always evaluates to true since the variable 
	// is unsigned and the MINIMUM is 0. Based on this, the condition could be eliminated. 
	// However, if the MINIMUM ever changes to something other than 0, we want to make sure the 
	// condition checks that. Therefore, leave the check in place even though it's (currently) 
	// unnecessary. 
	
	if( (paction >= MOISTURE_SENSOR_LOW_EC_ACTION_MIN) && (paction <= MOISTURE_SENSOR_LOW_EC_ACTION_MAX) )
	{
		rv = GuiLib_GetTextPtr( (UNS_16)(GuiStruct_itxtMoisControlMode_0 + paction), 0 );
	}
	else
	{
		rv = UNKNOWN_STR;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifdef _MSC_VER

// 2012.02.23 rmd : So now with CrossWorks version 2.2 they have included
// definitions for the strlcpy and strlcat functions. So we comment out our
// copies of thse functions. By the way I pulled this source code from the
// RedHat Newlib library source.

// 6/23/2014 ajv : However, Visual Studio does not include these definitions.
// Therefore, since this file is shared with the comm_server, allow these to be
// access by the comm server without having to redefine them elsewhere.

/* ---------------------------------------------------------- */
/*
 * Copy src to string dst of size siz.  At most siz-1 characters
 * will be copied.  Always NUL terminates (unless siz == 0).
 * Returns strlen(src); if retval >= siz, truncation occurred.
 */
extern size_t strlcpy( char *dst, const char *src, const size_t siz )
{
	char *d = dst;
	const char *s = src;
	size_t n = siz;

	/* Copy as many bytes as will fit */
	if( n != 0 )
	{
		while( --n != 0 )
		{
			if( (*d++ = *s++) == '\0' )
			{
				break;
			}
		}
	}

	/* Not enough room in dst, add NUL and traverse rest of src */
	if (n == 0)
	{
		if( siz != 0 )
		{
			*d = '\0';		/* NUL-terminate dst */
		}
		while( *s++ )
		{
			;
		}
	}

	return( (size_t)(s - src - 1) );	/* count does not include NUL */
}

/* ---------------------------------------------------------- */
/*
 * Appends src to string dst of size siz (unlike strncat, siz is the
 * full size of dst, not space left).  At most siz-1 characters
 * will be copied.  Always NUL terminates (unless siz <= strlen(dst)).
 * Returns strlen(src) + MIN(siz, strlen(initial dst)).
 * If retval >= siz, truncation occurred.
 */
extern size_t strlcat( char *dst, const char *src, const size_t siz )
{
	// The compiler makes pointers as a 'long int'. size_t is a 'long unsigned int'.
	
	char *d = dst;
	const char *s = src;
	size_t n = siz;
	size_t dlen;

	/* Find the end of dst and adjust bytes left but don't go past end */
	while( (n-- != 0) && (*d != '\0') )
	{
		d++;
	}

	dlen = (size_t)(d - dst);

	n = siz - dlen;

	if( n == 0 )
	{
		return( dlen + strlen(s) );
	}
	while( *s != '\0' )
	{
		if (n != 1)
		{
			*d++ = *s;
			n--;
		}
		s++;
	}
	*d = '\0';

	return( ( dlen + (size_t)(s - src) ) );	/* count does not include NUL */
}

/* ---------------------------------------------------------- */
/**
 * Finds the first occurrence of the substring 'needle' in the string 'haystack'. At most 
 * 'length' bytes is searched. 
 * 
 * @param phaystack The string in which to search 
 * @param pneedle The string to search for
 * @param siz The maximum 'searchee' string length 
 *  
 * @return A pointer to the beginning of the substring, or NULL if the substring is not found.
 */
extern char *strnstr( const char *phaystack, const char *pneedle, size_t siz )
{
	#if 1

		// 8/17/2016 ajv : Newlib's implementation
		const char *max = (phaystack + siz);

		int i;

		// ----------

		if( *phaystack == '\0' )
		{
			return( (*pneedle == '\0') ? ((char *)phaystack) : NULL );
		}

		// ----------

		while( phaystack < max )
		{
			i = 0;

			while( 1 )
			{
				if( pneedle[ i ] == '\0' )
				{
					return( (char *)phaystack );
				}

				if( pneedle[ i ] != phaystack[ i++ ] )
				{
					break;
				}
			}

			phaystack += 1;
		}

		return( (char *)NULL );

	#else

		// 8/17/2016 ajv : Apple's (BSD's) implementation
		char c, sc;

		size_t len;

		if( (c = *pneedle++) != '\0')
		{
			len = strlen(pneedle);

			do
			{
				do
				{
					if ((sc = *phaystack++) == '\0' || siz-- < 1)
					{
						return( NULL );
					}

				} while (sc != c);

				if( len > siz )
				{
					return( NULL );
				}

			} while( strncmp( phaystack, pneedle, len ) != 0 );

			phaystack--;
		}

		return( (char *)phaystack );

	#endif
}

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * strcat but first interpret format string.
 *  
 * @param dest A pointer to the destination string where the formatted string 
 *  		   will be appended.
 * @param pdest_size The size of the destination string. Anything characters 
 *  				 beyond this size will be truncated.
 * @param fmt The format string for sprintf. 
 * @param ... Variables to convert to strings for vsprintf 
 * 
 * @return char* A pointer to the destination string.
 */
extern char *sp_strlcat( char *dest, size_t pdest_size, char* fmt, ... )
{
	#ifndef _MSC_VER

		char		str[ 128 ];   // ONLY UP TO 128 chars or else
	#else

		// 1/16/2015 ajv : Since we use this routine to build SQL statements for the Comm Server, we
		// need to allocate enough space to fit an entire SQL statement.
		char		str[ SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE ];

	#endif

	va_list		vargs;
	
	va_start( vargs, fmt );
	vsnprintf( str, sizeof(str), fmt, vargs );
	
	// 6/1/2015 ajv : Don't forget the va_end.
	va_end( vargs );

	strlcat( dest, str, pdest_size );

	return( dest );
}

/* ---------------------------------------------------------- */
// put pstr into pbuf, remove the left pad, return pbuf
extern char *ShaveLeftPad( char *pbuf, const char *pstr )
{
	UNS_32	i, j;

	strncpy( pbuf, pstr, (strlen(pstr) + 1) );
	
	while( pbuf[0] == 0x20 )
	{
		for( i = 0, j = ((UNS_32)strlen(pbuf) + 1); i < j; ++i)
		{
			pbuf[ i ] = pbuf [ i + 1 ];
		}
	}
	
	return pbuf;
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param pstr 
 * 
 * @return char* 
 *
 * @author 2/20/2013 Adrianusv
 *
 * @revisions (none)
 */
extern char *trim_white_space( char *pstr )
{
	char *end_of_str;

	// Trim leading space
	while( isspace(*pstr) == (true) )
	{
		pstr++;
	}

	// Trim the trailing spaces if the string isn't all spaces
	if( *pstr != 0 )
	{
		// Trim trailing space
		end_of_str = pstr + strlen(pstr) - 1;

		while( (end_of_str > pstr) && (isspace(*end_of_str) == (true)) )
		{
			end_of_str--;
		}

		// Write new null terminator
		*(end_of_str+1) = 0;
	}

	return( pstr );
}

/* ---------------------------------------------------------- */
extern char *RemovePathFromFileName( char *str1 )
{
	// this function assumes that str1 points to a pathname+filename
	// if the last char is a \ it will return a pointer to the terminating null
	// this function assumes there is a terminating null
	//
	// moves through str1 finding the \'s and then pretending the string
	// start just after each occurance
		  
	char *ptr_to_char;
	
	// if there are no \'s will return ptr to original string
	char *just_beyond_last_occurance = str1;

	// in the gcc environment the file names have forward slashes instead of
	// back slashes
	ptr_to_char = strrchr( str1, '/' );  // strrchr finds the last occurance

	if( ptr_to_char != NULL )
	{
		ptr_to_char++;  // hop over it

		just_beyond_last_occurance = ptr_to_char;
	}

	return(  just_beyond_last_occurance );
}

/* ---------------------------------------------------------- */
// put pstr into pbuf, starting at the end remove any spaces by moving the null
// char, return pbuf
extern char *ShaveRightPad_63max( char *pbuf, const char *pstr )
{
	UNS_32	i;
	
	// use a local string buffer to perform length adjustment - use of the local
	// buffer allows calls such as ShaveRightPad_63max( mystr, mystr ) to work.
	char	str_64[ 64 ];

	strcpy( str_64, pstr );
  
	for( i = (strlen(str_64) - 1); i > 0; --i )
	{
		// if we've bumped into a character we're done.
		if( str_64[ i ] != 0x20 )
		{
			break;
		}
		else
		{
			// move the end of the string in one
			str_64[ i ] = 0x00;
		}
	}
	
	strcpy( pbuf, str_64 );
	
	return( pbuf );
}

/* ---------------------------------------------------------- */
extern char *PadLeft( char *pbuf, const UNS_32 psize )
{
	UNS_32	slen, hlen;
	
	char tmp[ 64 ];

	// test if we should be here
	slen = strlen( pbuf );

	if( slen >= psize )
	{
		return( pbuf );  // already bigger than they want
	}
	
	if( slen >= 60 )
	{
		return( pbuf );  // cause our temporary buffer ain't big enough
	}
	
	tmp[0] = 0;
	strcat( tmp, pbuf );  // get a copy of it
	
	slen = strlen( tmp );
	
	hlen = psize - slen;
	
	memset( pbuf, 0x20, hlen );
	
	pbuf[hlen] = 0;
	strcat( pbuf, tmp );
	
	return( pbuf );
}

/* ---------------------------------------------------------- */
extern char *PadRight( char *pbuf, const UNS_32 psize )
{
	UNS_32	i, j;

	j = strlen( pbuf );

	for( i = 0 ; i < (psize - j); ++i )
	{
		*(pbuf + j + i) = ' ';
	}
	
	*(pbuf + j + i) = 0;
	
	return( pbuf );
}

/* ---------------------------------------------------------- */
extern char *CenterDisplayString( char *pbuf, char *ptext )
{
	char lstr_64[ 64 ];

	UNS_32	slen, hlen;

	// in order that we may center it in itself, the result would be the string
	// in buffer is centered in the display width
	lstr_64[ 0 ] = 0;

	strcat( lstr_64, ptext );

	slen = strlen( lstr_64 );

	if ( slen < LINE_LEN )
	{
		hlen = (LINE_LEN - slen) / 2;
		memset( pbuf, 0x20, hlen );

		pbuf[ hlen ] = 0;
		strcat( pbuf, lstr_64 );

		PadRight( pbuf, LINE_LEN );
	}
	else
	{
		strncpy( pbuf, lstr_64, LINE_LEN );
		pbuf[ LINE_LEN ] = 0;
	}

	return( pbuf );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION The caller provides a character buffer starting address. With at least 33
    bytes of space. This function will return a NULL terminates string that represents the
    binary equivalent of the pvalue.

	@CALLER_MUTEX_REQUIREMENTS

	@MEMORY_RESPONSIBILITES

    @EXECUTED Executed within the context of the xxx task.
	
	@RETURN (none)

	@ORIGINAL 2012.03.12 rmd

	@REVISIONS
*/
extern char *format_binary_32( char *pbuf, const void *pvalue )
{
	pbuf[ 32 ] = 0;  // NULL term.

	UNS_32	lvalue_to_format;
	
	lvalue_to_format = *((UNS_32*)pvalue);

	UNS_32	z;
	
	for( z=0; z<32; z++ )
	{
		pbuf[ 31 - z ] = ((lvalue_to_format>>z) & 0x1) ? '1' : '0'; 
	}

	return( pbuf );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION The caller provides a memory block start pointer, a NULL terminated string to search for, and the size
    of the block to search. This function is designed to emulate strstr() behavior on memory blocks instead of NULL
    terminated strings. SR Freewave radio response buffers may contain embedded '\0' characters, which prematurely end
    strstr() seachs. This function continues the search for the specified length.

	@CALLER_MUTEX_REQUIREMENTS

	@MEMORY_RESPONSIBILITES

    @EXECUTED Executed within the context of the xxx task.
	
	@RETURN (none)

	@ORIGINAL 2015.04.10 mpd

	@REVISIONS
*/
extern char* find_string_in_block( const char *block_ptr, const char *string_to_find_ptr, const size_t block_length ) 
{
	char *return_ptr = NULL;
	UNS_32 string_lenght = strlen( string_to_find_ptr );
	BOOL_32 match = false;

	char *p1 = (char*)block_ptr;									// Pointer to memory block 
	char *p1m;														// Moving pointer to memory block
	char *p2 = (char*)string_to_find_ptr;							// Pointer to string to search for
	char *p2m;														// Moving pointer to string to search for
	char *p_last = ( p1 + ( block_length - string_lenght + 1 ) ); 	// The last place the string could start

	// 4/10/2015 mpd : Be sure the string to find is not empty.
	if( string_lenght > 0 ) 
	{
		// 4/10/2015 mpd : Be sure the string within the memory block.
		if( block_length >= string_lenght )
		{
			// 4/10/2015 mpd : Search every possible location until found.
			while( ( p1 <= p_last ) && ( return_ptr == NULL ) )  
			{
				// 4/10/2015 mpd : Set the inner-loop success flag.
				match = true;

				// 4/10/2015 mpd : Set the moving pointers.
				p1m = p1;
				p2m = p2;

				// 4/10/2015 mpd : Loop as long as there is a match and the string char is not NULL.
				while( match && ( *p2m ) )
				{
					if( *p1m != *p2m )
					{
						match = false;
					}
					p1m++;
					p2m++;
				}

				if( match )
				{
					// 4/10/2015 mpd : We reached the end of the string and all the characters matched. Return a pointer 
					// to the start of the match.
					return_ptr = p1;
				}
				else
				{
					// 4/10/2015 mpd : Try the next location.
					p1++;
				}
			}
		}
	}
	else 
	{
		// 4/10/2015 mpd : String length is 0. Return a pointer to the full block as strstr() does.
		return_ptr = p1;
	}

  return( return_ptr );
}



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

