/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_GROUP_BASE_FILE_H
#define _INC_GROUP_BASE_FILE_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cal_list.h"

#include	"cal_string.h"

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

	#define	GROUP_SHOW_3_ROWS_OF_STATIONS		(Pos_Y_StationGrid3 + 46)
	#define	GROUP_SHOW_4_ROWS_OF_STATIONS		(Pos_Y_StationGrid4 + 46)
	#define	GROUP_SHOW_5_ROWS_OF_STATIONS		(Pos_Y_StationGrid5 + 46)
	#define	GROUP_SHOW_6_ROWS_OF_STATIONS		(Pos_Y_StationGrid6 + 46)
	#define	GROUP_SHOW_7_ROWS_OF_STATIONS		(Pos_Y_StationGrid7 + 46)
	#define	GROUP_SHOW_8_ROWS_OF_STATIONS		(Pos_Y_StationGrid8 + 46)
	#define	GROUP_SHOW_9_ROWS_OF_STATIONS		(Pos_Y_StationGrid9 + 46)
	#define	GROUP_SHOW_10_ROWS_OF_STATIONS		(Pos_Y_StationGrid10 + 46)
	#define	GROUP_SHOW_11_ROWS_OF_STATIONS		(Pos_Y_StationGrid11 + 46)

	// ----------

	#define	GROUP_COMBO_BOX_Y_START				(Pos_Y_GroupListTop - 10)

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	GROUP_BASE_DEFINITION_VERSION		(7)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/**
 * All data group definitions that are to be edited <b>MUST</b> have this common
 * header for the structure ... this allows a common set of routines when 
 * editing this part of the structure ... we know what we are dealing with 
 *  
 * @revisions
 * 	3/20/2012 Added change bits to track whether changes have been distributed 
 */
typedef struct
{
	// The list support MIST_DLINK_TYPE must occur FIRST within this structure. That way we can
	// move through a list without knowing what the structure type is of the list elements.
	MIST_DLINK_TYPE		list_support;  // (3 pointers)


	// We track how many of this type of group have been created. We use this to generate the
	// next group identity number to use when adding a new group to the list. We may also use
	// this to create a unique default label string. When a new group is added to the list
	// EVERYBODY in the list has this value updated. All members in the list contain the same
	// value.
	UNS_32				number_of_groups_ever_created;


	// We use this number as a group identity. This value will be UNIQUE to this group in this
	// list. Each list always starts with one item on the list with a GID number of 1. It takes
	// on the value of number_of_groups_ever_created when a group is added to the list. This IS
	// the value put into the station structure that ties the station to this group.
	UNS_32				group_identity_number;


	char				description[ NUMBER_OF_CHARS_IN_A_NAME ];


	// 3/25/2013 ajv : Flag which indicates whether the group has been deleted
	// or not. This is necessary for syncing so that all controllers and
	// centrals will be notified that the group has been deleted. If set TRUE,
	// the group does not appear in the list of groups. Although this means
	// groups will never actually be removed from the file system, we currently
	// don't have a necessity to do so. If the need every arises, we can write a
	// clean-up routine to remove any groups where this flag is set TRUE.
	BOOL_32				deleted;

} GROUP_BASE_DEFINITION_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern BOOL_32	g_GROUP_creating_new;

extern UNS_32	g_GROUP_ID;

extern UNS_32	g_GROUP_list_item_index;

extern BOOL_32	g_GROUP_deleted__pending_transmission;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern UNS_32 nm_GROUP_get_group_ID( const void *pgroup );

extern char *nm_GROUP_get_name( const void *pgroup );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void *nm_GROUP_create_new_group( const MIST_LIST_HDR_TYPE_PTR const plist_hdr_ptr, char* pdefault_name, void (*pset_default_values_func_ptr)(void *const, const BOOL_32), const size_t pitem_size, const BOOL_32 pgroups_exist_bool, void *pnext_list_item );



extern BOOL_32 nm_GROUP_find_this_group_in_list( const MIST_LIST_HDR_TYPE_PTR plist_hdr_ptr, const void *const pgroup_to_find, void **answer_ptr );


extern void *nm_GROUP_get_ptr_to_group_at_this_location_in_list( const MIST_LIST_HDR_TYPE_PTR const plist_hdr_ptr, UNS_32 pindex_0 );


extern void *nm_GROUP_get_ptr_to_group_with_this_GID( const MIST_LIST_HDR_TYPE_PTR const plist_hdr_ptr, const UNS_32 pgroup_ID, const BOOL_32 psuppress_group_not_found_alert );

extern UNS_32 nm_GROUP_get_index_for_group_with_this_GID( const MIST_LIST_HDR_TYPE_PTR const plist_hdr_ptr, const UNS_32 pgroup_ID );



extern void GROUP_process_show_keyboard( const UNS_32 px_coordinate, const UNS_32 py_coordinate );


extern void nm_GROUP_create_new_group_from_UI( void *(*nm_create_new_group_func_ptr)(void), void *(*copy_group_into_guivars_func_ptr)(const UNS_32 pindex_0) );



extern void nm_GROUP_load_common_guivars( const void *const pgroup );

extern void FDTO_GROUP_return_to_menu( BOOL_32 *pediting_group, void *(*store_changes_from_guivar_func_ptr)(void) );

extern void FDTO_GROUP_draw_menu( const BOOL_32 pcomplete_redraw, BOOL_32 *pediting_group, const UNS_32 plist_count, const UNS_32 pstructure_to_draw, void *load_group_name_func_ptr, void *(*copy_group_into_guivars_func_ptr)(const UNS_32 pindex_0), const BOOL_32 pshow_add_new_item );

extern void GROUP_process_NEXT_and_PREV( const UNS_32 pkeycode, const UNS_32 plist_count, void *(*pextract_and_store_changes_funct_ptr)(void), void *(*pget_group_at_idx_func_ptr)(const UNS_32 pindex_0), void *(*copy_group_into_guivars_func_ptr)(const UNS_32 pindex_0) );

extern void GROUP_process_menu( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event, BOOL_32 *pediting_group, const UNS_32 plist_count, void *(*process_group_func_ptr)(const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event), void *(*add_group_func_ptr)(void), void *(*pget_group_at_idx_func_ptr)(const UNS_32 pindex_0), void *(*copy_group_into_guivars_func_ptr)(const UNS_32 pindex_0) );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

