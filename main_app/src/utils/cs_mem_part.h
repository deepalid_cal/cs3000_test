/*  file = mem_partioning.h  03.05.2003  rmd  */
/* ---------------------------------- */

#ifndef _INC_MEM_PARTIONING_H
#define _INC_MEM_PARTIONING_H


/* ---------------------------------- */
/* ---------------------------------- */

#include	"lpc_types.h"




/* ---------------------------------- */
/* ---------------------------------- */

// Max. number of memory partitions ... MUST be >= 2. This this define is used in mem.c to
// define the statistical arrays regarding allocations.
#define OS_MEM_MAX_PART		(11)

typedef struct {

	unsigned long	block_size;

	unsigned long	block_count;
	
} PARTITION_DEFINITION;


extern const PARTITION_DEFINITION partitions[ OS_MEM_MAX_PART ];


/* ---------------------------------- */
/* ---------------------------------- */


#define OS_MEM_NO_ERR			0000
#define OS_MEM_NO_FREE_BLKS		0110
#define OS_MEM_INVALID_BLKS		0111
#define OS_MEM_INVALID_SIZE		0112
#define OS_MEM_FULL				0113


/* ---------------------------------- */
/* ---------------------------------- */


// 2/4/2014 rmd : The maximum code image size we can ever have. Well not really. It is only
// limited by the size of this block. Which I have set at about twice our present code size.
// If this needs to grow one needs to update our memory use spread sheet in the main source
// root directory, and update the Crossworks project to reflect the needed heap. This define
// is to be used by the code download process to 'ask' for this block. With this set to
// 1433600 bytes we still have approximately 2Mbyte of available SDRAM space! And that's
// pretty darn nice to have at this stage in our development.
//
// 4/22/2015 rmd : This define sets the memory pool largest block size. Which is used during
// code receipts from the commserver. And potentially program data receipt from the
// commserver (if sending complete program data for a large chain). And to send program data
// to the commserver. We ALWAYS take this block when sending program data to the commserver
// because we do not know in advance how much data is pending to be sent. We decided this is
// an okay strategy.
#define MAXIMUM_CODE_IMAGE_AND_INBOUND_OR_OUTBOUND_COMMSERVER_MESSAGE_SIZE      (1433600)


/*
*********************************************************************************************************
*                                     MEMORY PARTITION DATA STRUCTURES
*********************************************************************************************************
*/

typedef struct {                       /* MEMORY CONTROL BLOCK                                         */
    void   *OSMemAddr;                 /* Pointer to beginning of memory partition                     */
    void   *OSMemFreeList;             /* Pointer to the beginning of the free list of memory blocks   */
    UNS_32  OSMemBlkSize;              /* Size (in bytes) of each block of memory                      */
    UNS_32  OSMemNBlks;                /* Total number of blocks in this partition                     */
    UNS_32  OSMemNFree;                /* Number of memory blocks remaining in this partition          */
} OS_MEM;

extern OS_MEM OSMemTbl[ OS_MEM_MAX_PART ];	/* Storage for memory partition manager             */


typedef struct {
    void   *OSAddr;                    /* Pointer to the beginning address of the memory partition     */
    void   *OSFreeList;                /* Pointer to the beginning of the free list of memory blocks   */
    UNS_32  OSBlkSize;                 /* Size (in bytes) of each memory block                         */
    UNS_32  OSNBlks;                   /* Total number of blocks in the partition                      */
    UNS_32  OSNFree;                   /* Number of memory blocks free                                 */
    UNS_32  OSNUsed;                   /* Number of memory blocks used                                 */
} OS_MEM_DATA;





/*
*********************************************************************************************************
*                                           MEMORY MANAGEMENT
*********************************************************************************************************
*/


void *OSMemGet( OS_MEM *pmem, UNS_32 *err );

UNS_8 OSMemPut( unsigned short pindex, void *pblk );

void init_mem_partitioning( void );


/* ---------------------------------- */
/* ---------------------------------- */


#endif


/* ---------------------------------- */
/* ---------------------------------- */
/* ---------------------------------- */
/* ---------------------------------- */

