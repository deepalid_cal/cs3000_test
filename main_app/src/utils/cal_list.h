// FILE = CAL_LIST.H

#ifndef _INC_CAL_LIST_H
#define _INC_CAL_LIST_H

/******************* Includes *****************/

#include	"lpc_types.h"




/******************** Defines ******************/ 

// provides list debugging capabilities found in cal_list.c and .h
//
// Change it to a 0 OR comment out the whole line (either way works)
#define LIST_DEBUG 1

/************** List Structures ***************/

/**** List Header Structure ****/

typedef struct
{
	void				*phead;		// Pointer to head of list

	void				*ptail;		// Pointer to tail of list

	UNS_32				count;		// Count of elements in list

	UNS_32				offset;		// Offset of the DLINK structure in each element.
	
	#if defined(LIST_DEBUG) && LIST_DEBUG
	BOOL_32			InUse;		// Flag to check for re-entrancy.
	#endif
	
} MIST_LIST_HDR_TYPE, *MIST_LIST_HDR_TYPE_PTR;


/**** Data Link Structure ****/
typedef struct
{
	void				*pPrev;			// Pointer to previous data element

	void				*pNext;			// Pointer to next data element

	MIST_LIST_HDR_TYPE	*pListHdr;		// Pointer to list header the element is on

} MIST_DLINK_TYPE, *MIST_DLINK_TYPE_PTR;

/**** List Functions ****/

void nm_ListInit( MIST_LIST_HDR_TYPE_PTR plisthdr, int offset );

INT_32 nm_ListInsert( MIST_LIST_HDR_TYPE_PTR plisthdr, void *new_elem, void *old_elem );

INT_32 nm_ListInsertHead( MIST_LIST_HDR_TYPE_PTR plisthdr, void *new_elem );

INT_32 nm_ListInsertTail( MIST_LIST_HDR_TYPE_PTR plisthdr, void *new_elem );





#if defined(LIST_DEBUG) && LIST_DEBUG

// Define macros to replace call with debug call in application file.
//
// 3/21/2017 rmd : These defines used to contain the 'remove path from filename' function
// with __FILE__ as an argument. Well that has turned out to be a very silly thing to do as
// it forces every use of the define to needlessly execute that function. It is only
// necessary to execute that function if we indeed have an error and we are attempting to
// embed the file name into an alert line.
#define nm_ListRemove(p,e)     nm_ListRemove_debug( (p), (e), __FILE__, __LINE__ )

#define nm_ListRemoveHead(p)   nm_ListRemoveHead_debug( (p), __FILE__, __LINE__ )

#define nm_ListRemoveTail(p)   nm_ListRemoveTail_debug( (p), __FILE__, __LINE__ )


INT_32 nm_ListRemove_debug( MIST_LIST_HDR_TYPE_PTR plisthdr, void *element, char *file, int line );

void *nm_ListRemoveHead_debug( MIST_LIST_HDR_TYPE_PTR plisthdr, char *file, int line );

void *nm_ListRemoveTail_debug( MIST_LIST_HDR_TYPE_PTR plisthdr, char *file, int line );


#else



INT_32 nm_ListRemove( MIST_LIST_HDR_TYPE_PTR plisthdr, void *element );

void *nm_ListRemoveHead( MIST_LIST_HDR_TYPE_PTR plisthdr );

void *nm_ListRemoveTail( MIST_LIST_HDR_TYPE_PTR plisthdr );


#endif  // of if making LIST_DEBUG version





void *nm_ListGetFirst( MIST_LIST_HDR_TYPE_PTR plisthdr );

void *nm_ListGetLast( MIST_LIST_HDR_TYPE_PTR plisthdr );

void *nm_ListGetNext( MIST_LIST_HDR_TYPE_PTR plisthdr, void *element );

void *nm_ListGetPrev( MIST_LIST_HDR_TYPE_PTR plisthdr, void *element );


BOOL_32 nm_OnList( const MIST_LIST_HDR_TYPE_PTR listhdr, const void *const element );



#define nm_ListGetCount(listhdr) ((listhdr)->count)


#endif /* _INC_CAL_LIST_H */

