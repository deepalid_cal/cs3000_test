/* +++Date last modified: 05-Jul-1997 */
/*
**  This is a copyrighted work which is functionally identical to work
**  originally published in Micro Cornucopia magazine (issue #52, March-April,
**  1990) and is freely licensed by the author, Walter Bright, for any use.
*/

/*_ mem.c   Fri Jan 26 1990   Modified by: Walter Bright */
/* Memory management package                    */


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"cs_common.h"

#include	"cs_mem.h"

#include	"cs_mem_part.h"

#include	"alerts.h"

#include	"wdt_and_powerfail.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define MEM_SHOW_ALLOCS_AND_FREES 0

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


UNS_32	mem_inited = 0;	// != 0 if initialized


UNS_32	mem_count;		// # of allocs that haven't been free'd

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#if defined(MEM_DEBUG) && MEM_DEBUG

// PART OF FOR MEM_DEBUG 

#define BEFOREVAL 0x12345678  /* value to detect underrun   */
#define AFTERVAL  0x87654321  /* value to detect overrun    */


UNS_32	mem_maxalloc;     /* max # of bytes allocated         */

UNS_32	mem_numalloc;     /* current # of bytes allocated           */

// variables for examining what the allocation by size distribution looks like
UNS_32	mem_current_allocations_of[ OS_MEM_MAX_PART ];

UNS_32	mem_max_allocations_of[ OS_MEM_MAX_PART ];


// PART OF FOR MEM_DEBUG 

/* The following should be selected to give maximum probability that    */
/* pointers loaded with these values will cause an obvious crash. On    */
/* Unix machines, a large value will cause a segment fault.       */
/* MALLOCVAL is the value to set malloc'd data to.                */

#define MALLOCVAL   0xDD
#define BADVAL      0xEE


// PART OF FOR MEM_DEBUG 

// this variable is so we can get at the start of the linked list of allocations
// compiler intializes with NULL pointers but we will also explicitly do so in init_mem
MEM_DEBUG_STRUCT mem_alloclist;


// Convert from a void *to a mem_debug struct.

#define mem_ptrtodl(p)  ((MEM_DEBUG_STRUCT*) ((char *)p - MEM_SIZE_OF_DEBUG_HEADER))

// Convert from a mem_debug struct to a mem_ptr.

#define mem_dltoptr(dl) ((void *) &((dl)->data[0]))


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void init_mem_debug( void ) {

unsigned short i;

	// This function MUST be called very early on in main. BEFORE the scheduler has been started. There is no protection from
	// pre-emption. And none is needed. As the OS scheduler is not running.

	if( mem_inited == 0 )
	{
		mem_count = 0;

		// the debug stuff
		mem_numalloc = 0;
		mem_maxalloc = 0;
		mem_alloclist.next = NULL;
		
		// 08.05.05 rmd FIX corrected bug here ... was 20 not OS_MEM_MAX_PART
		for ( i=0; i<OS_MEM_MAX_PART; i++ )  {
			
			mem_current_allocations_of[ i ] = 0;
			
			mem_max_allocations_of[ i ] = 0;

		}
		// end the debug stuff

		mem_inited = 1;
	}
	else
	{
		// CANNOT make any alert lines as the call to create the ALERT line wants to use the alerts pile MUTEX.
		// Which is not yet created. Because mutex creation involves an allocation from this memory pool. So the initialization of
		// the pool MUST be error free. And no alerts to tell us about a problem.
		for(;;);
	}
}

/* ---------------------------------------------------------- */
static void __adjust_block_size( UNS_32 *n )
{
	// If its not an even number of bytes need to up the count by one this is to avoid the
	// address error we would get when we write the AFTER_VAL - if we do it directly... using a
	// memcpy would avoid this problem and is surely the best way to handle this. We would also
	// get an error on the read but we never get that far.
	while ( ( *n % 4) != 0 ) (*n)++;

	// 10/29/2012 rmd : And add the debug support.
	*n += ( MEM_SIZE_OF_DEBUG_HEADER + sizeof(AFTERVAL) );
}

/* ---------------------------------------------------------- */
static void *mem_calloc_debug( UNS_32 palloc_size, char *fil, UNS_32 lin )
{
	MEM_DEBUG_STRUCT	*dl;
	
	UNS_32				i, err, ul;
	
	void				*rv;

	rv = dl = NULL; 	// establish the no memory condition
	
	__adjust_block_size( &palloc_size );

	// Based on the size, we know which partition to get the block from however we want to
	// ripple up to the next level if that partition is full.
	for( i=0; i<OS_MEM_MAX_PART; i++ )
	{
		if( partitions[ i ].block_size >= palloc_size ) {
		
			calsense_mem_ENTER_CRITICAL();

			dl = (MEM_DEBUG_STRUCT*) OSMemGet( &OSMemTbl[ i ], &err );
			
			// We guard against a partition running out of blocks...if that were to happen the code is written such that we would
			// actually try to find a block in the next block sized partition...if it does we need to change the alloc_size to reflect
			// this as we assign alloc_size to the dl->nbytes which is used to determine which partition to return the block to and also
			// we use alloc_size when tracking the mem_current_allocations_of.

			// Perform ALL the post work while the scheduler is suspended. It turns out some of the DEBUG tracking info will become
			// corrupt if not inside the protection from re-entry.
			if( (err == OS_MEM_NO_ERR) && (dl != NULL) )
			{
				// Update alloc size to reflect the actual block we obtained.
				palloc_size = partitions[ i ].block_size;
	
				// update the number of allocations by size
				mem_current_allocations_of[ i ] ++;
						
				if ( mem_current_allocations_of[ i ] > mem_max_allocations_of[ i ] ) {
				
					mem_max_allocations_of[ i ] = mem_current_allocations_of[ i ];
				
				}
				
				// Some of the following assignments would generate an ADDRESS ERROR in an external 32-bit
				// bus environment if the block sizes in the partition were not a multiple of the native data size.
				
				// 3/21/2017 rmd : DO NOT add the 'remove path from filename' function to modify fil here.
				// If you did that every single dynamic grab would be required to execute that function. And
				// that is a performance penalty. Only execute that function when there is an error.
				dl->file = fil;

				dl->line = lin;
			
				dl->nbytes = palloc_size;
				
				dl->beforeval = BEFOREVAL;
			
				// Memcpy is one way to deal with odd number of bytes requested. We also make our partition with a size multiple of 4.
				// Memcpy is safest way to go. Though a direct setting of the AFTERVAL should work too.
				ul = AFTERVAL;
				memcpy( &dl->data[ palloc_size - MEM_SIZE_OF_DEBUG_HEADER - sizeof(AFTERVAL) ], &ul, sizeof( ul ) );
				
			
				// Add dl to head of the debug allocation list.
				dl->next = mem_alloclist.next;
				dl->prev = &mem_alloclist;
			
				mem_alloclist.next = dl;
			
				if ( dl->next != NULL ) ((MEM_DEBUG_STRUCT*)dl->next)->prev = dl;
			
			
				mem_count++;
			
				mem_numalloc += dl->nbytes;
			
				if ( mem_numalloc > mem_maxalloc ) mem_maxalloc = mem_numalloc;
			
				rv = mem_dltoptr(dl);
				
				// I wish the entry and exit of the critcial section were cleaner BUT it is complicated by the highly desirable need of
				// creation of alert lines. So the relaxed coding standard you see here. Though somewhat hard to follow it is the best I
				// could come up with.
				calsense_mem_EXIT_CRITICAL();

				// we got our assignment ...get out of the loop
				break;
			}

			calsense_mem_EXIT_CRITICAL();

			// ----------			

			// 10/23/2012 rmd : NOTICE the alert lines are outside the SCHEDULER SUSPENDED section.
			if ( err == OS_MEM_NO_FREE_BLKS )
			{
				// Remember...DO NOT attempt alert lines while the scheudler is suspended. Alerts take and give a mutex and that can cause
				// the OS to crash is we use those OS API calls while the scheduler is suspended.
				Alert_Message_va( "Partition %u Out of memory (rqstd %u bytes) : %s %u", (i+1), palloc_size, RemovePathFromFileName(fil), lin );
			}
			else
			if( dl == NULL )
			{
				// This should NEVER happen. It mean we got back a successful err response from the OSMemGet function yet the pointer was a
				// NULL. Fatal error.
				Alert_Message_va( "OSMemGet returned success with NULL ptr? : %s %u", RemovePathFromFileName(fil), lin );

				FREEZE_and_RESTART_APP;
			}

			// ----------			

		}  // Of while looking for an available block.
	}

	if( dl == NULL )
	{
		// This happens if either the request is too big (beyond our largest block size). Or we are out of memory!
		if( palloc_size > partitions[ OS_MEM_MAX_PART - 1 ].block_size )
		{
			Alert_Message_va( "MEM REQUEST TOO BIG : %u bytes : %s, %u", palloc_size, RemovePathFromFileName(fil), lin );
		}
		else
		{
			//Alert_Message_va( "NO MEMORY : %u used in %u allocs : %s, %u", mem_numalloc, mem_count, fil, lin );
			Alert_Message_va( "NO MEMORY : rqstd %u bytes : %s, %u", palloc_size, RemovePathFromFileName(fil), lin );
		}
		
		FREEZE_and_RESTART_APP;
	}
	else
	{
		#if (MEM_SHOW_ALLOCS_AND_FREES == 1)
			Alert_Message_va( "Malloc : %ld bytes : %s, %d", palloc_size, RemovePathFromFileName(fil), lin );
		#endif
	}

	return( rv );
}

/* ---------------------------------------------------------- */
void *mem_malloc_debug( size_t n, char *fil, UNS_32 lin )
{
	void	*rv;
	
	rv = mem_calloc_debug( n, fil, lin );

	if ( rv != NULL )
	{
		memset( rv, MALLOCVAL, n );
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Test for partition availability. If available gets the block, assigns block
    start to block_ptr and returns (true). If returns (false) user should not use the
    block_ptr as it will be NULL.
	
	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED (NA from a variety of calling tasks)
	
	@RETURN (true) if available, (false) if not

	@ORIGINAL 2012.06.29 rmd

	@REVISIONS (none)
*/
extern BOOL_32 mem_oabia( const UNS_32 palloc_size, void **block_ptr, char *fil, UNS_32 lin )
{
	UNS_32				i;
	
	BOOL_32				rv;

	// 6/29/2012 rmd : Indicate one is not available.
	rv = (false);
	
	*block_ptr = NULL;

	// ----------

	// 10/29/2012 rmd : As does the mem_malloc function we need to adjust. But work with our own
	// adjusted size so that when we call mem_malloc we don't repeat the request adjustment.
	UNS_32	lsize;

	lsize = palloc_size;
	
	__adjust_block_size( &lsize );
	
	// ----------

	// 10/29/2012 rmd : Freeze the memory subsystem from other tasks acquiring blocks. So that
	// in between when we think one is available to actually securing the block for us another
	// (higher priority task) doesn't take it.
	calsense_mem_ENTER_CRITICAL();

	// ----------

	// Based on the size, we know which partition to get the block from however we want to
	// ripple up to the next level if that partition is full.
	for( i=0; i<OS_MEM_MAX_PART; i++ )
	{
		if( partitions[ i ].block_size >= lsize )
		{
			if( OSMemTbl[ i ].OSMemNFree > 0 )
			{
				rv = (true);
				
				*block_ptr = mem_malloc_debug( palloc_size, fil, lin );

				break;
			}
		}
	}

	// ----------

	calsense_mem_EXIT_CRITICAL();

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
void mem_free_debug( void *ptr, char *fil, UNS_32 lin )
{
	MEM_DEBUG_STRUCT *dl;
	
	unsigned short i;
	
	unsigned long after_val;
	
	unsigned char err;

	if ( ptr == NULL )
	{
		Alert_Message_va( "MEM_FREE: freeing null ptr : %s, %d", RemovePathFromFileName(fil), lin );
		
		FREEZE_and_RESTART_APP;
	}

	if ( mem_count <= 0 )
	{
		Alert_Message_va( "MEM_FREE : freeing with no count : %s, %d", RemovePathFromFileName(fil), lin );

		FREEZE_and_RESTART_APP;
	}


	dl = mem_ptrtodl( ptr );

	if ( dl->beforeval != BEFOREVAL )
	{
		Alert_Message_va( "MEM_FREE : pointer under run : %s, %d", RemovePathFromFileName(fil), lin );

		FREEZE_and_RESTART_APP;
	}


	memcpy( &after_val, &dl->data[ dl->nbytes - MEM_SIZE_OF_DEBUG_HEADER - sizeof(AFTERVAL) ], sizeof( unsigned long ) );
	
	if ( after_val != AFTERVAL )
	{
		Alert_Message_va( "MEM_FREE : pointer over run : %s, %d", RemovePathFromFileName(fil), lin );

		FREEZE_and_RESTART_APP;
	}
	
	if ( dl->nbytes > mem_numalloc )
	{
		Alert_Message_va( "MEM_FREE : no more to release : %s, %d", RemovePathFromFileName(fil), lin );

		FREEZE_and_RESTART_APP;
	}


	// AFTER RELEASE VIA OSMemPut the data in the dl stucture is no longer guaranteed
	// to be intact...we stick a pointer in there...therfore these operations must
	// be performed before returning the block to the pool of available blocks
	
	#if (MEM_SHOW_ALLOCS_AND_FREES == 1)
		Alert_Message_va( "MEM_FREE : %ld bytes : %s, %d", dl->nbytes, RemovePathFromFileName(fil), lin );
	#endif

	// Safe not to suspend till here. Protect against pre-emption while we actually jigger the block back into the free list.

	calsense_mem_ENTER_CRITICAL();

	// Remove dl from DEBUG linked list.
	if ( dl->prev )
	{
		((MEM_DEBUG_STRUCT*)dl->prev)->next = dl->next;
	}

	if ( dl->next )
	{
		((MEM_DEBUG_STRUCT*)dl->next)->prev = dl->prev;
	}

	mem_numalloc -= dl->nbytes;

	mem_count--;

	// Stomp on the about to be freed storage to help detect references after the storage was freed.
	// This is very tricky to do when in combination with the fixed memory block partition
	// scheme...remember the freed memory is now part of a linked list of =available blocks...
	// if we stomp on the pointer in the beginning of the block we'll blow up
	//
	// Conclusion is we can stomp on it but only the user data area (plus the AFTERVAL area).
	memset( &dl->data, BADVAL, dl->nbytes - MEM_SIZE_OF_DEBUG_HEADER );

	// Update the number of allocations by size.
	for ( i=0; i<OS_MEM_MAX_PART; i++ )
	{
		if ( dl->nbytes == partitions[ i ].block_size )
		{
			// NOW ACTUALLY RETURN THE BLOCK TO THE POOL OF FREE BLOCKS OR FREE IT
			err = OSMemPut( i, (void *)dl );

			if ( err == OS_MEM_NO_ERR )
			{
				mem_current_allocations_of[ i ]--;

				break;
			}
			else
			{
				Alert_Message_va( "MEM_FREE : OSMemPut error : %s, %d", RemovePathFromFileName(fil), lin );

				FREEZE_and_RESTART_APP;
			}
		}
	}

	calsense_mem_EXIT_CRITICAL();
}

/* ---------------------------------------------------------- */
void mem_show_allocations( void )
{
	#define ALLOCS_TO_SHOW	(20)
	
	MEM_DEBUG_STRUCT	*dl;
	
	char				str_64[ ALLOCS_TO_SHOW ][ 64 ];
	
	DATA_HANDLE			ldh;
	
	UNS_32				i;
	
	for( i=0, dl=(MEM_DEBUG_STRUCT*)mem_alloclist.next; ((dl != NULL) && (i<ALLOCS_TO_SHOW)); dl = (MEM_DEBUG_STRUCT*)dl->next, i++ )
	{
		snprintf( str_64[ i ], sizeof(str_64[ i ]), "size %lu : %s , %lu", dl->nbytes, RemovePathFromFileName(dl->file), dl->line );
		
		strlcat( str_64[ i ], "\n\r", sizeof(str_64[ i ]) );
	}

	for( i=0; i<ALLOCS_TO_SHOW; i++ )
	{
		ldh.dptr = (UNS_8*)str_64[ i ];

		ldh.dlen = strlen( str_64[ i ] );
		
		AddCopyOfBlockToXmitList( UPORT_RRE, ldh, NULL, SERIAL_DRIVER_HANDLING_SENDING_UNPACKETIZED_DATA, SERIAL_DRIVER_POST_NEW_TX_BLK_AVAIL );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#else   // of if PART OF FOR MEM_DEBUG 


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// NOT PART OF FOR MEM_DEBUG 


NOT USED. AND NOT TESTED.



void init_mem( void )
{
	if( mem_inited == 0 )
	{
		mem_count = 0;

		mem_inited++;
	}
	else
	{
		// CANNOT make any alert lines as the call to create the ALERT line wants to use the alerts pile MUTEX.
		// Which is not yet created. Because its creation involves an allocation from this memory pool. So the
		// initialization of the pool MUST be error free. And no alerts to tell us about a problem.
		for(;;);
	}
}

void *mem_malloc( size_t numbytes ) {

void	*rv;

	if ( numbytes == 0 ) {
		
		rv = NULL;

	} else {
	
		while ( 1 ) {
			
			rv = malloc( numbytes );   what is this malloc!!!! I thought we didnt use malloc at all;

			if ( rv == NULL ) {
	
				if ( mem_exception() ) continue;
	
			} else {
	
				mem_count++;
				break;
	
			}
	
		}
	
	}

	return( rv );
}

/* ---------------------------------------------------------- */
void *mem_calloc( size_t numbytes ) {

void 	*rv;

	if ( numbytes == 0 ) {

		rv = NULL;

	} else {
		
		while ( 1 ) {
			
			rv = calloc(numbytes,1);
			
			if( rv == NULL ) {
				
				if ( mem_exception() )
					continue;
			
			} else {
				
				mem_count++;
				break;
	
			}
		
		}
	
	}

	return( rv );
}

/* ---------------------------------------------------------- */
void mem_free( void *ptr ) {

	if ( ptr != NULL ) {

		if ( mem_count == 0 ) TraceStringFatal( "mem_count == 0" );

		mem_count--;

		free( ptr );

	}

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /* MEM_DEBUG */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


/*
void mem_test_for_stack_overrun( void ) {

unsigned short i;


	for ( i=0; i<16; i++ ) {
		
		if ( stack_test_space[ i ] != i ) {
		
			Alert_Message( "stack overrun!" );
			
		}
			
	}
	
}

*/

/*
void mem_term( void ) {

	if ( mem_inited ) {
#if defined(MEM_DEBUG) && MEM_DEBUG
		register struct mem_debug *dl;

		for ( dl = mem_alloclist.next; dl; dl = dl->next ) {
			fprintf(ferr,"Unfreed pointer: ");
			mem_printdl(dl);
		}

//      fprintf(ferr,"Max amount ever allocated == %ld bytes\n", mem_maxalloc);

#else
		if ( mem_count )
			fprintf(ferr,"%d unfreed items\n",mem_count);
		if ( mem_scount )
			fprintf(ferr,"%d unfreed s items\n",mem_scount);
#endif

		assert(mem_count == 0 && mem_scount == 0);
		mem_inited = 0;

	}

}
*/
/*******************
 * Debug version of mem_realloc().
 */

/*
void *mem_realloc_debug( void *oldp, 
  size_t n, char *fil, unsigned short lin
  )  {
	
void *p;
MEM_DEBUG_STRUCT *dl;

	if ( n == 0 ) {
		mem_free_debug(oldp,fil,lin);
		p = NULL;
	}
	else if ( oldp == NULL )
		p = mem_malloc_debug(n,fil,lin);
	else {
		p = mem_malloc_debug(n,fil,lin);
		if ( p != NULL ) {
			dl = mem_ptrtodl(oldp);
			if ( dl->nbytes < n )
				n = dl->nbytes;
			memcpy(p,oldp,n);
			mem_free_debug(oldp,fil,lin);
		}
	}
	return p;
}
*/

/***************************/

/*
void mem_check( void ) {

register MEM_DEBUG_STRUCT *dl;

	for ( dl = (MEM_DEBUG_STRUCT*)mem_alloclist.next; dl != NULL; dl = (MEM_DEBUG_STRUCT*)dl->next ) {
		
		mem_checkptr( mem_dltoptr( dl ) );

	}
	
}
*/

/***************************/

/*
void mem_checkptr( register void *p )  {

register MEM_DEBUG_STRUCT *dl;
	
	for ( dl = mem_alloclist.next; dl != NULL; dl = dl->next ) {
		if ( p >= (void *) &(dl->data[0]) &&
			 p < (void *)((char *)dl + sizeof(struct mem_debug)-1 + dl->nbytes) )
			goto L1;
	}
	assert(0);

	L1:
	dl = mem_ptrtodl(p);
	if ( dl->beforeval != BEFOREVAL ) {
#if LPTR
		fprintf(ferr,"Pointer x%lx underrun\n",p);
#else
		fprintf(ferr,"Pointer x%x underrun\n",p);
#endif
		goto err2;
	}
#if SUN || SUN386 // Bus error if we read a long from an odd address
	if ( memcmp(&dl->data[dl->nbytes],&afterval,sizeof(AFTERVAL)) != 0 )
#else
	if ( *(long *) &dl->data[dl->nbytes] != AFTERVAL )
#endif
	{
#if LPTR
		fprintf(ferr,"Pointer x%lx overrun\n",p);
#else
		fprintf(ferr,"Pointer x%x overrun\n",p);
#endif
		goto err2;
	}
	return;

	err2:
	mem_printdl(dl);
	assert(0);
}

*/


/*
void 
*mem_realloc(oldmem_ptr,newnumbytes) void 
*oldmem_ptr; size_t newnumbytes; 
{   void *p;

	if ( oldmem_ptr == NULL )
		p = mem_malloc(newnumbytes);
	else if ( newnumbytes == 0 ) {
		mem_free(oldmem_ptr);
		p = NULL;
	}
	else {
		do
			p = realloc(oldmem_ptr,newnumbytes);
		while ( p == NULL && mem_exception() );
	}

	return p;

}
*/


