/*  file = mem_partioning.c 03/04/2003   rmd  */
/* ---------------------------------- */
/* ---------------------------------- */

// 6/18/2014 ajv : Required for malloc
#include	<stdlib.h>

#include	"cs_mem_part.h"

#include	"cs_common.h"

#include	"cs_mem.h"

#include	"lpc_types.h"

#include	"wdt_and_powerfail.h"

#include	"irrigation_system.h"




/* ---------------------------------- */
/* ---------------------------------- */

// ----------

// define structure of the partitions...used when setting up the partitions and
// throughout the process of using and returning a block of memory...currently
// uses 343,392 bytes of heap the nice thing we know is that is the absolute
// bound...this progam uses that much heap...period.
//
//  2,305,664 BYTES total bytes of heap represented here
// 
// MUST have a minimum of 2 blocks per partition else won't create
//
// WARNING: All block SIZES must be an even multiple of 4 bytes for the Atmel
//  		UC3A/B architecture!! FOR ALL ARCHITECTURES I WOULD CONSIDER THIS
//  		NORMAL PRACTICE.
//
// NOTE: if you want a request to use a particular partition remember the
//  	 overhead of the memory scheme is 28 bytes ... so add 32 to the
//  	 partition block size and you'll see the expected results
const PARTITION_DEFINITION partitions[ OS_MEM_MAX_PART ] =
{

//   SIZE   QUANTITY  

// 2011.10.20 rmd : Why 1024 blocks? Well there is a need for some small number of blocks
// for things like timers and mutexes that FreeRTOS is being asked to make. However the BIG
// user of this partition is the irrigation list held on the irri side. It is an abbreviated
// ilc type structure. Presently at 64 bytes per.
// 5/1/2014 rmd : Another use of this partition is each timer takes a 40 byte allocation.
// And I suspect queues, semaphores, mutexes, etc take from this partition too.
{	   96,	1024 },

// 2011.10.17 rmd : Why 1024 blocks? Well first the prime consideration here is to support
// to main station list. This is the list of stations in the network. That is the list of
// stations that are saved to a file. This list could be managed out of a hard fixed array.
// But we have it as a list for now and I'm not sure of all the implications of such. One
// thing to note the file save is expecting a list. But it could also save the array as a
// fixed sized array regardless of how many stations there are. Anyway we're using a list
// with dynamic allocations at this time.
// 
// 2012.02.22 ajv : We've increased this from 192 bytes to 304. This was done to support a
// larger stations record, which grew from 160 bytes to 272 by making its variables 32 bits
// rather than 8 and 16 bits. In the future, it may be worthwhile to create a smaller
// partition to handle the overhead and save some of the heap.
// 
// 2012.05.25 ajv : Increased from 304 to 464 bytes. This change is to support a larger
// station structure which now supports change bits used for the data sync.
//
// 5/1/2014 rmd : Update again. Station structure size has now decreased to 160 bytes per
// station. Allow some margin and make these 192 byte blocks.
//
// 5/7/2015 rmd : Here we go again. The STATION_STRUCT size has changed to 168 now. Remember
// the memory partition debug info PLUS list overhead consumes 28 bytes per block. So at a
// minimum we want a 196 byte block size here. And to add some margin to this I'll make
// these blocks 208 bytes a piece.
{	  208,	1024 },

// These are the blocks FULL packets take. We need 134 packet of them when sending all log
// lines from a 48 station. If at the same time we're a FlowSense master receiving a large
// start time message from a controller we may need more. Also if the central asks for
// multiple LARGE OM's one after another they may sit in the transport. DEPENDS if we clean
// the OM's when a new IM arrives from the central. (WE SHOULD!!)
{	  560,	 256 },  


// 5/1/2014 rmd : This is just a general purpose 1K ish byte block. It is used but not sure
// where.
{	 1152,	 32 },
	

// 3/28/2014 rmd : These 2K ish blocks are heavily used during code distribution cause we
// feed the serial driver in the case of SR radios faster than it can transmit them. We
// don't need to have enough 2K blocks to hold the maximum entire code image because at our
// choosen feed rate of one packet per each 2 seconds we are actually feeding the serial
// driver only slightly faster than the radio can transmit. If HOWEVER there is a repeator I
// believe the radio throughout is cut in half. So I think having enough packets to buffer
// half plus a bit of the code image would be reasonable. The required size here is closely
// tied to the feed rate! The faster the feed rate the more packets needed. The slower the
// less. A feed rate of a bout 5 seconds requires few of these blocks. BUT it would take 40
// minutes to distribute the code if we set to that. Even for an M! Which an M can do it in
// as few as 10 minutes. If we use a feed rate of 2.5 seconds the fastest distribution
// possible for a 800K image will be 17 minutes.
// {	 2112,	  ((MAXIMUM_CODE_IMAGE_SIZE/2)/2000) },	
{	 2112,	 384 },	

// I've got most of the FreeRTOS stacks set to 0x1000
{	 4352,	  32 },

{	 8448,	  32 },

// 4/27/2012 rmd : The flow recording records are 24 bytes each. And we keep 1600 of them.
// The bitch is that each of the 4 possible systems supports a full flow recording tool. So
// we need 4 blocks for that. The other 2 are used a utility blocks used during file saves.
// This partition is also used for the Precipio trace event buffer. That block is 40620
// bytes. So make this block size 40688 by adding 64 and rounding up to make divisible by 8.
{	40688,	  MAX_POSSIBLE_SYSTEMS + 2 },

// 10/24/2012 rmd : This partition is what the comm_mngr uses when distributing a full load
// of new stations. 768 of them. Part of the change data load? This will need to be looked
// at further!!
{	116416,		2 },

// 5/8/2012 rmd : This is the size of the station file. Needed when we read the a full 768
// station file from the data flash on program startup. The file is read into this block of
// memory and then loaded station by station into a list of smaller partition memory blocks.
// Then this big block is freed. This block would also be used when writing a new copy of
// the station file to the flash. Because the function takes a complete copy of the list
// items as a blob and queues the write to be accomplished later.
//
// 5/1/2014 rmd : Updated to be based 192 bytes per station. The real size is 168 bytes per
// station but I added 24 bytes margin per station.
{	147456,		2 },

// 2/4/2014 rmd : For station report data file and the station history file when saving
// either one. The actual array of completed records for these is in SDRAM. However the file
// system insists on taking a copy of and saving from the copy. Also needed to read out the
// file upon program startup and that file read requires this block. The station report data
// and station history files each require a block under 700K bytes. We also use this
// partition for CODE DOWNLOAD. So this is the maximum code image size we can accept. At the
// time of this writing the image is at 702K. Hmmm. If we allow it to double in size (which
// I find extraordinary given all the low level support that doesn't need to be duplicated
// to add features!) that should cover us for a while.
// 
// 2/14/2014 ajv : Since we use the __adjust_block_size call to actually
// increase the memory that needs to be allocated, increase this block by the 28
// bytes that are added so the mem_obtain_a_block_if_available routine can allocate the
// necessary memory.
{	MAXIMUM_CODE_IMAGE_AND_INBOUND_OR_OUTBOUND_COMMSERVER_MESSAGE_SIZE + 28,	1 }	

};

/*
*********************************************************************************************************
*                                         LOCAL GLOBAL VARIABLES
*********************************************************************************************************
*/

OS_MEM OSMemTbl[ OS_MEM_MAX_PART ];		/* Storage for memory partition manager             */

/*
*********************************************************************************************************
*                                        CREATE A MEMORY PARTITION
*
* Description : Create a fixed-sized memory partition that will be managed.
*
* Arguments   : addr     is the starting address of the memory partition
*
*               nblks    is the number of memory blocks to create from the partition.
*
*               blksize  is the size (in bytes) of each block in the memory partition.
*
*               err      is a pointer to a variable containing an error message which will be set by
*                        this function to either:
*             
*                        OS_NO_ERR            if the memory partition has been created correctly.
*                        OS_MEM_INVALID_PART  no free partitions available
*   					 OS_MEM_INVALID_BLKS	user specified an invalid number of blocks
*   											(must be != 0)
*   					 OS_MEM_INVALID_SIZE  user specified an invalid block size (must be
* greater than the size of a pointer) Returns    : != (OS_MEM *)0  is the partition was
*   		   created == (OS_MEM *)0  if the partition was not created because of invalid
*   						   arguments or, no free partition is available.
*********************************************************************************************************
*/

static OS_MEM *__os_mem_create( unsigned short pindex, void *addr, UNS_32 nblks, UNS_32 blksize, UNS_8 *err )
{
	OS_MEM  *pmem;
	UNS_8   *pblk;
	void   **plink;
	unsigned long i;
	
	if (nblks == 0 ) {                                  /* Must have at least 1 blocks per partition      */
		*err = OS_MEM_INVALID_BLKS;
		return ((OS_MEM *)0);
	}
	if (blksize < sizeof(void *)) {                   /* Must contain space for at least a pointer      */
		*err = OS_MEM_INVALID_SIZE;
		return ((OS_MEM *)0);
	}
	
	/* Create linked list of free memory blocks      */
	if( nblks == 1 )
	{
		plink = (void **)addr;                            

		*plink = (void *)0;  // Last memory block points to NULL
	}
	else
	{
		plink = (void **)addr;

		pblk  = (UNS_8 *)addr + blksize;

		for (i = 0; i < (nblks - 1); i++)
		{
			*plink = (void *)pblk;

			plink  = (void **)pblk;

			pblk   = pblk + blksize;
		}

		*plink = (void *)0;  // Last memory block points to NULL
	}
	
	
	
	pmem = &OSMemTbl[ pindex ];
	
	pmem->OSMemAddr     = addr;                       /* Store start address of memory partition       */
	pmem->OSMemFreeList = addr;                       /* Initialize pointer to pool of free blocks     */
	pmem->OSMemNFree    = nblks;                      /* Store number of free blocks in MCB            */
	pmem->OSMemNBlks    = nblks;
	pmem->OSMemBlkSize  = blksize;      /* Store block size of each memory blocks        */
	
	
	*err = OS_MEM_NO_ERR;
	
	return( pmem ); 
}

/* ---------------------------------------------------------- */
void init_mem_partitioning( void ) {
	
unsigned short i;

unsigned char err;

unsigned char *ucp;

OS_MEM  *pmem;


	#if ( OS_MEM_MAX_PART < 2 )
	
		the number of partitions must be >= 2 ... not sure why though??? (historically not an issue as we have many more partitions)
		
	#endif
	
	
	// Calsense added code...for our application we want to go ahead and grab the partitions from the heap
	// using malloc...then using "OSMemCreate()" we enable each of the fixed block partitions for use...our
	// scheme is to have "mem_malloc" end up using a block from an appropiate sized partition...the nice
	// thing is that when we go to free the block the mem_malloc_debug "knows" from which partition the
	// block came from and can therefore return it correctly...this seems to say that in order to fix the
	// fragmentation problem (what the partioning is all about) we must employ some form of the mem_debug
	// header in the assigned block to make it easy to know where it came from...the most direct would be to
	// keep the pointer to OSMemTbl memory control block the block was from...that pointer could of course
	// get destroyed in a memory underrun problem...but MEM_DEBUG will tell us about those
	// 
	// This function MUST be called very early on in main. BEFORE the scheduler has been started. There is no protection from
	// pre-emption. And none is needed. As the OS scheduler is not running.
	for( i=0; i<OS_MEM_MAX_PART; i++ ) {
	
		ucp = malloc( partitions[ i ].block_count * partitions[ i ].block_size );
		
		if( ucp == NULL )
		{
			// CANNOT make any alert lines as the call to create the ALERT line wants to use the alerts pile MUTEX.
			// Which is not yet created. Because its creation involves an allocation from this memory pool. So the
			// initialization of the pool MUST be error free. And no alerts to tell us about a problem.
			FREEZE_and_RESTART_APP;
		}
		
		pmem = __os_mem_create( i, ucp, partitions[ i ].block_count, partitions[ i ].block_size, &err );
		
		if( pmem == NULL )
		{
			// CANNOT make any alert lines as the call to create the ALERT line wants to use the alerts pile MUTEX.
			// Which is not yet created. Because its creation involves an allocation from this memory pool. So the
			// initialization of the pool MUST be error free. And no alerts to tell us about a problem.
			FREEZE_and_RESTART_APP;
        }
		
	    
		//for debug show_mem_parts( i );
		
		/*
		init_lcd();
	    //SetContrastToMidScale();
		DisplayLEDOn();
		
		DFP( SLO_2, "partition %d", i );
		DFP( SLO_4, "block size  = %5d", pmem->OSMemBlkSize );
		DFP( SLO_5, "block count = %5d", pmem->OSMemNBlks );
		DFP( SLO_6, "available   = %5d", pmem->OSMemNFree );
		
		for ( www=0; www<0x30000; www++ ) {
			
			BangWDT();
			
		}
		*/
		
    }

	// now all the partitions have been created and are available for use by mem_malloc_debug and mem_free_debug
}

/* ---------------------------------------------------------- */
void *OSMemGet( OS_MEM *pmem, UNS_32 *err )
{
	// This function is only called from one place and is ONLY called when the scheduler has been suspended.

	void *rv;
    
	// See if there are any free memory blocks
	if( pmem->OSMemNFree > 0 )
	{
		rv = pmem->OSMemFreeList;					/* Yes, point to next free memory block          */

        pmem->OSMemFreeList = *(void **)rv;			/*      Adjust pointer to new free list          */

        pmem->OSMemNFree--;                         /*      One less memory block in this partition  */

        *err = OS_MEM_NO_ERR;                       /*      No error                                 */
    }
	else
	{
		rv = NULL;									/*      Return NULL pointer to caller            */
		
		*err = OS_MEM_NO_FREE_BLKS;                 /* No,  Notify caller of empty memory partition  */

		/* for debug
		DisplayLEDOn();
		ClearDisplay();
		for ( i=0; i<OS_MEM_MAX_PART; i++ ) {
			
			DFP( SLO_4+(i*40), " %6d, %3d, %3d ", OSMemTbl[ i ].OSMemBlkSize, OSMemTbl[ i ].OSMemNBlks, OSMemTbl[ i ].OSMemNFree );

		}
		
	    init_kbd( FALSE );
		WaitTillKey();
		*/
    }

	return( rv );
}

/*********************************************************************************************************
*                                         RELEASE A MEMORY BLOCK
*
* Description : Returns a memory block to a partition
*
* Arguments   : pmem    is a pointer to the memory partition control block
*
*               pblk    is a pointer to the memory block being released.
*
* Returns     : OS_NO_ERR         if the memory block was inserted into the partition
*               OS_MEM_FULL       if you are returning a memory block to an already FULL memory partition
*                                 (You freed more blocks than you allocated!)
*********************************************************************************************************/
UNS_8 OSMemPut( unsigned short pindex, void *pblk )
{
	UNS_8 rv;

	OS_MEM *pmem;

	// This function is only called from one place and is ONLY called when the scheduler has been suspended.

	//MEM_DEBUG_STRUCT *pdl;
	//static unsigned long lcounter;
    
	//	lcounter++;


	/*
	pdl = (MEM_DEBUG_STRUCT*)pblk;

	DFP( SLO_1, "been here %d times", lcounter );
	
	DFP( SLO_2, "index passed=%d, memory size passed=%d", pindex, pdl->nbytes );
	
	for ( i=0; i<OS_MEM_MAX_PART; i++ ) {
		
		DFP( SLO_4+(i*40), " %6d, %3d, %3d ", OSMemTbl[ i ].OSMemBlkSize, OSMemTbl[ i ].OSMemNBlks, OSMemTbl[ i ].OSMemNFree );

	}
	
	WaitTillKey();
	*/



	pmem = &OSMemTbl[ pindex ];


    if (pmem->OSMemNFree >= pmem->OSMemNBlks) {  	 /* Make sure all blocks not already returned          */


		/*
		mem_show_memstats();
		
		
		DFP( SLO_1, "block size is %d    block size is %d  ", pmem->OSMemBlkSize, pdl->nbytes );
		DFP( SLO_2, " total blocks %d         ", pmem->OSMemNBlks );
		DFP( SLO_3, "  free blocks %d         ", pmem->OSMemNFree );
				
		WaitTillKey();
			
		*/

		rv = OS_MEM_FULL;
		       
    } else {
    	
	    *(void **)pblk = pmem->OSMemFreeList;   /* Insert released block into free block list         */
	    
		pmem->OSMemFreeList = pblk;
	    
		pmem->OSMemNFree++;                          /* One more memory block in this partition            */

    	rv = OS_MEM_NO_ERR;	        					 /* Notify caller that memory block was released       */
		
	}

	return( rv );
}

/*
*********************************************************************************************************
*                                          QUERY MEMORY PARTITION
*
* Description : This function is used to determine the number of free memory blocks and the number of 
*               used memory blocks from a memory partition.
*
* Arguments   : pmem    is a pointer to the memory partition control block
*
*               pdata   is a pointer to a structure that will contain information about the memory
*                       partition.
*
* Returns     : OS_NO_ERR         Always returns no error.
*********************************************************************************************************
*/
/*

not used so comment out


INT8U OSMemQuery( OS_MEM *pmem, OS_MEM_DATA *pdata ) {

unsigned short SavedISRLevel;

    SavedISRLevel = ISRLevel;
    if ( ISRLevel < INT_MEM_BLACKOUT ) SetISRLevel( INT_MEM_BLACKOUT );

    pdata->OSAddr     = pmem->OSMemAddr;
    pdata->OSFreeList = pmem->OSMemFreeList;
    pdata->OSBlkSize  = pmem->OSMemBlkSize;
    pdata->OSNBlks    = pmem->OSMemNBlks;
    pdata->OSNFree    = pmem->OSMemNFree;

	SetISRLevel( SavedISRLevel );  // back to where it was

    pdata->OSNUsed    = pdata->OSNBlks - pdata->OSNFree;

    return( OS_NO_ERR );                         
}

*/

/*
void show_mem_parts( unsigned short where ) {

unsigned short j;
	
    init_lcd();
	init_lcdbias_hardware();  // sets the bias to mid_scale
	DisplayLEDOn();

	ClearDisplay();

	DFP( SLO_1, "%d     ", where );

	for ( j=0; j<OS_MEM_MAX_PART; j++ ) {
		
		DFP( SLO_4+(j*40), "%d: %6d, %3d, %3d ", j, 
												 OSMemTbl[ j ].OSMemBlkSize, 
												 OSMemTbl[ j ].OSMemNBlks, 
												 OSMemTbl[ j ].OSMemNFree );

	}
	
    init_kbd( FALSE );
	WaitTillKey();
	
	for ( j=0; j<20; j++ ) WaitOneMS();  // effective debounce

}		
*/		

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

