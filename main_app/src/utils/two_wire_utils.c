/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"two_wire_utils.h"

#include	"d_two_wire_discovery.h"

#include	"e_poc_decoder_list.h"

#include	"e_station_decoder_list.h"

#include	"r_decoder_stats.h"

#include	"tpmicro_data.h"

#include	"screen_utils.h"

#include	"speaker.h"

#include	"flowsense.h"

#include	"irri_comm.h"

#include	"alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/17/2014 ajv : This statically allocated array is used to build decoder
// lists such as those displayed on the Station and POC Assignment screens. This
// is necessary because when working with a list of decoders on such screens, we
// need a condensed list which only includes the particular type of decoder
// we're looking at.
DECODER_INFO_FOR_DECODER_LISTS decoder_info_for_display[ MAX_DECODERS_IN_A_BOX ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void TWO_WIRE_jump_to_decoder_list( const UNS_32 pdecoder__tpmicro_type )
{
	DISPLAY_EVENT_STRUCT	lde;

	GuiVar_MenuScreenToShow = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._02_menu = SCREEN_MENU_SETUP;
	lde._06_u32_argument1 = (true);
	lde._08_screen_to_draw = GuiVar_MenuScreenToShow;

	switch( pdecoder__tpmicro_type )
	{
		case DECODER__TPMICRO_TYPE__2_STATION:
			lde._03_structure_to_draw = GuiStruct_scrTwoWireStationAssignment_0;
			lde._04_func_ptr = (void*)FDTO_TWO_WIRE_STA_draw_menu;
			lde.key_process_func_ptr = TWO_WIRE_STA_process_menu;
			lde.populate_scroll_box_func_ptr = TWO_WIRE_STA_load_decoder_serial_number_into_guivar;
			Change_Screen( &lde );
			break;

		case DECODER__TPMICRO_TYPE__POC:
			lde._03_structure_to_draw = GuiStruct_scrTwoWirePOCAssignment_0;
			lde._04_func_ptr = (void*)FDTO_TWO_WIRE_POC_draw_menu;
			lde.key_process_func_ptr = TWO_WIRE_POC_process_menu;
			lde.populate_scroll_box_func_ptr = TWO_WIRE_POC_load_decoder_serial_number_into_guivar;
			Change_Screen( &lde );
			break;

		//default:
			// 4/16/2014 ajv : Since no valid decoder type was passed in, don't
			// change screens.
	}

}

/* ---------------------------------------------------------- */
/**
 * Turns on the 2-Wire line from the user interface. If the line was turned on
 * successfully, a good key beep is played; otherwise, a bad key beep is played.
 * 
 * NOTE: This function can be enhanced in the future to be an FDTO function and
 * display a dialog with information for the user.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the users presses the <b>Turn ON Two-Wire Path</b> button from the Two-Wire
 * menu.
 *
 * @author 1/29/2013 Adrianusv
 *
 * @revisions (none)
 */
extern void TWO_WIRE_turn_cable_power_on_or_off_from_ui( const BOOL_32 pon_or_off )
{
	// 1/29/2013 rmd : If the user keeps on pressing he'll get the bad beep until the message
	// has been sent to the TPMicro.
	if( tpmicro_data.two_wire_cable_power_operation.send_command == (false) )
	{
		tpmicro_data.two_wire_cable_power_operation.send_command = (true);

		tpmicro_data.two_wire_cable_power_operation.on = pon_or_off;  // true to turn on

		good_key_beep();
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/**
 * Starts the Two-Wire decoder discovery process from the user interface.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when 
 * the user presses the <b>Perform Discovery</b> button on the Two-Wire menu.
 *
 * @author 1/29/2013 Adrianusv
 *
 * @revisions (none)
 */
extern void TWO_WIRE_perform_discovery_process( const BOOL_32 pshow_progress_dialog, const BOOL_32 pcalled_from_ui )
{
	UNS_32	our_box_index;

	// ----------
	
	our_box_index = FLOWSENSE_get_controller_index();

	// ----------
	
	// 5/9/2016 ajv : Let's make sure we're in a state where we can actually perform a
	// discovery. Additionally, ensure we actually have a 2-Wire module installed. Not sure what
	// will happen if we try to perform a discovery without one, but why even try since we know
	// it won't find any decoders.
	if( (COMM_MNGR_network_is_available_for_normal_use()) && (chain.members[ our_box_index ].box_configuration.wi.two_wire_terminal_present) )
	{
		// 1/29/2013 rmd : If the user keeps on pressing he'll get the bad beep until the message
		// has been sent to the TPMicro.
		if( tpmicro_data.two_wire_perform_discovery == (false) )
		{
			// 8/4/2015 rmd : A rather brute force method but the only one I could come up with so far
			// to clear the 2W cable excessive current indication in both the UI and at the tpmicro and
			// keep them in sync. We could just clear the flag for our index in the blind here cause I
			// know the tpmicro code for sure clears the flag when the discovery begins. But then the
			// two_wire cable excessive current xmission state gets out of sync. So go this formal
			// route.
			if( irri_comm.two_wire_cable_excessive_current[ our_box_index ] || (tpmicro_data.two_wire_cable_excessive_current_xmission_state = TWO_WIRE_CABLE_ANOMALY_STATE_waiting_for_reponse_from_master) )
			{
				// 8/4/2015 rmd : Clear the two_wire cable excessive current display indication. This is a
				// user informative variable only.
				irri_comm.two_wire_cable_excessive_current[ our_box_index ] = (false);
				
				// 8/4/2015 rmd : And make the same message that carries the request to start the discovery
				// carry the request to clear the short state.
				tpmicro_data.two_wire_cable_excessive_current_xmission_state = TWO_WIRE_CABLE_ANOMALY_STATE_send_ack_to_tpmicro;
			}
			
			// ----------
			
			// 10/17/2013 ajv : Initialize the number of decoders discovered so far since we're starting
			// a new discovery process.
			tpmicro_data.decoders_discovered_so_far = 0;

			tpmicro_data.two_wire_perform_discovery = (true);

			if( pcalled_from_ui )
			{
				good_key_beep();
			}

			TWO_WIRE_init_discovery_dialog( pshow_progress_dialog );
		}
		else
		{
			if( pcalled_from_ui )
			{
				bad_key_beep();
			}
		}
	}
	else
	{
		if( !chain.members[ our_box_index ].box_configuration.wi.two_wire_terminal_present )
		{
			Alert_Message( "2-Wire Discovery Skipped - 2-Wire option missing" );
		}
		else
		{
			Alert_Message( "2-Wire Discovery Skipped - network not ready" );
		}

		// ----------

		if( pcalled_from_ui )
		{
			bad_key_beep();
		}
	}
}

/* ---------------------------------------------------------- */
/**
 * Clears all of the the Two-Wire decoder statistics from the user interface.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when 
 * the user presses the <b>Clear All Statistics</b> button on the Two-Wire menu.
 *
 * @author 2/1/2013 Adrianusv
 *
 * @revisions (none)
 */
extern void TWO_WIRE_clear_statistics_at_all_decoders_from_ui( void )
{
	if( tpmicro_data.two_wire_clear_statistics_at_all_decoders == (false) )
	{
		tpmicro_data.two_wire_clear_statistics_at_all_decoders = (true);

		good_key_beep();
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/**
 * Requests all of the Two-Wire decoder statistics from the user interface.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when 
 * the user presses the <b>Request All Statistics</b> button on the Two-Wire
 * menu.
 *
 * @author 1/30/2013 Adrianusv
 *
 * @revisions (none)
 */
extern void TWO_WIRE_request_statistics_from_all_decoders_from_ui( void )
{
	if( tpmicro_data.two_wire_request_statistics_from_all_decoders == (false) )
	{
		tpmicro_data.two_wire_request_statistics_from_all_decoders = (true);

		good_key_beep();
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/**
 * Starts or stops the loopback test with all decoders from the user interface.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when 
 * the user presses either the <b>Loopback Test All Controllers</b> or <b>Stop
 * Loopback Test All Controllers</b> button on the Two-Wire menu.
 * 
 * @param pstart_or_stop True if the loopback test should be started; otherwise,
 * false to stop it.
 *
 * @author 2/1/2013 Adrianusv
 *
 * @revisions (none)
 */
extern void TWO_WIRE_all_decoder_loopback_test_from_ui( const BOOL_32 pstart_or_stop )
{
	if( pstart_or_stop )
	{
		if( tpmicro_data.two_wire_start_all_decoder_loopback_test == (false) )
		{
			tpmicro_data.two_wire_start_all_decoder_loopback_test = (true);
	
			good_key_beep();
		}
		else
		{
			bad_key_beep();
		}
	}
	else
	{
		if( tpmicro_data.two_wire_stop_all_decoder_loopback_test == (false) )
		{
			tpmicro_data.two_wire_stop_all_decoder_loopback_test = (true);
	
			good_key_beep();
		}
		else
		{
			bad_key_beep();
		}
	}
}

/* ---------------------------------------------------------- */
/**
 * Turns on the specified Two-Wire decoder's output from the user interface.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * teh user presses the <b>Turn on output A/B</b> button from the Two-Wire
 * decoder list.
 * 
 * @param pdecoder_sn The serial number of the decoder to turn on.
 * 
 * @param poutput_0 The output to turn on. 0 represents Output A (black), 1
 * represents Output B (orange).
 *
 * @author 1/29/2013 Adrianusv
 *
 * @revisions (none)
 */
extern void TWO_WIRE_decoder_solenoid_operation_from_ui( const UNS_32 pdecoder_sn, const UNS_32 poutput_0, const BOOL_32 pon_or_off )
{
	// 1/29/2013 rmd : If the user keeps on pressing he'll get the bad beep until the message
	// has been sent to the TPMicro.
	if( tpmicro_data.send_2w_solenoid_location_on_off_command == (false) )
	{
		tpmicro_data.send_2w_solenoid_location_on_off_command = (true);
		
		tpmicro_data.two_wire_solenoid_location_on_off_command.sn = pdecoder_sn;
		
		tpmicro_data.two_wire_solenoid_location_on_off_command.output = poutput_0;

		tpmicro_data.two_wire_solenoid_location_on_off_command.on = pon_or_off;

		if( poutput_0 == DECODER_OUTPUT_A_BLACK )
		{
			GuiVar_TwoWireOutputOnA = pon_or_off;

			// 10/25/2013 ajv : The TP Micro only allows a single output to be
			// turned on in this fashion. It also handles turning off an output
			// if energizing another. Therefore, flip the UI flag for output B
			// if it's set when turning in output A.
			GuiVar_TwoWireOutputOnB = (false);
		}
		else
		{
			GuiVar_TwoWireOutputOnB = pon_or_off;

			// 10/25/2013 ajv : The TP Micro only allows a single output to be
			// turned on in this fashion. It also handles turning off an output
			// if energizing another. Therefore, flip the UI flag for output A
			// if it's set when turning in output B.
			GuiVar_TwoWireOutputOnA = (false);
		}
		
		good_key_beep();
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

