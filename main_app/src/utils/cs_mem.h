/* +++Date last modified: 05-Jul-1997 */
/*
**  This is a copyrighted work which is functionally identical to work
**  originally published in Micro Cornucopia magazine (issue #52, March-April,
**  1990) and is freely licensed by the author, Walter Bright, for any use.
*/

/*_ mem.h   Fri May 26 1989   Modified by: Walter Bright */
/* Copyright 1986-1988 by Northwest Software    */
/* All Rights Reserved                    */
/* Written by Walter Bright               */




/* ---------------------------------- */
/* ---------------------------------- */

/*  file = mem.h  03.05.2003  rmd       */
/*  and again updated by rmd 03.16.2009 */

/* ---------------------------------- */

#ifndef _INC_MEM_H
#define _INC_MEM_H

/* ---------------------------------- */
/* ---------------------------------- */

// 6/24/2014 ajv : Required for size_t
#include	<stdio.h>

#include	"cs_mem_part.h"

#include	"lpc_types.h"




/* ---------------------------------- */
/* ---------------------------------- */

// provides malloc debugging capabilities found in mem.c and mem.h - test
// dynamically allocated memory for overruns, underruns, free the same memory more than
// once and tries to bring out any use of memory already freed by filling it
//
// Change it to a 0 OR comment out the whole line (either way works)
#define MEM_DEBUG 1

/* ---------------------------------- */
/* ---------------------------------- */
/*
 * Memory management routines.
 *
 * Compiling:
 *
 *    #define MEM_DEBUG 1 when compiling to enable extended debugging
 *    features.
 *
 * Features always enabled:
 *
 *    o mem_init() is called at startup, and mem_term() at
 *      close, which checks to see that the number of alloc's is
 *      the same as the number of free's.
 *    o Behavior on out-of-memory conditions can be controlled
 *      via mem_setexception().
 *
 * Extended debugging features:
 *
 *    o Enabled by #define MEM_DEBUG 1 when compiling.
 *    o Check values are inserted before and after the alloc'ed data
 *      to detect pointer underruns and overruns.
 *    o Free'd pointers are checked against alloc'ed pointers.
 *    o Free'd storage is cleared to smoke out references to free'd data.
 *    o Realloc'd pointers are always changed, and the previous storage
 *      is cleared, to detect erroneous dependencies on the previous
 *      pointer.
 *    o The routine mem_checkptr() is provided to check an alloc'ed
 *      pointer.
 */



/* if creating debug version  */
#if (MEM_DEBUG == 1)

	/* Create a list of all alloc'ed pointers, retaining info about where   */
	/* each alloc came from. This is a real memory and speed hog, but who   */
	/* cares when you've got obscure pointer bugs.                    */
	
	typedef struct  {
	
		void *next;					// next in list			
		void *prev;					// previous value in list
		char *file;					// filename of where allocated
		unsigned long line;			// line number of where allocated
		unsigned long nbytes;		// size of the allocation
		unsigned long beforeval;	// detect underrun of data
	
		// items before this point are considered the header
	
		char data[ 4 ];				// the data actually allocated
	
	} MEM_DEBUG_STRUCT;
	
	
	#define MEM_SIZE_OF_DEBUG_HEADER (sizeof( MEM_DEBUG_STRUCT ) - 4)
	
	
	
	extern UNS_32	mem_count;		// # of allocs that haven't been free'd

	extern UNS_32	mem_maxalloc;     /* max # of bytes allocated         */
	
	extern UNS_32	mem_numalloc;     /* current # of bytes allocated           */
	
	// variables for examining what the allocation by size distribution looks like
	extern UNS_32	mem_current_allocations_of[ OS_MEM_MAX_PART ];
	
	extern UNS_32	mem_max_allocations_of[ OS_MEM_MAX_PART ];

	extern MEM_DEBUG_STRUCT mem_alloclist;
	
	
	
	
	#define init_mem()			init_mem_debug()
	
	#define mem_malloc( u )		mem_malloc_debug( (u), __FILE__, __LINE__ )
	
	#define mem_free( p )		mem_free_debug( (p), __FILE__, __LINE__ )
	
	
	void init_mem_debug( void );
	
	void *mem_malloc_debug( size_t n, char *fil, UNS_32 lin );
	

	// 3/21/2017 rmd : This define used to contain the 'remove path from filename' function with
	// __FILE__ as an argument. Well that has turned out to be a very silly thing to do as it
	// forces every use of the define to needlessly execute that function. It is only necessary
	// to execute that function if we indeed have an error and we are attempting to embed the
	// file name into an alert line.
	#define mem_obtain_a_block_if_available( size, pntr )	mem_oabia( (size), (pntr), __FILE__, __LINE__ )

	extern BOOL_32 mem_oabia( const UNS_32 palloc_size, void **block_ptr, char *fil, UNS_32 lin );
	

	void mem_free_debug( void *ptr, char *fil, UNS_32 lin );
	
	void mem_show_allocations( void );
	
#else

	void init_mem( void );
	
	void *mem_malloc( size_t numbytes );

	void mem_free( void *ptr );

#endif /* MEM_DEBUG */


#endif /* MEM_H */

