/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memset
#include	<string.h>

#include	"group_base_file.h"

#include	"app_startup.h"

#include	"background_calculations.h"

#include	"change.h"

#include	"comm_mngr.h"

#include	"cs_mem.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"d_copy_dialog.h"

#include	"dialog.h"

#include	"flash_storage.h"

#include	"flowsense.h"

#include	"irri_comm.h"

#include	"lcd_init.h"

#include	"m_main.h"

#include	"pdata_changes.h"

#include	"r_alerts.h"

#include	"range_check.h"

#include	"speaker.h"

#include	"stations.h"

#include	"e_keyboard.h"

#include	"scrollbox.h"

#include	"e_status.h"

#include	"station_groups.h"

#include	"manual_programs.h"

#include	"shared.h"

#include	"e_station_selection_grid.h"

#include	"walk_thru.h"



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	MIST_LIST_HDR_TYPE_PTR	list_hdr_ptr;

	void 					(*init_func_ptr)(void *const, BOOL_32);

	UNS_32					size;

	UNS_32					number_of_groups_ever_created;

	char					*label_ptr;

} GROUP_LIST_ADD_STRUCTURE;

/* ---------------------------------------------------------- */

BOOL_32	g_GROUP_creating_new;

UNS_32	g_GROUP_ID;

UNS_32	g_GROUP_list_item_index;

BOOL_32	g_GROUP_deleted__pending_transmission;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Allocates memory for and initializes a new list item. Once the new item has 
 * been initialized, it is inserted into the list at the tail. The rest of the 
 * list's items are also updated with the new count. This is a generic function
 * that can be used for any list group. If the passed in structure is not valid,
 * an alert is generated and a NULL is returned.
 * 
 * @mutex_requirements Calling function requires the appropriate list mutex
 * to ensure the list doesn't change during this process. This mutex may be
 * either list_program_data_recursive_MUTEX, list_system_recursive_MUTEX, or
 * list_poc_recursive_MUTEX, depending on which group being worked with.
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user creates a new group; within the context of the CommMngr task when a
 * new group is created using a central or handheld radio remote.
 * 
 * @param pglas A pointer to the structure to add to the list which includes a 
 * pointer to the list itself.
 * 
 * @param pgroups_exist_bool True if at least one group already exists in the
 * list; otherwise, false.
 * 
 * @param pnext_list_item A pointer to the list element to insert the new group
 * before. This should be NULL for lists that don't require a particular order.
 * So far, the only time this is not NULL is for the stations list where the
 * list must remain ordered by controller serial number and then by station
 * number.
 * 
 * @return void* A pointer to the newly added list item.
 *
 * @author 6/9/2011 Adrianusv
 *
 * @revisions
 *   6/26/2013 ajv : Added the pnext_list_item parameter to allow items to be
 *   inserted into a particular location in a list rather than just at the end.
 */
static void *nm_GROUP_add_new_group_to_the_list( GROUP_LIST_ADD_STRUCTURE *const pglas, const BOOL_32 pgroups_exist_bool, void *pnext_list_item )
{
	// ALL group structures MUST start with this base structure first.
	GROUP_BASE_DEFINITION_STRUCT	*lgroup_item;

	void	*rv;

	if( pglas != NULL )
	{
		lgroup_item = mem_malloc( pglas->size );
		
		// Initialize the whole structure to 0. This sets the ss array to all
		// false - the normal default when adding a new group. Also sets the
		// dead_space to all 0's. And the flags_uc to 0.
		memset( lgroup_item, 0x00, pglas->size );
		
		pglas->number_of_groups_ever_created = (pglas->number_of_groups_ever_created + 1);

		// Assign the group's UNIQUE number	to the next number for the new
		// group created
		lgroup_item->group_identity_number = pglas->number_of_groups_ever_created;

		// Initialize the name
		snprintf( lgroup_item->description, sizeof(lgroup_item->description),
				  "%s %u", pglas->label_ptr, lgroup_item->group_identity_number );

		// Execute the initialization function ... sometimes there isn't one - for
		// 'groups' that only show the stations (stations_in_use)
		if( pglas->init_func_ptr != NULL )
		{
			(pglas->init_func_ptr)( lgroup_item, pgroups_exist_bool );
		}
		
		lgroup_item->deleted = (false);
		
		// Add it to the list
		if( pnext_list_item == NULL )
		{
			// Insert the item at the end of the list
			nm_ListInsertTail( pglas->list_hdr_ptr, lgroup_item );
		}
		else
		{
			// Insert the item before the specified item to ensure the list
			// stays in order. Currently, this is only used to ensure the
			// stations list remains in numerical order.
			nm_ListInsert( pglas->list_hdr_ptr, lgroup_item, pnext_list_item );
		}

		rv = lgroup_item;

		// Now update everybody in the list to the new count of how many have
		// (ever) been made. So no matter which item we are editing when we add
		// a group we know however many have every been added of this type.
		lgroup_item = nm_ListGetFirst( pglas->list_hdr_ptr );

		while( lgroup_item != NULL )
		{
			lgroup_item->number_of_groups_ever_created = pglas->number_of_groups_ever_created;
		
			lgroup_item = nm_ListGetNext( pglas->list_hdr_ptr, lgroup_item );
		}
	}
	else
	{
		Alert_func_call_with_null_ptr();

		rv = NULL;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Creates a new group using the default settings and adds it to the specified 
 * list. This is a generic function that can be used for any list group.
 * 
 * @mutex_requirements Calling function requires the appropriate list mutex
 * to ensure the list doesn't change during this process. This mutex may be
 * either list_program_data_recursive_MUTEX, list_system_recursive_MUTEX, or
 * list_poc_recursive_MUTEX, depending on which group being worked with.
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user creates a new group; within the context of the CommMngr task when a
 * new group is created using a central or handheld radio remote.
 * 
 * @param plist_hdr_ptr A pointer to the list where the group should be added.
 * 
 * @param pdefault_name The default name to assign to the new group. This is a 
 * static const char found at the top of the associated .c file in the format of
 * <Group>_DEFAULT_NAME.
 * 
 * @param pset_default_values_func_ptr A pointer to the _set_default_values 
 * function for the group.
 * 
 * @param pitem_size The size of the group structure.
 * 
 * @param pgroups_exist_bool True if at least one group exists; otherwise, 
 * false. The only time this should be false is when initializing a list.
 * 
 * @param pnext_list_item A pointer to the list element to insert the new group
 * before. This should be NULL for lists that don't require a particular order.
 * So far, the only time this is not NULL is for the stations list where the
 * list must remain ordered by controller serial number and then by station
 * number.
 * 
 * @return void* A pointer to the newly created group.
 *
 * @author 9/8/2011 Adrianusv
 *
 * @revisions
 *   6/26/2013 ajv : Added the pnext_list_item parameter to allow items to be
 *   inserted into a particular location in a list rather than just at the end.
 */
extern void *nm_GROUP_create_new_group( const MIST_LIST_HDR_TYPE_PTR const plist_hdr_ptr,
										char* pdefault_name, 
										void (*pset_default_values_func_ptr)(void *const, const BOOL_32),
										const size_t pitem_size,
										const BOOL_32 pgroups_exist_bool,
										void *pnext_list_item )
{
	GROUP_LIST_ADD_STRUCTURE *glas_ptr;

	void *lgroup;

	glas_ptr = mem_malloc( sizeof(GROUP_LIST_ADD_STRUCTURE) );

	glas_ptr->list_hdr_ptr = plist_hdr_ptr;
	glas_ptr->size = pitem_size;
	glas_ptr->init_func_ptr = pset_default_values_func_ptr;
	glas_ptr->label_ptr = pdefault_name;

	if( (pgroups_exist_bool == (true)) && (plist_hdr_ptr->count > 0) )
	{
		glas_ptr->number_of_groups_ever_created = ( ((GROUP_BASE_DEFINITION_STRUCT*)nm_ListGetFirst(glas_ptr->list_hdr_ptr))->number_of_groups_ever_created );
	}
	else
	{
		// We can pass the number 0 to indicate there are no groups at this time
		glas_ptr->number_of_groups_ever_created = 0;
	}

	lgroup = nm_GROUP_add_new_group_to_the_list( glas_ptr, pgroups_exist_bool, pnext_list_item );

	mem_free( glas_ptr );

	return ( lgroup );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Returns the group ID of the specified group.
 * 
 * @mutex Calling function requires the appropriate list mutex to ensure the
 *  	  list doesn't change during this process. This mutex may be either
 *        list_program_data_recursive_MUTEX, list_system_recursive_MUTEX, or
 *  	  list_poc_recursive_MUTEX, depending on which group being worked with.
 *
 * @executed This can be called in the context of any task. As long as the
 *           calling function has the appropriate mutex, this should be fine.
 * 
 * @param pgroup A pointer to the group.
 * 
 * @return UNS_32 The group ID of the specified group. Or a 0 (NULL) if the pgroup argument
 * itself is a NULL.
 *
 * @author 9/8/2011 Adrianusv
 *
 * @revisions
 *    9/8/2011 Initial release
 */
extern UNS_32 nm_GROUP_get_group_ID( const void *pgroup )
{
	GROUP_BASE_DEFINITION_STRUCT	*lgroupBase;

	UNS_32	rv;

	// 2012.05.11 ajv : We're comparing pgroup against NULL instead of using the
	// nm_OnList routine because this generic function doesn't have any
	// knowledge about the list the group is supposed to be on. In fact, the
	// logic leading up to this function call has already verified the group is
	// on the list - this is simply a sanity check to ensure the memory location
	// hasn't been corrupted.
	if( pgroup != NULL )
	{
		lgroupBase = ((GROUP_BASE_DEFINITION_STRUCT*)(pgroup));
		
		rv = lgroupBase->group_identity_number;
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_func_call_with_null_ptr();

		#endif

		rv = 0;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the name of the specified group. If the group not valid, 
 * an alert is generated and NULL is returned.
 * 
 * @mutex Calling function requires the appropriate list mutex to ensure the
 *  	  list doesn't change during this process. This mutex may be either
 *        list_program_data_recursive_MUTEX, list_system_recursive_MUTEX, or
 *  	  list_poc_recursive_MUTEX, depending on which group being worked with.
 *
 * @executed This can be called in the context of any task. As long as the
 *           calling function has the appropriate mutex, this should be fine.
 * 
 * @param pgroup A pointer to the group.
 * 
 * @return char* A pointer to the name of the specified group if the group is 
 *  	         valid; otherwise, NULL.
 *
 * @author 9/8/2011 Adrianusv
 *
 * @revisions
 *    9/8/2011 Initial release
 */
extern char *nm_GROUP_get_name( const void *pgroup )
{
	GROUP_BASE_DEFINITION_STRUCT	*lgroupBase;

	char	*rv;

	// 2012.05.11 ajv : We're comparing pgroup against NULL instead of using the
	// nm_OnList routine because this generic function doesn't have any
	// knowledge about the list the group is supposed to be on. In fact, the
	// logic leading up to this function call has already verified the group is
	// on the list - this is simply a sanity check to ensure the memory location
	// hasn't been corrupted.
	if( pgroup != NULL )
	{
		lgroupBase = ((GROUP_BASE_DEFINITION_STRUCT*)(pgroup));

		rv = lgroupBase->description;
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_func_call_with_null_ptr();

		#endif

		rv = NULL;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef	_MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Scrolls through the list looking for a list member with a group ID matching 
 * the group ID of the passed pgroup_to_find. This function seemingly makes no 
 * sense however keep in mind the pgroup_to_find is a pointer to a structure 
 * that is NOT in the list. If the passed in group is invalid, an alert is 
 * generated. 
 * 
 * @mutex Calling function requires the appropriate list mutex to ensure the
 *  	  list doesn't change during this process. This mutex may be either
 *        list_program_data_recursive_MUTEX, list_system_recursive_MUTEX, or
 *  	  list_poc_recursive_MUTEX, depending on which group being worked with.
 *
 * @executed Executed within the context of the key processing task when the
 *  		 user leaves the screen; within the context of the CommMngr task
 *  		 when a programming change is made from the central or handheld
 *  		 radio remote.
 * 
 * @param plist_hdr_ptr A pointer to the list containing the specified group to 
 *  					find.
 * @param pgroup_to_find The group to find in the list.
 * @param answer_ptr A pointer to the group in the list that matches the 
 *  				 specified group.
 * 
 * @return BOOL_32 True if the group was found; otherwise, false.
 *
 * @author 9/14/2011 Adrianusv
 *
 * @revisions
 *    9/14/2011 Initial release
 */
extern BOOL_32 nm_GROUP_find_this_group_in_list( const MIST_LIST_HDR_TYPE_PTR plist_hdr_ptr,
											   const void *const pgroup_to_find,
											   void **answer_ptr )
{
	BOOL_32	rv;

	rv = (false);

	if( pgroup_to_find != NULL )
	{
		*answer_ptr = nm_ListGetFirst( plist_hdr_ptr ); 

		while( (nm_GROUP_get_group_ID(*answer_ptr) != nm_GROUP_get_group_ID(pgroup_to_find)) && (*answer_ptr != NULL) )
		{
			*answer_ptr = nm_ListGetNext( plist_hdr_ptr, *answer_ptr );
		}

		// If a matching group was found, set the return value to true.
		if( *answer_ptr != NULL )
		{
			rv = (true);
		}
	}
	else
	{
		Alert_func_call_with_null_ptr();
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the group ID at the specified index location. If the 
 * group is not found in the list, NULL is returned. Additionally, if the passed
 * in index is out of range, an alert is generated and NULL is returned.
 * 
 * @mutex Calling function requires the appropriate list mutex to ensure the
 *  	  list doesn't change during this process. This mutex may be either
 *        list_program_data_recursive_MUTEX, list_system_recursive_MUTEX, or
 *  	  list_poc_recursive_MUTEX, depending on which group being worked with.
 *
 * @executed Executed within the context of the display processing task when the
 *  		 list and group screens are drawn; within the key processing task
 *  		 when the user navigates through the group list screen.
 * 
 * @param plist_hdr_ptr  pointer to the list.
 * @param pindex_0 An index into the list - 0 based
 * 
 * @return void* A pointer to the group at the specified index into the list. Or a NULL if
 *  the request cannot be satisfied such as when the caller asks beyond the count in the
 *  list.
 *
 * @author 9/8/2011 Adrianusv
 *
 * @revisions
 *    9/8/2011 Initial release
 */
extern void *nm_GROUP_get_ptr_to_group_at_this_location_in_list( const MIST_LIST_HDR_TYPE_PTR const plist_hdr_ptr, UNS_32 pindex_0 )
{
	void	*lgroup;

	UNS_32	list_count_0;

	// 5/22/2012 rmd : Beware nm_ListGetCount is 1 based. The pindex_0 we are trying to find is
	// 0 based. So 0 is the first item in the list.
	list_count_0 = ((nm_ListGetCount( plist_hdr_ptr )) - 1);
	
	// 7/10/2014 ajv : Are we asking for more than there is on the list? If so,
	// the calling function is expecting a NULL.
	if( pindex_0 > list_count_0 )
	{
		lgroup = NULL;
	}
	else
	{
		lgroup = nm_ListGetFirst( plist_hdr_ptr ); 

		// 5/22/2012 rmd : If we are after index 0, the first item on the list we are done. If not
		// we need to get the next list item until the correct amount.
		while( pindex_0 > 0 )
		{
			// 5/22/2012 rmd : If we happen to go past the end of the list (which should not happen as
			// we checked how many on the list, the ListGetNext function will return a NULL. The desired
			// result.
			lgroup = nm_ListGetNext( plist_hdr_ptr, lgroup );
			
			pindex_0 -= 1;
		}
	}
	
	return ( lgroup );
}

/* ---------------------------------------------------------- */
/**
 * This function scrolls through the list looking for a LIST MEMBER with a group
 * ID matching the value of the pgroup_ID parameter. If such a group cannot be
 * found the function returns a NULL pointer. The user should test for such
 * before using the return value.
 * 
 * @mutex Calling function requires the appropriate list mutex to ensure the
 *  	  list doesn't change during this process. This mutex may be either
 *        list_program_data_recursive_MUTEX, list_system_recursive_MUTEX, or
 *  	  list_poc_recursive_MUTEX, depending on which group being worked with.
 *
 * @executed Executed within the context of the TD check task as part of
 *           building the ilc's.
 * 
 * @param plist_hdr_ptr  pointer to the list.
 * @param pgroup_ID A group ID.
 * 
 * @return void* A pointer to the group with the specified group ID.
 *
 * @author 9/20/2011 BobD
 *
 * @revisions
 *    9/20/2011 Initial release
 */
extern void *nm_GROUP_get_ptr_to_group_with_this_GID( const MIST_LIST_HDR_TYPE_PTR const plist_hdr_ptr, const UNS_32 pgroup_ID, const BOOL_32 psuppress_group_not_found_alert )
{
	void	*lgroup_ptr;
	
	if( plist_hdr_ptr != NULL )
	{
		lgroup_ptr = nm_ListGetFirst( plist_hdr_ptr );
		
		while( lgroup_ptr != NULL )
		{
			if( ((GROUP_BASE_DEFINITION_STRUCT*)lgroup_ptr)->group_identity_number == pgroup_ID )
			{
				break;
			}
			
			lgroup_ptr = nm_ListGetNext( plist_hdr_ptr, lgroup_ptr );
		}

		if( (lgroup_ptr == NULL) && (psuppress_group_not_found_alert == (false)) )
		{
			Alert_group_not_found();
		}
	}
	else
	{
		Alert_func_call_with_null_ptr();

		lgroup_ptr = NULL;
	}

	return( lgroup_ptr );
}

/* ---------------------------------------------------------- */
/**
 * Accepts a group ID (e.g., GuiVar_GroupID) and returns the position of that
 * index in the associated list. This is used to ensure the group the user
 * created or was editing is highlighted when they return to the list of groups.
 * 
 * @mutex Requires the appropriate list mutex to ensure the list doesn't change
 *        between locating the index and using it. This mutex may be either
 *        list_program_data_recursive_MUTEX, list_system_recursive_MUTEX, or
 *        list_poc_recursive_MUTEX, depending upon the group being worked with.
 *
 * @executed Executed within the context of the key processing task when the
 *           user selects a group on the group list screen.
 * 
 * @param plist_hdr_ptr A pointer to the list.
 * @param pgroup_ID The group ID.
 * 
 * @return UNS_32 A 0-based index of the requested group into the associated
 *                list.
 *
 * @author 12/14/2011 Adrianusv
 *
 * @revisions
 *    12/14/2011 Initial release
 */
extern UNS_32 nm_GROUP_get_index_for_group_with_this_GID( const MIST_LIST_HDR_TYPE_PTR const plist_hdr_ptr,
														  const UNS_32 pgroup_ID )
{
	void 	*lgroup;

	UNS_32	lgroup_index;

	lgroup_index = 0;

	if( pgroup_ID > 0 )
	{
		if( plist_hdr_ptr != NULL )
		{
			// Traverse through the list looking for the group that matches the
			// passed in group ID.
			lgroup = nm_ListGetFirst( plist_hdr_ptr );

			while( lgroup != NULL )
			{
				if( nm_GROUP_get_group_ID( lgroup ) == pgroup_ID )
				{
					break;
				}

				lgroup = nm_ListGetNext( plist_hdr_ptr, lgroup );

				++lgroup_index;
			}

			if( lgroup == NULL )
			{
				Alert_group_not_found();

				lgroup_index = 0;
			}
		}
		else
		{
			Alert_func_call_with_null_ptr();
		}
	}

	return( lgroup_index );
}

/* ---------------------------------------------------------- */
/**
 * Draws the on-screen keyboard at the specified x and y coordinates. If the
 * language is Spanish, the keyboard's cursor position is shifted 10 pixels to
 * the right to accomodate the longer prompt.
 * 
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user tries to edit a group name.
 * 
 * @param px_coordinate The x-coordinate of location to display the cursor
 * position where the user will type their text. The keyboard will be displayed
 * below this cursor.
 * 
 * @param py_coordinate The y-coordinate of location to display the cursor
 * position where the user will type their text. The keyboard will be displayed
 * below this cursor.
 *
 * @author 8/13/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void GROUP_process_show_keyboard( const UNS_32 px_coordinate, const UNS_32 py_coordinate )
{
	good_key_beep();

	if( GuiLib_LanguageIndex == GuiConst_LANGUAGE_ENGLISH )
	{
		KEYBOARD_draw_keyboard( px_coordinate, py_coordinate, NUMBER_OF_CHARS_IN_A_NAME, KEYBOARD_TYPE_QWERTY );
	}
	else
	{
		KEYBOARD_draw_keyboard( (px_coordinate + 10), py_coordinate, NUMBER_OF_CHARS_IN_A_NAME, KEYBOARD_TYPE_QWERTY );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * @mutex_requirements Calling function must have the appropriate list mutx. For
 * example, if creating a new system, the list_system_recursive_MUTEX must be
 * taken before calling this function.
 */
extern void nm_GROUP_create_new_group_from_UI( void *(*nm_create_new_group_func_ptr)(void), void *(*copy_group_into_guivars_func_ptr)(const UNS_32 pindex_0) )
{
	UNS_32	lgroup_id;

	// ---------------------

	g_GROUP_creating_new = (true);

	// ---------------------

	if( nm_create_new_group_func_ptr != NULL )
	{
		// 11/4/2013 ajv : We don't need to take a mutex here because the
		// appropriate mutex should have already been taken prior to calling
		// this routine.
		lgroup_id = nm_GROUP_get_group_ID( (*nm_create_new_group_func_ptr)() );

		// ---------------------

		// 10/31/2013 ajv : Set the group ID and show the new group.
		g_GROUP_ID = lgroup_id;

		if( copy_group_into_guivars_func_ptr != NULL )
		{
			(*copy_group_into_guivars_func_ptr)( g_GROUP_list_item_index );
		}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void FDTO_GROUP_draw_add_new_window( void )
{
	if( ScreenHistory[ screen_history_index ]._03_structure_to_draw == GuiStruct_scrMainLines_0 )
	{
		GuiLib_ShowScreen( GuiStruct_scrAddNewMainline_0, GuiLib_ActiveCursorFieldNo, GuiLib_RESET_AUTO_REDRAW );
	}
	else if( ScreenHistory[ screen_history_index ]._03_structure_to_draw == GuiStruct_scrManualPrograms_0 )
	{
		GuiLib_ShowScreen( GuiStruct_scrAddNewManualProgram_0, GuiLib_ActiveCursorFieldNo, GuiLib_RESET_AUTO_REDRAW );
	}
	else if( ScreenHistory[ screen_history_index ]._03_structure_to_draw == GuiStruct_scrWalkThru_0 )
	{
		GuiLib_ShowScreen( GuiStruct_scrAddNewWalkThru_0, GuiLib_ActiveCursorFieldNo, GuiLib_RESET_AUTO_REDRAW );
	}
	else //if( ScreenHistory[ screen_history_index ]._03_structure_to_draw == GuiStruct_scrStationGroups_0 )
	{
		GuiLib_ShowScreen( GuiStruct_scrAddNewGroup_0, GuiLib_ActiveCursorFieldNo, GuiLib_RESET_AUTO_REDRAW );
	}
	GuiLib_Refresh();
}

extern void nm_GROUP_load_common_guivars( const void *const pgroup )
{
	g_GROUP_ID = nm_GROUP_get_group_ID( pgroup );

	strlcpy( GuiVar_GroupName, nm_GROUP_get_name( pgroup ), sizeof(GuiVar_GroupName) );
}

extern void FDTO_GROUP_return_to_menu( BOOL_32 *pediting_group, void *(*store_changes_from_guivar_func_ptr)(void) )
{
	(*store_changes_from_guivar_func_ptr)();

	*pediting_group = (false);

	GuiLib_ScrollBox_To_Line( 0, (UNS_16)g_GROUP_list_item_index );

	GuiLib_ActiveCursorFieldNo = GuiLib_NO_CURSOR;

	FDTO_Redraw_Screen( (false) );
}

extern void FDTO_GROUP_draw_menu( const BOOL_32 pcomplete_redraw, BOOL_32 *pediting_group, const UNS_32 plist_count, const UNS_32 pstructure_to_draw, void *load_group_name_func_ptr, void *(*copy_group_into_guivars_func_ptr)(const UNS_32 pindex_0), const BOOL_32 pshow_add_new_item )
{
	UNS_32	lnumber_of_groups;

	INT_32	lcursor_to_select;

	UNS_32	ltop_line;

	ltop_line = 0;

	if( pcomplete_redraw == (true) )
	{
		lcursor_to_select = GuiLib_NO_CURSOR;

		*pediting_group = (false);

		// 5/13/2015 ajv : If we got here from the Status screen and we're on an appropriate screen,
		// jump to group which is next scheduled to irrigate.
		if( GuiVar_StatusShowLiveScreens )
		{
			if( pstructure_to_draw == GuiStruct_scrManualPrograms_0 )
			{
				g_GROUP_list_item_index = MANUAL_PROGRAMS_get_index_for_group_with_this_GID( GuiVar_StatusNextScheduledIrriGID );
			}
			else if( pstructure_to_draw == GuiStruct_scrStartTimesAndWaterDays_0 )
			{
				g_GROUP_list_item_index = STATION_GROUP_get_index_for_group_with_this_GID( GuiVar_StatusNextScheduledIrriGID );
			}
			else if( pstructure_to_draw == GuiStruct_scrWalkThru_0 )
			{
				g_GROUP_list_item_index = WALK_THRU_get_index_for_group_with_this_GID( GuiVar_StatusNextScheduledIrriGID );
			}
		}

		(*copy_group_into_guivars_func_ptr)( g_GROUP_list_item_index );
	}
	else
	{
		lcursor_to_select = (INT_32)GuiLib_ActiveCursorFieldNo;

		ltop_line = (UNS_32)GuiLib_ScrollBox_GetTopLine( 0 );
	}

	lnumber_of_groups = plist_count;

	GuiLib_ShowScreen( (UNS_16)pstructure_to_draw, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( *pediting_group == (true) )
	{
		// 10/24/2013 ajv : Draw the scroll box without highlighting the active
		// line and draw the indicator to show which group the user is looking
		// at.
		//
		// 11/4/2013 ajv : Note the (2 * pshow_add_new_item) in the NoOfLines
		// parameters. When pshow_add_new_item is TRUE, we need to display the
		// <Add New Group> item at the bottom of the list of groups. Since we've
		// now added an additional divider line between the groups and the Add
		// New Group item, we need to make sure we add two lines to the count.
		GuiLib_ScrollBox_Init_Custom_SetTopLine( 0, load_group_name_func_ptr, (INT_16)(lnumber_of_groups + (2 * pshow_add_new_item)), GuiLib_NO_CURSOR, (INT_16)ltop_line );
		GuiLib_ScrollBox_SetIndicator( 0, (UNS_16)g_GROUP_list_item_index );
		GuiLib_ScrollBox_Redraw( 0 );
	}
	else
	{
		// 11/25/2013 ajv : If the user is viewing a dialog box, such as the
		// Copy Group dialog, while the <Add New Group> item is highlighted and
		// they press BACK, the divider is highlighted rather than the text.
		// Therefore, if this is the case, move the item index down one.
		if( (g_GROUP_list_item_index >= plist_count) && (pshow_add_new_item == (true)) )
		{
			g_GROUP_list_item_index = (plist_count + 1);
		}

		// 10/24/2013 ajv : Draw the scroll box and highlight the active line.
		// Since we're redrawing the entire scroll box, there's no reason to
		// call SetIndicator with a -1 to hide it because the indicator simply
		// won't be drawn.
		//
		// 11/4/2013 ajv : Note the (2 * pshow_add_new_item) in the NoOfLines
		// parameters. When pshow_add_new_item is TRUE, we need to display the
		// <Add New Group> item at the bottom of the list of groups. Since we've
		// now added an additional divider line between the groups and the Add
		// New Group item, we need to make sure we add two lines to the count.
		GuiLib_ScrollBox_Init_Custom_SetTopLine( 0, load_group_name_func_ptr, (INT_16)(lnumber_of_groups + (2 * pshow_add_new_item)), (INT_16)g_GROUP_list_item_index, (INT_16)ltop_line );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// 11/1/2013 ajv : If we're redrawing the screen and we're on the 'Add New'
	// button, draw the appropriate screen. Otherwise, draw the normal screen
	// and the station grid, if requested.
	if( g_GROUP_list_item_index >= plist_count )
	{
		// 11/1/2013 ajv : No need to call GuiLib_Refresh here after drawing the
		// scroll line since it's handled by the following routine.
		FDTO_GROUP_draw_add_new_window();
	}
	else
	{
		if( g_STATION_SELECTION_GRID_user_pressed_back == (false) )
		{
			// 11/1/2013 ajv : Refresh the screen to draw the screen and scroll box
			// before drawing the station structure if requested.
			GuiLib_Refresh();
		}
	}
}

extern void GROUP_process_NEXT_and_PREV( const UNS_32 pkeycode, const UNS_32 plist_count, void *(*pextract_and_store_changes_funct_ptr)(void), void *(*pget_group_at_idx_func_ptr)(const UNS_32 pindex_0), void *(*copy_group_into_guivars_func_ptr)(const UNS_32 pindex_0) )
{
	UNS_32 lprev_group_index;

	lprev_group_index = g_GROUP_list_item_index;

	if( (pkeycode == KEY_NEXT) && (g_GROUP_list_item_index < (plist_count - 1)) )
	{
		if( GuiLib_CurStructureNdx != GuiStruct_scrViewStations_0 )
		{
			// 10/24/2013 ajv : Turn off the automatic redraw functions within the
			// SCROLL_BOX_to_line and SCROLL_BOX_up_or_down routines to ensure the
			// scroll bar does not appear.
			SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_DISABLED );

			SCROLL_BOX_to_line( 0, g_GROUP_list_item_index );

			SCROLL_BOX_up_or_down( 0, KEY_C_DOWN );

			// 10/24/2013 ajv : Turn off the automatic redraw functions within the
			// SCROLL_BOX_to_line and SCROLL_BOX_up_or_down routines to ensure the
			// scroll bar does not appear.
			SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_ENABLED );
		}

		++g_GROUP_list_item_index;
	}
	else if( (pkeycode == KEY_PREV) && (g_GROUP_list_item_index > 0) )
	{
		if( GuiLib_CurStructureNdx != GuiStruct_scrViewStations_0 )
		{
			// 10/24/2013 ajv : Turn off the automatic redraw functions within the
			// SCROLL_BOX_to_line and SCROLL_BOX_up_or_down routines to ensure the
			// scroll bar does not appear.
			SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_DISABLED );

			SCROLL_BOX_to_line( 0, g_GROUP_list_item_index );

			SCROLL_BOX_up_or_down( 0, KEY_C_UP );

			// 10/24/2013 ajv : Turn off the automatic redraw functions within the
			// SCROLL_BOX_to_line and SCROLL_BOX_up_or_down routines to ensure the
			// scroll bar does not appear.
			SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_ENABLED );
		}

		--g_GROUP_list_item_index;
	}

	// 10/24/2013 ajv : If the group has actually moved (meaning we're not the
	// very top or bottom of the list, save the current group's information and
	// repopulate the GuiVars with the next groups's. Finally, redraw the screen
	// to ensure the screen is updated.
	if( lprev_group_index != g_GROUP_list_item_index )
	{
		if( GuiLib_CurStructureNdx == GuiStruct_scrViewStations_0 )
		{
			good_key_beep();
		}

		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		(*pextract_and_store_changes_funct_ptr)();

		// 10/24/2013 ajv : Since g_GROUP_ID is only used for groups and we use
		// this routine to handle the Two-Wire list, check to make sure the
		// function pointer isn't NULL before trying to use it.
		if( pget_group_at_idx_func_ptr != NULL )
		{
			g_GROUP_ID = nm_GROUP_get_group_ID( (*pget_group_at_idx_func_ptr)( g_GROUP_list_item_index ) );
		}

		(*copy_group_into_guivars_func_ptr)( g_GROUP_list_item_index );

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

		Redraw_Screen( (false) );
	}
	else
	{
		bad_key_beep();
	}
}

extern void GROUP_process_menu( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event, BOOL_32 *pediting_group, const UNS_32 plist_count, void *(*process_group_func_ptr)(const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event), void *(*add_group_func_ptr)(void), void *(*pget_group_at_idx_func_ptr)(const UNS_32 pindex_0), void *(*copy_group_into_guivars_func_ptr)(const UNS_32 pindex_0) )
{
	DISPLAY_EVENT_STRUCT	lde;

	INT_32	lprev_group_index;

	if( *pediting_group == (true) )
	{
		(*process_group_func_ptr)( pkey_event );
	}
	else
	{
		switch( pkey_event.keycode )
		{
			case KEY_C_RIGHT:
			case KEY_SELECT:
				good_key_beep();

				g_GROUP_list_item_index = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );

				if( g_GROUP_list_item_index >= plist_count )
				{
					// 11/4/2013 ajv : If we've landed on the "Add New Group" item, we've actually jumped two
					// lines (we skip the divider line), so we have to reset the list_item_index
					// value to the list count to ensure the appropriate scroll
					// bar item is highlighted.
					g_GROUP_list_item_index = plist_count;

					// 11/1/2013 ajv : If the user is looking at the Add New Group screen, display the Copy
					// Group dialog. Otherwise, just create a new group (for Systems and Manual Programs).
					if( GuiLib_CurStructureNdx == GuiStruct_scrAddNewGroup_0 )
					{
						// 11/1/2013 ajv : When multiple groups already exist, prompt the user for which group to
						// copy. Otherwise, if there's only 1 group, simply copy the group and
						// display a message to the user telling them the group
						// was copied.
						if( plist_count > 1 )
						{
							COPY_DIALOG_draw_dialog( pediting_group, add_group_func_ptr );
						}
						else
						{
							COPY_DIALOG_peform_copy_and_redraw_screen( pediting_group, add_group_func_ptr, 0, g_GROUP_list_item_index );

							DIALOG_draw_ok_dialog( GuiStruct_dlgGroupCopied_0 );
						}
					}
					else if( add_group_func_ptr != NULL )
					{
						// 11/4/2013 ajv : No need to take a mutex here because
						// the each add_group function takes its respective
						// mutex.
						(*add_group_func_ptr)();

						// 11/4/2013 ajv : The user's added a new group, so
						// let's enter the editing portion of the screen and
						// default to the first cursor position (typically the
						// group name).
						*pediting_group = (true);

						GuiLib_ActiveCursorFieldNo = 0;

						Redraw_Screen( (false) );
					}
				}
				else
				{
					*pediting_group = (true);

					GuiLib_ActiveCursorFieldNo = 0;

					Redraw_Screen( (false) );
				}
				break;

			case KEY_NEXT:
			case KEY_PREV:
				// Change the key to either UP or DOWN so it can be processed
				// using the KEY_C_UP and KEY_C_DOWN case statement.
				if( pkey_event.keycode == KEY_NEXT )
				{
					pkey_event.keycode = KEY_C_DOWN;
				}
				else
				{
					pkey_event.keycode = KEY_C_UP;
				}
				// Don't break here. This will allow the routine to fall into
				// the next case statement which will actually move the cursor
				// up or down appropriately.

			case KEY_C_DOWN:
			case KEY_C_UP:
				lprev_group_index = (INT_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );

				// 11/4/2013 ajv : Since we may actually be skipping over a line
				// if we've reached the bottom of the list of groups, we want to
				// suppress the line getting highlighted until we've completed
				// this process. This ensure the user doesn't see the line we
				// skip over highlight briefly.
				SCROLL_BOX_toggle_scroll_box_automatic_redraw( (false) );

				SCROLL_BOX_up_or_down( 0, pkey_event.keycode );

				SCROLL_BOX_toggle_scroll_box_automatic_redraw( (true) );

				g_GROUP_list_item_index = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );

				if( lprev_group_index != (INT_32)g_GROUP_list_item_index )
				{
					// 11/4/2013 ajv : If we've exceeded the list of groups,
					// we've landed on the divider before the "Add New Group"
					// item. We don't actually want to land here - so skip over
					// it to the "Add New Group" item.
					if( g_GROUP_list_item_index == plist_count )
					{
						lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;

						if( pkey_event.keycode == KEY_C_UP )
						{
							// 11/4/2013 ajv : Use the GuiLib version rather
							// than our custom version because we don't want an
							// additional key beep.
							lde._04_func_ptr = (void*)&GuiLib_ScrollBox_Up;

							// 11/4/2013 ajv : Move the global list_item_index
							// as well.
							g_GROUP_list_item_index--;
						}
						else
						{
							// 11/4/2013 ajv : Use the GuiLib version rather
							// than our custom version because we don't want an
							// additional key beep.
							lde._04_func_ptr = (void*)&GuiLib_ScrollBox_Down;

							// 11/4/2013 ajv : Move the global list_item_index
							// as well.
							g_GROUP_list_item_index++;
						}

						lde._06_u32_argument1 = 0;
						Display_Post_Command( &lde );

						SCROLL_BOX_redraw( 0 );
					}

					if( g_GROUP_list_item_index < plist_count )
					{
						xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

						// 10/24/2013 ajv : Since g_GROUP_ID is only used for
						// groups and we use this routine to handle the Two-Wire
						// list, check to make sure the function pointer isn't
						// NULL before trying to use it.
						if( pget_group_at_idx_func_ptr != NULL )
						{
							g_GROUP_ID = nm_GROUP_get_group_ID( (*pget_group_at_idx_func_ptr)( g_GROUP_list_item_index ) );
						}

						(*copy_group_into_guivars_func_ptr)( g_GROUP_list_item_index );

						Redraw_Screen( (false) );

						xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
					}
					else
					{
						lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
						lde._04_func_ptr = (void*)&FDTO_GROUP_draw_add_new_window;
						Display_Post_Command( &lde );
					}
				}
				break;

			default:
				if( pkey_event.keycode == KEY_BACK )
				{
					GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;
				}

				KEY_process_global_keys( pkey_event );
		}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

