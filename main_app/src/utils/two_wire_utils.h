/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_E_TWO_WIRE_UTILS_H
#define _INC_E_TWO_WIRE_UTILS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"cs3000_tpmicro_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	UNS_32	sn;

	UNS_16	fw_vers;

	UNS_8	type;

	UNS_8	buffer;

} DECODER_INFO_FOR_DECODER_LISTS;

extern DECODER_INFO_FOR_DECODER_LISTS decoder_info_for_display[ MAX_DECODERS_IN_A_BOX ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void TWO_WIRE_jump_to_decoder_list( const UNS_32 pdecoder__tpmicro_type );

extern void TWO_WIRE_turn_cable_power_on_or_off_from_ui( const BOOL_32 pon_or_off );

extern void TWO_WIRE_perform_discovery_process( const BOOL_32 pshow_progress_dialog, const BOOL_32 pcalled_from_ui );

extern void TWO_WIRE_clear_statistics_at_all_decoders_from_ui( void );

extern void TWO_WIRE_request_statistics_from_all_decoders_from_ui( void );

extern void TWO_WIRE_all_decoder_loopback_test_from_ui( const BOOL_32 pstart_or_stop );

extern void TWO_WIRE_decoder_solenoid_operation_from_ui( const UNS_32 pdecoder_sn, const UNS_32 poutput_0, const BOOL_32 pon_or_off );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

