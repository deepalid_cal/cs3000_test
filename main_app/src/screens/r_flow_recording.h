/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_R_FLOW_RECORDING_H
#define _INC_R_FLOW_RECORDING_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FDTO_FLOW_RECORDING_redraw_scrollbox( void );

extern void FDTO_FLOW_RECORDING_draw_report( const BOOL_32 pcomplete_redraw );

extern void FLOW_RECORDING_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

