/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_factory_reset.h"

#include	"alerts.h"

#include	"battery_backed_vars.h"

#include	"cursor_utils.h"

#include	"d_tech_support.h"

#include	"dialog.h"

#include	"flash_storage.h"

#include	"flowsense.h"

#include	"foal_irri.h"

#include	"foal_lights.h"

#include	"guilib.h"

#include	"m_main.h"

#include	"speaker.h"

#include	"wdt_and_powerfail.h"

#include	"configuration_network.h"

#include	"app_startup.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_FACTORY_RESET_CLEAR_ALERTS	(0)
#define CP_FACTORY_RESET_FULL_RESET		(1)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart( UNS_32 psystem_shutdown_event )
{
	// 3/10/2017 rmd : So here is what I believe is happening. I noticed that after I saved the
	// network configuration file (below) and added a 3 second vTaskDelay to this task to allow
	// that queued save to take place, the flash file task WAS NOT GETTING A CHANCE TO RUN. I
	// believe that after the battery backed vars were initialized if tdcheck runs there is
	// something not right taking place. And I suspect it is BUDGET related because when running
	// in the debugger I saw that infamous BUG DATE CAUGHT crap. Which skc has never understood.
	//
	// 3/10/2017 rmd : So the approach here is to do the battery backed initializations at the
	// end of the process here. Remember this is under control of the KEY_PROCESS task. Which is
	// at a higher priority than TDCHECK. So perform the file delete and save activity with the
	// associated vTaskDelay needed to allow the file save to complete BEFORE we initialize all
	// the battery backed. After we initialize the battery backed we fall right into the system
	// shutdown task so tdcheck DOES NOT HAVE AN OPPORTUNITY TO RUN.
	//
	// 3/10/2017 rmd : Boosting the flash task does help the file reads and write processing
	// time. But probably not by much. It does tend to block out tdcheck from running though.
	// The flash activity takes precedence.
	vTaskPrioritySet( flash_storage_1_task_handle, PRIORITY_LEVEL_10 );
	
	// 2/21/2014 rmd : This is NOT a queued activity. And therefore the actual file work takes
	// place within the context of THIS task. Which in this case is the KEY_PROCESSING task. IF
	// there were a POWER FAILURE to occur while this file deletion activity was going bad
	// things could happen. WHY? Because the high priority powerfail TASK dutifully suspends all
	// tasks except notably the FLASH_STORAGE task. And the power fail task sets the shutdown
	// impending flag that the flash task picks up on and GRACEFULLY backs out of its writing
	// activities. And the power fail task intentionally give some 50+ms to the flash task to
	// achieve this graceful exit from its activites. The intent being to avoid trashing the FAT
	// if the decaying power hard reset slammed at just the wrong time. This IS THE REASON why
	// we want to queue all file activites. To allow a graceful exit from such activites in the
	// face of an impending shutdown!
	//
	// 2/21/2014 rmd : IN SUMMARY this would be safer as a queued activity. Which it is not now.
	// If it were we could follow this function call with a brute force vTaskDelay for say 1
	// second to allow the files deletes to complete.
	FLASH_STORAGE_delete_all_files( FLASH_INDEX_1_GENERAL_STORAGE );
	
	// ----------
	
	// 3/10/2017 rmd : So all files have been deleted. As the comment stated above that happened
	// within the context of this task! So it is done now.
	//
	// 3/9/2017 rmd : Production performs a factory reset at the conclusion of their testing in
	// order to deliver a cleaned up unit to the customer (alerts wiped, all user settings back
	// to their default, etc). BUT WE DON'T WANT TO WIPE the NETWORK ID which is stored in the
	// network file. Doing so is expecially bad for controllers on a hub. Because the user
	// wouldn't be able to add them to their 'company' account without knowing the network id.
	// And if they can't add them to their account they can't assign them to a hub to
	// communicate with them. And communicating with them is the only way to restore the NID.
	// Well we just delelted the network config file. But it remains intact in memory. So doing
	// a save will bring it back to the file system. THIS FILE SAVE IS QUEUED.
	//
	// 4/19/2017 rmd : Note - because the we do this file save here the file is of course not
	// missing upon reboot, and therefore is not built. Rebuilding a missing file results in all
	// the bits set (to trigger xmisson to the commserver), sooo we have to explicitly set the
	// bits here to force the network file contents to be sent.
	NETWORK_CONFIG_on_all_settings_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );

	if( (psystem_shutdown_event == SYSTEM_SHUTDOWN_EVENT_factory_reset_via_cloud_panel_swap_for_new_panel) || (psystem_shutdown_event == SYSTEM_SHUTDOWN_EVENT_factory_reset_via_cloud_panel_swap_for_old_panel) )
	{
		// 6/19/2018 rmd : To successfully perform the panel swap the commserver needs to see a 0
		// NID, so make sure to set that here! Can't get at config_n directly, so use this function
		// call.
		NETWORK_CONFIG_brute_force_set_network_ID_to_zero();
	}

	save_file_configuration_network();
	
	// ----------
	
	// 3/10/2017 rmd : Allow the queued file save to take place. Yes the flash task is at a
	// higher priority (we boosted it upon entry to this function) but the file save process has
	// vTaskDelays in it. And that would allow this task to proceed here. And before the file
	// write FINISHED we'd be into the system shutdown task! So we need to delay here. Note -
	// this delay causes ICED TASK alerts. As a matter of fact during the delete ALL files above
	// files one iced task alert may show. We've already been in the key_process task for a long
	// time (>1 sec). But that's okay. It is not hurting anything and we are going to erase
	// those alerts when we wipe the alerts (just after the delay).
	vTaskDelay( MS_to_TICKS(1000) );
	
	// ----------
	
	// 9/3/2015 rmd : Yes the two variables in weather preserves used to control if registration
	// asks the web to clear its stored pdata and if we block sending of pdata are cleared by
	// this init. However they are explicitly set upon the restart using the system_shutdown
	// event reason. This reason is saved in the battery backed restart_info structure.
	init_battery_backed_weather_preserves( (true) );
	
	init_battery_backed_station_preserves( (true) );
	
	init_battery_backed_system_preserves( (true) );
	
	init_battery_backed_poc_preserves( (true) );
	
	init_battery_backed_foal_irri( (true) );
	
	init_battery_backed_chain_members( (true) );
	
	init_battery_backed_general_use( (true) );
	
	init_battery_backed_lights_preserves( (true) );

	init_battery_backed_foal_lights( (true) );

	// ----------
	
	// 3/10/2017 rmd : Now wipe the alerts. Hiding what took place. Including the part about
	// saving the network configuration file, the iced task alerts we generated, and
	// initializing the battery backed structures.
	ALERTS_restart_all_piles();
	
	// ----------

	// 5/9/2016 rmd : So far this function is called for 3 reasons. 1) When the user using the
	// keypad requests a factory reset. 2 & 3) For commands 109 and 111 from the commserver
	// performing a factory reset as part of a panel swap. We going to try this. And see what is
	// wrong with doing so, but for any of these reasons after the unit comes back up following
	// this reset, and the main board and tpmicro have completed their initial exchange, we are
	// going to ask the tpmicro to do a discovery.
	weather_preserves.perform_a_discovery_following_a_FACTORY_reset = (true);
	
	// ----------
	
	// 2/21/2014 rmd : Formally produce a restart.
	SYSTEM_application_requested_restart( psystem_shutdown_event );
}

/* ---------------------------------------------------------- */
extern void FDTO_FACTORY_RESET_draw_screen( void )
{
	GuiLib_ShowScreen( GuiStruct_scrFactoryReset_0, CP_FACTORY_RESET_FULL_RESET, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void FACTORY_RESET_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			good_key_beep();

			if( GuiLib_ActiveCursorFieldNo == CP_FACTORY_RESET_FULL_RESET )
			{
				DIALOG_draw_ok_dialog( GuiStruct_dlgResettingController_0 );

				// ----------
				
				FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart( SYSTEM_SHUTDOWN_EVENT_factory_reset_via_keypad );
			}
			else
			{
				ALERTS_restart_all_piles();
			}

			break;

		case KEY_C_LEFT:
			CURSOR_Up( (true) );
			break;

		case KEY_C_RIGHT:
			if( GuiLib_ActiveCursorFieldNo == CP_FACTORY_RESET_CLEAR_ALERTS )
			{
				CURSOR_Down( (true) );
			}
			else
			{
				bad_key_beep();
			}
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;

			KEY_process_global_keys( pkey_event );

			// 6/5/2015 ajv : Since the user got here from the TECHNICAL SUPPORT dialog box, redraw that
			// dialog box.
			TECH_SUPPORT_draw_dialog( (true) );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

