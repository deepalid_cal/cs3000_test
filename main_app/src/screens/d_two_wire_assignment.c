/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"d_two_wire_assignment.h"

#include	"app_startup.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"d_two_wire_discovery.h"

#include	"dialog.h"

#include	"e_moisture_sensors.h"

#include	"flowsense.h"

#include	"r_orphan_two_wire_pocs.h"

#include	"r_orphan_two_wire_stations.h"

#include	"screen_utils.h"

#include	"speaker.h"

#include	"tpmicro_data.h"

#include	"two_wire_utils.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_TWO_WIRE_PERFORM_DISCOVERY		(50)
#define	CP_TWO_WIRE_ASSIGNMENT_STATIONS		(51)
#define	CP_TWO_WIRE_ASSIGNMENT_POCS			(52)
#define	CP_TWO_WIRE_CONFIGURE_MOISTURE		(53)
#define	CP_TWO_WIRE_UNLINK_ORPHAN_STATIONS	(54)
#define	CP_TWO_WIRE_UNLINK_ORPHAN_POCS		(55)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

UNS_32	g_TWO_WIRE_ASSIGNMENT_cursor_position_when_dialog_displayed;

// 4/9/2016 ajv : Keep track of the last cursor position the user had selected on the dialog
// so it brings them back to the same field when they open the dialog again.
static UNS_32 g_TWO_WIRE_last_cursor_position;

BOOL_32	g_TWO_WIRE_dialog_visible;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void FDTO_TWO_WIRE_ASSIGNMENT_draw_dialog( const BOOL_32 pcomplete_redraw )
{
	UNS_32	i;

	UNS_32	lcursor_to_select;

	// ----------

	GuiVar_TwoWireDiscoveredStaDecoders = (false);
	GuiVar_TwoWireDiscoveredPOCDecoders = (false);
	GuiVar_TwoWireDiscoveredMoisDecoders = (false);

	GuiVar_TwoWireOrphanedStationsExist = (false);
	GuiVar_TwoWireOrphanedPOCsExist = (false);

	// ----------

	xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );

	for( i = 0; i < MAX_DECODERS_IN_A_BOX; ++i )
	{
		if( tpmicro_data.decoder_info[ i ].sn > 0 )
		{
			if( tpmicro_data.decoder_info[ i ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__2_STATION )
			{
				GuiVar_TwoWireDiscoveredStaDecoders = (true);
			}
			else if( tpmicro_data.decoder_info[ i ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__POC )
			{
				GuiVar_TwoWireDiscoveredPOCDecoders = (true);
			}
			else if( tpmicro_data.decoder_info[ i ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__MOISTURE )
			{
				GuiVar_TwoWireDiscoveredMoisDecoders = (true);

				// 4/12/2016 rmd : And because a moisture decoder can operate one station we also need to
				// show the station line.
				GuiVar_TwoWireDiscoveredStaDecoders = (true);
			}
		}
	}

	xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );

	// ----------

	GuiVar_TwoWireOrphanedStationsExist = (ORPHAN_TWO_WIRE_STATIONS_populate_list() > 0);

	GuiVar_TwoWireOrphanedPOCsExist = (ORPHAN_TWO_WIRE_POCS_populate_list() > 0);

	// ----------

	g_TWO_WIRE_ASSIGNMENT_cursor_position_when_dialog_displayed = (UNS_32)GuiLib_ActiveCursorFieldNo;

	if( pcomplete_redraw == (true) )
	{
		// 4/9/2016 ajv : Retain the last active cursor position
		if( g_TWO_WIRE_last_cursor_position == 0 )
		{
			g_TWO_WIRE_last_cursor_position = CP_TWO_WIRE_PERFORM_DISCOVERY;
		}
		lcursor_to_select = g_TWO_WIRE_last_cursor_position;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	// ----------

	g_TWO_WIRE_dialog_visible = (true);

	GuiLib_ShowScreen( GuiStruct_dlgTwoWireAssignment_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void TWO_WIRE_ASSIGNMENT_draw_dialog( const BOOL_32 pcomplete_redraw )
{
	DISPLAY_EVENT_STRUCT lde;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void *)&FDTO_TWO_WIRE_ASSIGNMENT_draw_dialog;
	lde._06_u32_argument1 = pcomplete_redraw;
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
extern void TWO_WIRE_ASSIGNMENT_process_dialog( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TWO_WIRE_PERFORM_DISCOVERY:
					if( COMM_MNGR_network_is_available_for_normal_use() )
					{
						// 4/9/2016 ajv : Remember the last menu item that was selected.
						g_TWO_WIRE_last_cursor_position = GuiLib_ActiveCursorFieldNo;

						g_TWO_WIRE_dialog_visible = (false);

						DIALOG_close_ok_dialog();

						TWO_WIRE_perform_discovery_process( (true), (true) );
					}
					else
					{
						bad_key_beep();

						DIALOG_draw_ok_dialog( GuiStruct_dlgChainNotReady_TwoWire_0 );
					}
					break;

				case CP_TWO_WIRE_ASSIGNMENT_STATIONS:
					// 4/9/2016 ajv : Remember the last menu item that was selected.
					g_TWO_WIRE_last_cursor_position = GuiLib_ActiveCursorFieldNo;

					g_TWO_WIRE_dialog_visible = (false);
					TWO_WIRE_jump_to_decoder_list( DECODER__TPMICRO_TYPE__2_STATION );
					break;

				case CP_TWO_WIRE_ASSIGNMENT_POCS:
					// 4/9/2016 ajv : Remember the last menu item that was selected.
					g_TWO_WIRE_last_cursor_position = GuiLib_ActiveCursorFieldNo;

					g_TWO_WIRE_dialog_visible = (false);
					TWO_WIRE_jump_to_decoder_list( DECODER__TPMICRO_TYPE__POC );
					break;

				case CP_TWO_WIRE_CONFIGURE_MOISTURE:
					// 4/9/2016 ajv : Remember the last menu item that was selected.
					g_TWO_WIRE_last_cursor_position = GuiLib_ActiveCursorFieldNo;

					g_TWO_WIRE_dialog_visible = (false);

					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._02_menu = SCREEN_MENU_SETUP;
					lde._03_structure_to_draw = GuiStruct_scrMoisture_1;
					lde._04_func_ptr = &FDTO_MOISTURE_SENSOR_draw_menu;
					lde._06_u32_argument1 = (true);
					lde.key_process_func_ptr = MOISTURE_SENSOR_process_menu;
					Change_Screen( &lde );
					break;

				case CP_TWO_WIRE_UNLINK_ORPHAN_STATIONS:
					// 4/9/2016 ajv : Remember the last menu item that was selected.
					g_TWO_WIRE_last_cursor_position = GuiLib_ActiveCursorFieldNo;

					g_TWO_WIRE_dialog_visible = (false);

					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._02_menu = SCREEN_MENU_SETUP;
					lde._03_structure_to_draw = GuiStruct_rptOrphanTwoWireStations_0;
					lde._04_func_ptr = &FDTO_ORPHAN_TWO_WIRE_STATIONS_draw_report;
					lde._06_u32_argument1 = (true);
					lde.key_process_func_ptr = ORPHAN_TWO_WIRE_STATIONS_process_report;
					Change_Screen( &lde );
					break;

				case CP_TWO_WIRE_UNLINK_ORPHAN_POCS:
					// 4/9/2016 ajv : Remember the last menu item that was selected.
					g_TWO_WIRE_last_cursor_position = GuiLib_ActiveCursorFieldNo;

					g_TWO_WIRE_dialog_visible = (false);

					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._02_menu = SCREEN_MENU_SETUP;
					lde._03_structure_to_draw = GuiStruct_rptOrphanTwoWirePOCs_0;
					lde._04_func_ptr = &FDTO_ORPHAN_TWO_WIRE_POCS_draw_report;
					lde._06_u32_argument1 = (true);
					lde.key_process_func_ptr = ORPHAN_TWO_WIRE_POCS_process_report;
					Change_Screen( &lde );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_C_UP:
			CURSOR_Up( (true) );
			break;

		case KEY_C_DOWN:
			CURSOR_Down( (true) );
			break;

		case KEY_BACK:
			// 4/9/2016 ajv : Remember the last menu item that was selected.
			g_TWO_WIRE_last_cursor_position = GuiLib_ActiveCursorFieldNo;

			g_TWO_WIRE_dialog_visible = (false);

			good_key_beep();

			// 3/13/2014 ajv : Return to the last cursor position the user was
			// on when they pressed BACK and redraw the screen to remove the
			// dialog box.
			GuiLib_ActiveCursorFieldNo = (INT_16)g_TWO_WIRE_ASSIGNMENT_cursor_position_when_dialog_displayed;

			Redraw_Screen( (false) );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

