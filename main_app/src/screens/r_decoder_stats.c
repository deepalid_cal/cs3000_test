/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include    "r_decoder_stats.h"

#include	"cursor_utils.h"

#include	"d_two_wire_debug.h"

#include	"m_main.h"

#include	"screen_utils.h"

#include	"speaker.h"

#include	"tpmicro_data.h"

#include	"two_wire_utils.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_TWO_WIRE_STATS_REQUEST_ALL	(0)
#define	CP_TWO_WIRE_STATS_CLEAR_ALL		(1)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32	g_TWO_WIRE_list_item_index;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param pdecoder_index_0 
 *
 * @author 1/30/2013 Adrianusv
 *
 * @revisions (none)
 */
static void TWO_WIRE_copy_decoder_stats_into_guivars( const UNS_32 pdecoder_index_0 )
{
	GuiVar_TwoWireDecoderSN = tpmicro_data.decoder_info[ pdecoder_index_0 ].sn;

	GuiVar_TwoWireStatsBODs = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.bod_resets;
	GuiVar_TwoWireStatsEEPCRCComParams = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.eep_crc_err_com_parms;
	GuiVar_TwoWireStatsEEPCRCErrStats = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.eep_crc_err_stats;
	GuiVar_TwoWireStatsEEPCRCS1Params = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.eep_crc_err_sol1_parms;
	GuiVar_TwoWireStatsEEPCRCS2Params = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.eep_crc_err_sol2_parms;
	GuiVar_TwoWireStatsRxCRCErrs = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.rx_crc_errs;
	GuiVar_TwoWireStatsRxDataLpbkMsgs = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.rx_data_lpbk_msgs;
	GuiVar_TwoWireStatsRxDecRstMsgs = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.rx_dec_rst_msgs;
	GuiVar_TwoWireStatsRxDiscConfMsgs = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.rx_disc_conf_msgs;
	GuiVar_TwoWireStatsRxDiscRstMsgs = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.rx_disc_rst_msgs;
	GuiVar_TwoWireStatsRxEnqMsgs = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.rx_enq_msgs;
	GuiVar_TwoWireStatsRxGetParamsMsgs = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.rx_get_parms_msgs;
	GuiVar_TwoWireStatsRxIDReqMsgs = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.rx_id_req_msgs;
	GuiVar_TwoWireStatsRxLongMsgs = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.rx_long_msgs;
	GuiVar_TwoWireStatsRxMsgs = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.rx_msgs;
	GuiVar_TwoWireStatsRxPutParamsMsgs = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.rx_put_parms_msgs;
	GuiVar_TwoWireStatsRxSolCtrlMsgs = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.rx_sol_ctl_msgs;
	GuiVar_TwoWireStatsRxSolCurMeasReqMsgs = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.rx_sol_cur_meas_req_msgs;
	GuiVar_TwoWireStatsRxStatReqMsgs = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.rx_stat_req_msgs;
	GuiVar_TwoWireStatsRxStatsReqMsgs = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.rx_stats_req_msgs;
	GuiVar_TwoWireStatsS1UCos = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.sol_1_ucos;
	GuiVar_TwoWireStatsS2UCos = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.sol_2_ucos;
	GuiVar_TwoWireStatsTxAcksSent = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.tx_acks_sent;
	GuiVar_TwoWireStatsWDTs = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.wdt_resets;

	GuiVar_TwoWireStatsPORs = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.por_resets;
	GuiVar_TwoWireStatsTempMax = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.temp_maximum;
	GuiVar_TwoWireStatsTempCur = tpmicro_data.decoder_info[ pdecoder_index_0 ].decoder_statistics.temp_current;
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 *
 * @author 2/5/2013 Adrianusv
 *
 * @revisions (none)
 */
extern void FDTO_TWO_WIRE_update_decoder_stats( void )
{
	TWO_WIRE_copy_decoder_stats_into_guivars( g_TWO_WIRE_list_item_index );

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param pcomplete_redraw 
 *
 * @author 1/29/2013 Adrianusv
 *
 * @revisions (none)
 */
extern void FDTO_TWO_WIRE_draw_decoder_stats_report( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_show;

	UNS_32	i;

	if( pcomplete_redraw == (true) )
	{
		g_TWO_WIRE_list_item_index = 0;

		lcursor_to_show = CP_TWO_WIRE_STATS_REQUEST_ALL;

		TWO_WIRE_copy_decoder_stats_into_guivars( g_TWO_WIRE_list_item_index );

		GuiVar_TwoWireNumDiscoveredStaDecoders = 0;

		// Run through the Decoder list to get a count 
		for( i = 0; i < MAX_DECODERS_IN_A_BOX; ++i )
		{
			if( tpmicro_data.decoder_info[ i ].sn != DECODER_SERIAL_NUM_DEFAULT )
			{
				++GuiVar_TwoWireNumDiscoveredStaDecoders;
			}
			else
			{
				break;
			}
		}
	}
	else
	{
		lcursor_to_show = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_rptDecoderStats_0, (INT_16)lcursor_to_show, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param pkey_event 
 *
 * @author 1/29/2013 Adrianusv
 *
 * @revisions (none)
 */
extern void TWO_WIRE_process_decoder_stats_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	INT_32	lprev_decoder_index;

	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TWO_WIRE_STATS_REQUEST_ALL:
					TWO_WIRE_request_statistics_from_all_decoders_from_ui();
					break;

				case CP_TWO_WIRE_STATS_CLEAR_ALL:
					TWO_WIRE_clear_statistics_at_all_decoders_from_ui();
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_NEXT:
		case KEY_PREV:
			lprev_decoder_index = (INT_32)g_TWO_WIRE_list_item_index;

			if( pkey_event.keycode == KEY_NEXT )
			{
				if( g_TWO_WIRE_list_item_index < (GuiVar_TwoWireNumDiscoveredStaDecoders - 1) )
				{
					g_TWO_WIRE_list_item_index++;
				}
			}
			else
			{
				if( g_TWO_WIRE_list_item_index > 0 )
				{
					g_TWO_WIRE_list_item_index--;
				}
			}

			if( lprev_decoder_index != (INT_32)g_TWO_WIRE_list_item_index )
			{
				good_key_beep();

				TWO_WIRE_copy_decoder_stats_into_guivars( g_TWO_WIRE_list_item_index );

				Refresh_Screen();
			}
			else
			{
				bad_key_beep();
			}
			break;

		case KEY_C_LEFT:
			CURSOR_Up( (true) );
			break;

		case KEY_C_RIGHT:
			CURSOR_Down( (true) );
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;

			KEY_process_global_keys( pkey_event );

			// 6/5/2015 ajv : Since the user got here from the 2-WIRE DEBUG dialog box, redraw that
			// dialog box.
			TWO_WIRE_DEBUG_draw_dialog( (true) );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

