/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for atoi
#include	<stdlib.h>

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"e_comm_test.h"

#include	"alert_parsing.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"cent_comm.h"

#include	"change.h"

#include	"comm_mngr.h"

#include	"configuration_controller.h"

#include	"configuration_network.h"

#include	"controller_initiated.h"

#include	"cursor_utils.h"

#include	"d_comm_options.h"

#include	"dialog.h"

#include	"flash_storage.h"

#include	"flow_recorder.h"

#include	"flowsense.h"

#include	"manual_programs.h"

#include	"poc_report_data.h"

#include	"screen_utils.h"

#include	"speaker.h"

#include	"station_groups.h"

#include	"station_history_data.h"

#include	"system_report_data.h"

#include	"weather_control.h"

#include	"moisture_sensors.h"

#include	"walk_thru.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/18/2015 ajv : User screen
#define	CP_COMM_TEST_SEND_CHECK_FOR_UPDATES		(0)
#define	CP_COMM_TEST_SEND_ALERTS				(1)
#define CP_COMM_TEST_SEND_PROGRAM_DATA			(2)

// ----------

// 5/18/2015 ajv : DEBUG screen
#define CP_COMM_TEST_SEND_REPORT_DATA			(5)
#define CP_COMM_TEST_SEND_WEATHER_DATA			(6)
#define CP_COMM_TEST_SEND_RAIN_INDICATION		(7)
#define CP_COMM_TEST_SUPPRESS_CONNECTIONS		(10)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void COMM_TEST_send_all_program_data_to_the_cloud( UNS_32 psave_the_files, UNS_32 pstart_the_pdata_timer )
{
	// 11/9/2015 rmd : Executed from 3 places.
	//
	// case ONE: is under keypad control when user request to send all program data to the cloud
	// (this is primarily a test functionality). We start the timer in this case. And do not
	// save the files.
	//
	// case TWO: is upon receipt of a communications command to send all the pdata to the cloud.
	// This is initiated from the WEB UI itself or some other WEB process (panel swap). We start
	// the timer for this reason but do not save the files.
	//
	// case THREE: is on a main board code update. On a chain, when the update introduces NEW
	// variables to the pdata, there is a complication getting that pdata to send to the cloud
	// (due to the restart after the scan & code distribution to the slaves). In this case we do
	// not want to start the timer but do need to save all the files.
	
	// ----------
	
	Alert_Message( "Setting ALL Program Data bits" );
	
	// ----------
	
	// 5/18/2015 ajv : Set ALL change bits to ensure all of program data is sent
	NETWORK_CONFIG_on_all_settings_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );

	if( psave_the_files == SEND_PDATA_TO_CLOUD_save_the_files )
	{
		// 11/9/2015 rmd : Do not need to use the time delayed file save. That was originally
		// designed to be used only when we knew we had repetitive resaons to write a particular
		// file (like station file at a scheduled start time). This direct call is what would be
		// called when the delayed file write timer expires. If memory is not available the timer
		// would be started so we would try again. But in this case I'm not sure that will always
		// produce the desired results (for example code receipt triggers a restart).
		save_file_configuration_network();
	}
	
	// ----------
	
	WEATHER_on_all_settings_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );
	
	if( psave_the_files == SEND_PDATA_TO_CLOUD_save_the_files )
	{
		// 11/9/2015 rmd : See note on configuration file save above.
		save_file_weather_control();
	}
	
	// ----------
	
	SYSTEM_on_all_systems_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );
	
	if( psave_the_files == SEND_PDATA_TO_CLOUD_save_the_files )
	{
		// 11/9/2015 rmd : See note on configuration file save above.
		save_file_irrigation_system();
	}
	
	// ----------
	
	STATION_GROUP_on_all_groups_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );
	
	if( psave_the_files == SEND_PDATA_TO_CLOUD_save_the_files )
	{
		// 11/9/2015 rmd : See note on configuration file save above.
		save_file_station_group();
	}
	
	// ----------
	
	MANUAL_PROGRAMS_on_all_groups_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );
	
	if( psave_the_files == SEND_PDATA_TO_CLOUD_save_the_files )
	{
		// 11/9/2015 rmd : See note on configuration file save above.
		save_file_manual_programs();
	}
	
	// ----------
	
	STATION_on_all_stations_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );
	
	if( psave_the_files == SEND_PDATA_TO_CLOUD_save_the_files )
	{
		// 11/9/2015 rmd : See note on configuration file save above.
		save_file_station_info();
	}
	
	// ----------
	
	POC_on_all_pocs_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );
	
	if( psave_the_files == SEND_PDATA_TO_CLOUD_save_the_files )
	{
		// 11/9/2015 rmd : See note on configuration file save above.
		save_file_POC();
	}
	
	// ----------
	
	MOISTURE_SENSOR_on_all_moisture_sensors_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );
	
	if( psave_the_files == SEND_PDATA_TO_CLOUD_save_the_files )
	{
		// 11/9/2015 rmd : See note on configuration file save above.
		save_file_moisture_sensor();
	}
	
	// ----------
	
	LIGHTS_on_all_lights_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );

	if( psave_the_files == SEND_PDATA_TO_CLOUD_save_the_files )
	{
		// 11/9/2015 rmd : See note on configuration file save above.
		save_file_LIGHTS();
	}
	
	// ----------
	
	WALK_THRU_on_all_groups_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );
	
	if( psave_the_files == SEND_PDATA_TO_CLOUD_save_the_files )
	{
		// 3/30/2018 Ryan : See note on configuration file save above.
		save_file_walk_thru();
	}
	
	// ----------
	
	// 9/1/2015 rmd : Set our pending flag in case of a power failure. Will guarantee another
	// try when power returns if the files have been saved (remember counts on bits set in the
	// files themselves).
	weather_preserves.pending_changes_to_send_to_comm_server = (true);
	
	// ----------
	
	if( pstart_the_pdata_timer == SEND_PDATA_TO_CLOUD_start_the_pdata_timer )
	{
		// 9/1/2015 rmd : And start the timer which triggers the data to be sent in 30 seconds.
		xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGE_MADE_MS ), portMAX_DELAY );
	}

}

/* ---------------------------------------------------------- */
#if ALERTS_ABILITY_TO_GENERATE_ALL_ALERTS_AND_REPORTS_FOR_DEBUG


static UNS_32	g_COMM_TEST_index_of_report_data_to_send;


static void COMM_TEST_send_report_data_to_the_cloud( void )
{
	switch( g_COMM_TEST_index_of_report_data_to_send )
	{
		/*
		case 0:
			CONTROLLER_INITIATED_post_event( CI_EVENT_send_system_report_data );
			g_COMM_TEST_index_of_report_data_to_send++;
			break;

		case 1:
			CONTROLLER_INITIATED_post_event( CI_EVENT_send_poc_report_data );
			g_COMM_TEST_index_of_report_data_to_send++;
			break;

		case 2:
			CONTROLLER_INITIATED_post_event( CI_EVENT_send_station_report_data );
			g_COMM_TEST_index_of_report_data_to_send++;
			break;

		case 3:
			CONTROLLER_INITIATED_post_event( CI_EVENT_send_station_history );
			g_COMM_TEST_index_of_report_data_to_send++;
			break;

		case 4:
			CONTROLLER_INITIATED_post_event( CI_EVENT_send_lights_report_data );

		*/
		default:
			g_COMM_TEST_index_of_report_data_to_send = 0;
	}
}

/* ---------------------------------------------------------- */
static void COMM_TEST_generate_report_data_lines_for_DEBUG( void )
{
	SYSTEM_REPORT_DATA_generate_lines_for_DEBUG();

	POC_REPORT_DATA_generate_lines_for_DEBUG();

	STATION_REPORT_DATA_generate_lines_for_DEBUG();

	STATION_HISTORY_generate_lines_for_DEBUG();

	LIGHTS_REPORT_DATA_generate_lines_for_DEBUG();

	FLOW_RECORDING_generate_lines_for_DEBUG();
}

#endif

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 *
 * @author 12/17/2013 AdrianusV
 *
 * @revisions (none)
 */
static void COMM_TEST_copy_settings_into_guivars( void )
{
	const char delimiter[] = ".";

	char str_48[ 48 ];

	strlcpy( str_48, config_c.comm_server_ip_address, sizeof(str_48) );

	GuiVar_CommTestIPOctect1 = (UNS_32)(atoi( strtok((char*)str_48, delimiter) ));
	GuiVar_CommTestIPOctect2 = (UNS_32)(atoi( strtok(NULL, delimiter) ));
	GuiVar_CommTestIPOctect3 = (UNS_32)(atoi( strtok(NULL, delimiter) ));
	GuiVar_CommTestIPOctect4 = (UNS_32)(atoi( strtok(NULL, delimiter) ));
		
	GuiVar_CommTestIPPort = atoi( config_c.comm_server_port );

	// 2/7/2017 rmd : Just over ride now. This variable is no longer used.
	GuiVar_CommTestSuppressConnections = (false);
}

/* ---------------------------------------------------------- */
extern void FDTO_COMM_TEST_draw_screen( const BOOL_32 pcomplete_redraw, const UNS_32 pkeycode )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		GuiVar_CommTestShowDebug = (pkeycode == KEY_HELP);

		COMM_TEST_copy_settings_into_guivars();

		lcursor_to_select = CP_COMM_TEST_SEND_CHECK_FOR_UPDATES;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrCommTest_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param pkey_event 
 *
 * @author 12/17/2013 AdrianusV
 *
 * @revisions (none)
 */
extern void COMM_TEST_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	UNS_32	lci_message_to_post;

	// 2/19/2015 ajv : There is no event 0. So, we'll use this to identify that we haven't set
	// an event yet.
	lci_message_to_post = 0;

	switch( pkey_event.keycode )
	{
		case KEY_HELP:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				#if ALERTS_ABILITY_TO_GENERATE_ALL_ALERTS_AND_REPORTS_FOR_DEBUG

					case CP_COMM_TEST_SEND_ALERTS:
						good_key_beep();

						ALERTS_generate_all_alerts_for_debug( (true) );
						break;

					case CP_COMM_TEST_SEND_REPORT_DATA:
						good_key_beep();

						COMM_TEST_generate_report_data_lines_for_DEBUG();

						break;

				#endif

				// ----------
				
				case CP_COMM_TEST_SEND_PROGRAM_DATA:
					good_key_beep();

					// 11/9/2015 rmd : Being under user control we will not save the files but will of course
					// start the timer. When the timer expires all the pdata will be sent. If there is a power
					// failure prior to sending the data nothing will be sent upon re-power (cause we didn't
					// save the files).
					COMM_TEST_send_all_program_data_to_the_cloud( SEND_PDATA_TO_CLOUD_do_not_save_the_files, SEND_PDATA_TO_CLOUD_start_the_pdata_timer );

					break;

				// ----------
				
				default:
					bad_key_beep();
			}

			break;

		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_COMM_TEST_SEND_CHECK_FOR_UPDATES:
					ALERTS_parse_comm_command_string( MID_TO_COMMSERVER_CHECK_FOR_UPDATES_init_packet, GuiVar_CommTestStatus, sizeof(GuiVar_CommTestStatus) );

					Alert_Message( "User-Initiated Check for Updates" );

					lci_message_to_post = CI_MESSAGE_send_check_for_updates;
					break;

				case CP_COMM_TEST_SEND_ALERTS:
					ALERTS_parse_comm_command_string( MID_TO_COMMSERVER_ENGINEERING_ALERTS_init_packet, GuiVar_CommTestStatus, sizeof(GuiVar_CommTestStatus) );

					Alert_Message( "User-Initiated Alerts" );

					lci_message_to_post = CI_MESSAGE_send_alerts;
					break;
				
				case CP_COMM_TEST_SEND_PROGRAM_DATA:
					// 11/11/2016 rmd : THIS keypress does not force the sending of pdata by setting all the
					// bits. To do that press the HELP key when on this cursor position.
					ALERTS_parse_comm_command_string( MID_TO_COMMSERVER_PROGRAM_DATA_init_packet, GuiVar_CommTestStatus, sizeof(GuiVar_CommTestStatus) );

					Alert_Message( "User-Initiated Program Data" );

					// 4/21/2015 rmd : This event cause a messages sent to the commserver containing our code
					// version strings. If up to date the commserver will ACK accordingly which directly
					// triggers us to send our program data to the commserver. So this is the start of that
					// sequence.
					lci_message_to_post = CI_MESSAGE_ask_commserver_if_our_firmware_is_up_to_date;
					break;

				#if ALERTS_ABILITY_TO_GENERATE_ALL_ALERTS_AND_REPORTS_FOR_DEBUG

				case CP_COMM_TEST_SEND_REPORT_DATA:
					good_key_beep();
					
					Alert_Message( "User-Initiated Report Data" );

					COMM_TEST_send_report_data_to_the_cloud();

					break;

				#endif

				// ----------

				case CP_COMM_TEST_SEND_WEATHER_DATA:
					ALERTS_parse_comm_command_string( MID_TO_COMMSERVER_WEATHER_DATA_init_packet, GuiVar_CommTestStatus, sizeof(GuiVar_CommTestStatus) );

					Alert_Message( "User-Initiated Weather Data" );

					// 6/16/2015 rmd : Note this event bypasses the timer. Which you probably want.
					lci_message_to_post = CI_MESSAGE_send_weather_data;
					break;

				case CP_COMM_TEST_SEND_RAIN_INDICATION:
					ALERTS_parse_comm_command_string( MID_TO_COMMSERVER_RAIN_INDICATION_init_packet, GuiVar_CommTestStatus, sizeof(GuiVar_CommTestStatus) );

					Alert_Message( "User-Initiated Rain Indication" );

					// 6/16/2015 rmd : Note this event bypasses the timer. Which you probably want.
					lci_message_to_post = CI_MESSAGE_send_rain_indication;
					break;

				// ----------

				default:
					bad_key_beep();
			}

			if( GuiLib_ActiveCursorFieldNo != CP_COMM_TEST_SUPPRESS_CONNECTIONS )
			{
				if( cics.mode != CI_MODE_ready )
				{
					strlcpy( GuiVar_CommTestStatus, "Waiting for device to connect...", sizeof(GuiVar_CommTestStatus) );

					// 4/17/2015 ajv : Refresh the auto redraw fields to ensure the screen reflects the current
					// status.
					Refresh_Screen();

					// 11/26/2014 ajv : Display a message to the user indicating that the device is booting and
					// that communications are currently unavailable
					DIALOG_draw_ok_dialog( GuiStruct_dlgCIDeviceNotConnected_0 );
				}
				else
				{
					// 2/12/2015 ajv : Since the user initiated a command, let them know that the controller is
					// attempting to connect. In the event of a successful connection, this will quickly be
					// replaced with a status of what's actually happening. However, in the event of a failed
					// connection, at least the user sees something to indicate something is going on.
					strlcpy( GuiVar_CommTestStatus, "Preparing to send...", sizeof(GuiVar_CommTestStatus) );

					// 4/17/2015 ajv : Refresh the auto redraw fields to ensure the screen reflects the current
					// status.
					Refresh_Screen();
				}
			}
			break;

		case KEY_MINUS:
		case KEY_PLUS:
			bad_key_beep();
			break;

		case KEY_C_UP:
		case KEY_C_LEFT:
			CURSOR_Up( (true) );
			break;

		case KEY_C_DOWN:
		case KEY_C_RIGHT:
			CURSOR_Down( (true) );
			break;

		default:
			if( pkey_event.keycode == KEY_BACK )
			{
				GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;
			}

			KEY_process_global_keys( pkey_event );

			if( pkey_event.keycode == KEY_BACK )
			{
				// 6/5/2015 ajv : Since the user got here from the COMM OPTIONS dialog box, redraw that
				// dialog box.
				COMM_OPTIONS_draw_dialog( (true) );
			}
	}

	if( lci_message_to_post )
	{
		good_key_beep();

		CONTROLLER_INITIATED_post_to_messages_queue( lci_message_to_post, NULL, CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD, CI_POST_TO_BACK );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

