
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for abs
#include	<stdlib.h>

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"e_test.h"

#include	"e_test_sequential.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"configuration_network.h"

#include	"cs3000_comm_server_common.h"

#include	"cursor_utils.h"

#include	"dialog.h"

#include	"flowsense.h"

#include	"irri_comm.h"

#include	"irri_irri.h"

#include	"irri_lights.h"

#include	"m_main.h"

#include	"r_irri_details.h"

#include	"screen_utils.h"

#include	"speaker.h"

#include	"stations.h"

#include	"flash_storage.h"

#include	"cs_mem.h"

#include	"irri_comm.h"

#include	"epson_rx_8025sa.h"


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_TEST_SEQ_STATION			     ( 0 )
#define CP_TEST_SEQ_ON_OFF			     ( 1 )
#define CP_TEST_SEQ_AUTO_MODE		     ( 2 )
#define CP_TEST_SEQ_AUTO_ON_TIME 	     ( 3 )
#define CP_TEST_SEQ_AUTO_OFF_TIME        ( 4 )
										 
#define TEST_SEQ_STATUS_UNKNOWN          ( 0 ) 
#define TEST_SEQ_STATUS_OFF              ( 1 ) 
#define TEST_SEQ_STATUS_ON               ( 2 ) 
#define TEST_SEQ_STATUS_WAITING          ( 3 ) 

#define TEST_SEQ_TIME_DISPLAY_OFF        ( 0 ) 
#define TEST_SEQ_TIME_DISPLAY_ON         ( 1 ) 
#define TEST_SEQ_TIME_DISPLAY_COMPLETE   ( 2 ) 

// 6/24/2015 mpd : This is for toggle buttons. If it's currently off, turn it on.
#define TEST_SEQ_TURN_ITEM_ON         ( false ) 

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	TEST_SEQ_MINUTE_IN_SECONDS			( 60 )
#define	TEST_SEQ_WEEK_IN_SECONDS			( 604800 )
#define	TEST_SEQ_HOUR_IN_SECONDS			( 3600 )
#define	TEST_SEQ_AUTO_ON_IN_SECONDS			( 3 )
#define	TEST_SEQ_AUTO_OFF_IN_SECONDS		( 0 )
#define	TEST_SEQ_AUTO_LIMIT_IN_SECONDS		( 60 )

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32 g_TEST_SEQ_last_cursor_pos = CP_TEST_SEQ_ON_OFF;
static BOOL_32 g_TEST_SEQ_auto_off_delay = false;
static UNS_32 g_TEST_SEQ_auto_starting_station;
static UNS_32 g_TEST_SEQ_auto_starting_box;
//static BOOL_32 g_TEST_SEQ_pump_active_output = true;
static UNS_32 g_TEST_SEQ_previous_station;
static UNS_32 g_TEST_SEQ_irrigation_station_min = TEST_SEQ_IRRI_STATION_NUM_MIN;
static UNS_32 g_TEST_SEQ_irrigation_station_max = TEST_SEQ_IRRI_STATION_NUM_MAX;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void TEST_SEQ_set_test_time( UNS_32 time_in_seconds )
{
	GuiVar_TestStationTimeRemaining = time_in_seconds;

	// 6/24/2015 mpd : Don't display count-down for ridiculously long times.
	if( time_in_seconds < TEST_SEQ_HOUR_IN_SECONDS )
	{
		GuiVar_TestStationTimeDisplay = TEST_SEQ_TIME_DISPLAY_ON;
	}
	else
	{
		GuiVar_TestStationTimeDisplay = TEST_SEQ_TIME_DISPLAY_OFF;
	}

}

/* ---------------------------------------------------------- */
static BOOL_32 TEST_SEQ_master_valve_output_is_energized()
{
	BOOL_32 mv_energized = false;

	// 7/9/2015 mpd : This is for a stand-alone box. It will not work for a chain. 

	// 7/9/2015 mpd : Take the POC MUTEX before the call since the first param cannot be modified without it.
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	if( poc_preserves.poc[ 0 ].ws[ 0 ].pbf.master_valve_energized_irri )
	{
		mv_energized = true;
	}

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

	return( mv_energized );

}

/* ---------------------------------------------------------- */
static void TEST_SEQ_master_valve_control( BOOL_32 action )
{
	// 7/9/2015 mpd : This is for a stand-alone box. It will not work for a chain. 

	// 7/9/2015 mpd : Take the POC MUTEX before the call since the first param cannot be modified without it.
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	if( action == STATION_ON )
	{
		// 7/10/2015 mpd : FUTURE: Lower level code prevents the Master Valve from remaining on with the shortcuts
		// used in this screen turning on irrigation stations. There is no MV ON function at this time.
		//POC_PRESERVES_set_master_valve_energized_bit( &(poc_preserves.poc[ 0 ].ws[ 0 ]), MASTER_VALVE_ACTION_OPEN );
		//poc_preserves.poc[ 0 ].this_pocs_system_preserves_ptr->sbf.master_valve_open_all = true;
	}
	else
	{
		//POC_PRESERVES_set_master_valve_energized_bit( &(poc_preserves.poc[ 0 ].ws[ 0 ]), MASTER_VALVE_ACTION_CLOSE );
		//poc_preserves.poc[ 0 ].this_pocs_system_preserves_ptr->sbf.master_valve_open_all = false;
	}

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
static BOOL_32 TEST_SEQ_pump_output_is_energized()
{
	BOOL_32 pump_energized = false;

	// 7/9/2015 mpd : This is for a stand-alone box. It will not work for a chain. 

	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	if( poc_preserves.poc[ 0 ].ws[ 0 ].pbf.pump_energized_irri )
	{
		pump_energized = true;
	}

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

	return( pump_energized );

}

/* ---------------------------------------------------------- */
static void TEST_SEQ_pump_control( BOOL_32 action )
{
	// 7/9/2015 mpd : Safety requirements (i.e. a station must be on to operate the pump) must be handled by the caller.  
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	if( action == STATION_ON )
	{
		// 7/10/2015 mpd : This is for testing pump output. We don't care about active types. Just turn the output on.
		//poc_preserves.poc[ 0 ].ws[ 0 ].pbf.pump_energized_irri = g_TEST_SEQ_pump_active_output;

		// 7/10/2015 mpd : FUTURE: Lower level code prevents the Master Valve from remaining on with the shortcuts
		// used in this screen turning on irrigation stations. Without a MV, the pump will not go on. There is no pump
		// ON function at this time.
		//poc_preserves.poc[ 0 ].ws[ 0 ].pbf.pump_energized_irri = true;
	}
	else
	{
		//poc_preserves.poc[ 0 ].ws[ 0 ].pbf.pump_energized_irri = !g_TEST_SEQ_pump_active_output;
		//poc_preserves.poc[ 0 ].ws[ 0 ].pbf.pump_energized_irri = false;
	}

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
static void TEST_SEQ_light_control( UNS_32 light, BOOL_32 action )
{
	DATE_TIME ldt;

	// ----------

	EPSON_obtain_latest_time_and_date( &ldt );

	// 7/10/2015 mpd : Turn indicated light (0 - 3) on or off depending on "action"..

	// 9/4/2015 mpd : This is for a stand alone box. It will not work in a chain where lights are identified 0 - 47.
	if( action == STATION_OFF )
	{
		IRRI_LIGHTS_request_light_off( light );
	}
	else
	{
		// 9/4/2015 mpd : Turn the light on for 1 minute as we do for stations.
		IRRI_LIGHTS_request_light_on( light, ldt.D, ( ldt.T + TEST_SEQ_MINUTE_IN_SECONDS ) );
	}

}

/* ---------------------------------------------------------- */
static BOOL_32 TEST_SEQ_verify_station_status( BOOL_32 expect_on )
{
	BOOL_32 status_matches_expected_value = false;

	//IRRI_MAIN_LIST_OF_STATIONS_STRUCT	*imlss;

	// 6/23/2015 mpd : If "expect_on" is true, there should be exactly one station on the irri list. It better be the
	// one we turned on. If "expect_on" is false, there should be no stations on the list.

	// 7/13/2015 mpd : This verification was written back when this code requested a station to be on the IRRI list. Now
	// that we directly put the station on the list, it does not add any value. Just return true.
	status_matches_expected_value = true;

	#if 0
	// 7/9/2015 mpd : The "GuiVar_StationInfoNumber" variable is used for irrigation stations (beginning at 1) as well 
	// as the master valve, pump, and lights outputs. Does the current station number represent an irrigation station or 
	// one of the special outputs?
	if( GuiVar_StationInfoNumber < STATION_NUMBER_FOR_SPECIALS_MIN )
	{
		// 7/9/2015 mpd : This station number is for irrigation. 
		// 6/23/2015 mpd : We are requesting two entries off this list. Make sure it does not change while we are doing so.
		xSemaphoreTakeRecursive( irri_irri_recursive_MUTEX, portMAX_DELAY );

		imlss = nm_ListGetFirst( &irri_irri.list_of_irri_all_irrigation );

		if( imlss != NULL )
		{
			// 6/23/2015 mpd : There is a staion on the list. Dis we expect one to be on?
			if( expect_on )
			{
				// 6/23/2015 mpd : Is it the right station?
				if( ( GuiVar_StationInfoBoxIndex == imlss->box_index_0 ) &&
					( ( GuiVar_StationInfoNumber - 1 ) == imlss->station_number_0_u8 ) )
				{
					// 6/23/2015 mpd : It should be the only station.
					if( ( imlss = nm_ListGetNext( &irri_irri.list_of_irri_all_irrigation, imlss ) ) == NULL )
					{
						status_matches_expected_value = true;
					}
				}
			}
		}
		else
		{
			if( !expect_on )
			{
				status_matches_expected_value = true;
			}
		}

		xSemaphoreGiveRecursive( irri_irri_recursive_MUTEX );

	}
	else
	{
		// 7/9/2015 mpd : This non-irrigation station number requires special handling. 
		switch( GuiVar_StationInfoNumber )
		{
			// 7/10/2015 mpd : FUTURE: Add real checks when there is real functionality.
			case STATION_NUMBER_FOR_MASTER_VALVE:
			case STATION_NUMBER_FOR_PUMP:
			case STATION_NUMBER_FOR_LIGHT_1:
			case STATION_NUMBER_FOR_LIGHT_2:
			case STATION_NUMBER_FOR_LIGHT_3:
			case STATION_NUMBER_FOR_LIGHT_4:
			default:
				status_matches_expected_value = true;
				break;
		}
	}
	#endif

	return( status_matches_expected_value );

}

/* ---------------------------------------------------------- */
static void TEST_SEQ_copy_settings_into_guivars( const BOOL_32 pcomplete_redraw )
{
	STATION_STRUCT *lstation;
	UNS_32 light_index;

	NETWORK_CONFIG_get_controller_name_str_for_ui( GuiVar_StationInfoBoxIndex, (char*)&GuiVar_StationInfoControllerName );

	// 7/9/2015 mpd : The "GuiVar_StationInfoNumber" variable is used for irrigation stations (beginning at 1) as well 
	// as the master valve, pump, and lights outputs. Does the current station number represent an irrigation station or 
	// one of the special outputs?
	if( GuiVar_StationInfoNumber < STATION_NUMBER_FOR_SPECIALS_MIN )
	{
		// 7/9/2015 mpd : This station number is for irrigation. 

		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		// 7/13/2015 ajv : Note that using GuiVar_StationInfoNumber on this screen can cause quite a
		// bit of unexpected behavior throughout the controller. The reason is this is a shared
		// variable that's used throughout the controller to retain the last station a user was
		// working with.
		// 
		// For example, if a user was last viewing Station 2's programming, any screens they access
		// afterwards will default to showing station 2's information. By allowing this variable to
		// point to invalid station numbers, such as those assigned to
		// STATION_NUMBER_FOR_MASTER_VALVE, PUMP, and LIGHT1-L4, the user may try to access screens
		// using an invalid index.
		// 
		// In the near term, the box index and station number should be changed over to their own
		// easyGUI variables. Long-term, however, we also need to ensure any screens and reports
		// that try to access these variables have range-checking to ensure no invalid data is
		// displayed. The Test screen, for example, protects against this, but I don't believe the
		// reports do.
		lstation = nm_STATION_get_pointer_to_station( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1) );

		if( lstation != NULL )
		{
			strlcpy( GuiVar_StationDescription, nm_GROUP_get_name( lstation ), NUMBER_OF_CHARS_IN_A_NAME );

			STATION_get_station_number_string( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1), (char*)&GuiVar_StationInfoNumber_str, sizeof(GuiVar_StationInfoNumber_str) );

			// 6/23/2015 mpd : TBD: There may be an unacceptable time lag between requesting the station to be ON and 
			// actually seeing it on the IRRI list.
			if( TEST_SEQ_verify_station_status( GuiVar_TestStationOnOff == STATION_ON ) )
			{
				( GuiVar_TestStationOnOff == STATION_ON ) ? ( GuiVar_TestStationStatus = TEST_SEQ_STATUS_ON ) : ( GuiVar_TestStationStatus = TEST_SEQ_STATUS_OFF );
			}
			else
			{
				GuiVar_TestStationStatus = TEST_SEQ_STATUS_WAITING; 
			}
		}
		else
		{
			strlcpy( GuiVar_StationDescription, "", NUMBER_OF_CHARS_IN_A_NAME );

			strlcpy( GuiVar_StationInfoNumber_str, "", sizeof(GuiVar_StationInfoNumber_str) );

			strlcpy( GuiVar_StationInfoControllerName, "", sizeof(GuiVar_StationInfoControllerName) );

			GuiVar_TestStationStatus = TEST_SEQ_STATUS_UNKNOWN; 
		}

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}
	else
	{
		// 7/9/2015 mpd : This non-irrigation station number requires special handling. 
		switch( GuiVar_StationInfoNumber )
		{
			case STATION_NUMBER_FOR_MASTER_VALVE:
				// 7/13/2015 ajv : Note that this string needs to be moved over into easyGUI if we're going
				// to display it. However, also consider that the description field is actually no longer
				// displayed on the screen, so we may want to just consider removing this all together.
				strlcpy( GuiVar_StationDescription, "Master Valve", sizeof( GuiVar_StationDescription ) );
				strlcpy( GuiVar_StationInfoNumber_str, STATION_STRING_FOR_MASTER_VALVE, sizeof( GuiVar_StationInfoNumber_str ) );
				TEST_SEQ_master_valve_output_is_energized() ? ( GuiVar_TestStationStatus = TEST_SEQ_STATUS_ON ) : ( GuiVar_TestStationStatus = TEST_SEQ_STATUS_OFF );
				break;

			case STATION_NUMBER_FOR_PUMP:
				// 7/13/2015 ajv : Note that this string needs to be moved over into easyGUI if we're going
				// to display it. However, also consider that the description field is actually no longer
				// displayed on the screen, so we may want to just consider removing this all together.
				strlcpy( GuiVar_StationDescription, "Pump", sizeof( GuiVar_StationDescription ) );
				strlcpy( GuiVar_StationInfoNumber_str, STATION_STRING_FOR_PUMP, sizeof( GuiVar_StationInfoNumber_str ) );
				TEST_SEQ_pump_output_is_energized() ? ( GuiVar_TestStationStatus = TEST_SEQ_STATUS_ON ) : ( GuiVar_TestStationStatus = TEST_SEQ_STATUS_OFF );
				break;

			case STATION_NUMBER_FOR_LIGHT_1:
			case STATION_NUMBER_FOR_LIGHT_2:
			case STATION_NUMBER_FOR_LIGHT_3:
			case STATION_NUMBER_FOR_LIGHT_4:
				light_index = ( GuiVar_StationInfoNumber - STATION_NUMBER_FOR_LIGHT_1 );
				// 7/13/2015 ajv : Note that this string needs to be moved over into easyGUI if we're going
				// to display it. However, also consider that the description field is actually no longer
				// displayed on the screen, so we may want to just consider removing this all together.
				snprintf( GuiVar_StationDescription, sizeof( GuiVar_StationDescription ), "Light %d", ( light_index + 1 ) );
				snprintf( GuiVar_StationInfoNumber_str, sizeof( GuiVar_StationInfoNumber_str ), "L%d", ( light_index + 1 ) );
				IRRI_LIGHTS_light_is_energized( light_index ) ? ( GuiVar_TestStationStatus = TEST_SEQ_STATUS_ON ) : ( GuiVar_TestStationStatus = TEST_SEQ_STATUS_OFF );
				break;

			default:
				strlcpy( GuiVar_StationDescription, "", NUMBER_OF_CHARS_IN_A_NAME );
				strlcpy( GuiVar_StationInfoNumber_str, "", sizeof(GuiVar_StationInfoNumber_str) );
				strlcpy( GuiVar_StationInfoControllerName, "", sizeof(GuiVar_StationInfoControllerName) );
				Alert_Message( "TS1" );
				break;
		}
	}

}

#if 0
/* ---------------------------------------------------------- */
static BOOL_32 TEST_SEQ_set_pump_active_output()
{
	BOOL_32 rv = true;
	BY_SYSTEM_RECORD	*lbsr;

	// 7/9/2015 mpd : This function is probably unnecessary for manufacturing tests. The pump output should always be
	// active when energized.

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	// 7/9/2015 mpd : This is for a stand-alone box. It will not work for a chain. 
	lbsr = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( 0 );

	if( lbsr != NULL )
	{
		g_TEST_SEQ_pump_active_output = lbsr->sbf.pump_activate_output;
	}
	else
	{
		rv = false;
	}

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	return( rv );

}
#endif

// 7/9/2015 mpd : This function tested for the target station before performing "KEY_STOP" functionality. It worked at 
// the expense of added communication time. It has been replaced by brute force in "TEST_SEQ_turn_off_current_station()".
#if 0
/* ---------------------------------------------------------- */
static void TEST_SEQ_turn_off_current_station()
{
	STATION_STRUCT *lstation;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation = nm_STATION_get_pointer_to_station( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1) );

	if( lstation != NULL )
	{
		// 6/23/2015 mpd : TBD: Is there a better way to stop one station? For now, perform the same steps as key 
		// processing does upon receiving a KEY_STOP. Original comments can be found in "k_process.c" beginning at 
		// line 201.
		xSemaphoreTakeRecursive( irri_irri_recursive_MUTEX, portMAX_DELAY );

		if( irri_irri.list_of_irri_all_irrigation.count > 0 )
		{
			irri_comm.send_stop_key_record = (true);

			memset( &irri_comm.stop_key_record, 0x00, sizeof(STOP_KEY_RECORD_s) );

			irri_comm.stop_key_record.stop_for_the_highest_reason_in_all_systems = (true);
		}

		xSemaphoreGiveRecursive( irri_irri_recursive_MUTEX );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// 6/23/2015 mpd : Update the ON/OFF flag.
	GuiVar_TestStationOnOff = false;

	// 6/23/2015 mpd : We are in Auto mode. Set the count-down timer so we will know when to turn on the next station.
	if( GuiVar_TestStationAutoMode )
	{
		GuiVar_TestStationTimeRemaining = GuiVar_TestStationAutoOffTime;
	}

}
#endif

/* ---------------------------------------------------------- */
static void TEST_SEQ_turn_off_all_stations( const BOOL_32 pstop_key_pressed )
{
	IRRI_MAIN_LIST_OF_STATIONS_STRUCT	*liml_ptr, *tmp_liml_ptr;

	// 7/9/2015 mpd : This is a brute force test function that turns off the pump and master valve, then removes all
	// stations from the IRRI list. This is performed by a professional driver on a closed course. Adult supervision is 
	// required. Do not try this at home. If it runs for over four hours, seek immediate medical help.

	// 7/9/2015 mpd : The TP Micro will complain if the pump is on with no stations active. Turn it off first.
	TEST_SEQ_pump_control( STATION_OFF );
	TEST_SEQ_master_valve_control( STATION_OFF );
	TEST_SEQ_light_control( 0, STATION_OFF );
	TEST_SEQ_light_control( 1, STATION_OFF );
	TEST_SEQ_light_control( 2, STATION_OFF );
	TEST_SEQ_light_control( 3, STATION_OFF );

	// We are modifying the irri irrigation list. So better had take the MUTEX.
	xSemaphoreTakeRecursive( irri_irri_recursive_MUTEX, portMAX_DELAY );

	// ----------

	liml_ptr = nm_ListGetFirst( &irri_irri.list_of_irri_all_irrigation );

	while( liml_ptr != NULL )
	{
		// 7/9/2015 mpd : This is for a stand-alone box. It will not work for a chain. 

		// Okay so it is in the list. Delete the item.
		tmp_liml_ptr = liml_ptr;  // Save. THIS IS THE ONE WE ARE TO DELETE!

		liml_ptr = nm_ListGetNext( &irri_irri.list_of_irri_all_irrigation, liml_ptr );

		// Any housekeeping to do here? We are abruptly removing from the list. So far not. We count
		// on duplicate behaviour here in this irri list as in the master foal list. For example for
		// the case of MANUAL_PROGRAM with additional starts while still working off the prior one.
		// Both side modify their existing list record.

		if( nm_ListRemove( &irri_irri.list_of_irri_all_irrigation, tmp_liml_ptr ) != MIST_SUCCESS )
		{
			Alert_Message( "TS2" );
		}

		// And do not forget to free the associated memory.
		mem_free( tmp_liml_ptr );
	}

	xSemaphoreGiveRecursive( irri_irri_recursive_MUTEX );

	// 6/23/2015 mpd : Update the ON/OFF flag.
	GuiVar_TestStationOnOff = STATION_OFF;
	g_TEST_SEQ_previous_station = TEST_SEQ_IRRI_STATION_NUM_INVALID;


	// 7/13/2015 ajv : Turn off Auto mode and the timer display
	GuiVar_TestStationAutoMode = false;
	GuiVar_TestStationTimeDisplay = TEST_SEQ_TIME_DISPLAY_OFF;

	// ----------

	// 7/13/2015 ajv : Post a message to redraw the screen for the UI to reflect the changes.
	// Otherwise, certain screen features may not disappear as they're supposed to once the
	// outputs are turned off.
	Redraw_Screen( (false) );

	// ----------

	if( pstop_key_pressed )
	{
		// 7/13/2015 ajv : Since we've intercepted the STOP key, display the Stop dialog so the user
		// knows their stop command worked. However, since we're drawing the dialog abnormally, make
		// sure we set the reason in the list to TEST.
		GuiVar_StopKeyReasonInList = IN_LIST_FOR_TEST;

		DIALOG_draw_ok_dialog( GuiStruct_dlgStop_0 );
	}
}

/* ---------------------------------------------------------- */
static BOOL_32 TEST_SEQ_put_single_station_on_irri_list( UNS_32 station )
{
	IRRI_MAIN_LIST_OF_STATIONS_STRUCT	*liml_ptr, *tmp_liml_ptr;
	STATION_FROM_MAIN_LIST_DISTRIBUTION_STRUCT lsds;
	BOOL_32 rv = true;

	// 7/9/2015 mpd : This is a brute force test function that directly manipulates the IRRI list. 
	// 1. If the IRRI list is empty, it will add the station to the list.
	// 2. If a station is on the list, it will change the station number to the current station and set the remaining 
	// time to "GuiVar_TestStationTimeRemaining".
	// 3. If any additional stations are on the IRRI list, they will be removed.
	// 
	// This is performed by a professional driver on a closed course. Adult supervision is required. Do not try this 
	// at home. If it runs for over four hours, seek immediate medical help.

	// We are modifying the irri irrigation list. So better had take the MUTEX.
	xSemaphoreTakeRecursive( irri_irri_recursive_MUTEX, portMAX_DELAY );

	// ----------

	liml_ptr = nm_ListGetFirst( &irri_irri.list_of_irri_all_irrigation );

	if( liml_ptr == NULL )
	{
		// 7/9/2015 mpd : The list is empty. Add the station to the list in a semi-legitimate manner.
		lsds.system_gid = 0;
		lsds.requested_irrigation_seconds_u32 = GuiVar_TestStationTimeRemaining;
		lsds.soak_seconds_remaining = 0;
		lsds.cycle_seconds = GuiVar_TestStationTimeRemaining;
		lsds.box_index_0 = 0;
		lsds.station_number_0_u8 = station;
		lsds.reason_in_list_u8 = IN_LIST_FOR_TEST;
		irri_add_to_main_list_for_test_sequential( &lsds );
		liml_ptr = nm_ListGetFirst( &irri_irri.list_of_irri_all_irrigation );
		liml_ptr->remaining_ON_or_soak_seconds = GuiVar_TestStationTimeRemaining;
		liml_ptr->ibf.station_is_ON = true;
	}
	else
	{
		// 7/9/2015 mpd : There is a station on the list. Use that entry for the hot swap. Just modify the fields that 
		// need to be changed. The rest should be fine from the initial "irri_add_to_main_list()" call. This is a test
		// environment. We are not checking box indexes, GIDs, or station numbers. Just change it.
		liml_ptr->requested_irrigation_seconds_u32 = GuiVar_TestStationTimeRemaining;
		liml_ptr->remaining_ON_or_soak_seconds = GuiVar_TestStationTimeRemaining;
		liml_ptr->ibf.station_is_ON = true;
		liml_ptr->station_number_0_u8 = station;

		// 7/9/2015 mpd : See if there are any other stations on the list. There should not be.
		if( ( liml_ptr = nm_ListGetNext( &irri_irri.list_of_irri_all_irrigation, liml_ptr ) ) != NULL )
		{
			Alert_Message( "TS3" );
			rv = false;
		}

		while( liml_ptr != NULL )
		{
			// 7/9/2015 mpd : We should never get here.

			// Delete the item.
			tmp_liml_ptr = liml_ptr;  // Save. THIS IS THE ONE WE ARE TO DELETE!

			liml_ptr = nm_ListGetNext( &irri_irri.list_of_irri_all_irrigation, liml_ptr );

			if( nm_ListRemove( &irri_irri.list_of_irri_all_irrigation, tmp_liml_ptr ) != MIST_SUCCESS )
			{
				Alert_Message( "TS4" );
			}

			mem_free( tmp_liml_ptr );
		}
	}

	xSemaphoreGiveRecursive( irri_irri_recursive_MUTEX );

	return( rv );

}

/* ---------------------------------------------------------- */
static BOOL_32 TEST_SEQ_station_sequential_energize()
{
	BOOL_32 rv = true;

	// 7/9/2015 mpd : This is a brute force test function that controls the output to irrigation stations, the master
	// valve, pump, and lights in the most direct way possible. It does not use civil FOAL/IRRI communication paths.
	//  
	// This is performed by a professional driver on a closed course. Adult supervision is required. Do not try this 
	// at home. If it runs for over four hours, seek immediate medical help.

	// 7/9/2015 mpd : This is for a stand-alone box. It will not work for a chain. 

	// 7/9/2015 mpd : The "GuiVar_StationInfoNumber" variable is used for irrigation stations (beginning at 1) as well 
	// as the master valve, pump, and lights outputs. Station control requires special handling as the user transitions
	// from one type to another.

	if( GuiVar_StationInfoNumber < STATION_NUMBER_FOR_SPECIALS_MIN )
	{
		// 7/9/2015 mpd : This station number indicates an irrigation station.

		if( g_TEST_SEQ_previous_station > TEST_SEQ_IRRI_STATION_NUM_MAX )
		{
			// 7/10/2015 mpd : This is a non-irri to irri transition. There may still be non-irrigation outputs on.
			TEST_SEQ_turn_off_all_stations( (false) );
		}

		rv = TEST_SEQ_put_single_station_on_irri_list( ( GuiVar_StationInfoNumber - 1 ) );
	}
	else
	{
		// 7/9/2015 mpd : This non-irrigation station number requires special handling. 

		// 7/10/2015 mpd : There may still be non-irrigation outputs on. Turn off the previous non-irrigation station.
		// Don't worry about any irrigation stations that may still be on. There is no default case.
		switch( g_TEST_SEQ_previous_station )
		{
			case STATION_NUMBER_FOR_MASTER_VALVE:
				// 7/10/2015 mpd : Only turn off the master valve if we are not turning on the pump. The pump needs it.
				if( GuiVar_StationInfoNumber != STATION_NUMBER_FOR_PUMP )
				{
					TEST_SEQ_master_valve_control( STATION_OFF );
				}
				break;

			case STATION_NUMBER_FOR_PUMP:
				TEST_SEQ_pump_control( STATION_OFF );
				break;

			case STATION_NUMBER_FOR_LIGHT_1:
			case STATION_NUMBER_FOR_LIGHT_2:
			case STATION_NUMBER_FOR_LIGHT_3:
			case STATION_NUMBER_FOR_LIGHT_4:
				TEST_SEQ_light_control( ( g_TEST_SEQ_previous_station - STATION_NUMBER_FOR_LIGHT_1 ), STATION_OFF );
				break;
		}

		// 7/10/2015 mpd : Now turn on the current station.
		switch( GuiVar_StationInfoNumber )
		{
			case STATION_NUMBER_FOR_MASTER_VALVE:
				// 7/10/2015 mpd : A station must be on for the TP Micro to energize pump output. 
				rv = TEST_SEQ_put_single_station_on_irri_list( g_TEST_SEQ_irrigation_station_min - 1 );
				TEST_SEQ_master_valve_control( STATION_ON );
				break;

			case STATION_NUMBER_FOR_PUMP:
				// 7/10/2015 mpd : A station must be on for the TP Micro to energize pump output. 
				rv = TEST_SEQ_put_single_station_on_irri_list( g_TEST_SEQ_irrigation_station_min - 1 );
				TEST_SEQ_master_valve_control( STATION_ON );
				TEST_SEQ_pump_control( STATION_ON );
				break;

			case STATION_NUMBER_FOR_LIGHT_1:
			case STATION_NUMBER_FOR_LIGHT_2:
			case STATION_NUMBER_FOR_LIGHT_3:
			case STATION_NUMBER_FOR_LIGHT_4:
				TEST_SEQ_light_control( ( GuiVar_StationInfoNumber - STATION_NUMBER_FOR_LIGHT_1 ), STATION_ON );
				break;

			default:
				Alert_Message( "TS5" );
				rv = false;
				break;
		}
	}

	// 6/23/2015 mpd : Update the ON/OFF flag.
	if( rv )
	{
		GuiVar_TestStationOnOff = STATION_ON;
	}
	else
	{
		GuiVar_TestStationOnOff = STATION_OFF;
	}

	// 7/13/2015 ajv : Ensure the display is updated to reflect whether the station is ON or
	// not. Otherwise, when transitioning from non-irri to irri, the time display will disappear
	// but not reappear until the user navigates to the next station.
	GuiVar_TestStationTimeDisplay = (GuiVar_TestStationOnOff == STATION_ON);

	// 7/13/2015 ajv : And redraw the screen to ensure the time display appears or disappears
	// accordingly.
	Redraw_Screen( (false) );

	return( rv );

}

/* ---------------------------------------------------------- */
static void TEST_SEQ_change_target_station_number( BOOL_32 increment )
{
	STATION_STRUCT		*lstation;

	// 7/9/2015 mpd : This is for a stand-alone box. It will not work for a chain. Calls to STATION functions do not
	// test for NULL return values.

	// 7/9/2015 mpd : The "GuiVar_StationInfoNumber" variable is used for irrigation stations (beginning at 1) as well 
	// as the master valve, pump, and lights outputs. Station number navigation must handle both ranges of values. 

	// 7/9/2015 mpd : Save the current irrigation station info.
	g_TEST_SEQ_previous_station = GuiVar_StationInfoNumber;

	if( increment )
	{
		if( GuiVar_StationInfoNumber < STATION_NUMBER_FOR_SPECIALS_MIN )
		{
			// 7/9/2015 mpd : This station number indicates an irrigation station.
			if( GuiVar_StationInfoNumber < g_TEST_SEQ_irrigation_station_max )
			{
				// 7/10/2015 mpd : There are higher irrigation station numbers. 
				xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

				lstation = STATION_get_next_available_station( &GuiVar_StationInfoBoxIndex, &GuiVar_StationInfoNumber );

				xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
			}
			else
			{
				// 7/9/2015 mpd : The current station is the highest irrigation station in the box. Jump to the first 
				// non-irrigation station number.
				GuiVar_StationInfoNumber = STATION_NUMBER_FOR_SPECIALS_MIN;
			}
		}
		else 
		{
			// 7/9/2015 mpd : This non-irrigation station number requires special handling. 
			if( GuiVar_StationInfoNumber < STATION_NUMBER_FOR_SPECIALS_MAX )
			{
				// 7/10/2015 mpd : There are higher special station numbers. 
				GuiVar_StationInfoNumber++;
			}
			else
			{
				// 7/9/2015 mpd : The current station is the highest special station in the box. Jump to the first 
				// irrigation station number.
				GuiVar_StationInfoNumber = g_TEST_SEQ_irrigation_station_min;
			}
		}
	}
	else // Decrement
	{
		if( GuiVar_StationInfoNumber < STATION_NUMBER_FOR_SPECIALS_MIN )
		{
			// 7/9/2015 mpd : This station number indicates an irrigation station.
			if( GuiVar_StationInfoNumber > g_TEST_SEQ_irrigation_station_min )
			{
				xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

				// 7/10/2015 mpd : There are lower irrigation station numbers. 
				lstation = STATION_get_prev_available_station( &GuiVar_StationInfoBoxIndex, &GuiVar_StationInfoNumber );

				xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
			}
			else
			{
				// 7/9/2015 mpd : The current station is the lowest irrigation station in the box. Jump to the last 
				// non-irrigation station number.
				GuiVar_StationInfoNumber = STATION_NUMBER_FOR_SPECIALS_MAX;
			}
		}
		else 
		{
			// 7/9/2015 mpd : This non-irrigation station number requires special handling. 
			if( GuiVar_StationInfoNumber > STATION_NUMBER_FOR_SPECIALS_MIN )
			{
				// 7/10/2015 mpd : There are lower special station numbers. 
				GuiVar_StationInfoNumber--;
			}
			else
			{
				// 7/9/2015 mpd : The current station is the lowest special station in the box. Jump to the last 
				// irrigation station number.
				GuiVar_StationInfoNumber = g_TEST_SEQ_irrigation_station_max;
			}
		}
	}

	TEST_SEQ_copy_settings_into_guivars( (false) );

}

/* ---------------------------------------------------------- */
extern void FDTO_TEST_SEQ_draw_screen( const BOOL_32 pcomplete_redraw )
{
	STATION_STRUCT		*lstation;
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		// 6/24/2015 mpd : We just entered the screen. Initialize everything.

		// 7/13/2015 ajv : Make sure to grab the contorller's actual box index here in case the
		// controller is not "A" (index 0).
		GuiVar_StationInfoBoxIndex = FLOWSENSE_get_controller_index();
		GuiVar_StationInfoNumber = 1; 

		strlcpy( GuiVar_StationInfoNumber_str, "", sizeof( GuiVar_StationInfoNumber_str ) );

		// 6/24/2015 mpd : This initializes "GuiVar_StationInfoBoxIndex", "GuiVar_StationInfoNumber", and 
		// "GuiVar_StationInfoNumber_str".
		STATION_find_first_available_station_and_init_station_number_GuiVars();
		g_TEST_SEQ_irrigation_station_min = GuiVar_StationInfoNumber;

		// 7/10/2015 mpd : Station navigation is simplified if we know the min and max irrigation stations in the box.
		// Roll back to the highest station number, and then back to the first station to set the installed values.
		// Manufacturing does not add/remove boards with power on, so this can be done once at the start.
		lstation = STATION_get_prev_available_station( &GuiVar_StationInfoBoxIndex, &GuiVar_StationInfoNumber );
		g_TEST_SEQ_irrigation_station_max = GuiVar_StationInfoNumber;

		// 7/16/2015 mpd : Set the current station back to the first station.
		GuiVar_StationInfoNumber = g_TEST_SEQ_irrigation_station_min;

		strlcpy( GuiVar_StationInfoControllerName, "", sizeof( GuiVar_StationInfoControllerName ) );

		// 7/9/2015 mpd : Make sure we handle the pump activity correctly. This is only done once.
		//if( !TEST_SEQ_set_pump_active_output() )
		//{
		//	Alert_Message( "TS6" );
		//}

		lcursor_to_select = CP_TEST_SEQ_ON_OFF;
		g_TEST_SEQ_last_cursor_pos = CP_TEST_SEQ_ON_OFF;
		g_TEST_SEQ_auto_off_delay = false;

		// 6/24/2015 mpd : Initialize all GuiVars specific to this screen.
		GuiVar_TestStationStatus = 0;
		GuiVar_TestStationTimeDisplay = TEST_SEQ_TIME_DISPLAY_OFF;
		GuiVar_TestStationTimeRemaining = 0;
		GuiVar_TestStationOnOff = STATION_OFF;
		GuiVar_TestStationAutoMode = false;
		GuiVar_TestStationAutoOnTime = TEST_SEQ_AUTO_ON_IN_SECONDS;
		GuiVar_TestStationAutoOffTime = TEST_SEQ_AUTO_OFF_IN_SECONDS;
	}
	else
	{
		// 6/24/2015 mpd : The user kit a key. Not much to do here.
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	TEST_SEQ_copy_settings_into_guivars( pcomplete_redraw );

	GuiLib_ShowScreen( GuiStruct_scrTestSequential_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();

}

#if 0
/* ---------------------------------------------------------- */
static BOOL_32 TEST_SEQ_turn_on_current_station()
{
	BOOL_32 request_sent_to_irri = false;

	BY_SYSTEM_RECORD	*lpreserve;

	if( GuiVar_NetworkAvailable == (false) )
	{
		DIALOG_draw_ok_dialog( (GuiStruct_dlgChainNotReady_Test_0 + FLOWSENSE_we_are_poafs()) );
	}
	else
	{
		lpreserve = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( STATION_get_GID_irrigation_system(nm_STATION_get_pointer_to_station(GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1))) );

		if( lpreserve == NULL )
		{
			Alert_Message( "TS7" );
		}
		else
		{
			if( SYSTEM_PRESERVES_there_is_a_mlb( &(lpreserve->delivered_mlb_record) ) )
			{
				DIALOG_draw_ok_dialog( GuiStruct_dlgMainlineBreakInEffect_Test_0 );
			}
			else if( (GuiVar_StationInfoBoxIndex == FLOWSENSE_get_controller_index()) && (tpmicro_comm.fuse_blown == (true)) )
			{
				DIALOG_draw_ok_dialog( GuiStruct_dlgFuseBlown_0 );
			}
			else if( irri_comm.two_wire_cable_overheated[ GuiVar_StationInfoBoxIndex ] )
			{
				// 11/11/2014 rmd : This really isn't as good as it could be. Could also check if the
				// station is decoder based.

				DIALOG_draw_ok_dialog( GuiStruct_dlgTwoWireCableOverheated_Test_0 );
			}
			else if( irri_comm.test_request.in_use == (false) )
			{
				request_sent_to_irri = true;

				irri_comm.test_request.box_index_0 = GuiVar_StationInfoBoxIndex;
				irri_comm.test_request.station_number = (GuiVar_StationInfoNumber - 1);  // the gui is 1 based
				irri_comm.test_request.set_expected = false;

				// ----------

				// 6/23/2015 mpd : Set the time the station is to remain on the IRRI list. 
				irri_comm.test_request.time_seconds = GuiVar_TestStationTimeRemaining;

				// 6/23/2015 mpd : The "in_use" field is used by IRRI_COMM to block multiple requests. It does not stay 
				// on for more than a second, so we need a local flag to indicate the expected status of this station.
				irri_comm.test_request.in_use = true;
				GuiVar_TestStationOnOff = true;
			}
		}
	}

	return( request_sent_to_irri );

}
#endif

/* ---------------------------------------------------------- */
extern void FDTO_TEST_SEQ_update_screen()
{
	// 6/23/2015 mpd : This function is called every second by "td_check". Use that frequency for the manual test 
	// station ON count down as well as the auto run ON and OFF times.
	if( GuiVar_TestStationTimeDisplay == TEST_SEQ_TIME_DISPLAY_ON )
	{
		// 6/23/2015 mpd : This count-down variable is used for all ON and OFF timing.
		if( GuiVar_TestStationTimeRemaining )
		{
			// 6/23/2015 mpd : TBD: If this does not closely track the IRRI progress for the station, the 
			// IRRI_MAIN_LIST_OF_STATIONS_STRUCT "requested_irrigation_seconds_u32" field can be examined.
			GuiVar_TestStationTimeRemaining--;
		}

		if( GuiVar_TestStationAutoMode )
		{
			if( g_TEST_SEQ_auto_off_delay )
			{
				// 6/23/2015 mpd : TODO: The ON and OFF timers are set when the request goes to IRRI. They should wait 
				// to start until the station status actually changes.
				 
				// 6/23/2015 mpd : We are counting down the OFF time before the next station turns ON.
				if( GuiVar_TestStationTimeRemaining <= 0 )
				{
					// 6/23/2015 mpd : The OFF time expired. Go to the next station and turn it ON.
					g_TEST_SEQ_auto_off_delay = false;

					TEST_SEQ_change_target_station_number( true );

					// 6/23/2015 mpd : TBD: Do we want Auto mode to continue looping or stop after all the stations are 
					// cycled? This test prevents looping.
					if( ( GuiVar_StationInfoNumber == g_TEST_SEQ_auto_starting_station ) &&
						( GuiVar_StationInfoBoxIndex == g_TEST_SEQ_auto_starting_box ) )
					{
						GuiVar_TestStationAutoMode = false;
						GuiVar_TestStationTimeRemaining = 0;
						GuiVar_TestStationTimeDisplay = TEST_SEQ_TIME_DISPLAY_COMPLETE;
						TEST_SEQ_turn_off_all_stations( (false) ); 
					}
					else
					{
						// 6/23/2015 mpd : The stations will be cycled automatically. Use the Auto mode time.
						TEST_SEQ_set_test_time( GuiVar_TestStationAutoOnTime );

						// 7/10/2015 mpd : This function modified the IRRI list without turning any stations off. 
						TEST_SEQ_station_sequential_energize();

						// 6/23/2015 mpd : Keep the count-down display visable.
						GuiVar_TestStationTimeDisplay = TEST_SEQ_TIME_DISPLAY_ON;
					}
				}
			}
			else
			{
				// 6/23/2015 mpd : We are counting down the ON time before this station turns OFF.
				if( GuiVar_TestStationTimeRemaining <= 0 )
				{
					// 7/10/2015 mpd : "OFF Time" has been disabled. Communication overhead on a box with no loads
					// causes a long delay before the first station on the IRRI list will turn on. The "OFF Time" made 
					// every station a "first" station. Fully functional shortcuts to make this work may be found in the
					// future, so the delay code remains. The user has been blocked from changing it from zero. For now,
					// set the zero second delay so we will continue with the next station on the next update. Don't 
					// turn any stations off. 
					//TEST_SEQ_turn_off_all_stations(); 
					   
					// 6/23/2015 mpd : The ON time expired. Begin the OFF time for the current station.
					g_TEST_SEQ_auto_off_delay = true;

					// 6/23/2015 mpd : Keep the count-down display visable.
					GuiVar_TestStationTimeDisplay = TEST_SEQ_TIME_DISPLAY_ON;
				}
			}
		}
		else
		{
			// 6/23/2015 mpd : This is a manual test. The station will remain on a maximum of TEST_SEQ_MINUTE_IN_SECONDS
			// or until the user increments/decrements the test station number. Display any remaining ON time. IRRI will 
			// remove the station from the list when the test time expires. The local count-down clock may not sync 
			// exactly with the IRRI end time.
			if( GuiVar_TestStationTimeRemaining > 0 )
			{
				GuiVar_TestStationTimeDisplay = TEST_SEQ_TIME_DISPLAY_ON;
			}
			else
			{
				// 6/24/2015 mpd : The ON time has expired on a manual test. Remove the count-down display. IRRI will 
				// turn off the station within moments, so reset the ON/OFF flag. 
				GuiVar_TestStationTimeDisplay = TEST_SEQ_TIME_DISPLAY_OFF;
				GuiVar_TestStationOnOff = STATION_OFF;
			}
		}
	}

	// 7/13/2015 ajv : This is somewhat brute-force - we should really only redraw the screen if
	// something specifically requires it (e.g., a field appears or disappears), but it works
	// for now. However, be cautious because the _draw_screen function repopulates a number of
	// the GuiVars which may result in unexpected behavior.
	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
extern void TEST_SEQ_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	BOOL_32 value_is_incrementing = false;

	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				// 6/24/2015 mpd : This button is a toggle.
				case CP_TEST_SEQ_ON_OFF:
					if( GuiVar_TestStationOnOff == STATION_OFF )
					{
						TEST_SEQ_set_test_time( TEST_SEQ_MINUTE_IN_SECONDS );

						TEST_SEQ_station_sequential_energize() ? good_key_beep() : bad_key_beep();
					}
					else // Turn it off
					{
						good_key_beep();

						// 6/24/2015 mpd : Turn off the timer display
						GuiVar_TestStationTimeDisplay = false;

						TEST_SEQ_turn_off_all_stations( (false) ); 
					}
					break;

				case CP_TEST_SEQ_AUTO_MODE:
					// 6/24/2015 mpd : This button is a toggle.

					// 6/23/2015 mpd : Handle auto run bool and code. Toggling this key will cause the test time on the
					// current station to keep resetting. It will not increment the station.
					if( GuiVar_TestStationAutoMode == TEST_SEQ_TURN_ITEM_ON )
					{
						// 6/23/2015 mpd : The stations will be cycled automatically. Use the Auto mode time.
						TEST_SEQ_set_test_time( GuiVar_TestStationAutoOnTime );

						// 6/23/2015 mpd : Turn on Auto mode.
						GuiVar_TestStationAutoMode = true;

						// 6/23/2015 mpd : Auto mode does not have to start in the first box on the first channel. Save
						// the starting point so we know when to stop.
						g_TEST_SEQ_auto_starting_station = GuiVar_StationInfoNumber;
						g_TEST_SEQ_auto_starting_box = GuiVar_StationInfoBoxIndex;

						TEST_SEQ_station_sequential_energize() ? good_key_beep() : bad_key_beep();
					}
					else // Turn it off
					{
						good_key_beep();

						// 6/23/2015 mpd : Turn off Auto mode.
						GuiVar_TestStationAutoMode = false;

						// 6/24/2015 mpd : Turn off the timer display
						GuiVar_TestStationTimeDisplay = false;

						TEST_SEQ_turn_off_all_stations( (false) );
					}
					break;

				default:
					bad_key_beep();
			}

			Redraw_Screen( (false) );
			break;

		case KEY_PLUS:
		case KEY_NEXT:
			// 6/25/2015 mpd : Do not break after this assignment. Continue on to the KEY_MINUS/KEY_PREV code.
			value_is_incrementing = true;

		case KEY_MINUS:
		case KEY_PREV:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TEST_SEQ_STATION:
					good_key_beep();

					// 6/23/2015 mpd : Turn off Auto mode.
					GuiVar_TestStationAutoMode = false;

					// 6/23/2015 mpd : Turn off any stations thay may be on.
					TEST_SEQ_turn_off_all_stations( (false) );

					TEST_SEQ_change_target_station_number( value_is_incrementing );
					break;

				case CP_TEST_SEQ_ON_OFF:
					// 6/23/2015 mpd : This is a manual step mode. The user steps manually to the next station using 
					// KEY_PLUS or KEY_MINUS. These keys step to the next station and turn it on without actually 
					// selecting the "Turn On" button. 

					// 6/23/2015 mpd : Turn off Auto mode.
					GuiVar_TestStationAutoMode = false;

					// 6/23/2015 mpd : The stations are being cycled sequentially by the user. Turn them on for long 
					// enough to move the probe around.
					TEST_SEQ_set_test_time( TEST_SEQ_MINUTE_IN_SECONDS );

					// 7/10/2015 mpd : Communication overhead on a box with no loads causes a long delay before the 
					// first station on the IRRI list will turn on. Don't turn stations off for housekeeping reasons.
					// The sequential function will handle it. 
					// 6/23/2015 mpd : Turn off any stations thay may be on.
					//TEST_SEQ_turn_off_all_stations();

					// 6/23/2015 mpd : Go to the next station.
					TEST_SEQ_change_target_station_number( value_is_incrementing );

					// 6/23/2015 mpd : 
					TEST_SEQ_station_sequential_energize() ? good_key_beep() : bad_key_beep();
					break;

				case CP_TEST_SEQ_AUTO_ON_TIME:
					if( value_is_incrementing )
					{
						if( GuiVar_TestStationAutoOnTime < TEST_SEQ_AUTO_LIMIT_IN_SECONDS )
						{
							good_key_beep();
							GuiVar_TestStationAutoOnTime++;
						}
						else
						{
							bad_key_beep();
						}
					}
					else if( GuiVar_TestStationAutoOnTime > 0 )
					{
						good_key_beep();
						GuiVar_TestStationAutoOnTime--;
					}
					else
					{
						bad_key_beep();
					}
					break;

				// 7/10/2015 mpd : "OFF Time" has been disabled. Communication overhead on a box with no loads
				// causes a long delay before the first station on the IRRI list will turn on. The "OFF Time" made 
				// every station a "first" station. Fully functional shortcuts to make this work may be found in the
				// future, so the delay code remains. The user has been blocked from changing it from zero.
				#if 0
				case CP_TEST_SEQ_AUTO_OFF_TIME:
					if( value_is_incrementing )
					{
						if( GuiVar_TestStationAutoOffTime < TEST_SEQ_AUTO_LIMIT_IN_SECONDS )
						{
							good_key_beep();
							GuiVar_TestStationAutoOffTime++;
						}
						else
						{
							bad_key_beep();
						}
					}
					else if( GuiVar_TestStationAutoOffTime > 0 )
					{
						good_key_beep();
						GuiVar_TestStationAutoOffTime--;
					}
					else
					{
						bad_key_beep();
					}
					break;
				#endif

				default:
					bad_key_beep();
					break;
			}

			Refresh_Screen();
			break;

		case KEY_C_UP:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TEST_SEQ_AUTO_MODE:
					CURSOR_Select( CP_TEST_SEQ_STATION, (true) );
					break;

				default:
					CURSOR_Up( (true) );
			}
			break;

		case KEY_C_DOWN:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TEST_SEQ_ON_OFF:
					// 7/10/2015 mpd : "OFF Time" has been disabled. 
					CURSOR_Select( CP_TEST_SEQ_AUTO_ON_TIME, (true) );
					break;

				case CP_TEST_SEQ_AUTO_ON_TIME:
					bad_key_beep();
					break;

				default:
					CURSOR_Down( (true) );
			}
			break;

		case KEY_C_LEFT:
			CURSOR_Up( (true) );
			break;

		case KEY_C_RIGHT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TEST_SEQ_AUTO_ON_TIME:
					// 7/10/2015 mpd : "OFF Time" has been disabled. 
					bad_key_beep();
					break;

				default:
					CURSOR_Down( (true) );
			}
			break;

		case KEY_STOP:
			// 7/13/2015 ajv : Only do custome STOP key processing if the user has started to test at
			// station. Otherwise, allow the normal STOP key processor to handle the key press.
			if( GuiVar_TestStationOnOff == STATION_ON )
			{
				good_key_beep();
				TEST_SEQ_turn_off_all_stations( (true) );
			}
			else
			{
				KEY_process_global_keys( pkey_event );
			}
			break;

		case KEY_BACK:
			// 7/13/2015 ajv : Explicity ensure that any activity that was started on this screen is
			// terminated when the user leaves the screen. Not sure what the consequences (if any) of
			// not doing so are, but the last thing we want is an IRRI list that doesn't match the FOAL
			// list when Production attempts to use the tradtional Test feature to verify the Pump and
			// Master Valve operate correctly.
			TEST_SEQ_turn_off_all_stations( (false) );

			GuiVar_MenuScreenToShow = CP_MAIN_MENU_MANUAL;

			// No need to break here as this will allow it to fall into
			// the default case

		default:
			KEY_process_global_keys( pkey_event );
	}

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

