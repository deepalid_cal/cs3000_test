/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memset
#include	<string.h>

#include	"r_serial_port_info.h"

#include	"configuration_controller.h"

#include	"d_comm_options.h"

#include	"gpio_setup.h"

#include	"lpc3250_chip.h"

#include	"lpc32xx_gpio.h"

#include	"m_main.h"

#include	"screen_utils.h"

#include	"serport_drvr.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_SERIAL_PORT_INFO_RESET_COUNTERS	(0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * @executed Executed within the context of the display processing task when the
 *  		 screen is initially drawn; within the context of the TD check task
 *           as the screen is updated 4 times per second.
 */
extern void FDTO_SERIAL_PORT_INFO_draw_report( const BOOL_32 pcomplete_redraw )
{
	GuiVar_SerialPortOptionA = config_c.port_A_device_index;
	GuiVar_SerialPortOptionB = config_c.port_B_device_index;

	// ----------

	// 7/31/2013 rmd : Do NOT set any SerDrvrVars_s variables within this display function! May
	// only reference them.
	GuiVar_SerialPortRcvdA = SerDrvrVars_s[ UPORT_A ].stats.rcvd_bytes;
	GuiVar_SerialPortXmitA = SerDrvrVars_s[ UPORT_A ].stats.xmit_bytes;

	// ----------

	GuiVar_SerialPortRcvdB = SerDrvrVars_s[ UPORT_B ].stats.rcvd_bytes;
	GuiVar_SerialPortXmitB = SerDrvrVars_s[ UPORT_B ].stats.xmit_bytes;

	// ----------

	GuiVar_SerialPortRcvdTP = SerDrvrVars_s[ UPORT_TP ].stats.rcvd_bytes;
	GuiVar_SerialPortXmitTP = SerDrvrVars_s[ UPORT_TP ].stats.xmit_bytes;

	// ----------

	PORT_CTS_STATE( A );
	PORT_CTS_STATE( B );

	// 3/12/2014 rmd : For each screen draw read the line levels. CTS does update via interrupt
	// but ONLY in one transition direction. Therefore it is possible for the screen to not
	// reflect it true line level. This phenomenon does not affect the serial driver as it too
	// also reads the present line level when it wants to know the state of CTS. Like we are
	// going to do here.
	GuiVar_SerialPortCTSA = SerDrvrVars_s[ UPORT_A ].modem_control_line_status.i_cts;
	GuiVar_SerialPortCTSB = SerDrvrVars_s[ UPORT_B ].modem_control_line_status.i_cts;

	// ----------

	// 4/24/2015 rmd : Device present or not read the CD line level. When a device is not
	// present the index is 0 which is a legit value as far as the configuration table is
	// concerned.
	PORT_CD_STATE( A );
	PORT_CD_STATE( B );

	GuiVar_SerialPortCDA = SerDrvrVars_s[ UPORT_A ].modem_control_line_status.i_cd;
	GuiVar_SerialPortCDB = SerDrvrVars_s[ UPORT_B ].modem_control_line_status.i_cd;

	// ----------

	GuiVar_SerialPortXmittingA = (xmit_cntrl[ UPORT_A ].now_xmitting != NULL);
	GuiVar_SerialPortXmittingB = (xmit_cntrl[ UPORT_B ].now_xmitting != NULL);

	// ----------

	GuiVar_SerialPortXmitLengthA = GuiVar_SerialPortXmittingA ? (UNS_32)xmit_cntrl[ UPORT_A ].now_xmitting->Length : 0;
	GuiVar_SerialPortXmitLengthB = GuiVar_SerialPortXmittingB ? (UNS_32)xmit_cntrl[ UPORT_B ].now_xmitting->Length : 0;

	// ----------

	GuiVar_SerialPortStateA = SerDrvrVars_s[ UPORT_A ].SerportTaskState;
	GuiVar_SerialPortStateB = SerDrvrVars_s[ UPORT_B ].SerportTaskState;
	GuiVar_SerialPortStateTP = SerDrvrVars_s[ UPORT_TP ].SerportTaskState;

	// ----------

	if( pcomplete_redraw == (true) )
	{
		GuiLib_ShowScreen( GuiStruct_rptSerialPortActivity_0, CP_SERIAL_PORT_INFO_RESET_COUNTERS, GuiLib_RESET_AUTO_REDRAW );
	}
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * @executed Executed within the context of the KEY PROCESSING task.
 */
extern void SERIAL_PORT_INFO_process_report( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	UNS_32	i;

	switch( pkey_event.keycode )
	{
		// Reset Counters
		case KEY_SELECT:
			good_key_beep();

			for( i = 0; i < UPORT_TOTAL_PHYSICAL_PORTS; ++i )
			{
				taskENTER_CRITICAL();
				memset( &(SerDrvrVars_s[ i ].stats), 0, sizeof( UART_STATS_STRUCT) );
				taskEXIT_CRITICAL();

				Refresh_Screen();
			}
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;

			KEY_process_global_keys( pkey_event );

			// 6/5/2015 ajv : Since the user got here from the COMM OPTIONS dialog box, redraw that
			// dialog box.
			COMM_OPTIONS_draw_dialog( (true) );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

