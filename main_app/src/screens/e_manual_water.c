/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"e_manual_water.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"change.h"

#include	"combobox.h"

#include	"configuration_network.h"

#include	"cs3000_comm_server_common.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"flowsense.h"

#include	"group_base_file.h"

#include	"irri_comm.h"

#include	"irri_irri.h"

#include	"r_irri_details.h"

#include	"station_groups.h"

#include	"m_main.h"

#include	"speaker.h"

#include	"stations.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_MANUAL_WATER_STATION			(0)

// 10/30/2013 ajv : Suppressed the Controller cursor field for now until we
// figure out whether it's necessary or not. If re-enabling this, you'll need to
// re-enable the cursor field in the scrManualWater structure in easyGUI and
// change the font to ANSI 7 bold as well.
//#define CP_MANUAL_WATER_CONTROLLER	(1) 

#define CP_MANUAL_WATER_RUN_TIME		(2)
#define CP_MANUAL_WATER_START_STATION	(3)
#define CP_MANUAL_WATER_PROGRAM			(4)
#define CP_MANUAL_WATER_START_PROGRAM	(5)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32 g_MANUAL_WATER_program_GID;

static UNS_32 g_MANUAL_WATER_program_index;

static UNS_32 g_MANUAL_WATER_last_cursor_pos;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void MANUAL_WATER_process_total_min_10u( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	UNS_32	ltotal_run_minutes;

	UNS_32	linc_by;

	if( pkey_event.repeats > 144 )
	{
		linc_by = 8;
	}
	else if( pkey_event.repeats > 72 )
	{
		linc_by = 4;
	}
	else if( pkey_event.repeats > 48 )
	{
		linc_by = 2;
	}
	else
	{
		linc_by = 1;
	}

	ltotal_run_minutes = (UNS_32)(GuiVar_ManualWaterRunTime * 10);

	process_uns32( pkey_event.keycode, &ltotal_run_minutes, STATION_TOTAL_RUN_MINUTES_MIN_10u, STATION_TOTAL_RUN_MINUTES_MAX_10u, linc_by, (false) );

	GuiVar_ManualWaterRunTime = (float)(ltotal_run_minutes / 10.0);

	Refresh_Screen();
}

/* ---------------------------------------------------------- */
static void MANUAL_WATER_process_run_key( const BOOL_32 prun_a_station )
{
	BY_SYSTEM_RECORD	*lpreserve;

	BOOL_32	lmanual_watering_started;

	lmanual_watering_started = (false);

	if( !COMM_MNGR_network_is_available_for_normal_use() )
	{
		bad_key_beep();

		DIALOG_draw_ok_dialog( GuiStruct_dlgChainNotReady_0 );
	}
	else if( (GuiVar_StationInfoBoxIndex == FLOWSENSE_get_controller_index()) && (tpmicro_comm.fuse_blown == (true)) )
	{
		bad_key_beep();

		DIALOG_draw_ok_dialog( GuiStruct_dlgFuseBlown_0 );
	}
	else if( IRRI_COMM_if_any_2W_cable_is_over_heated() )
	{
		// 11/11/2014 rmd : Because you could be watering a prog have decided if any two_wire cable
		// is reporting problem don't allow any irrigation. Regardless of if it needs that cable or
		// not.
		// 
		// 11/11/2014 rmd : This really isn't as good as it could be. Could also check if the
		// station is decoder based.
		bad_key_beep();

		DIALOG_draw_ok_dialog( GuiStruct_dlgTwoWireCableOverheated_0 );
	}
	else if( prun_a_station == (true) )
	{
		lpreserve = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( STATION_GROUP_get_GID_irrigation_system_for_this_station(nm_STATION_get_pointer_to_station(GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1))) );

		if( lpreserve == NULL )
		{
			bad_key_beep();

			ALERT_MESSAGE_WITH_FILE_NAME( "System not found" );
		}
		else
		{
			if( SYSTEM_PRESERVES_there_is_a_mlb( &(lpreserve->delivered_mlb_record) ) )
			{
				bad_key_beep();
	
				DIALOG_draw_ok_dialog( GuiStruct_dlgMainlineBreakInEffect_0 );
			}
			else if( lpreserve->sbf.delivered_MVOR_in_effect_closed  )
			{
				bad_key_beep();

				DIALOG_draw_ok_dialog( GuiStruct_dlgMVORClosed_0 );
			}
			else
			{
				// 11/8/2013 ajv : No need to issue a good key beep since changing
				// to the Irrigation Details screen will do so automatically.
	
				irri_comm.manual_water_request.manual_how = MANUAL_WATER_A_STATION;
				irri_comm.manual_water_request.box_index_0 = GuiVar_StationInfoBoxIndex;
				irri_comm.manual_water_request.station_number = (GuiVar_StationInfoNumber - 1);  // the gui is 1 based
				irri_comm.manual_water_request.manual_seconds = (UNS_32)(GuiVar_ManualWaterRunTime * 60);
	
				lmanual_watering_started = (true);
			}
		}
	}
	else
	{
		// 11/8/2013 ajv : No need to issue a good key beep since changing
		// to the Irrigation Details screen will do so automatically.

		if( g_MANUAL_WATER_program_index < STATION_GROUP_get_num_groups_in_use() )
		{
			irri_comm.manual_water_request.manual_how = MANUAL_WATER_A_PROGRAM;
			irri_comm.manual_water_request.program_GID = g_MANUAL_WATER_program_GID;
		}
		else
		{
			irri_comm.manual_water_request.manual_how = MANUAL_WATER_ALL;
		}

		lmanual_watering_started = (true);
	}

	// ---------------------

	if( lmanual_watering_started == (true) )
	{
		irri_comm.manual_water_request.in_use = (true);

		// ---------------------

		IRRI_DETAILS_jump_to_irrigation_details();
	}
}

/* ---------------------------------------------------------- */
static void MANUAL_WATER_update_program( const UNS_32 pindex_0 )
{
	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( pindex_0 == STATION_GROUP_get_num_groups_in_use() )
	{
		strlcpy( GuiVar_ManualWaterProg, GuiLib_GetTextPtr(GuiStruct_txtAllStationGroups_0, 0), sizeof(GuiVar_ManualWaterProg) );
	}
	else
	{
		lgroup = STATION_GROUP_get_group_at_this_index( pindex_0 );

		if( lgroup == NULL )
		{
			Alert_group_not_found();

			lgroup = STATION_GROUP_get_group_at_this_index( 0 );
		}

		g_MANUAL_WATER_program_GID = nm_GROUP_get_group_ID( lgroup );

		strlcpy( GuiVar_ManualWaterProg, nm_GROUP_get_name( lgroup ), sizeof(GuiVar_ManualWaterProg) );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	Refresh_Screen();
}

/* ---------------------------------------------------------- */
static void MANUAL_WATER_process_program( const UNS_32 pkeycode )
{
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	process_uns32( pkeycode, &g_MANUAL_WATER_program_index, 0, STATION_GROUP_get_num_groups_in_use(), 1, (false) );

	MANUAL_WATER_update_program( g_MANUAL_WATER_program_index );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void nm_MANUAL_WATER_load_program_name_into_guivar( const INT_16 pindex_0_i16 )
{
	STATION_GROUP_STRUCT *lprogram;

	if( (UNS_32)pindex_0_i16 < STATION_GROUP_get_num_groups_in_use() )
	{
		lprogram = STATION_GROUP_get_group_at_this_index( (UNS_32)pindex_0_i16 );

		if( lprogram != NULL )
		{
			strlcpy( GuiVar_ComboBoxItemString, nm_GROUP_get_name( lprogram ), sizeof(GuiVar_ComboBoxItemString) );
		}
		else
		{
			Alert_group_not_found();
		}
	}
	else
	{
		strlcpy( GuiVar_ComboBoxItemString, GuiLib_GetTextPtr( GuiStruct_txtAllStationGroups_0, 0 ), sizeof(GuiVar_ComboBoxItemString) );
	}
}

/* ---------------------------------------------------------- */
static void FDTO_MANUAL_WATER_show_program_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxManualWaterProgram_0, &nm_MANUAL_WATER_load_program_name_into_guivar, (STATION_GROUP_get_num_groups_in_use() + 1), g_MANUAL_WATER_program_index );
}

/* ---------------------------------------------------------- */
static void MANUAL_WATER_copy_settings_into_guivars( const BOOL_32 pcomplete_redraw )
{
	STATION_GROUP_STRUCT	*lgroup;

	STATION_STRUCT			*lstation;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation = nm_STATION_get_pointer_to_station( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1) );

	if( lstation != NULL )
	{
		strlcpy( GuiVar_StationDescription, nm_GROUP_get_name( lstation ), NUMBER_OF_CHARS_IN_A_NAME );

		STATION_get_station_number_string( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1), (char*)&GuiVar_StationInfoNumber_str, sizeof(GuiVar_StationInfoNumber_str) );

		NETWORK_CONFIG_get_controller_name_str_for_ui( GuiVar_StationInfoBoxIndex, (char*)&GuiVar_StationInfoControllerName );

		GuiVar_ManualWaterRunTime = nm_STATION_calculate_and_copy_estimated_minutes_into_GuiVar( lstation );
	}
	else
	{
		strlcpy( GuiVar_StationDescription, "", NUMBER_OF_CHARS_IN_A_NAME );

		strlcpy( GuiVar_StationInfoNumber_str, "", sizeof(GuiVar_StationInfoNumber_str) );

		strlcpy( GuiVar_StationInfoControllerName, "", sizeof(GuiVar_StationInfoControllerName) );

		GuiVar_ManualWaterRunTime = 0.0;
	}

	if( pcomplete_redraw == (true) )
	{
		if( g_MANUAL_WATER_program_index == STATION_GROUP_get_num_groups_in_use() )
		{
			strlcpy( GuiVar_ComboBoxItemString, GuiLib_GetTextPtr( GuiStruct_txtAllStationGroups_0, 0 ), sizeof(GuiVar_ComboBoxItemString) );
		}
		else
		{
			lgroup = STATION_GROUP_get_group_at_this_index( g_MANUAL_WATER_program_index );

			if( lgroup != NULL )
			{
				g_MANUAL_WATER_program_GID = nm_GROUP_get_group_ID( lgroup );

				strlcpy( GuiVar_ManualWaterProg, nm_GROUP_get_name( lgroup ), sizeof(GuiVar_ManualWaterProg) );
			}
			else
			{
				Alert_group_not_found();
			}
		}
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void FDTO_MANUAL_WATER_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		STATION_find_first_available_station_and_init_station_number_GuiVars();

		lcursor_to_select = CP_MANUAL_WATER_START_STATION;

		g_MANUAL_WATER_last_cursor_pos = CP_MANUAL_WATER_STATION;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	MANUAL_WATER_copy_settings_into_guivars( pcomplete_redraw );

	GuiLib_ShowScreen( GuiStruct_scrManualWater_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void MANUAL_WATER_process_screen( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	STATION_STRUCT *lstation;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_cbxManualWaterProgram_0:
			COMBO_BOX_key_press( pkey_event.keycode, &g_MANUAL_WATER_program_index );

			// 6/2/2015 ajv : Update the program name if the combobox was closed
			if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
			{
				MANUAL_WATER_update_program( g_MANUAL_WATER_program_index );
			}
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_MANUAL_WATER_PROGRAM:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_MANUAL_WATER_show_program_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_MANUAL_WATER_START_STATION:
						case CP_MANUAL_WATER_START_PROGRAM:
							MANUAL_WATER_process_run_key( (GuiLib_ActiveCursorFieldNo == CP_MANUAL_WATER_START_STATION) );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_PLUS:
				case KEY_MINUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_MANUAL_WATER_STATION:
							good_key_beep();

							xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

							if( pkey_event.keycode == KEY_PLUS )
							{
								lstation = STATION_get_next_available_station( &GuiVar_StationInfoBoxIndex, &GuiVar_StationInfoNumber );
							}
							else
							{
								lstation = STATION_get_prev_available_station( &GuiVar_StationInfoBoxIndex, &GuiVar_StationInfoNumber );
							}

							MANUAL_WATER_copy_settings_into_guivars( (false) );

							xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

							Redraw_Screen( (false) );
							break;

						case CP_MANUAL_WATER_RUN_TIME:
							MANUAL_WATER_process_total_min_10u( pkey_event );
							break;

						case CP_MANUAL_WATER_PROGRAM:
							MANUAL_WATER_process_program( pkey_event.keycode );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_NEXT:
				case KEY_PREV:
					good_key_beep();

					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_MANUAL_WATER_STATION:
						case CP_MANUAL_WATER_RUN_TIME:
						case CP_MANUAL_WATER_START_STATION:
							xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

							if( pkey_event.keycode == KEY_NEXT )
							{
								lstation = STATION_get_next_available_station( &GuiVar_StationInfoBoxIndex, &GuiVar_StationInfoNumber );
							}
							else
							{
								lstation = STATION_get_prev_available_station( &GuiVar_StationInfoBoxIndex, &GuiVar_StationInfoNumber );
							}

							MANUAL_WATER_copy_settings_into_guivars( (false) );

							xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

							Redraw_Screen( (false) );
							break;

						case CP_MANUAL_WATER_PROGRAM:
						case CP_MANUAL_WATER_START_PROGRAM:
							if( pkey_event.keycode == KEY_NEXT )
							{
								pkey_event.keycode = KEY_PLUS;
							}
							else
							{
								pkey_event.keycode = KEY_MINUS;
							}

							MANUAL_WATER_process_program( pkey_event.keycode );
							break;
					}
					break;

				case KEY_C_UP:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_MANUAL_WATER_STATION:
						//case CP_MANUAL_WATER_CONTROLLER:
							bad_key_beep();
							break;

						case CP_MANUAL_WATER_RUN_TIME:
							CURSOR_Select( g_MANUAL_WATER_last_cursor_pos, (true) );
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_DOWN:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_MANUAL_WATER_STATION:
						//case CP_MANUAL_WATER_CONTROLLER:
							// 4/4/2013 ajv : Retain the last cursor position
							// so, if the user presses UP, they will be returned
							// to the same position they came from.
							g_MANUAL_WATER_last_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

							CURSOR_Select( CP_MANUAL_WATER_RUN_TIME, (true) );
							break;

						default:
							CURSOR_Down( (true) );
					}
					break;

				case KEY_C_LEFT:
					CURSOR_Up( (true) );
					break;

				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_BACK:
					GuiVar_MenuScreenToShow = CP_MAIN_MENU_MANUAL;
					// No need to break here as this will allow it to fall into
					// the default case

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

