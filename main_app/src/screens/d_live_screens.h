/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef	_INC_D_LIVE_SCREENS_H
#define	_INC_D_LIVE_SCREENS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_LIVE_SCREENS_MENU_SYSTEM_FLOW			(50)
#define	CP_LIVE_SCREENS_MENU_POC_FLOW				(51)
#define	CP_LIVE_SCREENS_MENU_BYPASS_MANIFOLD_FLOW	(52)
#define	CP_LIVE_SCREENS_MENU_ELECTRICAL				(53)
#define	CP_LIVE_SCREENS_MENU_COMMUNICATIONS			(54)
#define	CP_LIVE_SCREENS_MENU_WEATHER				(55)
#define	CP_LIVE_SCREENS_MENU_LIGHTS					(56)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern UNS_32	g_LIVE_SCREENS_cursor_position_when_dialog_displayed;

extern BOOL_32	g_LIVE_SCREENS_dialog_visible;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void LIVE_SCREENS_draw_dialog( const BOOL_32 pcomplete_redraw );

extern void LIVE_SCREENS_process_dialog( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

