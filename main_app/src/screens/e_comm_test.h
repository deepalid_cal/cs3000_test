/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_E_COMM_TEST_H
#define _INC_E_COMM_TEST_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	SEND_PDATA_TO_CLOUD_start_the_pdata_timer			(0x0199)
#define	SEND_PDATA_TO_CLOUD_do_not_start_the_pdata_timer	(0x0299)
#define	SEND_PDATA_TO_CLOUD_save_the_files					(0x0399)
#define	SEND_PDATA_TO_CLOUD_do_not_save_the_files			(0x0499)

extern void COMM_TEST_send_all_program_data_to_the_cloud( UNS_32 psave_the_files, UNS_32 pstart_the_pdata_timer );

// ----------

extern void FDTO_COMM_TEST_draw_screen( const BOOL_32 pcomplete_redraw, const UNS_32 pkeycode );

extern void COMM_TEST_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

