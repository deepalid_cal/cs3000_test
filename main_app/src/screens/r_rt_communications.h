/* ---------------------------------------------------------- */

#ifndef _INC_R_RT_COMMUNICATIONS_H
#define _INC_R_RT_COMMUNICATIONS_H

/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */

extern void FDTO_REAL_TIME_COMMUNICATIONS_update_report( void );

extern void FDTO_REAL_TIME_COMMUNICATIONS_draw_report( const BOOL_32 pcomplete_redraw );

extern void REAL_TIME_COMMUNICATIONS_process_report( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

