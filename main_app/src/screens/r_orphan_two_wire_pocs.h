/* ---------------------------------------------------------- */

#ifndef _INC_R_ORPHAN_TWO_WIRE_POCS_H
#define _INC_R_ORPHAN_TWO_WIRE_POCS_H

/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */

extern UNS_32 ORPHAN_TWO_WIRE_POCS_populate_list( void );

extern void FDTO_ORPHAN_TWO_WIRE_POCS_draw_report( const BOOL_32 pcomplete_redraw );

extern void ORPHAN_TWO_WIRE_POCS_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

