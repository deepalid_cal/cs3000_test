/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_E_WEATHER_SENSORS_H
#define _INC_E_WEATHER_SENSORS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void ET_GAGE_clear_runaway_gage( void );

extern void FDTO_WEATHER_SENSORS_draw_screen( const BOOL_32 pcomplete_redraw );

extern void WEATHER_SENSORS_process_screen( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

