/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"e_no_water_days.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"change.h"

#include	"combobox.h"

#include	"configuration_controller.h"

#include	"cs3000_comm_server_common.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"flowsense.h"

#include	"group_base_file.h"

#include	"station_groups.h"

#include	"m_main.h"

#include	"speaker.h"

#include	"stations.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_NOW_DAYS			(0)
#define CP_NOW_PROGRAM		(1)
#define CP_NOW_APPLY		(2)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32 g_NOW_program_GID;

static UNS_32 g_NOW_program_index;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void NOW_update_program( const UNS_32 pindex_0 )
{
	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( pindex_0 == STATION_GROUP_get_num_groups_in_use() )
	{
		strlcpy( GuiVar_NoWaterDaysProg, GuiLib_GetTextPtr(GuiStruct_txtAllStationGroups_0, 0), sizeof(GuiVar_NoWaterDaysProg) );
	}
	else
	{
		lgroup = STATION_GROUP_get_group_at_this_index( pindex_0 );

		if( lgroup == NULL )
		{
			Alert_group_not_found();

			lgroup = STATION_GROUP_get_group_at_this_index( 0 );
		}

		g_NOW_program_GID = nm_GROUP_get_group_ID( lgroup );

		strlcpy( GuiVar_NoWaterDaysProg, nm_GROUP_get_name( lgroup ), sizeof(GuiVar_NoWaterDaysProg) );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	Refresh_Screen();
}

/* ---------------------------------------------------------- */
static void NOW_process_program( const UNS_32 pkeycode )
{
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	process_uns32( pkeycode, &g_NOW_program_index, 0, STATION_GROUP_get_num_groups_in_use(), 1, (false) );

	NOW_update_program( g_NOW_program_index );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void nm_NOW_load_program_name_into_guivar( const INT_16 pindex_0_i16 )
{
	STATION_GROUP_STRUCT *lprogram;

	if( (UNS_32)pindex_0_i16 < STATION_GROUP_get_num_groups_in_use() )
	{
		lprogram = STATION_GROUP_get_group_at_this_index( (UNS_32)pindex_0_i16 );

		if( lprogram != NULL )
		{
			strlcpy( GuiVar_ComboBoxItemString, nm_GROUP_get_name( lprogram ), sizeof(GuiVar_ComboBoxItemString) );
		}
		else
		{
			Alert_group_not_found();
		}
	}
	else
	{
		strlcpy( GuiVar_ComboBoxItemString, GuiLib_GetTextPtr( GuiStruct_txtAllStationGroups_0, 0 ), sizeof(GuiVar_ComboBoxItemString) );
	}
}

/* ---------------------------------------------------------- */
static void FDTO_NOW_show_program_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxNoWaterDaysGroup_0, &nm_NOW_load_program_name_into_guivar, (STATION_GROUP_get_num_groups_in_use() + 1), g_NOW_program_index );
}

/* ---------------------------------------------------------- */
static void NOW_copy_settings_into_guivars( void )
{
	STATION_GROUP_STRUCT	*lprogram;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( g_NOW_program_index == STATION_GROUP_get_num_groups_in_use() )
	{
		strlcpy( GuiVar_ComboBoxItemString, GuiLib_GetTextPtr( GuiStruct_txtAllStationGroups_0, 0 ), NUMBER_OF_CHARS_IN_A_NAME );
	}
	else
	{
		lprogram = STATION_GROUP_get_group_at_this_index( g_NOW_program_index );

		if( lprogram != NULL )
		{
			g_NOW_program_GID = nm_GROUP_get_group_ID( lprogram );

			strlcpy( GuiVar_NoWaterDaysProg, nm_GROUP_get_name( lprogram ), sizeof(GuiVar_NoWaterDaysProg) );
		}
		else
		{
			Alert_group_not_found();
		}
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void FDTO_NOW_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		NOW_copy_settings_into_guivars();

		lcursor_to_select = CP_NOW_DAYS;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrNoWaterDays_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void NOW_process_screen( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_cbxNoWaterDaysGroup_0:
			COMBO_BOX_key_press( pkey_event.keycode, &g_NOW_program_index );

			// 6/2/2015 ajv : Update the program if the combobox was closed
			if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
			{
				NOW_update_program( g_NOW_program_index );
			}
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_NOW_PROGRAM:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_NOW_show_program_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_NOW_APPLY:
							good_key_beep();

							if( g_NOW_program_index == STATION_GROUP_get_num_groups_in_use() )
							{
								STATION_set_no_water_days_for_all_stations( GuiVar_NoWaterDaysDays, CHANGE_REASON_KEYPAD );
							}
							else
							{
								STATION_set_no_water_days_by_group( GuiVar_NoWaterDaysDays, g_NOW_program_GID, CHANGE_REASON_KEYPAD );
							}

							DIALOG_draw_ok_dialog( GuiStruct_dlgNoWaterDaysApplied_0 );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_PLUS:
				case KEY_MINUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_NOW_DAYS:
							good_key_beep();

							process_uns32( pkey_event.keycode, &GuiVar_NoWaterDaysDays, STATION_NO_WATER_DAYS_MIN, STATION_NO_WATER_DAYS_MAX, 1, (false) );
							Refresh_Screen();
							break;

						case CP_NOW_PROGRAM:
							NOW_process_program( pkey_event.keycode );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_C_UP:
				case KEY_C_LEFT:
					CURSOR_Up( (true) );
					break;

				case KEY_C_DOWN:
				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_BACK:
					GuiVar_MenuScreenToShow = CP_MAIN_MENU_NO_WATER_DAYS;
					// No need to break here as this will allow it to fall into
					// the default case

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

