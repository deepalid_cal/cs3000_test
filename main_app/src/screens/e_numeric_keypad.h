/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_E_NUMERIC_KEYPAD_H
#define _INC_E_NUMERIC_KEYPAD_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void NUMERIC_KEYPAD_draw_uint32_keypad( UNS_32 *pvalue, const UNS_32 pmin_value, const UNS_32 pmax_value );

extern void NUMERIC_KEYPAD_draw_int32_keypad( INT_32 *pvalue, const INT_32 pmin_value, const INT_32 pmax_value );

extern void NUMERIC_KEYPAD_draw_float_keypad( float *pvalue, const float pmin_value, const float pmax_value, const UNS_32 pnum_decimals_to_display );

extern void NUMERIC_KEYPAD_draw_double_keypad( double *pvalue, const double pmin_value, const double pmax_value, const UNS_32 pnum_decimals_to_display );

// ----------

extern void NUMERIC_KEYPAD_process_key( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event, void(*pDrawFunc)(const BOOL_32 pcomplete_redraw) );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

