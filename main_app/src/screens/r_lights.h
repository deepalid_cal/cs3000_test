
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_R_LIGHTS_H
#define _INC_R_LIGHTS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FDTO_LIGHTS_REPORT_redraw_scrollbox( void );

extern void FDTO_LIGHTS_REPORT_draw_report( const BOOL_32 pcomplete_redraw );

extern void LIGHTS_REPORT_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

