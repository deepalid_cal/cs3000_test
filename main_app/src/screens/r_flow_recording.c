/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"r_flow_recording.h"

#include	"app_startup.h"

#include	"cs3000_comm_server_common.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"flow_recorder.h"

#include	"irri_comm.h"

#include	"report_utils.h"

#include	"scrollbox.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Track the previous number of populated flow recording lines. If the number of
// populated lines changes between refreshes (e.g., a transition occurs), the
// entire scroll box must be redrawn. Otherwise, we can simply update the
// variables displayed on the screen.
static UNS_32 g_FLOW_RECORDING_line_count;

// Track the time stamp of each flow recording line to identify groups of lines
// that are created at the same time. When a new time stamp is encountered, a
// divider line is added to increase report readability.
static DATE_TIME g_FLOW_RECORDING_previous_time_stamp;

static UNS_32 g_FLOW_RECORDING_index_of_system_to_show;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in flow recording
 * flags.
 * 
 * @executed Executed within the context of the display processing task when
 *           each scroll line is drawn.
 * 
 * @param pstr A pointer to the character array to copy the text into.
 * @param size_of_str The size of the character array.
 * @param pflag The flow recording flag bit field to parse.
 * 
 * @return char* A pointer to the text string based upon the passed in value.
 *
 * @author 5/8/2012 Adrianusv
 *
 * @revisions
 *    5/8/2012 Initial release
 *    5/9/2012 Added #define to allow the developer to switch between showing
 *             all flags (debug mode) instead of a user-friendly description.
 */
static char *FLOW_RECORDING_get_flag_str( char *pstr, const UNS_32 psize_of_str, const FLOW_RECORDER_FLAG_BIT_FIELD pflag )
{
	if( pflag.flow_check_status == FRF_CHECK_STATUS_passed )
	{
		// 10/24/2014 ajv : Flow checking passed; no need to overwhelm the user
		// with text if it passed.
		strlcpy( pstr, " ", psize_of_str );
	}
	else if( pflag.flow_check_status == FRF_CHECK_STATUS_hi_flow )
	{
		strlcpy( pstr, "High Flow", psize_of_str );
	}
	else if( pflag.flow_check_status == FRF_CHECK_STATUS_lo_flow )
	{
		strlcpy( pstr, "Low Flow", psize_of_str );
	}
	else if( pflag.flow_check_status == FRF_CHECK_STATUS_not_going_to_perform )
	{
		if( pflag.FRF_NO_CHECK_REASON_no_flow_meter )
		{
			strlcpy( pstr, "No flow meter", psize_of_str );
		}
		else if( pflag.FRF_NO_CHECK_REASON_acquiring_expected )
		{
			strlcpy( pstr, "Acquiring flow", psize_of_str );
		}
		else if( pflag.FRF_NO_CHECK_REASON_station_cycles_too_low )
		{
			strlcpy( pstr, "Developing hist.", psize_of_str );
		}
		else if( pflag.FRF_NO_CHECK_REASON_cell_iterations_too_low )
		{
			strlcpy( pstr, "Developing hist.", psize_of_str );
		}
		else if( pflag.FRF_NO_UPDATE_REASON_thirty_percent )
		{
			strlcat( pstr, "Flow too far off", psize_of_str );
		}
		else if( pflag.FRF_NO_CHECK_REASON_not_enabled_by_user_setting )
		{
			strlcpy( pstr, "Checking off", psize_of_str );
		}
		else if( pflag.FRF_NO_CHECK_REASON_combo_not_allowed_to_check )
		{
			strlcpy( pstr, "Not checked", psize_of_str );
		}
		else if( pflag.FRF_NO_CHECK_REASON_unstable_flow )
		{
			strlcpy( pstr, "Unstable flow", psize_of_str );
		}
		else if( pflag.FRF_NO_CHECK_REASON_cycle_time_too_short )
		{
			strlcpy( pstr, "Cycle too short", psize_of_str );
		}
		else if( pflag.FRF_NO_UPDATE_REASON_zero_flow_rate )
		{
			strlcpy( pstr, "Zero flow rate", psize_of_str );
		}
		else if( pflag.FRF_NO_CHECK_REASON_reboot_or_chain_went_down )
		{
			strlcpy( pstr, "Power fail", psize_of_str );
		}
		else if( pflag.FRF_NO_CHECK_REASON_indicies_out_of_range )
		{
			strlcpy( pstr, "Flow too high", psize_of_str );
		}
		else if( pflag.FRF_NO_CHECK_REASON_not_supposed_to_check )
		{
			strlcpy( pstr, "Not checked", psize_of_str );
		}
	}
	
	return( PadRight( pstr, psize_of_str ) );
}

/* ---------------------------------------------------------- */
/**
 * Returns the number of populated flow recording lines for the specified
 * system. This is used to determine how many lines to display on the Flow
 * Recording report and to manage the scroll line movement.
 * 
 * @mutex Calling function must have the system_preserves_recursive_MUTEX mutex
 *  	  to ensure no flow recording lines are added or change during this
 *  	  process.
 *
 * @executed Executed within the context of the display processing task when the
 *  		 screen is initially drawn, and within the context of the key
 *  		 processing task when the user uses the Next and Prev buttons to
 *  		 change which system they're viewing.
 * 
 * @return UNS_32 The number of populated flow recording lines.
 *
 * @author 5/8/2012 Adrianusv
 *
 * @revisions
 *    5/8/2012 Initial release
 *    5/9/2012 Added check to see whether first_used is next_available,
 *             indicating there are no lines.
 *  		   Replaced for-loop which iterated through all 1600 records with a
 *  		   while-loop which finds next_avialable.
 */
static UNS_32 nm_FLOW_RECORDING_get_record_count( void )
{
	FLOW_RECORDING_CONTROL_STRUCT	*frcs_ptr;
	
	UNS_8	*ucp;

	UNS_32	lcount;

	// ----------

	// 10/23/2013 rmd : For syntax convenience below.
	frcs_ptr = &(system_preserves.system[ g_FLOW_RECORDING_index_of_system_to_show ].frcs);

	// ----------

	if( frcs_ptr->original_allocation == NULL )
	{
		// 10/23/2013 rmd : If no lines have ever been made the allocation is NULL.
		lcount = 0;	
	}
/*
	else
	if( frcs_ptr->first_to_display == frcs_ptr->next_available )
	{
		// 10/23/2013 rmd : Again an indication no lines have yet to be made. Though this check is
		// unecessary as this state is only a fleeting state not able to be witnessed by us as it is
		// within a MUTEX protected section. If we made the allocation that was done as part of
		// making a line and the result is first_to_display is NEVER equal to the next_available!
		// lcount = 0;
	}
*/
	else
	{
		ucp = frcs_ptr->first_to_display;

		lcount = 0;

		while( ucp != frcs_ptr->next_available )
		{
			lcount += 1;

			nm_flow_recorder_inc_pointer( &ucp, frcs_ptr->original_allocation );
		}
	}

	return( lcount );
}

/* ---------------------------------------------------------- */
/**
 * Draws an individual scroll line for the Flow Recording report. This is used
 * as the callback routine for the GuiLib_ScrollBox_Init routine.
 * 
 * @mutex Calling function must have the system_preserves_recursive_MUTEX mutex
 *  	  to ensure no flow recording lines are added or change during this
 *  	  process.
 *
 * @executed Executed within the context of the display processing task when the 
 *  		 screen is drawn; within the context of the key processing task when
 *  		 the user navigates up or down the scroll box.
 * 
 * @param pline_index_0_i16 The highlighted scroll box line - 0 based.
 *
 * @author 5/8/2012 Adrianusv
 *
 * @revisions
 *    5/8/2012 Initial release
 *    5/9/2012 Optimized routine and removed unused and unnecessary code.
 */
static void nm_FLOW_RECORDING_load_guivars_for_scroll_line( const INT_16 pline_index_0_i16 )
{
	CS3000_FLOW_RECORDER_RECORD	*frr;

	char str_16[ 16 ];

	INT_32	lindex_of_line_to_draw;

	INT_32	i;

	// ----------

	frr = (CS3000_FLOW_RECORDER_RECORD*)(system_preserves.system[ g_FLOW_RECORDING_index_of_system_to_show ].frcs.first_to_display);

	// ----------

	// 10/23/2013 rmd : AJ has stated that this function will not be called if there are 0 lines
	// available. So the following statement needs no record_count == 0 protection. It will be
	// at least 1 else we won't be executing this function.
	lindex_of_line_to_draw = (INT_32)(nm_FLOW_RECORDING_get_record_count() - 1) - pline_index_0_i16;

	for( i = 0; i < lindex_of_line_to_draw; ++i )
	{
		nm_flow_recorder_inc_pointer( (UNS_8**)&frr, system_preserves.system[ g_FLOW_RECORDING_index_of_system_to_show ].frcs.original_allocation );
	}

	// ----------

	GuiVar_FlowRecTimeStamp[ 0 ] = 0x00;
	strlcat( GuiVar_FlowRecTimeStamp, GetDateStr( str_16, sizeof(str_16), frr->dt.D, DATESTR_show_year_not, DATESTR_show_dow_not ), sizeof(GuiVar_FlowRecTimeStamp) );
	strlcat( GuiVar_FlowRecTimeStamp, " ", sizeof(GuiVar_FlowRecTimeStamp) );
	strlcat( GuiVar_FlowRecTimeStamp, TDUTILS_time_to_time_string_with_ampm( str_16, sizeof(str_16), frr->dt.T, TDUTILS_DONT_TREAT_AS_A_SX, TDUTILS_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ), sizeof(GuiVar_FlowRecTimeStamp) );

	GuiVar_RptController = frr->box_index_0;

	snprintf( GuiVar_RptStation, sizeof(GuiVar_RptStation), "%s", STATION_get_station_number_string( frr->box_index_0, frr->station_num, (char*)&str_16, sizeof(str_16) ) );

	GuiVar_FlowRecExpected = (float)frr->expected_flow;
	//GuiVar_FlowRecLearned = (float)frr->derated_expected_flow;
	GuiVar_FlowRecActual = (float)frr->portion_of_actual;

	FLOW_RECORDING_get_flag_str( GuiVar_FlowRecFlag, sizeof(GuiVar_FlowRecFlag), frr->flag );

	// Initialize the previous time stamp to the time stamp of the first
	// flow recording line.
	if( pline_index_0_i16 == 0 )
	{
		g_FLOW_RECORDING_previous_time_stamp.D = frr->dt.D;
		g_FLOW_RECORDING_previous_time_stamp.T = frr->dt.T;
	}

	if( DT1_IsEqualTo_DT2( &frr->dt, &g_FLOW_RECORDING_previous_time_stamp ) == (false) )
	{
		GuiVar_ShowDivider = (true);

		// Update the stored time stamp since the date and time have
		// changed.
		g_FLOW_RECORDING_previous_time_stamp.D = frr->dt.D;
		g_FLOW_RECORDING_previous_time_stamp.T = frr->dt.T;
	}
	else
	{
		GuiVar_ShowDivider = (false);
	}
}

/* ---------------------------------------------------------- */
/**
 * Redraws the scroll box on the Flow Recording report.
 *
 * @executed Executed within the context of the display processing task when the
 *  		 user changes irrigation systems and every 250 ms to update the
 *  		 screen if new lines are added.
 * 
 * @author 5/8/2012 Adrianusv
 *
 * @revisions
 *    5/8/2012 Initial release
 *    5/9/2012 Added mutex protection for the system_preserves structure.
 */
extern void FDTO_FLOW_RECORDING_redraw_scrollbox( void )
{
	UNS_32	lcurrent_line_count;

	UNS_32	lactive_line_to_show;


	FDTO_SYSTEM_load_system_name_into_guivar( g_FLOW_RECORDING_index_of_system_to_show );

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	lcurrent_line_count = nm_FLOW_RECORDING_get_record_count();

	// If the number of flow recording lines have changed, redraw the entire
	// scroll box. Otherwise, just update the contents of what's currently on
	// the screen.
	if( lcurrent_line_count != g_FLOW_RECORDING_line_count )
	{
		// 2012.06.21 ajv : If the user is currently viewing the very top line,
		// we want to keep the user on the top line. Therefore, redraw the whole
		// box instead of trying to retain the current top line.
		if( GuiLib_ScrollBox_GetActiveLine( 0, 0 ) == 0 )
		{
			GuiLib_ScrollBox_Close( 0 );

			// Initialize the new scroll box
			GuiLib_ScrollBox_Init( 0, &nm_FLOW_RECORDING_load_guivars_for_scroll_line, (INT_16)lcurrent_line_count, 0 );
		}
		else
		{
			lactive_line_to_show = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 0 , 0 ) + (lcurrent_line_count - g_FLOW_RECORDING_line_count);

			if( lactive_line_to_show > lcurrent_line_count )
			{
				lactive_line_to_show = lcurrent_line_count;
			}

			GuiLib_ScrollBox_Close( 0 );

			// Initialize the new scroll box
			GuiLib_ScrollBox_Init( 0, &nm_FLOW_RECORDING_load_guivars_for_scroll_line, (INT_16)lcurrent_line_count, (INT_16)lactive_line_to_show );
		}

		GuiLib_Refresh();
	}
	else
	{
		FDTO_SCROLL_BOX_redraw_retaining_topline( 0, lcurrent_line_count, (false) );
	}

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	g_FLOW_RECORDING_line_count = lcurrent_line_count;
}

/* ---------------------------------------------------------- */
/**
 * Draws the Flow Recording report screen.
 * 
 * @executed Executed within the context of the display processing task when the
 *  		 screen is initially drawn.
 * 
 * @author 5/8/2012 Adrianusv
 *
 * @revisions
 *    5/8/2012 Initial release
 *    5/9/2012 Added mutex protection for the system_preserves structure.
 */
extern void FDTO_FLOW_RECORDING_draw_report( const BOOL_32 pcomplete_redraw )
{
	GuiLib_ShowScreen( GuiStruct_rptFlowRecording_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );

	if( pcomplete_redraw == (true) )
	{
		GuiVar_StationDescription[ 0 ] = 0x00;

		g_FLOW_RECORDING_index_of_system_to_show = 0;

		FDTO_SYSTEM_load_system_name_into_guivar( g_FLOW_RECORDING_index_of_system_to_show );
	}

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	g_FLOW_RECORDING_line_count = nm_FLOW_RECORDING_get_record_count();

	FDTO_REPORTS_draw_report( pcomplete_redraw, g_FLOW_RECORDING_line_count, &nm_FLOW_RECORDING_load_guivars_for_scroll_line );

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while on the Flow Recording report screen.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 *
 * @param pkey_event The key event to be processed.
 *
 * @author 5/8/2012 Adrianusv
 *
 * @revisions
 *    5/8/2012 Initial release
 */
extern void FLOW_RECORDING_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	UNS_32	lkey;

	switch( pkey_event.keycode )
	{
		case KEY_NEXT:
		case KEY_PREV:
			if( pkey_event.keycode == KEY_NEXT )
			{
				lkey = KEY_PLUS;
			}
			else
			{
				lkey = KEY_MINUS;
			}

			process_uns32( lkey, &g_FLOW_RECORDING_index_of_system_to_show, 0, (SYSTEM_num_systems_in_use()-1), 1, (true) );

			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = (void*)&FDTO_FLOW_RECORDING_redraw_scrollbox;
			Display_Post_Command( &lde );
			break;

		default:
			REPORTS_process_report( pkey_event, 0, 0 );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

