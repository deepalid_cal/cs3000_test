/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"r_orphan_two_wire_stations.h"

#include	"app_startup.h"

#include	"change.h"

#include	"d_two_wire_assignment.h"

#include	"d_two_wire_discovery.h"

#include	"foal_defs.h"

#include	"flowsense.h"

#include	"group_base_file.h"

#include	"pdata_changes.h"

#include	"report_utils.h"

#include	"screen_utils.h"

#include	"speaker.h"

#include	"stations.h"

#include	"tpmicro_data.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	UNS_32	station_number_0;

	UNS_32	decoder_serial_number;

	UNS_32	decoder_output;

} ORPHAN_STATION_ITEM;

static ORPHAN_STATION_ITEM ORPHAN_STATIONS_list_of_orphans[ MAX_STATIONS_PER_CONTROLLER ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32 g_ORPHAN_TWO_WIRE_STATIONS_num_stations;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern UNS_32 ORPHAN_TWO_WIRE_STATIONS_populate_list( void )
{
	STATION_STRUCT	*lstation;

	BOOL_32	ldecoder_was_found;

	UNS_32	ldecoder_serial_number;

	UNS_32	lbox_index_0;

	UNS_32	i, j;

	// ----------

	g_ORPHAN_TWO_WIRE_STATIONS_num_stations = 0;

	lbox_index_0 = FLOWSENSE_get_controller_index();

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// 9/2/2014 ajv : Note that this process is extremely inefficient - i.e.,
	// worse than O(n). I've added some shortcuts to break out of the loops to
	// help, and the fact that the decoder list is sorted helps, but it's still
	// labor intensive. Not sure of a better way to get a symmetric difference
	// without traversing both lists, though, unless we generate a sorted list
	// of decoders assigned to stations first.
	for( i = 0; i < MAX_CHAIN_LENGTH * MAX_STATIONS_PER_CONTROLLER; ++i )
	{
		lstation = nm_GROUP_get_ptr_to_group_at_this_location_in_list( &station_info_list_hdr, i );

		if( lstation == NULL )
		{
			// 9/2/2014 ajv : Once we reach a NULL station, we've reached the
			// end of our list, so terminate the loop.
			break;
		}

		// 10/2/2014 ajv : Since 2-Wire processes are a by-controller affair,
		// only look at stations that belong to this physical box.
		if( nm_STATION_get_box_index_0( lstation ) == lbox_index_0 )
		{
			ldecoder_serial_number = nm_STATION_get_decoder_serial_number( lstation );

			ldecoder_was_found = (false);

			if( ldecoder_serial_number != DECODER_SERIAL_NUM_DEFAULT )
			{
				xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );

				for( j = 0; j < MAX_DECODERS_IN_A_BOX; ++j )
				{
					if( ldecoder_serial_number == tpmicro_data.decoder_info[ j ].sn )
					{
						ldecoder_was_found = (true);

						// 9/2/2014 ajv : We found the decoder assigned to this
						// station in the decoder list, so we're done with this
						// station. Break out of the loop.
						break;
					}
				}

				// 9/2/2014 ajv : If we did not find the decoder in the list, it
				// means we found a decoder serial number that is NO LONGER
				// CONNECTED to the 2-Wire path. Add this item to the list of
				// displayed stations.
				if( !ldecoder_was_found )
				{
					ORPHAN_STATIONS_list_of_orphans[ g_ORPHAN_TWO_WIRE_STATIONS_num_stations ].station_number_0 = nm_STATION_get_station_number_0( lstation );

					ORPHAN_STATIONS_list_of_orphans[ g_ORPHAN_TWO_WIRE_STATIONS_num_stations ].decoder_serial_number = ldecoder_serial_number;

					ORPHAN_STATIONS_list_of_orphans[ g_ORPHAN_TWO_WIRE_STATIONS_num_stations ].decoder_output = nm_STATION_get_decoder_output( lstation );

					g_ORPHAN_TWO_WIRE_STATIONS_num_stations += 1;
				}

				xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );
			}
		}
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( g_ORPHAN_TWO_WIRE_STATIONS_num_stations );
}

/* ---------------------------------------------------------- */
static void ORPHAN_TWO_WIRE_load_guivars_for_scroll_line( const INT_16 pline_index_0_i16 )
{
	char	str_16[ 16 ];

	snprintf( GuiVar_RptStation, sizeof(GuiVar_RptStation), "%s", STATION_get_station_number_string( FLOWSENSE_get_controller_index(), ORPHAN_STATIONS_list_of_orphans[ pline_index_0_i16 ].station_number_0, (char*)&str_16, sizeof(str_16) ) );

	GuiVar_RptDecoderSN = ORPHAN_STATIONS_list_of_orphans[ pline_index_0_i16 ].decoder_serial_number;

	GuiVar_RptDecoderOutput = ORPHAN_STATIONS_list_of_orphans[ pline_index_0_i16 ].decoder_output;
}

/* ---------------------------------------------------------- */
extern void FDTO_ORPHAN_TWO_WIRE_STATIONS_draw_report( const BOOL_32 pcomplete_redraw )
{
	GuiLib_ShowScreen( GuiStruct_rptOrphanTwoWireStations_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );

	FDTO_REPORTS_draw_report( pcomplete_redraw, g_ORPHAN_TWO_WIRE_STATIONS_num_stations, &ORPHAN_TWO_WIRE_load_guivars_for_scroll_line );
}

/* ---------------------------------------------------------- */
extern void ORPHAN_TWO_WIRE_STATIONS_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	STATION_STRUCT		*lstation;

	UNS_32	lbox_index_0;

	// ----------

	lbox_index_0 = FLOWSENSE_get_controller_index();

	// ----------

	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			if( GuiLib_ScrollBox_GetActiveLine(0, 0) >= 0 )
			{
				good_key_beep();

				xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

				lstation = nm_STATION_get_pointer_to_station( lbox_index_0, ORPHAN_STATIONS_list_of_orphans[ GuiLib_ScrollBox_GetActiveLine(0, 0) ].station_number_0 );

				nm_STATION_set_decoder_serial_number( lstation, DECODER_SERIAL_NUM_DEFAULT, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, lbox_index_0, CHANGE_set_change_bits, STATION_get_change_bits_ptr( lstation, CHANGE_REASON_KEYPAD ) );

				xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

				// 10/2/2014 ajv : Repopulate the list since it's contents have
				// changed.
				ORPHAN_TWO_WIRE_STATIONS_populate_list();

				Redraw_Screen( (true) );
			}
			else
			{
				bad_key_beep();
			}
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;

			KEY_process_global_keys( pkey_event );

			// 3/17/2016 ajv : Since the user got here from the 2-WIRE dialog box, redraw that dialog
			// box.
			TWO_WIRE_ASSIGNMENT_draw_dialog( (true) );
			break;

		default:
			REPORTS_process_report( pkey_event, 0, 0 );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

