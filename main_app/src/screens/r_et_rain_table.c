/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"r_et_rain_table.h"

#include	"app_startup.h"

#include	"battery_backed_vars.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"flash_storage.h"

#include	"m_main.h"

#include	"report_utils.h"

#include	"scrollbox.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static BOOL_32	g_ET_RAIN_TABLE_changes_made;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void FDTO_ET_RAIN_TABLE_process_et_value( const UNS_32 pkeycode, const UNS_32 pline_index_0 )
{
	ET_TABLE_ENTRY	et_entry;

	float	let_value_100u;


	g_ET_RAIN_TABLE_changes_made = (true);

	WEATHER_TABLES_get_et_table_entry_for_index( pline_index_0, &et_entry );

	// Make a local copy of the ET value to make editing easier.
	let_value_100u = (float)(et_entry.et_inches_u16_10000u / 10000.0);

	process_fl( pkeycode, &let_value_100u, 0.00F, 0.50F, 0.01F, (false) );

	et_entry.et_inches_u16_10000u = (UNS_16)(let_value_100u * 10000);
	et_entry.status = ET_STATUS_MANUALLY_EDITED_AT_CONTROLLER;

	WEATHER_TABLES_update_et_table_entry_for_index( pline_index_0, &et_entry );

	GuiLib_ScrollBox_RedrawLine( 0, (UNS_16)pline_index_0 );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Draws an individual scroll line for the ET and Rain table. This is used as
 * the callback routine for the GuiLib_ScrollBox_Init routine.
 * 
 * @mutex Calling function must have the list_poc_recursive_MUTEX mutex to
 *  	  ensure no records are added or changed during this process.
 *
 * @executed Executed within the context of the display processing task when the 
 *  		 screen is drawn; within the context of the key processing task when
 *  		 the user navigates up or down the scroll box.
 * 
 * @param pline_index_0_i16 The highlighted scroll box line - 0 based.
 *
 * @author 7/11/2012 Adrianusv
 *
 * @revisions
 *    7/11/2012 Initial release
 */
static void ET_RAIN_TABLE_load_guivars_for_scroll_line( const INT_16 pline_index_0_i16 )
{
	char	str_48[ 48 ];

	strlcpy( GuiVar_ETRainTableDate, GetDateStr(str_48, sizeof(str_48), (WEATHER_TABLES_get_et_table_date() - (UNS_32)pline_index_0_i16), DATESTR_show_long_year, DATESTR_show_dow_not), sizeof(GuiVar_ETRainTableDate) );


	ET_TABLE_ENTRY	et_entry;

	WEATHER_TABLES_get_et_table_entry_for_index( (UNS_32)pline_index_0_i16, &et_entry );
	
	GuiVar_ETRainTableET = (float)(et_entry.et_inches_u16_10000u / 10000.0);

	GuiVar_ETRainTableETStatus = et_entry.status;


	RAIN_TABLE_ENTRY	rain_entry;

	WEATHER_TABLES_get_rain_table_entry_for_index( (UNS_32)pline_index_0_i16, &rain_entry );
	
	GuiVar_ETRainTableRain = (float)(rain_entry.rain_inches_u16_100u / 100.0);

	if( pline_index_0_i16 == 0 )
	{
		GuiVar_ETRainTableShutdown = ( weather_preserves.rain.inhibit_irrigation_have_crossed_the_minimum || (weather_preserves.rain.rain_shutdown_rcvd_from_commserver_uns32 > 0) );
	}
	else
	{
		GuiVar_ETRainTableShutdown = (false);
	}

	GuiVar_ETRainTableRainStatus = rain_entry.status;
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 *
 * @author 2/8/2013 Adrianusv
 *
 * @revisions (none)
 */
extern void FDTO_ET_RAIN_TABLE_update_report( BOOL_32 const prefresh )
{
	// 4/28/2015 rmd : Executed of course within the context of the DISPLAY task. Runs at a 1
	// hertz rate when the real-time weather screen is being shown.
	xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );

	GuiVar_ETRainTableCurrentET = (weather_preserves.et_rip.et_inches_u16_10000u / 100);

	GuiVar_ETRainTableTable = weather_preserves.rain.rip.rain_inches_u16_100u;

	GuiVar_ETRainTableReport = weather_preserves.rain.midnight_to_midnight_raw_pulse_count;

	xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );

	if( prefresh )
	{
		// Refresh the screen to reflect the new values.
		GuiLib_Refresh();
	}
}

/* ---------------------------------------------------------- */
/**
 * Draws the ET and Rain Table screen.
 * 
 * @executed Executed within the context of the display processing task when the
 *  		 screen is initially drawn.
 * 
 * @author 7/12/2012 Adrianusv
 *
 * @revisions
 *    7/12/2012 Initial release
 */
extern void FDTO_ET_RAIN_TABLE_draw_report( const BOOL_32 pcomplete_redraw )
{
	g_ET_RAIN_TABLE_changes_made = (false);

	// ----------
	
	FDTO_ET_RAIN_TABLE_update_report( (false) );

	// ----------
	
	GuiLib_ShowScreen( GuiStruct_rptETRainTable_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );

	FDTO_REPORTS_draw_report( pcomplete_redraw, ET_RAIN_DAYS_IN_TABLE, &ET_RAIN_TABLE_load_guivars_for_scroll_line );
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while on the ET and Rain Table screen.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 *
 * @param pkey_event The key event to be processed.
 *
 * @author 7/12/2012 Adrianusv
 *
 * @revisions
 *    7/12/2012 Initial release
 */
extern void ET_RAIN_TABLE_process_report( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( pkey_event.keycode )
	{
		case KEY_PLUS:
		case KEY_MINUS:
			lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
			lde._04_func_ptr = (void*)&FDTO_ET_RAIN_TABLE_process_et_value;
			lde._06_u32_argument1 = pkey_event.keycode;
			lde._07_u32_argument2 = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );
			Display_Post_Command( &lde );
			break;

		case KEY_BACK:
			// Save the file, but delay it in case they continue to change
			// values. 10 seconds should be ample time.
			if( g_ET_RAIN_TABLE_changes_made == (true) )
			{
				// TODO - Trigger change lines for the ET values that have
				// changed.
				FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_WEATHER_TABLES, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );

				g_ET_RAIN_TABLE_changes_made = (false);
			}

			GuiVar_MenuScreenToShow = CP_MAIN_MENU_REPORTS;

			KEY_process_global_keys( pkey_event );
			break;

		default:
			REPORTS_process_report( pkey_event, 0, 0 );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

