/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"e_percent_adjust.h"

#include	"app_startup.h"

#include	"cursor_utils.h"

#include	"epson_rx_8025sa.h"

#include	"group_base_file.h"

#include	"m_main.h"

#include	"screen_utils.h"

#include	"station_groups.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_PERCENT_ADJUST_PERCENT_0			(0)
#define	CP_PERCENT_ADJUST_PERCENT_1			(1)
#define	CP_PERCENT_ADJUST_PERCENT_2			(2)
#define	CP_PERCENT_ADJUST_PERCENT_3			(3)
#define	CP_PERCENT_ADJUST_PERCENT_4			(4)
#define	CP_PERCENT_ADJUST_PERCENT_5			(5)
#define	CP_PERCENT_ADJUST_PERCENT_6			(6)
#define	CP_PERCENT_ADJUST_PERCENT_7			(7)
#define	CP_PERCENT_ADJUST_PERCENT_8			(8)
#define	CP_PERCENT_ADJUST_PERCENT_9			(9)

#define	CP_PERCENT_ADJUST_DAYS_0			(10)
#define	CP_PERCENT_ADJUST_DAYS_1			(11)
#define	CP_PERCENT_ADJUST_DAYS_2			(12)
#define	CP_PERCENT_ADJUST_DAYS_3			(13)
#define	CP_PERCENT_ADJUST_DAYS_4			(14)
#define	CP_PERCENT_ADJUST_DAYS_5			(15)
#define	CP_PERCENT_ADJUST_DAYS_6			(16)
#define	CP_PERCENT_ADJUST_DAYS_7			(17)
#define	CP_PERCENT_ADJUST_DAYS_8			(18)
#define	CP_PERCENT_ADJUST_DAYS_9			(19)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Processes the Plus and Minus keys to change the percent adjust value.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 *
 * @param pkeycode The key that was pressed.
 *
 * @author 11/4/2011 Adrianusv
 *
 * @revisions
 *    11/4/2011 Initial release
 */
static void PERCENT_ADJUST_process_percentage( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event, INT_32 *ppercentage, BOOL_32 *pshow_days )
{
	UNS_32	inc_by;

	if( pkey_event.repeats > 72 )
	{
		inc_by = 4;
	}
	else if( (pkey_event.repeats > 48) )
	{
		inc_by = 2;
	}
	else
	{
		inc_by = 1;
	}

	process_int32( pkey_event.keycode, ppercentage, (INT_32)(PERCENT_ADJUST_PERCENT_MIN_100u - 100), (INT_32)(PERCENT_ADJUST_PERCENT_MAX_100u - 100), inc_by, (false) );

	if( *ppercentage == 0 )
	{
		*pshow_days = (false);

		Redraw_Screen( (false) );
	}
	else if( *pshow_days == (false) )
	{
		Refresh_Screen();

		*pshow_days = (true);

		//Redraw_Screen( (false) );
	}
	else
	{
		Refresh_Screen();
	}
}

/* ---------------------------------------------------------- */
static void PERCENT_ADJUST_process_days( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event, UNS_32 *pdays, char *pend_date_str )
{
	UNS_32	inc_by;

	if( pkey_event.repeats > 72 )
	{
		inc_by = 4;
	}
	else if( pkey_event.repeats > 48 )
	{
		inc_by = 2;
	}
	else
	{
		inc_by = 1;
	}

	process_uns32( pkey_event.keycode, pdays, PERCENT_ADJUST_DAYS_MIN, PERCENT_ADJUST_DAYS_MAX, inc_by, (false) );

	// ---------------------

	char		str_48[ 48 ];

	DATE_TIME	ldt;

	EPSON_obtain_latest_time_and_date( &ldt );

	strlcpy( pend_date_str, GetDateStr(str_48, sizeof(str_48), (ldt.D + *pdays), DATESTR_show_short_year, DATESTR_show_short_dow), sizeof(GuiVar_PercentAdjustEndDate_0) );

	// ---------------------

	Refresh_Screen();
}

/* ---------------------------------------------------------- */
extern void FDTO_PERCENT_ADJUST_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		PERCENT_ADJUST_copy_group_into_guivars();

		lcursor_to_select = 0;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrPercentAdjust_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void PERCENT_ADJUST_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{

	switch( pkey_event.keycode )
	{
		case KEY_MINUS:
		case KEY_PLUS:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_PERCENT_ADJUST_PERCENT_0:
					PERCENT_ADJUST_process_percentage( pkey_event, &GuiVar_PercentAdjustPercent_0, &GuiVar_GroupSettingC_0 );
					break;

				case CP_PERCENT_ADJUST_PERCENT_1:
					PERCENT_ADJUST_process_percentage( pkey_event, &GuiVar_PercentAdjustPercent_1, &GuiVar_GroupSettingC_1 );
					break;

				case CP_PERCENT_ADJUST_PERCENT_2:
					PERCENT_ADJUST_process_percentage( pkey_event, &GuiVar_PercentAdjustPercent_2, &GuiVar_GroupSettingC_2 );
					break;

				case CP_PERCENT_ADJUST_PERCENT_3:
					PERCENT_ADJUST_process_percentage( pkey_event, &GuiVar_PercentAdjustPercent_3, &GuiVar_GroupSettingC_3 );
					break;

				case CP_PERCENT_ADJUST_PERCENT_4:
					PERCENT_ADJUST_process_percentage( pkey_event, &GuiVar_PercentAdjustPercent_4, &GuiVar_GroupSettingC_4 );
					break;

				case CP_PERCENT_ADJUST_PERCENT_5:
					PERCENT_ADJUST_process_percentage( pkey_event, &GuiVar_PercentAdjustPercent_5, &GuiVar_GroupSettingC_5 );
					break;

				case CP_PERCENT_ADJUST_PERCENT_6:
					PERCENT_ADJUST_process_percentage( pkey_event, &GuiVar_PercentAdjustPercent_6, &GuiVar_GroupSettingC_6 );
					break;

				case CP_PERCENT_ADJUST_PERCENT_7:
					PERCENT_ADJUST_process_percentage( pkey_event, &GuiVar_PercentAdjustPercent_7, &GuiVar_GroupSettingC_7 );
					break;

				case CP_PERCENT_ADJUST_PERCENT_8:
					PERCENT_ADJUST_process_percentage( pkey_event, &GuiVar_PercentAdjustPercent_8, &GuiVar_GroupSettingC_8 );
					break;

				case CP_PERCENT_ADJUST_PERCENT_9:
					PERCENT_ADJUST_process_percentage( pkey_event, &GuiVar_PercentAdjustPercent_9, &GuiVar_GroupSettingC_9 );
					break;

				case CP_PERCENT_ADJUST_DAYS_0:
					PERCENT_ADJUST_process_days( pkey_event, &GuiVar_GroupSettingB_0, (char*)&GuiVar_PercentAdjustEndDate_0 );
					break;

				case CP_PERCENT_ADJUST_DAYS_1:
					PERCENT_ADJUST_process_days( pkey_event, &GuiVar_GroupSettingB_1, (char*)&GuiVar_PercentAdjustEndDate_1 );
					break;

				case CP_PERCENT_ADJUST_DAYS_2:
					PERCENT_ADJUST_process_days( pkey_event, &GuiVar_GroupSettingB_2, (char*)&GuiVar_PercentAdjustEndDate_2 );
					break;

				case CP_PERCENT_ADJUST_DAYS_3:
					PERCENT_ADJUST_process_days( pkey_event, &GuiVar_GroupSettingB_3, (char*)&GuiVar_PercentAdjustEndDate_3 );
					break;

				case CP_PERCENT_ADJUST_DAYS_4:
					PERCENT_ADJUST_process_days( pkey_event, &GuiVar_GroupSettingB_4, (char*)&GuiVar_PercentAdjustEndDate_4 );
					break;

				case CP_PERCENT_ADJUST_DAYS_5:
					PERCENT_ADJUST_process_days( pkey_event, &GuiVar_GroupSettingB_5, (char*)&GuiVar_PercentAdjustEndDate_5 );
					break;

				case CP_PERCENT_ADJUST_DAYS_6:
					PERCENT_ADJUST_process_days( pkey_event, &GuiVar_GroupSettingB_6, (char*)&GuiVar_PercentAdjustEndDate_6 );
					break;

				case CP_PERCENT_ADJUST_DAYS_7:
					PERCENT_ADJUST_process_days( pkey_event, &GuiVar_GroupSettingB_7, (char*)&GuiVar_PercentAdjustEndDate_7 );
					break;

				case CP_PERCENT_ADJUST_DAYS_8:
					PERCENT_ADJUST_process_days( pkey_event, &GuiVar_GroupSettingB_8, (char*)&GuiVar_PercentAdjustEndDate_8 );
					break;

				case CP_PERCENT_ADJUST_DAYS_9:
					PERCENT_ADJUST_process_days( pkey_event, &GuiVar_GroupSettingB_9, (char*)&GuiVar_PercentAdjustEndDate_9 );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_PREV:
		case KEY_C_UP:
			if( (GuiLib_ActiveCursorFieldNo % 10) == 0 )
			{
				bad_key_beep();
			}
			else
			{
				CURSOR_Up( (true) );
			}
			break;

		case KEY_NEXT:
		case KEY_C_DOWN:
			if( (UNS_32)(GuiLib_ActiveCursorFieldNo % 10) == (STATION_GROUP_get_num_groups_in_use() - 1) )
			{
				bad_key_beep();
			}
			else
			{
				CURSOR_Down( (true) );
			}
			break;

		case KEY_C_LEFT:
			if( (GuiLib_ActiveCursorFieldNo >= CP_PERCENT_ADJUST_PERCENT_0) && (GuiLib_ActiveCursorFieldNo <= CP_PERCENT_ADJUST_PERCENT_9) )
			{
				CURSOR_Select( (UNS_32)((GuiLib_ActiveCursorFieldNo + 10) - 1), (true) );
			}
			else if( (GuiLib_ActiveCursorFieldNo >= CP_PERCENT_ADJUST_DAYS_0) && (GuiLib_ActiveCursorFieldNo <= CP_PERCENT_ADJUST_DAYS_9) )
			{
				CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo - 10), (true) );
			}
			else
			{
				bad_key_beep();
			}
			break;

		case KEY_C_RIGHT:
			if( (GuiLib_ActiveCursorFieldNo >= CP_PERCENT_ADJUST_PERCENT_0) && (GuiLib_ActiveCursorFieldNo <= CP_PERCENT_ADJUST_PERCENT_9) )
			{
				CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo + 10), (true) );
			}
			else if( (GuiLib_ActiveCursorFieldNo >= CP_PERCENT_ADJUST_DAYS_0) && (GuiLib_ActiveCursorFieldNo <= CP_PERCENT_ADJUST_DAYS_9) )
			{
				CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo - 10) + 1, (true) );
			}
			else
			{
				bad_key_beep();
			}
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = CP_MAIN_MENU_SCHEDULED_IRRIGATION;

			PERCENT_ADJUST_extract_and_store_changes_from_GuiVars();
			// No need to break here to allow this to fall into the default
			// condition.

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

