/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/21/2015 ajv : Required for strlcpy.
#include	<string.h>

#include	"e_en_programming_pw.h"

#include	"device_common.h"

#include	"device_EN_PremierWaveXE.h"

#include	"combobox.h"

#include	"cursor_utils.h"

#include	"d_comm_options.h"

#include	"d_device_exchange.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"e_keyboard.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_PW_XE_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY	(0)
#define	CP_PW_XE_PROGRAMMING_DHCP_NAME					(1)
#define	CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_1			(2)
#define	CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_2			(3)
#define	CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_3			(4)
#define	CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_4			(5)
#define	CP_PW_XE_PROGRAMMING_SUBNET_MASK				(6)
#define	CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_1			(7)
#define	CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_2			(8)
#define	CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_3			(9)
#define	CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_4			(10)
#define	CP_PW_XE_PROGRAMMING_READ_DEVICE				(11)
#define	CP_PW_XE_PROGRAMMING_PROGRAM_DEVICE				(12)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/22/2015 ajv : Since only the draw fuction is initially told which port the device is
// on, copy that port into a global variable that's accessible from the key processing task.
static UNS_32	g_PW_XE_PROGRAMMING_port;

static UNS_32	g_PW_XE_PROGRAMMING_previous_cursor_pos;

static BOOL_32	g_PW_XE_PROGRAMMING_PW_querying_device;

static BOOL_32	g_PW_XE_PROGRAMMING_read_after_write_pending;

static BOOL_32	g_PW_XE_PROGRAMMING_editing_wifi_settings;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_PW_XE_PROGRAMMING_process_device_exchange_key( const UNS_32 pkeycode )
{
	e_SHARED_get_info_text_from_programming_struct();

	switch( pkeycode )
	{
		case DEVICE_EXCHANGE_KEY_read_settings_completed_ok:
		case DEVICE_EXCHANGE_KEY_write_settings_completed_ok:
			if( g_PW_XE_PROGRAMMING_read_after_write_pending == (true) )
			{
				// 4/30/2015 mpd : A WRITE operation just completed. We are not done until the subsequent read 
				// completes. Act like a "DEVICE_EXCHANGE_KEY_read_settings_in_progress" key code arrived.

				// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
				GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress );
			}
			else
			{
				// 5/6/2015 mpd : A read operation just completed. We are done.

				// 4/22/2015 ajv : Get values from the programming struct
				PW_XE_PROGRAMMING_copy_programming_into_GuiVars();

				// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
				GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_completed_ok );
			}

			// 5/18/2015 ajv : Ensure the flag that indicates a dialog box is open is set to false.
			DIALOG_close_ok_dialog();

			FDTO_PW_XE_PROGRAMMING_draw_screen( (false), g_PW_XE_PROGRAMMING_port );
			break;

		case DEVICE_EXCHANGE_KEY_read_settings_error:
		case DEVICE_EXCHANGE_KEY_write_settings_error:

			// 5/7/2015 mpd : WEN programming responds with errors for invalid values after successful READ and WRITE 
			// operations. The user needs to see the current values.
			PW_XE_PROGRAMMING_copy_programming_into_GuiVars();

			// 5/7/2015 mpd : Do not break here. Continue.

		case DEVICE_EXCHANGE_KEY_busy_try_again_later:
		case DEVICE_EXCHANGE_KEY_read_settings_in_progress:
		case DEVICE_EXCHANGE_KEY_write_settings_in_progress:
			// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
			GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( pkeycode );

			// 5/18/2015 ajv : Update the progress dialog.
			DEVICE_EXCHANGE_draw_dialog();
			break;
	}
}

/* ---------------------------------------------------------- */
static void FDTO_PW_XE_PROGRAMMING_redraw_screen( const BOOL_32 pcomplete_redraw )
{
	// 6/15/2015 ajv : Since the FDTO_PW_XE_PROGRAMMING_draw_screen has two parameters, unlike
	// most other _draw_screen routines, we need to have a redraw routine which accepts a single
	// parameter for cases such as closing a keyboard.
	// 
	// Note that other dialog boxes use the normal _draw_screen routine passing in an
	// unknown value for pport, but that's ok because the complete_redraw parameter will be
	// false so g_PW_XE_PROGRAMMING_port won't be updated to the unknown value.
	FDTO_PW_XE_PROGRAMMING_draw_screen( pcomplete_redraw, g_PW_XE_PROGRAMMING_port );
}

/* ---------------------------------------------------------- */
extern void FDTO_PW_XE_PROGRAMMING_draw_screen( const BOOL_32 pcomplete_redraw, const UNS_32 pport )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw )
	{
		g_PW_XE_PROGRAMMING_port = pport;

		g_PW_XE_PROGRAMMING_read_after_write_pending = (false);

		g_PW_XE_PROGRAMMING_editing_wifi_settings = (false);

		// ----------

		GuiVar_CommOptionDeviceExchangeResult = DEVICE_EXCHANGE_KEY_read_settings_completed_ok;
		//PW_XE_PROGRAMMING_initialize_guivars();

		// 4/21/2015 ajv : Since we're drawing the screen from scratch, start the screen in the
		// "Reading settings from radio" device exchange mode until we're told otherwise.

		// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
		GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress );

		// 4/20/2015 ajv : Default to the MODE field since that's much more likely to be changed
		// than the port.
		lcursor_to_select = CP_PW_XE_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrENProgramming_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();

	// ----------

	if( pcomplete_redraw )
	{
		// 5/18/2015 ajv : Immediately display the dialog to indicate to the user that something is
		// happening
		DEVICE_EXCHANGE_draw_dialog();

		// 4/21/2015 ajv : Start the data retrieval automatically for the user. This isn't necessary, but it's a nicety, 
		// preventing the user from having to press the Read Radio button.
		e_SHARED_start_device_communication( g_PW_XE_PROGRAMMING_port, DEV_READ_OPERATION, &g_PW_XE_PROGRAMMING_PW_querying_device);
	}
}

/* ---------------------------------------------------------- */
extern void PW_XE_PROGRAMMING_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_dlgKeyboard_0:
			if( ((pkey_event.keycode == KEY_BACK) || ((pkey_event.keycode == KEY_SELECT) && (GuiLib_ActiveCursorFieldNo == 49 /*CP_QWERTY_KEYBOARD_OK*/))) )
			{
				// 4/21/2015 ajv : The keyboard routines always use GuiVar_GroupName. Therefore, copy the
				// contents of this variable back into DHCP Name when we're done.
				strlcpy( GuiVar_ENDHCPName, GuiVar_GroupName, sizeof(GuiVar_ENDHCPName) );
			}

			KEYBOARD_process_key( pkey_event, &FDTO_PW_XE_PROGRAMMING_redraw_screen );
			break;

		case GuiStruct_cbxENSubnetMask_0:
			COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_ENNetmask );
			break;

		case GuiStruct_cbxNoYes_0:
			COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_ENObtainIPAutomatically );
			break;

		default:
			switch( pkey_event.keycode )
			{
				case DEVICE_EXCHANGE_KEY_busy_try_again_later:

				case DEVICE_EXCHANGE_KEY_read_settings_completed_ok:
				case DEVICE_EXCHANGE_KEY_read_settings_error:
				case DEVICE_EXCHANGE_KEY_read_settings_in_progress:

				case DEVICE_EXCHANGE_KEY_write_settings_completed_ok:
				case DEVICE_EXCHANGE_KEY_write_settings_error:
				case DEVICE_EXCHANGE_KEY_write_settings_in_progress:

					if( (pkey_event.keycode != DEVICE_EXCHANGE_KEY_read_settings_in_progress) && (pkey_event.keycode != DEVICE_EXCHANGE_KEY_write_settings_in_progress) )
					{
						g_PW_XE_PROGRAMMING_PW_querying_device = (false);
					}

					// 4/21/2015 ajv : Redraw the screen displaying the results or indicating the error
					// condition.
					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._04_func_ptr = (void*)&FDTO_PW_XE_PROGRAMMING_process_device_exchange_key;
					lde._06_u32_argument1 = pkey_event.keycode;
					Display_Post_Command( &lde );

					if( ( pkey_event.keycode == DEVICE_EXCHANGE_KEY_write_settings_completed_ok ) && 
						( g_PW_XE_PROGRAMMING_read_after_write_pending == (true) ) )
					{
						// 5/1/2015 mpd : A WRITE operation successfully completed. We need to initiate a READ to verify the
						// WRITE was successful.
						g_PW_XE_PROGRAMMING_read_after_write_pending = (false);
						e_SHARED_start_device_communication( g_PW_XE_PROGRAMMING_port, DEV_READ_OPERATION, &g_PW_XE_PROGRAMMING_PW_querying_device);
					}
					else if( pkey_event.keycode == DEVICE_EXCHANGE_KEY_write_settings_error ) 
					{
						// 6/4/2015 mpd : The WRITE failed to complete. Clear this flag or an extra READ will be waiting
						// to annoy the user some time in the future.
						g_PW_XE_PROGRAMMING_read_after_write_pending = (false);
					}
					break;

				case KEY_SELECT:
					// 4/20/2015 ajv : If we'r already in the middle of a device exchange or syncing radios,
					// ignore the key press.
					if( (GuiVar_CommOptionDeviceExchangeResult != ( e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress  ) ) ) && 
						(GuiVar_CommOptionDeviceExchangeResult != ( e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_write_settings_in_progress ) ) ) )
					{
						switch( GuiLib_ActiveCursorFieldNo )
						{
							case CP_PW_XE_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY:
								good_key_beep();
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = (void*)&FDTO_e_SHARED_show_obtain_ip_automatically_dropdown;
								Display_Post_Command( &lde );
								break;

							case CP_PW_XE_PROGRAMMING_DHCP_NAME:
								good_key_beep();

								// 4/21/2015 ajv : The keyboard routines always use GuiVar_GroupName. Therefore, copy the
								// contents of the DHCP Name into this variable first. We'll extract it back out when we're
								// done.
								strlcpy( GuiVar_GroupName, GuiVar_ENDHCPName, sizeof(GuiVar_ENDHCPName) );

								// 4/21/2015 ajv : Blank out the DHCP Name so it is no longer visible on the screen.
								memset( GuiVar_ENDHCPName, 0x00, sizeof(GuiVar_ENDHCPName) );

								KEYBOARD_draw_keyboard( 102, 68, sizeof(GuiVar_ENDHCPName), KEYBOARD_TYPE_QWERTY );
								break;

							case CP_PW_XE_PROGRAMMING_SUBNET_MASK:
								good_key_beep();
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = (void*)&FDTO_e_SHARED_show_subnet_dropdown;
								Display_Post_Command( &lde );
								break;

							case CP_PW_XE_PROGRAMMING_PROGRAM_DEVICE:

								if( g_PW_XE_PROGRAMMING_PW_querying_device == (false) )
								{
									good_key_beep();

									// 4/21/2015 ajv : Store the button that was highlighted when the user pressed SELECT. Once
									// the write process is done, we'll revert back to this button.
									g_PW_XE_PROGRAMMING_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

									// 4/9/2015 mpd : Send any user changes to the programming struct.
									PW_XE_PROGRAMMING_extract_and_store_GuiVars();

									// 4/22/2015 ajv : Trigger save process
									e_SHARED_start_device_communication( g_PW_XE_PROGRAMMING_port, DEV_WRITE_OPERATION, &g_PW_XE_PROGRAMMING_PW_querying_device);

									// 6/8/2015 mpd : TODO: REMOVE THE READ DURING DEVELOPMENT
									// 5/6/2015 mpd : This flag will initiate a READ when the WRITE completes.
									g_PW_XE_PROGRAMMING_read_after_write_pending = (true);
								}
								else
								{
									bad_key_beep();
								}
								break;

							case CP_PW_XE_PROGRAMMING_READ_DEVICE:

								if( g_PW_XE_PROGRAMMING_PW_querying_device == (false) )
								{
									good_key_beep();

									// 4/21/2015 ajv : Store the button that was highlighted when the user pressed SELECT. Once
									// the read process is done, we'll revert back to this button.
									g_PW_XE_PROGRAMMING_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

									e_SHARED_start_device_communication( g_PW_XE_PROGRAMMING_port, DEV_READ_OPERATION, &g_PW_XE_PROGRAMMING_PW_querying_device);
								}
								else
								{
									bad_key_beep();
								}
								break;

							default:
								bad_key_beep();
						}
					}
					else
					{
						bad_key_beep();
					}
					break;

				case KEY_PLUS:
				case KEY_MINUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_PW_XE_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY:
							process_bool( &GuiVar_ENObtainIPAutomatically );

							// 4/21/2015 ajv : Since changing this value results in screen elements appearing and
							// disappearing, redraw the whole screen.
							Redraw_Screen( (false) );
							break;

						case CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_1: 
							process_uns32( pkey_event.keycode, &GuiVar_ENIPAddress_1, EN_PROGRAMMING_IP_ADDRESS_OCTET_MIN, EN_PROGRAMMING_IP_ADDRESS_OCTET_MAX, 1, (true) );
							break;

						case CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_2: 
							process_uns32( pkey_event.keycode, &GuiVar_ENIPAddress_2, EN_PROGRAMMING_IP_ADDRESS_OCTET_MIN, EN_PROGRAMMING_IP_ADDRESS_OCTET_MAX, 1, (true) );
							break;

						case CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_3: 
							process_uns32( pkey_event.keycode, &GuiVar_ENIPAddress_3, EN_PROGRAMMING_IP_ADDRESS_OCTET_MIN, EN_PROGRAMMING_IP_ADDRESS_OCTET_MAX, 1, (true) );
							break;

						case CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_4: 
							process_uns32( pkey_event.keycode, &GuiVar_ENIPAddress_4, EN_PROGRAMMING_IP_ADDRESS_OCTET_MIN, EN_PROGRAMMING_IP_ADDRESS_OCTET_MAX, 1, (true) );
							break;

						case CP_PW_XE_PROGRAMMING_SUBNET_MASK:
							process_uns32( pkey_event.keycode, &GuiVar_ENNetmask, EN_PROGRAMMING_NETMASK_MIN, EN_PROGRAMMING_NETMASK_MAX, 1, (false) );
							break;

						case CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_1:
							process_uns32( pkey_event.keycode, &GuiVar_ENGateway_1, EN_PROGRAMMING_GATEWAY_OCTET_MIN, EN_PROGRAMMING_GATEWAY_OCTET_MAX, 1, (true) );
							break;

						case CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_2:
							process_uns32( pkey_event.keycode, &GuiVar_ENGateway_2, EN_PROGRAMMING_GATEWAY_OCTET_MIN, EN_PROGRAMMING_GATEWAY_OCTET_MAX, 1, (true) );
							break;

						case CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_3:
							process_uns32( pkey_event.keycode, &GuiVar_ENGateway_3, EN_PROGRAMMING_GATEWAY_OCTET_MIN, EN_PROGRAMMING_GATEWAY_OCTET_MAX, 1, (true) );
							break;

						case CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_4:
							process_uns32( pkey_event.keycode, &GuiVar_ENGateway_4, EN_PROGRAMMING_GATEWAY_OCTET_MIN, EN_PROGRAMMING_GATEWAY_OCTET_MAX, 1, (true) );
							break;

						default:
							bad_key_beep();
					}

					Refresh_Screen();
					break;

				case KEY_C_UP:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_1: 
						case CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_2:
						case CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_3:
						case CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_4:
							g_PW_XE_PROGRAMMING_previous_cursor_pos = GuiLib_ActiveCursorFieldNo;

							CURSOR_Select( CP_PW_XE_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY, (true) );
							break;

						case CP_PW_XE_PROGRAMMING_SUBNET_MASK:
							// 4/21/2015 ajv : If the last cursor position somehow isn't one of the IP address octets,
							// automatically jump to the first octet.
							if( (g_PW_XE_PROGRAMMING_previous_cursor_pos < CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_1) || (g_PW_XE_PROGRAMMING_previous_cursor_pos > CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_4) )
							{
								g_PW_XE_PROGRAMMING_previous_cursor_pos = CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_1;
							}

							// 4/21/2015 ajv : Return back to the last cursor position the user moved down from
							CURSOR_Select( g_PW_XE_PROGRAMMING_previous_cursor_pos, (true) );
							break;

						case CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_1: 
						case CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_2:
						case CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_3:
						case CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_4:
							g_PW_XE_PROGRAMMING_previous_cursor_pos = GuiLib_ActiveCursorFieldNo;

							CURSOR_Select( CP_PW_XE_PROGRAMMING_SUBNET_MASK, (true) );
							break;

						case CP_PW_XE_PROGRAMMING_PROGRAM_DEVICE:
							// 4/21/2015 ajv : If the last cursor position somehow isn't one of the gateway octets,
							// automatically jump to the first octet.
							if( (g_PW_XE_PROGRAMMING_previous_cursor_pos < CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_1) || (g_PW_XE_PROGRAMMING_previous_cursor_pos > CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_4) )
							{
								g_PW_XE_PROGRAMMING_previous_cursor_pos = CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_1;
							}

							CURSOR_Select( g_PW_XE_PROGRAMMING_previous_cursor_pos, (true) );
							break;

						case CP_PW_XE_PROGRAMMING_READ_DEVICE:
							if( GuiVar_ENObtainIPAutomatically )
							{
								CURSOR_Up( (true) );
							}
							else
							{
								// 4/21/2015 ajv : If the last cursor position somehow isn't one of the gateway octets,
								// automatically jump to the first octet.
								if( (g_PW_XE_PROGRAMMING_previous_cursor_pos < CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_1) || (g_PW_XE_PROGRAMMING_previous_cursor_pos > CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_4) )
								{
									g_PW_XE_PROGRAMMING_previous_cursor_pos = CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_1;
								}

								// 4/21/2015 ajv : Return back to the last cursor position the user moved down from
								CURSOR_Select( g_PW_XE_PROGRAMMING_previous_cursor_pos, (true) );
							}
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_LEFT:
					CURSOR_Up( (true) );
					break;

				case KEY_C_DOWN:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_PW_XE_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY:
							if( GuiVar_ENObtainIPAutomatically )
							{
								CURSOR_Down( (true) );
							}
							else
							{
								// 4/21/2015 ajv : If the last cursor position somehow isn't one of the IP address octets,
								// automatically jump to the first octet.
								if( (g_PW_XE_PROGRAMMING_previous_cursor_pos < CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_1) || (g_PW_XE_PROGRAMMING_previous_cursor_pos > CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_4) )
								{
									g_PW_XE_PROGRAMMING_previous_cursor_pos = CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_1;
								}

								// 4/21/2015 ajv : Return back to the last cursor position the user moved down from
								CURSOR_Select( g_PW_XE_PROGRAMMING_previous_cursor_pos, (true) );
							}
							break;

						case CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_1: 
						case CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_2:
						case CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_3:
						case CP_PW_XE_PROGRAMMING_IP_ADDRESS_OCTET_4:
							g_PW_XE_PROGRAMMING_previous_cursor_pos = GuiLib_ActiveCursorFieldNo;

							CURSOR_Select( CP_PW_XE_PROGRAMMING_SUBNET_MASK, (true) );
							break;

						case CP_PW_XE_PROGRAMMING_SUBNET_MASK:
							// 4/21/2015 ajv : If the last cursor position somehow isn't one of the gateway octets,
							// automatically jump to the first octet.
							if( (g_PW_XE_PROGRAMMING_previous_cursor_pos < CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_1) || (g_PW_XE_PROGRAMMING_previous_cursor_pos > CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_4) )
							{
								g_PW_XE_PROGRAMMING_previous_cursor_pos = CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_1;
							}

							// 4/21/2015 ajv : Return back to the last cursor position the user moved down from
							CURSOR_Select( g_PW_XE_PROGRAMMING_previous_cursor_pos, (true) );
							break;

						case CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_1: 
						case CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_2:
						case CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_3:
						case CP_PW_XE_PROGRAMMING_GATEWAY_OCTET_4:
							g_PW_XE_PROGRAMMING_previous_cursor_pos = GuiLib_ActiveCursorFieldNo;

							CURSOR_Select( CP_PW_XE_PROGRAMMING_READ_DEVICE, (true) );
							break;

						case CP_PW_XE_PROGRAMMING_READ_DEVICE:
							bad_key_beep();
							break;

						default:
							CURSOR_Down( (true) );
							break;
					}
					break;

				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_BACK:
					GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;

					// 6/3/2015 mpd : This will allow the screens to be initialized upon reentry. Any unsaved edits 
					// made on this screen or the WIFI Settings screen have been lost. 

					KEY_process_global_keys( pkey_event );

					// 6/5/2015 ajv : Since the user got here from the COMM OPTIONS dialog box, redraw that
					// dialog box.
					COMM_OPTIONS_draw_dialog( (true) );
					break;

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

