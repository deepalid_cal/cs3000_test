/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"d_live_screens.h"

#include	"configuration_controller.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"r_rt_bypass_flow.h"

#include	"r_rt_communications.h"

#include	"r_rt_electrical.h"

#include	"r_rt_poc_flow.h"

#include	"r_rt_system_flow.h"

#include	"r_rt_weather.h"

#include	"r_rt_lights.h"

#include	"screen_utils.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

UNS_32	g_LIVE_SCREENS_cursor_position_when_dialog_displayed;

BOOL_32	g_LIVE_SCREENS_dialog_visible;

// 6/5/2015 ajv : Keep track of the last cursor position the user had selected on the dialog
// so it brings them back to the same field when they open the dialog again.
static UNS_32 g_LIVE_SCREENS_last_cursor_position;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_LIVE_SCREENS_draw_dialog( const BOOL_32 pcomplete_redraw )
{
	// ----------

	g_LIVE_SCREENS_cursor_position_when_dialog_displayed = (UNS_32)GuiLib_ActiveCursorFieldNo;

	// ----------

	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		// 6/5/2015 ajv : If we've never entered the screen before, the last cursor position will be
		// uninitialized at 0. In that case, set it to the top item in the list.
		if( g_LIVE_SCREENS_last_cursor_position == 0 )
		{
			if( GuiVar_MainMenuMainLinesAvailable )
			{
				g_LIVE_SCREENS_last_cursor_position = CP_LIVE_SCREENS_MENU_SYSTEM_FLOW;
			}
			else if( GuiVar_MainMenuPOCsExist == (true) )
			{
				g_LIVE_SCREENS_last_cursor_position = CP_LIVE_SCREENS_MENU_POC_FLOW;
			}
			else if( GuiVar_MainMenuBypassExists == (true) )
			{
				g_LIVE_SCREENS_last_cursor_position = CP_LIVE_SCREENS_MENU_BYPASS_MANIFOLD_FLOW;
			}
			else
			{
				g_LIVE_SCREENS_last_cursor_position = CP_LIVE_SCREENS_MENU_ELECTRICAL;
			}
		}

		lcursor_to_select = g_LIVE_SCREENS_last_cursor_position;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	g_LIVE_SCREENS_dialog_visible = (true);

	GuiLib_ShowScreen( GuiStruct_dlgLiveScreens_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
static void LIVE_SCREENS_change_screen( const UNS_32 pmenu, const UNS_32 pstructure, void (*pdraw_func_ptr)(const BOOL_32 pcomplete_redraw), void (*pprocess_funct_ptr)(const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event), void (*populate_scroll_box_func_ptr)(const INT_16 pindex_0_i16) )
{
	DISPLAY_EVENT_STRUCT	lde;

	GuiVar_MenuScreenToShow = (UNS_32)GuiLib_ActiveCursorFieldNo;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._02_menu = pmenu;
	lde._03_structure_to_draw = pstructure;
	lde._04_func_ptr = pdraw_func_ptr;
	lde._06_u32_argument1 = (true);
	lde._08_screen_to_draw = GuiVar_MenuScreenToShow;
	lde.key_process_func_ptr = pprocess_funct_ptr;
	lde.populate_scroll_box_func_ptr = populate_scroll_box_func_ptr;

	g_LIVE_SCREENS_dialog_visible = (false);

	Change_Screen( &lde );
}

/* ---------------------------------------------------------- */
extern void LIVE_SCREENS_draw_dialog( const BOOL_32 pcomplete_redraw )
{
	DISPLAY_EVENT_STRUCT	lde;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void*)&FDTO_LIVE_SCREENS_draw_dialog;
	lde._06_u32_argument1 = pcomplete_redraw;
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
extern void LIVE_SCREENS_process_dialog( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	// 6/5/2015 ajv : Retain the last cursor position the user was on so they go back to it when
	// they open the dialog again.
	g_LIVE_SCREENS_last_cursor_position = GuiLib_ActiveCursorFieldNo;

	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_LIVE_SCREENS_MENU_SYSTEM_FLOW:
					LIVE_SCREENS_change_screen( SCREEN_MENU_DIAGNOSTICS,
												GuiStruct_rptRTSystemFlow_0,
												(void *)&FDTO_REAL_TIME_SYSTEM_FLOW_draw_report,
												REAL_TIME_SYSTEM_FLOW_process_report,
												NULL );
					break;

				case CP_LIVE_SCREENS_MENU_POC_FLOW:
					LIVE_SCREENS_change_screen( SCREEN_MENU_DIAGNOSTICS,
												GuiStruct_rptRTPOCFlow_0,
												(void *)&FDTO_REAL_TIME_POC_FLOW_draw_report,
												REAL_TIME_POC_FLOW_process_report,
												NULL );
					break;

				case CP_LIVE_SCREENS_MENU_BYPASS_MANIFOLD_FLOW:
					LIVE_SCREENS_change_screen( SCREEN_MENU_DIAGNOSTICS,
												GuiStruct_rptRTBypassFlow_0,
												(void *)&FDTO_REAL_TIME_BYPASS_FLOW_draw_report,
												REAL_TIME_BYPASS_FLOW_process_report,
												NULL );
					break;

				case CP_LIVE_SCREENS_MENU_ELECTRICAL:
					LIVE_SCREENS_change_screen( SCREEN_MENU_DIAGNOSTICS,
												GuiStruct_rptRTElectrical_0,
												(void *)&FDTO_REAL_TIME_ELECTRICAL_draw_report,
												REAL_TIME_ELECTRICAL_process_report,
												NULL );
					break;

				case CP_LIVE_SCREENS_MENU_COMMUNICATIONS:
					LIVE_SCREENS_change_screen( SCREEN_MENU_DIAGNOSTICS,
												GuiStruct_rptRTCommunications_0,
												(void *)&FDTO_REAL_TIME_COMMUNICATIONS_draw_report,
												REAL_TIME_COMMUNICATIONS_process_report,
												NULL );
					break;

				case CP_LIVE_SCREENS_MENU_WEATHER:
					LIVE_SCREENS_change_screen( SCREEN_MENU_DIAGNOSTICS,
												GuiStruct_rptRTWeather_0,
												(void *)&FDTO_REAL_TIME_WEATHER_draw_report,
												REAL_TIME_WEATHER_process_report,
												NULL );
					break;

				case CP_LIVE_SCREENS_MENU_LIGHTS:
					LIVE_SCREENS_change_screen( SCREEN_MENU_DIAGNOSTICS,
												GuiStruct_rptRTLights_0,
												(void *)&FDTO_REAL_TIME_LIGHTS_draw_report,
												REAL_TIME_LIGHTS_process_report,
												NULL );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_C_UP:
			CURSOR_Up( (true) );
			break;

		case KEY_C_DOWN:
			CURSOR_Down( (true) );
			break;

		case KEY_BACK:
			good_key_beep();

			// 3/16/2016 ajv : Retain the last cursor position the user was on so they go back to it when
			// they open the dialog again.
			g_LIVE_SCREENS_last_cursor_position = GuiLib_ActiveCursorFieldNo;

			g_LIVE_SCREENS_dialog_visible = (false);

			// 3/16/2016 ajv : Return to the last cursor position the user was on when they pressed BACK
			// and redraw the screen to remove the dialog box.
			GuiLib_ActiveCursorFieldNo = (INT_16)g_LIVE_SCREENS_cursor_position_when_dialog_displayed;

			Redraw_Screen( (false) );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

