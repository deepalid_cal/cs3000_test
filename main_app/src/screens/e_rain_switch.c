/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/22/2016 mjb : Required for strlcpy
#include	<string.h>

#include	"e_rain_switch.h"

#include	"app_startup.h"

#include	"change.h"

#include	"combobox.h"

#include	"configuration_network.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"flowsense.h"

#include	"irri_comm.h"

#include	"m_main.h"

#include	"scrollbox.h"

#include	"speaker.h"

#include	"weather_control.h"


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_RAIN_SWITCH_IN_USE					(0)
#define	CP_RAIN_SWITCH_SERIAL_NUMBER_LIST		(1)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/16/2016 skc : List of switch in use and chain indices
static struct {
	BOOL_32 in_use;
	UNS_32 	chain_index;
} 					g_RAIN_SWITCH[ MAX_CHAIN_LENGTH ];

static	UNS_32		g_RAIN_SWITCH_count;

static	UNS_32		g_RAIN_SWITCH_active_line;

static	UNS_32		*g_RAIN_SWITCH_combo_box_guivar;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
// 6/16/2016 skc : Scrollbox callback function
// pline_index_0 - index in the screen list
static void RAIN_SWITCH_populate_rain_switch_scrollbox( const INT_16 pline_index_0 )
{
	WHATS_INSTALLED_STRUCT *pwi;

	UNS_32 chain_index;

	// ----------

	chain_index = g_RAIN_SWITCH[pline_index_0].chain_index;
	
	pwi = &chain.members[ chain_index ].box_configuration.wi;  
	 
	// ----------

	NETWORK_CONFIG_get_controller_name_str_for_ui( chain_index, (char*)&GuiVar_RainSwitchControllerName );

	xSemaphoreTakeRecursive( chain_members_recursive_MUTEX, portMAX_DELAY );

	// 3/22/2016 mjb : Let's check the chain members box configuration which we will then use to
	// determine if cards and terminals are or are not present and finally, we will use this to 
	// determine whether or not we need to show SW1 or weather as the connection point on the 
	// GUI screen for Rain Switch. 
	// TODO :  Might have an issue here with controllers on a chain that are not sequential. 
	// Also need to check string length to make sure we don't get too long. 
	if( pwi->weather_terminal_present && pwi->weather_card_present ) 
	{
		strlcat(GuiVar_RainSwitchControllerName, " (Weather Terminal - RainClick)", sizeof(GuiVar_RainSwitchControllerName));
	}
	else if( pwi->poc.card_present && pwi->poc.tb_present) 
	{
		strlcat(GuiVar_RainSwitchControllerName, " (POC Terminal - SW1)", sizeof(GuiVar_RainSwitchControllerName));
	}

	xSemaphoreGiveRecursive( chain_members_recursive_MUTEX );

	GuiVar_RainSwitchSelected = g_RAIN_SWITCH[ pline_index_0 ].in_use;
}

/* ---------------------------------------------------------- */
static void FDTO_RAIN_SWITCH_draw_rain_switch_scrollbox( const BOOL_32 pcomplete_redraw )
{
	UNS_32	i;

	INT_32	lactive_line;

	WHATS_INSTALLED_STRUCT *pwi;

	if( GuiVar_RainSwitchInUse == (true) )
	{
		if( (pcomplete_redraw == (true)) || (GuiLib_ActiveCursorFieldNo == CP_RAIN_SWITCH_IN_USE) )
		{
			lactive_line = GuiLib_NO_CURSOR;

			g_RAIN_SWITCH_count = 0;

			xSemaphoreTakeRecursive( chain_members_recursive_MUTEX, portMAX_DELAY );

			xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

			// 5/1/2014 rmd : If the chain is down or the comm_mngr is not in its normal token making
			// state prevent the user from seeing this screen.
			COMM_MNGR_alert_if_chain_members_should_not_be_referenced( __FILE__, __LINE__ );

			for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
			{
				if( chain.members[ i ].saw_during_the_scan )
				{
					// 6/15/2016 skc : Only add to the list if box has a valid HW configuration.
					// The chain item needs to have both weather card and terminal block, and/or have both POC
					// card and terminal block. If the chain item has both sets, then weather card has
					// priority.
					pwi = &chain.members[ i ].box_configuration.wi;
					if( (pwi->poc.tb_present && pwi->poc.card_present) ||
						(pwi->weather_card_present && pwi->weather_terminal_present) )
					{
						g_RAIN_SWITCH[ g_RAIN_SWITCH_count ].in_use = WEATHER_get_rain_switch_connected_to_this_controller( i );
						g_RAIN_SWITCH[ g_RAIN_SWITCH_count ].chain_index = i;
						g_RAIN_SWITCH_count++;
					}
				}
			}

			xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

			xSemaphoreGiveRecursive( chain_members_recursive_MUTEX );
		}
		else
		{
			if( GuiLib_ActiveCursorFieldNo == CP_RAIN_SWITCH_IN_USE )
			{
				lactive_line = GuiLib_NO_CURSOR;
			}
			else
			{
				lactive_line = (INT_32)g_RAIN_SWITCH_active_line;
			}
		}

		GuiLib_ScrollBox_Close( 1 );
		GuiLib_ScrollBox_Init( 1, &RAIN_SWITCH_populate_rain_switch_scrollbox, (INT_16)g_RAIN_SWITCH_count, (INT_16)lactive_line );
	}
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
static void FDTO_RAIN_SWITCH_enter_rain_switch_scrollbox( void )
{
	good_key_beep();

	GuiLib_ScrollBox_Close( 1 );
	GuiLib_ScrollBox_Init( 1, &RAIN_SWITCH_populate_rain_switch_scrollbox, (INT_16)g_RAIN_SWITCH_count, 0 );

	GuiLib_Cursor_Select( CP_RAIN_SWITCH_SERIAL_NUMBER_LIST );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
static void FDTO_RAIN_SWITCH_leave_rain_switch_scrollbox( void )
{
	GuiLib_ScrollBox_Close( 1 );
	GuiLib_ScrollBox_Init( 1, &RAIN_SWITCH_populate_rain_switch_scrollbox, (INT_16)g_RAIN_SWITCH_count, GuiLib_NO_CURSOR );

	FDTO_Cursor_Up( (true) );
}

/* ---------------------------------------------------------- */
/**
 * Draws the Rain Switch in use dropdown window atop the main window.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the users presses SELECT to change the Rain Switch in use setting.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_RAIN_SWITCH_show_rain_switch_in_use_dropdown( void )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( 184, 18, *g_RAIN_SWITCH_combo_box_guivar );
}

/* ---------------------------------------------------------- */
static void FDTO_RAIN_SWITCH_show_switch_connected_dropdown( const UNS_32 px_coord, const UNS_32 py_coord )
{
	g_RAIN_SWITCH_active_line = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 1, 0 );

	FDTO_COMBO_BOX_show_no_yes_dropdown( px_coord, py_coord, *g_RAIN_SWITCH_combo_box_guivar );
}

/* ---------------------------------------------------------- */
static void FDTO_RAIN_SWITCH_close_rain_switch_connected_dropdown( void )
{
	UNS_32 index;

	index = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 1, 0 );

	*g_RAIN_SWITCH_combo_box_guivar = (BOOL_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );

	nm_WEATHER_set_rain_switch_connected( *g_RAIN_SWITCH_combo_box_guivar, g_RAIN_SWITCH[index].chain_index, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, WEATHER_get_change_bits_ptr( CHANGE_REASON_KEYPAD ) );

	FDTO_COMBOBOX_hide();
}

/* ---------------------------------------------------------- */
extern void FDTO_RAIN_SWITCH_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		WEATHER_copy_rain_switch_settings_into_GuiVars();

		lcursor_to_select = 0;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrRainSwitch_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );

	FDTO_RAIN_SWITCH_draw_rain_switch_scrollbox( pcomplete_redraw );
}

/* ---------------------------------------------------------- */
/**
 * Processes the key event from the Rain Switch Setup screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user presses a key on the Rain Switch Setup screen.
 * 
 * @param pkey_event The key event to process.
 *
 * @author 8/29/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void RAIN_SWITCH_process_screen( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	UNS_32	lcontroller_index;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_cbxNoYes_0:
			if( g_RAIN_SWITCH_combo_box_guivar == &GuiVar_RainSwitchInUse )
			{
				COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_RainSwitchInUse );
			}
			else
			{
				// Redraw the Rain Switch scrollbox if the user closed the dropdown.
				if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
				{
					good_key_beep();

					lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
					lde._04_func_ptr = FDTO_RAIN_SWITCH_close_rain_switch_connected_dropdown;
					Display_Post_Command( &lde );

					Redraw_Screen( (false) );
				}
				else
				{
					COMBO_BOX_key_press( pkey_event.keycode, NULL );
				}
			}
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_RAIN_SWITCH_IN_USE:
							good_key_beep();

							g_RAIN_SWITCH_combo_box_guivar = &GuiVar_RainSwitchInUse;

							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_RAIN_SWITCH_show_rain_switch_in_use_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_RAIN_SWITCH_SERIAL_NUMBER_LIST:
							good_key_beep();

							lcontroller_index = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 1, 0 );

							g_RAIN_SWITCH_combo_box_guivar = &g_RAIN_SWITCH[ lcontroller_index ].in_use;

							lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
							lde._04_func_ptr = (void*)&FDTO_RAIN_SWITCH_show_switch_connected_dropdown;
							lde._06_u32_argument1 = 266;
							lde._07_u32_argument2 = (lcontroller_index == 0) ? 47 : 47 + (lcontroller_index * 14);
							Display_Post_Command( &lde );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch ( GuiLib_ActiveCursorFieldNo )
					{
						case CP_RAIN_SWITCH_IN_USE:
							process_bool( &GuiVar_RainSwitchInUse );

							// Since we have to show or hide the various Rain Switch
							// settings based upon this value, redraw the entire
							// screen.
							Redraw_Screen( (false) );
							break;

						case CP_RAIN_SWITCH_SERIAL_NUMBER_LIST:
							good_key_beep();
							lcontroller_index = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 1, 0 );

							g_RAIN_SWITCH[ lcontroller_index ].in_use = !(g_RAIN_SWITCH[ lcontroller_index ].in_use);

							nm_WEATHER_set_rain_switch_connected( g_RAIN_SWITCH[ lcontroller_index ].in_use, 
																  g_RAIN_SWITCH[ lcontroller_index ].chain_index,
																  CHANGE_generate_change_line, 
																  CHANGE_REASON_KEYPAD, 
																  FLOWSENSE_get_controller_index(), 
																  CHANGE_set_change_bits, 
																  WEATHER_get_change_bits_ptr( CHANGE_REASON_KEYPAD ) );
							SCROLL_BOX_redraw( 1 );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_PREV:
				case KEY_C_UP:
				case KEY_C_LEFT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_RAIN_SWITCH_SERIAL_NUMBER_LIST:
							if( GuiLib_ScrollBox_GetActiveLine( 1, 0 ) == 0 )
							{
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = FDTO_RAIN_SWITCH_leave_rain_switch_scrollbox;
								Display_Post_Command( &lde );
							}
							else
							{
								SCROLL_BOX_up_or_down( 1, KEY_C_UP );
							}
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_NEXT:
				case KEY_C_DOWN:
				case KEY_C_RIGHT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_RAIN_SWITCH_IN_USE:
							if( GuiVar_RainSwitchInUse == (true) )
							{
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = FDTO_RAIN_SWITCH_enter_rain_switch_scrollbox;
								Display_Post_Command( &lde );
							}
							else
							{
								bad_key_beep();
							}
							break;

						case CP_RAIN_SWITCH_SERIAL_NUMBER_LIST:
							SCROLL_BOX_up_or_down( 1, KEY_C_DOWN );
							break;

						default:
							bad_key_beep ();
					}
					break;

				default:
					if( pkey_event.keycode == KEY_BACK )
					{
						WEATHER_extract_and_store_rain_switch_changes_from_GuiVars();

						GuiVar_MenuScreenToShow = CP_MAIN_MENU_WEATHER;
					}

					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

