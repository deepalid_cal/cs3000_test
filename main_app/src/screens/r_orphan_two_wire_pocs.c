/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"r_orphan_two_wire_pocs.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"change.h"

#include	"d_two_wire_assignment.h"

#include	"d_two_wire_discovery.h"

#include	"foal_defs.h"

#include	"flowsense.h"

#include	"group_base_file.h"

#include	"pdata_changes.h"

#include	"report_utils.h"

#include	"screen_utils.h"

#include	"speaker.h"

#include	"stations.h"

#include	"tpmicro_data.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 6/10/2016 rmd : Include the POC search KEY fields.
	
	UNS_32	box_index_0;
	
	UNS_32	poc_type;
	
	// 6/10/2016 rmd : We want a single decoder serial number. An ORPHAN is a decoder serial
	// number within a POC where the decoder was not found in the discovery.
	UNS_32	decoder_serial_number;

} ORPHAN_POC_ITEM;

static ORPHAN_POC_ITEM ORPHAN_POCS_list_of_orphans[ MAX_POCS_IN_NETWORK ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32 g_ORPHAN_TWO_WIRE_POCS_num_pocs;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern UNS_32 ORPHAN_TWO_WIRE_POCS_populate_list( void )
{
	POC_GROUP_STRUCT	*lpoc;

	BOOL_32	ldecoder_was_found;

	UNS_32	lorphan_box_index_0, lour_index_0;

	UNS_32	lorphan_poc_type;

	UNS_32	lorphan_serial_number;

	UNS_32	lpoc_gid;

	UNS_32	i, j, k;

	// ----------

	g_ORPHAN_TWO_WIRE_POCS_num_pocs = 0;

	// 6/10/2016 rmd : Only locate POC's on this box.
	lour_index_0 = FLOWSENSE_get_controller_index();

	// ----------

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	for( i = 0; i < POC_get_list_count(); ++i )
	{
		lpoc = POC_get_group_at_this_index( i );

		if( lpoc != NULL )
		{
			lpoc_gid = nm_GROUP_get_group_ID( lpoc );

			// ----------
			
			lorphan_poc_type = POC_get_type_of_poc( lpoc_gid );

			lorphan_box_index_0 = POC_get_box_index_0( lpoc_gid );

			// ----------
			
			// 10/2/2014 ajv : Since 2-Wire processes are a by-controller affair, only look at stations
			// that belong to this physical box.
			//
			// 6/9/2016 rmd : By definition if a BYPASS is set to not show ALL its decoder serial
			// numbers are 0. If a single is showing we do indeed need to include it in this list.
			if( (lorphan_poc_type != POC__FILE_TYPE__TERMINAL) && (lorphan_box_index_0 == lour_index_0) && POC_get_show_for_this_poc( lpoc ) )
			{
				for( j = 0; j < POC_BYPASS_LEVELS_MAX; j++ )
				{
					lorphan_serial_number = POC_get_decoder_serial_number_for_this_poc_gid_and_level_0( lpoc_gid, j );
					
					if( lorphan_serial_number != DECODER_SERIAL_NUM_DEFAULT )
					{
						ldecoder_was_found = (false);

						xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );

						for( k=0; k<MAX_DECODERS_IN_A_BOX; ++k )
						{
							if( lorphan_serial_number == tpmicro_data.decoder_info[ k ].sn )
							{
								ldecoder_was_found = (true);

								// 9/2/2014 ajv : We found the decoder assigned to this
								// station in the decoder list, so we're done with this
								// station. Break out of the loop.
								break;
							}
						}

						xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );

						// 12/15/2014 ajv : If we did not find the decoder in the list, it means we found a decoder
						// serial number that is NO LONGER CONNECTED to the 2-Wire path. Add this item to the list
						// of displayed POCs.
						if( !ldecoder_was_found )
						{
							ORPHAN_POCS_list_of_orphans[ g_ORPHAN_TWO_WIRE_POCS_num_pocs ].box_index_0 = lorphan_box_index_0;
							ORPHAN_POCS_list_of_orphans[ g_ORPHAN_TWO_WIRE_POCS_num_pocs ].poc_type = lorphan_poc_type;
							ORPHAN_POCS_list_of_orphans[ g_ORPHAN_TWO_WIRE_POCS_num_pocs ].decoder_serial_number = lorphan_serial_number;

							g_ORPHAN_TWO_WIRE_POCS_num_pocs += 1;
						}
					}
				}
			}
		}
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( g_ORPHAN_TWO_WIRE_POCS_num_pocs );
}

/* ---------------------------------------------------------- */
static void ORPHAN_TWO_WIRE_POCS_load_guivars_for_scroll_line( const INT_16 pline_index_0_i16 )
{
	GuiVar_RptDecoderSN = ORPHAN_POCS_list_of_orphans[ pline_index_0_i16 ].decoder_serial_number;
}

/* ---------------------------------------------------------- */
extern void FDTO_ORPHAN_TWO_WIRE_POCS_draw_report( const BOOL_32 pcomplete_redraw )
{
	GuiLib_ShowScreen( GuiStruct_rptOrphanTwoWirePOCs_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );

	FDTO_REPORTS_draw_report( pcomplete_redraw, g_ORPHAN_TWO_WIRE_POCS_num_pocs, &ORPHAN_TWO_WIRE_POCS_load_guivars_for_scroll_line );
}

/* ---------------------------------------------------------- */
extern void ORPHAN_TWO_WIRE_POCS_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	POC_GROUP_STRUCT	*lpoc;

	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	BOOL_32	no_other_decoders_assigned_to_this_bypass;
	
	UNS_32	i;

	UNS_32	ltarget_decoder_ser_no, lfile_decoder_ser_no;

	UNS_32	ltarget_box_index_0, ltarget_poc_type;
	
	// ----------

	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			if( GuiLib_ScrollBox_GetActiveLine(0, 0) >= 0 )
			{
				xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

				ltarget_box_index_0 = ORPHAN_POCS_list_of_orphans[ GuiLib_ScrollBox_GetActiveLine(0, 0) ].box_index_0;
				ltarget_poc_type = ORPHAN_POCS_list_of_orphans[ GuiLib_ScrollBox_GetActiveLine(0, 0) ].poc_type;
				ltarget_decoder_ser_no = ORPHAN_POCS_list_of_orphans[ GuiLib_ScrollBox_GetActiveLine(0, 0) ].decoder_serial_number;
				
				// 6/10/2016 rmd : Find the target POC in the file. Remember we are only after those with
				// their SHOW flag set.
				if( (lpoc = nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn( ltarget_box_index_0, ltarget_poc_type, ltarget_decoder_ser_no, POC_SET_TO_SHOW_ONLY )) )
				{
					good_key_beep();
				
					// 6/9/2016 rmd : Must update the change bitfield ptr for the NEW poc we are working with.
					lchange_bitfield_to_set = POC_get_change_bits_ptr( lpoc, CHANGE_REASON_KEYPAD );
					
					// ----------
					
					// 6/10/2016 rmd : Regardless of poc_type we can perform this same chaeck against each poc
					// bypass level.

					// 6/9/2016 rmd : So we found a bypass. Sift through it to find if the serial number is in
					// it. If so set to 0. And if after that all ser no slots empty set to NOT SHOW as it is an
					// empty bypass.
					no_other_decoders_assigned_to_this_bypass = (true);
	
					// 6/9/2016 rmd : We should search through ALL levels looking for this serial number. To
					// purge any instance of it from the bypass. If the user assigned it to level 3 then set to
					// a 2 stage bypass it could effectively be hidden. Just seems we should clean it up here.
					for( i=0; i<POC_BYPASS_LEVELS_MAX; i++ )
					{
						lfile_decoder_ser_no = POC_get_decoder_serial_number_for_this_poc_and_level_0( lpoc, i );
	
						if( lfile_decoder_ser_no == ltarget_decoder_ser_no )
						{
							// 11/20/2018 Ryan : Setting the serial number back to zero was causing an error on the
							// slave controllers on a chain, because they use the decoder serial number to identify
							// POCs, as the gids are different for POCs between controllers on the chain. Without the
							// ability to identify the POCs, they could not be delinked on the slave controllers. If we
							// want to have the ablility to change the serial number to zero in the future, we might
							// need to add a new variable to the POC structure to identify the POC accross different
							// controller on the chain.

							//nm_POC_set_decoder_serial_number( lpoc, (i + 1), DECODER_SERIAL_NUM_DEFAULT, CHANGE_do_not_generate_change_line, 0, ltarget_box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
						}
						else
						if( lfile_decoder_ser_no != DECODER_SERIAL_NUM_DEFAULT )
						{
							no_other_decoders_assigned_to_this_bypass = (false);
						}
					}
	
					if( no_other_decoders_assigned_to_this_bypass )
					{
						// 6/1/2016 rmd : So as far as we are concerned right now there are NO DECODERS assigned to
						// this bypass poc. Mark it to NEVER show again.
						nm_POC_set_show_this_poc( lpoc, POC_SHOW_THIS_POC_DEFAULT, CHANGE_do_not_generate_change_line, 0, ltarget_box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
					}
				}
				else
				{
					bad_key_beep();

					Alert_Message( "Unable to find POC to remove" );
				}
				
				// ----------
				
				xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

				// 12/15/2014 ajv : Repopulate the list since it's contents have changed.
				ORPHAN_TWO_WIRE_POCS_populate_list();

				Redraw_Screen( (true) );
			}
			else
			{
				bad_key_beep();
			}
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;

			KEY_process_global_keys( pkey_event );

			// 3/17/2016 ajv : Since the user got here from the 2-WIRE dialog box, redraw that dialog
			// box.
			TWO_WIRE_ASSIGNMENT_draw_dialog( (true) );
			break;

		default:
			REPORTS_process_report( pkey_event, 0, 0 );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

