/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"e_status.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"background_calculations.h"

#include	"battery_backed_vars.h"

#include	"budgets.h"

#include	"change.h"

#include	"configuration_network.h"

#include	"cursor_utils.h"

#include	"d_live_screens.h"

#include	"d_process.h"

#include	"e_manual_programs.h"

#include	"e_poc.h"

#include	"e_programs.h"

#include	"e_station_groups.h"

#include	"e_stations_in_use.h"

#include	"e_turn_off.h"

#include	"e_weather_sensors.h"

#include	"flowsense.h"

#include	"irri_flow.h"

#include	"irri_irri.h"

#include	"irri_comm.h"

#include	"irri_lights.h"

#include	"r_budgets.h"

#include	"r_irri_details.h"

#include	"r_rt_communications.h"

#include	"r_rt_electrical.h"

#include	"r_rt_lights.h"

#include	"r_rt_system_flow.h"

#include	"r_rt_weather.h"

#include	"speaker.h"

#include	"tpmicro_data.h"

#include	"weather_control.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 02/09/2016 skc
#define CP_STATUS_BUDGET_IN_EFFECT			(19)

// 5/6/2015 ajv : Major alert conditions. Up to two can be displayed at any given time on
// the screen.
#define CP_STATUS_MLB_DETECTED				(20)
#define CP_STATUS_MVOR_IN_EFFECT			(21)
#define CP_STATUS_STATION_FLOW_ERRORS		(22)
#define CP_STATUS_STATION_ELEC_ERRORS		(23)
#define CP_STATUS_NOW_DAYS_IN_EFFECT		(24)

// 5/6/2015 ajv : Irrigation activity cursor positions.
#define CP_STATUS_CHAIN_DOWN				(30)
#define CP_STATUS_NO_STATIONS_IN_USE		(31)
#define CP_STATUS_2_WIRE_EXCESS_CURRENT		(32)
#define CP_STATUS_2_WIRE_OVERHEATED			(33)
#define CP_STATUS_FUSE_BLOWN				(34)
#define CP_STATUS_RAIN_SWITCH_ACTIVE		(35)
#define CP_STATUS_FREEZE_SWITCH_ACTIVE		(36)
#define CP_STATUS_IRRIGATION_RUNNING		(37)
#define CP_STATUS_NO_STATIONS_WATERING		(38)

// 5/6/2015 ajv : Next scheduled irrigation cursor positions.
#define	CP_STATUS_NEXT_SCHEDULED			(40)

// 5/6/2015 ajv : Weather cursor positions.
#define	CP_STATUS_ET_GAGE					(50)
#define CP_STATUS_RUNAWAY_GAGE				(51)
#define CP_STATUS_RAIN						(52)
#define CP_STATUS_WIND						(53)

// 5/6/2015 ajv : Flow cursor positions.
#define CP_STATUS_FLOW_RATE					(60)
#define CP_STATUS_NO_FLOW_METER				(61)

// 5/6/2015 ajv : Electrical current cursor positions.
#define CP_STATUS_CURRENT					(70)

// 11/28/2017 ajv : Access Control Login/Logout cursor position.
#define CP_STATUS_LOGIN						(71)

// 10/23/2015 ajv : Lights-related cursor positions.
#define	CP_STATUS_LIGHTS_ON					(75)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	STATUS_IRRIG_ACTIVITY_CHAIN_DOWN			(0)
#define	STATUS_IRRIG_ACTIVITY_NO_STATIONS_IN_USE	(1)
#define	STATUS_IRRIG_ACTIVITY_2_WIRE_EXCESS_CURRENT	(2)
#define	STATUS_IRRIG_ACTIVITY_2_WIRE_OVERHEATED		(3)
#define	STATUS_IRRIG_ACTIVITY_FUSE_BLOWN			(4)
#define	STATUS_IRRIG_ACTIVITY_RAIN_SWITCH_ACTIVE	(5)
#define	STATUS_IRRIG_ACTIVITY_FREEZE_SWITCH_ACTIVE	(6)
#define	STATUS_IRRIG_ACTIVITY_IRRIGATION_RUNNING	(7)
#define	STATUS_IRRIG_ACTIVITY_NO_STATIONS_WATERING	(8)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

UNS_32	g_STATUS_last_cursor_position;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * @task_execution Executed within the context of the KEY PROCESSING task when the user 
 * moves around the Status; within the context of the TD CHECK task on a 1 Hz basis to keep 
 * the screen up-to-date while the user is viewing the screen.
 */
static BOOL_32 FDTO_STATUS_update_major_alert( void )
{
	// 10/9/2013 ajv : A number of GuiVars are used to determine whether to show or hide
	// specific features on the screen. Unfortunately, in order for the screen to update when
	// the state of one of these changes, the screen needs to be completely redrawn. Therefore,
	// we'll track if any of these variables' states change and, if so, trigger a redraw.
	BOOL_32	lmajor_alert_changed;

	lmajor_alert_changed = (false);

	// ----------

	// 2012.06.19 ajv : Always take the preserves mutex before taking the list mutex. We may be
	// able to get around needing the list mutex here by building the entire screen using the
	// perserves, but I'm not sure how we'd deal with getting the name at the moment. Will
	// reevaluate later.
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem_struct;

	BY_SYSTEM_RECORD	*lpreserve;

	BOOL_32	mlb_in_effect;

	BOOL_32	mvor_in_effect;

	UNS_32	i;

	// 5/5/2015 ajv : Detect whether a mainline break or master valve override exists.
	mlb_in_effect = (false);
	mvor_in_effect = (false);

	for( i = 0; i < MAX_POSSIBLE_SYSTEMS; ++i )
	{
		lsystem_struct = SYSTEM_get_group_at_this_index( i );

		if( lsystem_struct != NULL )
		{
			lpreserve = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( nm_GROUP_get_group_ID(lsystem_struct) );

			if( lpreserve != NULL )
			{
				if( SYSTEM_PRESERVES_there_is_a_mlb( &(lpreserve->delivered_mlb_record) ) )
				{
					mlb_in_effect = (true);

					// 5/5/2015 ajv : Since we've found a mainline break, break out of the loop. At this stage,
					// we don't really care which system the mainline break is on.
					break;
				}
				else if( lpreserve->sbf.delivered_MVOR_in_effect_opened || lpreserve->sbf.delivered_MVOR_in_effect_closed )
				{
					mvor_in_effect = (true);
				}
			}
		}
	}

	if( GuiVar_StatusMLBInEffect != mlb_in_effect )
	{
		GuiVar_StatusMLBInEffect = mlb_in_effect;

		lmajor_alert_changed = (true);
	}
	else if( GuiVar_StatusMVORInEffect != mvor_in_effect )
	{
		GuiVar_StatusMVORInEffect = mvor_in_effect;

		lmajor_alert_changed = (true);
	}

	// ----------

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	// ----------

	return( lmajor_alert_changed );
}

/* ---------------------------------------------------------- */
/**
 * @task_execution Executed within the context of the KEY PROCESSING task when the user 
 * moves around the Status; within the context of the TD CHECK task on a 1 Hz basis to keep 
 * the screen up-to-date while the user is viewing the screen.
 */
static BOOL_32 FDTO_STATUS_update_irrigation_activity( void )
{
	BOOL_32	lirrig_activity_state_changed;

	UNS_32	previous_state;

	UNS_32	i;

	// ----------

	lirrig_activity_state_changed = (false);

	previous_state = GuiVar_StatusIrrigationActivityState;

	// ----------

	BOOL_32	excess_current_detected;

	BOOL_32	cable_overheated;

	excess_current_detected = (false);

	cable_overheated = (false);

	for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
	{
		if( irri_comm.two_wire_cable_excessive_current[ i ] )
		{
			excess_current_detected = (true);
		}

		if( irri_comm.two_wire_cable_overheated[ i ] )
		{
			cable_overheated = (true);
		}
	}

	// ----------

	if( comm_mngr.chain_is_down )
	{
		GuiVar_StatusIrrigationActivityState = STATUS_IRRIG_ACTIVITY_CHAIN_DOWN;
	}
	else if( (STATION_get_num_of_stations_physically_available() == 0) ||
			 (STATION_get_num_stations_in_use() == 0) )
	{
		GuiVar_StatusIrrigationActivityState = STATUS_IRRIG_ACTIVITY_NO_STATIONS_IN_USE;
	}
	else if( excess_current_detected )
	{
		GuiVar_StatusIrrigationActivityState = STATUS_IRRIG_ACTIVITY_2_WIRE_EXCESS_CURRENT;
	}
	else if( cable_overheated )
	{
		GuiVar_StatusIrrigationActivityState = STATUS_IRRIG_ACTIVITY_2_WIRE_OVERHEATED;
	}
	else if( tpmicro_comm.fuse_blown )
	{
		GuiVar_StatusIrrigationActivityState = STATUS_IRRIG_ACTIVITY_FUSE_BLOWN;
	}
	else if( GuiVar_StatusRainSwitchState )
	{
		GuiVar_StatusIrrigationActivityState = STATUS_IRRIG_ACTIVITY_RAIN_SWITCH_ACTIVE;
	}
	else if( GuiVar_StatusFreezeSwitchState )
	{
		GuiVar_StatusIrrigationActivityState = STATUS_IRRIG_ACTIVITY_FREEZE_SWITCH_ACTIVE;
	}
	else
	{
		xSemaphoreTakeRecursive( irri_irri_recursive_MUTEX, portMAX_DELAY );

		IRRI_MAIN_LIST_OF_STATIONS_STRUCT	*liml, *liml_thats_on;

		UNS_32	lstations_on;

		lstations_on = 0;

		liml_thats_on = NULL;  // compiler was complaining about un-initialized use

		liml = nm_ListGetFirst( &irri_irri.list_of_irri_all_irrigation );

		// Find the number of stations ON by looking at the remaining seconds on
		while( liml != NULL )
		{
			if( liml->ibf.station_is_ON == (true) )
			{
				++lstations_on;

				liml_thats_on = liml;
			}
			liml = nm_ListGetNext( &irri_irri.list_of_irri_all_irrigation, liml );
		}

		// 7/2/2015 ajv : Initialze the CycleLeft string in case it's not used. So far, it's only
		// used in the case where there's one station on.
		strlcpy( GuiVar_StatusCycleLeft, "", sizeof(GuiVar_StatusCycleLeft) );

		if( lstations_on > 0 )
		{
			if( lstations_on == 1 )
			{
				if( liml_thats_on != NULL )
				{
					GuiVar_StatusIrrigationActivityState = STATUS_IRRIG_ACTIVITY_IRRIGATION_RUNNING;

					char	station_str[ 48 ], controller_str[ 48 ];

					snprintf( GuiVar_StatusOverviewStationStr, sizeof(GuiVar_StatusOverviewStationStr), "Station %s on %s is watering", STATION_get_station_number_string( liml_thats_on->box_index_0, liml_thats_on->station_number_0_u8, (char*)&station_str, sizeof(station_str) ), NETWORK_CONFIG_get_controller_name_str_for_ui(liml_thats_on->box_index_0, (char*)&controller_str) );
					snprintf( GuiVar_StatusCycleLeft, sizeof(GuiVar_StatusCycleLeft), "%.1f min left of a %.1f cycle", (liml_thats_on->remaining_ON_or_soak_seconds / 60.0), (liml_thats_on->cycle_seconds / 60.0) );
				}
			}
			else
			{
				GuiVar_StatusIrrigationActivityState = STATUS_IRRIG_ACTIVITY_IRRIGATION_RUNNING;;

				snprintf( GuiVar_StatusOverviewStationStr, sizeof(GuiVar_StatusOverviewStationStr), "%d valves are on", lstations_on );
			}
		}
		else if( irri_irri.list_of_irri_all_irrigation.count > 0 )
		{
			if( irri_irri.list_of_irri_all_irrigation.count > 1 )
			{
				if( GuiVar_StatusWindPaused == (true) )
				{
					GuiVar_StatusIrrigationActivityState = STATUS_IRRIG_ACTIVITY_IRRIGATION_RUNNING;;

					snprintf( GuiVar_StatusOverviewStationStr, sizeof(GuiVar_StatusOverviewStationStr), "There are %d stations PAUSED", irri_irri.list_of_irri_all_irrigation.count );
				}
				else
				{
					GuiVar_StatusIrrigationActivityState = STATUS_IRRIG_ACTIVITY_IRRIGATION_RUNNING;

					snprintf( GuiVar_StatusOverviewStationStr, sizeof(GuiVar_StatusOverviewStationStr), "There are %d stations WAITING or SOAKING", irri_irri.list_of_irri_all_irrigation.count );
				}
			}
			else if( irri_irri.list_of_irri_all_irrigation.count == 1 )
			{
				if( GuiVar_StatusWindPaused == (true) )
				{
					GuiVar_StatusIrrigationActivityState = STATUS_IRRIG_ACTIVITY_IRRIGATION_RUNNING;

					strlcpy( GuiVar_StatusOverviewStationStr, "There is 1 station PAUSED", sizeof(GuiVar_StatusOverviewStationStr) );
				}
				else
				{
					GuiVar_StatusIrrigationActivityState = STATUS_IRRIG_ACTIVITY_IRRIGATION_RUNNING;

					strlcpy( GuiVar_StatusOverviewStationStr, "There is 1 station WAITING or SOAKING", sizeof(GuiVar_StatusOverviewStationStr) );
				}
			}
		}
		else
		{
			GuiVar_StatusIrrigationActivityState = STATUS_IRRIG_ACTIVITY_NO_STATIONS_WATERING;
		}

		xSemaphoreGiveRecursive( irri_irri_recursive_MUTEX );
	}

	if( GuiVar_StatusIrrigationActivityState != previous_state )
	{
		lirrig_activity_state_changed = (true);
	}

	#if 0
	else if( (GuiVar_StatusWindPaused == (true)) && (irri_irri.list_of_irri_all_irrigation.count == 0) )
	{
		lscreen_to_show = STATUS_OVERVIEW_STATIONS_PAUSED_WIND;

		strlcpy( GuiVar_StatusOverviewStationStr, "Some stations may not water", sizeof(GuiVar_StatusOverviewStationStr) );
	}
	#endif

	// ----------

	xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );

	GuiVar_StatusETGageReading = (float)(weather_preserves.et_rip.et_inches_u16_10000u / 10000.0);

	GuiVar_ETGagePercentFull = (((float)weather_preserves.remaining_gage_pulses / ET_GAGE_PULSE_CAPACITY) * 100.0);

	// 9/25/2012 rmd : Show the actual amount of rain the bucket has seen. Not the 'massaged'
	// amount headed for the table.
	//
	// 4/28/2015 rmd : This used to be the roll time to roll time pulse count. I switched to
	// mid-night to mid-night. That seems to relate better to the user.
	GuiVar_StatusRainBucketReading = (float)(weather_preserves.rain.midnight_to_midnight_raw_pulse_count / 100.0);

	// 10/13/2014 ajv : There's no need to set GuiVar_StatusWindGageReading
	// here. This is handled as part of the incoming token from the master.

	xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );

	// ----------

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem_struct;

	BY_SYSTEM_RECORD	*lpreserve;

	// 4/15/2016 ajv : For now, tally all of the mainlines' flow for display. If the user wants
	// to see a mainline-by-mainline breakdown, they can go to one of the irrigation details
	// screens.
	if( GuiVar_StatusShowSystemFlow == (true) )
	{
		GuiVar_StatusSystemFlowActual = 0;

		for( i = 0; i < MAX_POSSIBLE_SYSTEMS; ++i )
		{
			lsystem_struct = SYSTEM_get_group_at_this_index( i );

			if( lsystem_struct != NULL )
			{
				lpreserve = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( nm_GROUP_get_group_ID(lsystem_struct) );

				if( lpreserve != NULL )
				{
					GuiVar_StatusSystemFlowActual += (UNS_32)lpreserve->system_rcvd_most_recent_token_5_second_average;
				}
			}
		}

		// 6/23/2015 ajv : For now, we've decided to show the highest state for any of the mainlines
		// that are in use.
		GuiVar_StatusSystemFlowStatus = SYSTEM_get_highest_flow_checking_status_for_any_system();
	}

	// 02/08/2016 skc : See if we need to display "Budget Reductions In Effect"
	// 5/12/2016 skc : Only if there are POCs in the system!
	previous_state = GuiVar_BudgetsInEffect;
	GuiVar_BudgetsInEffect = BUDGET_in_effect_for_any_system();
	if( GuiVar_BudgetsInEffect != previous_state )
	{
		lirrig_activity_state_changed = (true);
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	// ----------

	xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );

	// 3/24/2016 rmd : Due to leakage current through the .022uF cap across the triac and the
	// transorb diode to gnd with 48 stations installed we can see .01 amps on the status even
	// though nothing is ON. We should supress this from the user as it just makes questions.
	// Even our own service people are perplexed when seeing this. So if the value is less than
	// 16ma I am going to call that 0. We have seen a few cases of the .01 showing on the
	// display and never .02. So I choose 16 based upon that.
	//
	// 3/24/2016 rmd : NOTE - performing this adjustment here has the benefit of leaving the raw
	// measurement intact if needed elsewhere (engineering or tectnical display).
	if( tpmicro_data.measured_ma_current_as_rcvd_from_master[ FLOWSENSE_get_controller_index() ] <= 16 )
	{
		GuiVar_StatusCurrentDraw = (0.0);
	}
	else
	{
		GuiVar_StatusCurrentDraw = (float)(tpmicro_data.measured_ma_current_as_rcvd_from_master[ FLOWSENSE_get_controller_index() ] / 1000.0);
	}

	xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );

	// ----------

	return( lirrig_activity_state_changed );
}

/* ---------------------------------------------------------- */
/**
 * @task_execution Executed within the context of the KEY PROCESSING task when the user 
 * moves around the Status; within the context of the TD CHECK task on a 1 Hz basis to keep 
 * the screen up-to-date while the user is viewing the screen.
 */
static void FDTO_STATUS_update_screen( void )
{
	// 10/9/2013 ajv : A number of GuiVars are used to determine whether to show or hide
	// specific features on the screen. Unfortunately, in order for the screen to update when
	// the state of one of these changes, the screen needs to be completely redrawn. Therefore,
	// we'll track if any of these variables' states change and, if so, trigger a redraw.
	BOOL_32	lweather_or_flow_state_changed;

	lweather_or_flow_state_changed = (false);

	// ----------
	
	// 10/9/2013 ajv : Set the various 'connected' GuiVars to ensure the
	// appropriate weather options are displayed on the screen.
	if( GuiVar_StatusETGageConnected != WEATHER_get_et_gage_is_in_use() )
	{
		GuiVar_StatusETGageConnected = WEATHER_get_et_gage_is_in_use();

		lweather_or_flow_state_changed = (true);
	}

	// ----------

	xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );

	if( GuiVar_ETGageRunawayGage != weather_preserves.run_away_gage )
	{
		GuiVar_ETGageRunawayGage = weather_preserves.run_away_gage;

		// 8/28/2014 ajv : Since the state of the runaway gage changed, force a
		// complete redraw to show or hide the button.
		lweather_or_flow_state_changed = (true);
	}

	xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );

	// ----------

	if( GuiVar_StatusRainBucketConnected != WEATHER_get_rain_bucket_is_in_use() )
	{
		GuiVar_StatusRainBucketConnected = WEATHER_get_rain_bucket_is_in_use();

		lweather_or_flow_state_changed = (true);
	}

	// ----------

	if( GuiVar_StatusWindGageConnected != WEATHER_get_wind_gage_in_use() )
	{
		GuiVar_StatusWindGageConnected = WEATHER_get_wind_gage_in_use();

		lweather_or_flow_state_changed = (true);
	}

	// ----------

	if( GuiVar_StatusRainSwitchConnected != WEATHER_get_rain_switch_in_use() )
	{
		GuiVar_StatusRainSwitchConnected = WEATHER_get_rain_switch_in_use();

		lweather_or_flow_state_changed = (true);
	}

	// ----------

	if( GuiVar_StatusFreezeSwitchConnected != WEATHER_get_freeze_switch_in_use() )
	{
		GuiVar_StatusFreezeSwitchConnected = WEATHER_get_freeze_switch_in_use();

		lweather_or_flow_state_changed = (true);
	}

	// ---------------------

	BOOL_32	lprevious_show_flow_state;

	lprevious_show_flow_state = GuiVar_StatusShowSystemFlow;

	// 10/2/2013 ajv : TODO - We need to find some way to display flow for
	// multiple systems on the Status screen.
	GuiVar_StatusShowSystemFlow = (POC_at_least_one_POC_has_a_flow_meter() == (true)) && (SYSTEM_num_systems_in_use() > 0);

	if( lprevious_show_flow_state != GuiVar_StatusShowSystemFlow )
	{
		lweather_or_flow_state_changed = (true);
	}
	
	// ----------

	BOOL_32	lprevious_lights_energized_state;

	lprevious_lights_energized_state = GuiVar_LightsAreEnergized;

	// 9/21/2015 mpd : Display "Lights are ON" if any light in the system is energized.
	GuiVar_LightsAreEnergized = ( IRRI_LIGHTS_get_number_of_energized_lights() > 0 );

	if( lprevious_lights_energized_state != GuiVar_LightsAreEnergized )
	{
		lweather_or_flow_state_changed = (true);
	}

	// ----------

	BOOL_32	lmajor_alert_changed;

	BOOL_32	lirrig_activity_state_changed;

	lmajor_alert_changed = FDTO_STATUS_update_major_alert();

	lirrig_activity_state_changed = FDTO_STATUS_update_irrigation_activity();

	// 10/9/2013 ajv : If we need to redraw the screen, do so now. Otherwise,
	// update the screen as normal.
	if( (lweather_or_flow_state_changed) || (lmajor_alert_changed) || (lirrig_activity_state_changed) )
	{
		FDTO_Redraw_Screen( (false) );

		// 08/12/2013 ajv : Additionally, post a message to update the GuiVars used to display the
		// next scheduled irrigation on the Status screen. This ensures the Status screen is
		// up-to-date after a schedule runs.
		postBackground_Calculation_Event( BKGRND_CALC_NEXT_SCHEDULED );
	}
	else
	{
		GuiLib_Refresh();
	}
}

/* ---------------------------------------------------------- */
static void FDTO_STATUS_jump_to_first_available_cursor_pos( const BOOL_32 pplay_key_beep )
{
	if( GuiVar_AccessControlEnabled )
	{
		FDTO_Cursor_Select( CP_STATUS_LOGIN, pplay_key_beep );
	}
	else if( GuiVar_StatusNOWDaysInEffect )
	{
		FDTO_Cursor_Select( CP_STATUS_NOW_DAYS_IN_EFFECT, pplay_key_beep );
	}
	else if( GuiVar_StatusElectricalErrors )
	{
		FDTO_Cursor_Select( CP_STATUS_STATION_ELEC_ERRORS, pplay_key_beep );
	}
	else if( GuiVar_StatusFlowErrors )
	{
		FDTO_Cursor_Select( CP_STATUS_STATION_FLOW_ERRORS, pplay_key_beep );
	}
	else if( GuiVar_StatusMVORInEffect )
	{
		FDTO_Cursor_Select( CP_STATUS_MVOR_IN_EFFECT, pplay_key_beep );
	}
	else if( GuiVar_StatusMLBInEffect )
	{
		FDTO_Cursor_Select( CP_STATUS_MLB_DETECTED, pplay_key_beep );
	}
	else
	{
		FDTO_Cursor_Select( (GuiVar_StatusIrrigationActivityState + CP_STATUS_CHAIN_DOWN), pplay_key_beep );
	}
}

/* ---------------------------------------------------------- */
/**
 * Safely calls the FDTO_STATUS_update_screen routine without requiring that the calling 
 * routine be within the context of the DISPLAY_PROCESSING task. 
 * 
 * @task_execution Executed within the context of the KEY PROCESSING task when the user 
 * moves around the Status; within the context of the TD CHECK task on a 1 Hz basis to keep 
 * the screen up-to-date while the user is viewing the screen.
 */
extern void STATUS_update_screen( const BOOL_32 pcomplete_redraw )
{
	DISPLAY_EVENT_STRUCT	lde;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void*)&FDTO_STATUS_update_screen;
	lde._06_u32_argument1 = pcomplete_redraw;
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
/**
 * @task_execution Executed within the context of the DISPLAY PROCESSING task when the 
 * screen is initially drawn. 
 */
extern void FDTO_STATUS_draw_screen( void )
{
	if( GuiVar_StatusShowLiveScreens )
	{
		// 6/24/2015 ajv : If the last cursor position is undefined for some reason, go straight to
		// the first available cursor position.
		if( g_STATUS_last_cursor_position == 0 )
		{
			// 9/23/2015 ajv : This MUST result in an FDTO_Cursor_Select instead of a queued
			// CURSOR_Select. Otherwise, the cursor may get lost because GuiLib_ActiveCursorFieldNo ==
			// CP_MAIN_MENU_STATUS only to be changed as soon as the queued CURSOR_Select is processed.
			FDTO_STATUS_jump_to_first_available_cursor_pos( (false) );
		}
		else
		{
			FDTO_Cursor_Select( g_STATUS_last_cursor_position, (false) );
		}

		g_MAIN_MENU_active_menu_item = CP_MAIN_MENU_NONE;
	}

	// 5/7/2015 ajv : In the event that CURSOR_Select fails on a redraw, the cursor will
	// automatically be set to position 0 (CP_MAIN_MENU_STATUS). However, the UI will still
	// think a cursor position on the Status screen is selected because
	// GuiVar_StatusShowLiveScreens is TRUE. Therefore, if this scenario happens, fix it.
	if( GuiVar_StatusShowLiveScreens && (GuiLib_ActiveCursorFieldNo == CP_MAIN_MENU_STATUS) )
	{
		GuiVar_StatusShowLiveScreens = (false);

		// 9/23/2015 ajv : This should never happen, so let's generate an alert if it does.
		Alert_Message( "CURSOR: status redraw caused cursor reset" );
	}

	if( (GuiLib_ActiveCursorFieldNo == CP_MAIN_MENU_STATUS) || (GuiVar_StatusShowLiveScreens) )
	{
		// 6/5/2015 ajv : Only redraw the STATUS screen if we're actually viewing the Status screen.
		FDTO_STATUS_update_screen();
	}

	if( (GuiLib_ActiveCursorFieldNo == CP_MAIN_MENU_STATUS) )
	{
		// 4/14/2016 skc : Update internal data for status display regarding Budget Reductions.
		DATE_TIME_COMPLETE_STRUCT today;
		EPSON_obtain_latest_complete_time_and_date( &today );
		BUDGET_calculate_ratios( &today, false );
	}
}

/* ---------------------------------------------------------- */
/**
 * @task_execution Executed within the context of the KEY PROCESSING task when the user 
 * presses a key while viewing the Status screen. 
 */
extern void STATUS_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	BOOL_32	display_new_screen;

	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			if( GuiLib_ActiveCursorFieldNo == CP_MAIN_MENU_STATUS )
			{
				GuiVar_StatusShowLiveScreens = (true);

				lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
				lde._04_func_ptr = &FDTO_STATUS_jump_to_first_available_cursor_pos;
				lde._06_u32_argument1 = (true);
				Display_Post_Command( &lde );
			}
			else
			{
				display_new_screen = (false);

				switch( GuiLib_ActiveCursorFieldNo )
				{
					case CP_STATUS_2_WIRE_EXCESS_CURRENT:
					case CP_STATUS_2_WIRE_OVERHEATED:
						bad_key_beep();
						break;

					case CP_STATUS_CHAIN_DOWN:
						display_new_screen = (true);

						lde._03_structure_to_draw = GuiStruct_rptRTCommunications_0;
						lde._04_func_ptr = (void *)&FDTO_REAL_TIME_COMMUNICATIONS_draw_report;
						lde._08_screen_to_draw = CP_LIVE_SCREENS_MENU_COMMUNICATIONS;
						lde.key_process_func_ptr = REAL_TIME_COMMUNICATIONS_process_report;
						break;

					case CP_STATUS_NO_STATIONS_IN_USE:
						display_new_screen = (true);

						lde._03_structure_to_draw = GuiStruct_scrViewStations_0;
						lde._04_func_ptr = (void *)&FDTO_STATIONS_IN_USE_draw_screen;
						lde._08_screen_to_draw = CP_SETUP_MENU_STATIONS_IN_USE;
						lde.key_process_func_ptr = STATIONS_IN_USE_process_screen;
						break;

					case CP_STATUS_FUSE_BLOWN:
					case CP_STATUS_CURRENT:
						display_new_screen = (true);

						lde._03_structure_to_draw = GuiStruct_rptRTElectrical_0;
						lde._04_func_ptr = (void *)&FDTO_REAL_TIME_ELECTRICAL_draw_report;
						lde._08_screen_to_draw = CP_LIVE_SCREENS_MENU_ELECTRICAL;
						lde.key_process_func_ptr = REAL_TIME_ELECTRICAL_process_report;
						break;

					case CP_STATUS_RAIN_SWITCH_ACTIVE:
					case CP_STATUS_FREEZE_SWITCH_ACTIVE:
					case CP_STATUS_ET_GAGE:
					case CP_STATUS_RUNAWAY_GAGE:
					case CP_STATUS_RAIN:
					case CP_STATUS_WIND:
						display_new_screen = (true);

						lde._03_structure_to_draw = GuiStruct_rptRTWeather_0;
						lde._04_func_ptr = (void *)&FDTO_REAL_TIME_WEATHER_draw_report;
						lde._08_screen_to_draw = CP_LIVE_SCREENS_MENU_WEATHER;
						lde.key_process_func_ptr = REAL_TIME_WEATHER_process_report;
						break;

					case CP_STATUS_NO_STATIONS_WATERING:
					case CP_STATUS_IRRIGATION_RUNNING:
						display_new_screen = (true);

						lde._03_structure_to_draw = GuiStruct_rptIrriDetails_0;
						lde._04_func_ptr = (void *)&FDTO_IRRI_DETAILS_draw_report;
						lde._08_screen_to_draw = CP_DIAGNOSTICS_MENU_IRRI_DETAILS;
						lde.key_process_func_ptr = IRRI_DETAILS_process_report;
						break;

					case CP_STATUS_NEXT_SCHEDULED:
						display_new_screen = (true);

						if( NETWORK_CONFIG_get_controller_off() )
						{
							lde._03_structure_to_draw = GuiStruct_scrTurnOff_0;
							lde._04_func_ptr = (void *)&FDTO_TURN_OFF_draw_screen;
							lde._08_screen_to_draw = CP_NOW_MENU_TURN_OFF;
							lde.key_process_func_ptr = TURN_OFF_process_screen;
						}
						else if( GuiVar_StatusTypeOfSchedule == STATUS_SCHEDULE_TYPE_GROUPS_ARE_EMPTY )
						{
							lde._03_structure_to_draw = GuiStruct_scrStationGroups_0;
							lde._04_func_ptr = (void *)&FDTO_STATION_GROUP_draw_menu;
							lde._08_screen_to_draw = CP_SCHEDULE_MENU_STATION_GROUPS;
							lde.key_process_func_ptr = STATION_GROUP_process_menu;
						}
						else if( GuiVar_StatusTypeOfSchedule == STATUS_SCHEDULE_TYPE_MANUAL_PROGRAM )
						{
							lde._03_structure_to_draw = GuiStruct_scrManualPrograms_0;
							lde._04_func_ptr = (void *)&FDTO_MANUAL_PROGRAMS_draw_menu;
							lde._08_screen_to_draw = CP_SCHEDULE_MENU_MANUAL_PROGRAMS;
							lde.key_process_func_ptr = MANUAL_PROGRAMS_process_menu;
						}
						else // if( (GuiVar_StatusTypeOfSchedule == STATUS_SCHEDULE_TYPE_STATION_GROUP) || (GuiVar_StatusTypeOfSchedule == STATUS_SCHEDULE_TYPE_NONE) )
						{
							lde._03_structure_to_draw = GuiStruct_scrStartTimesAndWaterDays_0;
							lde._04_func_ptr = (void *)&FDTO_SCHEDULE_draw_menu;
							lde._08_screen_to_draw = CP_SCHEDULE_MENU_PROGRAMS;
							lde.key_process_func_ptr = SCHEDULE_process_menu;
						}
						break;

					case CP_STATUS_MLB_DETECTED:
					case CP_STATUS_MVOR_IN_EFFECT:
					case CP_STATUS_FLOW_RATE:
						display_new_screen = (true);

						lde._03_structure_to_draw = GuiStruct_rptRTSystemFlow_0;
						lde._04_func_ptr = (void *)&FDTO_REAL_TIME_SYSTEM_FLOW_draw_report;
						lde._08_screen_to_draw = CP_LIVE_SCREENS_MENU_SYSTEM_FLOW;
						lde.key_process_func_ptr = REAL_TIME_SYSTEM_FLOW_process_report;
						break;

					case CP_STATUS_NO_FLOW_METER:
						display_new_screen = (true);

						lde._03_structure_to_draw = GuiStruct_scrPOC_0;
						lde._04_func_ptr = (void *)&FDTO_POC_draw_menu;
						lde._08_screen_to_draw = CP_POC_MENU_POC;
						lde.key_process_func_ptr = POC_process_menu;
						lde.populate_scroll_box_func_ptr = nm_POC_load_group_name_into_guivar;
						break;

					case CP_STATUS_LIGHTS_ON:
						display_new_screen = (true);

						lde._03_structure_to_draw = GuiStruct_rptRTLights_0;
						lde._04_func_ptr = (void *)&FDTO_REAL_TIME_LIGHTS_draw_report;
						lde._08_screen_to_draw = CP_LIVE_SCREENS_MENU_LIGHTS;
						lde.key_process_func_ptr = REAL_TIME_LIGHTS_process_report;
						break;

					case CP_STATUS_BUDGET_IN_EFFECT:
						display_new_screen = (true);

						lde._03_structure_to_draw = GuiStruct_rptBudgets_0;
						lde._04_func_ptr = (void *)&FDTO_BUDGET_REPORT_draw_report;
						lde._08_screen_to_draw = CP_MAIN_MENU_REPORTS;
						lde.key_process_func_ptr = BUDGET_REPORT_process_menu;
						break;

					default:
						bad_key_beep();
				}

				if( display_new_screen )
				{
					GuiVar_StatusShowLiveScreens = (false);

					g_STATUS_last_cursor_position = (UNS_32)GuiLib_ActiveCursorFieldNo;

					ScreenHistory[ screen_history_index ]._02_menu = CP_MAIN_MENU_STATUS;

					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._02_menu = SCREEN_MENU_MAIN;
					lde._06_u32_argument1 = (true);
					Change_Screen( &lde );
				}
			}
			break;
	
		case KEY_C_UP:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_STATUS_BUDGET_IN_EFFECT:
					bad_key_beep();
					break;

				case CP_STATUS_MLB_DETECTED:
					if( GuiVar_LightsAreEnergized )
					{
						CURSOR_Select( CP_STATUS_LIGHTS_ON, (true) );
					}
					else if( GuiVar_BudgetsInEffect )
					{
						CURSOR_Select( CP_STATUS_BUDGET_IN_EFFECT, (true) );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case CP_STATUS_MVOR_IN_EFFECT:
					if( GuiVar_StatusMLBInEffect )
					{
						CURSOR_Select( CP_STATUS_MLB_DETECTED, (true) );
					}
					else if( GuiVar_LightsAreEnergized )
					{
						CURSOR_Select( CP_STATUS_LIGHTS_ON, (true) );
					}
					else if( GuiVar_BudgetsInEffect )
					{
						CURSOR_Select( CP_STATUS_BUDGET_IN_EFFECT, (true) );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case CP_STATUS_STATION_FLOW_ERRORS:
					if( GuiVar_StatusMVORInEffect )
					{
						CURSOR_Select( CP_STATUS_MVOR_IN_EFFECT, (true) );
					}
					else if( GuiVar_StatusMLBInEffect )
					{
						CURSOR_Select( CP_STATUS_MLB_DETECTED, (true) );
					}
					else if( GuiVar_LightsAreEnergized )
					{
						CURSOR_Select( CP_STATUS_LIGHTS_ON, (true) );
					}
					else if( GuiVar_BudgetsInEffect )
					{
						CURSOR_Select( CP_STATUS_BUDGET_IN_EFFECT, (true) );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case CP_STATUS_STATION_ELEC_ERRORS:
					if( GuiVar_StatusFlowErrors )
					{
						CURSOR_Select( CP_STATUS_STATION_FLOW_ERRORS, (true) );
					}
					else if( GuiVar_StatusMVORInEffect )
					{
						CURSOR_Select( CP_STATUS_MVOR_IN_EFFECT, (true) );
					}
					else if( GuiVar_StatusMLBInEffect )
					{
						CURSOR_Select( CP_STATUS_MLB_DETECTED, (true) );
					}
					else if( GuiVar_LightsAreEnergized )
					{
						CURSOR_Select( CP_STATUS_LIGHTS_ON, (true) );
					}
					else if( GuiVar_BudgetsInEffect )
					{
						CURSOR_Select( CP_STATUS_BUDGET_IN_EFFECT, (true) );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case CP_STATUS_NOW_DAYS_IN_EFFECT:
					if( GuiVar_StatusElectricalErrors )
					{
						CURSOR_Select( CP_STATUS_STATION_ELEC_ERRORS, (true) );
					}
					else if( GuiVar_StatusFlowErrors )
					{
						CURSOR_Select( CP_STATUS_STATION_FLOW_ERRORS, (true) );
					}
					else if( GuiVar_StatusMVORInEffect )
					{
						CURSOR_Select( CP_STATUS_MVOR_IN_EFFECT, (true) );
					}
					else if( GuiVar_StatusMLBInEffect )
					{
						CURSOR_Select( CP_STATUS_MLB_DETECTED, (true) );
					}
					else if( GuiVar_LightsAreEnergized )
					{
						CURSOR_Select( CP_STATUS_LIGHTS_ON, (true) );
					}
					else if( GuiVar_BudgetsInEffect )
					{
						CURSOR_Select( CP_STATUS_BUDGET_IN_EFFECT, (true) );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case CP_STATUS_IRRIGATION_RUNNING:
				case CP_STATUS_NO_STATIONS_WATERING:
					if( GuiVar_StatusNOWDaysInEffect )
					{
						CURSOR_Select( CP_STATUS_NOW_DAYS_IN_EFFECT, (true) );
					}
					else if( GuiVar_StatusElectricalErrors )
					{
						CURSOR_Select( CP_STATUS_STATION_ELEC_ERRORS, (true) );
					}
					else if( GuiVar_StatusFlowErrors )
					{
						CURSOR_Select( CP_STATUS_STATION_FLOW_ERRORS, (true) );
					}
					else if( GuiVar_StatusMVORInEffect )
					{
						CURSOR_Select( CP_STATUS_MVOR_IN_EFFECT, (true) );
					}
					else if( GuiVar_StatusMLBInEffect )
					{
						CURSOR_Select( CP_STATUS_MLB_DETECTED, (true) );
					}
					else if( GuiVar_LightsAreEnergized )
					{
						CURSOR_Select( CP_STATUS_LIGHTS_ON, (true) );
					}
					else if( GuiVar_BudgetsInEffect )
					{
						CURSOR_Select( CP_STATUS_BUDGET_IN_EFFECT, (true) );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case CP_STATUS_LIGHTS_ON:
					bad_key_beep();
					break;

				default:
					CURSOR_Up( (true) );
			}
			break;

		case KEY_C_LEFT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_STATUS_BUDGET_IN_EFFECT:
					GuiVar_StatusShowLiveScreens = (false);
					CURSOR_Select( CP_MAIN_MENU_STATUS, (true) );
					break;

				case CP_STATUS_LIGHTS_ON:
				case CP_STATUS_MLB_DETECTED:
					if( GuiVar_BudgetsInEffect )
					{
						CURSOR_Up( (true) );
					}
					else
					{
						GuiVar_StatusShowLiveScreens = (false);
						CURSOR_Select( CP_MAIN_MENU_STATUS, (true) );
					}
					break;

				case CP_STATUS_MVOR_IN_EFFECT:
					if( GuiVar_StatusMLBInEffect ||
						GuiVar_BudgetsInEffect )
					{
						CURSOR_Up( (true) );
					}
					else
					{
						GuiVar_StatusShowLiveScreens = (false);
						CURSOR_Select( CP_MAIN_MENU_STATUS, (true) );
					}
					break;

				case CP_STATUS_STATION_FLOW_ERRORS:
					if( GuiVar_StatusMVORInEffect ||
						GuiVar_StatusMLBInEffect || 
						GuiVar_BudgetsInEffect )
					{
						CURSOR_Up( (true) );
					}
					else
					{
						GuiVar_StatusShowLiveScreens = (false);
						CURSOR_Select( CP_MAIN_MENU_STATUS, (true) );
					}
					break;

				case CP_STATUS_STATION_ELEC_ERRORS:
					if( GuiVar_StatusFlowErrors ||
						GuiVar_StatusMVORInEffect ||
						GuiVar_StatusMLBInEffect || 
						GuiVar_BudgetsInEffect )
					{
						CURSOR_Up( (true) );
					}
					else
					{
						GuiVar_StatusShowLiveScreens = (false);
						CURSOR_Select( CP_MAIN_MENU_STATUS, (true) );
					}
					break;

				case CP_STATUS_NOW_DAYS_IN_EFFECT:
					if( GuiVar_StatusElectricalErrors ||
						GuiVar_StatusFlowErrors ||
						GuiVar_StatusMVORInEffect ||
						GuiVar_StatusMLBInEffect || 
						GuiVar_BudgetsInEffect )
					{
						CURSOR_Up( (true) );
					}
					else
					{
						GuiVar_StatusShowLiveScreens = (false);
						CURSOR_Select( CP_MAIN_MENU_STATUS, (true) );
					}
					break;

				case CP_STATUS_IRRIGATION_RUNNING:
				case CP_STATUS_NO_STATIONS_WATERING:
					if( GuiVar_StatusNOWDaysInEffect ||
						GuiVar_StatusElectricalErrors ||
						GuiVar_StatusFlowErrors ||
						GuiVar_StatusMVORInEffect ||
						GuiVar_StatusMLBInEffect || 
						GuiVar_BudgetsInEffect )
					{
						CURSOR_Up( (true) );
					}
					else
					{
						GuiVar_StatusShowLiveScreens = (false);

						CURSOR_Select( CP_MAIN_MENU_STATUS, (true) );
					}
					break;

				default:
					CURSOR_Up( (true) );
			}
			break;
	
		case KEY_C_DOWN:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_STATUS_NEXT_SCHEDULED:
					if( GuiVar_StatusETGageConnected )
					{
						CURSOR_Select( CP_STATUS_ET_GAGE, (true) );
					}
					else if( GuiVar_ETGageRunawayGage )
					{
						CURSOR_Select( CP_STATUS_RUNAWAY_GAGE, (true) );
					}
					else if( GuiVar_StatusRainBucketConnected )
					{
						CURSOR_Select( CP_STATUS_RAIN, (true) );
					}
					else if( GuiVar_StatusWindGageConnected )
					{
						CURSOR_Select( CP_STATUS_WIND, (true) );
					}
					else if( GuiVar_StatusShowSystemFlow )
					{
						CURSOR_Select( CP_STATUS_FLOW_RATE, (true) );
					}
					else
					{
						CURSOR_Select( CP_STATUS_NO_FLOW_METER, (true) );
					}
					break;

				case CP_STATUS_ET_GAGE:
				case CP_STATUS_RUNAWAY_GAGE:
					if( GuiVar_StatusShowSystemFlow )
					{
						CURSOR_Select( CP_STATUS_FLOW_RATE, (true) );
					}
					else
					{
						CURSOR_Select( CP_STATUS_NO_FLOW_METER, (true) );
					}
					break;

				case CP_STATUS_RAIN:
					if( GuiVar_StatusWindGageConnected )
					{
						CURSOR_Select( CP_STATUS_WIND, (true) );
					}
					else
					{
						CURSOR_Select( CP_STATUS_CURRENT, (true) );
					}
					break;

				case CP_STATUS_WIND:
					CURSOR_Select( CP_STATUS_CURRENT, (true) );
					break;

				case CP_STATUS_FLOW_RATE:
				case CP_STATUS_NO_FLOW_METER:
				case CP_STATUS_CURRENT:
				case CP_STATUS_LOGIN:
					bad_key_beep();
					break;

				case CP_STATUS_LIGHTS_ON:
					// 10/23/2015 ajv : TODO
					GuiLib_ActiveCursorFieldNo = 19;
					CURSOR_Down( (true) );
					break;

				default:
					CURSOR_Down( (true) );
			}
			break;

		case KEY_C_RIGHT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_MAIN_MENU_STATUS:
					GuiVar_StatusShowLiveScreens = (true);

					if( GuiVar_StatusNOWDaysInEffect )
					{
						CURSOR_Select( CP_STATUS_NOW_DAYS_IN_EFFECT, (true) );
					}
					else if( GuiVar_StatusElectricalErrors )
					{
						CURSOR_Select( CP_STATUS_STATION_ELEC_ERRORS, (true) );
					}
					else if( GuiVar_StatusFlowErrors )
					{
						CURSOR_Select( CP_STATUS_STATION_FLOW_ERRORS, (true) );
					}
					else if( GuiVar_StatusMVORInEffect )
					{
						CURSOR_Select( CP_STATUS_MVOR_IN_EFFECT, (true) );
					}
					else if( GuiVar_StatusMLBInEffect )
					{
						CURSOR_Select( CP_STATUS_MLB_DETECTED, (true) );
					}
					else if( GuiVar_BudgetsInEffect )
					{
						CURSOR_Select( CP_STATUS_BUDGET_IN_EFFECT, true );
					}
					else
					{
						CURSOR_Select( (GuiVar_StatusIrrigationActivityState + CP_STATUS_CHAIN_DOWN), (true) );
					}
					break;

				case CP_STATUS_CURRENT:
					CURSOR_Select( CP_STATUS_LOGIN, true );
					break;

				case CP_STATUS_LOGIN:
					bad_key_beep();
					break;

				case CP_STATUS_LIGHTS_ON:
					// 10/23/2015 ajv : TODO
					GuiLib_ActiveCursorFieldNo = 19;
					CURSOR_Down( (true) );
					break;

				default:
					CURSOR_Down( (true) );
			}
			break;
	
		case KEY_BACK:
			GuiVar_StatusShowLiveScreens = (false);

			CURSOR_Select( CP_MAIN_MENU_STATUS, (true) );
			break;
	
		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

