/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_line_fill_times.h"

#include	"app_startup.h"

#include	"cursor_utils.h"

#include	"group_base_file.h"

#include	"screen_utils.h"

#include	"station_groups.h"

#include	"m_main.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_LINE_FILL_TIME_0		(0)
#define	CP_LINE_FILL_TIME_1		(1)
#define	CP_LINE_FILL_TIME_2		(2)
#define	CP_LINE_FILL_TIME_3		(3)
#define	CP_LINE_FILL_TIME_4		(4)
#define	CP_LINE_FILL_TIME_5		(5)
#define	CP_LINE_FILL_TIME_6		(6)
#define	CP_LINE_FILL_TIME_7		(7)
#define	CP_LINE_FILL_TIME_8		(8)
#define	CP_LINE_FILL_TIME_9		(9)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void FDTO_LINE_FILL_TIME_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		LINE_FILL_TIME_copy_group_into_guivars();

		lcursor_to_select = CP_LINE_FILL_TIME_0;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrLineFillTimes_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * @task_execution Executed within the context of the KEY PROCESSING task.
 */
extern void LINE_FILL_TIME_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		case KEY_MINUS:
		case KEY_PLUS:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_LINE_FILL_TIME_0:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_0, LINE_FILL_TIME_MIN, LINE_FILL_TIME_MAX, 1, (false) );
					break;

				case CP_LINE_FILL_TIME_1:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_1, LINE_FILL_TIME_MIN, LINE_FILL_TIME_MAX, 1, (false) );
					break;

				case CP_LINE_FILL_TIME_2:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_2, LINE_FILL_TIME_MIN, LINE_FILL_TIME_MAX, 1, (false) );
					break;

				case CP_LINE_FILL_TIME_3:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_3, LINE_FILL_TIME_MIN, LINE_FILL_TIME_MAX, 1, (false) );
					break;

				case CP_LINE_FILL_TIME_4:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_4, LINE_FILL_TIME_MIN, LINE_FILL_TIME_MAX, 1, (false) );
					break;

				case CP_LINE_FILL_TIME_5:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_5, LINE_FILL_TIME_MIN, LINE_FILL_TIME_MAX, 1, (false) );
					break;

				case CP_LINE_FILL_TIME_6:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_6, LINE_FILL_TIME_MIN, LINE_FILL_TIME_MAX, 1, (false) );
					break;

				case CP_LINE_FILL_TIME_7:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_7, LINE_FILL_TIME_MIN, LINE_FILL_TIME_MAX, 1, (false) );
					break;

				case CP_LINE_FILL_TIME_8:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_8, LINE_FILL_TIME_MIN, LINE_FILL_TIME_MAX, 1, (false) );
					break;

				case CP_LINE_FILL_TIME_9:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_9, LINE_FILL_TIME_MIN, LINE_FILL_TIME_MAX, 1, (false) );
					break;

				default:
					bad_key_beep();
			}
			Refresh_Screen();
			break;

		case KEY_PREV:
		case KEY_C_UP:
		case KEY_C_LEFT:
			CURSOR_Up( (true) );
			break;

		case KEY_NEXT:
		case KEY_C_DOWN:
		case KEY_C_RIGHT:
			CURSOR_Down( (true) );
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = CP_MAIN_MENU_POCS;
			LINE_FILL_TIME_extract_and_store_changes_from_GuiVars();
			// No need to break here to allow this to fall into the default
			// condition.

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

