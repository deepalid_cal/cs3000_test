/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"r_station_history.h"

#include	"app_startup.h"

#include	"configuration_controller.h"

#include	"cs3000_comm_server_common.h"

#include	"d_process.h"

#include	"group_base_file.h"

#include	"station_history_data.h"

#include	"report_utils.h"

#include	"scrollbox.h"

#include	"speaker.h"

#include	"stations.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	STATION_HISTORY_AMOUNT_TO_SCROLL_HORIZONTALLY		(20)

#define STATION_HISTORY_MAX_PIXELS_TO_SCROLL_HORIZONTALLY	(240)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Track the previous number of station report data lines. If the number of lines
// changes between refreshes, the entire scroll box must be redrawn. Otherwise,
// we can simply update the variables displayed on the screen.
static UNS_32 g_STATION_HISTORY_line_count;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void FDTO_STATION_HISTORY_load_station_name_to_guivar( void )
{
	const STATION_STRUCT	*lstation_ptr;

	char str_16[ 16 ];

	char *lname;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation_ptr = nm_STATION_get_pointer_to_station( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber-1) );

	lname = nm_GROUP_get_name( lstation_ptr );

	if( lname != NULL )
	{
		snprintf( GuiVar_StationDescription, sizeof(GuiVar_StationDescription), "%s %c %s: %s", GuiLib_GetTextPtr( GuiStruct_txtSta_0, 0 ), GuiVar_StationInfoBoxIndex + 'A', STATION_get_station_number_string( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1), (char*)&str_16, sizeof(str_16) ), nm_GROUP_get_name( lstation_ptr ) );
	}
	else
	{
		GuiVar_StationDescription[ 0 ] = 0;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Redraws the scroll box on the Station History report.
 *
 * @executed Executed within the context of the display processing task when the
 *  		 user changes stations.
 * 
 * @author 5/15/2012 Adrianusv
 *
 * @revisions
 *    5/15/2012 Initial release
 */
extern void FDTO_STATION_HISTORY_redraw_scrollbox( void )
{
	UNS_32	lcurrent_line_count;

	FDTO_STATION_HISTORY_load_station_name_to_guivar();

	lcurrent_line_count = STATION_HISTORY_fill_ptrs_and_return_how_many_lines( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber-1) );

	// If the number of records have changed, redraw the entire scroll box. Otherwise, just
	// update the contents of what's currently on the screen.
	FDTO_SCROLL_BOX_redraw_retaining_topline( 0, lcurrent_line_count, (lcurrent_line_count != g_STATION_HISTORY_line_count) );

	g_STATION_HISTORY_line_count = lcurrent_line_count;
}

/* ---------------------------------------------------------- */
/**
 * Draws the Station History report screen.
 * 
 * @executed Executed within the context of the display processing task when the
 *  		 screen is initially drawn.
 * 
 * @author 5/15/2012 Adrianusv
 *
 * @revisions
 *    5/15/2012 Initial release
 */
extern void FDTO_STATION_HISTORY_draw_report( const BOOL_32 pcomplete_redraw )
{
	GuiLib_ShowScreen( GuiStruct_rptStationHistory_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );

	// 7/27/2012 rmd : The station GET functions require possesion of this MUTEX.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );
	
	// If we haven't looked at a station yet, grab the first station. Otherwise,
	// use retain whatever station was last being viewed.
	if( (GuiVar_StationInfoBoxIndex == 0) && (GuiVar_StationInfoNumber == 0) )
	{
		STATION_STRUCT	*lstation;

		lstation = STATION_get_first_available_station();

		GuiVar_StationInfoBoxIndex = nm_STATION_get_box_index_0( lstation );

		GuiVar_StationInfoNumber = nm_STATION_get_station_number_0( lstation ) + 1;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// --------------------------
	
	g_STATION_HISTORY_line_count = STATION_HISTORY_fill_ptrs_and_return_how_many_lines( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber-1) );

	// --------------------------
	
	FDTO_REPORTS_draw_report( pcomplete_redraw, g_STATION_HISTORY_line_count, &STATION_HISTORY_draw_scroll_line );
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while on the Station History report screen.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 *
 * @param pkey_event The key event to be processed.
 *
 * @author 5/15/2012 Adrianusv
 *
 * @revisions
 *    5/15/2012 Initial release
 */
extern void STATION_HISTORY_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( pkey_event.keycode )
	{
		case KEY_NEXT:
		case KEY_PREV:
			good_key_beep();

			if( pkey_event.keycode == KEY_NEXT )
			{
				STATION_get_next_available_station( &GuiVar_StationInfoBoxIndex, &GuiVar_StationInfoNumber );
			}
			else
			{
				STATION_get_prev_available_station( &GuiVar_StationInfoBoxIndex, &GuiVar_StationInfoNumber );
			}

			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = (void*)&FDTO_STATION_HISTORY_redraw_scrollbox;
			Display_Post_Command( &lde );
			break;

		default:
			REPORTS_process_report( pkey_event, STATION_HISTORY_AMOUNT_TO_SCROLL_HORIZONTALLY, STATION_HISTORY_MAX_PIXELS_TO_SCROLL_HORIZONTALLY );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

