/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_E_LIGHTS_TEST_H
#define _INC_E_LIGHTS_TEST_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FDTO_LIGHTS_TEST_update_screen( void );

extern void FDTO_LIGHTS_TEST_draw_screen( const BOOL_32 pcomplete_redraw );

extern void LIGHTS_TEST_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

