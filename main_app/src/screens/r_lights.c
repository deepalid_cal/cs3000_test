
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"r_lights.h"

#include	"e_lights.h"

#include	"app_startup.h"

#include	"configuration_controller.h"

#include	"cs3000_comm_server_common.h"

#include	"d_process.h"

#include	"group_base_file.h"

#include	"report_data.h"

#include	"lights_report_data.h"

#include	"report_utils.h"

#include	"scrollbox.h"

#include	"speaker.h"

#include	"lights.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Track the previous number of light report data lines. If the number of lines
// changes between refreshes, the entire scroll box must be redrawn. Otherwise,
// we can simply update the variables displayed on the screen.
static UNS_32 g_LIGHTS_REPORT_line_count;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Redraws the scroll box on the Station History report.
 *
 * @executed Executed within the context of the display processing task when the
 *  		 user changes lights.
 * 
 * @author 5/15/2012 Adrianusv
 *
 * @revisions
 *    5/15/2012 Initial release
 */
extern void FDTO_LIGHTS_REPORT_redraw_scrollbox( void )
{
	UNS_32	lcurrent_line_count;

	// The light GET functions require possesion of this MUTEX.
	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	// Refresh the GuiVar strings. 
	LIGHTS_copy_light_struct_into_guivars( LIGHTS_get_lights_array_index( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 ) );

	// 8/31/2015 mpd : This screen  does not allow the user to edit the name. Display the long format ("Light 1 @ A:
	// Left Field").
	LIGHTS_populate_group_name( LIGHTS_get_lights_array_index( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 ), ( true ) );

	lcurrent_line_count = LIGHTS_REPORT_DATA_fill_ptrs_and_return_how_many_lines( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 );

	// If the number of records have changed, redraw the entire scroll box. Otherwise, just
	// update the contents of what's currently on the screen.
	FDTO_SCROLL_BOX_redraw_retaining_topline( 0, lcurrent_line_count, (lcurrent_line_count != g_LIGHTS_REPORT_line_count) );

	g_LIGHTS_REPORT_line_count = lcurrent_line_count;

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
/**
 * Draws the Station History report screen.
 * 
 * @executed Executed within the context of the display processing task when the
 *  		 screen is initially drawn.
 * 
 * @author 5/15/2012 Adrianusv
 *
 * @revisions
 *    5/15/2012 Initial release
 */
extern void FDTO_LIGHTS_REPORT_draw_report( const BOOL_32 pcomplete_redraw )
{
	GuiLib_ShowScreen( GuiStruct_rptLightsSummary_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );

	// The light GET functions require possesion of this MUTEX.
	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );
	
	if( pcomplete_redraw )
	{
		nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars();
	}

	// Refresh the GuiVar strings. 
	LIGHTS_copy_light_struct_into_guivars( LIGHTS_get_lights_array_index( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 ) );

	// 8/31/2015 mpd : This screen  does not allow the user to edit the name. Display the long format ("Light 1 @ A:
	// Left Field").
	LIGHTS_populate_group_name( LIGHTS_get_lights_array_index( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 ), ( true ) );

	// --------------------------
	
	g_LIGHTS_REPORT_line_count = LIGHTS_REPORT_DATA_fill_ptrs_and_return_how_many_lines( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 );

	// --------------------------
	
	FDTO_REPORTS_draw_report( pcomplete_redraw, g_LIGHTS_REPORT_line_count, &LIGHTS_REPORT_draw_scroll_line );

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while on the Station History report screen.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 *
 * @param pkey_event The key event to be processed.
 *
 * @author 5/15/2012 Adrianusv
 *
 * @revisions
 *    5/15/2012 Initial release
 */
extern void LIGHTS_REPORT_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( pkey_event.keycode )
	{
		case KEY_NEXT:
		case KEY_PREV:
			good_key_beep();

			xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

			if( pkey_event.keycode == KEY_NEXT )
			{
				nm_LIGHTS_get_next_available_light( &GuiVar_LightsBoxIndex_0, &GuiVar_LightsOutputIndex_0 );
			}
			else
			{
				nm_LIGHTS_get_prev_available_light( &GuiVar_LightsBoxIndex_0, &GuiVar_LightsOutputIndex_0 );
			}

			xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = (void*)&FDTO_LIGHTS_REPORT_redraw_scrollbox;
			Display_Post_Command( &lde );
			break;

		default:
			// Since we don't allow horizontal scrolling on this report, pass
			// 0's for the two parameters which controller the horizontal
			// scroll.
			REPORTS_process_report( pkey_event, 0, 0 );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

