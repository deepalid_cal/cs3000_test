/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"r_system_preserves.h"

#include	"battery_backed_vars.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"m_main.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32 g_SYSTEM_PRESERVES_index_of_system_to_show;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Updates the variables used to display the System Preserves and forces a
 * screen redraw.
 * 
 * @executed Executed within the context of the DISPLAY PROCESSING task when the
 *  		 screen is initially drawn; within the context of the TD CHECK task
 *  		 as the screen is updated 4 times per second.
 *
 * @author 4/5/2012 Adrianusv
 *
 * @revisions
 *    4/5/2012   Initial release
 */
static void FDTO_SYSTEM_PRESERVES_redraw_report( void )
{
	char	str_64[ 64 ];

	UNS_32	lraw_count;

	UNS_32	lcol;

	UNS_32	lrow;

	lcol = 0;

	lrow = 0;

	for( lraw_count = 0; lraw_count < SYSTEM_5_SECOND_AVERAGES_KEPT; ++lraw_count )
	{
		snprintf( str_64, sizeof(str_64), "%7.2f", (double)system_preserves.system[ g_SYSTEM_PRESERVES_index_of_system_to_show ].system_master_5_second_averages_ring[ lraw_count ] );

		GuiLib_DrawStr( (INT_16)(150 + (lcol * 53)), (INT_16)(57 + (12 * lrow)), GuiFont_ANSI7, -1, str_64, GuiLib_ALIGN_RIGHT, GuiLib_PS_NUM, GuiLib_TRANSPARENT_OFF, GuiLib_UNDERLINE_OFF, 0, 0, 0, 0, GuiConst_PIXEL_ON, GuiConst_PIXEL_OFF );

		if( ++lcol == 4 )
		{
			lcol = 0;
			++lrow;
		}
	}

	lcol = 0;

	lrow = 0;

	for( lraw_count = 0; lraw_count < SYSTEM_STABILITY_AVERAGES_KEPT; ++lraw_count )
	{
		snprintf( str_64, sizeof(str_64), "%7.2f", (double)system_preserves.system[ g_SYSTEM_PRESERVES_index_of_system_to_show ].system_stability_averages_ring[ lraw_count ] );

		GuiLib_DrawStr( (INT_16)(150 + (lcol * 53)), (INT_16)(138 + (12 * lrow)), GuiFont_ANSI7, -1, str_64, GuiLib_ALIGN_RIGHT, GuiLib_PS_NUM, GuiLib_TRANSPARENT_OFF, GuiLib_UNDERLINE_OFF, 0, 0, 0, 0, GuiConst_PIXEL_ON, GuiConst_PIXEL_OFF );

		if( ++lcol == 4 )
		{
			lcol = 0;
			++lrow;
		}
	}

	GuiVar_SystemPreserves5SecMostRecent = system_preserves.system[ g_SYSTEM_PRESERVES_index_of_system_to_show ].system_master_most_recent_5_second_average;
	GuiVar_SystemPreserves5SecNext = system_preserves.system[ g_SYSTEM_PRESERVES_index_of_system_to_show ].system_master_5_sec_avgs_next_index;

	GuiVar_SystemPreservesStabilityMostRecent5Sec = system_preserves.system[ g_SYSTEM_PRESERVES_index_of_system_to_show ].system_rcvd_most_recent_token_5_second_average;
	GuiVar_SystemPreservesStabilityNext = system_preserves.system[ g_SYSTEM_PRESERVES_index_of_system_to_show ].stability_avgs_index_of_last_computed;

	if( g_SYSTEM_PRESERVES_index_of_system_to_show < SYSTEM_num_systems_in_use() )
	{
		FDTO_SYSTEM_load_system_name_into_guivar( g_SYSTEM_PRESERVES_index_of_system_to_show );
	}
	else
	{
		snprintf( GuiVar_GroupName, sizeof(GuiVar_GroupName), "No System %d in file system", (g_SYSTEM_PRESERVES_index_of_system_to_show + 1) );
	}

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Draws the System Preserves report.
 * 
 * @executed Executed within the context of the DISPLAY PROCESSING task when the
 *  		 screen is initially drawn; within the context of the TD CHECK task
 *  		 as the screen is updated 4 times per second.
 * 
 * @param pcomplete_redraw True if the screen is to be redrawn completely
 *  				        (which should only happen on initial entry into the
 *                          screen); otherwise, false.
 *
 * @author 4/5/2012 Adrianusv
 *
 * @revisions
 *    4/5/2012   Initial release
 */
extern void FDTO_SYSTEM_PRESERVES_draw_report( const BOOL_32 pcomplete_redraw )
{
	if( pcomplete_redraw == (true) )
	{
		GuiLib_ShowScreen( GuiStruct_rptSystemPreserves_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );

		g_SYSTEM_PRESERVES_index_of_system_to_show = 0;
	}

    FDTO_SYSTEM_PRESERVES_redraw_report();
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while on the System Preserves report.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 *
 * @param pkey_event The key event to be processed.
 *
 * @author 4/5/2012 Adrianusv
 *
 * @revisions
 *    4/5/2012   Initial release
 */
extern void SYSTEM_PRESERVES_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	UNS_32	lkey;

	switch( pkey_event.keycode )
	{
		case KEY_PLUS:
		case KEY_MINUS:
		case KEY_NEXT:
		case KEY_PREV:
			if( pkey_event.keycode == KEY_NEXT )
			{
				lkey = KEY_PLUS;
			}
			else if( pkey_event.keycode == KEY_PREV )
			{
				lkey = KEY_MINUS;
			}
			else
			{
				lkey = pkey_event.keycode;
			}

			process_uns32( lkey, &g_SYSTEM_PRESERVES_index_of_system_to_show, 0, (MAX_POSSIBLE_SYSTEMS-1), 1, (true) );

			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = (void*)&FDTO_SYSTEM_PRESERVES_redraw_report;
			Display_Post_Command( &lde );
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;

			KEY_process_global_keys( pkey_event );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

