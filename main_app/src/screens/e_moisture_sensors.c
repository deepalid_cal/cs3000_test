/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_moisture_sensors.h"

// 4/4/2016 ajv : Required for memcpy and memset
#include	<string.h>

#include	"alerts.h"

#include	"app_startup.h"

#include	"change.h"

#include	"combobox.h"

#include	"configuration_controller.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"d_two_wire_assignment.h"

#include	"dialog.h"

#include	"e_keyboard.h"

#include	"e_numeric_keypad.h"

#include	"e_station_groups.h"

#include	"group_base_file.h"

#include	"m_main.h"

#include	"moisture_sensors.h"

#include	"scrollbox.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_MOISTURE_SENSOR_NAME				(0)
#define	CP_MOISTURE_SENSOR_ASSIGN_BUTTON	(1)
#define	CP_MOISTURE_SENSOR_TAB_MOISTURE		(3)
#define	CP_MOISTURE_SENSOR_TAB_TEMPERATURE	(4)
#define	CP_MOISTURE_SENSOR_TAB_EC			(5)

#define	CP_MOISTURE_SENSOR_CONTROL_MODE		(30)
#define	CP_MOISTURE_SENSOR_SET_POINT_HIGH	(31)
#define	CP_MOISTURE_SENSOR_SET_POINT_LOW	(32)

#define	CP_MOISTURE_SENSOR_TEMP_ACTION_HIGH	(40)
#define	CP_MOISTURE_SENSOR_TEMP_POINT_HIGH	(41)
#define	CP_MOISTURE_SENSOR_TEMP_ACTION_LOW	(42)
#define	CP_MOISTURE_SENSOR_TEMP_POINT_LOW	(43)

#define	CP_MOISTURE_SENSOR_EC_ACTION_HIGH	(50)
#define	CP_MOISTURE_SENSOR_EC_POINT_HIGH	(51)
#define	CP_MOISTURE_SENSOR_EC_ACTION_LOW	(52)
#define	CP_MOISTURE_SENSOR_EC_POINT_LOW		(53)

/* ---------------------------------------------------------- */

#define	MOISTURE_SENSOR_TAB_MOISTURE		(0)
#define	MOISTURE_SENSOR_TAB_TEMPERATURE		(1)
#define	MOISTURE_SENSOR_TAB_EC				(2)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32	g_MOISTURE_SENSOR_top_line;

static BOOL_32	g_MOISTURE_SENSOR_editing_group;

// 4/4/2016 ajv : Since the currently selected item in the list does not necessarily
// correspond to g_GROUP_list_item_index any more (since the latter corresponds to the
// Moisture Sensor list while the current item corresponds to the MOISTURE_SENSOR_MENU_items
// array), we need to keep track of which cursor item we're on as it relates to the
// POC_MENU_items array.
static UNS_32 g_MOISTURE_SENSOR_current_list_item_index;

// ----------

typedef struct
{
	void		*sensor_ptr;

} MOISTURE_SENSOR_MENU_SCROLL_BOX_ITEM;

// 4/4/2016 ajv : TODO Come up with a reasonable limit for this size of this structure
#define	MAX_MOISTURE_SENSORS_IN_NETWORK		(64)

static MOISTURE_SENSOR_MENU_SCROLL_BOX_ITEM MOISTURE_SENSOR_menu_items[ MAX_MOISTURE_SENSORS_IN_NETWORK ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void FDTO_MOISTURE_SENSOR_return_to_menu( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static UNS_32 MOISTURE_SENSOR_get_menu_index_for_displayed_sensor( void )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lsensor;

	UNS_32	rv;

	UNS_32	i;
	
	// ----------

	rv = 0;

	// ----------

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	lsensor = MOISTURE_SENSOR_get_ptr_to_physically_available_sensor(g_MOISTURE_SENSOR_current_list_item_index);

	for( i = 0; i < MOISTURE_SENSOR_get_list_count(); ++i )
	{
		if( MOISTURE_SENSOR_menu_items[ i ].sensor_ptr == lsensor )
		{
			rv = i;
			
			break;
		}
	}

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static void MOISTURE_SENSOR_process_NEXT_and_PREV( const UNS_32 pkeycode )
{
	UNS_32	lmenu_index_for_displayed_sensor;

	// ----------

	lmenu_index_for_displayed_sensor = MOISTURE_SENSOR_get_menu_index_for_displayed_sensor();
	
	// ----------

	if( (pkeycode == KEY_NEXT) || (pkeycode == KEY_PLUS) )
	{
		// 4/4/2016 ajv : Turn off the automatic redraw functions within the SCROLL_BOX_to_line and
		// SCROLL_BOX_up_or_down routines to ensure the scroll bar does not appear.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_DISABLED );

		SCROLL_BOX_to_line( 0, (INT_16)lmenu_index_for_displayed_sensor );

		// 4/4/2016 ajv : Don't allow lmenu_index_for_displayed_poc to exceed the max size of the
		// array
		if( lmenu_index_for_displayed_sensor < MAX_MOISTURE_SENSORS_IN_NETWORK )
		{
			++lmenu_index_for_displayed_sensor;

			// 7/14/2015 ajv : Attempt to move to the next poc.
			if( MOISTURE_SENSOR_menu_items[ lmenu_index_for_displayed_sensor ].sensor_ptr != NULL )
			{
				SCROLL_BOX_up_or_down( 0, KEY_C_DOWN );
			}
		}
		else
		{
			bad_key_beep();
		}

		// 7/9/2015 ajv : Now that we've moved the scroll bar without displaying it, reenable the
		// automatic redraw functions to ensure other calls are not affected.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_ENABLED );
	}
	else
	{
		// 7/9/2015 ajv : Turn off the automatic redraw functions within the SCROLL_BOX_to_line and
		// SCROLL_BOX_up_or_down routines to ensure the scroll bar does not appear.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_DISABLED );

		SCROLL_BOX_to_line( 0, (INT_16)lmenu_index_for_displayed_sensor );

		// 7/13/2015 ajv : Don't allow lmenu_index_for_displayed_poc to go below 0.
		if( lmenu_index_for_displayed_sensor > 0 )
		{
			--lmenu_index_for_displayed_sensor;

			// 7/14/2015 ajv : Attempt to move to the previous poc.
			if( MOISTURE_SENSOR_menu_items[ lmenu_index_for_displayed_sensor ].sensor_ptr != NULL )
			{
				SCROLL_BOX_up_or_down( 0, KEY_C_UP );
			}
		}
		else
		{
			bad_key_beep();
		}

		// 10/24/2013 ajv : Now that we've moved the scroll bar without displaying it, reenable the
		// automatic redraw functions to ensure other calls are not affected.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_ENABLED );
	}

	// ----------

	// 7/9/2015 ajv : If we've actually moved to another POC, save the current POC's information
	// and repopulate the GuiVars with the next POC's.
	if( MOISTURE_SENSOR_menu_items[ lmenu_index_for_displayed_sensor ].sensor_ptr != NULL )
	{
		MOISTURE_SENSOR_extract_and_store_changes_from_GuiVars();

		g_GROUP_list_item_index = MOISTURE_SENSOR_get_index_using_ptr_to_mois_struct( MOISTURE_SENSOR_menu_items[ lmenu_index_for_displayed_sensor ].sensor_ptr );

		xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

		MOISTURE_SENSOR_copy_group_into_guivars( g_GROUP_list_item_index );

		xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
	}
	else
	{
		bad_key_beep();
	}

	// 4/4/2016 ajv : Although nothing may have changed if we encountered the top or bottom of
	// the list, we need to force a redraw to ensure the scroll line of the currently selected
	// station is suppressed. Without this call, the SCROLL_BOX_to_line call drew the scroll
	// line and as soon as the next call to GuiLib_Refresh happens (such as once per second when
	// the date is updated), the scroll line will appear.
	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
/**
 * Processes the key event from the Moisture Sensor screen for a particular sensor.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 4/1/2016 Adrianusv
 */
static void MOISTURE_SENSOR_process_group( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	// ----------

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_dlgKeyboard_0:
			if( ((pkey_event.keycode == KEY_BACK) || ((pkey_event.keycode == KEY_SELECT) && (GuiLib_ActiveCursorFieldNo == 49 /*CP_QWERTY_KEYBOARD_OK*/))) )
			{
				MOISTURE_SENSOR_extract_and_store_group_name_from_GuiVars();
			}

			KEYBOARD_process_key( pkey_event, &FDTO_MOISTURE_SENSOR_draw_menu );
			break;

		case GuiStruct_dlgNumericKeypad_0:
			NUMERIC_KEYPAD_process_key( pkey_event, &FDTO_MOISTURE_SENSOR_draw_menu );
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_MOISTURE_SENSOR_NAME:
							GROUP_process_show_keyboard( 163, 27 );
							break;

						case CP_MOISTURE_SENSOR_ASSIGN_BUTTON:
							lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
							lde._02_menu = SCREEN_MENU_SCHEDULE;
							lde._03_structure_to_draw = GuiStruct_scrStationGroups_0;
							lde._04_func_ptr = (void *)&FDTO_STATION_GROUP_draw_menu;
							lde._06_u32_argument1 = (true);
							lde._08_screen_to_draw = CP_SCHEDULE_MENU_STATION_GROUPS;
							lde.key_process_func_ptr = STATION_GROUP_process_menu;
							lde.populate_scroll_box_func_ptr = nm_STATION_GROUP_load_group_name_into_guivar;

							Change_Screen( &lde );
							break;

						case CP_MOISTURE_SENSOR_SET_POINT_LOW:
							good_key_beep();

							NUMERIC_KEYPAD_draw_float_keypad( &GuiVar_MoisSetPoint_Low, MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_MIN, MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_MAX, 2 );
							break;

						case CP_MOISTURE_SENSOR_SET_POINT_HIGH:
							good_key_beep();

							NUMERIC_KEYPAD_draw_float_keypad( &GuiVar_MoisSetPoint_High, MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_MIN, MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_MAX, 2 );
							break;

						case CP_MOISTURE_SENSOR_TEMP_POINT_HIGH:
							good_key_beep();

							NUMERIC_KEYPAD_draw_int32_keypad( &GuiVar_MoisTempPoint_High, MOISTURE_SENSOR_HIGH_TEMP_POINT_MIN, MOISTURE_SENSOR_HIGH_TEMP_POINT_MAX );
							break;

						case CP_MOISTURE_SENSOR_TEMP_POINT_LOW:
							good_key_beep();

							NUMERIC_KEYPAD_draw_int32_keypad( &GuiVar_MoisTempPoint_Low, MOISTURE_SENSOR_LOW_TEMP_POINT_MIN, MOISTURE_SENSOR_LOW_TEMP_POINT_MAX );
							break;

						case CP_MOISTURE_SENSOR_EC_POINT_HIGH:
							good_key_beep();

							NUMERIC_KEYPAD_draw_float_keypad( &GuiVar_MoisECPoint_High, MOISTURE_SENSOR_HIGH_EC_POINT_MIN, MOISTURE_SENSOR_HIGH_EC_POINT_MAX, 2 );
							break;

						case CP_MOISTURE_SENSOR_EC_POINT_LOW:
							good_key_beep();

							NUMERIC_KEYPAD_draw_float_keypad( &GuiVar_MoisECPoint_Low, MOISTURE_SENSOR_LOW_EC_POINT_MIN, MOISTURE_SENSOR_LOW_EC_POINT_MAX, 2 );
							break;

						default:
							bad_key_beep();
					}
					Refresh_Screen();
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						// 10/15/2018 ajv : For now, we're blocking this value from being edited so force it to the 
						// default mode. 
						case CP_MOISTURE_SENSOR_CONTROL_MODE:
							process_uns32( pkey_event.keycode, &GuiVar_MoisControlMode, MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_stop_at_high_threshold, MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_stop_at_high_threshold, 1, (true) );

							// 10/15/2018 ajv : And force a redraw to ensure the fields display/hide properly.
							Redraw_Screen( (false) );
							break;

						case CP_MOISTURE_SENSOR_SET_POINT_LOW:
							process_fl( pkey_event.keycode, &GuiVar_MoisSetPoint_Low, MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_MIN, MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_MAX, 0.01f, (false) );
							break;

						case CP_MOISTURE_SENSOR_SET_POINT_HIGH:
							process_fl( pkey_event.keycode, &GuiVar_MoisSetPoint_High, MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_MIN, MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_MAX, 0.01f, (false) );
							break;

						case CP_MOISTURE_SENSOR_TEMP_ACTION_HIGH:
							process_uns32( pkey_event.keycode, &GuiVar_MoisTempAction_High, MOISTURE_SENSOR_HIGH_TEMP_ACTION_MIN, MOISTURE_SENSOR_HIGH_TEMP_ACTION_MAX, 1, (true) ); 
							break;

						case CP_MOISTURE_SENSOR_TEMP_POINT_HIGH:
							process_int32( pkey_event.keycode, &GuiVar_MoisTempPoint_High, MOISTURE_SENSOR_HIGH_TEMP_POINT_MIN, MOISTURE_SENSOR_HIGH_TEMP_POINT_MAX, 1, (false) ); 
							break;

						case CP_MOISTURE_SENSOR_TEMP_ACTION_LOW:
							process_uns32( pkey_event.keycode, &GuiVar_MoisTempAction_Low, MOISTURE_SENSOR_LOW_TEMP_ACTION_MIN, MOISTURE_SENSOR_LOW_TEMP_ACTION_MAX, 1, (true) ); 
							break;

						case CP_MOISTURE_SENSOR_TEMP_POINT_LOW:
							process_int32( pkey_event.keycode, &GuiVar_MoisTempPoint_Low, MOISTURE_SENSOR_LOW_TEMP_POINT_MIN, MOISTURE_SENSOR_LOW_TEMP_POINT_MAX, 1, (false) ); 
							break;

						case CP_MOISTURE_SENSOR_EC_ACTION_HIGH:
							process_uns32( pkey_event.keycode, &GuiVar_MoisECAction_High, MOISTURE_SENSOR_HIGH_EC_ACTION_MIN, MOISTURE_SENSOR_HIGH_EC_ACTION_MAX, 1, (true) ); 
							break;

						case CP_MOISTURE_SENSOR_EC_POINT_HIGH:
							process_fl( pkey_event.keycode, &GuiVar_MoisECPoint_High, MOISTURE_SENSOR_HIGH_EC_POINT_MIN, MOISTURE_SENSOR_HIGH_EC_POINT_MAX, 0.01f, (false) ); 
							break;

						case CP_MOISTURE_SENSOR_EC_ACTION_LOW:
							process_uns32( pkey_event.keycode, &GuiVar_MoisECAction_Low, MOISTURE_SENSOR_LOW_EC_ACTION_MIN, MOISTURE_SENSOR_LOW_EC_ACTION_MAX, 1, (true) ); 
							break;

						case CP_MOISTURE_SENSOR_EC_POINT_LOW:
							process_fl( pkey_event.keycode, &GuiVar_MoisECPoint_Low, MOISTURE_SENSOR_LOW_EC_POINT_MIN, MOISTURE_SENSOR_LOW_EC_POINT_MAX, 0.01f, (false) );
							break;

						default:
							bad_key_beep();
					}
					Refresh_Screen();
					break;

				case KEY_NEXT:
				case KEY_PREV: 
					MOISTURE_SENSOR_process_NEXT_and_PREV( pkey_event.keycode );
					break;

				case KEY_C_UP:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_MOISTURE_SENSOR_TAB_MOISTURE:
						case CP_MOISTURE_SENSOR_TAB_TEMPERATURE:
						case CP_MOISTURE_SENSOR_TAB_EC:
							CURSOR_Select( CP_MOISTURE_SENSOR_ASSIGN_BUTTON, (true) );
							break;

						case CP_MOISTURE_SENSOR_CONTROL_MODE:
							CURSOR_Select( CP_MOISTURE_SENSOR_TAB_MOISTURE, (true) );
							break;

						case CP_MOISTURE_SENSOR_TEMP_ACTION_HIGH:
							CURSOR_Select( CP_MOISTURE_SENSOR_TAB_TEMPERATURE, (true) );
							break;

						case CP_MOISTURE_SENSOR_EC_ACTION_HIGH:
							CURSOR_Select( CP_MOISTURE_SENSOR_TAB_EC, (true) );
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_DOWN:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_MOISTURE_SENSOR_ASSIGN_BUTTON:
							if( GuiVar_MoistureTabToDisplay == MOISTURE_SENSOR_TAB_MOISTURE )
							{
								CURSOR_Select( CP_MOISTURE_SENSOR_TAB_MOISTURE, (true) );
							}
							else if( GuiVar_MoistureTabToDisplay == MOISTURE_SENSOR_TAB_TEMPERATURE )
							{
								CURSOR_Select( CP_MOISTURE_SENSOR_TAB_TEMPERATURE, (true) );
							}
							else if( GuiVar_MoistureTabToDisplay == MOISTURE_SENSOR_TAB_EC )
							{
								CURSOR_Select( CP_MOISTURE_SENSOR_TAB_EC, (true) );
							}
							break;

						case CP_MOISTURE_SENSOR_TAB_MOISTURE:
							CURSOR_Select( CP_MOISTURE_SENSOR_CONTROL_MODE, (true) );
							break;

						case CP_MOISTURE_SENSOR_TAB_TEMPERATURE:
							CURSOR_Select( CP_MOISTURE_SENSOR_TEMP_ACTION_HIGH, (true) );
							break;

						case CP_MOISTURE_SENSOR_TAB_EC:
							CURSOR_Select( CP_MOISTURE_SENSOR_EC_ACTION_HIGH, (true) );
							break;

						default:
							CURSOR_Down( (true) );
					}
					break;

				case KEY_C_LEFT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_MOISTURE_SENSOR_NAME:
							KEY_process_BACK_from_editing_screen( (void*)&FDTO_MOISTURE_SENSOR_return_to_menu );
							break;

						case CP_MOISTURE_SENSOR_TAB_TEMPERATURE:
							CURSOR_Select( CP_MOISTURE_SENSOR_TAB_MOISTURE, (true) );

							GuiVar_MoistureTabToDisplay = MOISTURE_SENSOR_TAB_MOISTURE;

							// 10/15/2018 ajv : Since we're moving to a different tab, we need to perform a redraw to 
							// ensure the tab's fields display appropriately. 
							Redraw_Screen( (false) );
							break;

						case CP_MOISTURE_SENSOR_TAB_EC:
							CURSOR_Select( CP_MOISTURE_SENSOR_TAB_TEMPERATURE, (true) );

							GuiVar_MoistureTabToDisplay = MOISTURE_SENSOR_TAB_TEMPERATURE;

							// 10/15/2018 ajv : Since we're moving to a different tab, we need to perform a redraw to 
							// ensure the tab's fields display appropriately. 
							Redraw_Screen( (false) );
							break;

						case CP_MOISTURE_SENSOR_CONTROL_MODE:
							CURSOR_Select( CP_MOISTURE_SENSOR_TAB_MOISTURE, (true) );
							break;

						case CP_MOISTURE_SENSOR_TEMP_ACTION_HIGH:
							CURSOR_Select( CP_MOISTURE_SENSOR_TAB_TEMPERATURE, (true) );
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_RIGHT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_MOISTURE_SENSOR_TAB_MOISTURE:
							CURSOR_Select( CP_MOISTURE_SENSOR_TAB_TEMPERATURE, (true) );

							GuiVar_MoistureTabToDisplay = MOISTURE_SENSOR_TAB_TEMPERATURE;

							// 10/15/2018 ajv : Since we're moving to a different tab, we need to perform a redraw to 
							// ensure the tab's fields display appropriately. 
							Redraw_Screen( (false) );
							break;

						case CP_MOISTURE_SENSOR_TAB_TEMPERATURE:
							CURSOR_Select( CP_MOISTURE_SENSOR_TAB_EC, (true) );

							GuiVar_MoistureTabToDisplay = MOISTURE_SENSOR_TAB_EC;

							// 10/15/2018 ajv : Since we're moving to a different tab, we need to perform a redraw to 
							// ensure the tab's fields display appropriately. 
							Redraw_Screen( (false) );
							break;

						default:
							CURSOR_Down( (true) );
					}
					break;

				case KEY_BACK:
					KEY_process_BACK_from_editing_screen( (void*)&FDTO_MOISTURE_SENSOR_return_to_menu );
					break;

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern UNS_32 MOISTURE_SENSOR_populate_pointers_of_physically_available_sensors_for_display( void )
{
	MOISTURE_SENSOR_GROUP_STRUCT *lsensor;

	UNS_32	lavailable_sensors;

	UNS_32	i;

	// ----------

	lavailable_sensors = 0;

	memset( MOISTURE_SENSOR_menu_items, 0x00, sizeof(MOISTURE_SENSOR_menu_items) );

	// ----------

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	for( i = 0 ; i < MOISTURE_SENSOR_get_list_count(); i++ )
	{
		lsensor = MOISTURE_SENSOR_get_group_at_this_index( i );

		if( (lsensor != NULL) && (MOISTURE_SENSOR_get_physically_available(lsensor) == (true)) )
		{
			MOISTURE_SENSOR_menu_items[ lavailable_sensors ].sensor_ptr = lsensor;

			lavailable_sensors++;
		}
	}

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	// ----------

	return ( lavailable_sensors );
}

/* ---------------------------------------------------------- */
extern void MOISTURE_SENSOR_load_sensor_name_into_guivar( const INT_16 pindex_0_i16 )
{
	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	if( (UNS_32)pindex_0_i16 < MOISTURE_SENSOR_get_list_count() )
	{
		strlcpy( GuiVar_itmGroupName, nm_GROUP_get_name( MOISTURE_SENSOR_menu_items[ pindex_0_i16 ].sensor_ptr ), NUMBER_OF_CHARS_IN_A_NAME );
	}
	else
	{
		Alert_group_not_found();
	}

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern MOISTURE_SENSOR_GROUP_STRUCT *MOISTURE_SENSOR_get_ptr_to_physically_available_sensor( const UNS_32 pindex_0 )
{
	return( MOISTURE_SENSOR_menu_items[ pindex_0 ].sensor_ptr );
}

/* ---------------------------------------------------------- */
/**
 * Returns to the menu on the left from the editing screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task when the user
 * presses a key to move away from the editing screen and back onto the menu.
 * 
 * @param psave_changes True if the changes should be saved; otherwise, false.
 *
 * @author 4/1/2016 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_MOISTURE_SENSOR_return_to_menu( void )
{
	FDTO_GROUP_return_to_menu( &g_MOISTURE_SENSOR_editing_group, (void*)&MOISTURE_SENSOR_extract_and_store_changes_from_GuiVars );
}

/* ---------------------------------------------------------- */
extern void MOISTURE_SENSOR_update_measurements( void )
{
	MOISTURE_SENSOR_copy_latest_readings_into_guivars( g_GROUP_list_item_index );
}

/* ---------------------------------------------------------- */
/**
 * Draws the Moisture Sensor menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the contex of the DISPLAY PROCESSING task when the screen
 * is first navigated to.
 * 
 * @param pcomplete_redraw True if the screen should be complete redrawn; otherwise, false.
 * If false, only elements on the screen are updated, not the entire structure, essentially
 * refreshing the content.
 *
 * @author 4/1/2016 Adrianusv
 *
 * @revisions (none)
 */
extern void FDTO_MOISTURE_SENSOR_draw_menu( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lsensors_in_use;

	UNS_32	lactive_line;

	INT_32	lcursor_to_select;

	// ----------

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	// 4/4/2016 ajv : Only count sensors that are in use as valid sensors to display.
	lsensors_in_use = MOISTURE_SENSOR_populate_pointers_of_physically_available_sensors_for_display();

	if( pcomplete_redraw == (true) )
	{
		lcursor_to_select = GuiLib_NO_CURSOR;

		g_MOISTURE_SENSOR_top_line = 0;

		g_MOISTURE_SENSOR_editing_group = (false);

		g_MOISTURE_SENSOR_current_list_item_index = 0;

		g_GROUP_list_item_index = MOISTURE_SENSOR_get_index_using_ptr_to_mois_struct( MOISTURE_SENSOR_get_ptr_to_physically_available_sensor(g_MOISTURE_SENSOR_current_list_item_index) );

		MOISTURE_SENSOR_copy_group_into_guivars( g_GROUP_list_item_index );
	}
	else
	{
		lcursor_to_select = (INT_32)GuiLib_ActiveCursorFieldNo;

		g_MOISTURE_SENSOR_top_line = (UNS_32)GuiLib_ScrollBox_GetTopLine( 0 );
	}

	// 10/15/2018 ajv : Note that we're using index 1 for the scrMoisture structure. This is to 
	// retain backwards compability of the easyGUI project with other branches. Eventually, 
	// we'll dump index 0. 
	GuiLib_ShowScreen( GuiStruct_scrMoisture_1, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );

	// 4/4/2016 ajv : Close the scroll box to ensure the ScrollBox_Init function handles the
	// redraw properly.
	GuiLib_ScrollBox_Close( 0 );

	lactive_line = MOISTURE_SENSOR_get_menu_index_for_displayed_sensor();

	if( g_MOISTURE_SENSOR_editing_group == (true) )
	{
		// 10/24/2013 ajv : Draw the scroll box without highlighting the active
		// line and draw the indicator to show which station the user is looking
		// at.
		GuiLib_ScrollBox_Init_Custom_SetTopLine( 0, &MOISTURE_SENSOR_load_sensor_name_into_guivar, (INT_16)lsensors_in_use, GuiLib_NO_CURSOR, (INT_16)g_MOISTURE_SENSOR_top_line );
		GuiLib_ScrollBox_SetIndicator( 0, lactive_line );
		GuiLib_ScrollBox_Redraw( 0 );
	}
	else
	{
		GuiLib_ScrollBox_Init_Custom_SetTopLine( 0, &MOISTURE_SENSOR_load_sensor_name_into_guivar, (INT_16)lsensors_in_use, (INT_16)lactive_line, (INT_16)g_MOISTURE_SENSOR_top_line );
	}

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed on the Moisture Sensor menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 4/1/2016 Adrianusv
 */
extern void MOISTURE_SENSOR_process_menu( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	if( g_MOISTURE_SENSOR_editing_group == (true) )
	{
		MOISTURE_SENSOR_process_group( pkey_event );
	}
	else
	{
		switch( pkey_event.keycode )
		{
			case KEY_C_RIGHT:
			case KEY_SELECT:
				good_key_beep();

				g_MOISTURE_SENSOR_current_list_item_index = GuiLib_ScrollBox_GetActiveLine( 0, 0 );

				g_GROUP_list_item_index = MOISTURE_SENSOR_get_index_using_ptr_to_mois_struct( MOISTURE_SENSOR_get_ptr_to_physically_available_sensor( g_MOISTURE_SENSOR_current_list_item_index ) );

				g_MOISTURE_SENSOR_editing_group = (true);

				GuiLib_ActiveCursorFieldNo = CP_MOISTURE_SENSOR_NAME;

				Redraw_Screen( (false) );
				break;

			case KEY_NEXT:
			case KEY_PREV:
				// Change the key to either UP or DOWN so it can be processed using the KEY_C_UP and
				// KEY_C_DOWN case statement.
				if( pkey_event.keycode == KEY_NEXT )
				{
					pkey_event.keycode = KEY_C_DOWN;
				}
				else
				{
					pkey_event.keycode = KEY_C_UP;
				}
				// Don't break here. This will allow the routine to fall into
				// the next case statement which will actually move the cursor
				// up or down appropriately.

			case KEY_C_DOWN:
			case KEY_C_UP:
				SCROLL_BOX_up_or_down( 0, pkey_event.keycode );

				g_MOISTURE_SENSOR_current_list_item_index = GuiLib_ScrollBox_GetActiveLine( 0, 0 );

				g_GROUP_list_item_index = MOISTURE_SENSOR_get_index_using_ptr_to_mois_struct( MOISTURE_SENSOR_get_ptr_to_physically_available_sensor( g_MOISTURE_SENSOR_current_list_item_index ) );

				MOISTURE_SENSOR_copy_group_into_guivars( g_GROUP_list_item_index );

				// 4/16/2014 ajv : Since the screen contents change based upon
				// what type of POC is displayed (e.g., Bypass Manifold has
				// additional flow meters), a full redraw is necessary rather
				// than just a call to Refresh_Screen().
				Redraw_Screen( (false) );
				break;

			case KEY_BACK:
				GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;

				KEY_process_global_keys( pkey_event );

				// 4/22/2016 ajv : If the user got here from the 2-Wire dialog box, redraw that dialog
				// box.
				if( GuiVar_MenuScreenToShow == SCREEN_MENU_SETUP )
				{
					TWO_WIRE_ASSIGNMENT_draw_dialog( (true) );
				}
				break;

			default:
				KEY_process_global_keys( pkey_event );
		}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

