/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"d_copy_dialog.h"

#include	"app_startup.h"

#include	"d_process.h"

#include	"e_station_groups.h"

#include	"group_base_file.h"

#include	"scrollbox.h"

#include	"speaker.h"

#include	"station_groups.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static BOOL_32 *g_COPY_DIALOG_editing_group;

static void *(*g_COPY_DIALOG_add_group_func_ptr)();

static UNS_32 g_COPY_DIALOG_selected_group_index;

BOOL_32	g_COPY_DIALOG_visible;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/31/2013 ajv : Function prototypes for static routines
static void FDTO_COPY_DIALOG_draw_dialog( const BOOL_32 pcomplete_redraw );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void COPY_DIALOG_peform_copy_and_redraw_screen( BOOL_32 *pediting_group, void *(*add_group_func_ptr)(void), const UNS_32 pgroup_index_to_copy_from, const UNS_32 pgroup_index_to_copy_to )
{
	STATION_GROUP_STRUCT	*lgroup_to_copy_from, *lgroup_to_copy_to;

	if( add_group_func_ptr != NULL )
	{
		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		(*add_group_func_ptr)();

		// ---------------------

		lgroup_to_copy_from = STATION_GROUP_get_group_at_this_index( pgroup_index_to_copy_from );

		lgroup_to_copy_to = STATION_GROUP_get_group_at_this_index( pgroup_index_to_copy_to );

		STATION_GROUP_copy_group( lgroup_to_copy_from, lgroup_to_copy_to );

		// ---------------------

		STATION_GROUP_copy_group_into_guivars( pgroup_index_to_copy_to );

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}

	*pediting_group = (true);

	GuiLib_ActiveCursorFieldNo = 0;

	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
static void FDTO_COPY_DIALOG_draw_dialog( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		// 10/31/2013 ajv : Initialize the selected group when it's drawn for
		// the first time. This ensures the seleted group is preserved when
		// going in and out of the group combo box.
		g_COPY_DIALOG_selected_group_index = 0;

		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		strlcpy( GuiVar_GroupName, nm_GROUP_get_name( STATION_GROUP_get_group_at_this_index( 0 ) ), sizeof(GuiVar_GroupName) );

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	g_COPY_DIALOG_visible = (true);

	GuiLib_ShowScreen( GuiStruct_dlgCopyGroup_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_ScrollBox_Init_Custom_SetTopLine( 2, &nm_STATION_GROUP_load_group_name_into_guivar, (INT_16)STATION_GROUP_get_num_groups_in_use(), (INT_16)g_COPY_DIALOG_selected_group_index, (INT_16)g_COPY_DIALOG_selected_group_index );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void COPY_DIALOG_draw_dialog( BOOL_32 *pediting_group, void *(*add_group_func_ptr)(void) )
{
	// 10/31/2013 ajv : Make static copies of the passed-in parameters so they
	// can be used when the user presses the Select key after highlighting a .
	g_COPY_DIALOG_editing_group = pediting_group;
	g_COPY_DIALOG_add_group_func_ptr = add_group_func_ptr;

	// ---------------------
	
	DISPLAY_EVENT_STRUCT	lde;
	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = &FDTO_COPY_DIALOG_draw_dialog;
	lde._06_u32_argument1 = (true);
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
extern void COPY_DIALOG_process_dialog( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( pkey_event.keycode )
	{
		case KEY_ENG_SPA:
			KEY_process_ENG_SPA();

			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = &FDTO_COPY_DIALOG_draw_dialog;
			lde._06_u32_argument1 = (false);
			Display_Post_Command( &lde );
			break;

		case KEY_SELECT:
			good_key_beep();

			g_COPY_DIALOG_visible = (false);

			COPY_DIALOG_peform_copy_and_redraw_screen( g_COPY_DIALOG_editing_group, g_COPY_DIALOG_add_group_func_ptr, (UNS_32)GuiLib_ScrollBox_GetActiveLine( 2, 0 ), g_GROUP_list_item_index );
			break;

		case KEY_C_UP:
		case KEY_C_DOWN:
		case KEY_PLUS:
		case KEY_MINUS:
			SCROLL_BOX_up_or_down( 2, pkey_event.keycode );
			break;

		case KEY_BACK:
			good_key_beep();

			g_COPY_DIALOG_visible = (false);

			Redraw_Screen( (false) );
			break;

		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

