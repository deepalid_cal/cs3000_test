/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_E_LR_PROGRAMMING_RAVEON_H
#define _INC_E_LR_PROGRAMMING_RAVEON_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include "cs_common.h"

#include	"lpc_types.h"

#include	"k_process.h"




/*--------------------------------------------------------------*/
/*---------------------------------------------------------------*/

#define	LR_RAVEON_FREQUENCY_WHOLENUM_MIN				(450)
// 9/15/2017 ajv : Although the Raveon radio supports up to 480, we don't have the ability 
// to issue frequencies beyond 470. Therefore, set the max accordingly. 
#define	LR_RAVEON_FREQUENCY_WHOLENUM_MAX				(470)

#define	LR_RAVEON_FREQUENCY_DECIMAL_MIN					(0)
#define	LR_RAVEON_FREQUENCY_DECIMAL_MAX					(9875)

#define LR_RAVEON_XMT_POWER_MIN							(1)
#define LR_RAVEON_XMT_POWER_MAX							(10)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FDTO_LR_PROGRAMMING_raveon_draw_screen( const BOOL_32 pcomplete_redraw, const UNS_32 port );

extern void LR_PROGRAMMING_raveon_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

