/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_E_MOISTURE_SENSORS_H
#define _INC_E_MOISTURE_SENSORS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"

#include	"moisture_sensors.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern UNS_32 MOISTURE_SENSOR_populate_pointers_of_physically_available_sensors_for_display( void );

extern void MOISTURE_SENSOR_load_sensor_name_into_guivar( const INT_16 pindex_0_i16 );

extern MOISTURE_SENSOR_GROUP_STRUCT *MOISTURE_SENSOR_get_ptr_to_physically_available_sensor( const UNS_32 pindex_0 );

// ----------

extern void MOISTURE_SENSOR_update_measurements( void );

extern void FDTO_MOISTURE_SENSOR_draw_menu( const BOOL_32 pcomplete_redraw );

extern void MOISTURE_SENSOR_process_menu( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

