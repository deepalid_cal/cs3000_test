/* ---------------------------------------------------------- */

// 8/28/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"r_rt_electrical.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"configuration_network.h"

#include	"cursor_utils.h"

#include	"d_live_screens.h"

#include	"e_poc.h"

#include	"flowsense.h"

#include	"report_utils.h"

#include	"screen_utils.h"

#include	"tpmicro_data.h"





/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
// 6/1/2015 ajv : Note that this routine is extern, even though it's not used anywhere
// outside of this file. This is to ensure we don't run into the floating-point math issue
// we've seen where the compiler seems to optomize the math to result in a 0.
extern void REAL_TIME_ELECTRICAL_draw_scroll_line( const INT_16 pline_index_i16 )
{
	// 6/1/2015 ajv : See the same constant declarations in the bypass_operation.c file for
	// more explanation. We don't have the optimization problem here that is described there.
	// However as a cautionary move, we'll implement them here too. The VOLATILE constants seem
	// to allow a work around to a compiler bug. As I said better described in
	// bypass_operation.c.
	const volatile float ONE_THOUSAND = 1000.0;

	NETWORK_CONFIG_get_controller_name_str_for_ui( (UNS_32)pline_index_i16, (char *)&GuiVar_LiveScreensElectricalControllerName );

	// 11/1/2013 ajv : TODO For now, to limit how long the string is, we're
	// going to truncate the controller name manually. Ideally, we'd like to
	// have easyGUI automatically limit the size using a clipping rectangle or
	// similar structure, but those don't seem to behave properly on
	// scrolling lists.
	// 
	// 23 characters is how many characters, using fixed-width, we can draw
	// before we hit the amperage setting.
	strlcpy( GuiVar_LiveScreensElectricalControllerName, GuiVar_LiveScreensElectricalControllerName, 23 );

	xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );

	GuiVar_StatusCurrentDraw = ((float)tpmicro_data.measured_ma_current_as_rcvd_from_master[ pline_index_i16 ] / ONE_THOUSAND);

	// 11/8/2013 ajv : TODO This needs to use a distributed value rather than
	// the local tpmicro_data version
	GuiVar_StatusFuseBlown = tpmicro_comm.fuse_blown;

	xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void FDTO_REAL_TIME_ELECTRICAL_draw_report( const BOOL_32 pcomplete_redraw )
{
	if( pcomplete_redraw == (true) )
	{
		GuiLib_ShowScreen( GuiStruct_rptRTElectrical_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );

		GuiLib_ScrollBox_Close( 0 );

		// 10/16/2013 ajv : TODO Modify LIVE_SCREENS_draw_electrical_scroll_line
		// to use a pre-filled structure of controllers since controllers don't
		// need to be lettered sequentially.

		// 4/3/2014 rmd : This doesn't any sense to me. But was using the chain_members.NOCIC. Which
		// is gone now. So replaced with the FLOWSENSE value.
		GuiLib_ScrollBox_Init( 0, &REAL_TIME_ELECTRICAL_draw_scroll_line, FLOWSENSE_we_are_poafs() ? (INT_16)COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members() : 1, 0 );
	}
	else
	{
		GuiLib_ScrollBox_Redraw( 0 );
	}

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void REAL_TIME_ELECTRICAL_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		case KEY_BACK:
			GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;

			KEY_process_global_keys( pkey_event );

			if( GuiVar_MenuScreenToShow == SCREEN_MENU_DIAGNOSTICS )
			{
				// 3/16/2016 ajv : Since the user got here from the LIVE SCREENS dialog box, redraw that
				// dialog box.
				LIVE_SCREENS_draw_dialog( (true) );
			}
			break;

		default:
			REPORTS_process_report( pkey_event, 0, 0 );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

