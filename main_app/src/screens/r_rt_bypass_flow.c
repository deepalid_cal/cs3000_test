/* ---------------------------------------------------------- */

// 8/28/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"r_rt_bypass_flow.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"bypass_operation.h"

#include	"cursor_utils.h"

#include	"d_live_screens.h"

#include	"e_poc.h"

#include	"flowsense.h"

#include	"report_utils.h"

#include	"screen_utils.h"




/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void REAL_TIME_BYPASS_FLOW_draw_scroll_line( const INT_16 pline_index_i16 )
{
	POC_GROUP_STRUCT 	*lpoc;

	BY_POC_RECORD		*lpreserve;

	POC_FM_CHOICE_AND_RATE_DETAILS fm_choice_and_rate_details[ POC_BYPASS_LEVELS_MAX ];

	UNS_32	lpreserve_index;

	UNS_32	lpoc_GID;

	// ----------

	// 12/22/2014 ajv : Since line index is 0-based, make sure we increment the level by 1 so it
	// shows 1-3 instead of 0-2.
	GuiVar_LiveScreensBypassFlowLevel = (UNS_32)(pline_index_i16 + 1);

	// ----------

	// 2012.06.19 ajv : Always take the preserves mutex before taking the list
	// mutex. We may be able to get around needing the list mutex here by
	// building the entire screen using the perserves, but I'm not sure how we'd
	// deal with getting the name at the moment. Will reevaluate later.
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	// 6/1/2016 rmd : See if there is a bypass POC at this box that is physically available.
	lpoc = nm_POC_get_pointer_to_bypass_poc_with_this_box_index_0( FLOWSENSE_get_controller_index(), (true) );

	if( lpoc != NULL )
	{
		lpoc_GID = nm_GROUP_get_group_ID( lpoc );

		lpreserve = POC_PRESERVES_get_poc_preserve_for_this_poc_gid( lpoc_GID, &lpreserve_index );

		if( lpreserve != NULL )
		{
			GuiVar_LiveScreensBypassFlowMVOpen = ( ((lpreserve->ws[ pline_index_i16 ].pbf.master_valve_energized_irri == (true)) && (lpreserve->ws[ pline_index_i16 ].master_valve_type == POC_MV_TYPE_NORMALLY_CLOSED)) ||
												   ((lpreserve->ws[ pline_index_i16 ].pbf.master_valve_energized_irri == (false)) && (lpreserve->ws[ pline_index_i16 ].master_valve_type == POC_MV_TYPE_NORMALLY_OPEN)) );

			// ----------

			GuiVar_LiveScreensBypassFlowFlow = (UNS_32)lpreserve->ws[ pline_index_i16 ].delivered_5_second_average_gpm_irri;

			GuiVar_LiveScreensBypassFlowAvgFlow = (UNS_32)lpreserve->ws[ pline_index_i16 ].delivered_5_second_average_gpm_irri;

			// ----------

			if( (UNS_32)pline_index_i16 < (POC_get_bypass_number_of_levels(lpoc) - 1) )
			{
				GuiVar_LiveScreensBypassFlowShowMaxFlow = (true);
			}
			else
			{
				GuiVar_LiveScreensBypassFlowShowMaxFlow = (false);
			}

			// 8/21/2014 ajv : Retrieve the flow meter type so we can use it
			// to index into the bypass manifold transitions array.
			POC_get_fm_choice_and_rate_details( lpoc_GID, fm_choice_and_rate_details );

			GuiVar_LiveScreensBypassFlowMaxFlow = BYPASS_get_transition_rate_for_this_flow_meter_type( fm_choice_and_rate_details[ pline_index_i16 ].flow_meter_choice );

			strlcpy( GuiVar_GroupName, nm_GROUP_get_name(lpoc), sizeof(GuiVar_GroupName) );
		}
		else
		{
			// 8/21/2014 ajv : Since the preserve isn't valid, initialize the
			// variables to 0. This hsould never happen, but it's important to
			// make sure this case is covered.

			GuiVar_LiveScreensBypassFlowMVOpen = (false);

			GuiVar_LiveScreensBypassFlowFlow = 0;
			GuiVar_LiveScreensBypassFlowAvgFlow = 0; 
			GuiVar_LiveScreensBypassFlowMaxFlow = 0;

			GuiVar_GroupName[ 0 ] = 0x00;
		}
	}
	else
	{
		Alert_group_not_found();
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void FDTO_REAL_TIME_BYPASS_FLOW_draw_report( const BOOL_32 pcomplete_redraw )
{
	POC_GROUP_STRUCT	*lpoc;

	UNS_32	lnum_levels_to_display;

	if( pcomplete_redraw == (true) )
	{
		GuiLib_ShowScreen( GuiStruct_rptRTBypassFlow_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );

		// 6/1/2016 rmd : See if there is a bypass POC at this box that is physically available.
		lpoc = nm_POC_get_pointer_to_bypass_poc_with_this_box_index_0( FLOWSENSE_get_controller_index(), (true) );

		if( lpoc != NULL )
		{
			lnum_levels_to_display = POC_get_bypass_number_of_levels( lpoc );
		}
		else
		{
			lnum_levels_to_display = 0;
		}

		GuiLib_ScrollBox_Close( 0 );
		GuiLib_ScrollBox_Init( 0, &REAL_TIME_BYPASS_FLOW_draw_scroll_line, (INT_16)lnum_levels_to_display, 0 );
	}
	else
	{
		GuiLib_ScrollBox_Redraw( 0 );
	}

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void REAL_TIME_BYPASS_FLOW_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		case KEY_BACK:
			GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;

			KEY_process_global_keys( pkey_event );

			if( GuiVar_MenuScreenToShow == SCREEN_MENU_DIAGNOSTICS )
			{
				// 3/16/2016 ajv : Since the user got here from the LIVE SCREENS dialog box, redraw that
				// dialog box.
				LIVE_SCREENS_draw_dialog( (true) );
			}
			break;

		default:
			REPORTS_process_report( pkey_event, 0, 0 );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

