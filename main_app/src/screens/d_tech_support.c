/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"d_tech_support.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"e_factory_reset.h"

#include	"guilib.h"

#include	"r_memory_use.h"

#include	"r_serial_port_info.h"

#include	"r_task_list.h"

#include	"screen_utils.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_TECH_SUPPORT_MEMORY_USAGE			(50)
#define	CP_TECH_SUPPORT_TASK_LIST				(51)
#define	CP_TECH_SUPPORT_FACTORY_RESET			(52)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

UNS_32	g_TECH_SUPPORT_cursor_position_when_dialog_displayed;

BOOL_32	g_TECH_SUPPORT_dialog_visible;

// 6/5/2015 ajv : Keep track of the last cursor position the user had selected on the dialog
// so it brings them back to the same field when they open the dialog again.
static UNS_32 g_TECH_SUPPORT_last_cursor_position;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_TECH_SUPPORT_draw_dialog( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	g_TECH_SUPPORT_cursor_position_when_dialog_displayed = (UNS_32)GuiLib_ActiveCursorFieldNo;

	if( pcomplete_redraw == (true) )
	{
		// 6/5/2015 ajv : If we've never entered the screen before, the last cursor position will be
		// uninitialized at 0. In that case, set it to the top item in the list.
		if( g_TECH_SUPPORT_last_cursor_position == 0 )
		{
			g_TECH_SUPPORT_last_cursor_position = CP_TECH_SUPPORT_MEMORY_USAGE;
		}

		lcursor_to_select = g_TECH_SUPPORT_last_cursor_position;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	g_TECH_SUPPORT_dialog_visible = (true);

	GuiLib_ShowScreen( GuiStruct_dlgTechSupportMenu_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void TECH_SUPPORT_draw_dialog( const BOOL_32 pcomplete_redraw )
{
	DISPLAY_EVENT_STRUCT	lde;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void*)&FDTO_TECH_SUPPORT_draw_dialog;
	lde._06_u32_argument1 = pcomplete_redraw;
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
extern void TECH_SUPPORT_process_dialog( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			// 6/5/2015 ajv : Retain the last cursor position the user was on so they go back to it when
			// they open the dialog again.
			g_TECH_SUPPORT_last_cursor_position = GuiLib_ActiveCursorFieldNo;

			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TECH_SUPPORT_MEMORY_USAGE:
					g_TECH_SUPPORT_dialog_visible = (false);

					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._02_menu = SCREEN_MENU_SETUP;
					lde._03_structure_to_draw = GuiStruct_rptMemoryUsage_0;
					lde._04_func_ptr = (void *)&FDTO_MEM_draw_report;
					lde._06_u32_argument1 = (true);
					lde._08_screen_to_draw = GuiVar_MenuScreenToShow;
					lde.key_process_func_ptr = MEM_process_report;
					Change_Screen( &lde );
					break;

				case CP_TECH_SUPPORT_TASK_LIST:
					g_TECH_SUPPORT_dialog_visible = (false);

					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._02_menu = SCREEN_MENU_SETUP;
					lde._03_structure_to_draw = GuiStruct_rptTasks_0;
					lde._04_func_ptr = (void *)&FDTO_TASK_LIST_draw_report;
					lde._06_u32_argument1 = (true);
					lde._08_screen_to_draw = GuiVar_MenuScreenToShow;
					lde.key_process_func_ptr = TASK_LIST_process_report;
					Change_Screen( &lde );
					break;

				case CP_TECH_SUPPORT_FACTORY_RESET:
					g_TECH_SUPPORT_dialog_visible = (false);

					lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
					lde._02_menu = SCREEN_MENU_SETUP;
					lde._03_structure_to_draw = GuiStruct_scrFactoryReset_0;
					lde._04_func_ptr = (void *)&FDTO_FACTORY_RESET_draw_screen;
					lde._08_screen_to_draw = GuiVar_MenuScreenToShow;
					lde.key_process_func_ptr = FACTORY_RESET_process_screen;
					Change_Screen( &lde );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_C_UP:
			CURSOR_Up( (true) );
			break;

		case KEY_C_DOWN:
			CURSOR_Down( (true) );
			break;

		case KEY_BACK:
			good_key_beep();

			// 6/5/2015 ajv : Retain the last cursor position the user was on so they go back to it when
			// they open the dialog again.
			g_TECH_SUPPORT_last_cursor_position = GuiLib_ActiveCursorFieldNo;

			g_TECH_SUPPORT_dialog_visible = (false);

			// 3/13/2014 ajv : Return to the last cursor position the user was on when they pressed BACK
			// and redraw the screen to remove the dialog box.
			GuiLib_ActiveCursorFieldNo = (INT_16)g_TECH_SUPPORT_cursor_position_when_dialog_displayed;

			Redraw_Screen( (false) );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

