/* ---------------------------------------------------------- */

#include	"r_rt_communications.h"

#include	<string.h>

#include	"app_startup.h"

#include	"comm_mngr.h"

#include	"configuration_controller.h"

#include	"configuration_network.h"

#include	"controller_initiated.h"

#include	"d_live_screens.h"

#include	"flowsense.h"

#include	"GuiLib.h"

#include	"k_process.h"

#include	"screen_utils.h"

#include	"tpmicro_comm.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void FDTO_REAL_TIME_COMMUNICATIONS_update_report( void )
{
	// 4/2/2014 ajv : If the state has changed since the last refresh, we need
	// to redraw the entire screen.
	BOOL_32 prev_fl_enabled_state;

	// 4/2/2014 ajv : If the state has changed since the last refresh, we need
	// to redraw the entire screen.
	BOOL_32 prev_central_comm_enabled_state;

	// ----------
	
	prev_fl_enabled_state = GuiVar_LiveScreensCommFLEnabled;

	GuiVar_LiveScreensCommFLCommMngrMode = comm_mngr.mode;

	GuiVar_LiveScreensCommFLInForced = comm_mngr.chain_is_down;

	GuiVar_LiveScreensCommFLEnabled = FLOWSENSE_we_are_poafs();

	GuiVar_FLNumControllersInChain = COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members();

	// ---------------------

	prev_central_comm_enabled_state = GuiVar_LiveScreensCommCentralEnabled;

	GuiVar_LiveScreensCommCentralEnabled = port_device_table[ config_c.port_A_device_index ].on_port_A_enables_controller_to_make_CI_messages;
	
	if( GuiVar_LiveScreensCommCentralEnabled )
	{
		GuiVar_LiveScreensCommCentralConnected = CONFIG_is_connected();
	}
	else
	{
		GuiVar_LiveScreensCommCentralConnected = (false);
	}

	// ---------------------

	xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );

	// 9/30/2013 ajv : If we haven't received a message within the specified
	// amount of time, notify the user that there's no communication between the
	// main board and TP Micro.
	GuiVar_LiveScreensCommTPMicroConnected = ( tpmicro_comm.number_of_outgoing_since_last_incoming < TPMICRO_OUTGOING_BETWEEN_INCOMING_LIMIT );

	xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );

	// ---------------------

	if( (prev_fl_enabled_state != GuiVar_LiveScreensCommFLEnabled) || (prev_central_comm_enabled_state != GuiVar_LiveScreensCommCentralEnabled) )
	{
		// 4/2/2014 ajv : If the state has changed since the last refresh, we
		// need to redraw the entire screen.
		FDTO_Redraw_Screen( (true) );
	}
	else
	{
		GuiLib_Refresh();
	}
}

/* ---------------------------------------------------------- */
extern void FDTO_REAL_TIME_COMMUNICATIONS_draw_report( const BOOL_32 pcomplete_redraw )
{
	if( pcomplete_redraw == (true) )
	{
		GuiLib_ShowScreen( GuiStruct_rptRTCommunications_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );
	}

	FDTO_REAL_TIME_COMMUNICATIONS_update_report();
}

/* ---------------------------------------------------------- */
extern void REAL_TIME_COMMUNICATIONS_process_report( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		case KEY_BACK:
			GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;

			KEY_process_global_keys( pkey_event );

			if( GuiVar_MenuScreenToShow == SCREEN_MENU_DIAGNOSTICS )
			{
				// 3/16/2016 ajv : Since the user got here from the LIVE SCREENS dialog box, redraw that
				// dialog box.
				LIVE_SCREENS_draw_dialog( (true) );
			}
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

