/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"e_irrigation_system.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"change.h"

#include	"configuration_controller.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"e_station_selection_grid.h"

#include	"flowsense.h"

#include	"group_base_file.h"

#include	"e_keyboard.h"

#include	"e_poc.h"

#include	"irrigation_system.h"

#include	"poc.h"

#include	"scrollbox.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_SYSTEM_NAME						(0)
#define	CP_SYSTEM_VIEW_STATIONS				(1)
#define	CP_SYSTEM_ADD_STATIONS				(2)
#define CP_SYSTEM_POC_LIST					(3)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2012.06.15 ajv : We need to track if this system is watering when the user
// enters the screen. If so, we're going to display a warning message and
// prevent them from making any changes by forcing the cursor to remain on one
// of the tabs.
static BOOL_32	g_SYSTEM_is_watering;

static BOOL_32	g_SYSTEM_editing_group;

static UNS_32	g_SYSTEM_index_of_poc_when_dialog_displayed;

// 5/23/2016 ajv : Track the number of POCs that are available for display (i.e., physically
// available). We use this for two purposes: 1) to draw the correct number of lines in the
// easyGUI scroll box and 2) to prevent the user from dropping into the POC list if there
// are no POCs available.
static UNS_32	g_SYSTEM_num_pocs_physically_available;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void FDTO_SYSTEM_return_to_menu( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void SYSTEM_populate_poc_scrollbox( const INT_16 pline_index_0 )
{
	POC_GROUP_STRUCT *lpoc;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );
 
	lpoc = POC_MENU_items[ pline_index_0 ].poc_ptr;
 
	if( lpoc != NULL ) 
	{
		// 11/1/2013 ajv : TODO For now, to limit how long the string is, we're
		// going to truncate the POC name manually. Ideally, we'd like to have
		// easyGUI automatically limit the size using a clipping rectangle or
		// similar structure, but those don't seem to behave properly on
		// scrolling lists.
		// 
		// 24 characters is how many characters, using fixed-width, we can draw
		// before we hit the Yes/No selection.
		strlcpy( GuiVar_itmPOC, nm_GROUP_get_name( lpoc ), 24 ); 

		GuiVar_SystemPOCSelected = (g_GROUP_ID == POC_get_GID_irrigation_system(lpoc));
	}
	else
	{
		Alert_group_not_found();
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void FDTO_SYSTEM_draw_poc_scrollbox( void )
{
	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	g_SYSTEM_num_pocs_physically_available = POC_populate_pointers_of_POCs_for_display( false);

	GuiLib_ScrollBox_Close( 1 );
	GuiLib_ScrollBox_Init( 1, &SYSTEM_populate_poc_scrollbox, (INT_16)g_SYSTEM_num_pocs_physically_available, g_SYSTEM_index_of_poc_when_dialog_displayed );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
static void FDTO_SYSTEM_enter_poc_scrollbox( const BOOL_32 pmove_to_top_line )
{
	GuiLib_ScrollBox_Close( 1 );

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	g_SYSTEM_num_pocs_physically_available = POC_populate_pointers_of_POCs_for_display( false);

	if( pmove_to_top_line == (true) )
	{
		GuiLib_ScrollBox_Init( 1, &SYSTEM_populate_poc_scrollbox, (INT_16)g_SYSTEM_num_pocs_physically_available, 0 );
	}
	else
	{
		GuiLib_ScrollBox_Init_Custom_SetTopLine( 1, &SYSTEM_populate_poc_scrollbox, (INT_16)g_SYSTEM_num_pocs_physically_available, (INT_16)g_SYSTEM_num_pocs_physically_available, (INT_16)g_SYSTEM_num_pocs_physically_available );
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	GuiLib_Cursor_Select( CP_SYSTEM_POC_LIST );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
static void FDTO_SYSTEM_leave_poc_scrollbox( void )
{
	GuiLib_ScrollBox_Close( 1 );

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	g_SYSTEM_num_pocs_physically_available = POC_populate_pointers_of_POCs_for_display( false);

	GuiLib_ScrollBox_Init( 1, &SYSTEM_populate_poc_scrollbox, (INT_16)g_SYSTEM_num_pocs_physically_available, GuiLib_NO_CURSOR );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	FDTO_Cursor_Select( CP_SYSTEM_NAME, (true) );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Creates a new group.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user selects "Add New" or assigns a station to a new group.
 * 
 * @param passign_current_station_to_new_group True if the currently selected
 * station should be assigned to the newly created group; otherwise, false. This
 * is set true when the user removes a station from a group and assigns it to a
 * new group.
 * 
 * @param pshow_new_group True if the new group should be displayed, otherwise,
 * false. This is set true when the user selects "Add New", but remains false
 * when removing a station from a group and assigning it to a new group.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void SYSTEM_add_new_group( void )
{
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	nm_GROUP_create_new_group_from_UI( (void*)&nm_SYSTEM_create_new_group, (void*)&SYSTEM_copy_group_into_guivars );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Processes the key event from the irrigation system screen for a particular
 * system.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 8/10/2011 Adrianusv
 *
 * @revisions
 *    8/10/2011 Initial release
 */
static void SYSTEM_process_group( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	POC_GROUP_STRUCT		*lpoc;

	INT_32	lactive_line;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_dlgKeyboard_0:
			if( ((pkey_event.keycode == KEY_BACK) || ((pkey_event.keycode == KEY_SELECT) && (GuiLib_ActiveCursorFieldNo == 49 /*CP_QWERTY_KEYBOARD_OK*/))) )
			{
				SYSTEM_extract_and_store_group_name_from_GuiVars();
			}

			KEYBOARD_process_key( pkey_event, &FDTO_SYSTEM_draw_menu );
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_SYSTEM_NAME:
							GROUP_process_show_keyboard( 164, 27 );
							break;

						case CP_SYSTEM_POC_LIST:
							lpoc = POC_MENU_items[ GuiLib_ScrollBox_GetActiveLine(1, 0) ].poc_ptr;

							// 10/23/2013 ajv : POCs must be assigned to a
							// system. Therefore, do not allow the user to
							// remove a POC from a system - they must be pulled
							// into anther system.
							if( g_GROUP_ID == POC_get_GID_irrigation_system( lpoc ) )
							{
								bad_key_beep();

								g_SYSTEM_index_of_poc_when_dialog_displayed = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 1, 0 );

								// 10/23/2013 ajv : Display a warning to the
								// user as to why they can't remove the POC.
								DIALOG_draw_ok_dialog( GuiStruct_dlgCantRemovePOC_0 );
							}
							else
							{
								good_key_beep();

								// Assign the POC to this main line group.
								nm_POC_set_GID_irrigation_system( lpoc, g_GROUP_ID, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, POC_get_change_bits_ptr( lpoc, CHANGE_REASON_KEYPAD ) );
								SCROLL_BOX_redraw( 1 );
							}
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_SYSTEM_POC_LIST:
							lpoc = POC_MENU_items[ GuiLib_ScrollBox_GetActiveLine(1, 0) ].poc_ptr;

							if( pkey_event.keycode == KEY_PLUS )
							{
								if( g_GROUP_ID != POC_get_GID_irrigation_system( lpoc ) )
								{
									good_key_beep();

									// Assign the POC to this main line group.
									nm_POC_set_GID_irrigation_system( lpoc, g_GROUP_ID, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, POC_get_change_bits_ptr( lpoc, CHANGE_REASON_KEYPAD ) );
									SCROLL_BOX_redraw( 1 );
								}
								else
								{
									bad_key_beep();
								}
							}
							else
							{
								bad_key_beep();

								// 10/23/2013 ajv : POCs must be assigned to a
								// system. Therefore, do not allow the user to
								// remove a POC from a system - they must be
								// pulled into anther system.
								if( g_GROUP_ID == POC_get_GID_irrigation_system( lpoc ) )
								{
									g_SYSTEM_index_of_poc_when_dialog_displayed = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 1, 0 );

									// 10/23/2013 ajv : Display a warning to the
									// user as to why they can't remove the POC.
									DIALOG_draw_ok_dialog( GuiStruct_dlgCantRemovePOC_0 );
								}
								break;
							}
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_NEXT:
				case KEY_PREV:
					// 11/5/2014 ajv : Retain the currently selected POC when
					// the user presses NEXT or PREVIOUS.
					g_SYSTEM_index_of_poc_when_dialog_displayed = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 1, 0 );

					GROUP_process_NEXT_and_PREV( pkey_event.keycode, SYSTEM_num_systems_in_use(), (void*)&SYSTEM_extract_and_store_changes_from_GuiVars, (void*)&SYSTEM_get_group_at_this_index, (void*)&SYSTEM_copy_group_into_guivars );

					// 11/4/2014 ajv : Populate and draw the POC scroll box.
					lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
					lde._04_func_ptr = (void *)&FDTO_SYSTEM_draw_poc_scrollbox;
					Display_Post_Command( &lde );
					break;

				case KEY_C_UP:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_SYSTEM_POC_LIST:
							lactive_line = GuiLib_ScrollBox_GetActiveLine( 1, 0 );

							if( lactive_line > GuiLib_NO_CURSOR )
							{
								if( lactive_line == GuiLib_ScrollBox_GetTopLine( 1 ) )
								{
									lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
									lde._04_func_ptr = (void*)&FDTO_SYSTEM_leave_poc_scrollbox;
									Display_Post_Command( &lde );
								}
								else
								{
									SCROLL_BOX_up_or_down( 1, pkey_event.keycode );
								}
							}
							else
							{
								bad_key_beep();
							}
							break;
						
						default:
							bad_key_beep();
					}
					break;

				case KEY_C_DOWN:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_SYSTEM_NAME:
							if( g_SYSTEM_is_watering == (false) )
							{
								if( g_SYSTEM_num_pocs_physically_available > 0 )
								{
									good_key_beep();

									lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
									lde._04_func_ptr = (void*)&FDTO_SYSTEM_enter_poc_scrollbox;
									lde._06_u32_argument1 = (true);
									Display_Post_Command( &lde );
								}
								else
								{
									// 5/23/2016 ajv : There are no POCs physically available so don't allow the user to drop
									// down into the scroll box.
									bad_key_beep();
								}
							}
							else
							{
								// 10/23/2013 ajv : Display a warning to the
								// user that they can't make a change while the
								// system is watering.
								DIALOG_draw_ok_dialog( GuiStruct_dlgMainlineIrrigating_0 );
							}
							break;

						case CP_SYSTEM_POC_LIST:
							lactive_line = GuiLib_ScrollBox_GetActiveLine( 1, 0 );

							if( lactive_line > GuiLib_NO_CURSOR )
							{
								if( lactive_line == (INT_32)(g_SYSTEM_num_pocs_physically_available - 1) )
								{
									bad_key_beep();
								}
								else
								{
									SCROLL_BOX_up_or_down( 1, pkey_event.keycode );
								}
							}
							else
							{
								bad_key_beep();
							}
							break;

						default:
							CURSOR_Down( (true) );
					}
					break;

				case KEY_C_LEFT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_SYSTEM_NAME:
							KEY_process_BACK_from_editing_screen( (void*)&FDTO_SYSTEM_return_to_menu );
							break;

						case CP_SYSTEM_POC_LIST:
							lactive_line = GuiLib_ScrollBox_GetActiveLine( 1, 0 );

							if( lactive_line > GuiLib_NO_CURSOR )
							{
								if( lactive_line == GuiLib_ScrollBox_GetTopLine( 1 ) )
								{
									lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
									lde._04_func_ptr = (void*)&FDTO_SYSTEM_leave_poc_scrollbox;
									Display_Post_Command( &lde );
								}
								else
								{
									// 9/24/2015 ajv : Force the keycode to be KEY_C_UP so the SCROLL_BOX_up_or_down function
									// handles it properly.
									pkey_event.keycode = KEY_C_UP;

									SCROLL_BOX_up_or_down( 1, pkey_event.keycode );
								}
							}
							else
							{
								bad_key_beep();
							}
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_C_RIGHT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_SYSTEM_NAME:
							if( g_SYSTEM_is_watering == (false) )
							{
								if( g_SYSTEM_num_pocs_physically_available > 0 )
								{
									good_key_beep();

									lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
									lde._04_func_ptr = (void*)&FDTO_SYSTEM_enter_poc_scrollbox;
									lde._06_u32_argument1 = (true);
									Display_Post_Command( &lde );
								}
								else
								{
									// 5/23/2016 ajv : There are no POCs physically available so don't allow the user to drop
									// down into the scroll box.
									bad_key_beep();
								}
							}
							else
							{
								// 10/23/2013 ajv : Display a warning to the
								// user that they can't make a change while the
								// system is watering.
								DIALOG_draw_ok_dialog( GuiStruct_dlgMainlineIrrigating_0 );
							}
							break;

						case CP_SYSTEM_POC_LIST:
							lactive_line = GuiLib_ScrollBox_GetActiveLine( 1, 0 );

							if( lactive_line > GuiLib_NO_CURSOR )
							{
								if( lactive_line == (INT_32)(g_SYSTEM_num_pocs_physically_available - 1) )
								{
									bad_key_beep();
								}
								else
								{
									// 9/24/2015 ajv : Force the keycode to be KEY_C_DOWN so the SCROLL_BOX_up_or_down function
									// handles it properly.
									pkey_event.keycode = KEY_C_DOWN;

									SCROLL_BOX_up_or_down( 1, pkey_event.keycode );
								}
							}
							else
							{
								bad_key_beep();
							}
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_BACK:
					KEY_process_BACK_from_editing_screen( (void*)&FDTO_SYSTEM_return_to_menu );
					break;

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Returns to the menu on the left from the editing screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user presses a key to move away from the editing screen and back
 * onto the menu.
 * 
 * @param psave_changes True if the changes should be saved; otherwise, false.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_SYSTEM_return_to_menu( void )
{
	FDTO_GROUP_return_to_menu( &g_SYSTEM_editing_group, (void*)&SYSTEM_extract_and_store_changes_from_GuiVars );

	g_SYSTEM_index_of_poc_when_dialog_displayed = GuiLib_NO_CURSOR;

	// 11/4/2014 ajv : Force the POC scroll box to be redrawn as well.
	// Otherwise, it will blank out when screen is redrawn.
	FDTO_SYSTEM_draw_poc_scrollbox();
}

/* ---------------------------------------------------------- */
/**
 * Draws the Main Line menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the screen is first navigated to.
 * 
 * @param pcomplete_redraw True if the screen should be complete redrawn;
 * otherwise, false. If false, only elements on the screen are updated, not the
 * entire structure, essentially refreshing the content.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void FDTO_SYSTEM_draw_menu( const BOOL_32 pcomplete_redraw )
{
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	FDTO_GROUP_draw_menu( pcomplete_redraw, &g_SYSTEM_editing_group, SYSTEM_num_systems_in_use(), GuiStruct_scrMainLines_0, (void *)&SYSTEM_load_system_name_into_scroll_box_guivar, (void *)&SYSTEM_copy_group_into_guivars, (SYSTEM_num_systems_in_use() < MAX_POSSIBLE_SYSTEMS) );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	if( pcomplete_redraw )
	{
		g_SYSTEM_index_of_poc_when_dialog_displayed = GuiLib_NO_CURSOR;
	}

	// 11/4/2014 ajv : Populate and draw the the POC scroll box.
	FDTO_SYSTEM_draw_poc_scrollbox();
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed on the Main Line menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void SYSTEM_process_menu( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	GROUP_process_menu( pkey_event, &g_SYSTEM_editing_group, SYSTEM_num_systems_in_use(), (void *)&SYSTEM_process_group, (void *)&SYSTEM_add_new_group, (void *)&SYSTEM_get_group_at_this_index, (void *)&SYSTEM_copy_group_into_guivars );

	// Refresh the POC scroll box when navigating up and down the system menu
	if( (pkey_event.keycode != KEY_BACK) && ((UNS_32)GuiLib_ScrollBox_GetActiveLine(0, 0) < SYSTEM_num_systems_in_use()) && (g_SYSTEM_editing_group == (false)) )
	{
		SCROLL_BOX_redraw( 1 );
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

