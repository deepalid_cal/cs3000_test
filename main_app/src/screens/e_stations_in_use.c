/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_stations_in_use.h"

#include	"app_startup.h"

#include	"change.h"

#include	"configuration_controller.h"

#include	"cursor_utils.h"

#include	"e_station_selection_grid.h"

#include	"flowsense.h"

#include	"group_base_file.h"

#include	"m_main.h"

#include	"stations.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void FDTO_STATIONS_IN_USE_draw_screen( const BOOL_32 pcomplete_redraw )
{
	FDTO_STATION_SELECTION_GRID_draw_screen( pcomplete_redraw, (false) );
}

/* ---------------------------------------------------------- */
/**
 * Processes the key event from the Stations In Use screen.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 *
 * @param pkey_event The key event to be processed.
 *
 * @author 11/14/2011 Adrianusv
 *
 * @revisions
 *    11/14/2011 Initial release
 *    2/7/2012   Renamed function
 */
extern void STATIONS_IN_USE_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	STATION_SELECTION_GRID_process_screen( pkey_event );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

