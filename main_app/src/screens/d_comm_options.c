/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"d_comm_options.h"

#include	"configuration_controller.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"e_activate_options.h"

#include	"e_comm_test.h"

#include	"e_en_programming.h"

#include	"e_gr_programming_pw.h"

#include	"e_hub.h"

#include	"e_lr_programming_freewave.h"

#include	"e_lr_programming_raveon.h"

#include	"e_sr_programming.h"

#include	"e_wen_programming.h"

#include	"r_serial_port_info.h"

#include	"screen_utils.h"

#include	"speaker.h"

#include	"comm_mngr.h"

#include	"radio_test.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

UNS_32	g_COMM_OPTIONS_cursor_position_when_dialog_displayed;

BOOL_32	g_COMM_OPTIONS_dialog_visible;

// 6/5/2015 ajv : Keep track of the last cursor position the user had selected on the dialog
// so it brings them back to the same field when they open the dialog again.
static UNS_32 g_COMM_OPTIONS_last_cursor_position;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_COMM_OPTIONS_draw_dialog( const BOOL_32 pcomplete_redraw )
{
	GuiVar_CommOptionPortAIndex = config_c.port_A_device_index;
	GuiVar_CommOptionPortBIndex = config_c.port_B_device_index;

	// ----------

	// 3/13/2014 ajv : Determine whether there's anything on Port A and B to decide if we need
	// to show an option to access a programming screen.
	// 
	// 2/23/2017 ajv : No reason to show the GR devices since there's no programming available
	// for them anyway.
	GuiVar_CommOptionPortAInstalled = ((GuiVar_CommOptionPortAIndex != COMM_DEVICE_NONE_PRESENT) && 
									   (GuiVar_CommOptionPortAIndex != COMM_DEVICE_GR_AIRLINK) && 
									   (GuiVar_CommOptionPortAIndex != COMM_DEVICE_GR_PREMIERWAVE) &&
									   (GuiVar_CommOptionPortAIndex != COMM_DEVICE_MULTITECH_LTE) );

	// 2/28/2017 ajv : Port B only supports LR and SR devices.
	GuiVar_CommOptionPortBInstalled = ( (GuiVar_CommOptionPortBIndex == COMM_DEVICE_LR_FREEWAVE) ||
									    (GuiVar_CommOptionPortBIndex == COMM_DEVICE_LR_RAVEON) ||
									    (GuiVar_CommOptionPortBIndex == COMM_DEVICE_SR_FREEWAVE) );

	// ----------

	// 5/18/2015 ajv : Only display the Test Cloud Communications screen if there's an
	// internet-connected communication device available.
	GuiVar_CommOptionCloudCommTestAvailable = port_device_table[ config_c.port_A_device_index ].on_port_A_enables_controller_to_make_CI_messages;

	// 11/22/2017 ajv : Only display the Test Radio Communications screen if there's an LR or SR
	// radio on either Port A or B. 
	GuiVar_CommOptionRadioTestAvailable = RADIO_TEST_there_is_a_port_device_to_test();

	// ----------

	g_COMM_OPTIONS_cursor_position_when_dialog_displayed = (UNS_32)GuiLib_ActiveCursorFieldNo;

	// ----------

	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		// 6/5/2015 ajv : If we've never entered the screen before, the last cursor position will be
		// uninitialized at 0. In that case, set it to the top item in the list.
		if( g_COMM_OPTIONS_last_cursor_position == 0 )
		{
			if( GuiVar_CommOptionCloudCommTestAvailable )
			{
				g_COMM_OPTIONS_last_cursor_position = CP_COMM_OPTIONS_TEST_CLOUD_COMMUNICATIONS;
			}
			else if( GuiVar_CommOptionRadioTestAvailable )
			{
				g_COMM_OPTIONS_last_cursor_position = CP_COMM_OPTIONS_TEST_RADIO_COMMUNICATIONS;
			}
			else if( GuiVar_CommOptionPortAInstalled == (true) )
			{
				g_COMM_OPTIONS_last_cursor_position = CP_COMM_OPTIONS_PORT_A_PROGRAMMING;
			}
			else if( GuiVar_CommOptionPortBInstalled == (true) )
			{
				g_COMM_OPTIONS_last_cursor_position = CP_COMM_OPTIONS_PORT_B_PROGRAMMING;
			}
			else if( GuiVar_IsAHub == (true) )
			{
				g_COMM_OPTIONS_last_cursor_position = CP_COMM_OPTIONS_COMMUNICATIONS_HUB;
			}
			else
			{
				g_COMM_OPTIONS_last_cursor_position = CP_COMM_OPTIONS_SERIAL_PORT_STATS;
			}
		}

		lcursor_to_select = g_COMM_OPTIONS_last_cursor_position;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	g_COMM_OPTIONS_dialog_visible = (true);

	GuiLib_ShowScreen( GuiStruct_dlgCommOptions_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void COMM_OPTIONS_draw_dialog( const BOOL_32 pcomplete_redraw )
{
	DISPLAY_EVENT_STRUCT	lde;

	// ----------
	
	// 6/30/2017 rmd : If we are returning to this screen AFTER a device exchange, do the
	// required task maintenance to allow the tasks to resume their functionality.
	if( in_device_exchange_hammer )
	{
		task_control_EXIT_device_exchange_hammer();
	}
	
	// ----------
	
	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void*)&FDTO_COMM_OPTIONS_draw_dialog;
	lde._06_u32_argument1 = pcomplete_redraw;
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
extern void COMM_OPTIONS_process_dialog( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;


	switch( pkey_event.keycode )
	{
		case KEY_HELP:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_COMM_OPTIONS_TEST_CLOUD_COMMUNICATIONS:
					// 6/5/2015 ajv : Retain the last cursor position the user was on so they go back to it when
					// they open the dialog again.
					g_COMM_OPTIONS_last_cursor_position = GuiLib_ActiveCursorFieldNo;

					GuiVar_MenuScreenToShow = CP_COMM_OPTIONS_TEST_CLOUD_COMMUNICATIONS;

					lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
					lde._02_menu = SCREEN_MENU_SETUP;
					lde._03_structure_to_draw = GuiStruct_scrCommTest_0;
					lde._04_func_ptr = (void *)&FDTO_COMM_TEST_draw_screen;
					lde._06_u32_argument1 = (true);
					lde._07_u32_argument2 = pkey_event.keycode;
					lde.key_process_func_ptr = COMM_TEST_process_screen;
					Change_Screen( &lde );

					g_COMM_OPTIONS_dialog_visible = (false);

					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_SELECT:
			// 6/5/2015 ajv : Retain the last cursor position the user was on so they go back to it when
			// they open the dialog again.
			g_COMM_OPTIONS_last_cursor_position = GuiLib_ActiveCursorFieldNo;

			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_COMM_OPTIONS_TEST_CLOUD_COMMUNICATIONS:
					GuiVar_MenuScreenToShow = CP_COMM_OPTIONS_TEST_CLOUD_COMMUNICATIONS;

					lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
					lde._02_menu = SCREEN_MENU_SETUP;
					lde._03_structure_to_draw = GuiStruct_scrCommTest_0;
					lde._04_func_ptr = (void *)&FDTO_COMM_TEST_draw_screen;
					lde._06_u32_argument1 = (true);
					lde._07_u32_argument2 = pkey_event.keycode;
					lde.key_process_func_ptr = COMM_TEST_process_screen;
					Change_Screen( &lde );

					g_COMM_OPTIONS_dialog_visible = (false);

					break;

				case CP_COMM_OPTIONS_TEST_RADIO_COMMUNICATIONS:
					GuiVar_MenuScreenToShow = CP_COMM_OPTIONS_TEST_RADIO_COMMUNICATIONS;

					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._02_menu = SCREEN_MENU_SETUP;
					lde._03_structure_to_draw = GuiStruct_scrRadioTest_0;
					lde._04_func_ptr = (void *)&FDTO_RADIO_TEST_draw_radio_comm_test_screen;
					lde._06_u32_argument1 = (true);
					lde._07_u32_argument2 = 0;
					lde.key_process_func_ptr = RADIO_TEST_process_radio_comm_test_screen;
					Change_Screen( &lde );

					g_COMM_OPTIONS_dialog_visible = (false);

					break;

				case CP_COMM_OPTIONS_PORT_A_PROGRAMMING:

					switch( config_c.port_A_device_index )
					{
						case COMM_DEVICE_EN_UDS1100:
							task_control_ENTER_device_exchange_hammer();
							
							lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
							lde._02_menu = SCREEN_MENU_SETUP;
							lde._03_structure_to_draw = GuiStruct_scrENProgramming_0;
							lde._04_func_ptr = (void *)&FDTO_EN_PROGRAMMING_draw_screen;
							lde._06_u32_argument1 = (true);
							lde._07_u32_argument2 = UPORT_A;
							lde.key_process_func_ptr = EN_PROGRAMMING_process_screen;
							Change_Screen( &lde );

							g_COMM_OPTIONS_dialog_visible = (false);

							break;

						case COMM_DEVICE_WEN_WIBOX:
							task_control_ENTER_device_exchange_hammer();
							
							lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
							lde._02_menu = SCREEN_MENU_SETUP;
							lde._03_structure_to_draw = GuiStruct_scrWENProgramming_0;
							lde._04_func_ptr = (void *)&FDTO_WEN_PROGRAMMING_draw_screen;
							lde._06_u32_argument1 = (true);
							lde._07_u32_argument2 = UPORT_A;
							lde.key_process_func_ptr = WEN_PROGRAMMING_process_screen;
							Change_Screen( &lde );

							g_COMM_OPTIONS_dialog_visible = (false);
							break;

						case COMM_DEVICE_WEN_PREMIERWAVE:
							task_control_ENTER_device_exchange_hammer();
							
							lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
							lde._02_menu = SCREEN_MENU_SETUP;
							lde._03_structure_to_draw = GuiStruct_scrWENProgramming_0;
							lde._04_func_ptr = (void *)&FDTO_WEN_PROGRAMMING_draw_screen;
							lde._06_u32_argument1 = (true);
							lde._07_u32_argument2 = UPORT_A;
							lde.key_process_func_ptr = WEN_PROGRAMMING_process_screen;
							Change_Screen( &lde );

							g_COMM_OPTIONS_dialog_visible = (false);
							break;

						case COMM_DEVICE_LR_FREEWAVE:
							task_control_ENTER_device_exchange_hammer();
							
							lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
							lde._02_menu = SCREEN_MENU_SETUP;
							lde._03_structure_to_draw = GuiStruct_scrLRProgramming_LRS_0;
							lde._04_func_ptr = (void *)&FDTO_LR_PROGRAMMING_lrs_draw_screen;
							lde._06_u32_argument1 = (true);
							lde._07_u32_argument2 = (UPORT_A);
							lde.key_process_func_ptr = LR_PROGRAMMING_lrs_process_screen;
							Change_Screen( &lde );

							g_COMM_OPTIONS_dialog_visible = (false);
							break;

						case COMM_DEVICE_LR_RAVEON:
							task_control_ENTER_device_exchange_hammer();

							lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
							lde._02_menu = SCREEN_MENU_SETUP;
							lde._03_structure_to_draw = GuiStruct_scrLRProgramming_Raveon_0;
							lde._04_func_ptr = (void *)&FDTO_LR_PROGRAMMING_raveon_draw_screen;
							lde._06_u32_argument1 = (true);
							lde._07_u32_argument2 = (UPORT_A);//8/29/2017 BE: Note although an argument, this is really FIXED! to UPORT_A, because 
																//it reflects the cursor positon at the time of selection, which is port-A radios!!!
							lde.key_process_func_ptr = LR_PROGRAMMING_raveon_process_screen;
							Change_Screen( &lde );

							g_COMM_OPTIONS_dialog_visible = (false);
							break;

						case COMM_DEVICE_SR_FREEWAVE:
							task_control_ENTER_device_exchange_hammer();
							
							// 3/2/2015 mpd : Added SR Support.
							lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
							lde._02_menu = SCREEN_MENU_SETUP;
							lde._03_structure_to_draw = GuiStruct_scrSRProgramming_0;
							lde._04_func_ptr = (void *)&FDTO_SR_PROGRAMMING_draw_screen;
							lde._06_u32_argument1 = (true);
							lde._07_u32_argument2 = (SR_PORT_A);
							lde.key_process_func_ptr = SR_PROGRAMMING_process_screen;
							Change_Screen( &lde );

							g_COMM_OPTIONS_dialog_visible = (false);
							break;

						default:
							// 3/13/2014 ajv : This should never happens since the menu item only exists if the index is
							// something other than NONE.
							bad_key_beep();
							
							// 6/6/2017 rmd : Well it turns out it did happen when we were sort of in the middle of
							// Raveon LR programming. Code was circulating that allowed the menu option to be pushed
							// resulting in the cics mode change, and this bad_key_beep. In this case we MUST get the
							// cics mode back to normal otherwise the controller will never communicate again!
							task_control_EXIT_device_exchange_hammer();
							
					}
					break;

				case CP_COMM_OPTIONS_PORT_B_PROGRAMMING:
					// 2/28/2017 ajv : Port B only supports SR and LR devices.
					switch( config_c.port_B_device_index )
					{
						case COMM_DEVICE_LR_FREEWAVE:
							task_control_ENTER_device_exchange_hammer();
							
							lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
							lde._02_menu = SCREEN_MENU_SETUP;
							lde._03_structure_to_draw = GuiStruct_scrLRProgramming_LRS_0;
							lde._04_func_ptr = (void *)&FDTO_LR_PROGRAMMING_lrs_draw_screen;
							lde._06_u32_argument1 = (true);
							lde._07_u32_argument2 = (UPORT_B);//8/29/2017 BE: Note although an argument, this is really FIXED! to UPORT_B, because 
																//it reflects the cursor positon at the time of selection, which is: a port-B radio!!!
							lde.key_process_func_ptr = LR_PROGRAMMING_lrs_process_screen;
							Change_Screen( &lde );

							g_COMM_OPTIONS_dialog_visible = (false);
							break;

						case COMM_DEVICE_LR_RAVEON:
							task_control_ENTER_device_exchange_hammer();

							lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
							lde._02_menu = SCREEN_MENU_SETUP;
							lde._03_structure_to_draw = GuiStruct_scrLRProgramming_Raveon_0;
							lde._04_func_ptr = (void *)&FDTO_LR_PROGRAMMING_raveon_draw_screen;
							lde._06_u32_argument1 = (true);
							lde._07_u32_argument2 = (UPORT_B);
							lde.key_process_func_ptr = LR_PROGRAMMING_raveon_process_screen;
							Change_Screen( &lde );

							g_COMM_OPTIONS_dialog_visible = (false);
							break;

						case COMM_DEVICE_SR_FREEWAVE:
							task_control_ENTER_device_exchange_hammer();
							
							// 3/2/2015 mpd : Added SR Support.
							lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
							lde._02_menu = SCREEN_MENU_SETUP;
							lde._03_structure_to_draw = GuiStruct_scrSRProgramming_0;
							lde._04_func_ptr = (void *)&FDTO_SR_PROGRAMMING_draw_screen;
							lde._06_u32_argument1 = (true);
							lde._07_u32_argument2 = (SR_PORT_B);
							lde.key_process_func_ptr = SR_PROGRAMMING_process_screen;
							Change_Screen( &lde );

							g_COMM_OPTIONS_dialog_visible = (false);
							break;

						default:
							// 3/13/2014 ajv : This should never happens since the menu item only exists if the index is
							// something other than NONE.
							bad_key_beep();

							// 6/6/2017 rmd : Well it turns out it did happen when we were sort of in the middle of
							// Raveon LR programming. Code was circulating that allowed the menu option to be pushed
							// resulting in the cics mode change, and this bad_key_beep. In this case we MUST get the
							// cics mode back to normal otherwise the controller will never communicate again!
							task_control_EXIT_device_exchange_hammer();
							

					}
					break;

				case CP_COMM_OPTIONS_COMMUNICATIONS_HUB:
					GuiVar_MenuScreenToShow = CP_COMM_OPTIONS_COMMUNICATIONS_HUB;

					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._02_menu = SCREEN_MENU_SETUP;
					lde._03_structure_to_draw = GuiStruct_scrHub_0;
					lde._04_func_ptr = (void *)&FDTO_HUB_draw_screen;
					lde._06_u32_argument1 = (true);
					lde.key_process_func_ptr = HUB_process_screen;
					Change_Screen( &lde );

					g_COMM_OPTIONS_dialog_visible = (false);

					break;

				case CP_COMM_OPTIONS_SERIAL_PORT_STATS:
					GuiVar_MenuScreenToShow = CP_COMM_OPTIONS_SERIAL_PORT_STATS;

					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._02_menu = SCREEN_MENU_SETUP;
					lde._03_structure_to_draw = GuiStruct_rptSerialPortActivity_0;
					lde._04_func_ptr = (void *)&FDTO_SERIAL_PORT_INFO_draw_report;
					lde._06_u32_argument1 = (true);
					lde.key_process_func_ptr = SERIAL_PORT_INFO_process_report;
					Change_Screen( &lde );

					g_COMM_OPTIONS_dialog_visible = (false);

					break;

				case CP_COMM_OPTIONS_ACTIVATE_OPTIONS:
					GuiVar_MenuScreenToShow = CP_COMM_OPTIONS_ACTIVATE_OPTIONS;

					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._02_menu = SCREEN_MENU_SETUP;
					lde._03_structure_to_draw = GuiStruct_scrActivateOptions_0;
					lde._04_func_ptr = (void *)&FDTO_ACTIVATE_OPTIONS_draw_screen;
					lde._06_u32_argument1 = (true);
					lde.key_process_func_ptr = ACTIVATE_OPTIONS_process_screen;
					Change_Screen( &lde );

					g_COMM_OPTIONS_dialog_visible = (false);

					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_C_UP:
			CURSOR_Up( (true) );
			break;

		case KEY_C_DOWN:
			CURSOR_Down( (true) );
			break;

		case KEY_BACK:
			good_key_beep();

			// 6/5/2015 ajv : Retain the last cursor position the user was on so they go back to it when
			// they open the dialog again.
			g_COMM_OPTIONS_last_cursor_position = GuiLib_ActiveCursorFieldNo;

			g_COMM_OPTIONS_dialog_visible = (false);

			// 3/13/2014 ajv : Return to the last cursor position the user was
			// on when they pressed BACK and redraw the screen to remove the
			// dialog box.
			GuiLib_ActiveCursorFieldNo = (INT_16)g_COMM_OPTIONS_cursor_position_when_dialog_displayed;

			Redraw_Screen( (false) );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

