/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strncmp
#include	<string.h>

#include	"e_station_groups.h"

#include	"e_station_selection_grid.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"change.h"

#include	"combobox.h"

#include	"configuration_controller.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"group_base_file.h"

#include	"e_keyboard.h"

#include	"irrigation_system.h"

#include	"report_data.h"

#include	"station_groups.h"

#include	"speaker.h"

#include	"watersense.h"

#include	"weather_control.h"

#include	"scrollbox.h"

#include	"moisture_sensors.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_STATION_GROUP_NAME					(0)
#define CP_STATION_GROUP_MAINLINE_ASSIGNMENT	(1)
#define	CP_STATION_GROUP_PLANT_TYPE				(2)
#define	CP_STATION_GROUP_EXPOSURE				(3)
#define	CP_STATION_GROUP_HEAD_TYPE				(4)
#define	CP_STATION_GROUP_PRECIP_RATE			(5)
#define	CP_STATION_GROUP_SOIL_TYPE				(6)
#define	CP_STATION_GROUP_SLOPE_PERCENTAGE		(7)
#define	CP_STATION_GROUP_SOIL_STORAGE_CAPACITY	(8)
#define	CP_STATION_GROUP_Kc						(9)
#define	CP_STATION_GROUP_Kc_EDIT_MONTHLIES		(10)
#define	CP_STATION_GROUP_MOISTURE_SENSOR		(11)
#define	CP_STATION_GROUP_VIEW_STATIONS			(12)
#define	CP_STATION_GROUP_ADD_STATIONS			(13)

#define CP_STATION_GROUP_Kc1					(30)
#define CP_STATION_GROUP_Kc2					(31)
#define CP_STATION_GROUP_Kc3					(32)
#define CP_STATION_GROUP_Kc4					(33)
#define CP_STATION_GROUP_Kc5					(34)
#define CP_STATION_GROUP_Kc6					(35)
#define CP_STATION_GROUP_Kc7					(36)
#define CP_STATION_GROUP_Kc8					(37)
#define CP_STATION_GROUP_Kc9					(38)
#define CP_STATION_GROUP_Kc10					(39)
#define CP_STATION_GROUP_Kc11					(40)
#define CP_STATION_GROUP_Kc12					(41)
#define CP_STATION_GROUP_SAVE_Kc				(99)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static BOOL_32	g_STATION_GROUP_editing_group;

static UNS_32	g_STATION_GROUP_previous_cursor_pos;

// ----------

static UNS_32	g_STATION_GROUP_previous_plant_type;

static UNS_32	g_STATION_GROUP_previous_head_type;

static UNS_32	g_STATION_GROUP_previous_soil_type;

static UNS_32	g_STATION_GROUP_previous_exposure;

static UNS_32	g_CROP_COEFFICIENTS_previous_cursor_pos;

// 9/22/2016 skc : Structure to support moisture sensors list. The current list of
// moisture sensors is not sorted, sensors not found during discovery are not moved to
// the end of the list. Because we don't want to show items not available, we create a
// list of indices of only the sensors marked physically_available. This same strategy
// is used in e_rain_switch.c and e_freeze_switch.c. If the list ever becomes sorted,
// then we can abandon this.
static struct {
	UNS_32 index; // into moisture sensor list
}				g_MOISTURE_SENSORS[ MOISTURE_SENSORS_MAX_NUMBER_PER_NETWORK ];

static UNS_32 	g_MOISTURE_SENSORS_count; // number of physically available sensors

static UNS_32	g_MOISTURE_SENSORS_active_line; // keep track of listbox index

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void FDTO_STATION_GROUP_return_to_menu( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void STATION_GROUP_process_group_name_changed( const UNS_32 pprev_plant_type, const UNS_32 pprev_head_type, const UNS_32 pprev_exposure )
{
	char	lgroup_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	char	lprev_default_group_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	char	lprev_default_group_name__incl_copy[ NUMBER_OF_CHARS_IN_A_NAME ];

	// ----------

	STATION_GROUP_copy_details_into_group_name( lprev_default_group_name, pprev_plant_type, pprev_head_type, pprev_exposure );

	// 11/1/2013 ajv : Make of copy of the string with "Copy" behind it to compare against as
	// well. When a new group is created, the name includes "Copy" at the end of it. This
	// ensures the name is updated to the appropriate name even if it still includes Copy in the
	// name.
	snprintf( lprev_default_group_name__incl_copy, NUMBER_OF_CHARS_IN_A_NAME, "%s %s", lprev_default_group_name, GuiLib_GetTextPtr( GuiStruct_txtCopy_0, 0 ) );

	// ----------

	// Only overwrite the existing name if it hasn't been changed by the user.
	if( (strncmp( lprev_default_group_name, GuiVar_GroupName, NUMBER_OF_CHARS_IN_A_NAME ) == 0) || (strncmp( lprev_default_group_name__incl_copy, GuiVar_GroupName, NUMBER_OF_CHARS_IN_A_NAME ) == 0) )
	{
		STATION_GROUP_copy_details_into_group_name( lgroup_name, GuiVar_StationGroupPlantType, GuiVar_StationGroupHeadType, GuiVar_StationGroupExposure );

		if( strncmp( lgroup_name, GuiVar_GroupName, NUMBER_OF_CHARS_IN_A_NAME ) != 0 )
		{
			strlcpy( GuiVar_GroupName, lgroup_name, NUMBER_OF_CHARS_IN_A_NAME );
		}
	}

	STATION_GROUP_extract_and_store_group_name_from_GuiVars();

	// 6/3/2015 ajv : Since we've change the group name, make sure update the name in the list
	// of systems as well.
	SCROLL_BOX_redraw_line( 0, g_GROUP_list_item_index );
}

/* ---------------------------------------------------------- */
extern BOOL_32 STATION_GROUP_crop_coefficients_for_this_plant_and_exposure_are_equal( const UNS_32 pplant_type, const UNS_32 pexposure )
{
	BOOL_32		rv;

	float		Kl;

	Kl = WATERSENSE_get_Kl( Ks[ pplant_type ], Kd[ pplant_type ][ STATION_GROUP_DENSITY_FACTOR_AVERAGE ], Kmc[ pplant_type ][ WATERSENSE_get_Kmc_index_based_on_exposure( pexposure ) ] );

	if( (GuiVar_StationGroupKc1 == Kl) &&
		(GuiVar_StationGroupKc2 == Kl) &&
		(GuiVar_StationGroupKc3 == Kl) &&
		(GuiVar_StationGroupKc4 == Kl) &&
		(GuiVar_StationGroupKc5 == Kl) &&
		(GuiVar_StationGroupKc6 == Kl) &&
		(GuiVar_StationGroupKc7 == Kl) &&
		(GuiVar_StationGroupKc8 == Kl) &&
		(GuiVar_StationGroupKc9 == Kl) &&
		(GuiVar_StationGroupKc10 == Kl) &&
		(GuiVar_StationGroupKc11 == Kl) &&
		(GuiVar_StationGroupKc12 == Kl) )
	{
		rv = (true);
	}
	else
	{
		rv = (false);
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_update_crop_coefficients( void )
{
	float	Kl;

	Kl = WATERSENSE_get_Kl( GuiVar_StationGroupSpeciesFactor, GuiVar_StationGroupDensityFactor, GuiVar_StationGroupMicroclimateFactor );

	GuiVar_StationGroupKc1 = Kl;
	GuiVar_StationGroupKc2 = Kl;
	GuiVar_StationGroupKc3 = Kl;
	GuiVar_StationGroupKc4 = Kl;
	GuiVar_StationGroupKc5 = Kl;
	GuiVar_StationGroupKc6 = Kl;
	GuiVar_StationGroupKc7 = Kl;
	GuiVar_StationGroupKc8 = Kl;
	GuiVar_StationGroupKc9 = Kl;
	GuiVar_StationGroupKc10 = Kl;
	GuiVar_StationGroupKc11 = Kl;
	GuiVar_StationGroupKc12 = Kl;
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_update_mainline_assignment( const UNS_32 pindex_0 )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = SYSTEM_get_group_at_this_index( pindex_0 );

	if( lsystem == NULL )
	{
		Alert_group_not_found();

		lsystem = SYSTEM_get_group_at_this_index( 0 );
	}

	strlcpy( GuiVar_SystemName, nm_GROUP_get_name( lsystem ), sizeof(GuiVar_SystemName) );

	// ----------

	GuiVar_StationGroupSystemGID = nm_GROUP_get_group_ID( lsystem );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_process_mainline_assignment( const UNS_32 pkeycode )
{
	UNS_32	lindex;

	UNS_32	llist_count;

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	llist_count = (SYSTEM_num_systems_in_use() - 1);

	lindex = SYSTEM_get_index_for_group_with_this_GID( GuiVar_StationGroupSystemGID );

	process_uns32( pkeycode, &lindex, 0, llist_count, 1, (true) );

	STATION_GROUP_update_mainline_assignment( lindex );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_process_plant_type_changed( const BOOL_32 preplace_custom_Kl, const BOOL_32 preplace_custom_soil_storage_capacity )
{
	// 2/27/2015 ajv : When functions are declared as static, the compiler may inline the
	// functions and optimize out floating-point constant literals from the calculations. This
	// can result in a division-by-0 error. In order to resolve this, we declare the constant
	// literal as volatile which tells the compile to not optimize the value.
	#define	ONE_HUNDRED		(100.0F)

	// ----------

	if( g_STATION_GROUP_previous_plant_type != GuiVar_StationGroupPlantType )
	{
		if( preplace_custom_Kl )
		{
			GuiVar_StationGroupSpeciesFactor = Ks[ GuiVar_StationGroupPlantType ];

			GuiVar_StationGroupDensityFactor = Kd[ GuiVar_StationGroupPlantType ][ STATION_GROUP_DENSITY_FACTOR_AVERAGE ];

			GuiVar_StationGroupMicroclimateFactor = Kmc[ GuiVar_StationGroupPlantType ][ WATERSENSE_get_Kmc_index_based_on_exposure( GuiVar_StationGroupExposure ) ];

			STATION_GROUP_update_crop_coefficients();
		}

		if( preplace_custom_soil_storage_capacity )
		{
			GuiVar_StationGroupRootZoneDepth = ROOT_ZONE_DEPTH[ GuiVar_StationGroupPlantType ][ GuiVar_StationGroupSoilType ];

			GuiVar_StationGroupSoilStorageCapacity = WATERSENSE_get_RZWWS( (float)(GuiVar_StationGroupAllowableDepletion / ONE_HUNDRED), GuiVar_StationGroupAvailableWater, GuiVar_StationGroupRootZoneDepth );
		}

		STATION_GROUP_process_group_name_changed( g_STATION_GROUP_previous_plant_type, GuiVar_StationGroupHeadType, GuiVar_StationGroupExposure );
	}
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_pre_process_plant_type_changed( void )
{
	if( (g_STATION_GROUP_previous_plant_type != GuiVar_StationGroupPlantType) && (!STATION_GROUP_crop_coefficients_for_this_plant_and_exposure_are_equal( g_STATION_GROUP_previous_plant_type, GuiVar_StationGroupExposure )) && (GuiVar_StationGroupSoilStorageCapacity != WATERSENSE_get_RZWWS((float)(GuiVar_StationGroupAllowableDepletion / ONE_HUNDRED), GuiVar_StationGroupAvailableWater, ROOT_ZONE_DEPTH[ g_STATION_GROUP_previous_plant_type ][ GuiVar_StationGroupSoilType ])) )
	{
		DIALOG_draw_yes_no_cancel_dialog( GuiStruct_dlgStationGroupPlantTypeAffectsKlAndRZWWS_0 );
	}
	else if( (g_STATION_GROUP_previous_plant_type != GuiVar_StationGroupPlantType) && (!STATION_GROUP_crop_coefficients_for_this_plant_and_exposure_are_equal( g_STATION_GROUP_previous_plant_type, GuiVar_StationGroupExposure )) )
	{
		DIALOG_draw_yes_no_cancel_dialog( GuiStruct_dlgStationGroupPlantTypeAffectsKl_0 );
	}
	else if( (g_STATION_GROUP_previous_plant_type != GuiVar_StationGroupPlantType) && (GuiVar_StationGroupSoilStorageCapacity != WATERSENSE_get_RZWWS((float)(GuiVar_StationGroupAllowableDepletion / ONE_HUNDRED), GuiVar_StationGroupAvailableWater, ROOT_ZONE_DEPTH[ g_STATION_GROUP_previous_plant_type ][ GuiVar_StationGroupSoilType ])) )
	{
		DIALOG_draw_yes_no_cancel_dialog( GuiStruct_dlgStationGroupPlantTypeAffectsRZWWS_0 );
	}
	else
	{
		STATION_GROUP_process_plant_type_changed( (true), (true) );
		Refresh_Screen();
	}
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_process_plant_type_changed_dialog__affects_crop_coeff( void )
{
	switch( g_DIALOG_modal_result )
	{
		case MODAL_RESULT_YES:
			STATION_GROUP_process_plant_type_changed( (true), (true) );
			break;

		case MODAL_RESULT_NO:
			STATION_GROUP_process_plant_type_changed( (false), (true) );
			break;

		case MODAL_RESULT_CANCEL:
			GuiVar_StationGroupPlantType = g_STATION_GROUP_previous_plant_type;
			break;
	}

	Refresh_Screen();
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_process_plant_type_changed_dialog__affects_crop_coeff_and_rzwws( void )
{
	switch( g_DIALOG_modal_result )
	{
		case MODAL_RESULT_YES:
			STATION_GROUP_process_plant_type_changed( (true), (true) );
			break;

		case MODAL_RESULT_NO:
			STATION_GROUP_process_plant_type_changed( (false), (false) );
			break;

		case MODAL_RESULT_CANCEL:
			GuiVar_StationGroupPlantType = g_STATION_GROUP_previous_plant_type;
			break;
	}

	Refresh_Screen();
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_process_plant_type_changed_dialog__affects_rzwws( void )
{
	switch( g_DIALOG_modal_result )
	{
		case MODAL_RESULT_YES:
			STATION_GROUP_process_plant_type_changed( (true), (true) );
			break;

		case MODAL_RESULT_NO:
			STATION_GROUP_process_plant_type_changed( (true), (false) );
			break;

		case MODAL_RESULT_CANCEL:
			GuiVar_StationGroupPlantType = g_STATION_GROUP_previous_plant_type;
			break;
	}

	Refresh_Screen();
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_process_exposure_changed( const BOOL_32 preplace_custom_crop_coeff )
{
	if( g_STATION_GROUP_previous_exposure != GuiVar_StationGroupExposure )
	{
		if( preplace_custom_crop_coeff )
		{
			GuiVar_StationGroupMicroclimateFactor = Kmc[ GuiVar_StationGroupPlantType ][ WATERSENSE_get_Kmc_index_based_on_exposure( GuiVar_StationGroupExposure ) ];

			STATION_GROUP_update_crop_coefficients();
		}

		STATION_GROUP_process_group_name_changed( GuiVar_StationGroupPlantType, GuiVar_StationGroupHeadType, g_STATION_GROUP_previous_exposure );
	}
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_pre_process_exposure_changed( void )
{
	if( (g_STATION_GROUP_previous_exposure != GuiVar_StationGroupExposure) && (!STATION_GROUP_crop_coefficients_for_this_plant_and_exposure_are_equal( GuiVar_StationGroupPlantType, g_STATION_GROUP_previous_exposure )) )
	{
		DIALOG_draw_yes_no_cancel_dialog( GuiStruct_dlgStationGroupExposureAffectsKl_0 );
	}
	else
	{
		STATION_GROUP_process_exposure_changed( (true) );
		Refresh_Screen();
	}
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_process_exposure_changed_dialog__affects_crop_coeff( void )
{
	switch( g_DIALOG_modal_result )
	{
		case MODAL_RESULT_YES:
			STATION_GROUP_process_exposure_changed( (true) );
			break;

		case MODAL_RESULT_NO:
			STATION_GROUP_process_exposure_changed( (false) );
			break;

		case MODAL_RESULT_CANCEL:
			GuiVar_StationGroupExposure = g_STATION_GROUP_previous_exposure;
			break;
	}

	Refresh_Screen();
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_process_soil_type_changed(  const BOOL_32 preplace_custom_soil_storage_capacity )
{
	// 2/27/2015 ajv : When functions are declared as static, the compiler may inline the
	// functions and optimize out floating-point constant literals from the calculations. This
	// can result in a division-by-0 error. In order to resolve this, we declare the constant
	// literal as volatile which tells the compile to not optimize the value.
	#define	ONE_HUNDRED		(100.0F)

	if( g_STATION_GROUP_previous_soil_type != GuiVar_StationGroupSoilType )
	{
		if( preplace_custom_soil_storage_capacity )
		{
			GuiVar_StationGroupAllowableDepletion = (UNS_32)(MAD[ GuiVar_StationGroupSoilType ] * 100);

			GuiVar_StationGroupAvailableWater = AW[ GuiVar_StationGroupSoilType ];

			GuiVar_StationGroupRootZoneDepth = ROOT_ZONE_DEPTH[ GuiVar_StationGroupPlantType ][ GuiVar_StationGroupSoilType ];

			GuiVar_StationGroupSoilStorageCapacity = WATERSENSE_get_RZWWS( (float)(GuiVar_StationGroupAllowableDepletion / ONE_HUNDRED), GuiVar_StationGroupAvailableWater, GuiVar_StationGroupRootZoneDepth );
		}

		GuiVar_StationGroupSoilIntakeRate = IR[ GuiVar_StationGroupSoilType ];

		GuiVar_StationGroupAllowableSurfaceAccum = ASA[ GuiVar_StationGroupSoilType ][ GuiVar_StationGroupSlopePercentage ];
	}
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_pre_process_soil_type_changed( void )
{
	if( (g_STATION_GROUP_previous_soil_type != GuiVar_StationGroupSoilType) && (GuiVar_StationGroupSoilStorageCapacity != WATERSENSE_get_RZWWS(MAD[ g_STATION_GROUP_previous_soil_type ], AW[ g_STATION_GROUP_previous_soil_type ], ROOT_ZONE_DEPTH[ GuiVar_StationGroupPlantType ][ g_STATION_GROUP_previous_soil_type ])) )
	{
		DIALOG_draw_yes_no_cancel_dialog( GuiStruct_dlgStationGroupSoilTypeAffectsRZWWS_0 );
	}
	else
	{
		STATION_GROUP_process_soil_type_changed( (true) );
		Refresh_Screen();
	}
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_process_soil_type_changed_dialog__affects_rzwws( void )
{
	switch( g_DIALOG_modal_result )
	{
		case MODAL_RESULT_YES:
			STATION_GROUP_process_soil_type_changed( (true) );
			break;

		case MODAL_RESULT_NO:
			STATION_GROUP_process_soil_type_changed( (false) );
			break;

		case MODAL_RESULT_CANCEL:
			GuiVar_StationGroupSoilType = g_STATION_GROUP_previous_soil_type;
			break;
	}

	Refresh_Screen();
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_process_slope_percentage_changed( const UNS_32 pprev_slope_percentage )
{
	if( pprev_slope_percentage != GuiVar_StationGroupSlopePercentage )
	{
		GuiVar_StationGroupAllowableSurfaceAccum = ASA[ GuiVar_StationGroupSoilType ][ GuiVar_StationGroupSlopePercentage ];
		Refresh_Screen();
	}
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_process_head_type_changed( const BOOL_32 preplace_custom_precip_rate )
{
	if( g_STATION_GROUP_previous_head_type != GuiVar_StationGroupHeadType )
	{
		if( preplace_custom_precip_rate )
		{
			GuiVar_StationGroupPrecipRate = PR[ GuiVar_StationGroupHeadType ];
		}

		STATION_GROUP_process_group_name_changed( GuiVar_StationGroupPlantType, g_STATION_GROUP_previous_head_type, GuiVar_StationGroupExposure );
	}
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_pre_process_head_type_changed( void )
{
	if( (g_STATION_GROUP_previous_head_type != GuiVar_StationGroupHeadType) && (GuiVar_StationGroupPrecipRate != PR[ g_STATION_GROUP_previous_head_type ]) )
	{
		DIALOG_draw_yes_no_cancel_dialog( GuiStruct_dlgStationGroupHeadTypeAffectsPrecip_0 );
	}
	else
	{
		STATION_GROUP_process_head_type_changed( (true) );
		Refresh_Screen();
	}
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_process_head_type_changed_dialog__affects_precip( void )
{
	switch( g_DIALOG_modal_result )
	{
		case MODAL_RESULT_YES:
			STATION_GROUP_process_head_type_changed( (true) );
			break;

		case MODAL_RESULT_NO:
			STATION_GROUP_process_head_type_changed( (false) );
			break;

		case MODAL_RESULT_CANCEL:
			GuiVar_StationGroupHeadType = g_STATION_GROUP_previous_head_type;
			break;
	}

	Refresh_Screen();
}

/* ---------------------------------------------------------- */
// pindex_0 is listbox index
static void STATION_GROUP_update_moisture_sensor_assignment( const UNS_32 pindex_0 )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lsensor;
	
	// ----------

	lsensor = NULL;
	
	if( pindex_0 == 0 )
	{
		GuiVar_StationGroupMoistureSensorDecoderSN = 0;
	}
	else
	{
		xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

		// 4/4/2016 ajv : Subtract 1 off the index since we're effectively using a 1-based index
		// (with 0 being NONE) but this function requires a 0-based index.
		lsensor = MOISTURE_SENSOR_get_group_at_this_index( g_MOISTURE_SENSORS[pindex_0 -1].index );

		if( lsensor == NULL )
		{
			Alert_group_not_found();

			GuiVar_StationGroupMoistureSensorDecoderSN = 0;
		}
		else
		{
			GuiVar_StationGroupMoistureSensorDecoderSN = MOISTURE_SENSOR_get_decoder_serial_number( lsensor );
		}

		xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
	}

	if( GuiVar_StationGroupMoistureSensorDecoderSN == 0 )
	{
		strlcpy( GuiVar_MoisSensorName, GuiLib_GetTextLanguagePtr(GuiStruct_txtNone_0, 0, GuiLib_LanguageIndex), sizeof(GuiVar_MoisSensorName) );
	}
	else
	{
		strlcpy( GuiVar_MoisSensorName, nm_GROUP_get_name( lsensor ), sizeof(GuiVar_MoisSensorName) );
	}
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_process_moisture_sensor_assignment( const UNS_32 pkeycode )
{
	UNS_32	lindex_0;

	// ----------

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	if( GuiVar_StationGroupMoistureSensorDecoderSN == 0 )
	{
		lindex_0 = 0;
	}
	else
	{
		//lindex_0 = MOISTURE_SENSOR_get_index_for_group_with_this_serial_number( GuiVar_StationGroupMoistureSensorDecoderSN );
		lindex_0 = GuiLib_ScrollBox_GetActiveLine(0, 0);
	}

	process_uns32( pkeycode, &lindex_0, 0, MOISTURE_SENSOR_get_num_of_moisture_sensors_connected(), 1, (false) );

	STATION_GROUP_update_moisture_sensor_assignment( lindex_0 );

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	// ----------

	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
static void FDTO_STATION_GROUP_show_mainline_assignment_dropdown( void )
{
	UNS_32	lgroup_index_0;

	lgroup_index_0 = SYSTEM_get_index_for_group_with_this_GID( GuiVar_StationGroupSystemGID );

	FDTO_COMBOBOX_show( GuiStruct_cbxMainlineAssignment_0, &SYSTEM_load_system_name_into_scroll_box_guivar, SYSTEM_num_systems_in_use(), lgroup_index_0 );
}

/* ---------------------------------------------------------- */
static void FDTO_STATION_GROUP_close_mainline_assignment_dropdown( void )
{
	STATION_GROUP_update_mainline_assignment( (UNS_32)GuiLib_ScrollBox_GetActiveLine(0, 0) );

	FDTO_COMBOBOX_hide();
}

/* ---------------------------------------------------------- */
/**
 * Draws the plant type dropdown window atop the main window.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user presses the <b>SELECT</b> key when the plant type is selected.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_STATION_GROUP_show_plant_type_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxPlantType_0, &FDTO_COMBOBOX_add_items, (STATION_GROUP_PLANT_TYPE_MAX + 1), GuiVar_StationGroupPlantType );
}

/* ---------------------------------------------------------- */
/**
 * Closes the plant type dropdown and saves the selected setting if requested.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the users presses SELECT or BACK while dropdown is visible.
 * 
 * @param psave_changes True if the setting should be set to the selected line;
 * otherwise, false. This is set false if the user presses SELECT, or false if
 * the user presses BACK.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_STATION_GROUP_close_plant_type_dropdown( void )
{
	g_STATION_GROUP_previous_plant_type = GuiVar_StationGroupPlantType;

	GuiVar_StationGroupPlantType = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );

	FDTO_COMBOBOX_hide();

	STATION_GROUP_pre_process_plant_type_changed();
}

/* ---------------------------------------------------------- */
static void FDTO_STATION_GROUP_show_exposure_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxExposure_0, &FDTO_COMBOBOX_add_items, (STATION_GROUP_EXPOSURE_MAX + 1), GuiVar_StationGroupExposure );
}

/* ---------------------------------------------------------- */
static void FDTO_STATION_GROUP_close_exposure_dropdown( void )
{
	g_STATION_GROUP_previous_exposure = GuiVar_StationGroupExposure;

	GuiVar_StationGroupExposure = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );

	FDTO_COMBOBOX_hide();

	STATION_GROUP_pre_process_exposure_changed();
}

/* ---------------------------------------------------------- */
static void FDTO_STATION_GROUP_show_soil_type_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxSoilType_0, &FDTO_COMBOBOX_add_items, (STATION_GROUP_SOIL_TYPE_MAX + 1), GuiVar_StationGroupSoilType);
}

/* ---------------------------------------------------------- */
static void FDTO_STATION_GROUP_close_soil_type_dropdown( void )
{
	g_STATION_GROUP_previous_soil_type = GuiVar_StationGroupSoilType;

	GuiVar_StationGroupSoilType = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );

	FDTO_COMBOBOX_hide();

	STATION_GROUP_pre_process_soil_type_changed();
}

/* ---------------------------------------------------------- */
static void FDTO_STATION_GROUP_show_slope_percentage_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxSlopePercentage_0, &FDTO_COMBOBOX_add_items, (STATION_GROUP_SLOPE_PERCENTAGE_MAX + 1), GuiVar_StationGroupSlopePercentage );
}

/* ---------------------------------------------------------- */
static void FDTO_STATION_GROUP_close_slope_percentage_dropdown( void )
{
	UNS_32	lprev_setting;

	lprev_setting = GuiVar_StationGroupSlopePercentage;

	GuiVar_StationGroupSlopePercentage = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );

	STATION_GROUP_process_slope_percentage_changed( lprev_setting );

	FDTO_COMBOBOX_hide();
}

/* ---------------------------------------------------------- */
static void FDTO_STATION_GROUP_show_head_type_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxHeadType_0, &FDTO_COMBOBOX_add_items, (STATION_GROUP_HEAD_TYPE_MAX + 1), GuiVar_StationGroupHeadType );
}

/* ---------------------------------------------------------- */
static void FDTO_STATION_GROUP_close_head_type_dropdown( void )
{
	g_STATION_GROUP_previous_head_type = GuiVar_StationGroupHeadType;

	GuiVar_StationGroupHeadType = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );

	FDTO_COMBOBOX_hide();

	STATION_GROUP_pre_process_head_type_changed();
}

/* ---------------------------------------------------------- */
/**
 * Processes the PLUS and MINUS keys to change a particular month's crop
 * coefficient value.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkeycode The key that was pressed.
 * 
 * @param pkc_guivar_ptr A pointer to the appropriate Kc easyGUI variable.
 *
 * @author 7/2/2012 AdrianusV
 *
 * @revisions (none)
 */
static void STATION_GROUP_process_crop_coefficient( const UNS_32 pkeycode, float *pkc_guivar_ptr )
{
	process_fl( pkeycode, pkc_guivar_ptr, (float)(STATION_GROUP_CROP_COEFFICIENT_MIN_100u / 100.0), (float)(STATION_GROUP_CROP_COEFFICIENT_MAX_100u / 100.0), 0.01F, (false) );
}

/* ---------------------------------------------------------- */
static void FDTO_CROP_COEFFICIENTS_draw_dialog( void )
{
	GuiLib_ShowScreen( GuiStruct_dlgCropCoefficients_0, CP_STATION_GROUP_Kc1, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
static void CROP_COEFFICIENTS_process_dialog( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_STATION_GROUP_SAVE_Kc:
					good_key_beep();

					GuiLib_ActiveCursorFieldNo = CP_STATION_GROUP_Kc_EDIT_MONTHLIES;

					GuiVar_StationGroupKcsAreCustom = !( (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc2) &&
														 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc3) && 
														 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc4) && 
														 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc5) && 
														 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc6) && 
														 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc7) && 
														 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc8) && 
														 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc9) && 
														 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc10) && 
														 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc11) && 
														 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc12) );

					Redraw_Screen( (false) );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_MINUS:
		case KEY_PLUS:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_STATION_GROUP_Kc1:
					STATION_GROUP_process_crop_coefficient( pkey_event.keycode, &GuiVar_StationGroupKc1 );
					break;

				case CP_STATION_GROUP_Kc2:
					STATION_GROUP_process_crop_coefficient( pkey_event.keycode, &GuiVar_StationGroupKc2 );
					break;

				case CP_STATION_GROUP_Kc3:
					STATION_GROUP_process_crop_coefficient( pkey_event.keycode, &GuiVar_StationGroupKc3 );
					break;

				case CP_STATION_GROUP_Kc4:
					STATION_GROUP_process_crop_coefficient( pkey_event.keycode, &GuiVar_StationGroupKc4 );
					break;

				case CP_STATION_GROUP_Kc5:
					STATION_GROUP_process_crop_coefficient( pkey_event.keycode, &GuiVar_StationGroupKc5 );
					break;

				case CP_STATION_GROUP_Kc6:
					STATION_GROUP_process_crop_coefficient( pkey_event.keycode, &GuiVar_StationGroupKc6 );
					break;

				case CP_STATION_GROUP_Kc7:
					STATION_GROUP_process_crop_coefficient( pkey_event.keycode, &GuiVar_StationGroupKc7 );
					break;

				case CP_STATION_GROUP_Kc8:
					STATION_GROUP_process_crop_coefficient( pkey_event.keycode, &GuiVar_StationGroupKc8 );
					break;

				case CP_STATION_GROUP_Kc9:
					STATION_GROUP_process_crop_coefficient( pkey_event.keycode, &GuiVar_StationGroupKc9 );
					break;

				case CP_STATION_GROUP_Kc10:
					STATION_GROUP_process_crop_coefficient( pkey_event.keycode, &GuiVar_StationGroupKc10 );
					break;

				case CP_STATION_GROUP_Kc11:
					STATION_GROUP_process_crop_coefficient( pkey_event.keycode, &GuiVar_StationGroupKc11 );
					break;

				case CP_STATION_GROUP_Kc12:
					STATION_GROUP_process_crop_coefficient( pkey_event.keycode, &GuiVar_StationGroupKc12 );
					break;

				default:
					bad_key_beep();
			}
			Refresh_Screen();
			break;

		case KEY_C_UP:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_STATION_GROUP_Kc1:
				case CP_STATION_GROUP_Kc2:
				case CP_STATION_GROUP_Kc3:
				case CP_STATION_GROUP_Kc4:
				case CP_STATION_GROUP_Kc5:
				case CP_STATION_GROUP_Kc6:
					bad_key_beep();
					break;

				case CP_STATION_GROUP_Kc7:
				case CP_STATION_GROUP_Kc8:
				case CP_STATION_GROUP_Kc9:
				case CP_STATION_GROUP_Kc10:
				case CP_STATION_GROUP_Kc11:
				case CP_STATION_GROUP_Kc12:
					CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo - 6), (true) );
					break;

				case CP_STATION_GROUP_SAVE_Kc:
					CURSOR_Select( g_CROP_COEFFICIENTS_previous_cursor_pos, (true) );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_C_DOWN:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_STATION_GROUP_Kc1:
				case CP_STATION_GROUP_Kc2:
				case CP_STATION_GROUP_Kc3:
				case CP_STATION_GROUP_Kc4:
				case CP_STATION_GROUP_Kc5:
				case CP_STATION_GROUP_Kc6:
					CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo + 6), (true) );
					break;

				case CP_STATION_GROUP_Kc7:
				case CP_STATION_GROUP_Kc8:
				case CP_STATION_GROUP_Kc9:
				case CP_STATION_GROUP_Kc10:
				case CP_STATION_GROUP_Kc11:
				case CP_STATION_GROUP_Kc12:
					g_CROP_COEFFICIENTS_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;
					CURSOR_Select( CP_STATION_GROUP_SAVE_Kc, (true) );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_C_LEFT:
			CURSOR_Up( (true) );
			break;

		case KEY_C_RIGHT:
			if( GuiLib_ActiveCursorFieldNo == CP_STATION_GROUP_Kc12 )
			{
				g_CROP_COEFFICIENTS_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;
			}
			CURSOR_Down( (true) );
			break;

		case KEY_BACK:
			good_key_beep();

			GuiLib_ActiveCursorFieldNo = CP_STATION_GROUP_Kc_EDIT_MONTHLIES;

			GuiVar_StationGroupKcsAreCustom = !( (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc2) &&
												 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc3) && 
												 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc4) && 
												 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc5) && 
												 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc6) && 
												 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc7) && 
												 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc8) && 
												 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc9) && 
												 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc10) && 
												 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc11) && 
												 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc12) );

			Redraw_Screen( (false) );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

static void MOISTURE_SENSOR_load_sensor_name_into_scroll_box_guivar( const INT_16 pindex_1_i16 )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lsensor;

	UNS_32 idx;

	// ----------

	if( pindex_1_i16 == 0 )
	{
		// 4/4/2016 ajv : An index of 0 means there is no station assigned.
		strlcpy( GuiVar_itmGroupName, GuiLib_GetTextLanguagePtr(GuiStruct_txtNone_0, 0, GuiLib_LanguageIndex), NUMBER_OF_CHARS_IN_A_NAME );
	}
	else
	{
		xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

		// 4/4/2016 ajv : Subtract 1 from the index since it's 1-based, but the list expects it to
		// be 0-based.
		// 9/23/2016 skc : Get index into the real moisture sensor list.
		idx = g_MOISTURE_SENSORS[pindex_1_i16 - 1].index;

		lsensor = MOISTURE_SENSOR_get_group_at_this_index( idx );

		//if( nm_OnList(&moisture_sensor_group_list_hdr, lsensor) == (true) ) 
		if( lsensor != NULL)
		{
			strlcpy( GuiVar_itmGroupName, nm_GROUP_get_name( lsensor ), NUMBER_OF_CHARS_IN_A_NAME );
		}
		else
		{
			Alert_group_not_found();
		}

		xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
	}
}

/* ---------------------------------------------------------- */
static void FDTO_STATION_GROUP_show_moisture_sensor_assignment_dropdown( void )
{
	UNS_32	lgroup_index_0;
	UNS_32 i;
	MOISTURE_SENSOR_GROUP_STRUCT *psensor;

	// ----------

	if( GuiVar_StationGroupMoistureSensorDecoderSN == 0 )
	{
		lgroup_index_0 = 0;
	}
	else
	{
		lgroup_index_0 = MOISTURE_SENSOR_get_index_for_group_with_this_serial_number( GuiVar_StationGroupMoistureSensorDecoderSN );
	}

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	// 9/22/2016 skc : populate the list of moisture sensor indices used to draw the listbox
	g_MOISTURE_SENSORS_count = 0;
	g_MOISTURE_SENSORS_active_line = 0;
	for( i = 0; i < MOISTURE_SENSOR_get_list_count(); i++ )
	{
		psensor = MOISTURE_SENSOR_get_group_at_this_index( i );
		if(  MOISTURE_SENSOR_get_physically_available(psensor) )
		{
			// prevent out of bounds insertions
			if( g_MOISTURE_SENSORS_count < MOISTURE_SENSORS_MAX_NUMBER_PER_NETWORK )
			{
				if( i == lgroup_index_0 )
				{
					g_MOISTURE_SENSORS_active_line = g_MOISTURE_SENSORS_count;
				}

				// save index of the real list, i.e. we have a list of indices
				g_MOISTURE_SENSORS[ g_MOISTURE_SENSORS_count ].index = i;
				g_MOISTURE_SENSORS_count++;
			}
			else
			{
				Alert_Message_va("Too many moisture sensors: %d\n", MOISTURE_SENSOR_get_num_of_moisture_sensors_connected());
				break; // no point in looping anymore
			}
		}
	}

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	// 4/4/2016 ajv : Always add 1 to the count when displaying the scroll box because we need
	// to add "NONE" to the top of the list.
	FDTO_COMBOBOX_show( GuiStruct_cbxMoistureSensorAssignment_0, 
						&MOISTURE_SENSOR_load_sensor_name_into_scroll_box_guivar, 
						(MOISTURE_SENSOR_get_num_of_moisture_sensors_connected() + 1),  
						lgroup_index_0 ); 
}

/* ---------------------------------------------------------- */
static void FDTO_STATION_GROUP_close_moisture_sensor_assignment_dropdown( void )
{
	STATION_GROUP_update_moisture_sensor_assignment( (UNS_32)GuiLib_ScrollBox_GetActiveLine(0, 0) );

	FDTO_COMBOBOX_hide();
}

/* ---------------------------------------------------------- */
/**
 * Creates a new group.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user selects "Add New" or assigns a station to a new group.
 * 
 * @param passign_current_station_to_new_group True if the currently selected
 * station should be assigned to the newly created group; otherwise, false. This
 * is set true when the user removes a station from a group and assigns it to a
 * new group.
 * 
 * @param pshow_new_group True if the new group should be displayed, otherwise,
 * false. This is set true when the user selects "Add New", but remains false
 * when removing a station from a group and assigning it to a new group.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void STATION_GROUP_add_new_group( void )
{
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// 10/31/2013 ajv : Since we don't want to redraw the screen until AFTER the
	// copy is complete, pass in NULLs for the draw routines.
	nm_GROUP_create_new_group_from_UI( (void*)&nm_STATION_GROUP_create_new_group, NULL );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed on the Station Groups screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void STATION_GROUP_process_group( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	UNS_32	lprev_slope;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_dlgKeyboard_0:
			if( ((pkey_event.keycode == KEY_BACK) || ((pkey_event.keycode == KEY_SELECT) && (GuiLib_ActiveCursorFieldNo == 49 /*CP_QWERTY_KEYBOARD_OK*/))) )
			{
				STATION_GROUP_extract_and_store_group_name_from_GuiVars();
			}

			KEYBOARD_process_key( pkey_event, &FDTO_STATION_GROUP_draw_menu );
			break;

		case GuiStruct_dlgStationGroupPlantTypeAffectsKl_0:
			DIALOG_process_yes_no_cancel_dialog( pkey_event, (void *)&STATION_GROUP_process_plant_type_changed_dialog__affects_crop_coeff );
			break;

		case GuiStruct_dlgStationGroupPlantTypeAffectsKlAndRZWWS_0:
			DIALOG_process_yes_no_cancel_dialog( pkey_event, (void *)&STATION_GROUP_process_plant_type_changed_dialog__affects_crop_coeff_and_rzwws );
			break;

		case GuiStruct_dlgStationGroupPlantTypeAffectsRZWWS_0:
			DIALOG_process_yes_no_cancel_dialog( pkey_event, (void *)&STATION_GROUP_process_plant_type_changed_dialog__affects_rzwws );
			break;

		case GuiStruct_dlgStationGroupSoilTypeAffectsRZWWS_0:
			DIALOG_process_yes_no_cancel_dialog( pkey_event, (void *)&STATION_GROUP_process_soil_type_changed_dialog__affects_rzwws );
			break;

		case GuiStruct_dlgStationGroupHeadTypeAffectsPrecip_0:
			DIALOG_process_yes_no_cancel_dialog( pkey_event, (void *)&STATION_GROUP_process_head_type_changed_dialog__affects_precip );
			break;

		case GuiStruct_dlgStationGroupExposureAffectsKl_0:
			DIALOG_process_yes_no_cancel_dialog( pkey_event, (void *)&STATION_GROUP_process_exposure_changed_dialog__affects_crop_coeff );
			break;

		case GuiStruct_cbxMainlineAssignment_0:
			if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
			{
				good_key_beep();

				lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
				lde._04_func_ptr = FDTO_STATION_GROUP_close_mainline_assignment_dropdown;
				Display_Post_Command( &lde );
			}
			else
			{
				COMBO_BOX_key_press( pkey_event.keycode, NULL );
			}
			break;

		case GuiStruct_cbxPlantType_0:
			if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
			{
				good_key_beep();

				lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
				lde._04_func_ptr = FDTO_STATION_GROUP_close_plant_type_dropdown;
				Display_Post_Command( &lde );
			}
			else
			{
				COMBO_BOX_key_press( pkey_event.keycode, NULL );
			}
			break;

		case GuiStruct_cbxExposure_0:
			if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
			{
				good_key_beep();

				lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
				lde._04_func_ptr = FDTO_STATION_GROUP_close_exposure_dropdown;
				Display_Post_Command( &lde );
			}
			else
			{
				COMBO_BOX_key_press( pkey_event.keycode, NULL );
			}
			break;

		case GuiStruct_cbxSoilType_0:
			if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
			{
				good_key_beep();

				lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
				lde._04_func_ptr = FDTO_STATION_GROUP_close_soil_type_dropdown;
				Display_Post_Command( &lde );
			}
			else
			{
				COMBO_BOX_key_press( pkey_event.keycode, NULL );
			}
			break;

		case GuiStruct_cbxSlopePercentage_0:
			if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
			{
				good_key_beep();

				lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
				lde._04_func_ptr = FDTO_STATION_GROUP_close_slope_percentage_dropdown;
				Display_Post_Command( &lde );
			}
			else
			{
				COMBO_BOX_key_press( pkey_event.keycode, NULL );
			}
			break;

		case GuiStruct_cbxHeadType_0:
			if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
			{
				good_key_beep();

				lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
				lde._04_func_ptr = FDTO_STATION_GROUP_close_head_type_dropdown;
				Display_Post_Command( &lde );
			}
			else
			{
				COMBO_BOX_key_press( pkey_event.keycode, NULL );
			}
			break;

		case GuiStruct_dlgCropCoefficients_0:
			CROP_COEFFICIENTS_process_dialog( pkey_event );
			break;

		case GuiStruct_cbxMoistureSensorAssignment_0:
			if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
			{
				good_key_beep();

				lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
				lde._04_func_ptr = FDTO_STATION_GROUP_close_moisture_sensor_assignment_dropdown;
				Display_Post_Command( &lde );
			}
			else
			{
				COMBO_BOX_key_press( pkey_event.keycode, NULL );
			}
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_STATION_GROUP_NAME:
							GROUP_process_show_keyboard( 155, 25 );
							break;

						case CP_STATION_GROUP_MAINLINE_ASSIGNMENT:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_STATION_GROUP_show_mainline_assignment_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_STATION_GROUP_PLANT_TYPE:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_STATION_GROUP_show_plant_type_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_STATION_GROUP_EXPOSURE:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_STATION_GROUP_show_exposure_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_STATION_GROUP_SOIL_TYPE:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_STATION_GROUP_show_soil_type_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_STATION_GROUP_SLOPE_PERCENTAGE:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_STATION_GROUP_show_slope_percentage_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_STATION_GROUP_HEAD_TYPE:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_STATION_GROUP_show_head_type_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_STATION_GROUP_Kc_EDIT_MONTHLIES:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_CROP_COEFFICIENTS_draw_dialog;
							Display_Post_Command( &lde );
							break;

						case CP_STATION_GROUP_MOISTURE_SENSOR:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_STATION_GROUP_show_moisture_sensor_assignment_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_STATION_GROUP_VIEW_STATIONS:
						case CP_STATION_GROUP_ADD_STATIONS:
							// 11/4/2014 ajv : Since we're leaving the screen, make sure we save any changes the user
							// has made. Otherwise, they will be lost.
							STATION_GROUP_extract_and_store_changes_from_GuiVars();

							lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
							lde._03_structure_to_draw = GuiStruct_scrViewStations_0;
							lde._04_func_ptr = (void *)&FDTO_STATION_SELECTION_GRID_draw_screen;
							lde.key_process_func_ptr = STATION_SELECTION_GRID_process_screen;
							lde._06_u32_argument1 = (true);
							lde._07_u32_argument2 = (GuiLib_ActiveCursorFieldNo == CP_STATION_GROUP_VIEW_STATIONS);
							lde._08_screen_to_draw = GuiVar_MenuScreenToShow;
							Change_Screen( &lde );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_STATION_GROUP_MAINLINE_ASSIGNMENT:
							if( (SYSTEM_num_systems_in_use() > 1) || (GuiVar_StationGroupSystemGID == 0) )
							{
								STATION_GROUP_process_mainline_assignment( pkey_event.keycode );
							}
							else
							{
								bad_key_beep();
							}
							break;

						case CP_STATION_GROUP_PLANT_TYPE:
							g_STATION_GROUP_previous_plant_type = GuiVar_StationGroupPlantType;

							process_uns32( pkey_event.keycode, &GuiVar_StationGroupPlantType, STATION_GROUP_PLANT_TYPE_MIN, STATION_GROUP_PLANT_TYPE_MAX, 1, (true) );

							STATION_GROUP_pre_process_plant_type_changed();
							break;

						case CP_STATION_GROUP_EXPOSURE:
							g_STATION_GROUP_previous_exposure = GuiVar_StationGroupExposure;

							process_uns32( pkey_event.keycode, &GuiVar_StationGroupExposure, STATION_GROUP_EXPOSURE_MIN, STATION_GROUP_EXPOSURE_MAX, 1, (true) );

							STATION_GROUP_pre_process_exposure_changed();
							break;

						case CP_STATION_GROUP_SOIL_TYPE:
							g_STATION_GROUP_previous_soil_type = GuiVar_StationGroupSoilType;

							process_uns32( pkey_event.keycode, &GuiVar_StationGroupSoilType, STATION_GROUP_SOIL_TYPE_MIN, STATION_GROUP_SOIL_TYPE_MAX, 1, (true) );

							STATION_GROUP_pre_process_soil_type_changed();
							break;

						case CP_STATION_GROUP_SLOPE_PERCENTAGE:
							lprev_slope = GuiVar_StationGroupSlopePercentage;

							process_uns32( pkey_event.keycode, &GuiVar_StationGroupSlopePercentage, STATION_GROUP_SLOPE_PERCENTAGE_MIN, STATION_GROUP_SLOPE_PERCENTAGE_MAX, 1, (false) );

							STATION_GROUP_process_slope_percentage_changed( lprev_slope );
							break;

						case CP_STATION_GROUP_SOIL_STORAGE_CAPACITY:
							process_fl( pkey_event.keycode, &GuiVar_StationGroupSoilStorageCapacity, (float)(STATION_GROUP_SOIL_STORAGE_CAPACITY_MIN_100u / 100), (float)(STATION_GROUP_SOIL_STORAGE_CAPACITY_MAX_100u / 100), 0.01F, (false) );
							break;

						case CP_STATION_GROUP_HEAD_TYPE:
							g_STATION_GROUP_previous_head_type = GuiVar_StationGroupHeadType;

							process_uns32( pkey_event.keycode, &GuiVar_StationGroupHeadType, STATION_GROUP_HEAD_TYPE_MIN, STATION_GROUP_HEAD_TYPE_MAX, 1, (true) );

							STATION_GROUP_pre_process_head_type_changed();
							break;

						case CP_STATION_GROUP_PRECIP_RATE:
							process_fl( pkey_event.keycode, &GuiVar_StationGroupPrecipRate, (float)(STATION_GROUP_PRECIP_RATE_MIN_100000u / 100000.0), (float)(STATION_GROUP_PRECIP_RATE_MAX_100000u / 100000.0), 0.01F, (false) );
							break;

						case CP_STATION_GROUP_Kc:
							STATION_GROUP_process_crop_coefficient( pkey_event.keycode, &GuiVar_StationGroupKc1 );

							// 3/15/2016 ajv : Since the user is editting the shared Kc value, set all of the monthly
							// values to match.
							GuiVar_StationGroupKc2 = GuiVar_StationGroupKc1;
							GuiVar_StationGroupKc3 = GuiVar_StationGroupKc1;
							GuiVar_StationGroupKc4 = GuiVar_StationGroupKc1;
							GuiVar_StationGroupKc5 = GuiVar_StationGroupKc1;
							GuiVar_StationGroupKc6 = GuiVar_StationGroupKc1;
							GuiVar_StationGroupKc7 = GuiVar_StationGroupKc1;
							GuiVar_StationGroupKc8 = GuiVar_StationGroupKc1;
							GuiVar_StationGroupKc9 = GuiVar_StationGroupKc1;
							GuiVar_StationGroupKc10 = GuiVar_StationGroupKc1;
							GuiVar_StationGroupKc11 = GuiVar_StationGroupKc1;
							GuiVar_StationGroupKc12 = GuiVar_StationGroupKc1;
							break;

						case CP_STATION_GROUP_MOISTURE_SENSOR:
							// 4/4/2016 ajv : Since we're using the list count + 1 (for index 0 - NONE), we only have to
							// check to see whether there's at least one item in the list.
							if( MOISTURE_SENSOR_get_list_count() > 0 )
							{
								STATION_GROUP_process_moisture_sensor_assignment( pkey_event.keycode );
							}
							else
							{
								bad_key_beep();
							}
							break;

						default:
							bad_key_beep();
					}
					Refresh_Screen();
					break;

				case KEY_NEXT:
				case KEY_PREV:
					GROUP_process_NEXT_and_PREV( pkey_event.keycode, STATION_GROUP_get_num_groups_in_use(), (void*)&STATION_GROUP_extract_and_store_changes_from_GuiVars, (void*)&STATION_GROUP_get_group_at_this_index, (void*)&STATION_GROUP_copy_group_into_guivars );
					break;

				case KEY_C_UP:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_STATION_GROUP_SOIL_TYPE:
						case CP_STATION_GROUP_SLOPE_PERCENTAGE:
							g_STATION_GROUP_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;
							CURSOR_Select( CP_STATION_GROUP_PRECIP_RATE, (true) );
							break;

						case CP_STATION_GROUP_SOIL_STORAGE_CAPACITY:
							if( (g_STATION_GROUP_previous_cursor_pos != CP_STATION_GROUP_SOIL_TYPE) && (g_STATION_GROUP_previous_cursor_pos != CP_STATION_GROUP_SLOPE_PERCENTAGE) )
							{
								g_STATION_GROUP_previous_cursor_pos = CP_STATION_GROUP_SOIL_TYPE;
							}
							CURSOR_Select( g_STATION_GROUP_previous_cursor_pos, (true) );
							break;

						case CP_STATION_GROUP_Kc:
						case CP_STATION_GROUP_Kc_EDIT_MONTHLIES:
							g_STATION_GROUP_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;
							CURSOR_Select( CP_STATION_GROUP_SOIL_STORAGE_CAPACITY, (true) );
							break;

						case CP_STATION_GROUP_MOISTURE_SENSOR:
							if( (g_STATION_GROUP_previous_cursor_pos != CP_STATION_GROUP_Kc) && (g_STATION_GROUP_previous_cursor_pos != CP_STATION_GROUP_Kc_EDIT_MONTHLIES) )
							{
								if( GuiVar_StationGroupKcsAreCustom )
								{
									g_STATION_GROUP_previous_cursor_pos = CP_STATION_GROUP_Kc_EDIT_MONTHLIES;
								}
								else
								{
									g_STATION_GROUP_previous_cursor_pos = CP_STATION_GROUP_Kc;
								}
							}
							CURSOR_Select( g_STATION_GROUP_previous_cursor_pos, (true) );
							break;

						case CP_STATION_GROUP_VIEW_STATIONS:
						case CP_STATION_GROUP_ADD_STATIONS:
							CURSOR_Select( g_STATION_GROUP_previous_cursor_pos, (true) );
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_DOWN:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_STATION_GROUP_PRECIP_RATE:
							if( (g_STATION_GROUP_previous_cursor_pos != CP_STATION_GROUP_SOIL_TYPE) && (g_STATION_GROUP_previous_cursor_pos != CP_STATION_GROUP_SLOPE_PERCENTAGE) )
							{
								g_STATION_GROUP_previous_cursor_pos = CP_STATION_GROUP_SOIL_TYPE;
							}

							CURSOR_Select( g_STATION_GROUP_previous_cursor_pos, (true) );
							break;

						case CP_STATION_GROUP_SOIL_TYPE:
						case CP_STATION_GROUP_SLOPE_PERCENTAGE:
							g_STATION_GROUP_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;
							CURSOR_Select( CP_STATION_GROUP_SOIL_STORAGE_CAPACITY, (true) );
							break;

						case CP_STATION_GROUP_SOIL_STORAGE_CAPACITY:
							if( (g_STATION_GROUP_previous_cursor_pos != CP_STATION_GROUP_Kc) && (g_STATION_GROUP_previous_cursor_pos != CP_STATION_GROUP_Kc_EDIT_MONTHLIES) )
							{
								if( GuiVar_StationGroupKcsAreCustom )
								{
									g_STATION_GROUP_previous_cursor_pos = CP_STATION_GROUP_Kc_EDIT_MONTHLIES;
								}
								else
								{
									g_STATION_GROUP_previous_cursor_pos = CP_STATION_GROUP_Kc;
								}
							}

							CURSOR_Select( g_STATION_GROUP_previous_cursor_pos, (true) );
							break;

						case CP_STATION_GROUP_Kc:
							if( GuiVar_MainMenuMoisSensorExists )
							{
								g_STATION_GROUP_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;
								CURSOR_Select( CP_STATION_GROUP_MOISTURE_SENSOR, (true) );
							}
							else
							{
								CURSOR_Select( CP_STATION_GROUP_VIEW_STATIONS, (true) );
							}
							break;

						case CP_STATION_GROUP_Kc_EDIT_MONTHLIES:
							if( GuiVar_MainMenuMoisSensorExists )
							{
								g_STATION_GROUP_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;
								CURSOR_Select( CP_STATION_GROUP_MOISTURE_SENSOR, (true) );
							}
							else
							{
								CURSOR_Select( CP_STATION_GROUP_ADD_STATIONS, (true) );
							}
							break;

						case CP_STATION_GROUP_MOISTURE_SENSOR:
							g_STATION_GROUP_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;
							CURSOR_Select( CP_STATION_GROUP_VIEW_STATIONS, (true) );
							break;

						case CP_STATION_GROUP_VIEW_STATIONS:
						case CP_STATION_GROUP_ADD_STATIONS:
							bad_key_beep();
							break;

						default:
							CURSOR_Down( (true) );
					}
					break;

				case KEY_C_LEFT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_STATION_GROUP_NAME:
							KEY_process_BACK_from_editing_screen( (void*)&FDTO_STATION_GROUP_return_to_menu );
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_BACK:
					KEY_process_BACK_from_editing_screen( (void*)&FDTO_STATION_GROUP_return_to_menu );
					break;

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Returns to the menu on the left from the editing screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user presses a key to move away from the editing screen and back
 * onto the menu.
 * 
 * @param psave_changes True if the changes should be saved; otherwise, false.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_STATION_GROUP_return_to_menu( void )
{
	g_STATION_GROUP_previous_cursor_pos = CP_STATION_GROUP_SOIL_TYPE;

	FDTO_GROUP_return_to_menu( &g_STATION_GROUP_editing_group, (void*)&STATION_GROUP_extract_and_store_changes_from_GuiVars );
}

/* ---------------------------------------------------------- */
/**
 * Draws the Station Groups menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the screen is first navigated to.
 * 
 * @param pcomplete_redraw True if the screen should be complete redrawn;
 * otherwise, false. If false, only elements on the screen are updated, not the
 * entire structure, essentially refreshing the content.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void FDTO_STATION_GROUP_draw_menu( const BOOL_32 pcomplete_redraw )
{
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	FDTO_GROUP_draw_menu( pcomplete_redraw, &g_STATION_GROUP_editing_group, STATION_GROUP_get_num_groups_in_use(), GuiStruct_scrStationGroups_0, (void *)&nm_STATION_GROUP_load_group_name_into_guivar, (void *)&STATION_GROUP_copy_group_into_guivars, (true) );

	if( g_STATION_SELECTION_GRID_user_pressed_back == (true) )
	{
		FDTO_STATION_SELECTION_GRID_redraw_calling_screen( &g_STATION_GROUP_editing_group, (CP_STATION_GROUP_VIEW_STATIONS + !GuiVar_StationSelectionGridShowOnlyStationsInThisGroup) );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed on the Station Groups menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void STATION_GROUP_process_menu( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	GROUP_process_menu( pkey_event, &g_STATION_GROUP_editing_group, STATION_GROUP_get_num_groups_in_use(), (void *)&STATION_GROUP_process_group, (void *)&STATION_GROUP_add_new_group, (void *)&STATION_GROUP_get_group_at_this_index, (void *)&STATION_GROUP_copy_group_into_guivars );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

