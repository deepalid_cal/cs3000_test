/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"r_system_report.h"

#include	"app_startup.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"report_utils.h"

#include	"system_report_data.h"

#include	"scrollbox.h"

#include	"speaker.h"





/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	SYSTEM_REPORT_AMOUNT_TO_SCROLL_HORIZONTALLY		(20)

#define SYSTEM_REPORT_MAX_PIXELS_TO_SCROLL_HORIZONTALLY	(396)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Track the previous number of system report data lines. If the number of lines
// changes between refreshes, the entire scroll box must be redrawn. Otherwise,
// we can simply update the variables displayed on the screen.
static UNS_32 g_SYSTEM_REPORT_line_count;

static UNS_32 g_SYSTEM_REPORT_index_of_system_to_show;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Redraws the scroll box on the System Report.
 *
 * @executed Executed within the context of the display processing task when the
 *  		 user changes irrigation systems.
 * 
 * @author 5/14/2012 Adrianusv
 *
 * @revisions
 *    5/14/2012 Initial release
 */
extern void FDTO_SYSTEM_REPORT_redraw_scrollbox( void )
{
	UNS_32	lcurrent_line_count;

	// 8/1/2012 rmd : Populate the title gui var. doesn't actually draw the title.
	FDTO_SYSTEM_load_system_name_into_guivar( g_SYSTEM_REPORT_index_of_system_to_show );

	lcurrent_line_count = FDTO_SYSTEM_REPORT_fill_ptrs_and_return_how_many_lines( g_SYSTEM_REPORT_index_of_system_to_show );

	// If the number of records have changed, redraw the entire scroll box.
	// Otherwise, just update the contents of what's currently on the screen.
	FDTO_SCROLL_BOX_redraw_retaining_topline( 0, lcurrent_line_count, (lcurrent_line_count != g_SYSTEM_REPORT_line_count) );

	g_SYSTEM_REPORT_line_count = lcurrent_line_count;
}

/* ---------------------------------------------------------- */
/**
 * Draws the System Report screen.
 * 
 * @executed Executed within the context of the display processing task when the
 *  		 screen is initially drawn.
 * 
 * @author 5/14/2012 Adrianusv
 *
 * @revisions
 *    5/14/2012 Initial release
 */
extern void FDTO_SYSTEM_REPORT_draw_report( const BOOL_32 pcomplete_redraw )
{
	GuiLib_ShowScreen( GuiStruct_rptSystemSummary_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );

	if( pcomplete_redraw == (true) )
	{
		FDTO_SYSTEM_load_system_name_into_guivar( g_SYSTEM_REPORT_index_of_system_to_show );
	}

	// 9/4/2012 rmd : Show the last system when the user visited the screen. The very first time
	// will be index 0.
	g_SYSTEM_REPORT_line_count = FDTO_SYSTEM_REPORT_fill_ptrs_and_return_how_many_lines( g_SYSTEM_REPORT_index_of_system_to_show );
	
	FDTO_REPORTS_draw_report( pcomplete_redraw, g_SYSTEM_REPORT_line_count, &FDTO_SYSTEM_REPORT_load_guivars_for_scroll_line );
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while on the System Report screen.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 *
 * @param pkey_event The key event to be processed.
 *
 * @author 5/14/2012 Adrianusv
 *
 * @revisions
 *    5/14/2012 Initial release
 */
extern void SYSTEM_REPORT_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	UNS_32	lkey;

	switch( pkey_event.keycode )
	{
		case KEY_NEXT:
		case KEY_PREV:
			if( pkey_event.keycode == KEY_NEXT )
			{
				lkey = KEY_PLUS;
			}
			else
			{
				lkey = KEY_MINUS;
			}

			// Take the system mutex to get the list count
			xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

			process_uns32( lkey, &g_SYSTEM_REPORT_index_of_system_to_show, 0, (SYSTEM_num_systems_in_use()-1), 1, (true) );

			xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = (void*)&FDTO_SYSTEM_REPORT_redraw_scrollbox;
			Display_Post_Command( &lde );
			break;

		default:
			REPORTS_process_report( pkey_event, SYSTEM_REPORT_AMOUNT_TO_SCROLL_HORIZONTALLY, SYSTEM_REPORT_MAX_PIXELS_TO_SCROLL_HORIZONTALLY );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

