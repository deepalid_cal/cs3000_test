
/* ---------------------------------------------------------- */

// 8/28/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"r_rt_lights.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"cursor_utils.h"

#include	"epson_rx_8025sa.h"

#include	"d_live_screens.h"

#include	"e_lights.h"

#include	"irri_lights.h"

#include	"report_utils.h"

#include	"screen_utils.h"

#include	"scrollbox.h"

#include	"speaker.h"

static UNS_32 g_REAL_TIME_LIGHTS_PreviousEnergizedCount = 0;

//#define REAL_TIME_LIGHTS_DEBUG_SCROLL_BOX

// 9/1/2015 mpd : TODO: Fix comment box.
/* ---------------------------------------------------------- */
#ifdef REAL_TIME_LIGHTS_DEBUG_SCROLL_BOX
static void REAL_TIME_LIGHTS_draw_scroll_line( INT_16 pline_index_i16 )
#else
static void REAL_TIME_LIGHTS_draw_scroll_line( const INT_16 pline_index_i16 )
#endif
{
	IRRI_LIGHTS_LIST_COMPONENT *lillc;

	DATE_TIME ldt;

	UNS_32 box_index;

	UNS_32 output_index;

	UNS_32 stop_datetime_d;

	UNS_32 stop_datetime_t;

	UNS_32 remaining_seconds;

	// Deal with extra lines for scroll box testing.
	#ifdef REAL_TIME_LIGHTS_DEBUG_SCROLL_BOX
	pline_index_i16 = ( pline_index_i16 % IRRI_LIGHTS_get_number_of_energized_lights() );
	#endif

	// 6/1/2015 ajv : See the same constant declarations in the bypass_operation.c file for
	// more explanation. We don't have the optimization problem here that is described there.
	// However as a cautionary move, we'll implement them here too. The VOLATILE constants seem
	// to allow a work around to a compiler bug. As I said better described in
	// bypass_operation.c.
	const volatile float SIXTY_SECONDS_PER_MINUTE = 60.0;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );

	lillc = IRRI_LIGHTS_get_ptr_to_energized_light( ( UNS_32 )pline_index_i16 );

	box_index       = IRRI_LIGHTS_get_box_index( ( UNS_32 )pline_index_i16 );
	output_index    = IRRI_LIGHTS_get_output_index( ( UNS_32 )pline_index_i16 );
	stop_datetime_d = IRRI_LIGHTS_get_stop_date( ( UNS_32 )pline_index_i16 );
	stop_datetime_t = IRRI_LIGHTS_get_stop_time( ( UNS_32 )pline_index_i16 );

	snprintf( GuiVar_GroupName, sizeof( GuiVar_GroupName ), "Light %c%d: %s", (box_index + 'A'), (output_index + 1), nm_GROUP_get_name( nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index( box_index, output_index ) ) );

	// There are 4 possible reasons for lights being on: Unknown, Programmed, Manual, and Mobile. Use the value 
	// in the list. The indexed structure handles the text. 
	GuiVar_LightStartReason = IRRI_LIGHTS_get_reason_on_list( ( UNS_32 )pline_index_i16 );

	// 8/31/2015 mpd : This is no good since the preserves are not synced down the chain.
	//GuiVar_LightOnMinutes = ( lights_preserves.lights[ array_index ].lrr.actual_light_seconds_on / 60 );

	// 7/14/2015 mpd : Get today's date.
	EPSON_obtain_latest_time_and_date( &ldt );

	// Is the stop date today?
	if( stop_datetime_d == ldt.D )
	{
		// Stop date is today. Simple math. Minutes remaining is displayed with a single decimel place. Use a float
		// with 6 second granularity.
		if( stop_datetime_t >= ldt.T )
		{
			remaining_seconds = ( stop_datetime_t - ldt.T );
		}
		else
		{
			// There could be some lag due to FOAL/IRRI communication at OFF time. This will keep the result from going
			// negative.
			remaining_seconds = 0;
		}

		GuiVar_LightRemainingMinutes = ( remaining_seconds / SIXTY_SECONDS_PER_MINUTE );
	}
	else
	{
		// Stop date is in the future. Remaining time is a sum of what's left of today, plus whole days, plus time 
		// on stop day. Minutes remaining is displayed with a single decimel place. Use a float with 6 second 
		// granularity.
		remaining_seconds  = ( LIGHTS_24_HOURS_IN_SECONDS - ldt.T );
		remaining_seconds += ( ( stop_datetime_d - ldt.D - 1 ) * LIGHTS_24_HOURS_IN_SECONDS );
		remaining_seconds += stop_datetime_t;
		GuiVar_LightRemainingMinutes = ( remaining_seconds / SIXTY_SECONDS_PER_MINUTE );
	}

	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
extern void FDTO_REAL_TIME_LIGHTS_draw_report( const BOOL_32 pcomplete_redraw )
{
	// 9/2/2015 mpd : The Real Time POC code this was modeled after did not update correctly for lights as the ON count 
	// varied. Modifications have been made that are similar to the STATIONS model using "FDTO_REPORTS_draw_report()". 

	GuiLib_ShowScreen( GuiStruct_rptRTLights_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );

	// We need to keep the "irri_lights" list locked down until we have finished drawing the report.  
	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );

	g_REAL_TIME_LIGHTS_PreviousEnergizedCount = IRRI_LIGHTS_populate_pointers_of_energized_lights();

	// Create extra lines for scroll box testing.
	#ifdef REAL_TIME_LIGHTS_DEBUG_SCROLL_BOX
	g_REAL_TIME_LIGHTS_PreviousEnergizedCount = ( g_REAL_TIME_LIGHTS_PreviousEnergizedCount * 8 );
	#endif

	FDTO_REPORTS_draw_report( pcomplete_redraw, g_REAL_TIME_LIGHTS_PreviousEnergizedCount, &REAL_TIME_LIGHTS_draw_scroll_line );

	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
extern void FDTO_REAL_TIME_LIGHTS_redraw_scrollbox( void )
{
	UNS_32 energized_count;

	// We need to keep the "irri_lights" list locked down until we have finished drawing the report.  
	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );

	// Get the count of energized lights and update their pointers on the list.
	energized_count = IRRI_LIGHTS_populate_pointers_of_energized_lights();

	// Create extra lines for scroll box testing.
	#ifdef REAL_TIME_LIGHTS_DEBUG_SCROLL_BOX
	energized_count = ( energized_count * 8 );
	#endif

	// If the number of light lines have changed, redraw the entire scroll box. Otherwise, just update the contents of 
	// what's currently on the screen.
	FDTO_SCROLL_BOX_redraw_retaining_topline( 0, energized_count, ( energized_count != g_REAL_TIME_LIGHTS_PreviousEnergizedCount ) );

	g_REAL_TIME_LIGHTS_PreviousEnergizedCount = energized_count;

	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
extern void REAL_TIME_LIGHTS_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		case KEY_BACK:
			GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;

			KEY_process_global_keys( pkey_event );

			if( GuiVar_MenuScreenToShow == SCREEN_MENU_DIAGNOSTICS )
			{
				// 3/16/2016 ajv : Since the user got here from the LIVE SCREENS dialog box, redraw that
				// dialog box.
				LIVE_SCREENS_draw_dialog( (true) );
			}
			break;

		default:
			REPORTS_process_report( pkey_event, 0, 0 );
	}

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

