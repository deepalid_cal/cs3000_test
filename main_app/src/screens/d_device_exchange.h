/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_D_DEVICE_EXCHANGE_H
#define _INC_D_DEVICE_EXCHANGE_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void DEVICE_EXCHANGE_draw_dialog( void );

extern void FDTO_DEVICE_EXCHANGE_update_dialog( void );

extern void FDTO_DEVICE_EXCHANGE_close_dialog( const UNS_32 pkeycode );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

