/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_E_POC_H
#define _INC_E_POC_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"

#include	"poc.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	void		*poc_ptr;

} POC_MENU_SCROLL_BOX_ITEM;

extern POC_MENU_SCROLL_BOX_ITEM	POC_MENU_items[ MAX_POCS_IN_NETWORK ];

extern UNS_32	g_POC_top_line;

extern BOOL_32	g_POC_editing_group;

extern UNS_32	g_POC_current_list_item_index;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/26/2016 skc : Add flag to not display POCs not allowed for budgets
extern UNS_32 POC_populate_pointers_of_POCs_for_display( BOOL_32 flag_not_used );

extern void POC_load_poc_name_into_guivar( const INT_16 pindex_0_i16 );

extern POC_GROUP_STRUCT *POC_get_ptr_to_physically_available_poc( const UNS_32 pindex_0 );

extern UNS_32 POC_get_menu_index_for_displayed_poc( void );

// ----------

extern void FDTO_POC_draw_menu( const BOOL_32 pcomplete_redraw );

extern void POC_process_menu( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

