/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/6/2016 ajv : Required for modf
#include	<math.h>

// 5/5/2015 mpd : Required for atoi
#include	<stdlib.h>

// 4/6/2016 ajv : Required for strlcpy
#include	<string.h>

#include	"e_numeric_keypad.h"

#include	"cal_string.h"

#include	"cs_common.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"screen_utils.h"

#include	"speaker.h"





/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_NUMERIC_KEYPAD_1				(50)
#define	CP_NUMERIC_KEYPAD_2				(51)
#define	CP_NUMERIC_KEYPAD_3				(52)
#define	CP_NUMERIC_KEYPAD_4				(53)
#define	CP_NUMERIC_KEYPAD_5				(54)
#define	CP_NUMERIC_KEYPAD_6				(55)
#define	CP_NUMERIC_KEYPAD_7				(56)
#define	CP_NUMERIC_KEYPAD_8				(57)
#define	CP_NUMERIC_KEYPAD_9				(58)
#define	CP_NUMERIC_KEYPAD_PLUS_MINUS	(59)
#define	CP_NUMERIC_KEYPAD_0				(60)
#define	CP_NUMERIC_KEYPAD_DECIMAL_POINT	(61)
#define	CP_NUMERIC_KEYPAD_OK			(98)
#define	CP_NUMERIC_KEYPAD_CLEAR			(99)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	NUMERIC_KEYPAD_TYPE_UINT32		(0)
#define	NUMERIC_KEYPAD_TYPE_INT32		(1)
#define	NUMERIC_KEYPAD_TYPE_FLOAT		(2)
#define	NUMERIC_KEYPAD_TYPE_DOUBLE		(3)

// ----------

#define	NUMERIC_KEYPAD_HIDE_DECIMAL		(0)
#define	NUMERIC_KEYPAD_SHOW_DECIMAL		(1)

// ----------

#define	NUMERIC_KEYPAD_HIDE_NEGATIVE	(0)
#define	NUMERIC_KEYPAD_SHOW_NEGATIVE	(1)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/8/2016 ajv : Track which cursor the user was on when the keypad is displayed so we can
// make sure we return them back to the same cursor when they're done.
static	UNS_32	g_NUMERIC_KEYPAD_cursor_position_when_keypad_displayed;

// 4/8/2016 ajv : Track whether the keypad is for a uint32_t, int32_t, float, or double.
static	UNS_32	g_NUMERIC_KEYPAD_type;

// 4/8/2016 ajv : Store how many digits after the decimal point the floating-point or double
// value allows.
static	BOOL_32	g_NUMERIC_KEYPAD_num_decimals_to_display;

// ----------

static	UNS_32	*ptr_to_uint32_value;
static	UNS_32	min_uint32_value;
static	UNS_32	max_uint32_value;

static	INT_32	*ptr_to_int32_value;
static	INT_32	min_int32_value;
static	INT_32	max_int32_value;

static	float	*ptr_to_float_value;
static	float	min_float_value;
static	float	max_float_value;

static	double	*ptr_to_double_value;
static	double	min_double_value;
static	double	max_double_value;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Verify the value the user has entered is within the maximum range allowed. If it exceeds
 * the maximum, set it back down to the maximum.
 * 
 * @task_execution Executed within the context of the KEY PROCESSING task when the user adds
 * another digit to the displayed value.
 * 
 * @param pplay_key_beep True if a good or bad key beep should be played upon completion of
 * the function to indicate auditory feedback after the user presses a key; otherwise,
 * false.
 *
 * @author 4/8/2016 AdrianusV
 */
static void validate_upper_range( const BOOL_32 pplay_key_beep )
{
	BOOL_32	lval_out_of_range;

	// 4/8/2016 ajv : If the value is beyond the allowed maximum or minimum range (the latter
	// only if negative values are allowed), we're going to put the value in range and play a
	// bad key beep.
	lval_out_of_range = (false);

	// ----------

	if( g_NUMERIC_KEYPAD_type == NUMERIC_KEYPAD_TYPE_UINT32 )
	{
		if( atoi(GuiVar_NumericKeypadValue) > max_uint32_value )
		{
			snprintf( GuiVar_NumericKeypadValue, sizeof(GuiVar_NumericKeypadValue), "%d", max_uint32_value );

			lval_out_of_range = (true);
		}
	}
	else if( g_NUMERIC_KEYPAD_type == NUMERIC_KEYPAD_TYPE_INT32 )
	{
		if( atoi(GuiVar_NumericKeypadValue) > max_int32_value )
		{
			snprintf( GuiVar_NumericKeypadValue, sizeof(GuiVar_NumericKeypadValue), "%d", max_int32_value );

			lval_out_of_range = (true);
		}
		else if( (strstr(GuiVar_NumericKeypadValue, "-") != NULL) && (atoi(GuiVar_NumericKeypadValue) < min_int32_value) )
		{
			snprintf( GuiVar_NumericKeypadValue, sizeof(GuiVar_NumericKeypadValue), "%d", min_int32_value );

			lval_out_of_range = (true);
		}
	}
	else if( g_NUMERIC_KEYPAD_type == NUMERIC_KEYPAD_TYPE_FLOAT )
	{
		if( atof(GuiVar_NumericKeypadValue) > max_float_value )
		{
			snprintf( GuiVar_NumericKeypadValue, sizeof(GuiVar_NumericKeypadValue), "%*.f", g_NUMERIC_KEYPAD_num_decimals_to_display, max_float_value );

			lval_out_of_range = (true);
		}
		else if( (strstr(GuiVar_NumericKeypadValue, "-") != NULL) && (atof(GuiVar_NumericKeypadValue) < min_float_value) )
		{
			snprintf( GuiVar_NumericKeypadValue, sizeof(GuiVar_NumericKeypadValue), "%*.f", g_NUMERIC_KEYPAD_num_decimals_to_display, min_float_value );

			lval_out_of_range = (true);
		}
	}
	else // ( g_NUMERIC_KEYPAD_type == NUMERIC_KEYPAD_TYPE_DOUBLE )
	{
		if( atof(GuiVar_NumericKeypadValue) > max_double_value )
		{
			snprintf( GuiVar_NumericKeypadValue, sizeof(GuiVar_NumericKeypadValue), "%*.f", g_NUMERIC_KEYPAD_num_decimals_to_display, max_double_value );

			lval_out_of_range = (true);
		}
		else if( (strstr(GuiVar_NumericKeypadValue, "-") != NULL) && (atof(GuiVar_NumericKeypadValue) < min_double_value) )
		{
			snprintf( GuiVar_NumericKeypadValue, sizeof(GuiVar_NumericKeypadValue), "%*.f", g_NUMERIC_KEYPAD_num_decimals_to_display, min_double_value );

			lval_out_of_range = (true);
		}
	}

	// ----------

	if( pplay_key_beep )
	{
		if( lval_out_of_range )
		{
			bad_key_beep();
		}
		else
		{
			good_key_beep();
		}
	}
}

/* ---------------------------------------------------------- */
/**
 * Verify the value the user has entered is within the minimum range allowed. If it is below
 * the minium, set it back up to the minimum.
 * 
 * @task_execution Executed within the context of the KEY PROCESSING task when the user
 * closes the numeric keypad.
 * 
 * @param pplay_key_beep True if a good or bad key beep should be played upon completion of
 * the function to indicate auditory feedback after the user presses a key; otherwise,
 * false.
 *
 * @author 4/8/2016 AdrianusV
 */
static void validate_lower_range( const BOOL_32 pplay_key_beep )
{
	// 4/8/2016 ajv : We've already validated that the value is within the upper-range, but now
	// validate to ensure it's within the lower range. If not, we'll correct it.
	BOOL_32	lval_out_of_range;

	lval_out_of_range = (false);

	// ----------

	if( g_NUMERIC_KEYPAD_type == NUMERIC_KEYPAD_TYPE_UINT32 )
	{
		*ptr_to_uint32_value = atoi(GuiVar_NumericKeypadValue);

		if( *ptr_to_uint32_value < min_uint32_value )
		{
			*ptr_to_uint32_value = min_uint32_value;

			lval_out_of_range = (true);
		}
	}
	else if( g_NUMERIC_KEYPAD_type == NUMERIC_KEYPAD_TYPE_INT32 )
	{
		*ptr_to_int32_value = atoi(GuiVar_NumericKeypadValue);

		if( *ptr_to_int32_value < min_int32_value )
		{
			*ptr_to_int32_value = min_int32_value;

			lval_out_of_range = (true);
		}
	}
	else if( g_NUMERIC_KEYPAD_type == NUMERIC_KEYPAD_TYPE_FLOAT )
	{
		*ptr_to_float_value = atof(GuiVar_NumericKeypadValue);

		if( *ptr_to_float_value < min_float_value )
		{
			*ptr_to_float_value = min_float_value;

			lval_out_of_range = (true);
		}
	}
	else // ( g_NUMERIC_KEYPAD_type == NUMERIC_KEYPAD_TYPE_DOUBLE )
	{
		*ptr_to_double_value = atof(GuiVar_NumericKeypadValue);

		if( *ptr_to_double_value < min_double_value )
		{
			*ptr_to_double_value = min_double_value;

			lval_out_of_range = (true);
		}
	}

	// ----------

	if( lval_out_of_range )
	{
		bad_key_beep();
	}
	else
	{
		good_key_beep();
	}
}

/* ---------------------------------------------------------- */
/**
 * Displays the numeric keypad on the screen.
 * 
 * @task_execution Executed within the context of the DISPLAY PROCESSING task after the user
 * presses the SELECT key to display the numeric keypad or when the user presses a key to
 * change the displayed value.
 * 
 * @param pcomplete_redraw True if the dialog is being drawn for the first time; otherwise,
 * false.
 *
 * @author 4/8/2016 AdrianusV
 */
static void FDTO_show_keypad( const BOOL_32 pcomplete_redraw )
{
	if( pcomplete_redraw == (true) )
	{
		GuiLib_ActiveCursorFieldNo = CP_NUMERIC_KEYPAD_OK;
	}

	GuiLib_ShowScreen( GuiStruct_dlgNumericKeypad_0, GuiLib_ActiveCursorFieldNo, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Displays the numeric keypad on the screen.
 * 
 * @task_execution Executed within the context of the DISPLAY PROCESSING task after the user
 * presses the SELECT key to display the numeric keypad or when the user presses a key to
 * change the displayed value.
 *
 * @param pcomplete_redraw True if the dialog is being drawn for the first time; otherwise,
 * false.
 * 
 * @author 4/8/2016 AdrianusV
 */
static void show_keypad( const BOOL_32 pcomplete_redraw )
{
	DISPLAY_EVENT_STRUCT	lde;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void *)&FDTO_show_keypad;
	lde._06_u32_argument1 = pcomplete_redraw;
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
/**
 * Calls the screen's draw function to hide the keypad and redraw the original screen with
 * the new value.
 * 
 * @task_execution Executed within the context of the KEY PROCESSING task when the user
 * presses BACK or selects the OK button.
 * 
 * @param pDrawFunc A pointer to the draw routine for the screen which originally called
 * this function.
 *
 * @author 4/8/2016 AdrianusV
 */
static void hide_keypad( void(*pDrawFunc)(const BOOL_32 pcomplete_redraw) )
{
	DISPLAY_EVENT_STRUCT	lde;

	// ----------
	 
	// 4/8/2016 ajv : Revert back to the cursor that was highlighted when the numeric keypad was
	// displayed.
	GuiLib_ActiveCursorFieldNo = (INT_32)g_NUMERIC_KEYPAD_cursor_position_when_keypad_displayed;

	// ----------

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void*)pDrawFunc;
	lde._06_u32_argument1 = (false);
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
static void process_up_arrow( void )
{
	if( (GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_OK) ||
		(GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_CLEAR) )
	{
		CURSOR_Select( CP_NUMERIC_KEYPAD_0, (true) );
	}
	else if( (GuiLib_ActiveCursorFieldNo >= CP_NUMERIC_KEYPAD_4) &&
			 (GuiLib_ActiveCursorFieldNo <= CP_NUMERIC_KEYPAD_DECIMAL_POINT) )
	{
		CURSOR_Select( (GuiLib_ActiveCursorFieldNo - 3), (true) );
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void process_down_arrow( void )
{
	if( (GuiLib_ActiveCursorFieldNo >= CP_NUMERIC_KEYPAD_1) &&
		(GuiLib_ActiveCursorFieldNo <= CP_NUMERIC_KEYPAD_6) )
	{
		CURSOR_Select( (GuiLib_ActiveCursorFieldNo + 3), (true) );
	}
	else if( GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_7 )
	{
		if( GuiVar_NumericKeypadShowPlusMinus )
		{
			CURSOR_Select( CP_NUMERIC_KEYPAD_PLUS_MINUS, (true) );
		}
		else
		{
			CURSOR_Select( CP_NUMERIC_KEYPAD_0, (true) );
		}
	}
	else if( GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_8 )
	{
		CURSOR_Select( CP_NUMERIC_KEYPAD_0, (true) );
	}
	else if( GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_9 )
	{
		if( GuiVar_NumericKeypadShowDecimalPoint )
		{
			CURSOR_Select( CP_NUMERIC_KEYPAD_DECIMAL_POINT, (true) );
		}
		else
		{
			CURSOR_Select( CP_NUMERIC_KEYPAD_0, (true) );
		}
	}
	else if( (GuiLib_ActiveCursorFieldNo >= CP_NUMERIC_KEYPAD_PLUS_MINUS) &&
			 (GuiLib_ActiveCursorFieldNo <= CP_NUMERIC_KEYPAD_DECIMAL_POINT) )
	{
		CURSOR_Select( CP_NUMERIC_KEYPAD_OK, (true) );
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void process_left_arrow( void )
{
	if( GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_CLEAR )
	{
		CURSOR_Select( CP_NUMERIC_KEYPAD_OK, (true) );
	}
	else if( (GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_2) ||
			 (GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_3) ||
			 (GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_5) ||
			 (GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_6) ||
			 (GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_8) ||
			 (GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_9) ||
			 (GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_DECIMAL_POINT) )
	{
		CURSOR_Up( (true) );
	}
	else if( GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_0 )
	{
		if( GuiVar_NumericKeypadShowPlusMinus )
		{
			CURSOR_Select( CP_NUMERIC_KEYPAD_PLUS_MINUS, (true) );
		}
		else
		{
			bad_key_beep();
		}
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void process_right_arrow( void )
{
	if( GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_OK )
	{
		CURSOR_Select( CP_NUMERIC_KEYPAD_CLEAR, (true) );
	}
	else if( (GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_1) ||
			 (GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_2) ||
			 (GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_4) ||
			 (GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_5) ||
			 (GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_7) ||
			 (GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_8) ||
			 (GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_PLUS_MINUS) )
	{
		CURSOR_Down( (true) );
	}
	else if( GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_0 )
	{
		if( GuiVar_NumericKeypadShowDecimalPoint )
		{
			CURSOR_Select( CP_NUMERIC_KEYPAD_DECIMAL_POINT, (true) );
		}
		else
		{
			bad_key_beep();
		}
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void process_OK_button( void(*pDrawFunc)(const BOOL_32 pcomplete_redraw) )
{
	validate_lower_range( (true) );

	// ----------

	hide_keypad( pDrawFunc );	
}

/* ---------------------------------------------------------- */
static void process_key_press( const UNS_32 pkey_pressed )
{
	BOOL_32	lallowed_to_add_digit;

	char	lnum_to_append;

	// ----------

	if( pkey_pressed == CP_NUMERIC_KEYPAD_CLEAR )
	{
		if( strncmp( GuiVar_NumericKeypadValue, "0", sizeof(GuiVar_NumericKeypadValue) ) == 0 )
		{
			bad_key_beep();
		}
		else
		{
			good_key_beep();

			if( strlen(GuiVar_NumericKeypadValue) > 1 )
			{
				strlcpy( GuiVar_NumericKeypadValue, GuiVar_NumericKeypadValue, strlen(GuiVar_NumericKeypadValue) );
			}
			else
			{
				strlcpy( GuiVar_NumericKeypadValue, "0", sizeof(GuiVar_NumericKeypadValue) );
			}
		}
	}
	else if( pkey_pressed == CP_NUMERIC_KEYPAD_PLUS_MINUS )
	{
		if( strstr(GuiVar_NumericKeypadValue, "-") == NULL )
		{
			memmove( GuiVar_NumericKeypadValue + 1, GuiVar_NumericKeypadValue, (strlen(GuiVar_NumericKeypadValue) + 1) );

			GuiVar_NumericKeypadValue[ 0 ] = '-';
		}
		else
		{
			memmove( GuiVar_NumericKeypadValue, (GuiVar_NumericKeypadValue + 1), sizeof(GuiVar_NumericKeypadValue) );
		}

		validate_upper_range( (true) );
	}
	else if( pkey_pressed == CP_NUMERIC_KEYPAD_DECIMAL_POINT )
	{
		if( strstr(GuiVar_NumericKeypadValue, ".") == NULL )
		{
			good_key_beep();

			strlcat( GuiVar_NumericKeypadValue, ".", sizeof(GuiVar_NumericKeypadValue) );
		}
		else
		{
			bad_key_beep();
		}
	}
	else
	{
		lallowed_to_add_digit = (true);

		// 4/8/2016 ajv : If entering a floating-point value and a decimal exists, only allow them
		// to enter the maximum number of decimals.
		if( strstr(GuiVar_NumericKeypadValue, ".") != NULL )
		{
			char *decimal_value;

			decimal_value = strstr( GuiVar_NumericKeypadValue, "." );

			if( (strlen(decimal_value) - 1) == g_NUMERIC_KEYPAD_num_decimals_to_display )
			{
				lallowed_to_add_digit = (false);
			}
		}

		if( lallowed_to_add_digit )
		{
			lnum_to_append = '\0';

			switch( pkey_pressed )
			{
				case CP_NUMERIC_KEYPAD_1: lnum_to_append = '1'; break;
				case CP_NUMERIC_KEYPAD_2: lnum_to_append = '2'; break;
				case CP_NUMERIC_KEYPAD_3: lnum_to_append = '3'; break;
				case CP_NUMERIC_KEYPAD_4: lnum_to_append = '4'; break;
				case CP_NUMERIC_KEYPAD_5: lnum_to_append = '5'; break;
				case CP_NUMERIC_KEYPAD_6: lnum_to_append = '6'; break;
				case CP_NUMERIC_KEYPAD_7: lnum_to_append = '7'; break;
				case CP_NUMERIC_KEYPAD_8: lnum_to_append = '8'; break;
				case CP_NUMERIC_KEYPAD_9: lnum_to_append = '9'; break;
				case CP_NUMERIC_KEYPAD_0: lnum_to_append = '0'; break;
			}

			if( strncmp(GuiVar_NumericKeypadValue, "0", sizeof(GuiVar_NumericKeypadValue)) == 0 )
			{
				snprintf( GuiVar_NumericKeypadValue, sizeof(GuiVar_NumericKeypadValue), "%c", lnum_to_append );
			}
			else
			{
				sp_strlcat( GuiVar_NumericKeypadValue, sizeof(GuiVar_NumericKeypadValue), "%c", lnum_to_append );
			}

			validate_upper_range( (true) );
		}
		else
		{
			bad_key_beep();
		}
	}

	// ----------

	// 4/6/2016 ajv : Always force a full redraw to ensure the variable and keyboard all update
	// properly.
	show_keypad( (false) );
}

/* ---------------------------------------------------------- */
/**
 * Displays the numeric keypad on the screen.
 * 
 * @task_execution Executed within the context of the DISPLAY PROCESSING task after the user
 * presses the SELECT key to display the numeric keypad.
 * 
 * @param pallow_negative True if the value is allowed to be negative; otherwise, false. This
 * determines whether to display the '+/-' key on the keypad or not.
 *
 * @param pallow_decimal True if the value is a floating-point number with at least one
 * digit after the decimal; otherwise, false. This determines whether to display the '.' key
 * on the keypad or not.
 *
 * @author 4/8/2016 AdrianusV
 */
static void init_common_globals( const UNS_32 pvalue_type, const UNS_32 pnum_decimals_to_display, const BOOL_32 pallow_negative, const BOOL_32 pallow_decimal )
{
	// 4/6/2016 ajv : Trap the current cursor position on the screen
	g_NUMERIC_KEYPAD_cursor_position_when_keypad_displayed = (UNS_32)GuiLib_ActiveCursorFieldNo;

	// ----------

	g_NUMERIC_KEYPAD_type = pvalue_type;

	g_NUMERIC_KEYPAD_num_decimals_to_display = pnum_decimals_to_display;

	// 4/8/2016 ajv : Set the easyGUI variables accordingly to hide or display the respective
	// keys.
	GuiVar_NumericKeypadShowPlusMinus = pallow_negative;

	GuiVar_NumericKeypadShowDecimalPoint = pallow_decimal;
}

/* ---------------------------------------------------------- */
/**
 * Displays a numeric keypad to allow the user to direct entry a 32-bit unsigned variable.
 * 
 * @task_execution Executed within the context of the KEY PROCESSING task when the user
 * presses SELECT on a screen to direct entry a variable.
 * 
 * @param pvalue A pointer to the value being editted.
 * 
 * @param pmin_value The minimum allowed value.
 * 
 * @param pmax_value The maximum allowed value.
 *
 * @author 4/8/2016 AdrianusV
 */
extern void NUMERIC_KEYPAD_draw_uint32_keypad( UNS_32 *pvalue, const UNS_32 pmin_value, const UNS_32 pmax_value )
{
	init_common_globals( NUMERIC_KEYPAD_TYPE_UINT32, 0, NUMERIC_KEYPAD_HIDE_DECIMAL, NUMERIC_KEYPAD_HIDE_NEGATIVE );

	// ----------

	ptr_to_uint32_value = pvalue;

	min_uint32_value = pmin_value;

	max_uint32_value = pmax_value;

	// ----------

	snprintf( GuiVar_NumericKeypadValue, sizeof(GuiVar_NumericKeypadValue), "%u", *ptr_to_uint32_value );

	// ----------

	show_keypad( (true) );
}

/* ---------------------------------------------------------- */
/**
 * Displays a numeric keypad to allow the user to direct entry a 32-bit signed variable.
 * 
 * @task_execution Executed within the context of the KEY PROCESSING task when the user
 * presses SELECT on a screen to direct entry a variable.
 * 
 * @param pvalue A pointer to the value being editted.
 * 
 * @param pmin_value The minimum allowed value.
 * 
 * @param pmax_value The maximum allowed value.
 *
 * @param pnum_decimals_to_display The number of digits allowed after the decimal. This is
 * used to control both the display and the amount of digits the user can enter.
 * 
 * @author 4/8/2016 AdrianusV
 */
extern void NUMERIC_KEYPAD_draw_int32_keypad( INT_32 *pvalue, const INT_32 pmin_value, const INT_32 pmax_value )
{
	init_common_globals( NUMERIC_KEYPAD_TYPE_INT32, 0, NUMERIC_KEYPAD_HIDE_DECIMAL, (pmin_value < 0) );

	// ----------

	ptr_to_int32_value = pvalue;

	min_int32_value = pmin_value;

	max_int32_value = pmax_value;

	// ----------

	snprintf( GuiVar_NumericKeypadValue, sizeof(GuiVar_NumericKeypadValue), "%d", *ptr_to_int32_value );

	// ----------

	show_keypad( (true) );
}

/* ---------------------------------------------------------- */
/**
 * Displays a numeric keypad to allow the user to direct entry a floating-point variable.
 * 
 * @task_execution Executed within the context of the KEY PROCESSING task when the user
 * presses SELECT on a screen to direct entry a variable.
 * 
 * @param pvalue A pointer to the value being editted.
 * 
 * @param pmin_value The minimum allowed value.
 * 
 * @param pmax_value The maximum allowed value.
 *
 * @author 4/8/2016 AdrianusV
 */
extern void NUMERIC_KEYPAD_draw_float_keypad( float *pvalue, const float pmin_value, const float pmax_value, const UNS_32 pnum_decimals_to_display )
{
	init_common_globals( NUMERIC_KEYPAD_TYPE_FLOAT, pnum_decimals_to_display, (pmin_value < 0), (pnum_decimals_to_display > 0) );

	// ----------

	ptr_to_float_value = pvalue;

	min_float_value = pmin_value;

	max_float_value = pmax_value;

	// ----------

	snprintf( GuiVar_NumericKeypadValue, sizeof(GuiVar_NumericKeypadValue), "%.*f", pnum_decimals_to_display, *ptr_to_float_value );

	// ----------

	show_keypad( (true) );
}

/* ---------------------------------------------------------- */
/**
 * Displays a numeric keypad to allow the user to direct entry a double floating-point
 * variable.
 * 
 * @task_execution Executed within the context of the KEY PROCESSING task when the user
 * presses SELECT on a screen to direct entry a variable.
 * 
 * @param pvalue A pointer to the value being editted.
 * 
 * @param pmin_value The minimum allowed value.
 * 
 * @param pmax_value The maximum allowed value.
 * 
 * @param pnum_decimals_to_display The number of digits allowed after the decimal. This is
 * used to control both the display and the amount of digits the user can enter.
 *
 * @author 4/8/2016 AdrianusV
 */
extern void NUMERIC_KEYPAD_draw_double_keypad( double *pvalue, const double pmin_value, const double pmax_value, const UNS_32 pnum_decimals_to_display )
{
	init_common_globals( NUMERIC_KEYPAD_TYPE_DOUBLE, pnum_decimals_to_display, (pmin_value < 0), (pnum_decimals_to_display > 0) );

	// ----------

	ptr_to_double_value = pvalue;

	min_double_value = pmin_value;

	max_double_value = pmax_value;

	// ----------

	snprintf( GuiVar_NumericKeypadValue, sizeof(GuiVar_NumericKeypadValue), "%.*f", pnum_decimals_to_display, *ptr_to_double_value );

	// ----------

	show_keypad( (true) );
}

/* ---------------------------------------------------------- */
extern void NUMERIC_KEYPAD_process_key( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event,
										void(*pDrawFunc)(const BOOL_32 pcomplete_redraw) )
{
	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			if( GuiLib_ActiveCursorFieldNo == CP_NUMERIC_KEYPAD_OK )
			{
				process_OK_button( pDrawFunc );
			}
			else
			{
				process_key_press( (UNS_32)GuiLib_ActiveCursorFieldNo );
			}
			break;

		case KEY_C_UP:
			process_up_arrow();
			break;

		case KEY_C_DOWN:
			process_down_arrow();
			break;

		case KEY_C_LEFT:
			process_left_arrow();
			break;

		case KEY_C_RIGHT:
			process_right_arrow();
			break;

		// 4/6/2016 ajv : Allow the +/- keys to toggle the +/-, if available
		case KEY_PLUS:
		case KEY_MINUS:
			if( GuiVar_NumericKeypadShowPlusMinus )
			{
				process_key_press( CP_NUMERIC_KEYPAD_PLUS_MINUS );
			}
			else
			{
				bad_key_beep();
			}
			break;

		case KEY_BACK:
			process_OK_button( pDrawFunc );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

