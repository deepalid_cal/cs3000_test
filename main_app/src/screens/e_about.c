/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"e_about.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"change.h"

#include	"comm_mngr.h"

#include	"configuration_controller.h"

#include	"configuration_network.h"

#include	"cursor_utils.h"

#include	"dialog.h"

#include	"e_keyboard.h"

#include	"flash_storage.h"

#include	"flowsense.h"

#include	"irri_comm.h"

#include	"m_main.h"

#include	"range_check.h"

#include	"screen_utils.h"

#include	"shared.h"

#include	"speaker.h"

#include	"tpmicro_data.h"

#include	"weather_control.h"

#include	"e_numeric_keypad.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_ABOUT_NAME				(0)
#define	CP_ABOUT_SERIAL_NUMBER		(1)
#define	CP_ABOUT_NETWORK_ID			(2)
#define	CP_ABOUT_SHOW_TP_BOARD		(3)
#define	CP_ABOUT_WM_OPTION			(4)
#define	CP_ABOUT_SSE_OPTION			(5)
#define	CP_ABOUT_SSE_D_OPTION		(6)
#define	CP_ABOUT_FL_OPTION			(7)
#define CP_ABOUT_HUB_OPTION			(8)
#define CP_ABOUT_AQUAPONICS_OPTION	(9)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	ABOUT_MS_TO_ALLOW_ACCESS_TO_SERIAL_NUMBER	(10000)

// 4/1/2016 ajv : To ensure our WaterSense-approved model number matches the controller's
// display, only show the actual model number, not the full part number
// #define ABOUT_SHOW_FULL_PART_NUMBER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/23/2014 ajv : When moving the cursor down from either the serial number or
// network ID field, track which field we came from so, when the user moves back
// up, they are returned to the same field.
static UNS_32 g_ABOUT_prev_cursor_pos;

static UNS_32 g_ABOUT_controller_index;

// 6/26/2015 mpd : FogBugz #3149. Save this status once so all subsequent ABOUT functions use the same sources for 
// display data.
static UNS_32 g_ABOUT_chain_is_down;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Sets the controller's serial number.
 * 
 * @executed Executed within the context of the key procesing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param pserial_number The serial number to assign to the controller.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *                                   generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reasons
 *                           are defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * 
 * @return UNS_32 1 if the value was changed; otherwise, 0.
 *
 * @author 3/21/2012 Adrianusv
 *
 * @revisions
 *    3/21/2012 Initial release
 */
static UNS_32 ABOUT_set_serial_number( UNS_32 pserial_number,
									   const BOOL_32 pgenerate_change_line_bool,
									   const UNS_32 preason_for_change,
									   const UNS_32 pbox_index_0 )
{
	UNS_32	rv;

	rv = 0;

	if( SHARED_set_uint32_controller( &config_c.serial_number,
									  pserial_number,
									  CONTROLLER_MINIMUM_SERIAL_NUMBER,
									  CONTROLLER_MAXIMUM_SERIAL_NUMBER,
									  CONTROLLER_MINIMUM_SERIAL_NUMBER,
									  pgenerate_change_line_bool,
									  CHANGE_CONTROLLER_SERIAL_NUMBER,
									  preason_for_change,
									  pbox_index_0,
									  (false),
									  NULL,
									  NULL,
									  0,
									  "Serial Number" ) == (true) )
	{
		rv = 1;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Stores the controller name and serial number changes. This routine should be
 * called whenever one of the About variables is changed.
 * 
 * @executed Executed within the context of the KEY PROCESSING task when the
 *  		 user leaves the screen or presses the button to set the date and
 *  		 time; within the context of the CommMngr task when a programming
 *  		 change is made from the central or handheld radio remote.
 * 
 * @param preason_for_change The reason for the change, such as keypad.
 * 
 * @param pbox_index_0 The serial number of the controller
 *                                    which initiated the change.
 *
 * @author 3/21/2012 Adrianusv
 *
 * @revisions
 *    3/21/2012 Initial release
 */
extern void ABOUT_store_changes( const UNS_32 preason_for_change, const UNS_32 pbox_index_0 )
{
	UNS_32		lnumber_of_changes;
	
	// -----------

	lnumber_of_changes = 0;

	// ----------

	// 2/6/2015 rmd : With the exception of the serial number, changes on this screen cause the
	// BOX_CONFIGURATION to be sent to the master. Which in turn triggers a re-distribution of
	// the complete chain_members structure. As well as a re-registration with the central (if a
	// change has been detected).
	//
	// When it comes to the serial number I think the operator is on their own. If this
	// controller is a standalone things may work out. But if it is a slave on a chain I'm sure
	// the operator must go to the master and manually cause a RE-SCAN his network. Cycling
	// power at the master is the easiest way to achieve this.
	if( GuiVar_AboutDebug )
	{
		lnumber_of_changes += ABOUT_set_serial_number( GuiVar_AboutSerialNumber, CHANGE_generate_change_line, preason_for_change, pbox_index_0 );

		if( GuiVar_AboutFLOption != config_c.purchased_options.option_FL )
		{
			config_c.purchased_options.option_FL = GuiVar_AboutFLOption ? 1 : 0;

			lnumber_of_changes += 1;
		}

		if( (GuiVar_AboutSSEOption != config_c.purchased_options.option_SSE) ||
			(GuiVar_AboutSSEDOption != config_c.purchased_options.option_SSE_D) )
		{
			config_c.purchased_options.option_SSE = GuiVar_AboutSSEOption ? 1 : 0;
			config_c.purchased_options.option_SSE_D = GuiVar_AboutSSEDOption ? 1 : 0;

			lnumber_of_changes += 2;
		}

		if( GuiVar_AboutHubOption != config_c.purchased_options.option_HUB )
		{
			Alert_Message_va( "Change: -HUB from %s to %s (keypad)", GetNoYesStr( config_c.purchased_options.option_HUB ), GetNoYesStr( GuiVar_AboutHubOption ) );

			config_c.purchased_options.option_HUB = GuiVar_AboutHubOption ? 1 : 0;

			lnumber_of_changes += 1;
		}

		if( GuiVar_AboutAquaponicsOption != config_c.purchased_options.option_AQUAPONICS )
		{
			Alert_Message_va( "Change: -AQUAPONICS from %s to %s (keypad)", GetNoYesStr( config_c.purchased_options.option_AQUAPONICS ), GetNoYesStr( GuiVar_AboutAquaponicsOption ) );

			config_c.purchased_options.option_AQUAPONICS = GuiVar_AboutAquaponicsOption ? 1 : 0;

			lnumber_of_changes += 1;
		}

		if( lnumber_of_changes > 0 )
		{
			// 4/23/2014 ajv : Save the changes to the controller configuration file.
			FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_CONTROLLER_CONFIGURATION, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
		}
	}

	// ----------

	lnumber_of_changes += NETWORK_CONFIG_set_controller_name( GuiVar_GroupName, FLOWSENSE_get_controller_index(), CHANGE_generate_change_line, preason_for_change, pbox_index_0, CHANGE_set_change_bits, NETWORK_CONFIG_get_change_bits_ptr( CHANGE_REASON_KEYPAD ) );

	// -----------

	if( lnumber_of_changes > 0 )
	{
		// 5/1/2014 rmd : Take the mutex to protect against us missing a change due to multiple
		// processes manipulating the send_box_configuration_to_master flag.
		xSemaphoreTakeRecursive( irri_comm_recursive_MUTEX, portMAX_DELAY );

		// 4/16/2014 rmd : And trigger the configuration change to be sent to the master when the
		// next token arrives.
		irri_comm.send_box_configuration_to_master = (true);

		xSemaphoreGiveRecursive( irri_comm_recursive_MUTEX );
	}

	// ----------

	if( GuiVar_AboutDebug )
	{
		// 9/25/2014 ajv : Finally, set the network ID (which shouldn't ever be changed here except
		// in extreme cases by Production since the comm server sets it). This will inherently cause
		// the value to be synced down the chain.
		NETWORK_CONFIG_set_network_id( GuiVar_AboutNetworkID, CHANGE_generate_change_line, preason_for_change, pbox_index_0, CHANGE_set_change_bits, NETWORK_CONFIG_get_change_bits_ptr( CHANGE_REASON_KEYPAD ) );
	}
}

/* ---------------------------------------------------------- */
/**
 * Build the full model number and loads it into the GuiVar to be displayed on
 * the About screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the About screen is displayed.
 * 
 * @param pstation_count The number of stations available to be connected to the
 * terminal boards on the controller. This is a raw count and does not take into
 * account which terminals are available. Additionall,y it does not take 2-Wire
 * stations into consideration.
 *
 * @author 3/1/2013 Adrianusv
 *
 * @revisions (none)
 */
static void ABOUT_copy_model_number_into_guivar( const UNS_32 pstation_count )
{
	GuiVar_AboutAntennaHoles = 0;

	//------------------------------------------
	// MODEL NUMBER
	//------------------------------------------ 

	strlcpy( GuiVar_AboutModelNumber, "CS3000", sizeof(GuiVar_AboutModelNumber) );

	// -----------

	if( pstation_count > 0 )
	{
		char	str_8[ 8 ];

		snprintf( str_8, sizeof(str_8), "-%d", pstation_count );

		strlcat( GuiVar_AboutModelNumber, str_8, sizeof(GuiVar_AboutModelNumber) );
	}

	if( GuiVar_AboutTP2WireTerminal )
	{
		strlcat( GuiVar_AboutModelNumber, "-2W", sizeof(GuiVar_AboutModelNumber) );
	}

	//------------------------------------------
	// COMM OPTIONS
	//------------------------------------------

	// 6/26/2015 mpd : FogBugz #3149. If the chain is down, we need to use "config_c" and "tpmicro_comm" as the 
	// sources for display data.
	if( g_ABOUT_chain_is_down )
	{
		GuiVar_CommOptionPortAIndex = config_c.port_A_device_index;
		GuiVar_CommOptionPortBIndex = config_c.port_B_device_index;
	}
	else
	{
		GuiVar_CommOptionPortAIndex = chain.members[ g_ABOUT_controller_index ].box_configuration.port_A_device_index;
		GuiVar_CommOptionPortBIndex = chain.members[ g_ABOUT_controller_index ].box_configuration.port_B_device_index;
	}

	// ----------
	
	GuiVar_CommOptionPortAInstalled = (GuiVar_CommOptionPortAIndex != COMM_DEVICE_NONE_PRESENT);

	#ifdef ABOUT_SHOW_FULL_PART_NUMBER

		switch( GuiVar_CommOptionPortAIndex )
		{
			// 6/28/2013 rmd : For the case of no device this portion of the part number string is
			// blank.
			
			case COMM_DEVICE_LR_RAVEON:
			case COMM_DEVICE_LR_FREEWAVE:
				strlcat( GuiVar_AboutModelNumber, "-LR", sizeof(GuiVar_AboutModelNumber) );
				break;

			case COMM_DEVICE_SR_FREEWAVE:
				strlcat( GuiVar_AboutModelNumber, "-SR", sizeof(GuiVar_AboutModelNumber) );
				break;

			case COMM_DEVICE_EN_UDS1100:
			case COMM_DEVICE_EN_PREMIERWAVE:
				strlcat( GuiVar_AboutModelNumber, "-EN", sizeof(GuiVar_AboutModelNumber) );
				break;

			case COMM_DEVICE_GR_AIRLINK:
			case COMM_DEVICE_GR_PREMIERWAVE:
			case COMM_DEVICE_MULTITECH_LTE:
				strlcat( GuiVar_AboutModelNumber, "-GR", sizeof(GuiVar_AboutModelNumber) );
				break;

			case COMM_DEVICE_WEN_PREMIERWAVE:
			case COMM_DEVICE_WEN_WIBOX:
				strlcat( GuiVar_AboutModelNumber, "-WEN", sizeof(GuiVar_AboutModelNumber) );
				break;
		}

	#endif

	// ----------

	GuiVar_CommOptionPortBInstalled = (GuiVar_CommOptionPortBIndex != COMM_DEVICE_NONE_PRESENT);

	#ifdef ABOUT_SHOW_FULL_PART_NUMBER

		switch( GuiVar_CommOptionPortBIndex )
		{
			// 6/28/2013 rmd : For the case of no device this portion of the part number string is
			// blank.
			
			case COMM_DEVICE_LR_RAVEON:
			case COMM_DEVICE_LR_FREEWAVE:
				strlcat( GuiVar_AboutModelNumber, "-LR", sizeof(GuiVar_AboutModelNumber) );
				break;

			case COMM_DEVICE_SR_FREEWAVE:
				strlcat( GuiVar_AboutModelNumber, "-SR", sizeof(GuiVar_AboutModelNumber) );
				break;

			case COMM_DEVICE_EN_UDS1100:
			case COMM_DEVICE_EN_PREMIERWAVE:
				strlcat( GuiVar_AboutModelNumber, "-EN", sizeof(GuiVar_AboutModelNumber) );
				break;

			case COMM_DEVICE_GR_AIRLINK:
			case COMM_DEVICE_GR_PREMIERWAVE:
			case COMM_DEVICE_MULTITECH_LTE:
				strlcat( GuiVar_AboutModelNumber, "-GR", sizeof(GuiVar_AboutModelNumber) );
				break;

			case COMM_DEVICE_WEN_PREMIERWAVE:
			case COMM_DEVICE_WEN_WIBOX:
				strlcat( GuiVar_AboutModelNumber, "-WEN", sizeof(GuiVar_AboutModelNumber) );
				break;
		}

	#endif

	// ----------
	
	if( GuiVar_AboutTPDashMCard && GuiVar_AboutTPDashMTerminal )
	{
		GuiVar_AboutMSSEOption = (GuiVar_AboutSSEOption || GuiVar_AboutSSEDOption);
		GuiVar_AboutMOption = GuiVar_AboutWMOption;
	}
	else
	{
		GuiVar_AboutMSSEOption = (false);
		GuiVar_AboutMOption = (false);
	}

	#ifdef ABOUT_SHOW_FULL_PART_NUMBER

		if( GuiVar_AboutMOption )
		{
			strlcat( GuiVar_AboutModelNumber, "-M", sizeof(GuiVar_AboutModelNumber) );
		}
		else if( GuiVar_AboutMSSEOption )
		{
			strlcat( GuiVar_AboutModelNumber, "-MSSE", sizeof(GuiVar_AboutModelNumber) );
		}

	#endif

	//------------------------------------------
	// OTHER OPTIONS
	//------------------------------------------

	GuiVar_AboutWOption = (GuiVar_AboutTPDashWCard && GuiVar_AboutTPDashWTerminal);

	GuiVar_AboutLOption = (GuiVar_AboutTPDashLCard && GuiVar_AboutTPDashLTerminal);

	#ifdef ABOUT_SHOW_FULL_PART_NUMBER

		if( GuiVar_AboutWOption )
		{
			strlcat( GuiVar_AboutModelNumber, "-W", sizeof(GuiVar_AboutModelNumber) );
		}

		if( GuiVar_AboutLOption )
		{
			strlcat( GuiVar_AboutModelNumber, "-L", sizeof(GuiVar_AboutModelNumber) );
		}

		if( GuiVar_AboutFLOption )
		{
			strlcat( GuiVar_AboutModelNumber, "-FL", sizeof(GuiVar_AboutModelNumber) );
		}

		if( GuiVar_AboutHubOption )
		{
			strlcat( GuiVar_AboutModelNumber, "-HUB", sizeof(GuiVar_AboutModelNumber) );
		}

		if( GuiVar_AboutAquaponicsOption )
		{
			strlcat( GuiVar_AboutModelNumber, "-AQ", sizeof(GuiVar_AboutModelNumber) );
		}

	#endif

	//------------------------------------------
	// ENCLOSURE
	//------------------------------------------

	// 9/10/2013 ajv : Since a controller can only be installed in one type of enclosure, use a
	// nested if-else-if statement rather than parallel if statements.
	if( GuiVar_AboutSSEOption )
	{
		strlcat( GuiVar_AboutModelNumber, "-S", sizeof(GuiVar_AboutModelNumber) );
	}
	else if( GuiVar_AboutSSEDOption )
	{
		strlcat( GuiVar_AboutModelNumber, "-SD", sizeof(GuiVar_AboutModelNumber) );
	}
	else
	{
		strlcat( GuiVar_AboutModelNumber, "-WM", sizeof(GuiVar_AboutModelNumber) );
	}

	// 6/26/2015 mpd : FogBugz #3149. Use the GuiVar for this switch expression. It work if the chain is up or down.
	switch( GuiVar_CommOptionPortAIndex )
	{
		case COMM_DEVICE_LR_RAVEON:
		case COMM_DEVICE_LR_FREEWAVE:
		case COMM_DEVICE_SR_FREEWAVE:
		case COMM_DEVICE_GR_AIRLINK:
		case COMM_DEVICE_GR_PREMIERWAVE:
		case COMM_DEVICE_WEN_PREMIERWAVE:
		case COMM_DEVICE_WEN_WIBOX:
		case COMM_DEVICE_MULTITECH_LTE:
			GuiVar_AboutAntennaHoles++;
	}

	// 6/26/2015 mpd : FogBugz #3149. Use the GuiVar for this switch expression. It work if the chain is up or down.
	switch( GuiVar_CommOptionPortBIndex )
	{
		case COMM_DEVICE_LR_RAVEON:
		case COMM_DEVICE_LR_FREEWAVE:
		case COMM_DEVICE_SR_FREEWAVE:
		case COMM_DEVICE_GR_AIRLINK:
		case COMM_DEVICE_GR_PREMIERWAVE:
		case COMM_DEVICE_WEN_PREMIERWAVE:
		case COMM_DEVICE_WEN_WIBOX:
		case COMM_DEVICE_MULTITECH_LTE:
			GuiVar_AboutAntennaHoles++;
	}

	if( GuiVar_AboutAntennaHoles > 0 )
	{
		sp_strlcat( GuiVar_AboutModelNumber, sizeof(GuiVar_AboutModelNumber), "%d", GuiVar_AboutAntennaHoles );
	}
}

/* ---------------------------------------------------------- */
static void ABOUT_build_model_number( void )
{
	WHATS_INSTALLED_STRUCT	*lwi;

	UNS_32	lstation_count;

	UNS_32	i;

	// ----------


	// 6/26/2015 mpd : FogBugz #3149. If the chain is down, we need to use "config_c" and "tpmicro_comm" as the 
	// sources for display data. Set the pointer accordingly and get the appropriate MUTEX.
	if( g_ABOUT_chain_is_down )
	{
		xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );

		lwi = &tpmicro_comm.wi_holding;
	}
	else
	{
		xSemaphoreTakeRecursive( chain_members_recursive_MUTEX, portMAX_DELAY );

		// 4/24/2014 ajv : For now, look at this particular controller's what's
		// installed structure. In the future, by passing a controller index into
		// this routine, we can also display other controller's settings if we want.
		lwi = &chain.members[ g_ABOUT_controller_index ].box_configuration.wi;
	}

	// ----------
	
	// 3/1/2013 ajv : Grab the state of each component on the TP Board.
	// These are used to build the model number as well as visually
	// represent what items are present on the board itself.
	lstation_count = 0;

	for( i = 0; i < STATION_CARD_COUNT; i++ )
	{
		if( lwi->stations[ i ].card_present && lwi->stations[ i ].tb_present )
		{
			lstation_count += STATIONS_PER_CARD;
		}
	}

	// -----------

	// 4/24/2014 ajv : If the controller does not receive a response from the TP
	// Micro on power-up, the mode will stay in TP Micro ISP. So let's use that
	// for now to determine whether a TP Micro card was found or not.
	// Note: This DOES NOT cover the case where a TP Micro suddenly stopped
	// working. Not quite sure how to handle that case yet...
	GuiVar_AboutTPMicroCard = (comm_mngr.mode != COMM_MNGR_MODE_tpmicro_isp);

	// 4/24/2014 ajv : TODO Is there a way to determine whether a fuse card is
	// actually present or not? I think it's safe to assume it's always there...
	// Except in cases where the controller is powered by an AC power source
	// rather than through a TP Board.
	GuiVar_AboutTPFuseCard = (true);

	GuiVar_AboutTPPOCCard = lwi->poc.card_present;
	GuiVar_AboutTPPOCTerminal = lwi->poc.tb_present;

	GuiVar_AboutTP2WireTerminal = lwi->two_wire_terminal_present;

	GuiVar_AboutTPSta01_08Card = lwi->stations[ 0 ].card_present;
	GuiVar_AboutTPSta01_08Terminal = lwi->stations[ 0 ].tb_present;
	GuiVar_AboutTPSta09_16Card = lwi->stations[ 1 ].card_present;
	GuiVar_AboutTPSta09_16Terminal = lwi->stations[ 1 ].tb_present;
	GuiVar_AboutTPSta17_24Card = lwi->stations[ 2 ].card_present;
	GuiVar_AboutTPSta17_24Terminal = lwi->stations[ 2 ].tb_present;
	GuiVar_AboutTPSta25_32Card = lwi->stations[ 3 ].card_present;
	GuiVar_AboutTPSta25_32Terminal = lwi->stations[ 3 ].tb_present;
	GuiVar_AboutTPSta33_40Card = lwi->stations[ 4 ].card_present;
	GuiVar_AboutTPSta33_40Terminal = lwi->stations[ 4 ].tb_present;
	GuiVar_AboutTPSta41_48Card = lwi->stations[ 5 ].card_present;
	GuiVar_AboutTPSta41_48Terminal = lwi->stations[ 5 ].tb_present;

	GuiVar_AboutTPDashLCard = lwi->lights.card_present;
	GuiVar_AboutTPDashLTerminal = lwi->lights.tb_present;

	GuiVar_AboutTPDashMCard = lwi->dash_m_card_present;
	GuiVar_AboutTPDashMTerminal = lwi->dash_m_terminal_present;

	GuiVar_AboutTPDashWCard = lwi->weather_card_present;
	GuiVar_AboutTPDashWTerminal = lwi->weather_terminal_present;

	// 6/26/2015 mpd : FogBugz #3149. If the chain is down, we need to use "config_c" and "tpmicro_comm" as the 
	// sources for display data. Release the appropriate MUTEX.
	if( g_ABOUT_chain_is_down )
	{
		xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );
	}
	else
	{
		xSemaphoreGiveRecursive( chain_members_recursive_MUTEX );
	}

	// -----------

	ABOUT_copy_model_number_into_guivar( lstation_count );
}

/* ---------------------------------------------------------- */
/**
 * Copies the controller name, serial number, and full model number into easyGUI
 * variables. This screen replaces the "Part and Serial Numbers" screen in
 * previous model controllers.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the About screen is drawn.
 *
 * @author 3/21/2012 Adrianusv
 *
 * @revisions
 *    3/1/2013 Added model number.
 */
static void ABOUT_copy_settings_into_guivars()
{
	NETWORK_CONFIG_get_controller_name( GuiVar_GroupName, sizeof(GuiVar_GroupName), g_ABOUT_controller_index );

	GuiVar_AboutNetworkID = NETWORK_CONFIG_get_network_id();

	snprintf( GuiVar_AboutVersion, sizeof(GuiVar_AboutVersion), "%s", &restart_info.main_app_code_revision_string );

	// ----------

	// 6/26/2015 mpd : FogBugz #3149. Save this status once so all subsequent ABOUT functions use the same sources for 
	// display data.
	g_ABOUT_chain_is_down = comm_mngr.chain_is_down;

	if( (my_tick_count * portTICK_RATE_MS) < ABOUT_MS_TO_ALLOW_ACCESS_TO_SERIAL_NUMBER )
	{
		GuiVar_AboutSerialNumber = config_c.serial_number;

		// 5/15/2015 ajv : These MUST BE SET HERE. If they are set in the ABOUT_build_model_number,
		// they will be overwritten when the Production edits the value.
		GuiVar_AboutFLOption = config_c.purchased_options.option_FL;
		GuiVar_AboutSSEOption = config_c.purchased_options.option_SSE;
		GuiVar_AboutSSEDOption = config_c.purchased_options.option_SSE_D;
		GuiVar_AboutWMOption = !(GuiVar_AboutSSEOption || GuiVar_AboutSSEDOption);
		GuiVar_AboutHubOption = config_c.purchased_options.option_HUB;
		GuiVar_AboutAquaponicsOption = config_c.purchased_options.option_AQUAPONICS;
	}
	else
	{
		// 6/26/2015 mpd : FogBugz #3149. If the chain is down, we need to use "config_c" and "tpmicro_comm" as the 
		// sources for display data.
		if( g_ABOUT_chain_is_down )
		{
			GuiVar_AboutSerialNumber = config_c.serial_number; 

			GuiVar_AboutFLOption = config_c.purchased_options.option_FL; 
			GuiVar_AboutSSEOption = config_c.purchased_options.option_SSE; 
			GuiVar_AboutSSEDOption = config_c.purchased_options.option_SSE_D; 
			GuiVar_AboutWMOption = !(GuiVar_AboutSSEOption || GuiVar_AboutSSEDOption);
			GuiVar_AboutHubOption = config_c.purchased_options.option_HUB;
			GuiVar_AboutAquaponicsOption = config_c.purchased_options.option_AQUAPONICS;
		}
		else
		{
			GuiVar_AboutSerialNumber = chain.members[ g_ABOUT_controller_index ].box_configuration.serial_number;

			GuiVar_AboutFLOption = chain.members[ g_ABOUT_controller_index ].box_configuration.purchased_options.option_FL;
			GuiVar_AboutSSEOption = chain.members[ g_ABOUT_controller_index ].box_configuration.purchased_options.option_SSE;
			GuiVar_AboutSSEDOption = chain.members[ g_ABOUT_controller_index ].box_configuration.purchased_options.option_SSE_D;
			GuiVar_AboutWMOption = !(GuiVar_AboutSSEOption || GuiVar_AboutSSEDOption);
			GuiVar_AboutHubOption = chain.members[ g_ABOUT_controller_index ].box_configuration.purchased_options.option_HUB;
			GuiVar_AboutAquaponicsOption = chain.members[ g_ABOUT_controller_index ].box_configuration.purchased_options.option_AQUAPONICS;
		}
	}

	// -----------

	#if defined(NDEBUG)
		// 9/23/2014 ajv : Editing the serial number and network ID is tied to the AboutDebug
		// variable. When compiled for RELEASE, these cursor positions are not editable UNLESS the
		// user reaches the screen within the first 10 seconds of the controller powering up.
		// 
		// Note that 10 seconds is extremely generous. If you hold in the down arrow on power up,
		// and quickly move to the About menu item, it takes just about 4 seconds.
		if( (my_tick_count * portTICK_RATE_MS) < ABOUT_MS_TO_ALLOW_ACCESS_TO_SERIAL_NUMBER )
		{
			GuiVar_AboutDebug = (true);
		}
		else
		{
			GuiVar_AboutDebug = (false);
		}
	#else

		// 9/23/2014 ajv : Always allow access to the serial number and
		// purchased options when running in debug.
		GuiVar_AboutDebug = (true);

	#endif

	// -----------

	ABOUT_build_model_number();
}

/* ---------------------------------------------------------- */
/**
 * @executed Executed within the context of the DISPLAY PROCESSING task.
 */
extern void FDTO_ABOUT_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		g_ABOUT_controller_index = FLOWSENSE_get_controller_index();

		ABOUT_copy_settings_into_guivars();

		lcursor_to_select = CP_ABOUT_NAME;

		g_ABOUT_prev_cursor_pos = CP_ABOUT_SERIAL_NUMBER;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrAbout_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * @executed Executed within the context of the KEY PROCESSING task.
 */
static void ABOUT_process_purchased_option( BOOL_32 *ppurchased_option_guivar_ptr )
{
	process_bool( ppurchased_option_guivar_ptr );

	ABOUT_build_model_number();

	// 11/12/2013 ajv : Redraw the screen so the changes are reflected in the Other Options in
	// the middle of the screen.
	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
/**
 * @executed Executed within the context of the KEY PROCESSING task.
 */
static void ABOUT_process_enclosure_radio_buttons( void )
{
	if( GuiLib_ActiveCursorFieldNo == CP_ABOUT_WM_OPTION )
	{
		if( !GuiVar_AboutWMOption )
		{
			good_key_beep();

			GuiVar_AboutWMOption = (true);
			GuiVar_AboutSSEOption = !GuiVar_AboutWMOption;
			GuiVar_AboutSSEDOption = !GuiVar_AboutWMOption;
		}
		else
		{
			bad_key_beep();
		}
	}
	else if( GuiLib_ActiveCursorFieldNo == CP_ABOUT_SSE_OPTION )
	{
		if( !GuiVar_AboutSSEOption )
		{
			good_key_beep();

			GuiVar_AboutSSEOption = (true);
			GuiVar_AboutWMOption = !GuiVar_AboutSSEOption;
			GuiVar_AboutSSEDOption = !GuiVar_AboutSSEOption;
		}
		else
		{
			bad_key_beep();
		}
	}
	else if( GuiLib_ActiveCursorFieldNo == CP_ABOUT_SSE_D_OPTION )
	{
		if( !GuiVar_AboutSSEDOption )
		{
			good_key_beep();

			GuiVar_AboutSSEDOption = (true);
			GuiVar_AboutWMOption = !GuiVar_AboutSSEDOption;
			GuiVar_AboutSSEOption = !GuiVar_AboutSSEDOption;
		}
		else
		{
			bad_key_beep();
		}
	}
	else
	{
		// 5/15/2015 ajv : This should never happen
		bad_key_beep();
	}

	ABOUT_build_model_number();

	// 11/12/2013 ajv : Update the variables that have changed.
	Refresh_Screen();
}

/* ---------------------------------------------------------- */
/**
 * @executed Executed within the context of the KEY PROCESSING task.
 */
extern void ABOUT_process_screen( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	UNS_32	lcur_controller_index;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_dlgKeyboard_0:
			KEYBOARD_process_key( pkey_event, &FDTO_ABOUT_draw_screen );
			break;

		case GuiStruct_dlgNumericKeypad_0:
			NUMERIC_KEYPAD_process_key( pkey_event, &FDTO_ABOUT_draw_screen );
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_NEXT:
				case KEY_PREV:
					// 5/15/2015 ajv : If NOT in DEBUG, allow the user to view other controllers in the chain
					// using NEXT and PREV.
					if( (GuiVar_AboutDebug) || (!FLOWSENSE_we_are_poafs()) )
					{
						bad_key_beep();
					}
					else
					{
						lcur_controller_index = g_ABOUT_controller_index;

						do
						{
							if( pkey_event.keycode == KEY_NEXT )
							{
								g_ABOUT_controller_index++;

								if( g_ABOUT_controller_index > (MAX_CHAIN_LENGTH - 1) )
								{
									g_ABOUT_controller_index = 0;
								}
							}
							else // if( pkey_event.keycode == KEY_PREV )
							{
								if( g_ABOUT_controller_index > 0 )
								{
									g_ABOUT_controller_index--;
								}
								else
								{
									g_ABOUT_controller_index = (MAX_CHAIN_LENGTH - 1);
								}
							}

						} while( (chain.members[ g_ABOUT_controller_index ].saw_during_the_scan == (false)) && (g_ABOUT_controller_index != lcur_controller_index) );

						if( g_ABOUT_controller_index != lcur_controller_index )
						{
							good_key_beep();

							ABOUT_copy_settings_into_guivars();

							Redraw_Screen( (false) );
						}
						else
						{
							bad_key_beep();
						}
					}
					break;

				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_ABOUT_NAME:
							good_key_beep();

							KEYBOARD_draw_keyboard( 39, 27, NUMBER_OF_CHARS_IN_A_NAME, KEYBOARD_TYPE_QWERTY );
							break;

						case CP_ABOUT_SHOW_TP_BOARD:
							good_key_beep();

							if( (GuiVar_AboutSSEOption == (true)) || (GuiVar_AboutSSEDOption == (true)) )
							{
								DIALOG_draw_ok_dialog( GuiStruct_imgTPBoard_SSE_0 );
							}
							else
							{
								DIALOG_draw_ok_dialog( GuiStruct_imgTPBoard_WM_0 );
							}
							break;

						case CP_ABOUT_SERIAL_NUMBER:
							good_key_beep();

							NUMERIC_KEYPAD_draw_uint32_keypad( &GuiVar_AboutSerialNumber, CONTROLLER_MINIMUM_SERIAL_NUMBER, CONTROLLER_MAXIMUM_SERIAL_NUMBER );
							break;

						case CP_ABOUT_NETWORK_ID:
							good_key_beep();

							NUMERIC_KEYPAD_draw_uint32_keypad( &GuiVar_AboutNetworkID, NETWORK_CONFIG_NETWORK_ID_MIN, NETWORK_CONFIG_NETWORK_ID_MAX );
							break;

						case CP_ABOUT_FL_OPTION:
							ABOUT_process_purchased_option( &GuiVar_AboutFLOption );
							break;

						case CP_ABOUT_HUB_OPTION:
							ABOUT_process_purchased_option( &GuiVar_AboutHubOption );
							break;

						case CP_ABOUT_AQUAPONICS_OPTION:
							ABOUT_process_purchased_option( &GuiVar_AboutAquaponicsOption );
							break;

						case CP_ABOUT_WM_OPTION:
						case CP_ABOUT_SSE_OPTION:
						case CP_ABOUT_SSE_D_OPTION:
							ABOUT_process_enclosure_radio_buttons();
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_HELP:
					switch ( GuiLib_ActiveCursorFieldNo )
					{
						case CP_ABOUT_NETWORK_ID:
							good_key_beep();

							// 4/8/2016 ajv : To help assist in debugging, we've introduced the ability to quickly reset
							// the network ID by pressing HELP while it's highlighted. This cursor field is only
							// available during DEBUG and for Production, so there's really no harm for the customer.
							GuiVar_AboutNetworkID = 0;

							Refresh_Screen();
							break;

						default:
							KEY_process_global_keys( pkey_event );
					}

				case KEY_MINUS:
				case KEY_PLUS:
					switch ( GuiLib_ActiveCursorFieldNo )
					{
						case CP_ABOUT_SERIAL_NUMBER:
							process_uns32( pkey_event.keycode, &GuiVar_AboutSerialNumber, CONTROLLER_MINIMUM_SERIAL_NUMBER, CONTROLLER_MAXIMUM_SERIAL_NUMBER, 1, (false) );
							break;

						case CP_ABOUT_NETWORK_ID:
							process_uns32( pkey_event.keycode, &GuiVar_AboutNetworkID, NETWORK_CONFIG_NETWORK_ID_MIN, NETWORK_CONFIG_NETWORK_ID_MAX, 1, (false) );
							break;

						default:
							bad_key_beep();
					}
					Refresh_Screen();
					break;

				case KEY_C_UP:
					// 9/15/2014 ajv : If we're not in DEBUG, allow normal cursor movement rather than custom
					// movement.
					if( GuiVar_AboutDebug )
					{
						switch ( GuiLib_ActiveCursorFieldNo )
						{
							case CP_ABOUT_SERIAL_NUMBER:
							case CP_ABOUT_NETWORK_ID:
								g_ABOUT_prev_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

								CURSOR_Select( CP_ABOUT_NAME, (true) );
								break;

							case CP_ABOUT_FL_OPTION:
							case CP_ABOUT_WM_OPTION:
								if( g_ABOUT_prev_cursor_pos >= CP_ABOUT_WM_OPTION )
								{
									g_ABOUT_prev_cursor_pos = CP_ABOUT_SERIAL_NUMBER;
								}
								CURSOR_Select( g_ABOUT_prev_cursor_pos, (true) );
								break;

							case CP_ABOUT_HUB_OPTION:
								CURSOR_Select( CP_ABOUT_FL_OPTION, (true) ); 
								break;

							default:
								CURSOR_Up( (true) );
						}
					}
					else
					{
						CURSOR_Up( (true) );
					}
					break;

				case KEY_C_LEFT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_ABOUT_SSE_OPTION:
							CURSOR_Select( CP_ABOUT_FL_OPTION, (true) );
							break;

						case CP_ABOUT_SSE_D_OPTION:
							CURSOR_Select( CP_ABOUT_HUB_OPTION, (true) );
							break;

						case CP_ABOUT_FL_OPTION:
							CURSOR_Select( CP_ABOUT_WM_OPTION, (true) );
							break;

						case CP_ABOUT_HUB_OPTION:
							CURSOR_Select( CP_ABOUT_SSE_OPTION, (true) );
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_DOWN:
					// 9/15/2014 ajv : If we're not in DEBUG, allow normal cursor movement rather than custom
					// movement.
					if( GuiVar_AboutDebug )
					{
						switch ( GuiLib_ActiveCursorFieldNo )
						{
							case CP_ABOUT_NAME:
								if( g_ABOUT_prev_cursor_pos > CP_ABOUT_NETWORK_ID )
								{
									g_ABOUT_prev_cursor_pos = CP_ABOUT_SERIAL_NUMBER;
								}
								CURSOR_Select( g_ABOUT_prev_cursor_pos, (true) );
								break;

							case CP_ABOUT_SERIAL_NUMBER:
							case CP_ABOUT_NETWORK_ID:
								g_ABOUT_prev_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

								CURSOR_Select( CP_ABOUT_WM_OPTION, (true) );
								break;

							case CP_ABOUT_SSE_D_OPTION:
							case CP_ABOUT_HUB_OPTION:
								bad_key_beep();
								break;

							case CP_ABOUT_FL_OPTION:
								CURSOR_Select( CP_ABOUT_HUB_OPTION, (true) ); 
								break;

							default:
								CURSOR_Down( (true) );
						}
					}
					else
					{
						CURSOR_Down( (true) );
					}
					break;

				case KEY_C_RIGHT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_ABOUT_WM_OPTION:
							CURSOR_Select( CP_ABOUT_FL_OPTION, (true) );
							break;

						case CP_ABOUT_SSE_OPTION:
							CURSOR_Select( CP_ABOUT_HUB_OPTION, (true) );
							break;

						case CP_ABOUT_FL_OPTION:
							CURSOR_Select( CP_ABOUT_SSE_OPTION, (true) );
							break;

						case CP_ABOUT_HUB_OPTION:
							CURSOR_Select( CP_ABOUT_SSE_D_OPTION, (true) );
							break;

						case CP_ABOUT_SSE_D_OPTION:
							bad_key_beep();
							break;

						default:
							CURSOR_Down( (true) );
					}
					
					break;

				default:
					if( pkey_event.keycode == KEY_BACK )
					{
						ABOUT_store_changes( CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index() );

						GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;
					}

					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

