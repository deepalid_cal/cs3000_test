/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"e_historical_et_setup.h"

#include	"combobox.h"

#include	"GuiLib.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"e_keyboard.h"

#include	"et_data.h"

#include	"m_main.h"

#include	"screen_utils.h"

#include	"scrollbox.h"

#include	"speaker.h"

#include	"weather_control.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_HISTORICAL_ET_SOURCE				(0)
#define	CP_HISTORICAL_ET_STATE				(1)
#define CP_HISTORICAL_ET_COUNTY				(2)
#define	CP_HISTORICAL_ET_CITY				(3)
#define	CP_HISTORICAL_ET_MAX_PERCENT_OF_ET	(4)
#define	CP_HISTORICAL_ET_USER_ET_1			(5)
#define	CP_HISTORICAL_ET_USER_ET_2			(6)
#define	CP_HISTORICAL_ET_USER_ET_3			(7)
#define	CP_HISTORICAL_ET_USER_ET_4			(8)
#define	CP_HISTORICAL_ET_USER_ET_5			(9)
#define	CP_HISTORICAL_ET_USER_ET_6			(10)
#define	CP_HISTORICAL_ET_USER_ET_7			(11)
#define	CP_HISTORICAL_ET_USER_ET_8			(12)
#define	CP_HISTORICAL_ET_USER_ET_9			(13)
#define	CP_HISTORICAL_ET_USER_ET_10			(14)
#define	CP_HISTORICAL_ET_USER_ET_11			(15)
#define	CP_HISTORICAL_ET_USER_ET_12			(16)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/*
 * The following variables are support variables used after sorting the states,
 * counties, and cities to ensure only the appropriate locations are displayed.
 */
static UNS_32 g_state_display_order[ NO_OF_STATES ];
static UNS_32 g_county_display_order[ NO_OF_COUNTIES ];
static UNS_32 g_city_display_order[ NO_OF_CITIES ];

static UNS_32 g_state_display_count;
static UNS_32 g_county_display_count;
static UNS_32 g_city_display_count;

static BOOL_32 g_counties_sorted;
static BOOL_32 g_cities_sorted;

/**
 * Stores the last cursor position within the user-entered historical ET values.
 * This ensures the user is returned to the appropriate cursor position when
 * they move out and back into the ET values.
 */
static UNS_32 g_cursor_pos_of_direct_entry_field;

UNS_32 g_HISTORICAL_ET_state_index;

UNS_32 g_HISTORICAL_ET_county_index;

UNS_32 g_HISTORICAL_ET_city_index;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static INT_32 HISTORICAL_ET_qsort_partition( char strArray[][ MAX_REFERENCE_ET_NAME_LENGTH ], const INT_32 plower_bound, const INT_32 pupper_bound )
{
	static char swapArray[ MAX_REFERENCE_ET_NAME_LENGTH ];

	static char pivot[ MAX_REFERENCE_ET_NAME_LENGTH ];

	INT_32 left;
	INT_32 right;

	left = (plower_bound + 1);
	right = pupper_bound;

	strlcpy( (char *)pivot, (char *)strArray[ plower_bound ], MAX_REFERENCE_ET_NAME_LENGTH );

	while( left <= right )
	{
		while( (left <= right) && (strncmp( (char *)strArray[ left ], (char *)pivot, MAX_REFERENCE_ET_NAME_LENGTH ) <= 0) )
		{
			left++;
		}

		while ( (left <= right) && (strncmp( (char *)strArray[ right ], (char *)pivot, MAX_REFERENCE_ET_NAME_LENGTH ) > 0) )
		{
			right--;
		}

		if( left < right )
		{
			strlcpy( (char *)swapArray, (char *)strArray[ left ], MAX_REFERENCE_ET_NAME_LENGTH );
			strlcpy( (char *)strArray[ left ], (char *)strArray[ right ], MAX_REFERENCE_ET_NAME_LENGTH );
			strlcpy( (char *)strArray[ right ], (char *)swapArray, MAX_REFERENCE_ET_NAME_LENGTH );

			++left;
			--right;
		}
	}

	strlcpy( (char *)swapArray, (char *)strArray[ plower_bound ], MAX_REFERENCE_ET_NAME_LENGTH );
	strlcpy( (char *)strArray[ plower_bound ], (char *)strArray[ right ], MAX_REFERENCE_ET_NAME_LENGTH );
	strlcpy( (char *)strArray[ right ], (char *)swapArray, MAX_REFERENCE_ET_NAME_LENGTH );

	return( right );
}

static void HISTORICAL_ET_qsort( char strArray[][ MAX_REFERENCE_ET_NAME_LENGTH ], const INT_32 plower_bound, const INT_32 pupper_bound )
{
	INT_32 split;

	if( plower_bound < pupper_bound )
	{
		split = HISTORICAL_ET_qsort_partition( strArray, plower_bound, pupper_bound );

		HISTORICAL_ET_qsort( strArray, plower_bound, split - 1 );
		HISTORICAL_ET_qsort( strArray, split + 1, pupper_bound );
	}
}

static void HISTORICAL_ET_update_et_numbers( void )
{
	if( GuiVar_HistoricalETUseYourOwn == (false) )
	{
		strlcpy( GuiVar_HistoricalETState, States[ g_HISTORICAL_ET_state_index ], sizeof(GuiVar_HistoricalETState) );
		strlcpy( GuiVar_HistoricalETCounty, Counties[ g_HISTORICAL_ET_county_index ].county, sizeof(GuiVar_HistoricalETCounty) );
		strlcpy( GuiVar_HistoricalETCity, ET_PredefinedData[ g_HISTORICAL_ET_city_index ].city, sizeof(GuiVar_HistoricalETCity) );

		GuiVar_HistoricalET1 = (float)(ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 0 ] / 100.0);
		GuiVar_HistoricalET2 = (float)(ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 1 ] / 100.0);
		GuiVar_HistoricalET3 = (float)(ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 2 ] / 100.0);
		GuiVar_HistoricalET4 = (float)(ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 3 ] / 100.0);
		GuiVar_HistoricalET5 = (float)(ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 4 ] / 100.0);
		GuiVar_HistoricalET6 = (float)(ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 5 ] / 100.0);
		GuiVar_HistoricalET7 = (float)(ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 6 ] / 100.0);
		GuiVar_HistoricalET8 = (float)(ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 7 ] / 100.0);
		GuiVar_HistoricalET9 = (float)(ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 8 ] / 100.0);
		GuiVar_HistoricalET10 = (float)(ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 9 ] / 100.0);
		GuiVar_HistoricalET11 = (float)(ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 10 ] / 100.0);
		GuiVar_HistoricalET12 = (float)(ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 11 ] / 100.0);

		Refresh_Screen();
	}
}

static void HISTORICAL_ET_sort_states( void )
{
	static char StatesCopy[ NO_OF_STATES ][ MAX_REFERENCE_ET_NAME_LENGTH ];

	UNS_32 i, j;

	for( i = 0; i < NO_OF_STATES; ++i )
	{
		strlcpy( StatesCopy[ i ], States[ i ], MAX_REFERENCE_ET_NAME_LENGTH );
	}

	HISTORICAL_ET_qsort( StatesCopy, 0, (NO_OF_STATES-1) );

	for( i = 0, g_state_display_count = 0; i < NO_OF_STATES; ++i )
	{
		for( j = 0; j < NO_OF_STATES; ++j )
		{
			if( strncmp( StatesCopy[ i ], States[ j ], MAX_REFERENCE_ET_NAME_LENGTH ) == 0 )
			{
				g_state_display_order[ g_state_display_count ] = j;

				++g_state_display_count;
			}
		}
	}

	g_counties_sorted = (false);
	g_cities_sorted = (false);
}

static void HISTORICAL_ET_sort_counties( void )
{
	static char CountiesCopy[ MAX_COUNTIES ][ MAX_REFERENCE_ET_NAME_LENGTH ];

	INT_32 sortCount;

	UNS_32 i, j;

	for( i = 0, sortCount = 0; i < NO_OF_COUNTIES; ++i )
	{
		if( Counties[ i ].state_index == g_HISTORICAL_ET_state_index )
		{
			strlcpy( CountiesCopy[ sortCount ], Counties[ i ].county, MAX_REFERENCE_ET_NAME_LENGTH );

			++sortCount;
		}
	}

	HISTORICAL_ET_qsort( CountiesCopy, 0, (sortCount-1) );

	for( i = 0, g_county_display_count = 0; i < (UNS_32)sortCount; ++i )
	{
		for( j = 0; j < NO_OF_COUNTIES; ++j )
		{
			if( (strncmp ( CountiesCopy[ i ], Counties[ j ].county, MAX_REFERENCE_ET_NAME_LENGTH ) == 0 ) &&
			    (Counties[ j ].state_index == g_HISTORICAL_ET_state_index) )
			{
				g_county_display_order[ g_county_display_count ] = j;

				++g_county_display_count;
			}
		}
	}

	g_counties_sorted = (true);
	g_cities_sorted = (false);
}

static void HISTORICAL_ET_sort_cities( void )
{
	static char CitiesCopy[ NO_OF_CITIES ][ MAX_REFERENCE_ET_NAME_LENGTH ];

	INT_32 sortCount;

	UNS_32 i, j;

	for( i = 0, sortCount = 0; i < NO_OF_CITIES; ++i )
	{
		if( ET_PredefinedData[ i ].county_index == g_HISTORICAL_ET_county_index )
		{
			strlcpy( CitiesCopy[ sortCount ], ET_PredefinedData[ i ].city, MAX_REFERENCE_ET_NAME_LENGTH );

			++sortCount;
		}
	}

	HISTORICAL_ET_qsort( CitiesCopy, 0, (sortCount-1) );

	for( i = 0, g_city_display_count = 0; i < (UNS_32)sortCount; ++i )
	{
		for( j = 0; j < NO_OF_CITIES; ++j )
		{
			if( (strncmp( CitiesCopy[ i ], ET_PredefinedData[ j ].city, MAX_REFERENCE_ET_NAME_LENGTH ) == 0) &&
			    (ET_PredefinedData[ j ].county_index == g_HISTORICAL_ET_county_index) )
			{
				g_city_display_order[ g_city_display_count ] = j;

				++g_city_display_count;
			}
		}
	}

	g_cities_sorted = (true);
}

static void HISTORICAL_ET_process_reference_et_location( const UNS_32 pkeycode, UNS_32 *var_ptr, const UNS_32 pdisplay_order_array[], const UNS_32 pmin, const UNS_32 pmax )
{
	UNS_32	lindex;

	UNS_32	i;

        lindex = 0;

	for( i = 0; i <= pmax; ++i )
	{
		if ( *var_ptr == pdisplay_order_array[ i ] )
		{
			lindex = i;
		}
	}

	switch ( pkeycode )
	{
		case KEY_PLUS:
			good_key_beep();

			if( lindex == pmax )
			{
				lindex = pmin;
			}
			else
			{
				if( lindex < pmax )
				{
					lindex += 1;
				}
			}
			break;

		case KEY_MINUS:
			good_key_beep();

			if( lindex == pmin )
			{
				lindex = pmax;
			}
			else
			{
				if ( lindex >= 1 )
				{
					lindex -= 1;
				}
			}
			break;
	}

	*var_ptr = pdisplay_order_array[ lindex ];
}

static void HISTORICAL_ET_process_state_index( const UNS_32 pkeycode )
{
	HISTORICAL_ET_sort_states();

	HISTORICAL_ET_process_reference_et_location( pkeycode, &g_HISTORICAL_ET_state_index, g_state_display_order, 0, (g_state_display_count-1) );

	HISTORICAL_ET_sort_counties();

	g_HISTORICAL_ET_county_index = g_county_display_order[ 0 ];

	HISTORICAL_ET_sort_cities();

	g_HISTORICAL_ET_city_index = g_city_display_order[ 0 ];

	HISTORICAL_ET_update_et_numbers();
}

static void HISTORICAL_ET_process_county_index( const UNS_32 pkeycode )
{
	if( g_counties_sorted == (false) )
	{
		HISTORICAL_ET_sort_counties();
	}

	if( g_county_display_count == 1 )
	{
		bad_key_beep();
	}
	else
	{
		HISTORICAL_ET_process_reference_et_location( pkeycode, &g_HISTORICAL_ET_county_index, g_county_display_order, 0, (g_county_display_count-1) );

		HISTORICAL_ET_sort_cities();

		g_HISTORICAL_ET_city_index = g_city_display_order[ 0 ];

		HISTORICAL_ET_update_et_numbers();
	}
}

static void HISTORICAL_ET_process_city_index( const UNS_32 pkeycode )
{
	if( g_cities_sorted == (false) )
	{
		HISTORICAL_ET_sort_cities();
	}

	if( g_city_display_count == 1 )
	{
		bad_key_beep();
	}
	else
	{
		HISTORICAL_ET_process_reference_et_location( pkeycode, &g_HISTORICAL_ET_city_index, g_city_display_order, 0, (g_city_display_count-1) );

		HISTORICAL_ET_update_et_numbers();
	}
}

static void HISTORICAL_ET_process_user_et( const UNS_32 pkeycode, float *et_ptr )
{
	process_fl( pkeycode, et_ptr, WEATHER_REFERENCE_ET_MIN_100u, (WEATHER_REFERENCE_ET_MAX_100u / 100.0F), 0.01F, (false) );
	Refresh_Screen();
}

static void FDTO_HISTORICAL_ET_show_source_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxHistoricalETSource_0, &FDTO_COMBOBOX_add_items, 2, GuiVar_HistoricalETUseYourOwn );
}

static void FDTO_HISTORICAL_ET_populate_state_dropdown( const INT_16 pindex_0 )
{
	UNS_32	i;

	for( i = 0; i < g_state_display_count; ++i )
	{
		strlcpy( GuiVar_ComboBoxItemString, States[ g_state_display_order[ pindex_0 ] ], sizeof(GuiVar_ComboBoxItemString) );
	}
}

static void FDTO_HISTORICAL_ET_show_state_dropdown( void )
{
	HISTORICAL_ET_sort_states();

	UNS_32	lstate_to_display;

	lstate_to_display = 0;

	while( g_state_display_order[ lstate_to_display ] != g_HISTORICAL_ET_state_index )
	{
		++lstate_to_display;
	}

	FDTO_COMBOBOX_show( GuiStruct_cbxHistoricalETState_0, &FDTO_HISTORICAL_ET_populate_state_dropdown, g_state_display_count, lstate_to_display );
}

static void FDTO_HISTORICAL_ET_close_state_dropdown( void )
{
	if( g_HISTORICAL_ET_state_index != g_state_display_order[ GuiLib_ScrollBox_GetActiveLine( 0, 0 ) ] )
	{
		g_HISTORICAL_ET_state_index = g_state_display_order[ GuiLib_ScrollBox_GetActiveLine( 0, 0 ) ];

		HISTORICAL_ET_sort_counties();
		g_HISTORICAL_ET_county_index = g_county_display_order[ 0 ];

		HISTORICAL_ET_sort_cities();
		g_HISTORICAL_ET_city_index = g_city_display_order[ 0 ];

		HISTORICAL_ET_update_et_numbers();
	}

	FDTO_COMBOBOX_hide();
}

static void FDTO_HISTORICAL_ET_populate_county_dropdown( const INT_16 pindex_0 )
{
	UNS_32	i;

	for( i = 0; i < g_county_display_count; ++i )
	{
		strlcpy( GuiVar_ComboBoxItemString, Counties[ g_county_display_order[ pindex_0 ] ].county, sizeof(GuiVar_ComboBoxItemString) );
	}
}

static void FDTO_HISTORICAL_ET_show_county_dropdown( void )
{
	HISTORICAL_ET_sort_counties();

	UNS_32	lcounty_to_display;

	lcounty_to_display = 0;

	while( g_county_display_order[ lcounty_to_display ] != g_HISTORICAL_ET_county_index )
	{
		++lcounty_to_display;
	}

	FDTO_COMBOBOX_show( GuiStruct_cbxHistoricalETCounty_0, &FDTO_HISTORICAL_ET_populate_county_dropdown, g_county_display_count, lcounty_to_display );
}

static void FDTO_HISTORICAL_ET_close_county_dropdown( void )
{
	if( g_HISTORICAL_ET_county_index != g_county_display_order[ GuiLib_ScrollBox_GetActiveLine( 0, 0 ) ] )
	{
		g_HISTORICAL_ET_county_index = g_county_display_order[ GuiLib_ScrollBox_GetActiveLine( 0, 0 ) ];

		HISTORICAL_ET_sort_cities();
		g_HISTORICAL_ET_city_index = g_city_display_order[ 0 ];

		HISTORICAL_ET_update_et_numbers();
	}
	
	FDTO_COMBOBOX_hide();
}

static void FDTO_HISTORICAL_ET_populate_city_dropdown( const INT_16 pindex_0 )
{
	UNS_32	i;

	for( i = 0; i < g_city_display_count; ++i )
	{
		strlcpy( GuiVar_ComboBoxItemString, ET_PredefinedData[ g_city_display_order[ pindex_0 ] ].city, sizeof(GuiVar_ComboBoxItemString) );
	}
}

static void FDTO_HISTORICAL_ET_show_city_dropdown( void )
{
	HISTORICAL_ET_sort_cities();

	UNS_32	lcity_to_display;

	lcity_to_display = 0;

	while( g_city_display_order[ lcity_to_display ] != g_HISTORICAL_ET_city_index )
	{
		++lcity_to_display;
	}

	FDTO_COMBOBOX_show( GuiStruct_cbxHistoricalETCity_0, &FDTO_HISTORICAL_ET_populate_city_dropdown, g_city_display_count, lcity_to_display );
}

static void FDTO_HISTORICAL_ET_close_city_dropdown( void )
{
	g_HISTORICAL_ET_city_index = g_city_display_order[ GuiLib_ScrollBox_GetActiveLine( 0, 0 ) ];

	HISTORICAL_ET_update_et_numbers();

	FDTO_COMBOBOX_hide();
}

/* ---------------------------------------------------------- */
extern void FDTO_HISTORICAL_ET_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		WEATHER_copy_historical_et_settings_into_GuiVars();

		lcursor_to_select = 0;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrHistoricalET_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

extern void HISTORICAL_ET_process_screen( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_dlgKeyboard_0:
			if( ((pkey_event.keycode == KEY_BACK) || ((pkey_event.keycode == KEY_SELECT) && (GuiLib_ActiveCursorFieldNo == 49 /*CP_QWERTY_KEYBOARD_OK*/))) )
			{
				GuiLib_ActiveCursorFieldNo = (INT_16)g_cursor_pos_of_direct_entry_field;

				if( GuiLib_ActiveCursorFieldNo == CP_HISTORICAL_ET_STATE )
				{
					strlcpy( GuiVar_HistoricalETUserState, GuiVar_GroupName, MAX_REFERENCE_ET_NAME_LENGTH );
				}
				else if( GuiLib_ActiveCursorFieldNo == CP_HISTORICAL_ET_COUNTY )
				{
					strlcpy( GuiVar_HistoricalETUserCounty, GuiVar_GroupName, MAX_REFERENCE_ET_NAME_LENGTH );
				}
				else if( GuiLib_ActiveCursorFieldNo == CP_HISTORICAL_ET_CITY )
				{
					strlcpy( GuiVar_HistoricalETUserCity, GuiVar_GroupName, MAX_REFERENCE_ET_NAME_LENGTH );
				}
			}

			// Since the on-screen keyboard is visible, process the key event on
			// that structure instead of this screen.
			KEYBOARD_process_key( pkey_event, &FDTO_HISTORICAL_ET_draw_screen );
			break;

		case GuiStruct_cbxHistoricalETSource_0:
			COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_HistoricalETUseYourOwn );
			break;

		case GuiStruct_cbxHistoricalETState_0:
			if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
			{
				good_key_beep();

				lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
				lde._04_func_ptr = FDTO_HISTORICAL_ET_close_state_dropdown;
				Display_Post_Command( &lde );
			}
			else
			{
				COMBO_BOX_key_press( pkey_event.keycode, NULL );
			}
			break;

		case GuiStruct_cbxHistoricalETCounty_0:
			if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
			{
				good_key_beep();

				lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
				lde._04_func_ptr = FDTO_HISTORICAL_ET_close_county_dropdown;
				Display_Post_Command( &lde );
			}
			else
			{
				COMBO_BOX_key_press( pkey_event.keycode, NULL );
			}
			break;

		case GuiStruct_cbxHistoricalETCity_0:
			if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
			{
				good_key_beep();

				lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
				lde._04_func_ptr = FDTO_HISTORICAL_ET_close_city_dropdown;
				Display_Post_Command( &lde );
			}
			else
			{
				COMBO_BOX_key_press( pkey_event.keycode, NULL );
			}
			break;
	
		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_HISTORICAL_ET_SOURCE:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_HISTORICAL_ET_show_source_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_HISTORICAL_ET_STATE:
							if( GuiVar_HistoricalETUseYourOwn == (false) )
							{
								good_key_beep();
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = (void*)&FDTO_HISTORICAL_ET_show_state_dropdown;
								Display_Post_Command( &lde );
							}
							else
							{
								good_key_beep();

								strlcpy( GuiVar_GroupName, GuiVar_HistoricalETUserState, sizeof(GuiVar_GroupName) );

								g_cursor_pos_of_direct_entry_field = CP_HISTORICAL_ET_STATE;

								KEYBOARD_draw_keyboard( 96, 44, MAX_REFERENCE_ET_NAME_LENGTH, KEYBOARD_TYPE_QWERTY );
							}
							break;

						case CP_HISTORICAL_ET_COUNTY:
							if( GuiVar_HistoricalETUseYourOwn == (false) )
							{
								good_key_beep();
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = (void*)&FDTO_HISTORICAL_ET_show_county_dropdown;
								Display_Post_Command( &lde );
							}
							else
							{
								good_key_beep();

								strlcpy( GuiVar_GroupName, GuiVar_HistoricalETUserCounty, sizeof(GuiVar_GroupName) );

								g_cursor_pos_of_direct_entry_field = CP_HISTORICAL_ET_COUNTY;

								KEYBOARD_draw_keyboard( 96, 58, MAX_REFERENCE_ET_NAME_LENGTH, KEYBOARD_TYPE_QWERTY );
							}
							break;

						case CP_HISTORICAL_ET_CITY:
							if( GuiVar_HistoricalETUseYourOwn == (false) )
							{
								good_key_beep();
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = (void*)&FDTO_HISTORICAL_ET_show_city_dropdown;
								Display_Post_Command( &lde );
							}
							else
							{
								good_key_beep();

								strlcpy( GuiVar_GroupName, GuiVar_HistoricalETUserCity, sizeof(GuiVar_GroupName) );

								g_cursor_pos_of_direct_entry_field = CP_HISTORICAL_ET_CITY;

								KEYBOARD_draw_keyboard( 96, 72, MAX_REFERENCE_ET_NAME_LENGTH, KEYBOARD_TYPE_QWERTY );
							}
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_PLUS:
				case KEY_MINUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_HISTORICAL_ET_SOURCE:
							process_bool( &GuiVar_HistoricalETUseYourOwn );

							Redraw_Screen( (false) );
							break;

						case CP_HISTORICAL_ET_STATE:
							if( GuiVar_HistoricalETUseYourOwn == (false) )
							{
								HISTORICAL_ET_process_state_index( pkey_event.keycode );
							}
							else
							{
								bad_key_beep();
							}
							break;

						case CP_HISTORICAL_ET_COUNTY:
							if( GuiVar_HistoricalETUseYourOwn == (false) )
							{
								HISTORICAL_ET_process_county_index( pkey_event.keycode );
							}
							else
							{
								bad_key_beep();
							}
							break;

						case CP_HISTORICAL_ET_CITY:
							if( GuiVar_HistoricalETUseYourOwn == (false) )
							{
								HISTORICAL_ET_process_city_index( pkey_event.keycode );
							}
							else
							{
								bad_key_beep();
							}
							break;
							
						case CP_HISTORICAL_ET_MAX_PERCENT_OF_ET:
							process_uns32( pkey_event.keycode, &GuiVar_ETGageMaxPercent, WEATHER_ET_PERCENT_OF_HISTORICAL_CAP_MIN_100u, WEATHER_ET_PERCENT_OF_HISTORICAL_CAP_MAX_100u, 1, (false) );
							Refresh_Screen();
							break;

						case CP_HISTORICAL_ET_USER_ET_1:
							HISTORICAL_ET_process_user_et( pkey_event.keycode, &GuiVar_HistoricalETUser1 );
							break;

						case CP_HISTORICAL_ET_USER_ET_2:
							HISTORICAL_ET_process_user_et( pkey_event.keycode, &GuiVar_HistoricalETUser2 );
							break;

						case CP_HISTORICAL_ET_USER_ET_3:
							HISTORICAL_ET_process_user_et( pkey_event.keycode, &GuiVar_HistoricalETUser3 );
							break;

						case CP_HISTORICAL_ET_USER_ET_4:
							HISTORICAL_ET_process_user_et( pkey_event.keycode, &GuiVar_HistoricalETUser4 );
							break;

						case CP_HISTORICAL_ET_USER_ET_5:
							HISTORICAL_ET_process_user_et( pkey_event.keycode, &GuiVar_HistoricalETUser5 );
							break;

						case CP_HISTORICAL_ET_USER_ET_6:
							HISTORICAL_ET_process_user_et( pkey_event.keycode, &GuiVar_HistoricalETUser6 );
							break;

						case CP_HISTORICAL_ET_USER_ET_7:
							HISTORICAL_ET_process_user_et( pkey_event.keycode, &GuiVar_HistoricalETUser7 );
							break;

						case CP_HISTORICAL_ET_USER_ET_8:
							HISTORICAL_ET_process_user_et( pkey_event.keycode, &GuiVar_HistoricalETUser8 );
							break;

						case CP_HISTORICAL_ET_USER_ET_9:
							HISTORICAL_ET_process_user_et( pkey_event.keycode, &GuiVar_HistoricalETUser9 );
							break;

						case CP_HISTORICAL_ET_USER_ET_10:
							HISTORICAL_ET_process_user_et( pkey_event.keycode, &GuiVar_HistoricalETUser10 );
							break;

						case CP_HISTORICAL_ET_USER_ET_11:
							HISTORICAL_ET_process_user_et( pkey_event.keycode, &GuiVar_HistoricalETUser11 );
							break;

						case CP_HISTORICAL_ET_USER_ET_12:
							HISTORICAL_ET_process_user_et( pkey_event.keycode, &GuiVar_HistoricalETUser12 );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_C_UP:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_HISTORICAL_ET_SOURCE:
							bad_key_beep();
							break;

						case CP_HISTORICAL_ET_USER_ET_1:
						case CP_HISTORICAL_ET_USER_ET_2:
						case CP_HISTORICAL_ET_USER_ET_3:
						case CP_HISTORICAL_ET_USER_ET_4:
						case CP_HISTORICAL_ET_USER_ET_5:
						case CP_HISTORICAL_ET_USER_ET_6:
							CURSOR_Select( CP_HISTORICAL_ET_MAX_PERCENT_OF_ET, (true) );
							break;

						case CP_HISTORICAL_ET_USER_ET_7:
						case CP_HISTORICAL_ET_USER_ET_8:
						case CP_HISTORICAL_ET_USER_ET_9:
						case CP_HISTORICAL_ET_USER_ET_10:
						case CP_HISTORICAL_ET_USER_ET_11:
						case CP_HISTORICAL_ET_USER_ET_12:
							CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo-6), (true) );
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_DOWN:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_HISTORICAL_ET_USER_ET_1:
						case CP_HISTORICAL_ET_USER_ET_2:
						case CP_HISTORICAL_ET_USER_ET_3:
						case CP_HISTORICAL_ET_USER_ET_4:
						case CP_HISTORICAL_ET_USER_ET_5:
						case CP_HISTORICAL_ET_USER_ET_6:
							CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo+6), (true) );
							break;

						case CP_HISTORICAL_ET_USER_ET_7:
						case CP_HISTORICAL_ET_USER_ET_8:
						case CP_HISTORICAL_ET_USER_ET_9:
						case CP_HISTORICAL_ET_USER_ET_10:
						case CP_HISTORICAL_ET_USER_ET_11:
						case CP_HISTORICAL_ET_USER_ET_12:
							bad_key_beep();
							break;

						default:
							CURSOR_Down( (true) );
					}
					break;

				case KEY_C_LEFT:
					CURSOR_Up( (true) );
					break;

				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				default:
					if( pkey_event.keycode == KEY_BACK )
					{
						WEATHER_extract_and_store_historical_et_changes_from_GuiVars();

						GuiVar_MenuScreenToShow = CP_MAIN_MENU_WEATHER;
					}

					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

