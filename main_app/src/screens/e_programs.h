/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_E_PROGRAMS_H
#define _INC_E_PROGRAMS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FDTO_SCHEDULE_draw_menu( const BOOL_32 pcomplete_redraw );

extern void SCHEDULE_process_menu( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

