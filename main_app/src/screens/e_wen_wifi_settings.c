/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/12/2015 mpd : Required for isxdigit
#include	<ctype.h>

// 4/23/2015 mpd : Required for atoi
#include	<stdlib.h>

// 4/21/2015 ajv : Required for strlcpy.
#include	<string.h>

#include	"e_wen_wifi_settings.h"

#include	"combobox.h"

#include	"device_common.h"

#include	"device_WEN_PremierWaveXN.h"

#include	"configuration_controller.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"d_device_exchange.h"

#include	"device_WEN_WiBox.h"

#include	"dialog.h"

#include	"e_keyboard.h"

#include	"e_wen_programming.h"

#include	"group_base_file.h"

#include	"speaker.h"

#include	"alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_WEN_WIFI_SETTINGS_SSID						(0)
#define CP_WEN_WIFI_SETTINGS_RADIO_MODE					(1)
#define CP_WEN_WIFI_SETTINGS_SECURITY_SUITE				(2)
#define CP_WEN_WIFI_SETTINGS_WEP_AUTHENTICATION			(3)
#define CP_WEN_WIFI_SETTINGS_WEP_KEY_TYPE				(4)
#define CP_WEN_WIFI_SETTINGS_WEP_KEY_SIZE				(5)
#define CP_WEN_WIFI_SETTINGS_WEP_CHANGE_KEY				(6)
#define CP_WEN_WIFI_SETTINGS_WEP_KEY					(7)
#define CP_WEN_WIFI_SETTINGS_WEP_KEY_INDEX				(8)
#define CP_WEN_WIFI_SETTINGS_WEP_CHANGE_PASSPHRASE		(9)
#define CP_WEN_WIFI_SETTINGS_WEP_PASSPHRASE				(10)
#define CP_WEN_WIFI_SETTINGS_WPA_AUTHENTICATION			(11)
#define CP_WEN_WIFI_SETTINGS_WPA_KEY_TYPE				(12)
#define CP_WEN_WIFI_SETTINGS_WPA_CHANGE_KEY				(13)
#define CP_WEN_WIFI_SETTINGS_WPA_KEY					(14)
#define CP_WEN_WIFI_SETTINGS_WPA_CHANGE_PASSPHRASE		(15)
#define CP_WEN_WIFI_SETTINGS_WPA_PASSPHRASE				(16)
#define CP_WEN_WIFI_SETTINGS_IEEE8021X_TYPE				(17)
#define CP_WEN_WIFI_SETTINGS_LEAP_USERNAME				(18)
#define CP_WEN_WIFI_SETTINGS_LEAP_CHANGE_PASSWORD		(19)
#define CP_WEN_WIFI_SETTINGS_LEAP_PASSWORD				(20)
#define CP_WEN_WIFI_SETTINGS_EAP_TLS_USERNAME			(21)
#define CP_WEN_WIFI_SETTINGS_EAP_TLS_VALIDATE_CERT		(22)
#define CP_WEN_WIFI_SETTINGS_EAP_TLS_CHANGE_CREDENTIALS	(23)
#define CP_WEN_WIFI_SETTINGS_EAP_TLS_CREDENTIALS		(24)
#define CP_WEN_WIFI_SETTINGS_EAP_TTLS_OPTION			(25)
#define CP_WEN_WIFI_SETTINGS_EAP_TTLS_USERNAME			(26)
#define CP_WEN_WIFI_SETTINGS_EAP_TTLS_CHANGE_PASSWORD	(27)
#define CP_WEN_WIFI_SETTINGS_EAP_TTLS_PASSWORD			(28)
#define CP_WEN_WIFI_SETTINGS_PEAP_OPTION				(29)
#define CP_WEN_WIFI_SETTINGS_PEAP_USERNAME				(30)
#define CP_WEN_WIFI_SETTINGS_PEAP_CHANGE_PASSWORD		(31)
#define CP_WEN_WIFI_SETTINGS_PEAP_PASSWORD				(32)
#define CP_WEN_WIFI_SETTINGS_CCMP_ENABLED				(33)
#define	CP_WEN_WIFI_SETTINGS_TKIP_ENABLED				(34)
#define	CP_WEN_WIFI_SETTINGS_WEP_ENABLED				(35)
#define	CP_WEN_WIFI_SETTINGS_WPA_ENCRYPTION				(36)
#define	CP_WEN_WIFI_SETTINGS_WPA2_ENCRYPTION			(37)

// ----------

#define	WEN_RADIO_MODE_MIN			  (0)
#define	WEN_RADIO_MODE_MAX			  (5)
									  
#define WEN_WIRELESS_SECURITY_MIN	  (0)
#define WEN_WIRELESS_SECURITY_MAX	  (3)
									  
#define WEN_WEP_AUTHENTICATION_MIN	  (0)
#define WEN_WEP_AUTHENTICATION_MAX	  (1)

#define WEN_KEY_TYPE_MIN			  (0)
#define WEN_KEY_TYPE_MAX			  (1)
									  
#define WEN_KEY_SIZE_MIN			  (0)
#define WEN_KEY_SIZE_MAX			  (1)
									  
// 6/3/2015 mpd : This item is a single variable, not an indexed structure.
#define WEN_TX_KEY_MIN				  (1)
#define WEN_TX_KEY_MAX				  (4)
									  
#define WEN_WPA_AUTHENTICATION_MIN	  (0)
#define WEN_WPA_AUTHENTICATION_MAX	  (1)
#define WEN_WPA_AUTHENTICATION_PSK	  (0)
#define WEN_WPA_AUTHENTICATION_IEEE	  (1)
									  
#define WEN_WPA_IEEE8021X_MIN		  (0)
#define WEN_WPA_IEEE8021X_MAX		  (3)
#define WEN_WPA_IEEE8021X_LEAP		  (0)
#define WEN_WPA_IEEE8021X_EAP_TLS	  (1)
#define WEN_WPA_IEEE8021X_EAP_TTLS	  (2)
#define WEN_WPA_IEEE8021X_PEAP		  (3)
									  
#define WEN_EAP_TTLS_OPTION_MIN		  (0)
#define WEN_EAP_TTLS_OPTION_MAX		  (5)
									  
#define WEN_PEAP_OPTION_MIN			  (0)
#define WEN_PEAP_OPTION_MAX			  (1)

#define	WEN_WPA_ENCRYPTION_MIN		  (0)
#define	WEN_WPA_ENCRYPTION_MAX		  (1)

#define	WEN_WPA2_ENCRYPTION_MIN		  (0)
#define	WEN_WPA2_ENCRYPTION_MAX		  (4)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/2/2015 mpd : We need to know what field the user was on when the keyboard was displayed.
static UNS_32	g_WEN_PROGRAMMING_wifi_cp_using_keyboard;

static UNS_32	g_WEN_PROGRAMMING_wifi_previous_cursor_pos = CP_WEN_WIFI_SETTINGS_SSID;

static UNS_32	g_WEN_PROGRAMMING_wifi_saved_key_size;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void set_wen_programming_struct_from_wifi_guivars()
{
	if( GuiVar_WENRadioType == COMM_DEVICE_WEN_PREMIERWAVE )
	{
		DEV_DETAILS_STRUCT *dev_details;
		WEN_DETAILS_STRUCT *wen_details;

		//Alert_Message( "set_wen_programming_struct_from_wifi_guivars" );

		if( ( dev_state != NULL ) && ( dev_state->dev_details != NULL ) && ( dev_state->wen_details != NULL ) )
		{
			dev_details = dev_state->dev_details;
			wen_details = dev_state->wen_details;

			strlcpy( wen_details->key,           			GuiVar_WENKey,                     sizeof( wen_details->key ) );
			strlcpy( wen_details->passphrase,           	GuiVar_WENPassphrase,              sizeof( wen_details->passphrase ) );
			strlcpy( wen_details->ssid,           			GuiVar_WENSSID,                    sizeof( wen_details->ssid ) );
			strlcpy( wen_details->status,           		GuiVar_WENStatus,                  sizeof( wen_details->status ) );
			strlcpy( wen_details->wpa_eap_tls_credentials,	GuiVar_WENWPAEAPTLSCredentials,    sizeof( wen_details->wpa_eap_tls_credentials ) );
			strlcpy( wen_details->password,          		GuiVar_WENWPAPassword,             sizeof( wen_details->password ) );
			strlcpy( wen_details->user_name,          	 	GuiVar_WENWPAUsername,             sizeof( wen_details->user_name ) );

			// 5/29/2015 mpd : Several "wen->details" items are strings while the associated GuiVar is an UNS_32. These need 
			// to be mapped from indexed structures.
			strlcpy( wen_details->radio_mode,          e_SHARED_get_easyGUI_string_at_index( GuiStruct_itxtWENRadioMode_0,      GuiVar_WENRadioMode ),         sizeof( wen_details->radio_mode ) );
			strlcpy( wen_details->security,            e_SHARED_get_easyGUI_string_at_index( GuiStruct_itxtWENSecurity_0,       GuiVar_WENSecurity ),          sizeof( wen_details->security ) );
			strlcpy( wen_details->wep_authentication,  e_SHARED_get_easyGUI_string_at_index( GuiStruct_itxtWEPAuthentication_0, GuiVar_WENWEPAuthentication ), sizeof( wen_details->wep_authentication ) );
			strlcpy( wen_details->wep_key_size,        e_SHARED_get_easyGUI_string_at_index( GuiStruct_itxtKeySize_0,           GuiVar_WENWEPKeySize ),        sizeof( wen_details->wep_key_size ) );
			strlcpy( wen_details->wpa_authentication,  e_SHARED_get_easyGUI_string_at_index( GuiStruct_itxtWPAAuthentication_0, GuiVar_WENWPAAuthentication ), sizeof( wen_details->wpa_authentication ) );
			strlcpy( wen_details->wpa_eap_ttls_option, e_SHARED_get_easyGUI_string_at_index( GuiStruct_itxtEAPTTLSOption_0,     GuiVar_WENWPAEAPTTLSOption ),  sizeof( wen_details->wpa_eap_ttls_option ) );
			strlcpy( wen_details->wpa_ieee_802_1x,     e_SHARED_get_easyGUI_string_at_index( GuiStruct_itxtIEEE8021X_0,         GuiVar_WENWPAIEEE8021X ),      sizeof( wen_details->wpa_ieee_802_1x ) );
			strlcpy( wen_details->wpa_peap_option,     e_SHARED_get_easyGUI_string_at_index( GuiStruct_itxtPEAPOption_0,        GuiVar_WENWPAPEAPOption  ),    sizeof( wen_details->wpa_peap_option ) );

			// 6/17/2015 mpd : The programming side needs the appropriate key type based on the current security suite.
			if( GuiVar_WENSecurity == WEN_WIRELESS_SECURITY_WEP )
			{
				strlcpy( wen_details->w_key_type, e_SHARED_get_easyGUI_string_at_index( GuiStruct_itxtKeyType_0, GuiVar_WENWEPKeyType ), sizeof( wen_details->w_key_type ) );
			}
			else
			{
				strlcpy( wen_details->w_key_type, e_SHARED_get_easyGUI_string_at_index( GuiStruct_itxtKeyType_0, GuiVar_WENWPAKeyType ), sizeof( wen_details->w_key_type ) );
			}

			wen_details->wpa_eap_tls_valid_cert = GuiVar_WENWPAEAPTLSValidateCert;
			wen_details->wpa_encription_ccmp    = GuiVar_WENWPAEncryptionCCMP;
			wen_details->wpa_encription_tkip    = GuiVar_WENWPAEncryptionTKIP;
			wen_details->wpa_encription_wep     = GuiVar_WENWPAEncryptionWEP;

			// 6/3/2015 mpd : This item is a single variable, not an indexed structure. It's a string on the programming side.
			snprintf( wen_details->wep_tx_key, sizeof( wen_details->wep_tx_key ), "%d", GuiVar_WENWEPTXKey );

			// 6/15/2015 mpd : For security reasons, several fields cannot be read from the device. That means we cannot blindly
			// write the original values back to the device when the user hits the "Save" key. Only modify these fields if the
			// user explicitly tells us to do so.
			wen_details->user_changed_key         = GuiVar_WENChangeKey;
			wen_details->user_changed_passphrase  = GuiVar_WENChangePassphrase;
			wen_details->user_changed_password    = GuiVar_WENChangePassword;
			wen_details->user_changed_credentials = GuiVar_WENChangeCredentials;
		}
	}
	else if( GuiVar_WENRadioType == COMM_DEVICE_WEN_WIBOX )
	{
		WIBOX_DETAILS_STRUCT *ldetails;

		WIBOX_PROGRAMMABLE_VALUES_STRUCT *lactive_pvs;

		if( (dev_state != NULL) && (dev_state->WIBOX_details != NULL) && (dev_state->wibox_active_pvs != NULL) )
		{
			ldetails = dev_state->WIBOX_details;

			lactive_pvs = dev_state->wibox_active_pvs;

			// ----------

			strlcpy( lactive_pvs->ssid, GuiVar_WENSSID, sizeof(lactive_pvs->ssid) );

			lactive_pvs->security = GuiVar_WENSecurity;

			strlcpy( lactive_pvs->key, GuiVar_WENKey, sizeof(lactive_pvs->key) );
			strlcpy( lactive_pvs->passphrase, GuiVar_WENPassphrase, sizeof(lactive_pvs->passphrase) );

			lactive_pvs->wep_authentication = GuiVar_WENWEPAuthentication;

			lactive_pvs->wep_key_size = GuiVar_WENWEPKeySize;

			if( GuiVar_WENSecurity == WEN_WIRELESS_SECURITY_WEP )
			{
				lactive_pvs->w_key_type = GuiVar_WENWEPKeyType;
			}
			else
			{
				lactive_pvs->w_key_type = GuiVar_WENWPAKeyType;
			}

			lactive_pvs->wpa_encryption = GuiVar_WENWPAEncryption;

			lactive_pvs->wpa2_encryption = GuiVar_WENWPA2Encryption;

			// 4/1/2016 ajv : For security reasons, several fields cannot be read from the device. That
			// means we cannot blindly write the original values back to the device when the user hits
			// the "Save" key. Only modify these fields if the user explicitly tells us to do so.
			lactive_pvs->user_changed_key = GuiVar_WENChangeKey;

			lactive_pvs->user_changed_passphrase = GuiVar_WENChangePassphrase;
		}
	}
}

/* ---------------------------------------------------------- */
extern void get_wifi_guivars_from_wen_programming_struct()
{
	if( GuiVar_WENRadioType == COMM_DEVICE_WEN_PREMIERWAVE )
	{
		DEV_DETAILS_STRUCT *dev_details;
		WEN_DETAILS_STRUCT *wen_details;

		if( ( dev_state != NULL ) && ( dev_state->dev_details != NULL ) )
		{
			dev_details = dev_state->dev_details;

			if( dev_state->wen_details != NULL )
			{
				wen_details = dev_state->wen_details;

				// 6/2/2015 mpd : TBD: While the WIFI Settings screen is under construction, there is excessive exiting back to the
				// WEN Programming screen. Since there is no save on the WIFI screen, any changes made to those settings would be 
				// lost if we kept refreshing values from the programming structure every time we went back in. Use this flag to 
				// only update the GuiVars if there is new data from the device.
				if( !dev_state->gui_has_latest_data )
				{
					strlcpy( GuiVar_WENKey,		       			wen_details->key,           			sizeof( GuiVar_WENKey ) );
					strlcpy( GuiVar_WENStatus,		       		wen_details->status,           			sizeof( GuiVar_WENStatus ) );
					strlcpy( GuiVar_WENPassphrase,		    	wen_details->passphrase,  	         	sizeof( GuiVar_WENPassphrase ) );
					strlcpy( GuiVar_WENSSID,		       		wen_details->ssid,           			sizeof( GuiVar_WENSSID ) );
					strlcpy( GuiVar_WENWPAEAPTLSCredentials,	wen_details->wpa_eap_tls_credentials,	sizeof( GuiVar_WENWPAEAPTLSCredentials ) );
					strlcpy( GuiVar_WENWPAPassword,		    	wen_details->password,          		sizeof( GuiVar_WENWPAPassword ) );
					strlcpy( GuiVar_WENWPAUsername,		    	wen_details->user_name,          	 	sizeof( GuiVar_WENWPAUsername ) );

					e_SHARED_string_validation( GuiVar_WENSSID, 				sizeof( GuiVar_WENSSID ) );
					e_SHARED_string_validation( GuiVar_WENStatus, 				sizeof( GuiVar_WENStatus ) );
					e_SHARED_string_validation( GuiVar_WENWPAUsername, 			sizeof( GuiVar_WENWPAUsername ) );

					// 5/29/2015 mpd : These "wen->details" items are strings while the associated GuiVars ar UNS_32. The
					// strings need to be mapped to indexed structures. A table would be quick, but difficult to 
					// maintain.

					// 5/29/2015 mpd : TODO: These do not currently test for a -1 response. It will be unnecessary once all  
					// the user options have been tested. There is an alert in "e_SHARED_get_index_of_easyGUI_string()" 
					// until then.
					GuiVar_WENRadioMode         = e_SHARED_get_index_of_easyGUI_string( wen_details->radio_mode,          strlen( wen_details->radio_mode ),          GuiStruct_itxtWENRadioMode_0,      WEN_RADIO_MODE_MIN,         WEN_RADIO_MODE_MAX );
					GuiVar_WENSecurity          = e_SHARED_get_index_of_easyGUI_string( wen_details->security,            strlen( wen_details->security ),            GuiStruct_itxtWENSecurity_0,       WEN_WIRELESS_SECURITY_MIN,  WEN_WIRELESS_SECURITY_MAX );
					GuiVar_WENWEPAuthentication = e_SHARED_get_index_of_easyGUI_string( wen_details->wep_authentication,  strlen( wen_details->wep_authentication ),  GuiStruct_itxtWEPAuthentication_0, WEN_WEP_AUTHENTICATION_MIN, WEN_WEP_AUTHENTICATION_MAX );
					GuiVar_WENWEPKeySize        = e_SHARED_get_index_of_easyGUI_string( wen_details->wep_key_size,        strlen( wen_details->wep_key_size ),        GuiStruct_itxtKeySize_0,           WEN_KEY_SIZE_MIN,           WEN_KEY_SIZE_MAX );
					GuiVar_WENWPAAuthentication = e_SHARED_get_index_of_easyGUI_string( wen_details->wpa_authentication,  strlen( wen_details->wpa_authentication ),  GuiStruct_itxtWPAAuthentication_0, WEN_WPA_AUTHENTICATION_MIN, WEN_WPA_AUTHENTICATION_MAX );

					// 6/16/2015 mpd : Load both key types from the single programming struct value.
					GuiVar_WENWEPKeyType        = e_SHARED_get_index_of_easyGUI_string( wen_details->w_key_type,          strlen( wen_details->w_key_type ),          GuiStruct_itxtKeyType_0,           WEN_KEY_TYPE_MIN,           WEN_KEY_TYPE_MAX );
					GuiVar_WENWPAKeyType        = e_SHARED_get_index_of_easyGUI_string( wen_details->w_key_type,          strlen( wen_details->w_key_type ),          GuiStruct_itxtKeyType_0,           WEN_KEY_TYPE_MIN,           WEN_KEY_TYPE_MAX );

					GuiVar_WENWPAEAPTTLSOption  = e_SHARED_get_index_of_easyGUI_string( wen_details->wpa_eap_ttls_option, strlen( wen_details->wpa_eap_ttls_option ), GuiStruct_itxtEAPTTLSOption_0,     WEN_EAP_TTLS_OPTION_MIN,    WEN_EAP_TTLS_OPTION_MAX );
					GuiVar_WENWPAIEEE8021X      = e_SHARED_get_index_of_easyGUI_string( wen_details->wpa_ieee_802_1x,     strlen( wen_details->wpa_ieee_802_1x ),     GuiStruct_itxtIEEE8021X_0,         WEN_WPA_IEEE8021X_MIN,      WEN_WPA_IEEE8021X_MAX );
					GuiVar_WENWPAPEAPOption	    = e_SHARED_get_index_of_easyGUI_string( wen_details->wpa_peap_option,     strlen( wen_details->wpa_peap_option ),     GuiStruct_itxtPEAPOption_0,        WEN_PEAP_OPTION_MIN,        WEN_PEAP_OPTION_MAX );

					// 6/3/2015 mpd : This item is a single variable, not an indexed structure.
					GuiVar_WENWEPTXKey          = atoi( wen_details->wep_tx_key );

					GuiVar_WENWPAEAPTLSValidateCert = wen_details->wpa_eap_tls_valid_cert;
					GuiVar_WENWPAEncryptionCCMP 	= wen_details->wpa_encription_ccmp;
					GuiVar_WENWPAEncryptionTKIP 	= wen_details->wpa_encription_tkip;
					GuiVar_WENWPAEncryptionWEP 		= wen_details->wpa_encription_wep;
					//GuiVar_ENNetmask                = wen_details->mask_bits;
					//GuiVar_ENObtainIPAutomatically  = wen_details->dhcp_enabled;

					// 5/28/2015 mpd : RSSI is a signed INT_32.
					GuiVar_WENRSSI                  = wen_details->rssi;

					// 6/2/2015 mpd : This is the only place this flag is set to "true".
					dev_state->gui_has_latest_data = (true);

					// 6/16/2015 mpd : We have just read in values from the device. The user can now make any desired 
					// changes. Reset the flags so we don't keep repeating the same WRITE operations.
					GuiVar_WENChangeKey          = (false);
					GuiVar_WENChangePassphrase   = (false);
					GuiVar_WENChangePassword     = (false);
					GuiVar_WENChangeCredentials  = (false);
					g_WEN_PROGRAMMING_wifi_saved_key_size = GuiVar_WENWEPKeySize;
				}
			}
		}
	}
	else if( GuiVar_WENRadioType == COMM_DEVICE_WEN_WIBOX )
	{
		WIBOX_DETAILS_STRUCT *ldetails;

		WIBOX_PROGRAMMABLE_VALUES_STRUCT *lactive_pvs;

		if( (dev_state != NULL) && (dev_state->WIBOX_details != NULL) && (dev_state->wibox_active_pvs != NULL) )
		{
			ldetails = dev_state->WIBOX_details;

			lactive_pvs = dev_state->wibox_active_pvs;

			// 6/2/2015 mpd : TBD: While the WIFI Settings screen is under construction, there is
			// excessive exiting back to the WEN Programming screen. Since there is no save on the WIFI
			// screen, any changes made to those settings would be lost if we kept refreshing values
			// from the programming structure every time we went back in. Use this flag to only update
			// the GuiVars if there is new data from the device.
			if( !dev_state->gui_has_latest_data )
			{
				strlcpy( GuiVar_WENSSID, lactive_pvs->ssid, sizeof(GuiVar_WENSSID) );
				e_SHARED_string_validation( GuiVar_WENSSID, sizeof(GuiVar_WENSSID) );

				// 3/29/2016 ajv : Radio Mode is not editable for the WiBox. Force to 802.11bg.
				GuiVar_WENRadioMode = 3;

				GuiVar_WENSecurity = lactive_pvs->security;

				strlcpy( GuiVar_WENKey, lactive_pvs->key, sizeof(GuiVar_WENKey) );
				strlcpy( GuiVar_WENPassphrase, lactive_pvs->passphrase, sizeof( GuiVar_WENPassphrase ) );

				GuiVar_WENWEPKeySize = lactive_pvs->wep_key_size;

				GuiVar_WENWEPAuthentication = lactive_pvs->wep_authentication;

				// 4/1/2016 ajv : The WiBox doesn't allow any WPA authentication other than PSK, so force it
				// accordingly. We also block the user from changing it.
				GuiVar_WENWPAAuthentication = 0;

				// 4/1/2016 ajv : We don't allow the user to edit the key type on the WiBox - Force it to
				// Passphrase. This may seem somewhat extreme, but there's actually no way to determine
				// whether the user is using Hex or Passphrase from the menu without editing the passphrase,
				// which we can't read so we can't program.
				GuiVar_WENWEPKeyType = WEN_KEY_TYPE_PASS;
				GuiVar_WENWPAKeyType = WEN_KEY_TYPE_PASS;

				// 4/1/2016 ajv : We don't allow the user to edit the TX Key on the WiBox.
				GuiVar_WENWEPTXKey = 1;

				GuiVar_WENWPAEncryption = lactive_pvs->wpa_encryption;

				GuiVar_WENWPA2Encryption = lactive_pvs->wpa2_encryption;

				// ----------

				// 4/1/2016 ajv : This is the only place this flag is set TRUE.
				dev_state->gui_has_latest_data = (true);

				// 4/1/2016 ajv : We have just read in values from the device. The user can now make any
				// desired changes. Reset the flags so we don't keep repeating the same WRITE operations.
				GuiVar_WENChangeKey          = (false);

				GuiVar_WENChangePassphrase   = (false);

				g_WEN_PROGRAMMING_wifi_saved_key_size = GuiVar_WENWEPKeySize;
			}
		}
	}
}

/* ---------------------------------------------------------- */
extern BOOL_32 g_WEN_PROGRAMMING_wifi_values_in_range( BOOL_32 reset_values_on_error )
{
	BOOL_32 in_range = (true);

	UNS_32 hex_check_length = 0;

	// 6/17/2015 mpd : If the user changed the key size, we need to verify the key for the new length.
	if( g_WEN_PROGRAMMING_wifi_saved_key_size != GuiVar_WENWEPKeySize )
	{
		GuiVar_WENChangeKey = (true);
	}

	if( GuiVar_WENChangeKey )
	{
		// 6/15/2015 mpd : Test user input before allowing the long programming step.
		if( ( GuiVar_WENSecurity == WEN_WIRELESS_SECURITY_WEP ) && ( GuiVar_WENWEPKeyType == WEN_KEY_TYPE_HEX ) && ( GuiVar_WENWEPKeySize == WEN_KEY_SIZE_40 ) ) // WEP and HEX and 40 bits
		{
			hex_check_length = 10;
		}
		else if( ( GuiVar_WENSecurity == WEN_WIRELESS_SECURITY_WEP ) && ( GuiVar_WENWEPKeyType == WEN_KEY_TYPE_HEX ) && ( GuiVar_WENWEPKeySize == WEN_KEY_SIZE_104 ) ) // WEP and HEX and 104 bits
		{
			hex_check_length = 26;
		}
		else if( ( ( GuiVar_WENSecurity == WEN_WIRELESS_SECURITY_WPA ) || ( GuiVar_WENSecurity == WEN_WIRELESS_SECURITY_WPA2 ) ) && ( GuiVar_WENWPAKeyType == WEN_KEY_TYPE_HEX ) ) // WPA or WPA2 and HEX 
		{
			hex_check_length = 64;
		}

		// 6/15/2015 mpd : Verify the user input matches the appropriate key length. Note: WEP and WPA use the same key GUI 
		// variable.
		if( strlen( GuiVar_WENKey ) != hex_check_length )
		{
			if( GuiVar_WENSecurity == WEN_WIRELESS_SECURITY_WEP )
			{
				g_WEN_PROGRAMMING_wifi_previous_cursor_pos = CP_WEN_WIFI_SETTINGS_WEP_CHANGE_KEY;
			}
			else
			{
				g_WEN_PROGRAMMING_wifi_previous_cursor_pos = CP_WEN_WIFI_SETTINGS_WPA_CHANGE_KEY;
			}

			if( reset_values_on_error )
			{
				strlcpy( GuiVar_WENKey,	"", sizeof( GuiVar_WENKey ) );

				GuiVar_WENChangeKey = (false);
			}

			// 6/17/2015 mpd : We can't reset the key change without resetting any key size change too.
			GuiVar_WENWEPKeySize = g_WEN_PROGRAMMING_wifi_saved_key_size;

			in_range = (false);
			DIALOG_draw_ok_dialog( GuiStruct_dlgInvalidHexKeyLength_0 );
			bad_key_beep();
		}
	}

	return( in_range );
}

/* ---------------------------------------------------------- */
extern void g_WEN_PROGRAMMING_initialize_wifi_guivars()
{
	// 4/20/2015 ajv : Set the device exchange result to 'read_ok' to display the initialized variables.
	GuiVar_CommOptionDeviceExchangeResult = DEVICE_EXCHANGE_KEY_read_settings_completed_ok;

	// 5/27/2015 mpd : TBD: If any of these variables do not exist at this time (like SR repeater vars when in slave or 
	// master mode), they will have to be initialized later. 

	// 6/2/2015 mpd : TBD: While the WIFI Settings screen is under construction, there is excessive exiting back to the
	// WEN Programming screen. Since there is no save on the WIFI screen, any changes made to those settings would be 
	// lost if we kept initializing values every time we went back in. The flag belongs to 
	// "get_wifi_guivars_from_wen_programming_struct()". It works for this function too, but don't change it here!  
	if( !dev_state->gui_has_latest_data )
	{
		strlcpy( GuiVar_WENKey,		       			"", sizeof( GuiVar_WENKey ) );
		strlcpy( GuiVar_WENPassphrase,		    	"", sizeof( GuiVar_WENPassphrase ) );
		strlcpy( GuiVar_WENSSID,		       		"", sizeof( GuiVar_WENSSID ) );
		strlcpy( GuiVar_WENStatus,		       		"", sizeof( GuiVar_WENStatus ) );
		strlcpy( GuiVar_WENWPAEAPTLSCredentials,	"", sizeof( GuiVar_WENWPAEAPTLSCredentials ) );
		strlcpy( GuiVar_WENWPAPassword,		    	"", sizeof( GuiVar_WENWPAPassword ) );
		strlcpy( GuiVar_WENWPAUsername,		    	"", sizeof( GuiVar_WENWPAUsername ) );


		e_SHARED_string_validation( GuiVar_WENSSID, 				sizeof( GuiVar_WENSSID ) );
		e_SHARED_string_validation( GuiVar_WENStatus, 				sizeof( GuiVar_WENStatus ) );
		e_SHARED_string_validation( GuiVar_WENWPAUsername, 			sizeof( GuiVar_WENWPAUsername ) );

		// 6/15/2015 mpd : These are no longer visable until the user says "Y" to the change question. They don't need
		// to be set to "not set".
		//e_SHARED_string_validation( GuiVar_WENKey, 					sizeof( GuiVar_WENKey ) );
		//e_SHARED_string_validation( GuiVar_WENPassphrase, 			sizeof( GuiVar_WENPassphrase ) );
		//e_SHARED_string_validation( GuiVar_WENWPAEAPTLSCredentials,	sizeof( GuiVar_WENWPAEAPTLSCredentials ) );
		//e_SHARED_string_validation( GuiVar_WENWPAPassword, 			sizeof( GuiVar_WENWPAPassword ) );

		// 5/27/2015 mpd : TBD: These zero initializations may not be appropriate. Get valid values. 
		GuiVar_WENRadioMode 			= 0;
		GuiVar_WENRSSI 					= 0;
		GuiVar_WENSecurity 				= 0;
		GuiVar_WENWEPAuthentication 	= 0;
		GuiVar_WENWEPKeySize 			= 0;
		GuiVar_WENWEPKeyType 			= 0;
		GuiVar_WENWEPTXKey 				= 1; // There is no index 0.
		GuiVar_WENWPAAuthentication 	= 0;
		GuiVar_WENWPAEAPTLSValidateCert = 0;
		GuiVar_WENWPAEAPTTLSOption 		= 0;
		GuiVar_WENWPAEncryptionCCMP 	= 0;
		GuiVar_WENWPAEncryptionTKIP 	= 0;
		GuiVar_WENWPAEncryptionWEP 		= 0;
		GuiVar_WENWPAIEEE8021X 			= 0;
		GuiVar_WENWPAKeyType 			= 0;
		GuiVar_WENWPAPEAPOption 		= 0;
		GuiVar_WENWPAEncryption 		= 0;
		GuiVar_WENWPA2Encryption 		= 0;

		// 6/2/2015 mpd : Do not set this flag is set to "true" here. 
		// dev_state->gui_has_latest_data

		strlcpy( GuiVar_CommOptionInfoText, "", sizeof( GuiVar_CommOptionInfoText ) );
	}

	GuiVar_WENChangeKey          = (false);
	GuiVar_WENChangePassphrase   = (false);
	GuiVar_WENChangePassword     = (false);
	GuiVar_WENChangeCredentials  = (false);

}

/* ---------------------------------------------------------- */
extern void FDTO_WEN_PROGRAMMING_draw_wifi_settings( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		//lcursor_to_select = CP_WEN_WIFI_SETTINGS_SSID;
		lcursor_to_select = g_WEN_PROGRAMMING_wifi_previous_cursor_pos;
	}
	else
	{
		//lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
		lcursor_to_select = g_WEN_PROGRAMMING_wifi_previous_cursor_pos;
	}

	// 6/1/2015 mpd : The WEN Programming parent screen has alread read the values from the device. We do not need to 
	// start a device exchange here. Just get the values from the programming structure.

	GuiLib_ShowScreen( GuiStruct_scrWENWiFiSettings_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();

}

/* ---------------------------------------------------------- */
extern void WEN_PROGRAMMING_process_wifi_settings( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	UNS_32	key_size_bytes;

	BOOL_32 redraw = (false);
	BOOL_32 range_check = (false);

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_dlgKeyboard_0:
			if( ( ( pkey_event.keycode == KEY_BACK ) || ( ( pkey_event.keycode == KEY_SELECT ) && ( GuiLib_ActiveCursorFieldNo == 49 /*CP_QWERTY_KEYBOARD_OK*/ ) ) ) )
			{
				// 6/2/2015 mpd : The keyboard routines always use GuiVar_GroupName. Therefore, copy the contents of 
				// this variable back into the source when we're done.
				switch( g_WEN_PROGRAMMING_wifi_cp_using_keyboard )
				{
					case CP_WEN_WIFI_SETTINGS_SSID:
						strlcpy( GuiVar_WENSSID, GuiVar_GroupName, sizeof( GuiVar_WENSSID ) );
						break;

					case CP_WEN_WIFI_SETTINGS_WEP_KEY:
					case CP_WEN_WIFI_SETTINGS_WPA_KEY:
						strlcpy( GuiVar_WENKey, GuiVar_GroupName, sizeof( GuiVar_WENKey ) );
						range_check = (true);
						break;

					case CP_WEN_WIFI_SETTINGS_WEP_PASSPHRASE:
					case CP_WEN_WIFI_SETTINGS_WPA_PASSPHRASE:
						strlcpy( GuiVar_WENPassphrase, GuiVar_GroupName, sizeof( GuiVar_WENPassphrase ) );
						break;

					case CP_WEN_WIFI_SETTINGS_LEAP_USERNAME:
					case CP_WEN_WIFI_SETTINGS_EAP_TLS_USERNAME:
					case CP_WEN_WIFI_SETTINGS_EAP_TTLS_USERNAME:
					case CP_WEN_WIFI_SETTINGS_PEAP_USERNAME:
						strlcpy( GuiVar_WENWPAUsername, GuiVar_GroupName, sizeof( GuiVar_WENWPAUsername ) );
						break;

					case CP_WEN_WIFI_SETTINGS_LEAP_PASSWORD:
					case CP_WEN_WIFI_SETTINGS_EAP_TTLS_PASSWORD:
					case CP_WEN_WIFI_SETTINGS_PEAP_PASSWORD:
						strlcpy( GuiVar_WENWPAPassword, GuiVar_GroupName, sizeof( GuiVar_WENWPAPassword ) );
						break;

					case CP_WEN_WIFI_SETTINGS_EAP_TLS_CREDENTIALS:
						strlcpy( GuiVar_WENWPAEAPTLSCredentials, GuiVar_GroupName, sizeof( GuiVar_WENWPAEAPTLSCredentials ) );
						break;

					default:
						Alert_Message( "Wifi KB prog error");
						break;
				}
			}

			KEYBOARD_process_key( pkey_event, &FDTO_WEN_PROGRAMMING_draw_wifi_settings );

			if( range_check )
			{
				// 6/17/2015 mpd : Keyboard entry is complete and the keyboard dialog box has closed. Check the input 
				// for errors before the user can move to another cursor position. Leave all fields as-is so the user 
				// can make any necessary corrections. 
				g_WEN_PROGRAMMING_wifi_values_in_range( false );
				range_check = (false);
			}
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:

					g_WEN_PROGRAMMING_wifi_previous_cursor_pos = GuiLib_ActiveCursorFieldNo;

					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_WEN_WIFI_SETTINGS_SSID:
							e_SHARED_show_keyboard( 39, 30, GuiVar_WENSSID, ( sizeof(GuiVar_WENSSID) - 1 ), &g_WEN_PROGRAMMING_wifi_cp_using_keyboard, KEYBOARD_TYPE_QWERTY, true );
							break;

						case CP_WEN_WIFI_SETTINGS_WEP_KEY:
							key_size_bytes = ((GuiVar_WENWEPKeySize == WEN_KEY_SIZE_MAX) ? 104 : 40) / 4;

							// 6/16/2015 mpd : WEP and WPA share the same variable. Blank out the whole thing here since 
							// "e_SHARED_show_keyboard()" will only clear the space it is allowed to use.
							memset( GuiVar_WENKey, 0x00, sizeof(GuiVar_WENKey) );

							e_SHARED_show_keyboard( 41, 132, GuiVar_WENKey, key_size_bytes, &g_WEN_PROGRAMMING_wifi_cp_using_keyboard, KEYBOARD_TYPE_QWERTY__HEX_ONLY, false );
							break;

						case CP_WEN_WIFI_SETTINGS_WPA_KEY:
							e_SHARED_show_keyboard( 34, 132, GuiVar_WENKey, ( sizeof(GuiVar_WENKey) - 1 ), &g_WEN_PROGRAMMING_wifi_cp_using_keyboard, KEYBOARD_TYPE_QWERTY__HEX_ONLY, false );
							break;

						case CP_WEN_WIFI_SETTINGS_WEP_PASSPHRASE:
						case CP_WEN_WIFI_SETTINGS_WPA_PASSPHRASE:
							e_SHARED_show_keyboard( 66, 132, GuiVar_WENPassphrase, ( sizeof(GuiVar_WENPassphrase) - 1 ), &g_WEN_PROGRAMMING_wifi_cp_using_keyboard, KEYBOARD_TYPE_QWERTY, false );
							break;

						case CP_WEN_WIFI_SETTINGS_LEAP_USERNAME:
						case CP_WEN_WIFI_SETTINGS_EAP_TLS_USERNAME:
							e_SHARED_show_keyboard( 59, 116, GuiVar_WENWPAUsername, ( sizeof(GuiVar_WENWPAUsername) - 1 ), &g_WEN_PROGRAMMING_wifi_cp_using_keyboard, KEYBOARD_TYPE_QWERTY, true );
							break;

						case CP_WEN_WIFI_SETTINGS_EAP_TTLS_USERNAME:
						case CP_WEN_WIFI_SETTINGS_PEAP_USERNAME:
							e_SHARED_show_keyboard( 59, 132, GuiVar_WENWPAUsername, ( sizeof(GuiVar_WENWPAUsername) - 1 ), &g_WEN_PROGRAMMING_wifi_cp_using_keyboard, KEYBOARD_TYPE_QWERTY, true );
							break;

						case CP_WEN_WIFI_SETTINGS_LEAP_PASSWORD:
							e_SHARED_show_keyboard( 58, 148, GuiVar_WENWPAPassword, ( sizeof(GuiVar_WENWPAPassword) - 1 ), &g_WEN_PROGRAMMING_wifi_cp_using_keyboard, KEYBOARD_TYPE_QWERTY, false );
							break;

						case CP_WEN_WIFI_SETTINGS_EAP_TLS_CREDENTIALS:
							e_SHARED_show_keyboard( 66, 164, GuiVar_WENWPAEAPTLSCredentials, ( sizeof(GuiVar_WENWPAEAPTLSCredentials) - 1 ), &g_WEN_PROGRAMMING_wifi_cp_using_keyboard, KEYBOARD_TYPE_QWERTY, true );
							break;

						case CP_WEN_WIFI_SETTINGS_EAP_TTLS_PASSWORD:
						case CP_WEN_WIFI_SETTINGS_PEAP_PASSWORD:
							e_SHARED_show_keyboard( 58, 164, GuiVar_WENWPAPassword, ( sizeof(GuiVar_WENWPAPassword) - 1 ), &g_WEN_PROGRAMMING_wifi_cp_using_keyboard, KEYBOARD_TYPE_QWERTY, false );
							break;

						case CP_WEN_WIFI_SETTINGS_WEP_CHANGE_KEY:
						case CP_WEN_WIFI_SETTINGS_WPA_CHANGE_KEY:
							process_bool( &GuiVar_WENChangeKey );
							redraw = (true);
							break;

						case CP_WEN_WIFI_SETTINGS_WEP_CHANGE_PASSPHRASE:
						case CP_WEN_WIFI_SETTINGS_WPA_CHANGE_PASSPHRASE:
							process_bool( &GuiVar_WENChangePassphrase );
							redraw = (true);
							break;

						case CP_WEN_WIFI_SETTINGS_LEAP_CHANGE_PASSWORD:
						case CP_WEN_WIFI_SETTINGS_EAP_TTLS_CHANGE_PASSWORD:
						case CP_WEN_WIFI_SETTINGS_PEAP_CHANGE_PASSWORD:
							process_bool( &GuiVar_WENChangePassword );
							redraw = (true);
							break;

						case CP_WEN_WIFI_SETTINGS_EAP_TLS_VALIDATE_CERT:
							process_bool( &GuiVar_WENWPAEAPTLSValidateCert );
							redraw = (true);
							break;

						case CP_WEN_WIFI_SETTINGS_EAP_TLS_CHANGE_CREDENTIALS:
							process_bool( &GuiVar_WENChangeCredentials );
							redraw = (true);
							break;

						case CP_WEN_WIFI_SETTINGS_CCMP_ENABLED:
							// 4/1/2016 ajv : WPA doesn't support CCMP on the WiBox, so don't allow the user to make the
							// change if they're in that mode.
							if( (GuiVar_WENRadioType == COMM_DEVICE_WEN_WIBOX) && (GuiVar_WENSecurity == WEN_WIRELESS_SECURITY_WPA) )
							{
								bad_key_beep();
							}
							else
							{
								process_bool( &GuiVar_WENWPAEncryptionCCMP );
								redraw = (true);
							}
							break;

						case CP_WEN_WIFI_SETTINGS_TKIP_ENABLED:
							process_bool( &GuiVar_WENWPAEncryptionTKIP );
							redraw = (true);
							break;

						case CP_WEN_WIFI_SETTINGS_WEP_ENABLED:
							process_bool( &GuiVar_WENWPAEncryptionWEP );
							redraw = (true);
							break;

						default:
							bad_key_beep();
					}

					if( redraw )
					{
						Redraw_Screen( (false) );
					}
					else
					{
						Refresh_Screen();
					}
					break;

				case KEY_PLUS:
				case KEY_MINUS:

					g_WEN_PROGRAMMING_wifi_previous_cursor_pos = GuiLib_ActiveCursorFieldNo;

					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_WEN_WIFI_SETTINGS_RADIO_MODE:
							if( GuiVar_WENRadioType == COMM_DEVICE_WEN_PREMIERWAVE )
							{
								process_uns32( pkey_event.keycode, &GuiVar_WENRadioMode, WEN_RADIO_MODE_MIN, WEN_RADIO_MODE_MAX, 1, ( true ) );
							}
							else
							{
								// 3/29/2016 ajv : Radio mode is not editable on the WiBox
								bad_key_beep();
							}
							break;

						case CP_WEN_WIFI_SETTINGS_SECURITY_SUITE:
							process_uns32( pkey_event.keycode, &GuiVar_WENSecurity, WEN_WIRELESS_SECURITY_MIN, WEN_WIRELESS_SECURITY_MAX, 1, (false) );
							redraw = (true);
							break;

						case CP_WEN_WIFI_SETTINGS_WEP_AUTHENTICATION:
							process_uns32( pkey_event.keycode, &GuiVar_WENWEPAuthentication, WEN_WEP_AUTHENTICATION_MIN, WEN_WEP_AUTHENTICATION_MAX, 1, ( true ) );
							redraw = (true);
							break;

						case CP_WEN_WIFI_SETTINGS_WEP_KEY_TYPE:
							if( GuiVar_WENRadioType == COMM_DEVICE_WEN_PREMIERWAVE )
							{
								process_uns32( pkey_event.keycode, &GuiVar_WENWEPKeyType, WEN_KEY_TYPE_MIN, WEN_KEY_TYPE_MAX, 1, ( true ) );
								redraw = (true);
							}
							else
							{
								// 3/29/2016 ajv : We don't allow Key Type to be edited on the WiBox. Set the
								// get_wifi_guivars_from_wen_programming_struct function for a comment describing why.
								bad_key_beep();
							}
							break;

						case CP_WEN_WIFI_SETTINGS_WPA_KEY_TYPE:
							if( GuiVar_WENRadioType == COMM_DEVICE_WEN_PREMIERWAVE )
							{
								process_uns32( pkey_event.keycode, &GuiVar_WENWPAKeyType, WEN_KEY_TYPE_MIN, WEN_KEY_TYPE_MAX, 1, ( true ) );
								redraw = (true);
							}
							else
							{
								// 3/29/2016 ajv : We don't allow Key Type to be edited on the WiBox. Set the
								// get_wifi_guivars_from_wen_programming_struct function for a comment describing why.
								bad_key_beep();
							}
							break;

						case CP_WEN_WIFI_SETTINGS_WEP_KEY_SIZE:
							process_uns32( pkey_event.keycode, &GuiVar_WENWEPKeySize, WEN_KEY_SIZE_MIN, WEN_KEY_SIZE_MAX, 1, ( true ) );

							// 6/17/2015 mpd : A size change will require a key change. 
							if( g_WEN_PROGRAMMING_wifi_saved_key_size != GuiVar_WENWEPKeySize )
							{
								GuiVar_WENChangeKey = (true);
							}
							redraw = (true);
							break;

						case CP_WEN_WIFI_SETTINGS_WEP_KEY_INDEX:
							if( GuiVar_WENRadioType == COMM_DEVICE_WEN_PREMIERWAVE )
							{
								process_uns32( pkey_event.keycode, &GuiVar_WENWEPTXKey, WEN_TX_KEY_MIN, WEN_TX_KEY_MAX, 1, ( true ) );
							}
							else
							{
								// 3/29/2016 ajv : We don't allow the user to edit the key index for the WiBox
								bad_key_beep();
							}
							break;

						case CP_WEN_WIFI_SETTINGS_WPA_AUTHENTICATION:
							if( GuiVar_WENRadioType == COMM_DEVICE_WEN_PREMIERWAVE )
							{
								process_uns32( pkey_event.keycode, &GuiVar_WENWPAAuthentication, WEN_WPA_AUTHENTICATION_MIN, WEN_WPA_AUTHENTICATION_MAX, 1, ( true ) );
								redraw = (true);
							}
							else
							{
								// 3/29/2016 ajv : The WPA Authentication is not editable on the WiBox
								bad_key_beep();
							}
							break;

						case CP_WEN_WIFI_SETTINGS_CCMP_ENABLED:
							// 4/1/2016 ajv : WPA doesn't support CCMP on the WiBox, so don't allow the user to make the
							// change if they're in that mode.
							if( (GuiVar_WENRadioType == COMM_DEVICE_WEN_WIBOX) && (GuiVar_WENSecurity == WEN_WIRELESS_SECURITY_WPA) )
							{
								bad_key_beep();
							}
							else
							{
								process_bool( &GuiVar_WENWPAEncryptionCCMP );
								redraw = (true);
							}
							break;

						case CP_WEN_WIFI_SETTINGS_TKIP_ENABLED:
							process_bool( &GuiVar_WENWPAEncryptionTKIP );
							redraw = (true);
							break;

						case CP_WEN_WIFI_SETTINGS_WEP_ENABLED:
							process_bool( &GuiVar_WENWPAEncryptionWEP );
							redraw = (true);
							break;

						case CP_WEN_WIFI_SETTINGS_IEEE8021X_TYPE:
							process_uns32( pkey_event.keycode, &GuiVar_WENWPAIEEE8021X, WEN_WPA_IEEE8021X_MIN, WEN_WPA_IEEE8021X_MAX, 1, ( true ) );
							redraw = (true);
							break;

						case CP_WEN_WIFI_SETTINGS_EAP_TTLS_OPTION:
							process_uns32( pkey_event.keycode, &GuiVar_WENWPAEAPTTLSOption, WEN_EAP_TTLS_OPTION_MIN, WEN_EAP_TTLS_OPTION_MAX, 1, ( true ) );
							break;

						case CP_WEN_WIFI_SETTINGS_PEAP_OPTION:
							process_uns32( pkey_event.keycode, &GuiVar_WENWPAPEAPOption, WEN_PEAP_OPTION_MIN, WEN_PEAP_OPTION_MAX, 1, ( true ) );
							break;

						case CP_WEN_WIFI_SETTINGS_WEP_CHANGE_KEY:
						case CP_WEN_WIFI_SETTINGS_WPA_CHANGE_KEY:
							process_bool( &GuiVar_WENChangeKey );
							redraw = (true);
							break;

						case CP_WEN_WIFI_SETTINGS_WEP_CHANGE_PASSPHRASE:
						case CP_WEN_WIFI_SETTINGS_WPA_CHANGE_PASSPHRASE:
							process_bool( &GuiVar_WENChangePassphrase );
							redraw = (true);
							break;

						case CP_WEN_WIFI_SETTINGS_LEAP_CHANGE_PASSWORD:
						case CP_WEN_WIFI_SETTINGS_EAP_TTLS_CHANGE_PASSWORD:
						case CP_WEN_WIFI_SETTINGS_PEAP_CHANGE_PASSWORD:
							process_bool( &GuiVar_WENChangePassword );
							redraw = (true);
							break;

						case CP_WEN_WIFI_SETTINGS_EAP_TLS_VALIDATE_CERT:
							process_bool( &GuiVar_WENWPAEAPTLSValidateCert );
							redraw = (true);
							break;

						case CP_WEN_WIFI_SETTINGS_EAP_TLS_CHANGE_CREDENTIALS:
							process_bool( &GuiVar_WENChangeCredentials );
							redraw = (true);
							break;

						case CP_WEN_WIFI_SETTINGS_WPA_ENCRYPTION:
							process_uns32( pkey_event.keycode, &GuiVar_WENWPAEncryption, WEN_WPA_ENCRYPTION_MIN, WEN_WPA_ENCRYPTION_MAX, 1, (false) );
							break;

						case CP_WEN_WIFI_SETTINGS_WPA2_ENCRYPTION:
							process_uns32( pkey_event.keycode, &GuiVar_WENWPA2Encryption, WEN_WPA2_ENCRYPTION_MIN, WEN_WPA2_ENCRYPTION_MAX, 1, (false) );
							break;

						default:
							bad_key_beep();
					}

					if( redraw )
					{
						Redraw_Screen( false );
					}
					else
					{
						Refresh_Screen();
					}
					break;

				case KEY_C_UP:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_WEN_WIFI_SETTINGS_WEP_KEY_SIZE:
							CURSOR_Select( CP_WEN_WIFI_SETTINGS_WEP_AUTHENTICATION, (true) );
							break;

						case CP_WEN_WIFI_SETTINGS_WEP_CHANGE_KEY:
						case CP_WEN_WIFI_SETTINGS_WEP_CHANGE_PASSPHRASE:
							CURSOR_Select( CP_WEN_WIFI_SETTINGS_WEP_KEY_TYPE, (true) );
							break;

						case CP_WEN_WIFI_SETTINGS_TKIP_ENABLED:
						case CP_WEN_WIFI_SETTINGS_WEP_ENABLED:
							if( GuiVar_WENSecurity == WEN_WIRELESS_SECURITY_WEP )
							{
								// 6/17/2015 mpd : WEP
								GuiVar_WENWEPKeyType ?
									( GuiVar_WENChangePassphrase ? CURSOR_Select( CP_WEN_WIFI_SETTINGS_WEP_PASSPHRASE, (true) ) : CURSOR_Select( CP_WEN_WIFI_SETTINGS_WEP_CHANGE_PASSPHRASE, (true) ) ) :
									( GuiVar_WENChangeKey        ? CURSOR_Select( CP_WEN_WIFI_SETTINGS_WEP_KEY,        (true) ) : CURSOR_Select( CP_WEN_WIFI_SETTINGS_WEP_CHANGE_KEY,        (true) ) );
							}
							else 
							{
								// 6/17/2015 mpd : WPAX
								if( GuiVar_WENWPAAuthentication == WEN_WPA_AUTHENTICATION_PSK )
								{
									GuiVar_WENWPAKeyType ?
										( GuiVar_WENChangePassphrase ? CURSOR_Select( CP_WEN_WIFI_SETTINGS_WPA_PASSPHRASE, (true) ) : CURSOR_Select( CP_WEN_WIFI_SETTINGS_WPA_CHANGE_PASSPHRASE, (true) ) ) :
										( GuiVar_WENChangeKey        ? CURSOR_Select( CP_WEN_WIFI_SETTINGS_WPA_KEY,        (true) ) : CURSOR_Select( CP_WEN_WIFI_SETTINGS_WPA_CHANGE_KEY,        (true) ) );
								}
								else // ( GuiVar_WENWPAAuthentication == WEN_WPA_AUTHENTICATION_IEEE  )
								{
									if( GuiVar_WENWPAIEEE8021X == WEN_WPA_IEEE8021X_LEAP )
									{
										GuiVar_WENChangePassword ? CURSOR_Select( CP_WEN_WIFI_SETTINGS_LEAP_PASSWORD, (true) ) : CURSOR_Select( CP_WEN_WIFI_SETTINGS_LEAP_CHANGE_PASSWORD, (true) );
									}
									else if( GuiVar_WENWPAIEEE8021X == WEN_WPA_IEEE8021X_EAP_TLS )
									{
										GuiVar_WENChangeCredentials ? CURSOR_Select( CP_WEN_WIFI_SETTINGS_EAP_TLS_CREDENTIALS, (true) ) : CURSOR_Select( CP_WEN_WIFI_SETTINGS_EAP_TLS_CHANGE_CREDENTIALS, (true) );
									}
									else if( GuiVar_WENWPAIEEE8021X == WEN_WPA_IEEE8021X_EAP_TTLS )
									{
										GuiVar_WENChangePassword ? CURSOR_Select( CP_WEN_WIFI_SETTINGS_EAP_TTLS_PASSWORD, (true) ) : CURSOR_Select( CP_WEN_WIFI_SETTINGS_EAP_TTLS_CHANGE_PASSWORD, (true) );
									}
									else // ( GuiVar_WENWPAIEEE8021X == WEN_WPA_IEEE8021X_PEAP )
									{
										GuiVar_WENChangePassword ? CURSOR_Select( CP_WEN_WIFI_SETTINGS_PEAP_PASSWORD, (true) ) : CURSOR_Select( CP_WEN_WIFI_SETTINGS_PEAP_CHANGE_PASSWORD, (true) );
									}
								}
							}
							break;

						default:
							CURSOR_Up( (true) );
							break;
					}
					break;

				case KEY_C_LEFT:
					CURSOR_Up( (true) );
					break;

				case KEY_C_DOWN:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_WEN_WIFI_SETTINGS_WEP_KEY_TYPE:
							GuiVar_WENWEPKeyType ? CURSOR_Select( CP_WEN_WIFI_SETTINGS_WEP_CHANGE_PASSPHRASE, (true) ) : CURSOR_Select( CP_WEN_WIFI_SETTINGS_WEP_CHANGE_KEY, (true) );
							break;

						case CP_WEN_WIFI_SETTINGS_CCMP_ENABLED:
						case CP_WEN_WIFI_SETTINGS_TKIP_ENABLED:
							bad_key_beep();
							break;

						default:
							CURSOR_Down( (true) );
							break;
					}
					break;

				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_BACK:
					// 6/4/2015 mpd : Save the cursor position as the user leaves the screen. They may return.
					g_WEN_PROGRAMMING_wifi_previous_cursor_pos = GuiLib_ActiveCursorFieldNo;

					// 6/15/2015 ajv/mpd : If the keypad times out while the user is on this screen, the key
					// processing task fires off BACK keys until the screen queue reaches the Status screen. Blocking 
					// screen exits for invalid input would result in an endless loop because it would process the
					// back, display a dialog, close the dialog, and then start over by processing the back, and
					// so on. 
					// 
					// 6/16/2015 mpd : The user cannot be blocked from leaving this screen if values are out of range.
					// An error dialog will inform the user of the problem. Leave all fields as-is so the user can make 
					// any necessary corrections. 
					g_WEN_PROGRAMMING_wifi_values_in_range( false );

					GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;

					KEY_process_global_keys( pkey_event );
					break;

				default:
					// 6/15/2015 ajv : Need default key handler here so the user can press STOP, ENGLISH/SPANISH
					// and HELP.
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

