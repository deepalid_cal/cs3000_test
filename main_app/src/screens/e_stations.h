/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_E_STATIONS_H
#define _INC_E_STATIONS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern UNS_32	g_STATION_group_ID;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void STATION_update_cycle_too_short_warning( void );

// ----------

extern void FDTO_STATION_update_info_i_and_flow_status( void );

extern void FDTO_STATION_draw_info_0( const BOOL_32 pcomplete_redraw );

extern void STATION_process_station( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

// ----------

extern void nm_STATION_load_station_number_into_guivar( const INT_16 pindex_0_i16 );

extern void FDTO_STATION_return_to_menu( void );

extern void FDTO_STATION_draw_menu( const BOOL_32 pcomplete_redraw );

extern void STATION_process_menu( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

