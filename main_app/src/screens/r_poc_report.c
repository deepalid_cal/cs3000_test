/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"r_poc_report.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"e_poc.h"

#include	"e_lights.h"

#include	"poc_report_data.h"

#include	"report_utils.h"

#include	"scrollbox.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Track the previous number of POC report data lines. If the number of lines
// changes between refreshes, the entire scroll box must be redrawn. Otherwise,
// we can simply update the variables displayed on the screen.
static UNS_32 g_POC_USAGE_line_count;

static UNS_32 g_POC_USAGE_poc_count;

static UNS_32 g_POC_USAGE_index_of_poc_to_show;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Loads the POCs group name into the group name gui var. Since we are
    touching a gui var should this be a FDTO function. Maybe could get away with it not
    being so. But convieniently fits the overall report display scheme to be so. So we make
    it a FDTO function.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed ONLY within the context of the DISPLAY_PROCESS task. As it's name
    indicates. This occurs when the screen is initially drawn. And when the user uses the
    Next and Prev buttons to change which poc they're viewing.
	
	@RETURN (none)

	@ORIGINAL 2012.08.02 rmd

	@REVISIONS (none)
*/
static void FDTO_POC_USAGE_load_poc_name_into_guivar( const UNS_32 ppoc_index_0 )
{
	POC_GROUP_STRUCT	*lpoc;

	UNS_32	lgroup_id;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc = POC_get_ptr_to_physically_available_poc( ppoc_index_0 );

	// 8/2/2012 rmd : If lname is a NULL at this point it is very likely an alert line was made.
	// Though the line will not point to this area of the code. It would likely provide
	// indication within the nm_GROUP_get_name function which is called from many places.
	// Perhaps we should alert here too. And we do.
	if( lpoc != NULL )
	{
		strlcpy( GuiVar_GroupName, nm_GROUP_get_name( lpoc ), sizeof(GuiVar_GroupName) );

		lgroup_id = nm_GROUP_get_group_ID( lpoc );

		GuiVar_POCBoxIndex = POC_get_box_index_0( lgroup_id );
		GuiVar_POCType = POC_get_type_of_poc( lgroup_id );
		GuiVar_POCDecoderSN1 = POC_get_decoder_serial_number_for_this_poc_gid_and_level_0( lgroup_id, 0 );
	}
	else
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "POC: invalid result." );
		
		// 8/2/2012 rmd : Do we need to set the guivar name to something. We'll just make it a NULL
		// string. Conserve code space man!
		GuiVar_GroupName[ 0 ] = 0;
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Redraws the scroll box on the POC Report.
 *
 * @executed Executed within the context of the display processing task when the
 *  		 user changes irrigation systems.
 * 
 * @author 5/14/2012 Adrianusv
 *
 * @revisions
 *    5/14/2012 Initial release
 */
extern void FDTO_POC_USAGE_redraw_scrollbox( void )
{
	UNS_32	lcurrent_line_count;

	FDTO_POC_USAGE_load_poc_name_into_guivar( g_POC_USAGE_index_of_poc_to_show );

	lcurrent_line_count = FDTO_POC_REPORT_fill_ptrs_and_return_how_many_lines( g_POC_USAGE_index_of_poc_to_show );

	// If the number of records have changed, redraw the entire scroll box.
	// Otherwise, just update the contents of what's currently on the screen.
	FDTO_SCROLL_BOX_redraw_retaining_topline( 0, lcurrent_line_count, (lcurrent_line_count != g_POC_USAGE_line_count) );

	g_POC_USAGE_line_count = lcurrent_line_count;
}

/* ---------------------------------------------------------- */
/**
 * Draws the POC Report screen.
 * 
 * @executed Executed within the context of the display processing task when the
 *  		 screen is initially drawn.
 * 
 * @author 5/14/2012 Adrianusv
 *
 * @revisions
 *    5/14/2012 Initial release
 */
extern void FDTO_POC_USAGE_draw_report( const BOOL_32 pcomplete_redraw )
{
	GuiLib_ShowScreen( GuiStruct_rptPOCSummary_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );

	if( pcomplete_redraw == (true) )
	{
		g_POC_USAGE_poc_count = POC_populate_pointers_of_POCs_for_display( false );

		// 8/11/2014 ajv : Protect against the previous POC not longer being
		// available.
		if( g_POC_USAGE_index_of_poc_to_show >= g_POC_USAGE_poc_count )
		{
			g_POC_USAGE_index_of_poc_to_show = 0;
		}

		// 8/1/2012 rmd : Populate the title gui var. doesn't actually draw the
		// title.
		FDTO_POC_USAGE_load_poc_name_into_guivar( g_POC_USAGE_index_of_poc_to_show );
	}

	// We are intentionally using g_POC_USAGE_index_of_poc_to_show here without initializing it
	// to 0. We do this because we're using it as the global to track which POC the user was
	// looking at the last time they went to the screen. This does assume that the variable is
	// always initialized to 0 on startup, which it is in this platfrom.
	g_POC_USAGE_line_count = FDTO_POC_REPORT_fill_ptrs_and_return_how_many_lines( g_POC_USAGE_index_of_poc_to_show );

	FDTO_REPORTS_draw_report( pcomplete_redraw, g_POC_USAGE_line_count, &FDTO_POC_REPORT_load_guivars_for_scroll_line );
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while on the POC Report screen.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 *
 * @param pkey_event The key event to be processed.
 *
 * @author 5/14/2012 Adrianusv
 *
 * @revisions
 *    5/14/2012 Initial release
 */
extern void POC_USAGE_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	UNS_32	lkey;

	switch( pkey_event.keycode )
	{
		case KEY_NEXT:
		case KEY_PREV:
			if( pkey_event.keycode == KEY_NEXT )
			{
				lkey = KEY_PLUS;
			}
			else
			{
				lkey = KEY_MINUS;
			}

			// Take the POC mutex to get the list count
			xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

			process_uns32( lkey, &g_POC_USAGE_index_of_poc_to_show, 0, (g_POC_USAGE_poc_count - 1), 1, (false) );

			xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = (void*)&FDTO_POC_USAGE_redraw_scrollbox;
			Display_Post_Command( &lde );
			break;

		default:
			// Since we don't allow horizontal scrolling on this report, pass
			// 0's for the two parameters which controller the horizontal
			// scroll.
			REPORTS_process_report( pkey_event, 0, 0 );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

