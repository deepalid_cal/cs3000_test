/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_R_SYSTEM_REPORT_H
#define _INC_R_SYSTEM_REPORT_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FDTO_SYSTEM_REPORT_redraw_scrollbox( void );

extern void FDTO_SYSTEM_REPORT_draw_report( const BOOL_32 pcomplete_redraw );

extern void SYSTEM_REPORT_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

