/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_on_at_a_time.h"

#include	"app_startup.h"

#include	"combobox.h"

#include	"cursor_utils.h"

#include	"group_base_file.h"

#include	"flowsense.h"

#include	"irri_comm.h"

#include	"station_groups.h"

#include	"m_main.h"

#include	"screen_utils.h"

#include	"speaker.h"

#include	"alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_ON_AT_A_TIME_IN_GROUP_0		(0)
#define	CP_ON_AT_A_TIME_IN_GROUP_1		(1)
#define	CP_ON_AT_A_TIME_IN_GROUP_2		(2)
#define	CP_ON_AT_A_TIME_IN_GROUP_3		(3)
#define	CP_ON_AT_A_TIME_IN_GROUP_4		(4)
#define	CP_ON_AT_A_TIME_IN_GROUP_5		(5)
#define	CP_ON_AT_A_TIME_IN_GROUP_6		(6)
#define	CP_ON_AT_A_TIME_IN_GROUP_7		(7)
#define	CP_ON_AT_A_TIME_IN_GROUP_8		(8)
#define	CP_ON_AT_A_TIME_IN_GROUP_9		(9)
#define	CP_ON_AT_A_TIME_IN_SYSTEM_0		(10)
#define	CP_ON_AT_A_TIME_IN_SYSTEM_1		(11)
#define	CP_ON_AT_A_TIME_IN_SYSTEM_2		(12)
#define	CP_ON_AT_A_TIME_IN_SYSTEM_3		(13)
#define	CP_ON_AT_A_TIME_IN_SYSTEM_4		(14)
#define	CP_ON_AT_A_TIME_IN_SYSTEM_5		(15)
#define	CP_ON_AT_A_TIME_IN_SYSTEM_6		(16)
#define	CP_ON_AT_A_TIME_IN_SYSTEM_7		(17)
#define	CP_ON_AT_A_TIME_IN_SYSTEM_8		(18)
#define	CP_ON_AT_A_TIME_IN_SYSTEM_9		(19)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static UNS_32 ON_AT_A_TIME_get_max_allowed_on( void )
{
	UNS_32	rv;

	INT_32	i;

	rv = 0;

	// 10/2/2012 rmd : Using the electrical limit and the number of controllers in the chain
	// figure out the maximum allowed on. I've decided not to use the actual electrical limits
	// for each box. Though they may be available. Two reasons why. First low electrical limits
	// will cap the number here in a 'hidden' way. You'd have to know why today you can't move
	// it as far as you did yesterday. And secondly suppose they edit the electrical limit
	// later. Then the cap is 'not correct'. So what's the sense of doing it in the first place.
	// This decision may of course be re-considered. I mean then why have a variable electrical
	// limit.
	
	// ----------
	
	// 5/1/2014 rmd : If the chain is down or the comm_mngr is not in its normal token making
	// state prevent the user from seeing this screen.
	COMM_MNGR_alert_if_chain_members_should_not_be_referenced( __FILE__, __LINE__ );
	
	xSemaphoreTakeRecursive( chain_members_recursive_MUTEX, portMAX_DELAY );

	for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
	{
		if( chain.members[ i ].saw_during_the_scan )
		{
			// 10/2/2012 rmd : Note we are using the default instead of the maxiumum of 8. AJ thought
			// that would be better. I think I agree. We theorized if the electrical limit were to be
			// moved, most move it down not up. And anyway, take a stand-alone, why allow the user out
			// of the box to set an on at a time value of 8. Only to see 6 come on from the default
			// electrical limit?
			rv += ELECTRICAL_LIMIT_DEFAULT;
		}
	}

	xSemaphoreGiveRecursive( chain_members_recursive_MUTEX );

	// ----------

	if( rv > ON_AT_A_TIME_ABSOLUTE_MAXIMUM )
	{
		rv = ON_AT_A_TIME_ABSOLUTE_MAXIMUM;
	}

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Processes the PLUS and MINUS keys to change the on-at-a-time in group value.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @author 11/4/2011 Adrianusv
 *
 * @revisions
 *    11/4/2011 Initial release
 */
static void ON_AT_A_TIME_process_in_group( const UNS_32 pkeycode, UNS_32 *pin_group_guivar_ptr, UNS_32 *pin_system_guivar_ptr )
{
	UNS_32	lmax_on_in_group;

	lmax_on_in_group = ON_AT_A_TIME_get_max_allowed_on();

	// Manually process plus and minus keys to support 'X'.
	if( pkeycode == KEY_PLUS )
	{
		if( *pin_group_guivar_ptr == ON_AT_A_TIME_X )
		{
			bad_key_beep();
		}
		else if( *pin_group_guivar_ptr == lmax_on_in_group )
		{
			good_key_beep();
			*pin_group_guivar_ptr = ON_AT_A_TIME_X;
		}
		else if( *pin_group_guivar_ptr < lmax_on_in_group )
		{
			good_key_beep();
			*pin_group_guivar_ptr += 1;
		}
	}
	else // if( pkeycode == KEY_MINUS )
	{
		if( *pin_group_guivar_ptr == ON_AT_A_TIME_X )
		{
			good_key_beep();
			*pin_group_guivar_ptr = lmax_on_in_group;
		}
		else if( *pin_group_guivar_ptr > 1 )
		{
			good_key_beep();
			*pin_group_guivar_ptr -= 1;
		}
		else
		{
			bad_key_beep();
		}
	}

	// We don't allow the "in group" setting to be larger than in "in system"
	// setting, so adjust it accordingly.
	if( *pin_group_guivar_ptr != ON_AT_A_TIME_X )
	{
		if( *pin_group_guivar_ptr > *pin_system_guivar_ptr )
		{
			*pin_system_guivar_ptr = *pin_group_guivar_ptr;
		}
	}
}

/* ---------------------------------------------------------- */
/**
 * Processes the PLUS and MINUS keys to change the on-at-a-time in system value.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @author 11/4/2011 Adrianusv
 *
 * @revisions
 *    11/4/2011 Initial release
 */
static void ON_AT_A_TIME_process_in_system( const UNS_32 pkeycode, UNS_32 *pin_group_guivar_ptr, UNS_32 *pin_system_guivar_ptr )
{
	UNS_32	lmax_on_in_system;

	lmax_on_in_system = ON_AT_A_TIME_get_max_allowed_on();

	// Manually process plus and minus keys to support 'X'.
	if( pkeycode == KEY_PLUS )
	{
		if( *pin_system_guivar_ptr == ON_AT_A_TIME_X )
		{
			bad_key_beep();
		}
		else if( *pin_system_guivar_ptr == lmax_on_in_system )
		{
			good_key_beep();
			*pin_system_guivar_ptr = ON_AT_A_TIME_X;
		}
		else if( *pin_system_guivar_ptr < lmax_on_in_system )
		{
			good_key_beep();
			*pin_system_guivar_ptr += 1;
		}
		else
		{
			bad_key_beep();
		}
	}
	else // if( pkeycode == KEY_MINUS )
	{
		if( *pin_system_guivar_ptr == ON_AT_A_TIME_X )
		{
			good_key_beep();
			*pin_system_guivar_ptr = lmax_on_in_system;
		}
		else if( *pin_system_guivar_ptr > lmax_on_in_system )
		{
			good_key_beep();
			*pin_system_guivar_ptr = ON_AT_A_TIME_X;
		}
		else if( *pin_system_guivar_ptr > 1 )
		{
			good_key_beep();
			*pin_system_guivar_ptr -= 1;
		}
		else
		{
			bad_key_beep();
		}
	}

	// We don't allow the "in group" setting to be larger than in "in system"
	// setting, so adjust it accordingly.
	if( (*pin_system_guivar_ptr != ON_AT_A_TIME_X) && (*pin_group_guivar_ptr != ON_AT_A_TIME_X) )
	{
		if ( *pin_group_guivar_ptr > *pin_system_guivar_ptr )
		{
			*pin_group_guivar_ptr = *pin_system_guivar_ptr;
		}
	}
}

/* ---------------------------------------------------------- */
/**
 * Determine if the info panel should be displayed on the screen.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param none
 *
 * @author 6/25/2015 mpd
 *
 * @revisions
 *    6/25/2015 Initial release
 */
static void ON_AT_A_TIME_handle_info_panel()
{
	// 6/25/2015 mpd : FogBugz #2975. If any settings are displayed as "--", turn on the info panel to tell the user 
	// what it means.
	if( ( GuiVar_GroupSettingA_0 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingA_1 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingA_2 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingA_3 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingA_4 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingA_5 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingA_6 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingA_7 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingA_8 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingA_9 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingB_0 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingB_1 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingB_2 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingB_3 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingB_4 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingB_5 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingB_6 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingB_7 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingB_8 == ON_AT_A_TIME_X ) ||
		( GuiVar_GroupSettingB_9 == ON_AT_A_TIME_X ) )
	{
		GuiVar_OnAtATimeInfoDisplay = true;
	}
	else
	{
		GuiVar_OnAtATimeInfoDisplay = false;
	}

}

/* ---------------------------------------------------------- */
extern void FDTO_ON_AT_A_TIME_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		ON_AT_A_TIME_copy_group_into_guivars();

		lcursor_to_select = 0;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	// 6/25/2015 mpd : FogBugz #2975. Display the info panel if necessary. 
	ON_AT_A_TIME_handle_info_panel();

	GuiLib_ShowScreen( GuiStruct_scrOnAtATime_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Processes the key event from the irrigation On-at-a-Time screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 11/4/2011 Adrianusv
 *
 * @revisions
 *    11/4/2011 Initial release
 */
extern void ON_AT_A_TIME_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		case KEY_MINUS:
		case KEY_PLUS:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_ON_AT_A_TIME_IN_GROUP_0:
					ON_AT_A_TIME_process_in_group( pkey_event.keycode, &GuiVar_GroupSettingA_0, &GuiVar_GroupSettingB_0 ); 
					break;

				case CP_ON_AT_A_TIME_IN_GROUP_1:
					ON_AT_A_TIME_process_in_group( pkey_event.keycode, &GuiVar_GroupSettingA_1, &GuiVar_GroupSettingB_1 );
					break;

				case CP_ON_AT_A_TIME_IN_GROUP_2:
					ON_AT_A_TIME_process_in_group( pkey_event.keycode, &GuiVar_GroupSettingA_2, &GuiVar_GroupSettingB_2 );
					break;

				case CP_ON_AT_A_TIME_IN_GROUP_3:
					ON_AT_A_TIME_process_in_group( pkey_event.keycode, &GuiVar_GroupSettingA_3, &GuiVar_GroupSettingB_3 );
					break;

				case CP_ON_AT_A_TIME_IN_GROUP_4:
					ON_AT_A_TIME_process_in_group( pkey_event.keycode, &GuiVar_GroupSettingA_4, &GuiVar_GroupSettingB_4 );
					break;

				case CP_ON_AT_A_TIME_IN_GROUP_5:
					ON_AT_A_TIME_process_in_group( pkey_event.keycode, &GuiVar_GroupSettingA_5, &GuiVar_GroupSettingB_5 );
					break;

				case CP_ON_AT_A_TIME_IN_GROUP_6:
					ON_AT_A_TIME_process_in_group( pkey_event.keycode, &GuiVar_GroupSettingA_6, &GuiVar_GroupSettingB_6 );
					break;

				case CP_ON_AT_A_TIME_IN_GROUP_7:
					ON_AT_A_TIME_process_in_group( pkey_event.keycode, &GuiVar_GroupSettingA_7, &GuiVar_GroupSettingB_7 );
					break;

				case CP_ON_AT_A_TIME_IN_GROUP_8:
					ON_AT_A_TIME_process_in_group( pkey_event.keycode, &GuiVar_GroupSettingA_8, &GuiVar_GroupSettingB_8 );
					break;

				case CP_ON_AT_A_TIME_IN_GROUP_9:
					ON_AT_A_TIME_process_in_group( pkey_event.keycode, &GuiVar_GroupSettingA_9, &GuiVar_GroupSettingB_9 );
					break;

				case CP_ON_AT_A_TIME_IN_SYSTEM_0:
					ON_AT_A_TIME_process_in_system( pkey_event.keycode, &GuiVar_GroupSettingA_0, &GuiVar_GroupSettingB_0 );
					break;

				case CP_ON_AT_A_TIME_IN_SYSTEM_1:
					ON_AT_A_TIME_process_in_system( pkey_event.keycode, &GuiVar_GroupSettingA_1, &GuiVar_GroupSettingB_1 );
					break;

				case CP_ON_AT_A_TIME_IN_SYSTEM_2:
					ON_AT_A_TIME_process_in_system( pkey_event.keycode, &GuiVar_GroupSettingA_2, &GuiVar_GroupSettingB_2 );
					break;

				case CP_ON_AT_A_TIME_IN_SYSTEM_3:
					ON_AT_A_TIME_process_in_system( pkey_event.keycode, &GuiVar_GroupSettingA_3, &GuiVar_GroupSettingB_3 );
					break;

				case CP_ON_AT_A_TIME_IN_SYSTEM_4:
					ON_AT_A_TIME_process_in_system( pkey_event.keycode, &GuiVar_GroupSettingA_4, &GuiVar_GroupSettingB_4 );
					break;

				case CP_ON_AT_A_TIME_IN_SYSTEM_5:
					ON_AT_A_TIME_process_in_system( pkey_event.keycode, &GuiVar_GroupSettingA_5, &GuiVar_GroupSettingB_5 );
					break;

				case CP_ON_AT_A_TIME_IN_SYSTEM_6:
					ON_AT_A_TIME_process_in_system( pkey_event.keycode, &GuiVar_GroupSettingA_6, &GuiVar_GroupSettingB_6 );
					break;

				case CP_ON_AT_A_TIME_IN_SYSTEM_7:
					ON_AT_A_TIME_process_in_system( pkey_event.keycode, &GuiVar_GroupSettingA_7, &GuiVar_GroupSettingB_7 );
					break;

				case CP_ON_AT_A_TIME_IN_SYSTEM_8:
					ON_AT_A_TIME_process_in_system( pkey_event.keycode, &GuiVar_GroupSettingA_8, &GuiVar_GroupSettingB_8 );
					break;

				case CP_ON_AT_A_TIME_IN_SYSTEM_9:
					ON_AT_A_TIME_process_in_system( pkey_event.keycode, &GuiVar_GroupSettingA_9, &GuiVar_GroupSettingB_9 );
					break;

				default:
					bad_key_beep();
			}
			Redraw_Screen( (false) );
			break;

		case KEY_PREV:
		case KEY_C_UP:
			if( (GuiLib_ActiveCursorFieldNo % 10) == 0 )
			{
				bad_key_beep();
			}
			else
			{
				CURSOR_Up( (true) );
			}
			break;

		case KEY_NEXT:
		case KEY_C_DOWN:
			if( (UNS_32)(GuiLib_ActiveCursorFieldNo % 10) == (STATION_GROUP_get_num_groups_in_use() - 1) )
			{
				bad_key_beep();
			}
			else
			{
				CURSOR_Down( (true) );
			}
			break;

		case KEY_C_LEFT:
			if( (GuiLib_ActiveCursorFieldNo >= CP_ON_AT_A_TIME_IN_GROUP_0) && (GuiLib_ActiveCursorFieldNo <= CP_ON_AT_A_TIME_IN_GROUP_9) )
			{
				CURSOR_Select( (UNS_32)((GuiLib_ActiveCursorFieldNo + 10) - 1), (true) );
			}
			else if( (GuiLib_ActiveCursorFieldNo >= CP_ON_AT_A_TIME_IN_SYSTEM_0) && (GuiLib_ActiveCursorFieldNo <= CP_ON_AT_A_TIME_IN_SYSTEM_9) )
			{
				CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo - 10), (true) );
			}
			else
			{
				bad_key_beep();
			}
			break;

		case KEY_C_RIGHT:
			if( (GuiLib_ActiveCursorFieldNo >= CP_ON_AT_A_TIME_IN_GROUP_0) && (GuiLib_ActiveCursorFieldNo <= CP_ON_AT_A_TIME_IN_GROUP_9) )
			{
				CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo + 10), (true) );
			}
			else if( (GuiLib_ActiveCursorFieldNo >= CP_ON_AT_A_TIME_IN_SYSTEM_0) && (GuiLib_ActiveCursorFieldNo <= CP_ON_AT_A_TIME_IN_SYSTEM_9) )
			{
				CURSOR_Select( (UNS_32)((GuiLib_ActiveCursorFieldNo - 10) + 1), (true) );
			}
			else
			{
				bad_key_beep();
			}
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = CP_MAIN_MENU_MAIN_LINES;
			ON_AT_A_TIME_extract_and_store_changes_from_GuiVars();
			// No need to break here to allow this to fall into the default
			// condition.

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

