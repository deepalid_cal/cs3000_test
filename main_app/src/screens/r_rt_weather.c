/* ---------------------------------------------------------- */

#include	"r_rt_weather.h"

#include	"app_startup.h"

#include	"comm_mngr.h"

#include	"configuration_network.h"

#include	"controller_initiated.h"

#include	"cursor_utils.h"

#include	"d_live_screens.h"

#include	"e_weather_sensors.h"

#include	"GuiLib.h"

#include	"k_process.h"

#include	"screen_utils.h"

#include	"speaker.h"

#include	"tpmicro_data.h"

#include	"weather_control.h"




/* ---------------------------------------------------------- */

#define CP_RT_WEATHER_CLEAR_RUNAWAY_GAGE		(0)

/* ---------------------------------------------------------- */
extern void FDTO_REAL_TIME_WEATHER_update_report( void )
{
	BOOL_32		lforce_complete_redraw;

	lforce_complete_redraw = (false);

	// ---------------------

	NETWORK_CONFIG_get_controller_name_str_for_ui( WEATHER_get_et_gage_box_index(), (char*)&GuiVar_StatusETGageControllerName );

	NETWORK_CONFIG_get_controller_name_str_for_ui( WEATHER_get_rain_bucket_box_index(), (char*)&GuiVar_StatusRainBucketControllerName );

	NETWORK_CONFIG_get_controller_name_str_for_ui( WEATHER_get_wind_gage_box_index(), (char*)&GuiVar_StatusWindGageControllerName );

	// ---------------------

	xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );

	GuiVar_StatusETGageReading = (float)(weather_preserves.et_rip.et_inches_u16_10000u / 10000.0);

	if( GuiVar_ETGageRunawayGage != weather_preserves.run_away_gage )
	{
		GuiVar_ETGageRunawayGage = weather_preserves.run_away_gage;

		// 8/28/2014 ajv : Since the state of the runaway gage changed, force a
		// complete redraw to show or hide the button.
		lforce_complete_redraw = (true);
	}

	// 9/25/2012 rmd : Show the actual amount of rain the bucket has seen. Not
	// the 'massaged' amount headed for the table.
	GuiVar_StatusRainBucketReading = (float)(weather_preserves.rain.midnight_to_midnight_raw_pulse_count / 100.0);

	// 10/13/2014 ajv : There's no need to set GuiVar_StatusWindGageReading
	// here. This is handled as part of the incoming token from the master.

	GuiVar_StatusRainSwitchState = weather_preserves.rain_switch_active;

	GuiVar_StatusFreezeSwitchState = weather_preserves.freeze_switch_active;

	xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );

	// ---------------------

	if( lforce_complete_redraw == (true) )
	{
		FDTO_Redraw_Screen( (true) );
	}
	else
	{
		GuiLib_Refresh();
	}
}

/* ---------------------------------------------------------- */
extern void FDTO_REAL_TIME_WEATHER_draw_report( BOOL_32 pcomplete_redraw )
{
	INT_32	lcursor_to_show;

	// 8/29/2014 ajv : In order to force a full redraw after the user closes the
	// dialog box indicating that a runaway away gage has been cleared, check to
	// see the state of the GuiVar compared to what's in weather_preserves.
	if( (pcomplete_redraw == (true)) || (GuiVar_ETGageRunawayGage != weather_preserves.run_away_gage) )
	{
		if( weather_preserves.run_away_gage == (true) )
		{
			lcursor_to_show = CP_RT_WEATHER_CLEAR_RUNAWAY_GAGE;
		}
		else
		{
			lcursor_to_show = GuiLib_NO_CURSOR;
		}

		GuiLib_ShowScreen( GuiStruct_rptRTWeather_0, lcursor_to_show, GuiLib_RESET_AUTO_REDRAW );
	}

	FDTO_REAL_TIME_WEATHER_update_report();
}

/* ---------------------------------------------------------- */
extern void REAL_TIME_WEATHER_process_report( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_RT_WEATHER_CLEAR_RUNAWAY_GAGE:
					if( GuiVar_ETGageRunawayGage == (true) )
					{
						CURSOR_Select( 0, (false) );

						ET_GAGE_clear_runaway_gage();
					}
					else
					{
						bad_key_beep();
					}
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;

			KEY_process_global_keys( pkey_event );

			if( GuiVar_MenuScreenToShow == SCREEN_MENU_DIAGNOSTICS )
			{
				// 3/16/2016 ajv : Since the user got here from the LIVE SCREENS dialog box, redraw that
				// dialog box.
				LIVE_SCREENS_draw_dialog( (true) );
			}
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

