/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	<string.h>

#include	"e_hub.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"change.h"

#include	"configuration_controller.h"

#include	"combobox.h"

#include	"controller_initiated.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"flash_storage.h"

#include	"flowsense.h"

#include	"irri_comm.h"

#include	"m_main.h"

#include	"packet_router.h"

#include	"shared.h"

#include	"speaker.h"

#include	"textbox.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_HUB_IS_A_HUB				(0)
#define CP_HUB_VIEW_HUB_LIST_BUTTON	(1)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

UNS_32	g_HUB_cursor_position_when_dialog_displayed;

BOOL_32	g_HUB_dialog_visible;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_HUB_show_is_a_hub_dropdown( void )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( 163, 72, GuiVar_IsAHub );
}

/* ---------------------------------------------------------- */
static void FDTO_HUB_show_hub_list( void )
{
	HUB_LIST_ADDR_UNION	hlau;

	BOOL_32	lnow_displaying_et2000e;

	UNS_32	lhow_many;

	UNS_32	i;

	// ----------

	lnow_displaying_et2000e = (false);

	memset( GuiVar_HubList, 0x00, sizeof(GuiVar_HubList) );

	// ----------

	// 4/30/2017 rmd : The hub list queue is protected by a MUTEX. Always need to take before 
	// working with this queue.
	xSemaphoreTake( router_hub_list_MUTEX, portMAX_DELAY );

	lhow_many = uxQueueMessagesWaiting( router_hub_list_queue );

	if( lhow_many > 0 )
	{
		good_key_beep();

		for( i = 0; i < lhow_many; ++i )
		{
			// 6/22/2017 ajv : If we go through the entire queue pulling from the front and writing to
			// the back the queue will remain in its original order with the same number of items. Keep
			// in mind, we ALWAYS have to run through the ENTIRE list.

			// 6/19/2017 rmd : Do not wait for a queue item, there is no task adding to the queue except
			// for when a message comes from the commserver. Our goal is to pull the items that are on
			// the queue at this time. Not wait for future items that are added to the list.
			if( xQueueReceive( router_hub_list_queue, &hlau, 0 ) )
			{
				// 4/30/2017 rmd : Did we hit the divider between 3000 and 2000?
				if( hlau.sn_3000 == 0xffffffff )
				{
					lnow_displaying_et2000e = (true);
				}
				else if( lnow_displaying_et2000e )
				{
					sp_strlcat( GuiVar_HubList, sizeof(GuiVar_HubList), "%c%c%c\n", hlau.list_addr[0], hlau.list_addr[1], hlau.list_addr[2] );
				}
				else
				{
					sp_strlcat( GuiVar_HubList, sizeof(GuiVar_HubList), "%d\n", hlau.sn_3000 );
				}

				// 6/22/2017 ajv : Always write them back onto the queue. Do not wait if the queue is full,
				// nothing is removing items.
				xQueueSendToBack( router_hub_list_queue, &hlau, 0 );
			}
			else
			{
				// 4/30/2017 rmd : This should NEVER happen .... i think.
				Alert_Message( "HUB: unexp hub list queue ERROR" );
			}
		}
	}
	else
	{
		bad_key_beep();
	}

	xSemaphoreGive( router_hub_list_MUTEX );

	// 6/22/2017 ajv : Since the GuiLib calls are very slow and intensive, call them after 
	// releasing the hub list mutex. 
	if( lhow_many > 0 )
	{
		// 6/22/2017 ajv : The following two variables are used in dialog.c when closing all 
		// dialogs. 
		g_HUB_dialog_visible = (true);

		g_HUB_cursor_position_when_dialog_displayed = CP_HUB_VIEW_HUB_LIST_BUTTON;

		GuiLib_ShowScreen( GuiStruct_dlgHubList_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW ); 
		GuiLib_Refresh();
	}

}

/* ---------------------------------------------------------- */
static void HUB_extract_and_store_changes_from_GuiVars( void )
{
	if( GuiVar_IsAHub != config_c.hub_enabled_user_setting ) 
	{
		SHARED_set_bool_controller( &config_c.hub_enabled_user_setting,
									GuiVar_IsAHub,
									(true),
									(false), //(true), COMMENTED OUT TEMPORARILY TO SKIP ALERT LINE
									CHANGE_CONTROLLER_ACTS_AS_A_HUB, 
									CHANGE_REASON_KEYPAD,
								    FLOWSENSE_get_controller_index(),
									(false),
									NULL,
									NULL,
									0,
									"Acts as a Hub"
								 );

		// 6/28/2017 ajv : Schedule a save rather than saving immediately to prevent stacking of
		// saves.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_CONTROLLER_CONFIGURATION, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change ); 

		// 6/28/2017 ajv : Next trigger the configuration change to be sent to the master when the
		// next token arrives.
		xSemaphoreTakeRecursive( irri_comm_recursive_MUTEX, portMAX_DELAY );
		irri_comm.send_box_configuration_to_master = (true);
		xSemaphoreGiveRecursive( irri_comm_recursive_MUTEX );

		// 6/28/2017 ajv : And finally notify the CI task to update this controller's registration.
		CONTROLLER_INITIATED_update_comm_server_registration_info();
	}
}

/* ---------------------------------------------------------- */
extern void FDTO_HUB_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		GuiVar_IsAHub = config_c.hub_enabled_user_setting;

		lcursor_to_select = CP_HUB_IS_A_HUB;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrHub_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void HUB_process_screen( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_cbxNoYes_0:
			COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_IsAHub );
			break;

		case GuiStruct_dlgHubList_0:
			switch( pkey_event.keycode )
			{
				case KEY_BACK:
					good_key_beep();
					DIALOG_close_all_dialogs();
					break;

				case KEY_C_UP:
				case KEY_C_DOWN:
					TEXT_BOX_up_or_down( 0, pkey_event.keycode );
					break;

				default:
					KEY_process_global_keys( pkey_event );
			}
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_HUB_IS_A_HUB:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_HUB_show_is_a_hub_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_HUB_VIEW_HUB_LIST_BUTTON:
							// 6/22/2017 ajv : Don't process the good key beep here in case the list is empty.
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_HUB_show_hub_list;
							Display_Post_Command( &lde );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch ( GuiLib_ActiveCursorFieldNo )
					{
						case CP_HUB_IS_A_HUB:
							process_bool( &GuiVar_IsAHub ); 

							// 6/21/2017 ajv : Since changing this value causes information to appear and disappear,
							// redraw the screen.
							Redraw_Screen( (false) );
							break;

						default:
							bad_key_beep();
					}
					Refresh_Screen();
					break;

				case KEY_C_UP:
				case KEY_C_LEFT:
					CURSOR_Up( (true) );
					break;

				case KEY_C_DOWN:
				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				default:
					if( pkey_event.keycode == KEY_BACK )
					{
						// 6/21/2017 ajv : Save the change
						HUB_extract_and_store_changes_from_GuiVars();

						GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;
					}

					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

