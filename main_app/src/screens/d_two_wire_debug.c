/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"d_two_wire_debug.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"d_two_wire_discovery.h"

#include	"e_assign_decoder_sn.h"

#include	"guilib.h"

#include    "r_decoder_stats.h"

#include	"screen_utils.h"

#include	"speaker.h"

#include	"two_wire_utils.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_TWO_WIRE_DEBUG_ASSIGN_SERIAL_NUMBER	(50)
#define	CP_TWO_WIRE_DEBUG_DECODER_STATISTICS	(51)
#define	CP_TWO_WIRE_DEBUG_ACTIVATE_PATH			(52)
#define	CP_TWO_WIRE_DEBUG_DEACTIVATE_PATH		(53)
#define	CP_TWO_WIRE_DEBUG_PERFORM_LOOPBACK_TEST	(54)
#define	CP_TWO_WIRE_DEBUG_STOP_LOOPBACK_TEST	(55)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

UNS_32	g_TWO_WIRE_DEBUG_cursor_position_when_dialog_displayed;

BOOL_32	g_TWO_WIRE_DEBUG_dialog_visible;

// 6/5/2015 ajv : Keep track of the last cursor position the user had selected on the dialog
// so it brings them back to the same field when they open the dialog again.
static UNS_32 g_TWO_WIRE_DEBUG_last_cursor_position;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_TWO_WIRE_DEBUG_draw_dialog( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	g_TWO_WIRE_DEBUG_cursor_position_when_dialog_displayed = (UNS_32)GuiLib_ActiveCursorFieldNo;

	if( pcomplete_redraw == (true) )
	{
		// 6/5/2015 ajv : If we've never entered the screen before, the last cursor position will be
		// uninitialized at 0. In that case, set it to the top item in the list.
		if( g_TWO_WIRE_DEBUG_last_cursor_position == 0 )
		{
			g_TWO_WIRE_DEBUG_last_cursor_position = CP_TWO_WIRE_DEBUG_ASSIGN_SERIAL_NUMBER;
		}

		lcursor_to_select = g_TWO_WIRE_DEBUG_last_cursor_position;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	g_TWO_WIRE_DEBUG_dialog_visible = (true);

	GuiLib_ShowScreen( GuiStruct_dlgTwoWireDebugMenu_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void TWO_WIRE_DEBUG_draw_dialog( const BOOL_32 pcomplete_redraw )
{
	DISPLAY_EVENT_STRUCT	lde;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void*)&FDTO_TWO_WIRE_DEBUG_draw_dialog;
	lde._06_u32_argument1 = pcomplete_redraw;
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
extern void TWO_WIRE_DEBUG_process_dialog( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			// 6/5/2015 ajv : Retain the last cursor position the user was on so they go back to it when
			// they open the dialog again.
			g_TWO_WIRE_DEBUG_last_cursor_position = GuiLib_ActiveCursorFieldNo;

			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TWO_WIRE_DEBUG_ASSIGN_SERIAL_NUMBER:
					g_TWO_WIRE_DEBUG_dialog_visible = (false);

					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._02_menu = SCREEN_MENU_SETUP;
					lde._03_structure_to_draw = GuiStruct_scrTwoWireAssignSN_0;
					lde._04_func_ptr = (void *)&FDTO_TWO_WIRE_draw_assign_sn;
					lde._06_u32_argument1 = (true);
					lde._08_screen_to_draw = GuiVar_MenuScreenToShow;
					lde.key_process_func_ptr = TWO_WIRE_process_assign_sn;
					Change_Screen( &lde );
					break;

				case CP_TWO_WIRE_DEBUG_DECODER_STATISTICS:
					g_TWO_WIRE_DEBUG_dialog_visible = (false);

					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._02_menu = SCREEN_MENU_SETUP;
					lde._03_structure_to_draw = GuiStruct_rptDecoderStats_0;
					lde._04_func_ptr = (void *)&FDTO_TWO_WIRE_draw_decoder_stats_report;
					lde._06_u32_argument1 = (true);
					lde._08_screen_to_draw = GuiVar_MenuScreenToShow;
					lde.key_process_func_ptr = TWO_WIRE_process_decoder_stats_report;
					Change_Screen( &lde );
					break;

				case CP_TWO_WIRE_DEBUG_ACTIVATE_PATH:
					TWO_WIRE_turn_cable_power_on_or_off_from_ui( (true) );
					break;

				case CP_TWO_WIRE_DEBUG_DEACTIVATE_PATH:
					TWO_WIRE_turn_cable_power_on_or_off_from_ui( (false) );
					break;

				case CP_TWO_WIRE_DEBUG_PERFORM_LOOPBACK_TEST:
					TWO_WIRE_all_decoder_loopback_test_from_ui( (true) );
					break;

				case CP_TWO_WIRE_DEBUG_STOP_LOOPBACK_TEST:
					TWO_WIRE_all_decoder_loopback_test_from_ui( (false) );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_C_UP:
			CURSOR_Up( (true) );
			break;

		case KEY_C_DOWN:
			CURSOR_Down( (true) );
			break;

		case KEY_BACK:
			good_key_beep();

			// 6/5/2015 ajv : Retain the last cursor position the user was on so they go back to it when
			// they open the dialog again.
			g_TWO_WIRE_DEBUG_last_cursor_position = GuiLib_ActiveCursorFieldNo;

			g_TWO_WIRE_DEBUG_dialog_visible = (false);

			// 3/13/2014 ajv : Return to the last cursor position the user was on when they pressed BACK
			// and redraw the screen to remove the dialog box.
			GuiLib_ActiveCursorFieldNo = (INT_16)g_TWO_WIRE_DEBUG_cursor_position_when_dialog_displayed;

			Redraw_Screen( (false) );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

