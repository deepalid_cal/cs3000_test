/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_freeze_switch.h"

#include	"app_startup.h"

#include	"change.h"

#include	"combobox.h"

#include	"configuration_network.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"flowsense.h"

#include	"irri_comm.h"

#include	"m_main.h"

#include	"scrollbox.h"

#include	"speaker.h"

#include	"weather_control.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_FREEZE_SWITCH_IN_USE					(0)
#define	CP_FREEZE_SWITCH_SERIAL_NUMBER_LIST		(1)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static struct {
	BOOL_32 in_use;
	UNS_32 	chain_index;
}					g_FREEZE_SWITCH[ MAX_CHAIN_LENGTH ];

static	UNS_32		g_FREEZE_SWITCH_count;

static	UNS_32		g_FREEZE_SWITCH_active_line;

static	UNS_32		*g_FREEZE_SWITCH_combo_box_guivar;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FREEZE_SWITCH_populate_freeze_switch_scrollbox( const INT_16 pline_index_0 )
{
	UNS_32 chain_index;

	// ----------

	chain_index = g_FREEZE_SWITCH[pline_index_0].chain_index;

	// ----------

	NETWORK_CONFIG_get_controller_name_str_for_ui( chain_index, (char*)&GuiVar_FreezeSwitchControllerName );

	GuiVar_FreezeSwitchSelected = g_FREEZE_SWITCH[ pline_index_0 ].in_use;
}

/* ---------------------------------------------------------- */
static void FDTO_FREEZE_SWITCH_draw_freeze_switch_scrollbox( const BOOL_32 pcomplete_redraw )
{
	UNS_32	i;

	INT_32	lactive_line;

	WHATS_INSTALLED_STRUCT *pwi;

	if( GuiVar_FreezeSwitchInUse == (true) )
	{
		if( (pcomplete_redraw == (true)) || (GuiLib_ActiveCursorFieldNo == CP_FREEZE_SWITCH_IN_USE) )
		{
			lactive_line = GuiLib_NO_CURSOR;

			g_FREEZE_SWITCH_count = 0;

			xSemaphoreTakeRecursive( chain_members_recursive_MUTEX, portMAX_DELAY );

			xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

			// 5/1/2014 rmd : If the chain is down or the comm_mngr is not in its normal token making
			// state prevent the user from seeing this screen.
			COMM_MNGR_alert_if_chain_members_should_not_be_referenced( __FILE__, __LINE__ );
			
			for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
			{
				if( chain.members[ i ].saw_during_the_scan == (true) )
				{
					// 6/15/2016 skc : Only add to the list if box has a valid HW configuration.
					// The chain item needs to have both Weather card and terminal block.
					pwi = &chain.members[ i ].box_configuration.wi;

					if( pwi->weather_card_present && pwi->weather_terminal_present )
					{
						g_FREEZE_SWITCH[ g_FREEZE_SWITCH_count ].in_use = WEATHER_get_freeze_switch_connected_to_this_controller( i );
						g_FREEZE_SWITCH[ g_FREEZE_SWITCH_count ].chain_index = i;
						g_FREEZE_SWITCH_count++;
					}
				}
			}

			xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

			xSemaphoreGiveRecursive( chain_members_recursive_MUTEX );
		}
		else
		{
			if( GuiLib_ActiveCursorFieldNo == CP_FREEZE_SWITCH_IN_USE )
			{
				lactive_line = GuiLib_NO_CURSOR;
			}
			else
			{
				lactive_line = (INT_32)g_FREEZE_SWITCH_active_line;
			}
		}

		GuiLib_ScrollBox_Close( 1 );
		GuiLib_ScrollBox_Init( 1, &FREEZE_SWITCH_populate_freeze_switch_scrollbox, (INT_16)g_FREEZE_SWITCH_count, (INT_16)lactive_line );
	}
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
static void FDTO_FREEZE_SWITCH_enter_freeze_switch_scrollbox( void )
{
	good_key_beep();

	GuiLib_ScrollBox_Close( 1 );
	GuiLib_ScrollBox_Init( 1, &FREEZE_SWITCH_populate_freeze_switch_scrollbox, (INT_16)g_FREEZE_SWITCH_count, 0 );

	GuiLib_Cursor_Select( CP_FREEZE_SWITCH_SERIAL_NUMBER_LIST );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
static void FDTO_FREEZE_SWITCH_leave_freeze_switch_scrollbox( void )
{
	GuiLib_ScrollBox_Close( 1 );
	GuiLib_ScrollBox_Init( 1, &FREEZE_SWITCH_populate_freeze_switch_scrollbox, (INT_16)g_FREEZE_SWITCH_count, GuiLib_NO_CURSOR );

	FDTO_Cursor_Up( (true) );
}

/* ---------------------------------------------------------- */
/**
 * Draws the Freeze Switch in use dropdown window atop the main window.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the users presses SELECT to change the Freeze Switch in use setting.
 *
 * @author 6/14/2013 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_FREEZE_SWITCH_show_freeze_switch_in_use_dropdown( void )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( 199, 18, *g_FREEZE_SWITCH_combo_box_guivar );
}

/* ---------------------------------------------------------- */
static void FDTO_FREEZE_SWITCH_show_switch_connected_dropdown( const UNS_32 px_coord, const UNS_32 py_coord )
{
	g_FREEZE_SWITCH_active_line = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 1, 0 );

	FDTO_COMBO_BOX_show_no_yes_dropdown( px_coord, py_coord, *g_FREEZE_SWITCH_combo_box_guivar );
}

/* ---------------------------------------------------------- */
static void FDTO_FREEZE_SWITCH_close_freeze_switch_connected_dropdown( void )
{
	UNS_32 index;

	index = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 1, 0 );

	*g_FREEZE_SWITCH_combo_box_guivar = (BOOL_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );

	nm_WEATHER_set_freeze_switch_connected( *g_FREEZE_SWITCH_combo_box_guivar, g_FREEZE_SWITCH[index].chain_index, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, WEATHER_get_change_bits_ptr( CHANGE_REASON_KEYPAD ) );

	FDTO_COMBOBOX_hide();
}

/* ---------------------------------------------------------- */
extern void FDTO_FREEZE_SWITCH_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		WEATHER_copy_freeze_switch_settings_into_GuiVars();

		lcursor_to_select = 0;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrFreezeSwitch_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );

	FDTO_FREEZE_SWITCH_draw_freeze_switch_scrollbox( pcomplete_redraw );
}

/* ---------------------------------------------------------- */
/**
 * Processes the key event from the Freeze Switch Setup screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user presses a key on the Freeze Switch Setup screen.
 * 
 * @param pkey_event The key event to process.
 *
 * @author 6/14/2013 Adrianusv
 *
 * @revisions (none)
 */
extern void FREEZE_SWITCH_process_screen( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	UNS_32	lcontroller_index;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_cbxNoYes_0:
			if( g_FREEZE_SWITCH_combo_box_guivar == &GuiVar_FreezeSwitchInUse )
			{
				COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_FreezeSwitchInUse );
			}
			else
			{
				// Redraw the Freeze Switch scrollbox if the user closed the dropdown.
				if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
				{
					good_key_beep();

					lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
					lde._04_func_ptr = FDTO_FREEZE_SWITCH_close_freeze_switch_connected_dropdown;
					Display_Post_Command( &lde );

					Redraw_Screen( (false) );
				}
				else
				{
					COMBO_BOX_key_press( pkey_event.keycode, NULL );
				}
			}
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_FREEZE_SWITCH_IN_USE:
							good_key_beep();

							g_FREEZE_SWITCH_combo_box_guivar = &GuiVar_FreezeSwitchInUse;

							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_FREEZE_SWITCH_show_freeze_switch_in_use_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_FREEZE_SWITCH_SERIAL_NUMBER_LIST:
							good_key_beep();

							lcontroller_index = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 1, 0 );

							g_FREEZE_SWITCH_combo_box_guivar = &g_FREEZE_SWITCH[ lcontroller_index ].in_use;

							lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
							lde._04_func_ptr = (void*)&FDTO_FREEZE_SWITCH_show_switch_connected_dropdown;
							lde._06_u32_argument1 = 256;
							lde._07_u32_argument2 = (lcontroller_index == 0) ? 47 : 47 + (lcontroller_index * 14);
							Display_Post_Command( &lde );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch ( GuiLib_ActiveCursorFieldNo )
					{
						case CP_FREEZE_SWITCH_IN_USE:
							process_bool( &GuiVar_FreezeSwitchInUse );

							// Since we have to show or hide the various Freeze
							// Switch settings based upon this value, redraw the
							// entire screen.
							Redraw_Screen( (false) );
							break;

						case CP_FREEZE_SWITCH_SERIAL_NUMBER_LIST:
							good_key_beep();
							lcontroller_index = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 1, 0 );

							g_FREEZE_SWITCH[ lcontroller_index ].in_use = !(g_FREEZE_SWITCH[ lcontroller_index ].in_use);

							nm_WEATHER_set_freeze_switch_connected( g_FREEZE_SWITCH[ lcontroller_index ].in_use, 
																	g_FREEZE_SWITCH[ lcontroller_index ].chain_index,
																	CHANGE_generate_change_line, 
																	CHANGE_REASON_KEYPAD, 
																	FLOWSENSE_get_controller_index(), 
																	CHANGE_set_change_bits, 
																	WEATHER_get_change_bits_ptr( CHANGE_REASON_KEYPAD ) );
							SCROLL_BOX_redraw( 1 );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_PREV:
				case KEY_C_UP:
				case KEY_C_LEFT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_FREEZE_SWITCH_SERIAL_NUMBER_LIST:
							if( GuiLib_ScrollBox_GetActiveLine( 1, 0 ) == 0 )
							{
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = FDTO_FREEZE_SWITCH_leave_freeze_switch_scrollbox;
								Display_Post_Command( &lde );
							}
							else
							{
								SCROLL_BOX_up_or_down( 1, KEY_C_UP );
							}
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_NEXT:
				case KEY_C_DOWN:
				case KEY_C_RIGHT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_FREEZE_SWITCH_IN_USE:
							if( GuiVar_FreezeSwitchInUse == (true) )
							{
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = FDTO_FREEZE_SWITCH_enter_freeze_switch_scrollbox;
								Display_Post_Command( &lde );
							}
							else
							{
								bad_key_beep();
							}
							break;

						case CP_FREEZE_SWITCH_SERIAL_NUMBER_LIST:
							SCROLL_BOX_up_or_down( 1, KEY_C_DOWN );
							break;

						default:
							bad_key_beep ();
					}
					break;

				default:
					if( pkey_event.keycode == KEY_BACK )
					{
						WEATHER_extract_and_store_freeze_switch_changes_from_GuiVars();

						GuiVar_MenuScreenToShow = CP_MAIN_MENU_WEATHER;
					}

					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

