/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"d_device_exchange.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"GuiLib.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define DEVICE_EXCHANGE_PROGRESS_BAR_STEPS_TO_INC	(20)

// ---------------------

#define DEVICE_EXCHANGE_PROGRESS_BAR_MIN			(0)
#define DEVICE_EXCHANGE_PROGRESS_BAR_MAX			(200)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void DEVICE_EXCHANGE_draw_dialog( void )
{
	// 10/24/2013 ajv : Initialize the progress bar to start at position 0.
	GuiVar_DeviceExchangeProgressBar = DEVICE_EXCHANGE_PROGRESS_BAR_MIN;

	DIALOG_draw_ok_dialog( GuiStruct_dlgDeviceExchange_0 );
}

/* ---------------------------------------------------------- */
extern void FDTO_DEVICE_EXCHANGE_update_dialog( void )
{
	GuiVar_SpinnerPos = ((GuiVar_SpinnerPos + 1) % 4);

	// 5/18/2015 ajv : Update the progress bar every second.
	if( GuiVar_DeviceExchangeSyncingRadios )
	{
		if( GuiVar_DeviceExchangeProgressBar < DEVICE_EXCHANGE_PROGRESS_BAR_MAX )
		{
			GuiVar_DeviceExchangeProgressBar += DEVICE_EXCHANGE_PROGRESS_BAR_STEPS_TO_INC;
		}
		else
		{
			GuiVar_DeviceExchangeProgressBar = DEVICE_EXCHANGE_PROGRESS_BAR_MAX;
		}
	}
	else
	{
		GuiVar_DeviceExchangeProgressBar = DEVICE_EXCHANGE_PROGRESS_BAR_MAX;
	}

	FDTO_DIALOG_redraw_ok_dialog();
}

/* ---------------------------------------------------------- */
extern void FDTO_DEVICE_EXCHANGE_close_dialog( const UNS_32 pkeycode )
{
	KEY_TO_PROCESS_QUEUE_STRUCT		lktpqs;

	UNS_32	ldevice_exchange_result;

	ldevice_exchange_result = (GuiVar_CommOptionDeviceExchangeResult + DEVICE_EXCHANGE_KEY_MIN);

	switch( ldevice_exchange_result )
	{
		case DEVICE_EXCHANGE_KEY_busy_try_again_later:
		case DEVICE_EXCHANGE_KEY_read_settings_error:
			switch( pkeycode )
			{
				case KEY_SELECT:
				case KEY_BACK:
					// 5/18/2015 ajv : No need to play a good key beep here because processing the BACK key
					// below will handle that for us.

					g_DIALOG_modal_result = MODAL_RESULT_OK;

					// 5/18/2015 ajv : Ensure the flag that indicates a dialog box is open is set to false.
					DIALOG_close_ok_dialog();

					// 5/18/2015 ajv : Leave the screen since the read or write was unsuccessful.
					lktpqs.keycode = KEY_BACK;
					lktpqs.repeats = 0;
					(ScreenHistory[ screen_history_index ].key_process_func_ptr)( lktpqs );
					break;

				default:
					bad_key_beep();
			}
			break;

		case DEVICE_EXCHANGE_KEY_write_settings_error:
			switch( pkeycode )
			{
				case KEY_SELECT:
				case KEY_BACK:
					good_key_beep();

					g_DIALOG_modal_result = MODAL_RESULT_OK;

					// 5/18/2015 ajv : If there's an error writing the settings, we want to close the dialog
					// when they click OK, but we want to make sure they're still on the programming screen with
					// their settings displayed so they can try again.
					DIALOG_close_ok_dialog();
					break;

				default:
					bad_key_beep();
			}
			break;

		case DEVICE_EXCHANGE_KEY_read_settings_completed_ok:
		case DEVICE_EXCHANGE_KEY_write_settings_completed_ok:

			g_DIALOG_modal_result = MODAL_RESULT_OK;

			// 5/18/2015 ajv : This routine is called to close the dialog automatically when the read or
			// write is successful. Therefore, close the dialog but do not play a good or bad key beep.
			DIALOG_close_ok_dialog();
			break;

		case DEVICE_EXCHANGE_KEY_read_settings_in_progress:
		case DEVICE_EXCHANGE_KEY_write_settings_in_progress:
			// 10/18/2013 ajv : Since there's no OK button on the reading and writing progress screens,
			// prevent any user key presses from doing anything.
			
		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

