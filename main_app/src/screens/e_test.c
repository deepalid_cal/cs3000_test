/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for abs
#include	<stdlib.h>

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"e_test.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"configuration_network.h"

#include	"cs3000_comm_server_common.h"

#include	"cursor_utils.h"

#include	"dialog.h"

#include	"flowsense.h"

#include	"irri_comm.h"

#include	"irri_irri.h"

#include	"m_main.h"

#include	"r_irri_details.h"

#include	"screen_utils.h"

#include	"speaker.h"

#include	"stations.h"

#include	"flash_storage.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_TEST_STATION		(0)

// 10/30/2013 ajv : Suppressed the Controller cursor field for now until we
// figure out whether it's necessary or not. If re-enabling this, you'll need to
// re-enable the cursor field in the scrTest structure in easyGUI and change the
// font to ANSI 7 bold as well.
//#define CP_TEST_CONTROLLER	(1) 

#define CP_TEST_RUN_TIME	(2)
#define CP_TEST_START		(3)
#define CP_TEST_ACQUIRE		(4)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	TEST_MIN			(0.1F)
#define	TEST_MAX			(20.0F)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32 g_TEST_last_cursor_pos;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void TEST_store_time_if_changed( void )
{
	// 3/21/2013 rmd : If the test time has changed save it to the file system. So this becomes
	// this controllers new default test time. This could be controversial but I thought it the
	// right thing to do. Because of floating point math involved the comparisons may be
	// problematic hence our test for a noticeable change.
	INT_32	gui_test_time_seconds;  // keep as int math for the abs function
	
	gui_test_time_seconds = (INT_32)(GuiVar_TestRunTime * 60.0F);  // consider - the GuiVar is a float
	
	// 3/21/2013 rmd : If the difference is not at least half of the .1 minute increment value
	// ignore the so called change.
	if( abs( ( gui_test_time_seconds - ((INT_32)config_c.test_seconds) ) ) >= 3)
	{
		config_c.test_seconds = (UNS_32)gui_test_time_seconds;  // save to config struct memory
								
		// 3/21/2013 rmd : Save the file 10 seconds from now. Nothing special about 10 seconds.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_CONTROLLER_CONFIGURATION, 10 );
	}
}

/* ---------------------------------------------------------- */
static void TEST_copy_settings_into_guivars( const BOOL_32 pcomplete_redraw )
{
	STATION_STRUCT *lstation;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation = nm_STATION_get_pointer_to_station( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1) );

	if( lstation != NULL )
	{
		strlcpy( GuiVar_StationDescription, nm_GROUP_get_name( lstation ), NUMBER_OF_CHARS_IN_A_NAME );

		STATION_get_station_number_string( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1), (char*)&GuiVar_StationInfoNumber_str, sizeof(GuiVar_StationInfoNumber_str) );

		NETWORK_CONFIG_get_controller_name_str_for_ui( GuiVar_StationInfoBoxIndex, (char*)&GuiVar_StationInfoControllerName );
	}
	else
	{
		strlcpy( GuiVar_StationDescription, "", NUMBER_OF_CHARS_IN_A_NAME );

		strlcpy( GuiVar_StationInfoNumber_str, "", sizeof(GuiVar_StationInfoNumber_str) );

		strlcpy( GuiVar_StationInfoControllerName, "", sizeof(GuiVar_StationInfoControllerName) );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	if( pcomplete_redraw == (true) )
	{
		GuiVar_TestRunTime = (float)(config_c.test_seconds / 60.0);
	}
}

/* ---------------------------------------------------------- */
extern void FDTO_TEST_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		STATION_find_first_available_station_and_init_station_number_GuiVars();

		lcursor_to_select = CP_TEST_START;

		g_TEST_last_cursor_pos = CP_TEST_STATION;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	TEST_copy_settings_into_guivars( pcomplete_redraw );

	GuiLib_ShowScreen( GuiStruct_scrTest_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void TEST_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	BY_SYSTEM_RECORD	*lpreserve;

	STATION_STRUCT		*lstation;

	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TEST_START:
				case CP_TEST_ACQUIRE:
					if( !COMM_MNGR_network_is_available_for_normal_use() )
					{
						bad_key_beep();

						DIALOG_draw_ok_dialog( GuiStruct_dlgChainNotReady_0 );
					}
					else if( (GuiVar_StationInfoBoxIndex == FLOWSENSE_get_controller_index()) && (tpmicro_comm.fuse_blown == (true)) )
					{
						bad_key_beep();

						DIALOG_draw_ok_dialog( GuiStruct_dlgFuseBlown_0 );
					}
					else if( IRRI_COMM_if_any_2W_cable_is_over_heated() )
					{
						// 11/11/2014 rmd : Because you could be watering a prog have decided if any two_wire cable
						// is reporting problem don't allow any irrigation. Regardless of if it needs that cable or
						// not.
						// 
						// 11/11/2014 rmd : This really isn't as good as it could be. Could also check if the
						// station is decoder based.
						bad_key_beep();

						DIALOG_draw_ok_dialog( GuiStruct_dlgTwoWireCableOverheated_0 );
					}
					else
					{
						lpreserve = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( STATION_GROUP_get_GID_irrigation_system_for_this_station(nm_STATION_get_pointer_to_station(GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1))) );

						if( lpreserve == NULL )
						{
							bad_key_beep();

							ALERT_MESSAGE_WITH_FILE_NAME( "System not found" );
						}
						else
						{
							if( SYSTEM_PRESERVES_there_is_a_mlb( &(lpreserve->delivered_mlb_record) ) )
							{
								bad_key_beep();
		
								DIALOG_draw_ok_dialog( GuiStruct_dlgMainlineBreakInEffect_0 );
							}
							else if( lpreserve->sbf.delivered_MVOR_in_effect_closed  )
							{
								bad_key_beep();

								DIALOG_draw_ok_dialog( GuiStruct_dlgMVORClosed_0 );
							}
							else if( irri_comm.test_request.in_use == (false) )
							{
								good_key_beep();
		
								irri_comm.test_request.box_index_0 = GuiVar_StationInfoBoxIndex;
								irri_comm.test_request.station_number = (GuiVar_StationInfoNumber - 1);  // the gui is 1 based
								irri_comm.test_request.set_expected = (GuiLib_ActiveCursorFieldNo == CP_TEST_ACQUIRE);
		
								if( irri_comm.test_request.set_expected == (true) )
								{
									// Acquire Flow Rates should run for up to 6
									// minutes, so set it accordingly.
									irri_comm.test_request.time_seconds = (6 * 60);
								}
								else
								{
									irri_comm.test_request.time_seconds = (UNS_32)(GuiVar_TestRunTime * 60.0F);
								}
		
								// ----------
								
								// 3/21/2013 rmd : If the test time has changed save it to the file system. So this becomes
								// this controllers new default test time. This could be controversial but I thought it the
								// right thing to do.
								TEST_store_time_if_changed();
								
								// ----------
								
								irri_comm.test_request.in_use = (true);
		
								IRRI_DETAILS_jump_to_irrigation_details();
							}
							else
							{
								// 8/31/2012 rmd : We can show the user a pop-up to ask
								// him to try after the last station test request turns
								// ON.
								bad_key_beep();
							}
						}	
					}
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_PLUS:
		case KEY_MINUS:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TEST_STATION:
					good_key_beep();

					xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

					if( pkey_event.keycode == KEY_PLUS )
					{
						lstation = STATION_get_next_available_station( &GuiVar_StationInfoBoxIndex, &GuiVar_StationInfoNumber );
					}
					else
					{
						lstation = STATION_get_prev_available_station( &GuiVar_StationInfoBoxIndex, &GuiVar_StationInfoNumber );
					}

					TEST_copy_settings_into_guivars( (false) );

					xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

					Redraw_Screen( (false) );
					break;

				case CP_TEST_RUN_TIME:
					process_fl( pkey_event.keycode, &GuiVar_TestRunTime, TEST_MIN, TEST_MAX, 0.1F, (false) );
					Refresh_Screen();
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_NEXT:
		case KEY_PREV:
			good_key_beep();

			xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

			if( pkey_event.keycode == KEY_NEXT )
			{
				lstation = STATION_get_next_available_station( &GuiVar_StationInfoBoxIndex, &GuiVar_StationInfoNumber );
			}
			else
			{
				lstation = STATION_get_prev_available_station( &GuiVar_StationInfoBoxIndex, &GuiVar_StationInfoNumber );
			}

			TEST_copy_settings_into_guivars( (false) );

			xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

			Redraw_Screen( (false) );
			break;

		case KEY_C_UP:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TEST_STATION:
				//case CP_TEST_CONTROLLER:
					bad_key_beep();
					break;

				case CP_TEST_RUN_TIME:
					CURSOR_Select( g_TEST_last_cursor_pos, (true) );
					break;

				case CP_TEST_ACQUIRE:
					CURSOR_Select( CP_TEST_RUN_TIME, (true) );
					break;

				default:
					CURSOR_Up( (true) );
			}
			break;

		case KEY_C_DOWN:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TEST_STATION:
				//case CP_TEST_CONTROLLER:
					// 8/29/2013 ajv : Retain the last cursor position so, if
					// the user presses UP, they will be returned to the same
					// position they came from.
					g_TEST_last_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

					CURSOR_Select( CP_TEST_RUN_TIME, (true) );
					break;

				case CP_TEST_START:
					bad_key_beep();
					break;

				default:
					CURSOR_Down( (true) );
			}
			break;

		case KEY_C_LEFT:
			CURSOR_Up( (true) );
			break;

		case KEY_C_RIGHT:
			CURSOR_Down( (true) );
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = CP_MAIN_MENU_MANUAL;
			
			// 3/21/2013 rmd : If the test time has changed save it to the file system. So this becomes
			// this controllers new default test time. This could be controversial but I thought it the
			// right thing to do.
			TEST_store_time_if_changed();
			
			// No need to break here as this will allow it to fall into
			// the default case

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

