/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy and memset
#include	<string.h>

#include	"e_lights.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"cursor_utils.h"

#include	"e_keyboard.h"

#include	"epson_rx_8025sa.h"

#include	"screen_utils.h"

#include	"scrollbox.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_LIGHTS_NAME							(0)
#define	CP_LIGHTS_SCHEDULE_START_DATE			(1)
#define CP_LIGHTS_START_1						(2)
#define CP_LIGHTS_START_2						(3)
#define CP_LIGHTS_STOP_1						(4)
#define CP_LIGHTS_STOP_2						(5)

// ----------

#define	LIGHTS_SCHEDULE_PANEL_SCHEDULE_WILL_RUN				(0)
#define	LIGHTS_SCHEDULE_PANEL_SCHEDULE_WONT_RUN__NO_START	(1)
#define	LIGHTS_SCHEDULE_PANEL_SCHEDULE_WONT_RUN__NO_STOP	(2)
#define	LIGHTS_SCHEDULE_PANEL_SCHEDULE_WONT_RUN__START_AND_STOP_EQUAL	(3)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//#define	LIGHTS_INCLUDE_CALENDAR_UI_COMPONENTS

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32   g_LIGHTS_top_line;

static BOOL_32	g_LIGHTS_editing_group;

static UNS_32	g_LIGHTS_prev_cursor_pos;

// 9/29/2015 ajv : Since the currently selected item in the list does not necessarily
// correspond to g_GROUP_list_item_index any more (since the latter corresponds to the LIGHT
// list while the current item corresponds to the LIGHTS_MENU_items array), we need to keep
// track of which cursor item we're on as it relates to the LIGHTS_MENU_items array.
static UNS_32	g_LIGHTS_current_list_item_index;

// ----------

// 9/29/2015 ajv : This variable is used to determine which day we're on in our 14-day
// schedule.
static UNS_32	g_LIGHTS_first_sundays_date;

// 9/29/2015 ajv : This variable allows us to use the traditional process_uns32 function to
// change the date and then draw the associated date string to display on the screen
UNS_32	g_LIGHTS_numeric_date;

// 7/15/2015 mpd : The "g_LIGHTS_DailySchedule" array contains a complete copy of the 14 day
// daily schedule for the selected light. This allows the user to navigate through days
// without the GUI having to perform constant saves to the LIGHTS file. The day structure
// pointed to by "GuiVar_LightsDayIndex" is used to populate the 4 GuiVar times on the
// screen. These display times are in minutes.
LIGHTS_DAY_STRUCT g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ LIGHTS_SCHEDULE_DAYS_MAX ];

// ----------

#ifdef LIGHTS_INCLUDE_CALENDAR_UI_COMPONENTS

	// 8/26/2015 mpd : Light icons have been created in EasyGUI using the Font Editor. These 4 "Icon 16x16" characters are
	// placed on the Lights Schedule screen using the following defines. Each day on the 14 day calendar can contain zero, 
	// 1, or 2 icons indicating: no scheduled times, an ON time has been scheduled, an OFF time has been scheduled, or both.  
	#define LIGHTS_CALENDAR_ICON_THIS_DAY_ON	"W"		// White bulb icon with a black background.
	#define LIGHTS_CALENDAR_ICON_THIS_DAY_OFF	"Y" 	// White crossed-out bulb icon with a black background.
	#define LIGHTS_CALENDAR_ICON_OTHER_DAY_ON	"V"     // Black bulb icon with a white background.                 
	#define LIGHTS_CALENDAR_ICON_OTHER_DAY_OFF	"X"     // BLack crossed-out bulb icon with a white background.     

#endif

// ----------

typedef struct
{
	void        *light_ptr;

} LIGHTS_MENU_SCROLL_BOX_ITEM;

static LIGHTS_MENU_SCROLL_BOX_ITEM LIGHTS_MENU_items[ MAX_LIGHTS_IN_NETWORK ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void FDTO_LIGHTS_return_to_menu( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Gets the index into the "LIGHTS_MENU_items" array for the displayed light.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param void
 *
 * @return The array index ranging from 0 to 47.
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
static UNS_32 LIGHTS_get_menu_index_for_displayed_light( void )
{
	LIGHT_STRUCT	*llight;

	UNS_32  rv;

	UNS_32	i;

	// ----------

	rv = 0;

	// ----------

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	llight = nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 );

	for( i = 0; i < LIGHTS_get_list_count(); ++i )
	{
		if( LIGHTS_MENU_items[ i ].light_ptr == llight )
		{
			rv = i;

			break;
		}
	}

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Determines if the specified time exists anywhere in the 14 day schedule. Will return "false" if all 14 days have this
 * time set to "off". 
 * 
 * @param void
 *
 * @return BOOL_32 Indicates if a valid time exists in the 14 day schedule.
 *
 * @author 7/27/2015 mpd
 * 
 * @revisions
 *    7/27/2015 Initial release
 */
static BOOL_32 LIGHTS_start_time_exists_in_GUI_struct( void )
{
	BOOL_32 rv;

	UNS_32 day_index;

	// ----------

	rv = (false);

	// ----------

	for( day_index = 0; day_index < LIGHTS_SCHEDULE_DAYS_MAX; ++day_index )
	{
		if( (g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ day_index ].start_time_1_in_seconds != LIGHTS_OFF_TIME_IN_SECONDS) ||
			(g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ day_index ].start_time_2_in_seconds != LIGHTS_OFF_TIME_IN_SECONDS) )
		{
			rv = (true);

			break;
		}
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Determines if the specified time exists anywhere in the 14 day schedule. Will return "false" if all 14 days have this
 * time set to "off". 
 * 
 * @param void
 *
 * @return BOOL_32 Indicates if a valid time exists in the 14 day schedule.
 *
 * @author 7/27/2015 mpd
 * 
 * @revisions
 *    7/27/2015 Initial release
 */
static BOOL_32 LIGHTS_stop_time_exists_in_GUI_struct( void )
{
	BOOL_32 rv;

	UNS_32 day_index;

	// ----------

	rv = (false);

	// ----------

	for( day_index = 0; day_index < LIGHTS_SCHEDULE_DAYS_MAX; ++day_index )
	{
		if( (g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ day_index ].stop_time_1_in_seconds != LIGHTS_OFF_TIME_IN_SECONDS) ||
			(g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ day_index ].stop_time_2_in_seconds != LIGHTS_OFF_TIME_IN_SECONDS) )
		{
			rv = (true);

			break;
		}
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the next scheduled date and time values for a specific light from the 14 day schedule. A reference time is
 * passed in as the starting point. This generic function handles start and stop times based on the second parameter.
 * The "next" time is set using the reference pointers and a (true) is returned if a time is found in any time 1 or
 * time 2 schedule field. If no valid time is set in the 28 possible positions, or on the displayed screen,  this
 * function returns (false) and the destination values are set to the reference date and time.
 * 
 * @param dt_ptr The reference time.
 * 
 * @param look_for_start A Boolean indication if the search is for a start or stop time.
 * 
 * @param plight_index_0 The 0 - 47 light index.
 * 
 * @param next_date The destination for the "next" date.
 *
 * @param next_time The destination for the "next" time.
 *
 * @return BOOL_32 Indicates if a valid time exists in the 14 day schedule.
 *
 * @author 7/27/2015 mpd
 * 
 * @revisions
 *    7/27/2015 Initial release
 */
static BOOL_32 LIGHTS_get_next_scheduled_time( const DATE_TIME *const dt_ptr, const BOOL_32 look_for_start, const UNS_32 plight_index_0, DATE_TIME *next_dt )
{
	LIGHT_STRUCT *lls_ptr;

	DATE_TIME	temp_date;

	UNS_32 day_counter, todays_index;
	UNS_32 displayed_date;
	UNS_32 displayed_time_in_seconds;
	UNS_32 next_time_in_schedule_in_seconds;
	BOOL_32 found_a_usable_time;

	temp_date.D = UNS_16_MAX;
	temp_date.T = UNS_32_MAX;

	found_a_usable_time = (false);

	// This function returns the earliest start or stop time on the schedule (or on the displayed Lights Schedule 
	// screen) for this light. It may be later today, or up to 14 days from now. It can be in "xxxxx_time_1" or 
	// "xxxxx_time_2" fields. We have to look at both daily values since there is no restriction on the time order. 

	// This function is used on the "e_lights" screen. The "e_lights" screen uses the global
	// "g_LIGHTS_DailySchedule_in_seconds" array to save frequent reads and writes to the lights
	// file. Because of this, this function cannot use "LIGHTS_get_xxxxx_time_y()" calls. The
	// values returned by the file read may not match current user edited values.

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	lls_ptr = LIGHTS_get_light_at_this_index( plight_index_0 );

	// Determine what day this is in the 14 day lights schedule.
	todays_index = LIGHTS_get_day_index( dt_ptr->D );

	// If we are looking for a start time, be sure one exists. Same for stop time searches.
	if( ( look_for_start && LIGHTS_start_time_exists_in_GUI_struct() ) ||
		( !look_for_start && LIGHTS_stop_time_exists_in_GUI_struct() ) )
	{
		// Get the first time on this day. Use a local to save 3 more calls to the "get" function.
		if( look_for_start )
		{
			// 9/3/2015 mpd : This file read will return stale info. Use the global array to obtain all schedule data.
			//next_time_in_schedule_in_seconds = LIGHTS_get_start_time_1( lls_ptr, todays_index );
			next_time_in_schedule_in_seconds = g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ todays_index ].start_time_1_in_seconds;
		}
		else
		{
			next_time_in_schedule_in_seconds = g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ todays_index ].stop_time_1_in_seconds;
		}

		if( next_time_in_schedule_in_seconds < LIGHTS_24_HOURS_IN_SECONDS )
		{
			// This is a valid stop time. Is it later today, or did it already pass? "Now" counts as later today.
			if( next_time_in_schedule_in_seconds >= dt_ptr->T )
			{
				// This stop time is later today. Save it.
				found_a_usable_time = (true);
				temp_date.D = dt_ptr->D;
				temp_date.T = next_time_in_schedule_in_seconds;
			}
		}

		// Get the last time on this day. Use the same local.
		if( look_for_start )
		{
			next_time_in_schedule_in_seconds = g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ todays_index ].start_time_2_in_seconds;
		}
		else
		{
			next_time_in_schedule_in_seconds = g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ todays_index ].stop_time_2_in_seconds;
		}

		if( next_time_in_schedule_in_seconds < LIGHTS_24_HOURS_IN_SECONDS )
		{
			// This is a valid time. Is it later today, or did it already pass? "Now" counts as later today.
			if( next_time_in_schedule_in_seconds >= dt_ptr->T )
			{
				// This time is later today.  
				found_a_usable_time = (true);

				// It it earlier than what we already have saved? 
				if( temp_date.T > next_time_in_schedule_in_seconds )
				{
					temp_date.D = dt_ptr->D;
					temp_date.T = next_time_in_schedule_in_seconds;
				}
			}
		}

		if( !found_a_usable_time )
		{
			// There is not a valid stop time later today. Search the 14 day schedule starting with tomorrow. Note the 
			// "<=" for the loop end condition. If the two stop times on today's schedule are either invalid or already 
			// passed, we need to look at them again as day 14 times. The extra loop will do it.
			for( day_counter = 1; day_counter <= LIGHTS_SCHEDULE_DAYS_MAX; ++day_counter )
			{
				// The loop counter is cycling 1 - 13. "Tomorrow" is somewhere in the circular 0 - 13 daily schedule 
				// based on "todays_index". It needs to wrap as we pass 13. Use the modulus operator for the wrap. 
				if( look_for_start )
				{
					next_time_in_schedule_in_seconds = g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ ( ( todays_index + day_counter ) % LIGHTS_SCHEDULE_DAYS_MAX ) ].start_time_1_in_seconds;
				}
				else
				{
					next_time_in_schedule_in_seconds = g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ ( ( todays_index + day_counter ) % LIGHTS_SCHEDULE_DAYS_MAX ) ].stop_time_1_in_seconds;
				}

				if( next_time_in_schedule_in_seconds < LIGHTS_24_HOURS_IN_SECONDS )
				{
					found_a_usable_time = (true);

					// Save this valid time value. We will use it if nothing sooner is found.
					temp_date.D = ( dt_ptr->D + day_counter );
					temp_date.T = next_time_in_schedule_in_seconds;
				}

				if( look_for_start )
				{
					next_time_in_schedule_in_seconds = g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ ( ( todays_index + day_counter ) % LIGHTS_SCHEDULE_DAYS_MAX ) ].start_time_2_in_seconds;
				}
				else
				{
					next_time_in_schedule_in_seconds = g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ ( ( todays_index + day_counter ) % LIGHTS_SCHEDULE_DAYS_MAX ) ].stop_time_2_in_seconds;
				}

				if( next_time_in_schedule_in_seconds < LIGHTS_24_HOURS_IN_SECONDS )
				{
					// This is a valid time. It it earlier than what we already have saved? 
					if( temp_date.T > next_time_in_schedule_in_seconds )
					{
						found_a_usable_time = (true);

						temp_date.D = ( dt_ptr->D + day_counter );
						temp_date.T = next_time_in_schedule_in_seconds;
					}
				}

				if( found_a_usable_time )
				{
					// We found a date and time. Quit looking.
					break;
				}

			} // END for loop.

		} // END There is not a valid stop time later today.

		// Lastly, if the user is editing the schedule, the dynamic values displayed on the screen may be sooner than 
		// what has been saved in the schedule (the save does not occur until they exit that light).
		if( GuiLib_CurStructureNdx == GuiStruct_scrLights_0 )
		{
			// Calculate the date diaplayed on the screen.
			displayed_date = ( g_LIGHTS_first_sundays_date + LIGHTS_get_day_index( g_LIGHTS_numeric_date ) );

			// Get the first displayed time.
			if( look_for_start )
			{
				displayed_time_in_seconds = ( GuiVar_LightsStartTime1InMinutes * 60 );
			}
			else
			{
				displayed_time_in_seconds = ( GuiVar_LightsStopTime1InMinutes * 60 );
			}

			// Is this a valid time or "Off"?
			if( displayed_time_in_seconds < LIGHTS_24_HOURS_IN_SECONDS )
			{
				// Is it after the reference date and time? The date needs to be greater than or equal to the reference 
				// date. If it equals the reference date, verify the time before continuing. If the date is beyond, 
				// there is no need to check the time.
				if( ( ( displayed_date == dt_ptr->D ) && ( displayed_time_in_seconds > dt_ptr->T ) ) ||
					( displayed_date > dt_ptr->D ) )
				{
					// The time is usable. Note that this Boolean may have already been set. 
					found_a_usable_time = (true);

					// Now compare to what we have already found. Start by comparing the date to what we have saved.
					if( temp_date.D >= displayed_date )
					{
						// If the saved date is greater, or the time is greater on the same day, the displayed time is
						// sooner than what we have saved. 
						if( ( temp_date.D > displayed_date ) || ( temp_date.T > displayed_time_in_seconds ) )
						{
							// The displayed date and time are sooner than what we have saved. Use the displayed values.
							temp_date.D = displayed_date;
							temp_date.T = displayed_time_in_seconds;
						}
					}

				} // END the displayed date and time is after the reference date and time.

			} // END this is a valid time.

			// Get the second displayed time.
			if( look_for_start )
			{
				displayed_time_in_seconds = ( GuiVar_LightsStartTime2InMinutes * 60 );
			}
			else
			{
				displayed_time_in_seconds = ( GuiVar_LightsStopTime2InMinutes * 60 );
			}

			// Is this a valid time or "Off"?
			if( displayed_time_in_seconds < LIGHTS_24_HOURS_IN_SECONDS )
			{
				// Is it after the reference date and time? The date needs to be greater than or equal to the reference 
				// date. If it equals the reference date, verify the time before continuing. If the date is beyond, 
				// there is no need to check the time.
				if( ( ( displayed_date == dt_ptr->D ) && ( displayed_time_in_seconds > dt_ptr->T ) ) ||
					( displayed_date > dt_ptr->D ) )
				{
					// The time is usable. Note that this Boolean may have already been set. 
					found_a_usable_time = (true);

					// Now compare to what we have already found. Start by comparing the date to what we have saved.
					if( temp_date.D >= displayed_date )
					{
						// If the saved date is greater, or the time is greater on the same day, the displayed time is
						// sooner than what we have saved.
						if( ( temp_date.D > displayed_date ) || ( temp_date.T > displayed_time_in_seconds ) )
						{
							// The displayed date and time are sooner than what we have saved. Use the displayed values.
							temp_date.D = displayed_date;
							temp_date.T = displayed_time_in_seconds;
						}
					}

				} // END the displayed date and time is after the reference date and time.

			} // END this is a valid time.

		} // END if the user is on the Lights Schedule screen.

	} // END if start or stop exists.

	else
	{
		// A valid start or stop time does not exist in the schedule or on the editing screen.
		found_a_usable_time = (false);
	}

	if( found_a_usable_time )
	{
		// We found a date and time.
		next_dt->D = temp_date.D;
		next_dt->T = temp_date.T;
	}
	else
	{
		// This will happen for lights that have never been scheduled.
		next_dt->D = dt_ptr->D;
		next_dt->T = dt_ptr->T;
	}

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	return( found_a_usable_time );

}

/* ---------------------------------------------------------- */
/**
 * Obtains and displays the next start and stop times. A default message is displayed if there is not scheduled value in
 * the 14 day schedule.
 * 
 * @param void
 *
 * @return void
 *
 * @author 8/31/2015 mpd
 * 
 * @revisions
 *    8/31/2015 Initial release
 */
extern void LIGHTS_update_next_scheduled_date_and_time_panel( void )
{
	char date_str[ NUMBER_OF_CHARS_IN_A_NAME ];

	char time_str[ 8 ];

	DATE_TIME lnext_start_dt;
	DATE_TIME lnext_stop_dt;

	DATE_TIME current_dt;

	BOOL_32 light_has_start_time;
	BOOL_32 light_has_stop_time;

	// ----------

	light_has_start_time = LIGHTS_start_time_exists_in_GUI_struct();
	light_has_stop_time  = LIGHTS_stop_time_exists_in_GUI_struct();

	// ----------

	if( light_has_start_time && light_has_stop_time )
	{
		GuiVar_LightScheduleState = LIGHTS_SCHEDULE_PANEL_SCHEDULE_WILL_RUN;

		// The current schedule is valid. Get a reference time so we can display the next On and Off times. Without a
		// reference we don't know if the scheduled times for today have passed or are 14 days in the future.
		EPSON_obtain_latest_time_and_date( &current_dt );

		// Get the next scheduled start time for this light.
		LIGHTS_get_next_scheduled_time( &current_dt, (true), LIGHTS_get_lights_array_index( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 ), &lnext_start_dt );

		snprintf( GuiVar_LightScheduledStart, 
				  sizeof( GuiVar_LightScheduledStart ),
				  "%s, %s", 
				  TDUTILS_time_to_time_string_with_ampm( time_str, sizeof(time_str), lnext_start_dt.T, TDUTILS_DONT_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ),
				  GetDateStr( date_str, sizeof( date_str ), lnext_start_dt.D, DATESTR_show_long_year, DATESTR_show_long_dow )   );

		// 9/11/2015 mpd : FUTURE: This field will show the next SCHEDULED stop time. If the light is already on due to
		// a manual request, the next scheduled stop time is overridden by the manual value. The user may expect the 
		// light to go off at the scheduled time, but it won't. If the light is energized, we need to look at the stop
		// recorded in the On list and indicate an override somewhere on the screen. 

		// Get the next scheduled stop time for this light.
		LIGHTS_get_next_scheduled_time( &current_dt, (false), LIGHTS_get_lights_array_index( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 ), &lnext_stop_dt );

		snprintf( GuiVar_LightScheduledStop, 
				  sizeof( GuiVar_LightScheduledStop ),
				  "%s, %s", 
				  TDUTILS_time_to_time_string_with_ampm( time_str, sizeof(time_str), lnext_stop_dt.T, TDUTILS_DONT_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ),
				  GetDateStr( date_str, sizeof( date_str ), lnext_stop_dt.D, DATESTR_show_long_year, DATESTR_show_long_dow )    );
	}
	else if( light_has_start_time && !light_has_stop_time )
	{
		GuiVar_LightScheduleState = LIGHTS_SCHEDULE_PANEL_SCHEDULE_WONT_RUN__NO_STOP;
	}
	else
	{
		// 9/29/2015 ajv : If we fall into here, it means there's no start time
		GuiVar_LightScheduleState = LIGHTS_SCHEDULE_PANEL_SCHEDULE_WONT_RUN__NO_START;
	}
}

/* ---------------------------------------------------------- */
/**
 * Copies the contents of the passed in group into GuiVars which are used for
 * editing by the easyGUI library.
 * 
 * @mutex Calling function must have the list_lights_recursive_MUTEX to ensure the
 *        passed in group is not deleted during this process.
 *
 * @executed Executed within the context of the display processing task when a
 *           new screen is being drawn.
 * 
 * @param plight_index_0 The 0 - 47 index of the light.
 *
 * @author 9/14/2011 Adrianusv
 *
 * @revisions
 *    9/14/2011 Initial release
 */
static void LIGHTS_copy_group_into_guivars( const UNS_32 plight_index_0 )
{
	// 7/15/2015 mpd : This function needs to be called with "GuiVar_LightsDateNumeric", "GuiVar_LightsDate", and 
	// "GuiVar_LightsDayIndex" already set to the values correponding to today. 

	LIGHTS_copy_light_struct_into_guivars( plight_index_0 );

	// 9/3/2015 mpd : This copies the the saved schedule from the lights file to the global daily schedule. 
	LIGHTS_copy_file_schedule_into_global_daily_schedule( plight_index_0 );

	// 7/14/2015 mpd : Populate the GuiVars with the appropriate daily values from the schedule.
	LIGHTS_copy_daily_schedule_to_guivars( LIGHTS_get_day_index(g_LIGHTS_numeric_date) );

	LIGHTS_populate_group_name( LIGHTS_get_lights_array_index( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 ), (false) );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifdef LIGHTS_INCLUDE_CALENDAR_UI_COMPONENTS

/* ---------------------------------------------------------- */
/**
 * Displays icons in the 14 day lights schedule. Each day can contain a light bulb
 * icon indicating a start time is scheduled, a crossed-out light bulb icon
 * indicating a stop time is scheduled, both icons, or neither. The specific day being
 * displayed at the top of the screen determines which day on the calendar is
 * highlighted in black.
 * 
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param void
 *
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
static void LIGHTS_display_14_day_schedule( void )
{
	UNS_32 day_index;
	char *dest_ptr;

	// Loop through all 14 days. 
	for( day_index = 0; day_index < LIGHTS_SCHEDULE_DAYS_MAX; ++day_index )
	{
		// Move the black rectangle highlight to the appropriate position based on the day being displayed.
		if( LIGHTS_get_day_index(GuiVar_LightsDateNumeric) == day_index )
		{
			// The 32x42 pixel highlight needs to be positioned properly on the calendar based on the day currently 
			// being displayed at the top of the screen. Since this is the only place these values are used, don't 
			// bother with defines.
			GuiVar_LightsCalendarHighlightXOffset = 95 + ( ( day_index % 7 ) * 32 );
			GuiVar_LightsCalendarHighlightYOffset = 66 + ( ( day_index / 7 ) * 43 );
		}

		// Set the icon destination pointer to the appropriate position for the current day.
		switch( day_index )
		{
			case 0:
				dest_ptr = GuiVar_LightCalendarWeek1Sun;
				break;

			case 1:
				dest_ptr = GuiVar_LightCalendarWeek1Mon;
				break;

			case 2:
				dest_ptr = GuiVar_LightCalendarWeek1Tue;
				break;

			case 3:
				dest_ptr = GuiVar_LightCalendarWeek1Wed;
				break;

			case 4:
				dest_ptr = GuiVar_LightCalendarWeek1Thu;
				break;

			case 5:
				dest_ptr = GuiVar_LightCalendarWeek1Fri;
				break;

			case 6:
				dest_ptr = GuiVar_LightCalendarWeek1Sat;
				break;

			case 7:
				dest_ptr = GuiVar_LightCalendarWeek2Sun;
				break;

			case 8:
				dest_ptr = GuiVar_LightCalendarWeek2Mon;
				break;

			case 9:
				dest_ptr = GuiVar_LightCalendarWeek2Tue;
				break;

			case 10:
				dest_ptr = GuiVar_LightCalendarWeek2Wed;
				break;

			case 11:
				dest_ptr = GuiVar_LightCalendarWeek2Thu;
				break;

			case 12:
				dest_ptr = GuiVar_LightCalendarWeek2Fri;
				break;

			case 13:
				dest_ptr = GuiVar_LightCalendarWeek2Sat;
				break;

			default:
				break;
		} // END switch

		// Clear any current icons from the destination.
		strlcpy( dest_ptr, "", sizeof( dest_ptr ) );

		// The displayed day is highlighted in black. Determine if this is the highlighted day.
		if( LIGHTS_get_day_index(GuiVar_LightsDateNumeric) == day_index )
		{
			// This day has a black background. Display the white-on-black icons.

			// The "g_LIGHTS_DailySchedule" won't get updated until we leave this light or date. Use the displayed 
			// GuiVars to make the icon display decisions so icons immediately track editing changes. Note that 
			// displayed times are in minutes.
			if( ( GuiVar_LightsStartTime1InMinutes < LIGHTS_OFF_TIME_IN_MINUTES ) || ( GuiVar_LightsStartTime2InMinutes < LIGHTS_OFF_TIME_IN_MINUTES ) )
			{
				strlcat( dest_ptr, LIGHTS_CALENDAR_ICON_THIS_DAY_ON, sizeof( dest_ptr ) );
			}

			if( ( GuiVar_LightsStopTime1InMinutes < LIGHTS_OFF_TIME_IN_MINUTES ) || ( GuiVar_LightsStopTime2InMinutes < LIGHTS_OFF_TIME_IN_MINUTES ) )
			{
				strlcat( dest_ptr, LIGHTS_CALENDAR_ICON_THIS_DAY_OFF, sizeof( dest_ptr ) );
			}

		} // END displayed day

		else
		{
			// This day has a white background. Display the black-on-white icons.

			// The times for this day are not currently displayed on the screen. Look at the "g_LIGHTS_DailySchedule"
			// to make the icon display decisions. Note that saved times are in seconds.
			if( ( g_LIGHTS_DailySchedule_in_seconds[ day_index ].start_time_1_in_seconds < LIGHTS_OFF_TIME_IN_SECONDS ) || ( g_LIGHTS_DailySchedule_in_seconds[ day_index ].start_time_2_in_seconds < LIGHTS_OFF_TIME_IN_SECONDS ) )
			{
				strlcat( dest_ptr, LIGHTS_CALENDAR_ICON_OTHER_DAY_ON, sizeof( dest_ptr ) );
			}

			if( ( g_LIGHTS_DailySchedule_in_seconds[ day_index ].stop_time_1_in_seconds < LIGHTS_OFF_TIME_IN_SECONDS ) || ( g_LIGHTS_DailySchedule_in_seconds[ day_index ].stop_time_2_in_seconds < LIGHTS_OFF_TIME_IN_SECONDS ) )
			{
				strlcat( dest_ptr, LIGHTS_CALENDAR_ICON_OTHER_DAY_OFF, sizeof( dest_ptr ) );
			}

		} // END else not displayed day

	} // END for day_index loop

}

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Handles navigation from one light to the next (or previous). 
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
static void LIGHTS_process_NEXT_and_PREV( const UNS_32 pkeycode )
{
	UNS_32  lmenu_index_for_displayed_light;

	lmenu_index_for_displayed_light = LIGHTS_get_menu_index_for_displayed_light();

	if( (pkeycode == KEY_NEXT) || (pkeycode == KEY_PLUS) )
	{
		// Turn off the automatic redraw functions within the SCROLL_BOX_to_line and SCROLL_BOX_up_or_down routines to 
		// ensure the scroll bar does not appear.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_DISABLED );

		SCROLL_BOX_to_line( 0, (INT_16)lmenu_index_for_displayed_light );

		++lmenu_index_for_displayed_light;

		if( LIGHTS_MENU_items[ lmenu_index_for_displayed_light ].light_ptr != NULL )
		{
			SCROLL_BOX_up_or_down( 0, KEY_C_DOWN );
		}

		// Now that we've moved the scroll bar without displaying it, reenable the automatic redraw functions to ensure
		// other calls are not affected.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_ENABLED );
	}
	else
	{
		// Turn off the automatic redraw functions within the SCROLL_BOX_to_line and SCROLL_BOX_up_or_down routines to 
		// ensure the scroll bar does not appear.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_DISABLED );

		SCROLL_BOX_to_line( 0, (INT_16)lmenu_index_for_displayed_light );

		--lmenu_index_for_displayed_light;

		if( LIGHTS_MENU_items[ lmenu_index_for_displayed_light ].light_ptr != NULL )
		{
			SCROLL_BOX_up_or_down( 0, KEY_C_UP );
		}

		// Now that we've moved the scroll bar without displaying it, reenable the automatic redraw functions to ensure
		// other calls are not affected.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_ENABLED );
	}

	// If we've actually moved to another light (and not a controller heading), save the current lights's information 
	// and repopulate the GuiVars with the next light's.
	if( LIGHTS_MENU_items[ lmenu_index_for_displayed_light ].light_ptr != NULL )
	{
		LIGHTS_extract_and_store_changes_from_GuiVars();

		g_GROUP_list_item_index = LIGHTS_get_index_using_ptr_to_light_struct( LIGHTS_MENU_items[ lmenu_index_for_displayed_light ].light_ptr );

		// The group index has been updated for the next light. Leave the date alone so the user can step through 
		// multiple LIGHTS on the same date.
		LIGHTS_copy_group_into_guivars( g_GROUP_list_item_index );
	}
	else
	{
		bad_key_beep();
	}

	// 9/29/2015 ajv : Although nothing may have changed if we encountered the top or bottom of
	// the list, we need to force a redraw to ensure the scroll line of the currently selected
	// station is suppressed. Without this call, the SCROLL_BOX_to_line call drew the scroll
	// line and as soon as the next call to GuiLib_Refresh happens (such as once per second when
	// the date is updated), the scroll line will appear.
	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
/**
 * Obtains today's numerical date value. Sets the GuiVars containing today's date,
 * day index, and displayed date string. Initializes the GuiVar_LightsDateNumeric
 * with today's date.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param void
 *
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
static void LIGHTS_set_start_of_14_day_schedule( void )
{
	char date_str[ NUMBER_OF_CHARS_IN_A_NAME ];

	DATE_TIME ldt;

	// ----------

	// Get today's date.
	EPSON_obtain_latest_time_and_date( &ldt );

	// This value will change as the user navigates the 14 day schedule. 
	g_LIGHTS_numeric_date = ldt.D;

	strlcpy( GuiVar_LightsDate, GetDateStr( date_str, sizeof( date_str ), g_LIGHTS_numeric_date, DATESTR_show_long_year, DATESTR_show_long_dow ), sizeof( GuiVar_LightsDate ) );

	// This is the first Sunday of the current 14 day schedule. 
	g_LIGHTS_first_sundays_date = (ldt.D - LIGHTS_get_day_index(g_LIGHTS_numeric_date));
}

/* ---------------------------------------------------------- */
/**
 * Processes user daily schedule navigation through the 14 day schedule. This requires saving the 4 displayed times
 * in the daily schedule array, changing the displayed date, and displaying the times for the new date.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
static void LIGHTS_process_daily_schedule_change( const UNS_32 pkeycode )
{
	char date_str[ NUMBER_OF_CHARS_IN_A_NAME ];

	DATE_TIME ldt;

	// ----------

	EPSON_obtain_latest_time_and_date( &ldt );

	// ----------

	// Save any changes the user may have made to the displayed start and stop times. This is a local save; not a save 
	// to the LIGHTS file. That will happen if they change the displayed LIGHT or leave the screen.
	LIGHTS_copy_guivars_to_daily_schedule( LIGHTS_get_day_index( g_LIGHTS_numeric_date ) );

	// Allow the user to select dates in the current two-week schedule. Today is the first day the user can edit. It is 
	// probably not index 0 in the schedule. Index 0 is the first Sunday in the schedule (calculated from January 1, 
	// 2012. Today is an offset from the first Sunday.

	process_uns32( pkeycode, &g_LIGHTS_numeric_date, ldt.D, (ldt.D + LIGHTS_DAY_INDEX_MAX), 1, (false) );

	strlcpy( GuiVar_LightsDate, GetDateStr( date_str, sizeof( date_str ), g_LIGHTS_numeric_date, DATESTR_show_long_year, DATESTR_show_long_dow ), sizeof( GuiVar_LightsDate ) );

	// Display the local copy of the start and stop times for the new day. 
	LIGHTS_copy_daily_schedule_to_guivars( LIGHTS_get_day_index(g_LIGHTS_numeric_date) );

	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
/**
 * Processes the MINUS/PREV keys to change the start or stop time.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @param this_time_in_minutes Pointer to the time field being modofied. This can be a start or stop time.
 *
 * @param the_other_time_in_minutes The corresponding stop or start time.
 *
 * @param time_is_enabled Pointer to the enable flag associated with the time field being modified.
 *
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
static void LIGHTS_process_this_time( const UNS_32 pkeycode, UNS_32 *this_time_in_minutes, const UNS_32 the_other_time_in_minutes, BOOL_32 *time_is_enabled )
{
	// Times are saved in seconds; modified and displayed in minutes.

	// 7/16/2015 mpd : This function handles start times and stop times. To handle start times, call it with the start 
	// time pointer in "this_time" and the stop time pointer in "the_other_time". Swap them to handle stop times.

	if( (*this_time_in_minutes == LIGHTS_MIDNIGHT_TIME_IN_MINUTES) && (pkeycode == KEY_MINUS) )
	{
		good_key_beep();

		// 7/16/2015 mpd : Change the time from midnight (0 minutes) to the "off" value (1440 minutes).
		*this_time_in_minutes = LIGHTS_OFF_TIME_IN_MINUTES;
	}
	else if( (*this_time_in_minutes == LIGHTS_OFF_TIME_IN_MINUTES) && (pkeycode == KEY_PLUS) )
	{
		good_key_beep();

		// 7/16/2015 mpd : Change the time from the "off" value (1440 minutes) to midnight (0 minutes).
		*this_time_in_minutes = LIGHTS_MIDNIGHT_TIME_IN_MINUTES;
	}
	else
	{
		// 7/16/2015 mpd : Along with "normal" processing, this call will correctly handle incrementing from 11:50PM to 
		// "off", and decrementing from "off" to 11:50PM. It call does not deal with the midnight/off transitions.
		process_uns32( pkeycode, this_time_in_minutes, LIGHTS_MIDNIGHT_TIME_IN_MINUTES, LIGHTS_OFF_TIME_IN_MINUTES, 10, (false) );
	}

	// 7/16/2015 mpd : Correctly set the enabled flag to display a valid time or the "off" string.
	*time_is_enabled = (*this_time_in_minutes != LIGHTS_OFF_TIME_IN_MINUTES);

	// ----------

	// 9/29/2015 ajv : Populate the values that were just set into the display structure so the
	// subsequent check for valid start and stop times has the latest values.
	LIGHTS_copy_guivars_to_daily_schedule( LIGHTS_get_day_index(g_LIGHTS_numeric_date) );

	// ----------

	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
/**
 * Processes the key event from the LIGHTS screen for a particular LIGHT.
 * 
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
static void LIGHTS_process_group( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_dlgKeyboard_0:
			if( ( ( pkey_event.keycode == KEY_BACK ) || ( ( pkey_event.keycode == KEY_SELECT ) && ( GuiLib_ActiveCursorFieldNo == 49 /*CP_QWERTY_KEYBOARD_OK*/ ) ) ) )
			{
				LIGHTS_extract_and_store_group_name_from_GuiVars();
			}

			KEYBOARD_process_key( pkey_event, &FDTO_LIGHTS_draw_menu );
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_LIGHTS_NAME:
							GROUP_process_show_keyboard( 158, 27 );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_NEXT:
				case KEY_PREV: 
					LIGHTS_process_NEXT_and_PREV( pkey_event.keycode );
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_LIGHTS_SCHEDULE_START_DATE:
							LIGHTS_process_daily_schedule_change( pkey_event.keycode );
							break;

						case CP_LIGHTS_START_1:
							LIGHTS_process_this_time( pkey_event.keycode, &GuiVar_LightsStartTime1InMinutes, GuiVar_LightsStopTime1InMinutes, &GuiVar_LightsStartTimeEnabled1 );
							break;

						case CP_LIGHTS_START_2:
							LIGHTS_process_this_time( pkey_event.keycode, &GuiVar_LightsStartTime2InMinutes, GuiVar_LightsStopTime2InMinutes, &GuiVar_LightsStartTimeEnabled2 );
							break;

						case CP_LIGHTS_STOP_1:
							LIGHTS_process_this_time( pkey_event.keycode, &GuiVar_LightsStopTime1InMinutes, GuiVar_LightsStartTime1InMinutes, &GuiVar_LightsStopTimeEnabled1 );
							break;

						case CP_LIGHTS_STOP_2:
							LIGHTS_process_this_time( pkey_event.keycode, &GuiVar_LightsStopTime2InMinutes, GuiVar_LightsStartTime2InMinutes, &GuiVar_LightsStopTimeEnabled2 );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_C_UP:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_LIGHTS_START_2:
							CURSOR_Select( CP_LIGHTS_SCHEDULE_START_DATE, (true) );
							break;

						case CP_LIGHTS_STOP_1:
							CURSOR_Select( CP_LIGHTS_START_1, (true) );
							break;

						case CP_LIGHTS_STOP_2:
							CURSOR_Select( CP_LIGHTS_START_2, (true) );
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_DOWN:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_LIGHTS_START_1:
							CURSOR_Select( CP_LIGHTS_STOP_1, (true) );
							break;

						case CP_LIGHTS_START_2:
							CURSOR_Select( CP_LIGHTS_STOP_2, (true) );
							break;

						case CP_LIGHTS_STOP_1:
						case CP_LIGHTS_STOP_2:
							bad_key_beep();
							break;

						default:
							CURSOR_Down( (true) );
					}
					break;

				case KEY_C_LEFT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_LIGHTS_NAME:
							KEY_process_BACK_from_editing_screen( (void*)&FDTO_LIGHTS_return_to_menu );
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_BACK:
					KEY_process_BACK_from_editing_screen( (void*)&FDTO_LIGHTS_return_to_menu );
					break;

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Populates the "LIGHTS_MENU_items" array with pointers to all the availale lights.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param void
 *
 * @return UNS_32 The number of available lights.
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
static UNS_32 LIGHTS_populate_pointers_of_physically_available_lights_for_display( void )
{
	LIGHT_STRUCT *llight;

	UNS_32  lavailable_lights;

	UNS_32  i;

	// ----------

	lavailable_lights = 0;

	memset( LIGHTS_MENU_items, 0x00, sizeof( LIGHTS_MENU_items ) );

	// ----------

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	for( i = 0 ; i < LIGHTS_get_list_count(); i++ )
	{
		llight = LIGHTS_get_light_at_this_index( i );

		if( (llight != NULL) && (LIGHTS_get_physically_available(llight)) )
		{
			LIGHTS_MENU_items[ lavailable_lights ].light_ptr = llight;

			++lavailable_lights;
		}
	}

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	// ----------

	return( lavailable_lights );
}

/* ---------------------------------------------------------- */
/**
 * Sets the light name GuiVar.
 * 
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pindex_0_i16 The index of the light in the "LIGHTS_MENU_items" array.
 *
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
static void LIGHTS_load_light_name_into_guivar( const INT_16 pindex_0_i16 )
{
	char str_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	// ----------

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	if( (UNS_32)pindex_0_i16 < LIGHTS_get_list_count() )
	{
		strlcpy( str_48, nm_GROUP_get_name( LIGHTS_MENU_items[ pindex_0_i16 ].light_ptr ), NUMBER_OF_CHARS_IN_A_NAME );

		if( strncmp(str_48, LIGHTS_DEFAULT_NAME, strlen(LIGHTS_DEFAULT_NAME)) == 0 )
		{
			snprintf( GuiVar_itmGroupName, sizeof(GuiVar_itmGroupName), "Lights L%d", (LIGHTS_get_output_index(LIGHTS_MENU_items[ pindex_0_i16 ].light_ptr) + 1) );
		}
		else
		{
			strlcpy( GuiVar_itmGroupName, str_48, sizeof(GuiVar_itmGroupName) );
		}
	}
	else
	{
		Alert_group_not_found();
	}

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Rturns the "LIGHTS_GROUP_STRUCT" pointer for the specified light index.
 * 
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pindex_0 The index of the light in the "LIGHTS_MENU_items" array..
 *
 * @return LIGHTS_GROUP_STRUCT Pointer to the light structure for this light.
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
static LIGHT_STRUCT *LIGHTS_get_ptr_to_physically_available_light( const UNS_32 pindex_0 )
{
	// mpd : "pindex_0" is the index into the "LIGHTS_MENU_items" array. It is NOT the 0 - 47
	// index that uniquely identifies a light in the chain.
	return( LIGHTS_MENU_items[ pindex_0 ].light_ptr );
}

/* ---------------------------------------------------------- */
/**
 * Returns to the menu on the left from the editing screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user presses a key to move away from the editing screen and back
 * onto the menu.
 * 
 * @param void
 *
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
static void FDTO_LIGHTS_return_to_menu( void )
{
	FDTO_GROUP_return_to_menu( &g_LIGHTS_editing_group, (void*)&LIGHTS_extract_and_store_changes_from_GuiVars );
}

/* ---------------------------------------------------------- */
/**
 * Draws the Lights menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the screen is first navigated to.
 * 
 * @param pcomplete_redraw True if the screen should be complete redrawn;
 * otherwise, false. If false, only elements on the screen are updated, not the
 * entire structure, essentially refreshing the content.
 *
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern void FDTO_LIGHTS_draw_menu( const BOOL_32 pcomplete_redraw )
{
	UNS_32	llights_in_use;

	UNS_32	lactive_line;

	INT_32  lcursor_to_select;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	// Populate the list of available lights.
	llights_in_use = LIGHTS_populate_pointers_of_physically_available_lights_for_display();

	if( pcomplete_redraw == (true) )
	{
		lcursor_to_select = GuiLib_NO_CURSOR;

		g_LIGHTS_top_line = 0;

		g_LIGHTS_editing_group = (false);

		g_LIGHTS_current_list_item_index = 0;

		g_GROUP_list_item_index = LIGHTS_get_index_using_ptr_to_light_struct( LIGHTS_get_ptr_to_physically_available_light(g_LIGHTS_current_list_item_index) );

		// 7/14/2015 mpd : Set all the date related variables.
		LIGHTS_set_start_of_14_day_schedule();

		// 7/20/2015 mpd : Populate the GuiVar time variables from the 14 day schedule.
		LIGHTS_copy_group_into_guivars( g_GROUP_list_item_index );

		g_LIGHTS_prev_cursor_pos = CP_LIGHTS_NAME;
	}
	else
	{
		lcursor_to_select = (INT_32)GuiLib_ActiveCursorFieldNo;

		g_LIGHTS_top_line = (UNS_32)GuiLib_ScrollBox_GetTopLine( 0 );

		// Refresh the GuiVar strings.
		LIGHTS_copy_light_struct_into_guivars( LIGHTS_get_lights_array_index( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 ) );
	}

	LIGHTS_update_next_scheduled_date_and_time_panel();

	#ifdef LIGHTS_INCLUDE_CALENDAR_UI_COMPONENTS

		LIGHTS_display_14_day_schedule();

	#endif

	GuiLib_ShowScreen( GuiStruct_scrLights_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );

	// 10/24/2013 ajv : Close the scroll box to ensure the ScrollBox_Init function handles the redraw properly.
	GuiLib_ScrollBox_Close( 0 );

	lactive_line = LIGHTS_get_menu_index_for_displayed_light();

	if( g_LIGHTS_editing_group == (true) )
	{
		// 10/24/2013 ajv : Draw the scroll box without highlighting the active line and draw the indicator to show 
		// which light the user is looking at.
		GuiLib_ScrollBox_Init_Custom_SetTopLine( 0, &LIGHTS_load_light_name_into_guivar, (INT_16)llights_in_use, GuiLib_NO_CURSOR, (INT_16)g_LIGHTS_top_line );
		GuiLib_ScrollBox_SetIndicator( 0, lactive_line );
		GuiLib_ScrollBox_Redraw( 0 );
	}
	else
	{
		// If the last viewed station is beyond the initial 12 that's visible on
		// the screen, we need to move the top line down. For now, the selected
		// station will be at the very bottom of the list.
		if( (pcomplete_redraw == (true)) && (g_GROUP_list_item_index > 12) )
		{
			g_LIGHTS_top_line = (g_GROUP_list_item_index - 11);
		}

		// 10/24/2013 ajv : Draw the scroll box and highlight the active line.
		// Since we're redrawing the entire scroll box, there's no reason to
		// call SetIndicator with a -1 to hide it because the indicator simply
		// won't be drawn.
		GuiLib_ScrollBox_Init_Custom_SetTopLine( 0, &LIGHTS_load_light_name_into_guivar, (INT_16)llights_in_use, (INT_16)lactive_line, (INT_16)g_LIGHTS_top_line );
	}

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed on the LIGHT menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern void LIGHTS_process_menu( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	if( g_LIGHTS_editing_group == (true) )
	{
		LIGHTS_process_group( pkey_event );
	}
	else
	{
		switch( pkey_event.keycode )
		{
			case KEY_C_RIGHT:
			case KEY_SELECT:
				good_key_beep();

				g_LIGHTS_current_list_item_index = GuiLib_ScrollBox_GetActiveLine( 0, 0 );

				xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

				g_GROUP_list_item_index = LIGHTS_get_index_using_ptr_to_light_struct( LIGHTS_get_ptr_to_physically_available_light( g_LIGHTS_current_list_item_index ) );

				g_LIGHTS_editing_group = (true);

				GuiLib_ActiveCursorFieldNo = CP_LIGHTS_NAME;

				xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

				Redraw_Screen( (false) );
				break;

			case KEY_NEXT:
			case KEY_PREV:
				// Change the key to either UP or DOWN so it can be processed using the KEY_C_UP and
				// KEY_C_DOWN case statement.
				if( pkey_event.keycode == KEY_NEXT )
				{
					pkey_event.keycode = KEY_C_DOWN;
				}
				else
				{
					pkey_event.keycode = KEY_C_UP;
				}
				// Don't break here. This will allow the routine to fall into the next case statement which
				// will actually move the cursor up or down appropriately.

			case KEY_C_DOWN:
			case KEY_C_UP:
				SCROLL_BOX_up_or_down( 0, pkey_event.keycode );

				g_LIGHTS_current_list_item_index = GuiLib_ScrollBox_GetActiveLine( 0, 0 );

				xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

				g_GROUP_list_item_index = LIGHTS_get_index_using_ptr_to_light_struct( LIGHTS_get_ptr_to_physically_available_light( g_LIGHTS_current_list_item_index ) );

				LIGHTS_copy_group_into_guivars( g_GROUP_list_item_index );

				g_LIGHTS_prev_cursor_pos = CP_LIGHTS_NAME;

				xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

				Redraw_Screen( (false) );
				break;

			case KEY_BACK:
				GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;
				
				// 9/29/2015 ajv : No need to break here. This allows us to fall directly into the default
				// case.

			default:
				KEY_process_global_keys( pkey_event );
		}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

