/*  file = r_alerts.h    2009.05.27   rmd                     */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_R_ALERTS_H
#define _INC_R_ALERTS_H


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"battery_backed_vars.h"

#include	"alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/*
SPEEDING UP ALERTS: 

  1) CAN SCROLL WHAT'S BEING DISPLAY BY SHIFTING THE POINTER AND PARSING ONLY
     THE NEW ONE

  2) FOR STORAGE, COULD USE A SPECIAL FUNCTION TO DETERMINE LINE LENGTH INSTEAD
     OF THE FULL BLOWN PARSING FUNCTION
*/

/*
PREVENTING CORRUPTION:

  1) The biggest area of vulnerability is probably a power fail while adding an
     alert. This could leave "next" pointing not to the end of the alert we were
     writing in. A tactic to fight this would be not to set the "next" pointer
     until after the alert was actually on the pile. Maybe something could also
     be done about the "first" pointer as well.

  2) We have the test on the length byte in the function 
     "WithThisStartIndexReturnPreviousStartPtr" to control restarting after we
     detect a corrupt pile.
*/ 

// 10/31/2012 rmd : The minimum alert is the alert id as an UNS_16 + the date time + a
// length byte.
#define ALERTS_MIN_STORED_BYTE_COUNT ( sizeof(UNS_16) + sizeof(DATE_TIME) + 1 )

// Plus 1 for the string null plus another 1 for the alert length byte.
//
// 7/28/2016 rmd : This is BS. An alert can be constructed to store multiple strings. And we
// have some for example the MLB alert (alert #201). We could put multiple 128 byte strings
// onto the pile. Freom a practical point of view we don't . But could. So to say this is
// the maximum stored byte count is inaccurate!
#define ALERTS_MAX_STORED_BYTE_COUNT ( sizeof(UNS_16) + sizeof(DATE_TIME) + ALERTS_MAX_STORAGE_OF_TEXT_CHARS + 1 + 1 )

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	ALERTS_MAX_ALERTS_TO_SHOW			(15)

#define R_ALERTS_FILTER_SHOW_ALL			0x00
#define R_ALERTS_FILTER_SHOW_COMMUNICATION	0x01
#define R_ALERTS_FILTER_SHOW_IRRIGATION		0x02
#define R_ALERTS_FILTER_SHOW_WEATHER		0x03

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// The indicies into the alert pile of the start of each alert - starting
	// with the most recent and working towards the oldest.
	// 
	// 2012.06.04 ajv : Unfortunately, we now have differently sized alert
	// piles. Therefore, define this array of indicies using the largest pile.
	UNS_16 all_start_indicies[ (ENGINEERING_PILE_BYTES / ALERTS_MIN_STORED_BYTE_COUNT) ];

	// Indicies of those to display - starting with the most recent and working
	// towards the oldest (just the way we want to display them).
	// We can't just use the all_start_indicies array because of the alert
	// filters - this array keeps only the indicies that we are going to display
	// - controlled by the filters.
	// 
	// 2012.06.04 ajv : Unfortunately, we now have differently sized alert
	// piles. Therefore, define this array of indicies using the largest pile.
    UNS_16 display_start_indicies[ (ENGINEERING_PILE_BYTES / ALERTS_MIN_STORED_BYTE_COUNT) ];
	
	// Some display variables (indicies into the display_start_indicies array)
	UNS_32 display_index_of_first_line;

	// How many alerts the filter produces to display
	UNS_32 display_total;

	// Causes us to reparse the display before showing it
	BOOL_32 reparse;

	// There are events which cause us to have to do this for the display
	// such as when start times are hit or water days/start times are changed
	// (they could be set to OFF which makes them fall out of the hunt for the
	// oldest).
	BOOL_32 regenerate_displaystartindicies;

} ALERTS_DISPLAY_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern UNS_32   g_ALERTS_line_count;

extern UNS_32	g_ALERTS_pile_to_show;

extern UNS_32	g_ALERTS_filter_to_show;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern ALERTS_DISPLAY_STRUCT	alerts_display_struct_user;

extern ALERTS_DISPLAY_STRUCT	alerts_display_struct_changes;

extern ALERTS_DISPLAY_STRUCT	alerts_display_struct_tp_micro;

extern ALERTS_DISPLAY_STRUCT	alerts_display_struct_engineering;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void ALERTS_init_last_starts( void );

// ----------

extern UNS_32 nm_ALERTS_parse_alert_and_return_length( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, char *pdest_ptr, const UNS_32 pallowable_size, UNS_32 pindex );

// ----------

extern void nm_ALERTS_refresh_all_start_indicies( ALERTS_PILE_STRUCT *const ppile );

extern void nm_ALERTS_roll_and_add_new_display_start_index( ALERTS_PILE_STRUCT *const ppile, ALERTS_DISPLAY_STRUCT *const pdisplay_struct, const UNS_32 pstart_index );

extern void nm_ALERTS_change_filter( const UNS_32 pfilter, const BOOL_32 pgenerate_display_total_now );

extern ALERTS_DISPLAY_STRUCT *ALERTS_get_display_structure_for_pile( const ALERTS_PILE_STRUCT *const ppile );

extern BOOL_32 ALERT_this_alert_is_to_be_displayed( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pfilter, UNS_32 pindex );

// ----------

extern void FDTO_ALERTS_redraw_scrollbox( void );

extern void FDTO_ALERTS_draw_alerts( const BOOL_32 pcomplete_redraw );

extern void ALERTS_process_report( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

