/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef	_INC_M_MAIN_H
#define	_INC_M_MAIN_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_MAIN_MENU_NONE					(-1)
#define	CP_MAIN_MENU_STATUS					(0)
#define	CP_MAIN_MENU_SCHEDULED_IRRIGATION	(1)
#define	CP_MAIN_MENU_POCS 			  		(2)
#define	CP_MAIN_MENU_MAIN_LINES	  			(3)
#define	CP_MAIN_MENU_WEATHER				(4)
#define	CP_MAIN_MENU_BUDGETS				(5)
#define	CP_MAIN_MENU_LIGHTS					(6)
#define	CP_MAIN_MENU_MANUAL					(7)
#define	CP_MAIN_MENU_NO_WATER_DAYS			(8)
#define	CP_MAIN_MENU_REPORTS				(9)
#define	CP_MAIN_MENU_DIAGNOSTICS			(10)
#define	CP_MAIN_MENU_SETUP					(11)

#define CP_SCHEDULE_MENU_STATION_GROUPS		(20)
#define	CP_SCHEDULE_MENU_PROGRAMS			(21)
#define	CP_SCHEDULE_MENU_STATIONS			(22)
#define	CP_SCHEDULE_MENU_PRIORITIES			(23)
#define	CP_SCHEDULE_MENU_PERCENT_ADJUST		(24)
#define	CP_SCHEDULE_MENU_MANUAL_PROGRAMS	(25)
#define CP_SCHEDULE_MENU_FINISH_TIMES		(26)

#define	CP_POC_MENU_POC						(20)
#define	CP_POC_MENU_PUMP_USE				(21)
#define	CP_POC_MENU_LINE_FILL_TIME			(22)
#define	CP_POC_MENU_ACQUIRE_EXPECTEDS		(23)
#define	CP_POC_MENU_DELAY_BETWEEN_VALVES	(24)

#define	CP_MAINLINE_MENU_MAINLINE			(20)
#define	CP_MAINLINE_MENU_CAPACITY			(21)
#define	CP_MAINLINE_MENU_MAINLINE_BREAK		(22)
#define	CP_MAINLINE_MENU_ON_AT_A_TIME		(23)
#define	CP_MAINLINE_MENU_FLOW_CHECKING		(24)
#define	CP_MAINLINE_MENU_ALERT_ACTIONS		(25)
#define CP_MAINLINE_MENU_MVOR				(26)

#define	CP_WEATHER_MENU_HISTORICAL_ET		(20)
#define	CP_WEATHER_MENU_USE_OF_WEATHER		(21)
#define	CP_WEATHER_MENU_WEATHER_DEVICES		(22)
#define	CP_WEATHER_MENU_RAIN_SWITCH_SETUP	(23)
#define	CP_WEATHER_MENU_FREEZE_SWITCH_SETUP	(24)
#define	CP_WEATHER_MENU_MOISTURE_SENSORS	(25)

#define	CP_BUDGETS_MENU_SETUP				(20)
#define	CP_BUDGETS_MENU_AMOUNTS_BY_POC		(21)
#define CP_BUDGETS_MENU_REDUCTION_LIMITS	(22)
#define CP_BUDGETS_MENU_FLOW_TYPES			(23)
#define	CP_BUDGETS_MENU_USE_VS_BUDGET		(24)

#define	CP_LIGHTS_MENU_SCHEDULE				(20)
#define	CP_LIGHTS_MENU_TURN_ON_OR_OFF		(21)

#define	CP_MANUAL_MENU_TEST_STATIONS		(20)
#define	CP_MANUAL_MENU_MANUAL_WATERING		(21)
#define	CP_MANUAL_MENU_MANUAL_PROGRAMS		(22)
#define	CP_MANUAL_MENU_MVOR					(23)
#define	CP_MANUAL_MENU_LIGHTS_ON_OR_OFF		(24)
#define	CP_MANUAL_MENU_WALK_THRU			(25)
#define	CP_MANUAL_MENU_EXERCISE_MV			(26)

#define	CP_NOW_MENU_NO_WATER_DAYS			(20)
#define	CP_NOW_MENU_TURN_OFF				(21)

#define	CP_REPORTS_MENU_STATION_HISTORY		(20)
#define	CP_REPORTS_MENU_STATION				(21)
#define	CP_REPORTS_MENU_POC					(22)
#define	CP_REPORTS_MENU_MAINLINE			(23)
#define	CP_REPORTS_MENU_HOLDOVER			(24)
#define	CP_REPORTS_MENU_ET_AND_RAIN_TABLE	(25)
#define	CP_REPORTS_MENU_LIGHTS_USAGE		(26)

#define	CP_DIAGNOSTICS_MENU_ALERTS			(20)
#define	CP_DIAGNOSTICS_MENU_CHANGE_LINES	(21)
#define	CP_DIAGNOSTICS_MENU_IRRI_DETAILS	(22)
#define	CP_DIAGNOSTICS_MENU_FLOW_RECORDING	(23)
#define	CP_DIAGNOSTICS_MENU_LIVE_SCREENS	(24)

#define	CP_TECH_SUPPORT_MENU_ENG_ALERTS				(CP_DIAGNOSTICS_MENU_ALERTS)
#define	CP_TECH_SUPPORT_MENU_TP_MICRO_ALERTS		(CP_DIAGNOSTICS_MENU_CHANGE_LINES)
#define	CP_TECH_SUPPORT_MENU_FLOW_CHECKING			(CP_DIAGNOSTICS_MENU_IRRI_DETAILS)
#define	CP_TECH_SUPPORT_MENU_FLOW_TABLE				(CP_DIAGNOSTICS_MENU_FLOW_RECORDING)

#define	CP_SETUP_MENU_STATIONS_IN_USE		(20)
#define	CP_SETUP_MENU_TWO_WIRE				(21)
#define	CP_SETUP_MENU_FLOWSENSE				(22)
#define CP_SETUP_MENU_COMMUNICATION_OPTIONS	(23)
#define CP_SETUP_MENU_CONTRAST				(24)
#define	CP_SETUP_MENU_DATE_TIME				(25)
#define	CP_SETUP_MENU_ABOUT					(26)

// ----------

// 8/3/2015 ajv : When range checking the cursor position to see whether the cursor is on a
// menu item or not, these should be used. This ensures, for example, that the OK button on
// a dialog box (index 50) is not accidentally stored as a valid cursor position for a
// sub-menu.
#define CP_MIN_CURSOR_INDEX_ON_A_SUB_MENU	(20)
#define CP_MAX_CURSOR_INDEX_ON_A_SUB_MENU	(26)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern INT_32 g_MAIN_MENU_active_menu_item;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FDTO_MAIN_MENU_draw_menu( const BOOL_32 pcomplete_redraw );

extern void MAIN_MENU_process_menu( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

