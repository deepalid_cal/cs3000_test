/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"r_flowsense_stats.h"

#include	"app_startup.h"

#include	"comm_mngr.h"

#include	"flowsense.h"

#include	"irri_comm.h"

#include	"m_main.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * @task_execution Executed within the context of the DISPLAY PROCESSING task when the
 * screen is initially drawn; within the context of the TD check task as the screen is
 * updated 4 times per second.
 */
extern void FDTO_FLOWSENSE_STATS_draw_report( const BOOL_32 pcomplete_redraw )
{
	if( pcomplete_redraw == (true) )
	{
		GuiLib_ShowScreen( GuiStruct_rptFLOWSENSEStats_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );
	}

	// 10/1/2012 rmd : Yes we are taking this semaphore but the thing is we are not checking to
	// see if chain_members is even VALID! But that may be OK here. And we do perform a few
	// tests on the valid variable.
	xSemaphoreTakeRecursive( chain_members_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	GuiVar_ChainStatsFOALChainTGen = comm_stats.scan_msgs_generated;
	GuiVar_ChainStatsFOALChainRRcvd = comm_stats.scan_msg_responses_rcvd;
	GuiVar_ChainStatsIRRIChainTRcvd = comm_stats.scan_msgs_rcvd;
	GuiVar_ChainStatsIRRIChainRGen = comm_stats.scan_msg_responses_generated;

	GuiVar_ChainStatsFOALIrriTGen = comm_stats.tokens_generated;
	GuiVar_ChainStatsFOALIrriRRcvd = comm_stats.token_responses_rcvd;
	GuiVar_ChainStatsIRRIIrriTRcvd = comm_stats.tokens_rcvd;
	GuiVar_ChainStatsIRRIIrriRGen = comm_stats.token_responses_generated;

	GuiVar_ChainStatsInUseA = chain.members[  0 ].saw_during_the_scan;
	GuiVar_ChainStatsInUseB = chain.members[  1 ].saw_during_the_scan;
	GuiVar_ChainStatsInUseC = chain.members[  2 ].saw_during_the_scan;
	GuiVar_ChainStatsInUseD = chain.members[  3 ].saw_during_the_scan;
	GuiVar_ChainStatsInUseE = chain.members[  4 ].saw_during_the_scan;
	GuiVar_ChainStatsInUseF = chain.members[  5 ].saw_during_the_scan;
	GuiVar_ChainStatsInUseG = chain.members[  6 ].saw_during_the_scan;
	GuiVar_ChainStatsInUseH = chain.members[  7 ].saw_during_the_scan;
	GuiVar_ChainStatsInUseI = chain.members[  8 ].saw_during_the_scan;
	GuiVar_ChainStatsInUseJ = chain.members[  9 ].saw_during_the_scan;
	GuiVar_ChainStatsInUseK = chain.members[ 10 ].saw_during_the_scan;
	GuiVar_ChainStatsInUseL = chain.members[ 11 ].saw_during_the_scan;

	GuiVar_ChainStatsErrorsA = comm_mngr.failures_to_respond_to_the_token[  0 ];
	GuiVar_ChainStatsErrorsB = comm_mngr.failures_to_respond_to_the_token[  1 ];
	GuiVar_ChainStatsErrorsC = comm_mngr.failures_to_respond_to_the_token[  2 ];
	GuiVar_ChainStatsErrorsD = comm_mngr.failures_to_respond_to_the_token[  3 ];
	GuiVar_ChainStatsErrorsE = comm_mngr.failures_to_respond_to_the_token[  4 ];
	GuiVar_ChainStatsErrorsF = comm_mngr.failures_to_respond_to_the_token[  5 ];
	GuiVar_ChainStatsErrorsG = comm_mngr.failures_to_respond_to_the_token[  6 ];
	GuiVar_ChainStatsErrorsH = comm_mngr.failures_to_respond_to_the_token[  7 ];
	GuiVar_ChainStatsErrorsI = comm_mngr.failures_to_respond_to_the_token[  8 ];
	GuiVar_ChainStatsErrorsJ = comm_mngr.failures_to_respond_to_the_token[  9 ];
	GuiVar_ChainStatsErrorsK = comm_mngr.failures_to_respond_to_the_token[ 10 ];
	GuiVar_ChainStatsErrorsL = comm_mngr.failures_to_respond_to_the_token[ 11 ];

	GuiVar_ChainStatsCommMngrMode = comm_mngr.mode;

	GuiVar_ChainStatsCommMngrState = (comm_mngr.state / COMM_MNGR_STATE_cleaning_house);

	GuiVar_ChainStatsInForced = comm_mngr.chain_is_down;

	GuiVar_ChainStatsNextContact = next_contact.index;

	GuiVar_ChainStatsNextContactSerNum = chain.members[ next_contact.index ].box_configuration.serial_number;

	GuiVar_ChainStatsScans = comm_stats.rescans;

	GuiVar_ChainStatsPresentlyMakingTokens = ( (comm_mngr.mode == COMM_MNGR_MODE_operational) &&
											   (comm_mngr.state == COMM_MNGR_STATE_irrigation) &&
											   FLOWSENSE_we_are_a_master_one_way_or_another() &&
											   !comm_mngr.chain_is_down );
											   
	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );


	// 10/1/2012 rmd : Give back before the post to the high priority and long winded display
	// task.
	xSemaphoreGiveRecursive( chain_members_recursive_MUTEX );

	// ------------

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void FLOWSENSE_STATS_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		case KEY_BACK:
			GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;

			// No need to break here as this will allow it to fall into the default case

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

