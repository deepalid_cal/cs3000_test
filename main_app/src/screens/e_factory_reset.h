/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef	_INC_M_FACTORY_RESET_H
#define	_INC_M_FACTORY_RESET_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart( UNS_32 psystem_shutdown_event );


extern void FDTO_FACTORY_RESET_draw_screen( void );

extern void FACTORY_RESET_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

