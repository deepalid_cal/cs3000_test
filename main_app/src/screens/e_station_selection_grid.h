/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_STATION_SELECTION_GRID_H
#define _INC_STATION_SELECTION_GRID_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern BOOL_32 g_STATION_SELECTION_GRID_user_pressed_back;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern BOOL_32 STATION_SELECTION_GRID_station_is_selected( const UNS_32 pindex );

// ----------

extern UNS_32 STATION_SELECTION_GRID_get_count_of_selected_stations( void );

extern void STATION_SELECTION_GRID_populate_cursors( UNS_32 (*pget_group_ID_A_func_ptr)( void *const pstation ), UNS_32 (*pget_group_ID_B_func_ptr)( void *const pstation ), const UNS_32 pgroup_ID, const BOOL_32 pcomplete_redraw, const BOOL_32 phide_stations_not_in_use, const BOOL_32 ponly_show_stations_in_this_group );

// ----------

extern void FDTO_STATION_SELECTION_GRID_redraw_calling_screen( BOOL_32 *pediting_group, const UNS_32 pcursor_pos );

extern void FDTO_STATION_SELECTION_GRID_draw_screen( const BOOL_32 pcomplete_redraw, const BOOL_32 ponly_show_stations_in_this_group );

extern void STATION_SELECTION_GRID_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

