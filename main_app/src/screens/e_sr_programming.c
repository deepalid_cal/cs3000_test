/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/9/2015 mpd : Required for atoi
#include	<stdlib.h>

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"device_common.h"

#include	"e_sr_programming.h"

#include	"cursor_utils.h"

#include	"configuration_controller.h"

#include	"d_comm_options.h"

#include	"d_process.h"

#include	"d_device_exchange.h"

#include	"dialog.h"

#include	"speaker.h"

#include	"comm_mngr.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"device_SR_FREEWAVE.h"

#include	"combobox.h"

#include	"dialog.h"



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/12/2015 ajv : Moved cursor position defines to .C since they're only referenced in this
// file.

#define	CP_SR_PROGRAMMING_PORT					(0)
#define	CP_SR_PROGRAMMING_MODE					(1)
#define	CP_SR_PROGRAMMING_GROUP					(2)
#define	CP_SR_PROGRAMMING_REPEATER				(3)
#define	CP_SR_PROGRAMMING_SN_RCV				(4) 
#define	CP_SR_PROGRAMMING_SN_XMT				(5) 
#define	CP_SR_PROGRAMMING_XMIT_POWER			(6)
#define	CP_SR_PROGRAMMING_READ_DEVICE			(7)
#define	CP_SR_PROGRAMMING_PROGRAM_DEVICE		(8)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// 3/6/2015 mpd : Defines have been moved to the e_sr_programming.h file.

// 3/5/2015 mpd : These lists were created for a new UP/DOWN/LEFT/RIGHT navigation scheme that has not been implemented
// due to he complexity of cursor fields that are not always present. These are not needed for existing code.
/*
UNS_32 sr_cp_list[] = {
	SR_CP_PORT_PROGRAMMING,
	SR_CP_MODE_PROGRAMMING,
	SR_CP_GROUP_PROGRAMMING,
	SR_CP_REPEATER_PROGRAMMING,
	SR_CP_SN_RCV_PROGRAMMING,
	SR_CP_SN_XMT_PROGRAMMING,
	SR_CP_XMT_POWER_PROGRAMMING,
	SR_CP_SAVE_CHANGES,
	SR_CP_READ_RADIO
}; 
 
UNS_32 sr_port_list[] = {
	SR_PORT_A,
	SR_PORT_B
};

UNS_32 sr_mode_list[] = {
	SR_MODE_P_2_M_MASTER,
	SR_MODE_P_2_M_SLAVE,
	SR_MODE_P_2_M_REPEATER
};

UNS_32 sr_group_list[] = {
	SR_GROUP_0,
	SR_GROUP_1,
	SR_GROUP_2,
	SR_GROUP_3,
	SR_GROUP_4,
	SR_GROUP_5,
	SR_GROUP_6,
	SR_GROUP_7,
	SR_GROUP_8,
	SR_GROUP_9
};

char sr_repeater_list[] = {
	SR_REPEATER_A, 
	SR_REPEATER_B, 
	SR_REPEATER_C, 
	SR_REPEATER_D, 
	SR_REPEATER_E 
};

UNS_32 sr_sn_rcv_list[] = {
	SR_SN_RCV_0, 
	SR_SN_RCV_1, 
	SR_SN_RCV_2, 
	SR_SN_RCV_3, 
	SR_SN_RCV_4, 
	SR_SN_RCV_5, 
	SR_SN_RCV_6, 
	SR_SN_RCV_7, 
	SR_SN_RCV_8, 
	SR_SN_RCV_9, 
	SR_SN_RCV_F 
};

UNS_32 sr_sn_xmt_list[] = {
	SR_SN_XMT_0, 
	SR_SN_XMT_1, 
	SR_SN_XMT_2, 
	SR_SN_XMT_3, 
	SR_SN_XMT_4, 
	SR_SN_XMT_5, 
	SR_SN_XMT_6, 
	SR_SN_XMT_7, 
	SR_SN_XMT_8, 
	SR_SN_XMT_9, 
	SR_SN_XMT_F 
};

UNS_32 sr_xmt_power_list[] = {
	SR_XMT_POWER_0,
	SR_XMT_POWER_1,
	SR_XMT_POWER_2,
	SR_XMT_POWER_3,
	SR_XMT_POWER_4,
	SR_XMT_POWER_5,
	SR_XMT_POWER_6,
	SR_XMT_POWER_7,
	SR_XMT_POWER_8,
	SR_XMT_POWER_9,
	SR_XMT_POWER_10
};
*/
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static BOOL_32 SR_PROGRAMMING_querying_device;
UNS_32 *CurrentSubnetIDRcvPtr;
UNS_32 *CurrentSubnetIDXmtPtr;

static UNS_32	g_SR_PROGRAMMING_previous_cursor_pos;

// ----------

static const UNS_32 sr_mode_dropdown_lookup[ 3 ] =
{
	SR_MODE_P_2_M_MASTER,
	SR_MODE_P_2_M_SLAVE,
	SR_MODE_P_2_M_REPEATER
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void display_warning_if_repeater_links_to_itself( void )
{
	BOOL_32	lprev_value;

	lprev_value = GuiVar_SRRepeaterLinkedToItself;

	if( GuiVar_SRMode == SR_MODE_P_2_M_REPEATER )
	{
		// 6/19/2015 ajv : Notify the user if they've selected the same value for both subnet
		// transmit and receieve when programming a repeater.
		GuiVar_SRRepeaterLinkedToItself = (*CurrentSubnetIDRcvPtr == *CurrentSubnetIDXmtPtr);
	}
	else
	{
		GuiVar_SRRepeaterLinkedToItself = (false);
	}

	// 6/19/2015 ajv : If the value has changed, redraw the screen.
	if( lprev_value != GuiVar_SRRepeaterLinkedToItself )
	{
		Redraw_Screen( (false) );
	}
}


// 3/5/2015 mpd : The MODE enumeration list is full of holes due to the few SR radio options we support. Handle the
// navigateion details in this single location for consistency. Since this function is used during initialization,
// calling functions need to handle beeps and Redraw_Screen() as necessary. These steps cannot be handled here.
/* ---------------------------------------------------------- */
static void set_sr_mode( UNS_32 mode )
{
	switch( mode )
	{
		case SR_MODE_NEXT:
			switch( GuiVar_SRMode )
			{
				case SR_MODE_P_2_M_MASTER:
					GuiVar_SRMode = SR_MODE_P_2_M_SLAVE;
					break;

				case SR_MODE_P_2_M_SLAVE:
					GuiVar_SRMode = SR_MODE_P_2_M_REPEATER;
					break;

				// 3/5/2015 mpd : Allow values to wrap back to the first value.
				case SR_MODE_P_2_M_REPEATER:
				default:
					GuiVar_SRMode = SR_MODE_P_2_M_MASTER;
					break;
			}
			break;

		case SR_MODE_PREVIOUS:
			switch( GuiVar_SRMode )
			{
				// 3/5/2015 mpd : Allow values to wrap back to the last value.
				case SR_MODE_P_2_M_MASTER:
					GuiVar_SRMode = SR_MODE_P_2_M_REPEATER;
					break;

				case SR_MODE_P_2_M_SLAVE:
					GuiVar_SRMode = SR_MODE_P_2_M_MASTER;
					break;

				case SR_MODE_P_2_M_REPEATER:
				default:
					GuiVar_SRMode = SR_MODE_P_2_M_SLAVE;
					break;
			}
			break;

		// 3/5/2015 mpd : Allow direct manipulation of the mode. This may or may not be useful.
		case SR_MODE_P_2_M_MASTER: 
		case SR_MODE_P_2_M_SLAVE:
		case SR_MODE_P_2_M_REPEATER:
			GuiVar_SRMode = mode;
			break;

		// 3/5/2015 mpd : This is effectively a range check on the input parameter.
		default:
			GuiVar_SRMode = SR_MODE_INVALID;
			break;
	};

	// 3/5/2015 mpd : Each mode has it's own pair of subnet variables. This prevents the loss of current subnet values
	// when scrolling through modes. Use pointers so this is transparent to all downstream users including the SAVE CHANGES
	// key.
	switch( GuiVar_SRMode )
	{
		case SR_MODE_P_2_M_REPEATER:
			CurrentSubnetIDRcvPtr = &GuiVar_SRSubnetRcvRepeater;
			CurrentSubnetIDXmtPtr = &GuiVar_SRSubnetXmtRepeater;

			// 4/24/2015 mpd : Initialize the repeater value here if it is not valid. 
			if( ( GuiVar_SRRepeater < SR_REPEATER_MIN ) || ( GuiVar_SRRepeater > SR_REPEATER_MAX ) )
			{
				GuiVar_SRRepeater = SR_REPEATER_A;
			}

			// 6/19/2015 ajv : Repeaters are not allowed to have a transmit subnet of 0 - that's
			// reserved for the master. Therefore, if it's 0, automatically adjust it to 1.
			if( *CurrentSubnetIDXmtPtr == SR_SN_XMT_0 )
			{
				*CurrentSubnetIDXmtPtr = SR_SN_XMT_1;
			}
			break;

		case SR_MODE_P_2_M_SLAVE:
			CurrentSubnetIDRcvPtr = &GuiVar_SRSubnetRcvSlave;
			CurrentSubnetIDXmtPtr = &GuiVar_SRSubnetXmtSlave;
			break;

		case SR_MODE_P_2_M_MASTER:
		default:
			CurrentSubnetIDRcvPtr = &GuiVar_SRSubnetRcvMaster;
			CurrentSubnetIDXmtPtr = &GuiVar_SRSubnetXmtMaster;
			break;
	}

	display_warning_if_repeater_links_to_itself();
}
	
// 3/5/2015 mpd : The existance of the SubnetIDRcv field determines the result of UP and DOWN key operations. Determine
// the existance, then navigate to the appropriate cursor location.
/* ---------------------------------------------------------- */
static void navigate_around_subnet_rcv_field( const UNS_32 alternate_field )
{
	if( GuiVar_SRMode != SR_MODE_P_2_M_MASTER)
	{
		CURSOR_Select( CP_SR_PROGRAMMING_SN_RCV, (true) );
	}
	else 
	{
		CURSOR_Select( alternate_field, (true) );
	}
}

// 3/16/2015 mpd : Use unique identifiers "R" and "W" to define communication operations.
#define SR_READ		( 0x52 )
#define SR_WRITE	( 0x57 )

/* ---------------------------------------------------------- */
static void start_sr_device_communication( const UNS_32 port, UNS_32 operation )
{
	SR_PROGRAMMING_querying_device = (true);

	// 3/18/2014 rmd : Post the event to cause the comm_mngr to perform the device settings
	// readout and write the results to each of the guivars.

	COMM_MNGR_TASK_QUEUE_STRUCT	cmqs;
	
	if( operation == SR_WRITE )
	{
		cmqs.event = COMM_MNGR_EVENT_request_write_device_settings;
		//Alert_Message( "GUI: WRITE sent to COMM_MNGR" );
	}
	else
	{
		// 3/16/2015 mpd : Default to READ.
		cmqs.event = COMM_MNGR_EVENT_request_read_device_settings;
		//Alert_Message( "GUI: READ sent to COMM_MNGR" );
	}

	// 3/5/2015 mpd : Default to port A unless the calling function specifically requested port B. Note that defines for
	// SR_PORT_A != UPORT_A and SR_PORT_B != UPORT_B;
	cmqs.port = (port == SR_PORT_B ) ? UPORT_B : UPORT_A;

	COMM_MNGR_post_event_with_details( &cmqs );
}
	
/* ---------------------------------------------------------- */
static GuiConst_INT32U get_subnet_id_from_string( char *subnet_string )
{
	GuiConst_INT32U subnet_id;

	//Alert_Message( "get_subnet_id_from_string" );

	subnet_id = atoi( subnet_string );

	switch( subnet_id )
	{
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
			// 4/9/2015 mpd : Nothing more to do.
			break;

		case 15:
			subnet_id = SR_SN_RCV_MAX;
			// 4/9/2015 mpd : EasyGUI has assigned this value to "F". Note: SR_SN_XMT_MAX is equal to SR_SN_RCV_MAX. 
			// Use SR_SN_RCV_MAX for both cases.
			break;

		default:
			Alert_Message_va( "ERROR: Invalid SubnetID(1): %d", subnet_id );
			subnet_id = SR_SN_RCV_MAX;
			break;
	}

	return( subnet_id );
}

/* ---------------------------------------------------------- */
static void get_subnet_string_from_id( GuiConst_INT32U *subnet_id, char *subnet_string )
{
	// 4/9/2015 mpd : Make local space for the single char plus NULL so there is no question about the valid length. 
	char temp_string[2];

	//Alert_Message( "get_subnet_string_from_id" );

	// 4/9/2015 mpd : RCV and XMT define values are identical. Use RCV for both types.
	if( ( *subnet_id >= SR_SN_RCV_MIN ) && ( *subnet_id <= SR_SN_RCV_9 ) )
	{
		snprintf( temp_string, sizeof( temp_string ), "%d", *subnet_id );
	}
	else if( *subnet_id == SR_SN_RCV_F ) 
	{
		snprintf( temp_string, sizeof( temp_string ), "F" );
	}
	else  
	{
		snprintf( temp_string, sizeof( temp_string ), "-" );
		Alert_Message_va( "ERROR: Invalid SubnetID(2): %d", subnet_id );
	}

	// 4/9/2015 mpd : Copy the result to the destination pointer.
	memcpy( subnet_string, temp_string, sizeof( temp_string ) );

}

/* ---------------------------------------------------------- */
static void get_info_text_from_sr_programming_struct()
{
	//Alert_Message( "get_info_text_from_sr_programming_struct" );

	snprintf( GuiVar_CommOptionInfoText,     sizeof( GuiVar_CommOptionInfoText ),     sr_state->info_text );

}

/* ---------------------------------------------------------- */
static void clear_info_text()
{
	//Alert_Message( "get_info_text_from_sr_programming_struct" );

	strlcpy( GuiVar_CommOptionInfoText,     "", sizeof( GuiVar_CommOptionInfoText ) );

}

/* ---------------------------------------------------------- */
static void set_sr_programming_struct_from_guivars()
{
	//Alert_Message( "set_sr_programming_struct_from_guivars" );

	if( sr_state != NULL )
	{
		snprintf( sr_state->dynamic_pvs->modem_mode, sizeof(sr_state->dynamic_pvs->modem_mode), "%d", GuiVar_SRMode );

		// 4/9/2015 mpd : The port cannot be changed through programming. Don't change GuiVar_SRPort here!

		// 4/13/2015 mpd : NOTE: Group and Repeater should technically not appear on the screen at the same time. To 
		// match 2000e behavior, they do.  

		// 4/9/2015 mpd : Group is a simple "1" - "10" conversion. This is a 2 character field!
		snprintf( sr_state->network_group_id, sizeof(sr_state->network_group_id), "%2d", GuiVar_SRGroup );

		// 4/9/2015 mpd : The GUI repeater value ranges from 0 - 4, while the sr programming values are "A" - "E".
		snprintf( sr_state->repeater_group_id, sizeof(sr_state->repeater_group_id), "%c", (char)( GuiVar_SRRepeater + 'A' ) );

		// 4/9/2015 mpd : Power ranges from "0" - "10". This is a 2 character field!
		snprintf( sr_state->dynamic_pvs->rf_xmit_power, sizeof(sr_state->dynamic_pvs->rf_xmit_power), "%2d", GuiVar_SRTransmitPower );

		// 4/9/2015 mpd : The "current" pointers handle SubnetIDs based on the mode.
		// 4/9/2015 mpd : SubnetID strings range from "0" - "9" and "A" - "F", where "A" - "E" are repeaters. Deal with it. 
		switch( GuiVar_SRMode )
		{
			case SR_MODE_P_2_M_REPEATER:
				get_subnet_string_from_id( CurrentSubnetIDRcvPtr, sr_state->dynamic_pvs->subnet_rcv_id );
				get_subnet_string_from_id( CurrentSubnetIDXmtPtr, sr_state->dynamic_pvs->subnet_xmt_id );
				break;

			case SR_MODE_P_2_M_SLAVE:
				get_subnet_string_from_id( CurrentSubnetIDRcvPtr, sr_state->dynamic_pvs->subnet_rcv_id );
				snprintf( sr_state->dynamic_pvs->subnet_xmt_id, sizeof( sr_state->dynamic_pvs->subnet_xmt_id ), "F" );
				break;

			case SR_MODE_P_2_M_MASTER:
			default:
				snprintf( sr_state->dynamic_pvs->subnet_rcv_id, sizeof( sr_state->dynamic_pvs->subnet_rcv_id ), "0" );
				snprintf( sr_state->dynamic_pvs->subnet_xmt_id, sizeof( sr_state->dynamic_pvs->subnet_xmt_id ), "0" );
				break;
		}

		// 4/9/2015 mpd : This is a temporary field for debugging.
		//snprintf( GuiVar_CommOptionInfoText, sizeof(GuiVar_CommOptionInfoText), "Radio Name: %s", sr_state->dynamic_pvs->radio_name );
	}

}

/* ---------------------------------------------------------- */
static void get_guivars_from_sr_programming_struct()
{
	//Alert_Message( "get_guivars_from_sr_programming_struct" );

	if( sr_state != NULL )
	{
		// 4/9/2015 mpd : We can't get good values from the structure if no READ operation has ever been completed (reboot or new radio).  
		//if( sr_state->radio_settings_are_valid )
		// 5/12/2015 mpd : The valid test prevents any values from being displayed if a single value is invalid. This
		// is too strict. Show what we have.
		if( 1 )
		{
			strlcpy( GuiVar_SRSerialNumber, sr_state->serial_number, sizeof( GuiVar_SRSerialNumber ) );

			strlcpy( GuiVar_SRFirmwareVer, sr_state->sw_version, sizeof(GuiVar_SRFirmwareVer) );

			set_sr_mode( atoi( sr_state->dynamic_pvs->modem_mode ) );

			// 4/9/2015 mpd : The port cannot be changed through programming. Don't change GuiVar_SRPort here!

			// 4/9/2015 mpd : Group is a simple "1" - "10" conversion.
			GuiVar_SRGroup = atoi( sr_state->network_group_id );

			// 4/9/2015 mpd : The GUI repeater value ranges from 0 - 4, while the sr programming values are "A" - "E".
			GuiVar_SRRepeater = ( sr_state->repeater_group_id[0] - 'A' );

			// 4/9/2015 mpd : Power ranges from "0" - "10".
			GuiVar_SRTransmitPower = atoi( sr_state->dynamic_pvs->rf_xmit_power );

			// 4/9/2015 mpd : The "current" pointers handle SubnetIDs based on the mode.
			// 4/9/2015 mpd : SubnetID strings range from "0" - "9" and "A" - "F", where "A" - "E" are repeaters. Deal with it.
			memset(CurrentSubnetIDRcvPtr, get_subnet_id_from_string( sr_state->dynamic_pvs->subnet_rcv_id ), 1 ); 
			memset(CurrentSubnetIDXmtPtr, get_subnet_id_from_string( sr_state->dynamic_pvs->subnet_xmt_id ), 1 ); 
		}
	}

}

/* ---------------------------------------------------------- */
static BOOL_32 sr_values_in_range()
{
	BOOL_32 rv = true;

	// 5/12/2015 mpd : Verify that all GUI values are in range before performing a WRITE. Start at the top. Identify
	// the first problem and move the cursor. Any additional problem(s) will be caught on subsequent call(s) to this 
	// function.

	if( GuiVar_SRMode > SR_MODE_P_2_M_REPEATER )
	{
		//Alert_Message( "GuiVar_SRMode > SR_MODE_P_2_M_REPEATER" );
		CURSOR_Select( CP_SR_PROGRAMMING_MODE, (true) );
		rv = false;
	}
	else if( GuiVar_SRGroup > SR_GROUP_MAX )
	{
		//Alert_Message( "GuiVar_SRGroup > SR_GROUP_MAX" );
		CURSOR_Select( CP_SR_PROGRAMMING_GROUP, (true) );
		rv = false;
	}
	// 5/12/2015 mpd : This field is only valid for repeaters.
	else if( ( GuiVar_SRMode == SR_MODE_P_2_M_REPEATER ) && ( GuiVar_SRRepeater > SR_REPEATER_MAX ) )
	{
		//Alert_Message( "GuiVar_SRRepeater > SR_REPEATER_MAX" );
		CURSOR_Select( CP_SR_PROGRAMMING_REPEATER, (true) );
		rv = false;
	}
	// 5/12/2015 mpd : This field is only valid for slaves.
	else if( ( GuiVar_SRMode == SR_MODE_P_2_M_SLAVE ) && ( GuiVar_SRSubnetRcvSlave > SR_SN_RCV_9 ) )
	{
		//Alert_Message( "GuiVar_SRSubnetRcvSlave > SR_SN_RCV_9" );
		CURSOR_Select( CP_SR_PROGRAMMING_SN_RCV, (true) );
		rv = false;
	}
	// 5/12/2015 mpd : This field is only valid for repeaters.
	else if( ( GuiVar_SRMode == SR_MODE_P_2_M_REPEATER ) && ( GuiVar_SRSubnetRcvRepeater > SR_SN_RCV_9 ) )
	{
		//Alert_Message( "GuiVar_SRSubnetRcvRepeater > SR_SN_RCV_9" );
		CURSOR_Select( CP_SR_PROGRAMMING_SN_RCV, (true) );
		rv = false;
	}
	// 5/12/2015 mpd : This field is only valid for repeaters.
	else if( ( GuiVar_SRMode == SR_MODE_P_2_M_REPEATER ) && ( GuiVar_SRSubnetXmtRepeater > SR_SN_RCV_9 ) )
	{
		//Alert_Message( "GuiVar_SRSubnetXmtRepeater > SR_SN_RCV_9" );
		CURSOR_Select( CP_SR_PROGRAMMING_SN_XMT, (true) );
		rv = false;
	}
	else if( GuiVar_SRTransmitPower > SR_XMT_POWER_MAX )
	{
		//Alert_Message( "GuiVar_SRTransmitPower > SR_XMT_POWER_MAX" );
		CURSOR_Select( CP_SR_PROGRAMMING_XMIT_POWER, (true) );
		rv = false;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static void SR_PROGRAMMING_initialize_guivars( const UNS_32 port )
{
	// 4/20/2015 ajv : Set the device exchange result to 'read_ok' to display the initialized variables.
	GuiVar_CommOptionDeviceExchangeResult = DEVICE_EXCHANGE_KEY_read_settings_completed_ok;

	// 3/3/2015 mpd : Initialize all screen variables. These will be overridden once the radio values are read.
	strlcpy( GuiVar_SRSerialNumber, "--------", sizeof(GuiVar_SRSerialNumber) );

	set_sr_mode( SR_MODE_MIN );

	GuiVar_SRPort              = port;

	GuiVar_SRGroup             = SR_GROUP_MIN;

	// 4/24/2015 mpd : This GUIVar does not exist outside SR_MODE_P_2_M_REPEATER mode. It will have to be reinitialized
	// within "set_sr_mode()".
	GuiVar_SRRepeater          = SR_REPEATER_MIN;

	GuiVar_SRSubnetRcvMaster   = SR_SN_RCV_MIN; 
	GuiVar_SRSubnetRcvSlave    = SR_SN_RCV_MIN; 
	GuiVar_SRSubnetRcvRepeater = SR_SN_RCV_MIN; 

	GuiVar_SRSubnetXmtMaster   = SR_SN_XMT_MIN;
	GuiVar_SRSubnetXmtSlave    = SR_SN_XMT_MAX; // Note MAX value for slave Xmt.
	GuiVar_SRSubnetXmtRepeater = SR_SN_XMT_MIN;

	GuiVar_SRTransmitPower     = SR_XMT_POWER_MIN;

	clear_info_text();

}

/* ---------------------------------------------------------- */
static void FDTO_SR_PROGRAMMING_populate_mode_dropdown( const INT_16 pindex_0 )
{
	GuiVar_ComboBoxItemIndex = sr_mode_dropdown_lookup[ pindex_0 ];
}

/* ---------------------------------------------------------- */
static void FDTO_SR_PROGRAMMING_show_mode_dropdown( void )
{
	UNS_32	i;

	UNS_32	lactive_line;

	lactive_line = 0;

	// 4/21/2015 ajv : Find the index associated with the current SR Mode
	for( i = 0; i < 3; ++i )
	{
		if( sr_mode_dropdown_lookup[ i ] == GuiVar_SRMode )
		{
			lactive_line = i;
			
			break;
		}
	}

	FDTO_COMBOBOX_show( GuiStruct_cbxSRMode_0, &FDTO_SR_PROGRAMMING_populate_mode_dropdown, 3, lactive_line );
}

/* ---------------------------------------------------------- */
static void FDTO_SR_PROGRAMMING_show_receives_from_dropdown( void )
{
	UNS_32	subnet_rcv;

	// 4/20/2015 ajv : Grab the appropriate subnet receive value based upon the SR mode is
	// Point-to-MultiPoint Repeater or Point-to-MultiPoint Slave.
	if( GuiVar_SRMode == SR_MODE_P_2_M_REPEATER )
	{
		subnet_rcv = GuiVar_SRSubnetRcvRepeater;
	}
	else
	{
		subnet_rcv = GuiVar_SRSubnetRcvSlave;
	}

	FDTO_COMBOBOX_show( GuiStruct_cbxSRSubnetRcv_0, &FDTO_COMBOBOX_add_items, (SR_SN_RCV_9 + 1), subnet_rcv );
}

/* ---------------------------------------------------------- */
static void FDTO_SR_PROGRAMMING_process_device_exchange_key( const UNS_32 pkeycode, const UNS_32 pport )
{
	get_info_text_from_sr_programming_struct();

	switch( pkeycode )
	{
		case DEVICE_EXCHANGE_KEY_read_settings_completed_ok:
		case DEVICE_EXCHANGE_KEY_write_settings_completed_ok:
			// 4/14/2015 mpd : Get values from the programming struct, then redraw screen.
			get_guivars_from_sr_programming_struct();

			// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
			GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_completed_ok );

			// 5/18/2015 ajv : Ensure the flag that indicates a dialog box is open is set to false.
			DIALOG_close_ok_dialog();

			FDTO_SR_PROGRAMMING_draw_screen( (false), pport );
			break;

		case DEVICE_EXCHANGE_KEY_busy_try_again_later:
		case DEVICE_EXCHANGE_KEY_read_settings_error:
		case DEVICE_EXCHANGE_KEY_read_settings_in_progress:
		case DEVICE_EXCHANGE_KEY_write_settings_error:
		case DEVICE_EXCHANGE_KEY_write_settings_in_progress:
			// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
			GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( pkeycode );

			// 5/18/2015 ajv : Update the progress dialog.
			DEVICE_EXCHANGE_draw_dialog();
			break;

		default:
			// 4/14/2015 mpd : Nothing more to do.
			break;
	}
}

/* ---------------------------------------------------------- */
extern void FDTO_SR_PROGRAMMING_draw_screen( const BOOL_32 pcomplete_redraw, const UNS_32 pport )
{
	UNS_32	lcursor_to_select;
	UNS_32  lport;

	if( pcomplete_redraw == (true) )
	{
		// 3/5/2015 mpd : Default to port A unless the calling function specifically requested port B.
		lport = ( pport == SR_PORT_B ) ? SR_PORT_B : SR_PORT_A;

		SR_PROGRAMMING_initialize_guivars( lport );

		// 4/21/2015 ajv : Since we're drawing the screen from scratch, start the screen in the
		// "Reading settings from radio" device exchange mode until we're told otherwise.

		// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
		GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress );

		// 4/20/2015 ajv : Default to the MODE field since that's much more likely to be changed
		// than the port.
		lcursor_to_select = CP_SR_PROGRAMMING_MODE;
	}
	else
	{
		if( GuiLib_ActiveCursorFieldNo == GuiLib_NO_CURSOR )
		{
			lcursor_to_select = g_SR_PROGRAMMING_previous_cursor_pos;
		}
		else
		{
			lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
		}
	}

	GuiLib_ShowScreen( GuiStruct_scrSRProgramming_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();

	if( pcomplete_redraw == (true) )
	{
		// 5/18/2015 ajv : Immediately display the dialog to indicate to the user that something is
		// happening
		DEVICE_EXCHANGE_draw_dialog();

		// 3/21/2014 ajv : Start the data retrieval automatically for the user.
		// This isn't necessary, but it's a nicety, preventing the user from
		// having to press the Read Radio button.
		start_sr_device_communication( lport, SR_READ );
	}
}

/* ---------------------------------------------------------- */
extern void SR_PROGRAMMING_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	UNS_32	lactive_line;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_cbxSRMode_0:
			COMBO_BOX_key_press( pkey_event.keycode, &lactive_line );

			// 6/2/2015 ajv : Update the GuiVars associated with the mode at the selected line if the
			// combobox was closed
			if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
			{
				set_sr_mode( sr_mode_dropdown_lookup[ lactive_line ] );
				Redraw_Screen( (false) );
			}
			break;

		case GuiStruct_cbxSRSubnetRcv_0:
			COMBO_BOX_key_press( pkey_event.keycode, &lactive_line );

			// 6/2/2015 ajv : Update the GuiVars associated with the receive subnet ID at the selected
			// line if the combobox was closed
			if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
			{
				// 4/20/2015 ajv : Set the appropriate subnet receive value based upon the SR mode is
				// Point-to-MultiPoint Repeater or Point-to-MultiPoint Slave.
				if( GuiVar_SRMode == SR_MODE_P_2_M_REPEATER )
				{
					GuiVar_SRSubnetRcvRepeater = lactive_line;
				}
				else
				{
					GuiVar_SRSubnetRcvSlave = lactive_line;
				}

				display_warning_if_repeater_links_to_itself();

				Refresh_Screen();
			}
			break;

		default:
			switch( pkey_event.keycode )
			{
				case DEVICE_EXCHANGE_KEY_busy_try_again_later:

				case DEVICE_EXCHANGE_KEY_read_settings_completed_ok:
				case DEVICE_EXCHANGE_KEY_read_settings_error:
				case DEVICE_EXCHANGE_KEY_read_settings_in_progress:

				case DEVICE_EXCHANGE_KEY_write_settings_completed_ok:
				case DEVICE_EXCHANGE_KEY_write_settings_error:
				case DEVICE_EXCHANGE_KEY_write_settings_in_progress:
					SR_PROGRAMMING_querying_device = (false);

					// 3/18/2014 rmd : Redraw the screen displaying the results or
					// indicating the error condition.
					lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
					lde._04_func_ptr = (void*)&FDTO_SR_PROGRAMMING_process_device_exchange_key;
					lde._06_u32_argument1 = pkey_event.keycode;

					// 3/5/2015 mpd : In the case of SR radios on ports A and B, send the user to the correct port.
					if( GuiVar_SRPort == SR_PORT_B )
					{
						lde._07_u32_argument2 = SR_PORT_B;
					}
					else
					{
						lde._07_u32_argument2 = SR_PORT_A;
					}

					Display_Post_Command( &lde );
					break;

				case KEY_SELECT:
					// 4/20/2015 ajv : If we'r already in the middle of a device exchange or syncing radios,
					// ignore the key press.
					if( (GuiVar_CommOptionDeviceExchangeResult != ( e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress  ) ) ) && 
						(GuiVar_CommOptionDeviceExchangeResult != ( e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_write_settings_in_progress ) ) ) )
					{
						// 4/10/2015 mpd : Clear any existing info text for every user key event.  
						clear_info_text();

						switch( GuiLib_ActiveCursorFieldNo )
						{
							case CP_SR_PROGRAMMING_PROGRAM_DEVICE:
								if( sr_values_in_range() )
								{
									good_key_beep();

									// 4/21/2015 ajv : Store the button that was highlighted when the user pressed SELECT. Once
									// the write process is done, we'll revert back to this button.
									g_SR_PROGRAMMING_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

									// 4/9/2015 mpd : Send any user changes to the programming struct.
									set_sr_programming_struct_from_guivars();

									start_sr_device_communication( GuiVar_SRPort, SR_WRITE );
								}
								else
								{
									DIALOG_draw_ok_dialog( GuiStruct_dlgCantProgramRadio_0 );
									bad_key_beep();
								}
								break;

							case CP_SR_PROGRAMMING_READ_DEVICE:
								good_key_beep();

								// 4/21/2015 ajv : Store the button that was highlighted when the user pressed SELECT. Once
								// the read process is done, we'll revert back to this button.
								g_SR_PROGRAMMING_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

								start_sr_device_communication( GuiVar_SRPort, SR_READ );
								break;

							case CP_SR_PROGRAMMING_MODE:
								good_key_beep();
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = (void*)&FDTO_SR_PROGRAMMING_show_mode_dropdown;
								Display_Post_Command( &lde );
								break;

							case CP_SR_PROGRAMMING_SN_RCV:
								good_key_beep();
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = (void*)&FDTO_SR_PROGRAMMING_show_receives_from_dropdown;
								Display_Post_Command( &lde );
								break;

							case CP_SR_PROGRAMMING_PORT:
							case CP_SR_PROGRAMMING_GROUP: 
							case CP_SR_PROGRAMMING_REPEATER:
							case CP_SR_PROGRAMMING_SN_XMT:
							case CP_SR_PROGRAMMING_XMIT_POWER:
							default:
								bad_key_beep();
								break;
						}
					}
					else
					{
						bad_key_beep();
					}
					break;

				case KEY_C_UP:
					// 4/10/2015 mpd : Clear any existing info text for every user key event.  
					clear_info_text();

					// 3/5/2015 mpd : I just became aware of Calsense expected UP/DOWN navigation following the trail left by the user
					// instead of a predetermined path. This implementation requires bread crums on any line involving 2 or more cursor
					// fields. This is a TODO item not included in the current code.
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_SR_PROGRAMMING_REPEATER:
							CURSOR_Select( CP_SR_PROGRAMMING_MODE, (true) );
							break;

						case CP_SR_PROGRAMMING_PROGRAM_DEVICE:
							CURSOR_Select( CP_SR_PROGRAMMING_XMIT_POWER, (true) );
							break;

						case CP_SR_PROGRAMMING_SN_XMT:
							CURSOR_Select( CP_SR_PROGRAMMING_REPEATER, (true) );
							break;

						case CP_SR_PROGRAMMING_XMIT_POWER:
							navigate_around_subnet_rcv_field( CP_SR_PROGRAMMING_GROUP );
							break;

						case CP_SR_PROGRAMMING_SN_RCV:
							CURSOR_Select( CP_SR_PROGRAMMING_GROUP, (true) );
							break;

						case CP_SR_PROGRAMMING_PORT:
						case CP_SR_PROGRAMMING_MODE:
						case CP_SR_PROGRAMMING_GROUP: 
						case CP_SR_PROGRAMMING_READ_DEVICE:
						default:
							CURSOR_Up( (true) );
							break;
					}
					break;

				case KEY_C_LEFT:
					// 4/10/2015 mpd : Clear any existing info text for every user key event.  
					clear_info_text();

					CURSOR_Up( (true) );
					break;

				case KEY_C_DOWN:
					// 4/10/2015 mpd : Clear any existing info text for every user key event.  
					clear_info_text();

					// 3/5/2015 mpd : I just became aware of Calsense expected UP/DOWN navigation following the trail left by the user
					// instead of a predetermined path. This implementation requires bread crums on any line involving 2 or more cursor
					// fields. This is a TODO item not included in the current code.
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_SR_PROGRAMMING_GROUP: 
							navigate_around_subnet_rcv_field( CP_SR_PROGRAMMING_XMIT_POWER );
							break;

						case CP_SR_PROGRAMMING_SN_RCV:
							CURSOR_Select( CP_SR_PROGRAMMING_XMIT_POWER, (true) );
							break;
							
						case CP_SR_PROGRAMMING_REPEATER:
							CURSOR_Select( CP_SR_PROGRAMMING_SN_XMT, (true) );
							break;

						case CP_SR_PROGRAMMING_READ_DEVICE:
							bad_key_beep();
							break;

						case CP_SR_PROGRAMMING_PORT:
						case CP_SR_PROGRAMMING_MODE:
						case CP_SR_PROGRAMMING_SN_XMT:
						case CP_SR_PROGRAMMING_XMIT_POWER:
						case CP_SR_PROGRAMMING_PROGRAM_DEVICE:
						default:
							CURSOR_Down( (true) );
							break;
					}
					break;

				case KEY_C_RIGHT:
					// 4/10/2015 mpd : Clear any existing info text for every user key event.  
					clear_info_text();

					CURSOR_Down( (true) );
					break;

				case KEY_PREV:
				case KEY_NEXT:
					// 4/10/2015 mpd : Clear any existing info text for every user key event.  
					clear_info_text();

					bad_key_beep();
					break;

				case KEY_PLUS:
				case KEY_MINUS:
					// 4/10/2015 mpd : Clear any existing info text for every user key event.  
					clear_info_text();

					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_SR_PROGRAMMING_PORT:
							// 4/21/2015 ajv : The port field is only editable if there are FreeWave SR radios attached
							// both ports.
							if( (GuiVar_CommOptionPortAIndex == COMM_DEVICE_SR_FREEWAVE) && (GuiVar_CommOptionPortBIndex == COMM_DEVICE_SR_FREEWAVE) )
							{
								good_key_beep();

								GuiVar_SRPort = ( GuiVar_SRPort == SR_PORT_A ) ? SR_PORT_B : SR_PORT_A;
								//snprintf( GuiVar_CommOptionInfoText, sizeof(GuiVar_CommOptionInfoText), "GuiVar_SRPort: %d", GuiVar_SRPort );

								// 4/21/2015 ajv : Read the radio on the new port.
								start_sr_device_communication( GuiVar_SRPort, SR_READ );
							}
							else
							{
								bad_key_beep();
							}
							break;

						case CP_SR_PROGRAMMING_MODE:
							good_key_beep();

							if( pkey_event.keycode == KEY_PLUS )
							{
								set_sr_mode( SR_MODE_NEXT );
							}
							else // if( pkey_event.keycode == KEY_MINUS )
							{
								set_sr_mode( SR_MODE_PREVIOUS );
							}
							//snprintf( GuiVar_CommOptionInfoText, sizeof(GuiVar_CommOptionInfoText), "GuiVar_SRMode: %d", GuiVar_SRMode );
							Redraw_Screen( (false) );
							break;

						case CP_SR_PROGRAMMING_GROUP: 
							process_uns32( pkey_event.keycode, &GuiVar_SRGroup, SR_GROUP_MIN, SR_GROUP_MAX, 1, (false) );

							// 3/4/2015 mpd : DEBUGGING CODE. REMOVE FOR PRODUCTION.
							//snprintf( GuiVar_CommOptionInfoText, sizeof(GuiVar_CommOptionInfoText), "GuiVar_SRGroup: %d", GuiVar_SRGroup );
							break;

						case CP_SR_PROGRAMMING_REPEATER:
							process_uns32( pkey_event.keycode, &GuiVar_SRRepeater, SR_REPEATER_MIN, SR_REPEATER_MAX, 1, (false) );

							// 3/4/2015 mpd : DEBUGGING CODE. REMOVE FOR PRODUCTION.
							//snprintf( GuiVar_CommOptionInfoText, sizeof(GuiVar_CommOptionInfoText), "GuiVar_SRRepeater: %d", GuiVar_SRRepeater );
							break;

						case CP_SR_PROGRAMMING_SN_RCV:
							process_uns32( pkey_event.keycode, CurrentSubnetIDRcvPtr, SR_SN_RCV_MIN, (SR_SN_RCV_MAX-1), 1, (false) );

							display_warning_if_repeater_links_to_itself();

							//snprintf( GuiVar_CommOptionInfoText, sizeof(GuiVar_CommOptionInfoText), "Master Rcv: %d, Slave Rcv: %d, Repeater Rcv: %d", 
							//		  GuiVar_SRSubnetRcvMaster, GuiVar_SRSubnetRcvSlave, GuiVar_SRSubnetRcvRepeater );
							break;

						case CP_SR_PROGRAMMING_SN_XMT:
							process_uns32( pkey_event.keycode, CurrentSubnetIDXmtPtr, (SR_SN_XMT_MIN + 1), SR_SN_XMT_MAX, 1, (false) );

							display_warning_if_repeater_links_to_itself();

							//snprintf( GuiVar_CommOptionInfoText, sizeof(GuiVar_CommOptionInfoText), "Master Xmt: %d, Slave Xmt: %d, Repeater Xmt: %d", 
							//		  GuiVar_SRSubnetXmtMaster, GuiVar_SRSubnetXmtSlave, GuiVar_SRSubnetXmtRepeater );
							break;

						case CP_SR_PROGRAMMING_XMIT_POWER:
							process_uns32( pkey_event.keycode, &GuiVar_SRTransmitPower, SR_XMT_POWER_MIN, SR_XMT_POWER_MAX, 1, (false) );
							//snprintf( GuiVar_CommOptionInfoText, sizeof(GuiVar_CommOptionInfoText), "GuiVar_SRTransmitPower: %d", GuiVar_SRTransmitPower );
							break;

						case CP_SR_PROGRAMMING_READ_DEVICE:
						case CP_SR_PROGRAMMING_PROGRAM_DEVICE:
						default:
							bad_key_beep();
							//snprintf( GuiVar_CommOptionInfoText, sizeof(GuiVar_CommOptionInfoText), "PLUS key is invalid for this field" );
							break;
					}
					
					// 3/4/2015 mpd : Skip refresh if a redraw has been done.
					if( (GuiLib_ActiveCursorFieldNo != CP_SR_PROGRAMMING_PORT) && (GuiLib_ActiveCursorFieldNo != CP_SR_PROGRAMMING_MODE) )
					{
						Refresh_Screen();
					}
					break;

				case KEY_BACK:
					GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;

					KEY_process_global_keys( pkey_event );

					// 6/5/2015 ajv : Since the user got here from the COMM OPTIONS dialog box, redraw that
					// dialog box.
					COMM_OPTIONS_draw_dialog( (true) );
					break;

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

