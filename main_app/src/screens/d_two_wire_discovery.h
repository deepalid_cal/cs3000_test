/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_D_TWO_WIRE_DISCOVERY_H
#define _INC_D_TWO_WIRE_DISCOVERY_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"k_process.h"

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	TWO_WIRE_DISCOVERY_DIALOG_STATE_NONE_FOUND		(0)
#define	TWO_WIRE_DISCOVERY_DIALOG_STATE_DISCOVERING		(1)
#define	TWO_WIRE_DISCOVERY_DIALOG_STATE_COMPLETE		(2)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void TWO_WIRE_init_discovery_dialog( const BOOL_32 pshow_progress_dialog );

extern void FDTO_TWO_WIRE_update_discovery_dialog( const BOOL_32 pprocess_complete );

extern void FDTO_TWO_WIRE_close_discovery_dialog( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event, BOOL_32 *const pdialog_visible_ptr );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

