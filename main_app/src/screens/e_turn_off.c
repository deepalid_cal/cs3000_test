/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_turn_off.h"

#include	"background_calculations.h"

#include	"change.h"

#include	"configuration_controller.h"

#include	"configuration_network.h"

#include	"d_process.h"

#include	"flowsense.h"

#include	"m_main.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_TURN_OFF			(0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void FDTO_TURN_OFF_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		GuiVar_TurnOffControllerIsOff = NETWORK_CONFIG_get_controller_off();

		lcursor_to_select = CP_TURN_OFF;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrTurnOff_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void TURN_OFF_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			good_key_beep();

			GuiVar_TurnOffControllerIsOff = !GuiVar_TurnOffControllerIsOff;

			NETWORK_CONFIG_set_scheduled_irrigation_off( GuiVar_TurnOffControllerIsOff, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, NETWORK_CONFIG_get_change_bits_ptr(CHANGE_REASON_KEYPAD) );

			GuiVar_MenuScreenToShow = CP_MAIN_MENU_STATUS;

			// 5/12/2015 ajv : Trigger a Next Scheduled Irrigation calculation to ensure the controller
			// reports that it's turned OFF or that there's something scheduled.
			postBackground_Calculation_Event( BKGRND_CALC_NEXT_SCHEDULED );

			// Return to the Status screen by rolling back
			// the screen history and redrawing the initial
			// screen.
			while( screen_history_index > 0 )
			{
				--screen_history_index;
			}

			Display_Post_Command( &ScreenHistory[ screen_history_index ] );
			break;

		case KEY_BACK:
			switch( ScreenHistory[ screen_history_index ]._02_menu )
			{
				case SCREEN_MENU_MAIN:
					GuiVar_MenuScreenToShow = CP_MAIN_MENU_STATUS;
					break;

				default:
					GuiVar_MenuScreenToShow = CP_MAIN_MENU_NO_WATER_DAYS;
			}

			// No need to break here as this will allow it to fall into
			// the default case

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

