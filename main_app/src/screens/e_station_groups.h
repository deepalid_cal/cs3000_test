/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_E_STATION_GROUP_H
#define _INC_E_STATION_GROUP_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern BOOL_32 STATION_GROUP_crop_coefficients_for_this_plant_and_exposure_are_equal( const UNS_32 pplant_type, const UNS_32 pexposure );

// ----------

extern void FDTO_STATION_GROUP_draw_menu( const BOOL_32 pcomplete_redraw );

extern void STATION_GROUP_process_menu( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

