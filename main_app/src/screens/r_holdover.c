/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"r_holdover.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"change.h"

#include	"cs3000_comm_server_common.h"

#include	"d_process.h"

#include	"irri_comm.h"

#include	"report_utils.h"

#include	"scrollbox.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_HOLDOVER_clear_values( const UNS_32 pline_index_0 )
{
	STATION_STRUCT	*lstation;

	UNS_32	lbox_index_0, lstation_number_0;

	UNS_32	i;

	// ----------

	lstation = STATION_get_first_available_station();

	// ----------

	for( i = 0; i < pline_index_0; ++i )
	{
		lbox_index_0 = nm_STATION_get_box_index_0( lstation );

		lstation_number_0 = nm_STATION_get_station_number_0( lstation );

		// 3/10/2014 ajv : Since STATION_get_next_available_station expects the
		// station number as 1-based, increment the 0-based value by one.
		lstation_number_0 += 1;

		lstation = STATION_get_next_available_station( &lbox_index_0, &lstation_number_0 );
	}

	// ---------------------

	lbox_index_0 = nm_STATION_get_box_index_0( lstation );

	lstation_number_0 = nm_STATION_get_station_number_0( lstation );

	// ----------

	// 9/14/2015 ajv : No need to check whether a rain value exists here... we handle that
	// directly within the set function.
	STATION_HISTORY_set_rain_min_10u( lbox_index_0, lstation_number_0, 0, INITIATED_VIA_KEYPAD );

	// ----------

	// 3/14/2017 ajv : Also, ignore the moisture balance setting on the next irrigation and snap
	// it back to 50% at the conclusion, effectively resetting the rain for customers using
	// WaterSense as well.
	STATION_set_ignore_moisture_balance_at_next_irrigation_by_station( lbox_index_0, lstation_number_0, CHANGE_REASON_KEYPAD );

	// ----------

	GuiLib_ScrollBox_RedrawLine( 0, (UNS_16)pline_index_0 );

	// 5/5/2015 ajv : If we scroll down automatically to help users clear hold-over across
	// multiple stations quickly, we can trigger a WDT. This is because the process of scrolling
	// the list is so intensive that we starve the other tasks. We need to come up with a better
	// way to handle this.
	// 
	//GuiLib_ScrollBox_Down( 0 ); 

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
static void nm_HOLDOVER_load_guivars_for_scroll_line( const INT_16 pline_index_0_i16 )
{
	const volatile float ONE_HUNDRED_THOUSAND = 100000.0;

	const volatile float ONE_HUNDRED = 100.0;

	const volatile float SIXTY = 60.0;

	float	MB_inches_beyond_50_percent_of_RZWWS, precip_rate_in_per_min;

	STATION_STRUCT	*lstation;

	lstation = STATION_get_first_available_station();

	// ---------------------

	UNS_32	lbox_index_0, lstation_number_0;

	UNS_32	i;

	for( i = 0; i < pline_index_0_i16; ++i )
	{
		lbox_index_0 = nm_STATION_get_box_index_0( lstation );

		lstation_number_0 = nm_STATION_get_station_number_0( lstation );

		// 3/10/2014 ajv : Since STATION_get_next_available_station expects the
		// station number as 1-based, increment the 0-based value by one.
		lstation_number_0 += 1;

		lstation = STATION_get_next_available_station( &lbox_index_0, &lstation_number_0 );
	}

	// ---------------------

	lbox_index_0 = nm_STATION_get_box_index_0( lstation );

	lstation_number_0 = nm_STATION_get_station_number_0( lstation );

	// ---------------------
	
	char	str_16[ 16 ];

	GuiVar_RptController = lbox_index_0;

	snprintf( GuiVar_RptStation, sizeof(GuiVar_RptStation), "%s", STATION_get_station_number_string( lbox_index_0, lstation_number_0, (char*)&str_16, sizeof(str_16) ) );

	// 3/14/2017 ajv : For stations using Daily ET, we have to calculate the time based on the
	// current Moisture Balance level. Otherwise, just read the accumulated rain out of the
	// station preserves record.
	if( WEATHER_get_station_uses_daily_et(lstation) == (true) )
	{
		// 3/14/2017 ajv : The balance beyond 50% is calculated by taking the current moisture
		// balance and subtracting 50% of the RZWWS.
		MB_inches_beyond_50_percent_of_RZWWS = STATION_get_moisture_balance(lstation) - (STATION_MOISTURE_BALANCE_DEFAULT * (STATION_GROUP_get_soil_storage_capacity_inches_100u(lstation) / ONE_HUNDRED));

		// 3/15/2017 ajv : If the MB is over 50% and the flag to clear it has NOT been set,
		// calculate the accumulated rain.
		if( (MB_inches_beyond_50_percent_of_RZWWS > 0) && (nm_STATION_get_ignore_moisture_balance_at_next_irrigation_flag(lstation) == STATION_MOISTURE_BALANCE_CLEAR_IGNORE_FLAG) )
		{
			precip_rate_in_per_min = (float)((STATION_GROUP_get_station_precip_rate_in_per_hr_100000u(lstation) / ONE_HUNDRED_THOUSAND) / SIXTY);

			GuiVar_RptRainMin = (MB_inches_beyond_50_percent_of_RZWWS / precip_rate_in_per_min);
		}
		else
		{
			GuiVar_RptRainMin = 0.0F;
		}
	}
	else
	{
		GuiVar_RptRainMin = STATION_HISTORY_get_rain_min( lbox_index_0, lstation_number_0 );
	}
}

/* ---------------------------------------------------------- */
extern void FDTO_HOLDOVER_draw_report( const BOOL_32 pcomplete_redraw )
{
	GuiLib_ShowScreen( GuiStruct_rptHoldOver_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );

	if( pcomplete_redraw == (true) )
	{
		GuiVar_StationDescription[ 0 ] = 0x00;
	}

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	FDTO_REPORTS_draw_report( pcomplete_redraw, STATION_get_num_stations_in_use(), &nm_HOLDOVER_load_guivars_for_scroll_line );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void HOLDOVER_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			good_key_beep();

			lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
			lde._04_func_ptr = (void*)&FDTO_HOLDOVER_clear_values;
			lde._06_u32_argument1 = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );
			Display_Post_Command( &lde );
			break;

		default:
			REPORTS_process_report( pkey_event, 0, 0 );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

