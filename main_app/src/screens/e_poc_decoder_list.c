/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memset
#include	<string.h>

#include	"e_poc_decoder_list.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"battery_backed_vars.h"

#include	"change.h"

#include	"combobox.h"

#include	"configuration_controller.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"d_two_wire_assignment.h"

#include	"dialog.h"

#include	"flowsense.h"

#include	"group_base_file.h"

#include	"m_main.h"

#include	"poc.h"

#include	"scrollbox.h"

#include	"speaker.h"

#include	"tpmicro_data.h"

#include	"two_wire_utils.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_TWO_WIRE_POC_IN_USE				(0)
#define	CP_TWO_WIRE_POC_USED_AS_A_BYPASS	(1)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static BOOL_32	g_TWO_WIRE_POC_editing_decoder;

static UNS_32	*g_TWO_WIRE_POC_combo_box_guivar;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void FDTO_TWO_WIRE_POC_return_to_menu( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_POC_show_yes_no_dropdown( const UNS_32 px_coord, const UNS_32 py_coord )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( px_coord, py_coord, *g_TWO_WIRE_POC_combo_box_guivar );
}

/* ---------------------------------------------------------- */
static void TWO_WIRE_POC_copy_decoder_info_into_guivars( const UNS_32 pdecoder_index_0 )
{
	UNS_32		lbox_index_0;

	POC_GROUP_STRUCT	*lpoc;

	UNS_32		i;
	
	// ----------
	
	xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );

	GuiVar_TwoWireDecoderSN = decoder_info_for_display[ pdecoder_index_0 ].sn;

	GuiVar_TwoWireDecoderFWVersion = decoder_info_for_display[ pdecoder_index_0 ].fw_vers;

	xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );
	
	// ----------
	
	// 6/2/2016 rmd : Set default exit values for the gui.
	GuiVar_TwoWirePOCInUse = (false);
	
	GuiVar_TwoWirePOCUsedForBypass = (false);
	
	// ----------
	
	if (GuiVar_TwoWireDecoderSN > 0 )
	{
		xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

		// 4/16/2014 ajv : For now, only look at decoders which are
		// physically connected to THIS controller
		lbox_index_0 = FLOWSENSE_get_controller_index();

		// 6/9/2016 rmd : It is 'in use' if there is a bypass or single with this serial number.
		// Only if it is set to SHOW would we flag it as in use. You can get into the predictament
		// where if the decoder and file POC existed, then you did a discovery without the decoder
		// and using the ORPHANS screen unlinked it. Well then it doesn't belong to a BYPASS. And
		// the SINGLE is set not to SHOW. SO rightfully it should be indicated as NOT IN USE on this
		// screen.
		if( nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn( lbox_index_0, POC__FILE_TYPE__DECODER_SINGLE, GuiVar_TwoWireDecoderSN, POC_SET_TO_SHOW_ONLY ) )
		{
			GuiVar_TwoWirePOCInUse = (true);
		}

		// 6/9/2016 rmd : If it IS in the BYPASS - then the bypass MUST be set to SHOW. That is the
		// way our logic works. So only search for those set to show.
		if( (lpoc = nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn( lbox_index_0, POC__FILE_TYPE__DECODER_BYPASS, 0, POC_SET_TO_SHOW_ONLY )) )
		{
			// 6/9/2016 rmd : Just because we have a bypass doesn't mean the decoder is in it. Check for
			// that.
			for( i=0; i<POC_BYPASS_LEVELS_MAX; i++ )
			{
				if( POC_get_decoder_serial_number_for_this_poc_and_level_0( lpoc, i ) == GuiVar_TwoWireDecoderSN )
				{
					GuiVar_TwoWirePOCInUse = (true);
					
					GuiVar_TwoWirePOCUsedForBypass = (true);

					break;
				}
			}
		}

		xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
	}
	
}

/* ---------------------------------------------------------- */
static void TWO_WIRE_POC_extract_and_store_changes_from_guivars( void )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	POC_GROUP_STRUCT	*lpoc;

	UNS_32	lbox_index_0;

	UNS_32	i;

	UNS_32	ldecoder_serial_number_from_file;
	
	BOOL_32	assigned;

	BOOL_32	no_other_decoders_assigned_to_this_bypass;

	char	str_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	// ----------

	lbox_index_0 = FLOWSENSE_get_controller_index();
	
	lpoc = NULL;
	
	// ----------

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	// 6/9/2016 rmd : The code shall
	//
	// 1. If the user specifies the decoder is to be taken out of use right now we will do
	// nothing. We may have to adjust this behavior as needed based upon outcomes.
	//
	// 2. If the user has specified in use and to not be part of a bypass we check two things.
	// Can we find a bypass at this box and is this decoder in it. If so set the decoder serial
	// number to 0. And second does a SINGLE POC exist for this decoder, if so set it to show,
	// if not create one.
	//
	// 3. If the user has specified in use and to be part of a bypass the first thing we do is
	// to see if a SINGLE POC exists and if so mark it to not SHOW. Then we look for a bypass at
	// this box. If we find one we look for an open slot to add this decoder to it. If we don't
	// find one we create one and add the decoder to it.
	//
	// 4. Leave town.
	
	if( GuiVar_TwoWirePOCInUse )
	{
		if( GuiVar_TwoWirePOCUsedForBypass )
		{
			// 6/9/2016 rmd : User want it to be part of a BYPASS, see if this decoder is in a SINGLE
			// now that is set to SHOW.
			if( (lpoc = nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn( lbox_index_0, POC__FILE_TYPE__DECODER_SINGLE, GuiVar_TwoWireDecoderSN, POC_SET_TO_SHOW_ONLY )) )
			{
				// 6/9/2016 rmd : Must update the change bitfield ptr for the NEW poc we are working with.
				lchange_bitfield_to_set = POC_get_change_bits_ptr( lpoc, CHANGE_REASON_KEYPAD );
				
				// 6/9/2016 rmd : Now set to NOT SHOW.
				nm_POC_set_show_this_poc( lpoc, (false), CHANGE_do_not_generate_change_line, 0, lbox_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
			}
			
			// 6/9/2016 rmd : Now find the BYPASS. Either SHOWING or not. Remember it could already be
			// showing with another decoder in it. Also note decoder serial number is irrelevant when
			// looking for a BYPASS.
			if( (lpoc = nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn( lbox_index_0, POC__FILE_TYPE__DECODER_BYPASS, 0, POC_SET_TO_SHOW_OR_NOT )) )
			{
				// 6/9/2016 rmd : Must update the change bitfield ptr for the NEW poc we are working with.
				lchange_bitfield_to_set = POC_get_change_bits_ptr( lpoc, CHANGE_REASON_KEYPAD );
			}
			else
			{
				// 6/9/2016 rmd : No BYPASS found. Create one.
				lpoc = nm_POC_create_new_group( lbox_index_0, POC__FILE_TYPE__DECODER_BYPASS, GuiVar_TwoWireDecoderSN );
		
				// 6/9/2016 rmd : Must update the change bitfield ptr for the NEW poc we are working with.
				lchange_bitfield_to_set = POC_get_change_bits_ptr( lpoc, CHANGE_REASON_KEYPAD );
		
				nm_POC_set_poc_type( lpoc, POC__FILE_TYPE__DECODER_BYPASS, CHANGE_do_not_generate_change_line, 0, lbox_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
		
				snprintf( str_48, NUMBER_OF_CHARS_IN_A_NAME, "Bypass Manifold @ %c", lbox_index_0 + 'A' );
				nm_POC_set_name( lpoc, str_48, CHANGE_do_not_generate_change_line, 0, lbox_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
			}
			
			// 6/9/2016 rmd : Now assign the decoder to the BYPASS and set it to SHOW. Either if we
			// created it or not these steps take place.
			nm_POC_set_show_this_poc( lpoc, (true), CHANGE_do_not_generate_change_line, 0, lbox_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
			
			// ----------
			
			// 6/9/2016 rmd : Assign the decoder serial number if an open slot can be found. If not
			// error message to the screen. If we just created this bypass it will be slot 0.
			assigned = (false);
			
			for( i=0; i<POC_BYPASS_LEVELS_MAX; i++ )
			{
				if( POC_get_decoder_serial_number_for_this_poc_and_level_0( lpoc, i ) == DECODER_SERIAL_NUM_DEFAULT )
				{
					nm_POC_set_decoder_serial_number( lpoc, (i + 1), GuiVar_TwoWireDecoderSN, CHANGE_do_not_generate_change_line, 0, lbox_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );

					assigned = (true);
					
					break;
				}
			}

			if( !assigned )
			{
				GuiVar_TwoWirePOCInUse = (false);
				GuiVar_TwoWirePOCUsedForBypass = (false);

				DIALOG_draw_ok_dialog( GuiStruct_dlgTooManyBypassDecoders_0 );

				Alert_Message( "No decoder slot available in bypass" );
			}
		}
		else
		{
			// 6/9/2016 rmd : So the user wants to set it to NOT be part of a BYPASS. Let's see if we
			// can find a bypass. Technically we only have to search for those set to SHOW, (because
			// once created a bypass can only be set to not show when all serial numbers are 0), but
			// doesn't hurt to search for SHOW or NOT_SHOW. Remember serial number doesn't matter in a
			// search for a BYPASS, only box_index and poc_type. This counts on the rule that there can
			// only be one bypass per box.
			if( (lpoc = nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn( lbox_index_0, POC__FILE_TYPE__DECODER_BYPASS, 0, POC_SET_TO_SHOW_OR_NOT )) )
			{
				// 6/9/2016 rmd : Must update the change bitfield ptr for the NEW poc we are working with.
				lchange_bitfield_to_set = POC_get_change_bits_ptr( lpoc, CHANGE_REASON_KEYPAD );

				// 6/9/2016 rmd : So we found a bypass. Sift through it to find if the serial number is in
				// it. If so set to 0. And if after that all ser no slots empty set to NOT SHOW as it is an
				// empty bypass.
				no_other_decoders_assigned_to_this_bypass = (true);

				// 6/9/2016 rmd : I think we should search through ALL levels looking for this serial
				// number. To purge any instance of it from the bypass. If the user assigned it to level 3
				// then set to a 2 stage bypass it could effectively be hidden. Just seems we should clean
				// it up here.
				for( i=0; i<POC_BYPASS_LEVELS_MAX; i++ )
				{
					ldecoder_serial_number_from_file = POC_get_decoder_serial_number_for_this_poc_and_level_0( lpoc, i );

					if( ldecoder_serial_number_from_file == GuiVar_TwoWireDecoderSN )
					{
						nm_POC_set_decoder_serial_number( lpoc, (i + 1), DECODER_SERIAL_NUM_DEFAULT, CHANGE_do_not_generate_change_line, 0, lbox_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
					}
					else
					if( ldecoder_serial_number_from_file != DECODER_SERIAL_NUM_DEFAULT )
					{
						no_other_decoders_assigned_to_this_bypass = (false);
					}
				}

				if( no_other_decoders_assigned_to_this_bypass )
				{
					// 6/1/2016 rmd : So right now there are NO DECODERS assigned to this bypass poc. So hide it
					// from the users view and prevent it from syncing into poc_preserves.
					nm_POC_set_show_this_poc( lpoc, (false), CHANGE_do_not_generate_change_line, 0, lbox_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
				}
			}	
				
			// ----------
			
			// 6/9/2016 rmd : NOW see if the SINGLE decoder based POC exists for this decoder serial
			// number. If it does set it to SHOW. If not create it.
			if( (lpoc = nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn( lbox_index_0, POC__FILE_TYPE__DECODER_SINGLE, GuiVar_TwoWireDecoderSN, POC_SET_TO_SHOW_OR_NOT )) )
			{
				// 6/9/2016 rmd : Must update the change bitfield ptr for the NEW poc we are working with.
				lchange_bitfield_to_set = POC_get_change_bits_ptr( lpoc, CHANGE_REASON_KEYPAD );
			}
			else
			{
				// 6/9/2016 rmd : No SINGLE found. Create one.
				lpoc = nm_POC_create_new_group( lbox_index_0, POC__FILE_TYPE__DECODER_SINGLE, GuiVar_TwoWireDecoderSN );
		
				// 6/9/2016 rmd : Must update the change bitfield ptr for the NEW poc we are working with.
				lchange_bitfield_to_set = POC_get_change_bits_ptr( lpoc, CHANGE_REASON_KEYPAD );
		
				nm_POC_set_poc_type( lpoc, POC__FILE_TYPE__DECODER_SINGLE, CHANGE_do_not_generate_change_line, 0, lbox_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
		
				nm_POC_set_decoder_serial_number( lpoc, 1, GuiVar_TwoWireDecoderSN, CHANGE_do_not_generate_change_line, 0, lbox_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
				
				snprintf( str_48, NUMBER_OF_CHARS_IN_A_NAME, "Decoder %06d @ %c", GuiVar_TwoWireDecoderSN, lbox_index_0 + 'A' );
				nm_POC_set_name( lpoc, str_48, CHANGE_do_not_generate_change_line, 0, lbox_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
			}
			
			// 6/9/2016 rmd : Now set it to SHOW. Either if we created it or not.
			nm_POC_set_show_this_poc( lpoc, (true), CHANGE_do_not_generate_change_line, 0, lbox_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
		}
	}


	// ----------

	// 5/7/2014 ajv : Since we've potentially added a new POC to the file, force
	// a sync with the preserves to prevent the "TP_COMM: poc not found" alert.
	POC_PRESERVES_synchronize_preserves_to_file();

	// ----------

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void TWO_WIRE_POC_process_decoder_list( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_cbxNoYes_0:
			COMBO_BOX_key_press( pkey_event.keycode, g_TWO_WIRE_POC_combo_box_guivar );
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_TWO_WIRE_POC_IN_USE:
							good_key_beep();

							g_TWO_WIRE_POC_combo_box_guivar = &GuiVar_TwoWirePOCInUse;

							lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
							lde._04_func_ptr = (void*)&FDTO_POC_show_yes_no_dropdown;
							lde._06_u32_argument1 = 227;
							lde._07_u32_argument2 = 26;
							Display_Post_Command( &lde );
							break;

						case CP_TWO_WIRE_POC_USED_AS_A_BYPASS:
							good_key_beep();

							g_TWO_WIRE_POC_combo_box_guivar = &GuiVar_TwoWirePOCUsedForBypass;

							lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
							lde._04_func_ptr = (void*)&FDTO_POC_show_yes_no_dropdown;
							lde._06_u32_argument1 = 293;
							lde._07_u32_argument2 = 46;
							Display_Post_Command( &lde );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_PLUS:
				case KEY_MINUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_TWO_WIRE_POC_IN_USE:
							process_bool( &GuiVar_TwoWirePOCInUse );

							// 4/16/2014 ajv : Since this causes information to appear
							// or disappear from the screen, a full redraw is necessary
							// rather than just a GuiLib_Refresh.
							Redraw_Screen( (false) );
							break;

						case CP_TWO_WIRE_POC_USED_AS_A_BYPASS:
							process_bool( &GuiVar_TwoWirePOCUsedForBypass );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_NEXT:
				case KEY_PREV:
					GROUP_process_NEXT_and_PREV( pkey_event.keycode, GuiVar_TwoWireNumDiscoveredPOCDecoders, (void*)&TWO_WIRE_POC_extract_and_store_changes_from_guivars, NULL, (void*)&TWO_WIRE_POC_copy_decoder_info_into_guivars );

					// 10/24/2013 ajv : Update the on-screen indicator of which decoder the user
					// is on and refresh the screen to update it.
					GuiVar_TwoWirePOCDecoderIndex = (g_GROUP_list_item_index + 1);
					Refresh_Screen();
					break;

				case KEY_C_UP:
					CURSOR_Up( (true) );
					break;

				case KEY_C_DOWN:
					CURSOR_Down( (true) );
					break;

				case KEY_C_LEFT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_TWO_WIRE_POC_IN_USE:
							KEY_process_BACK_from_editing_screen( (void*)&FDTO_TWO_WIRE_POC_return_to_menu );
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_BACK:
					KEY_process_BACK_from_editing_screen( (void*)&FDTO_TWO_WIRE_POC_return_to_menu );
					break;

				default:
					KEY_process_global_keys( pkey_event );
			}

	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_TWO_WIRE_POC_return_to_menu( void )
{
	FDTO_GROUP_return_to_menu( &g_TWO_WIRE_POC_editing_decoder, (void*)&TWO_WIRE_POC_extract_and_store_changes_from_guivars );
}

/* ---------------------------------------------------------- */
extern void TWO_WIRE_POC_load_decoder_serial_number_into_guivar( const INT_16 pindex_0_i16 )
{
	snprintf( GuiVar_itmGroupName, sizeof(GuiVar_itmGroupName), "  %s %07d", GuiLib_GetTextPtr( GuiStruct_txtSN_0, 0 ), decoder_info_for_display[ pindex_0_i16 ].sn );
}

/* ---------------------------------------------------------- */
extern void FDTO_TWO_WIRE_POC_draw_menu( const BOOL_32 pcomplete_redraw )
{
	UNS_32 i;

	if( pcomplete_redraw == (true) )
	{
		g_GROUP_list_item_index = 0;

		memset( decoder_info_for_display, 0x00, sizeof(decoder_info_for_display) );

		GuiVar_TwoWireNumDiscoveredPOCDecoders = 0;

		// 4/17/2014 ajv : Run through the Decoder list to get a count and make
		// a local copy of just the POC decoders.
		for( i = 0; i < MAX_DECODERS_IN_A_BOX; ++i )
		{
			if( tpmicro_data.decoder_info[ i ].sn != DECODER_SERIAL_NUM_DEFAULT )
			{
				if( tpmicro_data.decoder_info[ i ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__POC )
				{
					decoder_info_for_display[ GuiVar_TwoWireNumDiscoveredPOCDecoders ].sn = tpmicro_data.decoder_info[ i ].sn;

					decoder_info_for_display[ GuiVar_TwoWireNumDiscoveredPOCDecoders ].fw_vers = tpmicro_data.decoder_info[ i ].id_info.fw_vers;

					GuiVar_TwoWireNumDiscoveredPOCDecoders += 1;
				}
			}
			else
			{
				// 5/19/2014 ajv : We've bumped into the first uninitialized
				// decoder which means we're done.
				break;
			}
		}
	}

	FDTO_GROUP_draw_menu( pcomplete_redraw, &g_TWO_WIRE_POC_editing_decoder, GuiVar_TwoWireNumDiscoveredPOCDecoders, GuiStruct_scrTwoWirePOCAssignment_0, (void *)&TWO_WIRE_POC_load_decoder_serial_number_into_guivar, (void *)&TWO_WIRE_POC_copy_decoder_info_into_guivars, (false) );

	// 10/24/2013 ajv : Update the on-screen indicator of which decoder the user
	// is on and refresh the screen to update it.
	GuiVar_TwoWirePOCDecoderIndex = (g_GROUP_list_item_index + 1);
	Refresh_Screen();
}

/* ---------------------------------------------------------- */
extern void TWO_WIRE_POC_process_menu( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	if( (!g_TWO_WIRE_POC_editing_decoder) && (pkey_event.keycode == KEY_BACK) )
	{
		GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;

		KEY_process_global_keys( pkey_event );

		// 3/17/2016 ajv : Since the user got here from the 2-WIRE dialog box, redraw that dialog
		// box.
		TWO_WIRE_ASSIGNMENT_draw_dialog( (true) );
	}
	else
	{
		GROUP_process_menu( pkey_event, &g_TWO_WIRE_POC_editing_decoder, GuiVar_TwoWireNumDiscoveredPOCDecoders, (void *)&TWO_WIRE_POC_process_decoder_list, NULL, NULL, (void *)&TWO_WIRE_POC_copy_decoder_info_into_guivars );

		// 10/24/2013 ajv : Update the on-screen indicator of which decoder the user
		// is on and refresh the screen to update it.
		GuiVar_TwoWirePOCDecoderIndex = (g_GROUP_list_item_index + 1);
		Refresh_Screen();
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

