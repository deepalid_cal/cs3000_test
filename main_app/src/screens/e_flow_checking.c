/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_flow_checking.h"

#include	"app_startup.h"

#include	"combobox.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"group_base_file.h"

#include	"irrigation_system.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_FLOW_CHECKING_IN_USE					(0)
#define CP_FLOW_CHECKING_RANGE_1				(1)
#define CP_FLOW_CHECKING_TOLERANCE_PLUS_1		(2)
#define CP_FLOW_CHECKING_TOLERANCE_MINUS_1		(3)
#define CP_FLOW_CHECKING_RANGE_2				(4)
#define CP_FLOW_CHECKING_TOLERANCE_PLUS_2		(5)
#define CP_FLOW_CHECKING_TOLERANCE_MINUS_2		(6)
#define CP_FLOW_CHECKING_RANGE_3				(7)
#define CP_FLOW_CHECKING_TOLERANCE_PLUS_3		(8)
#define CP_FLOW_CHECKING_TOLERANCE_MINUS_3		(9)
#define CP_FLOW_CHECKING_TOLERANCE_PLUS_4		(10)
#define CP_FLOW_CHECKING_TOLERANCE_MINUS_4		(11)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static BOOL_32 g_FLOW_CHECKING_editing_group;

static UNS_32 g_FLOW_CHECKING_prev_cursor_pos;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void FDTO_FLOW_CHECKING_return_to_menu( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_FLOW_CHECKING_show_dropdown( void )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( 272, 34, GuiVar_SystemFlowCheckingInUse );
}

/* ---------------------------------------------------------- */
static void FLOW_CHECKING_process_group( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_cbxNoYes_0:
			COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_SystemFlowCheckingInUse );
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_FLOW_CHECKING_IN_USE:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_FLOW_CHECKING_show_dropdown;
							Display_Post_Command( &lde );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_FLOW_CHECKING_IN_USE:
							process_bool( &GuiVar_SystemFlowCheckingInUse );
							Redraw_Screen( (false) );
							break;

						case CP_FLOW_CHECKING_RANGE_1:
							process_uns32( pkey_event.keycode, &GuiVar_SystemFlowCheckingRange1, IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_MIN, (GuiVar_SystemFlowCheckingRange2 - 1), 1, (false) );
							break;

						case CP_FLOW_CHECKING_RANGE_2:
							process_uns32( pkey_event.keycode, &GuiVar_SystemFlowCheckingRange2, (GuiVar_SystemFlowCheckingRange1 + 1), (GuiVar_SystemFlowCheckingRange3 - 1), 1, (false) );
							break;

						case CP_FLOW_CHECKING_RANGE_3:
							process_uns32( pkey_event.keycode, &GuiVar_SystemFlowCheckingRange3, (GuiVar_SystemFlowCheckingRange2 + 1), IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_MAX, 1, (false) );
							break;

						case CP_FLOW_CHECKING_TOLERANCE_PLUS_1:
							process_uns32( pkey_event.keycode, &GuiVar_SystemFlowCheckingTolerancePlus1, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MIN, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MAX, 1, (false) );
							break;

						case CP_FLOW_CHECKING_TOLERANCE_PLUS_2:
							process_uns32( pkey_event.keycode, &GuiVar_SystemFlowCheckingTolerancePlus2, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MIN, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MAX, 1, (false) );
							break;

						case CP_FLOW_CHECKING_TOLERANCE_PLUS_3:
							process_uns32( pkey_event.keycode, &GuiVar_SystemFlowCheckingTolerancePlus3, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MIN, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MAX, 1, (false) );
							break;

						case CP_FLOW_CHECKING_TOLERANCE_PLUS_4:
							process_uns32( pkey_event.keycode, &GuiVar_SystemFlowCheckingTolerancePlus4, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MIN, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MAX, 1, (false) );
							break;

						case CP_FLOW_CHECKING_TOLERANCE_MINUS_1:
							GuiVar_SystemFlowCheckingToleranceMinus1 = GuiVar_SystemFlowCheckingToleranceMinus1 * -1;
							process_int32( pkey_event.keycode, &GuiVar_SystemFlowCheckingToleranceMinus1, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MIN, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MAX, 1, (false) );
							GuiVar_SystemFlowCheckingToleranceMinus1 = GuiVar_SystemFlowCheckingToleranceMinus1 * -1;
							break;

						case CP_FLOW_CHECKING_TOLERANCE_MINUS_2:
							GuiVar_SystemFlowCheckingToleranceMinus2 = GuiVar_SystemFlowCheckingToleranceMinus2 * -1;
							process_int32( pkey_event.keycode, &GuiVar_SystemFlowCheckingToleranceMinus2, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MIN, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MAX, 1, (false) );
							GuiVar_SystemFlowCheckingToleranceMinus2 = GuiVar_SystemFlowCheckingToleranceMinus2 * -1;
							break;

						case CP_FLOW_CHECKING_TOLERANCE_MINUS_3:
							GuiVar_SystemFlowCheckingToleranceMinus3 = GuiVar_SystemFlowCheckingToleranceMinus3 * -1;
							process_int32( pkey_event.keycode, &GuiVar_SystemFlowCheckingToleranceMinus3, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MIN, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MAX, 1, (false) );
							GuiVar_SystemFlowCheckingToleranceMinus3 = GuiVar_SystemFlowCheckingToleranceMinus3 * -1;
							break;

						case CP_FLOW_CHECKING_TOLERANCE_MINUS_4:
							GuiVar_SystemFlowCheckingToleranceMinus4 = GuiVar_SystemFlowCheckingToleranceMinus4 * -1;
							process_int32( pkey_event.keycode, &GuiVar_SystemFlowCheckingToleranceMinus4, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MIN, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MAX, 1, (false) );
							GuiVar_SystemFlowCheckingToleranceMinus4 = GuiVar_SystemFlowCheckingToleranceMinus4 * -1;
							break;

						default:
							bad_key_beep();
					}
					Refresh_Screen();
					break;

				case KEY_NEXT:
				case KEY_PREV:
					GROUP_process_NEXT_and_PREV( pkey_event.keycode, SYSTEM_num_systems_in_use(), (void*)&FLOW_CHECKING_extract_and_store_changes_from_GuiVars, (void*)&SYSTEM_get_group_at_this_index, (void*)&FLOW_CHECKING_copy_group_into_guivars );
					break;

				case KEY_C_UP:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_FLOW_CHECKING_RANGE_1:
						case CP_FLOW_CHECKING_TOLERANCE_PLUS_1:
						case CP_FLOW_CHECKING_TOLERANCE_MINUS_1:
							g_FLOW_CHECKING_prev_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

							CURSOR_Select( CP_FLOW_CHECKING_IN_USE, (true) );
							break;

						case CP_FLOW_CHECKING_RANGE_2:
						case CP_FLOW_CHECKING_RANGE_3:
						case CP_FLOW_CHECKING_TOLERANCE_PLUS_2:
						case CP_FLOW_CHECKING_TOLERANCE_PLUS_3:
						case CP_FLOW_CHECKING_TOLERANCE_MINUS_2:
						case CP_FLOW_CHECKING_TOLERANCE_MINUS_3:
							CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo - 3), (true) );
							break;

						case CP_FLOW_CHECKING_TOLERANCE_PLUS_4:
						case CP_FLOW_CHECKING_TOLERANCE_MINUS_4:
							CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo - 2), (true) );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_C_DOWN:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_FLOW_CHECKING_IN_USE:
							if( g_FLOW_CHECKING_prev_cursor_pos == 0 )
							{
								g_FLOW_CHECKING_prev_cursor_pos = CP_FLOW_CHECKING_RANGE_1;
							}

							CURSOR_Select( g_FLOW_CHECKING_prev_cursor_pos, (true) );
							break;

						case CP_FLOW_CHECKING_RANGE_1:
						case CP_FLOW_CHECKING_RANGE_2:
						case CP_FLOW_CHECKING_TOLERANCE_PLUS_1:
						case CP_FLOW_CHECKING_TOLERANCE_PLUS_2:
						case CP_FLOW_CHECKING_TOLERANCE_MINUS_1:
						case CP_FLOW_CHECKING_TOLERANCE_MINUS_2:
							CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo + 3), (true) );
							break;

						case CP_FLOW_CHECKING_TOLERANCE_PLUS_3:
						case CP_FLOW_CHECKING_TOLERANCE_MINUS_3:
							CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo + 2), (true) );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_C_LEFT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_FLOW_CHECKING_IN_USE:
							KEY_process_BACK_from_editing_screen( (void*)&FDTO_FLOW_CHECKING_return_to_menu );
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_BACK:
					KEY_process_BACK_from_editing_screen( (void*)&FDTO_FLOW_CHECKING_return_to_menu );
					break;

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_FLOW_CHECKING_return_to_menu( void )
{
	FDTO_GROUP_return_to_menu( &g_FLOW_CHECKING_editing_group, (void*)&FLOW_CHECKING_extract_and_store_changes_from_GuiVars );
}

/* ---------------------------------------------------------- */
extern void FDTO_FLOW_CHECKING_draw_menu( const BOOL_32 pcomplete_redraw )
{
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	FDTO_GROUP_draw_menu( pcomplete_redraw, &g_FLOW_CHECKING_editing_group, SYSTEM_num_systems_in_use(), GuiStruct_scrFlowChecking_0, (void *)&SYSTEM_load_system_name_into_scroll_box_guivar, (void *)&FLOW_CHECKING_copy_group_into_guivars, (false) );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void FLOW_CHECKING_process_menu( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	GROUP_process_menu( pkey_event, &g_FLOW_CHECKING_editing_group, SYSTEM_num_systems_in_use(), (void *)&FLOW_CHECKING_process_group, NULL, (void *)&SYSTEM_get_group_at_this_index, (void *)&FLOW_CHECKING_copy_group_into_guivars );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

