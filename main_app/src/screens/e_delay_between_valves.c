/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_delay_between_valves.h"

#include	"app_startup.h"

#include	"combobox.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"group_base_file.h"

#include	"screen_utils.h"

#include	"station_groups.h"

#include	"m_main.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_DELAY_BETWEEN_VALVES_ENABLED		(0)
#define	CP_DELAY_BETWEEN_VALVES_0			(1)
#define	CP_DELAY_BETWEEN_VALVES_1			(2)
#define	CP_DELAY_BETWEEN_VALVES_2			(3)
#define	CP_DELAY_BETWEEN_VALVES_3			(4)
#define	CP_DELAY_BETWEEN_VALVES_4			(5)
#define	CP_DELAY_BETWEEN_VALVES_5			(6)
#define	CP_DELAY_BETWEEN_VALVES_6			(7)
#define	CP_DELAY_BETWEEN_VALVES_7			(8)
#define	CP_DELAY_BETWEEN_VALVES_8			(9)
#define	CP_DELAY_BETWEEN_VALVES_9			(10)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_DELAY_BETWEEN_VALVES_show_in_use_dropdown( void )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( 274, 19, GuiVar_DelayBetweenValvesInUse );
}

/* ---------------------------------------------------------- */
static void FDTO_DELAY_BETWEEN_VALVES_close_in_use_dropdown( void )
{
	GuiVar_DelayBetweenValvesInUse = (BOOL_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );

	FDTO_COMBOBOX_hide();
}

/* ---------------------------------------------------------- */
extern void FDTO_DELAY_BETWEEN_VALVES_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		DELAY_BETWEEN_VALVES_copy_group_into_guivars();

		lcursor_to_select = CP_DELAY_BETWEEN_VALVES_ENABLED;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrDelayBetweenValves_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * @task_execution Executed within the context of the KEY PROCESSING task.
 */
extern void DELAY_BETWEEN_VALVES_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_cbxNoYes_0:
			// 6/16/2015 ajv : If the user closed the dropdown, we need to redraw the screen to reflect
			// the change. Therefore, handle the processing of SELECT and BACK manually to cover for
			// this.
			if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
			{
				good_key_beep();

				lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
				lde._04_func_ptr = FDTO_DELAY_BETWEEN_VALVES_close_in_use_dropdown;
				Display_Post_Command( &lde );

				Redraw_Screen( (false) );
			}
			else
			{
				COMBO_BOX_key_press( pkey_event.keycode, NULL );
			}
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_DELAY_BETWEEN_VALVES_ENABLED:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_DELAY_BETWEEN_VALVES_show_in_use_dropdown;
							Display_Post_Command( &lde );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_DELAY_BETWEEN_VALVES_ENABLED:
							process_bool( &GuiVar_DelayBetweenValvesInUse );
							Redraw_Screen( (false) );
							break;

						case CP_DELAY_BETWEEN_VALVES_0:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_0, DELAY_BETWEEN_VALVES_MIN, DELAY_BETWEEN_VALVES_MAX, 1, (false) );
							break;

						case CP_DELAY_BETWEEN_VALVES_1:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_1, DELAY_BETWEEN_VALVES_MIN, DELAY_BETWEEN_VALVES_MAX, 1, (false) );
							break;

						case CP_DELAY_BETWEEN_VALVES_2:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_2, DELAY_BETWEEN_VALVES_MIN, DELAY_BETWEEN_VALVES_MAX, 1, (false) );
							break;

						case CP_DELAY_BETWEEN_VALVES_3:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_3, DELAY_BETWEEN_VALVES_MIN, DELAY_BETWEEN_VALVES_MAX, 1, (false) );
							break;

						case CP_DELAY_BETWEEN_VALVES_4:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_4, DELAY_BETWEEN_VALVES_MIN, DELAY_BETWEEN_VALVES_MAX, 1, (false) );
							break;

						case CP_DELAY_BETWEEN_VALVES_5:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_5, DELAY_BETWEEN_VALVES_MIN, DELAY_BETWEEN_VALVES_MAX, 1, (false) );
							break;

						case CP_DELAY_BETWEEN_VALVES_6:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_6, DELAY_BETWEEN_VALVES_MIN, DELAY_BETWEEN_VALVES_MAX, 1, (false) );
							break;

						case CP_DELAY_BETWEEN_VALVES_7:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_7, DELAY_BETWEEN_VALVES_MIN, DELAY_BETWEEN_VALVES_MAX, 1, (false) );
							break;

						case CP_DELAY_BETWEEN_VALVES_8:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_8, DELAY_BETWEEN_VALVES_MIN, DELAY_BETWEEN_VALVES_MAX, 1, (false) );
							break;

						case CP_DELAY_BETWEEN_VALVES_9:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_9, DELAY_BETWEEN_VALVES_MIN, DELAY_BETWEEN_VALVES_MAX, 1, (false) );
							break;

						default:
							bad_key_beep();
					}
					Refresh_Screen();
					break;

				case KEY_PREV:
					if( (GuiLib_ActiveCursorFieldNo >= CP_DELAY_BETWEEN_VALVES_1) && (GuiLib_ActiveCursorFieldNo <= CP_DELAY_BETWEEN_VALVES_9) )
					{
						CURSOR_Up( (true) );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case KEY_C_UP:
				case KEY_C_LEFT:
					CURSOR_Up( (true) );
					break;

				case KEY_NEXT:
					if( (GuiLib_ActiveCursorFieldNo >= CP_DELAY_BETWEEN_VALVES_0) && (GuiLib_ActiveCursorFieldNo <= CP_DELAY_BETWEEN_VALVES_8) )
					{
						CURSOR_Down( (true) );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case KEY_C_DOWN:
				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_BACK:
					GuiVar_MenuScreenToShow = CP_MAIN_MENU_POCS;

					DELAY_BETWEEN_VALVES_extract_and_store_changes_from_GuiVars();
					
					// No need to break here to allow this to fall into the default
					// condition.

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

