/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/9/2015 mpd : Required for atoi
#include	<stdlib.h>

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"device_common.h"

#include	"e_lr_programming_freewave.h"

#include	"combobox.h"

#include	"comm_mngr.h"

#include	"cursor_utils.h"

#include	"d_comm_options.h"

#include	"d_device_exchange.h"

#include	"d_process.h"

#include	"device_LR_FREEWAVE.h"

#include	"dialog.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_LR_PROGRAMMING_MODE						(0)
#define	CP_LR_PROGRAMMING_BEACON_RATE				(1)
#define	CP_LR_PROGRAMMING_FREQUENCY_WHOLE_NUM		(2)
#define	CP_LR_PROGRAMMING_FREQUENCY_DECIMAL			(3)
#define	CP_LR_PROGRAMMING_GROUP						(4)
#define	CP_LR_PROGRAMMING_XMIT_POWER				(5)
#define	CP_LR_PROGRAMMING_SLAVE_LINKS_TO			(6)
#define	CP_LR_PROGRAMMING_REPEATER_IN_USE			(7)
#define	CP_LR_PROGRAMMING_READ_DEVICE				(8)
#define	CP_LR_PROGRAMMING_PROGRAM_DEVICE			(9)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32	g_LR_PROGRAMMING_previous_cursor_pos;

// ----------

static const UNS_32 LR_mode_dropdown_lookup[ 3 ] =
{
	LR_MODE_MASTER,

	LR_MODE_SLAVE,

	LR_MODE_REPEATER
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void LR_PROGRAMMING_set_LR_mode( const UNS_32 pkey_event )
{
	switch( pkey_event )
	{
		case KEY_PLUS:
			switch( GuiVar_LRMode )
			{
				case LR_MODE_MASTER:
					GuiVar_LRMode = LR_MODE_SLAVE;
					break;

				case LR_MODE_SLAVE:
					GuiVar_LRMode = LR_MODE_REPEATER;
					break;

				case LR_MODE_REPEATER:
				default:
					GuiVar_LRMode = LR_MODE_MASTER;
					break;
			}
			break;

		case KEY_MINUS:
			switch( GuiVar_LRMode )
			{
				case LR_MODE_MASTER:
					GuiVar_LRMode = LR_MODE_REPEATER;
					break;

				case LR_MODE_SLAVE:
					GuiVar_LRMode = LR_MODE_MASTER;
					break;

				case LR_MODE_REPEATER:
				default:
					GuiVar_LRMode = LR_MODE_SLAVE;
					break;
			}
			break;
	};
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void LR_PROGRAMMING_initialize_guivars( const UNS_32 pport )
{
	// 4/20/2015 ajv : Set the device exchange result to 'read_ok' to display the initialized
	// variables.
	GuiVar_CommOptionDeviceExchangeResult = DEVICE_EXCHANGE_KEY_read_settings_completed_ok;

	// 3/3/2015 mpd : Initialize all screen variables. These will be overridden once the radio
	// values are read.
	strlcpy( GuiVar_LRSerialNumber, "--------", sizeof(GuiVar_LRSerialNumber) );

	GuiVar_LRMode = LR_MODE_MASTER;

	// 4/26/2017 ajv : TODO Modify easyGUI to use UPORT defines (1-based) instead of the 0-based
	// that Mike originally designed.
	GuiVar_LRPort = pport;

	GuiVar_LRGroup = LR_GROUP_MIN;

	GuiVar_LRBeaconRate = LR_XMIT_RATE_MIN;

	GuiVar_LRSlaveLinksToRepeater = LR_SLAVE_LINKS_TO_REPEATER_MIN;

	GuiVar_LRRepeaterInUse = LR_REPEATER_IN_USE_MIN;

	GuiVar_LRTransmitPower = LR_XMT_POWER_MIN;
}

/* ---------------------------------------------------------- */
static void FDTO_LR_PROGRAMMING_process_device_exchange_key( const UNS_32 pkeycode, const UNS_32 pport )
{
	switch( pkeycode )
	{
		case DEVICE_EXCHANGE_KEY_read_settings_completed_ok:
		case DEVICE_EXCHANGE_KEY_write_settings_completed_ok:
			// 4/14/2015 mpd : Get values from the programming struct, then redraw screen.
			LR_FREEWAVE_copy_settings_into_guivars();

			// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
			GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_completed_ok );

			// 5/18/2015 ajv : Ensure the flag that indicates a dialog box is open is set to false.
			DIALOG_close_ok_dialog();

			FDTO_LR_PROGRAMMING_lrs_draw_screen( (false), pport );
			break;

		case DEVICE_EXCHANGE_KEY_busy_try_again_later:
		case DEVICE_EXCHANGE_KEY_read_settings_error:
		case DEVICE_EXCHANGE_KEY_read_settings_in_progress:
		case DEVICE_EXCHANGE_KEY_write_settings_error:
		case DEVICE_EXCHANGE_KEY_write_settings_in_progress:
			// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
			GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( pkeycode );

			// 5/18/2015 ajv : Update the progress dialog.
			DEVICE_EXCHANGE_draw_dialog();
			break;

		default:
			// 4/14/2015 mpd : Nothing more to do.
			break;
	}
}

/* ---------------------------------------------------------- */
extern void FDTO_LR_PROGRAMMING_lrs_draw_screen( const BOOL_32 pcomplete_redraw, const UNS_32 pport )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		LR_PROGRAMMING_initialize_guivars( pport );

		// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
		GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress );

		// 4/20/2015 ajv : Default to the MODE field since that's much more likely to be changed
		// than the port.
		lcursor_to_select = CP_LR_PROGRAMMING_MODE;
	}
	else
	{
		if( GuiLib_ActiveCursorFieldNo == GuiLib_NO_CURSOR )
		{
			lcursor_to_select = g_LR_PROGRAMMING_previous_cursor_pos;
		}
		else
		{
			lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
		}
	}

	GuiLib_ShowScreen( GuiStruct_scrLRProgramming_LRS_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );

	GuiLib_Refresh();

	if( pcomplete_redraw == (true) )
	{
		// 5/18/2015 ajv : Immediately display the dialog to indicate to the user that something is
		// happening
		DEVICE_EXCHANGE_draw_dialog();

		// 3/21/2014 ajv : Start the data retrieval automatically for the user. This isn't
		// necessary, but it's a nicety, preventing the user from having to press the Read Radio
		// button.
		LR_FREEWAVE_post_device_query_to_queue( pport, LR_READ );
	}
}

/* ---------------------------------------------------------- */
extern void LR_PROGRAMMING_lrs_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		default:
			switch( pkey_event.keycode )
			{
				case DEVICE_EXCHANGE_KEY_busy_try_again_later:

				case DEVICE_EXCHANGE_KEY_read_settings_completed_ok:
				case DEVICE_EXCHANGE_KEY_read_settings_error:
				case DEVICE_EXCHANGE_KEY_read_settings_in_progress:

				case DEVICE_EXCHANGE_KEY_write_settings_completed_ok:
				case DEVICE_EXCHANGE_KEY_write_settings_error:
				case DEVICE_EXCHANGE_KEY_write_settings_in_progress:
					LR_PROGRAMMING_querying_device = (false);

					// 3/18/2014 rmd : Redraw the screen displaying the results or
					// indicating the error condition.
					lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
					lde._04_func_ptr = (void*)&FDTO_LR_PROGRAMMING_process_device_exchange_key;
					lde._06_u32_argument1 = pkey_event.keycode;
					lde._07_u32_argument2 = GuiVar_LRPort;

					Display_Post_Command( &lde );
					break;

				case KEY_SELECT:
					// 4/20/2015 ajv : If we're already in the middle of a device exchange or syncing radios,
					// ignore the key press.
					if( (GuiVar_CommOptionDeviceExchangeResult != ( e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress  ) ) ) && 
						(GuiVar_CommOptionDeviceExchangeResult != ( e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_write_settings_in_progress ) ) ) )
					{
						switch( GuiLib_ActiveCursorFieldNo )
						{
							case CP_LR_PROGRAMMING_READ_DEVICE:
								good_key_beep();

								// 4/21/2015 ajv : Store the button that was highlighted when the user pressed SELECT. Once
								// the read process is done, we'll revert back to this button.
								g_LR_PROGRAMMING_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

								LR_FREEWAVE_post_device_query_to_queue( GuiVar_LRPort, LR_READ );
								break;

							case CP_LR_PROGRAMMING_PROGRAM_DEVICE:
								good_key_beep();

								// 4/21/2015 ajv : Store the button that was highlighted when the user pressed SELECT. Once
								// the write process is done, we'll revert back to this button.
								g_LR_PROGRAMMING_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

								// 4/9/2015 mpd : Send any user changes to the programming struct.
								LR_FREEWAVE_extract_changes_from_guivars();

								LR_FREEWAVE_post_device_query_to_queue( GuiVar_LRPort, LR_WRITE );
								break;

							default:
								bad_key_beep();
						}
					}
					else
					{
						bad_key_beep();
					}
					break;

				case KEY_C_UP:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_LR_PROGRAMMING_FREQUENCY_WHOLE_NUM:
						case CP_LR_PROGRAMMING_FREQUENCY_DECIMAL:
							CURSOR_Select( CP_LR_PROGRAMMING_MODE, (true) );
							break;

						case CP_LR_PROGRAMMING_GROUP:
							CURSOR_Select( CP_LR_PROGRAMMING_FREQUENCY_WHOLE_NUM, (true) );
							break;

						case CP_LR_PROGRAMMING_PROGRAM_DEVICE:
							switch( GuiVar_LRMode )
							{
								case LR_MODE_SLAVE: 
									CURSOR_Select( CP_LR_PROGRAMMING_SLAVE_LINKS_TO, (true) );
									break;

								case LR_MODE_MASTER: 
									CURSOR_Select( CP_LR_PROGRAMMING_REPEATER_IN_USE, (true) );
									break;

								default:	// LR_MODE_REPEATER
									CURSOR_Select( CP_LR_PROGRAMMING_XMIT_POWER, (true) );
							}
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_LEFT:
					CURSOR_Up( (true) );
					break;

				case KEY_C_DOWN:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_LR_PROGRAMMING_MODE:
						case CP_LR_PROGRAMMING_BEACON_RATE: 
							CURSOR_Select( CP_LR_PROGRAMMING_FREQUENCY_WHOLE_NUM, (true) );
							break;

						case CP_LR_PROGRAMMING_FREQUENCY_WHOLE_NUM:
						case CP_LR_PROGRAMMING_FREQUENCY_DECIMAL:
							CURSOR_Select( CP_LR_PROGRAMMING_GROUP, (true) );
							break;

						case CP_LR_PROGRAMMING_XMIT_POWER: 
							switch( GuiVar_LRMode )
							{
								case LR_MODE_SLAVE: 
									CURSOR_Select( CP_LR_PROGRAMMING_SLAVE_LINKS_TO, (true) );
									break;

								case LR_MODE_MASTER: 
									CURSOR_Select( CP_LR_PROGRAMMING_REPEATER_IN_USE, (true) );
									break;

								default:	// LR_MODE_REPEATER
									CURSOR_Select( CP_LR_PROGRAMMING_READ_DEVICE, (true) );
							}
							break;

						case CP_LR_PROGRAMMING_SLAVE_LINKS_TO:
						case CP_LR_PROGRAMMING_REPEATER_IN_USE:
							CURSOR_Select( CP_LR_PROGRAMMING_READ_DEVICE, (true) );
							break;

						case CP_LR_PROGRAMMING_READ_DEVICE:
							bad_key_beep();
							break;

						default:
							CURSOR_Down( (true) );
					}
					break;

				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_PLUS:
				case KEY_MINUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_LR_PROGRAMMING_MODE:
							good_key_beep();

							LR_PROGRAMMING_set_LR_mode( pkey_event.keycode );

							Redraw_Screen( (false) );
							break;

						case CP_LR_PROGRAMMING_FREQUENCY_WHOLE_NUM:
							process_uns32( pkey_event.keycode, &GuiVar_LRFrequency_WholeNum, LR_FREQUENCY_MIN, LR_FREQUENCY_MAX, 1, (false) );

							if( GuiVar_LRFrequency_WholeNum == 470 )
							{
								GuiVar_LRFrequency_Decimal = 0;
							}
							break;

						case CP_LR_PROGRAMMING_FREQUENCY_DECIMAL:
							if( GuiVar_LRFrequency_WholeNum < 470 )
							{
								process_uns32( pkey_event.keycode, &GuiVar_LRFrequency_Decimal, LR_FREQUENCY_DECIMAL_MIN, LR_FREQUENCY_DECIMAL_MAX, 125, (false) );
							}
							else
							{
								bad_key_beep();
							}
							break;

						case CP_LR_PROGRAMMING_GROUP: 
							process_uns32( pkey_event.keycode, &GuiVar_LRGroup, LR_GROUP_MIN, LR_GROUP_MAX, 1, (false) );
							break;

						case CP_LR_PROGRAMMING_BEACON_RATE: 
							process_uns32( pkey_event.keycode, &GuiVar_LRBeaconRate, LR_XMIT_RATE_MIN, LR_XMIT_RATE_MAX, 1, (false) );
							break;

						case CP_LR_PROGRAMMING_XMIT_POWER:
							process_uns32( pkey_event.keycode, &GuiVar_LRTransmitPower, LR_XMT_POWER_MIN, LR_XMT_POWER_MAX, 1, (false) );
							break;

						case CP_LR_PROGRAMMING_SLAVE_LINKS_TO:
							process_bool( &GuiVar_LRSlaveLinksToRepeater );
							break;

						case CP_LR_PROGRAMMING_REPEATER_IN_USE:
							process_bool( &GuiVar_LRRepeaterInUse );
							break;

						default:
							bad_key_beep();
					}
					
					Refresh_Screen();
					break;

				case KEY_BACK:
					GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;

					KEY_process_global_keys( pkey_event );

					// 6/5/2015 ajv : Since the user got here from the COMM OPTIONS dialog box, redraw that
					// dialog box.
					COMM_OPTIONS_draw_dialog( (true) );
					break;

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

