/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for floorf
#include	<math.h>

#include	<string.h>

#include	"e_station_selection_grid.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"change.h"

#include	"configuration_network.h"

#include	"cal_math.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"flowsense.h"

#include	"group_base_file.h"

#include	"lcd_init.h"

#include	"m_main.h"

#include	"manual_programs.h"

#include	"range_check.h"

#include	"screen_utils.h"

#include	"scrollbox.h"

#include	"speaker.h"

#include	"stations.h"

#include	"station_groups.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

BOOL_32 g_STATION_SELECTION_GRID_user_pressed_back;

static BOOL_32 g_STATION_SELECTION_GRID_dialog_displayed;

static BOOL_32 g_STATION_SELECTION_GRID_user_toggled_display;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	STATION_SELECTION_GRID_X_COORD_OF_FIRST_STATION		(1)

#define	STATION_SELECTION_GRID_STATIONS_PER_ROW				(10)

#define STATION_SELECTION_GRID_AVAILABLE_HORIZ_PIXELS		(Pos_X_StationSelectionGridScrollBar - STATION_SELECTION_GRID_X_COORD_OF_FIRST_STATION)

#define	STATION_SELECTION_GRID_MAX_VISIBLE_ROWS				(12)

#define	STATION_SELECTION_GRID_MIN_MARKER_SIZE				(21)

#define	STATION_SELECTION_GRID_MAX_MARKER_SIZE				(175)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// How far down from the top of the screen the station selection box
	// starts. This is used when copying from the utility display buffer to the
	// primary display buffer and may change based upon how large the station
	// selection box is.
	UNS_32		pixels_from_top;

	// The row that is displayed at the top of the scroll box. This is used to
	// determine whether the user has scrolled off of the visible screen or not
	UNS_32		top_row;

	// The total number of rows that can be displayed on the screen. This is
	// used when copying from the utility display buffer to the primary display
	// buffer and may change based upon how large the station selection box is.
	UNS_32		visible_rows;

	UNS_32		number_of_stations;

	UNS_32		number_of_rows;

} CURSOR_STATS;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// A 0-based index for the cursor position.
	UNS_32		cursor_index;

	// A 0-based index for the index into the station selection array
	UNS_32		station_array_index;

	// The line this station appears on in the virtual (utility) buffer. This
	// is used to determine where the cursor needs to move to.
	UNS_32		row;

	// The column the station appears in. For example, if the line has two
	// stations, the first would be set to col 1, the next col 2.
	UNS_32		col;

	// The X-coordinate of the cursor position.
	UNS_32		X;

	// The Y-coordinate of the cursor position
	UNS_32		Y;

} CURSOR_OBJ;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// This is the part of the cursor object that is dynamically set at screen
// run-time and therefore does not need to be stored in flash as part of the
// screen definition. Saves code space. Considerable if you consider all the
// cursor positions for each screen.
//
// UPDATE: Not sure that argument makes sense when we move the constants out of
// code space.
typedef struct
{
	UNS_32		prompt_width;

	UNS_32		target_width;

	UNS_32		target_units_width;

} CURSOR_MANAGEMENT_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 10/30/2014 ajv : The box index of the controller this station is
	// connected to.
	UNS_32				box_index_0;

	// 11/12/2014 ajv : The 0-based station number for this station.
	UNS_32				station_number_0;

	// 11/12/2014 ajv : Indicates whether the station is assigned to this group
	// or not. This is used to hide stations in view mode or display them as
	// dashes (--) in edit mode if they do not belong to group. This is also
	// used to generate change lines when the user leaves the screen.
	BOOL_32				assigned_to_this_group; 

	// 11/12/2014 ajv : Indicates whether the station is to be displayed or not.
	// This is FALSE for stations that are not in use (unless viewing the
	// Stations In Use screen) and for stations which are not physically
	// connected.
	BOOL_32				station_visible;

	// 11/12/2014 ajv : A pointer to the station's description string to display
	// this station's description when it's highlighted in the grid.
	char				*description_ptr;

} CURSOR_STATION_LIST_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static CURSOR_STATS					CURSOR_statistics;

// this is a global set to the present cursor location - used primarily so we
// know how to draw the target... that is inverted or not
static CURSOR_OBJ					*CURSOR_current_cursor_ptr;

static CURSOR_OBJ					Group_CP[ MAX_STATIONS_PER_NETWORK ];

static CURSOR_MANAGEMENT_STRUCT		g_CURSOR_mgmt_add_on[ MAX_STATIONS_PER_NETWORK ];

static CURSOR_STATION_LIST_STRUCT 	station_selection_struct[ MAX_STATIONS_PER_NETWORK ];

static CURSOR_STATION_LIST_STRUCT	CURSOR_station_when_display_was_toggled;

static UNS_32						g_CURSOR_last_station_selected;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void __resize_scroll_box_marker( void )
{
	if( CURSOR_statistics.number_of_rows <= CURSOR_statistics.visible_rows )
	{
		GuiVar_StationSelectionGridMarkerSize = STATION_SELECTION_GRID_MAX_MARKER_SIZE;
	}
	else
	{
		// 7/7/2015 ajv : For each row that's not visible, we reduce the size of the marker so that
		// we can move at least one step each time we scroll down below the visible portion of the
		// screen. The equation to determine the size is effectively (175 / (rows/12)) once we
		// exceed the max number of visible rows.
		GuiVar_StationSelectionGridMarkerSize = __round_UNS16( (STATION_SELECTION_GRID_MAX_MARKER_SIZE / (CURSOR_statistics.number_of_rows / STATION_SELECTION_GRID_MAX_VISIBLE_ROWS)) );
	}
}

/* ---------------------------------------------------------- */
static void __move_scroll_box_marker( const BOOL_32 pmove_to_top )
{
	if( pmove_to_top )
	{
		GuiVar_StationSelectionGridMarkerPos = 0;
	}
	else
	{
		if( CURSOR_statistics.number_of_rows > CURSOR_statistics.visible_rows )
		{
			// 7/7/2015 ajv : For each row that's not visible, the marker size was reduced so that we
			// can move at least one step each time we scroll down below the visible portion of the
			// screen. The equation to determine the marker position is effectively ((175 * 12) / size)
			// once we exceed the max number of visible rows.
			GuiVar_StationSelectionGridMarkerPos = __round_UNS16( (((STATION_SELECTION_GRID_MAX_MARKER_SIZE * STATION_SELECTION_GRID_MAX_VISIBLE_ROWS) / GuiVar_StationSelectionGridMarkerSize)) );
		}
	}
}

/* ---------------------------------------------------------- */
/**
 * Fills the GuiVars used to display the station group and controller name at
 * the top of the screen.
 * 
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * after the user presses a key to move from one cursor position to another.
 *
 * @author 10/19/2011 AdrianusV
 *
 * @revisions
 * 	10/30/2014 ajv : Updated to include the station group name.
 */
static void __draw_group_name_and_controller_name( void )
{
	CURSOR_OBJ	*lcursor_pos_on_top_row;

	// ----------

	// 10/30/2014 ajv : Identify the cursor position at the top row by taking
	// teh current cursor position and decrementing until we reach the start of
	// the top row.
	lcursor_pos_on_top_row = CURSOR_current_cursor_ptr;

	while( lcursor_pos_on_top_row->row > CURSOR_statistics.top_row )
	{
		--lcursor_pos_on_top_row;
	}

	// ----------

	// 10/30/2014 ajv : Populate the GuiVars.

	strlcpy( GuiVar_StationSelectionGridGroupName, GuiVar_GroupName, sizeof(GuiVar_StationSelectionGridGroupName) );

	NETWORK_CONFIG_get_controller_name_str_for_ui( station_selection_struct[ lcursor_pos_on_top_row->station_array_index ].box_index_0, (char *)&GuiVar_StationSelectionGridControllerName );
}

/* ---------------------------------------------------------- */
/**
 * Copies the station description of the selected station into the GuiVar used
 * to display the station description. Note that this <b>DOES NOT</b> call
 * GuiLib_Refresh(). That's the responsibility of the calling function after all
 * of the GUI library calls have been made. This is to reduce the number of
 * calls to GuiLib_Refresh.
 * 
 * @task_execution Executed within the context of the of the DISPLAY PROCESSING
 * task after the user presses a key to move from one station to another.
 * 
 * @param pshow_description_bool TRUE if the station description should be
 * shown; otherwise, FALSE. Passing false will blank out the station description
 * field.
 * 
 * @param pindex The current index into the station_selection_struct array.
 *
 * @author 6/13/2011 AdrianusV
 *
 * @revisions (none)
 */
static void __draw_station_description_for_selected_cursor( const BOOL_32 pshow_description_bool, const UNS_32 pindex )
{
	char	str_8[ 8 ];

	INT_32	llen;

	llen = 0;

	// ----------

	if( pindex < MAX_STATIONS_PER_NETWORK )
	{
		if( pshow_description_bool == (true) )
		{
			// 10/30/2014 ajv : Make sure station description this location is
			// pointing to is valid.
			if( (char*)station_selection_struct[ pindex ].description_ptr != NULL )
			{
				llen = snprintf( GuiVar_StationDescription, NUMBER_OF_CHARS_IN_A_NAME, "%s %c %s: %s", GuiLib_GetTextPtr( GuiStruct_txtSta_0, 0 ), station_selection_struct[ pindex ].box_index_0 + 'A', STATION_get_station_number_string( station_selection_struct[ pindex ].box_index_0, station_selection_struct[ pindex ].station_number_0, (char*)&str_8, sizeof(str_8) ), (char*)station_selection_struct[ pindex ].description_ptr );
			}

			// If there is no description (i.e., a 0 length), blank out the
			// description
			if( llen == 0 )
			{
				GuiVar_StationDescription[ 0 ] = 0x00;
			}
		}
		else
		{
			// 10/30/2014 ajv : Since the caller requested the station
			// description to be suppressed, blank it out.
			GuiVar_StationDescription[ 0 ] = 0x00;
		}
	}
	else
	{
		Alert_index_out_of_range();

		GuiVar_StationDescription[ 0 ] = 0x00;
	}
}

/* ---------------------------------------------------------- */
/**
 * Draws the currently selected cursor on the station selection grid. Also calls
 * a function to draw the statino description for the currently selected
 * station.
 * 
 * @mutex_requirements Calling function does not require a specific mutex, but
 * this function does take the list_program_data_recursive_MUTEX mutex since the
 * station list is accessed.
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within teh context of the DISPLAY PROCESSING task
 * after the user presses a key to move from one station to another.
 * 
 * @param pcursor_to_draw A pointer to the cursor position to draw.
 * 
 * @author 7/18/2011 Adrianusv
 *
 * @revisions (none)
 */
static void __draw_station_number_for_current_cursor( const CURSOR_OBJ *const pcursor_to_draw )
{
	const UNS_32 OFFSET_TARGET_UNITS_FROM_END_OF_TARGET = 3;

	char	ldisplay_buf[ 256 ];

	UNS_32	ltext_color, lforeground_color, lbackground_color;

	INT_32	ltarget_x_i32, lcleanup_width_i32;

	UNS_32	lfont;

	UNS_32	lwidth_us;

	UNS_32	i;

	BOOL_32	lwrote_to_utility_buffer;

	lwrote_to_utility_buffer = (true);

	// Sanity check to make sure we're not trying write to an invalid row or
	// column
	if( (pcursor_to_draw->row > 0) && (pcursor_to_draw->col > 0) )
	{
		// "Draw" the station description now. All this does is set the GuiVar
		// used for the station description and therefore runs no risk of re-
		// entrance.
		//
		// If the CURSOR_current_cursor_ptr is at this cursor location then show
		// the real description
		if( pcursor_to_draw == CURSOR_current_cursor_ptr )
		{
			__draw_station_description_for_selected_cursor( (true), pcursor_to_draw->station_array_index );
		}
		else if( CURSOR_current_cursor_ptr == &Group_CP[ 0 ] )
		{
			__draw_station_description_for_selected_cursor( (true), 0 );
		}

		// Copy the actual station number to the display buffer
		if( station_selection_struct[ pcursor_to_draw->station_array_index ].assigned_to_this_group == (true) )
		{
			STATION_get_station_number_string( station_selection_struct[ pcursor_to_draw->station_array_index ].box_index_0, station_selection_struct[ pcursor_to_draw->station_array_index ].station_number_0, ldisplay_buf, sizeof(ldisplay_buf) );
		}
		else
		{
			strlcpy( ldisplay_buf, "---", sizeof(ldisplay_buf) );
		}

		// If the station is selected, highlight in black. Otherwise, gray it
		// out to indicate to the user it's not selected.
		lforeground_color = COLOR_BLACK;
		lfont = GuiFont_ANSI7Bold;

		// This is needed to cleanup the prior target and target units pixel
		// space if it's width has changed. It is also used to write the new
		// target and then bumped to write the target units.
		lwidth_us = GuiLib_GetTextWidth( ldisplay_buf, GuiFont_FontList[ lfont ], GuiLib_PS_NUM );

		ltarget_x_i32 = (INT_32)(pcursor_to_draw->X + g_CURSOR_mgmt_add_on[ pcursor_to_draw->cursor_index ].prompt_width);

		// If the cursor is at this target location we have to watch the width of
		// the target since the font changes from bold to not bold, resulting in the
		// target needing to be erased before writing the new target.
		if( lwidth_us != g_CURSOR_mgmt_add_on[ pcursor_to_draw->cursor_index ].target_width )
		{
			// At this point the width's used are from the LAST time we drew the
			// target
			lcleanup_width_i32 = (INT_32)(g_CURSOR_mgmt_add_on[ pcursor_to_draw->cursor_index ].target_width + 
										  OFFSET_TARGET_UNITS_FROM_END_OF_TARGET + 
										  g_CURSOR_mgmt_add_on[ pcursor_to_draw->cursor_index ].target_units_width);

			// What we are really doing here is drawing the back box - that's
			// what is important - cannot use a NULL string however as
			// GuiLib_DrawStr will do nothing - it returns immediately. If
			// what's written exceeds the bounds of the utility buffer, an error
			// is raised and the function returns false.
			lwrote_to_utility_buffer = CS_DrawStr(((pcursor_to_draw->row-1) * Pos_Y_StationSelectionGridSpacing),
								ltarget_x_i32, Pos_Y_StationSelectionGridSpacing,
								lfont, -1, " ", GuiLib_ALIGN_RIGHT, GuiLib_PS_NUM,
								GuiLib_TRANSPARENT_OFF, GuiLib_UNDERLINE_OFF,
								lcleanup_width_i32, 0, 0, GuiLib_BBP_NONE,
								COLOR_WHITE, COLOR_WHITE);
		}

		// Update this width so we know where to draw the target_units field
		g_CURSOR_mgmt_add_on[ pcursor_to_draw->cursor_index ].target_width = lwidth_us;

		// Set the foreground and background colors of the passed in cursor
		// position based upon whether it's currently selected or not.
		if( pcursor_to_draw == CURSOR_current_cursor_ptr )
		{
			ltext_color = COLOR_WHITE;
			lbackground_color = lforeground_color;
		}
		else
		{
			ltext_color = lforeground_color;
			lbackground_color = COLOR_WHITE;
		}

		// If writing to the utility buffer failed the first time, there's no
		// reason to try again.
		if( lwrote_to_utility_buffer == (true) )
		{
			lwrote_to_utility_buffer = CS_DrawStr(((pcursor_to_draw->row-1) * Pos_Y_StationSelectionGridSpacing),
								ltarget_x_i32, Pos_Y_StationSelectionGridSpacing,
								lfont, -1, ldisplay_buf, GuiLib_ALIGN_RIGHT,
								GuiLib_PS_NUM, GuiLib_TRANSPARENT_OFF,
								GuiLib_UNDERLINE_OFF, 0, 0, 0, GuiLib_BBP_NONE,
								ltext_color, lbackground_color);
		}

		g_CURSOR_mgmt_add_on[ pcursor_to_draw->cursor_index ].target_units_width = 0;

		if( (lwrote_to_utility_buffer == (true)) && (sizeof(utility_display_buf) >= sizeof(primary_display_buf)))
		{
			for( i = CURSOR_statistics.pixels_from_top; i < (Pos_Y_StationSelectionGridBottom - 1); ++i )
			{
				memcpy( primary_display_buf[ i ] + (STATION_SELECTION_GRID_X_COORD_OF_FIRST_STATION / 2), utility_display_buf[ (i-(CURSOR_statistics.pixels_from_top - 3)) + ((CURSOR_statistics.top_row - 1) * Pos_Y_StationSelectionGridSpacing) ], (STATION_SELECTION_GRID_AVAILABLE_HORIZ_PIXELS / 2) );
			}
		}
		else
		{
			CURSOR_statistics.number_of_stations = 0;
		}
	}
}

/* ---------------------------------------------------------- */
/**
 * Shows the Calsense-managed cursor on the station selection grid.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * after the user presses a key to move them from the easyGUI-managed cursors to
 * the station selection grid.
 * 
 * @param pstart_at_top_bool True if the cursor should start at the top of the
 * window; otherwise, false.
 *
 * @author 6/9/2011 Adrianusv
 *
 * @revisions (none)
 */
static void __show_cursor( const UNS_32 pstart_at_top_bool )
{
	UNS_32	cursor_index;

	UNS_32	cursor_to_draw;

	if( CURSOR_current_cursor_ptr == NULL )
	{
		if( pstart_at_top_bool == (true) )
		{
			__move_scroll_box_marker( (true) );

			cursor_index = g_CURSOR_last_station_selected;

			while( (Group_CP[ cursor_index ].row == 0) && (Group_CP[ cursor_index ].col == 0) )
			{
				++cursor_index;
			}

			CURSOR_current_cursor_ptr = &Group_CP[ cursor_index ];

			CURSOR_statistics.top_row = 1;

			cursor_to_draw = cursor_index;
		}
		else
		{
			if( (CURSOR_statistics.number_of_rows - 1) <= CURSOR_statistics.visible_rows )
			{
				CURSOR_statistics.top_row = 1;
			}
			else
			{
				CURSOR_statistics.top_row = (CURSOR_statistics.number_of_rows - CURSOR_statistics.visible_rows -1);
			}

			CURSOR_current_cursor_ptr = &Group_CP[ g_CURSOR_last_station_selected ];

			__move_scroll_box_marker( (false) );

			cursor_to_draw = g_CURSOR_last_station_selected;
		}

		__draw_group_name_and_controller_name();

		// 10/30/2014 ajv : Sanity check to ensure we're not trying to draw an
		// invalid location.
		if( (Group_CP[ cursor_to_draw ].row > 0) && (Group_CP[ cursor_to_draw ].col > 0) )
		{
			__draw_station_number_for_current_cursor( &Group_CP[ cursor_to_draw ] );
		}

		__draw_station_description_for_selected_cursor( (true), CURSOR_current_cursor_ptr->station_array_index );
	}
}

/* ---------------------------------------------------------- */
/**
 * Sets up and populates the station selection grid for a particular group.
 * Only stations that are in use and not filtered out are displayed.
 * 
 * @mutex_requirements Calling function does not require a specific mutex, but
 * this function does take the list_program_data_recursive_MUTEX mutex since the
 * station list is accessed.
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when a group screen is drawn which contains a station selection grid.
 * 
 * @param px_coordinate_start The x-coordinate of the first station to display.
 * Since we're right-aligning the station numbers, this value should usually be
 * 30.
 * 
 * @param py_coordinate_start The y-coordinate of the first station to display.
 * This value should be the exact location of the horizontal line in easyGUI.
 * 
 * @param pget_group_ID_func_ptr A pointer to the station's _get_GID_xxxxx
 * function where xxxxx represents the appropriate group structure being drawn.
 * For example, if calling this routine from the priority screen, pass in a
 * pointer to the following function:
 * 
 * <pre>nm_STATION_get_GID_irrigation_priorities</pre>
 * 
 * @param pcopy_to_display_buffer True if the contents of the utility buffer
 * should be copied to the primary display buffer; otherwise, false. So far, the
 * only time this should be false is when building the grid for the Irrigation
 * Systems screen because the station selection grid isn't shown until the user
 * selects the appropriate tab.
 * 
 * @param pcomplete_redraw True if the screen should be completely redrawn;
 * otherwise, false. If set true, all of the cursor positions are initialized.
 * 
 * @author 6/6/2011 Adrianusv
 *
 * @revisions (none)
 */
static void __FDTO_draw_all_cursor_positions( const UNS_32 px_coordinate_start,
											  UNS_32 py_coordinate_start )
{
	UNS_32	row;
	UNS_32	col;
	UNS_32	prev_controller_box_index_0;
	UNS_32	next_box_index_0;

	BOOL_32	start_of_new_controller;
	BOOL_32	lwrote_to_utility_buffer;

	UNS_32	lnumber_of_controllers;

	UNS_32	lindex_of_selected_station_when_display_was_toggled;

	UNS_32	i, j;

	char	str_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	// 11/13/2014 ajv : Always default to the first position in case there
	// are no stations selected.
	lindex_of_selected_station_when_display_was_toggled = 0;

	row = 1;
	col = 1;

	prev_controller_box_index_0 = 0;

	next_box_index_0 = 0;

	start_of_new_controller = (true);

	lwrote_to_utility_buffer = (true);

	lnumber_of_controllers = 0;


	__move_scroll_box_marker( (true) );


	g_CURSOR_last_station_selected = 0;


	memset( &Group_CP, 0x00, sizeof(Group_CP) );

	// Make room for the controller caption
	CURSOR_statistics.pixels_from_top = (py_coordinate_start - 1);
	py_coordinate_start = (UNS_32)(py_coordinate_start + 10);

	// Only restart at the top row if we're not in the grid when the grid is
	// redrawn.
	if( (g_STATION_SELECTION_GRID_dialog_displayed == (false)) && (g_STATION_SELECTION_GRID_user_toggled_display == (false)) )
	{
		CURSOR_statistics.top_row = 1;
	}

	// 11/13/2014 ajv : If the user toggles between view and edit modes, we want
	// to retain the highlighted cursor position. So, since we grabbed what
	// box index and station number when they pressed SELECT, run through the
	// updated grid and find cursor position that matches that station or, in
	// the case of switching to view mode, the closest selected station.
	if( g_STATION_SELECTION_GRID_user_toggled_display == (true) )
	{
		if( !GuiVar_StationSelectionGridShowOnlyStationsInThisGroup )
		{
			// 11/12/2014 ajv : Since we're toggling from view to edit mode,
			// find the new cursor position of the highlighted station.
			for( i = 0; i < MAX_STATIONS_PER_NETWORK; ++i )
			{
				if( (station_selection_struct[ i ].box_index_0 == CURSOR_station_when_display_was_toggled.box_index_0) &&
					(station_selection_struct[ i ].station_number_0 == CURSOR_station_when_display_was_toggled.station_number_0) )
				{
					lindex_of_selected_station_when_display_was_toggled = i;

					break;
				}
			}
		}
		else
		{
			// 11/12/2014 ajv : Sunce we're toggling from edit to view, find the
			// new cursor position of the highlighted station IF it's selected;
			// otherwise, pick the previous selected station.
			for( i = 0; i < MAX_STATIONS_PER_NETWORK; ++i )
			{
				// 11/13/2014 ajv : As we traverse through the array, set the
				// index to the next available station until we land on or pass
				// the last station that was highlighted.
				if( (station_selection_struct[ i ].box_index_0 < CURSOR_station_when_display_was_toggled.box_index_0) ||
					((station_selection_struct[ i ].box_index_0 == CURSOR_station_when_display_was_toggled.box_index_0) &&
					(station_selection_struct[ i ].station_number_0 <= CURSOR_station_when_display_was_toggled.station_number_0)) )
				{
					if( station_selection_struct[ i ].assigned_to_this_group )
					{
						lindex_of_selected_station_when_display_was_toggled = i;
					}
				}
				else
				{
					break;
				}
			}
		}
	}


	CURSOR_statistics.number_of_stations = 0;

	CURSOR_statistics.visible_rows = (UNS_32)floorf( (float)( ( Pos_Y_StationSelectionGridBottom - (CURSOR_statistics.pixels_from_top + 2) ) / Pos_Y_StationSelectionGridSpacing) );

	memset( utility_display_buf, 0xff, sizeof(utility_display_buf) );

	for( i = 0; i < MAX_STATIONS_PER_NETWORK; ++i )
	{
		if( station_selection_struct[ i ].station_visible == (true) )
		{
			if( start_of_new_controller == (true))
			{
				// ------------

				// 10/2/2012 rmd : Well we are referencing (non-atomic) elements of this structure. Such as
				// the name.
				xSemaphoreTakeRecursive( chain_members_recursive_MUTEX, portMAX_DELAY );

				// ------------
				++lnumber_of_controllers;

				if( (row == 1) && (col == 1) )
				{
					NETWORK_CONFIG_get_controller_name_str_for_ui( station_selection_struct[ i ].box_index_0, (char *)&GuiVar_StationSelectionGridControllerName );

					prev_controller_box_index_0 = station_selection_struct[ i ].box_index_0;

					// Since we updated the controller caption, redraw the
					// auto-redraw fields.
					GuiLib_Refresh();
				}
				else
				{
					NETWORK_CONFIG_get_controller_name_str_for_ui( station_selection_struct[ i ].box_index_0, (char *)&str_48 );

					lwrote_to_utility_buffer = CS_DrawStr((UNS_32)((row-1) * Pos_Y_StationSelectionGridSpacing), 0, Pos_Y_StationSelectionGridSpacing, GuiFont_ANSI7, -1, str_48, GuiLib_ALIGN_LEFT, GuiLib_PS_ON, GuiLib_TRANSPARENT_OFF, GuiLib_UNDERLINE_OFF, 300, 0, 0, GuiLib_BBP_NONE, COLOR_BLACK, COLOR_MENU_GRAY);

					++row;
				}

				// ------------

				xSemaphoreGiveRecursive( chain_members_recursive_MUTEX );

				// ------------

				start_of_new_controller = (false);

				// ------------
			}

			// Only populate the Group_CP array with stations that are in use
			// and not filtered out.
			Group_CP[ CURSOR_statistics.number_of_stations ].cursor_index = CURSOR_statistics.number_of_stations;
			Group_CP[ CURSOR_statistics.number_of_stations ].station_array_index = i;
			Group_CP[ CURSOR_statistics.number_of_stations ].row = row;
			Group_CP[ CURSOR_statistics.number_of_stations ].col = col;
			Group_CP[ CURSOR_statistics.number_of_stations ].X = (UNS_32)(px_coordinate_start + ((col - 1) * Pos_X_StationSelectionGridSpacing));
			Group_CP[ CURSOR_statistics.number_of_stations ].Y = (UNS_32)(py_coordinate_start + (row * Pos_Y_StationSelectionGridSpacing) - 1);

			// 11/13/2014 ajv : If the user toggled from view to edit mode or
			// vice versa, select the cursor position that matches the station
			// that was highlighted when they pressed SELECT.
			if( (g_STATION_SELECTION_GRID_user_toggled_display == (true)) && (i == lindex_of_selected_station_when_display_was_toggled) )
			{
				CURSOR_current_cursor_ptr = &Group_CP[ CURSOR_statistics.number_of_stations ];
			}

			++CURSOR_statistics.number_of_stations;

			// If there's still at least one more station, update the row and column
			// positions
			if( i < MAX_STATIONS_PER_NETWORK )
			{
				next_box_index_0 = 0;

				for( j = (i + 1); j < MAX_STATIONS_PER_NETWORK; ++j )
				{
					if( station_selection_struct[ j ].station_visible == (true) )
					{
						next_box_index_0 = station_selection_struct[ j ].box_index_0;
						break;
					}
				}

				if( prev_controller_box_index_0 == next_box_index_0 )
				{
					++col;

					// If we've reached the end of the row, move to the next row
					// and start over
					if( col > STATION_SELECTION_GRID_STATIONS_PER_ROW )
					{
						col = 1;
						++row;
					}
				}
				else
				{
					++row;

					col = 1;

					// reset the flag used to draw the controller serial number and
					// intialize the necessary information.
					start_of_new_controller = (true);

					prev_controller_box_index_0 = next_box_index_0;
				}
			}
			else
			{
				// We've reached the end of the array
			}
		}
	}

	CURSOR_statistics.number_of_rows = row;

	__resize_scroll_box_marker();

	if( (lwrote_to_utility_buffer == (true)) && (sizeof(utility_display_buf) >= sizeof(primary_display_buf)) )
	{
		for( i = CURSOR_statistics.pixels_from_top; i < (Pos_Y_StationSelectionGridBottom - 1); ++i )
		{
			memcpy( primary_display_buf[ i ] + (STATION_SELECTION_GRID_X_COORD_OF_FIRST_STATION / 2), utility_display_buf[ (i-(CURSOR_statistics.pixels_from_top-3)) ], (STATION_SELECTION_GRID_AVAILABLE_HORIZ_PIXELS / 2) );
		}
	}
	else
	{
		CURSOR_statistics.number_of_stations = 0;
	}

	if( (g_STATION_SELECTION_GRID_dialog_displayed == (false)) && (g_STATION_SELECTION_GRID_user_toggled_display == (false)) )
	{
		CURSOR_current_cursor_ptr = NULL;
	}

	for( i = 0; i < MAX_STATIONS_PER_NETWORK; ++i )
	{
		if( (Group_CP[ i ].row > 0) && (Group_CP[ i ].col > 0) )
		{
			__draw_station_number_for_current_cursor( &Group_CP[ i ] );
		}
	}

	__show_cursor( (false) );

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Moves the Calsense-managed cursor the requested direction based upon which
 * key the user pressed to move the cursor. If the move is successful, a good
 * key beep is played; otherwise, a bad key beep is played.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * after the user presses a key to move the Calsense-managed cursor.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @author 8/16/2012 Adrianusv
 *
 * @revisions (none)
 */
static void __FDTO_move_cursor( const UNS_32 pkeycode )
{
	INT_32		lamount_to_move;

	CURSOR_OBJ	*lcursor;

	// 12/15/2014 ajv : Take a local copy of the cursor so we can watch the cursor in the
	// debugger easier
	CURSOR_OBJ	*ltemp_cursor;

	CURSOR_OBJ	*lsaved_cursor;

	lamount_to_move = 0;

	lcursor = CURSOR_current_cursor_ptr;

	// test for NULL pointer - a NULL value for pCurCursor is an indication we
	// are on a screen that does not have cursor positions - such as a menu
	// screen
	if ( CURSOR_current_cursor_ptr != NULL )
	{
		if( pkeycode == KEY_C_UP )
		{
			if( CURSOR_current_cursor_ptr->row != 1 )
			{
				ltemp_cursor = lcursor;
	 
				// Move to the previous row
				while( ltemp_cursor->row > (lcursor->row - 1) )
				{
					ltemp_cursor -= 1;

					lamount_to_move--;
				}

				// If this row contains a cursor at the same column as the current cursor, jump there.
				// Otherwise, don't do anything since we're already at the end of the row.
				while( ltemp_cursor->col > lcursor->col )
				{
					ltemp_cursor -= 1;

					lamount_to_move--;
				}
			}
		}
		else if( pkeycode == KEY_C_DOWN )
		{
			if( CURSOR_current_cursor_ptr->row <= (CURSOR_statistics.number_of_rows - 1) )
			{
				ltemp_cursor = lcursor;

				// Move to the next row
				while( ltemp_cursor->row < (lcursor->row + 1) )
				{
					ltemp_cursor += 1;

					lamount_to_move++;
				}

				// If this row contains a cursor at the same column as the current cursor, jump there.
				// Otherwise, jump to the last cursor position in the row.
				while( (ltemp_cursor->col < lcursor->col) && ((ltemp_cursor + 1)->row == ltemp_cursor->row) )
				{
					ltemp_cursor += 1;

					lamount_to_move++;
				}
			}
		}
		else if( pkeycode == KEY_C_LEFT )
		{
			if( CURSOR_current_cursor_ptr->cursor_index != 0 )
			{
				lamount_to_move = -1;
			}
		}
		else // if( pkey_code == KEY_C_RIGHT )
		{
			if( (CURSOR_current_cursor_ptr->cursor_index != CURSOR_statistics.number_of_stations - 1) && (CURSOR_statistics.number_of_stations != 0) )
			{
				lamount_to_move = 1;
			}
		}
	
		// an amount of ZERO is our indication not to move
		if( lamount_to_move == 0 )
		{
			// If we've already moved some (1 or more positions) and we find
			// ourselves here it means the moves we made landed on to DISABLED
			// positions. If now we can't go any further we surely need to stay
			// at the original position upon function entry. Reset to make sure.
			//
			lcursor = CURSOR_current_cursor_ptr;  // set this back as our flag we ain't moving anywhere!
		}
		else
		{
			lcursor = lcursor + lamount_to_move;
		
			// sanity check ... if we are pointing to below the array OR beyond
			// the number of valid postions at this time
			if( (lcursor == NULL) || ((void*)lcursor < (void*)&Group_CP[ 0 ]) || ((void*)lcursor >= (void*)&Group_CP[ MAX_STATIONS_PER_NETWORK ]) )
			{
				// 12/10/2014 ajv : Generate a unique alert for each to deterime where the problem is
				if( lcursor == NULL )
				{
					Alert_Message( "CUR: move failed - NULL" );
				}
				else if( (void*)lcursor < (void*)&Group_CP[ 0 ] )
				{
					Alert_Message( "CUR: move failed - out-of-range @ start" );
				}
				else if( (void*)lcursor >= (void*)&Group_CP[ MAX_STATIONS_PER_NETWORK ] )
				{
					Alert_Message( "CUR: move failed - out-of-range @ end" );
				}

				lcursor = CURSOR_current_cursor_ptr;  // set this back as our flag we ain't moving anywhere!
			}
		}
	}

	// undo the attribute for where the cursor is now
	if( lcursor != CURSOR_current_cursor_ptr )
	{
		good_key_beep();

		// Get CURSOR_current_cursor_ptr off of the old target so when we redraw the
		// old target we undo the highlighting
		lsaved_cursor = CURSOR_current_cursor_ptr;
		CURSOR_current_cursor_ptr = lcursor;

		if (CURSOR_current_cursor_ptr->row > (CURSOR_statistics.top_row + CURSOR_statistics.visible_rows - 1))
		{
			// We've moved the cursor down past the visible space
			CURSOR_statistics.top_row = CURSOR_current_cursor_ptr->row - CURSOR_statistics.visible_rows + 1;
		}
		else if ( CURSOR_current_cursor_ptr->row < CURSOR_statistics.top_row )
		{
			// We've moved the cursor up past the top row
			CURSOR_statistics.top_row = CURSOR_current_cursor_ptr->row;
		}

		// Now that we know what the top row is, we need to set the controller
		// caption to match a station on the top row.
		__draw_group_name_and_controller_name();

		// Now redraw the cursor positions to remove the highlighting of the
		// previous cursor position and highlight the current cursor.
		__draw_station_number_for_current_cursor( lsaved_cursor );
		__draw_station_number_for_current_cursor( CURSOR_current_cursor_ptr );

		__move_scroll_box_marker( (false) );

		GuiLib_ShowScreen( GuiStruct_barStationSelection_16, GuiLib_ActiveCursorFieldNo, GuiLib_NO_RESET_AUTO_REDRAW );
		GuiLib_Refresh();
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/**
 * Processes the PLUS, MINUS, and SELECT keys for Calsense-managed cursor
 * positions on the station selection grid to select and deselect stations
 * within a group.
 * 
 * @mutex_requirements Calling function does not require a specific mutex, but
 * this function does take the list_program_data_recursive_MUTEX mutex since the
 * station list is accessed.
 *
 * @memory_responsibilities (none)
 *
 * @executed Executed within the context of the DISPLAY PROCESSING task after
 * the user presses the <b>PLUS</b>, <b>MINUS</b>, or <b>SELECT</b> key on the
 * station selection grid.
 * 
 * @param pkeycode The key that was pressed.
 * 
 * @author 8/15/2012 Adrianusv
 *
 * @revisions (none)
 */
static void __FDTO_process_cursor_position( const UNS_32 pkeycode )
{
	CURSOR_STATION_LIST_STRUCT *lstation;

	// Test for NULL pointer - a NULL value for GROUP_CURSOR_current_cursor_ptr
	// is an indication we are on a screen that does not have cursor positions -
	// such as a menu screen
	if( CURSOR_current_cursor_ptr != NULL )
	{
		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		lstation = &station_selection_struct[ CURSOR_current_cursor_ptr->station_array_index ];

		switch( pkeycode )
		{
			case KEY_SELECT:
				lstation->assigned_to_this_group = !lstation->assigned_to_this_group;

				if ( CURSOR_current_cursor_ptr->cursor_index != CURSOR_statistics.number_of_stations - 1 )
				{
					__FDTO_move_cursor( KEY_C_RIGHT );
				}
				else
				{
					good_key_beep();
				}
				break;

			case KEY_PLUS:
				if( lstation->assigned_to_this_group )
				{
					if ( CURSOR_current_cursor_ptr->cursor_index != CURSOR_statistics.number_of_stations - 1 )
					{
						__FDTO_move_cursor( KEY_C_RIGHT );
					}
					else
					{
						// 11/5/2014 ajv : Don't roll-over
						bad_key_beep();
					}
				}
				else
				{
					// Select the station
					lstation->assigned_to_this_group = (true);

					if ( CURSOR_current_cursor_ptr->cursor_index != CURSOR_statistics.number_of_stations - 1 )
					{
						__FDTO_move_cursor( KEY_C_RIGHT );
					}
					else
					{
						good_key_beep();
					}
				}
				break;

			case KEY_MINUS:
				// Deselect the station
				lstation->assigned_to_this_group = (false);

				if( !lstation->assigned_to_this_group )
				{
					if ( CURSOR_current_cursor_ptr->cursor_index != CURSOR_statistics.number_of_stations - 1 )
					{
						__FDTO_move_cursor( KEY_C_RIGHT );
					}
					else
					{
						// 11/5/2014 ajv : Don't roll-over
						bad_key_beep();
					}
				}
				else
				{
					if ( CURSOR_current_cursor_ptr->cursor_index != CURSOR_statistics.number_of_stations - 1 )
					{
						__FDTO_move_cursor( KEY_C_RIGHT );
					}
					else
					{
						good_key_beep();
					}
				}
				break;

			default:
				bad_key_beep();
		}

		// And now redraw the station
		__draw_station_number_for_current_cursor( CURSOR_current_cursor_ptr );

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void __FDTO_process_manual_program_cursor_position( const UNS_32 pkeycode )
{
	CURSOR_STATION_LIST_STRUCT *lstation;

	STATION_STRUCT	*lstation_struct_ptr;

	// Test for NULL pointer - a NULL value for GROUP_CURSOR_current_cursor_ptr
	// is an indication we are on a screen that does not have cursor positions -
	// such as a menu screen
	if( CURSOR_current_cursor_ptr != NULL )
	{
		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		lstation = &station_selection_struct[ CURSOR_current_cursor_ptr->station_array_index ];

		lstation_struct_ptr = nm_STATION_get_pointer_to_station( lstation->box_index_0, lstation->station_number_0 );

		switch( pkeycode )
		{
			case KEY_SELECT:
				if( lstation->assigned_to_this_group )
				{
					lstation->assigned_to_this_group = (false);

					if ( CURSOR_current_cursor_ptr->cursor_index != CURSOR_statistics.number_of_stations - 1 )
					{
						__FDTO_move_cursor( KEY_C_RIGHT );
					}
					else
					{
						good_key_beep();
					}
				}
				else
				{
					if( (nm_STATION_get_number_of_manual_programs_station_is_assigned_to(lstation_struct_ptr) >= MANUAL_PROGRAMS_NUMBER_OF_PROGRAMS_A_STATION_CAN_BE_ASSIGNED_TO) &&
						(g_GROUP_ID != STATION_get_GID_manual_program_A(lstation_struct_ptr)) &&
						(g_GROUP_ID != STATION_get_GID_manual_program_B(lstation_struct_ptr)) )
					{
						bad_key_beep();

						DIALOG_draw_ok_dialog( GuiStruct_dlgStationAlreadyIn2ManualPs_0 );

						g_STATION_SELECTION_GRID_dialog_displayed = (true);
					}
					else
					{
						// Select the station
						lstation->assigned_to_this_group = (true);

						if ( CURSOR_current_cursor_ptr->cursor_index != CURSOR_statistics.number_of_stations - 1 )
						{
							__FDTO_move_cursor( KEY_C_RIGHT );
						}
						else
						{
							good_key_beep();
						}
					}
				}
				break;

			case KEY_PLUS:
				if( lstation->assigned_to_this_group == (true) )
				{
					if ( CURSOR_current_cursor_ptr->cursor_index != CURSOR_statistics.number_of_stations - 1 )
					{
						__FDTO_move_cursor( KEY_C_RIGHT );
					}
					else
					{
						// 11/5/2014 ajv : Don't roll-over
						bad_key_beep();
					}
				}
				else if( (nm_STATION_get_number_of_manual_programs_station_is_assigned_to(lstation_struct_ptr) >= MANUAL_PROGRAMS_NUMBER_OF_PROGRAMS_A_STATION_CAN_BE_ASSIGNED_TO) &&
						 (g_GROUP_ID != STATION_get_GID_manual_program_A(lstation_struct_ptr)) &&
						 (g_GROUP_ID != STATION_get_GID_manual_program_B(lstation_struct_ptr)) )
				{
					bad_key_beep();

					DIALOG_draw_ok_dialog( GuiStruct_dlgStationAlreadyIn2ManualPs_0 );

					g_STATION_SELECTION_GRID_dialog_displayed = (true);
				}
				else
				{
					// Select the station
					lstation->assigned_to_this_group = (true);

					if ( CURSOR_current_cursor_ptr->cursor_index != CURSOR_statistics.number_of_stations - 1 )
					{
						__FDTO_move_cursor( KEY_C_RIGHT );
					}
					else
					{
						good_key_beep();
					}
				}
				break;

			case KEY_MINUS:
				if( lstation->assigned_to_this_group == (false) )
				{
					if ( CURSOR_current_cursor_ptr->cursor_index != CURSOR_statistics.number_of_stations - 1 )
					{
						__FDTO_move_cursor( KEY_C_RIGHT );
					}
					else
					{
						// 11/5/2014 ajv : Don't roll-over
						bad_key_beep();
					}
				}
				else
				{
					// Deselect the station
					lstation->assigned_to_this_group = (false);

					if ( CURSOR_current_cursor_ptr->cursor_index != CURSOR_statistics.number_of_stations - 1 )
					{
						__FDTO_move_cursor( KEY_C_RIGHT );
					}
					else
					{
						good_key_beep();
					}
				}
				break;

			default:
				bad_key_beep();
		}

		// And now redraw the station
		__draw_station_number_for_current_cursor( CURSOR_current_cursor_ptr );

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/**
 * Returns whether the passed-in cursor position is a station which is selected
 * or not. If the index is invalid, that is it's beyond the maximum station in a
 * chain, an alert is generated and false is returned.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the changes made on a group screen containing a station selection gride are
 * saved.
 * 
 * @param pindex An index into the station_selection_struct array.
 * 
 * @return BOOL_32 True if the station is selected (i.e., part of the group);
 * otherwise, false.
 *
 * @author 9/26/2011 Adrianusv
 *
 * @revisions (none)
 */
extern BOOL_32 STATION_SELECTION_GRID_station_is_selected( const UNS_32 pindex )
{
	BOOL_32	rv;

	rv = (false);

	if( pindex < MAX_STATIONS_PER_NETWORK )
	{
		rv = station_selection_struct[ pindex ].assigned_to_this_group;
	}
	else
	{
		Alert_index_out_of_range();
	}

	RangeCheck_bool( &rv, (false), NULL, "Station Selected" );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_SELECTION_GRID_get_count_of_selected_stations( void )
{
	UNS_32	rv; 

	UNS_32	i;

	rv = 0;

	for( i = 0; i < MAX_STATIONS_PER_NETWORK; ++i )
	{
		if( station_selection_struct[ i ].assigned_to_this_group )
		{
			rv++;
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Parses through the station information list and retrieves all stations 
 * assigned to the selected group ID. These stations are added to the 
 * station_selection_struct array.
 * 
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when a screen is drawn which contains a station selection grid.
 * 
 * @param pget_group_ID_A_func_ptr A pointer to the group structure's _get_GID
 * function.
 * 
 * @param pgroup_ID The group ID of the group to display.
 * 
 * @param pcomplete_redraw True if the screen should be completely redrawn;
 * otherwise, false. If set true, all of the cursor positions are initialized.
 * 
 * @param phide_stations_not_in_use True if stations not in use should be
 * hidden; otherwise, false. This should only be false when called from the
 * Stations In Use screen.
 *
 * @author 6/24/2011 Adrianusv
 *
 * @revisions (none)
 */
extern void STATION_SELECTION_GRID_populate_cursors( UNS_32 (*pget_group_ID_A_func_ptr)( void *const pstation ), UNS_32 (*pget_group_ID_B_func_ptr)( void *const pstation ), const UNS_32 pgroup_ID, const BOOL_32 pcomplete_redraw, const BOOL_32 phide_stations_not_in_use, const BOOL_32 ponly_show_stations_in_this_group )
{
	STATION_STRUCT	*lstation;

	BOOL_32	lassigned_to_this_group;

	UNS_32	i;



	// ----------

	if( pcomplete_redraw == (true) )
	{
		// At this point, we're assuming the list is empty.
		// To be safe, initialize the list to be sure it's completely clean.
		memset( station_selection_struct, 0x00, MAX_STATIONS_PER_NETWORK );
	}

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( pget_group_ID_A_func_ptr != NULL )
	{
		lstation = nm_ListGetFirst( &station_info_list_hdr );

		for( i = 0; i < nm_ListGetCount( &station_info_list_hdr ); ++i )
		{
			station_selection_struct[ i ].box_index_0 = nm_STATION_get_box_index_0( lstation );

			station_selection_struct[ i ].station_number_0 = nm_STATION_get_station_number_0( lstation );

			station_selection_struct[ i ].description_ptr = nm_STATION_get_description( lstation );

			// Determine whether the station is already in this group or not.
			if( (pget_group_ID_A_func_ptr != NULL) && ((pget_group_ID_A_func_ptr)( lstation ) == pgroup_ID) )
			{
				lassigned_to_this_group = (true);
			}
			else if( (pget_group_ID_B_func_ptr != NULL) && ((pget_group_ID_B_func_ptr)( lstation ) == pgroup_ID) )
			{
				lassigned_to_this_group = (true);
			}
			else
			{
				lassigned_to_this_group = (false);
			}

			// If it's a complete redraw, that is if the screen is being drawn
			// for the very first time, all stations that are in the group are
			// flagged as selected.
			if( pcomplete_redraw == (true) )
			{
				station_selection_struct[ i ].assigned_to_this_group = (lassigned_to_this_group && STATION_station_is_available_for_use( lstation ));
			}

			// 3/7/2013 ajv : Determine whether to show the station or not based
			// upon whether it's in use and it's card and terminal are connected
			if( ((phide_stations_not_in_use == (false)) && (nm_STATION_get_physically_available(lstation) == (true))) ||
				((phide_stations_not_in_use == (true)) && (STATION_station_is_available_for_use(lstation) == (true))) )
			{
				if( (ponly_show_stations_in_this_group == (false)) ||
					((ponly_show_stations_in_this_group == (true)) && (station_selection_struct[ i ].assigned_to_this_group == (true))) )
				{
					station_selection_struct[ i ].station_visible = (true);
				}
				else
				{
					station_selection_struct[ i ].station_visible = (false);
				}
			}
			else
			{
				station_selection_struct[ i ].station_visible = (false);
			}

			lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
		}
		
		// Force the remainder of the stations which don't exist on this chain
		// to not be shown.
		for( ; i < MAX_STATIONS_PER_NETWORK; ++i )
		{
			station_selection_struct[ i ].station_visible = (false);
		}
	}
	else
	{
		Alert_func_call_with_null_ptr();
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void FDTO_STATION_SELECTION_GRID_redraw_calling_screen( BOOL_32 *pediting_group, const UNS_32 pcursor_pos )
{
	g_STATION_SELECTION_GRID_user_pressed_back = (false);

	*pediting_group = (true);

	GuiLib_ActiveCursorFieldNo = (INT_16)pcursor_pos;

	FDTO_Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
static void draw_station_selection_grid_title( void )
{
	switch( g_MAIN_MENU_active_menu_item )
	{
		case CP_MAIN_MENU_SCHEDULED_IRRIGATION:
			switch (GuiVar_MenuScreenToShow)
			{
				case CP_SCHEDULE_MENU_STATION_GROUPS:
					snprintf( GuiVar_StationSelectionGridScreenTitle, sizeof(GuiVar_StationSelectionGridScreenTitle), "%s %s %s %s %s",
							  GuiLib_GetTextPtr( GuiStruct_txtScheduledIrrigation_0, 0 ), GuiLib_GetTextPtr( GuiStruct_symGT_0, 0 ),
							  GuiLib_GetTextPtr( GuiStruct_txtStationGroups_0, 0 ), GuiLib_GetTextPtr( GuiStruct_symGT_0, 0 ),
							  GuiVar_StationSelectionGridShowOnlyStationsInThisGroup ? GuiLib_GetTextPtr( GuiStruct_txtViewStations_0, 0 ) : GuiLib_GetTextPtr( GuiStruct_txtAddStations_0, 0 ) );
					break;

				case CP_SCHEDULE_MENU_MANUAL_PROGRAMS:
					snprintf( GuiVar_StationSelectionGridScreenTitle, sizeof(GuiVar_StationSelectionGridScreenTitle), "%s %s %s %s %s",
							  GuiLib_GetTextPtr( GuiStruct_txtScheduledIrrigation_0, 0 ), GuiLib_GetTextPtr( GuiStruct_symGT_0, 0 ),
							  GuiLib_GetTextPtr( GuiStruct_txtManualPrograms_0, 0 ), GuiLib_GetTextPtr( GuiStruct_symGT_0, 0 ),
							  GuiVar_StationSelectionGridShowOnlyStationsInThisGroup ? GuiLib_GetTextPtr( GuiStruct_txtViewStations_0, 0 ) : GuiLib_GetTextPtr( GuiStruct_txtAddStations_0, 0 ) );
					break;
			}
			break;

		case CP_MAIN_MENU_MANUAL:
			switch (GuiVar_MenuScreenToShow)
			{
				case CP_MANUAL_MENU_MANUAL_PROGRAMS:
					snprintf( GuiVar_StationSelectionGridScreenTitle, sizeof(GuiVar_StationSelectionGridScreenTitle), "%s %s %s %s %s",
							  GuiLib_GetTextPtr( GuiStruct_txtScheduledIrrigation_0, 0 ), GuiLib_GetTextPtr( GuiStruct_symGT_0, 0 ),
							  GuiLib_GetTextPtr( GuiStruct_txtManualPrograms_0, 0 ), GuiLib_GetTextPtr( GuiStruct_symGT_0, 0 ),
							  GuiVar_StationSelectionGridShowOnlyStationsInThisGroup ? GuiLib_GetTextPtr( GuiStruct_txtViewStations_0, 0 ) : GuiLib_GetTextPtr( GuiStruct_txtAddStations_0, 0 ) );
					break;
			}
			break;

		case CP_MAIN_MENU_SETUP:
			switch (GuiVar_MenuScreenToShow)
			{
				case CP_SETUP_MENU_STATIONS_IN_USE:
					snprintf( GuiVar_StationSelectionGridScreenTitle, sizeof(GuiVar_StationSelectionGridScreenTitle), "%s %s %s",
							  GuiLib_GetTextPtr( GuiStruct_txtSetup_0, 0 ), GuiLib_GetTextPtr( GuiStruct_symGT_0, 0 ),
							  GuiLib_GetTextPtr( GuiStruct_txtStationsInUse_0, 0 ) );
					break;
			}
			break;
	}

}

/* ---------------------------------------------------------- */
static void STATION_SELECTION_GRID_save_changes( void )
{
	MANUAL_PROGRAMS_GROUP_STRUCT	*lman_program;

	UNS_32	lbox_index_0;

	// ----------

	lbox_index_0 = FLOWSENSE_get_controller_index();

	// ----------

	// 11/5/2014 ajv : Save the changes that the user made
	switch( g_MAIN_MENU_active_menu_item )
	{
		case CP_MAIN_MENU_SCHEDULED_IRRIGATION:
			switch (GuiVar_MenuScreenToShow)
			{
				case CP_SCHEDULE_MENU_STATION_GROUPS:
					STATION_store_group_assignment_changes( &STATION_get_GID_station_group, &nm_STATION_set_GID_station_group, g_GROUP_ID, CHANGE_REASON_KEYPAD, lbox_index_0, CHANGE_set_change_bits, CHANGE_REASON_KEYPAD );
					break;

				case CP_SCHEDULE_MENU_MANUAL_PROGRAMS:
					lman_program = MANUAL_PROGRAMS_get_group_with_this_GID( g_GROUP_ID );

					if( lman_program != NULL )
					{
						STATION_store_manual_programs_assignment_changes( g_GROUP_ID, CHANGE_REASON_KEYPAD, lbox_index_0, CHANGE_set_change_bits, CHANGE_REASON_KEYPAD );
					}
					break;
			}
			break;

		case CP_MAIN_MENU_MANUAL:
			switch (GuiVar_MenuScreenToShow)
			{
				case CP_MANUAL_MENU_MANUAL_PROGRAMS:
					lman_program = MANUAL_PROGRAMS_get_group_with_this_GID( g_GROUP_ID );

					if( lman_program != NULL )
					{
						STATION_store_manual_programs_assignment_changes( g_GROUP_ID, CHANGE_REASON_KEYPAD, lbox_index_0, CHANGE_set_change_bits, CHANGE_REASON_KEYPAD );
					}
					break;
			}
			break;

		case CP_MAIN_MENU_SETUP:
			switch (GuiVar_MenuScreenToShow)
			{
				case CP_SETUP_MENU_STATIONS_IN_USE:
					STATION_store_stations_in_use( CHANGE_REASON_KEYPAD, lbox_index_0, CHANGE_REASON_KEYPAD );
					break;
			}
			break;
	}
}

/* ---------------------------------------------------------- */
extern void FDTO_STATION_SELECTION_GRID_draw_screen( const BOOL_32 pcomplete_redraw, const BOOL_32 ponly_show_stations_in_this_group )
{
	// 11/4/2014 ajv : Force this variable to false whenever the screen is
	// entered. This is used to notify the calling screen that the user left the
	// station selection grid so the calling screen can place the cursor on the
	// appropriate cursor position.
	g_STATION_SELECTION_GRID_user_pressed_back = (false);

	if( pcomplete_redraw == (true) )
	{
		GuiVar_StationSelectionGridShowOnlyStationsInThisGroup = ponly_show_stations_in_this_group;

		g_STATION_SELECTION_GRID_user_toggled_display = (false);

		g_STATION_SELECTION_GRID_dialog_displayed = (false);
	}

	switch( g_MAIN_MENU_active_menu_item )
	{
		case CP_MAIN_MENU_SCHEDULED_IRRIGATION:
			switch (GuiVar_MenuScreenToShow)
			{
				case CP_SCHEDULE_MENU_STATION_GROUPS:
					draw_station_selection_grid_title();

					STATION_SELECTION_GRID_populate_cursors( &STATION_get_GID_station_group, NULL, g_GROUP_ID, pcomplete_redraw, (true), GuiVar_StationSelectionGridShowOnlyStationsInThisGroup );
					break;

				case CP_SCHEDULE_MENU_MANUAL_PROGRAMS:
					draw_station_selection_grid_title();

					STATION_SELECTION_GRID_populate_cursors( &STATION_get_GID_manual_program_A, &STATION_get_GID_manual_program_B, g_GROUP_ID, pcomplete_redraw, (true), GuiVar_StationSelectionGridShowOnlyStationsInThisGroup );
					break;
			}
			break;

		case CP_MAIN_MENU_MANUAL:
			switch (GuiVar_MenuScreenToShow)
			{
				case CP_MANUAL_MENU_MANUAL_PROGRAMS:
					draw_station_selection_grid_title();

					STATION_SELECTION_GRID_populate_cursors( &STATION_get_GID_manual_program_A, &STATION_get_GID_manual_program_B, g_GROUP_ID, pcomplete_redraw, (true), GuiVar_StationSelectionGridShowOnlyStationsInThisGroup );
					break;
			}
			break;

		case CP_MAIN_MENU_SETUP:
			switch (GuiVar_MenuScreenToShow)
			{
				case CP_SETUP_MENU_STATIONS_IN_USE:
					// We use the group index to identify which group to display
					// stations for. In this case, we're not actually using a
					// group, but we're looking for any stations which have the
					// value set to true. Therefore, set the ID to true to fake
					// it.
					g_GROUP_ID = (true);

					// 11/4/2014 ajv : Since we don't want to display a group
					// name, simply NULL the first character.
					GuiVar_GroupName[ 0 ] = 0x00;

					draw_station_selection_grid_title();

					STATION_SELECTION_GRID_populate_cursors( &STATION_station_is_available_for_use, NULL, g_GROUP_ID, pcomplete_redraw, (false), (false) );
					break;
			}
			break;
	}

	// 11/12/2014 ajv : Draw the entire screen on a complete redraw AND if the
	// user closed a dialog box. Otherwise, the dialog box will remain atop the
	// screen.
	if( (pcomplete_redraw == (true)) || (g_STATION_SELECTION_GRID_dialog_displayed == (true)) )
	{
		GuiLib_ShowScreen( GuiStruct_scrViewStations_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );
	}

	__FDTO_draw_all_cursor_positions( 24, 48 );

	g_STATION_SELECTION_GRID_dialog_displayed = (false);

	g_STATION_SELECTION_GRID_user_toggled_display = (false);
}

/* ---------------------------------------------------------- */
extern void STATION_SELECTION_GRID_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	// ----------

	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
		case KEY_MINUS:
		case KEY_PLUS:
			if( GuiVar_StationSelectionGridShowOnlyStationsInThisGroup )
			{
				bad_key_beep();
			}
			else if( ((g_MAIN_MENU_active_menu_item == CP_MAIN_MENU_SCHEDULED_IRRIGATION) && (GuiVar_MenuScreenToShow == CP_SCHEDULE_MENU_MANUAL_PROGRAMS)) ||
					 ((g_MAIN_MENU_active_menu_item == CP_MAIN_MENU_MANUAL) && (GuiVar_MenuScreenToShow == CP_MANUAL_MENU_MANUAL_PROGRAMS)) )
			{
				lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
				lde._04_func_ptr = (void*)&__FDTO_process_manual_program_cursor_position;
				lde._06_u32_argument1 = pkey_event.keycode;
				Display_Post_Command( &lde );
			}
			else
			{
				lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
				lde._04_func_ptr = (void*)&__FDTO_process_cursor_position;
				lde._06_u32_argument1 = pkey_event.keycode;
				Display_Post_Command( &lde );
			}
			break;

		case KEY_C_UP:
		case KEY_C_DOWN:
		case KEY_C_LEFT:
		case KEY_C_RIGHT:
			lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
			lde._04_func_ptr = (void*)&__FDTO_move_cursor;
			lde._06_u32_argument1 = pkey_event.keycode;
			Display_Post_Command( &lde );
			break;

		case KEY_NEXT:
		case KEY_PREV:
			STATION_SELECTION_GRID_save_changes();

			switch( g_MAIN_MENU_active_menu_item )
			{
				case CP_MAIN_MENU_SCHEDULED_IRRIGATION:
					switch (GuiVar_MenuScreenToShow)
					{
						case CP_SCHEDULE_MENU_STATION_GROUPS:
							GROUP_process_NEXT_and_PREV( pkey_event.keycode, STATION_GROUP_get_num_groups_in_use(), (void*)&STATION_GROUP_extract_and_store_changes_from_GuiVars, (void*)&STATION_GROUP_get_group_at_this_index, (void*)&STATION_GROUP_copy_group_into_guivars );
							break;

						case CP_SCHEDULE_MENU_MANUAL_PROGRAMS:
							GROUP_process_NEXT_and_PREV( pkey_event.keycode, MANUAL_PROGRAMS_get_num_groups_in_use(), (void*)&MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars, (void*)&MANUAL_PROGRAMS_get_group_at_this_index, (void*)&MANUAL_PROGRAMS_copy_group_into_guivars );
							break;
					}
					break;

				case CP_MAIN_MENU_MANUAL:
					switch (GuiVar_MenuScreenToShow)
					{
						case CP_MANUAL_MENU_MANUAL_PROGRAMS:
							GROUP_process_NEXT_and_PREV( pkey_event.keycode, MANUAL_PROGRAMS_get_num_groups_in_use(), (void*)&MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars, (void*)&MANUAL_PROGRAMS_get_group_at_this_index, (void*)&MANUAL_PROGRAMS_copy_group_into_guivars );
							break;
					}
					break;

				case CP_MAIN_MENU_SETUP:
				default:
					bad_key_beep();
			}
			break;

		case KEY_BACK:
			g_STATION_SELECTION_GRID_user_pressed_back = (true);

			STATION_SELECTION_GRID_save_changes();

			// 7/8/2015 ajv : The Stations in Use screen is unique because it's accessible from the
			// Setup menu, but also from the Status screen if there are no stations in use. Therefore,
			// we need to decide where to go back to when the user presses BACK.
			if( (g_MAIN_MENU_active_menu_item == CP_MAIN_MENU_SETUP) && (GuiVar_MenuScreenToShow == CP_SETUP_MENU_STATIONS_IN_USE))
			{
				g_STATION_SELECTION_GRID_user_pressed_back = (false);

				switch( ScreenHistory[ screen_history_index ]._02_menu )
				{
					case SCREEN_MENU_MAIN:
						GuiVar_MenuScreenToShow = CP_MAIN_MENU_STATUS;
						break;

					default:
						GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;
				}
			}

			KEY_process_global_keys( pkey_event );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

