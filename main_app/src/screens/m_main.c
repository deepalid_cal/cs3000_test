/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"m_main.h"

#include	"app_startup.h"

#include	"configuration_controller.h"

#include	"contrast_and_speaker_vol.h"

#include	"cursor_utils.h"

#include	"d_comm_options.h"

#include	"d_live_screens.h"

#include	"d_process.h"

#include	"d_tech_support.h"

#include	"d_two_wire_assignment.h"

#include	"d_two_wire_debug.h"

#include	"dialog.h"

#include	"e_about.h"

#include	"e_acquire_expecteds.h"

#include	"e_alert_actions.h"

#include	"e_budgets.h"

#include	"e_budget_flow_types.h"

#include	"e_budget_reduction_limits.h"

#include	"e_budget_setup.h"

#include	"e_comm_test.h"

#include	"e_contrast_and_speaker_vol.h"

#include	"e_delay_between_valves.h"

#include	"e_flow_checking.h"

#include	"e_flowsense.h"

#include	"e_freeze_switch.h"

#include	"e_historical_et_setup.h"

#include	"e_line_fill_times.h"

#include	"e_irrigation_system.h"

#include	"e_mainline_break.h"

#include	"e_manual_programs.h"

#include	"e_manual_water.h"

#include	"e_no_water_days.h"

#include	"e_on_at_a_time.h"

#include	"e_percent_adjust.h"

#include	"e_station_groups.h"

#include	"e_poc.h"

#include	"e_lights.h"

#include	"e_moisture_sensors.h"

#include	"e_priorities.h"

#include	"e_programs.h"

#include	"e_pump.h"

#include	"e_rain_switch.h"

#include	"e_station_decoder_list.h"

#include	"e_stations.h"

#include	"e_stations_in_use.h"

#include	"e_status.h"

#include	"e_system_capacity.h"

#include	"e_test.h"

#include	"e_test_lights.h"

#include	"e_test_sequential.h"

#include	"e_time_and_date.h"

#include	"e_turn_off.h"

#include	"e_weather.h"

#include	"e_weather_sensors.h"

#include	"flash_storage.h"

#include	"flowsense.h"

#include	"irrigation_system.h"

#include	"lcd_init.h"

#include	"manual_programs.h"

#include	"moisture_sensors.h"

#include	"poc.h"

#include	"lights.h"

#include	"r_alerts.h"

#include	"r_budgets.h"

#include	"r_lights.h"

#include	"r_et_rain_table.h"

#include	"r_flow_checking.h"

#include	"r_flow_recording.h"

#include	"r_flow_table.h"

#include	"r_flowsense_stats.h"

#include	"r_holdover.h"

#include	"r_irri_details.h"

#include	"r_poc_report.h"

#include	"r_station_history.h"

#include	"r_station_report.h"

#include	"r_system_report.h"

#include	"scrollbox.h"

#include	"speaker.h"

#include	"station_groups.h"

// 4/16/2014 ajv : Required for the Two-Wire decoder types
#include	"tpmicro_comm.h"

#include	"tpmicro_data.h"

#include	"weather_control.h"

#include	"controller_initiated.h"

#include	"e_mvor.h"

#include	"e_budgets.h"

#include	"walk_thru.h"

#include	"e_walk_thru.h"

#include	"ftimes_task.h"

#include	"e_ftimes.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

INT_32 g_MAIN_MENU_active_menu_item;

// ----------

static UNS_32 g_MAIN_MENU_prev_cursor_pos__schedule;

static UNS_32 g_MAIN_MENU_prev_cursor_pos__poc_flow;

static UNS_32 g_MAIN_MENU_prev_cursor_pos__system_flow;

static UNS_32 g_MAIN_MENU_prev_cursor_pos__weather;

static UNS_32 g_MAIN_MENU_prev_cursor_pos__budgets;

static UNS_32 g_MAIN_MENU_prev_cursor_pos__lights;

static UNS_32 g_MAIN_MENU_prev_cursor_pos__manual;

static UNS_32 g_MAIN_MENU_prev_cursor_pos__no_water_days;

static UNS_32 g_MAIN_MENU_prev_cursor_pos__usage_reports;

static UNS_32 g_MAIN_MENU_prev_cursor_pos__diagnostics;

static UNS_32 g_MAIN_MENU_prev_cursor_pos__setup;

// 5/9/2018 Ryan : variable to be used in place of the cursor position 20, as that magic
// number was causing screen bugs in certain situtations.
static UNS_32 mm_top_cursor_pos_sub_menu;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_MAIN_MENU_show_screen_name_in_title_bar( void )
{
	#define	MAIN_MENU_X_COORD_TO_DISPLAY_SCREEN_NAME			(45)

	#define MAIN_MENU_X_COORD_TO_DISPLAY_SCREEN_NAME_SPANISH	(64)

	INT_32	ltext_to_draw;

	switch( g_MAIN_MENU_active_menu_item )
	{
		case CP_MAIN_MENU_STATUS: ltext_to_draw = GuiStruct_txtStatus_0; break;
		case CP_MAIN_MENU_SCHEDULED_IRRIGATION: ltext_to_draw = GuiStruct_txtScheduledIrrigation_0; break;
		case CP_MAIN_MENU_POCS: ltext_to_draw = GuiStruct_txtPOCs_0; break;
		case CP_MAIN_MENU_MAIN_LINES: ltext_to_draw = GuiStruct_txtMainlines_0; break;
		case CP_MAIN_MENU_WEATHER: ltext_to_draw = GuiStruct_txtWeather_0; break;
		case CP_MAIN_MENU_BUDGETS: ltext_to_draw = GuiStruct_txtBudgets_0; break;
		case CP_MAIN_MENU_LIGHTS: ltext_to_draw = GuiStruct_txtLights_0; break;
		case CP_MAIN_MENU_MANUAL: ltext_to_draw = GuiStruct_txtManualAndTest_0; break;
		case CP_MAIN_MENU_NO_WATER_DAYS: ltext_to_draw = GuiStruct_txtNoWaterDays_0; break;
		case CP_MAIN_MENU_REPORTS: ltext_to_draw = GuiStruct_txtReports_0; break;
		case CP_MAIN_MENU_DIAGNOSTICS: ltext_to_draw = GuiStruct_txtDiagnostics_0; break;
		case CP_MAIN_MENU_SETUP: ltext_to_draw = GuiStruct_txtSetup_0; break;
		default: ltext_to_draw = -1;
	}

	if( ltext_to_draw != -1 )
	{
		// 9/4/2018 Ryan : X coordinate in title bar different if in Spanish.
		if( GuiLib_LanguageIndex == GuiConst_LANGUAGE_ENGLISH )
		{
			FDTO_show_screen_name_in_title_bar( MAIN_MENU_X_COORD_TO_DISPLAY_SCREEN_NAME, (UNS_32)ltext_to_draw );
		}
		else
		{
			FDTO_show_screen_name_in_title_bar( MAIN_MENU_X_COORD_TO_DISPLAY_SCREEN_NAME_SPANISH, (UNS_32)ltext_to_draw );
		}
	}

	// 6/4/2015 ajv : Call GuiLib_Refresh() now to ensure the screen is updated to reflect the
	// new title.
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
static void MAIN_MENU_change_screen( const UNS_32 pmenu, const UNS_32 pstructure, void (*pdraw_func_ptr)(const BOOL_32 pcomplete_redraw), void (*pprocess_funct_ptr)(const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event), void (*populate_scroll_box_func_ptr)(const INT_16 pindex_0_i16) )
{
	DISPLAY_EVENT_STRUCT	lde;

	ScreenHistory[ screen_history_index ]._08_screen_to_draw = GuiVar_MenuScreenToShow;

	GuiVar_MenuScreenToShow = (UNS_32)GuiLib_ActiveCursorFieldNo;

	g_GROUP_list_item_index = 0;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._02_menu = pmenu;
	lde._03_structure_to_draw = pstructure;
	lde._04_func_ptr = pdraw_func_ptr;
	lde._06_u32_argument1 = (true);
	lde._08_screen_to_draw = GuiVar_MenuScreenToShow;
	lde.key_process_func_ptr = pprocess_funct_ptr;
	lde.populate_scroll_box_func_ptr = populate_scroll_box_func_ptr;

	Change_Screen( &lde );
}

/* ---------------------------------------------------------- */
static void MAIN_MENU_process_scheduled_irrigation_menu( const UNS_32 pkeycode )
{
	g_MAIN_MENU_prev_cursor_pos__schedule = (UNS_32)GuiLib_ActiveCursorFieldNo;

	switch( pkeycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_SCHEDULE_MENU_STATION_GROUPS:
					MAIN_MENU_change_screen( SCREEN_MENU_SCHEDULE,
											 GuiStruct_scrStationGroups_0,
											 (void *)&FDTO_STATION_GROUP_draw_menu,
											 STATION_GROUP_process_menu,
											 nm_STATION_GROUP_load_group_name_into_guivar );
					break;

				case CP_SCHEDULE_MENU_PROGRAMS:
					MAIN_MENU_change_screen( SCREEN_MENU_SCHEDULE,
											 GuiStruct_scrStartTimesAndWaterDays_0,
											 (void *)&FDTO_SCHEDULE_draw_menu,
											 SCHEDULE_process_menu,
											 nm_STATION_GROUP_load_group_name_into_guivar );
					break;

				case CP_SCHEDULE_MENU_STATIONS:
					MAIN_MENU_change_screen( SCREEN_MENU_SCHEDULE,
											 GuiStruct_scrStations_0,
											 (void *)&FDTO_STATION_draw_menu,
											 STATION_process_menu,
											 nm_STATION_load_station_number_into_guivar );
					break;

				case CP_SCHEDULE_MENU_PRIORITIES:
					MAIN_MENU_change_screen( SCREEN_MENU_SCHEDULE,
											 GuiStruct_scrPriorities_0,
											 (void *)&FDTO_PRIORITY_draw_screen,
											 PRIORITY_process_screen,
											 nm_STATION_GROUP_load_group_name_into_guivar );
					break;

				case CP_SCHEDULE_MENU_PERCENT_ADJUST:
					MAIN_MENU_change_screen( SCREEN_MENU_SCHEDULE,
											 GuiStruct_scrPercentAdjust_0,
											 (void *)&FDTO_PERCENT_ADJUST_draw_screen,
											 PERCENT_ADJUST_process_screen,
											 nm_STATION_GROUP_load_group_name_into_guivar );
					break;

				case CP_SCHEDULE_MENU_MANUAL_PROGRAMS:
					MAIN_MENU_change_screen( SCREEN_MENU_SCHEDULE,
											 GuiStruct_scrManualPrograms_0,
											 (void *)&FDTO_MANUAL_PROGRAMS_draw_menu,
											 MANUAL_PROGRAMS_process_menu,
											 MANUAL_PROGRAMS_load_group_name_into_guivar );
					break;
					
				case CP_SCHEDULE_MENU_FINISH_TIMES:
					// 7/30/2018 Ryan : User not allowed to enter screen before the calculation has been started
					// at least once.
					if( ftcs.mode == FTIMES_MODE_idle )
					{
						bad_key_beep();
					}
					else
					{
						MAIN_MENU_change_screen( SCREEN_MENU_SCHEDULE,
												 GuiStruct_rptFinishTimes_0,
												 (void *)&FDTO_FINISH_TIMES_draw_screen,
												 FINISH_TIMES_process_screen,
												 NULL );
					}
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_HELP:
		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void MAIN_MENU_process_poc_menu( const UNS_32 pkeycode )
{
	g_MAIN_MENU_prev_cursor_pos__poc_flow = (UNS_32)GuiLib_ActiveCursorFieldNo;

	switch( pkeycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_POC_MENU_POC:
					MAIN_MENU_change_screen( SCREEN_MENU_POCS,
											 GuiStruct_scrPOC_0,
											 (void *)&FDTO_POC_draw_menu,
											 POC_process_menu,
											 nm_POC_load_group_name_into_guivar );
					break;

				case CP_POC_MENU_PUMP_USE:
					MAIN_MENU_change_screen( SCREEN_MENU_POCS,
											 GuiStruct_scrPump_0,
											 (void *)&FDTO_PUMP_draw_screen,
											 PUMP_process_screen,
											 nm_STATION_GROUP_load_group_name_into_guivar );
					break;

				case CP_POC_MENU_LINE_FILL_TIME:
					MAIN_MENU_change_screen( SCREEN_MENU_POCS,
											 GuiStruct_scrLineFillTimes_0,
											 (void *)&FDTO_LINE_FILL_TIME_draw_screen,
											 LINE_FILL_TIME_process_screen,
											 nm_STATION_GROUP_load_group_name_into_guivar );
					break;

				case CP_POC_MENU_ACQUIRE_EXPECTEDS:
					MAIN_MENU_change_screen( SCREEN_MENU_POCS,
											 GuiStruct_scrAcquireExpecteds_0,
											 (void *)&FDTO_ACQUIRE_EXPECTEDS_draw_screen,
											 ACQUIRE_EXPECTEDS_process_screen,
											 nm_STATION_GROUP_load_group_name_into_guivar );
					break;

				case CP_POC_MENU_DELAY_BETWEEN_VALVES:
					MAIN_MENU_change_screen( SCREEN_MENU_POCS,
											 GuiStruct_scrDelayBetweenValves_0,
											 (void *)&FDTO_DELAY_BETWEEN_VALVES_draw_screen,
											 DELAY_BETWEEN_VALVES_process_screen,
											 nm_STATION_GROUP_load_group_name_into_guivar );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_HELP:
		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void MAIN_MENU_process_mainline_menu( const UNS_32 pkeycode )
{
	g_MAIN_MENU_prev_cursor_pos__system_flow = (UNS_32)GuiLib_ActiveCursorFieldNo;

	switch( pkeycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_MAINLINE_MENU_MAINLINE:
					MAIN_MENU_change_screen( SCREEN_MENU_MAINLINES,
											 GuiStruct_scrMainLines_0,
											 (void *)&FDTO_SYSTEM_draw_menu,
											 SYSTEM_process_menu,
											 SYSTEM_load_system_name_into_scroll_box_guivar );
					break;

				case CP_MAINLINE_MENU_CAPACITY:
					MAIN_MENU_change_screen( SCREEN_MENU_MAINLINES,
											 GuiStruct_scrMainlineCapacity_0,
											 (void *)&FDTO_SYSTEM_CAPACITY_draw_screen,
											 SYSTEM_CAPACITY_process_screen,
											 SYSTEM_load_system_name_into_scroll_box_guivar );
					break;

				case CP_MAINLINE_MENU_MAINLINE_BREAK:
					MAIN_MENU_change_screen( SCREEN_MENU_MAINLINES,
											 GuiStruct_scrMainlineBreak_0,
											 (void *)&FDTO_MAINLINE_BREAK_draw_screen,
											 MAINLINE_BREAK_process_screen,
											 SYSTEM_load_system_name_into_scroll_box_guivar );
					break;

				case CP_MAINLINE_MENU_ON_AT_A_TIME:
					MAIN_MENU_change_screen( SCREEN_MENU_MAINLINES,
											 GuiStruct_scrOnAtATime_0,
											 (void *)&FDTO_ON_AT_A_TIME_draw_screen,
											 ON_AT_A_TIME_process_screen,
											 nm_STATION_GROUP_load_group_name_into_guivar );
					break;

				case CP_MAINLINE_MENU_FLOW_CHECKING:
					MAIN_MENU_change_screen( SCREEN_MENU_MAINLINES,
											 GuiStruct_scrFlowChecking_0,
											 (void *)&FDTO_FLOW_CHECKING_draw_menu,
											 FLOW_CHECKING_process_menu,
											 SYSTEM_load_system_name_into_scroll_box_guivar );
					break;

				case CP_MAINLINE_MENU_ALERT_ACTIONS:
					MAIN_MENU_change_screen( SCREEN_MENU_MAINLINES,
											 GuiStruct_scrAlertActions_0,
											 (void *)&FDTO_ALERT_ACTIONS_draw_screen,
											 ALERT_ACTIONS_process_screen,
											 nm_STATION_GROUP_load_group_name_into_guivar );
					break;

				case CP_MAINLINE_MENU_MVOR:
					MAIN_MENU_change_screen( SCREEN_MENU_MAINLINES,
											 GuiStruct_scrMVOR_0,
											 (void *)&FDTO_MVOR_draw_menu,
											 MVOR_process_menu,
											 SYSTEM_load_system_name_into_scroll_box_guivar );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_HELP:
		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void MAIN_MENU_process_weather_menu( const UNS_32 pkeycode )
{
	g_MAIN_MENU_prev_cursor_pos__weather = (UNS_32)GuiLib_ActiveCursorFieldNo;

	switch( pkeycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_WEATHER_MENU_HISTORICAL_ET:
					MAIN_MENU_change_screen( SCREEN_MENU_WEATHER,
											 GuiStruct_scrHistoricalET_0,
											 (void *)&FDTO_HISTORICAL_ET_draw_screen,
											 HISTORICAL_ET_process_screen,
											 NULL );
					break;

				case CP_WEATHER_MENU_USE_OF_WEATHER:
					MAIN_MENU_change_screen( SCREEN_MENU_WEATHER,
											 GuiStruct_scrWeatherUse_0,
											 (void *)&FDTO_WEATHER_draw_screen,
											 WEATHER_process_screen,
											 nm_STATION_GROUP_load_group_name_into_guivar );
					break;

				case CP_WEATHER_MENU_WEATHER_DEVICES:
					MAIN_MENU_change_screen( SCREEN_MENU_WEATHER,
											 GuiStruct_scrWeatherSensors_0,
											 (void *)&FDTO_WEATHER_SENSORS_draw_screen,
											 WEATHER_SENSORS_process_screen,
											 NULL );
					break;

				case CP_WEATHER_MENU_RAIN_SWITCH_SETUP:
					MAIN_MENU_change_screen( SCREEN_MENU_WEATHER,
											 GuiStruct_scrRainSwitch_0,
											 (void *)&FDTO_RAIN_SWITCH_draw_screen,
											 RAIN_SWITCH_process_screen,
											 NULL );
					break;

				case CP_WEATHER_MENU_FREEZE_SWITCH_SETUP:
					MAIN_MENU_change_screen( SCREEN_MENU_WEATHER,
											 GuiStruct_scrFreezeSwitch_0,
											 (void *)&FDTO_FREEZE_SWITCH_draw_screen,
											 FREEZE_SWITCH_process_screen,
											 NULL );
					break;

				case CP_WEATHER_MENU_MOISTURE_SENSORS:
					MAIN_MENU_change_screen( SCREEN_MENU_WEATHER,
											 GuiStruct_scrMoisture_1,
											 (void *)&FDTO_MOISTURE_SENSOR_draw_menu,
											 MOISTURE_SENSOR_process_menu,
											 NULL );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_HELP:
		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void MAIN_MENU_process_budgets_menu( const UNS_32 pkeycode )
{
	g_MAIN_MENU_prev_cursor_pos__budgets = (UNS_32)GuiLib_ActiveCursorFieldNo;

	switch( pkeycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_BUDGETS_MENU_SETUP:
					MAIN_MENU_change_screen( SCREEN_MENU_BUDGETS,
											 GuiStruct_scrBudgetSetup_0,
											 (void *)&FDTO_BUDGET_SETUP_draw_menu,
											 BUDGET_SETUP_process_menu,
											 SYSTEM_load_system_name_into_scroll_box_guivar );
					break;

				case CP_BUDGETS_MENU_AMOUNTS_BY_POC:
					MAIN_MENU_change_screen( SCREEN_MENU_BUDGETS,
											 GuiStruct_scrBudgetAmountsPerPOC_0,
											 (void *)&FDTO_BUDGETS_draw_menu,
											 BUDGETS_process_menu,
											 nm_POC_load_group_name_into_guivar );
					break;

				case CP_BUDGETS_MENU_REDUCTION_LIMITS:
					MAIN_MENU_change_screen( SCREEN_MENU_BUDGETS,
											 GuiStruct_scrBudgetReductionLimits_0,
											 (void *)&FDTO_BUDGET_REDUCTION_LIMITS_draw_screen,
											 BUDGET_REDUCTION_LIMIT_process_screen,
											 nm_STATION_GROUP_load_group_name_into_guivar );
					break;

				case CP_BUDGETS_MENU_FLOW_TYPES:
					MAIN_MENU_change_screen( SCREEN_MENU_BUDGETS,
											 GuiStruct_scrBudgetFlowTypes_0,
											 (void *)&FDTO_BUDGET_FLOW_TYPES_draw_menu,
											 BUDGET_FLOW_TYPES_process_menu,
											 SYSTEM_load_system_name_into_scroll_box_guivar );
					break;

				case CP_BUDGETS_MENU_USE_VS_BUDGET:
					MAIN_MENU_change_screen( SCREEN_MENU_BUDGETS,
											 GuiStruct_rptBudgets_0,
											 (void *)&FDTO_BUDGET_REPORT_draw_report,
											 BUDGET_REPORT_process_menu,
											 NULL );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_HELP:
		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void MAIN_MENU_process_lights_menu( const UNS_32 pkeycode )
{
	g_MAIN_MENU_prev_cursor_pos__lights = (UNS_32)GuiLib_ActiveCursorFieldNo;

	switch( pkeycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_LIGHTS_MENU_SCHEDULE:
					good_key_beep();
					MAIN_MENU_change_screen( SCREEN_MENU_LIGHTS,
											 GuiStruct_scrLights_0,
											 (void *)&FDTO_LIGHTS_draw_menu,
											 LIGHTS_process_menu,
											 LIGHTS_load_group_name_into_guivar );
					break;

				case CP_LIGHTS_MENU_TURN_ON_OR_OFF:
					MAIN_MENU_change_screen( SCREEN_MENU_LIGHTS,
											 GuiStruct_scrLightsTest_0,
											 (void *)&FDTO_LIGHTS_TEST_draw_screen,
											 LIGHTS_TEST_process_screen,
											 LIGHTS_load_group_name_into_guivar );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_HELP:
		default:
			bad_key_beep();
	}

}

/* ---------------------------------------------------------- */
static void MAIN_MENU_process_manual_menu( const UNS_32 pkeycode )
{
	// 7/13/2015 ajv : When compiled for RELEASE, the production output test screen is only
	// available within the first 10-seconds of the controller powering up.
	// 
	// Note that 10 seconds is extremely generous. If you hold in the down arrow on power up,
	// and move to the Manual & Test menu item, it takes just about 2 seconds.
	#define	MANUAL_MS_TO_ALLOW_ACCESS_TO_TEST_SEQUENTIAL	( 10000 )

	BOOL_32	lallowed_to_access_test_sequential;

	lallowed_to_access_test_sequential = (false);

	g_MAIN_MENU_prev_cursor_pos__manual = (UNS_32)GuiLib_ActiveCursorFieldNo;

	switch( pkeycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_MANUAL_MENU_TEST_STATIONS:
					MAIN_MENU_change_screen( SCREEN_MENU_MANUAL,
											 GuiStruct_scrTest_0,
											 (void *)&FDTO_TEST_draw_screen,
											 TEST_process_screen,
											 NULL );
					break;

				case CP_MANUAL_MENU_MANUAL_WATERING:
					MAIN_MENU_change_screen( SCREEN_MENU_MANUAL,
											 GuiStruct_scrManualWater_0,
											 (void *)&FDTO_MANUAL_WATER_draw_screen,
											 MANUAL_WATER_process_screen,
											 NULL );
					break;

				case CP_MANUAL_MENU_MANUAL_PROGRAMS:
					MAIN_MENU_change_screen( SCREEN_MENU_MANUAL,
											 GuiStruct_scrManualPrograms_0,
											 (void *)&FDTO_MANUAL_PROGRAMS_draw_menu,
											 MANUAL_PROGRAMS_process_menu,
											 MANUAL_PROGRAMS_load_group_name_into_guivar );
					break;

				case CP_MANUAL_MENU_MVOR:
					MAIN_MENU_change_screen( SCREEN_MENU_MANUAL,
											 GuiStruct_scrMVOR_0,
											 (void *)&FDTO_MVOR_draw_menu,
											 MVOR_process_menu,
											 SYSTEM_load_system_name_into_scroll_box_guivar );
					break;

				case CP_MANUAL_MENU_LIGHTS_ON_OR_OFF:
					MAIN_MENU_change_screen( SCREEN_MENU_MANUAL,
											 GuiStruct_scrLightsTest_0,
											 (void *)&FDTO_LIGHTS_TEST_draw_screen,
											 LIGHTS_TEST_process_screen,
											 LIGHTS_load_group_name_into_guivar );
					break;

				case CP_MANUAL_MENU_WALK_THRU:
					MAIN_MENU_change_screen(	SCREEN_MENU_MANUAL,
												GuiStruct_scrWalkThru_0,
												(void *)&WALK_THRU_draw_menu,
												WALK_THRU_process_menu,
												WALK_THRU_load_group_name_into_guivar );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_HELP:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				// 6/23/2015 mpd : Allow access for the first 20 seconds.
				case CP_MANUAL_MENU_TEST_STATIONS:

					#ifdef NDEBUG

						// 7/13/2015 ajv : When compiled for RELEASE, the production output test screen is only
						// available within the first 10-seconds of the controller powering up.
						if( (my_tick_count * portTICK_RATE_MS) < MANUAL_MS_TO_ALLOW_ACCESS_TO_TEST_SEQUENTIAL )
						{
							lallowed_to_access_test_sequential = (true);
						}
						else
						{
							lallowed_to_access_test_sequential = (false);
						}

					#else

						// 7/13/2015 ajv : Always allow access to the Production Output Test in debug to not only
						// assist in debugging the screen, but also to be able to test outputs more easily.
						lallowed_to_access_test_sequential = (true);

					#endif

					if( lallowed_to_access_test_sequential )
					{
						if( !FLOWSENSE_we_are_poafs() )
						{
							MAIN_MENU_change_screen( SCREEN_MENU_MANUAL,
													 GuiStruct_scrTestSequential_0,
													 (void *)&FDTO_TEST_SEQ_draw_screen,
													 TEST_SEQ_process_screen,
													 NULL );
						}
						else
						{
							bad_key_beep();

							// 7/13/2015 ajv : TODO Uncomment the following line once the new easyGUI project is merged
							// with the main development trunk to notify the user the feature is not available if the 
							// controller is programmed to be part of a FLOWSENSE chain. This is necessary since the
							// screen bypasses normal irrigation protocols and access the irri list directly.
							#if 0
							DIALOG_draw_ok_dialog( GuiStruct_dlgTestSeqNotAvailable_0 );
							#endif
						}
					}
					else
					{
						bad_key_beep();
					}
					break;

				default:
					bad_key_beep();
			}
			break;

		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void MAIN_MENU_process_no_water_days_menu( const UNS_32 pkeycode )
{
	g_MAIN_MENU_prev_cursor_pos__no_water_days = (UNS_32)GuiLib_ActiveCursorFieldNo;

	switch( pkeycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_NOW_MENU_NO_WATER_DAYS:
					MAIN_MENU_change_screen( SCREEN_MENU_NO_WATER_DAYS,
											 GuiStruct_scrNoWaterDays_0,
											 (void *)&FDTO_NOW_draw_screen,
											 NOW_process_screen,
											 NULL );
					break;

				case CP_NOW_MENU_TURN_OFF:
					MAIN_MENU_change_screen( SCREEN_MENU_NO_WATER_DAYS,
											 GuiStruct_scrTurnOff_0,
											 (void *)&FDTO_TURN_OFF_draw_screen,
											 TURN_OFF_process_screen,
											 NULL );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_HELP:
		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void MAIN_MENU_process_reports_menu( const UNS_32 pkeycode )
{
	g_MAIN_MENU_prev_cursor_pos__usage_reports = (UNS_32)GuiLib_ActiveCursorFieldNo;

	switch( pkeycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_REPORTS_MENU_STATION_HISTORY:
					MAIN_MENU_change_screen( SCREEN_MENU_REPORTS,
											 GuiStruct_rptStationHistory_0,
											 (void *)&FDTO_STATION_HISTORY_draw_report,
											 STATION_HISTORY_process_report,
											 NULL );
					break;

				case CP_REPORTS_MENU_STATION:
					MAIN_MENU_change_screen( SCREEN_MENU_REPORTS,
											 GuiStruct_rptStationSummary_0,
											 (void *)&FDTO_STATION_REPORT_draw_report,
											 STATION_REPORT_process_report,
											 NULL );
					break;

				case CP_REPORTS_MENU_POC:
					MAIN_MENU_change_screen( SCREEN_MENU_REPORTS,
											 GuiStruct_rptPOCSummary_0,
											 (void *)&FDTO_POC_USAGE_draw_report,
											 POC_USAGE_process_report,
											 NULL );
					break;

				case CP_REPORTS_MENU_MAINLINE:
					MAIN_MENU_change_screen( SCREEN_MENU_REPORTS,
											 GuiStruct_rptSystemSummary_0,
											 (void *)&FDTO_SYSTEM_REPORT_draw_report,
											 SYSTEM_REPORT_process_report,
											 NULL );
					break;

				case CP_REPORTS_MENU_HOLDOVER:
					MAIN_MENU_change_screen( SCREEN_MENU_REPORTS,
											 GuiStruct_rptHoldOver_0,
											 (void *)&FDTO_HOLDOVER_draw_report,
											 HOLDOVER_process_report,
											 NULL );
					break;

				case CP_REPORTS_MENU_ET_AND_RAIN_TABLE:
					MAIN_MENU_change_screen( SCREEN_MENU_REPORTS,
											 GuiStruct_rptETRainTable_0,
											 (void *)&FDTO_ET_RAIN_TABLE_draw_report,
											 ET_RAIN_TABLE_process_report,
											 NULL );
					break;

				case CP_REPORTS_MENU_LIGHTS_USAGE:
					MAIN_MENU_change_screen( SCREEN_MENU_REPORTS,
											 GuiStruct_rptLightsSummary_0,
											 (void *)&FDTO_LIGHTS_REPORT_draw_report,
											 LIGHTS_REPORT_process_report,
											 LIGHTS_load_group_name_into_guivar );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_HELP:
		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void MAIN_MENU_process_diagnostics_menu( const UNS_32 pkeycode )
{
	g_MAIN_MENU_prev_cursor_pos__diagnostics = (UNS_32)GuiLib_ActiveCursorFieldNo;

	switch( pkeycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_DIAGNOSTICS_MENU_ALERTS:
				case CP_DIAGNOSTICS_MENU_CHANGE_LINES:
					if( GuiLib_ActiveCursorFieldNo == CP_DIAGNOSTICS_MENU_ALERTS )
					{
						g_ALERTS_pile_to_show = ALERTS_PILE_USER;
					}
					else
					{
						g_ALERTS_pile_to_show = ALERTS_PILE_CHANGES;
					}

					MAIN_MENU_change_screen( SCREEN_MENU_DIAGNOSTICS,
											 GuiStruct_rptAlerts_0,
											 (void *)&FDTO_ALERTS_draw_alerts,
											 ALERTS_process_report,
											 NULL );
					break;

				case CP_DIAGNOSTICS_MENU_IRRI_DETAILS:
					MAIN_MENU_change_screen( SCREEN_MENU_DIAGNOSTICS,
											 GuiStruct_rptIrriDetails_0,
											 (void *)&FDTO_IRRI_DETAILS_draw_report,
											 IRRI_DETAILS_process_report,
											 NULL );
					break;

				case CP_DIAGNOSTICS_MENU_FLOW_RECORDING:
					MAIN_MENU_change_screen( SCREEN_MENU_DIAGNOSTICS,
											 GuiStruct_rptFlowRecording_0,
											 (void *)&FDTO_FLOW_RECORDING_draw_report,
											 FLOW_RECORDING_process_report,
											 NULL );
					break;

				case CP_DIAGNOSTICS_MENU_LIVE_SCREENS:
					if( !g_LIVE_SCREENS_dialog_visible )
					{
						good_key_beep();

						LIVE_SCREENS_draw_dialog( (true) );
					}
					break;
			}
			break;

		case KEY_HELP:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TECH_SUPPORT_MENU_ENG_ALERTS:
				case CP_TECH_SUPPORT_MENU_TP_MICRO_ALERTS:
					if( GuiLib_ActiveCursorFieldNo == CP_TECH_SUPPORT_MENU_ENG_ALERTS )
					{
						g_ALERTS_pile_to_show = ALERTS_PILE_ENGINEERING;
					}
					else
					{
						g_ALERTS_pile_to_show = ALERTS_PILE_TP_MICRO;
					}

					MAIN_MENU_change_screen( SCREEN_MENU_DIAGNOSTICS,
											 GuiStruct_rptAlerts_0,
											 (void *)&FDTO_ALERTS_draw_alerts,
											 ALERTS_process_report,
											 NULL );
					break;

				// 4/27/2016 ajv : Prevent access to the Flow Checking and Flow Table screens when compiling
				// for Production.
				#ifndef NDEBUG

					case CP_TECH_SUPPORT_MENU_FLOW_CHECKING:
						MAIN_MENU_change_screen( SCREEN_MENU_DIAGNOSTICS,
												 GuiStruct_rptFlowCheckingStats_0,
												 (void *)&FDTO_FLOW_CHECKING_STATS_draw_report,
												 FLOW_CHECKING_STATS_process_report,
												 NULL );
						break;

					case CP_TECH_SUPPORT_MENU_FLOW_TABLE:
						MAIN_MENU_change_screen( SCREEN_MENU_DIAGNOSTICS,
												 GuiStruct_rptFlowTable_0,
												 (void *)&FDTO_FLOW_TABLE_draw_report,
												 FLOW_TABLE_process_report,
												 NULL );
						break;

				#endif

				default:
					bad_key_beep();
			}
			break;

		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void MAIN_MENU_process_setup_menu( const UNS_32 pkeycode )
{
	DISPLAY_EVENT_STRUCT	lde;

	g_MAIN_MENU_prev_cursor_pos__setup = (UNS_32)GuiLib_ActiveCursorFieldNo;

	switch( pkeycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_SETUP_MENU_STATIONS_IN_USE:
					MAIN_MENU_change_screen( SCREEN_MENU_SETUP,
											 GuiStruct_scrViewStations_0,
											 (void *)&FDTO_STATIONS_IN_USE_draw_screen,
											 STATIONS_IN_USE_process_screen,
											 NULL );
					break;

				case CP_SETUP_MENU_TWO_WIRE:
					if( !g_TWO_WIRE_dialog_visible )
					{
						good_key_beep();

						lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
						lde._04_func_ptr = (void*)&FDTO_TWO_WIRE_ASSIGNMENT_draw_dialog;
						lde._06_u32_argument1 = (true);
						Display_Post_Command( &lde );
					}
					break;

				case CP_SETUP_MENU_FLOWSENSE:
					MAIN_MENU_change_screen( SCREEN_MENU_SETUP,
											 GuiStruct_scrFLOWSENSE_0,
											 (void *)&FDTO_FLOWSENSE_draw_screen,
											 FLOWSENSE_process_screen,
											 NULL );
					break;

				case CP_SETUP_MENU_COMMUNICATION_OPTIONS:
					if( !g_COMM_OPTIONS_dialog_visible )
					{
						good_key_beep();

						COMM_OPTIONS_draw_dialog( (true) );
					}
					break;

				case CP_SETUP_MENU_CONTRAST:
					MAIN_MENU_change_screen( SCREEN_MENU_SETUP,
											 GuiStruct_scrContrastBacklightAndVolume_0,
											 (void *)&FDTO_LCD_draw_backlight_contrast_and_volume,
											 LCD_process_backlight_contrast_and_volume,
											 NULL );
					break;

				case CP_SETUP_MENU_DATE_TIME:
					MAIN_MENU_change_screen( SCREEN_MENU_SETUP,
											 GuiStruct_scrDateTime_0,
											 (void *)&FDTO_TIME_DATE_draw_screen,
											 TIME_DATE_process_screen,
											 NULL );
					break;

				case CP_SETUP_MENU_ABOUT:
					MAIN_MENU_change_screen( SCREEN_MENU_SETUP,
											 GuiStruct_scrAbout_0,
											 (void *)&FDTO_ABOUT_draw_screen,
											 ABOUT_process_screen,
											 NULL );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_HELP:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				// 4/27/2016 ajv : Prevent access to the FLOWSENSE stats screen when compiling for
				// Production.
				#ifndef NDEBUG

					case CP_SETUP_MENU_FLOWSENSE:
						MAIN_MENU_change_screen( SCREEN_MENU_SETUP,
												 GuiStruct_rptFLOWSENSEStats_0,
												 (void *)&FDTO_FLOWSENSE_STATS_draw_report,
												 FLOWSENSE_STATS_process_report,
												 NULL );
						break;

				#endif

				#if defined(BUILT_FOR_TWO_WIRE_DECODER_ASSEMBLY) && (BUILT_FOR_TWO_WIRE_DECODER_ASSEMBLY)

					case CP_SETUP_MENU_TWO_WIRE:
						good_key_beep();

						TWO_WIRE_DEBUG_draw_dialog( (true) );
						break;

				#endif

				case CP_SETUP_MENU_ABOUT:
					good_key_beep();

					TECH_SUPPORT_draw_dialog( (true) );
					break;

				default:
					bad_key_beep();
			}
			break;

		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void FDTO_MAIN_MENU_refresh_menu( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	lcursor_to_select = CP_MAIN_MENU_STATUS;

	switch( GuiVar_MenuScreenToShow )
	{
		case CP_MAIN_MENU_STATUS:
			FDTO_STATUS_draw_screen();
			break;

		case CP_MAIN_MENU_SCHEDULED_IRRIGATION:
			lcursor_to_select = g_MAIN_MENU_prev_cursor_pos__schedule;
			break;

		case CP_MAIN_MENU_POCS:
			lcursor_to_select = g_MAIN_MENU_prev_cursor_pos__poc_flow;
			break;

		case CP_MAIN_MENU_MAIN_LINES:
			lcursor_to_select = g_MAIN_MENU_prev_cursor_pos__system_flow;
			break;

		case CP_MAIN_MENU_WEATHER:
			lcursor_to_select = g_MAIN_MENU_prev_cursor_pos__weather;
			break;

		case CP_MAIN_MENU_BUDGETS:
			lcursor_to_select = g_MAIN_MENU_prev_cursor_pos__budgets;
			break;

		case CP_MAIN_MENU_LIGHTS:
			lcursor_to_select = g_MAIN_MENU_prev_cursor_pos__lights;
			break;

		case CP_MAIN_MENU_MANUAL:
			lcursor_to_select = g_MAIN_MENU_prev_cursor_pos__manual;
			break;

		case CP_MAIN_MENU_NO_WATER_DAYS:
			lcursor_to_select = g_MAIN_MENU_prev_cursor_pos__no_water_days;
			break;

		case CP_MAIN_MENU_REPORTS:
			lcursor_to_select = g_MAIN_MENU_prev_cursor_pos__usage_reports;
			break;

		case CP_MAIN_MENU_DIAGNOSTICS:
			lcursor_to_select = g_MAIN_MENU_prev_cursor_pos__diagnostics;
			break;

		case CP_MAIN_MENU_SETUP:
			lcursor_to_select = g_MAIN_MENU_prev_cursor_pos__setup;
			break;
	}

	if( GuiVar_MenuScreenToShow > CP_MAIN_MENU_STATUS )
	{
		if( pcomplete_redraw )
		{
			g_MAIN_MENU_active_menu_item = (INT_32)GuiVar_MenuScreenToShow;

			FDTO_Cursor_Select( lcursor_to_select, (false) );
		}
		else
		{
			if( g_MAIN_MENU_active_menu_item != CP_MAIN_MENU_NONE )
			{
				FDTO_Cursor_Select( lcursor_to_select, (false) );
			}
		}
	}

	FDTO_MAIN_MENU_show_screen_name_in_title_bar();
}

/* ---------------------------------------------------------- */
static void FDTO_MAIN_MENU_jump_to_submenu( void )
{
	UNS_32	lcursor_to_select;

	g_MAIN_MENU_active_menu_item = (INT_32)GuiVar_MenuScreenToShow;
	
	mm_top_cursor_pos_sub_menu = CP_MIN_CURSOR_INDEX_ON_A_SUB_MENU;

	FDTO_MAIN_MENU_show_screen_name_in_title_bar();

	switch( GuiVar_MenuScreenToShow )
	{
		case CP_MAIN_MENU_SCHEDULED_IRRIGATION:
			lcursor_to_select = (g_MAIN_MENU_prev_cursor_pos__schedule >= mm_top_cursor_pos_sub_menu) ? g_MAIN_MENU_prev_cursor_pos__schedule : mm_top_cursor_pos_sub_menu;
			break;

		case CP_MAIN_MENU_POCS:
			lcursor_to_select = (g_MAIN_MENU_prev_cursor_pos__poc_flow >= mm_top_cursor_pos_sub_menu) ? g_MAIN_MENU_prev_cursor_pos__poc_flow : mm_top_cursor_pos_sub_menu;
			break;

		case CP_MAIN_MENU_MAIN_LINES:
			lcursor_to_select = (g_MAIN_MENU_prev_cursor_pos__system_flow >= mm_top_cursor_pos_sub_menu) ? g_MAIN_MENU_prev_cursor_pos__system_flow : mm_top_cursor_pos_sub_menu;
			break;

		case CP_MAIN_MENU_WEATHER:
			lcursor_to_select = (g_MAIN_MENU_prev_cursor_pos__weather >= mm_top_cursor_pos_sub_menu) ? g_MAIN_MENU_prev_cursor_pos__weather : mm_top_cursor_pos_sub_menu;
			break;

		case CP_MAIN_MENU_BUDGETS:
			lcursor_to_select = (g_MAIN_MENU_prev_cursor_pos__budgets >= mm_top_cursor_pos_sub_menu) ? g_MAIN_MENU_prev_cursor_pos__budgets : mm_top_cursor_pos_sub_menu;
			break;

		case CP_MAIN_MENU_LIGHTS:
			lcursor_to_select = (g_MAIN_MENU_prev_cursor_pos__lights >= mm_top_cursor_pos_sub_menu) ? g_MAIN_MENU_prev_cursor_pos__lights : mm_top_cursor_pos_sub_menu;
			break;

		case CP_MAIN_MENU_MANUAL:
		
			// 5/9/2018 Ryan : When there are no stations available on the controller Test Stations,
			// Manual Watering, and Manual Programming are not shown on the Manual and Test menu, but
			// still hold the cursor value of 20. By changing this number, to a cursor value that is
			// visible on the screen, we avoid a bug where the cursor can become lost in the menu.
			if( GuiVar_MainMenuStationsExist == 0 )
			{
				mm_top_cursor_pos_sub_menu = CP_MIN_CURSOR_INDEX_ON_A_SUB_MENU + 3;
			}

			lcursor_to_select = (g_MAIN_MENU_prev_cursor_pos__manual >= mm_top_cursor_pos_sub_menu) ? g_MAIN_MENU_prev_cursor_pos__manual : mm_top_cursor_pos_sub_menu;
			break;

		case CP_MAIN_MENU_NO_WATER_DAYS:
			lcursor_to_select = (g_MAIN_MENU_prev_cursor_pos__no_water_days >= mm_top_cursor_pos_sub_menu) ? g_MAIN_MENU_prev_cursor_pos__no_water_days : mm_top_cursor_pos_sub_menu;
			break;

		case CP_MAIN_MENU_REPORTS:
		
			// 5/9/2018 Ryan : When there are no stations available on the controller Station History
			// and Station Summary are not shown on the Reports menu, but still hold the cursor value of
			// 20. By changing this number, to a cursor value that is visible on the screen, we avoid a
			// bug where the cursor can become lost in the menu.
			if( GuiVar_MainMenuStationsExist == 0 )
			{
				mm_top_cursor_pos_sub_menu = CP_MIN_CURSOR_INDEX_ON_A_SUB_MENU + 2;
			}

			lcursor_to_select = (g_MAIN_MENU_prev_cursor_pos__usage_reports >= mm_top_cursor_pos_sub_menu) ? g_MAIN_MENU_prev_cursor_pos__usage_reports : mm_top_cursor_pos_sub_menu;
			break;

		case CP_MAIN_MENU_DIAGNOSTICS:
			lcursor_to_select = (g_MAIN_MENU_prev_cursor_pos__diagnostics >= mm_top_cursor_pos_sub_menu) ? g_MAIN_MENU_prev_cursor_pos__diagnostics : mm_top_cursor_pos_sub_menu;
			break;

		case CP_MAIN_MENU_SETUP:
			lcursor_to_select = (g_MAIN_MENU_prev_cursor_pos__setup >= mm_top_cursor_pos_sub_menu) ? g_MAIN_MENU_prev_cursor_pos__setup : mm_top_cursor_pos_sub_menu;
			break;

		default:
			lcursor_to_select = mm_top_cursor_pos_sub_menu;
	}

	FDTO_Cursor_Select( lcursor_to_select, (true) );
}

/* ---------------------------------------------------------- */
static void MAIN_MENU_return_from_sub_menu( void )
{
	DISPLAY_EVENT_STRUCT	lde;

	GuiLib_ActiveCursorFieldNo = (INT_16)g_MAIN_MENU_active_menu_item;

	g_MAIN_MENU_active_menu_item = CP_MAIN_MENU_NONE;

	Redraw_Screen( (false) );

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void*)&FDTO_MAIN_MENU_refresh_menu;
	lde._06_u32_argument1 = (false);
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
static void MAIN_MENU_store_prev_cursor_pos( const UNS_32 lactive_cursor_pos )
{
	switch( GuiVar_MenuScreenToShow )
	{
		case CP_MAIN_MENU_SCHEDULED_IRRIGATION:
			g_MAIN_MENU_prev_cursor_pos__schedule = lactive_cursor_pos;
			break;

		case CP_MAIN_MENU_POCS:
			g_MAIN_MENU_prev_cursor_pos__poc_flow = lactive_cursor_pos;
			break;

		case CP_MAIN_MENU_MAIN_LINES:
			g_MAIN_MENU_prev_cursor_pos__system_flow = lactive_cursor_pos;
			break;

		case CP_MAIN_MENU_WEATHER:
			g_MAIN_MENU_prev_cursor_pos__weather = lactive_cursor_pos;
			break;

		case CP_MAIN_MENU_BUDGETS:
			g_MAIN_MENU_prev_cursor_pos__budgets = lactive_cursor_pos;
			break;

		case CP_MAIN_MENU_LIGHTS:
			g_MAIN_MENU_prev_cursor_pos__lights = lactive_cursor_pos;
			break;

		case CP_MAIN_MENU_MANUAL:
			g_MAIN_MENU_prev_cursor_pos__manual = lactive_cursor_pos;
			break;

		case CP_MAIN_MENU_NO_WATER_DAYS:
			g_MAIN_MENU_prev_cursor_pos__no_water_days = lactive_cursor_pos;
			break;

		case CP_MAIN_MENU_REPORTS:
			g_MAIN_MENU_prev_cursor_pos__usage_reports = lactive_cursor_pos;
			break;

		case CP_MAIN_MENU_DIAGNOSTICS:
			g_MAIN_MENU_prev_cursor_pos__diagnostics = lactive_cursor_pos;
			break;

		case CP_MAIN_MENU_SETUP:
			g_MAIN_MENU_prev_cursor_pos__setup = lactive_cursor_pos;
			break;
	}
}

/* ---------------------------------------------------------- */
extern void FDTO_MAIN_MENU_draw_menu( const BOOL_32 pcomplete_redraw )
{
	// 6/2/2016 skc : Constants for the different menu combinations.
	#define SCREEN_show_no_weather					(0)
	#define SCREEN_show_all_weather					(1)
	#define SCREEN_show_weather_only_rain_switch 	(2)

	INT_32	lcursor_to_select;

	UNS_32	lbox_index_0;

	UNS_32	i;

	WHATS_INSTALLED_STRUCT *pwi; // save typing and structure dereferencing

	// ----------
	
	// 10/25/2013 ajv : Set the GuiVars which determine which menu items to display on the Main
	// Menu.
	
	GuiVar_MainMenuStationsExist = (STATION_get_num_stations_in_use() > 0);

	GuiVar_MainMenuPOCsExist = POC_show_poc_menu_items();
	
	GuiVar_MainMenuFlowMetersExist = POC_at_least_one_POC_has_a_flow_meter();

	// 11/8/2013 ajv : To show the System Flow menu, there has to be at least one station OR
	// there must be a POC module must be installed somewhere or there must be at least 1 POC
	// decoder. Although the OR may seem odd at first, technically a controller can be used to
	// operate controllers without a flow meter or master valve. The reverse is true too - a
	// controller may operate a FM and MV without actually having any stations attached.
	GuiVar_MainMenuMainLinesAvailable = (GuiVar_MainMenuStationsExist || GuiVar_MainMenuPOCsExist);

	// 6/12/2015 ajv : The Manual & Test menu has Station- and Mainline-based (MVOR being the
	// latter) items. Therefore, if there are no stations and no Mainlines, we should hide the
	// menu altogether.
	GuiVar_MainMenuManualAvailable = (GuiVar_MainMenuStationsExist || GuiVar_MainMenuMainLinesAvailable);

	GuiVar_MainMenuBypassExists = POC_at_least_one_POC_is_a_bypass_manifold();

	GuiVar_MainMenuFlowCheckingInUse = SYSTEM_at_least_one_system_has_flow_checking();

	GuiVar_MainMenuFLOptionExists = config_c.purchased_options.option_FL;

	GuiVar_MainMenuHubOptionExists = config_c.purchased_options.option_HUB;

	// 2/24/2017 ajv : The Contrast, Backlight & Volume text changes based on the display type
	// (Kyocera or AZ Display) since the AZ Display does not support contrast adjust. Therefore,
	// preload the display type GuiVar.
	GuiVar_DisplayType = display_model_is;

	// ----------

	lbox_index_0 = FLOWSENSE_get_controller_index();

	if( chain.members[ lbox_index_0 ].saw_during_the_scan )
	{
		GuiVar_MainMenu2WireOptionsExist = chain.members[ lbox_index_0 ].box_configuration.wi.two_wire_terminal_present;
	}
	else
	{
		GuiVar_MainMenu2WireOptionsExist = (false);
	}

	// 8/28/2015 ajv : Should we suppress the Comm Options screen if there's no communication
	// options present? If we do, there's no way to get to the Serial Port Activity screen
	// anymore, which means no monitoring TP Micro traffic... so let's always enable it for now.
	//GuiVar_MainMenuCommOptionExists = ((config_c.port_A_device_index != COMM_DEVICE_NONE_PRESENT) || (config_c.port_B_device_index != COMM_DEVICE_NONE_PRESENT));
	GuiVar_MainMenuCommOptionExists = (true);

	// ----------

	// 5/26/2015 ajv : Initialize the weather GuiVars menu display to OFF. Loop
	// through chain.members to determine whether to display menus or not.
	GuiVar_MainMenuWeatherOptionsExist = SCREEN_show_no_weather;

	// 6/2/2016 skc : If ANY controller has weather card/terminal, then ALL controllers get to
	// use all the weather menu items.
	if( WEATHER_there_is_a_weather_option_in_the_chain() )
	{
		GuiVar_MainMenuWeatherOptionsExist = SCREEN_show_all_weather;
	}
	else // NONE of the controllers has weather card/terminal
	{
		// 6/3/2016 skc : If ANY controller has POC card/terminal, then use the rain switch option.
		for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
		{
			// 6/1/2015 ajv : Only count weather options for controllers that were detected in the
			// last scan.
			if( chain.members[ i ].saw_during_the_scan )
			{
				pwi = &chain.members[ i ].box_configuration.wi; // assign pointer, simplify code
				 
				// 3/14/2016 mjb : For the feature of using SW1 as a rain switch, we want to set the GUI screen
				// to our newly created GUI weather screen which shows only rain switch. This is for controllers
				// that did not purchase weather but want to have a rain switch.
				if( pwi->poc.tb_present && pwi->poc.card_present )
				{
					GuiVar_MainMenuWeatherOptionsExist = SCREEN_show_weather_only_rain_switch;

					break; // we have a hit, exit loop
				}
			}
		}	
	}

	// ----------
	 
	// 6/2/2016 skc : Moved the lights into separate loop
	GuiVar_MainMenuLightsOptionsExist = (false); // Initialize to off in case loop misses

	for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
	{
		// 6/1/2015 ajv : Only count lights options for controllers that were detected in the last
		// scan.
		if( chain.members[ i ].saw_during_the_scan )
		{
			pwi = &chain.members[ i ].box_configuration.wi; // assign pointer, simplify code

			if( (pwi->lights.card_present) && (pwi->lights.tb_present) )
			{
				GuiVar_MainMenuLightsOptionsExist = (true);

				break; // we have a hit, exit loop
			}
		}
	}

	// ----------
	
	// 10/6/2015 ajv : Only show the Real-Time Weather screen if a weather device is actually
	// present. Including a POC Terminal based SW1 rain switch.
	GuiVar_MainMenuWeatherDeviceExists =	WEATHER_get_et_gage_is_in_use() ||
											WEATHER_get_rain_bucket_is_in_use() ||
											WEATHER_get_wind_gage_in_use() ||
											WEATHER_get_freeze_switch_in_use() ||
											WEATHER_get_rain_switch_in_use() ||
											WEATHER_get_SW1_as_rain_switch_in_use();
											
	// ----------
	
	// 11/25/2013 ajv : Since we're not syncing report data across the chain yet, the Reports 
	// menu should not be visible from any controller other than the master. 

	// ----------
	
	// 4/4/2016 ajv : Only show the Moisture Sensors sub-menu item if there is at least one
	// sensor connected.
	GuiVar_MainMenuMoisSensorExists = (MOISTURE_SENSOR_get_num_of_moisture_sensors_connected() > 0);

	// 5/12/2016 skc : This EasyGui variable is tied to not showing reports on slaves, display of "Date & Time
	// on the Setup screen, and the display of budget screens/structures on slaves.
	// Controllers marked as slaves do not allow the user to manually change the date and time. Date time sent
	// from the comm server is still always distributed to the slave systems. We considered four options for
	// handling the manual changing of date time:
	//	  Master	Slave
	// 1) sync		no sync			Rejected, allows slaves to have bogus times
	// 2) no sync	no sync			Rejected, prevents manual time setting completely
	// 3) sync		not allowed		Selected
	// 4) sync		sync			Rejected, too easy to mess things up
	// We chose option #3 to implement. 
	// One consideration is the comm server is updating the time every day anyways.
	GuiVar_IsMaster = FLOWSENSE_we_are_a_master_one_way_or_another();
	
	// 01/27/2016 skc : Don't show budget reports if budgets is completely turned off
	// 02/02/2016 skc : If we are outside a budget period, don't show the menu either
	GuiVar_MainMenuBudgetsInUse = SYSTEM_at_least_one_system_has_budget_enabled();

	// 10/14/2018 ajv : Modify what is shown if the controller is in AQUAPONICS mode
	GuiVar_MainMenuAquaponicsMode = config_c.purchased_options.option_AQUAPONICS;

	// 4/22/2016 skc : At least one system must be in automatic reduction mode for all the
	// budget menus to display.
	for( i=0; i<SYSTEM_num_systems_in_use(); i++ )
	{
		BUDGET_DETAILS_STRUCT bds;
		SYSTEM_get_budget_details( SYSTEM_get_group_at_this_index(i), &bds );
		// Anything besides alert only is good enough
		if( bds.mode != IRRIGATION_SYSTEM_BUDGET_MODE_ALERT_ONLY )
		{
			GuiVar_BudgetModeIdx = bds.mode; // enable display of menu item(s)
			break; // only need one hit, done with the loop
		}
	}

	// ----------

	// 5/26/2015 ajv : Draw the actual screen now.

	if( pcomplete_redraw )
	{
		g_MAIN_MENU_active_menu_item = CP_MAIN_MENU_NONE;

		if( GuiVar_StatusShowLiveScreens )
		{
			lcursor_to_select = (INT_32)g_STATUS_last_cursor_position;
		}
		else
		{
			lcursor_to_select = (INT_32)GuiVar_MenuScreenToShow;
		}
	}
	else
	{
		lcursor_to_select = (INT_32)GuiLib_ActiveCursorFieldNo;

		// 6/26/2015 ajv : Make sure we store which cursor we were on when the redraw occurred so
		// the static globals are valid in the refresh_menu routine called below.
		if( (lcursor_to_select >= CP_MIN_CURSOR_INDEX_ON_A_SUB_MENU) && (lcursor_to_select <= CP_MAX_CURSOR_INDEX_ON_A_SUB_MENU) )
		{
			MAIN_MENU_store_prev_cursor_pos( lcursor_to_select );
		}
	}
	 
	GuiLib_ShowScreen( GuiStruct_scrMainMenu_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );

	FDTO_MAIN_MENU_refresh_menu( pcomplete_redraw );
}

/* ---------------------------------------------------------- */
static void FDTO_process_up_and_down_keys( const UNS_32 pkeycode )
{
	if( (pkeycode == KEY_C_DOWN) && (GuiLib_ActiveCursorFieldNo == CP_MAIN_MENU_SETUP) )
	{
		// 5/26/2015 ajv : Don't allow the user to scroll past SETUP
		bad_key_beep();
	}
	else if( (pkeycode == KEY_C_UP) && (GuiLib_ActiveCursorFieldNo == mm_top_cursor_pos_sub_menu) )
	{
		// 5/9/2018 Ryan : top user cursor field can be greater than 20, so this if statement was
		// changes to a use a variable instead.
		
		// 5/26/2015 ajv : Don't allow the user to navigate up from the the top cursor field in the
		// sub menu.
		bad_key_beep();
	}
	else if( ((pkeycode == KEY_C_UP) && (FDTO_Cursor_Up((true))) ) || 
			 ((pkeycode == KEY_C_DOWN) && (FDTO_Cursor_Down((true)))) )
	{
		if( GuiLib_ActiveCursorFieldNo <= CP_MAIN_MENU_SETUP )
		{
			GuiVar_MenuScreenToShow = (UNS_32)GuiLib_ActiveCursorFieldNo;

			FDTO_Redraw_Screen( (false) );

			FDTO_MAIN_MENU_refresh_menu( (false) );
		}
	}
}

/* ---------------------------------------------------------- */
extern void MAIN_MENU_process_menu( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	if( GuiVar_StatusShowLiveScreens )
	{
		STATUS_process_screen( pkey_event );
	}
	else if( g_COMM_OPTIONS_dialog_visible )
	{
		COMM_OPTIONS_process_dialog( pkey_event );
	}
	else if( g_LIVE_SCREENS_dialog_visible )
	{
		LIVE_SCREENS_process_dialog( pkey_event );
	}
	else if( g_TECH_SUPPORT_dialog_visible )
	{
		TECH_SUPPORT_process_dialog( pkey_event );
	}
	else if( g_TWO_WIRE_dialog_visible )
	{
		TWO_WIRE_ASSIGNMENT_process_dialog( pkey_event );
	}
	else if( g_TWO_WIRE_DEBUG_dialog_visible )
	{
		TWO_WIRE_DEBUG_process_dialog( pkey_event );
	}
	else
	{
		switch( pkey_event.keycode )
		{
			case KEY_PLUS:
			case KEY_MINUS:
				if( g_MAIN_MENU_active_menu_item == CP_MAIN_MENU_NONE )
				{
					// 2/15/2017 rmd : This used to be a 'secret' contrast adjust means. Was carried over from
					// the 2000's. On those units the display could become unreadable if the
					// temperature/contrast was far enough out. So this allowed you to adjust when at a 'known'
					// screen without seeing the screen. AFIK all that's gone with the 3000.
					//
					// BUT I'm leaving the case statements here as an easy way for a developer to add some
					// temporary key action here:

				}

				// 2/15/2017 rmd : Should always produce a bad key beep for production code.
				bad_key_beep();

				break;

			case KEY_C_UP:
			case KEY_C_DOWN:
				lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
				lde._04_func_ptr = (void *)&FDTO_process_up_and_down_keys;
				lde._06_u32_argument1 = pkey_event.keycode;
				Display_Post_Command( &lde );
				break;

			case KEY_C_LEFT:
				if( GuiLib_ActiveCursorFieldNo >= 20 )
				{
					good_key_beep();

					MAIN_MENU_store_prev_cursor_pos( (UNS_32)GuiLib_ActiveCursorFieldNo );

					MAIN_MENU_return_from_sub_menu();
				}
				else
				{
					bad_key_beep();
				}
				break;

			case KEY_HELP:
			case KEY_SELECT:
			case KEY_C_RIGHT:
				if( (g_MAIN_MENU_active_menu_item == CP_MAIN_MENU_NONE) && (pkey_event.keycode != KEY_HELP) )
				{
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_MAIN_MENU_STATUS:
							if( !GuiVar_StatusShowLiveScreens )
							{
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = &FDTO_MAIN_MENU_show_screen_name_in_title_bar;
								Display_Post_Command( &lde );
							}

							STATUS_process_screen( pkey_event );
							break;

						case CP_MAIN_MENU_SCHEDULED_IRRIGATION:
						case CP_MAIN_MENU_POCS:
						case CP_MAIN_MENU_MAIN_LINES:
						case CP_MAIN_MENU_WEATHER:
						case CP_MAIN_MENU_BUDGETS:
						case CP_MAIN_MENU_LIGHTS:
						case CP_MAIN_MENU_MANUAL:
						case CP_MAIN_MENU_NO_WATER_DAYS:
						case CP_MAIN_MENU_REPORTS:
						case CP_MAIN_MENU_DIAGNOSTICS:
						case CP_MAIN_MENU_SETUP:
							if( g_MAIN_MENU_active_menu_item == CP_MAIN_MENU_NONE )
							{
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = (void *)&FDTO_MAIN_MENU_jump_to_submenu;
								Display_Post_Command( &lde );
							}
							else
							{
								bad_key_beep();
							}
							break;

						default:
							bad_key_beep();
					}
				}
				else
				{
					if( pkey_event.keycode == KEY_C_RIGHT )
					{
						bad_key_beep();
					}
					else
					{
						switch( g_MAIN_MENU_active_menu_item )
						{
							case CP_MAIN_MENU_SCHEDULED_IRRIGATION:
								MAIN_MENU_process_scheduled_irrigation_menu( pkey_event.keycode );
								break;

							case CP_MAIN_MENU_POCS:
								MAIN_MENU_process_poc_menu( pkey_event.keycode );
								break;

							case CP_MAIN_MENU_MAIN_LINES:
								MAIN_MENU_process_mainline_menu( pkey_event.keycode );
								break;

							case CP_MAIN_MENU_WEATHER:
								MAIN_MENU_process_weather_menu( pkey_event.keycode );
								break;

							case CP_MAIN_MENU_BUDGETS:
								MAIN_MENU_process_budgets_menu( pkey_event.keycode );
								break;

							case CP_MAIN_MENU_LIGHTS:
								MAIN_MENU_process_lights_menu( pkey_event.keycode );
								break;

							case CP_MAIN_MENU_MANUAL:
								MAIN_MENU_process_manual_menu( pkey_event.keycode );
								break;

							case CP_MAIN_MENU_NO_WATER_DAYS:
								MAIN_MENU_process_no_water_days_menu( pkey_event.keycode );
								break;

							case CP_MAIN_MENU_REPORTS:
								MAIN_MENU_process_reports_menu( pkey_event.keycode );
								break;

							case CP_MAIN_MENU_DIAGNOSTICS:
								MAIN_MENU_process_diagnostics_menu( pkey_event.keycode );
								break;
						
							case CP_MAIN_MENU_SETUP:
								MAIN_MENU_process_setup_menu( pkey_event.keycode );
								break;

							default:
								bad_key_beep();
						}
					}
				}
				break;

			case KEY_BACK:
				if( GuiLib_ActiveCursorFieldNo > CP_MAIN_MENU_SETUP )
				{
					good_key_beep();

					MAIN_MENU_store_prev_cursor_pos( (UNS_32)GuiLib_ActiveCursorFieldNo );

					MAIN_MENU_return_from_sub_menu();
				}
				else if( GuiVar_MenuScreenToShow != CP_MAIN_MENU_STATUS )
				{
					GuiVar_MenuScreenToShow = CP_MAIN_MENU_STATUS;

					good_key_beep();
					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._04_func_ptr = (void *)&FDTO_MAIN_MENU_draw_menu;
					lde._06_u32_argument1 = (true);
					Display_Post_Command( &lde );
				}
				else
				{
					KEY_process_global_keys( pkey_event );
				}
				break;

			default:
				KEY_process_global_keys( pkey_event );
		}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

