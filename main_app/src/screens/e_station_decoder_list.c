/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memset
#include	<string.h>

#include	"e_station_decoder_list.h"

#include	"app_startup.h"

#include	"change.h"

#include	"configuration_controller.h"

#include	"cursor_utils.h"

#include	"d_two_wire_assignment.h"

#include	"group_base_file.h"

#include	"m_main.h"

#include	"screen_utils.h"

#include	"scrollbox.h"

#include	"speaker.h"

#include	"stations.h"

#include	"tpmicro_data.h"

#include	"two_wire_utils.h"

#include	"flowsense.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_TWO_WIRE_STATION_NUMBER_A		(0)
#define	CP_TWO_WIRE_TURN_ON_OUTPUT_A		(1)
#define	CP_TWO_WIRE_STATION_NUMBER_B		(2)
#define	CP_TWO_WIRE_TURN_ON_OUTPUT_B		(3)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static BOOL_32	g_TWO_WIRE_STA_editing_decoder;

static UNS_32	g_TWO_WIRE_STA_previously_selected_station_A;

static UNS_32	g_TWO_WIRE_STA_previously_selected_station_B;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void FDTO_TWO_WIRE_STA_return_to_menu( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param pkey_code 
 * @param pstation_guivar_ptr 
 * @param pstation_is_set_guivar_ptr 
 * @param pstation_desc_guivar_ptr 
 * @param ppreviously_selected_station 
 * @param pstation_number_of_other_output 
 * @param ppreviously_selected_station_of_other_output 
 *
 * @author 10/17/2013 AdrianusV
 *
 * @revisions (none)
 */
static void TWO_WIRE_STA_process_station_number( const UNS_32 pkey_code, UNS_32 *pstation_guivar_ptr, char *pstation_str_guivar_ptr, UNS_32 *pstation_is_set_guivar_ptr, char *pstation_desc_guivar_ptr, const UNS_32 ppreviously_selected_station, const UNS_32 pstation_number_of_other_output, const UNS_32 ppreviously_selected_station_of_other_output )
{
	STATION_STRUCT	*lstation;

	UNS_32	lprev_station_num;

	BOOL_32	lstation_available;

	good_key_beep();

	*pstation_is_set_guivar_ptr = (true);

	lprev_station_num = *pstation_guivar_ptr;

	lstation_available = (false);

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	do
	{
		if( pkey_code == KEY_PLUS )
		{
			if( *pstation_guivar_ptr == 0 )
			{
				*pstation_guivar_ptr = MAX_CONVENTIONAL_STATIONS;
			}
			else
			{
				(*pstation_guivar_ptr)++;
			}

			if( *pstation_guivar_ptr >= MAX_STATIONS_PER_CONTROLLER )
			{
				// 10/17/2013 ajv : We've reached the end, so wrap around to the
				// beginning.
				*pstation_guivar_ptr = 0;
			}
		}
		else
		{
			if( *pstation_guivar_ptr == MAX_CONVENTIONAL_STATIONS )
			{
				*pstation_guivar_ptr = 0;
			}
			else if( *pstation_guivar_ptr > MAX_CONVENTIONAL_STATIONS )
			{
				(*pstation_guivar_ptr)--;
			}
			else
			{
				// 10/17/2013 ajv : We've reached the beginning, so wrap around
				// to the end.
				*pstation_guivar_ptr = (MAX_STATIONS_PER_CONTROLLER - 1);
			}
		}

		STATION_get_station_number_string( FLOWSENSE_get_controller_index(), *pstation_guivar_ptr, pstation_str_guivar_ptr, sizeof(GuiVar_TwoWireStaStrA) );

		*pstation_is_set_guivar_ptr = (*pstation_guivar_ptr > 0);

		// 10/18/2013 ajv : Determine whether the selected station is a valid
		// station to assign or whether it's already assigned to another output
		// or decoder. If the station is valid, break out of the loop.
		// Otherwise, the loop will continue, skipping to the next available
		// station number.
		lstation = nm_STATION_get_pointer_to_station( FLOWSENSE_get_controller_index(), *pstation_guivar_ptr );

		if( *pstation_guivar_ptr == 0 )
		{
			// 10/18/2013 ajv : A selection of 0 (--- in the UI) is valid no
			// matter what.
			lstation_available = (true);
		}
		else if( (lstation == NULL) && (*pstation_guivar_ptr != pstation_number_of_other_output) )
		{
			// 10/17/2013 ajv : This station doesn't exist yet so it's valid
			// to be assigned to this output. The station will be created
			// when the user leaves the screen.
			lstation_available = (true);
		}
		else if( (*pstation_guivar_ptr == ppreviously_selected_station) && (*pstation_guivar_ptr != pstation_number_of_other_output) )
		{
			// 10/17/2013 ajv : The user landed on the same station they had
			// before, which means they've wrapped all the way around. Stop
			// processing.
			lstation_available = (true);
		}
		else if( (*pstation_guivar_ptr == ppreviously_selected_station_of_other_output) && (*pstation_guivar_ptr != pstation_number_of_other_output) )
		{
			// 10/17/2013 ajv : The station exists, but was previously
			// assigned to the other output. This is valid.
			lstation_available = (true);
		}
		else if( (lstation != NULL) && (nm_STATION_get_decoder_serial_number(lstation) == DECODER_SERIAL_NUM_DEFAULT) && (*pstation_guivar_ptr != pstation_number_of_other_output) )
		{
			// 10/18/2013 ajv : The station exists but is not assigned a
			// serial number, which means it's a valid selection.
			lstation_available = (true);
		}

	} while( lstation_available == (false) );


	if( lstation != NULL )
	{
		// 10/18/2013 ajv : If the station description is the default, hide it
		// from the user.
		if( strncmp( nm_STATION_get_description( lstation ), STATION_DEFAULT_DESCRIPTION, sizeof(GuiVar_TwoWireDescA) ) != 0 )
		{
			strlcpy( pstation_desc_guivar_ptr, nm_STATION_get_description( lstation ), sizeof(GuiVar_TwoWireDescA) );
		}
		else
		{
			strlcpy( pstation_desc_guivar_ptr, "", sizeof(GuiVar_TwoWireDescA) );
		}
	}
	else
	{
		strlcpy( pstation_desc_guivar_ptr, "", sizeof(GuiVar_TwoWireDescA) );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	Refresh_Screen();
}

static void TWO_WIRE_STA_turn_off_active_outputs( void )
{
	if( GuiVar_TwoWireOutputOnA == (true) )
	{
		TWO_WIRE_decoder_solenoid_operation_from_ui( GuiVar_TwoWireDecoderSN, DECODER_OUTPUT_A_BLACK, (false) );
	}

	if( GuiVar_TwoWireOutputOnB == (true) )
	{
		TWO_WIRE_decoder_solenoid_operation_from_ui( GuiVar_TwoWireDecoderSN, DECODER_OUTPUT_B_ORANGE, (false) );
	}
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param pdecoder_index_0 
 *
 * @author 1/29/2013 Adrianusv
 *
 * @revisions (none)
 */
static void TWO_WIRE_STA_copy_decoder_info_into_guivars( const UNS_32 pdecoder_index_0 )
{
	STATION_STRUCT	*lstation;

	UNS_32	lstation_decoder_sn;

	UNS_32	lstation_decoder_output;

	UNS_32	lstation_number_0;

	UNS_32	lthis_controllers_box_index_0;

	lthis_controllers_box_index_0 = FLOWSENSE_get_controller_index();

	// Initialize the station numbers for each output of the decoder.
	GuiVar_TwoWireStaA = 0;
	GuiVar_TwoWireStaB = 0;

	memset( GuiVar_TwoWireStaStrA, 0x00, sizeof(GuiVar_TwoWireStaStrA) );
	memset( GuiVar_TwoWireStaStrB, 0x00, sizeof(GuiVar_TwoWireStaStrB) );

	// 6/12/2013 ajv : Initialize the previously selected decoder to 0 so it the
	// station isn't reset when using NEXT and PREV.
	g_TWO_WIRE_STA_previously_selected_station_A = 0;
	g_TWO_WIRE_STA_previously_selected_station_B = 0;

	GuiVar_TwoWireStaAIsSet = (false);
	GuiVar_TwoWireStaBIsSet = (false);


	xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );

	GuiVar_TwoWireDecoderSN = decoder_info_for_display[ pdecoder_index_0 ].sn;

	GuiVar_TwoWireDecoderFWVersion = decoder_info_for_display[ pdecoder_index_0 ].fw_vers;

	GuiVar_TwoWireDecoderType = decoder_info_for_display[ pdecoder_index_0 ].type;

	xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );


	if (GuiVar_TwoWireDecoderSN > 0 )
	{
		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		lstation = nm_ListGetFirst( &station_info_list_hdr );

		// 10/18/2013 ajv : Loop through the stations until we find a match for both
		// Output A and Output B OR until we have looked at all of the stations.
		while( lstation != NULL )
		{
			// For now, only look at decoders connected to this controller.
			if( nm_STATION_get_box_index_0( lstation ) == lthis_controllers_box_index_0  )
			{
				lstation_decoder_sn = nm_STATION_get_decoder_serial_number( lstation );
				lstation_decoder_output = nm_STATION_get_decoder_output( lstation );

				lstation_number_0 = nm_STATION_get_station_number_0( lstation );

				if( (lstation_decoder_sn == decoder_info_for_display[ pdecoder_index_0 ].sn) && (lstation_number_0 > 0) )
				{
					if( lstation_decoder_output == DECODER_OUTPUT_A_BLACK )
					{
						GuiVar_TwoWireStaA = lstation_number_0;

						STATION_get_station_number_string( FLOWSENSE_get_controller_index(), lstation_number_0, GuiVar_TwoWireStaStrA, sizeof(GuiVar_TwoWireStaStrA) );

						g_TWO_WIRE_STA_previously_selected_station_A = GuiVar_TwoWireStaA;

						// 10/18/2013 ajv : If the station description is the
						// default, hide it from the user.
						if( strncmp( nm_STATION_get_description( lstation ), STATION_DEFAULT_DESCRIPTION, sizeof(GuiVar_TwoWireDescA) ) != 0 )
						{
							strlcpy( GuiVar_TwoWireDescA, nm_STATION_get_description( lstation ), sizeof(GuiVar_TwoWireDescA) );
						}
						else
						{
							strlcpy( GuiVar_TwoWireDescA, "", sizeof(GuiVar_TwoWireDescA) );
						}

						GuiVar_TwoWireStaAIsSet = (true);

						// 10/28/2013 ajv : Force the Output On flag to false
						// since we know the output is always off (at least
						// 'off' in terms of this particular feature) when we
						// enter the screen. The only way it could actually be
						// True when entering the screen is if the keypad times
						// out while the output is on (which should never happen
						// with a 1 min test), but we should always be explicit
						// when setting GuiVars.
						GuiVar_TwoWireOutputOnA = (false);
					}

					if( lstation_decoder_output == DECODER_OUTPUT_B_ORANGE )
					{
						GuiVar_TwoWireStaB = lstation_number_0;

						STATION_get_station_number_string( FLOWSENSE_get_controller_index(), lstation_number_0, GuiVar_TwoWireStaStrB, sizeof(GuiVar_TwoWireStaStrB) );

						g_TWO_WIRE_STA_previously_selected_station_B = GuiVar_TwoWireStaB;

						// 10/18/2013 ajv : If the station description is the
						// default, hide it from the user.
						if( strncmp( nm_STATION_get_description( lstation ), STATION_DEFAULT_DESCRIPTION, sizeof(GuiVar_TwoWireDescA) ) != 0 )
						{
							strlcpy( GuiVar_TwoWireDescB, nm_STATION_get_description( lstation ), sizeof(GuiVar_TwoWireDescB) );
						}
						else
						{
							strlcpy( GuiVar_TwoWireDescB, "", sizeof(GuiVar_TwoWireDescB) );
						}

						GuiVar_TwoWireStaBIsSet = (true);

						// 10/28/2013 ajv : Force the Output On flag to false
						// since we know the output is always off (at least
						// 'off' in terms of this particular feature) when we
						// enter the screen. The only way it could actually be
						// True when entering the screen is if the keypad times
						// out while the output is on (which should never happen
						// with a 1 min test), but we should always be explicit
						// when setting GuiVars.
						GuiVar_TwoWireOutputOnB = (false);
					}
				}
			}

			lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
		}

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param poutput 
 * @param pstation_number 
 * @param ppreviously_selected_station 
 *
 * @author 10/17/2013 AdrianusV
 *
 * @revisions (none)
 */
static void TWO_WIRE_STA_extract_and_store_changes_from_guivars_for_output( const UNS_32 poutput, const UNS_32 pstation_number, UNS_32 *ppreviously_selected_station, const UNS_32 pstation_number_of_other_output )
{
	STATION_STRUCT		*lstation;

	CHANGE_BITS_32_BIT	*lbitfield_of_changes;

	UNS_32	lbox_index_0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lbox_index_0 = FLOWSENSE_get_controller_index();

	if( pstation_number >= MAX_CONVENTIONAL_STATIONS )
	{
		lstation = nm_STATION_get_pointer_to_station( lbox_index_0, pstation_number );

		if( lstation == NULL )
		{
			lstation = nm_STATION_create_new_station( lbox_index_0, pstation_number, CHANGE_REASON_KEYPAD );
		}

		lbitfield_of_changes = STATION_get_change_bits_ptr( lstation, CHANGE_REASON_KEYPAD );

		// 7/7/2014 rmd : We want to set the station as physically available here. Even if just
		// created we must do this as the station is created with physically available set to
		// (false). Also consider if the station we just assigned was previously assigned then
		// 'unassigned' it would be marked as not physically available. So must make physically
		// available again.
		nm_STATION_set_physically_available( lstation, (true), CHANGE_do_not_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, lbox_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

		nm_STATION_set_decoder_serial_number( lstation, GuiVar_TwoWireDecoderSN, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, lbox_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

		nm_STATION_set_decoder_output( lstation, poutput, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, lbox_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
	}

	// 6/25/2013 ajv : Release the previous station assignment to allow the station to be used
	// for a different decoder.
	if( (*ppreviously_selected_station >= MAX_CONVENTIONAL_STATIONS) && (*ppreviously_selected_station != pstation_number) && (*ppreviously_selected_station != pstation_number_of_other_output) )
	{
		lstation = nm_STATION_get_pointer_to_station( lbox_index_0, *ppreviously_selected_station );

		if( lstation != NULL )
		{
			lbitfield_of_changes = STATION_get_change_bits_ptr( lstation, CHANGE_REASON_KEYPAD );

			// ----------
			
			// 10/18/2013 ajv : We have a unique case where we want to see a change line if the station
			// number was set to 0 (meaning no longer in use).
			nm_STATION_set_decoder_serial_number( lstation, DECODER_SERIAL_NUM_DEFAULT, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, lbox_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

			// 7/7/2014 rmd : This is a funny statement. What use is it? No comment written so we don't
			// know. I will leave it alone.
			nm_STATION_set_decoder_output( lstation, poutput, CHANGE_do_not_generate_change_line, CHANGE_REASON_KEYPAD, lbox_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

			// 7/7/2014 rmd : Physically available plays no role in this 'freeing up a station' logic.
			// HOWEVER if a station is no longer assigned to a decoder we mark it as no longer
			// physically available, ie (false), so it is not included in irrigation or other areas.
			nm_STATION_set_physically_available( lstation, (false), CHANGE_do_not_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, lbox_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
		}
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// 6/25/2013 ajv : Reset the previously selected station in case the
	// user re-enters the screen without switching to another decoder. This
	// ensures the station will be reset if it's changed again.
	*ppreviously_selected_station = pstation_number;
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 *
 * @author 1/30/2013 Adrianusv
 *
 * @revisions (none)
 */
static void TWO_WIRE_STA_extract_and_store_changes_from_guivars( void )
{
	TWO_WIRE_STA_extract_and_store_changes_from_guivars_for_output( DECODER_OUTPUT_A_BLACK, GuiVar_TwoWireStaA, &g_TWO_WIRE_STA_previously_selected_station_A, GuiVar_TwoWireStaB );

	TWO_WIRE_STA_extract_and_store_changes_from_guivars_for_output( DECODER_OUTPUT_B_ORANGE, GuiVar_TwoWireStaB, &g_TWO_WIRE_STA_previously_selected_station_B, GuiVar_TwoWireStaA );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param pkey_event 
 *
 * @author 1/29/2013 Adrianusv
 *
 * @revisions (none)
 */
static void TWO_WIRE_STA_process_decoder_list( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	UNS_32	lprev_decoder_type;

	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TWO_WIRE_TURN_ON_OUTPUT_A:
					TWO_WIRE_decoder_solenoid_operation_from_ui( GuiVar_TwoWireDecoderSN, DECODER_OUTPUT_A_BLACK, !(GuiVar_TwoWireOutputOnA) );
					Redraw_Screen( (false) );
					break;

				case CP_TWO_WIRE_TURN_ON_OUTPUT_B:
					TWO_WIRE_decoder_solenoid_operation_from_ui( GuiVar_TwoWireDecoderSN, DECODER_OUTPUT_B_ORANGE, !(GuiVar_TwoWireOutputOnB) );
					Redraw_Screen( (false) );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_PLUS:
		case KEY_MINUS:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TWO_WIRE_STATION_NUMBER_A:
					TWO_WIRE_STA_process_station_number( pkey_event.keycode, &GuiVar_TwoWireStaA, GuiVar_TwoWireStaStrA, &GuiVar_TwoWireStaAIsSet, (char*)&GuiVar_TwoWireDescA, g_TWO_WIRE_STA_previously_selected_station_A, GuiVar_TwoWireStaB, g_TWO_WIRE_STA_previously_selected_station_B );
					break;

				case CP_TWO_WIRE_STATION_NUMBER_B:
					TWO_WIRE_STA_process_station_number( pkey_event.keycode, &GuiVar_TwoWireStaB, GuiVar_TwoWireStaStrB, &GuiVar_TwoWireStaBIsSet, (char*)&GuiVar_TwoWireDescB, g_TWO_WIRE_STA_previously_selected_station_B, GuiVar_TwoWireStaA, g_TWO_WIRE_STA_previously_selected_station_A );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_NEXT:
		case KEY_PREV:
			TWO_WIRE_STA_turn_off_active_outputs();

			lprev_decoder_type = GuiVar_TwoWireDecoderType;

			GROUP_process_NEXT_and_PREV( pkey_event.keycode, GuiVar_TwoWireNumDiscoveredStaDecoders, (void*)&TWO_WIRE_STA_extract_and_store_changes_from_guivars, NULL, (void*)&TWO_WIRE_STA_copy_decoder_info_into_guivars );

			// 10/24/2013 ajv : Update the on-screen indicator of which decoder the user
			// is on and refresh the screen to update it.
			GuiVar_TwoWireStaDecoderIndex = (g_GROUP_list_item_index + 1);

			// 3/16/2016 ajv : If we've moved from a 2-Station to a Moisture Sensor decoder, or vice
			// versa, force a full screen redraw to ensure the appropriate easyGUI panel is displayed.
			if( lprev_decoder_type != GuiVar_TwoWireDecoderType )
			{
				Redraw_Screen( (false) );
			}
			else
			{
				Refresh_Screen();
			}
			break;

		case KEY_C_UP:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TWO_WIRE_STATION_NUMBER_B:
					CURSOR_Select( CP_TWO_WIRE_STATION_NUMBER_A, (true) );
					break;

				case CP_TWO_WIRE_TURN_ON_OUTPUT_B:
					CURSOR_Select( CP_TWO_WIRE_TURN_ON_OUTPUT_A, (true) );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_C_DOWN:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TWO_WIRE_STATION_NUMBER_A:
					CURSOR_Select( CP_TWO_WIRE_STATION_NUMBER_B, (true) );
					break;

				case CP_TWO_WIRE_TURN_ON_OUTPUT_A:
					CURSOR_Select( CP_TWO_WIRE_TURN_ON_OUTPUT_B, (true) );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_C_LEFT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TWO_WIRE_STATION_NUMBER_A:
					TWO_WIRE_STA_turn_off_active_outputs();

					KEY_process_BACK_from_editing_screen( (void*)&FDTO_TWO_WIRE_STA_return_to_menu );
					break;

				default:
					CURSOR_Up( (true) );
			}
			break;

		case KEY_C_RIGHT:
			CURSOR_Down( (true) );
			break;

		case KEY_BACK:
			TWO_WIRE_STA_turn_off_active_outputs();

			KEY_process_BACK_from_editing_screen( (void*)&FDTO_TWO_WIRE_STA_return_to_menu );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Returns to the menu on the left from the editing screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user presses a key to move away from the editing screen and back
 * onto the menu.
 * 
 * @param psave_changes True if the changes should be saved; otherwise, false.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_TWO_WIRE_STA_return_to_menu( void )
{
	FDTO_GROUP_return_to_menu( &g_TWO_WIRE_STA_editing_decoder, (void*)&TWO_WIRE_STA_extract_and_store_changes_from_guivars );
}

/* ---------------------------------------------------------- */
extern void TWO_WIRE_STA_load_decoder_serial_number_into_guivar( const INT_16 pindex_0_i16 )
{
	snprintf( GuiVar_itmGroupName, sizeof(GuiVar_itmGroupName), "  %s %07d", GuiLib_GetTextPtr( GuiStruct_txtSN_0, 0 ), decoder_info_for_display[ pindex_0_i16 ].sn );
}

/* ---------------------------------------------------------- */
/**
 * Draws the Two-Wire Station Decoder List menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the screen is first navigated to.
 * 
 * @param pcomplete_redraw True if the screen should be complete redrawn;
 * otherwise, false. If false, only elements on the screen are updated, not the
 * entire structure, essentially refreshing the content.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void FDTO_TWO_WIRE_STA_draw_menu( const BOOL_32 pcomplete_redraw )
{
	UNS_32 i;

	if( pcomplete_redraw == (true) )
	{
		g_GROUP_list_item_index = 0;

		memset( decoder_info_for_display, 0x00, sizeof(decoder_info_for_display) );

		GuiVar_TwoWireNumDiscoveredStaDecoders = 0;

		// 3/16/2016 ajv : Run through the Decoder list to get a count and make a local copy of just
		// the station and moisture sensor decoders (since the latter have a single station output
		// on them).
		for( i = 0; i < MAX_DECODERS_IN_A_BOX; ++i )
		{
			if( tpmicro_data.decoder_info[ i ].sn != DECODER_SERIAL_NUM_DEFAULT )
			{
				if( (tpmicro_data.decoder_info[ i ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__2_STATION) || (tpmicro_data.decoder_info[ i ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__MOISTURE) )
				{
					decoder_info_for_display[ GuiVar_TwoWireNumDiscoveredStaDecoders ].sn = tpmicro_data.decoder_info[ i ].sn;

					decoder_info_for_display[ GuiVar_TwoWireNumDiscoveredStaDecoders ].fw_vers = tpmicro_data.decoder_info[ i ].id_info.fw_vers;

					decoder_info_for_display[ GuiVar_TwoWireNumDiscoveredStaDecoders ].type = tpmicro_data.decoder_info[ i ].id_info.decoder_type__tpmicro_type;

					GuiVar_TwoWireNumDiscoveredStaDecoders += 1;
				}
			}
			else
			{
				// 5/19/2014 ajv : We've bumped into the first uninitialized
				// decoder which means we're done.
				break;
			}
		}
	}

	FDTO_GROUP_draw_menu( pcomplete_redraw, &g_TWO_WIRE_STA_editing_decoder, GuiVar_TwoWireNumDiscoveredStaDecoders, GuiStruct_scrTwoWireStationAssignment_0, (void *)&TWO_WIRE_STA_load_decoder_serial_number_into_guivar, (void *)&TWO_WIRE_STA_copy_decoder_info_into_guivars, (false) );

	// 10/24/2013 ajv : Update the on-screen indicator of which decoder the user
	// is on and refresh the screen to update it.
	GuiVar_TwoWireStaDecoderIndex = (g_GROUP_list_item_index + 1);
	Refresh_Screen();
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed on the Two-Wire Station Decoder List menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void TWO_WIRE_STA_process_menu( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	if( (!g_TWO_WIRE_STA_editing_decoder) && (pkey_event.keycode == KEY_BACK) )
	{
		GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;

		KEY_process_global_keys( pkey_event );

		// 3/17/2016 ajv : Since the user got here from the 2-WIRE dialog box, redraw that dialog
		// box.
		TWO_WIRE_ASSIGNMENT_draw_dialog( (true) );
	}
	else
	{
		GROUP_process_menu( pkey_event, &g_TWO_WIRE_STA_editing_decoder, GuiVar_TwoWireNumDiscoveredStaDecoders, (void *)&TWO_WIRE_STA_process_decoder_list, NULL, NULL, (void *)&TWO_WIRE_STA_copy_decoder_info_into_guivars );

		// 10/24/2013 ajv : Update the on-screen indicator of which decoder the user
		// is on and refresh the screen to update it.
		GuiVar_TwoWireStaDecoderIndex = (g_GROUP_list_item_index + 1);
		Refresh_Screen();
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

