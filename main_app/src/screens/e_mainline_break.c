/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_mainline_break.h"

#include	"app_startup.h"

#include	"combobox.h"

#include	"cursor_utils.h"

#include	"group_base_file.h"

#include	"irrigation_system.h"

#include	"m_main.h"

#include	"screen_utils.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_MAINLINE_BREAK_DURING_IRRI_0			(0)
#define	CP_MAINLINE_BREAK_DURING_IRRI_1			(1)
#define	CP_MAINLINE_BREAK_DURING_IRRI_2			(2)
#define	CP_MAINLINE_BREAK_DURING_IRRI_3			(3)
#define	CP_MAINLINE_BREAK_DURING_MVOR_0			(10)
#define	CP_MAINLINE_BREAK_DURING_MVOR_1			(11)
#define	CP_MAINLINE_BREAK_DURING_MVOR_2			(12)
#define	CP_MAINLINE_BREAK_DURING_MVOR_3			(13)
#define	CP_MAINLINE_BREAK_ALL_OTHER_TIMES_0		(20)
#define	CP_MAINLINE_BREAK_ALL_OTHER_TIMES_1		(21)
#define	CP_MAINLINE_BREAK_ALL_OTHER_TIMES_2		(22)
#define	CP_MAINLINE_BREAK_ALL_OTHER_TIMES_3		(23)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void FDTO_MAINLINE_BREAK_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		MAINLINE_BREAK_copy_group_into_guivars();

		lcursor_to_select = 0;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrMainlineBreak_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void MAINLINE_BREAK_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	UNS_32	inc_by;

	switch( pkey_event.keycode )
	{
		case KEY_MINUS:
		case KEY_PLUS:
			inc_by = SYSTEM_get_inc_by_for_MLB_and_capacity( pkey_event.repeats );

			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_MAINLINE_BREAK_DURING_IRRI_0:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_0, IRRIGATION_SYSTEM_MLB_MIN, IRRIGATION_SYSTEM_MLB_MAX, inc_by, (true) );
					break;

				case CP_MAINLINE_BREAK_DURING_IRRI_1:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_1, IRRIGATION_SYSTEM_MLB_MIN, IRRIGATION_SYSTEM_MLB_MAX, inc_by, (true) );
					break;

				case CP_MAINLINE_BREAK_DURING_IRRI_2:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_2, IRRIGATION_SYSTEM_MLB_MIN, IRRIGATION_SYSTEM_MLB_MAX, inc_by, (true) );
					break;

				case CP_MAINLINE_BREAK_DURING_IRRI_3:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_3, IRRIGATION_SYSTEM_MLB_MIN, IRRIGATION_SYSTEM_MLB_MAX, inc_by, (true) );
					break;

				case CP_MAINLINE_BREAK_DURING_MVOR_0:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_0, IRRIGATION_SYSTEM_MLB_MIN, IRRIGATION_SYSTEM_MLB_MAX, inc_by, (true) );
					break;

				case CP_MAINLINE_BREAK_DURING_MVOR_1:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_1, IRRIGATION_SYSTEM_MLB_MIN, IRRIGATION_SYSTEM_MLB_MAX, inc_by, (true) );
					break;

				case CP_MAINLINE_BREAK_DURING_MVOR_2:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_2, IRRIGATION_SYSTEM_MLB_MIN, IRRIGATION_SYSTEM_MLB_MAX, inc_by, (true) );
					break;

				case CP_MAINLINE_BREAK_DURING_MVOR_3:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_3, IRRIGATION_SYSTEM_MLB_MIN, IRRIGATION_SYSTEM_MLB_MAX, inc_by, (true) );
					break;

				case CP_MAINLINE_BREAK_ALL_OTHER_TIMES_0:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingC_0, IRRIGATION_SYSTEM_MLB_MIN, IRRIGATION_SYSTEM_MLB_MAX, inc_by, (true) );
					break;

				case CP_MAINLINE_BREAK_ALL_OTHER_TIMES_1:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingC_1, IRRIGATION_SYSTEM_MLB_MIN, IRRIGATION_SYSTEM_MLB_MAX, inc_by, (true) );
					break;

				case CP_MAINLINE_BREAK_ALL_OTHER_TIMES_2:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingC_2, IRRIGATION_SYSTEM_MLB_MIN, IRRIGATION_SYSTEM_MLB_MAX, inc_by, (true) );
					break;

				case CP_MAINLINE_BREAK_ALL_OTHER_TIMES_3:
					process_uns32( pkey_event.keycode, &GuiVar_GroupSettingC_3, IRRIGATION_SYSTEM_MLB_MIN, IRRIGATION_SYSTEM_MLB_MAX, inc_by, (true) );
					break;

				default:
					bad_key_beep();
			}
			Redraw_Screen( (false) );
			break;

		case KEY_PREV:
		case KEY_C_UP:
			if( (GuiLib_ActiveCursorFieldNo % 10) == 0 )
			{
				bad_key_beep();
			}
			else
			{
				CURSOR_Up( (true) );
			}
			break;

		case KEY_NEXT:
		case KEY_C_DOWN:
			if( (UNS_32)(GuiLib_ActiveCursorFieldNo % 10) == (SYSTEM_num_systems_in_use() - 1) )
			{
				bad_key_beep();
			}
			else
			{
				CURSOR_Down( (true) );
			}
			break;

		case KEY_C_LEFT:
			if( (GuiLib_ActiveCursorFieldNo >= CP_MAINLINE_BREAK_DURING_IRRI_0) && (GuiLib_ActiveCursorFieldNo <= CP_MAINLINE_BREAK_DURING_IRRI_3) )
			{
				CURSOR_Select( (UNS_32)((GuiLib_ActiveCursorFieldNo + 20) - 1), (true) );
			}
			else if( (GuiLib_ActiveCursorFieldNo >= CP_MAINLINE_BREAK_DURING_MVOR_0) && (GuiLib_ActiveCursorFieldNo <= CP_MAINLINE_BREAK_DURING_MVOR_3) )
			{
				CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo - 10), (true) );
			}
			else if( (GuiLib_ActiveCursorFieldNo >= CP_MAINLINE_BREAK_ALL_OTHER_TIMES_0) && (GuiLib_ActiveCursorFieldNo <= CP_MAINLINE_BREAK_ALL_OTHER_TIMES_3) )
			{
				CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo - 10), (true) );
			}
			else
			{
				bad_key_beep();
			}
			break;

		case KEY_C_RIGHT:
			if( (GuiLib_ActiveCursorFieldNo >= CP_MAINLINE_BREAK_DURING_IRRI_0) && (GuiLib_ActiveCursorFieldNo <= CP_MAINLINE_BREAK_DURING_IRRI_3) )
			{
				CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo + 10), (true) );
			}
			else if( (GuiLib_ActiveCursorFieldNo >= CP_MAINLINE_BREAK_DURING_MVOR_0) && (GuiLib_ActiveCursorFieldNo <= CP_MAINLINE_BREAK_DURING_MVOR_3) )
			{
				CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo + 10), (true) );
			}
			else if( (GuiLib_ActiveCursorFieldNo >= CP_MAINLINE_BREAK_ALL_OTHER_TIMES_0) && (GuiLib_ActiveCursorFieldNo <= CP_MAINLINE_BREAK_ALL_OTHER_TIMES_3) )
			{
				CURSOR_Select( (UNS_32)((GuiLib_ActiveCursorFieldNo - 20) + 1), (true) );
			}
			else
			{
				bad_key_beep();
			}
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = CP_MAIN_MENU_MAIN_LINES;

			MAINLINE_BREAK_extract_and_store_changes_from_GuiVars();
			// No need to break here to allow this to fall into the default
			// condition.

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

