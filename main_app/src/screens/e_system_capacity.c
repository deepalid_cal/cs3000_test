/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_system_capacity.h"

#include	"app_startup.h"

#include	"combobox.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"group_base_file.h"

#include	"irrigation_system.h"

#include	"m_main.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_SYSTEM_CAPACITY_IN_USE_0				(0)
#define	CP_SYSTEM_CAPACITY_IN_USE_1				(1)
#define	CP_SYSTEM_CAPACITY_IN_USE_2				(2)
#define	CP_SYSTEM_CAPACITY_IN_USE_3				(3)
#define	CP_SYSTEM_CAPACITY_WITH_PUMP_0			(10)
#define	CP_SYSTEM_CAPACITY_WITH_PUMP_1			(11)
#define	CP_SYSTEM_CAPACITY_WITH_PUMP_2			(12)
#define	CP_SYSTEM_CAPACITY_WITH_PUMP_3			(13)
#define	CP_SYSTEM_CAPACITY_WITHOUT_PUMP_0		(20)
#define	CP_SYSTEM_CAPACITY_WITHOUT_PUMP_1		(21)
#define	CP_SYSTEM_CAPACITY_WITHOUT_PUMP_2		(22)
#define	CP_SYSTEM_CAPACITY_WITHOUT_PUMP_3		(23)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32 *g_SYSTEM_CAPACITY_combo_box_guivar;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void SYSTEM_CAPACITY_process_capacity( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event, const BOOL_32 pcapacity_in_use, UNS_32 *pcapacity_ptr )
{
	UNS_32	inc_by;

	inc_by = SYSTEM_get_inc_by_for_MLB_and_capacity( pkey_event.repeats );

	if( pcapacity_in_use )
	{
		process_uns32( pkey_event.keycode, pcapacity_ptr, IRRIGATION_SYSTEM_CAPACITY_MIN, IRRIGATION_SYSTEM_CAPACITY_MAX, inc_by, (true) );
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void FDTO_SYSTEM_CAPACITY_show_dropdown( const UNS_32 px_coord, const UNS_32 py_coord )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( px_coord, py_coord, *g_SYSTEM_CAPACITY_combo_box_guivar );
}

/* ---------------------------------------------------------- */
extern void FDTO_SYSTEM_CAPACITY_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		SYSTEM_CAPACITY_copy_group_into_guivars();

		lcursor_to_select = 0;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrMainlineCapacity_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void SYSTEM_CAPACITY_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_cbxNoYes_0:
			COMBO_BOX_key_press( pkey_event.keycode, g_SYSTEM_CAPACITY_combo_box_guivar );
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_SYSTEM_CAPACITY_IN_USE_0:
							g_SYSTEM_CAPACITY_combo_box_guivar = &GuiVar_GroupSettingA_0;
							break;

						case CP_SYSTEM_CAPACITY_IN_USE_1:
							g_SYSTEM_CAPACITY_combo_box_guivar = &GuiVar_GroupSettingA_1;
							break;

						case CP_SYSTEM_CAPACITY_IN_USE_2:
							g_SYSTEM_CAPACITY_combo_box_guivar = &GuiVar_GroupSettingA_2;
							break;

						case CP_SYSTEM_CAPACITY_IN_USE_3:
							g_SYSTEM_CAPACITY_combo_box_guivar = &GuiVar_GroupSettingA_3;
							break;
					}

					if( (GuiLib_ActiveCursorFieldNo >= CP_SYSTEM_CAPACITY_IN_USE_0) && (GuiLib_ActiveCursorFieldNo <= CP_SYSTEM_CAPACITY_IN_USE_3) )
					{
						good_key_beep();

						lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
						lde._04_func_ptr = (void*)&FDTO_SYSTEM_CAPACITY_show_dropdown;
						lde._06_u32_argument1 = 158;
						lde._07_u32_argument2 = (GuiLib_ActiveCursorFieldNo == CP_SYSTEM_CAPACITY_IN_USE_0) ? GROUP_COMBO_BOX_Y_START : (UNS_32)((GuiLib_ActiveCursorFieldNo * Pos_Y_GroupSetting) + GROUP_COMBO_BOX_Y_START);
						Display_Post_Command( &lde );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_SYSTEM_CAPACITY_IN_USE_0:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_0, (false), (true), 1, (true) );
							break;

						case CP_SYSTEM_CAPACITY_IN_USE_1:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_1, (false), (true), 1, (true) );
							break;

						case CP_SYSTEM_CAPACITY_IN_USE_2:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_2, (false), (true), 1, (true) );
							break;

						case CP_SYSTEM_CAPACITY_IN_USE_3:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_3, (false), (true), 1, (true) );
							break;

						case CP_SYSTEM_CAPACITY_WITH_PUMP_0:
							SYSTEM_CAPACITY_process_capacity( pkey_event, GuiVar_GroupSettingA_0, &GuiVar_GroupSettingB_0 );
							break;

						case CP_SYSTEM_CAPACITY_WITH_PUMP_1:
							SYSTEM_CAPACITY_process_capacity( pkey_event, GuiVar_GroupSettingA_1, &GuiVar_GroupSettingB_1 );
							break;

						case CP_SYSTEM_CAPACITY_WITH_PUMP_2:
							SYSTEM_CAPACITY_process_capacity( pkey_event, GuiVar_GroupSettingA_2, &GuiVar_GroupSettingB_2 );
							break;

						case CP_SYSTEM_CAPACITY_WITH_PUMP_3:
							SYSTEM_CAPACITY_process_capacity( pkey_event, GuiVar_GroupSettingA_3, &GuiVar_GroupSettingB_3 );
							break;

						case CP_SYSTEM_CAPACITY_WITHOUT_PUMP_0:
							SYSTEM_CAPACITY_process_capacity( pkey_event, GuiVar_GroupSettingA_0, &GuiVar_GroupSettingC_0 );
							break;

						case CP_SYSTEM_CAPACITY_WITHOUT_PUMP_1:
							SYSTEM_CAPACITY_process_capacity( pkey_event, GuiVar_GroupSettingA_1, &GuiVar_GroupSettingC_1 );
							break;

						case CP_SYSTEM_CAPACITY_WITHOUT_PUMP_2:
							SYSTEM_CAPACITY_process_capacity( pkey_event, GuiVar_GroupSettingA_2, &GuiVar_GroupSettingC_2 );
							break;

						case CP_SYSTEM_CAPACITY_WITHOUT_PUMP_3:
							SYSTEM_CAPACITY_process_capacity( pkey_event, GuiVar_GroupSettingA_3, &GuiVar_GroupSettingC_3 );
							break;

						default:
							bad_key_beep();
					}
					Redraw_Screen( (false) );
					break;

				case KEY_PREV:
				case KEY_C_UP:
					if( (GuiLib_ActiveCursorFieldNo % 10) == 0 )
					{
						bad_key_beep();
					}
					else
					{
						CURSOR_Up( (true) );
					}
					break;

				case KEY_NEXT:
				case KEY_C_DOWN:
					if( (UNS_32)(GuiLib_ActiveCursorFieldNo % 10) == (SYSTEM_num_systems_in_use() - 1) )
					{
						bad_key_beep();
					}
					else
					{
						CURSOR_Down( (true) );
					}
					break;

				case KEY_C_LEFT:
					if( (GuiLib_ActiveCursorFieldNo >= CP_SYSTEM_CAPACITY_IN_USE_0) && (GuiLib_ActiveCursorFieldNo <= CP_SYSTEM_CAPACITY_IN_USE_3) )
					{
						CURSOR_Select( (UNS_32)((GuiLib_ActiveCursorFieldNo + 20) - 1), (true) );
					}
					else if( (GuiLib_ActiveCursorFieldNo >= CP_SYSTEM_CAPACITY_WITH_PUMP_0) && (GuiLib_ActiveCursorFieldNo <= CP_SYSTEM_CAPACITY_WITH_PUMP_3) )
					{
						CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo - 10), (true) );
					}
					else if( (GuiLib_ActiveCursorFieldNo >= CP_SYSTEM_CAPACITY_WITHOUT_PUMP_0) && (GuiLib_ActiveCursorFieldNo <= CP_SYSTEM_CAPACITY_WITHOUT_PUMP_3) )
					{
						CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo - 10), (true) );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case KEY_C_RIGHT:
					if( (GuiLib_ActiveCursorFieldNo >= CP_SYSTEM_CAPACITY_IN_USE_0) && (GuiLib_ActiveCursorFieldNo <= CP_SYSTEM_CAPACITY_IN_USE_3) )
					{
						CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo + 10), (true) );
					}
					else if( (GuiLib_ActiveCursorFieldNo >= CP_SYSTEM_CAPACITY_WITH_PUMP_0) && (GuiLib_ActiveCursorFieldNo <= CP_SYSTEM_CAPACITY_WITH_PUMP_3) )
					{
						CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo + 10), (true) );
					}
					else if( (GuiLib_ActiveCursorFieldNo >= CP_SYSTEM_CAPACITY_WITHOUT_PUMP_0) && (GuiLib_ActiveCursorFieldNo <= CP_SYSTEM_CAPACITY_WITHOUT_PUMP_3) )
					{
						CURSOR_Select( (UNS_32)((GuiLib_ActiveCursorFieldNo - 20) + 1), (true) );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case KEY_BACK:
					GuiVar_MenuScreenToShow = CP_MAIN_MENU_MAIN_LINES;

					SYSTEM_CAPACITY_extract_and_store_changes_from_GuiVars();
					// No need to break here to allow this to fall into the default
					// condition.

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

