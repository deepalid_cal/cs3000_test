/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 12/15/2015 skc : For strlcpy
#include	<string.h>

#include	"alerts.h"

#include	"app_startup.h"

#include	"budgets.h"

// 3/24/2016 skc : For __round_float()
#include	"cal_math.h"

#include	"change.h"

#include	"combobox.h"

#include	"configuration_network.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"e_budgets.h"

#include	"e_numeric_keypad.h"

#include	"e_poc.h"

#include	"epson_rx_8025sa.h"

#include	"flowsense.h"

#include	"group_base_file.h"

#include	"irrigation_system.h"

#include	"lights.h"

#include	"math.h"

#include	"poc.h"

#include	"scrollbox.h"

#include	"speaker.h"

#include	"et_data.h"


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// 3/22/2016 skc : Budgets and POCs

// 8/10/2018 Ryan : Changed name of first cursor position to reflect screen when annual
// budget is selected.
#define	CP_BUDGET_PERIOD_IDX_OR_ANNUAL_BUDGET		(0)
#define CP_BUDGET_PERIOD_START						(1)
#define CP_BUDGET_PERIOD_END						(2)
#define CP_BUDGET_FOR_THIS_PERIOD					(3)
#define	CP_BUDGET_UNIT_GALLON						(4)
#define	CP_BUDGET_UNIT_HCF							(5)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Forward declarations
static void FDTO_BUDGETS_return_to_menu( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/23/2016 skc : Variable for handling the messiness of converting from "01/21/16" to date_time.D
// This is for gui support.
UNS_32 g_BUDGET_SETUP_meter_read_date[ IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS ];

// 3/23/2016 skc : Variable for holding budget values while editing.
// Using a float because we may need to increment budget values using HCF units (1 HCF = 748.05 gal)
float g_BUDGET_SETUP_budget[ IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS ];

// 8/13/2018 Ryan : Variable for holding annual budget values while editing.
float g_BUDGET_SETUP_annual_budget;


static UNS_32 date_change_warning_flag;

/* ---------------------------------------------------------- */
static UNS_32 BUDGETS_get_inc_by_for_yearly_budget( const UNS_32 prepeats )
{
	UNS_32	rv;

	if( prepeats > 64 )
	{
		rv = 5000;
	}
	else if( prepeats > 32 )
	{
		rv = 500;
	}
	else
	{
		rv = 100;
	}

	return( rv );
} 


/* ---------------------------------------------------------- */
static UNS_32 POC_get_inc_by_for_monthly_budget( const UNS_32 prepeats )
{
	UNS_32	rv;
	const UNS_32  scale = 16;

#if 0
	UNS_32 exponent = prepeats / scale; //integer division
	if( exponent > 4 )
	{
		exponent = 4;
	}
	rv = pow(10, exponent);
#endif

	if(  prepeats > 3 * scale )
	{
		rv = pow(10, 3); // increment by 1000's
	}
	else if( prepeats > 2 * scale )
	{
		rv = pow(10,2); // increment by 100's
	}
	else if( prepeats > 1 * scale )
	{
		//rv = pow(10,1); // increment by 10's
		rv = pow(10,2); // increment by 100's
	}
	else
	{
		//rv = pow(10,0);
		//rv = pow(10,1);
		rv = pow(10,2); // increment by 100's
	}

	return( rv );
}

/* ---------------------------------------------------------- */
#if 0
static void FDTO_POC_show_budget_option_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxBudgetOption_0, &FDTO_COMBOBOX_add_items, 4, GuiVar_POCBudgetOption );
}
#endif

// 4/21/2016 skc : If the budgets will be exceeded, show a warning on the screen.
static void BUDGETS_show_warnings()
{
	// 8/10/2018 Ryan : also set warning if annual budget is equal zero.
	GuiVar_BudgetZeroWarning = ( ( GuiVar_BudgetForThisPeriod == 0 || GuiVar_BudgetAnnualValue == 0 ) ? 1 : 0);
	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
// 3/23/2016 skc : When the user changes the budget period, we need to repopulate the fields.
// A refined version of BUDGETS_copy_group_into_guivars() in poc.c
// The fields are populated from local storage, NOT from system/poc variables
static void BUDGETS_update_guivars( const UNS_32 budget_idx )
{
	// 5/23/2016 skc : Gallons to HCF conversion factor, handle FP bug.
	const volatile float HCF_CONVERSION = 748.05f;

	IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys;

	BUDGET_DETAILS_STRUCT bds;

	DATE_TIME_COMPLETE_STRUCT dtcs;

	float sf; // conversion from gallons to HCF (1 HCF = 748.05 gallons)

	UNS_32 index_poc_preserves;

	char str[16]; // 03/23/2016 skc : Must hold at least "01/31/2016"
	
	// ----------

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	ptr_sys = SYSTEM_get_group_with_this_GID( POC_get_GID_irrigation_system(POC_get_group_at_this_index(g_GROUP_list_item_index)) );

	// ----------

	// 3/28/2016 skc : Set the display units
	if( NETWORK_CONFIG_get_water_units() == NETWORK_CONFIG_WATER_UNIT_GALLONS)
	{
		 sf = 1.f;

		 GuiVar_BudgetUnitGallon = 1;
		 GuiVar_BudgetUnitHCF = 0;
	}
	else
	{
		 sf = 1.f / HCF_CONVERSION;

		 GuiVar_BudgetUnitGallon = 0;
		 GuiVar_BudgetUnitHCF = 1;
	}

	GuiVar_BudgetPeriodIdx = budget_idx;

	strlcpy( GuiVar_BudgetPeriodStart, GetDateStr(str, sizeof(str), g_BUDGET_SETUP_meter_read_date[budget_idx], DATESTR_show_short_year, 0), 16 );

	strlcpy( GuiVar_BudgetPeriodEnd, GetDateStr(str, sizeof(str), g_BUDGET_SETUP_meter_read_date[budget_idx+1], DATESTR_show_short_year, 0), 16 );

	GuiVar_BudgetForThisPeriod = sf * g_BUDGET_SETUP_budget[ budget_idx ];
	
	GuiVar_BudgetAnnualValue = sf * g_BUDGET_SETUP_annual_budget;

	// 5/12/2016 skc : We can't accurately show the expected use on slaves, we don't have the
	// raw data.
	if( GuiVar_IsMaster )
	{
		// 3/25/2016 skc : Budget period 0 has some special rules.
		if( budget_idx == 0 )
		{
			xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

			POC_PRESERVES_get_poc_preserve_for_this_poc_gid( g_GROUP_ID, &index_poc_preserves );
			GuiVar_BudgetUseThisPeriod = sf * nm_BUDGET_get_used( index_poc_preserves );

			xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
		}
		else
		{
			// 3/25/2016 skc : We're in the future, no editing of start date, and no used water.
			GuiVar_BudgetUseThisPeriod = 0;
		}

		// 3/23/2016 skc : Get the predicted volume for this budget period.
		// Use the start date for the budget period.
		SYSTEM_get_budget_details( ptr_sys, &bds );

		if( budget_idx == 0 )
		{
			// 3/25/2016 skc : The expected use = Used + Scheduled to the end of the period.
			EPSON_obtain_latest_complete_time_and_date( &dtcs );
			GuiVar_BudgetExpectedUseThisPeriod = sf * (nm_SYSTEM_get_used( ptr_sys ) + nm_BUDGET_predicted_volume( nm_GROUP_get_group_ID(ptr_sys), &bds, &dtcs, dtcs.date_time, false ));
		}
		else
		{
			// 3/25/2016 skc : We're in the future, use only the scheduled volume.
			DateAndTimeToDTCS( g_BUDGET_SETUP_meter_read_date[budget_idx], bds.meter_read_time, &dtcs );
			GuiVar_BudgetExpectedUseThisPeriod = sf * nm_BUDGET_predicted_volume( nm_GROUP_get_group_ID(ptr_sys), &bds, &dtcs, dtcs.date_time, false );
		}

		// 3/25/2016 skc : Do we need to display warnings?
		BUDGETS_show_warnings( bds.mode );
	}

	// ----------

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Update monthly budget values based on the annual budget value.
 * 
 * @executed Executed within the context of the BUDGET_SETUP_handle_annual_budget_change and
 * FDTO_BUDGETS_after_keypad function after annual budget value is altered.
 * 
 * @param none
 * 
 * @return void
 *
 * @author 8/10/2018 Ryan
 *
 * @revisions
 *    8/10/2018 Initial release
 */
static void BUDGET_SETUP_update_montly_budget( void )
{
	UNS_32	i;
	
	UNS_32	month;
	
	float	month_et;
	
	float 	lET_for_year;
	
	float	lbudget_per_inch_ET;
	
	DATE_TIME_COMPLETE_STRUCT dtcs;
	
	// ----------
	
	lET_for_year = 0;

	// ----------
	
	// 8/10/2018 Ryan : Calculate budget per inch ET based on monthly et values and new annual
	// budget.
	for( i = 1; i <= MONTHS_IN_A_YEAR; i++ )
	{
		month_et = ET_DATA_et_number_for_the_month( i );
		
		lET_for_year += month_et;
	}
	
	lbudget_per_inch_ET = g_BUDGET_SETUP_annual_budget / lET_for_year;
	
	// 8/13/2018 Ryan : Change monthly budget values to reflect new annual budget;
	EPSON_obtain_latest_complete_time_and_date( &dtcs );
	
	for( i = 0; i < MONTHS_IN_A_YEAR; i++ )
	{
		month = dtcs.__month + i;
		
		if( dtcs.__month + i > MONTHS_IN_A_YEAR )
		{
			month = dtcs.__month + i - MONTHS_IN_A_YEAR;
		}
		else
		{
			month = dtcs.__month + i;
		}
		
		month_et = ET_DATA_et_number_for_the_month( month );		

		g_BUDGET_SETUP_budget[i] = month_et * lbudget_per_inch_ET;
	}
}

/* ---------------------------------------------------------- */
/**
 * Changes annual budget value based on the +/- key press input of the user.
 * 
 * @executed Executed within the context of processing the Budget Amounts by POC screen.
 * 
 * @param pkey_event either the plus or the minus key press
 * 
 * @return void
 *
 * @author 8/10/2018 Ryan
 *
 * @revisions
 *    8/10/2018 Initial release
 */
static void BUDGET_SETUP_handle_annual_budget_change( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	// 10/15/2018 Ryan : Gallons to HCF conversion factor, handle FP bug.
	const volatile float HCF_CONVERSION = 748.05f;

	float pmin; 	// minimum allowed entry value

	float pmax;	// maximum allowed entry value

	float pdelta;	// delta for computing next entry value

	UNS_32 num_decimals;
	
	// ----------
	
	// 8/10/2018 Ryan : pdelta depends on the budget units we are using
	if( GuiVar_BudgetUnitHCF )
	{
		// 8/10/2018 Ryan : Allow user to change in units of 0.1 HCF.
		pdelta = 0.1f;
		pmin = POC_BUDGET_GALLONS_MIN;
		pmax = ( POC_BUDGET_GALLONS_MAX * MONTHS_IN_A_YEAR ) / HCF_CONVERSION;
		num_decimals = 1;
	}
	else
	{
		pdelta = BUDGETS_get_inc_by_for_yearly_budget( pkey_event.repeats );
		pmin = POC_BUDGET_GALLONS_MIN;
		pmax = POC_BUDGET_GALLONS_MAX * MONTHS_IN_A_YEAR;
		num_decimals = 0;
	}

	// 8/10/2018 Ryan : Update annual budgets, depending on units.
	if( GuiVar_BudgetUnitGallon )
	{
		UNS_32 temp = __round_float( g_BUDGET_SETUP_annual_budget, num_decimals );

		process_uns32_r( pkey_event.keycode, &temp, pmin, pmax, pdelta, false ); 

		// 10/15/2018 Ryan : Update holding variable and GUIvar with the new annual budget.
		g_BUDGET_SETUP_annual_budget = temp;

		GuiVar_BudgetAnnualValue = g_BUDGET_SETUP_annual_budget;
	}
	else
	{
		// 10/15/2018 Ryan : Slightly different method here. Process the UI field,
		// and then update the backing variable
		GuiVar_BudgetAnnualValue = __round_float( GuiVar_BudgetAnnualValue, num_decimals );

		process_fl( pkey_event.keycode, &GuiVar_BudgetAnnualValue, pmin, pmax, pdelta, false ); 

		g_BUDGET_SETUP_annual_budget = GuiVar_BudgetAnnualValue * HCF_CONVERSION;
	}
	
	BUDGET_SETUP_update_montly_budget();

	// ----------

	BUDGETS_show_warnings();
}

/* ---------------------------------------------------------- */
// 3/25/2016 skc : Handler for when the user change the budget value.
static void BUDGET_SETUP_handle_budget_change( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	// 5/23/2016 skc : Gallons to HCF conversion factor, handle FP bug.
	const volatile float HCF_CONVERSION = 748.05f;

	float pmin; 	// minimum allowed entry value

	float pmax;	// maximum allowed entry value

	float pdelta;	// delta for computing next entry value

	UNS_32 num_decimals;

	// ----------

	// 12/17/2015 skc : Determine how to increment the value based on Budget Option.
	// 03/22/2016 skc : We only support Manually Entered
	//switch( GuiVar_BudgetOptionIdx ) 
	//{ 
	//	case POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_as_percent_of_historical_et:
	//		pmin = POC_BUDGET_CALCULATION_PERCENT_OF_ET_MIN;
	//		pmax = POC_BUDGET_CALCULATION_PERCENT_OF_ET_MAX;
	//		pdelta = 1;
	//		break;
	//	// until we get a better handle on this...
	//	case POC_BUDGET_GALLONS_ENTRY_OPTION_projected_budget_from_water_bill:
	//	case POC_BUDGET_GALLONS_ENTRY_OPTION_direct_entry:
	//	default:
			// 3/24/2016 skc : pdelta depends on the budget units we are using
			if( GuiVar_BudgetUnitHCF )
			{
				// 5/6/2016 skc : Allow user to change in units of 0.1 HCF.
				pdelta = 0.1f;
				pmin = POC_BUDGET_GALLONS_MIN;
				pmax = POC_BUDGET_GALLONS_MAX / HCF_CONVERSION;
				num_decimals = 1;
			}
			else
			{
				pdelta = POC_get_inc_by_for_monthly_budget( pkey_event.repeats );
				pmin = POC_BUDGET_GALLONS_MIN;
				pmax = POC_BUDGET_GALLONS_MAX;
				num_decimals = 0;
			}
			
	//		break;
	//}

	if( GuiVar_BudgetUnitGallon )
	{
		// 5/6/2016 skc : Process the backing variable, and then use that to update the UI field.
		// The process_uns32_r makes the data be in multiples of 100.

		// 3/24/2016 skc : We can't send a float* to process_uns_32_r, thus we fake it.
		UNS_32 temp = __round_float( g_BUDGET_SETUP_budget[GuiVar_BudgetPeriodIdx], num_decimals );

		process_uns32_r( pkey_event.keycode, &temp, pmin, pmax, pdelta, false ); 

		g_BUDGET_SETUP_budget[GuiVar_BudgetPeriodIdx] = temp;

		GuiVar_BudgetForThisPeriod = g_BUDGET_SETUP_budget[GuiVar_BudgetPeriodIdx];
	}
	else
	{
		// 5/6/2016 skc : Slightly different method here. Process the UI field,
		// and then update the backing variable
		GuiVar_BudgetForThisPeriod = __round_float( GuiVar_BudgetForThisPeriod, num_decimals );

		process_fl( pkey_event.keycode, &GuiVar_BudgetForThisPeriod, pmin, pmax, pdelta, false ); 

		g_BUDGET_SETUP_budget[GuiVar_BudgetPeriodIdx] = GuiVar_BudgetForThisPeriod * HCF_CONVERSION;
	}

	// ----------

	BUDGETS_show_warnings();
}

/* ---------------------------------------------------------- */
// 03/01/2016 skc : Handle making changes to budget periods.
// Don't allow the dates to overlap.
// We only allow a max of 68/35 days in the budget period. This puts an upper bound on the data processing.
// idx is the index into budget dates array.
static void BUDGET_SETUP_handle_start_date_change( const UNS_32 pkeycode, GuiConst_TEXT *pgui_ptr, UNS_32 idx )
{
	// 5/23/2016 skc : Gallons to HCF conversion factor, handle FP bug.
	const volatile float HCF_CONVERSION = 748.05f;

	char str[ 16 ]; // The date string format is "01/21/16" for 8 characters total

	POC_GROUP_STRUCT *ptr_poc;

	IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys;

	BUDGET_DETAILS_STRUCT bds;

	UNS_32 num_pocs; // How many POCs are attached to a system?

	UNS_32 dum; // Needed to make a successfull call to POC_PRESERVES_get_poc_preserve_for_this_poc_gid()
	BY_POC_RECORD *bpr; // Will tell us how many POCs are attached to the system

	UNS_32 min;
	UNS_32 max;
	UNS_32 MAXBUDGETDAYS; // A budget period is allowed to be this many days max

	// ----------

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	ptr_poc = POC_get_group_at_this_index( g_GROUP_list_item_index );

	// ----------

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	bpr = POC_PRESERVES_get_poc_preserve_for_this_poc_gid( nm_GROUP_get_group_ID(ptr_poc), &dum );

	num_pocs = bpr->this_pocs_system_preserves_ptr->sbf.number_of_pocs_in_this_system;

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	// ----------

	ptr_sys = SYSTEM_get_group_with_this_GID( POC_get_GID_irrigation_system( ptr_poc ) );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	SYSTEM_get_budget_details( ptr_sys, &bds );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	// ----------

	// 6/7/2016 skc : If the date is the 1st, and the user wants to set the end date to the 15th
	// of the next month, he can't. At least with a 35 day limit, he can't.
	// We changed the limit to a flat 68 days.
	//MAXBUDGETDAYS = ( (bds.number_of_annual_periods == IRRIGATION_SYSTEM_BUDGET_ANNUAL_PERIODS_MAX) ? IRRIGATION_SYSTEM_BUDGET_MAXIMUM_PERIOD_12_DAYS : IRRIGATION_SYSTEM_BUDGET_MAXIMUM_PERIOD_6_DAYS); 
	MAXBUDGETDAYS = IRRIGATION_SYSTEM_BUDGET_MAXIMUM_PERIOD_6_DAYS; 

	// ----------

	// Determine the limits, depends on the index
	if( idx == 0) // First budget period
	{
		min = -MAXBUDGETDAYS + g_BUDGET_SETUP_meter_read_date[idx+1] - 1;
		max = g_BUDGET_SETUP_meter_read_date[idx+1] - 1;
	}
	else if( idx < bds.number_of_annual_periods )
	{
		min = g_BUDGET_SETUP_meter_read_date[idx-1] + 1;
		if( g_BUDGET_SETUP_meter_read_date[idx+1] - MAXBUDGETDAYS > min )
		{
			min = g_BUDGET_SETUP_meter_read_date[idx+1] - MAXBUDGETDAYS;
		}

		max = g_BUDGET_SETUP_meter_read_date[idx+1] - 1;
		if( max > g_BUDGET_SETUP_meter_read_date[idx-1] + MAXBUDGETDAYS )
		{
			max = g_BUDGET_SETUP_meter_read_date[idx-1] + MAXBUDGETDAYS;
		}
	}
	else // Last budget period
	{
		min = g_BUDGET_SETUP_meter_read_date[idx-1] + 1;
		max = g_BUDGET_SETUP_meter_read_date[idx-1] + MAXBUDGETDAYS;
	}

	process_uns32( pkeycode, &g_BUDGET_SETUP_meter_read_date[idx], min, max, 1, (false) );

	// pgui_ptr needs at least 16 characters of storage!!!
	strlcpy( pgui_ptr, GetDateStr(str, sizeof(str), g_BUDGET_SETUP_meter_read_date[idx], DATESTR_show_short_year, 0), 16 );

	// Fist time we show a warning, all POCs for this system will have the budget date changed
	if( (0 == date_change_warning_flag) && (num_pocs > 1) )
	{
		DIALOG_draw_ok_dialog( GuiStruct_dlgBudgetDateChange_0 );
		date_change_warning_flag = 1; // So we don't see again until a new POC is selected
	}

	// ----------
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	float sf;
	if( NETWORK_CONFIG_get_water_units() == NETWORK_CONFIG_WATER_UNIT_GALLONS)
	{
		sf = 1.f;
	}
	else
	{
		sf = 1.f / HCF_CONVERSION;
	}

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
 
	// ----------
	// 4/25/2016 skc : Update the expected volume.

	// 4/25/2016 skc : Copy local storage into variable used for calculation of predicted
	// volume.
	for( dum=0; dum<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS; dum++ )
	{
		bds.meter_read_date[dum] = g_BUDGET_SETUP_meter_read_date[dum];
	}

	DATE_TIME_COMPLETE_STRUCT dtcs;
	if( GuiVar_BudgetPeriodIdx == 0 )
	{
		// 3/25/2016 skc : The expected use = Used + Scheduled to the end of the period.
		EPSON_obtain_latest_complete_time_and_date( &dtcs );
		GuiVar_BudgetExpectedUseThisPeriod = GuiVar_BudgetUseThisPeriod + sf * nm_BUDGET_predicted_volume( nm_GROUP_get_group_ID(ptr_sys), &bds, &dtcs, dtcs.date_time, false );
	}
	else
	{
		// 3/25/2016 skc : We're in the future, use only the scheduled volume.
		DateAndTimeToDTCS( g_BUDGET_SETUP_meter_read_date[idx], bds.meter_read_time, &dtcs );
		GuiVar_BudgetExpectedUseThisPeriod = sf * nm_BUDGET_predicted_volume( nm_GROUP_get_group_ID(ptr_sys), &bds, &dtcs, dtcs.date_time, false );
	}

	BUDGETS_show_warnings();
}

/* ---------------------------------------------------------- */
// 3/25/2016 skc : Handler for when the user changes the active budget period.
// We use the system.number_of_annual_period value to set the upper limit of the index
static void BUDGET_SETUP_handle_period_change( const UNS_32 keycode )
{
	BUDGET_DETAILS_STRUCT bds;

	POC_GROUP_STRUCT *ptr_poc;

	IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys;

	ptr_poc = POC_get_group_at_this_index( g_GROUP_list_item_index );

	ptr_sys = SYSTEM_get_group_with_this_GID( POC_get_GID_irrigation_system( ptr_poc ) );

	SYSTEM_get_budget_details( ptr_sys, &bds );

	// 6/7/2016 skc : User can select a years worth of budget periods.
	process_uns32( keycode, &GuiVar_BudgetPeriodIdx, 0, bds.number_of_annual_periods - 1, 1, false ); 

	// Update the corresponding UI controls
	BUDGETS_update_guivars( GuiVar_BudgetPeriodIdx );
}

/* ---------------------------------------------------------- */
// 3/28/2016 skc : Handler for when the display units are changed
static void BUDGET_SETUP_handle_units_change( const UNS_32 field )
{
	// 4/5/2016 ajv : If the units have changed, save the change and redraw the screen.
	BOOL_32	lunits_changed;

	// ----------

	lunits_changed = (false);

	// ----------

	if( CP_BUDGET_UNIT_GALLON == field )
	{
		// 4/5/2016 ajv : Like a traditional radio button, only allow the user to SELECT the
		// highlighted radio button. To toggle it off, they need to select a different radio button.
		if( !GuiVar_BudgetUnitGallon )
		{
			process_bool( &GuiVar_BudgetUnitGallon );

			// 3/28/2016 skc : Change other associated radio buttons
			GuiVar_BudgetUnitHCF = !GuiVar_BudgetUnitGallon;

			lunits_changed = (true);
		}
		else
		{
			bad_key_beep();
		}
	}
	else
	{
		// 4/5/2016 ajv : Like a traditional radio button, only allow the user to SELECT the
		// highlighted radio button. To toggle it off, they need to select a different radio button.
		if( !GuiVar_BudgetUnitHCF )
		{
			process_bool( &GuiVar_BudgetUnitHCF );

			// 3/28/2016 skc : Change other associated radio buttons
			GuiVar_BudgetUnitGallon = !GuiVar_BudgetUnitHCF;

			lunits_changed = (true);
		}
		else
		{
			bad_key_beep();
		}
	}

	// ----------

	if( lunits_changed )
	{
		// 3/28/2016 skc : Save the Units setting
		NETWORK_CONFIG_set_water_units( (GuiVar_BudgetUnitGallon == 1 ? NETWORK_CONFIG_WATER_UNIT_GALLONS : NETWORK_CONFIG_WATER_UNIT_HCF), 
										CHANGE_generate_change_line, 
										CHANGE_REASON_KEYPAD, 
										FLOWSENSE_get_controller_index(), 
										CHANGE_set_change_bits, 
										NETWORK_CONFIG_get_change_bits_ptr(CHANGE_REASON_KEYPAD) );

		// ----------

		// Changing the budget units causes other fields to change, redraw the screen.
		BUDGETS_update_guivars( GuiVar_BudgetPeriodIdx );
		Redraw_Screen( (false) );
	}
}


/* ---------------------------------------------------------- */
static void BUDGETS_process_NEXT_and_PREV( const UNS_32 pkeycode )
{
	UNS_32	lmenu_index_for_displayed_poc;

	lmenu_index_for_displayed_poc = POC_get_menu_index_for_displayed_poc();

	if( (pkeycode == KEY_NEXT) || (pkeycode == KEY_PLUS) )
	{
		// 7/9/2015 ajv : Turn off the automatic redraw functions within the SCROLL_BOX_to_line and
		// SCROLL_BOX_up_or_down routines to ensure the scroll bar does not appear.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_DISABLED );

		SCROLL_BOX_to_line( 0, (INT_16)lmenu_index_for_displayed_poc );

		// 7/13/2015 ajv : Don't allow lmenu_index_for_displayed_poc to exceed the max size of the
		// array
		if( lmenu_index_for_displayed_poc < MAX_POCS_IN_NETWORK )
		{
			++lmenu_index_for_displayed_poc;

			// 7/14/2015 ajv : Attempt to move to the next poc.
			if( POC_MENU_items[ lmenu_index_for_displayed_poc ].poc_ptr != NULL )
			{
				SCROLL_BOX_up_or_down( 0, KEY_C_DOWN );
			}
		}
		else
		{
			bad_key_beep();
		}

		// 7/9/2015 ajv : Now that we've moved the scroll bar without displaying it, reenable the
		// automatic redraw functions to ensure other calls are not affected.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_ENABLED );
	}
	else
	{
		// 7/9/2015 ajv : Turn off the automatic redraw functions within the SCROLL_BOX_to_line and
		// SCROLL_BOX_up_or_down routines to ensure the scroll bar does not appear.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_DISABLED );

		SCROLL_BOX_to_line( 0, (INT_16)lmenu_index_for_displayed_poc );

		// 7/13/2015 ajv : Don't allow lmenu_index_for_displayed_poc to go below 0.
		if( lmenu_index_for_displayed_poc > 0 )
		{
			--lmenu_index_for_displayed_poc;

			// 7/14/2015 ajv : Attempt to move to the previous poc.
			if( POC_MENU_items[ lmenu_index_for_displayed_poc ].poc_ptr != NULL )
			{
				SCROLL_BOX_up_or_down( 0, KEY_C_UP );
			}
		}
		else
		{
			bad_key_beep();
		}

		// 10/24/2013 ajv : Now that we've moved the scroll bar without displaying it, reenable the
		// automatic redraw functions to ensure other calls are not affected.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_ENABLED );
	}

	// ----------

	// 7/9/2015 ajv : If we've actually moved to another POC, save the current POC's information
	// and repopulate the GuiVars with the next POC's.
	if( POC_MENU_items[ lmenu_index_for_displayed_poc ].poc_ptr != NULL )
	{
		BUDGETS_extract_and_store_changes_from_GuiVars();

		g_GROUP_list_item_index = POC_get_index_using_ptr_to_poc_struct( POC_MENU_items[ lmenu_index_for_displayed_poc ].poc_ptr );

		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		// 3/24/2016 skc : Keep the same budget index when switching POCs.
		BUDGETS_copy_group_into_guivars( g_GROUP_list_item_index, GuiVar_BudgetPeriodIdx );

		BUDGETS_update_guivars( GuiVar_BudgetPeriodIdx );

		date_change_warning_flag = 0; // Reset to show warning if budget date changes

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}
	else
	{
		bad_key_beep();
	}

	// 7/9/2015 ajv : Although nothing may have changed if we encountered the top or bottom of
	// the list, we need to force a redraw to ensure the scroll line of the currently selected
	// station is suppressed. Without this call, the SCROLL_BOX_to_line call drew the scroll
	// line and as soon as the next call to GuiLib_Refresh happens (such as once per second when
	// the date is updated), the scroll line will appear.
	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
// 5/6/2016 skc : Because we use a backing variable for the screen elements, We use this
// function as the callback from the numeric keypad. After updating the backing variable,
// call the normal screen draw function.
static void FDTO_BUDGETS_after_keypad( const BOOL_32 pcomplete_redraw )
{
	// 5/23/2016 skc : Gallons to HCF conversion factor, handle FP bug.
	const volatile float HCF_CONVERSION = 748.05f;
	if( GuiVar_BudgetEntryOptionIdx == POC_BUDGET_GALLONS_ENTRY_OPTION_direct_entry )
	{
		if( GuiVar_BudgetUnitGallon )
		{
			g_BUDGET_SETUP_budget[GuiVar_BudgetPeriodIdx] = GuiVar_BudgetForThisPeriod;
		}
		else
		{
			g_BUDGET_SETUP_budget[GuiVar_BudgetPeriodIdx] = GuiVar_BudgetForThisPeriod * HCF_CONVERSION;
		}
	}
	// 10/15/2018 Ryan : Added performming the conversion factor on the annual budget as well.
	else
	{
		if( GuiVar_BudgetUnitGallon )
		{
			g_BUDGET_SETUP_annual_budget = GuiVar_BudgetAnnualValue;
		}
		else
		{
			g_BUDGET_SETUP_annual_budget = GuiVar_BudgetAnnualValue * HCF_CONVERSION;
		}
		
		BUDGET_SETUP_update_montly_budget();
	}

	FDTO_BUDGETS_draw_menu( pcomplete_redraw );
}

/* ---------------------------------------------------------- */
static void FDTO_POC_show_budget_period_idx_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxBudgetPeriodIdx_0, &FDTO_COMBOBOX_add_items, 12, GuiVar_BudgetPeriodIdx ); 
}

static void FDTO_POC_close_budget_period_idx_dropdown( void )
{
	GuiVar_BudgetPeriodIdx = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );
	FDTO_COMBOBOX_hide();

	BUDGETS_update_guivars( GuiVar_BudgetPeriodIdx );
}


/* ---------------------------------------------------------- */
/**
 * Processes the key event from the BUDGETS screen for a particular POC.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 12/16/2015 skc (10/16/2012 Adrianusv)
 *
 * @revisions (none)
 */
static void BUDGETS_process_group( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	// 5/23/2016 skc : Gallons to HCF conversion factor, handle FP bug.
	const volatile float HCF_CONVERSION = 748.05f;

	DISPLAY_EVENT_STRUCT lde;

	switch( GuiLib_CurStructureNdx )
	{
		//case GuiStruct_cbxBudgetOption_0:
		//	COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_POCBudgetOption );
		//  break;

		case GuiStruct_dlgNumericKeypad_0:
			// 5/6/2016 skc : Intercept the callback from the keypad routine so we can update the
			// backing variable for budgets.
			NUMERIC_KEYPAD_process_key( pkey_event, &FDTO_BUDGETS_after_keypad );	
			break;

		case GuiStruct_cbxBudgetPeriodIdx_0:
			if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
			{
				good_key_beep();
				lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
				lde._04_func_ptr = FDTO_POC_close_budget_period_idx_dropdown;
				Display_Post_Command( &lde );
			}
			else
			{
				COMBO_BOX_key_press( pkey_event.keycode, NULL );
			}
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_BUDGET_PERIOD_IDX_OR_ANNUAL_BUDGET:
							// 9/27/2018 Ryan : When annual entry option is selected, open the keypad for entering the
							// annual budget, otherwise open the budget period dropdown.
							if( GuiVar_BudgetEntryOptionIdx != POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_using_annual_number )
							{
								good_key_beep();
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = (void*)&FDTO_POC_show_budget_period_idx_dropdown;
								Display_Post_Command( &lde );
							}
							else
							{
								good_key_beep();
								if( GuiVar_BudgetUnitGallon )
								{
									NUMERIC_KEYPAD_draw_float_keypad( &GuiVar_BudgetAnnualValue, 
																	   POC_BUDGET_GALLONS_MIN, 
																	   POC_BUDGET_GALLONS_MAX * MONTHS_IN_A_YEAR,
																	   0 );
								}
								else
								{
									NUMERIC_KEYPAD_draw_float_keypad( &GuiVar_BudgetAnnualValue, 
																	   POC_BUDGET_GALLONS_MIN, 
																	   ( POC_BUDGET_GALLONS_MAX * MONTHS_IN_A_YEAR ) / HCF_CONVERSION,
																	   1 );
								}
							}
							break;
						case CP_BUDGET_FOR_THIS_PERIOD:
							good_key_beep();
							if( GuiVar_BudgetUnitGallon )
							{
								NUMERIC_KEYPAD_draw_float_keypad( &GuiVar_BudgetForThisPeriod, 
																   POC_BUDGET_GALLONS_MIN, 
																   POC_BUDGET_GALLONS_MAX,
																   0 );
							}
							else
							{
								NUMERIC_KEYPAD_draw_float_keypad( &GuiVar_BudgetForThisPeriod, 
																   POC_BUDGET_GALLONS_MIN, 
																   POC_BUDGET_GALLONS_MAX / HCF_CONVERSION,
																   1 );
							}
							break;

						case CP_BUDGET_UNIT_GALLON:
							BUDGET_SETUP_handle_units_change( CP_BUDGET_UNIT_GALLON );
							break;

						case CP_BUDGET_UNIT_HCF:
							BUDGET_SETUP_handle_units_change( CP_BUDGET_UNIT_HCF );
							break;

						default:
							bad_key_beep();
					}
					Refresh_Screen();
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_BUDGET_PERIOD_IDX_OR_ANNUAL_BUDGET:
							// 9/27/2018 Ryan : When annual entry option is selected, change the annual budget,
							// otherwise change the budget period.
							if( GuiVar_BudgetEntryOptionIdx == POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_using_annual_number )
							{
								BUDGET_SETUP_handle_annual_budget_change( pkey_event );
							}
							else
							{
								BUDGET_SETUP_handle_period_change( pkey_event.keycode );
							}
							break;

						case CP_BUDGET_PERIOD_START:
							BUDGET_SETUP_handle_start_date_change( pkey_event.keycode, GuiVar_BudgetPeriodStart, GuiVar_BudgetPeriodIdx );
							break;

						case CP_BUDGET_PERIOD_END:
							BUDGET_SETUP_handle_start_date_change( pkey_event.keycode, GuiVar_BudgetPeriodEnd, GuiVar_BudgetPeriodIdx + 1 );
							break;

						case CP_BUDGET_FOR_THIS_PERIOD:
							BUDGET_SETUP_handle_budget_change( pkey_event );
							break;

						case CP_BUDGET_UNIT_GALLON:
							BUDGET_SETUP_handle_units_change( CP_BUDGET_UNIT_GALLON );
							break;

						case CP_BUDGET_UNIT_HCF:
							BUDGET_SETUP_handle_units_change( CP_BUDGET_UNIT_HCF );
							break;

						default:
							bad_key_beep();
					}
					Refresh_Screen();
					break;

				case KEY_NEXT:
				case KEY_PREV:
					BUDGETS_process_NEXT_and_PREV( pkey_event.keycode );
					break;

				case KEY_C_UP:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						// 9/10/2018 Ryan : Skip over unused cursor positions when not using direct entry.
						case CP_BUDGET_UNIT_GALLON:
							if( GuiVar_BudgetEntryOptionIdx != POC_BUDGET_GALLONS_ENTRY_OPTION_direct_entry )
							{
								GuiLib_Cursor_Hide();
								GuiLib_ActiveCursorFieldNo = CP_BUDGET_PERIOD_START;
								CURSOR_Up( true );
							}
							else 
							{
								CURSOR_Up( true );
							}
							break;
							
						// 5/27/2016 skc : Move to entering budget value
						case CP_BUDGET_UNIT_HCF:
							if( GuiVar_BudgetEntryOptionIdx == POC_BUDGET_GALLONS_ENTRY_OPTION_direct_entry )
							{
								FDTO_Cursor_Select( CP_BUDGET_FOR_THIS_PERIOD, true );
							}
							// 8/10/2018 Ryan : Move to annual budget when budget entry option is not direct entry.
							else
							{
								GuiLib_Cursor_Hide();
								GuiLib_ActiveCursorFieldNo = CP_BUDGET_PERIOD_START;
								CURSOR_Up( true );
							}
							break;

						default:
							CURSOR_Up( true );
					}
					break;

				case KEY_C_LEFT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						// 9/10/2018 Ryan : Skip over unused cursor positions when not using direct entry.
						case CP_BUDGET_UNIT_GALLON:
							if( GuiVar_BudgetEntryOptionIdx != POC_BUDGET_GALLONS_ENTRY_OPTION_direct_entry )
							{
								GuiLib_Cursor_Hide();
								GuiLib_ActiveCursorFieldNo = CP_BUDGET_PERIOD_START;
								CURSOR_Up( true );
							}
							else 
							{
								CURSOR_Up( true );
							}
							break;
							
						// 2/23/2017 ajv : Move back to the menu if the user presses LEFT from the Billing period
						// field.
						case CP_BUDGET_PERIOD_IDX_OR_ANNUAL_BUDGET:
							KEY_process_BACK_from_editing_screen( (void*)&FDTO_BUDGETS_return_to_menu );
							break;

						default:
							CURSOR_Up( true );
					}
					break;

				case KEY_C_DOWN:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						// 8/10/2018 Ryan : Move to display use in gallons curor position when direct entry budget
						// entry option not selected.
						case CP_BUDGET_PERIOD_IDX_OR_ANNUAL_BUDGET:
							if( GuiVar_BudgetEntryOptionIdx != POC_BUDGET_GALLONS_ENTRY_OPTION_direct_entry )
							{
								GuiLib_ActiveCursorFieldNo = CP_BUDGET_FOR_THIS_PERIOD;
								CURSOR_Down( true );	
							}
							else
							{
								CURSOR_Down( true );
							}
							break;
						
						default:
							CURSOR_Down( true );
					}
					break;

				case KEY_C_RIGHT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						// 8/10/2018 Ryan : Move to display use in gallons curor position when direct entry budget
						// entry option not selected.
						case CP_BUDGET_PERIOD_IDX_OR_ANNUAL_BUDGET:
							if( GuiVar_BudgetEntryOptionIdx != POC_BUDGET_GALLONS_ENTRY_OPTION_direct_entry )
							{
								GuiLib_ActiveCursorFieldNo = CP_BUDGET_FOR_THIS_PERIOD;
								CURSOR_Down( true );	
							}
							else
							{
								CURSOR_Down( true );
							}
							break;
							
						default:
							CURSOR_Down( true );
					}
					break;

				case KEY_BACK:
					KEY_process_BACK_from_editing_screen( (void*)&FDTO_BUDGETS_return_to_menu );
					break;

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/**
 * Returns to the menu on the left from the editing screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user presses a key to move away from the editing screen and back
 * onto the menu.
 * 
 * @author 12/16/2015 skc
 *
 * @revisions (none)
 */
static void FDTO_BUDGETS_return_to_menu( void )
{
	FDTO_GROUP_return_to_menu( &g_POC_editing_group, (void*)&BUDGETS_extract_and_store_changes_from_GuiVars );
}

/* ---------------------------------------------------------- */
/*
 * Draw the Budget menu
*/
extern void FDTO_BUDGETS_draw_menu( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lpocs_in_use;
	UNS_32	lactive_line;
	INT_32 lcursor_to_select = 0;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpocs_in_use = POC_populate_pointers_of_POCs_for_display( true ); 

	if( pcomplete_redraw == (true) )
	{
		lcursor_to_select = GuiLib_NO_CURSOR;
		g_POC_top_line = 0;
		g_POC_editing_group = (false);
		g_POC_current_list_item_index = 0;
		g_GROUP_list_item_index = POC_get_index_using_ptr_to_poc_struct( POC_get_ptr_to_physically_available_poc(g_POC_current_list_item_index) );
		BUDGETS_copy_group_into_guivars( g_GROUP_list_item_index, 0 );
		BUDGETS_update_guivars( 0 );
		date_change_warning_flag = 0; // Reset to show warning if budget date changes
	}
	else
	{
		lcursor_to_select = (INT_32)GuiLib_ActiveCursorFieldNo;
		g_POC_top_line = (UNS_32)GuiLib_ScrollBox_GetTopLine( 0 );
	}

	GuiLib_ShowScreen( GuiStruct_scrBudgetAmountsPerPOC_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_ScrollBox_Close( 0 );
	lactive_line = POC_get_menu_index_for_displayed_poc();

	if( g_POC_editing_group == (true) )
	{
		// 10/24/2013 ajv : Draw the scroll box without highlighting the active
		// line and draw the indicator to show which station the user is looking at.
		GuiLib_ScrollBox_Init_Custom_SetTopLine( 0, &POC_load_poc_name_into_guivar, (INT_16)lpocs_in_use, GuiLib_NO_CURSOR, (INT_16)g_POC_top_line );
		GuiLib_ScrollBox_SetIndicator( 0, lactive_line );
		GuiLib_ScrollBox_Redraw( 0 );
	}
	else
	{
		GuiLib_ScrollBox_Init_Custom_SetTopLine( 0, &POC_load_poc_name_into_guivar, (INT_16)lpocs_in_use, (INT_16)lactive_line, (INT_16)g_POC_top_line );
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed on the POC menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 12/17/2015 skc
 *
 * @revisions (none)
 */
extern void BUDGETS_process_menu( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	if( g_POC_editing_group == (true) )
	{
		BUDGETS_process_group( pkey_event );
	}
	else
	{
		switch( pkey_event.keycode )
		{
			case KEY_C_RIGHT:
			case KEY_SELECT:
				good_key_beep();

				g_POC_current_list_item_index = GuiLib_ScrollBox_GetActiveLine( 0, 0 );
				g_GROUP_list_item_index = POC_get_index_using_ptr_to_poc_struct( POC_get_ptr_to_physically_available_poc( g_POC_current_list_item_index ) );
				g_POC_editing_group = (true);
				GuiLib_ActiveCursorFieldNo = CP_BUDGET_PERIOD_IDX_OR_ANNUAL_BUDGET;
				Redraw_Screen( (false) );
				break;

			case KEY_NEXT:
			case KEY_PREV:
				// Change the key to either UP or DOWN so it can be processed
				// using the KEY_C_UP and KEY_C_DOWN case statement.
				if( pkey_event.keycode == KEY_NEXT )
				{
					pkey_event.keycode = KEY_C_DOWN;
				}
				else
				{
					pkey_event.keycode = KEY_C_UP;
				}
				// Don't break here. This will allow the routine to fall into
				// the next case statement which will actually move the cursor
				// up or down appropriately.

			case KEY_C_DOWN:
			case KEY_C_UP:
				SCROLL_BOX_up_or_down( 0, pkey_event.keycode );

				g_POC_current_list_item_index = GuiLib_ScrollBox_GetActiveLine( 0, 0 );
				g_GROUP_list_item_index = POC_get_index_using_ptr_to_poc_struct( POC_get_ptr_to_physically_available_poc( g_POC_current_list_item_index ) );
				BUDGETS_copy_group_into_guivars( g_GROUP_list_item_index, GuiVar_BudgetPeriodIdx );
				BUDGETS_update_guivars( GuiVar_BudgetPeriodIdx );
				date_change_warning_flag = 0; // Reset to show warning if budget date changes

				// 4/16/2014 ajv : Since the screen contents change based upon
				// what type of POC is displayed (e.g., Bypass Manifold has
				// additional flow meters), a full redraw is necessary rather
				// than just a call to Refresh_Screen().
				Redraw_Screen( (false) );
				break;

			default:
				if( pkey_event.keycode == KEY_BACK )
				{
					GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;
				}

				KEY_process_global_keys( pkey_event );
		}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


