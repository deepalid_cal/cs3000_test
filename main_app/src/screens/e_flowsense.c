/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"e_flowsense.h"

#include	"battery_backed_vars.h"

#include	"combobox.h"

#include	"comm_mngr.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"flowsense.h"

#include	"foal_defs.h"

#include	"m_main.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_FLOWSENSE_PART_OF_CHAIN		(0)
#define	CP_FLOWSENSE_CONTROLLER_LETTER	(1)
#define	CP_FLOWSENSE_NOCIC				(2)
#define CP_FLOWSENSE_START_SCAN_BUTTON	(3)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_FLOWSENSE_show_part_of_chain_dropdown( void )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( 191, 72, GuiVar_FLPartOfChain );
}

/* ---------------------------------------------------------- */
static void FLOWSENSE_process_controller_letter( const UNS_32 pkeycode )
{
	UNS_32	lprev_controller_index;

	lprev_controller_index = GuiVar_FLControllerIndex_0;

	process_uns32( pkeycode, &GuiVar_FLControllerIndex_0, 0, (MAX_CHAIN_LENGTH-1), 1, (false) );

	// Since changing this to or from "A" causes information to appear and
	// disappear, redraw the screen if this happens.
	if( ((GuiVar_FLControllerIndex_0 == 0) && (lprev_controller_index == 1)) ||
		((GuiVar_FLControllerIndex_0 == 1) && (lprev_controller_index == 0)) )
	{
		Redraw_Screen( (false) );
	}
}

/* ---------------------------------------------------------- */
static void FDTO_FLOWSENSE_show_controller_letter_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxControllerLetter_0, &FDTO_COMBOBOX_add_items, MAX_CHAIN_LENGTH, GuiVar_FLControllerIndex_0 );
}

/* ---------------------------------------------------------- */
extern void FDTO_FLOWSENSE_update_screen( void )
{
	UNS_32	i;

	BOOL_32	force_redraw;

	// ----------

	force_redraw = (false);

	// ----------

	// 4/18/2014 ajv : Since this is a intensive process because of call to
	// GuiLib_Refresh, only do this if we're actually part of a chain.
	if( GuiVar_FLPartOfChain == (true) )
	{
		GuiVar_ChainStatsNextContact = next_contact.index;

		GuiVar_ChainStatsCommMngrMode = comm_mngr.mode;

		if( GuiVar_ChainStatsInForced != comm_mngr.chain_is_down )
		{
			GuiVar_ChainStatsInForced = comm_mngr.chain_is_down;

			// 3/15/2016 ajv : Redraw the screen to allow the chain down text to appear or disppear.
			force_redraw = (true);
		}

		// 4/18/2014 ajv : Build a string which indicates all of the controllers
		// that existing within the chain.
		strlcpy( GuiVar_ChainStatsControllersInChain, "A", sizeof(GuiVar_ChainStatsControllersInChain) );

		for( i = 1; i < MAX_CHAIN_LENGTH; ++i )
		{
			if( chain.members[ i ].saw_during_the_scan )
			{
				sp_strlcat( GuiVar_ChainStatsControllersInChain, sizeof(GuiVar_ChainStatsControllersInChain), " %c", (i + 'A') );
			}
		}

		if( force_redraw )
		{
			FDTO_Redraw_Screen( (false) );
		}
		else
		{
			GuiLib_Refresh();
		}
	}
}

/* ---------------------------------------------------------- */
extern void FDTO_FLOWSENSE_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		FLOWSENSE_copy_settings_into_guivars();

		lcursor_to_select = 0;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrFLOWSENSE_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();

	if( GuiVar_FLPartOfChain == (true) )
	{
		FDTO_FLOWSENSE_update_screen();
	}
}

/* ---------------------------------------------------------- */
/**
 * Processes the key event from the FLOWSENSE Setup screen.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to process.
 *
 * @author 3/21/2012 Adrianusv
 *
 * @revisions
 *    3/21/2012 Initial release
 */
extern void FLOWSENSE_process_screen( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_cbxNoYes_0:
			COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_FLPartOfChain );
			break;

		case GuiStruct_cbxControllerLetter_0:
			COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_FLControllerIndex_0 );
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_FLOWSENSE_PART_OF_CHAIN:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_FLOWSENSE_show_part_of_chain_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_FLOWSENSE_CONTROLLER_LETTER:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_FLOWSENSE_show_controller_letter_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_FLOWSENSE_START_SCAN_BUTTON:
							good_key_beep();

							FLOWSENSE_extract_and_store_changes_from_GuiVars( (true) );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch ( GuiLib_ActiveCursorFieldNo )
					{
						case CP_FLOWSENSE_PART_OF_CHAIN:
							process_bool( &GuiVar_FLPartOfChain );

							// Since changing this value causes information to appear and disappear,
							// redraw the screen.
							Redraw_Screen( (false) );
							break;

						case CP_FLOWSENSE_CONTROLLER_LETTER:
							FLOWSENSE_process_controller_letter( pkey_event.keycode );
							break;

						case CP_FLOWSENSE_NOCIC:
							process_uns32( pkey_event.keycode, &GuiVar_FLNumControllersInChain, 2, MAX_CHAIN_LENGTH, 1, (false) );
							break;

						default:
							bad_key_beep();
					}
					Refresh_Screen();
					break;

				case KEY_C_UP:
				case KEY_C_LEFT:
					CURSOR_Up( (true) );
					break;

				case KEY_C_DOWN:
				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				default:
					if( pkey_event.keycode == KEY_BACK )
					{
						FLOWSENSE_extract_and_store_changes_from_GuiVars( (false) );

						GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;
					}

					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

