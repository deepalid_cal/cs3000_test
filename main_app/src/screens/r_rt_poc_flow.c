/* ---------------------------------------------------------- */

// 8/28/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"r_rt_poc_flow.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"cursor_utils.h"

#include	"d_live_screens.h"

#include	"e_poc.h"

#include	"report_utils.h"

#include	"screen_utils.h"

#include	"scrollbox.h"

#include	"speaker.h"





/* ---------------------------------------------------------- */

static void REAL_TIME_POC_FLOW_draw_scroll_line( const INT_16 pline_index_i16 )
{
	POC_GROUP_STRUCT *lpoc_struct;

	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc_struct = POC_get_ptr_to_physically_available_poc( (UNS_32)pline_index_i16 );

	if( lpoc_struct != NULL )
	{
		// 11/1/2013 ajv : TODO For now, to limit how long the string is, we're
		// going to truncate the POC name manually. Ideally, we'd like to have
		// easyGUI automatically limit the size using a clipping rectangle or
		// similar structure, but those don't seem to behave properly on
		// scrolling lists.
		// 
		// 32 characters is how many characters, using fixed-width, we can draw
		// before we hit the flow setting.
		strlcpy( GuiVar_LiveScreensPOCFlowName, nm_GROUP_get_name( lpoc_struct ), 32 );

		BY_POC_RECORD	*lpreserve;

		UNS_32			lindex;

		lpreserve = POC_PRESERVES_get_poc_preserve_for_this_poc_gid( nm_GROUP_get_group_ID(lpoc_struct), &lindex );

		if( lpreserve != NULL )
		{
			if( ((lpreserve->ws[ 0 ].pbf.master_valve_energized_irri == (true))  && (lpreserve->ws[ 0 ].master_valve_type == POC_MV_TYPE_NORMALLY_CLOSED)) ||
				((lpreserve->ws[ 1 ].pbf.master_valve_energized_irri == (true))  && (lpreserve->ws[ 1 ].master_valve_type == POC_MV_TYPE_NORMALLY_CLOSED)) ||
				((lpreserve->ws[ 2 ].pbf.master_valve_energized_irri == (true))  && (lpreserve->ws[ 2 ].master_valve_type == POC_MV_TYPE_NORMALLY_CLOSED)) ||
				((lpreserve->ws[ 0 ].pbf.master_valve_energized_irri == (false)) && (lpreserve->ws[ 0 ].master_valve_type == POC_MV_TYPE_NORMALLY_OPEN)) ||
				((lpreserve->ws[ 1 ].pbf.master_valve_energized_irri == (false)) && (lpreserve->ws[ 1 ].master_valve_type == POC_MV_TYPE_NORMALLY_OPEN)) ||
				((lpreserve->ws[ 2 ].pbf.master_valve_energized_irri == (false)) && (lpreserve->ws[ 2 ].master_valve_type == POC_MV_TYPE_NORMALLY_OPEN)) )
			{
				GuiVar_LiveScreensPOCFlowMVState = POC_MV_TYPE_NORMALLY_CLOSED;
			}
			else
			{
				GuiVar_LiveScreensPOCFlowMVState = POC_MV_TYPE_NORMALLY_OPEN;
			}

			GuiVar_StatusSystemFlowActual = ((UNS_32)lpreserve->ws[ 0 ].delivered_5_second_average_gpm_irri +
											 (UNS_32)lpreserve->ws[ 1 ].delivered_5_second_average_gpm_irri +
											 (UNS_32)lpreserve->ws[ 2 ].delivered_5_second_average_gpm_irri );
		}
		else
		{
			// 10/14/2013 ajv : Since the preserve isn't valid, initialize the
			// variables to 0. This should never happen, but it's important to
			// make sure this case is covered.


			// 10/14/2013 ajv : Invert ("NOT") the value since we're expecting a
			// false if it's closed and a true if it's open.
			GuiVar_LiveScreensPOCFlowMVState = !(POC_STATUS_MV_IS_CLOSED);

			GuiVar_StatusSystemFlowActual = 0;
		}
	}
	else
	{
		Alert_group_not_found();
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void FDTO_REAL_TIME_POC_FLOW_draw_report( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lpoc_count;

	if( pcomplete_redraw == (true) )
	{
		GuiLib_ShowScreen( GuiStruct_rptRTPOCFlow_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );

		GuiLib_ScrollBox_Close( 0 );

		lpoc_count = POC_populate_pointers_of_POCs_for_display( false );

		GuiLib_ScrollBox_Init( 0, &REAL_TIME_POC_FLOW_draw_scroll_line, (INT_16)lpoc_count, 0 );
	}
	else
	{
		GuiLib_ScrollBox_Redraw( 0 );
	}

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void REAL_TIME_POC_FLOW_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		case KEY_BACK:
			GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;

			KEY_process_global_keys( pkey_event );

			if( GuiVar_MenuScreenToShow == SCREEN_MENU_DIAGNOSTICS )
			{
				// 3/16/2016 ajv : Since the user got here from the LIVE SCREENS dialog box, redraw that
				// dialog box.
				LIVE_SCREENS_draw_dialog( (true) );
			}
			break;

		default:
			REPORTS_process_report( pkey_event, 0, 0 );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

