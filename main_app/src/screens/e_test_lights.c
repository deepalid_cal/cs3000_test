/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for abs
#include	<stdlib.h>

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"e_test_lights.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"comm_mngr.h"

#include	"configuration_network.h"

#include	"cursor_utils.h"

#include	"dialog.h"

#include	"epson_rx_8025sa.h"

#include	"flowsense.h"

#include	"irri_lights.h"

#include	"lights.h"

#include	"screen_utils.h"

#include	"speaker.h"

#include	"tpmicro_comm.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_LIGHTS_TEST_OUTPUT		    (0)
#define CP_LIGHTS_TEST_STOP_TIME		(1)
#define CP_LIGHTS_TEST_ON_OFF		    (2)
#define CP_LIGHTS_TEST_SET_STOP_TIME	(3)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define LIGHTS_TEST_TURN_ON		( true )
#define LIGHTS_TEST_TURN_OFF	( false )

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// These are the values used to obtain the displayed data and time strings. Times are stored in seconds, displayed in 
// minutes.  
static UNS_32 g_LIGHTS_TEST_irri_list_stop_date;
static UNS_32 g_LIGHTS_TEST_irri_list_stop_time_in_seconds;
static UNS_32 g_LIGHTS_TEST_editable_stop_date;
static UNS_32 g_LIGHTS_TEST_editable_stop_time_in_seconds;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void LIGHTS_TEST_turn_light_on_or_off( const UNS_32 box_index_0, const UNS_32 output_index_0, const BOOL_32 action )
{
	DATE_TIME ldt, temp_dt;

	// ----------

	if( action == LIGHTS_TEST_TURN_ON )
	{
		// Put the manual stop date and time into a DATE_TIME struct for the upcoming comparison. 
		temp_dt.D = g_LIGHTS_TEST_editable_stop_date;
		temp_dt.T = g_LIGHTS_TEST_editable_stop_time_in_seconds;

		EPSON_obtain_latest_time_and_date( &ldt );

		// The GUI should not have sent an ON with no energized time remaining. This is to make sure.
		if( DT1_IsBiggerThanOrEqualTo_DT2( &ldt, &temp_dt ) )
		{
			bad_key_beep();
		}
		else
		{
			if( IRRI_LIGHTS_request_light_on(LIGHTS_get_lights_array_index(box_index_0, output_index_0), g_LIGHTS_TEST_editable_stop_date, g_LIGHTS_TEST_editable_stop_time_in_seconds) )
			{
				good_key_beep();
			}
			else
			{
				bad_key_beep();
			}
		}
	}
	else // turn it off
	{
		if( IRRI_LIGHTS_request_light_off(LIGHTS_get_lights_array_index(box_index_0, output_index_0)) )
		{
			good_key_beep();
		}
		else
		{
			bad_key_beep();
		}
	}

}

/* ---------------------------------------------------------- */
static void LIGHTS_TEST_calculate_default_off_date_and_time( void )
{
	UNS_32 seconds_left_today;

	DATE_TIME ldt;

	UNS_32 rounded_time;

	// ----------

	// Set the default manual stop time to 4 hours from now. This default value is NOT recalculated as time moves 
	// on, or as the user navigates through lights on this screen. It is assumed the user will not be on the screen
	// long enough to require a recalculation.

	EPSON_obtain_latest_time_and_date( &ldt );

	// Truncate time to a 10 minute interval.
	rounded_time = ( ( ldt.T / LIGHTS_TEN_MINUTES_IN_SECONDS) * LIGHTS_TEN_MINUTES_IN_SECONDS );

	// Round up if appropriate.
	if( (ldt.T % LIGHTS_TEN_MINUTES_IN_SECONDS) >= ( LIGHTS_TEN_MINUTES_IN_SECONDS / 2 ) )
	{
		rounded_time += LIGHTS_TEN_MINUTES_IN_SECONDS;
	}

	// Test for the special case of incrementing by 4 hours when it's 8:00PM or later. Use "rounded_time" for the test 
	// and assignments to avoid rounding errors.
	if( rounded_time < LIGHTS_8_PM_IN_SECONDS )
	{
		g_LIGHTS_TEST_editable_stop_time_in_seconds = ( rounded_time + LIGHTS_FOUR_HOURS_IN_SECONDS );
		g_LIGHTS_TEST_editable_stop_date = ldt.D;
	}
	else
	{
		// Special case. The stop time extends into tomorrow. These are unsigned containers. Take baby steps to 
		// avoid going negative during a calculation.
		seconds_left_today = ( LIGHTS_24_HOURS_IN_SECONDS - rounded_time ); 
		g_LIGHTS_TEST_editable_stop_time_in_seconds = ( LIGHTS_FOUR_HOURS_IN_SECONDS - seconds_left_today );
		g_LIGHTS_TEST_editable_stop_date = ( ldt.D + 1 );
	}

}

/* ---------------------------------------------------------- */
static void nm_LIGHTS_TEST_load_irri_list_stop_date_and_time( void )
{
	// 9/21/2015 mpd : This function must be called while holding the "irri_lights_recursive_MUTEX", and after the 
	// "GuiVar_LightIsEnergized" value is determined. 

	DATE_TIME irri_stop_dt;

	if( GuiVar_LightIsEnergized )
	{
		xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );

		// 9/21/2015 mpd : This light is energized. Get the current stop date and time for the user to view and modify. 
		irri_stop_dt = IRRI_LIGHTS_get_light_stop_date_and_time( LIGHTS_get_lights_array_index( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 ) );

		// 9/22/2015 mpd : This copy will remain fixed until a new stop time is set.
		g_LIGHTS_TEST_irri_list_stop_date = irri_stop_dt.D;
		g_LIGHTS_TEST_irri_list_stop_time_in_seconds = irri_stop_dt.T;

		// Note: This function keeps the IRRI On list values up-to-date. It does not stomp on the editable values which
		// are only reset when switching the displayed light.

		xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );
	}
}

/* ---------------------------------------------------------- */
static void nm_LIGHTS_TEST_load_editable_stop_date_and_time( void )
{
	// 9/21/2015 mpd : This function must be called while holding the "irri_lights_recursive_MUTEX", and after the 
	// "GuiVar_LightIsEnergized" value is determined. 

	if( GuiVar_LightIsEnergized )
	{
		xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );

		// 9/21/2015 mpd : This light is energized. Get the current stop date and time for the user to view and modify. 
		nm_LIGHTS_TEST_load_irri_list_stop_date_and_time();

		g_LIGHTS_TEST_editable_stop_date = g_LIGHTS_TEST_irri_list_stop_date;
		g_LIGHTS_TEST_editable_stop_time_in_seconds = g_LIGHTS_TEST_irri_list_stop_time_in_seconds;

		xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );
	}
	else
	{
		// 9/22/2015 mpd : This light is de-energized. Get the default stop date and time for the user to view and 
		// modify.
		LIGHTS_TEST_calculate_default_off_date_and_time();
	}
}

/* ---------------------------------------------------------- */
static void nm_LIGHTS_TEST_update_stop_date_and_time_panels( void )
{
	char date_str[ NUMBER_OF_CHARS_IN_A_NAME ];

	char time_str[ 8 ];

	// ----------

	// This function handles stop date and time display panels. The "GuiVar_LightIsEnergized" and 
	// "g_LIGHTS_TEST_irri_list_stop_time_in_seconds" variables need to be valid before arriving here.

	if( GuiVar_LightIsEnergized )
	{
		xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );

		// 9/22/2015 mpd : The current stop date and time may have changed. Get the latest values. 
		nm_LIGHTS_TEST_load_irri_list_stop_date_and_time();

		// 9/22/2015 mpd : Is the editable stop date and time different than the IRRI On list date and time? 
		if( ( g_LIGHTS_TEST_editable_stop_date != g_LIGHTS_TEST_irri_list_stop_date ) ||
			( g_LIGHTS_TEST_editable_stop_time_in_seconds != g_LIGHTS_TEST_irri_list_stop_time_in_seconds ) )
		{
			// 9/22/2015 mpd : This enables the "Set New Stop Time" button.
			GuiVar_LightsStopTimeHasBeenEdited = (true);
		}
		else
		{
			// 9/22/2015 mpd : The user has not modified the stop date or time. Hide the "Set New Stop Time" button.
			GuiVar_LightsStopTimeHasBeenEdited = (false);
		}

		xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );
	}
	else // The light is de-energized.
	{
		// 9/22/2015 mpd : The light is de-energized. Hide the "Set New Stop Time" button.
		GuiVar_LightsStopTimeHasBeenEdited = (false);
	}

	// 9/21/2015 mpd : This GuiVar string is displayed only when the light is energized. It is the unmodified stop date  
	// and time from the IRRI On list.
	snprintf( GuiVar_LightIrriListStop, 
			  sizeof( GuiVar_LightIrriListStop ),
			  "%s, %s", 
			  TDUTILS_time_to_time_string_with_ampm( time_str, sizeof(time_str), g_LIGHTS_TEST_irri_list_stop_time_in_seconds, TDUTILS_DONT_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ),
			  GetDateStr( date_str, sizeof( date_str ), g_LIGHTS_TEST_irri_list_stop_date, DATESTR_show_long_year, DATESTR_show_long_dow )  );

	// 9/21/2015 mpd : This GuiVar string is displayed when the light status is On or Off. It is the editable stop date  
	// and time that was initialized from the IRRI On list. Once initialized, it is under user control. It does not get 
	// reset if the IRRI On list value changes.
	snprintf( GuiVar_LightEditableStop, 
			  sizeof( GuiVar_LightEditableStop ),
			  "%s, %s", 
			  TDUTILS_time_to_time_string_with_ampm( time_str, sizeof(time_str), g_LIGHTS_TEST_editable_stop_time_in_seconds, TDUTILS_DONT_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ),
			  GetDateStr( date_str, sizeof( date_str ), g_LIGHTS_TEST_editable_stop_date, DATESTR_show_long_year, DATESTR_show_long_dow )   );

}

/* ---------------------------------------------------------- */
static void LIGHTS_TEST_process_manual_stop_date_and_time( const UNS_32 pkeycode )
{
	DATE_TIME ldt, temp_dt, min_dt, max_dt;

	// ----------

	// NOTE: The stop date and time for manual lights on the CS3000 is handled differently from the 2000e. The stop date 
	// and time are now visable and should be adjusted BEFORE turning on the light. In the 2000e, the values did not 
	// appear until the light was on. They could then be adjusted. The IRRI ON list is the reason for the change. 

	// This function handles display and modification of the manual stop date and time. A KEY_PLUS increases the date
	// and time by 10 minutes up to the limit. A KEY_MINUS decreases the date and time by 10 minutes with 10 minutes 
	// from now (ignoring seconds) being the minimum limit. Any other pkeycode value skips the modification step. 

	// If the light is ON, a late-in-development changes allow the user to view the current stop date and time from the 
	// IRRI On list. They can make changes to a local copy following the same restrictyions as the manual stop date and
	// time for a light that is OFF. When the current stop date and time is edited, a warning message is displayed 
	// indicating that the Override button will modify the current value. No change is made to the IRRI On list until 
	// they hit the Override button. This prevents every 10-minute adjustment from generating token responses (from the 
	// users box) followed by broadcast tokens.

	EPSON_obtain_latest_time_and_date( &ldt );

	// 8/24/2015 mpd : TBD: This function currently increments by 10 minutes. The 2000e did 1 minute. Adjust if 
	// necessary.

	// Make copies so we don't modify the existing values yet.
	temp_dt.D = g_LIGHTS_TEST_editable_stop_date;
	temp_dt.T = g_LIGHTS_TEST_editable_stop_time_in_seconds;

	if( pkeycode == KEY_PLUS )
	{
		// Test for the special case of incrementing at 11:50PM. 
		if( temp_dt.T < LIGHTS_TIME_IN_SECONDS_MAX )
		{

			temp_dt.T += LIGHTS_TEN_MINUTES_IN_SECONDS;
		}
		else
		{
			// Special case. Set the stop time to midnight the following day.
			temp_dt.T = LIGHTS_TIME_IN_SECONDS_MIN;
			temp_dt.D++;
		}

		// Put a reasonable limit on KEY_PLUS values. For now, use 24 hours.
		max_dt.D  = ( ldt.D + 1 );
		max_dt.T  = ldt.T;

		if( DT1_IsBiggerThanOrEqualTo_DT2( &temp_dt, &max_dt ) == (true) )
		{
			// The temp stop time is beyond the limit. Don't allow the change.
			bad_key_beep();
		}
		else
		{
			// The temp stop time is within the allowed limit. Use it.
			good_key_beep();
			g_LIGHTS_TEST_editable_stop_date = temp_dt.D;
			g_LIGHTS_TEST_editable_stop_time_in_seconds = temp_dt.T;
		}
	}
	else if( pkeycode == KEY_MINUS )
	{
		// Test for the special case of decrementing at 12:10AM. 
		if( temp_dt.T > LIGHTS_TEN_MINUTES_IN_SECONDS )
		{
			temp_dt.T -= LIGHTS_TEN_MINUTES_IN_SECONDS;
		}
		else
		{
			// Special case. Set the stop time to midnight the previous day.
			temp_dt.T = LIGHTS_TIME_IN_SECONDS_MIN;
			temp_dt.D--;
		}

		// Sodium lights have a minimum ON time. Make sure the OFF time is far enough in the future. 
		if( ldt.T < ( LIGHTS_24_HOURS_IN_SECONDS - LIGHTS_MINIMUM_MANUAL_ON_IN_SECONDS) )
		{
			// The minimum ON time ends today.
			min_dt.D = ldt.D;
			min_dt.T = ( ldt.T + LIGHTS_MINIMUM_MANUAL_ON_IN_SECONDS );
		}
		else
		{
			// Special case. The minimum ON time extends into tomorrow.
			min_dt.D = ( ldt.D + 1 );

			// This is a very rare case. Keep the math simple by ignoring the remaining minutes before midnight.
			min_dt.T = LIGHTS_MINIMUM_MANUAL_ON_IN_SECONDS;
		}

		// Test to be sure the temp stop time is in the future.
		if( DT1_IsBiggerThanOrEqualTo_DT2( &min_dt, &temp_dt ) == (true) )
		{
			// The temp stop time is not far enough in the future to cover the minimum ON time. Don't allow the change.
			bad_key_beep();
		}
		else
		{
			// The temp stop time is far enough in the future. Use it.
			good_key_beep();
			g_LIGHTS_TEST_editable_stop_date = temp_dt.D;
			g_LIGHTS_TEST_editable_stop_time_in_seconds = temp_dt.T;
		}
	}

	// 9/28/2015 ajv : Require a redraw because the panel which shows "Turn Off" may need to be
	// redrawn to show the "Save Changes" button as well.
	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
extern void FDTO_LIGHTS_TEST_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32  lcursor_to_select;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	// 9/21/2015 mpd : We base decisions and act on this light's energized state. Don't let it change!
	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );

	if( pcomplete_redraw )
	{
		nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars();

		// 9/21/2015 mpd : Load the stop data and time values for this light.
		nm_LIGHTS_TEST_load_editable_stop_date_and_time();

		lcursor_to_select = CP_LIGHTS_TEST_OUTPUT;
	}
	else
	{
		// The user hit a key. Not much to do here.
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	// Refresh the GuiVar strings. This also sets ON/OFF state of the displayed light.
	LIGHTS_copy_light_struct_into_guivars( LIGHTS_get_lights_array_index( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 ) );

	// This screen does not allow the user to edit the name. Display the long format ("Light 1 @ A: Left Field").
	LIGHTS_populate_group_name( LIGHTS_get_lights_array_index( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 ), (false) );

	// Update the stop date and time display strings. 
	nm_LIGHTS_TEST_update_stop_date_and_time_panels();

	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	GuiLib_ShowScreen( GuiStruct_scrLightsTest_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void FDTO_LIGHTS_TEST_update_screen( void )
{
	DATE_TIME	irri_list_stop_dt;

	BOOL_32		llight_was_energized;

	BOOL_32		llight_was_edited;

	// ----------

	// 9/28/2015 ajv : If the energized state changes, redraw the screen because a panel changes
	// when this occurs.
	llight_was_energized = GuiVar_LightIsEnergized;

	llight_was_edited = GuiVar_LightsStopTimeHasBeenEdited;

	// ----------

	// 9/21/2015 mpd : We base decisions and act on this light's energized state. Don't let it change!
	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );

	// Get the current status of this light from the IRRI side.
	if( ( GuiVar_LightIsEnergized = IRRI_LIGHTS_light_is_energized( LIGHTS_get_lights_array_index( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 ) ) == (true) ) )
	{
		// This light is ON. We need to know the OFF date and time recorded in the IRRI ON list.
		irri_list_stop_dt = IRRI_LIGHTS_get_light_stop_date_and_time( LIGHTS_get_lights_array_index( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 ) );

		g_LIGHTS_TEST_irri_list_stop_date = irri_list_stop_dt.D;
		g_LIGHTS_TEST_irri_list_stop_time_in_seconds = irri_list_stop_dt.T;

		// Update the stop date and time display strings. 
		nm_LIGHTS_TEST_update_stop_date_and_time_panels();
	}

	// 9/22/2015 mpd : TBD: It would be better if we could keep this mutex and let the redraw give it back. 
	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

	if( (llight_was_energized != GuiVar_LightIsEnergized) || (llight_was_edited != GuiVar_LightsStopTimeHasBeenEdited) )
	{
		// The "Will go Off at" panel appears and disappears based on the energized status which can change at any time. For
		// now, redraw the screen every second. 
		Redraw_Screen( (false) );
	}
	else
	{
		Refresh_Screen();
	}
}

/* ---------------------------------------------------------- */
extern void LIGHTS_TEST_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				// 6/24/2015 mpd : This button is a toggle.
				case CP_LIGHTS_TEST_ON_OFF:
					if( !COMM_MNGR_network_is_available_for_normal_use() )
					{
						bad_key_beep();

						// 10/22/2015 ajv : TODO Create new dialog specific for lights
						DIALOG_draw_ok_dialog( GuiStruct_dlgChainNotReady_Lights_0 );
					}
					else
					{
						if( GuiVar_LightIsEnergized )
						{
							LIGHTS_TEST_turn_light_on_or_off( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0, LIGHTS_TEST_TURN_OFF );
						}
						else // Turn it on
						{
							// 10/22/2015 ajv : We only check for a blown fuse when turning ON the light - allow the
							// user to turn it off regardless.
							if( (GuiVar_LightsBoxIndex_0 == FLOWSENSE_get_controller_index()) && (tpmicro_comm.fuse_blown == (true)) )
							{
								bad_key_beep();

								DIALOG_draw_ok_dialog( GuiStruct_dlgFuseBlown_Lights_0 );
							}
							else
							{
								LIGHTS_TEST_turn_light_on_or_off( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0, LIGHTS_TEST_TURN_ON );  
							}
						}
					}
					break;

				case CP_LIGHTS_TEST_SET_STOP_TIME:
					// 9/22/2015 mpd : Modifying the stop date and time can use the same code as turning On a light. The
					// IRRI side will find it on the ON list and adjust the stop fields. Because this request is from 
					// the IRRI side, FOAL_COMM sets the reason to "IN_LIST_FOR_MANUAL" and overrides whatever stop
					// values already existed.
					LIGHTS_TEST_turn_light_on_or_off( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0, LIGHTS_TEST_TURN_ON );
					break;

				default:
					bad_key_beep();
			}

			Redraw_Screen( (false) );
			break;

		case KEY_NEXT:
		case KEY_PREV:
			good_key_beep();

			xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

			// 9/21/2015 mpd : We base decisions and act on this light's energized state. Don't let it change!
			xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );

			if( pkey_event.keycode == KEY_NEXT )
			{
				nm_LIGHTS_get_next_available_light( &GuiVar_LightsBoxIndex_0, &GuiVar_LightsOutputIndex_0 );
			}
			else
			{
				nm_LIGHTS_get_prev_available_light( &GuiVar_LightsBoxIndex_0, &GuiVar_LightsOutputIndex_0 );
			}

			// Load the strings we need into the GuiVars. This also sets ON/OFF state of the displayed light.
			LIGHTS_copy_light_struct_into_guivars( LIGHTS_get_lights_array_index( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 ) );

			// 9/21/2015 mpd : Load the stop date and time values for this light.
			nm_LIGHTS_TEST_load_editable_stop_date_and_time();

			xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

			xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

			Redraw_Screen( (false) );
			break;

		case KEY_PLUS:
		case KEY_MINUS:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_LIGHTS_TEST_OUTPUT:
					good_key_beep();

					xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

					if( pkey_event.keycode == KEY_PLUS )
					{
						nm_LIGHTS_get_next_available_light( &GuiVar_LightsBoxIndex_0, &GuiVar_LightsOutputIndex_0 );
					}
					else
					{
						nm_LIGHTS_get_prev_available_light( &GuiVar_LightsBoxIndex_0, &GuiVar_LightsOutputIndex_0 );
					}

					// Load the strings we need into the GuiVars. This also sets ON/OFF state of the displayed light.
					LIGHTS_copy_light_struct_into_guivars( LIGHTS_get_lights_array_index( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 ) );

					// 9/21/2015 mpd : Load the stop date and time values for this light.
					nm_LIGHTS_TEST_load_editable_stop_date_and_time();

					xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

					Redraw_Screen( (false) );
					break;

				case CP_LIGHTS_TEST_STOP_TIME:
					LIGHTS_TEST_process_manual_stop_date_and_time( pkey_event.keycode );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_C_UP:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_LIGHTS_TEST_SET_STOP_TIME:
					CURSOR_Select( CP_LIGHTS_TEST_STOP_TIME, (true) );
					break;

				default:
					CURSOR_Up( (true) );
			}
			break;

		case KEY_C_DOWN:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_LIGHTS_TEST_ON_OFF:
					bad_key_beep();
					break;

				default:
					CURSOR_Down( (true) );
			}
			break;

		case KEY_C_LEFT:
			CURSOR_Up( (true) );
			break;

		case KEY_C_RIGHT:
			CURSOR_Down( (true) );
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;

			// No need to break here as this will allow it to fall into
			// the default case

		default:
			KEY_process_global_keys( pkey_event );
	}

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

