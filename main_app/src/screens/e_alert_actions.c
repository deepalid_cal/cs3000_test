/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_alert_actions.h"

#include	"app_startup.h"

#include	"combobox.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"group_base_file.h"

#include	"station_groups.h"

#include	"m_main.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_0		(0)
#define	CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_1		(1)
#define	CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_2		(2)
#define	CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_3		(3)
#define	CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_4		(4)
#define	CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_5		(5)
#define	CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_6		(6)
#define	CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_7		(7)
#define	CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_8		(8)
#define	CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_9		(9)
#define	CP_ALERT_ACTIONS_LOW_FLOW_ACTION_0		(10)
#define	CP_ALERT_ACTIONS_LOW_FLOW_ACTION_1		(11)
#define	CP_ALERT_ACTIONS_LOW_FLOW_ACTION_2		(12)
#define	CP_ALERT_ACTIONS_LOW_FLOW_ACTION_3		(13)
#define	CP_ALERT_ACTIONS_LOW_FLOW_ACTION_4		(14)
#define	CP_ALERT_ACTIONS_LOW_FLOW_ACTION_5		(15)
#define	CP_ALERT_ACTIONS_LOW_FLOW_ACTION_6		(16)
#define	CP_ALERT_ACTIONS_LOW_FLOW_ACTION_7		(17)
#define	CP_ALERT_ACTIONS_LOW_FLOW_ACTION_8		(18)
#define	CP_ALERT_ACTIONS_LOW_FLOW_ACTION_9		(19)

#define	ALERT_ACTIONS_COMBO_BOX_X_OFFSET		(94)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32 *g_ALERT_ACTIONS_combo_box_guivar;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Draws the alert actions dropdown window atop the main window.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the users presses SELECT to change the alert actions setting.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_ALERT_ACTIONS_show_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxAlertAction_0, &FDTO_COMBOBOX_add_items, (ALERT_ACTIONS_ALERT_SHUTOFF + 1), *g_ALERT_ACTIONS_combo_box_guivar );
}

/* ---------------------------------------------------------- */
extern void FDTO_ALERT_ACTIONS_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		ALERT_ACTIONS_copy_group_into_guivars();

		lcursor_to_select = 0;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrAlertActions_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while on the alert actions screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 10/12/2012 AdrianusV
 *
 * @revisions (none)
 */
extern void ALERT_ACTIONS_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_cbxAlertAction_0:
			COMBO_BOX_key_press( pkey_event.keycode, g_ALERT_ACTIONS_combo_box_guivar);
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_0:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingA_0;
							break;

						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_1:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingA_1;
							break;

						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_2:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingA_2;
							break;

						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_3:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingA_3;
							break;

						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_4:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingA_4;
							break;

						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_5:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingA_5;
							break;

						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_6:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingA_6;
							break;

						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_7:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingA_7;
							break;

						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_8:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingA_8;
							break;

						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_9:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingA_9;
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_0:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingB_0;
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_1:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingB_1;
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_2:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingB_2;
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_3:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingB_3;
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_4:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingB_4;
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_5:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingB_5;
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_6:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingB_6;
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_7:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingB_7;
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_8:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingB_8;
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_9:
							g_ALERT_ACTIONS_combo_box_guivar = &GuiVar_GroupSettingB_9;
							break;
					}

					if( g_ALERT_ACTIONS_combo_box_guivar != NULL )
					{
						good_key_beep();

						if( GuiLib_ActiveCursorFieldNo == CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_0 )
						{
							GuiVar_ComboBox_X1 = 0;
							GuiVar_ComboBox_Y1 = 0;
						}
						else if( GuiLib_ActiveCursorFieldNo == CP_ALERT_ACTIONS_LOW_FLOW_ACTION_0 )
						{
							GuiVar_ComboBox_X1 = ALERT_ACTIONS_COMBO_BOX_X_OFFSET;
							GuiVar_ComboBox_Y1 = 0;
						}
						else
						{
							GuiVar_ComboBox_X1 = (GuiLib_ActiveCursorFieldNo < 10) ? 0 : ALERT_ACTIONS_COMBO_BOX_X_OFFSET;
							GuiVar_ComboBox_Y1 = (UNS_32)((GuiLib_ActiveCursorFieldNo % 10) * Pos_Y_GroupSetting);
						}

						lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
						lde._04_func_ptr = (void*)&FDTO_ALERT_ACTIONS_show_dropdown;
						Display_Post_Command( &lde );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_0:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_0, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_1:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_1, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_2:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_2, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_3:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_3, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_4:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_4, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_5:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_5, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_6:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_6, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_7:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_7, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_8:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_8, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_9:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_9, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_0:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_0, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_1:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_1, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_2:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_2, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_3:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_3, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_4:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_4, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_5:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_5, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_6:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_6, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_7:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_7, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_8:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_8, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						case CP_ALERT_ACTIONS_LOW_FLOW_ACTION_9:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_9, ALERT_ACTIONS_NONE, ALERT_ACTIONS_ALERT_SHUTOFF, 1, (true) );
							break;

						default:
							bad_key_beep();
					}
					Redraw_Screen( (false) );
					break;

				case KEY_PREV:
				case KEY_C_UP:
					if( (GuiLib_ActiveCursorFieldNo % 10) == 0 )
					{
						bad_key_beep();
					}
					else
					{
						CURSOR_Up( (true) );
					}
					break;

				case KEY_NEXT:
				case KEY_C_DOWN:
					if( (UNS_32)(GuiLib_ActiveCursorFieldNo % 10) == (STATION_GROUP_get_num_groups_in_use() - 1) )
					{
						bad_key_beep();
					}
					else
					{
						CURSOR_Down( (true) );
					}
					break;

				case KEY_C_LEFT:
					if( (GuiLib_ActiveCursorFieldNo >= CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_0) && (GuiLib_ActiveCursorFieldNo <= CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_9) )
					{
						CURSOR_Select( (UNS_32)((GuiLib_ActiveCursorFieldNo + 10) - 1), (true) );
					}
					else if( (GuiLib_ActiveCursorFieldNo >= CP_ALERT_ACTIONS_LOW_FLOW_ACTION_0) && (GuiLib_ActiveCursorFieldNo <= CP_ALERT_ACTIONS_LOW_FLOW_ACTION_9) )
					{
						CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo - 10), (true) );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case KEY_C_RIGHT:
					if( (GuiLib_ActiveCursorFieldNo >= CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_0) && (GuiLib_ActiveCursorFieldNo <= CP_ALERT_ACTIONS_HIGH_FLOW_ACTION_9) )
					{
						CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo + 10), (true) );
					}
					else if( (GuiLib_ActiveCursorFieldNo >= CP_ALERT_ACTIONS_LOW_FLOW_ACTION_0) && (GuiLib_ActiveCursorFieldNo <= CP_ALERT_ACTIONS_LOW_FLOW_ACTION_9) )
					{
						CURSOR_Select( (UNS_32)((GuiLib_ActiveCursorFieldNo - 10) + 1), (true) );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case KEY_BACK:
					GuiVar_MenuScreenToShow = CP_MAIN_MENU_MAIN_LINES;
					ALERT_ACTIONS_extract_and_store_changes_from_GuiVars();
					// No need to break here to allow this to fall into the default
					// condition.

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

