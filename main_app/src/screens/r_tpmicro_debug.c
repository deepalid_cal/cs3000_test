/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"r_tpmicro_debug.h"

#include	"screen_utils.h"

#include	"tpmicro_data.h"





/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define TPMICRO_DEBUG_START_X	(60)

// 11/15/2012 rmd : Based on a regular line being 11 pixels high we are starting at about
// 2.5 line down from the top of the display.
#define TPMICRO_DEBUG_START_Y	(27)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Draws the TP Micro Debug report.
 * 
 * @executed Executed within the context of the DISPLAY PROCESSING task when the
 *  		 screen is initially drawn; within the context of the TD CHECK task
 *           as the screen is updated 4 times per second.
 * 
 * @author 4/19/2012 BobD
 *
 * @revisions
 *    4/19/2012  Initial release
 *    4/23/2012  Added the error record received from the TP Micro.
 */
extern void FDTO_TPMICRO_draw_debug( const BOOL_32 pcomplete_redraw )
{
	UNS_32	i;

	if( pcomplete_redraw == (true) )
	{
		GuiLib_ShowScreen( GuiStruct_rptTPMicroMsgs_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );
	}
	
	for( i = 0; i < TPMICRO_DEBUG_MAX_LINES_OF_TEXT; ++i )
	{
		GuiLib_DrawStr( (TPMICRO_DEBUG_START_X+2), (INT_16)(TPMICRO_DEBUG_START_Y + (i*(ANSI7_HEIGHT+1))), GuiFont_ANSI7, -1,
						(char*)(TPMICRO_debug_text[ i ]), GuiLib_ALIGN_LEFT,  GuiLib_PS_OFF,
						GuiLib_TRANSPARENT_OFF,  GuiLib_UNDERLINE_OFF,
						ANSI7_boxwidth( 40 ), 0, 0, 0,
						GuiConst_PIXEL_ON, GuiConst_PIXEL_OFF );
	}

	// ----------------------------
	
	// 4/23/2012 rmd : Show the error record rcvd from the tp micro.
	GuiLib_DrawStr( (TPMICRO_DEBUG_START_X+2), (INT_16)(TPMICRO_DEBUG_START_Y + ((TPMICRO_DEBUG_MAX_LINES_OF_TEXT+1) * (ANSI7_HEIGHT+1))), GuiFont_ANSI7, -1,
					"~~~~####~~~~####~~~~####~~~~####", GuiLib_ALIGN_LEFT,  GuiLib_PS_OFF,
					GuiLib_TRANSPARENT_OFF,  GuiLib_UNDERLINE_OFF,
					ANSI7_boxwidth( 40 ), 0, 0, 0,
					GuiConst_PIXEL_ON, GuiConst_PIXEL_OFF );

	UNS_8	ltext[ 64 ];
	
	format_binary_32( (char*)ltext, &(tpmicro_data.rcvd_errors) );
	
	GuiLib_DrawStr( (TPMICRO_DEBUG_START_X+2), (INT_16)(TPMICRO_DEBUG_START_Y + ((TPMICRO_DEBUG_MAX_LINES_OF_TEXT+2) * (ANSI7_HEIGHT+1))), GuiFont_ANSI7, -1,
					(char*)(&ltext), GuiLib_ALIGN_LEFT,  GuiLib_PS_OFF,
					GuiLib_TRANSPARENT_OFF,  GuiLib_UNDERLINE_OFF,
					ANSI7_boxwidth( 40 ), 0, 0, 0,
					GuiConst_PIXEL_ON, GuiConst_PIXEL_OFF );

	// ----------------------------

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

