/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_E_LINE_FILL_TIME_H
#define _INC_E_LINE_FILL_TIME_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FDTO_LINE_FILL_TIME_draw_screen( const BOOL_32 pcomplete_redraw );

extern void LINE_FILL_TIME_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

