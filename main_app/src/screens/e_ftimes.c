/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/6/2018 Ryan : Required for strlcpy
#include	<string.h>

#include <stdio.h>

#include <stdlib.h>

#include	"e_ftimes.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"change.h"

#include	"configuration_controller.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"group_base_file.h"

#include	"scrollbox.h"

#include	"speaker.h"

#include	"report_utils.h"

#include	"e_programs.h"

#include	"ftimes_task.h"

#include	"ftimes_funcs.h"

#include	"cs_mem.h"

// ----------

#define	FTIME_AMOUNT_TO_SCROLL_HORIZONTALLY		(20)

// 7/12/2018 Ryan : Different pixels to scroll horizontally the screen can be set to,
// depending on the notes.
#define	FTIME_MAX_PIXELS_TO_SCROLL_HORIZONTALLY_no_note					(40)
#define	FTIME_MAX_PIXELS_TO_SCROLL_HORIZONTALLY_stoptime_note			(260)
#define	FTIME_MAX_PIXELS_TO_SCROLL_HORIZONTALLY_manual_program_note		(280)
#define	FTIME_MAX_PIXELS_TO_SCROLL_HORIZONTALLY_full_note				(460)

// 7/12/2018 Ryan : States to control the variable structure of the horizontal cursor.
#define	FTIME_HORIZONTAl_CURSOR_no_note									(3)
#define	FTIME_HORIZONTAl_CURSOR_manual_program_note						(2)
#define	FTIME_HORIZONTAl_CURSOR_stoptime_note	    					(1)
#define	FTIME_HORIZONTAl_CURSOR_full_note								(0)

// 7/12/2018 Ryan : Used by the dialog box when finish times are calculating.
#define	FTIME_CALC_PROGRESS_BAR_MIN										(0)
#define	FTIME_CALC_PROGRESS_BAR_MAX										(200)

#define	FTIME_CALC_in_progress											(true)
#define	FTIME_CALC_not_in_progress										(false)

// ----------

UNS_32	FINISH_TIME_calc_start_timestamp;			 

UNS_32	FINISH_TIME_scrollbox_index;

// ----------

static UNS_32	ft_mainline_index;

static UNS_32	ft_station_group_count;

static BOOL_32	ft_dialog_up;

static UNS_32	ft_max_pixels_to_scroll_horizontally;

static UNS_32	ft_current_line_GID;

static UNS_32	ft_scrollbox_top_line;

/* ---------------------------------------------------------- */
/**
 * Draws dialog box for the finish times calculations.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the FDTO_FINISH_TIMES_draw_screen, when
 * the finish time calculation is underway when the user enters the screen.
 *
 * @author 7/2/18 Ryan
 *
 * @revisions (none)
 */
extern void FINISH_TIMES_draw_dialog( void )
{
	// 7/5/2018 Ryan : If dialog box is opened before the calculation starts, this will fill in
	// the approprate default value, otherwise it should just display the current percet
	// complete.
	if( ftcs.mode == FTIMES_MODE_calculating )
	{
		GuiVar_FinishTimesCalcPercentComplete = ftcs.percent_complete;
		
		GuiVar_FinishTimesCalcProgressBar = ( ftcs.percent_complete * 2 );
	}
	else
	{
		GuiVar_FinishTimesCalcProgressBar = FTIME_CALC_PROGRESS_BAR_MIN;
		
		GuiVar_FinishTimesCalcPercentComplete = ftcs.percent_complete;
	}
	
	DIALOG_draw_ok_dialog( GuiStruct_dlgFinishTimesCalc_0 );
}

/* ---------------------------------------------------------- */
/**
 * Refreshes dialog box for the finish times calculations.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the td_check.c when the dialog box for finish times
 * calculations is active
 *
 * @author 7/2/18 Ryan
 *
 * @revisions (none)
 */
extern void FINISH_TIMES_update_dialog( void )
{
	// 7/6/2018 Ryan : Either update the dialog box, or close it if the calculations are
	// complete.
	if( ftcs.mode == FTIMES_MODE_calculating )
	{
		GuiVar_SpinnerPos = ((GuiVar_SpinnerPos + 1) % 4);
			
		GuiVar_FinishTimesCalcPercentComplete = ftcs.percent_complete;
		
		GuiVar_FinishTimesCalcProgressBar = ( GuiVar_FinishTimesCalcPercentComplete * 2 );
			
		FDTO_DIALOG_redraw_ok_dialog();
	}
	else if( ftcs.mode == FTIMES_MODE_results_available )
	{
		GuiVar_FinishTimesCalcPercentComplete = 100;
	
		GuiVar_FinishTimesCalcProgressBar = FTIME_CALC_PROGRESS_BAR_MAX;
		
		DIALOG_close_ok_dialog();
		
		FDTO_FINISH_TIMES_draw_screen( (true) );
	}
}

/* ---------------------------------------------------------- */
/**
 * Convert DATE_TIME to format used on finish times screen.
 * 
 * @mutex_requirements none
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the FINISH_TIMES_draw_scroll_line function
 * when displaying the finish time and start time on the screen.
 *
 * @param pdest target for conversion, pdest_size size of target, pdate_time the DATE_TIME
 * being converted.
 *
 * @author 7/30/18 Ryan
 *
 * @revisions (none)
 */
static char *DATE_TIME_to_Finish_Times_format( char *const pdest, const UNS_32 pdest_size, const DATE_TIME pdate_time )
{
	char t_str_32[ 32 ];

	UNS_32	lday, lmonth, lyear, ldow;
	
	DateToDMY( pdate_time.D, &lday, &lmonth, &lyear, &ldow );

	// We use ShaveLeftPad on the time piece cause we want to pack the string up
	// when there is only 1 digit for the hours.
	snprintf( pdest, pdest_size, "%s on %s, %s %d", ShaveLeftPad( t_str_32, TDUTILS_time_to_time_string_with_ampm( t_str_32, sizeof(t_str_32), pdate_time.T, TDUTILS_DONT_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ) ), GetDayShortStr( ldow ), GetMonthShortStr( lmonth - 1 ), lday );

	return( pdest );
}

/* ---------------------------------------------------------- */
/**
 * Draw a line of the scroll box on the finish times screen.
 * 
 * @mutex_requirements list_program_data_recursive_MUTEX
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the FDTO_FINISH_TIMES_draw_screen to get
 * the line count and horizonal screen width as well as drawing the scroll line. Also called
 * in the FINISH_TIMES_process_screen function to set ft_current_line_GID to the correct
 * value.
 *
 * @param pline_index_0_i16 The line currently being drawn.
 *
 * @author 7/2/18 Ryan
 *
 * @revisions (none)
 */
static void FINISH_TIMES_draw_scroll_line( const INT_16 pline_index_0_i16 )
{	
	FT_STATION_GROUP_STRUCT	*ft_group;
		
	UNS_32	i;
	
	UNS_32	time_exceed_finish_time;
		
	char	unit_of_time[4];
	
	INT_16	group_index_for_line;

	// ----------
	
	group_index_for_line = pline_index_0_i16;
	
	ft_station_group_count = 0;
	
	// ----------
	
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );	
	
	ft_group = nm_ListGetFirst( &ft_station_groups_list_hdr );
	
	for( i = 0; i < nm_ListGetCount( &ft_station_groups_list_hdr ); ++i )
	{
		// 6/21/2018 Ryan : Determine if the station group is on this mainline, and if it is
		// determine what order it should be displayed in relative to the other station groups on
		// the mainline.
		if( SYSTEM_get_index_for_group_with_this_GID( ft_group->GID_irrigation_system ) == ft_mainline_index )
		{
			// 7/10/2018 Ryan : get a count of the number of lines in the scrollbox.
			ft_station_group_count++;

			// 7/10/2018 Ryan : set horizontal width of screen. Screen should be as long as the longest
			// line requires.
			// 7/12/2018 Ryan : only make note about stop time if finish time exceeds the stop time by
			// at least one second.
			if( ( ft_group->results_hit_the_stop_time ) && ( ft_group->results_exceeded_stop_time_by_seconds > 0 ) )
			{
				if( ft_group->results_manual_programs_and_programmed_irrigation_clashed )
				{
					ft_max_pixels_to_scroll_horizontally = FTIME_MAX_PIXELS_TO_SCROLL_HORIZONTALLY_full_note;
					GuiVar_FinishTimesScrollBoxH = FTIME_HORIZONTAl_CURSOR_full_note;
				}
				else
				{
					if( ft_max_pixels_to_scroll_horizontally < FTIME_MAX_PIXELS_TO_SCROLL_HORIZONTALLY_stoptime_note )
					{
						ft_max_pixels_to_scroll_horizontally = FTIME_MAX_PIXELS_TO_SCROLL_HORIZONTALLY_stoptime_note;
						GuiVar_FinishTimesScrollBoxH = FTIME_HORIZONTAl_CURSOR_stoptime_note;
					}
				}
			}
			else if( ft_group->results_manual_programs_and_programmed_irrigation_clashed )
			{
				if( ft_max_pixels_to_scroll_horizontally < FTIME_MAX_PIXELS_TO_SCROLL_HORIZONTALLY_manual_program_note )
				{
					ft_max_pixels_to_scroll_horizontally = FTIME_MAX_PIXELS_TO_SCROLL_HORIZONTALLY_manual_program_note;
					GuiVar_FinishTimesScrollBoxH = FTIME_HORIZONTAl_CURSOR_manual_program_note;
				}
			}
			
			// 7/5/2018 Ryan : Should only display information If calculations are finished
			if( ft_dialog_up == FTIME_CALC_not_in_progress )
			{
				if (  STATION_GROUP_get_index_for_group_with_this_GID( ft_group->group_identity_number ) == group_index_for_line  )
				{
					ft_current_line_GID = ft_group->group_identity_number;				

					strlcpy( GuiVar_FinishTimesStationGroup, nm_GROUP_get_name( STATION_GROUP_get_group_with_this_GID( ft_group->group_identity_number ) ), sizeof(GuiVar_FinishTimesStationGroup) );

					// 7/5/2018 Ryan : Don't display default values for run time and start time if station group
					// does not run.
					if( ft_group->results_latest_finish_date_and_time.D == DATE_MINIMUM )
					{
						snprintf( GuiVar_FinishTimes, sizeof(GuiVar_FinishTimes), "Does not Run" );
						snprintf( GuiVar_FinishTimesStartTime, sizeof(GuiVar_FinishTimesStartTime), "" );
					}
					else
					{
						DATE_TIME_to_Finish_Times_format( GuiVar_FinishTimes, sizeof(GuiVar_FinishTimes), ft_group->results_latest_finish_date_and_time );
						DATE_TIME_to_Finish_Times_format( GuiVar_FinishTimesStartTime, sizeof(GuiVar_FinishTimesStartTime), ft_group->results_start_date_and_time_associated_with_the_latest_finish_date_and_time );
					}
					
					// 7/5/2018 Ryan : Change note depending on if finish time surpassed and manual programming
					// interfered or not.
					// 7/12/2018 Ryan : only make note about stop time if finish time exceeds the stop time by
					// at least one second.
					if(( ft_group->results_hit_the_stop_time == (true) ) && ( ft_group->results_exceeded_stop_time_by_seconds > 0 ) )
					{
						// 7/9/2018 Ryan : Determine what units should be used to show the amount of time the finish
						// time exceeded the start time by.
						if( ft_group->results_exceeded_stop_time_by_seconds / 3600 )
						{
							time_exceed_finish_time = ( ( ft_group->results_exceeded_stop_time_by_seconds + 1800 ) / 3600 );
							snprintf( unit_of_time, sizeof(unit_of_time), "hrs" );
						}
						else if( ft_group->results_exceeded_stop_time_by_seconds / 60 )
						{
							time_exceed_finish_time = ( ( ft_group->results_exceeded_stop_time_by_seconds + 30 ) / 60 );
							snprintf( unit_of_time, sizeof(unit_of_time), "min" );
						}
						else
						{
							time_exceed_finish_time = ft_group->results_exceeded_stop_time_by_seconds;
							snprintf( unit_of_time, sizeof(unit_of_time), "sec" );
						}
						
						if( ft_group->results_manual_programs_and_programmed_irrigation_clashed == (true) )
						{
							snprintf( GuiVar_FinishTimesNotes, sizeof(GuiVar_FinishTimesNotes), "(manual programming started during irrigation and finish time exceeds stop time by %d %s)", time_exceed_finish_time, unit_of_time );
						}
						else
						{
							snprintf( GuiVar_FinishTimesNotes, sizeof(GuiVar_FinishTimesNotes), "(finish time exceeds stop time by %d %s)", time_exceed_finish_time, unit_of_time );
						}
					}
					else if( ft_group->results_manual_programs_and_programmed_irrigation_clashed == (true) )
					{
						snprintf( GuiVar_FinishTimesNotes, sizeof(GuiVar_FinishTimesNotes), "(manual programming started during irrigation)" );
					}
					else
					{
						snprintf( GuiVar_FinishTimesNotes, sizeof(GuiVar_FinishTimesNotes), "" );	
					}
				}
			}
			else
			{
				snprintf( GuiVar_FinishTimesStationGroup, sizeof(GuiVar_FinishTimesStationGroup), "" );

				snprintf( GuiVar_FinishTimes, sizeof(GuiVar_FinishTimes), "" );
				
				snprintf( GuiVar_FinishTimesNotes, sizeof(GuiVar_FinishTimesNotes), "" );
				
				snprintf( GuiVar_FinishTimesStartTime, sizeof(GuiVar_FinishTimesStartTime), "" );
			}
		}
		// 8/2/2018 Ryan : For controllers with more than one mainline, where the screen will not be
		// the exact order of the station order index.
		else if( STATION_GROUP_get_index_for_group_with_this_GID( ft_group->group_identity_number ) == group_index_for_line )
		{
			group_index_for_line++;
		}
		
		ft_group = nm_ListGetNext( &ft_station_groups_list_hdr, ft_group );		
	} // closing for loop for ft_group going through finish times station group list.
	
	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
/**
 * Draw the finish times screen.
 * 
 * @mutex_requirements list_program_data_recursive_MUTEX
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the m_menu.c when the user enters the
 * finish times screen and in the FINISH_TIMES_update_dialog function when closing the
 * dialog box.
 *
 * @param pcomplete_redraw set when the screen needs to be completely redrawn
 *
 * @author 7/2/18 Ryan
 *
 * @revisions (none)
 */
extern void FDTO_FINISH_TIMES_draw_screen( const BOOL_32 pcomplete_redraw )
{  
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( pcomplete_redraw == ( true ) )
	{
		if( ft_mainline_index == NULL )
		{
			ft_mainline_index = 0;
		}

		if( FINISH_TIME_scrollbox_index == NULL )
		{
			FINISH_TIME_scrollbox_index = 0;
		}
		
		ft_dialog_up = FTIME_CALC_not_in_progress;
		
		// 7/10/2018 Ryan : set defaults for screen size;
		ft_max_pixels_to_scroll_horizontally = FTIME_MAX_PIXELS_TO_SCROLL_HORIZONTALLY_no_note;
		GuiVar_FinishTimesScrollBoxH = FTIME_HORIZONTAl_CURSOR_no_note;
		
	}

	// 7/9/2018 Ryan : Call the scroll line function once to get the scroll line count and
	// horizonal screen size.
	FINISH_TIMES_draw_scroll_line(0);

	// 7/2/2018 Ryan : If there is only 1 mainline, a slightly simplier screen shows up.
	if( SYSTEM_num_systems_in_use() > 1 )
	{
		GuiLib_ShowScreen( GuiStruct_rptFinishTimes_1, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW);
		
		// 7/11/2018 Ryan : only need to set GuiVar if more than one mainline on controller.
		snprintf( GuiVar_FinishTimesSystemGroup, sizeof(GuiVar_FinishTimesSystemGroup), "Mainline: %s", nm_GROUP_get_name( SYSTEM_get_group_at_this_index( ft_mainline_index ) )  );		
	}
	else
	{
		GuiLib_ShowScreen( GuiStruct_rptFinishTimes_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW);
	}
	
	// 7/24/2018 Ryan : For cases where an empty mainline is assigned a station group while
	// controller is on the screen.
	if( ft_station_group_count > 0 && FINISH_TIME_scrollbox_index == GuiLib_NO_CURSOR )
	{
		FINISH_TIME_scrollbox_index = 0;
	}
	
	// 6/27/2018 Ryan : If calculations are running when entering the screen, have the dialog
	// box draw, but only once, as to not overwrite a currently running dialog box.
	if( ftcs.mode != FTIMES_MODE_results_available )
	{
		if( ft_dialog_up == FTIME_CALC_not_in_progress )
		{
			ft_dialog_up = FTIME_CALC_in_progress;
			FINISH_TIMES_draw_dialog();
		}
	}

	// 7/19/2018 Ryan : Set timestamp for when calculation last occured, so that if it changes
	// we have something to compare it to.
	FINISH_TIME_calc_start_timestamp = ftcs.duration_start_time_stamp;

	// 7/11/2018 Ryan : Variable for devolopment use. Should be removed before release.
	GuiVar_FinishTimesCalcTime = ftcs.duration_calculation_float_seconds;
	
	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	
	// 7/10/2018 Ryan : Slightly moddified copy of FDTO_REPORTS_draw_report function that allows
	// the scroll box index to be preserved when going to the start times and water days screen.
	GuiLib_ScrollBox_Close( 0 );

	if( ( pcomplete_redraw == (true) ) && ( FINISH_TIME_scrollbox_index == 0 ) )
	{
		GuiVar_ScrollBoxHorizScrollPos = 0;

		ft_scrollbox_top_line = 0;

		if( ft_station_group_count == 0 )
		{
			GuiLib_ScrollBox_Init( 0, &FINISH_TIMES_draw_scroll_line, ft_station_group_count, GuiLib_NO_CURSOR );
		}
		else
		{
			GuiLib_ScrollBox_Init( 0, &FINISH_TIMES_draw_scroll_line, ft_station_group_count, ft_scrollbox_top_line );
		}
	}
	else
	{
		GuiLib_ScrollBox_Init_Custom_SetTopLine( 0, &FINISH_TIMES_draw_scroll_line, ft_station_group_count, FINISH_TIME_scrollbox_index, ft_scrollbox_top_line );
	}

	GuiLib_Refresh();
	
}

/* ---------------------------------------------------------- */
/**
 * Redraws the scroll box on the finish times screen.
 *
 * @executed Executed within the context of the display processing task when the
 *  		 user changes mainlines
 * 
 * @author 7/10/2018 Ryan
 *
 * @revisions (none)
 */
static void FDTO_FINISH_TIMES_redraw_scrollbox( void )
{
	// 7/9/2018 Ryan : reset the scroll box horizontal position to zero. As the horizonal max is
	// variable, if left the same the cursor could begin out of bounds.
	GuiVar_ScrollBoxHorizScrollPos = 0;
	GuiVar_ScrollBoxHorizScrollMarker = 0;
	
	// 7/10/2018 Ryan : Set current active scrollbox line to zero so active line does not
	// dissapear when returning from an empty mainline.
	FINISH_TIME_scrollbox_index = 0;
	
	// 7/10/2018 Ryan : set defaults for screen size;
	ft_max_pixels_to_scroll_horizontally = FTIME_MAX_PIXELS_TO_SCROLL_HORIZONTALLY_no_note;
	GuiVar_FinishTimesScrollBoxH = FTIME_HORIZONTAl_CURSOR_no_note;
	
	
	// 7/19/2018 Ryan : set timestamp for calculations.
	FINISH_TIME_calc_start_timestamp = ftcs.duration_start_time_stamp;
	
	// 7/10/2018 Ryan : set count and horizonal width for new mainline.
	FINISH_TIMES_draw_scroll_line(0);

	FDTO_SCROLL_BOX_redraw_retaining_topline( 0, ft_station_group_count, (true) );
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed on the Finish Times menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 6/20/2018 Ryan
 *
 * @revisions (none)
 */
extern void FINISH_TIMES_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{   

	DISPLAY_EVENT_STRUCT	lde;

	UNS_32	lkey;
	
	switch( pkey_event.keycode )
	{			
		case KEY_HELP:
			// 7/5/2018 rmd : A temporary way to start (or re-start if mid-calculation) the calculation.

			FTIMES_TASK_restart_calculation();

			good_key_beep();

			break;

		case KEY_SELECT:
			
			// 7/10/2018 Ryan : Set current scrollbox line for return from other screen.
			FINISH_TIME_scrollbox_index = GuiLib_ScrollBox_GetActiveLine( 0, 0 );
			
			// 7/9/2018 Ryan : Set index of start times and water days to the correct station group.
			FINISH_TIMES_draw_scroll_line( FINISH_TIME_scrollbox_index );
			g_GROUP_list_item_index = STATION_GROUP_get_index_for_group_with_this_GID( ft_current_line_GID );
			
			// 7/11/2018 Ryan : Change to the start times and water days screen.
			lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
			lde._03_structure_to_draw = GuiStruct_scrStartTimesAndWaterDays_0;
			lde._04_func_ptr = (void *)&FDTO_SCHEDULE_draw_menu;
			lde._06_u32_argument1 = (true);
			lde._08_screen_to_draw = CP_SCHEDULE_MENU_PROGRAMS;
			lde.key_process_func_ptr = SCHEDULE_process_menu;
			Change_Screen(&lde);
			Redraw_Screen(false);
			break;
						
		case KEY_NEXT:
		case KEY_PREV:
			// 7/11/2018 Ryan : Select correct mainline and redraw screen to display it.
			if( SYSTEM_num_systems_in_use() > 1 )
			{
				if( pkey_event.keycode == KEY_NEXT )
				{
					lkey = KEY_PLUS;
				}
				else
				{
					lkey = KEY_MINUS;
				}
				
				process_uns32( lkey, &ft_mainline_index, 0, (SYSTEM_num_systems_in_use() - 1), 1, (true) );
				good_key_beep();
				
				lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
				lde._04_func_ptr = (void*)&FDTO_FINISH_TIMES_redraw_scrollbox;
				Display_Post_Command( &lde );		
			}
			else
			{
				bad_key_beep();
			}
			
			break;

		default:
			
			// 7/10/2018 Ryan : Set current active scrollbox line.
			FINISH_TIME_scrollbox_index = GuiLib_ScrollBox_GetActiveLine( 0, 0 );
		
			REPORTS_process_report( pkey_event, FTIME_AMOUNT_TO_SCROLL_HORIZONTALLY, ft_max_pixels_to_scroll_horizontally );		
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
