/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_mvor.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"group_base_file.h"

#include	"irri_comm.h"

#include	"irrigation_system.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_MVOR_RUN_TIME						(0)
#define CP_MVOR_OPEN_BUTTON						(1)
#define CP_MVOR_CLOSE_BUTTON					(2)
#define CP_MVOR_CANCEL_BUTTON					(3)
#define CP_MVOR_SCHEDULE_DAY_0_OPEN				(4)
#define CP_MVOR_SCHEDULE_DAY_0_CLOSE			(5)
#define CP_MVOR_SCHEDULE_DAY_1_OPEN				(6)
#define CP_MVOR_SCHEDULE_DAY_1_CLOSE			(7)
#define CP_MVOR_SCHEDULE_DAY_2_OPEN				(8)
#define CP_MVOR_SCHEDULE_DAY_2_CLOSE			(9)
#define CP_MVOR_SCHEDULE_DAY_3_OPEN				(10)
#define CP_MVOR_SCHEDULE_DAY_3_CLOSE			(11)
#define CP_MVOR_SCHEDULE_DAY_4_OPEN				(12)
#define CP_MVOR_SCHEDULE_DAY_4_CLOSE			(13)
#define CP_MVOR_SCHEDULE_DAY_5_OPEN				(14)
#define CP_MVOR_SCHEDULE_DAY_5_CLOSE			(15)
#define CP_MVOR_SCHEDULE_DAY_6_OPEN				(16)
#define CP_MVOR_SCHEDULE_DAY_6_CLOSE			(17)

// ----------

#define MVOR_RUN_TIME_MIN (0.1F)
#define MVOR_RUN_TIME_MAX (48.0F)

// ----------

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static BOOL_32	g_MVOR_editing_group;

static UNS_32	g_MVOR_prev_cursor_pos;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void FDTO_MVOR_return_to_menu( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void MVOR_process_start_time( const UNS_32 pkeycode, UNS_32 *popen_time_ptr, UNS_32 *popen_time_enabled_ptr, UNS_32 *pclose_time_ptr )
{
	process_uns32( pkeycode, popen_time_ptr, (IRRIGATION_SYSTEM_MVOR_OPEN_TIME_MIN / 60), (IRRIGATION_SYSTEM_MVOR_OPEN_TIME_MAX / 60), 10, (true) );

	*popen_time_enabled_ptr = (*popen_time_ptr != (SCHEDULE_OFF_TIME / 60));

	if( (*popen_time_ptr != (SCHEDULE_OFF_TIME / 60)) && (*popen_time_ptr == *pclose_time_ptr) )
	{
		// 8/26/2015 ajv : TODO Show a warning that the open and close times are the same?
	}
	else if( (*popen_time_ptr != (SCHEDULE_OFF_TIME / 60)) && (*pclose_time_ptr == (SCHEDULE_OFF_TIME / 60)) )
	{
		// 8/26/2015 ajv : TODO Show a warning that there's no close time?
	}

	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
extern void FDTO_MVOR_update_screen( void )
{
	// 8/31/2015 ajv : When functions are declared as static, the compiler may inline the
	// functions and optimize out floating-point constant literals from the calculations. This
	// can result in a division-by-0 error. In order to resolve this, we declare the constant
	// literal as volatile which tells the compile to not optimize the value.
	static const volatile float SECONDS_PER_HOUR = 3600.0F;

	// ----------
	
	BY_SYSTEM_RECORD	*lpreserve;

	BOOL_32		lprev_in_effect_value;

	// ----------

	lprev_in_effect_value = GuiVar_MVORInEffect;

	// ----------

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	lpreserve = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( g_GROUP_ID );

	if( lpreserve != NULL )
	{
		GuiVar_MVORInEffect = (lpreserve->sbf.delivered_MVOR_in_effect_opened || lpreserve->sbf.delivered_MVOR_in_effect_closed);

		GuiVar_MVORState = lpreserve->sbf.delivered_MVOR_in_effect_closed;

		GuiVar_MVORTimeRemaining = (lpreserve->delivered_MVOR_remaining_seconds / SECONDS_PER_HOUR);
	}
	else
	{
		GuiVar_MVORInEffect = (false);

		GuiVar_MVORState = 0;

		GuiVar_MVORTimeRemaining = 0.0F;
	}

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	// ----------

	// 8/31/2015 ajv : Only redraw the screen if a MVOR was in effect and now isn't or vice
	// versa.
	if( lprev_in_effect_value != GuiVar_MVORInEffect )
	{
		FDTO_Redraw_Screen( (false) );
	}
	else
	{
		GuiLib_Refresh();
	}
}

/* ---------------------------------------------------------- */
static void MVOR_process_group( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_MVOR_OPEN_BUTTON:
				case CP_MVOR_CLOSE_BUTTON:
				case CP_MVOR_CANCEL_BUTTON:
					good_key_beep();

					if( GuiLib_ActiveCursorFieldNo == CP_MVOR_CLOSE_BUTTON )
					{
						irri_comm.mvor_request.mvor_action_to_take = MVOR_ACTION_CLOSE_MASTER_VALVE;
					}
					else if( GuiLib_ActiveCursorFieldNo == CP_MVOR_CANCEL_BUTTON )
					{
						irri_comm.mvor_request.mvor_action_to_take = MVOR_ACTION_CANCEL_MASTER_VALVE_OVERRIDE;
					}
					else
					{
						irri_comm.mvor_request.mvor_action_to_take = MVOR_ACTION_OPEN_MASTER_VALVE;
					}
					
					irri_comm.mvor_request.mvor_seconds = (GuiVar_MVORRunTime * 60 * 60);
					irri_comm.mvor_request.initiated_by = INITIATED_VIA_KEYPAD;
					irri_comm.mvor_request.system_gid = g_GROUP_ID;

					irri_comm.mvor_request.in_use = (true);
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_MINUS:
		case KEY_PLUS:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_MVOR_RUN_TIME:
					process_fl( pkey_event.keycode, &GuiVar_MVORRunTime, MVOR_RUN_TIME_MIN, MVOR_RUN_TIME_MAX, 0.1F, (false) );
					break;

				case CP_MVOR_SCHEDULE_DAY_0_OPEN:
					MVOR_process_start_time( pkey_event.keycode, &GuiVar_MVOROpen0, &GuiVar_MVOROpen0Enabled, &GuiVar_MVORClose0 );
					break;

				case CP_MVOR_SCHEDULE_DAY_1_OPEN:
					MVOR_process_start_time( pkey_event.keycode, &GuiVar_MVOROpen1, &GuiVar_MVOROpen1Enabled, &GuiVar_MVORClose1 );
					break;

				case CP_MVOR_SCHEDULE_DAY_2_OPEN:
					MVOR_process_start_time( pkey_event.keycode, &GuiVar_MVOROpen2, &GuiVar_MVOROpen2Enabled, &GuiVar_MVORClose2 );
					break;

				case CP_MVOR_SCHEDULE_DAY_3_OPEN:
					MVOR_process_start_time( pkey_event.keycode, &GuiVar_MVOROpen3, &GuiVar_MVOROpen3Enabled, &GuiVar_MVORClose3 );
					break;

				case CP_MVOR_SCHEDULE_DAY_4_OPEN:
					MVOR_process_start_time( pkey_event.keycode, &GuiVar_MVOROpen4, &GuiVar_MVOROpen4Enabled, &GuiVar_MVORClose4 );
					break;

				case CP_MVOR_SCHEDULE_DAY_5_OPEN:
					MVOR_process_start_time( pkey_event.keycode, &GuiVar_MVOROpen5, &GuiVar_MVOROpen5Enabled, &GuiVar_MVORClose5 );
					break;

				case CP_MVOR_SCHEDULE_DAY_6_OPEN:
					MVOR_process_start_time( pkey_event.keycode, &GuiVar_MVOROpen6, &GuiVar_MVOROpen6Enabled, &GuiVar_MVORClose6 );
					break;

				case CP_MVOR_SCHEDULE_DAY_0_CLOSE:
					MVOR_process_start_time( pkey_event.keycode, &GuiVar_MVORClose0, &GuiVar_MVORClose0Enabled, &GuiVar_MVOROpen0 );
					break;

				case CP_MVOR_SCHEDULE_DAY_1_CLOSE:
					MVOR_process_start_time( pkey_event.keycode, &GuiVar_MVORClose1, &GuiVar_MVORClose1Enabled, &GuiVar_MVOROpen1 );
					break;

				case CP_MVOR_SCHEDULE_DAY_2_CLOSE:
					MVOR_process_start_time( pkey_event.keycode, &GuiVar_MVORClose2, &GuiVar_MVORClose2Enabled, &GuiVar_MVOROpen2 );
					break;

				case CP_MVOR_SCHEDULE_DAY_3_CLOSE:
					MVOR_process_start_time( pkey_event.keycode, &GuiVar_MVORClose3, &GuiVar_MVORClose3Enabled, &GuiVar_MVOROpen3 );
					break;

				case CP_MVOR_SCHEDULE_DAY_4_CLOSE:
					MVOR_process_start_time( pkey_event.keycode, &GuiVar_MVORClose4, &GuiVar_MVORClose4Enabled, &GuiVar_MVOROpen4 );
					break;

				case CP_MVOR_SCHEDULE_DAY_5_CLOSE:
					MVOR_process_start_time( pkey_event.keycode, &GuiVar_MVORClose5, &GuiVar_MVORClose5Enabled, &GuiVar_MVOROpen5 );
					break;

				case CP_MVOR_SCHEDULE_DAY_6_CLOSE:
					MVOR_process_start_time( pkey_event.keycode, &GuiVar_MVORClose6, &GuiVar_MVORClose6Enabled, &GuiVar_MVOROpen6);
					break;

				default:
					bad_key_beep();
			}
			Refresh_Screen();
			break;

		case KEY_NEXT:
		case KEY_PREV:
			GROUP_process_NEXT_and_PREV( pkey_event.keycode, SYSTEM_num_systems_in_use(), (void*)&MVOR_extract_and_store_changes_from_GuiVars, (void*)&SYSTEM_get_group_at_this_index, (void*)&MVOR_copy_group_into_guivars );
			break;

		case KEY_C_UP:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_MVOR_OPEN_BUTTON:
				case CP_MVOR_CLOSE_BUTTON:
					g_MVOR_prev_cursor_pos = GuiLib_ActiveCursorFieldNo;

					CURSOR_Select( CP_MVOR_RUN_TIME, (true) );
					break;

				case CP_MVOR_SCHEDULE_DAY_0_OPEN:
				case CP_MVOR_SCHEDULE_DAY_0_CLOSE:
					if( GuiVar_MVORInEffect )
					{
						CURSOR_Select( CP_MVOR_CANCEL_BUTTON, (true) ) ;
					}
					else
					{
						CURSOR_Select( CP_MVOR_OPEN_BUTTON, (true) ) ;
					}
					break;

				case CP_MVOR_SCHEDULE_DAY_1_OPEN:
				case CP_MVOR_SCHEDULE_DAY_1_CLOSE:
				case CP_MVOR_SCHEDULE_DAY_2_OPEN:
				case CP_MVOR_SCHEDULE_DAY_2_CLOSE:
				case CP_MVOR_SCHEDULE_DAY_3_OPEN:
				case CP_MVOR_SCHEDULE_DAY_3_CLOSE:
				case CP_MVOR_SCHEDULE_DAY_4_OPEN:
				case CP_MVOR_SCHEDULE_DAY_4_CLOSE:
				case CP_MVOR_SCHEDULE_DAY_5_OPEN:
				case CP_MVOR_SCHEDULE_DAY_5_CLOSE:
				case CP_MVOR_SCHEDULE_DAY_6_OPEN:
				case CP_MVOR_SCHEDULE_DAY_6_CLOSE:
					CURSOR_Select( (GuiLib_ActiveCursorFieldNo - 2), (true) ) ;
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_C_DOWN:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_MVOR_RUN_TIME:
					// 8/31/2015 ajv : There's no need to check the CANCEL_BUTTON here because the RUN_TIME
					// cursor position doesn't exist when the cancel button is visible.
					if( (g_MVOR_prev_cursor_pos != CP_MVOR_OPEN_BUTTON) && (g_MVOR_prev_cursor_pos != CP_MVOR_CLOSE_BUTTON) )
					{
						g_MVOR_prev_cursor_pos = CP_MVOR_OPEN_BUTTON;
					}

					CURSOR_Select( g_MVOR_prev_cursor_pos, (true) ) ;
					break;

				case CP_MVOR_OPEN_BUTTON:
				case CP_MVOR_CLOSE_BUTTON:
				case CP_MVOR_CANCEL_BUTTON:
					g_MVOR_prev_cursor_pos = GuiLib_ActiveCursorFieldNo;

					CURSOR_Select( CP_MVOR_SCHEDULE_DAY_0_OPEN, (true) ) ;
					break;

				case CP_MVOR_SCHEDULE_DAY_0_OPEN:
				case CP_MVOR_SCHEDULE_DAY_0_CLOSE:
				case CP_MVOR_SCHEDULE_DAY_1_OPEN:
				case CP_MVOR_SCHEDULE_DAY_1_CLOSE:
				case CP_MVOR_SCHEDULE_DAY_2_OPEN:
				case CP_MVOR_SCHEDULE_DAY_2_CLOSE:
				case CP_MVOR_SCHEDULE_DAY_3_OPEN:
				case CP_MVOR_SCHEDULE_DAY_3_CLOSE:
				case CP_MVOR_SCHEDULE_DAY_4_OPEN:
				case CP_MVOR_SCHEDULE_DAY_4_CLOSE:
				case CP_MVOR_SCHEDULE_DAY_5_OPEN:
				case CP_MVOR_SCHEDULE_DAY_5_CLOSE:
					CURSOR_Select( (GuiLib_ActiveCursorFieldNo + 2), (true) ) ;
					break;

				case CP_MVOR_SCHEDULE_DAY_6_OPEN:
				case CP_MVOR_SCHEDULE_DAY_6_CLOSE:
				default:
					bad_key_beep();
			}
			break;

		case KEY_C_LEFT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_MVOR_RUN_TIME:
					KEY_process_BACK_from_editing_screen( (void*)&FDTO_MVOR_return_to_menu );
					break;

				default:
					CURSOR_Up( (true) );
			}
			break;

		case KEY_C_RIGHT:
			CURSOR_Down( (true) );
			break;

		case KEY_BACK:
			KEY_process_BACK_from_editing_screen( (void*)&FDTO_MVOR_return_to_menu );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_MVOR_return_to_menu( void )
{
	FDTO_GROUP_return_to_menu( &g_MVOR_editing_group, (void*)&MVOR_extract_and_store_changes_from_GuiVars );
}

/* ---------------------------------------------------------- */
extern void FDTO_MVOR_draw_menu( const BOOL_32 pcomplete_redraw )
{
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	FDTO_GROUP_draw_menu( pcomplete_redraw, &g_MVOR_editing_group, SYSTEM_num_systems_in_use(), GuiStruct_scrMVOR_0, (void *)&SYSTEM_load_system_name_into_scroll_box_guivar, (void *)&MVOR_copy_group_into_guivars, (false) );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void MVOR_process_menu( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	GROUP_process_menu( pkey_event, &g_MVOR_editing_group, SYSTEM_num_systems_in_use(), (void *)&MVOR_process_group, NULL, (void *)&SYSTEM_get_group_at_this_index, (void *)&MVOR_copy_group_into_guivars );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

