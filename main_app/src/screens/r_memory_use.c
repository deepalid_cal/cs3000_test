/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlen
#include	<string.h>

#include	"r_memory_use.h"

#include	"cs_common.h"

#include	"cs_mem.h"

#include	"cs_mem_part.h"

#include	"d_tech_support.h"

#include	"screen_utils.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Updates the variables used to display the memory allocations and forces a 
 * screen redraw. 
 * 
 * @executed Executed within the context of the display processing task when the
 *  		 screen is initially drawn; within the context of the TD check task
 *           as the screen is updated 4 times per second.
 *
 * @author 8/4/2011 Adrianusv
 *
 * @revisions
 *    8/4/2011   Initial release
 *    2/9/2012   Renamed function
 */
extern void FDTO_MEM_draw_report( const BOOL_32 pcomplete_redraw )
{
	#define	MEM_VERTICAL_SPACING_BETWEEN_LINES	(ANSI7_HEIGHT + 4)

	#define	MEM_X_COORD_OF_FIRST_COL			(105)

	#define	MEM_Y_COORD_OF_FIRST_COL			(38)

	UNS_64 total_mem;

	UNS_32 i;

	char str_64[ 64 ];

	total_mem = 0;
	
	if( pcomplete_redraw == (true) )
	{
		GuiLib_ShowScreen( GuiStruct_rptMemoryUsage_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );
	}

	// We're not using easyGUI variables here to allow partitions to be added
	// or removed without having to make changes within easyGUI.
	//
	// However, if we want easyGUI to handle this screen, it can be
	// accomplished using a scrollbox. However, this would require constant
	// calls to GuiLib_ScrollBox_Redraw.
	for( i = 0; i < OS_MEM_MAX_PART; ++i )
	{
		total_mem = (UNS_64)(total_mem + (partitions[ i ].block_size * partitions[ i ].block_count));

		// Block Size
		snprintf(str_64, sizeof(str_64), "%6lu", partitions[ i ].block_size );
		GuiLib_DrawStr( MEM_X_COORD_OF_FIRST_COL, (INT_16)(MEM_Y_COORD_OF_FIRST_COL + (i * MEM_VERTICAL_SPACING_BETWEEN_LINES)), GuiFont_ANSI7, -1, str_64, GuiLib_ALIGN_RIGHT, GuiLib_PS_NUM, GuiLib_TRANSPARENT_OFF, GuiLib_UNDERLINE_OFF, 0, 0, 0, 0, GuiConst_PIXEL_ON, GuiConst_PIXEL_OFF );

		// Current
		snprintf(str_64, sizeof(str_64), "%4u", mem_current_allocations_of[ i ] );
		GuiLib_DrawStr( (MEM_X_COORD_OF_FIRST_COL + 38), (INT_16)(MEM_Y_COORD_OF_FIRST_COL + (i * MEM_VERTICAL_SPACING_BETWEEN_LINES)), GuiFont_ANSI7, -1, str_64, GuiLib_ALIGN_RIGHT, GuiLib_PS_NUM, GuiLib_TRANSPARENT_OFF, GuiLib_UNDERLINE_OFF, 0, 0, 0, 0, GuiConst_PIXEL_ON, GuiConst_PIXEL_OFF );

		// Max Used
		snprintf(str_64, sizeof(str_64), "%4u", mem_max_allocations_of[ i ] );
		GuiLib_DrawStr( (MEM_X_COORD_OF_FIRST_COL + 96), (INT_16)(MEM_Y_COORD_OF_FIRST_COL + (i * MEM_VERTICAL_SPACING_BETWEEN_LINES)), GuiFont_ANSI7, -1, str_64, GuiLib_ALIGN_RIGHT, GuiLib_PS_NUM, GuiLib_TRANSPARENT_OFF, GuiLib_UNDERLINE_OFF, 0, 0, 0, 0, GuiConst_PIXEL_ON, GuiConst_PIXEL_OFF );

		// Max Available
		snprintf(str_64, sizeof(str_64), "%4lu", partitions[ i ].block_count );
		GuiLib_DrawStr( (MEM_X_COORD_OF_FIRST_COL + 165), (INT_16)(MEM_Y_COORD_OF_FIRST_COL + (i * MEM_VERTICAL_SPACING_BETWEEN_LINES)), GuiFont_ANSI7, -1, str_64, GuiLib_ALIGN_RIGHT, GuiLib_PS_NUM, GuiLib_TRANSPARENT_OFF, GuiLib_UNDERLINE_OFF, 0, 0, 0, 0, GuiConst_PIXEL_ON, GuiConst_PIXEL_OFF );
	}
	
	snprintf( str_64, sizeof(str_64), "%ld bytes in %ld allocs", mem_numalloc, mem_count );
	GuiLib_DrawStr( 160, 207, GuiFont_ANSI7, -1, str_64, GuiLib_ALIGN_CENTER, GuiLib_PS_NUM, GuiLib_TRANSPARENT_OFF, GuiLib_UNDERLINE_OFF, (INT_16)(strlen( str_64 ) * ANSI7_WIDTH), 0, 0, 0, GuiConst_PIXEL_ON, GuiConst_PIXEL_OFF );
	
	snprintf( str_64, sizeof(str_64), "(max %ld out of %ld)", mem_maxalloc, total_mem );
	GuiLib_DrawStr( 160, 219, GuiFont_ANSI7, -1, str_64, GuiLib_ALIGN_CENTER, GuiLib_PS_NUM, GuiLib_TRANSPARENT_OFF, GuiLib_UNDERLINE_OFF, (INT_16)(strlen( str_64 ) * ANSI7_WIDTH), 0, 0, 0, GuiConst_PIXEL_ON, GuiConst_PIXEL_OFF );

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void MEM_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		case KEY_BACK:
			GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;

			KEY_process_global_keys( pkey_event );

			// 6/5/2015 ajv : Since the user got here from the TECHNICAL SUPPORT dialog box, redraw that
			// dialog box.
			TECH_SUPPORT_draw_dialog( (true) );
			break;

		default:
			KEY_process_global_keys( pkey_event );
    }
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

