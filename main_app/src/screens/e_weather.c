/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_weather.h"

#include	"app_startup.h"

#include	"combobox.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"group_base_file.h"

#include	"station_groups.h"

#include	"m_main.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_WEATHER_USE_ET_0			(0)
#define	CP_WEATHER_USE_ET_1			(1)
#define	CP_WEATHER_USE_ET_2			(2)
#define	CP_WEATHER_USE_ET_3			(3)
#define	CP_WEATHER_USE_ET_4			(4)
#define	CP_WEATHER_USE_ET_5			(5)
#define	CP_WEATHER_USE_ET_6			(6)
#define	CP_WEATHER_USE_ET_7			(7)
#define	CP_WEATHER_USE_ET_8			(8)
#define	CP_WEATHER_USE_ET_9			(9)
#define	CP_WEATHER_USE_RAIN_0		(10)
#define	CP_WEATHER_USE_RAIN_1		(11)
#define	CP_WEATHER_USE_RAIN_2		(12)
#define	CP_WEATHER_USE_RAIN_3		(13)
#define	CP_WEATHER_USE_RAIN_4		(14)
#define	CP_WEATHER_USE_RAIN_5		(15)
#define	CP_WEATHER_USE_RAIN_6		(16)
#define	CP_WEATHER_USE_RAIN_7		(17)
#define	CP_WEATHER_USE_RAIN_8		(18)
#define	CP_WEATHER_USE_RAIN_9		(19)
#define	CP_WEATHER_USE_WIND_0		(20)
#define	CP_WEATHER_USE_WIND_1		(21)
#define	CP_WEATHER_USE_WIND_2		(22)
#define	CP_WEATHER_USE_WIND_3		(23)
#define	CP_WEATHER_USE_WIND_4		(24)
#define	CP_WEATHER_USE_WIND_5		(25)
#define	CP_WEATHER_USE_WIND_6		(26)
#define	CP_WEATHER_USE_WIND_7		(27)
#define	CP_WEATHER_USE_WIND_8		(28)
#define	CP_WEATHER_USE_WIND_9		(29)

#define	WEATHER_COMBO_BOX_X_START		(168)
#define	WEATHER_COMBO_BOX_X_OFFSET		(55)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32 *g_WEATHER_combo_box_guivar;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_WEATHER_show_et_rain_or_wind_in_use_dropdown( const UNS_32 px_coord, const UNS_32 py_coord )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( px_coord, py_coord, *g_WEATHER_combo_box_guivar );
}

/* ---------------------------------------------------------- */
extern void FDTO_WEATHER_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		WEATHER_copy_group_into_guivars();

		lcursor_to_select = 0;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrWeatherUse_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed on the Daily ET screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void WEATHER_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_cbxNoYes_0:
			COMBO_BOX_key_press( pkey_event.keycode, g_WEATHER_combo_box_guivar );
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_WEATHER_USE_ET_0:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingA_0;
							break;

						case CP_WEATHER_USE_ET_1:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingA_1;
							break;

						case CP_WEATHER_USE_ET_2:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingA_2;
							break;

						case CP_WEATHER_USE_ET_3:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingA_3;
							break;

						case CP_WEATHER_USE_ET_4:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingA_4;
							break;

						case CP_WEATHER_USE_ET_5:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingA_5;
							break;

						case CP_WEATHER_USE_ET_6:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingA_6;
							break;

						case CP_WEATHER_USE_ET_7:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingA_7;
							break;

						case CP_WEATHER_USE_ET_8:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingA_8;
							break;

						case CP_WEATHER_USE_ET_9:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingA_9;
							break;

						case CP_WEATHER_USE_RAIN_0:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingB_0;
							break;

						case CP_WEATHER_USE_RAIN_1:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingB_1;
							break;

						case CP_WEATHER_USE_RAIN_2:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingB_2;
							break;

						case CP_WEATHER_USE_RAIN_3:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingB_3;
							break;

						case CP_WEATHER_USE_RAIN_4:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingB_4;
							break;

						case CP_WEATHER_USE_RAIN_5:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingB_5;
							break;

						case CP_WEATHER_USE_RAIN_6:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingB_6;
							break;

						case CP_WEATHER_USE_RAIN_7:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingB_7;
							break;

						case CP_WEATHER_USE_RAIN_8:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingB_8;
							break;

						case CP_WEATHER_USE_RAIN_9:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingB_9;
							break;

						case CP_WEATHER_USE_WIND_0:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingC_0;
							break;

						case CP_WEATHER_USE_WIND_1:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingC_1;
							break;

						case CP_WEATHER_USE_WIND_2:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingC_2;
							break;

						case CP_WEATHER_USE_WIND_3:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingC_3;
							break;

						case CP_WEATHER_USE_WIND_4:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingC_4;
							break;

						case CP_WEATHER_USE_WIND_5:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingC_5;
							break;

						case CP_WEATHER_USE_WIND_6:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingC_6;
							break;

						case CP_WEATHER_USE_WIND_7:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingC_7;
							break;

						case CP_WEATHER_USE_WIND_8:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingC_8;
							break;

						case CP_WEATHER_USE_WIND_9:
							g_WEATHER_combo_box_guivar = &GuiVar_GroupSettingC_9;
							break;
					}

					if( g_WEATHER_combo_box_guivar != NULL )
					{
						good_key_beep();

						UNS_32	x_coord, y_coord;

						if( GuiLib_ActiveCursorFieldNo == CP_WEATHER_USE_ET_0 )
						{
							x_coord = WEATHER_COMBO_BOX_X_START;
							y_coord = GROUP_COMBO_BOX_Y_START;
						}
						else if( GuiLib_ActiveCursorFieldNo == CP_WEATHER_USE_RAIN_0 )
						{
							x_coord = WEATHER_COMBO_BOX_X_START + WEATHER_COMBO_BOX_X_OFFSET;
							y_coord = GROUP_COMBO_BOX_Y_START;
						}
						else if( GuiLib_ActiveCursorFieldNo == CP_WEATHER_USE_WIND_0 )
						{
							x_coord = WEATHER_COMBO_BOX_X_START + WEATHER_COMBO_BOX_X_OFFSET + WEATHER_COMBO_BOX_X_OFFSET;
							y_coord = GROUP_COMBO_BOX_Y_START;
						}
						else
						{
							if( GuiLib_ActiveCursorFieldNo < 10 )
							{
								x_coord = WEATHER_COMBO_BOX_X_START;
							}
							else if( GuiLib_ActiveCursorFieldNo < 20 )
							{
								x_coord = WEATHER_COMBO_BOX_X_START + WEATHER_COMBO_BOX_X_OFFSET;
							}
							else
							{
								x_coord = WEATHER_COMBO_BOX_X_START + WEATHER_COMBO_BOX_X_OFFSET + WEATHER_COMBO_BOX_X_OFFSET;
							}
							y_coord = (UNS_32)(((GuiLib_ActiveCursorFieldNo % 10) * Pos_Y_GroupSetting) + GROUP_COMBO_BOX_Y_START);
						}

						lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
						lde._04_func_ptr = (void*)&FDTO_WEATHER_show_et_rain_or_wind_in_use_dropdown;
						lde._06_u32_argument1 = x_coord;
						lde._07_u32_argument2 = y_coord;
						Display_Post_Command( &lde );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_WEATHER_USE_ET_0:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_0, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_ET_1:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_1, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_ET_2:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_2, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_ET_3:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_3, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_ET_4:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_4, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_ET_5:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_5, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_ET_6:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_6, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_ET_7:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_7, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_ET_8:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_8, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_ET_9:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_9, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_RAIN_0:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_0, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_RAIN_1:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_1, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_RAIN_2:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_2, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_RAIN_3:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_3, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_RAIN_4:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_4, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_RAIN_5:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_5, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_RAIN_6:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_6, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_RAIN_7:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_7, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_RAIN_8:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_8, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_RAIN_9:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingB_9, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_WIND_0:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingC_0, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_WIND_1:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingC_1, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_WIND_2:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingC_2, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_WIND_3:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingC_3, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_WIND_4:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingC_4, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_WIND_5:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingC_5, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_WIND_6:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingC_6, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_WIND_7:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingC_7, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_WIND_8:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingC_8, (false), (true), 1, (true) );
							break;

						case CP_WEATHER_USE_WIND_9:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingC_9, (false), (true), 1, (true) );
							break;

						default:
							bad_key_beep();
					}
					Redraw_Screen( (false) );
					break;

				case KEY_PREV:
				case KEY_C_UP:
					if( (GuiLib_ActiveCursorFieldNo % 10) == 0 )
					{
						bad_key_beep();
					}
					else
					{
						CURSOR_Up( (true) );
					}
					break;

				case KEY_NEXT:
				case KEY_C_DOWN:
					if( (UNS_32)(GuiLib_ActiveCursorFieldNo % 10) == (STATION_GROUP_get_num_groups_in_use() - 1) )
					{
						bad_key_beep();
					}
					else
					{
						CURSOR_Down( (true) );
					}
					break;

				case KEY_C_LEFT:
					if( (GuiLib_ActiveCursorFieldNo >= CP_WEATHER_USE_ET_0) && (GuiLib_ActiveCursorFieldNo <= CP_WEATHER_USE_ET_9) )
					{
						CURSOR_Select( (UNS_32)((GuiLib_ActiveCursorFieldNo + 20) - 1), (true) );
					}
					else if( (GuiLib_ActiveCursorFieldNo >= CP_WEATHER_USE_RAIN_0) && (GuiLib_ActiveCursorFieldNo <= CP_WEATHER_USE_RAIN_9) )
					{
						CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo - 10), (true) );
					}
					else if( (GuiLib_ActiveCursorFieldNo >= CP_WEATHER_USE_WIND_0) && (GuiLib_ActiveCursorFieldNo <= CP_WEATHER_USE_WIND_9) )
					{
						CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo - 10), (true) );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case KEY_C_RIGHT:
					if( (GuiLib_ActiveCursorFieldNo >= CP_WEATHER_USE_ET_0) && (GuiLib_ActiveCursorFieldNo <= CP_WEATHER_USE_ET_9) )
					{
						CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo + 10), (true) );
					}
					else if( (GuiLib_ActiveCursorFieldNo >= CP_WEATHER_USE_RAIN_0) && (GuiLib_ActiveCursorFieldNo <= CP_WEATHER_USE_RAIN_9) )
					{
						CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo + 10), (true) );
					}
					else if( (GuiLib_ActiveCursorFieldNo >= CP_WEATHER_USE_WIND_0) && (GuiLib_ActiveCursorFieldNo <= CP_WEATHER_USE_WIND_9) )
					{
						CURSOR_Select( (UNS_32)((GuiLib_ActiveCursorFieldNo - 20) + 1), (true) );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case KEY_BACK:
					GuiVar_MenuScreenToShow = CP_MAIN_MENU_WEATHER;
					WEATHER_extract_and_store_changes_from_GuiVars();
					// No need to break here to allow this to fall into the default
					// condition.

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

