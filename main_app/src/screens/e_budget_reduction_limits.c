/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"e_budget_reduction_limits.h"

#include	"app_startup.h"

#include	"cursor_utils.h"

#include	"epson_rx_8025sa.h"

#include	"group_base_file.h"

#include	"m_main.h"

#include	"screen_utils.h"

#include	"station_groups.h"

#include	"speaker.h"

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_BUDGET_REDUCTION_LIMIT_0			(0)
#define	CP_BUDGET_REDUCTION_LIMIT_1			(1)
#define	CP_BUDGET_REDUCTION_LIMIT_2			(2)
#define	CP_BUDGET_REDUCTION_LIMIT_3			(3)
#define	CP_BUDGET_REDUCTION_LIMIT_4			(4)
#define	CP_BUDGET_REDUCTION_LIMIT_5			(5)
#define	CP_BUDGET_REDUCTION_LIMIT_6			(6)
#define	CP_BUDGET_REDUCTION_LIMIT_7			(7)
#define	CP_BUDGET_REDUCTION_LIMIT_8			(8)
#define	CP_BUDGET_REDUCTION_LIMIT_9			(9)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Processes the Plus and Minus keys to change the budget reduction limit value.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 *
 * @param pkeycode The key that was pressed.
 *
 * @author 4/07/2016 skc
 *
 * @revisions
 *    4/07/2016 Initial release
 */
static void REDUCTION_LIMIT_process_percentage( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event, INT_32 *ppercentage )
{
	UNS_32	inc_by;

	UNS_32 keycode;

	if( pkey_event.repeats > 72 )
	{
		inc_by = 4;
	}
	else if( (pkey_event.repeats > 48) )
	{
		inc_by = 2;
	}
	else
	{
		inc_by = 1;
	}

	// 6/8/2016 skc : Reverse the polarity of the keycode. The reduction limit ranges from -100
	// to 0 on the screen. However, we want "+" to indicate "more reduction".
	keycode = (pkey_event.keycode == KEY_MINUS ? KEY_PLUS : KEY_MINUS);

	process_int32( keycode, ppercentage, 
				   -STATION_GROUP_BUDGET_PERCENTAGE_CAP_MAX, 
				   STATION_GROUP_BUDGET_PERCENTAGE_CAP_MIN, 
				   inc_by, (false) );

	Refresh_Screen();
}

/* ---------------------------------------------------------- */
extern void FDTO_BUDGET_REDUCTION_LIMITS_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		BUDGET_REDUCTION_LIMITS_copy_group_into_guivars();

		lcursor_to_select = 0;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrBudgetReductionLimits_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void BUDGET_REDUCTION_LIMIT_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{

	switch( pkey_event.keycode )
	{
		case KEY_MINUS:
		case KEY_PLUS:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_BUDGET_REDUCTION_LIMIT_0:
					REDUCTION_LIMIT_process_percentage( pkey_event, &GuiVar_BudgetReductionLimit_0 );
					break;

				case CP_BUDGET_REDUCTION_LIMIT_1:
					REDUCTION_LIMIT_process_percentage( pkey_event, &GuiVar_BudgetReductionLimit_1 );
					break;

				case CP_BUDGET_REDUCTION_LIMIT_2:
					REDUCTION_LIMIT_process_percentage( pkey_event, &GuiVar_BudgetReductionLimit_2 );
					break;

				case CP_BUDGET_REDUCTION_LIMIT_3:
					REDUCTION_LIMIT_process_percentage( pkey_event, &GuiVar_BudgetReductionLimit_3 );
					break;

				case CP_BUDGET_REDUCTION_LIMIT_4:
					REDUCTION_LIMIT_process_percentage( pkey_event, &GuiVar_BudgetReductionLimit_4 );
					break;

				case CP_BUDGET_REDUCTION_LIMIT_5:
					REDUCTION_LIMIT_process_percentage( pkey_event, &GuiVar_BudgetReductionLimit_5 );
					break;

				case CP_BUDGET_REDUCTION_LIMIT_6:
					REDUCTION_LIMIT_process_percentage( pkey_event, &GuiVar_BudgetReductionLimit_6 );
					break;

				case CP_BUDGET_REDUCTION_LIMIT_7:
					REDUCTION_LIMIT_process_percentage( pkey_event, &GuiVar_BudgetReductionLimit_7 );
					break;

				case CP_BUDGET_REDUCTION_LIMIT_8:
					REDUCTION_LIMIT_process_percentage( pkey_event, &GuiVar_BudgetReductionLimit_8 );
					break;

				case CP_BUDGET_REDUCTION_LIMIT_9:
					REDUCTION_LIMIT_process_percentage( pkey_event, &GuiVar_BudgetReductionLimit_9 );
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_PREV:
		case KEY_C_UP:
			if( (GuiLib_ActiveCursorFieldNo % 10) == 0 )
			{
				bad_key_beep();
			}
			else
			{
				CURSOR_Up( (true) );
			}
			break;

		case KEY_NEXT:
		case KEY_C_DOWN:
			if( (UNS_32)(GuiLib_ActiveCursorFieldNo % 10) == (STATION_GROUP_get_num_groups_in_use() - 1) )
			{
				bad_key_beep();
			}
			else
			{
				CURSOR_Down( (true) );
			}
			break;

		case KEY_C_LEFT:
			if( (GuiLib_ActiveCursorFieldNo >= CP_BUDGET_REDUCTION_LIMIT_0) && (GuiLib_ActiveCursorFieldNo <= CP_BUDGET_REDUCTION_LIMIT_9) )
			{
				CURSOR_Select( (UNS_32)((GuiLib_ActiveCursorFieldNo + 10) - 1), (true) );
			}
			else
			{
				bad_key_beep();
			}
			break;

		case KEY_C_RIGHT:
			if( (GuiLib_ActiveCursorFieldNo >= CP_BUDGET_REDUCTION_LIMIT_0) && (GuiLib_ActiveCursorFieldNo <= CP_BUDGET_REDUCTION_LIMIT_9) )
			{
				CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo + 10), (true) );
			}
			else
			{
				bad_key_beep();
			}
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = CP_MAIN_MENU_BUDGETS;

			BUDGET_REDUCTION_LIMITS_extract_and_store_changes_from_GuiVars();
			// No need to break here to allow this to fall into the default
			// condition.

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


