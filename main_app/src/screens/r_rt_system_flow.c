/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/28/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"r_rt_system_flow.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"cursor_utils.h"

#include	"d_live_screens.h"

#include	"d_process.h"

#include	"irri_comm.h"

#include	"report_utils.h"

#include	"system_report_data.h"

#include	"scrollbox.h"

#include	"speaker.h"





/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void REAL_TIME_SYSTEM_FLOW_draw_scroll_line( const INT_16 pline_index_i16 )
{
	// 8/31/2015 ajv : When functions are declared as static, the compiler may inline the
	// functions and optimize out floating-point constant literals from the calculations. This
	// can result in a division-by-0 error. In order to resolve this, we declare the constant
	// literal as volatile which tells the compile to not optimize the value.
	static const volatile float SECONDS_PER_HOUR = 3600.0F;

	// ----------

	IRRIGATION_SYSTEM_GROUP_STRUCT *lsystem_struct;

	BY_SYSTEM_RECORD	*lpreserve;

	// 2012.06.19 ajv : Always take the preserves mutex before taking the list
	// mutex. We may be able to get around needing the list mutex here by
	// building the entire screen using the perserves, but I'm not sure how we'd
	// deal with getting the name at the moment. Will reevaluate later.
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem_struct = SYSTEM_get_group_at_this_index( (UNS_32)pline_index_i16 );

	if( lsystem_struct != NULL )
	{
		// 11/1/2013 ajv : TODO For now, to limit how long the string is, we're
		// going to truncate the system name manually. Ideally, we'd like to
		// have easyGUI automatically limit the size using a clipping rectangle
		// or similar structure, but those don't seem to behave properly on
		// scrolling lists.
		// 
		// 24 characters is how many characters, using fixed-width, we can draw
		// before we hit the valves on setting.
		strlcpy( GuiVar_StatusSystemFlowSystem, nm_GROUP_get_name( lsystem_struct ), 24 );
		//strlcpy( GuiVar_StatusSystemFlowSystem, nm_GROUP_get_name( lsystem_struct ), sizeof(GuiVar_StatusSystemFlowSystem) );

		lpreserve = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( nm_GROUP_get_group_ID(lsystem_struct) );

		if( lpreserve != NULL )
		{
			// 6/26/2015 mpd : FogBugz #3113. Added the IRRI side variable "system_rcvd_most_recent_number_of_valves_ON"
			// to the BY_SYSTEM_RECORD structure. Display the IRRI side variable instead of the FOAL version.
			//GuiVar_StatusSystemFlowValvesOn = lpreserve->number_of_valves_ON;
			GuiVar_StatusSystemFlowValvesOn = lpreserve->system_rcvd_most_recent_number_of_valves_ON;
			GuiVar_StatusSystemFlowActual = (UNS_32)lpreserve->system_rcvd_most_recent_token_5_second_average;
			GuiVar_StatusSystemFlowExpected = lpreserve->ufim_expected_flow_rate_for_those_ON;

			// 10/14/2013 ajv : Set the flag to show MAINLINE BREAK for the system.
			GuiVar_StatusSystemFlowShowMLBDetected = SYSTEM_PRESERVES_there_is_a_mlb( &(lpreserve->delivered_mlb_record) );

			GuiVar_MVORInEffect = (lpreserve->sbf.delivered_MVOR_in_effect_opened || lpreserve->sbf.delivered_MVOR_in_effect_closed);

			if( GuiVar_StatusSystemFlowShowMLBDetected )
			{
				if( lpreserve->delivered_mlb_record.there_was_a_MLB_during_irrigation == (true) )
				{
					GuiVar_StatusSystemFlowMLBMeasured = (UNS_32)lpreserve->delivered_mlb_record.mlb_measured_during_irrigation_gpm;
					GuiVar_StatusSystemFlowMLBAllowed = (UNS_32)lpreserve->delivered_mlb_record.mlb_limit_during_irrigation_gpm;
				}
				else if( lpreserve->delivered_mlb_record.there_was_a_MLB_during_mvor_closed == (true) )
				{
					GuiVar_StatusSystemFlowMLBMeasured = (UNS_32)lpreserve->delivered_mlb_record.mlb_measured_during_mvor_closed_gpm;
					GuiVar_StatusSystemFlowMLBAllowed = (UNS_32)lpreserve->delivered_mlb_record.mlb_limit_during_mvor_closed_gpm;
				}
				else if( lpreserve->delivered_mlb_record.there_was_a_MLB_during_all_other_times == (true) )
				{
					GuiVar_StatusSystemFlowMLBMeasured = (UNS_32)lpreserve->delivered_mlb_record.mlb_measured_during_all_other_times_gpm;
					GuiVar_StatusSystemFlowMLBAllowed = (UNS_32)lpreserve->delivered_mlb_record.mlb_limit_during_all_other_times_gpm;
				}
			}
			else if( GuiVar_MVORInEffect )
			{
				GuiVar_MVORState = lpreserve->sbf.delivered_MVOR_in_effect_closed;

				GuiVar_MVORTimeRemaining = (lpreserve->delivered_MVOR_remaining_seconds / SECONDS_PER_HOUR);
			}
		}
		else
		{
			// 10/14/2013 ajv : Since the preserve isn't valid, initialize the
			// variables to 0. This hsould never happen, but it's important to
			// make sure this case is covered.
			
			GuiVar_StatusSystemFlowShowMLBDetected = (false);

			GuiVar_StatusSystemFlowValvesOn = 0;
			GuiVar_StatusSystemFlowActual = 0;
			GuiVar_StatusSystemFlowExpected = 0;
		}
	}
	else
	{
		Alert_group_not_found();
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void FDTO_REAL_TIME_SYSTEM_FLOW_draw_report( const BOOL_32 pcomplete_redraw )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT *lsystem_struct;

	BY_SYSTEM_RECORD	*lpreserve;

	BOOL_32	lprevious_mlb_state;

	BOOL_32	lprevious_mvor_state;

	UNS_32	i;

	// ----------

	if( pcomplete_redraw == (true) )
	{
		GuiLib_ShowScreen( GuiStruct_rptRTSystemFlow_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );
		GuiLib_ScrollBox_Close( 0 );
		GuiLib_ScrollBox_Init( 0, &REAL_TIME_SYSTEM_FLOW_draw_scroll_line, (INT_16)SYSTEM_num_systems_in_use(), 0 );
	}
	else
	{
		GuiLib_ScrollBox_Redraw( 0 );
	}

	// ----------

	// 5/14/2015 ajv : Once we've drawn the scroll box, determine whether we need to show the
	// Clear Mainline Break instructions.
	
	// 5/14/2015 ajv : Always take the preserves mutex before taking the list mutex. We may be
	// able to get around needing the list mutex here by building the entire screen using the
	// perserves, but I'm not sure how we'd deal with getting the name at the moment. Will
	// reevaluate later.
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lprevious_mlb_state = GuiVar_LiveScreensSystemMLBExists;

	lprevious_mvor_state = GuiVar_LiveScreensSystemMVORExists;
	
	// ----------

	GuiVar_LiveScreensSystemMLBExists = (false);

	GuiVar_LiveScreensSystemMVORExists = (false);

	for( i = 0; i < SYSTEM_num_systems_in_use(); ++i )
	{
		lsystem_struct = SYSTEM_get_group_at_this_index( i );

		if( lsystem_struct != NULL )
		{
			lpreserve = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( nm_GROUP_get_group_ID(lsystem_struct) );

			if( lpreserve != NULL )
			{
				if( SYSTEM_PRESERVES_there_is_a_mlb( &(lpreserve->delivered_mlb_record) ) )
				{
					GuiVar_LiveScreensSystemMLBExists = (true);

					break;
				}
				else if( lpreserve->sbf.delivered_MVOR_in_effect_opened || lpreserve->sbf.delivered_MVOR_in_effect_closed )
				{
					GuiVar_LiveScreensSystemMVORExists = (true);
				}
			}
		}
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	// ----------

	// 8/31/2015 ajv : We don't have enough room on the screen to show a note for both MLB and
	// MVOR at the bottom, so we're going to have the MLB notice take precedence since it takes
	// precedence when building the by-system records to display anyway.
	if( GuiVar_LiveScreensSystemMLBExists && GuiVar_LiveScreensSystemMVORExists )
	{
		GuiVar_LiveScreensSystemMVORExists = (false);
	}

	// ----------
	
	// 5/14/2015 ajv : If we've cleared a MLB or one has occurred while viewing the screen, for
	// a full redraw of the screen to ensure the clearing instructions are displayed or removed.
	if( (lprevious_mlb_state != GuiVar_LiveScreensSystemMLBExists) || (lprevious_mvor_state != GuiVar_LiveScreensSystemMVORExists) )
	{
		FDTO_Redraw_Screen( (true) );
	}
	else
	{
		GuiLib_Refresh();
	}
}

/* ---------------------------------------------------------- */
extern void REAL_TIME_SYSTEM_FLOW_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem_struct;

	BY_SYSTEM_RECORD				*lpreserve;

	UNS_32	lsystem_GID;

	// ----------

	switch( pkey_event.keycode )
	{
		case KEY_SELECT:

			// 5/14/2015 ajv : Always take the preserves mutex before taking the list mutex. We may be
			// able to get around needing the list mutex here by building the entire screen using the
			// perserves, but I'm not sure how we'd deal with getting the name at the moment. Will
			// reevaluate later.
			xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

			xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

			lsystem_struct = SYSTEM_get_group_at_this_index( (UNS_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 ) );

			if( lsystem_struct != NULL )
			{
				lsystem_GID = nm_GROUP_get_group_ID( lsystem_struct );

				lpreserve = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( lsystem_GID );

				if( lpreserve == NULL )
				{
					bad_key_beep();

					ALERT_MESSAGE_WITH_FILE_NAME( "System not found" );
				}
				else
				{
					if( SYSTEM_PRESERVES_there_is_a_mlb( &(lpreserve->delivered_mlb_record) ) )
					{
						good_key_beep();

						SYSTEM_clear_mainline_break( lsystem_GID );
					}
					else if( lpreserve->sbf.delivered_MVOR_in_effect_opened || lpreserve->sbf.delivered_MVOR_in_effect_closed )
					{
						good_key_beep();

						irri_comm.mvor_request.mvor_action_to_take = MVOR_ACTION_CANCEL_MASTER_VALVE_OVERRIDE;
						irri_comm.mvor_request.mvor_seconds = 0;
						irri_comm.mvor_request.initiated_by = INITIATED_VIA_KEYPAD;
						irri_comm.mvor_request.system_gid = lsystem_GID;
						irri_comm.mvor_request.in_use = (true);
					}
					else
					{
						bad_key_beep();
					}
				}
			}
			else
			{
				bad_key_beep();
			}

			xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

			xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;

			KEY_process_global_keys( pkey_event );

			if( GuiVar_MenuScreenToShow == SCREEN_MENU_DIAGNOSTICS )
			{
				// 3/16/2016 ajv : Since the user got here from the LIVE SCREENS dialog box, redraw that
				// dialog box.
				LIVE_SCREENS_draw_dialog( (true) );
			}
			break;

		default:
			REPORTS_process_report( pkey_event, 0, 0 );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

