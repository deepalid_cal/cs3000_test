/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"e_programs.h"

#include	"app_startup.h"

#include	"combobox.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"epson_rx_8025sa.h"

#include	"group_base_file.h"

#include	"station_groups.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_SCHEDULE_START_TIME			(0)
#define CP_SCHEDULE_MOW_DAY				(1)
#define	CP_SCHEDULE_STOP_TIME			(2)
#define CP_SCHEDULE_TYPE				(3)
#define CP_SCHEDULE_IRRIGATEON1ST		(4)
#define CP_SCHEDULE_WATER_DAY_SUN		(5)
#define CP_SCHEDULE_WATER_DAY_MON		(6)
#define CP_SCHEDULE_WATER_DAY_TUE		(7)
#define CP_SCHEDULE_WATER_DAY_WED		(8)
#define CP_SCHEDULE_WATER_DAY_THU 		(9)
#define CP_SCHEDULE_WATER_DAY_FRI		(10)
#define CP_SCHEDULE_WATER_DAY_SAT		(11)
#define CP_SCHEDULE_BEGIN_DATE			(12)
#define CP_SCHEDULE_USE_ET_AVERAGING	(13)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static BOOL_32	g_SCHEDULE_editing_group;

static UNS_32	g_SCHEDULE_prev_cursor_pos;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void FDTO_SCHEDULE_return_to_menu( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Processes the PLUS and MINUS keys to change the start time.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @author 7/26/2011 Adrianusv
 * 
 * @revisions
 *    7/26/2011 Initial release
 */
static void SCHEDULE_process_start_time( const UNS_32 pkeycode )
{
	BOOL_32	lprev_value;

	// ----------
	
	process_uns32( pkeycode, &GuiVar_ScheduleStartTime, (UNS_32)(SCHEDULE_START_TIME_MIN / 60), (UNS_32)(SCHEDULE_OFF_TIME / 60), 10, (true) );

	GuiVar_ScheduleStartTimeEnabled = ( GuiVar_ScheduleStartTime < (UNS_32)(SCHEDULE_OFF_TIME / 60) );

	// ----------

	lprev_value = GuiVar_ScheduleStartTimeEqualsStopTime;

	GuiVar_ScheduleStartTimeEqualsStopTime = (GuiVar_ScheduleStartTimeEnabled) && (GuiVar_ScheduleStartTime == GuiVar_ScheduleStopTime);

	// 5/7/2015 ajv : If the value of GuiVar_ScheduleStartTimeEqualsStopTime has changed, we
	// need to redraw the screen for the warning to display. So check that now.
	if( lprev_value != GuiVar_ScheduleStartTimeEqualsStopTime )
	{
		Redraw_Screen( (false) );
	}
}

/* ---------------------------------------------------------- */
/**
 * Processes the PLUS and MINUS keys to change the stop time.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @author 7/26/2011 Adrianusv
 * 
 * @revisions
 *    7/26/2011 Initial release
 */
static void SCHEDULE_process_stop_time( const UNS_32 pkeycode )
{
	BOOL_32	lprev_value;

	// ----------
	
	process_uns32( pkeycode, &GuiVar_ScheduleStopTime, (UNS_32)(SCHEDULE_START_TIME_MIN / 60), (UNS_32)(SCHEDULE_OFF_TIME / 60), 10, (true) );

	GuiVar_ScheduleStopTimeEnabled = ( GuiVar_ScheduleStopTime < (UNS_32)(SCHEDULE_OFF_TIME / 60) );

	// ----------

	lprev_value = GuiVar_ScheduleStartTimeEqualsStopTime;

	GuiVar_ScheduleStartTimeEqualsStopTime = (GuiVar_ScheduleStopTimeEnabled) && (GuiVar_ScheduleStartTime == GuiVar_ScheduleStopTime);

	// 5/7/2015 ajv : If the value of GuiVar_ScheduleStartTimeEqualsStopTime has changed, we
	// need to redraw the screen for the warning to display. So check that now.
	if( lprev_value != GuiVar_ScheduleStartTimeEqualsStopTime )
	{
		Redraw_Screen( (false) );
	}
}

static void FDTO_SCHEDULE_show_mow_day_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxMowDay_0, &FDTO_COMBOBOX_add_items, 8, GuiVar_ScheduleMowDay );
}

/* ---------------------------------------------------------- */
/**
 * Processes the PLUS and MINUS keys to change the schedule type.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @author 7/26/2011 Adrianusv
 * 
 * @revisions
 *    7/26/2011 Initial release
 */
static void SCHEDULE_process_schedule_type( const UNS_32 pkeycode )
{
	DATE_TIME ldt;

	char str_48[ 48 ];

	UNS_32 lprevious_type;

	// Store the previous type to identify whether we need to redraw the screen
	// or not based upon the new type
	lprevious_type = GuiVar_ScheduleType;
	 
	process_uns32( pkeycode, &GuiVar_ScheduleType, SCHEDULE_TYPE_MIN, SCHEDULE_TYPE_MAX, 1, (false) );

	// If something has appeared or disappeared from the screen, force a redraw
	// to update the display and the cursor positions
	if( ((lprevious_type == SCHEDULE_TYPE_ODD_DAYS) && (GuiVar_ScheduleType == SCHEDULE_TYPE_EVEN_DAYS)) ||
		(GuiVar_ScheduleType == SCHEDULE_TYPE_ODD_DAYS) || 
		(GuiVar_ScheduleType == SCHEDULE_TYPE_WEEKLY_SCHEDULE) ||
		((lprevious_type == SCHEDULE_TYPE_WEEKLY_SCHEDULE) && (GuiVar_ScheduleType == SCHEDULE_TYPE_EVERY_OTHER_DAY)) )
	{
		Redraw_Screen( (false) );
	}

	if( GuiVar_ScheduleType >= SCHEDULE_TYPE_EVERY_OTHER_DAY )
	{
		EPSON_obtain_latest_time_and_date( &ldt );

		if(  GuiVar_ScheduleBeginDate < ldt.D )
		{
			GuiVar_ScheduleBeginDate = ldt.D;

			strlcpy( GuiVar_ScheduleBeginDateStr, GetDateStr(str_48, sizeof(str_48), GuiVar_ScheduleBeginDate, DATESTR_show_long_year, DATESTR_show_long_dow), sizeof(GuiVar_ScheduleBeginDateStr) );
		}
	}
}

/* ---------------------------------------------------------- */
/**
 * Callback function for the GuiLib_ScrollBox_Init_Custom_SetTopLine routine
 * used to draw the schedule type dropdown.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the users presses SELECT to change the schedule type setting.
 * 
 * @param pindex_0 The index of the line to populate - 0-based.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_SCHEDULED_populate_schedule_type_dropdown( const INT_16 pline_index_0 )
{
	GuiVar_ScheduleTypeScrollItem = (UNS_32)pline_index_0;
}

/* ---------------------------------------------------------- */
/**
 * Draws the schedule type dropdown window atop the main window.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the users presses SELECT to change the schedule type setting.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_SCHEDULE_show_schedule_type_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxScheduleType_0, &FDTO_SCHEDULED_populate_schedule_type_dropdown, (SCHEDULE_TYPE_ONCE_EVERY_FOUR_WEEKS + 1), GuiVar_ScheduleType );
}

/* ---------------------------------------------------------- */
/**
 * Processes the PLUS and MINUS keys to change a water day.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pcursor_position The current cursor position - this is used to
 * identify which day is selected
 *
 * @author 7/26/2011 Adrianusv
 * 
 * @revisions
 *    7/26/2011 Initial release
 */
static void SCHEDULE_process_water_day( const UNS_32 pcursor_position )
{
	switch( pcursor_position )
	{
		case CP_SCHEDULE_WATER_DAY_SUN:
			process_bool( &GuiVar_ScheduleWaterDay_Sun );
			break;

		case CP_SCHEDULE_WATER_DAY_MON:
			process_bool( &GuiVar_ScheduleWaterDay_Mon );
			break;

		case CP_SCHEDULE_WATER_DAY_TUE:
			process_bool( &GuiVar_ScheduleWaterDay_Tue );
			break;

		case CP_SCHEDULE_WATER_DAY_WED:
			process_bool( &GuiVar_ScheduleWaterDay_Wed );
			break;

		case CP_SCHEDULE_WATER_DAY_THU:
			process_bool( &GuiVar_ScheduleWaterDay_Thu );
			break;

		case CP_SCHEDULE_WATER_DAY_FRI:
			process_bool( &GuiVar_ScheduleWaterDay_Fri );
			break;

		case CP_SCHEDULE_WATER_DAY_SAT:
			process_bool( &GuiVar_ScheduleWaterDay_Sat );
			break;

		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/**
 * Processes the PLUS and MINUS keys to change the schedule's begin date.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @author 7/26/2011 Adrianusv
 * 
 * @revisions
 *    7/26/2011 Initial release
 */
static void SCHEDULE_process_begin_date( const UNS_32 pkeycode )
{
	char str_48[ 48 ];

	DATE_TIME ldt;

	EPSON_obtain_latest_time_and_date( &ldt );

	process_uns32( pkeycode, &GuiVar_ScheduleBeginDate, (UNS_32)ldt.D, (UNS_32)(ldt.D + 365), 1, (false) );

	strlcpy( GuiVar_ScheduleBeginDateStr, GetDateStr(str_48, sizeof(str_48), GuiVar_ScheduleBeginDate, DATESTR_show_long_year, DATESTR_show_long_dow), sizeof(GuiVar_ScheduleBeginDateStr) );
}

/* ---------------------------------------------------------- */
static void FDTO_SCHEDULE_show_irrigate_on_1st_dropdown( void )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( 275, 82, GuiVar_ScheduleIrrigateOn29thOr31st );
}

/* ---------------------------------------------------------- */
static void FDTO_SCHEDULE_show_use_et_averaging_dropdown( void )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( 272, 132, GuiVar_ScheduleUseETAveraging );
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed on the Programs screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void SCHEDULE_process_group( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_cbxMowDay_0:
			COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_ScheduleMowDay );
			break;

		case GuiStruct_cbxScheduleType_0:
			COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_ScheduleType );
			break;

		case GuiStruct_cbxNoYes_0:
			if( GuiVar_ScheduleType == SCHEDULE_TYPE_ODD_DAYS )
			{
				COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_ScheduleIrrigateOn29thOr31st );
			}
			else
			{
				COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_ScheduleUseETAveraging );
			}
			break;

		default:
			switch( pkey_event.keycode ) 
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_SCHEDULE_MOW_DAY:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_SCHEDULE_show_mow_day_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_SCHEDULE_TYPE:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_SCHEDULE_show_schedule_type_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_SCHEDULE_WATER_DAY_SUN:
						case CP_SCHEDULE_WATER_DAY_MON:
						case CP_SCHEDULE_WATER_DAY_TUE:
						case CP_SCHEDULE_WATER_DAY_WED:
						case CP_SCHEDULE_WATER_DAY_THU:
						case CP_SCHEDULE_WATER_DAY_FRI:
						case CP_SCHEDULE_WATER_DAY_SAT:
							SCHEDULE_process_water_day( (UNS_32)GuiLib_ActiveCursorFieldNo );

							if( GuiLib_ActiveCursorFieldNo < CP_SCHEDULE_WATER_DAY_SAT )
							{
								CURSOR_Down( (false) );
							}
							break;

						case CP_SCHEDULE_IRRIGATEON1ST:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_SCHEDULE_show_irrigate_on_1st_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_SCHEDULE_USE_ET_AVERAGING:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_SCHEDULE_show_use_et_averaging_dropdown;
							Display_Post_Command( &lde );
							break;

						default:
							bad_key_beep();
					}
					Refresh_Screen();
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch ( GuiLib_ActiveCursorFieldNo )
					{
						case CP_SCHEDULE_START_TIME:
							SCHEDULE_process_start_time( pkey_event.keycode );
							break;

						case CP_SCHEDULE_STOP_TIME:
							SCHEDULE_process_stop_time( pkey_event.keycode );
							break;

						case CP_SCHEDULE_TYPE:
							SCHEDULE_process_schedule_type( pkey_event.keycode );
							break;

						case CP_SCHEDULE_MOW_DAY:
							process_uns32( pkey_event.keycode, &GuiVar_ScheduleMowDay, SCHEDULE_MOWDAY_MIN, SCHEDULE_MOWDAY_MAX, 1, (true) );
							break;

						case CP_SCHEDULE_IRRIGATEON1ST:
							process_bool( &GuiVar_ScheduleIrrigateOn29thOr31st );
							break;

						case CP_SCHEDULE_WATER_DAY_SUN:
						case CP_SCHEDULE_WATER_DAY_MON:
						case CP_SCHEDULE_WATER_DAY_TUE:
						case CP_SCHEDULE_WATER_DAY_WED:
						case CP_SCHEDULE_WATER_DAY_THU:
						case CP_SCHEDULE_WATER_DAY_FRI:
						case CP_SCHEDULE_WATER_DAY_SAT:
							SCHEDULE_process_water_day( (UNS_32)GuiLib_ActiveCursorFieldNo );

							if( GuiLib_ActiveCursorFieldNo < CP_SCHEDULE_WATER_DAY_SAT )
							{
								CURSOR_Down( (false) );
							}
							break;

						case CP_SCHEDULE_BEGIN_DATE:
							SCHEDULE_process_begin_date( pkey_event.keycode );
							break;

						case CP_SCHEDULE_USE_ET_AVERAGING:
							process_bool( &GuiVar_ScheduleUseETAveraging );
							break;

						default:
							bad_key_beep();
					}
					Refresh_Screen();
					break;

				case KEY_NEXT:
				case KEY_PREV:
					GROUP_process_NEXT_and_PREV( pkey_event.keycode, STATION_GROUP_get_num_groups_in_use(), (void*)&SCHEDULE_extract_and_store_changes_from_GuiVars, (void*)&STATION_GROUP_get_group_at_this_index, (void*)&SCHEDULE_copy_group_into_guivars );
					break;

				case KEY_C_UP:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_SCHEDULE_MOW_DAY:
						case CP_SCHEDULE_STOP_TIME:
							CURSOR_Select( CP_SCHEDULE_START_TIME, (true) );
							break;

						case CP_SCHEDULE_WATER_DAY_SUN:
						case CP_SCHEDULE_WATER_DAY_MON:
						case CP_SCHEDULE_WATER_DAY_TUE:
						case CP_SCHEDULE_WATER_DAY_WED:
						case CP_SCHEDULE_WATER_DAY_THU:
						case CP_SCHEDULE_WATER_DAY_FRI:
						case CP_SCHEDULE_WATER_DAY_SAT:
							CURSOR_Select( CP_SCHEDULE_TYPE, (true) );
							break;

						case CP_SCHEDULE_USE_ET_AVERAGING:
							CURSOR_Select( g_SCHEDULE_prev_cursor_pos, (true) );
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_DOWN:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_SCHEDULE_START_TIME:
						case CP_SCHEDULE_MOW_DAY:
							CURSOR_Select( CP_SCHEDULE_STOP_TIME, (true) );
							break;

						case CP_SCHEDULE_WATER_DAY_SUN:
						case CP_SCHEDULE_WATER_DAY_MON:
						case CP_SCHEDULE_WATER_DAY_TUE:
						case CP_SCHEDULE_WATER_DAY_WED:
						case CP_SCHEDULE_WATER_DAY_THU:
						case CP_SCHEDULE_WATER_DAY_FRI:
						case CP_SCHEDULE_WATER_DAY_SAT:
							g_SCHEDULE_prev_cursor_pos = GuiLib_ActiveCursorFieldNo;

							CURSOR_Select( CP_SCHEDULE_USE_ET_AVERAGING, (true) );
							break;

						default:
							CURSOR_Down( (true) );
					}
					break;

				case KEY_C_LEFT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_SCHEDULE_START_TIME:
							KEY_process_BACK_from_editing_screen( (void*)&FDTO_SCHEDULE_return_to_menu );
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_RIGHT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						default:
							CURSOR_Down( (true) );
					}
					break;

				case KEY_BACK:
					KEY_process_BACK_from_editing_screen( (void*)&FDTO_SCHEDULE_return_to_menu );
					break;

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Returns to the menu on the left from the editing screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user presses a key to move away from the editing screen and back
 * onto the menu.
 * 
 * @param psave_changes True if the changes should be saved; otherwise, false.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_SCHEDULE_return_to_menu( void )
{
	FDTO_GROUP_return_to_menu( &g_SCHEDULE_editing_group, (void*)&SCHEDULE_extract_and_store_changes_from_GuiVars );
}

/* ---------------------------------------------------------- */
/**
 * Draws the Programs menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the screen is first navigated to.
 * 
 * @param pcomplete_redraw True if the screen should be complete redrawn;
 * otherwise, false. If false, only elements on the screen are updated, not the
 * entire structure, essentially refreshing the content.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void FDTO_SCHEDULE_draw_menu( const BOOL_32 pcomplete_redraw )
{
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	FDTO_GROUP_draw_menu( pcomplete_redraw, &g_SCHEDULE_editing_group, STATION_GROUP_get_num_groups_in_use(), GuiStruct_scrStartTimesAndWaterDays_0, (void *)&nm_STATION_GROUP_load_group_name_into_guivar, (void *)&SCHEDULE_copy_group_into_guivars, (false) );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed on the Program menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void SCHEDULE_process_menu( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	GROUP_process_menu( pkey_event, &g_SCHEDULE_editing_group, STATION_GROUP_get_num_groups_in_use(), (void *)&SCHEDULE_process_group, NULL, (void *)&STATION_GROUP_get_group_at_this_index, (void *)&SCHEDULE_copy_group_into_guivars );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

