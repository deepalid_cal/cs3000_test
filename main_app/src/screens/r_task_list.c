/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"r_task_list.h"

#include	"d_tech_support.h"

#include	"m_main.h"

#include	"screen_utils.h"

#include	"serial.h"

#include	"speaker.h"

#include	"textbox.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Draws the OS statistics report.
 * 
 * @executed Executed within the context of the display processing task when the
 *           screen is initially drawn. 
 *  
 * @param pcomplete_redraw True if the screen is to be redrawn completely
 *  				        (which should only happen on initial entry into the
 *                          screen); otherwise, false.
 *
 * @author 8/4/2011 Adrianusv
 *
 * @revisions
 *    8/4/2011   Initial release
 *    2/9/2012   Renamed function
 */
extern void FDTO_TASK_LIST_draw_report( const BOOL_32 pcomplete_redraw )
{
	if( pcomplete_redraw == (true) )
	{
		#if configGENERATE_RUN_TIME_STATS

			vTaskGetRunTimeStats( (signed char*)&GuiVar_TaskList );

		#else

			strlcpy( GuiVar_TaskList, "Task List is currently unavailable", sizeof(GuiVar_TaskList) );

		#endif

		GuiLib_ShowScreen( GuiStruct_rptTasks_0, 0, GuiLib_RESET_AUTO_REDRAW );
		GuiLib_Refresh();
	}
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while on the OS statistics report.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 *
 * @param pkey_event The key event to be processed.
 *
 * @author 8/4/2011 Adrianusv
 *
 * @revisions
 *    8/4/2011   Initial release
 *    2/9/2012   Renamed function
 */
extern void TASK_LIST_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		#if configGENERATE_RUN_TIME_STATS

			case KEY_SELECT:
				good_key_beep();
				Redraw_Screen( (true) );
				break;

			case KEY_C_UP:
			case KEY_C_DOWN:
				TEXT_BOX_up_or_down( 0, pkey_event.keycode );
				break;
		#endif

		case KEY_BACK:
			GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;

			KEY_process_global_keys( pkey_event );

			// 6/5/2015 ajv : Since the user got here from the TECHNICAL SUPPORT dialog box, redraw that
			// dialog box.
			TECH_SUPPORT_draw_dialog( (true) );
			break;

		default:
			KEY_process_global_keys( pkey_event );
    }
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

