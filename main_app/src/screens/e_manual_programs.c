/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"e_manual_programs.h"

#include	"app_startup.h"

#include	"change.h"

#include	"combobox.h"

#include	"configuration_controller.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"e_station_selection_grid.h"

#include	"epson_rx_8025sa.h"

#include	"group_base_file.h"

#include	"e_keyboard.h"

#include	"station_groups.h"

#include	"manual_programs.h"

#include	"speaker.h"

#include	"scrollbox.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_MANUAL_PROGRAMS_NAME				(0)
#define	CP_MANUAL_PROGRAMS_WATER_DAY_SUN	(1)
#define	CP_MANUAL_PROGRAMS_WATER_DAY_MON	(2)
#define	CP_MANUAL_PROGRAMS_WATER_DAY_TUE	(3)
#define	CP_MANUAL_PROGRAMS_WATER_DAY_WED	(4)
#define	CP_MANUAL_PROGRAMS_WATER_DAY_THU	(5)
#define	CP_MANUAL_PROGRAMS_WATER_DAY_FRI	(6)
#define	CP_MANUAL_PROGRAMS_WATER_DAY_SAT	(7)
#define CP_MANUAL_PROGRAMS_START_TIME_1		(8)
#define CP_MANUAL_PROGRAMS_START_TIME_2		(9)
#define CP_MANUAL_PROGRAMS_START_TIME_3		(10)
#define CP_MANUAL_PROGRAMS_START_TIME_4		(11)
#define CP_MANUAL_PROGRAMS_START_TIME_5		(12)
#define CP_MANUAL_PROGRAMS_START_TIME_6		(13)
#define CP_MANUAL_PROGRAMS_START_DATE		(14)
#define CP_MANUAL_PROGRAMS_END_DATE			(15)
#define CP_MANUAL_PROGRAMS_RUN_TIME			(16)

#define CP_MANUAL_PROGRAMS_VIEW_STATIONS	(17)
#define CP_MANUAL_PROGRAMS_ADD_STATIONS		(18)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static BOOL_32 g_MANUAL_PROGRAMS_editing_group;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void FDTO_MANUAL_PROGRAMS_return_to_menu( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void MANUAL_PROGRAMS_update_will_run( void )
{
	#define	MANUAL_PROGRAM_WILL_NOT_RUN_REASON_WATER_DAYS	(0)
	#define	MANUAL_PROGRAM_WILL_NOT_RUN_REASON_START_TIMES	(1)
	#define	MANUAL_PROGRAM_WILL_NOT_RUN_REASON_DAYS_TO_RUN	(2)
	#define	MANUAL_PROGRAM_WILL_NOT_RUN_REASON_RUN_TIME		(3)
	#define	MANUAL_PROGRAM_WILL_NOT_RUN_REASON_NO_STATIONS	(4)
	#define	MANUAL_PROGRAM_WILL_NOT_RUN_REASON_UNHANDLED	(5)

	DATE_TIME	ldt;

	BOOL_32		today_is_in_the_schedule;

	BOOL_32		a_future_day_is_in_the_schedule;

	BOOL_32		a_future_day_is_a_water_day;

	BOOL_32		there_are_start_times;

	BOOL_32		there_is_a_start_time_later_today;

	UNS_32		day, month, year, dow;

	BOOL_32		today_is_a_water_day;

	UNS_32		start_date;

	UNS_32		i;

	UNS_32		lprevious_state;

	UNS_32		lprevious_reason;

	// ----------

	lprevious_state = GuiVar_ManualPWillRun;

	lprevious_reason = GuiVar_ManualPWillNotRunReason;

	// ----------

	// 5/20/2015 ajv : First, let's check to see whether today is in the schedule.
	EPSON_obtain_latest_time_and_date( &ldt );

	today_is_in_the_schedule = (false);

	if( (g_MANUAL_PROGRAMS_stop_date >= ldt.D) && (g_MANUAL_PROGRAMS_start_date <= ldt.D) )
	{
		today_is_in_the_schedule = (true);
	}

	// ----------

	// 5/20/2015 ajv : Next, look whether there's a day after today that's in the schedule.

	a_future_day_is_in_the_schedule = (false);

	if( (g_MANUAL_PROGRAMS_stop_date >= (UNS_32)(ldt.D + 1)) && (g_MANUAL_PROGRAMS_start_date <= g_MANUAL_PROGRAMS_stop_date) )
	{
		a_future_day_is_in_the_schedule = (true);
	}

	// ----------
	
	// 5/20/2015 ajv : Then, check to see whether we have at least one start time.

	there_are_start_times = (false);

	if( GuiVar_ManualPStartTime1Enabled || GuiVar_ManualPStartTime2Enabled || GuiVar_ManualPStartTime3Enabled || GuiVar_ManualPStartTime4Enabled || GuiVar_ManualPStartTime5Enabled || GuiVar_ManualPStartTime6Enabled )
	{
		there_are_start_times = (true);
	}

	// ----------

	// 5/20/2015 ajv : Then, look to see whether there is a start time later today.

	there_is_a_start_time_later_today = (false);

	if( there_are_start_times )
	{
		if( (GuiVar_ManualPStartTime1Enabled && (GuiVar_ManualPStartTime1 > ldt.T / 60)) ||
			(GuiVar_ManualPStartTime2Enabled && (GuiVar_ManualPStartTime2 > ldt.T / 60)) ||
			(GuiVar_ManualPStartTime3Enabled && (GuiVar_ManualPStartTime3 > ldt.T / 60)) ||
			(GuiVar_ManualPStartTime4Enabled && (GuiVar_ManualPStartTime4 > ldt.T / 60)) ||
			(GuiVar_ManualPStartTime5Enabled && (GuiVar_ManualPStartTime5 > ldt.T / 60)) ||
			(GuiVar_ManualPStartTime6Enabled && (GuiVar_ManualPStartTime6 > ldt.T / 60)) )
		{
			there_is_a_start_time_later_today = (true);
		}
	}

	// ----------

	// 5/20/2015 ajv : Next, determine whether today is a water day.

	today_is_a_water_day = (false);

	if( today_is_in_the_schedule )
	{
		DateToDMY( ldt.D, &day, &month, &year, &dow );

		if( (dow == 0) && GuiVar_ManualPWaterDay_Sun )
		{
			today_is_a_water_day = (true);
		}
		else if( (dow == 1) && GuiVar_ManualPWaterDay_Mon )
		{
			today_is_a_water_day = (true);
		}
		else if( (dow == 2) && GuiVar_ManualPWaterDay_Tue )
		{
			today_is_a_water_day = (true);
		}
		else if( (dow == 3) && GuiVar_ManualPWaterDay_Wed )
		{
			today_is_a_water_day = (true);
		}
		else if( (dow == 4) && GuiVar_ManualPWaterDay_Thu )
		{
			today_is_a_water_day = (true);
		}
		else if( (dow == 5) && GuiVar_ManualPWaterDay_Fri )
		{
			today_is_a_water_day = (true);
		}
		else if( (dow == 6) && GuiVar_ManualPWaterDay_Sat )
		{
			today_is_a_water_day = (true);
		}
	}

	// ----------

	// 5/20/2015 ajv : Next, check whether there is a day after today which is a water day.

	a_future_day_is_a_water_day = (false);

	if( a_future_day_is_in_the_schedule )
	{
		// 5/20/2015 ajv : To evalulate a_future_day_is_a_water_day, we first have to decide to
		// start with the start date or tomorrow... Remember that we already took care of TODAY
		// above as it's a special case.
		if( ldt.D >= g_MANUAL_PROGRAMS_start_date )
		{
			// 5/20/2015 ajv : The use of this could be thwarted by a test of
			// a_future_day_is_in_the_schedule before we enter the 7-day loop.
			start_date = (UNS_32)(ldt.D + 1);
		}
		else
		{
			start_date = g_MANUAL_PROGRAMS_start_date;
		}

		// 5/20/2015 ajv : Now scan through the next 7 days. However, make sure we don't go past the
		// start date.
		for( i = 0; i < DAYS_IN_A_WEEK; ++i )
		{
			DateToDMY( start_date + i, &day, &month, &year, &dow );

			if( (dow == 0) && GuiVar_ManualPWaterDay_Sun )
			{
				a_future_day_is_a_water_day = (true);
			}
			else if( (dow == 1) && GuiVar_ManualPWaterDay_Mon )
			{
				a_future_day_is_a_water_day = (true);
			}
			else if( (dow == 2) && GuiVar_ManualPWaterDay_Tue )
			{
				a_future_day_is_a_water_day = (true);
			}
			else if( (dow == 3) && GuiVar_ManualPWaterDay_Wed )
			{
				a_future_day_is_a_water_day = (true);
			}
			else if( (dow == 4) && GuiVar_ManualPWaterDay_Thu )
			{
				a_future_day_is_a_water_day = (true);
			}
			else if( (dow == 5) && GuiVar_ManualPWaterDay_Fri )
			{
				a_future_day_is_a_water_day = (true);
			}
			else if( (dow == 6) && GuiVar_ManualPWaterDay_Sat )
			{
				a_future_day_is_a_water_day = (true);
			}

			if( start_date + i >= g_MANUAL_PROGRAMS_stop_date )
			{
				// 5/20/2015 ajv : We're about to pass the stop date, so break out of the loop.
				break;
			}
		}
	}

	// ----------

	// 5/20/2015 ajv : Finally, let's determine whether we're actually going to run or not. This
	// will use the local variables we set above as well as verify there are stations and a run
	// time.
	if( (GuiVar_StationSelectionGridStationCount > 0) && (GuiVar_ManualPRunTime > 0) )
	{
		if( (today_is_in_the_schedule && there_is_a_start_time_later_today && today_is_a_water_day) ||
			(a_future_day_is_in_the_schedule && there_are_start_times && a_future_day_is_a_water_day) )
		{
			GuiVar_ManualPWillRun = (true);
		}
		else
		{
			GuiVar_ManualPWillRun = (false);
		}
	}
	else
	{
		GuiVar_ManualPWillRun = (false);
	}

	// 5/20/2015 ajv : Now that we know we're going to run or not, check whether there are NO
	// WATER DAYS that may affect the schedule. Similarly, if we're not going to run, let's
	// figure out why so we can tell the user.
	if( GuiVar_ManualPWillRun )
	{
		// 5/20/2015 ajv : TODO Determine whether NO WATER DAYS will prevent the manual program from
		// running.
	}
	else
	{
		if( GuiVar_ManualPRunTime == 0 )
		{
			GuiVar_ManualPWillNotRunReason = MANUAL_PROGRAM_WILL_NOT_RUN_REASON_RUN_TIME;
		}
		else if( GuiVar_StationSelectionGridStationCount == 0 )
		{
			GuiVar_ManualPWillNotRunReason = MANUAL_PROGRAM_WILL_NOT_RUN_REASON_NO_STATIONS;
		}
		else if( !there_are_start_times )
		{
			GuiVar_ManualPWillNotRunReason = MANUAL_PROGRAM_WILL_NOT_RUN_REASON_START_TIMES;
		}
		else if( !today_is_in_the_schedule && !a_future_day_is_in_the_schedule )
		{
			GuiVar_ManualPWillNotRunReason = MANUAL_PROGRAM_WILL_NOT_RUN_REASON_DAYS_TO_RUN;
		}
		else if( !today_is_a_water_day && !a_future_day_is_a_water_day )
		{
			GuiVar_ManualPWillNotRunReason = MANUAL_PROGRAM_WILL_NOT_RUN_REASON_WATER_DAYS;
		}
		else if( today_is_in_the_schedule && !a_future_day_is_in_the_schedule )
		{
			if( !there_is_a_start_time_later_today )
			{
				GuiVar_ManualPWillNotRunReason = MANUAL_PROGRAM_WILL_NOT_RUN_REASON_START_TIMES;
			}
			else if( !today_is_a_water_day )
			{
				GuiVar_ManualPWillNotRunReason = MANUAL_PROGRAM_WILL_NOT_RUN_REASON_DAYS_TO_RUN;
			}
		}
		else if( !today_is_in_the_schedule && a_future_day_is_in_the_schedule )
		{
			if( !a_future_day_is_a_water_day )
			{
				GuiVar_ManualPWillNotRunReason = MANUAL_PROGRAM_WILL_NOT_RUN_REASON_START_TIMES;
			}
			else
			{
				// 5/20/2015 ajv : We should never get here. It should be watering...
				GuiVar_ManualPWillNotRunReason = MANUAL_PROGRAM_WILL_NOT_RUN_REASON_UNHANDLED;
			}
		}
		else if( today_is_in_the_schedule && a_future_day_is_in_the_schedule )
		{
			// 5/20/2015 ajv : Handle the case of today is a water day but there are no start times left
			// for today and no other days in the schedule are water days.
			if( today_is_a_water_day && !there_is_a_start_time_later_today && !a_future_day_is_a_water_day )
			{
				// 5/20/2015 ajv : Take your pick. Tell the user to check his water days or start times.
				// Let's do start times for now, since this is what the ET2000e did.
				GuiVar_ManualPWillNotRunReason = MANUAL_PROGRAM_WILL_NOT_RUN_REASON_START_TIMES;
			}
			else if( !today_is_a_water_day && !a_future_day_is_a_water_day )
			{
				GuiVar_ManualPWillNotRunReason = MANUAL_PROGRAM_WILL_NOT_RUN_REASON_WATER_DAYS;
			}
			else
			{
				// 5/20/2015 ajv : We should never get here. It should be watering...
				GuiVar_ManualPWillNotRunReason = MANUAL_PROGRAM_WILL_NOT_RUN_REASON_UNHANDLED;
			}
		}
		else
		{
			// 5/20/2015 ajv : We should never get here.
			GuiVar_ManualPWillNotRunReason = MANUAL_PROGRAM_WILL_NOT_RUN_REASON_UNHANDLED;
		}
	}

	// ----------

	// 5/20/2015 ajv : If we've updated one of the GuiVars, redraw the screen since this causes
	// structures to appear and disappear.
	if( (lprevious_state != GuiVar_ManualPWillRun) || (lprevious_reason != GuiVar_ManualPWillNotRunReason) )
	{
		Redraw_Screen( (false) );
	}
}

/* ---------------------------------------------------------- */
static void MANUAL_PROGRAMS_process_water_day()
{
	switch( GuiLib_ActiveCursorFieldNo )
	{
		case CP_MANUAL_PROGRAMS_WATER_DAY_SUN:
			process_bool( &GuiVar_ManualPWaterDay_Sun );
			break;

		case CP_MANUAL_PROGRAMS_WATER_DAY_MON:
			process_bool( &GuiVar_ManualPWaterDay_Mon );
			break;

		case CP_MANUAL_PROGRAMS_WATER_DAY_TUE:
			process_bool( &GuiVar_ManualPWaterDay_Tue );
			break;

		case CP_MANUAL_PROGRAMS_WATER_DAY_WED:
			process_bool( &GuiVar_ManualPWaterDay_Wed );
			break;

		case CP_MANUAL_PROGRAMS_WATER_DAY_THU:
			process_bool( &GuiVar_ManualPWaterDay_Thu );
			break;

		case CP_MANUAL_PROGRAMS_WATER_DAY_FRI:
			process_bool( &GuiVar_ManualPWaterDay_Fri );
			break;

		case CP_MANUAL_PROGRAMS_WATER_DAY_SAT:
			process_bool( &GuiVar_ManualPWaterDay_Sat );
			break;

		default:
			bad_key_beep();
	}

	Refresh_Screen();

	if( GuiLib_ActiveCursorFieldNo < CP_MANUAL_PROGRAMS_WATER_DAY_SAT )
	{
		CURSOR_Down( (false) );
	}
}

/* ---------------------------------------------------------- */
static void MANUAL_PROGRAMS_process_start_time( const UNS_32 pkeycode, UNS_32 *pstart_time_ptr, UNS_32 *pstart_time_enabled_ptr )
{
	process_uns32( pkeycode, pstart_time_ptr, (UNS_32)(MANUAL_PROGRAMS_START_TIME_MIN / 60), (UNS_32)(SCHEDULE_OFF_TIME / 60), 10, (true) );

	*pstart_time_enabled_ptr = ( *pstart_time_ptr < (UNS_32)(SCHEDULE_OFF_TIME / 60) );
}

/* ---------------------------------------------------------- */
static void MANUAL_PROGRAMS_process_start_date( const UNS_32 pkeycode )
{
	char str_48[ 48 ];

	DATE_TIME ldt;

	EPSON_obtain_latest_time_and_date( &ldt );

	// 4/22/2015 ajv : If the start date is before today when the user tries to edit it,
	// automatically jump to today.
	if( g_MANUAL_PROGRAMS_start_date < ldt.D )
	{
		good_key_beep();

		g_MANUAL_PROGRAMS_start_date = ldt.D;
	}
	else
	{
		process_uns32( pkeycode, &g_MANUAL_PROGRAMS_start_date, ldt.D, g_MANUAL_PROGRAMS_stop_date, 1, (false) );
	}

	strlcpy( GuiVar_ManualPStartDateStr, GetDateStr(str_48, sizeof(str_48), g_MANUAL_PROGRAMS_start_date, DATESTR_show_short_year, DATESTR_show_short_dow), sizeof(GuiVar_ManualPStartDateStr) );

	if( g_MANUAL_PROGRAMS_stop_date < g_MANUAL_PROGRAMS_start_date )
	{
		g_MANUAL_PROGRAMS_stop_date = g_MANUAL_PROGRAMS_start_date;

		strlcpy( GuiVar_ManualPEndDateStr, GetDateStr(str_48, sizeof(str_48), g_MANUAL_PROGRAMS_stop_date, DATESTR_show_short_year, DATESTR_show_short_dow), sizeof(GuiVar_ManualPEndDateStr) );
	}
}

/* ---------------------------------------------------------- */
static void MANUAL_PROGRAMS_process_end_date( const UNS_32 pkeycode )
{
	char str_48[ 48 ];

	DATE_TIME ldt;

	EPSON_obtain_latest_time_and_date( &ldt );

	process_uns32( pkeycode, &g_MANUAL_PROGRAMS_stop_date, g_MANUAL_PROGRAMS_start_date, DATE_MAXIMUM, 1, (false) );

	strlcpy( GuiVar_ManualPEndDateStr, GetDateStr(str_48, sizeof(str_48), g_MANUAL_PROGRAMS_stop_date, DATESTR_show_short_year, DATESTR_show_short_dow), sizeof(GuiVar_ManualPEndDateStr) );
}

/* ---------------------------------------------------------- */
static void MANUAL_PROGRAMS_add_new_group( void )
{
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	nm_GROUP_create_new_group_from_UI( (void*)&nm_MANUAL_PROGRAMS_create_new_group, (void*)&MANUAL_PROGRAMS_copy_group_into_guivars );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Processes the key event from the Manual Programs screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 11/4/2011 Adrianusv
 *
 * @revisions
 *    11/4/2011 Initial release
 */
static void MANUAL_PROGRAMS_process_group( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_dlgKeyboard_0:
			if( ((pkey_event.keycode == KEY_BACK) || ((pkey_event.keycode == KEY_SELECT) && (GuiLib_ActiveCursorFieldNo == 49 /*CP_QWERTY_KEYBOARD_OK*/))) )
			{
				MANUAL_PROGRAMS_extract_and_store_group_name_from_GuiVars();
			}

			KEYBOARD_process_key( pkey_event, &FDTO_MANUAL_PROGRAMS_draw_menu );
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_MANUAL_PROGRAMS_NAME:
							GROUP_process_show_keyboard( 168, 27 );
							break;

						case CP_MANUAL_PROGRAMS_WATER_DAY_SUN:
						case CP_MANUAL_PROGRAMS_WATER_DAY_MON:
						case CP_MANUAL_PROGRAMS_WATER_DAY_TUE:
						case CP_MANUAL_PROGRAMS_WATER_DAY_WED:
						case CP_MANUAL_PROGRAMS_WATER_DAY_THU:
						case CP_MANUAL_PROGRAMS_WATER_DAY_FRI:
						case CP_MANUAL_PROGRAMS_WATER_DAY_SAT:
							MANUAL_PROGRAMS_process_water_day();

							Refresh_Screen();

							MANUAL_PROGRAMS_update_will_run();
							break;

						case CP_MANUAL_PROGRAMS_VIEW_STATIONS:
						case CP_MANUAL_PROGRAMS_ADD_STATIONS:
							// 11/4/2014 ajv : Since we're leaving the screen, make sure we save any changes the user
							// has made. Otherwise, they will be lost.
							MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars();

							lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
							lde._03_structure_to_draw = GuiStruct_scrViewStations_0;
							lde._04_func_ptr = (void *)&FDTO_STATION_SELECTION_GRID_draw_screen;
							lde.key_process_func_ptr = STATION_SELECTION_GRID_process_screen;
							lde._06_u32_argument1 = (true);
							lde._07_u32_argument2 = (GuiLib_ActiveCursorFieldNo == CP_MANUAL_PROGRAMS_VIEW_STATIONS);
							lde._08_screen_to_draw = GuiVar_MenuScreenToShow;
							Change_Screen( &lde );
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_MANUAL_PROGRAMS_WATER_DAY_SUN:
						case CP_MANUAL_PROGRAMS_WATER_DAY_MON:
						case CP_MANUAL_PROGRAMS_WATER_DAY_TUE:
						case CP_MANUAL_PROGRAMS_WATER_DAY_WED:
						case CP_MANUAL_PROGRAMS_WATER_DAY_THU:
						case CP_MANUAL_PROGRAMS_WATER_DAY_FRI:
						case CP_MANUAL_PROGRAMS_WATER_DAY_SAT:
							MANUAL_PROGRAMS_process_water_day();
							break;

						case CP_MANUAL_PROGRAMS_START_TIME_1:
							MANUAL_PROGRAMS_process_start_time( pkey_event.keycode, &GuiVar_ManualPStartTime1, &GuiVar_ManualPStartTime1Enabled );
							break;

						case CP_MANUAL_PROGRAMS_START_TIME_2:
							MANUAL_PROGRAMS_process_start_time( pkey_event.keycode, &GuiVar_ManualPStartTime2, &GuiVar_ManualPStartTime2Enabled );
							break;

						case CP_MANUAL_PROGRAMS_START_TIME_3:
							MANUAL_PROGRAMS_process_start_time( pkey_event.keycode, &GuiVar_ManualPStartTime3, &GuiVar_ManualPStartTime3Enabled );
							break;

						case CP_MANUAL_PROGRAMS_START_TIME_4:
							MANUAL_PROGRAMS_process_start_time( pkey_event.keycode, &GuiVar_ManualPStartTime4, &GuiVar_ManualPStartTime4Enabled );
							break;

						case CP_MANUAL_PROGRAMS_START_TIME_5:
							MANUAL_PROGRAMS_process_start_time( pkey_event.keycode, &GuiVar_ManualPStartTime5, &GuiVar_ManualPStartTime5Enabled );
							break;

						case CP_MANUAL_PROGRAMS_START_TIME_6:
							MANUAL_PROGRAMS_process_start_time( pkey_event.keycode, &GuiVar_ManualPStartTime6, &GuiVar_ManualPStartTime6Enabled );
							break;

						case CP_MANUAL_PROGRAMS_START_DATE:
							MANUAL_PROGRAMS_process_start_date( pkey_event.keycode );
							break;

						case CP_MANUAL_PROGRAMS_END_DATE:
							MANUAL_PROGRAMS_process_end_date( pkey_event.keycode );
							break;

						case CP_MANUAL_PROGRAMS_RUN_TIME:
							process_fl( pkey_event.keycode, &GuiVar_ManualPRunTime, (STATION_MANUAL_PROGRAM_SECONDS_MIN / 60.0), (STATION_MANUAL_PROGRAM_SECONDS_MAX / 60.0), 1.0F, (false) );
							break;

						default:
							bad_key_beep();
					}

					Refresh_Screen();

					MANUAL_PROGRAMS_update_will_run();
					break;

				case KEY_NEXT:
				case KEY_PREV:
					GROUP_process_NEXT_and_PREV( pkey_event.keycode, MANUAL_PROGRAMS_get_num_groups_in_use(), (void*)&MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars, (void*)&MANUAL_PROGRAMS_get_group_at_this_index, (void*)&MANUAL_PROGRAMS_copy_group_into_guivars );
					break;

				case KEY_C_UP:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_MANUAL_PROGRAMS_WATER_DAY_SUN:
						case CP_MANUAL_PROGRAMS_WATER_DAY_MON:
						case CP_MANUAL_PROGRAMS_WATER_DAY_TUE:
						case CP_MANUAL_PROGRAMS_WATER_DAY_WED:
						case CP_MANUAL_PROGRAMS_WATER_DAY_THU:
						case CP_MANUAL_PROGRAMS_WATER_DAY_FRI:
						case CP_MANUAL_PROGRAMS_WATER_DAY_SAT:
							CURSOR_Select( CP_MANUAL_PROGRAMS_NAME, (true) );
							break;

						case CP_MANUAL_PROGRAMS_START_TIME_1:
						case CP_MANUAL_PROGRAMS_START_TIME_2:
						case CP_MANUAL_PROGRAMS_START_TIME_3:
							CURSOR_Select( CP_MANUAL_PROGRAMS_WATER_DAY_SUN, (true) );
							break;

						case CP_MANUAL_PROGRAMS_START_TIME_4:
						case CP_MANUAL_PROGRAMS_START_TIME_5:
						case CP_MANUAL_PROGRAMS_START_TIME_6:
							CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo - 3), (true) );
							break;

						case CP_MANUAL_PROGRAMS_START_DATE:
						case CP_MANUAL_PROGRAMS_END_DATE:
							CURSOR_Select( CP_MANUAL_PROGRAMS_START_TIME_4, (true) );
							break;

						case CP_MANUAL_PROGRAMS_RUN_TIME:
							CURSOR_Select( CP_MANUAL_PROGRAMS_START_DATE, (true) );
							break;

						case CP_MANUAL_PROGRAMS_VIEW_STATIONS:
						case CP_MANUAL_PROGRAMS_ADD_STATIONS:
							CURSOR_Select( CP_MANUAL_PROGRAMS_RUN_TIME, (true) );
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_DOWN:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_MANUAL_PROGRAMS_WATER_DAY_SUN:
						case CP_MANUAL_PROGRAMS_WATER_DAY_MON:
						case CP_MANUAL_PROGRAMS_WATER_DAY_TUE:
						case CP_MANUAL_PROGRAMS_WATER_DAY_WED:
						case CP_MANUAL_PROGRAMS_WATER_DAY_THU:
						case CP_MANUAL_PROGRAMS_WATER_DAY_FRI:
						case CP_MANUAL_PROGRAMS_WATER_DAY_SAT:
							CURSOR_Select( CP_MANUAL_PROGRAMS_START_TIME_1, (true) );
							break;

						case CP_MANUAL_PROGRAMS_START_TIME_1:
						case CP_MANUAL_PROGRAMS_START_TIME_2:
						case CP_MANUAL_PROGRAMS_START_TIME_3:
							CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo + 3), (true) );
							break;

						case CP_MANUAL_PROGRAMS_START_TIME_4:
						case CP_MANUAL_PROGRAMS_START_TIME_5:
						case CP_MANUAL_PROGRAMS_START_TIME_6:
							CURSOR_Select( CP_MANUAL_PROGRAMS_START_DATE, (true) );
							break;

						case CP_MANUAL_PROGRAMS_START_DATE:
						case CP_MANUAL_PROGRAMS_END_DATE:
							CURSOR_Select( CP_MANUAL_PROGRAMS_RUN_TIME, (true) );
							break;

						case CP_MANUAL_PROGRAMS_VIEW_STATIONS:
						case CP_MANUAL_PROGRAMS_ADD_STATIONS:
							bad_key_beep();
							break;

						default:
							CURSOR_Down( (true) );
					}
					break;

				case KEY_C_LEFT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_MANUAL_PROGRAMS_NAME:
							KEY_process_BACK_from_editing_screen( (void*)&FDTO_MANUAL_PROGRAMS_return_to_menu );
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_BACK:
					KEY_process_BACK_from_editing_screen( (void*)&FDTO_MANUAL_PROGRAMS_return_to_menu );
					break;

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Returns to the menu on the left from the editing screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user presses a key to move away from the editing screen and back
 * onto the menu.
 * 
 * @param psave_changes True if the changes should be saved; otherwise, false.
 *
 * @author 11/5/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_MANUAL_PROGRAMS_return_to_menu( void )
{
	FDTO_GROUP_return_to_menu( &g_MANUAL_PROGRAMS_editing_group, (void*)&MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars );
}

/* ---------------------------------------------------------- */
/**
 * Draws the Manual Programs menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the screen is first navigated to.
 * 
 * @param pcomplete_redraw True if the screen should be complete redrawn;
 * otherwise, false. If false, only elements on the screen are updated, not the
 * entire structure, essentially refreshing the content.
 *
 * @author 11/5/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void FDTO_MANUAL_PROGRAMS_draw_menu( const BOOL_32 pcomplete_redraw )
{
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	FDTO_GROUP_draw_menu( pcomplete_redraw, &g_MANUAL_PROGRAMS_editing_group, MANUAL_PROGRAMS_get_num_groups_in_use(), GuiStruct_scrManualPrograms_0, (void *)&MANUAL_PROGRAMS_load_group_name_into_guivar, (void *)&MANUAL_PROGRAMS_copy_group_into_guivars, (true) );

	MANUAL_PROGRAMS_update_will_run();

	if( g_STATION_SELECTION_GRID_user_pressed_back == (true) )
	{
		FDTO_STATION_SELECTION_GRID_redraw_calling_screen( &g_MANUAL_PROGRAMS_editing_group, (CP_MANUAL_PROGRAMS_VIEW_STATIONS + !GuiVar_StationSelectionGridShowOnlyStationsInThisGroup) );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed on the Manual Programs menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 11/5/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void MANUAL_PROGRAMS_process_menu( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	GROUP_process_menu( pkey_event, &g_MANUAL_PROGRAMS_editing_group, MANUAL_PROGRAMS_get_num_groups_in_use(), (void *)&MANUAL_PROGRAMS_process_group, (void *)&MANUAL_PROGRAMS_add_new_group, (void *)&MANUAL_PROGRAMS_get_group_at_this_index, (void *)&MANUAL_PROGRAMS_copy_group_into_guivars );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

