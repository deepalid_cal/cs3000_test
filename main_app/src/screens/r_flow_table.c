/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"r_flow_table.h"

#include	"app_startup.h"

#include	"battery_backed_vars.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"report_utils.h"

#include	"scrollbox.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32 g_FLOW_TABLE_index_of_system_to_show;

#if 0
	static BOOL_32 g_FLOW_TABLE_show_iterations;
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Returns the value within the the flow table for the specified location.
 * 
 * @mutex Calling function must have the system_preserves_recursive_MUTEX mutex
 *  	  to ensure the system preserves doesn't change during this process.
 *
 * @executed Executed within the context of the display processing task when the 
 *  		 screen is drawn; within the context of the key processing task when
 *  		 the user navigates up or down the scroll box.
 * 
 * @param pbsr A pointer to the by system record.
 * @param pline_index_0_i16 The highlighted scroll box line - 0 based.
 * @param pcol_number_0 The column number - 0 based.
 * 
 * @return float The value contained within the specified cell.
 *
 * @author 6/25/2012 Adrianusv
 *
 * @revisions
 *    6/25/2012 Initial release
 */
static float nm_FLOW_TABLE_get_cell_value( BY_SYSTEM_RECORD *pbsr, INT_16 pline_index_0_i16, UNS_32 pcol_number_0 )
{
	float rv;

	if( (((UNS_32)pline_index_0_i16 * (pbsr->flow_check_derate_table_max_stations_ON-1)) + (pcol_number_0)) < DERATE_TABLE_CELLS )
	{
		#if 0
		if( g_FLOW_TABLE_show_iterations == (true) )
		{
			rv = pbsr->derate_cell_iterations[ (((UNS_32)pline_index_0_i16 * (pbsr->flow_check_derate_table_max_stations_ON-1)) + pcol_number_0) ];
		}
		else
		{
			rv = pbsr->derate_table_10u[ (((UNS_32)pline_index_0_i16 * (pbsr->flow_check_derate_table_max_stations_ON-1)) + pcol_number_0) ] / 10.0F;
		}
		#else

		rv = pbsr->derate_table_10u[ (((UNS_32)pline_index_0_i16 * (pbsr->flow_check_derate_table_max_stations_ON-1)) + pcol_number_0) ] / 10.0F;

		#endif
	}
	else
	{
		rv = 0.0F;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Draws an individual scroll line for the Flow Table report. This is used as
 * the callback routine for the GuiLib_ScrollBox_Init routine.
 * 
 * @mutex Calling function must have the system_preserves_recursive_MUTEX mutex
 *  	  to ensure the system preserves doesn't change during this process.
 * 
 * @executed Executed within the context of the display processing task when the 
 *  		 screen is drawn; within the context of the key processing task when
 *  		 the user navigates up or down the scroll box.
 * 
 * @param pline_index_0_i16 The highlighted scroll box line - 0 based.
 *
 * @author 06/22/2012 Adrianusv
 *
 * @revisions
 *    06/22/2012 Initial release
 */
static void nm_FLOW_TABLE_load_guivars_for_scroll_line( const INT_16 pline_index_0_i16 )
{
	BY_SYSTEM_RECORD	*bsr;

	bsr = &system_preserves.system[ g_FLOW_TABLE_index_of_system_to_show ];

	GuiVar_FlowTableGPM = ((UNS_32)pline_index_0_i16 * bsr->flow_check_derate_table_gpm_slot_size);

	GuiVar_FlowTable2Sta  = nm_FLOW_TABLE_get_cell_value( bsr, pline_index_0_i16, 0 );
	GuiVar_FlowTable3Sta  = nm_FLOW_TABLE_get_cell_value( bsr, pline_index_0_i16, 1 );
	GuiVar_FlowTable4Sta  = nm_FLOW_TABLE_get_cell_value( bsr, pline_index_0_i16, 2 );
	GuiVar_FlowTable5Sta  = nm_FLOW_TABLE_get_cell_value( bsr, pline_index_0_i16, 3 );
	GuiVar_FlowTable6Sta  = nm_FLOW_TABLE_get_cell_value( bsr, pline_index_0_i16, 4 );
	GuiVar_FlowTable7Sta  = nm_FLOW_TABLE_get_cell_value( bsr, pline_index_0_i16, 5 );
	GuiVar_FlowTable8Sta  = nm_FLOW_TABLE_get_cell_value( bsr, pline_index_0_i16, 6 );
	GuiVar_FlowTable9Sta  = nm_FLOW_TABLE_get_cell_value( bsr, pline_index_0_i16, 7 );
	GuiVar_FlowTable10Sta = nm_FLOW_TABLE_get_cell_value( bsr, pline_index_0_i16, 8 );
}

/* ---------------------------------------------------------- */
/**
 * Redraws the scroll box on the Flow Table report. This is currently not
 * called, but may be called by the TD Check task to update the report
 * real-time.
 *
 * @executed Executed within the context of the TD Check task when the screen is
 *  		 refreshed every 250 milliseconds.
 * 
 * @param pcomplete_redraw True if the entire screen should be redrawn, as is
 *                         the case when the index changes; otherwise, false.
 *
 * @author 06/22/2012 Adrianusv
 *
 * @revisions
 *    06/22/2012 Initial release
 */
extern void FDTO_FLOW_TABLE_redraw_scrollbox( void )
{
	FDTO_SYSTEM_load_system_name_into_guivar( g_FLOW_TABLE_index_of_system_to_show );

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	FDTO_SCROLL_BOX_redraw_retaining_topline( 0, system_preserves.system[ g_FLOW_TABLE_index_of_system_to_show ].flow_check_derate_table_number_of_gpm_slots, (false) );

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Draws the Flow Table report.
 * 
 * @executed Executed within the context of the display processing task when the
 *           screen is initially drawn.
 * 
 * @author 06/22/2012 Adrianusv
 *
 * @revisions
 *    06/22/2012 Initial release
 */
extern void FDTO_FLOW_TABLE_draw_report( const BOOL_32 pcomplete_redraw )
{
	GuiLib_ShowScreen( GuiStruct_rptFlowTable_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );

	if( pcomplete_redraw == (true) )
	{
		g_FLOW_TABLE_index_of_system_to_show = 0;

		FDTO_SYSTEM_load_system_name_into_guivar( g_FLOW_TABLE_index_of_system_to_show );
	}

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	FDTO_REPORTS_draw_report( pcomplete_redraw, system_preserves.system[ g_FLOW_TABLE_index_of_system_to_show ].flow_check_derate_table_number_of_gpm_slots, &nm_FLOW_TABLE_load_guivars_for_scroll_line );

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while on the Irrigation Details report.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 *
 * @param pkey_event The key event to be processed.
 *
 * @author 06/22/2012 Adrianusv
 *
 * @revisions
 *    06/22/2012 Initial release
 */
extern void FLOW_TABLE_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	UNS_32	lkey;

	switch( pkey_event.keycode )
	{
		case KEY_NEXT:
		case KEY_PREV:
			if( pkey_event.keycode == KEY_NEXT )
			{
				lkey = KEY_PLUS;
			}
			else
			{
				lkey = KEY_MINUS;
			}

			// Take the system mutex to get the list count
			xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

			process_uns32( lkey, &g_FLOW_TABLE_index_of_system_to_show, 0, (SYSTEM_num_systems_in_use()-1), 1, (true) );

			xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = (void*)&FDTO_FLOW_TABLE_redraw_scrollbox;
			Display_Post_Command( &lde );
			break;

		default:
			REPORTS_process_report( pkey_event, 0, 0 );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

