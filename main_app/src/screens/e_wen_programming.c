/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/23/2015 mpd : Required for atoi
#include	<stdlib.h>

// 4/21/2015 ajv : Required for strlcpy.
#include	<string.h>

#include	"e_wen_programming.h"

#include	"e_wen_wifi_settings.h"

#include	"device_common.h"

#include	"device_WEN_PremierWaveXN.h"

#include	"device_WEN_WiBox.h"

#include	"e_wen_wifi_settings.h"

#include	"combobox.h"

#include	"configuration_controller.h"

#include	"cursor_utils.h"

#include	"d_comm_options.h"

#include	"d_process.h"

#include	"d_device_exchange.h"

#include	"dialog.h"

#include	"e_keyboard.h"

#include	"group_base_file.h"

#include	"speaker.h"

#include	"alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_WEN_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY	(0)
#define	CP_WEN_PROGRAMMING_DHCP_NAME				(1)
#define	CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_1		(2)
#define	CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_2		(3)
#define	CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_3		(4)
#define	CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_4		(5)
#define	CP_WEN_PROGRAMMING_SUBNET_MASK				(6)
#define	CP_WEN_PROGRAMMING_GATEWAY_OCTET_1			(7)
#define	CP_WEN_PROGRAMMING_GATEWAY_OCTET_2			(8)
#define	CP_WEN_PROGRAMMING_GATEWAY_OCTET_3			(9)
#define	CP_WEN_PROGRAMMING_GATEWAY_OCTET_4			(10)
#define	CP_WEN_PROGRAMMING_WIFI_SETTINGS			(11)
#define	CP_WEN_PROGRAMMING_READ_DEVICE				(12)
#define	CP_WEN_PROGRAMMING_PROGRAM_DEVICE			(13)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/22/2015 ajv : Since only the draw fuction is initially told which port the device is
// on, copy that port into a global variable that's accessible from the key processing task.
static UNS_32	g_WEN_PROGRAMMING_port;

static UNS_32	g_WEN_PROGRAMMING_previous_cursor_pos;

static BOOL_32	g_WEN_PROGRAMMING_PW_querying_device;

static BOOL_32	g_WEN_PROGRAMMING_read_after_write_pending = false;

static BOOL_32	g_WEN_PROGRAMMING_editing_wifi_settings = false;

static UNS_32	g_WEN_PROGRAMMING_return_to_cp_after_wifi = CP_WEN_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void set_wen_programming_struct_from_guivars()
{
	if( GuiVar_WENRadioType == COMM_DEVICE_WEN_PREMIERWAVE )
	{
		DEV_DETAILS_STRUCT *dev_details;

		WEN_DETAILS_STRUCT *wen_details;

		// ----------

		if( ( dev_state != NULL ) && ( dev_state->dev_details != NULL ) && ( dev_state->wen_details != NULL ) )
		{
			dev_details = dev_state->dev_details;
			wen_details = dev_state->wen_details;

			dev_state->dhcp_enabled = GuiVar_ENObtainIPAutomatically;

			wen_details->mask_bits = ( XE_XN_DEVICE_NETMASK_MAX - GuiVar_ENNetmask );

			// 6/22/2015 mpd : Calsense recommends using the MAC address as the DHCP name. If the user has not set a value 
			// in the name field, use the MAC address. Don't override any string value other than "Not Set".
			if( ( strncmp( GuiVar_ENDHCPName, DEV_NOT_SET_STR, strlen( DEV_NOT_SET_STR ) ) == NULL ) &&
				( wen_details->dhcp_name_not_set ) )
			{
				strlcpy( wen_details->dhcp_name, dev_state->serial_number, sizeof( wen_details->dhcp_name ) );
			}
			else
			{
				strlcpy( wen_details->dhcp_name, GuiVar_ENDHCPName, sizeof( wen_details->dhcp_name ) );
			}

			snprintf( wen_details->ip_address,   sizeof(wen_details->ip_address),   "%d.%d.%d.%d", GuiVar_ENIPAddress_1, GuiVar_ENIPAddress_2, GuiVar_ENIPAddress_3, GuiVar_ENIPAddress_4 );
			snprintf( wen_details->ip_address_1, sizeof(wen_details->ip_address_1), "%d",          GuiVar_ENIPAddress_1 );
			snprintf( wen_details->ip_address_2, sizeof(wen_details->ip_address_2), "%d",          GuiVar_ENIPAddress_2 );
			snprintf( wen_details->ip_address_3, sizeof(wen_details->ip_address_3), "%d",          GuiVar_ENIPAddress_3 );
			snprintf( wen_details->ip_address_4, sizeof(wen_details->ip_address_4), "%d",          GuiVar_ENIPAddress_4 );

			snprintf( wen_details->gw_address,   sizeof(wen_details->gw_address),   "%d.%d.%d.%d", GuiVar_ENGateway_1, GuiVar_ENGateway_2, GuiVar_ENGateway_3, GuiVar_ENGateway_4 );
			snprintf( wen_details->gw_address_1, sizeof(wen_details->gw_address_1), "%d",          GuiVar_ENGateway_1 );
			snprintf( wen_details->gw_address_2, sizeof(wen_details->gw_address_2), "%d",          GuiVar_ENGateway_2 );
			snprintf( wen_details->gw_address_3, sizeof(wen_details->gw_address_3), "%d",          GuiVar_ENGateway_3 );
			snprintf( wen_details->gw_address_4, sizeof(wen_details->gw_address_4), "%d",          GuiVar_ENGateway_4 );

			// 9/24/2015 mpd : FogBugz #3178: Neither the Gui side or the device side code cares about the mask string.
			// It's the mask bits that count. Remove this field. 
			//strlcpy( wen_details->mask,         GuiVar_ENSubnetMask,  sizeof( wen_details->mask ) );

			// 5/27/2015 mpd : These are read-only values that do not need to be copied back to the programming structure.
			//wen_details->rssi = GuiVar_WENRSSI;
			//strlcpy( wen_details->mac_address, GuiVar_WENMACAddress, sizeof( wen_details->mac_address ) );

			// 5/27/2015 mpd : These WEN GuiVars are shared with EN. They are read-only values that do not need to
			// be copied back to the programming structure.
			//strlcpy( dev_state->model,                  GuiVar_ENModel,           sizeof( dev_state->model ) );
			//strlcpy( dev_state->firmware_version,       GuiVar_ENFirmwareVer,     sizeof( dev_state->firmware_version ) );
			//strlcpy( dev_state->serial_number,          GuiVar_ENMACAddress,      sizeof( dev_state->serial_number ) );
		}
	}
	else // COMM_DEVICE_WEN_WIBOX
	{
		WIBOX_DETAILS_STRUCT *ldetails;

		WIBOX_PROGRAMMABLE_VALUES_STRUCT *lactive_pvs;
		
		// ----------

		if( dev_state != NULL )
		{
			if( ( dev_state->WIBOX_details != NULL ) && ( dev_state->wibox_active_pvs != NULL ) )
			{
				ldetails  = dev_state->WIBOX_details;

				lactive_pvs = dev_state->wibox_active_pvs;

				// ----------

				// 3/29/2016 ajv : Lantronix recommends using the MAC address as the DHCP name. If the user
				// has not set a value in the name field, use the MAC address. Don't override any string
				// value other than "not set".
				if( ( strncmp( GuiVar_ENDHCPName, WIBOX_BASIC_INFO_NAME_NOT_SET_STR, WIBOX_BASIC_INFO_NAME_NOT_SET_LEN ) == NULL ) &&
					( ldetails->dhcp_name_not_set ) )
				{
					strlcpy( lactive_pvs->dhcp_name, ldetails->mac_address, sizeof( lactive_pvs->dhcp_name ) );
				}
				else
				{
					strlcpy( lactive_pvs->dhcp_name, GuiVar_ENDHCPName, sizeof( lactive_pvs->dhcp_name ) );
				}

				// ----------

				// 3/29/2016 ajv : The WiBox UDS1100 will not return a mask field in the response if the
				// mask is "255.255.255.255". Since this is useless anyway, don't allow processing to
				// continue with this value. Force it to something slightly less useless here since
				// modifying the pop up menu in EasyGUI would be a pain for very little gain.
				if( GuiVar_ENNetmask == 0 )
				{
					GuiVar_ENNetmask = 1;
				}

				// ----------

				// 9/23/2015 mpd : FogBugz #3164: The Lantronix WiBox does not use CIDR (Classless Inter
				// Domain Routing) format to define the network mask. The number of bits indicates the
				// available bits for host addressing (e.g. A mask of "8" produces a netmask of
				// "255.255.255.0" which has 8 bits available for hosts on the subnet. Other Lantronix
				// devices use CIDR format for programming (e.g. "192.168.254.150/24" specifies the IP
				// address as well as a network mask of "255.255.255.0"). The CIDR mask bit value indicates
				// the leading ones; not the trailing zeros. The CS3000 GUI code was written for UDS1100.
				// It's backwards for all newer Lantronix devices. The mask numbers are internal to CS3000
				// code. They become external at the GUI and in device programming. To eliminate extensive
				// rework, the GUI side for all device types will retain pre-CIDR mask values. As mask
				// values pass to and from the GUI code to the device code (e.g. "e_wen_programming.c" to
				// "device_WEN_PremierWaveXN.c"), the value will be transformed to match the receiving
				// format.
				ldetails->mask_bits = GuiVar_ENNetmask;

				// ----------

				// 3/29/2016 ajv : Zeros in octets 1 and 2 signal the device to use dynamic IP addressing.
				// If the user has specified zeros, force the DHCP indicator to "Yes". This may override the
				// user's choice.
				if( (GuiVar_ENIPAddress_1 == 0) && (GuiVar_ENIPAddress_2 == 0) )
				{
					GuiVar_ENObtainIPAutomatically = (true);
				}

				// ----------

				dev_state->dhcp_enabled = GuiVar_ENObtainIPAutomatically;

				// 3/29/2016 ajv : The programming side expects a BOOL_32. Make a safe conversion. 
				if( GuiVar_ENObtainIPAutomatically )
				{
					// 3/29/2016 ajv : Octet 3 is a bit map when octets 1 and 2 are zero. BOOTP = 0x4, DHCP =
					// 0x2, AutoIP - 0x1. A "1" in any bit turns that feature OFF. We want DHCP and AutoIP
					// enabled. BOOTP disabled.
					snprintf( lactive_pvs->ip_address,   sizeof(lactive_pvs->ip_address),   "%d.%d.%d.%d", 0, 0, 0, 0 );
					snprintf( lactive_pvs->ip_address_1, sizeof(lactive_pvs->ip_address_1), "%d", 0 );
					snprintf( lactive_pvs->ip_address_2, sizeof(lactive_pvs->ip_address_2), "%d", 0 );
					snprintf( lactive_pvs->ip_address_3, sizeof(lactive_pvs->ip_address_3), "%d", 4 );
					snprintf( lactive_pvs->ip_address_4, sizeof(lactive_pvs->ip_address_4), "%d", 0 );

					snprintf( lactive_pvs->gw_address,   sizeof(lactive_pvs->gw_address),   "%d.%d.%d.%d", 0, 0, 0, 0 );
					snprintf( lactive_pvs->gw_address_1, sizeof(lactive_pvs->gw_address_1), "%d", 0 );
					snprintf( lactive_pvs->gw_address_2, sizeof(lactive_pvs->gw_address_2), "%d", 0 );
					snprintf( lactive_pvs->gw_address_3, sizeof(lactive_pvs->gw_address_3), "%d", 0 );
					snprintf( lactive_pvs->gw_address_4, sizeof(lactive_pvs->gw_address_4), "%d", 0 );
				}
				else
				{
					// 3/29/2016 ajv : Individual IP address octets are used to actually program the device. The
					// 4 octet version is used for decision making. Don't skip setting it's value!
					snprintf( lactive_pvs->ip_address,   sizeof(lactive_pvs->ip_address),   "%d.%d.%d.%d", GuiVar_ENIPAddress_1, GuiVar_ENIPAddress_2, GuiVar_ENIPAddress_3, GuiVar_ENIPAddress_4 );
					snprintf( lactive_pvs->ip_address_1, sizeof(lactive_pvs->ip_address_1), "%d", GuiVar_ENIPAddress_1 );
					snprintf( lactive_pvs->ip_address_2, sizeof(lactive_pvs->ip_address_2), "%d", GuiVar_ENIPAddress_2 );
					snprintf( lactive_pvs->ip_address_3, sizeof(lactive_pvs->ip_address_3), "%d", GuiVar_ENIPAddress_3 );
					snprintf( lactive_pvs->ip_address_4, sizeof(lactive_pvs->ip_address_4), "%d", GuiVar_ENIPAddress_4 );

					snprintf( lactive_pvs->gw_address,   sizeof(lactive_pvs->gw_address),   "%d.%d.%d.%d", GuiVar_ENGateway_1, GuiVar_ENGateway_2, GuiVar_ENGateway_3, GuiVar_ENGateway_4 );
					snprintf( lactive_pvs->gw_address_1, sizeof(lactive_pvs->gw_address_1), "%d", GuiVar_ENGateway_1 );
					snprintf( lactive_pvs->gw_address_2, sizeof(lactive_pvs->gw_address_2), "%d", GuiVar_ENGateway_2 );
					snprintf( lactive_pvs->gw_address_3, sizeof(lactive_pvs->gw_address_3), "%d", GuiVar_ENGateway_3 );
					snprintf( lactive_pvs->gw_address_4, sizeof(lactive_pvs->gw_address_4), "%d", GuiVar_ENGateway_4 );
				}
			}
		}
	}
}

/* ---------------------------------------------------------- */
static void get_guivars_from_wen_programming_struct()
{
	if( GuiVar_WENRadioType == COMM_DEVICE_WEN_PREMIERWAVE )
	{
		DEV_DETAILS_STRUCT *dev_details;

		WEN_DETAILS_STRUCT *wen_details;

		//Alert_Message( "get_guivars_from_wen_programming_struct" );

		if( ( dev_state != NULL ) && ( dev_state->dev_details != NULL ) )
		{
			dev_details = dev_state->dev_details;

			if( dev_state->wen_details != NULL )
			{
				wen_details = dev_state->wen_details;

				GuiVar_ENObtainIPAutomatically = dev_state->dhcp_enabled;

				strlcpy( GuiVar_WENMACAddress, wen_details->mac_address, sizeof( GuiVar_WENMACAddress ) );
				e_SHARED_string_validation( GuiVar_WENMACAddress,		sizeof( GuiVar_WENMACAddress ) );

				// 5/27/2015 mpd : These WEN GuiVars are shared with EN.
				strlcpy( GuiVar_ENModel,           dev_state->model,                  sizeof( GuiVar_ENModel ) );
				strlcpy( GuiVar_ENFirmwareVer,     dev_state->firmware_version,       sizeof( GuiVar_ENFirmwareVer ) );
				strlcpy( GuiVar_ENMACAddress,      dev_state->serial_number,          sizeof( GuiVar_ENMACAddress ) );

				// 9/24/2015 mpd : FogBugz #3178: Neither the Gui side or the device side code cares about the mask string.
				// It's the mask bits that count. Remove this field. 
				//strlcpy( GuiVar_ENSubnetMask,      wen_details->mask,                 sizeof( GuiVar_ENSubnetMask ) );

				e_SHARED_string_validation( GuiVar_ENModel, 			sizeof( GuiVar_ENModel ) );
				e_SHARED_string_validation( GuiVar_ENFirmwareVer,    	sizeof( GuiVar_ENFirmwareVer ) );
				e_SHARED_string_validation( GuiVar_ENMACAddress, 	    sizeof( GuiVar_ENMACAddress ) );
				e_SHARED_string_validation( GuiVar_ENSubnetMask, 		sizeof( GuiVar_ENSubnetMask ) );

				if( wen_details->dhcp_name_not_set )
				{
					GuiVar_ENDHCPNameNotSet = true;
					strlcpy( GuiVar_ENDHCPName, DEV_NOT_SET_STR, sizeof( GuiVar_ENDHCPName ) );
				}
				else
				{
					GuiVar_ENDHCPNameNotSet = false;
					strlcpy( GuiVar_ENDHCPName, wen_details->dhcp_name, sizeof( GuiVar_ENDHCPName ) );
				}

				GuiVar_ENIPAddress_1 = atoi( wen_details->ip_address_1 );
				GuiVar_ENIPAddress_2 = atoi( wen_details->ip_address_2 );
				GuiVar_ENIPAddress_3 = atoi( wen_details->ip_address_3 );
				GuiVar_ENIPAddress_4 = atoi( wen_details->ip_address_4 );

				GuiVar_ENGateway_1 = atoi( wen_details->gw_address_1 );
				GuiVar_ENGateway_2 = atoi( wen_details->gw_address_2 );
				GuiVar_ENGateway_3 = atoi( wen_details->gw_address_3 );
				GuiVar_ENGateway_4 = atoi( wen_details->gw_address_4 );

				// 9/24/2015 mpd : FogBugz #3178: Get the mask bits from the device side. This step was missing.

				// 9/23/2015 mpd : FogBugz #3164: The Lantronix EN UDS1100 does not use CIDR (Classless Inter Domain Routing) format
				// to define the network mask. The number of bits indicates the available bits for host addressing (e.g. A mask of 
				// "8" produces a netmask of "255.255.255.0" which has 8 bits available for hosts on the subnet. Other Lantronix
				// devices use CIDR format for programming (e.g. "192.168.254.150/24" specifies the IP address as well as a network 
				// mask of "255.255.255.0"). The CIDR mask bit value indicates the leading ones; not the trailing zeros.
				// The CS3000 GUI code was written for UDS1100. It's backwards for all newer Lantronix devices. The mask numbers are 
				// internal to CS3000 code. They become external at the GUI and in device programming. To eliminate extensive 
				// rework, the GUI side for all device types will retain pre-CIDR mask values. As mask values pass to and from the 
				// GUI code to the device code (e.g. "e_wen_programming.c" to "device_WEN_PremierWaveXN.c"), the value will be 
				// transformed to match the receiving format.  
				GuiVar_ENNetmask = ( XE_XN_DEVICE_NETMASK_MAX - wen_details->mask_bits );
			}
		}
	}
	else // COMM_DEVICE_WEN_WIBOX
	{
		WIBOX_DETAILS_STRUCT *ldetails;

		WIBOX_PROGRAMMABLE_VALUES_STRUCT *lactive_pvs;

		if( dev_state != NULL )
		{
			if( (dev_state->WIBOX_details != NULL) && (dev_state->wibox_active_pvs != NULL) )
			{
				ldetails = dev_state->WIBOX_details;

				lactive_pvs = dev_state->wibox_active_pvs;

				// 3/29/2016 ajv : We can't get good values from the structure if no READ operation has
				// never been completed (reboot or new radio).
				//if( dev_state->device_settings_are_valid ) 
				if( (true) )
				{
					GuiVar_ENDHCPNameNotSet = ldetails->dhcp_name_not_set;

					if( ldetails->dhcp_name_not_set )
					{
						strlcpy( GuiVar_ENDHCPName, WIBOX_BASIC_INFO_NAME_NOT_SET_STR, sizeof( GuiVar_ENDHCPName ) );
					}
					else
					{
						strlcpy( GuiVar_ENDHCPName, lactive_pvs->dhcp_name, sizeof( GuiVar_ENDHCPName ) );
					}

					// 3/29/2016 ajv : The EN programming code includes CR and LF in strings that are sent directly to the Lantronix 
					// device. Those characters add ugly black boxes to the GUI fields. Get rid of them.  
					strtok( GuiVar_ENDHCPName, "\r\n" );

					// ----------

					strlcpy( GuiVar_ENFirmwareVer, dev_state->firmware_version, sizeof( GuiVar_ENFirmwareVer ) );

					if( strlen( GuiVar_ENFirmwareVer ) == 0 )
					{
						strlcpy( GuiVar_ENFirmwareVer, "unknown", sizeof( GuiVar_ENFirmwareVer ) );
					}

					// ----------

					GuiVar_ENObtainIPAutomatically = dev_state->dhcp_enabled;

					// ----------

					GuiVar_ENGateway_1 = atoi( lactive_pvs->gw_address_1 );
					GuiVar_ENGateway_2 = atoi( lactive_pvs->gw_address_2 );
					GuiVar_ENGateway_3 = atoi( lactive_pvs->gw_address_3 );
					GuiVar_ENGateway_4 = atoi( lactive_pvs->gw_address_4 );

					GuiVar_ENIPAddress_1 = atoi( lactive_pvs->ip_address_1 );
					GuiVar_ENIPAddress_2 = atoi( lactive_pvs->ip_address_2 );
					GuiVar_ENIPAddress_3 = atoi( lactive_pvs->ip_address_3 );
					GuiVar_ENIPAddress_4 = atoi( lactive_pvs->ip_address_4 );

					// ----------

					strlcpy( GuiVar_ENMACAddress, ldetails->mac_address, sizeof( GuiVar_ENMACAddress ) );

					if( strlen( GuiVar_ENMACAddress ) == 0 )
					{
						strlcpy( GuiVar_ENMACAddress, "unknown", sizeof( GuiVar_ENMACAddress ) );
					}

					strlcpy( GuiVar_WENMACAddress, GuiVar_ENMACAddress, sizeof(GuiVar_WENMACAddress) );

					// ----------

					// 4/30/2015 mpd : The Lantronix model response is a long string that looks like:
					// "Device Server Plus+! (Firmware Code:UA)". That's really not what we want to see.
					// The "Model" string we want is "UDS1100-IAP". It will be called "Device" to
					// differentiate it from the Lantronix "Model" string.
					strlcpy( GuiVar_ENModel, ldetails->device, sizeof( GuiVar_ENModel ) );

					if( strlen( GuiVar_ENModel ) == 0 )
					{
						strlcpy( GuiVar_ENModel, "unknown", sizeof( GuiVar_ENModel ) );
					}

					// ----------

					// 9/23/2015 mpd : FogBugz #3164: The Lantronix WiBox does not use CIDR (Classless
					// Inter Domain Routing) format to define the network mask. The number of bits indicates the
					// available bits for host addressing (e.g. A mask of "8" produces a netmask of
					// "255.255.255.0" which has 8 bits available for hosts on the subnet. Other Lantronix
					// devices use CIDR format for programming (e.g. "192.168.254.150/24" specifies the IP
					// address as well as a network mask of "255.255.255.0"). The CIDR mask bit value indicates
					// the leading ones; not the trailing zeros. The CS3000 GUI code was written for UDS1100.
					// It's backwards for all newer Lantronix devices. The mask numbers are internal to CS3000
					// code. They become external at the GUI and in device programming. To eliminate extensive
					// rework, the GUI side for all device types will retain pre-CIDR mask values. As mask
					// values pass to and from the GUI code to the device code (e.g. "e_wen_programming.c" to
					// "device_WEN_PremierWaveXN.c"), the value will be transformed to match the receiving
					// format.
					GuiVar_ENNetmask = ldetails->mask_bits;
				}
			}
		}
	}
}

/* ---------------------------------------------------------- */
static void g_WEN_PROGRAMMING_initialize_guivars()
{
	// 4/20/2015 ajv : Set the device exchange result to 'read_ok' to display the initialized variables.
	GuiVar_CommOptionDeviceExchangeResult = DEVICE_EXCHANGE_KEY_read_settings_completed_ok;

	// 5/27/2015 mpd : If any of these variables do not exist at this time (like DHCP Name when DHCP is disabled), 
	// they will have to be initialized later. 

	// 6/1/2015 mpd : Handle items in screen order.
	strlcpy( GuiVar_ENModel, "", sizeof( GuiVar_ENModel ) );

	GuiVar_ENObtainIPAutomatically = TRUE;

	strlcpy( GuiVar_ENDHCPName, "", sizeof( GuiVar_ENDHCPName ) );

	GuiVar_ENIPAddress_1 = 0;
	GuiVar_ENIPAddress_2 = 0;
	GuiVar_ENIPAddress_3 = 0;
	GuiVar_ENIPAddress_4 = 0;

	strlcpy( GuiVar_ENSubnetMask, "0.0.0.0", sizeof( GuiVar_ENSubnetMask ) );

	GuiVar_ENGateway_1 = 0;
	GuiVar_ENGateway_2 = 0;
	GuiVar_ENGateway_3 = 0;
	GuiVar_ENGateway_4 = 0;

	strlcpy( GuiVar_ENMACAddress,  "", sizeof( GuiVar_ENMACAddress ) );
	strlcpy( GuiVar_WENMACAddress, "", sizeof( GuiVar_WENMACAddress ) );
	strlcpy( GuiVar_ENFirmwareVer, "", sizeof( GuiVar_ENFirmwareVer ) );

	strlcpy( GuiVar_CommOptionInfoText, "", sizeof( GuiVar_CommOptionInfoText ) );
}


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void g_WEN_PROGRAMMING_handle_redraw_cursor( UNS_32 *cursor_to_select )
{
	// 6/16/2015 mpd : This variable saves the WEN Programming screen cursor position before going to the WIFI screen.
	// There are only 2 possibilities: the wifi settings button, and the program device button (if there are setting 
	// errors).
	if( g_WEN_PROGRAMMING_return_to_cp_after_wifi == CP_WEN_PROGRAMMING_WIFI_SETTINGS )
	{
		*cursor_to_select = CP_WEN_PROGRAMMING_WIFI_SETTINGS;

		g_WEN_PROGRAMMING_return_to_cp_after_wifi = CP_WEN_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY;
	}
	else if( g_WEN_PROGRAMMING_return_to_cp_after_wifi == CP_WEN_PROGRAMMING_PROGRAM_DEVICE )
	{
		*cursor_to_select = CP_WEN_PROGRAMMING_PROGRAM_DEVICE;

		g_WEN_PROGRAMMING_return_to_cp_after_wifi = CP_WEN_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY;
	}
	else if( GuiLib_ActiveCursorFieldNo == GuiLib_NO_CURSOR )
	{
		*cursor_to_select = g_WEN_PROGRAMMING_previous_cursor_pos;
	}
	else
	{
		*cursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}	

}

/* ---------------------------------------------------------- */
static void FDTO_WEN_PROGRAMMING_process_device_exchange_key( const UNS_32 pkeycode )
{
	e_SHARED_get_info_text_from_programming_struct();

	switch( pkeycode )
	{
		case DEVICE_EXCHANGE_KEY_read_settings_completed_ok:
		case DEVICE_EXCHANGE_KEY_write_settings_completed_ok:
			if( g_WEN_PROGRAMMING_read_after_write_pending == true )
			{
				// 4/30/2015 mpd : A WRITE operation just completed. We are not done until the subsequent read 
				// completes. Act like a "DEVICE_EXCHANGE_KEY_read_settings_in_progress" key code arrived.

				// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
				GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress );
			}
			else
			{
				// 5/6/2015 mpd : A read operation just completed. We are done.

				// 4/22/2015 ajv : Get values from the programming struct
				get_guivars_from_wen_programming_struct();

				// 6/4/2015 mpd : Get the WIFI Settings too!
				get_wifi_guivars_from_wen_programming_struct();

				// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
				GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_completed_ok );
			}

			// 5/18/2015 ajv : Ensure the flag that indicates a dialog box is open is set to false.
			DIALOG_close_ok_dialog();

			FDTO_WEN_PROGRAMMING_draw_screen( (false), g_WEN_PROGRAMMING_port );
			break;

		case DEVICE_EXCHANGE_KEY_read_settings_error:
		case DEVICE_EXCHANGE_KEY_write_settings_error:

			// 5/7/2015 mpd : WEN programming responds with errors for invalid values after successful READ and WRITE 
			// operations. The user needs to see the current values.
			get_guivars_from_wen_programming_struct();

			// 6/4/2015 mpd : Get the WIFI Settings too!
			get_wifi_guivars_from_wen_programming_struct();

			// 5/7/2015 mpd : Do not break here. Continue.

		case DEVICE_EXCHANGE_KEY_busy_try_again_later:
		case DEVICE_EXCHANGE_KEY_read_settings_in_progress:
		case DEVICE_EXCHANGE_KEY_write_settings_in_progress:
			// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
			GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( pkeycode );

			// 5/18/2015 ajv : Update the progress dialog.
			DEVICE_EXCHANGE_draw_dialog();
			break;
	}
}

/* ---------------------------------------------------------- */
static void FDTO_WEN_PROGRAMMING_redraw_screen( const BOOL_32 pcomplete_redraw )
{
	// 6/15/2015 ajv : Since the FDTO_WEN_PROGRAMMING_draw_screen has two parameters, unlike
	// most other _draw_screen routines, we need to have a redraw routine which accepts a single
	// parameter for cases such as closing a keyboard.
	// 
	// Note that other dialog boxes use the normal _draw_screen routine passing in an
	// unknown value for pport, but that's ok because the complete_redraw parameter will be
	// false so g_WEN_PROGRAMMING_port won't be updated to the unknown value.
	FDTO_WEN_PROGRAMMING_draw_screen( pcomplete_redraw, g_WEN_PROGRAMMING_port );
}

/* ---------------------------------------------------------- */
extern void FDTO_WEN_PROGRAMMING_draw_screen( const BOOL_32 pcomplete_redraw, const UNS_32 pport )
{
	UNS_32	lcursor_to_select;

	// 6/3/2015 mpd : If we have been editing wifi settings, don't treat this like a new screen entry. 
	if( ( pcomplete_redraw == ( true ) ) && ( g_WEN_PROGRAMMING_editing_wifi_settings == ( false ) ) )
	{
		g_WEN_PROGRAMMING_initialize_guivars();

		// 6/2/2015 mpd : The WIFI Settings screen is an extension of this screen space. Initialization needs to be done
		// here and now to allow users to enter and exit that screen without loosing changes. 
		g_WEN_PROGRAMMING_initialize_wifi_guivars();

		// 4/21/2015 ajv : Since we're drawing the screen from scratch, start the screen in the
		// "Reading settings from radio" device exchange mode until we're told otherwise.

		// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
		GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress );
	
		// ----------

		g_WEN_PROGRAMMING_port = pport;

		if( g_WEN_PROGRAMMING_port == UPORT_A )
		{
			GuiVar_WENRadioType = config_c.port_A_device_index;
		}
		else
		{
			GuiVar_WENRadioType = config_c.port_B_device_index;
		}

		// ----------

		// 4/20/2015 ajv : Default to the MODE field since that's much more likely to be changed
		// than the port.
		lcursor_to_select = CP_WEN_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY;
	}
	else
	{
		g_WEN_PROGRAMMING_handle_redraw_cursor( &lcursor_to_select );
	}

	GuiLib_ShowScreen( GuiStruct_scrWENProgramming_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();

	// 6/3/2015 mpd : If we have been editing wifi settings, don't treat this like a new screen entry. 
	if( ( pcomplete_redraw == ( true ) ) && ( g_WEN_PROGRAMMING_editing_wifi_settings == ( false ) ) )
	{
		// 5/18/2015 ajv : Immediately display the dialog to indicate to the user that something is
		// happening
		DEVICE_EXCHANGE_draw_dialog();

		// 4/21/2015 ajv : Start the data retrieval automatically for the user. This isn't necessary, but it's a nicety, 
		// preventing the user from having to press the Read Radio button.
		e_SHARED_start_device_communication( g_WEN_PROGRAMMING_port, DEV_READ_OPERATION, &g_WEN_PROGRAMMING_PW_querying_device);
	}
}

/* ---------------------------------------------------------- */
extern void WEN_PROGRAMMING_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_dlgKeyboard_0:
			if( ((pkey_event.keycode == KEY_BACK) || ((pkey_event.keycode == KEY_SELECT) && (GuiLib_ActiveCursorFieldNo == 49 /*CP_QWERTY_KEYBOARD_OK*/))) )
			{
				// 4/21/2015 ajv : The keyboard routines always use GuiVar_GroupName. Therefore, copy the
				// contents of this variable back into DHCP Name when we're done.
				strlcpy( GuiVar_ENDHCPName, GuiVar_GroupName, sizeof(GuiVar_ENDHCPName) );
			}

			KEYBOARD_process_key( pkey_event, &FDTO_WEN_PROGRAMMING_redraw_screen );
			break;

		case GuiStruct_cbxENSubnetMask_0:
			COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_ENNetmask );
			break;

		case GuiStruct_cbxNoYes_0:
			COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_ENObtainIPAutomatically );
			break;

		case GuiStruct_scrWENWiFiSettings_0:
			// 6/2/2015 mpd : We are on the wifi setting screen. Handle the key event there. 
			WEN_PROGRAMMING_process_wifi_settings( pkey_event );
			break;

		default:
			switch( pkey_event.keycode )
			{
				case DEVICE_EXCHANGE_KEY_busy_try_again_later:

				case DEVICE_EXCHANGE_KEY_read_settings_completed_ok:
				case DEVICE_EXCHANGE_KEY_read_settings_error:
				case DEVICE_EXCHANGE_KEY_read_settings_in_progress:

				case DEVICE_EXCHANGE_KEY_write_settings_completed_ok:
				case DEVICE_EXCHANGE_KEY_write_settings_error:
				case DEVICE_EXCHANGE_KEY_write_settings_in_progress:


					if( (pkey_event.keycode != DEVICE_EXCHANGE_KEY_read_settings_in_progress) && (pkey_event.keycode != DEVICE_EXCHANGE_KEY_write_settings_in_progress) )
					{
						g_WEN_PROGRAMMING_PW_querying_device = (false);
					}

					// 4/21/2015 ajv : Redraw the screen displaying the results or indicating the error
					// condition.
					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._04_func_ptr = (void*)&FDTO_WEN_PROGRAMMING_process_device_exchange_key;
					lde._06_u32_argument1 = pkey_event.keycode;
					Display_Post_Command( &lde );

					if( ( pkey_event.keycode == DEVICE_EXCHANGE_KEY_write_settings_completed_ok ) && 
						( g_WEN_PROGRAMMING_read_after_write_pending == true ) )
					{
						// 5/1/2015 mpd : A WRITE operation successfully completed. We need to initiate a READ to verify the
						// WRITE was successful.
						g_WEN_PROGRAMMING_read_after_write_pending = false;
						e_SHARED_start_device_communication( g_WEN_PROGRAMMING_port, DEV_READ_OPERATION, &g_WEN_PROGRAMMING_PW_querying_device);
					}
					else if( pkey_event.keycode == DEVICE_EXCHANGE_KEY_write_settings_error ) 
					{
						// 6/4/2015 mpd : The WRITE failed to complete. Clear this flag or an extra READ will be waiting
						// to annoy the user some time in the future.
						g_WEN_PROGRAMMING_read_after_write_pending = false;
					}
					break;

				case KEY_SELECT:
					// 4/20/2015 ajv : If we'r already in the middle of a device exchange or syncing radios,
					// ignore the key press.
					if( (GuiVar_CommOptionDeviceExchangeResult != ( e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress  ) ) ) && 
						(GuiVar_CommOptionDeviceExchangeResult != ( e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_write_settings_in_progress ) ) ) )
					{
						switch( GuiLib_ActiveCursorFieldNo )
						{
							case CP_WEN_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY:
								good_key_beep();
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = (void*)&FDTO_e_SHARED_show_obtain_ip_automatically_dropdown;
								Display_Post_Command( &lde );
								break;

							case CP_WEN_PROGRAMMING_DHCP_NAME:
								good_key_beep();

								// 4/21/2015 ajv : The keyboard routines always use GuiVar_GroupName. Therefore, copy the
								// contents of the DHCP Name into this variable first. We'll extract it back out when we're
								// done.
								strlcpy( GuiVar_GroupName, GuiVar_ENDHCPName, sizeof(GuiVar_ENDHCPName) );

								// 4/21/2015 ajv : Blank out the DHCP Name so it is no longer visible on the screen.
								memset( GuiVar_ENDHCPName, 0x00, sizeof(GuiVar_ENDHCPName) );

								KEYBOARD_draw_keyboard( 102, 68, sizeof(GuiVar_ENDHCPName), KEYBOARD_TYPE_QWERTY );
								break;

							case CP_WEN_PROGRAMMING_SUBNET_MASK:
								good_key_beep();
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = (void*)&FDTO_e_SHARED_show_subnet_dropdown;
								Display_Post_Command( &lde );
								break;

							case CP_WEN_PROGRAMMING_WIFI_SETTINGS:
								lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
								lde._02_menu = SCREEN_MENU_SETUP;
								lde._03_structure_to_draw = GuiStruct_scrWENWiFiSettings_0;
								lde._04_func_ptr = (void *)&FDTO_WEN_PROGRAMMING_draw_wifi_settings;
								lde._06_u32_argument1 = (true);
								lde.key_process_func_ptr = WEN_PROGRAMMING_process_wifi_settings;

								// 6/2/2015 mpd : This prevents this screen from initializing all the variables and 
								// power-cycling the device when we hit the BACK key on the WIFI Settings screen.
								g_WEN_PROGRAMMING_editing_wifi_settings = true;

								// 6/4/2015 mpd : Keep the cursor on the wifi settings button when we return.
								g_WEN_PROGRAMMING_return_to_cp_after_wifi = CP_WEN_PROGRAMMING_WIFI_SETTINGS;

								Change_Screen( &lde );
								break;

							case CP_WEN_PROGRAMMING_PROGRAM_DEVICE:

								if( g_WEN_PROGRAMMING_PW_querying_device == (false) )
								{
									// 5/8/2015 mpd : FUTURE: Be sure the firmware version meets minimum requirements.
									//if( dev_state->acceptable_version )

									// 6/22/2015 mpd : We are about to program the device. This range check counts.
									// Reset any invalid fields so programming will be successful.
									if( g_WEN_PROGRAMMING_wifi_values_in_range( true ) ) 
									{
										// 6/15/2015 mpd : FUTURE: Check version before attempting changes.

										good_key_beep();

										// 4/21/2015 ajv : Store the button that was highlighted when the user pressed SELECT. Once
										// the write process is done, we'll revert back to this button.
										g_WEN_PROGRAMMING_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

										// 4/9/2015 mpd : Send any user changes to the programming struct.
										set_wen_programming_struct_from_guivars();

										// 6/4/2015 mpd : Get the WIFI Settings too!
										set_wen_programming_struct_from_wifi_guivars();

										// 4/22/2015 ajv : Trigger save process
										e_SHARED_start_device_communication( g_WEN_PROGRAMMING_port, DEV_WRITE_OPERATION, &g_WEN_PROGRAMMING_PW_querying_device);

										// 6/8/2015 mpd : TODO: REMOVE THE READ DURING DEVELOPMENT
										// 5/6/2015 mpd : This flag will initiate a READ when the WRITE completes.
										g_WEN_PROGRAMMING_read_after_write_pending = true;
									}
									else
									{
										bad_key_beep();
									
										lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
										lde._02_menu = SCREEN_MENU_SETUP;
										lde._03_structure_to_draw = GuiStruct_scrWENWiFiSettings_0;
										lde._04_func_ptr = (void *)&FDTO_WEN_PROGRAMMING_draw_wifi_settings;
										lde._06_u32_argument1 = (true);
										lde.key_process_func_ptr = WEN_PROGRAMMING_process_wifi_settings;

										// 6/2/2015 mpd : This prevents this screen from initializing all the variables and 
										// power-cycling the device when we hit the BACK key on the WIFI Settings screen.
										g_WEN_PROGRAMMING_editing_wifi_settings = true;

										// 6/4/2015 mpd : Keep the cursor on the programming button when we return.
										g_WEN_PROGRAMMING_return_to_cp_after_wifi = CP_WEN_PROGRAMMING_PROGRAM_DEVICE;

										Change_Screen( &lde );
									}

								}
								else
								{
									DIALOG_draw_ok_dialog( GuiStruct_dlgCantProgramRadio_0 );
									bad_key_beep();
								}
								break;

							case CP_WEN_PROGRAMMING_READ_DEVICE:

								if( g_WEN_PROGRAMMING_PW_querying_device == (false) )
								{
									good_key_beep();

									// 4/21/2015 ajv : Store the button that was highlighted when the user pressed SELECT. Once
									// the read process is done, we'll revert back to this button.
									g_WEN_PROGRAMMING_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

									e_SHARED_start_device_communication( g_WEN_PROGRAMMING_port, DEV_READ_OPERATION, &g_WEN_PROGRAMMING_PW_querying_device);
								}
								else
								{
									bad_key_beep();
								}
								break;

							default:
								bad_key_beep();
						}
					}
					else
					{
						bad_key_beep();
					}
					break;

				case KEY_PLUS:
					// 5/15/2015 mpd : TURN OFF THIS DEBUG CODE FOR RELEASE
					//e_SHARED_network_connect_delay_is_over(120);
				case KEY_MINUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_WEN_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY:
							process_bool( &GuiVar_ENObtainIPAutomatically );

							// 4/21/2015 ajv : Since changing this value results in screen elements appearing and
							// disappearing, redraw the whole screen.
							Redraw_Screen( (false) );
							break;

						case CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_1: 
							process_uns32( pkey_event.keycode, &GuiVar_ENIPAddress_1, EN_PROGRAMMING_IP_ADDRESS_OCTET_MIN, EN_PROGRAMMING_IP_ADDRESS_OCTET_MAX, 1, (true) );
							break;

						case CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_2: 
							process_uns32( pkey_event.keycode, &GuiVar_ENIPAddress_2, EN_PROGRAMMING_IP_ADDRESS_OCTET_MIN, EN_PROGRAMMING_IP_ADDRESS_OCTET_MAX, 1, (true) );
							break;

						case CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_3: 
							process_uns32( pkey_event.keycode, &GuiVar_ENIPAddress_3, EN_PROGRAMMING_IP_ADDRESS_OCTET_MIN, EN_PROGRAMMING_IP_ADDRESS_OCTET_MAX, 1, (true) );
							break;

						case CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_4: 
							process_uns32( pkey_event.keycode, &GuiVar_ENIPAddress_4, EN_PROGRAMMING_IP_ADDRESS_OCTET_MIN, EN_PROGRAMMING_IP_ADDRESS_OCTET_MAX, 1, (true) );
							break;

						case CP_WEN_PROGRAMMING_SUBNET_MASK:
							process_uns32( pkey_event.keycode, &GuiVar_ENNetmask, EN_PROGRAMMING_NETMASK_MIN, EN_PROGRAMMING_NETMASK_MAX, 1, (false) );
							break;

						case CP_WEN_PROGRAMMING_GATEWAY_OCTET_1:
							process_uns32( pkey_event.keycode, &GuiVar_ENGateway_1, EN_PROGRAMMING_GATEWAY_OCTET_MIN, EN_PROGRAMMING_GATEWAY_OCTET_MAX, 1, (true) );
							break;

						case CP_WEN_PROGRAMMING_GATEWAY_OCTET_2:
							process_uns32( pkey_event.keycode, &GuiVar_ENGateway_2, EN_PROGRAMMING_GATEWAY_OCTET_MIN, EN_PROGRAMMING_GATEWAY_OCTET_MAX, 1, (true) );
							break;

						case CP_WEN_PROGRAMMING_GATEWAY_OCTET_3:
							process_uns32( pkey_event.keycode, &GuiVar_ENGateway_3, EN_PROGRAMMING_GATEWAY_OCTET_MIN, EN_PROGRAMMING_GATEWAY_OCTET_MAX, 1, (true) );
							break;

						case CP_WEN_PROGRAMMING_GATEWAY_OCTET_4:
							process_uns32( pkey_event.keycode, &GuiVar_ENGateway_4, EN_PROGRAMMING_GATEWAY_OCTET_MIN, EN_PROGRAMMING_GATEWAY_OCTET_MAX, 1, (true) );
							break;

						default:
							bad_key_beep();
					}

					Refresh_Screen();
					break;

				case KEY_C_UP:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_1: 
						case CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_2:
						case CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_3:
						case CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_4:
							g_WEN_PROGRAMMING_previous_cursor_pos = GuiLib_ActiveCursorFieldNo;

							CURSOR_Select( CP_WEN_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY, (true) );
							break;

						case CP_WEN_PROGRAMMING_SUBNET_MASK:
							// 4/21/2015 ajv : If the last cursor position somehow isn't one of the IP address octets,
							// automatically jump to the first octet.
							if( (g_WEN_PROGRAMMING_previous_cursor_pos < CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_1) || (g_WEN_PROGRAMMING_previous_cursor_pos > CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_4) )
							{
								g_WEN_PROGRAMMING_previous_cursor_pos = CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_1;
							}

							// 4/21/2015 ajv : Return back to the last cursor position the user moved down from
							CURSOR_Select( g_WEN_PROGRAMMING_previous_cursor_pos, (true) );
							break;

						case CP_WEN_PROGRAMMING_GATEWAY_OCTET_1: 
						case CP_WEN_PROGRAMMING_GATEWAY_OCTET_2:
						case CP_WEN_PROGRAMMING_GATEWAY_OCTET_3:
						case CP_WEN_PROGRAMMING_GATEWAY_OCTET_4:
							g_WEN_PROGRAMMING_previous_cursor_pos = GuiLib_ActiveCursorFieldNo;

							CURSOR_Select( CP_WEN_PROGRAMMING_SUBNET_MASK, (true) );
							break;

						case CP_WEN_PROGRAMMING_PROGRAM_DEVICE:
							CURSOR_Select( CP_WEN_PROGRAMMING_WIFI_SETTINGS, (true) );
							break;

						case CP_WEN_PROGRAMMING_READ_DEVICE:
							if( GuiVar_ENObtainIPAutomatically )
							{
								CURSOR_Up( (true) );
							}
							else
							{
								// 4/21/2015 ajv : If the last cursor position somehow isn't one of the gateway octets,
								// automatically jump to the first octet.
								//if( (g_WEN_PROGRAMMING_previous_cursor_pos < CP_WEN_PROGRAMMING_GATEWAY_OCTET_1) || (g_WEN_PROGRAMMING_previous_cursor_pos > CP_WEN_PROGRAMMING_GATEWAY_OCTET_4) )
								//{
								//	g_WEN_PROGRAMMING_previous_cursor_pos = CP_WEN_PROGRAMMING_GATEWAY_OCTET_1;
								//}

								// 4/21/2015 ajv : Return back to the last cursor position the user moved down from
								//CURSOR_Select( g_WEN_PROGRAMMING_previous_cursor_pos, (true) );

								// 5/28/2015 mpd : Enable WiFi Programming.
								CURSOR_Select( CP_WEN_PROGRAMMING_WIFI_SETTINGS, (true) );
							}
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_LEFT:
					CURSOR_Up( (true) );
					break;

				case KEY_C_DOWN:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_WEN_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY:
							if( GuiVar_ENObtainIPAutomatically )
							{
								CURSOR_Down( (true) );
							}
							else
							{
								// 4/21/2015 ajv : If the last cursor position somehow isn't one of the IP address octets,
								// automatically jump to the first octet.
								if( (g_WEN_PROGRAMMING_previous_cursor_pos < CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_1) || (g_WEN_PROGRAMMING_previous_cursor_pos > CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_4) )
								{
									g_WEN_PROGRAMMING_previous_cursor_pos = CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_1;
								}

								// 4/21/2015 ajv : Return back to the last cursor position the user moved down from
								CURSOR_Select( g_WEN_PROGRAMMING_previous_cursor_pos, (true) );
							}
							break;

						case CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_1: 
						case CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_2:
						case CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_3:
						case CP_WEN_PROGRAMMING_IP_ADDRESS_OCTET_4:
							g_WEN_PROGRAMMING_previous_cursor_pos = GuiLib_ActiveCursorFieldNo;

							CURSOR_Select( CP_WEN_PROGRAMMING_SUBNET_MASK, (true) );
							break;

						case CP_WEN_PROGRAMMING_SUBNET_MASK:
							// 4/21/2015 ajv : If the last cursor position somehow isn't one of the gateway octets,
							// automatically jump to the first octet.
							if( (g_WEN_PROGRAMMING_previous_cursor_pos < CP_WEN_PROGRAMMING_GATEWAY_OCTET_1) || (g_WEN_PROGRAMMING_previous_cursor_pos > CP_WEN_PROGRAMMING_GATEWAY_OCTET_4) )
							{
								g_WEN_PROGRAMMING_previous_cursor_pos = CP_WEN_PROGRAMMING_GATEWAY_OCTET_1;
							}

							// 4/21/2015 ajv : Return back to the last cursor position the user moved down from
							CURSOR_Select( g_WEN_PROGRAMMING_previous_cursor_pos, (true) );
							break;

						case CP_WEN_PROGRAMMING_GATEWAY_OCTET_1: 
						case CP_WEN_PROGRAMMING_GATEWAY_OCTET_2:
						case CP_WEN_PROGRAMMING_GATEWAY_OCTET_3:
						case CP_WEN_PROGRAMMING_GATEWAY_OCTET_4:
							g_WEN_PROGRAMMING_previous_cursor_pos = GuiLib_ActiveCursorFieldNo;

							//CURSOR_Select( CP_WEN_PROGRAMMING_READ_DEVICE, (true) );

							// 5/28/2015 mpd : Enable WiFi Programming.
							CURSOR_Select( CP_WEN_PROGRAMMING_WIFI_SETTINGS, (true) );
							break;

						case CP_WEN_PROGRAMMING_READ_DEVICE:
							bad_key_beep();
							break;

						default:
							CURSOR_Down( (true) );
							break;
					}
					break;

				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_BACK:
					GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;

					// 6/3/2015 mpd : This will allow the screens to be initialized upon reentry. Any unsaved edits 
					// made on this screen or the WIFI Settings screen have been lost. 

					// 6/3/2015 mpd : We are leaving the screen. Any cursor positions and GuiVar values are lost.
					g_WEN_PROGRAMMING_editing_wifi_settings = false;

					KEY_process_global_keys( pkey_event );

					// 6/5/2015 ajv : Since the user got here from the COMM OPTIONS dialog box, redraw that
					// dialog box.
					COMM_OPTIONS_draw_dialog( (true) );
					break;

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

