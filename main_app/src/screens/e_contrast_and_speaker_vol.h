/* ---------------------------------------------------------- */

#ifndef _INC_E_CONTRAST_AND_SPEAKER_VOL_H
#define _INC_E_CONTRAST_AND_SPEAKER_VOL_H

/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */

extern void FDTO_LCD_draw_backlight_contrast_and_volume( const BOOL_32 pcomplete_redraw );

extern void LCD_process_backlight_contrast_and_volume( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

