/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/21/2015 ajv : Required for strlcpy.
#include	<string.h>

#include	"device_common.h"

#include	"e_gr_programming_pw.h"

#include	"device_GR_PremierWaveXC.h"

#include	"app_startup.h"

#include	"comm_mngr.h"

#include	"configuration_controller.h"

#include	"cursor_utils.h"

#include	"dialog.h"

#include	"d_comm_options.h"

#include	"d_device_exchange.h"

#include	"d_process.h"

#include	"speaker.h"

#include	"alerts.h"

#include	"epson_rx_8025sa.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_GR_PROGRAMMING_READ_DEVICE					(0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/22/2015 ajv : Since only the draw fuction is initially told which port the device is
// on, copy that port into a global variable that's accessible from the key processing task.
static UNS_32	g_GR_PROGRAMMING_PW_port;

static BOOL_32 GR_PROGRAMMING_PW_querying_device;

static BOOL_32 GR_PROGRAMMING_read_after_write_pending = false;

// 4/8/2015 mpd : The users cannot modify any programmable values on the GR PremierWave radio. There is no need for this
// function.
/* ---------------------------------------------------------- */
//static void set_gr_programming_struct_from_guivars()
//{
//	//Alert_Message( "set_gr_programming_struct_from_guivars" );
//}

/* ---------------------------------------------------------- */
static void get_guivars_from_gr_programming_struct()
{
	DEV_DETAILS_STRUCT *dev_details;

	//Alert_Message( "get_guivars_from_gr_programming_struct" );

	if( ( dev_state != NULL ) && ( dev_state->dev_details != NULL ) )
	{
		dev_details = dev_state->dev_details;

		strlcpy( GuiVar_GRModel,           dev_state->model,                  sizeof( GuiVar_GRModel ) );
		strlcpy( GuiVar_GRFirmwareVersion, dev_state->firmware_version,       sizeof( GuiVar_GRFirmwareVersion ) );
		strlcpy( GuiVar_GRRadioSerialNum,  dev_state->serial_number,          sizeof( GuiVar_GRRadioSerialNum ) );
		strlcpy( GuiVar_GRIPAddress,       dev_details->ip_address,           sizeof( GuiVar_GRIPAddress ) );
		strlcpy( GuiVar_GRSIMState,        dev_details->sim_status,           sizeof( GuiVar_GRSIMState ) );
		strlcpy( GuiVar_GRNetworkState,    dev_details->network_status,       sizeof( GuiVar_GRNetworkState ) );
		strlcpy( GuiVar_GRGPRSStatus,      dev_details->packet_domain_status, sizeof( GuiVar_GRGPRSStatus ) );
		strlcpy( GuiVar_GRRSSI,            dev_details->signal_strength,      sizeof( GuiVar_GRRSSI ) );

		// 5/15/2015 mpd : This screen design is currently being reworked. Not all fields will be read at the same time.
		// Display an explanation instead of unexpected white space. 
		e_SHARED_string_validation( GuiVar_GRModel,           sizeof( GuiVar_GRModel ) );             
		e_SHARED_string_validation( GuiVar_GRFirmwareVersion, sizeof( GuiVar_GRFirmwareVersion ) );   
		e_SHARED_string_validation( GuiVar_GRRadioSerialNum,  sizeof( GuiVar_GRRadioSerialNum ) );    
		e_SHARED_string_validation( GuiVar_GRIPAddress,       sizeof( GuiVar_GRIPAddress ) );         
		e_SHARED_string_validation( GuiVar_GRSIMState,        sizeof( GuiVar_GRSIMState ) );          
		e_SHARED_string_validation( GuiVar_GRNetworkState,    sizeof( GuiVar_GRNetworkState ) );      
		e_SHARED_string_validation( GuiVar_GRGPRSStatus,      sizeof( GuiVar_GRGPRSStatus ) );        
		e_SHARED_string_validation( GuiVar_GRRSSI,            sizeof( GuiVar_GRRSSI ) );              
	}

}

/* ---------------------------------------------------------- */
static void GR_PROGRAMMING_initialize_guivars()
{
	// 4/20/2015 ajv : Set the device exchange result to 'read_ok' to display the initialized variables.
	GuiVar_CommOptionDeviceExchangeResult = DEVICE_EXCHANGE_KEY_read_settings_completed_ok;

	strlcpy( GuiVar_GRModel,           "", sizeof( GuiVar_GRModel ) );
	strlcpy( GuiVar_GRFirmwareVersion, "", sizeof( GuiVar_GRFirmwareVersion ) );
	strlcpy( GuiVar_GRRadioSerialNum,  "", sizeof( GuiVar_GRRadioSerialNum ) );
	strlcpy( GuiVar_GRIPAddress,       "", sizeof( GuiVar_GRIPAddress ) );
	strlcpy( GuiVar_GRSIMState,        "", sizeof( GuiVar_GRSIMState ) );
	strlcpy( GuiVar_GRNetworkState,    "", sizeof( GuiVar_GRNetworkState ) );
	strlcpy( GuiVar_GRGPRSStatus,      "", sizeof( GuiVar_GRGPRSStatus ) );
	strlcpy( GuiVar_GRRSSI,            "", sizeof( GuiVar_GRRSSI ) );

	strlcpy( GuiVar_CommOptionInfoText, "", sizeof( GuiVar_CommOptionInfoText ) );
}

/* ---------------------------------------------------------- */
static void FDTO_GR_PROGRAMMING_PW_process_device_exchange_key( const UNS_32 pkeycode )
{
	e_SHARED_get_info_text_from_programming_struct();

	switch( pkeycode )
	{
		case DEVICE_EXCHANGE_KEY_read_settings_completed_ok:
		case DEVICE_EXCHANGE_KEY_write_settings_completed_ok:
			if( GR_PROGRAMMING_read_after_write_pending == true )
			{
				// 4/30/2015 mpd : A WRITE operation just completed. We are not done until the subsequent read 
				// completes. Act like a "DEVICE_EXCHANGE_KEY_read_settings_in_progress" key code arrived.

				// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
				GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress );
			}
			else
			{
				// 5/6/2015 mpd : A read operation just completed. We are done.

				// 4/22/2015 ajv : Get values from the programming struct
				get_guivars_from_gr_programming_struct();

				// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
				GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_completed_ok );
			}

			// 5/18/2015 ajv : Ensure the flag that indicates a dialog box is open is set to false.
			DIALOG_close_ok_dialog();

			FDTO_GR_PROGRAMMING_PW_draw_screen( (false), g_GR_PROGRAMMING_PW_port );
			break;

		case DEVICE_EXCHANGE_KEY_read_settings_error:
		case DEVICE_EXCHANGE_KEY_write_settings_error:

			// 5/7/2015 mpd : GR programming responds with errors for invalid values after successful READ and WRITE 
			// operations. The user needs to see the current values.
			get_guivars_from_gr_programming_struct();

			// 5/7/2015 mpd : Do not break here. Continue.

		case DEVICE_EXCHANGE_KEY_busy_try_again_later:
		case DEVICE_EXCHANGE_KEY_read_settings_in_progress:
		case DEVICE_EXCHANGE_KEY_write_settings_in_progress:
			// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
			GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( pkeycode );

			// 5/18/2015 ajv : Update the progress dialog.
			DEVICE_EXCHANGE_draw_dialog();
			break;
	}
}

/* ---------------------------------------------------------- */
extern void FDTO_GR_PROGRAMMING_PW_draw_screen( const BOOL_32 pcomplete_redraw, const UNS_32 pport )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		GR_PROGRAMMING_initialize_guivars();

		g_GR_PROGRAMMING_PW_port = pport;

		// 4/21/2015 ajv : Since we're drawing the screen from scratch, start the screen in the
		// "Reading settings from radio" device exchange mode until we're told otherwise.

		// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
		GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress );

		lcursor_to_select = CP_GR_PROGRAMMING_READ_DEVICE;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrGRProgramming_PW_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();

	if( pcomplete_redraw == (true) )
	{
		// 5/18/2015 ajv : Immediately display the dialog to indicate to the user that something is
		// happening
		DEVICE_EXCHANGE_draw_dialog();

		// 4/22/2015 ajv : Start the data retrieval automatically for the user. This isn't
		// necessary, but it's a nicety, preventing the user from having to press the Read Radio
		// button.
		e_SHARED_start_device_communication( g_GR_PROGRAMMING_PW_port, DEV_READ_OPERATION, &GR_PROGRAMMING_PW_querying_device );
	}
}

/* ---------------------------------------------------------- */
extern void GR_PROGRAMMING_PW_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( pkey_event.keycode )
	{
		case DEVICE_EXCHANGE_KEY_busy_try_again_later:
		case DEVICE_EXCHANGE_KEY_read_settings_completed_ok:
		case DEVICE_EXCHANGE_KEY_read_settings_error:
		case DEVICE_EXCHANGE_KEY_read_settings_in_progress:
		case DEVICE_EXCHANGE_KEY_write_settings_completed_ok:
		case DEVICE_EXCHANGE_KEY_write_settings_error:
		case DEVICE_EXCHANGE_KEY_write_settings_in_progress:

			if( (pkey_event.keycode != DEVICE_EXCHANGE_KEY_read_settings_in_progress) && (pkey_event.keycode != DEVICE_EXCHANGE_KEY_write_settings_in_progress) )
			{
				GR_PROGRAMMING_PW_querying_device = (false);
			}

			// 4/22/2015 ajv : Redraw the screen displaying the results or indicating the error
			// condition.
			lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
			lde._04_func_ptr = (void*)&FDTO_GR_PROGRAMMING_PW_process_device_exchange_key;
			lde._06_u32_argument1 = pkey_event.keycode;
			Display_Post_Command( &lde );

			//if( ( pkey_event.keycode == DEVICE_EXCHANGE_KEY_write_settings_completed_ok ) && 
			//	( GR_PROGRAMMING_read_after_write_pending == true ) )
			//{
			//	// 5/1/2015 mpd : A WRITE operation successfully completed. We need to initiate a READ to verify the
			//	// WRITE was successful.
			//	GR_PROGRAMMING_read_after_write_pending = false;
			//	e_SHARED_start_device_communication( g_GR_PROGRAMMING_PW_port, E_SHARED_READ, &GR_PROGRAMMING_PW_querying_device );
			//}
			break;

		case KEY_SELECT:
			// 4/20/2015 ajv : If we'r already in the middle of a device exchange or syncing radios, ignore the key press.
			if( (GuiVar_CommOptionDeviceExchangeResult != ( e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress  ) ) ) && 
				(GuiVar_CommOptionDeviceExchangeResult != ( e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_write_settings_in_progress ) ) ) )
			{
				switch( GuiLib_ActiveCursorFieldNo )
				{
					// 6/10/2015 mpd : This function is moving to the diagnostic screen.
					#if 0
					case CP_GR_PROGRAMMING_PROGRAM_DEVICE:

						if( GR_PROGRAMMING_PW_querying_device == (false) )
						{
							// 4/10/2015 mpd : Clear any existing info text for every user key event.  
							e_SHARED_clear_info_text();

							// 5/15/2015 mpd : TBD: The WRITE function for GR is being moved to a "trouble shooting" screen.
							// Do any version checking on that screen.
							// 5/8/2015 mpd : Be sure the firmware version meets minimum requirements.
							//if( dev_state->acceptable_version )
							if( 1 )
							{
								good_key_beep();

								// 4/22/2015 ajv : Trigger save process
								e_SHARED_start_device_communication( g_GR_PROGRAMMING_PW_port, E_SHARED_WRITE, &GR_PROGRAMMING_PW_querying_device );

								// 5/6/2015 mpd : This flag will initiate a READ when the WRITE completes.
								GR_PROGRAMMING_read_after_write_pending = true;
							}
							//else
							//{
							//	bad_key_beep();
							//
							//	// 5/13/2015 mpd : TODO: This message is the default if a WRITE fails due to a timeout. 
							//	// Be sure "dev_state" is valid before making this decision.
							//	strlcpy( GuiVar_CommOptionInfoText,     "Minimum firmware version: 7.9.0.3A2", sizeof( GuiVar_CommOptionInfoText ) );
							//	strlcpy( GuiVar_CommOptionProgressText, "", sizeof( GuiVar_CommOptionProgressText ) );
							//}

						}
						else
						{
							bad_key_beep();
						}
						break;
					#endif

					case CP_GR_PROGRAMMING_READ_DEVICE:

						if( GR_PROGRAMMING_PW_querying_device == (false) )
						{
							good_key_beep();

							e_SHARED_start_device_communication( g_GR_PROGRAMMING_PW_port, DEV_READ_OPERATION, &GR_PROGRAMMING_PW_querying_device );
						}
						else
						{
							bad_key_beep();
						}
						break;

					default:
						bad_key_beep();
				}
			}
			break;
		
		case KEY_C_LEFT:
			CURSOR_Up( (true) );
			break;
		
		case KEY_C_RIGHT:
			CURSOR_Down( (true) );
			break;

		case KEY_PLUS:
			// 5/15/2015 mpd : TURN OFF THIS DEBUG CODE FOR RELEASE
			//e_SHARED_network_connect_delay_is_over(120);
		case KEY_MINUS:
		case KEY_C_UP:
		case KEY_C_DOWN:
			bad_key_beep();
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;

			KEY_process_global_keys( pkey_event );

			// 6/5/2015 ajv : Since the user got here from the COMM OPTIONS dialog box, redraw that
			// dialog box.
			COMM_OPTIONS_draw_dialog( (true) );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

