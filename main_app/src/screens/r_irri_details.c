/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"r_irri_details.h"

#include	"app_startup.h"

#include	"cs3000_comm_server_common.h"

#include	"e_status.h"

#include	"flowsense.h"

#include	"irri_comm.h"

#include	"irri_irri.h"

#include	"m_main.h"

#include	"report_utils.h"

#include	"screen_utils.h"

#include	"scrollbox.h"

#include	"speaker.h"

#include	"stations.h"

#include	"tpmicro_data.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define		IRRI_DETAILS_STATUS_WAITING		(0)
#define		IRRI_DETAILS_STATUS_ON			(1)
#define		IRRI_DETAILS_STATUS_SOAKING		(2)
#define		IRRI_DETAILS_STATUS_PAUSED		(3)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Track the previous number of populated irrigation details lines. If the
// number of populated lines changes between refreshes (e.g., a station turns on
// or off), the entire scroll box must be redrawn. Otherwise, we can simply
// update the variables displayed on the screen.
static UNS_32 g_IRRI_DETAILS_line_count;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void IRRI_DETAILS_jump_to_irrigation_details( void )
{
	DISPLAY_EVENT_STRUCT	lde;

	// Initialize the screen to show to the Irrigation
	// Details report.
	GuiVar_MenuScreenToShow = CP_DIAGNOSTICS_MENU_IRRI_DETAILS;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._02_menu = SCREEN_MENU_DIAGNOSTICS;
	lde._03_structure_to_draw = GuiStruct_rptIrriDetails_0;
	lde._04_func_ptr = (void *)&FDTO_IRRI_DETAILS_draw_report;
	lde._06_u32_argument1 = (true);
	lde._08_screen_to_draw = CP_DIAGNOSTICS_MENU_IRRI_DETAILS;
	lde.key_process_func_ptr = IRRI_DETAILS_process_report;
	Change_Screen( &lde );
}

/* ---------------------------------------------------------- */
/**
 * Draws an individual scroll line for the IRRI irrigation details screen. This 
 * is used as the callback routine for the GuiLib_ScrollBox_Init routine. 
 * 
 * @executed Executed within the context of the display processing task when the 
 *  		 screen is drawn; within the context of the key processing task when
 *  		 the user navigates up or down the scroll box.
 * 
 * @param pline_index_0_i16 The highlighted scroll box line - 0 based.
 *
 * @author 10/12/2011 Adrianusv
 *
 * @revisions
 *    10/12/2011 Initial release
 */
static void nm_FDTO_IRRI_DETAILS_load_guivars_for_scroll_line( const INT_16 pline_index_0_i16 )
{
	IRRI_MAIN_LIST_OF_STATIONS_STRUCT	*iml;

	IRRIGATION_LIST_COMPONENT	*ilc;

	char	str_16[ 16 ];

	UNS_32	lindex;

	INT_32	i;

	// ----------

	if( !GuiVar_IrriDetailsShowFOAL )
	{
		xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );

		iml = nm_ListGetFirst( &irri_irri.list_of_irri_all_irrigation );

		for( i = 0; iml != NULL; ++i )
		{
			if( i == pline_index_0_i16 )
			{
				break;
			}

			iml = nm_ListGetNext( &irri_irri.list_of_irri_all_irrigation, iml );
		}

		// ----------

		if( iml != NULL )
		{
			GuiVar_IrriDetailsController = iml->box_index_0;

			snprintf( GuiVar_IrriDetailsStation, sizeof(GuiVar_IrriDetailsStation), "%s", STATION_get_station_number_string( iml->box_index_0, iml->station_number_0_u8, (char*)&str_16, sizeof(str_16) ) );

			GuiVar_IrriDetailsReason = iml->ibf.w_reason_in_list;
			GuiVar_IrriDetailsProgLeft = ((float)(iml->requested_irrigation_seconds_u32) / 60.0F);
			GuiVar_IrriDetailsProgCycle = ((float)(iml->cycle_seconds) / 60.0F);

			if( iml->ibf.station_is_ON == (true) )
			{
				GuiVar_IrriDetailsRunCycle = ((float)(iml->remaining_ON_or_soak_seconds) / 60.0F);
				GuiVar_IrriDetailsRunSoak = 0.0F;
			}
			else
			{
				GuiVar_IrriDetailsRunCycle = 0.0F;
				GuiVar_IrriDetailsRunSoak = ((float)(iml->remaining_ON_or_soak_seconds) / 60.0F);
			}

			if( iml->ibf.station_is_ON )
			{
				GuiVar_IrriDetailsStatus = IRRI_DETAILS_STATUS_ON;

				// 9/19/2013 ajv : In order to display the latest current draw from
				// station preserves, we need to get the index into the preserves
				// array first. Once we have that, we can pull the last measured
				// current.
				if( STATION_PRESERVES_get_index_using_box_index_and_station_number( iml->box_index_0, (UNS_32)iml->station_number_0_u8, &lindex ) == (true) )
				{
					xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

					GuiVar_IrriDetailsCurrentDraw = ((float)station_preserves.sps[ lindex ].last_measured_current_ma / 1000.0F);

					xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
				}
				else
				{
					GuiVar_IrriDetailsCurrentDraw = 0.00F;
				}
			}
			else if( iml->remaining_ON_or_soak_seconds > 0 )
			{
				GuiVar_IrriDetailsStatus = IRRI_DETAILS_STATUS_SOAKING;

				GuiVar_IrriDetailsCurrentDraw = 0.00F;
			}
			else
			{
				GuiVar_IrriDetailsStatus = IRRI_DETAILS_STATUS_WAITING;

				GuiVar_IrriDetailsCurrentDraw = 0.00F;
			}

			// 3/12/2011 rmd : What have we learned. To get at the description causes us to take the
			// pdata mutex. Which can cause a DEADLOCK between this task and either the COMM_MNGR
			// or the TD_CHECK task. Havent exactly pinned down what is going on but between the use of
			// the foal irri and the irri irri mutex combined with this third pdata mutex we can get
			// into trouble if you are looking at this screen when you hit a start time. So I have
			// commented out this section.
			//
			// IMPORTANT: I think what we need to do is at the start time capture a pointer to the
			// station. Then we can use that pointer to get items for the station. We must note that if
			// there is a station deletion WHILE IT IS IRRIGATING that station must be removed from the
			// irrigation first. There are considerations to be addressed regarding this scenario. 
			GuiVar_StationDescription[ 0 ] = 0x00;
		}

		xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
	}
	else
	{
		xSemaphoreTakeRecursive( irri_irri_recursive_MUTEX, portMAX_DELAY );

		ilc = nm_ListGetFirst( &foal_irri.list_of_foal_all_irrigation );

		for( i = 0; ilc != NULL; ++i )
		{
			if( i == pline_index_0_i16 )
			{
				break;
			}

			ilc = nm_ListGetNext( &foal_irri.list_of_foal_all_irrigation, ilc );
		}

		// ----------

		if( ilc != NULL )
		{
			GuiVar_IrriDetailsController = ilc->box_index_0;

			snprintf( GuiVar_IrriDetailsStation, sizeof(GuiVar_IrriDetailsStation), "%s", STATION_get_station_number_string( ilc->box_index_0, ilc->station_number_0_u8, (char*)&str_16, sizeof(str_16) ) );

			GuiVar_IrriDetailsReason = ilc->bbf.w_reason_in_list;
			GuiVar_IrriDetailsProgLeft = ((float)(ilc->requested_irrigation_seconds_ul) / 60.0F);
			GuiVar_IrriDetailsProgCycle = ((float)(ilc->cycle_seconds_ul) / 60.0F);

			if( ilc->bbf.station_is_ON == (true) )
			{
				GuiVar_IrriDetailsRunCycle = ((float)(ilc->remaining_seconds_ON) / 60.0F);
				GuiVar_IrriDetailsRunSoak = 0.0F;
			}
			else
			{
				GuiVar_IrriDetailsRunCycle = 0.0F;
				GuiVar_IrriDetailsRunSoak = ((float)(ilc->soak_seconds_remaining_ul) / 60.0F);
			}

			if( ilc->bbf.station_is_ON )
			{
				GuiVar_IrriDetailsStatus = IRRI_DETAILS_STATUS_ON;

				// 9/19/2013 ajv : In order to display the latest current draw from station preserves, we
				// need to get the index into the preserves array first. Once we have that, we can pull the
				// last measured current.
				if( STATION_PRESERVES_get_index_using_box_index_and_station_number( ilc->box_index_0, (UNS_32)ilc->station_number_0_u8, &lindex ) == (true) )
				{
					xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

					GuiVar_IrriDetailsCurrentDraw = ((float)station_preserves.sps[ lindex ].last_measured_current_ma / 1000.0F);

					xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
				}
				else
				{
					GuiVar_IrriDetailsCurrentDraw = 0.00F;
				}
			}
			else if( ilc->soak_seconds_remaining_ul > 0 )
			{
				GuiVar_IrriDetailsStatus = IRRI_DETAILS_STATUS_SOAKING;

				GuiVar_IrriDetailsCurrentDraw = 0.00F;
			}
			else
			{
				GuiVar_IrriDetailsStatus = IRRI_DETAILS_STATUS_WAITING;

				GuiVar_IrriDetailsCurrentDraw = 0.00F;
			}

			// 3/12/2011 rmd : What have we learned. To get at the description causes us to take the
			// pdata mutex. Which can cause a DEADLOCK between this task and either the COMM_MNGR
			// or the TD_CHECK task. Havent exactly pinned down what is going on but between the use of
			// the foal irri and the irri irri mutex combined with this third pdata mutex we can get
			// into trouble if you are looking at this screen when you hit a start time. So I have
			// commented out this section.
			//
			// IMPORTANT: I think what we need to do is at the start time capture a pointer to the
			// station. Then we can use that pointer to get items for the station. We must note that if
			// there is a station deletion WHILE IT IS IRRIGATING that station must be removed from the
			// irrigation first. There are considerations to be addressed regarding this scenario. 
			GuiVar_StationDescription[ 0 ] = 0x00;
		}

		xSemaphoreGiveRecursive( irri_irri_recursive_MUTEX );
	}
}

/* ---------------------------------------------------------- */
static void FDTO_IRRI_DETAILS_update_flow_and_current( void )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem_struct;

	BY_SYSTEM_RECORD	*lpreserve;

	UNS_32	i;

	// ----------

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	GuiVar_StatusShowSystemFlow = (POC_at_least_one_POC_has_a_flow_meter() == (true)) && (SYSTEM_num_systems_in_use() > 0);

	if( GuiVar_StatusShowSystemFlow == (true) )
	{
		// 4/15/2016 ajv : For now, tally all of the mainlines' flow for display. If the user wants
		// to see a mainline-by-mainline breakdown, they can go to one of the irrigation details
		// screens.
		GuiVar_StatusSystemFlowActual = 0;

		for( i = 0; i < MAX_POSSIBLE_SYSTEMS; ++i )
		{
			lsystem_struct = SYSTEM_get_group_at_this_index( i );

			if( lsystem_struct != NULL )
			{
				lpreserve = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( nm_GROUP_get_group_ID(lsystem_struct) );

				if( lpreserve != NULL )
				{
					GuiVar_StatusSystemFlowActual += (UNS_32)lpreserve->system_rcvd_most_recent_token_5_second_average;
				}
			}
		}

		// 6/23/2015 ajv : For now, we've decided to show the highest state for any of the mainlines
		// that are in use.
		GuiVar_StatusSystemFlowStatus = SYSTEM_get_highest_flow_checking_status_for_any_system();
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	// ----------------------

	xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );

	GuiVar_StatusCurrentDraw = ((float)tpmicro_data.measured_ma_current_as_rcvd_from_master[ FLOWSENSE_get_controller_index() ] / 1000.0F);

	xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );

	// ----------
}

/* ---------------------------------------------------------- */
/**
 * Redraws the scroll box on the Contact Status report. This is currently not
 * called, but may be called by the TD Check task to update the report
 * real-time.
 *
 * @executed Executed within the context of the TD Check task when the screen is
 *           refreshed every 250 milliseconds.
 *
 * @author 10/12/2011 Adrianusv
 *
 * @revisions
 *    10/12/2011 Initial release
 *    2/9/2012   Renamed function. Modified to support unique draw_scroll_line
 *               functions for IRRI and FOAL lists.
 */
extern void FDTO_IRRI_DETAILS_redraw_scrollbox( void )
{
	// 2011.12.01 rmd : We've had ALOT of trouble taking the foal irri and irri
	// irri MUTEXES as one might think you should as we are traversing through
	// the lists. And these lists change frequently. A lower priority task like
	// the comm_mngr could be parsing the token and adding to the irri irri
	// list. Then the higher priority TD_CHECK task gets a message which causes
	// a re-draw of the this screen. And we attempt to take the irri irri mutex.
	// Well something happens and we get a deadlock. Which also involves the
	// PROGRAM DATA mutex which is taken by THIS TASK when drawing the screen
	// when getting the station description. Well a real DEAD LOCK condition
	// occurs. And the frickin DISPLAY QUEUE becomes full. And everything goes
	// down hill from there.

	UNS_32	lcurrent_line_count;

	// 2011.11.29 rmd : Because we are performing list operations ...
	// particularly scrolling through the list ... take the list mutexes.
	xSemaphoreTakeRecursive( irri_irri_recursive_MUTEX, portMAX_DELAY );

	lcurrent_line_count = nm_ListGetCount( &irri_irri.list_of_irri_all_irrigation );

	// If the number of irrigation details lines have changed, redraw the entire
	// scroll box. Otherwise, just update the contents of what's currently on
	// the screen.
	FDTO_SCROLL_BOX_redraw_retaining_topline( 0, lcurrent_line_count, (lcurrent_line_count != g_IRRI_DETAILS_line_count) );

	xSemaphoreGiveRecursive( irri_irri_recursive_MUTEX );

	// 7/29/2013 ajv : Update the flow rate and current draw, regardless of
	// whether any stations are running or not.
	FDTO_IRRI_DETAILS_update_flow_and_current();

	g_IRRI_DETAILS_line_count = lcurrent_line_count;
}

/* ---------------------------------------------------------- */
/**
 * Draws the Irrigiation Details report.
 * 
 * @executed Executed within the context of the display processing task when the
 *           screen is initially drawn.
 * 
 * @author 10/12/2011 Adrianusv
 *
 * @revisions
 *    10/12/2011 Initial release
 *    2/9/2012   Renamed function. Modified to support unique draw_scroll_line
 *               functions for IRRI and FOAL lists.
 */
extern void FDTO_IRRI_DETAILS_draw_report( const BOOL_32 pcomplete_redraw )
{
	if( pcomplete_redraw )
	{
		GuiVar_IrriDetailsShowFOAL = (false);
	}

	GuiLib_ShowScreen( GuiStruct_rptIrriDetails_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );


	if( !GuiVar_IrriDetailsShowFOAL )
	{
		xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );

		g_IRRI_DETAILS_line_count = nm_ListGetCount( &irri_irri.list_of_irri_all_irrigation );
	}
	else
	{
		xSemaphoreTakeRecursive( irri_irri_recursive_MUTEX, portMAX_DELAY );

		g_IRRI_DETAILS_line_count = nm_ListGetCount( &foal_irri.list_of_foal_all_irrigation );
	}

	FDTO_REPORTS_draw_report( pcomplete_redraw, g_IRRI_DETAILS_line_count, &nm_FDTO_IRRI_DETAILS_load_guivars_for_scroll_line );

	if( !GuiVar_IrriDetailsShowFOAL )
	{
		xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
	}
	else
	{
		xSemaphoreGiveRecursive( irri_irri_recursive_MUTEX );
	}


	// 7/29/2013 ajv : Update the flow rate and current draw, regardless of
	// whether any stations are running or not.
	FDTO_IRRI_DETAILS_update_flow_and_current();
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while on the Irrigation Details report.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 *
 * @param pkey_event The key event to be processed.
 *
 * @author 10/12/2011 Adrianusv
 *
 * @revisions
 *    10/12/2011 Initial release
 *    2/9/2012   Renamed function
 */
extern void IRRI_DETAILS_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		case KEY_HELP:
			good_key_beep();

			GuiVar_IrriDetailsShowFOAL = !GuiVar_IrriDetailsShowFOAL;

			Redraw_Screen( (false) );
			break;

		default:
			REPORTS_process_report( pkey_event, 0, 0 );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

