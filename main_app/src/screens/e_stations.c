/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for abs
#include	<stdlib.h>

// 6/18/2014 ajv : Required for memset
#include	<string.h>

#include	"e_stations.h"

#include	"app_startup.h"

#include	"change.h"

#include	"combobox.h"

#include	"configuration_network.h"

#include	"cursor_utils.h"

#include	"cs3000_comm_server_common.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"e_keyboard.h"

#include	"e_station_groups.h"

#include	"e_programs.h"

#include	"e_stations_in_use.h"

#include	"epson_rx_8025sa.h"

#include	"foal_irri.h"

#include	"irri_comm.h"

#include	"station_groups.h"

#include	"m_main.h"

#include	"r_alerts.h"

#include	"scrollbox.h"

#include	"speaker.h"

#include	"stations.h"

#include	"flowsense.h"

#include	"watersense.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_STATION_DESCRIPTION					(0)
#define CP_STATION_GROUP_ASSIGNMENT				(1)
#define CP_STATION_PERCENT_ET					(2)
#define CP_STATION_TOTAL_MIN					(3)
#define CP_STATION_CYCLE_TIME					(4)
#define CP_STATION_SOAK_IN_TIME					(5)
#define CP_STATION_NO_WATER_DAYS				(6)
#define CP_STATION_EXPECTED_FLOW				(7)
#define CP_STATION_SQUARE_FOOTAGE				(8)
#define CP_STATION_DU							(9)
#define CP_STATION_MOISTURE_BALANCE				(10)
#define CP_STATION_COPY_STATION					(11)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32	g_STATION_top_line;

static UNS_32	g_STATION_previous_cursor_pos;

static BOOL_32	g_STATION_editing_group;

static UNS_32	g_STATION_controllers_to_show_in_menu;

// ----------

typedef struct
{
	void		*station_ptr;

} STATION_MENU_SCROLL_BOX_ITEM;

static STATION_MENU_SCROLL_BOX_ITEM STATION_MENU_items[ (MAX_STATIONS_PER_NETWORK + MAX_CHAIN_LENGTH) ];

// ----------

typedef struct
{
	UNS_32	index;

	UNS_32	box_index_0;

} STATION_MENU_CONTROLLER_ITEM;

static STATION_MENU_CONTROLLER_ITEM STATION_MENU_box_indexes[ MAX_CHAIN_LENGTH ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

UNS_32	g_STATION_group_ID;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32 STATION_get_menu_index_for_displayed_station( void );

static void FDTO_STATION_draw_station( const BOOL_32 pcomplete_redraw );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static UNS_32 STATION_get_inc_by( const UNS_32 prepeats )
{
	UNS_32	rv;

	if( prepeats > 64 )
	{
		rv = 4;
	}
	else if( prepeats > 32 )
	{
		rv = 2;
	}
	else
	{
		rv = 1;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern void STATION_update_cycle_too_short_warning( void )
{
	BOOL_32	lprev_setting;

	lprev_setting = GuiVar_StationInfoCycleTooShort;

	if( GuiVar_StationInfoShowEstMin )
	{
		GuiVar_StationInfoCycleTooShort = ( (GuiVar_StationInfoEstMin < 3.1F) || (GuiVar_StationInfoCycleTime < 3.1F) );
	}
	else
	{
		GuiVar_StationInfoCycleTooShort = (false);
	}

	if( lprev_setting != GuiVar_StationInfoCycleTooShort )
	{
		// 7/8/2015 ajv : Since the CycleTooShort boolean causes a warning to appear or disappear
		// based on it's value, if the value's changed, force a full screen redraw.
		Redraw_Screen( (false) );
	}
}

/* ---------------------------------------------------------- */
static void STATION_update_estimated_minutes( void )
{
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	GuiVar_StationInfoEstMin = nm_STATION_calculate_and_copy_estimated_minutes_into_GuiVar( nm_STATION_get_pointer_to_station( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1) ) );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	STATION_update_cycle_too_short_warning();
}

/* ---------------------------------------------------------- */
static void STATION_update_soak_time( const UNS_32 pprev_du )
{
	STATION_STRUCT			*lstation;

	STATION_GROUP_STRUCT	*lgroup;

	UNS_32	lgroup_ID;

	// 3/16/2015 ajv : To ensur the group isn't deleted between retrieving and copying the
	// precip rate setting, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation = nm_STATION_get_pointer_to_station( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1) );

	if( lstation != NULL )
	{
		lgroup_ID = STATION_get_GID_station_group( lstation );

		lgroup = STATION_GROUP_get_group_with_this_GID( lgroup_ID );

		if( lgroup != NULL )
		{
			if( GuiVar_StationInfoSoakInTime == STATION_GROUP_get_soak_time_for_this_gid( lgroup_ID, pprev_du ) )
			{
				GuiVar_StationInfoSoakInTime = (UNS_32)WATERSENSE_get_soak_time__cycle_end_to_cycle_start( STATION_GROUP_get_allowable_surface_accumulation( lgroup ),
																				 STATION_GROUP_get_soil_intake_rate( lgroup ),
																				 STATION_GROUP_get_precip_rate_in_per_hr( lgroup ),
																				 GuiVar_StationInfoDU );
			}
		}
		else
		{
			GuiVar_StationInfoSoakInTime = 0;
		}
	}
	else
	{
		GuiVar_StationInfoSoakInTime = 0;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Processes the Plus and Minus keys to change the station's total minutes
 * value.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @author 7/28/2011 Adrianusv
 *
 * @revisions
 *    7/28/2011 Initial release
 */
static void STATION_process_total_min_10u( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	UNS_32 ltotal_run_minutes;

	ltotal_run_minutes = (UNS_32)(GuiVar_StationInfoTotalMinutes * 10);

	process_uns32( pkey_event.keycode, &ltotal_run_minutes, STATION_TOTAL_RUN_MINUTES_MIN_10u, STATION_TOTAL_RUN_MINUTES_MAX_10u, STATION_get_inc_by( pkey_event.repeats ), (false) );

	GuiVar_StationInfoTotalMinutes = ((float)ltotal_run_minutes / 10.0F);

	// Recalculate the estimated minutes in case we're using Percent Adjust.
	GuiVar_StationInfoEstMin = nm_STATION_calculate_and_copy_estimated_minutes_into_GuiVar( nm_STATION_get_pointer_to_station( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1) ) );

	Refresh_Screen();
}

/* ---------------------------------------------------------- */
/**
 * Processes the Plus and Minus keys to change the station's cycle minutes
 * value.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @author 7/28/2011 Adrianusv
 *
 * @revisions
 *    7/28/2011 Initial release
 */
static void STATION_process_cycle_min_10u( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	STATION_STRUCT	*lstation;

	UNS_32	lcycle_minutes_10u;

	UNS_32	lmin_cycle_time_10u, lmax_cycle_time_10u;

	// ----------

	// 6/25/2015 ajv : If we're in ET, the maximum cycle time is determined by the WaterSense
	// calculation, so set it accordingly. Otherwise, use our traditional default.
	if( GuiVar_StationInfoShowPercentOfET )
	{
		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		lstation = nm_STATION_get_pointer_to_station( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1) );

		lmax_cycle_time_10u = nm_STATION_get_watersense_cycle_max_10u( lstation );

		lmin_cycle_time_10u = STATION_CYCLE_MINUTES_MIN_10u;

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}
	else
	{
		lmax_cycle_time_10u = STATION_CYCLE_MINUTES_MAX_10u;

		lmin_cycle_time_10u = STATION_CYCLE_MINUTES_MIN_10u;
	}

	// ----------

	lcycle_minutes_10u = (UNS_32)(GuiVar_StationInfoCycleTime * 10);

	process_uns32( pkey_event.keycode, &lcycle_minutes_10u, lmin_cycle_time_10u, lmax_cycle_time_10u, STATION_get_inc_by( pkey_event.repeats ), (false) );

	GuiVar_StationInfoCycleTime = ((float)lcycle_minutes_10u / 10.0F);

	// ----------

	STATION_update_cycle_too_short_warning();
}

/* ---------------------------------------------------------- */
static void STATION_process_soak_min( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	STATION_STRUCT	*lstation;

	UNS_32	lmin_soak_time;

	// ----------

	// 6/25/2015 ajv : If we're in ET, the minimum soak time is determined by the WaterSense
	// calculation, so set it accordingly. Otherwise, use our traditional default.
	if( GuiVar_StationInfoShowPercentOfET )
	{
		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		lstation = nm_STATION_get_pointer_to_station( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1) );

		lmin_soak_time = nm_STATION_get_watersense_soak_min( lstation );

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}
	else
	{
		lmin_soak_time = STATION_SOAK_MINUTES_MIN;
	}

	// ----------

	process_uns32( pkey_event.keycode, &GuiVar_StationInfoSoakInTime, lmin_soak_time, STATION_SOAK_MINUTES_MAX, STATION_get_inc_by( pkey_event.repeats ), (false) );
}

/* ---------------------------------------------------------- */
/**
 * Processes the Plus and Minus keys to change the station's ET factor value.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @author 7/28/2011 Adrianusv
 *
 * @revisions
 *    7/28/2011 Initial release
 */
static void STATION_process_ET_factor( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	process_int32( pkey_event.keycode, &GuiVar_StationInfoETFactor, (STATION_ET_FACTOR_MIN - 100), (STATION_ET_FACTOR_MAX - 100), STATION_get_inc_by( pkey_event.repeats ), (false) );

	// 3/16/2015 ajv : Since we adjusted a factor that's used to determine stations run time,
	// update the run time now.
	STATION_update_estimated_minutes();
}

/* ---------------------------------------------------------- */
static void STATION_update_station_group( const UNS_32 pindex_0 )
{
	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup = STATION_GROUP_get_group_at_this_index( pindex_0 );

	if( lgroup == NULL )
	{
		Alert_group_not_found();

		lgroup = STATION_GROUP_get_group_at_this_index( 0 );
	}

	strlcpy( GuiVar_StationInfoStationGroup, nm_GROUP_get_name( lgroup ), sizeof(GuiVar_StationInfoStationGroup) );

	// ----------

	// 6/25/2015 ajv : Save the change immediately to the file so we can request a screen redraw
	// an update the various values on the screen.

	g_STATION_group_ID = nm_GROUP_get_group_ID( lgroup );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// 6/25/2015 ajv : Save all of the changes, including the new group setting. Although
	// somewhat drastic, changing the group real-time requires a variety of settings to be
	// updated, which is extremely complex due to the fact that we don't pass the group or group
	// ID into most functions, relying instead of determining what to use based on what's saved
	// for the station.
	STATION_extract_and_store_changes_from_GuiVars();

	// 6/25/2015 ajv : Force all of the parameters for the station to be redrawn.
	FDTO_STATION_draw_station( (true) );
}

/* ---------------------------------------------------------- */
/**
 * Processes the PLUS and MINUS keys to change the station group (program)
 * assignment.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @author 10/30/2012 Adrianusv
 *
 * @revisions (none)
 */
static void STATION_process_station_group( const UNS_32 pkeycode )
{
	UNS_32	lindex;

	UNS_32	llist_count;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	llist_count = (STATION_GROUP_get_num_groups_in_use() - 1);

	if( g_STATION_group_ID == 0 )
	{
		good_key_beep();

		if( pkeycode == KEY_PLUS )
		{
			lindex = 0;
		}
		else
		{
			lindex = llist_count;
		}

		STATION_update_station_group( lindex );
	}
	else
	{
		lindex = STATION_GROUP_get_index_for_group_with_this_GID( g_STATION_group_ID );

		if( ((pkeycode == KEY_PLUS) && (lindex == llist_count)) || ((pkeycode == KEY_MINUS) && (lindex == 0)) )
		{
			good_key_beep();

			g_STATION_group_ID = 0;

			strlcpy( GuiVar_StationInfoStationGroup, GuiLib_GetTextPtr( GuiStruct_txtNotChosen_0, 0 ), sizeof(GuiVar_StationInfoStationGroup) );

			// 6/29/2015 ajv : Don't show any station details if not assigned to a group
			GuiVar_StationInfoGroupHasStartTime = (false);
		}
		else
		{
			process_uns32( pkeycode, &lindex, 0, llist_count, 1, (true) );

			STATION_update_station_group( lindex );
		}
	}


	// 11/5/2013 ajv : If the group ID becomes 0 ("not chosen"), we need to hide
	// the Station Copy feature.
	GuiVar_StationInfoShowCopyStation = !(WEATHER_get_station_uses_daily_et( nm_STATION_get_pointer_to_station(GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1)) ));

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
static void FDTO_STATIONS_show_station_group_dropdown( void )
{
	UNS_32	lgroup_index_0;

	lgroup_index_0 = STATION_GROUP_get_index_for_group_with_this_GID( g_STATION_group_ID );

	FDTO_COMBOBOX_show( GuiStruct_cbxStationGroup_0, &nm_STATION_GROUP_load_group_name_into_guivar, STATION_GROUP_get_num_groups_in_use(), lgroup_index_0 );
}

/* ---------------------------------------------------------- */
static void FDTO_STATIONS_close_station_group_dropdown( void )
{
	STATION_update_station_group( (UNS_32)GuiLib_ScrollBox_GetActiveLine(0, 0) );

	FDTO_COMBOBOX_hide();
}

/* ---------------------------------------------------------- */
static void FDTO_STATION_draw_station( const BOOL_32 pcomplete_redraw )
{
	STATION_STRUCT *lstation;

	BOOL_32 lprevious_station_found;

	BOOL_32 lno_stations_in_use;


	if( nm_ListGetCount( &station_info_list_hdr ) > 0 )
	{
		// Take the station_preserves mutex to ensure the station we're pulling the
		// current information from isn't removed while we're accessing it.
		// Unfortunately, we need to take this before the program_data mutex instead
		// of only within the copy_station_into_guivars function because this is the
		// order it's taken everywhere else and we must preserve this to prevent a
		// deadlock.
		xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		// Try to get the station we last viewed. This may result in NULL if the
		// GuiVars are uninitialized - such as if we haven't viewed the Stations
		// screen since booting up.
		lstation = nm_STATION_get_pointer_to_station( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1) );

		// If we didn't get a valid station - say if this is the first time we've
		// started and the GuiVars are uninitialized, or if we're viewing the screen
		// when the station we're viewing is removed - grab the first available
		// station.
		if( (lstation == NULL) || (STATION_station_is_available_for_use( lstation ) == (false)) )
		{
			lprevious_station_found = (false);

			lstation = STATION_get_first_available_station();

			lno_stations_in_use = !(STATION_station_is_available_for_use( lstation ));
		}
		else
		{
			lprevious_station_found = (true);

			lno_stations_in_use = (false);
		}

		if( (pcomplete_redraw == (true)) || (lprevious_station_found == (false)) )
		{
			nm_STATION_copy_station_into_guivars( lstation );
		}
		else
		{
			// If we're simply refreshing the screen we're on, display the same
			// active cursor; otherwise, select the first cursor.
			/*
			if( (UNS_32)GuiLib_CurStructureNdx == pstructure_to_draw )
			{
				GuiLib_ShowScreen( (UNS_16)pstructure_to_draw, GuiLib_ActiveCursorFieldNo, GuiLib_RESET_AUTO_REDRAW );
			}
			else
			{
				GuiLib_ShowScreen( (UNS_16)pstructure_to_draw, 1, GuiLib_RESET_AUTO_REDRAW );
			}
			*/
		}

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

		xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
	}
}

/* ---------------------------------------------------------- */
/**
 * Updates and redraws the current (i) and flow status values on the Station
 * Information screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * every second when the Station Information screen is visible.
 *
 * @author 8/20/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void FDTO_STATION_update_info_i_and_flow_status( void )
{
	UNS_32	lstation_preserves_index;

	STATION_PRESERVES_get_index_using_box_index_and_station_number( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1), &lstation_preserves_index );

	GuiVar_StationInfoIStatus = station_preserves.sps[ lstation_preserves_index ].spbf.i_status;

	GuiVar_StationInfoFlowStatus = station_preserves.sps[ lstation_preserves_index ].spbf.flow_status;

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Processes the NEXT and PREVIOUS keys when the user is navigating from one
 * station to another while editing stations. It moves the scroll line indicator
 * increments or decrements the station, and redraws the screen to show the new
 * station's programming information.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user presses NEXT or PREV while editing a station.
 * 
 * @param pkeycode The key code to process.
 *
 * @author 10/24/2013 AdrianusV
 *
 * @revisions (none)
 */
static void STATION_process_NEXT_and_PREV( const UNS_32 pkeycode )
{
	UNS_32	lmenu_index_for_displayed_station;

	lmenu_index_for_displayed_station = STATION_get_menu_index_for_displayed_station();

	if( (pkeycode == KEY_NEXT) || (pkeycode == KEY_PLUS) )
	{
		// 10/24/2013 ajv : Turn off the automatic redraw functions within the SCROLL_BOX_to_line
		// and SCROLL_BOX_up_or_down routines to ensure the scroll bar does not appear.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_DISABLED );

		SCROLL_BOX_to_line( 0, (INT_16)lmenu_index_for_displayed_station );

		// 7/14/2015 ajv : Protect against overrunning the array
		if( lmenu_index_for_displayed_station < (MAX_STATIONS_PER_NETWORK + MAX_CHAIN_LENGTH) )
		{
			++lmenu_index_for_displayed_station;

			// 7/14/2015 ajv : First try to move to the next station. If the next position is NULL, it
			// means we've either hit the end of the list OR we've reached the last station on the
			// current controller. If the latter, check whether there is a station on the next
			// controller (two locations down) and jump to that station if there is.
			if( STATION_MENU_items[ lmenu_index_for_displayed_station ].station_ptr != NULL )
			{
				SCROLL_BOX_up_or_down( 0, KEY_C_DOWN );
			}
			else if( STATION_MENU_items[ (lmenu_index_for_displayed_station + 1) ].station_ptr != NULL )
			{
				++lmenu_index_for_displayed_station;

				good_key_beep();

				SCROLL_BOX_to_line( 0, (INT_16)lmenu_index_for_displayed_station );
			}
		}
		else
		{
			bad_key_beep();
		}

		// 10/24/2013 ajv : Now that we've moved the scroll bar without displaying it, reenable the
		// automatic redraw functions to ensure other calls are not affected.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_ENABLED );
	}
	else
	{
		// 10/24/2013 ajv : Turn off the automatic redraw functions within the SCROLL_BOX_to_line
		// and SCROLL_BOX_up_or_down routines to ensure the scroll bar does not appear.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_DISABLED );

		SCROLL_BOX_to_line( 0, (INT_16)lmenu_index_for_displayed_station );

		// 7/14/2015 ajv : Protect against underrunning the array
		if( lmenu_index_for_displayed_station > 1 )
		{
			--lmenu_index_for_displayed_station;

			// 7/14/2015 ajv : First try to move to the previous station. If the previous position is
			// NULL, it means we've either hit the beginning of the list OR we've reached the first
			// station on the current controller. If the latter, check whether there is a station on a
			// previous controller (two locations up) and jump to that station if there is.
			if( STATION_MENU_items[ lmenu_index_for_displayed_station ].station_ptr != NULL )
			{
				SCROLL_BOX_up_or_down( 0, KEY_C_UP );
			}
			else if( STATION_MENU_items[ (lmenu_index_for_displayed_station - 1) ].station_ptr != NULL )
			{
				--lmenu_index_for_displayed_station;

				good_key_beep();

				SCROLL_BOX_to_line( 0, (INT_16)lmenu_index_for_displayed_station );
			}
		}
		else
		{
			bad_key_beep();
		}

		// 10/24/2013 ajv : Now that we've moved the scroll bar without displaying it, reenable the
		// automatic redraw functions to ensure other calls are not affected.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_ENABLED );
	}

	// 10/24/2013 ajv : If we've actually moved to another station (and not a controller
	// heading), save the current station's information and repopulate the GuiVars with the next
	// station's.
	if( STATION_MENU_items[ lmenu_index_for_displayed_station ].station_ptr != NULL )
	{
		STATION_extract_and_store_changes_from_GuiVars();

		g_GROUP_list_item_index = STATION_get_index_using_ptr_to_station_struct( STATION_MENU_items[ lmenu_index_for_displayed_station ].station_ptr );

		// Take the station_preserves mutex to ensure the station we're pulling the current
		// information from isn't removed while we're accessing it. Unfortunately, we need to take
		// this before the program_data mutex instead of only within the copy_station_into_guivars
		// function because this is the order it's taken everywhere else and we must preserve this
		// to prevent a deadlock.
		xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		nm_STATION_copy_station_into_guivars( STATION_MENU_items[ lmenu_index_for_displayed_station ].station_ptr );

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

		xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
	}
	else
	{
		bad_key_beep();
	}

	// 10/24/2013 ajv : Although nothing may have changed if we encountered the top or bottom of
	// the list, we need to force a redraw to ensure the scroll line of the currently selected
	// station is suppressed. Without this call, the SCROLL_BOX_to_line call drew the scroll
	// line and as soon as the next call to GuiLib_Refresh happens (such as once per second when
	// the date is updated), the scroll line will appear.
	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
/**
 * Processes the key event from the station information screen for a particular
 * station.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 7/27/2011 Adrianusv
 *
 * @revisions
 *    7/27/2011 Initial release
 */
extern void STATION_process_station( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	STATION_STRUCT		*lstation;

	CHANGE_BITS_32_BIT	*lbitfield_of_changes;

	// 6/29/2015 ajv : Track the current distribution uniformity when it changes so we can
	// compare the soak-time calculated using the old DU to see whether the soak-time needs to
	// be recalculated.
	UNS_32	lprevious_du;

	UNS_32	lbox_index_0;



	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_dlgKeyboard_0:
		
			// 10/29/2018 Ryan : There was a check here that used to set the cursor position back to
			// where it was before the keyboard came up once the ok button was pressed or the back key
			// was pressed, but it was unessisary, as the cursor returns to the right position anyway
			// and as it was, it was causing an error where the KEYBOARD_process_key function didn't
			// have the correct cursor position information to close correctly. 

			KEYBOARD_process_key( pkey_event, &FDTO_STATION_draw_menu );
			break;

		case GuiStruct_cbxStationGroup_0:
			if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
			{
				good_key_beep();

				lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
				lde._04_func_ptr = FDTO_STATIONS_close_station_group_dropdown;
				Display_Post_Command( &lde );
			}
			else
			{
				COMBO_BOX_key_press( pkey_event.keycode, NULL );
			}
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_NEXT:
				case KEY_PREV:
					STATION_process_NEXT_and_PREV( pkey_event.keycode );
					break;

				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_STATION_DESCRIPTION:
							good_key_beep();

							KEYBOARD_draw_keyboard( 161, 27, NUMBER_OF_CHARS_IN_A_NAME, KEYBOARD_TYPE_QWERTY );
							break;

						case CP_STATION_GROUP_ASSIGNMENT:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_STATIONS_show_station_group_dropdown;
							Display_Post_Command( &lde );
							break;

						#if 1
						//USED FOR DEBUGGING WATERSENSE
						case CP_STATION_DU:
							good_key_beep();

							GuiVar_StationInfoMoistureBalancePercent = (STATION_MOISTURE_BALANCE_DEFAULT * 100.0F);

							for( UNS_32 i = 0; i < STATION_PRESERVE_ARRAY_SIZE; ++i )
							{
								station_preserves.sps[ i ].left_over_irrigation_seconds = 0;
							}

							Refresh_Screen();

							break;
						#endif

						case CP_STATION_COPY_STATION:
							good_key_beep();

							if( GuiVar_StationInfoShowPercentOfET == (false) )
							{
								// 8/28/2014 ajv : Generate a single alert for each copy activity rather than one per
								// station.
								Alert_station_copied_idx( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1), GuiVar_StationInfoStationGroup, CHANGE_REASON_STATION_COPY );

								lbox_index_0 = FLOWSENSE_get_controller_index();

								for( lstation = nm_ListGetFirst( &station_info_list_hdr ); lstation != NULL; lstation = nm_ListGetNext( &station_info_list_hdr, lstation ) )
								{
									if( nm_STATION_get_station_number_0(lstation) != (GuiVar_StationInfoNumber - 1) )
									{
										if( STATION_get_GID_station_group(lstation) == g_STATION_group_ID )
										{
											lbitfield_of_changes = STATION_get_change_bits_ptr( lstation, CHANGE_REASON_KEYPAD );

											nm_STATION_set_total_run_minutes( lstation, (UNS_32)(GuiVar_StationInfoTotalMinutes * 10), CHANGE_do_not_generate_change_line, CHANGE_REASON_STATION_COPY, lbox_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
											nm_STATION_set_cycle_minutes( lstation, (UNS_32)(GuiVar_StationInfoCycleTime * 10), CHANGE_do_not_generate_change_line, CHANGE_REASON_STATION_COPY, lbox_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
											nm_STATION_set_soak_minutes( lstation, GuiVar_StationInfoSoakInTime, CHANGE_do_not_generate_change_line, CHANGE_REASON_STATION_COPY, lbox_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
										}
									}
								}
								DIALOG_draw_ok_dialog( GuiStruct_dlgStationCopied_0 );
							}
							else
							{
								bad_key_beep();

								DIALOG_draw_ok_dialog( GuiStruct_dlgStationCopied_0 );
							}
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_PLUS:
				case KEY_MINUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_STATION_DESCRIPTION:
							STATION_process_NEXT_and_PREV( pkey_event.keycode );
							break;

						case CP_STATION_PERCENT_ET:
							STATION_process_ET_factor( pkey_event );
							break;

						case CP_STATION_TOTAL_MIN:
							STATION_process_total_min_10u( pkey_event );
							break;

						case CP_STATION_CYCLE_TIME:
							STATION_process_cycle_min_10u( pkey_event );
							break;

						case CP_STATION_SOAK_IN_TIME:
							STATION_process_soak_min( pkey_event );
							break;

						case CP_STATION_EXPECTED_FLOW:
							process_uns32( pkey_event.keycode, &GuiVar_StationInfoExpectedFlow, STATION_EXPECTED_FLOW_RATE_MIN, STATION_EXPECTED_FLOW_RATE_MAX, STATION_get_inc_by( pkey_event.repeats ), (false) );
							break;

						case CP_STATION_DU:
							lprevious_du = GuiVar_StationInfoDU;

							process_uns32( pkey_event.keycode, &GuiVar_StationInfoDU, STATION_DU_MIN, STATION_DU_MAX, STATION_get_inc_by( pkey_event.repeats ), (false) );

							// 3/16/2015 ajv : Since we adjusted a factor that's used to determine stations run time,
							// update the run time now.
							STATION_update_estimated_minutes();
							
							// 5/4/2015 ajv : Additionally, the DU affects the soak-in time, so update that as well
							// based on the new DU GuiVar value.
							STATION_update_soak_time( lprevious_du );
							break;

						case CP_STATION_SQUARE_FOOTAGE:
							process_uns32( pkey_event.keycode, &GuiVar_StationInfoSquareFootage, STATION_SQUARE_FOOTAGE_MIN, STATION_SQUARE_FOOTAGE_MAX, STATION_get_inc_by( pkey_event.repeats ), (false) );
							break;

						case CP_STATION_NO_WATER_DAYS:
							process_uns32( pkey_event.keycode, &GuiVar_StationInfoNoWaterDays, STATION_NO_WATER_DAYS_MIN, STATION_NO_WATER_DAYS_MAX, 1, (false) );
							break;

						case CP_STATION_GROUP_ASSIGNMENT:
							if( (STATION_GROUP_get_num_groups_in_use() > 1) || (g_STATION_group_ID == 0) )
							{
								STATION_process_station_group( pkey_event.keycode );
							}
							else
							{
								bad_key_beep();
							}
							break;

						default:
							bad_key_beep();
					}
					Refresh_Screen();
					break;

				case KEY_C_UP:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_STATION_PERCENT_ET:
						case CP_STATION_TOTAL_MIN:
						case CP_STATION_NO_WATER_DAYS:
							// Store the field we came from so when the user presses
							// DOWN, they're returned to the same field.
							g_STATION_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

							CURSOR_Select( CP_STATION_GROUP_ASSIGNMENT, (true) );
							break;

						case CP_STATION_EXPECTED_FLOW:
							CURSOR_Select( CP_STATION_SOAK_IN_TIME, (true) );
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_DOWN:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_STATION_GROUP_ASSIGNMENT:
							if( g_STATION_previous_cursor_pos == CP_STATION_NO_WATER_DAYS )
							{
								CURSOR_Select( g_STATION_previous_cursor_pos, (true) );
							}
							else
							{
								CURSOR_Down( (true) );
							}
							break;

						case CP_STATION_PERCENT_ET:
							CURSOR_Select( CP_STATION_CYCLE_TIME, (true) );
							break;

						case CP_STATION_SOAK_IN_TIME:
							CURSOR_Select( CP_STATION_EXPECTED_FLOW, (true) );
							break;

						case CP_STATION_NO_WATER_DAYS:
							bad_key_beep();
							break;

						default:
							CURSOR_Down( (true) );
					}
					break;

				case KEY_C_LEFT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_STATION_DESCRIPTION:
							KEY_process_BACK_from_editing_screen( (void*)&FDTO_STATION_return_to_menu );
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_BACK:
					KEY_process_BACK_from_editing_screen( (void*)&FDTO_STATION_return_to_menu );
					break;

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static UNS_32 nm_STATION_populate_pointers_for_menu( void )
{
	STATION_STRUCT *lstation;

	UNS_32	llast_controller;

	UNS_32	ladded_controllers;

	UNS_32	ladded_stations;

	UNS_32	i;

	llast_controller = 0;

	ladded_controllers = 0;

	ladded_stations = 0;

	memset( STATION_MENU_items, 0x00, sizeof(STATION_MENU_items) );
	memset( STATION_MENU_box_indexes, 0x00, sizeof(STATION_MENU_box_indexes) );

	for( i = 0 ; i < (nm_ListGetCount(&station_info_list_hdr) + g_STATION_controllers_to_show_in_menu); i++ )
	{
		lstation = nm_GROUP_get_ptr_to_group_at_this_location_in_list( &station_info_list_hdr, ((UNS_32)i - ladded_controllers) );

		if( (lstation != NULL) && (STATION_station_is_available_for_use(lstation) == (true)) )
		{
			if( (ladded_controllers == 0) || (llast_controller != nm_STATION_get_box_index_0(lstation)) )
			{
				STATION_MENU_items[ (ladded_stations + ladded_controllers) ].station_ptr = NULL;

				STATION_MENU_box_indexes[ ladded_controllers ].box_index_0 = nm_STATION_get_box_index_0( lstation );

				STATION_MENU_box_indexes[ ladded_controllers ].index = (ladded_stations + ladded_controllers);

				llast_controller = STATION_MENU_box_indexes[ ladded_controllers ].box_index_0;

				++ladded_controllers;
			}
			else
			{
				STATION_MENU_items[ (ladded_stations + ladded_controllers) ].station_ptr = lstation;

				ladded_stations++;
			}
		}
	}
	return ( ladded_stations );
}
 
/* ---------------------------------------------------------- */
extern void nm_STATION_load_station_number_into_guivar( const INT_16 pindex_0_i16 )
{
	UNS_32	lstation_number_0;

	UNS_32	lbox_index_0;

	UNS_32	i;

	if( (UNS_32)pindex_0_i16 < (nm_ListGetCount(&station_info_list_hdr) + g_STATION_controllers_to_show_in_menu) )
	{
		// If the station pointer is NULL, we're going to populate the scroll
		// line with the controller name or serial number.
		if( STATION_MENU_items[ pindex_0_i16 ].station_ptr == NULL )
		{
			GuiVar_itmSubNode = 0;

			for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
			{
				if( STATION_MENU_box_indexes[ i ].index == (UNS_32)pindex_0_i16 )
				{
					break;
				}
			}

			NETWORK_CONFIG_get_controller_name_str_for_ui( STATION_MENU_box_indexes[ i ].box_index_0, (char *)&GuiVar_itmStationName );

			strlcpy( GuiVar_itmStationNumber, "", sizeof(GuiVar_itmStationNumber) );
		}
		else
		{
			GuiVar_itmSubNode = 10;

			lstation_number_0 = nm_STATION_get_station_number_0( STATION_MENU_items[ pindex_0_i16 ].station_ptr );

			lbox_index_0 = nm_STATION_get_box_index_0( STATION_MENU_items[ pindex_0_i16 ].station_ptr );

			strlcpy( GuiVar_itmStationName, GuiLib_GetTextPtr( GuiStruct_txtStation_0, 0 ), NUMBER_OF_CHARS_IN_A_NAME );

			STATION_get_station_number_string( lbox_index_0, lstation_number_0, (char*)&GuiVar_itmStationNumber, sizeof(GuiVar_itmStationNumber) );
		}
	}
}

/* ---------------------------------------------------------- */
static void FDTO_STATION_draw_select_a_station_window( void )
{
	GuiLib_ShowScreen( GuiStruct_pnlSelectAStation_0, GuiLib_ActiveCursorFieldNo, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
static UNS_32 STATION_get_menu_index_for_displayed_station( void )
{
	STATION_STRUCT	*lstation;

	UNS_32	rv;

	UNS_32	i;

	rv = 0;

	if( (GuiVar_StationInfoBoxIndex == 0) && (GuiVar_StationInfoNumber == 0) )
	{
		rv = 1;
	}
	else
	{
		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		lstation = nm_STATION_get_pointer_to_station( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1) );

		for( i = 0; i < (nm_ListGetCount( &station_info_list_hdr ) + g_STATION_controllers_to_show_in_menu); ++i )
		{
			if( STATION_MENU_items[ i ].station_ptr == lstation )
			{
				rv = i;

				break;
			}
		}

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern void FDTO_STATION_return_to_menu( void )
{
	STATION_extract_and_store_changes_from_GuiVars();

	g_STATION_editing_group = (false);

	GuiLib_ScrollBox_To_Line( 0, (UNS_16)(STATION_get_menu_index_for_displayed_station()) );

	GuiLib_ActiveCursorFieldNo = GuiLib_NO_CURSOR;

	FDTO_Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
extern void FDTO_STATION_draw_menu( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lstations_in_use;

	UNS_32	lactive_line;

	INT_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		lcursor_to_select = GuiLib_NO_CURSOR;

		g_STATION_top_line = 0;

		g_STATION_editing_group = (false);

		// Initialize the previous cursor position
		g_STATION_previous_cursor_pos = CP_STATION_DESCRIPTION;

		FDTO_STATION_draw_station( pcomplete_redraw );
	}
	else
	{
		lcursor_to_select = (INT_32)GuiLib_ActiveCursorFieldNo;

		g_STATION_top_line = (UNS_32)GuiLib_ScrollBox_GetTopLine( 0 );
	}

	GuiLib_ShowScreen( GuiStruct_scrStations_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );

	// 10/24/2013 ajv : Close the scroll box to ensure the ScrollBox_Init
	// function handles the redraw properly.
	GuiLib_ScrollBox_Close( 0 );
	
	g_STATION_controllers_to_show_in_menu = STATION_get_unique_box_index_count();

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstations_in_use = nm_STATION_populate_pointers_for_menu();

	lactive_line = STATION_get_menu_index_for_displayed_station();

	if( g_STATION_editing_group == (true) )
	{
		// 10/24/2013 ajv : Draw the scroll box without highlighting the active
		// line and draw the indicator to show which station the user is looking
		// at.
		GuiLib_ScrollBox_Init_Custom_SetTopLine( 0, &nm_STATION_load_station_number_into_guivar, (INT_16)(lstations_in_use + g_STATION_controllers_to_show_in_menu), GuiLib_NO_CURSOR, (INT_16)g_STATION_top_line );
		GuiLib_ScrollBox_SetIndicator( 0, lactive_line );
		GuiLib_ScrollBox_Redraw( 0 );
	}
	else
	{
		// If the last viewed station is beyond the initial 12 that's visible on
		// the screen, we need to move the top line down. For now, the selected
		// station will be at the very bottom of the list.
		if( (pcomplete_redraw == (true)) && (lactive_line > 12) )
		{
			g_STATION_top_line = (lactive_line - 11);
		}

		// 10/24/2013 ajv : Draw the scroll box and highlight the active line.
		// Since we're redrawing the entire scroll box, there's no reason to
		// call SetIndicator with a -1 to hide it because the indicator simply
		// won't be drawn.
		GuiLib_ScrollBox_Init_Custom_SetTopLine( 0, &nm_STATION_load_station_number_into_guivar, (INT_16)(lstations_in_use + g_STATION_controllers_to_show_in_menu), (INT_16)lactive_line, (INT_16)g_STATION_top_line );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void STATION_process_menu( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	if( g_STATION_editing_group == (true) )
	{
		STATION_process_station( pkey_event );
	}
	else
	{
		switch( pkey_event.keycode )
		{
			case KEY_C_RIGHT:
			case KEY_SELECT:
				if( STATION_MENU_items[ GuiLib_ScrollBox_GetActiveLine( 0, 0 ) ].station_ptr != NULL )
				{
					good_key_beep();

					g_GROUP_list_item_index = STATION_get_index_using_ptr_to_station_struct( STATION_MENU_items[ GuiLib_ScrollBox_GetActiveLine( 0, 0 ) ].station_ptr );

					g_STATION_editing_group = (true);
					GuiLib_ActiveCursorFieldNo = CP_STATION_DESCRIPTION;

					Redraw_Screen( (false) );
				}
				else
				{
					bad_key_beep();
				}
				break;

			case KEY_NEXT:
			case KEY_PREV:
				// Change the key to either UP or DOWN so it can be processed
				// using the KEY_C_UP and KEY_C_DOWN case statement.
				if( pkey_event.keycode == KEY_NEXT )
				{
					pkey_event.keycode = KEY_C_DOWN;
				}
				else
				{
					pkey_event.keycode = KEY_C_UP;
				}
				// Don't break here. This will allow the routine to fall into
				// the next case statement which will actually move the cursor
				// up or down appropriately.

			case KEY_C_DOWN:
			case KEY_C_UP:
				SCROLL_BOX_up_or_down( 0, pkey_event.keycode );

				if( STATION_MENU_items[ GuiLib_ScrollBox_GetActiveLine( 0, 0 ) ].station_ptr == NULL )
				{
					lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
					lde._04_func_ptr = (void*)&FDTO_STATION_draw_select_a_station_window;
					Display_Post_Command( &lde );

					// 3/17/2015 ajv : Since we're not performing a full redraw of the screen, we need to
					// manually call the scroll box redraw function to ensure the controller name is
					// highlighted.
					SCROLL_BOX_redraw( 0 );
				}
				else
				{
					g_GROUP_list_item_index = STATION_get_index_using_ptr_to_station_struct( STATION_MENU_items[ GuiLib_ScrollBox_GetActiveLine( 0, 0 ) ].station_ptr );

					// Take the station_preserves mutex to ensure the station we're pulling the
					// current information from isn't removed while we're accessing it.
					// Unfortunately, we need to take this before the program_data mutex instead
					// of only within the copy_station_into_guivars function because this is the
					// order it's taken everywhere else and we must preserve this to prevent a
					// deadlock.
					xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

					xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

					nm_STATION_copy_station_into_guivars( STATION_MENU_items[ GuiLib_ScrollBox_GetActiveLine( 0, 0 ) ].station_ptr );

					xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

					xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );

					Redraw_Screen( (false) );
				}
				break;

			default:
				if( pkey_event.keycode == KEY_BACK )
				{
					GuiVar_MenuScreenToShow = CP_MAIN_MENU_SCHEDULED_IRRIGATION;
				}
				KEY_process_global_keys( pkey_event );
		}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

