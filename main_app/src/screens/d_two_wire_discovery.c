/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"d_two_wire_discovery.h"

#include	"d_process.h"

#include	"d_two_wire_assignment.h"

#include	"dialog.h"

#include	"two_wire_utils.h"

#include	"tpmicro_data.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/24/2013 ajv : With a MAX of 70 decoders, we've limited the size of the
// progress bar on the screen to double that. Therefore, each time a decoder is
// found, we need to draw two pixel columns.
#define TWO_WIRE_PROGRESS_BAR_PIXEL_COLS_PER_DECODER	(2)

// ---------------------

#define TWO_WIRE_PROGRESS_BAR_MIN	(0)
#define TWO_WIRE_PROGRESS_BAR_MAX	(MAX_DECODERS_IN_A_BOX * 4 / TWO_WIRE_PROGRESS_BAR_PIXEL_COLS_PER_DECODER)	// roughly 4 sec to find each decoder

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void TWO_WIRE_init_discovery_dialog( const BOOL_32 pshow_progress_dialog )
{
	GuiVar_TwoWireNumDiscoveredStaDecoders = 0;
	GuiVar_TwoWireNumDiscoveredPOCDecoders = 0;
	GuiVar_TwoWireNumDiscoveredMoisDecoders = 0;

	// 10/17/2013 ajv : Notify the UI that we're performing a discovery as
	// well.
	GuiVar_TwoWireDiscoveryState = TWO_WIRE_DISCOVERY_DIALOG_STATE_DISCOVERING;

	// 10/24/2013 ajv : Initialize the progress bar to start at position 0.
	// This is automatically incremented as decoders are found.
	GuiVar_TwoWireDiscoveryProgressBar = TWO_WIRE_PROGRESS_BAR_MIN;

	if( pshow_progress_dialog == (true) )
	{
		DIALOG_draw_ok_dialog( GuiStruct_dlgDiscoveringDecoders_0 );
	}
}

/* ---------------------------------------------------------- */
extern void FDTO_TWO_WIRE_update_discovery_dialog( const BOOL_32 pprocess_complete )
{
	UNS_32	i;

	// ----------

	if( pprocess_complete == (false) )
	{
		GuiVar_SpinnerPos = ((GuiVar_SpinnerPos + 1) % 4);

		if( GuiVar_TwoWireDiscoveryProgressBar < TWO_WIRE_PROGRESS_BAR_MAX )
		{
			// 10/24/2013 ajv : Update the progress bar every other second,
			// regardless of whether a decoder was found or not.
			if( (GuiVar_SpinnerPos % 2) == 1 )
			{
				GuiVar_TwoWireDiscoveryProgressBar++;
			}

			// 10/24/2013 ajv : However, if we've left the dialog and gone back,
			// we'd like to start the progress indicator where we were, so let's
			// compare our current progress bar (time-based) to a count-based size.
			if( (GuiVar_TwoWireNumDiscoveredStaDecoders * TWO_WIRE_PROGRESS_BAR_PIXEL_COLS_PER_DECODER) > GuiVar_TwoWireDiscoveryProgressBar )
			{
				GuiVar_TwoWireDiscoveryProgressBar = (GuiVar_TwoWireNumDiscoveredStaDecoders * TWO_WIRE_PROGRESS_BAR_PIXEL_COLS_PER_DECODER); 
			}
		}
		else
		{
			GuiVar_TwoWireDiscoveryProgressBar = TWO_WIRE_PROGRESS_BAR_MAX;
		}
	}
	else
	{
		// 10/24/2013 ajv : Set the progress bar to 100% if it's not there
		// already.
		GuiVar_TwoWireDiscoveryProgressBar = TWO_WIRE_PROGRESS_BAR_MAX;

		// ---------------------

		if( tpmicro_data.two_wire_cable_excessive_current_xmission_state != TWO_WIRE_CABLE_ANOMALY_STATE_idle )
		{
			GuiLib_CurStructureNdx = GuiStruct_dlgTwoWireCableShort_0;
		}
		else if( tpmicro_data.two_wire_cable_over_heated_xmission_state != TWO_WIRE_CABLE_ANOMALY_STATE_idle )
		{
			GuiLib_CurStructureNdx = GuiStruct_dlgTwoWireCableOverheated_Disc_0;
		}
		else if( (GuiVar_TwoWireNumDiscoveredStaDecoders > 0) || (GuiVar_TwoWireNumDiscoveredPOCDecoders > 0) || (GuiVar_TwoWireNumDiscoveredMoisDecoders > 0) ) 
		{
			GuiVar_TwoWireNumDiscoveredStaDecoders = 0;
			GuiVar_TwoWireNumDiscoveredPOCDecoders = 0;
			GuiVar_TwoWireNumDiscoveredMoisDecoders = 0;

			// 4/16/2014 ajv : Get an actual count of station and POC decoders by
			// looping through the decoder_info array and looking at each decoder
			// type.
			for( i = 0; i < MAX_DECODERS_IN_A_BOX; ++i )
			{
				switch( tpmicro_data.decoder_info[ i ].id_info.decoder_type__tpmicro_type )
				{
					case DECODER__TPMICRO_TYPE__2_STATION:
						GuiVar_TwoWireNumDiscoveredStaDecoders++;
						break;

					case DECODER__TPMICRO_TYPE__POC:
						GuiVar_TwoWireNumDiscoveredPOCDecoders++;
						break;

					case DECODER__TPMICRO_TYPE__MOISTURE:
						GuiVar_TwoWireNumDiscoveredMoisDecoders++;
						break;
				}
			}

			GuiVar_TwoWireDiscoveryState = TWO_WIRE_DISCOVERY_DIALOG_STATE_COMPLETE;
		}
		else
		{
			GuiVar_TwoWireDiscoveryState = TWO_WIRE_DISCOVERY_DIALOG_STATE_NONE_FOUND;
		}
	}

	FDTO_DIALOG_redraw_ok_dialog();
}

/* ---------------------------------------------------------- */
extern void FDTO_TWO_WIRE_close_discovery_dialog( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event, BOOL_32 *const pdialog_visible_ptr )
{
	if( (GuiVar_TwoWireDiscoveryState == TWO_WIRE_DISCOVERY_DIALOG_STATE_COMPLETE) && (pkey_event.keycode == KEY_SELECT) )
	{
		// 11/8/2013 ajv : Don't play a good key beep since one will be
		// generated when it goes to the Two-Wire Station Assignment screen.

		*pdialog_visible_ptr = (false);

		// ----------
		
		// 4/16/2014 ajv : Display a dialog prompting the user which screen
		// to go to.

		good_key_beep();

		DISPLAY_EVENT_STRUCT	lde;

		lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
		lde._04_func_ptr = (void*)&FDTO_TWO_WIRE_ASSIGNMENT_draw_dialog;
		lde._06_u32_argument1 = (true);
		Display_Post_Command( &lde );
	}
	else if( (GuiVar_TwoWireDiscoveryState == TWO_WIRE_DISCOVERY_DIALOG_STATE_DISCOVERING) && (pkey_event.keycode == KEY_SELECT) )
	{
		// 10/18/2013 ajv : Since there's no OK button on the discovering
		// decoders dialog while a discovery is in process, block the SELECT
		// button from closing the dialog. However, if they press BACK, close
		// the dialog.
		bad_key_beep();
	}
	else
	{
		good_key_beep();

		g_DIALOG_modal_result = MODAL_RESULT_OK;

		DIALOG_close_ok_dialog();
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

