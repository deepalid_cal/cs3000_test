/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"alerts.h"

#include	"app_startup.h"

#include	"budgets.h"

#include	"combobox.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"e_budget_setup.h"

#include	"e_budgets.h"

#include	"epson_rx_8025sa.h"

#include	"irri_comm.h"

#include	"irrigation_system.h"

#include	"speaker.h"

#include	"e_numeric_keypad.h"


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// 3/22/2016 skc : Budgets and Mainlines

// These need to match the Cursor fields defined in EasyGUI
#define CP_BUDGET_IN_USE					(0)
#define CP_BUDGET_PERIODS_PER_YEAR_IDX		(1)
#define CP_BUDGET_OPTION_IDX				(2)
#define CP_BUDGET_PERCENT_ET				(3)
#define CP_BUDGET_MODE_IDX					(4)
#define CP_BUDGET_SET_VALUES				(5)
#define CP_BUDGET_CALCULATE_SQUARE_FOOTAGE	(6)

// ----------

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static BOOL_32	g_BUDGET_SETUP_editing_group;

// ----------

static UNS_32 	g_BUDGET_SETUP_number_annual_periods;

// ----------

static	UNS_32	g_BUDGET_SETUP_entry_option;

// 5/25/2016 skc : When going from Budget setup to Budget data entry and back,
// helps with setting the active cursor position.
static BOOL_32 g_BUDGET_setup_button_pressed;

// 9/20/2018 Ryan : global for saving cursor posistion from left menu when using the Enter
// POC Budget amounts button.
static UNS_32	g_BUDGET_setup_selected_group;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void FDTO_BUDGET_SETUP_return_to_menu( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void BUDGET_SETUP_process_annual_periods_changed()
{
	IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys;
	UNS_32 month;
	UNS_32 year;
	DATE_TIME_COMPLETE_STRUCT dtcs;

	// ----------

	if( g_BUDGET_SETUP_number_annual_periods != GuiVar_BudgetPeriodsPerYearIdx )
	{
		EPSON_obtain_latest_complete_time_and_date( &dtcs );
		month = dtcs.__month;
		year = dtcs.__year;

		ptr_sys = SYSTEM_get_group_at_this_index( g_GROUP_list_item_index );

		// ----------

		// initialize the data when changing number of annual periods
		if( GuiVar_BudgetPeriodsPerYearIdx == 0 ) // 6 budget periods per year
		{
			// initialize dates
			// 4/26/2016 skc : Let's only start on odd-numbered months (Jan, Mar, May, Jul, Sep, Nov )
			if( month % 2 == 0 )
			{
				 month -= 1;
			}

			// initialize dates and zero budget values
			BUDGET_reset_budget_values( ptr_sys, month, year, 2 );

		}
		else // 12 budget periods per year
		{
			// initialize dates and zero budget values
			BUDGET_reset_budget_values( ptr_sys, month, year, 1 );
		}
	}
}

/* ---------------------------------------------------------- */
static void BUDGET_SETUP_process_entry_option_changed()
{
	IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys;
	UNS_32 month;
	UNS_32 year;
	DATE_TIME_COMPLETE_STRUCT dtcs;

	// ----------

	// 9/27/2018 Ryan : Only if budget entry option has changed
	if( g_BUDGET_SETUP_entry_option != GuiVar_BudgetEntryOptionIdx)
	{
		EPSON_obtain_latest_complete_time_and_date( &dtcs );
		month = dtcs.__month;
		year = dtcs.__year;

		ptr_sys = SYSTEM_get_group_at_this_index( g_GROUP_list_item_index );
		
		// 9/27/2018 Ryan : Reset budget values depending on entry option.
		if( GuiVar_BudgetEntryOptionIdx == POC_BUDGET_GALLONS_ENTRY_OPTION_direct_entry )
		{
			if( GuiVar_BudgetPeriodsPerYearIdx == 0 ) // 6 budget periods per year
			{
				// initialize dates
				// 4/26/2016 skc : Let's only start on odd-numbered months (Jan, Mar, May, Jul, Sep, Nov )
				if( month % 2 == 0 )
				{
					 month -= 1;
				}
	
				// initialize dates and zero budget values
				BUDGET_reset_budget_values( ptr_sys, month, year, 2 );
	
			}
			else // 12 budget periods per year
			{
				// initialize dates and zero budget values
				BUDGET_reset_budget_values( ptr_sys, month, year, 1 );
			}
		}
		else
		{
			BUDGET_reset_budget_values( ptr_sys, month, year, 1 );
		}	
	}
	
	// 9/10/2018 Ryan : when set to et entry option, the Auomatic budget mode is disabled.
	if( GuiVar_BudgetEntryOptionIdx == POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_using_et )
	{
		GuiVar_BudgetModeIdx = IRRIGATION_SYSTEM_BUDGET_MODE_ALERT_ONLY;
	}
}

static void BUDGET_pre_process_periods_per_year_changed()
{
	if( g_BUDGET_SETUP_number_annual_periods != GuiVar_BudgetPeriodsPerYearIdx )
	{
		DIALOG_draw_yes_no_cancel_dialog( GuiStruct_dlgBudgetChangeAnnualPeriods_0 );
	}

	//GuiLib_Cursor_Select( CP_BUDGET_PERIODS_PER_YEAR_IDX );
}

/* ---------------------------------------------------------- */
/**
 * Brings up a dialog box if user has made a change to the budget entry option.
 * 
 * @executed Executed within the context of process groups function of the budget setup
 * screen and the function that closes the entry option dropdown box.
 * 
 * @param none
 * 
 * @return void
 *
 * @author 8/10/2018 Ryan
 *
 * @revisions
 *    8/10/2018 Initial release
 */
static void BUDGET_pre_entry_option_changed()
{
	if( g_BUDGET_SETUP_entry_option != GuiVar_BudgetEntryOptionIdx )
	{
		DIALOG_draw_yes_no_cancel_dialog( GuiStruct_dlgBudgetChangeEntryOption_0 );
	}
}

/* ---------------------------------------------------------- */
static void BUDGET_SETUP_process_number_annual_periods_changed_dialog( void )
{
	switch( g_DIALOG_modal_result )
	{
	case MODAL_RESULT_YES:
		BUDGET_SETUP_process_annual_periods_changed(); 
		break;

	case MODAL_RESULT_NO:
	case MODAL_RESULT_CANCEL:
		GuiVar_BudgetPeriodsPerYearIdx = g_BUDGET_SETUP_number_annual_periods;
		break;
	}

    Refresh_Screen();
} 

/* ---------------------------------------------------------- */
/**
 * Process keys when the budget change entry options dialog is active.
 * 
 * @executed Executed within the context of process groups function of the budget setup
 * screen, in the case where the budget change entry options dialog is active
 * 
 * @param none
 * 
 * @return void
 *
 * @author 8/10/2018 Ryan
 *
 * @revisions
 *    8/10/2018 Initial release
 */
static void BUDGET_SETUP_process_entry_option_changed_dialog( void )
{
	switch( g_DIALOG_modal_result )
	{
	case MODAL_RESULT_YES:
		BUDGET_SETUP_process_entry_option_changed(); 
		break;

	case MODAL_RESULT_NO:
	case MODAL_RESULT_CANCEL:
		GuiVar_BudgetEntryOptionIdx = g_BUDGET_SETUP_entry_option;
		break;
	}

    Refresh_Screen();
} 

/* ---------------------------------------------------------- */
static void FDTO_POC_show_budget_in_use_dropdown( void )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( 213, 38, GuiVar_BudgetInUse );
}

/* ---------------------------------------------------------- */
static void FDTO_POC_show_budget_periods_per_year_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxBudgetPeriodsPerYear_0, &FDTO_COMBOBOX_add_items, 2, GuiVar_BudgetPeriodsPerYearIdx );
}

/* ---------------------------------------------------------- */
static void FDTO_POC_close_budget_periods_per_year_dropdown( void )
{
	g_BUDGET_SETUP_number_annual_periods = GuiVar_BudgetPeriodsPerYearIdx;

	GuiVar_BudgetPeriodsPerYearIdx = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );

	FDTO_COMBOBOX_hide();

	BUDGET_pre_process_periods_per_year_changed();
}

/* ---------------------------------------------------------- */
/**
 * Show dropdown box for budget entry options
 * 
 * @executed Executed within the context of process groups function of the budget setup
 * screen when the entry option is selected.
 * 
 * @param none
 * 
 * @return void
 *
 * @author 8/10/2018 Ryan
 *
 * @revisions
 *    8/10/2018 Initial release
 */
static void FDTO_POC_show_budget_option_dropdown( void )
{
	// 8/14/2018 Ryan : Added 2 options for dropdown box, depending on where the budget entry
	// option is on the screen.
	if( GuiVar_BudgetEntryOptionIdx == POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_using_annual_number )
	{
		FDTO_COMBOBOX_show( GuiStruct_cbxBudgetOption_1, &FDTO_COMBOBOX_add_items, ( POC_BUDGET_GALLONS_ENTRY_OPTION_MAX + 1 ), GuiVar_BudgetEntryOptionIdx );
	}
	else
	{
		FDTO_COMBOBOX_show( GuiStruct_cbxBudgetOption_0, &FDTO_COMBOBOX_add_items, ( POC_BUDGET_GALLONS_ENTRY_OPTION_MAX + 1 ), GuiVar_BudgetEntryOptionIdx );
	}
}

/* ---------------------------------------------------------- */
/**  
 * Closes the dropdown box for budget entry options.
 *
 * @executed Executed within the context of process groups function of the budget setup
 * screen.
 * 
 * @param none
 * 
 * @return void
 *
 * @author 8/10/2018 Ryan
 *
 * @revisions
 *    8/10/2018 Initial release
 */
static void FDTO_POC_close_budget_option_dropdown( void )
{
	g_BUDGET_SETUP_entry_option = GuiVar_BudgetEntryOptionIdx;
	
	GuiVar_BudgetEntryOptionIdx = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );
	
	FDTO_COMBOBOX_hide();
	
	BUDGET_pre_entry_option_changed();
}

/* ---------------------------------------------------------- */
static void FDTO_POC_show_budget_mode_dropdown( void )
{
	// 8/14/2018 Ryan : Depending on which budget mode is selected, the dropdown box will show
	// on different points on the screen.
	if( GuiVar_BudgetEntryOptionIdx == POC_BUDGET_GALLONS_ENTRY_OPTION_direct_entry )
	{
		FDTO_COMBOBOX_show( GuiStruct_cbxBudgetMode_0, &FDTO_COMBOBOX_add_items, 2, GuiVar_BudgetModeIdx );
	}
	else
	{
		FDTO_COMBOBOX_show( GuiStruct_cbxBudgetMode_1, &FDTO_COMBOBOX_add_items, 2, GuiVar_BudgetModeIdx );
	}
}

static void FDTO_POC_close_budget_mode_dropdown( void )
{
	GuiVar_BudgetModeIdx = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );
	FDTO_COMBOBOX_hide();

	GuiLib_Cursor_Select( CP_BUDGET_MODE_IDX );
}

/* ---------------------------------------------------------- */
static void BUDGET_SETUP_process_group( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT lde;
	IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys;

	// ----------

	switch( GuiLib_CurStructureNdx )
	{
	case GuiStruct_cbxNoYes_0:
		COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_BudgetInUse );
		break;

	case GuiStruct_cbxBudgetPeriodsPerYear_0:
		if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
		{
			good_key_beep();
			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = FDTO_POC_close_budget_periods_per_year_dropdown;
			Display_Post_Command( &lde );
		}
		else
		{
			COMBO_BOX_key_press( pkey_event.keycode, NULL );
		}
		break;
		
	// 9/27/2018 Ryan : Adding case for keypad used for ET percent entry.
	case GuiStruct_dlgNumericKeypad_0:
		NUMERIC_KEYPAD_process_key( pkey_event, &FDTO_BUDGET_SETUP_draw_menu );
		break;

	// 8/9/2018 Ryan : Adding case for budget entry option dropdown.
	case GuiStruct_cbxBudgetOption_0:
	case GuiStruct_cbxBudgetOption_1:
		if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
		{
			good_key_beep();
			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = FDTO_POC_close_budget_option_dropdown;
			Display_Post_Command( &lde );
		}
		else
		{
			COMBO_BOX_key_press( pkey_event.keycode, NULL );
		}
		break;

	case GuiStruct_cbxBudgetMode_0:
	case GuiStruct_cbxBudgetMode_1:
		if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
		{
			good_key_beep();
			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = FDTO_POC_close_budget_mode_dropdown;
			Display_Post_Command( &lde );
			BUDGET_SETUP_process_number_annual_periods_changed_dialog();
		}
		else
		{
			COMBO_BOX_key_press( pkey_event.keycode, NULL );
		}
		break;

	case GuiStruct_dlgBudgetChangeAnnualPeriods_0:
		DIALOG_process_yes_no_cancel_dialog( pkey_event, (void *)&BUDGET_SETUP_process_number_annual_periods_changed_dialog );
		break;
		
	case GuiStruct_dlgBudgetChangeEntryOption_0:
		DIALOG_process_yes_no_cancel_dialog( pkey_event, (void *)&BUDGET_SETUP_process_entry_option_changed_dialog );
		break;

	default:
		switch( pkey_event.keycode )
		{
			case KEY_SELECT:
				switch( GuiLib_ActiveCursorFieldNo )
				{
				case CP_BUDGET_IN_USE: 
					// 5/23/2016 skc : If the system has no POCs attached, then don't let user enable budgets.
					ptr_sys = SYSTEM_get_group_at_this_index( g_GROUP_list_item_index );
					if( SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid(nm_GROUP_get_group_ID(ptr_sys))->sbf.number_of_pocs_in_this_system > 0 )
					{
						good_key_beep();
						lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
						lde._04_func_ptr = (void*)&FDTO_POC_show_budget_in_use_dropdown;
						Display_Post_Command( &lde );
					}
					else
					{
						bad_key_beep();
					}
					break;
			
				case CP_BUDGET_PERIODS_PER_YEAR_IDX:
					good_key_beep();
					lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
					lde._04_func_ptr = (void*)&FDTO_POC_show_budget_periods_per_year_dropdown;
					Display_Post_Command( &lde );
					break;
				// 8/9/2018 Ryan : Adding ability for user to change the budget entry option.
				case CP_BUDGET_OPTION_IDX:
					good_key_beep();
					lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
					lde._04_func_ptr = (void*)&FDTO_POC_show_budget_option_dropdown ;
					Display_Post_Command( &lde );
					break;

				// 10/29/2018 Ryan : Moved ET percentage to it own cursor posistion
				case CP_BUDGET_PERCENT_ET:
					good_key_beep();
					NUMERIC_KEYPAD_draw_uint32_keypad( &GuiVar_BudgetETPercentageUsed, POC_BUDGET_CALCULATION_PERCENT_OF_ET_MIN, POC_BUDGET_CALCULATION_PERCENT_OF_ET_MAX );
					break;
				
				case CP_BUDGET_MODE_IDX:
					// 9/10/2018 Ryan : when set to et entry option, the Auomatic budget mode is disabled.
					if( GuiVar_BudgetEntryOptionIdx == POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_using_et )
					{
						bad_key_beep();
					}
					else
					{
						good_key_beep();
						lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
						lde._04_func_ptr = (void*)&FDTO_POC_show_budget_mode_dropdown ;
						Display_Post_Command( &lde );
					}
					break;
				case CP_BUDGET_SET_VALUES:
					// 5/25/2016 skc : We are leaving the screen, save changes.
					BUDGET_SETUP_extract_and_store_changes_from_GuiVars(); 

					// 5/25/2016 skc : Flag to prevent recursive drawing when returning to this screen.
					g_BUDGET_setup_button_pressed = true;
					
					// 9/20/2018 Ryan : Save currently selected group, for when user presses the back key.
					g_BUDGET_setup_selected_group = g_GROUP_list_item_index;

					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._03_structure_to_draw = GuiStruct_scrBudgetAmountsPerPOC_0;
					lde._04_func_ptr = (void *)&FDTO_BUDGETS_draw_menu;
					lde.key_process_func_ptr = BUDGETS_process_menu;
					lde._06_u32_argument1 = (true);
					lde._08_screen_to_draw = GuiVar_MenuScreenToShow;
					Change_Screen( &lde );

					break;
				// 8/21/2018 Ryan : case for pressing button for calculating square footage.
				case CP_BUDGET_CALCULATE_SQUARE_FOOTAGE:
					good_key_beep();
					
					// 11/1/2018 Ryan : load guiVAR for dialog boxes.
					ptr_sys = SYSTEM_get_group_at_this_index( g_GROUP_list_item_index );
					strlcpy( GuiVar_BudgetMainlineName, nm_GROUP_get_name( ptr_sys ), sizeof(GuiVar_BudgetMainlineName) );

					// 11/1/2018 Ryan : show one of the dialog boxes depending on if any of the stations had
					// their square footages calculated.
					if( BUDGET_calculate_square_footage( g_GROUP_list_item_index ) == true )
					{
						DIALOG_draw_ok_dialog( GuiStruct_dlgSquareFootageCalculation_0 );
					}
					else
					{
						DIALOG_draw_ok_dialog( GuiStruct_dlgSquareFootageCalculation_1 );
					}
					break;
					
				default:
						bad_key_beep();
				}
				Refresh_Screen();
				break;

			case KEY_MINUS:
			case KEY_PLUS:
				switch( GuiLib_ActiveCursorFieldNo )
				{
					case CP_BUDGET_IN_USE:
						// 5/23/2016 skc : If the system has no POCs attached, then don't let user enable budgets.
						ptr_sys = SYSTEM_get_group_at_this_index( g_GROUP_list_item_index );
						if( SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid(nm_GROUP_get_group_ID(ptr_sys))->sbf.number_of_pocs_in_this_system > 0 )
						{
							process_bool( &GuiVar_BudgetInUse );
							Redraw_Screen( false ); 
						}
						else
						{
							bad_key_beep();
						}
						break;

					case CP_BUDGET_PERIODS_PER_YEAR_IDX:
						g_BUDGET_SETUP_number_annual_periods = GuiVar_BudgetPeriodsPerYearIdx;
						process_uns32( pkey_event.keycode, &GuiVar_BudgetPeriodsPerYearIdx, 0, 1, 1, true );
						 
						BUDGET_pre_process_periods_per_year_changed();
						break;

					// 8/9/2018 Ryan : Added to allow user to change budget entry options.
					case CP_BUDGET_OPTION_IDX:
						g_BUDGET_SETUP_entry_option = GuiVar_BudgetEntryOptionIdx;
						process_uns32( pkey_event.keycode, &GuiVar_BudgetEntryOptionIdx,
									   POC_BUDGET_GALLONS_ENTRY_OPTION_MIN, POC_BUDGET_GALLONS_ENTRY_OPTION_MAX, 1, true );

						BUDGET_pre_entry_option_changed();
						Redraw_Screen( false );
						break;

					// 10/30/2018 Ryan : Case for new ET percentage cursor position.
					case CP_BUDGET_PERCENT_ET:
							process_uns32( pkey_event.keycode, &GuiVar_BudgetETPercentageUsed, POC_BUDGET_CALCULATION_PERCENT_OF_ET_MIN, POC_BUDGET_CALCULATION_PERCENT_OF_ET_MAX, 1, false );
							break;
					
					case CP_BUDGET_MODE_IDX:
						// 9/10/2018 Ryan : when set to et entry option, the Auomatic budget mode is disabled.
						if( GuiVar_BudgetEntryOptionIdx == POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_using_et )
						{
							bad_key_beep();
						}
						else
						{
							process_uns32( pkey_event.keycode, &GuiVar_BudgetModeIdx,
										   IRRIGATION_SYSTEM_BUDGET_MODE_MIN, IRRIGATION_SYSTEM_BUDGET_MODE_MAX, 1, true );
	
							// 7/18/2016 skc : EasyGUI was uncooperative with a paragraph widget.
							Redraw_Screen( false );
						}
						break;

#if 0
					case CP_BUDGET_SETUP_METER_READ_TIME:
						BUDGET_SETUP_process_meter_read_time( pkey_event.keycode );
#endif

					default:
						bad_key_beep();
				}
				Refresh_Screen();
				break;

			case KEY_NEXT:
			case KEY_PREV:
				GROUP_process_NEXT_and_PREV( pkey_event.keycode, SYSTEM_num_systems_in_use(), (void*)&BUDGET_SETUP_extract_and_store_changes_from_GuiVars, (void*)&SYSTEM_get_group_at_this_index, (void*)&BUDGET_SETUP_copy_group_into_guivars );
				break;

		case KEY_C_UP:
				switch( GuiLib_ActiveCursorFieldNo )
				{
					case CP_BUDGET_IN_USE:
						KEY_process_BACK_from_editing_screen( (void*)&FDTO_BUDGET_SETUP_return_to_menu );
						break;
					// 8/9/2018 Ryan : when annual budget entry option selected, CP_BUDGET_PERIODS_PER_YEAR_IDX
					// is hidden, so we skip over it.
					case CP_BUDGET_OPTION_IDX:
						if( GuiVar_BudgetEntryOptionIdx == POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_using_annual_number )
						{
							GuiLib_ActiveCursorFieldNo = CP_BUDGET_PERIODS_PER_YEAR_IDX;
							CURSOR_Up( true );
						}
						else
						{
							CURSOR_Up( true );
						}
						break;
					default:
						CURSOR_Up( true );
				}
				break;

			case KEY_C_LEFT:
				switch( GuiLib_ActiveCursorFieldNo )
				{
					case CP_BUDGET_IN_USE:
						KEY_process_BACK_from_editing_screen( (void*)&FDTO_BUDGET_SETUP_return_to_menu );
						break;
					// 8/9/2018 Ryan : when annual budget entry option selected, CP_BUDGET_PERIODS_PER_YEAR_IDX
					// is hidden, so we skip over it.
					case CP_BUDGET_OPTION_IDX:
						if( GuiVar_BudgetEntryOptionIdx == POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_using_annual_number )
						{
							GuiLib_ActiveCursorFieldNo = CP_BUDGET_PERIODS_PER_YEAR_IDX;
							CURSOR_Up( true );
						}
						else
						{
							CURSOR_Up( true );
						}
						break;
					default:
						CURSOR_Up( true );
				}
				break;

			case KEY_C_DOWN:
				switch( GuiLib_ActiveCursorFieldNo )
				{						
					// 8/23/2018 Ryan : The calculate square footage button only appears when using the
					// calculating ET entry option.
					case CP_BUDGET_SET_VALUES:
						if( GuiVar_BudgetEntryOptionIdx != POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_using_et )
						{
							bad_key_beep();
						}
						else
						{
							CURSOR_Down( true );
						}
						break;
					default:
						CURSOR_Down( true );
				}
				break;

			case KEY_C_RIGHT:
				switch( GuiLib_ActiveCursorFieldNo )
				{
					// 8/23/2018 Ryan : The calculate square footage button only appears when using the
					// calculating ET entry option.
					case CP_BUDGET_SET_VALUES:
						if( GuiVar_BudgetEntryOptionIdx != POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_using_et )
						{
							bad_key_beep();
						}
						else
						{
							CURSOR_Down( true );
						}
						break;
					default:
						CURSOR_Down( true );
				}
				break;

			case KEY_BACK:
				KEY_process_BACK_from_editing_screen( (void*)&FDTO_BUDGET_SETUP_return_to_menu );
				break;

			default:
				KEY_process_global_keys( pkey_event );
		}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_BUDGET_SETUP_return_to_menu( void )
{
	FDTO_GROUP_return_to_menu( &g_BUDGET_SETUP_editing_group, (void*)&BUDGET_SETUP_extract_and_store_changes_from_GuiVars );
}

/* ---------------------------------------------------------- */
extern void FDTO_BUDGET_SETUP_draw_menu( const BOOL_32 pcomplete_redraw )
{
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	// 9/20/2018 Ryan : sets the correct group that was active when the user left the menu. Must
	// be done before the screen is drawn, or the draw menu function could try to draw a
	// mainline that doesn't exsist.
	if( true == g_BUDGET_setup_button_pressed )
	{
		g_GROUP_list_item_index = g_BUDGET_setup_selected_group;
	}

	FDTO_GROUP_draw_menu( pcomplete_redraw, &g_BUDGET_SETUP_editing_group, SYSTEM_num_systems_in_use(), 
						  GuiStruct_scrBudgetSetup_0, (void *)&SYSTEM_load_system_name_into_scroll_box_guivar, 
						  (void *)&BUDGET_SETUP_copy_group_into_guivars, (false) );

	// 5/25/2016 skc : If returning from entering budget data, set the active cursor.
	if( true == g_BUDGET_setup_button_pressed )
	{
		g_BUDGET_setup_button_pressed = false; // Clear flag!!! prevent recursion
		g_BUDGET_SETUP_editing_group = (true);
		GuiLib_ActiveCursorFieldNo = (INT_16)CP_BUDGET_SET_VALUES;
		FDTO_Redraw_Screen( (false) ); // causes recursive calls back to here
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void BUDGET_SETUP_process_menu( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	GROUP_process_menu( pkey_event, &g_BUDGET_SETUP_editing_group, SYSTEM_num_systems_in_use(), (void *)&BUDGET_SETUP_process_group, NULL, (void *)&SYSTEM_get_group_at_this_index, (void *)&BUDGET_SETUP_copy_group_into_guivars );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

