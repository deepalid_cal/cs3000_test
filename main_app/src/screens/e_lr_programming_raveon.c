/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/9/2015 mpd : Required for atoi
#include	<stdlib.h>

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h> 

#include	"e_lr_programming_raveon.h"

#include	"device_common.h"

#include	"comm_mngr.h"

#include	"cursor_utils.h"

#include	"d_comm_options.h"

#include	"d_device_exchange.h"

#include	"d_process.h"

#include	"device_LR_RAVEON.h"

#include	"dialog.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_LR_RAVEON_PROGRAMMING_FREQUENCY_WHOLE_NUM		(0)

#define	CP_LR_RAVEON_PROGRAMMING_FREQUENCY_DECIMAL			(1) 

#define	CP_LR_RAVEON_PROGRAMMING_XMIT_POWER					(2)

#if 0
// 11/22/2017 ajv : This variable has been removed from the screen now that Sandia Labs, our
// only UB customer, is migrating to UC radios. 
#define	CP_LR_RAVEON_PROGRAMMING_USING_UB				(3)
#endif

#define	CP_LR_RAVEON_PROGRAMMING_READ_DEVICE				(4)
 
#define	CP_LR_RAVEON_PROGRAMMING_PROGRAM_DEVICE				(5)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32	g_LR_PROGRAMMING_previous_cursor_pos;

extern BOOL_32	LR_RAVEON_PROGRAMMING_querying_device;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*!
 * 
 * @name void LR_RAVEON_PROGRAMMING_initialize_guivars(const UNS_32 pport)
 * @description: initializes the Gui Variables associated with the device exchange screen 
 * @constraints: none 
 * @param const UNS_32 pport: port on which the device is connected (typically A or B) 
 */
static void LR_RAVEON_PROGRAMMING_initialize_guivars( const UNS_32 pport )
{
	// 4/20/2015 ajv : Set the device exchange result to 'read_ok' to display the initialized
	// variables.
	GuiVar_CommOptionDeviceExchangeResult = DEVICE_EXCHANGE_KEY_read_settings_completed_ok;

	// 3/3/2015 mpd : Initialize all screen variables. These will be overridden once the radio
	// values are read.
	strlcpy( GuiVar_LRSerialNumber, "--------", sizeof(GuiVar_LRSerialNumber) );

	// 4/26/2017 ajv : TODO Modify easyGUI to use UPORT defines (1-based) instead of the 0-based
	// that Mike originally designed.
	GuiVar_LRPort = pport;

	GuiVar_LRFrequency_WholeNum = LR_RAVEON_FREQUENCY_WHOLENUM_MIN; 
	 
	GuiVar_LRFrequency_Decimal = LR_RAVEON_FREQUENCY_DECIMAL_MIN; 

	GuiVar_LRTransmitPower = LR_RAVEON_XMT_POWER_MIN;

	GuiVar_LRHubFeatureNotAvailable = (false);
}

/* ---------------------------------------------------------- */
/*!
 * 
 * @name void FDTO_LR_RAVEON_PROGRAMMING_process_device_exchange_key(const UNS_32 pkeycode, const UNS_32 pport)
 * @description: Processes the psuedo-keys that are really the status of exchange with the 
 * device with which the controller is communicating 
 * @constraints: Should only be called from the Display task 
 * @param const UNS_32 pkeycode: pseudeo-key-code, which is the communication status with 
 * the device 
 * @param const UNS_32 pport: port on which the device is connected 
 */
static void FDTO_LR_RAVEON_PROGRAMMING_process_device_exchange_key( const UNS_32 pkeycode, const UNS_32 pport)
{
	switch( pkeycode )
	{
		case DEVICE_EXCHANGE_KEY_read_settings_completed_ok:

		case DEVICE_EXCHANGE_KEY_write_settings_completed_ok:

			// 4/14/2015 mpd : Get values from the programming struct, then redraw screen.
			LR_RAVEON_PROGRAMMING_copy_settings_into_guivars();

			// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
			GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_completed_ok );

			// 5/18/2015 ajv : Ensure the flag that indicates a dialog box is open is set to false.
			DIALOG_close_ok_dialog();

			FDTO_LR_PROGRAMMING_raveon_draw_screen( (false), pport );
			break;

		case DEVICE_EXCHANGE_KEY_busy_try_again_later:
		case DEVICE_EXCHANGE_KEY_read_settings_error:
		case DEVICE_EXCHANGE_KEY_read_settings_in_progress:
		case DEVICE_EXCHANGE_KEY_write_settings_error:
		case DEVICE_EXCHANGE_KEY_write_settings_in_progress:
			// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
			GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( pkeycode );

			// 5/18/2015 ajv : Update the progress dialog.
			DEVICE_EXCHANGE_draw_dialog();
			break;

		default:
			// 4/14/2015 mpd : Nothing more to do.
			break;
	}
}

/* ---------------------------------------------------------- */
/*!
 * 
 * @name void LR_RAVEON_PROGRAMMING_post_device_query_to_queue(const UNS_32 pport, const UNS_32 poperation)
 * @description: posts a request for comm_mngr to read/write to a device on a 
 * port 
 * @constraints: none 
 * @param const UNS_32 pport: port to which the device is connected (typically A or B) 
 * @param const UNS_32 poperation: type of operation: Read/ Write 
 */
static void LR_RAVEON_PROGRAMMING_post_device_query_to_queue( const UNS_32 pport, const UNS_32 poperation )
{
	COMM_MNGR_TASK_QUEUE_STRUCT	cmqs;

	// ----------

	LR_RAVEON_PROGRAMMING_querying_device = (true);

	// 3/18/2014 rmd : Post the event to cause the comm_mngr to perform the device settings
	// readout and write the results to each of the guivars.

	if( poperation == LR_RAVEON_WRITE )
	{
		cmqs.event = COMM_MNGR_EVENT_request_write_device_settings;

		comm_mngr.device_exchange_state = LR_RAVEON_WRITE;
	}
	else
	{
		cmqs.event = COMM_MNGR_EVENT_request_read_device_settings;

		comm_mngr.device_exchange_state = LR_RAVEON_READ; 
	}

	cmqs.port = pport;

	COMM_MNGR_post_event_with_details( &cmqs );
}

/* ---------------------------------------------------------- */
/*!
 * 
 * @name void FDTO_LR_PROGRAMMING_raveon_draw_screen(const BOOL_32 pcomplete_redraw, const UNS_32 pport)
 * @description: draws the screen for controlling the radio connected to port given by pport
 * @constraints: Should only be called from the Display task 
 * @param const BOOL_32 pcomplete_redraw: boolean indicating if the complete redraw is 
 * requested 
 * @param const UNS_32 pport: port on which the device is connected 
 */
extern void FDTO_LR_PROGRAMMING_raveon_draw_screen( const BOOL_32 pcomplete_redraw, const UNS_32 pport )
{
	UNS_32	lcursor_to_select;

	if( (pcomplete_redraw == (true)) )
	{
		LR_RAVEON_PROGRAMMING_initialize_guivars( pport );

		if( GuiVar_LRRadioType != RAVEON_RADIO_ID_SONIK )
		{
			// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
			// 8/31/2017 BE: The instruction below is predicated on doing a read, immediately after
			// screen draw; however, SONIK does not have such a read mode. 
			GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress );
		}

		lcursor_to_select = CP_LR_RAVEON_PROGRAMMING_FREQUENCY_WHOLE_NUM;
	}
	else
	{
		if( GuiLib_ActiveCursorFieldNo == GuiLib_NO_CURSOR )
		{
			lcursor_to_select = g_LR_PROGRAMMING_previous_cursor_pos;
		}
		else
		{
			lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
		}
	}

	GuiLib_ShowScreen( GuiStruct_scrLRProgramming_Raveon_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();

	if( pcomplete_redraw == (true) )
	{
		if( GuiVar_LRRadioType != RAVEON_RADIO_ID_SONIK )
		{
			// 5/18/2015 ajv : Immediately display the dialog to indicate to the user that something is
			// happening
			DEVICE_EXCHANGE_draw_dialog();

			// 3/21/2014 ajv : Start the data retrieval automatically for the user. This isn't
			// necessary, but it's a nicety, preventing the user from having to press the Read Radio
			// button.
			LR_RAVEON_PROGRAMMING_post_device_query_to_queue( pport, LR_RAVEON_READ );
		}
		else 
		{   
			//initialize the sonik state struct especially since it is not going through the automatic 
			//read cycle, which initializes this struct.  
			LR_RAVEON_initialize_state_struct();
		}
	}
}

/* ------------------------------------------------------------------------------------------------------ */
/*!
 * 
 * @name void LR_PROGRAMMING_raveon_process_screen(const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event)
 * @description: process keys pressed in the screen drawn by the corresponding draw_screen 
 * function 
 * @constraints: none 
 * @param const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event 
 */
extern void LR_PROGRAMMING_raveon_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		default:
			switch( pkey_event.keycode )
			{
				case DEVICE_EXCHANGE_KEY_busy_try_again_later:

				case DEVICE_EXCHANGE_KEY_read_settings_completed_ok:
				case DEVICE_EXCHANGE_KEY_read_settings_error:
				case DEVICE_EXCHANGE_KEY_read_settings_in_progress:

				case DEVICE_EXCHANGE_KEY_write_settings_completed_ok:
				case DEVICE_EXCHANGE_KEY_write_settings_error:
				case DEVICE_EXCHANGE_KEY_write_settings_in_progress:
					LR_RAVEON_PROGRAMMING_querying_device = (false);

					// 3/18/2014 rmd : Redraw the screen displaying the results or indicating the error 
					// condition. 
					lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;

					lde._04_func_ptr = (void*)&FDTO_LR_RAVEON_PROGRAMMING_process_device_exchange_key;

					lde._06_u32_argument1 = pkey_event.keycode;

					lde._07_u32_argument2 = GuiVar_LRPort;

					Display_Post_Command( &lde );
					break;

				case KEY_SELECT:
				{
					// 4/20/2015 ajv : If we're already in the middle of a device exchange or syncing radios,
					// ignore the key press.
					if( (GuiVar_CommOptionDeviceExchangeResult != ( e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress  ) ) ) && 
						(GuiVar_CommOptionDeviceExchangeResult != ( e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_write_settings_in_progress ) ) ) )
					{
						switch( GuiLib_ActiveCursorFieldNo )
						{
							case CP_LR_RAVEON_PROGRAMMING_READ_DEVICE:
								if( GuiVar_LRRadioType == RAVEON_RADIO_ID_SONIK )
								{
									bad_key_beep();
								}
								else 
								{
									good_key_beep();

									// 4/21/2015 ajv : Store the button that was highlighted when the user pressed SELECT. Once
									// the read process is done, we'll revert back to this button.
									g_LR_PROGRAMMING_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;
									// 5/18/2015 ajv : Immediately display the dialog to indicate to the user that something is

									GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress ); 

									DEVICE_EXCHANGE_draw_dialog();

									LR_RAVEON_PROGRAMMING_post_device_query_to_queue( GuiVar_LRPort, LR_RAVEON_READ );
								}
								break;

							case CP_LR_RAVEON_PROGRAMMING_PROGRAM_DEVICE:
								good_key_beep();

								// 4/21/2015 ajv : Store the button that was highlighted when the user pressed SELECT. Once
								// the write process is done, we'll revert back to this button.
								g_LR_PROGRAMMING_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

								// 4/9/2015 mpd : Send any user changes to the programming struct.
								LR_RAVEON_PROGRAMMING_extract_changes_from_guivars();

								// 5/18/2015 ajv : Immediately display the dialog
								GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_write_settings_in_progress ); 

								DEVICE_EXCHANGE_draw_dialog();

								LR_RAVEON_PROGRAMMING_post_device_query_to_queue( GuiVar_LRPort, LR_RAVEON_WRITE );
								break;

							default:
								bad_key_beep();
						}
					}
					else
					{
						bad_key_beep();
					}
					break;
				}
				case KEY_C_UP:
				{
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_LR_RAVEON_PROGRAMMING_FREQUENCY_WHOLE_NUM:
						case CP_LR_RAVEON_PROGRAMMING_FREQUENCY_DECIMAL: 
							bad_key_beep();
							break;

						case CP_LR_RAVEON_PROGRAMMING_PROGRAM_DEVICE:
						case CP_LR_RAVEON_PROGRAMMING_READ_DEVICE:
							if( GuiVar_LRRadioType == RAVEON_RADIO_ID_M7 )
							{
								CURSOR_Select( CP_LR_RAVEON_PROGRAMMING_XMIT_POWER, (true) );
							}
							else 
							{
								CURSOR_Select( CP_LR_RAVEON_PROGRAMMING_FREQUENCY_DECIMAL, (true) );
							}
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;
				}
				case KEY_C_LEFT:
				{
					CURSOR_Up( (true) );
					break;
				}
				case KEY_C_DOWN:
				{
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_LR_RAVEON_PROGRAMMING_FREQUENCY_WHOLE_NUM:
						case CP_LR_RAVEON_PROGRAMMING_FREQUENCY_DECIMAL:
							if( GuiVar_LRRadioType == RAVEON_RADIO_ID_M7 )
							{
								CURSOR_Select( CP_LR_RAVEON_PROGRAMMING_XMIT_POWER, (true) );
							}
							else 
							{
								CURSOR_Select( CP_LR_RAVEON_PROGRAMMING_FREQUENCY_WHOLE_NUM, (true) );
							}
							break;
						case CP_LR_RAVEON_PROGRAMMING_READ_DEVICE:
						case CP_LR_RAVEON_PROGRAMMING_PROGRAM_DEVICE:
							bad_key_beep();
							break;

						default:
							CURSOR_Down( (true) );
					}
					break;
				}
				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_PLUS:
				case KEY_MINUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_LR_RAVEON_PROGRAMMING_FREQUENCY_WHOLE_NUM:
							process_uns32( pkey_event.keycode, &GuiVar_LRFrequency_WholeNum, LR_RAVEON_FREQUENCY_WHOLENUM_MIN, LR_RAVEON_FREQUENCY_WHOLENUM_MAX, 1, (false) );
							break;

						case CP_LR_RAVEON_PROGRAMMING_FREQUENCY_DECIMAL:
							process_uns32( pkey_event.keycode, &GuiVar_LRFrequency_Decimal, LR_RAVEON_FREQUENCY_DECIMAL_MIN, LR_RAVEON_FREQUENCY_DECIMAL_MAX, 125, (false) );
							break;

						case CP_LR_RAVEON_PROGRAMMING_XMIT_POWER:
							//9/5/2017 BE: changed increment from 10 to 1; to support power mapping from 10% -- 100% to 
							//1 -- 10 
							process_uns32( pkey_event.keycode, &GuiVar_LRTransmitPower, LR_RAVEON_XMT_POWER_MIN, LR_RAVEON_XMT_POWER_MAX, 1, (false) );
							break;

						default:
							bad_key_beep();
					}
					
					Refresh_Screen();
					break;

				case KEY_BACK:	 
				{

					//8/16/2017 BE: Setup comm_mngr to come out of device exchange mode:
					task_control_EXIT_device_exchange_hammer (); 

					GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;

					KEY_process_global_keys( pkey_event );

					// 6/5/2015 ajv : Since the user got here from the COMM OPTIONS dialog box, redraw that
					// dialog box.
					COMM_OPTIONS_draw_dialog( (true) );
					break;
				}
				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

