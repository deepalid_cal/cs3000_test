/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"r_poc_preserves.h"

#include	"app_startup.h"

#include	"battery_backed_vars.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"m_main.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_POC_PRESERVES_POC_INDEX		(0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Updates the variables used to display the POC Preserves and forces a screen
 * redraw.
 * 
 * @executed Executed within the context of the DISPLAY PROCESSING task when the
 *  		 screen is initially drawn; within the context of the TD CHECK task
 *  		 as the screen is updated 4 times per second.
 *
 * @author 4/5/2012 Adrianusv
 *
 * @revisions
 *    4/5/2012   Initial release
 */
static void FDTO_POC_PRESERVES_redraw_report( void )
{
	POC_copy_preserve_info_into_guivars( GuiVar_POCPreservesIndex );

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Draws the POC preserves report.
 * 
 * @executed Executed within the context of the DISPLAY PROCESSING task when the
 *  		 screen is initially drawn; within the context of the TD CHECK task
 *  		 as the screen is updated 4 times per second.
 * 
 * @param pcomplete_redraw True if the screen is to be redrawn completely
 *  				        (which should only happen on initial entry into the
 *                          screen); otherwise, false.
 *
 * @author 4/5/2012 Adrianusv
 *
 * @revisions
 *    4/5/2012   Initial release
 */
extern void FDTO_POC_PRESERVES_draw_report( const BOOL_32 pcomplete_redraw )
{
	if( pcomplete_redraw == (true) )
	{
		GuiLib_ShowScreen( GuiStruct_rptPOCPreserves_0, CP_POC_PRESERVES_POC_INDEX, GuiLib_RESET_AUTO_REDRAW );

		GuiVar_POCPreservesIndex = 0;
	}

    FDTO_POC_PRESERVES_redraw_report();
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while on the POC preserves report.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 *
 * @param pkey_event The key event to be processed.
 *
 * @author 4/5/2012 Adrianusv
 *
 * @revisions
 *    4/5/2012   Initial release
 */
extern void POC_PRESERVES_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	UNS_32	lkey;

	switch( pkey_event.keycode )
	{
		case KEY_PLUS:
		case KEY_MINUS:
		case KEY_NEXT:
		case KEY_PREV:
			if( pkey_event.keycode == KEY_NEXT )
			{
				lkey = KEY_PLUS;
			}
			else if( pkey_event.keycode == KEY_PREV )
			{
				lkey = KEY_MINUS;
			}
			else
			{
				lkey = pkey_event.keycode;
			}

			process_uns32( lkey, &GuiVar_POCPreservesIndex, 0, (MAX_POCS_IN_NETWORK-1), 1, (true) );

			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = (void*)&FDTO_POC_PRESERVES_redraw_report;
			Display_Post_Command( &lde );
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;

			KEY_process_global_keys( pkey_event );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

