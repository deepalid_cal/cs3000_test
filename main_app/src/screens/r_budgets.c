
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 12/15/2015 skc : For strlcpy
#include	<string.h>

#include	<math.h>

#include 	<float.h>

#include    "r_budgets.h"

#include	"cal_math.h"

#include	"alerts.h"

#include    "app_startup.h"

#include    "budgets.h"

#include	"configuration_network.h"

#include	"e_poc.h"

#include	"epson_rx_8025sa.h"

#include    "group_base_file.h"

#include	"irrigation_system.h"

#include    "poc.h"

#include    "screen_utils.h"

#include	"scrollbox.h"

#include	"speaker.h"


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 03/10/2016 skc : Initial install at Jim's pointed to a mutex conflict
// using poc_preserves_recursive_mutex. The error log points to SYSTEM_get_used().
// Thus, the mutexes are optimized inside here to mitigate this potential failure point.
static void BUDGET_REPORT_copy_group_into_guivars( const UNS_32 ppoc_index_0 )
{
	// 5/25/2016 skc : For display in units of HCF
	const volatile float HCF_CONVERSION = 748.05f;
	float sf;

	POC_GROUP_STRUCT *lpoc;
	IRRIGATION_SYSTEM_GROUP_STRUCT *lsystem;
	STATION_GROUP_STRUCT *ptr_group;
	UNS_32 gid_sys;
	UNS_32 gid_poc;
	BUDGET_DETAILS_STRUCT bds;
	char str[ 224 ];
	DATE_TIME_COMPLETE_STRUCT today;
	UNS_32 num_used;
	float predicted;
	UNS_32 budget;
	float usedpoc;
	float Rpoc[ MAX_POCS_IN_NETWORK ];
	float ratio;
	UNS_32 percent;
	float reduction;
	BOOL_32 reduction_flag;
	UNS_32 num_groups;
	UNS_32 i;
	UNS_32 over_budget;

	EPSON_obtain_latest_complete_time_and_date( &today );

	// ----------

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc = POC_get_group_at_this_index( ppoc_index_0 );
	gid_sys = POC_get_GID_irrigation_system( lpoc );

	// 5/31/2016 skc : Make sure to set this easyGUI flag.
	GuiVar_BudgetUnitHCF = NETWORK_CONFIG_get_water_units();

	sf = (GuiVar_BudgetUnitHCF == 0 ? 1.f : 1.f / HCF_CONVERSION);
	// ----------

	// 6/23/2016 skc : Apparently this is needed by the scroll box
	g_GROUP_ID = nm_GROUP_get_group_ID( lpoc );

	strlcpy( GuiVar_GroupName, nm_GROUP_get_name( lpoc ), sizeof(GuiVar_GroupName) );

	// 6/21/2016 skc : Stopped using g_GRUOP_ID
	gid_poc = POC_get_gid_of_group_at_this_index(ppoc_index_0);

	GuiVar_POCBoxIndex = POC_get_box_index_0(gid_poc); // this makes the POC scroll box work

	budget = POC_get_budget( lpoc, 0 );
	GuiVar_BudgetAllocation = sf * budget;

	// 03/11/2016 skc : We saw a mutex deadlock with list_poc and poc_preserves
	// in the call to nm_SYSTEM_get_used.
	// bypass_operation.c #949 is the other side of the deadlock.
	// Thus we give list_poc_recursive_MUTEX back PRIOR to taking poc_preserves_recursive_MUTEX.
	// Since we don't have both mutexes at the same time, we avoid deadlock
	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	// ----------
	// 5/31/2016 skc : A MUTEX collision is also seen between list_poc, list_system and
	// system_preserves, The code has been refactored to not need both at the same time.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = SYSTEM_get_group_with_this_GID( gid_sys );
	num_used = nm_SYSTEM_get_used( lsystem );

	// 6/1/2016 skc : Show the associated mainline name
	strlcpy( GuiVar_BudgetMainlineName, nm_GROUP_get_name( lsystem ), sizeof(GuiVar_BudgetMainlineName) );

	// Show values for the system this POC is attached to
	SYSTEM_get_budget_details( lsystem, &bds );
	GuiVar_BudgetAllocationSYS = sf * nm_SYSTEM_get_budget( lsystem, 0 );

	// 8/23/2016 skc : Don't use the reduction limits in Alert Only mode. Use the
	// reduction limits when predicting volume in Automatic mode.
	BOOL_32 use_limits = (bds.mode == IRRIGATION_SYSTEM_BUDGET_MODE_ALERT_ONLY) ? false : true;

	// 8/24/2016 skc : Make sure budget calcs are initialized.
	BUDGET_calculate_ratios( &today, false );
	
	predicted = nm_BUDGET_predicted_volume( gid_sys, &bds, &today, today.date_time, use_limits );

	// 5/31/2016 skc : Let's give the mutex back as soon as possible
	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	// ----------

	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );
	UNS_32 index_poc_preserves;
	POC_PRESERVES_get_poc_preserve_for_this_poc_gid( gid_poc, &index_poc_preserves );
	usedpoc = nm_BUDGET_get_used( index_poc_preserves );
	GuiVar_BudgetUsed = sf * usedpoc;
	GuiVar_BudgetUsedSYS = sf * num_used;

		// 8/23/2016 skc : Get each POCs percentage of system usage
	memset( Rpoc, 0, sizeof(Rpoc) );
	nm_BUDGET_calc_rpoc( gid_sys, Rpoc );
	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

	// 8/23/2016 skc : Do some budget calcs
	ratio = (budget-usedpoc) / (Rpoc[ppoc_index_0] * predicted);
//debug_printf("ratio: %f %d %f %f\n", ratio, budget, usedpoc, predicted);

	reduction = 100.f * (1.f - ratio);
	if( reduction > 100 ) // limit value to 100% reduction
	{
		reduction = 100;
	}

	// 8/23/2016 skc : This same code exists in BUDGET_handle_alerts_at_midnight()
	reduction_flag = true; // assume reduction limits are not in effect
	num_groups = STATION_GROUP_get_num_groups_in_use();
	for( i = 0; i < num_groups; i++ )
	{
		ptr_group = STATION_GROUP_get_group_at_this_index( i );
		if( STATION_GROUP_get_GID_irrigation_system(ptr_group) == gid_sys )
		{
			// Test if the reduction limit applies
			UNS_32 limit = STATION_GROUP_get_budget_reduction_limit( ptr_group );
			if( (reduction > limit) && (limit < 100) )
			{
				reduction_flag = false; // the needed reduction is more than limit
				break; // we only need one hit to be done
			}
		}
	}


	// 6/24/2016 skc : Make consistent with the other budget screens. Predicted volume, on the
	// screen, is the SUM of used plus estimated to end of period.
	GuiVar_BudgetEst = sf * (predicted + num_used);

	// 5/31/2016 skc : We're done with MUTEXEs in this function.
	// ----------

	strlcpy( GuiVar_BudgetPeriod_0, GetDateStr(str, sizeof(str), bds.meter_read_date[0], DATESTR_show_short_year, 0), 16 );
	strlcpy( GuiVar_BudgetPeriod_1, GetDateStr(str, sizeof(str), bds.meter_read_date[1], DATESTR_show_short_year, 0), 16 );

	// If we are outside a budget period, this gets weird
	GuiVar_BudgetDaysLeftInPeriod = bds.meter_read_date[1] - today.date_time.D + (today.date_time.T <= bds.meter_read_time ? 1 : 0);  

	// 11/1/2018 Ryan : if the set budget is currently zero, do not show a budget alert message,
	// as it would include values found by dividing by zero.
	if( budget == 0 )
	{
		snprintf( str, sizeof(str), "" );
	}
	else
	{
	
		// Take one of two paths, alert only or automatic
		if( bds.mode == IRRIGATION_SYSTEM_BUDGET_MODE_ALERT_ONLY )
		{
			if (ratio >= 1)
			{
				percent = 100 - (100 * ((Rpoc[ppoc_index_0] * predicted) + usedpoc) / budget); 
				snprintf( str, sizeof(str), "Alert only\nPOC expected to be %d%% UNDER BUDGET at end of period", percent );
			}
			else if( ratio >= 0 )
			{
				// predict the amount we will go over budget
				percent = 100 * ((Rpoc[ppoc_index_0] * predicted) + usedpoc - budget) / budget;
				snprintf( str, sizeof(str), "Alert only\nPOC expected to be %d%% OVER BUDGET at end of period", percent);
			}
			else // already over budget
			{
				// predict the amount we will go over budget
				percent = 100 * ((Rpoc[ppoc_index_0] * predicted) + usedpoc - budget) / budget;
				over_budget =  100 * (usedpoc - budget) / budget;
				snprintf( str, sizeof(str), "Alert only\nPOC NOW %d%% OVER BUDGET. Expected to be %d%% over budget at end of period", over_budget, percent );  
			}
		}
		else if( bds.mode == IRRIGATION_SYSTEM_BUDGET_MODE_AUTOMATIC )// running automatic budget mode
		{
			if( ratio >= 1 ) // plenty of budget available
			{
				percent = 100 - (100 * ((Rpoc[ppoc_index_0] * predicted) + usedpoc) / budget); 
				snprintf( str, sizeof(str), "POC expected to be %d%% UNDER BUDGET at end of period", percent );
			}
			else if( ratio >= 0 ) // reduction limits in effect
			{
				// the budget reduction limits are all more than the required reduction, we will use entire
				// budget.
				if( (reduction_flag == true) )
				{
					snprintf( str, sizeof(str), "POC expected to be AT BUDGET at end of period" );
				}
				else // reduction limits in effect
				{
					// predict the amount we will go over budget
					percent = 100 * ((Rpoc[ppoc_index_0] * predicted) + usedpoc - budget) / budget;
					snprintf( str, sizeof(str), "POC expected to be %d%% OVER BUDGET at end of period", percent);
				}
			}
			else // we are already over budget
			{
				// predict the amount we will go over budget
				percent = 100 * ((Rpoc[ppoc_index_0] * predicted) + usedpoc - budget) / budget;
				over_budget =  100 * (usedpoc - budget) / budget;
				snprintf( str, sizeof(str), "POC NOW %d%% OVER BUDGET. Expected to be %d%% over budget at end of period", over_budget, percent );  
			}
		}
		else
		{
			Alert_Message( "unknown budget mode" );
		}
	}

	strlcpy( GuiVar_BudgetReportText, str, sizeof(GuiVar_BudgetReportText) ); 

	// ----------
}

/* ---------------------------------------------------------- */
/*
 * Draw the Budget Report screen
*/
extern void FDTO_BUDGET_REPORT_draw_report( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lpocs_in_use;
	UNS_32	lactive_line;
	INT_16 lcursor_to_select = 0;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpocs_in_use = POC_populate_pointers_of_POCs_for_display( true ); 

	if( pcomplete_redraw == (true) )
	{
		lcursor_to_select = GuiLib_NO_CURSOR;
		g_POC_top_line = 0;
		g_POC_current_list_item_index = 0;
		g_GROUP_list_item_index = POC_get_index_using_ptr_to_poc_struct( POC_get_ptr_to_physically_available_poc(g_POC_current_list_item_index) );
		BUDGET_REPORT_copy_group_into_guivars( g_GROUP_list_item_index );
	}
	else
	{
		lcursor_to_select = (INT_32)GuiLib_ActiveCursorFieldNo;
		g_POC_top_line = (UNS_32)GuiLib_ScrollBox_GetTopLine( 0 );
	}

	GuiLib_ShowScreen( GuiStruct_rptBudgets_0, lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_ScrollBox_Close( 0 );

	lactive_line = POC_get_menu_index_for_displayed_poc();
	GuiLib_ScrollBox_Init_Custom_SetTopLine( 0, &POC_load_poc_name_into_guivar, (INT_16)lpocs_in_use, (INT_16)lactive_line, (INT_16)g_POC_top_line );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void BUDGET_REPORT_process_menu( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
    switch( pkey_event.keycode )
    {
        case KEY_SELECT:
        case KEY_NEXT:
        case KEY_C_RIGHT:
        case KEY_C_DOWN:
            SCROLL_BOX_up_or_down( 0, KEY_C_DOWN );
            g_POC_current_list_item_index = GuiLib_ScrollBox_GetActiveLine( 0, 0 );
            g_GROUP_list_item_index = POC_get_index_using_ptr_to_poc_struct( POC_get_ptr_to_physically_available_poc( g_POC_current_list_item_index ) );
            BUDGET_REPORT_copy_group_into_guivars( g_GROUP_list_item_index );
            Redraw_Screen( false );
            break;

        case KEY_PREV:
        case KEY_C_LEFT:
        case KEY_C_UP:
            SCROLL_BOX_up_or_down( 0, KEY_C_UP );
            g_POC_current_list_item_index = GuiLib_ScrollBox_GetActiveLine( 0, 0 );
            g_GROUP_list_item_index = POC_get_index_using_ptr_to_poc_struct( POC_get_ptr_to_physically_available_poc( g_POC_current_list_item_index ) );
            BUDGET_REPORT_copy_group_into_guivars( g_GROUP_list_item_index );
            Redraw_Screen( false );
            break;

        default:
            if( pkey_event.keycode == KEY_BACK )
            {
                GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;
            }

            KEY_process_global_keys( pkey_event );
    }
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


