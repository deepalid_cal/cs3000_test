/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_priorities.h"

#include	"app_startup.h"

#include	"combobox.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"group_base_file.h"

#include	"station_groups.h"

#include	"m_main.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_PRIORITY_LEVEL_0		(0)
#define	CP_PRIORITY_LEVEL_1		(1)
#define	CP_PRIORITY_LEVEL_2		(2)
#define	CP_PRIORITY_LEVEL_3		(3)
#define	CP_PRIORITY_LEVEL_4		(4)
#define	CP_PRIORITY_LEVEL_5		(5)
#define	CP_PRIORITY_LEVEL_6		(6)
#define	CP_PRIORITY_LEVEL_7		(7)
#define	CP_PRIORITY_LEVEL_8		(8)
#define	CP_PRIORITY_LEVEL_9		(9)

#define	PRIORITY_COMBO_BOX_X_OFFSET		(246)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32 *g_PRIORITY_combo_box_guivar;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Callback function for the GuiLib_ScrollBox_Init_Custom_SetTopLine routine
 * used to draw the priority dropdown.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the users presses SELECT to change the priority setting.
 * 
 * @param pindex_0 The index of the line to populate - 0-based.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_PRIORITY_populate_dropdown( const INT_16 pindex_0 )
{
	GuiVar_ComboBoxItemIndex = (UNS_32)(pindex_0 + 1);
}

/* ---------------------------------------------------------- */
/**
 * Draws the priority dropdown window atop the main window.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the users presses SELECT to change the priority setting.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_PRIORITY_show_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxPriorities_0, &FDTO_PRIORITY_populate_dropdown, PRIORITY_HIGH, (*g_PRIORITY_combo_box_guivar - 1) );
}

/* ---------------------------------------------------------- */
extern void FDTO_PRIORITY_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		PRIORITY_copy_group_into_guivars();

		lcursor_to_select = 0;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrPriorities_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Processes the key event from the irrigation priority screen.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 5/16/2011 Adrianusv
 *
 * @revisions
 *    5/16/2011 Initial release
 */
extern void PRIORITY_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_cbxPriorities_0:
			COMBO_BOX_key_press( pkey_event.keycode, g_PRIORITY_combo_box_guivar );
			// 6/2/2015 ajv : Since the combobox is 0-based, but the priority is 1-based, make sure the
			// value is incremented by 1.
			*g_PRIORITY_combo_box_guivar += 1;
			Refresh_Screen();
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_PRIORITY_LEVEL_0:
							g_PRIORITY_combo_box_guivar = &GuiVar_GroupSettingA_0;
							break;

						case CP_PRIORITY_LEVEL_1:
							g_PRIORITY_combo_box_guivar = &GuiVar_GroupSettingA_1;
							break;

						case CP_PRIORITY_LEVEL_2:
							g_PRIORITY_combo_box_guivar = &GuiVar_GroupSettingA_2;
							break;

						case CP_PRIORITY_LEVEL_3:
							g_PRIORITY_combo_box_guivar = &GuiVar_GroupSettingA_3;
							break;

						case CP_PRIORITY_LEVEL_4:
							g_PRIORITY_combo_box_guivar = &GuiVar_GroupSettingA_4;
							break;

						case CP_PRIORITY_LEVEL_5:
							g_PRIORITY_combo_box_guivar = &GuiVar_GroupSettingA_5;
							break;

						case CP_PRIORITY_LEVEL_6:
							g_PRIORITY_combo_box_guivar = &GuiVar_GroupSettingA_6;
							break;

						case CP_PRIORITY_LEVEL_7:
							g_PRIORITY_combo_box_guivar = &GuiVar_GroupSettingA_7;
							break;

						case CP_PRIORITY_LEVEL_8:
							g_PRIORITY_combo_box_guivar = &GuiVar_GroupSettingA_8;
							break;

						case CP_PRIORITY_LEVEL_9:
							g_PRIORITY_combo_box_guivar = &GuiVar_GroupSettingA_9;
							break;
					}

					if( g_PRIORITY_combo_box_guivar != NULL )
					{
						good_key_beep();

						if( GuiLib_ActiveCursorFieldNo == CP_PRIORITY_LEVEL_0 )
						{
							GuiVar_ComboBox_Y1 = 0;
						}
						else
						{
							GuiVar_ComboBox_Y1 = (UNS_32)(GuiLib_ActiveCursorFieldNo * Pos_Y_GroupSetting);
						}

						lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
						lde._04_func_ptr = (void*)&FDTO_PRIORITY_show_dropdown;
						Display_Post_Command( &lde );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_PRIORITY_LEVEL_0:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_0, PRIORITY_LOW, PRIORITY_HIGH, 1, (false) );
							break;

						case CP_PRIORITY_LEVEL_1:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_1, PRIORITY_LOW, PRIORITY_HIGH, 1, (false) );
							break;

						case CP_PRIORITY_LEVEL_2:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_2, PRIORITY_LOW, PRIORITY_HIGH, 1, (false) );
							break;

						case CP_PRIORITY_LEVEL_3:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_3, PRIORITY_LOW, PRIORITY_HIGH, 1, (false) );
							break;

						case CP_PRIORITY_LEVEL_4:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_4, PRIORITY_LOW, PRIORITY_HIGH, 1, (false) );
							break;

						case CP_PRIORITY_LEVEL_5:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_5, PRIORITY_LOW, PRIORITY_HIGH, 1, (false) );
							break;

						case CP_PRIORITY_LEVEL_6:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_6, PRIORITY_LOW, PRIORITY_HIGH, 1, (false) );
							break;

						case CP_PRIORITY_LEVEL_7:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_7, PRIORITY_LOW, PRIORITY_HIGH, 1, (false) );
							break;

						case CP_PRIORITY_LEVEL_8:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_8, PRIORITY_LOW, PRIORITY_HIGH, 1, (false) );
							break;

						case CP_PRIORITY_LEVEL_9:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_9, PRIORITY_LOW, PRIORITY_HIGH, 1, (false) );
							break;

						default:
							bad_key_beep();
					}
					Redraw_Screen( (false) );
					break;

				case KEY_PREV:
				case KEY_C_UP:
				case KEY_C_LEFT:
					CURSOR_Up( (true) );
					break;

				case KEY_NEXT:
				case KEY_C_DOWN:
				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_BACK:
					GuiVar_MenuScreenToShow = CP_MAIN_MENU_SCHEDULED_IRRIGATION;
					PRIORITY_extract_and_store_changes_from_GuiVars();
					// No need to break here to allow this to fall into the default
					// condition.

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

