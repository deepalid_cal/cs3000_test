/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_E_LR_PROGRAMMING_LRS_H
#define _INC_E_LR_PROGRAMMING_LRS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define LR_MODE_MASTER		                    (2)
#define LR_MODE_SLAVE							(3)
#define LR_MODE_REPEATER		                (7)

#define LR_GROUP_MIN							(1)
#define LR_GROUP_MAX							(6)

#define LR_XMIT_RATE_MIN						(0)
#define LR_XMIT_RATE_MAX						(9)

#define LR_REPEATER_IN_USE_MIN					(false)
#define LR_REPEATER_IN_USE_MAX					(true)

#define	LR_SLAVE_LINKS_TO_REPEATER_MIN			(false)
#define	LR_SLAVE_LINKS_TO_REPEATER_MAX			(true)

#define	LR_FREQUENCY_MIN						(435)
#define	LR_FREQUENCY_MAX						(470)

#define	LR_FREQUENCY_DECIMAL_MIN				(0)
#define	LR_FREQUENCY_DECIMAL_MAX				(9875)

#define LR_XMT_POWER_MIN						(0)
#define LR_XMT_POWER_MAX						(10)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FDTO_LR_PROGRAMMING_lrs_draw_screen( const BOOL_32 pcomplete_redraw, const UNS_32 port );

extern void LR_PROGRAMMING_lrs_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

