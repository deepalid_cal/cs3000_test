/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"e_walk_thru.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"change.h"

#include	"configuration_controller.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"e_station_selection_grid.h"

#include	"group_base_file.h"

#include	"e_keyboard.h"

#include	"scrollbox.h"

#include	"speaker.h"

#include	"walk_thru.h"

#include	"flowsense.h"

#include	"cs3000_comm_server_common.h"

#include	"configuration_network.h"

#include	"combobox.h"

#include	"comm_mngr.h"

#include	"irri_comm.h"

#include	"r_irri_details.h"



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_WALK_THRU_NAME						(0)
#define	CP_WALK_THRU_DELAY_BEFORE_START			(1)
#define	CP_WALK_THRU_STATION_RUN_TIME			(2)
#define CP_WALK_THRU_CONTROLLER_NAME			(3)
#define	CP_WALK_THRU_START_CANCEL				(4)
#define	CP_WALK_THRU_STATION_LIST				(5)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static BOOL_32		wt_editing_group;

static UNS_32		wt_index_of_order_when_dialog_displayed;

static INT_16		wt_visible_number_in_order_sequence;

// ----------

// 3/21/2018 rmd : Variables associated with the delay countdown. The wt_ just stands for
// walk_thru to give them some identity.
static xTimerHandle	wt_delay_timer;

static INT_32		wt_delay_copy;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void WALK_THRU_return_to_menu( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void walk_thru_delay_timer_callback ( xTimerHandle pxTimer )
{
	// 3/21/2018 rmd : Remember this executes in the context of the very high priority RTOS
	// timer task, we do not typically want to perform much detailed processing at the timer
	// task priority, instead deferring such work to a lower priority task.

	// ----------
	
	KEY_TO_PROCESS_QUEUE_STRUCT		ktpqs;
					
	ktpqs.keycode = WALK_THRU_KEY_countdown_timer_expired;

	ktpqs.repeats = 0;

	xQueueSendToBack( Key_To_Process_Queue, &ktpqs, portMAX_DELAY );
}

/* ---------------------------------------------------------- */
/**
 * Finds the number of visible member the order sequence should have and returns the value  
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the entering, exiting, and drawing the
 * order scrollbox.
 *
 * @param (none)
 *
 * @author 3/14/2018 Ryan
 *
 * @revisions (none)
 */
static INT_16 WALK_THRU_get_visible_number_in_order_sequence()
{	
	BOOL_32				lempty_sequence_check;
	
	INT_16				lorder_sequence_count;

	INT_16				lnumber_visable_in_sequence;
	
	INT_16				lsequence_after_count;
	
	INT_16				lcount_empty_sequence;
	
	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );
	
	lcount_empty_sequence = 0;
	
	// 3/21/2018 Ryan : Go through each member of the order sequence.
	for( lorder_sequence_count = 0; lorder_sequence_count < MAX_NUMBER_OF_WALK_THRU_STATIONS; lorder_sequence_count++ )
	{
		// 3/21/2018 Ryan : If the sequence member has a blank station assigned to it, the remaining
		// part of the sequence is checked for non-blank station values.
		if( WALK_THRU_station_array_for_gui[ lorder_sequence_count ] == WALK_THRU_SEQUENCE_SLOT_no_station_set )
		{
			lempty_sequence_check = (true);
			for( lsequence_after_count = lorder_sequence_count ; lsequence_after_count < MAX_NUMBER_OF_WALK_THRU_STATIONS; lsequence_after_count++ )
			{
				if( WALK_THRU_station_array_for_gui[ lsequence_after_count ] != WALK_THRU_SEQUENCE_SLOT_no_station_set )
				{
					lempty_sequence_check = (false);
				}
			}
			
			// 3/21/2018 Ryan : Used to track end section of sequence with only blank value station
			// values. Only added to if those conditions are found
			if( lempty_sequence_check )
			{
				lcount_empty_sequence++;
			}
		}
	}

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );
	
	// 3/21/2018 Ryan : Subtraction of the empty portion of the sequence gives us the visible
	// sequence. Add one so the blank slot after is also visable.
	lnumber_visable_in_sequence = MAX_NUMBER_OF_WALK_THRU_STATIONS - lcount_empty_sequence + 1;
	
	// 4/2/2018 Ryan : Don't want to add a blank when already at maximum station capacity, so
	// here is a check to prevent that.
	if( lnumber_visable_in_sequence > MAX_NUMBER_OF_WALK_THRU_STATIONS )
	{
		lnumber_visable_in_sequence = MAX_NUMBER_OF_WALK_THRU_STATIONS;
	}

	return( lnumber_visable_in_sequence );
}

/* ---------------------------------------------------------- */
/**
 * Populates a line in the order scrollbox with order number, station string, and station
 * description.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of drawing the order scrollbox.
 *
 * @param pline_index_0 Relates to what line is being populated.
 *
 * @author 3/14/2018 Ryan
 *
 * @revisions (none)
 */
static void WALK_THRU_populate_order_scrollbox( const INT_16 pline_index_0 )
{
	
	WALK_THRU_STRUCT	*lwalkthru;
	
	STATION_STRUCT		*lstation;
	
	INT_32				lstation_num;
	
	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );
	
	lwalkthru = WALK_THRU_get_group_at_this_index( g_GROUP_list_item_index );
	lstation_num = WALK_THRU_station_array_for_gui[ pline_index_0 ];
	GuiVar_WalkThruOrder = pline_index_0 + 1;

	// 3/26/2018 Ryan : If the station is blank, there should be a '--' and no station
	// description.
	if( lstation_num == WALK_THRU_SEQUENCE_SLOT_no_station_set )
	{	
		snprintf( GuiVar_WalkThruStation_Str, sizeof(GuiVar_WalkThruStation_Str), "--" );
		snprintf( GuiVar_WalkThruStationDesc, sizeof(GuiVar_WalkThruStationDesc), "" );
	}
	else
	{	
		lstation = nm_STATION_get_pointer_to_station( GuiVar_WalkThruBoxIndex, (UNS_32)lstation_num );
		STATION_get_station_number_string( GuiVar_WalkThruBoxIndex, (UNS_32)lstation_num, (char*)&GuiVar_WalkThruStation_Str, sizeof(GuiVar_WalkThruStation_Str) );
		snprintf( GuiVar_WalkThruStationDesc, sizeof(GuiVar_WalkThruStationDesc), "%s", nm_STATION_get_description( lstation ) );
	}
	
	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void WALK_THRU_draw_order_scrollbox()
{
	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );	

	wt_visible_number_in_order_sequence = WALK_THRU_get_visible_number_in_order_sequence();

	GuiLib_ScrollBox_Close( 1 );
	GuiLib_ScrollBox_Init( 1, &WALK_THRU_populate_order_scrollbox, wt_visible_number_in_order_sequence, wt_index_of_order_when_dialog_displayed );

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
static void WALK_THRU_enter_order_scrollbox( const BOOL_32 pmove_to_top_line )
{
	GuiLib_ScrollBox_Close( 1 );

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	wt_visible_number_in_order_sequence = WALK_THRU_get_visible_number_in_order_sequence();

	if( pmove_to_top_line == (true) )
	{
		GuiLib_ScrollBox_Init( 1, &WALK_THRU_populate_order_scrollbox, wt_visible_number_in_order_sequence, 0 );
	}
	else
	{
		GuiLib_ScrollBox_Init_Custom_SetTopLine( 1, &WALK_THRU_populate_order_scrollbox, wt_visible_number_in_order_sequence, wt_visible_number_in_order_sequence, wt_visible_number_in_order_sequence );
	}

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );

	GuiLib_Cursor_Select( CP_WALK_THRU_STATION_LIST );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
static void WALK_THRU_leave_order_scrollbox( void )
{
	GuiLib_ScrollBox_Close( 1 );

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	wt_visible_number_in_order_sequence = WALK_THRU_get_visible_number_in_order_sequence();

	GuiLib_ScrollBox_Init( 1, &WALK_THRU_populate_order_scrollbox, wt_visible_number_in_order_sequence, GuiLib_NO_CURSOR );

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );

	FDTO_Cursor_Select( CP_WALK_THRU_START_CANCEL, (true) );
}

/* ---------------------------------------------------------- */
static void WALK_THRU_update_controller_names( const UNS_32 pindex_0 )
{
	
	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	NETWORK_CONFIG_get_controller_name_str_for_ui( pindex_0, (char *)&GuiVar_WalkThruControllerName );
	
	GuiVar_WalkThruBoxIndex = pindex_0;
	
	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );
}

static void WALK_THRU_process_controller_names( const UNS_32 pkeycode )
{

	UNS_32	lindex;

	UNS_32	llist_count;

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	llist_count = (COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members() - 1);

	lindex = GuiVar_WalkThruBoxIndex;

	process_uns32( pkeycode, &lindex, 0, llist_count, 1, (true) );

	WALK_THRU_update_controller_names( lindex );

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );

	Redraw_Screen( (false) );
}

static void WALK_THRU_show_controller_name_dropdown( void )
{
	UNS_32	lgroup_index_0;

	lgroup_index_0 = FLOWSENSE_get_controller_index();

	FDTO_COMBOBOX_show( GuiStruct_cbxWalkThruController_0 , &WALK_THRU_load_controller_name_into_scroll_box_guivar, COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members(), lgroup_index_0 );
}

static void WALK_THRU_close_controller_name_dropdown( void )
{
	WALK_THRU_update_controller_names( (UNS_32)GuiLib_ScrollBox_GetActiveLine(0, 0) );

	FDTO_COMBOBOX_hide();
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Creates a new group.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user selects "Add New" or assigns a station to a new group.
 * 
 * @param passign_current_station_to_new_group True if the currently selected
 * station should be assigned to the newly created group; otherwise, false. This
 * is set true when the user removes a station from a group and assigns it to a
 * new group.
 * 
 * @param pshow_new_group True if the new group should be displayed, otherwise,
 * false. This is set true when the user selects "Add New", but remains false
 * when removing a station from a group and assigning it to a new group.
 *
 * @author 3/14/2018 Ryan
 *
 * @revisions (none)
 */
static void WALK_THRU_add_new_group( void )
{
	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	nm_GROUP_create_new_group_from_UI( (void*)&nm_WAlK_THRU_create_new_group, (void*)&WALK_THRU_copy_group_into_guivars );
		
	// 3/27/2018 Ryan : If the controller was part of a chain, but is no longer, this check
	// makes sure the new walk-thru is set to the right controller.
	if( ( FLOWSENSE_we_are_poafs() == (false) ) && ( GuiVar_WalkThruBoxIndex != FLOWSENSE_get_controller_letter() - 'A' ) )
	{
		GuiVar_WalkThruBoxIndex = FLOWSENSE_get_controller_letter() - 'A';
	}

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Process when station is changed in the order scrollbox with the plus or minus keys.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the WALK_THRU_process_key_event function when the plus or
 * minus key is pressed while in the order scrollbox.
 *
 * @param pkeycode can be either the plus or minus keycode.
 *
 * @author 3/14/2018 Ryan
 *
 * @revisions (none)
 */
static void WALK_THRU_process_station_plus_minus( const UNS_32 pkeycode )
{
	WALK_THRU_STRUCT		*lwalkthru;
	
	STATION_STRUCT			*lstation;
	
	STATION_STRUCT			*lstation_copy;
	
	INT_32					lstation_num;
		
	INT_32					lstation_num_next;
		
	UNS_32					lstation_box_index;
	
	UNS_32					lstation_number_1;
	
	UNS_32					lstation_first;
	
	UNS_32					lstation_last;
	
	BOOL_32					no_stations_on_controller;
	
	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );
	
	lwalkthru = WALK_THRU_get_group_at_this_index( g_GROUP_list_item_index );
	lstation_num = WALK_THRU_station_array_for_gui[ (UNS_32)GuiLib_ScrollBox_GetActiveLine( 1, 0 ) ];

	// 3/23/2018 Ryan : Set the last station available in the slot sequence. We accomplish this
	// by going to the start of the station info list, using the
	// STATION_get_prev_available_station function to get the last station in the list
	// (nm_ListGetLast does not take 2 wire into account by STATION_get_prev_available_station
	// does) and decrimenting until the box index matches the one assigned to GuiVar_WalkThruBoxIndex
	lstation_box_index = 0;
	lstation = nm_ListGetFirst( &station_info_list_hdr );
	lstation_number_1 = nm_STATION_get_station_number_0( lstation ) + 1;
	lstation = STATION_get_prev_available_station( &lstation_box_index, &lstation_number_1 );
	no_stations_on_controller = (false);
	
	// 3/23/2018 Ryan : The function STATION_get_prev_available_station gives
	// the last station of the last controller on the chain. If that occurs then we need to fix
	// that.
	if( lstation_box_index == GuiVar_WalkThruBoxIndex )
	{
		lstation_last = nm_STATION_get_station_number_0( lstation );
	}
	else
	{
		// 3/26/2018 Ryan : Prevent while loop for looping forever.
		lstation_copy = lstation;

		while(( lstation_box_index != GuiVar_WalkThruBoxIndex ) && ( no_stations_on_controller == (false) ) )
		{
			lstation = STATION_get_prev_available_station( &lstation_box_index, &lstation_number_1 );
			if( lstation == lstation_copy )
			{
				// 3/26/2018 Ryan : If the loop got this far, then the controller doesn't have any available
				// stations, or FLOWSENSE was turned off at this controller and the currently selected
				// walk-thru must have been on another controller on the chain.
				no_stations_on_controller = (true);
			}
		}
		
		lstation_last = nm_STATION_get_station_number_0( lstation );
	}

	lstation_number_1 = lstation_last + 1;
	lstation = STATION_get_next_available_station( &lstation_box_index, &lstation_number_1 );
	
	// 3/23/2018 Ryan : The function STATION_get_next_available_station gives the first station
	// of the next controller on the chain. If that occurs then we need to fix that.
	if( lstation_box_index == GuiVar_WalkThruBoxIndex )
	{
		lstation_first = nm_STATION_get_station_number_0( lstation );
	}
	else
	{
		while(( lstation_box_index != GuiVar_WalkThruBoxIndex ) && ( no_stations_on_controller == (false) ) )
		{
			lstation = STATION_get_next_available_station( &lstation_box_index, &lstation_number_1 );
		}
		
		lstation_first = nm_STATION_get_station_number_0( lstation );
	}
	
	// 4/18/2018 Ryan : No editing allowed while chain is down.
	if( !COMM_MNGR_network_is_available_for_normal_use() )
	{
		bad_key_beep();
		DIALOG_draw_ok_dialog( GuiStruct_dlgWalkThruChainDown_0 );
	}
	else if( no_stations_on_controller )
	{
		// 4/18/2018 Ryan : Don't let the user edit the order when there are no stations available
		// on the controller.
		bad_key_beep();
		DIALOG_draw_ok_dialog( GuiStruct_dlgWalkThruNoStationsOnController_0 );
	}
	else
	{
		good_key_beep();

		// 3/23/2018 Ryan : Processing plus key.
		if( pkeycode == KEY_PLUS )
		{
			if( lstation_num != WALK_THRU_SEQUENCE_SLOT_no_station_set )
			{
				// 3/23/2018 Ryan : When the slot not empty, use STATION_get_next_available_station on a
				// base 1 version of the station number to find the next avaliable station.
				lstation_number_1 = lstation_num + 1;
				lstation = STATION_get_next_available_station( &lstation_box_index, &lstation_number_1 );
				lstation_num_next = nm_STATION_get_station_number_0( lstation );
			
				// 3/29/2018 Ryan : If next station is less that the current station or if the on a
				// different controller, then the slot should be set to blank.
				if( ( lstation_num_next < lstation_num ) || ( lstation_box_index != GuiVar_WalkThruBoxIndex ) )
				{
					lstation_num_next = WALK_THRU_SEQUENCE_SLOT_no_station_set;
				}
			}
			else
			{
				// 3/23/2018 Ryan : If the the station is blank, set the slot to the first available
				// station.
				lstation_num_next = lstation_first;
			}
											
			// 3/23/2018 Ryan : Check that the station chosen is not already set to another slot in the
			// sequence.
			while ( WALK_THRU_check_for_station_in_walk_thru( lwalkthru, lstation_num_next ) )
			{
				lstation_number_1 = lstation_num_next + 1;
				lstation = STATION_get_next_available_station( &lstation_box_index, &lstation_number_1 );
				lstation_num_next = nm_STATION_get_station_number_0( lstation );
			
				// 3/23/2018 Ryan : If station number is equal to the first availabe station or a station on
				// a different controller, then it has to be set to a blank.
				if( ( lstation_num_next == lstation_first ) || ( lstation_box_index != GuiVar_WalkThruBoxIndex ) )
				{
					lstation_num_next = WALK_THRU_SEQUENCE_SLOT_no_station_set;
				}
						
			} // end of while loop checking for duplicate stations
				
		} // end of processing plus button.
		else
		{
			// 3/23/2018 Ryan : Processing minus key.
			if( lstation_num == lstation_first )
			{
				// 3/23/2018 Ryan : Set the slot to empty if currently set to the first
				// available station.
				lstation_num_next = WALK_THRU_SEQUENCE_SLOT_no_station_set;
			}
			else if( lstation_num == WALK_THRU_SEQUENCE_SLOT_no_station_set )
			{
				// 3/23/2018 Ryan : If slot is empty, set to last available station.
				lstation_num_next = lstation_last;
			}
			else
			{
				// 3/23/2018 Ryan : When the slot not empty or the first available station, use
				// STATION_get_prev_available_station with a base 1 version of the station number to find
				// the prev avaliable station.
				lstation_number_1 = lstation_num + 1;
				lstation = STATION_get_prev_available_station ( &lstation_box_index, &lstation_number_1 );
				lstation_num_next = nm_STATION_get_station_number_0( lstation );
			}
				
			// 3/23/2018 Ryan : Check that the station chosen is not already set to another slot in the
			// sequence.
			while ( WALK_THRU_check_for_station_in_walk_thru( lwalkthru, lstation_num_next ) )
			{
				lstation_number_1 = lstation_num_next + 1;
				lstation = STATION_get_prev_available_station( &lstation_box_index, &lstation_number_1 );
				lstation_num_next = nm_STATION_get_station_number_0( lstation );
				
				// 3/23/2018 Ryan : If station number is equal to the last availabe station or a station on
				// another controller, then it has to be set to a blank.
				if( ( lstation_num_next >= lstation_last ) || ( lstation_box_index != GuiVar_WalkThruBoxIndex ) )
				{
					lstation_num_next = WALK_THRU_SEQUENCE_SLOT_no_station_set;
				}
			} // end of while loop checking for duplicate stations.
				
		} // end of processing minus button
			
		// 3/23/2018 Ryan : Set gui array with new slot value.
		WALK_THRU_station_array_for_gui[ (UNS_32)GuiLib_ScrollBox_GetActiveLine( 1, 0 ) ] = lstation_num_next;

	} // end of when controller has stations on it.

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Process when the start button is pressed.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the WALK_THRU_process_key_event function when the start
 * button is pressed.
 *
 * @param (none)
 *
 * @author 3/14/2018 Ryan
 *
 * @revisions (none)
 */
static void WALK_THRU_process_start_key()
{
	// 3/21/2018 Ryan : Makes sure cursor doesn't appear in scrollbox if screen is redrawn in
	// this function.
	wt_index_of_order_when_dialog_displayed = GuiLib_NO_CURSOR;

	// 3/21/2018 Ryan : If there is only one visible member of order sequence, it means that the
	// entire list must be empty.
	if( wt_visible_number_in_order_sequence == 1 )
	{
		bad_key_beep();
		
		DIALOG_draw_ok_dialog( GuiStruct_dlgWalkThruNoStations_0 );
	}
	else if( !COMM_MNGR_network_is_available_for_normal_use() )
	{
		bad_key_beep();

		DIALOG_draw_ok_dialog( GuiStruct_dlgChainNotReady_0 );
	}
	else if( (GuiVar_StationInfoBoxIndex == FLOWSENSE_get_controller_index()) && (tpmicro_comm.fuse_blown == (true)) )
	{
		// 3/26/2018 Ryan : This only triggers if the fuse is blown on the controller starting the
		// walk-thru.
		bad_key_beep();

		DIALOG_draw_ok_dialog( GuiStruct_dlgFuseBlown_0 );
	}
	else if( IRRI_COMM_if_any_2W_cable_is_over_heated() )
	{
		// 11/11/2014 rmd : Because you could be watering a prog have decided if any two_wire cable
		// is reporting problem don't allow any irrigation. Regardless of if it needs that cable or
		// not.
		// 
		// 11/11/2014 rmd : This really isn't as good as it could be. Could also check if the
		// station is decoder based.
		bad_key_beep();

		DIALOG_draw_ok_dialog( GuiStruct_dlgTwoWireCableOverheated_0 );
	}
	else if( irri_comm.walk_thru_need_to_send_gid_to_master )
	{
		// 3/23/2018 Ryan : Will not start if previous start of a walk-thru has not completed.
		bad_key_beep();
	}
	else
	{
		// 3/21/2018 rmd : If the timer doesn't exist, create it. Once created, it will persist for
		// the duration of the application.
		if( !wt_delay_timer )
		{
			wt_delay_timer = xTimerCreate( NULL, 1, (false), NULL, walk_thru_delay_timer_callback );
		}

		// ----------
		
		good_key_beep();
		
		GuiVar_WalkThruCountingDown = 1;

		// 3/20/2018 Ryan : Refresh screen to make button flip from start to stop.
		Redraw_Screen( (false) );

		// ----------
		
		// 3/21/2018 rmd : Need this copy to restore the displayed count if the user hits the CANCEL
		// key.
		wt_delay_copy = GuiVar_WalkThruDelay;

		xTimerChangePeriod( wt_delay_timer, MS_to_TICKS(1000), portMAX_DELAY );
	}
}

/* ---------------------------------------------------------- */
/**
 * Processes the key event from the irrigation system screen for a particular
 * system.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 3/26/2018 Ryan
 *
 * @revisions (none)
 *
 */
static void WALK_THRU_process_key_event( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	INT_32					lactive_line;
	
	INT_16					new_visible_number_in_order_sequence;
	
	// ----------
	
	// 3/21/2018 rmd : Handle our special key processing.
	if( pkey_event.keycode == WALK_THRU_KEY_countdown_timer_expired )
	{
		// 3/20/2018 Ryan : If Delay time has been reached, reset delay and begin walk-thru,
		// otherwise decrement the delay on the screen and wait another second.
		if( GuiVar_WalkThruDelay <= 0 )
		{
			// ----------
			
			irri_comm.walk_thru_need_to_send_gid_to_master = (true);

			irri_comm.walk_thru_gid_to_send = g_GROUP_ID;
			
			// ----------
			
			IRRI_DETAILS_jump_to_irrigation_details();
		}
		else
		{
			GuiVar_WalkThruDelay--;

			xTimerChangePeriod( wt_delay_timer, MS_to_TICKS(1000), portMAX_DELAY );
		}
	}
	else
	{
		// 3/21/2018 rmd : Regular key processing.
		switch( GuiLib_CurStructureNdx )
		{
			case GuiStruct_dlgKeyboard_0:
				if( ((pkey_event.keycode == KEY_BACK) || ((pkey_event.keycode == KEY_SELECT) && (GuiLib_ActiveCursorFieldNo == 49 /*CP_QWERTY_KEYBOARD_OK*/))) )
				{
					WALK_THRU_extract_and_store_group_name_from_GuiVars();
				}
	
				KEYBOARD_process_key( pkey_event, &WALK_THRU_draw_menu );
				break;
	
			case GuiStruct_cbxWalkThruController_0:
				if( ( pkey_event.keycode == KEY_SELECT ) || ( pkey_event.keycode == KEY_BACK ) )
				{
					good_key_beep();
	
					lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
					lde._04_func_ptr = WALK_THRU_close_controller_name_dropdown;
					Display_Post_Command( &lde );
				}
				else
				{
					COMBO_BOX_key_press( pkey_event.keycode, NULL );
				}
				break;
	
			default:
				switch( pkey_event.keycode )
				{
					case KEY_SELECT:
						switch( GuiLib_ActiveCursorFieldNo )
						{
							case CP_WALK_THRU_NAME:
								
								// 4/18/2018 Ryan : No editing allowed while chain is down.
								if( !COMM_MNGR_network_is_available_for_normal_use() )
								{
									bad_key_beep();
									DIALOG_draw_ok_dialog( GuiStruct_dlgWalkThruChainDown_0 );
								}
								else
								{
									GROUP_process_show_keyboard( 164, 27 );
								}
								break;
							
							case CP_WALK_THRU_CONTROLLER_NAME:
							
								// 4/18/2018 Ryan : No editing allowed while chain is down.
								if( !COMM_MNGR_network_is_available_for_normal_use() )
								{
									bad_key_beep();
									DIALOG_draw_ok_dialog( GuiStruct_dlgWalkThruChainDown_0 );
								}
								else if( wt_visible_number_in_order_sequence == 1 )
								{
									// 3/22/2018 Ryan : Can only change controller if order sequence is empty.
									good_key_beep();
									lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
									lde._04_func_ptr = WALK_THRU_show_controller_name_dropdown;
									Display_Post_Command( &lde );
								}
								else
								{
									// 4/18/2018 Ryan : Warn user that order sequence is not empty.
									bad_key_beep();
									DIALOG_draw_ok_dialog( GuiStruct_dlgWalkThruNotEmpty_0 );
								}
								break;
								
							case CP_WALK_THRU_START_CANCEL:
								if( GuiVar_WalkThruCountingDown == (true) )
								{
									good_key_beep();
									
									xTimerStop( wt_delay_timer, portMAX_DELAY );

									GuiVar_WalkThruCountingDown = (false);

									GuiVar_WalkThruDelay = wt_delay_copy;

									Redraw_Screen( (false) );
								}
								else
								{
									WALK_THRU_extract_and_store_changes_from_GuiVars();

									WALK_THRU_copy_group_into_guivars( g_GROUP_list_item_index );

									WALK_THRU_process_start_key();
								}
								break;
	
							default:
								bad_key_beep();
						}
						break;
	
					case KEY_MINUS:
					case KEY_PLUS:
						switch( GuiLib_ActiveCursorFieldNo )
						{
							case CP_WALK_THRU_DELAY_BEFORE_START:
							
								// 4/18/2018 Ryan : No editing allowed while the chain is down.
								if( !COMM_MNGR_network_is_available_for_normal_use() )
								{
									bad_key_beep();
									DIALOG_draw_ok_dialog( GuiStruct_dlgWalkThruChainDown_0 );
								}
								else
								{
									process_uns32( pkey_event.keycode, &GuiVar_WalkThruDelay, WALK_THRU_DELAY_BEFORE_START_MIN, WALK_THRU_DELAY_BEFORE_START_MAX, 1, (false) );
									Refresh_Screen();
								}
								break;
								
							case CP_WALK_THRU_STATION_RUN_TIME:
							
								// 4/18/2018 Ryan : No editing allowed while the chain is down.
								if( !COMM_MNGR_network_is_available_for_normal_use() )
								{
									bad_key_beep();
									DIALOG_draw_ok_dialog( GuiStruct_dlgWalkThruChainDown_0 );
								}
								else
								{
									process_uns32( pkey_event.keycode, &GuiVar_WalkThruRunTime, WALK_THRU_STATION_RUN_TIME_MIN, WALK_THRU_STATION_RUN_TIME_MAX, 1, (false) );
									Refresh_Screen();
								}
								break;
								
							case CP_WALK_THRU_CONTROLLER_NAME:
							
								// 4/18/2018 Ryan : No editing allowed while the chain is down.
								if( !COMM_MNGR_network_is_available_for_normal_use() )
								{
									bad_key_beep();
									DIALOG_draw_ok_dialog( GuiStruct_dlgWalkThruChainDown_0 );
								}
								else if( wt_visible_number_in_order_sequence == 1 )
								{
									// 3/22/2018 Ryan : Can not change controllers without an empty order sequence.
									WALK_THRU_process_controller_names( pkey_event.keycode);
									Refresh_Screen();
								}
								else
								{
									// 4/18/2018 Ryan : Warn user that order sequence is not empty.
									bad_key_beep();
									DIALOG_draw_ok_dialog( GuiStruct_dlgWalkThruNotEmpty_0 );
								}
								break;
	
							case CP_WALK_THRU_STATION_LIST:
								WALK_THRU_process_station_plus_minus( pkey_event.keycode);
	
								// 3/16/2018 Ryan : Have to refresh the scrollbox when the visible number of the order
								// sequence changes, so blank slot at the end of the sequence can be added or removed.
								wt_index_of_order_when_dialog_displayed = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 1, 0 );
								new_visible_number_in_order_sequence = WALK_THRU_get_visible_number_in_order_sequence();
								
								if( new_visible_number_in_order_sequence != wt_visible_number_in_order_sequence )
								{ 
									Redraw_Screen( (false) );
								}
								else
								{
									SCROLL_BOX_redraw( 1 );
								}
	
								break;
	
							default:
								bad_key_beep();
						}
						break;
	
					case KEY_NEXT:
					case KEY_PREV:
						switch( GuiLib_ActiveCursorFieldNo )
						{
							case CP_WALK_THRU_START_CANCEL:
								
								// 4/18/2018 Ryan : User can not leave the start/cancel button until the Walk-Thru has
								// started.
								if( GuiVar_WalkThruCountingDown == (true) )
								{
									bad_key_beep();
								}
								else
								{
									// 3/13/2018 Ryan : Retain the currently selected order when
									// the user presses NEXT or PREVIOUS.
									wt_index_of_order_when_dialog_displayed = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 1, 0 );
	
									GROUP_process_NEXT_and_PREV( pkey_event.keycode, WALK_THRU_get_num_groups_in_use(), (void*)&WALK_THRU_extract_and_store_changes_from_GuiVars, (void*)&WALK_THRU_get_group_at_this_index, (void*)&WALK_THRU_copy_group_into_guivars );
	
									// 3/13/2018 Ryan : Populate and draw the order scroll box.
									lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
									lde._04_func_ptr = (void *)&WALK_THRU_draw_order_scrollbox;
									Display_Post_Command( &lde );
									Redraw_Screen( (false) );
								}
								break;

							default:
								// 3/13/2018 Ryan : Retain the currently selected order when
								// the user presses NEXT or PREVIOUS.
								wt_index_of_order_when_dialog_displayed = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 1, 0 );
	
								GROUP_process_NEXT_and_PREV( pkey_event.keycode, WALK_THRU_get_num_groups_in_use(), (void*)&WALK_THRU_extract_and_store_changes_from_GuiVars, (void*)&WALK_THRU_get_group_at_this_index, (void*)&WALK_THRU_copy_group_into_guivars );
	
	
								// 3/13/2018 Ryan : Populate and draw the order scroll box.
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = (void *)&WALK_THRU_draw_order_scrollbox;
								Display_Post_Command( &lde );
								Redraw_Screen( (false) );
						}
						break;
	
					case KEY_C_UP:
						switch( GuiLib_ActiveCursorFieldNo )
						{
							case CP_WALK_THRU_NAME:
								bad_key_beep();
								break;
								
							case CP_WALK_THRU_START_CANCEL:
							
								// 4/18/2018 Ryan : User can not leave the start/cancel button until the Walk-Thru has
								// started.
								if( GuiVar_WalkThruCountingDown == (true) )
								{
									bad_key_beep();
								}
								else
								{
									CURSOR_Up( (true) );
								}
								break;
								
							case CP_WALK_THRU_STATION_LIST:
								lactive_line = GuiLib_ScrollBox_GetActiveLine( 1, 0 );
	
								if( lactive_line > GuiLib_NO_CURSOR )
								{
									if( lactive_line == 0 )
									{
										wt_index_of_order_when_dialog_displayed = GuiLib_NO_CURSOR;
										lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
										lde._04_func_ptr = (void*)&WALK_THRU_leave_order_scrollbox;
										Display_Post_Command( &lde );
									}
									else
									{
										SCROLL_BOX_up_or_down( 1, pkey_event.keycode );
									}
								}
								else
								{
									bad_key_beep();
								}
								break;
							
							default:
								CURSOR_Up( (true) );					
						}
						break;
	
					case KEY_C_DOWN:
						switch( GuiLib_ActiveCursorFieldNo )
						{
							case CP_WALK_THRU_START_CANCEL:
							
								// 4/18/2018 Ryan : User can not leave the start/cancel button until the Walk-Thru has
								// started.
								if( GuiVar_WalkThruCountingDown == (true) )
								{
									bad_key_beep();
								}
								else
								{
									good_key_beep();
									lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
									lde._04_func_ptr = (void*)&WALK_THRU_enter_order_scrollbox;
									lde._06_u32_argument1 = (true);
									Display_Post_Command( &lde );
								}
								break;
	
							case CP_WALK_THRU_STATION_LIST:
								lactive_line = GuiLib_ScrollBox_GetActiveLine( 1, 0 );
	
								if( lactive_line > GuiLib_NO_CURSOR )
								{
									SCROLL_BOX_up_or_down( 1, pkey_event.keycode );
								}
								else
								{
									bad_key_beep();
								}
								break;
	
							default:
								CURSOR_Down( (true) );
						}
						break;
	
					case KEY_C_LEFT:
						switch( GuiLib_ActiveCursorFieldNo )
						{
							case CP_WALK_THRU_NAME:
							
								KEY_process_BACK_from_editing_screen( (void*)&WALK_THRU_return_to_menu );
								
								break;
	
							case CP_WALK_THRU_STATION_LIST:
								lactive_line = GuiLib_ScrollBox_GetActiveLine( 1, 0 );
	
								if( lactive_line > GuiLib_NO_CURSOR )
								{
									if( lactive_line == 0 )
									{
										wt_index_of_order_when_dialog_displayed = GuiLib_NO_CURSOR;
										lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
										lde._04_func_ptr = (void*)&WALK_THRU_leave_order_scrollbox;
										Display_Post_Command( &lde );
									}
									else
									{
										// 3/14/2018 Ryan : Force the keycode to be KEY_C_UP so the SCROLL_BOX_up_or_down function
										// handles it properly.
										pkey_event.keycode = KEY_C_UP;
	
										SCROLL_BOX_up_or_down( 1, pkey_event.keycode );
									}
								}
								else
								{
									bad_key_beep();
								}
								break;
							
							case CP_WALK_THRU_START_CANCEL:
							
								// 4/18/2018 Ryan : User can not leave the start/cancel button until the Walk-Thru has
								// started.
								if( GuiVar_WalkThruCountingDown == (true) )
								{
									bad_key_beep();
								}
								else
								{
									CURSOR_Up( (true) );
								}
								break;
	
							default:
								CURSOR_Up( (true) );
						}
						break;
	
					case KEY_C_RIGHT:
						switch( GuiLib_ActiveCursorFieldNo )
						{
							case CP_WALK_THRU_START_CANCEL:
							
								// 4/18/2018 Ryan : User can not leave the start/cancel button until the Walk-Thru has
								// started.
								if( GuiVar_WalkThruCountingDown == (true) )
								{
									bad_key_beep();
								}
								else
								{
									good_key_beep();
									lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
									lde._04_func_ptr = (void*)&WALK_THRU_enter_order_scrollbox;
									lde._06_u32_argument1 = (true);
									Display_Post_Command( &lde );
								}
								break;
	
							case CP_WALK_THRU_STATION_LIST:
								lactive_line = GuiLib_ScrollBox_GetActiveLine( 1, 0 );
	
								if( lactive_line > GuiLib_NO_CURSOR )
								{
									if( lactive_line == ( wt_visible_number_in_order_sequence - 1 ) )
									{
										bad_key_beep();
									}
									else
									{
										// 3/14/2018 Ryan : Force the keycode to be KEY_C_DOWN so the SCROLL_BOX_up_or_down function
										// handles it properly.
										pkey_event.keycode = KEY_C_DOWN;
	
										SCROLL_BOX_up_or_down( 1, pkey_event.keycode );
									}
								}
								else
								{
									bad_key_beep();
								}
								break;
	
							default:
								CURSOR_Down( (true) );
						}
						break;
	
					case KEY_BACK:
					
						// 4/18/2018 Ryan : User can not leave the start/cancel button until the Walk-Thru has
						// started.
						if( GuiVar_WalkThruCountingDown == (true) )
						{
							bad_key_beep();
						}
						else
						{
							KEY_process_BACK_from_editing_screen( (void*)&WALK_THRU_return_to_menu );
						}
						
						break;
						
					case KEY_STOP:
					case KEY_ENG_SPA:
						switch( GuiLib_ActiveCursorFieldNo )
						{
							
							case CP_WALK_THRU_START_CANCEL:
								// 4/3/2018 Ryan : Stop countdown if stop button or ENG/SPA button is pressed. Otherwise
								// countdown is left paused where it was when the button was pressed.
								if( GuiVar_WalkThruCountingDown == (true) )
								{
									good_key_beep();
									
									xTimerStop( wt_delay_timer, portMAX_DELAY );

									GuiVar_WalkThruCountingDown = (false);

									GuiVar_WalkThruDelay = wt_delay_copy;

									Redraw_Screen( (false) );
								}
								
								KEY_process_global_keys( pkey_event );
								
								break;
								
							default:
								KEY_process_global_keys( pkey_event );
						}
						break;
	
					default:
						KEY_process_global_keys( pkey_event );
						
				}  // of key processing switch  statment
				
		}  // of switch statement of which structure

	}  // of normal key processing

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Returns to the menu on the left from the editing screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user presses a key to move away from the editing screen and back
 * onto the menu.
 * 
 * @param psave_changes True if the changes should be saved; otherwise, false.
 *
 * @author 3/14/2018 Ryan
 *
 * @revisions (none)
 */
static void WALK_THRU_return_to_menu( void )
{
	FDTO_GROUP_return_to_menu( &wt_editing_group, (void*)&WALK_THRU_extract_and_store_changes_from_GuiVars );

	wt_index_of_order_when_dialog_displayed = GuiLib_NO_CURSOR;

	// 3/14/2018 Ryan : Force the order scroll box to be redrawn as well. Otherwise, it will
	// blank out when screen is redrawn.
	WALK_THRU_draw_order_scrollbox();
}

/* ---------------------------------------------------------- */
/**
 * Draws the Main Line menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the screen is first navigated to.
 * 
 * @param pcomplete_redraw True if the screen should be complete redrawn;
 * otherwise, false. If false, only elements on the screen are updated, not the
 * entire structure, essentially refreshing the content.
 *
 * @author 3/14/2018 Ryan
 *
 * @revisions (none)
 */
extern void WALK_THRU_draw_menu( const BOOL_32 pcomplete_redraw )
{
	if( pcomplete_redraw )
	{
		wt_index_of_order_when_dialog_displayed = GuiLib_NO_CURSOR;

		// 4/3/2018 Ryan : Make sure the countdown is not active when first entering the screen.
		GuiVar_WalkThruCountingDown = (false);
	}

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );
	
	FDTO_GROUP_draw_menu( pcomplete_redraw, &wt_editing_group, WALK_THRU_get_num_groups_in_use(), GuiStruct_scrWalkThru_0, (void *)&WALK_THRU_load_group_name_into_guivar, (void *)&WALK_THRU_copy_group_into_guivars, (true) );
	
	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );

	// 3/14/2018 Ryan : Populate and draw the order scrollbox.
	WALK_THRU_draw_order_scrollbox();
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed on the Main Line menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 3/14/2018 Ryan
 *
 * @revisions (none)
 */
extern void WALK_THRU_process_menu( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	GROUP_process_menu( pkey_event, &wt_editing_group, WALK_THRU_get_num_groups_in_use(), (void *)&WALK_THRU_process_key_event, (void *)&WALK_THRU_add_new_group, (void *)&WALK_THRU_get_group_at_this_index, (void *)&WALK_THRU_copy_group_into_guivars );

	// Refresh the order scroll box when navigating up and down the system menu
	if( (pkey_event.keycode != KEY_BACK) && ((UNS_32)GuiLib_ScrollBox_GetActiveLine(0, 0) < WALK_THRU_get_num_groups_in_use()) && (wt_editing_group == (false)) )
	{
		SCROLL_BOX_redraw( 1 );
	}

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
