/*  file = r_alerts.c   2009.05.27  rmd                       */

// Our scheme is to allocate memory for the text to be displayed. It will be an
// array of array of chars. Each line will have a maximum of 64 chars of text.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for abs
#include	<stdlib.h>

// 6/18/2014 ajv : Required for memset
#include	<string.h>

#include	"r_alerts.h"

#include	"alert_parsing.h"

#include	"app_startup.h"

#include	"change.h"

#include	"dialog.h"

#include	"epson_rx_8025sa.h"

#include	"foal_comm.h"

#include	"irri_comm.h"

#include	"report_utils.h"

#include	"speaker.h"

#include	"screen_utils.h"

#include	"scrollbox.h"

#include	"station_groups.h"


// 8/29/2013 rmd : The following inlcudes can be deleted after controller initiated
// debug.
#include	"controller_initiated.h"

#include	"cent_comm.h"

#include	"flowsense.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	ALERT_LINES_TO_SHOW					(18)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2012.05.09 ajv : Preserve the number of displayed alerts to determine whether
// the number of alerts has changed when redrawing the scrollbox. If the number
// of alerts has changed, we need to handle the redraw the entire scroll box.
// Otherwise, we can just refresh what's visible on the screen.
UNS_32	g_ALERTS_line_count;

UNS_32	g_ALERTS_pile_to_show;

UNS_32	g_ALERTS_filter_to_show;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Initialized on each program start via "ALERTS_test_all_piles" which is called
// during the system startup task.
ALERTS_DISPLAY_STRUCT	alerts_display_struct_user;

ALERTS_DISPLAY_STRUCT	alerts_display_struct_changes;

ALERTS_DISPLAY_STRUCT	alerts_display_struct_tp_micro;

ALERTS_DISPLAY_STRUCT	alerts_display_struct_engineering;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* TODO: PROGRAM LAST STARTS								  */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#if 0

// we want the last starts to be preserved through a power-fail (program
// restart) so that the status screen shows appropiately after such - which
// means we have to also init them as part of a rom update - cause they could
// move as well as the means to init them in the first place.
static DATE_TIME        R_ALERTS_last_starts[ PROG_LAST ] __attribute__(BATTERY_BACKED_VARS);

/* ---------------------------------------------------------- */
extern void ALERTS_init_last_starts( void )
{
	UNS_32  i;

	for( i = 0; i < PROG_LAST; ++i )
	{
		EPSON_obtain_latest_time_and_date( &(R_ALERTS_last_starts[ i ]) );
	}
}

/* ---------------------------------------------------------- */

static DATE_TIME R_ALERTS_prog_active_last_start( char *pstr )
{
	// The passed pstr is the program name string. We are thinking of using that to determine which program we are
	// trying to find out when it last ran.
	// 
	// We are also thinking of storing the last time the program ran within the program itself.

	DATE_TIME rv;

	EPSON_obtain_latest_time_and_date( &rv );

	// To stop compiler warning messages.
	(void)pstr;

	// this function covers not showing alerts that exist for programs that are no longer
	// in use or no longer have any stations tied to them - if there aren't stations aren't tied to them
	// don't show the alerts - also if there isn't a start time or waterdays don't show the alert

	// establish a point in the future to compare against
	rv.D = (rv.D + 1000);
	rv.T = 0;


/* TODO just to get it to compile
	if ( ThisProgramHasASXAndWaterDay( rpWDSX, pprog ) == (true) ) {
	
		if ( ThereAreStationsAssignedToThisProgram( pprog ) == (true) ) {
		
			rv = R_ALERTS_last_starts[ pprog ];
			
		}
		
	}
*/

	return( rv );
}

/* ---------------------------------------------------------- */
static DATE_TIME R_ALERTS_oldest_active_last_start( void )
{
	DATE_TIME rv;

	EPSON_obtain_latest_time_and_date( &rv );

	// establish a point in the future to compare against
	rv.D = (rv.D + 1000);   
	rv.T = 0;

/* TODO just to get it to compile
PROG_ASS p;


	for ( p=PROG_A; p<PROG_LAST; p++ ) {
	
		if ( ThisProgramHasASXAndWaterDay( rpWDSX, p ) == (true) ) {
		
			if ( ThereAreStationsAssignedToThisProgram( p ) == (true) ) {

				if ( DT1_IsBiggerThanOrEqualTo_DT2( rv, R_ALERTS_last_starts[ p ] ) == (true) ) {
				
					rv = R_ALERTS_last_starts[ p ];
					
				}

			}

		}
		
	}
*/

	return( rv );
}

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ALERT LINE PARSING   									  */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


/* ---------------------------------------------------------- */
/**
 * Parses the alert line at the specified index, generating the text when
 * displaying the alert, and returns the length of the text, in bytes.
 * 
 * @warning Because mem_malloc and mem_free are protected by the
 *  		MEMORY_PROTECTION_MUTEX and because those functions call
 *  		Alert_Message as partitions run out or other various errors, the
 *  		code within the whole alerts displaying and alert creation chain of
 *  		functions (wrapped by the alerts_pile_recursive_MUTEX) cannot call
 *  		either the mem_malloc or mem_free functions.
 * 
 * @mutex Calling function must have the alerts_pile_recursive_MUTEX mutex to
 *  	  ensure the number of alerts doesn't change during this process. This
 *  	  is called when both adding an alert and when displaying alerts. Adding
 *  	  causes the pile to change. This could lead to an unpredictable display
 *  	  if the mutex is not taken.
 *
 * @executed 
 * 
 * @param pparse_why The reason for parsing the alert. This can either be
 *  				 PARSE_FOR_LENGTH_ONLY or PARSE_FOR_LENGTH_AND_TEXT.
 * @param pdest The memory location to put the text to display.
 * @param pdest_size The available space to place the text into.
 * @param pindex The index in the pile to the start of the alert.
 * 
 * @return UNS_32 The byte count of the parsed alert. This count is needed to
 *                move ahead the FIRST index when adding an alert.
 *
 * @author 5/10/2012 Adrianusv
 *
 * @revisions
 *    5/10/2012 Initial release
 */
extern UNS_32 nm_ALERTS_parse_alert_and_return_length( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, char *pdest_ptr, const UNS_32 pallowable_size, UNS_32 pindex )
{
	UNS_32 rv;

	// ----------

	// 10/31/2012 rmd : Because the alert ID is held on the pile as 2 bytes we retrieve into the
	// 16 bit variable. But wish to work with it beyond that as a native 32 bit variable. For
	// code size efficiency.
	UNS_16	aid_16;

	UNS_32	aid_32;

	// ----------

	DATE_TIME ldt;

	// Temporary buffer for use when parsing the date & time.
	char dt_buf[ 16 ];  

	// ----------

	rv = 0;

	// ----------
	
	// Get the alert ID
	rv += ALERTS_pull_object_off_pile( ppile, &aid_16, &pindex, sizeof(UNS_16) );
	
	aid_32 = aid_16;
	
	// Get the DATE_TIME
	rv += ALERTS_pull_object_off_pile( ppile, &ldt, &pindex, sizeof( DATE_TIME ) );

	// 9/10/2014 rmd : And clean the seconds field of the pseudo_ms value. Remember this value
	// is inserted to make alerts generated within the same second unique.
	ldt.T &= PSEUDO_MS_ANDING_MASK_TO_REMOVE;
	
	// ----------
	
	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( g_ALERTS_pile_to_show == ALERTS_PILE_TP_MICRO )
		{
			// When viewing the TP Micro pile, we want to see second-by-second
			// alerts. Therefore, we're sacrificing the month to show the time
			// the alert was generated down to the second.
			UNS_32 lday, lmonth, lyear, ldow;

			DateToDMY( ldt.D, &lday, &lmonth, &lyear, &ldow );

			#ifdef SUPPRESS_MONTH_FROM_TP_MICRO_ALERTS

			snprintf( GuiVar_AlertDate, sizeof(GuiVar_AlertDate), "%d    ", lday );

			#else

			snprintf( GuiVar_AlertDate, sizeof(GuiVar_AlertDate), "%d/%02d  ", lmonth, lday );

			#endif

			strlcpy( GuiVar_AlertTime, TDUTILS_time_to_time_string_with_ampm(dt_buf, sizeof(dt_buf), ldt.T, TDUTILS_DONT_TREAT_AS_A_SX, TDUTILS_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING), sizeof(GuiVar_AlertTime) );
		}
		else
		{
			strlcpy( GuiVar_AlertDate, GetDateStr(dt_buf, sizeof(dt_buf), ldt.D, DATESTR_show_year_not, DATESTR_show_dow_not), sizeof(GuiVar_AlertDate) );
			strlcpy( GuiVar_AlertTime, TDUTILS_time_to_time_string_with_ampm(dt_buf, sizeof(dt_buf), ldt.T, TDUTILS_DONT_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING), sizeof(GuiVar_AlertTime) );
		}
	}


	rv += nm_ALERT_PARSING_parse_alert_and_return_length( aid_32, ppile, pparse_why, pdest_ptr, pallowable_size, pindex);

	return( rv );
}


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* DISPLAY SUPPORT  										  */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


/* ---------------------------------------------------------- */
/**
 * Builds the array of indicies to display. Remember the start_indicies array
 * contains the indicies in reverse chronological order - the order we want them
 * in - so start with index 0 and work towards the last one, which is the
 * oldest.
 * 
 * @mutex Calling function must have the alerts_pile_recursive_MUTEX mutex to
 *  	  ensure the number of alerts doesn't change during this process.
 * 
 * @executed Executed within the context of the display processing task when the
 *  		 Alerts report is drawn.
 * 
 * @param ppile A pointer to the alert pile to restart the display indicies of.
 *
 * @author 5/9/2012 Adrianusv
 *
 * @revisions
 *    5/9/2012 Initial release
 */
static void nm_ALERTS_restart_display_start_indicies( ALERTS_PILE_STRUCT *const ppile, ALERTS_DISPLAY_STRUCT *const pdisplay_struct )
{
	UNS_32 i;

	pdisplay_struct->display_total = 0;

	for( i = 0; i < ppile->count; ++i )
	{
		// If the alert falls within the filtered view, capture the index of the
		// alert as one to show on the display and increment the display count.
		if( ALERT_this_alert_is_to_be_displayed( ppile, g_ALERTS_filter_to_show, pdisplay_struct->all_start_indicies[ i ] ) == (true) )
		{
			pdisplay_struct->display_start_indicies[ pdisplay_struct->display_total ] = pdisplay_struct->all_start_indicies[ i ];

			++pdisplay_struct->display_total;
		}
	}
}

/* ---------------------------------------------------------- */
/**
 * Refreshes the all_start_indicies variable when the alerts pile is reset.
 * 
 * @mutex Calling function must have the alerts_pile_recursive_MUTEX mutex to
 *  	  ensure the number of alerts doesn't change during this process.
 * 
 * @executed 
 *
 * @author 5/10/2012 Adrianusv
 *
 * @revisions
 *    5/10/2012 Initial release
 */
extern void nm_ALERTS_refresh_all_start_indicies( ALERTS_PILE_STRUCT *const ppile )
{
	ALERTS_DISPLAY_STRUCT	*ldisplay_struct;

	UNS_32  a_index;

	UNS_32  i;

	ldisplay_struct = ALERTS_get_display_structure_for_pile( ppile );

	memset( ldisplay_struct->all_start_indicies, 0x00, sizeof(ldisplay_struct->all_start_indicies) );

	// Fill in the array from the OLDEST to the most RECENT
	if( ppile->count > 0 )
	{
		a_index = ppile->first;

		for( i = ppile->count; i > 0; --i )
		{
			ldisplay_struct->all_start_indicies[ (i - 1) ] = (UNS_16)a_index;

			ALERTS_inc_index( ppile, &a_index, nm_ALERTS_parse_alert_and_return_length( ppile, PARSE_FOR_LENGTH_ONLY, NULL, 0, a_index ) );
		}
	}
}

/* ---------------------------------------------------------- */
/**
 * It has been decided that this start index needs to be inserted at the
 * beginning of the array of those to display.
 * 
 * For example, if there are 8 alerts to display when a new one is added, we
 * need to move those 8 down one slot. So we have 8 actions to perform. The
 * first manuever is index 8 = index 7.
 *
 * Display_total follows to tally the number of alerts when all are being
 * displayed. The goal is to always build up the stack of those to show, keeping
 * the most recent alert at index 0.
 *
 * If there are none display, there is nothing to do; simply add the new one and
 * increment the count at the end. If there is one or more, we need to perform
 * the roll.
 *
 * @mutex Calling function must have the alerts_pile_recursive_MUTEX mutex to
 *  	  ensure the number of alerts doesn't change during this process.
 * 
 * @executed 
 * 
 * @param pstart_index The start index of the alert being added.
 *
 * @author 5/10/2012 Adrianusv
 *
 * @revisions
 *    5/10/2012 Initial release
 */
extern void nm_ALERTS_roll_and_add_new_display_start_index( ALERTS_PILE_STRUCT *const ppile, ALERTS_DISPLAY_STRUCT *const pdisplay_struct, const UNS_32 pstart_index )
{
	UNS_32 lfrom_index, lto_index;

	// Bounds check the display_total - at this point the most should be 1 less
	// than the maximum possible number of alerts - correct when the alert was
	// added the display_total was decremented by one so it must be at no more
	// than ALERT_MAX_ALERTS - 2.
	// 11/27/2012 rmd : WHY are we testing display_total HERE?  TODO AJ and I found a problem
	// with the math. Have revised the >= to just >.
	if( pdisplay_struct->display_total > ((alert_piles[ ppile->alerts_pile_index ].pile_size / ALERTS_MIN_STORED_BYTE_COUNT) - 1) )
	{
		// Not sure we can make an alert line here. As this function may get
		// invoked again. And the process repeats for ever. So use assert for
		// now. Which also makes an alert line. However the RELEASE code comiles
		// assert to nothing. So safe in the field. And the way its coded we set
		// the display total to 5 and go on.
		// 
		// snprintf( str_64, 64, "Display total alerts=%d (out of range)", r_alerts.display_total );

		// First off make it so this error doesn't occur again. And now in range
		// that we may be successful when assert attempts its alert. Better yet
		// invalidate the alert pile. We have a test in assert for this. And
		// taking this approach at least (hopefully) we get something on the
		// screen about this problem.
		alerts_struct_engineering.ready_to_use_bool = (false);  // To prevent alert line attempt within assert function.

		// Bring it back into range. May not be of any value at this point. As
		// assert is going to end the show.
		pdisplay_struct->display_total = 5;

		assert( 0 );
	}
	else if( pdisplay_struct->display_total == 0 )
	{
		// Nothing to do
	}
	else
	{
		lfrom_index = (pdisplay_struct->display_total - 1);
		lto_index = pdisplay_struct->display_total;

		do
		{
			pdisplay_struct->display_start_indicies[ lto_index ] = pdisplay_struct->display_start_indicies[ lfrom_index ];

			// If we are done leave the while loop
			if( lfrom_index == 0 )
			{
				break;
			}

			// Otherwise, move on down through the pile
			lto_index--;
			lfrom_index--;

		} while( (true) );
	}

	// Add the new one!
	pdisplay_struct->display_start_indicies[ 0 ] = (UNS_16)pstart_index;

	// Update how many potential to display - the display_total is kept within
	// bounds as part of the process of adding the alert - we have also added
	// some bounds checking within this function.
	++pdisplay_struct->display_total;
}


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ALERT FILTER SELECTION AND PROCESSING					  */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while on the Alert Filters menu.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 *
 * @param pkey_event The key event to be processed.
 *
 * @author 5/9/2012 Adrianusv
 *
 * @revisions
 *    5/9/2012 Initial release
 */
/*
static void ALERTS_process_alert_filters( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	switch( pkey_event.keycode )
	{
		case KEY_A_L_1:
			nm_ALERTS_change_filter( R_ALERTS_FILTER_SHOW_IRRIGATION, (true) );
			break;

		case KEY_A_L_2:
			nm_ALERTS_change_filter( R_ALERTS_FILTER_SHOW_WEATHER, (true) );
			break;

		case KEY_A_L_3:
			nm_ALERTS_change_filter( R_ALERTS_FILTER_SHOW_COMMUNICATION, (true) );
			break;

		case KEY_A_L_5:
			nm_ALERTS_change_filter( R_ALERTS_FILTER_SHOW_ALL, (true) );
			break;

		default:
			KEY_process_global_keys( pkey_event );
			break;
	}

	if( (pkey_event.keycode == KEY_A_L_1) || (pkey_event.keycode == KEY_A_L_2) ||
		(pkey_event.keycode == KEY_A_L_3) || (pkey_event.keycode == KEY_A_L_5) )
	{
		GuiVar_AlertsPileToShow = ALERTS_PILE_USER;

		pkey_event.keycode = KEY_BACK;
		KEY_process_global_keys( pkey_event );
	}
}
*/

/* ---------------------------------------------------------- */
/**
 * Sets the display filter to the selected filter.
 * 
 * @mutex_requirements Calling function must have the
 * alerts_pile_recursive_MUTEX mutex to ensure the number of alerts doesn't
 * change during this process.
 * 
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the key processing task when
 * the user changes alert filters.
 * 
 * @param pfilter The new filter to change to.
 * 
 * @param ptriggered_by_key_press True if the display total should be
 * generated now; otherwise, false. This is used by the keyboard timeout
 * function to get an accurate r_alerts.display total now because it needs to be
 * used.
 *
 * @author 5/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void nm_ALERTS_change_filter( const UNS_32 pfilter, const BOOL_32 ptriggered_by_key_press )
{
	g_ALERTS_filter_to_show = pfilter;

	if( ptriggered_by_key_press == (false) )
	{
		nm_ALERTS_restart_display_start_indicies( &alerts_struct_user, &alerts_display_struct_user );

		// Always do this since the number of alerts may have changed
		alerts_display_struct_user.regenerate_displaystartindicies = (true);
	}
}

/* ---------------------------------------------------------- */
/**
 * This function serves two purposes:
 * 
 *   1) To tell us if this alert is included in the filtering. To do that, the 
 *      function pulls the alert apart to get at the date and, in some cases,
 *  	more like the station number.
 * 
 *   2) To capture some of the pulled part information for use in telling us if
 *      we can display the station number and what it is.
 * 
 * @executed Executed within the context of the display processing task when the
 * 			 Alerts report is drawn.
 * 
 * @param pfilter The currently selected filter.
 * @param pindex The index into the alert pile of the start of the alert.
 * 
 * @return BOOL_32 True if the alert is to be displayed; otherwise, false.
 *
 * @author 5/9/2012 Adrianusv
 *
 * @revisions
 *    5/9/2012 Initial release
 */
extern BOOL_32 ALERT_this_alert_is_to_be_displayed( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pfilter, UNS_32 pindex )
{
	// ----------

	// 10/31/2012 rmd : Because the alert ID is held on the pile as 2 bytes we retrieve into the
	// 16 bit variable. But wish to work with it beyond that as a native 32 bit variable. For
	// code size efficiency.
	UNS_16	aid_16;

	UNS_32	aid_32;

	// ----------

	// For when we have to pull apart certain alerts to get to their date/time
	// and station number when deciding if we want to show them
	DATE_TIME ldt;

	BOOL_32 rv;

	if( g_ALERTS_pile_to_show == ALERTS_PILE_USER )
	{
		rv = (false);

		// MUST use pull_object_off_pile to properly get the alert ID (aid) which is
		// always the first 2 bytes of the alert.
		ALERTS_pull_object_off_pile( ppile, &aid_16, &pindex, sizeof(UNS_16) );
		aid_32 = aid_16;

		ALERTS_pull_object_off_pile( ppile, &ldt, &pindex, sizeof( DATE_TIME ) );

		switch( pfilter )
		{
			case R_ALERTS_FILTER_SHOW_ALL:
				rv = (true);
				break;

			case R_ALERTS_FILTER_SHOW_COMMUNICATION:
				rv = (false);
				break;

			case R_ALERTS_FILTER_SHOW_IRRIGATION:
				rv = (false);
				break;

			case R_ALERTS_FILTER_SHOW_WEATHER:
				rv = (false);
				break;
		}
	}
	else
	{
		rv = (true);
	}

	return( rv );
}


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* SCREEN DRAWING AND KEY PROCESSING						  */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static ALERTS_PILE_STRUCT *ALERTS_get_currently_displayed_pile( void )
{
	ALERTS_PILE_STRUCT  *lpile;

	if( g_ALERTS_pile_to_show == ALERTS_PILE_USER )
	{
		lpile = &alerts_struct_user;
	}
	else if( g_ALERTS_pile_to_show == ALERTS_PILE_CHANGES )
	{
		lpile = &alerts_struct_changes;
	}
	else if( g_ALERTS_pile_to_show == ALERTS_PILE_TP_MICRO )
	{
		lpile = &alerts_struct_tp_micro;
	}
	else if( g_ALERTS_pile_to_show == ALERTS_PILE_ENGINEERING )
	{
		lpile = &alerts_struct_engineering;
	}
	else
	{
		lpile = &alerts_struct_user;

		Alert_Message_va( "Invalid alert pile to show: %d", g_ALERTS_pile_to_show );
	}

	return( lpile );
}

/* ---------------------------------------------------------- */
extern ALERTS_DISPLAY_STRUCT *ALERTS_get_display_structure_for_pile( const ALERTS_PILE_STRUCT *const ppile )
{
	ALERTS_DISPLAY_STRUCT  *ldisplay_struct;

	if( ppile == &alerts_struct_user )
	{
		ldisplay_struct = &alerts_display_struct_user;
	}
	else if( ppile == &alerts_struct_changes )
	{
		ldisplay_struct = &alerts_display_struct_changes;
	}
	else if( ppile == &alerts_struct_tp_micro )
	{
		ldisplay_struct = &alerts_display_struct_tp_micro;
	}
	else if( ppile == &alerts_struct_engineering )
	{
		ldisplay_struct = &alerts_display_struct_engineering;
	}
	else
	{
		ldisplay_struct = &alerts_display_struct_user;

		Alert_Message( "Invalid pile" );
	}

	return( ldisplay_struct );
}

/* ---------------------------------------------------------- */
/**
 * Draws an individual scroll line for the Alerts report. This is used as the
 * callback routine for the GuiLib_ScrollBox_Init routine.
 * 
 * @mutex Calling function must have the alerts_pile_recursive_MUTEX mutex to
 *  	  ensure the number of alerts doesn't change during this process.
 *
 * @executed Executed within the context of the display processing task whenever
 *  		 the screen is drawn or the scroll box is redrawn, which happens
 *  		 when a new alert is added.
 * 
 * @param pline_index_0_i16 The highlighted scroll box line - 0 based.
 *
 * @author 5/9/2012 Adrianusv
 *
 * @revisions
 *    5/9/2012 Initial release
 */
static void nm_ALERTS_draw_scroll_line( const INT_16 pline_index_0_i16 )
{
	ALERTS_PILE_STRUCT  *lpile;

	ALERTS_DISPLAY_STRUCT	*ldisplay_struct;

	lpile = ALERTS_get_currently_displayed_pile();

	ldisplay_struct = ALERTS_get_display_structure_for_pile( lpile );

	nm_ALERTS_parse_alert_and_return_length( lpile, PARSE_FOR_LENGTH_AND_TEXT, GuiVar_AlertString, ALERTS_MAX_TOTAL_PARSED_LENGTH, ldisplay_struct->display_start_indicies[ (UNS_16)ldisplay_struct->display_index_of_first_line + pline_index_0_i16 ] );
}

/* ---------------------------------------------------------- */
static void FDTO_ALERTS_redraw_retaining_topline( const UNS_32 pscrollbox_index,
												  const UNS_32 pnumber_of_lines,
												  const BOOL_32 predraw_entire_scrollbox,
												  const BOOL_32 pretain_active_line )
{
	INT_32 lactive_line;
	INT_32 ltop_line;

	if( pnumber_of_lines == 0 )
	{
		Redraw_Screen( (false) );
	}
	else
	{
		if( predraw_entire_scrollbox == (true) )
		{
			if( g_DIALOG_ok_dialog_visible == (false) )
			{
				lactive_line = GuiLib_ScrollBox_GetActiveLine( (UNS_8)pscrollbox_index, 0 );

				ltop_line = GuiLib_ScrollBox_GetTopLine( (UNS_8)pscrollbox_index );

				// Verify the active line is valid. If not, it means the scroll
				// box was empty and now it's not. In this case, we need to
				// redraw the entire screen to show the item.
				if( lactive_line < 0 )
				{
					lactive_line = 0;
				}

				// If viewing an alerts report, it's possible alerts will be added
				// above the currently selected line. Therefore, increment the
				// active line to retain the current line.
				if( pretain_active_line == (true) )
				{
					// 2012.06.21 ajv : However, if we're on the very first line, we
					// don't want to move the line. We want to stay on the top line.
					if( lactive_line > 0 )
					{
						++lactive_line;
					}
				}

				// If the active line now exceeds the number of lines, set the
				// active line to the last item.
				if( (UNS_32)lactive_line > pnumber_of_lines )
				{
					lactive_line = (INT_32)pnumber_of_lines; 
				}

				// If we've incremented the active line and the new active line is
				// off of the screen, increment the top line to keep the active line
				// visible.
				if( (pretain_active_line == (true)) && (lactive_line > (ltop_line + ALERT_LINES_TO_SHOW)-1) )
				{
					++ltop_line;
				}
				// Otherwise, if an item was removed from the scrollbox which causes
				// the list to shift up, decrement the top line to shift it up by
				// one.
				else if ((ltop_line > 0) && ((UNS_32)(lactive_line + ltop_line) == pnumber_of_lines))
				{
					--ltop_line;
				}

				GuiLib_ScrollBox_Close( (UNS_8)pscrollbox_index );

				// Initialize the new scroll box, setting both the active and top
				// lines.
				GuiLib_ScrollBox_Init_Custom_SetTopLine( (UNS_8)pscrollbox_index, &nm_ALERTS_draw_scroll_line, (INT_16)pnumber_of_lines, (INT_16)lactive_line, (INT_16)ltop_line ); 
			}
		}
		else
		{
			GuiLib_ScrollBox_Redraw( (UNS_8)pscrollbox_index );
		}

		// Refresh the variables and update the display
		GuiLib_Refresh();
	}
}

/* ---------------------------------------------------------- */
/**
 * Redraws the scroll box on the Alerts report, retaining the currently selected
 * line and the top line.
 * 
 * @mutex Calling function must have the alerts_pile_recursive_MUTEX mutex to
 *  	  ensure the number of alerts doesn't change during this process.
 *
 * @executed Executed within the context of the display processing task when a
 *           new alert is added while the user is viewing the Alerts report.
 *
 * @author 5/9/2012 Adrianusv
 *
 * @revisions
 *    5/9/2012 Initial release
 */
extern void FDTO_ALERTS_redraw_scrollbox( void )
{
	ALERTS_PILE_STRUCT  *lpile;

	ALERTS_DISPLAY_STRUCT	*ldisplay_struct;

	lpile = ALERTS_get_currently_displayed_pile();

	ldisplay_struct = ALERTS_get_display_structure_for_pile( lpile );

	xSemaphoreTakeRecursive( alerts_pile_recursive_MUTEX, portMAX_DELAY );

	if( ldisplay_struct->regenerate_displaystartindicies == (true) )
	{
		nm_ALERTS_restart_display_start_indicies( lpile, ldisplay_struct );

		ldisplay_struct->regenerate_displaystartindicies = (false);

		// Since the display may have changed significantly, reset the display
		// indicies
		ldisplay_struct->display_index_of_first_line = 0;

		ldisplay_struct->reparse = (true);
	}

	if( ldisplay_struct->reparse == (true) )
	{
		// Reset the scroll position to ensure the alerts are all left-justified.
		GuiVar_ScrollBoxHorizScrollPos = 0;

		if( ldisplay_struct->display_total <= ALERT_LINES_TO_SHOW )
		{
			GuiLib_ScrollBox_Close( 0 );
			FDTO_Redraw_Screen( (true) );
		}
		else
		{
			FDTO_ALERTS_redraw_retaining_topline( 0, ldisplay_struct->display_total, (ldisplay_struct->display_total != g_ALERTS_line_count), (true) );
		}
	}

	xSemaphoreGiveRecursive( alerts_pile_recursive_MUTEX );

	GuiLib_Refresh();

	g_ALERTS_line_count = ldisplay_struct->display_total;

	ldisplay_struct->reparse = (false);
}

/* ---------------------------------------------------------- */
/**
 * Draws the Alerts report screen.
 * 
 * @mutex Calling function must have the alerts_pile_recursive_MUTEX mutex to
 *  	  ensure the number of alerts doesn't change during this process.
 *
 * @executed Executed within the context of the display processing task when the
 *           screen is initially drawn.
 * 
 * @author 5/9/2012 Adrianusv
 *
 * @revisions
 *    5/9/2012 Initial release
 */
extern void FDTO_ALERTS_draw_alerts( const BOOL_32 pcomplete_redraw )
{
	#define	ALERTS_REPORT_X_COORD_TO_DISPLAY_SCREEN_NAME			(108)

	#define ALERTS_REPORT_X_COORD_TO_DISPLAY_SCREEN_NAME_SPANISH	(128)

	ALERTS_PILE_STRUCT  *lpile;

	ALERTS_DISPLAY_STRUCT	*ldisplay_struct;

	INT_32	ltext_to_draw;

	// ----------

	GuiLib_ShowScreen( GuiStruct_rptAlerts_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );

	// ----------

	lpile = ALERTS_get_currently_displayed_pile();

	ldisplay_struct = ALERTS_get_display_structure_for_pile( lpile );

	// ----------

	if( ldisplay_struct == &alerts_display_struct_changes )
	{
		ltext_to_draw = GuiStruct_txtChangeLines_0;
	}
	else if( ldisplay_struct == &alerts_display_struct_tp_micro )
	{
		ltext_to_draw = GuiStruct_txtTPMicroAlerts_0;
	}
	else if( ldisplay_struct == &alerts_display_struct_engineering )
	{
		ltext_to_draw = GuiStruct_txtEngineeringAlerts_0;
	}
	else // if( ldisplay_struct == &alerts_display_struct_user )
	{
		ltext_to_draw = GuiStruct_txtAlerts_0;
	}

	// 9/4/2018 Ryan : X coordinate in title bar different if in Spanish.
	if( GuiLib_LanguageIndex == GuiConst_LANGUAGE_ENGLISH )
	{
		FDTO_show_screen_name_in_title_bar( ALERTS_REPORT_X_COORD_TO_DISPLAY_SCREEN_NAME, (UNS_32)ltext_to_draw );
	}
	else
	{
		FDTO_show_screen_name_in_title_bar( ALERTS_REPORT_X_COORD_TO_DISPLAY_SCREEN_NAME_SPANISH, (UNS_32)ltext_to_draw );
	}

	// ----------

	// Reset the scroll position to ensure the alerts are all left-justified.
	GuiVar_ScrollBoxHorizScrollPos = 0;

	xSemaphoreTakeRecursive( alerts_pile_recursive_MUTEX, portMAX_DELAY );

	// cause the alerts display to update if required
	// it turns out that being on screens such as station info and changing the
	// program assignment or being on the sx's and water days screen can change
	// the way the "status" view can look (you know stations not on a given
	// program anymore, or a program not active anymore - maybe even for
	// stations not in use anymore and who knows what other things should
	// reflect a change in the view so...
	ldisplay_struct->regenerate_displaystartindicies = (true);

	nm_ALERTS_restart_display_start_indicies( lpile, ldisplay_struct );

	ldisplay_struct->regenerate_displaystartindicies = (false);

	// Since the display may have changed significantly, reset the display
	// indicies
	ldisplay_struct->display_index_of_first_line = 0;

	ldisplay_struct->reparse = (true);

	g_ALERTS_line_count = ldisplay_struct->display_total;

	// ----------

	FDTO_REPORTS_draw_report( pcomplete_redraw, g_ALERTS_line_count, &nm_ALERTS_draw_scroll_line );

	// ----------

	xSemaphoreGiveRecursive( alerts_pile_recursive_MUTEX );

	ldisplay_struct->reparse = (false);
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while on the Alerts report screen.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 *
 * @param pkey_event The key event to be processed.
 *
 * @author 5/9/2012 Adrianusv
 *
 * @revisions
 *    5/9/2012 Initial release
 */
extern void ALERTS_process_report( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	static const UNS_32 MAX_ALERT_PIXELS = (319 - 12);

	UNS_32 Y_POS_OF_ALERT_STRING;

	ALERTS_PILE_STRUCT  *lpile;

	ALERTS_DISPLAY_STRUCT	*ldisplay_struct;

	char    str_256[ ALERTS_MAX_TOTAL_PARSED_LENGTH ];

	UNS_32	ltext_width;

	UNS_32  i;


	Y_POS_OF_ALERT_STRING = (81 - 2);

	lpile = ALERTS_get_currently_displayed_pile();

	ldisplay_struct = ALERTS_get_display_structure_for_pile( lpile );

	switch( pkey_event.keycode )
	{
		case KEY_C_LEFT:
		case KEY_C_RIGHT:
			xSemaphoreTakeRecursive( alerts_pile_recursive_MUTEX, portMAX_DELAY );

			nm_ALERTS_parse_alert_and_return_length( lpile, PARSE_FOR_LENGTH_AND_TEXT, str_256, ALERTS_MAX_TOTAL_PARSED_LENGTH, ldisplay_struct->display_start_indicies[ (UNS_16)ldisplay_struct->display_index_of_first_line + GuiLib_ScrollBox_GetActiveLine(0, 0) ] );

			ltext_width = GuiLib_GetTextWidth( str_256, GuiFont_FontList[GuiFont_ANSI7], GuiLib_PS_ON ) + Y_POS_OF_ALERT_STRING;

			if( ltext_width > MAX_ALERT_PIXELS )
			{
				if( ((pkey_event.keycode == KEY_C_LEFT) && (GuiVar_ScrollBoxHorizScrollPos < 0)) ||
					(((pkey_event.keycode == KEY_C_RIGHT) && (abs(GuiVar_ScrollBoxHorizScrollPos) < (INT_32)(ltext_width - MAX_ALERT_PIXELS)))) )
				{
					good_key_beep();

					for( i = 0; ((GuiVar_ScrollBoxHorizScrollPos-1 < 0) && (i < 4)); ++i )
					{
						if( pkey_event.keycode == KEY_C_LEFT )
						{
							++GuiVar_ScrollBoxHorizScrollPos;
						}
						else
						{
							--GuiVar_ScrollBoxHorizScrollPos;
						}
					}

					SCROLL_BOX_redraw( 0 );
				}
				else
				{
					// Already at beginning or end of the line
					bad_key_beep();
				}
			}
			else
			{
				// The text fits without scrolling
				bad_key_beep();
			}

			xSemaphoreGiveRecursive( alerts_pile_recursive_MUTEX );
			break;

		case KEY_C_UP:
		case KEY_C_DOWN:
			// 9/26/2014 ajv : Reset the horizontal scroll position in case the
			// user has scrolled right.
			GuiVar_ScrollBoxHorizScrollPos = 0;

			REPORTS_process_report( pkey_event, 0, 0 );
			break;

		// 7/1/2015 mpd : FogBugz #3150. This debug code was used to generate alerts on the user pile.
		#if 0
		case KEY_ENG_SPA:
			good_key_beep();
			//Alert_user_debug( FLOWSENSE_get_controller_index() );
			Alert_user_debug();
			break;
		#endif

		default:
			REPORTS_process_report( pkey_event, 0, 0 );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

