/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_R_RT_ELECTRICAL_H
#define _INC_R_RT_ELECTRICAL_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FDTO_REAL_TIME_ELECTRICAL_redraw_scrollbox( void );

extern void FDTO_REAL_TIME_ELECTRICAL_draw_report( const BOOL_32 pcomplete_redraw );

extern void REAL_TIME_ELECTRICAL_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

