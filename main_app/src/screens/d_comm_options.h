
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef	_INC_D_COMM_OPTIONS_H
#define	_INC_D_COMM_OPTIONS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_COMM_OPTIONS_TEST_CLOUD_COMMUNICATIONS	(50)
#define	CP_COMM_OPTIONS_TEST_RADIO_COMMUNICATIONS	(51)
#define	CP_COMM_OPTIONS_PORT_A_PROGRAMMING			(52)
#define	CP_COMM_OPTIONS_PORT_B_PROGRAMMING			(53)
#define	CP_COMM_OPTIONS_COMMUNICATIONS_HUB			(54)
#define	CP_COMM_OPTIONS_SERIAL_PORT_STATS			(55)
#define	CP_COMM_OPTIONS_ACTIVATE_OPTIONS			(56)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern UNS_32	g_COMM_OPTIONS_cursor_position_when_dialog_displayed;

extern BOOL_32	g_COMM_OPTIONS_dialog_visible;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void COMM_OPTIONS_draw_dialog( const BOOL_32 pcomplete_redraw );

extern void COMM_OPTIONS_process_dialog( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

