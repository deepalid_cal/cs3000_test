/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"e_keyboard.h"

#include	"cal_string.h"

#include	"configuration_controller.h"

#include	"configuration_network.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"e_about.h"

#include	"e_irrigation_system.h"

#include	"e_manual_programs.h"

#include	"e_poc.h"

#include	"e_station_groups.h"

#include	"e_stations.h"

#include	"irrigation_system.h"

#include	"k_process.h"

#include	"manual_programs.h"

#include	"poc.h"

#include	"speaker.h"

#include	"station_groups.h"

#include	"stations.h"

#include	"scrollbox.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_KEYBOARD_LAYOUT				(48)

// ----------

#define	CP_QWERTY_KEYBOARD_OK			(49)
#define	CP_QWERTY_KEYBOARD_1			(50)
#define	CP_QWERTY_KEYBOARD_2			(51)
#define	CP_QWERTY_KEYBOARD_3			(52)
#define	CP_QWERTY_KEYBOARD_4			(53)
#define	CP_QWERTY_KEYBOARD_5			(54)
#define	CP_QWERTY_KEYBOARD_6			(55)
#define	CP_QWERTY_KEYBOARD_7			(56)
#define	CP_QWERTY_KEYBOARD_8			(57)
#define	CP_QWERTY_KEYBOARD_9			(58)
#define	CP_QWERTY_KEYBOARD_0			(59)
#define	CP_QWERTY_KEYBOARD_DASH			(60)
#define	CP_QWERTY_KEYBOARD_EQUAL		(61)
#define	CP_QWERTY_KEYBOARD_Q			(62)
#define	CP_QWERTY_KEYBOARD_W			(63)
#define	CP_QWERTY_KEYBOARD_E			(64)
#define	CP_QWERTY_KEYBOARD_R			(65)
#define	CP_QWERTY_KEYBOARD_T			(66)
#define	CP_QWERTY_KEYBOARD_Y			(67)
#define	CP_QWERTY_KEYBOARD_U			(68)
#define	CP_QWERTY_KEYBOARD_I			(69)
#define	CP_QWERTY_KEYBOARD_O			(70)
#define	CP_QWERTY_KEYBOARD_P			(71)
#define	CP_QWERTY_KEYBOARD_LBRACKET		(72)
#define	CP_QWERTY_KEYBOARD_RBRACKET		(73)
#define	CP_QWERTY_KEYBOARD_A			(74)
#define	CP_QWERTY_KEYBOARD_S			(75)
#define	CP_QWERTY_KEYBOARD_D			(76)
#define	CP_QWERTY_KEYBOARD_F			(77)
#define	CP_QWERTY_KEYBOARD_G			(78)
#define	CP_QWERTY_KEYBOARD_H			(79)
#define	CP_QWERTY_KEYBOARD_J			(80)
#define	CP_QWERTY_KEYBOARD_K			(81)
#define	CP_QWERTY_KEYBOARD_L			(82)
#define	CP_QWERTY_KEYBOARD_SEMICOLON	(83)
#define	CP_QWERTY_KEYBOARD_APOS			(84)
#define	CP_QWERTY_KEYBOARD_BACKSLASH	(85)
#define	CP_QWERTY_KEYBOARD_Z			(86)
#define	CP_QWERTY_KEYBOARD_X			(87)
#define	CP_QWERTY_KEYBOARD_C			(88)
#define	CP_QWERTY_KEYBOARD_V			(89)
#define	CP_QWERTY_KEYBOARD_B			(90)
#define	CP_QWERTY_KEYBOARD_N			(91)
#define	CP_QWERTY_KEYBOARD_M			(92)
#define	CP_QWERTY_KEYBOARD_COMMA		(93)
#define	CP_QWERTY_KEYBOARD_PERIOD		(94)
#define	CP_QWERTY_KEYBOARD_SLASH		(95)
#define	CP_QWERTY_KEYBOARD_ACCENT		(96)
#define	CP_QWERTY_KEYBOARD_SHIFT		(97)
#define	CP_QWERTY_KEYBOARD_SPACE		(98)
#define	CP_QWERTY_KEYBOARD_BACKSPACE	(99)

// ----------

#define	WRAP_KEYBOARD			(true)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static	BOOL_32	g_KEYBOARD_max_string_length;

static	UNS_32	g_KEYBOARD_cursor_position_when_keyboard_displayed;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void FDTO_show_keyboard( const BOOL_32 pcomplete_redraw );

static void hide_keyboard( void(*pDrawFunc)(const BOOL_32 pcomplete_redraw) );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Moves the cursor up on the on-screen keyboard. If the WRAP_KEYBOARD 
 * preprocessor directive is defined, the cursor will wrap top to bottom. 
 * 
 * @executed Executed within the context of the key processing task when the
 *           user presses a navigation key.
 *
 * @author 5/17/2011 Adrianusv
 *
 * @revisions
 *    5/17/2011 Initial release
 */
static void process_up_arrow( void )
{
	switch( GuiVar_KeyboardType )
	{
		case KEYBOARD_TYPE_QWERTY:
			if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_OK )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_SPACE, (true) );
			}
			else if( (GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_1) ||
					 (GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_2) )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_SHIFT, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_3 )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_X, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_4 )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_C, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( (GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_5) ||
					 (GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_6) ||
					 (GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_7) ||
					 (GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_8) )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_OK, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_9 )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_COMMA, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_0 )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_PERIOD, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( (GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_DASH) ||
					 (GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_EQUAL) )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_BACKSPACE, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo <= CP_QWERTY_KEYBOARD_BACKSLASH )
			{
				CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo - 12), (true) );
			}
			else if( GuiLib_ActiveCursorFieldNo <= CP_QWERTY_KEYBOARD_ACCENT )
			{
				CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo - 11), (true) );
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_SHIFT )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_Z, (true) );
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_BACKSPACE )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_SLASH, (true) );
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_SPACE )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_N, (true) );
			}
			else
			{
				bad_key_beep();
			}
			break;

		case KEYBOARD_TYPE_QWERTY__HEX_ONLY:
			if( (GuiLib_ActiveCursorFieldNo >= CP_QWERTY_KEYBOARD_1) && (GuiLib_ActiveCursorFieldNo <= CP_QWERTY_KEYBOARD_0) )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_OK, (true) );
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_E )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_3, (true) );
			}
			else if( (GuiLib_ActiveCursorFieldNo >= CP_QWERTY_KEYBOARD_A) && (GuiLib_ActiveCursorFieldNo <= CP_QWERTY_KEYBOARD_F) )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_E, (true) );
			}
			else if( (GuiLib_ActiveCursorFieldNo >= CP_QWERTY_KEYBOARD_C) && (GuiLib_ActiveCursorFieldNo <= CP_QWERTY_KEYBOARD_B) )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_F, (true) );
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_BACKSPACE )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_B, (true) );
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_OK )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_BACKSPACE, (true) );
			}
			else
			{
				bad_key_beep();
			}
			break;

		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/**
 * Moves the cursor down on the on-screen keyboard. If the WRAP_KEYBOARD 
 * preprocessor directive is defined, the cursor will wrap bottom to top. 
 * 
 * @executed Executed within the context of the key processing task when the
 *           user presses a navigation key.
 *
 * @author 5/17/2011 Adrianusv
 *
 * @revisions
 *    5/17/2011 Initial release
 */
static void process_down_arrow( void )
{
	switch( GuiVar_KeyboardType )
	{
		case KEYBOARD_TYPE_QWERTY:
			if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_OK )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_7, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_BACKSPACE )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_DASH, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_SPACE )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_OK, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_SHIFT )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_2, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_X )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_3, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_C )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_4, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_COMMA )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_9, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_PERIOD )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_0, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( (GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_A) ||
					 (GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_Z) )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_SHIFT, (true) );
			}
			else if( (GuiLib_ActiveCursorFieldNo >= CP_QWERTY_KEYBOARD_V) &&
					 (GuiLib_ActiveCursorFieldNo <= CP_QWERTY_KEYBOARD_M) )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_SPACE, (true) );
			}
			else if( (GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_SLASH) ||
					 (GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_ACCENT) )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_BACKSPACE, (true) );
			}
			else if( GuiLib_ActiveCursorFieldNo <= CP_QWERTY_KEYBOARD_RBRACKET )
			{
				CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo + 12), (true) );
			}
			else if( GuiLib_ActiveCursorFieldNo <= CP_QWERTY_KEYBOARD_BACKSLASH )
			{
				CURSOR_Select( (UNS_32)(GuiLib_ActiveCursorFieldNo + 11), (true) );
			}
			else
			{
				bad_key_beep();
			}
			break;

		case KEYBOARD_TYPE_QWERTY__HEX_ONLY:
			if( (GuiLib_ActiveCursorFieldNo >= CP_QWERTY_KEYBOARD_1) && (GuiLib_ActiveCursorFieldNo <= CP_QWERTY_KEYBOARD_0) )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_E, (true) );
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_E )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_D, (true) );
			}
			else if( (GuiLib_ActiveCursorFieldNo >= CP_QWERTY_KEYBOARD_A) && (GuiLib_ActiveCursorFieldNo <= CP_QWERTY_KEYBOARD_F) )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_C, (true) );
			}
			else if( (GuiLib_ActiveCursorFieldNo >= CP_QWERTY_KEYBOARD_C) && (GuiLib_ActiveCursorFieldNo <= CP_QWERTY_KEYBOARD_B) )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_BACKSPACE, (true) );
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_BACKSPACE )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_OK, (true) );
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_OK )
			{
				CURSOR_Select( CP_QWERTY_KEYBOARD_6, (true) );
			}
			else
			{
				bad_key_beep();
			}
			break;

		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/**
 * Moves the cursor left on the on-screen keyboard. If the WRAP_KEYBOARD 
 * preprocessor directive is defined, the cursor will wrap left to right.
 * 
 * @executed Executed within the context of the key processing task when the
 *           user presses a navigation key.
 *
 * @author 5/17/2011 Adrianusv
 *
 * @revisions
 *    5/17/2011 Initial release
 */
static void process_left_arrow( void )
{
	switch( GuiVar_KeyboardType )
	{
		case KEYBOARD_TYPE_QWERTY:
			if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_1 )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_EQUAL, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_Q )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_RBRACKET, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_A )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_BACKSLASH, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_Z )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_ACCENT, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_SHIFT )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_BACKSPACE, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else
			{
				CURSOR_Up( (true) );
			}
			break;

		case KEYBOARD_TYPE_QWERTY__HEX_ONLY:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_QWERTY_KEYBOARD_OK:
					CURSOR_Select( CP_QWERTY_KEYBOARD_BACKSPACE, (true) );
					break;

				default:
					CURSOR_Up( (true) );
			}
			break;

		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/**
 * Moves the cursor right on the on-screen keyboard. If the WRAP_KEYBOARD 
 * preprocessor directive is defined, the cursor will wrap right to left.
 * 
 * @executed Executed within the context of the key processing task when the
 *           user presses a navigation key.
 *
 * @author 5/17/2011 Adrianusv
 *
 * @revisions
 *    5/17/2011 Initial release
 */
static void process_right_arrow( void )
{
	switch( GuiVar_KeyboardType )
	{
		case KEYBOARD_TYPE_QWERTY:
			if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_EQUAL )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_1, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_RBRACKET )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_Q, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_BACKSLASH )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_A, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_ACCENT )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_Z, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_BACKSPACE )
			{
				#ifdef WRAP_KEYBOARD
					CURSOR_Select( CP_QWERTY_KEYBOARD_SHIFT, (true) );
				#else
					bad_key_beep();
				#endif
			}
			else
			{
				CURSOR_Down( (true) );
			}
			break;

		case KEYBOARD_TYPE_QWERTY__HEX_ONLY:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_QWERTY_KEYBOARD_BACKSPACE:
					CURSOR_Select( CP_QWERTY_KEYBOARD_OK, (true) );
					break;

				default:
					CURSOR_Down( (true) );
			}
			break;

		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void process_OK_button( void(*pDrawFunc)(const BOOL_32 pcomplete_redraw) )
{
	const char txtUnnamed[] = "Unnamed";

	good_key_beep();

	strlcpy( GuiVar_GroupName, trim_white_space((char*)GuiVar_GroupName), sizeof(GuiVar_GroupName) );

	// 11/22/2013 ajv : Since many screens will not make sense if the user
	// erases the entire name, don't allow them to do so. If the group name is
	// blank when they try to leave the screen, replace it with the default
	// name.
	if( strlen(GuiVar_GroupName) == 0 )
	{
		if( pDrawFunc == &FDTO_ABOUT_draw_screen )
		{
			strlcpy( GuiVar_GroupName, CONTROLLER_DEFAULT_NAME, sizeof(GuiVar_GroupName) );
		}
		else if( pDrawFunc == &FDTO_STATION_draw_menu )
		{
			strlcpy( GuiVar_GroupName, STATION_DEFAULT_DESCRIPTION, sizeof(GuiVar_GroupName) );
		}
		else if( pDrawFunc == &FDTO_MANUAL_PROGRAMS_draw_menu )
		{
			snprintf( GuiVar_GroupName, sizeof(GuiVar_GroupName), "%s %s", txtUnnamed, MANUAL_PROGRAMS_DEFAULT_NAME );
		}
		else if( pDrawFunc == &FDTO_POC_draw_menu )
		{
			snprintf( GuiVar_GroupName, sizeof(GuiVar_GroupName), "%s %s", txtUnnamed, POC_DEFAULT_NAME );
		}
		else if( pDrawFunc == &FDTO_STATION_GROUP_draw_menu )
		{
			snprintf( GuiVar_GroupName, sizeof(GuiVar_GroupName), "%s %s", txtUnnamed, STATION_GROUP_DEFAULT_NAME );
		}
		else if( pDrawFunc == &FDTO_SYSTEM_draw_menu )
		{
			snprintf( GuiVar_GroupName, sizeof(GuiVar_GroupName), "%s %s", txtUnnamed, IRRIGATION_SYSTEM_DEFAULT_NAME );
		}
		else
		{
			// 11/22/2013 ajv : In case we add another group type and forget to add a condition here,
			// set a default name.
			strlcpy( GuiVar_GroupName, "Unnamed group", sizeof(GuiVar_GroupName) );
		}
	}

	if( (pDrawFunc == &FDTO_MANUAL_PROGRAMS_draw_menu) ||
		(pDrawFunc == &FDTO_POC_draw_menu) ||
		(pDrawFunc == &FDTO_STATION_GROUP_draw_menu) ||
		(pDrawFunc == &FDTO_SYSTEM_draw_menu) )
	{
		// 6/3/2015 ajv : Since we've change the group name, make sure update the name in the list
		// of groups on the left-hand side of the screen as well.
		SCROLL_BOX_redraw_line( 0, g_GROUP_list_item_index );
	}

	hide_keyboard( pDrawFunc );
}

/* ---------------------------------------------------------- */
/**
 * Draws the selected key to the user string.
 * 
 * @executed Executed within the context of the key processing task when the 
 *  		 user presses SELECT or the asterisk key.
 * 
 * @param pkey_pressed The key that was pressed.
 * @param pgroup_name A pointer to the group name which is being edited.
 * @param pmax_length The maximum length of the group name. The default is 
 *  				  NUMBER_OF_CHARS_IN_A_NAME
 *
 * @author 5/17/2011 Adrianusv
 *
 * @revisions
 *    5/17/2011 Initial release
 */
static void process_key_press( const UNS_32 pkey_pressed,
								 char *pgroup_name,
								 const UNS_32 pmax_length )
{
	DISPLAY_EVENT_STRUCT	lde;

	UNS_32	llen;

	UNS_8	lchar;

	lchar = '\0';
	
	switch( pkey_pressed )
	{
		case CP_QWERTY_KEYBOARD_1:
			if( GuiVar_KeyboardType == KEYBOARD_TYPE_QWERTY__HEX_ONLY )
			{
				lchar = '1';
			}
			else
			{
				lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '!' : '1';
			}
			break;
		case CP_QWERTY_KEYBOARD_2:
			if( GuiVar_KeyboardType == KEYBOARD_TYPE_QWERTY__HEX_ONLY )
			{
				lchar = '2';
			}
			else
			{
				lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '@' : '2';
			}
			break;
		case CP_QWERTY_KEYBOARD_3:
			if( GuiVar_KeyboardType == KEYBOARD_TYPE_QWERTY__HEX_ONLY )
			{
				lchar = '3';
			}
			else
			{
				lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '#' : '3';
			}
			break;
		case CP_QWERTY_KEYBOARD_4:
			if( GuiVar_KeyboardType == KEYBOARD_TYPE_QWERTY__HEX_ONLY )
			{
				lchar = '4';
			}
			else
			{
				lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '$' : '4';
			}
			break;
		case CP_QWERTY_KEYBOARD_5:
			if( GuiVar_KeyboardType == KEYBOARD_TYPE_QWERTY__HEX_ONLY )
			{
				lchar = '5';
			}
			else
			{
				lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '%' : '5';
			}
			break;
		case CP_QWERTY_KEYBOARD_6:
			if( GuiVar_KeyboardType == KEYBOARD_TYPE_QWERTY__HEX_ONLY )
			{
				lchar = '6';
			}
			else
			{
				lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '^' : '6';
			}
			break;
		case CP_QWERTY_KEYBOARD_7:
			if( GuiVar_KeyboardType == KEYBOARD_TYPE_QWERTY__HEX_ONLY )
			{
				lchar = '7';
			}
			else
			{
				lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '&' : '7';
			}
			break;
		case CP_QWERTY_KEYBOARD_8:
			if( GuiVar_KeyboardType == KEYBOARD_TYPE_QWERTY__HEX_ONLY )
			{
				lchar = '8';
			}
			else
			{
				lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '*' : '8';
			}
			break;
		case CP_QWERTY_KEYBOARD_9:
			if( GuiVar_KeyboardType == KEYBOARD_TYPE_QWERTY__HEX_ONLY )
			{
				lchar = '9';
			}
			else
			{
				lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '(' : '9';
			}
			break;
		case CP_QWERTY_KEYBOARD_0:
			if( GuiVar_KeyboardType == KEYBOARD_TYPE_QWERTY__HEX_ONLY )
			{
				lchar = '0';
			}
			else
			{
				lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? ')' : '0';
			}
			break;
		case CP_QWERTY_KEYBOARD_DASH:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '_' : '-';
			break;
		case CP_QWERTY_KEYBOARD_EQUAL:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '+' : '=';
			break;
		case CP_QWERTY_KEYBOARD_Q:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'Q' : 'q';
			break;
		case CP_QWERTY_KEYBOARD_W:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'W' : 'w';
			break;
		case CP_QWERTY_KEYBOARD_E:
			if( GuiVar_KeyboardType == KEYBOARD_TYPE_QWERTY__HEX_ONLY )
			{
				lchar = 'E';
			}
			else
			{
				lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'E' : 'e';
			}
			break;
		case CP_QWERTY_KEYBOARD_R:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'R' : 'r';
			break;
		case CP_QWERTY_KEYBOARD_T:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'T' : 't';
			break;
		case CP_QWERTY_KEYBOARD_Y:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'Y' : 'y';
			break;
		case CP_QWERTY_KEYBOARD_U:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'U' : 'u';
			break;
		case CP_QWERTY_KEYBOARD_I:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'I' : 'i';
			break;
		case CP_QWERTY_KEYBOARD_O:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'O' : 'o';
			break;
		case CP_QWERTY_KEYBOARD_P:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'P' : 'p';
			break;
		case CP_QWERTY_KEYBOARD_LBRACKET:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '{' : '[';
			break;
		case CP_QWERTY_KEYBOARD_RBRACKET:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '}' : ']';
			break;
		case CP_QWERTY_KEYBOARD_A:
			if( GuiVar_KeyboardType == KEYBOARD_TYPE_QWERTY__HEX_ONLY )
			{
				lchar = 'A';
			}
			else
			{
				lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'A' : 'a';
			}
			break;
		case CP_QWERTY_KEYBOARD_S:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'S' : 's';
			break;
		case CP_QWERTY_KEYBOARD_D:
			if( GuiVar_KeyboardType == KEYBOARD_TYPE_QWERTY__HEX_ONLY )
			{
				lchar = 'D';
			}
			else
			{
				lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'D' : 'd';
			}
			break;
		case CP_QWERTY_KEYBOARD_F:
			if( GuiVar_KeyboardType == KEYBOARD_TYPE_QWERTY__HEX_ONLY )
			{
				lchar = 'F';
			}
			else
			{
				lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'F' : 'f';
			}
			break;
		case CP_QWERTY_KEYBOARD_G:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'G' : 'g';
			break;
		case CP_QWERTY_KEYBOARD_H:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'H' : 'h';
			break;
		case CP_QWERTY_KEYBOARD_J:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'J' : 'j';
			break;
		case CP_QWERTY_KEYBOARD_K:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'K' : 'k';
			break;
		case CP_QWERTY_KEYBOARD_L:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'L' : 'l';
			break;
		case CP_QWERTY_KEYBOARD_SEMICOLON:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? ':' : ';';
			break;
		case CP_QWERTY_KEYBOARD_APOS:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '"' : '\'';
			break;
		case CP_QWERTY_KEYBOARD_BACKSLASH:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '|' : '\\';
			break;
		case CP_QWERTY_KEYBOARD_Z:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'Z' : 'z';
			break;
		case CP_QWERTY_KEYBOARD_X:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'X' : 'x';
			break;
		case CP_QWERTY_KEYBOARD_C:
			if( GuiVar_KeyboardType == KEYBOARD_TYPE_QWERTY__HEX_ONLY )
			{
				lchar = 'C';
			}
			else
			{
				lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'C' : 'c';
			}
			break;
		case CP_QWERTY_KEYBOARD_V:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'V' : 'v';
			break;
		case CP_QWERTY_KEYBOARD_B:
			if( GuiVar_KeyboardType == KEYBOARD_TYPE_QWERTY__HEX_ONLY )
			{
				lchar = 'B';
			}
			else
			{
				lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'B' : 'b';
			}
			break;
		case CP_QWERTY_KEYBOARD_N:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'N' : 'n';
			break;
		case CP_QWERTY_KEYBOARD_M:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? 'M' : 'm';
			break;
		case CP_QWERTY_KEYBOARD_COMMA:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '<' : ',';
			break;
		case CP_QWERTY_KEYBOARD_PERIOD:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '>' : '.';
			break;
		case CP_QWERTY_KEYBOARD_SLASH:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '?' : '/';
			break;
		case CP_QWERTY_KEYBOARD_ACCENT:
			lchar = ( GuiVar_KeyboardShiftEnabled == (true) ) ? '~' : '`';
			break;
		case CP_QWERTY_KEYBOARD_SHIFT:
			good_key_beep();
			GuiVar_KeyboardShiftEnabled = !GuiVar_KeyboardShiftEnabled;
			break;
		case CP_QWERTY_KEYBOARD_SPACE:
			lchar = ' ';
			break;
		case CP_QWERTY_KEYBOARD_BACKSPACE:
			llen = strlen( pgroup_name );

			if( llen > 0 )
			{
				good_key_beep();

				pgroup_name[ (llen - 1) ] = '\0';

				// Automatically enable the Shift key if the user has deleted
				// the last character of the text.
				if( (llen - 1) == 0 )
				{
					GuiVar_KeyboardShiftEnabled = (true);
				}
			}
			else
			{
				bad_key_beep();
			}
			break;
	}

	// Concatenate the new character to the string and sound a good key beep if
	// it was done, or a bad key beep if the length was already at the max.
	if( (pkey_pressed == CP_QWERTY_KEYBOARD_SHIFT) || (pkey_pressed == CP_QWERTY_KEYBOARD_BACKSPACE) )
	{
		// Don't do anything. We've already handled the good and bad_key_beep.
	}
	else
	{
		llen = strlen( pgroup_name );

		if( llen < pmax_length )
		{
			good_key_beep();

			pgroup_name[ llen ] = lchar;

			// NULL terminate the string if there's room to do so.
			if( (llen + 1) < pmax_length )
			{
				pgroup_name[ (llen + 1) ] = '\0';
			}
		}
		else
		{
			bad_key_beep();
		}

		if( GuiVar_KeyboardShiftEnabled == (true) )
		{
			/// Reset the shift key. It's not being used as an Caps Lock button.
			GuiVar_KeyboardShiftEnabled = (false);
		}
	}

	// 6/15/2015 ajv : Always force a full redraw to ensure the variable and keyboard all update
	// properly.
	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void*)&FDTO_show_keyboard;
	lde._06_u32_argument1 = (false);
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
/**
 * Draws the on-screen keyboard. By default, the SHIFT key is disabled until the 
 * user either selects it, presses *, or erases all of the text.
 * 
 * @executed Executed within the context of the display processing task when
 *  		 the user tries to edit a string.
 * 
 * @author 5/17/2011 Adrianusv
 *
 * @revisions
 *    5/17/2011 Initial release
 */
static void FDTO_show_keyboard( const BOOL_32 pcomplete_redraw )
{
	if( pcomplete_redraw == (true) )
	{
		GuiVar_KeyboardShiftEnabled = (false);

		GuiLib_ActiveCursorFieldNo = CP_QWERTY_KEYBOARD_OK;
	}

	GuiLib_ShowScreen( GuiStruct_dlgKeyboard_0, GuiLib_ActiveCursorFieldNo, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Hides the on-screen keyboard by calling the passed in draw function. The 
 * complete_redraw paramater of the draw function is set to false. 
 * 
 * @executed Executed within the context of the key processing task when the
 *           user presses BACK when viewing the keyboard.
 * 
 * @param pDrawFunc A pointer to the draw function of the screen that's being
 *                  viewed.
 *
 * @author 6/9/2011 Adrianusv
 *
 * @revisions
 *    6/9/2011 Initial release
 */
static void hide_keyboard( void(*pDrawFunc)(const BOOL_32 pcomplete_redraw) )
{
	DISPLAY_EVENT_STRUCT	lde;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void*)pDrawFunc;
	lde._06_u32_argument1 = (false);

	GuiLib_ActiveCursorFieldNo = (INT_32)g_KEYBOARD_cursor_position_when_keyboard_displayed;

	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
/**
 * Draws the on-screen keyboard at the specified x and y coordinates. If the
 * language is Spanish, the keyboard's cursor position is shifted 10 pixels to
 * the right to accomodate the longer prompt.
 * 
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user tries to edit a group name.
 * 
 * @param px_coordinate The x-coordinate of location to display the cursor
 * position where the user will type their text. The keyboard will be displayed
 * below this cursor.
 * 
 * @param py_coordinate The y-coordinate of location to display the cursor
 * position where the user will type their text. The keyboard will be displayed
 * below this cursor.
 *
 * @author 6/9/2011 Adrianusv
 *
 * @revisions (none)
 */
extern void KEYBOARD_draw_keyboard( const UNS_32 px_coordinate, const UNS_32 py_coordinate, const UNS_32 pmax_string_len, const UNS_32 pkeyboard_type )
{
	DISPLAY_EVENT_STRUCT	lde;

	GuiVar_KeyboardStartX = px_coordinate;
	GuiVar_KeyboardStartY = py_coordinate;

	GuiVar_KeyboardType = pkeyboard_type;

	g_KEYBOARD_max_string_length = pmax_string_len;

	g_KEYBOARD_cursor_position_when_keyboard_displayed = (UNS_32)GuiLib_ActiveCursorFieldNo;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void *)&FDTO_show_keyboard;
	lde._06_u32_argument1 = (true);

	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
/**
 * Processes the key event when the on-screen keyboard is visible.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 * @param pDrawFunc A pointer to the draw function of the screen that's being
 *                  viewed.
 *
 * @author 1/24/2012 Adrianusv
 *
 * @revisions
 *    1/24/2012 Initial release
 */
extern void KEYBOARD_process_key( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event,
								  void(*pDrawFunc)(const BOOL_32 pcomplete_redraw) )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			if( GuiLib_ActiveCursorFieldNo == CP_QWERTY_KEYBOARD_OK )
			{
				process_OK_button( pDrawFunc );
			}
			else
			{
				process_key_press( (UNS_32)GuiLib_ActiveCursorFieldNo, GuiVar_GroupName, g_KEYBOARD_max_string_length );
			}
			break;

		case KEY_C_UP:
			process_up_arrow();
			break;

		case KEY_C_DOWN:
			process_down_arrow();
			break;

		case KEY_C_LEFT:
			process_left_arrow();
			break;

		case KEY_C_RIGHT:
			process_right_arrow();
			break;

		// 2012.01.17 ajv - Enable the shift key when the user presses PLUS
		case KEY_PLUS:
			if( GuiVar_KeyboardType != KEYBOARD_TYPE_QWERTY__HEX_ONLY )
			{
				if( GuiVar_KeyboardShiftEnabled == (false) )
				{
					good_key_beep();
					GuiVar_KeyboardShiftEnabled = !GuiVar_KeyboardShiftEnabled;

					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._04_func_ptr = (void*)&FDTO_show_keyboard;
					lde._06_u32_argument1 = (false);
					Display_Post_Command( &lde );
				}
				else
				{
					bad_key_beep();
				}
			}
			else
			{
				 bad_key_beep();
			}
			break;

		// 2012.01.17 ajv - Disable the shift key when the user presses MINUS
		case KEY_MINUS:
			if( GuiVar_KeyboardType != KEYBOARD_TYPE_QWERTY__HEX_ONLY )
			{
				if( GuiVar_KeyboardShiftEnabled == (true) )
				{
					good_key_beep();
					GuiVar_KeyboardShiftEnabled = !GuiVar_KeyboardShiftEnabled;

					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._04_func_ptr = (void*)&FDTO_show_keyboard;
					lde._06_u32_argument1 = (false);
					Display_Post_Command( &lde );
				}
				else
				{
					bad_key_beep();
				}
			}
			else
			{
				bad_key_beep();
			}
			break;

		case KEY_BACK:
			process_OK_button( pDrawFunc );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

