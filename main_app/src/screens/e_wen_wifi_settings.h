/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_E_WEN_WIFI_SETTINGS_H
#define _INC_E_WEN_WIFI_SETTINGS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/29/2016 ajv : These are required to be EXTERN so we can access them from the
// device_WEN_WiBox file.
#define WEN_WIRELESS_SECURITY_NONE    (0)
#define WEN_WIRELESS_SECURITY_WEP     (1)
#define WEN_WIRELESS_SECURITY_WPA     (2)
#define WEN_WIRELESS_SECURITY_WPA2    (3)

#define WEN_WEP_AUTHENTICATION_OPEN   (0)
#define WEN_WEP_AUTHENTICATION_SHARED (1)

#define WEN_KEY_SIZE_40				  (0)
#define WEN_KEY_SIZE_104			  (1)

#define WEN_KEY_TYPE_HEX			  (0)
#define WEN_KEY_TYPE_PASS			  (1)

#define	WEN_WPA_ENCRYPTION_TKIP		  (0)
#define	WEN_WPA_ENCRYPTION_TKIP_WEP	  (1)

#define	WEN_WPA2_ENCRYPTION_CCMP		  (0)
#define	WEN_WPA2_ENCRYPTION_CCMP_TKIP	  (1)
#define	WEN_WPA2_ENCRYPTION_CCMP_WEP	  (2)
#define	WEN_WPA2_ENCRYPTION_TKIP		  (3)
#define	WEN_WPA2_ENCRYPTION_TKIP_WEP	  (4)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void set_wen_programming_struct_from_wifi_guivars();

extern void get_wifi_guivars_from_wen_programming_struct();

extern BOOL_32 g_WEN_PROGRAMMING_wifi_values_in_range();

extern void g_WEN_PROGRAMMING_initialize_wifi_guivars();

extern void FDTO_WEN_PROGRAMMING_draw_wifi_settings( const BOOL_32 pcomplete_redraw );

extern void WEN_PROGRAMMING_process_wifi_settings( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

