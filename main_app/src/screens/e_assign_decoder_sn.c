/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_assign_decoder_sn.h"

#include	"app_startup.h"

#include	"change.h"

#include	"comm_mngr.h"

#include	"configuration_controller.h"

#include	"cursor_utils.h"

#include	"flash_storage.h"

#include	"irri_comm.h"

#include	"m_main.h"

#include	"r_alerts.h"

#include	"screen_utils.h"

#include	"speaker.h"

#include	"stations.h"

#include	"tpmicro_data.h"

#include	"two_wire_utils.h"

#include	"flowsense.h"

#include	"d_two_wire_debug.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_TWO_WIRE_DECODER_SN			(0)
#define	CP_TWO_WIRE_ASSIGN_SN			(1)
#define CP_TWO_WIRE_PERFORM_DISCOVERY	(2)
#define	CP_TWO_WIRE_TEST_DECODER		(3)

/* ---------------------------------------------------------- */

// 10/3/2013 ajv : Store the last cursor position so when the user returns from
// the TP Micro Alerts screen, they are automatically on the next cursor
// position.
static UNS_32 g_TWO_WIRE_last_cursor_pos;

/* ---------------------------------------------------------- */
/**
 * Draws the TP Micro Alerts screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user presses <b>Assign</b>, <b>Perform Discovery</b>, or <b>Test
 * Decoder</b>.
 *
 * @author 10/3/2013 AdrianusV
 *
 * @revisions (none)
 */
static void TWO_WIRE_jump_to_TP_Micro_alerts( void )
{
	DISPLAY_EVENT_STRUCT	lde;

	ScreenHistory[ screen_history_index ]._08_screen_to_draw = GuiVar_MenuScreenToShow;

	GuiVar_MenuScreenToShow = CP_TECH_SUPPORT_MENU_TP_MICRO_ALERTS;

	g_ALERTS_pile_to_show = ALERTS_PILE_TP_MICRO;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._02_menu = SCREEN_MENU_DIAGNOSTICS;
	lde._03_structure_to_draw = GuiStruct_rptAlerts_0;
	lde._04_func_ptr = (void *)&FDTO_ALERTS_draw_alerts;
	lde._06_u32_argument1 = (true);
	lde._08_screen_to_draw = CP_TECH_SUPPORT_MENU_TP_MICRO_ALERTS;
	lde.key_process_func_ptr = ALERTS_process_report;
	Change_Screen( &lde );
}

/* ---------------------------------------------------------- */
/**
 * Sets variables to trigger the TP Micro to assign the specified serial number
 * to the first available decoder with no serial number.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user presses the <b>Apply</b> button on the Assign Decoder Serial Number
 * screen.
 * 
 * @param pdecoder_sn A pointer to the decoder serial number to assign. This is
 * a pointer because the value is automatically incremented after the user
 * presses <b>Apply</b> to expedite the process of assigning serial numbers to
 * multiple decoders.
 *
 * @author 6/17/2013 Adrianusv
 *
 * @revisions
 *  6/18/2013 ajv : Added incrementing serial number and refreshing the screen
 *  to reflect the change.
 * 
 *  10/2/2013 ajv : Removed incrementing of serial number.
 *                  Added saving of last assigned serial number into FLASH.
 */
static void TWO_WIRE_assign_decoder_sn( UNS_32 *const pdecoder_sn )
{
	// 1/29/2013 rmd : If the user keeps on pressing he'll get the bad beep
	// until the message has been sent to the TPMicro.
	if( tpmicro_data.two_wire_set_decoder_sn == (false) )
	{
		tpmicro_data.two_wire_set_decoder_sn = (true);
		
		tpmicro_data.sn_to_set = *pdecoder_sn;

		// 10/3/2013 ajv : Store the last assigned serial number into the
		// configuration file to retain it through a power fail.
		config_c.last_assigned_decoder_serial_number = GuiVar_TwoWireDecoderSNToAssign;

		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_CONTROLLER_CONFIGURATION, 10 );

		good_key_beep();
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/**
 * Draws the Assign Decoder Serial Number screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task.
 * 
 * @param pcomplete_redraw True if the screen is being entered for the first
 * time; otherwise, false to retain the existing cursor position.
 *
 * @author 6/17/2013 Adrianusv
 *
 * @revisions
 *   10/3/2013 ajv : Modified default cursor position to start with to the last
 *   cursor position. On an initial power-up, this will default to the serial
 *   number field.
 */
extern void FDTO_TWO_WIRE_draw_assign_sn( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		// 10/3/2013 ajv : Normally, we'd force the cursor to an explicit
		// location. However, to retain the position when running through the
		// process, we're going to use a global. The first time the screen is
		// entered, it's initialized to a 0, which results in the cursor being
		// on the serial number.
		lcursor_to_select = g_TWO_WIRE_last_cursor_pos;

		GuiVar_TwoWireDecoderSNToAssign = config_c.last_assigned_decoder_serial_number;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrTwoWireAssignSN_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while the user is on the Assign Decoder Serial
 * Number screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user pressess a key.
 * 
 * @param pkey_event The key event to process.
 *
 * @author 6/17/2013 Adrianusv
 *
 * @revisions
 *  6/18/2013 ajv : Prevented the user from being able to assign a serial number
 *  less than the minimum of 0x400.
 * 
 *  10/2/2013 ajv : Added processing of the <b>Test Decoder</b> button which
 *  automatically assigns Output A and B to Station T1 and T2 respectively, and
 *  turns them on for two minutes.
 * 
 *  10/3/2013 ajv : Added processing of the <b>Perform Discovery</b> button
 *  which initiates a discovery process.
 */
extern void TWO_WIRE_process_assign_sn( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	STATION_STRUCT		*lstation;

	CHANGE_BITS_32_BIT	*lbitfield_of_changes;

	UNS_32	lbox_index_0;

	UNS_32	i, j;
	
	UNS_32	linc_by;

	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TWO_WIRE_ASSIGN_SN:
					// 6/18/2013 ajv : Don't allow the user to assign a serial
					// number less than the minimum of 0x400.
					if( GuiVar_TwoWireDecoderSNToAssign >= 0x400 )
					{
						TWO_WIRE_assign_decoder_sn( &GuiVar_TwoWireDecoderSNToAssign );

						// 10/3/2013 ajv : Set the last cursor position to the
						// next cursor field so that the user automatically
						// starts at the next command when they return from the
						// TP Micro Alerts.
						g_TWO_WIRE_last_cursor_pos = (UNS_32)(GuiLib_ActiveCursorFieldNo + 1);

						TWO_WIRE_jump_to_TP_Micro_alerts();
					}
					else
					{
						bad_key_beep();
					}
					break;

				case CP_TWO_WIRE_PERFORM_DISCOVERY:
					TWO_WIRE_perform_discovery_process( (false), (true) );

					// 10/3/2013 ajv : Set the last cursor position to the
					// next cursor field so that the user automatically
					// starts at the next command when they return from the
					// TP Micro Alerts.
					g_TWO_WIRE_last_cursor_pos = (UNS_32)(GuiLib_ActiveCursorFieldNo + 1);

					TWO_WIRE_jump_to_TP_Micro_alerts();
					break;

				case CP_TWO_WIRE_TEST_DECODER:
					good_key_beep();

					// ----------
					
					// 7/7/2014 rmd : This station creation is only for initial decoder bring up. And is not
					// intended to ever be used by an end user. In-house test use only. We create the station
					// and then test both outputs. As an aid to the testor. So he doesn't manually have to make
					// the station assignments. Code designed for a standalone when it comes to those delays
					// waiting for the token.
					
					// ----------

					// 10/2/2013 ajv : First, assign the decoder output and
					// serial number to each station. Since there are two
					// outputs per decoder, we loop through this for each
					// output. Station T1 and T2 are reused each time the user
					// presses the Test Decoder button.
					for( i = DECODER_OUTPUT_A_BLACK; i <= DECODER_OUTPUT_B_ORANGE; ++i )
					{
						xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

						lstation = nm_STATION_get_pointer_to_station( FLOWSENSE_get_controller_index(), (MAX_CONVENTIONAL_STATIONS + i) );

						lbox_index_0 = FLOWSENSE_get_controller_index();

						// 10/2/2013 ajv : If the station doesn't exist yet,
						// create the station so it can be used. This should
						// only happen once since we reuse station T1 and T2.
						if( lstation == NULL )
						{
							lstation = nm_STATION_create_new_station( lbox_index_0, (MAX_CONVENTIONAL_STATIONS + i), CHANGE_REASON_KEYPAD );
						}

						lbitfield_of_changes = STATION_get_change_bits_ptr( lstation, CHANGE_REASON_KEYPAD );

						nm_STATION_set_physically_available( lstation, CHANGE_generate_change_line, (false), CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, lbox_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
						nm_STATION_set_decoder_serial_number( lstation, GuiVar_TwoWireDecoderSNToAssign, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, lbox_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
						nm_STATION_set_decoder_output( lstation, (DECODER_OUTPUT_A_BLACK + i), CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, lbox_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

						xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
					}

					// 10/2/2013 ajv : Wait for token response to be sent to the master. Delay for 1 second
					// (1000 ms) or until changes_made_on_this_controller is false, whichever comes first. This
					// ensures the changes are sent to the FOAL machine prior to trying to turning on the
					// stations.
					for( j = 0; (comm_mngr.changes.send_changes_to_master) == (true) && (j < 100); ++j )
					{
						vTaskDelay( MS_to_TICKS( 10 ) );
					}

					// 10/2/2013 ajv : Finally, initiate the test. Since we can
					// only issue a single test request per token, once we've
					// initiated the process, pause until the IRRI machine
					// reports that it's sent the request to the FOAL machine.
					for( i = DECODER_OUTPUT_A_BLACK; i <= DECODER_OUTPUT_B_ORANGE; ++i )
					{
						irri_comm.test_request.box_index_0 = FLOWSENSE_get_controller_index();
						irri_comm.test_request.station_number = (MAX_CONVENTIONAL_STATIONS + i);
						irri_comm.test_request.set_expected = (false);
						// 10/15/2014 rmd : I've decided that a ONE minute test is sufficient. The decoder circuit
						// for the most part is either going to energize the solenoid or not. Duration beyond that
						// initial turn-on will produce few results. If any. In my opinion at the moment.
						irri_comm.test_request.time_seconds = 60;
						irri_comm.test_request.in_use = (true);

						// 10/2/2013 ajv : Wait for token response to be sent to the master. Delay for 1 second
						// (1000 ms) or until test_request.in_use is false, whichever comes first.
						for( j = 0; (irri_comm.test_request.in_use) == (true) && (j < 100); ++j )
						{
							vTaskDelay( MS_to_TICKS( 10 ) );
						}
					}

					// 10/3/2013 ajv : Set the last cursor position to the first
					// cursor field so that the user automatically starts at the
					// serial number when they return from the TP Micro Alerts.
					g_TWO_WIRE_last_cursor_pos = CP_TWO_WIRE_DECODER_SN;

					// 10/2/2013 ajv : Finally, now that the FOAL is starting
					// the process of turning both stations on, jump to the TP
					// Micro alerts so the user can verify that no shorts occur.
					TWO_WIRE_jump_to_TP_Micro_alerts();
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_PLUS:
		case KEY_MINUS:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_TWO_WIRE_DECODER_SN:
				
					// 7/27/2018 Ryan : Value of each pkey_event increases depending on how long the user holds
					// down the key.
					if( pkey_event.repeats > 100 )
					{
						linc_by = 25;
					}
					else if( pkey_event.repeats > 60 )
					{
						linc_by = 10;
					}
					else if( pkey_event.repeats > 30 )
					{
						linc_by = 5;
					}
					else
					{
						linc_by = 1;
					}
					
					process_uns32( pkey_event.keycode, &GuiVar_TwoWireDecoderSNToAssign, 0x400, DECODER_SERIAL_NUM_MAX, linc_by, (false) );
					Refresh_Screen();
					break;

				default:
					bad_key_beep();
			}
			break;

		case KEY_C_UP:
			CURSOR_Up( (true) );
			break;

		case KEY_C_DOWN:
			CURSOR_Down( (true) );
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;

			KEY_process_global_keys( pkey_event );

			// 6/5/2015 ajv : Since the user got here from the 2-WIRE DEBUG dialog box, redraw that
			// dialog box.
			TWO_WIRE_DEBUG_draw_dialog( (true) );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

