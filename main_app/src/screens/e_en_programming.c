/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/23/2015 mpd : Required for atoi
#include	<stdlib.h>

// 4/21/2015 ajv : Required for strlcpy.
#include	<string.h>

#include	"device_common.h"

#include	"e_en_programming.h"

#include	"combobox.h"

#include	"configuration_controller.h"

#include	"cursor_utils.h"

#include	"dialog.h"

#include	"d_comm_options.h"

#include	"d_device_exchange.h"

#include	"d_process.h"

#include	"e_keyboard.h"

#include	"group_base_file.h"

#include	"speaker.h"

#include	"device_EN_UDS1100.h"

#include	"comm_mngr.h"

#include	"app_startup.h"

#include	"alerts.h"

//#include 	"dialog.h"


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_EN_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY	(0)
#define	CP_EN_PROGRAMMING_DHCP_NAME					(1)
#define	CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_1		(2)
#define	CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_2		(3)
#define	CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_3		(4)
#define	CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_4		(5)
#define	CP_EN_PROGRAMMING_SUBNET_MASK				(6)
#define	CP_EN_PROGRAMMING_GATEWAY_OCTET_1			(7)
#define	CP_EN_PROGRAMMING_GATEWAY_OCTET_2			(8)
#define	CP_EN_PROGRAMMING_GATEWAY_OCTET_3			(9)
#define	CP_EN_PROGRAMMING_GATEWAY_OCTET_4			(10)
#define	CP_EN_PROGRAMMING_READ_DEVICE				(11)
#define	CP_EN_PROGRAMMING_PROGRAM_DEVICE			(12)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static BOOL_32 EN_PROGRAMMING_querying_device;

static BOOL_32 EN_PROGRAMMING_setup_mode_read_pending = false;

// 6/9/2015 mpd : MONITOR MODE is moving to the diagnostic screen.
//static BOOL_32 EN_PROGRAMMING_monitor_mode_read_pending = false;

// 4/22/2015 ajv : Since only the draw fuction is initially told which port the device is
// on, copy that port into a global variable that's accessible from the key processing task.
static UNS_32	g_EN_PROGRAMMING_port;

static UNS_32	g_EN_PROGRAMMING_previous_cursor_pos;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void set_en_programming_struct_from_guivars()
{
	EN_DETAILS_STRUCT *en_details;
	EN_PROGRAMMABLE_VALUES_STRUCT *active_pvs;

	//Alert_Message( "set_en_programming_struct_from_guivars" );

	if( dev_state != NULL )
	{
		if( ( dev_state->en_details != NULL ) && ( dev_state->en_active_pvs != NULL ) )
		{
			en_details  = dev_state->en_details;
			active_pvs = dev_state->en_active_pvs;

			// 4/27/2015 mpd : Calsense recommends using the MAC address as the DHCP name. If the user has not set a value 
			// in the name field, use the MAC address. Don't override any string value other than "not set".
			if( ( strncmp( GuiVar_ENDHCPName, EN_BASIC_INFO_NAME_NOT_SET_STR, EN_BASIC_INFO_NAME_NOT_SET_LEN ) == NULL ) &&
				( en_details->dhcp_name_not_set ) )
			{
				strlcpy( active_pvs->dhcp_name, en_details->mac_address, sizeof( active_pvs->dhcp_name ) );
			}
			else
			{
				strlcpy( active_pvs->dhcp_name, GuiVar_ENDHCPName, sizeof( active_pvs->dhcp_name ) );
			}

			// 4/22/2015 mpd : This is a read only field. Don't bother sending to the programming code. 
			//strlcpy( dev_state->sw_version, GuiVar_ENFirmwareVer, sizeof( dev_state->sw_version ) );

			// 4/22/2015 mpd : This is a read only field. Don't bother sending to the programming code. 
			//strlcpy( dev_state->mac_address, GuiVar_ENMACAddress, sizeof( dev_state->mac_address ) );

			// 4/22/2015 mpd : This is a read only field. Don't bother sending to the programming code. 
			//strlcpy( dev_state->model, GuiVar_ENModel, sizeof( dev_state->model ) );

			// 9/24/2015 mpd : The UDS1100 will not return a mask field in the response if the mask is 
			// "255.255.255.255". Since this is useless anyway, don't allow processing to continue with this value. 
			// Force it to something slightly less useless here since modifying the pop up menu in EasyGUI would be a 
			// pain for very little gain.
			if( GuiVar_ENNetmask == 0 )
			{
				GuiVar_ENNetmask = 1;
			}

			// 9/23/2015 mpd : FogBugz #3164: The Lantronix EN UDS1100 does not use CIDR (Classless Inter Domain Routing) format
			// to define the network mask. The number of bits indicates the available bits for host addressing (e.g. A mask of 
			// "8" produces a netmask of "255.255.255.0" which has 8 bits available for hosts on the subnet. Other Lantronix
			// devices use CIDR format for programming (e.g. "192.168.254.150/24" specifies the IP address as well as a network 
			// mask of "255.255.255.0"). The CIDR mask bit value indicates the leading ones; not the trailing zeros.
			// The CS3000 GUI code was written for UDS1100. It's backwards for all newer Lantronix devices. The mask numbers are 
			// internal to CS3000 code. They become external at the GUI and in device programming. To eliminate extensive 
			// rework, the GUI side for all device types will retain pre-CIDR mask values. As mask values pass to and from the 
			// GUI code to the device code (e.g. "e_wen_programming.c" to "device_WEN_PremierWaveXN.c"), the value will be 
			// transformed to match the receiving format.  
			// 4/22/2015 mpd : This is a direct copy of UNS_32 values.
			en_details->mask_bits = GuiVar_ENNetmask;

			// 9/24/2015 mpd : EasyGUI currently uses "GuiVar_ENNetmask", but is leaving "GuiVar_ENSubnetMask" all NULL.
			// Post write verification will have to be scrapped. 
			// 9/24/2015 mpd : FogBugz #3178: Device programming is done using "en_details->mask_bits", but verification
			// after the write operation checks this string. We don't care about the individual octets.
			//strlcpy( active_pvs->mask, GuiVar_ENSubnetMask, sizeof( active_pvs->mask ) );

			// 5/21/2015 mpd : Zeros in octets 1 and 2 signal the device to use dynamic IP addressing. If the user has 
			// specified zeros, force the DHCP indicator to "Yes". This may override the user's choice. 
			if( ( GuiVar_ENIPAddress_1 == 0 ) && ( GuiVar_ENIPAddress_2 == 0 ) )
			{
				GuiVar_ENObtainIPAutomatically = true;
			}

			// 4/22/2015 mpd : The programming side expects a BOOL_32. Make a safe conversion. 
			if( GuiVar_ENObtainIPAutomatically )
			{
				dev_state->dhcp_enabled = true;

				// 5/21/2015 mpd : Octet 3 is a bit map when octets 1 and 2 are zero. BOOTP = 0x4, DHCP = 0x2, AutoIP - 0x1.
				// A "1" in any bit turns that feature OFF. We want DHCP and AutoIP enabled. BOOTP disabled.
				snprintf( active_pvs->ip_address,   sizeof(active_pvs->ip_address),   "%d.%d.%d.%d", 0, 0, 0, 0 );
				snprintf( active_pvs->ip_address_1, sizeof(active_pvs->ip_address_1), "%d", 0 );
				snprintf( active_pvs->ip_address_2, sizeof(active_pvs->ip_address_2), "%d", 0 );
				snprintf( active_pvs->ip_address_3, sizeof(active_pvs->ip_address_3), "%d", 4 );
				snprintf( active_pvs->ip_address_4, sizeof(active_pvs->ip_address_4), "%d", 0 );

				snprintf( active_pvs->gw_address,   sizeof(active_pvs->gw_address),   "%d.%d.%d.%d", 0, 0, 0, 0 );
				snprintf( active_pvs->gw_address_1, sizeof(active_pvs->gw_address_1), "%d", 0 );
				snprintf( active_pvs->gw_address_2, sizeof(active_pvs->gw_address_2), "%d", 0 );
				snprintf( active_pvs->gw_address_3, sizeof(active_pvs->gw_address_3), "%d", 0 );
				snprintf( active_pvs->gw_address_4, sizeof(active_pvs->gw_address_4), "%d", 0 );
			}
			else
			{
				dev_state->dhcp_enabled = false;

				// 4/28/2015 mpd : Individual IP address octets are used to actually program the device. The 4 octet version
				// is used for decision making. Don't skip setting it's value! 
				snprintf( active_pvs->ip_address,   sizeof(active_pvs->ip_address),   "%d.%d.%d.%d", GuiVar_ENIPAddress_1, GuiVar_ENIPAddress_2, GuiVar_ENIPAddress_3, GuiVar_ENIPAddress_4 );
				snprintf( active_pvs->ip_address_1, sizeof(active_pvs->ip_address_1), "%d", GuiVar_ENIPAddress_1 );
				snprintf( active_pvs->ip_address_2, sizeof(active_pvs->ip_address_2), "%d", GuiVar_ENIPAddress_2 );
				snprintf( active_pvs->ip_address_3, sizeof(active_pvs->ip_address_3), "%d", GuiVar_ENIPAddress_3 );
				snprintf( active_pvs->ip_address_4, sizeof(active_pvs->ip_address_4), "%d", GuiVar_ENIPAddress_4 );

				snprintf( active_pvs->gw_address,   sizeof(active_pvs->gw_address),   "%d.%d.%d.%d", GuiVar_ENGateway_1, GuiVar_ENGateway_2, GuiVar_ENGateway_3, GuiVar_ENGateway_4 );
				snprintf( active_pvs->gw_address_1, sizeof(active_pvs->gw_address_1), "%d", GuiVar_ENGateway_1 );
				snprintf( active_pvs->gw_address_2, sizeof(active_pvs->gw_address_2), "%d", GuiVar_ENGateway_2 );
				snprintf( active_pvs->gw_address_3, sizeof(active_pvs->gw_address_3), "%d", GuiVar_ENGateway_3 );
				snprintf( active_pvs->gw_address_4, sizeof(active_pvs->gw_address_4), "%d", GuiVar_ENGateway_4 );
			}
		}
	}

}

/* ---------------------------------------------------------- */
static void get_guivars_from_en_programming_struct()
{
	EN_DETAILS_STRUCT *en_details;
	EN_PROGRAMMABLE_VALUES_STRUCT *active_pvs;

	//Alert_Message( "get_guivars_from_en_programming_struct" );

	if( dev_state != NULL )
	{
		if( ( dev_state->en_details != NULL ) && ( dev_state->en_active_pvs != NULL ) )
		{
			en_details = dev_state->en_details;
			active_pvs = dev_state->en_active_pvs;

			// 4/9/2015 mpd : We can't get good values from the structure if no READ operation has ever been completed (reboot or new radio).  
			//if( dev_state->device_settings_are_valid )
			if( 1 )
			{
				if( en_details->dhcp_name_not_set )
				{
					GuiVar_ENDHCPNameNotSet = true;
					strlcpy( GuiVar_ENDHCPName, EN_BASIC_INFO_NAME_NOT_SET_STR, sizeof( GuiVar_ENDHCPName ) );
				}
				else
				{
					GuiVar_ENDHCPNameNotSet = false;
					strlcpy( GuiVar_ENDHCPName, active_pvs->dhcp_name, sizeof( GuiVar_ENDHCPName ) );
				}

				// 4/27/2015 mpd : The EN programming code includes CR and LF in strings that are sent directly to the Lantronix 
				// device. Those characters add ugly black boxes to the GUI fields. Get rid of them.  
				strtok( GuiVar_ENDHCPName, "\r\n" );

				// 6/9/2015 mpd : All device types now use "dev_state->firmware_version".
				strlcpy( GuiVar_ENFirmwareVer, dev_state->firmware_version, sizeof( GuiVar_ENFirmwareVer ) );
				if( strlen( GuiVar_ENFirmwareVer ) == 0 )
				{
					strlcpy( GuiVar_ENFirmwareVer, "unknown", sizeof( GuiVar_ENFirmwareVer ) );
				}

				// 4/28/2015 mpd : Use the programming code boolean for this decision. That's how the device is configured.
				if ( !dev_state->dhcp_enabled )
				{
					GuiVar_ENObtainIPAutomatically = false;
				}
				else
				{
					GuiVar_ENObtainIPAutomatically = true;
				}

				// 5/1/2015 mpd : MONITOR MODE reads are now done for DHCP enablen and disabled. They are also done at the 
				// end of WRITE operations to get the latest network values.
				//GuiVar_ENGateway_1 = atoi( dev_state->mon_gw_address_1 );
				//GuiVar_ENGateway_2 = atoi( dev_state->mon_gw_address_2 );
				//GuiVar_ENGateway_3 = atoi( dev_state->mon_gw_address_3 );
				//GuiVar_ENGateway_4 = atoi( dev_state->mon_gw_address_4 );

				//GuiVar_ENIPAddress_1 = atoi( dev_state->mon_ip_address_1 );
				//GuiVar_ENIPAddress_2 = atoi( dev_state->mon_ip_address_2 );
				//GuiVar_ENIPAddress_3 = atoi( dev_state->mon_ip_address_3 );
				//GuiVar_ENIPAddress_4 = atoi( dev_state->mon_ip_address_4 );

				// 5/20/2015 mpd : MONITOR MODE output has been moved to the diagnistics screen. IP and gateway values
				// have been removed from the DHCP enabled version of this screen. Populate the GuiVars with PVS values.
				GuiVar_ENGateway_1 = atoi( active_pvs->gw_address_1 );
				GuiVar_ENGateway_2 = atoi( active_pvs->gw_address_2 );
				GuiVar_ENGateway_3 = atoi( active_pvs->gw_address_3 );
				GuiVar_ENGateway_4 = atoi( active_pvs->gw_address_4 );

				GuiVar_ENIPAddress_1 = atoi( active_pvs->ip_address_1 );
				GuiVar_ENIPAddress_2 = atoi( active_pvs->ip_address_2 );
				GuiVar_ENIPAddress_3 = atoi( active_pvs->ip_address_3 );
				GuiVar_ENIPAddress_4 = atoi( active_pvs->ip_address_4 );

				// 6/9/2015 mpd : TBD: This field may not be valid if DHCP is enabled. 
				//strlcpy( GuiVar_ENSubnetMask, active_pvs->mask, sizeof(GuiVar_ENSubnetMask) );
				//if( strlen( GuiVar_ENSubnetMask ) == 0 )
				//{
				//	strlcpy( GuiVar_ENSubnetMask, "unknown", sizeof(GuiVar_ENSubnetMask) );
				//}

				strlcpy( GuiVar_ENMACAddress, en_details->mac_address, sizeof( GuiVar_ENMACAddress ) );
				if( strlen( GuiVar_ENMACAddress ) == 0 )
				{
					strlcpy( GuiVar_ENMACAddress, "unknown", sizeof( GuiVar_ENMACAddress ) );
				}

				// 4/30/2015 mpd : The Lantronix model response is a long string that looks like:
				// "Device Server Plus+! (Firmware Code:UA)". That's really not what we want to see.
				// The "Model" string we want is "UDS1100-IAP". It will be called "Device" to
				// differentiate it from the Lantronix "Model" string.

				//strlcpy( GuiVar_ENModel, dev_state->model, sizeof( GuiVar_ENModel ) );
				strlcpy( GuiVar_ENModel, en_details->device, sizeof( GuiVar_ENModel ) );
				if( strlen( GuiVar_ENModel ) == 0 )
				{
					strlcpy( GuiVar_ENModel, "unknown", sizeof( GuiVar_ENModel ) );
				}

				// 9/23/2015 mpd : FogBugz #3164: The Lantronix EN UDS1100 does not use CIDR (Classless Inter Domain Routing) format
				// to define the network mask. The number of bits indicates the available bits for host addressing (e.g. A mask of 
				// "8" produces a netmask of "255.255.255.0" which has 8 bits available for hosts on the subnet. Other Lantronix
				// devices use CIDR format for programming (e.g. "192.168.254.150/24" specifies the IP address as well as a network 
				// mask of "255.255.255.0"). The CIDR mask bit value indicates the leading ones; not the trailing zeros.
				// The CS3000 GUI code was written for UDS1100. It's backwards for all newer Lantronix devices. The mask numbers are 
				// internal to CS3000 code. They become external at the GUI and in device programming. To eliminate extensive 
				// rework, the GUI side for all device types will retain pre-CIDR mask values. As mask values pass to and from the 
				// GUI code to the device code (e.g. "e_wen_programming.c" to "device_WEN_PremierWaveXN.c"), the value will be 
				// transformed to match the receiving format.  
				GuiVar_ENNetmask = en_details->mask_bits;
			}
		}
	}

}

// 5/13/2015 mpd : Unlike the SR, there are no groups, frequencies, or subnet settings for users or radios to 
// get wrong. EN programming is limited to IP addresses and masks which we have no way to verify for accuracy
// in a specific installation. This function is not needed for EN. 
///* ---------------------------------------------------------- */
//static BOOL_32 en_values_in_range()
//{
//	BOOL_32 rv = true;
//
//	// 5/12/2015 mpd : Verify that all GUI values are in range before performing a WRITE. Start at the top. Identify
//	// the first problem and move the cursor. Any additional problem(s) will be caught on subsequent call(s) to this 
//	// function.
//
//	return( rv );
//}

/* ---------------------------------------------------------- */
static void EN_PROGRAMMING_initialize_guivars()
{
	// 4/20/2015 ajv : Set the device exchange result to 'read_ok' to display the initialized variables.
	GuiVar_CommOptionDeviceExchangeResult = DEVICE_EXCHANGE_KEY_read_settings_completed_ok;

	strlcpy( GuiVar_ENDHCPName, "", sizeof( GuiVar_ENDHCPName ) );

	strlcpy( GuiVar_ENFirmwareVer, "", sizeof( GuiVar_ENFirmwareVer ) );

	GuiVar_ENGateway_1 = 0;
	GuiVar_ENGateway_2 = 0;
	GuiVar_ENGateway_3 = 0;
	GuiVar_ENGateway_4 = 0;

	GuiVar_ENIPAddress_1 = 0;
	GuiVar_ENIPAddress_2 = 0;
	GuiVar_ENIPAddress_3 = 0;
	GuiVar_ENIPAddress_4 = 0;

	strlcpy( GuiVar_ENMACAddress, "", sizeof( GuiVar_ENMACAddress ) );

	strlcpy( GuiVar_ENModel, "", sizeof( GuiVar_ENModel ) );

	// 4/22/2015 mpd : Set the most used value. 
	GuiVar_ENNetmask = EN_PROGRAMMING_NETMASK_DEFAULT;

	GuiVar_ENObtainIPAutomatically = true;

	strlcpy( GuiVar_CommOptionInfoText, "", sizeof( GuiVar_CommOptionInfoText ) );

}

/* ---------------------------------------------------------- */
static void FDTO_EN_PROGRAMMING_process_device_exchange_key( const UNS_32 pkeycode )
{
	e_SHARED_get_info_text_from_programming_struct();

	switch( pkeycode )
	{
		case DEVICE_EXCHANGE_KEY_read_settings_completed_ok:
		case DEVICE_EXCHANGE_KEY_write_settings_completed_ok:
			if( ( pkeycode == DEVICE_EXCHANGE_KEY_write_settings_completed_ok ) &&
				( EN_PROGRAMMING_setup_mode_read_pending == true ) )
			{
				// 5/22/2015 mpd : A WRITE operation just completed. We need a READ to verify the settings made it
				// into the radio. Act like a "DEVICE_EXCHANGE_KEY_read_settings_in_progress" key code arrived to 
				// continue processing.

				// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
				GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress );

				// 5/18/2015 ajv : Update the progress dialog.
				DEVICE_EXCHANGE_draw_dialog();
			}
			else
			{
				// 4/30/2015 mpd : A MONITOR MODE read operation just completed. We are done.

				// 4/21/2015 ajv : Get values from the programming struct
				get_guivars_from_en_programming_struct();

				// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
				GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_completed_ok );

				// 5/18/2015 ajv : Ensure the flag that indicates a dialog box is open is set to false.
				DIALOG_close_ok_dialog();

				FDTO_EN_PROGRAMMING_draw_screen( (false), g_EN_PROGRAMMING_port );
			}
			break;

		case DEVICE_EXCHANGE_KEY_read_settings_error:
		case DEVICE_EXCHANGE_KEY_write_settings_error:

			// 5/26/2015 mpd : TODO: GR programming refreshes the GuiVars here. Add this step the next time full EN 
			// testing will be performed.
			
			// 5/7/2015 mpd : GR programming responds with errors for invalid values after successful READ and WRITE 
			// operations. The user needs to see the current values.
			//get_guivars_from_en_programming_struct();

			// 5/7/2015 mpd : Do not break here. Continue.

		case DEVICE_EXCHANGE_KEY_busy_try_again_later:
		case DEVICE_EXCHANGE_KEY_read_settings_in_progress:
		case DEVICE_EXCHANGE_KEY_write_settings_in_progress:
			// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
			GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( pkeycode );

			// 5/18/2015 ajv : Update the progress dialog.
			DEVICE_EXCHANGE_draw_dialog();
			break;
	}
}

/* ---------------------------------------------------------- */
static void FDTO_EN_PROGRAMMING_redraw_screen( const BOOL_32 pcomplete_redraw )
{
	// 6/15/2015 ajv : Since the FDTO_EN_PROGRAMMING_draw_screen has two parameters, unlike most
	// other _draw_screen routines, we need to have a redraw routine which accepts a single
	// parameter for cases such as closing a keyboard.
	// 
	// Note that other dialog boxes use the normal _draw_screen routine passing in an
	// unknown value for pport, but that's ok because the complete_redraw parameter will be
	// false so g_EN_PROGRAMMING_port won't be updated to the unknown value.
	FDTO_EN_PROGRAMMING_draw_screen( pcomplete_redraw, g_EN_PROGRAMMING_port );
}

/* ---------------------------------------------------------- */
extern void FDTO_EN_PROGRAMMING_draw_screen( const BOOL_32 pcomplete_redraw, const UNS_32 pport )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		EN_PROGRAMMING_initialize_guivars();

		// 4/21/2015 ajv : Since we're drawing the screen from scratch, start the screen in the
		// "Reading settings from radio" device exchange mode until we're told otherwise.

		// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
		GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress );

		g_EN_PROGRAMMING_port = pport;

		// 4/20/2015 ajv : Default to the MODE field since that's much more likely to be changed
		// than the port.
		lcursor_to_select = CP_EN_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY;
	}
	else
	{
		if( GuiLib_ActiveCursorFieldNo == GuiLib_NO_CURSOR )
		{
			lcursor_to_select = g_EN_PROGRAMMING_previous_cursor_pos;
		}
		else
		{
			lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
		}
	}

	GuiLib_ShowScreen( GuiStruct_scrENProgramming_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();

	if( pcomplete_redraw == (true) )
	{
		// 5/18/2015 ajv : Immediately display the dialog to indicate to the user that something is
		// happening
		DEVICE_EXCHANGE_draw_dialog();

		// 4/21/2015 ajv : Start the data retrieval automatically for the user. This isn't
		// necessary, but it's a nicety, preventing the user from having to press the Read Radio
		// button.
		e_SHARED_start_device_communication( g_EN_PROGRAMMING_port, DEV_READ_OPERATION, &EN_PROGRAMMING_querying_device );

		// 4/28/2015 mpd : This flag will initiate a MONITOR MODE read when the SETUP MODE 
		// read completes.

		// 5/20/2015 mpd : Screen redesign in progress. Block MONITOR MODE for now.
		//EN_PROGRAMMING_monitor_mode_read_pending = true;
	}
}

/* ---------------------------------------------------------- */
extern void EN_PROGRAMMING_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_dlgKeyboard_0:
			if( ((pkey_event.keycode == KEY_BACK) || ((pkey_event.keycode == KEY_SELECT) && (GuiLib_ActiveCursorFieldNo == 49 /*CP_QWERTY_KEYBOARD_OK*/))) )
			{
				// 4/21/2015 ajv : The keyboard routines always use GuiVar_GroupName. Therefore, copy the
				// contents of this variable back into DHCP Name when we're done.
				strlcpy( GuiVar_ENDHCPName, GuiVar_GroupName, sizeof(GuiVar_ENDHCPName) );
			}

			KEYBOARD_process_key( pkey_event, &FDTO_EN_PROGRAMMING_redraw_screen );
			break;

		case GuiStruct_cbxENSubnetMask_0:
			COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_ENNetmask );
			break;

		case GuiStruct_cbxNoYes_0:
			COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_ENObtainIPAutomatically );
			break;

		default:
			switch( pkey_event.keycode )
			{
				case DEVICE_EXCHANGE_KEY_busy_try_again_later:

				case DEVICE_EXCHANGE_KEY_read_settings_completed_ok:
				case DEVICE_EXCHANGE_KEY_read_settings_error:
				case DEVICE_EXCHANGE_KEY_read_settings_in_progress:

				case DEVICE_EXCHANGE_KEY_write_settings_completed_ok:
				case DEVICE_EXCHANGE_KEY_write_settings_error:
				case DEVICE_EXCHANGE_KEY_write_settings_in_progress:

					// 4/21/2015 ajv : Redraw the screen displaying the results or indicating the error
					// condition.
					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._04_func_ptr = (void*)&FDTO_EN_PROGRAMMING_process_device_exchange_key;
					lde._06_u32_argument1 = pkey_event.keycode;
					Display_Post_Command( &lde );

					// 6/9/2015 mpd : MONITOR MODE is moving to the diagnostic screen.
					//if( ( ( pkey_event.keycode == DEVICE_EXCHANGE_KEY_read_settings_completed_ok ) || 
					//	  ( pkey_event.keycode == DEVICE_EXCHANGE_KEY_write_settings_completed_ok ) ) && 
					//	  ( EN_PROGRAMMING_monitor_mode_read_pending == true ) )
					//{
					//	// 5/1/2015 mpd : A SETUP MODE READ, or a WRITE operation successfully completed. We need to
					//	// initiate the MONITOR MODE read to get the latest network info.
					//	EN_PROGRAMMING_monitor_mode_read_pending = false;
					//	e_SHARED_start_device_communication( g_EN_PROGRAMMING_port, E_SHARED_MONITOR_READ, &EN_PROGRAMMING_querying_device );
					//}

					if( ( ( pkey_event.keycode == DEVICE_EXCHANGE_KEY_write_settings_completed_ok ) ) && 
						( EN_PROGRAMMING_setup_mode_read_pending == true ) )
					{
						// 5/1/2015 mpd : A WRITE operation successfully completed. We need to initiate a SETUP MODE
						// read to verify the settings successfully made it into the radio.
						EN_PROGRAMMING_setup_mode_read_pending = false;
						e_SHARED_start_device_communication( g_EN_PROGRAMMING_port, DEV_READ_OPERATION, &EN_PROGRAMMING_querying_device );
					}
					break;

				case KEY_SELECT:
					// 4/20/2015 ajv : If we'r already in the middle of a device exchange or syncing radios,
					// ignore the key press.
					if( (GuiVar_CommOptionDeviceExchangeResult != ( e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_in_progress  ) ) ) && 
						(GuiVar_CommOptionDeviceExchangeResult != ( e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_write_settings_in_progress ) ) ) )
					{
						switch( GuiLib_ActiveCursorFieldNo )
						{
							case CP_EN_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY:
								good_key_beep();
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = (void*)&FDTO_e_SHARED_show_obtain_ip_automatically_dropdown;
								Display_Post_Command( &lde );
								break;

							case CP_EN_PROGRAMMING_DHCP_NAME:
								good_key_beep();

								// 4/21/2015 ajv : The keyboard routines always use GuiVar_GroupName. Therefore, copy the
								// contents of the DHCP Name into this variable first. We'll extract it back out when we're
								// done.
								strlcpy( GuiVar_GroupName, GuiVar_ENDHCPName, sizeof(GuiVar_GroupName) );

								// 4/21/2015 ajv : Blank out the DHCP Name so it is no longer visible on the screen.
								memset( GuiVar_ENDHCPName, 0x00, sizeof(GuiVar_ENDHCPName) );

								KEYBOARD_draw_keyboard( 102, 68, sizeof(GuiVar_ENDHCPName), KEYBOARD_TYPE_QWERTY );
								break;

							case CP_EN_PROGRAMMING_SUBNET_MASK:
								good_key_beep();
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = (void*)&FDTO_e_SHARED_show_subnet_dropdown;
								Display_Post_Command( &lde );
								break;

							case CP_EN_PROGRAMMING_PROGRAM_DEVICE:
								// 5/13/2015 mpd : Not needed for EN.
								//if( en_values_in_range() )
								//{
								//}
								//else
								//{
								//	DIALOG_draw_ok_dialog( GuiStruct_dlgCantProgramRadio_0 );
								//	bad_key_beep();
								//}

								good_key_beep();

								// 4/21/2015 ajv : Store the button that was highlighted when the user pressed SELECT. Once
								// the write process is done, we'll revert back to this button.
								g_EN_PROGRAMMING_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

								// 4/9/2015 mpd : Send any user changes to the programming struct.
								set_en_programming_struct_from_guivars();

								e_SHARED_start_device_communication( g_EN_PROGRAMMING_port, DEV_WRITE_OPERATION, &EN_PROGRAMMING_querying_device );

								// 4/28/2015 mpd : This flag will initiate a MONITOR MODE read when the write
								// completes.
								
								// 5/20/2015 mpd : Screen redesign in progress. Block MONITOR MODE for now.
								//EN_PROGRAMMING_monitor_mode_read_pending = true;

								// 5/22/2015 mpd : This flag will initiate a SETUP MODE read when the write
								// completes.
								EN_PROGRAMMING_setup_mode_read_pending = true;
								break;

							case CP_EN_PROGRAMMING_READ_DEVICE:
								good_key_beep();

								// 4/21/2015 ajv : Store the button that was highlighted when the user pressed SELECT. Once
								// the read process is done, we'll revert back to this button.
								g_EN_PROGRAMMING_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

								e_SHARED_start_device_communication( g_EN_PROGRAMMING_port, DEV_READ_OPERATION, &EN_PROGRAMMING_querying_device );

								// 4/28/2015 mpd : This flag will initiate a MONITOR MODE read when the SETUP MODE 
								// read completes.

								// 5/20/2015 mpd : Screen redesign in progress. Block MONITOR MODE for now.
								//EN_PROGRAMMING_monitor_mode_read_pending = true;
								break;

							default:
								bad_key_beep();
						}
					}
					else
					{
						bad_key_beep();
					}
					break;

				case KEY_PLUS:
				case KEY_MINUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_EN_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY:
							process_bool( &GuiVar_ENObtainIPAutomatically );

							// 4/21/2015 ajv : Since changing this value results in screen elements appearing and
							// disappearing, redraw the whole screen.
							Redraw_Screen( (false) );
							break;

						case CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_1: 
							process_uns32( pkey_event.keycode, &GuiVar_ENIPAddress_1, EN_PROGRAMMING_IP_ADDRESS_OCTET_MIN, EN_PROGRAMMING_IP_ADDRESS_OCTET_MAX, 1, (true) );
							break;

						case CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_2: 
							process_uns32( pkey_event.keycode, &GuiVar_ENIPAddress_2, EN_PROGRAMMING_IP_ADDRESS_OCTET_MIN, EN_PROGRAMMING_IP_ADDRESS_OCTET_MAX, 1, (true) );
							break;

						case CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_3: 
							process_uns32( pkey_event.keycode, &GuiVar_ENIPAddress_3, EN_PROGRAMMING_IP_ADDRESS_OCTET_MIN, EN_PROGRAMMING_IP_ADDRESS_OCTET_MAX, 1, (true) );
							break;

						case CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_4: 
							process_uns32( pkey_event.keycode, &GuiVar_ENIPAddress_4, EN_PROGRAMMING_IP_ADDRESS_OCTET_MIN, EN_PROGRAMMING_IP_ADDRESS_OCTET_MAX, 1, (true) );
							break;

						case CP_EN_PROGRAMMING_SUBNET_MASK:
							process_uns32( pkey_event.keycode, &GuiVar_ENNetmask, EN_PROGRAMMING_NETMASK_MIN, EN_PROGRAMMING_NETMASK_MAX, 1, (false) );
							break;

						case CP_EN_PROGRAMMING_GATEWAY_OCTET_1:
							process_uns32( pkey_event.keycode, &GuiVar_ENGateway_1, EN_PROGRAMMING_GATEWAY_OCTET_MIN, EN_PROGRAMMING_GATEWAY_OCTET_MAX, 1, (true) );
							break;

						case CP_EN_PROGRAMMING_GATEWAY_OCTET_2:
							process_uns32( pkey_event.keycode, &GuiVar_ENGateway_2, EN_PROGRAMMING_GATEWAY_OCTET_MIN, EN_PROGRAMMING_GATEWAY_OCTET_MAX, 1, (true) );
							break;

						case CP_EN_PROGRAMMING_GATEWAY_OCTET_3:
							process_uns32( pkey_event.keycode, &GuiVar_ENGateway_3, EN_PROGRAMMING_GATEWAY_OCTET_MIN, EN_PROGRAMMING_GATEWAY_OCTET_MAX, 1, (true) );
							break;

						case CP_EN_PROGRAMMING_GATEWAY_OCTET_4:
							process_uns32( pkey_event.keycode, &GuiVar_ENGateway_4, EN_PROGRAMMING_GATEWAY_OCTET_MIN, EN_PROGRAMMING_GATEWAY_OCTET_MAX, 1, (true) );
							break;

						default:
							bad_key_beep();
					}

					Refresh_Screen();
					break;

				case KEY_C_UP:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_1: 
						case CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_2:
						case CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_3:
						case CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_4:
							g_EN_PROGRAMMING_previous_cursor_pos = GuiLib_ActiveCursorFieldNo;

							CURSOR_Select( CP_EN_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY, (true) );
							break;

						case CP_EN_PROGRAMMING_SUBNET_MASK:
							// 4/21/2015 ajv : If the last cursor position somehow isn't one of the IP address octets,
							// automatically jump to the first octet.
							if( (g_EN_PROGRAMMING_previous_cursor_pos < CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_1) || (g_EN_PROGRAMMING_previous_cursor_pos > CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_4) )
							{
								g_EN_PROGRAMMING_previous_cursor_pos = CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_1;
							}

							// 4/21/2015 ajv : Return back to the last cursor position the user moved down from
							CURSOR_Select( g_EN_PROGRAMMING_previous_cursor_pos, (true) );
							break;

						case CP_EN_PROGRAMMING_GATEWAY_OCTET_1: 
						case CP_EN_PROGRAMMING_GATEWAY_OCTET_2:
						case CP_EN_PROGRAMMING_GATEWAY_OCTET_3:
						case CP_EN_PROGRAMMING_GATEWAY_OCTET_4:
							g_EN_PROGRAMMING_previous_cursor_pos = GuiLib_ActiveCursorFieldNo;

							CURSOR_Select( CP_EN_PROGRAMMING_SUBNET_MASK, (true) );
							break;

						case CP_EN_PROGRAMMING_PROGRAM_DEVICE:
							if( GuiVar_ENObtainIPAutomatically )
							{
								CURSOR_Select( CP_EN_PROGRAMMING_DHCP_NAME, (true) );
							}
							else
							{
								// 4/21/2015 ajv : If the last cursor position somehow isn't one of the gateway octets,
								// automatically jump to the first octet.
								if( (g_EN_PROGRAMMING_previous_cursor_pos < CP_EN_PROGRAMMING_GATEWAY_OCTET_1) || (g_EN_PROGRAMMING_previous_cursor_pos > CP_EN_PROGRAMMING_GATEWAY_OCTET_4) )
								{
									g_EN_PROGRAMMING_previous_cursor_pos = CP_EN_PROGRAMMING_GATEWAY_OCTET_1;
								}

								// 4/21/2015 ajv : Return back to the last cursor position the user moved down from
								CURSOR_Select( g_EN_PROGRAMMING_previous_cursor_pos, (true) );
							}
							break;

						case CP_EN_PROGRAMMING_READ_DEVICE:
							if( GuiVar_ENObtainIPAutomatically )
							{
								CURSOR_Up( (true) );
							}
							else
							{
								// 4/21/2015 ajv : If the last cursor position somehow isn't one of the gateway octets,
								// automatically jump to the first octet.
								if( (g_EN_PROGRAMMING_previous_cursor_pos < CP_EN_PROGRAMMING_GATEWAY_OCTET_1) || (g_EN_PROGRAMMING_previous_cursor_pos > CP_EN_PROGRAMMING_GATEWAY_OCTET_4) )
								{
									g_EN_PROGRAMMING_previous_cursor_pos = CP_EN_PROGRAMMING_GATEWAY_OCTET_1;
								}

								// 4/21/2015 ajv : Return back to the last cursor position the user moved down from
								CURSOR_Select( g_EN_PROGRAMMING_previous_cursor_pos, (true) );
							}
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_LEFT:
					CURSOR_Up( (true) );
					break;

				case KEY_C_DOWN:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_EN_PROGRAMMING_OBTAIN_IP_AUTOMATICALLY:
							if( GuiVar_ENObtainIPAutomatically )
							{
								CURSOR_Down( (true) );
							}
							else
							{
								// 4/21/2015 ajv : If the last cursor position somehow isn't one of the IP address octets,
								// automatically jump to the first octet.
								if( (g_EN_PROGRAMMING_previous_cursor_pos < CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_1) || (g_EN_PROGRAMMING_previous_cursor_pos > CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_4) )
								{
									g_EN_PROGRAMMING_previous_cursor_pos = CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_1;
								}

								// 4/21/2015 ajv : Return back to the last cursor position the user moved down from
								CURSOR_Select( g_EN_PROGRAMMING_previous_cursor_pos, (true) );
							}
							break;

						case CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_1: 
						case CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_2:
						case CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_3:
						case CP_EN_PROGRAMMING_IP_ADDRESS_OCTET_4:
							g_EN_PROGRAMMING_previous_cursor_pos = GuiLib_ActiveCursorFieldNo;

							CURSOR_Select( CP_EN_PROGRAMMING_SUBNET_MASK, (true) );
							break;

						case CP_EN_PROGRAMMING_SUBNET_MASK:
							// 4/21/2015 ajv : If the last cursor position somehow isn't one of the gateway octets,
							// automatically jump to the first octet.
							if( (g_EN_PROGRAMMING_previous_cursor_pos < CP_EN_PROGRAMMING_GATEWAY_OCTET_1) || (g_EN_PROGRAMMING_previous_cursor_pos > CP_EN_PROGRAMMING_GATEWAY_OCTET_4) )
							{
								g_EN_PROGRAMMING_previous_cursor_pos = CP_EN_PROGRAMMING_GATEWAY_OCTET_1;
							}

							// 4/21/2015 ajv : Return back to the last cursor position the user moved down from
							CURSOR_Select( g_EN_PROGRAMMING_previous_cursor_pos, (true) );
							break;

						case CP_EN_PROGRAMMING_GATEWAY_OCTET_1: 
						case CP_EN_PROGRAMMING_GATEWAY_OCTET_2:
						case CP_EN_PROGRAMMING_GATEWAY_OCTET_3:
						case CP_EN_PROGRAMMING_GATEWAY_OCTET_4:
							g_EN_PROGRAMMING_previous_cursor_pos = GuiLib_ActiveCursorFieldNo;

							CURSOR_Select( CP_EN_PROGRAMMING_READ_DEVICE, (true) );
							break;

						case CP_EN_PROGRAMMING_READ_DEVICE:
							bad_key_beep();
							break;

						default:
							CURSOR_Down( (true) );
							break;
					}
					break;

				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_BACK:
					GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;

					KEY_process_global_keys( pkey_event );

					// 6/5/2015 ajv : Since the user got here from the COMM OPTIONS dialog box, redraw that
					// dialog box.
					COMM_OPTIONS_draw_dialog( (true) );
					break;

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

