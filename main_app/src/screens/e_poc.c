/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_poc.h"

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"alerts.h"

#include	"app_startup.h"

#include	"change.h"

#include	"combobox.h"

#include	"configuration_controller.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"e_keyboard.h"

#include	"group_base_file.h"

#include	"m_main.h"

#include	"poc.h"

#include	"scrollbox.h"

#include	"speaker.h"

#include	"tpmicro_comm.h"

#include	"e_numeric_keypad.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_POC_NAME							(0)
#define	CP_POC_USAGE						(1)
#define CP_POC_BYPASS_LEVELS				(2)
#define CP_POC_DECODER_1					(3)
#define CP_POC_FM_TYPE_1					(4)
#define	CP_POC_FM_K_VALUE_1					(5)
#define CP_POC_FM_OFFSET_1					(6)
#define CP_POC_BERMAD_REED_SWITCH_TYPE_1	(7)
#define CP_POC_MV_TYPE						(8)
#define CP_POC_DECODER_2					(9)
#define CP_POC_FM_TYPE_2					(10)
#define	CP_POC_FM_K_VALUE_2					(11)
#define CP_POC_FM_OFFSET_2					(12)
#define CP_POC_BERMAD_REED_SWITCH_TYPE_2	(13)
#define CP_POC_DECODER_3					(14)
#define CP_POC_FM_TYPE_3					(15)
#define	CP_POC_FM_K_VALUE_3					(16)
#define CP_POC_FM_OFFSET_3					(17)
#define CP_POC_BERMAD_REED_SWITCH_TYPE_3	(18)

//--------------------- 

#define POC_FLOW_METER_Y_OFFSET_BYPASS_LEVEL_1		(44)
#define POC_FLOW_METER_Y_OFFSET_BYPASS_LEVEL_2		(102)

// 8/20/2014 ajv : Subtract 39 from the offset since the cursor field is at the
// extreme bottom of the screen so we need to draw the combo box up from the
// cursor, rather than down.
#define POC_FLOW_METER_Y_OFFSET_BYPASS_LEVEL_3		(146 - 39)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 05.19.2016 rmd : These four screen support variables are also used on a few other screens. Notably
// some of the budgets screens.

UNS_32	g_POC_top_line;

BOOL_32	g_POC_editing_group;

// 8/11/2014 ajv : Since the currently selected item in the list does not
// necessarily correspond to g_GROUP_list_item_index any more (since the latter
// corresponds to the POC list while the current item corresponds to the
// POC_MENU_items array), we need to keep track of which cursor item we're on as
// it relates to the POC_MENU_items array.
UNS_32 g_POC_current_list_item_index;

POC_MENU_SCROLL_BOX_ITEM POC_MENU_items[ MAX_POCS_IN_NETWORK ];

// ----------

static UNS_32	g_POC_prev_cursor_pos;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void FDTO_POC_return_to_menu( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static float POC_get_inc_by_for_K_value( const UNS_32 prepeats )
{
	float	rv;

	if( prepeats > 96 )
	{
		rv = 0.15F;
	}
	else if( prepeats > 64 )
	{
		rv = 0.016F;
	}
	else if( prepeats > 32 )
	{
		rv = 0.008F;
	}
	else
	{
		rv = 0.001F;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static float POC_get_inc_by_for_offset( const UNS_32 prepeats )
{
	float	rv;

	if( prepeats > 64 )
	{
		rv = 0.008F;
	}
	else if( prepeats > 32 )
	{
		rv = 0.004F;
	}
	else
	{
		rv = 0.001F;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static void POC_process_decoder_serial_number( const UNS_32 pkeycode, UNS_32 *var_ptr )
{
	POC_GROUP_STRUCT	*lpoc;

	UNS_32	lindex;

	UNS_32	i;

	// ----------

	lpoc = POC_get_group_at_this_index( g_GROUP_list_item_index );

	// ----------

	lindex = 0;

	for( i = 0; i < POC_BYPASS_LEVELS_MAX; ++i )
	{
		if ( *var_ptr == POC_get_decoder_serial_number_for_this_poc_and_level_0( lpoc, i ) )
		{
			lindex = i;

			// 5/20/2014 ajv : Since we found it, no need to look any
			// further so break out of the loop.
			break;
		}
	}

	// ----------

	process_uns32( pkeycode, &lindex, 0, (GuiVar_POCBypassStages - 1), 1, (false) );

	if( POC_get_decoder_serial_number_for_this_poc_and_level_0( lpoc, lindex ) != DECODER_SERIAL_NUM_DEFAULT )
	{
		*var_ptr = POC_get_decoder_serial_number_for_this_poc_and_level_0( lpoc, lindex );
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void FDTO_POC_show_poc_usage_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxPOCUsage_0, &FDTO_COMBOBOX_add_items, 3, GuiVar_POCUsage );
}

/* ---------------------------------------------------------- */
static void FDTO_POC_show_master_valve_dropdown( void )
{
	// 6/3/2015 ajv : Determine how much to vertically offset the Master Valve ComboBox based on
	// where it's placed on the screen. The default Y-offset is where the field is when using a
	// single POC (non-Bypass) without a Reed switch-style flow sensor.
	if( GuiVar_POCType == POC__FILE_TYPE__DECODER_BYPASS )
	{
		GuiVar_ComboBox_Y1 = 42;
	}
	else
	{
		if( GuiVar_POCFMType1IsBermad )
		{
			GuiVar_ComboBox_Y1 = 12;
		}
		else if( GuiVar_POCFlowMeterType1 == POC_FM_FMBX )
		{
			GuiVar_ComboBox_Y1 = 24;
		}
		else
		{
			GuiVar_ComboBox_Y1 = 0;
		}
	}

	FDTO_COMBOBOX_show( GuiStruct_cbxMasterValve_0, &FDTO_COMBOBOX_add_items, 2, GuiVar_POCMVType1 );
}

/* ---------------------------------------------------------- */
static void FDTO_POC_show_flow_meter_dropdown( void )
{
	UNS_32	lactive_line;

	if( GuiVar_POCType == POC__FILE_TYPE__DECODER_BYPASS )
	{
		if( GuiLib_ActiveCursorFieldNo == CP_POC_FM_TYPE_1 )
		{
			lactive_line = GuiVar_POCFlowMeterType1;

			GuiVar_ComboBox_Y1 = POC_FLOW_METER_Y_OFFSET_BYPASS_LEVEL_1;
		}
		else if( GuiLib_ActiveCursorFieldNo == CP_POC_FM_TYPE_2 )
		{
			lactive_line = GuiVar_POCFlowMeterType2;

			GuiVar_ComboBox_Y1 = POC_FLOW_METER_Y_OFFSET_BYPASS_LEVEL_2;
		}
		else
		{
			lactive_line = GuiVar_POCFlowMeterType3;

			GuiVar_ComboBox_Y1 = POC_FLOW_METER_Y_OFFSET_BYPASS_LEVEL_3;
		}
	}
	else
	{
		lactive_line = GuiVar_POCFlowMeterType1;

		GuiVar_ComboBox_Y1 = 0;
	}

	FDTO_COMBOBOX_show( GuiStruct_cbxFlowMeter_0, &FDTO_COMBOBOX_add_items, (POC_FM_CHOICE_MAX + 1), lactive_line );
}

/* ---------------------------------------------------------- */
static void FDTO_POC_close_flow_meter_dropdown( void )
{
	UNS_32	*fm_type_ptr;

	UNS_32	previous_fm_type;

	UNS_32	*rs_type_ptr;

	UNS_32	*fm_type_is_bermad_guivar_ptr;

	// ----------

	if( (GuiVar_ComboBox_Y1 == 0) || (GuiVar_ComboBox_Y1 == POC_FLOW_METER_Y_OFFSET_BYPASS_LEVEL_1) ) 
	{
		fm_type_ptr = &GuiVar_POCFlowMeterType1;

		rs_type_ptr = &GuiVar_POCReedSwitchType1;

		fm_type_is_bermad_guivar_ptr = &GuiVar_POCFMType1IsBermad;
	}
	else if( GuiVar_ComboBox_Y1 == POC_FLOW_METER_Y_OFFSET_BYPASS_LEVEL_2 )
	{
		fm_type_ptr = &GuiVar_POCFlowMeterType2;

		rs_type_ptr = &GuiVar_POCReedSwitchType2;

		fm_type_is_bermad_guivar_ptr = &GuiVar_POCFMType2IsBermad;
	}
	else // if( GuiVar_cbxFlowMeter_Y == POC_FLOW_METER_Y_OFFSET_BYPASS_LEVEL_3 )
	{
		fm_type_ptr = &GuiVar_POCFlowMeterType3;

		rs_type_ptr = &GuiVar_POCReedSwitchType3;

		fm_type_is_bermad_guivar_ptr = &GuiVar_POCFMType3IsBermad;
	}

	// ----------

	// 1/16/2015 ajv : We use the previous flow meter type to help range check the reed switch
	// type when a BERMAD is selected, so get it now before we update the FM type GuiVar.
	previous_fm_type = *fm_type_ptr;

	*fm_type_ptr = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );

	// ----------

	// 4/21/2016 ajv : Also make sure the FMTypeIsBermad selection is set.
	*fm_type_is_bermad_guivar_ptr = (*fm_type_ptr >= POC_FM_BERMAD_0150);

	// ----------

	if( (*fm_type_ptr == POC_FM_BERMAD_0600) && (previous_fm_type == POC_FM_BERMAD_0400) && (*rs_type_ptr == POC_FM_REED_SWITCH_1_TO_1) )
	{
		// 1/15/2015 ajv : Since we moved from a 4" BERMAD to a 6" and the 6" doesn't support the 1
		// pulse/1 gal reed switch, force it up to a 1 to 10.
		*rs_type_ptr = POC_FM_REED_SWITCH_1_TO_10;
	}

	// 2/11/2015 ajv : We don't currently support 1 to 100 and 1 to 1000 reed switches.
	#if 0
	else if( (*fm_type_ptr == POC_FM_BERMAD_0400) && (previous_fm_type == POC_FM_BERMAD_0600) && (*rs_type_ptr == POC_FM_REED_SWITCH_1_TO_1000) )
	{
		// 1/15/2015 ajv : Since we moved from a 6" BERMAD to a 4" and the 4" doesn't support the 1
		// pulse/1000 gal reed switch, force it to down to a 1 to 100.
		*rs_type_ptr = POC_FM_REED_SWITCH_1_TO_100;
	}
	#endif

	// ----------

	FDTO_COMBOBOX_hide();
}

/* ---------------------------------------------------------- */
/**
 * Processes the Plus and Minus keys to change the flow meter value.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkeycode The key that was pressed.
 * @param pfm_type_guivar_ptr A pointer to the appropriate flow meter easyGUI
 *  						  variable. This can be GuiVar_POCFlowMeterType1
 *  						  through 3.
 * 
 * @author 8/8/2011 Adrianusv
 *
 * @revisions
 *    8/8/2011  Initial release
 */
static void POC_process_fm_type( const UNS_32 pkeycode, UNS_32 *pfm_type_guivar_ptr, UNS_32 *pfm_type_is_bermad_guivar_ptr, UNS_32 *prs_type_guivar_ptr )
{
	UNS_32 lprevious_fm_type;

	// Track what the previous type was to determine whether we need to redraw
	// the screen or not. For example, when changing from FM-3 to FMBX, we need
	// to redraw the screen to show the K value and offet fields.
	lprevious_fm_type = *pfm_type_guivar_ptr;

	// 1/16/2015 ajv : We allow the user to select any size flow meter for any level of a bypass
	// manifold. However, when they try to leave the screen, they will receive a message if they
	// did not size them from smallest to largest.
	process_uns32( pkeycode, pfm_type_guivar_ptr, POC_FM_CHOICE_MIN, POC_FM_CHOICE_MAX, 1, (false) );

	if( (*pfm_type_guivar_ptr == POC_FM_BERMAD_0600) && (lprevious_fm_type == POC_FM_BERMAD_0400) && (*prs_type_guivar_ptr == POC_FM_REED_SWITCH_1_TO_1) )
	{
		// 1/15/2015 ajv : Since we moved from a 4" BERMAD to a 6" and the 6" doesn't support the 1
		// pulse/1 gal reed switch, force it up to a 1 to 10.
		*prs_type_guivar_ptr = POC_FM_REED_SWITCH_1_TO_10;
	}

	// 2/11/2015 ajv : We don't currently support 1 to 100 and 1 to 1000 reed switches.
	#if 0
	else if( (*pfm_type_guivar_ptr == POC_FM_BERMAD_0400) && (lprevious_fm_type == POC_FM_BERMAD_0600) && (*prs_type_guivar_ptr == POC_FM_REED_SWITCH_1_TO_1000) )
	{
		// 1/15/2015 ajv : Since we moved from a 6" BERMAD to a 4" and the 4" doesn't support the 1
		// pulse/1000 gal reed switch, force it to down to a 1 to 100.
		*prs_type_guivar_ptr = POC_FM_REED_SWITCH_1_TO_100;
	}
	#endif

	if( ((*pfm_type_is_bermad_guivar_ptr == (false)) && (*pfm_type_guivar_ptr >= POC_FM_BERMAD_0150)) ||
		((*pfm_type_is_bermad_guivar_ptr == (true)) && (*pfm_type_guivar_ptr < POC_FM_BERMAD_0150)) )
	{
		*pfm_type_is_bermad_guivar_ptr = !(*pfm_type_is_bermad_guivar_ptr);

		// 1/16/2015 ajv : Since we need to show or hide the Reed Switch type if the user changed
		// to or from a BERMAD, redraw the screen.
		Redraw_Screen( (false));
	}
	else if( (*pfm_type_guivar_ptr == POC_FM_FMBX) || (lprevious_fm_type == POC_FM_FMBX) )
	{
		// 1/16/2015 ajv : Since the K value and offset fields appear and disppear based upon the
		// flow meter type, redraw the screen if the flow meter type is or just was FMBX.
		Redraw_Screen( (false) );
	}
}

/* ---------------------------------------------------------- */
static void POC_process_reed_switch_type( const UNS_32 pkeycode, const UNS_32 fm_type_guivar, UNS_32 *prs_type_guivar_ptr )
{
	if( (fm_type_guivar >= POC_FM_BERMAD_0150) && (fm_type_guivar <= POC_FM_BERMAD_0400) )
	{
		process_uns32( pkeycode, prs_type_guivar_ptr, POC_FM_REED_SWITCH_1_TO_1, POC_FM_REED_SWITCH_1_TO_10, 1, (false) );
	}
	else if( (fm_type_guivar >= POC_FM_BERMAD_0600) && (fm_type_guivar <= POC_FM_BERMAD_1000) )
	{
		process_uns32( pkeycode, prs_type_guivar_ptr, POC_FM_REED_SWITCH_1_TO_10, POC_FM_REED_SWITCH_1_TO_10, 1, (false) );
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
extern UNS_32 POC_get_menu_index_for_displayed_poc( void )
{
	// 05.19.2016 rmd : NOTE - this function is also called from a budgets screen.

	POC_GROUP_STRUCT	*lpoc;

	UNS_32	rv;

	UNS_32	i;

	rv = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lpoc = nm_POC_get_pointer_to_poc_with_this_gid_and_box_index( g_GROUP_ID, GuiVar_POCBoxIndex );

	for( i = 0; i < POC_get_list_count(); ++i )
	{
		if( POC_MENU_items[ i ].poc_ptr == lpoc )
		{
			rv = i;
			
			break;
		}
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
static void POC_process_NEXT_and_PREV( const UNS_32 pkeycode )
{
	UNS_32	lmenu_index_for_displayed_poc;

	lmenu_index_for_displayed_poc = POC_get_menu_index_for_displayed_poc();

	if( (pkeycode == KEY_NEXT) || (pkeycode == KEY_PLUS) )
	{
		// 7/9/2015 ajv : Turn off the automatic redraw functions within the SCROLL_BOX_to_line and
		// SCROLL_BOX_up_or_down routines to ensure the scroll bar does not appear.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_DISABLED );

		SCROLL_BOX_to_line( 0, (INT_16)lmenu_index_for_displayed_poc );

		// 7/13/2015 ajv : Don't allow lmenu_index_for_displayed_poc to exceed the max size of the
		// array
		if( lmenu_index_for_displayed_poc < MAX_POCS_IN_NETWORK )
		{
			++lmenu_index_for_displayed_poc;

			// 7/14/2015 ajv : Attempt to move to the next poc.
			if( POC_MENU_items[ lmenu_index_for_displayed_poc ].poc_ptr != NULL )
			{
				SCROLL_BOX_up_or_down( 0, KEY_C_DOWN );
			}
		}
		else
		{
			bad_key_beep();
		}

		// 7/9/2015 ajv : Now that we've moved the scroll bar without displaying it, reenable the
		// automatic redraw functions to ensure other calls are not affected.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_ENABLED );
	}
	else
	{
		// 7/9/2015 ajv : Turn off the automatic redraw functions within the SCROLL_BOX_to_line and
		// SCROLL_BOX_up_or_down routines to ensure the scroll bar does not appear.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_DISABLED );

		SCROLL_BOX_to_line( 0, (INT_16)lmenu_index_for_displayed_poc );

		// 7/13/2015 ajv : Don't allow lmenu_index_for_displayed_poc to go below 0.
		if( lmenu_index_for_displayed_poc > 0 )
		{
			--lmenu_index_for_displayed_poc;

			// 7/14/2015 ajv : Attempt to move to the previous poc.
			if( POC_MENU_items[ lmenu_index_for_displayed_poc ].poc_ptr != NULL )
			{
				SCROLL_BOX_up_or_down( 0, KEY_C_UP );
			}
		}
		else
		{
			bad_key_beep();
		}

		// 10/24/2013 ajv : Now that we've moved the scroll bar without displaying it, reenable the
		// automatic redraw functions to ensure other calls are not affected.
		SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_ENABLED );
	}

	// ----------

	// 7/9/2015 ajv : If we've actually moved to another POC, save the current POC's information
	// and repopulate the GuiVars with the next POC's.
	if( POC_MENU_items[ lmenu_index_for_displayed_poc ].poc_ptr != NULL )
	{
		POC_extract_and_store_changes_from_GuiVars();

		g_GROUP_list_item_index = POC_get_index_using_ptr_to_poc_struct( POC_MENU_items[ lmenu_index_for_displayed_poc ].poc_ptr );

		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		POC_copy_group_into_guivars( g_GROUP_list_item_index );

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}
	else
	{
		bad_key_beep();
	}

	// 7/9/2015 ajv : Although nothing may have changed if we encountered the top or bottom of
	// the list, we need to force a redraw to ensure the scroll line of the currently selected
	// station is suppressed. Without this call, the SCROLL_BOX_to_line call drew the scroll
	// line and as soon as the next call to GuiLib_Refresh happens (such as once per second when
	// the date is updated), the scroll line will appear.
	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
/**
 * Processes the key event from the POC screen for a particular POC.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 10/16/2012 Adrianusv
 *
 * @revisions (none)
 */
static void POC_process_group( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_dlgKeyboard_0:
			if( ((pkey_event.keycode == KEY_BACK) || ((pkey_event.keycode == KEY_SELECT) && (GuiLib_ActiveCursorFieldNo == 49 /*CP_QWERTY_KEYBOARD_OK*/))) )
			{
				POC_extract_and_store_group_name_from_GuiVars();
			}

			KEYBOARD_process_key( pkey_event, &FDTO_POC_draw_menu );
			break;

		case GuiStruct_dlgNumericKeypad_0:
			NUMERIC_KEYPAD_process_key( pkey_event, &FDTO_POC_draw_menu );
			break;

		case GuiStruct_cbxPOCUsage_0:
			COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_POCUsage );
			break;

		case GuiStruct_cbxMasterValve_0:
			COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_POCMVType1 );
			break;

		case GuiStruct_cbxFlowMeter_0:
			if( (pkey_event.keycode == KEY_SELECT) || (pkey_event.keycode == KEY_BACK) )
			{
				good_key_beep();

				lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
				lde._04_func_ptr = FDTO_POC_close_flow_meter_dropdown;
				Display_Post_Command( &lde );
			}
			else
			{
				COMBO_BOX_key_press( pkey_event.keycode, NULL );
			}
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_POC_NAME:
							GROUP_process_show_keyboard( 150, 27 );
							break;

						case CP_POC_USAGE:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_POC_show_poc_usage_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_POC_FM_TYPE_1:
						case CP_POC_FM_TYPE_2:
						case CP_POC_FM_TYPE_3:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_POC_show_flow_meter_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_POC_MV_TYPE:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_POC_show_master_valve_dropdown;
							Display_Post_Command( &lde );
							break;

						case CP_POC_FM_K_VALUE_1:
							good_key_beep();

							NUMERIC_KEYPAD_draw_float_keypad( &GuiVar_POCFlowMeterK1, POC_FM_KVALUE_MIN, POC_FM_KVALUE_MAX, 3 );
							break;

						case CP_POC_FM_K_VALUE_2:
							good_key_beep();

							NUMERIC_KEYPAD_draw_float_keypad( &GuiVar_POCFlowMeterK2, POC_FM_KVALUE_MIN, POC_FM_KVALUE_MAX, 3 );
							break;

						case CP_POC_FM_K_VALUE_3:
							good_key_beep();

							NUMERIC_KEYPAD_draw_float_keypad( &GuiVar_POCFlowMeterK3, POC_FM_KVALUE_MIN, POC_FM_KVALUE_MAX, 3 );
							break;

						case CP_POC_FM_OFFSET_1:
							good_key_beep();

							NUMERIC_KEYPAD_draw_float_keypad( &GuiVar_POCFlowMeterOffset1, POC_FM_OFFSET_MIN, POC_FM_OFFSET_MAX, 3 );
							break;

						case CP_POC_FM_OFFSET_2:
							good_key_beep();

							NUMERIC_KEYPAD_draw_float_keypad( &GuiVar_POCFlowMeterOffset2, POC_FM_OFFSET_MIN, POC_FM_OFFSET_MAX, 3 );
							break;

						case CP_POC_FM_OFFSET_3:
							good_key_beep();

							NUMERIC_KEYPAD_draw_float_keypad( &GuiVar_POCFlowMeterOffset3, POC_FM_OFFSET_MIN, POC_FM_OFFSET_MAX, 3 );
							break;

						default:
							bad_key_beep();
					}
					Refresh_Screen();
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch (GuiLib_ActiveCursorFieldNo)
					{
						case CP_POC_USAGE:
							process_uns32( pkey_event.keycode, &GuiVar_POCUsage, POC_USAGE_MIN, POC_USAGE_MAX, 1, (false) );
							break;

						case CP_POC_BYPASS_LEVELS:
							process_uns32( pkey_event.keycode, &GuiVar_POCBypassStages, POC_BYPASS_LEVELS_MIN, POC_BYPASS_LEVELS_MAX, 1, (false) );

							// 4/16/2014 ajv : Since this causes information to
							// appear or disappear from the screen, a full
							// redraw is necessary rather than just a
							// GuiLib_Refresh.
							Redraw_Screen( (false) );
							break;

						case CP_POC_DECODER_1:
							POC_process_decoder_serial_number( pkey_event.keycode, &GuiVar_POCDecoderSN1 );
							break;

						case CP_POC_FM_TYPE_1:
							POC_process_fm_type( pkey_event.keycode, &GuiVar_POCFlowMeterType1, &GuiVar_POCFMType1IsBermad, &GuiVar_POCReedSwitchType1 );
							break;

						case CP_POC_FM_K_VALUE_1:
							process_fl( pkey_event.keycode, &GuiVar_POCFlowMeterK1, POC_FM_KVALUE_MIN, POC_FM_KVALUE_MAX, POC_get_inc_by_for_K_value( pkey_event.repeats ), (false) );
							break;

						case CP_POC_FM_OFFSET_1:
							process_fl( pkey_event.keycode, &GuiVar_POCFlowMeterOffset1, POC_FM_OFFSET_MIN, POC_FM_OFFSET_MAX, POC_get_inc_by_for_offset( pkey_event.repeats ), (false) );
							break;

						case CP_POC_BERMAD_REED_SWITCH_TYPE_1:
							POC_process_reed_switch_type( pkey_event.keycode, GuiVar_POCFlowMeterType1, &GuiVar_POCReedSwitchType1 );
							break;

						case CP_POC_MV_TYPE:
							process_uns32( pkey_event.keycode, &GuiVar_POCMVType1, POC_MV_TYPE_NORMALLY_OPEN, POC_MV_TYPE_NORMALLY_CLOSED, 1, (true) );
							break;

						case CP_POC_DECODER_2:
							POC_process_decoder_serial_number( pkey_event.keycode, &GuiVar_POCDecoderSN2 );
							break;

						case CP_POC_FM_TYPE_2:
							POC_process_fm_type( pkey_event.keycode, &GuiVar_POCFlowMeterType2, &GuiVar_POCFMType2IsBermad, &GuiVar_POCReedSwitchType2 );
							break;

						case CP_POC_FM_K_VALUE_2:
							process_fl( pkey_event.keycode, &GuiVar_POCFlowMeterK2, POC_FM_KVALUE_MIN, POC_FM_KVALUE_MAX, POC_get_inc_by_for_K_value( pkey_event.repeats ), (false) );
							break;

						case CP_POC_FM_OFFSET_2:
							process_fl( pkey_event.keycode, &GuiVar_POCFlowMeterOffset2, POC_FM_OFFSET_MIN, POC_FM_OFFSET_MAX, POC_get_inc_by_for_offset( pkey_event.repeats ), (false) );
							break;

						case CP_POC_BERMAD_REED_SWITCH_TYPE_2:
							POC_process_reed_switch_type( pkey_event.keycode, GuiVar_POCFlowMeterType2, &GuiVar_POCReedSwitchType2 );
							break;

						case CP_POC_DECODER_3:
							POC_process_decoder_serial_number( pkey_event.keycode, &GuiVar_POCDecoderSN3 );
							break;

						case CP_POC_FM_TYPE_3:
							POC_process_fm_type( pkey_event.keycode, &GuiVar_POCFlowMeterType3, &GuiVar_POCFMType3IsBermad, &GuiVar_POCReedSwitchType3 );
							break;

						case CP_POC_FM_K_VALUE_3:
							process_fl( pkey_event.keycode, &GuiVar_POCFlowMeterK3, POC_FM_KVALUE_MIN, POC_FM_KVALUE_MAX, POC_get_inc_by_for_K_value( pkey_event.repeats ), (false) );
							break;

						case CP_POC_FM_OFFSET_3:
							process_fl( pkey_event.keycode, &GuiVar_POCFlowMeterOffset3, POC_FM_OFFSET_MIN, POC_FM_OFFSET_MAX, POC_get_inc_by_for_offset( pkey_event.repeats ), (false) );
							break;

						case CP_POC_BERMAD_REED_SWITCH_TYPE_3:
							POC_process_reed_switch_type( pkey_event.keycode, GuiVar_POCFlowMeterType3, &GuiVar_POCReedSwitchType3 );
							break;

						default:
							bad_key_beep();
					}
					Refresh_Screen();
					break;

				case KEY_NEXT:
				case KEY_PREV: 
					POC_process_NEXT_and_PREV( pkey_event.keycode );
					break;

				case KEY_C_UP:
					if( GuiVar_POCType != POC__FILE_TYPE__DECODER_BYPASS )
					{
						CURSOR_Up( (true) );
					}
					else
					{
						switch( GuiLib_ActiveCursorFieldNo )
						{
							case CP_POC_BERMAD_REED_SWITCH_TYPE_1:
								CURSOR_Select( CP_POC_DECODER_1, (true) );
								break;

							case CP_POC_MV_TYPE:
								CURSOR_Select( CP_POC_FM_TYPE_1, (true) );
								break;

							case CP_POC_DECODER_2:
								g_POC_prev_cursor_pos = CP_POC_DECODER_2;

								CURSOR_Up( (true) );
								break;

							case CP_POC_BERMAD_REED_SWITCH_TYPE_2:
								CURSOR_Select( CP_POC_DECODER_2, (true) );
								break;

							case CP_POC_DECODER_3:
								CURSOR_Select( CP_POC_FM_TYPE_2, (true) );
								break;

							case CP_POC_FM_K_VALUE_2:
								g_POC_prev_cursor_pos = CP_POC_FM_K_VALUE_2;

								CURSOR_Select( CP_POC_MV_TYPE, (true) );
								break;

							case CP_POC_FM_K_VALUE_3:
								CURSOR_Select( CP_POC_FM_OFFSET_2, (true) );
								break;

							case CP_POC_BERMAD_REED_SWITCH_TYPE_3:
								CURSOR_Select( CP_POC_DECODER_3, (true) );
								break;

							default:
								CURSOR_Up( (true) );
						}
					}
					break;

				case KEY_C_DOWN:
					if( GuiVar_POCType != POC__FILE_TYPE__DECODER_BYPASS )
					{
						CURSOR_Down( (true) );
					}
					else
					{
						switch( GuiLib_ActiveCursorFieldNo )
						{
							case CP_POC_FM_TYPE_1:
								CURSOR_Select( CP_POC_MV_TYPE, (true) );
								break;

							case CP_POC_MV_TYPE:
								CURSOR_Select( g_POC_prev_cursor_pos, (true) );
								break;

							case CP_POC_FM_TYPE_2:
								CURSOR_Select( CP_POC_DECODER_3, (true) );
								break;

							case CP_POC_FM_OFFSET_2:
								CURSOR_Select( CP_POC_FM_K_VALUE_3, (true) );
								break;

							case CP_POC_FM_TYPE_3:
								bad_key_beep();
								break;

							default:
								CURSOR_Down( (true) );
						}
					}
					break;

				case KEY_C_LEFT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_POC_NAME:
							KEY_process_BACK_from_editing_screen( (void*)&FDTO_POC_return_to_menu );
							break;

						default:
							CURSOR_Up( (true) );
					}
					break;

				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_BACK:
					KEY_process_BACK_from_editing_screen( (void*)&FDTO_POC_return_to_menu );
					break;

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
// 7/26/2016 skc : Populate the global array with valid POC items. The flag_not_used
// parameter is for screens that wish to suppress POCs not used with budgets.
extern UNS_32 POC_populate_pointers_of_POCs_for_display( BOOL_32 flag_not_used )
{
	POC_GROUP_STRUCT *lpoc;

	UNS_32	lavailable_POCs;

	UNS_32	i;

	// ----------

	lavailable_POCs = 0;

	memset( POC_MENU_items, 0x00, sizeof(POC_MENU_items) );

	// ----------

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	for( i = 0 ; i < POC_get_list_count(); i++ )
	{
		lpoc = POC_get_group_at_this_index( i );

		if( (lpoc != NULL) && POC_get_show_for_this_poc(lpoc) )
		{
			// Budget screens don't display POCs not part of budget calculations.
			if( flag_not_used ) // then we need to filter the list of POCs
			{
				// Only show POCs marked as irrigation and non-irrigation, and allowed to be used with
				// budgets due to possible mixed flow meters
				if( (POC_get_usage(nm_GROUP_get_group_ID(lpoc)) != POC_USAGE_NOT_USED) &&
					 POC_use_for_budget( i ) ) 
				{
					POC_MENU_items[ lavailable_POCs ].poc_ptr = lpoc;
					lavailable_POCs++;
				}
			}
			else // show all the POCs
			{
				POC_MENU_items[ lavailable_POCs ].poc_ptr = lpoc;
				lavailable_POCs++;
			}
		}
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	// ----------

	return ( lavailable_POCs );
}

/* ---------------------------------------------------------- */
extern void POC_load_poc_name_into_guivar( const INT_16 pindex_0_i16 )
{
	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	if( (UNS_32)pindex_0_i16 < POC_get_list_count() )
	{
		strlcpy( GuiVar_itmGroupName, nm_GROUP_get_name( POC_MENU_items[ pindex_0_i16 ].poc_ptr ), NUMBER_OF_CHARS_IN_A_NAME );
	}
	else
	{
		Alert_group_not_found();
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern POC_GROUP_STRUCT *POC_get_ptr_to_physically_available_poc( const UNS_32 pindex_0 )
{
	return( POC_MENU_items[ pindex_0 ].poc_ptr );
}

/* ---------------------------------------------------------- */
/**
 * Returns to the menu on the left from the editing screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user presses a key to move away from the editing screen and back
 * onto the menu.
 * 
 * @param psave_changes True if the changes should be saved; otherwise, false.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_POC_return_to_menu( void )
{
	FDTO_GROUP_return_to_menu( &g_POC_editing_group, (void*)&POC_extract_and_store_changes_from_GuiVars );
}

/* ---------------------------------------------------------- */
/**
 * Draws the POC menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the screen is first navigated to.
 * 
 * @param pcomplete_redraw True if the screen should be complete redrawn;
 * otherwise, false. If false, only elements on the screen are updated, not the
 * entire structure, essentially refreshing the content.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void FDTO_POC_draw_menu( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lpocs_in_use;

	UNS_32	lactive_line;

	INT_32	lcursor_to_select;

	// ----------

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	// 8/11/2014 ajv : Only count POCs that are in use as valid POCs to display.
	lpocs_in_use = POC_populate_pointers_of_POCs_for_display( false );

	if( pcomplete_redraw == (true) )
	{
		lcursor_to_select = GuiLib_NO_CURSOR;

		g_POC_top_line = 0;

		g_POC_editing_group = (false);

		g_POC_current_list_item_index = 0;

		g_GROUP_list_item_index = POC_get_index_using_ptr_to_poc_struct( POC_get_ptr_to_physically_available_poc(g_POC_current_list_item_index) );

		POC_copy_group_into_guivars( g_GROUP_list_item_index );

		// 7/11/2014 ajv : Default the previous cursor position to the Decoder 2
		// serial number. That way, when the user presses the DOWN arrow from
		// the Master Valve field, it will jump there by default rather than the
		// K Value field.
		g_POC_prev_cursor_pos = CP_POC_DECODER_2;
	}
	else
	{
		lcursor_to_select = (INT_32)GuiLib_ActiveCursorFieldNo;

		g_POC_top_line = (UNS_32)GuiLib_ScrollBox_GetTopLine( 0 );
	}

	GuiLib_ShowScreen( GuiStruct_scrPOC_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );

	// 8/27/2015 ajv : Close the scroll box to ensure the ScrollBox_Init function handles the
	// redraw properly.
	GuiLib_ScrollBox_Close( 0 );

	lactive_line = POC_get_menu_index_for_displayed_poc();

	if( g_POC_editing_group == (true) )
	{
		// 10/24/2013 ajv : Draw the scroll box without highlighting the active
		// line and draw the indicator to show which station the user is looking
		// at.
		GuiLib_ScrollBox_Init_Custom_SetTopLine( 0, &POC_load_poc_name_into_guivar, (INT_16)lpocs_in_use, GuiLib_NO_CURSOR, (INT_16)g_POC_top_line );
		GuiLib_ScrollBox_SetIndicator( 0, lactive_line );
		GuiLib_ScrollBox_Redraw( 0 );
	}
	else
	{
		GuiLib_ScrollBox_Init_Custom_SetTopLine( 0, &POC_load_poc_name_into_guivar, (INT_16)lpocs_in_use, (INT_16)lactive_line, (INT_16)g_POC_top_line );
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed on the POC menu.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void POC_process_menu( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	if( g_POC_editing_group == (true) )
	{
		POC_process_group( pkey_event );
	}
	else
	{
		switch( pkey_event.keycode )
		{
			case KEY_C_RIGHT:
			case KEY_SELECT:
				good_key_beep();

				g_POC_current_list_item_index = GuiLib_ScrollBox_GetActiveLine( 0, 0 );

				g_GROUP_list_item_index = POC_get_index_using_ptr_to_poc_struct( POC_get_ptr_to_physically_available_poc( g_POC_current_list_item_index ) );

				g_POC_editing_group = (true);

				GuiLib_ActiveCursorFieldNo = CP_POC_NAME;

				Redraw_Screen( (false) );
				break;

			case KEY_NEXT:
			case KEY_PREV:
				// Change the key to either UP or DOWN so it can be processed
				// using the KEY_C_UP and KEY_C_DOWN case statement.
				if( pkey_event.keycode == KEY_NEXT )
				{
					pkey_event.keycode = KEY_C_DOWN;
				}
				else
				{
					pkey_event.keycode = KEY_C_UP;
				}
				// Don't break here. This will allow the routine to fall into
				// the next case statement which will actually move the cursor
				// up or down appropriately.

			case KEY_C_DOWN:
			case KEY_C_UP:
				SCROLL_BOX_up_or_down( 0, pkey_event.keycode );

				g_POC_current_list_item_index = GuiLib_ScrollBox_GetActiveLine( 0, 0 );

				g_GROUP_list_item_index = POC_get_index_using_ptr_to_poc_struct( POC_get_ptr_to_physically_available_poc( g_POC_current_list_item_index ) );

				POC_copy_group_into_guivars( g_GROUP_list_item_index );

				// 7/11/2014 ajv : Default the previous cursor position to the Decoder 2
				// serial number. That way, when the user presses the DOWN arrow from
				// the Master Valve field, it will jump there by default rather than the
				// K Value field.
				g_POC_prev_cursor_pos = CP_POC_DECODER_2;

				// 4/16/2014 ajv : Since the screen contents change based upon
				// what type of POC is displayed (e.g., Bypass Manifold has
				// additional flow meters), a full redraw is necessary rather
				// than just a call to Refresh_Screen().
				Redraw_Screen( (false) );
				break;

			default:
				if( pkey_event.keycode == KEY_BACK )
				{
					GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;
				}

				KEY_process_global_keys( pkey_event );
		}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

