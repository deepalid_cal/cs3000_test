/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_E_BUDGETS_H
#define _INC_E_BUDGETS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"irrigation_system.h"

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Globals for gui support when converting from EasyGui to internal data storage

// 3/25/2016 skc : Variable for handling the messiness of converting from "01/21/16" to date_time.D
extern UNS_32 g_BUDGET_SETUP_meter_read_date[ IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS ];

// 3/25/2016 skc : Variable for holding budget values while editing.
// Using a float because we may need to increment budget values using HCF units (1 HCF = 748.05 gal)
extern float g_BUDGET_SETUP_budget[ IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS ];

// 8/13/2018 Ryan : Variable for holding annual budget values while editing.
extern float g_BUDGET_SETUP_annual_budget;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FDTO_BUDGETS_draw_menu( const BOOL_32 pcomplete_redraw );

extern void BUDGETS_process_menu( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

