/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_E_TEST_SEQUENTIAL_H
#define _INC_E_TEST_SEQUENTIAL_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define STATION_ON		( true )
#define STATION_OFF		( false )

// 7/9/2015 mpd : The "GuiVar_StationInfoNumber" variable is used for irrigation stations (beginning at 1) as well as 
// the main valve, pump, and lights outputs. These defines are for non-irrigation values that require special handling.
#define STATION_NUMBER_FOR_MASTER_VALVE	    ( 0xFF01 )
#define STATION_NUMBER_FOR_PUMP			    ( 0xFF02 )
#define STATION_NUMBER_FOR_LIGHT_1		    ( 0xFF03 )
#define STATION_NUMBER_FOR_LIGHT_2		    ( 0xFF04 )
#define STATION_NUMBER_FOR_LIGHT_3		    ( 0xFF05 )
#define STATION_NUMBER_FOR_LIGHT_4		    ( 0xFF06 )
#define STATION_NUMBER_FOR_SPECIALS_MIN	    STATION_NUMBER_FOR_MASTER_VALVE
#define STATION_NUMBER_FOR_SPECIALS_MAX	    STATION_NUMBER_FOR_LIGHT_4
#define TEST_SEQ_IRRI_STATION_NUM_INVALID   ( 0 )
#define TEST_SEQ_IRRI_STATION_NUM_MIN	    ( 1 )
#define TEST_SEQ_IRRI_STATION_NUM_MAX	    ( 48 )

#define STATION_STRING_FOR_MASTER_VALVE	    ( "MV" )
#define STATION_STRING_FOR_PUMP			    ( "PMP" )
											
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FDTO_TEST_SEQ_draw_screen( const BOOL_32 pcomplete_redraw );

extern void FDTO_TEST_SEQ_update_screen();

extern void TEST_SEQ_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

