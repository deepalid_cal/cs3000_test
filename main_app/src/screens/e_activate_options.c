/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/17/2017 ajv : Required for memset
#include	<string.h>

// 6/17/2017 ajv : Required for atoi
#include	<stdlib.h>

#include	"e_activate_options.h"

#include	"cursor_utils.h"

#include	"d_comm_options.h"

#include	"e_keyboard.h"

#include	"m_main.h"

#include	"screen_utils.h"

#include	"speaker.h"

#include	"configuration_controller.h"

#include	"app_startup.h"

#include	"irri_comm.h"

#include	"dialog.h"

#include	"flash_storage.h"

#include	"hashids.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_ACTIVATE_OPTIONS_ACTIVATION_CODE				(0)
#define CP_ACTIVATE_OPTIONS_ACTIVATE_BUTTON				(1)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/** 
* This is a custom implementation of the hashids function. It is loosely modeled after the
* main.c file that's included in the original source code.
* It creates a Hash ID, of the serial number of the controller and the feature to be
* activated
*
* @mutex (none) 
*
* @memory_responsibilities (none) 
*
* @task_execution Executed within the context of the KEY_PROCESSING_TASK
* 
* @param v_buff : array holding the generated hash
* @param pserial_num : serial number of the controller for which activation code is being
* generated
*
* @author 7/18/2017 bashire
* 
* @revisions (none)
*
*/
static void generate_activation_code( char v_buff[ 30 ], const UNS_64 pserial_num )
{
	hashids_t *hashids;
	char *salt = HASHIDS_DEFAULT_SALT, *alphabet = HASHIDS_DEFAULT_ALPHABET,
		*buffer;
	size_t min_hash_length = HASHIDS_DEFAULT_MIN_HASH_LENGTH, numbers_count;
	unsigned long long *numbers, v_numbers[ 30 ];

	// ----------

	// 7/18/2017 BE: allocate local memory for the hashids_t struct: v_result;
	char v_alphabet[ 100 ], v_copy1[ 100 ], v_copy2[ 100 ], v_salt[ 20 ], v_separators[ 50 ], v_guards[ 20 ];
	hashids_t v_result, *p_result;
	v_result.alphabet = v_alphabet;
	v_result.alphabet_copy_1 = v_copy1;
	v_result.alphabet_copy_2 = v_copy2;
	v_result.salt = v_salt;
	v_result.separators = v_separators, v_result.guards = v_guards;
	buffer = v_buff;
	numbers = v_numbers;
	p_result = &v_result;

	buffer[ 0 ] = '\0';

	// ----------

	/* initialize hashids */
	hashids = hashids_init3( salt, min_hash_length, alphabet, p_result );

	/* error checking */
	if( !hashids )
	{
		// 10/14/2018 ajv : No need to stamp an alert message here because the init3 function 
		// already stamped the relevant engineering alert to notify the user what the issue was. 

		return;
	}

	/* collect numbers */
	numbers_count = 1;
	numbers[ 0 ] = pserial_num;

	/* encode, print, cleanup */
	hashids_encode( hashids, buffer, numbers_count, numbers );
}

/* ---------------------------------------------------------- */
/**
 * @description: sets the feature bit, sets flag to let master know, sounds
 * the good-beep. 
 *  
 *
 * @mutex (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the Key_Processing_Task
 *
 * @author 7/4/2017 bashire
 * @lastRevision: 7/18/2017 bashire
 */
static void save_activated_option( void )
{
	good_key_beep();

	//set flag to send new box configuration to the maaster
	// 5/1/2014 rmd : Take the mutex to protect against us missing a change due to multiple
	// processes manipulating the send_box_configuration_to_master flag.
	xSemaphoreTakeRecursive( irri_comm_recursive_MUTEX, portMAX_DELAY );

	// 4/16/2014 rmd : And trigger the configuration change to be sent to the master when the
	// next token arrives.
	irri_comm.send_box_configuration_to_master = (true);

	xSemaphoreGiveRecursive( irri_comm_recursive_MUTEX );

	//Do a delayed storage of configuration
	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_CONTROLLER_CONFIGURATION, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
}

/* ---------------------------------------------------------- */
/**
 * @description: draws screen showing the options, including the Activatio code and Activate 
 *  		   option choices
 *  
 *
 * @mutex (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the Display-processing-task
 * 
 * @param pcomplete_redraw (Input): boolean, indicating whether complete redraw will occur.
 *
 * @author: 6/20/2017 Adrianusv 
 * @lastrevison: 7/18/2017  
 */
extern void FDTO_ACTIVATE_OPTIONS_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		lcursor_to_select = CP_ACTIVATE_OPTIONS_ACTIVATION_CODE;

		// 6/17/2017 ajv : We're using GroupName here since that's what
		memset( GuiVar_GroupName, 0x00, sizeof(GuiVar_GroupName) );
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrActivateOptions_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * @description: Key processing for activating purchased options: user chooses Activation
 * code, to enter the purchased activation code OR label, "Activate Option" to use the
 *  entered code to request feature activation. If the code is correct, the feature is
 *  activated by updateing config_c.purchased_options.featureBit;  a good-beep is played,
 *  flag is set to send message, and a dialog is displayed on the controller screen, that
 *  the feature has been activated and the updated config file is stored in the Flash.
 *
 * @mutex (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the Key_Processing_Task
 * 
 * @param pkey_event (Input): pointter to key structure for the key pressed 
 *
 * @author 7/18/2017 bashire
 * @lastRevision: 7/18/2017 bashire
 */
extern void ACTIVATE_OPTIONS_process_screen( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	char 	v_buff[ 30 ];

	char	featureType;

	INT_32	strIndx;

	UNS_64	serial_num;

	// ----------

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_dlgKeyboard_0:
			if( ((pkey_event.keycode == KEY_BACK) || ((pkey_event.keycode == KEY_SELECT) && (GuiLib_ActiveCursorFieldNo == 49 /*CP_QWERTY_KEYBOARD_OK*/))) )
			{
				// 6/17/2017 ajv : Copy the modified GroupName back into the ActivationCode GuiVar.
				strlcpy( GuiVar_ActivationCode, GuiVar_GroupName, sizeof(GuiVar_ActivationCode) );
			}

			KEYBOARD_process_key( pkey_event, &FDTO_ACTIVATE_OPTIONS_draw_screen );
			break;

			// 6/17/2017 ajv : No need to process the GuiStruct_dlgActivationSuccessful dialogs since
			// they are standard "OK" dialogs and are handled automatically via the Key Processing task.

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_ACTIVATE_OPTIONS_ACTIVATION_CODE:
							good_key_beep();

							// 6/17/2017 ajv : Copy the ActivationCode into the GroupName GuiVar since that's what the
							// keyboard is expecting to edit.
							strlcpy( GuiVar_GroupName, GuiVar_ActivationCode, sizeof(GuiVar_GroupName) );

							KEYBOARD_draw_keyboard( 158, 91, 10, KEYBOARD_TYPE_QWERTY );
							break;

						case CP_ACTIVATE_OPTIONS_ACTIVATE_BUTTON:
							//Replace last character with a null, use the stripped character to determine if the feature 
							//is FL (1), HUB (2), or AQUAPONICS (3)
							strIndx = strlen( GuiVar_GroupName );
							featureType = GuiVar_GroupName[ strIndx - 1 ];
							GuiVar_GroupName[ strIndx - 1 ] = '\0';   //strip the last character and store NULL in its place

							//Produce the correct Hash Id for the controller and the feature
							serial_num = (UNS_64)config_c.serial_number;

							serial_num += atoi( &featureType );

							//call the hashId generator
							generate_activation_code( v_buff, serial_num );


							if( (strcmp( GuiVar_GroupName, v_buff )) == 0 )
							{
								//Display, relevant feature successfully activated, dialog
								if( featureType == '1' )  //feature '1' -> CS3-FL
								{
									config_c.purchased_options.option_FL = (true);

									save_activated_option();

									DIALOG_draw_ok_dialog( GuiStruct_dlgActivationSuccessful_0 );
								}
								else if( featureType == '2' ) //feature '2' ->CS3-HUB-OPT
								{
									config_c.purchased_options.option_HUB = (true);

									save_activated_option();

									DIALOG_draw_ok_dialog( GuiStruct_dlgActivationSuccessful_3 );
								}
								else if( featureType == '3' ) //feature '3' ->CS3-AQUAPONICS
								{
									config_c.purchased_options.option_AQUAPONICS = (true);

									save_activated_option();

									DIALOG_draw_ok_dialog( GuiStruct_dlgActivationSuccessful_10 );
								}
								else
								{
									bad_key_beep();

									DIALOG_draw_ok_dialog( GuiStruct_dlgActivationUnsuccessful_0 );
								}
							}
							else
							{
								bad_key_beep();

								DIALOG_draw_ok_dialog( GuiStruct_dlgActivationUnsuccessful_0 );
							}
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_C_UP:
				case KEY_C_LEFT:
					CURSOR_Up( (true) );
					break;

				case KEY_C_DOWN:
				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_BACK:
					GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;

					KEY_process_global_keys( pkey_event );

					// 6/17/2017 ajv : Since the user got here from the COMM OPTIONS dialog box, redraw that
					// dialog box.
					COMM_OPTIONS_draw_dialog( (true) );
					break;

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
