/* ---------------------------------------------------------- */

#ifndef _INC_R_RT_WEATHER_H
#define _INC_R_RT_WEATHER_H

/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */

extern void FDTO_REAL_TIME_WEATHER_update_report( void );

extern void FDTO_REAL_TIME_WEATHER_draw_report( BOOL_32 pcomplete_redraw );

extern void REAL_TIME_WEATHER_process_report( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

