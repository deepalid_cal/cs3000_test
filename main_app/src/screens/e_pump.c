/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_pump.h"

#include	"app_startup.h"

#include	"combobox.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"group_base_file.h"

#include	"station_groups.h"

#include	"m_main.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_PUMP_IN_USE_0		(0)
#define	CP_PUMP_IN_USE_1		(1)
#define	CP_PUMP_IN_USE_2		(2)
#define	CP_PUMP_IN_USE_3		(3)
#define	CP_PUMP_IN_USE_4		(4)
#define	CP_PUMP_IN_USE_5		(5)
#define	CP_PUMP_IN_USE_6		(6)
#define	CP_PUMP_IN_USE_7		(7)
#define	CP_PUMP_IN_USE_8		(8)
#define	CP_PUMP_IN_USE_9		(9)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32 *g_PUMP_combo_box_guivar;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Draws the pump use dropdown window atop the main window.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the users presses SELECT to change the pump use setting.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_PUMP_show_dropdown( const UNS_32 px_coord, const UNS_32 py_coord )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( px_coord, py_coord, *g_PUMP_combo_box_guivar );
}

/* ---------------------------------------------------------- */
extern void FDTO_PUMP_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		PUMP_copy_group_into_guivars();

		lcursor_to_select = 0;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrPump_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Processes the key event from the pump screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 11/4/2011 Adrianusv
 *
 * @revisions
 *    11/4/2011 Initial release
 */
extern void PUMP_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_cbxNoYes_0:
			COMBO_BOX_key_press( pkey_event.keycode, g_PUMP_combo_box_guivar );
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_PUMP_IN_USE_0:
							g_PUMP_combo_box_guivar = &GuiVar_GroupSettingA_0;
							break;

						case CP_PUMP_IN_USE_1:
							g_PUMP_combo_box_guivar = &GuiVar_GroupSettingA_1;
							break;

						case CP_PUMP_IN_USE_2:
							g_PUMP_combo_box_guivar = &GuiVar_GroupSettingA_2;
							break;

						case CP_PUMP_IN_USE_3:
							g_PUMP_combo_box_guivar = &GuiVar_GroupSettingA_3;
							break;

						case CP_PUMP_IN_USE_4:
							g_PUMP_combo_box_guivar = &GuiVar_GroupSettingA_4;
							break;

						case CP_PUMP_IN_USE_5:
							g_PUMP_combo_box_guivar = &GuiVar_GroupSettingA_5;
							break;

						case CP_PUMP_IN_USE_6:
							g_PUMP_combo_box_guivar = &GuiVar_GroupSettingA_6;
							break;

						case CP_PUMP_IN_USE_7:
							g_PUMP_combo_box_guivar = &GuiVar_GroupSettingA_7;
							break;

						case CP_PUMP_IN_USE_8:
							g_PUMP_combo_box_guivar = &GuiVar_GroupSettingA_8;
							break;

						case CP_PUMP_IN_USE_9:
							g_PUMP_combo_box_guivar = &GuiVar_GroupSettingA_9;
							break;
					}

					if( g_PUMP_combo_box_guivar != NULL )
					{
						good_key_beep();

						lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
						lde._04_func_ptr = (void*)&FDTO_PUMP_show_dropdown;
						lde._06_u32_argument1 = 251;
						lde._07_u32_argument2 = (GuiLib_ActiveCursorFieldNo == CP_PUMP_IN_USE_0) ? GROUP_COMBO_BOX_Y_START : (UNS_32)((GuiLib_ActiveCursorFieldNo * Pos_Y_GroupSetting) + GROUP_COMBO_BOX_Y_START);
						Display_Post_Command( &lde );
					}
					else
					{
						bad_key_beep();
					}
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_PUMP_IN_USE_0:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_0, (false), (true), 1, (true) );
							break;

						case CP_PUMP_IN_USE_1:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_1, (false), (true), 1, (true) );
							break;

						case CP_PUMP_IN_USE_2:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_2, (false), (true), 1, (true) );
							break;

						case CP_PUMP_IN_USE_3:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_3, (false), (true), 1, (true) );
							break;

						case CP_PUMP_IN_USE_4:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_4, (false), (true), 1, (true) );
							break;

						case CP_PUMP_IN_USE_5:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_5, (false), (true), 1, (true) );
							break;

						case CP_PUMP_IN_USE_6:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_6, (false), (true), 1, (true) );
							break;

						case CP_PUMP_IN_USE_7:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_7, (false), (true), 1, (true) );
							break;

						case CP_PUMP_IN_USE_8:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_8, (false), (true), 1, (true) );
							break;

						case CP_PUMP_IN_USE_9:
							process_uns32( pkey_event.keycode, &GuiVar_GroupSettingA_9, (false), (true), 1, (true) );
							break;

						default:
							bad_key_beep();
					}
					Redraw_Screen( (false) );
					break;

				case KEY_PREV:
				case KEY_C_UP:
				case KEY_C_LEFT:
					CURSOR_Up( (true) );
					break;

				case KEY_NEXT:
				case KEY_C_DOWN:
				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				case KEY_BACK:
					GuiVar_MenuScreenToShow = CP_MAIN_MENU_POCS;
					PUMP_extract_and_store_changes_from_GuiVars();
					// No need to break here to allow this to fall into the default
					// condition.

				default:
					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

