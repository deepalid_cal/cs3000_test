/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"device_common.h"

#include	"e_gr_programming_airlink.h"

#include	"configuration_controller.h"

#include	"dialog.h"

#include	"d_comm_options.h"

#include	"d_device_exchange.h"

#include	"d_process.h"

#include	"speaker.h"

#include	"comm_mngr.h"

#include	"app_startup.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_GR_PROGRAMMING_READ_DEVICE					(0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/22/2015 ajv : Since only the draw fuction is initially told which port the device is
// on, copy that port into a global variable that's accessible from the key processing task.
static UNS_32	g_GR_PROGRAMMING_AIRLINK_port;

static BOOL_32 GR_PROGRAMMING_AIRLINK_querying_device;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void start_gr_device_inquiry( const UNS_32 pport )
{
	GR_PROGRAMMING_AIRLINK_querying_device = (true);

	// 3/18/2014 rmd : Post the event to cause the comm_mngr to perform the device settings
	// readout and write the results to each of the guivars.

	COMM_MNGR_TASK_QUEUE_STRUCT	cmqs;
	
	cmqs.event = COMM_MNGR_EVENT_request_read_device_settings;

	cmqs.port = pport;
				
	COMM_MNGR_post_event_with_details( &cmqs );
}
	
/* ---------------------------------------------------------- */
static void GR_PROGRAMMING_AIRLINK_initialize_guivars( void )
{
	// 4/20/2015 ajv : Set the device exchange result to 'read_ok' to display the initialized variables.
	GuiVar_CommOptionDeviceExchangeResult = DEVICE_EXCHANGE_KEY_read_settings_completed_ok;

	// ---------------------
	// Build the radio model

	strlcpy( GuiVar_GRMake, "Sierra Wireless AirLink", sizeof(GuiVar_GRMake) );

	// ati0
	strlcpy( GuiVar_GRModel, "-----", sizeof(GuiVar_GRModel) );

	// ---------------------
	// Populate the SIM card information
	
	// at*netphone?
	strlcpy( GuiVar_GRPhoneNumber, "-----------", sizeof(GuiVar_GRPhoneNumber) );

	// at*netip?
	strlcpy( GuiVar_GRIPAddress, "---.---.---.---", sizeof(GuiVar_GRIPAddress) );

	// at+iccid?
	strlcpy( GuiVar_GRSIMID, "--------------------", sizeof(GuiVar_GRSIMID) );

	// ---------------------
	// Populate the network statistics

	// at*netstate?
	strlcpy( GuiVar_GRNetworkState, "-------------", sizeof(GuiVar_GRNetworkState) );

	// at*netop?
	strlcpy( GuiVar_GRCarrier, "----", sizeof(GuiVar_GRCarrier) );

	// at*netserv?
	strlcpy( GuiVar_GRService, "----", sizeof(GuiVar_GRService) );

	// at*netrssi?
	strlcpy( GuiVar_GRRSSI, "----", sizeof(GuiVar_GRRSSI) );

	// at+ecio?
	strlcpy( GuiVar_GRECIO, "----", sizeof(GuiVar_GRRSSI) );

	// ---------------------
	// Populate the radio information
	
	// at*globalid?
	strlcpy( GuiVar_GRRadioSerialNum, "---------------", sizeof(GuiVar_GRRadioSerialNum) );

	// ati1 (first line of response only)
	strlcpy( GuiVar_GRFirmwareVersion, "---------", sizeof(GuiVar_GRFirmwareVersion) );
}

/* ---------------------------------------------------------- */
static void FDTO_GR_PROGRAMMING_AIRLINK_process_device_exchange_key( const UNS_32 pkeycode )
{
	switch( pkeycode )
	{
		case DEVICE_EXCHANGE_KEY_read_settings_completed_ok:
		case DEVICE_EXCHANGE_KEY_write_settings_completed_ok:
			// 4/22/2015 ajv : TODO Get values from the programming struct

			// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
			GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( DEVICE_EXCHANGE_KEY_read_settings_completed_ok );

			// 5/18/2015 ajv : Update the progress dialog.
			DEVICE_EXCHANGE_draw_dialog();
			break;

		case DEVICE_EXCHANGE_KEY_busy_try_again_later:
		case DEVICE_EXCHANGE_KEY_read_settings_error:
		case DEVICE_EXCHANGE_KEY_read_settings_in_progress:
		case DEVICE_EXCHANGE_KEY_write_settings_error:
		case DEVICE_EXCHANGE_KEY_write_settings_in_progress:
			// 6/1/2015 mpd : Put the device exchange result into a range EasyGUI can handle.
			GuiVar_CommOptionDeviceExchangeResult = e_SHARED_index_keycode_for_gui( pkeycode );

			// 5/18/2015 ajv : Ensure the flag that indicates a dialog box is open is set to false.
			DIALOG_close_ok_dialog();

			FDTO_GR_PROGRAMMING_AIRLINK_draw_screen( (false), g_GR_PROGRAMMING_AIRLINK_port );
			break;
	}
}

/* ---------------------------------------------------------- */
extern void FDTO_GR_PROGRAMMING_AIRLINK_draw_screen( const BOOL_32 pcomplete_redraw, const UNS_32 pport )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		GR_PROGRAMMING_AIRLINK_initialize_guivars();

		g_GR_PROGRAMMING_AIRLINK_port = pport;

		lcursor_to_select = CP_GR_PROGRAMMING_READ_DEVICE;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrGRProgramming_AirLink_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();

	if( pcomplete_redraw == (true) )
	{
		// 5/18/2015 ajv : Immediately display the dialog to indicate to the user that something is
		// happening
		DEVICE_EXCHANGE_draw_dialog();

		// 3/21/2014 ajv : Start the data retrieval automatically for the user.
		// This isn't necessary, but it's a nicety, preventing the user from
		// having to press the Read Radio button.
		start_gr_device_inquiry( pport );
	}
}

/* ---------------------------------------------------------- */
extern void GR_PROGRAMMING_AIRLINK_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( pkey_event.keycode )
	{
		case DEVICE_EXCHANGE_KEY_busy_try_again_later:
		case DEVICE_EXCHANGE_KEY_read_settings_completed_ok:
		case DEVICE_EXCHANGE_KEY_read_settings_error:
		case DEVICE_EXCHANGE_KEY_read_settings_in_progress:
		case DEVICE_EXCHANGE_KEY_write_settings_completed_ok:
		case DEVICE_EXCHANGE_KEY_write_settings_error:
		case DEVICE_EXCHANGE_KEY_write_settings_in_progress:

			if( (pkey_event.keycode != DEVICE_EXCHANGE_KEY_read_settings_in_progress) && (pkey_event.keycode != DEVICE_EXCHANGE_KEY_write_settings_in_progress) )
			{
				GR_PROGRAMMING_AIRLINK_querying_device = (false);
			}

			// 3/18/2014 rmd : Redraw the screen displaying the results or
			// indicating the error condition.
			lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
			lde._04_func_ptr = (void*)&FDTO_GR_PROGRAMMING_AIRLINK_process_device_exchange_key;
			lde._06_u32_argument1 = pkey_event.keycode;
			Display_Post_Command( &lde );
			break;

		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_GR_PROGRAMMING_READ_DEVICE:
					if( GR_PROGRAMMING_AIRLINK_querying_device == (false) )
					{
						good_key_beep();
						lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
						lde._04_func_ptr = (void*)&FDTO_GR_PROGRAMMING_AIRLINK_draw_screen;
						lde._06_u32_argument1 = (true);
						lde._07_u32_argument2 = g_GR_PROGRAMMING_AIRLINK_port;
						Display_Post_Command( &lde );
					}
					else
					{
						bad_key_beep();
					}
					break;

				default:
					bad_key_beep();
			}
			break;
		
		case KEY_BACK:
			GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;

			KEY_process_global_keys( pkey_event );

			// 6/5/2015 ajv : Since the user got here from the COMM OPTIONS dialog box, redraw that
			// dialog box.
			COMM_OPTIONS_draw_dialog( (true) );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

