/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"r_flow_checking.h"

#include	"app_startup.h"

#include	"battery_backed_vars.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"foal_irri.h"

#include	"m_main.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32 g_FLOW_CHECKING_index_of_system_to_show;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Updates the variables used to display the Flow Checking statistics and forces
 * a screen redraw.
 * 
 * @executed Executed within the context of the display processing task when the
 *  		 screen is initially drawn; within the context of the TD check task
 *           as the screen is updated 4 times per second.
 *
 * @author 6/14/2012 Adrianusv
 *
 * @revisions
 *    6/14/2012   Initial release
 */
static void FDTO_FLOW_CHECKING_STATS_redraw_report( void )
{
	FDTO_SYSTEM_load_system_name_into_guivar( g_FLOW_CHECKING_index_of_system_to_show );

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	GuiVar_FlowCheckingStable = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].sbf.stable_flow;

	GuiVar_FlowCheckingAllowed = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].sbf.flow_checking_enabled_and_allowed;
	GuiVar_FlowCheckingFMs = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].sbf.number_of_flow_meters_in_this_sys;
	GuiVar_FlowCheckingHasNCMV = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].sbf.master_valve_has_at_least_one_normally_closed;
	//GuiVar_FlowCheckingTallysUsingExpecteds = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].sbf.tallys_driven_with_expecteds;

	GuiVar_FlowCheckingFlag0 = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].sbf.system_level_no_valves_ON_therefore_no_flow_checking;
	GuiVar_FlowCheckingFlag1 = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].sbf.system_level_valves_are_ON_but_will_not_be_checking_flow;
	GuiVar_FlowCheckingFlag2 = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].sbf.system_level_valves_are_ON_and_waiting_to_check_flow;
	GuiVar_FlowCheckingFlag3 = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].sbf.system_level_valves_are_ON_and_actively_checking;
	GuiVar_FlowCheckingFlag4 = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].sbf.system_level_valves_are_ON_and_waiting_to_update_derate_table;
	GuiVar_FlowCheckingFlag5 = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].sbf.system_level_valves_are_ON_and_has_updated_the_derate_table;
	GuiVar_FlowCheckingFlag6 = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].sbf.system_level_valves_are_ON_and_waiting_to_acquire_expected;

	GuiVar_FlowCheckingChecked = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].sbf.checked_or_updated_and_made_flow_recording_lines;

	GuiVar_FlowCheckingLFTime = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].flow_checking_block_out_remaining_seconds;
	GuiVar_FlowCheckingINTime = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].inhibit_next_turn_ON_remaining_seconds;

	GuiVar_FlowCheckingMVJOTime = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].timer_MVJO_flow_checking_blockout_seconds_remaining;
	GuiVar_FlowCheckingStopIrriTime = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].timer_MLB_just_stopped_irrigating_blockout_seconds_remaining;
	GuiVar_FlowCheckingMVTime = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].transition_timer_all_stations_are_OFF;

	// Since the master_valve_open_all is set TRUE when it's OPEN and FALSE when
	// it's CLOSED, invert the value to display either open or closed.
	GuiVar_FlowCheckingMVState = !(system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].sbf.mv_open_for_irrigation);

	GuiVar_FlowCheckingPumpTime = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].transition_timer_all_pump_valves_are_OFF;
	GuiVar_FlowCheckingPumpState = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].sbf.pump_activate_for_irrigation;
	
	GuiVar_FlowCheckingNumOn = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].system_master_number_of_valves_ON;
	GuiVar_FlowCheckingExpOn = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].ufim_expected_flow_rate_for_those_ON;
	GuiVar_FlowCheckingOnForProbList = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].ufim_one_ON_from_the_problem_list_b;

	GuiVar_FlowCheckingExp = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].flow_check_derated_expected;
	GuiVar_FlowCheckingHi = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].flow_check_hi_limit;
	GuiVar_FlowCheckingLo = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].flow_check_lo_limit;

	GuiVar_FlowCheckingCycles = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].flow_check_required_station_cycles;
	GuiVar_FlowCheckingIterations = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].flow_check_required_cell_iteration;
	GuiVar_FlowCheckingAllowedToLock = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].flow_check_allow_table_to_lock;
	GuiVar_FlowCheckingSlotSize = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].flow_check_derate_table_gpm_slot_size;
	GuiVar_FlowCheckingMaxOn = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].flow_check_derate_table_max_stations_ON;
	GuiVar_FlowCheckingSlots = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].flow_check_derate_table_number_of_gpm_slots;

	GuiVar_FlowCheckingGroupCnt0 = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].ufim_flow_check_group_count_of_ON[ 0 ];
	GuiVar_FlowCheckingGroupCnt1 = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].ufim_flow_check_group_count_of_ON[ 1 ];
	GuiVar_FlowCheckingGroupCnt2 = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].ufim_flow_check_group_count_of_ON[ 2 ];
	GuiVar_FlowCheckingGroupCnt3 = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].ufim_flow_check_group_count_of_ON[ 3 ];

	GuiVar_FlowCheckingPumpMix = system_preserves.system[ g_FLOW_CHECKING_index_of_system_to_show ].ufim_there_is_a_PUMP_mix_condition_b;

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Draws the Flow Checking report.
 * 
 * @executed Executed within the context of the display processing task when the
 *  		 screen is initially drawn; within the context of the TD check task
 *           as the screen is updated 4 times per second.
 * 
 * @param pcomplete_redraw True if the screen is to be redrawn completely
 *  				        (which should only happen on initial entry into the
 *                          screen); otherwise, false.
 *
 * @author 6/14/2012 Adrianusv
 *
 * @revisions
 *    6/14/2012   Initial release
 */
extern void FDTO_FLOW_CHECKING_STATS_draw_report( const BOOL_32 pcomplete_redraw )
{
	if( pcomplete_redraw == (true) )
	{
		GuiLib_ShowScreen( GuiStruct_rptFlowCheckingStats_0, GuiLib_NO_CURSOR, GuiLib_RESET_AUTO_REDRAW );

		g_FLOW_CHECKING_index_of_system_to_show = 0;
	}

    FDTO_FLOW_CHECKING_STATS_redraw_report();
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while on the Flow Checking report.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 *
 * @param pkey_event The key event to be processed.
 *
 * @author 6/14/2012 Adrianusv
 *
 * @revisions
 *    6/14/2012   Initial release
 */
extern void FLOW_CHECKING_STATS_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	UNS_32	lkey;

	switch( pkey_event.keycode )
	{
		case KEY_PLUS:
		case KEY_MINUS:
		case KEY_NEXT:
		case KEY_PREV:
			if( pkey_event.keycode == KEY_NEXT )
			{
				lkey = KEY_PLUS;
			}
			else if( pkey_event.keycode == KEY_PREV )
			{
				lkey = KEY_MINUS;
			}
			else
			{
				lkey = pkey_event.keycode;
			}

			process_uns32( lkey, &g_FLOW_CHECKING_index_of_system_to_show, 0, (MAX_POSSIBLE_SYSTEMS-1), 1, (true) );

			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = (void*)&FDTO_FLOW_CHECKING_STATS_redraw_report;
			Display_Post_Command( &lde );
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = CP_MAIN_MENU_DIAGNOSTICS;

			KEY_process_global_keys( pkey_event );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

