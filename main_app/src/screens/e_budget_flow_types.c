/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"e_budget_flow_types.h"

#include	"app_startup.h"

#include	"cursor_utils.h"

#include	"epson_rx_8025sa.h"

#include	"group_base_file.h"

#include	"irrigation_system.h"

#include	"m_main.h"

#include	"screen_utils.h"

//#include	"station_groups.h"

#include	"speaker.h"

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// 4/12/2016 skc : Budgets and Flow Types

// These need to match the Cursor fields defined in EasyGUI
#define	CP_BUDGET_FLOW_TYPE_IRRIGATION		(1)
#define	CP_BUDGET_FLOW_TYPE_MVOR			(2)
#define	CP_BUDGET_FLOW_TYPE_NON_CONTROLLER	(3)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static BOOL_32	g_BUDGET_SETUP_editing_group;

static void FDTO_BUDGET_FLOW_TYPES_return_to_menu( void );

/* ---------------------------------------------------------- */
extern void BUDGET_FLOW_TYPES_process_group( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{

	switch( pkey_event.keycode )
	{
		case KEY_SELECT:
		case KEY_MINUS:
		case KEY_PLUS:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_BUDGET_FLOW_TYPE_IRRIGATION:
					// 4/21/2016 skc : We don't allow the user to change this.
					bad_key_beep(); 
					//process_bool( &GuiVar_BudgetFlowTypeIrrigation );
					break;

				case CP_BUDGET_FLOW_TYPE_MVOR:
					process_bool( &GuiVar_BudgetFlowTypeMVOR );
					break;

				case CP_BUDGET_FLOW_TYPE_NON_CONTROLLER:
					process_bool( &GuiVar_BudgetFlowTypeNonController );
					break;

				default:
					bad_key_beep();
			}
			Refresh_Screen();
			break;

		case KEY_NEXT:
		case KEY_PREV:
			GROUP_process_NEXT_and_PREV( pkey_event.keycode, SYSTEM_num_systems_in_use(), (void*)&BUDGET_FLOW_TYPES_extract_and_store_changes_from_GuiVars, (void*)&SYSTEM_get_group_at_this_index, (void*)&BUDGET_FLOW_TYPES_copy_group_into_guivars );
			break;


		case KEY_C_UP:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				default:
					CURSOR_Up( (true) );

					// 2/23/2017 ajv : Since a context-sensitive description displays based on the field
					// selected, a redraw is required instead of just the standard refresh that the CURSOR_Up
					// routine performs.
					Redraw_Screen( (false) );
			}
			break;

		case KEY_C_LEFT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_BUDGET_FLOW_TYPE_IRRIGATION:
					KEY_process_BACK_from_editing_screen( (void*)&FDTO_BUDGET_FLOW_TYPES_return_to_menu );
					break;
				default:
					CURSOR_Up( true );

					// 2/23/2017 ajv : Since a context-sensitive description displays based on the field
					// selected, a redraw is required instead of just the standard refresh that the CURSOR_Up
					// routine performs.
					Redraw_Screen( (false) );
			}
			break;

		case KEY_C_DOWN:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				default:
					CURSOR_Down( true );

					// 2/23/2017 ajv : Since a context-sensitive description displays based on the field
					// selected, a redraw is required instead of just the standard refresh that the CURSOR_Down
					// routine performs.
					Redraw_Screen( (false) );
			}
			break;

		case KEY_C_RIGHT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				default:
					CURSOR_Down( true );

					// 2/23/2017 ajv : Since a context-sensitive description displays based on the field
					// selected, a redraw is required instead of just the standard refresh that the CURSOR_Down
					// routine performs.
					Redraw_Screen( (false) );
			}
			break;

		case KEY_BACK:
			KEY_process_BACK_from_editing_screen( (void*)&FDTO_BUDGET_FLOW_TYPES_return_to_menu );
			break;

		default:
			KEY_process_global_keys( pkey_event );
	}

	// 4/21/2016 skc : Display explanations for the flow types.
	GuiVar_BudgetFlowTypeIndex = GuiLib_ActiveCursorFieldNo;
}

/* ---------------------------------------------------------- */
static void FDTO_BUDGET_FLOW_TYPES_return_to_menu( void )
{
	FDTO_GROUP_return_to_menu( &g_BUDGET_SETUP_editing_group, (void*)&BUDGET_FLOW_TYPES_extract_and_store_changes_from_GuiVars );
}

/* ---------------------------------------------------------- */
extern void FDTO_BUDGET_FLOW_TYPES_draw_menu( const BOOL_32 pcomplete_redraw )
{
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	FDTO_GROUP_draw_menu( pcomplete_redraw, &g_BUDGET_SETUP_editing_group, SYSTEM_num_systems_in_use(), 
						  GuiStruct_scrBudgetFlowTypes_0, (void *)&SYSTEM_load_system_name_into_scroll_box_guivar, 
						  (void *)&BUDGET_FLOW_TYPES_copy_group_into_guivars, (false) );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	// 2/23/2017 ajv : Only initialize the flow type if we're performing a full redraw of the
	// screen.
	if( pcomplete_redraw )
	{
		// 4/27/2016 skc : Initialize flow type description.
		GuiVar_BudgetFlowTypeIndex = CP_BUDGET_FLOW_TYPE_IRRIGATION;
	}
}

/* ---------------------------------------------------------- */
extern void BUDGET_FLOW_TYPES_process_menu( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	GROUP_process_menu( pkey_event, &g_BUDGET_SETUP_editing_group, SYSTEM_num_systems_in_use(), (void *)&BUDGET_FLOW_TYPES_process_group, NULL, (void *)&SYSTEM_get_group_at_this_index, (void *)&BUDGET_FLOW_TYPES_copy_group_into_guivars );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


