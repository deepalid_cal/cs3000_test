/* ---------------------------------------------------------- */

#include	"e_contrast_and_speaker_vol.h"

#include	"contrast_and_speaker_vol.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"gpio_setup.h"

#include	"flash_storage.h"

#include	"lcd_init.h"

#include	"m_main.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */

#define	CP_CBV_CONTRAST				(0)
#define	CP_CBV_BACKLIGHT			(1)
#define	CP_CBV_VOLUME				(2)

/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void FDTO_LCD_draw_backlight_contrast_and_volume( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		if( display_model_is == DISPLAY_MODEL_KYOCERA_KG057 )
		{
			lcursor_to_select = CP_CBV_CONTRAST;
		}
		else
		{
			lcursor_to_select = CP_CBV_BACKLIGHT;
		}
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	CONTRAST_AND_SPEAKER_VOL__copy_settings_into_guivars();

	GuiLib_ShowScreen( GuiStruct_scrContrastBacklightAndVolume_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void LCD_process_backlight_contrast_and_volume( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	BOOL_32	ladjustment_complete;

	ladjustment_complete = (false);

	switch( pkey_event.keycode )
	{
		case KEY_PLUS:
		case KEY_C_RIGHT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_CBV_CONTRAST:
					if( LCD_contrast_DARKEN() == (true) )
					{
						ladjustment_complete = (true);
					}
					else
					{
						bad_key_beep();
					}
					break;

				case CP_CBV_BACKLIGHT:
					if( LED_backlight_brighter() == (true) )
					{
						ladjustment_complete = (true);
					}
					else
					{
						bad_key_beep();
					}
					break;

				case CP_CBV_VOLUME:
					if( SPEAKER_vol_increase() == (true) )
					{
						ladjustment_complete = (true);
					}
					else
					{
						bad_key_beep();
					}
					break;
			}
			break;

		case KEY_MINUS:
		case KEY_C_LEFT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_CBV_CONTRAST:
					if( LCD_contrast_LIGHTEN() == (true) )
					{
						ladjustment_complete = (true);
					}
					else
					{
						bad_key_beep();
					}
					break;

				case CP_CBV_BACKLIGHT:
					if( LED_backlight_darker() == (true) )
					{
						ladjustment_complete = (true);
					}
					else
					{
						bad_key_beep();
					}
					break;

				case CP_CBV_VOLUME:
					if( SPEAKER_vol_decrease() == (true) )
					{
						ladjustment_complete = (true);
					}
					else
					{
						bad_key_beep();
					}
					break;
			}
			break;

		case KEY_C_UP:
			CURSOR_Up( (true) );
			break;

		case KEY_C_DOWN:
			CURSOR_Down( (true) );
			break;

		default:
			if( pkey_event.keycode == KEY_BACK )
			{
				GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;
			}

			KEY_process_global_keys( pkey_event );
			break;
	}

	if( ladjustment_complete == (true) )
	{
		good_key_beep();

		lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
		lde._04_func_ptr = FDTO_LCD_draw_backlight_contrast_and_volume;
		lde._06_u32_argument1 = (false);
		Display_Post_Command( &lde );

		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_CONTRAST_AND_VOLUME, 5 );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

