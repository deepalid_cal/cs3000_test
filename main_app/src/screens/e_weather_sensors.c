/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_weather_sensors.h"

#include	"app_startup.h"

#include	"battery_backed_vars.h"

#include	"combobox.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"irri_comm.h"

#include	"m_main.h"

#include	"speaker.h"

#include	"weather_control.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_ET_GAGE_IN_USE						(0)
#define CP_ET_GAGE_INSTALLED_AT					(1)
#define	CP_ET_GAGE_LOG_PULSES					(2)
#define	CP_ET_GAGE_PERCENT_FULL					(3)
#define	CP_ET_GAGE_SKIP_TONIGHT					(4)

#define	CP_RAIN_BUCKET_IN_USE					(5)
#define	CP_RAIN_BUCKET_INSTALLED_AT				(6)
#define CP_RAIN_BUCKET_MINIMUM					(7)
#define CP_RAIN_BUCKET_MAX_HOURLY				(8)
#define CP_RAIN_BUCKET_24_HOUR_MAX				(9)

#define	CP_WIND_GAGE_IN_USE						(10)
#define	CP_WIND_GAGE_INSTALLED_AT				(11)
#define	CP_WIND_GAGE_PAUSE_SPEED				(12)
#define	CP_WIND_GAGE_RESUME_SPEED				(13)

#define	CP_ET_GAGE_CLEAR_RUNAWAY_GAGE			(14)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32 WEATHER_GuiVar_num_of_dash_w_controllers;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void process_weather_sensor_installed_at( const UNS_32 pkeycode, UNS_32 *pinstalled_at_index_ptr, char *pinstalled_at_ptr, const UNS_32 psize_of_installed_at )
{
	process_uns32( pkeycode, pinstalled_at_index_ptr, 0, (WEATHER_GuiVar_num_of_dash_w_controllers - 1), 1, (WEATHER_GuiVar_num_of_dash_w_controllers > 1) );

	snprintf( pinstalled_at_ptr, psize_of_installed_at, "%c", WEATHER_GuiVar_box_indexes_with_dash_w_option[ *pinstalled_at_index_ptr ] + 'A' );

	Refresh_Screen();
}

/* ---------------------------------------------------------- */
/**
 * Draws the ET Gage in use dropdown window atop the main window.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the users presses SELECT to change the ET Gage in use setting.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_ET_GAGE_show_et_gage_in_use_dropdown( void )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( 106, 14, GuiVar_ETGageInUse );
}

/* ---------------------------------------------------------- */
/**
 * Draws the log pulses dropdown window atop the main window.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the users presses SELECT to change the log pulses setting.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_ET_GAGE_show_log_pulses_dropdown( void )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( 202, 44, GuiVar_ETGageLogPulses );
}

/* ---------------------------------------------------------- */
/**
 * Draws the skip tonight dropdown window atop the main window.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the users presses SELECT to change the skip tonight setting.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_ET_GAGE_show_skip_tonight_dropdown( void )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( 202, 72, GuiVar_ETGageSkipTonight );
}

/* ---------------------------------------------------------- */
extern void ET_GAGE_clear_runaway_gage( void )
{
	if( (weather_preserves.run_away_gage == (true)) && (irri_comm.clear_runaway_gage_pressed == (false)) )
	{
		good_key_beep();

		DIALOG_draw_ok_dialog( GuiStruct_dlgRunawayGageCleared_0 );

		irri_comm.clear_runaway_gage_pressed = (true);
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/**
 * Draws the Rain Bucket in use dropdown window atop the main window.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the users presses SELECT to change the Rain Bucket in use setting.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_RAIN_BUCKET_show_rain_bucket_in_use_dropdown( void )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( 116, 90, GuiVar_RainBucketInUse );
}

/* ---------------------------------------------------------- */
/**
 * Draws the Wind Gage in use dropdown window atop the main window.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the users presses SELECT to change the Wind Gage in use setting.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_WIND_GAGE_show_wind_gage_in_use_dropdown( void )
{
	FDTO_COMBO_BOX_show_no_yes_dropdown( 107, 166, GuiVar_WindGageInUse );
}

/* ---------------------------------------------------------- */
extern void FDTO_WEATHER_SENSORS_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	UNS_32	i;

	if( pcomplete_redraw == (true) )
	{
		// 10/16/2014 ajv : Assume there are no -W controllers from the start
		// and increment this as they're found
		WEATHER_GuiVar_num_of_dash_w_controllers = 0;

		// 10/17/2014 ajv : Since we're directly accessing chain members, make
		// sure we're allowed to.
		if( COMM_MNGR_network_is_available_for_normal_use() == (true) )
		{
			xSemaphoreTakeRecursive( chain_members_recursive_MUTEX, portMAX_DELAY );

			for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
			{
				// 10/20/2014 ajv : TODO We have a potential issue here. If we
				// limit the selection to just controllers with a -W option
				// installed, we get into a weird situation when a -W option is
				// removed and the user navigates to the Weather Sensors screen.
				if( (chain.members[ i ].saw_during_the_scan) ) //&& (chain.members[ i ].box_configuration.wi.weather_card_present && chain.members[ i ].box_configuration.wi.weather_terminal_present) )
				{
					WEATHER_GuiVar_box_indexes_with_dash_w_option[ WEATHER_GuiVar_num_of_dash_w_controllers ] = i;

					// 10/16/2014 ajv : Increment to get to the next slot
					WEATHER_GuiVar_num_of_dash_w_controllers++;
				}
			}

			xSemaphoreGiveRecursive( chain_members_recursive_MUTEX );
		}

		WEATHER_copy_et_gage_settings_into_GuiVars();
		WEATHER_copy_rain_bucket_settings_into_GuiVars();
		WEATHER_copy_wind_gage_settings_into_GuiVars();

		lcursor_to_select = CP_ET_GAGE_IN_USE;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrWeatherSensors_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Processes the key event from the ET Gage Setup screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user presses a key on the ET Gage Setup screen.
 * 
 * @param pkey_event The key event to process.
 *
 * @author 8/28/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void WEATHER_SENSORS_process_screen( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_cbxNoYes_0:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_ET_GAGE_IN_USE:
					COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_ETGageInUse );
					break;

				case CP_ET_GAGE_LOG_PULSES:
					COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_ETGageLogPulses );
					break;

				case CP_ET_GAGE_SKIP_TONIGHT:
					COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_ETGageSkipTonight );
					break;

				case CP_RAIN_BUCKET_IN_USE:
					COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_RainBucketInUse );
					break;

				case CP_WIND_GAGE_IN_USE:
					COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_WindGageInUse );
					break;
			}
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_ET_GAGE_IN_USE:
						case CP_ET_GAGE_LOG_PULSES:
						case CP_ET_GAGE_SKIP_TONIGHT:
						case CP_RAIN_BUCKET_IN_USE:
						case CP_WIND_GAGE_IN_USE:

							good_key_beep();

							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;

							if( GuiLib_ActiveCursorFieldNo == CP_ET_GAGE_IN_USE )
							{
								lde._04_func_ptr = (void*)&FDTO_ET_GAGE_show_et_gage_in_use_dropdown;
							}
							else if( GuiLib_ActiveCursorFieldNo == CP_ET_GAGE_LOG_PULSES )
							{
								lde._04_func_ptr = (void*)&FDTO_ET_GAGE_show_log_pulses_dropdown;
							}
							else if( GuiLib_ActiveCursorFieldNo == CP_ET_GAGE_SKIP_TONIGHT )
							{
								lde._04_func_ptr = (void*)&FDTO_ET_GAGE_show_skip_tonight_dropdown;
							}
							else if( GuiLib_ActiveCursorFieldNo == CP_RAIN_BUCKET_IN_USE )
							{
								lde._04_func_ptr = (void*)&FDTO_RAIN_BUCKET_show_rain_bucket_in_use_dropdown;
							}
							else if( GuiLib_ActiveCursorFieldNo == CP_WIND_GAGE_IN_USE )
							{
								lde._04_func_ptr = (void*)&FDTO_WIND_GAGE_show_wind_gage_in_use_dropdown;
							}

							Display_Post_Command( &lde );
							break;

						case CP_ET_GAGE_CLEAR_RUNAWAY_GAGE:
							ET_GAGE_clear_runaway_gage();
							break;

						default:
							bad_key_beep();
					}
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					switch ( GuiLib_ActiveCursorFieldNo )
					{
						case CP_ET_GAGE_IN_USE:
							process_bool( &GuiVar_ETGageInUse );

							// Since we have to show or hide the various ET Gage
							// settings based upon this value, redraw the entire screen.
							Redraw_Screen( (false) );
							break;

						case CP_ET_GAGE_INSTALLED_AT:
							process_weather_sensor_installed_at( pkey_event.keycode, &WEATHER_GuiVar_ETGageInstalledAtIndex, (char*)&GuiVar_ETGageInstalledAt, sizeof(GuiVar_ETGageInstalledAt) );
							break;

						case CP_ET_GAGE_LOG_PULSES:
							process_bool( &GuiVar_ETGageLogPulses );
							Refresh_Screen();
							break;

						case CP_ET_GAGE_PERCENT_FULL:
							process_uns32( pkey_event.keycode, &GuiVar_ETGagePercentFull, 1, 100, 1, (false) );
							GuiVar_ETGageSkipTonight = (true);

							// Since we have to show or hide a message describing what
							// Skip Tonight does, redraw the entire screen.
							Redraw_Screen( (false) );
							break;

						case CP_ET_GAGE_SKIP_TONIGHT:
							process_bool( &GuiVar_ETGageSkipTonight );

							// Since we have to show or hide a message describing what
							// Skip Tonight does, redraw the entire screen.
							Redraw_Screen( (false) );
							break;

						case CP_RAIN_BUCKET_IN_USE:
							process_bool( &GuiVar_RainBucketInUse );

							// Since we have to show or hide the various Rain Bucket
							// settings based upon this value, redraw the entire
							// screen.
							Redraw_Screen( (false) );
							break;

						case CP_RAIN_BUCKET_INSTALLED_AT:
							process_weather_sensor_installed_at( pkey_event.keycode, &WEATHER_GuiVar_RainBucketInstalledAtIndex, (char*)&GuiVar_RainBucketInstalledAt, sizeof(GuiVar_RainBucketInstalledAt) );
							break;

						case CP_RAIN_BUCKET_MINIMUM:
							process_fl( pkey_event.keycode, &GuiVar_RainBucketMinimum, (WEATHER_RAIN_BUCKET_MINIMUM_MIN_100u / 100.0F), (WEATHER_RAIN_BUCKET_MINIMUM_MAX_100u / 100.0F), 0.01F, (false) );
							Refresh_Screen();
							break;

						case CP_RAIN_BUCKET_MAX_HOURLY:
							process_fl( pkey_event.keycode, &GuiVar_RainBucketMaximumHourly, (WEATHER_RAIN_BUCKET_MAXIMUM_HOURLY_MIN_100u / 100.0F), (WEATHER_RAIN_BUCKET_MAXIMUM_HOURLY_MAX_100u / 100.0F), 0.01F, (false) );
							Refresh_Screen();
							break;

						case CP_RAIN_BUCKET_24_HOUR_MAX:
							process_fl( pkey_event.keycode, &GuiVar_RainBucketMaximum24Hour, (WEATHER_RAIN_BUCKET_MAXIMUM_24_HOUR_MIN_100u / 100.0F), (WEATHER_RAIN_BUCKET_MAXIMUM_24_HOUR_MAX_100u / 100.0F), 0.01F, (false) );
							Refresh_Screen();
							break;

						case CP_WIND_GAGE_IN_USE:
							process_bool( &GuiVar_WindGageInUse );

							// Since we have to show or hide the various Wind Gage
							// settings based upon this value, redraw the entire
							// screen.
							Redraw_Screen( (false) );
							break;

						case CP_WIND_GAGE_INSTALLED_AT:
							process_weather_sensor_installed_at( pkey_event.keycode, &WEATHER_GuiVar_WindGageInstalledAtIndex, (char*)&GuiVar_WindGageInstalledAt, sizeof(GuiVar_WindGageInstalledAt) );
							break;

						case CP_WIND_GAGE_PAUSE_SPEED:
							// The minimum pause speed is 1 mph less than the pause
							// speed.
							process_uns32( pkey_event.keycode, &GuiVar_WindGagePauseSpeed, (GuiVar_WindGageResumeSpeed+1), WEATHER_WIND_PAUSE_SPEED_MAX, 1, (false) );
							Refresh_Screen();
							break;

						case CP_WIND_GAGE_RESUME_SPEED:
							// The maximum resume speed is 1 mph less than the pause
							// speed.
							process_uns32( pkey_event.keycode, &GuiVar_WindGageResumeSpeed, WEATHER_WIND_RESUME_SPEED_MIN, (GuiVar_WindGagePauseSpeed-1), 1, (false) );
							Refresh_Screen();
							break;
					}
					break;

				case KEY_C_UP:
				case KEY_C_LEFT:
					CURSOR_Up( (true) );
					break;

				case KEY_C_DOWN:
				case KEY_C_RIGHT:
					CURSOR_Down( (true) );
					break;

				default:
					if( pkey_event.keycode == KEY_BACK )
					{
						WEATHER_extract_and_store_et_gage_changes_from_GuiVars();
						WEATHER_extract_and_store_rain_bucket_changes_from_GuiVars();
						WEATHER_extract_and_store_wind_gage_changes_from_GuiVars();

						GuiVar_MenuScreenToShow = CP_MAIN_MENU_WEATHER;
					}

					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

