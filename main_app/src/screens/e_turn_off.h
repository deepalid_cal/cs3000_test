/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_E_TURN_OFF_H
#define _INC_E_TURN_OFF_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FDTO_TURN_OFF_draw_screen( const BOOL_32 pcomplete_redraw );

extern void TURN_OFF_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

