/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef	_INC_D_COPY_DIALOG_H
#define	_INC_D_COPY_DIALOG_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern BOOL_32	g_COPY_DIALOG_visible;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void COPY_DIALOG_peform_copy_and_redraw_screen( BOOL_32 *pediting_group, void *(*add_group_func_ptr)(void), const UNS_32 pgroup_index_to_copy_from, const UNS_32 pgroup_index_to_copy_to );

extern void COPY_DIALOG_draw_dialog( BOOL_32 *pediting_group, void *(*add_group_func_ptr)(void) );

extern void COPY_DIALOG_process_dialog( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

