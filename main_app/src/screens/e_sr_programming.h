/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_E_SR_PROGRAMMING_H
#define _INC_E_SR_PROGRAMMING_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define SR_PORT_A                               (0)
#define SR_PORT_B                               (1)
#define SR_PORT_MIN								(SR_PORT_A)
#define SR_PORT_MAX								(SR_PORT_B)

#define SR_MODE_P_2_M_MASTER                    (2)
#define SR_MODE_P_2_M_SLAVE                     (3)
#define SR_MODE_P_2_M_REPEATER                  (7)
#define SR_MODE_NEXT                            (1000)
#define SR_MODE_PREVIOUS                        (1001)
#define SR_MODE_MIN								(SR_MODE_P_2_M_MASTER)
#define SR_MODE_MAX								(SR_MODE_P_2_M_REPEATER)
#define SR_MODE_INVALID			                (99)

#define SR_GROUP_1                              (1)
#define SR_GROUP_2                              (2)
#define SR_GROUP_3                              (3)
#define SR_GROUP_4                              (4)
#define SR_GROUP_5                              (5)
#define SR_GROUP_6                              (6)
#define SR_GROUP_7                              (7)
#define SR_GROUP_8                              (8)
#define SR_GROUP_9                              (9)
#define SR_GROUP_10                             (10)
#define SR_GROUP_MIN							(SR_GROUP_1)
#define SR_GROUP_MAX							(SR_GROUP_10)

#define SR_REPEATER_A                           (0)
#define SR_REPEATER_B                           (1)
#define SR_REPEATER_C                           (2)
#define SR_REPEATER_D                           (3)
#define SR_REPEATER_E                           (4)
#define SR_REPEATER_MIN							(SR_REPEATER_A)
#define SR_REPEATER_MAX							(SR_REPEATER_E)

#define SR_SN_RCV_0                             (0)
#define SR_SN_RCV_1                             (1)
#define SR_SN_RCV_2                             (2)
#define SR_SN_RCV_3                             (3)
#define SR_SN_RCV_4                             (4)
#define SR_SN_RCV_5                             (5)
#define SR_SN_RCV_6                             (6)
#define SR_SN_RCV_7                             (7)
#define SR_SN_RCV_8                             (8)
#define SR_SN_RCV_9                             (9)
#define SR_SN_RCV_F                             (10)
#define SR_SN_RCV_MIN							(SR_SN_RCV_0)
#define SR_SN_RCV_MAX							(SR_SN_RCV_F)

#define SR_SN_XMT_0                             (0)
#define SR_SN_XMT_1                             (1)
#define SR_SN_XMT_2                             (2)
#define SR_SN_XMT_3                             (3)
#define SR_SN_XMT_4                             (4)
#define SR_SN_XMT_5                             (5)
#define SR_SN_XMT_6                             (6)
#define SR_SN_XMT_7                             (7)
#define SR_SN_XMT_8                             (8)
#define SR_SN_XMT_9                             (9)
#define SR_SN_XMT_F                             (10)
#define SR_SN_XMT_MIN							(SR_SN_XMT_0)
#define SR_SN_XMT_MAX							(SR_SN_XMT_F)

#define SR_XMT_POWER_0                          (0)
#define SR_XMT_POWER_1                          (1)
#define SR_XMT_POWER_2                          (2)
#define SR_XMT_POWER_3                          (3)
#define SR_XMT_POWER_4                          (4)
#define SR_XMT_POWER_5                          (5)
#define SR_XMT_POWER_6                          (6)
#define SR_XMT_POWER_7                          (7)
#define SR_XMT_POWER_8                          (8)
#define SR_XMT_POWER_9                          (9)
#define SR_XMT_POWER_10                         (10)
#define SR_XMT_POWER_MIN						(SR_XMT_POWER_0)
#define SR_XMT_POWER_MAX						(SR_XMT_POWER_10)


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FDTO_SR_PROGRAMMING_draw_screen( const BOOL_32 pcomplete_redraw, const UNS_32 port );

extern void SR_PROGRAMMING_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

