/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"e_time_and_date.h"

#include	"change.h"

#include	"combobox.h"

#include	"configuration_controller.h"

#include	"configuration_network.h"

#include	"cursor_utils.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"epson_rx_8025sa.h"

#include	"flowsense.h"

#include	"m_main.h"

#include	"speaker.h"

#include	"comm_mngr.h"

#include	"app_startup.h"


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CP_DT_MONTH				(0)
#define	CP_DT_DAY				(1)
#define	CP_DT_YEAR				(2)
#define	CP_DT_HOUR				(3)
#define	CP_DT_MIN				(4)
#define	CP_DT_SEC				(5)
#define CP_DT_SET_DT_BUTTON		(6)
#define	CP_DT_TIMEZONE			(7)

#define todAM					(0)
#define todPM					(1)

#define TIMEDATE_YEAR_MIN		(1993)
#define	TIMEDATE_YEAR_MAX		(2078)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/**
 * 2012.05.23 ajv : This global controls which date cursor position the user
 * left when navigating away from one of the date fields. This ensures they are
 * returned to that position when they go back to a time field. For example,
 * if the year was selected and the user presses DOWN followed by UP, they will
 * go back to the year position rather than the month as it did previously.
 */
static UNS_32 g_TIME_DATE_previous_cursor_pos;

/**
 * 2012.05.23 ajv : This global controls which date cursor position the user
 * left when navigating away from one of the time fields. This ensures they are
 * returned to that position when they go back to a time field. For example,
 * if the minutes was selected and the user presses DOWN followed by UP, they 
 * will go back to the year position rather than the hour as it did previously.
 */
static UNS_32 g_TIME_DATE_previous_time_cursor_pos;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Returns the passed in year incremented by 1. 
 * 
 * @executed Executed within the context of the key processing task when the 
 *  		 user adjusts the month, day or year.
 * 
 * @param pyear The year to increment by 1.
 * 
 * @return UNS_32 The passed in year incremented by 1. If the passed in year is 
 *  	          greater than or equal to the maximum, the minimum is returned.
 *
 * @author 7/22/2011 Adrianusv
 *
 * @revisions
 *    7/22/2011 Initial release
 *    2/7/2012  Modified to use defines for the min and max. Updated to use
 *              32-bit unsigned variables.
 */
static UNS_32 TIME_DATE_increment_year( const UNS_32 pyear ) 
{
	UNS_32 rv;

    if( pyear >= TIMEDATE_YEAR_MAX )
	{
        rv = TIMEDATE_YEAR_MIN;
	}
    else
	{
        rv = pyear + 1;
    }

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the passed in year decremented by 1. 
 * 
 * @executed Executed within the context of the key processing task when the 
 *  		 user adjusts the month, day or year.
 * 
 * @param pyear The year to decrement by 1.
 * 
 * @return UNS_32 The passed in year decremented by 1. If the passed in year is 
 *  	          less than or equal to the minimum, the maximum is returned.
 *
 * @author 7/22/2011 Adrianusv
 *
 * @revisions
 *    7/22/2011 Initial release
 *    2/7/2012  Modified to use defines for the min and max. Updated to use
 *              32-bit unsigned variables.
 */
static UNS_32 TIME_DATE_decrement_year( const UNS_32 pyear )
{
	UNS_32 rv;

	if( pyear <= TIMEDATE_YEAR_MIN )
	{
        rv = TIMEDATE_YEAR_MAX ;
    }
	else
	{
        rv = pyear - 1;
    }

	return( rv );
}

static void FDTO_TIME_DATE_show_time_zone_dropdown( void )
{
	FDTO_COMBOBOX_show( GuiStruct_cbxTimeZone_0, &FDTO_COMBOBOX_add_items, (tzUS_EASTERN_STANDARD_TIME + 1), GuiVar_DateTimeTimeZone );
}

/* ---------------------------------------------------------- */
/**
 * Returns the day of the week for the passed in date. The day of the week is 0 
 * based, with SUNDAY defined as 0. 
 * 
 * @executed Executed within the context of the key processing task when the 
 *  		 user adjusts the month, day or year.
 * 
 * @param pday The day.
 * @param pmonth The month.
 * @param pyear The year.
 * 
 * @return UNS_32 The day of the week for the corresponding date.
 *
 * @author 7/22/2011 Adrianusv
 *
 * @revisions
 *    7/22/2011 Initial release
 *    2/7/2012  Updated to use 32-bit unsigned variables.
 */
static UNS_32 TIME_DATE_get_day_of_week( const UNS_32 pday, const UNS_32 pmonth, const UNS_32 pyear )
{
	UNS_32	rv;

	UNS_32	ldate;

	ldate = DMYToDate( pday, pmonth, pyear );

	rv = DayOfWeek( ldate );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Processes the Plus and Minus keys to increment or decrement the month.
 * 
 * @executed Executed within the context of the key processing task when the 
 *  		 user changes the month.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @author 7/22/2011 Adrianusv
 *
 * @revisions
 *    7/22/2011 Initial release
 */
static void TIME_DATE_process_month( const UNS_32 pkeycode )
{
	good_key_beep();

	if( pkeycode == KEY_PLUS )
	{
		if( GuiVar_DateTimeMonth < 12 )
		{
			++GuiVar_DateTimeMonth;
		}
		else
		{
			GuiVar_DateTimeYear = TIME_DATE_increment_year( GuiVar_DateTimeYear );
			GuiVar_DateTimeMonth = 1;
		}
	}
   	else if( pkeycode == KEY_MINUS )
	{
		if( GuiVar_DateTimeMonth > 1 )
		{
			--GuiVar_DateTimeMonth;
		}
		else
		{
			GuiVar_DateTimeYear = TIME_DATE_decrement_year( GuiVar_DateTimeYear );
			GuiVar_DateTimeMonth = 12;
		}
	}

	// 6/25/2015 mpd : FogBugz #3032. Check day to be sure it exists in the new month. 
	if( GuiVar_DateTimeDay > NumberOfDaysInMonth( GuiVar_DateTimeMonth, GuiVar_DateTimeYear ) )
	{
		// 6/25/2015 mpd : This will set the day to "30" in most months; "28" when stepping past February. The day will
		// remain at this reduced value as the user steps through months since there is no way of knowing what day they
		// want once the desired month is displayed.
		GuiVar_DateTimeDay = NumberOfDaysInMonth( GuiVar_DateTimeMonth, GuiVar_DateTimeYear );
	}

	// Update the day of the week since it may have changed
	GuiVar_DateTimeDayOfWeek = TIME_DATE_get_day_of_week( GuiVar_DateTimeDay, GuiVar_DateTimeMonth, GuiVar_DateTimeYear );

	// Show the "Set date and time" button
	GuiVar_DateTimeShowButton = (true);
}

/* ---------------------------------------------------------- */
/**
 * Processes the Plus and Minus keys to increment or decrement the day.
 * 
 * @executed Executed within the context of the key processing task when the 
 *  		 user changes the day.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @author 7/22/2011 Adrianusv
 *
 * @revisions
 *    7/22/2011 Initial release
 */
static void TIME_DATE_process_day( const UNS_32 pkeycode )
{
	good_key_beep();

	if( pkeycode == KEY_PLUS )
	{
		if( GuiVar_DateTimeDay < NumberOfDaysInMonth(GuiVar_DateTimeMonth, GuiVar_DateTimeYear) )
		{
			++GuiVar_DateTimeDay;
		}
		else
		{
			if( GuiVar_DateTimeMonth < 12 )
			{
				++GuiVar_DateTimeMonth;
			}
			else
			{
				GuiVar_DateTimeYear = TIME_DATE_increment_year( GuiVar_DateTimeYear );
				GuiVar_DateTimeMonth = 1;
			}
			GuiVar_DateTimeDay = 1;
		}
	}
	else if( pkeycode == KEY_MINUS )
	{
		if( GuiVar_DateTimeDay > 1 )
		{
			--GuiVar_DateTimeDay;
		}
		else
		{
			if( GuiVar_DateTimeMonth > 1 )
			{
				--GuiVar_DateTimeMonth;
			}
			else
			{
				GuiVar_DateTimeYear = TIME_DATE_decrement_year( GuiVar_DateTimeYear );
				GuiVar_DateTimeMonth = 12;
			}
			GuiVar_DateTimeDay = NumberOfDaysInMonth( GuiVar_DateTimeMonth, GuiVar_DateTimeYear );
		}
	}

	// Update the day of the week since it may have changed
	GuiVar_DateTimeDayOfWeek = TIME_DATE_get_day_of_week( GuiVar_DateTimeDay, GuiVar_DateTimeMonth, GuiVar_DateTimeYear );

	// Show the "Set date and time" button
	GuiVar_DateTimeShowButton = (true);
}

/* ---------------------------------------------------------- */
/**
 * Processes the Plus and Minus keys to increment or decrement the year.
 * 
 * @executed Executed within the context of the key processing task when the 
 *  		 user changes the year.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @author 7/22/2011 Adrianusv
 *
 * @revisions
 *    7/22/2011 Initial release
 */
static void TIME_DATE_process_year( const UNS_32 pkeycode )
{
	good_key_beep();

	if( pkeycode == KEY_PLUS )
	{
		GuiVar_DateTimeYear = TIME_DATE_increment_year( GuiVar_DateTimeYear );
	}
	else if( pkeycode == KEY_MINUS )
	{
		GuiVar_DateTimeYear = TIME_DATE_decrement_year( GuiVar_DateTimeYear );
	}
	
	// Update the day of the week since it may have changed
	GuiVar_DateTimeDayOfWeek = TIME_DATE_get_day_of_week( GuiVar_DateTimeDay, GuiVar_DateTimeMonth, GuiVar_DateTimeYear );

	// Show the "Set date and time" button
	GuiVar_DateTimeShowButton = (true);
}

/* ---------------------------------------------------------- */
/**
 * Processes the Plus and Minus keys to increment or decrement the hour.
 * 
 * @executed Executed within the context of the key processing task when the 
 *  		 user changes the hour.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @author 7/22/2011 Adrianusv
 *
 * @revisions
 *    7/22/2011 Initial release
 */
static void TIME_DATE_process_hour( const UNS_32 pkeycode )
{
	UNS_32	lhour;

	// Determine the hours in 24 hour format
	if( GuiVar_DateTimeHour == 12 )
	{
		if( GuiVar_DateTimeAMPM == todAM )
		{
			lhour = 0;
		}
		else
		{
			lhour = 12;
		}
	}
	else if( GuiVar_DateTimeAMPM == todAM )
	{
		lhour = GuiVar_DateTimeHour;
	}
	else
	{
		lhour = GuiVar_DateTimeHour + 12;
	}

	// Save the 24 hour format value
	process_uns32( pkeycode, &lhour, 0, 23, 1, true );

	// Update the GuiVar to reflect the new time in 12 hour format
	GuiVar_DateTimeHour = lhour % 12;

	if( GuiVar_DateTimeHour == 0 )
	{
		GuiVar_DateTimeHour = 12;
	}

	if( lhour < 12 )
	{
		GuiVar_DateTimeAMPM = todAM;
	}
	else
	{
		GuiVar_DateTimeAMPM = todPM;
	}

	// 4/11/2012 rmd : I decided when you change either the hours or the minutes the seconds
	// snaps to 55.
	GuiVar_DateTimeSec = 55;

	// Show the "Set date and time" button
	GuiVar_DateTimeShowButton = (true);
}

/* ---------------------------------------------------------- */
/**
 * Processes the Plus and Minus keys to increment or decrement the minute. If 
 * the NDEBUG compiler directive is NOT set, the seconds are forced to 55 after 
 * the minute is changed. 
 * 
 * @executed Executed within the context of the key processing task when the 
 *  		 user changes the minute.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @author 7/22/2011 Adrianusv
 *
 * @revisions
 *    7/22/2011 Initial release
 */
static void TIME_DATE_process_minutes( const UNS_32 pkeycode )
{
	process_uns32( pkeycode, &GuiVar_DateTimeMin, 0, 59, 1, true );

	// 4/11/2012 rmd : I decided when you change either the hours or the minutes the seconds
	// snaps to 55.
	GuiVar_DateTimeSec = 55;

	// Show the "Set date and time" button
	GuiVar_DateTimeShowButton = (true);
}

/* ---------------------------------------------------------- */
/**
 * Processes the Plus and Minus keys to increment or decrement the seconds.
 * 
 * @executed Executed within the context of the key processing task when the 
 *  		 user changes the seconds.
 * 
 * @param pkeycode The key that was pressed.
 *
 * @author 7/22/2011 Adrianusv
 *
 * @revisions
 *    7/22/2011 Initial release
 */
static void TIME_DATE_process_seconds( const UNS_32 pkeycode )
{
	process_uns32( pkeycode, &GuiVar_DateTimeSec, 0, 59, 1, true );

	// Show the "Set date and time" button
	GuiVar_DateTimeShowButton = (true);
}

/* ---------------------------------------------------------- */
/**
 * Stores the date and time changes and generates a change line. This routine 
 * should be called whenever the time is changed, whether it's due to daylight 
 * saving time or due to a manual change by the user. 
 * 
 * @executed Executed within the context of the key processing task when the
 *  		 user leaves the screen or presses the button to set the date and
 *  		 time; within the context of the CommMngr task when a programming
 *  		 change is made from the central or handheld radio remote.
 * 
 * @param pchange_date_and_time_bool True if the user is changing the actual 
 *  								 date and time; otherwise, false. If this is
 *  								 false, only changes to the time zone
 *  								 settings are stored.
 * @param preason_for_change The reason for the change, such as keypad.
 * @param pbox_index_0 The serial number of the controller
 *                                    which initiated the change.
 *
 * @author 7/22/2011 Adrianusv
 *
 * @revisions
 *    7/22/2011 Initial release
 */
static void TIME_DATE_store_changes( const BOOL_32 pchange_date_and_time_bool,
									 const UNS_32 preason_for_change,
									 const UNS_32 pbox_index_0 )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	DATE_TIME_COMPLETE_STRUCT lcurrent_dt, lnew_dt;

	UNS_32	lnumber_of_changes;

	lnumber_of_changes = 0;

	if( pchange_date_and_time_bool == (true) )
	{
		EPSON_obtain_latest_complete_time_and_date( &lcurrent_dt );

		EPSON_obtain_latest_complete_time_and_date( &lnew_dt );

		lnew_dt.__dayofweek = (UNS_8)GuiVar_DateTimeDayOfWeek;
		lnew_dt.__month = (UNS_16)GuiVar_DateTimeMonth;
		lnew_dt.__day = (UNS_16)GuiVar_DateTimeDay;
		lnew_dt.__year = (UNS_16)GuiVar_DateTimeYear;

		if( GuiVar_DateTimeHour == 12 )
		{
			if( GuiVar_DateTimeAMPM == todAM )
			{
				lnew_dt.__hours = 0;
			}
			else
			{
				lnew_dt.__hours = 12;
			}
		}
		else if( GuiVar_DateTimeAMPM == todAM )
		{
			lnew_dt.__hours = (UNS_16)GuiVar_DateTimeHour;
		}
		else
		{
			lnew_dt.__hours = (UNS_16)(GuiVar_DateTimeHour + 12);
		}

		lnew_dt.__minutes = (UNS_16)GuiVar_DateTimeMin;
		lnew_dt.__seconds = (UNS_16)GuiVar_DateTimeSec;

		lnew_dt.date_time.T = HMSToTime( lnew_dt.__hours, lnew_dt.__minutes, lnew_dt.__seconds );
		lnew_dt.date_time.D = (UNS_16)DMYToDate( GuiVar_DateTimeDay, GuiVar_DateTimeMonth, GuiVar_DateTimeYear );

		// 12/03/2015 skc : If the datetime is set, then tell the rest of the chain.
		if( EPSON_set_date_time( lnew_dt.date_time, preason_for_change ) )
		{
			// If the datetime was actually set,
			// then prepare for sending tokens to IRRI's
			// We're in the keypad task, better do a Mutex.
			xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

			comm_mngr.token_date_time.dt = lnew_dt.date_time;

			comm_mngr.token_date_time.reason = preason_for_change;

			comm_mngr.flag_update_date_time = true;

			xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );
		}
	}
	else
	{
		lchange_bitfield_to_set = NETWORK_CONFIG_get_change_bits_ptr( CHANGE_REASON_KEYPAD );

		NETWORK_CONFIG_set_time_zone( GuiVar_DateTimeTimeZone, CHANGE_generate_change_line, preason_for_change, pbox_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	}
}

/* ---------------------------------------------------------- */
/**
 * Updates the date and time as long as the user hasn't made a change to it. As 
 * soon as the user makes a change to either the date or time, the time stops 
 * updating automatically. 
 * 
 * @executed Executed within the context of the TD check task.
 *
 * @author 10/12/2011 Adrianusv
 *
 * @revisions
 *    10/12/2011 Initial release
 */
extern void FDTO_TIME_DATE_update_screen( void )
{
	DATE_TIME_COMPLETE_STRUCT ldt;

	// Only update the clock if the user hasn't made a change.
	if( GuiVar_DateTimeShowButton == (false) )
	{
		EPSON_obtain_latest_complete_time_and_date( &ldt );

		GuiVar_DateTimeDayOfWeek = ldt.__dayofweek;
		GuiVar_DateTimeMonth = ldt.__month;
		GuiVar_DateTimeDay = ldt.__day;
		GuiVar_DateTimeYear = ldt.__year;
		GuiVar_DateTimeHour = ldt.__hours % 12;

		if( GuiVar_DateTimeHour == 0 )
		{
			GuiVar_DateTimeHour = 12;
		}

		GuiVar_DateTimeMin = ldt.__minutes;
		GuiVar_DateTimeSec = ldt.__seconds;

		if( ldt.__hours < 12 )
		{
			GuiVar_DateTimeAMPM = todAM;
		}
		else
		{
			GuiVar_DateTimeAMPM = todPM;
		}

		GuiLib_Refresh();
	}
}

/* ---------------------------------------------------------- */
/**
 * Draws the Date and Time screen.
 * 
 * @executed Executed within the context of the display processing task.
 * 
 * @param pcomplete_redraw True if the entire screen should be redrawn; 
 *  					        otherwise, false. 
 *
 * @author 7/22/2011 Adrianusv
 *
 * @revisions
 *    7/22/2011 Initial release
 */
static void TIME_DATE_copy_settings_into_guivars( void )
{
	DATE_TIME_COMPLETE_STRUCT ldt;

	g_TIME_DATE_previous_cursor_pos = CP_DT_MONTH;
	g_TIME_DATE_previous_time_cursor_pos = CP_DT_HOUR;

	EPSON_obtain_latest_complete_time_and_date( &ldt );

	GuiVar_DateTimeDayOfWeek = ldt.__dayofweek;
	GuiVar_DateTimeMonth = ldt.__month;
	GuiVar_DateTimeDay = ldt.__day;
	GuiVar_DateTimeYear = ldt.__year;
	GuiVar_DateTimeHour = ldt.__hours % 12;

	if( GuiVar_DateTimeHour == 0 )
	{
		GuiVar_DateTimeHour = 12;
	}

	GuiVar_DateTimeMin = ldt.__minutes;
	GuiVar_DateTimeSec = ldt.__seconds;

	if( ldt.__hours < 12 )
	{
		GuiVar_DateTimeAMPM = todAM;
	}
	else
	{
		GuiVar_DateTimeAMPM = todPM;
	}

	GuiVar_DateTimeTimeZone = NETWORK_CONFIG_get_time_zone();

	GuiVar_DateTimeShowButton = (false);
}

/* ---------------------------------------------------------- */
extern void FDTO_TIME_DATE_draw_screen( const BOOL_32 pcomplete_redraw )
{
	UNS_32	lcursor_to_select;

	if( pcomplete_redraw == (true) )
	{
		TIME_DATE_copy_settings_into_guivars();

		lcursor_to_select = CP_DT_TIMEZONE;
	}
	else
	{
		lcursor_to_select = (UNS_32)GuiLib_ActiveCursorFieldNo;
	}

	GuiLib_ShowScreen( GuiStruct_scrDateTime_0, (INT_16)lcursor_to_select, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Processes the key event from the Date and Time screen.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param pkey_event The key event to process.
 *
 * @author 7/22/2011 Adrianusv
 *
 * @revisions
 *    7/22/2011 Initial release
 *    5/23/2012 Added support for retaining which cursor position the user left
 *  			when moving from the date or time fields.
 */
extern void TIME_DATE_process_screen( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	BOOL_32	lshow_set_button;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_cbxTimeZone_0:
			COMBO_BOX_key_press( pkey_event.keycode, &GuiVar_DateTimeTimeZone );
			break;

		default:
			switch( pkey_event.keycode )
			{
				case KEY_SELECT:
					switch ( GuiLib_ActiveCursorFieldNo )
					{
						case CP_DT_SET_DT_BUTTON:
							good_key_beep();

							TIME_DATE_store_changes( (true), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index() );

							// Hide the "Set date and time" button
							GuiVar_DateTimeShowButton = (false);

							CURSOR_Select( g_TIME_DATE_previous_time_cursor_pos, (false) );

							Redraw_Screen( (false) );
							break;

						case CP_DT_TIMEZONE:
							good_key_beep();
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_TIME_DATE_show_time_zone_dropdown;
							Display_Post_Command( &lde );
							break;

						default:
							bad_key_beep();

					}
					break;

				case KEY_MINUS:
				case KEY_PLUS:
					lshow_set_button = GuiVar_DateTimeShowButton;

					switch ( GuiLib_ActiveCursorFieldNo )
					{
						case CP_DT_MONTH:
							TIME_DATE_process_month( pkey_event.keycode );
							break;

						case CP_DT_DAY:
							TIME_DATE_process_day( pkey_event.keycode );
							break;

						case CP_DT_YEAR:
							TIME_DATE_process_year( pkey_event.keycode );
							break;

						case CP_DT_HOUR:
							TIME_DATE_process_hour( pkey_event.keycode );
							break;

						case CP_DT_MIN:
							TIME_DATE_process_minutes( pkey_event.keycode );
							break;

						case CP_DT_SEC:
							TIME_DATE_process_seconds( pkey_event.keycode );
							break;

						case CP_DT_TIMEZONE:
							process_uns32( pkey_event.keycode, &GuiVar_DateTimeTimeZone, tzHAWAIIAN_STANDARD_TIME, tzUS_EASTERN_STANDARD_TIME, 1, (false) );
							break;

						default:
							bad_key_beep();

					}
					if( (lshow_set_button != GuiVar_DateTimeShowButton) || (GuiLib_ActiveCursorFieldNo == CP_DT_TIMEZONE) )
					{
						Redraw_Screen( (false) );
					}
					else
					{
						Refresh_Screen();
					}
					break;

				case KEY_C_UP:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_DT_MONTH:
						case CP_DT_DAY:
						case CP_DT_YEAR:
							bad_key_beep();
							break;

						case CP_DT_HOUR:
						case CP_DT_MIN:
						case CP_DT_SEC:
							g_TIME_DATE_previous_time_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

							CURSOR_Select( g_TIME_DATE_previous_cursor_pos, (true) ); 
							break;

						case CP_DT_SET_DT_BUTTON:
							CURSOR_Select( g_TIME_DATE_previous_time_cursor_pos, (true) ); 
							break;

						case CP_DT_TIMEZONE:
							if( FLOWSENSE_we_are_a_master_one_way_or_another() )
							{
								if( GuiVar_DateTimeShowButton == (true) )
								{
									CURSOR_Select( CP_DT_SET_DT_BUTTON, (true) ); 
								}
								else
								{
									CURSOR_Select( g_TIME_DATE_previous_time_cursor_pos, (true) ); 
								}
							}
							else
							{
								// 2/1/2016 ajv : Don't allow the user to edit date and time from the slave because we don't
								// have a method to sync this change up to the master.
								bad_key_beep();
							}
							break;

						default:
							CURSOR_Up( (true) );
							break;
					}
					break;

				case KEY_C_DOWN:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_DT_MONTH:
						case CP_DT_DAY:
						case CP_DT_YEAR:
							g_TIME_DATE_previous_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

							CURSOR_Select( g_TIME_DATE_previous_time_cursor_pos, (true) );
							break;

						case CP_DT_HOUR:
						case CP_DT_MIN:
						case CP_DT_SEC:
							g_TIME_DATE_previous_time_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;

							if( GuiVar_DateTimeShowButton == (true) )
							{
								CURSOR_Select( CP_DT_SET_DT_BUTTON, (true) );
							}
							else
							{
								CURSOR_Select( CP_DT_TIMEZONE, (true) );
							}
							break;

						default:
							CURSOR_Down( (true) );
					}
					break;

				case KEY_C_LEFT:
					if( FLOWSENSE_we_are_a_master_one_way_or_another() )
					{
						CURSOR_Up( (true) );
					}
					else
					{
						// 2/1/2016 ajv : Don't allow the user to edit date and time from the slave because we don't
						// have a method to sync this change up to the master.
						bad_key_beep();
					}
					break;

				case KEY_C_RIGHT:
					switch( GuiLib_ActiveCursorFieldNo )
					{
						case CP_DT_SEC:
							g_TIME_DATE_previous_time_cursor_pos = (UNS_32)GuiLib_ActiveCursorFieldNo;
							CURSOR_Down( (true) );
							break;

						default:
							CURSOR_Down( (true) );
					}
					break;

				default:
					if( pkey_event.keycode == KEY_BACK )
					{
						TIME_DATE_store_changes( (false), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index() );

						GuiVar_MenuScreenToShow = CP_MAIN_MENU_SETUP;
					}

					KEY_process_global_keys( pkey_event );
			}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

