#ifndef INC_CHAIN_SYNC_VARS_H
#define INC_CHAIN_SYNC_VARS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"lpc_types.h"

#include	"flash_storage.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/13/2018 rmd : Functionality - the checksums are calculated when the file is written,
// and are a checksum of the file contents (more about that later). The checksums are not
// calculated when a change is made to the file image in memory (meaning user leaves the
// screen). The idea being, when there are CLEAN tokens being sent back and forth between
// slaves and the master, we can accompany each token response with a file checksum data,
// one file crc per token resp. The master will look at it and decide if a file sync
// needs to take place.
//
// Any CHANGES pending in memory will start the dirty token_resp process again. And if a
// token_resp holds pdata changes no file sync checksum will be included in the token_resp.
// And no checksum test will be made by the master.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/29/2018 rmd : I'm thinking I want at least every controller in a possible 12 controller
// chain the opportunity to send additional pending file changes, and to give plenty of
// token time to distirbute any pending changes. In a broad sense we are not in a particular
// hurry to begin sending file crc's to the master. The intent of this feature is to catch
// sync errors, in time, and fix them. If a discrepancy exists for a few minutes that is no
// big deal. We are making a big assupmtion with this count of so called clean tokens, that
// a run of 16 clean tokens means all the pdata distributing and file saving that needs to
// happen has indeed happened. I'm not sure I'm in love with this tracking mechanism,
// because it doesn't feel very concrete, however i think it will work, and i haven't
// thought of a better way yet.
#define	CHAIN_SYNC_NUMBER_OF_CLEAN_TOKENS_REQUIRED_SINCE_CHANGE_OR_FILE_SAVE		(16)


typedef struct
{
	// 8/13/2018 rmd : As we build token responses, if allowed we try to include a file checksum
	// to be checked. This variable controls which file is next up. Initialize to zero on
	// startup.
	UNS_32				ff_next_file_crc_to_send_0;
	
	// ----------
	
	// 8/29/2018 rmd : A count of how many tokens have transpired since any variable change
	// process was started. This count is set to zero at boot/startup. It is also set to 0
	// whenever a tokan or token response carries any pdata change content.
	UNS_32				clean_tokens_since_change_detected_or_file_save[ FF_NUMBER_OF_FILES ];

	// ----------
	
	// 9/5/2018 rmd : On startup, and at the time pdata change activity is detected in the token
	// or token response, the checksum is marked as invalid and the clean token count driven to
	// zero. When the clean token count reaches the threshold again we try to include the
	// checksum in the token repsonse, and if the checksum isn't valid we attempt to calculate
	// it. The only thing stopping a successful calculation would be memory unavailable for the
	// calculation. This variable would then prevent us from sending that checksum, and allow us
	// to attempt the calculation again at a later token response.
	BOOL_32				crc_is_valid[ FF_NUMBER_OF_FILES ];
	
	// 8/30/2018 rmd : The checksum that we embed within the token reponse. And this is also the
	// checksum that the master compares the token response value against. REMEMBER, these are
	// not direct FILE crc's, that are picked out of the flash file system. These are custom
	// crc's calculated discretely using data picked out of the list item structure. This is
	// because the list items contain data not guaranteed to be the same across all the boxes in
	// a chain, such as the list pointers to the list header, the prev, and the next list items.
	// These pointers are to dynamic memory blocks that in no way are guaranteed to be the same
	// across all boxes in a chain.
	CRC_BASE_TYPE		the_crc[ FF_NUMBER_OF_FILES ];
	
}  CHAIN_SYNC_CONTROL_STRUCTURE;


extern CHAIN_SYNC_CONTROL_STRUCTURE	cscs;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 8/30/2018 rmd : The function that calcualte the CRC. Remember, these are not straight
	// file CRC's, because the file contains things, like list item memory pointers, that are
	// likely unique to the execution environment. At at each controller in a chain the
	// execution environment can be different, so the memory where the list resides can be
	// different.
	char		*file_name_string;

	BOOL_32		(* const __crc_calculation_function_ptr)( const UNS_32 );
	
	void		(* const __set_bits_on_all_groups_to_cause_distribution_in_the_next_token)( void );
	
	// 11/27/2018 dmd : Add a new entry to the structure to specify the clean house function
	// associated with that file entry. Used when we encounter a crc error for a file and the
	// master would send a clean house command only to the affected file.
	void		(* const __clean_house_processing) (void);

} CHAIN_SYNC_FILE_PERTINANTS;


extern CHAIN_SYNC_FILE_PERTINANTS const chain_sync_file_pertinants[ FF_NUMBER_OF_FILES ];


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


extern void init_chain_sync_vars( void );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

