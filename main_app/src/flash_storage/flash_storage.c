// file = flash_storage.c                    03.28.2011  rmd  */

// 8/20/2014 rmd : A NOTE ON OPTIMIZATION: I found that the flash_storage FOLDER in the
// Crossworks project has optimization set to level 1. As a common setting for all builds. I
// couldn't remember why this was done. But after some poking around I see testing I did
// during the spi writes in the spi_flash_driver source that refers to the speed
// improvements attained. I think it may have been a left over that this was actually saved
// this way. But it has likely been this way a long time. And I feel it best that the flash
// file reads and writes have their timing remain consistent from DEBUG to RELEASE. So I am
// leaving the crossworks property in place.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"flash_storage.h"

#include	"df_storage_mngr.h"

#include	"app_startup.h"

#include	"spi_flash_driver.h"

#include	"lpc32xx_gpio_driver.h"

#include	"wdt_and_powerfail.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"group_base_file.h"

#include	"configuration_controller.h"

#include	"configuration_network.h"

#include	"report_data.h"

#include	"system_report_data.h"

#include	"poc_report_data.h"

#include	"lights_report_data.h"

#include	"tpmicro_data.h"

#include	"weather_control.h"

#include	"weather_tables.h"

#include	"irrigation_system.h"

#include	"poc.h"

#include	"lights.h"

#include	"manual_programs.h"

#include	"station_groups.h"

#include	"flowsense.h"

#include	"contrast_and_speaker_vol.h"

#include	"cs_mem.h"

#include	"moisture_sensors.h"

#include	"budget_report_data.h"

#include	"code_distribution_task.h"

#include	"walk_thru.h"

#include	"chain_sync_funcs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	FLASH_STORAGE_seconds_to_delay_file_save_during_find_or_create	(10)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void FLASH_STORAGE_delete_all_files( UNS_32 flash_index )
{
	DF_DIR_ENTRY_s			*search_dir_entry, *found_dir_entry;
	
	unsigned long			dir_index_ul, rstatus_ul;
	
	search_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );
	memset( search_dir_entry, 0, sizeof(DF_DIR_ENTRY_s) );  // better do this so the filename string length is defined

	found_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );
	memset( found_dir_entry, 0, sizeof(DF_DIR_ENTRY_s) );  // better do this so the filename string length is defined

	// START of data flash operations...we don't want the data flash to change until we are done doing our operations
	DfTakeStorageMgrMutex( flash_index, portMAX_DELAY );

	dir_index_ul = 0;
	while ( TRUE )
	{
		// With a fields_to_check of 0 this will return every in-use dir entry
		rstatus_ul = DfFindDirEntry_NM( flash_index, search_dir_entry, found_dir_entry, 0, dir_index_ul );

		if( rstatus_ul != DF_DIR_ENTRY_NOT_FOUND )
		{
			//sprintf( str_64, "deleting %s", found_dir_entry->Filename_c ); 
			//Alert_Message( str_64 ); 

			DfDelFileFromDF_NM( flash_index, rstatus_ul, found_dir_entry );

			// if we just returned the last possible directory entry then we can't go any further
			if( rstatus_ul == ((ssp_define[ flash_index ].max_directory_entries) - 1) )
			{

				break;  // we're all done  ... last of all possible directory entries

			}
			else
			{

				dir_index_ul = rstatus_ul + 1;  // To resume search at the next entry

			}

		}
		else
		{

			break;  // we're all done  ... no more files found

		}

	}
	
	DfGiveStorageMgrMutex( flash_index );

	mem_free( search_dir_entry );
	mem_free( found_dir_entry );
}

/* ---------------------------------------------------------- */
static UNS_32 nm_find_latest_version_and_latest_edit_of_a_filename( UNS_32 flash_index, DF_DIR_ENTRY_s *pdir_entry )
{

// The _NM means NO MUTEX activity within this function there for the caller must be responsible for the use of the DATA_FLASH mutex.
//
// This function returns the dir index and fully loads the pfull_dir_entry structure. The pdir_entry union is valid ONLY if (rv_ul != DF_DIR_ENTRY_NOT_FOUND).
// AND it is important to note that pdir_entry is NOT touched if no matches are found.

	DF_DIR_ENTRY_s		*lsearch_dir_entry;
	
	DF_DIR_ENTRY_s		*lresults_dir_entry;
	
	UNS_32				files_found_ul, dir_index_ul;
	
	UNS_32				rstatus_ul, rv_ul;
	
	// ----------
	
	// THE CALLER IS RESPONSIBLE FOR TAKING AND RELEASING THE DATA FLASH MUTEX

	// ----------
	
	// NOTE: Upon entry to the function the pfull_dir_entry contains the filename. Nothing else.
	// We snatch that and then allow the DfFindDirEntry_NM to load lresults_full_dir_entry if it
	// finds a file
	lsearch_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );

	// Put the filename we are looking for into place.
	strlcpy( (char*)&(lsearch_dir_entry->Filename_c), (char*)&(pdir_entry->Filename_c), sizeof(lsearch_dir_entry->Filename_c) );

	// ----------
	
	// 1/20/2015 rmd : Secure the memory space where the returned dir entry would reside if a
	// file is found. We do not need to 0 out the lresults_full_dir_entry structure ... we use
	// the return value from DfFindDirEntry_NM to tell us if it is valid.
	lresults_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );

	// ----------
	
	// 1/20/2015 rmd : Setup so the comparison value so we guarantee the first file found is
	// picked up.
	pdir_entry->code_image_date_or_file_contents_revision = 0;

	rv_ul = DF_DIR_ENTRY_NOT_FOUND;  // to indicate no file found

	files_found_ul = 0;  // if we only find one latest version file we are done with the hunt ... also used as a debug aid

	dir_index_ul = 0;  // start with the first directory entry

	while( (true) )
	{
		rstatus_ul = DfFindDirEntry_NM( flash_index, lsearch_dir_entry, lresults_dir_entry, DF_COMPARISON_MASK_FILENAME, dir_index_ul );

		if( rstatus_ul != DF_DIR_ENTRY_NOT_FOUND )
		{
			files_found_ul += 1;  // inc how many files we have found

			// 1/20/2015 rmd : A matching directory entry was found. found_dir_entry is filled in with
			// the complete directory entry for the item. Capture the directory entry if newer revision
			// or code date detected. Use >= to include files with a revision value of 0.
			if( lresults_dir_entry->code_image_date_or_file_contents_revision >= pdir_entry->code_image_date_or_file_contents_revision )
			{
				*pdir_entry = *lresults_dir_entry;  // copy the latest possible full directory candidate

				rv_ul = rstatus_ul;  // and capture the directory index for this entry

				//snprintf( str_128, sizeof(str_128), "V%ld %s: found version %lx, edit %ld, %ld bytes", files_found_ul, lresults_full_dir_entry->dir.Filename_c, lresults_full_dir_entry->dir.version_ul, lresults_full_dir_entry->dir.edit_count_ul, lresults_full_dir_entry->dir.ItemSize_ul );
				//Alert_Message( str_128 );
			}

			// if we just returned the last possible directory entry then we can't go any further
			if( rstatus_ul == (ssp_define[ flash_index ].max_directory_entries - 1) )
			{
				break;  // we're all done  ... last of all possible directory entries
			}
			else
			{
				dir_index_ul = rstatus_ul + 1;  // To resume search at the next entry
			}

		}
		else
		{
			break;  // we're all done  ... no more files found
		}

	}

	// ----------
	
	// 1/20/2015 rmd : Now find the latest edit_count_ul file. This may not be necessary as
	// there can only be one file with the same filename. Well that is not exactly true.
	// Normally there is only ONE. But if something goes wrong or there is a power failure at
	// just the right moment there may be two.
	if( files_found_ul > 1 )
	{
		// 1/20/2015 rmd : Okay so we have found either the latest code date or greatest file
		// revision number - depending on the file type. So take a copy of the appropriate variable
		// and update the search directory entry to reflect.
		lsearch_dir_entry->code_image_date_or_file_contents_revision = pdir_entry->code_image_date_or_file_contents_revision;
		
		// and reset the edit_count_ul value in the results as it is not necessarily the largest edit value...that's what we're finding next
		pdir_entry->edit_count_ul = 0;

		rv_ul = DF_DIR_ENTRY_NOT_FOUND;  // reset the return value to again indicate no file found

		dir_index_ul = 0;  // start AGAIN at the first directory entry

		while( TRUE )
		{
			// 1/20/2015 rmd : Now using the FILENAME and the version or code date (depending on if this
			// is a code image file) as criteria find the file with the largest edit count.
			rstatus_ul = DfFindDirEntry_NM( flash_index, lsearch_dir_entry, lresults_dir_entry, DF_COMPARISON_MASK_FILENAME | DF_COMPARISON_MASK_DATE_OR_REVISION, dir_index_ul );

			if( rstatus_ul != DF_DIR_ENTRY_NOT_FOUND )
			{
				// 1/20/2015 rmd : A matching directory entry was found. found_dir_entry is filled in with
				// the complete directory entry for the item.  Capture the newest edit_count_ul found. Make
				// sure to use the >= to return at least the first file found.
				if( lresults_dir_entry->edit_count_ul >= pdir_entry->edit_count_ul )
				{
					*pdir_entry = *lresults_dir_entry;  // copy the latest possible full directory candidate

					rv_ul = rstatus_ul;  // and capture the directory index for this entry

					//snprintf( str_128, sizeof(str_128), "E %s: found version %lx, edit %ld, %ld bytes", lresults_full_dir_entry->dir.Filename_c, lresults_full_dir_entry->dir.version_ul, lresults_full_dir_entry->dir.edit_count_ul, lresults_full_dir_entry->dir.ItemSize_ul );
					//Alert_Message( str_128 );
				}

				// if we just returned the last possible directory entry then we can't go any further
				if( rstatus_ul == (ssp_define[ flash_index ].max_directory_entries-1) )
				{
					break;  // we're all done  ... last of all possible directory entries
				}
				else
				{
					dir_index_ul = rstatus_ul + 1;  // To resume search at the next entry
				}

			}
			else
			{
				break;  // we're all done  ... no more files found
			}

		}
	
	}

	mem_free( lsearch_dir_entry );  // don't forget to free this!

	mem_free( lresults_dir_entry );  // don't forget to free this!

	// sanity check
	if( (files_found_ul > 0) && (pdir_entry->edit_count_ul == 0) )
	{
		// Do not make such alert lines if the reason we got funny results is the impending power failure.
		if( restart_info.SHUTDOWN_impending != TRUE )
		{
			Alert_Message( "DF: condition should not exist." );
		}

	}

	return( rv_ul );
}

/* ---------------------------------------------------------- */
extern void FLASH_STORAGE_request_code_image_version_stamp( const char *pfile_name )
{
	// ----------

	// 2/20/2014 rmd : Only support for looking at the special code image files. In which the
	// version stamp is the TIME as the edit_count and the DATE as the version.
	//
	// For files other than code image files the request is dropped. To obtain the code image
	// version information requires a search through directory entries. The file contents is not
	// actually read out from the flash.
	//
	// The version information is returned via a queue message to the COMM_MNGR task. The queue
	// message will contain the version time and date. No memory responsibilities are
	// transferred to the calling task (to the comm_mngr).
	
	// ----------

	FLASH_STORAGE_QUEUE_STRUCT	lfsqs;

	// ----------

	if( (pfile_name == CS3000_APP_FILENAME) || (pfile_name == TPMICRO_APP_FILENAME) )
	{
		// 2/20/2014 rmd : Clean queue message.
		memset( &lfsqs, 0x00, sizeof( FLASH_STORAGE_QUEUE_STRUCT ) );
		
		lfsqs.command = FLASH_TASK_EVENT_return_code_image_version_stamp;

		lfsqs.const_file_name_ptr = pfile_name;

		// ----------

		if( xQueueSendToBack( *(ssp_define[ FLASH_INDEX_0_BOOTCODE_AND_EXE ].task_queue_ptr), &lfsqs, (portTickType)0 ) != pdPASS )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "FLASH STORAGE: queue full" );
		}
	}
	else
	{
		Alert_Message( "FILE VERSION: not a code file!" );
	}

	// ----------
}

/* ---------------------------------------------------------- */
extern void FLASH_STORAGE_request_code_image_file_read( const char *pfile_name, const UNS_32 pcompletion_event )
{
	// ----------

	// 2/20/2014 rmd : Only supports reading of the code image files. For any other file the
	// request is dropped. The memory for the file is acquired when the read actually takes
	// place - based upon the file size.
	
	// 2/20/2014 rmd : AFTER the read completes a queue message is sent to the CODE_DISTRIBUTION
	// task informing the particular file read has happened and providing the memory handle.
	// This queue message also transfers memory release responsibility to the code_distribution
	// task. Because the response is a queue posting to the code_distribution task it generally
	// only makes sense to call this function from within the context of that task.

	// ----------

	FLASH_STORAGE_QUEUE_STRUCT	lfsqs;

	// ----------

	if( (pfile_name == CS3000_APP_FILENAME) || (pfile_name == TPMICRO_APP_FILENAME) )
	{
		if( (pcompletion_event == FLASH_TASK_EVENT_read_code_image_file_to_memory_for_tpmicro_isp_mode) || (pcompletion_event == FLASH_TASK_EVENT_read_code_image_file_to_memory_for_code_transmit) )
		{
			// 2/20/2014 rmd : Clean queue message.
			memset( &lfsqs, 0x00, sizeof( FLASH_STORAGE_QUEUE_STRUCT ) );
			
			lfsqs.command = pcompletion_event;

			lfsqs.const_file_name_ptr = pfile_name;
	
			// ----------
	
			if( xQueueSendToBack( *(ssp_define[ FLASH_INDEX_0_BOOTCODE_AND_EXE ].task_queue_ptr), &lfsqs, (portTickType)0 ) != pdPASS )
			{
				ALERT_MESSAGE_WITH_FILE_NAME( "FLASH STORAGE: queue full" );
			}
		}
		else
		{
			Alert_Message( "FLASH: unexpd parameter" );
		}
	}
	else
	{
		Alert_Message( "FILE READ: not a code file!" );
	}
}

/* ---------------------------------------------------------- */
static void return_code_image_version_stamp( UNS_32 pflash_index, FLASH_STORAGE_QUEUE_STRUCT *pfsqs )
{
	// 3/3/2014 rmd : This function use is intentionally restricted to only the TWO code image
	// files stored in flash 0. Only they use the edit count and version fields in the directory
	// entry to represent the code image TIME and DATE.

	// 3/3/2014 rmd : Upon completion of locating the directory entry the memory acquisition and
	// file read, a queue message is sent tot he comm_mngr task informing of the completion. And
	// transfering the memory handle and release responsibility to the comm_mngr task.
	
	// 3/3/2014 rmd : If the code_date and code_time returned in the queue message to the
	// tpmicro are both 0 then the file could not be found.
	
	// ----------

	// 2/20/2014 rmd : Use a pointer and get local memory. These directory entries are fairly
	// large. Like 160 bytes in size.
	DF_DIR_ENTRY_s	*ldir_entry;
	
	UNS_32			rstatus_ul;
	
	COMM_MNGR_TASK_QUEUE_STRUCT	cmeqs;
	
	// ----------

	// 2/24/2014 rmd : Clean the queue message. When both code_date and code_time are ZERO that
	// is the indication the file was not found.
	memset( &cmeqs, 0x00, sizeof(COMM_MNGR_TASK_QUEUE_STRUCT) );

	// 2/24/2014 rmd : Set the proper event based upon filename.
	if( pfsqs->const_file_name_ptr == CS3000_APP_FILENAME )
	{
		cmeqs.event = COMM_MNGR_EVENT_main_code_image_version_stamp;
	}
	else
	{
		cmeqs.event = COMM_MNGR_EVENT_tpmicro_code_image_version_stamp;
	}

	// ----------

	// 2/20/2014 rmd : Allocate and clean memory for the dir entry.
	ldir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );

	memset( ldir_entry, 0x00, sizeof(DF_DIR_ENTRY_s) );
	
	// 2/20/2014 rmd : ldir_entry will not be a NULL. If so means system out of memory and code
	// will STOP deep within the mem_malloc chain of calls.

	// ----------

	strlcpy( ldir_entry->Filename_c, pfsqs->const_file_name_ptr, sizeof(ldir_entry->Filename_c) );

	// ----------

	// START of data flash operations...we don't want the data flash to change until we are done doing our operations
	DfTakeStorageMgrMutex( FLASH_INDEX_0_BOOTCODE_AND_EXE, portMAX_DELAY );

	// ----------

	// NO NEED to initialize anything else in the dir_entry for the following - only the filename
	rstatus_ul = nm_find_latest_version_and_latest_edit_of_a_filename( FLASH_INDEX_0_BOOTCODE_AND_EXE, ldir_entry );
	
	if( rstatus_ul != DF_DIR_ENTRY_NOT_FOUND )
	{
		// 3/4/2014 rmd : Here's what we're after. Capture them now.
		cmeqs.code_date = ldir_entry->code_image_date_or_file_contents_revision;
		
		cmeqs.code_time = ldir_entry->edit_count_ul;
	}
	else
	{
		if( !restart_info.SHUTDOWN_impending )
		{
			Alert_flash_file_not_found( ldir_entry->Filename_c );
		}

		// 3/3/2014 rmd : Remember code_date and code_time are still 0. Indicating file not
		// found.
	}
	
	// ----------
	
	// END of data flash operations...return the mutex
	DfGiveStorageMgrMutex( FLASH_INDEX_0_BOOTCODE_AND_EXE );
	
	// ----------

	// 2/20/2014 rmd : If we aborted due to a power fail the file read isn't likely to have
	// completed. I think it doesn't matter as all tasks are shuttered. All tasks except this
	// flash storage task that is. But the comm_mngr task will no longer be active. So what we
	// post we never be acted on. Technically we should free the memory and NULL the memory
	// pointer - but like I said it doesn't matter cause we're headed for a shutdown anyway.
	if( restart_info.SHUTDOWN_impending )
	{
		// 2/24/2014 rmd : No special steps needed regarding memory - just make an alert line for
		// curiousity purposes.
		Alert_Message_va( "File Ver Read: abort due to power fail (%s).", ldir_entry->Filename_c );
	}

	// ----------

	// 2/24/2014 rmd : Post the queue message for the comm_mngr task.
	COMM_MNGR_post_event_with_details( &cmeqs );

	// ----------
	
	mem_free( ldir_entry );  // release the memory we allocated in the beginning of the function
}

/* ---------------------------------------------------------- */
static void read_code_image_file_to_memory( UNS_32 pflash_index, FLASH_STORAGE_QUEUE_STRUCT *pfsqs )
{
	// 2/20/2014 rmd : This function use is intentionally restricted to only the TWO code image
	// files stored in flash 0. I've done this so one thinks long and hard about why the
	// application needs to re-read an application level file from the file system. Presently
	// that need has not come up. So by not making it easy to do the coders will have to stop
	// and ask why?
	//
	// 2/20/2014 rmd : Upon completion of the memory acquisition and file read, a queue message
	// is sent to the comm_mngr task informing of the completion. And transfering the memory
	// handle and release responsibility to the comm_mngr task.
	
	// ----------

	// 2/20/2014 rmd : Use a pointer and get local memory. These directory entries are fairly
	// large. Like 160 bytes in size.
	DF_DIR_ENTRY_s					*ldir_entry;
	
	UNS_32							rstatus_ul;
	
	CODE_DISTRIBUTION_TASK_QUEUE_STRUCT		cdtqs;
	
	void							*image_ptr;
	
	BOOL_32							lerror;
	
	// ----------

	lerror = (false);
	
	// 2/24/2014 rmd : Clean the queue message. The NULL pointer is the message to the COMM_MNGR
	// task that the file read didn't take place. We need to send the queue message one way or
	// another to keep the tpmicro messaging within the comm_mngr task moving.
	memset( &cdtqs, 0x00, sizeof(CODE_DISTRIBUTION_TASK_QUEUE_STRUCT) );

	if( pfsqs->command == FLASH_TASK_EVENT_read_code_image_file_to_memory_for_tpmicro_isp_mode )
	{
		cdtqs.event = CODE_DISTRIBUTION_EVENT_tpmicro_binary_file_read_for_isp_complete;
	}
	else
	if( pfsqs->command == FLASH_TASK_EVENT_read_code_image_file_to_memory_for_code_transmit )
	{
		// 2/24/2014 rmd : Set the proper event based upon filename.
		if( pfsqs->const_file_name_ptr == CS3000_APP_FILENAME )
		{
			cdtqs.event = CODE_DISTRIBUTION_EVENT_main_binary_file_read_complete;
		}
		else
		{
			cdtqs.event = CODE_DISTRIBUTION_EVENT_tpmicro_binary_file_read_complete;
		}
	}
	else
	{
		lerror = (true);
		
		Alert_Message( "FLASH STORAGE - improper parameter" );
	}

	// ----------

	if( !lerror )
	{
		// 2/20/2014 rmd : Allocate and clean memory for the dir entry.
		ldir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );
	
		memset( ldir_entry, 0x00, sizeof(DF_DIR_ENTRY_s) );
		
		// 2/20/2014 rmd : ldir_entry will not be a NULL. If so means system out of memory and code
		// will STOP deep within the mem_malloc chain of calls.
	
		// ----------
	
		strlcpy( ldir_entry->Filename_c, pfsqs->const_file_name_ptr, sizeof(ldir_entry->Filename_c) );
	
		// ----------
	
		// START of data flash operations...we don't want the data flash to change until we are done doing our operations
		DfTakeStorageMgrMutex( FLASH_INDEX_0_BOOTCODE_AND_EXE, portMAX_DELAY );
	
		// ----------
	
		// NO NEED to initialize anything else in the lfound_full_dir_entry for the following - only the filename
		rstatus_ul = nm_find_latest_version_and_latest_edit_of_a_filename( FLASH_INDEX_0_BOOTCODE_AND_EXE, ldir_entry );
		
		if( rstatus_ul != DF_DIR_ENTRY_NOT_FOUND )
		{
			// 2/20/2014 rmd : Need memory for the file when read. Obtain block based upon indicated
			// file size. 
			//if( mem_obtain_a_block_if_available( ldir_entry->ItemSize_ul, (void**)&cmeqs.dh.dptr) )
			if( mem_obtain_a_block_if_available( ldir_entry->ItemSize_ul, &image_ptr ) )
			{
				if( DfReadFileFromDF_NM( FLASH_INDEX_0_BOOTCODE_AND_EXE, ldir_entry, image_ptr ) != DF_RESULT_SUCCESSFUL )
				{
					// 5/26/2017 rmd : As a sign of trouble NULL the pointer. And free the memory to clean up,
					// after all we did get it.
					mem_free( image_ptr );
	
					image_ptr = NULL;
				}
			}
			else
			{
				Alert_Message( "File read: code image memory block not available" );
			}
		}
		else
		{
			if( !restart_info.SHUTDOWN_impending )
			{
				Alert_flash_file_not_found( ldir_entry->Filename_c );
			}
		}
		
		// ----------
		
		// END of data flash operations...return the mutex
		DfGiveStorageMgrMutex( FLASH_INDEX_0_BOOTCODE_AND_EXE );
		
		// ----------
	
		// 2/20/2014 rmd : If we aborted due to a power fail the file read isn't likely to have
		// completed. I think it doesn't matter as all tasks are shuttered. All tasks except this
		// flash storage task that is. But the comm_mngr task will no longer be active. So what we
		// post we never be acted on. Technically we should free the memory and NULL the memory
		// pointer - but like I said it doesn't matter cause we're headed for a shutdown anyway.
		if( restart_info.SHUTDOWN_impending )
		{
			// 2/24/2014 rmd : No special steps needed regarding memory - just make an alert line for
			// curiousity purposes.
			Alert_Message_va( "File Read: abort due to power fail (%s).", ldir_entry->Filename_c );
		}
	
		// ----------
	
		// 5/26/2017 rmd : At first I thought we should only signal the task if the file read was
		// successful, meaning got the memory and completed the read. BUT, it turns out the
		// comm_mngr OR code_distribution tasks are 'waiting' for the event to occur, one way or
		// another, and use a NULL value in the data handle ptr as a signal of trouble. So always
		// return an event.
		cdtqs.dh.dlen = ldir_entry->ItemSize_ul;
		
		cdtqs.dh.dptr = image_ptr;
		
		// 2/24/2014 rmd : Post the queue message to the code_distribution task.
		CODE_DISTRIBUTION_post_event_with_details( &cdtqs );

		// ----------
		
		mem_free( ldir_entry );  // release the memory we allocated in the beginning of the function
	}
}

/* ---------------------------------------------------------- */
extern void FLASH_FILE_find_or_create_variable_file(	UNS_32				pflash_index,
														const char			*pfile_name,
														UNS_32				platest_revision,
														void				*pvariable_ptr,
														UNS_32				pfile_size,
														xSemaphoreHandle	pdata_recursive_mutex,
														void				(*pupdater_func_ptr)(UNS_32),
														void				(*pinitialization_func_ptr)(void),
														UNS_32				pff_name )
{

	DF_DIR_ENTRY_s		*lfound_dir_entry;
	
	UNS_32				rstatus_ul;
	
	BOOL_32				initialize_and_save, older_version_found;
	
	// ----------
	
	// Not all data stored to a file uses a mutex.
	if( pdata_recursive_mutex != NULL )
	{
		xSemaphoreTakeRecursive( pdata_recursive_mutex, portMAX_DELAY );
	}

	// ----------
	
	lfound_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );  // MEMORY ALLOCATION
	
	// 1/21/2015 rmd : Clean it for good measure.
	memset( lfound_dir_entry, 0x00, sizeof(DF_DIR_ENTRY_s) );

	strlcpy( lfound_dir_entry->Filename_c, pfile_name, sizeof(lfound_dir_entry->Filename_c) );
	
	// ----------
	
	initialize_and_save = (false);

	older_version_found = (false);
	
	// ----------
	
	// START of data flash operations...we don't want the data flash to change until we are done doing our operations
	DfTakeStorageMgrMutex( pflash_index, portMAX_DELAY );
	
	// 1/20/2015 rmd : NO NEED to initialize anything else in the lfound_full_dir_entry for the
	// following - only the filename. We find all files with the same file NAME and return the
	// directory entry for the file with the numerically largest version.
	rstatus_ul = nm_find_latest_version_and_latest_edit_of_a_filename( pflash_index, lfound_dir_entry );
	
	if( rstatus_ul != DF_DIR_ENTRY_NOT_FOUND )
	{
		// 1/21/2015 rmd : The found dir entry is loaded up with the most recent file as defined by
		// the revision and edit count values.

		// ----------
		
		// 1/21/2015 rmd : If the version is older set us up to call the updater function.
		if( lfound_dir_entry->code_image_date_or_file_contents_revision < platest_revision )
		{
			Alert_flash_file_found_old_version( (char*)pfile_name );

			older_version_found = (true);
		}
		
		// ----------
		
		// 1/21/2015 rmd : Check the file size is greater than 0. A pseudo sanity check. And check
		// the file size is not greater than the size the caller has specified.
		if( (lfound_dir_entry->ItemSize_ul > 0) && (lfound_dir_entry->ItemSize_ul <= pfile_size) )
		{
			#if ALERT_SHOW_ROUTINE_FILE_FINDS_DURING_STARTUP
				Alert_flash_file_found( lfound_dir_entry->Filename_c, lfound_dir_entry->code_image_date_or_file_contents_revision, lfound_dir_entry->edit_count_ul, lfound_dir_entry->ItemSize_ul );
			#endif

			// ----------
			
			DfReadFileFromDF_NM( pflash_index, lfound_dir_entry, pvariable_ptr );
		}
		else
		{
			Alert_flash_file_size_error( (char*)pfile_name );
			
			initialize_and_save = (true);
		}
	}
	else
	{
		if( restart_info.SHUTDOWN_impending )
		{
			Alert_Message_va( "File Read: abort due to power fail (%s).", pfile_name );
		}
		else
		{
			Alert_flash_file_not_found( (char*)pfile_name );
			
			initialize_and_save = (true);
		}
	}
	
	// ----------
	
	// END of data flash operations...return the mutex
	DfGiveStorageMgrMutex( pflash_index );
	
	// ----------
	
	// 10/17/2012 rmd : If the power fail isr has fired no reason to attempt this operation.
	if( !restart_info.SHUTDOWN_impending )
	{
		// 1/21/2015 rmd : The intialize takes precedence over updating.
		if( initialize_and_save )
		{
			if( pinitialization_func_ptr != NULL )  // protect against this fatal error
			{
				(*pinitialization_func_ptr)();
			}
			else
			{
				Alert_Message( "Flash File: trying to initialize but no function" );
			}
		}
		else
		if( older_version_found )
		{
			if( pupdater_func_ptr != NULL )  // protect against this fatal error
			{
				pupdater_func_ptr( lfound_dir_entry->code_image_date_or_file_contents_revision );
			}
			else
			{
				Alert_Message( "Flash File: trying to update but no function" );
			}
		}

		if( initialize_and_save || older_version_found )
		{
			// 1/27/2015 rmd : This FIND_or_CREATE function is only called during the startup task. At
			// which point the FLASH_FILE tasks have not yet been created. In the STARTUP task we call
			// this function back to back for multiple files. Checking on their existence and health
			// during the boot process. This function itself puts heavy demands on our memory partition
			// system. Because it requires a temporary memory block to read the file out into. To write
			// the file (a queued write) also requires that very same biggest block. So if one file is
			// being written (queued) the other cannot run this init function. So that is a problem
			// during startup. We run out of memory. One solution is to postpone the actual write queing
			// activity for 10 seconds. That allows us time to pass through there init calls. And the
			// saves have the built-in mechanism to again postpone if the memory is still not available.
			// Eventually it will become available.
			// 
			// 2/10/2015 ajv : There's a slim possibility that the length of this delay can actually
			// result in an error, though the controller will correct itself. Specifically, if a number
			// of files are pending at the master when a FLOWSENSE scan starts, a code distribution
			// error may occur at the completion of the scan if there's no memory available for the code
			// image. In this case, the master restarts and, in theory, the pending files should
			// complete before the scan completes.
			FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( pff_name, FLASH_STORAGE_seconds_to_delay_file_save_during_find_or_create );
		}
	}
	
	// ----------
	
	mem_free( lfound_dir_entry );  // release the memory we allocated in the beginning of the function
	
	// ----------
	
	// Not all data stored to a file uses a mutex.
	if( pdata_recursive_mutex != NULL )
	{
		xSemaphoreGiveRecursive( pdata_recursive_mutex );
	}

}

/* ---------------------------------------------------------- */
extern void FLASH_FILE_initialize_list_and_find_or_create_group_list_file(	const UNS_32			pflash_index,
					   														const char				*pfile_name,
					   														UNS_32					platest_revision,
					   														MIST_LIST_HDR_TYPE		*plist_hdr_ptr,
					   														UNS_32 const			*pitem_size_array,
					   														xSemaphoreHandle		plist_recursive_mutex,
					   														void					(*pupdater_func_ptr)(UNS_32),
					   														void					(*pinitialization_func_ptr)(void *const, const BOOL_32),
					   														char					*pdefault_label,
					   														UNS_32					pff_name )
{
	// 8/30/2018 rmd : This function is executed in the context of the app_startup task. As a
	// matter of fact the only time we read out a file during the application lifetime is during
	// the boot/startup process.
	//
	// 8/30/2018 rmd : As list items are added a new dynamic memory allocation from the memory
	// pool is acquired.
	//
	// 8/30/2018 rmd : After the file is read out, or created, we calculate the chain_sync CRC
	// for the list items. Using the file CRC was not practical because not everything in the
	// file can assumed to be the same from box to box. Take the group list structure, it
	// contains our list support structure. In it is a pointer to the list header, prev, and
	// next list items. At some point this is saved into the file. Are the memory allocations
	// guaranteed to be the same across the boxes in a chain? If you ask me, absolutely not.
	//
	// 1/21/2015 rmd : The caller includes a pointer to the p_item_size_array. Which is an array
	// of list item size using the strucutre revision as the array index.

	// ----------
	
	DF_DIR_ENTRY_s		*lfound_dir_entry;
	
	UNS_32				rstatus_ul;
	
	UNS_8				*b_ucp;

	UNS_8				*l_ucp;
	
	UNS_8				*start_ucp;
	
	BOOL_32				initialize_and_save, older_version_found;
	
	UNS_32				latest_list_item_size, file_list_item_size;
	
	// ----------
	
	if( plist_recursive_mutex != NULL )
	{
		// Take the list MUTEX early on and keep it. 
		xSemaphoreTakeRecursive( plist_recursive_mutex, portMAX_DELAY );
	}

	// ----------
	
	// Okay on program startup initialize the list  ... BY OUR DEFINITION the list_support is
	// ALWAYS at offsetof of 0 into the structure.
	nm_ListInit( plist_hdr_ptr, 0 );  // so now we have an empty list...go find the most recent file for this type
	
	// ----------
	
	lfound_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );  // MEMORY ALLOCATION

	// 1/21/2015 rmd : Clean it for good measure.
	memset( lfound_dir_entry, 0x00, sizeof(DF_DIR_ENTRY_s) );

	strlcpy( lfound_dir_entry->Filename_c, pfile_name, sizeof(lfound_dir_entry->Filename_c) );

	// ----------
	
	initialize_and_save = (false);

	older_version_found = (false);

	// 1/26/2015 rmd : We know this without having to read a file. And need to set this value
	// for the case when there is no file to read.
	latest_list_item_size = pitem_size_array[ platest_revision ];

	// ----------
	
	// START of data flash operations...we don't want the data flash to change until we are done doing our operations
	DfTakeStorageMgrMutex( pflash_index, portMAX_DELAY );

	// NO NEED to initialize anything else in the lfound_full_dir_entry for the following - only the filename
	rstatus_ul = nm_find_latest_version_and_latest_edit_of_a_filename( pflash_index, lfound_dir_entry );

	if( rstatus_ul != DF_DIR_ENTRY_NOT_FOUND )
	{
		// 1/21/2015 rmd : The found dir entry is loaded up with the most recent file as defined by
		// the revision and edit count values.

		// ----------
		
		// 1/21/2015 rmd : If the version is older set us up to call the updater function.
		if( lfound_dir_entry->code_image_date_or_file_contents_revision < platest_revision )
		{
			Alert_flash_file_found_old_version( (char*)pfile_name );

			older_version_found = (true);
		}
		
		// ----------
		
		// 1/21/2015 rmd : Using the array of list item sizes, indexed by the revision value, we
		// check if the file size makes sense. If not we will initialize the list with a single
		// default item.
		file_list_item_size = pitem_size_array[ lfound_dir_entry->code_image_date_or_file_contents_revision ];
		
		// ----------
		
		// 1/23/2015 rmd : Note the robustness of the following if. Trying to make sure things make
		// sense before proceeding with this file.
		if( (lfound_dir_entry->ItemSize_ul % file_list_item_size == 0) && (latest_list_item_size >= file_list_item_size) && ((lfound_dir_entry->ItemSize_ul / file_list_item_size) <= MAX_STATIONS_PER_NETWORK) && (lfound_dir_entry->ItemSize_ul > 0) )
		{
			#if ALERT_SHOW_ROUTINE_FILE_FINDS_DURING_STARTUP
				Alert_flash_file_found( lfound_dir_entry->Filename_c, lfound_dir_entry->code_image_date_or_file_contents_revision, lfound_dir_entry->edit_count_ul, lfound_dir_entry->ItemSize_ul );
			#endif
	
			// ----------
			
			// crc ???
			
			// ----------
			
			// 2012.02.22 rmd : Get a chunk of memory to read the WHOLE file into.
			start_ucp = mem_malloc( lfound_dir_entry->ItemSize_ul );  // This memory is only used locally and will be freed within this function.

			l_ucp = start_ucp;

			DfReadFileFromDF_NM( pflash_index, lfound_dir_entry, start_ucp );

			do
			{
				// This is the memory for the actual list items...this memory will be freed only if we
				// delete one of these group items ... otherwise once allocated it will remain so as long as
				// we are powered up.
				b_ucp = mem_malloc( latest_list_item_size );

				memcpy( b_ucp, l_ucp, file_list_item_size );
				
				nm_ListInsertTail( plist_hdr_ptr, b_ucp );

				l_ucp += file_list_item_size;  // move to the next item

				lfound_dir_entry->ItemSize_ul -= file_list_item_size;  // reduce the remaining length

			} while( lfound_dir_entry->ItemSize_ul > 0 );

			// 2012.02.22 rmd : We're done with it. We have taken a copy of each list item, one-by-one,
			// into newly allocated memory for the list items, and added them to the list.
			mem_free( start_ucp );
		}
		else
		{
			Alert_flash_file_size_error( (char*)pfile_name );
			
			initialize_and_save = (true);
		}
	}
	else
	{
		if( restart_info.SHUTDOWN_impending == (true) )
		{
			Alert_Message_va( "File Read: abort due to power fail (%s).", pfile_name );
		}
		else
		{
			Alert_flash_file_not_found( (char*)pfile_name );
			
			initialize_and_save = (true);
		}
	}
	
	// ----------
	
	// END of data flash operations...return the flash file system mutex.
	DfGiveStorageMgrMutex( pflash_index );
	
	// ----------
	
	// 10/17/2012 rmd : If the power fail isr has fired no reason to attempt this operation.
	if( !restart_info.SHUTDOWN_impending )
	{
		// 1/21/2015 rmd : The intialize takes precedence over updating.
		if( initialize_and_save )
		{
			// 1/21/2015 rmd : There are checks within the call chain for NULL parameters. However test
			// anyway so we can see a coding error. Should always reference the init function even if it
			// does nothing.
			if( pinitialization_func_ptr != NULL )
			{
				nm_GROUP_create_new_group( plist_hdr_ptr, pdefault_label, pinitialization_func_ptr, latest_list_item_size, (false), NULL );
			}
			else
			{
				Alert_Message( "Flash List: trying to initialize but no function" );
			}
		}
		else
		if( older_version_found )
		{
			if( pupdater_func_ptr != NULL )  // protect against this fatal error
			{
				pupdater_func_ptr( lfound_dir_entry->code_image_date_or_file_contents_revision );
			}
			else
			{
				Alert_Message( "Flash File: trying to update but no function" );
			}
		}

		if( initialize_and_save || older_version_found )
		{
			// 1/27/2015 rmd : This FIND_or_CREATE function is only called during the startup task. At
			// which point the FLASH_FILE tasks have not yet been created. In the STARTUP task we call
			// this function back to back for multiple files. Checking on their existence and health
			// during the boot process. This function itself puts heavy demands on our memory partition
			// system. Because it requires a temporary memory block to read the file out into. To write
			// the file (a queued write) also requires that very same biggest block. So if one file is
			// being written (queued) the other cannot run this init function. So that is a problem
			// during startup. We run out of memory. One solution is to postpone the actual write queing
			// activity for 10 seconds. That allows us time to pass through there init calls. And the
			// saves have the built-in mechanism to again postpone if the memory is still not available.
			// Eventually it will become available.
			// 
			// 2/10/2015 ajv : There's a slim possibility that the length of this delay can actually
			// result in an error, though the controller will correct itself. Specifically, if a number
			// of files are pending at the master when a FLOWSENSE scan starts, a code distribution
			// error may occur at the completion of the scan if there's no memory available for the code
			// image. In this case, the master restarts and, in theory, the pending files should
			// complete before the scan completes.
			FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( pff_name, FLASH_STORAGE_seconds_to_delay_file_save_during_find_or_create );
		}
		
	}
	
	// ----------
	
	mem_free( lfound_dir_entry );  // release the memory we allocated in the beginning of the function
	
	// ----------
	
	if( plist_recursive_mutex != NULL )
	{
		// Give back the list mutex.
		xSemaphoreGiveRecursive( plist_recursive_mutex );
	}	
}

/* ---------------------------------------------------------- */
extern void FLASH_FILE_find_or_create_reports_file(	const UNS_32		pflash_index,
					   								const char			*pfile_name,
					   								UNS_32				platest_revision,

													void				*pvariable_ptr,

													UNS_32 const		*preport_record_size_array,
													UNS_32 const		*preport_record_count_array,
													UNS_32				pcomplete_structure_size,

													xSemaphoreHandle	preport_data_recursive_mutex,
													void				(*pupdater_func_ptr)(UNS_32),
													void				(*pinitialization_func_ptr)(void),

													UNS_32				pff_name )
{
	DF_DIR_ENTRY_s		*lfound_dir_entry;
	
	UNS_32				rstatus_ul, iii;
	
	UNS_8				*from_ucp;

	UNS_8				*to_ucp;
	
	UNS_8				*start_ucp;
	
	BOOL_32				initialize_and_save, older_version_found;
	
	UNS_32				latest_record_size, file_record_size;

	UNS_32				latest_record_count, file_record_count;
	
	// ----------
	
	/*
	// 9/8/2016 rmd : Technique to DELETE a specific file prior to initialization. I have
	// sucessfully done this to fix a trashed file. You do have to uncomment this code. Adjust
	// for your particular file. Build the binary. Send to the SINGLE controller you need to
	// fix. Then build a second binary and ALSO SEND TO THE CONTROLLER. Remember you couldn't
	// just revert back to the original code. What you end up with must be a newer or FORCED
	// binary for the controller (because of the time and date stamp in the binary).
	if( pff_name == FF_BUDGET_REPORT_DATA )
	{
		FLASH_STORAGE_QUEUE_STRUCT fsqs;
		
		fsqs.const_file_name_ptr = pfile_name;

		delete_all_files_with_this_name( pflash_index, &fsqs );
	}
	*/
	
	// ----------
	
	if( preport_data_recursive_mutex != NULL )
	{
		// Take the list MUTEX early on and keep it. 
		xSemaphoreTakeRecursive( preport_data_recursive_mutex, portMAX_DELAY );
	}

	// ----------
	
	lfound_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );  // MEMORY ALLOCATION

	// 1/21/2015 rmd : Clean it for good measure.
	memset( lfound_dir_entry, 0x00, sizeof(DF_DIR_ENTRY_s) );

	strlcpy( lfound_dir_entry->Filename_c, pfile_name, sizeof(lfound_dir_entry->Filename_c) );

	// ----------
	
	initialize_and_save = (false);

	older_version_found = (false);

	// ----------
	
	// START of data flash operations...we don't want the data flash to change until we are done doing our operations
	DfTakeStorageMgrMutex( pflash_index, portMAX_DELAY );

	// NO NEED to initialize anything else in the lfound_full_dir_entry for the following - only the filename
	rstatus_ul = nm_find_latest_version_and_latest_edit_of_a_filename( pflash_index, lfound_dir_entry );

	if( rstatus_ul != DF_DIR_ENTRY_NOT_FOUND )
	{
		// 1/21/2015 rmd : The found dir entry is loaded up with the most recent file as defined by
		// the revision and edit count values.

		// ----------
		
		// 1/21/2015 rmd : If the version is older set us up to call the updater function.
		if( lfound_dir_entry->code_image_date_or_file_contents_revision < platest_revision )
		{
			Alert_flash_file_found_old_version( (char*)pfile_name );

			older_version_found = (true);
		}
		
		// ----------
		
		// 1/21/2015 rmd : Using the array of list item sizes, indexed by the revision value, we
		// check if the file size makes sense. If not we will initialize the file contents.
		latest_record_size = preport_record_size_array[ platest_revision ];

		latest_record_count = preport_record_count_array[ platest_revision ];
		
		file_record_size = preport_record_size_array[ lfound_dir_entry->code_image_date_or_file_contents_revision ];

		file_record_count = preport_record_count_array[ lfound_dir_entry->code_image_date_or_file_contents_revision ];
		
		// ----------
		
		if( (latest_record_size >= file_record_size) && (latest_record_count >= file_record_count) && (lfound_dir_entry->ItemSize_ul == (sizeof(REPORT_DATA_FILE_BASE_STRUCT) + (file_record_count * file_record_size))) )
		{
			#if ALERT_SHOW_ROUTINE_FILE_FINDS_DURING_STARTUP
				Alert_flash_file_found( lfound_dir_entry->Filename_c, lfound_dir_entry->code_image_date_or_file_contents_revision, lfound_dir_entry->edit_count_ul, lfound_dir_entry->ItemSize_ul );
			#endif
	
			// ----------
			
			// crc ????
			
			// ----------
			
			// 2012.02.22 rmd : Get a chunk of memory to read the WHOLE file into.
			start_ucp = mem_malloc( lfound_dir_entry->ItemSize_ul );  // This memory is only used locally and will be freed within this function.

			from_ucp = start_ucp;

			to_ucp = pvariable_ptr;
			
			// ----------
			
			DfReadFileFromDF_NM( pflash_index, lfound_dir_entry, start_ucp );

			// ----------
			
			memcpy( to_ucp, from_ucp, sizeof(REPORT_DATA_FILE_BASE_STRUCT) );
			
			to_ucp += sizeof(REPORT_DATA_FILE_BASE_STRUCT);

			from_ucp += sizeof(REPORT_DATA_FILE_BASE_STRUCT);
			
			for( iii=0; iii<file_record_count; iii++)
			{
				memcpy( to_ucp, from_ucp, file_record_size );
					
				from_ucp += file_record_size;
				
				to_ucp += latest_record_size;
			}
			
			// 1/23/2015 rmd : We're done with it. We have taken a copy of each record, one-by-one, into
			// the final variable location.
			mem_free( start_ucp );
		}
		else
		{
			Alert_flash_file_size_error( (char*)pfile_name );
			
			initialize_and_save = (true);
		}
	}
	else
	{
		if( restart_info.SHUTDOWN_impending == (true) )
		{
			Alert_Message_va( "File Read: abort due to power fail (%s).", pfile_name );
		}
		else
		{
			Alert_flash_file_not_found( (char*)pfile_name );
			
			initialize_and_save = (true);
		}
	}
	
	// ----------
	
	// END of data flash operations...return the flash file system mutex.
	DfGiveStorageMgrMutex( pflash_index );
	
	// ----------
	
	// 10/17/2012 rmd : If the power fail isr has fired no reason to attempt this operation.
	if( !restart_info.SHUTDOWN_impending )
	{
		// 1/21/2015 rmd : The intialize takes precedence over updating.
		if( initialize_and_save )
		{
			// 1/21/2015 rmd : There are checks within the call chain for NULL parameters. However test
			// anyway so we can see a coding error. Should always reference the init function even if it
			// does nothing.
			if( pinitialization_func_ptr != NULL )
			{
				pinitialization_func_ptr();
			}
			else
			{
				Alert_Message( "Flash List: trying to initialize but no function" );
			}
		}
		else
		if( older_version_found )
		{
			if( pupdater_func_ptr != NULL )  // protect against this fatal error
			{
				pupdater_func_ptr( lfound_dir_entry->code_image_date_or_file_contents_revision );
			}
			else
			{
				Alert_Message( "Flash File: trying to update but no function" );
			}
		}

		if( initialize_and_save || older_version_found )
		{
			// 1/27/2015 rmd : This FIND_or_CREATE function is only called during the startup task. At
			// which point the FLASH_FILE tasks have not yet been created. In the STARTUP task we call
			// this function back to back for multiple files. Checking on their existence and health
			// during the boot process. This function itself puts heavy demands on our memory partition
			// system. Because it requires a temporary memory block to read the file out into. In the
			// case of STATION_HISTORY or STATION_REPORT they require the biggest block we have. To
			// write the file (a queued write) also requires that very same biggest block. So if one
			// file is being written (queued) the other cannot run this init function. So that is a
			// problem during startup. We run out of memory. One solution is to postpone the actual
			// write queing activity for 10 seconds. That allows us time to pass through there init
			// calls. And the saves have the built-in mechanism to again postpone if the memory is still
			// not available. Eventually it will become available.
			// 
			// 2/10/2015 ajv : There's a slim possibility that the length of this delay can actually
			// result in an error, though the controller will correct itself. Specifically, if a number
			// of files are pending at the master when a FLOWSENSE scan starts, a code distribution
			// error may occur at the completion of the scan if there's no memory available for the code
			// image. In this case, the master restarts and, in theory, the pending files should
			// complete before the scan completes.
			FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( pff_name, FLASH_STORAGE_seconds_to_delay_file_save_during_find_or_create );
		}
	}
	
	// ----------
	
	mem_free( lfound_dir_entry );  // release the memory we allocated in the beginning of the function
	
	// ----------
	
	if( preport_data_recursive_mutex != NULL )
	{
		// Give back the list mutex.
		xSemaphoreGiveRecursive( preport_data_recursive_mutex );
	}	
}

/* ---------------------------------------------------------- */
extern void FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file(	const UNS_32		pflash_index,
																	const char			*pfile_name,
																	UNS_32				pversion,
																	void				*pbuffer_ptr,
																	UNS_32				pfile_size,
																	xSemaphoreHandle	pdata_recursive_mutex,
																	UNS_32				pff_name )
{
	// In order to ensure the calling task ends up with the expected data in the file system we
	// make a copy of this data in our own allocated memory. After the file is actually written
	// the FLASH STORAGE task can free the memory if requested to.
	// 
	// The intention for this function is for other tasks to be calling it. It is not called
	// internally by the flash file system.
	//
	// 2/4/2014 rmd : NOTE - for the special case of the CODE file types the memory block
	// holding the code image must be stable. And the caller is giving up control of this block
	// to the flash file write subsystem. The caller must not use or disturb the code image
	// subsequent to calling this function else a corrupted image may be written.

	// ----------

	FLASH_STORAGE_QUEUE_STRUCT	lfsqs;

	BOOL_32		got_memory;
	
	UNS_32		iii;
	
	// ----------
	
	got_memory = (false);
	
	if( (pfile_name == CS3000_APP_FILENAME) || (pfile_name == TPMICRO_APP_FILENAME) )
	{
		// 2/4/2014 rmd : The caller is responsible for having the code image available in the
		// memory block pbuffer_ptr. And the caller is to recognize he is giving control of this
		// memory to another process - the flash file write process. The code image must not change
		// or be disturbed following the callers use of this function. After the write to the file
		// system the memory block will be freed by the flash file subsystem.
		got_memory = (true);
		
		lfsqs.file_buffer_ptr = pbuffer_ptr;
	}
	else
	{
		// 2/20/2014 rmd : The file name 'number' is used for a delayed write if there is no memory
		// available. And also used to store the file CRC for application file sync verification
		// purposes. If it is out of range prevent the write!
		if( pff_name < FF_NUMBER_OF_FILES )
		{
			// 2/4/2014 rmd : For all other files get a memory block and place the data to be copied
			// into it.
			if( mem_obtain_a_block_if_available( pfile_size, &(lfsqs.file_buffer_ptr) ) == (true) )
			{
				// 2/4/2014 rmd : show ability to continue with the file write.
				got_memory = (true);
	
				// ----------
			
				// 11/7/2012 rmd : If the caller provided a MUTEX take the mutex just prior to the data
				// copy. Be aware however not all data stored to a file uses a mutex.
				if( pdata_recursive_mutex != NULL )
				{
					xSemaphoreTakeRecursive( pdata_recursive_mutex, portMAX_DELAY );
				}
				
				// ----------
			
				// 2/4/2014 rmd : Fill the memory with the data for the file.
				memcpy( lfsqs.file_buffer_ptr, pbuffer_ptr, pfile_size );
				
				// ----------
				
				// 8/11/2015 rmd : Unfortunately an exception for this particular station history file. If
				// we got the memory and are going to do the file write clear the holding flag.
				if( pff_name == FF_STATION_HISTORY )
				{
					// 8/11/2015 rmd : While we have the completed records mutex take the station preserves
					// mutex. The preserves flag and rip are supposed to be in sync with the completed records.
					// So this seems logical to nest the mutexes.
					xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );
					
					// 8/12/2015 rmd : Note - running through this whole array takes about 1.6ms. Not great but
					// not bad. And only done when the station history file is saved.
					for( iii=0; iii<STATION_PRESERVE_ARRAY_SIZE; iii++ )
					{
						if( station_preserves.sps[ iii ].spbf.station_history_rip_needs_to_be_saved )
						{
							station_preserves.sps[ iii ].spbf.station_history_rip_needs_to_be_saved = (false);
							
							// 8/11/2015 rmd : And wipe its contents. Though this is not needed.
							nm_init_station_history_record( &(station_preserves.sps[ iii ].station_history_rip) );
						}
					}
					
					xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
				}
				
				// ----------
				
				// Not all data stored to a file uses a mutex.
				if( pdata_recursive_mutex != NULL )
				{
					xSemaphoreGiveRecursive( pdata_recursive_mutex );
				}
				
			}	
			else
			{
					#if ALERT_SHOW_ROUTINE_FILE_WRITES
						Alert_flash_file_write_postponed( (char*)pfile_name );
					#endif
				
					// 6/29/2012 rmd : And queue up the request for 5 seconds from now.
					FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( pff_name, 5 );
			}
		}
		else
		{
			Alert_Message( "DATA FILE WRITE: file number out of range!" );
		}
	}

	// ----------

	if( got_memory )
	{
		lfsqs.command = FLASH_TASK_EVENT_write_data_to_flash_file;

		lfsqs.const_file_name_ptr = pfile_name;

		lfsqs.file_version = pversion;

		lfsqs.file_size = pfile_size;

		lfsqs.ff_name = pff_name;
		
		if( xQueueSendToBack( *(ssp_define[ pflash_index].task_queue_ptr), &lfsqs, (portTickType)0 ) != pdPASS )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "FLASH STORAGE: no room on queue" );
		}
	}

	// ----------
}

/* ---------------------------------------------------------- */
void FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file(	const UNS_32		pflash_index,
																const char			*pfile_name,
																UNS_32				pversion,
																MIST_LIST_HDR_TYPE	*plist_hdr_ptr,
																UNS_32				plist_item_size,
																xSemaphoreHandle	plist_recursive_mutex,
																UNS_32				pff_name )

{
	FLASH_STORAGE_QUEUE_STRUCT	lfsqs;
	
	unsigned char	*to_ptr, *from_ptr;
	
	UNS_32			lfile_size;
	

	lfile_size = ( plist_hdr_ptr->count * plist_item_size );
	
	// ----------

	// 2/20/2014 rmd : The file name 'number' is used for a delayed write if there is no memory
	// available. And also used to store the file CRC for application file sync verification
	// purposes. If it is out of range prevent the write!
	if( pff_name < FF_NUMBER_OF_FILES )
	{
		if( mem_obtain_a_block_if_available( lfile_size, &(lfsqs.file_buffer_ptr) ) == (true) )
		{
			// Since we are sequencing through a list that may change (user adds/deletes an element). So
			// protect the list. Remember this function can be called from multiple threads. Not only
			// from the key processing. The communications thread can call it. And when the token is
			// received any user editable list may be updated and saved.
			//
			// At times when this function is called it is inside a section ALREADY protected with the
			// list mutex. We could introduce recursive mutexes. As an alternative the programmer can
			// just send us a NULL as the list mutex. And then this function will not try and take the
			// mutex.
		
			// ----------
	
			// 11/7/2012 rmd : If the caller provided a MUTEX take the mutex just prior to the data
			// copy. Be aware however not all data stored to a file uses a mutex.
			if( plist_recursive_mutex != NULL )
			{
				xSemaphoreTakeRecursive( plist_recursive_mutex, portMAX_DELAY );
			}
		
			// ----------
	
			to_ptr = lfsqs.file_buffer_ptr;
		
			from_ptr = nm_ListGetFirst( plist_hdr_ptr );  // do not REMOVE from the list just get a pointer to
			while( from_ptr != NULL )
			{
				memcpy( to_ptr, from_ptr, plist_item_size );
		
				to_ptr += plist_item_size;
		
				from_ptr = nm_ListGetNext( plist_hdr_ptr, from_ptr );
			}
		
			// ----------
	
			if( plist_recursive_mutex != NULL )
			{
				xSemaphoreGiveRecursive( plist_recursive_mutex );
			}	
		
			// ----------
	
			lfsqs.command = FLASH_TASK_EVENT_write_data_to_flash_file;
			lfsqs.const_file_name_ptr = pfile_name;
			lfsqs.file_version = pversion;
		
			lfsqs.file_size = lfile_size;
	
			lfsqs.ff_name = pff_name;
	
			if( xQueueSendToBack( *(ssp_define[ pflash_index].task_queue_ptr), &lfsqs, (portTickType)0 ) != pdPASS )
			{
				// 7/17/2012 rmd : If we get this error instead of growing the size of the queue we may want
				// to consider the delayed file write technique. Trying again when perhaps there is room on
				// the queue.
				ALERT_MESSAGE_WITH_FILE_NAME( "FLASH STORAGE: no room on queue" );
			}
		}
		else
		{
			#if ALERT_SHOW_ROUTINE_FILE_WRITES
				Alert_flash_file_write_postponed( (char*)pfile_name );
			#endif
		
			// 6/29/2012 rmd : And queue up the request for 5 seconds from now. Just picked 5 seconds
			// thinking well lets just give it some time to free up memory (used for other file saves?).
			FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( pff_name, 5 );
		}
	}
	else
	{
		Alert_Message( "LIST FILE WRITE: file number out of range!" );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Loop through the directory entries and delete all files that match the
    files name. This function manages the data flash mutex at this level. The caller of this
    function does not need to concern themselves with the mutex.

    @CALLER MUTEX REQUIREMENTS (none) the flash file mutex is taken within this function

    @EXECUTED Executed within the context of the FLASH_STORAGE task only. Call the
    FLASH_STORAGE_delete_files_with_this_name function to activate this routine. Though we
    probably could, I have decided to keep it safe and not call this function directly from
    other tasks.

	@RETURN (none)

    @AUTHOR 2011.11.16 rmd
 
    @REVISIONS
	    2012.04.12 ajv : Modified function to static
*/
static void delete_all_files_with_this_name( UNS_32 flash_index, FLASH_STORAGE_QUEUE_STRUCT *pfsqs )
{
	DF_DIR_ENTRY_s		*lsearch_dir_entry;
	
	DF_DIR_ENTRY_s		*lresults_dir_entry;
	
	UNS_32				dir_index_ul, rstatus_ul, files_deleted_ul;
	
	// ----------
	
	DfTakeStorageMgrMutex( flash_index, portMAX_DELAY );  // freeze the data flash so only we can do operations
	
	lsearch_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );
	memset( lsearch_dir_entry, 0, sizeof(DF_DIR_ENTRY_s) );  // Good practice ... better do this so the filename string length is defined.

	lresults_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );
	memset( lresults_dir_entry, 0, sizeof(DF_DIR_ENTRY_s) );  // Good practice ... better do this so the filename string length is defined.

    // load up the search directory entry with the filename, the latest version, and the latest edit ... to be used to decide to delete or not
	strlcpy( (char*)&lsearch_dir_entry->Filename_c, pfsqs->const_file_name_ptr, sizeof(lsearch_dir_entry->Filename_c) );

	files_deleted_ul = 0;
    dir_index_ul = 0;  // start at the beginning

	while( TRUE )
	{
		rstatus_ul = DfFindDirEntry_NM( flash_index, lsearch_dir_entry, lresults_dir_entry, DF_COMPARISON_MASK_FILENAME, dir_index_ul );

		if( rstatus_ul != DF_DIR_ENTRY_NOT_FOUND )
		{
			// Okay we found an entry that matches in filename ... DELETE IT!
			// NO MATCH...delete

			#if ALERT_SHOW_ROUTINE_FILE_DELETION
				Alert_flash_file_deleting_obsolete( flash_index, lresults_dir_entry->Filename_c );
			#endif
			
			DfDelFileFromDF_NM( flash_index, rstatus_ul, lresults_dir_entry );

			files_deleted_ul += 1;

			// if we just returned the last possible directory entry then we can't go any further
			if( rstatus_ul == (ssp_define[ flash_index ].max_directory_entries - 1) )
			{
				break;  // we're all done  ... last of all possible directory entries
			}
			else
			{
				dir_index_ul = rstatus_ul + 1;  // To resume search at the next entry
			}
		}
		else
		{
			break;  // we're all done  ... no more files found
		}
	}

	if( files_deleted_ul == 0 )
	{
		#if ALERT_SHOW_ROUTINE_FILE_DELETION
			Alert_flash_file_obsolete_file_not_found( lsearch_dir_entry->Filename_c );
		#endif
	}

	mem_free( lsearch_dir_entry );

	mem_free( lresults_dir_entry );
	
	DfGiveStorageMgrMutex( flash_index );  // release the data flash for operations by others
}

/* ---------------------------------------------------------- */
static void nm_find_older_files_and_delete( UNS_32 flash_index, const char *pfile_name, UNS_32 pversion_ul, UNS_32 pedit_count_ul )
{
    // Loop through the directory entries and delete all files that do not match the submitted key fields.
	// This function manages the data flash mutex at this level. The caller of this function does not need to concern themselves with the mutex.

	DF_DIR_ENTRY_s		*lsearch_dir_entry;
	
	DF_DIR_ENTRY_s		*lresults_dir_entry;
	
	UNS_32				dir_index_ul, rstatus_ul, files_deleted_ul;
	
	// ----------
	
	lsearch_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );

	lresults_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );

	// ----------
	
    // load up the search directory entry with the filename, the latest version, and the latest edit ... to be used to decide to delete or not
	strlcpy( (char*)&lsearch_dir_entry->Filename_c, (char*)pfile_name, sizeof(lsearch_dir_entry->Filename_c) );

	lsearch_dir_entry->code_image_date_or_file_contents_revision = pversion_ul;

	lsearch_dir_entry->edit_count_ul = pedit_count_ul;

	// ----------
	
	files_deleted_ul = 0;

    dir_index_ul = 0;  // start at the beginning

	while( TRUE )
	{
		rstatus_ul = DfFindDirEntry_NM( flash_index, lsearch_dir_entry, lresults_dir_entry, DF_COMPARISON_MASK_FILENAME, dir_index_ul );

		if( rstatus_ul != DF_DIR_ENTRY_NOT_FOUND )
		{
			// okay we found an entry that matches in filename only so far...test against the latest VERSION and EDIT_COUNT...if no match then delete the file
			if( DfDirEntryCompare( lsearch_dir_entry, lresults_dir_entry, DF_COMPARISON_MASK_DATE_OR_REVISION | DF_COMPARISON_MASK_EDIT_COUNT ) != 0 )
			{
				// NO MATCH...delete
				#if ALERT_SHOW_ROUTINE_FILE_DELETION
					Alert_flash_file_deleting( flash_index, lresults_dir_entry->Filename_c, lresults_dir_entry->code_image_date_or_file_contents_revision, lresults_dir_entry->edit_count_ul, lresults_dir_entry->ItemSize_ul );
				#endif
				
				DfDelFileFromDF_NM( flash_index, rstatus_ul, lresults_dir_entry );

				files_deleted_ul += 1;
			}

			// if we just returned the last possible directory entry then we can't go any further
			if( rstatus_ul == (ssp_define[ flash_index ].max_directory_entries - 1) )
			{
				break;  // we're all done  ... last of all possible directory entries
			}
			else
			{
				dir_index_ul = rstatus_ul + 1;  // To resume search at the next entry
			}
		}
		else
		{
			break;  // we're all done  ... no more files found
		}
	}

	#if ALERT_SHOW_ROUTINE_FILE_DELETION
		if( files_deleted_ul == 0 )
		{
			Alert_flash_file_old_version_not_found( flash_index, lsearch_dir_entry->Filename_c );
		}
	#endif

	mem_free( lsearch_dir_entry );

	mem_free( lresults_dir_entry );
}

/* ---------------------------------------------------------- */
extern void write_data_to_flash_file( const UNS_32 pflash_index, FLASH_STORAGE_QUEUE_STRUCT *pfsqs )
{
	// This function is executed within the context of the flash_storage task. And manages the
	// data flash mutex at this level. The caller of this function does not need to concern
	// themselves with the mutex.

	// This function is called potentially the very first time a variable is stored (ie no file
	// exists) and during routine file updates.

	// ----------
	
	DF_DIR_ENTRY_s		*ldir_entry;

	// ----------
	
	// Build up a directory entry so we can find the latest version in the DataFlash.
	ldir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );

	// to start with a fully clean directory entry
	memset( ldir_entry, 0x00, sizeof(DF_DIR_ENTRY_s) );

	// ----------
	
	// For the hunt for the latest version and latest edit just provide the filename.
	strlcpy( (char*)&(ldir_entry->Filename_c), pfsqs->const_file_name_ptr, sizeof(ldir_entry->Filename_c) );

	// ----------
	
	DfTakeStorageMgrMutex( pflash_index, portMAX_DELAY );  // freeze the data flash at its current state

	// ----------
	
	if( (pfsqs->const_file_name_ptr == CS3000_APP_FILENAME) || (pfsqs->const_file_name_ptr == TPMICRO_APP_FILENAME) )
	{
		ldir_entry->code_image_date_or_file_contents_revision = convert_code_image_date_to_version_number( pfsqs->file_buffer_ptr, pfsqs->const_file_name_ptr );

		ldir_entry->edit_count_ul = convert_code_image_time_to_edit_count( pfsqs->file_buffer_ptr, pfsqs->const_file_name_ptr );
		
		// 2/7/2014 rmd : If either are 0 we have a problem in the conversion.
		if( (ldir_entry->code_image_date_or_file_contents_revision == 0) || (ldir_entry->edit_count_ul == 0) )
		{
			Alert_Message( "FILE: code revision conversion problem" );	
		}
	}
	else
	{
		if( nm_find_latest_version_and_latest_edit_of_a_filename( pflash_index, ldir_entry ) != DF_DIR_ENTRY_NOT_FOUND )
		{
			ldir_entry->edit_count_ul += 1;  // this was REALLY the reason to find the latest_version_latest_edit file
		}
		else
		{
			ldir_entry->edit_count_ul = 1;
		}

		// 1/28/2015 rmd : And make sure to set the NEW REVISION value - if its new - into the
		// directory entry now. AFTER the search for the latest version and edit function call. As
		// that function call returns with the ldir_entry loaded with the specifics for the file
		// found.
		ldir_entry->code_image_date_or_file_contents_revision = pfsqs->file_version;
	}

	// ----------
	
	ldir_entry->ItemSize_ul = pfsqs->file_size;

	// ldir_entry filename is preserved throughout previous function to find latest

	if( DfWriteFileToDF_NM( pflash_index, ldir_entry, pfsqs->file_buffer_ptr ) == DF_DUPL_ITEM_ALREADY_EXISTS )
	{
		// 2/7/2014 rmd : For normal user files this should never happen as we just incremented the
		// edit count. However for code images if the same file is sent twice this error will post.
		Alert_Message( "FILE: file should not already exist!" );
	}
	else
	{
		#if ALERT_SHOW_ROUTINE_FILE_WRITES
			Alert_flash_file_writing( pflash_index, ldir_entry->Filename_c, ldir_entry->code_image_date_or_file_contents_revision, ldir_entry->edit_count_ul, ldir_entry->ItemSize_ul );
		#endif
	}
	
	// ----------
	
	// 2/4/2014 rmd : We always free the file data buffer memory after the write has completed.
	// This is by design. We are given the memory free responsiblity.
	mem_free( pfsqs->file_buffer_ptr );
	
	// ----------
	
	// 1/20/2015 rmd : Now remove older file versions. There should be at least one.
	nm_find_older_files_and_delete( pflash_index, pfsqs->const_file_name_ptr, ldir_entry->code_image_date_or_file_contents_revision, ldir_entry->edit_count_ul );

	// ----------
	
	DfGiveStorageMgrMutex( pflash_index );

	// ----------
	
	mem_free( ldir_entry );  // don't free this memory till we are done with the data contained within it
	
	// ----------
	
	// 2/20/2014 rmd : For the case of the code image files we signal the code_distribution task
	// these writes have taken place.
	if( pfsqs->const_file_name_ptr == CS3000_APP_FILENAME )
	{
		CODE_DISTRIBUTION_post_event( CODE_DISTRIBUTION_EVENT_main_binary_file_write_complete );
	}
	else
	if( pfsqs->const_file_name_ptr == TPMICRO_APP_FILENAME )
	{
		CODE_DISTRIBUTION_post_event( CODE_DISTRIBUTION_EVENT_tpmicro_binary_file_write_complete );
	}
	
}
		
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Can be invoked from any task. Will queue up the request on the flash
    storage queue to process when task loading allows. Typically called during the startup
    task to purge obselete files from the file system.

    @CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED From any task.

	@RETURN (none)

	@AUTHOR 2011.11.16 rmd
*/
void FLASH_STORAGE_delete_all_files_with_this_name(	UNS_32 flash_index, const char *pfile_name )
{
	FLASH_STORAGE_QUEUE_STRUCT	lfsqs;
	
	if( pfile_name != NULL )
	{
		lfsqs.command = FLASH_TASK_EVENT_delete_all_files_with_this_name;

		lfsqs.const_file_name_ptr = pfile_name;
	
		if( xQueueSendToBack( *(ssp_define[ flash_index].task_queue_ptr), &lfsqs, (portTickType)0 ) != pdPASS )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "FLASH STORAGE: no room on queue" );
		}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void FLASH_STORAGE_flash_storage_task( void *pvParameters )
{
	// Task to process certain flash storage needs. This includes writing variable files and group files to the flash. Determining what has changed
	// and generating the appropriate change lines. And performing the flash cleaning job of removing older file versions.
	// 
	// We used to just call these flash write function on the fly as we were say performing editing or other activities that caused a saved data structure to change.
	// The change meant we should save it to data flash. So we just called that function. But it proved to be somewhat lengthy and you could actually notice a pause
	// in the UI as we wrote to the data flash when we left a screen. So I moved this work to a task at one priority level lower than the keypad task.

	FLASH_STORAGE_QUEUE_STRUCT	fsqs;
	
	UNS_32	flash_index, task_index;
	
	//UNS_32	ticks_delta;
	
	// ----------
	
	// 10/23/2012 rmd : Passed parameter is the address to the Task_Table entry used to create
	// this task. So we can figure out it's index to use when keeping the watchdog activity
	// monitor moving. And extract the port this task is responsible for.
	TASK_ENTRY_STRUCT	*tt_ptr;
	
	tt_ptr = pvParameters;
	
	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// 10/23/2012 rmd : And in this case the device we are responsible for is in the table.
	flash_index = (UNS_32)tt_ptr->parameter;
	
	while( (true) )
	{
		// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
		if ( xQueueReceive( *(ssp_define[ flash_index ].task_queue_ptr), &fsqs, MS_to_TICKS( 500 ) ) == pdTRUE ) 
		{
			// 10/17/2012 rmd : Only if the powerfail ISR has not fired do we want to start another
			// flash file operation.
			if( restart_info.SHUTDOWN_impending == (false) )
			{
				switch( fsqs.command )
				{
					case FLASH_TASK_EVENT_return_code_image_version_stamp:
						return_code_image_version_stamp( flash_index, &fsqs );
						break;
	
					case FLASH_TASK_EVENT_read_code_image_file_to_memory_for_tpmicro_isp_mode:
					case FLASH_TASK_EVENT_read_code_image_file_to_memory_for_code_transmit:
						read_code_image_file_to_memory( flash_index, &fsqs );
						break;
	
					case FLASH_TASK_EVENT_write_data_to_flash_file:
					
						// 11/2/2018 rmd : ALL file writes, no matter what file type, come through here. Regarding
						// the file CRC that we pass around through the tokens, one thing i am doing, is using when
						// pdata is added or extracted from a message (either from commserver, token, or token
						// response) as a way to catch all pdata changes, and INVALIDATE the file crc i have on
						// hand. WELL THAT AIN'T GOOD ENOUGH! When the clean house command comes in, we delete all
						// the file content (unwind the list) and save the file, BUT DO NOT SET ANY CHANG BITS, so
						// there is no resultant message content. This is intentional (don't want to get into why
						// here, but i believe to stop those changes from syncing back to the master). So it ended
						// up after the clean house was performed at the slaves, I was still using the saved
						// previously calculated file crc, and that was incorrect. So catching here as a supplement
						// spot to invalidate the file crc covers both regular file saves that take place, and the
						// clean command associated file saves. I'm still keeping the extract pdata content in the
						// token/token_resp too.
						CHAIN_SYNC_inhibit_crc_inclusion_in_the_token_response( fsqs.ff_name );
						
						// ----------
						
						//ticks_delta = my_tick_count;
						
						write_data_to_flash_file( flash_index, &fsqs );
						
						//ticks_delta = (my_tick_count - ticks_delta);
						
						// 3/10/2017 rmd : An alert just for confidence building. I've been watching it now for
						//about two months. So feel all is fine.
						//Alert_Message_va( "file write duration is %u ms", (ticks_delta*5) );
						break;
	
					case FLASH_TASK_EVENT_delete_all_files_with_this_name:
						delete_all_files_with_this_name( flash_index, &fsqs );
						break;
				}

			}

		}

		// ----------
		
		// 10/23/2012 rmd : Keep the activity indication going.
		task_last_execution_stamp[ task_index ] = xTaskGetTickCount();

		// ----------
		
	}  // end of task while loop

}  // of function

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/16/2012 rmd : The timer handles for the time delayed file save. Which is automatically
// activated if the memory pool is short enough memory to take a copy of the data to be
// saved. This comes into play on start up when saving two large files such as station
// history and station report data. They are each around the 600K mark. And if we did not
// have the mechanism to postpone the saving of one of them we would need sufficient memory
// in the pool to support both simultaneously. Meaning TWO 600K blocks. With the memory not
// available detection triggering a postponed file save we only need ONE 600K block!
xTimerHandle	ff_timers[ FF_NUMBER_OF_FILES ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void ff_timer_callback( xTimerHandle pxTimer )
{
	// This is executed within the context of the Timer Task. As any one of the file timers
	// expires.

	UNS_32	which_file_timer_expired;

	which_file_timer_expired = (UNS_32)pvTimerGetTimerID( pxTimer );
	
	switch( which_file_timer_expired )
	{
		case FF_CONTROLLER_CONFIGURATION:
			save_file_configuration_controller();
			break;
			
		case FF_NETWORK_CONFIGURATION:
			save_file_configuration_network();
			break;
			
		case FF_SYSTEM_REPORT_DATA:
			save_file_system_report_records();
			break;
			
		case FF_POC_REPORT_DATA:
			save_file_poc_report_records();
			break;
			
		case FF_LIGHTS_REPORT_DATA:
			save_file_lights_report_data();
			break;

		case FF_STATION_REPORT_DATA:
			save_file_station_report_data();
			break;
			
		case FF_STATION_HISTORY:
			save_file_station_history();
			break;
			
		case FF_WEATHER_CONTROL:
			save_file_weather_control();
			break;
			
		case FF_WEATHER_TABLES:
			save_file_weather_tables();
			break;
			
		case FF_TPMICRO_DATA:
			save_file_tpmicro_data();
			break;
			
		case FF_STATION_INFO:
			save_file_station_info();
			break;
			
		case FF_IRRIGATION_SYSTEM:
			save_file_irrigation_system();
			break;
			
		case FF_POC:
			save_file_POC();
			break;
			
		case FF_MANUAL_PROGRAMS:
			save_file_manual_programs();
			break;
			
		case FF_LANDSCAPE_DETAILS:
			save_file_station_group();
			break;
			
		case FF_FLOWSENSE:
			save_file_flowsense();
			break;
			
		case FF_CONTRAST_AND_VOLUME:
			save_file_contrast_and_speaker_vol();
			break;
			
		case FF_LIGHTS:
			save_file_LIGHTS();
			break;

		case FF_MOISTURE_SENSORS:
			save_file_moisture_sensor();
			break;
			
		case FF_BUDGET_REPORT_DATA:
			save_file_budget_report_records();
			break;
			
		case FF_WALK_THRU:
			save_file_walk_thru();
			break;

		default:
			Alert_Message_va( "FILE SAVE: %d case not handled" );
			
	}
}

/* ---------------------------------------------------------- */
extern void FLASH_STORAGE_create_delayed_file_save_timers( void )
{
	INT_32	i;
	
	for( i=0; i<FF_NUMBER_OF_FILES; i++ )
	{
		// 7/16/2012 rmd : The timers are created here and now. Meaning the creation is not a post
		// to the timer task queue. Like most other timer task activities. So upon exit from this
		// function, no matter what priority the task is this is called from ... the timers have
		// been created.
		ff_timers[ i ] = xTimerCreate( (const signed char*)"",			// Just a text name, not used by the kernel.
		                               MS_to_TICKS( 1000 ),				// The timer period in ticks. Cannot be 0. And can be changed when timer is started.
									   pdFALSE,							// The timers will NOT auto-reload themselves when they expire.
									   (void *)i,						// Assign each timer a unique id equal to its array index.
									   ff_timer_callback				// Each timer calls the same callback when it expires.
									  );
	}
	
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This function is integrated into the file save function within the
    FLASH_STORAGE level function itself. To date has only been completed for the 'variable'
    type file storage (not the list file saves). This function can also be called directly
    by the user to cause a file save X number of seconds from the call. This is convenient
    for cases where repetitive changes to a file will be made and we would like one
    concluding save to take place at the end. Such as during the speaker volume adjustment.
 
    Note that passing 0 seconds into the routine will result in the save occurring in 25
    milliseconds. The FreeRTOS documentation does not explicilty state that a timer period
    of 0 is invalid when changing the timer period, but we'd rather be safe. This 25
    milliseconds is not fixed - if we decide we want to increase or decrease that time, edit
    it within this procedure.
	
	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the all tasks. The xTimerChangePeriod function
    actually posts to the timer task. Which being a high priority task runs immediately.
    Starting the timer.
	
	@RETURN (none)

	@ORIGINAL 2012.06.04 rmd

	@REVISIONS (none)
*/
extern void FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( UNS_32 const pff_filename, UNS_32 const pseconds_till_save )
{
	UNS_32	timer_period_in_ticks;

	if( pseconds_till_save > 0 )
	{
		timer_period_in_ticks = MS_to_TICKS(1000 * pseconds_till_save);
	}
	else
	{
		// 9/18/2015 rmd : Note that passing 0 seconds into the routine will result in the save
		// occurring in 25 milliseconds. The FreeRTOS documentation does not explicilty state that a
		// timer period of 0 is invalid when changing the timer period, but we'd rather be safe.
		timer_period_in_ticks = MS_to_TICKS(25);
	}

	if( pff_filename >= FF_NUMBER_OF_FILES )
	{
		Alert_Message_va( "FILE WRITE: file number %u out of range", pff_filename );
	}
	else
	{
		// The timer function to change the period actually changes the period and STARTS the timer!
		//
		// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
		// when posting is safe. Anything on the timer queue is quickly processed and therefore the
		// queue is effectively always empty.
		xTimerChangePeriod( ff_timers[ pff_filename ], timer_period_in_ticks, portMAX_DELAY );
	}
}

/* ---------------------------------------------------------- */
extern void FLASH_STORAGE_if_not_running_start_file_save_timer( UNS_32 const pff_filename, UNS_32 pseconds_till_save )
{ 
	// 8/6/2015 rmd : May be executed within the context of any task.
	
	// ----------
	
	if( pff_filename >= FF_NUMBER_OF_FILES )
	{
		Alert_Message_va( "FILE WRITE: file number %u out of range", pff_filename );
	}
	else
	if( !ff_timers[ pff_filename ] )
	{
		Alert_Message_va( "FILE WRITE: timer doesn't exist %u", pff_filename );
	}
	else
	{
		if( !pseconds_till_save )
		{
			Alert_Message( "file save in 0 seconds?" );	

			// 8/6/2015 rmd : Just set it to something. Not sure what happens if you set the timer
			// period to 0?
			pseconds_till_save = 1;
		}

		// 8/15/2014 rmd : Only if it is not running. That's the behavior for this funciton. Not to
		// keep restarting the timer each time this function is called.
		if( !xTimerIsTimerActive( ff_timers[ pff_filename ] ) )
		{
			// The timer function to change the period actually changes the period and STARTS the timer!
			//
			// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
			// when posting is safe. Anything on the timer queue is quickly processed and therefore the
			// queue is effectively always empty.
			xTimerChangePeriod( ff_timers[ pff_filename ], MS_to_TICKS( 1000 * pseconds_till_save), portMAX_DELAY );
		}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


/**
 * Allows deletion of a file based upon it's filename. This should be used on a
 * firmware update if a file has been removed.
 * 
 * @mutex Takes and gives the DF Storage Manager mutex to prevent reentrancy.
 *
 * @executed
 *
 * @param pflash_index_u32 Either FLASH_INDEX_0_BOOTCODE_AND_EXE or
 *  					   FLASH_INDEX_1_GENERAL_STORAGE
 * @param pfilename The actual filename string.
 *
 * @author 11/11/2011 Adrianusv
 *
 * @revisions
 *    11/11/2011 Initial release
 */
/*
extern void FLASH_STORAGE_delete_a_file_using_filename( const UNS_32 pflash_index_u32, 
const char *pfilename ) { 
	DF_DIR_ENTRY_s		*lfound_dir_entry;

	UNS_32				lrstatus_u32;

	lfound_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );

	strlcpy( lfound_dir_entry->Filename_c, pfilename, sizeof(lfound_dir_entry->Filename_c) );

	// START of data flash operations... we don't want the data flash to change
	// until we are done doing our operations
	DfTakeStorageMgrMutex( pflash_index_u32, portMAX_DELAY );

	lrstatus_u32 = DFLASH_STORAGE_find_latest_version_and_latest_edit_of_a_filename_NM( pflash_index_u32, lfound_dir_entry );

	if( lrstatus_u32 != DF_DIR_ENTRY_NOT_FOUND )
	{
		DfDelFileFromDF_NM( pflash_index_u32, lrstatus_u32, lfound_dir_entry );

		Alert_Message_va( "%s deleted", pfilename );
	}
	else
	{
		Alert_Message_va( "File Not Found to Delete: %s", pfilename );
	}

	DfGiveStorageMgrMutex( pflash_index_u32 );
}
*/







/* 
void toggle_LED( UNS_32 flash_index )
{
	if( flash_index == 0 )
	{
		if( P0_OUTP_STATE & CS_LED_YELLOW )
		{
			CS_LED_YELLOW_OFF;	
		}
		else
		{
			CS_LED_YELLOW_ON;
		}
	}
	else
	{
		if( P0_OUTP_STATE & CS_LED_RED )
		{
			CS_LED_RED_OFF;	
		}
		else
		{
			CS_LED_RED_ON;
		}
	}
}

void flash_file_system_test_TASK( void *pvParameters )
{
	#define DF_MAX_FILE_SIZE_DURING_TEST	0x10000
	
	UNS_8	*buf, starting_value, captured_starting_value;

	UNS_32	flash_index, iii, file_size, size_reset_value_0, size_reset_value_1, results;
	
	UNS_8	str_256[ 256 ];
	
	vTaskDelay( 2500 );
	
	flash_index = (UNS_32)pvParameters;
	
	buf = mem_malloc( DF_MAX_FILE_SIZE_DURING_TEST );
	
	file_size = 1;

	starting_value = 0x00;
	
	size_reset_value_0 = 1;
	size_reset_value_1 = DF_MAX_FILE_SIZE_DURING_TEST;
	
	for( ;; )
	{
		//Alert_Message( "" );

		captured_starting_value = starting_value;

		for( iii=0; iii<file_size; iii++ )
		{
			// write (incrementing value) pattern into each 32-bit word of memory
			*(buf + iii) = starting_value;
			
			if( flash_index == 0 )
			{
				starting_value++;
			}
			else
			{
				starting_value--;
			}
		}

		FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file( flash_index, "static name", 0x0A, buf, file_size );
		
		toggle_LED( flash_index );
		
		memset( buf, 0x00, DF_MAX_FILE_SIZE_DURING_TEST );
		// We've cleared the buf. If the file is not found and the init_func_ptr is NULL (as it is) the buf will stay cleared. And therefore the test
		// will fail if the file is NOT found.
		FLASH_STORAGE_find_or_create_variable_file( flash_index, "static name", 0x0A, buf, file_size, NULL );

		toggle_LED( flash_index );

		// TEST it
		results = TRUE;
		for( iii=0; iii<file_size; iii++ )
		{
			// write (incrementing value) pattern into each 32-bit word of memory
			if( *(buf + iii) != captured_starting_value )
			{
				snprintf( (char*)str_256, sizeof(str_256), "\r\n\n Chip: %d, FAILED! File Size=%d", flash_index, file_size );
				term_dat_out( str_256);

				results = FALSE;
				
				for( ;; );  // FREEZE!!!!!

				break;  // only tell about once
			}

			if( flash_index == 0 )
			{
				captured_starting_value++;
			}
			else
			{
				captured_starting_value--;
			}
		}

		if( results == TRUE )
		{
			snprintf( (char*)str_256, sizeof(str_256), "\r\n Chip: %d, SUCCESS! File Size=%d", flash_index, file_size );
			term_dat_out( str_256);
		}
		

		if( flash_index == 0 )
		{
			file_size += 0x30;  // An increment size. So we continue on with other sizes.

			if( file_size > DF_MAX_FILE_SIZE_DURING_TEST )
			{
				size_reset_value_0 += 1;
				if( size_reset_value_0 > DF_MAX_FILE_SIZE_DURING_TEST )
				{
					size_reset_value_0 = 1;	
				}
				
				file_size = size_reset_value_0;  // All possible file sizes will eventually be tried?
			}
		}
		else
		{
			file_size -= 0x200;  // An increment size. So we continue on with other sizes.

			if( file_size > DF_MAX_FILE_SIZE_DURING_TEST )
			{
				size_reset_value_1 -= 1;

				if( size_reset_value_1 == 0 )
				{
					size_reset_value_1 = DF_MAX_FILE_SIZE_DURING_TEST;	
				}

				file_size = size_reset_value_1;  // All possible file sizes will eventually be tried?
			}
		}
	}
}

*/





/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/

#ifdef NOT_COMPILING_THIS_SPI_FLASH_TEST_CODE

/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/

#include	"serial.h"

/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
///////////////////////////////////////////////////////////////
//
//  File:   DfTestTask.c - A task to exercise the DF storage
//                          Manager functions and to provide
//                          examples of their usage.
///////////////////////////////////////////////////////////////

// Largest data item is sized to be just over one cluster. Presently the
// DF subsystem is configured to have 3 DF pages per cluster,
// or 3 * 512 = 1536 bytes.
#define DF_FILE1_SIZE   6000

// Force the test to use the highest level API to the flash file system. The level normally used that is.
// Commenting out will cause the test to use a mid level API that typically isn't used by the application.
//
// Your testing methodology ought to be to first do the test at the LOW LEVEL API level. Meaning the # define for
// DF_TEST_USING_HIGH_LEVEL_API would be commented out. After you have analyzed the output file names and such then
// wrap up testing at the high level API level.
#define DF_TEST_USING_HIGH_LEVEL_API

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
///////////////////////////////////////////////////////////////
//
//  char *DfStatusToStr(unsigned long df_status_ul)
//
//  This function converts a DF storage manager return status
//  value into a pointer to a message string for that status.
//
//  Note that in the interest of simplicity and space I used
//  somewhat unconventional formatting for the switch statement
//  cases. While unconventional, it's really quite readable.
//
///////////////////////////////////////////////////////////////
char *DfStatusToStr(unsigned long df_status_ul)
{
	char *pStr;

	switch( df_status_ul)
	{
	case DF_RESULT_SUCCESSFUL:          pStr = "DF_RESULT_SUCCESSFUL";          break;
	case DF_DIR_ENTRY_NOT_FOUND:        pStr = "DF_DIR_ENTRY_NOT_FOUND";        break;
	case DF_INSUFFICIENT_SPACE:         pStr = "DF_INSUFFICIENT_SPACE";         break;
	case DF_DUPL_ITEM_ALREADY_EXISTS:   pStr = "DF_DUPL_ITEM_ALREADY_EXISTS";   break;
	case DF_ZERO_ITEM_SIZE_IS_INVALID:  pStr = "DF_ZERO_ITEM_SIZE_IS_INVALID";  break;
	case DF_NO_DIR_ENTRY_AVAIL:         pStr = "DF_NO_DIR_ENTRY_AVAIL";         break;
	case DF_INVALID_CLUSTER_VALUE:      pStr = "DF_INVALID_CLUSTER_VALUE";      break;
	case DF_INVALID_SIZE_SPECIFIED:     pStr = "DF_INVALID_SIZE_SPECIFIED";     break;
	case DF_CRC_DOES_NOT_MATCH:         pStr = "DF_CRC_DOES_NOT_MATCH";         break;
	default:                            pStr = "** UNKNOWN STATUS **";          break;
	}
	return pStr;
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void DisplayDirEntry( UNS_32 flash_index, DF_DIR_ENTRY_s *pde )
{
	UNS_8		str_160[ 160 ];
	snprintf( (char*)str_160, sizeof(str_160), "Chip %d, Filename: %32s, Version = %8x, Edit_count = %8x, Size = %8u bytes, CRC = %8x \r\n\n", flash_index, pde->Filename_c, pde->version_ul, pde->edit_count_ul, pde->ItemSize_ul, pde->ItemCrc32_ul );
	term_dat_out( str_160 );
}

/* ---------------------------------------------------------- */
void Display_all_directory_entries( UNS_32 flash_index )
{
	unsigned long		index;

	unsigned long		rstatus;

	DF_DIR_ENTRY_s		*pDirEntry2;

	DF_DIR_ENTRY_s		*pDirEntry1;

	term_dat_out( (UNS_8*)"**** ALL Directory Entries Start ****\r\n" );


	pDirEntry1 = mem_malloc(sizeof(DF_DIR_ENTRY_s));
	if (pDirEntry1) memset(pDirEntry1, 0, sizeof(DF_DIR_ENTRY_s)); else for(;;);


	pDirEntry2 = mem_malloc(sizeof(DF_DIR_ENTRY_s));
	if (pDirEntry2) memset(pDirEntry2, 0, sizeof(DF_DIR_ENTRY_s)); else for(;;);


	#ifdef DF_TEST_USING_HIGH_LEVEL_API
		DfTakeStorageMgrMutex( flash_index, portMAX_DELAY );
	#endif	

	// We leave *pDirEntry1 uninitialized because we will force the DfFindDirEntry()
	// function to find ALL directory entries by setting fields_to_check_us to zero.
	index = 0;
	while ( TRUE )
	{
		// This call should return every in-use dir entry in *pDirEntry2
		rstatus = DfFindDirEntry_NM( flash_index,
									 pDirEntry1,	// Search pattern fields
								     pDirEntry2,	// Where dir entry hits are copied
								     0,				// dir entry fields to check: 0 = none (all entries match)
								     index);		// Starting dir index

		if( DF_DIR_ENTRY_NOT_FOUND != rstatus)
		{
			// Display the directory entry "hit"
			DisplayDirEntry( flash_index, pDirEntry2 );

			index = rstatus + 1;        // To resume search at the next dir entry
		}
		else
		{
			// We will "break" when DF_DIR_ENTRY_NOT_FOUND is returned by the find function
			break;
		}
	}

	term_dat_out( (UNS_8*)"**** Directory Entries End ****\r\n" );

	#ifdef DF_TEST_USING_HIGH_LEVEL_API
		DfGiveStorageMgrMutex( flash_index );
	#endif	

	mem_free( pDirEntry1 );
	mem_free( pDirEntry2 );
}


/* ---------------------------------------------------------- */
void Display_free_space( UNS_32 flash_index )
{
    // Get and report free space on DF
	unsigned long   free_space;

	#ifdef DF_TEST_USING_HIGH_LEVEL_API
		DfTakeStorageMgrMutex( flash_index, portMAX_DELAY );
        free_space = DfGetFreeSpace_NM( flash_index );
		DfGiveStorageMgrMutex( flash_index );
	#else
		free_space = DfGetFreeSpace_NM( flash_index );
	#endif	
		
	UNS_8		str_128[ 128 ];
    snprintf( (char*)str_128, sizeof(str_128), "Chip %d, DF Space available: %lu bytes\r\n\n", flash_index, free_space );
	term_dat_out( str_128 );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void file_system_test( UNS_32 flash_index )
{
	UNS_8			str_256[ 256 ];
	
	DF_DIR_ENTRY_s	*pDirEntry2;
		
	DF_DIR_ENTRY_s	*pDirEntry1;
	
	unsigned long	*pMem, results;
	
	unsigned long	index;

	char			*pString;
	
	unsigned long	size;

	#ifndef DF_TEST_USING_HIGH_LEVEL_API
		UNS_32	rstatus;
	#endif	

	// Force re-init of DF (uncommented for some testing)
	//init_DF_storage_mngr( TRUE );

	// Announce task start-up
	snprintf( (char*)str_256, sizeof(str_256), "\r\n>>>> Chip %d DF Storage Manager Test <<<< - %s \r\n\n", flash_index, __DATE__ );
	term_dat_out( str_256 );


	#ifndef DF_TEST_USING_HIGH_LEVEL_API

		// Gain exclusive access to the DF storage manager (wait up to 1/2 second)
		term_dat_out( (UNS_8*)"Calling DfTakeStorageMgrMutex twice to gain access to the DF Storage Manager..\n\r");
		if( pdTRUE == DfTakeStorageMgrMutex( flash_index, 100 ))
		{
			term_dat_out( (UNS_8*)"Access was gained on first call.\n\r");
		}
		else
		{
			term_dat_out( (UNS_8*)"Access was denied on first call! Test aborted!\n\r");
			for(;;);	// HANG
		}
	
	#endif


    Display_free_space( flash_index );

	

	///////////////////////////////////////////////////////
	//
	// Create and write the first data item to DF
	//
	///////////////////////////////////////////////////////

	// Get a chunk of memory for the file data.
	pMem = mem_malloc( DF_FILE1_SIZE );

	// Write a data pattern into the chunk of memory.
	if( NULL != pMem )
	{
		for( index = 0; index < DF_FILE1_SIZE/sizeof(unsigned long); index++ )
		{
			// write known (incrementing value) pattern into each 32-bit word of memory
			*(pMem + index) = index;
		}
	}
	else
	{
		// Allocation error - display message and hang
		term_dat_out( (UNS_8*)"Unable to allocated file sized memory block!\n\r" );

		for(;;);	// HANG
	}

	snprintf( (char*)str_256, sizeof(str_256), "Chip %d: Now writing item #1 (%d bytes) to DF...\n\r", flash_index, DF_FILE1_SIZE );
	term_dat_out( str_256 );

	// Now write the data to the DF. We can also include other information along with
	// the primary key fields of Item ID, Filename, and Version ID.

	#ifdef DF_TEST_USING_HIGH_LEVEL_API

		FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file( flash_index, "QuiteBogusTestCase", 0x00000043, pMem, DF_FILE1_SIZE );
		// The prior call to write the file only queues the request to the flash file system task. The actual work will get done subsequently. And will try
		// to take the mutex. Therefore we must as you see NOT have taken the mutex.
		vTaskDelay( 500 );

	#else

		memset( &tmp_dir,  0x00,  sizeof(DF_DIR_ENTRY_s) );
		snprintf( tmp_dir.Filename_c, sizeof(tmp_dir.Filename_c), "QuiteBogusTestCase" );
		tmp_dir.ItemSize_ul = DF_FILE1_SIZE;
		tmp_dir.version_ul = 0x00000043;
		tmp_dir.edit_count_ul = 1;
		rstatus = DfWriteFileToDF_NM( flash_index, &tmp_dir, (unsigned char *)pMem );
	
		snprintf( (char*)str_256, sizeof(str_256), "Chip %d: Write of item 1 (size = %lu) returned status = %s\n\r", flash_index, (unsigned long)DF_FILE1_SIZE, DfStatusToStr(rstatus) );
		term_dat_out( str_256 );

	#endif

	Display_free_space( flash_index );

	/////////////////////////////////////////////////////////
	//
	// Create and write a second data item to DF. This will
	// be just a text string.
	//
	/////////////////////////////////////////////////////////

	// Clear out our previously (dynamically) allocated buffer
	memset( pMem, 0, DF_FILE1_SIZE );
	// Copy a silly meaningless string into the buffer
	pString = "The quick brown fox is lazy so it walks around the even lazier dog.\n\r";
	strcpy( (char*)pMem, pString );
	size = strlen( pString );

	snprintf( (char*)str_256, sizeof(str_256), "Chip %d, Now writing item #2 to DF...\n\r", flash_index );
	term_dat_out( str_256);

	#ifdef DF_TEST_USING_HIGH_LEVEL_API

		FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file( flash_index, "VerySillyTestCase", 0x00000003, pMem, size );
		// The prior call to write the file only queues the request to the flash file system task. The actual work will get done subsequently. And will try
		// to take the mutex. Therefore we must as you see NOT have taken the mutex.
		vTaskDelay( 500 );

	#else

		memset( &tmp_dir,  0x00,  sizeof(DF_DIR_ENTRY_s) );
		snprintf( tmp_dir.Filename_c, sizeof(tmp_dir.Filename_c), "VerySillyTestCase" );
		tmp_dir.ItemSize_ul = size;
		tmp_dir.version_ul = 0x00000003;
		tmp_dir.edit_count_ul = 1;
		rstatus = DfWriteFileToDF_NM( flash_index, &tmp_dir, (unsigned char *)pMem );
	
		snprintf( (char*)str_256, sizeof(str_256), "Chip %d, Write of item 2 (size = %lu) returned status = %s\n\r", flash_index, size, DfStatusToStr(rstatus) );
		term_dat_out( str_256);

	#endif

	Display_free_space( flash_index );

	///////////////////////////////////////////////////////
	//
	// Create and write a third data item to DF
	//
	///////////////////////////////////////////////////////
	// Clear out our previously (dynamically) allocated buffer
	memset( pMem, 0, DF_FILE1_SIZE );
	// Copy a silly meaningless string into the buffer
	pString = "A very short string.\n\r";
	strcpy( (char*)pMem, pString );
	size = strlen( pString );

	snprintf( (char*)str_256, sizeof(str_256), "Chip %d, Now writing item #3 to DF...\n\r", flash_index );
	term_dat_out( str_256);

	#ifdef DF_TEST_USING_HIGH_LEVEL_API

		FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file( flash_index, "VerySillyTestCase", 0x00000003, pMem, size );
		// The prior call to write the file only queues the request to the flash file system task. The actual work will get done subsequently. And will try
		// to take the mutex. Therefore we must as you see NOT have taken the mutex.
		vTaskDelay( 500 );

	#else

		memset( &tmp_dir,  0x00,  sizeof(DF_DIR_ENTRY_s) );
		snprintf( tmp_dir.Filename_c, sizeof(tmp_dir.Filename_c), "VerySillyTestCase" );
		tmp_dir.ItemSize_ul = size;
		tmp_dir.version_ul = 0x00000003;
		tmp_dir.edit_count_ul = 2;
		rstatus = DfWriteFileToDF_NM( flash_index, &tmp_dir, (unsigned char *)pMem );
	
		snprintf( (char*)str_256, sizeof(str_256), "Chip %d: Write of item 3 (size = %lu) returned status = %s\n\r", flash_index, size, DfStatusToStr(rstatus));
		term_dat_out( str_256);

	#endif

	Display_free_space( flash_index );

	/////////////////////////////////////////////////////////////
	//
	//  Next we will list all directory entries
	//
	/////////////////////////////////////////////////////////////

	// Allocate two directory entry structures and zero them out
	pDirEntry1 = mem_malloc(sizeof(DF_DIR_ENTRY_s));
	if (pDirEntry1) memset(pDirEntry1, 0, sizeof(DF_DIR_ENTRY_s)); else for(;;);

	pDirEntry2 = mem_malloc(sizeof(DF_DIR_ENTRY_s));
	if (pDirEntry2) memset(pDirEntry2, 0, sizeof(DF_DIR_ENTRY_s)); else for(;;);


	Display_all_directory_entries( flash_index );







	#ifdef DF_TEST_USING_HIGH_LEVEL_API
	
		// Now find the files that should exist. Should only be two of them. The 6000 byte one
		// and the 22 byte small string one. First the 6000 byte one.

		memset( pMem, 0x00, DF_FILE1_SIZE );
	
		snprintf( (char*)str_256, sizeof(str_256), "\r\n\n Chip: %d, Reading VerySillyTestCase with short string.\r\n", flash_index );
		term_dat_out( str_256);

		FLASH_STORAGE_find_or_create_variable_file( flash_index, "VerySillyTestCase", 0x03, pMem, 22, NULL );
		
		if( (strncmp( (const char*)pMem, (const char*)"A very short string.\n\r", 22 )!= 0) )
		{
			snprintf( (char*)str_256, sizeof(str_256), "\r\n\n Chip: %d, VerySillyTestCase : FAILED!\r\n", flash_index );
			term_dat_out( str_256);
		}
		else
		{
			snprintf( (char*)str_256, sizeof(str_256), "\r\n\n Chip: %d, VerySillyTestCase : SUCCESS!\r\n", flash_index );
			term_dat_out( str_256);
		}
		

		memset( pMem, 0x00, DF_FILE1_SIZE );

		snprintf( (char*)str_256, sizeof(str_256), "\r\n\n Chip: %d, Reading QuiteBogusTestCase 6000 byte file and test array contents.\r\n", flash_index );
		term_dat_out( str_256);

		FLASH_STORAGE_find_or_create_variable_file( flash_index, "QuiteBogusTestCase", 0x43, pMem, DF_FILE1_SIZE, NULL );

		results = TRUE;
		
		for( index = 0; index < DF_FILE1_SIZE/sizeof(unsigned long); index++ )
		{
			if( *(pMem + index) != index )
			{
				results = FALSE;

				snprintf( (char*)str_256, sizeof(str_256), "\r\n\n Chip: %d, QuiteBogusTestCase : FAILED!\r\n", flash_index );
				term_dat_out( str_256);

				break;
			}
		}

		if( results == TRUE )
		{
			snprintf( (char*)str_256, sizeof(str_256), "\r\n\n Chip: %d, QuiteBogusTestCase : SUCCESS!\r\n", flash_index );
			term_dat_out( str_256);
		}

	#else

		//  THE FOLLOWING IS FOR WHEN NOT USING THE HIGH LEVEL API.
		//  Now we're going to look for specific items in the directory. We want
		//  to find items with ItemId_ul = 0x00000011 and Filename = "VerySillyTestCase"
		//  but don't care about the VersionId_ul or other fields.
		//
	
		//
		// Inititialize the first dir entry structure with our search criteria:
		// a) ItemId_us = 0x00000011;
		// b) Filename  = "VerySillyTestCase"
		//
		// The remaining fields of *pDirEntry1 are left uninitialized since
		// we aren't including them in the search criteria.
		//
		memset(pDirEntry1, 0, sizeof(DF_DIR_ENTRY_s));      // A good idea
	
		strcpy(pDirEntry1->Filename_c, "VerySillyTestCase");
		pDirEntry1->version_ul = 3;
	
		// Explain what we're doing next.
		term_dat_out( (UNS_8*)"Now we will look for specified directory entries...\r\n\n");
	
		////////////////////////////////////////////////////////////////////////////////
		//
		//  Based upon our search criteria, the DfFindDirEntry function should return
		//  "hits" for two directory entries: those that both have the item ID and
		//  Filename called out above.
		//
		////////////////////////////////////////////////////////////////////////////////
		index = 0;
		while( TRUE )
		{
			rstatus = DfFindDirEntry_NM( flash_index,
										 pDirEntry1, pDirEntry2,
										 (DF_DIR_ENTRY_KEY_FILENAME | DF_DIR_ENTRY_KEY_VERSION),
										 index);                           // dir index
	
			if( DF_DIR_ENTRY_NOT_FOUND != rstatus )
			{
				// A matching directory entry was found. *pDirEntry2 should be filled in with
				// the complete directory entry structure for the item now.
	
				// Display the directory entry "hits"
				term_dat_out( (UNS_8*)"...hit...\r\n" );
				DisplayDirEntry( pDirEntry2 );
	
				index = rstatus + 1;        // To resume search at the next entry
			}
			else
			{
				break;
			}
		}
	
	
		//  Now let's find and read the FIRST item we wrote. Since we know the key
		//  fields, we can try to read it directly.
		// Explain what we're doing next..
		term_dat_out( (UNS_8*)"\n\n\n\rWe will now read the FIRST file from the DF...\r\n" );
	
		memset( pDirEntry1, 0, sizeof(DF_DIR_ENTRY_s) );        // A good idea
	
		// Initialize the key fields to find
		strcpy(pDirEntry1->Filename_c, "QuiteBogusTestCase");	// Filename
		pDirEntry1->version_ul = 0x00000043;					// VersionID
		pDirEntry1->edit_count_ul = 0x00000001;					// ItemID
	
		index = 0;
		rstatus = DfFindDirEntry_NM( flash_index, pDirEntry1, pDirEntry2, (DF_DIR_ENTRY_KEY_FILENAME | DF_DIR_ENTRY_KEY_VERSION | DF_DIR_ENTRY_KEY_EDIT_COUNT), index);
	
		if( rstatus != DF_DIR_ENTRY_NOT_FOUND )
		{
			// A matching directory entry was found. *pDirEntry2 should be filled in with
			// the complete directory entry structure for the item now.
	
			// Display the directory entry "hits"
			term_dat_out( (UNS_8*)"...found it...\r\n" );
			DisplayDirEntry( pDirEntry2 );
	
			// Clear out our previously allocated buffer. We'll read the data into here.
			memset( pMem, 0, DF_FILE1_SIZE );
	
	
			// Make the call
			rstatus = DfReadFileFromDF_NM( flash_index, pDirEntry2, (unsigned char *)pMem );
	
			// Display the result
			snprintf( (char*)str_256, sizeof(str_256), "Return status from DfReadDataFromDF (item 1) is %s.\n\r", DfStatusToStr(rstatus) );
			term_dat_out( str_256 );
		}
		else
		{
			term_dat_out( (UNS_8*)"\n\rFAILURE : COULDN'T FIND ENTRY!\n\r" );
		}
	
	
		//  Now let's find and read the THIRD item we wrote. Since we know the key
		//  fields, we can try to read it directly.
		// Explain what we're doing next..
		term_dat_out( (UNS_8*)"\n\n\n\rWe will now read the THIRD file from the DF...\r\n" );
		memset( pDirEntry1, 0, sizeof(DF_DIR_ENTRY_s) );        // A good idea
	
		// Initialize the key fields to find
		strcpy(pDirEntry1->Filename_c, "VerySillyTestCase");	// Filename
		pDirEntry1->version_ul = 0x00000003;					// VersionID
		pDirEntry1->edit_count_ul = 0x00000002;					// ItemID
	
		index = 0;
		rstatus = DfFindDirEntry_NM( flash_index, pDirEntry1, pDirEntry2, (DF_DIR_ENTRY_KEY_FILENAME | DF_DIR_ENTRY_KEY_VERSION | DF_DIR_ENTRY_KEY_EDIT_COUNT), index);
	
		if( rstatus != DF_DIR_ENTRY_NOT_FOUND )
		{
			// A matching directory entry was found. *pDirEntry2 should be filled in with
			// the complete directory entry structure for the item now.
	
			// Display the directory entry "hits"
			term_dat_out( (UNS_8*)"...found it...\r\n" );
			DisplayDirEntry( pDirEntry2 );
	
			// Clear out our previously allocated buffer. We'll read the data into here.
			memset( pMem, 0, DF_FILE1_SIZE );
	
			// Make the call
			rstatus = DfReadFileFromDF_NM( flash_index, pDirEntry2, (unsigned char *)pMem );
	
			// Display the result
			snprintf( (char*)str_256, sizeof(str_256), "Return status from DfReadDataFromDF (item 1) is %s.\n\r", DfStatusToStr(rstatus) );
			term_dat_out( str_256 );
			term_dat_out( (UNS_8*)pMem );
		}
		else
		{
			term_dat_out( (UNS_8*)"\n\rFAILURE : COULDN'T FIND ENTRY!\n\r" );
		}
	
	
		//  Now let's find and read the SECOND item we wrote. Since we know the key
		//  fields, we can try to read it directly.
		// Explain what we're doing next..
		term_dat_out( (UNS_8*)"\n\n\n\rWe will now read the SECOND file from the DF...\r\n" );
		memset( pDirEntry1, 0, sizeof(DF_DIR_ENTRY_s) );        // A good idea
	
		// Initialize the key fields to find
		strcpy(pDirEntry1->Filename_c, "VerySillyTestCase");	// Filename
		pDirEntry1->version_ul = 0x00000003;					// VersionID
		pDirEntry1->edit_count_ul = 0x00000001;					// ItemID
	
		index = 0;
		rstatus = DfFindDirEntry_NM( flash_index, pDirEntry1, pDirEntry2, (DF_DIR_ENTRY_KEY_FILENAME | DF_DIR_ENTRY_KEY_VERSION | DF_DIR_ENTRY_KEY_EDIT_COUNT), index);
	
		if( rstatus != DF_DIR_ENTRY_NOT_FOUND )
		{
			// A matching directory entry was found. *pDirEntry2 should be filled in with
			// the complete directory entry structure for the item now.
	
			// Display the directory entry "hits"
			term_dat_out( (UNS_8*)"...found it...\r\n" );
			DisplayDirEntry( pDirEntry2 );
	
			// Clear out our previously allocated buffer. We'll read the data into here.
			memset( pMem, 0, DF_FILE1_SIZE );
	
			// Make the call
			rstatus = DfReadFileFromDF_NM( flash_index, pDirEntry2, (unsigned char *)pMem );
	
			// Display the result
			snprintf( (char*)str_256, sizeof(str_256), "Return status from DfReadDataFromDF (item 1) is %s.\n\r", DfStatusToStr(rstatus) );
			term_dat_out( str_256 );
			term_dat_out( (UNS_8*)pMem );
		}
		else
		{
			term_dat_out( (UNS_8*)"\n\rFAILURE : COULDN'T FIND ENTRY!\n\r" );
		}
	
	
	
		Display_free_space( flash_index );

		Display_all_directory_entries( flash_index );
	
	
		//--------------------------------------
	
		// Now let's delete the second item we created
		// The delete function will delete all files that match in file name but do not match in version or edit_count. So to preserve a file provide that information.
		// Give up access to storage manager..
		DfGiveStorageMgrMutex( flash_index );
		__find_older_files_and_delete( flash_index, "VerySillyTestCase", 3, 2 );
	
		if( pdTRUE == DfTakeStorageMgrMutex( flash_index, 100 ))
		{
			term_dat_out( (UNS_8*)"Access was gained on first call.\n\r");
		}
		else
		{
			term_dat_out( (UNS_8*)"Access was denied on first call! Test aborted!\n\r");
			for(;;);  // HANG
		}
		
		Display_free_space( flash_index );

		Display_all_directory_entries( flash_index );
	
		//--------------------------------------
	
		// Now let's delete the second item we created
		// The delete function will delete all files that match in file name but do not match in version or edit_count. So to preserve a file provide that information.
		// Give up access to storage manager..
		DfGiveStorageMgrMutex( flash_index );
		__find_older_files_and_delete( flash_index, "QuiteBogusTestCase", 0, 0 );
	
		if( pdTRUE == DfTakeStorageMgrMutex( flash_index, 100 ))
		{
			term_dat_out( (UNS_8*)"Access was gained on first call.\n\r");
		}
		else
		{
			term_dat_out( (UNS_8*)"Access was denied on first call! Test aborted!\n\r");
			for(;;);  // HANG
		}
	
		Display_free_space( flash_index );

		Display_all_directory_entries( flash_index );

		//--------------------------------------
	
		// Now let's delete the second item we created
		// The delete function will delete all files that match in file name but do not match in version or edit_count. So to preserve a file provide that information.
		// Give up access to storage manager..
		DfGiveStorageMgrMutex( flash_index );
		__find_older_files_and_delete( flash_index, "VerySillyTestCase", 0, 0 );
	
		if( pdTRUE == DfTakeStorageMgrMutex( flash_index, 100 ))
		{
			term_dat_out( (UNS_8*)"Access was gained on first call.\n\r");
		}
		else
		{
			term_dat_out( (UNS_8*)"Access was denied on first call! Test aborted!\n\r");
			for(;;);  // HANG
		}

		Display_free_space( flash_index );

		Display_all_directory_entries( flash_index );
	
		//--------------------------------------
	#endif

	// Free dynamically allocated memory..
	mem_free( pDirEntry1 );
	mem_free( pDirEntry2 );
	mem_free( pMem );

	#ifdef DF_TEST_USING_HIGH_LEVEL_API
		// nothing to do for this case
	#else
        // Give up access to storage manager..
		DfGiveStorageMgrMutex( flash_index );
	#endif

    term_dat_out( (UNS_8*)"End of DF Storage Manager Test\n\r");
}

#endif  // of file system test

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

