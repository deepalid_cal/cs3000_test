/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	<string.h>

#include	"chain_sync_vars.h"

#include	"app_startup.h"

#include	"station_groups.h"

#include	"poc.h"

#include	"moisture_sensors.h"

#include	"weather_control.h"

#include	"configuration_network.h"

#include	"lights.h"

#include	"walk_thru.h"

#include	"manual_programs.h"

#include	"irrigation_system.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


CHAIN_SYNC_CONTROL_STRUCTURE	cscs;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 11/6/2018 rmd : Here's a note about the order of the CRC checks. Right now it follows the
// FF_xxxxxxx define value. However that is probably NOT the optimal order due to the file
// dependencies. We probably should go in the order listed below, which comes from the
// building of the pdata message. That order is designed to deliver the files in order such
// that the dependencies are satisfied. SO HOW COME I DIDN'T IMPLEMENT? Well as it turns out
// you cannot firmly control the syncing order relative to the bad file encountered order.
// We could following the initial clean house sync, but for routine changes you never know
// which file we are testing relaitve to change being distributed. We could do that work,
// and enhance this array and the sync function set to support, but I've seen the syncing
// straighten itself out in the face of files errors, and everybody wants the job done.
// Changing the file sync order and synchronizing it to various activities would be perhaps
// another level of improvment.
//
// 11/6/2018 rmd : Suggested order (that we aren't yet doing)-
/*
FF_NETWORK_CONFIGURATION
FF_WEATHER_CONTROL
FF_IRRIGATION_SYSTEM
FF_LANDSCAPE_DETAILS
FF_MANUAL_PROGRAMS
FF_STATION_INFO
FF_POC
FF_LIGHTS
FF_MOISTURE_SENSORS
FF_WALK_THRU
*/
		
CHAIN_SYNC_FILE_PERTINANTS const chain_sync_file_pertinants[ FF_NUMBER_OF_FILES ] =
{
	// FF_CONTROLLER_CONFIGURATION - not included in the sync scheme
	{
		"",
		
		NULL,

		NULL,
		
		NULL
	},
	
	// FF_NETWORK_CONFIGURATION
	{
		"Network Config",
		
		NETWORK_CONFIG_calculate_chain_sync_crc,
		
		NETWORK_CONFIG_set_bits_on_all_settings_to_cause_distribution_in_the_next_token,
		
		NULL
	},
	
	// FF_SYSTEM_REPORT_DATA - not included in the sync scheme
	{
		"",
		
		NULL,

		NULL,
		
		NULL
	},
	
	// FF_POC_REPORT_DATA - not included in the sync scheme
	{
		"",
		
		NULL,

		NULL,
		
		NULL
	},
	
	// FF_STATION_REPORT_DATA - not included in the sync scheme
	{
		"",
		
		NULL,

		NULL,
		
		NULL
	},
	
	// FF_STATION_HISTORY - not included in the sync scheme
	{
		"",
		
		NULL,

		NULL,
		
		NULL
	},
	
	// FF_WEATHER_CONTROL
	{
		"Weather Control",
		
		WEATHER_CONTROL_calculate_chain_sync_crc,

		WEATHER_CONTROL_set_bits_on_all_settings_to_cause_distribution_in_the_next_token,
		
		NULL
	},
	
	// FF_WEATHER_TABLES - not included in the sync scheme
	{
		"",
		
		NULL,

		NULL,
		
		NULL
	},
	
	// FF_TPMICRO_DATA - not included in the sync scheme
	{
		"",
		
		NULL,

		NULL,
		
		NULL
	},
	
	// FF_STATION_INFO
	{
		"Stations",
		
		STATION_calculate_chain_sync_crc,

		STATION_set_bits_on_all_stations_to_cause_distribution_in_the_next_token,
		
		STATION_clean_house_processing
	},
	
	// FF_IRRIGATION_SYSTEM
	{
		"Mainline",
		
		IRRIGATION_SYSTEM_calculate_chain_sync_crc,
		
		IRRIGATION_SYSTEM_set_bits_on_all_systems_to_cause_distribution_in_the_next_token,
		
		SYSTEM_clean_house_processing
	},
	
	// FF_POC
	{
		"POC",
		
		POC_calculate_chain_sync_crc,

		POC_set_bits_on_all_pocs_to_cause_distribution_in_the_next_token,
		
		POC_clean_house_processing
		
	},
	
	// FF_MANUAL_PROGRAMS
	{
		"Manual Programs",
		
		MANUAL_PROGRAMS_calculate_chain_sync_crc,

		MANUAL_PROGRAMS_set_bits_on_all_groups_to_cause_distribution_in_the_next_token,
		
		MANUAL_PROGRAMS_clean_house_processing
	},
	
	// FF_LANDSCAPE_DETAILS
	{
		"Station Group",
		
		STATION_GROUPS_calculate_chain_sync_crc,
		
		STATION_GROUP_set_bits_on_all_groups_to_cause_distribution_in_the_next_token,
		
		STATION_GROUP_clean_house_processing
	},
	
	// FF_FLOWSENSE - not included in the sync scheme
	{
		"",
		
		NULL,

		NULL,
		
		NULL
	},
	
	// FF_CONTRAST_AND_VOLUME - not included in the sync scheme
	{
		"",
		
		NULL,

		NULL,
		
		NULL
	},
	
	// FF_LIGHTS
	{
		"Lights",
		
		LIGHTS_calculate_chain_sync_crc,
		
		LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token,
		
		LIGHTS_clean_house_processing
	},
	
	// FF_LIGHTS_REPORT_DATA - not included in the sync scheme
	{
		"",
		
		NULL,
		
		NULL,
		
		NULL
	},
	
	// FF_MOISTURE_SENSORS
	{
		"Moisture Sensor",
		
		MOISTURE_SENSOR_calculate_chain_sync_crc,
		
		MOISTURE_SENSOR_set_bits_on_all_moisture_sensors_to_cause_distribution_in_the_next_token,
		
		MOISTURE_SENSOR_clean_house_processing
	},
	
	// FF_BUDGET_REPORT_DATA - not included in the sync scheme
	{
		"",
		
		NULL,

		NULL,
		
		NULL
	},
	
	// FF_WALK_THRU
	{
		"Walk Thru",
		
		WALK_THRU_calculate_chain_sync_crc,

		WALK_THRU_set_bits_on_all_groups_to_cause_distribution_in_the_next_token,
		
		WALK_THRU_clean_house_processing
	}

};

/*
			();

			();
*/


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void init_chain_sync_vars( void )
{
	// 8/13/2018 rmd : Should be called during the boot startup, in the context of the
	// app_startup task. And IT MUST BE CALLED PRIOR TO ANY FILE init FUNCTIONS ARE CALLED.
	// Because it is those init functions that calculate the checksum, as opposed to this
	// function which 0's the checksums!
	
	// ----------
	
	xSemaphoreTakeRecursive( chain_sync_control_structure_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	cscs.ff_next_file_crc_to_send_0 = 0;

	memset( &cscs.clean_tokens_since_change_detected_or_file_save, 0x00, sizeof(cscs.clean_tokens_since_change_detected_or_file_save) );
	
	memset( &cscs.crc_is_valid, (false), sizeof(cscs.crc_is_valid) );
	
	memset( &cscs.the_crc, 0x00, sizeof(cscs.the_crc) );
	
	// ----------
	
	xSemaphoreGiveRecursive( chain_sync_control_structure_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

