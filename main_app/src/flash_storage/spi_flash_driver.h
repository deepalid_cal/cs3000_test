/*  file = spi_flash_driver.h                 03.17.2011  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_SPI_FLASH_DRIVER_H
#define INC_SPI_FLASH_DRIVER_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"lpc32xx_ssp.h"

#include	"df_storage_internals.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	UNS_32				write_protect;

	UNS_32				chip_select_bit;
	
	UNS_32				miso_mask;
	
	SSP_REGS_T			*ssp_base;
	
	// You cannot store in the actual ptr to the queue (semaphore) here. Because it is not yet defined at compile time.
	// So what we have is a pointer to the mutex ptr. And there for need to dereference it when using. Be careful.
	xQueueHandle		*task_queue_ptr;
	
	// You cannot store in the actual ptr to the queue (semaphore) here. Because it is not yet defined at compile time.
	// So what we have is a pointer to the mutex ptr. And there for need to dereference it when using. Be careful.
	xSemaphoreHandle	*flash_mutex_ptr;
	
	MASTER_DF_RECORD_s	*mr_struct_ptr;
	
	UNS_32				master_record_page;

	UNS_32				first_directory_page;

	UNS_32				last_directory_page;

	UNS_32				first_data_cluster;

	char				key_string[ DF_MAGIC_STR_LENGTH ];
	
	UNS_32				max_directory_entries;
	
} SSP_BASE_STRUCT;


extern const SSP_BASE_STRUCT	ssp_define[ 2 ];


extern MASTER_DF_RECORD_s		flash_0_mr_buffer, flash_1_mr_buffer;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#define		SPI_FLASH_SUCCESS	TRUE

#define		SPI_FLASH_FAILURE	FALSE


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

void init_FLASH_DRIVE_SSP_channel( UNS_32 flash_index );

UNS_32 SPI_FLASH_erase_4k_sector_and_write_up_to_4k_using_byte_programming( UNS_32 flash_index, UNS_32 paddress, DATA_HANDLE pdh );

UNS_32 SPI_FLASH_fast_read_as_much_as_needed_to_buffer( UNS_32 flash_index, UNS_32 paddress, DATA_HANDLE pdh );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

