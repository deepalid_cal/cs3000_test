#ifndef INC_CHAIN_SYNC_FUNCS_H
#define INC_CHAIN_SYNC_FUNCS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"lpc_types.h"

#include	"crc.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


extern void CHAIN_SYNC_inhibit_crc_inclusion_in_the_token_response( UNS_32 pff_file_name );

extern void CHAIN_SYNC_increment_clean_token_counts( void );


UNS_32 CHAIN_SYNC_push_data_onto_block( UNS_8 **pto_ptr, void *pfrom_ptr, UNS_32 plength );


extern UNS_32 CHAIN_SYNC_load_a_chain_sync_crc_into_the_token_response( UNS_8 **pchain_sync_ptr );


extern void CHAIN_SYNC_test_deliverd_crc( UNS_32 pextracted_ff_name, CRC_BASE_TYPE pextracted_crc, UNS_32 const pfrom_controller_index );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

