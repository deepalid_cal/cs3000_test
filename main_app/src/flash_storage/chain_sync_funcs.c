/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	<string.h>

#include	"chain_sync_funcs.h"

#include	"chain_sync_vars.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"cs_mem.h"

#include	"comm_mngr.h"



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void CHAIN_SYNC_inhibit_crc_inclusion_in_the_token_response( UNS_32 pff_file_name )
{
	// 8/13/2018 rmd : This function is called during the boot startup, in the context of the
	// app_startup task. It is also called when the master receives changes from a slave in the
	// token response. It is called when the master distributes changes to slaves in the token.
	// And it is called when slaves distribute their changes to the master. All of those token
	// related cases must be within the context of the COMM_MNGR task.
	
	// ----------
	
	xSemaphoreTakeRecursive( chain_sync_control_structure_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	if( pff_file_name < FF_NUMBER_OF_FILES )
	{
		// 11/6/2018 rmd : Commented out development line.
		//Alert_Message_va( "BLOCKING %s", chain_sync_file_pertinants[ pff_file_name ].file_name_string );

		cscs.clean_tokens_since_change_detected_or_file_save[ pff_file_name ] = 0;

		cscs.crc_is_valid[ pff_file_name ] = (false);
	}
	else
	{
		Alert_Message( "SYNC VARS: file # out of range" );
	}
	
	// ----------
	
	xSemaphoreGiveRecursive( chain_sync_control_structure_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
void CHAIN_SYNC_increment_clean_token_counts( void )
{
	// 11/2/2018 rmd : Called during the irri_comm processing of an incoming token.
	
	// ----------
	
	UNS_32	iii;
	
	// ----------
	
	xSemaphoreTakeRecursive( chain_sync_control_structure_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	// 8/30/2018 rmd : We've seen a clean token so give them all an opportunity to ratchet up
	// their count. When a change is seen only the count for the file associated with the change
	// is set to 0. Could have set them all to 0 I suppose, but went for the discrete approach
	// first.
	for( iii=0; iii<FF_NUMBER_OF_FILES; iii++ )
	{
		if( cscs.clean_tokens_since_change_detected_or_file_save[ iii ] < CHAIN_SYNC_NUMBER_OF_CLEAN_TOKENS_REQUIRED_SINCE_CHANGE_OR_FILE_SAVE )
		{
			cscs.clean_tokens_since_change_detected_or_file_save[ iii ] += 1;
		}
	}
	
	// ----------
	
	xSemaphoreGiveRecursive( chain_sync_control_structure_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
UNS_32 CHAIN_SYNC_push_data_onto_block( UNS_8 **pto_ptr, void *pfrom_ptr, UNS_32 plength )
{
	// 11/2/2018 rmd : So when adding a string to the crc we only include strlen number of
	// characters, well if the length is 0 let's not find out if memcpy can process that
	// properly.
	if( plength )
	{
		memcpy( *pto_ptr, pfrom_ptr, plength );
		
		*pto_ptr += plength;
	}
	
	// 11/2/2018 rmd : Always return the length added, even if 0.
	return( plength );
}

/* ---------------------------------------------------------- */
static void calculate_crc_and_set_if_crc_is_valid( UNS_32 const pff_name )
{
	// 8/30/2018 rmd : Executed in the context of the comm_mngr task when token responses are
	// being made. And that's it.

	// ----------

	// 8/30/2018 rmd : And only if the ff_name is within the allowed range.
	if( pff_name < FF_NUMBER_OF_FILES )
	{
		xSemaphoreTakeRecursive( chain_sync_control_structure_recursive_MUTEX, portMAX_DELAY );

		// ----------
		
		// 9/5/2018 rmd : Sanity check, should only be called if checksum is not valid.
		if( cscs.crc_is_valid[ pff_name ] )
		{
			Alert_Message_va( "CSCS: crc is valid? %s", chain_sync_file_pertinants[ pff_name ].file_name_string );
		}
		
		// ----------
		
		if( chain_sync_file_pertinants[ pff_name ].__crc_calculation_function_ptr != NULL )
		{
			// 9/5/2018 rmd : If the calculation is successful set the return value to (true).
			cscs.crc_is_valid[ pff_name ] = chain_sync_file_pertinants[ pff_name ].__crc_calculation_function_ptr( pff_name );

			if( cscs.crc_is_valid[ pff_name ] )
			{
				Alert_Message_va( "CSCS: calculated crc for %s", chain_sync_file_pertinants[ pff_name ].file_name_string );
			}
			else
			{
				Alert_Message_va( "CSCS: failed to calculate crc for %s", chain_sync_file_pertinants[ pff_name ].file_name_string );
			}
		}
		
		// ----------
		
		xSemaphoreGiveRecursive( chain_sync_control_structure_recursive_MUTEX );
	}
	else
	{
		// 8/30/2018 rmd : Should never see this o.o.r. alert.
		Alert_Message( "CSCS: ff_name oor" );
	}
}

/* ---------------------------------------------------------- */
UNS_32 CHAIN_SYNC_load_a_chain_sync_crc_into_the_token_response( UNS_8 **pchain_sync_ptr )
{
	// 8/30/2018 rmd : Executed in the context of the comm_mngr task when token responses are
	// being made. And that's it.

	// ----------

	UNS_32	rv;

	UNS_32	lsize_of_content;
	
	UNS_8	*lucp;
	
	// ----------
	
	rv = 0;
	
	*pchain_sync_ptr = NULL;
	
	// ----------
	
	xSemaphoreTakeRecursive( chain_sync_control_structure_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	// 9/4/2018 rmd : Only load into the token if the CRC calculation function pointer is not
	// NULL. That is our definition of if this structure participates in the crc chain_sync
	// scheme.
	if( chain_sync_file_pertinants[ cscs.ff_next_file_crc_to_send_0 ].__crc_calculation_function_ptr != NULL )
	{
		if( cscs.clean_tokens_since_change_detected_or_file_save[ cscs.ff_next_file_crc_to_send_0 ] >= CHAIN_SYNC_NUMBER_OF_CLEAN_TOKENS_REQUIRED_SINCE_CHANGE_OR_FILE_SAVE )
		{
			// 11/6/2018 rmd : Commented out development line.
			//Alert_Message_va( "INCLUDING %s", chain_sync_file_pertinants[ cscs.ff_next_file_crc_to_send_0 ].file_name_string );

			// 9/5/2018 rmd : If hasn't been calculated yet, make an attempt to do so.
			if( !cscs.crc_is_valid[ cscs.ff_next_file_crc_to_send_0 ] )
			{
				calculate_crc_and_set_if_crc_is_valid( cscs.ff_next_file_crc_to_send_0 );
			}
			
			// ----------
			
			// 9/5/2018 rmd : If the calculation was successful, include the checksum in the token
			// response.
			if( cscs.crc_is_valid[ cscs.ff_next_file_crc_to_send_0 ] )
			{
				// 8/13/2018 rmd : We send the file ID and the CRC.
				lsize_of_content = sizeof(UNS_32) + sizeof(CRC_BASE_TYPE);
		
				if( mem_obtain_a_block_if_available( lsize_of_content, (void**)pchain_sync_ptr ) )
				{
					lucp = *pchain_sync_ptr;
					
					memcpy( lucp, &cscs.ff_next_file_crc_to_send_0, sizeof(UNS_32) );
					lucp += sizeof( UNS_32 );
					rv += sizeof( UNS_32 );
					
					memcpy( lucp, &cscs.the_crc[ cscs.ff_next_file_crc_to_send_0 ], sizeof(CRC_BASE_TYPE) );
					lucp += sizeof( CRC_BASE_TYPE );
					rv += sizeof( CRC_BASE_TYPE );
				}
			}
		}
		else
		{
			// 11/6/2018 rmd : Commented out development line.
			//Alert_Message_va( "Not enough CLEAN for %s", chain_sync_file_pertinants[ cscs.ff_next_file_crc_to_send_0 ].file_name_string );
		}

	}  // of if participating  
		
	// ----------
	
	// 8/13/2018 rmd : Must always advance the index referring to the next checksum to try to
	// send. This function is called once per token response being built, so about every 20
	// tokens or so each strucutre has an opportunity send it's checksum.
	cscs.ff_next_file_crc_to_send_0 += 1;

	if( cscs.ff_next_file_crc_to_send_0 >= FF_NUMBER_OF_FILES )
	{
		cscs.ff_next_file_crc_to_send_0 = 0;
	}
	
	
	// ----------

	xSemaphoreGiveRecursive( chain_sync_control_structure_recursive_MUTEX );

	// ----------
		
	return( rv );
}

/* ---------------------------------------------------------- */
void CHAIN_SYNC_test_deliverd_crc( UNS_32 pextracted_ff_name, CRC_BASE_TYPE pextracted_crc, UNS_32 const pfrom_controller_index )
{

	//UNS_32 i;
	DATA_HANDLE		ldh;
	
	// 9/4/2018 rmd : This function executes within the context of the COMM_MNGR task - as part
	// of the token_resp processing. Think of it as executing on the FOAL side.
	
	// 11/2/2018 rmd : Range check the array index. If there are message extraction issues,
	// which result in an insane ff index, we can get a DATA ABORT error, so avoid that.
	if( pextracted_ff_name < FF_NUMBER_OF_FILES )
	{
		// 9/4/2018 rmd : Remember we are the master. Check to make sure we ourselves haven't made a
		// change, or received a change from the commserver. If there is any chance of a pdata
		// change pushing around the network, don't make this test. Eventually a change triggers the
		// file save, which updates the checksum, and we don't want to encroach upon the timing of
		// that file save and this test.
		if( cscs.clean_tokens_since_change_detected_or_file_save[ pextracted_ff_name ] >= CHAIN_SYNC_NUMBER_OF_CLEAN_TOKENS_REQUIRED_SINCE_CHANGE_OR_FILE_SAVE )
		{
			// 11/6/2018 rmd : Commented out development line.
			//Alert_Message_va( "CRC testing: %c %s", pfrom_controller_index + 'A', chain_sync_file_pertinants[ pextracted_ff_name ].file_name_string );

			// 9/5/2018 rmd : If hasn't been calculated yet, make an attempt to do so.
			if( !cscs.crc_is_valid[ pextracted_ff_name ] )
			{
				calculate_crc_and_set_if_crc_is_valid( pextracted_ff_name );
			}
			
			// ----------
			
			// 9/5/2018 rmd : If the calculation was successful, include the checksum in the token
			// response.
			if( cscs.crc_is_valid[ pextracted_ff_name ] )
			{
				// 9/4/2018 rmd : This is what the whole chain sync project is about! Do we have a match?
				if( pextracted_crc != cscs.the_crc[ pextracted_ff_name ] )
				{
					Alert_Message_va( "CHAIN SYNC: controller %c, %s crc error", pfrom_controller_index + 'A', chain_sync_file_pertinants[ pextracted_ff_name ].file_name_string );
					
					// ----------
					
					// 9/4/2018 rmd : Make sure a function has been specified.
					if( chain_sync_file_pertinants[ pextracted_ff_name ].__set_bits_on_all_groups_to_cause_distribution_in_the_next_token != NULL )
					{
						chain_sync_file_pertinants[ pextracted_ff_name ].__set_bits_on_all_groups_to_cause_distribution_in_the_next_token();
					}
					
					// ----------
					
					// 11/27/2018 dmd : The crc on the slaves and the master do not match, so we need to send a
					// special clean house request indicating to the slave to delete the file contents
					// associated with that file.  A new crc error clean house command
					// - "MID_FOAL_TO_IRRI__CRC_ERROR_CLEAN_HOUSE_REQUES" and a new state -
					// "COMM_MNGR_STATE_crc_cleaning_house", is introduced in the comm_mngr, which will send the
					// command and file index of the problem file to the slave controller on the chain showing a
					// CRC error. 
					
					// 12/10/2018 dmd : Check if the function is specified.
					if( chain_sync_file_pertinants[ pextracted_ff_name ].__clean_house_processing != NULL )
					{
						comm_mngr.is_a_NEW_member_in_the_chain[ pfrom_controller_index ] = (true);
					
						comm_mngr.state = COMM_MNGR_STATE_crc_cleaning_house;
					
						//next_contact.index = pfrom_controller_index;
						next_contact.command_to_use = MID_FOAL_TO_IRRI__CRC_ERROR_CLEAN_HOUSE_REQUEST;

						ldh.dlen = sizeof( COMM_COMMANDS ) + sizeof(UNS_32);
					
						ldh.dptr = mem_malloc ( ldh.dlen );
					
						memcpy( ldh.dptr, &next_contact.command_to_use, sizeof(COMM_COMMANDS) );
					 
						memcpy( (ldh.dptr + sizeof(COMM_COMMANDS)), &pextracted_ff_name, sizeof(pextracted_ff_name) );
					
						next_contact.message_handle = ldh;
					}
				
					// ----------
				
				}
			}
		}
		else
		{
			// 11/6/2018 rmd : Commented out development line.
			//Alert_Message_va( "CRC ignoring: count %d, %c %s", cscs.clean_tokens_since_change_detected_or_file_save[ pextracted_ff_name ], pfrom_controller_index + 'A', chain_sync_file_pertinants[ pextracted_ff_name ].file_name_string );
		}
	}
	else
	{
		Alert_Message( "CRC Test index error" );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

