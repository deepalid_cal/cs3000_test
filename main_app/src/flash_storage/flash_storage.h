/* file = flash_storage.h  10.12.2009  rmd  */
/* ---------------------------------------------------------- */

#ifndef INC_FLASH_STORAGE_H_
#define INC_FLASH_STORAGE_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cal_list.h"

#include	"df_storage_mngr.h"

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// These defines represent each of the TWO DIFFERENT flash chips in our LPC3250 design.

#ifndef _MSC_VER

	#define	FLASH_INDEX_0_BOOTCODE_AND_EXE	(0)

	#define	FLASH_INDEX_1_GENERAL_STORAGE	(1)

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/16/2012 rmd : The defines providing the ability to save a file at some point later in
// time. I forgot what the FF stands for. 'File' something I suppose. It's nice and
// recognizable so I'll leave it. IMPORTANT - these numbers must go in order and start from
// 0. No skipping or out of sequence. They must macth the timer id field as the timers are
// created.

// 11/2/2018 rmd : WARNING WARNING WARNING !!!!!!!!!!!!! do not redefine the values
// associated here. We could except apparently they are used in the pdata/commserver
// messaging, to associate a bit set in a pdata message to indicate particular
// file content, and I belive compiled into the commserver. See the PDATA_extract function
// suite.

// 4/16/2014 rmd : CLEAN ACTIVITY - none
#define		FF_CONTROLLER_CONFIGURATION		(0)

// 4/16/2014 rmd : CLEAN ACTIVITY - Discrete settings. Broadcast from master to all on
// chain. No action needed upon clean request. Setting overwritten when master distributes.
#define		FF_NETWORK_CONFIGURATION		(1)

// 4/16/2014 rmd : CLEAN ACTIVITY - delete all records.
#define		FF_SYSTEM_REPORT_DATA			(2)

// 4/16/2014 rmd : CLEAN ACTIVITY - delete all records.
#define		FF_POC_REPORT_DATA				(3)

// 4/16/2014 rmd : CLEAN ACTIVITY - delete all records.
#define		FF_STATION_REPORT_DATA			(4)

// 4/16/2014 rmd : CLEAN ACTIVITY - delete all records.
#define		FF_STATION_HISTORY				(5)

// 4/16/2014 rmd : CLEAN ACTIVITY - Discrete settings. Broadcast from master to all on
// chain. No action needed upon clean request. Setting overwritten when master distributes.
#define		FF_WEATHER_CONTROL				(6)

// 4/16/2014 rmd : CLEAN ACTIVITY - delete all records.
#define		FF_WEATHER_TABLES				(7)

// 4/16/2014 rmd : CLEAN ACTIVITY - ?
#define		FF_TPMICRO_DATA					(8)

// 4/16/2014 rmd : CLEAN ACTIVITY - delete all stations and re-create the default.
#define		FF_STATION_INFO					(9)

// 4/16/2014 rmd : CLEAN ACTIVITY - delete all stations and re-create the default.
#define		FF_IRRIGATION_SYSTEM			(10)

// 4/16/2014 rmd : CLEAN ACTIVITY - delete all stations and re-create the default.
#define		FF_POC							(11)

// 4/16/2014 rmd : CLEAN ACTIVITY - delete all stations and re-create the default.
#define		FF_MANUAL_PROGRAMS				(12)

// 4/16/2014 rmd : CLEAN ACTIVITY - delete all stations and re-create the default.
#define		FF_LANDSCAPE_DETAILS			(13)

// 4/16/2014 rmd : CLEAN ACTIVITY - none
#define		FF_FLOWSENSE					(14)

// 4/16/2014 rmd : CLEAN ACTIVITY - none
#define		FF_CONTRAST_AND_VOLUME			(15)

// 7/7/2015 mpd : CLEAN ACTIVITY - none
#define		FF_LIGHTS						(16)

// 7/21/2015 mpd : CLEAN ACTIVITY - delete all records.
#define		FF_LIGHTS_REPORT_DATA			(17)

// 4/16/2014 rmd : CLEAN ACTIVITY - delete all moisture sensors and re-create the default.
#define		FF_MOISTURE_SENSORS				(18)

// 02/22/2016 skc : CLEAN ACTIVITY - delete all records.
#define		FF_BUDGET_REPORT_DATA			(19)

#define		FF_WALK_THRU					(20)



// 2/3/2014 rmd : And that IS THE END of the list of everyday user settings files. That is
// sync'd down the chain. And have the potential for delayed writes. The code image files do
// not participate in such activities by design.

// 3/25/2014 rmd : NOTE - when you add a case here you must add a case to the FLASH_STORAGE
// ff_timer_callback function!

// 8/13/2018 rmd : NOTE - and add to the const array in the top of chain_sync_vars.c which
// includes the crc and bit setting functions.

// ----------


// This is always to be ONE more than the number of the last file name define. It sets the
// number of timers. And the size of the CRC array.
#define		FF_NUMBER_OF_FILES				(21)


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define		FLASH_STORAGE_seconds_to_delay_file_save_after_rolling_report_data	(2)

#define		FLASH_STORAGE_seconds_to_delay_file_save_after_sending_report_data	(2)

#define		FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change	(0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/13/2017 rmd : Flash task queue events.

#define	FLASH_TASK_EVENT_write_data_to_flash_file								(22)

#define	FLASH_TASK_EVENT_delete_older_versions									(33)

#define	FLASH_TASK_EVENT_delete_all_files_with_this_name						(44)

#define	FLASH_TASK_EVENT_return_code_image_version_stamp						(55)

#define	FLASH_TASK_EVENT_read_code_image_file_to_memory_for_tpmicro_isp_mode	(66)

#define	FLASH_TASK_EVENT_read_code_image_file_to_memory_for_code_transmit		(77)


typedef struct
{
	UNS_32			command;

	// We provide a pointer to the CONST filename. As opposed to the queue carrying a copy of
	// the name.
	const char 		*const_file_name_ptr;

	UNS_32			file_version;

	UNS_32			file_edit;

	void			*file_buffer_ptr;

	UNS_32			file_size;
	
	UNS_32			ff_name;
	
} FLASH_STORAGE_QUEUE_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


void FLASH_STORAGE_delete_all_files( UNS_32 flash_index );

void FLASH_STORAGE_delete_all_files_with_this_name(	UNS_32 flash_index, const char *pfile_name );


extern void FLASH_STORAGE_request_code_image_version_stamp( const char *pfile_name );


extern void FLASH_STORAGE_request_code_image_file_read( const char *pfile_name, const UNS_32 pcompletion_event );


// ----------

extern void FLASH_FILE_find_or_create_variable_file(	UNS_32				pflash_index,
														const char			*pfile_name,
														UNS_32				platest_revision,
														void				*pvariable_ptr,
														UNS_32				pfile_size,
														xSemaphoreHandle	pdata_recursive_mutex,
														void				(*pupdater_func_ptr)(UNS_32),
														void				(*pinitialization_func_ptr)(void),
														UNS_32				pff_name );
														
														
extern void FLASH_FILE_initialize_list_and_find_or_create_group_list_file(	const UNS_32			pflash_index,
					   														const char				*pfile_name,
					   														UNS_32					platest_revision,
					   														MIST_LIST_HDR_TYPE		*plist_hdr_ptr,
					   														UNS_32 const			*pitem_size_array,
					   														xSemaphoreHandle		plist_recursive_mutex,
					   														void					(*pupdater_func_ptr)(UNS_32),
					   														void					(*pinitialization_func_ptr)(void *const, const BOOL_32),
					   														char					*pdefault_label,
					   														UNS_32					pff_name );
																			
extern void FLASH_FILE_find_or_create_reports_file(	const UNS_32		pflash_index,
					   								const char			*pfile_name,
					   								UNS_32				platest_revision,

													void				*pvariable_ptr,

													UNS_32 const		*preport_record_size_array,
													UNS_32 const		*preport_record_count_array,
													UNS_32				pcomplete_structure_size,

													xSemaphoreHandle	preport_data_recursive_mutex,
													void				(*pupdater_func_ptr)(UNS_32),
													void				(*pinitialization_func_ptr)(void),

													UNS_32				pff_name );
// ----------

extern void FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file(	const UNS_32		pflash_index,
																	const char			*pfile_name,
																	UNS_32				pversion,
																	void				*pbuffer_ptr,
																	UNS_32				pfile_size,
																	xSemaphoreHandle	pdata_recursive_mutex,
																	UNS_32				pff_name );
																	

void FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file(	const UNS_32		pflash_index,
																const char			*pfile_name,
																UNS_32				pversion,
																MIST_LIST_HDR_TYPE	*plist_hdr_ptr,
																UNS_32				plist_item_size,
																xSemaphoreHandle	plist_recursive_mutex,
																UNS_32				pff_name );
																
// ----------

void FLASH_STORAGE_flash_storage_task( void *pvParameters );



extern void FLASH_STORAGE_create_delayed_file_save_timers( void );



extern void FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( UNS_32 const pff_filename, UNS_32 const pseconds_till_save );

extern void FLASH_STORAGE_if_not_running_start_file_save_timer( UNS_32 const pff_filename, UNS_32 pseconds_till_save );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /* _MSC_VER */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /* INC_FLASH_STORAGE_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

