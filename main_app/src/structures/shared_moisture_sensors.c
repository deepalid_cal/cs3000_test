/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_SHARED_MOISTURE_C
#define _INC_SHARED_MOISTURE_C

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs3000_tpmicro_common.h"

#include	"group_base_file.h"

#include	"bithacks.h"

#include	"change.h"

#include	"moisture_sensors.h"

#include	"shared.h"

#ifndef	_MSC_VER

	#include	"alerts.h"

	#include	"app_startup.h"
	
	#include	"cs_mem.h"

	#include	"battery_backed_vars.h"

#else

	#include	<stdlib.h>

	#include	"sql_merge.h"

#endif




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define MOISTURE_SENSOR_CHANGE_BITFIELD_name_bit							(0)
#define	MOISTURE_SENSOR_CHANGE_BITFIELD_box_index_bit						(1)
#define	MOISTURE_SENSOR_CHANGE_BITFIELD_decoder_serial_number_bit			(2)
#define	MOISTURE_SENSOR_CHANGE_BITFIELD_physically_available_bit			(3)
#define	MOISTURE_SENSOR_CHANGE_BITFIELD_in_use_bit							(4)
#define	MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_control_mode				(5)

#define	nlu_was_old_high_set_point_uns32_bit								(6)
#define	nlu_was_old_low_set_point_uns32_bit									(7)

#define	MOISTURE_SENSOR_CHANGE_BITFIELD_low_temp_point_bit					(8)
#define	MOISTURE_SENSOR_CHANGE_BITFIELD_additional_soak_seconds_bit			(9)

#define	MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_high_set_point_float_bit	(10)
#define	MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_low_set_point_float_bit	(11)

#define	MOISTURE_SENSOR_CHANGE_BITFIELD_high_temp_point_bit					(12)
#define	MOISTURE_SENSOR_CHANGE_BITFIELD_high_temp_action_bit				(13)
#define	MOISTURE_SENSOR_CHANGE_BITFIELD_low_temp_action_bit					(14)
#define	MOISTURE_SENSOR_CHANGE_BITFIELD_high_ec_point_bit					(15)
#define	MOISTURE_SENSOR_CHANGE_BITFIELD_low_ec_point_bit					(16)
#define	MOISTURE_SENSOR_CHANGE_BITFIELD_high_ec_action_bit					(17)
#define	MOISTURE_SENSOR_CHANGE_BITFIELD_low_ec_action_bit					(18)

// ----------

static char* const MOISTURE_SENSOR_database_field_names[ (MOISTURE_SENSOR_CHANGE_BITFIELD_low_ec_action_bit + 1) ] =
{
	"Name",
	"BoxIndex",
	"DecoderSerialNumber",
	"PhysicallyAvailable",
	"InUse",
	"MoistureControlMode",

	// 10/24/2018 rmd : Used to be old UNS_32 set points, no longer used
	"",
	"",

	"LowTempPoint",
	"AdditionalSoakSeconds",

	// 10/5/2018 rmd : As of this date, two NEW database field names.
	"MoistureHighSetPoint_float",
	"MoistureLowSetPoint_float",

	"HighTempPoint",
	"HighTempAction",
	"LowTempAction",

	"HighECPoint",
	"LowECPoint",
	"HighECAction",
	"LowECAction",
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
struct MOISTURE_SENSOR_GROUP_STRUCT
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	// ----------

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;
	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

	// ----------
	
	// 2/2/2016 rmd : The identifying parameters that identify a moisture sensor decoder. These
	// two are not directly user editable but indeed are sync'd (with the server and the other
	// boxes on the chain if any).
	UNS_32			box_index_0;

	UNS_32			decoder_serial_number;

	// ----------
	
	// 2/2/2016 rmd : This is set (true) when the user has, post discovery, indicated that he
	// wishes to make available for use this moisture decoder. The moisture sensor list item is
	// created and marked physically available and in_use. If a subsequent scan does NOT find
	// this decoder then we find him in the moisture sensor file list and mark this physically
	// available (false). A subsequent scan can mark this back (true) if the decoder is once
	// again found. This variable is sync'd (with the server and the other boxes on the chain if
	// any).
	BOOL_32			physically_available;

	// 2/2/2016 rmd : The user sets this to indicate he wishes to make this sensor available for
	// assignment to control a station group. This variable is sync'd (with the server and the
	// other boxes on the chain if any). NOTE - this variable is not actually used. Following
	// the discovery all DISCOVERED moisture sensors are 'added to/ensured to be in' the file
	// list and are physically available and in_use.
	BOOL_32			in_use;

	// ----------
	
	// 2/4/2016 rmd : Three modes allowed. HIGH THRESHOLD mode. LOW THRESHOLD mode. And both
	// meaning LOW to start and HIGH to stop irrigation. This variable is sync'd (with the
	// server and the other boxes on the chain if any). 
	//  
	// 10/15/2018 ajv : Two more modes were introduced: DO NOTHING and ALERT ONLY. These modes
	// provide users the ability to monitor the sensor before using it to drive irrigation. 
	//
	// 10/15/2018 ajv : This variable is currently NOT USED. This introduces a risk if/when it
	// becomes user-editable because the developer implementing additional modes may not realize
	// this oversight and cause irrigation to stop regardless of what mode the user selects. 
	// In fact, this oversight has prevented us from implementing the two new modes for the time
	// being.
	UNS_32			moisture_control_mode;
	

	// 2/29/2016 rmd : This value reflects a percentage called volumetric water content (VWC).
	// With our sensor completely submerged the percentage we calculate apparently comes in at
	// approx 75%. So I would imagine the user set point with the sensor in the ground is going
	// to be something like 35%. We shall see. This variable is sync'd (with the server and the
	// other boxes on the chain if any).
	//
	// 10/5/2018 rmd : In file Revision 3 these were rendered obsolete. Replaced with floating
	// point versions you'll see later in this structure. THEY ARE REFERENCED IN THE UPDATER
	// FUNCTION SO LEAVE VARIABLES IN PLACE IN THE STRUCTURE. Theoretically someday they can be
	// removed, if one believes all code has been updated, but i don't know how you know that.
	UNS_32			nlu_moisture_high_set_point_uns32;

	UNS_32			nlu_moisture_low_set_point_uns32;

	// ----------
	
	// 2/2/2016 rmd : This variable is sync'd (with the server and the other boxes on the chain
	// if any).
	UNS_32			low_temp_point;
	
	// ----------
	
	// 2/2/2016 rmd : We allow multiple station groups to use the same sensor. This is because
	// station groups are formed based upon other criteria than simply which sensor controls the
	// station in it. The consequence however is 1) we assume all station groups that use the
	// sensor have the same start time and 2) that at least one of the stations in the group or
	// groups irrigates the sensor. But we don't know which one. So to be sure to allow water
	// time to soak in after the last station is irrigated (keeping cycles balanced) before we
	// start the next cycle, we force an additional 20 minutes soak-in time. The present plan is
	// this variable is not exposed to the user. It will however be sync'd (with the server and
	// the other boxes on the chain if any).
	//
	// 10/15/2018 ajv : This variable is currently NOT USED. If the comment above is still
	// valid, we should consider implementing this at some point in the future, whether it's
	// user-editable or not.
	UNS_32			additional_soak_seconds;
	
	// ----------
	
	// 2/18/2016 rmd : After power up at the 75 second mark the TPMicro obtains the latest
	// moisture/temp/EC readings from the decoder and then goes about it's business of handing
	// it up to the CS3000. As received from the TPMicro the reading is stored in our raw
	// reading variable below. Once received at the MASTER we pick the reading apart into its
	// components and perform any conversion math needed. And we set the time stamp so we know
	// the age of the reading.
	
	// 2/23/2016 rmd : These are the variables involved as the reading is received from the
	// TPMicro. The reading is then to be included in the next token response. Are to be
	// considered SLAVE side variables. NOT SYNC'D TO SERVER.
	MOISTURE_SENSOR_DECODER_RESPONSE	reading_string_storage__slave;
	
	// 2/23/2016 rmd : A SLAVE side variable. NOT SYNC'D TO SERVER.
	BOOL_32								reading_string_send_to_master__slave;
	
	// ----------
	
	// 2/23/2016 rmd : These are the latest readings as received from the slave. The range on
	// the readings typically produced is from 0.0 to about 80.0. And is truly a decimal number
	// meaning will show 41.2% and such. Is to be consider a MASTER side variable.
	//
	// 10/5/2018 rmd : In file Rev 3 we changed the scaling on this variable to be between 0 and
	// 1.0 (roughly speaking - as AJ and Ryan report values can be negative).
	// NOT SYNC'D TO SERVER.
	float		latest_moisture_vwc_percentage;

	// 3/21/2016 rmd : Note is a SIGNED INT as temperature can go negative. Is to be consider a
	// MASTER side variable. NOT SYNC'D TO SERVER.
	INT_32		latest_temperature_fahrenheit;

	// 3/29/2016 rmd : Has a range of 0 through 72,000. Is to be consider a MASTER side
	// variable. NOT SYNC'D TO SERVER.
	//
	// 8/17/2018 Ryan : Renamed from micro_siemen_per_cm to
	// conductivity_reading_deci_siemen_per_m to reflect the new units being used. Also changed
	// from UNS_32 to float.
	float		latest_conductivity_reading_deci_siemen_per_m;
	
	// 2/18/2016 rmd : This is set (false) during code start. And is a flag in case the
	// application wants to use the latest reading before it has arrived. REMEMBER - as the
	// latest readings arrive from the sensor and written to the list item the file IS NOT
	// SAVED. Therefore upon application startup when the file is read out the latest readings
	// are not valid until a new reading arrives from the TPMicro. Which should happen within 2
	// minutes from startup (given both the TPMicro and Main Board applications restarted). This
	// is a MASTER side variable. NOT SYNC'D TO SERVER.
	BOOL_32		latest_reading_valid;
	
	// 2/23/2016 rmd : This flag works in addition to the latest_reading_valid. The latest
	// reading valid flag is really only used till the first reading arrives. When that is true
	// we also demand the reading be not more than 20 minutes old. The logic being if something
	// is wrong with the sensor let the TPMicro try twice [TPMicro reads the sensor decoder once
	// every 10 minutes]. This is a MASTER side variable. NOT SYNC'D TO SERVER.
	UNS_32		latest_reading_time_stamp;
	
	// 3/29/2016 rmd : This is just a character value that we assign to this 32-bit variable.
	// Takes on the value 'x' or 'z'. NOT SYNC'D TO SERVER. Not saved to the file when updated.
	UNS_32		detected_sensor_model;
	
	// ----------
	
	// 5/23/2018 rmd : The was an unused in rev 1. Renamed in rev 2 to use as a protection
	// variable so irrigation doesn't forever stall during some unforseen malfunction with the
	// 2W network or the moisture decoder itself. Should be zeroed upon code startup, and at a
	// start time, and allow say 5 minutes to get a valid and recent reading, before allowing
	// another cycle to irrigate. After allowing a new cycle do not need to again zero this, as
	// all station need to reach the required cycle count before the next use of this variable.
	UNS_32		invalid_or_old_reading_seconds;

	BOOL_32		unused_b;
	
	// ----------
	
	// 4/8/2016 rmd : Added in Rev 2

	// 5/10/2018 rmd : A variable to support the moisture sensing logic during irrigation. Set
	// to 0 at the start time. Each second as part of the maintain function we check if a
	// moisture sensor reading has crossed the set point. If it has, this is set (true). NOT
	// SYNC'D DOWN THE CHAIN OR TO COMMSERVER. BUT SHOULD BE SAVED WHEN SET TO TRUE OR FALSE
	// BECAUSE IT CONTROLS IRRIGATION AND SHOULD BE PRESERVED THROUGH A POWER FAILURE.
	BOOL_32			moisture_sensor_control__saved__has_crossed_the_set_point;
	

	// 5/10/2018 rmd : A variable to support the moisture sensing logic during irrigation. Set
	// to 0 at the start time. And, when a station turns ON, if this variable is less than the
	// station history rip cycle count, is incremented. Used as part of the list removal
	// criteria if the sensor reading has crossed the set point is TRUE. NOT SYNC'D DOWN THE
	// CHAIN OR TO COMMSERVER. BUT SHOULD BE SAVED WHEN SET TO TRUE OR FALSE BECAUSE IT CONTROLS
	// IRRIGATION AND SHOULD BE PRESERVED THROUGH A POWER FAILURE.
	UNS_32			moisture_sensor_control__saved__required_cycles_before_removal;


	// 5/17/2018 rmd : This one initialized to (false) each second. Indicates this sensor is not
	// being used to control irrigation right this moment. This variable is dynamic, and not
	// useful outside of the foal irri maintain function. As it changes no file save is
	// necessary.
	BOOL_32 		moisture_sensor_control__transient__has_actively_irrigating_stations;

	// 5/10/2018 rmd : This variable is dynamic, and not useful outside of the foal irri
	// maintain function. As it changes no file save is necessary.
	BOOL_32 		moisture_sensor_control__transient__all_stations_are_off_and_soaking_completed;

	// 5/10/2018 rmd : This variable is dynamic, and not useful outside of the foal irri
	// maintain function. As it changes no file save is necessary.
	BOOL_32 		moisture_sensor_control__transient__all_stations_have_reached_the_required_cycles;
	
	// ----------
	
	// 2/29/2016 rmd : This value reflects a percentage called volumetric water content (VWC).
	// With our sensor completely submerged the percentage we calculate apparently comes in at
	// approx 75%. So I would imagine the user set point with the sensor in the ground is going
	// to be something like 35%. We shall see. This variable is sync'd (with the server and the
	// other boxes on the chain if any).
	//
	// 10/5/2018 rmd : This variable was ADDED in Rev 3, to replace the existing (now nlu_)
	// UNS_32 set point variables. AJ rescaled the reading and set point to a 0 to 1, fractional
	// scale. This was done to better fit the industry (i just followed directions)? The default
	// is 0.32.
	float			moisture_high_set_point_float;

	float			moisture_low_set_point_float;

	// ----------

	// 7/11/2018 ajv : We already had a low temp point, now adding a high temp point as well. 
	// This allows users to be alerted (or potentially take some action down the line) if/when 
	// the threshold is crossed. The high point is specifically being added to support 
	// aquaponics installations. 
	INT_32			high_temp_point;

	// 7/11/2018 ajv : If the high or low temperature threshold is crossed, the user has the 
	// ability to do something. Currently, the two modes are "do nothing" and "alert me". In the 
	// future, we may add other modes. 
	UNS_32			high_temp_action;
	UNS_32			low_temp_action;

	// ----------

	// 7/11/2018 ajv : Allow the user to specify upper and lower EC value thresholds for 
	// alerting purposes. 
	float			high_ec_point;
	float			low_ec_point;

	// 7/11/2018 ajv : If the high or low electrical conductivity threshold is crossed, the user
	// has the ability to do something. Currently, the two modes are "do nothing" and "alert 
	// me". In the future, we may add other modes. 
	UNS_32			high_ec_action;
	UNS_32			low_ec_action;

	// ----------

	// 10/8/2018 ajv : If a threshold is crossed, we will either alert the user or take some 
	// action. We don't want to continually alert the user, however, when the threshold has been 
	// crossed. Therefore, set a flag when the threshold is crossed and clear it when it returns 
	// to a valid range. THESE ARE NOT SYNCED DOWN THE CHAIN! 
	BOOL_32			moisture_threshold_crossed;

	BOOL_32			temperature_threshold_crossed;

	BOOL_32			conductivity_threshold_crossed;
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_set_name(	MOISTURE_SENSOR_GROUP_STRUCT *const pmois,
											char *pname,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 preason_for_change,
											const UNS_32 pinitiator_box_index_0,
											const BOOL_32 pset_change_bits,
											CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											#ifdef _MSC_VER
											, char *pmerge_update_str,
											char *pmerge_insert_fields_str,
											char *pmerge_insert_values_str
											#endif
										)
{
	SHARED_set_name_32_bit_change_bits( pmois,
										pname,
										pgenerate_change_line_bool,
										preason_for_change,
										pinitiator_box_index_0,
										pset_change_bits,
										pchange_bitfield_to_set,
										&pmois->changes_to_upload_to_comm_server,
										MOISTURE_SENSOR_CHANGE_BITFIELD_name_bit
										#ifdef _MSC_VER
										, MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_name_bit ],
										pmerge_update_str,
										pmerge_insert_fields_str,
										pmerge_insert_values_str
										#endif
									  );
}

/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_set_box_index(	void *const pmois,
												UNS_32 pnew_box_index,
												const BOOL_32 pgenerate_change_line_bool,
												const UNS_32 preason_for_change,
												const UNS_32 pinitiator_box_index_0,
												const BOOL_32 pset_change_bits,
												CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												#ifdef _MSC_VER
												, char *pmerge_update_str,
												char *pmerge_insert_fields_str,
												char *pmerge_insert_values_str
												#endif
											)
{
	SHARED_set_uint32_with_32_bit_change_bits_group(	pmois,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->box_index_0),
														pnew_box_index,
														BOX_INDEX_MINIMUM,
														BOX_INDEX_MAXIMUM,
														BOX_INDEX_DEFAULT,
														pgenerate_change_line_bool,
														CHANGE_MOISTURE_SENSOR_BOX_INDEX,
														preason_for_change,
														pinitiator_box_index_0,
														pset_change_bits,
														pchange_bitfield_to_set,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->changes_to_upload_to_comm_server),
														MOISTURE_SENSOR_CHANGE_BITFIELD_box_index_bit,
														MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_box_index_bit ]
														#ifdef _MSC_VER
														, pmerge_update_str,
														pmerge_insert_fields_str,
														pmerge_insert_values_str
														#endif
													);
}

/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_set_decoder_serial_number(	void *const pmois,
															UNS_32 pdecoder_serial_number,
															const BOOL_32 pgenerate_change_line_bool,
															const UNS_32 preason_for_change,
															const UNS_32 pinitiator_box_index_0,
															const BOOL_32 pset_change_bits,
															CHANGE_BITS_32_BIT *pchange_bitfield_to_set
															#ifdef _MSC_VER
															, char *pmerge_update_str,
															char *pmerge_insert_fields_str,
															char *pmerge_insert_values_str
															#endif
														)
{
	SHARED_set_uint32_with_32_bit_change_bits_group(	pmois,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->decoder_serial_number),
														pdecoder_serial_number,
														DECODER_SERIAL_NUM_MIN,
														DECODER_SERIAL_NUM_MAX,
														DECODER_SERIAL_NUM_DEFAULT,
														pgenerate_change_line_bool,
														CHANGE_MOISTURE_SENSOR_DECODER_SERIAL_NUMBER,
														preason_for_change,
														pinitiator_box_index_0,
														pset_change_bits,
														pchange_bitfield_to_set,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->changes_to_upload_to_comm_server),
														MOISTURE_SENSOR_CHANGE_BITFIELD_decoder_serial_number_bit,
														MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_decoder_serial_number_bit ]
														#ifdef _MSC_VER
														, pmerge_update_str,
														pmerge_insert_fields_str,
														pmerge_insert_values_str
														#endif
													);
}

/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_set_physically_available(	void *const pmois,
															BOOL_32 pphysically_available,
															const BOOL_32 pgenerate_change_line_bool,
															const UNS_32 preason_for_change,
															const UNS_32 pinitiator_box_index_0,
															const BOOL_32 pset_change_bits,
															CHANGE_BITS_32_BIT *pchange_bitfield_to_set
															#ifdef _MSC_VER
															, char *pmerge_update_str,
															char *pmerge_insert_fields_str,
															char *pmerge_insert_values_str
															#endif
														)
{
	SHARED_set_bool_with_32_bit_change_bits_group(	pmois,
													&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->physically_available),
													pphysically_available,
													(false),
													pgenerate_change_line_bool,
													CHANGE_MOISTURE_SENSOR_PHYSICALLY_AVAILABLE,
													preason_for_change,
													pinitiator_box_index_0,
													pset_change_bits,
													pchange_bitfield_to_set,
													&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->changes_to_upload_to_comm_server),
													MOISTURE_SENSOR_CHANGE_BITFIELD_physically_available_bit,
													MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_physically_available_bit ]
													#ifdef _MSC_VER
													, pmerge_update_str,
													pmerge_insert_fields_str,
													pmerge_insert_values_str
													#endif
												);
}

/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_set_in_use(	void *const pmois,
											BOOL_32 pmoisture_sensor_in_use,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 preason_for_change,
											const UNS_32 pinitiator_box_index_0,
											const BOOL_32 pset_change_bits,
											CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											#ifdef _MSC_VER
											, char *pmerge_update_str,
											char *pmerge_insert_fields_str,
											char *pmerge_insert_values_str
											#endif
										)
{
	SHARED_set_bool_with_32_bit_change_bits_group(	pmois,
													&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->in_use),
													pmoisture_sensor_in_use,
													(false),
													pgenerate_change_line_bool,
													CHANGE_MOISTURE_SENSOR_IN_USE,
													preason_for_change,
													pinitiator_box_index_0,
													pset_change_bits,
													pchange_bitfield_to_set,
													&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->changes_to_upload_to_comm_server),
													MOISTURE_SENSOR_CHANGE_BITFIELD_in_use_bit,
													MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_in_use_bit ]
													#ifdef _MSC_VER
													, pmerge_update_str,
													pmerge_insert_fields_str,
													pmerge_insert_values_str
													#endif
												);
}

/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_set_moisture_control_mode(	void *const pmois,
															UNS_32 pmoisture_control_mode,
															const BOOL_32 pgenerate_change_line_bool,
															const UNS_32 preason_for_change,
															const UNS_32 pinitiator_box_index_0,
															const BOOL_32 pset_change_bits,
															CHANGE_BITS_32_BIT *pchange_bitfield_to_set
															#ifdef _MSC_VER
															, char *pmerge_update_str,
															char *pmerge_insert_fields_str,
															char *pmerge_insert_values_str
															#endif
														 )
{
	SHARED_set_uint32_with_32_bit_change_bits_group(	pmois,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->moisture_control_mode),
														pmoisture_control_mode,
														MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_MIN,
														MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_MAX,
														MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_DEFAULT,
														pgenerate_change_line_bool,
														CHANGE_MOISTURE_SENSOR_MOISTURE_CONTROL_MODE,
														preason_for_change,
														pinitiator_box_index_0,
														pset_change_bits,
														pchange_bitfield_to_set,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->changes_to_upload_to_comm_server),
														MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_control_mode,
														MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_control_mode ]
														#ifdef _MSC_VER
														, pmerge_update_str,
														pmerge_insert_fields_str,
														pmerge_insert_values_str
														#endif
													);
}

/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_set_low_temp_point(	void *const pmois,
													UNS_32 plow_temp_point,
													const BOOL_32 pgenerate_change_line_bool,
													const UNS_32 preason_for_change,
													const UNS_32 pinitiator_box_index_0,
													const BOOL_32 pset_change_bits,
													CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													#ifdef _MSC_VER
													, char *pmerge_update_str,
													char *pmerge_insert_fields_str,
													char *pmerge_insert_values_str
													#endif
												 )
{
	SHARED_set_uint32_with_32_bit_change_bits_group(	pmois,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->low_temp_point),
														plow_temp_point,
														MOISTURE_SENSOR_LOW_TEMP_POINT_MIN,
														MOISTURE_SENSOR_LOW_TEMP_POINT_MAX,
														MOISTURE_SENSOR_LOW_TEMP_POINT_DEFAULT,
														pgenerate_change_line_bool,
														CHANGE_MOISTURE_SENSOR_LOW_TEMP_POINT,
														preason_for_change,
														pinitiator_box_index_0,
														pset_change_bits,
														pchange_bitfield_to_set,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->changes_to_upload_to_comm_server),
														MOISTURE_SENSOR_CHANGE_BITFIELD_low_temp_point_bit,
														MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_low_temp_point_bit ]
														#ifdef _MSC_VER
														, pmerge_update_str,
														pmerge_insert_fields_str,
														pmerge_insert_values_str
														#endif
													);
}

/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_set_additional_soak_seconds(	void *const pmois,
															UNS_32 padditional_soak_seconds,
															const BOOL_32 pgenerate_change_line_bool,
															const UNS_32 preason_for_change,
															const UNS_32 pinitiator_box_index_0,
															const BOOL_32 pset_change_bits,
															CHANGE_BITS_32_BIT *pchange_bitfield_to_set
															#ifdef _MSC_VER
															, char *pmerge_update_str,
															char *pmerge_insert_fields_str,
															char *pmerge_insert_values_str
															#endif
														 )
{
	SHARED_set_uint32_with_32_bit_change_bits_group(	pmois,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->additional_soak_seconds),
														padditional_soak_seconds,
														MOISTURE_SENSOR_ADDITIONAL_SOAK_SECONDS_MIN,
														MOISTURE_SENSOR_ADDITIONAL_SOAK_SECONDS_MAX,
														MOISTURE_SENSOR_ADDITIONAL_SOAK_SECONDS_DEFAULT,
														pgenerate_change_line_bool,
														CHANGE_MOISTURE_SENSOR_ADDITIONAL_SOAK,
														preason_for_change,
														pinitiator_box_index_0,
														pset_change_bits,
														pchange_bitfield_to_set,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->changes_to_upload_to_comm_server),
														MOISTURE_SENSOR_CHANGE_BITFIELD_additional_soak_seconds_bit,
														MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_additional_soak_seconds_bit ]
														#ifdef _MSC_VER
														, pmerge_update_str,
														pmerge_insert_fields_str,
														pmerge_insert_values_str
														#endif
													);
}

/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_set_moisture_high_set_point_float(	void *const pmois,
																	float pmoisture_high_set_point,
																	const BOOL_32 pgenerate_change_line_bool,
																	const UNS_32 preason_for_change,
																	const UNS_32 pinitiator_box_index_0,
																	const BOOL_32 pset_change_bits,
																	CHANGE_BITS_32_BIT *pchange_bitfield_to_set
																	#ifdef _MSC_VER
																	, char *pmerge_update_str,
																	char *pmerge_insert_fields_str,
																	char *pmerge_insert_values_str
																	#endif
																 )
{
	SHARED_set_float_with_32_bit_change_bits_group(	pmois,
													&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->moisture_high_set_point_float),
													pmoisture_high_set_point,
													MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_MIN,
													MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_MAX,
													MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_DEFAULT,
													pgenerate_change_line_bool,
													CHANGE_MOISTURE_SENSOR_MOISTURE_HIGH_SET_POINT_float,
													preason_for_change,
													pinitiator_box_index_0,
													pset_change_bits,
													pchange_bitfield_to_set,
													&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->changes_to_upload_to_comm_server),
													MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_high_set_point_float_bit,
													MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_high_set_point_float_bit ]
													#ifdef _MSC_VER
													, pmerge_update_str,
													pmerge_insert_fields_str,
													pmerge_insert_values_str
													#endif
												);
}

/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_set_moisture_low_set_point_float(	void *const pmois,
																	float pmoisture_low_set_point,
																	const BOOL_32 pgenerate_change_line_bool,
																	const UNS_32 preason_for_change,
																	const UNS_32 pinitiator_box_index_0,
																	const BOOL_32 pset_change_bits,
																	CHANGE_BITS_32_BIT *pchange_bitfield_to_set
																	#ifdef _MSC_VER
																	, char *pmerge_update_str,
																	char *pmerge_insert_fields_str,
																	char *pmerge_insert_values_str
																	#endif
																 )
{
	SHARED_set_float_with_32_bit_change_bits_group(	pmois,
													&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->moisture_low_set_point_float),
													pmoisture_low_set_point,
													MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_MIN,
													MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_MAX,
													MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_DEFAULT,
													pgenerate_change_line_bool,
													CHANGE_MOISTURE_SENSOR_MOISTURE_LOW_SET_POINT_float,
													preason_for_change,
													pinitiator_box_index_0,
													pset_change_bits,
													pchange_bitfield_to_set,
													&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->changes_to_upload_to_comm_server),
													MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_low_set_point_float_bit,
													MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_low_set_point_float_bit ]
													#ifdef _MSC_VER
													, pmerge_update_str,
													pmerge_insert_fields_str,
													pmerge_insert_values_str
													#endif
												);
}

/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_set_high_temp_point(	void *const pmois,
													INT_32 phigh_temp_point,
													const BOOL_32 pgenerate_change_line_bool,
													const UNS_32 preason_for_change,
													const UNS_32 pinitiator_box_index_0,
													const BOOL_32 pset_change_bits,
													CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													#ifdef _MSC_VER
													, char *pmerge_update_str,
													char *pmerge_insert_fields_str,
													char *pmerge_insert_values_str
													#endif
												 )
{
	SHARED_set_int32_with_32_bit_change_bits_group(		pmois,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->high_temp_point),
														phigh_temp_point,
														MOISTURE_SENSOR_HIGH_TEMP_POINT_MIN,
														MOISTURE_SENSOR_HIGH_TEMP_POINT_MAX,
														MOISTURE_SENSOR_HIGH_TEMP_POINT_DEFAULT,
														pgenerate_change_line_bool,
														CHANGE_MOISTURE_SENSOR_HIGH_TEMP_POINT,
														preason_for_change,
														pinitiator_box_index_0,
														pset_change_bits,
														pchange_bitfield_to_set,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->changes_to_upload_to_comm_server),
														MOISTURE_SENSOR_CHANGE_BITFIELD_high_temp_point_bit,
														MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_high_temp_point_bit ]
														#ifdef _MSC_VER
														, pmerge_update_str,
														pmerge_insert_fields_str,
														pmerge_insert_values_str
														#endif
													);
}

/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_set_high_temp_action(	void *const pmois,
														UNS_32 phigh_temp_action,
														const BOOL_32 pgenerate_change_line_bool,
														const UNS_32 preason_for_change,
														const UNS_32 pinitiator_box_index_0,
														const BOOL_32 pset_change_bits,
														CHANGE_BITS_32_BIT *pchange_bitfield_to_set
														#ifdef _MSC_VER
														, char *pmerge_update_str,
														char *pmerge_insert_fields_str,
														char *pmerge_insert_values_str
														#endif
														)
{
	SHARED_set_uint32_with_32_bit_change_bits_group(	pmois,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->high_temp_action),
														phigh_temp_action,
														MOISTURE_SENSOR_HIGH_TEMP_ACTION_MIN,
														MOISTURE_SENSOR_HIGH_TEMP_ACTION_MAX,
														MOISTURE_SENSOR_HIGH_TEMP_ACTION_DEFAULT,
														pgenerate_change_line_bool,
														CHANGE_MOISTURE_SENSOR_HIGH_TEMP_ACTION,
														preason_for_change,
														pinitiator_box_index_0,
														pset_change_bits,
														pchange_bitfield_to_set,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->changes_to_upload_to_comm_server),
														MOISTURE_SENSOR_CHANGE_BITFIELD_high_temp_action_bit,
														MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_high_temp_action_bit ]
														#ifdef _MSC_VER
														, pmerge_update_str,
														pmerge_insert_fields_str,
														pmerge_insert_values_str
														#endif
													);
}

/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_set_low_temp_action(	void *const pmois,
													UNS_32 plow_temp_action,
													const BOOL_32 pgenerate_change_line_bool,
													const UNS_32 preason_for_change,
													const UNS_32 pinitiator_box_index_0,
													const BOOL_32 pset_change_bits,
													CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													#ifdef _MSC_VER
													, char *pmerge_update_str,
													char *pmerge_insert_fields_str,
													char *pmerge_insert_values_str
													#endif
													)
{
	SHARED_set_uint32_with_32_bit_change_bits_group(	pmois,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->low_temp_action),
														plow_temp_action,
														MOISTURE_SENSOR_LOW_TEMP_ACTION_MIN,
														MOISTURE_SENSOR_LOW_TEMP_ACTION_MAX,
														MOISTURE_SENSOR_LOW_TEMP_ACTION_DEFAULT,
														pgenerate_change_line_bool,
														CHANGE_MOISTURE_SENSOR_LOW_TEMP_ACTION,
														preason_for_change,
														pinitiator_box_index_0,
														pset_change_bits,
														pchange_bitfield_to_set,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->changes_to_upload_to_comm_server),
														MOISTURE_SENSOR_CHANGE_BITFIELD_low_temp_action_bit,
														MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_low_temp_action_bit ]
														#ifdef _MSC_VER
														, pmerge_update_str,
														pmerge_insert_fields_str,
														pmerge_insert_values_str
														#endif
													);
}

/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_set_high_ec_point(	void *const pmois,
													float phigh_ec_point,
													const BOOL_32 pgenerate_change_line_bool,
													const UNS_32 preason_for_change,
													const UNS_32 pinitiator_box_index_0,
													const BOOL_32 pset_change_bits,
													CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													#ifdef _MSC_VER
													, char *pmerge_update_str,
													char *pmerge_insert_fields_str,
													char *pmerge_insert_values_str
													#endif
													)
{
	SHARED_set_float_with_32_bit_change_bits_group(	pmois,
													&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->high_ec_point),
													phigh_ec_point,
													MOISTURE_SENSOR_HIGH_EC_POINT_MIN,
													MOISTURE_SENSOR_HIGH_EC_POINT_MAX,
													MOISTURE_SENSOR_HIGH_EC_POINT_DEFAULT,
													pgenerate_change_line_bool,
													CHANGE_MOISTURE_SENSOR_HIGH_EC_POINT,
													preason_for_change,
													pinitiator_box_index_0,
													pset_change_bits,
													pchange_bitfield_to_set,
													&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->changes_to_upload_to_comm_server),
													MOISTURE_SENSOR_CHANGE_BITFIELD_high_ec_point_bit,
													MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_high_ec_point_bit ]
													#ifdef _MSC_VER
													, pmerge_update_str,
													pmerge_insert_fields_str,
													pmerge_insert_values_str
													#endif
													);
}

/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_set_low_ec_point(	void *const pmois,
													float plow_ec_point,
													const BOOL_32 pgenerate_change_line_bool,
													const UNS_32 preason_for_change,
													const UNS_32 pinitiator_box_index_0,
													const BOOL_32 pset_change_bits,
													CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													#ifdef _MSC_VER
													, char *pmerge_update_str,
													char *pmerge_insert_fields_str,
													char *pmerge_insert_values_str
													#endif
													)
{
	SHARED_set_float_with_32_bit_change_bits_group(	pmois,
													&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->low_ec_point),
													plow_ec_point,
													MOISTURE_SENSOR_LOW_EC_POINT_MIN,
													MOISTURE_SENSOR_LOW_EC_POINT_MAX,
													MOISTURE_SENSOR_LOW_EC_POINT_DEFAULT,
													pgenerate_change_line_bool,
													CHANGE_MOISTURE_SENSOR_LOW_EC_POINT,
													preason_for_change,
													pinitiator_box_index_0,
													pset_change_bits,
													pchange_bitfield_to_set,
													&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->changes_to_upload_to_comm_server),
													MOISTURE_SENSOR_CHANGE_BITFIELD_low_ec_point_bit,
													MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_low_ec_point_bit ]
													#ifdef _MSC_VER
													, pmerge_update_str,
													pmerge_insert_fields_str,
													pmerge_insert_values_str
													#endif
													);
}

/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_set_high_ec_action(	void *const pmois,
													UNS_32 phigh_ec_action,
													const BOOL_32 pgenerate_change_line_bool,
													const UNS_32 preason_for_change,
													const UNS_32 pinitiator_box_index_0,
													const BOOL_32 pset_change_bits,
													CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													#ifdef _MSC_VER
													, char *pmerge_update_str,
													char *pmerge_insert_fields_str,
													char *pmerge_insert_values_str
													#endif
													)
{
	SHARED_set_uint32_with_32_bit_change_bits_group(	pmois,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->high_ec_action),
														phigh_ec_action,
														MOISTURE_SENSOR_HIGH_EC_ACTION_MIN,
														MOISTURE_SENSOR_HIGH_EC_ACTION_MAX,
														MOISTURE_SENSOR_HIGH_EC_ACTION_DEFAULT,
														pgenerate_change_line_bool,
														CHANGE_MOISTURE_SENSOR_HIGH_EC_ACTION,
														preason_for_change,
														pinitiator_box_index_0,
														pset_change_bits,
														pchange_bitfield_to_set,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->changes_to_upload_to_comm_server),
														MOISTURE_SENSOR_CHANGE_BITFIELD_high_ec_action_bit,
														MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_high_ec_action_bit ]
														#ifdef _MSC_VER
														, pmerge_update_str,
														pmerge_insert_fields_str,
														pmerge_insert_values_str
														#endif
													);
}

/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_set_low_ec_action(	void *const pmois,
													UNS_32 plow_ec_action,
													const BOOL_32 pgenerate_change_line_bool,
													const UNS_32 preason_for_change,
													const UNS_32 pinitiator_box_index_0,
													const BOOL_32 pset_change_bits,
													CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													#ifdef _MSC_VER
													, char *pmerge_update_str,
													char *pmerge_insert_fields_str,
													char *pmerge_insert_values_str
													#endif
													)
{
	SHARED_set_uint32_with_32_bit_change_bits_group(	pmois,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->low_ec_action),
														plow_ec_action,
														MOISTURE_SENSOR_LOW_EC_ACTION_MIN,
														MOISTURE_SENSOR_LOW_EC_ACTION_MAX,
														MOISTURE_SENSOR_LOW_EC_ACTION_DEFAULT,
														pgenerate_change_line_bool,
														CHANGE_MOISTURE_SENSOR_LOW_EC_ACTION,
														preason_for_change,
														pinitiator_box_index_0,
														pset_change_bits,
														pchange_bitfield_to_set,
														&(((MOISTURE_SENSOR_GROUP_STRUCT*)pmois)->changes_to_upload_to_comm_server),
														MOISTURE_SENSOR_CHANGE_BITFIELD_low_ec_action_bit,
														MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_low_ec_action_bit ]
														#ifdef _MSC_VER
														, pmerge_update_str,
														pmerge_insert_fields_str,
														pmerge_insert_values_str
														#endif
													);
}

/* ---------------------------------------------------------- */
static void nm_MOISTURE_SENSOR_store_changes(	MOISTURE_SENSOR_GROUP_STRUCT *const ptemporary_group,
												const BOOL_32 pmoisture_sensor_created,
												const UNS_32 preason_for_change,
												const UNS_32 pinitiator_box_index_0,
												const BOOL_32 pset_change_bits,
												const UNS_32 pchanges_received_from,
												CHANGE_BITS_32_BIT pbitfield_of_changes
												#ifdef _MSC_VER
												, char *pmerge_update_str,
												char *pmerge_insert_fields_str,
												char *pmerge_insert_values_str
												#endif
											)
{
	// 2/4/2016 rmd : Locates the group in the list that matches the passed in temporary group
	// and uses the structure's set routines to compare the two and make any changes to the
	// list's group. If any changes are made during this process, a function is called to store
	// the changes in the list.
	//
	// Caller MUST be holding the moisture_sensor_items_recursive_MUTEX to ensure a stable
	// moisture sensor list during this functions activity.

	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;
	
	// ----------
	
	#ifndef _MSC_VER

	// 2/9/2016 rmd : Just based on the GID find a match in the list.
	nm_GROUP_find_this_group_in_list( &moisture_sensor_group_list_hdr, ptemporary_group, (void**)&lmois );

	if( lmois != NULL )
	#else

	lmois = ptemporary_group;

	#endif
	{
		#ifndef _MSC_VER

			lchange_bitfield_to_set = MOISTURE_SENSOR_get_change_bits_ptr( lmois, pchanges_received_from );

		#else

			// 7/28/2015 ajv : Initialize to NULL since this variable is used by the CommServer but
			// expected to be NULL.
			lchange_bitfield_to_set = NULL;

		#endif

		// ----------

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || B_IS_SET( pbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_name_bit ) )
		{
			nm_MOISTURE_SENSOR_set_name(	lmois, ptemporary_group->base.description, !(pmoisture_sensor_created), preason_for_change, pinitiator_box_index_0, pset_change_bits, lchange_bitfield_to_set
											#ifdef _MSC_VER
											, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
											#endif
										  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || B_IS_SET( pbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_box_index_bit ) )
		{
			nm_MOISTURE_SENSOR_set_box_index(	lmois, ptemporary_group->box_index_0, !(pmoisture_sensor_created), preason_for_change, pinitiator_box_index_0, pset_change_bits, lchange_bitfield_to_set
												#ifdef _MSC_VER
												, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												#endif
											  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || B_IS_SET( pbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_decoder_serial_number_bit ) )
		{
			nm_MOISTURE_SENSOR_set_decoder_serial_number(	lmois, ptemporary_group->decoder_serial_number, !(pmoisture_sensor_created), preason_for_change, pinitiator_box_index_0, pset_change_bits, lchange_bitfield_to_set
															#ifdef _MSC_VER
															, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
															#endif
														  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || B_IS_SET( pbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_physically_available_bit ) )
		{
			nm_MOISTURE_SENSOR_set_physically_available(	lmois, ptemporary_group->physically_available, !(pmoisture_sensor_created), preason_for_change, pinitiator_box_index_0, pset_change_bits, lchange_bitfield_to_set
															#ifdef _MSC_VER
															, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
															#endif
														  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || B_IS_SET( pbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_in_use_bit ) )
		{
			nm_MOISTURE_SENSOR_set_in_use(	lmois, ptemporary_group->in_use, !(pmoisture_sensor_created), preason_for_change, pinitiator_box_index_0, pset_change_bits, lchange_bitfield_to_set
											#ifdef _MSC_VER
											, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
											#endif
										  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || B_IS_SET( pbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_control_mode ) )
		{
			nm_MOISTURE_SENSOR_set_moisture_control_mode(	lmois, ptemporary_group->moisture_control_mode, !(pmoisture_sensor_created), preason_for_change, pinitiator_box_index_0, pset_change_bits, lchange_bitfield_to_set
															#ifdef _MSC_VER
															, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
															#endif
														  );
		}
		
		if( (preason_for_change == CHANGE_REASON_KEYPAD) || B_IS_SET( pbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_low_temp_point_bit ) )
		{
			nm_MOISTURE_SENSOR_set_low_temp_point(	lmois, ptemporary_group->low_temp_point, !(pmoisture_sensor_created), preason_for_change, pinitiator_box_index_0, pset_change_bits, lchange_bitfield_to_set
													#ifdef _MSC_VER
													, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
													#endif
												  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || B_IS_SET( pbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_additional_soak_seconds_bit ) )
		{
			nm_MOISTURE_SENSOR_set_additional_soak_seconds(	lmois, ptemporary_group->additional_soak_seconds, !(pmoisture_sensor_created), preason_for_change, pinitiator_box_index_0, pset_change_bits, lchange_bitfield_to_set
															#ifdef _MSC_VER
															, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
															#endif
														  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || B_IS_SET( pbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_high_set_point_float_bit ) )
		{
			nm_MOISTURE_SENSOR_set_moisture_high_set_point_float(	lmois, ptemporary_group->moisture_high_set_point_float, !(pmoisture_sensor_created), preason_for_change, pinitiator_box_index_0, pset_change_bits, lchange_bitfield_to_set
															#ifdef _MSC_VER
															, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
															#endif
														  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || B_IS_SET( pbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_low_set_point_float_bit ) )
		{
			nm_MOISTURE_SENSOR_set_moisture_low_set_point_float(	lmois, ptemporary_group->moisture_low_set_point_float, !(pmoisture_sensor_created), preason_for_change, pinitiator_box_index_0, pset_change_bits, lchange_bitfield_to_set
															#ifdef _MSC_VER
															, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
															#endif
														  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || B_IS_SET( pbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_high_temp_point_bit ) )
		{
			nm_MOISTURE_SENSOR_set_high_temp_point(	lmois, ptemporary_group->high_temp_point, !(pmoisture_sensor_created), preason_for_change, pinitiator_box_index_0, pset_change_bits, lchange_bitfield_to_set
													#ifdef _MSC_VER
													, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
													#endif
													);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || B_IS_SET( pbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_high_temp_action_bit ) )
		{
			nm_MOISTURE_SENSOR_set_high_temp_action(	lmois, ptemporary_group->high_temp_action, !(pmoisture_sensor_created), preason_for_change, pinitiator_box_index_0, pset_change_bits, lchange_bitfield_to_set
														#ifdef _MSC_VER
														, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
														#endif
														);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || B_IS_SET( pbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_low_temp_action_bit ) )
		{
			nm_MOISTURE_SENSOR_set_low_temp_action(	lmois, ptemporary_group->low_temp_action, !(pmoisture_sensor_created), preason_for_change, pinitiator_box_index_0, pset_change_bits, lchange_bitfield_to_set
													#ifdef _MSC_VER
													, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
													#endif
													);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || B_IS_SET( pbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_high_ec_point_bit ) )
		{
			nm_MOISTURE_SENSOR_set_high_ec_point(	lmois, ptemporary_group->high_ec_point, !(pmoisture_sensor_created), preason_for_change, pinitiator_box_index_0, pset_change_bits, lchange_bitfield_to_set
													#ifdef _MSC_VER
													, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
													#endif
													);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || B_IS_SET( pbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_low_ec_point_bit ) )
		{
			nm_MOISTURE_SENSOR_set_low_ec_point(	lmois, ptemporary_group->low_ec_point, !(pmoisture_sensor_created), preason_for_change, pinitiator_box_index_0, pset_change_bits, lchange_bitfield_to_set
													#ifdef _MSC_VER
													, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
													#endif
													);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || B_IS_SET( pbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_high_ec_action_bit ) )
		{
			nm_MOISTURE_SENSOR_set_high_ec_action(	lmois, ptemporary_group->high_ec_action, !(pmoisture_sensor_created), preason_for_change, pinitiator_box_index_0, pset_change_bits, lchange_bitfield_to_set
													#ifdef _MSC_VER
													, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
													#endif
													);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || B_IS_SET( pbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_low_ec_action_bit ) )
		{
			nm_MOISTURE_SENSOR_set_low_ec_action(	lmois, ptemporary_group->low_ec_action, !(pmoisture_sensor_created), preason_for_change, pinitiator_box_index_0, pset_change_bits, lchange_bitfield_to_set
													#ifdef _MSC_VER
													, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
													#endif
													);
		}

	}
	#ifndef _MSC_VER
	else
	{
		Alert_group_not_found();
	}
	#endif
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex Calling function must have the list_moisture_items_recursive_MUTEX mutex to
 *  	  ensure no list items are added or removed during this process.
 *
 * @executed Executed within the context of the CommMngr task as a token or
 *           token response is being built.
 * 
 * @param pucp A pointer to an unsigned character pointer which contains the
 *             information that was received.
 * @param preason_for_change The reason for the change, such as keypad.
 * 
 * @return UNS_32 The total size of the extracted data. This is used to
 *                increment the ucp in the calling function.
 *
 * @author 4/5/2012 Adrianusv
 *
 * @revisions
 *    4/5/2012 Initial release
 */
extern UNS_32 nm_MOISTURE_SENSOR_extract_and_store_changes_from_comm(	const unsigned char *pucp,
																		const UNS_32 preason_for_change,
																		const BOOL_32 pset_change_bits,
																		const UNS_32 pchanges_received_from
																		#ifdef _MSC_VER
																		, char *pSQL_statements,
																		char *pSQL_search_condition_str,
																		char *pSQL_update_str,
																		char *pSQL_insert_fields_str,
																		char *pSQL_insert_values_str,
																		char *pSQL_single_merge_statement_buf,
																		const UNS_32 pnetwork_ID
																		#endif
																		)
{
	UNS_32	ldecoder_serial_number;

	UNS_32	lsize_of_bitfield;

	CHANGE_BITS_32_BIT	lbitfield_of_changes;

	MOISTURE_SENSOR_GROUP_STRUCT 	*ltemporary_mois;

	#ifndef _MSC_VER

	MOISTURE_SENSOR_GROUP_STRUCT 	*lmatching_mois;

	#endif

	UNS_32	lnum_changed_moisture_sensors;

	// 4/12/2016 rmd : Used to suppress CHANGE lines during the SET functions.
	BOOL_32	lmois_created;

	UNS_32	i;

	UNS_32	rv;

	// ----------
	
	// 7/10/2014 rmd : This function is used when parsing either a token or token resp. When a
	// moisture sensor is created or modified. This function can be thought about as executed at
	// either the FOAL side or IRRI side ie at masters or slaves. It happens at both.
	
	// ----------
	
	rv = 0;

	lmois_created = (false);

	// ----------

	// Extract the number of changed groups
	memcpy( &lnum_changed_moisture_sensors, pucp, sizeof(lnum_changed_moisture_sensors) );
	pucp += sizeof( lnum_changed_moisture_sensors );
	rv += sizeof( lnum_changed_moisture_sensors );

	/*
	// 8/8/2016 rmd : For debug only.
	if( pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE )
	{
		Alert_Message_va( "rcvd from slave: MOISTURE SENSOR CHANGES for %u groups", lnum_changed_moisture_sensors );
	}
	else
	if( pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER )
	{
		Alert_Message_va( "rcvd from master: MOISTURE SENSOR CHANGES for %u groups", lnum_changed_moisture_sensors );
	}
	else
	{
		Alert_Message_va( "rcvd from UNK: MOISTURE SENSOR CHANGES for %u groups", lnum_changed_moisture_sensors );
	}
	*/
	
	// ----------
	
	for( i = 0; i < lnum_changed_moisture_sensors; ++i )
	{
		// 2/9/2016 rmd : The OVERHEAD is the decoder serial number, an UNS_32 indicating the size
		// of the bitfield, and the 32-bit change bitfield itself.

		memcpy( &ldecoder_serial_number, pucp, sizeof(ldecoder_serial_number) );
		pucp += sizeof( ldecoder_serial_number );
		rv += sizeof( ldecoder_serial_number );

		memcpy( &lsize_of_bitfield, pucp, sizeof(lsize_of_bitfield) );
		pucp += sizeof( lsize_of_bitfield );
		rv += sizeof( lsize_of_bitfield );

		memcpy( &lbitfield_of_changes, pucp, sizeof(lbitfield_of_changes) );
		pucp += sizeof( lbitfield_of_changes );
		rv += sizeof( lbitfield_of_changes );

		// ----------
	
		#ifdef _MSC_VER
	
			// 1/16/2015 ajv : Make sure the strings are fully initialized to NULL since we rely on this
			// to determine whether the string is empty or not.
			memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
			memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
			memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
			memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	
			// ----------
	
			// 1/19/2015 ajv : Insert the network ID in case the MOISTURE SENSOR doesn't exist in the
			// database yet.
			SQL_MERGE_build_upsert_uint32( pnetwork_ID, SQL_MERGE_include_field_name_in_insert_clause, "NetworkID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

		#endif
	
		// ----------

		// 1/8/2015 ajv : When compiling for the CommServer, we build a MERGE statement if any of
		// the station's settings have changed. Therefore, we can completely skip over searching for
		// the station - this will fail anyway since the list doesn't exist in the comm server.
		#ifndef _MSC_VER

			// 4/12/2016 rmd : See if this sensor already is in the file list.
			lmatching_mois = MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number( ldecoder_serial_number );
			
			// 3/6/2013 ajv : If the specified group doesn't exist yet, we need to create it and assign
			// it the group ID.
			if( lmatching_mois == NULL )
			{
				// 4/11/2016 rmd : Before we go to create a list item see if the list ONLY has the 'DUMMY'
				// place-holder decoder. With a 0 serial number. This would be the list item we made when
				// the file was created. If that is the ONLY list item we'll take it over. This step helps
				// AJ with the UI and cleans up some list presentation issues. How to easily skip over dummy
				// list items. He says so.
				lmatching_mois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

				// 4/11/2016 rmd : There should always be at least ONE list item but I'm checking for NULL
				// just in case.
				if( (lmatching_mois != NULL) && (nm_ListGetCount( &moisture_sensor_group_list_hdr ) == 1) && (lmatching_mois->decoder_serial_number == 0) )
				{
					// 4/11/2016 rmd : Do not create one. We have a pointer to the 'dummy' list item that has a
					// 0 serial number. We will use it below. BUT we must set ALL the bits to ensure this ENTIRE
					// list item is sent to the CommServer because it has not yet been sent at all. By choice.
					SHARED_set_all_32_bit_change_bits( &(lmatching_mois->changes_to_upload_to_comm_server), moisture_sensor_items_recursive_MUTEX );
				}
				else
				{
					// 2/26/2016 rmd : Create a new group with physically available set to (false) and in_use
					// set to (false). We need to explicitly set those two to (true).
					lmatching_mois = nm_MOISTURE_SENSOR_create_new_group();

					lmois_created = (true);
				}
				
				// ----------

				if( pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER )
				{
					// 5/7/2014 ajv : If we don't want to set the change bits that means we called this function
					// via a TOKEN receipt. And any MOIS creation would be for MOIS SENSORS not at this box. And
					// therefore we don't have to resend out these changes. The distribution of this activity
					// creating MOISs and assigning the values STOPS here in this case.
					SHARED_clear_all_32_bit_change_bits( &lmatching_mois->changes_to_send_to_master, moisture_sensor_items_recursive_MUTEX );

					//Alert_Message_va( "PDATA: POC %u (idx: %d, typ: %d) created cleared", lpoc_id, lbox_index, lpoc_type );
				}
				else
				{
					//Alert_Message_va( "PDATA: POC %u (idx: %d, typ: %d) created", lpoc_id, lbox_index, lpoc_type );
				}
			}

		#endif

		// ----------

		#ifndef _MSC_VER

		if( lmatching_mois != NULL )

		#endif
		{
			// Allocate memory for, and initialize, the temporary group
			#ifndef _MSC_VER
				ltemporary_mois = mem_malloc( sizeof(MOISTURE_SENSOR_GROUP_STRUCT) );
			#else
				ltemporary_mois = malloc( sizeof(MOISTURE_SENSOR_GROUP_STRUCT) );
			#endif

			memset( ltemporary_mois, 0x00, sizeof(MOISTURE_SENSOR_GROUP_STRUCT) );

			#ifndef _MSC_VER

				// Make a copy of the group. We do this so we can change the settings
				// and pass it to the store_changes routine which will actually update
				// the group's settings.
				memcpy( ltemporary_mois, lmatching_mois, sizeof(MOISTURE_SENSOR_GROUP_STRUCT) );

			#else

				// 5/1/2015 ajv : Since we don't copy an existing MOIS into the temporary one when parsing
				// using the CommServer, make sure we set the identifing variables manually based on what
				// came in at the beginning of the memory block.
				//
				// 4/12/2016 rmd : I don't know why we're doing this???????? Just mimicing AJ.
				ltemporary_mois->decoder_serial_number = ldecoder_serial_number;
				
			#endif

			if( B_IS_SET( lbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_name_bit ) )
			{
				memcpy( &ltemporary_mois->base.description, pucp, NUMBER_OF_CHARS_IN_A_NAME );
				pucp += NUMBER_OF_CHARS_IN_A_NAME;
				rv += NUMBER_OF_CHARS_IN_A_NAME;
			}

			if( B_IS_SET( lbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_box_index_bit ) )
			{
				memcpy( &ltemporary_mois->box_index_0, pucp, sizeof(ltemporary_mois->box_index_0) );
				pucp += sizeof( ltemporary_mois->box_index_0 );
				rv += sizeof( ltemporary_mois->box_index_0 );
			}

			if( B_IS_SET( lbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_decoder_serial_number_bit ) )
			{
				memcpy( &ltemporary_mois->decoder_serial_number, pucp, sizeof(ltemporary_mois->decoder_serial_number) );
				pucp += sizeof( ltemporary_mois->decoder_serial_number );
				rv += sizeof( ltemporary_mois->decoder_serial_number );
			}

			if( B_IS_SET( lbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_physically_available_bit ) )
			{
				memcpy( &ltemporary_mois->physically_available, pucp, sizeof(ltemporary_mois->physically_available) );
				pucp += sizeof( ltemporary_mois->physically_available );
				rv += sizeof( ltemporary_mois->physically_available );
			}

			if( B_IS_SET( lbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_in_use_bit ) )
			{
				memcpy( &ltemporary_mois->in_use, pucp, sizeof(ltemporary_mois->in_use) );
				pucp += sizeof( ltemporary_mois->in_use );
				rv += sizeof( ltemporary_mois->in_use );
			}

			if( B_IS_SET( lbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_control_mode ) )
			{
				memcpy( &ltemporary_mois->moisture_control_mode, pucp, sizeof(ltemporary_mois->moisture_control_mode) );
				pucp += sizeof( ltemporary_mois->moisture_control_mode );
				rv += sizeof( ltemporary_mois->moisture_control_mode );
			}
			
			if( B_IS_SET( lbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_low_temp_point_bit ) )
			{
				memcpy( &ltemporary_mois->low_temp_point, pucp, sizeof(ltemporary_mois->low_temp_point) );
				pucp += sizeof( ltemporary_mois->low_temp_point );
				rv += sizeof( ltemporary_mois->low_temp_point );
			}

			if( B_IS_SET( lbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_additional_soak_seconds_bit ) )
			{
				memcpy( &ltemporary_mois->additional_soak_seconds, pucp, sizeof(ltemporary_mois->additional_soak_seconds) );
				pucp += sizeof( ltemporary_mois->additional_soak_seconds );
				rv += sizeof( ltemporary_mois->additional_soak_seconds );
			}

			if( B_IS_SET( lbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_high_set_point_float_bit ) )
			{
				memcpy( &ltemporary_mois->moisture_high_set_point_float, pucp, sizeof(ltemporary_mois->moisture_high_set_point_float) );
				pucp += sizeof( ltemporary_mois->moisture_high_set_point_float );
				rv += sizeof( ltemporary_mois->moisture_high_set_point_float );
			}

			if( B_IS_SET( lbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_low_set_point_float_bit ) )
			{
				memcpy( &ltemporary_mois->moisture_low_set_point_float, pucp, sizeof(ltemporary_mois->moisture_low_set_point_float) );
				pucp += sizeof( ltemporary_mois->moisture_low_set_point_float );
				rv += sizeof( ltemporary_mois->moisture_low_set_point_float );
			}

			if( B_IS_SET( lbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_high_temp_point_bit ) )
			{
				memcpy( &ltemporary_mois->high_temp_point, pucp, sizeof(ltemporary_mois->high_temp_point) ); 
				pucp += sizeof( ltemporary_mois->high_temp_point );
				rv += sizeof( ltemporary_mois->high_temp_point );
			}

			if( B_IS_SET( lbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_high_temp_action_bit ) )
			{
				memcpy( &ltemporary_mois->high_temp_action, pucp, sizeof(ltemporary_mois->high_temp_action) ); 
				pucp += sizeof( ltemporary_mois->high_temp_action );
				rv += sizeof( ltemporary_mois->high_temp_action );
			}

			if( B_IS_SET( lbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_low_temp_action_bit ) )
			{
				memcpy( &ltemporary_mois->low_temp_action, pucp, sizeof(ltemporary_mois->low_temp_action) ); 
				pucp += sizeof( ltemporary_mois->low_temp_action );
				rv += sizeof( ltemporary_mois->low_temp_action );
			}

			if( B_IS_SET( lbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_high_ec_point_bit ) )
			{
				memcpy( &ltemporary_mois->high_ec_point, pucp, sizeof(ltemporary_mois->high_ec_point) ); 
				pucp += sizeof( ltemporary_mois->high_ec_point );
				rv += sizeof( ltemporary_mois->high_ec_point );
			}

			if( B_IS_SET( lbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_low_ec_point_bit ) )
			{
				memcpy( &ltemporary_mois->low_ec_point, pucp, sizeof(ltemporary_mois->low_ec_point) ); 
				pucp += sizeof( ltemporary_mois->low_ec_point );
				rv += sizeof( ltemporary_mois->low_ec_point );
			}

			if( B_IS_SET( lbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_high_ec_action_bit ) )
			{
				memcpy( &ltemporary_mois->high_ec_action, pucp, sizeof(ltemporary_mois->high_ec_action) ); 
				pucp += sizeof( ltemporary_mois->high_ec_action );
				rv += sizeof( ltemporary_mois->high_ec_action );
			}

			if( B_IS_SET( lbitfield_of_changes, MOISTURE_SENSOR_CHANGE_BITFIELD_low_ec_action_bit ) )
			{
				memcpy( &ltemporary_mois->low_ec_action, pucp, sizeof(ltemporary_mois->low_ec_action) ); 
				pucp += sizeof( ltemporary_mois->low_ec_action );
				rv += sizeof( ltemporary_mois->low_ec_action );
			}

			// ----------
			
			nm_MOISTURE_SENSOR_store_changes(	ltemporary_mois, lmois_created, preason_for_change, 0, pset_change_bits, pchanges_received_from, lbitfield_of_changes
												#ifdef _MSC_VER
												, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												#endif
												);

			// ----------
			 
			#ifndef _MSC_VER

				mem_free( ltemporary_mois );

			#else

				SQL_MERGE_build_search_condition_uint32( "NetworkID", pnetwork_ID, pSQL_search_condition_str );

				SQL_MERGE_build_search_condition_uint32( "DecoderSerialNumber", ltemporary_mois->decoder_serial_number, pSQL_search_condition_str );

				SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements( DATABASE_TABLE_NAME_MOISTURE_SENSORS, pSQL_search_condition_str, SQL_MERGE_include_field_name_in_insert_clause, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf );

				// ----------
				
				free( ltemporary_mois );

			#endif
		}
		#ifndef _MSC_VER
		else
		{
			Alert_group_not_found();
		}
		#endif
	}

	#ifndef _MSC_VER

		// Only save the file if we've extracted information out of the ucp. This is
		// a sanity check to ensure the bit is only set if data was included.
		if( rv > 0 )
		{
			// 5/13/2015 ajv : We want to save the file in the following cases:
			// 
			//   - A message from the CommServer since we've responded that we received it and need to
			//     ensure it's saved.
			//   - A token response from the slave to the master
			//   - A token from the master to the slave
			//   
			// We DO NOT want to save the file when a token is received by the master since we've
			// already saved the file for one of the reasons above. This will otherwise result in a
			// double save for each change - once when it comes in and once when it's received when the
			// master distributes the changes back out.
			if( (pchanges_received_from == CHANGE_REASON_CENTRAL_OR_MOBILE) ||
				(pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE) ||
				((pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER) && (!FLOWSENSE_we_are_a_master_one_way_or_another())) )
			{
				// 12/10/2014 ajv : Schedule a save rather than saving immediately to prevent stacking of
				// saves. Additionally, set a relatively short time to hel cover cases where users make a
				// lot of changes in a short period.
				FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_MOISTURE_SENSORS, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
			}
		}
		else
		{
			Alert_bit_set_with_no_data();
		}

	#endif

	return( rv );
}

/* ---------------------------------------------------------- */

#ifdef _MSC_VER

extern INT_32 MOISTURE_SENSOR_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	INT_32	rv;

	// ----------

	// 4/15/2016 ajv : In most cases, we're going to pass back an array_index of 0. However, in
	// the event the value in question is part of an array, we're going to pass back the actual
	// index so that the CommServer can sort not just on the bit number, but also the
	// array_index for values that share the same bit number.
	*psecondary_sort_order = 0;

	rv = -1;

	// ----------

	if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_name_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MOISTURE_SENSOR_CHANGE_BITFIELD_name_bit;
		*psize_of_var_ptr = sizeof(lmois->base.description);
	}
	else if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_box_index_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MOISTURE_SENSOR_CHANGE_BITFIELD_box_index_bit;
		*psize_of_var_ptr = sizeof(lmois->box_index_0);
	}
	else if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_decoder_serial_number_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MOISTURE_SENSOR_CHANGE_BITFIELD_decoder_serial_number_bit;
		*psize_of_var_ptr = sizeof(lmois->decoder_serial_number);
	}
	else if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_physically_available_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MOISTURE_SENSOR_CHANGE_BITFIELD_physically_available_bit;
		*psize_of_var_ptr = sizeof(lmois->physically_available);
	}
	else if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_in_use_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MOISTURE_SENSOR_CHANGE_BITFIELD_in_use_bit;
		*psize_of_var_ptr = sizeof(lmois->in_use);
	}
	else if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_control_mode ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_control_mode;
		*psize_of_var_ptr = sizeof(lmois->moisture_control_mode);
	}
	else if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ nlu_MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_high_set_point_uns32_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = nlu_MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_high_set_point_uns32_bit;
		*psize_of_var_ptr = sizeof(lmois->nlu_moisture_high_set_point_uns32);
	}
	else if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ nlu_MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_low_set_point_uns32_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = nlu_MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_low_set_point_uns32_bit;
		*psize_of_var_ptr = sizeof(lmois->nlu_moisture_low_set_point_uns32);
	}
	else if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_low_temp_point_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MOISTURE_SENSOR_CHANGE_BITFIELD_low_temp_point_bit;
		*psize_of_var_ptr = sizeof(lmois->low_temp_point);
	}
	else if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_additional_soak_seconds_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MOISTURE_SENSOR_CHANGE_BITFIELD_additional_soak_seconds_bit;
		*psize_of_var_ptr = sizeof(lmois->additional_soak_seconds);
	}
	else if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_high_set_point_float_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_high_set_point_float_bit;
		*psize_of_var_ptr = sizeof(lmois->moisture_high_set_point_float);
	}
	else if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_low_set_point_float_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_low_set_point_float_bit;
		*psize_of_var_ptr = sizeof(lmois->moisture_low_set_point_float);
	}
	else if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_high_temp_point_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MOISTURE_SENSOR_CHANGE_BITFIELD_high_temp_point_bit;
		*psize_of_var_ptr = sizeof(lmois->high_temp_point);
	}
	else if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_high_temp_action_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MOISTURE_SENSOR_CHANGE_BITFIELD_high_temp_action_bit;
		*psize_of_var_ptr = sizeof(lmois->high_temp_action);
	}
	else if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_low_temp_action_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MOISTURE_SENSOR_CHANGE_BITFIELD_low_temp_action_bit;
		*psize_of_var_ptr = sizeof(lmois->low_temp_action);
	}
	else if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_high_ec_point_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MOISTURE_SENSOR_CHANGE_BITFIELD_high_ec_point_bit;
		*psize_of_var_ptr = sizeof(lmois->high_ec_point);
	}
	else if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_low_ec_point_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MOISTURE_SENSOR_CHANGE_BITFIELD_low_ec_point_bit;
		*psize_of_var_ptr = sizeof(lmois->low_ec_point);
	}
	else if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_high_ec_action_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MOISTURE_SENSOR_CHANGE_BITFIELD_high_ec_action_bit;
		*psize_of_var_ptr = sizeof(lmois->high_ec_action);
	}
	else if( strncmp( pfield_name, MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_low_ec_action_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MOISTURE_SENSOR_CHANGE_BITFIELD_low_ec_action_bit;
		*psize_of_var_ptr = sizeof(lmois->low_ec_action);
	}
	
	return( rv );
}

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

