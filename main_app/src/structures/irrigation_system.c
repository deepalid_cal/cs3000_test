/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for abs
#include	<stdlib.h>

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"cs_common.h"

#include	"irrigation_system.h"

#include	"app_startup.h"

#include	"bithacks.h"

#include	"budgets.h"

#include	"change.h"

#include	"comm_mngr.h"

#include	"cs_mem.h"

#include	"e_budget_setup.h"

#include	"e_station_selection_grid.h"

#include	"epson_rx_8025sa.h"

#include	"flash_storage.h"

#include	"flowsense.h"

#include	"foal_comm.h"

#include	"foal_flow.h"

#include	"group_base_file.h"

#include	"irri_comm.h"

#include	"pdata_changes.h"

#include	"range_check.h"

#include	"r_alerts.h"

#include	"shared.h"

#include	"alerts.h"

#include	"controller_initiated.h"

#include	"poc.h"

#include	"chain_sync_vars.h"

#include	"chain_sync_funcs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static MIST_LIST_HDR_TYPE	system_group_list_hdr;

// ----------

// 2/18/2016 rmd : Must include the shared c source after the system_group_list_header
// declaration.
#include	"shared_irrigation_system.c"

// ----------

const char IRRIGATION_SYSTEM_DEFAULT_NAME[] =			"Mainline";

static const char IRRIGATION_SYSTEM_FILENAME[] =		"IRRIGATION_SYSTEM";

// ----------
// 02/10/2016 skc : It's unwise to use #defines for array sizes when
// it's very easy for the value to be inadvertantly changed. The old
// structures need to be very well defined in size in order for the
// versioning scheme to work.

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	BOOL_32							used_for_irrigation_bool;

	BOOL_32							capacity_in_use_bool;
	UNS_32							capacity_with_pump_gpm;
	UNS_32							capacity_without_pump_gpm;

	UNS_32							mlb_during_irri_gpm;
	UNS_32							mlb_during_mvor_gpm;
	UNS_32							mlb_all_other_times_gpm;

	BOOL_32							flow_checking_in_use_bool;
	UNS_32							flow_checking_ranges_gpm[ 3 ];

	UNS_32							flow_checking_tolerances_plus_gpm[ 4 ];
	UNS_32							flow_checking_tolerances_minus_gpm[ 4 ];

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

	BOOL_32		derate_table_allowed_to_lock;
	UNS_32		derate_table_gpm_slot_size;				// default of 1
	UNS_32		derate_table_max_stations_ON;			// default of 8
	UNS_32		derate_table_number_of_gpm_slots;		// default of 400
	UNS_32		derate_table_before_checking_number_of_station_cycles;		// default of 6
	UNS_32		derate_table_before_checking_number_of_cell_iterations;		// default of 6

} IRRIGATION_SYSTEM_GROUP_STRUCT_REV_0_and_1;

// ----------

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	BOOL_32							used_for_irrigation_bool;

	BOOL_32							capacity_in_use_bool;
	UNS_32							capacity_with_pump_gpm;
	UNS_32							capacity_without_pump_gpm;

	UNS_32							mlb_during_irri_gpm;
	UNS_32							mlb_during_mvor_gpm;
	UNS_32							mlb_all_other_times_gpm;

	BOOL_32							flow_checking_in_use_bool;
	UNS_32							flow_checking_ranges_gpm[ 3 ];

	UNS_32							flow_checking_tolerances_plus_gpm[ 4 ];
	UNS_32							flow_checking_tolerances_minus_gpm[ 4 ];

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

	BOOL_32		derate_table_allowed_to_lock;
	UNS_32		derate_table_gpm_slot_size;				// default of 1
	UNS_32		derate_table_max_stations_ON;			// default of 8
	UNS_32		derate_table_number_of_gpm_slots;		// default of 400
	UNS_32		derate_table_before_checking_number_of_station_cycles;		// default of 6
	UNS_32		derate_table_before_checking_number_of_cell_iterations;		// default of 6

	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

} IRRIGATION_SYSTEM_GROUP_STRUCT_REV_2;

// ----------

typedef struct
{
	UNS_32	open_time;

	UNS_32	close_time;

} MVOR_SCHEDULE_STRUCT;

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	BOOL_32							used_for_irrigation_bool;

	BOOL_32							capacity_in_use_bool;
	UNS_32							capacity_with_pump_gpm;
	UNS_32							capacity_without_pump_gpm;

	UNS_32							mlb_during_irri_gpm;
	UNS_32							mlb_during_mvor_gpm;
	UNS_32							mlb_all_other_times_gpm;

	BOOL_32							flow_checking_in_use_bool;
	UNS_32							flow_checking_ranges_gpm[ 3 ];

	UNS_32							flow_checking_tolerances_plus_gpm[ 4 ];
	UNS_32							flow_checking_tolerances_minus_gpm[ 4 ];

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

	BOOL_32		derate_table_allowed_to_lock;
	UNS_32		derate_table_gpm_slot_size;				// default of 1
	UNS_32		derate_table_max_stations_ON;			// default of 8
	UNS_32		derate_table_number_of_gpm_slots;		// default of 400
	UNS_32		derate_table_before_checking_number_of_station_cycles;		// default of 6
	UNS_32		derate_table_before_checking_number_of_cell_iterations;		// default of 6
	
	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;
	
	MVOR_SCHEDULE_STRUCT			mvor_schedule[ 7 ];

} IRRIGATION_SYSTEM_GROUP_STRUCT_REV_3;

// ----------

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	BOOL_32							used_for_irrigation_bool;

	BOOL_32							capacity_in_use_bool;
	UNS_32							capacity_with_pump_gpm;
	UNS_32							capacity_without_pump_gpm;

	UNS_32							mlb_during_irri_gpm;
	UNS_32							mlb_during_mvor_gpm;
	UNS_32							mlb_all_other_times_gpm;

	BOOL_32							flow_checking_in_use_bool;
	UNS_32							flow_checking_ranges_gpm[ 3 ];

	UNS_32							flow_checking_tolerances_plus_gpm[ 4 ];
	UNS_32							flow_checking_tolerances_minus_gpm[ 4 ];

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;
	
	BOOL_32		derate_table_allowed_to_lock;
	UNS_32		derate_table_gpm_slot_size;				// default of 1
	UNS_32		derate_table_max_stations_ON;			// default of 8
	UNS_32		derate_table_number_of_gpm_slots;		// default of 400
	UNS_32		derate_table_before_checking_number_of_station_cycles;		// default of 6
	UNS_32		derate_table_before_checking_number_of_cell_iterations;		// default of 6
    
	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

	UNS_32		mvor_open_times[ 7 ];
	UNS_32		mvor_close_times[ 7 ];

	UNS_32		budget_use_choice;
	UNS_32		budget_meter_read_time;
	UNS_32		budget_number_of_annual_budget_periods;
	UNS_32		budget_period_start_date[ 12 ];
	UNS_32		budget_period_end_date[ 12 ];
	BOOL_32		budget_halt_irrigation_when_budget_reached;
	
} IRRIGATION_SYSTEM_GROUP_STRUCT_REV_4_and_5;

// 3/7/2016 rmd : NOTE - Rev 5 didn't add any new variables. It was created to redfine some
// exisitng MVOR vars.

// ----------

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	BOOL_32							used_for_irrigation_bool;

	BOOL_32							capacity_in_use_bool;
	UNS_32							capacity_with_pump_gpm;
	UNS_32							capacity_without_pump_gpm;

	UNS_32							mlb_during_irri_gpm;
	UNS_32							mlb_during_mvor_gpm;
	UNS_32							mlb_all_other_times_gpm;

	BOOL_32							flow_checking_in_use_bool;
	UNS_32							flow_checking_ranges_gpm[ 3 ];

	UNS_32							flow_checking_tolerances_plus_gpm[ 4 ];
	UNS_32							flow_checking_tolerances_minus_gpm[ 4 ];

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;
	
	BOOL_32		derate_table_allowed_to_lock;
	UNS_32		derate_table_gpm_slot_size;				// default of 1
	UNS_32		derate_table_max_stations_ON;			// default of 8
	UNS_32		derate_table_number_of_gpm_slots;		// default of 400
	UNS_32		derate_table_before_checking_number_of_station_cycles;		// default of 6
	UNS_32		derate_table_before_checking_number_of_cell_iterations;		// default of 6
    
	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

	UNS_32		mvor_open_times[ 7 ];
	UNS_32		mvor_close_times[ 7 ];

	UNS_32		budget_use_choice;
	UNS_32		budget_meter_read_time;
	UNS_32		budget_number_of_annual_budget_periods;
	UNS_32		budget_period_start_date[ 12 ];
	UNS_32		budget_period_end_date[ 12 ];
	BOOL_32		budget_halt_irrigation_when_budget_reached;
	BOOL_32		in_use;

} IRRIGATION_SYSTEM_GROUP_STRUCT_REV_6_and_7;

// ----------

// 1/26/2015 rmd : The structure revision number. If the structure definition changes this
// value is to be changed. Typically incremented by 1. Then in the updater function handle
// the change specifics whatever they may be. Generally meaning initialize newly added
// values. But could be more extensive than that.
#define	IRRIGATION_SYSTEM_LATEST_FILE_REVISION				(8)

// ----------

const UNS_32 irrigation_system_list_item_sizes[ IRRIGATION_SYSTEM_LATEST_FILE_REVISION + 1 ] =
{
	// 1/26/2015 rmd : In order for this array to contain OLDER list item sizes the implication
	// is that the older structure definitions are to be retained. That means the newer
	// structure definition has a different name.

	// ----------
	
	// 1/30/2015 rmd : Revision 0 structure.
	sizeof( IRRIGATION_SYSTEM_GROUP_STRUCT_REV_0_and_1 ),

	// 1/30/2015 rmd : Revision 1 structure. Same as Revision 0. Revision 1 does not change
	// structure size. It was created in order to initialize the comm_server distribution
	// variable.
	sizeof( IRRIGATION_SYSTEM_GROUP_STRUCT_REV_0_and_1 ),

	sizeof( IRRIGATION_SYSTEM_GROUP_STRUCT_REV_2 ),

	// 8/26/2015 ajv : Revision 3 introduces the Master Valve Override Schedule.
	sizeof( IRRIGATION_SYSTEM_GROUP_STRUCT_REV_3 ),

	// 9/14/2015 rmd : Rev 4 adds budgets.
	sizeof( IRRIGATION_SYSTEM_GROUP_STRUCT_REV_4_and_5 ),

	// 9/21/2015 rmd : Rev 5 reworked the Master Valve Override Schedule. No size change, just
	// redefinition of existing (unreleased) variables.
	sizeof( IRRIGATION_SYSTEM_GROUP_STRUCT_REV_4_and_5 ),

	// 9/22/2015 ajv : Rev 6 adds the "In Use" flag which is used to indicate whether systems
	// have been deleted.
	sizeof( IRRIGATION_SYSTEM_GROUP_STRUCT_REV_6_and_7 ),

	// 2/17/2016 ajv : Rev 7 resets the in_use flag back to TRUE. A previous firmware release
	// had a bug which could result in the flag being set FALSE unintentionally.
	sizeof( IRRIGATION_SYSTEM_GROUP_STRUCT_REV_6_and_7 ),

	// 02/03/2016 skc : Rev 8 is the first version to officially support Budgets
	sizeof( IRRIGATION_SYSTEM_GROUP_STRUCT ),
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void nm_SYSTEM_set_default_values( void *const psystem, const BOOL_32 pset_change_bits );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void nm_irrigation_system_structure_updater( UNS_32 pfrom_revision )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only when
	// the file revision is changed. The caller is to be holding the list_system
	// recursive_MUTEX.

	// ----------
	
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsys;

	CHANGE_BITS_32_BIT	*lbitfield_of_changes;

	UNS_32	box_index_0;

	UNS_32	i;

	DATE_TIME_COMPLETE_STRUCT dtcs;
	UNS_32 month;
	UNS_32 year;
	// ----------
	
	// 9/21/2015 rmd : Because of the order of the init_file function calls in the app_startup
	// task we know the controller index is valid for use at this [early] point during the boot
	// process that this updater function is called.
	box_index_0 = FLOWSENSE_get_controller_index();

	// ----------
	
	if( pfrom_revision == IRRIGATION_SYSTEM_LATEST_FILE_REVISION )
	{
		Alert_Message_va( "IRRI SYS file unexpd update %u", pfrom_revision );	
	}
	else
	{
		Alert_Message_va( "IRRI SYS file update : to revision %u from %u", IRRIGATION_SYSTEM_LATEST_FILE_REVISION, pfrom_revision );

		// ----------
		
		if( pfrom_revision == 0 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 0 to REVISION 1. Because
			// of revision 0 bugs the upload to comm_server bits are in an unknown state. They need to
			// be set to trigger a complete send of PDATA to the comm_server.
			
			lsys = nm_ListGetFirst( &system_group_list_hdr );
		
			while( lsys != NULL )
			{
				// 1/30/2015 rmd : Because of revision 0 bugs these bits were not set. We need to set them
				// to guarantee all the program data is sent to the comm_server this one time.
				lsys->changes_to_upload_to_comm_server = UNS_32_MAX;
				
				// ----------
				
				lsys = nm_ListGetNext( &system_group_list_hdr, lsys );
			}
			
			// ----------
			
			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 1 to 2 changes if
			// needed.
			pfrom_revision += 1;
		}
		
		// ----------
		
		if( pfrom_revision == 1 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 1 to REVISION 2.
			lsys = nm_ListGetFirst( &system_group_list_hdr );

			while( lsys != NULL )
			{
				// 4/30/2015 ajv : Clear all of the pending Program Data bits.
				lsys->changes_uploaded_to_comm_server_awaiting_ACK = CHANGE_BITS_32_BIT_ALL_CLEAR;

				lsys = nm_ListGetNext( &system_group_list_hdr, lsys );
			}

			pfrom_revision += 1;
		}

		// ----------

		if( pfrom_revision == 2 )
		{
			// 8/26/2015 ajv : This is the work to do when moving from REVISION 2 to REVISION 3.
			lsys = nm_ListGetFirst( &system_group_list_hdr );

			while( lsys != NULL )
			{
				lbitfield_of_changes = &lsys->changes_to_send_to_master;

				for( i = 0; i < DAYS_IN_A_WEEK; ++i )
				{
					nm_SYSTEM_set_mvor_schedule_open_time( lsys, i, IRRIGATION_SYSTEM_MVOR_OPEN_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

					nm_SYSTEM_set_mvor_schedule_close_time( lsys, i, IRRIGATION_SYSTEM_MVOR_CLOSE_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
				}

				lsys = nm_ListGetNext( &system_group_list_hdr, lsys );
			}

			xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

			// 8/27/2015 ajv : Also make sure we initialize the Master Valve Override stop date and time
			// that's stored in each of the system preserves.
			for( i = 0; i < MAX_POSSIBLE_SYSTEMS; ++i )
			{
				system_preserves.system[ i ].mvor_stop_date = DATE_MINIMUM;

				system_preserves.system[ i ].mvor_stop_time = TIME_MINIMUM;
			}

			xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

			pfrom_revision += 1;
		}

		// ----------
		
		if( pfrom_revision == 3 )
		{
			// 9/14/2015 rmd : This is the work to do when moving from REV 3 to REV 4.
			// 02/10/2016 skc : We don't need to do anything, the only changes were
			// for budgets which didn't actually get implemented until REV 7.

			pfrom_revision += 1;
		}

		// ----------

		if( pfrom_revision == 4 )
		{
			// 9/21/2015 ajv : This is the work to do when moving from REVISION 4 to REVISION 5.
			lsys = nm_ListGetFirst( &system_group_list_hdr );

			// 9/21/2015 ajv : Since we redefined the memory location used by the MVOR variables, all we
			// need to do is reset them back to their defaults to clean it all up.
			while( lsys != NULL )
			{
				lbitfield_of_changes = &lsys->changes_to_send_to_master;

				for( i = 0; i < DAYS_IN_A_WEEK; ++i )
				{
					nm_SYSTEM_set_mvor_schedule_open_time( lsys, i, IRRIGATION_SYSTEM_MVOR_OPEN_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

					nm_SYSTEM_set_mvor_schedule_close_time( lsys, i, IRRIGATION_SYSTEM_MVOR_CLOSE_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
				}

				lsys = nm_ListGetNext( &system_group_list_hdr, lsys );
			}

			xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

			// 9/21/2015 ajv : In case a controller in the office had an existing Master Valve Override
			// schedule prior to this update, reset stop date and time that's stored in each of the
			// system preserves.
			for( i = 0; i < MAX_POSSIBLE_SYSTEMS; ++i )
			{
				system_preserves.system[ i ].mvor_stop_date = DATE_MINIMUM;

				system_preserves.system[ i ].mvor_stop_time = TIME_MINIMUM;
			}

			xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

			pfrom_revision += 1;
		}

		// ----------

		// 3/7/2016 rmd : Rev 6 added the in use variable. However after being deployed a bug was
		// discovered (visiting the Mainline screen) which would set the in_use (false) and save the
		// file. So we had to create Rev 7 in order to cause the updateer to run giving us the
		// opportunity to once again set in_use back to (true).
		if( (pfrom_revision == 5) || (pfrom_revision == 6) )
		{
			lsys = nm_ListGetFirst( &system_group_list_hdr );

			while( lsys != NULL )
			{
				lbitfield_of_changes = &lsys->changes_to_send_to_master;

				nm_SYSTEM_set_system_in_use( lsys, IRRIGATION_SYSTEM_IN_USE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				lsys = nm_ListGetNext( &system_group_list_hdr, lsys );
			}

			if( pfrom_revision == 5 )
			{
				pfrom_revision += 2;
			}
			else
			{
				pfrom_revision += 1;
			}
		}

		// ----------

		if( pfrom_revision == 7 )
		{
			// 02/03/2016 skc : This is the work to do when moving from REVISION 7 to REVISON 8.
			// Make sure all budget stuff is properly initialized
			lsys = nm_ListGetFirst( &system_group_list_hdr );

			while( lsys != NULL )
			{
				lbitfield_of_changes = &lsys->changes_to_send_to_master;

				nm_SYSTEM_set_budget_in_use( lsys, IRRIGATION_SYSTEM_BUDGET_IN_USE_NO, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				nm_SYSTEM_set_budget_mode( lsys, IRRIGATION_SYSTEM_BUDGET_MODE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				nm_SYSTEM_set_budget_meter_read_time( lsys, IRRIGATION_SYSTEM_BUDGET_METER_READ_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				// 01/11/2016 skc : We want to use the current year.
				EPSON_obtain_latest_complete_time_and_date( &dtcs );
				month = dtcs.__month; // one-based
				year = dtcs.__year;

				// 04/12/2016 skc : Set the 1st date to the start of the month and go to the future.
				for( i=0; i<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS; i++ )
				{
					nm_SYSTEM_set_budget_period( lsys, i, 
												 DMYToDate( DATE_MINIMUM_DAY_OF_MONTH, month, year ), 
												 CHANGE_do_not_generate_change_line, 
												 CHANGE_REASON_DONT_SHOW_A_REASON, 
												 box_index_0, 
												 CHANGE_set_change_bits, 
												 lbitfield_of_changes );

					// increment budget date
					month += 1; // point to the next month
					if( month > MONTHS_IN_A_YEAR ) // handle wraparound situation
					{
						month = 1; // use one-based months
						year += 1;
					}
				}

				// 4/12/2016 skc : Set the default flow type for storing usage in a budget period.
				for( i=0; i<FLOW_TYPE_NUM_TYPES; i++ )
				{
					// 5/09/2016 skc : Only set the bits we actually use to true, set others to false.
					nm_SYSTEM_set_budget_flow_type( lsys, i, 
													B_IS_SET(FLOW_TYPE_DEFAULT, i) ? (true) : (false), 
													CHANGE_do_not_generate_change_line, 
													CHANGE_REASON_DONT_SHOW_A_REASON, 
													box_index_0, 
													CHANGE_set_change_bits, 
													lbitfield_of_changes );
				}
				
				lsys->volume_predicted = 0;

				lsys->poc_ratio = 0;

				lsys = nm_ListGetNext( &system_group_list_hdr, lsys );
			}

			pfrom_revision += 1;
		}

		// ----------

	}

	// ----------

	// 1/29/2015 rmd : Well after the updater has run the pfrom_revision is supposed to equal
	// the latest revision. By design of this update process.
	if( pfrom_revision != IRRIGATION_SYSTEM_LATEST_FILE_REVISION )
	{
		Alert_Message( "IRRI SYS updater error" );
	}
}

/* ---------------------------------------------------------- */
extern void init_file_irrigation_system( void )
{
	FLASH_FILE_initialize_list_and_find_or_create_group_list_file(	FLASH_INDEX_1_GENERAL_STORAGE,
																	IRRIGATION_SYSTEM_FILENAME,
																	IRRIGATION_SYSTEM_LATEST_FILE_REVISION,
																	&system_group_list_hdr,
																	irrigation_system_list_item_sizes,
																	list_system_recursive_MUTEX,
																	&nm_irrigation_system_structure_updater,
																	&nm_SYSTEM_set_default_values,
																	(char*)IRRIGATION_SYSTEM_DEFAULT_NAME,
																	FF_IRRIGATION_SYSTEM );
}

/* ---------------------------------------------------------- */
extern void save_file_irrigation_system( void )
{
	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE,
															IRRIGATION_SYSTEM_FILENAME,
															IRRIGATION_SYSTEM_LATEST_FILE_REVISION,
															&system_group_list_hdr,
															sizeof( IRRIGATION_SYSTEM_GROUP_STRUCT ),
															list_system_recursive_MUTEX,
															FF_IRRIGATION_SYSTEM );
}

/* ---------------------------------------------------------- */
/**
 * Sets the default values for an irrigation system group. If the passed in
 * group is NULL, an error is raised and this function takes no further action.
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 *  	  ensure the group whose values are being set isn't deleted during this
 *  	  process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param psystem A pointer to the group to set.
 *
 * @author 8/10/2011 Adrianusv
 *
 * @revisions
 *    8/10/2011  Initial release
 *    1/24/2012  Added check to ensure passed in group is not NULL
 *    4/3/2012   Added initialization of change bits
 */
static void nm_SYSTEM_set_default_values( void *const psystem, const BOOL_32 pset_change_bits )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	UNS_32	box_index_0;

	UNS_32	i;

	box_index_0 = FLOWSENSE_get_controller_index();

	// 01/11/2016 skc : We want to use the current year.
	DATE_TIME_COMPLETE_STRUCT dtcs;
	EPSON_obtain_latest_complete_time_and_date( &dtcs );

	// ------------------------

	if( psystem != NULL )
	{
		// 5/14/2014 ajv : Clear all of the change bits for POC since it's just
		// been created. We'll then explicitly set each bit based upon whether
		// pset_change_bits is true or not.
		SHARED_clear_all_32_bit_change_bits( &((IRRIGATION_SYSTEM_GROUP_STRUCT *)psystem)->changes_to_send_to_master, list_system_recursive_MUTEX );
		SHARED_clear_all_32_bit_change_bits( &((IRRIGATION_SYSTEM_GROUP_STRUCT *)psystem)->changes_to_distribute_to_slaves, list_system_recursive_MUTEX );
		SHARED_clear_all_32_bit_change_bits( &((IRRIGATION_SYSTEM_GROUP_STRUCT *)psystem)->changes_uploaded_to_comm_server_awaiting_ACK, list_system_recursive_MUTEX );

		// 1/20/2015 ajv : However, we should explicitly set ALL bits for the Comm Server to ensure
		// changes are uploaded on a clean start.
		SHARED_set_all_32_bit_change_bits( &((IRRIGATION_SYSTEM_GROUP_STRUCT *)psystem)->changes_to_upload_to_comm_server, list_system_recursive_MUTEX );
		
		// ----------

		// 5/2/2014 ajv : When creating a new group, always set the changes to
		// be sent to the master. Once the master receives the changes, it will
		// distribute them back out to the slaves.
		lchange_bitfield_to_set = &((IRRIGATION_SYSTEM_GROUP_STRUCT *)psystem)->changes_to_send_to_master;

		// ----------

		nm_SYSTEM_set_system_used_for_irri( psystem, IRRIGATION_SYSTEM_USED_FOR_IRRI_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_SYSTEM_set_capacity_with_pump( psystem, IRRIGATION_SYSTEM_CAPACITY_PUMP_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_SYSTEM_set_capacity_without_pump( psystem, IRRIGATION_SYSTEM_CAPACITY_NONPUMP_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_SYSTEM_set_mlb_during_irri( psystem, IRRIGATION_SYSTEM_MLB_DURING_IRRI_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_SYSTEM_set_mlb_during_mvor( psystem, IRRIGATION_SYSTEM_MLB_DURING_MVOR_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_SYSTEM_set_mlb_all_other_times( psystem, IRRIGATION_SYSTEM_MLB_ALL_OTHER_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_SYSTEM_set_flow_checking_in_use( psystem, IRRIGATION_SYSTEM_FLOW_CHECKING_IN_USE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		for( i = 0; i < IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_RANGES; ++i )
		{
			nm_SYSTEM_set_flow_checking_range( psystem, (i + 1), IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS[ i ], CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
		}

		for( i = 0; i < IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_TOLERANCES; ++i )
		{
			nm_SYSTEM_set_flow_checking_tolerance_plus( psystem, (i + 1), IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS[ i ], CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
			nm_SYSTEM_set_flow_checking_tolerance_minus( psystem, (i + 1), IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS[ i ], CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
		}
		
		// ------------------------

		((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->derate_table_allowed_to_lock = IRRIGATION_SYSTEM_DERATE_ALLOWED_TO_LOCK_DEFAULT;

		((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->derate_table_gpm_slot_size = IRRIGATION_SYSTEM_DERATE_GPM_SLOT_SIZE_DEFAULT;

		((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->derate_table_max_stations_ON = IRRIGATION_SYSTEM_DERATE_MAX_STATIONS_ON_DEFAULT;

		((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->derate_table_number_of_gpm_slots = IRRIGATION_SYSTEM_DERATE_NUMBER_OF_GPM_SLOTS_DEFAULT;

		((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->derate_table_before_checking_number_of_station_cycles = IRRIGATION_SYSTEM_DERATE_STATION_CYCLES_DEFAULT;

		((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->derate_table_before_checking_number_of_cell_iterations = IRRIGATION_SYSTEM_DERATE_CELL_ITERATIONS_DEFAULT;
		
		// ------------------------

		nm_SYSTEM_set_budget_mode( psystem, IRRIGATION_SYSTEM_BUDGET_MODE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_SYSTEM_set_budget_meter_read_time( psystem, IRRIGATION_SYSTEM_BUDGET_METER_READ_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_SYSTEM_set_budget_number_of_annual_periods( psystem, IRRIGATION_SYSTEM_BUDGET_ANNUAL_PERIODS_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		// 03/22/2016 skc : Default periods start the first day of the current month and proceed into the future.
		// Originally, we started on the next month. Further review exposes this as an invalid
		// concept. A system may sit for weeks and/or months after leaving the factory, any future
		// budget dates will have passed by the time the system is actually put into use. Thus, why
		// bother setting budget dates into the future for default settings?
		UNS_32 month;
		UNS_32 year;

		month = dtcs.__month;
		year = dtcs.__year;
		for( i=0; i<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS; i++ )
		{
			nm_SYSTEM_set_budget_period( psystem, i, DMYToDate( DATE_MINIMUM_DAY_OF_MONTH, month, year ), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

			month += 1;
			if( month > MONTHS_IN_A_YEAR )
			{
				month = 1; // 03/23/2016 skc : DMYToDate uses months that start numbering at 1
				year += 1;
			}
		}

		nm_SYSTEM_set_budget_in_use( psystem, IRRIGATION_SYSTEM_BUDGET_IN_USE_NO, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		for( i=0; i<FLOW_TYPE_NUM_TYPES; i++ )
		{
			// 4/19/2016 skc : Only set the bits we actually use.
			if( B_IS_SET(FLOW_TYPE_DEFAULT, i) )
			{
				nm_SYSTEM_set_budget_flow_type( psystem, i, (true), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
			}
		}

		// ------------------------

		for( i = 0; i < DAYS_IN_A_WEEK; ++i )
		{
			nm_SYSTEM_set_mvor_schedule_open_time( psystem, i, IRRIGATION_SYSTEM_MVOR_OPEN_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

			nm_SYSTEM_set_mvor_schedule_close_time( psystem, i, IRRIGATION_SYSTEM_MVOR_CLOSE_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
		}

		nm_SYSTEM_set_system_in_use( psystem, IRRIGATION_SYSTEM_IN_USE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
	}
	else
	{
		Alert_func_call_with_null_ptr();
	}
}

/* ---------------------------------------------------------- */
/**
 * Creates a new irrigation system group and adds it to the irrigation system
 * list.
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 * ensure the list isn't modified during this process.
 *
 * @executed Executed within the context of the key processing task when the
 *  		 user creates a new group; within the context of the CommMngr task
 *  		 when a new group is created using a central or handheld radio
 *  		 remote.
 * 
 * @return void* A pointer to the newly created group.
 * 
 * @author 8/10/2011 Adrianusv
 * 
 * @revisions
 *    8/10/2011 Initial release
 */
extern void *nm_SYSTEM_create_new_group( const UNS_32 pchanges_received_from )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*litem_to_insert_ahead_of;
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	// ----------

	litem_to_insert_ahead_of = nm_ListGetFirst( &system_group_list_hdr );

	// 2/11/2016 ajv : Loop through each manual program until we either find one that's deleted
	// or we reach the end of the list. If we find one that's been deleted, we're going to
	// insert the new item ahead of it.
	while( litem_to_insert_ahead_of != NULL )
	{
		if( !litem_to_insert_ahead_of->in_use )
		{
			// 2/11/2016 ajv : We found a deleted one so break out of the looop so the new item in
			// inserted ahead of it.
			break;
		}

		litem_to_insert_ahead_of = nm_ListGetNext( &system_group_list_hdr, litem_to_insert_ahead_of );
	}

	// ----------
	
	// 5/2/2014 ajv : TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// (requires pchanges_received_from)

	// 2/11/2016 ajv : Note that we're no longer passing a NULL for the pnext_list_item
	// parameter. The reason is because the list is now sorted with deleted items (aka not in
	// use) at the end of the list. We therefore need to insert a new item before the first
	// deleted item, if it exists.
	lsystem = nm_GROUP_create_new_group( &system_group_list_hdr, (char*)IRRIGATION_SYSTEM_DEFAULT_NAME, &nm_SYSTEM_set_default_values, sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT), (true), litem_to_insert_ahead_of );

	return ( lsystem );
}

/* ---------------------------------------------------------- */
/**
 * This function runs through the irrigation system list and copies the contents of any
 * changed systems to the passed in ucp.
 * 
 * @executed Executed within the context of the CommMngr task as a token, token response, or
 * Program Data message to the Comm Server is being built.
 * 
 * @param pucp A pointer to the pointer where the program data is being built.
 * 
 * @param pmem_used_so_far The amount of memory used so far. Once this reaches our limit for
 * a single token (MEMORY_TO_ALLOCATE_FOR_SENDING_CHANGES), processing of the remaining
 * changes stops until it's time to build the next token or token response.
 * 
 * @param pmem_used_so_far_is_less_than_allocated_memory A pointer to the flag which
 * indicates whether we're allowed to add more changes to the token or token response.
 * 
 * @param preason_data_is_being_built The reason why the data is being built to send. Based
 * upon this reason, we examine a different change bit structure to determine what to
 * send. Valid values are CHANGE_REASON_SYNC_SENDING_TO_MASTER,
 * CHANGE_REASON_SYNC_DISTRIBUTING_TO_SLAVES, CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER.
 * 
 * @return UNS_32 The total size of the added data. This is used to determine whether any
 * changes were detected so the appropriate file change bit can be set as well as to
 * determine how much memory must be allocated to store the changes.
 *
 * @author 9/14/2011 Adrianusv
 */
extern UNS_32 SYSTEM_build_data_to_send( UNS_8 **pucp,
										 const UNS_32 pmem_used_so_far,
										 BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
										 const UNS_32 preason_data_is_being_built,
										 const UNS_32 pallocated_memory )
{
	CHANGE_BITS_32_BIT	*lbitfield_of_changes_to_use_to_determine_what_to_send;

	CHANGE_BITS_32_BIT	lbitfield_of_changes_to_send;

	IRRIGATION_SYSTEM_GROUP_STRUCT		*lsystem;

	UNS_8	*llocation_of_num_changed_systems;

	UNS_8	*llocation_of_bitfield;

	// 1/14/2015 ajv : Track how many POCs are added to the message.
	UNS_32	lnum_changed_systems;

	// Store the amount of memory used for current system. This is compared to the system's
	// overhead to determine whether any of the system's values were actually added or not.
	UNS_32	lmem_used_by_this_system;

	// Determine how much overhead each system requires in case we add the overhead and don't
	// have enough room to add of the actual changes.
	UNS_32	lmem_overhead_per_system;

	UNS_32	rv;

	// ----------
	
	rv = 0;

	lnum_changed_systems = 0;

	// ----------

	// 1/14/2015 ajv : Take the appropriate mutex to ensure no systems are added or removed
	// during this process.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lsystem = nm_ListGetFirst( &system_group_list_hdr );

	// 1/14/2015 ajv : Loop through each system and count how many systems have change bits set. This
	// is really just used to determine whether there's at least one system which needs to be
	// examined.
	while( lsystem != NULL )
	{
		if( *(SYSTEM_get_change_bits_ptr( lsystem, preason_data_is_being_built )) != CHANGE_BITS_32_BIT_ALL_CLEAR )
		{
			lnum_changed_systems++;

			// 1/14/2015 ajv : Since we don't actually care about the number of systems that have
			// changes at this point - just that at least one has changed - we can break out of the loop
			// now.
			break;
		}

		lsystem = nm_ListGetNext( &system_group_list_hdr, lsystem );
	}

	// ----------

	if( lnum_changed_systems > 0 )
	{
		// 1/14/2015 ajv : Initialize the changed systems back to 0. Since not all of the systems
		// may be added due to token size limitations, we need to ensure that the receiving party
		// only processes the number of systems actually added to the message.
		lnum_changed_systems = 0;

		// ----------

		// 1/14/2015 ajv : Allocate space to store the number of changed stations that are included
		// in the message so the receiving party can correctly parse the data. This will be filled
		// in later once we have an actual count.
		rv = PDATA_allocate_space_for_num_changed_groups_in_pucp( pucp, &llocation_of_num_changed_systems, pmem_used_so_far, pmem_used_so_far_is_less_than_allocated_memory, pallocated_memory );

		// ----------

		// 2/9/2016 rmd : Well there was at least enough space for the 2 bytes for the count of
		// number of changed groups.
		if( *pmem_used_so_far_is_less_than_allocated_memory )
		{
			lsystem = nm_ListGetFirst( &system_group_list_hdr );

			while( lsystem != NULL )
			{
				lbitfield_of_changes_to_use_to_determine_what_to_send = SYSTEM_get_change_bits_ptr( lsystem, preason_data_is_being_built );

				// 1/14/2015 ajv : If this has changed...
				if( lbitfield_of_changes_to_use_to_determine_what_to_send != CHANGE_BITS_32_BIT_ALL_CLEAR )
				{
					// 2/9/2016 rmd : The OVERHEAD is the GID, an UNS_32 indicating the size of the bitfield,
					// and the 32-bit change bitfield itself.
					lmem_overhead_per_system = (3 * sizeof(UNS_32));

					// 1/14/2015 ajv : If we still have room available in the message for the overhead, add it
					// now.
					if( *pmem_used_so_far_is_less_than_allocated_memory && ((pmem_used_so_far + rv + lmem_overhead_per_system) < pallocated_memory) )
					{
						PDATA_copy_var_into_pucp( pucp, &lsystem->base.group_identity_number, sizeof(lsystem->base.group_identity_number) );

						PDATA_copy_bitfield_info_into_pucp( pucp, sizeof(CHANGE_BITS_32_BIT), sizeof(UNS_32), &llocation_of_bitfield ); 
					}
					else
					{
						// 9/19/2013 ajv : Stop processing if we've already filled out the token with the maximum
						// amount of data to send.
						*pmem_used_so_far_is_less_than_allocated_memory = (false);

						// 2/11/2016 rmd : No overhead added so leave pucp alone.
						
						break;
					}

					lmem_used_by_this_system = lmem_overhead_per_system;

					// ----------

					// Initialize the bit field
					lbitfield_of_changes_to_send = 0x00;
					
					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_description_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->base.description,
							NUMBER_OF_CHARS_IN_A_NAME,
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_used_with_irri_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->used_for_irrigation_bool,
							sizeof(lsystem->used_for_irrigation_bool),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_in_use_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->capacity_in_use_bool,
							sizeof(lsystem->capacity_in_use_bool),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_with_pump_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->capacity_with_pump_gpm,
							sizeof(lsystem->capacity_with_pump_gpm),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_without_pump_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->capacity_without_pump_gpm,
							sizeof(lsystem->capacity_without_pump_gpm),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_during_irri_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->mlb_during_irri_gpm,
							sizeof(lsystem->mlb_during_irri_gpm),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_during_mvor_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->mlb_during_mvor_gpm,
							sizeof(lsystem->mlb_during_mvor_gpm),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_all_other_times_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->mlb_all_other_times_gpm,
							sizeof(lsystem->mlb_all_other_times_gpm),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_in_use_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->flow_checking_in_use_bool,
							sizeof(lsystem->flow_checking_in_use_bool),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_ranges_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->flow_checking_ranges_gpm,
							sizeof(lsystem->flow_checking_ranges_gpm),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_tolerances_plus_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->flow_checking_tolerances_plus_gpm,
							sizeof(lsystem->flow_checking_tolerances_plus_gpm),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_tolerances_minus_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->flow_checking_tolerances_minus_gpm,
							sizeof(lsystem->flow_checking_tolerances_minus_gpm),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_mvor_open_times_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->mvor_open_times,
							sizeof(lsystem->mvor_open_times),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_mvor_close_times_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->mvor_close_times,
							sizeof(lsystem->mvor_close_times),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					// ----------

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_in_use_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->budget_in_use,
							sizeof(lsystem->budget_in_use),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_meter_read_time_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->budget_meter_read_time,
							sizeof(lsystem->budget_meter_read_time),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							 pucp,
							 &lbitfield_of_changes_to_send,
							 IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_annual_periods_bit,
							 lbitfield_of_changes_to_use_to_determine_what_to_send,
							 &lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							 &lsystem->budget_number_of_annual_periods,
							 sizeof(lsystem->budget_number_of_annual_periods),
							 (lmem_used_by_this_system + rv + pmem_used_so_far),
							 pmem_used_so_far_is_less_than_allocated_memory,
							 pallocated_memory,
							 preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_meter_read_date_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							lsystem->budget_meter_read_date,
							sizeof(lsystem->budget_meter_read_date),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_mode_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->budget_mode,
							sizeof(lsystem->budget_mode),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_in_use_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->in_use,
							sizeof(lsystem->in_use),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_system += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_flow_type_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lsystem->changes_uploaded_to_comm_server_awaiting_ACK,
							&lsystem->budget_flow_type,
							sizeof(lsystem->budget_flow_type),
							(lmem_used_by_this_system + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );


					// ----------

					// 2/10/2016 rmd : If we saw any changed bits and ACTUALLY ADDED CONTENT BEYOND THE ORIGINAL
					// OVERHEAD then complete out the message by placing the bitfield in the overhead and
					// updating function housekeeping.
					if( lmem_used_by_this_system > lmem_overhead_per_system )
					{
						// 2/10/2016 rmd : Bump the count of changed list items in the message. Place the bitfield
						// of changes into the overhead. And update the return value.
						lnum_changed_systems += 1;
						
						memcpy( llocation_of_bitfield, &lbitfield_of_changes_to_send, sizeof(lbitfield_of_changes_to_send) );
						
						rv += lmem_used_by_this_system;
					}
					else
					{
						// 2/10/2016 rmd : Else back out from the message the overhead already added.
						*pucp -= lmem_overhead_per_system;
					}
					
				}

				lsystem = nm_ListGetNext( &system_group_list_hdr, lsystem );
			}

			// ----------
			
			// 1/14/2015 ajv : If we've added the number of changes but didn't actually add any data
			// because we hit our limit, back out the number of changes.
			if( lnum_changed_systems > 0 )
			{
				memcpy( llocation_of_num_changed_systems, &lnum_changed_systems, sizeof(lnum_changed_systems) );
			}
			else
			{
				// 2/10/2016 rmd : If we didn't add any content beyond moving the msg pointer over where the
				// number of changed groups goes, then put the pointer back and return a 0 to indicate
				// nothing added.
				*pucp -= sizeof( lnum_changed_systems );

				rv = 0;
			}
		}
		else
		{
			// 2/11/2016 rmd : We couldn't do anything, meaning couldn't even add in the
			// num_of_changed_sensors. In that case RV is still 0 and pucp hasn't moved so nothing to
			// do here.
		}
	}

	// ----------

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Copies the contents of the passed in group into GuiVars which are used for
 * editing by the easyGUI library.
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 * ensure the passed in group is not deleted during this process.
 *
 * @executed Executed within the context of the display processing task when a
 *           new screen is being drawn.
 * 
 * @param psystem A pointer to the irrigation system group.
 *
 * @author 9/14/2011 Adrianusv
 *
 * @revisions
 *    9/14/2011 Initial release
 */
extern void SYSTEM_copy_group_into_guivars( const UNS_32 psystem_index_0 )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT *lsystem;

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = SYSTEM_get_group_at_this_index( psystem_index_0 );

	nm_GROUP_load_common_guivars( lsystem );

	GuiVar_SystemInUse = lsystem->in_use;

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void MAINLINE_BREAK_fill_guivars( const UNS_32 psystem_index_0, char *psystem_name, UNS_32 *pduring_irri, UNS_32 *pduring_mvor, UNS_32 *pall_other_times, BOOL_32 *pvisible )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT *lsystem;

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	if( nm_ListGetCount( &system_group_list_hdr ) > psystem_index_0 )
	{
		lsystem = SYSTEM_get_group_at_this_index( psystem_index_0 );

		// 9/6/2016 skc : We added the ability to delete mainlines, check the in_use flag.
		if( lsystem->in_use )
		{

			strlcpy( psystem_name, nm_GROUP_get_name( lsystem ), sizeof(GuiVar_GroupName_0) );

			*pduring_irri = lsystem->mlb_during_irri_gpm;

			*pduring_mvor = lsystem->mlb_during_mvor_gpm;

			*pall_other_times = lsystem->mlb_all_other_times_gpm;

			*pvisible = (true);
		}
		else
		{
			*pvisible = (false);
		}
	}
	else
	{
		*pvisible = (false);
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void MAINLINE_BREAK_copy_group_into_guivars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	MAINLINE_BREAK_fill_guivars( lindex_0++, GuiVar_GroupName_0, &GuiVar_GroupSettingA_0, &GuiVar_GroupSettingB_0, &GuiVar_GroupSettingC_0, &GuiVar_GroupSetting_Visible_0 );
	MAINLINE_BREAK_fill_guivars( lindex_0++, GuiVar_GroupName_1, &GuiVar_GroupSettingA_1, &GuiVar_GroupSettingB_1, &GuiVar_GroupSettingC_1, &GuiVar_GroupSetting_Visible_1 );
	MAINLINE_BREAK_fill_guivars( lindex_0++, GuiVar_GroupName_2, &GuiVar_GroupSettingA_2, &GuiVar_GroupSettingB_2, &GuiVar_GroupSettingC_2, &GuiVar_GroupSetting_Visible_2 );
	MAINLINE_BREAK_fill_guivars( lindex_0++, GuiVar_GroupName_3, &GuiVar_GroupSettingA_3, &GuiVar_GroupSettingB_3, &GuiVar_GroupSettingC_3, &GuiVar_GroupSetting_Visible_3 );
}

/* ---------------------------------------------------------- */
static void SYSTEM_CAPACITY_fill_guivars( const UNS_32 psystem_index_0, char *psystem_name, UNS_32 *pin_use, UNS_32 *pwith_pump, UNS_32 *pwithout_pump, UNS_32 *pvisible )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT *lsystem;

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	if( nm_ListGetCount( &system_group_list_hdr ) > psystem_index_0 )
	{
		lsystem = SYSTEM_get_group_at_this_index( psystem_index_0 );

		// 9/2/2016 skc : We added the ability to delete mainlines, check the in_use flag.
		if( lsystem->in_use )
		{

			strlcpy( psystem_name, nm_GROUP_get_name( lsystem ), sizeof(GuiVar_GroupName_0) );

			*pin_use = lsystem->capacity_in_use_bool;

			*pwith_pump = lsystem->capacity_with_pump_gpm;

			*pwithout_pump = lsystem->capacity_without_pump_gpm;

			*pvisible = (true);
		}
		else
		{
			*pvisible = (false);
		}
	}
	else
	{
		*pvisible = (false);
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void SYSTEM_CAPACITY_copy_group_into_guivars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	SYSTEM_CAPACITY_fill_guivars( lindex_0++, GuiVar_GroupName_0, &GuiVar_GroupSettingA_0, &GuiVar_GroupSettingB_0, &GuiVar_GroupSettingC_0, &GuiVar_GroupSetting_Visible_0 );
	SYSTEM_CAPACITY_fill_guivars( lindex_0++, GuiVar_GroupName_1, &GuiVar_GroupSettingA_1, &GuiVar_GroupSettingB_1, &GuiVar_GroupSettingC_1, &GuiVar_GroupSetting_Visible_1 );
	SYSTEM_CAPACITY_fill_guivars( lindex_0++, GuiVar_GroupName_2, &GuiVar_GroupSettingA_2, &GuiVar_GroupSettingB_2, &GuiVar_GroupSettingC_2, &GuiVar_GroupSetting_Visible_2 );
	SYSTEM_CAPACITY_fill_guivars( lindex_0++, GuiVar_GroupName_3, &GuiVar_GroupSettingA_3, &GuiVar_GroupSettingB_3, &GuiVar_GroupSettingC_3, &GuiVar_GroupSetting_Visible_3 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void FLOW_CHECKING_copy_group_into_guivars( const UNS_32 psystem_index_0 )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT *lsystem;

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = SYSTEM_get_group_at_this_index( psystem_index_0 );

	nm_GROUP_load_common_guivars( lsystem );

	GuiVar_SystemFlowCheckingInUse = lsystem->flow_checking_in_use_bool;
	GuiVar_SystemFlowCheckingRange1 = lsystem->flow_checking_ranges_gpm[ 0 ];
	GuiVar_SystemFlowCheckingRange2 = lsystem->flow_checking_ranges_gpm[ 1 ];
	GuiVar_SystemFlowCheckingRange3 = lsystem->flow_checking_ranges_gpm[ 2 ];
	GuiVar_SystemFlowCheckingTolerancePlus1 = lsystem->flow_checking_tolerances_plus_gpm[ 0 ];
	GuiVar_SystemFlowCheckingTolerancePlus2 = lsystem->flow_checking_tolerances_plus_gpm[ 1 ];
	GuiVar_SystemFlowCheckingTolerancePlus3 = lsystem->flow_checking_tolerances_plus_gpm[ 2 ];
	GuiVar_SystemFlowCheckingTolerancePlus4 = lsystem->flow_checking_tolerances_plus_gpm[ 3 ];
	GuiVar_SystemFlowCheckingToleranceMinus1 = ((INT_32)lsystem->flow_checking_tolerances_minus_gpm[ 0 ] * -1);
	GuiVar_SystemFlowCheckingToleranceMinus2 = ((INT_32)lsystem->flow_checking_tolerances_minus_gpm[ 1 ] * -1);
	GuiVar_SystemFlowCheckingToleranceMinus3 = ((INT_32)lsystem->flow_checking_tolerances_minus_gpm[ 2 ] * -1);
	GuiVar_SystemFlowCheckingToleranceMinus4 = ((INT_32)lsystem->flow_checking_tolerances_minus_gpm[ 3 ] * -1);

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void MVOR_copy_group_into_guivars( const UNS_32 psystem_index_0 )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT *lsystem;

	BY_SYSTEM_RECORD	*lpreserve;

	// ----------

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = SYSTEM_get_group_at_this_index( psystem_index_0 );

	// ----------

	nm_GROUP_load_common_guivars( lsystem );

	GuiVar_MVORRunTime = 4.0F;

	GuiVar_MVOROpen0 = (lsystem->mvor_open_times[ 0 ] / 60);
	GuiVar_MVOROpen1 = (lsystem->mvor_open_times[ 1 ] / 60);
	GuiVar_MVOROpen2 = (lsystem->mvor_open_times[ 2 ] / 60);
	GuiVar_MVOROpen3 = (lsystem->mvor_open_times[ 3 ] / 60);
	GuiVar_MVOROpen4 = (lsystem->mvor_open_times[ 4 ] / 60);
	GuiVar_MVOROpen5 = (lsystem->mvor_open_times[ 5 ] / 60);
	GuiVar_MVOROpen6 = (lsystem->mvor_open_times[ 6 ] / 60);

	GuiVar_MVOROpen0Enabled = (lsystem->mvor_open_times[ 0 ] != SCHEDULE_OFF_TIME);
	GuiVar_MVOROpen1Enabled = (lsystem->mvor_open_times[ 1 ] != SCHEDULE_OFF_TIME);
	GuiVar_MVOROpen2Enabled = (lsystem->mvor_open_times[ 2 ] != SCHEDULE_OFF_TIME);
	GuiVar_MVOROpen3Enabled = (lsystem->mvor_open_times[ 3 ] != SCHEDULE_OFF_TIME);
	GuiVar_MVOROpen4Enabled = (lsystem->mvor_open_times[ 4 ] != SCHEDULE_OFF_TIME);
	GuiVar_MVOROpen5Enabled = (lsystem->mvor_open_times[ 5 ] != SCHEDULE_OFF_TIME);
	GuiVar_MVOROpen6Enabled = (lsystem->mvor_open_times[ 6 ] != SCHEDULE_OFF_TIME);

	GuiVar_MVORClose0 = (lsystem->mvor_close_times[ 0 ] / 60);
	GuiVar_MVORClose1 = (lsystem->mvor_close_times[ 1 ] / 60);
	GuiVar_MVORClose2 = (lsystem->mvor_close_times[ 2 ] / 60);
	GuiVar_MVORClose3 = (lsystem->mvor_close_times[ 3 ] / 60);
	GuiVar_MVORClose4 = (lsystem->mvor_close_times[ 4 ] / 60);
	GuiVar_MVORClose5 = (lsystem->mvor_close_times[ 5 ] / 60);
	GuiVar_MVORClose6 = (lsystem->mvor_close_times[ 6 ] / 60);

	GuiVar_MVORClose0Enabled = (lsystem->mvor_close_times[ 0 ] != SCHEDULE_OFF_TIME);
	GuiVar_MVORClose1Enabled = (lsystem->mvor_close_times[ 1 ] != SCHEDULE_OFF_TIME);
	GuiVar_MVORClose2Enabled = (lsystem->mvor_close_times[ 2 ] != SCHEDULE_OFF_TIME);
	GuiVar_MVORClose3Enabled = (lsystem->mvor_close_times[ 3 ] != SCHEDULE_OFF_TIME);
	GuiVar_MVORClose4Enabled = (lsystem->mvor_close_times[ 4 ] != SCHEDULE_OFF_TIME);
	GuiVar_MVORClose5Enabled = (lsystem->mvor_close_times[ 5 ] != SCHEDULE_OFF_TIME);
	GuiVar_MVORClose6Enabled = (lsystem->mvor_close_times[ 6 ] != SCHEDULE_OFF_TIME);

	// ----------

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	// ----------

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	lpreserve = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( g_GROUP_ID );

	if( lpreserve != NULL )
	{
		GuiVar_MVORInEffect = (lpreserve->sbf.delivered_MVOR_in_effect_opened || lpreserve->sbf.delivered_MVOR_in_effect_closed);

		GuiVar_MVORState = lpreserve->sbf.delivered_MVOR_in_effect_closed;

		GuiVar_MVORTimeRemaining = (lpreserve->delivered_MVOR_remaining_seconds / 3600.0F);
	}
	else
	{
		GuiVar_MVORInEffect = (false);

		GuiVar_MVORState = 0;

		GuiVar_MVORTimeRemaining = 0.0F;
	}

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
// 3/22/2016 skc : Prepare UI for editing budgets per POC
extern void BUDGET_SETUP_copy_group_into_guivars( const UNS_32 psystem_index_0 )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT *lsystem;

	int i;
	//char str[ 16 ]; // The date string format is "01/21/16" for 8 characters total
	
	// ----------

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = SYSTEM_get_group_at_this_index( psystem_index_0 );

	// ----------

	nm_GROUP_load_common_guivars( lsystem );

	GuiVar_BudgetInUse = lsystem->budget_in_use;

	// 5/23/2016 skc : If no POCs attached to system, set the variable
	GuiVar_BudgetSysHasPOC = (SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid(nm_GROUP_get_group_ID(lsystem))->sbf.number_of_pocs_in_this_system > 0 ? 1 : 0);

	// 7/18/2016 skc : Special case, what if the user created a new mainline and attached the
	// POC to it?
	if( !GuiVar_BudgetSysHasPOC )
	{
		// 7/18/2016 skc : If there are no POC attached, disable the screen for the user, but don't
		// change any settings.
		GuiVar_BudgetInUse = false;
	}
	// 3/24/2016 skc : Convert from 12/6 to EasyGui index
	if( lsystem->budget_number_of_annual_periods == IRRIGATION_SYSTEM_BUDGET_ANNUAL_PERIODS_MIN)
	{
		GuiVar_BudgetPeriodsPerYearIdx = 0;
	}
	else
	{
		GuiVar_BudgetPeriodsPerYearIdx = 1;
	}

	// 8/9/2018 Ryan : There are now several budget entry options, so we must check the POCs
	// attached to this budget for their assigned budget entry option and assign it to the
	// GUI var.
	for( UNS_32 i=0; i<MAX_POCS_IN_NETWORK; i++ )
		{
			const BY_POC_RECORD * const pbpr = &poc_preserves.poc[ i ];

			// Find POCs enabled and attached to the system
			if( pbpr->poc_gid != 0 &&
				g_GROUP_ID == pbpr->this_pocs_system_preserves_ptr->system_gid )
			{
				POC_GROUP_STRUCT *ptr_poc = POC_get_group_at_this_index( POC_get_index_for_group_with_this_GID(pbpr->poc_gid) );

				GuiVar_BudgetEntryOptionIdx = POC_get_budget_gallons_entry_option( ptr_poc );
				
				// 9/10/2018 Ryan : set ET percentage used in budget, even if this entry mode is not
				// currently in use, as the entry mode could change.
				GuiVar_BudgetETPercentageUsed = POC_get_budget_percent_ET( ptr_poc );
			}
		}

	GuiVar_BudgetModeIdx = lsystem->budget_mode;

	// 03/18/2016 skc: Convert from seconds to minutes
	//GuiVar_BudgetMeterReadTime = (UNS_32)IRRIGATION_SYSTEM_BUDGET_METER_READ_TIME_DEFAULT / 60L; 

	for( i=0; i<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS; ++i )
	{
		g_BUDGET_SETUP_meter_read_date[i] = lsystem->budget_meter_read_date[i];
	}

	//strlcpy( GuiVar_BudgetPeriod_0, GetDateStr(str, sizeof(str), lsystem->budget_meter_read_date[0], DATESTR_show_short_year, 0), sizeof(GuiVar_BudgetPeriod_0) );

	// ----------

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
// 4/12/2016 skc : Prepare UI for editing budget flow types per System
extern void BUDGET_FLOW_TYPES_copy_group_into_guivars( const UNS_32 psystem_index_0 )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*ptr_sys;

	// ----------

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	ptr_sys = SYSTEM_get_group_at_this_index( psystem_index_0 );

	nm_GROUP_load_common_guivars( ptr_sys );

	// 4/27/2016 skc : Irrigation flow is ALWAYS checked, and marked solid
	GuiVar_BudgetFlowTypeIrrigation = (ptr_sys->budget_flow_type[FLOW_TYPE_IRRIGATION] ? 2 : 2);
	GuiVar_BudgetFlowTypeMVOR = (ptr_sys->budget_flow_type[FLOW_TYPE_MVOR] ? 1 : 0);
	GuiVar_BudgetFlowTypeNonController = (ptr_sys->budget_flow_type[FLOW_TYPE_NON_CONTROLLER] ? 1 : 0);

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}


/* ---------------------------------------------------------- */
/**
 * Stores the group name. This routine is called when the user presses BACK to
 * close the on-screen keyboard so the list of groups is updated with the new
 * name.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user presses BACK to close the on-screen keyboard.
 *
 * @author 12/11/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void SYSTEM_extract_and_store_group_name_from_GuiVars( void )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lsystem = SYSTEM_get_group_at_this_index( g_GROUP_list_item_index );

	if( lsystem != NULL )
	{
		SHARED_set_name_32_bit_change_bits( lsystem, GuiVar_GroupName, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, SYSTEM_get_change_bits_ptr( lsystem, CHANGE_REASON_KEYPAD ), &lsystem->changes_to_upload_to_comm_server, IRRIGATION_SYSTEM_CHANGE_BITFIELD_description_bit );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Copies the contents of the GuiVars, used for editing by the easyGUI library,
 * into a temporary structure which is used for comparison with a group in the
 * list to identify changes.
 * 
 * @executed Executed within the context of the display processing task.
 * 
 * @author 9/14/2011 Adrianusv
 *
 * @revisions
 *    9/14/2011 Initial release
 *    4/3/2012  Added passing of (false) into set routine to indiciate the
 *  			change was made locally so change bits are set properly.
 *  			Modified so all memory allocation, storing of changes, and
 *  			memory deallocation are handled within this routine.
 */
extern void SYSTEM_extract_and_store_changes_from_GuiVars( void )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	lsystem = mem_malloc( sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT) );

	// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
	// update just the variable that's potentially changed in the UI.
	memcpy( lsystem, SYSTEM_get_group_with_this_GID( g_GROUP_ID ), sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT) );

	// ----------
	
	strlcpy( lsystem->base.description, GuiVar_GroupName, sizeof(lsystem->base.description) );

	lsystem->in_use = GuiVar_SystemInUse;

	nm_SYSTEM_store_changes( lsystem, g_GROUP_creating_new, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

	// ----------

	mem_free( lsystem );

	// ----------

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	// Reset the Creating New flag to ensure we don't accidentally create
	// another new group when using the BACK key.
	g_GROUP_creating_new = (false);
}

/* ---------------------------------------------------------- */
static void MAINLINE_BREAK_extract_and_store_individual_group_change( const UNS_32 psystem_index_0, UNS_32 *pduring_irri, UNS_32 *pduring_mvor, UNS_32 *pall_other_times, UNS_32 *pvisible )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	if( *pvisible == (true) )
	{
		lsystem = mem_malloc( sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT) );

		// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
		// update just the variable that's potentially changed in the UI.
		memcpy( lsystem, SYSTEM_get_group_at_this_index( psystem_index_0 ), sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT) );

		// ----------

		lsystem->mlb_during_irri_gpm = *pduring_irri;
		lsystem->mlb_during_mvor_gpm = *pduring_mvor;
		lsystem->mlb_all_other_times_gpm = *pall_other_times;

		nm_SYSTEM_store_changes( lsystem, (false), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

		// ----------

		mem_free( lsystem );
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void MAINLINE_BREAK_extract_and_store_changes_from_GuiVars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	MAINLINE_BREAK_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_0, &GuiVar_GroupSettingB_0, &GuiVar_GroupSettingC_0, &GuiVar_GroupSetting_Visible_0 );
	MAINLINE_BREAK_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_1, &GuiVar_GroupSettingB_1, &GuiVar_GroupSettingC_1, &GuiVar_GroupSetting_Visible_1 );
	MAINLINE_BREAK_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_2, &GuiVar_GroupSettingB_2, &GuiVar_GroupSettingC_2, &GuiVar_GroupSetting_Visible_2 );
	MAINLINE_BREAK_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_3, &GuiVar_GroupSettingB_3, &GuiVar_GroupSettingC_3, &GuiVar_GroupSetting_Visible_3 );
}

/* ---------------------------------------------------------- */
static void SYSTEM_CAPACITY_extract_and_store_individual_group_change( const UNS_32 psystem_index_0, UNS_32 *pcapacity_in_use, UNS_32 *pwith_pump, UNS_32 *pwithout_pump, UNS_32 *pvisible )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	if( *pvisible == (true) )
	{
		lsystem = mem_malloc( sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT) );

		// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
		// update just the variable that's potentially changed in the UI.
		memcpy( lsystem, SYSTEM_get_group_at_this_index( psystem_index_0 ), sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT) );

		// ----------

		lsystem->capacity_in_use_bool = *pcapacity_in_use;
		lsystem->capacity_with_pump_gpm = *pwith_pump;
		lsystem->capacity_without_pump_gpm = *pwithout_pump;

		nm_SYSTEM_store_changes( lsystem, (false), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

		// ----------

		mem_free( lsystem );
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void SYSTEM_CAPACITY_extract_and_store_changes_from_GuiVars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	SYSTEM_CAPACITY_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_0, &GuiVar_GroupSettingB_0, &GuiVar_GroupSettingC_0, &GuiVar_GroupSetting_Visible_0 );
	SYSTEM_CAPACITY_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_1, &GuiVar_GroupSettingB_1, &GuiVar_GroupSettingC_1, &GuiVar_GroupSetting_Visible_1 );
	SYSTEM_CAPACITY_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_2, &GuiVar_GroupSettingB_2, &GuiVar_GroupSettingC_2, &GuiVar_GroupSetting_Visible_2 );
	SYSTEM_CAPACITY_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_3, &GuiVar_GroupSettingB_3, &GuiVar_GroupSettingC_3, &GuiVar_GroupSetting_Visible_3 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void FLOW_CHECKING_extract_and_store_changes_from_GuiVars( void )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lsystem = mem_malloc( sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT) );

	// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
	// update just the variable that's potentially changed in the UI.
	memcpy( lsystem, SYSTEM_get_group_with_this_GID( g_GROUP_ID ), sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT) );

	// ----------

	lsystem->flow_checking_in_use_bool = GuiVar_SystemFlowCheckingInUse;
	lsystem->flow_checking_ranges_gpm[ 0 ] = GuiVar_SystemFlowCheckingRange1;
	lsystem->flow_checking_ranges_gpm[ 1 ] = GuiVar_SystemFlowCheckingRange2;
	lsystem->flow_checking_ranges_gpm[ 2 ] = GuiVar_SystemFlowCheckingRange3;
	lsystem->flow_checking_tolerances_plus_gpm[ 0 ] = GuiVar_SystemFlowCheckingTolerancePlus1;
	lsystem->flow_checking_tolerances_plus_gpm[ 1 ] = GuiVar_SystemFlowCheckingTolerancePlus2;
	lsystem->flow_checking_tolerances_plus_gpm[ 2 ] = GuiVar_SystemFlowCheckingTolerancePlus3;
	lsystem->flow_checking_tolerances_plus_gpm[ 3 ] = GuiVar_SystemFlowCheckingTolerancePlus4;
	lsystem->flow_checking_tolerances_minus_gpm[ 0 ] = (UNS_32)abs(GuiVar_SystemFlowCheckingToleranceMinus1);
	lsystem->flow_checking_tolerances_minus_gpm[ 1 ] = (UNS_32)abs(GuiVar_SystemFlowCheckingToleranceMinus2);
	lsystem->flow_checking_tolerances_minus_gpm[ 2 ] = (UNS_32)abs(GuiVar_SystemFlowCheckingToleranceMinus3);
	lsystem->flow_checking_tolerances_minus_gpm[ 3 ] = (UNS_32)abs(GuiVar_SystemFlowCheckingToleranceMinus4);

	nm_SYSTEM_store_changes( lsystem, (false), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

	// ----------

	mem_free( lsystem );

	// ----------

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void MVOR_extract_and_store_changes_from_GuiVars( void )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	// ----------

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lsystem = mem_malloc( sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT) );

	// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
	// update just the variable that's potentially changed in the UI.
	memcpy( lsystem, SYSTEM_get_group_with_this_GID( g_GROUP_ID ), sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT) );

	// ----------

	lsystem->mvor_open_times[ 0 ] = (GuiVar_MVOROpen0 * 60);
	lsystem->mvor_open_times[ 1 ] = (GuiVar_MVOROpen1 * 60);
	lsystem->mvor_open_times[ 2 ] = (GuiVar_MVOROpen2 * 60);
	lsystem->mvor_open_times[ 3 ] = (GuiVar_MVOROpen3 * 60);
	lsystem->mvor_open_times[ 4 ] = (GuiVar_MVOROpen4 * 60);
	lsystem->mvor_open_times[ 5 ] = (GuiVar_MVOROpen5 * 60);
	lsystem->mvor_open_times[ 6 ] = (GuiVar_MVOROpen6 * 60);

	lsystem->mvor_close_times[ 0 ] = (GuiVar_MVORClose0 * 60);
	lsystem->mvor_close_times[ 1 ] = (GuiVar_MVORClose1 * 60);
	lsystem->mvor_close_times[ 2 ] = (GuiVar_MVORClose2 * 60);
	lsystem->mvor_close_times[ 3 ] = (GuiVar_MVORClose3 * 60);
	lsystem->mvor_close_times[ 4 ] = (GuiVar_MVORClose4 * 60);
	lsystem->mvor_close_times[ 5 ] = (GuiVar_MVORClose5 * 60);
	lsystem->mvor_close_times[ 6 ] = (GuiVar_MVORClose6 * 60);

	nm_SYSTEM_store_changes( lsystem, (false), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

	// ----------

	mem_free( lsystem );

	// ----------

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

// 3/22/2016 skc : Save UI code for Budgets per Mainline
// The variables set here should jibe with those used in nm_SYSTEM_BUDGET_store_changes()
extern void BUDGET_SETUP_extract_and_store_changes_from_GuiVars( void )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	// ----------

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	// 9/27/2018 Ryan : By default, set this flag to false. Will be switched to true if et
	// value is changed.
	g_BUDGET_using_et_calculate_budget = (false);

	lsystem = mem_malloc( sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT) );

	// 5/05/2016 skc : Use the new copy method started by ajv on 4/9/2016
	memcpy( lsystem, SYSTEM_get_group_with_this_GID( g_GROUP_ID ), sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT) );

	// ----------

	// 7/18/2016 skc : If the mainline has no POC's, then we don't allow changes to the budget
	// settings. The situation being prevented is the user creating a new mainline, attaching
	// the POC, then reattaching the POC to the original mainline. In this case, we don't want
	// the user to have to re-enable budgets.
	GuiVar_BudgetSysHasPOC = (SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid(nm_GROUP_get_group_ID(lsystem))->sbf.number_of_pocs_in_this_system > 0 ? 1 : 0);
	if( GuiVar_BudgetSysHasPOC == 1)
	{
		lsystem->budget_in_use = GuiVar_BudgetInUse;

		// 03/22/2016 skc : Convert from indices to actual months
		if( GuiVar_BudgetEntryOptionIdx == POC_BUDGET_GALLONS_ENTRY_OPTION_direct_entry )
		{
			if( GuiVar_BudgetPeriodsPerYearIdx == 0 )
			{ 
				lsystem->budget_number_of_annual_periods = IRRIGATION_SYSTEM_BUDGET_ANNUAL_PERIODS_MIN;
			}
			else
			{
				lsystem->budget_number_of_annual_periods = IRRIGATION_SYSTEM_BUDGET_ANNUAL_PERIODS_DEFAULT;
			}
		}
		// 8/9/2018 Ryan :When budget entry option set to annual, period should be set to 12.
		else 
		{
			lsystem->budget_number_of_annual_periods = IRRIGATION_SYSTEM_BUDGET_ANNUAL_PERIODS_DEFAULT;
		}

		// 03/02/2016 skc : No user interface, still need default value here!!!
		//lsystem->budget_meter_read_time = GuiVar_BudgetMeterReadTime * 60; 
		lsystem->budget_meter_read_time = IRRIGATION_SYSTEM_BUDGET_METER_READ_TIME_DEFAULT;

		// 3/24/2016 skc : This is a POC variable, but it is not used yet. Iterate all attached
		// POCs and change their setting. Use GuiVar_BudgetOptionIdx
		// 4/5/2016 skc : Hard coded value to POC_BUDGET_GALLONS_ENTRY_OPTION_DEFAULT
		for( UNS_32 i=0; i<MAX_POCS_IN_NETWORK; i++ )
		{
			const BY_POC_RECORD * const pbpr = &poc_preserves.poc[ i ];

			// 7/27/2016 skc : Can we ever have poc_gid = 0 AND a matching system_gid???
			// Find POCs enabled and attached to the system
			if( pbpr->poc_gid != 0 &&
				g_GROUP_ID == pbpr->this_pocs_system_preserves_ptr->system_gid )
			{
				POC_GROUP_STRUCT *ptr_poc = POC_get_group_at_this_index( POC_get_index_for_group_with_this_GID(pbpr->poc_gid) );

				nm_POC_set_budget_gallons_entry_option( ptr_poc,
														GuiVar_BudgetEntryOptionIdx, 
														CHANGE_generate_change_line, 
														CHANGE_REASON_KEYPAD, 
														FLOWSENSE_get_controller_index(), 
														CHANGE_set_change_bits, 
														POC_get_change_bits_ptr(ptr_poc, CHANGE_REASON_KEYPAD) );
														
				// 9/6/2018 Ryan : If Historical ET entry option selected, set all of the involved POCs ET
				// percentage to chosen value.
				if( GuiVar_BudgetEntryOptionIdx == POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_using_et )
				{

					nm_POC_set_budget_calculation_percent_of_et( ptr_poc,
															GuiVar_BudgetETPercentageUsed, 
															CHANGE_generate_change_line, 
															CHANGE_REASON_KEYPAD, 
															FLOWSENSE_get_controller_index(), 
															CHANGE_set_change_bits, 
															POC_get_change_bits_ptr(ptr_poc, CHANGE_REASON_KEYPAD) );
				}
			}
		}

		// 9/14/2018 Ryan : If the et percentage used is changed for any of the POCs in the system,
		// update the budget values. Values also updated when budget initially set to et entry mode.
		if( g_BUDGET_using_et_calculate_budget == (true) )
		{
			BUDGET_calc_POC_monthly_budget_using_et( lsystem, BUDGET_et_changed );
			
			g_BUDGET_using_et_calculate_budget = (false);
		}

		lsystem->budget_mode = GuiVar_BudgetModeIdx;

		nm_SYSTEM_store_changes( lsystem, (false), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

	}

	mem_free( lsystem );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
// 3/22/2016 skc : Save UI code for Budgets Flow Types per Mainline
extern void BUDGET_FLOW_TYPES_extract_and_store_changes_from_GuiVars( void )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	// ----------

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = mem_malloc( sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT) );

	// 5/05/2016 skc : Use the new copy method started by ajv on 4/9/2016
	memcpy( lsystem, SYSTEM_get_group_with_this_GID( g_GROUP_ID ), sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT) );

	// ----------

	// 4/12/2016 skc : These types of Irrigation flow are ALWAYS used
	lsystem->budget_flow_type[FLOW_TYPE_IRRIGATION] = true;
	lsystem->budget_flow_type[IN_LIST_FOR_PROGRAMMED_IRRIGATION] = (true);
	lsystem->budget_flow_type[IN_LIST_FOR_MANUAL_PROGRAM] = (true);
	lsystem->budget_flow_type[IN_LIST_FOR_MANUAL] = (true);
	lsystem->budget_flow_type[IN_LIST_FOR_WALK_THRU] = (true);
	lsystem->budget_flow_type[IN_LIST_FOR_TEST] = (true);
	lsystem->budget_flow_type[IN_LIST_FOR_MOBILE] = (true);

	lsystem->budget_flow_type[FLOW_TYPE_MVOR] = GuiVar_BudgetFlowTypeMVOR;
	lsystem->budget_flow_type[FLOW_TYPE_NON_CONTROLLER] = GuiVar_BudgetFlowTypeNonController;

	nm_SYSTEM_store_changes( lsystem, (false), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

	// ----------

	mem_free( lsystem );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Copies the group's name and settings into the Group List GuiVars to display
 * on the list of groups.
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 *  	  ensure the group whose value is being read isn't deleted during this
 *  	  process.
 *
 * @executed Executed within the context of the display processing task when the
 *           group list is displayed.
 * 
 * @param pindex_0 The 0-based index into the irrigation priorities group list.
 *
 * @author 9/14/2011 Adrianusv
 *
 * @revisions
 *    9/14/2011 Initial release
 */
extern void SYSTEM_load_system_name_into_scroll_box_guivar( const INT_16 pindex_0_i16 )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT *lsystem;

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	if( (UNS_32)pindex_0_i16 < SYSTEM_num_systems_in_use() )
	{
		lsystem = SYSTEM_get_group_at_this_index( (UNS_32)pindex_0_i16 );

		if( nm_OnList(&system_group_list_hdr, lsystem) == (true) )
		{
			strlcpy( GuiVar_itmGroupName, nm_GROUP_get_name( lsystem ), NUMBER_OF_CHARS_IN_A_NAME );
		}
		else
		{
			Alert_group_not_found();
		}
	}
	else if( (UNS_32)pindex_0_i16 == SYSTEM_num_systems_in_use() )
	{
		snprintf( GuiVar_itmGroupName, NUMBER_OF_CHARS_IN_A_NAME, "%s", GuiLib_GetTextPtr( GuiStruct_txtGroupDivider_0, 0 ) );
	}
	else
	{
		snprintf( GuiVar_itmGroupName, NUMBER_OF_CHARS_IN_A_NAME, " <%s %s>", GuiLib_GetTextPtr( GuiStruct_txtAddNew_0, 0 ), GuiLib_GetTextPtr( GuiStruct_txtMainlines_0, 0 ) );
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Writes the irrigation system's name to the screen based upon the passed-in
 * index. This generic function is used by the various by-system reports
 * including the System Report, System Preserves, Flow Recording, Flow Table,
 * and Flow Checking.
 * 
 * @executed Executed within the context of the display processing task when
 *  		 building reports; within the context of the key processing task
 *  		 when the user switches to a different system using the NEXT and
 *  		 PREV buttons on a report.
 * 
 * @param pindex_0 - The index of the group to retrieve.
 *
 * @author 7/17/2012 Adrianusv
 *
 * @revisions
 *    7/17/2012 Initial release
 */
extern void FDTO_SYSTEM_load_system_name_into_guivar( const UNS_32 pindex_0 )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	char*	lname;

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = SYSTEM_get_group_at_this_index( pindex_0 );

	lname = nm_GROUP_get_name( lsystem );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	if( lname != NULL )
	{
		strlcpy( GuiVar_GroupName, lname, sizeof(GuiVar_GroupName) );
	}
	else
	{
		GuiVar_GroupName[ 0 ] = 0x00;
	}
}

/* ---------------------------------------------------------- */
extern UNS_32 SYSTEM_get_inc_by_for_MLB_and_capacity( const UNS_32 prepeats )
{
	UNS_32	rv;

	if( prepeats > 64 )
	{
		rv = 10;
	}
	else if( prepeats > 32 )
	{
		rv = 5;
	}
	else
	{
		rv = 1;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern void *SYSTEM_get_group_with_this_GID( const UNS_32 pgroup_ID )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT *lsystem;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, pgroup_ID, (false) );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( lsystem );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the group with the specified group ID. This is a wrapper
 * for the nm_GROUP_get_ptr_to_group_at_this_location_in_list function so we
 * don't need to expose the list header.
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 * ensure the group that's found isn't deleted before it's used.
 *
 * @executed Executed within the context of the display processing task when the
 *  		 list and group screens are drawn; within the key processing task
 *  		 when the user navigates through the group list screen.
 * 
 * @param pindex_0 The index of the group to retrieve - 0 based.
 * 
 * @return void* A pointer to the group with the specified group ID. Or a NULL if the list
 * does not contain (index_0+1) items.
 *
 * @author 9/15/2011 Adrianusv
 *
 * @revisions
 *    9/15/2011 Initial release
 */
extern void *SYSTEM_get_group_at_this_index( const UNS_32 pindex_0 )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT *lsystem;

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_at_this_location_in_list( &system_group_list_hdr, pindex_0 );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return ( lsystem );
}

/* ---------------------------------------------------------- */
/**
 * Accepts a group ID (e.g., GuiVar_GroupID) and returns the position of that
 * index in the associated list. This is used to ensure the group the user
 * created or was editing is higlighted when they return to the list of groups.
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 *  	  ensure the list doesn't change between locating the index and using
 *  	  it.
 *
 * @executed Executed within the context of the key processing task when the
 *           user selects a group on the group list screen.
 * 
 * @param psystem_ID The group ID
 * 
 * @return UNS_32 A 0-based index of the requested group into the associated
 *                list.
 *
 * @author 12/15/2011 Adrianusv
 *
 * @revisions
 *    12/15/2011 Initial release
 */
extern UNS_32 SYSTEM_get_index_for_group_with_this_GID( const UNS_32 psystem_ID )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	rv = nm_GROUP_get_index_for_group_with_this_GID( &system_group_list_hdr, psystem_ID );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the last irrigation system group ID by retrieving the 
 * number_of_groups_ever_created value from the head of the list. 
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 *  	  ensure another group isn't added to the list between the number being
 *  	  retrieved and being used.
 *
 * @executed Executed within the context of the startup task when the stations 
 *  		 list is created for the first time; also executed within the
 *  		 display processing and key processing task as the user enters and
 *  		 leaves the editing screen.
 * 
 * @return UNS_32 The group ID of the last irrigation system group.
 *
 * @author 9/7/2011 Adrianusv
 *
 * @revisions
 *    9/7/2011   Initial release
 *    2/9/2012	 Renamed to include nm_ prefix
 */
extern UNS_32 SYSTEM_get_last_group_ID( void )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	rv = ((GROUP_BASE_DEFINITION_STRUCT*)system_group_list_hdr.phead)->number_of_groups_ever_created;

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the number of items in the list. This is a wrapper for the 
 * nm_ListGetCount macro so the list header doesn't need to be exposed. 
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 *  	  ensure a group isn't added or removed between the count being
 *  	  retrieved and being used.
 *
 * @executed Executed within the context of the display processing task when the 
 *  		 list of groups is drawn; within the context of the key processing
 *  		 task when the user moves up and down the list of groups and when
 *  		 the user tries to delete a group.
 * 
 * @return UNS_32 The number of items in the list.
 *
 * @author 9/7/2011 Adrianusv
 *
 * @revisions
 *    9/7/2011   Initial release
 *    2/9/2012	 Renamed to include nm_ prefix
*/
extern UNS_32 SYSTEM_get_list_count( void )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	rv = nm_ListGetCount( &system_group_list_hdr );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 SYSTEM_num_systems_in_use( void )
{
	// 3/14/2016 rmd : This function can be called from any task. There are no MUTEX
	// requirements for the caller. The functionality counts on the system list being ordered
	// such that all the 'in_use' systems come first in the list. We check the return value to
	// not exceed the MAX_POSSIBLE_SYSTEMS value. There should never be more than 4 systems
	// in_use at any one time. Also the caller may be counting on that fact to say index into
	// his array.
	
	// ----------
	
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	UNS_32	rv;
	
	BOOL_32	saw_one_not_in_use;

	// ----------

	rv = 0;

	// ----------

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	saw_one_not_in_use = (false);
	
	lsystem = nm_ListGetFirst( &system_group_list_hdr );

	while( lsystem != NULL )
	{
		if( lsystem->in_use )
		{
			++rv;

			if( saw_one_not_in_use )
			{
				// 5/20/2016 rmd : This signifies that we encountered a mainline in_use AFTER we encountered
				// one not in use. AJ said the order of the list would be guaranteed to have all the systems
				// in_use in the beginning of the list.
				Alert_Message( "Mainlines list unexpected order!" );
			}
		}
		else
		{
			saw_one_not_in_use = (true);
		}

		// ----------

		lsystem = nm_ListGetNext( &system_group_list_hdr, lsystem );
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	// ----------
	
	// 3/14/2016 rmd : Sanity check.
	if( rv > MAX_POSSIBLE_SYSTEMS )
	{
		Alert_Message( "More than 4 mainlines IN USE!" );
		
		// 3/14/2016 rmd : I think the safest return value is to indicate ONE system in use. Not 4!
		rv = 1;
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the whether a particular irrigation system is used for irrigation or
 * not. If the group isn't valid (e.g., is NULL or is not on the list), an alert
 * is generated and the default value is returned.
 * 
 * @executed 
 * 
 * @param psystem A pointer to the irrigation system.
 * 
 * @return BOOL_32 True if the system is used for irrigation; otherwise, false.
 *
 * @author 10/10/2011 Adrianusv
 *
 * @revisions
 *    10/10/2011 Initial release
 */
extern BOOL_32 SYSTEM_get_used_for_irri_bool( UNS_32 const psystem_gid )
{
	UNS_32	rv;

	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, psystem_gid, (false) );

	rv = SHARED_get_bool_32_bit_change_bits_group( lsystem,
												   &lsystem->used_for_irrigation_bool,
												   IRRIGATION_SYSTEM_USED_FOR_IRRI_DEFAULT,
												   &nm_SYSTEM_set_system_used_for_irri,
												   &lsystem->changes_to_send_to_master,
												   SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_used_with_irri_bit ] );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}
	
/* ---------------------------------------------------------- */
/**
 * Gets the mainline break during irrigation number for a particular irrigation
 * system. If the group isn't valid (e.g., is NULL or is not on the list), an
 * alert is generated and the default value is returned.
 * 
 * @executed 
 * 
 * @param psystem A pointer to the irrigation system.
 * 
 * @return UNS_32 The mainline break during irrigation number for the system.
 *
 * @author 10/10/2011 Adrianusv
 *
 * @revisions
 *    10/10/2011 Initial release
 */
extern UNS_32 SYSTEM_get_mlb_during_irri_gpm( const UNS_32 psystem_gid )
{
	UNS_32	rv;

	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, psystem_gid, (false) );

	rv = SHARED_get_uint32_32_bit_change_bits_group( lsystem,
													 &lsystem->mlb_during_irri_gpm,
													 IRRIGATION_SYSTEM_MLB_MIN,
													 IRRIGATION_SYSTEM_MLB_MAX,
													 IRRIGATION_SYSTEM_MLB_DURING_IRRI_DEFAULT,
													 &nm_SYSTEM_set_mlb_during_irri,
													 &lsystem->changes_to_send_to_master,
													 SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_during_irri_bit ] );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the mainline break during master valve override number for a particular
 * irrigation system. If the group isn't valid (e.g., is NULL or is not on the
 * list), an alert is generated and the default value is returned.
 * 
 * @executed 
 * 
 * @param psystem A pointer to the irrigation system.
 * 
 * @return UNS_32 The mainline break during master valve override number for the
 *                system.
 *
 * @author 10/10/2011 Adrianusv
 *
 * @revisions
 *    10/10/2011 Initial release
 */
extern UNS_32 SYSTEM_get_mlb_during_mvor_opened_gpm( const UNS_32 psystem_gid )
{
	UNS_32	rv;

	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, psystem_gid, (false) );

	rv = SHARED_get_uint32_32_bit_change_bits_group( lsystem,
													 &lsystem->mlb_during_mvor_gpm,
													 IRRIGATION_SYSTEM_MLB_MIN,
													 IRRIGATION_SYSTEM_MLB_MAX,
													 IRRIGATION_SYSTEM_MLB_DURING_MVOR_DEFAULT,
													 &nm_SYSTEM_set_mlb_during_mvor,
													 &lsystem->changes_to_send_to_master,
													 SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_during_mvor_bit ] );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the mainline break during all other times number for a particular
 * irrigation system. If the group isn't valid (e.g., is NULL or is not on the
 * list), an alert is generated and the default value is returned.
 * 
 * @executed 
 * 
 * @param psystem A pointer to the irrigation system.
 * 
 * @return UNS_32 The mainline break during all other times number for the
 *                system.
 *
 * @author 10/10/2011 Adrianusv
 *
 * @revisions
 *    10/10/2011 Initial release
 */
extern UNS_32 SYSTEM_get_mlb_during_all_other_times_gpm( const UNS_32 psystem_gid )
{
	UNS_32	rv;

	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, psystem_gid, (false) );

	rv = SHARED_get_uint32_32_bit_change_bits_group( lsystem,
													 &lsystem->mlb_all_other_times_gpm,
													 IRRIGATION_SYSTEM_MLB_MIN,
													 IRRIGATION_SYSTEM_MLB_MAX,
													 IRRIGATION_SYSTEM_MLB_ALL_OTHER_DEFAULT,
													 &nm_SYSTEM_set_mlb_all_other_times,
													 &lsystem->changes_to_send_to_master,
													 SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_all_other_times_bit ] );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets whether a particular irrigation uses system capacity or not. If the
 * group isn't valid (e.g., is NULL or is not on the list), an alert is
 * generated and the default value is returned.
 * 
 * @executed 
 * 
 * @param psystem A pointer to the irrigation system.
 * 
 * @return BOOL_32 True if the irrigation system uses system capacity; otherwise,
 *               false.
 *
 * @author 10/10/2011 Adrianusv
 *
 * @revisions
 *    10/10/2011 Initial release
 */
extern BOOL_32 SYSTEM_get_capacity_in_use_bool( const UNS_32 psystem_gid )
{
	BOOL_32	rv;

	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, psystem_gid, (false) );

	rv = SHARED_get_bool_32_bit_change_bits_group( lsystem,
												   &lsystem->capacity_in_use_bool,
												   IRRIGATION_SYSTEM_CAPACITY_IN_USE_DEFAULT,
												   &nm_SYSTEM_set_capacity_in_use,
												   &lsystem->changes_to_send_to_master,
												   SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_in_use_bit ] );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the system capacity with pump number for a particular irrigation system.
 * If the group isn't valid (e.g., is NULL or is not on the list), an alert is
 * generated and the default value is returned.
 * 
 * @executed 
 * 
 * @param psystem A pointer to the irrigation system.
 * 
 * @return UNS_32 The system capacity with pump number for the system.
 *
 * @author 10/10/2011 Adrianusv
 *
 * @revisions
 *    10/10/2011 Initial release
 */
extern UNS_32 SYSTEM_get_capacity_with_pump_gpm( const UNS_32 psystem_gid )
{
	UNS_32	rv;

	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, psystem_gid, (false) );

	rv = SHARED_get_uint32_32_bit_change_bits_group( lsystem,
													 &lsystem->capacity_with_pump_gpm,
													 IRRIGATION_SYSTEM_CAPACITY_MIN,
													 IRRIGATION_SYSTEM_CAPACITY_MAX,
													 IRRIGATION_SYSTEM_CAPACITY_PUMP_DEFAULT,
													 &nm_SYSTEM_set_capacity_with_pump,
													 &lsystem->changes_to_send_to_master,
													 SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_with_pump_bit ] );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the system capacity without pump number for a particular irrigation
 * system. If the group isn't valid (e.g., is NULL or is not on the list), an
 * alert is generated and the default value is returned.
 * 
 * @executed 
 * 
 * @param psystem A pointer to the irrigation system.
 * 
 * @return UNS_32 The system capacity without pump number for the system.
 *
 * @author 10/10/2011 Adrianusv
 *
 * @revisions
 *    10/10/2011 Initial release
 */
extern UNS_32 SYSTEM_get_capacity_without_pump_gpm( const UNS_32 psystem_gid )
{
	UNS_32	rv;

	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, psystem_gid, (false) );

	rv = SHARED_get_uint32_32_bit_change_bits_group( lsystem,
													 &lsystem->capacity_without_pump_gpm,
													 IRRIGATION_SYSTEM_CAPACITY_MIN,
													 IRRIGATION_SYSTEM_CAPACITY_MAX,
													 IRRIGATION_SYSTEM_CAPACITY_NONPUMP_DEFAULT,
													 &nm_SYSTEM_set_capacity_without_pump,
													 &lsystem->changes_to_send_to_master,
													 SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_without_pump_bit ] );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * This function returns whether or not flow checking is enabled or not for the
 * passed system gid. For EACH system, as part of developing the system average
 * flow rate, this function is called at a 1 Hz rate. But ONLY for systems with
 * a flow meter. A copy of the return value is used to make a setting in the
 * system_preserve entry. Also called during the valve flow check function itself.
 * 
 * @executed Executed within the context of the TD_CHECK task as described in
 * the description of this function.
 * 
 * @param psystem_gid The group ID of a particular irrigation system.
 * 
 * @return BOOL_32 True if the system with the passd in group ID is using flow
 *               checking; otherwise, false.
 *
 * @author 1/31/2012 BobD
 *
 * @revisions
 *    1/31/2012 Initial release
 */
extern BOOL_32 SYSTEM_get_flow_checking_in_use( const UNS_32 psystem_gid )
{
	BOOL_32	rv;

	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, psystem_gid, (false) );

	rv = SHARED_get_bool_32_bit_change_bits_group( lsystem,
												   &lsystem->flow_checking_in_use_bool,
												   IRRIGATION_SYSTEM_FLOW_CHECKING_IN_USE_DEFAULT,
												   &nm_SYSTEM_set_flow_checking_in_use,
												   &lsystem->changes_to_send_to_master,
												   SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_in_use_bit ] );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern void SYSTEM_get_flow_check_ranges_gpm( const UNS_32 psystem_gid, UNS_32 *parray )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	char	lfield_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32	i;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, psystem_gid, (false) );

	if( lsystem != NULL )
	{
		for( i = 0; i < IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_RANGES; ++i )
		{
			snprintf( lfield_name, sizeof(lfield_name), "%s%d", SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_ranges_bit ], (i + 1) );

			// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
			// the value by setting it to the default.
			if( RangeCheck_uint32( &lsystem->flow_checking_ranges_gpm[ i ], IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_MIN, IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_MAX, IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS[ i ], nm_GROUP_get_name( lsystem ), lfield_name ) == (false) )
			{
				nm_SYSTEM_set_flow_checking_range( lsystem,
												   i,
												   lsystem->flow_checking_ranges_gpm[ i ],
												   CHANGE_do_not_generate_change_line,
												   CHANGE_REASON_RANGE_CHECK_FAILED,
												   FLOWSENSE_get_controller_index(),
												   CHANGE_set_change_bits,
												   &lsystem->changes_to_send_to_master );
			}
		}

		memcpy( parray, &(lsystem->flow_checking_ranges_gpm), (IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_RANGES * sizeof(UNS_32)) );
	}
	else
	{
		Alert_group_not_found();

		for( i = 0; i < IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_RANGES; ++i )
		{
			parray[ i ] = IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS[ i ];
		}
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void SYSTEM_get_flow_check_tolerances_plus_gpm( const UNS_32 psystem_gid, UNS_32 *parray )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	char	lfield_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32	i;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, psystem_gid, (false) );

	if( lsystem != NULL )
	{
		for( i = 0; i < IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_TOLERANCES; ++i )
		{
			snprintf( lfield_name, sizeof(lfield_name), "%s%d", SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_tolerances_plus_bit ], (i + 1) );

			// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
			// the value by setting it to the default.
			if( RangeCheck_uint32( &lsystem->flow_checking_tolerances_plus_gpm[ i ], IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MIN, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MAX, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS[ i ], nm_GROUP_get_name( lsystem ), lfield_name ) == (false) )
			{
				nm_SYSTEM_set_flow_checking_tolerance_plus( lsystem,
															i,
															lsystem->flow_checking_tolerances_plus_gpm[ i ],
															CHANGE_do_not_generate_change_line,
															CHANGE_REASON_RANGE_CHECK_FAILED,
															FLOWSENSE_get_controller_index(),
															CHANGE_set_change_bits,
															&lsystem->changes_to_send_to_master );
			}
		}

		memcpy( parray, &(lsystem->flow_checking_tolerances_plus_gpm), (IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_TOLERANCES * sizeof(UNS_32)) );
	}
	else
	{
		Alert_group_not_found();

		for( i = 0; i < IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_TOLERANCES; ++i )
		{
			parray[ i ] = IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS[ i ];
		}
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void SYSTEM_get_flow_check_tolerances_minus_gpm( const UNS_32 psystem_gid, UNS_32 *parray )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	char	lfield_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32	i;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, psystem_gid, (false) );

	if( lsystem != NULL )
	{
		for( i = 0; i < IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_TOLERANCES; ++i )
		{
			snprintf( lfield_name, sizeof(lfield_name), "%s%d", SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_tolerances_minus_bit ], (i + 1) );

			// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
			// the value by setting it to the default.
			if( RangeCheck_uint32( &lsystem->flow_checking_tolerances_minus_gpm[ i ], IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MIN, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MAX, IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS[ i ], nm_GROUP_get_name( lsystem ), lfield_name ) == (false) )
			{
				nm_SYSTEM_set_flow_checking_tolerance_minus( lsystem,
															 i,
															 lsystem->flow_checking_tolerances_minus_gpm[ i ],
															 CHANGE_do_not_generate_change_line,
															 CHANGE_REASON_RANGE_CHECK_FAILED,
															 FLOWSENSE_get_controller_index(),
															 CHANGE_set_change_bits,
															 &lsystem->changes_to_send_to_master );
			}
		}

		memcpy( parray, &(lsystem->flow_checking_tolerances_minus_gpm), (IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_TOLERANCES * sizeof(UNS_32)) );
	}
	else
	{
		Alert_group_not_found();

		for( i = 0; i < IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_TOLERANCES; ++i )
		{
			parray[ i ] = IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS[ i ];
		}
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This function returns the valve cycle count REQUIREMENT as part of derate
    table cell development. Called from the valve flow checking function as part of the
    decision to check flow or not.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task. At a 1Hz rate as part of the
    flow checking function.
	
    @RETURN The valve cycle count requirement for the system GID paramter. If the gid cannot
    be found the default value is returned.

	@ORIGINAL 2012.05.31 rmd

	@REVISIONS (none)
*/
/* ---------------------------------------------------------- */
extern UNS_32 SYSTEM_get_required_station_cycles_before_flow_checking( const UNS_32 psystem_gid )
{
	UNS_32	rv;

	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, psystem_gid, (false) );

	rv = SHARED_get_uint32_32_bit_change_bits_group( lsystem,
													 &lsystem->derate_table_before_checking_number_of_station_cycles,
													 IRRIGATION_SYSTEM_DERATE_STATION_CYCLES_MIN,
													 IRRIGATION_SYSTEM_DERATE_STATION_CYCLES_MAX,
													 IRRIGATION_SYSTEM_DERATE_STATION_CYCLES_DEFAULT,
													 NULL,
													 &lsystem->changes_to_send_to_master,
													 "DerateNumberOfStation" );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This function returns the cell iteration count REQUIREMENT as part of
    derate table cell development. Called from the valve flow checking function as part of
    the decision to check flow or not.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task. At a 1Hz rate as part of the
    flow checking function.
	
    @RETURN The cell iteration count requirement for the system GID parameter. If the gid
    cannot be found the default value is returned.

	@ORIGINAL 2012.05.31 rmd

	@REVISIONS (none)
*/
/* ---------------------------------------------------------- */
extern UNS_32 SYSTEM_get_required_cell_iterations_before_flow_checking( const UNS_32 psystem_gid )
{
	UNS_32	rv;

	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, psystem_gid, (false) );

	rv = SHARED_get_uint32_32_bit_change_bits_group( lsystem,
													 &lsystem->derate_table_before_checking_number_of_cell_iterations,
													 IRRIGATION_SYSTEM_DERATE_CELL_ITERATIONS_MIN,
													 IRRIGATION_SYSTEM_DERATE_CELL_ITERATIONS_MAX,
													 IRRIGATION_SYSTEM_DERATE_CELL_ITERATIONS_DEFAULT,
													 NULL,
													 &lsystem->changes_to_send_to_master,
													 "DerateNumberOfCell" );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SYSTEM_get_allow_table_to_lock( const UNS_32 psystem_gid )
{
	BOOL_32	rv;

	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, psystem_gid, (false) );

	rv = SHARED_get_bool_32_bit_change_bits_group( lsystem,
												   &lsystem->derate_table_allowed_to_lock,
												   IRRIGATION_SYSTEM_DERATE_ALLOWED_TO_LOCK_DEFAULT,
												   NULL,
												   &lsystem->changes_to_send_to_master,
												   "DerateAllowedToLock" );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 SYSTEM_get_derate_table_gpm_slot_size( const UNS_32 psystem_gid )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	UNS_32	rv;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, psystem_gid, (false) );

	// 6/6/2012 ajv : TODO What should the limits of the slot size be? We ran into the 20 gpm
	// cap in the ET2000e, but that was with a much smaller table. For now, we'll use the old
	// cap of 20.
	rv = SHARED_get_uint32_32_bit_change_bits_group( lsystem,
													 &lsystem->derate_table_gpm_slot_size,
													 1,
													 20,
													 IRRIGATION_SYSTEM_DERATE_GPM_SLOT_SIZE_DEFAULT,
													 NULL,
													 &lsystem->changes_to_send_to_master,
													 "DerateSlotSize" );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 SYSTEM_get_derate_table_max_stations_ON( const UNS_32 psystem_gid )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	UNS_32	rv;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, psystem_gid, (false) );

	// 6/6/2012 ajv : TODO What should the limits of the max stations on be? In the ET2000e, we
	// limited the max on to 24, so perhaps use that? Or should we allow up to the 72 that it
	// currently supports?
	rv = SHARED_get_uint32_32_bit_change_bits_group( lsystem,
													 &lsystem->derate_table_max_stations_ON,
													 1,
													 24,
													 IRRIGATION_SYSTEM_DERATE_MAX_STATIONS_ON_DEFAULT,
													 NULL,
													 &lsystem->changes_to_send_to_master,
													 "DerateMaxStationOn" );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 SYSTEM_get_derate_table_number_of_gpm_slots( const UNS_32 psystem_gid )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	UNS_32	rv;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, psystem_gid, (false) );

	// 6/6/2012 ajv : TODO What should the limits of the max stations on be? In the ET2000e, we
	// limited the max on to 24, so perhaps use that? Or should we allow up to the 72 that it
	// currently supports?
	rv = SHARED_get_uint32_32_bit_change_bits_group( lsystem,
													 &lsystem->derate_table_number_of_gpm_slots,
													 0,
													 IRRIGATION_SYSTEM_DERATE_NUMBER_OF_GPM_SLOTS_DEFAULT,
													 IRRIGATION_SYSTEM_DERATE_NUMBER_OF_GPM_SLOTS_DEFAULT,
													 NULL,
													 &lsystem->changes_to_send_to_master,
													 "DerateNumberOfSlots" );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern CHANGE_BITS_32_BIT *SYSTEM_get_change_bits_ptr( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem, const UNS_32 pchange_reason )
{
	return( SHARED_get_32_bit_change_bits_ptr( pchange_reason, &psystem->changes_to_send_to_master, &psystem->changes_to_distribute_to_slaves, &psystem->changes_to_upload_to_comm_server ) );
}

/* ---------------------------------------------------------- */
extern void SYSTEM_clean_house_processing( void )
{
	// 4/16/2014 rmd : Executed within the context of the comm_mngr task. ONLY BY SLAVES. THE
	// CHANGES MADE HERE DO NOT BE DISTRIBUTED. THE STATION LIST IS BEING WIPED IN PREPARATION
	// OF BEING ADDED AS A NEW MEMBER TO A CHAIN. Called when the result of a scan indicates
	// this controller is a new slave in the chain. Used when building up chains. Likely ONLY
	// used when assembling a chain for the first time or when increasing the length of a chain
	// to a length it has never 'run' at before. Or if you changed the LETTER of a slave in a
	// chain.
	
	void	*tptr;
	
	// ----------

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	tptr = nm_ListRemoveHead( &system_group_list_hdr );

	while( tptr != NULL )
	{
		mem_free( tptr );

		tptr = nm_ListRemoveHead( &system_group_list_hdr );
	}

	// ----------

	// 4/16/2014 rmd : By setting the no groups exist flag to false no change bits will be set
	// for this 'dummy' group. And that is what we want.
	nm_GROUP_create_new_group( &system_group_list_hdr, (char*)IRRIGATION_SYSTEM_DEFAULT_NAME, &nm_SYSTEM_set_default_values, sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT), (false), NULL );

	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE, IRRIGATION_SYSTEM_FILENAME, IRRIGATION_SYSTEM_LATEST_FILE_REVISION, &system_group_list_hdr, sizeof( IRRIGATION_SYSTEM_GROUP_STRUCT ), list_system_recursive_MUTEX, FF_IRRIGATION_SYSTEM );

	// ----------

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	// ----------

	// 5/13/2014 ajv : Since we've removed and re-added a system, synchronize to
	// the system preserves
	SYSTEM_PRESERVES_synchronize_preserves_to_file();
}

/* ---------------------------------------------------------- */
extern void IRRIGATION_SYSTEM_set_bits_on_all_systems_to_cause_distribution_in_the_next_token( void )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	// ----------

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_ListGetFirst( &system_group_list_hdr );

	while( lsystem != NULL )
	{
		// 5/1/2014 rmd : This function ONLY being called at the conclusion of a SCAN by the master
		// of a real multiple controller chain - we want to broadcast to the slaves using tokens
		// about all the hardware we presently know about. In this case the stations.
		SHARED_set_all_32_bit_change_bits( &lsystem->changes_to_distribute_to_slaves, list_system_recursive_MUTEX );

		lsystem = nm_ListGetNext( &system_group_list_hdr, lsystem );
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	// ----------

	// 4/29/2014 ajv : Set a flag in the changes structure to notify this
	// controller to examine its lists for changes to distribute to the other
	// controllers in the chain in it's next token resposne. The flag is part of
	// the mutex protected comm_mngr struct so take the MUTEX.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	comm_mngr.changes.distribute_changes_to_slave = (true);

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void SYSTEM_on_all_systems_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_ListGetFirst( &system_group_list_hdr );

	while( lsystem != NULL )
	{
		if( pset_or_clear == COMMSERVER_CHANGE_BITS_ALL_CLEARED )
		{
			SHARED_clear_all_32_bit_change_bits( &lsystem->changes_to_upload_to_comm_server, list_system_recursive_MUTEX );
		}
		else
		{
			SHARED_set_all_32_bit_change_bits( &lsystem->changes_to_upload_to_comm_server, list_system_recursive_MUTEX );
		}

		lsystem = nm_ListGetNext( &system_group_list_hdr, lsystem );
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * @mutex_requirements Requires the caller to have the pending_changes_recursive_MUTEX to
 * ensure no other function tries to modify the pending_changes_to_send_to_comm_server flag.
 *
 * @task_execution Executed within the context of the CONTROLLER INITIATED task when it
 * first starts and when the waiting response flags are cleared after a communication.
 */
extern void nm_SYSTEM_update_pending_change_bits( const UNS_32 pcomm_error_occurred )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	BOOL_32	lfile_save_necessary;

	lfile_save_necessary = (false);

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_ListGetFirst( &system_group_list_hdr );

	while( lsystem != NULL )
	{
		// 4/30/2015 ajv : No reason to process this if none of the bits are set. This also helps
		// reduce the file save activity.
		if( lsystem->changes_uploaded_to_comm_server_awaiting_ACK != CHANGE_BITS_32_BIT_ALL_CLEAR )
		{
			// 4/29/2015 ajv : If a communication error occurred while sending Program Data, copy the
			// bits of variables that were sent back into the original bit field to ensure they're sent
			// again during the next attempt. Otherwise, clear the pending bits.
			if( pcomm_error_occurred )
			{
				lsystem->changes_to_upload_to_comm_server |= lsystem->changes_uploaded_to_comm_server_awaiting_ACK;

				// 4/30/2015 ajv : Since we've determined there were changes that were not successfully
				// sent, set the flag to trigger another attempt
				weather_preserves.pending_changes_to_send_to_comm_server = (true);

				// 4/30/2015 ajv : Also set the timer to force the data to send. If the timer's already
				// running, this will simply restart it.
				// 
				// 4/30/2015 ajv : BECAUSE of the potential that this function is executed many times (once
				// for each item changed) we could not start our pdata timer with a post to the CI queue.
				// The queue would overflow. So start the timer directly.
				xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGES_NOT_SENT_MS ), portMAX_DELAY );
			}
			else
			{
				SHARED_clear_all_32_bit_change_bits( &lsystem->changes_uploaded_to_comm_server_awaiting_ACK, list_system_recursive_MUTEX );
			}

			// 4/30/2015 ajv : At least one system was updated. So make sure we save the file.
			lfile_save_necessary = (true);
		}

		lsystem = nm_ListGetNext( &system_group_list_hdr, lsystem );
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	if( lfile_save_necessary )
	{
		// 4/29/2015 ajv : Now that the bits are set or cleared accordingly, trigger a file save.
		// If power fails between building the message and receiving the ACK, the original bit field
		// hasn't been saved to flash yet, so it will resend the associated variables during the
		// next attempt.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_IRRIGATION_SYSTEM, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
	}
}

/* ---------------------------------------------------------- */
extern BOOL_32 SYSTEM_at_least_one_system_has_flow_checking( void )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	BOOL_32	rv;

	rv = (false);

	// ----------

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_ListGetFirst( &system_group_list_hdr );

	while( lsystem != NULL )
	{
		if( lsystem->flow_checking_in_use_bool )
		{
			rv = (true);

			// 6/1/2015 ajv : We've found one, so no need to continue looping through the rest of the
			// systems.
			break;
		}
		lsystem = nm_ListGetNext( &system_group_list_hdr, lsystem );
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Updates the GuiVar_StatusSystemFlowStatus variable which is used to display
 * the flow checking state (e.g., "Waiting", "Checking", etc.).
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the Status or Irrigation Details screens are updated.
 * 
 * @param pindex_0 The index of the system, 0-based.
 *
 * @author 10/14/2013 AdrianusV
 *
 * @revisions (none)
 */
extern UNS_32 SYSTEM_get_flow_checking_status( const UNS_32 pindex_0 )
{
	#define FLOW_CHECKING_NONE_ON 				(0)
	#define FLOW_CHECKING_NOT_GOING_TO_CHECK	(1)
	#define FLOW_CHECKING_WAITING 				(2)
	#define FLOW_CHECKING_CHECKING				(3)
	#define FLOW_CHECKING_LEARNING				(4)
	#define FLOW_CHECKING_LEARNED				(5)
	#define FLOW_CHECKING_ACQUIRING 			(6)

	// ----------

	UNS_32	rv;

	rv = FLOW_CHECKING_NONE_ON;

	// ----------

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	if( system_preserves.system[ pindex_0 ].sbf.system_level_no_valves_ON_therefore_no_flow_checking )
	{
		rv = FLOW_CHECKING_NONE_ON;
	}
	else if( system_preserves.system[ pindex_0 ].sbf.system_level_valves_are_ON_but_will_not_be_checking_flow )
	{
		rv = FLOW_CHECKING_NOT_GOING_TO_CHECK;
	}
	else if( system_preserves.system[ pindex_0 ].sbf.system_level_valves_are_ON_and_actively_checking )
	{
		rv = FLOW_CHECKING_CHECKING;
	}
	else if( system_preserves.system[ pindex_0 ].sbf.system_level_valves_are_ON_and_waiting_to_update_derate_table )
	{
		rv = FLOW_CHECKING_LEARNING;
	}
	else if( system_preserves.system[ pindex_0 ].sbf.system_level_valves_are_ON_and_has_updated_the_derate_table )
	{
		rv = FLOW_CHECKING_LEARNED;
	}
	else if( system_preserves.system[ pindex_0 ].sbf.system_level_valves_are_ON_and_waiting_to_acquire_expected )
	{
		rv = FLOW_CHECKING_ACQUIRING;
	}
	else if( system_preserves.system[ pindex_0 ].sbf.system_level_valves_are_ON_and_waiting_to_check_flow )
	{
		rv = FLOW_CHECKING_WAITING;
	}
	else
	{
		rv = FLOW_CHECKING_NONE_ON;
	}

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 SYSTEM_get_highest_flow_checking_status_for_any_system( void )
{
	UNS_32	status;

	UNS_32	rv;

	UNS_32	i;

	// ----------

	status = 0;	// FLOW_CHECKING_NONE_ON

	rv = status;
	
	// ----------

	// 6/24/2015 ajv : Loop through each mainline and keep only highest flow checking reason.
	for( i = 0; i < MAX_POSSIBLE_SYSTEMS; ++i )
	{
		status = SYSTEM_get_flow_checking_status( i );

		if( status > rv )
		{
			rv = status;
		}
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Initiates the process to clear a mainline break for the specified system.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user tries to clear a mainline break from the Status screen or the System
 * Flow Live Screen.
 * 
 * @param pindex_0 The index of the system whose mainline break is to be cleared
 * - 0-based.
 *
 * @author 6/18/2012 AdrianusV
 *
 * @revisions
 *   10/14/2013 ajv : Updated to include a passed-in system index rather than a
 *   global variable which indicated the currently selected system.
 */
extern void SYSTEM_clear_mainline_break( const UNS_32 psystem_GID )
{
	BY_SYSTEM_RECORD	*lpreserve;

	UNS_32	i;

	// ----------

	// 6/19/2012 ajv : Always take the preserves mutex before taking the list mutex. We may be
	// able to get around needing the list mutex here by building the entire screen using the
	// perserves, but I'm not sure how we'd deal with getting the name at the moment. Will
	// reevaluate later.
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	lpreserve = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( psystem_GID );

	if( lpreserve != NULL )
	{
		if( SYSTEM_PRESERVES_there_is_a_mlb( &(lpreserve->delivered_mlb_record) ) )
		{
			// 5/14/2015 ajv : Search for and insert the GID into an available slot within the
			// system_gids_to_clear_mlbs_for structure
			for( i = 0; i < MAX_POSSIBLE_SYSTEMS; ++i )
			{
				if( irri_comm.system_gids_to_clear_mlbs_for[ i ] == 0 )
				{
					irri_comm.system_gids_to_clear_mlbs_for[ i ] = (UNS_16)lpreserve->system_gid;

					// 5/14/2015 ajv : Since we've added the GID to an available slot, break out of the loop.
					break;
				}
			}
		}
	}
	else
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "System not found" );
	}

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void SYSTEM_check_for_mvor_schedule_start( const DATE_TIME_COMPLETE_STRUCT *const pdtcs_ptr )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	char	lsystem_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	// ----------

	// 6/19/2012 ajv : Always take the preserves mutex before taking the list mutex. We may be
	// able to get around needing the list mutex here by building the entire screen using the
	// perserves, but I'm not sure how we'd deal with getting the name at the moment. Will
	// reevaluate later.
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lsystem = nm_ListGetFirst( &system_group_list_hdr );

	while( lsystem != NULL )
	{
		if( lsystem->mvor_open_times[ pdtcs_ptr->__dayofweek ] == pdtcs_ptr->date_time.T )
		{
			strlcpy( lsystem_name, nm_GROUP_get_name(lsystem), sizeof(lsystem_name) );

			// 10/9/2015 rmd : Some notes - we can only SCHEDULE the MV to OPEN. Cannot schedule it to
			// close. And apparently we must have a CLOSE time in place on the same day to accompany the
			// OPEN time. The implication here is you cannot schedule a MVOR opening to cross midnight.
			// I think that is just fine.
			if( lsystem->mvor_close_times[ pdtcs_ptr->__dayofweek ] == SCHEDULE_OFF_TIME )
			{
				Alert_MVOR_skipped( lsystem_name, SKIPPED_DUE_TO_MVOR_NO_CLOSE_TIME );
			}
			else if ( lsystem->mvor_close_times[ pdtcs_ptr->__dayofweek ] == lsystem->mvor_open_times[ pdtcs_ptr->__dayofweek ] )
			{
				// 8/26/2015 ajv : Don't open it and notify the user so they know why their scheduled master
				// valve override didn't run. This was carried over from a recent change in the ET2000e.
				Alert_MVOR_skipped( lsystem_name, SKIPPED_DUE_TO_MVOR_SAME_OPEN_CLOSE_TIMES );
			}
			else if ( lsystem->mvor_close_times[ pdtcs_ptr->__dayofweek ] > lsystem->mvor_open_times[ pdtcs_ptr->__dayofweek ] )
			{
				// 8/26/2015 ajv : In order to allow users to effectively cross midnight without output
				// chatter, add 15-seconds to the length of time it will remain open. This way, a guy could
				// open it one day and close it the next without output chatter. 15-seconds should be plenty
				// of time for token travel time.
				FOAL_IRRI_initiate_or_cancel_a_master_valve_override( nm_GROUP_get_group_ID(lsystem), MVOR_ACTION_OPEN_MASTER_VALVE, ((lsystem->mvor_close_times[ pdtcs_ptr->__dayofweek ] - lsystem->mvor_open_times[ pdtcs_ptr->__dayofweek ]) + 15), INITIATED_VIA_MVOR_SCHEDULE );
			}
			else
			{
				Alert_MVOR_skipped( lsystem_name, SKIPPED_DUE_TO_MVOR_CLOSE_BEFORE_OPEN );
			}
		}

		lsystem = nm_ListGetNext( &system_group_list_hdr, lsystem );
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SYSTEM_this_system_is_in_use( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem_ptr )
{
	UNS_32	rv;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	// 10/29/2015 rmd : I do find it ODD that if this GET fails its range check that the default
	// we use to bring the in_use variable back into range is (true). Then the calling function
	// would go ahead and USE this system [mainline]. It's just interesting to me. I would think
	// if the in_use was out of range we might fix it and NOT want to rely on the rest of the
	// system variables to perform potentially critical operations like MLB checking!
	rv = SHARED_get_bool_32_bit_change_bits_group(	psystem_ptr,
													&psystem_ptr->in_use,
													IRRIGATION_SYSTEM_IN_USE_DEFAULT,
													&nm_SYSTEM_set_system_in_use,
													&psystem_ptr->changes_to_send_to_master,
													SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_in_use_bit ] );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the budget use choice for a particular irrigation system. If the group isn't valid
 * (e.g., is NULL or is not on the list), an alert is generated and the default value is
 * returned.
 * 
 * @executed 
 * 
 * @param psystem_gid The GID of the system we want the information for.
 * 
 * @return UNS_32 The budget use choice for the system.
 *
 * @author 10/9/2015 bobd
 *
 * @revisions
 *    10/9/2015 Initial release
 *    03/22/2016 skc : Changed name and return type
 */
extern BOOL_32 SYSTEM_get_budget_in_use( const UNS_32 psystem_gid )
{
	UNS_32	rv;

	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	// To ensure the group isn't deleted between retrieving and copying this
	// setting, take the list_system_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	lsystem = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, psystem_gid, (false) );

	rv = SHARED_get_bool_32_bit_change_bits_group( lsystem,
													 &lsystem->budget_in_use,
													 IRRIGATION_SYSTEM_BUDGET_IN_USE_NO,
													 &nm_SYSTEM_set_budget_in_use,
													 &lsystem->changes_to_send_to_master,
													 SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_in_use_bit ] );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
// 12/22/2015 skc: Return a copy of the data pertaining to budgets
// The portion of the IRRIGATION_SYSTEM_GROUP_STRUCT is copied into the BUDGET_DETAILS_STRUCT
// pbds_ptr is allocated by the caller
extern void SYSTEM_get_budget_details( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem_ptr, BUDGET_DETAILS_STRUCT *pbds_ptr )
{
	UNS_32	i;
	
	// ----------
	
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );
	
	// 10/29/2015 rmd : Clear the output parameter.
	memset( pbds_ptr, 0x00, sizeof(BUDGET_DETAILS_STRUCT) );
	
	pbds_ptr->in_use = SHARED_get_bool_32_bit_change_bits_group(	psystem_ptr,
																		&psystem_ptr->budget_in_use,
																		IRRIGATION_SYSTEM_BUDGET_IN_USE_NO,
																		&nm_SYSTEM_set_budget_in_use,
																		&psystem_ptr->changes_to_send_to_master,
																		SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_in_use_bit ] );
																		
	pbds_ptr->meter_read_time = SHARED_get_uint32_32_bit_change_bits_group(	psystem_ptr,
																			&psystem_ptr->budget_meter_read_time,
																			IRRIGATION_SYSTEM_BUDGET_METER_READ_TIME_MIN,
																			IRRIGATION_SYSTEM_BUDGET_METER_READ_TIME_MAX,
																			IRRIGATION_SYSTEM_BUDGET_METER_READ_TIME_DEFAULT,
																			&nm_SYSTEM_set_budget_meter_read_time,
																			&psystem_ptr->changes_to_send_to_master,
																			SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_meter_read_time_bit ] );
	
	pbds_ptr->number_of_annual_periods = SHARED_get_uint32_32_bit_change_bits_group( psystem_ptr,
																			&psystem_ptr->budget_number_of_annual_periods,
																			IRRIGATION_SYSTEM_BUDGET_ANNUAL_PERIODS_MIN,
																			IRRIGATION_SYSTEM_BUDGET_ANNUAL_PERIODS_MAX,
																			IRRIGATION_SYSTEM_BUDGET_ANNUAL_PERIODS_DEFAULT,
																			&nm_SYSTEM_set_budget_number_of_annual_periods,
																			&psystem_ptr->changes_to_send_to_master,
																			SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_annual_periods_bit ] );
																			
	// ----------
	for( i=0; i<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS; ++i )
	{
		pbds_ptr->meter_read_date[i] = SHARED_get_uint32_from_array_32_bit_change_bits_group(	psystem_ptr,
																					i,
																					&psystem_ptr->budget_meter_read_date[ i ],
																					IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS,
																					IRRIGATION_SYSTEM_BUDGET_PERIOD_DATE_MIN,
																					IRRIGATION_SYSTEM_BUDGET_PERIOD_DATE_MAX,
																					IRRIGATION_SYSTEM_BUDGET_PERIOD_DATE_DEFAULT,
																					&nm_SYSTEM_set_budget_period,
																					&psystem_ptr->changes_to_send_to_master,
																					SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_meter_read_date_bit ] );
	}

	pbds_ptr->mode = SHARED_get_uint32_32_bit_change_bits_group( psystem_ptr,
																 &psystem_ptr->budget_mode,
																 IRRIGATION_SYSTEM_BUDGET_MODE_MIN,
																 IRRIGATION_SYSTEM_BUDGET_MODE_MAX,
																 IRRIGATION_SYSTEM_BUDGET_MODE_DEFAULT,
																 &nm_SYSTEM_set_budget_mode,
																 &psystem_ptr->changes_to_send_to_master,
																 SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_mode_bit ] );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
// 01/05/2016 skc : Return the budget period index for the specified date
// return UNS_32_MAX if out of range
extern UNS_32 SYSTEM_get_budget_period_index( BUDGET_DETAILS_STRUCT* pbds_ptr, DATE_TIME dt )
{
	UNS_32 retval = UNS_32_MAX; // pessimistic default
	DATE_TIME dt_start;
	DATE_TIME dt_end;

	// Initialize times to meter read time
	dt_start.T = pbds_ptr->meter_read_time;
	dt_end.T = pbds_ptr->meter_read_time;

	// loop the time periods, first positive test is the one we want
	for( UNS_32 i=0; i<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS-1; ++i )
	{
		dt_start.D = pbds_ptr->meter_read_date[i];
		dt_end.D = pbds_ptr->meter_read_date[i+1];

		// 3/23/2016 skc : The budget period starts and ends at noon. 
		// Where noon at the end date is not part of the budget period.
		if( DT1_IsBiggerThanOrEqualTo_DT2( &dt, &dt_start ) &&
			DT1_IsBiggerThan_DT2( &dt_end, &dt ) )
		{
			retval = i; // this is the budget period for the dt parameter
			break; // we have a match
		}
	}

	return retval;
}

extern BOOL_32 SYSTEM_at_least_one_system_has_budget_enabled( void )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;
	BUDGET_DETAILS_STRUCT bds;
	BOOL_32	rv = false; // pessimistic default
	DATE_TIME today;
	UNS_32 numsys;
	UNS_32 system_id;

	EPSON_obtain_latest_time_and_date( &today );

	// loop all possible mainlines
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );
	numsys = SYSTEM_num_systems_in_use();
	for( UNS_32 i = 0; i < numsys; ++i )
	{
		lsystem = SYSTEM_get_group_at_this_index( i );
		if( lsystem != NULL )
		{
			system_id = nm_GROUP_get_group_ID( lsystem );

			// sanity check, make sure we have valid system object
			if( 0 != system_id )
			{
				SYSTEM_get_budget_details( lsystem, &bds );

				// if option other than No is selected
				if( bds.in_use != IRRIGATION_SYSTEM_BUDGET_IN_USE_NO ) 
				{
					// Only respond if today is within the first budget period
					if( 0 == SYSTEM_get_budget_period_index( &bds, today ) )
					{
						rv = true;
						break; // we're done, bail out
					}
				}
			}
		}
	}
	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return( rv );
}

// 01/28/2016 skc : Returns the sum of the budget values for POCs attached to a system
// @mutex_requirements -  list_system_recursive_MUTEX, list_poc_recursive_MUTEX
extern UNS_32 nm_SYSTEM_get_budget( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem_ptr, UNS_32 idx_budget )
{
	UNS_32 rv = 0;
	UNS_32 irri_gid; // for testing if a POC is a member of psystem_ptr
	POC_GROUP_STRUCT* ptr_poc;
	UNS_32 i;

	//validate idx_budget
	if( (idx_budget<0) || (idx_budget>=IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS) )
	{
		Alert_Message_va("Invalid budget index: %d", idx_budget);
		return rv;
	}

	irri_gid = nm_GROUP_get_group_ID( psystem_ptr );

	// 6/10/2016 rmd : Pass through the preserves looking for active preserves records (gid !=
	// 0), that are for a POC belonging to the SYSTEM we are interested in. [The ppoc_preserves
	// by its definition holds ALL active POCs in the network, so we will see all the pocs.]
	for( i=0; i<MAX_POCS_IN_NETWORK; i++ )
	{
		const BY_POC_RECORD * const pbpr = &poc_preserves.poc[ i ];

		// 8/2/2016 skc : Filter out POC with no gid, don't match the system, not used for budgets
		if( (pbpr->poc_gid != 0) &&
			(pbpr->this_pocs_system_preserves_ptr->system_gid == irri_gid) && 
			POC_use_for_budget( i ) )
		{
			ptr_poc = POC_get_group_at_this_index( POC_get_index_for_group_with_this_GID(pbpr->poc_gid) );
			rv += POC_get_budget( ptr_poc, idx_budget );
		}
	}

	return rv;
}

// 02/01/2016 skc : Returns the sum of water used for POCs attached to a system for the active budget period.
// @mutex_requirements -  list_system_recursive_MUTEX, poc_preserves_recursive_MUTEX
extern UNS_32 nm_SYSTEM_get_used( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem_ptr )
{
	UNS_32 rv = 0;
	UNS_32 irri_gid; // for testing if a POC is a member of psystem_ptr

	irri_gid = nm_GROUP_get_group_ID( psystem_ptr );

	// 6/10/2016 rmd : Pass through the preserves looking for active preserves records (gid !=
	// 0), that are for a POC belonging to the SYSTEM we are interested in. [The ppoc_preserves
	// by its definition holds ALL active POCs in the network, so we will see all the pocs.]
	for( UNS_32 bp=0; bp<MAX_POCS_IN_NETWORK; ++bp )
	{
		// 8/2/2016 skc : Filter out POC with no gid, don't match the system, not used for budgets
		if( (poc_preserves.poc[ bp ].poc_gid != 0) &&
			(poc_preserves.poc[ bp ].this_pocs_system_preserves_ptr->system_gid == irri_gid) &&
			POC_use_for_budget( bp ) )
		{
			// 4/19/2016 skc : Call helper function to get usage by POC.
			rv += nm_BUDGET_get_used( bp );
		}
	}

	return rv;
}

/* ---------------------------------------------------------- */
// 02/29/2016 skc - Accessor function
// @mutex_requirements - list_system_recursive_MUTEX
// @task_execution - TD_CHECK
extern BOOL_32 nm_SYSTEM_get_budget_flow_type( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem_ptr, UNS_32 idx )
{
	BOOL_32 rv;

	rv = false;

	if( idx < FLOW_TYPE_NUM_TYPES )
	{
		rv = SHARED_get_bool_from_array_32_bit_change_bits_group( psystem_ptr,
													 &psystem_ptr->budget_flow_type[idx],
																  idx,
																  FLOW_TYPE_NUM_TYPES,
													 (false),
													 &nm_SYSTEM_set_budget_flow_type,
													 &psystem_ptr->changes_to_send_to_master,
													 SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_flow_type_bit ] );

	}
	else
	{
		Alert_Message_va("Invalid flow type index: %d", idx);
	}

	return rv;
}

/* ---------------------------------------------------------- */
// 02/29/2016 skc - Accessor function
// Because the backing variable is a runtime temp, no validation is needed.
// @mutex_requirements - list_system_recursive_MUTEX
// @task_execution - TD_CHECK
extern float nm_SYSTEM_get_Vp( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem_ptr )
{
	return psystem_ptr->volume_predicted;
}

/* ---------------------------------------------------------- */
// 02/29/2016 skc - Accessor function
// Because the backing variable is a runtime temp, no validation is needed.
// @mutex_requirements - list_system_recursive_MUTEX
// @task_execution - TD_CHECK
extern void nm_SYSTEM_set_Vp( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem_ptr, float pVp )
{
	psystem_ptr->volume_predicted = pVp;
}

/* ---------------------------------------------------------- */
// 02/29/2016 skc - Accessor function
// Because the backing variable is a runtime temp, no validation is needed.
// @mutex_requirements - list_system_recursive_MUTEX
// @task_execution - TD_CHECK
extern float nm_SYSTEM_get_poc_ratio( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem_ptr )
{
	return psystem_ptr->poc_ratio;
}

/* ---------------------------------------------------------- */
// 02/29/2016 skc - Accessor function
// Because the backing variable is a runtime temp, no validation is needed.
// @mutex_requirements - list_system_recursive_MUTEX
// @task_execution - TD_CHECK
extern void nm_SYSTEM_set_poc_ratio(IRRIGATION_SYSTEM_GROUP_STRUCT *psystem_ptr, float pratio )
{
	psystem_ptr->poc_ratio = pratio;
}

/* ---------------------------------------------------------- */
void IRRIGATION_SYSTEM_load_ftimes_list( void )
{
	// 2/14/2018 rmd : Executed within the context of the ftimes task. Upon entry it is assumed
	// the ftimes station group list is empty and initialized.
	
	// ----------
	
	IRRIGATION_SYSTEM_GROUP_STRUCT	*file_group;
	
	FT_SYSTEM_GROUP_STRUCT		*ft_group;

	// ----------

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	file_group = nm_ListGetFirst( &system_group_list_hdr );

	while( file_group )
	{
		// 3/31/2018 rmd : The used_for_irrigation_bool term is always (true) afaik, as of this
		// writing. But I included it to prepare for future uses.
		if( file_group->in_use && file_group->used_for_irrigation_bool )
		{
			if( mem_obtain_a_block_if_available( sizeof(FT_SYSTEM_GROUP_STRUCT), (void**)&ft_group ) )
			{
				memset( ft_group, 0x00, sizeof(FT_SYSTEM_GROUP_STRUCT) );
				
				ft_group->system_gid = file_group->base.group_identity_number;

				// ----------
				
				ft_group->capacity_in_use_bool = file_group->capacity_in_use_bool;

				ft_group->capacity_with_pump_gpm = file_group->capacity_with_pump_gpm;

				ft_group->capacity_without_pump_gpm = file_group->capacity_without_pump_gpm;
				
				// ----------
				
				// 2/14/2018 rmd : Now add it to the ftimes list.
				nm_ListInsertTail( &ft_system_groups_list_hdr, ft_group );
			}
		}

		// ----------

		file_group = nm_ListGetNext( &system_group_list_hdr, file_group );
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
BOOL_32 IRRIGATION_SYSTEM_calculate_chain_sync_crc( UNS_32 const pff_name )
{
	// 8/30/2018 rmd : Executed in the context of the comm_mngr task when token responses are
	// being made. And that's it.

	// ----------

	IRRIGATION_SYSTEM_GROUP_STRUCT	*lirrigation_system_ptr;
	
	UNS_8				*checksum_start;

	UNS_32				checksum_length;

	UNS_8				*ucp;
	
	BOOL_32				rv;

	// ----------

	rv = (false);

	// ----------
	
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	// 8/30/2018 rmd : So the CRC calculation function wants a continuous memory space and a
	// length to calculate the crc off. In this function we will pick out the desired data, and
	// simply pile it up on itself, list item by list item. We don't care how messy the pile is
	// because we never attempt to reference any data once it is added to the pile. The data is
	// only there to include in the checksum.
	//
	// 8/30/2018 rmd : Well we know it can't be bigger than the list item count times the size
	// of each list item, so start with a memory block that big.
	if( mem_obtain_a_block_if_available( (system_group_list_hdr.count * sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT)), (void**)&checksum_start ) )
	{
		// 9/5/2018 rmd : Well then the calculation will be successful. Nothing else can prevent
		// that outcome.
		rv = (true);
		
		// ----------
		
		ucp = checksum_start;
		
		checksum_length = 0;
		
		// ----------
		
		// 8/30/2018 rmd : Rattle though the list and push the fields we want to include in the sync
		// test onto the block to be checksummed.
		lirrigation_system_ptr = nm_ListGetFirst( &system_group_list_hdr );
		
		while( lirrigation_system_ptr != NULL )
		{
			// 9/4/2018 rmd : Make sure you include only TRUE USER SETTINGS, no flags, or pointers to
			// memory, etc...
			
			// ----------
			
			// 11/2/2018 rmd : Due to the inconsistent way the group names strings are handled during
			// the sync (we do not regligiously take all 48 characters and always carry them through the
			// sync process, there are places we only copy up until the null), the part of the string
			// following the null char is not guaranteed to be in sync. So only include up until the
			// null in the crc test.
			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->base.description, strlen(lirrigation_system_ptr->base.description) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->used_for_irrigation_bool, sizeof(lirrigation_system_ptr->used_for_irrigation_bool) );


			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->capacity_in_use_bool, sizeof(lirrigation_system_ptr->capacity_in_use_bool) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->capacity_with_pump_gpm, sizeof(lirrigation_system_ptr->capacity_with_pump_gpm) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->capacity_without_pump_gpm, sizeof(lirrigation_system_ptr->capacity_without_pump_gpm) );


			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->mlb_during_irri_gpm, sizeof(lirrigation_system_ptr->mlb_during_irri_gpm) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->mlb_during_mvor_gpm, sizeof(lirrigation_system_ptr->mlb_during_mvor_gpm) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->mlb_all_other_times_gpm, sizeof(lirrigation_system_ptr->mlb_all_other_times_gpm) );


			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->flow_checking_in_use_bool, sizeof(lirrigation_system_ptr->flow_checking_in_use_bool) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->flow_checking_ranges_gpm, sizeof(lirrigation_system_ptr->flow_checking_ranges_gpm) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->flow_checking_tolerances_plus_gpm, sizeof(lirrigation_system_ptr->flow_checking_tolerances_plus_gpm) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->flow_checking_tolerances_minus_gpm, sizeof(lirrigation_system_ptr->flow_checking_tolerances_minus_gpm) );
			
			// ----------
			
			// skipping the 3 changes bit fields
			
			// skipping 6 derate table variables - they are not sync'd user settings
			
			// skipping 1 changes bit field
			
			// ----------
			
			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->mvor_open_times, sizeof(lirrigation_system_ptr->mvor_open_times) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->mvor_close_times, sizeof(lirrigation_system_ptr->mvor_close_times) );


			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->budget_in_use, sizeof(lirrigation_system_ptr->budget_in_use) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->budget_meter_read_time, sizeof(lirrigation_system_ptr->budget_meter_read_time) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->budget_number_of_annual_periods, sizeof(lirrigation_system_ptr->budget_number_of_annual_periods) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->budget_meter_read_date, sizeof(lirrigation_system_ptr->budget_meter_read_date) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->budget_mode, sizeof(lirrigation_system_ptr->budget_mode) );


			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->in_use, sizeof(lirrigation_system_ptr->in_use) );

			// ----------
			
			// skipping volume predicted and poc_ratio - they are not sync'd user settings
			
			// ----------
			
			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lirrigation_system_ptr->budget_flow_type, sizeof(lirrigation_system_ptr->budget_flow_type) );
			
			// END
			
			// ----------
		
			lirrigation_system_ptr = nm_ListGetNext( &system_group_list_hdr, lirrigation_system_ptr );
		}
		
		// ----------
		
		// 8/30/2018 rmd : Now calculate the checksum.
		cscs.the_crc[ pff_name ] = CRC_calculate_32bit_big_endian( checksum_start, checksum_length );
		
		// ----------
		
		// 9/4/2018 rmd : We're done with the block, so of course free it.
		mem_free( checksum_start );
	}
	else
	{
		// 9/5/2018 rmd : So by design it's okay if there is no memory. The checksum won't be set
		// valid, and the next time we try to include it in a token response we'll try to calculate
		// it again. Self protecting and retrying. But let's alert about this so we know it's taking
		// place.
		Alert_Message_va( "SYNC: no mem to calc checksum %s", chain_sync_file_pertinants[ pff_name ].file_name_string );
	}
	
	// ----------
	
	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
