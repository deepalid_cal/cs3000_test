/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_SWITCH_INPUT_H
#define _INC_SWITCH_INPUT_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"cal_string.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// ALL these defines are used to set unsigned chars - do not make them values that take 2 bytes
#define SIC_FUNCTIONALITY_BASE_VALUE_FOR_CHANGE_LINES						0x11
#define SIC_FUNCTIONALITY_not_in_use										0x11
#define SIC_FUNCTIONALITY_operate_as_a_rain_switch							0x12
#define SIC_FUNCTIONALITY_operate_as_a_freeze_switch						0x13
#define SIC_FUNCTIONALITY_custom_operation									0x14

#define SIC_CONTACT_TYPE_BASE_VALUE_FOR_CHANGE_LINES						0x21
#define SIC_CONTACT_TYPE_normally_open										0x21
#define SIC_CONTACT_TYPE_normally_closed									0x22

#define SIC_IRRIGATION_TO_AFFECT_BASE_VALUE_FOR_CHANGE_LINES				0x31
#define SIC_IRRIGATION_TO_AFFECT_all_irrigation								0x31
// this rain and freeze switches use this one
#define SIC_IRRIGATION_TO_AFFECT_all_scheduled_irrigation					0x32
#define SIC_IRRIGATION_TO_AFFECT_only_programmed_irrigation					0x33
#define SIC_IRRIGATION_TO_AFFECT_only_manual_programs						0x34
//#define SIC_IRRIGATION_TO_AFFECT_only_radio_remote
//#define SIC_IRRIGATION_TO_AFFECT_only_test
//#define SIC_IRRIGATION_TO_AFFECT_only_walk_thru
//#define SIC_IRRIGATION_TO_AFFECT_only_manual
//#define SIC_IRRIGATION_TO_AFFECT_only_manual_program_1
//#define SIC_IRRIGATION_TO_AFFECT_only_manual_program_2
//#define SIC_IRRIGATION_TO_AFFECT_only_programmed
//#define SIC_IRRIGATION_TO_AFFECT_only_scheduled_ho

#define SIC_ACTION_TO_TAKE_BASE_VALUE_FOR_CHANGE_LINES						0x41
// this is the function of rain and freeze
#define SIC_ACTION_TO_TAKE_stop__throw_away_remaining_time__zero_ho			0x41
// this is the STOP function
#define SIC_ACTION_TO_TAKE_stop__throw_away_remaining_time					0x42
// this is the PAUSE
#define SIC_ACTION_TO_TAKE_pause											0x43



// the normal state
#define SIC_SWITCH_STATUS_inactive											0x51
// the take action state
#define SIC_SWITCH_STATUS_active											0x52

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	char	name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32	controller_serial_number;

	BOOL_32	connected_bool;

	// Whether switch contact is normally open or normally closed (i.e., to
	// operate on switch closure or opening)
    BOOL_32	normally_closed_bool;

	BOOL_32   stops_irrigation_bool;

	// Whether it is in its NORMAL or ACTIVE position
	BOOL_32	currently_active_bool;

	// Sometimes this is a choice for the user to fill in; sometimes not. For
	// example, the rain switch functionality is likely to remain fixed at
	// SCHEDULED and MANUAL programs..
	UNS_32	irrigation_to_affect;

} SWITCH_INPUT_CONTROL_STRUCT;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void nm_SWITCH_set_name( SWITCH_INPUT_CONTROL_STRUCT *pswitch, const char *pname, const BOOL_32 pgenerate_change_line_bool, const UNS_32 pchange_line_index, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits );

extern void nm_SWITCH_set_serial_number( SWITCH_INPUT_CONTROL_STRUCT *pswitch, UNS_32 pserial_number, const UNS_32 pdefault_value, const BOOL_32 pgenerate_change_line_bool, const UNS_32 pchange_line_index, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits );

extern void nm_SWITCH_set_connected( SWITCH_INPUT_CONTROL_STRUCT *pswitch, BOOL_32 pconnected_bool, const BOOL_32 pdefault_value, const BOOL_32 pgenerate_change_line_bool, const UNS_32 pchange_line_index, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits );

extern void nm_SWITCH_set_normally_closed( SWITCH_INPUT_CONTROL_STRUCT *pswitch, BOOL_32 pnormally_closed_bool, const BOOL_32 pdefault_value, const BOOL_32 pgenerate_change_line_bool, const UNS_32 pchange_line_index, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits );

extern void nm_SWITCH_set_stops_irrigation( SWITCH_INPUT_CONTROL_STRUCT *pswitch, BOOL_32 pstops_irrigation_bool, const BOOL_32 pdefault_value, const BOOL_32 pgenerate_change_line_bool, const UNS_32 pchange_line_index, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits );

extern void nm_SWITCH_set_currently_active( SWITCH_INPUT_CONTROL_STRUCT *pswitch, BOOL_32 pcurrently_active_bool, const BOOL_32 pdefault_value, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits );

extern void nm_SWITCH_set_irrigation_to_affect( SWITCH_INPUT_CONTROL_STRUCT *pswitch, UNS_32 pirrigation_to_affect, const UNS_32 pmin_value, const UNS_32 pmax_value, const UNS_32 pdefault_value, const BOOL_32 pgenerate_change_line_bool, const UNS_32 pchange_line_index, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits );


extern char *SWITCH_get_name( char *const pbuf, const UNS_32 allowable_size );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

