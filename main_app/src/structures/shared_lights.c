
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_SHARED_LIGHTS_C
#define _INC_SHARED_LIGHTS_C

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"group_base_file.h"

#include	"cal_td_utils.h"

#include	"change.h"

#ifndef	_MSC_VER

	#include	"alerts.h"

	#include	"app_startup.h"
	
	#include	"cs_mem.h"

	#include	"battery_backed_vars.h"

#else

	#include	<stdlib.h>

	#include	"sql_merge.h"

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	LIGHTS_CHANGE_BITFIELD_description_bit						( 0 )
#define	LIGHTS_CHANGE_BITFIELD_box_index_bit						( 1 )
#define	LIGHTS_CHANGE_BITFIELD_output_index_bit						( 2 )
#define	LIGHTS_CHANGE_BITFIELD_physically_available_bit				( 3 )
#define	LIGHTS_CHANGE_BITFIELD_day_00_bit							( 4 )
#define	LIGHTS_CHANGE_BITFIELD_day_01_bit							( 5 )
#define	LIGHTS_CHANGE_BITFIELD_day_02_bit							( 6 )
#define	LIGHTS_CHANGE_BITFIELD_day_03_bit							( 7 )
#define	LIGHTS_CHANGE_BITFIELD_day_04_bit							( 8 )
#define	LIGHTS_CHANGE_BITFIELD_day_05_bit							( 9 )
#define	LIGHTS_CHANGE_BITFIELD_day_06_bit							( 10 )
#define	LIGHTS_CHANGE_BITFIELD_day_07_bit							( 11 )
#define	LIGHTS_CHANGE_BITFIELD_day_08_bit							( 12 )
#define	LIGHTS_CHANGE_BITFIELD_day_09_bit							( 13 )
#define	LIGHTS_CHANGE_BITFIELD_day_10_bit							( 14 )
#define	LIGHTS_CHANGE_BITFIELD_day_11_bit							( 15 )
#define	LIGHTS_CHANGE_BITFIELD_day_12_bit							( 16 )
#define	LIGHTS_CHANGE_BITFIELD_day_13_bit							( 17 )
																	  
// ----------                                                         

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Note: Not all strings are completely defined here to save string storage space. The "DayXX_" strings will be expanded 
// in code to generate 4 separate strings. This is a complete list of data base field names:
//	"Name"
//	"BoxIndex"
//	"OutputIndex"
//	"CardConnected"
//	"Day00_StartTime1"
//	"Day00_StartTime2"
//	"Day00_StopTime1"
//	"Day00_StopTime2"
//	"Day01_StartTime1"
//	"Day01_StartTime2"
//	"Day01_StopTime1"
//	"Day01_StopTime2"
//	"Day02_StartTime1"
//	"Day02_StartTime2"
//	"Day02_StopTime1"
//	"Day02_StopTime2"
//	"Day03_StartTime1"
//	"Day03_StartTime2"
//	"Day03_StopTime1"
//	"Day03_StopTime2"
//	"Day04_StartTime1"
//	"Day04_StartTime2"
//	"Day04_StopTime1"
//	"Day04_StopTime2"
//	"Day05_StartTime1"
//	"Day05_StartTime2"
//	"Day05_StopTime1"
//	"Day05_StopTime2"
//	"Day06_StartTime1"
//	"Day06_StartTime2"
//	"Day06_StopTime1"
//	"Day06_StopTime2"
//	"Day07_StartTime1"
//	"Day07_StartTime2"
//	"Day07_StopTime1"
//	"Day07_StopTime2"
//	"Day08_StartTime1"
//	"Day08_StartTime2"
//	"Day08_StopTime1"
//	"Day08_StopTime2"
//	"Day09_StartTime1"
//	"Day09_StartTime2"
//	"Day09_StopTime1"
//	"Day09_StopTime2"
//	"Day10_StartTime1"
//	"Day10_StartTime2"
//	"Day10_StopTime1"
//	"Day10_StopTime2"
//	"Day11_StartTime1"
//	"Day11_StartTime2"
//	"Day11_StopTime1"
//	"Day11_StopTime2"
//	"Day12_StartTime1"
//	"Day12_StartTime2"
//	"Day12_StopTime1"
//	"Day12_StopTime2"
//	"Day13_StartTime1"
//	"Day13_StartTime2"
//	"Day13_StopTime1"
//	"Day13_StopTime2"

static char* const LIGHTS_database_field_names[ ] =
{
	"Name",
	"BoxIndex",
	"OutputIndex",
	"CardConnected",
	"Day00_",
	"Day01_",
	"Day02_",
	"Day03_",
	"Day04_",
	"Day05_",
	"Day06_",
	"Day07_",
	"Day08_",
	"Day09_",
	"Day10_",
	"Day11_",
	"Day12_",
	"Day13_"
};

#define LIGHTS_NUM_DAY_STRING_CHARS_TO_COMPARE						( 6 )
#define LIGHTS_NUM_DAY_STRING_CHARS_WITH_NAMES						( 17 ) // "Day00_StartTime1\0" 

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * The definition of the group's structure. If the contents of this structure
 * are changed or reordered, you <b>MUST</b> update LIGHTS_LATEST_FILE_REVISION. 
 *  
 * This structure is defined in lights.c to protect it from unauthorized use. It 
 * should not be moved to the lights.h file. 
 * 
 * @revisions
 *    7/17/2015 mpd
 */

struct LIGHT_STRUCT
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	// -------------------------------------------- //
	//	WHICH PHYSICAL LIGHT IS THIS? - THESE PARAMETERS PIN THAT DOWN
	// -------------------------------------------- //

	// 9/11/2015 mpd : The web server can calculate todays date relative to January 1, 2012 and the resulting day index 
	// just as easily as the controller can do it. We don't need this complication.

	// This date (using Epson Date_TIME->D) is needed in the data base to sync the 14 day rotating lights schedule with
	// calendar days.
	//UNS_32			todays_date;

	// This day index is needed in the data base to sync the 14 day rotating lights schedule with calendar days.
	//UNS_32			todays_day_index;

	// 4/17/2014 ajv : The 0-based box (controller) index which coincides with
	// the controller's communication address.
	UNS_32			box_index_0;

	// 7/13/2015 mpd : The 0-based light output index indicating which of the 4 light outputs is associated with this 
	// struct. 
	UNS_32			output_index_0;
	
	// ----------

	// -------------------------------------------- //
	//	WHETHER THE LIGHT MODULES ARE CONNECTED TO THE TP BOARD
	// -------------------------------------------- //

	// 7/24/2015 mpd : Tracks whether the LIGHT card and terminal are both physically connected to
	// the TP Micro or not. This is a single flag for the 4 light outputs on the controller. This value needs to be kept
	// in sync with the physically available bits in the battery backed bit fields associated with these 4 lights.
	BOOL_32			light_physically_available;

	// -------------------------------------------- //
	//	COMMON LIGHT DETAILS
	// -------------------------------------------- //

    // 7/16/2015 mpd : This is a 14 day schedule anchored on a "first Sunday" that occurs every other week beginning on
	// January 1, 2012 (the last Jan 1 on a Sunday). The user can edit the schedule beginning on the array entry 
	// corresponding to today (an offset from the first Sunday which is entry 0). The next 13 days are also available 
	// for edit. These days will wrap around to the beginning of the array if today is not the first Sunday.
	LIGHTS_DAY_STRUCT daily_schedule[ LIGHTS_SCHEDULE_DAYS_MAX ];

	// -------------------------------------------- //
	//  CHANGE BITS
	// -------------------------------------------- //

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;
	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

	// This structure is 324 bytes long on 8/19/2015.
};

#if 0
/* ---------------------------------------------------------- */
/**
 * Sets the box index for the specified light.
 * 
 * @mutex Calling function must have the list_lights_recursive_MUTEX to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param plight A pointer to the group to update.
 * @param pbox_index The box index.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 8/11/2011 Adrianusv
 *
 * @revisions
 *    8/11/2011 Initial release
 *    4/5/2012  Added setting of change bits to distribute changes
 *    7/20/2015 mpd Cloned POC function and adapted for LIGHTS.
 */
extern void nm_LIGHTS_set_todays_date( void *const plight,
									   UNS_32 ptodays_date,
									   const BOOL_32 pgenerate_change_line_bool,
									   const UNS_32 preason_for_change,
									   const UNS_32 pbox_index_0,
									   const BOOL_32 pset_change_bits,
									   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
									   #ifdef _MSC_VER
									   , char *pmerge_update_str,
									   char *pmerge_insert_fields_str,
									   char *pmerge_insert_values_str
									   #endif
									 )
{
	if( SHARED_set_uint32_with_32_bit_change_bits_group( plight,
														 &(((LIGHT_STRUCT*)plight)->todays_date),
														 ptodays_date,
														 LIGHTS_BEGINNING_OF_TIME,
														 LIGHTS_END_OF_TIME,
														 LIGHTS_BEGINNING_OF_TIME,
														 pgenerate_change_line_bool,
														 CHANGE_LIGHTS_TODAYS_DATE,
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((LIGHT_STRUCT*)plight)->changes_to_upload_to_comm_server),
														 LIGHTS_CHANGE_BITFIELD_date_bit,
														 LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_date_bit ]
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   ) == ( true ) )
	{
		#ifndef	_MSC_VER

			// 7/20/2015 mpd : TODO: Remove this obsolete "if" block and reformat the set call.
			//LIGHTS_PRESERVES_request_a_resync();

		#endif
	}
}

// 8/27/2015 mpd : TODO: Fix comment box.
/* ---------------------------------------------------------- */
/**
 * Sets the box index for the specified light.
 * 
 * @mutex Calling function must have the list_lights_recursive_MUTEX to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param plight A pointer to the group to update.
 * @param pbox_index The box index.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 8/11/2011 Adrianusv
 *
 * @revisions
 *    8/11/2011 Initial release
 *    4/5/2012  Added setting of change bits to distribute changes
 *    7/20/2015 mpd Cloned POC function and adapted for LIGHTS.
 */
extern void nm_LIGHTS_set_todays_index( void *const plight,
										UNS_32 ptodays_day_index,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const BOOL_32 pset_change_bits,
										CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
									  )
{
	if( SHARED_set_uint32_with_32_bit_change_bits_group( plight,
														 &(((LIGHT_STRUCT*)plight)->todays_day_index),
														 ptodays_day_index,
														 LIGHTS_DAY_INDEX_MIN,
														 LIGHTS_DAY_INDEX_MAX,
														 LIGHTS_DAY_INDEX_DEFAULT,
														 pgenerate_change_line_bool,
														 CHANGE_LIGHTS_TODAYS_DATE,
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((LIGHT_STRUCT*)plight)->changes_to_upload_to_comm_server),
														 LIGHTS_CHANGE_BITFIELD_day_index,
														 LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_day_index ]
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   ) == ( true ) )
	{
		#ifndef	_MSC_VER

			// 7/20/2015 mpd : TODO: Remove this obsolete "if" block and reformat the set call.
			//LIGHTS_PRESERVES_request_a_resync();

		#endif
	}
}
#endif

/* ---------------------------------------------------------- */
/**
 * Sets the name string for the specified light.
 * 
 * @mutex Calling function must have the list_lights_recursive_MUTEX to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 * 
 * @param plight Pointer to the "LIGHTS_GROUP_STRUCT" of the light.
 *
 * @param pavailable Pointer to name string.
 *
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * 
 * @param preason_for_change The reason the change occurred. Possible reason are
 *  						 defined in change.h.
 * 
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * 
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @param pchange_bitfield_to_set Pointer to the bit field.
 * 
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 mpd Cloned POC function and adapted for LIGHTS.
 */
extern void nm_LIGHTS_set_name( LIGHT_STRUCT *const plight,
							 char *pname,
							 const BOOL_32 pgenerate_change_line_bool,
							 const UNS_32 preason_for_change,
							 const UNS_32 pbox_index_0,
							 const BOOL_32 pset_change_bits,
							 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
							 #ifdef _MSC_VER
							 , char *pmerge_update_str,
							 char *pmerge_insert_fields_str,
							 char *pmerge_insert_values_str
							 #endif
						   )
{
	SHARED_set_name_32_bit_change_bits( plight,
										pname,
										pgenerate_change_line_bool,
										preason_for_change,
										pbox_index_0,
										pset_change_bits,
										pchange_bitfield_to_set,
										&plight->changes_to_upload_to_comm_server,
										LIGHTS_CHANGE_BITFIELD_description_bit
										#ifdef _MSC_VER
										, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_description_bit ],
										pmerge_update_str,
										pmerge_insert_fields_str,
										pmerge_insert_values_str
										#endif
									  );
}

/* ---------------------------------------------------------- */
/**
 * Sets the box index for the specified light.
 * 
 * @mutex Calling function must have the list_lights_recursive_MUTEX to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param plight A pointer to the group to update.
 * @param pbox_index The box index.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 8/11/2011 Adrianusv
 *
 * @revisions
 *    8/11/2011 Initial release
 *    4/5/2012  Added setting of change bits to distribute changes
 *    7/20/2015 mpd Cloned POC function and adapted for LIGHTS.
 */
extern void nm_LIGHTS_set_box_index( void *const plight,
								  UNS_32 pbox_index,
								  const BOOL_32 pgenerate_change_line_bool,
								  const UNS_32 preason_for_change,
								  const UNS_32 pbox_index_0,
								  const BOOL_32 pset_change_bits,
								  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
								  #ifdef _MSC_VER
								  , char *pmerge_update_str,
								  char *pmerge_insert_fields_str,
								  char *pmerge_insert_values_str
								  #endif
								)
{
	if( SHARED_set_uint32_with_32_bit_change_bits_group( plight,
														 &(((LIGHT_STRUCT*)plight)->box_index_0),
														 pbox_index,
														 BOX_INDEX_MINIMUM,
														 BOX_INDEX_MAXIMUM,
														 BOX_INDEX_DEFAULT,
														 pgenerate_change_line_bool,
														 CHANGE_LIGHTS_BOX_INDEX,
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((LIGHT_STRUCT*)plight)->changes_to_upload_to_comm_server),
														 LIGHTS_CHANGE_BITFIELD_box_index_bit,
														 LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_box_index_bit ]
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   ) == ( true ) )
	{
		#ifndef	_MSC_VER

			// 7/20/2015 mpd : TODO: Remove this obsolete "if" block and reformat the set call.
			//LIGHTS_PRESERVES_request_a_resync();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * Sets the output index for the specified light.
 * 
 * @mutex Calling function must have the list_lights_recursive_MUTEX to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 * 
 * @param plight Pointer to the "LIGHTS_GROUP_STRUCT" of the light.
 *
 * @param pavailable The output index.
 *
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * 
 * @param preason_for_change The reason the change occurred. Possible reason are
 *  						 defined in change.h.
 * 
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * 
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @param pchange_bitfield_to_set Pointer to the bit field.
 * 
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern void nm_LIGHTS_set_output_index( void *const plight,
										UNS_32 plight_output_index,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const BOOL_32 pset_change_bits,
										CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
									  )
{
	if( SHARED_set_uint32_with_32_bit_change_bits_group( plight,
														 &(((LIGHT_STRUCT*)plight)->output_index_0),
														 plight_output_index,
														 LIGHTS_OUTPUT_INDEX_MIN,
														 LIGHTS_OUTPUT_INDEX_MAX,
														 LIGHTS_OUTPUT_INDEX_DEFAULT,
														 pgenerate_change_line_bool,
														 CHANGE_LIGHTS_OUTPUT_INDEX,
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((LIGHT_STRUCT*)plight)->changes_to_upload_to_comm_server),
														 LIGHTS_CHANGE_BITFIELD_output_index_bit,
														 LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_output_index_bit ]
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   ) == ( true ) )
	{
		#ifndef	_MSC_VER

			// 7/20/2015 mpd : TODO: Remove this obsolete "if" block and reformat the set call.
			//LIGHTS_PRESERVES_request_a_resync();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * Sets the physically available Boolean for the specified light.
 * 
 * @mutex Calling function must have the list_lights_recursive_MUTEX to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 * 
 * @param plight Pointer to the "LIGHTS_GROUP_STRUCT" of the light.
 *
 * @param pavailable Pointer to the Boolean.
 *
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * 
 * @param preason_for_change The reason the change occurred. Possible reason are
 *  						 defined in change.h.
 * 
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * 
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @param pchange_bitfield_to_set Pointer to the bit field.
 * 
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern void nm_LIGHTS_set_physically_available( void *const plight,
											 BOOL_32 pavailable,
											 const BOOL_32 pgenerate_change_line_bool,
											 const UNS_32 preason_for_change,
											 const UNS_32 pbox_index_0,
											 const BOOL_32 pset_change_bits,
											 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											 #ifdef _MSC_VER
											 , char *pmerge_update_str,
											 char *pmerge_insert_fields_str,
											 char *pmerge_insert_values_str
											 #endif
										   )
{
	if( SHARED_set_bool_with_32_bit_change_bits_group( plight,
													   &(((LIGHT_STRUCT*)plight)->light_physically_available),
													   pavailable,
													   ( false ),
													   pgenerate_change_line_bool,
													   CHANGE_LIGHTS_PHYSICALLY_AVAILABLE,
													   preason_for_change,
													   pbox_index_0,
													   pset_change_bits,
													   pchange_bitfield_to_set,
													   &(((LIGHT_STRUCT*)plight)->changes_to_upload_to_comm_server),
													   LIGHTS_CHANGE_BITFIELD_physically_available_bit,
													   LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_physically_available_bit ]
													   #ifdef _MSC_VER
													   , pmerge_update_str,
													   pmerge_insert_fields_str,
													   pmerge_insert_values_str
													   #endif
													 ) == ( true ) )
	{
		//#ifndef	_MSC_VER

		// 8/25/2015 mpd : The "light_physically_available" Boolean in the LIGHT_STRUCT was just changed. Update the 
		// preserves bit to match the new value (pavailable).
		//LIGHTS_PRESERVES_set_bit_field_bit( ( (LIGHT_STRUCT* ) plight )->box_index_0, 
		//									( (LIGHT_STRUCT* ) plight )->output_index_0, 
		//									LIGHTS_BITFIELD_BOOL_light_is_available_bit,
		//									pavailable );
		//#endif
	}

	#ifndef	_MSC_VER

	// 8/25/2015 mpd : The "light_physically_available" Boolean in the LIGHT_STRUCT may have changed. Update the 
	// preserves bit to match the new value (pavailable). This call has deliberately been removed from the "true" return
	// block. The lights file got out of sync with the preserves during development and would not self-correct. Since 
	// lights preserves do not sync down the chain, handling the bit on every call to this function creates very little 
	// extra work.
	LIGHTS_PRESERVES_set_bit_field_bit( ( (LIGHT_STRUCT* ) plight )->box_index_0, 
										( (LIGHT_STRUCT* ) plight )->output_index_0, 
										LIGHTS_BITFIELD_BOOL_light_is_available_bit,
										pavailable );
	#endif

}

/* ---------------------------------------------------------- */
/**
 * Sets the first start time for the specified light on the specified day in the 14 day schedule.
 * 
 * @mutex Calling function must have the list_lights_recursive_MUTEX to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 * 
 * @param plight Pointer to the "LIGHTS_GROUP_STRUCT" of the light.
 *
 * @param pindex The day index used to access the 14 day schedule.
 *
 * @param plight_start_time_1 The start time.
 *
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * 
 * @param preason_for_change The reason the change occurred. Possible reason are
 *  						 defined in change.h.
 * 
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * 
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @param pchange_bitfield_to_set Pointer to the bit field.
 * 
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern void nm_LIGHTS_set_start_time_1( void *const plight,
										const UNS_32 pindex,
										UNS_32 plight_start_time_1,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const BOOL_32 pset_change_bits,
										CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
									  )
{
	char	lfield_name[ LIGHTS_NUM_DAY_STRING_CHARS_WITH_NAMES ];

	if( ( pindex >= LIGHTS_DAY_INDEX_MIN ) && ( pindex <= LIGHTS_DAY_INDEX_MAX ) )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%sStartTime1", LIGHTS_database_field_names[ ( LIGHTS_CHANGE_BITFIELD_day_00_bit + pindex ) ] );

		SHARED_set_uint32_with_32_bit_change_bits_group( plight,
														 &(((LIGHT_STRUCT*)plight)->daily_schedule[ pindex ].start_time_1_in_seconds),
														 plight_start_time_1,
														 LIGHTS_START_TIME_MIN,
														 LIGHTS_START_TIME_MAX,
														 LIGHTS_START_TIME_DEFAULT,
														 pgenerate_change_line_bool,
														 ( CHANGE_LIGHTS_DAY_00_START_TIME_1 + pindex ),
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((LIGHT_STRUCT*)plight)->changes_to_upload_to_comm_server),
														 ( LIGHTS_CHANGE_BITFIELD_day_00_bit + pindex ),
														 lfield_name
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   );
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_index_out_of_range();

		#endif
	}

}

/* ---------------------------------------------------------- */
/**
 * Sets the second start time for the specified light on the specified day in the 14 day schedule.
 * 
 * @mutex Calling function must have the list_lights_recursive_MUTEX to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 * 
 * @param plight Pointer to the "LIGHTS_GROUP_STRUCT" of the light.
 *
 * @param pindex The day index used to access the 14 day schedule.
 *
 * @param plight_start_time_2 The start time.
 *
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * 
 * @param preason_for_change The reason the change occurred. Possible reason are
 *  						 defined in change.h.
 * 
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * 
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @param pchange_bitfield_to_set Pointer to the bit field.
 * 
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern void nm_LIGHTS_set_start_time_2( void *const plight,
										const UNS_32 pindex,
										UNS_32 plight_start_time_2,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const BOOL_32 pset_change_bits,
										CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
									  )
{
	char	lfield_name[ LIGHTS_NUM_DAY_STRING_CHARS_WITH_NAMES ];

	if( ( pindex >= LIGHTS_DAY_INDEX_MIN ) && ( pindex <= LIGHTS_DAY_INDEX_MAX ) )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%sStartTime2", LIGHTS_database_field_names[ ( LIGHTS_CHANGE_BITFIELD_day_00_bit + pindex ) ] );

		SHARED_set_uint32_with_32_bit_change_bits_group( plight,
														 &(((LIGHT_STRUCT*)plight)->daily_schedule[ pindex ].start_time_2_in_seconds),
														 plight_start_time_2,
														 LIGHTS_START_TIME_MIN,
														 LIGHTS_START_TIME_MAX,
														 LIGHTS_START_TIME_DEFAULT,
														 pgenerate_change_line_bool,
														 ( CHANGE_LIGHTS_DAY_00_START_TIME_2 + pindex ),
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((LIGHT_STRUCT*)plight)->changes_to_upload_to_comm_server),
														 ( LIGHTS_CHANGE_BITFIELD_day_00_bit + pindex ),
														 lfield_name
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   );
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_index_out_of_range();

		#endif
	}

}

/* ---------------------------------------------------------- */
/**
 * Sets the first stop time for the specified light on the specified day in the 14 day schedule.
 * 
 * @mutex Calling function must have the list_lights_recursive_MUTEX to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 * 
 * @param plight Pointer to the "LIGHTS_GROUP_STRUCT" of the light.
 *
 * @param pindex The day index used to access the 14 day schedule.
 *
 * @param plight_stop_time_1 The start time.
 *
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * 
 * @param preason_for_change The reason the change occurred. Possible reason are
 *  						 defined in change.h.
 * 
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * 
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @param pchange_bitfield_to_set Pointer to the bit field.
 * 
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern void nm_LIGHTS_set_stop_time_1( void *const plight,
									   const UNS_32 pindex,
									   UNS_32 plight_stop_time_1,
									   const BOOL_32 pgenerate_change_line_bool,
									   const UNS_32 preason_for_change,
									   const UNS_32 pbox_index_0,
									   const BOOL_32 pset_change_bits,
									   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
									   #ifdef _MSC_VER
									   , char *pmerge_update_str,
									   char *pmerge_insert_fields_str,
									   char *pmerge_insert_values_str
									   #endif
								)
{
	char	lfield_name[ LIGHTS_NUM_DAY_STRING_CHARS_WITH_NAMES ];

	if( ( pindex >= LIGHTS_DAY_INDEX_MIN ) && ( pindex <= LIGHTS_DAY_INDEX_MAX ) )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%sStopTime1", LIGHTS_database_field_names[ ( LIGHTS_CHANGE_BITFIELD_day_00_bit + pindex ) ] );

		SHARED_set_uint32_with_32_bit_change_bits_group( plight,
														 &(((LIGHT_STRUCT*)plight)->daily_schedule[ pindex ].stop_time_1_in_seconds),
														 plight_stop_time_1,
														 LIGHTS_STOP_TIME_MIN,
														 LIGHTS_STOP_TIME_MAX,
														 LIGHTS_STOP_TIME_DEFAULT,
														 pgenerate_change_line_bool,
														 ( CHANGE_LIGHTS_DAY_00_STOP_TIME_1 + pindex ),
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((LIGHT_STRUCT*)plight)->changes_to_upload_to_comm_server),
														 ( LIGHTS_CHANGE_BITFIELD_day_00_bit + pindex ),
														 lfield_name
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   );
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_index_out_of_range();

		#endif
	}

}

/* ---------------------------------------------------------- */
/**
 * Sets the second stop time for the specified light on the specified day in the 14 day schedule.
 * 
 * @mutex Calling function must have the list_lights_recursive_MUTEX to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 * 
 * @param plight Pointer to the "LIGHTS_GROUP_STRUCT" of the light.
 *
 * @param pindex The day index used to access the 14 day schedule.
 *
 * @param plight_stop_time_2 The start time.
 *
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * 
 * @param preason_for_change The reason the change occurred. Possible reason are
 *  						 defined in change.h.
 * 
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * 
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @param pchange_bitfield_to_set Pointer to the bit field.
 * 
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern void nm_LIGHTS_set_stop_time_2( void *const plight,
									   const UNS_32 pindex,
									   UNS_32 plight_stop_time_2,
									   const BOOL_32 pgenerate_change_line_bool,
									   const UNS_32 preason_for_change,
									   const UNS_32 pbox_index_0,
									   const BOOL_32 pset_change_bits,
									   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
									   #ifdef _MSC_VER
									   , char *pmerge_update_str,
									   char *pmerge_insert_fields_str,
									   char *pmerge_insert_values_str
									   #endif
									 )
{
	char	lfield_name[ LIGHTS_NUM_DAY_STRING_CHARS_WITH_NAMES ];

	if( ( pindex >= LIGHTS_DAY_INDEX_MIN ) && ( pindex <= LIGHTS_DAY_INDEX_MAX ) )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%sStopTime2", LIGHTS_database_field_names[ ( LIGHTS_CHANGE_BITFIELD_day_00_bit + pindex ) ] );

		SHARED_set_uint32_with_32_bit_change_bits_group( plight,
														 &(((LIGHT_STRUCT*)plight)->daily_schedule[ pindex ].stop_time_2_in_seconds),
														 plight_stop_time_2,
														 LIGHTS_STOP_TIME_MIN,
														 LIGHTS_STOP_TIME_MAX,
														 LIGHTS_STOP_TIME_DEFAULT,
														 pgenerate_change_line_bool,
														 ( CHANGE_LIGHTS_DAY_00_STOP_TIME_2 + pindex ),
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((LIGHT_STRUCT*)plight)->changes_to_upload_to_comm_server),
														 ( LIGHTS_CHANGE_BITFIELD_day_00_bit + pindex ),
														 lfield_name
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   );
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_index_out_of_range();

		#endif
	}

}

/* ---------------------------------------------------------- */
/**
 * Locates the group in the list that matches the passed in temporary group and
 * uses the structure's set routines to compare the two and make any changes to
 * the list's group. If any changes are made during this process, a function is
 * called to store the changes in the list.
 * 
 * @mutex Calling function must have the list_lights_recursive_MUTEX to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 * 
 * @executed Executed within the context of the key processing task when the
 *  		 user leaves the screen; within the context of the CommMngr task
 *  		 when a programming change is made from the central or handheld
 *  		 radio remote.
 * 
 * @param ptemporary_group A pointer to a temporary group which contains the
 *                         changes to store.
 * @param ptype_of_change Whether the change is an add, delete, or edit.
 * @param preason_for_change The reason for the change, such as keypad.
 * @param pbox_index_0 The serial number of the controller
 *                                    which initiated the change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 8/11/2011 Adrianusv
 *
 * @revisions
 *    8/11/2011 Initial release
 *    4/5/2012  Removed unnecessary variables and function calls
 *  			Added passing of pset_change_bits into set routine to indicate
 *  			whether the change was made locally so change bits are set
 *  			properly.
 *  			Made procedure static since it's now only called from within
 *  			this file.
 *    4/12/2012 Suppressed saving of bypass manifold settings if there's no
 *  			bypass manifold in use.
 *  			Added check to ensure transition flow rates are within range
 *    7/20/2015 mpd Cloned POC function and adapted for LIGHTS.
 */
static void nm_LIGHTS_store_changes( LIGHT_STRUCT *const ptemporary_group,
								  const UNS_32 plight_created,
								  const UNS_32 preason_for_change,
								  const UNS_32 pbox_index_0,
								  const BOOL_32 pset_change_bits,
								  const UNS_32 pchanges_received_from,
								  CHANGE_BITS_32_BIT pbitfield_of_changes
								  #ifdef _MSC_VER
								  , char *pmerge_update_str,
								  char *pmerge_insert_fields_str,
								  char *pmerge_insert_values_str
								  #endif
								)
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	LIGHT_STRUCT	*llight;

	UNS_32 day_index = 0;

	#ifndef _MSC_VER

	nm_GROUP_find_this_group_in_list( &light_list_hdr, ptemporary_group, (void**)&llight );

	if( llight != NULL )
	#else

	llight = ptemporary_group;

	#endif
	{
		#ifndef _MSC_VER

			// 5/12/2014 ajv : Only generate the alert that the group was created if
			// it was actually created AND it wasn't created as part of a token or
			// token response receipt.
			if( (plight_created == ( true )) && ((pchanges_received_from != CHANGE_REASON_CHANGED_AT_SLAVE) && (pchanges_received_from != CHANGE_REASON_DISTRIBUTED_BY_MASTER)) )
			{
				Alert_ChangeLine_Group( CHANGE_GROUP_CREATED, nm_GROUP_get_name( ptemporary_group ), NULL, NULL, NUMBER_OF_CHARS_IN_A_NAME, preason_for_change, pbox_index_0 );
			}

		#endif

		// 5/1/2014 ajv : Initialize since we're passing it into the routine
		// and without an init, the compiler will complain about using it
		// uninitialized.
		lchange_bitfield_to_set = NULL;

		#ifndef _MSC_VER

			lchange_bitfield_to_set = LIGHTS_get_change_bits_ptr( llight, pchanges_received_from );

		#endif

		#if 0
		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, LIGHTS_CHANGE_BITFIELD_date_bit ) == ( true )) )
		{
			nm_LIGHTS_set_todays_date( llight, ptemporary_group->todays_date, !(plight_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
							 #ifdef _MSC_VER
							 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
							 #endif
						   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, LIGHTS_CHANGE_BITFIELD_day_index ) == ( true )) )
		{
			nm_LIGHTS_set_todays_index( llight, ptemporary_group->todays_day_index, !(plight_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
							 #ifdef _MSC_VER
							 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
							 #endif
						   );
		}
		#endif

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, LIGHTS_CHANGE_BITFIELD_description_bit ) == ( true )) )
		{
			nm_LIGHTS_set_name( llight, nm_GROUP_get_name(ptemporary_group), !(plight_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
							 #ifdef _MSC_VER
							 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
							 #endif
						   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, LIGHTS_CHANGE_BITFIELD_box_index_bit ) == ( true )) )
		{
			nm_LIGHTS_set_box_index( llight, ptemporary_group->box_index_0, !(plight_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
								  #ifdef _MSC_VER
								  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
								  #endif
								);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, LIGHTS_CHANGE_BITFIELD_output_index_bit ) == ( true )) )
		{
			nm_LIGHTS_set_output_index( llight, ptemporary_group->output_index_0, !(plight_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
								  #ifdef _MSC_VER
								  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
								  #endif
								);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, LIGHTS_CHANGE_BITFIELD_physically_available_bit ) == ( true )) )
		{
			nm_LIGHTS_set_physically_available( llight, ptemporary_group->light_physically_available, !(plight_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											 #ifdef _MSC_VER
											 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
											 #endif
										   );
		}

		// 7/14/2015 mpd : Changes are grouped by schedule day. If any of the times on a given day are changed, the
		// entire day is stored. 
		for( day_index = 0; day_index < LIGHTS_SCHEDULE_DAYS_MAX; ++day_index )
		{
			if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, ( LIGHTS_CHANGE_BITFIELD_day_00_bit + day_index ) ) == ( true ) ) )
			{
				nm_LIGHTS_set_start_time_1( llight, 
											day_index,
											ptemporary_group->daily_schedule[ day_index ].start_time_1_in_seconds, 
											!(plight_created), 
											preason_for_change, 
											pbox_index_0, 
											pset_change_bits, 
											lchange_bitfield_to_set
										 #ifdef _MSC_VER
										 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
										 #endif
									   );
				nm_LIGHTS_set_start_time_2( llight, 
											day_index,
											ptemporary_group->daily_schedule[ day_index ].start_time_2_in_seconds, 
											!(plight_created), 
											preason_for_change, 
											pbox_index_0, 
											pset_change_bits, 
											lchange_bitfield_to_set
										 #ifdef _MSC_VER
										 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
										 #endif
									   );
				nm_LIGHTS_set_stop_time_1( llight, 
										   day_index,
										   ptemporary_group->daily_schedule[ day_index ].stop_time_1_in_seconds, 
										   !(plight_created), 
										   preason_for_change, 
										   pbox_index_0, 
										   pset_change_bits, 
										   lchange_bitfield_to_set
										 #ifdef _MSC_VER
										 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
										 #endif
									   );
				nm_LIGHTS_set_stop_time_2( llight, 
										   day_index,
										   ptemporary_group->daily_schedule[ day_index ].stop_time_2_in_seconds, 
										   !(plight_created), 
										   preason_for_change, 
										   pbox_index_0, 
										   pset_change_bits, 
										   lchange_bitfield_to_set
										 #ifdef _MSC_VER
										 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
										 #endif
									   );
			}
		}
	}
	#ifndef _MSC_VER
	else
	{
		Alert_group_not_found();
	}
	#endif
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex Calling function must have the list_lights_recursive_MUTEX mutex to
 *  	  ensure no list items are added or removed during this process.
 *
 * @executed Executed within the context of the CommMngr task as a token or
 *           token response is being built.
 * 
 * @param pucp A pointer to an unsigned character pointer which contains the
 *             information that was received.
 * @param preason_for_change The reason for the change, such as keypad.
 * 
 * @return UNS_32 The total size of the extracted data. This is used to
 *                increment the ucp in the calling function.
 *
 * @author 4/5/2012 Adrianusv
 *
 * @revisions
 *    4/5/2012 Initial release
 *    7/20/2015 mpd Cloned POC function and adapted for LIGHTS.
 */
extern UNS_32 nm_LIGHTS_extract_and_store_changes_from_comm( const unsigned char *pucp,
														  const UNS_32 preason_for_change,
														  const BOOL_32 pset_change_bits,
														  const UNS_32 pchanges_received_from
														  #ifdef _MSC_VER
														  , char *pSQL_statements,
														  char *pSQL_search_condition_str,
														  char *pSQL_update_str,
														  char *pSQL_insert_fields_str,
														  char *pSQL_insert_values_str,
														  char *pSQL_single_merge_statement_buf,
														  const UNS_32 pnetwork_ID
														  #endif
														)
{
	CHANGE_BITS_32_BIT	lbitfield_of_changes;

	LIGHT_STRUCT 	*ltemporary_light;

	#ifndef _MSC_VER

	LIGHT_STRUCT 	*lmatching_light;

	#endif

	UNS_32	lnum_changed_lights;

	UNS_32	lbox_index;

	UNS_32	loutput_index;

	UNS_32  day_index;

	UNS_32	lsize_of_bitfield;

	BOOL_32	llight_created;

	UNS_32	i;

	UNS_32	rv;

	// ----------

	// 7/10/2014 rmd : This function is used when parsing either a token or token resp. When a
	// light is created or modified. This function can be thought about as executed at either the
	// FOAL side or IRRI side ie at masters or slaves. It happens at both.
	
	// ----------
	
	rv = 0;

	llight_created = ( false );

	// ----------

	// Extract the number of changed groups
	memcpy( &lnum_changed_lights, pucp, sizeof( lnum_changed_lights ) );
	pucp += sizeof( lnum_changed_lights );
	rv += sizeof( lnum_changed_lights );

	/*
	// 8/8/2016 rmd : For debug only.
	if( pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE )
	{
		Alert_Message_va( "rcvd from slave: LIGHTS CHANGES for %u groups", lnum_changed_lights );
	}
	else
	if( pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER )
	{
		Alert_Message_va( "rcvd from master: LIGHTS CHANGES for %u groups", lnum_changed_lights );
	}
	else
	{
		Alert_Message_va( "rcvd from UNK: LIGHTS CHANGES for %u groups", lnum_changed_lights );
	}
	*/
	
	// ----------
	
	for( i = 0; i < lnum_changed_lights; ++i )
	{
		// 7/17/2015 mpd : Extract the GID, box, and output index values from the header. These are needed to uniquely 
		// identify this light. 
		//memcpy( &llight_gid, pucp, sizeof( llight_gid ) );
		//pucp += sizeof( llight_gid );
		//rv += sizeof( llight_gid );

		memcpy( &lbox_index, pucp, sizeof( lbox_index ) );
		pucp += sizeof( lbox_index );
		rv += sizeof( lbox_index );

		memcpy( &loutput_index, pucp, sizeof( loutput_index ) );
		pucp += sizeof( loutput_index );
		rv += sizeof( loutput_index );

		memcpy( &lsize_of_bitfield, pucp, sizeof(lsize_of_bitfield) );
		pucp += sizeof( lsize_of_bitfield );
		rv += sizeof( lsize_of_bitfield );

		memcpy( &lbitfield_of_changes, pucp, sizeof(lbitfield_of_changes) );
		pucp += sizeof( lbitfield_of_changes );
		rv += sizeof( lbitfield_of_changes );

		// ----------

		#ifndef _MSC_VER

			// Find the existing group with the group ID
			lmatching_light = nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index( lbox_index, loutput_index );

			// 3/6/2013 ajv : If the specified group doesn't exist yet, we need to
			// create it and assign it the group ID.
			if( lmatching_light == NULL )
			{
				lmatching_light = nm_LIGHTS_create_new_group( lbox_index, loutput_index );

				llight_created = ( true );

				// Force the new Group ID to the passed-in group ID.
				//lmatching_light->base.group_identity_number = llight_gid;

				// ----------
				
				if( pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER )
				{
					// 5/7/2014 ajv : If we don't want to set the change bits that
					// means we called this function via a TOKEN receipt. And any
					// LIGHT creation would be for LIGHTs not at this box. And therefore
					// we don't have to resend out these changes. The distribution
					// of this activity creating LIGHTs and assigning the values STOPS
					// here in this case.
					SHARED_clear_all_32_bit_change_bits( &lmatching_light->changes_to_send_to_master, list_lights_recursive_MUTEX );

					//Alert_Message_va( "PDATA: LIGHT %u (idx: %d, typ: %d) created cleared", llight_id, lbox_index, llight_type );
				}
				else
				{
					//Alert_Message_va( "PDATA: LIGHT %u (idx: %d, typ: %d) created", llight_id, lbox_index, llight_type );
				}
			}
			else
			{
				//Alert_Message_va( "PDATA: LIGHT %u (idx: %d, typ: %d) found 0x%08x", llight_id, lbox_index, llight_type, lbitfield_of_changes );
			}

		#endif

		// ----------

		#ifndef _MSC_VER

		if( lmatching_light != NULL )

		#endif
		{
			// Allocate memory for, and initialize, the temporary group
			#ifndef _MSC_VER
				ltemporary_light = mem_malloc( sizeof(LIGHT_STRUCT) );
			#else
				ltemporary_light = malloc( sizeof(LIGHT_STRUCT) );
			#endif

			memset( ltemporary_light, 0x00, sizeof(LIGHT_STRUCT) );

			#ifndef _MSC_VER

				// Make a copy of the group. We do this so we can change the settings
				// and pass it to the store_changes routine which will actually update
				// the group's settings.
				memcpy( ltemporary_light, lmatching_light, sizeof(LIGHT_STRUCT) );

			#else

				// 5/1/2015 ajv : Since we don't copy an existing LIGHT into the temporary one when
				// parsing using the CommServer, make sure we set the identifing variables manually
				// based on what came in at the beginning of the memory block.
				//ltemporary_light->base.group_identity_number = llight_gid;
				ltemporary_light->box_index_0 = lbox_index;
				ltemporary_light->output_index_0 = loutput_index;

			#endif

			#if 0
			if( B_IS_SET( lbitfield_of_changes, LIGHTS_CHANGE_BITFIELD_date_bit ) == ( true ) )
			{
				memcpy( &ltemporary_light->todays_date, pucp, sizeof( ltemporary_light->todays_date ) );
				pucp += sizeof( ltemporary_light->todays_date );
				rv += sizeof( ltemporary_light->todays_date );
			}

			if( B_IS_SET( lbitfield_of_changes, LIGHTS_CHANGE_BITFIELD_day_index ) == ( true ) )
			{
				memcpy( &ltemporary_light->todays_day_index, pucp, sizeof( ltemporary_light->todays_day_index ) );
				pucp += sizeof( ltemporary_light->todays_day_index );
				rv += sizeof( ltemporary_light->todays_day_index );
			}
			#endif

			if( B_IS_SET( lbitfield_of_changes, LIGHTS_CHANGE_BITFIELD_description_bit ) == ( true ) )
			{
				memcpy( &ltemporary_light->base.description, pucp, NUMBER_OF_CHARS_IN_A_NAME );
				pucp += NUMBER_OF_CHARS_IN_A_NAME;
				rv += NUMBER_OF_CHARS_IN_A_NAME;
			}

			// 7/17/2015 mpd : This field is redundant. It's in the header. Repeat it here to account for all fields in
			// the struct. 
			if( B_IS_SET( lbitfield_of_changes, LIGHTS_CHANGE_BITFIELD_box_index_bit ) == ( true ) )
			{
				memcpy( &ltemporary_light->box_index_0, pucp, sizeof( ltemporary_light->box_index_0 ) );
				pucp += sizeof( ltemporary_light->box_index_0 );
				rv += sizeof( ltemporary_light->box_index_0 );
			}

			// 7/17/2015 mpd : This field is redundant. It's in the header. Repeat it here to account for all fields in
			// the struct. 
			if( B_IS_SET( lbitfield_of_changes, LIGHTS_CHANGE_BITFIELD_output_index_bit ) == ( true ) )
			{
				memcpy( &ltemporary_light->output_index_0, pucp, sizeof( ltemporary_light->output_index_0 ) );
				pucp += sizeof( ltemporary_light->output_index_0 );
				rv += sizeof( ltemporary_light->output_index_0 );
			}

			if( B_IS_SET( lbitfield_of_changes, LIGHTS_CHANGE_BITFIELD_physically_available_bit ) == ( true ) )
			{
				memcpy( &ltemporary_light->light_physically_available, pucp, sizeof( ltemporary_light->light_physically_available ) );
				pucp += sizeof( ltemporary_light->light_physically_available );
				rv += sizeof( ltemporary_light->light_physically_available );
			}

			for( day_index = 0; day_index < LIGHTS_SCHEDULE_DAYS_MAX; ++day_index )
			{
				// 7/17/2015 mpd : Only extract days if there has been a change.
				if( B_IS_SET( lbitfield_of_changes, ( LIGHTS_CHANGE_BITFIELD_day_00_bit + day_index ) ) == ( true ) )
				{
					memcpy( &ltemporary_light->daily_schedule[ day_index ], pucp, sizeof( ltemporary_light->daily_schedule[ day_index ] ) );
					pucp += sizeof( ltemporary_light->daily_schedule[ day_index ] );
					rv += sizeof( ltemporary_light->daily_schedule[ day_index ] );
				}
			}

			// ----------

			#ifdef _MSC_VER

				// 1/16/2015 ajv : Make sure the strings are fully initialized to NULL since we rely on this
				// to determine whether the string is empty or not.
				memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

				// 1/19/2015 ajv : Insert the network ID in case the LIGHT doesn't exist in the database yet.
				SQL_MERGE_build_upsert_uint32( pnetwork_ID, SQL_MERGE_include_field_name_in_insert_clause, "NetworkID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

			#endif

			// ----------

			nm_LIGHTS_store_changes( ltemporary_light, llight_created, preason_for_change, 0, pset_change_bits, pchanges_received_from, lbitfield_of_changes
									 #ifdef _MSC_VER
									 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
									 #endif
								   );
			#ifndef _MSC_VER

				mem_free( ltemporary_light );

			#else

				SQL_MERGE_build_search_condition_uint32( "NetworkID", pnetwork_ID, pSQL_search_condition_str );
				SQL_MERGE_build_search_condition_uint32( "BoxIndex", ltemporary_light->box_index_0, pSQL_search_condition_str );
				SQL_MERGE_build_search_condition_uint32( "OutputIndex", ltemporary_light->output_index_0, pSQL_search_condition_str );

				SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements( DATABASE_TABLE_NAME_LIGHTS, pSQL_search_condition_str, SQL_MERGE_include_field_name_in_insert_clause, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf );

				// ----------

				free( ltemporary_light );

			#endif
		}
		#ifndef _MSC_VER
		else
		{
			Alert_group_not_found();
		}
		#endif
	}

	#ifndef _MSC_VER

		// Only save the file if we've extracted information out of the ucp. This is
		// a sanity check to ensure the bit is only set if data was included.
		if( rv > 0 )
		{
			// 5/13/2015 ajv : We want to save the file in the following cases:
			// 
			//   - A message from the CommServer since we've responded that we received it and need to
			//     ensure it's saved.
			//   - A token response from the slave to the master
			//   - A token from the master to the slave
			//   
			// We DO NOT want to save the file when a token is received by the master since we've
			// already saved the file for one of the reasons above. This will otherwise result in a
			// double save for each change - once when it comes in and once when it's received when the
			// master distributes the changes back out.
			if( (pchanges_received_from == CHANGE_REASON_CENTRAL_OR_MOBILE) ||
				(pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE) ||
				((pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER) && (!FLOWSENSE_we_are_a_master_one_way_or_another())) )
			{
				// 12/10/2014 ajv : Schedule a save rather than saving immediately to prevent stacking of
				// saves. Additionally, set a relatively short time to hel cover cases where users make a
				// lot of changes in a short period.
				FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_LIGHTS, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
			}
		}
		else
		{
			Alert_bit_set_with_no_data();
		}

	#endif

	return( rv );
}

#ifdef _MSC_VER

/* ---------------------------------------------------------- */
extern INT_32 LIGHTS_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order )
{
	LIGHT_STRUCT	*llight;

	INT_32	rv;

	// ----------

	// 4/15/2016 ajv : In most cases, we're going to pass back an array_index of 0. However, in
	// the event the value in question is part of an array, we're going to pass back the actual
	// index so that the CommServer can sort not just on the bit number, but also the
	// array_index for values that share the same bit number.
	*psecondary_sort_order = 0;

	rv = -1;

	// ----------

	if( strncmp( pfield_name, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_description_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = LIGHTS_CHANGE_BITFIELD_description_bit;
		*psize_of_var_ptr = sizeof(llight->base.description);
	}
	else if( strncmp( pfield_name, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_box_index_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = LIGHTS_CHANGE_BITFIELD_box_index_bit;
		*psize_of_var_ptr = sizeof(llight->box_index_0);
	}
	else if( strncmp( pfield_name, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_output_index_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = LIGHTS_CHANGE_BITFIELD_output_index_bit;
		*psize_of_var_ptr = sizeof(llight->output_index_0);
	}
	else if( strncmp( pfield_name, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_physically_available_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = LIGHTS_CHANGE_BITFIELD_physically_available_bit;
		*psize_of_var_ptr = sizeof(llight->light_physically_available);
	}
	else if( strncmp( pfield_name, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_day_00_bit ], 6 ) == 0 )
	{
		// 9/21/2015 wjb : Note the above condition is not checking the entire length of the field
		// name. This is because the string that Wissam wants is Day00_ instead of
		// Day00_StartTime1. By only comparing the first 6 characters, we can find it properly.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "StartTime1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "StartTime2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "StopTime1" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "StopTime2" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}

		rv = LIGHTS_CHANGE_BITFIELD_day_00_bit;
		*psize_of_var_ptr = sizeof(llight->daily_schedule[0].start_time_1_in_seconds);
	}
	else if( strncmp( pfield_name, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_day_01_bit ], 6 ) == 0 )
	{
		// 9/21/2015 wjb : Note the above condition is not checking the entire length of the field
		// name. This is because the string that Wissam wants is Day00_ instead of
		// Day01_StartTime1. By only comparing the first 6 characters, we can find it properly.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "StartTime1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "StartTime2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "StopTime1" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "StopTime2" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}

		rv = LIGHTS_CHANGE_BITFIELD_day_01_bit;
		*psize_of_var_ptr = sizeof(llight->daily_schedule[1].start_time_1_in_seconds);
	}
	else if( strncmp( pfield_name, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_day_02_bit ], 6 ) == 0 )
	{
		// 9/21/2015 wjb : Note the above condition is not checking the entire length of the field
		// name. This is because the string that Wissam wants is Day00_ instead of
		// Day02_StartTime1. By only comparing the first 6 characters, we can find it properly.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "StartTime1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "StartTime2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "StopTime1" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "StopTime2" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}

		rv = LIGHTS_CHANGE_BITFIELD_day_02_bit;
		*psize_of_var_ptr = sizeof(llight->daily_schedule[2].start_time_1_in_seconds);
	}
	else if( strncmp( pfield_name, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_day_03_bit ], 6 ) == 0 )
	{
		// 9/21/2015 wjb : Note the above condition is not checking the entire length of the field
		// name. This is because the string that Wissam wants is Day00_ instead of
		// Day03_StartTime1. By only comparing the first 6 characters, we can find it properly.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "StartTime1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "StartTime2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "StopTime1" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "StopTime2" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}

		rv = LIGHTS_CHANGE_BITFIELD_day_03_bit;
		*psize_of_var_ptr = sizeof(llight->daily_schedule[3].start_time_1_in_seconds);
	}
	else if( strncmp( pfield_name, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_day_04_bit ], 6 ) == 0 )
	{
		// 9/21/2015 wjb : Note the above condition is not checking the entire length of the field
		// name. This is because the string that Wissam wants is Day00_ instead of
		// Day04_StartTime1. By only comparing the first 6 characters, we can find it properly.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "StartTime1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "StartTime2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "StopTime1" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "StopTime2" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}

		rv = LIGHTS_CHANGE_BITFIELD_day_04_bit;
		*psize_of_var_ptr = sizeof(llight->daily_schedule[4].start_time_1_in_seconds);
	}
	else if( strncmp( pfield_name, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_day_05_bit ], 6 ) == 0 )
	{
		// 9/21/2015 wjb : Note the above condition is not checking the entire length of the field
		// name. This is because the string that Wissam wants is Day00_ instead of
		// Day05_StartTime1. By only comparing the first 6 characters, we can find it properly.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "StartTime1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "StartTime2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "StopTime1" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "StopTime2" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}

		rv = LIGHTS_CHANGE_BITFIELD_day_05_bit;
		*psize_of_var_ptr = sizeof(llight->daily_schedule[5].start_time_1_in_seconds);
	}
	else if( strncmp( pfield_name, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_day_06_bit ], 6 ) == 0 )
	{
		// 9/21/2015 wjb : Note the above condition is not checking the entire length of the field
		// name. This is because the string that Wissam wants is Day00_ instead of
		// Day06_StartTime1. By only comparing the first 6 characters, we can find it properly.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "StartTime1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "StartTime2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "StopTime1" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "StopTime2" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}

		rv = LIGHTS_CHANGE_BITFIELD_day_06_bit;
		*psize_of_var_ptr = sizeof(llight->daily_schedule[6].start_time_1_in_seconds);
	}
	else if( strncmp( pfield_name, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_day_07_bit ], 6 ) == 0 )
	{
		// 9/21/2015 wjb : Note the above condition is not checking the entire length of the field
		// name. This is because the string that Wissam wants is Day00_ instead of
		// Day07_StartTime1. By only comparing the first 6 characters, we can find it properly.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "StartTime1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "StartTime2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "StopTime1" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "StopTime2" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}

		rv = LIGHTS_CHANGE_BITFIELD_day_07_bit;
		*psize_of_var_ptr = sizeof(llight->daily_schedule[7].start_time_1_in_seconds);
	}
	else if( strncmp( pfield_name, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_day_08_bit ], 6 ) == 0 )
	{
		// 9/21/2015 wjb : Note the above condition is not checking the entire length of the field
		// name. This is because the string that Wissam wants is Day00_ instead of
		// Day08_StartTime1. By only comparing the first 6 characters, we can find it properly.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "StartTime1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "StartTime2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "StopTime1" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "StopTime2" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}

		rv = LIGHTS_CHANGE_BITFIELD_day_08_bit;
		*psize_of_var_ptr = sizeof(llight->daily_schedule[8].start_time_1_in_seconds);
	}
	else if( strncmp( pfield_name, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_day_09_bit ], 6 ) == 0 )
	{
		// 9/21/2015 wjb : Note the above condition is not checking the entire length of the field
		// name. This is because the string that Wissam wants is Day00_ instead of
		// Day09_StartTime1. By only comparing the first 6 characters, we can find it properly.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "StartTime1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "StartTime2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "StopTime1" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "StopTime2" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}

		rv = LIGHTS_CHANGE_BITFIELD_day_09_bit;
		*psize_of_var_ptr = sizeof(llight->daily_schedule[9].start_time_1_in_seconds);
	}
	else if( strncmp( pfield_name, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_day_10_bit ], 6 ) == 0 )
	{
		// 9/21/2015 wjb : Note the above condition is not checking the entire length of the field
		// name. This is because the string that Wissam wants is Day00_ instead of
		// Day10_StartTime1. By only comparing the first 6 characters, we can find it properly.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "StartTime1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "StartTime2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "StopTime1" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "StopTime2" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}

		rv = LIGHTS_CHANGE_BITFIELD_day_10_bit;
		*psize_of_var_ptr = sizeof(llight->daily_schedule[10].start_time_1_in_seconds);
	}
	else if( strncmp( pfield_name, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_day_11_bit ], 6 ) == 0 )
	{
		// 9/21/2015 wjb : Note the above condition is not checking the entire length of the field
		// name. This is because the string that Wissam wants is Day00_ instead of
		// Day11_StartTime1. By only comparing the first 6 characters, we can find it properly.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "StartTime1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "StartTime2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "StopTime1" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "StopTime2" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}

		rv = LIGHTS_CHANGE_BITFIELD_day_11_bit;
		*psize_of_var_ptr = sizeof(llight->daily_schedule[11].start_time_1_in_seconds);
	}
	else if( strncmp( pfield_name, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_day_12_bit ], 6 ) == 0 )
	{
		// 9/21/2015 wjb : Note the above condition is not checking the entire length of the field
		// name. This is because the string that Wissam wants is Day00_ instead of
		// Day12_StartTime1. By only comparing the first 6 characters, we can find it properly.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "StartTime1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "StartTime2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "StopTime1" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "StopTime2" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}

		rv = LIGHTS_CHANGE_BITFIELD_day_12_bit;
		*psize_of_var_ptr = sizeof(llight->daily_schedule[12].start_time_1_in_seconds);
	}
	else if( strncmp( pfield_name, LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_day_13_bit ], 6 ) == 0 )
	{
		// 9/21/2015 wjb : Note the above condition is not checking the entire length of the field
		// name. This is because the string that Wissam wants is Day00_ instead of
		// Day13_StartTime1. By only comparing the first 6 characters, we can find it properly.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "StartTime1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "StartTime2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "StopTime1" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "StopTime2" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}

		rv = LIGHTS_CHANGE_BITFIELD_day_13_bit;
		*psize_of_var_ptr = sizeof(llight->daily_schedule[13].start_time_1_in_seconds);
	}

	return( rv );

}

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

