/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for abs
#include	<stdlib.h>

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"cs_common.h"

#include	"stations.h"

#include	"app_startup.h"

#include	"bithacks.h"

#include	"change.h"

#include	"cs3000_comm_server_common.h"

#include	"cs3000_tpmicro_common.h"

#include	"e_stations.h"

#include	"epson_rx_8025sa.h"

#include	"flash_storage.h"

#include	"foal_comm.h"

#include	"irri_time.h"

#include	"lcd_init.h"

#include	"irrigation_system.h"

#include	"station_groups.h"

#include	"pdata_changes.h"

// TODO Only necessary for now until we get Min and Max decoder serial numbers.
#include	"poc.h"

#include	"r_alerts.h"

#include	"range_check.h"

#include	"shared.h"

#include	"manual_programs.h"

#include	"flowsense.h"

#include	"comm_mngr.h"

#include	"cs_mem.h"

#include	"e_station_selection_grid.h"

#include	"tpmicro_data.h"

#include	"shared_stations.c"

#include	"controller_initiated.h"

#include	"watersense.h"

#include	"background_calculations.h"

#include	"ftimes_funcs.h"

#include	"foal_irri.h"

#include	"chain_sync_vars.h"

#include	"chain_sync_funcs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;


	BOOL_32				physically_available;

	BOOL_32				in_use_bool;

	UNS_32				station_number_0;

	UNS_32				box_index_0;

	UNS_32				decoder_serial_number;

	UNS_32				decoder_output;

	UNS_32				total_run_minutes_10u;
	UNS_32				cycle_minutes_10u;
	UNS_32				soak_minutes;
	UNS_32				et_factor_100u;

	UNS_32				expected_flow_rate_gpm;

	UNS_32				distribution_uniformity_100u;

	UNS_32				no_water_days;

	UNS_32				manual_program_A_seconds;
	UNS_32				manual_program_B_seconds;

	UNS_32				GID_irrigation_system;
	UNS_32				GID_station_group;

	UNS_32				GID_manual_program_A;
	UNS_32				GID_manual_program_B;

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

} STATION_STRUCT_REV_1;

// ----------

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	BOOL_32				physically_available;

	BOOL_32				in_use_bool;

	UNS_32				station_number_0;

	UNS_32				box_index_0;

	UNS_32				decoder_serial_number;

	UNS_32				decoder_output;

	UNS_32				total_run_minutes_10u;
	UNS_32				cycle_minutes_10u;
	UNS_32				soak_minutes;
	UNS_32				et_factor_100u;

	UNS_32				expected_flow_rate_gpm;

	UNS_32				distribution_uniformity_100u;

	UNS_32				no_water_days;

	UNS_32				manual_program_A_seconds;
	UNS_32				manual_program_B_seconds;

	UNS_32				GID_irrigation_system;
	UNS_32				GID_station_group;

	UNS_32				GID_manual_program_A;
	UNS_32				GID_manual_program_B;

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

	UNS_32				moisture_balance_100000u;

} STATION_STRUCT_REV_2;

// ----------

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	BOOL_32				physically_available;
	
	BOOL_32				in_use_bool;

	UNS_32				station_number_0;

	UNS_32				box_index_0;
	
	UNS_32				decoder_serial_number;

	UNS_32				decoder_output;

	UNS_32				total_run_minutes_10u;
	UNS_32				cycle_minutes_10u;
	UNS_32				soak_minutes;
	UNS_32				et_factor_100u;

	UNS_32				expected_flow_rate_gpm;

	UNS_32				distribution_uniformity_100u;

	UNS_32				no_water_days;

	UNS_32				manual_program_A_seconds;
	UNS_32				manual_program_B_seconds;

	UNS_32				GID_irrigation_system;
	UNS_32				GID_station_group;

	UNS_32				GID_manual_program_A;
	UNS_32				GID_manual_program_B;

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

	UNS_32				moisture_balance_100000u;

	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

} STATION_STRUCT_REV_3;

// ----------

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	BOOL_32				physically_available;
	
	BOOL_32				in_use_bool;

	UNS_32				station_number_0;

	UNS_32				box_index_0;
	
	UNS_32				decoder_serial_number;

	UNS_32				decoder_output;

	UNS_32				total_run_minutes_10u;
	UNS_32				cycle_minutes_10u;
	UNS_32				soak_minutes;
	UNS_32				et_factor_100u;

	UNS_32				expected_flow_rate_gpm;

	UNS_32				distribution_uniformity_100u;

	UNS_32				no_water_days;

	UNS_32				manual_program_A_seconds;
	UNS_32				manual_program_B_seconds;

	UNS_32				GID_irrigation_system;
	UNS_32				GID_station_group;

	UNS_32				GID_manual_program_A;
	UNS_32				GID_manual_program_B;

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

	UNS_32				moisture_balance_100000u;

	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

	UNS_32				square_footage;

} STATION_STRUCT_REV_4;

// ----------

// 1/26/2015 rmd : NOTE - upon REVISION update the record can only grow in size. At least
// that is the way I've coded the flash file supporting functions. It may be possible to
// decrease the record size. I just haven't gone through the code with that in mind.
#define	STATION_INFO_LATEST_FILE_REVISION		(7)

// ----------

const UNS_32 station_info_list_item_sizes[ STATION_INFO_LATEST_FILE_REVISION + 1 ] =
{
	// 1/26/2015 rmd : In order for this array to contain OLDER list item sizes the implication
	// is that the older structure definitions are to be retained. That means the newer
	// structure definition has a different name.

	sizeof( STATION_STRUCT_REV_1 ),

	// 2/4/2015 rmd : Revision 1 is the same size. Needed a variable set. See updater below.
	sizeof( STATION_STRUCT_REV_1 ),

	sizeof( STATION_STRUCT_REV_2 ),

	sizeof( STATION_STRUCT_REV_3 ),

	sizeof( STATION_STRUCT_REV_4 ),

	sizeof( STATION_STRUCT ),

	// 4/15/2016 ajv : Revision 6 is the same size. We're using this opportunity to set each
	// station's moisture balance back to 50% now that the run time has been improved.
	sizeof( STATION_STRUCT ),

	// 4/7/2017 ajv : Revision 7 is the same size. We're repurposing a 32-bit variable to be
	// used as the station's clear moisture balance flag as well as initializing it if the MB is
	// less than 50%.
	sizeof( STATION_STRUCT )
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static const char STATION_INFO_FILENAME[] =		"STATION INFORMATION";

static const char STATION_NAME[] =				"Station";

MIST_LIST_HDR_TYPE								station_info_list_hdr;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void nm_STATION_set_default_values_resulting_in_station_0_for_this_box( void *const pstation, const BOOL_32 pset_change_bits );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void nm_station_structure_updater( UNS_32 pfrom_revision )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only when
	// the file revision is changed. The caller is to be holding the list_program_data
	// recursive_MUTEX.

	// ----------
	
	STATION_STRUCT		*lss;
	
	CHANGE_BITS_32_BIT	*lbitfield_of_changes;

	UNS_32	box_index_0;

	box_index_0 = FLOWSENSE_get_controller_index();

	// ----------
	
	if( pfrom_revision == STATION_INFO_LATEST_FILE_REVISION )
	{
		Alert_Message_va( "STATIONS file unexpd update %u", pfrom_revision );	
	}
	else
	{
		Alert_Message_va( "STATIONS file update : to revision %u from %u", STATION_INFO_LATEST_FILE_REVISION, pfrom_revision );

		// ----------
		
		if( pfrom_revision == 0 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 0 to REVISION 1. Because
			// of revision 0 bugs the upload to comm_server bits are in an unknown state. They need to
			// be set to trigger a complete send of PDATA to the comm_server.

			lss = nm_ListGetFirst( &station_info_list_hdr );
		
			while( lss != NULL )
			{
				// 1/30/2015 rmd : Because of revision 0 bugs these bits were not set. We need to set them
				// to guarantee all the program data is sent to the comm_server this one time.
				lss->changes_to_upload_to_comm_server = UNS_32_MAX;
				
				// ----------
				
				lss = nm_ListGetNext( &station_info_list_hdr, lss );
			}
			
			// ----------
			
			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 1 to 2 changes if
			// needed.
			pfrom_revision += 1;
		}
		
		// ----------
		
		if( pfrom_revision == 1 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 1 to REVISION 2.
			

			lss = nm_ListGetFirst( &station_info_list_hdr );

			while( lss != NULL )
			{
				lbitfield_of_changes = &lss->changes_to_send_to_master;

				// 3/16/2015 ajv : The WaterSense protocol specification indicates that the moisture balance
				// calculation starts with one-half full root zone. Therefore, set the moisture balance
				// accordingly.
				nm_STATION_set_moisture_balance_percent( lss, STATION_MOISTURE_BALANCE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				lss = nm_ListGetNext( &station_info_list_hdr, lss );
			}

			pfrom_revision += 1;
		}

		// ----------
		
		if( pfrom_revision == 2 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 2 to REVISION 3.
			lss = nm_ListGetFirst( &station_info_list_hdr );

			while( lss != NULL )
			{
				// 4/30/2015 ajv : Clear all of the pending Program Data bits.
				lss->changes_uploaded_to_comm_server_awaiting_ACK = CHANGE_BITS_32_BIT_ALL_CLEAR;

				lss = nm_ListGetNext( &station_info_list_hdr, lss );
			}

			pfrom_revision += 1;
		}

		// ----------
		
		if( pfrom_revision == 3 )
		{
			// 6/25/2015 ajv : This is the work to do when moving from REVISION 3 to REVISION 4.
			lss = nm_ListGetFirst( &station_info_list_hdr );

			while( lss != NULL )
			{
				lbitfield_of_changes = &lss->changes_to_send_to_master;

				// 6/25/2015 ajv : Since we're going to allow the user to edit their cycle and soak times
				// when using Daily ET now, we need to copy the values calculated by the WaterSense
				// equations into the physical variables.
				if( WEATHER_get_station_uses_daily_et(lss) )
				{
					nm_STATION_set_cycle_minutes( lss, nm_STATION_get_watersense_cycle_max_10u(lss), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
					nm_STATION_set_soak_minutes( lss, nm_STATION_get_watersense_soak_min(lss), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
				}

				// 7/2/2015 ajv : We've modified moisture balance percent to be stored as a float to better
				// handle a negative moisture balance. Therefore, we're going to reset it to 50% on the
				// update. This is safe to do now since there was no way to modify moisture balance prior to
				// this new release.
				nm_STATION_set_moisture_balance_percent( lss, STATION_MOISTURE_BALANCE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				// 7/8/2015 ajv : And we've introduced a new square footage variable that needs to be
				// initialized.
				nm_STATION_set_square_footage( lss, STATION_SQUARE_FOOTAGE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				lss = nm_ListGetNext( &station_info_list_hdr, lss );
			}

			pfrom_revision += 1;
		}

		// ----------
		
		if( pfrom_revision == 4 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 4 to REVISION 5.

			lss = nm_ListGetFirst( &station_info_list_hdr );

			while( lss != NULL )
			{
				// 12/14/2015 rmd : Set these unused variables to 0 so they contain a known value.
				lss->ignore_moisture_balance_next_irrigation = 0;
				
				lss->no_longer_used_05 = 0;

				lss->no_longer_used_06 = 0;

				// ----------
				
				lss = nm_ListGetNext( &station_info_list_hdr, lss );
			}

			pfrom_revision += 1;
		}

		// ----------

		if( pfrom_revision == 5 )
		{
			// 4/15/2016 ajv : This is the work to do when moving from REVISION 5 to REVISION 6.
			lss = nm_ListGetFirst( &station_info_list_hdr );

			while( lss != NULL )
			{
				lbitfield_of_changes = &lss->changes_to_send_to_master;

				// 4/15/2016 ajv : We had some logic errors in our run time calculation which may have
				// resulted in the moisture balance being unexpectedly high (as seen in Milpitas) or low (as
				// seen at Jim's and Richard's houses). Therefore, now that we've corrected the errors,
				// reset the balance back to 50% when this station irrigates next. Since we don't trust the
				// current value, we'll also ignore the current moisture balance and irrigate no matter
				// what.
				nm_STATION_set_ignore_moisture_balance_at_next_irrigation( lss, STATION_MOISTURE_BALANCE_SET_IGNORE_FLAG, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				// ----------

				lss = nm_ListGetNext( &station_info_list_hdr, lss );
			}

			pfrom_revision += 1;
		}

		// ----------

		if( pfrom_revision == 6 )
		{
			// 4/7/2017 ajv : This is the work to do when moving from REVISION 6 to REVISION 7.
			lss = nm_ListGetFirst( &station_info_list_hdr );

			while( lss != NULL )
			{
				lbitfield_of_changes = &lss->changes_to_send_to_master;

				// 4/7/2017 ajv : Since we're unsure of what this value may be, we want to explicity force
				// it false. HOWEVER, if someone has accumulated rain, do we really want to lose that?
				// Probably not. So let's only reset it to FALSE if the Moisture Balance is less than 50%.
				if( lss->moisture_balance_percent < STATION_MOISTURE_BALANCE_DEFAULT )
				{
					nm_STATION_set_ignore_moisture_balance_at_next_irrigation( lss, STATION_MOISTURE_BALANCE_CLEAR_IGNORE_FLAG, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
				}

				// ----------

				lss = nm_ListGetNext( &station_info_list_hdr, lss );
			}

			pfrom_revision += 1;
		}
	}

	// ----------

	// 1/29/2015 rmd : Well after the updater has run the pfrom_revision is supposed to equal
	// the latest revision. By design of this update process.
	if( pfrom_revision != STATION_INFO_LATEST_FILE_REVISION )
	{
		Alert_Message( "STATIONS updater error" );
	}
}

/* ---------------------------------------------------------- */
extern void init_file_station_info( void )
{
	/**
	 * WARNING: This init function must run AFTER the last group init function. 
	 * Otherwise, stations will be assigned to groups that don't exist since they 
	 * haven't been created yet. 
	 */

	FLASH_FILE_initialize_list_and_find_or_create_group_list_file(	FLASH_INDEX_1_GENERAL_STORAGE,
																	STATION_INFO_FILENAME,
																	STATION_INFO_LATEST_FILE_REVISION,
																	&station_info_list_hdr,
																	station_info_list_item_sizes,
																	list_program_data_recursive_MUTEX,
																	&nm_station_structure_updater,
																	&nm_STATION_set_default_values_resulting_in_station_0_for_this_box,
																	(char*)STATION_NAME,
																	FF_STATION_INFO );
}

/* ---------------------------------------------------------- */
extern void save_file_station_info( void )
{
	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE,
															STATION_INFO_FILENAME,
															STATION_INFO_LATEST_FILE_REVISION,
															&station_info_list_hdr,
															sizeof( STATION_STRUCT ),
															list_program_data_recursive_MUTEX,
														    FF_STATION_INFO );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Sets the default values for a particular station. With the exception of the
 * serial number and the station number. Those must be set seperately. Because 
 * there really isn't such a thing as default values for them.
 *  
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose values are being set isn't deleted during
 *  	  this process.
 *
 * @executed Executed within the context of the STARTUP_TASK. And as this 
 *  		 function is called from the function
 *  		 nm_STATION_create_new_station_for_this_box the notes about the
 *  		 tasks it is invoked from apply (COMM_MNGR, KEY_PROCESS)
 *  
 * @param pstation_voidptr A pointer to the station to set.
 *  
 * @author 10/20/2011 BobD
 * 
 * @revisions
 * 	  10/20/2011  Initial release
 */
static void nm_STATION_set_default_values_resulting_in_station_0_for_this_box( void *const pstation_voidptr, const BOOL_32 pset_change_bits )
{
	STATION_STRUCT	*const	pstation;  // This allows the debugger to display the struct.

	CHANGE_BITS_32_BIT	*lbitfield_of_changes;

	UNS_32	box_index_0;

	box_index_0 = FLOWSENSE_get_controller_index();

	// ------------------------

	// Test if we passed a NULL pointer.
	if( pstation_voidptr != NULL )
	{
		*((STATION_STRUCT**)(&pstation)) = pstation_voidptr;  // Consciously force the assignment through typecasts.

		// 5/14/2014 ajv : Clear all of the change bits for station since it's just
		// been created. We'll then explicitly set each bit based upon whether
		// pset_change_bits is true or not.
		SHARED_clear_all_32_bit_change_bits( &pstation->changes_to_send_to_master, list_program_data_recursive_MUTEX );
		SHARED_clear_all_32_bit_change_bits( &pstation->changes_to_distribute_to_slaves, list_program_data_recursive_MUTEX );
		SHARED_clear_all_32_bit_change_bits( &pstation->changes_uploaded_to_comm_server_awaiting_ACK, list_program_data_recursive_MUTEX );

		// 1/20/2015 ajv : However, we should explicitly set ALL bits for the Comm Server to ensure
		// changes are uploaded on a clean start.
		SHARED_set_all_32_bit_change_bits( &pstation->changes_to_upload_to_comm_server, list_program_data_recursive_MUTEX );

		// ----------

		// 5/2/2014 ajv : When creating a new group, always set the changes to
		// be sent to the master. Once the master receives the changes, it will
		// distribute them back out to the slaves.
		lbitfield_of_changes = &pstation->changes_to_send_to_master;

		// ----------
		
		nm_STATION_set_physically_available( pstation, STATION_PHYSICALLY_AVAILABLE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_set_box_index( pstation, box_index_0, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );  // this box

		nm_STATION_set_station_number( pstation, 0, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );  // station 0

		nm_STATION_set_in_use( pstation, STATION_INUSE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_set_description( pstation, STATION_DEFAULT_DESCRIPTION, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_set_decoder_serial_number( pstation, DECODER_SERIAL_NUM_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_STATION_set_decoder_output( pstation, DECODER_OUTPUT_A_BLACK, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_set_total_run_minutes( pstation, STATION_TOTAL_RUN_MINUTES_DEFAULT_10u, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_STATION_set_cycle_minutes( pstation, STATION_CYCLE_MINUTES_DEFAULT_10u, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_STATION_set_soak_minutes( pstation, STATION_SOAK_MINUTES_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_STATION_set_ET_factor( pstation, STATION_ET_FACTOR_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_set_expected_flow_rate( pstation, STATION_EXPECTED_FLOW_RATE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_set_distribution_uniformity( pstation, STATION_DU_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_set_no_water_days( pstation, STATION_NO_WATER_DAYS_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		// 3/16/2015 ajv : The WaterSense protocol specification indicates that the moisture balance
		// calculation starts with one-half full root zone. Therefore, set the moisture balance
		// accordingly to 50%. This allows us to maintain the same moisture content regardless of
		// what the user adjusts their soil storage capacity to.
		nm_STATION_set_moisture_balance_percent( pstation, STATION_MOISTURE_BALANCE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_set_square_footage( pstation, STATION_SQUARE_FOOTAGE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_set_ignore_moisture_balance_at_next_irrigation( pstation, STATION_MOISTURE_BALANCE_CLEAR_IGNORE_FLAG, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		// ----------

		// 12/13/2012 ajv : By default, we're not going to assign stations to any groups in an
		// attempt to simplify the grouping concept for users since we received feedback that users
		// didn't know what to do when all of the stations were already assigned to a group.
		nm_STATION_set_GID_station_group( pstation, 0, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_STATION_set_GID_manual_program_A( pstation, 0, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_STATION_set_GID_manual_program_B( pstation, 0, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
	}
	else
	{
		Alert_func_call_with_null_ptr();
	}
}

/* ---------------------------------------------------------- */
/**
 * Add a new station to the station list. It is assumed that the station is not 
 * on the list already. We assign the serial number and station number here. 
 * Think of it as part of the default settings. But in this case is special by 
 * station and so must be attended to explicitly. I would think we would 
 * generally not use this function to create stations for boxes other than 
 * ourselves. There is a special case of that when we are faking all 768 
 * stations. In that case we fake up other box serial numbers on the chain. 
 *  
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *        to ensure the list isn't modified during this process.
 *
 * @executed Executed within the context of the STARTUP_TASK. And likely I would 
 *  		 beleive the COMM_MNGR task as it receives messages from the bottom
 *  		 half about station hardware which is present (if the stations do
 *  		 not already exist). Also probably from the KEY_PROCESS task as the
 *  		 user assignes station numbers to newly discovered decoders. Some
 *  		 stations reported up from the bottom half will be created
 *  		 immediately as their station numbers are known. Decoder stations
 *  		 cannot as the user gets to make the assignment! Or does he???
 *  
 * @param pcontroller_serial_number The serial number of the controller the 
 *                                  station is physically connected to. 
 * @param pstation_number_0 The station number to assign to the station.
 * 
 * @return void* A pointer to the newly created station.
 *  
 * @author BobD 10/20/2011
 * 
 * @revisions
 *    10/20/2011 Initial release
 */
extern void *nm_STATION_create_new_station( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, const UNS_32 pchanges_received_from )
{
	STATION_STRUCT		*lstation, *lnext_station;

	CHANGE_BITS_32_BIT	*lbitfield_of_changes;

	lstation = NULL;
	
	// The station list is sorted by box index and station number.
	lnext_station = nm_ListGetFirst( &station_info_list_hdr );

	while( lnext_station != NULL )
	{
		// If the box index of the station to be added is less, stop and insert here.
		if( pbox_index_0 < nm_STATION_get_box_index_0(lnext_station) )
		{
			break;
		}
		else if( pbox_index_0 == nm_STATION_get_box_index_0(lnext_station) )
		{
			// If the station number of the station to be added is greater,
			// stop and insert here.
			if( pstation_number_0 < nm_STATION_get_station_number_0(lnext_station) )
			{
				break;	
			}
		}

		lnext_station = nm_ListGetNext( &station_info_list_hdr, lnext_station );
	}

	// 11/19/2014 ajv : No need to check if lnext_station is NULL. If it is, the new station is
	// automatically added to the end of the list.
	lstation = nm_GROUP_create_new_group( &station_info_list_hdr, (char*)STATION_NAME, &nm_STATION_set_default_values_resulting_in_station_0_for_this_box, sizeof(STATION_STRUCT), (true), lnext_station );

	lbitfield_of_changes = STATION_get_change_bits_ptr( lstation, pchanges_received_from );

	nm_STATION_set_station_number( lstation, pstation_number_0, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, lbitfield_of_changes );
	nm_STATION_set_box_index( lstation, pbox_index_0, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, lbitfield_of_changes );

	return( lstation );
}

/* ---------------------------------------------------------- */
/**
 * This function runs through the station list and copies the contents of any stations that
 * have changed to the passed in ucp.
 * 
 * @executed Executed within the context of the CommMngr task as a token, token response, or
 * Program Data message to the Comm Server is being built.
 * 
 * @param pucp A pointer to an unsigned character pointer which contains the data to be
 * sent.
 * 
 * @param pmem_used_so_far The amount of memory used so far. Once this reaches our limit for
 * a single token (MEMORY_TO_ALLOCATE_FOR_SENDING_CHANGES), processing of the remaining
 * changes stops until it's time to build the next token or token response.
 * 
 * @param pmem_used_so_far_is_less_than_allocated_memory A pointer to the flag which
 * indicates whether we're allowed to add more changes to the token or token response.
 * 
 * @param preason_data_is_being_built The reason why the data is being built to send. Based
 * upon this reason, we examine a different change bit structure to determine what to
 * send. Valid values are CHANGE_REASON_SYNC_SENDING_TO_MASTER,
 * CHANGE_REASON_SYNC_DISTRIBUTING_TO_SLAVES, CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER.
 * 
 * @return UNS_32 The total size of the added data. This is used to determine whether any
 * changes were detected so the appropriate file change bit can be set as well as to
 * determine how much memory must be allocated to store the changes.
 *
 * @author 9/22/2011 Adrianusv
 */
extern UNS_32 STATION_build_data_to_send( UNS_8 **pucp,
										  const UNS_32 pmem_used_so_far,
										  BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
										  const UNS_32 preason_data_is_being_built,
										  const UNS_32 pallocated_memory )
{
	CHANGE_BITS_32_BIT	*lbitfield_of_changes_to_use_to_determine_what_to_send;

	CHANGE_BITS_32_BIT	lbitfield_of_changes_to_send;

	STATION_STRUCT		*lstation;

	UNS_8	*llocation_of_num_stations;

	UNS_8	*llocation_of_bitfield;

	// 1/14/2015 ajv : Track how many stations are added to the message.
	UNS_32	lnum_changed_stations;

	// 1/14/2015 ajv : Store the amount of memory used for current group. This is compared to
	// the group's overhead to determine whether any of the group's values were actually added
	// or not.
	UNS_32	lmem_used_by_this_station;

	// 1/14/2015 ajv : Determine how much overhead each group requires in case we add the
	// overhead and don't have enough room to add of the actual changes.
	UNS_32	lmem_overhead_per_station;

	UNS_32	rv;

	// ----------

	rv = 0;

	lnum_changed_stations = 0;

	// ----------

	// 1/14/2015 ajv : Take the appropriate mutex to ensure no stations are added or removed
	// during this process.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lstation = nm_ListGetFirst( &station_info_list_hdr );

	// 1/14/2015 ajv : Loop through each station and count how many stations have change bits
	// set. This is really just used to determine whether there's at least station which needs
	// to be examined.
	while( lstation != NULL )
	{
		if( *(STATION_get_change_bits_ptr( lstation, preason_data_is_being_built )) != CHANGE_BITS_32_BIT_ALL_CLEAR )
		{
			lnum_changed_stations++;

			// 1/14/2015 ajv : Since we don't actually care about the number of stations that have
			// changed at this point - just that at least one has changed - we can break out of the loop
			// now.
			break;
		}

		lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
	}

	// ----------

	if( lnum_changed_stations > 0 )
	{
		// 1/14/2015 ajv : Initialize the changed stations back to 0. Since not all of the stations may
		// be added due to token size limitations, we need to ensure that the receiving party only
		// processes the number of stations actually added to the message.
		lnum_changed_stations = 0;

		// ----------

		// 1/14/2015 ajv : Allocate space to store the number of changed stations that are included
		// in the message so the receiving party can correctly parse the data. This will be filled
		// in later once we have an actual count.
		rv = PDATA_allocate_space_for_num_changed_groups_in_pucp( pucp, &llocation_of_num_stations, pmem_used_so_far, pmem_used_so_far_is_less_than_allocated_memory, pallocated_memory );

		// ----------

		// 2/9/2016 rmd : Well there was at least enough space for the 2 bytes for the count of
		// number of changed groups.
		if( *pmem_used_so_far_is_less_than_allocated_memory )
		{
			lstation = nm_ListGetFirst( &station_info_list_hdr );

			while( lstation != NULL )
			{
				lbitfield_of_changes_to_use_to_determine_what_to_send = STATION_get_change_bits_ptr( lstation, preason_data_is_being_built );

				// 1/14/2015 ajv : If this station has changed...
				if( *lbitfield_of_changes_to_use_to_determine_what_to_send != CHANGE_BITS_32_BIT_ALL_CLEAR )
				{
					// 2/9/2016 rmd : The OVERHEAD is the box index, the station number, an UNS_32 indicating
					// the size of the bitfield, and the 32-bit change bitfield itself.
					lmem_overhead_per_station = (4 * sizeof(UNS_32));

					// 1/14/2015 ajv : If we still have room available in the message for the station's
					// overhead, add it now.
					if( *pmem_used_so_far_is_less_than_allocated_memory && ((pmem_used_so_far + rv + lmem_overhead_per_station) < pallocated_memory) )
					{
						// If a change was detected, we need to include a number of other settings in the size
						// including box_index, station number, the bit field to indicate which settings changed,
						// and the size of the bit field.
						PDATA_copy_var_into_pucp( pucp, &lstation->box_index_0, sizeof(lstation->box_index_0) );

						PDATA_copy_var_into_pucp( pucp, &lstation->station_number_0, sizeof(lstation->station_number_0) );

						PDATA_copy_bitfield_info_into_pucp( pucp, sizeof(CHANGE_BITS_32_BIT), sizeof(UNS_32), &llocation_of_bitfield );
					}
					else
					{
						// 9/19/2013 ajv : Stop processing stations if we've already filled out the token with the
						// maximum amount of data to send.
						*pmem_used_so_far_is_less_than_allocated_memory = (false);

						break;
					}

					lmem_used_by_this_station = lmem_overhead_per_station;

					// ----------

					// Initialize the bit field
					lbitfield_of_changes_to_send = 0x00;

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_description_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->base.description,
							NUMBER_OF_CHARS_IN_A_NAME,
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_physically_available,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->physically_available,
							sizeof(lstation->physically_available),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_in_use_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->in_use_bool,
							sizeof(lstation->in_use_bool),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_station_number_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->station_number_0,
							sizeof(lstation->station_number_0),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_box_index_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->box_index_0,
							sizeof(lstation->box_index_0),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_decoder_serial_number_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->decoder_serial_number,
							sizeof(lstation->decoder_serial_number),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_decoder_output_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->decoder_output,
							sizeof(lstation->decoder_output),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_total_run_minutes_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->total_run_minutes_10u,
							sizeof(lstation->total_run_minutes_10u),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_cycle_minutes_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->cycle_minutes_10u,
							sizeof(lstation->cycle_minutes_10u),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_soak_minutes_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->soak_minutes,
							sizeof(lstation->soak_minutes),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_et_factor_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->et_factor_100u,
							sizeof(lstation->et_factor_100u),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_expected_flow_rate_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->expected_flow_rate_gpm,
							sizeof(lstation->expected_flow_rate_gpm),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_distribution_uniformity_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->distribution_uniformity_100u,
							sizeof(lstation->distribution_uniformity_100u),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_no_water_days_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->no_water_days,
							sizeof(lstation->no_water_days),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_GID_station_group_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->GID_station_group,
							sizeof(lstation->GID_station_group),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_GID_manual_program_A,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->GID_manual_program_A,
							sizeof(lstation->GID_manual_program_A),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_GID_manual_program_B,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->GID_manual_program_B,
							sizeof(lstation->GID_manual_program_B),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_moisture_balance_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->moisture_balance_percent,
							sizeof(lstation->moisture_balance_percent),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_square_footage_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->square_footage,
							sizeof(lstation->square_footage),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_station += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_CHANGE_BITFIELD_ignore_moisture_balance_flag,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lstation->changes_uploaded_to_comm_server_awaiting_ACK,
							&lstation->ignore_moisture_balance_next_irrigation,
							sizeof(lstation->ignore_moisture_balance_next_irrigation),
							(lmem_used_by_this_station + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					// ----------

					// 2/10/2016 rmd : If we saw any changed bits and ACTUALLY ADDED CONTENT BEYOND THE ORIGINAL
					// OVERHEAD then complete out the message by placing the bitfield in the overhead and
					// updating function housekeeping.
					if( lmem_used_by_this_station > lmem_overhead_per_station )
					{
						// 2/10/2016 rmd : Bump the count of changed list items in the message. Place the bitfield
						// of changes into the overhead. And update the return value.
						lnum_changed_stations += 1;
						
						memcpy( llocation_of_bitfield, &lbitfield_of_changes_to_send, sizeof(lbitfield_of_changes_to_send) );
						
						rv += lmem_used_by_this_station;
					}
					else
					{
						// 2/10/2016 rmd : Else back out from the message the overhead already added.
						*pucp -= lmem_overhead_per_station;
					}
					
				}

				lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
			}

			// ----------
			
			// 1/14/2015 ajv : If we've added the number of changes but didn't actually add any data
			// because we hit our limit, back out the number of changes.
			if( lnum_changed_stations > 0 )
			{
				memcpy( llocation_of_num_stations, &lnum_changed_stations, sizeof(lnum_changed_stations) );
			}
			else
			{
				// 2/10/2016 rmd : If we didn't add any content beyond moving the msg pointer over where the
				// number of changed groups goes, then put the pointer back and return a 0 to indicate
				// nothing added.
				*pucp -= sizeof( lnum_changed_stations );

				rv = 0;
			}
		}
		else
		{
			// 2/11/2016 rmd : We couldn't do anything, meaning couldn't even add in the
			// num_of_changed_sensors. In that case RV is still 0 and pucp hasn't moved so nothing to
			// do here.
		}
	}

	// ----------

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Calculates and copies the estimated run time into the appropriate GuiVar
 * which is used by the easyGUI library on the Station Information screen.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *        to ensure the passed in station is not deleted during this process.
 *
 * @executed Executed within the context of the display processing task when the
 *  		 Station Information screen is being drawn; within the key
 *  		 processing task when the user adjusts their percent of ET.
 * 
 * @param pstation A pointer to the station.
 *
 * @author 7/12/2012 Adrianusv
 *
 * @revisions
 *    7/12/2012 Initial release
 */
extern float nm_STATION_calculate_and_copy_estimated_minutes_into_GuiVar( const STATION_STRUCT *const pstation )
{
	DATE_TIME_COMPLETE_STRUCT	ldt;

	STATION_GROUP_STRUCT	*lschedule;

	float du_as_a_float;

	float rv;

	EPSON_obtain_latest_complete_time_and_date( &ldt );

	// 2/21/2013 ajv : Only get a pointer to the group if the GID is something
	// other than 0. If it's 0, it means it's not assigned to a schedule yet.
	if( pstation->GID_station_group > 0 )
	{
		lschedule = STATION_GROUP_get_group_with_this_GID( pstation->GID_station_group );
	}
	else
	{
		lschedule = NULL;
	}

	// 2/15/2018 rmd : Ensure the conversion is cleanly done. Wasn't comfortable just type
	// casting in the following function parameter list.
	du_as_a_float = GuiVar_StationInfoDU;

	rv = nm_WATERSENSE_calculate_run_minutes_for_this_station( NOT_FOR_FTIMES, (STATION_STRUCT *const)pstation, lschedule, NULL, &ldt, du_as_a_float );

	rv = nm_WATERSENSE_calculate_adjusted_run_time(	NOT_FOR_FTIMES,
													(STATION_STRUCT *const)pstation,
													NULL,
													rv,
													(GuiVar_StationInfoETFactor + 100),
													PERCENT_ADJUST_get_station_percentage_100u( (STATION_STRUCT *const)pstation, ldt.date_time.D ) );

	return( rv );
}

/* ---------------------------------------------------------- */
extern void STATION_find_first_available_station_and_init_station_number_GuiVars( void )
{
	STATION_STRUCT *lstation;

	lstation = NULL;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( nm_ListGetCount( &station_info_list_hdr ) > 0 )
	{
		// Try to get the station we last viewed. This may result in NULL if the
		// GuiVars are uninitialized - such as if we haven't viewed the Stations
		// screen since booting up.
		lstation = nm_STATION_get_pointer_to_station( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1) );

		// If we didn't get a valid station - say if this is the first time we've
		// started and the GuiVars are uninitialized, or if we're viewing the screen
		// when the station we're viewing is removed - grab the first available
		// station.
		if( (lstation == NULL) || (STATION_station_is_available_for_use( lstation ) == (false)) )
		{
			lstation = STATION_get_first_available_station();

			if( lstation != NULL )
			{
				GuiVar_StationInfoBoxIndex = nm_STATION_get_box_index_0( lstation );
				GuiVar_StationInfoNumber = (nm_STATION_get_station_number_0( lstation ) + 1);
				STATION_get_station_number_string( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1), (char*)&GuiVar_StationInfoNumber_str, sizeof(GuiVar_StationInfoNumber_str) );
			}
		}
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	if( lstation == NULL )
	{
		Alert_Message( "No stations available" );
	}
}

/* ---------------------------------------------------------- */
/**
 * Copies the contents of the passed in station into GuiVars which are used for
 * editing by the easyGUI library.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *        to ensure the passed in station is not deleted during this process.
 *
 * @executed Executed within the context of the display processing task when the
 *           Station Information screen is being drawn.
 * 
 * @param pstation A pointer to the station.
 *
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 */
extern void nm_STATION_copy_station_into_guivars( STATION_STRUCT *const pstation )
{
	DATE_TIME_COMPLETE_STRUCT	ldt;

	// ----------

	EPSON_obtain_latest_complete_time_and_date( &ldt );

	if( pstation != NULL)
	{
		g_GROUP_ID = nm_GROUP_get_group_ID( pstation );

		strlcpy( GuiVar_GroupName, nm_GROUP_get_name( pstation ), sizeof(GuiVar_GroupName) );

		GuiVar_StationInfoNumber = (UNS_32)(pstation->station_number_0 + 1);
		STATION_get_station_number_string( GuiVar_StationInfoNumber, (UNS_32)pstation->station_number_0, (char*)&GuiVar_StationInfoNumber_str, sizeof(GuiVar_StationInfoNumber_str) );

		GuiVar_StationInfoBoxIndex = nm_STATION_get_box_index_0( pstation );

		GuiVar_StationDecoderSerialNumber = nm_STATION_get_decoder_serial_number( pstation );
		GuiVar_StationDecoderOutput = nm_STATION_get_decoder_output( pstation );

		GuiVar_StationInfoETFactor = (STATION_get_ET_factor_100u( pstation ) - 100);

		GuiVar_StationInfoTotalMinutes = ((float)STATION_get_total_run_minutes_10u( pstation ) / 10.0F);

		// 3/16/2015 ajv : Note that this is positioned PRIOR to calculating the estimated minutes.
		// This is because DU, or application efficiency, is a necessary part of the calculation
		// and, when populating the screen, we use the GuiVars to perform the calculation.
		GuiVar_StationInfoDU = nm_STATION_get_distribution_uniformity_100u( pstation );

		GuiVar_StationInfoEstMin = nm_STATION_calculate_and_copy_estimated_minutes_into_GuiVar( pstation );

		GuiVar_StationInfoCycleTime = ((float)nm_STATION_get_cycle_minutes_10u( pstation ) / 10.0F);

		GuiVar_StationInfoSoakInTime = nm_STATION_get_soak_minutes( pstation );

		GuiVar_StationInfoExpectedFlow = nm_STATION_get_expected_flow_rate_gpm( pstation );

		GuiVar_StationInfoNoWaterDays = STATION_get_no_water_days( pstation );

		if( WEATHER_get_station_uses_daily_et( pstation ) )
		{
			GuiVar_StationInfoShowPercentOfET = (true);
			GuiVar_StationInfoShowEstMin = (true);
		}
		else
		{
			GuiVar_StationInfoShowPercentOfET = (false);
			GuiVar_StationInfoShowEstMin = (false);
		}
		
		GuiVar_StationInfoMoistureBalancePercent = (STATION_get_moisture_balance_percent( pstation ) * 100.0F);

		GuiVar_StationInfoMoistureBalance = STATION_get_moisture_balance( pstation );

		GuiVar_StationInfoSquareFootage = STATION_get_square_footage( pstation );
	
		// ---------------------

		UNS_32	lpercent_adjust;

		UNS_32 	lpercent_adjust_days;
				
		lpercent_adjust = PERCENT_ADJUST_get_station_percentage_100u( pstation, ldt.date_time.D );

		lpercent_adjust_days = PERCENT_ADJUST_get_station_remaining_days( pstation, ldt.date_time.D );

		// 3/21/2016 skc : Only show the message if Percent Adjust has a value and is set to run for more than zero days.
		if( (lpercent_adjust != 100) && (lpercent_adjust_days > 0) )
		{
			GuiVar_StationInfoShowPercentAdjust = (true);

			GuiVar_StationInfoShowEstMin = (true);

			GuiVar_PercentAdjustPercent = (abs((INT_32)(lpercent_adjust - 100)));

			GuiVar_PercentAdjustPositive = (lpercent_adjust > 100);

			GuiVar_PercentAdjustNumberOfDays = lpercent_adjust_days;
		}
		else
		{
			GuiVar_StationInfoShowPercentAdjust = (false);
		}

		// ----------

		// 7/8/2015 ajv : This MUST be checked AFTER we update EstMin based on Percent Adjust.
		STATION_update_cycle_too_short_warning();

		// ----------

		STATION_GROUP_STRUCT	*lstation_group;

		g_STATION_group_ID = pstation->GID_station_group;

		if( g_STATION_group_ID > 0 )
		{
			lstation_group = STATION_GROUP_get_group_with_this_GID( pstation->GID_station_group );

			strlcpy( GuiVar_StationInfoStationGroup, nm_GROUP_get_name( lstation_group ), sizeof(GuiVar_StationInfoStationGroup) );

			GuiVar_StationGroupPrecipRate = STATION_GROUP_get_precip_rate_in_per_hr( lstation_group );

			// 3/17/2015 ajv : If the station is using Daily ET, there's nothing to copy, so suppress
			// the Copy button.
			GuiVar_StationInfoShowCopyStation = !(WEATHER_get_station_uses_daily_et( pstation ));

			// 6/29/2015 ajv : Don't show any station details if there are no start times or water days
			// for this group.
			GuiVar_StationInfoGroupHasStartTime = nm_IRRITIME_this_program_is_scheduled_to_run( pstation, lstation_group );
		}
		else
		{
			strlcpy( GuiVar_StationInfoStationGroup, GuiLib_GetTextPtr( GuiStruct_txtNotChosen_0, 0 ), sizeof(GuiVar_StationInfoStationGroup) );

			// 6/29/2015 ajv : Don't show any station details if not assigned to a group
			GuiVar_StationInfoGroupHasStartTime = (false);
		}

		// 11/5/2013 ajv : Set the group name to be displayed in the Copy
		// Station portion of the screen.
		STATION_copy_group_name_into_copy_station_GuiVar_and_set_offsets( (char *)&GuiVar_StationInfoStationGroup );

		// ---------------------

		UNS_32	lstation_preserves_index;

		if( STATION_PRESERVES_get_index_using_ptr_to_station_struct( pstation, &lstation_preserves_index ) == (true) )
		{
			GuiVar_StationInfoIStatus = station_preserves.sps[ lstation_preserves_index ].spbf.i_status;

			GuiVar_StationInfoFlowStatus = station_preserves.sps[ lstation_preserves_index ].spbf.flow_status;
		}
	}
	else
	{
		Alert_func_call_with_null_ptr();
	}
}

/* ---------------------------------------------------------- */
/**
 * Copies a truncated version of the group name into the StationCopyGroupName
 * GuiVar to be displayed in the Copy Station portion of the Stations screen.
 * Additionally, sets the GuiVars used to dynamically draw "the" and "group"
 * around the name.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when drawing the screen and when the user changes the station's group
 * assignment.
 * 
 * @param pgroup_name A pointer to the group name to copy.
 *
 * @author 11/5/2013 AdrianusV
 *
 * @revisions (none)
 */
extern void STATION_copy_group_name_into_copy_station_GuiVar_and_set_offsets( const char *const pgroup_name )
{
	// 11/5/2013 ajv : TODO Since we're concatenating "the" and "group" around
	// the group name, we have to limit the maximum length of the group name
	// itself. By changing the font style to fixed-width, we can fit a maximum
	// of 28 characters into the group name. Therefore, only copy the first 28
	// characters.
	// 
	// This can be improved by writing a routine which uses GuiLib_GetTextWidth
	// to calculate the actual number of pixels available and determining
	// exactly how many of the passed-in group name's characters can be written
	// to maximize the amount of letters shown.
	strlcpy( GuiVar_StationCopyGroupName, pgroup_name, 28 );

	// 11/5/2013 ajv : We need to center the group name between the "the" and
	// "group" text. We're doing so using the easyGUI library by drawing "the"
	// to the left the name and "group" to the right based upon the length of
	// the string. Therefore, set the variables appropriately so the text moves
	// dynamically with the group name.
	GuiVar_StationCopyTextOffset_Right = (GuiLib_GetTextWidth( GuiVar_StationCopyGroupName, GuiFont_FontList[ GuiFont_ANSI7 ], GuiLib_PS_ON ) / 2);
	GuiVar_StationCopyTextOffset_Left = (GuiVar_StationCopyTextOffset_Right * -1);
}

/* ---------------------------------------------------------- */
/**
 * Copies the contents of the GuiVars, used for editing by the easyGUI library,
 * into a temporary structure which is used for comparison with a group in the
 * list to identify changes.
 * 
 * @executed Executed within the context of the display processing task.
 * 
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    4/6/2012  Added passing of (false) into set routine to indiciate the
 *  			change was made locally so change bits are set properly.
 *  			Modified so all memory allocation, storing of changes, and
 *  			memory deallocation are handled within this routine.
 */
extern void STATION_extract_and_store_changes_from_GuiVars( void )
{
	STATION_STRUCT		*lstation;

	UNS_32	lprev_group_ID;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lstation = mem_malloc( sizeof(STATION_STRUCT) );

	// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
	// update just the variable that's potentially changed in the UI.
	memcpy( lstation, nm_STATION_get_pointer_to_station(GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1)), sizeof(STATION_STRUCT) );
	
	// ----------

	strlcpy( lstation->base.description, GuiVar_GroupName, sizeof(lstation->base.description) );

	lstation->decoder_serial_number = GuiVar_StationDecoderSerialNumber;
	lstation->decoder_output = GuiVar_StationDecoderOutput;
	lstation->total_run_minutes_10u = (UNS_32)(GuiVar_StationInfoTotalMinutes * 10);
	lstation->cycle_minutes_10u = (UNS_32)(GuiVar_StationInfoCycleTime * 10);
	lstation->soak_minutes = GuiVar_StationInfoSoakInTime;
	lstation->et_factor_100u = (GuiVar_StationInfoETFactor + 100);
	lstation->expected_flow_rate_gpm = GuiVar_StationInfoExpectedFlow;

	lstation->distribution_uniformity_100u = GuiVar_StationInfoDU;
	lstation->no_water_days = GuiVar_StationInfoNoWaterDays;

	lstation->moisture_balance_percent = (GuiVar_StationInfoMoistureBalancePercent / 100.0F);
	
	lstation->square_footage = GuiVar_StationInfoSquareFootage;

	// ----------

	nm_STATION_store_changes( lstation, g_GROUP_creating_new, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

	// ----------

	mem_free( lstation );

	// ----------

	lstation = nm_STATION_get_pointer_to_station( GuiVar_StationInfoBoxIndex, (GuiVar_StationInfoNumber - 1) );

	lprev_group_ID = STATION_get_GID_station_group( lstation );

	nm_STATION_set_GID_station_group( lstation, g_STATION_group_ID, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, STATION_get_change_bits_ptr( lstation, CHANGE_REASON_KEYPAD ) );

	STATION_adjust_group_settings_after_changing_group_assignment( lstation, lprev_group_ID, g_STATION_group_ID, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD );

	// ----------

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// Reset the Creating New flag to ensure we don't accidentally create
	// another new group when using the BACK key.
	g_GROUP_creating_new = (false);
}

/* ---------------------------------------------------------- */
/**
 * Iterates through the station information list to see whether any stations are
 * assigned to the specified group. 
 *  
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the stations and group whose value is being read isn't
 *  	  deleted during this process.
 *
 * @executed Executed within the context of the key processing task when the
 *           user tries to delete a group.
 *  
 * @param pgroup_ID The group ID to look for in the station information list. 
 * @param pget_group_ID_func_ptr A pointer to the STATION_get_xxxx_GID routine.
 * 
 * @return BOOL_32 True if the specified group is empty; otherwise, false.
 *  
 * @author 9/22/2011 Adrianusv
 * 
 * @revisions 
 *    9/22/2011 Initial release
 */
extern BOOL_32 STATION_this_group_has_no_stations_assigned_to_it( const UNS_32 pgroup_ID,
																  UNS_32 (*pget_group_ID_func_ptr)(const void *pstation) )
{
	STATION_STRUCT *lstation;

	BOOL_32 rv;

	rv = (true);

	if( pget_group_ID_func_ptr != NULL )
	{
		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		lstation = nm_ListGetFirst( &station_info_list_hdr ); 

		if( lstation != NULL )
		{
			while( lstation != NULL )
			{
				if( ((pget_group_ID_func_ptr)( lstation )) == pgroup_ID )
				{
					rv = (false);
				}
				lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
			}
		}
		else
		{
			rv = (false);
		}

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}
	else
	{
		Alert_func_call_with_null_ptr();

		rv = (false);
	}
	return( rv );
}

/* ---------------------------------------------------------- */
extern void STATION_adjust_group_settings_after_changing_group_assignment( STATION_STRUCT *const pstation, const UNS_32 pold_group_ID, const UNS_32 pnew_group_ID, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, const UNS_32 preason_for_change )
{
	CHANGE_BITS_32_BIT	*lbitfield_of_changes;

	UNS_32	lcurrent_group_max_cycle_time_10u, lcurrent_group_min_soak_time;

	UNS_32	lnew_group_max_cycle_time_10u, lnew_group_min_soak_time;

	UNS_32	lstation_current_cycle_time_10u, lstation_current_soak_time;

	UNS_32	lstation_new_cycle_time_10u, lstation_new_soak_time;

	// ---------- 

	if( WEATHER_get_station_uses_daily_et(pstation) )
	{
		lbitfield_of_changes = STATION_get_change_bits_ptr( pstation, preason_for_change );

		if( pold_group_ID > 0 )
		{
			lcurrent_group_max_cycle_time_10u = STATION_GROUP_get_cycle_time_10u_for_this_gid( pold_group_ID );

			lcurrent_group_min_soak_time = STATION_GROUP_get_soak_time_for_this_gid( pold_group_ID, nm_STATION_get_distribution_uniformity_100u(pstation) );
		}

		lnew_group_max_cycle_time_10u = STATION_GROUP_get_cycle_time_10u_for_this_gid( pnew_group_ID );

		lnew_group_min_soak_time =  STATION_GROUP_get_soak_time_for_this_gid( pnew_group_ID, nm_STATION_get_distribution_uniformity_100u(pstation) );

		lstation_current_cycle_time_10u = nm_STATION_get_cycle_minutes_10u( pstation );

		lstation_current_soak_time = nm_STATION_get_soak_minutes( pstation );

		// 6/26/2015 ajv : Initialize the new cycle and soak times to the current ones so the _set_
		// routines can probably determine whether there's been a change or not.
		lstation_new_cycle_time_10u = lstation_current_cycle_time_10u;

		lstation_new_soak_time = lstation_current_soak_time;

		if( pold_group_ID == 0 )
		{
			// 6/25/2015 ajv : Since the station was not assigned to a group yet, the cycle and soak
			// times are set based on the WaterSense max and min, respectively.
			lstation_new_cycle_time_10u = lnew_group_max_cycle_time_10u;
			lstation_new_soak_time = lnew_group_min_soak_time;
		}
		else if( pnew_group_ID > 0 )
		{
			if( lstation_current_cycle_time_10u == lcurrent_group_max_cycle_time_10u )
			{
				lstation_new_cycle_time_10u = lnew_group_max_cycle_time_10u;
			}
			else if( lstation_current_cycle_time_10u > lnew_group_max_cycle_time_10u )
			{
				lstation_new_cycle_time_10u = lnew_group_max_cycle_time_10u;
			}

			if( lstation_current_soak_time == lcurrent_group_min_soak_time )
			{
				lstation_new_soak_time = lnew_group_min_soak_time;
			}
			else if( lstation_current_soak_time < lnew_group_min_soak_time )
			{
				lstation_new_soak_time = lnew_group_min_soak_time;
			}
		}
		else
		{
			// 6/25/2015 ajv : The station's been removed from a group without assigning it to a new
			// group. Off-hand, it seems like there'd be nothing to do since the station won't run
			// anyway, BUT cycle and soak may be used by Manual. Therefore, let's reset them back to the
			// defaults.
			lstation_new_cycle_time_10u = STATION_CYCLE_MINUTES_DEFAULT_10u;

			lstation_new_soak_time = STATION_SOAK_MINUTES_DEFAULT;
		}

		nm_STATION_set_cycle_minutes( pstation, lstation_new_cycle_time_10u, CHANGE_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_set_soak_minutes( pstation, lstation_new_soak_time, CHANGE_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, lbitfield_of_changes );
	}
}

/* ---------------------------------------------------------- */
/**
 * Stores the group assignment changes made to one or more stations.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure a group isn't added or removed between the count being
 *  	  retrieved and being used.
 *
 * @executed Executed within the context of the key processing task when the 
 *  		 user leaves a by-group setting screen.
 * 
 * @param pget_group_ID_func_ptr A pointer to the STATION_get_xxxx_GID routine.
 * @param pset_group_ID_func_ptr A pointer to the STATION_set_xxxx_GID routine.
 * @param pgroup_ID The group to assign the station to.
 * @param preason_for_change The reason for the change, such as keypad.
 * @param pbox_index_0 The serial number of the controller
 *                                    which initiated the change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    4/6/2012  Added pset_change_bits parameter to pass into the set Group ID
 *              function.
 */
extern void STATION_store_group_assignment_changes( UNS_32 (*pget_group_ID_func_ptr)( void *const pstation ),
													void (*pset_group_ID_func_ptr)( void *const pstation, const UNS_32 pValue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set ),
													const UNS_32 pnew_group_ID,
													const UNS_32 preason_for_change,
													const UNS_32 pbox_index_0,
													const BOOL_32 pset_change_bits,
													const UNS_32 pchanges_received_from )
{
	STATION_STRUCT		*lstation;

	CHANGE_BITS_32_BIT	*lbitfield_of_changes;

	UNS_32	lcurrent_group_id;

	UNS_32	i;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// For now, we're assuming the station information list and the station
	// selection list have an identical number of elements and the order of
	// the lists haven't changed. This is a BIG assumption and we may need to
	// find a better way to deal with this.
	lstation = nm_ListGetFirst( &station_info_list_hdr );

	for( i = 0; i < MAX_STATIONS_PER_NETWORK; ++i )
	{
		if( lstation != NULL )
		{
			lcurrent_group_id = pget_group_ID_func_ptr( lstation );

			lbitfield_of_changes = STATION_get_change_bits_ptr( lstation, pchanges_received_from );

			if( STATION_SELECTION_GRID_station_is_selected( i ) == (true) )
			{
				if( lcurrent_group_id != pnew_group_ID )
				{
					pset_group_ID_func_ptr( lstation, pnew_group_ID, CHANGE_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, lbitfield_of_changes );

					// 6/25/2015 ajv : For the case of Station Groups, we also need to adjust the cycle and soak
					// times based on group's charateristics if the station is using ET.
					if( (pget_group_ID_func_ptr == &STATION_get_GID_station_group) )
					{
						STATION_adjust_group_settings_after_changing_group_assignment( lstation, lcurrent_group_id, pnew_group_ID, pbox_index_0, pset_change_bits, pchanges_received_from );
					}
				}
			}
			else
			{
				// 11/19/2013 ajv : Only remove stations if they are actually in use and connected to the
				// controller.
				if( (lcurrent_group_id == pnew_group_ID) && (STATION_station_is_available_for_use(lstation) == (true)) )
				{
					pset_group_ID_func_ptr( lstation, 0, CHANGE_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, lbitfield_of_changes );
				}
			}
		}

		lstation = nm_ListGetNext( &station_info_list_hdr, lstation);
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void STATION_store_manual_programs_assignment_changes( const UNS_32 pgroup_ID,
															  const UNS_32 preason_for_change,
															  const UNS_32 pbox_index_0,
															  const BOOL_32 pset_change_bits,
															  const UNS_32 pchanges_received_from )
{
	STATION_STRUCT		*lstation;

	CHANGE_BITS_32_BIT	*lbitfield_of_changes;

	UNS_32 i;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// For now, we're assuming the station information list and the station
	// selection list have an identical number of elements and the order of
	// the lists haven't changed. This is a BIG assumption and we may need to
	// find a better way to deal with this.
	lstation = nm_ListGetFirst( &station_info_list_hdr );

	for( i = 0; i < MAX_STATIONS_PER_NETWORK; ++i )
	{
		lbitfield_of_changes = STATION_get_change_bits_ptr( lstation, pchanges_received_from );

		if( STATION_SELECTION_GRID_station_is_selected( i ) == (true) )
		{
			if( (lstation->GID_manual_program_A == 0) && (lstation->GID_manual_program_B != pgroup_ID) )
			{
				nm_STATION_set_GID_manual_program_A( lstation, pgroup_ID, CHANGE_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, lbitfield_of_changes );
			}
			else
			if( (lstation->GID_manual_program_B == 0) && (lstation->GID_manual_program_A != pgroup_ID) )
			{
				nm_STATION_set_GID_manual_program_B( lstation, pgroup_ID, CHANGE_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, lbitfield_of_changes );
			}
		}
		else
		{
			if( lstation->GID_manual_program_A == pgroup_ID )
			{
				nm_STATION_set_GID_manual_program_A( lstation, 0, CHANGE_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, lbitfield_of_changes );
			}
			else if( lstation->GID_manual_program_B == pgroup_ID )
			{
				nm_STATION_set_GID_manual_program_B( lstation, 0, CHANGE_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, lbitfield_of_changes );
			}
		}

		lstation = nm_ListGetNext( &station_info_list_hdr, lstation);
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Iterates through the stations list and sets or unsets the station in use
 * flag for each station that's selected on the Stations In Use screen.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING TASK when
 * the user leaves the Stations In Use screen.
 * 
 * @param preason_for_change The reason for the change, such as keypad.
 * 
 * @param pbox_index_0 The serial number of the controller which
 * initiated the change.
 *
 * @author 8/20/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void STATION_store_stations_in_use( const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const UNS_32 pchanges_received_from )
{
	STATION_STRUCT		*lstation;

	CHANGE_BITS_32_BIT	*lbitfield_of_changes;

	UNS_32 lnumber_of_changes;

	UNS_32 i;

	lnumber_of_changes = 0;

	i = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// For now, we're assuming the station information list and the station
	// selection list have an identical number of elements and the order of
	// the lists haven't changed. This is a BIG assumption and we may need to
	// find a better way to deal with this.
	lstation = nm_ListGetFirst( &station_info_list_hdr );

	while( lstation != NULL )
	{
		// 7/8/2014 rmd : Only change the in_use setting if the station is physically available.
		// Which means either the associated station card and terminal are both connected. Or there
		// was a decoder present for the station following the last discovery.
		if( nm_STATION_get_physically_available(lstation) == (true) )
		{
			lbitfield_of_changes = STATION_get_change_bits_ptr( lstation, pchanges_received_from );

			nm_STATION_set_in_use( lstation, STATION_SELECTION_GRID_station_is_selected( i ), CHANGE_generate_change_line, preason_for_change, pbox_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
		}

		lstation = nm_ListGetNext( &station_info_list_hdr, lstation);

		++i;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_get_index_using_ptr_to_station_struct( const STATION_STRUCT *const pstation )
{
	STATION_STRUCT	*lstation;

	UNS_32	rv;

	UNS_32	i;

	rv = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation = nm_ListGetFirst( &station_info_list_hdr );

	for( i = 0; i < nm_ListGetCount( &station_info_list_hdr ); ++i )
	{
		if( lstation == pstation )
		{
			rv = i;

			break;
		}

		lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_get_num_of_stations_physically_available( void )
{
	STATION_STRUCT	*lstation;

	UNS_32		rv;

	rv = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation = nm_ListGetFirst( &station_info_list_hdr );

	// If the first station is not in use, find the first station in use.
	while( lstation != NULL )
	{
		// 7/8/2014 rmd : If physically available that indicates that either the terminal and card
		// are present. Or a decoder is present to support the station (according to the last
		// discovery).
		if( nm_STATION_get_physically_available(lstation) == (true) )
		{
			rv += 1;
		}

		lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_get_num_stations_in_use( void )
{
	STATION_STRUCT	*lstation;

	UNS_32		rv;

	rv = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation = nm_ListGetFirst( &station_info_list_hdr );

	while( lstation != NULL )
	{
		rv += STATION_station_is_available_for_use( lstation );

		lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_get_num_stations_assigned_to_groups( void )
{
	STATION_STRUCT	*lstation;

	UNS_32		rv;

	rv = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation = nm_ListGetFirst( &station_info_list_hdr );

	while( lstation != NULL )
	{
		// 11/14/2013 ajv : TODO Should we be looking at just Station Groups
		// here or Station Groups and Manual Programs?
		#if 1
		rv += (lstation->GID_station_group > 0);
		#else
		rv += ( (lstation->GID_station_group > 0) ||
			    (lstation->GID_manual_program_A > 0 ) ||
			    (lstation->GID_manual_program_B > 0 ) );
		#endif

		lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the first station in use,.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when displaying a screen with by-station information, including the various
 * station reports.
 * 
 * @return STATION_STRUCT* A pointer to the first station in use. If none are in
 * use, a NULL is returned.
 *
 * @author 8/20/2012 Adrianusv
 *
 * @revisions (none)
 */
extern STATION_STRUCT *STATION_get_first_available_station( void )
{
	STATION_STRUCT	*lstation;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation = nm_ListGetFirst( &station_info_list_hdr );

	// If the first station is not in use, find the first station in use.
	if( (lstation != NULL) && (STATION_station_is_available_for_use(lstation) == (false)) )
	{
		do
		{
			lstation = nm_ListGetNext( &station_info_list_hdr, lstation );

		} while ( (lstation != NULL) && (STATION_station_is_available_for_use(lstation) == (false)) );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( lstation );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_get_unique_box_index_count( void )
{
	STATION_STRUCT	*lstation;

	UNS_32	rv;

	UNS_32	box_index_0, prev_box_index_0;

	rv = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation = nm_ListGetFirst( &station_info_list_hdr );

	if( lstation != NULL )
	{
		// 4/24/2014 ajv : Initialize the previous box index to an invalid value
		// to ensure it counts the very first box index found.
		prev_box_index_0 = MAX_CHAIN_LENGTH;

		do
		{
			if( (lstation->physically_available) && (lstation->in_use_bool) )
			{
				box_index_0 = lstation->box_index_0;

				if( box_index_0 != prev_box_index_0 )
				{
					// 4/24/2014 ajv : We found a unique box index.
					rv++;

					prev_box_index_0 = box_index_0;
				}
			}

			lstation = nm_ListGetNext( &station_info_list_hdr, lstation );

		} while( lstation != NULL );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns TRUE if the station is in use and both it's station card and terminal
 * board are connected. If the station isn't valid, an alert is generated and
 * FALSE is returned.
 * 
 * @mutex_requirements Calling function must have the
 * list_program_data_recursive_MUTEX mutex to ensure the station whose value is
 * being retrieve isn't deleted during this process.
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when determining what stations to display on the screen; within the context
 * of the TD_CHECK task when a start time is crossed.
 * 
 * @param pstation 
 * 
 * @return BOOL_32 True if the station is in use and the card and terminal are
 * both connected; otherwise, false. Also returns false is the station is not
 * valid.
 *
 * @author 3/1/2013 Adrianusv
 *
 * @revisions (none)
 */
extern BOOL_32 STATION_station_is_available_for_use( void *const pstation )
{
	BOOL_32	lin_use;

	BOOL_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lin_use = SHARED_get_bool_32_bit_change_bits_group( pstation,
														&(((STATION_STRUCT*)pstation)->in_use_bool),
														STATION_INUSE_DEFAULT,
														nm_STATION_set_in_use,
														&(((STATION_STRUCT*)pstation)->changes_to_send_to_master),
														STATION_database_field_names[ STATION_CHANGE_BITFIELD_in_use_bit ] );

	// 7/8/2014 rmd : Needs to look at two variables to decide if station is available for use.
	// The first is the "in_use" variable. Which is directly set by the end user. The second is
	// the physically available which is carefully set for both terminal/card stations and
	// decoder based stations as events occur that logically would change its value. Such as a
	// decoder discovery that does not produce a decoder for the station. Or the whats installed
	// arrives from the tpmicro indicating the card or terminal is not present. Or is present.
	rv = ( (lin_use) && (nm_STATION_get_physically_available(pstation) == (true)) );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns TRUE if the station card and terminal board are connected. If the
 * station isn't valid, an alert is generated and FALSE is returned.
 * 
 * @mutex_requirements Calling function must have the
 * list_program_data_recursive_MUTEX mutex to ensure the station whose value is
 * being retrieve isn't deleted during this process.
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when determining what stations to display on the screen.
 * 
 * @param pstation 
 * 
 * @return BOOL_32 True if the station's card and terminal are both connected;
 * otherwise, false. Also returns false is the station is not valid.
 *
 * @author 3/1/2013 Adrianusv
 *
 * @revisions (none)
 */
extern BOOL_32 nm_STATION_get_physically_available( STATION_STRUCT *const pstation )
{
	BOOL_32	rv;

	rv = SHARED_get_bool_32_bit_change_bits_group( pstation,
												   &pstation->physically_available,
												   (false),
												   nm_STATION_set_physically_available,
												   &pstation->changes_to_send_to_master,
												   STATION_database_field_names[ STATION_CHANGE_BITFIELD_physically_available ] );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the station number assigned to a particular station. If the station
 * isn't valid, an alert is generated and 0 is returned.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being retrieved isn't deleted
 *  	  during this process.
 * 
 * @executed 
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 The station number assigned to the station.
 *
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    2/13/2012 Replaced (pstation != NULL) with call to OnList to ensure the
 *              list and station are valid, and the station is on the list.
 */
extern UNS_32 nm_STATION_get_station_number_0( STATION_STRUCT *const pstation )
{
	UNS_32	rv;

	if( nm_OnList(&station_info_list_hdr, pstation) == (true) )
	{
		rv = pstation->station_number_0;
	}
	else
	{
		Alert_item_not_on_list( pstation, station_info_list_hdr );

		// TODO What to return if serial number is invalid?

		rv = 0;
	}

	// TODO Range check return value

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the controller serial number assigned to a particular station. If the
 * station isn't valid, an alert is generated and 0 is returned.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being retrieved isn't deleted
 *  	  during this process.
 * 
 * @executed 
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 The controller serial number assigned to the station.
 *
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    2/13/2012 Replaced (pstation != NULL) with call to OnList to ensure the
 *              list and station are valid, and the station is on the list.
 */
extern UNS_32 nm_STATION_get_box_index_0( STATION_STRUCT *const pstation )
{
	UNS_32	rv;

	rv = SHARED_get_uint32_32_bit_change_bits_group( pstation,
													 &pstation->box_index_0,
													 BOX_INDEX_MINIMUM,
													 BOX_INDEX_MAXIMUM,
													 BOX_INDEX_DEFAULT,
													 nm_STATION_set_box_index,
													 &pstation->changes_to_send_to_master,
													 STATION_database_field_names[ STATION_CHANGE_BITFIELD_box_index_bit ] );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 nm_STATION_get_decoder_serial_number( STATION_STRUCT *const pstation )
{
	UNS_32	rv;

	rv = SHARED_get_uint32_32_bit_change_bits_group( pstation,
													 &pstation->decoder_serial_number,
													 DECODER_SERIAL_NUM_MIN,
													 DECODER_SERIAL_NUM_MAX,
													 DECODER_SERIAL_NUM_DEFAULT,
													 nm_STATION_set_decoder_serial_number,
													 &pstation->changes_to_send_to_master,
													 STATION_database_field_names[ STATION_CHANGE_BITFIELD_decoder_serial_number_bit ] );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 nm_STATION_get_decoder_output( STATION_STRUCT *const pstation )
{
	UNS_32	rv;

	rv = SHARED_get_uint32_32_bit_change_bits_group( pstation,
													 &pstation->decoder_output,
													 DECODER_OUTPUT_A_BLACK,
													 DECODER_OUTPUT_B_ORANGE,
													 DECODER_OUTPUT_A_BLACK,
													 nm_STATION_set_decoder_output,
													 &pstation->changes_to_send_to_master,
													 STATION_database_field_names[ STATION_CHANGE_BITFIELD_decoder_output_bit ] );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the total run time, in minutes, assigned to a particular station. If the
 * station isn't valid, an alert is generated and the default value is returned.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being retrieved isn't deleted
 *  	  during this process.
 * 
 * @executed Executed within the context of the display processing task when the
 *           Station Information screen is being drawn.
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 The total run time of the station.
 *
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    2/13/2012 Replaced (pstation != NULL) with call to OnList to ensure the
 *              list and station are valid, and the station is on the list.
 */
extern UNS_32 STATION_get_total_run_minutes_10u( STATION_STRUCT *const pstation )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( pstation,
													 &pstation->total_run_minutes_10u,
													 STATION_TOTAL_RUN_MINUTES_MIN_10u,
													 STATION_TOTAL_RUN_MINUTES_MAX_10u,
													 STATION_TOTAL_RUN_MINUTES_DEFAULT_10u,
													 nm_STATION_set_total_run_minutes,
													 &pstation->changes_to_send_to_master,
													 STATION_database_field_names[ STATION_CHANGE_BITFIELD_total_run_minutes_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the cycle time, in minutes, assigned to a particular station. If the
 * station isn't valid, an alert is generated and the default value is returned.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being retrieved isn't deleted
 *  	  during this process.
 * 
 * @executed Executed within the context of the display processing task when the
 *           Station Information screen is being drawn.
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 The cycle time of the station.
 *
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    2/13/2012 Replaced (pstation != NULL) with call to OnList to ensure the
 *              list and station are valid, and the station is on the list.
 */
extern UNS_32 nm_STATION_get_cycle_minutes_10u( STATION_STRUCT *const pstation )
{
	UNS_32		rv_10u;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( pstation != NULL )
	{
		rv_10u = SHARED_get_uint32_32_bit_change_bits_group( pstation,
															 &pstation->cycle_minutes_10u,
															 STATION_CYCLE_MINUTES_MIN_10u,
															 STATION_CYCLE_MINUTES_MAX_10u,
															 STATION_CYCLE_MINUTES_DEFAULT_10u,
															 nm_STATION_set_cycle_minutes,
															 &pstation->changes_to_send_to_master,
															 STATION_database_field_names[ STATION_CHANGE_BITFIELD_cycle_minutes_bit ] );
	}
	else
	{
		rv_10u = STATION_CYCLE_MINUTES_DEFAULT_10u;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv_10u );
}

/* ---------------------------------------------------------- */
extern UNS_32 nm_STATION_get_watersense_cycle_max_10u( STATION_STRUCT *const pstation )
{
	UNS_32		lgroup_ID;

	UNS_32		rv;

	// ----------

	// 6/25/2015 ajv : Default to the current cycle time in case there's an error or the
	// station's not using Daily ET.
	// 
	// 10/14/2015 ajv : Be careful not to use the nm_STATION_get_cycle_minutes_10u call here.
	// Doing so will result in a range-check error if the calculated value exceeds the defined
	// maximum even though we're going to adjust it below.
	if( pstation != NULL )
	{
		rv = pstation->cycle_minutes_10u;
	}
	else
	{
		rv = STATION_CYCLE_MINUTES_DEFAULT_10u;
	}
	
	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( (pstation != NULL) && (WEATHER_get_station_uses_daily_et(pstation)) )
	{
		lgroup_ID = STATION_get_GID_station_group( pstation );

		// If the station has a GID of 0, which means it's not assigned to a group yet, return the
		// default value.
		if( lgroup_ID > 0 )
		{
			rv = STATION_GROUP_get_cycle_time_10u_for_this_gid( lgroup_ID );
		}
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the soak-in time, in minutes, assigned to a particular station. If the
 * station isn't valid, an alert is generated and the default value is returned.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being retrieved isn't deleted
 *  	  during this process.
 * 
 * @executed Executed within the context of the display processing task when the
 *           Station Information screen is being drawn.
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 The soak-in time of the station.
 *
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    2/13/2012 Replaced (pstation != NULL) with call to OnList to ensure the
 *              list and station are valid, and the station is on the list.
 */
extern UNS_32 nm_STATION_get_soak_minutes( STATION_STRUCT *const pstation )
{
	UNS_32		rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( pstation != NULL )
	{
		rv = SHARED_get_uint32_32_bit_change_bits_group( pstation,
														 &pstation->soak_minutes,
														 STATION_SOAK_MINUTES_MIN,
														 STATION_SOAK_MINUTES_MAX,
														 STATION_SOAK_MINUTES_DEFAULT,
														 nm_STATION_set_soak_minutes,
														 &pstation->changes_to_send_to_master,
														 STATION_database_field_names[ STATION_CHANGE_BITFIELD_soak_minutes_bit ] );
	}
	else
	{
		rv = STATION_SOAK_MINUTES_DEFAULT;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 nm_STATION_get_watersense_soak_min( STATION_STRUCT *const pstation )
{
	UNS_32		lgroup_ID;

	UNS_32		rv;

	// ----------

	// 6/25/2015 ajv : Default to the current soak time in case there's an error or the
	// station's not using Daily ET.
	// 
	// 10/14/2015 ajv : Be careful not to use the nm_STATION_get_soak_minutes call here. Doing
	// so will result in a range-check error if the calculated value exceeds the defined maximum
	// even though we're going to adjust it below.
	if( pstation != NULL )
	{
		rv = pstation->soak_minutes;
	}
	else
	{
		rv = STATION_SOAK_MINUTES_DEFAULT;
	}

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( (pstation != NULL) && (WEATHER_get_station_uses_daily_et(pstation)) )
	{
		lgroup_ID = STATION_get_GID_station_group( pstation );

		// If the station has a GID of 0, which means it's not assigned to a group yet, return the
		// default value.
		if( lgroup_ID > 0 )
		{
			rv = STATION_GROUP_get_soak_time_for_this_gid( lgroup_ID, nm_STATION_get_distribution_uniformity_100u(pstation) );
		}
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the ET factor (aka percent of ET), assigned to a particular station. If
 * the station isn't valid, an alert is generated and the default value is
 * returned.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being retrieved isn't deleted
 *  	  during this process.
 * 
 * @executed Executed within the context of the display processing task when the
 *           Station Information screen is being drawn.
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 The ET factor of the station.
 *
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    2/13/2012 Replaced (pstation != NULL) with call to OnList to ensure the
 *              list and station are valid, and the station is on the list.
 */
extern UNS_32 STATION_get_ET_factor_100u( STATION_STRUCT *const pstation )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( pstation,
													 &pstation->et_factor_100u,
													 STATION_ET_FACTOR_MIN,
													 STATION_ET_FACTOR_MAX,
													 STATION_ET_FACTOR_DEFAULT,
													 nm_STATION_set_ET_factor,
													 &pstation->changes_to_send_to_master,
													 STATION_database_field_names[ STATION_CHANGE_BITFIELD_et_factor_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the expected flow rate, in gpm, assigned to a particular station. If
 * the station isn't valid, an alert is generated and the default value is
 * returned.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being retrieved isn't deleted
 *  	  during this process.
 * 
 * @executed Executed within the context of the display processing task when the
 *           Station Information screen is being drawn.
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 The expected flow rate of the station.
 *
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    2/13/2012 Replaced (pstation != NULL) with call to OnList to ensure the
 *              list and station are valid, and the station is on the list.
 */
extern UNS_32 nm_STATION_get_expected_flow_rate_gpm( STATION_STRUCT *const pstation )
{
	UNS_32	rv;

	rv = SHARED_get_uint32_32_bit_change_bits_group( pstation,
													 &pstation->expected_flow_rate_gpm,
													 STATION_EXPECTED_FLOW_RATE_MIN,
													 STATION_EXPECTED_FLOW_RATE_MAX,
													 STATION_EXPECTED_FLOW_RATE_DEFAULT,
													 nm_STATION_set_expected_flow_rate,
													 &pstation->changes_to_send_to_master,
													 STATION_database_field_names[ STATION_CHANGE_BITFIELD_expected_flow_rate_bit ] );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the distribution uniformity assigned to a particular station. If the
 * station isn't valid, an alert is generated and the default value is returned.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being retrieved isn't deleted
 *  	  during this process.
 * 
 * @executed Executed within the context of the display processing task when the
 *           Station Information screen is being drawn.
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 The distribution uniformity of the station.
 *
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    2/13/2012 Replaced (pstation != NULL) with call to OnList to ensure the
 *              list and station are valid, and the station is on the list.
 */
extern UNS_32 nm_STATION_get_distribution_uniformity_100u( STATION_STRUCT *const pstation )
{
	UNS_32	rv;

	rv = SHARED_get_uint32_32_bit_change_bits_group( pstation,
													 &pstation->distribution_uniformity_100u,
													 STATION_DU_MIN,
													 STATION_DU_MAX,
													 STATION_DU_DEFAULT,
													 nm_STATION_set_distribution_uniformity,
													 &pstation->changes_to_send_to_master,
													 STATION_database_field_names[ STATION_CHANGE_BITFIELD_distribution_uniformity_bit ] );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the no water days assigned to a particular station. If the station isn't
 * valid, an alert is generated and the default value is returned.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being retrieved isn't deleted
 *  	  during this process.
 * 
 * @executed Executed within the context of the display processing task when the
 *           Station Information screen is being drawn.
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 The no water days of the station.
 *
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    2/13/2012 Replaced (pstation != NULL) with call to OnList to ensure the
 *              list and station are valid, and the station is on the list.
 */
extern UNS_32 STATION_get_no_water_days( STATION_STRUCT *const pstation )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( pstation,
													 &pstation->no_water_days,
													 STATION_NO_WATER_DAYS_MIN,
													 STATION_NO_WATER_DAYS_MAX,
													 STATION_NO_WATER_DAYS_DEFAULT,
													 nm_STATION_set_no_water_days,
													 &pstation->changes_to_send_to_master,
													 STATION_database_field_names[ STATION_CHANGE_BITFIELD_no_water_days_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_decrement_no_water_days_and_return_value_after_decrement( STATION_STRUCT *const pstation )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( nm_OnList(&station_info_list_hdr, pstation) == (true) )
	{
		// 5/6/2014 rmd : With the range check may work out even if 0 to begin with, but unnerving
		// because of the -1 term in the parameter list. So test if greater than 0.
		if( pstation->no_water_days > 0 )
		{
			nm_STATION_set_no_water_days( pstation, (pstation->no_water_days - 1), CHANGE_generate_change_line, CHANGE_REASON_HIT_A_STARTTIME, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, STATION_get_change_bits_ptr( pstation, CHANGE_REASON_HIT_A_STARTTIME ) );
		}

		rv = pstation->no_water_days;
	}
	else
	{
		Alert_item_not_on_list( pstation, station_info_list_hdr );

		rv = STATION_NO_WATER_DAYS_DEFAULT;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern void STATION_chain_down_NOW_days_midnight_maintenance( void )
{
	// 5/6/2014 rmd : Called from TD_CHECK task. At midnight and only if the chain is down. To
	// keep the NO Water days decrementing. So when the chain is back up we aren't left with an
	// unexpected number of no water days.
	
	STATION_STRUCT		*lss_ptr;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );
	
	lss_ptr	= nm_ListGetFirst( &station_info_list_hdr );

	while( lss_ptr != NULL )
	{
		STATION_decrement_no_water_days_and_return_value_after_decrement( lss_ptr );
		
		lss_ptr = nm_ListGetNext( &station_info_list_hdr, lss_ptr );
	}
	
	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Gets the station description of a particular station. If the station isn't
 * valid, an alert is generated and the default value is returned.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being retrieved isn't deleted
 *  	  during this process.
 * 
 * @executed Executed within the context of the display processing task when the
 *           Station Information screen is being drawn.
 * 
 * @param pstation A pointer to the station.
 * 
 * @return char * A pointer to the station's description.
 *
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    2/13/2012 Replaced (pstation != NULL) with call to OnList to ensure the
 *              list and station are valid, and the station is on the list.
 */
extern char *nm_STATION_get_description( STATION_STRUCT *const pstation )
{
	char	*rv;

	if( nm_OnList(&station_info_list_hdr, pstation) == (true) )
	{
		rv = (char*)pstation->base.description;
	}
	else
	{
		Alert_item_not_on_list( pstation, station_info_list_hdr );

		rv = NULL;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 nm_STATION_get_number_of_manual_programs_station_is_assigned_to( STATION_STRUCT *const pstation )
{
	UNS_32	rv;

	rv = 0;

	if( nm_OnList(&station_info_list_hdr, pstation) == (true) )
	{
		rv += (pstation->GID_manual_program_A > 0);
		rv += (pstation->GID_manual_program_B > 0);
	}
	else
	{
		Alert_item_not_on_list( pstation, station_info_list_hdr );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the landscape details group ID assigned to a particular station. If
 * the station pointer is invalid, a 0 is returned. Therefore, the user
 * <b>MUST</b> check for a value of 0 when using this function and take
 * appropriate action since a GID of 0 is invalid.
 * 
 * @mutex_requirements Calling function must have the
 * list_program_data_recursive_MUTEX mutex to ensure the station whose value is
 * being retrieved isn't deleted during this process.
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 The plant material group ID. If the station is not found, 0 is
 * returned.
 *
 * @author 4/24/2013 Adrianusv
 *
 * @revisions (none)
 */
extern UNS_32 STATION_get_GID_station_group( void *const pstation )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( pstation,
													 &((STATION_STRUCT*)pstation)->GID_station_group,
													 0,
													 STATION_GROUP_get_last_group_ID(),
													 0,
													 nm_STATION_set_GID_station_group,
													 &((STATION_STRUCT*)pstation)->changes_to_send_to_master,
													 STATION_database_field_names[ STATION_CHANGE_BITFIELD_GID_station_group_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_get_GID_manual_program_A( void *const pstation )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( pstation,
													 &((STATION_STRUCT*)pstation)->GID_manual_program_A,
													 0,
													 MANUAL_PROGRAMS_get_last_group_ID(),
													 0,
													 nm_STATION_set_GID_manual_program_A,
													 &((STATION_STRUCT*)pstation)->changes_to_send_to_master,
													 STATION_database_field_names[ STATION_CHANGE_BITFIELD_GID_manual_program_A ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_get_GID_manual_program_B( void *const pstation )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( pstation,
													 &((STATION_STRUCT*)pstation)->GID_manual_program_B,
													 0,
													 MANUAL_PROGRAMS_get_last_group_ID(),
													 0,
													 nm_STATION_set_GID_manual_program_B,
													 &((STATION_STRUCT*)pstation)->changes_to_send_to_master,
													 STATION_database_field_names[ STATION_CHANGE_BITFIELD_GID_manual_program_B ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 STATION_does_this_station_belong_to_this_manual_program( STATION_STRUCT *const pstation, UNS_32 pmanual_program_gid )
{
	BOOL_32 rv;

	rv = (false);
	
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( nm_OnList(&station_info_list_hdr, pstation) )
	{
		if( pmanual_program_gid == ((STATION_STRUCT*)pstation)->GID_manual_program_A )
		{
			rv = (true);
		}

		if( pmanual_program_gid == ((STATION_STRUCT*)pstation)->GID_manual_program_B )
		{
			rv = (true);
		}
	}
	else
	{
		Alert_item_not_on_list( pstation, station_info_list_hdr );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern float STATION_get_moisture_balance_percent( STATION_STRUCT *const pstation )
{
	float	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_float_32_bit_change_bits_group( pstation,
													&pstation->moisture_balance_percent,
													STATION_MOISTURE_BALANCE_MIN,
													STATION_MOISTURE_BALANCE_MAX,
													STATION_MOISTURE_BALANCE_DEFAULT,
													nm_STATION_set_moisture_balance_percent,
													&pstation->changes_to_send_to_master,
													STATION_database_field_names[ STATION_CHANGE_BITFIELD_moisture_balance_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern float STATION_get_moisture_balance( STATION_STRUCT *const pstation )
{
	float	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = (STATION_get_moisture_balance_percent(pstation) * (STATION_GROUP_get_soil_storage_capacity_inches_100u(pstation) / 100.0F));

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_get_square_footage( STATION_STRUCT *const pstation )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( pstation,
													 &pstation->square_footage,
													 STATION_SQUARE_FOOTAGE_MIN,
													 STATION_SQUARE_FOOTAGE_MAX,
													 STATION_SQUARE_FOOTAGE_DEFAULT,
													 nm_STATION_set_square_footage,
													 &pstation->changes_to_send_to_master,
													 STATION_database_field_names[ STATION_CHANGE_BITFIELD_square_footage_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern void STATION_set_ignore_moisture_balance_at_next_irrigation_all_stations( const UNS_32 preason_for_change )
{
	STATION_STRUCT	*lstation;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation = nm_ListGetFirst( &station_info_list_hdr );

	while( lstation != NULL )
	{
		// 3/15/2017 ajv : Only clear the moisture balance for stations that are phyiscally available in and in use.
		if( STATION_station_is_available_for_use(lstation) )
		{
			// 3/15/2017 ajv : Since we generate a single alert when clear the MB for all stations,
			// there's no reason to generate an individual change line for each station.
			nm_STATION_set_ignore_moisture_balance_at_next_irrigation( lstation, STATION_MOISTURE_BALANCE_SET_IGNORE_FLAG, CHANGE_do_not_generate_change_line, preason_for_change, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, STATION_get_change_bits_ptr(lstation, preason_for_change) );
		}

		lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------

	Alert_reset_moisture_balance_all_stations();

	if( preason_for_change == CHANGE_REASON_CENTRAL_OR_MOBILE )
	{
		// 3/15/2017 ajv : Since this command comes from Command Center Online, it inherently won't
		// trigger resending Program Data. Therefore, force the change bits to be set in the
		// changes_to_upload_to_comm_server bit field so the changes are uploaded.
		STATION_on_all_stations_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );

		// 3/15/2017 ajv : Also, set our pending flag in case of a power failure. Will guarantee
		// another try when power returns if the files have been saved (remember counts on bits set
		// in the files themselves).
		weather_preserves.pending_changes_to_send_to_comm_server = (true);

		// 3/15/2017 ajv : Finally, start the timer which triggers the data to be sent in 30
		// seconds.
		xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGE_MADE_MS ), portMAX_DELAY );
	}
}

/* ---------------------------------------------------------- */
extern void STATION_set_ignore_moisture_balance_at_next_irrigation_by_group( const UNS_32 pgroup_ID, const UNS_32 preason_for_change )
{
	STATION_GROUP_STRUCT	*lgroup;

	STATION_STRUCT			*lstation;

	char	str_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lstation = nm_ListGetFirst( &station_info_list_hdr );

	while( lstation != NULL )
	{
		// 3/15/2017 ajv : Only clear the moisture balance for stations that are phyiscally available in and in use.
		if( (STATION_get_GID_station_group(lstation) == pgroup_ID) && (STATION_station_is_available_for_use(lstation)) )
		{
			// 3/15/2017 ajv : Since we generate a single alert when clear the MB for the station group,
			// there's no reason to generate an individual change line for each station.
			nm_STATION_set_ignore_moisture_balance_at_next_irrigation( lstation, STATION_MOISTURE_BALANCE_SET_IGNORE_FLAG, CHANGE_do_not_generate_change_line, preason_for_change, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, STATION_get_change_bits_ptr( lstation, preason_for_change ) );
		}

		lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
	}

	// ----------

	lgroup = STATION_GROUP_get_group_with_this_GID( pgroup_ID );

	if( lgroup != NULL )
	{
		strlcpy( str_48, nm_GROUP_get_name( lgroup ), NUMBER_OF_CHARS_IN_A_NAME ); 
	}
	else
	{
		strlcpy( str_48, "<Unknown Group>", NUMBER_OF_CHARS_IN_A_NAME ); 
	}

	// ----------

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	Alert_reset_moisture_balance_by_group( str_48, pgroup_ID );

	// ----------

	if( preason_for_change == CHANGE_REASON_CENTRAL_OR_MOBILE )
	{
		// 3/15/2017 ajv : Since this command comes from Command Center Online, it inherently won't
		// trigger resending Program Data. Therefore, force the change bits to be set in the
		// changes_to_upload_to_comm_server bit field so the changes are uploaded.
		STATION_on_all_stations_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );

		// 3/15/2017 ajv : Also, set our pending flag in case of a power failure. Will guarantee
		// another try when power returns if the files have been saved (remember counts on bits set
		// in the files themselves).
		weather_preserves.pending_changes_to_send_to_comm_server = (true);

		// 3/15/2017 ajv : Finally, start the timer which triggers the data to be sent in 30
		// seconds.
		xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGE_MADE_MS ), portMAX_DELAY );
	}
}

/* ---------------------------------------------------------- */
extern void STATION_set_ignore_moisture_balance_at_next_irrigation_by_station( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, const UNS_32 preason_for_change )
{
	STATION_STRUCT	*lstation;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation = nm_STATION_get_pointer_to_station( pbox_index_0, pstation_number_0 );

	// 3/15/2017 ajv : Only clear the moisture balance for stations that are phyiscally available in and in use.
	if( STATION_station_is_available_for_use(lstation) )
	{
		nm_STATION_set_ignore_moisture_balance_at_next_irrigation( lstation, STATION_MOISTURE_BALANCE_SET_IGNORE_FLAG, CHANGE_generate_change_line, preason_for_change, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, STATION_get_change_bits_ptr( lstation, preason_for_change ) );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------

	if( preason_for_change == CHANGE_REASON_CENTRAL_OR_MOBILE )
	{
		// 3/15/2017 ajv : Since this command comes from Command Center Online, it inherently won't
		// trigger resending Program Data. Therefore, force the change bits to be set in the
		// changes_to_upload_to_comm_server bit field so the changes are uploaded.
		STATION_on_all_stations_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );

		// 3/15/2017 ajv : Also, set our pending flag in case of a power failure. Will guarantee
		// another try when power returns if the files have been saved (remember counts on bits set
		// in the files themselves).
		weather_preserves.pending_changes_to_send_to_comm_server = (true);

		// 3/15/2017 ajv : Finally, start the timer which triggers the data to be sent in 30
		// seconds.
		xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGE_MADE_MS ), portMAX_DELAY );
	}
}

/* ---------------------------------------------------------- */
// 12/23/2015 skc : Copy relevant station data into a budget structure
// This save many cpu cycles for obtaining read only values used in budget calculations
// Mutex - caller is expected to take the list_program_data_recursive_MUTEX
extern void nm_STATION_get_Budget_data( STATION_STRUCT* const pstation_ptr, STATION_STRUCT_for_budgets *pbudget_ptr)
{
	// Sanity check
	if(  (NULL == pstation_ptr) || (NULL == pbudget_ptr) )
	{
		#ifndef	_MSC_VER

		ALERT_MESSAGE_WITH_FILE_NAME("NULL pointer parameter");

		#endif

		return; // Any better ideas?
	}

	pbudget_ptr->box_index_0 = pstation_ptr->box_index_0;
	pbudget_ptr->station_number_0 = pstation_ptr->station_number_0;
	pbudget_ptr->physically_available = pstation_ptr->physically_available;
	pbudget_ptr->in_use_bool = pstation_ptr->in_use_bool;
	pbudget_ptr->total_run_minutes_10u = pstation_ptr->total_run_minutes_10u;
	pbudget_ptr->uses_et = WEATHER_get_station_uses_daily_et( pstation_ptr );
	pbudget_ptr->et_factor_100u = pstation_ptr->et_factor_100u;
	pbudget_ptr->uniformity = pstation_ptr->distribution_uniformity_100u;
	pbudget_ptr->expected_flow_rate_gpm = pstation_ptr->expected_flow_rate_gpm;
	pbudget_ptr->precip_rate = STATION_GROUP_get_station_precip_rate_in_per_hr_100000u( pstation_ptr );
	pbudget_ptr->no_water_days = pstation_ptr->no_water_days;
	pbudget_ptr->GID_station_group = pstation_ptr->GID_station_group;
	pbudget_ptr->GID_irrigation_system = STATION_GROUP_get_GID_irrigation_system_for_this_station( pstation_ptr );
	pbudget_ptr->GID_manual_program_A = pstation_ptr->GID_manual_program_A;
	pbudget_ptr->GID_manual_program_B = pstation_ptr->GID_manual_program_B;
}

/* ---------------------------------------------------------- */
extern void STATION_set_no_water_days_for_all_stations( const UNS_32 pnow_days, const UNS_32 preason_for_change )
{
	STATION_STRUCT	*lstation;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation = nm_ListGetFirst( &station_info_list_hdr );

	while( lstation != NULL )
	{
		// 10/5/2015 ajv : Only set no water days for stations that are phyiscally available in and
		// in use.
		if( STATION_station_is_available_for_use(lstation) )
		{
			// 10/5/2015 ajv : Since we generate a single alert when setting the No Water Days for all
			// stations, there's no reason to generate an individual change line for each station.
			nm_STATION_set_no_water_days( lstation, pnow_days, CHANGE_do_not_generate_change_line, preason_for_change, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, STATION_get_change_bits_ptr( lstation, preason_for_change ) );
		}

		lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------

	Alert_set_no_water_days_all_stations( pnow_days );

	if( preason_for_change == CHANGE_REASON_CENTRAL_OR_MOBILE )
	{
		// 3/23/2016 ajv : Since this command comes from Command Center Online, it inherently won't
		// trigger resending Program Data. However, that means Program Data will report that the
		// controller is OFF when it's actually ON or vice versa. Therefore, force the change bits
		// to be set in the changes_to_upload_to_comm_server bit field so the changes are uploaded.
		STATION_on_all_stations_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );

		// 3/23/2016 ajv : Also, set our pending flag in case of a power failure. Will guarantee
		// another try when power returns if the files have been saved (remember counts on bits set
		// in the files themselves).
		weather_preserves.pending_changes_to_send_to_comm_server = (true);

		// 3/23/2016 ajv : Finally, start the timer which triggers the data to be sent in 30
		// seconds.
		xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGE_MADE_MS ), portMAX_DELAY );
	}

	// ----------

	// 12/14/2015 ajv : Trigger an updated background calculation to ensure the No Water Day
	// change is applied to the next scheduled calculation.
	postBackground_Calculation_Event( BKGRND_CALC_NEXT_SCHEDULED );
}

/* ---------------------------------------------------------- */
extern void STATION_set_no_water_days_by_group( const UNS_32 pnow_days, const UNS_32 pgroup_ID, const UNS_32 preason_for_change )
{
	STATION_GROUP_STRUCT	*lgroup;

	STATION_STRUCT			*lstation;

	char	str_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lstation = nm_ListGetFirst( &station_info_list_hdr );

	while( lstation != NULL )
	{
		// 10/5/2015 ajv : Only set no water days for stations that are phyiscally available in and
		// in use that belong to this group.
		if( (STATION_get_GID_station_group(lstation) == pgroup_ID) && (STATION_station_is_available_for_use(lstation)) )
		{
			// 10/5/2015 ajv : Since we generate a single alert when setting the No Water Days for a
			// station group, there's no reason to generate an individual change line for each
			// station.
			nm_STATION_set_no_water_days( lstation, pnow_days, CHANGE_do_not_generate_change_line, preason_for_change, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, STATION_get_change_bits_ptr( lstation, preason_for_change ) );
		}

		lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
	}
	
	// ----------

	lgroup = STATION_GROUP_get_group_with_this_GID( pgroup_ID );

	if( lgroup != NULL )
	{
		strlcpy( str_48, nm_GROUP_get_name( lgroup ), NUMBER_OF_CHARS_IN_A_NAME ); 
	}
	else
	{
		strlcpy( str_48, "<Unknown Group>", NUMBER_OF_CHARS_IN_A_NAME ); 
	}

	// ----------

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------

	Alert_set_no_water_days_by_group( str_48, pgroup_ID, pnow_days );

	if( preason_for_change == CHANGE_REASON_CENTRAL_OR_MOBILE )
	{
		// 3/23/2016 ajv : Since this command comes from Command Center Online, it inherently won't
		// trigger resending Program Data. However, that means Program Data will report that the
		// controller is OFF when it's actually ON or vice versa. Therefore, force the change bits
		// to be set in the changes_to_upload_to_comm_server bit field so the changes are uploaded.
		STATION_on_all_stations_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );

		// 3/23/2016 ajv : Also, set our pending flag in case of a power failure. Will guarantee
		// another try when power returns if the files have been saved (remember counts on bits set
		// in the files themselves).
		weather_preserves.pending_changes_to_send_to_comm_server = (true);

		// 3/23/2016 ajv : Finally, start the timer which triggers the data to be sent in 30
		// seconds.
		xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGE_MADE_MS ), portMAX_DELAY );
	}

	// ----------

	// 12/14/2015 ajv : Trigger an updated background calculation to ensure the No Water Day
	// change is applied to the next scheduled calculation.
	postBackground_Calculation_Event( BKGRND_CALC_NEXT_SCHEDULED );
}


/* ---------------------------------------------------------- */
extern void STATION_set_no_water_days_by_box( const UNS_32 pnow_days, const UNS_32 pbox_index_0, const UNS_32 preason_for_change )
{
	STATION_STRUCT			*lstation;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lstation = nm_ListGetFirst( &station_info_list_hdr );

	while( lstation != NULL )
	{
		// 2/10/2016 wjb : Only set no water days for stations that are phyiscally available in and
		// in use that belong to this box.
		if ( (nm_STATION_get_box_index_0(lstation) == pbox_index_0) && (STATION_station_is_available_for_use(lstation)) )
		{
			// 2/10/2016 wjb : Since we generate a single alert when setting the No Water Days for a
			// box, there's no reason to generate an individual change line for each station.
			nm_STATION_set_no_water_days( lstation, pnow_days, CHANGE_do_not_generate_change_line, preason_for_change, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, STATION_get_change_bits_ptr(lstation, preason_for_change) );
		}

		lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
	}

	// ----------

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------

	Alert_set_no_water_days_by_box(  pbox_index_0, pnow_days );

	if( preason_for_change == CHANGE_REASON_CENTRAL_OR_MOBILE )
	{
		// 3/23/2016 ajv : Since this command comes from Command Center Online, it inherently won't
		// trigger resending Program Data. However, that means Program Data will report that the
		// controller is OFF when it's actually ON or vice versa. Therefore, force the change bits
		// to be set in the changes_to_upload_to_comm_server bit field so the changes are uploaded.
		STATION_on_all_stations_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );

		// 3/23/2016 ajv : Also, set our pending flag in case of a power failure. Will guarantee
		// another try when power returns if the files have been saved (remember counts on bits set
		// in the files themselves).
		weather_preserves.pending_changes_to_send_to_comm_server = (true);

		// 3/23/2016 ajv : Finally, start the timer which triggers the data to be sent in 30
		// seconds.
		xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGE_MADE_MS ), portMAX_DELAY );
	}

	// ----------

	// 02/11/2015 wjb : Trigger an updated background calculation to ensure the No Water Day
	// change is applied to the next scheduled calculation.
	postBackground_Calculation_Event( BKGRND_CALC_NEXT_SCHEDULED );
}

/* ---------------------------------------------------------- */
extern void STATION_set_no_water_days_by_station( const UNS_32 pnow_days, const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, const UNS_32 preason_for_change )
{
	STATION_STRUCT	*lstation;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation = nm_STATION_get_pointer_to_station( pbox_index_0, pstation_number_0 );

	// 10/5/2015 ajv : Only set no water days for stations that are phyiscally available in and
	// in use.
	if( STATION_station_is_available_for_use(lstation) )
	{
		nm_STATION_set_no_water_days( lstation, pnow_days, CHANGE_generate_change_line, preason_for_change, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, STATION_get_change_bits_ptr( lstation, preason_for_change ) );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------

	if( preason_for_change == CHANGE_REASON_CENTRAL_OR_MOBILE )
	{
		// 3/23/2016 ajv : Since this command comes from Command Center Online, it inherently won't
		// trigger resending Program Data. However, that means Program Data will report that the
		// controller is OFF when it's actually ON or vice versa. Therefore, force the change bits
		// to be set in the changes_to_upload_to_comm_server bit field so the changes are uploaded.
		STATION_on_all_stations_set_or_clear_commserver_change_bits( COMMSERVER_CHANGE_BITS_ALL_SET );

		// 3/23/2016 ajv : Also, set our pending flag in case of a power failure. Will guarantee
		// another try when power returns if the files have been saved (remember counts on bits set
		// in the files themselves).
		weather_preserves.pending_changes_to_send_to_comm_server = (true);

		// 3/23/2016 ajv : Finally, start the timer which triggers the data to be sent in 30
		// seconds.
		xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGE_MADE_MS ), portMAX_DELAY );
	}

	// ----------

	// 12/14/2015 ajv : Trigger an updated background calculation to ensure the No Water Day
	// change is applied to the next scheduled calculation.
	postBackground_Calculation_Event( BKGRND_CALC_NEXT_SCHEDULED );
}

/* ---------------------------------------------------------- */
/**
 * This routine returns the value of the flag used to reset the moisture balance at the
 * conclusion of this station's next programmed irrigation. The flag is also used to skip
 * the moisture balance check at the start of the next irrigation.
 * 
 * @mutex_requirements The caller must be holding list_program_data_recursive_MUTEX to
 * ensure the station stays valid.
 *
 * @task_execution Executed within the context of the TD_CHECK task when stations are added
 * to the irrigation list and when the station completes it's programmed irrigation.
 * 
 * @param pstation A pointer to the station.
 *
 * @author 4/18/2016 AdrianusV
 */
extern BOOL_32 nm_STATION_get_ignore_moisture_balance_at_next_irrigation_flag( STATION_STRUCT *const pstation )
{
	return( pstation->ignore_moisture_balance_next_irrigation );
}

/* ---------------------------------------------------------- */
extern CHANGE_BITS_32_BIT *STATION_get_change_bits_ptr( STATION_STRUCT *pstation, const UNS_32 pchange_reason )
{
	return( SHARED_get_32_bit_change_bits_ptr( pchange_reason, &pstation->changes_to_send_to_master, &pstation->changes_to_distribute_to_slaves, &pstation->changes_to_upload_to_comm_server ) );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the station with the specified controller serial and 
 * station numbers. If no matching station is found in the stations list, NULL 
 * is returned. 
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being located isn't deleted
 *  	  during this process.
 *
 * @executed 
 * 
 * @param pcontroller_serial_number The serial number of the controller the 
 *  								station is physically connected to.
 * @param pstation_number_0 The 0-based station number.
 * 
 * @return STATION_STRUCT* A pointer to the station. Or NULL if not found!
 *
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 */
extern STATION_STRUCT *nm_STATION_get_pointer_to_station( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0 )
{
	// 10/24/2014 rmd : Caller must be holding the list_program_data MUTEX. Returns NULL if not
	// found.

	STATION_STRUCT *lstation;

	// 10/24/2014 rmd : A way to get an and learn the size of the station struct. Since it isn't
	// exposed to the application.
	//UNS_32	struct_size;
	//struct_size = sizeof( STATION_STRUCT );
	
	lstation = nm_ListGetFirst( &station_info_list_hdr );

	while( lstation != NULL )
	{
		if ( (lstation->box_index_0 == pbox_index_0) && (lstation->station_number_0 == pstation_number_0) )
		{
			break;
		}

		lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
	}

	return( lstation );
}

/* ---------------------------------------------------------- */
extern STATION_STRUCT *nm_STATION_get_pointer_to_decoder_based_station( const UNS_32 pbox_index_0, const UNS_32 pdecoder_serial_number, const UNS_32 pdecoder_output )
{
	// 10/24/2014 rmd : Same MUTEX requirements as prior function. Caller must be holding the
	// list_program_data MUTEX. Returns NULL if not found.
	
	STATION_STRUCT *lstation;

	lstation = NULL;
	
	// 10/24/2014 rmd : If the caller passed the all zeros in the argument (something wrong) the
	// return value would likely be conventional station 0 for box A. Let's not allow that to
	// happen.
	if( pdecoder_serial_number != 0 )
	{
		lstation = nm_ListGetFirst( &station_info_list_hdr );
	
		while( lstation != NULL )
		{
			if ( (lstation->box_index_0 == pbox_index_0) && (lstation->decoder_serial_number == pdecoder_serial_number) && (lstation->decoder_output == pdecoder_output) )
			{
				break;
			}
	
			lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
		}
	}

	return( lstation );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the next available station that's in use. If no other
 * stations are in use, the pointer to the original station is returned.
 * 
 * @mutex_requirements Calling function must have the
 * list_program_data_recursive_MUTEXT mutex to ensure the station we're working
 * with isn't removed during this process.
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the of the KEY PROCESSING task
 * when the user presses the NEXT key on the Station Programming screen.
 * 
 * @param pbox_index_ptr A pointer to the serial number of the controller
 * the station is physically connected to.
 * 
 * @param pstation_number_1_ptr A pointer to the 1-based station number.
 * 
 * @return STATION_STRUCT* A pointer to the next available station.
 *
 * @author 9/22/2011 Adrianusv
 *
 * @revisions (none)
 */
extern STATION_STRUCT *STATION_get_next_available_station( UNS_32 *pbox_index_ptr, UNS_32 *pstation_number_1_ptr )
{
	STATION_STRUCT	*lstation_ptr;
	
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation_ptr = nm_STATION_get_pointer_to_station( *pbox_index_ptr, ((*pstation_number_1_ptr) - 1) );

	if( lstation_ptr != NULL )
	{
		// 2012.08.12 ajv : Loop through the station list until we either find
		// the next station in use or we land back on the station we started
		// with.
		do
		{
			// We're now pointing to the current record, so traverse to the next available.
			lstation_ptr = nm_ListGetNext( &station_info_list_hdr, lstation_ptr );

			if( lstation_ptr == NULL )
			{
				// The current record was the last station in the list, so grab the first station.
				lstation_ptr = nm_ListGetFirst( &station_info_list_hdr );
			}

			if( (lstation_ptr->box_index_0 == *pbox_index_ptr) && (lstation_ptr->station_number_0 == (*pstation_number_1_ptr)-1) )
			{
				// We've landed on the same station we started with. Therefore,
				// break out of the loop.
			}

		} while( STATION_station_is_available_for_use(lstation_ptr) == (false) );

		// 7/27/2012 rmd : Assign the box index and station number.
		*pbox_index_ptr = lstation_ptr->box_index_0;

		*pstation_number_1_ptr = ((lstation_ptr->station_number_0) + 1);
	}
	else
	{
		Alert_station_not_found();
	}
	
	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return ( lstation_ptr );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the previous available station that's in use. If no
 * other stations are in use, the pointer to the original station is returned.
 * 
 * @mutex_requirements Calling function must have the
 * list_program_data_recursive_MUTEXT mutex to ensure the station we're working
 * with isn't removed during this process.
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the of the KEY PROCESSING task
 * when the user presses the PREV key on the Station Programming screen.
 * 
 * @param pbox_index_ptr A pointer to the serial number of the controller
 * the station is physically connected to.
 * 
 * @param pstation_number_1_ptr A pointer to the 1-based station number.
 * 
 * @return STATION_STRUCT* A pointer to the previous available station.
 *
 * @author 9/22/2011 Adrianusv
 *
 * @revisions (none)
 */
extern STATION_STRUCT *STATION_get_prev_available_station( UNS_32 *pbox_index_ptr, UNS_32 *pstation_number_1_ptr )
{
	STATION_STRUCT	*lstation_ptr;
	
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation_ptr = nm_STATION_get_pointer_to_station( *pbox_index_ptr, ((*pstation_number_1_ptr) - 1) );

	if( lstation_ptr != NULL )
	{
		// 2012.08.12 ajv : Loop through the station list until we either find
		// the previous station in use or we land back on the station we started
		// with.
		do
		{
			// We're now pointing to the current record, so traverse to the next available.
			lstation_ptr = nm_ListGetPrev( &station_info_list_hdr, lstation_ptr );

			if( lstation_ptr == NULL )
			{
				// The current record was the first station in the list, so grab the last station.
				lstation_ptr = nm_ListGetLast( &station_info_list_hdr );
			}

			if( (lstation_ptr->box_index_0 == *pbox_index_ptr) && (lstation_ptr->station_number_0 == (*pstation_number_1_ptr)-1) )
			{
				// We've landed on the same station we started with. Therefore,
				// break out of the loop.
			}

		} while( STATION_station_is_available_for_use(lstation_ptr) == (false) );
	}

	// 7/27/2012 rmd : Assign the box index and station number.
	if( lstation_ptr != NULL )
	{
		*pbox_index_ptr = lstation_ptr->box_index_0;

		*pstation_number_1_ptr = ((lstation_ptr->station_number_0) + 1);
	}
	else
	{
		Alert_station_not_found();
	}
	
	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return ( lstation_ptr );
}

/* ---------------------------------------------------------- */
extern void STATION_clean_house_processing( void )
{
	// 4/16/2014 rmd : Executed within the context of the comm_mngr task. ONLY BY SLAVES. THE
	// CHANGES MADE HERE DO NOT BE DISTRIBUTED. THE STATION LIST IS BEING WIPED IN PREPARATION
	// OF BEING ADDED AS A NEW MEMBER TO A CHAIN. Called when the result of a scan indicates
	// this controller is a new slave in the chain. Used when building up chains. Likely ONLY
	// used when assembling a chain for the first time or when increasing the length of a chain
	// to a length it has never 'run' at before. Or if you changed the LETTER of a slave in a
	// chain.
	
	void	*tptr;
	
	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	tptr = nm_ListRemoveHead( &station_info_list_hdr );

	while( tptr != NULL )
	{
		mem_free( tptr );

		tptr = nm_ListRemoveHead( &station_info_list_hdr );
	}

	// ----------

	// 4/16/2014 rmd : By setting the no groups exist flag to false no change bits will be set
	// for this 'dummy' group. And that is what we want.
	nm_GROUP_create_new_group( &station_info_list_hdr, (char*)STATION_NAME, &nm_STATION_set_default_values_resulting_in_station_0_for_this_box, sizeof( STATION_STRUCT ), (false), NULL );

	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE, STATION_INFO_FILENAME, STATION_INFO_LATEST_FILE_REVISION, &station_info_list_hdr, sizeof( STATION_STRUCT ), list_program_data_recursive_MUTEX, FF_STATION_INFO );

	// ----------

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void STATION_set_not_physically_available_based_upon_communication_scan_results( void )
{
	// 4/24/2014 rmd : Executed within the context of the comm_mngr. At the conclusion of the
	// scan by masters (chains and standalones). By standalones so they could clean their list
	// in case they were in a chain previously. The changes made here should be marked for
	// distribution.
	
	// ----------
	
	STATION_STRUCT		*lstation_ptr;
	
	UNS_32	our_box_index_0;

	// ----------

	our_box_index_0 = FLOWSENSE_get_controller_index();
	
	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	lstation_ptr = nm_ListGetFirst( &station_info_list_hdr );

	while( lstation_ptr != NULL )
	{
		// 4/24/2014 rmd : HIDE stations based upon the scan results. Revealing or station creation
		// takes place when a controller receives its whats installed from its OWN tpmicro.
		if( !chain.members[ lstation_ptr->box_index_0 ].saw_during_the_scan )
		{
			// 4/24/2014 rmd : Set physically_available (false) if needed. Set the change bits so the
			// change is distributed.
			nm_STATION_set_physically_available( lstation_ptr, (false), CHANGE_generate_change_line, CHANGE_REASON_NORMAL_STARTUP, our_box_index_0, (true), STATION_get_change_bits_ptr( lstation_ptr, CHANGE_REASON_NORMAL_STARTUP ) );
		}
		
		// ----------
		
		lstation_ptr = nm_ListGetNext( &station_info_list_hdr, lstation_ptr );
	}

	// ----------

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void STATION_for_stations_on_this_box_set_physically_available_based_upon_discovery_results( void )
{
	// 4/24/2014 rmd : Executed within the context of the comm_mngr when parsing the message
	// from the tpmicro when contains the decoder list. At the conclusion of the discovery.
	// Think of this as executing on the irri side at a slave. We are concerned only with
	// stations that belong to this box. And the decoder list is for decoders on this box.
	
	// ----------
	
	STATION_STRUCT		*lstation_ptr;

	UNS_32				ddd, our_box_index_0;
	
	BOOL_32				found_decoder;

	// ----------

	our_box_index_0 = FLOWSENSE_get_controller_index();
	
	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );
	
	xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lstation_ptr = nm_ListGetFirst( &station_info_list_hdr );

	while( lstation_ptr != NULL )
	{
		// 7/8/2014 rmd : Only if the station is on our box. And the decoder serial number is
		// non-zero meaning this is a decoder based station.
		if( (lstation_ptr->box_index_0 == our_box_index_0) && lstation_ptr->decoder_serial_number )
		{
			found_decoder = (false);
			
			for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
			{
				// 7/8/2014 rmd : If the serial number is 0 we've reached the end of populated entries in
				// the list.
				if( !tpmicro_data.decoder_info[ ddd ].sn )
				{
					break;					
				}

				if( tpmicro_data.decoder_info[ ddd ].sn == lstation_ptr->decoder_serial_number )
				{
					// 7/8/2014 rmd : This station indeed uses this decoder. So make him physically available.
					nm_STATION_set_physically_available( lstation_ptr, (true), CHANGE_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, our_box_index_0, (true), STATION_get_change_bits_ptr( lstation_ptr, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED ) );

					// 7/8/2014 rmd : And flag that we found the decoder for this station so do not set
					// physically available for this station (false).
					found_decoder = (true);
					
					break;
				}
			}
			
			if( !found_decoder )
			{
				// 4/24/2014 rmd : Set physically_available (false) for the case when no decoder is found to
				// support the station.
				nm_STATION_set_physically_available( lstation_ptr, (false), CHANGE_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_REMOVED, our_box_index_0, (true), STATION_get_change_bits_ptr( lstation_ptr, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_REMOVED ) );
			}
		}

		// ----------
		
		lstation_ptr = nm_ListGetNext( &station_info_list_hdr, lstation_ptr );
	}

	// ----------

	xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void STATION_set_bits_on_all_stations_to_cause_distribution_in_the_next_token( void )
{
	STATION_STRUCT	*lstation_ptr;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation_ptr = nm_ListGetFirst( &station_info_list_hdr );

	while( lstation_ptr != NULL )
	{
		// 5/1/2014 rmd : This function ONLY being called at the conclusion of a SCAN by the master
		// of a real multiple controller chain - we want to broadcast to the slaves using tokens
		// about all the hardware we presently know about. In this case the stations.
		SHARED_set_all_32_bit_change_bits( &lstation_ptr->changes_to_distribute_to_slaves, list_program_data_recursive_MUTEX );

		lstation_ptr = nm_ListGetNext( &station_info_list_hdr, lstation_ptr );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------

	// 4/29/2014 ajv : Set a flag in the changes structure to notify this
	// controller to examine its lists for changes to distribute to the other
	// controllers in the chain in it's next token resposne. The flag is part of
	// the mutex protected comm_mngr struct so take the MUTEX.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	comm_mngr.changes.distribute_changes_to_slave = (true);

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void STATION_on_all_stations_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear )
{
	STATION_STRUCT	*lstation_ptr;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation_ptr = nm_ListGetFirst( &station_info_list_hdr );

	while( lstation_ptr != NULL )
	{
		if( pset_or_clear == COMMSERVER_CHANGE_BITS_ALL_CLEARED )
		{
			SHARED_clear_all_32_bit_change_bits( &lstation_ptr->changes_to_upload_to_comm_server, list_program_data_recursive_MUTEX );
		}
		else
		{
			SHARED_set_all_32_bit_change_bits( &lstation_ptr->changes_to_upload_to_comm_server, list_program_data_recursive_MUTEX );
		}

		lstation_ptr = nm_ListGetNext( &station_info_list_hdr, lstation_ptr );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}


/* ---------------------------------------------------------- */
/**
 * @mutex_requirements Requires the caller to have the pending_changes_recursive_MUTEX to
 * ensure no other function tries to modify the pending_changes_to_send_to_comm_server flag.
 *
 * @task_execution Executed within the context of the CONTROLLER INITIATED task when it
 * first starts and when the waiting response flags are cleared after a communication.
 */
extern void nm_STATION_update_pending_change_bits( const UNS_32 pcomm_error_occurred )
{
	STATION_STRUCT	*lstation;

	BOOL_32	lfile_save_necessary;

	lfile_save_necessary = (false);

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lstation = nm_ListGetFirst( &station_info_list_hdr );

	while( lstation != NULL )
	{
		// 4/30/2015 ajv : No reason to process this if none of the bits are set. This also helps
		// reduce the file save activity.
		if( lstation->changes_uploaded_to_comm_server_awaiting_ACK != CHANGE_BITS_32_BIT_ALL_CLEAR )
		{
			// 4/29/2015 ajv : If a communication error occurred while sending Program Data, copy the
			// bits of variables that were sent back into the original bit field to ensure they're sent
			// again during the next attempt. Otherwise, clear the pending bits.
			if( pcomm_error_occurred )
			{
				lstation->changes_to_upload_to_comm_server |= lstation->changes_uploaded_to_comm_server_awaiting_ACK;

				// 4/30/2015 ajv : Since we've determined there were changes that were not successfully
				// sent, set the flag to trigger another attempt
				weather_preserves.pending_changes_to_send_to_comm_server = (true);

				// 4/30/2015 ajv : Also set the timer to force the data to send. If the timer's already
				// running, this will simply restart it.
				// 
				// 4/30/2015 ajv : BECAUSE of the potential that this function is executed many times (once
				// for each item changed) we could not start our pdata timer with a post to the CI queue.
				// The queue would overflow. So start the timer directly.
				xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGES_NOT_SENT_MS ), portMAX_DELAY );
			}
			else
			{
				SHARED_clear_all_32_bit_change_bits( &lstation->changes_uploaded_to_comm_server_awaiting_ACK, list_program_data_recursive_MUTEX );
			}

			// 4/30/2015 ajv : At least one station was updated. So make sure we save the file.
			lfile_save_necessary = (true);
		}

		lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	if( lfile_save_necessary )
	{
		// 4/29/2015 ajv : Now that the bits are set or cleared accordingly, trigger a file save.
		// If power fails between building the message and receiving the ACK, the original bit field
		// hasn't been saved to flash yet, so it will resend the associated variables during the
		// next attempt.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_STATION_INFO, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
	}
}

/* ---------------------------------------------------------- */
void STATIONS_load_all_the_stations_for_ftimes( void )
{
	// 2/12/2018 rmd : Executed within the context of the FTIMES task. Upon entry it is assumed
	// that the station list has been cleaned ie set to all zeros. It is also assumed the FTimes
	// Station Group list has been built.
	
	STATION_STRUCT	*lstation_ptr;
	
	UNS_32			stations_so_far;
	
	UNS_32			lstation_preserves_index;
	
	const volatile float TEN_POINT_ZERO = 10.0;

	const volatile float SIXTY_POINT_ZERO = 60.0;
	
	// ----------
	
	if( station_info_list_hdr.count >= FTIMES_MAX_NUMBER_OF_STATIONS )
	{
		Alert_Message( "FTIMES ERROR: too many stations in network!" );
	}
	else
	{
		stations_so_far = 0;
		
		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );
	
		lstation_ptr = nm_ListGetFirst( &station_info_list_hdr );
	
		while( lstation_ptr != NULL )
		{
			// 2/12/2018 rmd : FTIMES on those available and in use.
			if( lstation_ptr->physically_available && lstation_ptr->in_use_bool )
			{
				// 2/14/2018 rmd : Find the station group in our ftimes station group list.
				ft_stations[ stations_so_far ].station_group_ptr = FTIMES_FUNCS_find_station_group_in_ftimes_list_with_this_GID( lstation_ptr->GID_station_group );

				ft_stations[ stations_so_far ].system_ptr = FTIMES_FUNCS_find_system_group_in_ftimes_list_with_this_GID( ft_stations[ stations_so_far ].station_group_ptr->GID_irrigation_system );
				
				// 2/14/2018 rmd : If the station is assigned to a station group and we found the group in
				// our ftimes list, OR if the station is assigned to a manual program, we may proceed.
				// Otherwise we're not interested in this station for ftimes cause there are no potential
				// start times available. Also check the system_ptr is there.
				if( ft_stations[ stations_so_far ].system_ptr && (ft_stations[ stations_so_far ].station_group_ptr || lstation_ptr->GID_manual_program_A || lstation_ptr->GID_manual_program_B) )
				{
					// 2/12/2018 rmd : Occupy the array slot, and fill out the station.
					ft_stations[ stations_so_far ].ftimes_support.slot_loaded = (true);
					
					// 2/12/2018 rmd : Remember the array has been completely zeroed already, so no need to be
					// explicit when desiring a zero. Also no need to explicitly init the list links.
					
					ft_stations[ stations_so_far ].station_number_0 = lstation_ptr->station_number_0;

					// ----------
					
					ft_stations[ stations_so_far ].box_index_0 = lstation_ptr->box_index_0;
	
					// 5/11/2012 rmd : Make sure the index is within range as we are going to use it 'blindly'
					// to index arrays. If out of range a bug like that could cause a crash and be VERY hard to
					// find.
					RangeCheck_uint32( &ft_stations[ stations_so_far ].box_index_0, BOX_INDEX_MINIMUM, BOX_INDEX_MAXIMUM, BOX_INDEX_DEFAULT, NULL, "Box Index" );

					// ----------
					
					ft_stations[ stations_so_far ].user_programmed_seconds = (((float)lstation_ptr->total_run_minutes_10u * SIXTY_POINT_ZERO) / TEN_POINT_ZERO);
	
					ft_stations[ stations_so_far ].cycle_seconds_original_from_station_info = (((float)lstation_ptr->cycle_minutes_10u * SIXTY_POINT_ZERO) / TEN_POINT_ZERO);

					ft_stations[ stations_so_far ].soak_seconds_original_from_station_info = ((float)lstation_ptr->soak_minutes * SIXTY_POINT_ZERO);
					
					// ----------
					
					// 8/2/2018 rmd : This is really a nit pick I suppose, but should better reflect what is
					// actually going to run. Watersense lops of and collects partials of 3 minute cycles. It
					// collects them, accumulating them. Eventually they'll be irrigated. So include the PRESENT
					// status of the accumulated time, because it is real, and will be potentially irrigated in
					// the coming days.
					STATION_PRESERVES_get_index_using_box_index_and_station_number( lstation_ptr->box_index_0, lstation_ptr->station_number_0, &lstation_preserves_index );
					
					ft_stations[ stations_so_far ].water_sense_deferred_seconds = station_preserves.sps[ lstation_preserves_index ].left_over_irrigation_seconds;

					// ----------
					
					ft_stations[ stations_so_far ].et_factor_100u = lstation_ptr->et_factor_100u;

					ft_stations[ stations_so_far ].distribution_uniformity_100u = lstation_ptr->distribution_uniformity_100u;
					

					ft_stations[ stations_so_far ].expected_flow_rate_gpm_u16 = lstation_ptr->expected_flow_rate_gpm;
	

					ft_stations[ stations_so_far ].manual_program_ptrs[ 0 ] = FTIMES_VARS_return_ptr_to_manual_program_group_in_ftimes_list_with_this_GID( lstation_ptr->GID_manual_program_A );
					ft_stations[ stations_so_far ].manual_program_ptrs[ 1 ] = FTIMES_VARS_return_ptr_to_manual_program_group_in_ftimes_list_with_this_GID( lstation_ptr->GID_manual_program_B );
					

					ft_stations[ stations_so_far ].bbf.w_uses_the_pump = ft_stations[ stations_so_far ].station_group_ptr->pump_in_use;
					
					ft_stations[ stations_so_far ].bbf.station_priority = ft_stations[ stations_so_far ].station_group_ptr->priority_level;
					
					// ----------
					
					ft_stations[ stations_so_far ].bbf.flow_check_hi_action = ft_stations[ stations_so_far ].station_group_ptr->high_flow_action;

					ft_stations[ stations_so_far ].bbf.flow_check_lo_action = ft_stations[ stations_so_far ].station_group_ptr->low_flow_action;
					
					ft_stations[ stations_so_far ].bbf.flow_check_group = FOAL_IRRI_translate_alert_actions_to_flow_checking_group( &ft_stations[ stations_so_far ].bbf );

					// ----------
					
					// 3/15/2018 rmd : BECAUSE prior to loading the stations, we completely ZERO the entire
					// ft_stations array, the following statements need NOT execute. They are here for reference
					// though. Leave commented out.

					//ft_stations[ stations_so_far ].bbf.flow_check_to_be_excluded_from_future_checking = (false);

					//ft_stations[ stations_so_far ].bbf.flow_check_when_possible_based_on_reason_in_list = (false);
					
					//ft_stations[ stations_so_far ].bbf.w_involved_in_a_flow_problem = (false);
					
					//ft_stations[ stations_so_far ].bbf.at_some_point_flow_was_checked = (false);
					
					//ft_stations[ stations_so_far ].bbf.at_some_point_should_check_flow = (false);
					
					//ft_stations[ stations_so_far ].bbf.responds_to_rain = (false);

					//ft_stations[ stations_so_far ].bbf.responds_to_wind = (false);

					// ----------
					
					stations_so_far += 1;
				}
			}
			
			lstation_ptr = nm_ListGetNext( &station_info_list_hdr, lstation_ptr );
		}
	
		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}
}

/* ---------------------------------------------------------- */
BOOL_32 STATION_calculate_chain_sync_crc( UNS_32 const pff_name )
{
	// 8/30/2018 rmd : Executed in the context of the comm_mngr task when token responses are
	// being made. And that's it.

	// ----------

	STATION_STRUCT		*lstation_ptr;
	
	UNS_8				*checksum_start;

	UNS_32				checksum_length;

	UNS_8				*ucp;
	
	BOOL_32				rv;

	// ----------

	rv = (false);

	// ----------
	
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	// 8/30/2018 rmd : So the CRC calculation function wants a continuous memory space and a
	// length to calculate the crc off. In this function we will pick out the desired data, and
	// simply pile it up on itself, list item by list item. We don't care how messy the pile is
	// because we never attempt to reference any data once it is added to the pile. The data is
	// only there to include in the checksum.
	//
	// 8/30/2018 rmd : Well we know it can't be bigger than the list item count times the size
	// of each list item, so start with a memory block that big.
	if( mem_obtain_a_block_if_available( (station_info_list_hdr.count * sizeof(STATION_STRUCT)), (void**)&checksum_start ) )
	{
		// 9/5/2018 rmd : Well then the calculation will be successful. Nothing else can prevent
		// that outcome.
		rv = (true);
		
		// ----------
		
		ucp = checksum_start;
		
		checksum_length = 0;
		
		// ----------
		
		// 8/30/2018 rmd : Rattle though the list and push the fields we want to include in the sync
		// test onto the block to be checksummed.
		lstation_ptr = nm_ListGetFirst( &station_info_list_hdr );
		
		while( lstation_ptr != NULL )
		{
			// 9/4/2018 rmd : Make sure you include only TRUE USER SETTINGS, no flags, or pointers to
			// memory, etc...
			
			// ----------
			
			// 11/2/2018 rmd : Due to the inconsistent way the group names strings are handled during
			// the sync (we do not regligiously take all 48 characters and always carry them through the
			// sync process, there are places we only copy up until the null), the part of the string
			// following the null char is not guaranteed to be in sync. So only include up until the
			// null in the crc test.
			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->base.description, strlen(lstation_ptr->base.description) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->physically_available, sizeof(lstation_ptr->physically_available) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->in_use_bool, sizeof(lstation_ptr->in_use_bool) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->station_number_0, sizeof(lstation_ptr->station_number_0) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->box_index_0, sizeof(lstation_ptr->box_index_0) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->decoder_serial_number, sizeof(lstation_ptr->decoder_serial_number) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->decoder_output, sizeof(lstation_ptr->decoder_output) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->total_run_minutes_10u, sizeof(lstation_ptr->total_run_minutes_10u) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->cycle_minutes_10u, sizeof(lstation_ptr->cycle_minutes_10u) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->soak_minutes, sizeof(lstation_ptr->soak_minutes) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->et_factor_100u, sizeof(lstation_ptr->et_factor_100u) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->expected_flow_rate_gpm, sizeof(lstation_ptr->expected_flow_rate_gpm) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->distribution_uniformity_100u, sizeof(lstation_ptr->distribution_uniformity_100u) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->no_water_days, sizeof(lstation_ptr->no_water_days) );

			// 9/12/2018 rmd : Skipping no longer used (x3)

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->GID_station_group, sizeof(lstation_ptr->GID_station_group) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->GID_manual_program_A, sizeof(lstation_ptr->GID_manual_program_A) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->GID_manual_program_B, sizeof(lstation_ptr->GID_manual_program_B) );

			// 9/12/2018 rmd : Skipping changes bitfield (x3).
			
			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->moisture_balance_percent, sizeof(lstation_ptr->moisture_balance_percent) );

			// 9/12/2018 rmd : Skipping changes bitfield.
			
			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_ptr->square_footage, sizeof(lstation_ptr->square_footage) );
			
			// 9/12/2018 rmd : Skipping suspect, internal values.
			
			// END
			
			// ----------
		
			lstation_ptr = nm_ListGetNext( &station_info_list_hdr, lstation_ptr );
		}
		
		// ----------
		
		// 8/30/2018 rmd : Now calculate the checksum.
		cscs.the_crc[ pff_name ] = CRC_calculate_32bit_big_endian( checksum_start, checksum_length );
		
		// ----------
		
		// 9/4/2018 rmd : We're done with the block, so of course free it.
		mem_free( checksum_start );
	}
	else
	{
		// 9/5/2018 rmd : So by design it's okay if there is no memory. The checksum won't be set
		// valid, and the next time we try to include it in a token response we'll try to calculate
		// it again. Self protecting and retrying. But let's alert about this so we know it's taking
		// place.
		Alert_Message_va( "SYNC: no mem to calc checksum %s", chain_sync_file_pertinants[ pff_name ].file_name_string );
	}
	
	// ----------
	
	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

