
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"walk_thru.h"

#include	"app_startup.h"

#include	"bithacks.h"

#include	"change.h"

#include	"shared.h"

#include	"comm_mngr.h"

#include	"controller_initiated.h"

#include	"flash_storage.h"

#include	"irri_comm.h"

#include	"chain_sync_vars.h"

#include	"chain_sync_funcs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static MIST_LIST_HDR_TYPE	walk_thru_group_list_hdr;

INT_32	WALK_THRU_station_array_for_gui[ MAX_NUMBER_OF_WALK_THRU_STATIONS ];

// ----------

// 2/18/2016 rmd : Must include the shared c source after the
// manual_programs_group_list_header declaration.
#include	"shared_walk_thru.c"

// ----------

static const char WALK_THRU_FILENAME[] =	"WALK_THRU";


const char WALK_THRU_DEFAULT_NAME[] =		"Walk Thru";

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 1/26/2015 rmd : NOTE - upon REVISION update the record can only grow in size. At least
// that is the way I've coded the flash file supporting functions. It may be possible to
// decrease the record size. I just haven't gone through the code with that in mind.

#define	WALK_THRU_LATEST_FILE_REVISION		(1)

// ----------

const UNS_32 walk_thru_list_item_sizes[ WALK_THRU_LATEST_FILE_REVISION + 1 ] =
{
	// 1/26/2015 rmd : In order for this array to contain OLDER list item sizes the implication
	// is that the older structure definitions are to be retained. That means the newer
	// structure definition has a different name.

	// ----------
	sizeof( WALK_THRU_STRUCT ),

	sizeof( WALK_THRU_STRUCT )
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void nm_WALK_THRU_set_default_values( void *const pgroup, const BOOL_32 pset_change_bits );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void nm_walk_thru_updater( UNS_32 pfrom_revision )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only when
	// the file revision is changed. The caller is to be holding the walk_thru
	// recursive_MUTEX.

	// ----------
	
	//WALK_THRU_STRUCT	*mpgs;
	
	//CHANGE_BITS_32_BIT	*lbitfield_of_changes;

	//UNS_32	box_index_0;

	// ----------
	
	// 9/21/2015 rmd : Because of the order of the init_file function calls in the app_startup
	// task we know the controller index is valid for use at this [early] point during the boot
	// process that this updater function is called.
	//box_index_0 = FLOWSENSE_get_controller_index();

	// ----------

	if( pfrom_revision == WALK_THRU_LATEST_FILE_REVISION )
	{
		Alert_Message_va( "WALK THRU file unexpd update %u", pfrom_revision );	
	}
	
	// ----------
	// 2/9/2018 Ryan : body of updater goes here.
	// ----------

	// 1/29/2015 rmd : Well after the updater has run the pfrom_revision is supposed to equal
	// the latest revision. By design of this update process.
	if( pfrom_revision != WALK_THRU_LATEST_FILE_REVISION )
	{
		Alert_Message( "WALK THRU updater error" );
	}
}

/* ---------------------------------------------------------- */
extern void init_file_walk_thru( void )
{
	FLASH_FILE_initialize_list_and_find_or_create_group_list_file(	FLASH_INDEX_1_GENERAL_STORAGE,
																	WALK_THRU_FILENAME,
																	WALK_THRU_LATEST_FILE_REVISION,
																	&walk_thru_group_list_hdr,
																	walk_thru_list_item_sizes,
																	walk_thru_recursive_MUTEX,
																	&nm_walk_thru_updater,
																	&nm_WALK_THRU_set_default_values,
																	(char*)WALK_THRU_DEFAULT_NAME,
																	FF_WALK_THRU );
}

/* ---------------------------------------------------------- */
extern void save_file_walk_thru( void )
{
	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE,
															WALK_THRU_FILENAME,
															WALK_THRU_LATEST_FILE_REVISION,
															&walk_thru_group_list_hdr,
															sizeof( WALK_THRU_STRUCT ),
															walk_thru_recursive_MUTEX,
															FF_WALK_THRU );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void nm_WALK_THRU_set_default_values( void *const pgroup, const BOOL_32 pset_change_bits )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	UNS_32	i;

	// ------------------------

	UNS_32	box_index_0;

	box_index_0 = FLOWSENSE_get_controller_index();

	// ------------------------

	if( pgroup != NULL )
	{
		// 5/14/2014 ajv : Clear all of the change bits for POC since it's just
		// been created. We'll then explicitly set each bit based upon whether
		// pset_change_bits is true or not.
		SHARED_clear_all_32_bit_change_bits( &((WALK_THRU_STRUCT *)pgroup)->changes_to_send_to_master, walk_thru_recursive_MUTEX );
		SHARED_clear_all_32_bit_change_bits( &((WALK_THRU_STRUCT *)pgroup)->changes_to_distribute_to_slaves, walk_thru_recursive_MUTEX );
		SHARED_clear_all_32_bit_change_bits( &((WALK_THRU_STRUCT *)pgroup)->changes_uploaded_to_comm_server_awaiting_ACK, walk_thru_recursive_MUTEX );

		// 1/20/2015 ajv : However, we should explicitly set ALL bits for the Comm Server to ensure
		// changes are uploaded on a clean start.
		SHARED_set_all_32_bit_change_bits( &((WALK_THRU_STRUCT *)pgroup)->changes_to_upload_to_comm_server, walk_thru_recursive_MUTEX );

		// ----------

		// 5/2/2014 ajv : When creating a new group, always set the changes to
		// be sent to the master. Once the master receives the changes, it will
		// distribute them back out to the slaves.
		lchange_bitfield_to_set = &((WALK_THRU_STRUCT *)pgroup)->changes_to_send_to_master;
		// ----------

		nm_WALK_THRU_set_station_run_time( pgroup, WALK_THRU_STATION_RUN_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
		
		nm_WALK_THRU_set_delay_before_start( pgroup, WALK_THRU_DELAY_BEFORE_START_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_WALK_THRU_set_in_use( pgroup, WALK_THRU_IN_USE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
		
		nm_WALK_THRU_set_box_index_0( pgroup, WALK_THRU_BOX_INDEX_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
		
		for( i = 0; i < MAX_NUMBER_OF_WALK_THRU_STATIONS; ++i )
		{
			nm_WALK_THRU_set_stations( pgroup, i, WALK_THRU_SEQUENCE_SLOT_station_number_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
		}
	}
	else
	{
		Alert_func_call_with_null_ptr();
	}
}

/* ---------------------------------------------------------- */
extern void *nm_WAlK_THRU_create_new_group( const UNS_32 pchanges_received_from )
{
	WALK_THRU_STRUCT	*litem_to_insert_ahead_of;

	WALK_THRU_STRUCT	*lgroup;

	// ----------

	litem_to_insert_ahead_of = nm_ListGetFirst( &walk_thru_group_list_hdr );

	// 2/11/2016 ajv : Loop through each manual program until we either find one that's deleted
	// or we reach the end of the list. If we find one that's been deleted, we're going to
	// insert the new item ahead of it.
	while( litem_to_insert_ahead_of != NULL )
	{
		if( !litem_to_insert_ahead_of->in_use )
		{
			// 2/11/2016 ajv : We found a deleted one so break out of the looop so the new item in
			// inserted ahead of it.
			break;
		}

		litem_to_insert_ahead_of = nm_ListGetNext( &walk_thru_group_list_hdr, litem_to_insert_ahead_of );
	}

	// ----------

	// 5/2/2014 ajv : TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// (requires pchanges_received_from)

	// 2/11/2016 ajv : Note that we're no longer passing a NULL for the pnext_list_item
	// parameter. The reason is because the list is now sorted with deleted items (aka not in
	// use) at the end of the list. We therefore need to insert a new item before the first
	// deleted item, if it exists.
	lgroup = nm_GROUP_create_new_group( &walk_thru_group_list_hdr, (char*)WALK_THRU_DEFAULT_NAME, &nm_WALK_THRU_set_default_values, sizeof(WALK_THRU_STRUCT), (true), litem_to_insert_ahead_of );

	// ----------

	return( lgroup );
}

/* ---------------------------------------------------------- */
/**
 * This function runs through the POC list and copies the contents of any changed groups to
 * the passed in ucp.
 * 
 * @executed Executed within the context of the CommMngr task as a token, token response, or
 * Program Data message to the Comm Server is being built.
 * 
 * @param pucp A pointer to the pointer where the program data is being built.
 * 
 * @param pmem_used_so_far The amount of memory used so far. Once this reaches our limit for
 * a single token (MEMORY_TO_ALLOCATE_FOR_SENDING_CHANGES), processing of the remaining
 * changes stops until it's time to build the next token or token response.
 * 
 * @param pmem_used_so_far_is_less_than_allocated_memory A pointer to the flag which
 * indicates whether we're allowed to add more changes to the token or token response.
 * 
 * @param preason_data_is_being_built The reason why the data is being built to send. Based
 * upon this reason, we examine a different change bit structure to determine what to
 * send. Valid values are CHANGE_REASON_SYNC_SENDING_TO_MASTER,
 * CHANGE_REASON_SYNC_DISTRIBUTING_TO_SLAVES, CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER.
 * 
 * @return UNS_32 The total size of the added data. This is used to determine whether any
 * changes were detected so the appropriate file change bit can be set as well as to
 * determine how much memory must be allocated to store the changes.
 *
 * @author Ryan
 */
extern UNS_32 WALK_THRU_build_data_to_send( UNS_8 **pucp,
												  const UNS_32 pmem_used_so_far,
												  BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
												  const UNS_32 preason_data_is_being_built,
												  const UNS_32 pallocated_memory )
{
	CHANGE_BITS_32_BIT	*lbitfield_of_changes_to_use_to_determine_what_to_send;

	CHANGE_BITS_32_BIT	lbitfield_of_changes_to_send;

	WALK_THRU_STRUCT	*lgroup;

	UNS_8	*llocation_of_num_changed_groups;

	UNS_8	*llocation_of_bitfield;

	// 3/29/2018 Ryan : Track how many walk-thrus are added to the message.
	UNS_32	lnum_changed_groups;

	// Store the amount of memory used for current walk-thru. This is compared to the
	// program's overhead to determine whether any of the program's values were actually added
	// or not.
	UNS_32	lmem_used_by_this_group;

	// Determine how much overhead each walk-thru requires in case we add the overhead and
	// don't have enough room to add of the actual changes.
	UNS_32	lmem_overhead_per_group;

	UNS_32	rv;

	// ----------

	rv = 0;

	lnum_changed_groups = 0;

	// ----------

	// 3/29/2018 Ryan : Take the appropriate mutex to ensure no walk-thrus are added or
	// removed during this process.
	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lgroup = nm_ListGetFirst( &walk_thru_group_list_hdr );

	// 3/29/2018 Ryan : Loop through each walk-thru and count how many have change bits
	// set. This is really just used to determine whether there's at least one walk-thru which
	// needs to be examined.
	while( lgroup != NULL )
	{
		if( *(WALK_THRU_get_change_bits_ptr( lgroup, preason_data_is_being_built )) != CHANGE_BITS_32_BIT_ALL_CLEAR )
		{
			lnum_changed_groups++;

			// 3/29/2018 Ryan : Since we don't actually care about the number of walk-thrus that
			// have changed at this point - just that at least one has changed - we can break out of the
			// loop now.
			break;
		}

		lgroup = nm_ListGetNext( &walk_thru_group_list_hdr, lgroup );
	}

	// ----------

	if( lnum_changed_groups > 0 )
	{
		// 3/29/2018 Ryan : Initialize the changed walk-thrus back to 0. Since not all of the
		// walk-thrus may be added due to token size limitations, we need to ensure that the
		// receiving party only processes the number of walk-thrus actually added to the message.
		lnum_changed_groups = 0;

		// ----------

		// 3/29/2018 Ryan : Allocate space to store the number of changed walk-thrus that are
		// included in the message so the receiving party can correctly parse the data. This will be
		// filled in later once we have an actual count.
		rv = PDATA_allocate_space_for_num_changed_groups_in_pucp( pucp, &llocation_of_num_changed_groups, pmem_used_so_far, pmem_used_so_far_is_less_than_allocated_memory, pallocated_memory );

		// ----------

		// 3/29/2018 Ryan : Well there was at least enough space for the 2 bytes for the count of
		// number of changed groups.
		if( *pmem_used_so_far_is_less_than_allocated_memory )
		{
			lgroup = nm_ListGetFirst( &walk_thru_group_list_hdr );

			while( lgroup != NULL )
			{
				lbitfield_of_changes_to_use_to_determine_what_to_send = WALK_THRU_get_change_bits_ptr( lgroup, preason_data_is_being_built );

				// 3/29/2018 Ryan : If this walk-thru has changed...
				if( *lbitfield_of_changes_to_use_to_determine_what_to_send != CHANGE_BITS_32_BIT_ALL_CLEAR )
				{
					// 3/29/2018 Ryan : The OVERHEAD is the GID, an UNS_32 indicating the size of the bitfield,
					// and the 32-bit change bitfield itself.
					lmem_overhead_per_group = (3 * sizeof(UNS_32));

					// 3/29/2018 Ryan : If we still have room available in the message for the overhead, add it
					// now.
					if( *pmem_used_so_far_is_less_than_allocated_memory && ((pmem_used_so_far + rv + lmem_overhead_per_group) < pallocated_memory) )
					{
						PDATA_copy_var_into_pucp( pucp, &lgroup->base.group_identity_number, sizeof(lgroup->base.group_identity_number) );

						PDATA_copy_bitfield_info_into_pucp( pucp, sizeof(CHANGE_BITS_32_BIT), sizeof(UNS_32), &llocation_of_bitfield ); 
					}
					else
					{
						// 3/29/2018 Ryan : Stop processing if we've already filled out the token with the maximum
						// amount of data to send.
						*pmem_used_so_far_is_less_than_allocated_memory = (false);

						// 3/29/2018 Ryan : No overhead added so leave pucp alone.
						
						break;
					}

					lmem_used_by_this_group = lmem_overhead_per_group;

					// ----------

					// Initialize the bit field
					lbitfield_of_changes_to_send = 0x00;

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							WALK_THRU_CHANGE_BITFIELD_description_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->base.description,
							NUMBER_OF_CHARS_IN_A_NAME,
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							WALK_THRU_CHANGE_BITFIELD_station_run_time_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->station_run_time_minutes_10u,
							sizeof(lgroup->station_run_time_minutes_10u),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );
					
					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							WALK_THRU_CHANGE_BITFIELD_delay_before_start_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->delay_before_start_seconds,
							sizeof(lgroup->delay_before_start_seconds),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							WALK_THRU_CHANGE_BITFIELD_box_index_0_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->box_index_0,
							sizeof(lgroup->box_index_0),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							WALK_THRU_CHANGE_BITFIELD_stations_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->stations,
							sizeof(lgroup->stations),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );
							
					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							WALK_THRU_CHANGE_BITFIELD_in_use_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->in_use,
							sizeof(lgroup->in_use),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					// ----------

					// 3/29/2018 Ryan : If we saw any changed bits and ACTUALLY ADDED CONTENT BEYOND THE
					// ORIGINAL OVERHEAD then complete out the message by placing the bitfield in the overhead
					// and updating function housekeeping.
					if( lmem_used_by_this_group > lmem_overhead_per_group )
					{
						// 3/29/2018 Ryan : Bump the count of changed list items in the message. Place the bitfield
						// of changes into the overhead. And update the return value.
						lnum_changed_groups += 1;
						
						memcpy( llocation_of_bitfield, &lbitfield_of_changes_to_send, sizeof(lbitfield_of_changes_to_send) );
						
						rv += lmem_used_by_this_group;
					}
					else
					{
						// 3/29/2018 Ryan : Else back out from the message the overhead already added.
						*pucp -= lmem_overhead_per_group;
					}
					
				}

				lgroup = nm_ListGetNext( &walk_thru_group_list_hdr, lgroup );
			}

			// ----------

			// 3/29/2018 Ryan : If we've added the number of changes but didn't actually add any data
			// because we hit our limit, back out the number of changes.
			if( lnum_changed_groups > 0 )
			{
				memcpy( llocation_of_num_changed_groups, &lnum_changed_groups, sizeof(lnum_changed_groups) );
			}
			else
			{
				// 3/29/2018 Ryan : If we didn't add any content beyond moving the msg pointer over where
				// the number of changed groups goes, then put the pointer back and return a 0 to indicate
				// nothing added.
				*pucp -= sizeof( lnum_changed_groups );

				rv = 0;
			}
		}
		else
		{
			// 3/29/2018 Ryan : We couldn't do anything, meaning couldn't even add in the
			// num_of_changed_sensors. In that case RV is still 0 and pucp hasn't moved so nothing to
			// do here.
		}
	}

	// ----------

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
extern void WALK_THRU_copy_group_into_guivars( const UNS_32 pgroup_index_0 )
{
	WALK_THRU_STRUCT 	*ltempgroup;
	
	UNS_32				i;

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	ltempgroup = WALK_THRU_get_group_at_this_index( pgroup_index_0 );

	nm_GROUP_load_common_guivars( ltempgroup );

	GuiVar_WalkThruRunTime = ltempgroup->station_run_time_minutes_10u;

	GuiVar_WalkThruDelay = ltempgroup->delay_before_start_seconds;
	GuiVar_WalkThruBoxIndex = ltempgroup->box_index_0;
	NETWORK_CONFIG_get_controller_name_str_for_ui( ltempgroup->box_index_0, (char *)&GuiVar_WalkThruControllerName );
	
	for( i = 0; i < MAX_NUMBER_OF_WALK_THRU_STATIONS; i++ )
	{
		WALK_THRU_station_array_for_gui[i] = ltempgroup->stations[i];
	}
	

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );
}

extern void WALK_THRU_load_controller_name_into_scroll_box_guivar( const INT_16 pindex_0_i16 )
{
	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	if( (UNS_32)pindex_0_i16 < COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members() )
	{
		NETWORK_CONFIG_get_controller_name_str_for_ui( pindex_0_i16, (char *)&GuiVar_ComboBoxItemString );
	}
	else if( (UNS_32)pindex_0_i16 == COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members() )
	{
		snprintf( GuiVar_ComboBoxItemString, NUMBER_OF_CHARS_IN_A_NAME, "%s", GuiLib_GetTextPtr( GuiStruct_txtGroupDivider_0, 0 ) );
	}
	else
	{
		snprintf( GuiVar_ComboBoxItemString, NUMBER_OF_CHARS_IN_A_NAME, " <%s>", GuiLib_GetTextPtr( GuiStruct_txtAddWalkThru_0, 0 ) );
	}

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern BOOL_32 WALK_THRU_check_for_station_in_walk_thru( WALK_THRU_STRUCT *pwalkthru, const INT_32 pstation_number )
{
	UNS_32 i;
	
	UNS_32	lstation_current;
	
	BOOL_32	lstation_check = (false);
	
	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );
	
	if( pstation_number != WALK_THRU_SEQUENCE_SLOT_no_station_set )
	{
		for( i = 0; i < MAX_NUMBER_OF_WALK_THRU_STATIONS; i++ )
		{
			lstation_current = WALK_THRU_station_array_for_gui[ i ];
			if( lstation_current == pstation_number )
			{
				lstation_check = (true);
			}
		}
	}
	
	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );
	return(lstation_check);
}

/* ---------------------------------------------------------- */
/**
 * Stores the group name. This routine is called when the user presses BACK to
 * close the on-screen keyboard so the list of groups is updated with the new
 * name.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user presses BACK to close the on-screen keyboard.
 *
 * @author 12/11/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void WALK_THRU_extract_and_store_group_name_from_GuiVars( void )
{
	WALK_THRU_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	lgroup = WALK_THRU_get_group_at_this_index( g_GROUP_list_item_index );

	if( lgroup != NULL )
	{
		SHARED_set_name_32_bit_change_bits( lgroup, GuiVar_GroupName, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, WALK_THRU_get_change_bits_ptr( lgroup, CHANGE_REASON_KEYPAD ), &lgroup->changes_to_upload_to_comm_server, WALK_THRU_CHANGE_BITFIELD_description_bit );
	}

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void WALK_THRU_extract_and_store_changes_from_GuiVars( void )
{
	WALK_THRU_STRUCT	*ltempgroup;
	
	UNS_32				i;

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	// ----------

	ltempgroup = mem_malloc( sizeof(WALK_THRU_STRUCT) );

	// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
	// update just the variable that's potentially changed in the UI.
	memcpy( ltempgroup, WALK_THRU_get_group_with_this_GID( g_GROUP_ID ), sizeof(WALK_THRU_STRUCT) );
	
	// ----------

	strlcpy( ltempgroup->base.description, GuiVar_GroupName, sizeof(ltempgroup->base.description) );

	// Since easyGUI adjusts the time in minutes, multiple the start times by 60
	// to get the time in seconds.
	ltempgroup->station_run_time_minutes_10u = GuiVar_WalkThruRunTime;

	ltempgroup->delay_before_start_seconds = GuiVar_WalkThruDelay;
	ltempgroup->box_index_0 = GuiVar_WalkThruBoxIndex;
	for( i = 0; i < MAX_NUMBER_OF_WALK_THRU_STATIONS; i++ )
	{
		ltempgroup->stations[i] = WALK_THRU_station_array_for_gui[i];
	}

	nm_WALK_THRU_store_changes( ltempgroup, g_GROUP_creating_new, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

	// ----------

	mem_free( ltempgroup );

	// ----------

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );

	// Reset the Creating New flag to ensure we don't accidentally create
	// another new group when using the BACK key.
	g_GROUP_creating_new = (false);
}

/* ---------------------------------------------------------- */
extern void WALK_THRU_load_group_name_into_guivar( const INT_16 pindex_0_i16 )
{
	WALK_THRU_STRUCT *lgroup;
	
	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	if( (UNS_32)pindex_0_i16 < WALK_THRU_get_num_groups_in_use() )
	{
		lgroup = WALK_THRU_get_group_at_this_index( (UNS_32)pindex_0_i16 );

		if( nm_OnList(&walk_thru_group_list_hdr, lgroup) == (true) )
		{
			strlcpy( GuiVar_itmGroupName, nm_GROUP_get_name( lgroup ), NUMBER_OF_CHARS_IN_A_NAME );
		}
		else
		{
			Alert_group_not_found();
		}
	}
	else if( (UNS_32)pindex_0_i16 == WALK_THRU_get_num_groups_in_use() )
	{
		snprintf( GuiVar_itmGroupName, NUMBER_OF_CHARS_IN_A_NAME, "%s", GuiLib_GetTextPtr( GuiStruct_txtGroupDivider_0, 0 ) );
	}
	else
	{
		snprintf( GuiVar_itmGroupName, NUMBER_OF_CHARS_IN_A_NAME, " <%s>", GuiLib_GetTextPtr( GuiStruct_txtAddWalkThru_0, 0 ) );
	}
	
	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void *WALK_THRU_get_group_with_this_GID( const UNS_32 pgroup_ID )
{
	WALK_THRU_STRUCT *lgroup;

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &walk_thru_group_list_hdr, pgroup_ID, (false) );

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );

	return( lgroup );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the group with the specified group ID. This is a wrapper
 * for the nm_GROUP_get_ptr_to_group_at_this_location_in_list function so we
 * don't need to expose the list header.
 * 
 * @mutex Calling function must have the walk_thru_recursive_MUTEX mutex
 *        to ensure the group that's found isn't deleted before it's used.
 *
 * @executed Executed within the context of the display processing task when the
 *  		 list and group screens are drawn; within the key processing task
 *  		 when the user navigates through the group list screen.
 * 
 * @param pindex_0 The index of the group to retrieve - 0 based.
 * 
 * @return void* A pointer to the group with the specified group ID.
 *
 * @author 9/15/2011 Adrianusv
 *
 * @revisions
 *    9/15/2011 Initial release
 */
extern void *WALK_THRU_get_group_at_this_index( const UNS_32 pindex_0 )
{
	WALK_THRU_STRUCT *lgroup;

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	lgroup = nm_GROUP_get_ptr_to_group_at_this_location_in_list( &walk_thru_group_list_hdr, pindex_0 );

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );

	return( lgroup );
}

/* ---------------------------------------------------------- */
/**
 * Accepts a group ID (e.g., GuiVar_GroupIndex) and returns the position of that
 * index in the associated list. This is used to ensure the group the user
 * created or was editing is higlighted when they return to the list of groups.
 * 
 * @mutex Calling function must have the walk_thru_recursive_MUTEX mutex
 *  	  to ensure the list doesn't change between locating the index and using
 *  	  it.
 *
 * @executed Executed within the context of the key processing task when the
 *           user selects a group on the group list screen.
 * 
 * @param pgroup_ID The group ID
 * 
 * @return UNS_32 A 0-based index of the requested group into the associated
 *                list.
 *
 * @author 12/15/2011 Adrianusv
 *
 * @revisions
 *    12/15/2011 Initial release
 */
extern UNS_32 WALK_THRU_get_index_for_group_with_this_GID( const UNS_32 pgroup_ID )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	rv = nm_GROUP_get_index_for_group_with_this_GID( &walk_thru_group_list_hdr, pgroup_ID );

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the last Manual Programs group ID by retrieving the
 * number_of_groups_ever_created value from the head of the list.
 * 
 * @mutex Calling function must have the walk_thru_recursive_MUTEX mutex
 *  	  to ensure another group isn't added to the list between the number
 *  	  being retrieved and being used.
 *
 * @executed Executed within the context of the startup task when the stations 
 *  		 list is created for the first time; also executed within the
 *  		 display processing and key processing task as the user enters and
 *  		 leaves the editing screen.
 * 
 * @return UNS_32 The group ID of the last Manual Programs group.
 *
 * @author 9/7/2011 Adrianusv
 *
 * @revisions
 *    9/7/2011   Initial release
 *    2/8/2012	 Renamed to include nm_ prefix
 */
 /*
extern UNS_32 WALK_THRU_get_last_group_ID( void )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	rv = ((GROUP_BASE_DEFINITION_STRUCT*)walk_thru_group_list_hdr.phead)->number_of_groups_ever_created;

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );

	return( rv );
}
*/
/* ---------------------------------------------------------- */
extern UNS_32 WALK_THRU_get_num_groups_in_use( void )
{
	WALK_THRU_STRUCT *lgroup;

	UNS_32	rv;

	// ----------

	rv = 0;

	// ----------

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	lgroup = nm_ListGetFirst( &walk_thru_group_list_hdr );

	while( lgroup != NULL )
	{
		if( lgroup->in_use )
		{
			rv++;
		}

		// ----------

		lgroup = nm_ListGetNext( &walk_thru_group_list_hdr, lgroup );
	}

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
extern CHANGE_BITS_32_BIT *WALK_THRU_get_change_bits_ptr( WALK_THRU_STRUCT *pgroup, const UNS_32 pchange_reason )
{
	return( SHARED_get_32_bit_change_bits_ptr( pchange_reason, &pgroup->changes_to_send_to_master, &pgroup->changes_to_distribute_to_slaves, &pgroup->changes_to_upload_to_comm_server ) );
}

/* ---------------------------------------------------------- */
extern UNS_32 WALK_THRU_get_station_run_time( WALK_THRU_STRUCT *const lgroup )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( lgroup,
													 &lgroup->station_run_time_minutes_10u,
													 WALK_THRU_STATION_RUN_TIME_MIN,
													 WALK_THRU_STATION_RUN_TIME_MAX,
													 WALK_THRU_STATION_RUN_TIME_DEFAULT,
													 &nm_WALK_THRU_set_station_run_time,
													 &lgroup->changes_to_send_to_master,
													 WALK_THRU_database_field_names[ WALK_THRU_CHANGE_BITFIELD_station_run_time_bit ] );

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 WALK_THRU_get_delay_before_start( WALK_THRU_STRUCT *const lgroup )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( lgroup,
													 &lgroup->delay_before_start_seconds,
													 WALK_THRU_DELAY_BEFORE_START_MIN,
													 WALK_THRU_DELAY_BEFORE_START_MAX,
													 WALK_THRU_DELAY_BEFORE_START_DEFAULT,
													 &nm_WALK_THRU_set_delay_before_start,
													 &lgroup->changes_to_send_to_master,
													 WALK_THRU_database_field_names[ WALK_THRU_CHANGE_BITFIELD_delay_before_start_bit ] );

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 WALK_THRU_get_box_index( WALK_THRU_STRUCT *const lgroup )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( lgroup,
													 &lgroup->box_index_0,
													 WALK_THRU_BOX_INDEX_MIN,
													 WALK_THRU_BOX_INDEX_MAX,
													 WALK_THRU_BOX_INDEX_DEFAULT,
													 &nm_WALK_THRU_set_box_index_0,
													 &lgroup->changes_to_send_to_master,
													 WALK_THRU_database_field_names[ WALK_THRU_CHANGE_BITFIELD_box_index_0_bit ] );

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
// 2/8/2018 Ryan : work in progress on this function.
extern INT_32 WALK_THRU_get_stations( WALK_THRU_STRUCT *const lgroup, const UNS_32 pslot_index_0 )
{
	char 	lfield_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32	rv;

	if( pslot_index_0 < MAX_NUMBER_OF_WALK_THRU_STATIONS)
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", WALK_THRU_database_field_names[ WALK_THRU_CHANGE_BITFIELD_stations_bit ], (pslot_index_0 + 1) );

		rv = SHARED_get_int32_from_array_32_bit_change_bits_group( 	lgroup,
																	pslot_index_0,
																	&lgroup->stations[ pslot_index_0 ],
																	MAX_NUMBER_OF_WALK_THRU_STATIONS,
																	WALK_THRU_SEQUENCE_SLOT_station_number_MIN,
																	WALK_THRU_SEQUENCE_SLOT_station_number_MAX,
																	WALK_THRU_SEQUENCE_SLOT_station_number_DEFAULT,
																	&nm_WALK_THRU_set_stations,
																	&lgroup->changes_to_send_to_master,
																	lfield_name );
	}
	else
	{
		Alert_index_out_of_range();
		
		rv = WALK_THRU_SEQUENCE_SLOT_station_number_DEFAULT;
	}

	return ( rv );
}

/* ---------------------------------------------------------- */
extern void WALK_THRU_clean_house_processing( void )
{
	// 4/16/2014 rmd : Executed within the context of the comm_mngr task. ONLY BY SLAVES. THE
	// CHANGES MADE HERE DO NOT BE DISTRIBUTED. THE STATION LIST IS BEING WIPED IN PREPARATION
	// OF BEING ADDED AS A NEW MEMBER TO A CHAIN. Called when the result of a scan indicates
	// this controller is a new slave in the chain. Used when building up chains. Likely ONLY
	// used when assembling a chain for the first time or when increasing the length of a chain
	// to a length it has never 'run' at before. Or if you changed the LETTER of a slave in a
	// chain.
	
	void	*tptr;
	
	// ----------

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	tptr = nm_ListRemoveHead( &walk_thru_group_list_hdr );

	while( tptr != NULL )
	{
		mem_free( tptr );

		tptr = nm_ListRemoveHead( &walk_thru_group_list_hdr );
	}

	// ----------

	// 4/16/2014 rmd : By setting the no groups exist flag to false no change bits will be set
	// for this 'dummy' group. And that is what we want.
	nm_GROUP_create_new_group( &walk_thru_group_list_hdr, (char*)WALK_THRU_DEFAULT_NAME, &nm_WALK_THRU_set_default_values, sizeof(WALK_THRU_STRUCT), (false), NULL );

	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE, WALK_THRU_FILENAME, WALK_THRU_LATEST_FILE_REVISION, &walk_thru_group_list_hdr, sizeof( WALK_THRU_STRUCT ), walk_thru_recursive_MUTEX, FF_WALK_THRU );

	// ----------

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
extern void WALK_THRU_set_bits_on_all_groups_to_cause_distribution_in_the_next_token( void )
{
	WALK_THRU_STRUCT	*lgroup;

	// ----------

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lgroup = nm_ListGetFirst( &walk_thru_group_list_hdr );

	while( lgroup != NULL )
	{
		// 5/1/2014 rmd : This function ONLY being called at the conclusion of a SCAN by the master
		// of a real multiple controller chain - we want to broadcast to the slaves using tokens
		// about all the hardware we presently know about.
		SHARED_set_all_32_bit_change_bits( &lgroup->changes_to_distribute_to_slaves, walk_thru_recursive_MUTEX );

		lgroup = nm_ListGetNext( &walk_thru_group_list_hdr, lgroup );
	}

	// ----------

	// 4/29/2014 ajv : Set a flag in the changes structure to notify this
	// controller to examine its lists for changes to distribute to the other
	// controllers in the chain in it's next token resposne. The flag is part of
	// the mutex protected comm_mngr struct so take the MUTEX.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	comm_mngr.changes.distribute_changes_to_slave = (true);

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );

	// ----------

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void WALK_THRU_on_all_groups_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear )
{
	WALK_THRU_STRUCT	*lgroup_ptr;

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	lgroup_ptr = nm_ListGetFirst( &walk_thru_group_list_hdr );

	while( lgroup_ptr != NULL )
	{
		if( pset_or_clear == COMMSERVER_CHANGE_BITS_ALL_CLEARED )
		{
			SHARED_clear_all_32_bit_change_bits( &lgroup_ptr->changes_to_upload_to_comm_server, walk_thru_recursive_MUTEX );
		}
		else
		{
			SHARED_set_all_32_bit_change_bits( &lgroup_ptr->changes_to_upload_to_comm_server, walk_thru_recursive_MUTEX );
		}

		lgroup_ptr = nm_ListGetNext( &walk_thru_group_list_hdr, lgroup_ptr );
	}

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * @mutex_requirements Requires the caller to have the pending_changes_recursive_MUTEX to
 * ensure no other function tries to modify the pending_changes_to_send_to_comm_server flag.
 *
 * @task_execution Executed within the context of the CONTROLLER INITIATED task when it
 * first starts and when the waiting response flags are cleared after a communication.
 */
extern void nm_WALK_THRU_update_pending_change_bits( const UNS_32 pcomm_error_occurred )
{
	WALK_THRU_STRUCT	*lprogram;

	BOOL_32	lfile_save_necessary;

	lfile_save_necessary = (false);

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	lprogram = nm_ListGetFirst( &walk_thru_group_list_hdr );

	while( lprogram != NULL )
	{
		// 4/30/2015 ajv : No reason to process this if none of the bits are set. This also helps
		// reduce the file save activity.
		if( lprogram->changes_uploaded_to_comm_server_awaiting_ACK != CHANGE_BITS_32_BIT_ALL_CLEAR )
		{
			// 4/29/2015 ajv : If a communication error occurred while sending Program Data, copy the
			// bits of variables that were sent back into the original bit field to ensure they're sent
			// again during the next attempt. Otherwise, clear the pending bits.
			if( pcomm_error_occurred )
			{
				lprogram->changes_to_upload_to_comm_server |= lprogram->changes_uploaded_to_comm_server_awaiting_ACK;

				// 4/30/2015 ajv : Since we've determined there were changes that were not successfully
				// sent, set the flag to trigger another attempt
				weather_preserves.pending_changes_to_send_to_comm_server = (true);

				// 4/30/2015 ajv : Also set the timer to force the data to send. If the timer's already
				// running, this will simply restart it.
				// 
				// 4/30/2015 ajv : BECAUSE of the potential that this function is executed many times (once
				// for each item changed) we could not start our pdata timer with a post to the CI queue.
				// The queue would overflow. So start the timer directly.
				xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGES_NOT_SENT_MS ), portMAX_DELAY );
			}
			else
			{
				SHARED_clear_all_32_bit_change_bits( &lprogram->changes_uploaded_to_comm_server_awaiting_ACK, walk_thru_recursive_MUTEX );
			}

			// 4/30/2015 ajv : At least one manual program was updated. So make sure we save the file.
			lfile_save_necessary = (true);
		}

		lprogram = nm_ListGetNext( &walk_thru_group_list_hdr, lprogram );
	}

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );

	if( lfile_save_necessary )
	{
		// 4/29/2015 ajv : Now that the bits are set or cleared accordingly, trigger a file save.
		// If power fails between building the message and receiving the ACK, the original bit field
		// hasn't been saved to flash yet, so it will resend the associated variables during the
		// next attempt.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_WALK_THRU, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
	}
}

/* ---------------------------------------------------------- */
static BOOL_32 there_are_no_pending_walk_thru_changes_to_send_to_the_master( UNS_32 pwalk_thru_gid )
{
	BOOL_32		rv;
	
	WALK_THRU_STRUCT	*lgroup;
	
	// ----------
	
	// 3/21/2018 rmd : Default to true so we proceed and try to start the walk thru even if
	// something goes wrong. Otherwise we're sort of stuck on the screen.
	rv = (true);
	
	// ----------
	
	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	lgroup = WALK_THRU_get_group_with_this_GID( pwalk_thru_gid );

	if( lgroup )
	{
		rv = (lgroup->changes_to_send_to_master == CHANGE_BITS_32_BIT_ALL_CLEAR);
	}

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 WALK_THRU_load_walk_thru_gid_for_token_response( UNS_8 **pwalk_thru_ptr )
{
	UNS_32	rv;

	UNS_8	*ucp;
	
	// ----------
	
	rv = 0;
	
	*pwalk_thru_ptr = NULL;
	
	// ----------
	
	// 3/21/2018 rmd : There is a bit of a complication. If the user was editing the walk thru
	// screen, and then hit the START button, the countdown could expire BEFORE the changes make
	// it to the master. If that happened, and for some reason the changes would not FIT INTO
	// THE FIRST SUBSEQUENT TOKEN RESPONSE, we could possibly signal the master to start the
	// sequence, as it stands without the users latest edits in place. To prevent that wait
	// until there are no change bits set.
	if( irri_comm.walk_thru_need_to_send_gid_to_master && there_are_no_pending_walk_thru_changes_to_send_to_the_master( irri_comm.walk_thru_gid_to_send ) )
	{
		// 3/21/2018 rmd : CLEAR the flag!
		irri_comm.walk_thru_need_to_send_gid_to_master = (false);
		
		// ----------
		
		// 3/21/2018 rmd : We load the token response with only the GID!
		ucp = mem_malloc( sizeof(UNS_32) );

		*pwalk_thru_ptr = ucp;

		memcpy( ucp, &irri_comm.walk_thru_gid_to_send, sizeof(UNS_32) );
		rv += sizeof(UNS_32);
	}

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 WALK_THRU_load_kick_off_struct( UNS_32 pwalk_thru_gid, WALK_THRU_KICK_OFF_STRUCT *pwalk_thru_ptr )
{
	BOOL_32	rv;
	
	WALK_THRU_STRUCT	*lgroup;
	
	// ----------
	
	// 3/22/2018 rmd : Assume we can't find the group.
	rv = (false);
	
	// ----------

	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );

	lgroup = WALK_THRU_get_group_with_this_GID( pwalk_thru_gid );

	if( lgroup )
	{
		rv = (true);
		
		// ----------
		
		// 3/22/2018 rmd : Load the KICK OFF struct.
		pwalk_thru_ptr->box_index_0 = lgroup->box_index_0;

		// 3/22/2018 rmd : Convert to seconds.
		pwalk_thru_ptr->run_time_seconds = (lgroup->station_run_time_minutes_10u * 6);
		
		memcpy( pwalk_thru_ptr->stations, lgroup->stations, sizeof(pwalk_thru_ptr->stations) );
		
		// 10/16/2018 Ryan : nm_GROUP_get_name is protected by walk thru mutex.
		strlcpy( pwalk_thru_ptr->group_name, nm_GROUP_get_name( lgroup ), sizeof( pwalk_thru_ptr->group_name ) );
	}
	else
	{
		// 3/22/2018 rmd : This should NEVER happen and deserves an alert line.
		Alert_Message_va( "WALK THRU : could not find %u group", pwalk_thru_gid );
	}

	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
BOOL_32 WALK_THRU_calculate_chain_sync_crc( UNS_32 const pff_name )
{
	// 8/30/2018 rmd : Executed in the context of the comm_mngr task when token responses are
	// being made. And that's it.

	// ----------

	WALK_THRU_STRUCT	*lwalk_thru_ptr;
	
	UNS_8				*checksum_start;

	UNS_32				checksum_length;

	UNS_8				*ucp;
	
	BOOL_32				rv;

	// ----------

	rv = (false);

	// ----------
	
	xSemaphoreTakeRecursive( walk_thru_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	// 8/30/2018 rmd : So the CRC calculation function wants a continuous memory space and a
	// length to calculate the crc off. In this function we will pick out the desired data, and
	// simply pile it up on itself, list item by list item. We don't care how messy the pile is
	// because we never attempt to reference any data once it is added to the pile. The data is
	// only there to include in the checksum.
	//
	// 8/30/2018 rmd : Well we know it can't be bigger than the list item count times the size
	// of each list item, so start with a memory block that big.
	if( mem_obtain_a_block_if_available( (walk_thru_group_list_hdr.count * sizeof(WALK_THRU_STRUCT)), (void**)&checksum_start ) )
	{
		// 9/5/2018 rmd : Well then the calculation will be successful. Nothing else can prevent
		// that outcome.
		rv = (true);
		
		// ----------
		
		ucp = checksum_start;
		
		checksum_length = 0;
		
		// ----------
		
		// 8/30/2018 rmd : Rattle though the list and push the fields we want to include in the sync
		// test onto the block to be checksummed.
		lwalk_thru_ptr = nm_ListGetFirst( &walk_thru_group_list_hdr );
		
		while( lwalk_thru_ptr != NULL )
		{
			// 9/4/2018 rmd : Make sure you include only TRUE USER SETTINGS, no flags, or pointers to
			// memory, etc...
			
			// ----------
			
			// 11/2/2018 rmd : Due to the inconsistent way the group names strings are handled during
			// the sync (we do not regligiously take all 48 characters and always carry them through the
			// sync process, there are places we only copy up until the null), the part of the string
			// following the null char is not guaranteed to be in sync. So only include up until the
			// null in the crc test.
			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lwalk_thru_ptr->base.description, strlen(lwalk_thru_ptr->base.description) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lwalk_thru_ptr->station_run_time_minutes_10u, sizeof(lwalk_thru_ptr->station_run_time_minutes_10u) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lwalk_thru_ptr->delay_before_start_seconds, sizeof(lwalk_thru_ptr->delay_before_start_seconds) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lwalk_thru_ptr->box_index_0, sizeof(lwalk_thru_ptr->box_index_0) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lwalk_thru_ptr->stations, sizeof(lwalk_thru_ptr->stations) );

			// skipping the 4 changes bit fields
			
			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lwalk_thru_ptr->in_use, sizeof(lwalk_thru_ptr->in_use) );

			// END
			
			// ----------
		
			lwalk_thru_ptr = nm_ListGetNext( &walk_thru_group_list_hdr, lwalk_thru_ptr );
		}
		
		// ----------
		
		// 8/30/2018 rmd : Now calculate the checksum.
		cscs.the_crc[ pff_name ] = CRC_calculate_32bit_big_endian( checksum_start, checksum_length );
		
		// ----------
		
		// 9/4/2018 rmd : We're done with the block, so of course free it.
		mem_free( checksum_start );
	}
	else
	{
		// 9/5/2018 rmd : So by design it's okay if there is no memory. The checksum won't be set
		// valid, and the next time we try to include it in a token response we'll try to calculate
		// it again. Self protecting and retrying. But let's alert about this so we know it's taking
		// place.
		Alert_Message_va( "SYNC: no mem to calc checksum %s", chain_sync_file_pertinants[ pff_name ].file_name_string );
	}
	
	// ----------
	
	xSemaphoreGiveRecursive( walk_thru_recursive_MUTEX );
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
