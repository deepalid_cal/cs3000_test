/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_STATIONS_H
#define _INC_STATIONS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"cal_list.h"

#include	"pdata_changes.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define STATION_DEFAULT_DESCRIPTION				"(description not yet set)"

// 7/7/2014 rmd : We need the physically available default to be (false). For the "phantom"
// station that is created when the file is initially created. This station is created to
// avoid empty list errors. And is actually station 1 on the A controller. But not
// physically available to begin with.
#define STATION_PHYSICALLY_AVAILABLE_DEFAULT	(false)

#define STATION_INUSE_DEFAULT					(true)

#define BOX_INDEX_MINIMUM						(0)
#define BOX_INDEX_MAXIMUM						(MAX_CHAIN_LENGTH-1)
#define BOX_INDEX_DEFAULT						(BOX_INDEX_MINIMUM)

#define STATION_TOTAL_RUN_MINUTES_MIN_10u		(0)
#define STATION_TOTAL_RUN_MINUTES_MAX_10u		(9999)
#define STATION_TOTAL_RUN_MINUTES_DEFAULT_10u	(10)

#define STATION_CYCLE_MINUTES_MIN_10u			(1)		//  0.1 mins
#define STATION_CYCLE_MINUTES_MAX_10u			(24000)	// 40.0 hours - the maximum cycle time possible with WaterSense calculation (sandy loam @ 0-3% slope)
#define STATION_CYCLE_MINUTES_DEFAULT_10u		(50)	//  5.0 mins

#define STATION_SOAK_MINUTES_MIN				(5)
#define STATION_SOAK_MINUTES_MAX   		 		(720)
#define STATION_SOAK_MINUTES_DEFAULT			(20)

#define STATION_ET_FACTOR_MIN					(0)		// -100%
#define STATION_ET_FACTOR_MAX					(400)	// +300%
#define STATION_ET_FACTOR_DEFAULT				(100)	// 0%

#define STATION_EXPECTED_FLOW_RATE_MIN			(1)
#define STATION_EXPECTED_FLOW_RATE_MAX			(MAXIMUM_SYSTEM_CAPACITY)
#define STATION_EXPECTED_FLOW_RATE_DEFAULT		(1)

#define	STATION_ALERT_DEFAULT					(0)

// 4/8/2016 rmd : The MINIMUM DU that Joe Perez, AJ, and myself agreed to should be 40%.
// This had a minimum of 0%. As the DU becomes very low the run time ramps through the roof.
// And the practicality of a DU below 40% is suspect. So for now we are taking the liberty
// to limit this to no less than 40%.
#define STATION_DU_MIN							(40)
#define STATION_DU_MAX							(100)
#define STATION_DU_DEFAULT						(70)

#define STATION_NO_WATER_DAYS_MIN				(0)
#define STATION_NO_WATER_DAYS_MAX				(31)
#define STATION_NO_WATER_DAYS_DEFAULT			(0)

#define STATION_MAX_WATER_DAYS_MIN				(0)
#define STATION_MAX_WATER_DAYS_MAX				(31)
#define STATION_MAX_WATER_DAYS_DEFAULT			(0)

#define STATION_MANUAL_PROGRAM_SECONDS_MIN		(0)
#define STATION_MANUAL_PROGRAM_SECONDS_MAX		(8*60*60)	// 8 hours max MP time ... rmd just picked it!
#define STATION_MANUAL_PROGRAM_SECONDS_DEFAULT	(10*60)		// 10 minutes default MP time ... at Richard's request

// 11/3/2015 ajv : The minimum MB value is simply here to provide a bottom-end for range
// checking. We started with -100%, but we discovered Ralph's controller - which has a small
// RZWWS and only waters a few times a week - hit that pretty quickly. So we're going to
// increase it substantially for now so we can learn where a reasonable minimum is.
#define	STATION_MOISTURE_BALANCE_MIN			(-100.00F)	// -10,000%
#define	STATION_MOISTURE_BALANCE_MAX			(1.0F)		// 100%
#define	STATION_MOISTURE_BALANCE_DEFAULT		(0.50F)		// 50%


#define	STATION_SQUARE_FOOTAGE_MIN				(1)
// 12/14/2016 rmd : In Washington DC a project with Dave Rippe ended up with station square
// feet approaching 80000 sq ft. So make this maximum 99999. Stopping before 100000 so we
// don't add another digit to the screen.
#define	STATION_SQUARE_FOOTAGE_MAX				(99999)
#define	STATION_SQUARE_FOOTAGE_DEFAULT			(1000)

#define	STATION_MOISTURE_BALANCE_SET_IGNORE_FLAG	(true)
#define	STATION_MOISTURE_BALANCE_CLEAR_IGNORE_FLAG	(false)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern MIST_LIST_HDR_TYPE	station_info_list_hdr;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct STATION_STRUCT STATION_STRUCT;

// 12/23/2015 skc : It is perhaps silly to constantly be calling accessor
// methods wrapped in Mutexes for every call to read a structure element.
// This structure is intended to alleviate this, by making a copy of certain values.
typedef struct
{
	UNS_32 box_index_0;
	UNS_32 station_number_0;
	BOOL_32 physically_available;
	BOOL_32 in_use_bool;
	UNS_32 total_run_minutes_10u;
	UNS_32 uses_et;
	UNS_32 et_factor_100u;
	UNS_32 uniformity;
	UNS_32 percent_adjust;
	UNS_32 expected_flow_rate_gpm;
	float  precip_rate;
	UNS_32 no_water_days;
	UNS_32 GID_station_group;
	UNS_32 GID_irrigation_system;
	UNS_32 GID_manual_program_A;
	UNS_32 GID_manual_program_B;


} STATION_STRUCT_for_budgets;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_file_station_info( void );

extern void save_file_station_info( void );

// ----------

extern void nm_STATION_store_changes( STATION_STRUCT *const ptemporary_station,
									  const BOOL_32 pstation_created,
									  const UNS_32 preason_for_change,
									  const UNS_32 pbox_index_0,
									  const BOOL_32 pset_change_bits,
									  const UNS_32 pchanges_received_from,
									  CHANGE_BITS_32_BIT lbitfield_of_changes
									  #ifdef _MSC_VER
									  , char *pSQL_update_str,
									  char *pSQL_insert_fields_str,
									  char *pSQL_insert_values_str
									  #endif
									);

extern void *nm_STATION_create_new_station( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, const UNS_32 preason_data_is_being_built );

// ----------

extern UNS_32 STATION_build_data_to_send( UNS_8 **pucp,
										  const UNS_32 pmem_used_so_far,
										  BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
										  const UNS_32 preason_data_is_being_built,
										  const UNS_32 pallocated_memory );

extern UNS_32 nm_STATION_extract_and_store_changes_from_comm( const unsigned char *pucp,
															  const UNS_32 preason_for_change,
															  const BOOL_32 pset_change_bits,
															  const UNS_32 pchanges_received_from
															  #ifdef _MSC_VER
															  , char *pSQL_statements,
															  char *pSQL_search_condition_str,
															  char *pSQL_update_str,
															  char *pSQL_insert_fields_str,
															  char *pSQL_insert_values_str,
															  char *pSQL_single_merge_statement_buf,
															  const UNS_32 pnetwork_ID
															  #endif
															);

// ----------

#ifdef _MSC_VER

extern INT_32 STATION_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order );

#endif

// ----------

extern float nm_STATION_calculate_and_copy_estimated_minutes_into_GuiVar( const STATION_STRUCT *const pstation );

extern void STATION_find_first_available_station_and_init_station_number_GuiVars( void );

extern void nm_STATION_copy_station_into_guivars( STATION_STRUCT *const pstation );

extern void STATION_copy_group_name_into_copy_station_GuiVar_and_set_offsets( const char *const pgroup_name );

extern void STATION_extract_and_store_changes_from_GuiVars( void );

extern void STATION_store_stations_in_use( const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const UNS_32 pchanges_received_from );

// ----------

extern BOOL_32 STATION_this_group_has_no_stations_assigned_to_it( const UNS_32 pgroup_ID, UNS_32 (*pGetIDFunction)(const void *pstation) );

extern void STATION_adjust_group_settings_after_changing_group_assignment( STATION_STRUCT *const pstation, const UNS_32 pold_group_ID, const UNS_32 pnew_group_ID, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, const UNS_32 preason_for_change );

extern void STATION_store_group_assignment_changes( UNS_32 (*pget_group_ID_func_ptr)( void *const pstation ), void (*pset_group_ID_func_ptr)( void *const pstation, const UNS_32 pValue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set ), const UNS_32 pgroup_ID, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, const UNS_32 pchanges_received_from );

extern void STATION_store_manual_programs_assignment_changes( const UNS_32 pgroup_ID, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, const UNS_32 pchanges_received_from );

// ----------

extern UNS_32 STATION_get_index_using_ptr_to_station_struct( const STATION_STRUCT *const pstation );

extern UNS_32 STATION_get_num_of_stations_physically_available( void );

extern UNS_32 STATION_get_num_stations_in_use( void );

extern UNS_32 STATION_get_num_stations_assigned_to_groups( void );

extern STATION_STRUCT *STATION_get_first_available_station( void );

extern UNS_32 STATION_get_unique_box_index_count( void );

// ----------

extern BOOL_32 STATION_station_is_available_for_use( void *const pstation );
extern BOOL_32 nm_STATION_get_physically_available( STATION_STRUCT *const pstation );
extern UNS_32 nm_STATION_get_station_number_0( STATION_STRUCT *const pstation );
extern UNS_32 nm_STATION_get_box_index_0( STATION_STRUCT *const pstation );
extern UNS_32 nm_STATION_get_decoder_serial_number( STATION_STRUCT *const pstation );
extern UNS_32 nm_STATION_get_decoder_output( STATION_STRUCT *const pstation );
extern UNS_32 STATION_get_total_run_minutes_10u( STATION_STRUCT *const pstation );
extern UNS_32 nm_STATION_get_cycle_minutes_10u( STATION_STRUCT *const pstation );
extern UNS_32 nm_STATION_get_watersense_cycle_max_10u( STATION_STRUCT *const pstation );
extern UNS_32 nm_STATION_get_soak_minutes( STATION_STRUCT *const pstation );
extern UNS_32 nm_STATION_get_watersense_soak_min( STATION_STRUCT *const pstation );
extern UNS_32 STATION_get_ET_factor_100u( STATION_STRUCT *const pstation );
extern UNS_32 nm_STATION_get_expected_flow_rate_gpm( STATION_STRUCT *const pstation );
extern UNS_32 nm_STATION_get_distribution_uniformity_100u( STATION_STRUCT *const pstation );

extern UNS_32 STATION_get_no_water_days( STATION_STRUCT *const pstation );
extern UNS_32 STATION_decrement_no_water_days_and_return_value_after_decrement( STATION_STRUCT *const pstation );
extern void STATION_chain_down_NOW_days_midnight_maintenance( void );

extern char *nm_STATION_get_description( STATION_STRUCT *const pstation );

extern UNS_32 nm_STATION_get_number_of_manual_programs_station_is_assigned_to( STATION_STRUCT *const pstation );

extern UNS_32 STATION_get_GID_station_group( void *const pstation );

extern UNS_32 STATION_get_GID_manual_program_A( void *const pstation );
extern UNS_32 STATION_get_GID_manual_program_B( void *const pstation );

extern BOOL_32 STATION_does_this_station_belong_to_this_manual_program( STATION_STRUCT *const pstation, UNS_32 pmanual_program_gid );

extern float STATION_get_moisture_balance_percent( STATION_STRUCT *const pstation );
extern float STATION_get_moisture_balance( STATION_STRUCT *const pstation );

extern UNS_32 STATION_get_square_footage( STATION_STRUCT *const pstation );

extern BOOL_32 nm_STATION_get_ignore_moisture_balance_at_next_irrigation_flag( STATION_STRUCT *const pstation );

// ----------

extern void STATION_set_ignore_moisture_balance_at_next_irrigation_all_stations( const UNS_32 preason_for_change );

extern void STATION_set_ignore_moisture_balance_at_next_irrigation_by_group( const UNS_32 pgroup_ID, const UNS_32 preason_for_change );

extern void STATION_set_ignore_moisture_balance_at_next_irrigation_by_station( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, const UNS_32 preason_for_change );

// ----------

extern void STATION_set_no_water_days_for_all_stations( const UNS_32 pnow_days, const UNS_32 preason_for_change );

extern void STATION_set_no_water_days_by_group(const UNS_32 pnow_days, const UNS_32 pgroup_ID, const UNS_32 preason_for_change);

extern void STATION_set_no_water_days_by_box( const UNS_32 pnow_days, const UNS_32 pbox_index_0, const UNS_32 preason_for_change );

extern void STATION_set_no_water_days_by_station( const UNS_32 pnow_days, const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, const UNS_32 preason_for_change );

// ----------

extern CHANGE_BITS_32_BIT *STATION_get_change_bits_ptr( STATION_STRUCT *pstation, const UNS_32 pchange_reason );

// ----------

extern void nm_STATION_set_physically_available( void *const pstation,
												 BOOL_32 pphysically_available,
												 const BOOL_32 pgenerate_change_line_bool,
												 const UNS_32 preason_for_change,
												 const UNS_32 pbox_index_0,
												 const BOOL_32 pset_change_bits,
												 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												 #ifdef _MSC_VER
												 , char *pmerge_update_str,
												 char *pmerge_insert_fields_str,
												 char *pmerge_insert_values_str
												 #endif
												);

extern void nm_STATION_set_in_use( void *const pstation,
								   BOOL_32 pin_use_bool,
								   const BOOL_32 pgenerate_change_line_bool,
								   const UNS_32 preason_for_change,
								   const UNS_32 pbox_index_0,
								   const BOOL_32 pset_change_bits,
								   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
								   #ifdef _MSC_VER
								   , char *pmerge_update_str,
								   char *pmerge_insert_fields_str,
								   char *pmerge_insert_values_str
								   #endif
								  );

extern void nm_STATION_set_decoder_serial_number( void *const pstation,
												  UNS_32 pdecoder_serial_number,
												  const BOOL_32 pgenerate_change_line_bool,
												  const UNS_32 preason_for_change,
												  const UNS_32 pbox_index_0,
												  const BOOL_32 pset_change_bits,
												  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , char *pmerge_update_str,
												  char *pmerge_insert_fields_str,
												  char *pmerge_insert_values_str
												  #endif
												);

extern void nm_STATION_set_decoder_output( void *const pstation,
										   UNS_32 pdecoder_output,
										   const BOOL_32 pgenerate_change_line_bool,
										   const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const BOOL_32 pset_change_bits,
										   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , char *pmerge_update_str,
										   char *pmerge_insert_fields_str,
										   char *pmerge_insert_values_str
										   #endif
										 );

extern void nm_STATION_set_total_run_minutes( void *const pstation,
											  UNS_32 ptotal_run_minutes_10u,
											  const BOOL_32 pgenerate_change_line_bool,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
											);

extern void nm_STATION_set_cycle_minutes( void *const pstation,
										  UNS_32 pcycle_time_minutes_10u,
										  const BOOL_32 pgenerate_change_line_bool,
										  const UNS_32 preason_for_change,
										  const UNS_32 pbox_index_0,
										  const BOOL_32 pset_change_bits,
										  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										  #ifdef _MSC_VER
										  , char *pmerge_update_str,
										  char *pmerge_insert_fields_str,
										  char *pmerge_insert_values_str
										  #endif
										);

extern void nm_STATION_set_soak_minutes( void *const pstation,
										 UNS_32 psoak_minutes,
										 const BOOL_32 pgenerate_change_line_bool,
										 const UNS_32 preason_for_change,
										 const UNS_32 pbox_index_0,
										 const BOOL_32 pset_change_bits,
										 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										 #ifdef _MSC_VER
										 , char *pmerge_update_str,
										 char *pmerge_insert_fields_str,
										 char *pmerge_insert_values_str
										 #endif
									   );

extern void nm_STATION_set_expected_flow_rate( void *const pstation,
											   UNS_32 pexpected_flow_rate_gpm,
											   const BOOL_32 pgenerate_change_line_bool,
											   const UNS_32 preason_for_change,
											   const UNS_32 pbox_index_0,
											   const BOOL_32 pset_change_bits,
											   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											   #ifdef _MSC_VER
											   , char *pmerge_update_str,
											   char *pmerge_insert_fields_str,
											   char *pmerge_insert_values_str
											   #endif
											 );

extern void nm_STATION_set_no_water_days( void *const pstation,
										  UNS_32 pno_water_days,
										  const BOOL_32 pgenerate_change_line_bool,
										  const UNS_32 preason_for_change,
										  const UNS_32 pbox_index_0,
										  const BOOL_32 pset_change_bits,
										  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										  #ifdef _MSC_VER
										  , char *pmerge_update_str,
										  char *pmerge_insert_fields_str,
										  char *pmerge_insert_values_str
										  #endif
										);

extern void nm_STATION_set_GID_station_group( void *const pstation,
											  UNS_32 pGID_station_group,
											  const BOOL_32 pgenerate_change_line_bool,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
											);

extern void nm_STATION_set_GID_manual_program_A( void *const pstation,
												 UNS_32 pGID_manual_program_A,
												 const BOOL_32 pgenerate_change_line_bool,
												 const UNS_32 preason_for_change,
												 const UNS_32 pbox_index_0,
												 const BOOL_32 pset_change_bits,
												 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												 #ifdef _MSC_VER
												 , char *pmerge_update_str,
												 char *pmerge_insert_fields_str,
												 char *pmerge_insert_values_str
												 #endif
											   );

extern void nm_STATION_set_GID_manual_program_B( void *const pstation,
												 UNS_32 pGID_manual_program_B,
												 const BOOL_32 pgenerate_change_line_bool,
												 const UNS_32 preason_for_change,
												 const UNS_32 pbox_index_0,
												 const BOOL_32 pset_change_bits,
												 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												 #ifdef _MSC_VER
												 , char *pmerge_update_str,
												 char *pmerge_insert_fields_str,
												 char *pmerge_insert_values_str
												 #endif
											   );

extern void nm_STATION_set_moisture_balance_percent( void *const pstation,
													 float pmoisture_balance_percent,
													 const BOOL_32 pgenerate_change_line_bool,
													 const UNS_32 preason_for_change,
													 const UNS_32 pbox_index_0,
													 const BOOL_32 pset_change_bits,
													 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													 #ifdef _MSC_VER
													 , char *pmerge_update_str,
													 char *pmerge_insert_fields_str,
													 char *pmerge_insert_values_str
													 #endif
												   );

extern void nm_STATION_set_square_footage( void *const pstation,
										   UNS_32 psquare_footage,
										   const BOOL_32 pgenerate_change_line_bool,
										   const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const BOOL_32 pset_change_bits,
										   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , char *pmerge_update_str,
										   char *pmerge_insert_fields_str,
										   char *pmerge_insert_values_str
										   #endif
										 );

extern void nm_STATION_set_ignore_moisture_balance_at_next_irrigation(	void *const pstation,
																				BOOL_32 pphysically_available,
																				const BOOL_32 pgenerate_change_line_bool,
																				const UNS_32 preason_for_change,
																				const UNS_32 pbox_index_0,
																				const BOOL_32 pset_change_bits,
																				CHANGE_BITS_32_BIT *pchange_bitfield_to_set
																				#ifdef _MSC_VER
																				, char *pmerge_update_str,
																				char *pmerge_insert_fields_str,
																				char *pmerge_insert_values_str
																				#endif
																			);

extern void nm_STATION_get_Budget_data( STATION_STRUCT* const pstation, STATION_STRUCT_for_budgets *budget);

// ----------

extern STATION_STRUCT *nm_STATION_get_pointer_to_station( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0 );

extern STATION_STRUCT *nm_STATION_get_pointer_to_decoder_based_station( const UNS_32 pbox_index_0, const UNS_32 pdecoder_serial_number, const UNS_32 pdecoder_output );

extern STATION_STRUCT *STATION_get_next_available_station( UNS_32 *pserial_number_ptr, UNS_32 *pstation_number_0_ptr );

extern STATION_STRUCT *STATION_get_prev_available_station( UNS_32 *pserial_number_ptr, UNS_32 *pstation_number_0_ptr );

// ----------

extern void STATION_clean_house_processing( void );


extern void STATION_set_not_physically_available_based_upon_communication_scan_results( void );

extern void STATION_for_stations_on_this_box_set_physically_available_based_upon_discovery_results( void );


extern void STATION_set_bits_on_all_stations_to_cause_distribution_in_the_next_token( void );

// ----------

#define COMMSERVER_CHANGE_BITS_ALL_SET		(0x22)

#define COMMSERVER_CHANGE_BITS_ALL_CLEARED	(0x33)

extern void STATION_on_all_stations_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear );

extern void nm_STATION_update_pending_change_bits( const UNS_32 pcomm_error_occurred );

// ----------

extern void STATIONS_load_all_the_stations_for_ftimes( void );


extern BOOL_32 STATION_calculate_chain_sync_crc( UNS_32 const pff_name );



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

