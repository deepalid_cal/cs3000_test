/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_SHARED_MANUAL_PROGRAMS_C
#define _INC_SHARED_MANUAL_PROGRAMS_C

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"group_base_file.h"

#include	"cal_td_utils.h"

#include	"change.h"

#ifndef	_MSC_VER

	#include	"alerts.h"

	#include	"app_startup.h"
	
	#include	"cs_mem.h"

	#include	"battery_backed_vars.h"

#else

	#include	<stdlib.h>

	#include	"sql_merge.h"

#endif




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define		MANUAL_PROGRAMS_CHANGE_BITFIELD_description_bit		(0)
#define		MANUAL_PROGRAMS_CHANGE_BITFIELD_start_times_bit		(1)
#define		MANUAL_PROGRAMS_CHANGE_BITFIELD_days_bit			(2)
#define		MANUAL_PROGRAMS_CHANGE_BITFIELD_start_date_bit		(3)

#define		MANUAL_PROGRAMS_CHANGE_BITFIELD_stop_date_bit		(4)
#define		MANUAL_PROGRAMS_CHANGE_BITFIELD_run_time_bit		(5)
#define		MANUAL_PROGRAMS_CHANGE_BITFIELD_in_use_bit			(6)

// ----------

static char* const MANUAL_PROGRAMS_database_field_names[ (MANUAL_PROGRAMS_CHANGE_BITFIELD_in_use_bit + 1) ] =
{
	"Name",
	"StartTime",		// This is actually an array of 6 values - StartTime1 - StartTime6
	"Day",				// This is actually an array of 7 values - Day1 - Day7
	"StartDate",
	"StopDate",
	"RunTime",
	"InUse"
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/**
 * The definition of the group's structure. If the contents of this structure
 * are changed or reordered, you <b>MUST</b> update the MANUAL_PROGRAMS_LATEST_FILE_VERSION 
 * located in this structure's header file. 
 */
struct MANUAL_PROGRAMS_GROUP_STRUCT
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	UNS_32							start_times[ MANUAL_PROGRAMS_NUMBER_OF_START_TIMES ];

	// 10/3/2012 rmd : Sunday is of course 0.
	BOOL_32							days[ DAYS_IN_A_WEEK ];
	
	UNS_32							start_date;

	UNS_32							stop_date;

	UNS_32							run_time_seconds;

	// -------------------------------------------- //
	//  CHANGE BITS
	// -------------------------------------------- //

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

	// -------------------------------------------- //

	// 4/30/2015 ajv : Added in Rev 2
	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

	// ----------

	// 9/22/2015 ajv : Added in Rev 3
	// 
	// By default, all mainline are "in use". If a user deletes a mainline, this
	// flag is set false. Although this keeps the item in memory and in the file, this is
	// necessary so the change can propagate through the chain and to the CommServer. If this
	// flag is FALSE, it will be hidden from view and completely inaccessible to the user.
	BOOL_32							in_use;
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void nm_MANUAL_PROGRAMS_set_start_time( void *const pgroup,
											   const UNS_32 pstart_index_0,
											   UNS_32 pstart_time,
											   const BOOL_32 pgenerate_change_line_bool,
											   const UNS_32 preason_for_change,
											   const UNS_32 pbox_index_0,
											   const BOOL_32 pset_change_bits,
											   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											   #ifdef _MSC_VER
											   , char *pmerge_update_str,
											   char *pmerge_insert_fields_str,
											   char *pmerge_insert_values_str
											   #endif
											 )
{
	char	lfield_name[ 32 ];

	if( pstart_index_0 < MANUAL_PROGRAMS_NUMBER_OF_START_TIMES )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_start_times_bit ], (pstart_index_0 + 1) );

		SHARED_set_uint32_with_32_bit_change_bits_group( pgroup,
														 &(((MANUAL_PROGRAMS_GROUP_STRUCT*)pgroup)->start_times[ pstart_index_0 ]),
														 pstart_time,
														 MANUAL_PROGRAMS_START_TIME_MIN,
														 MANUAL_PROGRAMS_START_TIME_MAX,
														 MANUAL_PROGRAMS_START_TIME_DEFAULT,
														 pgenerate_change_line_bool,
														 (CHANGE_MANUAL_PROGRAMS_START_TIME_1 + pstart_index_0),
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((MANUAL_PROGRAMS_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
														 MANUAL_PROGRAMS_CHANGE_BITFIELD_start_times_bit,
														 lfield_name
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   );
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
static void nm_MANUAL_PROGRAMS_set_water_day( void *const pgroup,
											  const UNS_32 pday,
											  BOOL_32 pis_a_water_day_bool,
											  const BOOL_32 pgenerate_change_line_bool,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
											)
{
	char	lfield_name[ 32 ];

	if( pday < DAYS_IN_A_WEEK )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_days_bit ], (pday + 1) );

		SHARED_set_bool_with_32_bit_change_bits_group( pgroup,
													   &(((MANUAL_PROGRAMS_GROUP_STRUCT*)pgroup)->days[ pday ]),
													   pis_a_water_day_bool,
													   MANUAL_PROGRAMS_WATERDAY_DEFAULT,
													   pgenerate_change_line_bool,
													   (CHANGE_MANUAL_PROGRAMS_WATER_DAY_SUN + pday),
													   preason_for_change,
													   pbox_index_0,
													   pset_change_bits,
													   pchange_bitfield_to_set,
													   &(((MANUAL_PROGRAMS_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													   MANUAL_PROGRAMS_CHANGE_BITFIELD_days_bit,
													   lfield_name
													   #ifdef _MSC_VER
													   , pmerge_update_str,
													   pmerge_insert_fields_str,
													   pmerge_insert_values_str
													   #endif
													 );
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
static void nm_MANUAL_PROGRAMS_set_start_date( void *const pgroup,
											   UNS_32 pstart_date,
											   const BOOL_32 pgenerate_change_line_bool,
											   const UNS_32 preason_for_change,
											   const UNS_32 pbox_index_0,
											   const BOOL_32 pset_change_bits,
											   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											   #ifdef _MSC_VER
											   , char *pmerge_update_str,
											   char *pmerge_insert_fields_str,
											   char *pmerge_insert_values_str
											   #endif
											 )
{
	// 11/17/2014 rmd : We used to limit the date range from todays date to todays date + 365
	// days. That was restrictive and annoying. So I've changed this to allow any valid date as
	// defined by our application.
	SHARED_set_date_with_32_bit_change_bits_group( pgroup,
												   &(((MANUAL_PROGRAMS_GROUP_STRUCT*)pgroup)->start_date),
												   pstart_date,
												   DATE_MINIMUM,
												   DATE_MAXIMUM,
												   DATE_MINIMUM,
												   pgenerate_change_line_bool,
												   CHANGE_MANUAL_PROGRAMS_START_DATE,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((MANUAL_PROGRAMS_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
												   MANUAL_PROGRAMS_CHANGE_BITFIELD_start_date_bit,
												   MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_start_date_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
											     );
}

/* ---------------------------------------------------------- */
static void nm_MANUAL_PROGRAMS_set_end_date( void *const pgroup,
											 UNS_32 pstop_date,
											 const BOOL_32 pgenerate_change_line_bool,
											 const UNS_32 preason_for_change,
											 const UNS_32 pbox_index_0,
											 const BOOL_32 pset_change_bits,
											 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											 #ifdef _MSC_VER
											 , char *pmerge_update_str,
											 char *pmerge_insert_fields_str,
											 char *pmerge_insert_values_str
											 #endif
										   )
{
	SHARED_set_date_with_32_bit_change_bits_group( pgroup,
												   &(((MANUAL_PROGRAMS_GROUP_STRUCT*)pgroup)->stop_date),
												   pstop_date,
												   DATE_MINIMUM,
												   DATE_MAXIMUM,
												   DATE_MINIMUM,
												   pgenerate_change_line_bool,
												   CHANGE_MANUAL_PROGRAMS_STOP_DATE,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((MANUAL_PROGRAMS_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
												   MANUAL_PROGRAMS_CHANGE_BITFIELD_stop_date_bit,
												   MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_stop_date_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
											     );
}

/* ---------------------------------------------------------- */
static void nm_MANUAL_PROGRAMS_set_run_time( void *const pgroup,
											 UNS_32 prun_time_seconds,
											 const BOOL_32 pgenerate_change_line_bool,
											 const UNS_32 preason_for_change,
											 const UNS_32 pbox_index_0,
											 const BOOL_32 pset_change_bits,
											 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											 #ifdef _MSC_VER
											 , char *pmerge_update_str,
											 char *pmerge_insert_fields_str,
											 char *pmerge_insert_values_str
											 #endif
										   )
{
	SHARED_set_uint32_with_32_bit_change_bits_group( pgroup,
													 &(((MANUAL_PROGRAMS_GROUP_STRUCT*)pgroup)->run_time_seconds),
													 prun_time_seconds,
													 STATION_MANUAL_PROGRAM_SECONDS_MIN,
													 STATION_MANUAL_PROGRAM_SECONDS_MAX,
													 STATION_MANUAL_PROGRAM_SECONDS_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_MANUAL_PROGRAMS_RUN_TIME,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((MANUAL_PROGRAMS_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 MANUAL_PROGRAMS_CHANGE_BITFIELD_run_time_bit,
													 MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_run_time_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

static void nm_MANUAL_PROGRAMS_set_in_use(	void *const pgroup,
											BOOL_32 pin_use,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0,
											const BOOL_32 pset_change_bits,
											CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											#ifdef _MSC_VER
											, char *pmerge_update_str,
											char *pmerge_insert_fields_str,
											char *pmerge_insert_values_str
											#endif
									   )
{
	SHARED_set_bool_with_32_bit_change_bits_group(pgroup,
												   &(((MANUAL_PROGRAMS_GROUP_STRUCT*)pgroup)->in_use),
												   pin_use,
												   MANUAL_PROGRAMS_IN_USE_DEFAULT,
												   pgenerate_change_line_bool,
												   CHANGE_MANUAL_PROGRAMS_IN_USE,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((MANUAL_PROGRAMS_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
												   MANUAL_PROGRAMS_CHANGE_BITFIELD_in_use_bit,
												   MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_in_use_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
static void nm_MANUAL_PROGRAMS_store_changes( MANUAL_PROGRAMS_GROUP_STRUCT *const ptemporary_group,
											  const BOOL_32 pgroup_created,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  const UNS_32 pchanges_received_from,
											  CHANGE_BITS_32_BIT pbitfield_of_changes
											  #ifdef _MSC_VER
											  , char *pSQL_update_str,
											  char *pSQL_insert_fields_str,
											  char *pSQL_insert_values_str
											  #endif
											)
{
	MANUAL_PROGRAMS_GROUP_STRUCT 	*lgroup;

	CHANGE_BITS_32_BIT				*lchange_bitfield_to_set;

	UNS_32	i;

	// ----------

	#ifndef _MSC_VER
	nm_GROUP_find_this_group_in_list( &manual_programs_group_list_hdr, ptemporary_group, (void**)&lgroup );

	if( lgroup != NULL )
	#else

	// 1/7/2015 ajv : Since the SQL statements manage only updating the value if it's changed,
	// there's no need for us to determine whether the group already exists in the database or
	// not. Therefore, lgroup and ptemporary_group can be the same value.
	lgroup = ptemporary_group;

	#endif
	{
		#ifndef _MSC_VER

			if( pgroup_created == (true) )
			{
				Alert_ChangeLine_Group( CHANGE_GROUP_CREATED, nm_GROUP_get_name( ptemporary_group ), NULL, NULL, NUMBER_OF_CHARS_IN_A_NAME, preason_for_change, pbox_index_0 );
			}

			lchange_bitfield_to_set = MANUAL_PROGRAMS_get_change_bits_ptr( lgroup, pchanges_received_from );

		#else

			// 7/28/2015 ajv : Initialize to NULL since this variable is used by the CommServer but
			// expected to be NULL.
			lchange_bitfield_to_set = NULL;

		#endif

		// 4/24/2015 ajv : Only process the change if it was either initiated via the keypad
		// (locally) or if the change bit was set (when called via communications)
		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, MANUAL_PROGRAMS_CHANGE_BITFIELD_description_bit ) == (true)) )
		{
			SHARED_set_name_32_bit_change_bits( lgroup, nm_GROUP_get_name( ptemporary_group ), !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set, &lgroup->changes_to_upload_to_comm_server, MANUAL_PROGRAMS_CHANGE_BITFIELD_description_bit
												#ifdef _MSC_VER
												, MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_description_bit ], pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												#endif
											  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, MANUAL_PROGRAMS_CHANGE_BITFIELD_start_times_bit ) == (true)) )
		{
			for( i = 0; i < MANUAL_PROGRAMS_NUMBER_OF_START_TIMES; i++ )
			{
				nm_MANUAL_PROGRAMS_set_start_time( lgroup, i, ptemporary_group->start_times[ i ], !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												   #ifdef _MSC_VER
												   , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												   #endif
												 );
			}
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, MANUAL_PROGRAMS_CHANGE_BITFIELD_days_bit ) == (true)) )
		{
			for( i = 0; i < DAYS_IN_A_WEEK; i++ )
			{
				nm_MANUAL_PROGRAMS_set_water_day( lgroup, i, ptemporary_group->days[ i ], !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												  #endif
												);
			}
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, MANUAL_PROGRAMS_CHANGE_BITFIELD_start_date_bit ) == (true)) )
		{
			nm_MANUAL_PROGRAMS_set_start_date( lgroup, ptemporary_group->start_date, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											   #ifdef _MSC_VER
											   , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											   #endif
											 );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, MANUAL_PROGRAMS_CHANGE_BITFIELD_stop_date_bit ) == (true)) )
		{
			nm_MANUAL_PROGRAMS_set_end_date( lgroup, ptemporary_group->stop_date, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											 #ifdef _MSC_VER
											 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											 #endif
										   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, MANUAL_PROGRAMS_CHANGE_BITFIELD_run_time_bit ) == (true)) )
		{
			nm_MANUAL_PROGRAMS_set_run_time( lgroup, ptemporary_group->run_time_seconds, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											 #ifdef _MSC_VER
											 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											 #endif
										   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, MANUAL_PROGRAMS_CHANGE_BITFIELD_in_use_bit ) == (true)) )
		{
			#ifndef _MSC_VER

				// 2/11/2016 ajv : If the group was deleted, reorder the list by moving the deleted item to
				// the end of the list.
				if( (lgroup->in_use) && (!ptemporary_group->in_use) )
				{
					nm_ListRemove( &manual_programs_group_list_hdr, lgroup );

					nm_ListInsertTail( &manual_programs_group_list_hdr, lgroup );
				}

			#endif

			nm_MANUAL_PROGRAMS_set_in_use(	lgroup, ptemporary_group->in_use, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											#ifdef _MSC_VER
											, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											#endif
										 );
		}
	}
	#ifndef _MSC_VER
	else
	{
		Alert_group_not_found();
	}
	#endif
}

/* ---------------------------------------------------------- */
extern UNS_32 nm_MANUAL_PROGRAMS_extract_and_store_changes_from_comm( const unsigned char *pucp,
																	  const UNS_32 preason_for_change,
																	  const BOOL_32 pset_change_bits,
																	  const UNS_32 pchanges_received_from
																	  #ifdef _MSC_VER
																	  , char *pSQL_statements,
																	  char *pSQL_search_condition_str,
																	  char *pSQL_update_str,
																	  char *pSQL_insert_fields_str,
																	  char *pSQL_insert_values_str,
																	  char *pSQL_single_merge_statement_buf,
																	  const UNS_32 pnetwork_ID
																	  #endif
																	)
{
	CHANGE_BITS_32_BIT				lbitfield_of_changes;

	MANUAL_PROGRAMS_GROUP_STRUCT 	*ltemporary_group;

	#ifndef _MSC_VER

	MANUAL_PROGRAMS_GROUP_STRUCT	*lmatching_group;

	#endif

	UNS_32	lnum_changed_groups;

	UNS_32	lsize_of_bitfield;

	BOOL_32	lgroup_created;

	UNS_32	lgroup_id;

	UNS_32	i;

	UNS_32	rv;


	rv = 0;

	lgroup_created = (false);

	// Extract the number of changed groups
	memcpy( &lnum_changed_groups, pucp, sizeof(lnum_changed_groups) );
	pucp += sizeof( lnum_changed_groups );
	rv += sizeof( lnum_changed_groups );

	/*
	// 8/8/2016 rmd : For debug only.
	if( pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE )
	{
		Alert_Message_va( "rcvd from slave: MANUAL PROGRAM CHANGES for %u groups", lnum_changed_groups );
	}
	else
	if( pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER )
	{
		Alert_Message_va( "rcvd from master: MANUAL PROGRAM CHANGES for %u groups", lnum_changed_groups );
	}
	else
	{
		Alert_Message_va( "rcvd from UNK: MANUAL PROGRAM CHANGES for %u groups", lnum_changed_groups );
	}
	*/
	
	// ----------
	
	for( i = 0; i < lnum_changed_groups; ++i )
	{
		memcpy( &lgroup_id, pucp, sizeof(lgroup_id) );
		pucp += sizeof( lgroup_id );
		rv += sizeof( lgroup_id );

		memcpy( &lsize_of_bitfield, pucp, sizeof(lsize_of_bitfield) );
		pucp += sizeof( lsize_of_bitfield );
		rv += sizeof( lsize_of_bitfield );

		memcpy( &lbitfield_of_changes, pucp, sizeof(lbitfield_of_changes) );
		pucp += sizeof( lbitfield_of_changes );
		rv += sizeof( lbitfield_of_changes );

		// 1/7/2015 ajv : When compiling for the CommServer, we build a MERGE statement if any of
		// the station's settings have changed. Therefore, we can completely skip over searching for
		// the station - this will fail anyway since the list doesn't exist in the comm server.
		#ifndef _MSC_VER

			// Find the existing group with the group ID
			lmatching_group = nm_GROUP_get_ptr_to_group_with_this_GID( &manual_programs_group_list_hdr, lgroup_id, (true) );

			// 3/6/2013 ajv : If the specified group doesn't exist yet, we need to
			// create it and assign it the group ID.
			if( lmatching_group == NULL )
			{
				lmatching_group = nm_MANUAL_PROGRAMS_create_new_group( pchanges_received_from );

				// Force the new Group ID to the passed-in group ID.
				lmatching_group->base.group_identity_number = lgroup_id;

				lgroup_created = (true);

				// ----------

				if( pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER )
				{
					// 5/2/2014 ajv : Any group creation caused by receipt of a
					// token is  for stations not at this box. And therefore
					// we don't have to resend out these changes. The distribution
					// of this activity creating groups and assigning the values
					// STOPS here in this case.
					SHARED_clear_all_32_bit_change_bits( &lmatching_group->changes_to_send_to_master, list_program_data_recursive_MUTEX );

					//Alert_Message_va( "PDATA: ManP %u created cleared", lgroup_id );
				}
				else
				{
					//Alert_Message_va( "PDATA: ManP %u created", lgroup_id );
				}
			}
			else
			{
				//Alert_Message_va( "PDATA: ManP %u found 0x%08x", lgroup_id, lbitfield_of_changes );
			}

		#endif

		#ifndef _MSC_VER

		if( lmatching_group != NULL )

		#endif
		{
			// Allocate memory for, and initialize, the temporary group
			#ifndef _MSC_VER
				ltemporary_group = mem_malloc( sizeof(MANUAL_PROGRAMS_GROUP_STRUCT) );
			#else
				ltemporary_group = malloc( sizeof(MANUAL_PROGRAMS_GROUP_STRUCT) );
			#endif

			memset( ltemporary_group, 0x00, sizeof(MANUAL_PROGRAMS_GROUP_STRUCT) );

			#ifndef _MSC_VER

				// Make a copy of the group. We do this so we can change the settings
				// and pass it to the store_changes routine which will actually update
				// the group's settings.
				memcpy( ltemporary_group, lmatching_group, sizeof(MANUAL_PROGRAMS_GROUP_STRUCT) );

			#else

				ltemporary_group->base.group_identity_number = lgroup_id;

			#endif

			if( B_IS_SET( lbitfield_of_changes, MANUAL_PROGRAMS_CHANGE_BITFIELD_description_bit ) == (true) )
			{
				memcpy( &ltemporary_group->base.description, pucp, NUMBER_OF_CHARS_IN_A_NAME );
				pucp += NUMBER_OF_CHARS_IN_A_NAME;
				rv += NUMBER_OF_CHARS_IN_A_NAME;
			}

			if( B_IS_SET( lbitfield_of_changes, MANUAL_PROGRAMS_CHANGE_BITFIELD_start_times_bit ) == (true) )
			{
				memcpy( &ltemporary_group->start_times, pucp, sizeof(ltemporary_group->start_times) );
				pucp += sizeof( ltemporary_group->start_times );
				rv += sizeof( ltemporary_group->start_times );
			}

			if( B_IS_SET( lbitfield_of_changes, MANUAL_PROGRAMS_CHANGE_BITFIELD_days_bit ) == (true) )
			{
				memcpy( &ltemporary_group->days, pucp, sizeof(ltemporary_group->days) );
				pucp += sizeof( ltemporary_group->days );
				rv += sizeof( ltemporary_group->days );
			}

			if( B_IS_SET( lbitfield_of_changes, MANUAL_PROGRAMS_CHANGE_BITFIELD_start_date_bit ) == (true) )
			{
				memcpy( &ltemporary_group->start_date, pucp, sizeof(ltemporary_group->start_date) );
				pucp += sizeof( ltemporary_group->start_date );
				rv += sizeof( ltemporary_group->start_date );
			}

			if( B_IS_SET( lbitfield_of_changes, MANUAL_PROGRAMS_CHANGE_BITFIELD_stop_date_bit ) == (true) )
			{
				memcpy( &ltemporary_group->stop_date, pucp, sizeof(ltemporary_group->stop_date) );
				pucp += sizeof( ltemporary_group->stop_date );
				rv += sizeof( ltemporary_group->stop_date );
			}

			if( B_IS_SET( lbitfield_of_changes, MANUAL_PROGRAMS_CHANGE_BITFIELD_run_time_bit ) == (true) )
			{
				memcpy( &ltemporary_group->run_time_seconds, pucp, sizeof(ltemporary_group->run_time_seconds) );
				pucp += sizeof( ltemporary_group->run_time_seconds );
				rv += sizeof( ltemporary_group->run_time_seconds );
			}

			if( B_IS_SET( lbitfield_of_changes, MANUAL_PROGRAMS_CHANGE_BITFIELD_in_use_bit ) == (true) )
			{
				memcpy( &ltemporary_group->in_use, pucp, sizeof(ltemporary_group->in_use) );
				pucp += sizeof( ltemporary_group->in_use );
				rv += sizeof( ltemporary_group->in_use );
			}

			// ----------

			#ifdef _MSC_VER

				// 1/16/2015 ajv : Make sure the strings are fully initialized to NULL since we rely on this
				// to determine whether the string is empty or not.
				memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

				// 4/11/2016 ajv : Insert the network ID and GID in case the group doesn't exist in the
				// database yet.
				SQL_MERGE_build_upsert_uint32( pnetwork_ID, SQL_MERGE_include_field_name_in_insert_clause, "NetworkID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
				SQL_MERGE_build_upsert_uint32( ltemporary_group->base.group_identity_number, SQL_MERGE_include_field_name_in_insert_clause, "ProgramGID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

			#endif

			// Now that we've extracted the settings out of the communication
			// structure, store the changes
			nm_MANUAL_PROGRAMS_store_changes( ltemporary_group, lgroup_created, preason_for_change, 0, pset_change_bits, pchanges_received_from, lbitfield_of_changes
											  #ifdef _MSC_VER
											  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											  #endif
											);

			#ifndef _MSC_VER

				mem_free( ltemporary_group );

			#else

				// ----------

				SQL_MERGE_build_search_condition_uint32( "NetworkID", pnetwork_ID, pSQL_search_condition_str );
				SQL_MERGE_build_search_condition_uint32( "ProgramGID", ltemporary_group->base.group_identity_number, pSQL_search_condition_str );

				SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements( DATABASE_TABLE_NAME_MANUAL_PROGRAMS, pSQL_search_condition_str, SQL_MERGE_include_field_name_in_insert_clause, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf );

				// ----------

				free( ltemporary_group );

			#endif
		}
		#ifndef _MSC_VER
		else
		{
			Alert_group_not_found();
		}
		#endif
	}

	#ifndef _MSC_VER

		// Only save the file if we've extracted information out of the ucp. This is
		// a sanity check to ensure the bit is only set if data was included.
		if( rv > 0 )
		{
			// 5/13/2015 ajv : We want to save the file in the following cases:
			// 
			//   - A message from the CommServer since we've responded that we received it and need to
			//     ensure it's saved.
			//   - A token response from the slave to the master
			//   - A token from the master to the slave
			//   
			// We DO NOT want to save the file when a token is received by the master since we've
			// already saved the file for one of the reasons above. This will otherwise result in a
			// double save for each change - once when it comes in and once when it's received when the
			// master distributes the changes back out.
			if( (pchanges_received_from == CHANGE_REASON_CENTRAL_OR_MOBILE) ||
				(pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE) ||
				((pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER) && (!FLOWSENSE_we_are_a_master_one_way_or_another())) )
			{
				// 12/10/2014 ajv : Schedule a save rather than saving immediately to prevent stacking of
				// saves. Additionally, set a relatively short time to hel cover cases where users make a
				// lot of changes in a short period.
				FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_MANUAL_PROGRAMS, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
			}
		}
		else
		{
			Alert_bit_set_with_no_data();
		}

	#endif

	return ( rv );
}

#ifdef _MSC_VER

/* ---------------------------------------------------------- */
extern INT_32 MANUAL_PROGRAMS_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order )
{
	MANUAL_PROGRAMS_GROUP_STRUCT	*lgroup;

	INT_32	rv;

	// ----------

	// 4/15/2016 ajv : In most cases, we're going to pass back an array_index of 0. However, in
	// the event the value in question is part of an array, we're going to pass back the actual
	// index so that the CommServer can sort not just on the bit number, but also the
	// array_index for values that share the same bit number.
	*psecondary_sort_order = 0;

	rv = -1;

	// ----------

	if( strncmp( pfield_name, MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_description_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MANUAL_PROGRAMS_CHANGE_BITFIELD_description_bit;
		*psize_of_var_ptr = sizeof(lgroup->base.description);
	}
	else if( strncmp( pfield_name, MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_start_times_bit ], 9 ) == 0 )
	{
		// 1/13/2015 ajv : Note the above condition is not checking the entire length of the field
		// name. This is because there are 6 unique start time values: StartTime1 - StartTime6. By
		// only comparing the first 9 characters, we can ensure we catch all 6 in one condition.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "3" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "4" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}
		else if( strstr( pfield_name, "5" ) != NULL )
		{
			*psecondary_sort_order = 4;
		}
		else if( strstr( pfield_name, "6" ) != NULL )
		{
			*psecondary_sort_order = 5;
		}
		else if( strstr( pfield_name, "7" ) != NULL )
		{
			*psecondary_sort_order = 6;
		}

		rv = MANUAL_PROGRAMS_CHANGE_BITFIELD_start_times_bit;
		*psize_of_var_ptr = sizeof(lgroup->start_times[ 0 ]);
	}
	else if( strncmp( pfield_name, MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_days_bit ], 3 ) == 0 )
	{
		// 1/13/2015 ajv : Note the above condition is not checking the entire length of the field
		// name. This is because there are 7 unique day values: Day1 - Day7. By only comparing
		// the first 3 characters, we can ensure we catch all 7 in one condition.
		// 
		// Keep in mind, however, this means we can't have another field in this table start with
		// the word "Day".

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "3" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "4" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}
		else if( strstr( pfield_name, "5" ) != NULL )
		{
			*psecondary_sort_order = 4;
		}
		else if( strstr( pfield_name, "6" ) != NULL )
		{
			*psecondary_sort_order = 5;
		}
		else if( strstr( pfield_name, "7" ) != NULL )
		{
			*psecondary_sort_order = 6;
		}

		rv = MANUAL_PROGRAMS_CHANGE_BITFIELD_days_bit;
		*psize_of_var_ptr = sizeof(lgroup->days[ 0 ]);
	}
	else if( strncmp( pfield_name, MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_start_date_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MANUAL_PROGRAMS_CHANGE_BITFIELD_start_date_bit;
		*psize_of_var_ptr = sizeof(lgroup->start_date);
	}
	else if( strncmp( pfield_name, MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_stop_date_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MANUAL_PROGRAMS_CHANGE_BITFIELD_stop_date_bit;
		*psize_of_var_ptr = sizeof(lgroup->stop_date);
	}
	else if( strncmp( pfield_name, MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_run_time_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MANUAL_PROGRAMS_CHANGE_BITFIELD_run_time_bit;
		*psize_of_var_ptr = sizeof(lgroup->run_time_seconds);
	}
	else if( strncmp( pfield_name, MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_in_use_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = MANUAL_PROGRAMS_CHANGE_BITFIELD_in_use_bit;
		*psize_of_var_ptr = sizeof(lgroup->in_use);
	}

	return( rv );
}

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

