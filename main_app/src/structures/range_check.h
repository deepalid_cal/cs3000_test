/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_RANGE_CHECK_H
#define _INC_RANGE_CHECK_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

// 6/18/2014 ajv : Required to ensure ALERT_INCLUDE_FILENAME and associated
// macros are defined
#include	"cs_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#if ALERT_INCLUDE_FILENAME

	/* ---------------------------------------------------------- */
	/**
	 * Range checks a boolean value to ensure it's either true or false. If the 
	 * range check fails, the passed-in value is reset to the specified default 
	 * value. An alert is also generated stating the value is out-of-range.
	 *  
	 * Note: When compiling using the ARM DEBUG build configuration, the 
	 * filename and line number of where this call was made are also included in 
	 * the alert line. 
	 *  
	 * @mutex_requirements (none) 
	 *
	 * @memory_responsibilities (none) 
	 *
	 * @task_execution May be executed within the context of any task since the 
	 * routine performs a simple comparison, resets the value to the default if 
	 * the range check fails, and generates an associated alert.
	 * 
	 * @param pValue (BOOL_32) A pointer to the value to range check.
	 *  
	 * @param pDefault (BOOL_32) The default value to set the value to if the 
	 * range check fails. 
	 *  
	 * @param pGroupName (char *) The name of the group this setting belongs to.
	 * If the setting is not part of a group, pass NULL. 
	 *  
	 * @param pVariableName (char *) A user-friendly string describing what the 
	 * value represents. This is used when generating the alert so the user 
	 * understands what range check failed. For example, you may pass "Priority 
	 * Level" to describe a priority level. 
	 * 
	 * @return BOOL_32 True if the range check passed; otherwise, false.
	 *
	 * @author 9/1/2011 AdrianusV
	 *
	 * @revisions (none)
	 */
	#define	RangeCheck_bool( pValue, pDefault, pGroupName, pVariableName )					RC_bool_with_filename( pValue, pDefault, pGroupName, pVariableName, __FILE__, __LINE__ )

	/* ---------------------------------------------------------- */
	/**
	 * Range checks a date value (passed in as a 32-bit unsigned integer) to 
	 * ensure it's either within the specified minimum and maximum range. If the 
	 * value is out of range, the value is set to the default. An alert line is 
	 * also generated stating that the value was out of range. 
	 *  
	 * Note: When compiling using the ARM DEBUG build configuration, the 
	 * filename and line number of where this call was made are also included in 
	 * the alert line. 
	 *  
	 * @mutex_requirements (none) 
	 *
	 * @memory_responsibilities (none) 
	 *
	 * @task_execution May be executed within the context of any task since the 
	 * routine performs a simple comparison, resets the value to the default if 
	 * the range check fails, and generates an associated alert.
	 * 
	 * @param pValue (UNS_32) A pointer to the value to range check.
	 *  
	 * @param pMin (UNS_32) The minimum allowed value. 
	 *  
	 * @param pMax (UNS_32) The maximum allowed value. 
	 *  
	 * @param pDefault (UNS_32) The default value to set the value to if the 
	 * range check fails. 
	 *  
	 * @param pGroupName (char *) The name of the group this setting belongs to.
	 * If the setting is not part of a group, pass NULL. 
	 *  
	 * @param pVariableName (char *) A user-friendly string describing what the 
	 * value represents. This is used when generating the alert so the user 
	 * understands what range check failed. For example, you may pass "Priority 
	 * Level" to describe a priority level. 
	 * 
	 * @return BOOL_32 True if the range check passed; otherwise, false.
	 *
	 * @author 4/12/2012 AdrianusV
	 *
	 * @revisions (none)
	 */
	#define	RangeCheck_date( pValue, pMin, pMax, pDefault, pGroupName, pVariableName )		RC_date_with_filename( pValue, pMin, pMax, pDefault, pGroupName, pVariableName, __FILE__, __LINE__ )

	/* ---------------------------------------------------------- */
	/**
	 * Range checks a 32-bit signed integer value to ensure it's within the 
	 * specified minimum and maximum range. If the value is out of range, the 
	 * value is set to the default. An alert line is also generated stating that 
	 * the value was out of range. 
	 *  
	 * Note: When compiling using the ARM DEBUG build configuration, the 
	 * filename and line number of where this call was made are also included in 
	 * the alert line. 
	 *  
	 * @mutex_requirements (none) 
	 *
	 * @memory_responsibilities (none) 
	 *
	 * @task_execution May be executed within the context of any task since the 
	 * routine performs a simple comparison, resets the value to the default if 
	 * the range check fails, and generates an associated alert.
	 * 
	 * @param pValue (INT_32) A pointer to the value to range check.
	 *  
	 * @param pMin (INT_32) The minimum allowed value. 
	 *  
	 * @param pMax (INT_32) The maximum allowed value. 
	 *  
	 * @param pDefault (INT_32) The default value to set the value to if the 
	 * range check fails. 
	 *  
	 * @param pGroupName (char *) The name of the group this setting belongs to.
	 * If the setting is not part of a group, pass NULL. 
	 *  
	 * @param pVariableName (char *) A user-friendly string describing what the 
	 * value represents. This is used when generating the alert so the user 
	 * understands what range check failed. For example, you may pass "Priority 
	 * Level" to describe a priority level. 
	 * 
	 * @return BOOL_32 True if the range check passed; otherwise, false.
	 *
	 * @author 5/24/2012 AdrianusV
	 *
	 * @revisions (none)
	 */
	#define	RangeCheck_int32( pValue, pMin, pMax, pDefault, pGroupName, pVariableName )		RC_int32_with_filename( (INT_32*)pValue, pMin, pMax, pDefault, pGroupName, pVariableName, __FILE__, __LINE__ )

	/* ---------------------------------------------------------- */
	/**
	 * Range checks each character of the passed in string to verify they are 
	 * printable characters. Any characters that are not printable are replaced 
	 * with a question mark (?). An alert line is also generated stating that 
	 * the string contains invalid characters
	 *  
	 * Note: When compiling using the ARM DEBUG build configuration, the 
	 * filename and line number of where this call was made are also included in 
	 * the alert line. 
	 *  
	 * @mutex_requirements (none) 
	 *
	 * @memory_responsibilities (none) 
	 *
	 * @task_execution May be executed within the context of any task since the 
	 * routine performs a simple comparison, resets the value to the default if 
	 * the range check fails, and generates an associated alert.
	 * 
	 * @param pstring (char *) A pointer to the string to range check.
	 *  
	 * @param pLength (UNS_32) The maximum length of the string
	 *  
	 * @param pVariableName (char *) A user-friendly string describing what the 
	 * value represents. This is used when generating the alert so the user 
	 * understands what range check failed. For example, you may pass "Priority 
	 * Level" to describe a priority level. 
	 * 
	 * @return BOOL_32 True if there are no invalid characters; otherwise, 
	 * false.
	 *
	 * @author 9/6/2011 AdrianusV
	 *
	 * @revisions (none)
	 */
	#define	RangeCheck_string( pString, pLength, pVariableName )							RC_string_with_filename( pString, pLength, pVariableName, __FILE__, __LINE__ )

	/* ---------------------------------------------------------- */
	/**
	 * Range checks a time value (passed in as a 32-bit unsigned integer) to 
	 * ensure it's either within the specified minimum and maximum range. If the 
	 * value is out of range, the value is set to the default. An alert line is 
	 * also generated stating that the value was out of range. 
	 *  
	 * Note: When compiling using the ARM DEBUG build configuration, the 
	 * filename and line number of where this call was made are also included in 
	 * the alert line. 
	 *  
	 * @mutex_requirements (none) 
	 *
	 * @memory_responsibilities (none) 
	 *
	 * @task_execution May be executed within the context of any task since the 
	 * routine performs a simple comparison, resets the value to the default if 
	 * the range check fails, and generates an associated alert.
	 * 
	 * @param pValue (UNS_32) A pointer to the value to range check.
	 *  
	 * @param pMin (UNS_32) The minimum allowed value. 
	 *  
	 * @param pMax (UNS_32) The maximum allowed value. 
	 *  
	 * @param pDefault (UNS_32) The default value to set the value to if the 
	 * range check fails. 
	 *  
	 * @param pGroupName (char *) The name of the group this setting belongs to.
	 * If the setting is not part of a group, pass NULL. 
	 *  
	 * @param pVariableName (char *) A user-friendly string describing what the 
	 * value represents. This is used when generating the alert so the user 
	 * understands what range check failed. For example, you may pass "Priority 
	 * Level" to describe a priority level. 
	 * 
	 * @return BOOL_32 True if the range check passed; otherwise, false.
	 *
	 * @author 4/12/2012 AdrianusV
	 *
	 * @revisions (none)
	 */
	#define	RangeCheck_time( pValue, pMin, pMax, pDefault, pGroupName, pVariableName )		RC_time_with_filename( pValue, pMin, pMax, pDefault, pGroupName, pVariableName, __FILE__, __LINE__ )

	/* ---------------------------------------------------------- */
	/**
	 * Range checks a 32-bit unsigned integer value to ensure it's within the 
	 * specified minimum and maximum range. If the value is out of range, the 
	 * value is set to the default. An alert line is also generated stating that 
	 * the value was out of range. 
	 *  
	 * Note: When compiling using the ARM DEBUG build configuration, the 
	 * filename and line number of where this call was made are also included in 
	 * the alert line. 
	 *  
	 * @mutex_requirements (none) 
	 *
	 * @memory_responsibilities (none) 
	 *
	 * @task_execution May be executed within the context of any task since the 
	 * routine performs a simple comparison, resets the value to the default if 
	 * the range check fails, and generates an associated alert.
	 * 
	 * @param pValue (UNS_32) A pointer to the value to range check.
	 *  
	 * @param pMin (UNS_32) The minimum allowed value. 
	 *  
	 * @param pMax (UNS_32) The maximum allowed value. 
	 *  
	 * @param pDefault (UNS_32) The default value to set the value to if the 
	 * range check fails. 
	 *  
	 * @param pGroupName (char *) The name of the group this setting belongs to.
	 * If the setting is not part of a group, pass NULL. 
	 *  
	 * @param pVariableName (char *) A user-friendly string describing what the 
	 * value represents. This is used when generating the alert so the user 
	 * understands what range check failed. For example, you may pass "Priority 
	 * Level" to describe a priority level. 
	 * 
	 * @return BOOL_32 True if the range check passed; otherwise, false.
	 *
	 * @author 8/1/2011 AdrianusV
	 *
	 * @revisions (none)
	 */
	#define	RangeCheck_uint32( pValue, pMin, pMax, pDefault, pGroupName, pVariableName )	RC_uint32_with_filename( (UNS_32*)pValue, pMin, pMax, pDefault, pGroupName, pVariableName, __FILE__, __LINE__ )

	#define	RangeCheck_uns8( pValue, pMin, pMax, pDefault, pGroupName, pVariableName )		RC_uns8_with_filename( (UNS_8*)pValue, pMin, pMax, pDefault, pGroupName, pVariableName, __FILE__, __LINE__ )

	#define	RangeCheck_float( pValue, pMin, pMax, pDefault, pGroupName, pVariableName )		RC_float_with_filename( (float*)pValue, pMin, pMax, pDefault, pGroupName, pVariableName, __FILE__, __LINE__ )


	extern BOOL_32 RC_bool_with_filename( BOOL_32 *const pValue, const BOOL_32 pDefault, const char *const pGroupName, const char *const pVariableName, const char *const pFilename, const UNS_32 pLine );

	extern BOOL_32 RC_date_with_filename( UNS_32 *const pValue, const UNS_32 pMin, const UNS_32 pMax, const UNS_32 pDefault, const char *const pGroupName, const char *const pVariableName, const char *const pFilename, const UNS_32 pLine );

	extern BOOL_32 RC_int32_with_filename( INT_32 *const pValue, const INT_32 pMin, const INT_32 pMax, const INT_32 pDefault, const char *const pGroupName, const char *const pVariableName, const char *const pFilename, const UNS_32 pLine );

	extern BOOL_32 RC_string_with_filename( char *const pString, const UNS_32 pLength, const char *const pVariableName, const char *const pFilename, const UNS_32 pLine );

	extern BOOL_32 RC_time_with_filename( UNS_32 *const pValue, const UNS_32 pMin, const UNS_32 pMax, const UNS_32 pDefault, const char *const pGroupName, const char *const pVariableName, const char *const pFilename, const UNS_32 pLine );

	extern BOOL_32 RC_uint32_with_filename( UNS_32 *const pValue, const UNS_32 pMin, const UNS_32 pMax, const UNS_32 pDefault, const char *const pGroupName, const char *const pVariableName, const char *const pFilename, const UNS_32 pLine );

	extern BOOL_32 RC_uns8_with_filename( UNS_8 *const pValue, const UNS_8 pMin, const UNS_8 pMax, const UNS_8 pDefault, const char *const pGroupName, const char *const pVariableName, const char *const pFilename, const UNS_32 pLine );

	extern BOOL_32 RC_float_with_filename( float *const pValue, const float pMin, const float pMax, const float pDefault, const char *const pGroupName, const char *const pVariableName, const char *const pFilename, const UNS_32 pLine );

#else	// !ALERT_INCLUDE_FILENAME

	#define	RangeCheck_bool( pValue, pDefault, pGroupName, pVariableName )					RC_bool_without_filename( pValue, pDefault, pGroupName, pVariableName )

	#define	RangeCheck_date( pValue, pMin, pMax, pDefault, pGroupName, pVariableName )		RC_date_without_filename( pValue, pMin, pMax, pDefault, pGroupName, pVariableName )

	#define	RangeCheck_int32( pValue, pMin, pMax, pDefault, pGroupName, pVariableName )		RC_int32_without_filename( (INT_32*)pValue, pMin, pMax, pDefault, pGroupName, pVariableName )

	#define	RangeCheck_string( pString, pLength, pVariableName )							RC_string_without_filename( pString, pLength, pVariableName )

	#define	RangeCheck_time( pValue, pMin, pMax, pDefault, pGroupName, pVariableName )		RC_time_without_filename( pValue, pMin, pMax, pDefault, pGroupName, pVariableName )

	#define	RangeCheck_uint32( pValue, pMin, pMax, pDefault, pGroupName, pVariableName )	RC_uint32_without_filename( (UNS_32*)pValue, pMin, pMax, pDefault, pGroupName, pVariableName )

	#define	RangeCheck_uns8( pValue, pMin, pMax, pDefault, pGroupName, pVariableName )		RC_uns8_without_filename( (UNS_8*)pValue, pMin, pMax, pDefault, pGroupName, pVariableName )

	#define	RangeCheck_float( pValue, pMin, pMax, pDefault, pGroupName, pVariableName )		RC_float_without_filename( (float*)pValue, pMin, pMax, pDefault, pGroupName, pVariableName )


	extern BOOL_32 RC_bool_without_filename( BOOL_32 *const pValue, const BOOL_32 pDefault, const char *const pGroupName, const char *const pVariableName );

	extern BOOL_32 RC_date_without_filename( UNS_32 *const pValue, const UNS_32 pMin, const UNS_32 pMax, const UNS_32 pDefault, const char *const pGroupName, const char *const pVariableName );
	
	extern BOOL_32 RC_int32_without_filename( INT_32 *const pValue, const INT_32 pMin, const INT_32 pMax, const INT_32 pDefault, const char *const pGroupName, const char *const pVariableName );

	extern BOOL_32 RC_string_without_filename( char *const pString, const UNS_32 pLength, const char *const pVariableName );

	extern BOOL_32 RC_time_without_filename( UNS_32 *const pValue, const UNS_32 pMin, const UNS_32 pMax, const UNS_32 pDefault, const char *const pGroupName, const char *const pVariableName );

	extern BOOL_32 RC_uint32_without_filename( UNS_32 *const pValue, const UNS_32 pMin, const UNS_32 pMax, const UNS_32 pDefault, const char *const pGroupName, const char *const pVariableName );

	extern BOOL_32 RC_uns8_without_filename( UNS_8 *const pValue, const UNS_8 pMin, const UNS_8 pMax, const UNS_8 pDefault, const char *const pGroupName, const char *const pVariableName );

	extern BOOL_32 RC_float_without_filename( float *const pValue, const float pMin, const float pMax, const float pDefault, const char *const pGroupName, const char *const pVariableName );

#endif	// ALERT_INCLUDE_FILENAME

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

