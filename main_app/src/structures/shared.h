/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_SHARED_H
#define _INC_SHARED_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	"cal_td_utils.h"

#include	"configuration_network.h"

#include	"lpc_types.h"

#include	"pdata_changes.h"

#include	"stations.h"

#ifndef _MSC_VER

	#include	"cs_common.h"

#endif




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

extern void SHARED_set_32_bit_change_bits( CHANGE_BITS_32_BIT* pchange_bits_for_controller_ptr,
									CHANGE_BITS_32_BIT* pchange_bits_for_central_ptr,
									UNS_32 pchange_bit_to_set,
									const UNS_32 preason_for_change,
									const BOOL_32 pset_change_bits );

extern void SHARED_set_64_bit_change_bits( CHANGE_BITS_64_BIT* pchange_bits_for_controller_ptr,
										   CHANGE_BITS_64_BIT* pchange_bits_for_central_ptr,
										   UNS_32 pchange_bit_to_set,
										   const UNS_32 preason_for_change,
										   const BOOL_32 pset_change_bits );

// ----------

extern void SHARED_set_all_32_bit_change_bits( CHANGE_BITS_32_BIT *pchange_bits_to_se, xSemaphoreHandle precursive_mutex  );

extern void SHARED_clear_all_32_bit_change_bits( CHANGE_BITS_32_BIT *pchange_bits_to_clear, xSemaphoreHandle precursive_mutex  );

extern void SHARED_set_all_64_bit_change_bits( CHANGE_BITS_64_BIT *pchange_bits_to_set, xSemaphoreHandle precursive_mutex  );

extern void SHARED_clear_all_64_bit_change_bits( CHANGE_BITS_64_BIT *pchange_bits_to_clear, xSemaphoreHandle precursive_mutex  );

// ----------

extern CHANGE_BITS_32_BIT *SHARED_get_32_bit_change_bits_ptr( const UNS_32 pchange_reason,
															  CHANGE_BITS_32_BIT *pchanges_to_send_to_master,
															  CHANGE_BITS_32_BIT *pchanges_to_distribute_to_slaves,
															  CHANGE_BITS_32_BIT *pchanges_to_upload_to_comm_server );

extern CHANGE_BITS_64_BIT *SHARED_get_64_bit_change_bits_ptr( const UNS_32 pchange_reason,
															  CHANGE_BITS_64_BIT *pchanges_to_send_to_master,
															  CHANGE_BITS_64_BIT *pchanges_to_distribute_to_slaves,
															  CHANGE_BITS_64_BIT *pchanges_to_upload_to_comm_server );

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern BOOL_32 SHARED_set_bool_controller( BOOL_32 *pvar_to_change_ptr,
										   BOOL_32 pnew_value,
										   const BOOL_32 pdefault,
										   const BOOL_32 pgenerate_change_line_bool,
										   const UNS_32 pchange_line_index,
										   const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const BOOL_32 pset_change_bits,
										   CHANGE_BITS_32_BIT *pchange_bits_for_controller,
										   CHANGE_BITS_32_BIT *pchange_bits_for_central,
										   UNS_32 pchange_bit_to_set,
										   const char *pfield_name
										   #ifdef _MSC_VER
										   , char *pmerge_update_str,
										   char *pmerge_insert_fields_str,
										   char *pmerge_insert_values_str
										   #endif
										 );

extern BOOL_32 SHARED_set_uint32_controller( UNS_32 *pvar_to_change_ptr,
											 UNS_32 pnew_value,
											 const UNS_32 pmin,
											 const UNS_32 pmax,
											 const UNS_32 pdefault,
											 const BOOL_32 pgenerate_change_line_bool,
											 const UNS_32 pchange_line_index,
											 const UNS_32 preason_for_change,
											 const UNS_32 pbox_index_0,
											 const BOOL_32 pset_change_bits,
											 CHANGE_BITS_32_BIT *pchange_bits_for_controller,
											 CHANGE_BITS_32_BIT *pchange_bits_for_central,
											 UNS_32 pchange_bit_to_set,
											 const char *pfield_name
											 #ifdef _MSC_VER
											 , char *pmerge_update_str,
											 char *pmerge_insert_fields_str,
											 char *pmerge_insert_values_str
											 #endif
											);

extern BOOL_32 SHARED_set_string_controller( char *pvar_to_change_ptr,
											 const UNS_32 psize_of_string,
											 const char *pnew_value,
											 const BOOL_32 pgenerate_change_line_bool,
											 const UNS_32 pchange_line_index,
											 const UNS_32 preason_for_change,
											 const UNS_32 pbox_index_0,
											 const BOOL_32 pset_change_bits,
											 CHANGE_BITS_32_BIT *pchange_bits_for_controller,
											 CHANGE_BITS_32_BIT *pchange_bits_for_central,
											 UNS_32 pchange_bit_to_set,
											 const char *pfield_name
											 #ifdef _MSC_VER
											 , char *pmerge_update_str,
											 char *pmerge_insert_fields_str,
											 char *pmerge_insert_values_str
											 #endif
											);

extern BOOL_32 SHARED_set_time_controller( UNS_32 *pvar_to_change_ptr,
										   UNS_32 pnew_value,
										   const UNS_32 pmin,
										   const UNS_32 pmax,
										   const UNS_32 pdefault,
										   const BOOL_32 pgenerate_change_line_bool,
										   const UNS_32 pchange_line_index,
										   const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const BOOL_32 pset_change_bits,
										   CHANGE_BITS_32_BIT *pchange_bits_for_controller,
										   CHANGE_BITS_32_BIT *pchange_bits_for_central,
										   UNS_32 pchange_bit_to_set,
										   const char *pfield_name
										   #ifdef _MSC_VER
										   , char *pmerge_update_str,
										   char *pmerge_insert_fields_str,
										   char *pmerge_insert_values_str
										   #endif
										 );

extern UNS_32 SHARED_set_DLS_controller( DAYLIGHT_SAVING_TIME_STRUCT *pvar_to_change_ptr,
										 DAYLIGHT_SAVING_TIME_STRUCT pdls,
										 const UNS_32 pdefault_month,
										 const UNS_32 pdefault_min_day,
										 const UNS_32 pdefault_max_day,
										 const BOOL_32 pgenerate_change_line_bool,
										 const UNS_32 pchange_line_index,
										 const UNS_32 preason_for_change,
										 const UNS_32 pbox_index_0,
										 const BOOL_32 pset_change_bits,
										 CHANGE_BITS_32_BIT *pchange_bits_for_controller,
										 CHANGE_BITS_32_BIT *pchange_bits_for_central,
										 CHANGE_BITS_32_BIT pchange_bit_to_set,
										 const char *pfield_name_month
										 #ifdef _MSC_VER
										 , const char *pfield_name_min_day,
										 const char *pfield_name_max_day,
										 char *pmerge_update_str,
										 char *pmerge_insert_fields_str,
										 char *pmerge_insert_values_str
										 #endif
									   );

// ----------

extern BOOL_32 SHARED_set_bool_with_32_bit_change_bits_group( void *const pgroup,
															  BOOL_32 *pvar_to_change_ptr,
															  BOOL_32 pnew_value,
															  const BOOL_32 pdefault,
															  const BOOL_32 pgenerate_change_line_bool,
															  const UNS_32 pchange_line_index,
															  const UNS_32 preason_for_change,
															  const UNS_32 pbox_index_0,
															  const BOOL_32 pset_change_bits,
															  CHANGE_BITS_32_BIT *pchange_bits_for_controller,
															  CHANGE_BITS_32_BIT *pchange_bits_for_central,
															  UNS_32 pchange_bit_to_set,
															  const char *pfield_name
															  #ifdef _MSC_VER
															  , char *pmerge_update_str,
															  char *pmerge_insert_fields_str,
															  char *pmerge_insert_values_str
															  #endif
															);

extern BOOL_32 SHARED_set_bool_with_64_bit_change_bits_group( void *const pgroup,
															  BOOL_32 *pvar_to_change_ptr,
															  BOOL_32 pnew_value,
															  const BOOL_32 pdefault,
															  const BOOL_32 pgenerate_change_line_bool,
															  const UNS_32 pchange_line_index,
															  const UNS_32 preason_for_change,
															  const UNS_32 pbox_index_0,
															  const BOOL_32 pset_change_bits,
															  CHANGE_BITS_64_BIT *pchange_bits_for_controller,
															  CHANGE_BITS_64_BIT *pchange_bits_for_central,
															  UNS_32 pchange_bit_to_set,
															  const char *pfield_name
															  #ifdef _MSC_VER
															  , char *pmerge_update_str,
															  char *pmerge_insert_fields_str,
															  char *pmerge_insert_values_str
															  #endif
															);

extern BOOL_32 SHARED_set_uint32_with_32_bit_change_bits_group( void *const pgroup,
																UNS_32 *pvar_to_change_ptr,
																UNS_32 pnew_value,
																const UNS_32 pmin,
																const UNS_32 pmax,
																const UNS_32 pdefault,
																const BOOL_32 pgenerate_change_line_bool,
																const UNS_32 pchange_line_index,
																const UNS_32 preason_for_change,
																const UNS_32 pbox_index_0,
																const BOOL_32 pset_change_bits,
																CHANGE_BITS_32_BIT *pchange_bits_for_controller,
																CHANGE_BITS_32_BIT *pchange_bits_for_central,
																UNS_32 pchange_bit_to_set,
																const char *pfield_name
																#ifdef _MSC_VER
																, char *pmerge_update_str,
																char *pmerge_insert_fields_str,
																char *pmerge_insert_values_str
																#endif
															  );

extern BOOL_32 SHARED_set_uint32_with_64_bit_change_bits_group( void *const pgroup,
																UNS_32 *pvar_to_change_ptr,
																UNS_32 pnew_value,
																const UNS_32 pmin,
																const UNS_32 pmax,
																const UNS_32 pdefault,
																const BOOL_32 pgenerate_change_line_bool,
																const UNS_32 pchange_line_index,
																const UNS_32 preason_for_change,
																const UNS_32 pbox_index_0,
																const BOOL_32 pset_change_bits,
																CHANGE_BITS_64_BIT *pchange_bits_for_controller,
																CHANGE_BITS_64_BIT *pchange_bits_for_central,
																UNS_32 pchange_bit_to_set,
																const char *pfield_name
																#ifdef _MSC_VER
																, char *pmerge_update_str,
																char *pmerge_insert_fields_str,
																char *pmerge_insert_values_str
																#endif
															  );

extern BOOL_32 SHARED_set_int32_with_32_bit_change_bits_group( void *const pgroup,
															   INT_32 *pvar_to_change_ptr,
															   INT_32 pnew_value,
															   const INT_32 pmin,
															   const INT_32 pmax,
															   const INT_32 pdefault,
															   const BOOL_32 pgenerate_change_line_bool,
															   const UNS_32 pchange_line_index,
															   const UNS_32 preason_for_change,
															   const UNS_32 pbox_index_0,
															   const BOOL_32 pset_change_bits,
															   CHANGE_BITS_32_BIT *pchange_bits_for_controller,
															   CHANGE_BITS_32_BIT *pchange_bits_for_central,
															   UNS_32 pchange_bit_to_set,
															   const char *pfield_name
															   #ifdef _MSC_VER
															   , char *pmerge_update_str,
															   char *pmerge_insert_fields_str,
															   char *pmerge_insert_values_str
															   #endif
															 );

extern BOOL_32 SHARED_set_float_with_32_bit_change_bits_group(	void *const pgroup,
																float *pvar_to_change_ptr,
																float pnew_value,
																const float pmin,
																const float pmax,
																const float pdefault,
																const BOOL_32 pgenerate_change_line_bool,
																const UNS_32 pchange_line_index,
																const UNS_32 preason_for_change,
																const UNS_32 pbox_index_0,
																const BOOL_32 pset_change_bits,
																CHANGE_BITS_32_BIT *pchange_bits_for_controller,
																CHANGE_BITS_32_BIT *pchange_bits_for_central,
																UNS_32 pchange_bit_to_set,
																const char *pfield_name
																#ifdef _MSC_VER
																, char *pmerge_update_str,
																char *pmerge_insert_fields_str,
																char *pmerge_insert_values_str
																#endif
															  );
															  
extern BOOL_32 SHARED_set_date_with_32_bit_change_bits_group( void *const pgroup,
															  UNS_32 *pvar_to_change_ptr,
															  UNS_32 pnew_value,
															  const UNS_32 pmin,
															  const UNS_32 pmax,
															  const UNS_32 pdefault,
															  const BOOL_32 pgenerate_change_line_bool,
															  const UNS_32 pchange_line_index,
															  const UNS_32 preason_for_change,
															  const UNS_32 pbox_index_0,
															  const BOOL_32 pset_change_bits,
															  CHANGE_BITS_32_BIT *pchange_bits_for_controller,
															  CHANGE_BITS_32_BIT *pchange_bits_for_central,
															  UNS_32 pchange_bit_to_set,
															  const char *pfield_name
															  #ifdef _MSC_VER
															  , char *pmerge_update_str,
															  char *pmerge_insert_fields_str,
															  char *pmerge_insert_values_str
															  #endif
															);

extern BOOL_32 SHARED_set_date_with_64_bit_change_bits_group( void *const pgroup,
															  UNS_32 *pvar_to_change_ptr,
															  UNS_32 pnew_value,
															  const UNS_32 pmin,
															  const UNS_32 pmax,
															  const UNS_32 pdefault,
															  const BOOL_32 pgenerate_change_line_bool,
															  const UNS_32 pchange_line_index,
															  const UNS_32 preason_for_change,
															  const UNS_32 pbox_index_0,
															  const BOOL_32 pset_change_bits,
															  CHANGE_BITS_64_BIT *pchange_bits_for_controller,
															  CHANGE_BITS_64_BIT *pchange_bits_for_central,
															  UNS_32 pchange_bit_to_set,
															  const char *pfield_name
															  #ifdef _MSC_VER
															  , char *pmerge_update_str,
															  char *pmerge_insert_fields_str,
															  char *pmerge_insert_values_str
															  #endif
															);

extern BOOL_32 SHARED_set_time_with_64_bit_change_bits_group( void *const pgroup,
															  UNS_32 *pvar_to_change_ptr,
															  UNS_32 pnew_value,
															  const UNS_32 pmin,
															  const UNS_32 pmax,
															  const UNS_32 pdefault,
															  const BOOL_32 pgenerate_change_line_bool,
															  const UNS_32 pchange_line_index,
															  const UNS_32 preason_for_change,
															  const UNS_32 pbox_index_0,
															  const BOOL_32 pset_change_bits,
															  CHANGE_BITS_64_BIT *pchange_bits_for_controller,
															  CHANGE_BITS_64_BIT *pchange_bits_for_central,
															  UNS_32 pchange_bit_to_set,
															  const char *pfield_name
															  #ifdef _MSC_VER
															  , char *pmerge_update_str,
															  char *pmerge_insert_fields_str,
															  char *pmerge_insert_values_str
															  #endif
															);

extern BOOL_32 SHARED_set_date_time_with_64_bit_change_bits_group( void *const pgroup,
																   DATE_TIME *pvar_to_change_ptr,
																   DATE_TIME pnew_value,
																   const DATE_TIME pmin,
																   const DATE_TIME pmax,
																   const DATE_TIME pdefault,
																   const BOOL_32 pgenerate_change_line_bool,
																   const UNS_32 preason_for_change,
																   const UNS_32 pbox_index_0,
																   const BOOL_32 pset_change_bits,
																   CHANGE_BITS_64_BIT *pchange_bits_for_controller,
																   CHANGE_BITS_64_BIT *pchange_bits_for_central,
																   UNS_32 pchange_bit_to_set,
																   const char *pfield_name
																   #ifdef _MSC_VER
																   , char *pmerge_update_str,
																   char *pmerge_insert_fields_str,
																   char *pmerge_insert_values_str
																   #endif
																 );

extern BOOL_32 SHARED_set_on_at_a_time_group( void *const pgroup,
											  UNS_32 *pvar_to_change_ptr,
											  UNS_32 pnew_value,
											  const UNS_32 pmin,
											  const UNS_32 pmax,
											  const UNS_32 pdefault,
											  const BOOL_32 pgenerate_change_line_bool,
											  const UNS_32 pchange_line_index,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_64_BIT *pchange_bits_for_controller,
											  CHANGE_BITS_64_BIT *pchange_bits_for_central,
											  UNS_32 pchange_bit_to_set,
											  const char *pfield_name
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
										    );
// ----------

extern BOOL_32 SHARED_set_name_32_bit_change_bits( void *pgroup,
												   char *pName,
												   const BOOL_32 pgenerate_change_line_bool,
												   const UNS_32 preason_for_change,
												   const UNS_32 pbox_index_0,
												   const BOOL_32 pset_change_bits,
												   CHANGE_BITS_32_BIT *pchange_bits_for_controller,
												   CHANGE_BITS_32_BIT *pchange_bits_for_central,
												   const UNS_32 pchange_bit_to_set
												   #ifdef _MSC_VER
												   , const char *pfield_name,
												   char *pmerge_update_str,
												   char *pmerge_insert_fields_str,
												   char *pmerge_insert_values_str
												   #endif
												 );

extern BOOL_32 SHARED_set_name_64_bit_change_bits( void *pgroup,
												   char *pName,
												   const BOOL_32 pgenerate_change_line_bool,
												   const UNS_32 preason_for_change,
												   const UNS_32 pbox_index_0,
												   const BOOL_32 pset_change_bits,
												   CHANGE_BITS_64_BIT *pchange_bits_for_controller,
												   CHANGE_BITS_64_BIT *pchange_bits_for_central,
												   const UNS_32 pchange_bit_to_set
												   #ifdef _MSC_VER
												   , const char *pfield_name,
												   char *pmerge_update_str,
												   char *pmerge_insert_fields_str,
												   char *pmerge_insert_values_str
												   #endif
												 );

// ----------

extern BOOL_32 SHARED_set_bool_station( STATION_STRUCT *const pstation,
										BOOL_32 *pvar_to_change_ptr,
										BOOL_32 pnew_value,
										const BOOL_32 pdefault,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 pchange_line_index,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const BOOL_32 pset_change_bits,
										CHANGE_BITS_32_BIT *pchange_bits_for_controller,
										CHANGE_BITS_32_BIT *pchange_bits_for_central,
										UNS_32 pchange_bit_to_set,
										const char *pfield_name
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
									   );

extern BOOL_32 SHARED_set_uint8_station( STATION_STRUCT *const pstation,
										 UNS_8 *pvar_to_change_ptr,
										 UNS_8 pnew_value,
										 const UNS_8 pmin,
										 const UNS_8 pmax,
										 const UNS_8 pdefault,
										 const BOOL_32 pgenerate_change_line_bool,
										 const UNS_32 pchange_line_index,
										 const UNS_32 preason_for_change,
										 const UNS_32 pbox_index_0,
										 const BOOL_32 pset_change_bits,
										 CHANGE_BITS_32_BIT *pchange_bits_for_controller,
										 CHANGE_BITS_32_BIT *pchange_bits_for_central,
										 UNS_32 pchange_bit_to_set,
										 const char *pfield_name
										 #ifdef _MSC_VER
										 , char *pmerge_update_str,
										 char *pmerge_insert_fields_str,
										 char *pmerge_insert_values_str
										 #endif
									  );

extern BOOL_32 SHARED_set_uint32_station( STATION_STRUCT *const pstation,
										  UNS_32 *pvar_to_change_ptr,
										  UNS_32 pnew_value,
										  const UNS_32 pmin,
										  const UNS_32 pmax,
										  const UNS_32 pdefault,
										  const BOOL_32 pgenerate_change_line_bool,
										  const UNS_32 pchange_line_index,
										  const UNS_32 preason_for_change,
										  const UNS_32 pbox_index_0,
										  const BOOL_32 pset_change_bits,
										  CHANGE_BITS_32_BIT *pchange_bits_for_controller,
										  CHANGE_BITS_32_BIT *pchange_bits_for_central,
										  UNS_32 pchange_bit_to_set,
										  const char *pfield_name
										  #ifdef _MSC_VER
										  , char *pmerge_update_str,
										  char *pmerge_insert_fields_str,
										  char *pmerge_insert_values_str
										  #endif
										);

extern BOOL_32 SHARED_set_float_station( STATION_STRUCT *const pstation,
										 float *pvar_to_change_ptr,
										 float pnew_value,
										 const float pmin,
										 const float pmax,
										 const float pdefault,
										 const BOOL_32 pgenerate_change_line_bool,
										 const UNS_32 pchange_line_index,
										 const UNS_32 preason_for_change,
										 const UNS_32 pbox_index_0,
										 const BOOL_32 pset_change_bits,
										 CHANGE_BITS_32_BIT *pchange_bits_for_controller,
										 CHANGE_BITS_32_BIT *pchange_bits_for_central,
										 UNS_32 pchange_bit_to_set,
										 const char *pfield_name
										 #ifdef _MSC_VER
										 , char *pmerge_update_str,
										 char *pmerge_insert_fields_str,
										 char *pmerge_insert_values_str
										 #endif
									   );

extern BOOL_32 SHARED_set_GID_station( STATION_STRUCT *const pstation,
									   UNS_32 *pvar_to_change_ptr,
									   UNS_32 pnew_GID,
									   const UNS_32 plast_GID,
									   const UNS_32 preason_for_change,
									   const UNS_32 pbox_index_0,
									   const BOOL_32 pset_change_bits,
									   CHANGE_BITS_32_BIT *pchange_bits_for_controller,
									   CHANGE_BITS_32_BIT *pchange_bits_for_central,
									   UNS_32 pchange_bit_to_set,
									   const char *pfield_name
									   #ifdef _MSC_VER
									   , char *pmerge_update_str,
									   char *pmerge_insert_fields_str,
									   char *pmerge_insert_values_str
									   #endif
									 );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern BOOL_32 SHARED_get_bool( BOOL_32 *pvalue,
								const BOOL_32 pdefault,
								void (*pset_func_ptr)(BOOL_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
								CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
								const char *const pvar_name );

extern UNS_32 SHARED_get_uint32( UNS_32 *pvalue,
								 const UNS_32 pmin,
								 const UNS_32 pmax,
								 const UNS_32 pdefault,
								 void (*pset_func_ptr)(UNS_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
								 CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
								 const char *const pvar_name );

extern INT_32 SHARED_get_int32( INT_32 *pvalue,
								const INT_32 pmin,
								const INT_32 pmax,
								const INT_32 pdefault,
								void (*pset_func_ptr)(INT_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
								CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
								const char *const pvar_name );

extern BOOL_32 SHARED_get_bool_from_array( BOOL_32 *pvalue,
										   const UNS_32 pindex,
										   const UNS_32 parray_size,
										   const BOOL_32 pdefault,
										   void (*pset_func_ptr)(BOOL_32 pvalue, const UNS_32 pindex, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
										   CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
										   const char *const pvar_name );

extern UNS_32 SHARED_get_uint32_from_array( UNS_32 *pvalue,
											const UNS_32 pindex,
											const UNS_32 parray_size,
											const UNS_32 pmin,
											const UNS_32 pmax,
											const UNS_32 pdefault,
											void (*pset_func_ptr)(UNS_32 pvalue, const UNS_32 pindex, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
											CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
											const char *const pvar_name );

extern BOOL_32 SHARED_get_bool_32_bit_change_bits_group( void *const pgroup,
														 BOOL_32 *pvalue,
														 const BOOL_32 pdefault,
														 void (*pset_func_ptr)(void *const pgroup, BOOL_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
														 CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
														 const char *const pvar_name );

extern BOOL_32 SHARED_get_bool_64_bit_change_bits_group( void *const pgroup,
														 BOOL_32 *pvalue,
														 const BOOL_32 pdefault,
														 void (*pset_func_ptr)(void *const pgroup, BOOL_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_64_BIT *pchange_bitfield_to_set),
														 CHANGE_BITS_64_BIT *pchange_bitfield_to_set,
														 const char *const pvar_name );

extern UNS_32 SHARED_get_uint32_32_bit_change_bits_group( void *const pgroup,
														  UNS_32 *pvalue,
														  const UNS_32 pmin,
														  const UNS_32 pmax,
														  const UNS_32 pdefault,
														  void (*pset_func_ptr)(void *const pgroup, UNS_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
														  CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
														  const char *const pvar_name );

extern UNS_32 SHARED_get_uint32_64_bit_change_bits_group( void *const pgroup,
														  UNS_32 *pvalue,
														  const UNS_32 pmin,
														  const UNS_32 pmax,
														  const UNS_32 pdefault,
														  void (*pset_func_ptr)(void *const pgroup, UNS_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_64_BIT *pchange_bitfield_to_set),
														  CHANGE_BITS_64_BIT *pchange_bitfield_to_set,
														  const char *const pvar_name );

extern INT_32 SHARED_get_int32_32_bit_change_bits_group( void *const pgroup,
														 INT_32 *pvalue,
														 const INT_32 pmin,
														 const INT_32 pmax,
														 const INT_32 pdefault,
														 void (*pset_func_ptr)(void *const pgroup, INT_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
														 CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
														 const char *const pvar_name );

extern INT_32 SHARED_get_int32_64_bit_change_bits_group( void *const pgroup,
														 INT_32 *pvalue,
														 const INT_32 pmin,
														 const INT_32 pmax,
														 const INT_32 pdefault,
														 void (*pset_func_ptr)(void *const pgroup, INT_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_64_BIT *pchange_bitfield_to_set),
														 CHANGE_BITS_64_BIT *pchange_bitfield_to_set,
														 const char *const pvar_name );

extern float SHARED_get_float_32_bit_change_bits_group( void *const pgroup,
														float *pvalue,
														const float pmin,
														const float pmax,
														const float pdefault,
														void (*pset_func_ptr)(void *const pgroup, float pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
														CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
														const char *const pvar_name );

extern BOOL_32 SHARED_get_bool_from_array_32_bit_change_bits_group( void *const pgroup,
																	BOOL_32 *pvalue,
																	const UNS_32 pindex,
																	const UNS_32 parray_size,
																	const BOOL_32 pdefault,
																	void (*pset_func_ptr)(void *const pgroup, BOOL_32 pvalue, const UNS_32 pindex, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
																	CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
																	const char *const pvar_name );

extern BOOL_32 SHARED_get_bool_from_array_64_bit_change_bits_group( void *const pgroup,
																	BOOL_32 *pvalue,
																	const UNS_32 pindex,
																	const UNS_32 parray_size,
																	const BOOL_32 pdefault,
																	void (*pset_func_ptr)(void *const pgroup, BOOL_32 pvalue, const UNS_32 pindex, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_64_BIT *pchange_bitfield_to_set),
																	CHANGE_BITS_64_BIT *pchange_bitfield_to_set,
																	const char *const pvar_name );

extern UNS_32 SHARED_get_uint32_from_array_32_bit_change_bits_group( void *const pgroup,
																	 const UNS_32 pindex,
																	 UNS_32 *pvalue,
																	 const UNS_32 parray_size,
																	 const UNS_32 pmin,
																	 const UNS_32 pmax,
																	 const UNS_32 pdefault,
																	 void (*pset_func_ptr)(void *const pgroup, const UNS_32 pindex, UNS_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
																	 CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
																	 const char *const pvar_name );
																
extern INT_32 SHARED_get_int32_from_array_32_bit_change_bits_group( void *const pgroup,
																	 const UNS_32 pindex,
																	 INT_32 *pvalue,
																	 const UNS_32 parray_size,
																	 const UNS_32 pmin,
																	 const UNS_32 pmax,
																	 const UNS_32 pdefault,
																	 void (*pset_func_ptr)(void *const pgroup, const UNS_32 pindex, INT_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
																	 CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
																	 const char *const pvar_name );


extern UNS_32 SHARED_get_uint32_from_array_64_bit_change_bits_group( void *const pgroup,
																	 UNS_32 *pvalue,
																	 const UNS_32 pindex,
																	 const UNS_32 parray_size,
																	 const UNS_32 pmin,
																	 const UNS_32 pmax,
																	 const UNS_32 pdefault,
																	 void (*pset_func_ptr)(void *const pgroup, UNS_32 pvalue, const UNS_32 pindex, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_64_BIT *pchange_bitfield_to_set),
																	 CHANGE_BITS_64_BIT *pchange_bitfield_to_set,
																	 const char *const pvar_name );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

