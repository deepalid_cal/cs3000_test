/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_SHARED_STATIONS_C
#define _INC_SHARED_STATIONS_C

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs3000_tpmicro_common.h"

#include	"group_base_file.h"

#include	"bithacks.h"

#include	"change.h"

#include	"shared.h"

#ifndef	_MSC_VER

	#include	"semphr.h"

	#include	"alerts.h"

	#include	"app_startup.h"
	
	#include	"cs_mem.h"

	#include	"battery_backed_vars.h"

#else

	#include	<stdlib.h>

	#include	"sql_merge.h"

#endif




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define STATION_CHANGE_BITFIELD_description_bit							(0)
#define	STATION_CHANGE_BITFIELD_physically_available					(1)
#define	STATION_CHANGE_BITFIELD_in_use_bit								(2)
#define	STATION_CHANGE_BITFIELD_station_number_bit						(3)

#define	STATION_CHANGE_BITFIELD_box_index_bit							(4)
#define	STATION_CHANGE_BITFIELD_decoder_serial_number_bit				(5)
#define	STATION_CHANGE_BITFIELD_decoder_output_bit						(6)
#define	STATION_CHANGE_BITFIELD_total_run_minutes_bit					(7)

#define	STATION_CHANGE_BITFIELD_cycle_minutes_bit						(8)
#define	STATION_CHANGE_BITFIELD_soak_minutes_bit						(9)
#define	STATION_CHANGE_BITFIELD_et_factor_bit							(10)
#define	STATION_CHANGE_BITFIELD_expected_flow_rate_bit					(11)

#define	STATION_CHANGE_BITFIELD_distribution_uniformity_bit				(12)
#define	STATION_CHANGE_BITFIELD_no_water_days_bit						(13)

// 7/20/2015 rmd : Was manual program run times. Now no longer used.
#define	STATION_CHANGE_BITFIELD_available_01							(14)
#define	STATION_CHANGE_BITFIELD_available_02							(15)

// 9/23/2015 ajv : Was irrigation system GID
#define	STATION_CHANGE_BITFIELD_available_03							(16)
#define	STATION_CHANGE_BITFIELD_GID_station_group_bit					(17)
#define	STATION_CHANGE_BITFIELD_GID_manual_program_A					(18)
#define	STATION_CHANGE_BITFIELD_GID_manual_program_B					(19)

#define	STATION_CHANGE_BITFIELD_moisture_balance_bit					(20)
#define	STATION_CHANGE_BITFIELD_square_footage_bit						(21)
#define	STATION_CHANGE_BITFIELD_ignore_moisture_balance_flag			(22)

// ----------

static char* const STATION_database_field_names[ (STATION_CHANGE_BITFIELD_ignore_moisture_balance_flag + 1) ] =
{
	"Name",
	// 4/21/2015 ajv : CardConnected stores the PhysicallyAvailable value.
	"CardConnected",
	"InUse",
	"StationNumber",
	"BoxIndex",
	"DecoderSerialNumber",
	"DecoderOutput",
	"TotalMinutesRun",
	"CycleMinutes",
	"SoakMinutes",
	"ET_Factor",
	"ExpectedFlowRate",
	"DistributionUniformity",
	"NoWaterDays",
	"",
	"",
	"",
	"StationGroupID",
	"ManualProgramA_ID",
	"ManualProgramB_ID",
	"MoistureBalance",
	"SquareFootage",
	"ClearAccumulatedRainFlag"
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * The definition of the station structure. If the contents of this structure 
 * are changed or reordered, you <b>MUST</b> update the 
 * STATION_INFO_LATEST_FILE_VERSION located in this structure's file. 
 * 
 * @revisions
 *    2/10/2012  Updated variables to be 32-bit unsigned and BOOL_32
 * @revisions
 *    4/17/2014  Removed the two indications if a station hardware is installed. Replaced with physically_available.
 * @revisions
 *    4/18/2014  Removed serial number. Replaced with box index.
 */
struct STATION_STRUCT
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	// ----------

	// 4/17/2014 rmd : This variable indicates if the station card & terminal are physically
	// present. If one or the other or both are missing this is false. The user cannot set this
	// variable. It is automatically managed based upon the various conditions (decoder present,
	// decoder operative, terminal and card present, etc...) When set false station is not
	// available to the user.
	//
	// ALSO set false if the controllers index is not found in the chain following a scan in the
	// 'hide hardware' function.
	BOOL_32				physically_available;
	
	// ----------

	// 4/17/2014 rmd : This is the variable the user sets. To hide stations that he knows he
	// won't be using even though they are physically available.
	BOOL_32				in_use_bool;

	// ----------
	
	// 4/17/2014 rmd : 2011.09.22 rmd : Indicating 0-based. Do not change the naming convention
	// or switch to a 1-based value! The station preserves array indexing function counts on this.
	UNS_32				station_number_0;

	// ----------
	
	// 4/17/2014 rmd : For sometime we used the box serial number to tie a station to a box in
	// the chain. This proved somewhat cumbersome during chain building and modification
	// activites. I may have been able to have been worked out but using the box index is more
	// direct and allows clear hiding of stations using the scan results - regardless of the
	// serial number of the panels being used. Using a box index is also a great aid when
	// SWAPPING panels as the stations belong to a box with that index. Not a box with a
	// specific serial number. This is 0 based.
	//
	// 4/18/2014 rmd : DO NOT change the size of this without reworking the way changes are
	// transmitted. The sizeof this variable is used on one side. And the size of UNS_32 is used
	// on the other (by necessity it seems). So beware!
	UNS_32				box_index_0;
	
	// ----------
	
	// The serial number of the 2-Wire decoder this station is assigned to. If
	// the serial number is 0, the station connected to a conventional wire
	// terminal.
	UNS_32				decoder_serial_number;

	// The decoder output (A or B) the station is physically connected to.
	UNS_32				decoder_output;

	UNS_32				total_run_minutes_10u;
	UNS_32				cycle_minutes_10u;
	UNS_32				soak_minutes;
	UNS_32				et_factor_100u;

	UNS_32				expected_flow_rate_gpm;

	UNS_32				distribution_uniformity_100u;

	// 10/2/2012 rmd : This would be the number directly edited on the station information
	// screen. How are other forms of NOW implemented?
	UNS_32				no_water_days;

	// 7/20/2015 rmd : The following 8 bytes used to be the run time for MANUAL PROGRAM. It was
	// to support if we wanted each station in a manual program to have its own run length. But
	// we are not doing that now. The run time is associated with the manual program itself. And
	// is global to the program for all station within a particular manual program.
	UNS_32				no_longer_used_01;				// manual_program_A_seconds;
	UNS_32				no_longer_used_02;				// manual_program_B_seconds;
	
	// ----------

	UNS_32				no_longer_used_03;				// GID_irrigation_system;
	UNS_32				GID_station_group;

	// 10/2/2012 rmd : Each station is allowed to belong to two different Manual Programs. The
	// gui will be responsible for recognizing a given station already is a member of two other
	// manual program groups.
	UNS_32				GID_manual_program_A;
	UNS_32				GID_manual_program_B;

	// ----------

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

	// ----------

	// 9/15/2015 rmd : Added in REV 2 as an UNS_32. And then converted to a float in REV 4.
	float				moisture_balance_percent;

	// ----------

	// 4/30/2015 ajv : Added in Rev 3
	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

	// ----------

	// 7/8/2015 ajv : Added in Rev 4
	UNS_32				square_footage;

	// ----------

	// 10/12/2015 rmd : Added in REV 5.
	
	// 4/18/2016 ajv : Introduced this flag to initialize the moisture balance back to 50% at
	// the conclusion of the next irrigation. This can be used to correct the MB if a bug
	// results in unexpected values.
	BOOL_32		ignore_moisture_balance_next_irrigation;

	// 5/5/2016 skc : No longer used budget variables
	UNS_16		no_longer_used_05;
	
	UNS_8		no_longer_used_06;

	// ----------
	 
	// 4/18/2016 ajv: Added in Rev 6
	// 
	// 4/7/2017 ajv : Moved to 32-bit variable above.
	UNS_8		no_longer_used_ignore_moisture_balance_next_irrigation;
	
	// ----------
	
	// 5/7/2015 RMD : NOTE ----> DO NOT FORGET TO VISIT THE MEM PARTITION SCHEME TO SEE IF YOU
	// NEED TO INCREASE THE SIZE OF THE SECOND PARTITION. IT IS SPECIFICALLY CRAFTED TO HOLD THE
	// STATION STRUCT LIST ITEMS. AND MUST HAVE MARGIN UP AND ABOVE THIS STRUCTURE SIZE. EACH
	// PARTITION REQUIRES 28 BYTES FOR THE PARTITION SCHEME OVERHEAD. AND I LIKE SOME MARGIN
	// ABOVE THAT. SO WE WOULD MAKE FOR EXAMPLE 168 + 28 = 196 PLUS 12 BYTES MARGIN = 208 BYTES.
	// PROBABLY AN 8 BYTE MARGIN WOULD SUFFICE.
	// 
	// 7/8/2015 ajv : We've added square footage without increasing this pool. Therefore, our
	// margin is now 8-bytes instead of 12... 172 + 28 = 200 PLUS 8 BYTES MARGIN = 208 BYTES.
	//
	// 10/12/2015 rmd : And now I added 4 more bytes to the size of the structure for BUDGETS.
	// This reduces the margin to 4 bytes: 176 + 28 + 4 = 208 byte partition size.
	
	// ----------
	
	// 2011.10.03 rmd : Presently at 160 bytes. Pretty darn large. Be aware the mem_partitioning
	// sub-system is crafted to hold 768 individual copies of this structure. That is how big
	// the list could grow to in a full network. That IS our station count limit! If the size of
	// this structure is changed one may need to change the memory partition table to
	// accomodate. Though there is some margin there.
	// 
	// 2012.02.10 ajv : Now at 272 bytes after changing to 32-bit unsigned and _Bools.
	// 
	// 2012.05.25 ajv : Now at 420 bytes after adding support for tracking changes.
	//
	// 10/24/2012 rmd : Not sure how this decreased in size (from 420 bytes) ... but now at 332
	// bytes. I let the compiler tell me the size of course. I will go refigure the mem
	// partition needs!
	//
	// 3/20/2015 ajv : We're now down to 164 bytes. The significant drop has to do with how
	// change bits are managed - before we had a 32-bit value for each variable - now we have
	// there 32-bit change bit fields.
	//
	// 5/7/2015 rmd : With the addition of the moisture balance UNS_32 and the changes_uploaded
	// UNS_32 we are back at 168.
	// 
	// 7/8/2015 ajv : Now that we've added the square fotage, we're at 172.
	//
	// 10/12/2015 rmd : Now that we've added the budget support we are at 176.
	
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void nm_STATION_set_description( STATION_STRUCT *const pstation,
										char *pdescription,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const BOOL_32 pset_change_bits,
										CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
									  )
{
	// 5/12/2014 ajv : The GROUP_set_name routine generates a change line using
	// the Alert_ChangeLine_Group routine. However, we want the station
	// description to use the Alert_ChangeLine_Station routine so pass in
	// (false) and, after we set the name, generate the appropriate change line.
	if( SHARED_set_name_32_bit_change_bits( pstation,
											pdescription,
											CHANGE_do_not_generate_change_line,
											preason_for_change,
											pbox_index_0,
											pset_change_bits,
											pchange_bitfield_to_set,
											&pstation->changes_to_upload_to_comm_server,
											STATION_CHANGE_BITFIELD_description_bit
										    #ifdef _MSC_VER
											, STATION_database_field_names[ STATION_CHANGE_BITFIELD_description_bit ],
											pmerge_update_str,
										    pmerge_insert_fields_str,
										    pmerge_insert_values_str
										    #endif
										   ) == (true) )
	{
		#ifndef	_MSC_VER

		if ( pgenerate_change_line_bool == (true) )
		{
			Alert_ChangeLine_Station( CHANGE_STATION_DESCRIPTION, pstation->box_index_0, pstation->station_number_0, 0, 0, NUMBER_OF_CHARS_IN_A_NAME, preason_for_change, pbox_index_0 );
		}

		#endif
	}
}

/* ---------------------------------------------------------- */
extern void nm_STATION_set_physically_available( void *const pstation,
												 BOOL_32 pphysically_available,
												 const BOOL_32 pgenerate_change_line_bool,
												 const UNS_32 preason_for_change,
												 const UNS_32 pbox_index_0,
												 const BOOL_32 pset_change_bits,
												 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												 #ifdef _MSC_VER
												 , char *pmerge_update_str,
												 char *pmerge_insert_fields_str,
												 char *pmerge_insert_values_str
												 #endif
												)
{
	SHARED_set_bool_station( pstation,
							 &(((STATION_STRUCT*)pstation)->physically_available),
							 pphysically_available,
							 (false),
							 pgenerate_change_line_bool,
							 CHANGE_STATION_PHYSICALLY_AVAILABLE,
							 preason_for_change,
							 pbox_index_0,
							 pset_change_bits,
							 pchange_bitfield_to_set,
							 &(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
							 STATION_CHANGE_BITFIELD_physically_available,
							 STATION_database_field_names[ STATION_CHANGE_BITFIELD_physically_available ]
							 #ifdef _MSC_VER
							 , pmerge_update_str,
							 pmerge_insert_fields_str,
							 pmerge_insert_values_str
							 #endif
						   );
}

/* ---------------------------------------------------------- */
/**
 * Sets whether a particular station is in use or not. If the passed in station 
 * is NULL, an error is raised and a 0 is returned. Additionally, if the 
 * parameter that's passed in is out-of-range, an error is raised and the value 
 * isn't updated. 
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being changed isn't deleted
 *  	  during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param pstation A pointer to the station to update.
 * @param pin_use_bool True if the station is in use otherwise, false.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    4/6/2012  Added setting of change bits to distribute changes
 */
extern void nm_STATION_set_in_use( void *const pstation,
								   BOOL_32 pin_use_bool,
								   const BOOL_32 pgenerate_change_line_bool,
								   const UNS_32 preason_for_change,
								   const UNS_32 pbox_index_0,
								   const BOOL_32 pset_change_bits,
								   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
								   #ifdef _MSC_VER
								   , char *pmerge_update_str,
								   char *pmerge_insert_fields_str,
								   char *pmerge_insert_values_str
								   #endif
								  )
{
	SHARED_set_bool_station( pstation,
							 &(((STATION_STRUCT*)pstation)->in_use_bool),
							 pin_use_bool,
							 STATION_INUSE_DEFAULT,
							 pgenerate_change_line_bool,
							 CHANGE_STATION_IN_USE,
							 preason_for_change,
							 pbox_index_0,
							 pset_change_bits,
							 pchange_bitfield_to_set,
							 &(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
							 STATION_CHANGE_BITFIELD_in_use_bit,
							 STATION_database_field_names[ STATION_CHANGE_BITFIELD_in_use_bit ]
							 #ifdef _MSC_VER
							 , pmerge_update_str,
							 pmerge_insert_fields_str,
							 pmerge_insert_values_str
							 #endif
							);
}

/* ---------------------------------------------------------- */
/**
 * Sets the station number for a particular station. If the passed in station is
 * NULL, an error is raised and a 0 is returned. Additionally, if the parameter 
 * that's passed in is out-of-range, an error is raised and the value isn't 
 * updated. 
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being changed isn't deleted
 *  	  during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param pstation A pointer to the station to update.
 * @param pstation_number_0 The 0-based station number to assign to the station.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    4/6/2012  Added setting of change bits to distribute changes
 */
static void nm_STATION_set_station_number( void *const pstation,
										   UNS_32 pstation_number_0,
										   const BOOL_32 pgenerate_change_line_bool,
										   const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const BOOL_32 pset_change_bits,
										   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , char *pmerge_update_str,
										   char *pmerge_insert_fields_str,
										   char *pmerge_insert_values_str
										   #endif
										 )
{
	SHARED_set_uint32_station( pstation,
							   &(((STATION_STRUCT*)pstation)->station_number_0),
							   pstation_number_0,
							   0,
							   (MAX_STATIONS_PER_CONTROLLER - 1),
							   0,
							   pgenerate_change_line_bool,
							   CHANGE_STATION_NUMBER,
							   preason_for_change,
							   pbox_index_0,
							   pset_change_bits,
							   pchange_bitfield_to_set,
							   &(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
							   STATION_CHANGE_BITFIELD_station_number_bit,
							   STATION_database_field_names[ STATION_CHANGE_BITFIELD_station_number_bit ]
							   #ifdef _MSC_VER
							   , pmerge_update_str,
							   pmerge_insert_fields_str,
							   pmerge_insert_values_str
							   #endif
							 );
}

/* ---------------------------------------------------------- */
/**
 * Sets the controller serial number for a particular station. If the passed in
 * station is NULL, an error is raised and a 0 is returned. Additionally, if the
 * parameter that's passed in is out-of-range, an error is raised and the value
 * isn't updated.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being changed isn't deleted
 *  	  during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param pstation A pointer to the station to update.
 * @param pserial_number The controller serial number to assign to the station.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    4/6/2012  Added setting of change bits to distribute changes
 */
static void nm_STATION_set_box_index( void *const pstation,
									  UNS_32 pbox_index,
									  const BOOL_32 pgenerate_change_line_bool,
									  const UNS_32 preason_for_change,
									  const UNS_32 pbox_index_0,
									  const BOOL_32 pset_change_bits,
									  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
									  #ifdef _MSC_VER
									  , char *pmerge_update_str,
									  char *pmerge_insert_fields_str,
									  char *pmerge_insert_values_str
									  #endif
									)
{
	SHARED_set_uint32_station( pstation,
							   &(((STATION_STRUCT*)pstation)->box_index_0),
							   pbox_index,
							   BOX_INDEX_MINIMUM,
							   BOX_INDEX_MAXIMUM,
							   BOX_INDEX_DEFAULT,
							   pgenerate_change_line_bool,
							   CHANGE_STATION_BOX_INDEX,
							   preason_for_change,
							   pbox_index_0,
							   pset_change_bits,
							   pchange_bitfield_to_set,
							   &(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
							   STATION_CHANGE_BITFIELD_box_index_bit,
							   STATION_database_field_names[ STATION_CHANGE_BITFIELD_box_index_bit ]
							   #ifdef _MSC_VER
							   , pmerge_update_str,
							   pmerge_insert_fields_str,
							   pmerge_insert_values_str
							   #endif
							 );
}

/* ---------------------------------------------------------- */
extern void nm_STATION_set_decoder_serial_number( void *const pstation,
												  UNS_32 pdecoder_serial_number,
												  const BOOL_32 pgenerate_change_line_bool,
												  const UNS_32 preason_for_change,
												  const UNS_32 pbox_index_0,
												  const BOOL_32 pset_change_bits,
												  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , char *pmerge_update_str,
												  char *pmerge_insert_fields_str,
												  char *pmerge_insert_values_str
												  #endif
												)
{
	SHARED_set_uint32_station( pstation,
							   &(((STATION_STRUCT*)pstation)->decoder_serial_number),
							   pdecoder_serial_number,
							   DECODER_SERIAL_NUM_MIN,
							   DECODER_SERIAL_NUM_MAX,
							   DECODER_SERIAL_NUM_DEFAULT,
							   pgenerate_change_line_bool,
							   CHANGE_STATION_DECODER_SERIAL_NUMBER,
							   preason_for_change,
							   pbox_index_0,
							   pset_change_bits,
							   pchange_bitfield_to_set,
							   &(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
							   STATION_CHANGE_BITFIELD_decoder_serial_number_bit,
							   STATION_database_field_names[ STATION_CHANGE_BITFIELD_decoder_serial_number_bit ]
							   #ifdef _MSC_VER
							   , pmerge_update_str,
							   pmerge_insert_fields_str,
							   pmerge_insert_values_str
							   #endif
							 );
}

extern void nm_STATION_set_decoder_output( void *const pstation,
										   UNS_32 pdecoder_output,
										   const BOOL_32 pgenerate_change_line_bool,
										   const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const BOOL_32 pset_change_bits,
										   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , char *pmerge_update_str,
										   char *pmerge_insert_fields_str,
										   char *pmerge_insert_values_str
										   #endif
										 )
{
	SHARED_set_uint32_station( pstation,
							   &(((STATION_STRUCT*)pstation)->decoder_output),
							   pdecoder_output,
							   DECODER_OUTPUT_A_BLACK,
							   DECODER_OUTPUT_B_ORANGE,
							   DECODER_OUTPUT_A_BLACK,
							   pgenerate_change_line_bool,
							   CHANGE_STATION_DECODER_OUTPUT,
							   preason_for_change,
							   pbox_index_0,
							   pset_change_bits,
							   pchange_bitfield_to_set,
							   &(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
							   STATION_CHANGE_BITFIELD_decoder_output_bit,
							   STATION_database_field_names[ STATION_CHANGE_BITFIELD_decoder_output_bit ]
							   #ifdef _MSC_VER
							   , pmerge_update_str,
							   pmerge_insert_fields_str,
							   pmerge_insert_values_str
							   #endif
							 );
}

/* ---------------------------------------------------------- */
/**
 * Sets the total run time, in minutes, for a particular station. If the passed
 * in station is NULL, an error is raised and a 0 is returned. Additionally, if
 * the parameter that's passed in is out-of-range, an error is raised and the
 * value isn't updated.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being changed isn't deleted
 *  	  during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param pstation A pointer to the station to update.
 * @param ptotal_run_minutes_10u The total run time, in minutes, to assign to
 *                               the station.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    4/6/2012  Added setting of change bits to distribute changes
 */
extern void nm_STATION_set_total_run_minutes( void *const pstation,
											  UNS_32 ptotal_run_minutes_10u,
											  const BOOL_32 pgenerate_change_line_bool,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
											)
{
	SHARED_set_uint32_station( pstation,
							   &(((STATION_STRUCT*)pstation)->total_run_minutes_10u),
							   ptotal_run_minutes_10u,
							   STATION_TOTAL_RUN_MINUTES_MIN_10u,
							   STATION_TOTAL_RUN_MINUTES_MAX_10u,
							   STATION_TOTAL_RUN_MINUTES_DEFAULT_10u,
							   pgenerate_change_line_bool,
							   CHANGE_STATION_TOTAL_RUN_MINUTES,
							   preason_for_change,
							   pbox_index_0,
							   pset_change_bits,
							   pchange_bitfield_to_set,
							   &(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
							   STATION_CHANGE_BITFIELD_total_run_minutes_bit,
							   STATION_database_field_names[ STATION_CHANGE_BITFIELD_total_run_minutes_bit ]
							   #ifdef _MSC_VER
							   , pmerge_update_str,
							   pmerge_insert_fields_str,
							   pmerge_insert_values_str
							   #endif
							 );
}

/* ---------------------------------------------------------- */
/**
 * Sets the cycle time, in minutes, for a particular station. If the passed in
 * station is NULL, an error is raised and a 0 is returned. Additionally, if the
 * parameter that's passed in is out-of-range, an error is raised and the value
 * isn't updated.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being changed isn't deleted
 *  	  during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param pstation A pointer to the station to update.
 * @param pcycle_time_minutes_10u The cycle time, in minutes, to assign to the
 *                                station.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    4/6/2012  Added setting of change bits to distribute changes
 */
extern void nm_STATION_set_cycle_minutes( void *const pstation,
										  UNS_32 pcycle_time_minutes_10u,
										  const BOOL_32 pgenerate_change_line_bool,
										  const UNS_32 preason_for_change,
										  const UNS_32 pbox_index_0,
										  const BOOL_32 pset_change_bits,
										  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										  #ifdef _MSC_VER
										  , char *pmerge_update_str,
										  char *pmerge_insert_fields_str,
										  char *pmerge_insert_values_str
										  #endif
										)
{
	UNS_32	lmax_cycle_time_10u;

	// 8/26/2015 ajv : Since the ParserDLL doesn't have access to the function necessary to
	// determine whether we're in Daily ET or the function get the WaterSense max cycle time,
	// just use the fixed max for now. We should consider how to handle this in the future,
	// though. TODO
	#ifndef _MSC_VER

		// 6/25/2015 ajv : If we're in ET, the maximum cycle time is determined by the WaterSense
		// calculation, so set it accordingly. Otherwise, use our traditional default.
		if( WEATHER_get_station_uses_daily_et(pstation) )
		{
			lmax_cycle_time_10u = nm_STATION_get_watersense_cycle_max_10u( pstation );
		}
		else
		{
			lmax_cycle_time_10u = STATION_CYCLE_MINUTES_MAX_10u;
		}

		// ----------

		// 9/12/2017 rmd : Special range check. Because the max cycle time is CALCULATED using
		// floating point math and quite a few different values here in the controller, AND
		// independently calculated by Wissam in the CCO, there is a slight chance the user could
		// set his cycle time greater in CCO than what is allowed in the controller, and the range
		// check function here in the controller fails and sets the cycle time to the default of 5
		// min. We thought it better to catch that and set to the controllers calculated max
		// instead, for that case the value is greater than the max.
		if (pcycle_time_minutes_10u > lmax_cycle_time_10u)
		{
			Alert_Message_va("CYCLE TIME: special range - from %u to %u", pcycle_time_minutes_10u, lmax_cycle_time_10u);

			pcycle_time_minutes_10u = lmax_cycle_time_10u;
		}

	#else

		lmax_cycle_time_10u = STATION_CYCLE_MINUTES_MAX_10u;

	#endif
	
	// ----------
	
	SHARED_set_uint32_station( pstation,
							   &(((STATION_STRUCT*)pstation)->cycle_minutes_10u),
							   pcycle_time_minutes_10u,
							   STATION_CYCLE_MINUTES_MIN_10u,
							   lmax_cycle_time_10u,
							   STATION_CYCLE_MINUTES_DEFAULT_10u,
							   pgenerate_change_line_bool,
							   CHANGE_STATION_CYCLE_MINUTES,
							   preason_for_change,
							   pbox_index_0,
							   pset_change_bits,
							   pchange_bitfield_to_set,
							   &(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
							   STATION_CHANGE_BITFIELD_cycle_minutes_bit,
							   STATION_database_field_names[ STATION_CHANGE_BITFIELD_cycle_minutes_bit ]
							   #ifdef _MSC_VER
							   , pmerge_update_str,
							   pmerge_insert_fields_str,
							   pmerge_insert_values_str
							   #endif
							 );
}

/* ---------------------------------------------------------- */
/**
 * Sets the soak-in time, in minutes, for a particular station. If the passed in
 * station is NULL, an error is raised and a 0 is returned. Additionally, if the
 * parameter that's passed in is out-of-range, an error is raised and the value
 * isn't updated.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being changed isn't deleted
 *  	  during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param pstation A pointer to the station to update.
 * @param psoak_minutes The soak-in time, in minutes, to assign to the station.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    4/6/2012  Added setting of change bits to distribute changes
 */
extern void nm_STATION_set_soak_minutes( void *const pstation,
										 UNS_32 psoak_minutes,
										 const BOOL_32 pgenerate_change_line_bool,
										 const UNS_32 preason_for_change,
										 const UNS_32 pbox_index_0,
										 const BOOL_32 pset_change_bits,
										 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										 #ifdef _MSC_VER
										 , char *pmerge_update_str,
										 char *pmerge_insert_fields_str,
										 char *pmerge_insert_values_str
										 #endif
									   )
{
	UNS_32	lmin_soak_time;

	// 8/26/2015 ajv : Since the ParserDLL doesn't have access to the function necessary to
	// determine whether we're in Daily ET or the function get the WaterSense max cycle time,
	// just use the fixed max for now. We should consider how to handle this in the future,
	// though. TODO

	#ifndef _MSC_VER

		// 6/25/2015 ajv : If we're in ET, the minimum soak time is determined by the WaterSense
		// calculation, so set it accordingly. Otherwise, use our traditional default.
		if( WEATHER_get_station_uses_daily_et(pstation) )
		{
			lmin_soak_time = nm_STATION_get_watersense_soak_min( pstation );
		}
		else
		{
			lmin_soak_time = STATION_SOAK_MINUTES_MIN;
		}

	#else

		lmin_soak_time = STATION_SOAK_MINUTES_MIN;

	#endif

	SHARED_set_uint32_station( pstation,
							   &(((STATION_STRUCT*)pstation)->soak_minutes),
							   psoak_minutes,
							   lmin_soak_time,
							   STATION_SOAK_MINUTES_MAX,
							   STATION_SOAK_MINUTES_DEFAULT,
							   pgenerate_change_line_bool,
							   CHANGE_STATION_SOAK_MINUTES,
							   preason_for_change,
							   pbox_index_0,
							   pset_change_bits,
							   pchange_bitfield_to_set,
							   &(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
							   STATION_CHANGE_BITFIELD_soak_minutes_bit,
							   STATION_database_field_names[ STATION_CHANGE_BITFIELD_soak_minutes_bit ]
							   #ifdef _MSC_VER
							   , pmerge_update_str,
							   pmerge_insert_fields_str,
							   pmerge_insert_values_str
							   #endif
							 );
}

/* ---------------------------------------------------------- */
/**
 * Sets the ET factor (aka percent of ET) for a particular station. If the
 * passed in station is NULL, an error is raised and a 0 is returned.
 * Additionally, if the parameter that's passed in is out-of-range, an error is
 * raised and the value isn't updated.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being changed isn't deleted
 *  	  during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param pstation A pointer to the station to update.
 * @param pet_factor_100u The ET factor to assign to the station.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    4/6/2012  Added setting of change bits to distribute changes
 */
static void nm_STATION_set_ET_factor( void *const pstation,
									  UNS_32 pet_factor_100u,
									  const BOOL_32 pgenerate_change_line_bool,
									  const UNS_32 preason_for_change,
									  const UNS_32 pbox_index_0,
									  const BOOL_32 pset_change_bits,
									  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
									  #ifdef _MSC_VER
									  , char *pmerge_update_str,
									  char *pmerge_insert_fields_str,
									  char *pmerge_insert_values_str
									  #endif
									)
{
	SHARED_set_uint32_station( pstation,
							   &(((STATION_STRUCT*)pstation)->et_factor_100u),
							   pet_factor_100u,
							   STATION_ET_FACTOR_MIN,
							   STATION_ET_FACTOR_MAX,
							   STATION_ET_FACTOR_DEFAULT,
							   pgenerate_change_line_bool,
							   CHANGE_STATION_ET_FACTOR,
							   preason_for_change,
							   pbox_index_0,
							   pset_change_bits,
							   pchange_bitfield_to_set,
							   &(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
							   STATION_CHANGE_BITFIELD_et_factor_bit,
							   STATION_database_field_names[ STATION_CHANGE_BITFIELD_et_factor_bit ]
							   #ifdef _MSC_VER
							   , pmerge_update_str,
							   pmerge_insert_fields_str,
							   pmerge_insert_values_str
							   #endif
							 );
}

/* ---------------------------------------------------------- */
/**
 * Sets the expected flow rate for a particular station. If the passed in
 * station is NULL, an error is raised and a 0 is returned. Additionally, if the
 * parameter that's passed in is out-of-range, an error is raised and the value
 * isn't updated.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being changed isn't deleted
 *  	  during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param pstation A pointer to the station to update.
 * @param pexpected_flow_rate_gpm The expected flow rate to assign to the
 *                                station.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    4/6/2012  Added setting of change bits to distribute changes
 */
extern void nm_STATION_set_expected_flow_rate( void *const pstation,
											   UNS_32 pexpected_flow_rate_gpm,
											   const BOOL_32 pgenerate_change_line_bool,
											   const UNS_32 preason_for_change,
											   const UNS_32 pbox_index_0,
											   const BOOL_32 pset_change_bits,
											   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											   #ifdef _MSC_VER
											   , char *pmerge_update_str,
											   char *pmerge_insert_fields_str,
											   char *pmerge_insert_values_str
											   #endif
											 )
{
	if( SHARED_set_uint32_station( pstation,
								   &(((STATION_STRUCT*)pstation)->expected_flow_rate_gpm),
								   pexpected_flow_rate_gpm,
								   STATION_EXPECTED_FLOW_RATE_MIN,
								   STATION_EXPECTED_FLOW_RATE_MAX,
								   STATION_EXPECTED_FLOW_RATE_DEFAULT,
								   pgenerate_change_line_bool,
								   CHANGE_STATION_EXPECTED_FLOW_RATE,
								   preason_for_change,
								   pbox_index_0,
								   pset_change_bits,
								   pchange_bitfield_to_set,
								   &(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
								   STATION_CHANGE_BITFIELD_expected_flow_rate_bit,
								   STATION_database_field_names[ STATION_CHANGE_BITFIELD_expected_flow_rate_bit ]
								   #ifdef _MSC_VER
								   , pmerge_update_str,
								   pmerge_insert_fields_str,
								   pmerge_insert_values_str
								   #endif
								 ) == (true) )
	{
		#ifndef	_MSC_VER

		// 6/25/2012 rmd : And we need to 0 the station count. To force the derate table to re-learn
		// for this new expected flow rate.

		// 8/15/2012 ajv : However, if we're initializing the file for the very
		// first time, the station preserves aren't synced yet. Therefore, only
		// perform this if we're NOT initializing for the first time. Otherwise,
		// we'll receive a number of "item not on list" alerts.
		//
		// 7/17/2015 rmd : Apparently the CHANGE_REASON_DONT_SHOW_A_REASON reason is only used when
		// setting the defaults. Which was the intent to not update the station_preserves when we
		// were initially building a station - adding it to the list of stations. However I no
		// longer believe the cited reason above about seeing "item not on list" alerts. That
		// doesn't make sense to me as the station preserves records for ALL stations ALWAYS exists.
		// Anyway it's almost no code (the issue is the complication for the reader) and likely
		// doesn't hurt anything so I'll leave it alone.
		if( preason_for_change != CHANGE_REASON_DONT_SHOW_A_REASON )
		{
			// 6/13/2012 rmd : As we are changing the station preserves.
			xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

			UNS_32	station_preserves_index;

			if( STATION_PRESERVES_get_index_using_ptr_to_station_struct( pstation, &station_preserves_index ) == (true) )
			{
				station_preserves.sps[ station_preserves_index ].spbf.flow_check_station_cycles_count = 0;
			}

			xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
		}

		#endif	// _MSC_VER
	}
}

/* ---------------------------------------------------------- */
/**
 * Sets the distribution uniformity for a particular station. If the passed in
 * station is NULL, an error is raised and a 0 is returned. Additionally, if the
 * parameter that's passed in is out-of-range, an error is raised and the value
 * isn't updated.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being changed isn't deleted
 *  	  during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param pstation A pointer to the station to update.
 * @param pdistribution_uniformity_100u The distribution uniformity to assign to
 *  									the station.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    4/6/2012  Added setting of change bits to distribute changes
 */
static void nm_STATION_set_distribution_uniformity( void *const pstation,
													UNS_32 pdistribution_uniformity_100u,
													const BOOL_32 pgenerate_change_line_bool,
													const UNS_32 preason_for_change,
													const UNS_32 pbox_index_0,
													const BOOL_32 pset_change_bits,
													CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													#ifdef _MSC_VER
													, char *pmerge_update_str,
													char *pmerge_insert_fields_str,
													char *pmerge_insert_values_str
													#endif
												  )
{
	SHARED_set_uint32_station( pstation,
							   &(((STATION_STRUCT*)pstation)->distribution_uniformity_100u),
							   pdistribution_uniformity_100u,
							   STATION_DU_MIN,
							   STATION_DU_MAX,
							   STATION_DU_DEFAULT,
							   pgenerate_change_line_bool,
							   CHANGE_STATION_DISTRIBUTION_UNIFORMITY,
							   preason_for_change,
							   pbox_index_0,
							   pset_change_bits,
							   pchange_bitfield_to_set,
							   &(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
							   STATION_CHANGE_BITFIELD_distribution_uniformity_bit,
							   STATION_database_field_names[ STATION_CHANGE_BITFIELD_distribution_uniformity_bit ]
							   #ifdef _MSC_VER
							   , pmerge_update_str,
							   pmerge_insert_fields_str,
							   pmerge_insert_values_str
							   #endif
							 );
}

/* ---------------------------------------------------------- */
/**
 * Sets the no water days for a particular station. If the passed in station is
 * NULL, an error is raised and a 0 is returned. Additionally, if the parameter
 * that's passed in is out-of-range, an error is raised and the value isn't
 * updated.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the station whose value is being changed isn't deleted
 *  	  during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param pstation A pointer to the station to update.
 * @param pno_water_days The no water days to assign to the station.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    4/6/2012  Added setting of change bits to distribute changes
 */
extern void nm_STATION_set_no_water_days( void *const pstation,
										  UNS_32 pno_water_days,
										  const BOOL_32 pgenerate_change_line_bool,
										  const UNS_32 preason_for_change,
										  const UNS_32 pbox_index_0,
										  const BOOL_32 pset_change_bits,
										  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										  #ifdef _MSC_VER
										  , char *pmerge_update_str,
										  char *pmerge_insert_fields_str,
										  char *pmerge_insert_values_str
										  #endif
										)
{
	SHARED_set_uint32_station( pstation,
							   &(((STATION_STRUCT*)pstation)->no_water_days),
							   pno_water_days,
							   STATION_NO_WATER_DAYS_MIN,
							   STATION_NO_WATER_DAYS_MAX,
							   STATION_NO_WATER_DAYS_DEFAULT,
							   pgenerate_change_line_bool,
							   CHANGE_STATION_NO_WATER_DAYS,
							   preason_for_change,
							   pbox_index_0,
							   pset_change_bits,
							   pchange_bitfield_to_set,
							   &(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
							   STATION_CHANGE_BITFIELD_no_water_days_bit,
							   STATION_database_field_names[ STATION_CHANGE_BITFIELD_no_water_days_bit ]
							   #ifdef _MSC_VER
							   , pmerge_update_str,
							   pmerge_insert_fields_str,
							   pmerge_insert_values_str
							   #endif
							 );
}

/* ---------------------------------------------------------- */
/**
 * Sets the landscape details group ID for a particular station. If the station
 * pointer is invalid, an error is raised. Additionally, if the range check of
 * the passed in group ID fails, the station is not updated.
 * 
 * @mutex_requirements Calling function must have the
 * list_program_data_recursive_MUTEX mutex to ensure the station whose value is
 * being set isn't deleted during this process.
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param pstation A pointer to the setation.
 * 
 * @param pGID_station_group The landscape details group ID to assign to the
 * station.
 * 
 * @param preason_for_change The reason the change occured. Possible reasons are
 * defined in change.h
 * 
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 * change.
 * 
 * @param pset_change_bits True if the change bits should be set; otherwise,
 * false. Change bits should NOT bet set if the change came through
 * communications or if the controller's flash is initialized.
 *
 * @author 4/24/2013 Adrianusv
 *
 * @revisions (none)
 */
extern void nm_STATION_set_GID_station_group( void *const pstation,
											  UNS_32 pGID_station_group,
											  const BOOL_32 pgenerate_change_line_bool,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
											)
{
	#ifndef _MSC_VER

		UNS_32	poriginal_GID;

		poriginal_GID = ((STATION_STRUCT*)pstation)->GID_station_group;

	#endif

	if( SHARED_set_GID_station( pstation,
								&(((STATION_STRUCT*)pstation)->GID_station_group),
								pGID_station_group,
								UNS_32_MAX,
								preason_for_change,
								pbox_index_0,
								pset_change_bits,
								pchange_bitfield_to_set,
								&(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
								STATION_CHANGE_BITFIELD_GID_station_group_bit,
								STATION_database_field_names[ STATION_CHANGE_BITFIELD_GID_station_group_bit ]
								#ifdef _MSC_VER
								, pmerge_update_str,
								pmerge_insert_fields_str,
								pmerge_insert_values_str
								#endif
							  ) == (true) )
	{
		#ifndef _MSC_VER

			if( pgenerate_change_line_bool )
			{
				if( pGID_station_group != 0 )
				{
					if( poriginal_GID != 0 )
					{
						Alert_station_moved_from_one_group_to_another_idx( ((STATION_STRUCT*)pstation)->box_index_0, ((STATION_STRUCT*)pstation)->station_number_0, nm_GROUP_get_name( STATION_GROUP_get_group_with_this_GID( poriginal_GID ) ), nm_GROUP_get_name( STATION_GROUP_get_group_with_this_GID( pGID_station_group ) ), preason_for_change );
					}
					else
					{
						Alert_station_added_to_group_idx( ((STATION_STRUCT*)pstation)->box_index_0, ((STATION_STRUCT*)pstation)->station_number_0, nm_GROUP_get_name( STATION_GROUP_get_group_with_this_GID( pGID_station_group ) ), preason_for_change );
					}
				}
				else
				{
					Alert_station_removed_from_group_idx( ((STATION_STRUCT*)pstation)->box_index_0, ((STATION_STRUCT*)pstation)->station_number_0, nm_GROUP_get_name( STATION_GROUP_get_group_with_this_GID( poriginal_GID ) ), preason_for_change );
				}
			}

		#endif
	}
}

/* ---------------------------------------------------------- */
extern void nm_STATION_set_GID_manual_program_A( void *const pstation,
												 UNS_32 pGID_manual_program_A,
												 const BOOL_32 pgenerate_change_line_bool,
												 const UNS_32 preason_for_change,
												 const UNS_32 pbox_index_0,
												 const BOOL_32 pset_change_bits,
												 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												 #ifdef _MSC_VER
												 , char *pmerge_update_str,
												 char *pmerge_insert_fields_str,
												 char *pmerge_insert_values_str
												 #endif
											   )
{
	#ifndef _MSC_VER

		UNS_32	poriginal_GID;

		poriginal_GID = ((STATION_STRUCT*)pstation)->GID_manual_program_A;

	#endif

	if( SHARED_set_GID_station( pstation,
								&(((STATION_STRUCT*)pstation)->GID_manual_program_A),
								pGID_manual_program_A,
								UNS_32_MAX,
								preason_for_change,
								pbox_index_0,
								pset_change_bits,
								pchange_bitfield_to_set,
								&(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
								STATION_CHANGE_BITFIELD_GID_manual_program_A,
								STATION_database_field_names[ STATION_CHANGE_BITFIELD_GID_manual_program_A ]
								#ifdef _MSC_VER
								, pmerge_update_str,
								pmerge_insert_fields_str,
								pmerge_insert_values_str
								#endif
							   ) == (true) )
	{
		#ifndef _MSC_VER

			if( pgenerate_change_line_bool )
			{
				if( pGID_manual_program_A != 0 )
				{
					// 7/23/2015 ajv : It's not actually possible to move a station from one Manual Program to
					// another via the keypad. However, it is possible from Command Center Online so make sure
					// we generate the appropriate alerts.
					if( poriginal_GID != 0 )
					{
						Alert_station_moved_from_one_group_to_another_idx( ((STATION_STRUCT*)pstation)->box_index_0, ((STATION_STRUCT*)pstation)->station_number_0, nm_GROUP_get_name( MANUAL_PROGRAMS_get_group_with_this_GID( poriginal_GID ) ), nm_GROUP_get_name( MANUAL_PROGRAMS_get_group_with_this_GID( pGID_manual_program_A ) ), preason_for_change );
					}
					else
					{
						Alert_station_added_to_group_idx( ((STATION_STRUCT*)pstation)->box_index_0, ((STATION_STRUCT*)pstation)->station_number_0, nm_GROUP_get_name( MANUAL_PROGRAMS_get_group_with_this_GID( pGID_manual_program_A ) ), preason_for_change );
					}
				}
				else
				{
					Alert_station_removed_from_group_idx( ((STATION_STRUCT*)pstation)->box_index_0, ((STATION_STRUCT*)pstation)->station_number_0, nm_GROUP_get_name( MANUAL_PROGRAMS_get_group_with_this_GID( poriginal_GID ) ), preason_for_change );
				}
			}

		#endif
	}
}

/* ---------------------------------------------------------- */
extern void nm_STATION_set_GID_manual_program_B( void *const pstation,
												 UNS_32 pGID_manual_program_B,
												 const BOOL_32 pgenerate_change_line_bool,
												 const UNS_32 preason_for_change,
												 const UNS_32 pbox_index_0,
												 const BOOL_32 pset_change_bits,
												 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												 #ifdef _MSC_VER
												 , char *pmerge_update_str,
												 char *pmerge_insert_fields_str,
												 char *pmerge_insert_values_str
												 #endif
											   )
{
	#ifndef _MSC_VER

		UNS_32	poriginal_GID;

		poriginal_GID = ((STATION_STRUCT*)pstation)->GID_manual_program_B;

	#endif

	if( SHARED_set_GID_station( pstation,
								&(((STATION_STRUCT*)pstation)->GID_manual_program_B),
								pGID_manual_program_B,
								UNS_32_MAX,
								preason_for_change,
								pbox_index_0,
								pset_change_bits,
								pchange_bitfield_to_set,
								&(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
								STATION_CHANGE_BITFIELD_GID_manual_program_B,
								STATION_database_field_names[ STATION_CHANGE_BITFIELD_GID_manual_program_B ]
								#ifdef _MSC_VER
								, pmerge_update_str,
								pmerge_insert_fields_str,
								pmerge_insert_values_str
								#endif
							   ) == (true) )
	{
		#ifndef _MSC_VER

			if( pgenerate_change_line_bool )
			{
				if( pGID_manual_program_B != 0 )
				{
					// 7/23/2015 ajv : It's not actually possible to move a station from one Manual Program to
					// another via the keypad. However, it is possible from Command Center Online so make sure
					// we generate the appropriate alerts.
					if( poriginal_GID != 0 )
					{
						Alert_station_moved_from_one_group_to_another_idx( ((STATION_STRUCT*)pstation)->box_index_0, ((STATION_STRUCT*)pstation)->station_number_0, nm_GROUP_get_name( MANUAL_PROGRAMS_get_group_with_this_GID( poriginal_GID ) ), nm_GROUP_get_name( MANUAL_PROGRAMS_get_group_with_this_GID( pGID_manual_program_B ) ), preason_for_change );
					}
					else
					{
						Alert_station_added_to_group_idx( ((STATION_STRUCT*)pstation)->box_index_0, ((STATION_STRUCT*)pstation)->station_number_0, nm_GROUP_get_name( MANUAL_PROGRAMS_get_group_with_this_GID( pGID_manual_program_B ) ), preason_for_change );
					}
				}
				else
				{
					Alert_station_removed_from_group_idx( ((STATION_STRUCT*)pstation)->box_index_0, ((STATION_STRUCT*)pstation)->station_number_0, nm_GROUP_get_name( MANUAL_PROGRAMS_get_group_with_this_GID( poriginal_GID ) ), preason_for_change );
				}
			}

		#endif
	}
}

/* ---------------------------------------------------------- */
extern void nm_STATION_set_moisture_balance_percent( void *const pstation,
													 float pmoisture_balance_percent,
													 const BOOL_32 pgenerate_change_line_bool,
													 const UNS_32 preason_for_change,
													 const UNS_32 pbox_index_0,
													 const BOOL_32 pset_change_bits,
													 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													 #ifdef _MSC_VER
													 , char *pmerge_update_str,
													 char *pmerge_insert_fields_str,
													 char *pmerge_insert_values_str
													 #endif
												   )
{
	SHARED_set_float_station( pstation,
							  &(((STATION_STRUCT*)pstation)->moisture_balance_percent),
							  pmoisture_balance_percent,
							  STATION_MOISTURE_BALANCE_MIN,
							  STATION_MOISTURE_BALANCE_MAX,
							  STATION_MOISTURE_BALANCE_DEFAULT,
							  pgenerate_change_line_bool,
							  CHANGE_STATION_MOISTURE_BALANCE_FL,
							  preason_for_change,
							  pbox_index_0,
							  pset_change_bits,
							  pchange_bitfield_to_set,
							  &(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
							  STATION_CHANGE_BITFIELD_moisture_balance_bit,
							  STATION_database_field_names[ STATION_CHANGE_BITFIELD_moisture_balance_bit ]
							  #ifdef _MSC_VER
							  , pmerge_update_str,
							  pmerge_insert_fields_str,
							  pmerge_insert_values_str
							  #endif
							);
}

/* ---------------------------------------------------------- */
extern void nm_STATION_set_square_footage( void *const pstation,
										   UNS_32 psquare_footage,
										   const BOOL_32 pgenerate_change_line_bool,
										   const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const BOOL_32 pset_change_bits,
										   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , char *pmerge_update_str,
										   char *pmerge_insert_fields_str,
										   char *pmerge_insert_values_str
										   #endif
										 )
{
	SHARED_set_uint32_station( pstation,
							   &(((STATION_STRUCT*)pstation)->square_footage),
							   psquare_footage,
							   STATION_SQUARE_FOOTAGE_MIN,
							   STATION_SQUARE_FOOTAGE_MAX,
							   STATION_SQUARE_FOOTAGE_DEFAULT,
							   pgenerate_change_line_bool,
							   CHANGE_STATION_SQUARE_FOOTAGE,
							   preason_for_change,
							   pbox_index_0,
							   pset_change_bits,
							   pchange_bitfield_to_set,
							   &(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
							   STATION_CHANGE_BITFIELD_square_footage_bit,
							   STATION_database_field_names[ STATION_CHANGE_BITFIELD_square_footage_bit ]
							   #ifdef _MSC_VER
							   , pmerge_update_str,
							   pmerge_insert_fields_str,
							   pmerge_insert_values_str
							   #endif
							 );
}

/* ---------------------------------------------------------- */
/**
 * This routine sets the value of the flag used to reset the moisture balance at the
 * conclusion of this station's next programmed irrigation. The flag is also used to skip
 * the moisture balance check at the start of the next irrigation.
 * 
 * @mutex_requirements The caller must be holding list_program_data_recursive_MUTEX to
 * ensure the station stays valid.
 *
 * @task_execution Executed within the context of the TD_CHECK task when stations the
 * station completes it's programmed irrigation.
 * 
 * @author 4/18/2016 AdrianusV
 */
extern void nm_STATION_set_ignore_moisture_balance_at_next_irrigation(	void *const pstation,
																		BOOL_32 pset_or_clear_flag,
																		const BOOL_32 pgenerate_change_line_bool,
																		const UNS_32 preason_for_change,
																		const UNS_32 pbox_index_0,
																		const BOOL_32 pset_change_bits,
																		CHANGE_BITS_32_BIT *pchange_bitfield_to_set
																		#ifdef _MSC_VER
																		, char *pmerge_update_str,
																		char *pmerge_insert_fields_str,
																		char *pmerge_insert_values_str
																		#endif
																	  )
{
	SHARED_set_bool_station( pstation,
							 &(((STATION_STRUCT*)pstation)->ignore_moisture_balance_next_irrigation),
							 pset_or_clear_flag,
							 STATION_MOISTURE_BALANCE_CLEAR_IGNORE_FLAG,
							 pgenerate_change_line_bool,
							 CHANGE_STATION_IGNORE_MOISTURE_BALANCE_FLAG,
							 preason_for_change,
							 pbox_index_0,
							 pset_change_bits,
							 pchange_bitfield_to_set,
							 &(((STATION_STRUCT*)pstation)->changes_to_upload_to_comm_server),
							 STATION_CHANGE_BITFIELD_ignore_moisture_balance_flag,
							 STATION_database_field_names[ STATION_CHANGE_BITFIELD_ignore_moisture_balance_flag ]
							 #ifdef _MSC_VER
							 , pmerge_update_str,
							 pmerge_insert_fields_str,
							 pmerge_insert_values_str
							 #endif
						   );
}

/* ---------------------------------------------------------- */
/**
 * Locates the station in the list that matches the passed in temporary station
 * and uses the structure's set routines to compare the two and make any changes
 * to the list's station. If any changes are made during this process, a
 * function is called to store the changes in the list.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *        to ensure the list isn't modified during this process.
 *
 * @executed Executed within the context of the key processing task when the
 *  		 user leaves the screen; within the context of the CommMngr task
 *  		 when a programming change is made from the central or handheld
 *  		 radio remote.
 * 
 * @param ptemporary_station A pointer to a temporary station which contains the
 *                           changes to store.
 * @param ptype_of_change Whether the change is an add, delete, or edit.
 * @param preason_for_change The reason for the change, such as keypad.
 * @param pbox_index_0 The serial number of the controller
 *                                    which initiated the change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @param pcalled_from_comm True if the routine was called from the routine
 * which extracts changes from communications; otherwise, false. This is
 * critical because there are more variables that are received through
 * communications than there are from the GUI. This ensures all of the
 * appropriate changes are stored.
 * 
 * @author 9/22/2011 Adrianusv
 *
 * @revisions
 *    9/22/2011 Initial release
 *    4/6/2012  Removed unnecessary variables and function calls
 *  			Added passing of pset_change_bits into set routine to indicate
 *  			whether the change was made locally so change bits are set
 *  			properly.
 *  			Made procedure static since it's now only called from within
 *  			this file.
 */
extern void nm_STATION_store_changes( STATION_STRUCT *const ptemporary_station,
									  const BOOL_32 pstation_created,
									  const UNS_32 preason_for_change,
									  const UNS_32 pbox_index_0,
									  const BOOL_32 pset_change_bits,
									  const UNS_32 pchanges_received_from,
									  CHANGE_BITS_32_BIT lbitfield_of_changes
									  #ifdef _MSC_VER
									  , char *pSQL_update_str,
									  char *pSQL_insert_fields_str,
									  char *pSQL_insert_values_str
									  #endif
									)
{
	STATION_STRUCT		*lstation;

	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	if( ptemporary_station != NULL)
	{
		#ifndef _MSC_VER
		nm_GROUP_find_this_group_in_list( &station_info_list_hdr, ptemporary_station, (void**)&lstation );

		if( lstation != NULL )
		#else

		// 12/17/2014 ajv : Since the SQL statements manage only updating the value if it's changed,
		// there's no need for us to determine whether the station already exists in the database or
		// not. Therefore, lstation and temporary_station can be the same value.
		lstation = ptemporary_station;

		#endif
		{
			#ifndef _MSC_VER

				lchange_bitfield_to_set = STATION_get_change_bits_ptr( lstation, pchanges_received_from );

			#else

				// 7/28/2015 ajv : Initialize to NULL since this variable is used by the CommServer but
				// expected to be NULL.
				lchange_bitfield_to_set = NULL;

			#endif

			// 4/24/2015 ajv : Only process the change if it was either initiated via the keypad
			// (locally) or if the change bit was set (when called via communications)
			if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_description_bit ) == (true)) )
			{
				nm_STATION_set_description( lstation, nm_GROUP_get_name( ptemporary_station ), !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											#ifdef _MSC_VER
											, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											#endif
										  );
			}

			// 9/19/2013 ajv : If this routine was called from the function which parses changes from
			// communications, store every variable, not just the ones the user is allowed to change
			// from the Station Information screen using the UI.
			if( pchanges_received_from != CHANGE_REASON_KEYPAD )
			{
				if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_physically_available ) == (true) )
				{
					nm_STATION_set_physically_available( lstation, ptemporary_station->physically_available, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
														 #ifdef _MSC_VER
														 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
														 #endif
													   );
				}

				if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_in_use_bit ) == (true) )
				{
					nm_STATION_set_in_use( lstation, ptemporary_station->in_use_bool, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
										   #endif
										 );
				}

				if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_station_number_bit ) == (true) )
				{
					nm_STATION_set_station_number( lstation, ptemporary_station->station_number_0, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												   #ifdef _MSC_VER
												   , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												   #endif
												 );
				}

				if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_box_index_bit ) == (true) )
				{
					nm_STATION_set_box_index( lstation, ptemporary_station->box_index_0, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											  #endif
											);
				}

				if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_decoder_serial_number_bit ) == (true) )
				{
					nm_STATION_set_decoder_serial_number( lstation, ptemporary_station->decoder_serial_number, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
														  #ifdef _MSC_VER
														  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
														  #endif
														);
				}

				if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_decoder_output_bit ) == (true) )
				{
					nm_STATION_set_decoder_output( lstation, ptemporary_station->decoder_output, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												   #ifdef _MSC_VER
												   , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												   #endif
												 );
				}

				if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_GID_station_group_bit ) == (true) )
				{
					nm_STATION_set_GID_station_group( lstation, ptemporary_station->GID_station_group, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
															 #ifdef _MSC_VER
															 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
															 #endif
														   );
				}

				if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_GID_manual_program_A ) == (true) )
				{
					nm_STATION_set_GID_manual_program_A( lstation, ptemporary_station->GID_manual_program_A, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
															 #ifdef _MSC_VER
															 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
															 #endif
														   );
				}

				if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_GID_manual_program_B ) == (true) )
				{
					nm_STATION_set_GID_manual_program_B( lstation, ptemporary_station->GID_manual_program_B, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
															 #ifdef _MSC_VER
															 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
															 #endif
														   );
				}
			}

			// 4/24/2015 ajv : Only process the change if it was either initiated via the keypad
			// (locally) or if the change bit was set (when called via communications)
			if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_total_run_minutes_bit ) == (true)) )
			{
				nm_STATION_set_total_run_minutes( lstation, ptemporary_station->total_run_minutes_10u, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												  #endif
												);
			}

			if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_cycle_minutes_bit ) == (true)) )
			{
				nm_STATION_set_cycle_minutes( lstation, ptemporary_station->cycle_minutes_10u, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											  #endif
											);
			}

			if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_soak_minutes_bit ) == (true)) )
			{
				nm_STATION_set_soak_minutes( lstation, ptemporary_station->soak_minutes, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											 #ifdef _MSC_VER
											 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											 #endif
										   );
			}

			if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_et_factor_bit ) == (true)) )
			{
				nm_STATION_set_ET_factor( lstation, ptemporary_station->et_factor_100u, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
										  #ifdef _MSC_VER
										  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
										  #endif
										);
			}

			if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_expected_flow_rate_bit ) == (true)) )
			{
				nm_STATION_set_expected_flow_rate( lstation, ptemporary_station->expected_flow_rate_gpm, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												   #ifdef _MSC_VER
												   , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												   #endif
												 );
			}

			if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_distribution_uniformity_bit ) == (true)) )
			{
				nm_STATION_set_distribution_uniformity( lstation, ptemporary_station->distribution_uniformity_100u, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
														 #ifdef _MSC_VER
														 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
														 #endif
													   );
			}

			if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_no_water_days_bit ) == (true)) )
			{
				nm_STATION_set_no_water_days( lstation, ptemporary_station->no_water_days, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											  #endif
											);
			}

			if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_moisture_balance_bit ) == (true)) )
			{
				nm_STATION_set_moisture_balance_percent( lstation, ptemporary_station->moisture_balance_percent, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
														 #ifdef _MSC_VER
														 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
														 #endif
													   );
			}

			if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_square_footage_bit ) == (true)) )
			{
				nm_STATION_set_square_footage( lstation, ptemporary_station->square_footage, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											   #ifdef _MSC_VER
											   , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											   #endif
											 );
			}

			if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_ignore_moisture_balance_flag ) == (true)) )
			{
				nm_STATION_set_ignore_moisture_balance_at_next_irrigation( lstation, ptemporary_station->ignore_moisture_balance_next_irrigation, !(pstation_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
																				   #ifdef _MSC_VER
																				   , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
																				   #endif
																				 );
			}
		}
		#ifndef _MSC_VER
		else
		{
			Alert_group_not_found();
		}
		#endif
	}
	else
	{
		#ifndef _MSC_VER

			Alert_func_call_with_null_ptr();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure no list items are added or removed during this process.
 *
 * @executed Executed within the context of the CommMngr task as a token or
 *           token response is being built.
 * 
 * @param pucp A pointer to an unsigned character pointer which contains the
 *             information that was received.
 * @param preason_for_change The reason for the change, such as keypad.
 * 
 * @return UNS_32 The total size of the extracted data. This is used to
 *                increment the ucp in the calling function.
 *
 * @author 4/6/2012 Adrianusv
 *
 * @revisions
 *    4/6/2012 Initial release
 */
extern UNS_32 nm_STATION_extract_and_store_changes_from_comm( const unsigned char *pucp,
															  const UNS_32 preason_for_change,
															  const BOOL_32 pset_change_bits,
															  const UNS_32 pchanges_received_from
															  #ifdef _MSC_VER
															  , char *pSQL_statements,
															  char *pSQL_search_condition_str,
															  char *pSQL_update_str,
															  char *pSQL_insert_fields_str,
															  char *pSQL_insert_values_str,
															  char *pSQL_single_merge_statement_buf,
															  const UNS_32 pnetwork_ID
															  #endif
															)
{
	CHANGE_BITS_32_BIT	lbitfield_of_changes;

	STATION_STRUCT 	*ltemporary_station;

	#ifndef _MSC_VER

	STATION_STRUCT	*lexisting_station;

	#endif

	UNS_32	lnum_changed_stations;

	UNS_32	lsize_of_bitfield;

	BOOL_32	lstation_created;

	UNS_32	lbox_index_0, lstation_number_0;

	UNS_32	i;

	UNS_32	rv;
	
	// ----------

	rv = 0;

	lstation_created = (false);

	// ----------

	// Extract the number of changed groups
	memcpy( &lnum_changed_stations, pucp, sizeof(lnum_changed_stations) );
	pucp += sizeof( lnum_changed_stations );
	rv += sizeof( lnum_changed_stations );

	/*
	// 8/8/2016 rmd : For debug only.
	if( pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE )
	{
		Alert_Message_va( "rcvd from slave: STATION CHANGES for %u groups", lnum_changed_stations );
	}
	else
	if( pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER )
	{
		Alert_Message_va( "rcvd from master: STATION CHANGES for %u groups", lnum_changed_stations );
	}
	else
	{
		Alert_Message_va( "rcvd from UNK: STATION CHANGES for %u groups", lnum_changed_stations );
	}
	*/
	
	for( i = 0; i < lnum_changed_stations; ++i )
	{
#ifndef _MSC_VER
		// 8/8/2016 rmd : Because COMM_MNGR is a HIGH PRIORITY task when it is processing pdata
		// changes (we boosted it above tdcheck and key and display) need to let the other tasks
		// breath. This really is needed when we are here to process 100's of station changes. It
		// can take seconds to do that. So with our token carrying lots of changes to speed up the
		// syncing this reared its head when I tried to sync a changes on every station (all station
		// tied to one station group and then change the landscape parameters to trigger).
		if( (i % 25) == 0 )
		{
			// 8/8/2016 rmd : And return the MUTEX so any task waiting for this MUTEX can proceed. When
			// our 25ms expires we'll try to take it back and that in turn will effectively elevate the
			// priority of the task holding the MUTEX. Giving the MUTEX here was done to stop the FLASH
			// task from complaining about a MUTEX take. It seems the time to process say 350 station
			// changes (which I've seen happen if you change only one item in all 352) can take seconds!
			BOOL_32	we_are_holding;

			we_are_holding = ( xTaskGetCurrentTaskHandle() == xSemaphoreGetMutexHolder(list_program_data_recursive_MUTEX) );
			
			if( we_are_holding )
			{
				xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
			}
			
			vTaskDelay( MS_to_TICKS( 25 ) );

			// 8/8/2016 rmd : And if we were holding it of course take it again.
			if( we_are_holding )
			{
				xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );
			}
		}
#endif
		
		// ----------
		
		memcpy( &lbox_index_0, pucp, sizeof(UNS_32) );
		pucp += sizeof( UNS_32 );
		rv += sizeof( UNS_32 );

		memcpy( &lstation_number_0, pucp, sizeof(lstation_number_0) );
		pucp += sizeof( lstation_number_0 );
		rv += sizeof( lstation_number_0 );

		memcpy( &lsize_of_bitfield, pucp, sizeof(lsize_of_bitfield) );
		pucp += sizeof( lsize_of_bitfield );
		rv += sizeof( lsize_of_bitfield );

		memcpy( &lbitfield_of_changes, pucp, sizeof(lbitfield_of_changes) );
		pucp += sizeof( lbitfield_of_changes );
		rv += sizeof( lbitfield_of_changes );

		// 12/2/2014 ajv : When compiling for the CommServer, we build a MERGE statement if any of
		// the station's settings have changed. Therefore, we can completely skip over searching for
		// the station - this will fail anyway since the list doesn't exist in the comm server.
		#ifndef _MSC_VER

			// Find the existing group with the group ID
			lexisting_station = nm_STATION_get_pointer_to_station( lbox_index_0, lstation_number_0 );

			// 3/6/2013 ajv : If the specified station doesn't exist yet, we need to
			// create it and assign it the appropriate serial and station number.
			if( lexisting_station == NULL )
			{
				lexisting_station = nm_STATION_create_new_station( lbox_index_0, lstation_number_0, pchanges_received_from );

				// 9/19/2013 ajv : Force the type of change flag to added to prevent
				// change lines from being generated. This scenario is possible
				// because the stations may have already existed on the master
				// (in which it would be an edit) but may not exist at this
				// controller.
				lstation_created = (true);

				// ----------
				
				if( pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER )
				{
					// 4/29/2014 rmd : If we don't want to set the change bits that means we called this
					// function via a TOKEN receipt. And any station creation would be for stations not at this
					// box. And therefore we don't have to resend out these changes. The distribution of this
					// activity creating stations and assigning the values STOPS here in this case.
					SHARED_clear_all_32_bit_change_bits( &lexisting_station->changes_to_send_to_master, list_program_data_recursive_MUTEX );

					//Alert_Message_va( "PDATA: station %c%u created cleared", 'A'+lbox_index_0, lstation_number_0 );
				}
				else
				{
					//Alert_Message_va( "PDATA: station %c%u created", 'A'+lbox_index_0, lstation_number_0 );
				}
			}
			else
			{
				//Alert_Message_va( "PDATA: station %c%u found 0x%08x", 'A'+lbox_index_0, lstation_number_0, lbitfield_of_changes );
			}

		#endif


		#ifndef _MSC_VER

		if( lexisting_station != NULL )

		#endif
		{
			// Allocate memory for, and initialize, the temporary group
			#ifndef _MSC_VER
				ltemporary_station = mem_malloc( sizeof(STATION_STRUCT) );
			#else
				ltemporary_station = malloc( sizeof(STATION_STRUCT) );
			#endif

			memset( ltemporary_station, 0x00, sizeof(STATION_STRUCT) );

			#ifndef _MSC_VER

				// Make a copy of the group. We do this so we can change the settings
				// and pass it to the store_changes routine which will actually update
				// the group's settings.
				memcpy( ltemporary_station, lexisting_station, sizeof(STATION_STRUCT) );

			#else

				// 5/1/2015 ajv : Since we don't copy an existing station into the temporary one when
				// parsing using the CommServer, make sure we set the station number and box index manually
				// based on what came in at the beginning of the memory block.
				ltemporary_station->station_number_0 = lstation_number_0;
				ltemporary_station->box_index_0 = lbox_index_0;

			#endif

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_description_bit ) == (true) )
			{
				memcpy( &ltemporary_station->base.description, pucp, NUMBER_OF_CHARS_IN_A_NAME );
				pucp += NUMBER_OF_CHARS_IN_A_NAME;
				rv += NUMBER_OF_CHARS_IN_A_NAME;
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_physically_available ) == (true) )
			{
				memcpy( &ltemporary_station->physically_available, pucp, sizeof(ltemporary_station->physically_available) );
				pucp += sizeof( ltemporary_station->physically_available );
				rv += sizeof( ltemporary_station->physically_available );

				//Alert_Message_va( "PDATA: station %c%u phy avail set %u", 'A'+lbox_index_0, lstation_number_0, ltemporary_station->physically_available );
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_in_use_bit ) == (true) )
			{
				memcpy( &ltemporary_station->in_use_bool, pucp, sizeof(ltemporary_station->in_use_bool) );
				pucp += sizeof( ltemporary_station->in_use_bool );
				rv += sizeof( ltemporary_station->in_use_bool );
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_station_number_bit ) == (true) )
			{
				memcpy( &ltemporary_station->station_number_0, pucp, sizeof(ltemporary_station->station_number_0) );
				pucp += sizeof( ltemporary_station->station_number_0 );
				rv += sizeof( ltemporary_station->station_number_0 );
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_box_index_bit ) == (true) )
			{
				memcpy( &ltemporary_station->box_index_0, pucp, sizeof(ltemporary_station->box_index_0) );
				pucp += sizeof( ltemporary_station->box_index_0 );
				rv += sizeof( ltemporary_station->box_index_0 );
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_decoder_serial_number_bit ) == (true) )
			{
				memcpy( &ltemporary_station->decoder_serial_number, pucp, sizeof(ltemporary_station->decoder_serial_number) );
				pucp += sizeof( ltemporary_station->decoder_serial_number );
				rv += sizeof( ltemporary_station->decoder_serial_number );
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_decoder_output_bit ) == (true) )
			{
				memcpy( &ltemporary_station->decoder_output, pucp, sizeof(ltemporary_station->decoder_output) );
				pucp += sizeof( ltemporary_station->decoder_output );
				rv += sizeof( ltemporary_station->decoder_output );
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_total_run_minutes_bit ) == (true) )
			{
				memcpy( &ltemporary_station->total_run_minutes_10u, pucp, sizeof(ltemporary_station->total_run_minutes_10u) );
				pucp += sizeof( ltemporary_station->total_run_minutes_10u );
				rv += sizeof( ltemporary_station->total_run_minutes_10u );
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_cycle_minutes_bit ) == (true) )
			{
				memcpy( &ltemporary_station->cycle_minutes_10u, pucp, sizeof(ltemporary_station->cycle_minutes_10u) );
				pucp += sizeof( ltemporary_station->cycle_minutes_10u );
				rv += sizeof( ltemporary_station->cycle_minutes_10u );
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_soak_minutes_bit ) == (true) )
			{
				memcpy( &ltemporary_station->soak_minutes, pucp, sizeof(ltemporary_station->soak_minutes) );
				pucp += sizeof( ltemporary_station->soak_minutes );
				rv += sizeof( ltemporary_station->soak_minutes );
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_et_factor_bit ) == (true) )
			{
				memcpy( &ltemporary_station->et_factor_100u, pucp, sizeof(ltemporary_station->et_factor_100u) );
				pucp += sizeof( ltemporary_station->et_factor_100u );
				rv += sizeof( ltemporary_station->et_factor_100u );
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_expected_flow_rate_bit ) == (true) )
			{
				memcpy( &ltemporary_station->expected_flow_rate_gpm, pucp, sizeof(ltemporary_station->expected_flow_rate_gpm) );
				pucp += sizeof( ltemporary_station->expected_flow_rate_gpm );
				rv += sizeof( ltemporary_station->expected_flow_rate_gpm );
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_distribution_uniformity_bit ) == (true) )
			{
				memcpy( &ltemporary_station->distribution_uniformity_100u, pucp, sizeof(ltemporary_station->distribution_uniformity_100u) );
				pucp += sizeof( ltemporary_station->distribution_uniformity_100u );
				rv += sizeof( ltemporary_station->distribution_uniformity_100u );
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_no_water_days_bit ) == (true) )
			{
				memcpy( &ltemporary_station->no_water_days, pucp, sizeof(ltemporary_station->no_water_days) );
				pucp += sizeof( ltemporary_station->no_water_days );
				rv += sizeof( ltemporary_station->no_water_days );
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_GID_station_group_bit ) == (true) )
			{
				memcpy( &ltemporary_station->GID_station_group, pucp, sizeof(ltemporary_station->GID_station_group) );
				pucp += sizeof( ltemporary_station->GID_station_group );
				rv += sizeof( ltemporary_station->GID_station_group );
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_GID_manual_program_A ) == (true) )
			{
				memcpy( &ltemporary_station->GID_manual_program_A, pucp, sizeof(ltemporary_station->GID_manual_program_A) );
				pucp += sizeof( ltemporary_station->GID_manual_program_A );
				rv += sizeof( ltemporary_station->GID_manual_program_A );
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_GID_manual_program_B ) == (true) )
			{
				memcpy( &ltemporary_station->GID_manual_program_B, pucp, sizeof(ltemporary_station->GID_manual_program_B) );
				pucp += sizeof( ltemporary_station->GID_manual_program_B );
				rv += sizeof( ltemporary_station->GID_manual_program_B );
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_moisture_balance_bit ) == (true) )
			{
				memcpy( &ltemporary_station->moisture_balance_percent, pucp, sizeof(ltemporary_station->moisture_balance_percent) );
				pucp += sizeof( ltemporary_station->moisture_balance_percent );
				rv += sizeof( ltemporary_station->moisture_balance_percent );
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_square_footage_bit ) == (true) )
			{
				memcpy( &ltemporary_station->square_footage, pucp, sizeof(ltemporary_station->square_footage) );
				pucp += sizeof( ltemporary_station->square_footage );
				rv += sizeof( ltemporary_station->square_footage );
			}

			if( B_IS_SET( lbitfield_of_changes, STATION_CHANGE_BITFIELD_ignore_moisture_balance_flag ) == (true) )
			{
				memcpy( &ltemporary_station->ignore_moisture_balance_next_irrigation, pucp, sizeof(ltemporary_station->ignore_moisture_balance_next_irrigation) );
				pucp += sizeof( ltemporary_station->ignore_moisture_balance_next_irrigation );
				rv += sizeof( ltemporary_station->ignore_moisture_balance_next_irrigation );
			}

			// ----------

			#ifdef _MSC_VER

				// 1/16/2015 ajv : Make sure the strings are fully initialized to NULL since we rely on this
				// to determine whether the string is empty or not.
				memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

				// 1/19/2015 ajv : Insert the network ID in case the station doesn't exist in the database
				// yet.
				SQL_MERGE_build_upsert_uint32( pnetwork_ID, SQL_MERGE_include_field_name_in_insert_clause, "NetworkID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

			#endif

			// ----------

			// Now that we've extracted the settings out of the communication structure, store the
			// changes
			nm_STATION_store_changes( ltemporary_station, lstation_created, preason_for_change, 0, pset_change_bits, pchanges_received_from, lbitfield_of_changes
									  #ifdef _MSC_VER
									  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
									  #endif
									 );

			// ----------

			#ifndef _MSC_VER

				mem_free( ltemporary_station );

			#else

				SQL_MERGE_build_search_condition_uint32( "NetworkID", pnetwork_ID, pSQL_search_condition_str );
				SQL_MERGE_build_search_condition_uint32( "BoxIndex", ltemporary_station->box_index_0, pSQL_search_condition_str );
				SQL_MERGE_build_search_condition_uint32( "StationNumber", ltemporary_station->station_number_0, pSQL_search_condition_str );

				SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements( DATABASE_TABLE_NAME_STATIONS, pSQL_search_condition_str, SQL_MERGE_include_field_name_in_insert_clause, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf );

				// ----------

				free( ltemporary_station );

			#endif
		}
		#ifndef _MSC_VER
		else
		{
			Alert_group_not_found();
		}
		#endif
	}

	#ifndef _MSC_VER

		// Only save the file if we've extracted information out of the ucp. This is a sanity check
		// to ensure the bit is only set if data was included.
		if( rv > 0 )
		{
			// 5/13/2015 ajv : We want to save the file in the following cases:
			// 
			//   - A message from the CommServer since we've responded that we received it and need to
			//     ensure it's saved.
			//   - A token response from the slave to the master
			//   - A token from the master to the slave
			//   
			// We DO NOT want to save the file when a token is received by the master since we've
			// already saved the file for one of the reasons above. This will otherwise result in a
			// double save for each change - once when it comes in and once when it's received when the
			// master distributes the changes back out.
			if( (pchanges_received_from == CHANGE_REASON_CENTRAL_OR_MOBILE) ||
				(pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE) ||
				((pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER) && (!FLOWSENSE_we_are_a_master_one_way_or_another())) )
			{
				// 12/10/2014 ajv : Schedule a save rather than saving immediately to prevent stacking of
				// saves. Additionally, set a relatively short time to help cover cases where users make a
				// lot of changes in a short period.
				FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_STATION_INFO, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
			}
		}
		else
		{
			Alert_bit_set_with_no_data();
		}

	#endif

	return ( rv );
}

#ifdef _MSC_VER

/* ---------------------------------------------------------- */
extern INT_32 STATION_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order )
{
	STATION_STRUCT	*lstation;

	INT_32	rv;

	// ----------

	// 4/15/2016 ajv : In most cases, we're going to pass back an array_index of 0. However, in
	// the event the value in question is part of an array, we're going to pass back the actual
	// index so that the CommServer can sort not just on the bit number, but also the
	// array_index for values that share the same bit number.
	*psecondary_sort_order = 0;

	rv = -1;

	// ----------

	if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_description_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_description_bit;
		*psize_of_var_ptr = sizeof(lstation->base.description);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_physically_available ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_physically_available;
		*psize_of_var_ptr = sizeof(lstation->physically_available);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_in_use_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_in_use_bit;
		*psize_of_var_ptr = sizeof(lstation->in_use_bool);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_station_number_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_station_number_bit;
		*psize_of_var_ptr = sizeof(lstation->station_number_0);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_box_index_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_box_index_bit;
		*psize_of_var_ptr = sizeof(lstation->box_index_0);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_decoder_serial_number_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_decoder_serial_number_bit;
		*psize_of_var_ptr = sizeof(lstation->decoder_serial_number);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_decoder_output_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_decoder_output_bit;
		*psize_of_var_ptr = sizeof(lstation->decoder_output);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_total_run_minutes_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_total_run_minutes_bit;
		*psize_of_var_ptr = sizeof(lstation->total_run_minutes_10u);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_cycle_minutes_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_cycle_minutes_bit;
		*psize_of_var_ptr = sizeof(lstation->cycle_minutes_10u);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_soak_minutes_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_soak_minutes_bit;
		*psize_of_var_ptr = sizeof(lstation->soak_minutes);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_et_factor_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_et_factor_bit;
		*psize_of_var_ptr = sizeof(lstation->et_factor_100u);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_expected_flow_rate_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_expected_flow_rate_bit;
		*psize_of_var_ptr = sizeof(lstation->expected_flow_rate_gpm);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_distribution_uniformity_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_distribution_uniformity_bit;
		*psize_of_var_ptr = sizeof(lstation->distribution_uniformity_100u);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_no_water_days_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_no_water_days_bit;
		*psize_of_var_ptr = sizeof(lstation->no_water_days);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_GID_station_group_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_GID_station_group_bit;
		*psize_of_var_ptr = sizeof(lstation->GID_station_group);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_GID_manual_program_A ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_GID_manual_program_A;
		*psize_of_var_ptr = sizeof(lstation->GID_manual_program_A);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_GID_manual_program_B ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_GID_manual_program_B;
		*psize_of_var_ptr = sizeof(lstation->GID_manual_program_B);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_moisture_balance_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_moisture_balance_bit;
		*psize_of_var_ptr = sizeof(lstation->moisture_balance_percent);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_square_footage_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_square_footage_bit;
		*psize_of_var_ptr = sizeof(lstation->square_footage);
	}
	else if( strncmp( pfield_name, STATION_database_field_names[ STATION_CHANGE_BITFIELD_ignore_moisture_balance_flag ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_CHANGE_BITFIELD_ignore_moisture_balance_flag;
		*psize_of_var_ptr = sizeof(lstation->ignore_moisture_balance_next_irrigation);
	}

	return( rv );
}

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

