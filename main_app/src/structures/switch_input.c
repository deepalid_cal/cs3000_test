/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"switch_input.h"

#include	"app_startup.h"

#include	"change.h"

#include	"configuration_controller.h"

#include	"range_check.h"

#include	"shared.h"

#include	"et_data.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex 
 *
 * @executed 
 * 
 * @param pswitch A pointer to the SWITCH_INPUT_CONTROL_STRUCT object to change.
 * @param pchange_line_index The index of the change line to generate.
 * @param pname The name to assign to the switch object.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
extern void nm_SWITCH_set_name( SWITCH_INPUT_CONTROL_STRUCT *pswitch,
								const char *pname,
								const BOOL_32 pgenerate_change_line_bool,
								const UNS_32 pchange_line_index,
								const UNS_32 preason_for_change,
								const UNS_32 pbox_index_0,
								const BOOL_32 pset_change_bits )
{
	SHARED_set_string_controller( pswitch->name,
								  MAX_REFERENCE_ET_NAME_LENGTH,
								  pname,
								  pgenerate_change_line_bool,
								  pchange_line_index,
								  preason_for_change,
								  pbox_index_0,
								  pset_change_bits,
								  NULL,
								  NULL,
								  0,
								  "Switch Name" );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex 
 *
 * @executed 
 * 
 * @param pswitch A pointer to the SWITCH_INPUT_CONTROL_STRUCT object to change.
 * @param pdefault_value The default value if the range check of the passed-in
 *                       variable fails.
 * @param pchange_line_index The index of the change line to generate.
 * @param pserial_number The controller serial number to assign to the switch.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *                                   generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *  	                     defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
extern void nm_SWITCH_set_serial_number( SWITCH_INPUT_CONTROL_STRUCT *pswitch,
										 UNS_32 pserial_number,
										 const UNS_32 pdefault_value,
										 const BOOL_32 pgenerate_change_line_bool,
										 const UNS_32 pchange_line_index,
										 const UNS_32 preason_for_change,
										 const UNS_32 pbox_index_0,
										 const BOOL_32 pset_change_bits )
{
	SHARED_set_uint32_controller( &pswitch->controller_serial_number,
								  pserial_number,
								  CONTROLLER_MINIMUM_SERIAL_NUMBER,
								  CONTROLLER_MAXIMUM_SERIAL_NUMBER,
								  pdefault_value,
								  pgenerate_change_line_bool,
								  pchange_line_index,
								  preason_for_change,
								  pbox_index_0,
								  pset_change_bits,
								  NULL,
								  NULL,
								  0,
								  "Switch Controller S/N" );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex 
 *
 * @executed 
 * 
 * @param pswitch A pointer to the SWITCH_INPUT_CONTROL_STRUCT object to change.
 * @param pdefault_value The default value if the range check of the passed-in
 *                       variable fails.
 * @param pchange_line_index The index of the change line to generate.
 * @param pconnected_bool True if a switch is connected; otherwise, false.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
extern void nm_SWITCH_set_connected( SWITCH_INPUT_CONTROL_STRUCT *pswitch,
									 BOOL_32 pconnected_bool,
									 const BOOL_32 pdefault_value,
									 const BOOL_32 pgenerate_change_line_bool,
									 const UNS_32 pchange_line_index,
									 const UNS_32 preason_for_change,
									 const UNS_32 pbox_index_0,
									 const BOOL_32 pset_change_bits )
{
	SHARED_set_bool_controller( &pswitch->connected_bool,
								pconnected_bool,
								pdefault_value,
								pgenerate_change_line_bool,
								pchange_line_index,
								preason_for_change,
								pbox_index_0,
								pset_change_bits,
								NULL,
								NULL,
								0,
								"Switch Connected" );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex 
 *
 * @executed 
 * 
 * @param pswitch A pointer to the SWITCH_INPUT_CONTROL_STRUCT object to change.
 * @param pdefault_value The default value if the range check of the passed-in
 *                       variable fails.
 * @param pchange_line_index The index of the change line to generate.
 * @param pnormally_closed_bool True if the switch is normally closed;
 *                              otherwise, false.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
extern void nm_SWITCH_set_normally_closed( SWITCH_INPUT_CONTROL_STRUCT *pswitch,
										   BOOL_32 pnormally_closed_bool,
										   const BOOL_32 pdefault_value,
										   const BOOL_32 pgenerate_change_line_bool,
										   const UNS_32 pchange_line_index,
										   const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const BOOL_32 pset_change_bits )
{
	SHARED_set_bool_controller( &pswitch->normally_closed_bool,
								pnormally_closed_bool,
								pdefault_value,
								pgenerate_change_line_bool,
								pchange_line_index,
								preason_for_change,
								pbox_index_0,
								pset_change_bits,
								NULL,
								NULL,
								0,
								"Switch Normally Closed" );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex 
 *
 * @executed 
 * 
 * @param pswitch A pointer to the SWITCH_INPUT_CONTROL_STRUCT object to change.
 * @param pdefault_value The default value if the range check of the passed-in
 *                       variable fails.
 * @param pchange_line_index The index of the change line to generate.
 * @param pstops_irrigation_bool True if the switch stops irrigation, otherwise,
 *                               false.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
extern void nm_SWITCH_set_stops_irrigation( SWITCH_INPUT_CONTROL_STRUCT *pswitch,
											BOOL_32 pstops_irrigation_bool,
											const BOOL_32 pdefault_value,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 pchange_line_index,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0,
											const BOOL_32 pset_change_bits )
{
	SHARED_set_bool_controller( &pswitch->stops_irrigation_bool,
								pstops_irrigation_bool,
								pdefault_value,
								pgenerate_change_line_bool,
								pchange_line_index,
								preason_for_change,
								pbox_index_0,
								pset_change_bits,
								NULL,
								NULL,
								0,
								"Switch Stops Irrigation" );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex 
 *
 * @executed 
 * 
 * @param pswitch A pointer to the SWITCH_INPUT_CONTROL_STRUCT object to change.
 * @param pdefault_value The default value if the range check of the passed-in
 *                       variable fails.
 * @param pcurrently_active_bool True if the switch is active; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
extern void nm_SWITCH_set_currently_active( SWITCH_INPUT_CONTROL_STRUCT *pswitch,
											BOOL_32 pcurrently_active_bool,
											const BOOL_32 pdefault_value,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0,
											const BOOL_32 pset_change_bits )
{
	// We don't generate a change line with this change because an alert
	// is generated.
	SHARED_set_bool_controller( &pswitch->currently_active_bool,
								pcurrently_active_bool,
								pdefault_value,
								(false),
								0,
								preason_for_change,
								pbox_index_0,
								pset_change_bits,
								NULL,
								NULL,
								0,
								"Switch Currently Active" );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex 
 *
 * @executed 
 * 
 * @param pswitch A pointer to the SWITCH_INPUT_CONTROL_STRUCT object to change.
 * @param pdefault_value The default value if the range check of the passed-in
 *                       variable fails.
 * @param pchange_line_index The index of the change line to generate.
 * @param pstops_irrigation_bool 
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
extern void nm_SWITCH_set_irrigation_to_affect( SWITCH_INPUT_CONTROL_STRUCT *pswitch,
												UNS_32 pirrigation_to_affect,
												const UNS_32 pmin_value,
												const UNS_32 pmax_value,
												const UNS_32 pdefault_value,
												const BOOL_32 pgenerate_change_line_bool,
												const UNS_32 pchange_line_index,
												const UNS_32 preason_for_change,
												const UNS_32 pbox_index_0,
												const BOOL_32 pset_change_bits )
{
	SHARED_set_uint32_controller( &pswitch->irrigation_to_affect,
								  pirrigation_to_affect,
								  pmin_value,
								  pmax_value,
								  pdefault_value,
								  pgenerate_change_line_bool,
								  pchange_line_index,
								  preason_for_change,
								  pbox_index_0,
								  pset_change_bits,
								  NULL,
								  NULL,
								  0,
								  "Switch Irrigation to Affect" );
}

/* ---------------------------------------------------------- */
extern char *SWITCH_get_name( char *const pbuf, const UNS_32 allowable_size )
{
	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );
/*
	if ( sic.name[ 0 ] == 0x00 )
	{
		snprintf( pbuf, allowable_size, "<Unnamed Switch>" );
	}
	else
	{
		strlcpy( pbuf, sic.name, allowable_size );
	}
*/
	snprintf( pbuf, allowable_size, "<name not available>" );

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	return( pbuf );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

