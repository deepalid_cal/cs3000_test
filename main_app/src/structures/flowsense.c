/*  file = flowsense.c                        03.24.2014 ajv  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"flowsense.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"battery_backed_vars.h"

#include	"change.h"

#include	"comm_mngr.h"

#include	"flash_storage.h"

#include	"range_check.h"

#include	"shared.h"

#include	"weather_control.h"

#include	"foal_irri.h"

#include	"foal_lights.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static const char FLOWSENSE_FILENAME[] =	"FLOWSENSE";

// 1/26/2015 rmd : NOTE - upon REVISION update the record can only grow in size. At least
// that is the way I've coded the flash file supporting functions. It may be possible to
// decrease the record size. I just haven't gone through the code with that in mind.

#define	FLOWSENSE_LATEST_FILE_REVISION		(0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

struct FLOWSENSE_STRUCT
{
	// This is the single letter given to a controller. It's range is 'A' to ('A' +
	// MAX_CHAIN_LENGTH - 1). The packets of the scan message invent serial numbers starting at
	// 0x41 or the character 'A'. We only use this form of addressing for the outgoing scan
	// packets. The scan response uses regular serial number based addressing.
	UNS_32					controller_letter;

	// Do not want to refer to this directly cause it is actually qualified with the -FL CMOS
	// setting within a function used to evaluate. Hence the underscores as a wake up call.
	BOOL_32					POAFS;

	// 10/2/2012 rmd : The actual user setting for the Number Of Controllers In the Chain.
	UNS_32					NOCIC;
};

static FLOWSENSE_STRUCT fl_struct;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void FLOWSENSE_set_default_values( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/**
 * Sets the controller's communication address.
 * 
 * @executed Executed within the context of the key procesing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param pcontroller_letter The communication address to assign to the
 * controller.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *                                   generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reasons
 *                           are defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * 
 * @return UNS_32 1 if the value was changed; otherwise, 0.
 *
 * @author 3/21/2012 Adrianusv
 *
 * @revisions
 *    3/21/2012 Initial release
 */
static UNS_32 set_controller_letter( UNS_32 pcontroller_letter,
									 const BOOL_32 pgenerate_change_line_bool,
									 const UNS_32 preason_for_change,
									 const UNS_32 pbox_index_0 )
{
	UNS_32	lcopy;

	UNS_32	rv;

	rv = 0;

	// 3/25/2014 rmd : Get a copy.
	lcopy = fl_struct.controller_letter;
	
	// 3/25/2014 rmd : Set it to the new value.
	fl_struct.controller_letter = pcontroller_letter;
	
	// 3/25/2014 rmd : Make the range check on the new value.
	RangeCheck_uint32( &fl_struct.controller_letter, 'A', ('A' + MAX_CHAIN_LENGTH - 1), 'A', NULL, "Controller Letter" );

	// 3/25/2014 rmd : If there was a change record it.
	if( fl_struct.controller_letter != lcopy )
	{
		// 4/21/2014 rmd : Update the copy used when making the startup alert lines.
		restart_info.controller_index = (pcontroller_letter-'A');

		if( pgenerate_change_line_bool == (true) )
		{
			Alert_ChangeLine_Controller( CHANGE_CONTROLLER_LETTER, FLOWSENSE_get_controller_index(), &lcopy, &fl_struct.controller_letter, sizeof(fl_struct.controller_letter), preason_for_change, pbox_index_0 );
		}

		// 3/25/2014 rmd : Flag there was a change.
		rv = 1;
	}
 
	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Sets whether the controller is part of a FLOWSENSE chain or not.
 * 
 * @executed Executed within the context of the key procesing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param ppart_of_a_chain_bool True if the controller is part of a FLOWSENSE
 *                              chain; otherwise, false.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *                                   generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reasons
 *                           are defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * 
 * @return UNS_32 1 if the value was changed; otherwise, 0.
 *
 * @author 3/21/2012 Adrianusv
 *
 * @revisions
 *    3/21/2012 Initial release
 */
static UNS_32 set_part_of_a_chain( BOOL_32 ppart_of_a_chain_bool,
								   const BOOL_32 pgenerate_change_line_bool,
								   const UNS_32 preason_for_change,
								   const UNS_32 pbox_index_0 )
{
	return( SHARED_set_bool_controller( &fl_struct.POAFS,
										ppart_of_a_chain_bool,
										(false),
										pgenerate_change_line_bool,
										CHANGE_CONTROLLER_PART_OF_A_CHAIN,
										preason_for_change,
										pbox_index_0,
										(false),
										NULL,
										NULL,
										0,
										"Part of a Chain" ) );
}

/* ---------------------------------------------------------- */
/**
 * Sets the number of controllers in the chain.
 * 
 * @executed Executed within the context of the key procesing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param pnum_controllers_in_chain The number of controllers in the chain to assign
 *  							to the controller.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *                                   generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reasons
 *                           are defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * 
 * @return UNS_32 1 if the value was changed; otherwise, 0.
 *
 * @author 3/21/2012 Adrianusv
 *
 * @revisions
 *    3/21/2012 Initial release
 */
static UNS_32 set_num_controllers_in_chain( UNS_32 pnum_controllers_in_chain,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0 )
{
	BOOL_32	lrange_check_passed;

	UNS_32	rv;

	rv = 0;

	lrange_check_passed = RangeCheck_uint32( &pnum_controllers_in_chain, 1, MAX_CHAIN_LENGTH, 1, NULL, "Number of Controllers in Chain" );

	if( lrange_check_passed == (true) )
	{
		if( fl_struct.NOCIC != pnum_controllers_in_chain )
		{
			if( pgenerate_change_line_bool == (true) )
			{
				Alert_ChangeLine_Controller( CHANGE_CONTROLLER_CONTROLLERS_IN_CHAIN, FLOWSENSE_get_controller_index(), &fl_struct.NOCIC, &pnum_controllers_in_chain, sizeof(pnum_controllers_in_chain), preason_for_change, pbox_index_0 );
			}

			// 10/2/2012 rmd : Make the assignement.
			fl_struct.NOCIC = pnum_controllers_in_chain;

			rv = 1;
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static void FLOWSENSE_set_default_values( void )
{
	UNS_32	box_index_0;

	// 4/21/2014 rmd : CANNOT use the FLOWSENSE_get_controller_index function to get the index
	// that we have not yet initialized!!!! Don't do it. Besides we are going to set the letter
	// to A anyway. So we KNOW the box_index for this controller is 0.
	box_index_0 = 0;

	// ----------
	
	// During a scan we use this letter as opposed to the serial number.
	set_controller_letter( 'A', CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0 );

	set_part_of_a_chain( (false), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0 );

	set_num_controllers_in_chain( 2, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0 );
}

/* ---------------------------------------------------------- */
static void flowsense_updater( UNS_32 pfrom_revision )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only when
	// the file revision is changed. FYI there is no MUTEX on the flowsense structure data.

	// ----------
	
	if( pfrom_revision == FLOWSENSE_LATEST_FILE_REVISION )
	{
		Alert_Message_va( "FLOWSENSE file unexpd update %u", pfrom_revision );	
	}
	else
	{
		if( pfrom_revision == 0 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 0 to REVISION 1.

			
			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 1 to 2 changes if
			// needed.
			pfrom_revision += 1;
		}
		
		// ----------
		
		/*
		if( pfrom_revision == 1 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 1 to REVISION 2.
				


			pfrom_revision += 1;
		}
		*/
	}

	// ----------

	// 1/29/2015 rmd : Well after the updater has run the pfrom_revision is supposed to equal
	// the latest revision. By design of this update process.
	if( pfrom_revision != FLOWSENSE_LATEST_FILE_REVISION )
	{
		Alert_Message( "FLOWSENSE updater error" );
	}
}

/* ---------------------------------------------------------- */
extern void init_file_flowsense( void )
{
	FLASH_FILE_find_or_create_variable_file(	FLASH_INDEX_1_GENERAL_STORAGE,
												FLOWSENSE_FILENAME,
												FLOWSENSE_LATEST_FILE_REVISION,
												&fl_struct,
												sizeof( fl_struct ),
												NULL,
												&flowsense_updater,
												&FLOWSENSE_set_default_values,
												FF_FLOWSENSE );
												
	// ----------
	
	// 7/24/2015 rmd : Check out this possible scenario. The controller has been used for a
	// while and the letter was set to a "B" say. The controller letter SET routine has kept the
	// restart_info copy up to date. Now let's say the battery is replaced. The battery backed
	// restart_info is reset and indicates a letter of 'A'. So we need to cover that hole here
	// with a stay in-sync statement. Which as it turns out can and should run on every startup
	// without harm.
	restart_info.controller_index = (fl_struct.controller_letter - 'A');
}

/* ---------------------------------------------------------- */
extern void save_file_flowsense( void )
{
	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE,
															FLOWSENSE_FILENAME,
															FLOWSENSE_LATEST_FILE_REVISION,
															&fl_struct,
															sizeof(fl_struct),
															NULL,
															FF_FLOWSENSE );
}

/* ---------------------------------------------------------- */
/**
 * Copies the communication address and FLOWSENSE setup changes to easyGUI
 * variables.
 * 
 * @executed Executed within the context of the display processing task.
 *
 * @author 3/21/2012 Adrianusv
 *
 * @revisions
 *    3/21/2012 Initial release
 */
extern void FLOWSENSE_copy_settings_into_guivars( void )
{
	GuiVar_FLControllerIndex_0 = (fl_struct.controller_letter - 'A');

	GuiVar_FLPartOfChain = fl_struct.POAFS;

	GuiVar_FLNumControllersInChain = fl_struct.NOCIC;
}

/* ---------------------------------------------------------- */
/**
 * Stores the communication address and FLOWSENSE setup changes. This routine
 * should be called whenever one of the FLOWSENSE setup variables is changed.
 * 
 * @executed Executed within the context of the key processing task when the
 *  		 user leaves the screen or presses the button to set the date and
 *  		 time; within the context of the CommMngr task when a programming
 *  		 change is made from the central or handheld radio remote.
 * 
 * @param preason_for_change The reason for the change, such as keypad.
 * @param pbox_index_0 The serial number of the controller
 *                                    which initiated the change.
 *
 * @author 3/21/2012 Adrianusv
 *
 * @revisions
 *    3/21/2012 Initial release
 */
extern void FLOWSENSE_extract_and_store_changes_from_GuiVars( const BOOL_32 pforce_scan )
{
	COMM_MNGR_TASK_QUEUE_STRUCT	cmqs;
	
	BOOL_32		post_it;
	
	UNS_32		lnumber_of_changes;

	// ----------
	
	lnumber_of_changes = 0;

	post_it = (false);
	
	// ----------
	
	lnumber_of_changes += set_controller_letter( (GuiVar_FLControllerIndex_0 + 'A'), CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, GuiVar_FLControllerIndex_0 );

	// ----------

	lnumber_of_changes += set_part_of_a_chain( (BOOL_32)(GuiVar_FLPartOfChain), CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, GuiVar_FLControllerIndex_0 );

	lnumber_of_changes += set_num_controllers_in_chain( GuiVar_FLNumControllersInChain, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, GuiVar_FLControllerIndex_0 );

	// ----------
	
	// 4/29/2014 rmd : The only way pforce_scan was set true is off the screen cursor
	// position. If there are changes too and we are a MASTER we set up to post this same event
	// (only once in any case).
	if( pforce_scan == (true) )
	{
		post_it = (true);

		cmqs.event = COMM_MNGR_EVENT_request_to_start_a_scan;

		cmqs.reason_for_scan = COMM_MNGR_reason_to_scan_keypad;
	}

	// ----------

	if( lnumber_of_changes > 0 )
	{
		// 4/29/2014 rmd : Fundemental changes to the chain have been made. Need to rescan,
		// potentially clean any NEW controllers, and re-sync the program data. If we are a master
		// request a scan to start. For a slave we need to ask the master to do this if actually
		// part of a live chain now. If not in a live chain now then its up to the master to do a
		// rescan.
		if( FLOWSENSE_we_are_a_master_one_way_or_another() )
		{
			// 3/6/2014 rmd : Request a scan. Will start when the pending message exchange has
			// completed.
			post_it = (true);

			cmqs.event = COMM_MNGR_EVENT_request_to_start_a_scan;

			cmqs.reason_for_scan = COMM_MNGR_reason_to_scan_keypad;
		}
		else
		{
			post_it = (true);

			cmqs.event = COMM_MNGR_EVENT_tell_master_to_scan_and_report_we_are_NEW;
		}
		
		// ----------
		
		// 11/26/2014 rmd : We have decided fundamental changes such as these result in the
		// cancellation of any ongoing irrigation. If we are a master the irrigation will be
		// cancelled at receipt of any scan response that determines the responding controller is
		// NEW. If we are part of a chain everyone in the chain will see the CLEAN_HOUSE request
		// that is directed at the NEW controller (only the NEW controller will truly clean house).
		// But all controllers will wipe both their irri and foal irrigation lists.
		if( !FLOWSENSE_we_are_poafs() )
		{
			FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation();

			FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights();
		}
		
				
		// 12/10/2014 ajv : Schedule a save rather than saving immediately to prevent stacking of
		// saves. Additionally, set a short time to ensure the save occurs before the scan starts in
		// case the scan triggers a restart (this can occur if a slave has restarted). This is
		// critical because, if the user sets the master to be part of a FLOWSENSE chain and then
		// leaves the screen or forces a scan, the change may not be saved and the controller will
		// cycle power and not know that it's supposed to be part of a chain when it comes back
		// online.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_FLOWSENSE, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
	}

	// ----------

	if( post_it )
	{
		COMM_MNGR_post_event_with_details( &cmqs );
	}
}

/* ---------------------------------------------------------- */
extern UNS_32 FLOWSENSE_get_controller_letter( void )
{
	if( !RangeCheck_uint32( &fl_struct.controller_letter, 'A', ('A' + MAX_CHAIN_LENGTH - 1), 'A', NULL, "Controller Letter" ) )
	{
		// 3/25/2014 rmd : Fixed the out of range variable. Now save it.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_FLOWSENSE, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
	}
	
	return( fl_struct.controller_letter );
}

/* ---------------------------------------------------------- */
extern UNS_32 FLOWSENSE_get_controller_index( void )
{
	// 9/24/2015 rmd : Returns a ZERO based index 0..11 [MAX_CHAIN_LENGTH-1].

	return( FLOWSENSE_get_controller_letter() - 'A' );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION We are Part Of A Flow-on-a-loop System (POAFS) determines if the controller
    part number contains a -FL (meaning they bought the option) and that they have enabled
    POAFS. Indicating they want several controllers to operate together as one.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the RING_BUFFER_ANALYSIS_TASK when routing
    packets. Also executed within the context of the COMM_MNGR task as part of the token
    generation.

	@RETURN Returns (true) if we are part of a bigger system. (false) otherwise.

	@AUTHOR 2012.03.09 rmd
*/
extern BOOL_32 FLOWSENSE_we_are_poafs( void )
{
	if( !RangeCheck_bool( &fl_struct.POAFS, (false), NULL, "POAFS" ) )
	{
		// 3/25/2014 rmd : Fixed the out of range variable. Now save it.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_FLOWSENSE, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
	}

	// 3/25/2014 rmd : Note qualifying the returned value with the purchased option. Not sure
	// this is the right place to do this. Does the caller always 'want' this? Kind of a dirty
	// little trick.
	return( config_c.purchased_options.option_FL && fl_struct.POAFS );
}

/* ---------------------------------------------------------- */
extern UNS_32 FLOWSENSE_get_num_controllers_in_chain( void )
{
	// 10/24/2018 Ryan : This function only gives the correct number when called by the master
	// controller in the chain, but not by the slaves in the chain. This is because the NOCIC
	// variable is not a sync'd pdata variable.
	
	// ----------
	
	if( !RangeCheck_uint32( &fl_struct.NOCIC, 2, MAX_CHAIN_LENGTH, 2, NULL, "NOCIC" ) )
	{
		// 3/25/2014 rmd : Fixed the out of range variable. Now save it.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_FLOWSENSE, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
	}
	
	return( fl_struct.NOCIC );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION A single controller is its own master. Meaning it is making tokens and
    sending them to itself. In a chain the 'A' controller is the master. Meaning it is the
    one making tokens and sending them to all boxes. The determination of master or not is
    strictly based on the configuration settings. It does not take into account if we are in
    forced or not. That is a detail the comm_mngr task internally accounts for. Prior to the
    assessment we range check our scan char since so much throughout the code depends on
    this value being sane.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the COMM_MNGR task as part of the token
    generation. Also executed within the context of the KEY_PROCESSING_TASK. When a group is
    removed from the list. As part of the decision to make an alert line or not.

	@RETURN (true) if we are a master. (false) otherwise.

	@AUTHOR 2012.03.09 rmd
*/
extern BOOL_32 FLOWSENSE_we_are_a_master_one_way_or_another( void )
{
	BOOL_32	rv;

	// 2012.03.09 rmd : If we are explicitly part of a larger chain then we are the master if
	// our scan char is 'A'. Otherwise (if we are not POAFS) we are the master.
	if( FLOWSENSE_we_are_poafs() )
	{
		rv = (FLOWSENSE_get_controller_letter() == 'A');
	}
	else
	{
		rv = (true);
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Returns (true) if we are master 'A' controller of a true multiple
    controller chain.

	@CALLER MUTEX REQUIREMENTS

    @EXECUTED Executed within the context of the COMM_MNGR task as part of the token
    generation. Also executed within the context of the KEY_PROCESSING_TASK. When a group is
    removed from the list. As part of the decision to make an alert line or not.

    @RETURN (true) is we are truly part of a chain and are the 'A' controller. (false)
    otherwise.

	@AUTHOR 2012.03.09 rmd
*/
extern BOOL_32 FLOWSENSE_we_are_the_master_of_a_multiple_controller_network( void )
{
	// 2012.03.09 rmd : If we are part of a larger chain AND our scan_char is 'A' then we are
	// the master!
	return( (FLOWSENSE_we_are_poafs() == (true)) && (FLOWSENSE_get_controller_letter() == 'A') );
}

/* ---------------------------------------------------------- */
BOOL_32 FLOWSENSE_we_are_a_slave_in_a_chain( void )
{
	// 9/24/2018 rmd : If we are part of a chain AND our scan_char is not 'A', then we are a
	// slave.
	return( FLOWSENSE_we_are_poafs() && (FLOWSENSE_get_controller_letter() != 'A') );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

