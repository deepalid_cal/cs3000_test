/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_MOISTURE_SENSOR_H
#define _INC_MOISTURE_SENSOR_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	<stdint.h>
	
// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	"lpc_types.h"

#include	"pdata_changes.h"

#include	"protocolmsgs.h"

#ifndef _MSC_VER

	#include	"alerts.h"

	#include	"cal_td_utils.h"
	
	#include	"cal_list.h"

	#include	"battery_backed_vars.h"

#endif

#include	"moisture_sensor_recorder.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/10/2014 rmd : Because when the file is FIRST created and a placeholder group is added
// it should be set such that it is not really physically there. Its as if it is an
// uninstalled moisture sensor decoder that is made by default.
#define MOISTURE_SENSOR_PHYSICALLY_AVAILABLE_DEFAULT	(false)

// ----------

#define	MOISTURE_SENSOR_IN_USE_DEFAULT	(false)

// ----------

// 7/11/2018 ajv : Rescaled these values to introduce "DO NOTHING" and "ALERT ONLY". This 
// means the control mode will be programatically changed on the next firmware update 
// introducing these variables.
#define	MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_do_nothing							(0)
#define	MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_alert_only							(1)
#define	MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_stop_at_high_threshold				(2)
#define	MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_start_at_low_threshold				(3)
#define	MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_start_and_stop_using_banded_threshold	(4)

#define	MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_MIN		(MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_do_nothing)
// 10/8/2018 ajv : For now, we don't allow anything other than stopping when the high 
// threshold is crossed. 
#define	MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_MAX		(MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_stop_at_high_threshold)
#define	MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_DEFAULT	(MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_stop_at_high_threshold)

// ----------

// 2/5/2016 rmd : It would be nice to scale the moisture reading output to a 0 to 100 scale.
//
// 9/21/2016 rmd : The raw moisture sensor reading tends to come in around 29 to 30 when
// installed in turf. Adjustements of 1 whole point proved to be too much. Moving you from
// watering estentially all the time to never watering. At least if you want to really fine
// tune. So we decided to scale the SET POINT presented to the user. Richard was running at
// about 30 and we want to scale to a 72 so multiply by 2.4. This does two things. First
// gives the user more control via finer granularity. And two sclaes the reading to our old
// familiar moisture sensor.
//
// 10/5/2018 rmd : In file structure revision 3 we added new floating point variables for
// the setpoint, and changed their scaling to be 0 to 1.0, with a default of 0.32.
// Supposedly this is familiar to the scientific industry?
#define	MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_MIN		(0.0f)
#define	MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_MAX		(1.0f)
#define	MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_DEFAULT	(0.32f)

// 10/24/2018 rmd : Keep the old MIN/MAX/DEFAULT defines for support in updater function.
#define	nlu_MOISTURE_SENSOR_MOISTURE_SET_POINT_UNS32_MIN		(10)
#define	nlu_MOISTURE_SENSOR_MOISTURE_SET_POINT_UNS32_MAX		(100)
#define	nlu_MOISTURE_SENSOR_MOISTURE_SET_POINT_UNS32_DEFAULT	(72)

// ----------

// 2/5/2016 rmd : Maybe these are degrees? Hopefully we can scale the sensor output into
// degrees.
#define	MOISTURE_SENSOR_LOW_TEMP_POINT_MIN				(0)
#define	MOISTURE_SENSOR_LOW_TEMP_POINT_MAX				(72)
#define	MOISTURE_SENSOR_LOW_TEMP_POINT_DEFAULT			(35)

#define	MOISTURE_SENSOR_LOW_TEMP_ACTION_do_nothing		(0)
#define	MOISTURE_SENSOR_LOW_TEMP_ACTION_alert_only		(1)

#define	MOISTURE_SENSOR_LOW_TEMP_ACTION_MIN				(MOISTURE_SENSOR_LOW_TEMP_ACTION_do_nothing)
#define	MOISTURE_SENSOR_LOW_TEMP_ACTION_MAX				(MOISTURE_SENSOR_LOW_TEMP_ACTION_alert_only)
#define	MOISTURE_SENSOR_LOW_TEMP_ACTION_DEFAULT			(MOISTURE_SENSOR_LOW_TEMP_ACTION_do_nothing)

// ----------

// 7/11/2018 ajv : There is a comment above for the LOW_TEMP_POINT that indicates unknown 
// scaling at this time. We need to understand that before allowing users to edit these 
// values. 
#define	MOISTURE_SENSOR_HIGH_TEMP_POINT_MIN				(0)
#define	MOISTURE_SENSOR_HIGH_TEMP_POINT_MAX				(72)
#define	MOISTURE_SENSOR_HIGH_TEMP_POINT_DEFAULT			(45)

#define	MOISTURE_SENSOR_HIGH_TEMP_ACTION_do_nothing		(0)
#define	MOISTURE_SENSOR_HIGH_TEMP_ACTION_alert_only		(1)

#define	MOISTURE_SENSOR_HIGH_TEMP_ACTION_MIN			(MOISTURE_SENSOR_HIGH_TEMP_ACTION_do_nothing)
#define	MOISTURE_SENSOR_HIGH_TEMP_ACTION_MAX			(MOISTURE_SENSOR_HIGH_TEMP_ACTION_alert_only)
#define	MOISTURE_SENSOR_HIGH_TEMP_ACTION_DEFAULT		(MOISTURE_SENSOR_HIGH_TEMP_ACTION_do_nothing)

// ----------

// 4/12/2016 rmd : Set the default to 20 minutes. And maximum to one hour.
#define	MOISTURE_SENSOR_ADDITIONAL_SOAK_SECONDS_MIN		(0)
#define	MOISTURE_SENSOR_ADDITIONAL_SOAK_SECONDS_MAX		(1 * 60 * 60)
#define	MOISTURE_SENSOR_ADDITIONAL_SOAK_SECONDS_DEFAULT	(20 * 60)

// ----------

// 7/11/2018 ajv : Not sure what to set these to yet. Will review with Sara Berhane. 
#define	MOISTURE_SENSOR_LOW_EC_POINT_MIN			(0.0F)
#define	MOISTURE_SENSOR_LOW_EC_POINT_MAX			(10.0F)
#define	MOISTURE_SENSOR_LOW_EC_POINT_DEFAULT		(1.0F)

#define	MOISTURE_SENSOR_LOW_EC_ACTION_do_nothing	(0)
#define	MOISTURE_SENSOR_LOW_EC_ACTION_alert_only	(1)

#define	MOISTURE_SENSOR_LOW_EC_ACTION_MIN			(MOISTURE_SENSOR_LOW_EC_ACTION_do_nothing)
#define	MOISTURE_SENSOR_LOW_EC_ACTION_MAX			(MOISTURE_SENSOR_LOW_EC_ACTION_alert_only)
#define	MOISTURE_SENSOR_LOW_EC_ACTION_DEFAULT		(MOISTURE_SENSOR_LOW_EC_ACTION_do_nothing)

// ---------- 

// 7/11/2018 ajv : Not sure what to set these to yet. Will review with Sara Berhane. 
#define	MOISTURE_SENSOR_HIGH_EC_POINT_MIN			(0.0F)
#define	MOISTURE_SENSOR_HIGH_EC_POINT_MAX			(10.0F)
#define	MOISTURE_SENSOR_HIGH_EC_POINT_DEFAULT		(6.0F)

#define	MOISTURE_SENSOR_HIGH_EC_ACTION_do_nothing	(0)
#define	MOISTURE_SENSOR_HIGH_EC_ACTION_alert_only	(1)

#define	MOISTURE_SENSOR_HIGH_EC_ACTION_MIN			(MOISTURE_SENSOR_HIGH_EC_ACTION_do_nothing)
#define	MOISTURE_SENSOR_HIGH_EC_ACTION_MAX			(MOISTURE_SENSOR_HIGH_EC_ACTION_alert_only)
#define	MOISTURE_SENSOR_HIGH_EC_ACTION_DEFAULT		(MOISTURE_SENSOR_HIGH_EC_ACTION_do_nothing)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct MOISTURE_SENSOR_GROUP_STRUCT	MOISTURE_SENSOR_GROUP_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifdef _MSC_VER

	extern INT_32 MOISTURE_SENSOR_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order );

#endif

// ----------

#ifndef _MSC_VER

	extern void init_file_moisture_sensor( void );
	
	extern void save_file_moisture_sensor( void );
	
	// ----------
	
	extern void *nm_MOISTURE_SENSOR_create_new_group( void );
	
	// ----------
	
	extern void MOISTURE_SENSOR_copy_group_into_guivars( const UNS_32 pmois_index_0 );

	extern void MOISTURE_SENSOR_copy_latest_readings_into_guivars( const UNS_32 pmois_index_0 );

	// ----------
	
	extern UNS_32 MOISTURE_SENSOR_build_data_to_send( UNS_8 **pucp,
										  const UNS_32 pmem_used_so_far,
										  BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
										  const UNS_32 preason_data_is_being_built,
										  const UNS_32 pallocated_memory );
	
	extern void MOISTURE_SENSOR_extract_and_store_group_name_from_GuiVars( void );

	extern void MOISTURE_SENSOR_extract_and_store_changes_from_GuiVars( void );

	extern UNS_32 nm_MOISTURE_SENSOR_extract_and_store_changes_from_comm(	const unsigned char *pucp,
																			const UNS_32 preason_for_change,
																			const BOOL_32 pset_change_bits,
																			const UNS_32 pchanges_received_from
																			#ifdef _MSC_VER
																			, char *pSQL_statements,
																			char *pSQL_search_condition_str,
																			char *pSQL_update_str,
																			char *pSQL_insert_fields_str,
																			char *pSQL_insert_values_str,
																			char *pSQL_single_merge_statement_buf,
																			const UNS_32 pnetwork_ID
																			#endif
																			);
																			
	// ----------
	
	extern UNS_32 MOISTURE_SENSOR_get_index_using_ptr_to_mois_struct( const MOISTURE_SENSOR_GROUP_STRUCT *const pmois );
	
	extern MOISTURE_SENSOR_GROUP_STRUCT *nm_MOISTURE_SENSOR_get_pointer_to_file_moisture_sensor_with_this_box_index_and_decoder_serial_number( const UNS_32 pbox_index, const UNS_32 pdecoder_sn );
	
	extern MOISTURE_SENSOR_GROUP_STRUCT *MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number( UNS_32 pserial_number );
	
	// ----------
	
	extern void *MOISTURE_SENSOR_get_group_at_this_index( const UNS_32 pindex_0 );
	
	extern void *MOISTURE_SENSOR_get_group_with_this_GID( const UNS_32 pgroup_ID );
	
	extern UNS_32 MOISTURE_SENSOR_get_gid_of_group_at_this_index( const UNS_32 pindex_0 );
	
	extern UNS_32 MOISTURE_SENSOR_get_index_for_group_with_this_serial_number( const UNS_32 pserial_num );
		
	extern BOOL_32 MOISTURE_SENSOR_this_decoder_is_in_use_and_physically_available( UNS_32 pserial_number );
	
	// ----------
	
	extern UNS_32 MOISTURE_SENSOR_get_last_group_ID( void );
	
	extern UNS_32 MOISTURE_SENSOR_get_list_count( void );
	
	// ----------
	
	extern BOOL_32 MOISTURE_SENSOR_get_physically_available( MOISTURE_SENSOR_GROUP_STRUCT *const pmois );

	extern BOOL_32 MOISTURE_SENSOR_get_decoder_serial_number( MOISTURE_SENSOR_GROUP_STRUCT *const pmois );
	
	extern UNS_32 MOISTURE_SENSOR_get_num_of_moisture_sensors_connected( void );

	extern BOOL_32 MOISTURE_SENSOR_fill_out_recorder_record( MOISTURE_SENSOR_GROUP_STRUCT *pmois, MOISTURE_SENSOR_RECORDER_RECORD *pmsrr );
	
	// ----------
	
	extern CHANGE_BITS_32_BIT *MOISTURE_SENSOR_get_change_bits_ptr( MOISTURE_SENSOR_GROUP_STRUCT *pmois, const UNS_32 pchange_reason );
	
	// ----------
	
	extern void MOISTURE_SENSOR_clean_house_processing( void );
	
	
	extern void MOISTURE_SENSOR_set_not_physically_available_based_upon_communication_scan_results( void );
	
	
	extern void MOISTURE_SENSOR_set_physically_available_or_create_new_moisture_sensors_as_needed_based_upon_the_discovery_results( void );
	

	extern void MOISTURE_SENSORS_check_each_sensor_for_a_crossed_threshold( void );

	
	extern void MOISTURE_SENSOR_set_bits_on_all_moisture_sensors_to_cause_distribution_in_the_next_token( void );
	
	extern void MOISTURE_SENSOR_on_all_moisture_sensors_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear );
	
	extern void nm_MOISTURE_SENSOR_update_pending_change_bits( const UNS_32 pcomm_error_occurred );
	
	
	extern void nm_MOISTURE_SENSOR_set_new_reading_slave_side_variables( MOISTURE_SENSOR_DECODER_RESPONSE *presponse_ptr, MOISTURE_SENSOR_GROUP_STRUCT *lmois );
	
	extern UNS_8* MOISTURE_SENSOR_find_a_sensor_reading_to_send_to_the_master( UNS_32 *phow_many_bytes_ptr );
	
	extern BOOL_32 MOISTURE_SENSOR_extract_moisture_reading_from_token_response( UNS_8 **pucp_ptr, UNS_32 const pcontroller_index );
	
	// ----------
	
	extern void MOISTURE_SENSORS_initialize__saved__irrigation_control_variables_for_this_moisture_sensor( UNS_32 const psensor_serial_number );
	
	extern void MOISTURE_SENSORS_initialize__transient__control_variables_for_all_sensors( void );
	
	extern void MOISTURE_SENSORS_update__transient__control_variables_for_this_station( IRRIGATION_LIST_COMPONENT *pilc );
	
	extern BOOL_32 MOISTURE_SENSORS_ilc_is_allowed_to_be_removed_from_the_list( IRRIGATION_LIST_COMPONENT const *const pilc );
	
	extern void MOISTURE_SENSORS_check_each_sensor_to_possibly_update__saved__control_variables( void );

	extern BOOL_32 MOISTURE_SENSORS_this_ilc_has_irrigated_the_required_cycles( IRRIGATION_LIST_COMPONENT const * const pilc );
	
	// ----------
	
	extern BOOL_32 MOISTURE_SENSOR_calculate_chain_sync_crc( UNS_32 const pff_name );
	
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

