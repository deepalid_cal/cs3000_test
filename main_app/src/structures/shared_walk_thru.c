/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_SHARED_WALK_THRU_C
#define _INC_SHARED_WALK_THRU_C

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"group_base_file.h"

#include	"cal_td_utils.h"

#include	"change.h"

#ifndef	_MSC_VER

	#include	"alerts.h"

	#include	"app_startup.h"
	
	#include	"cs_mem.h"

	#include	"battery_backed_vars.h"
	
	#include	"flowsense.h"
	
	#include	"flash_storage.h"	

#else

	#include	<stdlib.h>

	#include	"sql_merge.h"

#endif




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define		WALK_THRU_CHANGE_BITFIELD_description_bit			(0)
#define		WALK_THRU_CHANGE_BITFIELD_station_run_time_bit		(1)
#define		WALK_THRU_CHANGE_BITFIELD_delay_before_start_bit	(2)
#define		WALK_THRU_CHANGE_BITFIELD_box_index_0_bit			(3)
#define		WALK_THRU_CHANGE_BITFIELD_stations_bit				(4)
#define		WALK_THRU_CHANGE_BITFIELD_in_use_bit				(5)

// ----------

static char* const WALK_THRU_database_field_names[ (WALK_THRU_CHANGE_BITFIELD_in_use_bit + 1) ] =
{
	"Name",
	"StationRunTime",
	"DelayBeforeStart",				
	"BoxIndex",
	"StationOrder"	// This is actually an array of 127 values - StationOrder000 - StationOrder127
	"InUse"
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
struct WALK_THRU_STRUCT
{
	GROUP_BASE_DEFINITION_STRUCT	base; // includes description which will be the name

	// 2/2/2018 Ryan : The length of time to run each station for. Defaults to 5-minutes with a
	// range of 0.1 - 15.0 minutes.
	UNS_32	station_run_time_minutes_10u;

	// 2/2/2018 Ryan : The amount of time to wait until the first station turns on after a
	// walk-thru schedule is started by the user. When the user initiates a walk-thru, a timer
	// is started. When the timer expires, the walk-thru commences by adding the stations to the
	// irrigation list. Defaults to 15-seconds with a range of 1-second to 5-minutes.
	UNS_32	delay_before_start_seconds;

	// 2/2/2018 Ryan : The controller the stations are all assigned to in this particular
	// walk-thru schedule. range of 0 (box one in chain) to 11 (box 12 in chain)
	UNS_32	box_index_0;

	// 2/2/2018 Ryan : The stations included in the walk thru, organized in sequence, with slot
	// 0 being the first station to be added to the list and slot 127 being the last. Each slot
	// will store a station number, with -1 indicateing no station in the slot. Slots do not
	// need to be filled consecutively. Initialized to all -1's.
	INT_32	stations[ MAX_NUMBER_OF_WALK_THRU_STATIONS ];

	// -------------------------------------------- //
	//  CHANGE BITS
	// -------------------------------------------- //

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;
	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

	// -------------------------------------------- //
	// 2/2/2018 Ryan : Flag that is set true for all in use walk thrus. Once the flag is set to
	// false, the walk thru will be hidden from view and inaccessible to the user.
	BOOL_32	in_use;

};


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void nm_WALK_THRU_set_station_run_time( void *const pgroup,
											 UNS_32 prun_time_seconds,
											 const BOOL_32 pgenerate_change_line_bool,
											 const UNS_32 preason_for_change,
											 const UNS_32 pbox_index_0,
											 const BOOL_32 pset_change_bits,
											 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											 #ifdef _MSC_VER
											 , char *pmerge_update_str,
											 char *pmerge_insert_fields_str,
											 char *pmerge_insert_values_str
											 #endif
										   )
{
	SHARED_set_uint32_with_32_bit_change_bits_group( pgroup,
													 &(((WALK_THRU_STRUCT*)pgroup)->station_run_time_minutes_10u),
													 prun_time_seconds,
													 WALK_THRU_STATION_RUN_TIME_MIN,
													 WALK_THRU_STATION_RUN_TIME_MAX,
													 WALK_THRU_STATION_RUN_TIME_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_WALK_THRU_STATION_RUN_TIME,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((WALK_THRU_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 WALK_THRU_CHANGE_BITFIELD_station_run_time_bit,
													 WALK_THRU_database_field_names[ WALK_THRU_CHANGE_BITFIELD_station_run_time_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_WALK_THRU_set_delay_before_start( void *const pgroup,
											 UNS_32 pdelay_before_start_seconds,
											 const BOOL_32 pgenerate_change_line_bool,
											 const UNS_32 preason_for_change,
											 const UNS_32 pbox_index_0,
											 const BOOL_32 pset_change_bits,
											 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											 #ifdef _MSC_VER
											 , char *pmerge_update_str,
											 char *pmerge_insert_fields_str,
											 char *pmerge_insert_values_str
											 #endif
										   )
{
	SHARED_set_uint32_with_32_bit_change_bits_group( pgroup,
													 &(((WALK_THRU_STRUCT*)pgroup)->delay_before_start_seconds),
													 pdelay_before_start_seconds,
													 WALK_THRU_DELAY_BEFORE_START_MIN,
													 WALK_THRU_DELAY_BEFORE_START_MAX,
													 WALK_THRU_DELAY_BEFORE_START_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_WALK_THRU_DELAY_BEFORE_START,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((WALK_THRU_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 WALK_THRU_CHANGE_BITFIELD_delay_before_start_bit,
													 WALK_THRU_database_field_names[ WALK_THRU_CHANGE_BITFIELD_delay_before_start_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_WALK_THRU_set_box_index_0( void *const pgroup,
											 UNS_32 pnew_box_index_0,
											 const BOOL_32 pgenerate_change_line_bool,
											 const UNS_32 preason_for_change,
											 const UNS_32 pbox_index_0,
											 const BOOL_32 pset_change_bits,
											 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											 #ifdef _MSC_VER
											 , char *pmerge_update_str,
											 char *pmerge_insert_fields_str,
											 char *pmerge_insert_values_str
											 #endif
										   )
{
	SHARED_set_uint32_with_32_bit_change_bits_group( pgroup,
													 &(((WALK_THRU_STRUCT*)pgroup)->box_index_0),
													 pnew_box_index_0,
													 WALK_THRU_BOX_INDEX_MIN,
													 WALK_THRU_BOX_INDEX_MAX,
													 WALK_THRU_BOX_INDEX_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_WALK_THRU_BOX_INDEX_0,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((WALK_THRU_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 WALK_THRU_CHANGE_BITFIELD_box_index_0_bit,
													 WALK_THRU_database_field_names[ WALK_THRU_CHANGE_BITFIELD_box_index_0_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
extern void nm_WALK_THRU_set_stations( void *const pgroup,
												   const UNS_32 pslot_index_0,
												   INT_32	pstation_number,
												   const BOOL_32 pgenerate_change_line_bool,
												   const UNS_32 preason_for_change,
												   const UNS_32 pbox_index_0,
												   const BOOL_32 pset_change_bits,
												   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												   #ifdef _MSC_VER
												   , char *pmerge_update_str,
												   char *pmerge_insert_fields_str,
												   char *pmerge_insert_values_str
												   #endif
												 )
{
	char lfield_name[ 32 ];

	#ifndef _MSC_VER

		INT_32	poriginal_station;

		poriginal_station = ((WALK_THRU_STRUCT*)pgroup)->stations[ pslot_index_0 ];

	#endif

	if( pslot_index_0 < MAX_NUMBER_OF_WALK_THRU_STATIONS )
	{
		// 10/23/2018 wjb : Develop the DB field name.
		snprintf(lfield_name, sizeof(lfield_name), "%s%d", WALK_THRU_database_field_names[ WALK_THRU_CHANGE_BITFIELD_stations_bit ], pslot_index_0);

		if ( SHARED_set_int32_with_32_bit_change_bits_group(	pgroup, 
														&(((WALK_THRU_STRUCT*)pgroup)->stations[ pslot_index_0 ]),
														pstation_number,
														WALK_THRU_SEQUENCE_SLOT_station_number_MIN,
														WALK_THRU_SEQUENCE_SLOT_station_number_MAX,
														WALK_THRU_SEQUENCE_SLOT_station_number_DEFAULT,
														0,
														(false),
														preason_for_change,
														pbox_index_0,
														pset_change_bits,
														pchange_bitfield_to_set,
														&(((WALK_THRU_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
														WALK_THRU_CHANGE_BITFIELD_stations_bit,
														lfield_name
														#ifdef _MSC_VER
														, pmerge_update_str,
														pmerge_insert_fields_str,
														pmerge_insert_values_str
														#endif
														) == (true) )
		{
			#ifndef _MSC_VER

				if( pgenerate_change_line_bool )
				{
					if( poriginal_station != WALK_THRU_SEQUENCE_SLOT_no_station_set )
					{
						Alert_walk_thru_station_added_or_removed( ((WALK_THRU_STRUCT*)pgroup)->box_index_0, ((WALK_THRU_STRUCT*)pgroup)->base.description, poriginal_station, WALK_THRU_STATION_REMOVED, preason_for_change );
					}

					if( pstation_number != WALK_THRU_SEQUENCE_SLOT_no_station_set )
					{
						Alert_walk_thru_station_added_or_removed( ((WALK_THRU_STRUCT*)pgroup)->box_index_0, ((WALK_THRU_STRUCT*)pgroup)->base.description, pstation_number, WALK_THRU_STATION_ADDED, preason_for_change );
					}
				}

			#endif

		}
	}
	else
	{
		#ifndef _MSC_VER

		Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
static void nm_WALK_THRU_set_in_use(	void *const pgroup,
											BOOL_32 pin_use,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0,
											const BOOL_32 pset_change_bits,
											CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											#ifdef _MSC_VER
											, char *pmerge_update_str,
											char *pmerge_insert_fields_str,
											char *pmerge_insert_values_str
											#endif
									   )
{
	SHARED_set_bool_with_32_bit_change_bits_group(pgroup,
												   &(((WALK_THRU_STRUCT*)pgroup)->in_use),
												   pin_use,
												   WALK_THRU_IN_USE_DEFAULT,
												   pgenerate_change_line_bool,
												   CHANGE_WALK_THRU_IN_USE,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((WALK_THRU_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
												   WALK_THRU_CHANGE_BITFIELD_in_use_bit,
												   WALK_THRU_database_field_names[ WALK_THRU_CHANGE_BITFIELD_in_use_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
static void nm_WALK_THRU_store_changes( WALK_THRU_STRUCT *const ptemporary_group,
											  const BOOL_32 pgroup_created,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  const UNS_32 pchanges_received_from,
											  CHANGE_BITS_32_BIT pbitfield_of_changes
											  #ifdef _MSC_VER
											  , char *pSQL_update_str,
											  char *pSQL_insert_fields_str,
											  char *pSQL_insert_values_str
											  #endif
											)
{
	WALK_THRU_STRUCT 	*lgroup;

	CHANGE_BITS_32_BIT				*lchange_bitfield_to_set;

	UNS_32	i;

	// ----------

	#ifndef _MSC_VER
	nm_GROUP_find_this_group_in_list( &walk_thru_group_list_hdr, ptemporary_group, (void**)&lgroup );

	if( lgroup != NULL )
	#else

	// 1/7/2015 ajv : Since the SQL statements manage only updating the value if it's changed,
	// there's no need for us to determine whether the group already exists in the database or
	// not. Therefore, lgroup and ptemporary_group can be the same value.
	lgroup = ptemporary_group;

	#endif
	{
		#ifndef _MSC_VER

			if( pgroup_created == (true) )
			{
				Alert_ChangeLine_Group( CHANGE_GROUP_CREATED, nm_GROUP_get_name( ptemporary_group ), NULL, NULL, NUMBER_OF_CHARS_IN_A_NAME, preason_for_change, pbox_index_0 );
			}

			lchange_bitfield_to_set = WALK_THRU_get_change_bits_ptr( lgroup, pchanges_received_from );

		#else

			// 7/28/2015 ajv : Initialize to NULL since this variable is used by the CommServer but
			// expected to be NULL.
			lchange_bitfield_to_set = NULL;

		#endif

		// 4/24/2015 ajv : Only process the change if it was either initiated via the keypad
		// (locally) or if the change bit was set (when called via communications)
		
		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, WALK_THRU_CHANGE_BITFIELD_description_bit ) == (true)) )
		{
			SHARED_set_name_32_bit_change_bits( lgroup, nm_GROUP_get_name(ptemporary_group), !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set, &lgroup->changes_to_upload_to_comm_server, WALK_THRU_CHANGE_BITFIELD_description_bit
											   #ifdef _MSC_VER
											   , WALK_THRU_database_field_names[ WALK_THRU_CHANGE_BITFIELD_description_bit ], pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											   #endif
											 );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, WALK_THRU_CHANGE_BITFIELD_station_run_time_bit ) == (true)) )
		{
			nm_WALK_THRU_set_station_run_time( lgroup, ptemporary_group->station_run_time_minutes_10u, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											   #ifdef _MSC_VER
											   , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											   #endif
											 );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, WALK_THRU_CHANGE_BITFIELD_delay_before_start_bit ) == (true)) )
		{
			nm_WALK_THRU_set_delay_before_start( lgroup, ptemporary_group->delay_before_start_seconds, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											 #ifdef _MSC_VER
											 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											 #endif
										   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, WALK_THRU_CHANGE_BITFIELD_box_index_0_bit ) == (true)) )
		{
			nm_WALK_THRU_set_box_index_0( lgroup, ptemporary_group->box_index_0, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											 #ifdef _MSC_VER
											 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											 #endif
										   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, WALK_THRU_CHANGE_BITFIELD_stations_bit ) == (true)) )
		{
			for( i = 0; i < MAX_NUMBER_OF_WALK_THRU_STATIONS; i++ )
			{
				nm_WALK_THRU_set_stations( lgroup, ( i ), ptemporary_group->stations[ i ], CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
											#ifdef _MSC_VER
											, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											#endif
											);
			}
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, WALK_THRU_CHANGE_BITFIELD_in_use_bit ) == (true)) )
		{
			#ifndef _MSC_VER

				// 2/11/2016 ajv : If the group was deleted, reorder the list by moving the deleted item to
				// the end of the list.
				if( (lgroup->in_use) && (!ptemporary_group->in_use) )
				{
					nm_ListRemove( &walk_thru_group_list_hdr, lgroup );

					nm_ListInsertTail( &walk_thru_group_list_hdr, lgroup );
				}

			#endif

			nm_WALK_THRU_set_in_use(	lgroup, ptemporary_group->in_use, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											#ifdef _MSC_VER
											, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											#endif
										 );
		}
	}
	#ifndef _MSC_VER
	else
	{
		Alert_group_not_found();
	}
	#endif
}

/* ---------------------------------------------------------- */
extern UNS_32 nm_WALK_THRU_extract_and_store_changes_from_comm( const unsigned char *pucp,
																	  const UNS_32 preason_for_change,
																	  const BOOL_32 pset_change_bits,
																	  const UNS_32 pchanges_received_from
																	  #ifdef _MSC_VER
																	  , char *pSQL_statements,
																	  char *pSQL_search_condition_str,
																	  char *pSQL_update_str,
																	  char *pSQL_insert_fields_str,
																	  char *pSQL_insert_values_str,
																	  char *pSQL_single_merge_statement_buf,
																	  const UNS_32 pnetwork_ID
																	  #endif
																	)
{
	CHANGE_BITS_32_BIT	lbitfield_of_changes;

	WALK_THRU_STRUCT 	*ltemporary_group;

	#ifndef _MSC_VER

	WALK_THRU_STRUCT	*lmatching_group;

	#endif

	UNS_32	lnum_changed_groups;

	UNS_32	lsize_of_bitfield;

//	UNS_32	ltemporary_uns_32;

	BOOL_32	lgroup_created;

	UNS_32	lgroup_id;

	UNS_32	i;

	UNS_32	rv;


	rv = 0;

	lgroup_created = (false);

	// Extract the number of changed groups
	memcpy( &lnum_changed_groups, pucp, sizeof(lnum_changed_groups) );
	pucp += sizeof( lnum_changed_groups );
	rv += sizeof( lnum_changed_groups );

	/*
	// 8/8/2016 rmd : For debug only.
	if( pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE )
	{
		Alert_Message_va( "rcvd from slave: MANUAL PROGRAM CHANGES for %u groups", lnum_changed_groups );
	}
	else
	if( pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER )
	{
		Alert_Message_va( "rcvd from master: MANUAL PROGRAM CHANGES for %u groups", lnum_changed_groups );
	}
	else
	{
		Alert_Message_va( "rcvd from UNK: MANUAL PROGRAM CHANGES for %u groups", lnum_changed_groups );
	}
	*/
	
	// ----------
	
	for( i = 0; i < lnum_changed_groups; ++i )
	{
		memcpy( &lgroup_id, pucp, sizeof(lgroup_id) );
		pucp += sizeof( lgroup_id );
		rv += sizeof( lgroup_id );

		memcpy( &lsize_of_bitfield, pucp, sizeof(lsize_of_bitfield) );
		pucp += sizeof( lsize_of_bitfield );
		rv += sizeof( lsize_of_bitfield );

		memcpy( &lbitfield_of_changes, pucp, sizeof(lbitfield_of_changes) );
		pucp += sizeof( lbitfield_of_changes );
		rv += sizeof( lbitfield_of_changes );

		// 1/7/2015 ajv : When compiling for the CommServer, we build a MERGE statement if any of
		// the station's settings have changed. Therefore, we can completely skip over searching for
		// the station - this will fail anyway since the list doesn't exist in the comm server.
		#ifndef _MSC_VER

			// Find the existing group with the group ID
			lmatching_group = nm_GROUP_get_ptr_to_group_with_this_GID( &walk_thru_group_list_hdr, lgroup_id, (true) );

			// 3/6/2013 ajv : If the specified group doesn't exist yet, we need to
			// create it and assign it the group ID.
			if( lmatching_group == NULL )
			{
				lmatching_group = nm_WAlK_THRU_create_new_group( pchanges_received_from );

				// Force the new Group ID to the passed-in group ID.
				lmatching_group->base.group_identity_number = lgroup_id;

				lgroup_created = (true);

				// ----------

				if( pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER )
				{
					// 5/2/2014 ajv : Any group creation caused by receipt of a
					// token is  for stations not at this box. And therefore
					// we don't have to resend out these changes. The distribution
					// of this activity creating groups and assigning the values
					// STOPS here in this case.
					SHARED_clear_all_32_bit_change_bits( &lmatching_group->changes_to_send_to_master, list_program_data_recursive_MUTEX );

					//Alert_Message_va( "PDATA: ManP %u created cleared", lgroup_id );
				}
				else
				{
					//Alert_Message_va( "PDATA: ManP %u created", lgroup_id );
				}
			}
			else
			{
				//Alert_Message_va( "PDATA: ManP %u found 0x%08x", lgroup_id, lbitfield_of_changes );
			}

		#endif

		#ifndef _MSC_VER

		if( lmatching_group != NULL )

		#endif
		{
			// Allocate memory for, and initialize, the temporary group
			#ifndef _MSC_VER
				ltemporary_group = mem_malloc( sizeof(WALK_THRU_STRUCT) );
			#else
				ltemporary_group = malloc( sizeof(WALK_THRU_STRUCT) );
			#endif

			memset( ltemporary_group, 0x00, sizeof(WALK_THRU_STRUCT) );

			#ifndef _MSC_VER

				// Make a copy of the group. We do this so we can change the settings
				// and pass it to the store_changes routine which will actually update
				// the group's settings.
				memcpy( ltemporary_group, lmatching_group, sizeof(WALK_THRU_STRUCT) );

			#else

				ltemporary_group->base.group_identity_number = lgroup_id;

			#endif

			if( B_IS_SET( lbitfield_of_changes, WALK_THRU_CHANGE_BITFIELD_description_bit ) == (true) )
			{
				memcpy( &ltemporary_group->base.description, pucp, NUMBER_OF_CHARS_IN_A_NAME );
				pucp += NUMBER_OF_CHARS_IN_A_NAME;
				rv += NUMBER_OF_CHARS_IN_A_NAME;
			}

			if( B_IS_SET( lbitfield_of_changes, WALK_THRU_CHANGE_BITFIELD_station_run_time_bit ) == (true) )
			{
				memcpy( &ltemporary_group->station_run_time_minutes_10u, pucp, sizeof(ltemporary_group->station_run_time_minutes_10u) );
				pucp += sizeof( ltemporary_group->station_run_time_minutes_10u );
				rv += sizeof( ltemporary_group->station_run_time_minutes_10u );
			}

			if( B_IS_SET( lbitfield_of_changes, WALK_THRU_CHANGE_BITFIELD_delay_before_start_bit ) == (true) )
			{
				memcpy( &ltemporary_group->delay_before_start_seconds, pucp, sizeof(ltemporary_group->delay_before_start_seconds) );
				pucp += sizeof( ltemporary_group->delay_before_start_seconds );
				rv += sizeof( ltemporary_group->delay_before_start_seconds );
			}

			if( B_IS_SET( lbitfield_of_changes, WALK_THRU_CHANGE_BITFIELD_box_index_0_bit ) == (true) )
			{
				memcpy( &ltemporary_group->box_index_0, pucp, sizeof(ltemporary_group->box_index_0) );
				pucp += sizeof( ltemporary_group->box_index_0 );
				rv += sizeof( ltemporary_group->box_index_0 );
			}

			if( B_IS_SET( lbitfield_of_changes, WALK_THRU_CHANGE_BITFIELD_stations_bit ) == (true) )
			{
				memcpy( &ltemporary_group->stations, pucp, sizeof(ltemporary_group->stations) );
				pucp += sizeof( ltemporary_group->stations );
				rv += sizeof( ltemporary_group->stations );
			}

			if( B_IS_SET( lbitfield_of_changes, WALK_THRU_CHANGE_BITFIELD_in_use_bit ) == (true) )
			{
				memcpy( &ltemporary_group->in_use, pucp, sizeof(ltemporary_group->in_use) );
				pucp += sizeof( ltemporary_group->in_use );
				rv += sizeof( ltemporary_group->in_use );
			}

			// ----------

			#ifdef _MSC_VER

				// 1/16/2015 ajv : Make sure the strings are fully initialized to NULL since we rely on this
				// to determine whether the string is empty or not.
				memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

				// 4/11/2016 ajv : Insert the network ID and GID in case the group doesn't exist in the
				// database yet.
				SQL_MERGE_build_upsert_uint32( pnetwork_ID, SQL_MERGE_include_field_name_in_insert_clause, "NetworkID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
				SQL_MERGE_build_upsert_uint32( ltemporary_group->base.group_identity_number, SQL_MERGE_include_field_name_in_insert_clause, "ProgramGID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

			#endif

			// Now that we've extracted the settings out of the communication
			// structure, store the changes
			nm_WALK_THRU_store_changes( ltemporary_group, lgroup_created, preason_for_change, 0, pset_change_bits, pchanges_received_from, lbitfield_of_changes
											  #ifdef _MSC_VER
											  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											  #endif
											);

			#ifndef _MSC_VER

				mem_free( ltemporary_group );

			#else

				// ----------

				SQL_MERGE_build_search_condition_uint32( "NetworkID", pnetwork_ID, pSQL_search_condition_str );
				SQL_MERGE_build_search_condition_uint32( "ProgramGID", ltemporary_group->base.group_identity_number, pSQL_search_condition_str );

				SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements( DATABASE_TABLE_NAME_WALK_THRU, pSQL_search_condition_str, SQL_MERGE_include_field_name_in_insert_clause, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf );

				// ----------

				free( ltemporary_group );

			#endif
		}
		#ifndef _MSC_VER
		else
		{
			Alert_group_not_found();
		}
		#endif
	}

	#ifndef _MSC_VER

		// Only save the file if we've extracted information out of the ucp. This is
		// a sanity check to ensure the bit is only set if data was included.
		if( rv > 0 )
		{
			// 5/13/2015 ajv : We want to save the file in the following cases:
			// 
			//   - A message from the CommServer since we've responded that we received it and need to
			//     ensure it's saved.
			//   - A token response from the slave to the master
			//   - A token from the master to the slave
			//   
			// We DO NOT want to save the file when a token is received by the master since we've
			// already saved the file for one of the reasons above. This will otherwise result in a
			// double save for each change - once when it comes in and once when it's received when the
			// master distributes the changes back out.
			if( (pchanges_received_from == CHANGE_REASON_CENTRAL_OR_MOBILE) ||
				(pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE) ||
				((pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER) && (!FLOWSENSE_we_are_a_master_one_way_or_another())) )
			{
				// 12/10/2014 ajv : Schedule a save rather than saving immediately to prevent stacking of
				// saves. Additionally, set a relatively short time to hel cover cases where users make a
				// lot of changes in a short period.
				FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_WALK_THRU, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
			}
		}
		else
		{
			Alert_bit_set_with_no_data();
		}

	#endif

	return ( rv );
}

#ifdef _MSC_VER

/* ---------------------------------------------------------- */
extern INT_32 WALK_THRU_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order )
{
	WALK_THRU_STRUCT	*lgroup;

	INT_32	rv;

	char stationOrder[3];

	INT_32	stationOrderInt;

	INT_32	Station_order_column_size = strlen(WALK_THRU_database_field_names[WALK_THRU_CHANGE_BITFIELD_stations_bit]);

	// ----------

	// 4/15/2016 ajv : In most cases, we're going to pass back an array_index of 0. However, in
	// the event the value in question is part of an array, we're going to pass back the actual
	// index so that the CommServer can sort not just on the bit number, but also the
	// array_index for values that share the same bit number.
	*psecondary_sort_order = 0;

	rv = -1;

	// ----------

	if( strncmp( pfield_name, WALK_THRU_database_field_names[ WALK_THRU_CHANGE_BITFIELD_description_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WALK_THRU_CHANGE_BITFIELD_description_bit;
		*psize_of_var_ptr = sizeof(lgroup->base.description);
	}
	else if ( strncmp( pfield_name, WALK_THRU_database_field_names[ WALK_THRU_CHANGE_BITFIELD_station_run_time_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WALK_THRU_CHANGE_BITFIELD_station_run_time_bit;
		*psize_of_var_ptr = sizeof(lgroup -> station_run_time_minutes_10u);
	}
	else if (strncmp(pfield_name, WALK_THRU_database_field_names[WALK_THRU_CHANGE_BITFIELD_delay_before_start_bit], NUMBER_OF_CHARS_IN_A_NAME) == 0)
	{
		rv = WALK_THRU_CHANGE_BITFIELD_delay_before_start_bit;
		*psize_of_var_ptr = sizeof(lgroup->delay_before_start_seconds);
	}
	else if (strncmp(pfield_name, WALK_THRU_database_field_names[WALK_THRU_CHANGE_BITFIELD_box_index_0_bit], NUMBER_OF_CHARS_IN_A_NAME) == 0)
	{
		rv = WALK_THRU_CHANGE_BITFIELD_box_index_0_bit;
		*psize_of_var_ptr = sizeof(lgroup->box_index_0);
	}
	// 10/23/2018 rmd : NOTE - only compare up to the base common part of the field name,
	// remember, the actual database field name has a number appended to the base name.
	else if (strncmp(pfield_name, WALK_THRU_database_field_names[WALK_THRU_CHANGE_BITFIELD_stations_bit], (Station_order_column_size - 3)) == 0)
	{
		// 10/23/2018 wjb : Note the above condition is not checking the entire length of the field
		// name. This is because there are a 128 numbers of station order in this array.

		// take the last three digits from the "StationOrderXXX", which will tell us the index number 
		// in the station array.
		stationOrder[0] = pfield_name[Station_order_column_size - 3];
		stationOrder[1] = pfield_name[Station_order_column_size - 2];
		stationOrder[2] = pfield_name[Station_order_column_size - 1];

		stationOrderInt = atoi(stationOrder);

		*psecondary_sort_order = stationOrderInt;

		rv = WALK_THRU_CHANGE_BITFIELD_stations_bit;
		*psize_of_var_ptr = sizeof(lgroup->stations[0]);
	}
	else if( strncmp( pfield_name, WALK_THRU_database_field_names[ WALK_THRU_CHANGE_BITFIELD_in_use_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WALK_THRU_CHANGE_BITFIELD_in_use_bit;
		*psize_of_var_ptr = sizeof(lgroup->in_use);
	}

	return( rv );
}

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

