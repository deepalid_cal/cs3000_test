/*  file = flowsense.h                        03.24.2014 ajv  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_FLOWSENSE_H
#define _INC_FLOWSENSE_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct FLOWSENSE_STRUCT	FLOWSENSE_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_file_flowsense( void );

extern void save_file_flowsense( void );

// ---------------------

extern void FLOWSENSE_copy_settings_into_guivars( void );

extern void FLOWSENSE_extract_and_store_changes_from_GuiVars( const BOOL_32 pforce_scan );

// ---------------------

extern UNS_32 FLOWSENSE_get_controller_letter( void );

extern UNS_32 FLOWSENSE_get_controller_index( void );

extern BOOL_32 FLOWSENSE_we_are_poafs( void );

extern UNS_32 FLOWSENSE_get_num_controllers_in_chain( void );

// ---------------------

extern BOOL_32 FLOWSENSE_we_are_a_master_one_way_or_another( void );

extern BOOL_32 FLOWSENSE_we_are_the_master_of_a_multiple_controller_network( void );

extern BOOL_32 FLOWSENSE_we_are_a_slave_in_a_chain( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

