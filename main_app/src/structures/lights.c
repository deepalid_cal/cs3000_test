
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"lights.h"

#include	"e_lights.h"

#include	"bithacks.h"

#include	"cal_math.h"

#include	"change.h"

#include	"comm_mngr.h"

#include	"cs_mem.h"

#include	"dialog.h"

#include	"group_base_file.h"

#include	"flash_storage.h"

#include	"flowsense.h"

#include	"foal_comm.h"

#include	"irrigation_system.h"

#include	"pdata_changes.h"

#include	"range_check.h"

#include	"shared.h"

#include	"tpmicro_comm.h"

#include	"app_startup.h"

#include	"r_alerts.h"

#include	"epson_rx_8025sa.h"

#include	"tpmicro_data.h"

#include	"irri_lights.h"

#include	"shared_lights.c"

#include	"controller_initiated.h"

#include	"chain_sync_vars.h"

#include	"chain_sync_funcs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static const char LIGHTS_FILENAME[] =		"LIGHTS";

MIST_LIST_HDR_TYPE							light_list_hdr;

char const LIGHTS_DEFAULT_NAME[] =		 	"(description not yet set)";

DATE_TIME g_LIGHTS_initial_date_time;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void nm_LIGHTS_set_default_values( void *const plight, const BOOL_32 pset_change_bits );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 1/26/2015 rmd : The structure revision number. If the structure definition changes this
// value is to be changed. Typically incremented by 1. Then in the updater function handle
// the change specifics whatever they may be. Generally meaning initialize newly added
// values. But could be more extensive than that.
#define	LIGHTS_LATEST_FILE_REVISION		(0)

// ----------

const UNS_32 light_list_item_sizes[ LIGHTS_LATEST_FILE_REVISION + 1 ] =
{
	// 1/26/2015 rmd : In order for this array to contain OLDER list item sizes the implication
	// is that the older structure definitions are to be retained. That means the newer
	// structure definition has a different name.

	// ----------
	
	// 1/30/2015 rmd : Revision 0 structure.
	sizeof( LIGHT_STRUCT ),

};

/* ---------------------------------------------------------- */
static void nm_light_structure_updater( UNS_32 pfrom_revision )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only when
	// the file revision is changed. The caller is to be holding the list_lights_recursive_MUTEX.
}

/* ---------------------------------------------------------- */
extern void init_file_LIGHTS( void )
{
	FLASH_FILE_initialize_list_and_find_or_create_group_list_file(	FLASH_INDEX_1_GENERAL_STORAGE,
																	LIGHTS_FILENAME,
																	LIGHTS_LATEST_FILE_REVISION,
																	&light_list_hdr,
																	light_list_item_sizes,
																	list_lights_recursive_MUTEX,			
																	&nm_light_structure_updater,
																	&nm_LIGHTS_set_default_values,
																	(char*)LIGHTS_DEFAULT_NAME,
																	FF_LIGHTS );	
}

/* ---------------------------------------------------------- */
extern void save_file_LIGHTS( void )
{
	// 9/9/2015 mpd : TBD: Shouldn't there be protection around all the flash functions?
	//xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE,
															LIGHTS_FILENAME,
															LIGHTS_LATEST_FILE_REVISION,
															&light_list_hdr,
															sizeof( LIGHT_STRUCT ),
															list_lights_recursive_MUTEX,			
															FF_LIGHTS );							
}

/* ---------------------------------------------------------- */
/**
 * Sets the default values for a LIGHT group. If the passed in group is NULL, an
 * error is raised and this function takes no further action.
 * 
 * @mutex Calling function must have the light_recursive_MUTEX mutex to ensure the
 *  	  group whose values are being set isn't deleted during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param plight A pointer to the group to set.
 *
 * @author 8/11/2011 Adrianusv
 *
 * @revisions
 *    8/11/2011  Initial release
 *    2/10/2012  Added check to ensure passed in group is not NULL
 *    4/5/2012   Added initialization of change bits
 */
static void nm_LIGHTS_set_default_values( void *const plight, const BOOL_32 pset_change_bits )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	UNS_32	box_index_0;

	UNS_32	day_index;

	// ----------

	box_index_0 = FLOWSENSE_get_controller_index();

	// ------------------------

	if( plight != NULL )
	{
		xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

		// 5/14/2014 ajv : Clear all of the change bits for LIGHT since it's just
		// been created. We'll then explicitly set each bit based upon whether
		// pset_change_bits is true or not.
		SHARED_clear_all_32_bit_change_bits( &((LIGHT_STRUCT *)plight)->changes_to_send_to_master, list_lights_recursive_MUTEX );					
		SHARED_clear_all_32_bit_change_bits( &((LIGHT_STRUCT *)plight)->changes_to_distribute_to_slaves, list_lights_recursive_MUTEX );				
		SHARED_clear_all_32_bit_change_bits( &((LIGHT_STRUCT *)plight)->changes_uploaded_to_comm_server_awaiting_ACK, list_lights_recursive_MUTEX );

		// 1/20/2015 ajv : However, we should explicitly set ALL bits for the Comm Server to ensure
		// changes are uploaded on a clean start.
		SHARED_set_all_32_bit_change_bits( &((LIGHT_STRUCT *)plight)->changes_to_upload_to_comm_server, list_lights_recursive_MUTEX );			
		
		// ------------------------

		// 5/2/2014 ajv : When setting the default values, always set the changes to
		// be sent to the master. Once the master receives the changes, it will
		// distribute them back out to the slaves.
		lchange_bitfield_to_set = &((LIGHT_STRUCT *)plight)->changes_to_send_to_master;

		// ------------------------

		nm_LIGHTS_set_name( plight, (char *)LIGHTS_DEFAULT_NAME, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		// ----------

		// 9/29/2015 ajv : Note that the box index, and output index must be set properly after a
		// new group is created based on the actual values.
		nm_LIGHTS_set_box_index( plight, box_index_0, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_LIGHTS_set_output_index( plight, 0, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		// ----------

		nm_LIGHTS_set_physically_available( plight, LIGHTS_PHYSICALLY_AVAILABLE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		for( day_index = 0; day_index < LIGHTS_SCHEDULE_DAYS_MAX; ++day_index )
		{
			nm_LIGHTS_set_start_time_1( plight, day_index, LIGHTS_START_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

			nm_LIGHTS_set_start_time_2( plight, day_index, LIGHTS_START_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

			nm_LIGHTS_set_stop_time_1( plight, day_index, LIGHTS_STOP_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

			nm_LIGHTS_set_stop_time_2( plight, day_index, LIGHTS_STOP_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
		}

		xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );
	}
	else
	{
		Alert_func_call_with_null_ptr();
	}
}

/* ---------------------------------------------------------- */
/**
 * Creates a new LIGHT group and adds it to the LIGHT list.
 * 
 * @mutex Calling function must have the list_lights_recursive_MUTEX to ensure the
 *        list isn't modified during this process.
 *
 * @executed Executed within the context of the key processing task when the
 *  		 user creates a new group; within the context of the CommMngr task
 *  		 when a new group is created using a central or handheld radio
 *  		 remote.
 * 
 * @return void* A pointer to the newly created group.
 * 
 * @author 8/11/2011 Adrianusv
 * 
 * @revisions
 *    8/11/2011 Initial release
 */
extern void *nm_LIGHTS_create_new_group( const UNS_32 pbox_index_0, const UNS_32 output_index )
{
	LIGHT_STRUCT *llight, *lnext_light;

	lnext_light = nm_ListGetFirst( &light_list_hdr );

	while( lnext_light != NULL )
	{
		// 7/20/2015 mpd : If the box index of the light to be created and added to the list is less
		// than the next one, stop and it insert here.
		if( pbox_index_0 < LIGHTS_get_box_index( lnext_light ) )
		{
			break;
		}
		else if( pbox_index_0 == LIGHTS_get_box_index( lnext_light ) )
		{
			// 7/20/2015 mpd : If the box index is the same, sort by output index.
			if( output_index < LIGHTS_get_output_index( lnext_light ) )
			{
				// 7/20/2015 mpd : Insert the light here.
				break;
			}
		}

		lnext_light = nm_ListGetNext( &light_list_hdr, lnext_light );
	}

	// 11/19/2014 ajv : No need to check if lnext_light is NULL. If it is, the new LIGHT is
	// automatically added to the end of the list.
	llight = nm_GROUP_create_new_group( &light_list_hdr, (char*)LIGHTS_DEFAULT_NAME, &nm_LIGHTS_set_default_values, sizeof(LIGHT_STRUCT), (true), lnext_light );

	return( llight );
}

/* ---------------------------------------------------------- */
/**
 * This function runs through the LIGHT list and copies the contents of any changed LIGHTs to
 * the passed in ucp.
 * 
 * @executed Executed within the context of the CommMngr task as a token, token response, or
 * Program Data message to the Comm Server is being built.
 * 
 * @param pucp A pointer to the pointer where the program data is being built.
 * 
 * @param pmem_used_so_far The amount of memory used so far. Once this reaches our limit for
 * a single token (MEMORY_TO_ALLOCATE_FOR_SENDING_CHANGES), processing of the remaining
 * changes stops until it's time to build the next token or token response.
 * 
 * @param pmem_used_so_far_is_less_than_allocated_memory A pointer to the flag which
 * indicates whether we're allowed to add more changes to the token or token response.
 * 
 * @param preason_data_is_being_built The reason why the data is being built to send. Based
 * upon this reason, we examine a different change bit structure to determine what to
 * send. Valid values are CHANGE_REASON_SYNC_SENDING_TO_MASTER,
 * CHANGE_REASON_SYNC_DISTRIBUTING_TO_SLAVES, CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER.
 * 
 * @return UNS_32 The total size of the added data. This is used to determine whether any
 * changes were detected so the appropriate file change bit can be set as well as to
 * determine how much memory must be allocated to store the changes.
 *
 * @author 9/14/2011 Adrianusv
 */
extern UNS_32 LIGHTS_build_data_to_send( UNS_8 **pucp,
									  const UNS_32 pmem_used_so_far,
									  BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
									  const UNS_32 preason_data_is_being_built,
									  const UNS_32 pallocated_memory )
{
	CHANGE_BITS_32_BIT	*lbitfield_of_changes_to_use_to_determine_what_to_send;

	CHANGE_BITS_32_BIT	lbitfield_of_changes_to_send;

	LIGHT_STRUCT	*llight;

	UNS_8	*llocation_of_num_changed_lights;

	UNS_8	*llocation_of_bitfield;

	// 1/14/2015 ajv : Track how many LIGHTs are added to the message.
	UNS_32	lnum_changed_lights;

	// Store the amount of memory used for current LIGHT. This is compared to the LIGHT's overhead
	// to determine whether any of the LIGHT's values were actually added or not.
	UNS_32	lmem_used_by_this_light;

	// Determine how much overhead each LIGHT requires in case we add the overhead and don't have
	// enough room to add of the actual changes.
	UNS_32	lmem_overhead_per_light;

	UNS_32 day_index;

	UNS_32	rv;

	// ----------

	rv = 0;

	lnum_changed_lights = 0;

	// ----------

	// 1/14/2015 ajv : Take the appropriate mutex to ensure no LIGHTs are added or removed during
	// this process.
	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	// ----------

	llight = nm_ListGetFirst( &light_list_hdr );

	// 1/14/2015 ajv : Loop through each LIGHT and count how many LIGHTs have change bits set. This
	// is really just used to determine whether there's at least one LIGHT which needs to be
	// examined.
	while( llight != NULL )
	{
		if( *(LIGHTS_get_change_bits_ptr( llight, preason_data_is_being_built )) != CHANGE_BITS_32_BIT_ALL_CLEAR )
		{
			lnum_changed_lights++;

			// 1/14/2015 ajv : Since we don't actually care about the number of LIGHTs that have changed
			// at this point - just that at least one has changed - we can break out of the loop now.
			break;
		}

		llight = nm_ListGetNext( &light_list_hdr, llight );
	}

	// ----------

	if( lnum_changed_lights > 0 )
	{
		// 1/14/2015 ajv : Initialize the changed LIGHTs back to 0. Since not all of the LIGHTs may
		// be added due to token size limitations, we need to ensure that the receiving party only
		// processes the number of LIGHTs actually added to the message.
		lnum_changed_lights = 0;

		// ----------

		// 1/14/2015 ajv : Allocate space to store the number of changed LIGHTs that are included in
		// the message so the receiving party can correctly parse the data. This will be filled in
		// later once we have an actual count.
		rv = PDATA_allocate_space_for_num_changed_groups_in_pucp( pucp, &llocation_of_num_changed_lights, pmem_used_so_far, pmem_used_so_far_is_less_than_allocated_memory, pallocated_memory );

		// ----------

		// 2/9/2016 rmd : Well there was at least enough space for the 2 bytes for the count of
		// number of changed groups.
		if( *pmem_used_so_far_is_less_than_allocated_memory )
		{
			llight = nm_ListGetFirst( &light_list_hdr );

			while( llight != NULL )
			{
				lbitfield_of_changes_to_use_to_determine_what_to_send = LIGHTS_get_change_bits_ptr( llight, preason_data_is_being_built );

				// 1/14/2015 ajv : If this LIGHT has changed...
				if( *lbitfield_of_changes_to_use_to_determine_what_to_send != CHANGE_BITS_32_BIT_ALL_CLEAR )
				{
					// 2/9/2016 rmd : The OVERHEAD is the box index, the output index, an UNS_32 indicating the
					// size of the bitfield, and the 32-bit change bitfield itself.
					lmem_overhead_per_light = (4 * sizeof(UNS_32));

					// 1/14/2015 ajv : If we still have room available in the message for the overhead, add it
					// now.
					if( *pmem_used_so_far_is_less_than_allocated_memory && ((pmem_used_so_far + rv + lmem_overhead_per_light) < pallocated_memory) )
					{
						PDATA_copy_var_into_pucp( pucp, &llight->box_index_0, sizeof(llight->box_index_0) );

						PDATA_copy_var_into_pucp( pucp, &llight->output_index_0, sizeof(llight->output_index_0) );

						PDATA_copy_bitfield_info_into_pucp( pucp, sizeof(CHANGE_BITS_32_BIT), sizeof(UNS_32), &llocation_of_bitfield ); 
					}
					else
					{
						// 9/19/2013 ajv : Stop processing if we've already filled out the token with the maximum
						// amount of data to send.
						*pmem_used_so_far_is_less_than_allocated_memory = (false);

						// 2/11/2016 rmd : No overhead added so leave pucp alone.
						
						break;
					}

					lmem_used_by_this_light = lmem_overhead_per_light;

					// ----------

					// Initialize the bit field
					lbitfield_of_changes_to_send = 0x00;

					lmem_used_by_this_light += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							LIGHTS_CHANGE_BITFIELD_description_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&llight->changes_uploaded_to_comm_server_awaiting_ACK,
							&llight->base.description,
							NUMBER_OF_CHARS_IN_A_NAME,
							(lmem_used_by_this_light + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_light += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							LIGHTS_CHANGE_BITFIELD_box_index_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&llight->changes_uploaded_to_comm_server_awaiting_ACK,
							&llight->box_index_0,
							sizeof(llight->box_index_0),
							(lmem_used_by_this_light + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_light += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							LIGHTS_CHANGE_BITFIELD_output_index_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&llight->changes_uploaded_to_comm_server_awaiting_ACK,
							&llight->output_index_0,
							sizeof(llight->output_index_0),
							(lmem_used_by_this_light + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_light += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							LIGHTS_CHANGE_BITFIELD_physically_available_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&llight->changes_uploaded_to_comm_server_awaiting_ACK,
							&llight->light_physically_available,
							sizeof(llight->light_physically_available),
							(lmem_used_by_this_light + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					for( day_index = 0; day_index < LIGHTS_SCHEDULE_DAYS_MAX; ++day_index )
					{
						lmem_used_by_this_light += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							( LIGHTS_CHANGE_BITFIELD_day_00_bit + day_index ),
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&llight->changes_uploaded_to_comm_server_awaiting_ACK,
							&llight->daily_schedule[ day_index ],
							sizeof( llight->daily_schedule[ day_index ] ),
							( lmem_used_by_this_light + rv + pmem_used_so_far ),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );
					}

					// ----------

					// 2/10/2016 rmd : If we saw any changed bits and ACTUALLY ADDED CONTENT BEYOND THE ORIGINAL
					// OVERHEAD then complete out the message by placing the bitfield in the overhead and
					// updating function housekeeping.
					if( lmem_used_by_this_light > lmem_overhead_per_light )
					{
						// 2/10/2016 rmd : Bump the count of changed list items in the message. Place the bitfield
						// of changes into the overhead. And update the return value.
						lnum_changed_lights += 1;
						
						memcpy( llocation_of_bitfield, &lbitfield_of_changes_to_send, sizeof(lbitfield_of_changes_to_send) );
						
						rv += lmem_used_by_this_light;
					}
					else
					{
						// 2/10/2016 rmd : Else back out from the message the overhead already added.
						*pucp -= lmem_overhead_per_light;
					}
					
				}

				llight = nm_ListGetNext( &light_list_hdr, llight );
			}

			// ----------
			
			// 1/14/2015 ajv : If we've added the number of changes but didn't actually add any data
			// because we hit our limit, back out the number of changes.
			if( lnum_changed_lights > 0 )
			{
				memcpy( llocation_of_num_changed_lights, &lnum_changed_lights, sizeof(lnum_changed_lights) );
			}
			else
			{
				// 2/10/2016 rmd : If we didn't add any content beyond moving the msg pointer over where the
				// number of changed groups goes, then put the pointer back and return a 0 to indicate
				// nothing added.
				*pucp -= sizeof( lnum_changed_lights );

				rv = 0;
			}
		}
		else
		{
			// 2/11/2016 rmd : We couldn't do anything, meaning couldn't even add in the
			// num_of_changed_sensors. In that case RV is still 0 and pucp hasn't moved so nothing to
			// do here.
		}
	}

	// ----------

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Saves the 4 GuiVar times to the appropriate location in the 14 day schedule.
 * 
 * @param void
 *
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern void LIGHTS_copy_guivars_to_daily_schedule( const UNS_32 pday_index )
{
	// 7/15/2015 mpd : Copy the 4 GuiVars into the daily schedule on the appropriate day. Times are saved in seconds; 
	// modified and displayed in minutes.
	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ pday_index ].start_time_1_in_seconds = ( GuiVar_LightsStartTime1InMinutes * 60 );
	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ pday_index ].start_time_2_in_seconds = ( GuiVar_LightsStartTime2InMinutes * 60 );
	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ pday_index ].stop_time_1_in_seconds  = ( GuiVar_LightsStopTime1InMinutes  * 60 );
	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ pday_index ].stop_time_2_in_seconds  = ( GuiVar_LightsStopTime2InMinutes  * 60 );
}

/* ---------------------------------------------------------- */
/**
 * Loads the 4 GuiVar times from the appropriate location in the 14 day schedule. Sets the enabled flags.
 * 
 * @param void
 *
 * @return void
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern void LIGHTS_copy_daily_schedule_to_guivars( const UNS_32 pday_index )
{
	// 7/15/2015 mpd : Copy the daily schedule on the appropriate day to the 4 GuiVars. Times are saved in seconds; 
	// modified and displayed in minutes.
	GuiVar_LightsStartTime1InMinutes = ( g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ pday_index ].start_time_1_in_seconds / 60 );
	GuiVar_LightsStartTime2InMinutes = ( g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ pday_index ].start_time_2_in_seconds / 60 );
	GuiVar_LightsStopTime1InMinutes  = ( g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ pday_index ].stop_time_1_in_seconds  / 60 );
	GuiVar_LightsStopTime2InMinutes  = ( g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ pday_index ].stop_time_2_in_seconds  / 60 );

	// 7/15/2015 mpd : These Booleans control the display of time values or the text string "off".
	GuiVar_LightsStartTimeEnabled1 = !( GuiVar_LightsStartTime1InMinutes == LIGHTS_OFF_TIME_IN_MINUTES );
	GuiVar_LightsStartTimeEnabled2 = !( GuiVar_LightsStartTime2InMinutes == LIGHTS_OFF_TIME_IN_MINUTES );
	GuiVar_LightsStopTimeEnabled1  = !( GuiVar_LightsStopTime1InMinutes  == LIGHTS_OFF_TIME_IN_MINUTES );
	GuiVar_LightsStopTimeEnabled2  = !( GuiVar_LightsStopTime2InMinutes  == LIGHTS_OFF_TIME_IN_MINUTES );
}

/* ---------------------------------------------------------- */
/**
 * Populates the group name string with a short or long version of the light name.
 * 
 * @executed Executed within the context of the display processing task when a
 *           new screen is being drawn.
 * 
 * @param plight_index_0 The 0 - 47 index of the light.
 *
 * @param long_format Indicates which format to use. A (true) value provides the long string.
 *
 * @author 8/21/2015 mpd
 */
extern void LIGHTS_populate_group_name( const UNS_32 plight_index_0, const BOOL_32 long_format )
{
	LIGHT_STRUCT *llight;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	llight = LIGHTS_get_light_at_this_index( plight_index_0 );

	if( long_format )
	{
		// "Light A1: Left Field" 
		snprintf( GuiVar_GroupName, sizeof(GuiVar_GroupName), "Light %c%d: %s", (llight->box_index_0 + 'A'), GuiVar_LightsOutput, nm_GROUP_get_name( llight ) );
	}
	else
	{
		// "Left Field" 
		strlcpy( GuiVar_GroupName, nm_GROUP_get_name( llight ), sizeof(GuiVar_GroupName) );
	}

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
/**
 * Copies the contents of the LIGHT_STRUCT for the specified light into GuiVars.
 * 
 * @executed Executed within the context of the display processing task when a
 *           new screen is being drawn.
 * 
 * @param plight_index_0 The 0 - 47 index of the light.
 *
 * @author 8/21/2015 mpd
 */
extern void LIGHTS_copy_light_struct_into_guivars( const UNS_32 plight_index_0 )
{
	LIGHT_STRUCT *llight;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	llight = LIGHTS_get_light_at_this_index( plight_index_0 );

	if( llight != NULL )
	{
		g_GROUP_ID = nm_GROUP_get_group_ID( llight );

		GuiVar_LightsBoxIndex_0 = llight->box_index_0;

		GuiVar_LightIsPhysicallyAvailable = llight->light_physically_available;

		GuiVar_LightsOutputIndex_0 = llight->output_index_0;

		GuiVar_LightsOutput = ( llight->output_index_0 + 1 );

		// Get the current status of this light from the IRRI side.
		GuiVar_LightIsEnergized = IRRI_LIGHTS_light_is_energized( plight_index_0 );

		NETWORK_CONFIG_get_controller_name_str_for_ui( GuiVar_LightsBoxIndex_0, (char*)&GuiVar_StationInfoControllerName );
	}
	else
	{
		Alert_group_not_found();
	}

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );
}

// 9/3/2015 mpd : Fix comment block.
/* ---------------------------------------------------------- */
/**
 * Copies the contents of the LIGHT_STRUCT for the specified light into GuiVars.
 * 
 * @executed Executed within the context of the display processing task when a
 *           new screen is being drawn.
 * 
 * @param plight_index_0 The 0 - 47 index of the light.
 *
 * @author 8/21/2015 mpd
 */
extern void LIGHTS_copy_file_schedule_into_global_daily_schedule( const UNS_32 plight_index_0 )
{
	LIGHT_STRUCT *llight;

	UNS_32 day_index;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	llight = LIGHTS_get_light_at_this_index( plight_index_0 );

	// 7/15/2015 mpd : The "g_LIGHTS_DailySchedule" array contains a complete copy of the 14 day daily schedule for the 
	// selected light. This allows the user to navigate through days without the GUI having to perform constant saves to 
	// the LIGHTS file. The day structure pointed to by "GuiVar_LightsDayIndex" is used to populate the 4 GuiVar times 
	// on the screen. The 4 GuiVar display times are in minutes. Every other time is in seconds.
	for( day_index = 0; day_index < LIGHTS_SCHEDULE_DAYS_MAX; ++day_index )
	{
		// 7/14/2015 mpd : Time values for LIGHTS match what was done for STATIONS. Time is stored in seconds even 
		// though users are only allowed 10 minute granularity in the GUI. Zero is midnight. The last valid time is 
		// 11:50PM which is 85800 seconds. "LIGHTS_24_HOURS_IN_SECONDS" is 86400 seconds. 
		g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ day_index ].start_time_1_in_seconds = llight->daily_schedule[ day_index ].start_time_1_in_seconds;
		g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ day_index ].start_time_2_in_seconds = llight->daily_schedule[ day_index ].start_time_2_in_seconds;
		g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ day_index ].stop_time_1_in_seconds  = llight->daily_schedule[ day_index ].stop_time_1_in_seconds;
		g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ day_index ].stop_time_2_in_seconds  = llight->daily_schedule[ day_index ].stop_time_2_in_seconds;
	}

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Stores the group name. This routine is called when the user presses BACK to
 * close the on-screen keyboard so the list of groups is updated with the new
 * name.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user presses BACK to close the on-screen keyboard.
 *
 * @author 12/11/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void LIGHTS_extract_and_store_group_name_from_GuiVars( void )
{
	LIGHT_STRUCT	*llight;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	llight = LIGHTS_get_light_at_this_index( g_GROUP_list_item_index );

	if( llight != NULL )
	{
		SHARED_set_name_32_bit_change_bits( llight, GuiVar_GroupName, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, LIGHTS_get_change_bits_ptr( llight, CHANGE_REASON_KEYPAD ), &llight->changes_to_upload_to_comm_server, LIGHTS_CHANGE_BITFIELD_description_bit );
	}

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

}

// 9/3/2015 mpd : TODO: Update comment for the daily schedule global.
/* ---------------------------------------------------------- */
/**
 * Copies the contents of the GuiVars, used for editing by the easyGUI library,
 * into a temporary structure which is used for comparison with a group in the
 * list to identify changes.  Once this is done, the temporary group is passed
 * into the _store_changes routine where the settings are stored into memory.
 * 
 * @executed Executed within the context of the display processing task.
 * 
 * @author 8/11/2011 Adrianusv
 *
 * @revisions
 *    8/11/2011 Initial release
 *    4/5/2012  Added passing of (false) into set routine to indiciate the
 *  			change was made locally so change bits are set properly.
 *  			Modified so all memory allocation, storing of changes, and
 *  			memory deallocation are handled within this routine.
 */
extern void LIGHTS_extract_and_store_changes_from_GuiVars( void )
{
	LIGHT_STRUCT	*llight;

	UNS_32 day_index = 0;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	// ----------

	llight = mem_malloc( sizeof(LIGHT_STRUCT) );

	// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
	// update just the variable that's potentially changed in the UI.
	memcpy( llight, nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 ), sizeof(LIGHT_STRUCT) );
	
	// ----------

	strlcpy( llight->base.description, GuiVar_GroupName, sizeof(llight->base.description) );

	// ----------

	// Save any changes to the current display.
	LIGHTS_copy_guivars_to_daily_schedule( LIGHTS_get_day_index(g_LIGHTS_numeric_date) );

	// ----------

	// Copy the entire 14 day schedule from the global struct (used for screen editing) into the temporary copy. 
	for( day_index = 0; day_index < LIGHTS_SCHEDULE_DAYS_MAX; ++day_index )
	{
		llight->daily_schedule[ day_index ].start_time_1_in_seconds = g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ day_index ].start_time_1_in_seconds;
		llight->daily_schedule[ day_index ].start_time_2_in_seconds = g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ day_index ].start_time_2_in_seconds;
		llight->daily_schedule[ day_index ].stop_time_1_in_seconds  = g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ day_index ].stop_time_1_in_seconds;
		llight->daily_schedule[ day_index ].stop_time_2_in_seconds  = g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY[ day_index ].stop_time_2_in_seconds; 
	}

	// ----------

	nm_LIGHTS_store_changes( llight, g_GROUP_creating_new, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

	// ----------

	mem_free( llight );

	// ----------

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	// Reset the Creating New flag to ensure we don't accidentally create
	// another new group when using the BACK key.
	g_GROUP_creating_new = (false);

}

/* ---------------------------------------------------------- */
/**
 * Copies the group's name and settings into the Group List GuiVars to display
 * on the list of groups.
 * 
 * @mutex This is used as a callback. Get the mutex here.
 *
 * @executed Executed within the context of the display processing task when the
 *           group list is displayed.
 * 
 * @param pindex_0 The 0-based index into the irrigation priorities group list.
 *
 * @author 9/14/2011 Adrianusv
 *
 * @revisions
 *    9/14/2011 Initial release
 */
extern void LIGHTS_load_group_name_into_guivar( const INT_16 pindex_0_i16 )
{
	LIGHT_STRUCT *llight;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	llight = LIGHTS_get_light_at_this_index( (UNS_32)pindex_0_i16 );

	if( nm_OnList(&light_list_hdr, llight) == (true) )
	{
		strlcpy( GuiVar_itmGroupName, nm_GROUP_get_name( llight ), NUMBER_OF_CHARS_IN_A_NAME );
	}
	else
	{
		Alert_group_not_found();
	}

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
extern UNS_32 LIGHTS_get_index_using_ptr_to_light_struct( const LIGHT_STRUCT *const plight )
{
	LIGHT_STRUCT	*llight;

	UNS_32	rv;

	UNS_32	i;

	rv = 0;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	llight = nm_ListGetFirst( &light_list_hdr );

	for( i = 0; i < nm_ListGetCount( &light_list_hdr ); ++i )
	{
		if( llight == plight )
		{
			rv = i;

			break;
		}

		llight = nm_ListGetNext( &light_list_hdr, llight );
	}

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the terminal LIGHT connected to the controller with the specified
 * box_index. If no matching LIGHT is found, a NULL is returned.
 * 
 * @mutex_requirements Calling function must have the list_lights_recursive_MUTEX mutex
 * to ensure the group that's found isn't deleted before it's used.
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the COMM MNGR task when a new
 * hardware structure is received from the TP Micro.
 * 
 * @param pbox_index_0 The 0-based box (controller) index which matches the
 * controller's communication address.
 * 
 * @return void* A pointer (of type LIGHTS_GROUP_STRUCT) to the LIGHT that's found.
 * If no LIGHT is found for the serial number, NULL is returned.
 *
 * @author 8/24/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void *nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index( const UNS_32 pbox_index_0, const UNS_32 poutput_index_0 )
{
	LIGHT_STRUCT *llight;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	llight = nm_ListGetFirst( &light_list_hdr );

	while( llight != NULL )
	{
		if( ( llight->box_index_0 == pbox_index_0 ) && ( llight->output_index_0 == poutput_index_0 ) )
		{
			break;
		}

		llight = nm_ListGetNext( &light_list_hdr, llight );
	}

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	return ( llight );

}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the group with the specified group ID. This is a wrapper
 * for the nm_GROUP_get_ptr_to_group_at_this_location_in_list function so we
 * don't need to expose the list header.
 * 
 * @mutex Calling function must have the list_lights_recursive_MUTEX to ensure the
 *        group that's found isn't deleted before it's used.
 *
 * @executed Executed within the context of the display processing task when the
 *  		 list and group screens are drawn; within the key processing task
 *  		 when the user navigates through the group list screen.
 * 
 * @param pindex_0 The index of the group to retrieve - 0 based.
 * 
 * @return void* A pointer to the group with the specified group ID. Or a NULL if the index
 * is beyond the lists range.
 *
 * @author 9/15/2011 Adrianusv
 *
 * @revisions
 *    9/15/2011 Initial release
 */
extern void *LIGHTS_get_light_at_this_index( const UNS_32 pindex_0 )
{
	LIGHT_STRUCT *llight;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	llight = nm_GROUP_get_ptr_to_group_at_this_location_in_list( &light_list_hdr, pindex_0 );

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	return( llight );
}

/* ---------------------------------------------------------- */
/**
 * Returns the number of items in the list. This is a wrapper for the 
 * nm_ListGetCount macro so the list header doesn't need to be exposed. 
 * 
 * @mutex Calling function must have the list_lights_recursive_MUTEX to ensure a
 *  	  group isn't added or removed between the count being retrieved and
 *  	  being used.
 *
 * @executed Executed within the context of the display processing task when the 
 *  		 list of groups is drawn; within the context of the key processing
 *  		 task when the user moves up and down the list of groups and when
 *  		 the user tries to delete a group.
 * 
 * @return UNS_32 The number of items in the list.
 *
 * @author 9/7/2011 Adrianusv
 *
 * @revisions
 *    9/7/2011   Initial release
 *    2/10/2012	 Renamed to include nm_ prefix
 */
extern UNS_32 LIGHTS_get_list_count( void )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	rv = nm_ListGetCount( &light_list_hdr );

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Calculates the 0 - 47 light index given the box and outputs.
 
    @CALLER MUTEX REQUIREMENTS none
 
    @EXECUTED Various callers, various tasks.
 
    @PARAM pbox_index The 0 - 11 box index on the chain.
 
    @PARAM poutput_index The 0 - 3 output index in the box.
 
    @RETURN A 0 - 47 index. 
	
	@AUTHOR 8/3/2015 mpd
*/
/* ---------------------------------------------------------- */
extern UNS_32 LIGHTS_get_lights_array_index( const UNS_32 pbox_index, const UNS_32 poutput_index )
{
	// Sanity checks. 
	RangeCheck_uint32( &pbox_index, BOX_INDEX_MINIMUM, BOX_INDEX_MAXIMUM, BOX_INDEX_DEFAULT, NULL, "Box Index" );
	RangeCheck_uint32( &poutput_index, LIGHTS_OUTPUT_INDEX_MIN, LIGHTS_OUTPUT_INDEX_MAX, LIGHTS_OUTPUT_INDEX_DEFAULT, NULL, "Output Index" );

	// This index is valid for numerous lights arrays and structures.
	return( ( pbox_index * MAX_LIGHTS_IN_A_BOX ) + poutput_index );  

}

/* ---------------------------------------------------------- */
/**
 * Returns the box index for the light with the specified struct.
 * 
 * @param plights_gid The GID of the light.
 *
 * @return UNS_32 The box index (0 -11).
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern UNS_32 LIGHTS_get_box_index( LIGHT_STRUCT *const plight )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( plight,
													 &plight->box_index_0,
													 BOX_INDEX_MINIMUM,
													 BOX_INDEX_MAXIMUM,
													 FLOWSENSE_get_controller_letter(),
													 &nm_LIGHTS_set_box_index,
													 &plight->changes_to_send_to_master,
													 LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_box_index_bit ] );

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the output index for the light with the specified struct.
 * 
 * @param plights_gid The GID of the light.
 *
 * @return UNS_32 The output index (0 - 3).
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern UNS_32 LIGHTS_get_output_index( LIGHT_STRUCT *const plight )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( plight,
													 &plight->output_index_0,
													 LIGHTS_OUTPUT_INDEX_MIN,
													 LIGHTS_OUTPUT_INDEX_MAX,
													 FLOWSENSE_get_controller_letter(),
													 &nm_LIGHTS_set_box_index,
													 &plight->changes_to_send_to_master,
													 LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_output_index_bit ] );

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns TRUE if the LIGHT card and terminal board are connected. If the LIGHT
 * isn't valid, an alert is generated and FALSE is returned.
 * 
 * @mutex_requirements (none)
 * 
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the COMM_MNGR task when
 * building a controller information token response.
 * 
 * @param plight A pointer to the point of connection.
 * 
 * @return BOOL_32 True if the LIGHT's card and terminal are both connected;
 * otherwise, false. Also returns false is the LIGHT is not valid.
 *
 * @author 9/4/2013 Adrianusv
 *
 * @revisions (none)
 */
extern BOOL_32 LIGHTS_get_physically_available( LIGHT_STRUCT *const plight )
{
	BOOL_32	rv;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_bool_32_bit_change_bits_group( plight,
												   &plight->light_physically_available,
												   LIGHTS_PHYSICALLY_AVAILABLE_DEFAULT,
												   &nm_LIGHTS_set_physically_available,
												   &plight->changes_to_send_to_master,
												   LIGHTS_database_field_names[ LIGHTS_CHANGE_BITFIELD_physically_available_bit ] );

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the first start time for the specified light.
 * 
 * @param plight Pointer to the "LIGHTS_GROUP_STRUCT" of the light.
 *
 * @param pindex The day index used to access the 14 day schedule.
 *
 * @return UNS_32 The start time in minutes (0 - 1440).
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern UNS_32 LIGHTS_get_start_time_1( LIGHT_STRUCT *const plight, const UNS_32 pindex )
{
	//UNS_32 lsize;

	UNS_32	rv;

	//lsize = sizeof( LIGHT_STRUCT ); 		// 324 bytes as of 09/15/2015

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_from_array_32_bit_change_bits_group( plight,
																pindex,
																&plight->daily_schedule[ pindex ].start_time_1_in_seconds,
																LIGHTS_SCHEDULE_DAYS_MAX,
																LIGHTS_START_TIME_MIN,
																LIGHTS_START_TIME_MAX,
																LIGHTS_START_TIME_DEFAULT,
																&nm_LIGHTS_set_start_time_1,
																&plight->changes_to_send_to_master,
																LIGHTS_database_field_names[ ( LIGHTS_CHANGE_BITFIELD_day_00_bit + pindex ) ] );

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the second start time for the specified light.
 * 
 * @param plight Pointer to the "LIGHTS_GROUP_STRUCT" of the light.
 *
 * @param pindex The day index used to access the 14 day schedule.
 *
 * @return UNS_32 The start time in minutes (0 - 1440).
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern UNS_32 LIGHTS_get_start_time_2( LIGHT_STRUCT *const plight, const UNS_32 pindex )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_from_array_32_bit_change_bits_group( plight,
																pindex,
																&plight->daily_schedule[ pindex ].start_time_2_in_seconds,
																LIGHTS_SCHEDULE_DAYS_MAX,
																LIGHTS_START_TIME_MIN,
																LIGHTS_START_TIME_MAX,
																LIGHTS_START_TIME_DEFAULT,
																&nm_LIGHTS_set_start_time_2,
																&plight->changes_to_send_to_master,
																LIGHTS_database_field_names[ ( LIGHTS_CHANGE_BITFIELD_day_00_bit + pindex ) ] );

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the first stop time for the specified light.
 * 
 * @param plight Pointer to the "LIGHTS_GROUP_STRUCT" of the light.
 *
 * @param pindex The day index used to access the 14 day schedule.
 *
 * @return UNS_32 The stop time in minutes (0 - 1440).
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern UNS_32 LIGHTS_get_stop_time_1( LIGHT_STRUCT *const plight, const UNS_32 pindex )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_from_array_32_bit_change_bits_group( plight,
																pindex,
																&plight->daily_schedule[ pindex ].stop_time_1_in_seconds,
																LIGHTS_SCHEDULE_DAYS_MAX,
																LIGHTS_STOP_TIME_MIN,
																LIGHTS_STOP_TIME_MAX,
																LIGHTS_STOP_TIME_DEFAULT,
																&nm_LIGHTS_set_stop_time_1,
																&plight->changes_to_send_to_master,
																LIGHTS_database_field_names[ ( LIGHTS_CHANGE_BITFIELD_day_00_bit + pindex ) ] );

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the second stop time for the specified light.
 * 
 * @param plight Pointer to the "LIGHTS_GROUP_STRUCT" of the light.
 *
 * @param pindex The day index used to access the 14 day schedule.
 *
 * @return UNS_32 The stop time in minutes (0 - 1440).
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern UNS_32 LIGHTS_get_stop_time_2( LIGHT_STRUCT *const plight, const UNS_32 pindex )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_from_array_32_bit_change_bits_group( plight,
																pindex,
																&plight->daily_schedule[ pindex ].stop_time_2_in_seconds,
																LIGHTS_SCHEDULE_DAYS_MAX,
																LIGHTS_STOP_TIME_MIN,
																LIGHTS_STOP_TIME_MAX,
																LIGHTS_STOP_TIME_DEFAULT,
																&nm_LIGHTS_set_stop_time_2,
																&plight->changes_to_send_to_master,
																LIGHTS_database_field_names[ ( LIGHTS_CHANGE_BITFIELD_day_00_bit + pindex ) ] );

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the day index value for the specified numerical date value. The day index is a 0 - 13 range indicating the
 * day within the 14 day light schedule. The schedule starts on the first Sunday which is based on the "first" first
 * Sunday of January 1, 2012.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param numeric_date A numerical date obtained through Epson functions beginning January 1 1900.
 *
 * @return UNS_32 The 0 - 13 day index.
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern UNS_32 LIGHTS_get_day_index( const UNS_32 numeric_date )
{
	// 7/15/2015 mpd : Determine the day index since the beginning of time.
	return( ( numeric_date - LIGHTS_BEGINNING_OF_TIME ) % LIGHTS_SCHEDULE_DAYS_MAX );

}

/* ---------------------------------------------------------- */
extern BOOL_32 LIGHTS_has_a_stop_time( LIGHT_STRUCT *const plight )
{
	BOOL_32	rv;

	UNS_32	lday;

	// ----------

	rv = (false);

	// ----------

	for( lday = 0; lday < LIGHTS_SCHEDULE_DAYS_MAX; ++lday )
	{
		if( (LIGHTS_get_stop_time_1(plight, lday) != SCHEDULE_OFF_TIME) || (LIGHTS_get_stop_time_2(plight, lday) != SCHEDULE_OFF_TIME) )
		{
			rv = (true);
			
			// 9/29/2015 ajv : Since we found a valid stop time in the schedule, break out of the loop.
			break;
		}
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
extern CHANGE_BITS_32_BIT *LIGHTS_get_change_bits_ptr( LIGHT_STRUCT *plight, const UNS_32 pchange_reason )
{
	return( SHARED_get_32_bit_change_bits_ptr( pchange_reason, &plight->changes_to_send_to_master, &plight->changes_to_distribute_to_slaves, &plight->changes_to_upload_to_comm_server ) );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the first available light. If no lights are
 * physically available, a NULL is returned.
 * 
 * @mutex_requirements Calling function must have the
 * list_lights_recursive_MUTEX to ensure the light we're working
 * with isn't removed during this process.
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the of the KEY PROCESSING task
 * when the user presses the NEXT key on the Station Programming screen.
 * 
 * @param pbox_index_ptr A pointer to the serial number of the controller
 * the light is physically connected to.
 * 
 * @param poutput_index_1_ptr A pointer to the 1-based light number.
 * 
 * @return LIGHTS_STRUCT* A pointer to the next available light.
 *
 * @author 8/20/2015 mpd
 */
extern LIGHT_STRUCT *nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars( void )
{
	LIGHT_STRUCT	*llight_ptr;

	llight_ptr = NULL;

	// ----------
	
	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	if( nm_ListGetCount( &light_list_hdr ) > 0 )
	{
		// 9/28/2015 ajv :  Try to get the last lights output we viewed. This may result in NULL if
		// the GuiVars are uninitialized such as if we haven't viewed the Lights screen since
		// booting up.
		llight_ptr = nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index( GuiVar_LightsBoxIndex_0, GuiVar_LightsOutputIndex_0 );

		// 9/28/2015 ajv : If we couldn't find a lights output or it's no longer physically
		// available, loop through all of the outputs until we either hit the end or we find the
		// one we're looking for.
		if( (llight_ptr == NULL) || (!LIGHTS_get_physically_available(llight_ptr)) )
		{
			llight_ptr = nm_ListGetFirst( &light_list_hdr );

			if( (llight_ptr != NULL) && (!LIGHTS_get_physically_available(llight_ptr)) )
			{
				do
				{
					llight_ptr = nm_ListGetNext( &light_list_hdr, llight_ptr );
				
				} while ( (llight_ptr != NULL) && (!LIGHTS_get_physically_available(llight_ptr)) );
			}
		}

		if( llight_ptr != NULL )
		{
			// The LIGHTS_STRUCT box and index values are zero based UNS_32 as are the GuiVars.
			GuiVar_LightsBoxIndex_0 = LIGHTS_get_box_index( llight_ptr );
			GuiVar_LightsOutputIndex_0 = LIGHTS_get_output_index( llight_ptr );
		}
		else
		{
			// 9/28/2015 ajv : This should never happen - it shouldn't be possible to get to this screen
			// if there are no lights physically available
			Alert_Message( "No lights available" );
		}
	}
	
	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	return ( llight_ptr );

}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the next available light that's in use. If no other
 * lights are in use, the pointer to the original light is returned.
 * 
 * @mutex_requirements Calling function must have the
 * list_lights_recursive_MUTEX to ensure the light we're working
 * with isn't removed during this process.
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the of the KEY PROCESSING task
 * when the user presses the NEXT key on the Station Programming screen.
 * 
 * @param pbox_index_ptr A pointer to the serial number of the controller
 * the light is physically connected to.
 * 
 * @param poutput_index_0 A pointer to the 0-based light number.
 * 
 * @return LIGHTS_STRUCT* A pointer to the next available light.
 *
 * @author 9/22/2011 Adrianusv
 *
 * @revisions 7/23/2015 mpd Cloned code from "stations.c" and adapted it to LIGHTS.
 */
extern LIGHT_STRUCT *nm_LIGHTS_get_next_available_light( UNS_32 *pbox_index_ptr, UNS_32 *poutput_index_0 )
{
	LIGHT_STRUCT	*llight_ptr;
	
	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	// 7/23/2015 mpd : Lights are simpler to locate than stations given the fixed 12 x 4 configuration. A simpler search 
	// would work here, but this model is working well for stations. Let's use it for lights too.
	 
	llight_ptr = nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index( *pbox_index_ptr, *poutput_index_0 );

	if( llight_ptr != NULL )
	{
		// 2012.08.12 ajv : Loop through the light list until we either find
		// the next light in use or we land back on the light we started
		// with.
		do
		{
			// We're now pointing to the current record, so traverse to the next available.
			llight_ptr = nm_ListGetNext( llight_ptr->base.list_support.pListHdr, llight_ptr );

			if( llight_ptr == NULL )
			{
				// The current record was the last light in the list, so grab the first light.
				llight_ptr = nm_ListGetFirst( &light_list_hdr );
			}

			if( ( llight_ptr->box_index_0 == *pbox_index_ptr ) && ( llight_ptr->output_index_0 == *poutput_index_0 ) )
			{
				// We've landed on the same light we started with. Therefore,
				// break out of the loop.
				break;
			}

		} while( llight_ptr->light_physically_available == (false) );

		// 7/27/2012 rmd : Assign the box index and light number.
		*pbox_index_ptr = llight_ptr->box_index_0;

		*poutput_index_0 = llight_ptr->output_index_0;
	}
	else
	{
		Alert_group_not_found();
	}
	
	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	return ( llight_ptr );

}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the previous available light that's in use. If no
 * other lights are in use, the pointer to the original light is returned.
 * 
 * @mutex_requirements Calling function must have the
 * list_lights_recursive_MUTEX to ensure the light we're working
 * with isn't removed during this process.
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the of the KEY PROCESSING task
 * when the user presses the PREV key on the Station Programming screen.
 * 
 * @param pbox_index_ptr A pointer to the serial number of the controller
 * the light is physically connected to.
 * 
 * @param poutput_index_0 A pointer to the 0-based light number.
 * 
 * @return LIGHTS_STRUCT* A pointer to the previous available light.
 *
 * @author 9/22/2011 Adrianusv
 *
 * @revisions 7/23/2015 mpd Cloned code from "stations.c" and adapted it to LIGHTS.
 */
extern LIGHT_STRUCT *nm_LIGHTS_get_prev_available_light( UNS_32 *pbox_index_ptr, UNS_32 *poutput_index_0 )
{
	LIGHT_STRUCT	*llight_ptr;
	
	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	// 7/23/2015 mpd : Lights are simpler to locate than stations given the fixed 12 x 4 configuration. A simpler search 
	// would work here, but this model is working well for stations. Let's use it for lights too.

	llight_ptr = nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index( *pbox_index_ptr, *poutput_index_0 );

	if( llight_ptr != NULL )
	{
		// 2012.08.12 ajv : Loop through the light list until we either find
		// the previous light in use or we land back on the light we started
		// with.
		do
		{
			// We're now pointing to the current record, so traverse to the next available.
			llight_ptr = nm_ListGetPrev( llight_ptr->base.list_support.pListHdr, llight_ptr );

			if( llight_ptr == NULL )
			{
				// The current record was the first light in the list, so grab the last light.
				llight_ptr = nm_ListGetLast( &light_list_hdr );
			}

			if( ( llight_ptr->box_index_0 == *pbox_index_ptr ) && ( llight_ptr->output_index_0 == *poutput_index_0 ) )
			{
				// We've landed on the same light we started with. Therefore,
				// break out of the loop.
				break;
			}

		} while( llight_ptr->light_physically_available == (false) );
	}

	// 7/27/2012 rmd : Assign the box index and light number.
	if( llight_ptr != NULL )
	{
		*pbox_index_ptr = llight_ptr->box_index_0;

		*poutput_index_0 = llight_ptr->output_index_0;
	}
	else
	{
		Alert_group_not_found();
	}
	
	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	return ( llight_ptr );

}

/* ---------------------------------------------------------- */
extern void LIGHTS_clean_house_processing( void )
{
	// 4/16/2014 rmd : Executed within the context of the comm_mngr task. ONLY BY SLAVES. THE
	// CHANGES MADE HERE DO NOT BE DISTRIBUTED. THE LIGHTS LIST IS BEING WIPED IN PREPARATION
	// OF BEING ADDED AS A NEW MEMBER TO A CHAIN. Called when the result of a scan indicates
	// this controller is a new slave in the chain. Used when building up chains. Likely ONLY
	// used when assembling a chain for the first time or when increasing the length of a chain
	// to a length it has never 'run' at before. Or if you changed the LETTER of a slave in a
	// chain.
	
	void	*tptr;
	
	// ----------

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );
	
	tptr = nm_ListRemoveHead( &light_list_hdr );

	while( tptr != NULL )
	{
		mem_free( tptr );

		tptr = nm_ListRemoveHead( &light_list_hdr );
	}

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	// ----------

	// 11/18/2014 ajv : No need to create a "dummy" LIGHT here. Simply save the file with the
	// changes and allow LIGHTs to be created using the normal method.

	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE, LIGHTS_FILENAME, LIGHTS_LATEST_FILE_REVISION, &light_list_hdr, sizeof( LIGHT_STRUCT ), list_lights_recursive_MUTEX, FF_LIGHTS );

}

/* ---------------------------------------------------------- */
extern void LIGHTS_set_not_physically_available_based_upon_communication_scan_results( void )
{
	// 4/24/2014 rmd : Executed within the context of the comm_mngr. At the conclusion of the
	// scan by masters (chains and standalones). By standalones so they could clean their list
	// in case they were in a chain previously. The changes made here should be marked for
	// distribution.
	
	// ----------
	
	LIGHT_STRUCT	*llight;
	
	UNS_32	box_index_0;
	UNS_32	output_index_0;

	// ----------

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );
	
	// This function loops through all lights looking for lights in boxes that did not respond during a scan. Those
	// lights are marked unavailable. Since the LIGHT_STRUCT array is based on box and output indexes, it's simpler to 
	// loop through the entire array. This differs from stations which performed a "while" loop on the station list.
	// This function does not make any lights available. 
	for( box_index_0 = 0; box_index_0 < MAX_CHAIN_LENGTH; ++box_index_0 )
	{
		for( output_index_0 = 0; output_index_0 < MAX_LIGHTS_IN_A_BOX; ++output_index_0 )
		{
			// 4/24/2014 rmd : Can only HIDE lights based upon the scan results. Revealing or light
			// creating takes place when a controller receives its whats installed from its OWN tpmicro.
			if( !chain.members[ box_index_0 ].saw_during_the_scan )
			{
				llight = nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index( box_index_0, output_index_0 );

				// 9/10/2015 mpd : We can only hide it if it ever existed. There is nothing to do here if this light was 
				// never saved in the lights file.
				if( llight != NULL )
				{
					nm_LIGHTS_set_physically_available( llight, (false), CHANGE_generate_change_line, CHANGE_REASON_NORMAL_STARTUP, box_index_0, (true), LIGHTS_get_change_bits_ptr( llight, CHANGE_REASON_NORMAL_STARTUP ) );
				}
			}
		}
	}
	// ----------

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
extern void LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token( void )
{
	LIGHT_STRUCT	*llight;

	// ----------

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	llight = nm_ListGetFirst( &light_list_hdr );

	while( llight != NULL )
	{
		// 5/1/2014 rmd : This function ONLY being called at the conclusion of a SCAN by the master
		// of a real multiple controller chain - we want to broadcast to the slaves using tokens
		// about all the hardware we presently know about. In this case the lights.
		SHARED_set_all_32_bit_change_bits( &llight->changes_to_distribute_to_slaves, list_lights_recursive_MUTEX );

		llight = nm_ListGetNext( &light_list_hdr, llight );
	}

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	// ----------

	// 4/29/2014 ajv : Set a flag in the changes structure to notify this
	// controller to examine its lists for changes to distribute to the other
	// controllers in the chain in it's next token resposne. The flag is part of
	// the mutex protected comm_mngr struct so take the MUTEX.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	comm_mngr.changes.distribute_changes_to_slave = (true);

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
extern void LIGHTS_on_all_lights_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear )
{
	LIGHT_STRUCT	*llight;

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	llight = nm_ListGetFirst( &light_list_hdr );

	while( llight != NULL )
	{
		if( pset_or_clear == COMMSERVER_CHANGE_BITS_ALL_CLEARED )
		{
			SHARED_clear_all_32_bit_change_bits( &llight->changes_to_upload_to_comm_server, list_lights_recursive_MUTEX );
		}
		else
		{
			SHARED_set_all_32_bit_change_bits( &llight->changes_to_upload_to_comm_server, list_lights_recursive_MUTEX );
		}

		llight = nm_ListGetNext( &light_list_hdr, llight );
	}

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
/**
 * @mutex_requirements Requires the caller to have the pending_changes_recursive_MUTEX to
 * ensure no other function tries to modify the pending_changes_to_send_to_comm_server flag.
 *
 * @task_execution Executed within the context of the CONTROLLER INITIATED task when it
 * first starts and when the waiting response flags are cleared after a communication.
 */
extern void nm_LIGHTS_update_pending_change_bits( const UNS_32 pcomm_error_occurred )
{
	LIGHT_STRUCT	*llight;

	BOOL_32	lfile_save_necessary;

	lfile_save_necessary = (false);

	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	llight = nm_ListGetFirst( &light_list_hdr );

	while( llight != NULL )
	{
		// 4/30/2015 ajv : No reason to process this if none of the bits are set. This also helps
		// reduce the file save activity.
		if( llight->changes_uploaded_to_comm_server_awaiting_ACK != CHANGE_BITS_32_BIT_ALL_CLEAR )
		{
			// 4/29/2015 ajv : If a communication error occurred while sending Program Data, copy the
			// bits of variables that were sent back into the original bit field to ensure they're sent
			// again during the next attempt. Otherwise, clear the pending bits.
			if( pcomm_error_occurred )
			{
				llight->changes_to_upload_to_comm_server |= llight->changes_uploaded_to_comm_server_awaiting_ACK;

				// 4/30/2015 ajv : Since we've determined there were changes that were not successfully
				// sent, set the flag to trigger another attempt
				weather_preserves.pending_changes_to_send_to_comm_server = (true);

				// 4/30/2015 ajv : Also set the timer to force the data to send. If the timer's already
				// running, this will simply restart it.
				// 
				// 4/30/2015 ajv : BECAUSE of the potential that this function is executed many times (once
				// for each item changed) we could not start our pdata timer with a post to the CI queue.
				// The queue would overflow. So start the timer directly.
				xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGES_NOT_SENT_MS ), portMAX_DELAY );
			}
			else
			{
				SHARED_clear_all_32_bit_change_bits( &llight->changes_uploaded_to_comm_server_awaiting_ACK, list_lights_recursive_MUTEX );
			}

			// 4/30/2015 ajv : At least one system was updated. So make sure we save the file.
			lfile_save_necessary = (true);
		}

		llight = nm_ListGetNext( &light_list_hdr, llight );
	}

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

	if( lfile_save_necessary )
	{
		// 4/29/2015 ajv : Now that the bits are set or cleared accordingly, trigger a file save.
		// If power fails between building the message and receiving the ACK, the original bit field
		// hasn't been saved to flash yet, so it will resend the associated variables during the
		// next attempt.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_LIGHTS, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
	}

}

/* ---------------------------------------------------------- */
BOOL_32 LIGHTS_calculate_chain_sync_crc( UNS_32 const pff_name )
{
	// 8/30/2018 rmd : Executed in the context of the comm_mngr task when token responses are
	// being made. And that's it.

	// ----------

	LIGHT_STRUCT		*llight_ptr;
	
	UNS_8				*checksum_start;

	UNS_32				checksum_length;

	UNS_8				*ucp;
	
	BOOL_32				rv;

	// ----------

	rv = (false);

	// ----------
	
	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	// 8/30/2018 rmd : So the CRC calculation function wants a continuous memory space and a
	// length to calculate the crc off. In this function we will pick out the desired data, and
	// simply pile it up on itself, list item by list item. We don't care how messy the pile is
	// because we never attempt to reference any data once it is added to the pile. The data is
	// only there to include in the checksum.
	//
	// 8/30/2018 rmd : Well we know it can't be bigger than the list item count times the size
	// of each list item, so start with a memory block that big.
	if( mem_obtain_a_block_if_available( (light_list_hdr.count * sizeof(LIGHT_STRUCT)), (void**)&checksum_start ) )
	{
		// 9/5/2018 rmd : Well then the calculation will be successful. Nothing else can prevent
		// that outcome.
		rv = (true);
		
		// ----------
		
		ucp = checksum_start;
		
		checksum_length = 0;
		
		// ----------
		
		// 8/30/2018 rmd : Rattle though the list and push the fields we want to include in the sync
		// test onto the block to be checksummed.
		llight_ptr = nm_ListGetFirst( &light_list_hdr );
		
		while( llight_ptr != NULL )
		{
			// 9/4/2018 rmd : Make sure you include only TRUE USER SETTINGS, no flags, or pointers to
			// memory, etc...
			
			// ----------
			
			// 11/2/2018 rmd : Due to the inconsistent way the group names strings are handled during
			// the sync (we do not regligiously take all 48 characters and always carry them through the
			// sync process, there are places we only copy up until the null), the part of the string
			// following the null char is not guaranteed to be in sync. So only include up until the
			// null in the crc test.
			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &llight_ptr->base.description, strlen(llight_ptr->base.description) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &llight_ptr->box_index_0, sizeof(llight_ptr->box_index_0) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &llight_ptr->output_index_0, sizeof(llight_ptr->output_index_0) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &llight_ptr->light_physically_available, sizeof(llight_ptr->light_physically_available) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &llight_ptr->daily_schedule, sizeof(llight_ptr->daily_schedule) );
			
			// skipping over 4 change bit fields
			
			// END
			
			// ----------
		
			llight_ptr = nm_ListGetNext( &light_list_hdr, llight_ptr );
		}
		
		// ----------
		
		// 8/30/2018 rmd : Now calculate the checksum.
		cscs.the_crc[ pff_name ] = CRC_calculate_32bit_big_endian( checksum_start, checksum_length );
		
		// ----------
		
		// 9/4/2018 rmd : We're done with the block, so of course free it.
		mem_free( checksum_start );
	}
	else
	{
		// 9/5/2018 rmd : So by design it's okay if there is no memory. The checksum won't be set
		// valid, and the next time we try to include it in a token response we'll try to calculate
		// it again. Self protecting and retrying. But let's alert about this so we know it's taking
		// place.
		Alert_Message_va( "SYNC: no mem to calc checksum %s", chain_sync_file_pertinants[ pff_name ].file_name_string );
	}
	
	// ----------
	
	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

