/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_WALK_THRU_H
#define _INC_WALK_THRU_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"cal_list.h"

#include	"cal_td_utils.h"

#include	"pdata_changes.h"

#include	"cal_string.h"


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/7/2018 Ryan : limits and default value for station run time.
#define WALK_THRU_STATION_RUN_TIME_MIN						(1) 		// 6 second min, 10u
#define WALK_THRU_STATION_RUN_TIME_MAX						(150)		// 15 minutes max, 10u
#define WALK_THRU_STATION_RUN_TIME_DEFAULT					(30)		// 3 minutes default, 10u

// 2/7/2018 Ryan : limits and default value for delay before start.
#define WALK_THRU_DELAY_BEFORE_START_MIN					(5) 		// 5 second min
// 4/3/2018 Ryan : If the delay was 15 min or more, there would be interference from the
// screen going into sleep mode.
#define WALK_THRU_DELAY_BEFORE_START_MAX					(5*60)		// 5 minutes max
#define WALK_THRU_DELAY_BEFORE_START_DEFAULT				(5)			// 5 second default

// 2/7/2018 Ryan : limits and default value for box index base 0.
#define WALK_THRU_BOX_INDEX_MIN								(0)			// box 0 min
#define WALK_THRU_BOX_INDEX_MAX								(11)		// box 11 max
#define WALK_THRU_BOX_INDEX_DEFAULT							(0)			// box 0 default

// ----------

// 3/22/2018 rmd : The range of actual station numbers in the sequence. 0 being station #1,
// 48 being station T1, etc up to station T80. The (-1) is a 'magic' value and signifies the
// slot is empty - meaning no station there.
#define WALK_THRU_SEQUENCE_SLOT_no_station_set				(-1)

#define WALK_THRU_SEQUENCE_SLOT_station_number_MIN			(WALK_THRU_SEQUENCE_SLOT_no_station_set)		// station -1 min
#define WALK_THRU_SEQUENCE_SLOT_station_number_MAX			(MAX_STATIONS_PER_CONTROLLER - 1)				// station 175 max, -1 because zero based
#define WALK_THRU_SEQUENCE_SLOT_station_number_DEFAULT		(WALK_THRU_SEQUENCE_SLOT_no_station_set)		// station -1 default

// ----------

// 3/22/2018 Ryan : Limit on the NUMBER of stations in the sequence. How LONG the sequence
// can be.
#define MAX_NUMBER_OF_WALK_THRU_STATIONS					(128)

// 9/22/2015 ajv : All manual program are "in use" until deleted by the user.
#define	WALK_THRU_IN_USE_DEFAULT							(true)

// 2/20/2018 Ryan : True/False for station change alert lines functions
#define WALK_THRU_STATION_ADDED								(true)
#define WALK_THRU_STATION_REMOVED							(false)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct WALK_THRU_STRUCT		WALK_THRU_STRUCT;

// ----------

// 3/22/2018 rmd : Structure used on the FOAL side to manage starting and irrigating a walk
// thru sequence. Definition is here to prevent circular references.
typedef struct
{
	UNS_32	box_index_0;
	
	INT_32	stations[ MAX_NUMBER_OF_WALK_THRU_STATIONS ];
	
	UNS_32	run_time_seconds;
	
	// 10/16/2018 Ryan : group name added to struct so it can be displayed in an alert to the
	// user.
	char	group_name[ NUMBER_OF_CHARS_IN_A_NAME ];
	
} WALK_THRU_KICK_OFF_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern const char WALK_THRU_DEFAULT_NAME[];


extern INT_32	WALK_THRU_station_array_for_gui[ MAX_NUMBER_OF_WALK_THRU_STATIONS ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_file_walk_thru( void );

extern void save_file_walk_thru( void );



extern void *nm_WAlK_THRU_create_new_group( const UNS_32 pchanges_received_from );



extern UNS_32 WALK_THRU_build_data_to_send( UNS_8 **pucp,
												  const UNS_32 pmem_used_so_far,
												  BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
												  const UNS_32 preason_data_is_being_built,
												  const UNS_32 pallocated_memory );

extern UNS_32 nm_WALK_THRU_extract_and_store_changes_from_comm( const unsigned char *pucp,
																	  const UNS_32 preason_for_change,
																	  const BOOL_32 pset_change_bits,
																	  const UNS_32 pchanges_received_from
																	  #ifdef _MSC_VER
																	  , char *pSQL_statements,
																	  char *pSQL_search_condition_str,
																	  char *pSQL_update_str,
																	  char *pSQL_insert_fields_str,
																	  char *pSQL_insert_values_str,
																	  char *pSQL_single_merge_statement_buf,
																	  const UNS_32 pnetwork_ID
																	  #endif
																	);

// ----------

#ifdef _MSC_VER

extern INT_32 WALK_THRU_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order );

#endif

// ----------

extern void WALK_THRU_copy_group_into_guivars( const UNS_32 pgroup_index_0 );

extern void WALK_THRU_extract_and_store_group_name_from_GuiVars( void );

extern void WALK_THRU_extract_and_store_changes_from_GuiVars( void );

			
extern BOOL_32 WALK_THRU_check_for_station_in_walk_thru( WALK_THRU_STRUCT *pwalkthru, const INT_32 pstation_number );
			
extern void WALK_THRU_load_group_name_into_guivar( const INT_16 pindex_0_i16 );

extern void WALK_THRU_load_controller_name_into_scroll_box_guivar( const INT_16 pindex_0_i16 );

extern void *WALK_THRU_get_group_with_this_GID( const UNS_32 pgroup_ID );

extern void *WALK_THRU_get_group_at_this_index( const UNS_32 pindex_0 );

extern UNS_32 WALK_THRU_get_index_for_group_with_this_GID( const UNS_32 pgroup_ID );

extern UNS_32 WALK_THRU_get_num_groups_in_use( void );

extern UNS_32 WALK_THRU_get_station_run_time( WALK_THRU_STRUCT *const lgroup );

extern UNS_32 WALK_THRU_get_delay_before_start( WALK_THRU_STRUCT *const lgroup );

extern UNS_32 WALK_THRU_get_box_index( WALK_THRU_STRUCT *const lgroup );

extern INT_32 WALK_THRU_get_stations( WALK_THRU_STRUCT *const lgroup, const UNS_32 pslot_index_0 );

extern CHANGE_BITS_32_BIT *WALK_THRU_get_change_bits_ptr( WALK_THRU_STRUCT *pgroup, const UNS_32 pchange_reason );

extern void WALK_THRU_clean_house_processing( void );

extern void WALK_THRU_set_bits_on_all_groups_to_cause_distribution_in_the_next_token( void );

extern void WALK_THRU_on_all_groups_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear );

extern void nm_WALK_THRU_update_pending_change_bits( const UNS_32 pcomm_error_occurred );

extern void nm_WALK_THRU_set_stations( void *const pgroup,
												   const UNS_32 pslot_index_0,
												   INT_32	pstation_number,
												   const BOOL_32 pgenerate_change_line_bool,
												   const UNS_32 preason_for_change,
												   const UNS_32 pbox_index_0,
												   const BOOL_32 pset_change_bits,
												   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												   #ifdef _MSC_VER
												   , char *pmerge_update_str,
												   char *pmerge_insert_fields_str,
												   char *pmerge_insert_values_str
												   #endif
												 );
								 
// ----------

extern UNS_32 WALK_THRU_load_walk_thru_gid_for_token_response( UNS_8 **pwalk_thru_ptr );

extern BOOL_32 WALK_THRU_load_kick_off_struct( UNS_32 pwalk_thru_gid, WALK_THRU_KICK_OFF_STRUCT *pwalk_thru_ptr );

// ----------

extern BOOL_32 WALK_THRU_calculate_chain_sync_crc( UNS_32 const pff_name );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

