/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_MANUAL_PROGRAMS_H
#define _INC_MANUAL_PROGRAMS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"cal_list.h"

#include	"cal_td_utils.h"

#include	"pdata_changes.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	MANUAL_PROGRAMS_NUMBER_OF_PROGRAMS_A_STATION_CAN_BE_ASSIGNED_TO		(2)

#define	MANUAL_PROGRAMS_NUMBER_OF_START_TIMES			(6)

#define	MANUAL_PROGRAMS_START_TIME_MIN					(0)
#define	MANUAL_PROGRAMS_START_TIME_MAX					(SCHEDULE_OFF_TIME)
#define	MANUAL_PROGRAMS_START_TIME_DEFAULT				(SCHEDULE_OFF_TIME)

// Default the first start time to 7:00 AM at Richard W.'s request.
#define	MANUAL_PROGRAMS_START_TIME_1_DEFAULT			(7 * 60 * 60)

// Default the first start time to 11:00 AM at Richard W.'s request.
#define	MANUAL_PROGRAMS_START_TIME_2_DEFAULT			(11 * 60 * 60)

// Default the first start time to 3:00 PM at Richard W.'s request.
#define	MANUAL_PROGRAMS_START_TIME_3_DEFAULT			(15 * 60 * 60)

#define MANUAL_PROGRAMS_WATERDAY_DEFAULT				(false)

// 9/22/2015 ajv : All manual program are "in use" until deleted by the user.
#define	MANUAL_PROGRAMS_IN_USE_DEFAULT					(true)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct MANUAL_PROGRAMS_GROUP_STRUCT		MANUAL_PROGRAMS_GROUP_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern const char MANUAL_PROGRAMS_DEFAULT_NAME[];

extern UNS_32 g_MANUAL_PROGRAMS_start_date;

extern UNS_32 g_MANUAL_PROGRAMS_stop_date;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_file_manual_programs( void );

extern void save_file_manual_programs( void );



extern void *nm_MANUAL_PROGRAMS_create_new_group( const UNS_32 pchanges_received_from );



extern UNS_32 MANUAL_PROGRAMS_build_data_to_send( UNS_8 **pucp,
												  const UNS_32 pmem_used_so_far,
												  BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
												  const UNS_32 preason_data_is_being_built,
												  const UNS_32 pallocated_memory );

extern UNS_32 nm_MANUAL_PROGRAMS_extract_and_store_changes_from_comm( const unsigned char *pucp,
																	  const UNS_32 preason_for_change,
																	  const BOOL_32 pset_change_bits,
																	  const UNS_32 pchanges_received_from
																	  #ifdef _MSC_VER
																	  , char *pSQL_statements,
																	  char *pSQL_search_condition_str,
																	  char *pSQL_update_str,
																	  char *pSQL_insert_fields_str,
																	  char *pSQL_insert_values_str,
																	  char *pSQL_single_merge_statement_buf,
																	  const UNS_32 pnetwork_ID
																	  #endif
																	);

// ----------

#ifdef _MSC_VER

extern INT_32 MANUAL_PROGRAMS_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order );

#endif

// ----------

extern void MANUAL_PROGRAMS_copy_group_into_guivars( const UNS_32 pgroup_index_0 );

extern void MANUAL_PROGRAMS_extract_and_store_group_name_from_GuiVars( void );

extern void MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars( void );



extern void MANUAL_PROGRAMS_load_group_name_into_guivar( const INT_16 pindex_0_i16 );



extern void *MANUAL_PROGRAMS_get_group_with_this_GID( const UNS_32 pgroup_ID );

extern void *MANUAL_PROGRAMS_get_group_at_this_index( const UNS_32 pindex_0 );

extern UNS_32 MANUAL_PROGRAMS_get_index_for_group_with_this_GID( const UNS_32 pgroup_ID );

extern UNS_32 MANUAL_PROGRAMS_get_last_group_ID( void );

extern UNS_32 MANUAL_PROGRAMS_get_num_groups_in_use( void );


extern UNS_32 MANUAL_PROGRAMS_get_start_date( MANUAL_PROGRAMS_GROUP_STRUCT *const lgroup );

extern UNS_32 MANUAL_PROGRAMS_get_end_date( MANUAL_PROGRAMS_GROUP_STRUCT *const lgroup );

extern BOOL_32 MANUAL_PROGRAMS_today_is_a_water_day( MANUAL_PROGRAMS_GROUP_STRUCT *const lgroup, const UNS_32 pdow );

extern UNS_32 MANUAL_PROGRAMS_get_start_time( MANUAL_PROGRAMS_GROUP_STRUCT *const lgroup, const UNS_32 pstart_index );

extern UNS_32 MANUAL_PROGRAMS_get_run_time( MANUAL_PROGRAMS_GROUP_STRUCT *const lgroup );

extern UNS_32 MANUAL_PROGRAMS_return_the_number_of_starts_for_this_GID( const UNS_32 pgid, const DATE_TIME_COMPLETE_STRUCT *const pdtcs_ptr );


extern CHANGE_BITS_32_BIT *MANUAL_PROGRAMS_get_change_bits_ptr( MANUAL_PROGRAMS_GROUP_STRUCT *pgroup, const UNS_32 pchange_reason );

extern void MANUAL_PROGRAMS_clean_house_processing( void );

extern void MANUAL_PROGRAMS_set_bits_on_all_groups_to_cause_distribution_in_the_next_token( void );

extern void MANUAL_PROGRAMS_on_all_groups_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear );

extern void nm_MANUAL_PROGRAMS_update_pending_change_bits( const UNS_32 pcomm_error_occurred );


extern void MANUAL_PROGRAMS_load_ftimes_list( void );


extern BOOL_32 MANUAL_PROGRAMS_calculate_chain_sync_crc( UNS_32 const pff_name );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

