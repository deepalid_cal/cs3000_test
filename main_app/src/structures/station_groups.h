/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_STATION_GROUP_H
#define _INC_STATION_GROUP_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	"cal_list.h"

#include	"lpc_types.h"

#include	"cal_td_utils.h"

#include	"stations.h"

#ifndef _MSC_VER

	#include	"ftimes_vars.h"

#endif




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	STATION_GROUP_PLANT_TYPE_COOL_SEASON_TURF	(0)	// Fescue
#define	STATION_GROUP_PLANT_TYPE_WARM_SEASON_TURF	(1)	// Bermuda
#define	STATION_GROUP_PLANT_TYPE_COMBINED_TURF		(2)
#define	STATION_GROUP_PLANT_TYPE_ANNUALS			(3)
#define	STATION_GROUP_PLANT_TYPE_TREES				(4)
#define	STATION_GROUP_PLANT_TYPE_GROUND_COVER		(5)
#define	STATION_GROUP_PLANT_TYPE_SHRUBS_HIGH		(6)
#define	STATION_GROUP_PLANT_TYPE_SHRUBS_MEDIUM		(7)
#define	STATION_GROUP_PLANT_TYPE_SHRUBS_LOW			(8)
#define	STATION_GROUP_PLANT_TYPE_MIXED_HIGH			(9)
#define	STATION_GROUP_PLANT_TYPE_MIXED_MEDIUM		(10)
#define	STATION_GROUP_PLANT_TYPE_MIXED_LOW			(11)
#define	STATION_GROUP_PLANT_TYPE_NATIVE				(12)
#define	STATION_GROUP_PLANT_TYPE_NATIVE_GRASSES		(13)

#define	STATION_GROUP_PLANT_TYPE_MIN				(STATION_GROUP_PLANT_TYPE_COOL_SEASON_TURF)
#define	STATION_GROUP_PLANT_TYPE_MAX				(STATION_GROUP_PLANT_TYPE_NATIVE_GRASSES)
#define STATION_GROUP_PLANT_TYPE_DEFAULT			(STATION_GROUP_PLANT_TYPE_COOL_SEASON_TURF)

// ----------

#define	STATION_GROUP_DENSITY_FACTOR_LOW			(0)
#define	STATION_GROUP_DENSITY_FACTOR_AVERAGE		(1)
#define	STATION_GROUP_DENSITY_FACTOR_HIGH			(2)

// ----------

#define STATION_GROUP_MICROCLIMATE_FACTOR_LOW		(0)
#define STATION_GROUP_MICROCLIMATE_FACTOR_AVERAGE	(1)
#define STATION_GROUP_MICROCLIMATE_FACTOR_HIGH		(2)

// ----------

#define	STATION_GROUP_HEAD_TYPE_SPRAY				(0)
#define	STATION_GROUP_HEAD_TYPE_SPRAY_STREAM		(1)
#define	STATION_GROUP_HEAD_TYPE_SPRAY_HIGH_EFF		(2)
#define	STATION_GROUP_HEAD_TYPE_ROTOR_FULL			(3)
#define	STATION_GROUP_HEAD_TYPE_ROTOR_PART			(4)
#define	STATION_GROUP_HEAD_TYPE_ROTOR_MIXED			(5)
#define	STATION_GROUP_HEAD_TYPE_ROTOR_STREAM		(6)
#define	STATION_GROUP_HEAD_TYPE_IMPACT_FULL			(7)
#define	STATION_GROUP_HEAD_TYPE_IMPACT_PART			(8)
#define	STATION_GROUP_HEAD_TYPE_IMPACT_MIXED		(9)
#define	STATION_GROUP_HEAD_TYPE_BUBBLER				(10)
#define	STATION_GROUP_HEAD_TYPE_DRIP				(11)
#define	STATION_GROUP_HEAD_TYPE_DRIP_SUBSURFACE		(12)

#define	STATION_GROUP_HEAD_TYPE_MIN				(STATION_GROUP_HEAD_TYPE_SPRAY)
#define	STATION_GROUP_HEAD_TYPE_MAX				(STATION_GROUP_HEAD_TYPE_DRIP_SUBSURFACE)
#define	STATION_GROUP_HEAD_TYPE_DEFAULT			(STATION_GROUP_HEAD_TYPE_SPRAY)

// ----------

#define	STATION_GROUP_PRECIP_RATE_MIN_100000u		(1000)
#define	STATION_GROUP_PRECIP_RATE_MAX_100000u		(1000000-1)
#define	STATION_GROUP_PRECIP_RATE_DEFAULT_100000u	(100000)

// ----------

#define	STATION_GROUP_SOIL_TYPE_CLAY				(0)
#define STATION_GROUP_SOIL_TYPE_SILTY_CLAY			(1)
#define STATION_GROUP_SOIL_TYPE_CLAY_LOAM			(2)
#define STATION_GROUP_SOIL_TYPE_LOAM				(3)
#define STATION_GROUP_SOIL_TYPE_SANDY_LOAM			(4)
#define STATION_GROUP_SOIL_TYPE_LOAMY_SAND			(5)
#define STATION_GROUP_SOIL_TYPE_SAND				(6)

#define	STATION_GROUP_SOIL_TYPE_MIN					(STATION_GROUP_SOIL_TYPE_CLAY)
#define	STATION_GROUP_SOIL_TYPE_MAX					(STATION_GROUP_SOIL_TYPE_SAND)
#define	STATION_GROUP_SOIL_TYPE_DEFAULT				(STATION_GROUP_SOIL_TYPE_LOAM)

// ----------

#define	STATION_GROUP_SLOPE_0_3_PERCENT				(0)		// None/Slight Grade
#define	STATION_GROUP_SLOPE_4_6_PERCENT				(1)		// Gentle Grade
#define	STATION_GROUP_SLOPE_7_12_PERCENT			(2)		// Mild Grade
#define	STATION_GROUP_SLOPE_13_PLUS_PERCENT			(3)		// Moderate to Steep Grade

#define	STATION_GROUP_SLOPE_PERCENTAGE_MIN			(STATION_GROUP_SLOPE_0_3_PERCENT)
#define	STATION_GROUP_SLOPE_PERCENTAGE_MAX			(STATION_GROUP_SLOPE_13_PLUS_PERCENT)
#define	STATION_GROUP_SLOPE_PERCENTAGE_DEFAULT		(STATION_GROUP_SLOPE_0_3_PERCENT)

// ----------

#define STATION_GROUP_SLOPE_LOCATION_ALL			(0)
#define STATION_GROUP_SLOPE_LOCATION_TOP			(1)
#define STATION_GROUP_SLOPE_LOCATION_MIDDLE			(2)
#define STATION_GROUP_SLOPE_LOCATION_BOTTOM			(3)

#define STATION_GROUP_SLOPE_LOCATION_MIN			(STATION_GROUP_SLOPE_LOCATION_ALL)
#define STATION_GROUP_SLOPE_LOCATION_MAX			(STATION_GROUP_SLOPE_LOCATION_BOTTOM)
#define STATION_GROUP_SLOPE_LOCATION_DEFAULT		(STATION_GROUP_SLOPE_LOCATION_ALL)

// ----------

#define	STATION_GROUP_EXPOSURE_FULL_SUN				(0)
#define	STATION_GROUP_EXPOSURE_25_PCT_SHADE			(1)
#define	STATION_GROUP_EXPOSURE_50_PCT_SHADE			(2)
#define	STATION_GROUP_EXPOSURE_75_PCT_SHADE			(3)
#define	STATION_GROUP_EXPOSURE_FULL_SHADE			(4)

#define	STATION_GROUP_EXPOSURE_MIN					(STATION_GROUP_EXPOSURE_FULL_SUN)
#define	STATION_GROUP_EXPOSURE_MAX					(STATION_GROUP_EXPOSURE_FULL_SHADE)
#define	STATION_GROUP_EXPOSURE_DEFAULT				(STATION_GROUP_EXPOSURE_FULL_SUN)

// ----------

#define STATION_GROUP_SOIL_STORAGE_CAPACITY_MIN_100u		(1)
#define	STATION_GROUP_SOIL_STORAGE_CAPACITY_MAX_100u		(600)
#define	STATION_GROUP_SOIL_STORAGE_CAPACITY_DEFAULT_100u	(150)

// ----------

#define	STATION_GROUP_USABLE_RAIN_PERCENTAGE_MIN		(50)
#define	STATION_GROUP_USABLE_RAIN_PERCENTAGE_MAX		(100)
#define	STATION_GROUP_USABLE_RAIN_PERCENTAGE_DEFAULT	(80)

// ----------

#define	STATION_GROUP_CROP_COEFFICIENT_MIN_100u			(1)
#define	STATION_GROUP_CROP_COEFFICIENT_MAX_100u			(300)
#define	STATION_GROUP_CROP_COEFFICIENT_DEFAULT_100u		(100)

// ----------

#define	STATION_GROUP_ALLOWABLE_DEPLETION_MIN_100u		(10)
#define	STATION_GROUP_ALLOWABLE_DEPLETION_MAX_100u		(100)
#define	STATION_GROUP_ALLOWABLE_DEPLETION_DEFAULT_100u	(50)

// ----------

#define	STATION_GROUP_AVAILABLE_WATER_MIN_100u			(1)
#define	STATION_GROUP_AVAILABLE_WATER_MAX_100u			(100)
#define	STATION_GROUP_AVAILABLE_WATER_DEFAULT_100u		(15)

// ----------

#define	STATION_GROUP_ROOT_ZONE_DEPTH_MIN_10u			(10)
#define	STATION_GROUP_ROOT_ZONE_DEPTH_MAX_10u			(480)
#define	STATION_GROUP_ROOT_ZONE_DEPTH_DEFAULT_10u		(100)

// ----------

#define	STATION_GROUP_SPECIES_FACTOR_MIN_100u			(10)
#define	STATION_GROUP_SPECIES_FACTOR_MAX_100u			(100)
#define	STATION_GROUP_SPECIES_FACTOR_DEFAULT_100u		(50)

// ----------

#define	STATION_GROUP_DENSITY_FACTOR_MIN_100u			(10)
#define	STATION_GROUP_DENSITY_FACTOR_MAX_100u			(200)
#define	STATION_GROUP_DENSITY_FACTOR_DEFAULT_100u		(100)

// ----------

#define	STATION_GROUP_MICROCLIMATE_FACTOR_MIN_100u		(10)
#define	STATION_GROUP_MICROCLIMATE_FACTOR_MAX_100u		(200)
#define	STATION_GROUP_MICROCLIMATE_FACTOR_DEFAULT_100u	(100)

// ----------

#define	STATION_GROUP_SOIL_INTAKE_RATE_MIN_100u			(1)
#define	STATION_GROUP_SOIL_INTAKE_RATE_MAX_100u			(100)
#define	STATION_GROUP_SOIL_INTAKE_RATE_DEFAULT_100u		(35)

// ----------

#define	STATION_GROUP_ALLOWABLE_SURFACE_ACCUM_MIN_100u		(1)
#define	STATION_GROUP_ALLOWABLE_SURFACE_ACCUM_MAX_100u		(100)
#define	STATION_GROUP_ALLOWABLE_SURFACE_ACCUM_DEFAULT_100u	(25)

/* ---------------------------------------------------------- */

#define PRIORITY_HIGH						(3)
#define PRIORITY_MEDIUM						(2)
#define	PRIORITY_LOW						(1)
#define PRIORITY_LEVEL_DEFAULT				(PRIORITY_LOW)

/* ---------------------------------------------------------- */

#define PERCENT_ADJUST_PERCENT_MIN_100u			(20)
#define PERCENT_ADJUST_PERCENT_MAX_100u			(200)
#define PERCENT_ADJUST_PERCENT_DEFAULT_100u		(100)

#define PERCENT_ADJUST_DAYS_MIN					(0)
#define PERCENT_ADJUST_DAYS_MAX					(366)
#define PERCENT_ADJUST_DAYS_DEFAULT				(0)

/* ---------------------------------------------------------- */

#define	SCHEDULE_ENABLED_DEFAULT				(false)

// 7/2/2018 rmd : DO NOT CHANGE THE VALUE OR ORDER OF THESE DEFINES. THERE IS WATER DAY MATH
// DEPENDANT UPON THE VALUE OF THESE DEFINES.
#define	SCHEDULE_TYPE_EVERY_DAY					(0)
#define	SCHEDULE_TYPE_EVEN_DAYS					(1)
#define	SCHEDULE_TYPE_ODD_DAYS					(2)
#define	SCHEDULE_TYPE_WEEKLY_SCHEDULE			(3)
#define	SCHEDULE_TYPE_EVERY_OTHER_DAY			(4)
#define SCHEDULE_TYPE_EVERY_THREE_DAYS			(5)
#define SCHEDULE_TYPE_EVERY_FOUR_DAYS			(6)
#define SCHEDULE_TYPE_EVERY_FIVE_DAYS			(7)
#define SCHEDULE_TYPE_EVERY_SIXTH_DAYS			(8)
#define SCHEDULE_TYPE_EVERY_SEVEN_DAYS			(9)
#define SCHEDULE_TYPE_EVERY_EIGHT_DAYS			(10)
#define SCHEDULE_TYPE_EVERY_NINE_DAYS			(11)
#define SCHEDULE_TYPE_EVERY_TEN_DAYS			(12)
#define SCHEDULE_TYPE_EVERY_ELEVEN_DAYS			(13)
#define SCHEDULE_TYPE_EVERY_TWELVE_DAYS			(14)
#define SCHEDULE_TYPE_EVERY_THIRTEEN_DAYS		(15)
#define SCHEDULE_TYPE_EVERY_FOURTEEN_DAYS		(16)
#define SCHEDULE_TYPE_ONCE_EVERY_THREE_WEEKS	(17)
#define SCHEDULE_TYPE_ONCE_EVERY_FOUR_WEEKS		(18)

#define	SCHEDULE_TYPE_DEFAULT					(SCHEDULE_TYPE_EVERY_DAY)
#define	SCHEDULE_TYPE_MIN						(SCHEDULE_TYPE_EVERY_DAY)
#define	SCHEDULE_TYPE_MAX						(SCHEDULE_TYPE_ONCE_EVERY_FOUR_WEEKS)


// 7/10/2015 ajv : When running an interval schedule, such as every three days, we want to
// be able to use to schedule type index (e.g., SCHEDULE_TYPE_EVERY_THREE_DAYS) to determine
// the actual number of days. Therefore, since SCHEDULE_TYPE_EVERY_OTHER_DAY's index is 4,
// and it's an interval of every 2 days, the offset is 2. This works for all cases between
// EVERY_OTHER_DAY (4-2=2) up to EVERY_FOURTEEN_DAYS (16-2=14)
#define	SCHEDULE_TYPE_NUMBER_OF_DAYS_OFFSET		(2)

// ----------

#define SCHEDULE_MOW_DAY_NOT_SET				(DAYS_IN_A_WEEK)

// ----------

#define SCHEDULE_OFF_TIME						(86400)

// ----------

#define	SCHEDULE_START_TIME_MIN					(0)
#define	SCHEDULE_START_TIME_MAX					(86400-600)	// The latest start time is 11:50 PM.
#define	SCHEDULE_START_TIME_DEFAULT				(0)

// ----------

#define	SCHEDULE_STOP_TIME_MIN					(0)
#define	SCHEDULE_STOP_TIME_MAX					(SCHEDULE_OFF_TIME)
#define	SCHEDULE_STOP_TIME_DEFAULT				(SCHEDULE_OFF_TIME)

// ----------

#define SCHEDULE_WATERDAY_DEFAULT				(false)

// ----------

#define SCHEDULE_IRRIGATEON29THOR31ST_DEFAULT	(false)

// ----------

#define	SCHEDULE_MOWDAY_MIN						(0)
#define	SCHEDULE_MOWDAY_MAX						(SCHEDULE_MOW_DAY_NOT_SET)
#define SCHEDULE_MOWDAY_DEFAULT					(SCHEDULE_MOW_DAY_NOT_SET)

// ----------
// 06/28/2018 dmd: The new irrigation dates are calculated and saved for future use. The
// following defines are for actions to indicate if the new calculated irrigation date needs
// to be updated and saved to a file depending on the schedule type.
#define SCHEDULE_BEGIN_DATE_ACTION_do_nothing								(0)
#define	SCHEDULE_BEGIN_DATE_ACTION_update_it_but_dont_save_it_to_the_file	(1)
#define SCHEDULE_BEGIN_DATE_ACTION_advance_it_and_save_it_to_the_file		(2)

// ----------

#define SCHEDULE_LASTRAN_DEFAULT			(0)

/* ---------------------------------------------------------- */

#define	WEATHER_ET_MODE_MINUTES				(0)
#define WEATHER_ET_MODE_DAILY_ET			(1)
#define	WEATHER_ET_MODE_MAX					(WEATHER_ET_MODE_DAILY_ET)

// 3/18/2015 ajv : WaterSense requires that users be directed to be WaterSense compliant out
// of the box. Therefore, we'll default the controller to using ET but allow them to change
// to minutes if they want afterwards. Note that we DID NOT automatically force existing
// users to using ET from REV 1 to REV 2. This will allow the handful of customers already
// using the controller to migrate to WaterSense as time goes on.
#define WEATHER_ET_MODE_DEFAULT				(WEATHER_ET_MODE_DAILY_ET)

// ----------

// 3/13/2015 ajv : Since WaterSense does not use ET Averaging, we've changed the default
// value from TRUE to FALSE and updated all of the existing groups between REV 1 and REV 2
// of the Station Group file.
//
// 4/7/2016 rmd : After further discussion with AJ I explained that 1) ET averaging only
// affects the weekly scheduling option not the fixed period schedules and 2) over the
// course of a one week period the amount of irrigation applied is the same ET Averaging or
// not. With those two facts the net result to the the Moisture Balance over the course of a
// week is nada. ADDITIONALLY in the 2000e we had the default set to (true) enabled and we
// learned that it was almost always left that way. So in view of all that I've changed the
// default to (true).
#define	WEATHER_ET_AVERAGING_IN_USE_DEFAULT	(true)

// ----------

#define WEATHER_RAIN_IN_USE_DEFAULT			(true)

// ----------

#define WEATHER_WIND_IN_USE_DEFAULT			(false)

/* ---------------------------------------------------------- */

#define ALERT_ACTIONS_NONE					(0)
#define ALERT_ACTIONS_ALERT_NO_ACTION		(1)
#define	ALERT_ACTIONS_ALERT_SHUTOFF			(2)

#define ALERT_ACTIONS_HIGH_DEFAULT			(ALERT_ACTIONS_ALERT_NO_ACTION)
#define ALERT_ACTIONS_LOW_DEFAULT			(ALERT_ACTIONS_ALERT_NO_ACTION)

/* ---------------------------------------------------------- */

// 5/2/2016 ajv : Updated from UNS_32_MAX to INT_32_MAX because it caused storage issues in
// the Command Center Online database.
#define	ON_AT_A_TIME_X						(INT_32_MAX)
#define ON_AT_A_TIME_MIN					(1)
#define ON_AT_A_TIME_DEFAULT				(ON_AT_A_TIME_MIN)

// 4/4/2014 rmd : Where this value comes from I don't know. Have a feeling I just picked it.
// Regardless it sure seems like enough. 30 on at a time!
#define	ON_AT_A_TIME_ABSOLUTE_MAXIMUM		(30)

/* ---------------------------------------------------------- */

#define PUMP_INUSE_DEFAULT						(true)

/* ---------------------------------------------------------- */

#define LINE_FILL_TIME_MIN				(15)

// 8/28/2014 ajv : Not sure why this was only set to 180. The ET2000e
// actually has a whopping 1800 seconds (30 MINUTES!) Seems awfully excessive
// but Bob explained that there's actually a historical reason behind it, so
// we'll preserve that here in the CS3000.
#define LINE_FILL_TIME_MAX				(1800)

#define LINE_FILL_TIME_DEFAULT			(120)

// ----------

// 9/12/2014 rmd : ZERO is the default. David Byma and I reasoned that the user should need
// no more than 120 seconds to get his slow closing valve to close. Remember this is the
// time that the next valve turn ON is delayed. And you'll see that the MAX is carried
// through to a test made when building the message for the TPMICRO. Its a sanity check
// against seeing the MV open and no valve ON. We now have to TOLERATE this condition during
// the slow closing valve delay. So the longer this delay the less strict we become when
// testing for this system abnormality. That is the MV is open for what appears to be no
// good reason.
#define DELAY_BETWEEN_VALVES_MIN			(0)

// 9/12/2014 rmd : READ COMMENT ABOVE BEFORE CHANGING.
#define DELAY_BETWEEN_VALVES_MAX			(120)

#define DELAY_BETWEEN_VALVES_DEFAULT		(0)

/* ---------------------------------------------------------- */

#define	ACQUIRE_EXPECTEDS_DEFAULT			(false)

/* ---------------------------------------------------------- */

// 9/9/2015 rmd : The scheduled irrigaiton maximum reduction allowed by the budgets algorithm.
// A value of 0 means no reduction at all. A value of 100 means that the reduction scheme to
// stay within budget could end of not providing any water at all. Budget percentage adjust
// cap default is 25% as suggested by David Byma. Seems a reasonable place to start.
#define	STATION_GROUP_BUDGET_PERCENTAGE_CAP_MIN		(0)
#define	STATION_GROUP_BUDGET_PERCENTAGE_CAP_MAX		(100)
#define	STATION_GROUP_BUDGET_PERCENTAGE_CAP_DEFAULT	(25)

/* ---------------------------------------------------------- */

// 9/22/2015 ajv : All station groups are "in use" until deleted by the user.
#define	STATION_GROUP_IN_USE_DEFAULT				(true)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct STATION_GROUP_STRUCT STATION_GROUP_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


extern const char STATION_GROUP_DEFAULT_NAME[];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/7/2016 rmd : A structure used to obtain several items at once from a station group to
// avoid repeated formal GET functions.
typedef struct
{
	BOOL_32		schedule_enabled;

	UNS_32		schedule_type;
	
	BOOL_32		wday[ DAYS_IN_A_WEEK ];
	
	BOOL_32		irrigates_after_29th_or_31st;

	BOOL_32		uses_et_averaging;

	UNS_32		sx;

} RUN_TIME_CALC_SUPPORT_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_file_station_group( void );

extern void save_file_station_group( void );



extern void *nm_STATION_GROUP_create_new_group( const UNS_32 pchanges_received_from );

extern void STATION_GROUP_copy_group( const STATION_GROUP_STRUCT *pgroup_to_copy_from, STATION_GROUP_STRUCT *pgroup_to_copy_to );




extern UNS_32 STATION_GROUP_build_data_to_send( UNS_8 **pucp,
												const UNS_32 pmem_used_so_far,
												BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
												const UNS_32 preason_data_is_being_built,
												const UNS_32 pallocated_memory );

extern UNS_32 nm_STATION_GROUP_extract_and_store_changes_from_comm( const unsigned char *pucp,
																	const UNS_32 preason_for_change,
																	const BOOL_32 pset_change_bits,
																	const UNS_32 pchanges_received_from
																	#ifdef _MSC_VER
																	, char *pSQL_statements,
																	char *pSQL_search_condition_str,
																	char *pSQL_update_str,
																	char *pSQL_insert_fields_str,
																	char *pSQL_insert_values_str,
																	char *pSQL_single_merge_statement_buf,
																	const UNS_32 pnetwork_ID
																	#endif
																  );

// ----------

#ifdef _MSC_VER

extern INT_32 STATION_GROUP_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order );

#endif

// ----------

extern void nm_SCHEDULE_set_last_ran( void *const pgroup,
									  const DATE_TIME plast_ran_dt,
									  const BOOL_32 pgenerate_change_line_bool,
									  const UNS_32 preason_for_change,
									  const UNS_32 pbox_index_0,
									  const BOOL_32 pset_change_bits,
									  CHANGE_BITS_64_BIT *pchange_bitfield_to_set
									  #ifdef _MSC_VER
									  , char *pmerge_update_str,
									  char *pmerge_insert_fields_str,
									  char *pmerge_insert_values_str
									  #endif
									);

extern void nm_SCHEDULE_set_begin_date( void *const pgroup,
										UNS_32 pbegin_date,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const BOOL_32 pset_change_bits,
										CHANGE_BITS_64_BIT *pchange_bitfield_to_set
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
									  );

extern void nm_ACQUIRE_EXPECTEDS_set_acquire_expected( void *const pgroup,
													   BOOL_32 pacquire_expecteds,
													   const BOOL_32 pgenerate_change_line_bool,
													   const UNS_32 preason_for_change,
													   const UNS_32 pbox_index_0,
													   const BOOL_32 pset_change_bits,
													   CHANGE_BITS_64_BIT *pchange_bitfield_to_set
													   #ifdef _MSC_VER
													   , char *pmerge_update_str,
													   char *pmerge_insert_fields_str,
													   char *pmerge_insert_values_str
													   #endif
													 );


extern void STATION_GROUP_copy_group_into_guivars( const UNS_32 pgroup_index_0 );

extern void PRIORITY_copy_group_into_guivars( void );

extern void PERCENT_ADJUST_copy_group_into_guivars( void );

extern void SCHEDULE_copy_group_into_guivars( const UNS_32 pgroup_index_0 );

extern void WEATHER_copy_group_into_guivars( void);

extern void ALERT_ACTIONS_copy_group_into_guivars( void );

extern void ON_AT_A_TIME_copy_group_into_guivars( void );

extern void PUMP_copy_group_into_guivars( void );

extern void LINE_FILL_TIME_copy_group_into_guivars( void );

extern void DELAY_BETWEEN_VALVES_copy_group_into_guivars( void );

extern void ACQUIRE_EXPECTEDS_copy_group_into_guivars( void );

extern void BUDGET_REDUCTION_LIMITS_copy_group_into_guivars( void );



extern void STATION_GROUP_extract_and_store_group_name_from_GuiVars( void );

extern void STATION_GROUP_extract_and_store_changes_from_GuiVars( void );

extern void PRIORITY_extract_and_store_changes_from_GuiVars( void );

extern void PERCENT_ADJUST_extract_and_store_changes_from_GuiVars( void );

extern void SCHEDULE_extract_and_store_changes_from_GuiVars( void );

extern void WEATHER_extract_and_store_changes_from_GuiVars( void );

extern void RAIN_extract_and_store_changes_from_GuiVars( void );

extern void WIND_extract_and_store_changes_from_GuiVars( void );

extern void ALERT_ACTIONS_extract_and_store_changes_from_GuiVars( void );

extern void ON_AT_A_TIME_extract_and_store_changes_from_GuiVars( void );

extern void PUMP_extract_and_store_changes_from_GuiVars( void );

extern void LINE_FILL_TIME_extract_and_store_changes_from_GuiVars( void );

extern void DELAY_BETWEEN_VALVES_extract_and_store_changes_from_GuiVars( void );

extern void ACQUIRE_EXPECTEDS_extract_and_store_changes_from_GuiVars( void );

extern void BUDGET_REDUCTION_LIMITS_extract_and_store_changes_from_GuiVars( void );


extern void nm_STATION_GROUP_load_group_name_into_guivar( const INT_16 pindex_0_i16 );

// ----------

extern STATION_GROUP_STRUCT *STATION_GROUP_get_group_with_this_GID( const UNS_32 pgroup_ID );

extern void *STATION_GROUP_get_group_at_this_index( const UNS_32 pindex_0 );

extern UNS_32 STATION_GROUP_get_index_for_group_with_this_GID( const UNS_32 pgroup_ID );

extern UNS_32 STATION_GROUP_get_last_group_ID( void );

extern UNS_32 STATION_GROUP_get_num_groups_in_use( void );

// ----------

extern UNS_32 STATION_GROUP_get_plant_type( STATION_GROUP_STRUCT *const pgroup );

extern UNS_32 STATION_GROUP_get_head_type( STATION_GROUP_STRUCT *const pgroup );

extern UNS_32 STATION_GROUP_get_soil_type( STATION_GROUP_STRUCT *const pgroup );

extern UNS_32 STATION_GROUP_get_slope_percentage( STATION_GROUP_STRUCT *const pgroup );

extern float STATION_GROUP_get_precip_rate_in_per_hr( STATION_GROUP_STRUCT *const pgroup );

extern float STATION_GROUP_get_allowable_surface_accumulation( STATION_GROUP_STRUCT *const pgroup );

extern float STATION_GROUP_get_soil_intake_rate( STATION_GROUP_STRUCT *const pgroup );

extern BOOL_32 SCHEDULE_get_enabled( STATION_GROUP_STRUCT *const pgroup );

extern UNS_32 SCHEDULE_get_start_time( STATION_GROUP_STRUCT *const pgroup );

extern UNS_32 SCHEDULE_get_stop_time( STATION_GROUP_STRUCT *const pgroup );

extern UNS_32 SCHEDULE_get_schedule_type( STATION_GROUP_STRUCT *const pgroup );

extern UNS_32 SCHEDULE_get_mow_day( STATION_GROUP_STRUCT *const pgroup );

extern BOOL_32 SCHEDULE_does_it_irrigate_on_this_date( STATION_GROUP_STRUCT *const pgroup, const DATE_TIME_COMPLETE_STRUCT *const pDTCS, const UNS_32 action_to_take );

extern BOOL_32 SCHEDULE_get_irrigate_after_29th_or_31st( STATION_GROUP_STRUCT *const pgroup );

extern BOOL_32 DAILY_ET_get_et_in_use( STATION_GROUP_STRUCT *const pgroup );

extern BOOL_32 RAIN_get_rain_in_use( STATION_GROUP_STRUCT *const pgroup );

// ----------

extern UNS_32 PERCENT_ADJUST_get_percentage_100u_for_this_gid( const UNS_32 pGID, const UNS_32 pdate );

extern UNS_32 PERCENT_ADJUST_get_remaining_days_for_this_gid( const UNS_32 pGID, const UNS_32 pdate );

extern UNS_32 STATION_GROUP_get_cycle_time_10u_for_this_gid( const UNS_32 pGID );

extern UNS_32 STATION_GROUP_get_soak_time_for_this_gid( const UNS_32 pGID, const UNS_32 pdistribution_uniformity_100u );

// ----------

extern UNS_32 STATION_GROUP_get_station_precip_rate_in_per_hr_100000u( STATION_STRUCT *const pstation );

extern UNS_32 STATION_GROUP_get_station_crop_coefficient_100u( STATION_STRUCT *const pstation, const UNS_32 pmonth );

extern UNS_32 STATION_GROUP_get_crop_coefficient_100u( UNS_32 pGID, UNS_32 pmonth );

extern UNS_32 STATION_GROUP_get_station_usable_rain_percentage_100u( STATION_STRUCT *const pstation );

extern UNS_32 PRIORITY_get_station_priority_level( STATION_STRUCT *const pstation );

extern UNS_32 PERCENT_ADJUST_get_station_percentage_100u( STATION_STRUCT *const pstation, const UNS_32 pdate );

extern UNS_32 PERCENT_ADJUST_get_station_remaining_days( STATION_STRUCT *const pstation, const UNS_32 pdate );

extern UNS_32 SCHEDULE_get_station_schedule_type( STATION_STRUCT *const pstation );

extern BOOL_32 SCHEDULE_get_station_waters_this_day( STATION_STRUCT *const pstation, const UNS_32 pday );



extern BOOL_32 SCHEDULE_get_run_time_calculation_support( STATION_GROUP_STRUCT *psgs_ptr, RUN_TIME_CALC_SUPPORT_STRUCT *psupport_struct_ptr );


extern UNS_32 WEATHER_get_station_uses_daily_et( STATION_STRUCT *const pstation );

extern BOOL_32 WEATHER_get_station_uses_et_averaging( STATION_STRUCT *const pstation );

extern BOOL_32 WEATHER_get_station_uses_rain( STATION_STRUCT *const pstation );

extern UNS_32 STATION_GROUP_get_soil_storage_capacity_inches_100u( STATION_STRUCT *const pstation );

extern BOOL_32 WEATHER_get_station_uses_wind( STATION_STRUCT *const pstation );

extern UNS_32 ALERT_ACTIONS_get_station_high_flow_action( STATION_STRUCT *const pstation );

extern UNS_32 ALERT_ACTIONS_get_station_low_flow_action( STATION_STRUCT *const pstation );

extern BOOL_32 PUMP_get_station_uses_pump( STATION_STRUCT *const pstation );

extern UNS_32 LINE_FILL_TIME_get_station_line_fill_seconds( STATION_STRUCT *const pstation );

extern UNS_32 DELAY_BETWEEN_VALVES_get_station_slow_closing_valve_seconds( STATION_STRUCT *const pstation );

extern BOOL_32 ACQUIRE_EXPECTEDS_get_station_acquire_expected_flow( STATION_STRUCT *const pstation );


extern UNS_32 STATION_GROUP_get_GID_irrigation_system_for_this_station( STATION_STRUCT *const pstation );

extern UNS_32 STATION_GROUP_get_GID_irrigation_system( STATION_GROUP_STRUCT *const pgroup );


extern UNS_32 STATION_GROUP_get_budget_reduction_limit( STATION_GROUP_STRUCT *const pgroup );

extern UNS_32 STATION_GROUP_get_budget_reduction_limit_for_this_station( STATION_STRUCT *const pstation );
 
// ----------

extern void nm_STATION_GROUP_set_budget_start_time_flag( STATION_GROUP_STRUCT *pgroup, UNS_32 value );
extern UNS_32 nm_STATION_GROUP_get_budget_start_time_flag( STATION_GROUP_STRUCT *pgroup );
extern void STATION_GROUP_clear_budget_start_time_flags(); 
// ----------

extern UNS_32 ON_AT_A_TIME_get_on_at_a_time_within_system_for_this_gid( const UNS_32 pon_at_a_time_gid );

extern void ON_AT_A_TIME_init_all_groups_ON_count( void );

extern BOOL_32 ON_AT_A_TIME_can_another_valve_come_ON_within_this_group( const UNS_32 pon_at_a_time_gid );

extern void ON_AT_A_TIME_bump_this_groups_ON_count( const UNS_32 pon_at_a_time_gid );

extern void ACQUIRE_EXPECTEDS_clear_acquire_expected_flow_flag_for_GID( const UNS_32 pgroup_ID );

// ----------

extern char *STATION_GROUP_copy_details_into_group_name( char *pgroup_name, const UNS_32 pplant_type, const UNS_32 phead_type, const UNS_32 pexposure );

// ----------

extern CHANGE_BITS_64_BIT *STATION_GROUP_get_change_bits_ptr( STATION_GROUP_STRUCT *pgroup, const UNS_32 pchange_reason );

extern void STATION_GROUP_clean_house_processing( void );

extern void STATION_GROUP_set_bits_on_all_groups_to_cause_distribution_in_the_next_token( void );

extern void STATION_GROUP_on_all_groups_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear );

extern void nm_STATION_GROUP_update_pending_change_bits( const UNS_32 pcomm_error_occurred );


extern UNS_32 STATION_GROUPS_if_the_moisture_decoder_is_available_return_decoder_serial_number_for_this_station_group( STATION_GROUP_STRUCT *const psgs_ptr );


#ifndef _MSC_VER

extern BOOL_32 STATION_GROUPS_skip_irrigation_due_to_a_mow_day(	BOOL_32 pfor_ftimes,
																STATION_GROUP_STRUCT *const psgs_ptr,
																FT_STATION_STRUCT* pft_station_ptr,
																const DATE_TIME_COMPLETE_STRUCT *pdtcs_ptr );
																
#endif																

extern void STATION_GROUPS_load_ftimes_list( UNS_32 const pcalculation_date );



extern BOOL_32 STATION_GROUPS_calculate_chain_sync_crc( UNS_32 const pff_name );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

