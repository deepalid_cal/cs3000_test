
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_LIGHTS_NEW_H
#define _INC_LIGHTS_NEW_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"group_base_file.h"

#include	"cal_list.h"

#include	"cs3000_tpmicro_common.h"

#include	"foal_defs.h"

#include	"pdata_changes.h"

#include 	"cal_td_utils.h"

#include	"station_groups.h"



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */

// 6/10/2014 rmd : Because when the file is FIRST created and a placeholder group is added
// it should be set such that it is not really physically there. Its as if it is an
// uninstalled terminal light that is made by default.
#define LIGHTS_PHYSICALLY_AVAILABLE_DEFAULT	(false)

/* ---------------------------------------------------------- */

#define LIGHTS_SCHEDULE_DAYS_MAX	           ( 14 )
#define LIGHTS_DAY_INDEX_MIN	               ( 0 )
#define LIGHTS_DAY_INDEX_MAX	               ( LIGHTS_SCHEDULE_DAYS_MAX - 1 )
#define LIGHTS_DAY_INDEX_DEFAULT			   ( LIGHTS_DAY_INDEX_MIN )
											   
#define LIGHTS_START_DATE_MIN			       ( 0xA413 ) // Adjust value for Jan 1, 2015
#define LIGHTS_START_DATE_DEFAULT		       ( 0xA46D ) // April Fool's Day 2015
#define LIGHTS_BEGINNING_OF_TIME		       ( 0x9FCB ) // Sunday, January 1, 2012 (the last January 1 on a Sunday)
#define LIGHTS_END_OF_TIME				       ( 0xFFFF ) // I'll be retired
#define LIGHTS_START_DATE_MAX			       ( LIGHTS_END_OF_TIME )
			
// 8/24/2015 mpd : These defines are for all time values except GuiVars. They use minues.
#define LIGHTS_24_HOURS_IN_SECONDS		       ( 60 * 60* 24 )														// 86400 seconds
#define LIGHTS_OFF_TIME_IN_SECONDS		       ( SCHEDULE_OFF_TIME )												// 86400 seconds
#define LIGHTS_TIME_IN_SECONDS_MIN		       ( 0 )									 							// 12:00AM Midnight
#define LIGHTS_TEN_MINUTES_IN_SECONDS	       ( 10 * 60)			 												// 10 minute increment/decrements
#define LIGHTS_TIME_IN_SECONDS_MAX		       ( ( LIGHTS_24_HOURS_IN_SECONDS ) - LIGHTS_TEN_MINUTES_IN_SECONDS ) 	// 11:50PM (85800 seconds)
#define LIGHTS_FOUR_HOURS_IN_SECONDS	       ( 4 * 60 * 60 )														// Used for default manual stop time calculation
#define LIGHTS_8_PM_IN_SECONDS			       ( 20 * 60 * 60 )														// Used for default manual stop date rollover
											   
// 9/17/2015 mpd : Sodium lights have a minimum ON time. Use 20 minutes for now.
#define LIGHTS_MINIMUM_MANUAL_ON_IN_SECONDS    ( 20 * 60)			 												// 10 minute increment/decrements

// 8/31/2015 mpd : Only GuiVar times should be using these values.
#define LIGHTS_OFF_TIME_IN_MINUTES		       ( 60 * 24 )						  			// 1440 minutes
#define LIGHTS_MIDNIGHT_TIME_IN_MINUTES	 	   ( 0 )							 			// 12:00AM Midnight

// 7/14/2015 mpd : Time values for LIGHTS match what was done for STATIONS. Time is stored in seconds even though users
// are only allowed 10 minute granularity in the GUI. Zero is midnight. The last valid time is 11:50PM which is 85800 
// seconds. "SCHEDULE_OFF_TIME" is 86400 seconds. 
#define LIGHTS_START_TIME_MIN			       ( 0 ) 				
#define LIGHTS_START_TIME_MAX			       ( LIGHTS_24_HOURS_IN_SECONDS ) 
#define LIGHTS_START_TIME_DEFAULT		       ( LIGHTS_START_TIME_MAX )
											   
#define LIGHTS_STOP_TIME_MIN			       ( 0 )
#define LIGHTS_STOP_TIME_MAX			       ( LIGHTS_24_HOURS_IN_SECONDS )
#define LIGHTS_STOP_TIME_DEFAULT		       ( LIGHTS_STOP_TIME_MAX )

#define LIGHTS_SCHEDULED_OFF			       ( LIGHTS_24_HOURS_IN_SECONDS )

#define LIGHTS_OUTPUT_1						   ( 0 )
#define LIGHTS_OUTPUT_2						   ( 1 )
#define LIGHTS_OUTPUT_3						   ( 2 )
#define LIGHTS_OUTPUT_4						   ( 3 )
											   
#define LIGHTS_OUTPUT_INDEX_MIN				   ( LIGHTS_OUTPUT_1 )
#define LIGHTS_OUTPUT_INDEX_MAX				   ( LIGHTS_OUTPUT_4 )
#define LIGHTS_OUTPUT_INDEX_DEFAULT		  	   ( LIGHTS_OUTPUT_1 )

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct LIGHT_STRUCT LIGHT_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern MIST_LIST_HDR_TYPE	light_list_hdr;

extern char const LIGHTS_DEFAULT_NAME[];

extern DATE_TIME g_LIGHTS_initial_date_time;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct 
{
	// 7/17/2015 mpd : These times are in seconds to match the STATIONS implementation. Midnight is zero, 11:50PM is
	// 85800, and "off" is 86400.

	UNS_32			start_time_1_in_seconds;

	UNS_32			start_time_2_in_seconds;

	UNS_32			stop_time_1_in_seconds;

	UNS_32			stop_time_2_in_seconds;

} LIGHTS_DAY_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_file_LIGHTS( void );

extern void save_file_LIGHTS( void );

// ----------

extern void nm_LIGHTS_set_name( LIGHT_STRUCT *const plight,
								char *pname,
								const BOOL_32 pgenerate_change_line_bool,
								const UNS_32 preason_for_change,
								const UNS_32 pbox_index_0,
								const BOOL_32 pset_change_bits,
								CHANGE_BITS_32_BIT *pchange_bitfield_to_set
								#ifdef _MSC_VER
								, char *pmerge_update_str,
								char *pmerge_insert_fields_str,
								char *pmerge_insert_values_str
								#endif
							  );

extern void nm_LIGHTS_set_box_index( void *const plight,
									 UNS_32 pbox_index,
									 const BOOL_32 pgenerate_change_line_bool,
									 const UNS_32 preason_for_change,
									 const UNS_32 pbox_index_0,
									 const BOOL_32 pset_change_bits,
									 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
									 #ifdef _MSC_VER
									 , char *pmerge_update_str,
									 char *pmerge_insert_fields_str,
									 char *pmerge_insert_values_str
									 #endif
								   );

extern void nm_LIGHTS_set_output_index( void *const plight,
										UNS_32 plight_output_index,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const BOOL_32 pset_change_bits,
										CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
									  );

extern void nm_LIGHTS_set_physically_available( void *const plight,
												BOOL_32 pavailable,
												const BOOL_32 pgenerate_change_line_bool,
												const UNS_32 preason_for_change,
												const UNS_32 pbox_index_0,
												const BOOL_32 pset_change_bits,
												CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												#ifdef _MSC_VER
												, char *pmerge_update_str,
												char *pmerge_insert_fields_str,
												char *pmerge_insert_values_str
												#endif
											  );

extern void nm_LIGHTS_set_start_time_1( void *const plight,
										const UNS_32 pindex,
										UNS_32 plight_start_time_1,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const BOOL_32 pset_change_bits,
										CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
									  );

extern void nm_LIGHTS_set_start_time_2( void *const plight,
										const UNS_32 pindex,
										UNS_32 plight_start_time_2,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const BOOL_32 pset_change_bits,
										CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
									  );

extern void nm_LIGHTS_set_stop_time_1( void *const plight,
									   const UNS_32 pindex,
									   UNS_32 plight_stop_time_1,
									   const BOOL_32 pgenerate_change_line_bool,
									   const UNS_32 preason_for_change,
									   const UNS_32 pbox_index_0,
									   const BOOL_32 pset_change_bits,
									   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
									   #ifdef _MSC_VER
									   , char *pmerge_update_str,
									   char *pmerge_insert_fields_str,
									   char *pmerge_insert_values_str
									   #endif
									 );

extern void nm_LIGHTS_set_stop_time_2( void *const plight,
									   const UNS_32 pindex,
									   UNS_32 plight_stop_time_2,
									   const BOOL_32 pgenerate_change_line_bool,
									   const UNS_32 preason_for_change,
									   const UNS_32 pbox_index_0,
									   const BOOL_32 pset_change_bits,
									   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
									   #ifdef _MSC_VER
									   , char *pmerge_update_str,
									   char *pmerge_insert_fields_str,
									   char *pmerge_insert_values_str
									   #endif
									 );

// ----------

extern void *nm_LIGHTS_create_new_group( const UNS_32 pbox_index_0, const UNS_32 output_index );

// ----------

extern UNS_32 LIGHTS_build_data_to_send( UNS_8 **pucp,
										 const UNS_32 pmem_used_so_far,
										 BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
										 const UNS_32 preason_data_is_being_built,
										 const UNS_32 pallocated_memory );

extern UNS_32 nm_LIGHTS_extract_and_store_changes_from_comm( const unsigned char *pucp,
															 const UNS_32 preason_for_change,
															 const BOOL_32 pset_change_bits,
															 const UNS_32 pchanges_received_from
															 #ifdef _MSC_VER
															 , char *pSQL_statements,
															 char *pSQL_search_condition_str,
															 char *pSQL_update_str,
															 char *pSQL_insert_fields_str,
															 char *pSQL_insert_values_str,
															 char *pSQL_single_merge_statement_buf,
															 const UNS_32 pnetwork_ID
															 #endif
														   );

// ----------

#ifdef _MSC_VER

extern INT_32 LIGHTS_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order );

#endif

extern void LIGHTS_copy_guivars_to_daily_schedule( const UNS_32 pday_index );

extern void LIGHTS_copy_daily_schedule_to_guivars( const UNS_32 pday_index );

extern void LIGHTS_populate_group_name( const UNS_32 plight_index_0, const BOOL_32 long_format );

extern void LIGHTS_copy_light_struct_into_guivars( const UNS_32 plight_index_0 );

extern void LIGHTS_copy_file_schedule_into_global_daily_schedule( const UNS_32 plight_index_0 );

extern void LIGHTS_extract_and_store_group_name_from_GuiVars( void );

extern void LIGHTS_extract_and_store_changes_from_GuiVars( void );

// ----------

extern void LIGHTS_load_group_name_into_guivar( const INT_16 pindex_0_i16 );

// ----------

extern UNS_32 LIGHTS_get_index_using_ptr_to_light_struct( const LIGHT_STRUCT *const plight );

extern void *nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index( const UNS_32 pbox_index_0, const UNS_32 poutput_index_0 );

// ----------

extern void *LIGHTS_get_light_at_this_index( const UNS_32 pindex_0 );

extern UNS_32 LIGHTS_get_list_count( void );

// ----------

extern UNS_32 LIGHTS_get_lights_array_index( const UNS_32 pbox_index, const UNS_32 poutput_index );

extern UNS_32 LIGHTS_get_box_index( LIGHT_STRUCT *const plight );

extern UNS_32 LIGHTS_get_output_index( LIGHT_STRUCT *const plight );

extern BOOL_32 LIGHTS_get_physically_available( LIGHT_STRUCT *const plight );

extern UNS_32 LIGHTS_get_start_time_1( LIGHT_STRUCT *const plight, const UNS_32 pindex );

extern UNS_32 LIGHTS_get_start_time_2( LIGHT_STRUCT *const plight, const UNS_32 pindex );

extern UNS_32 LIGHTS_get_stop_time_1( LIGHT_STRUCT *const plight, const UNS_32 pindex );

extern UNS_32 LIGHTS_get_stop_time_2( LIGHT_STRUCT *const plight, const UNS_32 pindex );

extern UNS_32 LIGHTS_get_day_index( const UNS_32 numeric_date );

extern BOOL_32 LIGHTS_has_a_stop_time( LIGHT_STRUCT *const plight );

// ----------

extern CHANGE_BITS_32_BIT *LIGHTS_get_change_bits_ptr( LIGHT_STRUCT *plight, const UNS_32 pchange_reason );

// ----------

extern LIGHT_STRUCT *nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars( void );

extern LIGHT_STRUCT *nm_LIGHTS_get_next_available_light( UNS_32 *pbox_index_ptr, UNS_32 *poutput_index_0 );

extern LIGHT_STRUCT *nm_LIGHTS_get_prev_available_light( UNS_32 *pbox_index_ptr, UNS_32 *poutput_index_0 );

extern void LIGHTS_clean_house_processing( void );

extern void LIGHTS_set_not_physically_available_based_upon_communication_scan_results( void );

extern void LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token( void );

extern void LIGHTS_on_all_lights_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear );

extern void nm_LIGHTS_update_pending_change_bits( const UNS_32 pcomm_error_occurred );


extern BOOL_32 LIGHTS_calculate_chain_sync_crc( UNS_32 const pff_name );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

