/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

// 3/21/2016 skc : For __roundf()
#include	"cal_math.h"

#include	"station_groups.h"

#include	"cs_common.h"

#include	"app_startup.h"

#include	"bithacks.h"

#include	"change.h"

#include	"comm_mngr.h"

#include	"cs_mem.h"

#include	"e_station_selection_grid.h"

#include	"epson_rx_8025sa.h"

#include	"flash_storage.h"

#include	"flowsense.h"

#include	"foal_comm.h"

#include	"group_base_file.h"

#include	"pdata_changes.h"

#include	"range_check.h"

#include	"r_alerts.h"

#include	"shared.h"

#include	"watersense.h"

#include	"weather_control.h"

#include	"controller_initiated.h"

#include	"chain_sync_vars.h"

#include	"chain_sync_funcs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static MIST_LIST_HDR_TYPE	station_group_group_list_hdr;

// ----------

// 2/18/2016 rmd : Must include the shared c source after the
// station_group_group_list_header declaration.
#include	"shared_station_groups.c"

// ----------

static const char LANDSCAPE_DETAILS_FILENAME[] =	"LANDSCAPE_DETAILS";

const char STATION_GROUP_DEFAULT_NAME[] =			"Station Group";

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	UNS_32			plant_type;

	UNS_32			head_type;

	UNS_32			precip_rate_in_100000u;

	UNS_32			soil_type;

	UNS_32			slope_percentage_100u;

	UNS_32			exposure;

	UNS_32			usable_rain_percentage_100u;

	UNS_32			crop_coefficient_100u[ MONTHS_IN_A_YEAR ];


	UNS_32			priority_level;

	UNS_32			percent_adjust_100u;

	UNS_32			percent_adjust_start_date;

	UNS_32			percent_adjust_end_date;

	BOOL_32			schedule_enabled_bool;

	UNS_32			schedule_type;

	UNS_32			start_time;
	UNS_32			stop_time;

	UNS_32			begin_date;

	BOOL_32			water_days_bool[ DAYS_IN_A_WEEK ];

	BOOL_32			irrigate_on_29th_or_31st_bool;

	UNS_32			mow_day;

	DATE_TIME		last_ran_dt;

	UNS_32			tail_ends_throw_away_upperbound_percentage_100u;

	UNS_32			tail_ends_make_adjustment_upperbound_percentage_100u;

	UNS_32			maximum_leftover_irrigation_percentage_100u;

	BOOL_32			et_mode;

	BOOL_32			use_et_averaging_bool;

	BOOL_32			rain_in_use_bool;

	UNS_32			maximum_accumulation_inches_100u;

	BOOL_32			wind_in_use_bool;

	UNS_32			high_flow_action;

	UNS_32			low_flow_action;

	UNS_32			on_at_a_time_in_group;

	UNS_32			on_at_a_time_in_system;

	UNS_32			presently_ON_group_count;

	BOOL_32			pump_in_use;

	UNS_32			line_fill_time_sec;
	UNS_32			valve_close_time_sec;

	BOOL_32			acquire_expecteds;

	CHANGE_BITS_64_BIT	changes_to_send_to_master;
	CHANGE_BITS_64_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_64_BIT	changes_to_upload_to_comm_server;

} STATION_GROUP_STRUCT_REV_0_and_1;

// ----------

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	UNS_32			plant_type;

	UNS_32			head_type;

	UNS_32			precip_rate_in_100000u;

	UNS_32			soil_type;

	UNS_32			slope_percentage;

	UNS_32			exposure;

	UNS_32			usable_rain_percentage_100u;

	UNS_32			crop_coefficient_100u[ MONTHS_IN_A_YEAR ];

	UNS_32			priority_level;

	UNS_32			percent_adjust_100u;

	UNS_32			percent_adjust_start_date;

	UNS_32			percent_adjust_end_date;

	BOOL_32			schedule_enabled_bool;

	UNS_32			schedule_type;

	UNS_32			start_time;
	UNS_32			stop_time;

	UNS_32			begin_date;

	BOOL_32			water_days_bool[ DAYS_IN_A_WEEK ];

	BOOL_32			irrigate_on_29th_or_31st_bool;

	UNS_32			mow_day;

	DATE_TIME		last_ran_dt;

	UNS_32			tail_ends_throw_away_upperbound_percentage_100u;

	UNS_32			tail_ends_make_adjustment_upperbound_percentage_100u;

	UNS_32			maximum_leftover_irrigation_percentage_100u;

	BOOL_32			et_in_use;

	BOOL_32			use_et_averaging_bool;

	BOOL_32			rain_in_use_bool;

	UNS_32			soil_storage_capacity_inches_100u;

	BOOL_32			wind_in_use_bool;

	UNS_32			high_flow_action;

	UNS_32			low_flow_action;

	UNS_32			on_at_a_time_in_group;

	UNS_32			on_at_a_time_in_system;

	UNS_32			presently_ON_group_count;

	BOOL_32			pump_in_use;

	UNS_32			line_fill_time_sec;
	UNS_32			valve_close_time_sec;


	BOOL_32			acquire_expecteds;

	CHANGE_BITS_64_BIT	changes_to_send_to_master;
	CHANGE_BITS_64_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_64_BIT	changes_to_upload_to_comm_server;

	UNS_32			allowable_depletion_100u;

	UNS_32			available_water_100u;

	UNS_32			root_zone_depth_10u;

	UNS_32			species_factor_100u;

	UNS_32			density_factor_100u;

	UNS_32			microclimate_factor_100u;

	UNS_32			soil_intake_rate_100u;

	UNS_32			allowable_surface_accumulation_100u;

} STATION_GROUP_STRUCT_REV_2;

// ----------

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	UNS_32			plant_type;

	UNS_32			head_type;

	UNS_32			precip_rate_in_100000u;

	UNS_32			soil_type;

	UNS_32			slope_percentage;

	UNS_32			exposure;

	UNS_32			usable_rain_percentage_100u;

	UNS_32			crop_coefficient_100u[ MONTHS_IN_A_YEAR ];

	UNS_32			priority_level;

	UNS_32			percent_adjust_100u;

	UNS_32			percent_adjust_start_date;

	UNS_32			percent_adjust_end_date;

	BOOL_32			schedule_enabled_bool;

	UNS_32			schedule_type;

	UNS_32			start_time;
	UNS_32			stop_time;

	UNS_32			begin_date;

	BOOL_32			water_days_bool[ DAYS_IN_A_WEEK ];

	BOOL_32			irrigate_on_29th_or_31st_bool;

	UNS_32			mow_day;

	DATE_TIME		last_ran_dt;

	UNS_32			nlu_tail_ends_throw_away_upperbound_percentage_100u;

	UNS_32			nlu_tail_ends_make_adjustment_upperbound_percentage_100u;

	UNS_32			nlu_maximum_leftover_irrigation_percentage_100u;

	BOOL_32			et_in_use;

	BOOL_32			use_et_averaging_bool;

	BOOL_32			rain_in_use_bool;

	UNS_32			soil_storage_capacity_inches_100u;

	BOOL_32			wind_in_use_bool;

	UNS_32			high_flow_action;

	UNS_32			low_flow_action;

	UNS_32			on_at_a_time_in_group;

	UNS_32			on_at_a_time_in_system;

	UNS_32			presently_ON_group_count;

	BOOL_32			pump_in_use;

	UNS_32			line_fill_time_sec;
	UNS_32			delay_between_valve_time_sec;

	BOOL_32			acquire_expecteds;

	CHANGE_BITS_64_BIT	changes_to_send_to_master;
	CHANGE_BITS_64_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_64_BIT	changes_to_upload_to_comm_server;

	UNS_32			allowable_depletion_100u;

	UNS_32			available_water_100u;

	UNS_32			root_zone_depth_10u;

	UNS_32			species_factor_100u;

	UNS_32			density_factor_100u;

	UNS_32			microclimate_factor_100u;

	UNS_32			soil_intake_rate_100u;

	UNS_32			allowable_surface_accumulation_100u;

	CHANGE_BITS_64_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

} STATION_GROUP_STRUCT_REV_3;

// ----------

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	UNS_32			plant_type;

	UNS_32			head_type;

	UNS_32			precip_rate_in_100000u;

	UNS_32			soil_type;

	UNS_32			slope_percentage;

	UNS_32			exposure;

	UNS_32			usable_rain_percentage_100u;

	UNS_32			crop_coefficient_100u[ MONTHS_IN_A_YEAR ];

	UNS_32			priority_level;

	UNS_32			percent_adjust_100u;

	UNS_32			percent_adjust_start_date;

	UNS_32			percent_adjust_end_date;

	BOOL_32			schedule_enabled_bool;

	UNS_32			schedule_type;

	UNS_32			start_time;
	UNS_32			stop_time;

	UNS_32			begin_date;

	BOOL_32			water_days_bool[ DAYS_IN_A_WEEK ];

	BOOL_32			irrigate_on_29th_or_31st_bool;

	UNS_32			mow_day;

	DATE_TIME		last_ran_dt;

	UNS_32			nlu_tail_ends_throw_away_upperbound_percentage_100u;

	UNS_32			nlu_tail_ends_make_adjustment_upperbound_percentage_100u;

	UNS_32			nlu_maximum_leftover_irrigation_percentage_100u;

	BOOL_32			et_in_use;

	BOOL_32			use_et_averaging_bool;

	BOOL_32			rain_in_use_bool;

	UNS_32			soil_storage_capacity_inches_100u;

	BOOL_32			wind_in_use_bool;

	UNS_32			high_flow_action;

	UNS_32			low_flow_action;

	UNS_32			on_at_a_time_in_group;

	UNS_32			on_at_a_time_in_system;

	UNS_32			presently_ON_group_count;

	BOOL_32			pump_in_use;

	UNS_32			line_fill_time_sec;
	UNS_32			delay_between_valve_time_sec;

	BOOL_32			acquire_expecteds;

	CHANGE_BITS_64_BIT	changes_to_send_to_master;
	CHANGE_BITS_64_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_64_BIT	changes_to_upload_to_comm_server;

	UNS_32			allowable_depletion_100u;

	UNS_32			available_water_100u;

	UNS_32			root_zone_depth_10u;

	UNS_32			species_factor_100u;

	UNS_32			density_factor_100u;

	UNS_32			microclimate_factor_100u;

	UNS_32			soil_intake_rate_100u;

	UNS_32			allowable_surface_accumulation_100u;

	CHANGE_BITS_64_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

	UNS_32			budget_reduction_percentage_cap;

} STATION_GROUP_STRUCT_REV_4;

// ----------

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	UNS_32			plant_type;

	UNS_32			head_type;

	UNS_32			precip_rate_in_100000u;

	UNS_32			soil_type;

	UNS_32			slope_percentage;

	UNS_32			exposure;

	UNS_32			usable_rain_percentage_100u;

	UNS_32			crop_coefficient_100u[ MONTHS_IN_A_YEAR ];

	// -------------------
	
	// The priority level - Low (1), Medium (2), or High (3)
	UNS_32			priority_level;

	// -------------------
	
	// The percentage to increase or decrease the station's run times by
	UNS_32			percent_adjust_100u;

	// The date to start the percent adjust
	UNS_32			percent_adjust_start_date;

	// The date to end the percent adjust
	UNS_32			percent_adjust_end_date;

	// -------------------

	// TODO Add enabled flag to UI
	BOOL_32			schedule_enabled_bool;

	UNS_32			schedule_type;

	UNS_32			start_time;
	UNS_32			stop_time;

	UNS_32			begin_date;

	BOOL_32			water_days_bool[ DAYS_IN_A_WEEK ];

	// 4/7/2016 rmd : This is IMPROPERLY named. What it should say is
	// irrigates_after_29th_or_31st. And applies to only an ODD day irrigation schedule when we
	// are on the FIRST of the month when the prior month ended with 29 or 31 days. This
	// variable if (true) signifies we should irrigate in that case on the FIRST even after
	// irrigating the day before.
	BOOL_32			irrigate_on_29th_or_31st_bool;

	UNS_32			mow_day;

	// Used to determine which alerts to display when showing 'ALERTS TO SEE'.
	// Set at the start time on a watering day.
	DATE_TIME		last_ran_dt;

	// 7/7/2015 ajv : Tail-Ends is no longer used now that the WaterSense specification requires
	// all run times to be greater than 3 minutes.
	UNS_32			nlu_tail_ends_throw_away_upperbound_percentage_100u;

	// 7/7/2015 ajv : Tail-Ends is no longer used now that the WaterSense specification requires
	// all run times to be greater than 3 minutes.
	UNS_32			nlu_tail_ends_make_adjustment_upperbound_percentage_100u;

	// 7/7/2015 ajv : Left-over irrigation has been removed to make room for WaterSense-related
	// values in the Station Preserves structure.
	UNS_32			nlu_maximum_leftover_irrigation_percentage_100u;

	BOOL_32			et_in_use;

	// 11/12/2015 ajv : Muddy Mondays - smoothed out the skip day effect in DailyET such that
	// we can now irrigate the same amount each day even following skip days. We take the
	// skip day water and spread it out evenly across all the irrigation days in the schedule.
	// This "ET AVERAGING" as we should probably call it can be enabled or disabled by program
	// in set-up. After your ROM update it will be enabled and controllers will ship with this
	// enabled when you first turn on daily ET.
	//
	// 4/8/2016 rmd : NOTE - ONLY affects the irrigated amount when irrigating using a weekly
	// schedule.
	BOOL_32			use_et_averaging_bool;

	BOOL_32			rain_in_use_bool;

	// The maximum amount of rain allowed to build up.
	UNS_32			soil_storage_capacity_inches_100u;

	BOOL_32			wind_in_use_bool;

	UNS_32			high_flow_action;

	UNS_32			low_flow_action;

	// The number of stations allowed to turn on a a time within this group
	UNS_32			on_at_a_time_in_group;

	// The number of stations allowed to turn on a a time within the system.
	// Note that this may cross multiple systems.
	UNS_32			on_at_a_time_in_system;

	// 2011.11.02 rmd : The dynamically maintained count of stations ON in this
	// group. Once per second (on average) the foal irrigation maintenance
	// function zeros and then updates this number. And uses it to DECIDE if
	// another valve from this group can turn ON.
	//
	// The system count is extracted by looking at the system count for each
	// station ON (that allowed value is part of this group but we could have
	// valve ON across several groups). And 'remembering' the lowest allowed
	// within system value (by sytem .. and therefore it is stored within the
	// system (file) list similarly to how this one is stored.
	//
	// We do not care about saving these numbers to the actual file system. It's
	// okay if it happens. Gee would we ever get into a position where we'll
	// read out a file in the midst of irrigation. I think not. But beware!
	UNS_32			presently_ON_group_count;

	// -------------------
	
	BOOL_32			pump_in_use;

	// -------------------

	UNS_32			line_fill_time_sec;
	UNS_32			delay_between_valve_time_sec;

	// -------------------

	BOOL_32			acquire_expecteds;

	// -------------------------------------------- //
	//  CHANGE BITS
	// -------------------------------------------- //

	CHANGE_BITS_64_BIT	changes_to_send_to_master;
	CHANGE_BITS_64_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_64_BIT	changes_to_upload_to_comm_server;

	// -------------------

	UNS_32			allowable_depletion_100u;

	UNS_32			available_water_100u;

	UNS_32			root_zone_depth_10u;

	UNS_32			species_factor_100u;

	UNS_32			density_factor_100u;

	UNS_32			microclimate_factor_100u;

	UNS_32			soil_intake_rate_100u;

	UNS_32			allowable_surface_accumulation_100u;

	// ----------
	
	// 4/30/2015 ajv : Added in Rev 3

	// ----------
	
	CHANGE_BITS_64_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

	// ----------
	
	// 9/9/2015 rmd : Added in Rev 4

	// ----------
	
	// 9/18/2015 rmd : A whole number which is a percentage of the scheduled irrigation. Range
	// is 0% to 100%.
	UNS_32			budget_reduction_percentage_cap;

	// ----------

	// 9/22/2015 ajv : Added in Rev 5

	// 9/22/2015 ajv : From this point forth, station groups are assigned to mainlines rather
	// than individual stations. This resolves a number of issues related to on-at-a-time, flow
	// checking, and budgets which were caused by a station group consisting of stations
	// spanning multiple mainlines.
	UNS_32			GID_irrigation_system;

	// 9/22/2015 ajv : By default, all mainline are "in use". If a user deletes a mainline, this
	// flag is set false. Although this keeps the item in memory and in the file, this is
	// necessary so the change can propagate through the chain and to the CommServer. If this
	// flag is FALSE, it will be hidden from view and completely inaccessible to the user.
	BOOL_32			in_use;
	
	// ----------
	
	// 4/8/2016 rmd : REV 6 made no size change. Needed to roll number to force updater to run
	// to set ET Averaging to (true).
	
} STATION_GROUP_STRUCT_REV_5_AND_REV_6;

// ----------

// 1/26/2015 rmd : NOTE - upon REVISION update the record can only grow in size. At least
// that is the way I've coded the flash file supporting functions. It may be possible to
// decrease the record size. I just haven't gone through the code with that in mind.
#define	LANDSCAPE_DETAILS_LATEST_FILE_REVISION				(7)

// ----------

const UNS_32 station_group_list_item_sizes[ LANDSCAPE_DETAILS_LATEST_FILE_REVISION + 1 ] =
{
	// 1/26/2015 rmd : In order for this array to contain OLDER list item sizes the implication
	// is that the older structure definitions are to be retained. That means the OLDER, what
	// was, structure definition has a different name.

	// ----------
	
	sizeof( STATION_GROUP_STRUCT_REV_0_and_1 ),

	// 2/4/2015 rmd : Revision 1 is the same size. Needed a variable set. See updater below.
	sizeof( STATION_GROUP_STRUCT_REV_0_and_1 ),

	// 3/13/2015 ajv : Revision 2 introduces a variety of WaterSense-related variables to the
	// Station Groups structure. Additionally, it redefines a handful of settings including
	// slope percentage, plant type, and head type.
	sizeof( STATION_GROUP_STRUCT_REV_2 ),

	// 9/9/2015 rmd : Revision 3 added the changes uploaded to commserver tracking variable.
	sizeof( STATION_GROUP_STRUCT_REV_3 ),

	// 9/22/2015 rmd : Revision 4 added the budget reduction percentage cap
	sizeof( STATION_GROUP_STRUCT_REV_4 ),

	sizeof( STATION_GROUP_STRUCT_REV_5_AND_REV_6 ),

	sizeof( STATION_GROUP_STRUCT_REV_5_AND_REV_6 ),

	// 4/8/2016 rmd : This is the REV 7 structure.
	sizeof( STATION_GROUP_STRUCT )
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void STATION_GROUP_prepopulate_default_groups( void );

static void nm_STATION_GROUP_set_default_values( void *const pgroup, const BOOL_32 pset_change_bits );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void nm_station_group_updater( UNS_32 pfrom_revision )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only when
	// the file revision is changed. The caller is to be holding the list_program_data
	// recursive_MUTEX.

	// ----------
	
	// 2/27/2015 ajv : When functions are declared as static, the compiler may inline the
	// functions and optimize out floating-point constant literals from the calculations. This
	// can result in a division-by-0 error. In order to resolve this, we declare the constant
	// literal as volatile which tells the compile to not optimize the value.
	const volatile float ONE_HUNDRED_THOUSAND = 100000.0F;

	const volatile float ONE_HUNDRED = 100.0F;

	const volatile float TEN = 10.0F;

	// ----------
	
	STATION_GROUP_STRUCT	*lsgs;
	
	CHANGE_BITS_64_BIT		*lbitfield_of_changes;

	UNS_32	box_index_0;

	UNS_32	i;

	// ----------

	// 9/21/2015 rmd : Because of the order of the init_file function calls in the app_startup
	// task we know the controller index is valid for use at this [early] point during the boot
	// process that this updater function is called.
	box_index_0 = FLOWSENSE_get_controller_index();

	// ----------
	
	if( pfrom_revision == LANDSCAPE_DETAILS_LATEST_FILE_REVISION )
	{
		Alert_Message_va( "LNDSCAPE DTLS file unexpd update %u", pfrom_revision );	
	}
	else
	{
		Alert_Message_va( "LNDSCAPE DTLS file update : to revision %u from %u", LANDSCAPE_DETAILS_LATEST_FILE_REVISION, pfrom_revision );

		// ----------
		
		if( pfrom_revision == 0 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 0 to REVISION 1. Because
			// of revision 0 bugs the upload to comm_server bits are in an unknown state. They need to
			// be set to trigger a complete send of PDATA to the comm_server.

			lsgs = nm_ListGetFirst( &station_group_group_list_hdr );
		
			while( lsgs != NULL )
			{
				// 1/30/2015 rmd : Because of revision 0 bugs these bits were not set. We need to set them
				// to guarantee all the program data is sent to the comm_server this one time.
				lsgs->changes_to_upload_to_comm_server = UNS_32_MAX;
				
				// ----------
				
				lsgs = nm_ListGetNext( &station_group_group_list_hdr, lsgs );
			}
			
			// ----------
			
			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 1 to 2 changes if
			// needed.
			pfrom_revision += 1;
		}
		
		// ----------
		
		if( pfrom_revision == 1 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 1 to REVISION 2.

			#define	STATION_GROUP_REV_1_PLANT_TYPE_TURF				(0)
			#define	STATION_GROUP_REV_1_PLANT_TYPE_SHRUBS			(1)
			#define	STATION_GROUP_REV_1_PLANT_TYPE_TREES			(2)
			#define	STATION_GROUP_REV_1_PLANT_TYPE_COLOR_BEDS		(3)

			#define	STATION_GROUP_REV_1_HEAD_TYPE_SPRAY				(0)
			#define STATION_GROUP_REV_1_HEAD_TYPE_ROTOR				(1)
			#define STATION_GROUP_REV_1_HEAD_TYPE_BUBBLER			(2)
			#define STATION_GROUP_REV_1_HEAD_TYPE_DRIP				(3)

			// ----------

			UNS_32	lnew_plant_type;

			UNS_32	lnew_head_type;

			UNS_32	lnew_slope_percentage;

			float	Kl_100u;

			float	RZWWS_100u;

			char	str_48[ NUMBER_OF_CHARS_IN_A_NAME ];

			// ----------

			lsgs = nm_ListGetFirst( &station_group_group_list_hdr );

			while( lsgs != NULL )
			{
				// 5/2/2014 ajv : When editing a group, always set the changes to be sent to the master.
				// Once the master receives the changes, it will distribute them back out to the slaves.
				lbitfield_of_changes = &((STATION_GROUP_STRUCT *)lsgs)->changes_to_send_to_master;

				// ----------
				
				switch( lsgs->plant_type )
				{
					case STATION_GROUP_REV_1_PLANT_TYPE_TURF:
						lnew_plant_type = STATION_GROUP_PLANT_TYPE_COOL_SEASON_TURF;
						break;

					case STATION_GROUP_REV_1_PLANT_TYPE_SHRUBS:
						lnew_plant_type = STATION_GROUP_PLANT_TYPE_SHRUBS_MEDIUM;
						break;

					case STATION_GROUP_REV_1_PLANT_TYPE_TREES:
						lnew_plant_type = STATION_GROUP_PLANT_TYPE_TREES;
						break;

					case STATION_GROUP_REV_1_PLANT_TYPE_COLOR_BEDS:
						lnew_plant_type = STATION_GROUP_PLANT_TYPE_ANNUALS;
						break;

					default:
						// 3/13/2015 ajv : Just in case the original value was out-of-range, set it to the default.
						lnew_plant_type = STATION_GROUP_PLANT_TYPE_DEFAULT;
				}

				nm_STATION_GROUP_set_plant_type( lsgs, lnew_plant_type, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				// ----------

				if( lsgs->slope_percentage <= 3 )
				{
					lnew_slope_percentage = STATION_GROUP_SLOPE_0_3_PERCENT;
				}
				else if( (lsgs->slope_percentage >= 4) && (lsgs->slope_percentage <= 6) )
				{
					lnew_slope_percentage = STATION_GROUP_SLOPE_4_6_PERCENT;
				}
				else if( (lsgs->slope_percentage >= 7) && (lsgs->slope_percentage <= 12) )
				{
					lnew_slope_percentage = STATION_GROUP_SLOPE_7_12_PERCENT;
				}
				else
				{
					lnew_slope_percentage = STATION_GROUP_SLOPE_13_PLUS_PERCENT;
				}

				nm_STATION_GROUP_set_slope_percentage( lsgs, lnew_slope_percentage, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				// ----------

				RZWWS_100u = (WATERSENSE_get_RZWWS( MAD[ lsgs->soil_type ], AW[ lsgs->soil_type ], ROOT_ZONE_DEPTH[ lnew_plant_type ][ lsgs->soil_type ] ) * ONE_HUNDRED);

				nm_STATION_GROUP_set_soil_storage_capacity( lsgs, (UNS_32)RZWWS_100u, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				// ----------

				switch( lsgs->head_type )
				{
					case STATION_GROUP_REV_1_HEAD_TYPE_SPRAY:
						lnew_head_type = STATION_GROUP_HEAD_TYPE_SPRAY;
						break;

					case STATION_GROUP_REV_1_HEAD_TYPE_ROTOR:
						lnew_head_type = STATION_GROUP_HEAD_TYPE_ROTOR_PART;
						break;

					case STATION_GROUP_REV_1_HEAD_TYPE_BUBBLER:
						lnew_head_type = STATION_GROUP_HEAD_TYPE_BUBBLER;
						break;

					case STATION_GROUP_REV_1_HEAD_TYPE_DRIP:
						lnew_head_type = STATION_GROUP_HEAD_TYPE_DRIP;
						break;

					default:
						// 3/13/2015 ajv : Just in case the original value was out-of-range, set it to the default.
						lnew_head_type = STATION_GROUP_HEAD_TYPE_DEFAULT;
				}

				nm_STATION_GROUP_set_head_type( lsgs, lnew_head_type, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				// ----------
				
				nm_STATION_GROUP_set_precip_rate( lsgs, (UNS_32)(PR[ lnew_head_type ] * ONE_HUNDRED_THOUSAND), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				// ----------

				Kl_100u = WATERSENSE_get_Kl( Ks[ lnew_plant_type ], Kd[ lnew_plant_type ][ STATION_GROUP_DENSITY_FACTOR_AVERAGE ], Kmc[ lnew_plant_type ][ WATERSENSE_get_Kmc_index_based_on_exposure( lsgs->exposure ) ] ) * ONE_HUNDRED;

				for( i = 0; i < MONTHS_IN_A_YEAR; ++i )
				{
					nm_STATION_GROUP_set_crop_coefficient( lsgs, i, (UNS_32)(Kl_100u), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
				}

				// ----------

				// 3/13/2015 ajv : Since WaterSense does not use ET Averaging, we've changed the default
				// value from TRUE to FALSE and are updating the existing groups here.
				nm_DAILY_ET_set_use_ET_averaging( lsgs, WEATHER_ET_AVERAGING_IN_USE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				// ----------
				
				STATION_GROUP_copy_details_into_group_name( str_48, lnew_plant_type, lnew_head_type, lsgs->exposure );
				
				SHARED_set_name_64_bit_change_bits( lsgs, str_48, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes, &lsgs->changes_to_upload_to_comm_server, STATION_GROUP_CHANGE_BITFIELD_description_bit
													#ifdef _MSC_VER
													, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_description_bit ], pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
													#endif
												  );
				
				// ----------

				// 3/16/2015 ajv : Initialize the new variables. It's important, at least in this case, that
				// the inits occur AFTER the existing values have been updated because we're relying on the
				// new values to index into a variety of arrays to set the default values.
				nm_STATION_GROUP_set_allowable_depletion( lsgs, (UNS_32)(MAD[ lsgs->soil_type ] * ONE_HUNDRED), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				nm_STATION_GROUP_set_available_water( lsgs, (UNS_32)(AW[ lsgs->soil_type ] * ONE_HUNDRED), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				nm_STATION_GROUP_set_root_zone_depth( lsgs, (UNS_32)(ROOT_ZONE_DEPTH[ lnew_plant_type ][ lsgs->soil_type ] * TEN), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				nm_STATION_GROUP_set_species_factor( lsgs, (UNS_32)(Ks[ lnew_plant_type ] * ONE_HUNDRED), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				nm_STATION_GROUP_set_density_factor( lsgs, (UNS_32)(Kd[ lnew_plant_type ][ STATION_GROUP_DENSITY_FACTOR_AVERAGE ] * ONE_HUNDRED), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				nm_STATION_GROUP_set_microclimate_factor( lsgs, (UNS_32)(Kmc[ lnew_plant_type ][ WATERSENSE_get_Kmc_index_based_on_exposure( lsgs->exposure ) ] * ONE_HUNDRED), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				nm_STATION_GROUP_set_soil_intake_rate( lsgs, (UNS_32)(IR[ lsgs->soil_type ] * ONE_HUNDRED), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				nm_STATION_GROUP_set_allowable_surface_accumulation( lsgs, (UNS_32)(ASA[ lsgs->soil_type ][ lnew_slope_percentage ] * ONE_HUNDRED), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				// ----------

				lsgs = nm_ListGetNext( &station_group_group_list_hdr, lsgs );
			}

			// ----------

			pfrom_revision += 1;
		}

		// ----------

		if( pfrom_revision == 2 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 2 to REVISION 3.
			lsgs = nm_ListGetFirst( &station_group_group_list_hdr );

			while( lsgs != NULL )
			{
				// 4/30/2015 ajv : Clear all of the pending Program Data bits.
				lsgs->changes_uploaded_to_comm_server_awaiting_ACK = CHANGE_BITS_32_BIT_ALL_CLEAR;

				lsgs = nm_ListGetNext( &station_group_group_list_hdr, lsgs );
			}

			// ----------

			pfrom_revision += 1;
		}

		// ----------
		
		if( pfrom_revision == 3 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 3 to REVISION 4.
			lsgs = nm_ListGetFirst( &station_group_group_list_hdr );

			while( lsgs != NULL )
			{
				// 5/2/2014 ajv : When editing a group, always set the changes to be sent to the master.
				// Once the master receives the changes, it will distribute them back out to the slaves.
				lbitfield_of_changes = &((STATION_GROUP_STRUCT *)lsgs)->changes_to_send_to_master;

				// ----------
				
				// 9/9/2015 rmd : Set the budget percentage cap to the default [25%].
				nm_STATION_GROUP_set_budget_reduction_percentage_cap( lsgs, STATION_GROUP_BUDGET_PERCENTAGE_CAP_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
				
				lsgs = nm_ListGetNext( &station_group_group_list_hdr, lsgs );
			}

			// ----------

			pfrom_revision += 1;
		}

		// ----------
		
		if( pfrom_revision == 4 )
		{
			// 9/22/2015 ajv : This is the work to do when moving from REVISION 4 to REVISION 5.
			lsgs = nm_ListGetFirst( &station_group_group_list_hdr );

			while( lsgs != NULL )
			{
				// 9/22/2015 ajv : When editing a group, always set the changes to be sent to the master.
				// Once the master receives the changes, it will distribute them back out to the slaves.
				lbitfield_of_changes = &((STATION_GROUP_STRUCT *)lsgs)->changes_to_send_to_master;

				// ----------

				nm_STATION_GROUP_set_GID_irrigation_system( lsgs, nm_GROUP_get_group_ID( SYSTEM_get_group_at_this_index( 0 ) ), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				nm_STATION_GROUP_set_in_use( lsgs, STATION_GROUP_IN_USE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				lsgs = nm_ListGetNext( &station_group_group_list_hdr, lsgs );
			}

			// ----------

			pfrom_revision += 1;
		}

		// ----------
		
		if( pfrom_revision == 5 )
		{
			// 9/22/2015 ajv : This is the work to do when moving from REVISION 5 to REVISION 6.
			lsgs = nm_ListGetFirst( &station_group_group_list_hdr );

			while( lsgs != NULL )
			{
				// 9/22/2015 ajv : When editing a group, always set the changes to be sent to the master.
				// Once the master receives the changes, it will distribute them back out to the slaves.
				lbitfield_of_changes = &((STATION_GROUP_STRUCT *)lsgs)->changes_to_send_to_master;

				// ----------

				// 4/8/2016 rmd : NOTE - moving from revision 5 to 6 was done only to initialize the
				// ETAveraging variable to (true). There was NO structure SIZE or otherwise change.
				
				// 4/8/2016 rmd : We have decided to re-introduce ET AVERAGING for the user. And to turn it
				// ON by default and upon this ROM upgrade change the setting to (true).
				nm_DAILY_ET_set_use_ET_averaging( lsgs, WEATHER_ET_AVERAGING_IN_USE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				// ----------

				// 5/2/2016 ajv : Also, we've changed the value for on-at-a-time "X" to no longer be
				// UNS_32_MAX because it caused problems for Wissam. Therefore, if the user has it already
				// set to the old "X" value, set it to the new one now.
				if( lsgs->on_at_a_time_in_group == UNS_32_MAX )
				{
					nm_ON_AT_A_TIME_set_on_at_a_time_in_group( lsgs, ON_AT_A_TIME_X, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
				}
				
				if( lsgs->on_at_a_time_in_system == UNS_32_MAX )
				{
					nm_ON_AT_A_TIME_set_on_at_a_time_in_system( lsgs, ON_AT_A_TIME_X, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
				}


				// ----------
				
				lsgs = nm_ListGetNext( &station_group_group_list_hdr, lsgs );
			}

			// ----------

			pfrom_revision += 1;
		}

		// ----------
		
		if( pfrom_revision == 6 )
		{
			// 9/22/2015 ajv : This is the work to do when moving from REVISION 6 to REVISION 7.
			lsgs = nm_ListGetFirst( &station_group_group_list_hdr );

			while( lsgs != NULL )
			{
				// 9/22/2015 ajv : When editing a group, always set the changes to be sent to the master.
				// Once the master receives the changes, it will distribute them back out to the slaves.
				lbitfield_of_changes = &((STATION_GROUP_STRUCT *)lsgs)->changes_to_send_to_master;

				// ----------

				// 4/8/2016 rmd : Initialize our new moisture_sensor_serial_number variable.
				nm_STATION_GROUP_set_moisture_sensor_serial_number( lsgs, DECODER_SERIAL_NUM_MIN, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				// ----------
				
				lsgs = nm_ListGetNext( &station_group_group_list_hdr, lsgs );
			}

			// ----------

			pfrom_revision += 1;
		}

	}

	// ----------

	// 1/29/2015 rmd : Well after the updater has run the pfrom_revision is supposed to equal
	// the latest revision. By design of this update process.
	if( pfrom_revision != LANDSCAPE_DETAILS_LATEST_FILE_REVISION )
	{
		Alert_Message( "LNDSCAPE DTLS updater error" );
	}
}

/* ---------------------------------------------------------- */
extern void init_file_station_group( void )
{
	FLASH_FILE_initialize_list_and_find_or_create_group_list_file(	FLASH_INDEX_1_GENERAL_STORAGE,
																	LANDSCAPE_DETAILS_FILENAME,
																	LANDSCAPE_DETAILS_LATEST_FILE_REVISION,
																	&station_group_group_list_hdr,
																	station_group_list_item_sizes,
																	list_program_data_recursive_MUTEX,
																	&nm_station_group_updater,
																	&nm_STATION_GROUP_set_default_values,
																	(char*)STATION_GROUP_DEFAULT_NAME,
																	FF_LANDSCAPE_DETAILS );
	STATION_GROUP_prepopulate_default_groups();
}

/* ---------------------------------------------------------- */
extern void save_file_station_group( void )
{
	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE,
															LANDSCAPE_DETAILS_FILENAME,
															LANDSCAPE_DETAILS_LATEST_FILE_REVISION,
															&station_group_group_list_hdr,
															sizeof( STATION_GROUP_STRUCT ),
															list_program_data_recursive_MUTEX,
															FF_LANDSCAPE_DETAILS );
}

/* ---------------------------------------------------------- */
extern char *STATION_GROUP_copy_details_into_group_name( char *pgroup_name, const UNS_32 pplant_type, const UNS_32 phead_type, const UNS_32 pexposure )
{
	char	plant_type_str[ 48 ];

	char	head_type_str[ 48 ];

	// ----------

	if( (pplant_type >= STATION_GROUP_PLANT_TYPE_COOL_SEASON_TURF) &&
		(pplant_type <= STATION_GROUP_PLANT_TYPE_COMBINED_TURF) )
	{
		strlcpy( plant_type_str, "Turf", sizeof(plant_type_str) );
	}
	else if( pplant_type == STATION_GROUP_PLANT_TYPE_ANNUALS )
	{
		strlcpy( plant_type_str, "Annuals", sizeof(plant_type_str) );
	}
	else if( pplant_type == STATION_GROUP_PLANT_TYPE_TREES )
	{
		strlcpy( plant_type_str, "Trees", sizeof(plant_type_str) );
	}

	else if( pplant_type == STATION_GROUP_PLANT_TYPE_GROUND_COVER )
	{
		strlcpy( plant_type_str, "Ground Cover", sizeof(plant_type_str) );
	}
	else if( (pplant_type >= STATION_GROUP_PLANT_TYPE_SHRUBS_HIGH) &&
			 (pplant_type <= STATION_GROUP_PLANT_TYPE_SHRUBS_LOW) )
	{
		strlcpy( plant_type_str, "Shrubs", sizeof(plant_type_str) );
	}
	else if( (pplant_type >= STATION_GROUP_PLANT_TYPE_MIXED_HIGH) &&
			 (pplant_type <= STATION_GROUP_PLANT_TYPE_MIXED_LOW) )
	{
		strlcpy( plant_type_str, "Mixed", sizeof(plant_type_str) );
	}
	else if( (pplant_type >= STATION_GROUP_PLANT_TYPE_NATIVE) &&
			 (pplant_type <= STATION_GROUP_PLANT_TYPE_NATIVE_GRASSES) )
	{
		strlcpy( plant_type_str, "Natives", sizeof(plant_type_str) );
	}
	else
	{
		strlcpy( plant_type_str, "UNK PLANT", sizeof(plant_type_str) );
	}

	// ----------

	if( (phead_type >= STATION_GROUP_HEAD_TYPE_SPRAY) &&
		(phead_type <= STATION_GROUP_HEAD_TYPE_SPRAY_HIGH_EFF) )
	{
		strlcpy( head_type_str, "Spray", sizeof(head_type_str) );
	}
	else if ( (phead_type >= STATION_GROUP_HEAD_TYPE_ROTOR_FULL) &&
			  (phead_type <= STATION_GROUP_HEAD_TYPE_ROTOR_STREAM) )
	{
		strlcpy( head_type_str, "Rotor", sizeof(head_type_str) );
	}
	else if ( (phead_type >= STATION_GROUP_HEAD_TYPE_IMPACT_FULL) &&
			  (phead_type <= STATION_GROUP_HEAD_TYPE_IMPACT_MIXED) )
	{
		strlcpy( head_type_str, "Impact", sizeof(head_type_str) );
	}
	else if( phead_type == STATION_GROUP_HEAD_TYPE_BUBBLER )
	{
		strlcpy( head_type_str, "Bubbler", sizeof(head_type_str) );
	}
	else if ( (phead_type >= STATION_GROUP_HEAD_TYPE_DRIP) &&
			  (phead_type <= STATION_GROUP_HEAD_TYPE_DRIP_SUBSURFACE) )
	{
		strlcpy( head_type_str, "Drip", sizeof(head_type_str) );
	}
	else
	{
		strlcpy( plant_type_str, "UNK HEAD", sizeof(plant_type_str) );
	}

	// ----------

	snprintf( pgroup_name, NUMBER_OF_CHARS_IN_A_NAME, "%s %s %s", plant_type_str, head_type_str, GetExposureStr(pexposure) );
	
	return( pgroup_name );
}

/* ---------------------------------------------------------- */
static void STATION_GROUP_prepopulate_default_groups( void )
{
	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// If this is the first time we've initialized the list, we'll only have one
	// item in the list and it's name will be "Station Group 1". If this is
	// the case, rename the first group.
	if( nm_ListGetCount( &station_group_group_list_hdr ) == 1 )
	{
		lgroup = nm_ListGetFirst(&station_group_group_list_hdr);

		if( strncmp( lgroup->base.description, (const char *)STATION_GROUP_DEFAULT_NAME, strlen(STATION_GROUP_DEFAULT_NAME) ) == 0 )
		{
			STATION_GROUP_copy_details_into_group_name( lgroup->base.description, lgroup->plant_type, lgroup->head_type, lgroup->exposure );

			FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_LANDSCAPE_DETAILS, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
		}
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_default_values( void *const pgroup, const BOOL_32 pset_change_bits )
{
	// 2/27/2015 ajv : When functions are declared as static, the compiler may inline the
	// functions and optimize out floating-point constant literals from the calculations. This
	// can result in a division-by-0 error. In order to resolve this, we declare the constant
	// literal as volatile which tells the compile to not optimize the value.
	const volatile float ONE_HUNDRED = 100.0F;

	const volatile float TEN = 10.0F;

	// ----------

	CHANGE_BITS_64_BIT	*lbitfield_of_changes;

	DATE_TIME	ldt;

	float	Kl_100u;

	UNS_32	i;

	UNS_32	box_index_0;

	box_index_0 = FLOWSENSE_get_controller_index();

	// ------------------------

	if( pgroup != NULL )
	{
		// 5/14/2014 ajv : Clear all of the change bits for station group since
		// it's just been created. We'll then explicitly set each bit based upon
		// whether pset_change_bits is true or not.
		SHARED_clear_all_64_bit_change_bits( &((STATION_GROUP_STRUCT *)pgroup)->changes_to_send_to_master, list_program_data_recursive_MUTEX );
		SHARED_clear_all_64_bit_change_bits( &((STATION_GROUP_STRUCT *)pgroup)->changes_to_distribute_to_slaves, list_program_data_recursive_MUTEX );
		SHARED_clear_all_64_bit_change_bits( &((STATION_GROUP_STRUCT *)pgroup)->changes_uploaded_to_comm_server_awaiting_ACK, list_program_data_recursive_MUTEX );

		// 1/20/2015 ajv : However, we should explicitly set ALL bits for the Comm Server to ensure
		// changes are uploaded on a clean start.
		SHARED_set_all_64_bit_change_bits( &((STATION_GROUP_STRUCT *)pgroup)->changes_to_upload_to_comm_server, list_program_data_recursive_MUTEX );

		// ----------

		// 5/2/2014 ajv : When creating a new group, always set the changes to
		// be sent to the master. Once the master receives the changes, it will
		// distribute them back out to the slaves.
		lbitfield_of_changes = &((STATION_GROUP_STRUCT *)pgroup)->changes_to_send_to_master;
 
		// -------------------

		// 8/18/2016 skc : This group has yet to send budget alert at start time
		((STATION_GROUP_STRUCT *)pgroup)->flag_budget_start_time_alert_sent = 0;

		// -------------------

		// 7/7/2015 ajv : Initialize no longer used values to 0 so we know they're value if/when we
		// repurpose them later.
		((STATION_GROUP_STRUCT *)pgroup)->unused_b = 0;
		((STATION_GROUP_STRUCT *)pgroup)->unused_c = 0;

		// ----------

		nm_STATION_GROUP_set_plant_type( pgroup, STATION_GROUP_PLANT_TYPE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_STATION_GROUP_set_head_type( pgroup, STATION_GROUP_HEAD_TYPE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_STATION_GROUP_set_precip_rate( pgroup, (UNS_32)(PR[ STATION_GROUP_HEAD_TYPE_DEFAULT ] * 100000.0F), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_STATION_GROUP_set_soil_type( pgroup, STATION_GROUP_SOIL_TYPE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_STATION_GROUP_set_slope_percentage( pgroup, STATION_GROUP_SLOPE_PERCENTAGE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_STATION_GROUP_set_exposure( pgroup, STATION_GROUP_EXPOSURE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_STATION_GROUP_set_usable_rain( pgroup, STATION_GROUP_USABLE_RAIN_PERCENTAGE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		// ----------

		// 6/23/2015 ajv : Calculate the default values below based on the default paramters above.
		// Allow these to be dynamic in case one of the default parameters changes in the future.
		
		Kl_100u = WATERSENSE_get_Kl( Ks[ ((STATION_GROUP_STRUCT *)pgroup)->plant_type ], Kd[ ((STATION_GROUP_STRUCT *)pgroup)->plant_type ][ STATION_GROUP_DENSITY_FACTOR_AVERAGE ], Kmc[ ((STATION_GROUP_STRUCT *)pgroup)->plant_type ][ WATERSENSE_get_Kmc_index_based_on_exposure( ((STATION_GROUP_STRUCT *)pgroup)->exposure ) ] ) * ONE_HUNDRED;

		for( i = 0; i < MONTHS_IN_A_YEAR; ++i )
		{
			nm_STATION_GROUP_set_crop_coefficient( pgroup, i, Kl_100u, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		}

		nm_STATION_GROUP_set_allowable_depletion( pgroup, (UNS_32)(MAD[ ((STATION_GROUP_STRUCT *)pgroup)->soil_type ] * ONE_HUNDRED), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_GROUP_set_available_water( pgroup, (UNS_32)(AW[ ((STATION_GROUP_STRUCT *)pgroup)->soil_type ] * ONE_HUNDRED), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_GROUP_set_root_zone_depth( pgroup, (UNS_32)(ROOT_ZONE_DEPTH[ ((STATION_GROUP_STRUCT *)pgroup)->plant_type ][ ((STATION_GROUP_STRUCT *)pgroup)->soil_type ] * TEN), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_GROUP_set_soil_storage_capacity( pgroup, (UNS_32)(WATERSENSE_get_RZWWS( (((STATION_GROUP_STRUCT *)pgroup)->allowable_depletion_100u / ONE_HUNDRED), (float)(((STATION_GROUP_STRUCT *)pgroup)->available_water_100u / ONE_HUNDRED), (float)(((STATION_GROUP_STRUCT *)pgroup)->root_zone_depth_10u / TEN) ) * ONE_HUNDRED), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_GROUP_set_species_factor( pgroup, (UNS_32)(Ks[ ((STATION_GROUP_STRUCT *)pgroup)->plant_type ] * ONE_HUNDRED), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_GROUP_set_density_factor( pgroup, (UNS_32)(Kd[ ((STATION_GROUP_STRUCT *)pgroup)->plant_type ][ STATION_GROUP_DENSITY_FACTOR_AVERAGE ] * ONE_HUNDRED), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_GROUP_set_microclimate_factor( pgroup, (UNS_32)(Kmc[ ((STATION_GROUP_STRUCT *)pgroup)->plant_type ][ WATERSENSE_get_Kmc_index_based_on_exposure( ((STATION_GROUP_STRUCT *)pgroup)->exposure ) ] * ONE_HUNDRED), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_GROUP_set_soil_intake_rate( pgroup, (UNS_32)(IR[ ((STATION_GROUP_STRUCT *)pgroup)->soil_type ] * ONE_HUNDRED), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_GROUP_set_allowable_surface_accumulation( pgroup, (UNS_32)(ASA[ ((STATION_GROUP_STRUCT *)pgroup)->soil_type ][ ((STATION_GROUP_STRUCT *)pgroup)->slope_percentage ] * ONE_HUNDRED), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		// -------------------

		nm_PRIORITY_set_priority( pgroup, PRIORITY_LEVEL_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		// -------------------

		EPSON_obtain_latest_time_and_date( &ldt );

		nm_PERCENT_ADJUST_set_percent( pgroup, PERCENT_ADJUST_PERCENT_DEFAULT_100u, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_PERCENT_ADJUST_set_start_date( pgroup, ldt.D, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_PERCENT_ADJUST_set_end_date( pgroup, ldt.D, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		// -------------------

		nm_SCHEDULE_set_enabled( pgroup, SCHEDULE_ENABLED_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_SCHEDULE_set_start_time( pgroup, SCHEDULE_START_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_SCHEDULE_set_stop_time( pgroup, SCHEDULE_STOP_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_SCHEDULE_set_mow_day( pgroup, SCHEDULE_MOWDAY_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_SCHEDULE_set_schedule_type( pgroup, SCHEDULE_TYPE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		for( i = SUNDAY; i < DAYS_IN_A_WEEK; ++i )
		{
			nm_SCHEDULE_set_water_day( pgroup, i, SCHEDULE_WATERDAY_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		}

		nm_SCHEDULE_set_begin_date( pgroup, ldt.D, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_SCHEDULE_set_irrigate_on_29th_or_31st( pgroup, SCHEDULE_IRRIGATEON29THOR31ST_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		// 10/4/2013 ajv : To ensure the last ran is always consistent, set it
		// to the default date and time rather than the current date and time.
		ldt.D = DATE_MINIMUM;
		ldt.T = TIME_DEFAULT;

		nm_SCHEDULE_set_last_ran( pgroup, ldt, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		// -------------------

		nm_DAILY_ET_set_ET_in_use( pgroup, WEATHER_ET_MODE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_DAILY_ET_set_use_ET_averaging( pgroup, WEATHER_ET_AVERAGING_IN_USE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		// -------------------

		nm_RAIN_set_rain_in_use( pgroup, WEATHER_RAIN_IN_USE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		// -------------------

		nm_WIND_set_wind_in_use( pgroup, WEATHER_WIND_IN_USE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		// -------------------

		nm_ALERT_ACTIONS_set_high_flow_action( pgroup, ALERT_ACTIONS_HIGH_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_ALERT_ACTIONS_set_low_flow_action( pgroup, ALERT_ACTIONS_LOW_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		// -------------------

		nm_ON_AT_A_TIME_set_on_at_a_time_in_group( pgroup, ON_AT_A_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_ON_AT_A_TIME_set_on_at_a_time_in_system( pgroup, ON_AT_A_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		// -------------------

		nm_PUMP_set_pump_in_use( pgroup, PUMP_INUSE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		// -------------------

		nm_LINE_FILL_TIME_set_line_fill_time( pgroup, LINE_FILL_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
		nm_DELAY_BETWEEN_VALVES_set_delay_time( pgroup, DELAY_BETWEEN_VALVES_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		// -------------------

		nm_ACQUIRE_EXPECTEDS_set_acquire_expected( pgroup, ACQUIRE_EXPECTEDS_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		// -------------------

		nm_STATION_GROUP_set_budget_reduction_percentage_cap( pgroup, STATION_GROUP_BUDGET_PERCENTAGE_CAP_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		// -------------------

		nm_STATION_GROUP_set_GID_irrigation_system( pgroup, nm_GROUP_get_group_ID( SYSTEM_get_group_at_this_index( 0 ) ), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		nm_STATION_GROUP_set_in_use( pgroup, STATION_GROUP_IN_USE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );

		// ----------
		
		// 3/30/2016 rmd : A decoder serial number of 0 indicates the station groupd does not use a
		// moisture sensor decoder. Even if there is a serial number when we check to use it we see
		// if the decoder is in the list, is physically available, is in_use, and has a RECENT
		// reading.
		nm_STATION_GROUP_set_moisture_sensor_serial_number( pgroup, DECODER_SERIAL_NUM_MIN, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lbitfield_of_changes );
	}
	else
	{
		Alert_func_call_with_null_ptr();
	}
}

/* ---------------------------------------------------------- */
extern void *nm_STATION_GROUP_create_new_group( const UNS_32 pchanges_received_from )
{
	STATION_GROUP_STRUCT	*litem_to_insert_ahead_of;

	STATION_GROUP_STRUCT	*lgroup;

	// ----------

	litem_to_insert_ahead_of = nm_ListGetFirst( &station_group_group_list_hdr );

	// 2/11/2016 ajv : Loop through each manual program until we either find one that's deleted
	// or we reach the end of the list. If we find one that's been deleted, we're going to
	// insert the new item ahead of it.
	while( litem_to_insert_ahead_of != NULL )
	{
		if( !litem_to_insert_ahead_of->in_use )
		{
			// 2/11/2016 ajv : We found a deleted one so break out of the looop so the new item in
			// inserted ahead of it.
			break;
		}

		litem_to_insert_ahead_of = nm_ListGetNext( &station_group_group_list_hdr, litem_to_insert_ahead_of );
	}

	// ----------

	// 5/2/2014 ajv : TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// (requires pchanges_received_from)

	// 2/11/2016 ajv : Note that we're no longer passing a NULL for the pnext_list_item
	// parameter. The reason is because the list is now sorted with deleted items (aka not in
	// use) at the end of the list. We therefore need to insert a new item before the first
	// deleted item, if it exists.
	lgroup = nm_GROUP_create_new_group( &station_group_group_list_hdr, (char*)STATION_GROUP_DEFAULT_NAME, &nm_STATION_GROUP_set_default_values, sizeof(STATION_GROUP_STRUCT), (true), litem_to_insert_ahead_of );

	STATION_GROUP_copy_details_into_group_name( lgroup->base.description, STATION_GROUP_PLANT_TYPE_DEFAULT, STATION_GROUP_HEAD_TYPE_DEFAULT, STATION_GROUP_EXPOSURE_DEFAULT );

	return ( lgroup );
}

/* ---------------------------------------------------------- */
extern void STATION_GROUP_copy_group( const STATION_GROUP_STRUCT *pgroup_to_copy_from, STATION_GROUP_STRUCT *pgroup_to_copy_to )
{
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// 10/31/2013 ajv : Unfortunately, since the station group contains a number
	// of items we DON'T want to copy (e.g., group ID, change bits, presently on
	// group count, last ran, etc.), we have to manually copy the pieces we want
	// rather than doing a straight memcpy.

	// 11/21/2013 ajv : Only append the "Copy" text to the string if it doesn't
	// already have "Copy" in the text.
	if( strstr( pgroup_to_copy_from->base.description, GuiLib_GetTextPtr( GuiStruct_txtCopy_0, 0 ) ) == NULL )
	{
		snprintf( pgroup_to_copy_to->base.description, NUMBER_OF_CHARS_IN_A_NAME, "%s %s", pgroup_to_copy_from->base.description, GuiLib_GetTextPtr( GuiStruct_txtCopy_0, 0 ) );
	}
	else
	{
		strlcpy( pgroup_to_copy_to->base.description, pgroup_to_copy_from->base.description, NUMBER_OF_CHARS_IN_A_NAME );
	}

	pgroup_to_copy_to->plant_type = pgroup_to_copy_from->plant_type;
	pgroup_to_copy_to->head_type = pgroup_to_copy_from->head_type;
	pgroup_to_copy_to->precip_rate_in_100000u = pgroup_to_copy_from->precip_rate_in_100000u;
	pgroup_to_copy_to->soil_type = pgroup_to_copy_from->soil_type;
	pgroup_to_copy_to->slope_percentage = pgroup_to_copy_from->slope_percentage;
	pgroup_to_copy_to->exposure = pgroup_to_copy_from->exposure;
	pgroup_to_copy_to->usable_rain_percentage_100u = pgroup_to_copy_from->usable_rain_percentage_100u;
	memcpy( pgroup_to_copy_to->crop_coefficient_100u, pgroup_to_copy_from->crop_coefficient_100u, sizeof(pgroup_to_copy_to->crop_coefficient_100u) );
	pgroup_to_copy_to->priority_level = pgroup_to_copy_from->priority_level;
	pgroup_to_copy_to->percent_adjust_100u = pgroup_to_copy_from->percent_adjust_100u;
	pgroup_to_copy_to->percent_adjust_start_date = pgroup_to_copy_from->percent_adjust_start_date;
	pgroup_to_copy_to->percent_adjust_end_date = pgroup_to_copy_from->percent_adjust_end_date;
	pgroup_to_copy_to->schedule_enabled_bool = pgroup_to_copy_from->schedule_enabled_bool;
	pgroup_to_copy_to->schedule_type = pgroup_to_copy_from->schedule_type;
	pgroup_to_copy_to->start_time = pgroup_to_copy_from->start_time;
	pgroup_to_copy_to->stop_time = pgroup_to_copy_from->stop_time;
	pgroup_to_copy_to->begin_date = pgroup_to_copy_from->begin_date;
	memcpy( pgroup_to_copy_to->water_days_bool, pgroup_to_copy_from->water_days_bool, sizeof(pgroup_to_copy_to->water_days_bool) );
	pgroup_to_copy_to->irrigate_on_29th_or_31st_bool = pgroup_to_copy_from->irrigate_on_29th_or_31st_bool;
	pgroup_to_copy_to->mow_day = pgroup_to_copy_from->mow_day;
	pgroup_to_copy_to->et_in_use = pgroup_to_copy_from->et_in_use;
	pgroup_to_copy_to->use_et_averaging_bool = pgroup_to_copy_from->use_et_averaging_bool;
	pgroup_to_copy_to->rain_in_use_bool = pgroup_to_copy_from->rain_in_use_bool;
	pgroup_to_copy_to->soil_storage_capacity_inches_100u = pgroup_to_copy_from->soil_storage_capacity_inches_100u;
	pgroup_to_copy_to->wind_in_use_bool = pgroup_to_copy_from->wind_in_use_bool;
	pgroup_to_copy_to->high_flow_action = pgroup_to_copy_from->high_flow_action;
	pgroup_to_copy_to->low_flow_action = pgroup_to_copy_from->low_flow_action;
	pgroup_to_copy_to->on_at_a_time_in_group = pgroup_to_copy_from->on_at_a_time_in_group;
	pgroup_to_copy_to->on_at_a_time_in_system = pgroup_to_copy_from->on_at_a_time_in_system;
	pgroup_to_copy_to->pump_in_use = pgroup_to_copy_from->pump_in_use;
	pgroup_to_copy_to->line_fill_time_sec = pgroup_to_copy_from->line_fill_time_sec;
	pgroup_to_copy_to->delay_between_valve_time_sec = pgroup_to_copy_from->delay_between_valve_time_sec;
	pgroup_to_copy_to->acquire_expecteds = pgroup_to_copy_from->acquire_expecteds;
	pgroup_to_copy_to->allowable_depletion_100u = pgroup_to_copy_from->allowable_depletion_100u;
	pgroup_to_copy_to->available_water_100u = pgroup_to_copy_from->available_water_100u;
	pgroup_to_copy_to->root_zone_depth_10u = pgroup_to_copy_from->root_zone_depth_10u;
	pgroup_to_copy_to->species_factor_100u = pgroup_to_copy_from->species_factor_100u;
	pgroup_to_copy_to->density_factor_100u = pgroup_to_copy_from->density_factor_100u;
	pgroup_to_copy_to->microclimate_factor_100u = pgroup_to_copy_from->microclimate_factor_100u;
	pgroup_to_copy_to->soil_intake_rate_100u = pgroup_to_copy_from->soil_intake_rate_100u;
	pgroup_to_copy_to->allowable_surface_accumulation_100u = pgroup_to_copy_from->allowable_surface_accumulation_100u;
	
	pgroup_to_copy_to->budget_reduction_percentage_cap = pgroup_to_copy_from->budget_reduction_percentage_cap;

	pgroup_to_copy_to->GID_irrigation_system = pgroup_to_copy_from->GID_irrigation_system;

	pgroup_to_copy_to->in_use = (true);
	
	pgroup_to_copy_to->moisture_sensor_serial_number = pgroup_to_copy_from->moisture_sensor_serial_number;

	// 9/26/2016 skc : FogBugz #61, Apparently the changebits are not fully set when making a
	// copy of a station group. Thus we hack here and set all of them.
	SHARED_set_all_64_bit_change_bits( &pgroup_to_copy_to->changes_to_send_to_master, list_program_data_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * This function runs through the station group list and copies the contents of any changed
 * groups to the passed in ucp.
 * 
 * @executed Executed within the context of the CommMngr task as a token, token response, or
 * Program Data message to the Comm Server is being built.
 * 
 * @param pucp A pointer to the pointer where the program data is being built.
 * 
 * @param pmem_used_so_far The amount of memory used so far. Once this reaches our limit for
 * a single token (MEMORY_TO_ALLOCATE_FOR_SENDING_CHANGES), processing of the remaining
 * changes stops until it's time to build the next token or token response.
 * 
 * @param pmem_used_so_far_is_less_than_allocated_memory A pointer to the flag which
 * indicates whether we're allowed to add more changes to the token or token response.
 * 
 * @param preason_data_is_being_built The reason why the data is being built to send. Based
 * upon this reason, we examine a different change bit structure to determine what to
 * send. Valid values are CHANGE_REASON_SYNC_SENDING_TO_MASTER,
 * CHANGE_REASON_SYNC_DISTRIBUTING_TO_SLAVES, CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER.
 * 
 * @return UNS_32 The total size of the added data. This is used to determine whether any
 * changes were detected so the appropriate file change bit can be set as well as to
 * determine how much memory must be allocated to store the changes.
 *
 * @author Adrianusv
 */
extern UNS_32 STATION_GROUP_build_data_to_send( UNS_8 **pucp,
												const UNS_32 pmem_used_so_far,
												BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
												const UNS_32 preason_data_is_being_built,
												const UNS_32 pallocated_memory )
{
	CHANGE_BITS_64_BIT	*lbitfield_of_changes_to_use_to_determine_what_to_send;

	CHANGE_BITS_64_BIT	lbitfield_of_changes_to_send;

	STATION_GROUP_STRUCT	*lgroup;

	UNS_8	*llocation_of_num_changed_groups;

	UNS_8	*llocation_of_bitfield;

	// 1/14/2015 ajv : Track how many station groups are added to the message.
	UNS_32	lnum_changed_groups;

	// Store the amount of memory used for current station group. This is compared to the
	// group's overhead to determine whether any of the group's values were actually added or
	// not.
	UNS_32	lmem_used_by_this_group;

	// Determine how much overhead each station group requires in case we add the overhead and
	// don't have enough room to add of the actual changes.
	UNS_32	lmem_overhead_per_group;

	UNS_32	rv;

	// ----------

	rv = 0;

	lnum_changed_groups = 0;
	
	// ----------

	// 1/14/2015 ajv : Take the appropriate mutex to ensure no station groups are added or
	// removed during this process.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lgroup = nm_ListGetFirst( &station_group_group_list_hdr );

	// 1/14/2015 ajv : Loop through each station group and count how many groups have change
	// bits set. This is really just used to determine whether there's at least one group which
	// needs to be examined.
	while( lgroup != NULL )
	{
		if( *(STATION_GROUP_get_change_bits_ptr( lgroup, preason_data_is_being_built )) != CHANGE_BITS_64_BIT_ALL_CLEAR )
		{
			lnum_changed_groups++;
			// 
			// 1/14/2015 ajv : Since we don't actually care about the number of station groups that have
			// changed at this point - just that at least one has changed - we can break out of the loop
			// now.
			break;
		}

		lgroup = nm_ListGetNext( &station_group_group_list_hdr, lgroup );
	}

	// ----------

	if( lnum_changed_groups > 0)
	{
		// 1/14/2015 ajv : Initialize the changed station groups back to 0. Since not all of the
		// groups may be added due to token size limitations, we need to ensure that the receiving
		// party only processes the number of POCs actually added to the message.
		lnum_changed_groups = 0;

		// ----------

		// 1/14/2015 ajv : Allocate space to store the number of changed station groups that are
		// included in the message so the receiving party can correctly parse the data. This will be
		// filled in later once we have an actual count.
		rv = PDATA_allocate_space_for_num_changed_groups_in_pucp( pucp, &llocation_of_num_changed_groups, pmem_used_so_far, pmem_used_so_far_is_less_than_allocated_memory, pallocated_memory );

		// ----------

		// 2/9/2016 rmd : Well there was at least enough space for the 2 bytes for the count of
		// number of changed groups.
		if( *pmem_used_so_far_is_less_than_allocated_memory )
		{
			lgroup = nm_ListGetFirst( &station_group_group_list_hdr ); 

			while( lgroup != NULL )
			{
				lbitfield_of_changes_to_use_to_determine_what_to_send = STATION_GROUP_get_change_bits_ptr( lgroup, preason_data_is_being_built );

				// 1/14/2015 ajv : If this station group has changed...
				if( *lbitfield_of_changes_to_use_to_determine_what_to_send != CHANGE_BITS_64_BIT_ALL_CLEAR )
				{
					// 2/9/2016 rmd : The OVERHEAD is the GID, an UNS_32 indicating the size of the bitfield,
					// and the 64-bit change bitfield itself.
					lmem_overhead_per_group = ( sizeof(UNS_32) + sizeof(UNS_32) + sizeof(UNS_64) );

					// 1/14/2015 ajv : If we still have room available in the message for the overhead, add it
					// now.
					if( *pmem_used_so_far_is_less_than_allocated_memory && ((pmem_used_so_far + rv + lmem_overhead_per_group) < pallocated_memory) )
					{
						PDATA_copy_var_into_pucp( pucp, &lgroup->base.group_identity_number, sizeof(lgroup->base.group_identity_number) );

						PDATA_copy_bitfield_info_into_pucp( pucp, sizeof(CHANGE_BITS_64_BIT), sizeof(UNS_32), &llocation_of_bitfield ); 
					}
					else
					{
						// 9/19/2013 ajv : Stop processing if we've already filled out the token with the maximum
						// amount of data to send.
						*pmem_used_so_far_is_less_than_allocated_memory = (false);

						// 2/11/2016 rmd : No overhead added so leave pucp alone.
						
						break;
					}

					lmem_used_by_this_group = lmem_overhead_per_group;

					// ----------

					// Initialize the bit field
					lbitfield_of_changes_to_send = 0x00;

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_description_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->base.description,
							NUMBER_OF_CHARS_IN_A_NAME,
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_plant_type_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->plant_type,
							sizeof(lgroup->plant_type),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_head_type_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->head_type,
							sizeof(lgroup->head_type),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_precip_rate_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->precip_rate_in_100000u,
							sizeof(lgroup->precip_rate_in_100000u),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_soil_type_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->soil_type,
							sizeof(lgroup->soil_type),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_slope_percentage_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->slope_percentage,
							sizeof(lgroup->slope_percentage),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_exposure_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->exposure,
							sizeof(lgroup->exposure),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_usable_rain_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->usable_rain_percentage_100u,
							sizeof(lgroup->usable_rain_percentage_100u),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_crop_coefficient_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->crop_coefficient_100u,
							sizeof(lgroup->crop_coefficient_100u),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_priority_level_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->priority_level,
							sizeof(lgroup->priority_level),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_percent_adjust_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->percent_adjust_100u,
							sizeof(lgroup->percent_adjust_100u),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_percent_adjust_start_date_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->percent_adjust_start_date,
							sizeof(lgroup->percent_adjust_start_date),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_percent_adjust_end_date_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->percent_adjust_end_date,
							sizeof(lgroup->percent_adjust_end_date),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_schedule_enabled_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->schedule_enabled_bool,
							sizeof(lgroup->schedule_enabled_bool),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_schedule_type_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->schedule_type,
							sizeof(lgroup->schedule_type),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_start_time_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->start_time,
							sizeof(lgroup->start_time),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_stop_time_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->stop_time,
							sizeof(lgroup->stop_time),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_begin_date_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->begin_date,
							sizeof(lgroup->begin_date),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_water_days_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->water_days_bool,
							sizeof(lgroup->water_days_bool),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_irrigate_on_29th_or_31st_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->irrigate_on_29th_or_31st_bool,
							sizeof(lgroup->irrigate_on_29th_or_31st_bool),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_mow_day_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->mow_day,
							sizeof(lgroup->mow_day),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_last_ran_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->last_ran_dt,
							sizeof(lgroup->last_ran_dt),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_et_in_use,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->et_in_use,
							sizeof(lgroup->et_in_use),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_use_et_averaging,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->use_et_averaging_bool,
							sizeof(lgroup->use_et_averaging_bool),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_rain_in_use_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->rain_in_use_bool,
							sizeof(lgroup->rain_in_use_bool),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_soil_storage_capacity_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->soil_storage_capacity_inches_100u,
							sizeof(lgroup->soil_storage_capacity_inches_100u),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_wind_in_use_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->wind_in_use_bool,
							sizeof(lgroup->wind_in_use_bool),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_high_flow_action_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->high_flow_action,
							sizeof(lgroup->high_flow_action),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_low_flow_action_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->low_flow_action,
							sizeof(lgroup->low_flow_action),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_on_at_a_time_in_group_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->on_at_a_time_in_group,
							sizeof(lgroup->on_at_a_time_in_group),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_on_at_a_time_in_system_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->on_at_a_time_in_system,
							sizeof(lgroup->on_at_a_time_in_system),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_pump_in_use_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->pump_in_use,
							sizeof(lgroup->pump_in_use),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_line_fill_time_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->line_fill_time_sec,
							sizeof(lgroup->line_fill_time_sec),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_delay_between_valves_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->delay_between_valve_time_sec,
							sizeof(lgroup->delay_between_valve_time_sec),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_acquire_at_start_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->acquire_expecteds,
							sizeof(lgroup->acquire_expecteds),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_allowable_depletion_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->allowable_depletion_100u,
							sizeof(lgroup->allowable_depletion_100u),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_available_water_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->available_water_100u,
							sizeof(lgroup->available_water_100u),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_root_zone_depth_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->root_zone_depth_10u,
							sizeof(lgroup->root_zone_depth_10u),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_species_factor_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->species_factor_100u,
							sizeof(lgroup->species_factor_100u),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_density_factor_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->density_factor_100u,
							sizeof(lgroup->density_factor_100u),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_microclimate_factor_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->microclimate_factor_100u,
							sizeof(lgroup->microclimate_factor_100u),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_soil_intake_rate_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->soil_intake_rate_100u,
							sizeof(lgroup->soil_intake_rate_100u),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_allowable_surface_accum_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->allowable_surface_accumulation_100u,
							sizeof(lgroup->allowable_surface_accumulation_100u),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_budget_reduction_cap_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->budget_reduction_percentage_cap,
							sizeof(lgroup->budget_reduction_percentage_cap),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_GID_irrigation_system_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->GID_irrigation_system,
							sizeof(lgroup->GID_irrigation_system),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_in_use_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->in_use,
							sizeof(lgroup->in_use),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_64_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							STATION_GROUP_CHANGE_BITFIELD_moisture_sensor_ser_num_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->moisture_sensor_serial_number,
							sizeof(lgroup->moisture_sensor_serial_number),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					// ----------

					// 2/10/2016 rmd : If we saw any changed bits and ACTUALLY ADDED CONTENT BEYOND THE ORIGINAL
					// OVERHEAD then complete out the message by placing the bitfield in the overhead and
					// updating function housekeeping.
					if( lmem_used_by_this_group > lmem_overhead_per_group )
					{
						// 2/10/2016 rmd : Bump the count of changed list items in the message. Place the bitfield
						// of changes into the overhead. And update the return value.
						lnum_changed_groups += 1;
						
						memcpy( llocation_of_bitfield, &lbitfield_of_changes_to_send, sizeof(lbitfield_of_changes_to_send) );
						
						rv += lmem_used_by_this_group;
					}
					else
					{
						// 2/10/2016 rmd : Else back out from the message the overhead already added.
						*pucp -= lmem_overhead_per_group;
					}
					
				}

				lgroup = nm_ListGetNext( &station_group_group_list_hdr, lgroup );
			}

			// ----------

			// 1/14/2015 ajv : If we've added the number of changes but didn't actually add any data
			// because we hit our limit, back out the number of changes.
			if( lnum_changed_groups > 0 )
			{
				memcpy( llocation_of_num_changed_groups, &lnum_changed_groups, sizeof(lnum_changed_groups) );
			}
			else
			{
				// 2/10/2016 rmd : If we didn't add any content beyond moving the msg pointer over where the
				// number of changed groups goes, then put the pointer back and return a 0 to indicate
				// nothing added.
				*pucp -= sizeof( lnum_changed_groups );

				rv = 0;
			}
		}
		else
		{
			// 2/11/2016 rmd : We couldn't do anything, meaning couldn't even add in the
			// num_of_changed_sensors. In that case RV is still 0 and pucp hasn't moved so nothing to
			// do here.
		}
	}

	// ----------

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------

	return ( rv );
}

/* ---------------------------------------------------------- */
extern void STATION_GROUP_copy_group_into_guivars( const UNS_32 pgroup_index_0 )
{
	STATION_GROUP_STRUCT			*lgroup;

	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	// ----------
	
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup = STATION_GROUP_get_group_at_this_index( pgroup_index_0 );

	if( lgroup != NULL )
	{
		nm_GROUP_load_common_guivars( lgroup );

		GuiVar_StationGroupPlantType = lgroup->plant_type;

		GuiVar_StationGroupHeadType = lgroup->head_type;
		GuiVar_StationGroupPrecipRate = (float)(lgroup->precip_rate_in_100000u / 100000.0);

		GuiVar_StationGroupExposure = lgroup->exposure;

		GuiVar_StationGroupUsableRain = lgroup->usable_rain_percentage_100u;

		GuiVar_StationGroupSoilStorageCapacity = (float)(lgroup->soil_storage_capacity_inches_100u / 100.0);

		GuiVar_StationGroupSoilType = lgroup->soil_type;

		GuiVar_StationGroupSlopePercentage = lgroup->slope_percentage;

		GuiVar_StationGroupKc1  = (float)(lgroup->crop_coefficient_100u[ 0 ] / 100.0);
		GuiVar_StationGroupKc2  = (float)(lgroup->crop_coefficient_100u[ 1 ] / 100.0);
		GuiVar_StationGroupKc3  = (float)(lgroup->crop_coefficient_100u[ 2 ] / 100.0);
		GuiVar_StationGroupKc4  = (float)(lgroup->crop_coefficient_100u[ 3 ] / 100.0);
		GuiVar_StationGroupKc5  = (float)(lgroup->crop_coefficient_100u[ 4 ] / 100.0);
		GuiVar_StationGroupKc6  = (float)(lgroup->crop_coefficient_100u[ 5 ] / 100.0);
		GuiVar_StationGroupKc7  = (float)(lgroup->crop_coefficient_100u[ 6 ] / 100.0);
		GuiVar_StationGroupKc8  = (float)(lgroup->crop_coefficient_100u[ 7 ] / 100.0);
		GuiVar_StationGroupKc9  = (float)(lgroup->crop_coefficient_100u[ 8 ] / 100.0);
		GuiVar_StationGroupKc10 = (float)(lgroup->crop_coefficient_100u[ 9 ] / 100.0);
		GuiVar_StationGroupKc11 = (float)(lgroup->crop_coefficient_100u[ 10 ] / 100.0);
		GuiVar_StationGroupKc12 = (float)(lgroup->crop_coefficient_100u[ 11 ] / 100.0);

		GuiVar_StationGroupKcsAreCustom = !( (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc2) &&
										     (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc3) && 
											 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc4) && 
											 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc5) && 
											 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc6) && 
											 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc7) && 
											 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc8) && 
											 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc9) && 
											 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc10) && 
											 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc11) && 
											 (GuiVar_StationGroupKc1 == GuiVar_StationGroupKc12) );

		GuiVar_StationGroupAllowableDepletion = lgroup->allowable_depletion_100u;

		GuiVar_StationGroupAvailableWater = (float)(lgroup->available_water_100u / 100.0);

		GuiVar_StationGroupRootZoneDepth = (float)(lgroup->root_zone_depth_10u / 10.0);

		GuiVar_StationGroupSpeciesFactor = (float)(lgroup->species_factor_100u / 100.0);

		GuiVar_StationGroupDensityFactor = (float)(lgroup->density_factor_100u / 100.0);

		GuiVar_StationGroupMicroclimateFactor = (float)(lgroup->microclimate_factor_100u / 100.0);

		GuiVar_StationGroupSoilIntakeRate = (float)(lgroup->soil_intake_rate_100u / 100.0);

		GuiVar_StationGroupAllowableSurfaceAccum = (float)(lgroup->allowable_surface_accumulation_100u / 100.0);

		GuiVar_StationGroupInUse = lgroup->in_use;

		// ----------

		GuiVar_StationGroupSystemGID = lgroup->GID_irrigation_system;

		if( GuiVar_StationGroupSystemGID > 0 )
		{
			lsystem = SYSTEM_get_group_with_this_GID( lgroup->GID_irrigation_system );

			strlcpy( GuiVar_SystemName, nm_GROUP_get_name( lsystem ), sizeof(GuiVar_SystemName) );
		}

		// ----------

		// 3/30/2016 rmd : Maybe this gui var should be renamed to match (we switched from GID to
		// serial number).
		GuiVar_StationGroupMoistureSensorDecoderSN = lgroup->moisture_sensor_serial_number;

		if( GuiVar_StationGroupMoistureSensorDecoderSN > 0 )
		{
			// 3/30/2016 rmd : We technically need this for the GROUP_get_name function call.
			xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

			lmois = MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number( lgroup->moisture_sensor_serial_number );

			strlcpy( GuiVar_MoisSensorName, nm_GROUP_get_name( lmois ), sizeof(GuiVar_MoisSensorName) );

			xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
		}
		else
		{
			// 4/4/2016 ajv : If the serial number is 0, it means we don't have a sensor assigned to
			// this group yet.
			strlcpy( GuiVar_MoisSensorName, GuiLib_GetTextLanguagePtr(GuiStruct_txtNone_0, 0, GuiLib_LanguageIndex), NUMBER_OF_CHARS_IN_A_NAME );
		}

		// ----------

		STATION_SELECTION_GRID_populate_cursors( &STATION_get_GID_station_group, NULL, g_GROUP_ID, (true), (true), (true) );

		GuiVar_StationSelectionGridStationCount = STATION_SELECTION_GRID_get_count_of_selected_stations();

	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void PRIORITY_fill_guivars( const UNS_32 pgroup_index_0, char *pgroup_name, UNS_32 *ppriority_level, UNS_32 *pvisible )
{
	STATION_GROUP_STRUCT *lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( nm_ListGetCount( &station_group_group_list_hdr ) > pgroup_index_0 )
	{
		lgroup = STATION_GROUP_get_group_at_this_index( pgroup_index_0 );

		// 9/2/2016 skc : We added the ability to delete station groups, check the in_use flag.
		if( lgroup->in_use )
		{
			strlcpy( pgroup_name, nm_GROUP_get_name( lgroup ), sizeof(GuiVar_GroupName_0) );

			*ppriority_level = lgroup->priority_level;

			*pvisible = (true);
		}
		else
		{
			*pvisible = (false);
		}
	}
	else
	{
		*pvisible = (false);
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void PRIORITY_copy_group_into_guivars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	PRIORITY_fill_guivars( lindex_0++, GuiVar_GroupName_0, &GuiVar_GroupSettingA_0, &GuiVar_GroupSetting_Visible_0 );
	PRIORITY_fill_guivars( lindex_0++, GuiVar_GroupName_1, &GuiVar_GroupSettingA_1, &GuiVar_GroupSetting_Visible_1 );
	PRIORITY_fill_guivars( lindex_0++, GuiVar_GroupName_2, &GuiVar_GroupSettingA_2, &GuiVar_GroupSetting_Visible_2 );
	PRIORITY_fill_guivars( lindex_0++, GuiVar_GroupName_3, &GuiVar_GroupSettingA_3, &GuiVar_GroupSetting_Visible_3 );
	PRIORITY_fill_guivars( lindex_0++, GuiVar_GroupName_4, &GuiVar_GroupSettingA_4, &GuiVar_GroupSetting_Visible_4 );
	PRIORITY_fill_guivars( lindex_0++, GuiVar_GroupName_5, &GuiVar_GroupSettingA_5, &GuiVar_GroupSetting_Visible_5 );
	PRIORITY_fill_guivars( lindex_0++, GuiVar_GroupName_6, &GuiVar_GroupSettingA_6, &GuiVar_GroupSetting_Visible_6 );
	PRIORITY_fill_guivars( lindex_0++, GuiVar_GroupName_7, &GuiVar_GroupSettingA_7, &GuiVar_GroupSetting_Visible_7 );
	PRIORITY_fill_guivars( lindex_0++, GuiVar_GroupName_8, &GuiVar_GroupSettingA_8, &GuiVar_GroupSetting_Visible_8 );
	PRIORITY_fill_guivars( lindex_0++, GuiVar_GroupName_9, &GuiVar_GroupSettingA_9, &GuiVar_GroupSetting_Visible_9 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void PERCENT_ADJUST_fill_guivars( const UNS_32 pgroup_index_0, char *pgroup_name, INT_32 *ppercentage, UNS_32 *pdays, UNS_32 *pshow_days, char *pend_date_str, BOOL_32 *pvisible )
{
	STATION_GROUP_STRUCT	*lgroup;

	DATE_TIME	ldt;

	char	str_48[ 48 ];

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( nm_ListGetCount( &station_group_group_list_hdr ) > pgroup_index_0 )
	{
		lgroup = STATION_GROUP_get_group_at_this_index( pgroup_index_0 );

		// 9/2/2016 skc : We added the ability to delete station groups, check the in_use flag.
		if( lgroup->in_use )
		{
			strlcpy( pgroup_name, nm_GROUP_get_name( lgroup ), sizeof(GuiVar_GroupName_0) );

			// A value of 1.0 means no change. Therefore, subtract 1.0 to display 0.
			*ppercentage = (INT_32)(lgroup->percent_adjust_100u - 100);

			// 10/21/2013 ajv : TODO Hide days if percentage is 0
			if( *ppercentage == 0 )
			{
				*pshow_days = (false);
			}
			else
			{
				*pshow_days = (true);
			}

			EPSON_obtain_latest_time_and_date( &ldt );

			if( (ldt.D >= lgroup->percent_adjust_start_date) && (ldt.D <= lgroup->percent_adjust_end_date) )
			{
				*pdays = (lgroup->percent_adjust_end_date - ldt.D);
			}
			else
			{
				*pdays = 0;
			}

			strlcpy( pend_date_str, GetDateStr(str_48, sizeof(str_48), (ldt.D + *pdays), DATESTR_show_short_year, DATESTR_show_short_dow), sizeof(GuiVar_PercentAdjustEndDate_0) );

			*pvisible = (true);
		}
		else
		{
			*pvisible = (false);
		}
	}
	else
	{
		*pvisible = (false);
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void PERCENT_ADJUST_copy_group_into_guivars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	PERCENT_ADJUST_fill_guivars( lindex_0++, GuiVar_GroupName_0, &GuiVar_PercentAdjustPercent_0, &GuiVar_GroupSettingB_0, &GuiVar_GroupSettingC_0, (char*)&GuiVar_PercentAdjustEndDate_0, &GuiVar_GroupSetting_Visible_0 );
	PERCENT_ADJUST_fill_guivars( lindex_0++, GuiVar_GroupName_1, &GuiVar_PercentAdjustPercent_1, &GuiVar_GroupSettingB_1, &GuiVar_GroupSettingC_1, (char*)&GuiVar_PercentAdjustEndDate_1, &GuiVar_GroupSetting_Visible_1 );
	PERCENT_ADJUST_fill_guivars( lindex_0++, GuiVar_GroupName_2, &GuiVar_PercentAdjustPercent_2, &GuiVar_GroupSettingB_2, &GuiVar_GroupSettingC_2, (char*)&GuiVar_PercentAdjustEndDate_2, &GuiVar_GroupSetting_Visible_2 );
	PERCENT_ADJUST_fill_guivars( lindex_0++, GuiVar_GroupName_3, &GuiVar_PercentAdjustPercent_3, &GuiVar_GroupSettingB_3, &GuiVar_GroupSettingC_3, (char*)&GuiVar_PercentAdjustEndDate_3, &GuiVar_GroupSetting_Visible_3 );
	PERCENT_ADJUST_fill_guivars( lindex_0++, GuiVar_GroupName_4, &GuiVar_PercentAdjustPercent_4, &GuiVar_GroupSettingB_4, &GuiVar_GroupSettingC_4, (char*)&GuiVar_PercentAdjustEndDate_4, &GuiVar_GroupSetting_Visible_4 );
	PERCENT_ADJUST_fill_guivars( lindex_0++, GuiVar_GroupName_5, &GuiVar_PercentAdjustPercent_5, &GuiVar_GroupSettingB_5, &GuiVar_GroupSettingC_5, (char*)&GuiVar_PercentAdjustEndDate_5, &GuiVar_GroupSetting_Visible_5 );
	PERCENT_ADJUST_fill_guivars( lindex_0++, GuiVar_GroupName_6, &GuiVar_PercentAdjustPercent_6, &GuiVar_GroupSettingB_6, &GuiVar_GroupSettingC_6, (char*)&GuiVar_PercentAdjustEndDate_6, &GuiVar_GroupSetting_Visible_6 );
	PERCENT_ADJUST_fill_guivars( lindex_0++, GuiVar_GroupName_7, &GuiVar_PercentAdjustPercent_7, &GuiVar_GroupSettingB_7, &GuiVar_GroupSettingC_7, (char*)&GuiVar_PercentAdjustEndDate_7, &GuiVar_GroupSetting_Visible_7 );
	PERCENT_ADJUST_fill_guivars( lindex_0++, GuiVar_GroupName_8, &GuiVar_PercentAdjustPercent_8, &GuiVar_GroupSettingB_8, &GuiVar_GroupSettingC_8, (char*)&GuiVar_PercentAdjustEndDate_8, &GuiVar_GroupSetting_Visible_8 );
	PERCENT_ADJUST_fill_guivars( lindex_0++, GuiVar_GroupName_9, &GuiVar_PercentAdjustPercent_9, &GuiVar_GroupSettingB_9, &GuiVar_GroupSettingC_9, (char*)&GuiVar_PercentAdjustEndDate_9, &GuiVar_GroupSetting_Visible_9 );
}

/* ---------------------------------------------------------- */
extern void SCHEDULE_copy_group_into_guivars( const UNS_32 pgroup_index_0 )
{
	STATION_GROUP_STRUCT *lgroup;
	
	STATION_STRUCT	*lstation;

	DATE_TIME	ldt;

	char	str_48[ 48 ];

	GuiVar_ScheduleNoStationsInGroup = (true);

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup = STATION_GROUP_get_group_at_this_index( pgroup_index_0 );

	if( lgroup != NULL )
	{
		nm_GROUP_load_common_guivars( lgroup );

		// Since easyGUI expects the time in minutes, divide the start and
		// stop times by 60.
		GuiVar_ScheduleStartTime = (UNS_32)( lgroup->start_time / 60 );
		GuiVar_ScheduleStopTime = (UNS_32)( lgroup->stop_time / 60 );

		GuiVar_ScheduleStartTimeEnabled = lgroup->schedule_enabled_bool;
		GuiVar_ScheduleStopTimeEnabled = ( GuiVar_ScheduleStopTime < (UNS_32)(SCHEDULE_OFF_TIME / 60) );

		GuiVar_ScheduleType = lgroup->schedule_type;

		GuiVar_ScheduleMowDay = lgroup->mow_day;

		GuiVar_ScheduleWaterDay_Sun = lgroup->water_days_bool[ 0 ];
		GuiVar_ScheduleWaterDay_Mon = lgroup->water_days_bool[ 1 ];
		GuiVar_ScheduleWaterDay_Tue = lgroup->water_days_bool[ 2 ];
		GuiVar_ScheduleWaterDay_Wed = lgroup->water_days_bool[ 3 ];
		GuiVar_ScheduleWaterDay_Thu = lgroup->water_days_bool[ 4 ];
		GuiVar_ScheduleWaterDay_Fri = lgroup->water_days_bool[ 5 ];
		GuiVar_ScheduleWaterDay_Sat = lgroup->water_days_bool[ 6 ];

		GuiVar_ScheduleIrrigateOn29thOr31st = lgroup->irrigate_on_29th_or_31st_bool;

		// 11/14/2013 ajv : Make sure the begin date is valid (i.e., today or
		// later). If not, set it to today's date.
		EPSON_obtain_latest_time_and_date( &ldt );

		if( lgroup->begin_date < ldt.D )
		{
			GuiVar_ScheduleBeginDate = ldt.D;
		}
		else
		{
			GuiVar_ScheduleBeginDate = lgroup->begin_date;
		}

		strlcpy( GuiVar_ScheduleBeginDateStr, GetDateStr(str_48, sizeof(str_48), GuiVar_ScheduleBeginDate, DATESTR_show_long_year, DATESTR_show_long_dow), sizeof(GuiVar_ScheduleBeginDateStr) );

		// 4/9/2016 ajv : Only display the ET Averaging setting if Daily ET is in use for this
		// Station Group.
		GuiVar_ScheduleUsesET = lgroup->et_in_use;

		GuiVar_ScheduleUseETAveraging = lgroup->use_et_averaging_bool;

		// ----------

		// 5/13/2015 ajv : Verify that at least one station is assigned to the group. If not,
		// display a warning to the user that the schedule will not run because of that.
		lstation = nm_ListGetFirst( &station_info_list_hdr );

		while( lstation != NULL )
		{
			if( (STATION_station_is_available_for_use(lstation) == (true)) && (STATION_get_GID_station_group(lstation) == g_GROUP_ID) )
			{
				GuiVar_ScheduleNoStationsInGroup = (false);

				// 5/13/2015 ajv : Since we've identified at least one station in this group, we can break
				// out of the loop.
				break;
			}

			lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
		}
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void WEATHER_fill_guivars( const UNS_32 pgroup_index_0, char *pgroup_name, UNS_32 *pet_in_use, UNS_32 *prain_in_use, UNS_32 *pwind_in_use, UNS_32 *pvisible )
{
	STATION_GROUP_STRUCT *lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( nm_ListGetCount( &station_group_group_list_hdr ) > pgroup_index_0 )
	{
		lgroup = STATION_GROUP_get_group_at_this_index( pgroup_index_0 );

		// 9/20/2016 skc : We added the ability to delete station groups, check the in_use flag.
		if( lgroup->in_use)
		{

			strlcpy( pgroup_name, nm_GROUP_get_name( lgroup ), sizeof(GuiVar_GroupName_0) );

			*pet_in_use = lgroup->et_in_use;

			*prain_in_use = lgroup->rain_in_use_bool;

			*pwind_in_use = lgroup->wind_in_use_bool;

			*pvisible = (true);
		}
		else
		{
			*pvisible = (false);
		}
	}
	else
	{
		*pvisible = (false);
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void WEATHER_copy_group_into_guivars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	WEATHER_fill_guivars( lindex_0++, GuiVar_GroupName_0, &GuiVar_GroupSettingA_0, &GuiVar_GroupSettingB_0, &GuiVar_GroupSettingC_0, &GuiVar_GroupSetting_Visible_0 );
	WEATHER_fill_guivars( lindex_0++, GuiVar_GroupName_1, &GuiVar_GroupSettingA_1, &GuiVar_GroupSettingB_1, &GuiVar_GroupSettingC_1, &GuiVar_GroupSetting_Visible_1 );
	WEATHER_fill_guivars( lindex_0++, GuiVar_GroupName_2, &GuiVar_GroupSettingA_2, &GuiVar_GroupSettingB_2, &GuiVar_GroupSettingC_2, &GuiVar_GroupSetting_Visible_2 );
	WEATHER_fill_guivars( lindex_0++, GuiVar_GroupName_3, &GuiVar_GroupSettingA_3, &GuiVar_GroupSettingB_3, &GuiVar_GroupSettingC_3, &GuiVar_GroupSetting_Visible_3 );
	WEATHER_fill_guivars( lindex_0++, GuiVar_GroupName_4, &GuiVar_GroupSettingA_4, &GuiVar_GroupSettingB_4, &GuiVar_GroupSettingC_4, &GuiVar_GroupSetting_Visible_4 );
	WEATHER_fill_guivars( lindex_0++, GuiVar_GroupName_5, &GuiVar_GroupSettingA_5, &GuiVar_GroupSettingB_5, &GuiVar_GroupSettingC_5, &GuiVar_GroupSetting_Visible_5 );
	WEATHER_fill_guivars( lindex_0++, GuiVar_GroupName_6, &GuiVar_GroupSettingA_6, &GuiVar_GroupSettingB_6, &GuiVar_GroupSettingC_6, &GuiVar_GroupSetting_Visible_6 );
	WEATHER_fill_guivars( lindex_0++, GuiVar_GroupName_7, &GuiVar_GroupSettingA_7, &GuiVar_GroupSettingB_7, &GuiVar_GroupSettingC_7, &GuiVar_GroupSetting_Visible_7 );
	WEATHER_fill_guivars( lindex_0++, GuiVar_GroupName_8, &GuiVar_GroupSettingA_8, &GuiVar_GroupSettingB_8, &GuiVar_GroupSettingC_8, &GuiVar_GroupSetting_Visible_8 );
	WEATHER_fill_guivars( lindex_0++, GuiVar_GroupName_9, &GuiVar_GroupSettingA_9, &GuiVar_GroupSettingB_9, &GuiVar_GroupSettingC_9, &GuiVar_GroupSetting_Visible_9 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void ALERT_ACTIONS_fill_guivars( const UNS_32 pgroup_index_0, char *pgroup_name, UNS_32 *pin_group, UNS_32 *pin_system, UNS_32 *pvisible )
{
	STATION_GROUP_STRUCT *lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( nm_ListGetCount( &station_group_group_list_hdr ) > pgroup_index_0 )
	{
		lgroup = STATION_GROUP_get_group_at_this_index( pgroup_index_0 );

		if( lgroup->in_use	)
		{
			strlcpy( pgroup_name, nm_GROUP_get_name( lgroup ), sizeof(GuiVar_GroupName_0) );

			*pin_group = lgroup->high_flow_action;

			*pin_system = lgroup->low_flow_action;

			*pvisible = (true);
		}
		else
		{
			*pvisible = (false);
		}
	}
	else
	{
		*pvisible = (false);
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void ALERT_ACTIONS_copy_group_into_guivars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	ALERT_ACTIONS_fill_guivars( lindex_0++, GuiVar_GroupName_0, &GuiVar_GroupSettingA_0, &GuiVar_GroupSettingB_0, &GuiVar_GroupSetting_Visible_0 );
	ALERT_ACTIONS_fill_guivars( lindex_0++, GuiVar_GroupName_1, &GuiVar_GroupSettingA_1, &GuiVar_GroupSettingB_1, &GuiVar_GroupSetting_Visible_1 );
	ALERT_ACTIONS_fill_guivars( lindex_0++, GuiVar_GroupName_2, &GuiVar_GroupSettingA_2, &GuiVar_GroupSettingB_2, &GuiVar_GroupSetting_Visible_2 );
	ALERT_ACTIONS_fill_guivars( lindex_0++, GuiVar_GroupName_3, &GuiVar_GroupSettingA_3, &GuiVar_GroupSettingB_3, &GuiVar_GroupSetting_Visible_3 );
	ALERT_ACTIONS_fill_guivars( lindex_0++, GuiVar_GroupName_4, &GuiVar_GroupSettingA_4, &GuiVar_GroupSettingB_4, &GuiVar_GroupSetting_Visible_4 );
	ALERT_ACTIONS_fill_guivars( lindex_0++, GuiVar_GroupName_5, &GuiVar_GroupSettingA_5, &GuiVar_GroupSettingB_5, &GuiVar_GroupSetting_Visible_5 );
	ALERT_ACTIONS_fill_guivars( lindex_0++, GuiVar_GroupName_6, &GuiVar_GroupSettingA_6, &GuiVar_GroupSettingB_6, &GuiVar_GroupSetting_Visible_6 );
	ALERT_ACTIONS_fill_guivars( lindex_0++, GuiVar_GroupName_7, &GuiVar_GroupSettingA_7, &GuiVar_GroupSettingB_7, &GuiVar_GroupSetting_Visible_7 );
	ALERT_ACTIONS_fill_guivars( lindex_0++, GuiVar_GroupName_8, &GuiVar_GroupSettingA_8, &GuiVar_GroupSettingB_8, &GuiVar_GroupSetting_Visible_8 );
	ALERT_ACTIONS_fill_guivars( lindex_0++, GuiVar_GroupName_9, &GuiVar_GroupSettingA_9, &GuiVar_GroupSettingB_9, &GuiVar_GroupSetting_Visible_9 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void ON_AT_A_TIME_fill_guivars( const UNS_32 pgroup_index_0, char *pgroup_name, UNS_32 *pin_group, UNS_32 *pin_system, UNS_32 *pvisible )
{
	STATION_GROUP_STRUCT *lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( nm_ListGetCount( &station_group_group_list_hdr ) > pgroup_index_0 )
	{
		lgroup = STATION_GROUP_get_group_at_this_index( pgroup_index_0 );

		// 9/2/2016 skc : We added the ability to delete station groups, check the in_use flag.
		if( lgroup->in_use )
		{
			strlcpy( pgroup_name, nm_GROUP_get_name( lgroup ), sizeof(GuiVar_GroupName_0) );

			*pin_group = lgroup->on_at_a_time_in_group;

			*pin_system = lgroup->on_at_a_time_in_system;

			*pvisible = (true);
		}
		else
		{
			*pvisible = (false);
		}
	}
	else
	{
		*pvisible = (false);
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void ON_AT_A_TIME_copy_group_into_guivars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	ON_AT_A_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_0, &GuiVar_GroupSettingA_0, &GuiVar_GroupSettingB_0, &GuiVar_GroupSetting_Visible_0 );
	ON_AT_A_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_1, &GuiVar_GroupSettingA_1, &GuiVar_GroupSettingB_1, &GuiVar_GroupSetting_Visible_1 );
	ON_AT_A_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_2, &GuiVar_GroupSettingA_2, &GuiVar_GroupSettingB_2, &GuiVar_GroupSetting_Visible_2 );
	ON_AT_A_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_3, &GuiVar_GroupSettingA_3, &GuiVar_GroupSettingB_3, &GuiVar_GroupSetting_Visible_3 );
	ON_AT_A_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_4, &GuiVar_GroupSettingA_4, &GuiVar_GroupSettingB_4, &GuiVar_GroupSetting_Visible_4 );
	ON_AT_A_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_5, &GuiVar_GroupSettingA_5, &GuiVar_GroupSettingB_5, &GuiVar_GroupSetting_Visible_5 );
	ON_AT_A_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_6, &GuiVar_GroupSettingA_6, &GuiVar_GroupSettingB_6, &GuiVar_GroupSetting_Visible_6 );
	ON_AT_A_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_7, &GuiVar_GroupSettingA_7, &GuiVar_GroupSettingB_7, &GuiVar_GroupSetting_Visible_7 );
	ON_AT_A_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_8, &GuiVar_GroupSettingA_8, &GuiVar_GroupSettingB_8, &GuiVar_GroupSetting_Visible_8 );
	ON_AT_A_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_9, &GuiVar_GroupSettingA_9, &GuiVar_GroupSettingB_9, &GuiVar_GroupSetting_Visible_9 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void PUMP_fill_guivars( const UNS_32 pgroup_index_0, char *pgroup_name, UNS_32 *ppump_in_use, UNS_32 *pvisible )
{
	STATION_GROUP_STRUCT *lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( nm_ListGetCount( &station_group_group_list_hdr ) > pgroup_index_0 )
	{
		lgroup = STATION_GROUP_get_group_at_this_index( pgroup_index_0 );

		// 9/2/2016 skc : We added the ability to delete station groups, check the in_use flag.
		if( lgroup->in_use )
		{

			strlcpy( pgroup_name, nm_GROUP_get_name( lgroup ), sizeof(GuiVar_GroupName_0) );

			*ppump_in_use = lgroup->pump_in_use;

			*pvisible = (true);
		}
		else
		{
			*pvisible = (false);
		}
	}
	else
	{
		*pvisible = (false);
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void PUMP_copy_group_into_guivars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	PUMP_fill_guivars( lindex_0++, GuiVar_GroupName_0, &GuiVar_GroupSettingA_0, &GuiVar_GroupSetting_Visible_0 );
	PUMP_fill_guivars( lindex_0++, GuiVar_GroupName_1, &GuiVar_GroupSettingA_1, &GuiVar_GroupSetting_Visible_1 );
	PUMP_fill_guivars( lindex_0++, GuiVar_GroupName_2, &GuiVar_GroupSettingA_2, &GuiVar_GroupSetting_Visible_2 );
	PUMP_fill_guivars( lindex_0++, GuiVar_GroupName_3, &GuiVar_GroupSettingA_3, &GuiVar_GroupSetting_Visible_3 );
	PUMP_fill_guivars( lindex_0++, GuiVar_GroupName_4, &GuiVar_GroupSettingA_4, &GuiVar_GroupSetting_Visible_4 );
	PUMP_fill_guivars( lindex_0++, GuiVar_GroupName_5, &GuiVar_GroupSettingA_5, &GuiVar_GroupSetting_Visible_5 );
	PUMP_fill_guivars( lindex_0++, GuiVar_GroupName_6, &GuiVar_GroupSettingA_6, &GuiVar_GroupSetting_Visible_6 );
	PUMP_fill_guivars( lindex_0++, GuiVar_GroupName_7, &GuiVar_GroupSettingA_7, &GuiVar_GroupSetting_Visible_7 );
	PUMP_fill_guivars( lindex_0++, GuiVar_GroupName_8, &GuiVar_GroupSettingA_8, &GuiVar_GroupSetting_Visible_8 );
	PUMP_fill_guivars( lindex_0++, GuiVar_GroupName_9, &GuiVar_GroupSettingA_9, &GuiVar_GroupSetting_Visible_9 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void LINE_FILL_TIME_fill_guivars( const UNS_32 pgroup_index_0, char *pgroup_name, UNS_32 *pline_fill_seconds, UNS_32 *pvisible )
{
	STATION_GROUP_STRUCT *lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( nm_ListGetCount( &station_group_group_list_hdr ) > pgroup_index_0 )
	{
		lgroup = STATION_GROUP_get_group_at_this_index( pgroup_index_0 );

		// 9/2/2016 skc : We added the ability to delete station groups, check the in_use flag.
		if( lgroup->in_use )
		{
			strlcpy( pgroup_name, nm_GROUP_get_name( lgroup ), sizeof(GuiVar_GroupName_0) );

			*pline_fill_seconds = lgroup->line_fill_time_sec;

			*pvisible = (true);
		}
		else
		{
			*pvisible = (false);
		}
	}
	else
	{
		*pvisible = (false);
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void LINE_FILL_TIME_copy_group_into_guivars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	LINE_FILL_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_0, &GuiVar_GroupSettingA_0, &GuiVar_GroupSetting_Visible_0 );
	LINE_FILL_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_1, &GuiVar_GroupSettingA_1, &GuiVar_GroupSetting_Visible_1 );
	LINE_FILL_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_2, &GuiVar_GroupSettingA_2, &GuiVar_GroupSetting_Visible_2 );
	LINE_FILL_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_3, &GuiVar_GroupSettingA_3, &GuiVar_GroupSetting_Visible_3 );
	LINE_FILL_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_4, &GuiVar_GroupSettingA_4, &GuiVar_GroupSetting_Visible_4 );
	LINE_FILL_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_5, &GuiVar_GroupSettingA_5, &GuiVar_GroupSetting_Visible_5 );
	LINE_FILL_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_6, &GuiVar_GroupSettingA_6, &GuiVar_GroupSetting_Visible_6 );
	LINE_FILL_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_7, &GuiVar_GroupSettingA_7, &GuiVar_GroupSetting_Visible_7 );
	LINE_FILL_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_8, &GuiVar_GroupSettingA_8, &GuiVar_GroupSetting_Visible_8 );
	LINE_FILL_TIME_fill_guivars( lindex_0++, GuiVar_GroupName_9, &GuiVar_GroupSettingA_9, &GuiVar_GroupSetting_Visible_9 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void DELAY_BETWEEN_VALVES_fill_guivars( const UNS_32 pgroup_index_0, char *pgroup_name, UNS_32 *pdelay_between_valves, UNS_32 *pvisible )
{
	STATION_GROUP_STRUCT *lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( nm_ListGetCount( &station_group_group_list_hdr ) > pgroup_index_0 )
	{
		lgroup = STATION_GROUP_get_group_at_this_index( pgroup_index_0 );

		strlcpy( pgroup_name, nm_GROUP_get_name( lgroup ), sizeof(GuiVar_GroupName_0) );

		*pdelay_between_valves = lgroup->delay_between_valve_time_sec;

		if( *pdelay_between_valves > 0 )
		{
			GuiVar_DelayBetweenValvesInUse = (true);
		}

		*pvisible = (true);
	}
	else
	{
		*pvisible = (false);
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void DELAY_BETWEEN_VALVES_copy_group_into_guivars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// 6/16/2015 ajv : For now, we've opted not to store whether there are slow-closing valves
	// in the file. Instead, we're going to dynamically set it based upon whether any of the
	// delay between valve times are not 0. So set it FALSE for now, and, in the fill_guivars
	// routine, set it true if we find any that have a value other than 0.
	GuiVar_DelayBetweenValvesInUse = (false);

	DELAY_BETWEEN_VALVES_fill_guivars( lindex_0++, GuiVar_GroupName_0, &GuiVar_GroupSettingA_0, &GuiVar_GroupSetting_Visible_0 );
	DELAY_BETWEEN_VALVES_fill_guivars( lindex_0++, GuiVar_GroupName_1, &GuiVar_GroupSettingA_1, &GuiVar_GroupSetting_Visible_1 );
	DELAY_BETWEEN_VALVES_fill_guivars( lindex_0++, GuiVar_GroupName_2, &GuiVar_GroupSettingA_2, &GuiVar_GroupSetting_Visible_2 );
	DELAY_BETWEEN_VALVES_fill_guivars( lindex_0++, GuiVar_GroupName_3, &GuiVar_GroupSettingA_3, &GuiVar_GroupSetting_Visible_3 );
	DELAY_BETWEEN_VALVES_fill_guivars( lindex_0++, GuiVar_GroupName_4, &GuiVar_GroupSettingA_4, &GuiVar_GroupSetting_Visible_4 );
	DELAY_BETWEEN_VALVES_fill_guivars( lindex_0++, GuiVar_GroupName_5, &GuiVar_GroupSettingA_5, &GuiVar_GroupSetting_Visible_5 );
	DELAY_BETWEEN_VALVES_fill_guivars( lindex_0++, GuiVar_GroupName_6, &GuiVar_GroupSettingA_6, &GuiVar_GroupSetting_Visible_6 );
	DELAY_BETWEEN_VALVES_fill_guivars( lindex_0++, GuiVar_GroupName_7, &GuiVar_GroupSettingA_7, &GuiVar_GroupSetting_Visible_7 );
	DELAY_BETWEEN_VALVES_fill_guivars( lindex_0++, GuiVar_GroupName_8, &GuiVar_GroupSettingA_8, &GuiVar_GroupSetting_Visible_8 );
	DELAY_BETWEEN_VALVES_fill_guivars( lindex_0++, GuiVar_GroupName_9, &GuiVar_GroupSettingA_9, &GuiVar_GroupSetting_Visible_9 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void ACQUIRE_EXPECTEDS_fill_guivars( const UNS_32 pgroup_index_0, char *pgroup_name, UNS_32 *pacquire_expecteds, UNS_32 *pvisible )
{
	STATION_GROUP_STRUCT *lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( nm_ListGetCount( &station_group_group_list_hdr ) > pgroup_index_0 )
	{
		lgroup = STATION_GROUP_get_group_at_this_index( pgroup_index_0 );

		strlcpy( pgroup_name, nm_GROUP_get_name( lgroup ), sizeof(GuiVar_GroupName_0) );

		*pacquire_expecteds = lgroup->acquire_expecteds;

		*pvisible = (true);
	}
	else
	{
		*pvisible = (false);
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void ACQUIRE_EXPECTEDS_copy_group_into_guivars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	ACQUIRE_EXPECTEDS_fill_guivars( lindex_0++, GuiVar_GroupName_0, &GuiVar_GroupSettingA_0, &GuiVar_GroupSetting_Visible_0 );
	ACQUIRE_EXPECTEDS_fill_guivars( lindex_0++, GuiVar_GroupName_1, &GuiVar_GroupSettingA_1, &GuiVar_GroupSetting_Visible_1 );
	ACQUIRE_EXPECTEDS_fill_guivars( lindex_0++, GuiVar_GroupName_2, &GuiVar_GroupSettingA_2, &GuiVar_GroupSetting_Visible_2 );
	ACQUIRE_EXPECTEDS_fill_guivars( lindex_0++, GuiVar_GroupName_3, &GuiVar_GroupSettingA_3, &GuiVar_GroupSetting_Visible_3 );
	ACQUIRE_EXPECTEDS_fill_guivars( lindex_0++, GuiVar_GroupName_4, &GuiVar_GroupSettingA_4, &GuiVar_GroupSetting_Visible_4 );
	ACQUIRE_EXPECTEDS_fill_guivars( lindex_0++, GuiVar_GroupName_5, &GuiVar_GroupSettingA_5, &GuiVar_GroupSetting_Visible_5 );
	ACQUIRE_EXPECTEDS_fill_guivars( lindex_0++, GuiVar_GroupName_6, &GuiVar_GroupSettingA_6, &GuiVar_GroupSetting_Visible_6 );
	ACQUIRE_EXPECTEDS_fill_guivars( lindex_0++, GuiVar_GroupName_7, &GuiVar_GroupSettingA_7, &GuiVar_GroupSetting_Visible_7 );
	ACQUIRE_EXPECTEDS_fill_guivars( lindex_0++, GuiVar_GroupName_8, &GuiVar_GroupSettingA_8, &GuiVar_GroupSetting_Visible_8 );
	ACQUIRE_EXPECTEDS_fill_guivars( lindex_0++, GuiVar_GroupName_9, &GuiVar_GroupSettingA_9, &GuiVar_GroupSetting_Visible_9 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void BUDGET_REDUCTION_LIMITS_fill_guivars( const UNS_32 pgroup_index_0, char *pgroup_name, INT_32 *plimit, BOOL_32 *pvisible )
{
	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( pgroup_index_0 < STATION_GROUP_get_num_groups_in_use() )
	{
		lgroup = STATION_GROUP_get_group_at_this_index( pgroup_index_0 );

		// 9/2/2016 skc : We added the ability to delete station groups, check the in_use flag.
		if( lgroup->in_use )
		{

			strlcpy( pgroup_name, nm_GROUP_get_name( lgroup ), sizeof(GuiVar_GroupName_0) );

			// A value of 0 means no reductions are permitted,
			// 100 means the time can be reduced to zero.
			*plimit = (INT_32)(lgroup->budget_reduction_percentage_cap);

			// 5/26/2016 skc : Change to display negative numbers
			*plimit *= -1;

			*pvisible = (true);
		}
		else
		{
			*pvisible = (false);
		}
	}
	else
	{
		*pvisible = (false);
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void BUDGET_REDUCTION_LIMITS_copy_group_into_guivars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	BUDGET_REDUCTION_LIMITS_fill_guivars( lindex_0++, GuiVar_GroupName_0, &GuiVar_BudgetReductionLimit_0, &GuiVar_GroupSetting_Visible_0 );
	BUDGET_REDUCTION_LIMITS_fill_guivars( lindex_0++, GuiVar_GroupName_1, &GuiVar_BudgetReductionLimit_1, &GuiVar_GroupSetting_Visible_1 );
	BUDGET_REDUCTION_LIMITS_fill_guivars( lindex_0++, GuiVar_GroupName_2, &GuiVar_BudgetReductionLimit_2, &GuiVar_GroupSetting_Visible_2 );
	BUDGET_REDUCTION_LIMITS_fill_guivars( lindex_0++, GuiVar_GroupName_3, &GuiVar_BudgetReductionLimit_3, &GuiVar_GroupSetting_Visible_3 );
	BUDGET_REDUCTION_LIMITS_fill_guivars( lindex_0++, GuiVar_GroupName_4, &GuiVar_BudgetReductionLimit_4, &GuiVar_GroupSetting_Visible_4 );
	BUDGET_REDUCTION_LIMITS_fill_guivars( lindex_0++, GuiVar_GroupName_5, &GuiVar_BudgetReductionLimit_5, &GuiVar_GroupSetting_Visible_5 );
	BUDGET_REDUCTION_LIMITS_fill_guivars( lindex_0++, GuiVar_GroupName_6, &GuiVar_BudgetReductionLimit_6, &GuiVar_GroupSetting_Visible_6 );
	BUDGET_REDUCTION_LIMITS_fill_guivars( lindex_0++, GuiVar_GroupName_7, &GuiVar_BudgetReductionLimit_7, &GuiVar_GroupSetting_Visible_7 );
	BUDGET_REDUCTION_LIMITS_fill_guivars( lindex_0++, GuiVar_GroupName_8, &GuiVar_BudgetReductionLimit_8, &GuiVar_GroupSetting_Visible_8 );
    BUDGET_REDUCTION_LIMITS_fill_guivars( lindex_0++, GuiVar_GroupName_9, &GuiVar_BudgetReductionLimit_9, &GuiVar_GroupSetting_Visible_9 );
}

/* ---------------------------------------------------------- */
extern void STATION_GROUP_extract_and_store_group_name_from_GuiVars( void )
{
	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup = STATION_GROUP_get_group_at_this_index( g_GROUP_list_item_index );

	if( lgroup != NULL )
	{
		SHARED_set_name_64_bit_change_bits( lgroup, GuiVar_GroupName, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, STATION_GROUP_get_change_bits_ptr( lgroup, CHANGE_REASON_KEYPAD ), &lgroup->changes_to_upload_to_comm_server, STATION_GROUP_CHANGE_BITFIELD_description_bit );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
// 3/21/2016 skc : Use proper rounding instead of truncation.
// We have had problems with the user entering numbers, and then finding the number has
// changed (i.e 0.67 goes to 0.66). This is due to truncation of specific problematic
// decimal values.
extern void STATION_GROUP_extract_and_store_changes_from_GuiVars( void )
{
	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lgroup = mem_malloc( sizeof(STATION_GROUP_STRUCT) );

	// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
	// update just the variable that's potentially changed in the UI.
	memcpy( lgroup, STATION_GROUP_get_group_with_this_GID( g_GROUP_ID ), sizeof(STATION_GROUP_STRUCT) );

	// ----------

	strlcpy( lgroup->base.description, GuiVar_GroupName, sizeof(lgroup->base.description) );
	lgroup->plant_type = GuiVar_StationGroupPlantType;
	lgroup->head_type = GuiVar_StationGroupHeadType;
	lgroup->precip_rate_in_100000u = __round_float( GuiVar_StationGroupPrecipRate * 100000.0F, 0 );
	lgroup->exposure = GuiVar_StationGroupExposure;
	lgroup->usable_rain_percentage_100u = GuiVar_StationGroupUsableRain;

	lgroup->soil_storage_capacity_inches_100u = __round_float( GuiVar_StationGroupSoilStorageCapacity * 100.0F, 0 );

	lgroup->soil_type = GuiVar_StationGroupSoilType;
	lgroup->slope_percentage = GuiVar_StationGroupSlopePercentage;
	lgroup->crop_coefficient_100u[ 0 ] = __round_float( GuiVar_StationGroupKc1 * 100, 0 );
	lgroup->crop_coefficient_100u[ 1 ] = __round_float( GuiVar_StationGroupKc2 * 100, 0 );
	lgroup->crop_coefficient_100u[ 2 ] = __round_float( GuiVar_StationGroupKc3 * 100, 0 );
	lgroup->crop_coefficient_100u[ 3 ] = __round_float( GuiVar_StationGroupKc4 * 100, 0 );
	lgroup->crop_coefficient_100u[ 4 ] = __round_float( GuiVar_StationGroupKc5 * 100, 0 );
	lgroup->crop_coefficient_100u[ 5 ] = __round_float( GuiVar_StationGroupKc6 * 100, 0 );
	lgroup->crop_coefficient_100u[ 6 ] = __round_float( GuiVar_StationGroupKc7 * 100, 0 );
	lgroup->crop_coefficient_100u[ 7 ] = __round_float( GuiVar_StationGroupKc8 * 100, 0 );
	lgroup->crop_coefficient_100u[ 8 ] = __round_float( GuiVar_StationGroupKc9 * 100, 0 );
	lgroup->crop_coefficient_100u[ 9 ] = __round_float( GuiVar_StationGroupKc10 * 100, 0 );
	lgroup->crop_coefficient_100u[ 10 ] = __round_float( GuiVar_StationGroupKc11 * 100, 0 );
	lgroup->crop_coefficient_100u[ 11 ] = __round_float( GuiVar_StationGroupKc12 * 100, 0 );

	lgroup->allowable_depletion_100u = GuiVar_StationGroupAllowableDepletion;
	lgroup->available_water_100u = __round_float( GuiVar_StationGroupAvailableWater * 100, 0 );

	lgroup->root_zone_depth_10u = __round_float( GuiVar_StationGroupRootZoneDepth * 10, 0 );

	lgroup->species_factor_100u = __round_float( GuiVar_StationGroupSpeciesFactor * 100, 0 );

	lgroup->density_factor_100u = __round_float( GuiVar_StationGroupDensityFactor * 100, 0 );

	lgroup->microclimate_factor_100u = __round_float( GuiVar_StationGroupMicroclimateFactor * 100, 0 );

	lgroup->soil_intake_rate_100u = __round_float( GuiVar_StationGroupSoilIntakeRate * 100, 0 );

	lgroup->allowable_surface_accumulation_100u = __round_float( GuiVar_StationGroupAllowableSurfaceAccum * 100, 0 );
	
	lgroup->GID_irrigation_system = GuiVar_StationGroupSystemGID;

	lgroup->in_use = GuiVar_StationGroupInUse;

	// 3/30/2016 rmd : GUI var needs renaming.
	lgroup->moisture_sensor_serial_number = GuiVar_StationGroupMoistureSensorDecoderSN;

	nm_STATION_GROUP_store_changes( lgroup, g_GROUP_creating_new, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

	// ----------

	mem_free( lgroup );

	// ----------
	
	// 7/7/2015 ajv : Now, in case we've made any changes to the group which affect cycle and
	// soak, update the cycle and soak times for any stations assigned to the group. If the
	// existing cycle is below the new maximum cycle time, it won't be adjusted. Similarly, if
	// the existing soak time is above the new minimum soak time, it won't be adjusted.

	STATION_STRUCT	*lstation;

	UNS_32	i;

	lstation = nm_ListGetFirst( &station_info_list_hdr );

	for( i = 0; i < nm_ListGetCount( &station_info_list_hdr ); ++i )
	{
		if( STATION_get_GID_station_group(lstation) == g_GROUP_ID )
		{
			// 7/7/2015 ajv : The following call may look odd since g_GROUP_ID is the same for both the
			// old and new values. This is expected since we haven't actually changed the group
			// assignment. The function doesn't actually care if the group IDs are the same so this is
			// ok to do (and it sure beats having to create another function which effectively does the
			// same thing with only a single group ID parameter).
			STATION_adjust_group_settings_after_changing_group_assignment( lstation, g_GROUP_ID, g_GROUP_ID, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD );
		}

		lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
	}

	// ----------

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// Reset the Creating New flag to ensure we don't accidentally create
	// another new group when using the BACK key.
	g_GROUP_creating_new = (false);
}

/* ---------------------------------------------------------- */
static void PRIORITY_extract_and_store_individual_group_change( const UNS_32 pgroup_index_0, UNS_32 *ppriority_level, UNS_32 *pvisible )
{
	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( *pvisible == (true) )
	{
		lgroup = mem_malloc( sizeof(STATION_GROUP_STRUCT) );

		// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
		// update just the variable that's potentially changed in the UI.
		memcpy( lgroup, STATION_GROUP_get_group_at_this_index( pgroup_index_0 ), sizeof(STATION_GROUP_STRUCT) );

		// ----------

		lgroup->priority_level = *ppriority_level;

		nm_STATION_GROUP_store_changes( lgroup, (false), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

		// ----------

		mem_free( lgroup );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void PRIORITY_extract_and_store_changes_from_GuiVars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	PRIORITY_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_0, &GuiVar_GroupSetting_Visible_0 );
	PRIORITY_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_1, &GuiVar_GroupSetting_Visible_1 );
	PRIORITY_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_2, &GuiVar_GroupSetting_Visible_2 );
	PRIORITY_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_3, &GuiVar_GroupSetting_Visible_3 );
	PRIORITY_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_4, &GuiVar_GroupSetting_Visible_4 );
	PRIORITY_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_5, &GuiVar_GroupSetting_Visible_5 );
	PRIORITY_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_6, &GuiVar_GroupSetting_Visible_6 );
	PRIORITY_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_7, &GuiVar_GroupSetting_Visible_7 );
	PRIORITY_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_8, &GuiVar_GroupSetting_Visible_8 );
	PRIORITY_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_9, &GuiVar_GroupSetting_Visible_9 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void PERCENT_ADJUST_extract_and_store_individual_group_change( const UNS_32 pgroup_index_0, INT_32 *ppercentage, UNS_32 *pdays, UNS_32 *pvisible )
{
	DATE_TIME	ldt;

	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( *pvisible == (true) )
	{
		lgroup = mem_malloc( sizeof(STATION_GROUP_STRUCT) );

		// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
		// update just the variable that's potentially changed in the UI.
		memcpy( lgroup, STATION_GROUP_get_group_at_this_index( pgroup_index_0 ), sizeof(STATION_GROUP_STRUCT) );

		// ----------

		lgroup->percent_adjust_100u = (UNS_32)(*ppercentage + 100);

		if( lgroup->percent_adjust_100u != PERCENT_ADJUST_PERCENT_DEFAULT_100u )
		{
			EPSON_obtain_latest_time_and_date( &ldt );

			lgroup->percent_adjust_start_date = ldt.D;

			lgroup->percent_adjust_end_date = (ldt.D + *pdays);
		}

		nm_STATION_GROUP_store_changes( lgroup, (false), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

		// ----------

		mem_free( lgroup );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void PERCENT_ADJUST_extract_and_store_changes_from_GuiVars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	PERCENT_ADJUST_extract_and_store_individual_group_change( lindex_0++, &GuiVar_PercentAdjustPercent_0, &GuiVar_GroupSettingB_0, &GuiVar_GroupSetting_Visible_0 );
	PERCENT_ADJUST_extract_and_store_individual_group_change( lindex_0++, &GuiVar_PercentAdjustPercent_1, &GuiVar_GroupSettingB_1, &GuiVar_GroupSetting_Visible_1 );
	PERCENT_ADJUST_extract_and_store_individual_group_change( lindex_0++, &GuiVar_PercentAdjustPercent_2, &GuiVar_GroupSettingB_2, &GuiVar_GroupSetting_Visible_2 );
	PERCENT_ADJUST_extract_and_store_individual_group_change( lindex_0++, &GuiVar_PercentAdjustPercent_3, &GuiVar_GroupSettingB_3, &GuiVar_GroupSetting_Visible_3 );
	PERCENT_ADJUST_extract_and_store_individual_group_change( lindex_0++, &GuiVar_PercentAdjustPercent_4, &GuiVar_GroupSettingB_4, &GuiVar_GroupSetting_Visible_4 );
	PERCENT_ADJUST_extract_and_store_individual_group_change( lindex_0++, &GuiVar_PercentAdjustPercent_5, &GuiVar_GroupSettingB_5, &GuiVar_GroupSetting_Visible_5 );
	PERCENT_ADJUST_extract_and_store_individual_group_change( lindex_0++, &GuiVar_PercentAdjustPercent_6, &GuiVar_GroupSettingB_6, &GuiVar_GroupSetting_Visible_6 );
	PERCENT_ADJUST_extract_and_store_individual_group_change( lindex_0++, &GuiVar_PercentAdjustPercent_7, &GuiVar_GroupSettingB_7, &GuiVar_GroupSetting_Visible_7 );
	PERCENT_ADJUST_extract_and_store_individual_group_change( lindex_0++, &GuiVar_PercentAdjustPercent_8, &GuiVar_GroupSettingB_8, &GuiVar_GroupSetting_Visible_8 );
	PERCENT_ADJUST_extract_and_store_individual_group_change( lindex_0++, &GuiVar_PercentAdjustPercent_9, &GuiVar_GroupSettingB_9, &GuiVar_GroupSetting_Visible_9 );
}

/* ---------------------------------------------------------- */
extern void SCHEDULE_extract_and_store_changes_from_GuiVars( void )
{
	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lgroup = mem_malloc( sizeof(STATION_GROUP_STRUCT) );

	// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
	// update just the variable that's potentially changed in the UI.
	memcpy( lgroup, STATION_GROUP_get_group_with_this_GID( g_GROUP_ID ), sizeof(STATION_GROUP_STRUCT) );

	// ----------

	lgroup->schedule_enabled_bool = GuiVar_ScheduleStartTimeEnabled;
	lgroup->schedule_type = GuiVar_ScheduleType;
	lgroup->start_time = (GuiVar_ScheduleStartTime * 60);
	lgroup->stop_time = (GuiVar_ScheduleStopTime * 60);
	lgroup->begin_date = GuiVar_ScheduleBeginDate;
	lgroup->water_days_bool[ SUNDAY ] = GuiVar_ScheduleWaterDay_Sun;
	lgroup->water_days_bool[ MONDAY ] = GuiVar_ScheduleWaterDay_Mon;
	lgroup->water_days_bool[ TUESDAY ] = GuiVar_ScheduleWaterDay_Tue;
	lgroup->water_days_bool[ WEDNESDAY ] = GuiVar_ScheduleWaterDay_Wed;
	lgroup->water_days_bool[ THURSDAY ] = GuiVar_ScheduleWaterDay_Thu;
	lgroup->water_days_bool[ FRIDAY ] = GuiVar_ScheduleWaterDay_Fri;
	lgroup->water_days_bool[ SATURDAY ] = GuiVar_ScheduleWaterDay_Sat;
	lgroup->irrigate_on_29th_or_31st_bool = GuiVar_ScheduleIrrigateOn29thOr31st;
	lgroup->mow_day = GuiVar_ScheduleMowDay;

	lgroup->use_et_averaging_bool = GuiVar_ScheduleUseETAveraging;

	nm_STATION_GROUP_store_changes( lgroup, (false), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

	// ----------

	mem_free( lgroup );
	
	// ----------

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void WEATHER_extract_and_store_individual_group_change( const UNS_32 pgroup_index_0, UNS_32 *pet_in_use, UNS_32 *prain_in_use, UNS_32 *pwind_in_use, UNS_32 *pvisible )
{
	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( *pvisible == (true) )
	{
		lgroup = mem_malloc( sizeof(STATION_GROUP_STRUCT) );

		// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
		// update just the variable that's potentially changed in the UI.
		memcpy( lgroup, STATION_GROUP_get_group_at_this_index( pgroup_index_0 ), sizeof(STATION_GROUP_STRUCT) );

		// ----------

		lgroup->et_in_use = *pet_in_use;
		lgroup->rain_in_use_bool = *prain_in_use;
		lgroup->wind_in_use_bool = *pwind_in_use;

		nm_STATION_GROUP_store_changes( lgroup, (false), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

		// ----------

		mem_free( lgroup );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void WEATHER_extract_and_store_changes_from_GuiVars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	WEATHER_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_0, &GuiVar_GroupSettingB_0, &GuiVar_GroupSettingC_0, &GuiVar_GroupSetting_Visible_0 );
	WEATHER_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_1, &GuiVar_GroupSettingB_1, &GuiVar_GroupSettingC_1, &GuiVar_GroupSetting_Visible_1 );
	WEATHER_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_2, &GuiVar_GroupSettingB_2, &GuiVar_GroupSettingC_2, &GuiVar_GroupSetting_Visible_2 );
	WEATHER_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_3, &GuiVar_GroupSettingB_3, &GuiVar_GroupSettingC_3, &GuiVar_GroupSetting_Visible_3 );
	WEATHER_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_4, &GuiVar_GroupSettingB_4, &GuiVar_GroupSettingC_4, &GuiVar_GroupSetting_Visible_4 );
	WEATHER_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_5, &GuiVar_GroupSettingB_5, &GuiVar_GroupSettingC_5, &GuiVar_GroupSetting_Visible_5 );
	WEATHER_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_6, &GuiVar_GroupSettingB_6, &GuiVar_GroupSettingC_6, &GuiVar_GroupSetting_Visible_6 );
	WEATHER_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_7, &GuiVar_GroupSettingB_7, &GuiVar_GroupSettingC_7, &GuiVar_GroupSetting_Visible_7 );
	WEATHER_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_8, &GuiVar_GroupSettingB_8, &GuiVar_GroupSettingC_8, &GuiVar_GroupSetting_Visible_8 );
	WEATHER_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_9, &GuiVar_GroupSettingB_9, &GuiVar_GroupSettingC_9, &GuiVar_GroupSetting_Visible_9 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void ALERT_ACTIONS_extract_and_store_individual_group_change( const UNS_32 pgroup_index_0, UNS_32 *phigh_flow_action, UNS_32 *plow_flow_action, UNS_32 *pvisible )
{
	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( *pvisible == (true) )
	{
		lgroup = mem_malloc( sizeof(STATION_GROUP_STRUCT) );

		// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
		// update just the variable that's potentially changed in the UI.
		memcpy( lgroup, STATION_GROUP_get_group_at_this_index( pgroup_index_0 ), sizeof(STATION_GROUP_STRUCT) );

		// ----------

		lgroup->high_flow_action = *phigh_flow_action;

		lgroup->low_flow_action = *plow_flow_action;

		nm_STATION_GROUP_store_changes( lgroup, (false), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

		// ----------

		mem_free( lgroup );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void ALERT_ACTIONS_extract_and_store_changes_from_GuiVars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	ALERT_ACTIONS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_0, &GuiVar_GroupSettingB_0, &GuiVar_GroupSetting_Visible_0 );
	ALERT_ACTIONS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_1, &GuiVar_GroupSettingB_1, &GuiVar_GroupSetting_Visible_1 );
	ALERT_ACTIONS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_2, &GuiVar_GroupSettingB_2, &GuiVar_GroupSetting_Visible_2 );
	ALERT_ACTIONS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_3, &GuiVar_GroupSettingB_3, &GuiVar_GroupSetting_Visible_3 );
	ALERT_ACTIONS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_4, &GuiVar_GroupSettingB_4, &GuiVar_GroupSetting_Visible_4 );
	ALERT_ACTIONS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_5, &GuiVar_GroupSettingB_5, &GuiVar_GroupSetting_Visible_5 );
	ALERT_ACTIONS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_6, &GuiVar_GroupSettingB_6, &GuiVar_GroupSetting_Visible_6 );
	ALERT_ACTIONS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_7, &GuiVar_GroupSettingB_7, &GuiVar_GroupSetting_Visible_7 );
	ALERT_ACTIONS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_8, &GuiVar_GroupSettingB_8, &GuiVar_GroupSetting_Visible_8 );
	ALERT_ACTIONS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_9, &GuiVar_GroupSettingB_9, &GuiVar_GroupSetting_Visible_9 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void ON_AT_A_TIME_extract_and_store_individual_group_change( const UNS_32 pgroup_index_0, UNS_32 *pin_group, UNS_32 *pin_system, UNS_32 *pvisible )
{
	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( *pvisible == (true) )
	{
		lgroup = mem_malloc( sizeof(STATION_GROUP_STRUCT) );

		// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
		// update just the variable that's potentially changed in the UI.
		memcpy( lgroup, STATION_GROUP_get_group_at_this_index( pgroup_index_0 ), sizeof(STATION_GROUP_STRUCT) );

		// ----------

		lgroup->on_at_a_time_in_group = *pin_group;

		lgroup->on_at_a_time_in_system = *pin_system;

		nm_STATION_GROUP_store_changes( lgroup, (false), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

		// ----------

		mem_free( lgroup );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void ON_AT_A_TIME_extract_and_store_changes_from_GuiVars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	ON_AT_A_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_0, &GuiVar_GroupSettingB_0, &GuiVar_GroupSetting_Visible_0 );
	ON_AT_A_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_1, &GuiVar_GroupSettingB_1, &GuiVar_GroupSetting_Visible_1 );
	ON_AT_A_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_2, &GuiVar_GroupSettingB_2, &GuiVar_GroupSetting_Visible_2 );
	ON_AT_A_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_3, &GuiVar_GroupSettingB_3, &GuiVar_GroupSetting_Visible_3 );
	ON_AT_A_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_4, &GuiVar_GroupSettingB_4, &GuiVar_GroupSetting_Visible_4 );
	ON_AT_A_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_5, &GuiVar_GroupSettingB_5, &GuiVar_GroupSetting_Visible_5 );
	ON_AT_A_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_6, &GuiVar_GroupSettingB_6, &GuiVar_GroupSetting_Visible_6 );
	ON_AT_A_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_7, &GuiVar_GroupSettingB_7, &GuiVar_GroupSetting_Visible_7 );
	ON_AT_A_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_8, &GuiVar_GroupSettingB_8, &GuiVar_GroupSetting_Visible_8 );
	ON_AT_A_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_9, &GuiVar_GroupSettingB_9, &GuiVar_GroupSetting_Visible_9 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void PUMP_extract_and_store_individual_group_change( const UNS_32 pgroup_index_0, UNS_32 *ppump_in_use, UNS_32 *pvisible )
{
	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( *pvisible == (true) )
	{
		lgroup = mem_malloc( sizeof(STATION_GROUP_STRUCT) );

		// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
		// update just the variable that's potentially changed in the UI.
		memcpy( lgroup, STATION_GROUP_get_group_at_this_index( pgroup_index_0 ), sizeof(STATION_GROUP_STRUCT) );

		// ----------

		lgroup->pump_in_use = *ppump_in_use;

		nm_STATION_GROUP_store_changes( lgroup, (false), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

		// ----------

		mem_free( lgroup );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void PUMP_extract_and_store_changes_from_GuiVars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	PUMP_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_0, &GuiVar_GroupSetting_Visible_0 );
	PUMP_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_1, &GuiVar_GroupSetting_Visible_1 );
	PUMP_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_2, &GuiVar_GroupSetting_Visible_2 );
	PUMP_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_3, &GuiVar_GroupSetting_Visible_3 );
	PUMP_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_4, &GuiVar_GroupSetting_Visible_4 );
	PUMP_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_5, &GuiVar_GroupSetting_Visible_5 );
	PUMP_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_6, &GuiVar_GroupSetting_Visible_6 );
	PUMP_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_7, &GuiVar_GroupSetting_Visible_7 );
	PUMP_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_8, &GuiVar_GroupSetting_Visible_8 );
	PUMP_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_9, &GuiVar_GroupSetting_Visible_9 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void LINE_FILL_TIME_extract_and_store_individual_group_change( const UNS_32 pgroup_index_0, UNS_32 *pline_fill_seconds, UNS_32 *pvisible )
{
	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( *pvisible == (true) )
	{
		lgroup = mem_malloc( sizeof(STATION_GROUP_STRUCT) );

		// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
		// update just the variable that's potentially changed in the UI.
		memcpy( lgroup, STATION_GROUP_get_group_at_this_index( pgroup_index_0 ), sizeof(STATION_GROUP_STRUCT) );

		// ----------

		lgroup->line_fill_time_sec = *pline_fill_seconds;

		nm_STATION_GROUP_store_changes( lgroup, (false), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

		// ----------

		mem_free( lgroup );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void LINE_FILL_TIME_extract_and_store_changes_from_GuiVars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	LINE_FILL_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_0, &GuiVar_GroupSetting_Visible_0 );
	LINE_FILL_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_1, &GuiVar_GroupSetting_Visible_1 );
	LINE_FILL_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_2, &GuiVar_GroupSetting_Visible_2 );
	LINE_FILL_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_3, &GuiVar_GroupSetting_Visible_3 );
	LINE_FILL_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_4, &GuiVar_GroupSetting_Visible_4 );
	LINE_FILL_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_5, &GuiVar_GroupSetting_Visible_5 );
	LINE_FILL_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_6, &GuiVar_GroupSetting_Visible_6 );
	LINE_FILL_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_7, &GuiVar_GroupSetting_Visible_7 );
	LINE_FILL_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_8, &GuiVar_GroupSetting_Visible_8 );
	LINE_FILL_TIME_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_9, &GuiVar_GroupSetting_Visible_9 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change( const UNS_32 pgroup_index_0, UNS_32 *pdelay_between_valves, UNS_32 *pvisible )
{
	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( *pvisible == (true) )
	{
		lgroup = mem_malloc( sizeof(STATION_GROUP_STRUCT) );

		// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
		// update just the variable that's potentially changed in the UI.
		memcpy( lgroup, STATION_GROUP_get_group_at_this_index( pgroup_index_0 ), sizeof(STATION_GROUP_STRUCT) );

		// ----------

		// 6/16/2015 ajv : For now, we've opted not to store whether there are slow-closing valves
		// in the file. Instead, we dynamically set it based upon whether any of the delay between
		// valve times are not 0. So if the user set it to NO, force the delay between valves to 0.
		if( !GuiVar_DelayBetweenValvesInUse )
		{
			*pdelay_between_valves = 0;
		}

		lgroup->delay_between_valve_time_sec = *pdelay_between_valves;

		nm_STATION_GROUP_store_changes( lgroup, (false), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

		// ----------

		mem_free( lgroup );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void DELAY_BETWEEN_VALVES_extract_and_store_changes_from_GuiVars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_0, &GuiVar_GroupSetting_Visible_0 );
	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_1, &GuiVar_GroupSetting_Visible_1 );
	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_2, &GuiVar_GroupSetting_Visible_2 );
	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_3, &GuiVar_GroupSetting_Visible_3 );
	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_4, &GuiVar_GroupSetting_Visible_4 );
	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_5, &GuiVar_GroupSetting_Visible_5 );
	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_6, &GuiVar_GroupSetting_Visible_6 );
	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_7, &GuiVar_GroupSetting_Visible_7 );
	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_8, &GuiVar_GroupSetting_Visible_8 );
	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_9, &GuiVar_GroupSetting_Visible_9 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change( const UNS_32 pgroup_index_0, UNS_32 *pacquire_expecteds, UNS_32 *pvisible )
{
	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( *pvisible == (true) )
	{
		lgroup = mem_malloc( sizeof(STATION_GROUP_STRUCT) );

		// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
		// update just the variable that's potentially changed in the UI.
		memcpy( lgroup, STATION_GROUP_get_group_at_this_index( pgroup_index_0 ), sizeof(STATION_GROUP_STRUCT) );

		// ----------

		lgroup->acquire_expecteds = *pacquire_expecteds;

		nm_STATION_GROUP_store_changes( lgroup, (false), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

		// ----------

		mem_free( lgroup );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void ACQUIRE_EXPECTEDS_extract_and_store_changes_from_GuiVars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_0, &GuiVar_GroupSetting_Visible_0 );
	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_1, &GuiVar_GroupSetting_Visible_1 );
	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_2, &GuiVar_GroupSetting_Visible_2 );
	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_3, &GuiVar_GroupSetting_Visible_3 );
	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_4, &GuiVar_GroupSetting_Visible_4 );
	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_5, &GuiVar_GroupSetting_Visible_5 );
	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_6, &GuiVar_GroupSetting_Visible_6 );
	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_7, &GuiVar_GroupSetting_Visible_7 );
	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_8, &GuiVar_GroupSetting_Visible_8 );
	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_GroupSettingA_9, &GuiVar_GroupSetting_Visible_9 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change( const UNS_32 pgroup_index_0, INT_32 *plimit, UNS_32 *pvisible )
{
	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( *pvisible == (true) )
	{
		lgroup = mem_malloc( sizeof(STATION_GROUP_STRUCT) );

		// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
		// update just the variable that's potentially changed in the UI.
		memcpy( lgroup, STATION_GROUP_get_group_at_this_index( pgroup_index_0 ), sizeof(STATION_GROUP_STRUCT) );

		// 5/26/2016 skc : If the value is negative, convert to positive. If the user interface
		// allows negative numbers this is how we handle it.
		if( *plimit < 0 )
		{
			*plimit *= -1;
		}
		lgroup->budget_reduction_percentage_cap = (UNS_32)(*plimit);

		nm_STATION_GROUP_store_changes( lgroup, (false), CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

		mem_free( lgroup );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void BUDGET_REDUCTION_LIMITS_extract_and_store_changes_from_GuiVars( void )
{
	UNS_32	lindex_0;

	lindex_0 = 0;

	// 05/19/2016 rmd : For the 10 potential station groups on the screen.
	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_BudgetReductionLimit_0, &GuiVar_GroupSetting_Visible_0 );
	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_BudgetReductionLimit_1, &GuiVar_GroupSetting_Visible_1 );
	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_BudgetReductionLimit_2, &GuiVar_GroupSetting_Visible_2 );
	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_BudgetReductionLimit_3, &GuiVar_GroupSetting_Visible_3 );
	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_BudgetReductionLimit_4, &GuiVar_GroupSetting_Visible_4 );
	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_BudgetReductionLimit_5, &GuiVar_GroupSetting_Visible_5 );
	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_BudgetReductionLimit_6, &GuiVar_GroupSetting_Visible_6 );
	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_BudgetReductionLimit_7, &GuiVar_GroupSetting_Visible_7 );
	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_BudgetReductionLimit_8, &GuiVar_GroupSetting_Visible_8 );
	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change( lindex_0++, &GuiVar_BudgetReductionLimit_9, &GuiVar_GroupSetting_Visible_9 );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void nm_STATION_GROUP_load_group_name_into_guivar( const INT_16 pindex_0_i16 )
{
	STATION_GROUP_STRUCT *lgroup;

	if( (UNS_32)pindex_0_i16 < STATION_GROUP_get_num_groups_in_use() )
	{
		lgroup = STATION_GROUP_get_group_at_this_index( (UNS_32)pindex_0_i16 );

		if( nm_OnList(&station_group_group_list_hdr, lgroup) == (true) )
		{
			strlcpy( GuiVar_itmGroupName, nm_GROUP_get_name( lgroup ), NUMBER_OF_CHARS_IN_A_NAME );
		}
		else
		{
			Alert_group_not_found();
		}
	}
	else if( (UNS_32)pindex_0_i16 == STATION_GROUP_get_num_groups_in_use() )
	{
		snprintf( GuiVar_itmGroupName, NUMBER_OF_CHARS_IN_A_NAME, "%s", GuiLib_GetTextPtr( GuiStruct_txtGroupDivider_0, 0 ) );
	}
	else
	{
		snprintf( GuiVar_itmGroupName, NUMBER_OF_CHARS_IN_A_NAME, "  <%s %s>", GuiLib_GetTextPtr( GuiStruct_txtAddNew_0, 0 ), GuiLib_GetTextPtr( GuiStruct_txtGroup_0, 0 ) );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the schedule with the passed in GID value. If the 
 * matching group cannot be found, an alert is generated and a NULL is returned. 
 * The user <b>MUST</b> test for NULL before using the return value! 
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the list doesn't change during this process.
 *
 * @executed Executed within the context of the TD check task on a 1 Hz basis 
 *  		 when the task checks for a start time.
 * 
 * @param pgroup_ID The schedule's group ID.
 * 
 * @return STATION_GROUP_GROUP_STRUCT* A pointer to the schedule with the passed in 
 *  	   group ID.
 *
 * @author 9/20/2011 BobD
 *
 * @revisions
 *    9/20/2011 Initial release
 */
extern STATION_GROUP_STRUCT *STATION_GROUP_get_group_with_this_GID( const UNS_32 pgroup_ID )
{
	STATION_GROUP_STRUCT	*rv;
	
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// If the passed-in group ID is 0, it means the station we grabbed it from
	// isn't assigned to a schedule group yet. Therefore return NULL. We'll then
	// handle the NULL in the calling function.
	if( pgroup_ID > 0 )
	{
		rv = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, pgroup_ID, (false) );
		
		if( rv == NULL )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "Group with this GID not in the list." );
		}
	}
	else
	{
		rv = NULL;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern void *STATION_GROUP_get_group_at_this_index( const UNS_32 pindex_0 )
{
	STATION_GROUP_STRUCT *lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup = nm_GROUP_get_ptr_to_group_at_this_location_in_list( &station_group_group_list_hdr, pindex_0 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( lgroup );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_GROUP_get_index_for_group_with_this_GID( const UNS_32 pgroup_ID )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = nm_GROUP_get_index_for_group_with_this_GID( &station_group_group_list_hdr, pgroup_ID );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_GROUP_get_last_group_ID( void )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = ((GROUP_BASE_DEFINITION_STRUCT*)station_group_group_list_hdr.phead)->number_of_groups_ever_created;

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_GROUP_get_num_groups_in_use( void )
{
	STATION_GROUP_STRUCT *lgroup;

	UNS_32	rv;
	
	// ----------

	rv = 0;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup = nm_ListGetFirst( &station_group_group_list_hdr );

	while( lgroup != NULL )
	{
		if( lgroup->in_use )
		{
			++rv;
		}

		// ----------

		lgroup = nm_ListGetNext( &station_group_group_list_hdr, lgroup );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern UNS_32 STATION_GROUP_get_plant_type( STATION_GROUP_STRUCT *const pgroup )
{
	UNS_32 rv;

	// To ensure the group isn't deleted between retrieving and copying the head
	// type setting, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_64_bit_change_bits_group( pgroup,
													 &pgroup->plant_type,
													 STATION_GROUP_PLANT_TYPE_MIN,
													 STATION_GROUP_PLANT_TYPE_MAX,
													 STATION_GROUP_PLANT_TYPE_DEFAULT,
													 &nm_STATION_GROUP_set_plant_type,
													 &pgroup->changes_to_send_to_master,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_plant_type_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_GROUP_get_head_type( STATION_GROUP_STRUCT *const pgroup )
{
	UNS_32 rv;

	// To ensure the group isn't deleted between retrieving and copying the head
	// type setting, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_64_bit_change_bits_group( pgroup,
													 &pgroup->head_type,
													 STATION_GROUP_HEAD_TYPE_MIN,
													 STATION_GROUP_HEAD_TYPE_MAX,
													 STATION_GROUP_HEAD_TYPE_DEFAULT,
													 &nm_STATION_GROUP_set_head_type,
													 &pgroup->changes_to_send_to_master,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_head_type_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_GROUP_get_soil_type( STATION_GROUP_STRUCT *const pgroup )
{
	UNS_32 rv;

	// To ensure the group isn't deleted between retrieving and copying the head
	// type setting, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_64_bit_change_bits_group( pgroup,
													 &pgroup->soil_type,
													 STATION_GROUP_SOIL_TYPE_MIN,
													 STATION_GROUP_SOIL_TYPE_MAX,
													 STATION_GROUP_SOIL_TYPE_DEFAULT,
													 &nm_STATION_GROUP_set_soil_type,
													 &pgroup->changes_to_send_to_master,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_soil_type_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_GROUP_get_slope_percentage( STATION_GROUP_STRUCT *const pgroup )
{
	UNS_32 rv;

	// To ensure the group isn't deleted between retrieving and copying the head
	// type setting, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_64_bit_change_bits_group( pgroup,
													 &pgroup->slope_percentage,
													 STATION_GROUP_SLOPE_PERCENTAGE_MIN,
													 STATION_GROUP_SLOPE_PERCENTAGE_MAX,
													 STATION_GROUP_SLOPE_PERCENTAGE_DEFAULT,
													 &nm_STATION_GROUP_set_slope_percentage,
													 &pgroup->changes_to_send_to_master,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_slope_percentage_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern float STATION_GROUP_get_precip_rate_in_per_hr( STATION_GROUP_STRUCT *const pgroup )
{
	UNS_32	precip_rate_100000u;

	float	rv;

	// To ensure the group isn't deleted between retrieving and copying the head
	// type setting, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	precip_rate_100000u = SHARED_get_uint32_64_bit_change_bits_group( pgroup,
																	  &pgroup->precip_rate_in_100000u,
																	  STATION_GROUP_PRECIP_RATE_MIN_100000u,
																	  STATION_GROUP_PRECIP_RATE_MAX_100000u,
																	  STATION_GROUP_PRECIP_RATE_DEFAULT_100000u,
																	  &nm_STATION_GROUP_set_precip_rate,
																	  &pgroup->changes_to_send_to_master,
																	  STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_precip_rate_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	rv = (float)(precip_rate_100000u / 100000.0);

	return( rv );
}

/* ---------------------------------------------------------- */
extern float STATION_GROUP_get_allowable_surface_accumulation( STATION_GROUP_STRUCT *const pgroup )
{
	UNS_32	allowable_surface_accum_100u;

	float	rv;

	// To ensure the group isn't deleted between retrieving and copying the head
	// type setting, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	allowable_surface_accum_100u = SHARED_get_uint32_64_bit_change_bits_group( pgroup,
																			   &pgroup->allowable_surface_accumulation_100u,
																			   STATION_GROUP_ALLOWABLE_SURFACE_ACCUM_MIN_100u,
																			   STATION_GROUP_ALLOWABLE_SURFACE_ACCUM_MAX_100u,
																			   STATION_GROUP_ALLOWABLE_SURFACE_ACCUM_DEFAULT_100u,
																			   &nm_STATION_GROUP_set_allowable_surface_accumulation,
																			   &pgroup->changes_to_send_to_master,
																			   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_allowable_surface_accum_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	rv = (float)(allowable_surface_accum_100u / 100.0);

	return( rv );
}

/* ---------------------------------------------------------- */
extern float STATION_GROUP_get_soil_intake_rate( STATION_GROUP_STRUCT *const pgroup )
{
	UNS_32	soil_intake_rate_100u;

	float	rv;

	// To ensure the group isn't deleted between retrieving and copying the head
	// type setting, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	soil_intake_rate_100u = SHARED_get_uint32_64_bit_change_bits_group( pgroup,
																		&pgroup->soil_intake_rate_100u,
																		STATION_GROUP_SOIL_INTAKE_RATE_MIN_100u,
																		STATION_GROUP_SOIL_INTAKE_RATE_MAX_100u,
																		STATION_GROUP_SOIL_INTAKE_RATE_DEFAULT_100u,
																		&nm_STATION_GROUP_set_soil_intake_rate,
																		&pgroup->changes_to_send_to_master,
																		STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_soil_intake_rate_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	rv = (float)(soil_intake_rate_100u / 100.0);

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns whether the specified schedule is enabled or not.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the list doesn't change during this process.
 *
 * @executed Executed within the context of the TD check task when a start time
 *           is crossed and one or more stations are using Daily ET.
 * 
 * @param pgroup a pointer to the schedule group.
 * 
 * @return BOOL_32 True if the schedule is enabled; otherwise, false.
 *
 * @author 7/12/2012 Adrianusv
 *
 * @revisions
 *    7/12/2012 Initial release
 */
extern BOOL_32 SCHEDULE_get_enabled( STATION_GROUP_STRUCT *const pgroup )
{
	BOOL_32 rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_bool_64_bit_change_bits_group( pgroup,
												   &pgroup->schedule_enabled_bool,
												   SCHEDULE_ENABLED_DEFAULT,
												   &nm_SCHEDULE_set_enabled,
												   &pgroup->changes_to_send_to_master,
												   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_schedule_enabled_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * This function uses the provided pointer to reach the start time member and 
 * retrieve and return its value. A test is made that the pointer is valid. And 
 * a range check is made on the retrieved value before it is returned. 
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the list doesn't change during this process.
 *
 * @executed Executed within the context of the TD check task on a 1 Hz basis 
 *  		 when the task checks for a start time.
 * 
 * @param pgroup A pointer to the group.
 * 
 * @return UNS_32 The start time of the specified group.
 *
 * @author 9/20/2011 BobD
 *
 * @revisions
 *    9/20/2011 Initial release
 *    2/7/2012  Replaced pgroup != NULL with call to nm_OnList to verify the
 *              pointer is not only not NULL, but is also a member of the list.
 */
extern UNS_32 SCHEDULE_get_start_time( STATION_GROUP_STRUCT *const pgroup )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// 12/10/2012 ajv : We MUST look at the enabled flag to determine whether there is a valid
	// start time or not. Otherwise, we'll fail the range check and send a MIN value of 0,
	// which equates to midnight. So, if the schedule is not enabled, return 86400.
	if( pgroup->schedule_enabled_bool )
	{
		rv = SHARED_get_uint32_64_bit_change_bits_group( pgroup,
														 &pgroup->start_time,
														 SCHEDULE_START_TIME_MIN,
														 SCHEDULE_OFF_TIME,		// 6/17/2015 ajv : Note that this is not SCHEDULE_START_TIME_MAX. This is intentional as a time of 86400 is valid when storing as it indicates OFF.
														 SCHEDULE_OFF_TIME,
														 &nm_SCHEDULE_set_start_time,
														 &pgroup->changes_to_send_to_master,
														 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_start_time_bit ] );
	}
	else
	{
		rv = SCHEDULE_OFF_TIME;
	}
	
	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * This function uses the provided pointer to reach the stop time member and 
 * retrieve and return its value. A test is made that the pointer is valid. And 
 * a range check is made on the retrieved value before it is returned. 
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the list doesn't change during this process.
 *
 * @executed Executed within the context of the TD check task on a 1 Hz basis 
 *  		 when the task checks for a start time.
 * 
 * @param pgroup A pointer to the group.
 * 
 * @return UNS_32 The stop time of the specified group.
 *
 * @author 9/20/2011 BobD
 *
 * @revisions
 *    9/20/2011 Initial release
 *    2/7/2012  Replaced pgroup != NULL with call to nm_OnList to verify the
 *              pointer is not only not NULL, but is also a member of the list.
 */
extern UNS_32 SCHEDULE_get_stop_time( STATION_GROUP_STRUCT *const pgroup )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_64_bit_change_bits_group( pgroup,
													 &pgroup->stop_time,
													 SCHEDULE_STOP_TIME_MIN,
													 SCHEDULE_STOP_TIME_MAX,
													 SCHEDULE_STOP_TIME_MIN,
													 &nm_SCHEDULE_set_stop_time,
													 &pgroup->changes_to_send_to_master,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_stop_time_bit ] );
	
	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 SCHEDULE_get_schedule_type( STATION_GROUP_STRUCT *const pgroup )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_64_bit_change_bits_group( pgroup,
													 &pgroup->schedule_type,
													 SCHEDULE_TYPE_MIN,
													 SCHEDULE_TYPE_MAX,
													 SCHEDULE_TYPE_DEFAULT,
													 &nm_SCHEDULE_set_schedule_type,
													 &pgroup->changes_to_send_to_master,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_schedule_type_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * This function uses the provided pointer to reach the mow day member and 
 * retrieve and return its value. A test is made that the pointer is valid. And 
 * a range check is made on the retrieved value before it is returned.
 * 
 * @mutex_requirements Calling function must have the
 * list_program_data_recursive_MUTEX mutex to ensure the list doesn't change
 * during this process.
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the TD CHECK task on a 1 Hz
 * basis when the task checks for a start time.
 * 
 * @param pgroup A pointer to the group.
 * 
 * @return UNS_32 The mow day of the specified group.
 *
 * @author 4/15/2013 Adrianusv
 *
 * @revisions (none)
 */
extern UNS_32 SCHEDULE_get_mow_day( STATION_GROUP_STRUCT *const pgroup )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_64_bit_change_bits_group( pgroup,
													 &pgroup->mow_day,
													 SCHEDULE_MOWDAY_MIN,
													 SCHEDULE_MOWDAY_MAX,
													 SCHEDULE_MOWDAY_DEFAULT,
													 &nm_SCHEDULE_set_mow_day,
													 &pgroup->changes_to_send_to_master,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_mow_day_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
static void ripple_begin_date_when_irrigating_every_third_day_or_greater( STATION_GROUP_STRUCT *const pgroup,  const UNS_32	pdate, const UNS_32	pnumber_of_days_in_schedule, UNS_32	*new_begin_date, CHANGE_BITS_64_BIT	*pchange_bitfield_to_set, const UNS_32 paction_to_take )
{
	do
	{
		// 7/3/2015 ajv : Continue to increment the next begin date by the number of days in the
		// schedule until we either hit or cross today.
		*new_begin_date += pnumber_of_days_in_schedule;

	} while( *new_begin_date < pdate );

	if( (paction_to_take == SCHEDULE_BEGIN_DATE_ACTION_update_it_but_dont_save_it_to_the_file) || (paction_to_take == SCHEDULE_BEGIN_DATE_ACTION_advance_it_and_save_it_to_the_file) )
	{
		// 7/10/2015 ajv : And now, since the begin date was in the past and we've corrected it,
		// save the change to the file. We're doing this to prevent having to perform this action
		// for every station assigned to this group.
		nm_SCHEDULE_set_begin_date( pgroup, *new_begin_date, CHANGE_do_not_generate_change_line, CHANGE_REASON_HIT_A_STARTTIME, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, pchange_bitfield_to_set );
	}
}

/* ---------------------------------------------------------- */
/**
 * Investigates the schedule using the provided pointer. A test is made that the pointer is
 * valid.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex to ensure
 * the list doesn't change during this process.
 *
 * @executed Executed within the context of the TD check task on a 1 Hz basis  when the task
 * checks for a start time.
 * 
 * @author 9/20/2011 BobD
 */
BOOL_32 SCHEDULE_does_it_irrigate_on_this_date(	STATION_GROUP_STRUCT *const 			pgroup,
												const DATE_TIME_COMPLETE_STRUCT *const	pDTCS,
												const UNS_32							paction_to_take )
{
	CHANGE_BITS_64_BIT	*lbitfield_of_changes;

	UNS_32	lday, lmonth, lyear, ldow;

	// 7/9/2015 ajv : If pallowed_to_update_begin_date is true, we're going to move the begin
	// date and save it to the file, replacing the existing begin date. This variable is used to
	// set that new date.
	UNS_32	new_begin_date;

	BOOL_32	rv;

	// ----------

	rv = (false);

	// 7/3/2015 ajv : Default the next begin date to what the current begin date is set to. In
	// the event it's in the past (i.e., we missed a start time due to a power fail), we're
	// going to adjust it and save it to the file.
	new_begin_date = pgroup->begin_date;

	// 11/19/2014 ajv : Since we may be updating the begin date, get the appropriate change bits
	// field.
	lbitfield_of_changes = STATION_GROUP_get_change_bits_ptr( pgroup, CHANGE_REASON_HIT_A_STARTTIME );

	// ----------
	
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// 2012.02.07 ajv - Verify the list and the group are both valid and the group is a member
	// of the list.
	if( nm_OnList(&station_group_group_list_hdr, pgroup) == (true) )
	{
		switch( pgroup->schedule_type )
		{
			case SCHEDULE_TYPE_EVERY_DAY:
				rv = (true);

				// 7/3/2015 ajv : Set the next scheduled irrigation date to the next day since we run every
				// day. Note that we do this regardless of whether pallowed_to_update_begin_date is true or
				// not, but we only save it to the file if it's TRUE.
				new_begin_date = (pDTCS->date_time.D + 1);
				break;

			case SCHEDULE_TYPE_EVEN_DAYS:
				if( (pDTCS->__day % 2) == 0 )
				{
					rv = (true);

					// 7/3/2015 ajv : Find the next even day at set the next scheduled irrigation date to that.
					// However, only do so if we're allowed to update the begin date because there's no point in
					// looping through the days if we're not going to do anything with it.
					if( paction_to_take == SCHEDULE_BEGIN_DATE_ACTION_advance_it_and_save_it_to_the_file )
					{
						do
						{
							new_begin_date += 1;

							DateToDMY( new_begin_date, &lday, &lmonth, &lyear, &ldow );

						} while( (lday % 2) != 0 );
					}
				}
				break;

			case SCHEDULE_TYPE_ODD_DAYS:
				if( (pDTCS->__day % 2) == 1 )
				{
					// 7/3/2015 ajv : If it's the first of the month and the previous month ended on an odd day,
					// check to see whether the user has indicated to irrigate the 1st or not.
					if( (pDTCS->__day == 1) &&
						((pDTCS->__month == 1) || (pDTCS->__month == 2) ||
						 (pDTCS->__month == 4) || (pDTCS->__month == 6) ||
						 (pDTCS->__month == 8) || (pDTCS->__month == 9) ||
						 (pDTCS->__month == 11)) )
					{
						if( pgroup->irrigate_on_29th_or_31st_bool )
						{
							rv = (true);
						}
					}
					else if( (IsLeapYear(pDTCS->__year) == (true)) && (pDTCS->__day == 1) && (pDTCS->__month == 3) )
					{
						if( pgroup->irrigate_on_29th_or_31st_bool == (true) )
						{
							rv = (true);
						}
					}
					else
					{
						rv = (true);
					}
				}

				// 7/3/2015 ajv : Since we've determined today is a water day, we need to update the next
				// scheduled irrigation date to the next odd date. However, only do so if we're allowed to
				// update the begin date because there's no point in looping through the days if we're not
				// going to do anything with it.
				if( (rv) && (paction_to_take == SCHEDULE_BEGIN_DATE_ACTION_advance_it_and_save_it_to_the_file) )
				{
					// 7/3/2015 ajv : Find the next odd day and set the next scheduled irrigation date to that. 
					do
					{
						new_begin_date += 1;

						DateToDMY( new_begin_date, &lday, &lmonth, &lyear, &ldow );

					} while( (lday % 2) != 1 );


					// 7/3/2015 ajv : Check to see if the new begin date is the first of the month and whether
					// we're allowed to irrigate on the first or not. If we're not, we need to move the begin
					// date ahead to the 3rd of the month.
					if( (lday == 1) &&
						((lmonth == 1) || (lmonth == 2) ||
						 (lmonth == 4) || (lmonth == 6) ||
						 (lmonth == 8) || (lmonth == 9) ||
						 (lmonth == 11)) )
					{
						if( pgroup->irrigate_on_29th_or_31st_bool )
						{
							// 7/9/2015 ajv : Since it's the 1st and we're not allowed to irrigate, skip ahead two days
							// to reach the 3rd.
							new_begin_date += 2;
						}
					}
					else if( (IsLeapYear(lyear) == (true)) && (lday == 1) && (lmonth == 3) )
					{
						if( pgroup->irrigate_on_29th_or_31st_bool == (true) )
						{
							// 7/9/2015 ajv : Since it's the 1st and we're not allowed to irrigate, skip ahead two days
							// to reach the 3rd.
							new_begin_date += 2;
						}
					}
				}
				break;

			case SCHEDULE_TYPE_WEEKLY_SCHEDULE:
				if( pgroup->water_days_bool[ pDTCS->__dayofweek ] )
				{
					rv = (true);

					// 7/3/2015 ajv : Find the next water day and set the next scheduled irrigation date to
					// that. However, only do so if we're allowed to update the begin date because there's no
					// point in looping through the days if we're not going to do anything with it.
					if( paction_to_take == SCHEDULE_BEGIN_DATE_ACTION_advance_it_and_save_it_to_the_file )
					{
						do
						{
							new_begin_date += 1;

							DateToDMY( new_begin_date, &lday, &lmonth, &lyear, &ldow );

						} while( !pgroup->water_days_bool[ ldow ] );
					}
				}
				break;

			case SCHEDULE_TYPE_EVERY_OTHER_DAY:
			case SCHEDULE_TYPE_EVERY_THREE_DAYS:
			case SCHEDULE_TYPE_EVERY_FOUR_DAYS:
			case SCHEDULE_TYPE_EVERY_FIVE_DAYS:
			case SCHEDULE_TYPE_EVERY_SIXTH_DAYS:
			case SCHEDULE_TYPE_EVERY_SEVEN_DAYS:
			case SCHEDULE_TYPE_EVERY_EIGHT_DAYS:
			case SCHEDULE_TYPE_EVERY_NINE_DAYS:
			case SCHEDULE_TYPE_EVERY_TEN_DAYS:
			case SCHEDULE_TYPE_EVERY_ELEVEN_DAYS:
			case SCHEDULE_TYPE_EVERY_TWELVE_DAYS:
			case SCHEDULE_TYPE_EVERY_THIRTEEN_DAYS:
			case SCHEDULE_TYPE_EVERY_FOURTEEN_DAYS:
				// 7/3/2015 ajv : First, determine whether the next scheduled start date is in the past
				// (i.e., we missed a start time due to a power fail). If so, correct it.
				if( pDTCS->date_time.D > pgroup->begin_date )
				{
					ripple_begin_date_when_irrigating_every_third_day_or_greater( pgroup, pDTCS->date_time.D, (pgroup->schedule_type - SCHEDULE_TYPE_NUMBER_OF_DAYS_OFFSET), &new_begin_date, lbitfield_of_changes, paction_to_take );
				}

				if( pDTCS->date_time.D == pgroup->begin_date )
				{
					rv = (true);

					// 7/3/2015 ajv : Increment the begin date by the number of days in the schedule.  Note that
					// we do this regardless of whether pallowed_to_update_begin_date is true or not, but we
					// only save it to the file if it's TRUE.
					new_begin_date += (pgroup->schedule_type - SCHEDULE_TYPE_NUMBER_OF_DAYS_OFFSET);
				}
				break;

			case SCHEDULE_TYPE_ONCE_EVERY_THREE_WEEKS:
				// 7/3/2015 ajv : First, determine whether the next scheduled start date is in the past
				// (i.e., we missed a start time due to a power fail). If so, correct it.
				if( pDTCS->date_time.D > pgroup->begin_date )
				{
					ripple_begin_date_when_irrigating_every_third_day_or_greater( pgroup, pDTCS->date_time.D, 21, &new_begin_date, lbitfield_of_changes, paction_to_take );
				}

				if( pDTCS->date_time.D == pgroup->begin_date )
				{
					rv = (true);

					// 7/3/2015 ajv : Increment the begin date by the number of days in the schedule.  Note that
					// we do this regardless of whether pallowed_to_update_begin_date is true or not, but we
					// only save it to the file if it's TRUE.
					new_begin_date += 21;
				}
				break;

			case SCHEDULE_TYPE_ONCE_EVERY_FOUR_WEEKS:
				// 7/3/2015 ajv : First, determine whether the next scheduled start date is in the past
				// (i.e., we missed a start time due to a power fail). If so, correct it.
				if( pDTCS->date_time.D > pgroup->begin_date )
				{
					ripple_begin_date_when_irrigating_every_third_day_or_greater( pgroup, pDTCS->date_time.D, 28, &new_begin_date, lbitfield_of_changes, paction_to_take );
				}

				if( pDTCS->date_time.D == pgroup->begin_date )
				{
					rv = (true);

					// 7/3/2015 ajv : Increment the begin date by the number of days in the schedule.  Note that
					// we do this regardless of whether pallowed_to_update_begin_date is true or not, but we
					// only save it to the file if it's TRUE.
					new_begin_date += 28;
				}
				break;

			default:
				Alert_Message( "Invalid schedule type" );
		}

		// 7/9/2015 ajv : The ONLY time we're allowed to update the begin date is AFTER we've
		// completed our check for start times. Doing so mid-process will result in stations not
		// turning on (as we saw with Richard's controller).
		if( (new_begin_date != pgroup->begin_date) && (paction_to_take == SCHEDULE_BEGIN_DATE_ACTION_advance_it_and_save_it_to_the_file) )
		{
			nm_SCHEDULE_set_begin_date( pgroup, new_begin_date, CHANGE_do_not_generate_change_line, CHANGE_REASON_HIT_A_STARTTIME, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, lbitfield_of_changes );
		}
	}
	else
	{
		Alert_item_not_on_list( pgroup, station_group_group_list_hdr );

		rv = (false);
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SCHEDULE_get_irrigate_after_29th_or_31st( STATION_GROUP_STRUCT *const pgroup )
{
	BOOL_32 rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_bool_64_bit_change_bits_group( pgroup,
												   &pgroup->irrigate_on_29th_or_31st_bool,
												   SCHEDULE_IRRIGATEON29THOR31ST_DEFAULT,
												   &nm_SCHEDULE_set_irrigate_on_29th_or_31st,
												   &pgroup->changes_to_send_to_master,
												   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_irrigate_on_29th_or_31st_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 DAILY_ET_get_et_in_use( STATION_GROUP_STRUCT *const pgroup )
{
	BOOL_32 rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_bool_64_bit_change_bits_group( pgroup,
												   &pgroup->et_in_use,
												   WEATHER_ET_MODE_DEFAULT,
												   &nm_DAILY_ET_set_ET_in_use,
												   &pgroup->changes_to_send_to_master,
												   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_et_in_use ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 RAIN_get_rain_in_use( STATION_GROUP_STRUCT *const pgroup )
{
	BOOL_32 rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_bool_64_bit_change_bits_group( pgroup,
												   &pgroup->rain_in_use_bool,
												   WEATHER_RAIN_IN_USE_DEFAULT,
												   &nm_RAIN_set_rain_in_use,
												   &pgroup->changes_to_send_to_master,
												   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_rain_in_use_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 PERCENT_ADJUST_get_percentage_100u_for_this_gid( const UNS_32 pGID, const UNS_32 pdate )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32	rv;

	// ----------
	
	// 6/25/2015 ajv : Initialize to the default in case any of the conditions fail.
	rv = PERCENT_ADJUST_PERCENT_DEFAULT_100u;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, pGID, (false) );

	if( lgroup != NULL )
	{
		if( (pdate >= lgroup->percent_adjust_start_date) && (pdate <= lgroup->percent_adjust_end_date) && (lgroup->percent_adjust_start_date != lgroup->percent_adjust_end_date) )
		{
			rv = SHARED_get_uint32_64_bit_change_bits_group( lgroup,
															 &lgroup->percent_adjust_100u,
															 PERCENT_ADJUST_PERCENT_MIN_100u,
															 PERCENT_ADJUST_PERCENT_MAX_100u,
															 PERCENT_ADJUST_PERCENT_DEFAULT_100u,
															 &nm_PERCENT_ADJUST_set_percent,
															 &lgroup->changes_to_send_to_master,
															 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_percent_adjust_bit ] );
		}
	}
	else
	{
		Alert_station_not_in_group();
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 PERCENT_ADJUST_get_remaining_days_for_this_gid( const UNS_32 pGID, const UNS_32 pdate )
{
	STATION_GROUP_STRUCT	*lgroup;

	DATE_TIME	ldt;

	UNS_32	lend_date;

	UNS_32	rv;

	// ----------
	
	// 6/25/2015 ajv : Initialize to the default of 0 days in case any of the conditions fail.
	rv = PERCENT_ADJUST_DAYS_DEFAULT;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, pGID, (false) );

	if( lgroup != NULL )
	{
		if( (pdate >= lgroup->percent_adjust_start_date) && (pdate <= lgroup->percent_adjust_end_date) )
		{
			EPSON_obtain_latest_time_and_date( &ldt );

			lend_date = SHARED_get_uint32_64_bit_change_bits_group( lgroup,
																	&lgroup->percent_adjust_end_date,
																	DATE_MINIMUM,
																	DATE_MAXIMUM,
																	ldt.D,
																	&nm_PERCENT_ADJUST_set_end_date,
																	&lgroup->changes_to_send_to_master,
																	STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_percent_adjust_end_date_bit ] );
			rv = (lend_date - pdate);

			RangeCheck_uint32( &rv, PERCENT_ADJUST_DAYS_MIN, PERCENT_ADJUST_DAYS_MAX, PERCENT_ADJUST_DAYS_DEFAULT, nm_GROUP_get_name( lgroup ), "Percent Adjust Days" );
		}
	}
	else
	{
		Alert_station_not_in_group();
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_GROUP_get_cycle_time_10u_for_this_gid( const UNS_32 pGID )
{
	STATION_GROUP_STRUCT	*lgroup;

	float	calculated_cycle_time;

	UNS_32	rv;

	// ----------
	
	rv = STATION_CYCLE_MINUTES_DEFAULT_10u;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, pGID, (false) );

	if( lgroup != NULL )
	{
		calculated_cycle_time = WATERSENSE_get_cycle_time( STATION_GROUP_get_allowable_surface_accumulation( lgroup ),
														   STATION_GROUP_get_soil_intake_rate( lgroup ),
														   STATION_GROUP_get_precip_rate_in_per_hr( lgroup ) );

		rv = (UNS_32)(calculated_cycle_time * 10);
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_GROUP_get_soak_time_for_this_gid( const UNS_32 pGID, const UNS_32 pdistribution_uniformity_100u )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32	rv;

	// ----------
	
	rv = STATION_SOAK_MINUTES_DEFAULT;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, pGID, (false) );

	if( lgroup != NULL )
	{
		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, pGID, (false) );

		if( lgroup != NULL )
		{
			rv = (UNS_32)WATERSENSE_get_soak_time__cycle_end_to_cycle_start( STATION_GROUP_get_allowable_surface_accumulation( lgroup ),
												   STATION_GROUP_get_soil_intake_rate( lgroup ),
												   STATION_GROUP_get_precip_rate_in_per_hr( lgroup ),
												   pdistribution_uniformity_100u );
		}
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_GROUP_get_station_precip_rate_in_per_hr_100000u( STATION_STRUCT *const pstation )
{
	UNS_32		rv;

	// 2011.08.31 ajv - If the station's not in the group or the group is
	// invalid, return the default value.
	rv = STATION_GROUP_PRECIP_RATE_DEFAULT_100000u;

	// To ensure the group isn't deleted between retrieving and copying the
	// precip rate setting, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( pstation != NULL )
	{
		UNS_32		lgroup_ID;

		lgroup_ID = STATION_get_GID_station_group( pstation );

		// If the station has a GID of 0, which means it's not assigned to a group
		// yet, return the default value.
		if( lgroup_ID > 0 )
		{
			STATION_GROUP_STRUCT	*lgroup;

			lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, lgroup_ID, (false) );

			rv = SHARED_get_uint32_64_bit_change_bits_group( lgroup,
															 &lgroup->precip_rate_in_100000u,
															 STATION_GROUP_PRECIP_RATE_MIN_100000u,
															 STATION_GROUP_PRECIP_RATE_MAX_100000u,
															 STATION_GROUP_PRECIP_RATE_DEFAULT_100000u,
															 &nm_STATION_GROUP_set_precip_rate,
															 &lgroup->changes_to_send_to_master,
															 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_precip_rate_bit ] );
		}
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_GROUP_get_station_crop_coefficient_100u( STATION_STRUCT *const pstation, const UNS_32 pmonth )
{
	STATION_GROUP_STRUCT	*lgroup;

	char	lfield_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32	lgroup_ID;

	UNS_32	rv;

	// 2011.08.31 ajv - If the station's not in the group or the group is invalid, return the
	// default value.
	rv = STATION_GROUP_CROP_COEFFICIENT_DEFAULT_100u;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( pstation != NULL )
	{
		lgroup_ID = STATION_get_GID_station_group( pstation );

		// If the station has a GID of 0, which means it's not assigned to a group yet, return the
		// default value.
		if( lgroup_ID > 0 )
		{
			lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, lgroup_ID, (false) );

			if( (pmonth - 1) < MONTHS_IN_A_YEAR )
			{
				snprintf( lfield_name, sizeof(lfield_name), "%s%d", STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_crop_coefficient_bit ], pmonth );

				rv = SHARED_get_uint32_from_array_64_bit_change_bits_group( lgroup,
																			&lgroup->crop_coefficient_100u[ (pmonth-1) ],
																			(pmonth - 1),
																			MONTHS_IN_A_YEAR,
																			STATION_GROUP_CROP_COEFFICIENT_MIN_100u,
																			STATION_GROUP_CROP_COEFFICIENT_MAX_100u,
																			STATION_GROUP_CROP_COEFFICIENT_DEFAULT_100u,
																			&nm_STATION_GROUP_set_crop_coefficient,
																			&lgroup->changes_to_send_to_master,
																			lfield_name );
			}
			else
			{
				Alert_index_out_of_range();

				rv = STATION_GROUP_CROP_COEFFICIENT_DEFAULT_100u;
			}
		}
	}
	else
	{
		Alert_func_call_with_null_ptr();
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_GROUP_get_crop_coefficient_100u( UNS_32 pGID, UNS_32 pmonth )
{
		STATION_GROUP_STRUCT	*lgroup;

	char	lfield_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32	rv;

	// 2011.08.31 ajv - If the station's not in the group or the group is invalid, return the
	// default value.
	rv = STATION_GROUP_CROP_COEFFICIENT_DEFAULT_100u;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// If the station has a GID of 0, which means it's not assigned to a group yet, return the
	// default value.
	if( pGID > 0 )
	{
		lgroup = STATION_GROUP_get_group_with_this_GID( pGID );

		if( (pmonth - 1) < MONTHS_IN_A_YEAR )
		{
			snprintf( lfield_name, sizeof(lfield_name), "%s%d", STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_crop_coefficient_bit ], pmonth );

			rv = SHARED_get_uint32_from_array_64_bit_change_bits_group( lgroup,
																		&lgroup->crop_coefficient_100u[ (pmonth-1) ],
																		(pmonth - 1),
																		MONTHS_IN_A_YEAR,
																		STATION_GROUP_CROP_COEFFICIENT_MIN_100u,
																		STATION_GROUP_CROP_COEFFICIENT_MAX_100u,
																		STATION_GROUP_CROP_COEFFICIENT_DEFAULT_100u,
																		&nm_STATION_GROUP_set_crop_coefficient,
																		&lgroup->changes_to_send_to_master,
																		lfield_name );
		}
		else
		{
			Alert_index_out_of_range();

			rv = STATION_GROUP_CROP_COEFFICIENT_DEFAULT_100u;
		}
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_GROUP_get_station_usable_rain_percentage_100u( STATION_STRUCT *const pstation )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32		lgroup_ID;

	UNS_32		rv;

	// 2011.08.31 ajv - If the station's not in the group or the group is
	// invalid, return the default value.
	rv = STATION_GROUP_USABLE_RAIN_PERCENTAGE_DEFAULT;

	// To ensure the group isn't deleted between retrieving and copying the
	// precip rate setting, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( pstation != NULL )
	{
		lgroup_ID = STATION_get_GID_station_group( pstation );

		// If the station has a GID of 0, which means it's not assigned to a group
		// yet, return the default value.
		if( lgroup_ID > 0 )
		{
			lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, lgroup_ID, (false) );

			rv = SHARED_get_uint32_64_bit_change_bits_group( lgroup,
															 &lgroup->usable_rain_percentage_100u,
															 STATION_GROUP_USABLE_RAIN_PERCENTAGE_MIN,
															 STATION_GROUP_USABLE_RAIN_PERCENTAGE_MAX,
															 STATION_GROUP_USABLE_RAIN_PERCENTAGE_DEFAULT,
															 &nm_STATION_GROUP_set_usable_rain,
															 &lgroup->changes_to_send_to_master,
															 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_usable_rain_bit ] );
		}
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the priority level associated with a particular station. If the station
 * isn't assigned to any group, an alert is generated and the default value is
 * returned.
 * 
 * @executed Executed within the context of the TD check task as part of
 *           building the ilc's. 
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 The priority level of the station.
 *
 * @author 7/21/2011 Adrianusv
 *
 * @revisions
 *    7/21/2011 Initial release
 *    3/27/2012 Fixed range check since LOW is now 1 and HIGH is now 3.
 */
extern UNS_32 PRIORITY_get_station_priority_level( STATION_STRUCT *const pstation )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32	lGID;

	UNS_32	rv;

	// To ensure the group isn't deleted between retrieving and copying the
	// priority level, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lGID = STATION_get_GID_station_group( pstation );

	// If the station has a GID of 0, which means it's not assigned to a group
	// yet, return the default value.
	if( lGID > 0 )
	{
		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, lGID, (false) );

		rv = SHARED_get_uint32_64_bit_change_bits_group( lgroup,
														 &lgroup->priority_level,
														 PRIORITY_LOW,
														 PRIORITY_HIGH,
														 PRIORITY_LEVEL_DEFAULT,
														 &nm_PRIORITY_set_priority,
														 &lgroup->changes_to_send_to_master,
														 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_priority_level_bit ] );
	}
	else
	{
		rv = PRIORITY_LEVEL_DEFAULT;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the percent adjust percentage associated with a particular station. 
 * for a particular day. If the station isn't assigned to any group, an alert is
 * generated and the default value is returned. 
 * 
 * @executed Executed within the context of the TD check task as part of
 *           building the ilc's. 
 * 
 * @param pstation A pointer to the station. 
 * @param pdate The current date and time.
 * 
 * @return UNS_32 The percent adjust percentage of the station.
 *
 * @author 9/12/2011 Adrianusv
 *
 * @revisions
 *    9/12/2011 Initial release
 */
extern UNS_32 PERCENT_ADJUST_get_station_percentage_100u( STATION_STRUCT *const pstation, const UNS_32 pdate )
{
	UNS_32	lGID;

	UNS_32	rv;

	// To ensure the group isn't deleted between retrieving and copying the
	// setting, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lGID = STATION_get_GID_station_group( pstation );

	// If the station has a GID of 0, which means it's not assigned to a group
	// yet, return the default value.
	if( lGID > 0 )
	{
		rv = PERCENT_ADJUST_get_percentage_100u_for_this_gid( lGID, pdate );
	}
	else
	{
		rv = PERCENT_ADJUST_PERCENT_DEFAULT_100u;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the number of days remaining for the specified station's percent
 * adjust group.
 * 
 * @executed Executed within the context of the Display Processing task when
 *           drawing the Station Information screen.
 * 
 * @param pstation A pointer to the station.
 * @param pdate The current date.
 * 
 * @return UNS_32 The number of days between the passed in date and the percent
 *                adjust end date.
 *
 * @author 7/12/2012 Adrianusv
 *
 * @revisions
 *    7/12/2012 Initial release
 */
extern UNS_32 PERCENT_ADJUST_get_station_remaining_days( STATION_STRUCT *const pstation, const UNS_32 pdate )
{
	UNS_32	lGID;

	UNS_32	rv;

	// To ensure the group isn't deleted between retrieving and copying the
	// setting, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lGID = STATION_get_GID_station_group( pstation );

	// If the station has a GID of 0, which means it's not assigned to a group
	// yet, return 0.
	if( lGID > 0 )
	{
		rv = PERCENT_ADJUST_get_remaining_days_for_this_gid( lGID, pdate );
	}
	else
	{
		rv = PERCENT_ADJUST_DAYS_DEFAULT;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the schedule type of a particular station's schedule. If the station
 * isn't assigned to any group, an alert is generated and the default value is
 * returned.
 * 
 * @executed 
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 The schedule type of the schedule for the specified station.
 *
 * @author 7/21/2011 Adrianusv
 *
 * @revisions
 *    7/21/2011 Initial release
 *    8/31/2011 Forced the return value to the setting's default if the station
 *              is not assigned to a group.
 */
extern UNS_32 SCHEDULE_get_station_schedule_type( STATION_STRUCT *const pstation )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32		lGID;

	UNS_32		rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lGID = STATION_get_GID_station_group( pstation );

	// If the station has a GID of 0, which means it's not assigned to a group
	// yet, return the default value.
	if( lGID > 0 )
	{
		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, lGID, (false) );

		rv = SCHEDULE_get_schedule_type( lgroup );
	}
	else
	{
		rv = SCHEDULE_TYPE_DEFAULT;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets whether the specified day is a water day for a particular station's
 * schedule. If the station isn't assigned to any group, an alert is generated
 * and the default value is returned.
 * 
 * @executed 
 * 
 * @param pstation A pointer to the station.
 * @param pday The day
 * 
 * @return BOOL_32 True if the specified day is a water day for the specified
 *               station's schedule; otherwise, false.
 *
 * @author 7/21/2011 Adrianusv
 *
 * @revisions
 *    7/21/2011 Initial release
 *    8/31/2011 Forced the return value to the setting's default if the station
 *              is not assigned to a group.
 */
extern BOOL_32 SCHEDULE_get_station_waters_this_day( STATION_STRUCT *const pstation, const UNS_32 pday )
{
	STATION_GROUP_STRUCT	*lgroup;

	char	lfield_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32		lGID;

	BOOL_32		rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lGID = STATION_get_GID_station_group( pstation );

	// If the station has a GID of 0, which means it's not assigned to a group
	// yet, return the default value.
	if( lGID > 0 )
	{
		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, lGID, (false) );

		if( pday < DAYS_IN_A_WEEK )
		{
			snprintf( lfield_name, sizeof(lfield_name), "%s%s", STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_water_days_bit ], GetDayShortStr( pday ) );

			rv = SHARED_get_bool_from_array_64_bit_change_bits_group( lgroup,
																	  &lgroup->water_days_bool[ pday ],
																	  pday,
																	  DAYS_IN_A_WEEK,
																	  SCHEDULE_WATERDAY_DEFAULT,
																	  &nm_SCHEDULE_set_water_day,
																	  &lgroup->changes_to_send_to_master,
																	  lfield_name );
		}
		else
		{
			Alert_index_out_of_range();

			rv = SCHEDULE_WATERDAY_DEFAULT;
		}
	}
	else
	{
		rv = SCHEDULE_WATERDAY_DEFAULT;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SCHEDULE_get_run_time_calculation_support( STATION_GROUP_STRUCT *psgs_ptr, RUN_TIME_CALC_SUPPORT_STRUCT *psupport_struct_ptr )
{
	BOOL_32		rv;
	
	// 4/5/2016 rmd : If not on the list return (false) indicating to the caller do not use
	// results.
	rv = (false);
	
	// ----------
	
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( nm_OnList( &station_group_group_list_hdr, psgs_ptr ) )
	{
		// 4/5/2016 rmd : So the station group pointer is valid. Use it!

		psupport_struct_ptr->schedule_enabled = psgs_ptr->schedule_enabled_bool;

		psupport_struct_ptr->schedule_type = psgs_ptr->schedule_type;
		
		memcpy( psupport_struct_ptr->wday, psgs_ptr->water_days_bool, sizeof(psgs_ptr->water_days_bool) );

		psupport_struct_ptr->irrigates_after_29th_or_31st = psgs_ptr->irrigate_on_29th_or_31st_bool;
		
		psupport_struct_ptr->uses_et_averaging = psgs_ptr->use_et_averaging_bool;
		
		// 4/5/2016 rmd : And get the start time.
		psupport_struct_ptr->sx = psgs_ptr->start_time;
		
		rv = (true);
	}
	else
	{
		// 4/7/2016 rmd : Setting a bunch of defaults is not really needed as the caller should not
		// use the structure content if we return (false).
	}
	
	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the method to calculate run times for a particular station.
 * 
 * @executed Executed within the context of the TD check task as part of 
 *  		 building the ilc's.
 * 
 * @param pstation A pointer to the station.
 * 
 * @return BOOL_32 True if the station uses ET; otherwise, false.
 *
 * @author 7/21/2011 Adrianusv
 *
 * @revisions
 *    7/21/2011 Initial release
 */
extern UNS_32 WEATHER_get_station_uses_daily_et( STATION_STRUCT *const pstation )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32		lGID;

	BOOL_32		rv;

	// To ensure the group isn't deleted between retrieving and copying the
	// daily ET setting, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lGID = STATION_get_GID_station_group( pstation );

	// If the station has a GID of 0, which means it's not assigned to a group
	// yet, return the default value.
	if( lGID > 0 )
	{
		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, lGID, (false) );

		rv = DAILY_ET_get_et_in_use( lgroup );
	}
	else
	{
		rv = (false);
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets whether ET averaging is enabled for a particular station or not.
 * 
 * @executed Executed within the context of the TD check task as part of 
 *  		 building the ilc's.
 * 
 * @param pstation A pointer to the station.
 * 
 * @return BOOL_32 True if ET averaging is in use for the station; otherwise,
 *               false.
 *
 * @author 7/9/2012 Adrianusv
 *
 * @revisions
 *    7/9/2012 Initial release
 */
extern BOOL_32 WEATHER_get_station_uses_et_averaging( STATION_STRUCT *const pstation )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32	lGID;

	BOOL_32	rv;

	// To ensure the group isn't deleted between retrieving and copying the
	// daily ET setting, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lGID = STATION_get_GID_station_group( pstation );

	// If the station has a GID of 0, which means it's not assigned to a group
	// yet, return the default value.
	if( lGID > 0 )
	{
		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, lGID, (false) );

		if( lgroup != NULL )
		{
			// If the user is running in minutes, always return false.
			if( lgroup->et_in_use == (true) )
			{
				rv = SHARED_get_bool_64_bit_change_bits_group( lgroup,
															   &lgroup->use_et_averaging_bool,
															   WEATHER_ET_AVERAGING_IN_USE_DEFAULT,
															   &nm_DAILY_ET_set_use_ET_averaging,
															   &lgroup->changes_to_send_to_master,
															   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_use_et_averaging ] );
			}
			else
			{
				rv = (false);
			}
		}
		else
		{
			Alert_station_not_in_group();

			rv = WEATHER_ET_AVERAGING_IN_USE_DEFAULT;
		}
	}
	else
	{
		rv = WEATHER_ET_AVERAGING_IN_USE_DEFAULT;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the whether the rain is in use or not for a particular station. If the
 * station isn't assigned to any group, an alert is generated and the default
 * value is returned.
 * 
 * @executed Executed within the context of the TD check task as part of
 *           building the ilc's. 
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 True if the rain is in use for the station; otherwise, false.
 *
 * @author 7/21/2011 Adrianusv
 *
 * @revisions
 *    7/21/2011 Initial release
 */
extern BOOL_32 WEATHER_get_station_uses_rain( STATION_STRUCT *const pstation )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32	lGID;

	BOOL_32	rv;

	// To ensure the group isn't deleted between retrieving and copying the
	// priority level, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lGID = STATION_get_GID_station_group( pstation );

	// If the station has a GID of 0, which means it's not assigned to a group
	// yet, return false.
	if( lGID > 0 )
	{
		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, lGID, (false) );

		rv = RAIN_get_rain_in_use( lgroup );
	}
	else
	{
		rv = (false);
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the maximum amount of rain allowed for a particular station. If the
 * station isn't assigned to any group, an alert is generated and the default
 * value is returned.
 * 
 * @executed Executed within the context of the TD check task as part of
 *           building the ilc's. 
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 The maximum amount of rain allowed to build up.
 *
 * @author 7/21/2011 Adrianusv
 *
 * @revisions
 *    7/21/2011 Initial release
 */
extern UNS_32 STATION_GROUP_get_soil_storage_capacity_inches_100u( STATION_STRUCT *const pstation )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32	lGID;

	UNS_32	rv;

	// To ensure the group isn't deleted between retrieving and copying the
	// priority level, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lGID = STATION_get_GID_station_group( pstation );

	// If the station has a GID of 0, which means it's not assigned to a group
	// yet, return the default.
	if( lGID > 0 )
	{
		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, lGID, (false) );
		
		rv = SHARED_get_uint32_64_bit_change_bits_group( lgroup,
														 &lgroup->soil_storage_capacity_inches_100u,
														 STATION_GROUP_SOIL_STORAGE_CAPACITY_MIN_100u,
														 STATION_GROUP_SOIL_STORAGE_CAPACITY_MAX_100u,
														 STATION_GROUP_SOIL_STORAGE_CAPACITY_DEFAULT_100u,
														 &nm_STATION_GROUP_set_soil_storage_capacity,
														 &lgroup->changes_to_send_to_master,
														 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_soil_storage_capacity_bit ] );
	}
	else
	{
		rv = STATION_GROUP_SOIL_STORAGE_CAPACITY_DEFAULT_100u;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the whether the wind is in use or not for a particular station. If the
 * station isn't assigned to any group, an alert is generated and the default
 * value is returned.
 * 
 * @executed Executed within the context of the TD check task as part of
 *           building the ilc's. 
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 True if the wind is in use for the station; otherwise, false.
 *
 * @author 7/21/2011 Adrianusv
 *
 * @revisions
 *    7/21/2011 Initial release
 */
extern BOOL_32 WEATHER_get_station_uses_wind( STATION_STRUCT *const pstation )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32	lGID;

	BOOL_32	rv;

	// To ensure the group isn't deleted between retrieving and copying the
	// priority level, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lGID = STATION_get_GID_station_group( pstation );

	// If the station has a GID of 0, which means it's not assigned to a group
	// yet, return the default value.
	if( lGID > 0 )
	{
		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, lGID, (false) );

		rv = SHARED_get_bool_64_bit_change_bits_group( lgroup,
													   &lgroup->wind_in_use_bool,
													   WEATHER_WIND_IN_USE_DEFAULT,
													   &nm_WIND_set_wind_in_use,
													   &lgroup->changes_to_send_to_master,
													   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_wind_in_use_bit ] );
	}
	else
	{
		rv = WEATHER_WIND_IN_USE_DEFAULT;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the high flow alert action associated with a particular station.
 * 
 * @executed Executed within the context of the TD check task as part of
 *           building the ilc's.
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 The high flow alert action of the station.
 *
 * @author 7/21/2011 Adrianusv
 *
 * @revisions
 *    7/21/2011 Initial release
 */
extern UNS_32 ALERT_ACTIONS_get_station_high_flow_action( STATION_STRUCT *const pstation )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32	lGID;

	UNS_32	rv;

	// To ensure the group isn't deleted between retrieving and copying the
	// high flow action, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lGID = STATION_get_GID_station_group( pstation );

	// If the station has a GID of 0, which means it's not assigned to a group
	// yet, return the default value.
	if( lGID > 0 )
	{
		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, lGID, (false) );

		rv = SHARED_get_uint32_64_bit_change_bits_group( lgroup,
														 &lgroup->high_flow_action,
														 ALERT_ACTIONS_NONE,
														 ALERT_ACTIONS_ALERT_SHUTOFF,
														 ALERT_ACTIONS_HIGH_DEFAULT,
														 &nm_ALERT_ACTIONS_set_high_flow_action,
														 &lgroup->changes_to_send_to_master,
														 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_high_flow_action_bit ] );
	}
	else
	{
		rv = ALERT_ACTIONS_HIGH_DEFAULT;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the low flow alert action associated with a particular station.
 * 
 * @executed Executed within the context of the TD check task as part of
 *           building the ilc's.
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 The low flow alert action of the station.
 *
 * @author 7/21/2011 Adrianusv
 *
 * @revisions
 *    7/21/2011 Initial release
 */
extern UNS_32 ALERT_ACTIONS_get_station_low_flow_action( STATION_STRUCT *const pstation )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32	lGID;

	UNS_32	rv;

	// To ensure the group isn't deleted between retrieving and copying the
	// low flow action, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lGID = STATION_get_GID_station_group( pstation );

	// If the station has a GID of 0, which means it's not assigned to a group
	// yet, return the default value.
	if( lGID > 0 )
	{
		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, lGID, (false) );

		rv = SHARED_get_uint32_64_bit_change_bits_group( lgroup,
														 &lgroup->low_flow_action,
														 ALERT_ACTIONS_NONE,
														 ALERT_ACTIONS_ALERT_SHUTOFF,
														 ALERT_ACTIONS_LOW_DEFAULT,
														 &nm_ALERT_ACTIONS_set_low_flow_action,
														 &lgroup->changes_to_send_to_master,
														 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_low_flow_action_bit ] );
	}
	else
	{
		rv = ALERT_ACTIONS_LOW_DEFAULT;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the whether the pump is in use or not for a particular station. If the
 * station isn't assigned to any group, an alert is generated and the default
 * value is returned.
 * 
 * @executed Executed within the context of the TD check task as part of
 *           building the ilc's. 
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 True if the pump is in use for the station; otherwise, false.
 *
 * @author 7/21/2011 Adrianusv
 *
 * @revisions
 *    7/21/2011 Initial release
 */
extern BOOL_32 PUMP_get_station_uses_pump( STATION_STRUCT *const pstation )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32	lGID;

	BOOL_32	rv;

	// To ensure the group isn't deleted between retrieving and copying the
	// priority level, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lGID = STATION_get_GID_station_group( pstation );

	// If the station has a GID of 0, which means it's not assigned to a group
	// yet, return false.
	if( lGID > 0 )
	{
		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, lGID, (false) );

		rv = SHARED_get_bool_64_bit_change_bits_group( lgroup,
													   &lgroup->pump_in_use,
													   PUMP_INUSE_DEFAULT,
													   &nm_PUMP_set_pump_in_use,
													   &lgroup->changes_to_send_to_master,
													   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_pump_in_use_bit ] );
	}
	else
	{
		rv = PUMP_INUSE_DEFAULT;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the line fill seconds associated with a particular station. If the
 * station isn't assigned to any group, an alert is generated and the default
 * value is returned.
 * 
 * @executed Executed within the context of the TD check task as part of
 *           building the ilc's. 
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 The line fill seconds of the station.
 *
 * @author 12/6/2011 BobD
 *
 * @revisions
 *    12/6/2011 Initial release
 */
extern UNS_32 LINE_FILL_TIME_get_station_line_fill_seconds( STATION_STRUCT *const pstation )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32	lGID;

	UNS_32	rv;

	// To ensure the group isn't deleted between retrieving and copying the
	// setting, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lGID = STATION_get_GID_station_group( pstation );

	// If the station has a GID of 0, which means it's not assigned to a group
	// yet, return the default value.
	if( lGID > 0 )
	{
		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, lGID, (false) );

		rv = SHARED_get_uint32_64_bit_change_bits_group( lgroup,
														 &lgroup->line_fill_time_sec,
														 LINE_FILL_TIME_MIN,
														 LINE_FILL_TIME_MAX,
														 LINE_FILL_TIME_DEFAULT,
														 &nm_LINE_FILL_TIME_set_line_fill_time,
														 &lgroup->changes_to_send_to_master,
														 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_line_fill_time_bit ] );
	}
	else
	{
		rv = LINE_FILL_TIME_DEFAULT;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the valve close seconds associated with a particular station. If the
 * station isn't assigned to any group, an alert is generated and the default
 * value is returned.
 * 
 * @executed Executed within the context of the TD check task as part of
 *           building the ilc's. 
 * 
 * @param pstation A pointer to the station.
 * 
 * @return UNS_32 The valve close seconds of the station.
 *
 * @author 12/6/2011 BobD
 *
 * @revisions
 *    12/6/2011 Initial release
 */
extern UNS_32 DELAY_BETWEEN_VALVES_get_station_slow_closing_valve_seconds( STATION_STRUCT *const pstation )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32	lGID;

	UNS_32	rv;

	// To ensure the group isn't deleted between retrieving and copying the
	// setting, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lGID = STATION_get_GID_station_group( pstation );

	// If the station has a GID of 0, which means it's not assigned to a group
	// yet, return the default value.
	if( lGID > 0 )
	{
		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, lGID, (false) );

		rv = SHARED_get_uint32_64_bit_change_bits_group( lgroup,
														 &lgroup->delay_between_valve_time_sec,
														 DELAY_BETWEEN_VALVES_MIN,
														 DELAY_BETWEEN_VALVES_MAX,
														 DELAY_BETWEEN_VALVES_DEFAULT,
														 &nm_DELAY_BETWEEN_VALVES_set_delay_time,
														 &lgroup->changes_to_send_to_master,
														 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_delay_between_valves_bit ] );
	}
	else
	{
		rv = DELAY_BETWEEN_VALVES_DEFAULT;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 ACQUIRE_EXPECTEDS_get_station_acquire_expected_flow( STATION_STRUCT *const pstation )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32	lGID;

	BOOL_32	rv;

	// To ensure the group isn't deleted between retrieving and copying the
	// priority level, take the list_program_data_recursive_MUTEX mutex.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lGID = STATION_get_GID_station_group( pstation );

	// If the station has a GID of 0, which means it's not assigned to a group
	// yet, return false.
	if( lGID > 0 )
	{
		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, lGID, (false) );

		rv = SHARED_get_bool_64_bit_change_bits_group( lgroup,
													   &lgroup->acquire_expecteds,
													   ACQUIRE_EXPECTEDS_DEFAULT,
													   &nm_ACQUIRE_EXPECTEDS_set_acquire_expected,
													   &lgroup->changes_to_send_to_master,
													   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_acquire_at_start_bit ] );
	}
	else
	{
		rv = ACQUIRE_EXPECTEDS_DEFAULT;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_GROUP_get_GID_irrigation_system_for_this_station( STATION_STRUCT *const pstation )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// 9/28/2015 ajv : Suppress the "group not found" alert in case the station isn't assigned
	// to a group yet because, in that case, we still want to use the default mainline.
	lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, STATION_get_GID_station_group( pstation ), (true) );

	// 9/28/2015 ajv : If the station isn't assigned to a station group yet, we have an issue
	// because that also means there's no mainline assignment either. For now, we'll ovecome
	// this by simply using the mainline which is assigned to the first station group. Not
	// ideal, but we'll work up a better solution later.
	if( lgroup == NULL )
	{
		lgroup = nm_ListGetFirst( &station_group_group_list_hdr );
	}

	rv = SHARED_get_uint32_64_bit_change_bits_group( lgroup,
													 &lgroup->GID_irrigation_system,
													 1,
													 SYSTEM_get_last_group_ID(),
													 nm_GROUP_get_group_ID( SYSTEM_get_group_at_this_index( 0 ) ),
													 nm_STATION_GROUP_set_GID_irrigation_system,
													 &lgroup->changes_to_send_to_master,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_GID_irrigation_system_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_GROUP_get_GID_irrigation_system( STATION_GROUP_STRUCT *const pgroup )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// 3/10/2016 rmd : There is a NULL test made on pgroup in the following generic get
	// function.
	rv = SHARED_get_uint32_64_bit_change_bits_group( pgroup,
													 &pgroup->GID_irrigation_system,
													 1,
													 SYSTEM_get_last_group_ID(),
													 nm_GROUP_get_group_ID( SYSTEM_get_group_at_this_index( 0 ) ),
													 nm_STATION_GROUP_set_GID_irrigation_system,
													 &pgroup->changes_to_send_to_master,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_GID_irrigation_system_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_GROUP_get_budget_reduction_limit( STATION_GROUP_STRUCT *const lgroup )
{
	UNS_32	rv;

	rv = 0;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );
	
	if( lgroup != NULL )
	{

		// 3/10/2016 rmd : There is a NULL test made on pgroup in the following generic get
		// function.
		rv = SHARED_get_uint32_64_bit_change_bits_group( lgroup,
														 &lgroup->budget_reduction_percentage_cap,
														 STATION_GROUP_BUDGET_PERCENTAGE_CAP_MIN,
														 STATION_GROUP_BUDGET_PERCENTAGE_CAP_MAX,
														 STATION_GROUP_BUDGET_PERCENTAGE_CAP_DEFAULT,
														 &nm_STATION_GROUP_set_budget_reduction_percentage_cap,
														 &lgroup->changes_to_send_to_master,
														 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_budget_reduction_cap_bit ] );

	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return rv;
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_GROUP_get_budget_reduction_limit_for_this_station( STATION_STRUCT *const pstation )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// 9/28/2015 ajv : Suppress the "group not found" alert in case the station isn't assigned
	// to a group yet because, in that case, we still want to use the default mainline.
	lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, STATION_get_GID_station_group( pstation ), (true) );

	
	rv = STATION_GROUP_get_budget_reduction_limit( lgroup );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
// 08/18/2016 skc - Accessor function Because the backing variable is a
// runtime temp, no validation is needed.
// @mutex_requirements - list_data_recursive_MUTEX
// @task_execution - TD_CHECK
extern void nm_STATION_GROUP_set_budget_start_time_flag( STATION_GROUP_STRUCT *pgroup, UNS_32 value )
{
	pgroup->flag_budget_start_time_alert_sent = value;
}

// 08/18/2016 skc - Accessor function Because the backing variable is a
// runtime temp, no validation is needed.
// @mutex_requirements - list_data_recursive_MUTEX
// @task_execution - TD_CHECK
extern UNS_32 nm_STATION_GROUP_get_budget_start_time_flag( STATION_GROUP_STRUCT *pgroup )
{
	return pgroup->flag_budget_start_time_alert_sent;
}

// 8/18/2016 skc : Clear the flag_budget_start_time_alert_sent value for all station groups.
// @mutex_requirements - none
// @task_execution - TD_CHECK
extern void STATION_GROUP_clear_budget_start_time_flags()
{
	STATION_GROUP_STRUCT *lsgs;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lsgs = nm_ListGetFirst( &station_group_group_list_hdr );
		
	while( lsgs != NULL )
	{
		nm_STATION_GROUP_set_budget_start_time_flag( lsgs, 0 );
			
		lsgs = nm_ListGetNext( &station_group_group_list_hdr, lsgs );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}


/* ---------------------------------------------------------- */
/**
 * Returns the on-at-a-time in system setting associated with a group based upon
 * its ID. If the group isn't found, an alert is generated and the default value 
 * is returned. 
 * 
 * @executed Executed within the context of the TD check task as part of the 
 *  		 FOAL irrigation list maintenance.
 * 
 * @param pon_at_a_time_gid The group's ID
 * 
 * @return UNS_32 On-at-a-Time in system for the specified group.
 *
 * @author 7/21/2011 Adrianusv
 *
 * @revisions
 *    7/21/2011 Initial release
 */
extern UNS_32 ON_AT_A_TIME_get_on_at_a_time_within_system_for_this_gid( const UNS_32 pon_at_a_time_gid )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32	rv;

	// If GID  is 0, it means the station we're getting the number for isn't
	// assigned to a group yet. This is a valid condition for which we return
	// the default value.
	if( pon_at_a_time_gid > 0 )
	{
		// To ensure the group isn't deleted between retrieving and copying the
		// setting, take the list_program_data_recursive_MUTEX mutex.
		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, pon_at_a_time_gid, (false) );

		if( lgroup != NULL )
		{
			if( lgroup->on_at_a_time_in_system != ON_AT_A_TIME_X )
			{
				rv = SHARED_get_uint32_64_bit_change_bits_group( lgroup,
																 &lgroup->on_at_a_time_in_system,
																 ON_AT_A_TIME_MIN,
																 ON_AT_A_TIME_ABSOLUTE_MAXIMUM,
																 ON_AT_A_TIME_DEFAULT,
																 &nm_ON_AT_A_TIME_set_on_at_a_time_in_system,
																 &lgroup->changes_to_send_to_master,
																 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_on_at_a_time_in_system_bit ] );
			}
			else
			{
				rv = ON_AT_A_TIME_X;
			}
		}
		else
		{
			Alert_station_not_in_group();

			rv = ON_AT_A_TIME_DEFAULT;
		}

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}
	else
	{
		rv = ON_AT_A_TIME_DEFAULT;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Roll through each group in the on at a time list and set to 0 the number ON
 * within the group.
 * 
 * @executed Executed within the context of the TD check task as part of the
 *           FOAL irrigation list maintenance
 *
 * @author 11/2/2011 BobD
 *
 * @revisions
 *    11/2/2011 Initial release
 */
extern void ON_AT_A_TIME_init_all_groups_ON_count( void )
{
	STATION_GROUP_STRUCT	*lgroup;
	
	// Take the program data MUTEX to protect against an editing change that
	// say deletes a group while we are passing through the list.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup = nm_ListGetFirst( &station_group_group_list_hdr );
	
	while( lgroup != NULL )
	{
		lgroup->presently_ON_group_count = 0;
		
		lgroup = nm_ListGetNext( &station_group_group_list_hdr, lgroup );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Given the pgid of a station that we are considering to turn ON. It is not yet
 * ON! Find the group with the pgid value. If none found return (false). That
 * would be the safe thing to do. That is not to allow this valve to come ON.
 * However if this happens would generate an alert line again and again each
 * time this valve is considered each second. Hmmmm. Should remove the station
 * from the irrigation list (TODO)! If the group is found, find out if the ON
 * count is at the limit. If not return (true). If so return (false).
 * Additionally check that the number ON in the group does not exceed it's own
 * limit. This is an opportunistic check that has nothing to do with the
 * consideration to turn ON or not for this valve.
 * 
 * @executed Executed within the context of the TD check task as part of the
 *           FOAL irrigation list maintenance
 * 
 * @param pon_at_a_time_gid The group's ID
 * 
 * @return BOOL_32 (see function description above)
 *
 * @author 11/2/2011 BobD
 *
 * @revisions
 *    11/2/2011 Initial release
 */
extern BOOL_32 ON_AT_A_TIME_can_another_valve_come_ON_within_this_group( const UNS_32 pon_at_a_time_gid )
{
	STATION_GROUP_STRUCT	*lgroup;

	BOOL_32	rv;

	// 2011.11.02 rmd : If we cannot find the group or we've exceeded the limit, do not allow
	// the valve to turn ON.
	// 
	// 2012.02.17 ajv : Set this early on so that any failure will always result in returning
	// false.
	rv = (false);

	// If GID  is 0, it means the station we're getting the number for isn't assigned to a group
	// yet. This is a valid condition for which we return the default value.
	if( pon_at_a_time_gid > 0 )
	{
		// Take the program data MUTEX to protect against an editing change that say deletes a group
		// while we are passing through the list.
		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, pon_at_a_time_gid, (false) );

		if( lgroup != NULL )
		{
			if( lgroup->presently_ON_group_count < lgroup->on_at_a_time_in_group )
			{
				// 2011.11.02 rmd : We are not yet at the limit. 
				rv = (true);
			}
			else
			{
				// Incidentally check this. Within this group if the number ON is more than the allowed
				// number ON flag an error.
				if( lgroup->presently_ON_group_count > lgroup->on_at_a_time_in_group )
				{
					// 2011.11.02 rmd : This is an ERROR. Should not be possible!
					ALERT_MESSAGE_WITH_FILE_NAME( "too many ON in group" );
				}
			}
		}
		else
		{
			Alert_group_not_found();
		}

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Increments the on-at-a-time limit for a particular group based upon its ID.
 * If we exceed the limit, an alert is generated however we continue on as
 * normal.
 * 
 * @executed Executed within the context of the TD check task as part of the
 *           FOAL irrigation list maintenance
 * 
 * @param pon_at_a_time_gid The group's ID.
 *
 * @author 10/25/2011 BobD
 *
 * @revisions
 *    10/25/2011 Initial release
 */
extern void ON_AT_A_TIME_bump_this_groups_ON_count( const UNS_32 pon_at_a_time_gid )
{
	STATION_GROUP_STRUCT	*lgroup;

	// If GID  is 0, it means the station we're getting the number for isn't
	// assigned to a group yet. This is a valid condition but means we can't
	// bump the group count.
	if( pon_at_a_time_gid > 0 )
	{
		// Take the program data MUTEX to protect against an editing change that
		// say deletes a group while we are passing through the list.
		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, pon_at_a_time_gid, (false) );

		if( lgroup != NULL )
		{
			// Bump it regardless if it puts us over the limit or not.
			lgroup->presently_ON_group_count += 1;

			if( lgroup->presently_ON_group_count > lgroup->on_at_a_time_in_group )
			{
				// 2011.11.02 rmd : This is an ERROR. Should not be possible!
				ALERT_MESSAGE_WITH_FILE_NAME( "More valves ON than the limit!" );
			}
		}
		else
		{
			Alert_group_not_found();
		}

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}
}

/* ---------------------------------------------------------- */
extern void ACQUIRE_EXPECTEDS_clear_acquire_expected_flow_flag_for_GID( const UNS_32 pgroup_ID )
{
	STATION_GROUP_STRUCT	*lgroup;

	CHANGE_BITS_64_BIT	*lbitfield_of_changes;

	// ----------

	// If the station has a GID of 0, which means it's not assigned to a group
	// yet, return false.
	if( pgroup_ID > 0 )
	{
		// To ensure the group isn't deleted between retrieving and copying the
		// priority level, take the list_program_data_recursive_MUTEX mutex.
		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, pgroup_ID, (false) );

		if( lgroup != NULL )
		{
			lbitfield_of_changes = STATION_GROUP_get_change_bits_ptr( lgroup, CHANGE_REASON_HIT_A_STARTTIME );

			nm_ACQUIRE_EXPECTEDS_set_acquire_expected( lgroup, (false), CHANGE_generate_change_line, CHANGE_REASON_HIT_A_STARTTIME, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, lbitfield_of_changes );
		}

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}
}

/* ---------------------------------------------------------- */
extern CHANGE_BITS_64_BIT *STATION_GROUP_get_change_bits_ptr( STATION_GROUP_STRUCT *pgroup, const UNS_32 pchange_reason )
{
	return( SHARED_get_64_bit_change_bits_ptr( pchange_reason, &pgroup->changes_to_send_to_master, &pgroup->changes_to_distribute_to_slaves, &pgroup->changes_to_upload_to_comm_server ) );
}

/* ---------------------------------------------------------- */
extern void STATION_GROUP_clean_house_processing( void )
{
	// 4/16/2014 rmd : Executed within the context of the comm_mngr task. ONLY BY SLAVES. THE
	// CHANGES MADE HERE DO NOT BE DISTRIBUTED. THE STATION LIST IS BEING WIPED IN PREPARATION
	// OF BEING ADDED AS A NEW MEMBER TO A CHAIN. Called when the result of a scan indicates
	// this controller is a new slave in the chain. Used when building up chains. Likely ONLY
	// used when assembling a chain for the first time or when increasing the length of a chain
	// to a length it has never 'run' at before. Or if you changed the LETTER of a slave in a
	// chain.
	
	void	*tptr;
	
	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	tptr = nm_ListRemoveHead( &station_group_group_list_hdr );

	while( tptr != NULL )
	{
		mem_free( tptr );

		tptr = nm_ListRemoveHead( &station_group_group_list_hdr );
	}

	// ----------

	// 4/16/2014 rmd : By setting the no groups exist flag to false no change bits will be set
	// for this 'dummy' group. And that is what we want.
	nm_GROUP_create_new_group( &station_group_group_list_hdr, (char*)STATION_GROUP_DEFAULT_NAME, &nm_STATION_GROUP_set_default_values, sizeof(STATION_GROUP_STRUCT), (false), NULL );

	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE, LANDSCAPE_DETAILS_FILENAME, LANDSCAPE_DETAILS_LATEST_FILE_REVISION, &station_group_group_list_hdr, sizeof( STATION_GROUP_STRUCT ), list_program_data_recursive_MUTEX, FF_LANDSCAPE_DETAILS );

	// ----------

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void STATION_GROUP_set_bits_on_all_groups_to_cause_distribution_in_the_next_token( void )
{
	STATION_GROUP_STRUCT	*lgroup;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lgroup = nm_ListGetFirst( &station_group_group_list_hdr );

	while( lgroup != NULL )
	{
		// 5/1/2014 rmd : This function ONLY being called at the conclusion of a SCAN by the master
		// of a real multiple controller chain - we want to broadcast to the slaves using tokens
		// about all the hardware we presently know about.
		SHARED_set_all_64_bit_change_bits( &lgroup->changes_to_distribute_to_slaves, list_program_data_recursive_MUTEX );

		lgroup = nm_ListGetNext( &station_group_group_list_hdr, lgroup );
	}

	// ----------

	// 4/29/2014 ajv : Set a flag in the changes structure to notify this
	// controller to examine its lists for changes to distribute to the other
	// controllers in the chain in it's next token resposne. The flag is part of
	// the mutex protected comm_mngr struct so take the MUTEX.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	comm_mngr.changes.distribute_changes_to_slave = (true);

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );

	// ----------

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void STATION_GROUP_on_all_groups_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear )
{
	STATION_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup = nm_ListGetFirst( &station_group_group_list_hdr );

	while( lgroup != NULL )
	{
		if( pset_or_clear == COMMSERVER_CHANGE_BITS_ALL_CLEARED )
		{
			SHARED_clear_all_64_bit_change_bits( &lgroup->changes_to_upload_to_comm_server, list_program_data_recursive_MUTEX );
		}
		else
		{
			SHARED_set_all_64_bit_change_bits( &lgroup->changes_to_upload_to_comm_server, list_program_data_recursive_MUTEX );
		}

		lgroup = nm_ListGetNext( &station_group_group_list_hdr, lgroup );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * @mutex_requirements Requires the caller to have the pending_changes_recursive_MUTEX to
 * ensure no other function tries to modify the pending_changes_to_send_to_comm_server flag.
 *
 * @task_execution Executed within the context of the CONTROLLER INITIATED task when it
 * first starts and when the waiting response flags are cleared after a communication.
 */
extern void nm_STATION_GROUP_update_pending_change_bits( const UNS_32 pcomm_error_occurred )
{
	STATION_GROUP_STRUCT	*lgroup;

	BOOL_32	lfile_save_necessary;

	lfile_save_necessary = (false);

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup = nm_ListGetFirst( &station_group_group_list_hdr );

	while( lgroup != NULL )
	{
		// 4/30/2015 ajv : No reason to process this if none of the bits are set. This also helps
		// reduce the file save activity.
		if( lgroup->changes_uploaded_to_comm_server_awaiting_ACK != CHANGE_BITS_32_BIT_ALL_CLEAR )
		{
			// 4/29/2015 ajv : If a communication error occurred while sending Program Data, copy the
			// bits of variables that were sent back into the original bit field to ensure they're sent
			// again during the next attempt. Otherwise, clear the pending bits.
			if( pcomm_error_occurred )
			{
				lgroup->changes_to_upload_to_comm_server |= lgroup->changes_uploaded_to_comm_server_awaiting_ACK;

				// 4/30/2015 ajv : Since we've determined there were changes that were not successfully
				// sent, set the flag to trigger another attempt
				weather_preserves.pending_changes_to_send_to_comm_server = (true);

				// 4/30/2015 ajv : Also set the timer to force the data to send. If the timer's already
				// 
				// 4/30/2015 ajv : BECAUSE of the potential that this function is executed many times (once
				// for each item changed) we could not start our pdata timer with a post to the CI queue.
				// The queue would overflow. So start the timer directly.
				// running, this will simply restart it.
				xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGES_NOT_SENT_MS ), portMAX_DELAY );
			}
			else
			{
				SHARED_clear_all_64_bit_change_bits( &lgroup->changes_uploaded_to_comm_server_awaiting_ACK, list_program_data_recursive_MUTEX );
			}

			// 4/30/2015 ajv : At least one group was updated. So make sure we save the file.
			lfile_save_necessary = (true);
		}

		lgroup = nm_ListGetNext( &station_group_group_list_hdr, lgroup );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	if( lfile_save_necessary )
	{
		// 4/29/2015 ajv : Now that the bits are set or cleared accordingly, trigger a file save.
		// If power fails between building the message and receiving the ACK, the original bit field
		// hasn't been saved to flash yet, so it will resend the associated variables during the
		// next attempt.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_LANDSCAPE_DETAILS, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
	}
}

/* ---------------------------------------------------------- */
extern UNS_32 STATION_GROUPS_if_the_moisture_decoder_is_available_return_decoder_serial_number_for_this_station_group( STATION_GROUP_STRUCT *const psgs_ptr )
{
	// 3/30/2016 rmd : Returns 0 if psgs_ptr is NULL. Also returns 0 if the station group is not
	// using moisture sensing. Otherwise returns a moisture sensor decoder serial number.
	
	// ----------
	
	UNS_32	rv;

	// ----------
	
	// 3/30/2016 rmd : If the station isn't assigned to a station group yet, we want to return a
	// 0 serial number which means the station is NOT using moisture sensing.
	rv = 0;
	
	// ----------
	
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( psgs_ptr != NULL )
	{
		rv = SHARED_get_uint32_64_bit_change_bits_group( psgs_ptr,
														 &psgs_ptr->moisture_sensor_serial_number,
														 DECODER_SERIAL_NUM_MIN,
														 DECODER_SERIAL_NUM_MAX,
														 DECODER_SERIAL_NUM_DEFAULT,
														 nm_STATION_GROUP_set_moisture_sensor_serial_number,
														 &psgs_ptr->changes_to_send_to_master,
														 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_moisture_sensor_ser_num_bit ] );
														 
		// 3/31/2016 rmd : NOW check that the sensor with this serial number is in_use and
		// physically available. This is an IMPORTANT CHECK we make so that where this function is
		// called at the start time we only play moisture sensing for decoders that look like
		// they're alive and well on the 2-Wire cable.
		if( MOISTURE_SENSOR_this_decoder_is_in_use_and_physically_available( rv ) )
		{
			// 3/31/2016 rmd : Then the return value is good as is. Nothing to do.
		}
		else
		{
			rv = 0;
		}
	}
	
	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 STATION_GROUPS_skip_irrigation_due_to_a_mow_day(	BOOL_32 pfor_ftimes,
																STATION_GROUP_STRUCT *const psgs_ptr,
																FT_STATION_STRUCT* pft_station_ptr,
																const DATE_TIME_COMPLETE_STRUCT *pdtcs_ptr )
{
	BOOL_32	rv;
	
	UNS_32	lmow_day;
	
	// 2/26/2018 rmd : Default is not to skip irrigation.
	rv = (false);
	
	// ----------
	
	if( pfor_ftimes )
	{
		lmow_day = pft_station_ptr->station_group_ptr->mow_day;
	}
	else
	{
		lmow_day = SCHEDULE_get_mow_day( psgs_ptr );
	}
	
	// 2012.07.02 ajv : Based on a discussion with Bob, David, and Richard this
	// morning, it was decided that we will skip irrigation for a 24-hour period
	// surrounding a mow day using the ET roll time as the way to define the
	// 24-hour period. Therefore, if a start time is crossed after the roll time
	// on the day before a mow day, the irrigation is skipped. Similarly, if a
	// start time is crossed before the roll time on a mow day, the irrigation
	// is skipped.
	if( lmow_day != SCHEDULE_MOW_DAY_NOT_SET )
	{
		if( ((pdtcs_ptr->__dayofweek == 6) && (lmow_day == 0)) || ((UNS_32)(pdtcs_ptr->__dayofweek + 1) == lmow_day) )
		{
			// Tomorrow's a mow day, so check to see if we've crossed the ET
			// roll time. If so, skip irrigation.
			if( pdtcs_ptr->date_time.T >= NETWORK_CONFIG_get_start_of_irrigation_day() )
			{
				rv = (true);
			}
		}
		else
		if( pdtcs_ptr->__dayofweek == lmow_day )
		{
			// Today's a mow day, so check to see if it's before the next ET
			// roll time. If so, skip irrigation.
			if( pdtcs_ptr->date_time.T < NETWORK_CONFIG_get_start_of_irrigation_day() )
			{
				rv = (true);
			}
		}
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
void STATION_GROUPS_load_ftimes_list( UNS_32 const pcalculation_date )
{
	// 2/14/2018 rmd : Executed within the context of the ftimes task. Upon entry it is assumed
	// the ftimes station group list is empty and initialized.
	
	// ----------
	
	STATION_GROUP_STRUCT		*regular_station_group;
	
	FT_STATION_GROUP_STRUCT		*ft_group;
	
	UNS_32						schedule_type_days;
	
	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	regular_station_group = nm_ListGetFirst( &station_group_group_list_hdr );

	while( regular_station_group != NULL )
	{
		if( regular_station_group->in_use )
		{
			if( mem_obtain_a_block_if_available( sizeof(FT_STATION_GROUP_STRUCT), (void**)&ft_group ) )
			{
				memset( ft_group, 0x00, sizeof(FT_STATION_GROUP_STRUCT) );
				
				ft_group->group_identity_number = regular_station_group->base.group_identity_number;

				ft_group->precip_rate_in_100000u = regular_station_group->precip_rate_in_100000u;

				memcpy( &ft_group->crop_coefficient_100u, &regular_station_group->crop_coefficient_100u, sizeof(ft_group->crop_coefficient_100u) );

				ft_group->priority_level = regular_station_group->priority_level;

				ft_group->percent_adjust_100u = regular_station_group->percent_adjust_100u;
				ft_group->percent_adjust_start_date = regular_station_group->percent_adjust_start_date;
				ft_group->percent_adjust_end_date = regular_station_group->percent_adjust_end_date;

				ft_group->schedule_enabled_bool = regular_station_group->schedule_enabled_bool;
				ft_group->schedule_type = regular_station_group->schedule_type;

				ft_group->start_time = regular_station_group->start_time;
				ft_group->stop_time = regular_station_group->stop_time;

				// ----------
				
				ft_group->a_scheduled_irrigation_date_in_the_past = regular_station_group->begin_date;
				
				// 7/2/2018 rmd : Because of the way regular irrigation pushes the begin_date along to the
				// next scheduled irrigation, for those schedule types that rely upon the begin date test if
				// it is in the future. If so, must pull back to today or ealier so our own ftimes
				// irrigation day check math works.
				if( ft_group->schedule_type > SCHEDULE_TYPE_EVERY_OTHER_DAY )
				{
					if( ft_group->a_scheduled_irrigation_date_in_the_past > pcalculation_date )
					{
						// 7/2/2018 rmd : Then we have to move it earlier for our water day check math to work
						// [because we do the math and check differently than regular irrigation!!].
						if( ft_group->schedule_type == SCHEDULE_TYPE_ONCE_EVERY_THREE_WEEKS )
						{
							schedule_type_days = 21;
						}
						else
						if( ft_group->schedule_type == SCHEDULE_TYPE_ONCE_EVERY_FOUR_WEEKS )
						{
							schedule_type_days = 28;
						}
						else
						{
							schedule_type_days = ft_group->schedule_type - SCHEDULE_TYPE_NUMBER_OF_DAYS_OFFSET;
						}
						
						do
						{
							// 7/2/2018 rmd : Should only execute once - but use a do-while loop to make sure.
							ft_group->a_scheduled_irrigation_date_in_the_past -= schedule_type_days;

						}while( ft_group->a_scheduled_irrigation_date_in_the_past > pcalculation_date );
					}
				}
				
				// ----------

				memcpy( &ft_group->water_days_bool, &regular_station_group->water_days_bool, sizeof(ft_group->water_days_bool) );

				ft_group->irrigate_on_29th_or_31st_bool = regular_station_group->irrigate_on_29th_or_31st_bool;
				ft_group->mow_day = regular_station_group->mow_day;

				ft_group->et_in_use = regular_station_group->et_in_use;
				ft_group->use_et_averaging_bool = regular_station_group->use_et_averaging_bool;

				ft_group->on_at_a_time__allowed_ON_in_station_group__user_setting = regular_station_group->on_at_a_time_in_group;
				ft_group->on_at_a_time__allowed_ON_in_mainline__user_setting = regular_station_group->on_at_a_time_in_system;
				
				ft_group->pump_in_use = regular_station_group->pump_in_use;
				
				ft_group->line_fill_time_sec = regular_station_group->line_fill_time_sec;
				ft_group->delay_between_valve_time_sec = regular_station_group->delay_between_valve_time_sec;
				
				ft_group->high_flow_action = regular_station_group->high_flow_action;
				ft_group->low_flow_action = regular_station_group->low_flow_action;
				
				ft_group->GID_irrigation_system = regular_station_group->GID_irrigation_system;
				
				// ----------
				
				// 5/2/2018 rmd : Now initialize the ftimes results variables.
				
				ft_group->results_elapsed_irrigation_time = 0;
				
				ft_group->results_start_date_and_time_associated_with_the_latest_finish_date_and_time.T = SCHEDULE_OFF_TIME;
				ft_group->results_start_date_and_time_associated_with_the_latest_finish_date_and_time.D = DATE_MINIMUM;
				
				ft_group->results_latest_finish_date_and_time.T = SCHEDULE_OFF_TIME;
				ft_group->results_latest_finish_date_and_time.D = DATE_MINIMUM;

				ft_group->results_latest_finish_date_and_time_holding.T = SCHEDULE_OFF_TIME;
				ft_group->results_latest_finish_date_and_time_holding.D = DATE_MINIMUM;

				ft_group->results_hit_the_stop_time = (false);

				ft_group->results_exceeded_stop_time_by_seconds = 0;
				
				ft_group->results_manual_programs_and_programmed_irrigation_clashed = (false);
				
				// ----------
				
				// 2/14/2018 rmd : Now add it to the ftimes list.
				nm_ListInsertTail( &ft_station_groups_list_hdr, ft_group );
			}
		}

		// ----------

		regular_station_group = nm_ListGetNext( &station_group_group_list_hdr, regular_station_group );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
BOOL_32 STATION_GROUPS_calculate_chain_sync_crc( UNS_32 const pff_name )
{
	// 8/30/2018 rmd : Executed in the context of the comm_mngr task when token responses are
	// being made. And that's it.

	// ----------

	STATION_GROUP_STRUCT	*lstation_group_ptr;
	
	UNS_8					*checksum_start;

	UNS_32					checksum_length;

	UNS_8					*ucp;
	
	BOOL_32					rv;

	// ----------

	rv = (false);

	// ----------
	
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	// 8/30/2018 rmd : So the CRC calculation function wants a continuous memory space and a
	// length to calculate the crc off. In this function we will pick out the desired data, and
	// simply pile it up on itself, list item by list item. We don't care how messy the pile is
	// because we never attempt to reference any data once it is added to the pile. The data is
	// only there to include in the checksum.
	//
	// 8/30/2018 rmd : Well we know it can't be bigger than the list item count times the size
	// of each list item, so start with a memory block that big.
	if( mem_obtain_a_block_if_available( (station_group_group_list_hdr.count * sizeof(STATION_GROUP_STRUCT)), (void**)&checksum_start ) )
	{
		// 9/5/2018 rmd : Well then the calculation will be successful. Nothing else can prevent
		// that outcome.
		rv = (true);
		
		// ----------
		
		ucp = checksum_start;
		
		checksum_length = 0;
		
		// ----------
		
		// 8/30/2018 rmd : Rattle though the list and push the fields we want to include in the sync
		// test onto the block to be checksummed.
		lstation_group_ptr = nm_ListGetFirst( &station_group_group_list_hdr );
		
		while( lstation_group_ptr != NULL )
		{
			// 9/4/2018 rmd : Make sure you include only TRUE USER SETTINGS, no flags, or pointers to
			// memory, etc...
			
			// ----------
			
			// 11/2/2018 rmd : Due to the inconsistent way the group names strings are handled during
			// the sync (we do not regligiously take all 48 characters and always carry them through the
			// sync process, there are places we only copy up until the null), the part of the string
			// following the null char is not guaranteed to be in sync. So only include up until the
			// null in the crc test.
			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->base.description, strlen(lstation_group_ptr->base.description) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->plant_type, sizeof(lstation_group_ptr->plant_type) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->head_type, sizeof(lstation_group_ptr->head_type) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->precip_rate_in_100000u, sizeof(lstation_group_ptr->precip_rate_in_100000u) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->soil_type, sizeof(lstation_group_ptr->soil_type) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->slope_percentage, sizeof(lstation_group_ptr->slope_percentage) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->exposure, sizeof(lstation_group_ptr->exposure) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->usable_rain_percentage_100u, sizeof(lstation_group_ptr->usable_rain_percentage_100u) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->crop_coefficient_100u, sizeof(lstation_group_ptr->crop_coefficient_100u) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->priority_level, sizeof(lstation_group_ptr->priority_level) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->percent_adjust_100u, sizeof(lstation_group_ptr->percent_adjust_100u) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->percent_adjust_start_date, sizeof(lstation_group_ptr->percent_adjust_start_date) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->percent_adjust_end_date, sizeof(lstation_group_ptr->percent_adjust_end_date) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->schedule_enabled_bool, sizeof(lstation_group_ptr->schedule_enabled_bool) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->schedule_type, sizeof(lstation_group_ptr->schedule_type) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->start_time, sizeof(lstation_group_ptr->start_time) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->stop_time, sizeof(lstation_group_ptr->stop_time) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->begin_date, sizeof(lstation_group_ptr->begin_date) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->water_days_bool, sizeof(lstation_group_ptr->water_days_bool) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->irrigate_on_29th_or_31st_bool, sizeof(lstation_group_ptr->irrigate_on_29th_or_31st_bool) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->mow_day, sizeof(lstation_group_ptr->mow_day) );

			// 9/4/2018 rmd : skipping a few flags and things not to crc here

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->et_in_use, sizeof(lstation_group_ptr->et_in_use) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->use_et_averaging_bool, sizeof(lstation_group_ptr->use_et_averaging_bool) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->rain_in_use_bool, sizeof(lstation_group_ptr->rain_in_use_bool) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->soil_storage_capacity_inches_100u, sizeof(lstation_group_ptr->soil_storage_capacity_inches_100u) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->wind_in_use_bool, sizeof(lstation_group_ptr->wind_in_use_bool) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->high_flow_action, sizeof(lstation_group_ptr->high_flow_action) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->low_flow_action, sizeof(lstation_group_ptr->low_flow_action) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->on_at_a_time_in_group, sizeof(lstation_group_ptr->on_at_a_time_in_group) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->on_at_a_time_in_system, sizeof(lstation_group_ptr->on_at_a_time_in_system) );

			// 9/12/2018 rmd : skipping dynamic on at a time

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->pump_in_use, sizeof(lstation_group_ptr->pump_in_use) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->line_fill_time_sec, sizeof(lstation_group_ptr->line_fill_time_sec) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->delay_between_valve_time_sec, sizeof(lstation_group_ptr->delay_between_valve_time_sec) );

			// 9/12/2018 rmd : skipping acquired expecteds - sort of a dynamic thing. And of course
			// skipping the change bitfields.
			
			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->allowable_depletion_100u, sizeof(lstation_group_ptr->allowable_depletion_100u) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->available_water_100u, sizeof(lstation_group_ptr->available_water_100u) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->root_zone_depth_10u, sizeof(lstation_group_ptr->root_zone_depth_10u) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->species_factor_100u, sizeof(lstation_group_ptr->species_factor_100u) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->density_factor_100u, sizeof(lstation_group_ptr->density_factor_100u) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->microclimate_factor_100u, sizeof(lstation_group_ptr->microclimate_factor_100u) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->soil_intake_rate_100u, sizeof(lstation_group_ptr->soil_intake_rate_100u) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->allowable_surface_accumulation_100u, sizeof(lstation_group_ptr->allowable_surface_accumulation_100u) );

			// 9/12/2018 rmd : Skipping change bitfield here.

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->budget_reduction_percentage_cap, sizeof(lstation_group_ptr->budget_reduction_percentage_cap) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->GID_irrigation_system, sizeof(lstation_group_ptr->GID_irrigation_system) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->in_use, sizeof(lstation_group_ptr->in_use) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lstation_group_ptr->moisture_sensor_serial_number, sizeof(lstation_group_ptr->moisture_sensor_serial_number) );
			
			// END
			
			// ----------
		
			lstation_group_ptr = nm_ListGetNext( &station_group_group_list_hdr, lstation_group_ptr );
		}
		
		// ----------
		
		// 8/30/2018 rmd : Now calculate the checksum.
		cscs.the_crc[ pff_name ] = CRC_calculate_32bit_big_endian( checksum_start, checksum_length );
		
		// ----------
		
		// 9/4/2018 rmd : We're done with the block, so of course free it.
		mem_free( checksum_start );
	}
	else
	{
		// 9/5/2018 rmd : So by design it's okay if there is no memory. The checksum won't be set
		// valid, and the next time we try to include it in a token response we'll try to calculate
		// it again. Self protecting and retrying. But let's alert about this so we know it's taking
		// place.
		Alert_Message_va( "SYNC: no mem to calc checksum %s", chain_sync_file_pertinants[ pff_name ].file_name_string );
	}
	
	// ----------
	
	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
