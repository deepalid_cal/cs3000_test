/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_POC_NEW_H
#define _INC_POC_NEW_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"cal_list.h"

#include	"cs3000_tpmicro_common.h"

#include	"foal_defs.h"

#include	"pdata_changes.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/10/2014 rmd : Because when the file is FIRST created and a placeholder group is added
// it should be set to not show to the user. It's as if it is an uninstalled terminal poc
// that the user has permantely hidden.
#define POC_SHOW_THIS_POC_DEFAULT		(false)

/* ---------------------------------------------------------- */

#define	POC_MV_TYPE_NORMALLY_OPEN		(0)
#define	POC_MV_TYPE_NORMALLY_CLOSED		(1)

#define	POC_MV_TYPE_DEFAULT				(POC_MV_TYPE_NORMALLY_CLOSED)

/* ---------------------------------------------------------- */

#define POC_FM_NONE				( 0)

#define POC_FM_100B				( 1)

#define POC_FM_125B				( 2)

#define POC_FM_150B				( 3)

#define POC_FM_150				( 4)

#define POC_FM_200B				( 5)

#define POC_FM_200				( 6)

#define POC_FM_300				( 7)

#define POC_FM_FMBX				( 8)

// 1/16/2015 rmd : Note important not to change the order of the following 9 BERMAD flow
// meters. There are arrays within the BYPASS code which hold the transition points that are
// ordered to match the order here. We also count on the 0150 Bermad meter to be the FIRST
// and the 1000 Bermad meter to be the LAST.

#define POC_FM_BERMAD_0150		( 9)

#define POC_FM_BERMAD_0200		(10)

#define POC_FM_BERMAD_0250		(11)

#define POC_FM_BERMAD_0300R		(12)

#define POC_FM_BERMAD_0300		(13)

#define POC_FM_BERMAD_0400		(14)

#define POC_FM_BERMAD_0600		(15)

#define POC_FM_BERMAD_0800		(16)

#define POC_FM_BERMAD_1000		(17)

// ----------

#define	POC_FM_CHOICE_MIN			(POC_FM_NONE)
#define	POC_FM_CHOICE_MAX			(POC_FM_BERMAD_1000)
#define	POC_FM_CHOICE_DEFAULT		(POC_FM_NONE)

/* ---------------------------------------------------------- */

// 1/15/2015 rmd : Set the MINIMUM FLOW RATE for the BERMAD flow transducers. David Byma and
// I agreed to 10 gpm regardless of pulse rate or meter size.
#define POC_FM_BERMAD_MAXIMUM_SECONDS_SINCE_LAST_PULSE_1_1	(6)

#define POC_FM_BERMAD_MAXIMUM_SECONDS_SINCE_LAST_PULSE_1_10	(60)

/* ---------------------------------------------------------- */

#define	POC_CONTROLLER_SN_MIN		(CONTROLLER_MINIMUM_SERIAL_NUMBER)
#define	POC_CONTROLLER_SN_MAX		(CONTROLLER_MAXIMUM_SERIAL_NUMBER)
#define	POC_CONTROLLER_SN_DEFAULT	(CONTROLLER_MINIMUM_SERIAL_NUMBER)

/* ---------------------------------------------------------- */

#define POC_USAGE_NOT_USED			(0)
#define POC_USAGE_IRRIGATION		(1)
#define POC_USAGE_NON_IRRIGATION	(2)

#define POC_USAGE_MIN				POC_USAGE_NOT_USED
#define POC_USAGE_MAX				POC_USAGE_NON_IRRIGATION
#define	POC_USAGE_DEFAULT			POC_USAGE_IRRIGATION

/* ---------------------------------------------------------- */

#define	POC_MV_DELAY_MIN			(5)
#define	POC_MV_DELAY_MAX			(60)
#define	POC_MV_DELAY_DEFAULT		(5)

/* ---------------------------------------------------------- */

// 9/23/2016 ajv : Set the K value min and maxes to match the ET2000e. Originally, the max
// was limited to 15.0.
#define POC_FM_KVALUE_MIN			(0.0F)
#define POC_FM_KVALUE_MAX			(800.0F)
#define POC_FM_KVALUE_DEFAULT		(1.0F)

/* ---------------------------------------------------------- */

// 9/23/2016 ajv : Set the offset min and maxs to match the ET2000e. Originally, the max was
// set to 800.0.
#define	POC_FM_OFFSET_MIN			(-10.0F)
#define	POC_FM_OFFSET_MAX			(10.0F)
#define	POC_FM_OFFSET_DEFAULT		(0.20F)

/* ---------------------------------------------------------- */

#define	POC_FM_REED_SWITCH_1_TO_1		(0)
#define	POC_FM_REED_SWITCH_1_TO_10		(1)
//#define	POC_FM_REED_SWITCH_1_TO_100		(2)
//#define	POC_FM_REED_SWITCH_1_TO_1000	(3)

#define	POC_FM_REED_SWITCH_MIN			(POC_FM_REED_SWITCH_1_TO_1)
// 1/16/2015 ajv : For now, only support up to a 1 to 10 Hydrometer. The UI already supports
// up to a 1 to 1000 reed switch, so simply update this max if support for other reed
// switches are added later.
#define	POC_FM_REED_SWITCH_MAX			(POC_FM_REED_SWITCH_1_TO_10)
#define	POC_FM_REED_SWITCH_DEFAULT		(POC_FM_REED_SWITCH_1_TO_1)

/* ---------------------------------------------------------- */

#define	POC_BYPASS_INUSE_DEFAULT	(false)

/* ---------------------------------------------------------- */

#define POC_BYPASS_LEVELS_MIN		(2)

// 2012.01.19 rmd : This value is also used (as it should be) as the maximum
// number of flow meters one can have at a POC. Think of it and use it that way.
#define POC_BYPASS_LEVELS_MAX		(3)

#define	POC_BYPASS_LEVELS_DEFAULT	(2)

/* ---------------------------------------------------------- */

// 7/18/2012 rmd : Used to indicate the state of a POC. From a macro level. Meaning the MV's
// should be opened or closed. Not taking into account they type of MV (normally opened or
// closed) or how many of them there are (bypass manifold).
#define	POC_STATUS_MV_IS_OPENED		(true)
#define	POC_STATUS_MV_IS_CLOSED		(false)

#define	POC_STATUS_PUMP_IS_ON		(true)
#define	POC_STATUS_PUMP_IS_OFF		(false)

/* ---------------------------------------------------------- */

#define	POC_BUDGET_GALLONS_ENTRY_OPTION_direct_entry								(0)
#define	POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_using_annual_number				(1)
#define	POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_using_et							(2)
//#define	POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_as_percent_of_historical_et		(3)
//#define	POC_BUDGET_GALLONS_ENTRY_OPTION_projected_budget_from_water_bill			(4)

// 10/12/2018 Ryan : Max changed to reflect the number of budget entry method options
// current avaliable for the budget(base zero).
#define	POC_BUDGET_GALLONS_ENTRY_OPTION_MIN											(POC_BUDGET_GALLONS_ENTRY_OPTION_direct_entry)
#define	POC_BUDGET_GALLONS_ENTRY_OPTION_MAX											(POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_using_et)
#define	POC_BUDGET_GALLONS_ENTRY_OPTION_DEFAULT										(POC_BUDGET_GALLONS_ENTRY_OPTION_direct_entry)

/* ---------------------------------------------------------- */

#define	POC_BUDGET_CALCULATION_PERCENT_OF_ET_MIN		(10)
#define POC_BUDGET_CALCULATION_PERCENT_OF_ET_MAX		(300)
#define POC_BUDGET_CALCULATION_PERCENT_OF_ET_DEFAULT	(80)

/* ---------------------------------------------------------- */

#define	POC_BUDGET_AUTO_ADJUST_USING_ET_DEFAULT		(false)

/* ---------------------------------------------------------- */

#define	POC_BUDGET_CLOSE_POC_AT_BUDGET_DEFAULT		(false)

/* ---------------------------------------------------------- */

#define	POC_BUDGET_GALLONS_MIN		(0)
#define POC_BUDGET_GALLONS_MAX		(100000000)
#define POC_BUDGET_GALLONS_DEFAULT	(POC_BUDGET_GALLONS_MIN)

/* ---------------------------------------------------------- */

// 7/11/2018 ajv : Historically, we activate all pump outputs when irrigation starts when 
// valves are set to use a pump. However, now that we can independently monitor the current 
// draw of each output, this results in NO ELECTRICAL CURRENT alerts on all pump terminals 
// where a pump is not physically attached. This new setting allows the user to stop this 
// behavior by only energizing pump outputs where a pump is attached. 
#define	POC_PUMP_PHYSICALLY_ATTACHED_DEFAULT	(true)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct POC_GROUP_STRUCT POC_GROUP_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern const char POC_DEFAULT_NAME[];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	UNS_32	flow_meter_choice;

	UNS_32	kvalue_100000u;

	// 1/29/2015 rmd : INT_32 cause this number does go negative for some meter types.
	INT_32	offset_100000u;

	// 1/29/2015 rmd : This is to support the Bermad reed switch or opto switch type flow
	// meters. Which can be configured for a variety of pulse rates. Presently we support ONLY
	// two configurations. 1 gallon equals 1 pulse. And 10 gallons equals 1 pulse. This variable
	// uses the REED_SWITCH defines.
	UNS_32	reed_switch;

} POC_FM_CHOICE_AND_RATE_DETAILS;


typedef struct
{
	UNS_32		poc_gid;

	UNS_32		box_index;
	
	UNS_32		poc_type__file_type;

	// 6/6/2016 rmd : The flag if the poc is still available for the user to see.
	BOOL_32		show_this_poc;
	
	UNS_32		usage;
	
	UNS_32		decoder_serial_number[ POC_BYPASS_LEVELS_MAX ];

	UNS_32		master_valve_type[ POC_BYPASS_LEVELS_MAX ];
	
	// 8/25/2015 rmd : Just as a void pointer so we don't have to drag in the battery backed
	// vars.h file.
	void		*this_pocs_system_preserves_ptr;
	
} POC_PRESERVES_SYNCD_ITEMS_STRUCT;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_file_POC( void );

extern void save_file_POC( void );

// ----------

extern void nm_POC_set_name( POC_GROUP_STRUCT *const ppoc,
							 char *pname,
							 const BOOL_32 pgenerate_change_line_bool,
							 const UNS_32 preason_for_change,
							 const UNS_32 pbox_index_0,
							 const BOOL_32 pset_change_bits,
							 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
							 #ifdef _MSC_VER
							 , char *pmerge_update_str,
							 char *pmerge_insert_fields_str,
							 char *pmerge_insert_values_str
							 #endif
						   );

extern void nm_POC_set_box_index( void *const ppoc,
								  UNS_32 pbox_index,
								  const BOOL_32 pgenerate_change_line_bool,
								  const UNS_32 preason_for_change,
								  const UNS_32 pbox_index_0,
								  const BOOL_32 pset_change_bits,
								  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
								  #ifdef _MSC_VER
								  , char *pmerge_update_str,
								  char *pmerge_insert_fields_str,
								  char *pmerge_insert_values_str
								  #endif
								);

extern void nm_POC_set_decoder_serial_number( void *const ppoc,
											  const UNS_32 plevel,
											  UNS_32 pdecoder_serial_number,
											  const BOOL_32 pgenerate_change_line_bool,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
										    );

extern void nm_POC_set_show_this_poc(	void *const ppoc,
										BOOL_32 pshow_this_poc,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const BOOL_32 pset_change_bits,
										CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
										);

extern void nm_POC_set_poc_usage( void *const ppoc,
								  UNS_32 ppoc_usage,
								  const BOOL_32 pgenerate_change_line_bool,
								  const UNS_32 preason_for_change,
								  const UNS_32 pbox_index_0,
								  const BOOL_32 pset_change_bits,
								  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
								  #ifdef _MSC_VER
								  , char *pmerge_update_str,
								  char *pmerge_insert_fields_str,
								  char *pmerge_insert_values_str
								  #endif
								);

extern void nm_POC_set_poc_type( void *const ppoc,
								 UNS_32 ptype_of_poc,
								 const BOOL_32 pgenerate_change_line_bool,
								 const UNS_32 preason_for_change,
								 const UNS_32 pbox_index_0,
								 const BOOL_32 pset_change_bits,
								 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
								 #ifdef _MSC_VER
								 , char *pmerge_update_str,
								 char *pmerge_insert_fields_str,
								 char *pmerge_insert_values_str
								 #endif
							   );

extern void nm_POC_set_bypass_number_of_levels( void *const ppoc,
												BOOL_32 pnumber_of_levels,
												const BOOL_32 pgenerate_change_line_bool,
												const UNS_32 preason_for_change,
												const UNS_32 pbox_index_0,
												const BOOL_32 pset_change_bits,
												CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												#ifdef _MSC_VER
												, char *pmerge_update_str,
												char *pmerge_insert_fields_str,
												char *pmerge_insert_values_str
												#endif
											  );

extern void nm_POC_set_GID_irrigation_system( void *const ppoc,
											  const UNS_32 pGID_irrigation_system,
											  const BOOL_32 pgenerate_change_line_bool,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
											);

extern void nm_POC_set_budget_gallons_entry_option(	void *const ppoc,
													UNS_32 pbudget_gallons_entry_option,
													const BOOL_32 pgenerate_change_line_bool,
													const UNS_32 preason_for_change,
													const UNS_32 pbox_index_0,
													const BOOL_32 pset_change_bits,
													CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													#ifdef _MSC_VER
													, char *pmerge_update_str,
													char *pmerge_insert_fields_str,
													char *pmerge_insert_values_str
													#endif
												  );

extern void nm_POC_set_budget_calculation_percent_of_et(	void *const ppoc,
															UNS_32 pbudget_calculation_percent_of_et,
															const BOOL_32 pgenerate_change_line_bool,
															const UNS_32 preason_for_change,
															const UNS_32 pbox_index_0,
															const BOOL_32 pset_change_bits,
															CHANGE_BITS_32_BIT *pchange_bitfield_to_set
															#ifdef _MSC_VER
															, char *pmerge_update_str,
															char *pmerge_insert_fields_str,
															char *pmerge_insert_values_str
															#endif
													   );
 
// ----------

extern void *nm_POC_create_new_group( const UNS_32 pbox_index_0, const UNS_32 ppoc_type, const UNS_32 pdecoder_serial_number );

// ----------

extern UNS_32 POC_build_data_to_send( UNS_8 **pucp,
									  const UNS_32 pmem_used_so_far,
									  BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
									  const UNS_32 preason_data_is_being_built,
									  const UNS_32 pallocated_memory );

extern UNS_32 nm_POC_extract_and_store_changes_from_comm( const unsigned char *pucp,
														  const UNS_32 preason_for_change,
														  const BOOL_32 pset_change_bits,
														  const UNS_32 pchanges_received_from
														  #ifdef _MSC_VER
														  , char *pSQL_statements,
														  char *pSQL_search_condition_str,
														  char *pSQL_update_str,
														  char *pSQL_insert_fields_str,
														  char *pSQL_insert_values_str,
														  char *pSQL_single_merge_statement_buf,
														  const UNS_32 pnetwork_ID
														  #endif
														);

// ----------

#ifdef _MSC_VER

extern INT_32 POC_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order );

#endif

extern void POC_copy_group_into_guivars( const UNS_32 ppoc_index_0 );

extern void BUDGETS_copy_group_into_guivars( const UNS_32 ppoc_index_0, const UNS_32 pbudget_index_0 );


extern void POC_copy_preserve_info_into_guivars( UNS_32 const ppoc_preserves_index );


extern void POC_extract_and_store_group_name_from_GuiVars( void );

extern void POC_extract_and_store_changes_from_GuiVars( void );

extern void BUDGETS_extract_and_store_changes_from_GuiVars( void );

// ----------

extern void nm_POC_load_group_name_into_guivar( const INT_16 pindex_0_i16 );

// ----------

#define POC_SET_TO_SHOW_ONLY	(true)
#define POC_SET_TO_SHOW_OR_NOT	(false)

extern void *nm_POC_get_pointer_to_terminal_poc_with_this_box_index_0( const UNS_32 pbox_index );

extern void *nm_POC_get_pointer_to_bypass_poc_with_this_box_index_0( const UNS_32 pbox_index_0, const BOOL_32 ptest_show_this_poc_to_be_true );

extern POC_GROUP_STRUCT *nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn( const UNS_32 pbox_index_0, const UNS_32 ppoc_file_type, const UNS_32 pdecoder_sn, const BOOL_32 ptest_show_this_poc_to_be_true );

extern POC_GROUP_STRUCT *nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn_and_show( const UNS_32 pbox_index_0, const UNS_32 ppoc_file_type, const UNS_32 pdecoder_sn, const BOOL_32 pshow );

// ----------

extern UNS_32 POC_get_index_using_ptr_to_poc_struct( const POC_GROUP_STRUCT *const ppoc );

// 6/9/2016 rmd : Be careful using this function. The GID is not a KEY field to conduct
// searched with. Know your use of this function and why.
extern void *nm_POC_get_pointer_to_poc_with_this_gid_and_box_index( UNS_32 const ppoc_gid, UNS_32 const pbox_index );

// ----------
// 8/23/2016 skc : This group of functions is deprecated, try to use the box, type, decoder
// family instead.
extern void *POC_get_group_at_this_index( const UNS_32 pindex_0 );

extern UNS_32 POC_get_gid_of_group_at_this_index( const UNS_32 pindex_0 );

extern UNS_32 POC_get_index_for_group_with_this_GID( const UNS_32 ppoc_ID );

// ----------

extern UNS_32 POC_get_last_group_ID( void );

extern UNS_32 POC_get_list_count( void );

// ----------

extern UNS_32 POC_get_GID_irrigation_system( POC_GROUP_STRUCT *const ppoc );

extern UNS_32 POC_get_type_of_poc( const UNS_32 ppoc_gid );

extern UNS_32 POC_get_box_index_0( const UNS_32 ppoc_gid );


extern UNS_32 POC_get_decoder_serial_number_for_this_poc_gid_and_level_0( const UNS_32 ppoc_gid, const UNS_32 plevel_0 );

extern UNS_32 POC_get_decoder_serial_number_for_this_poc_and_level_0( const POC_GROUP_STRUCT *const ppoc, const UNS_32 plevel_0 );

extern UNS_32 POC_get_decoder_serial_number_index_0( const POC_GROUP_STRUCT *const ppoc, const UNS_32 pdecoder_serial_num );


extern BOOL_32 POC_get_show_for_this_poc( POC_GROUP_STRUCT *const ppoc );

extern UNS_32 POC_get_bypass_number_of_levels( POC_GROUP_STRUCT *const ppoc );

extern BOOL_32 POC_get_fm_choice_and_rate_details( const UNS_32 ppoc_gid, POC_FM_CHOICE_AND_RATE_DETAILS pfms_ptr[] );

extern UNS_32 POC_get_usage( const UNS_32 ppoc_gid );

extern BOOL_32 POC_get_master_valve_NO_or_NC( const UNS_32 ppoc_gid, UNS_32 pmv_ptr[] );

extern BOOL_32 POC_show_poc_menu_items( void );

extern BOOL_32 POC_at_least_one_POC_has_a_flow_meter( void );

extern BOOL_32 POC_at_least_one_POC_is_a_bypass_manifold( void );

extern BOOL_32 POC_there_are_pocs_to_show( void );

extern UNS_32 POC_get_budget( POC_GROUP_STRUCT *const ppoc, UNS_32 idx_budget );

extern BOOL_32 POC_get_has_pump_attached( POC_GROUP_STRUCT *const ppoc );

extern void POC_set_budget( POC_GROUP_STRUCT *ppoc, UNS_32 idx_budget, UNS_32 budget );

extern BOOL_32 POC_use_for_budget( UNS_32 idx );

// ----------

extern UNS_32 POC_get_GID_irrigation_system_using_POC_gid( const UNS_32 ppoc_gid );

// ----------

extern CHANGE_BITS_32_BIT *POC_get_change_bits_ptr( POC_GROUP_STRUCT *ppoc, const UNS_32 pchange_reason );

// ----------

extern void POC_clean_house_processing( void );




extern void POC_set_bits_on_all_pocs_to_cause_distribution_in_the_next_token( void );

extern void POC_on_all_pocs_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear );

extern void nm_POC_update_pending_change_bits( const UNS_32 pcomm_error_occurred );

// ----------

extern void POC_load_fields_syncd_to_the_poc_preserves( const POC_GROUP_STRUCT *const ppoc, POC_PRESERVES_SYNCD_ITEMS_STRUCT *psync );

// ----------

extern UNS_32 POC_get_budget_gallons_entry_option( POC_GROUP_STRUCT *const ppoc );

extern UNS_32 POC_get_budget_percent_ET( POC_GROUP_STRUCT *const ppoc );

// ----------

extern BOOL_32 POC_calculate_chain_sync_crc( UNS_32 const pff_name );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
