/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_SHARED_STATION_GROUPS_C
#define _INC_SHARED_STATION_GROUPS_C

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"group_base_file.h"

#include	"cal_td_utils.h"

#include	"change.h"

#ifndef	_MSC_VER

	#include	"alerts.h"

	#include	"app_startup.h"
	
	#include	"cs_mem.h"

	#include	"battery_backed_vars.h"

	#include	"moisture_sensors.h"

#else

	#include	<stdlib.h>

	#include	"sql_merge.h"

#endif




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	STATION_GROUP_CHANGE_BITFIELD_description_bit				(0)
#define	STATION_GROUP_CHANGE_BITFIELD_plant_type_bit				(1)
#define	STATION_GROUP_CHANGE_BITFIELD_head_type_bit					(2)
#define	STATION_GROUP_CHANGE_BITFIELD_precip_rate_bit				(3)

#define	STATION_GROUP_CHANGE_BITFIELD_soil_type_bit					(4)
#define	STATION_GROUP_CHANGE_BITFIELD_slope_percentage_bit			(5)
#define	STATION_GROUP_CHANGE_BITFIELD_exposure_bit					(6)
#define	STATION_GROUP_CHANGE_BITFIELD_usable_rain_bit				(7)

#define	STATION_GROUP_CHANGE_BITFIELD_crop_coefficient_bit			(8)
#define	STATION_GROUP_CHANGE_BITFIELD_priority_level_bit			(9)
#define	STATION_GROUP_CHANGE_BITFIELD_percent_adjust_bit			(10)
#define	STATION_GROUP_CHANGE_BITFIELD_percent_adjust_start_date_bit	(11)

#define	STATION_GROUP_CHANGE_BITFIELD_percent_adjust_end_date_bit	(12)
#define	STATION_GROUP_CHANGE_BITFIELD_schedule_enabled_bit			(13)
#define	STATION_GROUP_CHANGE_BITFIELD_schedule_type_bit				(14)
#define	STATION_GROUP_CHANGE_BITFIELD_start_time_bit				(15)

#define	STATION_GROUP_CHANGE_BITFIELD_stop_time_bit					(16)
#define	STATION_GROUP_CHANGE_BITFIELD_begin_date_bit				(17)
#define	STATION_GROUP_CHANGE_BITFIELD_water_days_bit				(18)
#define	STATION_GROUP_CHANGE_BITFIELD_irrigate_on_29th_or_31st_bit	(19)

#define	STATION_GROUP_CHANGE_BITFIELD_mow_day_bit					(20)
#define	STATION_GROUP_CHANGE_BITFIELD_last_ran_bit					(21)
//#define	STATION_GROUP_CHANGE_BITFIELD_nlu_tail_ends_throw_away_bit		(22)
//#define	STATION_GROUP_CHANGE_BITFIELD_nlu_tail_ends_make_adjustment_bit	(23)

//#define	STATION_GROUP_CHANGE_BITFIELD_nlu_maximum_leftover_irri_bit		(24)
#define	STATION_GROUP_CHANGE_BITFIELD_et_in_use						(25)
#define	STATION_GROUP_CHANGE_BITFIELD_use_et_averaging				(26)
#define	STATION_GROUP_CHANGE_BITFIELD_rain_in_use_bit				(27)

#define	STATION_GROUP_CHANGE_BITFIELD_soil_storage_capacity_bit		(28)
#define	STATION_GROUP_CHANGE_BITFIELD_wind_in_use_bit				(29)
#define	STATION_GROUP_CHANGE_BITFIELD_high_flow_action_bit			(30)
#define	STATION_GROUP_CHANGE_BITFIELD_low_flow_action_bit			(31)

#define	STATION_GROUP_CHANGE_BITFIELD_on_at_a_time_in_group_bit		(32)
#define	STATION_GROUP_CHANGE_BITFIELD_on_at_a_time_in_system_bit	(33)
#define	STATION_GROUP_CHANGE_BITFIELD_pump_in_use_bit				(34)
#define	STATION_GROUP_CHANGE_BITFIELD_line_fill_time_bit			(35)

#define	STATION_GROUP_CHANGE_BITFIELD_delay_between_valves_bit		(36)
#define	STATION_GROUP_CHANGE_BITFIELD_acquire_at_start_bit			(37)
#define	STATION_GROUP_CHANGE_BITFIELD_allowable_depletion_bit		(38)
#define	STATION_GROUP_CHANGE_BITFIELD_available_water_bit			(39)

#define	STATION_GROUP_CHANGE_BITFIELD_root_zone_depth_bit			(40)
#define	STATION_GROUP_CHANGE_BITFIELD_species_factor_bit			(41)
#define	STATION_GROUP_CHANGE_BITFIELD_density_factor_bit			(42)
#define	STATION_GROUP_CHANGE_BITFIELD_microclimate_factor_bit		(43)

#define	STATION_GROUP_CHANGE_BITFIELD_soil_intake_rate_bit			(44)
#define	STATION_GROUP_CHANGE_BITFIELD_allowable_surface_accum_bit	(45)
#define	STATION_GROUP_CHANGE_BITFIELD_budget_reduction_cap_bit		(46)
#define	STATION_GROUP_CHANGE_BITFIELD_GID_irrigation_system_bit		(47)

#define	STATION_GROUP_CHANGE_BITFIELD_in_use_bit					(48)

#define	STATION_GROUP_CHANGE_BITFIELD_moisture_sensor_ser_num_bit	(49)

// ----------

static char* const STATION_GROUP_database_field_names[ (STATION_GROUP_CHANGE_BITFIELD_moisture_sensor_ser_num_bit + 1) ] =
{
	"Name",
	"PlantType",
	"HeadType",
	"PrecipRate",
	"SoilType",
	"SlopePercentage",
	"Exposure",
	"UsableRainPercentage",
	"CropCoefficient",		// CropCoefficient1 .. 12
	"PriorityLevel",
	"PercentAdjust",
	"StartDate",
	"EndDate",
	"Enabled",
	"ScheduleType",
	"StartTime",
	"StopTime",
	"BeginDate",
	"WaterDays",	// WaterDaysSun .. Sat
	"IrrigateOn29Or31",
	"MowDay",
	"LastRan",
	"",				// No longer used - used to be "TailEndsThrowAway"
	"",				// No longer used - used to be "TailEndsMakeAdjustment"
	"",				// No longer used - used to be "MaximumLeftover"
	"ET_Mode",
	"UseET_Averaging",
	"RainInUse",
	"MaximumAccumulation",
	"WindInUse",
	"HighFlowAction",
	"LowFlowAction",
	"On_At_A_TimeInGroup",
	"On_At_A_TimeInSystem",
	"PumpInUse",
	"LineFillTime",
	"ValveCloseTime",
	"AcquireExpecteds",
	"AllowableDepletion",
	"AvailableWater",
	"RootDepth",
	"SpeciesFactor",
	"DensityFactor",
	"MicroclimateFactor",
	"SoilIntakeRate",
	"AllowableSurfaceAccum",
	"BudgetReductionCap",
	"SystemID",
	"InUse",
	"MoisSensorSerNum"
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * The definition of the group's structure. If the contents of this structure
 * are changed or reordered, you <b>MUST</b> update the 
 * STATION_GROUP_LATEST_FILE_VERSION located in this structure's header file. 
 */
struct STATION_GROUP_STRUCT
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	UNS_32			plant_type;

	UNS_32			head_type;

	UNS_32			precip_rate_in_100000u;

	UNS_32			soil_type;

	UNS_32			slope_percentage;

	UNS_32			exposure;

	UNS_32			usable_rain_percentage_100u;

	UNS_32			crop_coefficient_100u[ MONTHS_IN_A_YEAR ];

	// -------------------
	
	// The priority level - Low (1), Medium (2), or High (3)
	UNS_32			priority_level;

	// -------------------
	
	// The percentage to increase or decrease the station's run times by
	UNS_32			percent_adjust_100u;

	// The date to start the percent adjust
	UNS_32			percent_adjust_start_date;

	// The date to end the percent adjust
	UNS_32			percent_adjust_end_date;

	// -------------------

	// TODO Add enabled flag to UI
	BOOL_32			schedule_enabled_bool;

	UNS_32			schedule_type;

	UNS_32			start_time;
	UNS_32			stop_time;

	UNS_32			begin_date;

	BOOL_32			water_days_bool[ DAYS_IN_A_WEEK ];

	// 4/7/2016 rmd : This is IMPROPERLY named. What it should say is
	// irrigates_after_29th_or_31st. And applies to only an ODD day irrigation schedule when we
	// are on the FIRST of the month when the prior month ended with 29 or 31 days. This
	// variable if (true) signifies we should irrigate in that case on the FIRST even after
	// irrigating the day before.
	BOOL_32			irrigate_on_29th_or_31st_bool;

	UNS_32			mow_day;

	// Used to determine which alerts to display when showing 'ALERTS TO SEE'.
	// Set at the start time on a watering day.
	DATE_TIME		last_ran_dt;

	// 8/18/2016 skc : flag to prevent sending multiple alerts at start time. Useful when
	// multiple stations exist in a station group.
	UNS_32			flag_budget_start_time_alert_sent;

	// 8/18/2016 skc : Renamed to "unused"
	UNS_32			unused_b;

	UNS_32			unused_c;

	BOOL_32			et_in_use;

	// 11/12/2015 ajv : Muddy Mondays - smoothed out the skip day effect in DailyET such that
	// we can now irrigate the same amount each day even following skip days. We take the
	// skip day water and spread it out evenly across all the irrigation days in the schedule.
	// This "ET AVERAGING" as we should probably call it can be enabled or disabled by program
	// in set-up. After your ROM update it will be enabled and controllers will ship with this
	// enabled when you first turn on daily ET.
	//
	// 4/8/2016 rmd : NOTE - ONLY affects the irrigated amount when irrigating using a weekly
	// schedule.
	BOOL_32			use_et_averaging_bool;

	BOOL_32			rain_in_use_bool;

	// The maximum amount of rain allowed to build up.
	UNS_32			soil_storage_capacity_inches_100u;

	BOOL_32			wind_in_use_bool;

	UNS_32			high_flow_action;

	UNS_32			low_flow_action;

	// The number of stations allowed to turn on a a time within this group
	UNS_32			on_at_a_time_in_group;

	// The number of stations allowed to turn on a a time within the system.
	UNS_32			on_at_a_time_in_system;

	// 2011.11.02 rmd : The dynamically maintained count of stations ON in this
	// group. Once per second (on average) the foal irrigation maintenance
	// function zeros and then updates this number. And uses it to DECIDE if
	// another valve from this group can turn ON.
	//
	// The system count is extracted by looking at the system count for each
	// station ON (that allowed value is part of this group but we could have
	// valve ON across several groups). And 'remembering' the lowest allowed
	// within system value (by sytem .. and therefore it is stored within the
	// system (file) list similarly to how this one is stored.
	//
	// We do not care about saving these numbers to the actual file system. It's
	// okay if it happens. Gee would we ever get into a position where we'll
	// read out a file in the midst of irrigation. I think not. But beware!
	UNS_32			presently_ON_group_count;

	// -------------------
	
	BOOL_32			pump_in_use;

	// -------------------

	UNS_32			line_fill_time_sec;
	UNS_32			delay_between_valve_time_sec;

	// -------------------

	BOOL_32			acquire_expecteds;

	// -------------------------------------------- //
	//  CHANGE BITS
	// -------------------------------------------- //

	CHANGE_BITS_64_BIT	changes_to_send_to_master;
	CHANGE_BITS_64_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_64_BIT	changes_to_upload_to_comm_server;

	// -------------------

	UNS_32			allowable_depletion_100u;
	
	// 9/12/2018 rmd : What is this ????
	UNS_32			available_water_100u;

	UNS_32			root_zone_depth_10u;

	UNS_32			species_factor_100u;

	UNS_32			density_factor_100u;

	UNS_32			microclimate_factor_100u;

	UNS_32			soil_intake_rate_100u;

	UNS_32			allowable_surface_accumulation_100u;

	// ----------
	
	// 4/30/2015 ajv : Added in Rev 3

	// ----------
	
	CHANGE_BITS_64_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

	// ----------
	
	// 9/9/2015 rmd : Added in Rev 4

	// ----------
	
	// 9/18/2015 rmd : A whole number which is a percentage of the scheduled irrigation. Range
	// is 0% to 100%.
	UNS_32			budget_reduction_percentage_cap;

	// ----------

	// 9/22/2015 ajv : Added in Rev 5

	// 9/22/2015 ajv : From this point forth, station groups are assigned to mainlines rather
	// than individual stations. This resolves a number of issues related to on-at-a-time, flow
	// checking, and budgets which were caused by a station group consisting of stations
	// spanning multiple mainlines.
	UNS_32			GID_irrigation_system;

	// 9/22/2015 ajv : By default, all mainline are "in use". If a user deletes a mainline, this
	// flag is set false. Although this keeps the item in memory and in the file, this is
	// necessary so the change can propagate through the chain and to the CommServer. If this
	// flag is FALSE, it will be hidden from view and completely inaccessible to the user.
	BOOL_32			in_use;

	// ----------
	
	// 4/8/2016 rmd : REV 6 - no changes.

	// 4/8/2016 rmd : NOTE - moving from revision 5 to 6 was done only to initialize the
	// ETAveraging variable to (true). There was NO structure SIZE or otherwise change.
	
	// ----------
	
	// 4/8/2016 rmd : Added in Rev 7

	// 4/8/2016 rmd : When 0 indicates no moisture sensor is to be used to control this station
	// group.
	UNS_32			moisture_sensor_serial_number;
	
	// ----------
	
	// 3/19/2015 ajv : NOTE! When a new variable is added to this structure, make sure you
	// include it in the STATION_GROUP_copy_group routine, found in station_groups.c.

};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_plant_type( void *const pgroup,
											 UNS_32 pplant_type,
											 const BOOL_32 pgenerate_change_line_bool,
											 const UNS_32 preason_for_change,
											 const UNS_32 pbox_index_0,
											 const BOOL_32 pset_change_bits,
											 CHANGE_BITS_64_BIT *pchange_bitfield_to_set
											 #ifdef _MSC_VER
											 , char *pmerge_update_str,
											 char *pmerge_insert_fields_str,
											 char *pmerge_insert_values_str
											 #endif
										   )
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->plant_type),
													 pplant_type,
													 STATION_GROUP_PLANT_TYPE_MIN,
													 STATION_GROUP_PLANT_TYPE_MAX,
													 STATION_GROUP_PLANT_TYPE_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_STATION_GROUP_PLANT_TYPE,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_plant_type_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_plant_type_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_head_type( void *const pgroup,
											UNS_32 phead_type,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0,
											const BOOL_32 pset_change_bits,
											CHANGE_BITS_64_BIT *pchange_bitfield_to_set
											#ifdef _MSC_VER
											, char *pmerge_update_str,
											char *pmerge_insert_fields_str,
											char *pmerge_insert_values_str
											#endif
										  )
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->head_type),
													 phead_type,
													 STATION_GROUP_HEAD_TYPE_MIN,
													 STATION_GROUP_HEAD_TYPE_MAX,
													 STATION_GROUP_HEAD_TYPE_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_STATION_GROUP_HEAD_TYPE,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_head_type_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_head_type_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_precip_rate( void *const pgroup,
											  UNS_32 pprecip_rate_in_100000u,
											  const BOOL_32 pgenerate_change_line_bool,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_64_BIT *pchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
											)
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->precip_rate_in_100000u),
													 pprecip_rate_in_100000u,
													 STATION_GROUP_PRECIP_RATE_MIN_100000u,
													 STATION_GROUP_PRECIP_RATE_MAX_100000u,
													 STATION_GROUP_PRECIP_RATE_DEFAULT_100000u,
													 pgenerate_change_line_bool,
													 CHANGE_STATION_GROUP_PRECIP_RATE,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_precip_rate_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_precip_rate_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_soil_type( void *const pgroup,
											UNS_32 psoil_type,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0,
											const BOOL_32 pset_change_bits,
											CHANGE_BITS_64_BIT *pchange_bitfield_to_set
											#ifdef _MSC_VER
											, char *pmerge_update_str,
											char *pmerge_insert_fields_str,
											char *pmerge_insert_values_str
											#endif
										  )
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->soil_type),
													 psoil_type,
													 STATION_GROUP_SOIL_TYPE_MIN,
													 STATION_GROUP_SOIL_TYPE_MAX,
													 STATION_GROUP_SOIL_TYPE_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_STATION_GROUP_SOIL_TYPE,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_soil_type_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_soil_type_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_slope_percentage( void *const pgroup,
												   UNS_32 pslope_percentage_100u,
												   const BOOL_32 pgenerate_change_line_bool,
												   const UNS_32 preason_for_change,
												   const UNS_32 pbox_index_0,
												   const BOOL_32 pset_change_bits,
												   CHANGE_BITS_64_BIT *pchange_bitfield_to_set
												   #ifdef _MSC_VER
												   , char *pmerge_update_str,
												   char *pmerge_insert_fields_str,
												   char *pmerge_insert_values_str
												   #endif
												 )
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->slope_percentage),
													 pslope_percentage_100u,
													 STATION_GROUP_SLOPE_PERCENTAGE_MIN,
													 STATION_GROUP_SLOPE_PERCENTAGE_MAX,
													 STATION_GROUP_SLOPE_PERCENTAGE_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_STATION_GROUP_SLOPE_PERCENTAGE,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_slope_percentage_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_slope_percentage_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												  );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_exposure( void *const pgroup,
										   UNS_32 pexposure,
										   const BOOL_32 pgenerate_change_line_bool,
										   const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const BOOL_32 pset_change_bits,
										   CHANGE_BITS_64_BIT *pchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , char *pmerge_update_str,
										   char *pmerge_insert_fields_str,
										   char *pmerge_insert_values_str
										   #endif
										 )
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->exposure),
													 pexposure,
													 STATION_GROUP_EXPOSURE_MIN,
													 STATION_GROUP_EXPOSURE_MAX,
													 STATION_GROUP_EXPOSURE_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_STATION_GROUP_EXPOSURE,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_exposure_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_exposure_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_usable_rain( void *const pgroup,
											  UNS_32 pusable_rain_100u,
											  const BOOL_32 pgenerate_change_line_bool,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_64_BIT *pchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
											)
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->usable_rain_percentage_100u),
													 pusable_rain_100u,
													 STATION_GROUP_USABLE_RAIN_PERCENTAGE_MIN,
													 STATION_GROUP_USABLE_RAIN_PERCENTAGE_MAX,
													 STATION_GROUP_USABLE_RAIN_PERCENTAGE_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_STATION_GROUP_USABLE_RAIN,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_usable_rain_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_usable_rain_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_crop_coefficient( void *const pgroup,
												   UNS_32 pmonth_0,
												   UNS_32 pcrop_coefficient_100u,
												   const BOOL_32 pgenerate_change_line_bool,
												   const UNS_32 preason_for_change,
												   const UNS_32 pbox_index_0,
												   const BOOL_32 pset_change_bits,
												   CHANGE_BITS_64_BIT *pchange_bitfield_to_set
												   #ifdef _MSC_VER
												   , char *pmerge_update_str,
												   char *pmerge_insert_fields_str,
												   char *pmerge_insert_values_str
												   #endif
												 )
{
	char	lfield_name[ 32 ];

	if( pmonth_0 < MONTHS_IN_A_YEAR )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_crop_coefficient_bit ], (pmonth_0 + 1) );

		SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
														 &(((STATION_GROUP_STRUCT*)pgroup)->crop_coefficient_100u[ pmonth_0 ]),
														 pcrop_coefficient_100u,
														 STATION_GROUP_CROP_COEFFICIENT_MIN_100u,
														 STATION_GROUP_CROP_COEFFICIENT_MAX_100u,
														 STATION_GROUP_CROP_COEFFICIENT_DEFAULT_100u,
														 pgenerate_change_line_bool,
														 (CHANGE_STATION_GROUP_CROP_COEFF_1 + pmonth_0),
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
														 STATION_GROUP_CHANGE_BITFIELD_crop_coefficient_bit,
														 lfield_name
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   );
	}
	else
	{
		#ifndef _MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
static void nm_PRIORITY_set_priority( void *const pgroup,
									  UNS_32 ppriority_level,
									  const BOOL_32 pgenerate_change_line_bool,
									  const UNS_32 preason_for_change,
									  const UNS_32 pbox_index_0,
									  const BOOL_32 pset_change_bits,
									  CHANGE_BITS_64_BIT *pchange_bitfield_to_set
									  #ifdef _MSC_VER
									  , char *pmerge_update_str,
									  char *pmerge_insert_fields_str,
									  char *pmerge_insert_values_str
									  #endif
									)
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->priority_level),
													 ppriority_level,
													 PRIORITY_LOW,
													 PRIORITY_HIGH,
													 PRIORITY_LEVEL_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_IRRIGATION_PRIORITY_LEVEL,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_priority_level_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_priority_level_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_PERCENT_ADJUST_set_percent( void *const pgroup,
										   UNS_32 ppercent_adjust_100u,
										   const BOOL_32 pgenerate_change_line_bool,
										   const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const BOOL_32 pset_change_bits,
										   CHANGE_BITS_64_BIT *pchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , char *pmerge_update_str,
										   char *pmerge_insert_fields_str,
										   char *pmerge_insert_values_str
										   #endif
										 )
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->percent_adjust_100u),
													 ppercent_adjust_100u,
													 PERCENT_ADJUST_PERCENT_MIN_100u,
													 PERCENT_ADJUST_PERCENT_MAX_100u,
													 PERCENT_ADJUST_PERCENT_DEFAULT_100u,
													 pgenerate_change_line_bool,
													 CHANGE_PERCENT_ADJUST,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_percent_adjust_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_percent_adjust_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_PERCENT_ADJUST_set_start_date( void *const pgroup,
											  UNS_32 pstart_date,
											  const BOOL_32 pgenerate_change_line_bool,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_64_BIT *pchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
											)
{
	SHARED_set_date_with_64_bit_change_bits_group( pgroup,
												   &(((STATION_GROUP_STRUCT*)pgroup)->percent_adjust_start_date),
												   pstart_date,
												   DATE_MINIMUM,
												   DATE_MAXIMUM,
												   DATE_MINIMUM,
												   pgenerate_change_line_bool,
												   CHANGE_PERCENT_ADJUST_START_DATE,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
												   STATION_GROUP_CHANGE_BITFIELD_percent_adjust_start_date_bit,
												   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_percent_adjust_start_date_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
static void nm_PERCENT_ADJUST_set_end_date( void *const pgroup,
											UNS_32 pend_date,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0,
											const BOOL_32 pset_change_bits,
											CHANGE_BITS_64_BIT *pchange_bitfield_to_set
											#ifdef _MSC_VER
											, char *pmerge_update_str,
											char *pmerge_insert_fields_str,
											char *pmerge_insert_values_str
											#endif
										  )
{
	SHARED_set_date_with_64_bit_change_bits_group( pgroup,
												   &(((STATION_GROUP_STRUCT*)pgroup)->percent_adjust_end_date),
												   pend_date,
												   DATE_MINIMUM,
												   DATE_MAXIMUM,
												   DATE_MINIMUM,
												   pgenerate_change_line_bool,
												   CHANGE_PERCENT_ADJUST_END_DATE,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
												   STATION_GROUP_CHANGE_BITFIELD_percent_adjust_end_date_bit,
												   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_percent_adjust_end_date_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
static void nm_SCHEDULE_set_enabled( void *const pgroup,
									 BOOL_32 penabled_bool,
									 const BOOL_32 pgenerate_change_line_bool,
									 const UNS_32 preason_for_change,
									 const UNS_32 pbox_index_0,
									 const BOOL_32 pset_change_bits,
									 CHANGE_BITS_64_BIT *pchange_bitfield_to_set
									 #ifdef _MSC_VER
									 , char *pmerge_update_str,
									 char *pmerge_insert_fields_str,
									 char *pmerge_insert_values_str
									 #endif
								   )
{
	SHARED_set_bool_with_64_bit_change_bits_group( pgroup,
												   &(((STATION_GROUP_STRUCT*)pgroup)->schedule_enabled_bool),
												   penabled_bool,
												   SCHEDULE_ENABLED_DEFAULT,
												   pgenerate_change_line_bool,
												   CHANGE_IRRIGATION_SCHEDULE_ENABLED,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
												   STATION_GROUP_CHANGE_BITFIELD_schedule_enabled_bit,
												   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_schedule_enabled_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
static void nm_SCHEDULE_set_start_time( void *const pgroup,
										UNS_32 pstart_time,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const BOOL_32 pset_change_bits,
										CHANGE_BITS_64_BIT *pchange_bitfield_to_set
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
									  )
{
	SHARED_set_time_with_64_bit_change_bits_group( pgroup,
												   &(((STATION_GROUP_STRUCT*)pgroup)->start_time),
												   pstart_time,
												   SCHEDULE_START_TIME_MIN,
												   SCHEDULE_OFF_TIME,	// 6/17/2015 ajv : Note that this is not SCHEDULE_START_TIME_MAX. This is intentional as a time of 86400 is valid when storing as it indicates OFF.
												   SCHEDULE_START_TIME_DEFAULT,
												   pgenerate_change_line_bool,
												   CHANGE_IRRIGATION_SCHEDULE_START_TIME,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
												   STATION_GROUP_CHANGE_BITFIELD_start_time_bit,
												   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_start_time_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
static void nm_SCHEDULE_set_stop_time( void *const pgroup,
									   UNS_32 pstop_time,
									   const BOOL_32 pgenerate_change_line_bool,
									   const UNS_32 preason_for_change,
									   const UNS_32 pbox_index_0,
									   const BOOL_32 pset_change_bits,
									   CHANGE_BITS_64_BIT *pchange_bitfield_to_set
									   #ifdef _MSC_VER
									   , char *pmerge_update_str,
									   char *pmerge_insert_fields_str,
									   char *pmerge_insert_values_str
									   #endif
									 )
{
	SHARED_set_time_with_64_bit_change_bits_group( pgroup,
												   &(((STATION_GROUP_STRUCT*)pgroup)->stop_time),
												   pstop_time,
												   SCHEDULE_STOP_TIME_MIN,
												   SCHEDULE_STOP_TIME_MAX,
												   SCHEDULE_STOP_TIME_DEFAULT,
												   pgenerate_change_line_bool,
												   CHANGE_IRRIGATION_SCHEDULE_STOP_TIME,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
												   STATION_GROUP_CHANGE_BITFIELD_stop_time_bit,
												   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_stop_time_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
static void nm_SCHEDULE_set_mow_day( void *const pgroup,
									 UNS_32 pmow_day,
									 const BOOL_32 pgenerate_change_line_bool,
									 const UNS_32 preason_for_change,
									 const UNS_32 pbox_index_0,
									 const BOOL_32 pset_change_bits,
									 CHANGE_BITS_64_BIT *pchange_bitfield_to_set
									 #ifdef _MSC_VER
									 , char *pmerge_update_str,
									 char *pmerge_insert_fields_str,
									 char *pmerge_insert_values_str
									 #endif
								   )
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->mow_day),
													 pmow_day,
													 SCHEDULE_MOWDAY_MIN,
													 SCHEDULE_MOWDAY_MAX,
													 SCHEDULE_MOWDAY_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_IRRIGATION_SCHEDULE_MOW_DAY,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_mow_day_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_mow_day_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_SCHEDULE_set_schedule_type( void *const pgroup,
										   UNS_32 pschedule_type,
										   const BOOL_32 pgenerate_change_line_bool,
										   const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const BOOL_32 pset_change_bits,
										   CHANGE_BITS_64_BIT *pchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , char *pmerge_update_str,
										   char *pmerge_insert_fields_str,
										   char *pmerge_insert_values_str
										   #endif
										 )
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->schedule_type),
													 pschedule_type,
													 SCHEDULE_TYPE_MIN,
													 SCHEDULE_TYPE_MAX,
													 SCHEDULE_TYPE_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_IRRIGATION_SCHEDULE_TYPE,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_schedule_type_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_schedule_type_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_SCHEDULE_set_water_day( void *const pgroup,
									   const UNS_32 pday,
									   BOOL_32 pis_a_water_day_bool,
									   const BOOL_32 pgenerate_change_line_bool,
									   const UNS_32 preason_for_change,
									   const UNS_32 pbox_index_0,
									   const BOOL_32 pset_change_bits,
									   CHANGE_BITS_64_BIT *pchange_bitfield_to_set
									   #ifdef _MSC_VER
									   , char *pmerge_update_str,
									   char *pmerge_insert_fields_str,
									   char *pmerge_insert_values_str
									   #endif
									 )
{
	char	lfield_name[ 32 ];

	if( pday < DAYS_IN_A_WEEK )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%s", STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_water_days_bit ], GetDayShortStr( pday ) );

		SHARED_set_bool_with_64_bit_change_bits_group( pgroup,
													   &(((STATION_GROUP_STRUCT*)pgroup)->water_days_bool[ pday ]),
													   pis_a_water_day_bool,
													   SCHEDULE_WATERDAY_DEFAULT,
													   pgenerate_change_line_bool,
													   (CHANGE_IRRIGATION_SCHEDULE_WATER_DAY_SUN + pday),
													   preason_for_change,
													   pbox_index_0,
													   pset_change_bits,
													   pchange_bitfield_to_set,
													   &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													   STATION_GROUP_CHANGE_BITFIELD_water_days_bit,
													   lfield_name
													   #ifdef _MSC_VER
													   , pmerge_update_str,
													   pmerge_insert_fields_str,
													   pmerge_insert_values_str
													   #endif
													 );
	}
	else
	{
		#ifndef _MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
extern void nm_SCHEDULE_set_begin_date( void *const pgroup,
										UNS_32 pbegin_date,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const BOOL_32 pset_change_bits,
										CHANGE_BITS_64_BIT *pchange_bitfield_to_set
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
									  )
{
	SHARED_set_date_with_64_bit_change_bits_group( pgroup,
												   &(((STATION_GROUP_STRUCT*)pgroup)->begin_date),
												   pbegin_date,
												   DATE_MINIMUM,
												   DATE_MAXIMUM,
												   DATE_MINIMUM,
												   pgenerate_change_line_bool,
												   CHANGE_IRRIGATION_SCHEDULE_BEGIN_DATE,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
												   STATION_GROUP_CHANGE_BITFIELD_begin_date_bit,
												   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_begin_date_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
static void nm_SCHEDULE_set_irrigate_on_29th_or_31st( void *const pgroup,
													  BOOL_32 pirrigate_on_29th_or_31st_bool,
													  const BOOL_32 pgenerate_change_line_bool,
													  const UNS_32 preason_for_change,
													  const UNS_32 pbox_index_0,
													  const BOOL_32 pset_change_bits,
													  CHANGE_BITS_64_BIT *pchange_bitfield_to_set
													  #ifdef _MSC_VER
													  , char *pmerge_update_str,
													  char *pmerge_insert_fields_str,
													  char *pmerge_insert_values_str
													  #endif
													)
{
	SHARED_set_bool_with_64_bit_change_bits_group( pgroup,
												   &(((STATION_GROUP_STRUCT*)pgroup)->irrigate_on_29th_or_31st_bool),
												   pirrigate_on_29th_or_31st_bool,
												   SCHEDULE_IRRIGATEON29THOR31ST_DEFAULT,
												   pgenerate_change_line_bool,
												   CHANGE_IRRIGATION_SCHEDULE_IRRIGATE_ON_29_or_31,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
												   STATION_GROUP_CHANGE_BITFIELD_irrigate_on_29th_or_31st_bit,
												   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_irrigate_on_29th_or_31st_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
extern void nm_SCHEDULE_set_last_ran( void *const pgroup, 
									  const DATE_TIME plast_ran_dt,
									  const BOOL_32 pgenerate_change_line_bool,
									  const UNS_32 preason_for_change,
									  const UNS_32 pbox_index_0,
									  const BOOL_32 pset_change_bits,
									  CHANGE_BITS_64_BIT *pchange_bitfield_to_set
									  #ifdef _MSC_VER
									  , char *pmerge_update_str,
									  char *pmerge_insert_fields_str,
									  char *pmerge_insert_values_str
									  #endif
									)
{
	SHARED_set_date_time_with_64_bit_change_bits_group( pgroup,
														&(((STATION_GROUP_STRUCT*)pgroup)->last_ran_dt),
														plast_ran_dt,
														plast_ran_dt, // TODO Determine minimum allowed value
														plast_ran_dt, // TODO Determine maximum allowed value
														plast_ran_dt, // TODO Determine default value in case range check fails
														pgenerate_change_line_bool,
														preason_for_change,
														pbox_index_0,
														pset_change_bits,
														pchange_bitfield_to_set,
														&(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
														STATION_GROUP_CHANGE_BITFIELD_last_ran_bit,
														STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_last_ran_bit ]
														#ifdef _MSC_VER
														, pmerge_update_str,
														pmerge_insert_fields_str,
														pmerge_insert_values_str
														#endif
													  );
}

/* ---------------------------------------------------------- */
static void nm_DAILY_ET_set_ET_in_use( void *const pgroup,
									   BOOL_32 pet_in_use,
									   const BOOL_32 pgenerate_change_line_bool,
									   const UNS_32 preason_for_change,
									   const UNS_32 pbox_index_0,
									   const BOOL_32 pset_change_bits,
									   CHANGE_BITS_64_BIT *pchange_bitfield_to_set
									   #ifdef _MSC_VER
									   , char *pmerge_update_str,
									   char *pmerge_insert_fields_str,
									   char *pmerge_insert_values_str
									   #endif
								     )
{ 
	SHARED_set_bool_with_64_bit_change_bits_group( pgroup,
												   &(((STATION_GROUP_STRUCT*)pgroup)->et_in_use),
												   pet_in_use,
												   WEATHER_ET_MODE_DEFAULT,
												   pgenerate_change_line_bool,
												   CHANGE_ET_MODE,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
												   STATION_GROUP_CHANGE_BITFIELD_et_in_use,
												   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_et_in_use ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
static void nm_DAILY_ET_set_use_ET_averaging( void *const pgroup,
											  BOOL_32 puse_et_averaging_bool,
											  const BOOL_32 pgenerate_change_line_bool,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_64_BIT *pchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
											)
{ 
	SHARED_set_bool_with_64_bit_change_bits_group( pgroup,
												   &(((STATION_GROUP_STRUCT*)pgroup)->use_et_averaging_bool),
												   puse_et_averaging_bool,
												   WEATHER_ET_AVERAGING_IN_USE_DEFAULT,
												   pgenerate_change_line_bool,
												   CHANGE_ET_USE_ET_AVERAGING,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
												   STATION_GROUP_CHANGE_BITFIELD_use_et_averaging,
												   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_use_et_averaging ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
static void nm_RAIN_set_rain_in_use( void *const pgroup,
									 BOOL_32 prain_in_use_bool,
									 const BOOL_32 pgenerate_change_line_bool,
									 const UNS_32 preason_for_change,
									 const UNS_32 pbox_index_0,
									 const BOOL_32 pset_change_bits,
									 CHANGE_BITS_64_BIT *pchange_bitfield_to_set
									 #ifdef _MSC_VER
									 , char *pmerge_update_str,
									 char *pmerge_insert_fields_str,
									 char *pmerge_insert_values_str
									 #endif
								   )
{
	SHARED_set_bool_with_64_bit_change_bits_group( pgroup,
												   &(((STATION_GROUP_STRUCT*)pgroup)->rain_in_use_bool),
												   prain_in_use_bool,
												   WEATHER_RAIN_IN_USE_DEFAULT,
												   pgenerate_change_line_bool,
												   CHANGE_RAIN_IN_USE,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
												   STATION_GROUP_CHANGE_BITFIELD_rain_in_use_bit,
												   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_rain_in_use_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_soil_storage_capacity( void *const pgroup,
														UNS_32 psoil_storage_capacity_100u,
														const BOOL_32 pgenerate_change_line_bool,
														const UNS_32 preason_for_change,
														const UNS_32 pbox_index_0,
														const BOOL_32 pset_change_bits,
														CHANGE_BITS_64_BIT *pchange_bitfield_to_set
														#ifdef _MSC_VER
														, char *pmerge_update_str,
														char *pmerge_insert_fields_str,
														char *pmerge_insert_values_str
														#endif
													   )
{
	if (SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
														 &(((STATION_GROUP_STRUCT*)pgroup)->soil_storage_capacity_inches_100u),
														 psoil_storage_capacity_100u,
														 STATION_GROUP_SOIL_STORAGE_CAPACITY_MIN_100u,
														 STATION_GROUP_SOIL_STORAGE_CAPACITY_MAX_100u,
														 STATION_GROUP_SOIL_STORAGE_CAPACITY_DEFAULT_100u,
														 pgenerate_change_line_bool,
														 CHANGE_SOIL_STORAGE_CAPACITY,
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
														 STATION_GROUP_CHANGE_BITFIELD_soil_storage_capacity_bit,
														 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_soil_storage_capacity_bit ]
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   ) == (true) )
	{
		#ifndef	_MSC_VER

			STATION_STRUCT	*lstation;

			// 3/14/2017 ajv : If the soil storage capacity has changed, chances are the Moisture
			// Balance is no longer valid. Therefore, allow the next irrigation to go through, ignoring
			// the current MB value, and snap it back to 50% when it's done.
			// 
			// This only has to be done at the master because it will sync the changes back down the
			// chain.
			if( FLOWSENSE_we_are_a_master_one_way_or_another() )
			{
				lstation = nm_ListGetFirst( &station_info_list_hdr );

				while( lstation != NULL )
				{
					if( STATION_get_GID_station_group(lstation) == (((STATION_GROUP_STRUCT*)pgroup)->base.group_identity_number) )
					{
						nm_STATION_set_ignore_moisture_balance_at_next_irrigation( lstation, STATION_MOISTURE_BALANCE_SET_IGNORE_FLAG, CHANGE_do_not_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, STATION_get_change_bits_ptr(lstation, preason_for_change) );
					}

					lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
				}
			}

		#endif
	}
}

/* ---------------------------------------------------------- */
static void nm_WIND_set_wind_in_use( void *const pgroup,
									 BOOL_32 pwind_in_use_bool,
									 const BOOL_32 pgenerate_change_line_bool,
									 const UNS_32 preason_for_change,
									 const UNS_32 pbox_index_0,
									 const BOOL_32 pset_change_bits,
									 CHANGE_BITS_64_BIT *pchange_bitfield_to_set
									 #ifdef _MSC_VER
									 , char *pmerge_update_str,
									 char *pmerge_insert_fields_str,
									 char *pmerge_insert_values_str
									 #endif
								   )
{
	SHARED_set_bool_with_64_bit_change_bits_group( pgroup,
												   &(((STATION_GROUP_STRUCT*)pgroup)->wind_in_use_bool),
												   pwind_in_use_bool,
												   WEATHER_WIND_IN_USE_DEFAULT,
												   pgenerate_change_line_bool,
												   CHANGE_WIND_IN_USE,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
												   STATION_GROUP_CHANGE_BITFIELD_wind_in_use_bit,
												   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_wind_in_use_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
static void nm_ALERT_ACTIONS_set_high_flow_action( void *const pgroup,
												   UNS_32 phigh_flow_action,
												   const BOOL_32 pgenerate_change_line_bool,
												   const UNS_32 preason_for_change,
												   const UNS_32 pbox_index_0,
												   const BOOL_32 pset_change_bits,
												   CHANGE_BITS_64_BIT *pchange_bitfield_to_set
												   #ifdef _MSC_VER
												   , char *pmerge_update_str,
												   char *pmerge_insert_fields_str,
												   char *pmerge_insert_values_str
												   #endif
												 )
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->high_flow_action),
													 phigh_flow_action,
													 ALERT_ACTIONS_NONE,
													 ALERT_ACTIONS_ALERT_SHUTOFF,
													 ALERT_ACTIONS_HIGH_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_ALERT_ACTION_HIGH,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_high_flow_action_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_high_flow_action_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_ALERT_ACTIONS_set_low_flow_action( void *const pgroup,
												  UNS_32 plow_flow_action,
												  const BOOL_32 pgenerate_change_line_bool,
												  const UNS_32 preason_for_change,
												  const UNS_32 pbox_index_0,
												  const BOOL_32 pset_change_bits,
												  CHANGE_BITS_64_BIT *pchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , char *pmerge_update_str,
												  char *pmerge_insert_fields_str,
												  char *pmerge_insert_values_str
												  #endif
												)
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->low_flow_action),
													 plow_flow_action,
													 ALERT_ACTIONS_NONE,
													 ALERT_ACTIONS_ALERT_SHUTOFF,
													 ALERT_ACTIONS_LOW_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_ALERT_ACTION_LOW,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_low_flow_action_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_low_flow_action_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_ON_AT_A_TIME_set_on_at_a_time_in_group( void *const pgroup,
													   UNS_32 pon_at_a_time_in_group,
													   const BOOL_32 pgenerate_change_line_bool,
													   const UNS_32 preason_for_change,
													   const UNS_32 pbox_index_0,
													   const BOOL_32 pset_change_bits,
													   CHANGE_BITS_64_BIT *pchange_bitfield_to_set
													   #ifdef _MSC_VER
													   , char *pmerge_update_str,
													   char *pmerge_insert_fields_str,
													   char *pmerge_insert_values_str
													   #endif
													 )
{
	SHARED_set_on_at_a_time_group( pgroup,
								   &(((STATION_GROUP_STRUCT*)pgroup)->on_at_a_time_in_group),
								   pon_at_a_time_in_group,
								   ON_AT_A_TIME_MIN,
								   ON_AT_A_TIME_ABSOLUTE_MAXIMUM,
								   ON_AT_A_TIME_DEFAULT,
								   pgenerate_change_line_bool,
								   CHANGE_ON_AT_A_TIME_IN_GROUP,
								   preason_for_change,
								   pbox_index_0,
								   pset_change_bits,
								   pchange_bitfield_to_set,
								   &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
								   STATION_GROUP_CHANGE_BITFIELD_on_at_a_time_in_group_bit,
								   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_on_at_a_time_in_group_bit ]
								   #ifdef _MSC_VER
								   , pmerge_update_str,
								   pmerge_insert_fields_str,
								   pmerge_insert_values_str
								   #endif
								 );
}

/* ---------------------------------------------------------- */
static void nm_ON_AT_A_TIME_set_on_at_a_time_in_system( void *const pgroup,
														UNS_32 pon_at_a_time_in_system,
														const BOOL_32 pgenerate_change_line_bool,
														const UNS_32 preason_for_change,
														const UNS_32 pbox_index_0,
														const BOOL_32 pset_change_bits,
														CHANGE_BITS_64_BIT *pchange_bitfield_to_set
														#ifdef _MSC_VER
														, char *pmerge_update_str,
														char *pmerge_insert_fields_str,
														char *pmerge_insert_values_str
														#endif
													  )
{
	SHARED_set_on_at_a_time_group( pgroup,
								   &(((STATION_GROUP_STRUCT*)pgroup)->on_at_a_time_in_system),
								   pon_at_a_time_in_system,
								   ON_AT_A_TIME_MIN,
								   ON_AT_A_TIME_ABSOLUTE_MAXIMUM,
								   ON_AT_A_TIME_DEFAULT,
								   pgenerate_change_line_bool,
								   CHANGE_ON_AT_A_TIME_IN_SYSTEM,
								   preason_for_change,
								   pbox_index_0,
								   pset_change_bits,
								   pchange_bitfield_to_set,
								   &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
								   STATION_GROUP_CHANGE_BITFIELD_on_at_a_time_in_system_bit,
								   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_on_at_a_time_in_system_bit ]
								   #ifdef _MSC_VER
								   , pmerge_update_str,
								   pmerge_insert_fields_str,
								   pmerge_insert_values_str
								   #endif
								 );
}

/* ---------------------------------------------------------- */
static void nm_PUMP_set_pump_in_use( void *const pgroup,
									 BOOL_32 ppump_in_use_bool,
									 const BOOL_32 pgenerate_change_line_bool,
									 const UNS_32 preason_for_change,
									 const UNS_32 pbox_index_0,
									 const BOOL_32 pset_change_bits,
									 CHANGE_BITS_64_BIT *pchange_bitfield_to_set
									 #ifdef _MSC_VER
									 , char *pmerge_update_str,
									 char *pmerge_insert_fields_str,
									 char *pmerge_insert_values_str
									 #endif
								   )
{
	SHARED_set_bool_with_64_bit_change_bits_group( pgroup,
												   &(((STATION_GROUP_STRUCT*)pgroup)->pump_in_use),
												   ppump_in_use_bool,
												   PUMP_INUSE_DEFAULT,
												   pgenerate_change_line_bool,
												   CHANGE_PUMP_IN_USE,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
												   STATION_GROUP_CHANGE_BITFIELD_pump_in_use_bit,
												   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_pump_in_use_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
static void nm_LINE_FILL_TIME_set_line_fill_time( void *const pgroup,
												  UNS_32 pline_fill_time_sec,
												  const BOOL_32 pgenerate_change_line_bool,
												  const UNS_32 preason_for_change,
												  const UNS_32 pbox_index_0,
												  const BOOL_32 pset_change_bits,
												  CHANGE_BITS_64_BIT *pchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , char *pmerge_update_str,
												  char *pmerge_insert_fields_str,
												  char *pmerge_insert_values_str
												  #endif
												)
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->line_fill_time_sec),
													 pline_fill_time_sec,
													 LINE_FILL_TIME_MIN,
													 LINE_FILL_TIME_MAX,
													 LINE_FILL_TIME_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_LINE_FILL_TIME,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_line_fill_time_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_line_fill_time_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_DELAY_BETWEEN_VALVES_set_delay_time( void *const pgroup,
													UNS_32 pdelay_between_valve_time_sec,
													const BOOL_32 pgenerate_change_line_bool,
													const UNS_32 preason_for_change,
													const UNS_32 pbox_index_0,
													const BOOL_32 pset_change_bits,
													CHANGE_BITS_64_BIT *pchange_bitfield_to_set
													#ifdef _MSC_VER
													, char *pmerge_update_str,
													char *pmerge_insert_fields_str,
													char *pmerge_insert_values_str
													#endif
												  )
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->delay_between_valve_time_sec),
													 pdelay_between_valve_time_sec,
													 DELAY_BETWEEN_VALVES_MIN,
													 DELAY_BETWEEN_VALVES_MAX,
													 DELAY_BETWEEN_VALVES_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_DELAY_BETWEEN_VALVES,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_delay_between_valves_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_delay_between_valves_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												  );
}

/* ---------------------------------------------------------- */
extern void nm_ACQUIRE_EXPECTEDS_set_acquire_expected( void *const pgroup,
													   BOOL_32 pacquire_expecteds,
													   const BOOL_32 pgenerate_change_line_bool,
													   const UNS_32 preason_for_change,
													   const UNS_32 pbox_index_0,
													   const BOOL_32 pset_change_bits,
													   CHANGE_BITS_64_BIT *pchange_bitfield_to_set
													   #ifdef _MSC_VER
													   , char *pmerge_update_str,
													   char *pmerge_insert_fields_str,
													   char *pmerge_insert_values_str
													   #endif
													 )
{
	SHARED_set_bool_with_64_bit_change_bits_group( pgroup,
												   &(((STATION_GROUP_STRUCT*)pgroup)->acquire_expecteds),
												   pacquire_expecteds,
												   ACQUIRE_EXPECTEDS_DEFAULT,
												   pgenerate_change_line_bool,
												   CHANGE_ACQUIRE_EXPECTEDS,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
												   STATION_GROUP_CHANGE_BITFIELD_acquire_at_start_bit,
												   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_acquire_at_start_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_allowable_depletion( void *const pgroup,
													  UNS_32 pallowable_depletion,
													  const BOOL_32 pgenerate_change_line_bool,
													  const UNS_32 preason_for_change,
													  const UNS_32 pbox_index_0,
													  const BOOL_32 pset_change_bits,
													  CHANGE_BITS_64_BIT *pchange_bitfield_to_set
													  #ifdef _MSC_VER
													  , char *pmerge_update_str,
													  char *pmerge_insert_fields_str,
													  char *pmerge_insert_values_str
													  #endif
													)
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->allowable_depletion_100u),
													 pallowable_depletion,
													 STATION_GROUP_ALLOWABLE_DEPLETION_MIN_100u,
													 STATION_GROUP_ALLOWABLE_DEPLETION_MAX_100u,
													 STATION_GROUP_ALLOWABLE_DEPLETION_DEFAULT_100u,
													 pgenerate_change_line_bool,
													 CHANGE_STATION_GROUP_ALLOWABLE_DEPLETION,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_allowable_depletion_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_allowable_depletion_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												  );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_available_water( void *const pgroup,
												  UNS_32 pavailable_water,
												  const BOOL_32 pgenerate_change_line_bool,
												  const UNS_32 preason_for_change,
												  const UNS_32 pbox_index_0,
												  const BOOL_32 pset_change_bits,
												  CHANGE_BITS_64_BIT *pchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , char *pmerge_update_str,
												  char *pmerge_insert_fields_str,
												  char *pmerge_insert_values_str
												  #endif
												)
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->available_water_100u),
													 pavailable_water,
													 STATION_GROUP_AVAILABLE_WATER_MIN_100u,
													 STATION_GROUP_AVAILABLE_WATER_MAX_100u,
													 STATION_GROUP_AVAILABLE_WATER_DEFAULT_100u,
													 pgenerate_change_line_bool,
													 CHANGE_STATION_GROUP_AVAILABLE_WATER,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_available_water_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_available_water_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												  );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_root_zone_depth( void *const pgroup,
												  UNS_32 proot_zone_depth_10u,
												  const BOOL_32 pgenerate_change_line_bool,
												  const UNS_32 preason_for_change,
												  const UNS_32 pbox_index_0,
												  const BOOL_32 pset_change_bits,
												  CHANGE_BITS_64_BIT *pchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , char *pmerge_update_str,
												  char *pmerge_insert_fields_str,
												  char *pmerge_insert_values_str
												  #endif
												)
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->root_zone_depth_10u),
													 proot_zone_depth_10u,
													 STATION_GROUP_ROOT_ZONE_DEPTH_MIN_10u,
													 STATION_GROUP_ROOT_ZONE_DEPTH_MAX_10u,
													 STATION_GROUP_ROOT_ZONE_DEPTH_DEFAULT_10u,
													 pgenerate_change_line_bool,
													 CHANGE_STATION_GROUP_ROOT_ZONE_DEPTH,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_root_zone_depth_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_root_zone_depth_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												  );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_species_factor( void *const pgroup,
												 UNS_32 pspecies_factor_100u,
												 const BOOL_32 pgenerate_change_line_bool,
												 const UNS_32 preason_for_change,
												 const UNS_32 pbox_index_0,
												 const BOOL_32 pset_change_bits,
												 CHANGE_BITS_64_BIT *pchange_bitfield_to_set
												 #ifdef _MSC_VER
												 , char *pmerge_update_str,
												 char *pmerge_insert_fields_str,
												 char *pmerge_insert_values_str
												 #endif
												)
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->species_factor_100u),
													 pspecies_factor_100u,
													 STATION_GROUP_SPECIES_FACTOR_MIN_100u,
													 STATION_GROUP_SPECIES_FACTOR_MAX_100u,
													 STATION_GROUP_SPECIES_FACTOR_DEFAULT_100u,
													 pgenerate_change_line_bool,
													 CHANGE_STATION_GROUP_SPECIES_FACTOR,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_species_factor_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_species_factor_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												  );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_density_factor( void *const pgroup,
												 UNS_32 pdensity_factor_100u,
												 const BOOL_32 pgenerate_change_line_bool,
												 const UNS_32 preason_for_change,
												 const UNS_32 pbox_index_0,
												 const BOOL_32 pset_change_bits,
												 CHANGE_BITS_64_BIT *pchange_bitfield_to_set
												 #ifdef _MSC_VER
												 , char *pmerge_update_str,
												 char *pmerge_insert_fields_str,
												 char *pmerge_insert_values_str
												 #endif
												)
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->density_factor_100u),
													 pdensity_factor_100u,
													 STATION_GROUP_DENSITY_FACTOR_MIN_100u,
													 STATION_GROUP_DENSITY_FACTOR_MAX_100u,
													 STATION_GROUP_DENSITY_FACTOR_DEFAULT_100u,
													 pgenerate_change_line_bool,
													 CHANGE_STATION_GROUP_DENSITY_FACTOR,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_density_factor_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_density_factor_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												  );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_microclimate_factor( void *const pgroup,
													  UNS_32 pmicroclimate_factor_100u,
													  const BOOL_32 pgenerate_change_line_bool,
													  const UNS_32 preason_for_change,
													  const UNS_32 pbox_index_0,
													  const BOOL_32 pset_change_bits,
													  CHANGE_BITS_64_BIT *pchange_bitfield_to_set
													  #ifdef _MSC_VER
													  , char *pmerge_update_str,
													  char *pmerge_insert_fields_str,
													  char *pmerge_insert_values_str
													  #endif
													)
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->microclimate_factor_100u),
													 pmicroclimate_factor_100u,
													 STATION_GROUP_MICROCLIMATE_FACTOR_MIN_100u,
													 STATION_GROUP_MICROCLIMATE_FACTOR_MAX_100u,
													 STATION_GROUP_MICROCLIMATE_FACTOR_DEFAULT_100u,
													 pgenerate_change_line_bool,
													 CHANGE_STATION_GROUP_MICROCLIMATE_FACTOR,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_microclimate_factor_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_microclimate_factor_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												  );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_soil_intake_rate( void *const pgroup,
													  UNS_32 psoil_intake_rate_100u,
													  const BOOL_32 pgenerate_change_line_bool,
													  const UNS_32 preason_for_change,
													  const UNS_32 pbox_index_0,
													  const BOOL_32 pset_change_bits,
													  CHANGE_BITS_64_BIT *pchange_bitfield_to_set
													  #ifdef _MSC_VER
													  , char *pmerge_update_str,
													  char *pmerge_insert_fields_str,
													  char *pmerge_insert_values_str
													  #endif
													)
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->soil_intake_rate_100u),
													 psoil_intake_rate_100u,
													 STATION_GROUP_SOIL_INTAKE_RATE_MIN_100u,
													 STATION_GROUP_SOIL_INTAKE_RATE_MAX_100u,
													 STATION_GROUP_SOIL_INTAKE_RATE_DEFAULT_100u,
													 pgenerate_change_line_bool,
													 CHANGE_STATION_GROUP_SOIL_INTAKE_RATE,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_soil_intake_rate_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_soil_intake_rate_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												  );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_allowable_surface_accumulation( void *const pgroup,
																 UNS_32 pallowable_surface_accumulation_100u,
																 const BOOL_32 pgenerate_change_line_bool,
																 const UNS_32 preason_for_change,
																 const UNS_32 pbox_index_0,
																 const BOOL_32 pset_change_bits,
																 CHANGE_BITS_64_BIT *pchange_bitfield_to_set
																 #ifdef _MSC_VER
																 , char *pmerge_update_str,
																 char *pmerge_insert_fields_str,
																 char *pmerge_insert_values_str
																 #endif
															   )
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->allowable_surface_accumulation_100u),
													 pallowable_surface_accumulation_100u,
													 STATION_GROUP_ALLOWABLE_SURFACE_ACCUM_MIN_100u,
													 STATION_GROUP_ALLOWABLE_SURFACE_ACCUM_MAX_100u,
													 STATION_GROUP_ALLOWABLE_SURFACE_ACCUM_DEFAULT_100u,
													 pgenerate_change_line_bool,
													 CHANGE_STATION_GROUP_ALLOWABLE_SURFACE_ACCUM,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_allowable_surface_accum_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_allowable_surface_accum_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												  );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_budget_reduction_percentage_cap(	void *const pgroup,
																	UNS_32 preduction_cap,
																	const BOOL_32 pgenerate_change_line_bool,
																	const UNS_32 preason_for_change,
																	const UNS_32 pbox_index_0,
																	const BOOL_32 pset_change_bits,
																	CHANGE_BITS_64_BIT *pchange_bitfield_to_set
																	#ifdef _MSC_VER
																	, char *pmerge_update_str,
																	char *pmerge_insert_fields_str,
																	char *pmerge_insert_values_str
																	#endif
															    )
{
	SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
													 &(((STATION_GROUP_STRUCT*)pgroup)->budget_reduction_percentage_cap),
													 preduction_cap,
													 STATION_GROUP_BUDGET_PERCENTAGE_CAP_MIN,
													 STATION_GROUP_BUDGET_PERCENTAGE_CAP_MAX,
													 STATION_GROUP_BUDGET_PERCENTAGE_CAP_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_STATION_GROUP_BUDGET_REDUCTION_CAP,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
													 STATION_GROUP_CHANGE_BITFIELD_budget_reduction_cap_bit,
													 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_budget_reduction_cap_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_GID_irrigation_system(	void *const pgroup,
												UNS_32 pGID_irrigation_system,
												const BOOL_32 pgenerate_change_line_bool,
												const UNS_32 preason_for_change,
												const UNS_32 pbox_index_0,
												const BOOL_32 pset_change_bits,
												CHANGE_BITS_64_BIT *pchange_bitfield_to_set
												#ifdef _MSC_VER
												, char *pmerge_update_str,
												char *pmerge_insert_fields_str,
												char *pmerge_insert_values_str
												#endif
											)
{
	if( SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
														 &(((STATION_GROUP_STRUCT*)pgroup)->GID_irrigation_system),
														 pGID_irrigation_system,
														 1,
														 UNS_32_MAX,
														 #ifndef _MSC_VER
															// 9/23/2015 ajv : The CommServer doesn't have access to this function, so set the default
															// to 1 for now until we figure out a better solution.
															nm_GROUP_get_group_ID( SYSTEM_get_group_at_this_index( 0 ) ),
														 #else
															1,
														 #endif
														 CHANGE_do_not_generate_change_line,
														 CHANGE_STATION_GROUP_SYSTEM_GID,
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
														 STATION_GROUP_CHANGE_BITFIELD_GID_irrigation_system_bit,
														 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_GID_irrigation_system_bit ]
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													) == (true) )

	{
		#ifndef	_MSC_VER

			IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

			xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

			lsystem = SYSTEM_get_group_with_this_GID( pGID_irrigation_system );

			if( pgenerate_change_line_bool && lsystem )
			{
				Alert_station_group_assigned_to_mainline( nm_GROUP_get_name(pgroup), nm_GROUP_get_name(lsystem), preason_for_change, pbox_index_0 );
			}

			xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

		#endif
	}
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_in_use(	void *const pgroup,
											BOOL_32 pin_use,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0,
											const BOOL_32 pset_change_bits,
											CHANGE_BITS_64_BIT *pchange_bitfield_to_set
											#ifdef _MSC_VER
											, char *pmerge_update_str,
											char *pmerge_insert_fields_str,
											char *pmerge_insert_values_str
											#endif
										)
{
	SHARED_set_bool_with_64_bit_change_bits_group( pgroup,
												   &(((STATION_GROUP_STRUCT*)pgroup)->in_use),
												   pin_use,
												   STATION_GROUP_IN_USE_DEFAULT,
												   pgenerate_change_line_bool,
												   CHANGE_STATION_GROUP_IN_USE,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
												   STATION_GROUP_CHANGE_BITFIELD_in_use_bit,
												   STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_in_use_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_set_moisture_sensor_serial_number(	void *const pgroup,
															UNS_32 pmoisture_sensor_serial_number,
															const BOOL_32 pgenerate_change_line_bool,
															const UNS_32 preason_for_change,
															const UNS_32 pbox_index_0,
															const BOOL_32 pset_change_bits,
															CHANGE_BITS_64_BIT *pchange_bitfield_to_set
															#ifdef _MSC_VER
															, char *pmerge_update_str,
															char *pmerge_insert_fields_str,
															char *pmerge_insert_values_str
															#endif
														 )
{
	if( SHARED_set_uint32_with_64_bit_change_bits_group( pgroup,
														 &(((STATION_GROUP_STRUCT*)pgroup)->moisture_sensor_serial_number),
														 pmoisture_sensor_serial_number,
														 DECODER_SERIAL_NUM_MIN,
														 DECODER_SERIAL_NUM_MAX,
														 DECODER_SERIAL_NUM_DEFAULT,
														 CHANGE_do_not_generate_change_line,
														 CHANGE_STATION_GROUP_MOISTURE_SENSOR_SERIAL_NUMBER,
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((STATION_GROUP_STRUCT*)pgroup)->changes_to_upload_to_comm_server),
														 STATION_GROUP_CHANGE_BITFIELD_moisture_sensor_ser_num_bit,
														 STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_moisture_sensor_ser_num_bit ]
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													) == (true) )

	{
		#ifndef	_MSC_VER

			MOISTURE_SENSOR_GROUP_STRUCT	*lmoisture_sensor;

			xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

			if( pgenerate_change_line_bool )
			{
				if( pmoisture_sensor_serial_number == 0 )
				{
					// 4/13/2016 rmd : In the case the moisture sensor decoder serial number is ZERO which means
					// not in use this is our alert line.
					Alert_station_group_assigned_to_a_moisture_sensor( nm_GROUP_get_name(pgroup), "<NONE ASSIGNED>", preason_for_change, pbox_index_0 );
				}
				else
				{
					lmoisture_sensor = MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number( pmoisture_sensor_serial_number );
					
					if( lmoisture_sensor )
					{
						Alert_station_group_assigned_to_a_moisture_sensor( nm_GROUP_get_name(pgroup), nm_GROUP_get_name(lmoisture_sensor), preason_for_change, pbox_index_0 );
					}
					else
					{
						// 4/13/2016 rmd : If we cannot find the sensor something has gone wrong. And we can
						// identify that.
						Alert_station_group_assigned_to_a_moisture_sensor( nm_GROUP_get_name(pgroup), "ERROR - SENSOR NOT FOUND", preason_for_change, pbox_index_0 );
					}
				}
			}
			
			xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

		#endif
	}
}

/* ---------------------------------------------------------- */
static void nm_STATION_GROUP_store_changes( STATION_GROUP_STRUCT *const ptemporary_group,
											const BOOL_32 pgroup_created,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0,
											const BOOL_32 pset_change_bits,
											const UNS_32 pchanges_received_from,
											CHANGE_BITS_64_BIT pbitfield_of_changes
											#ifdef _MSC_VER
											, char *pmerge_update_str,
											char *pmerge_insert_fields_str,
											char *pmerge_insert_values_str
											#endif
										  )
{
	STATION_GROUP_STRUCT	*lgroup;

	CHANGE_BITS_64_BIT		*lchange_bitfield_to_set;

	BOOL_32	lgenerate_begin_date_change_line;

	UNS_32	i;

	// ----------

	#ifndef _MSC_VER

	nm_GROUP_find_this_group_in_list( &station_group_group_list_hdr, ptemporary_group, (void**)&lgroup );

	if( lgroup != NULL )

	#else

	lgroup = ptemporary_group;

	#endif
	{
		#ifndef _MSC_VER

			if( pgroup_created == (true) )
			{
				Alert_ChangeLine_Group( CHANGE_GROUP_CREATED, nm_GROUP_get_name( ptemporary_group ), NULL, NULL, NUMBER_OF_CHARS_IN_A_NAME, preason_for_change, pbox_index_0 );
			}

		#endif
		
		#ifndef _MSC_VER

			lchange_bitfield_to_set = STATION_GROUP_get_change_bits_ptr( lgroup, pchanges_received_from );

		#else

			// 7/28/2015 ajv : Initialize to NULL since this variable is used by the CommServer but
			// expected to be NULL.
			lchange_bitfield_to_set = NULL;

		#endif

		// 4/24/2015 ajv : Only process the change if it was either initiated via the keypad
		// (locally) or if the change bit was set (when called via communications)
		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_description_bit ) == (true)) )
		{
			SHARED_set_name_64_bit_change_bits( lgroup, nm_GROUP_get_name( ptemporary_group ), !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set, &lgroup->changes_to_upload_to_comm_server, STATION_GROUP_CHANGE_BITFIELD_description_bit
												#ifdef _MSC_VER
												, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_description_bit ], pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												#endif
											  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_plant_type_bit ) == (true)) )
		{
			nm_STATION_GROUP_set_plant_type( lgroup, ptemporary_group->plant_type, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											 #ifdef _MSC_VER
											 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
											 #endif
										   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_head_type_bit ) == (true)) )
		{
			nm_STATION_GROUP_set_head_type( lgroup, ptemporary_group->head_type, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											#ifdef _MSC_VER
											, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
											#endif
										  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_precip_rate_bit ) == (true)) )
		{
			nm_STATION_GROUP_set_precip_rate( lgroup, ptemporary_group->precip_rate_in_100000u, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
											  #endif
											);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_soil_type_bit ) == (true)) )
		{
			nm_STATION_GROUP_set_soil_type( lgroup, ptemporary_group->soil_type, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											#ifdef _MSC_VER
											, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
											#endif
										  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_slope_percentage_bit ) == (true)) )
		{
			nm_STATION_GROUP_set_slope_percentage( lgroup, ptemporary_group->slope_percentage, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												   #ifdef _MSC_VER
												   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												   #endif
												 );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_exposure_bit ) == (true)) )
		{
			nm_STATION_GROUP_set_exposure( lgroup, ptemporary_group->exposure, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
										   #endif
										 );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_usable_rain_bit ) == (true)) )
		{
			nm_STATION_GROUP_set_usable_rain( lgroup, ptemporary_group->usable_rain_percentage_100u, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
											  #endif
											);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_soil_storage_capacity_bit ) == (true)) )
		{
			nm_STATION_GROUP_set_soil_storage_capacity( lgroup, ptemporary_group->soil_storage_capacity_inches_100u, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
														#ifdef _MSC_VER
														, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
														#endif
													  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_crop_coefficient_bit ) == (true)) )
		{
			for( i = 0; i < MONTHS_IN_A_YEAR; ++i )
			{
				nm_STATION_GROUP_set_crop_coefficient( lgroup, i, ptemporary_group->crop_coefficient_100u[ i ], !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
													   #ifdef _MSC_VER
													   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
													   #endif
													 );
			}
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_priority_level_bit ) == (true)) )
		{
			nm_PRIORITY_set_priority( lgroup, ptemporary_group->priority_level, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
									  #ifdef _MSC_VER
									  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
									  #endif
									);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_percent_adjust_bit ) == (true)) )
		{
			nm_PERCENT_ADJUST_set_percent( lgroup, ptemporary_group->percent_adjust_100u, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
										   #endif
										 );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_percent_adjust_start_date_bit ) == (true)) )
		{
			// 10/23/2013 ajv : Since we don't actually display the start date anywhere, don't generate
			// a change line when it changes.
			nm_PERCENT_ADJUST_set_start_date( lgroup, ptemporary_group->percent_adjust_start_date, CHANGE_do_not_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
											  #endif
											);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_percent_adjust_end_date_bit ) == (true)) )
		{
			// 1/26/2015 ajv : Only generate a change line if pgenerage_change_line is true AND percent
			// adjust is in effect.
			nm_PERCENT_ADJUST_set_end_date( lgroup, ptemporary_group->percent_adjust_end_date, !(pgroup_created) && (ptemporary_group->percent_adjust_100u != PERCENT_ADJUST_PERCENT_DEFAULT_100u), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											#ifdef _MSC_VER
											, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
											#endif
										  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_schedule_enabled_bit ) == (true)) )
		{
			// 8/27/2014 ajv : Since this is not user-settable, always suppress the change line.
			nm_SCHEDULE_set_enabled( lgroup, ptemporary_group->schedule_enabled_bool, CHANGE_do_not_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
									 #ifdef _MSC_VER
									 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
									 #endif
								   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_schedule_type_bit ) == (true)) )
		{
			nm_SCHEDULE_set_schedule_type( lgroup, ptemporary_group->schedule_type, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
										   #endif
										 );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_start_time_bit ) == (true)) )
		{
			nm_SCHEDULE_set_start_time( lgroup, ptemporary_group->start_time, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
										#ifdef _MSC_VER
										, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
										#endif
									  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_stop_time_bit ) == (true)) )
		{
			nm_SCHEDULE_set_stop_time( lgroup, ptemporary_group->stop_time, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
									   #ifdef _MSC_VER
									   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
									   #endif
									 );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_begin_date_bit ) == (true)) )
		{
			// 5/8/2015 ajv : We don't want to generate a change line for the begin date if it was
			// changed because the user crossed a start time. Therefore, check the reason before
			// generating the change line.
			if( !(pgroup_created) && (preason_for_change != CHANGE_REASON_HIT_A_STARTTIME) )
			{
				lgenerate_begin_date_change_line = (true);
			}
			else
			{
				lgenerate_begin_date_change_line = (false);
			}

			nm_SCHEDULE_set_begin_date( lgroup, ptemporary_group->begin_date, lgenerate_begin_date_change_line, preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
										#ifdef _MSC_VER
										, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
										#endif
									  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_water_days_bit ) == (true)) )
		{
			for( i = SUNDAY; i < DAYS_IN_A_WEEK; ++i )
			{
				nm_SCHEDULE_set_water_day( lgroup, i, ptemporary_group->water_days_bool[ i ], !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
										   #endif
										 );
			}
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_irrigate_on_29th_or_31st_bit ) == (true)) )
		{
			nm_SCHEDULE_set_irrigate_on_29th_or_31st( lgroup, ptemporary_group->irrigate_on_29th_or_31st_bool, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
													  #ifdef _MSC_VER
													  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
													  #endif
													);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_mow_day_bit ) == (true)) )
		{
			nm_SCHEDULE_set_mow_day( lgroup, ptemporary_group->mow_day, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
									 #ifdef _MSC_VER
									 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
									 #endif
								   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_last_ran_bit ) == (true)) )
		{
			nm_SCHEDULE_set_last_ran( lgroup, ptemporary_group->last_ran_dt, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
									 #ifdef _MSC_VER
									 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
									 #endif
								   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_et_in_use ) == (true)) )
		{
			nm_DAILY_ET_set_ET_in_use( lgroup, ptemporary_group->et_in_use, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
									 #ifdef _MSC_VER
									 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
									 #endif
								   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_use_et_averaging ) == (true)) )
		{
			nm_DAILY_ET_set_use_ET_averaging( lgroup, ptemporary_group->use_et_averaging_bool, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
											  #endif
											);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_rain_in_use_bit ) == (true)) )
		{
			nm_RAIN_set_rain_in_use( lgroup, ptemporary_group->rain_in_use_bool, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
									 #ifdef _MSC_VER
									 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
									 #endif
								   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_wind_in_use_bit ) == (true)) )
		{
			nm_WIND_set_wind_in_use( lgroup, ptemporary_group->wind_in_use_bool, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
									 #ifdef _MSC_VER
									 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
									 #endif
								   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_high_flow_action_bit ) == (true)) )
		{
			nm_ALERT_ACTIONS_set_high_flow_action( lgroup, ptemporary_group->high_flow_action, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												   #ifdef _MSC_VER
												   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												   #endif
												 );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_low_flow_action_bit ) == (true)) )
		{
			nm_ALERT_ACTIONS_set_low_flow_action( lgroup, ptemporary_group->low_flow_action, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												  #endif
												);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_on_at_a_time_in_group_bit ) == (true)) )
		{
			nm_ON_AT_A_TIME_set_on_at_a_time_in_group( lgroup, ptemporary_group->on_at_a_time_in_group, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
													   #ifdef _MSC_VER
													   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
													   #endif
													 );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_on_at_a_time_in_system_bit ) == (true)) )
		{
			nm_ON_AT_A_TIME_set_on_at_a_time_in_system( lgroup, ptemporary_group->on_at_a_time_in_system, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
														#ifdef _MSC_VER
														, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
														#endif
													  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_pump_in_use_bit ) == (true)) )
		{
			nm_PUMP_set_pump_in_use( lgroup, ptemporary_group->pump_in_use, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
									 #ifdef _MSC_VER
									 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
									 #endif
								   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_line_fill_time_bit ) == (true)) )
		{
			nm_LINE_FILL_TIME_set_line_fill_time( lgroup, ptemporary_group->line_fill_time_sec, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												  #endif
												);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_delay_between_valves_bit ) == (true)) )
		{
			nm_DELAY_BETWEEN_VALVES_set_delay_time( lgroup, ptemporary_group->delay_between_valve_time_sec, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
										  #ifdef _MSC_VER
										  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
										  #endif
										);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_acquire_at_start_bit ) == (true)) )
		{
			nm_ACQUIRE_EXPECTEDS_set_acquire_expected( lgroup, ptemporary_group->acquire_expecteds, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
													   #ifdef _MSC_VER
													   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
													   #endif
													 );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_allowable_depletion_bit ) == (true)) )
		{
			// 5/8/2015 ajv : Since this value is not user-editable, don't generate a change line.
			nm_STATION_GROUP_set_allowable_depletion( lgroup, ptemporary_group->allowable_depletion_100u, CHANGE_do_not_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
													  #ifdef _MSC_VER
													  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
													  #endif
													);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_available_water_bit ) == (true)) )
		{
			// 5/8/2015 ajv : Since this value is not user-editable, don't generate a change line.
			nm_STATION_GROUP_set_available_water( lgroup, ptemporary_group->available_water_100u, CHANGE_do_not_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												  #endif
												);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_root_zone_depth_bit ) == (true)) )
		{
			// 5/8/2015 ajv : Since this value is not user-editable, don't generate a change line.
			nm_STATION_GROUP_set_root_zone_depth( lgroup, ptemporary_group->root_zone_depth_10u, CHANGE_do_not_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												  #endif
												);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_species_factor_bit ) == (true)) )
		{
			// 5/8/2015 ajv : Since this value is not user-editable, don't generate a change line.
			nm_STATION_GROUP_set_species_factor( lgroup, ptemporary_group->species_factor_100u, CHANGE_do_not_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												 #ifdef _MSC_VER
												 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												 #endif
												);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_density_factor_bit ) == (true)) )
		{
			// 5/8/2015 ajv : Since this value is not user-editable, don't generate a change line.
			nm_STATION_GROUP_set_density_factor( lgroup, ptemporary_group->density_factor_100u, CHANGE_do_not_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												 #ifdef _MSC_VER
												 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												 #endif
												);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_microclimate_factor_bit ) == (true)) )
		{
			// 5/8/2015 ajv : Since this value is not user-editable, don't generate a change line.
			nm_STATION_GROUP_set_microclimate_factor( lgroup, ptemporary_group->microclimate_factor_100u, CHANGE_do_not_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
													  #ifdef _MSC_VER
													  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
													  #endif
													);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_soil_intake_rate_bit ) == (true)) )
		{
			// 5/8/2015 ajv : Since this value is not user-editable, don't generate a change line.
			nm_STATION_GROUP_set_soil_intake_rate( lgroup, ptemporary_group->soil_intake_rate_100u, CHANGE_do_not_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												   #ifdef _MSC_VER
												   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												   #endif
												 );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_allowable_surface_accum_bit ) == (true)) )
		{
			// 5/8/2015 ajv : Since this value is not user-editable, don't generate a change line.
			nm_STATION_GROUP_set_allowable_surface_accumulation( lgroup, ptemporary_group->allowable_surface_accumulation_100u, CHANGE_do_not_generate_change_line, preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
																 #ifdef _MSC_VER
																 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
																 #endif
															   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_budget_reduction_cap_bit ) == (true)) )
		{
			nm_STATION_GROUP_set_budget_reduction_percentage_cap(	lgroup, ptemporary_group->budget_reduction_percentage_cap, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
																	#ifdef _MSC_VER
																	, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
																	#endif
																);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_GID_irrigation_system_bit ) == (true)) )
		{
			nm_STATION_GROUP_set_GID_irrigation_system(	lgroup, ptemporary_group->GID_irrigation_system, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												#ifdef _MSC_VER
												, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												#endif
											);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_in_use_bit ) == (true)) )
		{
			#ifndef _MSC_VER

				// 2/11/2016 ajv : If the group was deleted, reorder the list by moving the deleted item to
				// the end of the list.
				if( (lgroup->in_use) && (!ptemporary_group->in_use) )
				{
					nm_ListRemove( &station_group_group_list_hdr, lgroup );

					nm_ListInsertTail( &station_group_group_list_hdr, lgroup );
				}

			#endif

			nm_STATION_GROUP_set_in_use(	lgroup, ptemporary_group->in_use, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											#ifdef _MSC_VER
											, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
											#endif
										);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET_64( pbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_moisture_sensor_ser_num_bit ) == (true)) )
		{
			nm_STATION_GROUP_set_moisture_sensor_serial_number(	lgroup, ptemporary_group->moisture_sensor_serial_number, !(pgroup_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												#ifdef _MSC_VER
												, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												#endif
											);
		}
	}
	#ifndef _MSC_VER
	else
	{
		Alert_group_not_found();
	}
	#endif
}

/* ---------------------------------------------------------- */
extern UNS_32 nm_STATION_GROUP_extract_and_store_changes_from_comm( const unsigned char *pucp,
																	const UNS_32 preason_for_change,
																	const BOOL_32 pset_change_bits,
																	const UNS_32 pchanges_received_from
																	#ifdef _MSC_VER
																	, char *pSQL_statements,
																	char *pSQL_search_condition_str,
																	char *pSQL_update_str,
																	char *pSQL_insert_fields_str,
																	char *pSQL_insert_values_str,
																	char *pSQL_single_merge_statement_buf,
																	const UNS_32 pnetwork_ID
																	#endif
																  )
{
	CHANGE_BITS_64_BIT	lbitfield_of_changes;

	STATION_GROUP_STRUCT 	*ltemporary_group;

	#ifndef _MSC_VER

	STATION_GROUP_STRUCT 	*lmatching_group;

	#endif

	UNS_32	lnum_changed_groups;

	UNS_32	lsize_of_bitfield;

	BOOL_32	lgroup_created;

	UNS_32	lgroup_id;

	UNS_32	i;

	UNS_32	rv;

	// ----------

	lgroup_created = (false);

	rv = 0;

	// ----------

	// Extract the number of changed groups
	memcpy( &lnum_changed_groups, pucp, sizeof(lnum_changed_groups) );
	pucp += sizeof( lnum_changed_groups );
	rv += sizeof( lnum_changed_groups );

	// 8/8/2016 rmd : For debug only.
	/*
	if( pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE )
	{
		Alert_Message_va( "rcvd from slave: STATION GROUP CHANGES for %u groups", lnum_changed_groups );
	}
	else
	if( pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER )
	{
		Alert_Message_va( "rcvd from master: STATION GROUP CHANGES for %u groups", lnum_changed_groups );
	}
	else
	{
		Alert_Message_va( "rcvd from UNK: STATION GROUP CHANGES for %u groups", lnum_changed_groups );
	}
	*/
	
	// ----------
	
	for( i = 0; i < lnum_changed_groups; ++i )
	{
		memcpy( &lgroup_id, pucp, sizeof(lgroup_id) );
		pucp += sizeof( lgroup_id );
		rv += sizeof( lgroup_id );

		memcpy( &lsize_of_bitfield, pucp, sizeof(lsize_of_bitfield) );
		pucp += sizeof( lsize_of_bitfield );
		rv += sizeof( lsize_of_bitfield );

		memcpy( &lbitfield_of_changes, pucp, sizeof(lbitfield_of_changes) );
		pucp += sizeof( lbitfield_of_changes );
		rv += sizeof( lbitfield_of_changes );

		#ifndef _MSC_VER

			// Find the existing group with the group ID
			lmatching_group = nm_GROUP_get_ptr_to_group_with_this_GID( &station_group_group_list_hdr, lgroup_id, (true) );

			// 3/6/2013 ajv : If the specified group doesn't exist yet, we need to
			// create it and assign it the group ID.
			if( lmatching_group == NULL )
			{
				lmatching_group = nm_STATION_GROUP_create_new_group( pchanges_received_from );

				lgroup_created = (true);

				// Force the new Group ID to the passed-in group ID.
				lmatching_group->base.group_identity_number = lgroup_id;
				// ----------

				if( pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER )
				{
					// 5/2/2014 ajv : Any group creation caused by receipt of a
					// token is  for stations not at this box. And therefore
					// we don't have to resend out these changes. The distribution
					// of this activity creating groups and assigning the values
					// STOPS here in this case.
					SHARED_clear_all_64_bit_change_bits( &lmatching_group->changes_to_send_to_master, list_program_data_recursive_MUTEX );

					//Alert_Message_va( "PDATA: Station Group %u created cleared", lgroup_id );
				}
				else
				{
					//Alert_Message_va( "PDATA: Station Group %u created", lgroup_id );
				}
			}
			else
			{
				//Alert_Message_va( "PDATA: Station Group %u found 0x%016llx", lgroup_id, lbitfield_of_changes );
			}

		#endif

		// ----------

		#ifndef _MSC_VER

		if( lmatching_group != NULL )

		#endif
		{
			// Allocate memory for, and initialize, the temporary group
			#ifndef _MSC_VER
				ltemporary_group = mem_malloc( sizeof(STATION_GROUP_STRUCT) );
			#else
				ltemporary_group = malloc( sizeof(STATION_GROUP_STRUCT) );
			#endif

			memset( ltemporary_group, 0x00, sizeof(STATION_GROUP_STRUCT) );

			#ifndef _MSC_VER

				// Make a copy of the group. We do this so we can change the settings
				// and pass it to the store_changes routine which will actually update
				// the group's settings.
				memcpy( ltemporary_group, lmatching_group, sizeof(STATION_GROUP_STRUCT) );

			#else

				ltemporary_group->base.group_identity_number = lgroup_id;

			#endif

			// ------------------

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_description_bit ) == (true) )
			{
				memcpy( &ltemporary_group->base.description, pucp, NUMBER_OF_CHARS_IN_A_NAME );
				pucp += NUMBER_OF_CHARS_IN_A_NAME;
				rv += NUMBER_OF_CHARS_IN_A_NAME;
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_plant_type_bit ) == (true) )
			{
				memcpy( &ltemporary_group->plant_type, pucp, sizeof(ltemporary_group->plant_type) );
				pucp += sizeof(ltemporary_group->plant_type);
				rv += sizeof(ltemporary_group->plant_type);
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_head_type_bit ) == (true) )
			{
				memcpy( &ltemporary_group->head_type, pucp, sizeof(ltemporary_group->head_type) );
				pucp += sizeof(ltemporary_group->head_type);
				rv += sizeof(ltemporary_group->head_type);
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_precip_rate_bit ) == (true) )
			{
				memcpy( &ltemporary_group->precip_rate_in_100000u, pucp, sizeof(ltemporary_group->precip_rate_in_100000u) );
				pucp += sizeof(ltemporary_group->precip_rate_in_100000u);
				rv += sizeof(ltemporary_group->precip_rate_in_100000u);
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_soil_type_bit ) == (true) )
			{
				memcpy( &ltemporary_group->soil_type, pucp, sizeof(ltemporary_group->soil_type) );
				pucp += sizeof(ltemporary_group->soil_type);
				rv += sizeof(ltemporary_group->soil_type);
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_slope_percentage_bit ) == (true) )
			{
				memcpy( &ltemporary_group->slope_percentage, pucp, sizeof(ltemporary_group->slope_percentage) );
				pucp += sizeof(ltemporary_group->slope_percentage);
				rv += sizeof(ltemporary_group->slope_percentage);
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_exposure_bit ) == (true) )
			{
				memcpy( &ltemporary_group->exposure, pucp, sizeof(ltemporary_group->exposure) );
				pucp += sizeof(ltemporary_group->exposure);
				rv += sizeof(ltemporary_group->exposure);
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_usable_rain_bit ) == (true) )
			{
				memcpy( &ltemporary_group->usable_rain_percentage_100u, pucp, sizeof(ltemporary_group->usable_rain_percentage_100u) );
				pucp += sizeof(ltemporary_group->usable_rain_percentage_100u);
				rv += sizeof(ltemporary_group->usable_rain_percentage_100u);
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_crop_coefficient_bit ) == (true) )
			{
				memcpy( &ltemporary_group->crop_coefficient_100u, pucp, sizeof(ltemporary_group->crop_coefficient_100u) );
				pucp += sizeof(ltemporary_group->crop_coefficient_100u);
				rv += sizeof(ltemporary_group->crop_coefficient_100u);
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_priority_level_bit ) == (true) )
			{
				memcpy( &ltemporary_group->priority_level, pucp, sizeof(ltemporary_group->priority_level) );
				pucp += sizeof( ltemporary_group->priority_level );
				rv += sizeof( ltemporary_group->priority_level );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_percent_adjust_bit ) == (true) )
			{
				memcpy( &ltemporary_group->percent_adjust_100u, pucp, sizeof(ltemporary_group->percent_adjust_100u) );
				pucp += sizeof( ltemporary_group->percent_adjust_100u );
				rv += sizeof( ltemporary_group->percent_adjust_100u );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_percent_adjust_start_date_bit ) == (true) )
			{
				memcpy( &ltemporary_group->percent_adjust_start_date, pucp, sizeof(ltemporary_group->percent_adjust_start_date) );
				pucp += sizeof( ltemporary_group->percent_adjust_start_date );
				rv += sizeof( ltemporary_group->percent_adjust_start_date );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_percent_adjust_end_date_bit ) == (true) )
			{
				memcpy( &ltemporary_group->percent_adjust_end_date, pucp, sizeof(ltemporary_group->percent_adjust_end_date) );
				pucp += sizeof( ltemporary_group->percent_adjust_end_date );
				rv += sizeof( ltemporary_group->percent_adjust_end_date );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_schedule_enabled_bit ) == (true) )
			{
				memcpy( &ltemporary_group->schedule_enabled_bool, pucp, sizeof(ltemporary_group->schedule_enabled_bool) );
				pucp += sizeof( ltemporary_group->schedule_enabled_bool );
				rv += sizeof( ltemporary_group->schedule_enabled_bool );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_schedule_type_bit ) == (true) )
			{
				memcpy( &ltemporary_group->schedule_type, pucp, sizeof(ltemporary_group->schedule_type) );
				pucp += sizeof( ltemporary_group->schedule_type );
				rv += sizeof( ltemporary_group->schedule_type );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_start_time_bit ) == (true) )
			{
				memcpy( &ltemporary_group->start_time, pucp, sizeof(ltemporary_group->start_time) );
				pucp += sizeof( ltemporary_group->start_time );
				rv += sizeof( ltemporary_group->start_time );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_stop_time_bit ) == (true) )
			{
				memcpy( &ltemporary_group->stop_time, pucp, sizeof(ltemporary_group->stop_time) );
				pucp += sizeof( ltemporary_group->stop_time );
				rv += sizeof( ltemporary_group->stop_time );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_begin_date_bit ) == (true) )
			{
				memcpy( &ltemporary_group->begin_date, pucp, sizeof(ltemporary_group->begin_date) );
				pucp += sizeof( ltemporary_group->begin_date );
				rv += sizeof( ltemporary_group->begin_date );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_water_days_bit ) == (true) )
			{
				memcpy( &ltemporary_group->water_days_bool, pucp, sizeof(ltemporary_group->water_days_bool) );
				pucp += sizeof( ltemporary_group->water_days_bool );
				rv += sizeof( ltemporary_group->water_days_bool );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_irrigate_on_29th_or_31st_bit ) == (true) )
			{
				memcpy( &ltemporary_group->irrigate_on_29th_or_31st_bool, pucp, sizeof(ltemporary_group->irrigate_on_29th_or_31st_bool) );
				pucp += sizeof( ltemporary_group->irrigate_on_29th_or_31st_bool );
				rv += sizeof( ltemporary_group->irrigate_on_29th_or_31st_bool );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_mow_day_bit ) == (true) )
			{
				memcpy( &ltemporary_group->mow_day, pucp, sizeof(ltemporary_group->mow_day) );
				pucp += sizeof( ltemporary_group->mow_day );
				rv += sizeof( ltemporary_group->mow_day );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_last_ran_bit ) == (true) )
			{
				memcpy( &ltemporary_group->last_ran_dt, pucp, sizeof(ltemporary_group->last_ran_dt) );
				pucp += sizeof( ltemporary_group->last_ran_dt );
				rv += sizeof( ltemporary_group->last_ran_dt );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_et_in_use ) == (true) )
			{
				memcpy( &ltemporary_group->et_in_use, pucp, sizeof(ltemporary_group->et_in_use) );
				pucp += sizeof( ltemporary_group->et_in_use );
				rv += sizeof( ltemporary_group->et_in_use );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_use_et_averaging ) == (true) )
			{
				memcpy( &ltemporary_group->use_et_averaging_bool, pucp, sizeof(ltemporary_group->use_et_averaging_bool) );
				pucp += sizeof( ltemporary_group->use_et_averaging_bool );
				rv += sizeof( ltemporary_group->use_et_averaging_bool );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_rain_in_use_bit ) == (true) )
			{
				memcpy( &ltemporary_group->rain_in_use_bool, pucp, sizeof(ltemporary_group->rain_in_use_bool) );
				pucp += sizeof( ltemporary_group->rain_in_use_bool );
				rv += sizeof( ltemporary_group->rain_in_use_bool );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_soil_storage_capacity_bit ) == (true) )
			{
				memcpy( &ltemporary_group->soil_storage_capacity_inches_100u, pucp, sizeof(ltemporary_group->soil_storage_capacity_inches_100u) );
				pucp += sizeof( ltemporary_group->soil_storage_capacity_inches_100u );
				rv += sizeof( ltemporary_group->soil_storage_capacity_inches_100u );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_wind_in_use_bit ) == (true) )
			{
				memcpy( &ltemporary_group->wind_in_use_bool, pucp, sizeof(ltemporary_group->wind_in_use_bool) );
				pucp += sizeof( ltemporary_group->wind_in_use_bool );
				rv += sizeof( ltemporary_group->wind_in_use_bool );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_high_flow_action_bit ) == (true) )
			{
				memcpy( &ltemporary_group->high_flow_action, pucp, sizeof(ltemporary_group->high_flow_action) );
				pucp += sizeof( ltemporary_group->high_flow_action );
				rv += sizeof( ltemporary_group->high_flow_action );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_low_flow_action_bit ) == (true) )
			{
				memcpy( &ltemporary_group->low_flow_action, pucp, sizeof(ltemporary_group->low_flow_action) );
				pucp += sizeof( ltemporary_group->low_flow_action );
				rv += sizeof( ltemporary_group->low_flow_action );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_on_at_a_time_in_group_bit ) == (true) )
			{
				memcpy( &ltemporary_group->on_at_a_time_in_group, pucp, sizeof(ltemporary_group->on_at_a_time_in_group) );
				pucp += sizeof( ltemporary_group->on_at_a_time_in_group );
				rv += sizeof( ltemporary_group->on_at_a_time_in_group );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_on_at_a_time_in_system_bit ) == (true) )
			{
				memcpy( &ltemporary_group->on_at_a_time_in_system, pucp, sizeof(ltemporary_group->on_at_a_time_in_system) );
				pucp += sizeof( ltemporary_group->on_at_a_time_in_system );
				rv += sizeof( ltemporary_group->on_at_a_time_in_system );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_pump_in_use_bit ) == (true) )
			{
				memcpy( &ltemporary_group->pump_in_use, pucp, sizeof(ltemporary_group->pump_in_use) );
				pucp += sizeof( ltemporary_group->pump_in_use );
				rv += sizeof( ltemporary_group->pump_in_use );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_line_fill_time_bit ) == (true) )
			{
				memcpy( &ltemporary_group->line_fill_time_sec, pucp, sizeof(ltemporary_group->line_fill_time_sec) );
				pucp += sizeof( ltemporary_group->line_fill_time_sec );
				rv += sizeof( ltemporary_group->line_fill_time_sec );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_delay_between_valves_bit ) == (true) )
			{
				memcpy( &ltemporary_group->delay_between_valve_time_sec, pucp, sizeof(ltemporary_group->delay_between_valve_time_sec) );
				pucp += sizeof( ltemporary_group->delay_between_valve_time_sec );
				rv += sizeof( ltemporary_group->delay_between_valve_time_sec );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_acquire_at_start_bit ) == (true) )
			{
				memcpy( &ltemporary_group->acquire_expecteds, pucp, sizeof(ltemporary_group->acquire_expecteds) );
				pucp += sizeof( ltemporary_group->acquire_expecteds );
				rv += sizeof( ltemporary_group->acquire_expecteds );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_allowable_depletion_bit ) == (true) )
			{
				memcpy( &ltemporary_group->allowable_depletion_100u, pucp, sizeof(ltemporary_group->allowable_depletion_100u) );
				pucp += sizeof( ltemporary_group->allowable_depletion_100u );
				rv += sizeof( ltemporary_group->allowable_depletion_100u );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_available_water_bit ) == (true) )
			{
				memcpy( &ltemporary_group->available_water_100u, pucp, sizeof(ltemporary_group->available_water_100u) );
				pucp += sizeof( ltemporary_group->available_water_100u );
				rv += sizeof( ltemporary_group->available_water_100u );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_root_zone_depth_bit ) == (true) )
			{
				memcpy( &ltemporary_group->root_zone_depth_10u, pucp, sizeof(ltemporary_group->root_zone_depth_10u) );
				pucp += sizeof( ltemporary_group->root_zone_depth_10u );
				rv += sizeof( ltemporary_group->root_zone_depth_10u );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_species_factor_bit ) == (true) )
			{
				memcpy( &ltemporary_group->species_factor_100u, pucp, sizeof(ltemporary_group->species_factor_100u) );
				pucp += sizeof( ltemporary_group->species_factor_100u );
				rv += sizeof( ltemporary_group->species_factor_100u );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_density_factor_bit ) == (true) )
			{
				memcpy( &ltemporary_group->density_factor_100u, pucp, sizeof(ltemporary_group->density_factor_100u) );
				pucp += sizeof( ltemporary_group->density_factor_100u );
				rv += sizeof( ltemporary_group->density_factor_100u );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_microclimate_factor_bit ) == (true) )
			{
				memcpy( &ltemporary_group->microclimate_factor_100u, pucp, sizeof(ltemporary_group->microclimate_factor_100u) );
				pucp += sizeof( ltemporary_group->microclimate_factor_100u );
				rv += sizeof( ltemporary_group->microclimate_factor_100u );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_soil_intake_rate_bit ) == (true) )
			{
				memcpy( &ltemporary_group->soil_intake_rate_100u, pucp, sizeof(ltemporary_group->soil_intake_rate_100u) );
				pucp += sizeof( ltemporary_group->soil_intake_rate_100u );
				rv += sizeof( ltemporary_group->soil_intake_rate_100u );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_allowable_surface_accum_bit ) == (true) )
			{
				memcpy( &ltemporary_group->allowable_surface_accumulation_100u, pucp, sizeof(ltemporary_group->allowable_surface_accumulation_100u) );
				pucp += sizeof( ltemporary_group->allowable_surface_accumulation_100u );
				rv += sizeof( ltemporary_group->allowable_surface_accumulation_100u );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_budget_reduction_cap_bit ) == (true) )
			{
				memcpy( &ltemporary_group->budget_reduction_percentage_cap, pucp, sizeof(ltemporary_group->budget_reduction_percentage_cap) );
				pucp += sizeof( ltemporary_group->budget_reduction_percentage_cap );
				rv += sizeof( ltemporary_group->budget_reduction_percentage_cap );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_GID_irrigation_system_bit ) == (true) )
			{
				memcpy( &ltemporary_group->GID_irrigation_system, pucp, sizeof(ltemporary_group->GID_irrigation_system) );
				pucp += sizeof( ltemporary_group->GID_irrigation_system );
				rv += sizeof( ltemporary_group->GID_irrigation_system );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_in_use_bit ) == (true) )
			{
				memcpy( &ltemporary_group->in_use, pucp, sizeof(ltemporary_group->in_use) );
				pucp += sizeof( ltemporary_group->in_use );
				rv += sizeof( ltemporary_group->in_use );
			}

			if( B_IS_SET_64( lbitfield_of_changes, STATION_GROUP_CHANGE_BITFIELD_moisture_sensor_ser_num_bit ) == (true) )
			{
				memcpy( &ltemporary_group->moisture_sensor_serial_number, pucp, sizeof(ltemporary_group->moisture_sensor_serial_number) );
				pucp += sizeof( ltemporary_group->moisture_sensor_serial_number );
				rv += sizeof( ltemporary_group->moisture_sensor_serial_number );
			}

			// ----------
			
			#ifdef _MSC_VER

				// 1/16/2015 ajv : Make sure the strings are fully initialized to NULL since we rely on this
				// to determine whether the string is empty or not.
				memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

				// 4/11/2016 ajv : Insert the network ID and GID in case the group doesn't exist in the
				// database yet.
				SQL_MERGE_build_upsert_uint32( pnetwork_ID, SQL_MERGE_include_field_name_in_insert_clause, "NetworkID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
				SQL_MERGE_build_upsert_uint32( ltemporary_group->base.group_identity_number, SQL_MERGE_include_field_name_in_insert_clause, "StationGroupGID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

			#endif
	
			
			// ----------

			nm_STATION_GROUP_store_changes( ltemporary_group, lgroup_created, preason_for_change, 0, pset_change_bits, pchanges_received_from, lbitfield_of_changes
											#ifdef _MSC_VER
											, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											#endif
										  );

			// ----------

			#ifndef _MSC_VER

				mem_free( ltemporary_group );

			#else

				SQL_MERGE_build_search_condition_uint32( "NetworkID", pnetwork_ID, pSQL_search_condition_str );
				SQL_MERGE_build_search_condition_uint32( "StationGroupGID", ltemporary_group->base.group_identity_number, pSQL_search_condition_str );

				SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements( DATABASE_TABLE_NAME_STATION_GROUPS, pSQL_search_condition_str, SQL_MERGE_include_field_name_in_insert_clause, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf );

				// ----------

				free( ltemporary_group );

			#endif
		}
		#ifndef _MSC_VER
		else
		{
			Alert_group_not_found();
		}
		#endif
	}

	#ifndef _MSC_VER

		// Only save the file if we've extracted information out of the ucp. This is
		// a sanity check to ensure the bit is only set if data was included.
		if( rv > 0 )
		{
			// 5/13/2015 ajv : We want to save the file in the following cases:
			// 
			//   - A message from the CommServer since we've responded that we received it and need to
			//     ensure it's saved.
			//   - A token response from the slave to the master
			//   - A token from the master to the slave
			//   
			// We DO NOT want to save the file when a token is received by the master since we've
			// already saved the file for one of the reasons above. This will otherwise result in a
			// double save for each change - once when it comes in and once when it's received when the
			// master distributes the changes back out.
			if( (pchanges_received_from == CHANGE_REASON_CENTRAL_OR_MOBILE) ||
				(pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE) ||
				((pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER) && (!FLOWSENSE_we_are_a_master_one_way_or_another())) )
			{
				// 12/10/2014 ajv : Schedule a save rather than saving immediately to prevent stacking of
				// saves. Additionally, set a relatively short time to hel cover cases where users make a
				// lot of changes in a short period.
				FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_LANDSCAPE_DETAILS, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
			}
		}
		else
		{
			Alert_bit_set_with_no_data();
		}

	#endif

	return ( rv );
}

#ifdef _MSC_VER

/* ---------------------------------------------------------- */
extern INT_32 STATION_GROUP_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order )
{
	STATION_GROUP_STRUCT	*lgroup;

	INT_32	rv;

	// ----------

	// 4/15/2016 ajv : In most cases, we're going to pass back an array_index of 0. However, in
	// the event the value in question is part of an array, we're going to pass back the actual
	// index so that the CommServer can sort not just on the bit number, but also the
	// array_index for values that share the same bit number.
	*psecondary_sort_order = 0;

	rv = -1;

	// ----------

	if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_description_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_description_bit;
		*psize_of_var_ptr = sizeof(lgroup->base.description);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_plant_type_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_plant_type_bit;
		*psize_of_var_ptr = sizeof(lgroup->plant_type);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_head_type_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_head_type_bit;
		*psize_of_var_ptr = sizeof(lgroup->head_type);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_precip_rate_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_precip_rate_bit;
		*psize_of_var_ptr = sizeof(lgroup->precip_rate_in_100000u);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_soil_type_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_soil_type_bit;
		*psize_of_var_ptr = sizeof(lgroup->soil_type);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_slope_percentage_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_slope_percentage_bit;
		*psize_of_var_ptr = sizeof(lgroup->slope_percentage);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_exposure_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_exposure_bit;
		*psize_of_var_ptr = sizeof(lgroup->exposure);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_usable_rain_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_usable_rain_bit;
		*psize_of_var_ptr = sizeof(lgroup->usable_rain_percentage_100u);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_crop_coefficient_bit ], 15 ) == 0 )
	{
		// 9/21/2015 ajv : Note the above condition is not checking the entire length of the field
		// name. This is because there are 12 unique crop coefficient values: CropCoefficient1 - 
		// CropCoefficient12. By only comparing the first 15 characters, we can ensure we catch all
		// 12 in one condition.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "12" ) != NULL )
		{
			*psecondary_sort_order = 11;
		}
		else if( strstr( pfield_name, "11" ) != NULL )
		{
			*psecondary_sort_order = 10;
		}
		else if( strstr( pfield_name, "10" ) != NULL )
		{
			*psecondary_sort_order = 9;
		}
		else if( strstr( pfield_name, "9" ) != NULL )
		{
			*psecondary_sort_order = 8;
		}
		else if( strstr( pfield_name, "8" ) != NULL )
		{
			*psecondary_sort_order = 7;
		}
		else if( strstr( pfield_name, "7" ) != NULL )
		{
			*psecondary_sort_order = 6;
		}
		else if( strstr( pfield_name, "6" ) != NULL )
		{
			*psecondary_sort_order = 5;
		}
		else if( strstr( pfield_name, "5" ) != NULL )
		{
			*psecondary_sort_order = 4;
		}
		else if( strstr( pfield_name, "4" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}
		else if( strstr( pfield_name, "3" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}

		rv = STATION_GROUP_CHANGE_BITFIELD_crop_coefficient_bit;
		*psize_of_var_ptr = sizeof(lgroup->crop_coefficient_100u[ 0 ]);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_priority_level_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_priority_level_bit;
		*psize_of_var_ptr = sizeof(lgroup->priority_level);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_percent_adjust_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_percent_adjust_bit;
		*psize_of_var_ptr = sizeof(lgroup->percent_adjust_100u);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_percent_adjust_start_date_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_percent_adjust_start_date_bit;
		*psize_of_var_ptr = sizeof(lgroup->percent_adjust_start_date);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_percent_adjust_end_date_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_percent_adjust_end_date_bit;
		*psize_of_var_ptr = sizeof(lgroup->percent_adjust_end_date);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_schedule_enabled_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_schedule_enabled_bit;
		*psize_of_var_ptr = sizeof(lgroup->schedule_enabled_bool);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_schedule_type_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_schedule_type_bit;
		*psize_of_var_ptr = sizeof(lgroup->schedule_type);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_start_time_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_start_time_bit;
		*psize_of_var_ptr = sizeof(lgroup->start_time);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_stop_time_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_stop_time_bit;
		*psize_of_var_ptr = sizeof(lgroup->stop_time);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_begin_date_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_begin_date_bit;
		*psize_of_var_ptr = sizeof(lgroup->begin_date);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_water_days_bit ], 9 ) == 0 )
	{
		// 9/21/2015 ajv : Note the above condition is not checking the entire length of the field
		// name. This is because there are 7 unique water day values: WaterDaysSun - WaterDaysSat.
		// By only comparing the first 9 characters, we can ensure we catch all 7 in one condition.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "Sun" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "Mon" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "Tue" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "Wed" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}
		else if( strstr( pfield_name, "Thu" ) != NULL )
		{
			*psecondary_sort_order = 4;
		}
		else if( strstr( pfield_name, "Fri" ) != NULL )
		{
			*psecondary_sort_order = 5;
		}
		else if( strstr( pfield_name, "Sat" ) != NULL )
		{
			*psecondary_sort_order = 6;
		}

		rv = STATION_GROUP_CHANGE_BITFIELD_water_days_bit;
		*psize_of_var_ptr = sizeof(lgroup->water_days_bool[ 0 ]);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_irrigate_on_29th_or_31st_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_irrigate_on_29th_or_31st_bit;
		*psize_of_var_ptr = sizeof(lgroup->irrigate_on_29th_or_31st_bool);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_mow_day_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_mow_day_bit;
		*psize_of_var_ptr = sizeof(lgroup->mow_day);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_last_ran_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_last_ran_bit;
		*psize_of_var_ptr = sizeof(lgroup->last_ran_dt);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_et_in_use ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_et_in_use;
		*psize_of_var_ptr = sizeof(lgroup->et_in_use);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_use_et_averaging ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_use_et_averaging;
		*psize_of_var_ptr = sizeof(lgroup->use_et_averaging_bool);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_rain_in_use_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_rain_in_use_bit;
		*psize_of_var_ptr = sizeof(lgroup->rain_in_use_bool);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_soil_storage_capacity_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_soil_storage_capacity_bit;
		*psize_of_var_ptr = sizeof(lgroup->soil_storage_capacity_inches_100u);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_wind_in_use_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_wind_in_use_bit;
		*psize_of_var_ptr = sizeof(lgroup->wind_in_use_bool);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_high_flow_action_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_high_flow_action_bit;
		*psize_of_var_ptr = sizeof(lgroup->high_flow_action);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_low_flow_action_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_low_flow_action_bit;
		*psize_of_var_ptr = sizeof(lgroup->low_flow_action);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_on_at_a_time_in_group_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_on_at_a_time_in_group_bit;
		*psize_of_var_ptr = sizeof(lgroup->on_at_a_time_in_group);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_on_at_a_time_in_system_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_on_at_a_time_in_system_bit;
		*psize_of_var_ptr = sizeof(lgroup->on_at_a_time_in_system);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_pump_in_use_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_pump_in_use_bit;
		*psize_of_var_ptr = sizeof(lgroup->pump_in_use);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_line_fill_time_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_line_fill_time_bit;
		*psize_of_var_ptr = sizeof(lgroup->line_fill_time_sec);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_delay_between_valves_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_delay_between_valves_bit;
		*psize_of_var_ptr = sizeof(lgroup->delay_between_valve_time_sec);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_acquire_at_start_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_acquire_at_start_bit;
		*psize_of_var_ptr = sizeof(lgroup->acquire_expecteds);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_allowable_depletion_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_allowable_depletion_bit;
		*psize_of_var_ptr = sizeof(lgroup->allowable_depletion_100u);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_available_water_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_available_water_bit;
		*psize_of_var_ptr = sizeof(lgroup->available_water_100u);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_root_zone_depth_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_root_zone_depth_bit;
		*psize_of_var_ptr = sizeof(lgroup->root_zone_depth_10u);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_species_factor_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_species_factor_bit;
		*psize_of_var_ptr = sizeof(lgroup->species_factor_100u);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_density_factor_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_density_factor_bit;
		*psize_of_var_ptr = sizeof(lgroup->density_factor_100u);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_microclimate_factor_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_microclimate_factor_bit;
		*psize_of_var_ptr = sizeof(lgroup->microclimate_factor_100u);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_soil_intake_rate_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_soil_intake_rate_bit;
		*psize_of_var_ptr = sizeof(lgroup->soil_intake_rate_100u);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_allowable_surface_accum_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_allowable_surface_accum_bit;
		*psize_of_var_ptr = sizeof(lgroup->allowable_surface_accumulation_100u);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_budget_reduction_cap_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_budget_reduction_cap_bit;
		*psize_of_var_ptr = sizeof(lgroup->budget_reduction_percentage_cap);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_GID_irrigation_system_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_GID_irrigation_system_bit;
		*psize_of_var_ptr = sizeof(lgroup->GID_irrigation_system);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_in_use_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_in_use_bit;
		*psize_of_var_ptr = sizeof(lgroup->in_use);
	}
	else if( strncmp( pfield_name, STATION_GROUP_database_field_names[ STATION_GROUP_CHANGE_BITFIELD_moisture_sensor_ser_num_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = STATION_GROUP_CHANGE_BITFIELD_moisture_sensor_ser_num_bit;
		*psize_of_var_ptr = sizeof(lgroup->moisture_sensor_serial_number);
	}

	return( rv );
}

#endif


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

