/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_SHARED_POC_C
#define _INC_SHARED_POC_C

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"group_base_file.h"

#include	"cal_td_utils.h"

#include	"change.h"

#ifndef	_MSC_VER

	#include	"alerts.h"

	#include	"app_startup.h"
	
	#include	"cs_mem.h"

	#include	"battery_backed_vars.h"

#else

	#include	<stdlib.h>

	#include	"sql_merge.h"

#endif




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	POC_CHANGE_BITFIELD_description_bit						(0)
#define	POC_CHANGE_BITFIELD_GID_irri_system_bit					(1)
#define	POC_CHANGE_BITFIELD_box_index_bit						(2)
#define	POC_CHANGE_BITFIELD_type_of_poc_bit						(3)

#define	POC_CHANGE_BITFIELD_decoder_serial_number_bit			(4)

// 6/6/2016 rmd : This is for the 'show' flag.
#define	POC_CHANGE_BITFIELD_poc_physically_available_bit		(5)

#define	POC_CHANGE_BITFIELD_poc_usage_bit						(6)
#define	POC_CHANGE_BITFIELD_master_valve_type_bit				(7)

#define	POC_CHANGE_BITFIELD_flow_meter_type_bit					(8)
#define	POC_CHANGE_BITFIELD_bypass_number_of_levels				(9)

#define	POC_CHANGE_BITFIELD_budget_gallons_entry_option_bit		(10)
#define	POC_CHANGE_BITFIELD_budget_calc_percent_of_et_bit		(11)
// 03/01/2016 skc : Comment out unused bits
//#define	POC_CHANGE_BITFIELD_budget_auto_adjust_budget_using_live_et_bit		(12)
//#define	POC_CHANGE_BITFIELD_budget_close_poc_at_budget_bit					(13)
#define	POC_CHANGE_BITFIELD_budget_gallons_bit					(14)

#define	POC_CHANGE_BITFIELD_has_pump_attached_bit				(15)

// ----------

static char* const POC_database_field_names[ (POC_CHANGE_BITFIELD_has_pump_attached_bit + 1) ] =
{
	"Name",
	"SystemGID",
	"BoxIndex",
	"TypeOfPOC",
	"DecoderSerialNumber",	// DecoderSerialNumber1 .. 3
	"POC_CardConnected",
	"POC_Usage",
	"MasterValveType",		// MasterValveType1 .. 3
	"FlowMeter",			// Flow meter type includes FlowMeterChoice1 .. 3, FlowMeterK_Value1 .. 3, and FlowMeterOffset_Value1 .. 3
	"NumberOfLevels",
	
	"BudgetGallonsEntryOption",
	"BudgetCalcPercentOfET",
	// 03/01/2016 skc : Null out unused fields
	"",						// BudgetAutoAdjustUsingLiveET
	"",						// BudgetClosePOCAtBudget
	"BudgetGallons",			// 1 .. 24  -  one for each period
	"HasPumpAttached"
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * The definition of the group's structure. If the contents of this structure
 * are changed or reordered, you <b>MUST</b> update POC_LATEST_FILE_REVISION.
 * 
 * @revisions
 *    2/10/2012  Updated variables to be 32-bit unsigned and BOOL_32
 */
struct POC_GROUP_STRUCT
{
	// 5/27/2016 rmd : WARNING - RULES FOR IDENTIFYING POCS IN THE FILE LIST. DO NOT USE THE
	// GID. The GID is not sync'd properly amongst master and slaves on a chain. In other words
	// the B TERMINAL POC at the B controller will have a different GID than the B TERMINAL POC
	// at the A controller. Use the box_index, the decoder serial_number, and the poc_type to
	// uniquely identify a poc in the file list.
	
	// ----------
	
	GROUP_BASE_DEFINITION_STRUCT	base;

	// -------------------------------------------- //
	//	SYSTEM_THIS_POC_BELONGS_TO
	// -------------------------------------------- //

	UNS_32			GID_irrigation_system;

	// -------------------------------------------- //
	//	WHICH PHYSICAL POC IS THIS? - THESE PARAMETERS PIN THAT DOWN
	// -------------------------------------------- //

	// 4/17/2014 ajv : The 0-based box (controller) index which coincides with
	// the controller's communication address.
	UNS_32			box_index_0;
	
	// ----------

	// 5/19/2014 rmd : One of these three types:	POC__FILE_TYPE__TERMINAL
	//  											POC__FILE_TYPE__DECODER_SINGLE
	//  											POC__FILE_TYPE__DECODER_BYPASS
	// 5/19/2014 rmd : Note the naming convention to help ensure no confusion on which defines
	// to use for this variable.
	UNS_32			poc_type__file_type;

	// ----------

	// 4/17/2014 ajv : The decoder serial number assigned to this POC. For
	// POC_TYPE_TERMINAL, this should always be 0. For Bypass Manifolds
	// comprised of multiple single-POC decoders; position 0 of the array
	// indicates stage 1 (the smallest level), position 1 is stage 2, and so on.
	UNS_32			decoder_serial_number[ POC_BYPASS_LEVELS_MAX ];

	// -------------------------------------------- //
	//	WHETHER THE POC MODULES ARE CONNECTED TO THE TP BOARD
	// -------------------------------------------- //

	// 3/4/2013 ajv : Tracks whether the POC card and terminal are both physically connected to
	// the TP Micro or not. This is a single flag for the whole POC. Even for the case of a
	// BYPASS. So if one decoder is missing the whole BYPASS is considered unavailable. This
	// flag is set upon creation of the bypass. And cleared at conclusion of a discovery if one
	// of the decoders is missing.
	
	// 6/3/2016 rmd : This used to be called physically available. And we used it to represent
	// just that. So if a POC terminal and card was detected we automatically created the file
	// POC and set this (true). If you removed the terminal (as part of maintenance) we would
	// detect that and automatically set this (false). Well this term was included in the POC
	// preserves sync so when set (false) would be removed from the preserves. THE PROBLEM with
	// this is that if you had budgets turned ON and you only temporarily removed the terminal
	// card, when terminal was replaced you had lost your budgets use so far in the budget
	// billing cycle. And that was unacceptable.
	//
	// 6/6/2016 rmd : So renamed and repurposed this variable to represent a variable that removes
	// the POC from the preserves and hides it from the display. It is set automatically for the
	// case of the TERMINAL. And for decoder based POCs when the user specifies the decoder is in
	// use. For the case of the TERMINAL it is NEVER set (false) (at this time - plans are to be
	// able to do so at the web). For the decoders it can be set false by moving the decoder into
	// a BYPASS or UNLINKING the decoder if it is not found during a discovery.
	//
	// BEWARE when set (false) the POC is removed from the preserves and that destroys the budget
	// accumulators for this billing cycle. There is no recovery.
	BOOL_32			show_this_poc;

	// 7/8/2014 rmd : A working variable needed to temporarily track decoder presence when
	// working to set the physically available variable following a decoder discovery. The
	// complication is driven by the presence of a BYPASS poc. The setting of this variable is
	// NOT to be distributed and written to the file. That is not needed. And this variable
	// should not be used for any purpose other than what it is already used for. It has no
	// other use.
	BOOL_32			individually_available[ POC_BYPASS_LEVELS_MAX ];

	// -------------------------------------------- //
	//	COMMON POC DETAILS
	// -------------------------------------------- //

	// 2011.12.20 ajv : We use the poc_usage setting to determine whether to
	// display the POC in the list of available POCs or not. This way, if the
	// chain only has one flow meter and master valve connected, only the single
	// POC is displayed when building a system.
	//
	//  #define POC_USAGE_NOT_USED			(0)
	//  #define POC_USAGE_IRRIGATION		(1)
	//  #define POC_USAGE_NON_IRRIGATION	(2)
	UNS_32			poc_usage;
	
	// -------------------------------------------- //

	// Normally OPEN or Normally CLOSED.
	UNS_32	  		master_valve_type[ POC_BYPASS_LEVELS_MAX ];

	// ----------
	
	// 1/29/2015 rmd : 36 unused bytes within the structure. This was the flow meter choice with
	// K and O. But was obsoleted when we added the reed_switch setting to the structure.
	UNS_32			unused_a[ 9 ];

	// -------------------------------------------- //
	//	BYPASS SPECIFICS
	// -------------------------------------------- //

	// 4/17/2014 ajv : The number of stages of the bypass manifold. For now,
	// this should always either be 2 or 3.
	UNS_32			bypass_number_of_levels;
	
	// ----------
	
	// 1/29/2015 rmd : Was preliminary budget variables. Made unused when structure revision
	// moved to revision 1.
	UNS_32		unused_b[ 2 ];

	// ----------
	
	// 1/29/2015 rmd : The new flow meter choice, K, O, and reed_switch information. Introduced
	// when structure revision moved from 0 to revsion 1.
	POC_FM_CHOICE_AND_RATE_DETAILS	flow_meter[ POC_BYPASS_LEVELS_MAX ];
	
	// -------------------------------------------- //
	//  CHANGE BITS
	// -------------------------------------------- //

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

	// ----------

	// 4/30/2015 ajv : Added in Rev 2

	// ----------
	
	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;
	
	// ----------
	
	// 9/9/2015 rmd : Added in Rev 3.

	// ----------
	
	// 9/9/2015 rmd : Options are direct entry, and to calculate based upon total station square
	// footage and historical et. IMPORTANT QUESTION - is the second option to calculate the budget 
	// available if the mainline uses more than 1 POC? - because we're not sure how to split the
	// budget number among the POC's. A third option is to enter projected budget from the water
	// bill and modify using delivered ET.
	// POC_BUDGET_GALLONS_ENTRY_OPTION_direct_entry (0)
	// POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_as_percent_of_historical_et (1)
	// POC_BUDGET_GALLONS_ENTRY_OPTION_projected_budget_from_water_bill (2)
	UNS_32		budget_gallons_entry_option;
	
	// 10/29/2015 rmd : Used if the user wants US to calculate the budget.
	UNS_32		budget_calculation_percent_of_et;

	// ----------
	 
	// 7/11/2018 ajv : Historically, we activate all pump outputs when irrigation starts when 
	// valves are set to use a pump. However, now that we can independently monitor the current 
	// draw of each output, this results in NO ELECTRICAL CURRENT alerts on all pump terminals 
	// where a pump is not physically attached. This new setting allows the user to stop this 
	// behavior by only energizing pump outputs where a pump is attached. 
	BOOL_32		has_pump_attached;
	
	BOOL_32		unused_d;

	// ----------
	
	// 11/5/2015 rmd : The budgeted amount for this POC by period. Note this array holds 24
	// values but we never use more than the first 12 entries. Or is it 13 (allowing the user
	// to enter the coming 12 periods)? The 24 comes from development starts and stops and
	// just ended up this way.
	UNS_32		budget_gallons[ IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS ];
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void nm_POC_set_name( POC_GROUP_STRUCT *const ppoc,
							 char *pname,
							 const BOOL_32 pgenerate_change_line_bool,
							 const UNS_32 preason_for_change,
							 const UNS_32 pbox_index_0,
							 const BOOL_32 pset_change_bits,
							 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
							 #ifdef _MSC_VER
							 , char *pmerge_update_str,
							 char *pmerge_insert_fields_str,
							 char *pmerge_insert_values_str
							 #endif
						   )
{
	SHARED_set_name_32_bit_change_bits( ppoc,
										pname,
										pgenerate_change_line_bool,
										preason_for_change,
										pbox_index_0,
										pset_change_bits,
										pchange_bitfield_to_set,
										&ppoc->changes_to_upload_to_comm_server,
										POC_CHANGE_BITFIELD_description_bit
										#ifdef _MSC_VER
										, POC_database_field_names[ POC_CHANGE_BITFIELD_description_bit ],
										pmerge_update_str,
										pmerge_insert_fields_str,
										pmerge_insert_values_str
										#endif
									  );
}

/* ---------------------------------------------------------- */
/**
 * Sets the controller serial number associated with a particular POC. If the
 * passed in group is NULL, an error is raised and a 0 is returned.
 * Additionally, if the parameter that's passed in is out-of-range, an error is
 * raised and the value isn't updated.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param ppoc A pointer to the group to update.
 * @param pcontroller_serial_number The serial number to assign to the group.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 8/11/2011 Adrianusv
 *
 * @revisions
 *    8/11/2011 Initial release
 *    4/5/2012  Added setting of change bits to distribute changes
 */
extern void nm_POC_set_box_index( void *const ppoc,
								  UNS_32 pbox_index,
								  const BOOL_32 pgenerate_change_line_bool,
								  const UNS_32 preason_for_change,
								  const UNS_32 pbox_index_0,
								  const BOOL_32 pset_change_bits,
								  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
								  #ifdef _MSC_VER
								  , char *pmerge_update_str,
								  char *pmerge_insert_fields_str,
								  char *pmerge_insert_values_str
								  #endif
								)
{
	if( SHARED_set_uint32_with_32_bit_change_bits_group( ppoc,
														 &(((POC_GROUP_STRUCT*)ppoc)->box_index_0),
														 pbox_index,
														 BOX_INDEX_MINIMUM,
														 BOX_INDEX_MAXIMUM,
														 BOX_INDEX_DEFAULT,
														 pgenerate_change_line_bool,
														 CHANGE_POC_BOX_INDEX,
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((POC_GROUP_STRUCT*)ppoc)->changes_to_upload_to_comm_server),
														 POC_CHANGE_BITFIELD_box_index_bit,
														 POC_database_field_names[ POC_CHANGE_BITFIELD_box_index_bit ]
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   ) == (true) )
	{
		#ifndef	_MSC_VER

			// 7/18/2012 rmd : The poc_preserves contains the box_index. Any changes we need to trigger
			// a re-sync.
			POC_PRESERVES_request_a_resync();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * Sets the decoder serial number for a particular POC. If the passed in group
 * is NULL, an error is raised and a 0 is returned. Additionally, if the
 * parameter that's passed in is out-of-range, an error is raised and the value
 * isn't updated.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param ppoc A pointer to the group to update.
 * @param pdecoder_serial_number The decoder serial number to assign to the
 *  							 group.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 8/11/2011 Adrianusv
 *
 * @revisions
 *    8/11/2011 Initial release
 *    4/5/2012  Added setting of change bits to distribute changes
 */
extern void nm_POC_set_decoder_serial_number( void *const ppoc,
											  const UNS_32 plevel,
											  UNS_32 pdecoder_serial_number,
											  const BOOL_32 pgenerate_change_line_bool,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
										    )
{
	char	lfield_name[ 32 ];

	if( (plevel >= 1) && (plevel <= POC_BYPASS_LEVELS_MAX) )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", POC_database_field_names[ POC_CHANGE_BITFIELD_decoder_serial_number_bit ], plevel );

		if( SHARED_set_uint32_with_32_bit_change_bits_group( ppoc,
															 &(((POC_GROUP_STRUCT*)ppoc)->decoder_serial_number[ (plevel - 1) ]),
															 pdecoder_serial_number,
															 DECODER_SERIAL_NUM_MIN,
															 DECODER_SERIAL_NUM_MAX,
															 DECODER_SERIAL_NUM_DEFAULT,
															 pgenerate_change_line_bool,
															 (CHANGE_POC_DECODER_SERIAL_NUMBER1 + plevel - 1),
															 preason_for_change,
															 pbox_index_0,
															 pset_change_bits,
															 pchange_bitfield_to_set,
															 &(((POC_GROUP_STRUCT*)ppoc)->changes_to_upload_to_comm_server),
															 POC_CHANGE_BITFIELD_decoder_serial_number_bit,
															 lfield_name
															 #ifdef _MSC_VER
															 , pmerge_update_str,
															 pmerge_insert_fields_str,
															 pmerge_insert_values_str
															 #endif
														   ) == (true) )
		{
			#ifndef	_MSC_VER

				// 7/18/2012 rmd : The poc_preserves contains the decoder serial number. So if it changes we
				// need to trigger a re-sync.
				POC_PRESERVES_request_a_resync();

			#endif
		}
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
extern void nm_POC_set_show_this_poc(	void *const ppoc,
										BOOL_32 pshow_this_poc,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const BOOL_32 pset_change_bits,
										CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
									)
{
	if( SHARED_set_bool_with_32_bit_change_bits_group( ppoc,
													   &(((POC_GROUP_STRUCT*)ppoc)->show_this_poc),
													   pshow_this_poc,
													   POC_SHOW_THIS_POC_DEFAULT,
													   pgenerate_change_line_bool,
													   CHANGE_POC_PHYSICALLY_AVAILABLE,
													   preason_for_change,
													   pbox_index_0,
													   pset_change_bits,
													   pchange_bitfield_to_set,
													   &(((POC_GROUP_STRUCT*)ppoc)->changes_to_upload_to_comm_server),
													   POC_CHANGE_BITFIELD_poc_physically_available_bit,
													   POC_database_field_names[ POC_CHANGE_BITFIELD_poc_physically_available_bit ]
													   #ifdef _MSC_VER
													   , pmerge_update_str,
													   pmerge_insert_fields_str,
													   pmerge_insert_values_str
													   #endif
													 ) == (true) )
	{
		#ifndef	_MSC_VER

			// 7/18/2012 rmd : The poc_preserves contains the box_index. Any changes we need to trigger
			// a re-sync.
			POC_PRESERVES_request_a_resync();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * Sets whether a particular POC is in use or not. If the passed in group is
 * NULL, an error is raised and a 0 is returned. Additionally, if the parameter
 * that's passed in is out-of-range, an error is raised and the value isn't
 * updated.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param ppoc A pointer to the group to update.
 * @param ppoc_in_use_bool True if the POC is in use otherwise, false.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 8/12/2011 Adrianusv
 *
 * @revisions
 *    8/12/2011 Initial release
 *    4/5/2012  Added setting of change bits to distribute changes
 */
extern void nm_POC_set_poc_usage( void *const ppoc,
								  UNS_32 ppoc_usage,
								  const BOOL_32 pgenerate_change_line_bool,
								  const UNS_32 preason_for_change,
								  const UNS_32 pbox_index_0,
								  const BOOL_32 pset_change_bits,
								  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
								  #ifdef _MSC_VER
								  , char *pmerge_update_str,
								  char *pmerge_insert_fields_str,
								  char *pmerge_insert_values_str
								  #endif
								)
{
	if( SHARED_set_uint32_with_32_bit_change_bits_group( ppoc,
														 &(((POC_GROUP_STRUCT*)ppoc)->poc_usage),
														 ppoc_usage,
														 POC_USAGE_MIN,
														 POC_USAGE_MAX,
														 POC_USAGE_DEFAULT,
														 pgenerate_change_line_bool,
														 CHANGE_POC_USAGE,
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((POC_GROUP_STRUCT*)ppoc)->changes_to_upload_to_comm_server),
														 POC_CHANGE_BITFIELD_poc_usage_bit,
														 POC_database_field_names[ POC_CHANGE_BITFIELD_poc_usage_bit ]
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   ) == (true) )
	{
		#ifndef	_MSC_VER

			// 7/18/2012 rmd : The poc_preserves contains the type of poc. So if it changes we need to
			// trigger a re-sync.
			POC_PRESERVES_request_a_resync();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * Sets the type of POC (i.e., POC_TYPE_TERMINAL, POC_TYPE_2_WIRE_POC_DECODER,
 * orPOC_TYPE_2_WIRE_BYPASS_DECODER) for a particular POC. If the passed in
 * group is NULL, an error is raised and a 0 is returned. Additionally, if the
 * parameter that's passed in is out-of-range, an error is raised and the value
 * isn't updated.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param ppoc A pointer to the group to update.
 * @param ptype_of_poc The type of POC.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 8/12/2011 Adrianusv
 *
 * @revisions
 *    8/12/2011 Initial release
 *    4/5/2012  Added setting of change bits to distribute changes
 */
extern void nm_POC_set_poc_type( void *const ppoc,
								 UNS_32 ptype_of_poc,
								 const BOOL_32 pgenerate_change_line_bool,
								 const UNS_32 preason_for_change,
								 const UNS_32 pbox_index_0,
								 const BOOL_32 pset_change_bits,
								 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
								 #ifdef _MSC_VER
								 , char *pmerge_update_str,
								 char *pmerge_insert_fields_str,
								 char *pmerge_insert_values_str
								 #endif
							   )
{
	if( SHARED_set_uint32_with_32_bit_change_bits_group( ppoc,
														 &(((POC_GROUP_STRUCT*)ppoc)->poc_type__file_type),
														 ptype_of_poc,
														 POC__FILE_TYPE__MIN,
														 POC__FILE_TYPE__MAX,
														 POC__FILE_TYPE__DEFAULT,
														 pgenerate_change_line_bool,
														 CHANGE_POC_TYPE,
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((POC_GROUP_STRUCT*)ppoc)->changes_to_upload_to_comm_server),
														 POC_CHANGE_BITFIELD_type_of_poc_bit,
														 POC_database_field_names[ POC_CHANGE_BITFIELD_type_of_poc_bit ]
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   ) == (true) )
	{
		#ifndef	_MSC_VER

			// 7/18/2012 rmd : The poc_preserves contains the type of poc. So if it changes we need to
			// trigger a re-sync.
			POC_PRESERVES_request_a_resync();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * Sets the master valve type of a particular POC. Level 1 is used for the
 * built-in POC, levels 2 and 3 are available when using the POC as a bypass
 * manifold. If the passed in group is NULL, an error is raised and a 0 is
 * returned. Additionally, if the parameter that's passed in is out-of-range, an
 * error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param ppoc A pointer to the group to update.
 * @param plevel A 1-based level which indicates the level of the master valve
 *               when using a Bypass Manifold. If not using a Bypass Manifold,
 *  			 this number should always be 1.
 * @param pmaster_valve_type The master valve type to assign to the level.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 12/20/2011 Adrianusv
 *
 * @revisions
 *    12/20/2011 Initial release
 *    4/5/2012   Added setting of change bits to distribute changes
 *    4/12/2012  Added identification of which level master valve is out of
 *               range
 */
static void nm_POC_set_mv_type( void *const ppoc,
								const UNS_32 plevel,
								UNS_32 pmaster_valve_type,
								const BOOL_32 pgenerate_change_line_bool,
								const UNS_32 preason_for_change,
								const UNS_32 pbox_index_0,
								const BOOL_32 pset_change_bits,
								CHANGE_BITS_32_BIT *pchange_bitfield_to_set
								#ifdef _MSC_VER
								, char *pmerge_update_str,
								char *pmerge_insert_fields_str,
								char *pmerge_insert_values_str
								#endif
							  )
{
	char	lfield_name[ 32 ];

	if( (plevel >= 1) && (plevel <= POC_BYPASS_LEVELS_MAX) )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", POC_database_field_names[ POC_CHANGE_BITFIELD_master_valve_type_bit ], plevel );

		if( SHARED_set_uint32_with_32_bit_change_bits_group( ppoc,
															 &(((POC_GROUP_STRUCT*)ppoc)->master_valve_type[ (plevel - 1) ]),
															 pmaster_valve_type,
															 POC_MV_TYPE_NORMALLY_OPEN,
															 POC_MV_TYPE_NORMALLY_CLOSED,
															 POC_MV_TYPE_DEFAULT,
															 pgenerate_change_line_bool,
															 (CHANGE_POC_MV_TYPE1 + plevel - 1),
															 preason_for_change,
															 pbox_index_0,
															 pset_change_bits,
															 pchange_bitfield_to_set,
															 &(((POC_GROUP_STRUCT*)ppoc)->changes_to_upload_to_comm_server),
															 POC_CHANGE_BITFIELD_master_valve_type_bit,
															 lfield_name
															 #ifdef _MSC_VER
															 , pmerge_update_str,
															 pmerge_insert_fields_str,
															 pmerge_insert_values_str
															 #endif
														   ) == (true) )
		{
			#ifndef	_MSC_VER

				// 7/18/2012 rmd : The poc_preserves contains the mv type. So if it changes we need to
				// trigger a re-sync.
				POC_PRESERVES_request_a_resync();

			#endif
		}
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * Sets the flow meter type of a particular POC. Level 1 is used for the
 * built-in POC, levels 2 and 3 are available when using the POC as a bypass
 * manifold. If the passed in group is NULL, an error is raised and a 0 is
 * returned. Additionally, if the parameter that's passed in is out-of-range, an
 * error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param ppoc A pointer to the group to update.
 * @param plevel A 1-based level which indicates the level of the master valve
 *               when using a Bypass Manifold. If not using a Bypass Manifold,
 *  			 this number should always be 1.
 * @param pflow_meter_type The flow valve type to assign to the level.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 12/20/2011 Adrianusv
 *
 * @revisions
 *    12/20/2011 Initial release
 *    4/5/2012   Added setting of change bits to distribute changes
 *    4/12/2012  Added identification of which level flow meter is out of range
 */
static void nm_POC_set_fm_type( void *const ppoc,
								const UNS_32 plevel,
								UNS_32 pflow_meter_type,
								const BOOL_32 pgenerate_change_line_bool,
								const UNS_32 preason_for_change,
								const UNS_32 pbox_index_0,
								const BOOL_32 pset_change_bits,
								CHANGE_BITS_32_BIT *pchange_bitfield_to_set
								#ifdef _MSC_VER
								, char *pmerge_update_str,
								char *pmerge_insert_fields_str,
								char *pmerge_insert_values_str
								#endif
							  )
{
	char	lfield_name[ 32 ];

	if( (plevel >= 1) && (plevel <= POC_BYPASS_LEVELS_MAX) )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%sChoice%d", POC_database_field_names[ POC_CHANGE_BITFIELD_flow_meter_type_bit ], plevel );

		SHARED_set_uint32_with_32_bit_change_bits_group( ppoc,
														 &(((POC_GROUP_STRUCT*)ppoc)->flow_meter[ (plevel - 1) ].flow_meter_choice),
														 pflow_meter_type,
														 POC_FM_CHOICE_MIN,
														 POC_FM_CHOICE_MAX,
														 POC_FM_CHOICE_DEFAULT,
														 pgenerate_change_line_bool,
														 (CHANGE_POC_FM_TYPE1 + plevel - 1),
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((POC_GROUP_STRUCT*)ppoc)->changes_to_upload_to_comm_server),
														 POC_CHANGE_BITFIELD_flow_meter_type_bit,
														 lfield_name
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   );
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * Sets the K value of a particular POC. Level 1 is used for the built-in POC,
 * levels 2 and 3 are available when using the POC as a bypass manifold. If the
 * passed in group is NULL, an error is raised and a 0 is returned.
 * Additionally, if the parameter that's passed in is out-of-range, an error is
 * raised and the value isn't updated.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param ppoc A pointer to the group to update.
 * @param plevel A 1-based level which indicates the level of the master valve
 *               when using a Bypass Manifold. If not using a Bypass Manifold,
 *  			 this number should always be 1.
 * @param pkvalue_100000u The K value to assign to the level.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 12/20/2011 Adrianusv
 *
 * @revisions
 *    12/20/2011 Initial release
 *    4/5/2012   Added setting of change bits to distribute changes
 *    4/12/2012  Added identification of which level k value is out of range
 */
static void nm_POC_set_kvalue( void *const ppoc,
							   const UNS_32 plevel,
							   UNS_32 pkvalue_100000u,
							   const BOOL_32 pgenerate_change_line_bool,
							   const UNS_32 preason_for_change,
							   const UNS_32 pbox_index_0,
							   const BOOL_32 pset_change_bits,
							   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
							   #ifdef _MSC_VER
							   , char *pmerge_update_str,
							   char *pmerge_insert_fields_str,
							   char *pmerge_insert_values_str
							   #endif
							 )
{
	char	lfield_name[ 32 ];

	if( (plevel >= 1) && (plevel <= POC_BYPASS_LEVELS_MAX) )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%sK_Value%d", POC_database_field_names[ POC_CHANGE_BITFIELD_flow_meter_type_bit ], plevel );

		SHARED_set_uint32_with_32_bit_change_bits_group( ppoc,
														 &(((POC_GROUP_STRUCT*)ppoc)->flow_meter[ (plevel - 1) ].kvalue_100000u),
														 pkvalue_100000u,
														 (UNS_32)(POC_FM_KVALUE_MIN * 100000.0F),
														 (UNS_32)(POC_FM_KVALUE_MAX * 100000.0F),
														 (UNS_32)(POC_FM_KVALUE_DEFAULT * 100000.0F),
														 pgenerate_change_line_bool,
														 (CHANGE_POC_K_VALUE1 + plevel - 1),
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((POC_GROUP_STRUCT*)ppoc)->changes_to_upload_to_comm_server),
														 POC_CHANGE_BITFIELD_flow_meter_type_bit,
														 lfield_name
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   );
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * Sets the offset of a particular POC. Level 1 is used for the built-in POC,
 * levels 2 and 3 are available when using the POC as a bypass manifold. If the
 * passed in group is NULL, an error is raised and a 0 is returned.
 * Additionally, if the parameter that's passed in is out-of-range, an error is
 * raised and the value isn't updated.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param ppoc A pointer to the group to update.
 * @param plevel A 1-based level which indicates the level of the master valve
 *               when using a Bypass Manifold. If not using a Bypass Manifold,
 *  			 this number should always be 1.
 * @param poffset_100000u The offset to assign to the level. @param
 * pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 12/20/2011 Adrianusv
 *
 * @revisions
 *    12/20/2011 Initial release
 *    4/5/2012   Added setting of change bits to distribute changes
 *    4/12/2012  Added identification of which level offset is out of range
 */
static void nm_POC_set_offset( void *const ppoc,
							   const UNS_32 plevel,
							   INT_32 poffset_100000u,
							   const BOOL_32 pgenerate_change_line_bool,
							   const UNS_32 preason_for_change,
							   const UNS_32 pbox_index_0,
							   const BOOL_32 pset_change_bits,
							   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
							   #ifdef _MSC_VER
							   , char *pmerge_update_str,
							   char *pmerge_insert_fields_str,
							   char *pmerge_insert_values_str
							   #endif
							 )
{
	char	lfield_name[ 32 ];

	if( (plevel >= 1) && (plevel <= POC_BYPASS_LEVELS_MAX) )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%sOffset_Value%d", POC_database_field_names[ POC_CHANGE_BITFIELD_flow_meter_type_bit ], plevel );

		SHARED_set_int32_with_32_bit_change_bits_group( ppoc,
														 &(((POC_GROUP_STRUCT*)ppoc)->flow_meter[ (plevel - 1) ].offset_100000u),
														 poffset_100000u,
														 (INT_32)(POC_FM_OFFSET_MIN * 100000.0F),
														 (INT_32)(POC_FM_OFFSET_MAX * 100000.0F),
														 (INT_32)(POC_FM_OFFSET_DEFAULT * 100000.0F),
														 pgenerate_change_line_bool,
														 (CHANGE_POC_OFFSET1 + plevel - 1),
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((POC_GROUP_STRUCT*)ppoc)->changes_to_upload_to_comm_server),
														 POC_CHANGE_BITFIELD_flow_meter_type_bit,
														 lfield_name
														#ifdef _MSC_VER
														, pmerge_update_str,
														pmerge_insert_fields_str,
														pmerge_insert_values_str
														#endif
													  );
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
static void nm_POC_set_reed_switch( void *const ppoc,
									const UNS_32 plevel,
									UNS_32 preed_switch,
									const BOOL_32 pgenerate_change_line_bool,
									const UNS_32 preason_for_change,
									const UNS_32 pbox_index_0,
									const BOOL_32 pset_change_bits,
									CHANGE_BITS_32_BIT *pchange_bitfield_to_set
									#ifdef _MSC_VER
									, char *pmerge_update_str,
									char *pmerge_insert_fields_str,
									char *pmerge_insert_values_str
									#endif
								   )
{
	char	lfield_name[ 32 ];

	if( (plevel >= 1) && (plevel <= POC_BYPASS_LEVELS_MAX) )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%sReedSwitch%d", POC_database_field_names[ POC_CHANGE_BITFIELD_flow_meter_type_bit ], plevel );

		SHARED_set_uint32_with_32_bit_change_bits_group( ppoc,
														 &(((POC_GROUP_STRUCT*)ppoc)->flow_meter[ (plevel - 1) ].reed_switch),
														 preed_switch,
														 POC_FM_REED_SWITCH_MIN,
														 POC_FM_REED_SWITCH_MAX,
														 POC_FM_REED_SWITCH_DEFAULT,
														 pgenerate_change_line_bool,
														 (CHANGE_POC_REED_SWITCH_1 + plevel - 1),
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((POC_GROUP_STRUCT*)ppoc)->changes_to_upload_to_comm_server),
														 POC_CHANGE_BITFIELD_flow_meter_type_bit,
														 lfield_name
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   );
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
extern void nm_POC_set_bypass_number_of_levels( void *const ppoc,
												BOOL_32 pnumber_of_levels,
												const BOOL_32 pgenerate_change_line_bool,
												const UNS_32 preason_for_change,
												const UNS_32 pbox_index_0,
												const BOOL_32 pset_change_bits,
												CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												#ifdef _MSC_VER
												, char *pmerge_update_str,
												char *pmerge_insert_fields_str,
												char *pmerge_insert_values_str
												#endif
											  )
{
	SHARED_set_uint32_with_32_bit_change_bits_group( ppoc,
													 &(((POC_GROUP_STRUCT*)ppoc)->bypass_number_of_levels),
													 pnumber_of_levels,
													 POC_BYPASS_LEVELS_MIN,
													 POC_BYPASS_LEVELS_MAX,
													 POC_BYPASS_LEVELS_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_POC_BYPASS_NUMBER_OF_LEVELS,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((POC_GROUP_STRUCT*)ppoc)->changes_to_upload_to_comm_server),
													 POC_CHANGE_BITFIELD_bypass_number_of_levels,
													 POC_database_field_names[ POC_CHANGE_BITFIELD_bypass_number_of_levels ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
/**
 * Sets the irrigation system group ID that a particular POC is assigned to. If 
 * the passed in group is NULL, an error is raised and a 0 is returned. 
 * Additionally, if the parameter that's passed in is out-of-range, an error is 
 * raised and the value isn't updated. 
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *  		 a change is made from the central or handheld radio remote.
 *  
 * @param ppoc A pointer to the group to update.
 * @param pGID_irrigation_system The group ID of the irrigation system the POC 
 *  							 is assigned to.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 8/12/2011 Adrianusv
 *
 * @revisions
 *    8/12/2011 Initial release
 *    4/5/2012  Added setting of change bits to distribute changes
 */
extern void nm_POC_set_GID_irrigation_system( void *const ppoc,
											  const UNS_32 pGID_irrigation_system,
											  const BOOL_32 pgenerate_change_line_bool,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
											)
{
	// 5/12/2014 ajv : Since we don't have a change line built for this yet,
	// don't try to generate a change line automatically. Instead, build one up
	// manually if a change occurred.
	if( SHARED_set_uint32_with_32_bit_change_bits_group( ppoc,
														 &(((POC_GROUP_STRUCT*)ppoc)->GID_irrigation_system),
														 pGID_irrigation_system,
														 1,
														 UNS_32_MAX,
														 #ifndef _MSC_VER
															// 9/23/2015 ajv : The CommServer doesn't have access to this function, so set the default
															// to 1 for now until we figure out a better solution.
															nm_GROUP_get_group_ID( SYSTEM_get_group_at_this_index( 0 ) ),
														 #else
															1,
														 #endif
														 CHANGE_do_not_generate_change_line,
														 CHANGE_POC_SYSTEM_GID,
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((POC_GROUP_STRUCT*)ppoc)->changes_to_upload_to_comm_server),
														 POC_CHANGE_BITFIELD_GID_irri_system_bit,
														 POC_database_field_names[ POC_CHANGE_BITFIELD_GID_irri_system_bit ]
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   ) == (true) )
	{
		#ifndef	_MSC_VER

			IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

			xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

			lsystem = SYSTEM_get_group_with_this_GID( pGID_irrigation_system );

			if( pgenerate_change_line_bool == (true))
			{
				Alert_poc_assigned_to_mainline( nm_GROUP_get_name(ppoc), nm_GROUP_get_name(lsystem), preason_for_change, pbox_index_0 );
			}

			xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

			// 7/18/2012 rmd : The poc_preserves contain a pointer to the system_preserves record that
			// is locked to the system the poc uses. So if we change the system we must change the
			// pointer in the poc_preserves.
			POC_PRESERVES_request_a_resync();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * Set the POC option for entering in budget values. Although each POC has it's
 * own setting, all the POCs for mainline are supposed to have the same value.
 * It is the callers responsibility to ensure this. 
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen;
 *  
 * @param ppoc A pointer to the group to update.
 * @param pbudget_gallons_entry_option The value of the option.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 4/05/2016 skc
 *
 * @revisions
 *    4/5/2012  Made function an extern
 */
extern void nm_POC_set_budget_gallons_entry_option(	void *const ppoc,
													UNS_32 pbudget_gallons_entry_option,
													const BOOL_32 pgenerate_change_line_bool,
													const UNS_32 preason_for_change,
													const UNS_32 pbox_index_0,
													const BOOL_32 pset_change_bits,
													CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													#ifdef _MSC_VER
													, char *pmerge_update_str,
													char *pmerge_insert_fields_str,
													char *pmerge_insert_values_str
													#endif
												  )
{
	if ( ( SHARED_set_uint32_with_32_bit_change_bits_group( ppoc,
													 &(((POC_GROUP_STRUCT*)ppoc)->budget_gallons_entry_option),
													 pbudget_gallons_entry_option,
													 POC_BUDGET_GALLONS_ENTRY_OPTION_MIN,
													 POC_BUDGET_GALLONS_ENTRY_OPTION_MAX,
													 POC_BUDGET_GALLONS_ENTRY_OPTION_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_POC_BUDGET_GALLONS_ENTRY_OPTION,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((POC_GROUP_STRUCT*)ppoc)->changes_to_upload_to_comm_server),
													 POC_CHANGE_BITFIELD_budget_gallons_entry_option_bit,
													 POC_database_field_names[ POC_CHANGE_BITFIELD_budget_gallons_entry_option_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   ) == (true) ) && ( pbudget_gallons_entry_option == POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_using_et ) )
	{
		#ifndef _MSC_VER
		// 9/14/2018 Ryan : set flag that budget will need to be updated since the entry option
		// just changed to the et option.
		g_BUDGET_using_et_calculate_budget = (true);
		#endif
	}
}

/* ---------------------------------------------------------- */
extern void nm_POC_set_budget_calculation_percent_of_et(	void *const ppoc,
															UNS_32 pbudget_calculation_percent_of_et,
															const BOOL_32 pgenerate_change_line_bool,
															const UNS_32 preason_for_change,
															const UNS_32 pbox_index_0,
															const BOOL_32 pset_change_bits,
															CHANGE_BITS_32_BIT *pchange_bitfield_to_set
															#ifdef _MSC_VER
															, char *pmerge_update_str,
															char *pmerge_insert_fields_str,
															char *pmerge_insert_values_str
															#endif
													   )
{
	if ( SHARED_set_uint32_with_32_bit_change_bits_group( ppoc,
													 &(((POC_GROUP_STRUCT*)ppoc)->budget_calculation_percent_of_et),
													 pbudget_calculation_percent_of_et,
													 POC_BUDGET_CALCULATION_PERCENT_OF_ET_MIN,
													 POC_BUDGET_CALCULATION_PERCENT_OF_ET_MAX,
													 POC_BUDGET_CALCULATION_PERCENT_OF_ET_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_POC_BUDGET_CALCULATION_PERCENT_OF_ET,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((POC_GROUP_STRUCT*)ppoc)->changes_to_upload_to_comm_server),
													 POC_CHANGE_BITFIELD_budget_calc_percent_of_et_bit,
													 POC_database_field_names[ POC_CHANGE_BITFIELD_budget_calc_percent_of_et_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   ) == (true) )
	{
		#ifndef _MSC_VER
		// 9/14/2018 Ryan : set flag that budget will need to be updated to reflect a changed et
		// percent.
		g_BUDGET_using_et_calculate_budget = (true);
		#endif
	}
}

/* ---------------------------------------------------------- */
static void nm_POC_set_budget_gallons(	void *const ppoc,
										const UNS_32 pperiod_index0,
										UNS_32 pbudget_gallons,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const BOOL_32 pset_change_bits,
										CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
									 )
{
	char	lfield_name[ 32 ];
	UNS_32 changeline;

	if( (pperiod_index0 >= 0) && (pperiod_index0 < IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS) )
	{
		// 9/17/2015 rmd : Develop the DB field name. Wissam names them 1..12.
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", POC_database_field_names[ POC_CHANGE_BITFIELD_budget_gallons_bit ], pperiod_index0+1 );

		// ----------

		// 8/1/2016 skc : Send different ID based on the units configuration
		changeline = CHANGE_POC_BUDGET_GALLONS_0; 
#ifndef _MSC_VER
		if( NETWORK_CONFIG_get_water_units() == NETWORK_CONFIG_WATER_UNIT_HCF )
		{
			changeline = CHANGE_POC_BUDGET_HCF_0;
		}
#endif

		SHARED_set_uint32_with_32_bit_change_bits_group(	ppoc,
															&(((POC_GROUP_STRUCT*)ppoc)->budget_gallons[ pperiod_index0 ]),
															pbudget_gallons,
															POC_BUDGET_GALLONS_MIN,
															POC_BUDGET_GALLONS_MAX,
															POC_BUDGET_GALLONS_DEFAULT,
															pgenerate_change_line_bool,
															(changeline + pperiod_index0),
															preason_for_change,
															pbox_index_0,
															pset_change_bits,
															pchange_bitfield_to_set,
															&(((POC_GROUP_STRUCT*)ppoc)->changes_to_upload_to_comm_server),
															POC_CHANGE_BITFIELD_budget_gallons_bit,
															lfield_name
															#ifdef _MSC_VER
															, pmerge_update_str,
															pmerge_insert_fields_str,
															pmerge_insert_values_str
															#endif
													   );
	}
	else
	{
		#ifndef _MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *  	  group whose value is being changed isn't deleted during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param ppoc A pointer to the group to update.
 * @param ppump_attached True if the POC has a pump attached; otherwise, false.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 10/8/2018 Adrianusv
 *
 * @revisions
 *    10/8/2018 Initial release
 */
extern void nm_POC_set_has_pump_attached( void *const ppoc,
										  UNS_32 ppump_attached,
										  const BOOL_32 pgenerate_change_line_bool,
										  const UNS_32 preason_for_change,
										  const UNS_32 pbox_index_0,
										  const BOOL_32 pset_change_bits,
										  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										  #ifdef _MSC_VER
										  , char *pmerge_update_str,
										  char *pmerge_insert_fields_str,
										  char *pmerge_insert_values_str
										  #endif
										)
{
	SHARED_set_bool_with_32_bit_change_bits_group( ppoc,
												   &(((POC_GROUP_STRUCT *)ppoc)->has_pump_attached), 
												   ppump_attached,
												   POC_PUMP_PHYSICALLY_ATTACHED_DEFAULT,
												   pgenerate_change_line_bool,
												   CHANGE_POC_HAS_PUMP_ATTACHED,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((POC_GROUP_STRUCT*)ppoc)->changes_to_upload_to_comm_server),
												   POC_CHANGE_BITFIELD_has_pump_attached_bit,
												   POC_database_field_names[ POC_CHANGE_BITFIELD_has_pump_attached_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
/**
 * Locates the group in the list that matches the passed in temporary group and
 * uses the structure's set routines to compare the two and make any changes to
 * the list's group. If any changes are made during this process, a function is
 * called to store the changes in the list.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *        list isn't modified during this process.
 *
 * @executed Executed within the context of the key processing task when the
 *  		 user leaves the screen; within the context of the CommMngr task
 *  		 when a programming change is made from the central or handheld
 *  		 radio remote.
 * 
 * @param ptemporary_group A pointer to a temporary group which contains the
 *                         changes to store.
 * @param ptype_of_change Whether the change is an add, delete, or edit.
 * @param preason_for_change The reason for the change, such as keypad.
 * @param pbox_index_0 The serial number of the controller
 *                                    which initiated the change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 8/11/2011 Adrianusv
 *
 * @revisions
 *    8/11/2011 Initial release
 *    4/5/2012  Removed unnecessary variables and function calls
 *  			Added passing of pset_change_bits into set routine to indicate
 *  			whether the change was made locally so change bits are set
 *  			properly.
 *  			Made procedure static since it's now only called from within
 *  			this file.
 *    4/12/2012 Suppressed saving of bypass manifold settings if there's no
 *  			bypass manifold in use.
 *  			Added check to ensure transition flow rates are within range
 */
static void nm_POC_store_changes( POC_GROUP_STRUCT *const ptemporary_group,
								  const UNS_32 ppoc_created,
								  const UNS_32 preason_for_change,
								  const UNS_32 pbox_index_0,
								  const BOOL_32 pset_change_bits,
								  const UNS_32 pchanges_received_from,
								  CHANGE_BITS_32_BIT pbitfield_of_changes
								  #ifdef _MSC_VER
								  , char *pmerge_update_str,
								  char *pmerge_insert_fields_str,
								  char *pmerge_insert_values_str
								  #endif
								)
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	POC_GROUP_STRUCT	*lpoc;

	BOOL_32	lsync_with_poc_preserves;

	BOOL_32	lthis_bypass_level_is_in_use;

	UNS_32	lnum_levels;

	UNS_32	i;

	// ----------

	#ifndef _MSC_VER

	nm_GROUP_find_this_group_in_list( &poc_group_list_hdr, ptemporary_group, (void**)&lpoc );

	if( lpoc != NULL )
	#else

	lpoc = ptemporary_group;

	#endif
	{
		#ifndef _MSC_VER

			// 5/12/2014 ajv : Only generate the alert that the group was created if
			// it was actually created AND it wasn't created as part of a token or
			// token response receipt.
			if( (ppoc_created == (true)) && ((pchanges_received_from != CHANGE_REASON_CHANGED_AT_SLAVE) && (pchanges_received_from != CHANGE_REASON_DISTRIBUTED_BY_MASTER)) )
			{
				Alert_ChangeLine_Group( CHANGE_GROUP_CREATED, nm_GROUP_get_name( ptemporary_group ), NULL, NULL, NUMBER_OF_CHARS_IN_A_NAME, preason_for_change, pbox_index_0 );
			}

		#endif

		#ifndef _MSC_VER

			lchange_bitfield_to_set = POC_get_change_bits_ptr( lpoc, pchanges_received_from );

		#else

			// 7/28/2015 ajv : Initialize to NULL since this variable is used by the CommServer but
			// expected to be NULL.
			lchange_bitfield_to_set = NULL;

		#endif

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, POC_CHANGE_BITFIELD_description_bit ) == (true)) )
		{
			nm_POC_set_name( lpoc, nm_GROUP_get_name(ptemporary_group), !(ppoc_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
							 #ifdef _MSC_VER
							 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
							 #endif
						   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, POC_CHANGE_BITFIELD_box_index_bit ) == (true)) )
		{
			nm_POC_set_box_index( lpoc, ptemporary_group->box_index_0, !(ppoc_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
								  #ifdef _MSC_VER
								  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
								  #endif
								);
		}

		lsync_with_poc_preserves = (lpoc->show_this_poc != ptemporary_group->show_this_poc);

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, POC_CHANGE_BITFIELD_poc_physically_available_bit ) == (true)) )
		{
			nm_POC_set_show_this_poc(	lpoc, ptemporary_group->show_this_poc, !(ppoc_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
										#ifdef _MSC_VER
										, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
										#endif
									);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, POC_CHANGE_BITFIELD_poc_usage_bit ) == (true)) )
		{
			nm_POC_set_poc_usage( lpoc, ptemporary_group->poc_usage, !(ppoc_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
								  #ifdef _MSC_VER
								  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
								  #endif
								);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, POC_CHANGE_BITFIELD_type_of_poc_bit ) == (true)) )
		{
			nm_POC_set_poc_type( lpoc, ptemporary_group->poc_type__file_type, !(ppoc_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
								 #ifdef _MSC_VER
								 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
								 #endif
							   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, POC_CHANGE_BITFIELD_master_valve_type_bit ) == (true)) )
		{
			// 2012.08.15 ajv : Although there are 3 master valves available on a bypass manifold, only
			// level 1's master valve can be changed from normally closed to normally open. Therefore,
			// only store the changes for level 1's master valve.
			for( lnum_levels = 0; lnum_levels < POC_BYPASS_LEVELS_MAX; ++lnum_levels )
			{
				nm_POC_set_mv_type( lpoc, (lnum_levels + 1), ptemporary_group->master_valve_type[ lnum_levels ], !(ppoc_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
									#ifdef _MSC_VER
									, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
									#endif
								  );
			}
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, POC_CHANGE_BITFIELD_decoder_serial_number_bit ) == (true)) )
		{
			for( lnum_levels = 0; lnum_levels < POC_BYPASS_LEVELS_MAX; ++lnum_levels )
			{
				// 8/20/2014 ajv : Only show change lines for bypass manifold levels that are in use. For
				// example, when a flow meter is changed for level 2, level 3 may be automatically adjusted
				// since level 3 must be at least the same size, or larger, than level 2. If the user
				// only has 2 levels, there's no reason to show the level 3 change line.
				lthis_bypass_level_is_in_use = ((ptemporary_group->poc_type__file_type != POC__FILE_TYPE__DECODER_BYPASS) && (lnum_levels == 0)) ||
											   ((ptemporary_group->poc_type__file_type == POC__FILE_TYPE__DECODER_BYPASS) && (lnum_levels < ptemporary_group->bypass_number_of_levels));

				nm_POC_set_decoder_serial_number( lpoc, (lnum_levels + 1), ptemporary_group->decoder_serial_number[ lnum_levels ], (!(ppoc_created) && lthis_bypass_level_is_in_use), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												  #endif
												);
			}
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, POC_CHANGE_BITFIELD_flow_meter_type_bit ) == (true)) )
		{
			for( lnum_levels = 0; lnum_levels < POC_BYPASS_LEVELS_MAX; ++lnum_levels )
			{
				// 8/20/2014 ajv : Only show change lines for bypass manifold levels that are in use. For
				// example, when a flow meter is changed for level 2, level 3 may be automatically adjusted
				// since level 3 must be at least the same size, or larger, than level 2. If the user
				// only has 2 levels, there's no reason to show the level 3 change line.
				lthis_bypass_level_is_in_use = ((ptemporary_group->poc_type__file_type != POC__FILE_TYPE__DECODER_BYPASS) && (lnum_levels == 0)) ||
											   ((ptemporary_group->poc_type__file_type == POC__FILE_TYPE__DECODER_BYPASS) && (lnum_levels < ptemporary_group->bypass_number_of_levels));

				nm_POC_set_fm_type( lpoc, (lnum_levels + 1), ptemporary_group->flow_meter[ lnum_levels ].flow_meter_choice, (!(ppoc_created) && lthis_bypass_level_is_in_use), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
									#ifdef _MSC_VER
									, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
									#endif
								  );

				nm_POC_set_kvalue( lpoc, (lnum_levels + 1), ptemporary_group->flow_meter[ lnum_levels ].kvalue_100000u, (!(ppoc_created) && lthis_bypass_level_is_in_use), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
								   #ifdef _MSC_VER
								   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
								   #endif
								 );

				nm_POC_set_offset( lpoc, (lnum_levels + 1), ptemporary_group->flow_meter[ lnum_levels ].offset_100000u, (!(ppoc_created) && lthis_bypass_level_is_in_use), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
								   #ifdef _MSC_VER
								   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
								   #endif
								 );

				nm_POC_set_reed_switch( lpoc, (lnum_levels + 1), ptemporary_group->flow_meter[ lnum_levels ].reed_switch, (!(ppoc_created) && lthis_bypass_level_is_in_use), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
										#ifdef _MSC_VER
										, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
										#endif
									  );
			}
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, POC_CHANGE_BITFIELD_bypass_number_of_levels ) == (true)) )
		{
			nm_POC_set_bypass_number_of_levels( lpoc, ptemporary_group->bypass_number_of_levels, !(ppoc_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												#ifdef _MSC_VER
												, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												#endif
											   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, POC_CHANGE_BITFIELD_GID_irri_system_bit ) == (true)) )
		{
			nm_POC_set_GID_irrigation_system( lpoc, ptemporary_group->GID_irrigation_system, !(ppoc_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
											  #endif
											  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, POC_CHANGE_BITFIELD_budget_gallons_entry_option_bit ) == (true)) )
		{
			nm_POC_set_budget_gallons_entry_option(	lpoc, ptemporary_group->budget_gallons_entry_option, !(ppoc_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
													#ifdef _MSC_VER
													, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
													#endif
												  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, POC_CHANGE_BITFIELD_budget_calc_percent_of_et_bit ) == (true)) )
		{
			nm_POC_set_budget_calculation_percent_of_et(	lpoc, ptemporary_group->budget_calculation_percent_of_et, !(ppoc_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
															#ifdef _MSC_VER
															, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
															#endif
													   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, POC_CHANGE_BITFIELD_budget_gallons_bit ) == (true)) )
		{
			for( i=0; i<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS; i++ )
			{
				nm_POC_set_budget_gallons(	lpoc, i, ptemporary_group->budget_gallons[ i ], !(ppoc_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											#ifdef _MSC_VER
											, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
											#endif
										 );
			}
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, POC_CHANGE_BITFIELD_has_pump_attached_bit ) == (true)) )
		{
			nm_POC_set_has_pump_attached(	lpoc, ptemporary_group->has_pump_attached, !(ppoc_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												#ifdef _MSC_VER
												, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												#endif
											   );
		}

		#ifndef _MSC_VER

			if( lsync_with_poc_preserves )
			{
				// 5/7/2014 ajv : Since we've changed the phyiscally available
				// attribute, force a sync with the preserves to prevent the
				// "IRRI_FLOW: poc not found" alert.
				POC_PRESERVES_synchronize_preserves_to_file();
			}

		#endif
	}
	#ifndef _MSC_VER
	else
	{
		Alert_group_not_found();
	}
	#endif
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex Calling function must have the list_poc_recursive_MUTEX mutex to
 *  	  ensure no list items are added or removed during this process.
 *
 * @executed Executed within the context of the CommMngr task as a token or
 *           token response is being built.
 * 
 * @param pucp A pointer to an unsigned character pointer which contains the
 *             information that was received.
 * @param preason_for_change The reason for the change, such as keypad.
 * 
 * @return UNS_32 The total size of the extracted data. This is used to
 *                increment the ucp in the calling function.
 *
 * @author 4/5/2012 Adrianusv
 *
 * @revisions
 *    4/5/2012 Initial release
 */
extern UNS_32 nm_POC_extract_and_store_changes_from_comm( const unsigned char *pucp,
														  const UNS_32 preason_for_change,
														  const BOOL_32 pset_change_bits,
														  const UNS_32 pchanges_received_from
														  #ifdef _MSC_VER
														  , char *pSQL_statements,
														  char *pSQL_search_condition_str,
														  char *pSQL_update_str,
														  char *pSQL_insert_fields_str,
														  char *pSQL_insert_values_str,
														  char *pSQL_single_merge_statement_buf,
														  const UNS_32 pnetwork_ID
														  #endif
														)
{
	UNS_32	lsize_of_bitfield;

	CHANGE_BITS_32_BIT	lbitfield_of_changes;

	POC_GROUP_STRUCT 	*ltemporary_poc;


	#ifndef _MSC_VER

		POC_GROUP_STRUCT 	*lmatching_poc;

	#endif


	UNS_32	lnum_changed_pocs;

	UNS_32	lbox_index;

	UNS_32	lpoc_type;

	UNS_32	ldecoder_serial_numbers[ POC_BYPASS_LEVELS_MAX ];

	BOOL_32	lpoc_created;

	UNS_32	lpoc_gid;

	UNS_32	i;

	UNS_32	rv;

	// ----------
	
	// 7/10/2014 rmd : This function is used when parsing either a token or token resp. When a
	// poc is created or modified. This function can be thought about as executed at either the
	// FOAL side or IRRI side ie at masters or slaves. It happens at both.
	
	// ----------
	
	rv = 0;

	lpoc_created = (false);
	
	// ----------

	// Extract the number of changed groups
	memcpy( &lnum_changed_pocs, pucp, sizeof(lnum_changed_pocs) );
	pucp += sizeof( lnum_changed_pocs );
	rv += sizeof( lnum_changed_pocs );

	// DEBUG ONLY
	// Alert_Message_va("extract #changed pocs=%d", lnum_changed_pocs );
	
	/*
	// 8/8/2016 rmd : For debug only.
	if( pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE )
	{
		Alert_Message_va( "rcvd from slave: POC CHANGES for %u groups", lnum_changed_pocs );
	}
	else
	if( pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER )
	{
		Alert_Message_va( "rcvd from master: POC CHANGES for %u groups", lnum_changed_pocs );
	}
	else
	{
		Alert_Message_va( "rcvd from UNK: POC CHANGES for %u groups", lnum_changed_pocs );
	}
	*/
	
	// ----------
	
	for( i = 0; i < lnum_changed_pocs; ++i )
	{
		memcpy( &lpoc_gid, pucp, sizeof(lpoc_gid) );
		pucp += sizeof( lpoc_gid );
		rv += sizeof( lpoc_gid );

		// 11/19/2014 ajv : Grab the box index, POC type, and decoder serial number array so we can
		// insert the new POC (if a new one is being created) into the appropriate location in the
		// list to keep it sorted.
		memcpy( &lbox_index, pucp, sizeof(lbox_index) );
		pucp += sizeof( lbox_index );
		rv += sizeof( lbox_index );

		memcpy( &lpoc_type, pucp, sizeof(lpoc_type) );
		pucp += sizeof( lpoc_type );
		rv += sizeof( lpoc_type );

		memcpy( &ldecoder_serial_numbers, pucp, sizeof(ldecoder_serial_numbers) );
		pucp += sizeof( ldecoder_serial_numbers );
		rv += sizeof( ldecoder_serial_numbers );

		memcpy( &lsize_of_bitfield, pucp, sizeof(lsize_of_bitfield) );
		pucp += sizeof( lsize_of_bitfield );
		rv += sizeof( lsize_of_bitfield );

		memcpy( &lbitfield_of_changes, pucp, sizeof(lbitfield_of_changes) );
		pucp += sizeof( lbitfield_of_changes );
		rv += sizeof( lbitfield_of_changes );

		// ----------

		// 1/8/2015 ajv : When compiling for the CommServer, we build a MERGE statement if any of
		// the station's settings have changed. Therefore, we can completely skip over searching for
		// the station - this will fail anyway since the list doesn't exist in the comm server.
		#ifndef _MSC_VER

			// 6/1/2016 rmd : Find an existing group. I suppose it doesn't have to be physically
			// available. Take for example the case where we are resurecting a decoder based POC after a
			// 2W wiring problem where we did a scan and the decoder wasn't seen. In this case the POC
			// associated with that decoder was marked physically not available.
			//
			// 6/2/2016 rmd : We only provide the SINGLE DECODER serial number. That is because when
			// finding a POC the only case where a decoder serial number is involved is for the
			// DECODER_SINGLE case. Both TERMINAL and BYPASS are identified by using only the poc_type
			// and box_index.
			//
			// 6/8/2016 rmd : We must allow the search for matching POC's to include those set to not
			// SHOW. This is critical to the POC RESURRECTION scheme.
			//
			// 8/5/2016 rmd : A POC MUST NEVER BE REFERRED TO BY ITS GID. THE POC GID IS NOT GUARANTEED
			// TO MATCH ACROSS CONTROLLERS ON A CHAIN. THE POC IS LOCATED USING THE BOX_INDEX, TYPE, AND
			// DECODER_SERIAL_NUMBER.
			lmatching_poc = nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn( lbox_index, lpoc_type, ldecoder_serial_numbers[0], POC_SET_TO_SHOW_OR_NOT );

			// 6/2/2016 rmd : If not found CREATE it.
			if( lmatching_poc == NULL )
			{
				lmatching_poc = nm_POC_create_new_group( lbox_index, lpoc_type, ldecoder_serial_numbers[ 0 ] );

				lpoc_created = (true);

				// ----------
				
				// 5/7/2014 ajv : Since we've added a new POC to the file force a
				// sync with the preserves to prevent the "TP_COMM: poc not found"
				// alert.
				POC_PRESERVES_request_a_resync();

				// ----------

				if( pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER )
				{
					// 5/7/2014 ajv : If we don't want to set the change bits that
					// means we called this function via a TOKEN receipt. And any
					// POC creation would be for POCs not at this box. And therefore
					// we don't have to resend out these changes. The distribution
					// of this activity creating POCs and assigning the values STOPS
					// here in this case.
					SHARED_clear_all_32_bit_change_bits( &lmatching_poc->changes_to_send_to_master, list_poc_recursive_MUTEX );

					//Alert_Message_va( "PDATA: POC %u (idx: %d, typ: %d) created cleared", lpoc_id, lbox_index, lpoc_type );
				}
				else
				{
					//Alert_Message_va( "PDATA: POC %u (idx: %d, typ: %d) created", lpoc_id, lbox_index, lpoc_type );
				}
			}
			else
			{
				//Alert_Message_va( "PDATA: POC %u (idx: %d, typ: %d) found 0x%08x", lpoc_id, lbox_index, lpoc_type, lbitfield_of_changes );
			}

		#endif

		// ----------

		#ifndef _MSC_VER

		if( lmatching_poc != NULL )

		#endif
		{
			// Allocate memory for, and initialize, the temporary group
			#ifndef _MSC_VER
				ltemporary_poc = mem_malloc( sizeof(POC_GROUP_STRUCT) );
			#else
				ltemporary_poc = malloc( sizeof(POC_GROUP_STRUCT) );
			#endif

			memset( ltemporary_poc, 0x00, sizeof(POC_GROUP_STRUCT) );

			#ifndef _MSC_VER

				// Make a copy of the group. We do this so we can change the settings
				// and pass it to the store_changes routine which will actually update
				// the group's settings.
				memcpy( ltemporary_poc, lmatching_poc, sizeof(POC_GROUP_STRUCT) );

			#else

				// 5/1/2015 ajv : Since we don't copy an existing POC into the temporary one when
				// parsing using the CommServer, make sure we set the identifing variables manually
				// based on what came in at the beginning of the memory block.
				ltemporary_poc->base.group_identity_number = lpoc_gid;
				ltemporary_poc->poc_type__file_type = lpoc_type;
				ltemporary_poc->box_index_0 = lbox_index;
				memcpy( &ltemporary_poc->decoder_serial_number, &ldecoder_serial_numbers, sizeof(ltemporary_poc->decoder_serial_number) );

			#endif

			if( B_IS_SET( lbitfield_of_changes, POC_CHANGE_BITFIELD_description_bit ) == (true) )
			{
				memcpy( &ltemporary_poc->base.description, pucp, NUMBER_OF_CHARS_IN_A_NAME );
				pucp += NUMBER_OF_CHARS_IN_A_NAME;
				rv += NUMBER_OF_CHARS_IN_A_NAME;
			}

			if( B_IS_SET( lbitfield_of_changes, POC_CHANGE_BITFIELD_GID_irri_system_bit ) == (true) )
			{
				memcpy( &ltemporary_poc->GID_irrigation_system, pucp, sizeof(ltemporary_poc->GID_irrigation_system) );
				pucp += sizeof( ltemporary_poc->GID_irrigation_system );
				rv += sizeof( ltemporary_poc->GID_irrigation_system );
			}

			if( B_IS_SET( lbitfield_of_changes, POC_CHANGE_BITFIELD_box_index_bit ) == (true) )
			{
				memcpy( &ltemporary_poc->box_index_0, pucp, sizeof(ltemporary_poc->box_index_0) );
				pucp += sizeof( ltemporary_poc->box_index_0 );
				rv += sizeof( ltemporary_poc->box_index_0 );
			}

			if( B_IS_SET( lbitfield_of_changes, POC_CHANGE_BITFIELD_type_of_poc_bit ) == (true) )
			{
				memcpy( &ltemporary_poc->poc_type__file_type, pucp, sizeof(ltemporary_poc->poc_type__file_type) );
				pucp += sizeof( ltemporary_poc->poc_type__file_type );
				rv += sizeof( ltemporary_poc->poc_type__file_type );
			}

			if( B_IS_SET( lbitfield_of_changes, POC_CHANGE_BITFIELD_decoder_serial_number_bit ) == (true) )
			{
				memcpy( &ltemporary_poc->decoder_serial_number, pucp, sizeof(ltemporary_poc->decoder_serial_number) );
				pucp += sizeof( ltemporary_poc->decoder_serial_number );
				rv += sizeof( ltemporary_poc->decoder_serial_number );
			}

			if( B_IS_SET( lbitfield_of_changes, POC_CHANGE_BITFIELD_poc_physically_available_bit ) == (true) )
			{
				memcpy( &ltemporary_poc->show_this_poc, pucp, sizeof(ltemporary_poc->show_this_poc) );
				pucp += sizeof( ltemporary_poc->show_this_poc );
				rv += sizeof( ltemporary_poc->show_this_poc );

				//Alert_Message_va( "PDATA: POC %u phy avail set %u", lpoc_id, ltemporary_poc->poc_physically_available ); 
			}

			if( B_IS_SET( lbitfield_of_changes, POC_CHANGE_BITFIELD_poc_usage_bit ) == (true) )
			{
				memcpy( &ltemporary_poc->poc_usage, pucp, sizeof(ltemporary_poc->poc_usage) );
				pucp += sizeof( ltemporary_poc->poc_usage );
				rv += sizeof( ltemporary_poc->poc_usage );
			}

			if( B_IS_SET( lbitfield_of_changes, POC_CHANGE_BITFIELD_master_valve_type_bit ) == (true) )
			{
				memcpy( &ltemporary_poc->master_valve_type, pucp, sizeof(ltemporary_poc->master_valve_type) );
				pucp += sizeof( ltemporary_poc->master_valve_type );
				rv += sizeof( ltemporary_poc->master_valve_type );
			}

			if( B_IS_SET( lbitfield_of_changes, POC_CHANGE_BITFIELD_flow_meter_type_bit ) == (true) )
			{
				memcpy( &ltemporary_poc->flow_meter, pucp, sizeof(ltemporary_poc->flow_meter) );
				pucp += sizeof( ltemporary_poc->flow_meter );
				rv += sizeof( ltemporary_poc->flow_meter );
			}

			if( B_IS_SET( lbitfield_of_changes, POC_CHANGE_BITFIELD_bypass_number_of_levels ) == (true) )
			{
				memcpy( &ltemporary_poc->bypass_number_of_levels, pucp, sizeof(ltemporary_poc->bypass_number_of_levels) );
				pucp += sizeof( ltemporary_poc->bypass_number_of_levels );
				rv += sizeof( ltemporary_poc->bypass_number_of_levels );
			}

			if( B_IS_SET( lbitfield_of_changes, POC_CHANGE_BITFIELD_budget_gallons_entry_option_bit ) == (true) )
			{
				memcpy( &ltemporary_poc->budget_gallons_entry_option, pucp, sizeof(ltemporary_poc->budget_gallons_entry_option) );
				pucp += sizeof( ltemporary_poc->budget_gallons_entry_option );
				rv += sizeof( ltemporary_poc->budget_gallons_entry_option );
			}

			if( B_IS_SET( lbitfield_of_changes, POC_CHANGE_BITFIELD_budget_calc_percent_of_et_bit ) == (true) )
			{
				memcpy( &ltemporary_poc->budget_calculation_percent_of_et, pucp, sizeof(ltemporary_poc->budget_calculation_percent_of_et) );
				pucp += sizeof( ltemporary_poc->budget_calculation_percent_of_et );
				rv += sizeof( ltemporary_poc->budget_calculation_percent_of_et );
			}

			if( B_IS_SET( lbitfield_of_changes, POC_CHANGE_BITFIELD_budget_gallons_bit ) == (true) )
			{
				memcpy( &ltemporary_poc->budget_gallons, pucp, sizeof(ltemporary_poc->budget_gallons) );
				pucp += sizeof( ltemporary_poc->budget_gallons );
				rv += sizeof( ltemporary_poc->budget_gallons );
			}

			if( B_IS_SET( lbitfield_of_changes, POC_CHANGE_BITFIELD_has_pump_attached_bit ) == (true) )
			{
				memcpy( &ltemporary_poc->has_pump_attached, pucp, sizeof(ltemporary_poc->has_pump_attached) );
				pucp += sizeof( ltemporary_poc->has_pump_attached ); 
				rv += sizeof( ltemporary_poc->has_pump_attached );
			}

			// ----------

			#ifdef _MSC_VER

				// 1/16/2015 ajv : Make sure the strings are fully initialized to NULL since we rely on this
				// to determine whether the string is empty or not.
				memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

				// 4/11/2016 ajv : Insert the network ID and GID in case the POC doesn't exist in the
				// database yet.
				SQL_MERGE_build_upsert_uint32( pnetwork_ID, SQL_MERGE_include_field_name_in_insert_clause, "NetworkID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
				SQL_MERGE_build_upsert_uint32( ltemporary_poc->base.group_identity_number, SQL_MERGE_include_field_name_in_insert_clause, "POC_GID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

			#endif
			// ----------

			nm_POC_store_changes( ltemporary_poc, lpoc_created, preason_for_change, 0, pset_change_bits, pchanges_received_from, lbitfield_of_changes
								  #ifdef _MSC_VER
								  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
								  #endif
								);
			// ----------

			#ifndef _MSC_VER

				mem_free( ltemporary_poc );

			#else

				SQL_MERGE_build_search_condition_uint32( "NetworkID", pnetwork_ID, pSQL_search_condition_str );
				SQL_MERGE_build_search_condition_uint32( "BoxIndex", ltemporary_poc->box_index_0, pSQL_search_condition_str );
				SQL_MERGE_build_search_condition_uint32( "POC_GID", ltemporary_poc->base.group_identity_number, pSQL_search_condition_str );

				SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements( DATABASE_TABLE_NAME_POCS, pSQL_search_condition_str, SQL_MERGE_include_field_name_in_insert_clause, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf );

				// ----------

				free( ltemporary_poc );

			#endif
		}
		#ifndef _MSC_VER
		else
		{
			Alert_group_not_found();
		}
		#endif
	}
     
	// DEBUG ONLY
	//Alert_Message_va(" store count=%d", poc_group_list_hdr.count);

	#ifndef _MSC_VER

		// Only save the file if we've extracted information out of the ucp. This is
		// a sanity check to ensure the bit is only set if data was included.
		if( rv > 0 )
		{
			// 5/13/2015 ajv : We want to save the file in the following cases:
			// 
			//   - A message from the CommServer since we've responded that we received it and need to
			//     ensure it's saved.
			//   - A token response from the slave to the master
			//   - A token from the master to the slave
			//   
			// We DO NOT want to save the file when a token is received by the master since we've
			// already saved the file for one of the reasons above. This will otherwise result in a
			// double save for each change - once when it comes in and once when it's received when the
			// master distributes the changes back out.
			if( (pchanges_received_from == CHANGE_REASON_CENTRAL_OR_MOBILE) ||
				(pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE) ||
				((pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER) && (!FLOWSENSE_we_are_a_master_one_way_or_another())) )
			{
				// 12/10/2014 ajv : Schedule a save rather than saving immediately to prevent stacking of
				// saves. Additionally, set a relatively short time to hel cover cases where users make a
				// lot of changes in a short period.
				FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_POC, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
			}
		}
		else
		{
			Alert_bit_set_with_no_data();
		}

	#endif

	return( rv );
}

#ifdef _MSC_VER

/* ---------------------------------------------------------- */
extern INT_32 POC_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order )
{
	POC_GROUP_STRUCT	*lpoc;

	INT_32	rv;

	// ----------

	// 4/15/2016 ajv : In most cases, we're going to pass back an array_index of 0. However, in
	// the event the value in question is part of an array, we're going to pass back the actual
	// index so that the CommServer can sort not just on the bit number, but also the
	// array_index for values that share the same bit number.
	*psecondary_sort_order = 0;

	rv = -1;

	// ----------

	if( strncmp( pfield_name, POC_database_field_names[ POC_CHANGE_BITFIELD_description_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = POC_CHANGE_BITFIELD_description_bit;
		*psize_of_var_ptr = sizeof(lpoc->base.description);
	}
	else if( strncmp( pfield_name, POC_database_field_names[ POC_CHANGE_BITFIELD_GID_irri_system_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = POC_CHANGE_BITFIELD_GID_irri_system_bit;
		*psize_of_var_ptr = sizeof(lpoc->GID_irrigation_system);
	}
	else if( strncmp( pfield_name, POC_database_field_names[ POC_CHANGE_BITFIELD_box_index_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = POC_CHANGE_BITFIELD_box_index_bit;
		*psize_of_var_ptr = sizeof(lpoc->box_index_0);
	}
	else if( strncmp( pfield_name, POC_database_field_names[ POC_CHANGE_BITFIELD_type_of_poc_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = POC_CHANGE_BITFIELD_type_of_poc_bit;
		*psize_of_var_ptr = sizeof(lpoc->poc_type__file_type);
	}
	else if( strncmp( pfield_name, POC_database_field_names[ POC_CHANGE_BITFIELD_decoder_serial_number_bit ], 19 ) == 0 )
	{
		// 1/13/2015 ajv : Note the above condition is not checking the entire length of the field
		// name. This is because there are 3 unique decoder serial number values:
		// DecoderSerialNumber1 - DecoderSerialNumber3. By only comparing the first 19 characters,
		// we can ensure we catch all 3 in one condition.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "3" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}

		rv = POC_CHANGE_BITFIELD_decoder_serial_number_bit;
		*psize_of_var_ptr = sizeof(lpoc->decoder_serial_number[ 0 ]);
	}
	else if( strncmp( pfield_name, POC_database_field_names[ POC_CHANGE_BITFIELD_poc_physically_available_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = POC_CHANGE_BITFIELD_poc_physically_available_bit;
		*psize_of_var_ptr = sizeof(lpoc->show_this_poc);
	}
	else if( strncmp( pfield_name, POC_database_field_names[ POC_CHANGE_BITFIELD_poc_usage_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = POC_CHANGE_BITFIELD_poc_usage_bit;
		*psize_of_var_ptr = sizeof(lpoc->poc_usage);
	}
	else if( strncmp( pfield_name, POC_database_field_names[ POC_CHANGE_BITFIELD_master_valve_type_bit ], 15 ) == 0 )
	{
		// 1/13/2015 ajv : Note the above condition is not checking the entire length of the field
		// name. This is because there are 3 unique master valve values: MasterValveType1 -
		// MasterValveType3. By only comparing the first 15 characters, we can ensure we catch
		// all 3 in one condition.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "3" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}

		rv = POC_CHANGE_BITFIELD_master_valve_type_bit;
		*psize_of_var_ptr = sizeof(lpoc->master_valve_type[ 0 ]);
	}
	else if( strncmp( pfield_name, POC_database_field_names[ POC_CHANGE_BITFIELD_flow_meter_type_bit ], 9 ) == 0 )
	{
		// 4/15/2016 ajv : Note the above condition is not checking the entire length of the field
		// name. This is because there are a number of unique flow meter values: FlowMeterChoice1 ..
		// 3, FlowMeterK_Value1 .. 3, FlowMeterOffset_Value1 .. 3, and FlowMeterReedSwitch1 .. 3. By
		// only comparing the first 12 characters, we can ensure we catch all 9 in one condition.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name. This is a bit unique from other locations because it's not just an
		// array, but also a structure which contains 4 unique variables. Therefore, we're going to
		// fake the indexes a bit to ensure the values are sorted properly.
		if( strstr( pfield_name, "Choice1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "K_Value1" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "Offset_Value1" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "ReedSwitch1" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}
		if( strstr( pfield_name, "Choice2" ) != NULL )
		{
			*psecondary_sort_order = 4;
		}
		else if( strstr( pfield_name, "K_Value2" ) != NULL )
		{
			*psecondary_sort_order = 5;
		}
		else if( strstr( pfield_name, "Offset_Value2" ) != NULL )
		{
			*psecondary_sort_order = 6;
		}
		else if( strstr( pfield_name, "ReedSwitch2" ) != NULL )
		{
			*psecondary_sort_order = 7;
		}
		if( strstr( pfield_name, "Choice3" ) != NULL )
		{
			*psecondary_sort_order = 8;
		}
		else if( strstr( pfield_name, "K_Value3" ) != NULL )
		{
			*psecondary_sort_order = 9;
		}
		else if( strstr( pfield_name, "Offset_Value3" ) != NULL )
		{
			*psecondary_sort_order = 10;
		}
		else if( strstr( pfield_name, "ReedSwitch3" ) != NULL )
		{
			*psecondary_sort_order = 11;
		}

		rv = POC_CHANGE_BITFIELD_flow_meter_type_bit;

		// 4/15/2016 ajv : Don't use *psecondary_sort_order as the index into the array because the array
		// index must be 0-2, but the sort order is 0-11.
		*psize_of_var_ptr = sizeof(lpoc->flow_meter[0].flow_meter_choice);
	}
	else if( strncmp( pfield_name, POC_database_field_names[ POC_CHANGE_BITFIELD_bypass_number_of_levels ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = POC_CHANGE_BITFIELD_bypass_number_of_levels;
		*psize_of_var_ptr = sizeof(lpoc->bypass_number_of_levels);
	}
	else if( strncmp( pfield_name, POC_database_field_names[ POC_CHANGE_BITFIELD_budget_gallons_entry_option_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = POC_CHANGE_BITFIELD_budget_gallons_entry_option_bit;
		*psize_of_var_ptr = sizeof(lpoc->budget_gallons_entry_option);
	}
	else if( strncmp( pfield_name, POC_database_field_names[ POC_CHANGE_BITFIELD_budget_calc_percent_of_et_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = POC_CHANGE_BITFIELD_budget_calc_percent_of_et_bit;
		*psize_of_var_ptr = sizeof(lpoc->budget_calculation_percent_of_et);
	}
	else if( strncmp( pfield_name, POC_database_field_names[ POC_CHANGE_BITFIELD_budget_gallons_bit ], 13 ) == 0 )
	{
		// 4/21/2016 wjb : Note the above condition is not checking the entire length of the field
		// name. This is because there are a number of unique budget gallons: BudgetGallons1 -
		// BudgetGallons12. By only comparing the first 14 characters, we can 
		// ensure we catch all 12 in one condition.

		// 4/21/2016 wjb : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		
		if( strstr( pfield_name, "24" ) != NULL )
		{
			*psecondary_sort_order = 23;
		}
		else if( strstr( pfield_name, "23" ) != NULL )
		{
			*psecondary_sort_order = 22;
		}
		else if( strstr( pfield_name, "22" ) != NULL )
		{
			*psecondary_sort_order = 21;
		}
		else if( strstr( pfield_name, "21" ) != NULL )
		{
			*psecondary_sort_order = 20;
		}
		else if( strstr( pfield_name, "20" ) != NULL )
		{
			*psecondary_sort_order = 19;
		}
		else if( strstr( pfield_name, "19" ) != NULL )
		{
			*psecondary_sort_order = 18;
		}
		else if( strstr( pfield_name, "18" ) != NULL )
		{
			*psecondary_sort_order = 17;
		}
		else if( strstr( pfield_name, "17" ) != NULL )
		{
			*psecondary_sort_order = 16;
		}
		else if( strstr( pfield_name, "16" ) != NULL )
		{
			*psecondary_sort_order = 15;
		}
		else if( strstr( pfield_name, "15" ) != NULL )
		{
			*psecondary_sort_order = 14;
		}
		else if (strstr(pfield_name, "14") != NULL)
		{
			*psecondary_sort_order = 13;
		}
		else if (strstr(pfield_name, "13") != NULL)
		{
			*psecondary_sort_order = 12;
		}
		else if (strstr(pfield_name, "12") != NULL)
		{
			*psecondary_sort_order = 11;
		}
		else if (strstr(pfield_name, "11") != NULL)
		{
			*psecondary_sort_order = 10;
		}
		else if (strstr(pfield_name, "10") != NULL)
		{
			*psecondary_sort_order = 9;
		}
		else if (strstr(pfield_name, "9") != NULL)
		{
			*psecondary_sort_order = 8;
		}
		else if (strstr(pfield_name, "8") != NULL)
		{
			*psecondary_sort_order = 7;
		}
		else if (strstr(pfield_name, "7") != NULL)
		{
			*psecondary_sort_order = 6;
		}
		else if (strstr(pfield_name, "6") != NULL)
		{
			*psecondary_sort_order = 5;
		}
		else if (strstr(pfield_name, "5") != NULL)
		{
			*psecondary_sort_order = 4;
		}
		else if (strstr(pfield_name, "4") != NULL)
		{
			*psecondary_sort_order = 3;
		}
		else if (strstr(pfield_name, "3") != NULL)
		{
			*psecondary_sort_order = 2;
		}
		else if (strstr(pfield_name, "2") != NULL)
		{
			*psecondary_sort_order = 1;
		}
		else if (strstr(pfield_name, "1") != NULL)
		{
			*psecondary_sort_order = 0;
		}

		rv = POC_CHANGE_BITFIELD_budget_gallons_bit;
		*psize_of_var_ptr = sizeof(lpoc->budget_gallons[0]);
	}
	else if( strncmp( pfield_name, POC_database_field_names[ POC_CHANGE_BITFIELD_has_pump_attached_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = POC_CHANGE_BITFIELD_has_pump_attached_bit;
		*psize_of_var_ptr = sizeof(lpoc->has_pump_attached);
	}

	return( rv );
}

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

