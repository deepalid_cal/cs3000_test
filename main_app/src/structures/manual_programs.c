/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"manual_programs.h"

#include	"app_startup.h"

#include	"bithacks.h"

#include	"change.h"

#include	"comm_mngr.h"

#include	"configuration_controller.h"

#include	"cs_mem.h"

#include	"e_station_selection_grid.h"

#include	"epson_rx_8025sa.h"

#include	"flash_storage.h"

#include	"flowsense.h"

#include	"group_base_file.h"

#include	"station_groups.h"

#include	"pdata_changes.h"

#include	"range_check.h"

#include	"r_alerts.h"

#include	"shared.h"

#include	"stations.h"

#include	"controller_initiated.h"

#include	"chain_sync_vars.h"

#include	"chain_sync_funcs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static MIST_LIST_HDR_TYPE	manual_programs_group_list_hdr;

// ----------

// 2/18/2016 rmd : Must include the shared c source after the
// manual_programs_group_list_header declaration.
#include	"shared_manual_programs.c"

// ----------

static const char MANUAL_PROGRAMS_FILENAME[] =	"MANUAL_PROGRAMS";


const char MANUAL_PROGRAMS_DEFAULT_NAME[] =		"Manual Program";

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	UNS_32							start_times[ MANUAL_PROGRAMS_NUMBER_OF_START_TIMES ];

	BOOL_32							days[ DAYS_IN_A_WEEK ];
	
	UNS_32							start_date;

	UNS_32							stop_date;

	UNS_32							run_time_seconds;

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

} MANUAL_PROGRAMS_GROUP_STRUCT_REV_1;

// ----------

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	UNS_32							start_times[ MANUAL_PROGRAMS_NUMBER_OF_START_TIMES ];

	BOOL_32							days[ DAYS_IN_A_WEEK ];
	
	UNS_32							start_date;

	UNS_32							stop_date;

	UNS_32							run_time_seconds;

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;
	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

} MANUAL_PROGRAMS_GROUP_STRUCT_REV_2;

// ----------

// 1/26/2015 rmd : NOTE - upon REVISION update the record can only grow in size. At least
// that is the way I've coded the flash file supporting functions. It may be possible to
// decrease the record size. I just haven't gone through the code with that in mind.

#define	MANUAL_PROGRAMS_LATEST_FILE_REVISION		(3)

// ----------

const UNS_32 manual_programs_list_item_sizes[ MANUAL_PROGRAMS_LATEST_FILE_REVISION + 1 ] =
{
	// 1/26/2015 rmd : In order for this array to contain OLDER list item sizes the implication
	// is that the older structure definitions are to be retained. That means the newer
	// structure definition has a different name.

	// ----------
	
	sizeof( MANUAL_PROGRAMS_GROUP_STRUCT_REV_1 ),

	// 2/4/2015 rmd : Revision 1 is the same size. Needed a variable set. See updater below.
	sizeof( MANUAL_PROGRAMS_GROUP_STRUCT_REV_1 ),

	// 9/22/2015 ajv : Revision 2 added the changes_uploaded_to_comm_server_awaiting_ACK field
	sizeof( MANUAL_PROGRAMS_GROUP_STRUCT_REV_2 ),

	// 9/22/2015 ajv : Rev 3 adds the "In Use" flag which is used to indicate whether a manual
	// program has been deleted.
	sizeof( MANUAL_PROGRAMS_GROUP_STRUCT )
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

UNS_32 g_MANUAL_PROGRAMS_start_date;

UNS_32 g_MANUAL_PROGRAMS_stop_date;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void nm_MANUAL_PROGRAMS_set_default_values( void *const pgroup, const BOOL_32 pset_change_bits );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void nm_manual_programs_updater( UNS_32 pfrom_revision )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only when
	// the file revision is changed. The caller is to be holding the list_program_data
	// recursive_MUTEX.

	// ----------
	
	MANUAL_PROGRAMS_GROUP_STRUCT	*mpgs;
	
	CHANGE_BITS_32_BIT	*lbitfield_of_changes;

	UNS_32	box_index_0;

	// ----------
	
	// 9/21/2015 rmd : Because of the order of the init_file function calls in the app_startup
	// task we know the controller index is valid for use at this [early] point during the boot
	// process that this updater function is called.
	box_index_0 = FLOWSENSE_get_controller_index();

	// ----------

	if( pfrom_revision == MANUAL_PROGRAMS_LATEST_FILE_REVISION )
	{
		Alert_Message_va( "MAN PROGS file unexpd update %u", pfrom_revision );	
	}
	else
	{
		Alert_Message_va( "MAN PROGS file update : to revision %u from %u", MANUAL_PROGRAMS_LATEST_FILE_REVISION, pfrom_revision );

		// ----------
		
		if( pfrom_revision == 0 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 0 to REVISION 1. Because
			// of revision 0 bugs the upload to comm_server bits are in an unknown state. They need to
			// be set to trigger a complete send of PDATA to the comm_server.

			mpgs = nm_ListGetFirst( &manual_programs_group_list_hdr );
		
			while( mpgs != NULL )
			{
				// 1/30/2015 rmd : Because of revision 0 bugs these bits were not set. We need to set them
				// to guarantee all the program data is sent to the comm_server this one time.
				mpgs->changes_to_upload_to_comm_server = UNS_32_MAX;
				
				// ----------
				
				mpgs = nm_ListGetNext( &manual_programs_group_list_hdr, mpgs );
			}
			
			// ----------
			
			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 1 to 2 changes if
			// needed.
			pfrom_revision += 1;
		}
		
		// ----------
		
		if( pfrom_revision == 1 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 1 to REVISION 2.
			mpgs = nm_ListGetFirst( &manual_programs_group_list_hdr );

			while( mpgs != NULL )
			{
				// 4/30/2015 ajv : Clear all of the pending Program Data bits.
				mpgs->changes_uploaded_to_comm_server_awaiting_ACK = CHANGE_BITS_32_BIT_ALL_CLEAR;

				mpgs = nm_ListGetNext( &manual_programs_group_list_hdr, mpgs );
			}


			pfrom_revision += 1;
		}

		// ----------

		if( pfrom_revision == 2 )
		{
			// 9/22/2015 ajv : This is the work to do when moving from REVISION 2 to REVISION 3.
			mpgs = nm_ListGetFirst( &manual_programs_group_list_hdr );

			while( mpgs != NULL )
			{
				lbitfield_of_changes = &mpgs->changes_to_send_to_master;

				nm_MANUAL_PROGRAMS_set_in_use( mpgs, MANUAL_PROGRAMS_IN_USE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				mpgs = nm_ListGetNext( &manual_programs_group_list_hdr, mpgs );
			}


			pfrom_revision += 1;
		}
	}

	// ----------

	// 1/29/2015 rmd : Well after the updater has run the pfrom_revision is supposed to equal
	// the latest revision. By design of this update process.
	if( pfrom_revision != MANUAL_PROGRAMS_LATEST_FILE_REVISION )
	{
		Alert_Message( "MAN PROGS updater error" );
	}
}

/* ---------------------------------------------------------- */
extern void init_file_manual_programs( void )
{
	FLASH_FILE_initialize_list_and_find_or_create_group_list_file(	FLASH_INDEX_1_GENERAL_STORAGE,
																	MANUAL_PROGRAMS_FILENAME,
																	MANUAL_PROGRAMS_LATEST_FILE_REVISION,
																	&manual_programs_group_list_hdr,
																	manual_programs_list_item_sizes,
																	list_program_data_recursive_MUTEX,
																	&nm_manual_programs_updater,
																	&nm_MANUAL_PROGRAMS_set_default_values,
																	(char*)MANUAL_PROGRAMS_DEFAULT_NAME,
																	FF_MANUAL_PROGRAMS );
}

/* ---------------------------------------------------------- */
extern void save_file_manual_programs( void )
{
	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE,
															MANUAL_PROGRAMS_FILENAME,
															MANUAL_PROGRAMS_LATEST_FILE_REVISION,
															&manual_programs_group_list_hdr,
															sizeof( MANUAL_PROGRAMS_GROUP_STRUCT ),
															list_program_data_recursive_MUTEX,
															FF_MANUAL_PROGRAMS );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void nm_MANUAL_PROGRAMS_set_default_values( void *const pgroup, const BOOL_32 pset_change_bits )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	DATE_TIME	ldt;

	UNS_32	i;

	// ------------------------

	UNS_32	box_index_0;

	box_index_0 = FLOWSENSE_get_controller_index();

	// ------------------------

	if( pgroup != NULL )
	{
		// 5/14/2014 ajv : Clear all of the change bits for POC since it's just
		// been created. We'll then explicitly set each bit based upon whether
		// pset_change_bits is true or not.
		SHARED_clear_all_32_bit_change_bits( &((MANUAL_PROGRAMS_GROUP_STRUCT *)pgroup)->changes_to_send_to_master, list_program_data_recursive_MUTEX );
		SHARED_clear_all_32_bit_change_bits( &((MANUAL_PROGRAMS_GROUP_STRUCT *)pgroup)->changes_to_distribute_to_slaves, list_program_data_recursive_MUTEX );
		SHARED_clear_all_32_bit_change_bits( &((MANUAL_PROGRAMS_GROUP_STRUCT *)pgroup)->changes_uploaded_to_comm_server_awaiting_ACK, list_program_data_recursive_MUTEX );

		// 1/20/2015 ajv : However, we should explicitly set ALL bits for the Comm Server to ensure
		// changes are uploaded on a clean start.
		SHARED_set_all_32_bit_change_bits( &((MANUAL_PROGRAMS_GROUP_STRUCT *)pgroup)->changes_to_upload_to_comm_server, list_program_data_recursive_MUTEX );

		// ----------

		// 5/2/2014 ajv : When creating a new group, always set the changes to
		// be sent to the master. Once the master receives the changes, it will
		// distribute them back out to the slaves.
		lchange_bitfield_to_set = &((MANUAL_PROGRAMS_GROUP_STRUCT *)pgroup)->changes_to_send_to_master;

		// ----------
		
		for( i = 0; i < MANUAL_PROGRAMS_NUMBER_OF_START_TIMES; ++i )
		{
			nm_MANUAL_PROGRAMS_set_start_time( pgroup, i, MANUAL_PROGRAMS_START_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
		}

		// 10/3/2012 rmd : And now by special request from Richard set the first 3 starts to 7AM,
		// 11AM, and 3PM. These are the contractor defaults! Commonly used new construction times.
		nm_MANUAL_PROGRAMS_set_start_time( pgroup, 0, MANUAL_PROGRAMS_START_TIME_1_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
		nm_MANUAL_PROGRAMS_set_start_time( pgroup, 1, MANUAL_PROGRAMS_START_TIME_2_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
		nm_MANUAL_PROGRAMS_set_start_time( pgroup, 2, MANUAL_PROGRAMS_START_TIME_3_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
		
		// ----------
		
		for( i = 0; i < DAYS_IN_A_WEEK; ++i )
		{
			// 10/3/2012 rmd : Set all the days OFF. Otherwise the thing would run as we have start
			// times and default 10 minutes of run time.
			nm_MANUAL_PROGRAMS_set_water_day( pgroup, i, MANUAL_PROGRAMS_WATERDAY_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
		}
		
		// ----------
		
		EPSON_obtain_latest_time_and_date( &ldt );

		nm_MANUAL_PROGRAMS_set_start_date( pgroup, ldt.D, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
		nm_MANUAL_PROGRAMS_set_end_date( pgroup, ldt.D, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		//rmd
		//stop_date = ldt.D + (3 * 365);  // set to roughly 3 years from now
		
		// ----------

		nm_MANUAL_PROGRAMS_set_run_time( pgroup, STATION_MANUAL_PROGRAM_SECONDS_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_MANUAL_PROGRAMS_set_in_use( pgroup, MANUAL_PROGRAMS_IN_USE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
	}
	else
	{
		Alert_func_call_with_null_ptr();
	}
}

/* ---------------------------------------------------------- */
extern void *nm_MANUAL_PROGRAMS_create_new_group( const UNS_32 pchanges_received_from )
{
	MANUAL_PROGRAMS_GROUP_STRUCT	*litem_to_insert_ahead_of;

	MANUAL_PROGRAMS_GROUP_STRUCT	*lgroup;

	// ----------

	litem_to_insert_ahead_of = nm_ListGetFirst( &manual_programs_group_list_hdr );

	// 2/11/2016 ajv : Loop through each manual program until we either find one that's deleted
	// or we reach the end of the list. If we find one that's been deleted, we're going to
	// insert the new item ahead of it.
	while( litem_to_insert_ahead_of != NULL )
	{
		if( !litem_to_insert_ahead_of->in_use )
		{
			// 2/11/2016 ajv : We found a deleted one so break out of the looop so the new item in
			// inserted ahead of it.
			break;
		}

		litem_to_insert_ahead_of = nm_ListGetNext( &manual_programs_group_list_hdr, litem_to_insert_ahead_of );
	}

	// ----------

	// 5/2/2014 ajv : TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// (requires pchanges_received_from)

	// 2/11/2016 ajv : Note that we're no longer passing a NULL for the pnext_list_item
	// parameter. The reason is because the list is now sorted with deleted items (aka not in
	// use) at the end of the list. We therefore need to insert a new item before the first
	// deleted item, if it exists.
	lgroup = nm_GROUP_create_new_group( &manual_programs_group_list_hdr, (char*)MANUAL_PROGRAMS_DEFAULT_NAME, &nm_MANUAL_PROGRAMS_set_default_values, sizeof(MANUAL_PROGRAMS_GROUP_STRUCT), (true), litem_to_insert_ahead_of );

	// ----------

	return( lgroup );
}

/* ---------------------------------------------------------- */
/**
 * This function runs through the POC list and copies the contents of any changed groups to
 * the passed in ucp.
 * 
 * @executed Executed within the context of the CommMngr task as a token, token response, or
 * Program Data message to the Comm Server is being built.
 * 
 * @param pucp A pointer to the pointer where the program data is being built.
 * 
 * @param pmem_used_so_far The amount of memory used so far. Once this reaches our limit for
 * a single token (MEMORY_TO_ALLOCATE_FOR_SENDING_CHANGES), processing of the remaining
 * changes stops until it's time to build the next token or token response.
 * 
 * @param pmem_used_so_far_is_less_than_allocated_memory A pointer to the flag which
 * indicates whether we're allowed to add more changes to the token or token response.
 * 
 * @param preason_data_is_being_built The reason why the data is being built to send. Based
 * upon this reason, we examine a different change bit structure to determine what to
 * send. Valid values are CHANGE_REASON_SYNC_SENDING_TO_MASTER,
 * CHANGE_REASON_SYNC_DISTRIBUTING_TO_SLAVES, CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER.
 * 
 * @return UNS_32 The total size of the added data. This is used to determine whether any
 * changes were detected so the appropriate file change bit can be set as well as to
 * determine how much memory must be allocated to store the changes.
 *
 * @author Adrianusv
 */
extern UNS_32 MANUAL_PROGRAMS_build_data_to_send( UNS_8 **pucp,
												  const UNS_32 pmem_used_so_far,
												  BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
												  const UNS_32 preason_data_is_being_built,
												  const UNS_32 pallocated_memory )
{
	CHANGE_BITS_32_BIT	*lbitfield_of_changes_to_use_to_determine_what_to_send;

	CHANGE_BITS_32_BIT	lbitfield_of_changes_to_send;

	MANUAL_PROGRAMS_GROUP_STRUCT		*lgroup;

	UNS_8	*llocation_of_num_changed_groups;

	UNS_8	*llocation_of_bitfield;

	// 1/14/2015 ajv : Track how many manual programs are added to the message.
	UNS_32	lnum_changed_groups;

	// Store the amount of memory used for current manual program. This is compared to the
	// program's overhead to determine whether any of the program's values were actually added
	// or not.
	UNS_32	lmem_used_by_this_group;

	// Determine how much overhead each manual program requires in case we add the overhead and
	// don't have enough room to add of the actual changes.
	UNS_32	lmem_overhead_per_group;

	UNS_32	rv;

	// ----------

	rv = 0;

	lnum_changed_groups = 0;

	// ----------

	// 1/14/2015 ajv : Take the appropriate mutex to ensure no manual programs are added or
	// removed during this process.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lgroup = nm_ListGetFirst( &manual_programs_group_list_hdr );

	// 1/14/2015 ajv : Loop through each manual program and count how many have change bits set.
	// This is really just used to determine whether there's at least one program which needs to
	// be examined.
	while( lgroup != NULL )
	{
		if( *(MANUAL_PROGRAMS_get_change_bits_ptr( lgroup, preason_data_is_being_built )) != CHANGE_BITS_32_BIT_ALL_CLEAR )
		{
			lnum_changed_groups++;

			// 1/14/2015 ajv : Since we don't actually care about the number of manual programs that
			// have changed at this point - just that at least one has changed - we can break out of the
			// loop now.
			break;
		}

		lgroup = nm_ListGetNext( &manual_programs_group_list_hdr, lgroup );
	}

	// ----------

	if( lnum_changed_groups > 0 )
	{
		// 1/14/2015 ajv : Initialize the changed manual programs back to 0. Since not all of the
		// progarms may be added due to token size limitations, we need to ensure that the receiving
		// party only processes the number of programs actually added to the message.
		lnum_changed_groups = 0;

		// ----------

		// 1/14/2015 ajv : Allocate space to store the number of changed manual programs that are
		// included in the message so the receiving party can correctly parse the data. This will be
		// filled in later once we have an actual count.
		rv = PDATA_allocate_space_for_num_changed_groups_in_pucp( pucp, &llocation_of_num_changed_groups, pmem_used_so_far, pmem_used_so_far_is_less_than_allocated_memory, pallocated_memory );

		// ----------

		// 2/9/2016 rmd : Well there was at least enough space for the 2 bytes for the count of
		// number of changed groups.
		if( *pmem_used_so_far_is_less_than_allocated_memory )
		{
			lgroup = nm_ListGetFirst( &manual_programs_group_list_hdr );

			while( lgroup != NULL )
			{
				lbitfield_of_changes_to_use_to_determine_what_to_send = MANUAL_PROGRAMS_get_change_bits_ptr( lgroup, preason_data_is_being_built );

				// 1/14/2015 ajv : If this manual program has changed...
				if( *lbitfield_of_changes_to_use_to_determine_what_to_send != CHANGE_BITS_32_BIT_ALL_CLEAR )
				{
					// 2/9/2016 rmd : The OVERHEAD is the GID, an UNS_32 indicating the size of the bitfield,
					// and the 32-bit change bitfield itself.
					lmem_overhead_per_group = (3 * sizeof(UNS_32));

					// 1/14/2015 ajv : If we still have room available in the message for the overhead, add it
					// now.
					if( *pmem_used_so_far_is_less_than_allocated_memory && ((pmem_used_so_far + rv + lmem_overhead_per_group) < pallocated_memory) )
					{
						PDATA_copy_var_into_pucp( pucp, &lgroup->base.group_identity_number, sizeof(lgroup->base.group_identity_number) );

						PDATA_copy_bitfield_info_into_pucp( pucp, sizeof(CHANGE_BITS_32_BIT), sizeof(UNS_32), &llocation_of_bitfield ); 
					}
					else
					{
						// 9/19/2013 ajv : Stop processing if we've already filled out the token with the maximum
						// amount of data to send.
						*pmem_used_so_far_is_less_than_allocated_memory = (false);

						// 2/11/2016 rmd : No overhead added so leave pucp alone.
						
						break;
					}

					lmem_used_by_this_group = lmem_overhead_per_group;

					// ----------

					// Initialize the bit field
					lbitfield_of_changes_to_send = 0x00;

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							MANUAL_PROGRAMS_CHANGE_BITFIELD_description_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->base.description,
							NUMBER_OF_CHARS_IN_A_NAME,
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							MANUAL_PROGRAMS_CHANGE_BITFIELD_start_times_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->start_times,
							sizeof(lgroup->start_times),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							MANUAL_PROGRAMS_CHANGE_BITFIELD_days_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->days,
							sizeof(lgroup->days),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							MANUAL_PROGRAMS_CHANGE_BITFIELD_start_date_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->start_date,
							sizeof(lgroup->start_date),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							MANUAL_PROGRAMS_CHANGE_BITFIELD_stop_date_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->stop_date,
							sizeof(lgroup->stop_date),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							MANUAL_PROGRAMS_CHANGE_BITFIELD_run_time_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->run_time_seconds,
							sizeof(lgroup->run_time_seconds),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_group += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							MANUAL_PROGRAMS_CHANGE_BITFIELD_in_use_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lgroup->changes_uploaded_to_comm_server_awaiting_ACK,
							&lgroup->in_use,
							sizeof(lgroup->in_use),
							(lmem_used_by_this_group + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					// ----------

					// 2/10/2016 rmd : If we saw any changed bits and ACTUALLY ADDED CONTENT BEYOND THE ORIGINAL
					// OVERHEAD then complete out the message by placing the bitfield in the overhead and
					// updating function housekeeping.
					if( lmem_used_by_this_group > lmem_overhead_per_group )
					{
						// 2/10/2016 rmd : Bump the count of changed list items in the message. Place the bitfield
						// of changes into the overhead. And update the return value.
						lnum_changed_groups += 1;
						
						memcpy( llocation_of_bitfield, &lbitfield_of_changes_to_send, sizeof(lbitfield_of_changes_to_send) );
						
						rv += lmem_used_by_this_group;
					}
					else
					{
						// 2/10/2016 rmd : Else back out from the message the overhead already added.
						*pucp -= lmem_overhead_per_group;
					}
					
				}

				lgroup = nm_ListGetNext( &manual_programs_group_list_hdr, lgroup );
			}

			// ----------

			// 1/14/2015 ajv : If we've added the number of changes but didn't actually add any data
			// because we hit our limit, back out the number of changes.
			if( lnum_changed_groups > 0 )
			{
				memcpy( llocation_of_num_changed_groups, &lnum_changed_groups, sizeof(lnum_changed_groups) );
			}
			else
			{
				// 2/10/2016 rmd : If we didn't add any content beyond moving the msg pointer over where the
				// number of changed groups goes, then put the pointer back and return a 0 to indicate
				// nothing added.
				*pucp -= sizeof( lnum_changed_groups );

				rv = 0;
			}
		}
		else
		{
			// 2/11/2016 rmd : We couldn't do anything, meaning couldn't even add in the
			// num_of_changed_sensors. In that case RV is still 0 and pucp hasn't moved so nothing to
			// do here.
		}
	}

	// ----------

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
extern void MANUAL_PROGRAMS_copy_group_into_guivars( const UNS_32 pgroup_index_0 )
{
	MANUAL_PROGRAMS_GROUP_STRUCT *lgroup;

	char str_48[ 48 ];

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup = MANUAL_PROGRAMS_get_group_at_this_index( pgroup_index_0 );

	nm_GROUP_load_common_guivars( lgroup );

	// Since easyGUI expects the time in minutes, divide the start times by 60.
	GuiVar_ManualPStartTime1 = (UNS_32)( lgroup->start_times[ 0 ] / 60 );
	GuiVar_ManualPStartTime2 = (UNS_32)( lgroup->start_times[ 1 ] / 60 );
	GuiVar_ManualPStartTime3 = (UNS_32)( lgroup->start_times[ 2 ] / 60 );
	GuiVar_ManualPStartTime4 = (UNS_32)( lgroup->start_times[ 3 ] / 60 );
	GuiVar_ManualPStartTime5 = (UNS_32)( lgroup->start_times[ 4 ] / 60 );
	GuiVar_ManualPStartTime6 = (UNS_32)( lgroup->start_times[ 5 ] / 60 );

	GuiVar_ManualPStartTime1Enabled = ( GuiVar_ManualPStartTime1 < (UNS_32)(SCHEDULE_OFF_TIME / 60) );
	GuiVar_ManualPStartTime2Enabled = ( GuiVar_ManualPStartTime2 < (UNS_32)(SCHEDULE_OFF_TIME / 60) );
	GuiVar_ManualPStartTime3Enabled = ( GuiVar_ManualPStartTime3 < (UNS_32)(SCHEDULE_OFF_TIME / 60) );
	GuiVar_ManualPStartTime4Enabled = ( GuiVar_ManualPStartTime4 < (UNS_32)(SCHEDULE_OFF_TIME / 60) );
	GuiVar_ManualPStartTime5Enabled = ( GuiVar_ManualPStartTime5 < (UNS_32)(SCHEDULE_OFF_TIME / 60) );
	GuiVar_ManualPStartTime6Enabled = ( GuiVar_ManualPStartTime6 < (UNS_32)(SCHEDULE_OFF_TIME / 60) );

	GuiVar_ManualPWaterDay_Sun = lgroup->days[ 0 ];
	GuiVar_ManualPWaterDay_Mon = lgroup->days[ 1 ];
	GuiVar_ManualPWaterDay_Tue = lgroup->days[ 2 ];
	GuiVar_ManualPWaterDay_Wed = lgroup->days[ 3 ];
	GuiVar_ManualPWaterDay_Thu = lgroup->days[ 4 ];
	GuiVar_ManualPWaterDay_Fri = lgroup->days[ 5 ];
	GuiVar_ManualPWaterDay_Sat = lgroup->days[ 6 ];

	g_MANUAL_PROGRAMS_start_date = lgroup->start_date;
	g_MANUAL_PROGRAMS_stop_date = lgroup->stop_date;

	strlcpy( GuiVar_ManualPStartDateStr, GetDateStr(str_48, sizeof(str_48), g_MANUAL_PROGRAMS_start_date, DATESTR_show_short_year, DATESTR_show_short_dow), sizeof(GuiVar_ManualPStartDateStr) );
	strlcpy( GuiVar_ManualPEndDateStr, GetDateStr(str_48, sizeof(str_48), g_MANUAL_PROGRAMS_stop_date, DATESTR_show_short_year, DATESTR_show_short_dow), sizeof(GuiVar_ManualPEndDateStr) );

	GuiVar_ManualPRunTime = ((float)lgroup->run_time_seconds / 60.0F);

	GuiVar_ManualPInUse = lgroup->in_use;

	STATION_SELECTION_GRID_populate_cursors( &STATION_get_GID_manual_program_A, &STATION_get_GID_manual_program_B, g_GROUP_ID, (true), (true), (true) );

	GuiVar_StationSelectionGridStationCount = STATION_SELECTION_GRID_get_count_of_selected_stations();

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Stores the group name. This routine is called when the user presses BACK to
 * close the on-screen keyboard so the list of groups is updated with the new
 * name.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user presses BACK to close the on-screen keyboard.
 *
 * @author 12/11/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void MANUAL_PROGRAMS_extract_and_store_group_name_from_GuiVars( void )
{
	MANUAL_PROGRAMS_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup = MANUAL_PROGRAMS_get_group_at_this_index( g_GROUP_list_item_index );

	if( lgroup != NULL )
	{
		SHARED_set_name_32_bit_change_bits( lgroup, GuiVar_GroupName, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, MANUAL_PROGRAMS_get_change_bits_ptr( lgroup, CHANGE_REASON_KEYPAD ), &lgroup->changes_to_upload_to_comm_server, MANUAL_PROGRAMS_CHANGE_BITFIELD_description_bit );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars( void )
{
	MANUAL_PROGRAMS_GROUP_STRUCT	*lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lgroup = mem_malloc( sizeof(MANUAL_PROGRAMS_GROUP_STRUCT) );

	// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
	// update just the variable that's potentially changed in the UI.
	memcpy( lgroup, MANUAL_PROGRAMS_get_group_with_this_GID( g_GROUP_ID ), sizeof(MANUAL_PROGRAMS_GROUP_STRUCT) );
	
	// ----------

	strlcpy( lgroup->base.description, GuiVar_GroupName, sizeof(lgroup->base.description) );

	// Since easyGUI adjusts the time in minutes, multiple the start times by 60
	// to get the time in seconds.
	lgroup->start_times[ 0 ] = (GuiVar_ManualPStartTime1 * 60);
	lgroup->start_times[ 1 ] = (GuiVar_ManualPStartTime2 * 60);
	lgroup->start_times[ 2 ] = (GuiVar_ManualPStartTime3 * 60);
	lgroup->start_times[ 3 ] = (GuiVar_ManualPStartTime4 * 60);
	lgroup->start_times[ 4 ] = (GuiVar_ManualPStartTime5 * 60);
	lgroup->start_times[ 5 ] = (GuiVar_ManualPStartTime6 * 60);

	lgroup->days[ 0 ] = GuiVar_ManualPWaterDay_Sun;
	lgroup->days[ 1 ] = GuiVar_ManualPWaterDay_Mon;
	lgroup->days[ 2 ] = GuiVar_ManualPWaterDay_Tue;
	lgroup->days[ 3 ] = GuiVar_ManualPWaterDay_Wed;
	lgroup->days[ 4 ] = GuiVar_ManualPWaterDay_Thu;
	lgroup->days[ 5 ] = GuiVar_ManualPWaterDay_Fri;
	lgroup->days[ 6 ] = GuiVar_ManualPWaterDay_Sat;

	lgroup->start_date = g_MANUAL_PROGRAMS_start_date;
	lgroup->stop_date = g_MANUAL_PROGRAMS_stop_date;

	lgroup->run_time_seconds = (UNS_32)(GuiVar_ManualPRunTime * 60.0F);

	lgroup->in_use = GuiVar_ManualPInUse;

	nm_MANUAL_PROGRAMS_store_changes( lgroup, g_GROUP_creating_new, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

	// ----------

	mem_free( lgroup );

	// ----------

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// Reset the Creating New flag to ensure we don't accidentally create
	// another new group when using the BACK key.
	g_GROUP_creating_new = (false);
}

/* ---------------------------------------------------------- */
extern void MANUAL_PROGRAMS_load_group_name_into_guivar( const INT_16 pindex_0_i16 )
{
	MANUAL_PROGRAMS_GROUP_STRUCT *lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( (UNS_32)pindex_0_i16 < MANUAL_PROGRAMS_get_num_groups_in_use() )
	{
		lgroup = MANUAL_PROGRAMS_get_group_at_this_index( (UNS_32)pindex_0_i16 );

		if( nm_OnList(&manual_programs_group_list_hdr, lgroup) == (true) )
		{
			strlcpy( GuiVar_itmGroupName, nm_GROUP_get_name( lgroup ), NUMBER_OF_CHARS_IN_A_NAME );
		}
		else
		{
			Alert_group_not_found();
		}
	}
	else if( (UNS_32)pindex_0_i16 == MANUAL_PROGRAMS_get_num_groups_in_use() )
	{
		snprintf( GuiVar_itmGroupName, NUMBER_OF_CHARS_IN_A_NAME, "%s", GuiLib_GetTextPtr( GuiStruct_txtGroupDivider_0, 0 ) );
	}
	else
	{
		snprintf( GuiVar_itmGroupName, NUMBER_OF_CHARS_IN_A_NAME, " <%s %s>", GuiLib_GetTextPtr( GuiStruct_txtAddNew_0, 0 ), GuiLib_GetTextPtr( GuiStruct_txtProgram_0, 0 ) );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void *MANUAL_PROGRAMS_get_group_with_this_GID( const UNS_32 pgroup_ID )
{
	MANUAL_PROGRAMS_GROUP_STRUCT *lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &manual_programs_group_list_hdr, pgroup_ID, (false) );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( lgroup );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the group with the specified group ID. This is a wrapper
 * for the nm_GROUP_get_ptr_to_group_at_this_location_in_list function so we
 * don't need to expose the list header.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *        to ensure the group that's found isn't deleted before it's used.
 *
 * @executed Executed within the context of the display processing task when the
 *  		 list and group screens are drawn; within the key processing task
 *  		 when the user navigates through the group list screen.
 * 
 * @param pindex_0 The index of the group to retrieve - 0 based.
 * 
 * @return void* A pointer to the group with the specified group ID.
 *
 * @author 9/15/2011 Adrianusv
 *
 * @revisions
 *    9/15/2011 Initial release
 */
extern void *MANUAL_PROGRAMS_get_group_at_this_index( const UNS_32 pindex_0 )
{
	MANUAL_PROGRAMS_GROUP_STRUCT *lgroup;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup = nm_GROUP_get_ptr_to_group_at_this_location_in_list( &manual_programs_group_list_hdr, pindex_0 );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( lgroup );
}

/* ---------------------------------------------------------- */
/**
 * Accepts a group ID (e.g., GuiVar_GroupIndex) and returns the position of that
 * index in the associated list. This is used to ensure the group the user
 * created or was editing is higlighted when they return to the list of groups.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure the list doesn't change between locating the index and using
 *  	  it.
 *
 * @executed Executed within the context of the key processing task when the
 *           user selects a group on the group list screen.
 * 
 * @param pgroup_ID The group ID
 * 
 * @return UNS_32 A 0-based index of the requested group into the associated
 *                list.
 *
 * @author 12/15/2011 Adrianusv
 *
 * @revisions
 *    12/15/2011 Initial release
 */
extern UNS_32 MANUAL_PROGRAMS_get_index_for_group_with_this_GID( const UNS_32 pgroup_ID )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = nm_GROUP_get_index_for_group_with_this_GID( &manual_programs_group_list_hdr, pgroup_ID );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the last Manual Programs group ID by retrieving the
 * number_of_groups_ever_created value from the head of the list.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure another group isn't added to the list between the number
 *  	  being retrieved and being used.
 *
 * @executed Executed within the context of the startup task when the stations 
 *  		 list is created for the first time; also executed within the
 *  		 display processing and key processing task as the user enters and
 *  		 leaves the editing screen.
 * 
 * @return UNS_32 The group ID of the last Manual Programs group.
 *
 * @author 9/7/2011 Adrianusv
 *
 * @revisions
 *    9/7/2011   Initial release
 *    2/8/2012	 Renamed to include nm_ prefix
 */
extern UNS_32 MANUAL_PROGRAMS_get_last_group_ID( void )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = ((GROUP_BASE_DEFINITION_STRUCT*)manual_programs_group_list_hdr.phead)->number_of_groups_ever_created;

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 MANUAL_PROGRAMS_get_num_groups_in_use( void )
{
	MANUAL_PROGRAMS_GROUP_STRUCT *lgroup;

	UNS_32	rv;

	// ----------

	rv = 0;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup = nm_ListGetFirst( &manual_programs_group_list_hdr );

	while( lgroup != NULL )
	{
		if( lgroup->in_use )
		{
			++rv;
		}

		// ----------

		lgroup = nm_ListGetNext( &manual_programs_group_list_hdr, lgroup );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 MANUAL_PROGRAMS_get_start_date( MANUAL_PROGRAMS_GROUP_STRUCT *const lgroup )
{
	DATE_TIME	ldt;

	UNS_32	rv;

	EPSON_obtain_latest_time_and_date( &ldt );
	
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( lgroup,
													 &lgroup->start_date,
													 DATE_MINIMUM,
													 DATE_MAXIMUM,
													 ldt.D,
													 &nm_MANUAL_PROGRAMS_set_start_date,
													 &lgroup->changes_to_send_to_master,
													 MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_start_date_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 MANUAL_PROGRAMS_get_end_date( MANUAL_PROGRAMS_GROUP_STRUCT *const lgroup )
{
	DATE_TIME	ldt;

	UNS_32	rv;

	EPSON_obtain_latest_time_and_date( &ldt );

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( lgroup,
													 &lgroup->stop_date,
													 DATE_MINIMUM,
													 DATE_MAXIMUM,
													 ldt.D,
													 &nm_MANUAL_PROGRAMS_set_end_date,
													 &lgroup->changes_to_send_to_master,
													 MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_stop_date_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 MANUAL_PROGRAMS_today_is_a_water_day( MANUAL_PROGRAMS_GROUP_STRUCT *const lgroup, const UNS_32 pdow )
{
	char	lfield_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	BOOL_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( pdow < DAYS_IN_A_WEEK )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_days_bit ], (pdow + 1) );

		rv = SHARED_get_bool_from_array_32_bit_change_bits_group( lgroup,
																  &lgroup->days[ pdow ],
																  pdow,
																  DAYS_IN_A_WEEK,
																  MANUAL_PROGRAMS_WATERDAY_DEFAULT,
																  &nm_MANUAL_PROGRAMS_set_water_day,
																  &lgroup->changes_to_send_to_master,
																  lfield_name );
	}
	else
	{
		Alert_index_out_of_range();

		rv = MANUAL_PROGRAMS_WATERDAY_DEFAULT;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 MANUAL_PROGRAMS_get_start_time( MANUAL_PROGRAMS_GROUP_STRUCT *const lgroup, const UNS_32 pstart_index )
{
	char	lfield_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32		rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	if( pstart_index < MANUAL_PROGRAMS_NUMBER_OF_START_TIMES )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_start_times_bit ], (pstart_index + 1) );

		rv = SHARED_get_uint32_from_array_32_bit_change_bits_group( lgroup,
																	pstart_index,
																	&lgroup->start_times[ pstart_index ],
																	MANUAL_PROGRAMS_NUMBER_OF_START_TIMES,
																	MANUAL_PROGRAMS_START_TIME_MIN,
																	MANUAL_PROGRAMS_START_TIME_MAX,
																	MANUAL_PROGRAMS_START_TIME_DEFAULT,
																	&nm_MANUAL_PROGRAMS_set_start_time,
																	&lgroup->changes_to_send_to_master,
																	lfield_name );
	}
	else
	{
		Alert_index_out_of_range();

		rv = MANUAL_PROGRAMS_START_TIME_DEFAULT;
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 MANUAL_PROGRAMS_get_run_time( MANUAL_PROGRAMS_GROUP_STRUCT *const lgroup )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( lgroup,
													 &lgroup->run_time_seconds,
													 STATION_MANUAL_PROGRAM_SECONDS_MIN,
													 STATION_MANUAL_PROGRAM_SECONDS_MAX,
													 STATION_MANUAL_PROGRAM_SECONDS_DEFAULT,
													 &nm_MANUAL_PROGRAMS_set_run_time,
													 &lgroup->changes_to_send_to_master,
													 MANUAL_PROGRAMS_database_field_names[ MANUAL_PROGRAMS_CHANGE_BITFIELD_run_time_bit ] );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern CHANGE_BITS_32_BIT *MANUAL_PROGRAMS_get_change_bits_ptr( MANUAL_PROGRAMS_GROUP_STRUCT *pgroup, const UNS_32 pchange_reason )
{
	return( SHARED_get_32_bit_change_bits_ptr( pchange_reason, &pgroup->changes_to_send_to_master, &pgroup->changes_to_distribute_to_slaves, &pgroup->changes_to_upload_to_comm_server ) );
}

/* ---------------------------------------------------------- */
extern UNS_32 MANUAL_PROGRAMS_return_the_number_of_starts_for_this_GID( const UNS_32 pgid, const DATE_TIME_COMPLETE_STRUCT *const pdtcs_ptr )
{
	MANUAL_PROGRAMS_GROUP_STRUCT	*lgroup;

	UNS_32	rv;
	
	rv = 0;
	
	// 11/6/2012 ajv : A GID of 0 for Manual Programs implies that the station
	// is not assigned to a group. This is valid in this case. Therefore, we
	// shouldn't try to get a pointer to the group because doing so will
	// generate an alert.
	if( pgid > 0 )
	{
		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		lgroup = nm_GROUP_get_ptr_to_group_with_this_GID( &manual_programs_group_list_hdr, pgid, (false) );
		
		if( lgroup != NULL )
		{
			// 10/4/2012 rmd : Are we within the date range allowed to run?
			if( (pdtcs_ptr->date_time.D >= lgroup->start_date) && (pdtcs_ptr->date_time.D <= lgroup->stop_date) )
			{
				// 10/4/2012 rmd : Is today a water day?
				if( lgroup->days[ pdtcs_ptr->__dayofweek ] == (true) )
				{
					UNS_32	i;
					
					for( i=0; i<MANUAL_PROGRAMS_NUMBER_OF_START_TIMES; i++ )
					{
						if( lgroup->start_times[ i ] == pdtcs_ptr->date_time.T )
						{
							// 10/4/2012 rmd : Indicate one more cycle requested.
							rv += 1;
						}
						
					}
					
				}  // of it's a water day

			}  // of we're within the date range

		}  // of if we found a group with the GID

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}
	
	return( rv );
	
}

/* ---------------------------------------------------------- */
extern void MANUAL_PROGRAMS_clean_house_processing( void )
{
	// 4/16/2014 rmd : Executed within the context of the comm_mngr task. ONLY BY SLAVES. THE
	// CHANGES MADE HERE DO NOT BE DISTRIBUTED. THE STATION LIST IS BEING WIPED IN PREPARATION
	// OF BEING ADDED AS A NEW MEMBER TO A CHAIN. Called when the result of a scan indicates
	// this controller is a new slave in the chain. Used when building up chains. Likely ONLY
	// used when assembling a chain for the first time or when increasing the length of a chain
	// to a length it has never 'run' at before. Or if you changed the LETTER of a slave in a
	// chain.
	
	void	*tptr;
	
	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	tptr = nm_ListRemoveHead( &manual_programs_group_list_hdr );

	while( tptr != NULL )
	{
		mem_free( tptr );

		tptr = nm_ListRemoveHead( &manual_programs_group_list_hdr );
	}

	// ----------

	// 4/16/2014 rmd : By setting the no groups exist flag to false no change bits will be set
	// for this 'dummy' group. And that is what we want.
	nm_GROUP_create_new_group( &manual_programs_group_list_hdr, (char*)MANUAL_PROGRAMS_DEFAULT_NAME, &nm_MANUAL_PROGRAMS_set_default_values, sizeof(MANUAL_PROGRAMS_GROUP_STRUCT), (false), NULL );

	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE, MANUAL_PROGRAMS_FILENAME, MANUAL_PROGRAMS_LATEST_FILE_REVISION, &manual_programs_group_list_hdr, sizeof( MANUAL_PROGRAMS_GROUP_STRUCT ), list_program_data_recursive_MUTEX, FF_MANUAL_PROGRAMS );

	// ----------

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------

	// 5/13/2014 ajv : Since we've removed and re-added a system, synchronize to
	// the system preserves
	SYSTEM_PRESERVES_synchronize_preserves_to_file();
}

/* ---------------------------------------------------------- */
extern void MANUAL_PROGRAMS_set_bits_on_all_groups_to_cause_distribution_in_the_next_token( void )
{
	MANUAL_PROGRAMS_GROUP_STRUCT	*lgroup;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lgroup = nm_ListGetFirst( &manual_programs_group_list_hdr );

	while( lgroup != NULL )
	{
		// 5/1/2014 rmd : This function ONLY being called at the conclusion of a SCAN by the master
		// of a real multiple controller chain - we want to broadcast to the slaves using tokens
		// about all the hardware we presently know about.
		SHARED_set_all_32_bit_change_bits( &lgroup->changes_to_distribute_to_slaves, list_program_data_recursive_MUTEX );

		lgroup = nm_ListGetNext( &manual_programs_group_list_hdr, lgroup );
	}

	// ----------

	// 4/29/2014 ajv : Set a flag in the changes structure to notify this
	// controller to examine its lists for changes to distribute to the other
	// controllers in the chain in it's next token resposne. The flag is part of
	// the mutex protected comm_mngr struct so take the MUTEX.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	comm_mngr.changes.distribute_changes_to_slave = (true);

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );

	// ----------

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void MANUAL_PROGRAMS_on_all_groups_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear )
{
	MANUAL_PROGRAMS_GROUP_STRUCT	*lgroup_ptr;

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lgroup_ptr = nm_ListGetFirst( &manual_programs_group_list_hdr );

	while( lgroup_ptr != NULL )
	{
		if( pset_or_clear == COMMSERVER_CHANGE_BITS_ALL_CLEARED )
		{
			SHARED_clear_all_32_bit_change_bits( &lgroup_ptr->changes_to_upload_to_comm_server, list_program_data_recursive_MUTEX );
		}
		else
		{
			SHARED_set_all_32_bit_change_bits( &lgroup_ptr->changes_to_upload_to_comm_server, list_program_data_recursive_MUTEX );
		}

		lgroup_ptr = nm_ListGetNext( &manual_programs_group_list_hdr, lgroup_ptr );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * @mutex_requirements Requires the caller to have the pending_changes_recursive_MUTEX to
 * ensure no other function tries to modify the pending_changes_to_send_to_comm_server flag.
 *
 * @task_execution Executed within the context of the CONTROLLER INITIATED task when it
 * first starts and when the waiting response flags are cleared after a communication.
 */
extern void nm_MANUAL_PROGRAMS_update_pending_change_bits( const UNS_32 pcomm_error_occurred )
{
	MANUAL_PROGRAMS_GROUP_STRUCT	*lprogram;

	BOOL_32	lfile_save_necessary;

	lfile_save_necessary = (false);

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lprogram = nm_ListGetFirst( &manual_programs_group_list_hdr );

	while( lprogram != NULL )
	{
		// 4/30/2015 ajv : No reason to process this if none of the bits are set. This also helps
		// reduce the file save activity.
		if( lprogram->changes_uploaded_to_comm_server_awaiting_ACK != CHANGE_BITS_32_BIT_ALL_CLEAR )
		{
			// 4/29/2015 ajv : If a communication error occurred while sending Program Data, copy the
			// bits of variables that were sent back into the original bit field to ensure they're sent
			// again during the next attempt. Otherwise, clear the pending bits.
			if( pcomm_error_occurred )
			{
				lprogram->changes_to_upload_to_comm_server |= lprogram->changes_uploaded_to_comm_server_awaiting_ACK;

				// 4/30/2015 ajv : Since we've determined there were changes that were not successfully
				// sent, set the flag to trigger another attempt
				weather_preserves.pending_changes_to_send_to_comm_server = (true);

				// 4/30/2015 ajv : Also set the timer to force the data to send. If the timer's already
				// running, this will simply restart it.
				// 
				// 4/30/2015 ajv : BECAUSE of the potential that this function is executed many times (once
				// for each item changed) we could not start our pdata timer with a post to the CI queue.
				// The queue would overflow. So start the timer directly.
				xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGES_NOT_SENT_MS ), portMAX_DELAY );
			}
			else
			{
				SHARED_clear_all_32_bit_change_bits( &lprogram->changes_uploaded_to_comm_server_awaiting_ACK, list_program_data_recursive_MUTEX );
			}

			// 4/30/2015 ajv : At least one manual program was updated. So make sure we save the file.
			lfile_save_necessary = (true);
		}

		lprogram = nm_ListGetNext( &manual_programs_group_list_hdr, lprogram );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	if( lfile_save_necessary )
	{
		// 4/29/2015 ajv : Now that the bits are set or cleared accordingly, trigger a file save.
		// If power fails between building the message and receiving the ACK, the original bit field
		// hasn't been saved to flash yet, so it will resend the associated variables during the
		// next attempt.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_MANUAL_PROGRAMS, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
	}
}

/* ---------------------------------------------------------- */
extern void MANUAL_PROGRAMS_load_ftimes_list( void )
{
	MANUAL_PROGRAMS_GROUP_STRUCT	*mp_from_list;

	FT_MANUAL_PROGRAM_STRUCT		*ft_mp;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	// ----------

	mp_from_list = nm_ListGetFirst( &manual_programs_group_list_hdr );

	while( mp_from_list != NULL )
	{
		if( mp_from_list->in_use )
		{
			if( mem_obtain_a_block_if_available( sizeof(FT_MANUAL_PROGRAM_STRUCT), (void**)&ft_mp ) )
			{
				memset( ft_mp, 0x00, sizeof(FT_MANUAL_PROGRAM_STRUCT) );
				
				ft_mp->group_identity_number = mp_from_list->base.group_identity_number;
				
				memcpy( ft_mp->start_times, mp_from_list->start_times, sizeof(ft_mp->start_times) );

				memcpy( ft_mp->days, mp_from_list->days, sizeof(ft_mp->days) );
				
				ft_mp->start_date = mp_from_list->start_date;

				ft_mp->stop_date = mp_from_list->stop_date;

				ft_mp->run_time_seconds = mp_from_list->run_time_seconds;
				
				// ----------
				
				// 2/14/2018 rmd : Now add it to the ftimes list.
				nm_ListInsertTail( &ft_manual_programs_list_hdr, ft_mp );
			}
		}


		mp_from_list = nm_ListGetNext( &manual_programs_group_list_hdr, mp_from_list );
	}

	// ----------

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
BOOL_32 MANUAL_PROGRAMS_calculate_chain_sync_crc( UNS_32 const pff_name )
{
	// 8/30/2018 rmd : Executed in the context of the comm_mngr task when token responses are
	// being made. And that's it.

	// ----------

	MANUAL_PROGRAMS_GROUP_STRUCT	*lmanual_program_ptr;
	
	UNS_8				*checksum_start;

	UNS_32				checksum_length;

	UNS_8				*ucp;
	
	BOOL_32				rv;

	// ----------

	rv = (false);

	// ----------
	
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	// 8/30/2018 rmd : So the CRC calculation function wants a continuous memory space and a
	// length to calculate the crc off. In this function we will pick out the desired data, and
	// simply pile it up on itself, list item by list item. We don't care how messy the pile is
	// because we never attempt to reference any data once it is added to the pile. The data is
	// only there to include in the checksum.
	//
	// 8/30/2018 rmd : Well we know it can't be bigger than the list item count times the size
	// of each list item, so start with a memory block that big.
	if( mem_obtain_a_block_if_available( (manual_programs_group_list_hdr.count * sizeof(MANUAL_PROGRAMS_GROUP_STRUCT)), (void**)&checksum_start ) )
	{
		// 9/5/2018 rmd : Well then the calculation will be successful. Nothing else can prevent
		// that outcome.
		rv = (true);
		
		// ----------
		
		ucp = checksum_start;
		
		checksum_length = 0;
		
		// ----------
		
		// 8/30/2018 rmd : Rattle though the list and push the fields we want to include in the sync
		// test onto the block to be checksummed.
		lmanual_program_ptr = nm_ListGetFirst( &manual_programs_group_list_hdr );
		
		while( lmanual_program_ptr != NULL )
		{
			// 9/4/2018 rmd : Make sure you include only TRUE USER SETTINGS, no flags, or pointers to
			// memory, etc...
			
			// ----------
			
			// 11/2/2018 rmd : Due to the inconsistent way the group names strings are handled during
			// the sync (we do not regligiously take all 48 characters and always carry them through the
			// sync process, there are places we only copy up until the null), the part of the string
			// following the null char is not guaranteed to be in sync. So only include up until the
			// null in the crc test.
			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmanual_program_ptr->base.description, strlen(lmanual_program_ptr->base.description) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmanual_program_ptr->start_times, sizeof(lmanual_program_ptr->start_times) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmanual_program_ptr->days, sizeof(lmanual_program_ptr->days) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmanual_program_ptr->start_date, sizeof(lmanual_program_ptr->start_date) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmanual_program_ptr->stop_date, sizeof(lmanual_program_ptr->stop_date) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmanual_program_ptr->run_time_seconds, sizeof(lmanual_program_ptr->run_time_seconds) );

			// skipping the 4 changes bit fields
			
			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmanual_program_ptr->in_use, sizeof(lmanual_program_ptr->in_use) );


			// END
			
			// ----------
		
			lmanual_program_ptr = nm_ListGetNext( &manual_programs_group_list_hdr, lmanual_program_ptr );
		}
		
		// ----------
		
		// 8/30/2018 rmd : Now calculate the checksum.
		cscs.the_crc[ pff_name ] = CRC_calculate_32bit_big_endian( checksum_start, checksum_length );
		
		// ----------
		
		// 9/4/2018 rmd : We're done with the block, so of course free it.
		mem_free( checksum_start );
	}
	else
	{
		// 9/5/2018 rmd : So by design it's okay if there is no memory. The checksum won't be set
		// valid, and the next time we try to include it in a token response we'll try to calculate
		// it again. Self protecting and retrying. But let's alert about this so we know it's taking
		// place.
		Alert_Message_va( "SYNC: no mem to calc checksum %s", chain_sync_file_pertinants[ pff_name ].file_name_string );
	}
	
	// ----------
	
	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

