/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strncmp
#include	<string.h>

#include	"shared.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"background_calculations.h"

#include	"bithacks.h"

#include	"change.h"

#include	"comm_mngr.h"

#include	"configuration_controller.h"

#include	"flowsense.h"

#include	"range_check.h"

#include	"station_groups.h"

#include	"stations.h"

#include	"controller_initiated.h"

#ifdef	_MSC_VER

	#include	"sql_merge.h"

#endif




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void set_comm_mngr_changes_flag_based_upon_reason_for_change( const UNS_32 pchange_received_from )
{
	// 10/1/2012 rmd : Set a flag in the changes structure to notify
	// this controller to examine its lists for changes to
	// distribute. The flag is part of the mutex protected comm_mngr
	// struct so take the MUTEX.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	switch( pchange_received_from )
	{
		case CHANGE_REASON_NO_REASON:
		case CHANGE_REASON_KEYPAD:
		case CHANGE_REASON_NEW_FIRMWARE:
		case CHANGE_REASON_RANGE_CHECK_FAILED:
		case CHANGE_REASON_ACQUIRED_EXPECTED_GPM:
		case CHANGE_REASON_HIT_A_STARTTIME:
		case CHANGE_REASON_SHARING_WEATHER:
		case CHANGE_REASON_ITS_RAINING:
		case CHANGE_REASON_DAY_CHANGE:
		case CHANGE_REASON_NORMAL_STARTUP:
		case CHANGE_REASON_DONT_SHOW_A_REASON:
		case CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED:
		case CHANGE_REASON_TERMINAL_CARD_OR_DECODER_REMOVED:
		case CHANGE_REASON_STATION_COPY:
		case CHANGE_REASON_SYNC_SENDING_TO_MASTER:
			// 5/8/2014 ajv : Set a flag in the changes structure to notify
			// this controller to examine its lists for changes to send to
			// the master in it's next token response.
			comm_mngr.changes.send_changes_to_master = (true);
			break;

		case CHANGE_REASON_CENTRAL_OR_MOBILE:
		case CHANGE_REASON_CHANGED_AT_SLAVE:
		case CHANGE_REASON_SYNC_DISTRIBUTING_TO_SLAVES:
			// 5/8/2014 ajv : Set a flag in the changes structure to notify
			// this controller to examine its lists for changes to send to
			// distribute to the slaves in it's next token and to the central in
			// it's next upload to the web.
			comm_mngr.changes.distribute_changes_to_slave = (true);

			// 5/8/2014 ajv : Only send changes made at the master to the CommServer. If the change came
			// from Command Center Online, or we're in the process of distributing changes, don't set
			// the flag.
			if( pchange_received_from == CHANGE_REASON_CHANGED_AT_SLAVE )
			{
				xSemaphoreTakeRecursive( pending_changes_recursive_MUTEX, portMAX_DELAY );

				// 4/30/2015 ajv : Set the flag to indicate there are pending changes to send to the
				// CommServer.
				weather_preserves.pending_changes_to_send_to_comm_server = (true);

				xSemaphoreGiveRecursive( pending_changes_recursive_MUTEX );

				// 4/24/2015 rmd : BECAUSE of the potential that this function is executed many times (once
				// for each item changed) we could not start our pdata timer with a post to the CI queue.
				// The queue would overflow. So start the timer directly.
				//
				// 5/8/2015 rmd : NOTE - this function is called very early on during the startup as part of
				// the file initalization function calls. BEFORE the CI task and THIS TIMER exist. So we
				// must test for a NULL timer here. When the CI task is created the
				// weather_preserves.pending_changes_to_send_to_comm_server flag is tested, and if set this
				// timer will be started then.
				if( cics.pdata_timer != NULL )
				{
					xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGE_MADE_MS ), portMAX_DELAY );
				}
			}
			break;

		case CHANGE_REASON_DISTRIBUTED_BY_MASTER:
		case CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER:
		default:
			// 5/8/2014 ajv : Do nothing
			break;
	}

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );

	// ------------

	// 07/08/2013 ajv : Post a message to update the GuiVars used to display
	// the next scheduled irrigation on the Status screen. This ensures the
	// Status screen is up-to-date once the user views it.
	postBackground_Calculation_Event( BKGRND_CALC_NEXT_SCHEDULED );
}

/* ---------------------------------------------------------- */
extern void SHARED_set_32_bit_change_bits( CHANGE_BITS_32_BIT* pchange_bits_for_controller_ptr,
										   CHANGE_BITS_32_BIT* pchange_bits_for_central_ptr,
										   UNS_32 pchange_bit_to_set,
										   const UNS_32 preason_for_change,
										   const BOOL_32 pset_change_bits )
{
	if( pset_change_bits )
	{
		if( pchange_bits_for_controller_ptr != NULL )
		{
			B_SET( *pchange_bits_for_controller_ptr, pchange_bit_to_set );
		}

		if( pchange_bits_for_central_ptr != NULL )
		{
			B_SET( *pchange_bits_for_central_ptr, pchange_bit_to_set );
		}

		set_comm_mngr_changes_flag_based_upon_reason_for_change( preason_for_change );
	}
}

/* ---------------------------------------------------------- */
extern void SHARED_set_64_bit_change_bits( CHANGE_BITS_64_BIT* pchange_bits_for_controller_ptr,
										   CHANGE_BITS_64_BIT* pchange_bits_for_central_ptr,
										   UNS_32 pchange_bit_to_set,
										   const UNS_32 preason_for_change,
										   const BOOL_32 pset_change_bits )
{
	if( pset_change_bits )
	{
		if( pchange_bits_for_controller_ptr != NULL )
		{
			B_SET_64( *pchange_bits_for_controller_ptr, pchange_bit_to_set );
		}

		if( pchange_bits_for_central_ptr != NULL )
		{
			B_SET_64( *pchange_bits_for_central_ptr, pchange_bit_to_set );
		}

		set_comm_mngr_changes_flag_based_upon_reason_for_change( preason_for_change );
	}
}

/* ---------------------------------------------------------- */
extern void SHARED_set_all_32_bit_change_bits( CHANGE_BITS_32_BIT *pchange_bits_to_set, xSemaphoreHandle precursive_mutex  )
{
	xSemaphoreTakeRecursive( precursive_mutex, portMAX_DELAY );

	if( pchange_bits_to_set != NULL )
	{
		*pchange_bits_to_set = CHANGE_BITS_32_BIT_ALL_SET;
	}
	else
	{
		Alert_func_call_with_null_ptr();
	}

	xSemaphoreGiveRecursive( precursive_mutex );
}

/* ---------------------------------------------------------- */
extern void SHARED_clear_all_32_bit_change_bits( CHANGE_BITS_32_BIT *pchange_bits_to_clear, xSemaphoreHandle precursive_mutex  )
{
	xSemaphoreTakeRecursive( precursive_mutex, portMAX_DELAY );

	if( pchange_bits_to_clear != NULL )
	{
		*pchange_bits_to_clear = CHANGE_BITS_32_BIT_ALL_CLEAR;
	}
	else
	{
		Alert_func_call_with_null_ptr();
	}

	xSemaphoreGiveRecursive( precursive_mutex );
}

/* ---------------------------------------------------------- */
extern void SHARED_set_all_64_bit_change_bits( CHANGE_BITS_64_BIT *pchange_bits_to_set, xSemaphoreHandle precursive_mutex  )
{
	xSemaphoreTakeRecursive( precursive_mutex, portMAX_DELAY );

	if( pchange_bits_to_set != NULL )
	{
		*pchange_bits_to_set = CHANGE_BITS_64_BIT_ALL_SET;
	}
	else
	{
		Alert_func_call_with_null_ptr();
	}

	xSemaphoreGiveRecursive( precursive_mutex );
}

/* ---------------------------------------------------------- */
extern void SHARED_clear_all_64_bit_change_bits( CHANGE_BITS_64_BIT *pchange_bits_to_clear, xSemaphoreHandle precursive_mutex )
{
	xSemaphoreTakeRecursive( precursive_mutex, portMAX_DELAY );

	if( pchange_bits_to_clear != NULL )
	{
		*pchange_bits_to_clear = CHANGE_BITS_64_BIT_ALL_CLEAR;
	}
	else
	{
		Alert_func_call_with_null_ptr();
	}

	xSemaphoreGiveRecursive( precursive_mutex );
}

/* ---------------------------------------------------------- */
extern CHANGE_BITS_32_BIT *SHARED_get_32_bit_change_bits_ptr( const UNS_32 pchange_reason, CHANGE_BITS_32_BIT *pchanges_to_send_to_master, CHANGE_BITS_32_BIT *pchanges_to_distribute_to_slaves, CHANGE_BITS_32_BIT *pchanges_to_upload_to_comm_server )
{
	CHANGE_BITS_32_BIT *lchange_bitfield_to_set;

	// 5/1/2014 ajv : Based upon the change reason, we need to set the
	// appropriate change bit field so it's either uploaded to the master or
	// distributed down the chain. Notice that there's no reference to the
	// change_bits_for_central bitfield here. This is intentional. ALL changes
	// will be flagged to be sent to the central, regardless of weather it's a
	// master or a slave, even though only the master actually sends the
	// changes.
	switch( pchange_reason )
	{
		case CHANGE_REASON_KEYPAD:
		case CHANGE_REASON_NEW_FIRMWARE:
		case CHANGE_REASON_RANGE_CHECK_FAILED:
		case CHANGE_REASON_ACQUIRED_EXPECTED_GPM:
		case CHANGE_REASON_HIT_A_STARTTIME:
		case CHANGE_REASON_SHARING_WEATHER:
		case CHANGE_REASON_ITS_RAINING:
		case CHANGE_REASON_DAY_CHANGE:
		case CHANGE_REASON_NORMAL_STARTUP:
		case CHANGE_REASON_DONT_SHOW_A_REASON:
		case CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED:
		case CHANGE_REASON_TERMINAL_CARD_OR_DECODER_REMOVED:
		case CHANGE_REASON_STATION_COPY:
			lchange_bitfield_to_set = pchanges_to_send_to_master;
			break;

		case CHANGE_REASON_DISTRIBUTED_BY_MASTER:
			// 5/1/2014 ajv : The changes were received from a MASTER, so
			// there's really nothing to do since the pset_change_bits flag
			// is FALSE. Therefore, let's set this to NULL.
			lchange_bitfield_to_set = NULL;
			break;

		case CHANGE_REASON_CENTRAL_OR_MOBILE:
		case CHANGE_REASON_CHANGED_AT_SLAVE:
			// 5/1/2014 ajv : The changes were received from a SLAVE, so the
			// changes must be distributed to all slaves in the chain.
			lchange_bitfield_to_set = pchanges_to_distribute_to_slaves;
			break;

		case CHANGE_REASON_SYNC_SENDING_TO_MASTER:
			lchange_bitfield_to_set = pchanges_to_send_to_master;
			break;

		case CHANGE_REASON_SYNC_DISTRIBUTING_TO_SLAVES:
			lchange_bitfield_to_set = pchanges_to_distribute_to_slaves;
			break;

		case CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER:
			lchange_bitfield_to_set = pchanges_to_upload_to_comm_server;
			break;

		default:
			Alert_Message( "Unknown change reason" );

			// 5/1/2014 ajv : What if it's an unknown reason? Not quite sure
			// which to use, but it's probably safe to send it to the master
			// which will distribute if necessary.
			lchange_bitfield_to_set = pchanges_to_send_to_master;
			break;
	}

	return( lchange_bitfield_to_set );
}

/* ---------------------------------------------------------- */
extern CHANGE_BITS_64_BIT *SHARED_get_64_bit_change_bits_ptr( const UNS_32 pchange_reason, CHANGE_BITS_64_BIT *pchanges_to_send_to_master, CHANGE_BITS_64_BIT *pchanges_to_distribute_to_slaves, CHANGE_BITS_64_BIT *pchanges_to_upload_to_comm_server )
{
	CHANGE_BITS_64_BIT *lchange_bitfield_to_set;

	// 5/1/2014 ajv : Based upon the change reason, we need to set the
	// appropriate change bit field so it's either uploaded to the master or
	// distributed down the chain. Notice that there's no reference to the
	// change_bits_for_central bitfield here. This is intentional. ALL changes
	// will be flagged to be sent to the central, regardless of weather it's a
	// master or a slave, even though only the master actually sends the
	// changes.
	switch( pchange_reason )
	{
		case CHANGE_REASON_KEYPAD:
		case CHANGE_REASON_NEW_FIRMWARE:
		case CHANGE_REASON_RANGE_CHECK_FAILED:
		case CHANGE_REASON_ACQUIRED_EXPECTED_GPM:
		case CHANGE_REASON_HIT_A_STARTTIME:
		case CHANGE_REASON_SHARING_WEATHER:
		case CHANGE_REASON_ITS_RAINING:
		case CHANGE_REASON_DAY_CHANGE:
		case CHANGE_REASON_NORMAL_STARTUP:
		case CHANGE_REASON_DONT_SHOW_A_REASON:
		case CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED:
		case CHANGE_REASON_TERMINAL_CARD_OR_DECODER_REMOVED:
		case CHANGE_REASON_STATION_COPY:
			lchange_bitfield_to_set = pchanges_to_send_to_master;
			break;

		case CHANGE_REASON_DISTRIBUTED_BY_MASTER:
			// 5/1/2014 ajv : The changes were received from a MASTER, so
			// there's really nothing to do since the pset_change_bits flag
			// is FALSE. Therefore, let's set this to NULL.
			lchange_bitfield_to_set = NULL;
			break;

		case CHANGE_REASON_CENTRAL_OR_MOBILE:
		case CHANGE_REASON_CHANGED_AT_SLAVE:
			// 5/1/2014 ajv : The changes were received from a SLAVE, so the
			// changes must be distributed to all slaves in the chain.
			lchange_bitfield_to_set = pchanges_to_distribute_to_slaves;
			break;

		case CHANGE_REASON_SYNC_SENDING_TO_MASTER:
			lchange_bitfield_to_set = pchanges_to_send_to_master;
			break;

		case CHANGE_REASON_SYNC_DISTRIBUTING_TO_SLAVES:
			lchange_bitfield_to_set = pchanges_to_distribute_to_slaves;
			break;

		case CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER:
			lchange_bitfield_to_set = pchanges_to_upload_to_comm_server;
			break;

		default:
			Alert_Message( "Unknown change reason" );

			// 5/1/2014 ajv : What if it's an unknown reason? Not quite sure
			// which to use, but it's probably safe to send it to the master
			// which will distribute if necessary.
			lchange_bitfield_to_set = pchanges_to_send_to_master;
			break;
	}

	return( lchange_bitfield_to_set );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// #ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_bool_controller( BOOL_32 *pvar_to_change_ptr,
										   BOOL_32 pnew_value,
										   const BOOL_32 pdefault,
										   const BOOL_32 pgenerate_change_line_bool,
										   const UNS_32 pchange_line_index,
										   const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const BOOL_32 pset_change_bits,
										   CHANGE_BITS_32_BIT *pchange_bits_for_controller,
										   CHANGE_BITS_32_BIT *pchange_bits_for_central,
										   UNS_32 pchange_bit_to_set,
										   const char *pfield_name
										   #ifdef _MSC_VER
										   , char *pmerge_update_str,
										   char *pmerge_insert_fields_str,
										   char *pmerge_insert_values_str
										   #endif
										 )
{
	BOOL_32	lrange_check_passed;

	BOOL_32 rv;

	rv = (false);

	// 2/2/2015 ajv : If the range check fails, we've decided to correct the value and then
	// continue processing as if that was value that was passed in.
	lrange_check_passed = ((preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED) && (RangeCheck_bool( &pnew_value, pdefault, NULL, pfield_name )));

	#ifndef _MSC_VER

		// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
		// return value is set properly so the change bits are set and the calling function knows
		// it's ok to use the value.
		if( (*pvar_to_change_ptr != pnew_value) || (!lrange_check_passed) )
		{
			if( pgenerate_change_line_bool == (true) )
			{
				Alert_ChangeLine_Controller( pchange_line_index, pbox_index_0, pvar_to_change_ptr, &pnew_value, sizeof(pnew_value), preason_for_change, pbox_index_0 );
			}

			*pvar_to_change_ptr = pnew_value;

			rv = (true);
		}

		// 5/12/2014 ajv : If the value was actually changed OR the change was made from the slave
		// and uploaded to the master, set the change bits. This second condition is important
		// because changes made directly at the master are sent to itself as if from a slave. In
		// this case, this routine is called during the _extract_and_store_from_comm routine but
		// the values are identical. However, we still need to distribute those
		// changes to the rest of the chain.
		if( (rv == (true)) || (!lrange_check_passed) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
		{
			SHARED_set_32_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
		}

	#else

		SQL_MERGE_build_upsert_bool( pnew_value, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

		rv = (true);

	#endif

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_uint32_controller( UNS_32 *pvar_to_change_ptr,
											 UNS_32 pnew_value,
											 const UNS_32 pmin,
											 const UNS_32 pmax,
											 const UNS_32 pdefault,
											 const BOOL_32 pgenerate_change_line_bool,
											 const UNS_32 pchange_line_index,
											 const UNS_32 preason_for_change,
											 const UNS_32 pbox_index_0,
											 const BOOL_32 pset_change_bits,
											 CHANGE_BITS_32_BIT *pchange_bits_for_controller,
											 CHANGE_BITS_32_BIT *pchange_bits_for_central,
											 UNS_32 pchange_bit_to_set,
											 const char *pfield_name
											 #ifdef _MSC_VER
											 , char *pmerge_update_str,
											 char *pmerge_insert_fields_str,
											 char *pmerge_insert_values_str
											 #endif
											)
{
	BOOL_32	lrange_check_passed;

	BOOL_32 rv;

	rv = (false);

	// 2/2/2015 ajv : If the range check fails, we've decided to correct the value and then
	// continue processing as if that was value that was passed in.
	lrange_check_passed = ((preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED) && (RangeCheck_uint32( &pnew_value, pmin, pmax, pdefault, NULL, pfield_name )));

	#ifndef _MSC_VER

		// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
		// return value is set properly so the change bits are set and the calling function knows
		// it's ok to use the value.
		if( (*pvar_to_change_ptr != pnew_value) || (!lrange_check_passed) )
		{
			if( pgenerate_change_line_bool == (true) )
			{
				Alert_ChangeLine_Controller( pchange_line_index, pbox_index_0, pvar_to_change_ptr, &pnew_value, sizeof(pnew_value), preason_for_change, pbox_index_0 );
			}

			*pvar_to_change_ptr = pnew_value;

			rv = (true);
		}

		// 5/12/2014 ajv : If the value was actually changed OR the change was
		// made from the slave and uploaded to the master, set the change bits.
		// This second condition is important because changes made directly at
		// the master are sent to itself as if from a slave. In this case, this
		// routine is called during the _extract_and_store_from_comm routine but
		// the values are identical. However, we still need to distribute those
		// changes to the rest of the chain.
		if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
		{
			SHARED_set_32_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
		}
	#else

		SQL_MERGE_build_upsert_uint32( pnew_value, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

		rv = (true);

	#endif

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_string_controller( char *pvar_to_change_ptr,
											 const UNS_32 psize_of_string,
											 const char *pnew_value,
											 const BOOL_32 pgenerate_change_line_bool,
											 const UNS_32 pchange_line_index,
											 const UNS_32 preason_for_change,
											 const UNS_32 pbox_index_0,
											 const BOOL_32 pset_change_bits,
											 CHANGE_BITS_32_BIT *pchange_bits_for_controller,
											 CHANGE_BITS_32_BIT *pchange_bits_for_central,
											 UNS_32 pchange_bit_to_set,
											 const char *pfield_name
											 #ifdef _MSC_VER
											 , char *pmerge_update_str,
											 char *pmerge_insert_fields_str,
											 char *pmerge_insert_values_str
											 #endif
											)
{
	BOOL_32	lrange_check_passed;

	BOOL_32 rv;

	rv = (false);

	// 2/2/2015 ajv : If the range check fails, the value will automatically be corrected. W
	// will then continue processing as if that was value that was passed in.
	lrange_check_passed = ((preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED) && (RangeCheck_string( (char *)pnew_value, psize_of_string, pfield_name )));

	#ifndef _MSC_VER

		// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
		// return value is set properly so the change bits are set and the calling function knows
		// it's ok to use the value.
		if( (strncmp( pvar_to_change_ptr, pnew_value, psize_of_string ) != 0) || (!lrange_check_passed) )
		{
			if( pgenerate_change_line_bool == (true) )
			{
				Alert_ChangeLine_Controller( pchange_line_index, pbox_index_0, pvar_to_change_ptr, pnew_value, psize_of_string, preason_for_change, pbox_index_0 );
			}

			if( strlcpy(pvar_to_change_ptr, pnew_value, psize_of_string) >= psize_of_string )
			{
				Alert_string_too_long( pvar_to_change_ptr, psize_of_string );
			}

			rv = (true);
		}

		// 5/12/2014 ajv : If the value was actually changed OR the change was
		// made from the slave and uploaded to the master, set the change bits.
		// This second condition is important because changes made directly at
		// the master are sent to itself as if from a slave. In this case, this
		// routine is called during the _extract_and_store_from_comm routine but
		// the values are identical. However, we still need to distribute those
		// changes to the rest of the chain.
		if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
		{
			SHARED_set_32_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
		}

	#else

		SQL_MERGE_build_upsert_string( pnew_value, psize_of_string, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

		rv = (true);

	#endif

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_time_controller( UNS_32 *pvar_to_change_ptr,
										   UNS_32 pnew_value,
										   const UNS_32 pmin,
										   const UNS_32 pmax,
										   const UNS_32 pdefault,
										   const BOOL_32 pgenerate_change_line_bool,
										   const UNS_32 pchange_line_index,
										   const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const BOOL_32 pset_change_bits,
										   CHANGE_BITS_32_BIT *pchange_bits_for_controller,
										   CHANGE_BITS_32_BIT *pchange_bits_for_central,
										   UNS_32 pchange_bit_to_set,
										   const char *pfield_name
										   #ifdef _MSC_VER
										   , char *pmerge_update_str,
										   char *pmerge_insert_fields_str,
										   char *pmerge_insert_values_str
										   #endif
										 )
{
	BOOL_32	lrange_check_passed;

	BOOL_32 rv;

	rv = (false);

	// 2/2/2015 ajv : If the range check fails, we've decided to correct the value and then
	// continue processing as if that was value that was passed in.
	lrange_check_passed = ((preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED) && (RangeCheck_uint32( &pnew_value, pmin, pmax, pdefault, NULL, pfield_name )));

	#ifndef _MSC_VER

		// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
		// return value is set properly so the change bits are set and the calling function knows
		// it's ok to use the value.
		if( (*pvar_to_change_ptr != pnew_value) || (!lrange_check_passed) )
		{
			if( pgenerate_change_line_bool == (true) )
			{
				Alert_ChangeLine_Controller( pchange_line_index, pbox_index_0, pvar_to_change_ptr, &pnew_value, sizeof(pnew_value), preason_for_change, pbox_index_0 );
			}

			*pvar_to_change_ptr = pnew_value;

			rv = (true);
		}

		// 5/12/2014 ajv : If the value was actually changed OR the change was
		// made from the slave and uploaded to the master, set the change bits.
		// This second condition is important because changes made directly at
		// the master are sent to itself as if from a slave. In this case, this
		// routine is called during the _extract_and_store_from_comm routine but
		// the values are identical. However, we still need to distribute those
		// changes to the rest of the chain.
		if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
		{
			SHARED_set_32_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
		}
	#else

		SQL_MERGE_build_upsert_time( pnew_value, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

		rv = (true);

	#endif

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 SHARED_set_DLS_controller( DAYLIGHT_SAVING_TIME_STRUCT *pvar_to_change_ptr,
										 DAYLIGHT_SAVING_TIME_STRUCT pdls,
										 const UNS_32 pdefault_month,
										 const UNS_32 pdefault_min_day,
										 const UNS_32 pdefault_max_day,
										 const BOOL_32 pgenerate_change_line_bool,
										 const UNS_32 pchange_line_index,
										 const UNS_32 preason_for_change,
										 const UNS_32 pbox_index_0,
										 const BOOL_32 pset_change_bits,
										 CHANGE_BITS_32_BIT *pchange_bits_for_controller,
										 CHANGE_BITS_32_BIT *pchange_bits_for_central,
										 CHANGE_BITS_32_BIT pchange_bit_to_set,
										 const char *pfield_name_month
										 #ifdef _MSC_VER
										 , const char *pfield_name_min_day,
										 const char *pfield_name_max_day,
										 char *pmerge_update_str,
										 char *pmerge_insert_fields_str,
										 char *pmerge_insert_values_str
										 #endif
									   )
{
	UNS_32	lrange_checks_passed;

	UNS_32	rv;

	rv = 0;

	// 2/2/2015 ajv : If the range check fails, we've decided to correct the value and then
	// continue processing as if that was value that was passed in.
	lrange_checks_passed = (preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED);
	lrange_checks_passed += RangeCheck_uint32( &pdls.month, 1, 12, pdefault_month, NULL, pfield_name_month );
	lrange_checks_passed += RangeCheck_uint32( &pdls.min_day, 0, 31, pdefault_min_day, NULL, pfield_name_month );
	lrange_checks_passed += RangeCheck_uint32( &pdls.max_day, 0, 31, pdefault_max_day, NULL, pfield_name_month );

	// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
	// return value is set properly so the change bits are set and the calling function knows
	// it's ok to use the value.
	if( (memcmp( pvar_to_change_ptr, &pdls, sizeof(DAYLIGHT_SAVING_TIME_STRUCT) ) != 0) || (lrange_checks_passed != 0) )
	{
		#ifndef _MSC_VER

			if( pgenerate_change_line_bool == (true) )
			{
				// 11/25/2013 ajv : TODO Generate change line
			}

			memcpy( pvar_to_change_ptr, &pdls, sizeof(DAYLIGHT_SAVING_TIME_STRUCT) );

			rv = 1;

		#else

			SQL_MERGE_build_upsert_uint32( pdls.month, SQL_MERGE_include_field_name_in_insert_clause, pfield_name_month, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );
			SQL_MERGE_build_upsert_uint32( pdls.min_day, SQL_MERGE_include_field_name_in_insert_clause, pfield_name_min_day, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );
			SQL_MERGE_build_upsert_uint32( pdls.max_day, SQL_MERGE_include_field_name_in_insert_clause, pfield_name_max_day, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

		#endif
	}

	#ifndef _MSC_VER

	// 5/12/2014 ajv : If the value was actually changed OR the change was
	// made from the slave and uploaded to the master, set the change bits.
	// This second condition is important because changes made directly at
	// the master are sent to itself as if from a slave. In this case, this
	// routine is called during the _extract_and_store_from_comm routine but
	// the values are identical. However, we still need to distribute those
	// changes to the rest of the chain.
	if( (rv == (1)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
	{
		SHARED_set_32_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
	}

	#endif

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 SHARED_set_bool_group( void *const pgroup,
									  BOOL_32 *pvar_to_change_ptr,
									  BOOL_32 pnew_value,
									  const BOOL_32 pdefault,
									  const BOOL_32 pgenerate_change_line_bool,
									  const UNS_32 pchange_line_index,
									  const UNS_32 preason_for_change,
									  const UNS_32 pbox_index_0,
									  const char *pfield_name
									  #ifdef _MSC_VER
									  , char *pmerge_update_str,
									  char *pmerge_insert_fields_str,
									  char *pmerge_insert_values_str
									  #endif
									)
{
	BOOL_32	lrange_check_passed;

	BOOL_32 rv;

	rv = (false);

	if( pgroup != NULL )
	{
		// 2/2/2015 ajv : If the range check fails, we've decided to correct the value and then
		// continue processing as if that was value that was passed in.
		lrange_check_passed = ((preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED) && (RangeCheck_bool( &pnew_value, pdefault, nm_GROUP_get_name( pgroup ), pfield_name )));

		#ifndef	_MSC_VER

			// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
			// return value is set properly so the change bits are set and the calling function knows
			// it's ok to use the value.
			if( (*pvar_to_change_ptr != pnew_value) || (!lrange_check_passed) )
			{
				if( pgenerate_change_line_bool == (true) )
				{
					Alert_ChangeLine_Group( pchange_line_index, nm_GROUP_get_name( pgroup ), pvar_to_change_ptr, &pnew_value, sizeof( pnew_value ), preason_for_change, pbox_index_0 );
				}

				*pvar_to_change_ptr = pnew_value;

				rv = (true);
			}

		#else

			SQL_MERGE_build_upsert_bool( pnew_value, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

			rv = (true);

		#endif
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_func_call_with_null_ptr();

		#endif
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_bool_with_32_bit_change_bits_group( void *const pgroup,
															  BOOL_32 *pvar_to_change_ptr,
															  BOOL_32 pnew_value,
															  const BOOL_32 pdefault,
															  const BOOL_32 pgenerate_change_line_bool,
															  const UNS_32 pchange_line_index,
															  const UNS_32 preason_for_change,
															  const UNS_32 pbox_index_0,
															  const BOOL_32 pset_change_bits,
															  CHANGE_BITS_32_BIT *pchange_bits_for_controller,
															  CHANGE_BITS_32_BIT *pchange_bits_for_central,
															  UNS_32 pchange_bit_to_set,
															  const char *pfield_name
															  #ifdef _MSC_VER
															  , char *pmerge_update_str,
															  char *pmerge_insert_fields_str,
															  char *pmerge_insert_values_str
															  #endif
															)
{
	BOOL_32 rv;

	rv = SHARED_set_bool_group( pgroup,
							    pvar_to_change_ptr,
							    pnew_value,
							    pdefault,
							    pgenerate_change_line_bool,
							    pchange_line_index,
							    preason_for_change,
							    pbox_index_0,
							    pfield_name
								#ifdef _MSC_VER
								, pmerge_update_str,
								pmerge_insert_fields_str,
								pmerge_insert_values_str
								#endif
							  );

	#ifndef _MSC_VER

		// 5/12/2014 ajv : If the value was actually changed OR the change was
		// made from the slave and uploaded to the master, set the change bits.
		// This second condition is important because changes made directly at
		// the master are sent to itself as if from a slave. In this case, this
		// routine is called during the _extract_and_store_from_comm routine but
		// the values are identical. However, we still need to distribute those
		// changes to the rest of the chain.
		if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
		{
			SHARED_set_32_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
		}

	#endif

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_bool_with_64_bit_change_bits_group( void *const pgroup,
															  BOOL_32 *pvar_to_change_ptr,
															  BOOL_32 pnew_value,
															  const BOOL_32 pdefault,
															  const BOOL_32 pgenerate_change_line_bool,
															  const UNS_32 pchange_line_index,
															  const UNS_32 preason_for_change,
															  const UNS_32 pbox_index_0,
															  const BOOL_32 pset_change_bits,
															  CHANGE_BITS_64_BIT *pchange_bits_for_controller,
															  CHANGE_BITS_64_BIT *pchange_bits_for_central,
															  UNS_32 pchange_bit_to_set,
															  const char *pfield_name
															  #ifdef _MSC_VER
															  , char *pmerge_update_str,
															  char *pmerge_insert_fields_str,
															  char *pmerge_insert_values_str
															  #endif
															)
{
	BOOL_32 rv;

	rv = SHARED_set_bool_group( pgroup,
							    pvar_to_change_ptr,
							    pnew_value,
							    pdefault,
							    pgenerate_change_line_bool,
							    pchange_line_index,
							    preason_for_change,
							    pbox_index_0,
							    pfield_name
								#ifdef _MSC_VER
								, pmerge_update_str,
								pmerge_insert_fields_str,
								pmerge_insert_values_str
								#endif
							  );

	#ifndef _MSC_VER

		// 5/12/2014 ajv : If the value was actually changed OR the change was
		// made from the slave and uploaded to the master, set the change bits.
		// This second condition is important because changes made directly at
		// the master are sent to itself as if from a slave. In this case, this
		// routine is called during the _extract_and_store_from_comm routine but
		// the values are identical. However, we still need to distribute those
		// changes to the rest of the chain.
		if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
		{
			SHARED_set_64_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
		}

	#endif

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 SHARED_set_uint32_group( void *const pgroup,
										UNS_32 *pvar_to_change_ptr,
										UNS_32 pnew_value,
										const UNS_32 pmin,
										const UNS_32 pmax,
										const UNS_32 pdefault,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 pchange_line_index,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const char *pfield_name
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
									  )
{
	BOOL_32	lrange_check_passed;

	BOOL_32 rv;

	rv = (false);

	if( pgroup != NULL )
	{
		// 2/2/2015 ajv : If the range check fails, we've decided to correct the value and then
		// continue processing as if that was value that was passed in.
		lrange_check_passed = ((preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED) && (RangeCheck_uint32( &pnew_value, pmin, pmax, pdefault, nm_GROUP_get_name( pgroup ), pfield_name )));

		#ifndef _MSC_VER

			// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
			// return value is set properly so the change bits are set and the calling function knows
			// it's ok to use the value.
			if( (*pvar_to_change_ptr != pnew_value) || (!lrange_check_passed) )
			{
				if( pgenerate_change_line_bool == (true) )
				{
					Alert_ChangeLine_Group( pchange_line_index, nm_GROUP_get_name( pgroup ), pvar_to_change_ptr, &pnew_value, sizeof(pnew_value), preason_for_change, pbox_index_0 );
				}

				*pvar_to_change_ptr = pnew_value;

				rv = (true);
			}

		#else

			SQL_MERGE_build_upsert_uint32( pnew_value, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

			rv = (true);

		#endif
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_func_call_with_null_ptr();

		#endif
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_uint32_with_32_bit_change_bits_group( void *const pgroup,
																UNS_32 *pvar_to_change_ptr,
																UNS_32 pnew_value,
																const UNS_32 pmin,
																const UNS_32 pmax,
																const UNS_32 pdefault,
																const BOOL_32 pgenerate_change_line_bool,
																const UNS_32 pchange_line_index,
																const UNS_32 preason_for_change,
																const UNS_32 pbox_index_0,
																const BOOL_32 pset_change_bits,
																CHANGE_BITS_32_BIT *pchange_bits_for_controller,
																CHANGE_BITS_32_BIT *pchange_bits_for_central,
																UNS_32 pchange_bit_to_set,
																const char *pfield_name
																#ifdef _MSC_VER
																, char *pmerge_update_str,
																char *pmerge_insert_fields_str,
																char *pmerge_insert_values_str
																#endif
															  )
{
	BOOL_32 rv;

	rv = SHARED_set_uint32_group( pgroup,
								  pvar_to_change_ptr,
								  pnew_value,
								  pmin,
								  pmax,
								  pdefault,
								  pgenerate_change_line_bool,
								  pchange_line_index,
								  preason_for_change,
								  pbox_index_0,
								  pfield_name
								  #ifdef _MSC_VER
								  , pmerge_update_str,
								  pmerge_insert_fields_str,
								  pmerge_insert_values_str
								  #endif
								);

	#ifndef _MSC_VER

		// 5/12/2014 ajv : If the value was actually changed OR the change was
		// made from the slave and uploaded to the master, set the change bits.
		// This second condition is important because changes made directly at
		// the master are sent to itself as if from a slave. In this case, this
		// routine is called during the _extract_and_store_from_comm routine but
		// the values are identical. However, we still need to distribute those
		// changes to the rest of the chain.
		if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
		{
			SHARED_set_32_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
		}

	#endif

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_uint32_with_64_bit_change_bits_group( void *const pgroup,
																UNS_32 *pvar_to_change_ptr,
																UNS_32 pnew_value,
																const UNS_32 pmin,
																const UNS_32 pmax,
																const UNS_32 pdefault,
																const BOOL_32 pgenerate_change_line_bool,
																const UNS_32 pchange_line_index,
																const UNS_32 preason_for_change,
																const UNS_32 pbox_index_0,
																const BOOL_32 pset_change_bits,
																CHANGE_BITS_64_BIT *pchange_bits_for_controller,
																CHANGE_BITS_64_BIT *pchange_bits_for_central,
																UNS_32 pchange_bit_to_set,
																const char *pfield_name
																#ifdef _MSC_VER
																, char *pmerge_update_str,
																char *pmerge_insert_fields_str,
																char *pmerge_insert_values_str
																#endif
															  )
{
	BOOL_32 rv;

	rv = SHARED_set_uint32_group( pgroup,
								  pvar_to_change_ptr,
								  pnew_value,
								  pmin,
								  pmax,
								  pdefault,
								  pgenerate_change_line_bool,
								  pchange_line_index,
								  preason_for_change,
								  pbox_index_0,
								  pfield_name
								  #ifdef _MSC_VER
								  , pmerge_update_str,
								  pmerge_insert_fields_str,
								  pmerge_insert_values_str
								  #endif
								);

	#ifndef _MSC_VER

		// 5/12/2014 ajv : If the value was actually changed OR the change was
		// made from the slave and uploaded to the master, set the change bits.
		// This second condition is important because changes made directly at
		// the master are sent to itself as if from a slave. In this case, this
		// routine is called during the _extract_and_store_from_comm routine but
		// the values are identical. However, we still need to distribute those
		// changes to the rest of the chain.
		if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
		{
			SHARED_set_64_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
		}

	#endif

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 SHARED_set_int32_group( void *const pgroup,
									   INT_32 *pvar_to_change_ptr,
									   INT_32 pnew_value,
									   const INT_32 pmin,
									   const INT_32 pmax,
									   const INT_32 pdefault,
									   const BOOL_32 pgenerate_change_line_bool,
									   const UNS_32 pchange_line_index,
									   const UNS_32 preason_for_change,
									   const UNS_32 pbox_index_0,
									   const char *pfield_name
									   #ifdef _MSC_VER
									   , char *pmerge_update_str,
									   char *pmerge_insert_fields_str,
									   char *pmerge_insert_values_str
									   #endif
									 )
{
	BOOL_32	lrange_check_passed;

	BOOL_32 rv;

	rv = (false);

	if( pgroup != NULL )
	{
		// 2/2/2015 ajv : If the range check fails, we've decided to correct the value and then
		// continue processing as if that was value that was passed in.
		lrange_check_passed = ((preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED) && (RangeCheck_int32( &pnew_value, pmin, pmax, pdefault, nm_GROUP_get_name( pgroup ), pfield_name )));

		#ifndef _MSC_VER

			// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
			// return value is set properly so the change bits are set and the calling function knows
			// it's ok to use the value.
			if( (*pvar_to_change_ptr != pnew_value) || (!lrange_check_passed) )
			{
				if( pgenerate_change_line_bool == (true) )
				{
					Alert_ChangeLine_Group( pchange_line_index, nm_GROUP_get_name( pgroup ), pvar_to_change_ptr, &pnew_value, sizeof(pnew_value), preason_for_change, pbox_index_0 );
				}

				*pvar_to_change_ptr = pnew_value;

				rv = (true);
			}

		#else

			SQL_MERGE_build_upsert_int32( pnew_value, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

			rv = (true);

		#endif
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_func_call_with_null_ptr();

		#endif
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_int32_with_32_bit_change_bits_group( void *const pgroup,
															   INT_32 *pvar_to_change_ptr,
															   INT_32 pnew_value,
															   const INT_32 pmin,
															   const INT_32 pmax,
															   const INT_32 pdefault,
															   const BOOL_32 pgenerate_change_line_bool,
															   const UNS_32 pchange_line_index,
															   const UNS_32 preason_for_change,
															   const UNS_32 pbox_index_0,
															   const BOOL_32 pset_change_bits,
															   CHANGE_BITS_32_BIT *pchange_bits_for_controller,
															   CHANGE_BITS_32_BIT *pchange_bits_for_central,
															   UNS_32 pchange_bit_to_set,
															   const char *pfield_name
															   #ifdef _MSC_VER
															   , char *pmerge_update_str,
															   char *pmerge_insert_fields_str,
															   char *pmerge_insert_values_str
															   #endif
															 )
{
	BOOL_32 rv;

	rv = SHARED_set_int32_group( pgroup,
								 pvar_to_change_ptr,
								 pnew_value,
								 pmin,
								 pmax,
								 pdefault,
								 pgenerate_change_line_bool,
								 pchange_line_index,
								 preason_for_change,
								 pbox_index_0,
								 pfield_name
								 #ifdef _MSC_VER
								 , pmerge_update_str,
								 pmerge_insert_fields_str,
								 pmerge_insert_values_str
								 #endif
							   );

	#ifndef _MSC_VER

		// 5/12/2014 ajv : If the value was actually changed OR the change was
		// made from the slave and uploaded to the master, set the change bits.
		// This second condition is important because changes made directly at
		// the master are sent to itself as if from a slave. In this case, this
		// routine is called during the _extract_and_store_from_comm routine but
		// the values are identical. However, we still need to distribute those
		// changes to the rest of the chain.
		if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
		{
			SHARED_set_32_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
		}

	#endif

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 SHARED_set_float_group(	void *const pgroup,
										float *pvar_to_change_ptr,
										float pnew_value,
										const float pmin,
										const float pmax,
										const float pdefault,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 pchange_line_index,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const char *pfield_name
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
									  )
{
	BOOL_32	lrange_check_passed;

	BOOL_32 rv;

	rv = (false);

	if( pgroup != NULL )
	{
		// 2/2/2015 ajv : If the range check fails, we've decided to correct the value and then
		// continue processing as if that was value that was passed in.
		lrange_check_passed = ((preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED) && (RangeCheck_float( &pnew_value, pmin, pmax, pdefault, nm_GROUP_get_name( pgroup ), pfield_name )));

		#ifndef _MSC_VER

			// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
			// return value is set properly so the change bits are set and the calling function knows
			// it's ok to use the value.
			if( (*pvar_to_change_ptr != pnew_value) || (!lrange_check_passed) )
			{
				if( pgenerate_change_line_bool == (true) )
				{
					Alert_ChangeLine_Group( pchange_line_index, nm_GROUP_get_name( pgroup ), pvar_to_change_ptr, &pnew_value, sizeof(pnew_value), preason_for_change, pbox_index_0 );
				}

				*pvar_to_change_ptr = pnew_value;

				rv = (true);
			}

		#else

			SQL_MERGE_build_upsert_float( pnew_value, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

			rv = (true);

		#endif
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_func_call_with_null_ptr();

		#endif
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_float_with_32_bit_change_bits_group(	void *const pgroup,
																float *pvar_to_change_ptr,
																float pnew_value,
																const float pmin,
																const float pmax,
																const float pdefault,
																const BOOL_32 pgenerate_change_line_bool,
																const UNS_32 pchange_line_index,
																const UNS_32 preason_for_change,
																const UNS_32 pbox_index_0,
																const BOOL_32 pset_change_bits,
																CHANGE_BITS_32_BIT *pchange_bits_for_controller,
																CHANGE_BITS_32_BIT *pchange_bits_for_central,
																UNS_32 pchange_bit_to_set,
																const char *pfield_name
																#ifdef _MSC_VER
																, char *pmerge_update_str,
																char *pmerge_insert_fields_str,
																char *pmerge_insert_values_str
																#endif
															  )
{
	BOOL_32 rv;

	rv = SHARED_set_float_group(	pgroup,
									pvar_to_change_ptr,
									pnew_value,
									pmin,
									pmax,
									pdefault,
									pgenerate_change_line_bool,
									pchange_line_index,
									preason_for_change,
									pbox_index_0,
									pfield_name
									#ifdef _MSC_VER
									, pmerge_update_str,
									pmerge_insert_fields_str,
									pmerge_insert_values_str
									#endif
								);

	#ifndef _MSC_VER

		// 5/12/2014 ajv : If the value was actually changed OR the change was
		// made from the slave and uploaded to the master, set the change bits.
		// This second condition is important because changes made directly at
		// the master are sent to itself as if from a slave. In this case, this
		// routine is called during the _extract_and_store_from_comm routine but
		// the values are identical. However, we still need to distribute those
		// changes to the rest of the chain.
		if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
		{
			SHARED_set_32_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
		}

	#endif

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 SHARED_set_date_group( void *const pgroup,
									  UNS_32 *pvar_to_change_ptr,
									  UNS_32 pnew_value,
									  const UNS_32 pmin,
									  const UNS_32 pmax,
									  const UNS_32 pdefault,
									  const BOOL_32 pgenerate_change_line_bool,
									  const UNS_32 pchange_line_index,
									  const UNS_32 preason_for_change,
									  const UNS_32 pbox_index_0,
									  const char *pfield_name
									  #ifdef _MSC_VER
									  , char *pmerge_update_str,
									  char *pmerge_insert_fields_str,
									  char *pmerge_insert_values_str
									  #endif
									)
{
	BOOL_32	lrange_check_passed;

	BOOL_32		rv;

	rv = (false);

	if( pgroup != NULL )
	{
		lrange_check_passed = ( preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED );

		if( pnew_value < pmin )
		{
			pnew_value = pmin;

			// Don't generate a range check error - this is a common occurrence
		}
		else if ( pnew_value > pmax )
		{
			// 2/2/2015 ajv : If the range check fails, we've decided to correct the value and then
			// continue processing as if that was value that was passed in.
			pnew_value = pdefault;

			lrange_check_passed = (false);

			#ifndef _MSC_VER

				#if ALERT_INCLUDE_FILENAME
					Alert_range_check_failed_date_with_filename( pfield_name, nm_GROUP_get_name( pgroup ), pnew_value, CS_FILE_NAME( __FILE__ ), __LINE__ );
				#else
					Alert_range_check_failed_date_without_filename( pfield_name, nm_GROUP_get_name( pgroup ), pnew_value );
				#endif

			#endif
		}

		#ifndef _MSC_VER

			// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
			// return value is set properly so the change bits are set and the calling function knows
			// it's ok to use the value.
			if( (*pvar_to_change_ptr != pnew_value) || (!lrange_check_passed) )
			{
				if( pgenerate_change_line_bool == (true) )
				{
					Alert_ChangeLine_Group( pchange_line_index, nm_GROUP_get_name( pgroup ), pvar_to_change_ptr, &pnew_value, sizeof(pnew_value), preason_for_change, pbox_index_0 );
				}

				*pvar_to_change_ptr = pnew_value;

				rv = (true);
			}

		#else

			SQL_MERGE_build_upsert_date( pnew_value, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

			rv = (true);

		#endif
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_func_call_with_null_ptr();

		#endif
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_date_with_32_bit_change_bits_group( void *const pgroup,
															  UNS_32 *pvar_to_change_ptr,
															  UNS_32 pnew_value,
															  const UNS_32 pmin,
															  const UNS_32 pmax,
															  const UNS_32 pdefault,
															  const BOOL_32 pgenerate_change_line_bool,
															  const UNS_32 pchange_line_index,
															  const UNS_32 preason_for_change,
															  const UNS_32 pbox_index_0,
															  const BOOL_32 pset_change_bits,
															  CHANGE_BITS_32_BIT *pchange_bits_for_controller,
															  CHANGE_BITS_32_BIT *pchange_bits_for_central,
															  UNS_32 pchange_bit_to_set,
															  const char *pfield_name
															  #ifdef _MSC_VER
															  , char *pmerge_update_str,
															  char *pmerge_insert_fields_str,
															  char *pmerge_insert_values_str
															  #endif
															)
{
	BOOL_32 rv;

	rv = SHARED_set_date_group( pgroup,
								pvar_to_change_ptr,
								pnew_value,
								pmin,
								pmax,
								pdefault,
								pgenerate_change_line_bool,
								pchange_line_index,
								preason_for_change,
								pbox_index_0,
								pfield_name
								#ifdef _MSC_VER
								, pmerge_update_str,
								pmerge_insert_fields_str,
								pmerge_insert_values_str
								#endif
							  );

	#ifndef _MSC_VER

		// 5/12/2014 ajv : If the value was actually changed OR the change was
		// made from the slave and uploaded to the master, set the change bits.
		// This second condition is important because changes made directly at
		// the master are sent to itself as if from a slave. In this case, this
		// routine is called during the _extract_and_store_from_comm routine but
		// the values are identical. However, we still need to distribute those
		// changes to the rest of the chain.
		if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
		{
			SHARED_set_32_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
		}

	#endif

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_date_with_64_bit_change_bits_group( void *const pgroup,
															  UNS_32 *pvar_to_change_ptr,
															  UNS_32 pnew_value,
															  const UNS_32 pmin,
															  const UNS_32 pmax,
															  const UNS_32 pdefault,
															  const BOOL_32 pgenerate_change_line_bool,
															  const UNS_32 pchange_line_index,
															  const UNS_32 preason_for_change,
															  const UNS_32 pbox_index_0,
															  const BOOL_32 pset_change_bits,
															  CHANGE_BITS_64_BIT *pchange_bits_for_controller,
															  CHANGE_BITS_64_BIT *pchange_bits_for_central,
															  UNS_32 pchange_bit_to_set,
															  const char *pfield_name
															  #ifdef _MSC_VER
															  , char *pmerge_update_str,
															  char *pmerge_insert_fields_str,
															  char *pmerge_insert_values_str
															  #endif
															)
{
	BOOL_32 rv;

	rv = SHARED_set_date_group( pgroup,
								pvar_to_change_ptr,
								pnew_value,
								pmin,
								pmax,
								pdefault,
								pgenerate_change_line_bool,
								pchange_line_index,
								preason_for_change,
								pbox_index_0,
								pfield_name
								#ifdef _MSC_VER
								, pmerge_update_str,
								pmerge_insert_fields_str,
								pmerge_insert_values_str
								#endif
							  );

	#ifndef _MSC_VER

		// 5/12/2014 ajv : If the value was actually changed OR the change was
		// made from the slave and uploaded to the master, set the change bits.
		// This second condition is important because changes made directly at
		// the master are sent to itself as if from a slave. In this case, this
		// routine is called during the _extract_and_store_from_comm routine but
		// the values are identical. However, we still need to distribute those
		// changes to the rest of the chain.
		if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
		{
			SHARED_set_64_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
		}

	#endif

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 SHARED_set_time_group( void *const pgroup,
									  UNS_32 *pvar_to_change_ptr,
									  UNS_32 pnew_value,
									  const UNS_32 pmin,
									  const UNS_32 pmax,
									  const UNS_32 pdefault,
									  const BOOL_32 pgenerate_change_line_bool,
									  const UNS_32 pchange_line_index,
									  const UNS_32 preason_for_change,
									  const UNS_32 pbox_index_0,
									  const char *pfield_name
									  #ifdef _MSC_VER
									  , char *pmerge_update_str,
									  char *pmerge_insert_fields_str,
									  char *pmerge_insert_values_str
									  #endif
									)
{
	BOOL_32	lrange_check_passed;

	BOOL_32		rv;

	rv = (false);

	if( pgroup != NULL )
	{
		// 2/2/2015 ajv : If the range check fails, we've decided to correct the value and then
		// continue processing as if that was value that was passed in.
		lrange_check_passed = ((preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED) && (RangeCheck_time( &pnew_value, pmin, pmax, pdefault, nm_GROUP_get_name( pgroup ), pfield_name )));

		#ifndef _MSC_VER

			// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
			// return value is set properly so the change bits are set and the calling function knows
			// it's ok to use the value.
			if( (*pvar_to_change_ptr != pnew_value) || (!lrange_check_passed) )
			{
				if( pgenerate_change_line_bool == (true) )
				{
					Alert_ChangeLine_Group( pchange_line_index, nm_GROUP_get_name( pgroup ), pvar_to_change_ptr, &pnew_value, sizeof(pnew_value), preason_for_change, pbox_index_0 );
				}

				*pvar_to_change_ptr = pnew_value;

				rv = (true);
			}

		#else

			// 1/20/2015 ajv : Start and Stop times are stored as integers in the database. Therefore,
			// make sure we use the uint32 routine rather than the time routine. Otherwise, a Native
			// Error 2124 (invalid operand) will be generated.
			SQL_MERGE_build_upsert_uint32( pnew_value, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

			rv = (true);

		#endif
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_func_call_with_null_ptr();

		#endif
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_time_with_64_bit_change_bits_group( void *const pgroup,
															  UNS_32 *pvar_to_change_ptr,
															  UNS_32 pnew_value,
															  const UNS_32 pmin,
															  const UNS_32 pmax,
															  const UNS_32 pdefault,
															  const BOOL_32 pgenerate_change_line_bool,
															  const UNS_32 pchange_line_index,
															  const UNS_32 preason_for_change,
															  const UNS_32 pbox_index_0,
															  const BOOL_32 pset_change_bits,
															  CHANGE_BITS_64_BIT *pchange_bits_for_controller,
															  CHANGE_BITS_64_BIT *pchange_bits_for_central,
															  UNS_32 pchange_bit_to_set,
															  const char *pfield_name
															  #ifdef _MSC_VER
															  , char *pmerge_update_str,
															  char *pmerge_insert_fields_str,
															  char *pmerge_insert_values_str
															  #endif
															)
{
	BOOL_32 rv;

	rv = SHARED_set_time_group( pgroup,
								pvar_to_change_ptr,
								pnew_value,
								pmin,
								pmax,
								pdefault,
								pgenerate_change_line_bool,
								pchange_line_index,
								preason_for_change,
								pbox_index_0,
								pfield_name
								#ifdef _MSC_VER
								, pmerge_update_str,
								pmerge_insert_fields_str,
								pmerge_insert_values_str
								#endif
							  );

	#ifndef _MSC_VER

		// 5/12/2014 ajv : If the value was actually changed OR the change was
		// made from the slave and uploaded to the master, set the change bits.
		// This second condition is important because changes made directly at
		// the master are sent to itself as if from a slave. In this case, this
		// routine is called during the _extract_and_store_from_comm routine but
		// the values are identical. However, we still need to distribute those
		// changes to the rest of the chain.
		if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
		{
			SHARED_set_64_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
		}

	#endif

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 SHARED_set_date_time_group( void *const pgroup,
										   DATE_TIME *pvar_to_change_ptr,
										   DATE_TIME pnew_value,
										   const DATE_TIME pmin,
										   const DATE_TIME pmax,
										   const DATE_TIME pdefault,
										   const BOOL_32 pgenerate_change_line_bool,
										   const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const char *pfield_name
										   #ifdef _MSC_VER
										   , char *pmerge_update_str,
										   char *pmerge_insert_fields_str,
										   char *pmerge_insert_values_str
										   #endif
										 )
{
	BOOL_32	lrange_check_passed;

	BOOL_32	rv;

	rv = (false);

	if( pgroup != NULL )
	{
		// 1/9/2015 ajv : TODO Range check passed-in new date/time value
		lrange_check_passed = ( preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED );

		#ifndef _MSC_VER

			// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
			// return value is set properly so the change bits are set and the calling function knows
			// it's ok to use the value.
			if( (DT1_IsEqualTo_DT2( pvar_to_change_ptr, &pnew_value ) == (false)) || (!lrange_check_passed) )
			{
				memcpy( &pnew_value, pvar_to_change_ptr, sizeof(DATE_TIME) );

				rv = (true);
			}

		#else

			SQL_MERGE_build_upsert_timestamp( pnew_value, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

			rv = (true);

		#endif
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_func_call_with_null_ptr();

		#endif
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_date_time_with_64_bit_change_bits_group( void *const pgroup,
																   DATE_TIME *pvar_to_change_ptr,
																   DATE_TIME pnew_value,
																   const DATE_TIME pmin,
																   const DATE_TIME pmax,
																   const DATE_TIME pdefault,
																   const BOOL_32 pgenerate_change_line_bool,
																   const UNS_32 preason_for_change,
																   const UNS_32 pbox_index_0,
																   const BOOL_32 pset_change_bits,
																   CHANGE_BITS_64_BIT *pchange_bits_for_controller,
																   CHANGE_BITS_64_BIT *pchange_bits_for_central,
																   UNS_32 pchange_bit_to_set,
																   const char *pfield_name
																   #ifdef _MSC_VER
																   , char *pmerge_update_str,
																   char *pmerge_insert_fields_str,
																   char *pmerge_insert_values_str
																   #endif
																 )
{
	BOOL_32 rv;

	rv = SHARED_set_date_time_group( pgroup,
									 pvar_to_change_ptr,
									 pnew_value,
									 pmin,
									 pmax,
									 pdefault,
									 pgenerate_change_line_bool,
									 preason_for_change,
									 pbox_index_0,
									 pfield_name
									 #ifdef _MSC_VER
									 , pmerge_update_str,
									 pmerge_insert_fields_str,
									 pmerge_insert_values_str
									 #endif
								   );

	#ifndef _MSC_VER

		// 5/12/2014 ajv : If the value was actually changed OR the change was
		// made from the slave and uploaded to the master, set the change bits.
		// This second condition is important because changes made directly at
		// the master are sent to itself as if from a slave. In this case, this
		// routine is called during the _extract_and_store_from_comm routine but
		// the values are identical. However, we still need to distribute those
		// changes to the rest of the chain.
		if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
		{
			SHARED_set_64_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
		}

	#endif

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_on_at_a_time_group( void *const pgroup,
											  UNS_32 *pvar_to_change_ptr,
											  UNS_32 pnew_value,
											  const UNS_32 pmin,
											  const UNS_32 pmax,
											  const UNS_32 pdefault,
											  const BOOL_32 pgenerate_change_line_bool,
											  const UNS_32 pchange_line_index,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_64_BIT *pchange_bits_for_controller,
											  CHANGE_BITS_64_BIT *pchange_bits_for_central,
											  UNS_32 pchange_bit_to_set,
											  const char *pfield_name
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
										    )
{
	BOOL_32	lrange_check_passed;

	BOOL_32 rv;

	rv = (false);

	if( pgroup != NULL )
	{
		lrange_check_passed = ( preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED );

		// 2012.05.23 ajv : Since "X" is stored using a so-called magic number, don't range check if
		// it's "X"
		if( pnew_value != ON_AT_A_TIME_X )
		{
			// 2/2/2015 ajv : If the range check fails, we've decided to correct the value and then
			// continue processing as if that was value that was passed in.
			lrange_check_passed = ((lrange_check_passed) && (RangeCheck_uint32( &pnew_value, pmin, pmax, pdefault, nm_GROUP_get_name( pgroup ), pfield_name )));
		}

		#ifndef	_MSC_VER

			// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
			// return value is set properly so the change bits are set and the calling function knows
			// it's ok to use the value.
			if( (*pvar_to_change_ptr != pnew_value) || (!lrange_check_passed) )
			{
				if( pgenerate_change_line_bool == (true) )
				{
					Alert_ChangeLine_Group( pchange_line_index, nm_GROUP_get_name( pgroup ), pvar_to_change_ptr, &pnew_value, sizeof(pnew_value), preason_for_change, pbox_index_0 );
				}

				*pvar_to_change_ptr = pnew_value;

				rv = (true);
			}

			// 12/15/2014 ajv : If the value was actually changed OR the change was made from the slave
			// and uploaded to the master, set the change bits. This second condition is important
			// because changes made directly at the master are sent to itself as if from a slave. In
			// this case, this routine is called during the _extract_and_store_from_comm routine but
			// the values are identical. However, we still need to distribute those changes to the rest
			// of the chain.
			if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
			{
				SHARED_set_64_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
			}

		#else

			SQL_MERGE_build_upsert_uint32( pnew_value, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

			rv = (true);

		#endif
	}
	else
	{
		#ifndef _MSC_VER

			Alert_func_call_with_null_ptr();

		#endif
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 SHARED_set_name( void *pgroup,
								char *pName,
								const BOOL_32 pgenerate_change_line_bool,
								const UNS_32 preason_for_change,
								const UNS_32 pbox_index_0
							    #ifdef _MSC_VER
								, const char *pfield_name,
							    char *pmerge_update_str,
							    char *pmerge_insert_fields_str,
							    char *pmerge_insert_values_str
							    #endif
							  )
{
	GROUP_BASE_DEFINITION_STRUCT	*lbase_definition;

	BOOL_32	lrange_check_passed;

	BOOL_32	rv;

	rv = (false);

	// 2012.05.11 ajv : We're comparing pgroup against NULL instead of using the
	// nm_OnList routine because this generic function doesn't have any
	// knowledge about the list the group is supposed to be on. In fact, the
	// logic leading up to this function call has already verified the group is
	// on the list - this is simply a sanity check to ensure the memory location
	// hasn't been corrupted.
	if( pgroup != NULL )
	{
		lbase_definition = (GROUP_BASE_DEFINITION_STRUCT*)pgroup;

		// 2/2/2015 ajv : If the range check fails, we've decided to correct the value and then
		// continue processing as if that was value that was passed in.
		lrange_check_passed = ((preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED) && (RangeCheck_string( pName, NUMBER_OF_CHARS_IN_A_NAME, "Group name" )));

		#ifndef _MSC_VER

			// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
			// return value is set properly so the change bits are set and the calling function knows
			// it's ok to use the value.
			if( (strncmp(lbase_definition->description, pName, sizeof(lbase_definition->description)) != 0) || (!lrange_check_passed) )
			{
				if( pgenerate_change_line_bool == (true) )
				{
					// TODO Add the group name to the alert?
					//Alert_ChangeLine_Group( CHANGE_GROUP_NAME, nm_GROUP_get_name( pgroup ), (char*)lBaseDefinition->description, (char*)pName, NUMBER_OF_CHARS_IN_A_NAME, preason_for_change, pbox_index_0 );
					Alert_ChangeLine_Group( CHANGE_GROUP_NAME, pName, NULL, NULL, NUMBER_OF_CHARS_IN_A_NAME, preason_for_change, pbox_index_0 );
				}

				if( strlcpy(lbase_definition->description, pName, sizeof(lbase_definition->description)) >= sizeof(lbase_definition->description) )
				{
					// Overflow
					Alert_string_too_long( nm_GROUP_get_name( pgroup ), sizeof(lbase_definition->description) );
				}

				rv = (true);
			}

		#else

			SQL_MERGE_build_upsert_string( pName, NUMBER_OF_CHARS_IN_A_NAME, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

			rv = (true);

		#endif
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_func_call_with_null_ptr();

		#endif
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_name_32_bit_change_bits( void *pgroup,
												   char *pName,
												   const BOOL_32 pgenerate_change_line_bool,
												   const UNS_32 preason_for_change,
												   const UNS_32 pbox_index_0,
												   const BOOL_32 pset_change_bits,
												   CHANGE_BITS_32_BIT *pchange_bits_for_controller,
												   CHANGE_BITS_32_BIT *pchange_bits_for_central,
												   const UNS_32 pchange_bit_to_set
												   #ifdef _MSC_VER
												   , const char *pfield_name,
												   char *pmerge_update_str,
												   char *pmerge_insert_fields_str,
												   char *pmerge_insert_values_str
												   #endif
												 )
{
	BOOL_32	rv;

	rv = SHARED_set_name( pgroup,
						  pName,
						  pgenerate_change_line_bool,
						  preason_for_change,
						  pbox_index_0
						  #ifdef _MSC_VER
						  , pfield_name,
						  pmerge_update_str,
						  pmerge_insert_fields_str,
						  pmerge_insert_values_str
						  #endif
						);

	#ifndef _MSC_VER

		// 5/12/2014 ajv : If the value was actually changed OR the change was
		// made from the slave and uploaded to the master, set the change bits.
		// This second condition is important because changes made directly at
		// the master are sent to itself as if from a slave. In this case, this
		// routine is called during the _extract_and_store_from_comm routine but
		// the values are identical. However, we still need to distribute those
		// changes to the rest of the chain.
		if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
		{
			SHARED_set_32_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
		}

	#endif

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_name_64_bit_change_bits( void *pgroup,
												   char *pName,
												   const BOOL_32 pgenerate_change_line_bool,
												   const UNS_32 preason_for_change,
												   const UNS_32 pbox_index_0,
												   const BOOL_32 pset_change_bits,
												   CHANGE_BITS_64_BIT *pchange_bits_for_controller,
												   CHANGE_BITS_64_BIT *pchange_bits_for_central,
												   const UNS_32 pchange_bit_to_set
												   #ifdef _MSC_VER
												   , const char *pfield_name,
												   char *pmerge_update_str,
												   char *pmerge_insert_fields_str,
												   char *pmerge_insert_values_str
												   #endif
												 )
{
	BOOL_32	rv;

	rv = SHARED_set_name( pgroup,
						  pName,
						  pgenerate_change_line_bool,
						  preason_for_change,
						  pbox_index_0
						  #ifdef _MSC_VER
						  , pfield_name,
						  pmerge_update_str,
						  pmerge_insert_fields_str,
						  pmerge_insert_values_str
						  #endif
						);

	#ifndef _MSC_VER

		// 5/12/2014 ajv : If the value was actually changed OR the change was
		// made from the slave and uploaded to the master, set the change bits.
		// This second condition is important because changes made directly at
		// the master are sent to itself as if from a slave. In this case, this
		// routine is called during the _extract_and_store_from_comm routine but
		// the values are identical. However, we still need to distribute those
		// changes to the rest of the chain.
		if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
		{
			SHARED_set_64_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
		}

	#endif

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Retrieves a descriptive string for a station. If the station description is
 * unique, that is what's used. Otherwise, it displays "Station" followed by the
 * station number.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the COMM MNGR task when new
 * station programming is received.
 * 
 * @param pstation A pointer to the station.
 * 
 * @param pstr A pointer to the string to store the text in.
 * 
 * @return char* The descriptive string.
 *
 * @author 9/19/2013 AdrianusV
 *
 * @revisions (none)
 */
static char *STATION_get_descriptive_string( STATION_STRUCT *const pstation, char *pstr )
{
	#ifndef _MSC_VER

		UNS_32	lstation_number_0;

		UNS_32	lbox_index_0;

		char	str_48[ 48 ];

		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		if( nm_OnList( &station_info_list_hdr, pstation ) == (true) )
		{
			lstation_number_0 = nm_STATION_get_station_number_0( pstation );

			lbox_index_0 = nm_STATION_get_box_index_0( pstation );

			if( strncmp( nm_GROUP_get_name( pstation ), STATION_DEFAULT_DESCRIPTION, NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
			{
				snprintf( pstr, NUMBER_OF_CHARS_IN_A_NAME, "%s %s", GuiLib_GetTextPtr( GuiStruct_txtStation_0, 0 ), STATION_get_station_number_string( lbox_index_0, lstation_number_0, (char*)str_48, sizeof(str_48) ) );
			}
			else
			{
				strlcpy( pstr, nm_GROUP_get_name( pstation ), NUMBER_OF_CHARS_IN_A_NAME );
			}
		}

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	#else

		strlcpy( pstr, nm_GROUP_get_name( pstation ), NUMBER_OF_CHARS_IN_A_NAME );

	#endif

	return( pstr );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_bool_station( STATION_STRUCT *const pstation,
										BOOL_32 *pvar_to_change_ptr,
										BOOL_32 pnew_value,
										const BOOL_32 pdefault,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 pchange_line_index,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const BOOL_32 pset_change_bits,
										CHANGE_BITS_32_BIT *pchange_bits_for_controller,
										CHANGE_BITS_32_BIT *pchange_bits_for_central,
										UNS_32 pchange_bit_to_set,
										const char *pfield_name
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
									   )
{
	char str_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	BOOL_32	lrange_check_passed;

	BOOL_32 rv;

	rv = (false);

	if( pstation != NULL )
	{
		// 2/2/2015 ajv : If the range check fails, we've decided to correct the value and then
		// continue processing as if that was value that was passed in.
		lrange_check_passed = ((preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED) && (RangeCheck_bool( &pnew_value, pdefault, STATION_get_descriptive_string( pstation, str_48 ), pfield_name )));

		#ifndef _MSC_VER

			// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
			// return value is set properly so the change bits are set and the calling function knows
			// it's ok to use the value.
			if( (*pvar_to_change_ptr != pnew_value) || (!lrange_check_passed) )
			{
				if( pgenerate_change_line_bool == (true) )
				{
					Alert_ChangeLine_Station( pchange_line_index, nm_STATION_get_box_index_0( pstation ), nm_STATION_get_station_number_0( pstation ), pvar_to_change_ptr, &pnew_value, sizeof( pnew_value ), preason_for_change, pbox_index_0 );
				}

				*pvar_to_change_ptr = pnew_value;

				rv = (true);
			}

			// 5/12/2014 ajv : If the value was actually changed OR the change was
			// made from the slave and uploaded to the master, set the change bits.
			// This second condition is important because changes made directly at
			// the master are sent to itself as if from a slave. In this case, this
			// routine is called during the _extract_and_store_from_comm routine but
			// the values are identical. However, we still need to distribute those
			// changes to the rest of the chain.
			if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
			{
				SHARED_set_32_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
			}

		#else

			SQL_MERGE_build_upsert_bool( pnew_value, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

			rv = (true);

		#endif
	}
	else
	{
		#ifndef _MSC_VER

			Alert_func_call_with_null_ptr();

		#endif
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_uint8_station( STATION_STRUCT *const pstation,
										 UNS_8 *pvar_to_change_ptr,
										 UNS_8 pnew_value,
										 const UNS_8 pmin,
										 const UNS_8 pmax,
										 const UNS_8 pdefault,
										 const BOOL_32 pgenerate_change_line_bool,
										 const UNS_32 pchange_line_index,
										 const UNS_32 preason_for_change,
										 const UNS_32 pbox_index_0,
										 const BOOL_32 pset_change_bits,
										 CHANGE_BITS_32_BIT *pchange_bits_for_controller,
										 CHANGE_BITS_32_BIT *pchange_bits_for_central,
										 UNS_32 pchange_bit_to_set,
										 const char *pfield_name
										 #ifdef _MSC_VER
										 , char *pmerge_update_str,
										 char *pmerge_insert_fields_str,
										 char *pmerge_insert_values_str
										 #endif
									   )
{
	char str_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	BOOL_32	lrange_check_passed;

	BOOL_32 rv;

	rv = (false);

	if( pstation != NULL )
	{
		// 2/2/2015 ajv : If the range check fails, we've decided to correct the value and then
		// continue processing as if that was value that was passed in.
		lrange_check_passed = ((preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED) && (RangeCheck_uns8( &pnew_value, pmin, pmax, pdefault, STATION_get_descriptive_string( pstation, str_48 ), pfield_name )));

		#ifndef _MSC_VER

			// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
			// return value is set properly so the change bits are set and the calling function knows
			// it's ok to use the value.
			if( (*pvar_to_change_ptr != pnew_value) || (!lrange_check_passed) )
			{
				if( pgenerate_change_line_bool == (true) )
				{
					Alert_ChangeLine_Station( pchange_line_index, nm_STATION_get_box_index_0( pstation ), nm_STATION_get_station_number_0( pstation ), pvar_to_change_ptr, &pnew_value, sizeof( pnew_value ), preason_for_change, pbox_index_0 );
				}

				*pvar_to_change_ptr = pnew_value;

				rv = (true);
			}

			// 5/12/2014 ajv : If the value was actually changed OR the change was
			// made from the slave and uploaded to the master, set the change bits.
			// This second condition is important because changes made directly at
			// the master are sent to itself as if from a slave. In this case, this
			// routine is called during the _extract_and_store_from_comm routine but
			// the values are identical. However, we still need to distribute those
			// changes to the rest of the chain.
			if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
			{
				SHARED_set_32_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
			}

		#else

			SQL_MERGE_build_upsert_bool( pnew_value, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

			rv = (true);

		#endif
	}
	else
	{
		#ifndef _MSC_VER

			Alert_func_call_with_null_ptr();

		#endif
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_uint32_station( STATION_STRUCT *const pstation,
										  UNS_32 *pvar_to_change_ptr,
										  UNS_32 pnew_value,
										  const UNS_32 pmin,
										  const UNS_32 pmax,
										  const UNS_32 pdefault,
										  const BOOL_32 pgenerate_change_line_bool,
										  const UNS_32 pchange_line_index,
										  const UNS_32 preason_for_change,
										  const UNS_32 pbox_index_0,
										  const BOOL_32 pset_change_bits,
										  CHANGE_BITS_32_BIT *pchange_bits_for_controller,
										  CHANGE_BITS_32_BIT *pchange_bits_for_central,
										  UNS_32 pchange_bit_to_set,
										  const char *pfield_name
										  #ifdef _MSC_VER
										  , char *pmerge_update_str,
										  char *pmerge_insert_fields_str,
										  char *pmerge_insert_values_str
										  #endif
										)
{
	char str_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	BOOL_32	lrange_check_passed;

	BOOL_32 rv;

	rv = (false);

	if( pstation != NULL )
	{
		// 2/2/2015 ajv : If the range check fails, we've decided to correct the value and then
		// continue processing as if that was value that was passed in.
		lrange_check_passed = ((preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED) && (RangeCheck_uint32( &pnew_value, pmin, pmax, pdefault, STATION_get_descriptive_string( pstation, str_48 ), pfield_name )));

		#ifndef _MSC_VER

			// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
			// return value is set properly so the change bits are set and the calling function knows
			// it's ok to use the value.
			if( (*pvar_to_change_ptr != pnew_value) || (!lrange_check_passed) )
			{
				if( pgenerate_change_line_bool == (true) )
				{
					Alert_ChangeLine_Station( pchange_line_index, nm_STATION_get_box_index_0( pstation ), nm_STATION_get_station_number_0( pstation ), pvar_to_change_ptr, &pnew_value, sizeof(pnew_value), preason_for_change, pbox_index_0 );
				}

				*pvar_to_change_ptr = pnew_value;

				rv = (true);
			}

			// 5/12/2014 ajv : If the value was actually changed OR the change was
			// made from the slave and uploaded to the master, set the change bits.
			// This second condition is important because changes made directly at
			// the master are sent to itself as if from a slave. In this case, this
			// routine is called during the _extract_and_store_from_comm routine but
			// the values are identical. However, we still need to distribute those
			// changes to the rest of the chain.
			if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
			{
				SHARED_set_32_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
			}

		#else

			SQL_MERGE_build_upsert_uint32( pnew_value, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

			rv = (true);

		#endif
	}
	else
	{
		#ifndef _MSC_VER

			Alert_func_call_with_null_ptr();

		#endif
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_float_station( STATION_STRUCT *const pstation,
										 float *pvar_to_change_ptr,
										 float pnew_value,
										 const float pmin,
										 const float pmax,
										 const float pdefault,
										 const BOOL_32 pgenerate_change_line_bool,
										 const UNS_32 pchange_line_index,
										 const UNS_32 preason_for_change,
										 const UNS_32 pbox_index_0,
										 const BOOL_32 pset_change_bits,
										 CHANGE_BITS_32_BIT *pchange_bits_for_controller,
										 CHANGE_BITS_32_BIT *pchange_bits_for_central,
										 UNS_32 pchange_bit_to_set,
										 const char *pfield_name
										 #ifdef _MSC_VER
										 , char *pmerge_update_str,
										 char *pmerge_insert_fields_str,
										 char *pmerge_insert_values_str
										 #endif
									   )
{
	char str_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	BOOL_32	lrange_check_passed;

	BOOL_32 rv;

	rv = (false);

	if( pstation != NULL )
	{
		// 2/2/2015 ajv : If the range check fails, we've decided to correct the value and then
		// continue processing as if that was value that was passed in.
		lrange_check_passed = ((preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED) && (RangeCheck_float( &pnew_value, pmin, pmax, pdefault, STATION_get_descriptive_string( pstation, str_48 ), pfield_name )));

		#ifndef _MSC_VER

			// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
			// return value is set properly so the change bits are set and the calling function knows
			// it's ok to use the value.
			if( (*pvar_to_change_ptr != pnew_value) || (!lrange_check_passed) )
			{
				if( pgenerate_change_line_bool == (true) )
				{
					Alert_ChangeLine_Station( pchange_line_index, nm_STATION_get_box_index_0( pstation ), nm_STATION_get_station_number_0( pstation ), pvar_to_change_ptr, &pnew_value, sizeof(pnew_value), preason_for_change, pbox_index_0 );
				}

				*pvar_to_change_ptr = pnew_value;

				rv = (true);
			}

			// 5/12/2014 ajv : If the value was actually changed OR the change was
			// made from the slave and uploaded to the master, set the change bits.
			// This second condition is important because changes made directly at
			// the master are sent to itself as if from a slave. In this case, this
			// routine is called during the _extract_and_store_from_comm routine but
			// the values are identical. However, we still need to distribute those
			// changes to the rest of the chain.
			if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
			{
				SHARED_set_32_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
			}

		#else

			SQL_MERGE_build_upsert_float( pnew_value, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

			rv = (true);

		#endif
	}
	else
	{
		#ifndef _MSC_VER

			Alert_func_call_with_null_ptr();

		#endif
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_set_GID_station( STATION_STRUCT *const pstation,
									   UNS_32 *pvar_to_change_ptr,
									   UNS_32 pnew_GID,
									   const UNS_32 plast_GID,
									   const UNS_32 preason_for_change,
									   const UNS_32 pbox_index_0,
									   const BOOL_32 pset_change_bits,
									   CHANGE_BITS_32_BIT *pchange_bits_for_controller,
									   CHANGE_BITS_32_BIT *pchange_bits_for_central,
									   UNS_32 pchange_bit_to_set,
									   const char *pfield_name
									   #ifdef _MSC_VER
									   , char *pmerge_update_str,
									   char *pmerge_insert_fields_str,
									   char *pmerge_insert_values_str
									   #endif
									 )
{
	char str_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	BOOL_32	lrange_check_passed;

	BOOL_32 rv;

	rv = (false);

	if( pstation != NULL )
	{
		// 2/2/2015 ajv : If the range check fails, we've decided to correct the value and then
		// continue processing as if that was value that was passed in.
		lrange_check_passed = ((preason_for_change != CHANGE_REASON_RANGE_CHECK_FAILED) && (RangeCheck_uint32( &pnew_GID, 0, plast_GID, 0, STATION_get_descriptive_string( pstation, str_48 ), pfield_name )));

		#ifndef _MSC_VER

			// 2/10/2015 ajv : If the range check fails, we still want to fall into here to ensure the
			// return value is set properly so the change bits are set and the calling function knows
			// it's ok to use the value.
			if( (*pvar_to_change_ptr != pnew_GID) || (!lrange_check_passed) )
			{
				*pvar_to_change_ptr = pnew_GID;

				rv = (true);
			}

			// 5/12/2014 ajv : If the value was actually changed OR the change was
			// made from the slave and uploaded to the master, set the change bits.
			// This second condition is important because changes made directly at
			// the master are sent to itself as if from a slave. In this case, this
			// routine is called during the _extract_and_store_from_comm routine but
			// the values are identical. However, we still need to distribute those
			// changes to the rest of the chain.
			if( (rv == (true)) || (preason_for_change == CHANGE_REASON_CHANGED_AT_SLAVE) )
			{
				SHARED_set_32_bit_change_bits( pchange_bits_for_controller, pchange_bits_for_central, pchange_bit_to_set, preason_for_change, pset_change_bits );
			}

		#else

			SQL_MERGE_build_upsert_uint32( pnew_GID, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str );

			rv = (true);

		#endif
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_func_call_with_null_ptr();

		#endif
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static BOOL_32 set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error( void )
{
	BOOL_32		rv;
	
	rv = (true);
	
	// ----------
	
	// 11/1/2018 rmd : AHHH, except if we are a SLAVE on a chain, DO NOT set the CHANGE_BITS.
	// Why? The field has been experiencing an interesting but devastating problem when the
	// master is building up a new chain (distributing the files from the master following the
	// clean house) and there are communication problems that lead to a failed sync. Check this
	// scenario out: during the sync there is a lost packet (think radio) and that packet
	// happens to contain the STATION GROUPS. Then the STATIONS arrive and for various reasons
	// (one being to calculate the estimated run time when in ET) the GET STATION GROUP function
	// is called. Well if that station belongs to a station group that doesn't exist, the OUT OF
	// RANGE here would be called, the result of which is the station is removed from the
	// station group, because we set the GID to 0. And then the WORST part, if we set the change
	// bits, the removel from the station group is sent back to the MASTER, and eventually
	// sync'd with the central. And the Station Group assignment for a station can be lost
	// forever!
	//
	// So, if you were able to understand all that, you'd know then that this phenomenon can
	// only happen at the slaves (only slaves do a CLEAN). So if we prevent the change bits from
	// being SET at the slaves for any OOR condition, the value will be fixed locally, and not
	// sent to the master. And the GOOD part, we then give our FILE CRC testing a chance to
	// detect the errant Station Group file, and the Stations file too. And BOTH are do
	// subsequently get fixed as the files crc's fail.
	//
	// 11/6/2018 rmd : ANOTHER THING TO NOTE, we only need to use this function in the GET
	// functions. In other words, when there is an attempt to use a value, and it goes out of
	// range, we will correct it locally, but not distriubte that change to the master. What we
	// are trying to fix is OOR errors that occur due to a messed up sync, and all the SET
	// functions that receive the data, via a token in irri_comm.c, already have their flag set
	// (false) to not set the change bits. It's the get functions that we need to suppress
	// setting the flag for. They would be used subsequent to receiving the data, and it's those
	// errors we want to let the chain_sync fix. (I feel pretty nervous about this whole scheme,
	// it seems to work, and normal keypad changes made at the slave will sync properly to the
	// master, but I betcha there is a hole in the scheme somewhere - but I do think it is an
	// improvement to where we were at).
	if( FLOWSENSE_we_are_a_slave_in_a_chain() )
	{
		rv = (false);
	}

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_get_bool( BOOL_32 *pvalue,
								const BOOL_32 pdefault,
								void (*pset_func_ptr)(BOOL_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
								CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
								const char *const pvar_name )
{
	// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
	// the value by setting it to the default.
	if( RangeCheck_bool( pvalue, pdefault, NULL, pvar_name ) == (false) )
	{
		if( pset_func_ptr != NULL )
		{
			// 2/2/2015 ajv : We fixed the value, so now trigger the set routine to actually trigger the
			// process to set and sync the variable.
			//
			// 11/6/2018 rmd : Prevent setting the change bits/sync if we are a slave. See write up
			// accompanying the set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error
			// function.
			pset_func_ptr( *pvalue, CHANGE_do_not_generate_change_line, CHANGE_REASON_RANGE_CHECK_FAILED, FLOWSENSE_get_controller_index(), set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error(), pchange_bitfield_to_set );
		}
	}
	
	return( *pvalue );
}

/* ---------------------------------------------------------- */
extern UNS_32 SHARED_get_uint32( UNS_32 *pvalue,
								 const UNS_32 pmin,
								 const UNS_32 pmax,
								 const UNS_32 pdefault,
								 void (*pset_func_ptr)(UNS_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
								 CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
								 const char *const pvar_name )
{
	// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
	// the value by setting it to the default.
	if( RangeCheck_uint32( pvalue, pmin, pmax, pdefault, NULL, pvar_name ) == (false) )
	{
		if( pset_func_ptr != NULL )
		{
			// 2/2/2015 ajv : We fixed the value, so now trigger the set routine to actually trigger the
			// process to set and sync the variable.
			//
			// 11/6/2018 rmd : Prevent setting the change bits/sync if we are a slave. See write up
			// accompanying the set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error
			// function.
			pset_func_ptr( *pvalue, CHANGE_do_not_generate_change_line, CHANGE_REASON_RANGE_CHECK_FAILED, FLOWSENSE_get_controller_index(), set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error(), pchange_bitfield_to_set );
		}
	}

	return( *pvalue );
}

/* ---------------------------------------------------------- */
extern INT_32 SHARED_get_int32( INT_32 *pvalue,
								const INT_32 pmin,
								const INT_32 pmax,
								const INT_32 pdefault,
								void (*pset_func_ptr)(INT_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
								CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
								const char *const pvar_name )
{
	// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
	// the value by setting it to the default.
	if( RangeCheck_int32( pvalue, pmin, pmax, pdefault, NULL, pvar_name ) == (false) )
	{
		if( pset_func_ptr != NULL )
		{
			// 2/2/2015 ajv : We fixed the value, so now trigger the set routine to actually trigger the
			// process to set and sync the variable.
			//
			// 11/6/2018 rmd : Prevent setting the change bits/sync if we are a slave. See write up
			// accompanying the set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error
			// function.
			pset_func_ptr( *pvalue, CHANGE_do_not_generate_change_line, CHANGE_REASON_RANGE_CHECK_FAILED, FLOWSENSE_get_controller_index(), set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error(), pchange_bitfield_to_set );
		}
	}

	return( *pvalue );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_get_bool_from_array( BOOL_32 *pvalue,
										   const UNS_32 pindex,
										   const UNS_32 parray_size,
										   const BOOL_32 pdefault,
										   void (*pset_func_ptr)(BOOL_32 pvalue, const UNS_32 pindex, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
										   CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
										   const char *const pvar_name )
{
	if( pindex < parray_size )
	{
		// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
		// the value by setting it to the default.
		if( RangeCheck_bool( pvalue, pdefault, NULL, pvar_name ) == (false) )
		{
			if( pset_func_ptr != NULL )
			{
				// 2/2/2015 ajv : We fixed the value, so now trigger the set routine to actually trigger the
				// process to set and sync the variable.
				//
				// 11/6/2018 rmd : Prevent setting the change bits/sync if we are a slave. See write up
				// accompanying the set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error
				// function.
				pset_func_ptr( *pvalue, pindex, CHANGE_do_not_generate_change_line, CHANGE_REASON_RANGE_CHECK_FAILED, FLOWSENSE_get_controller_index(), set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error(), pchange_bitfield_to_set );
			}
		}
	}
	else
	{
		Alert_index_out_of_range();

		*pvalue = pdefault;
	}

	return( *pvalue );
}

/* ---------------------------------------------------------- */
extern UNS_32 SHARED_get_uint32_from_array( UNS_32 *pvalue,
											const UNS_32 pindex,
											const UNS_32 parray_size,
											const UNS_32 pmin,
											const UNS_32 pmax,
											const UNS_32 pdefault,
											void (*pset_func_ptr)(UNS_32 pvalue, const UNS_32 pindex, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
											CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
											const char *const pvar_name )
{
	if( pindex < parray_size )
	{
		// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
		// the value by setting it to the default.
		if( RangeCheck_uint32( pvalue, pmin, pmax, pdefault, NULL, pvar_name ) == (false) )
		{
			if( pset_func_ptr != NULL )
			{
				// 2/2/2015 ajv : We fixed the value, so now trigger the set routine to actually trigger the
				// process to set and sync the variable.
				//
				// 11/6/2018 rmd : Prevent setting the change bits/sync if we are a slave. See write up
				// accompanying the set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error
				// function.
				pset_func_ptr( *pvalue, pindex, CHANGE_do_not_generate_change_line, CHANGE_REASON_RANGE_CHECK_FAILED, FLOWSENSE_get_controller_index(), set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error(), pchange_bitfield_to_set );
			}
		}
	}
	else
	{
		Alert_index_out_of_range();

		*pvalue = pdefault;
	}

	return( *pvalue );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_get_bool_32_bit_change_bits_group( void *const pgroup,
														 BOOL_32 *pvalue,
														 const BOOL_32 pdefault,
														 void (*pset_func_ptr)(void *const pgroup, BOOL_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
														 CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
														 const char *const pvar_name )
{
	if( pgroup != NULL )
	{
		// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
		// the value by setting it to the default.
		if( RangeCheck_bool( pvalue, pdefault, ((GROUP_BASE_DEFINITION_STRUCT*)pgroup)->description, pvar_name ) == (false) )
		{
			if( pset_func_ptr != NULL )
			{
				// 2/2/2015 ajv : We fixed the value, so now trigger the set routine to actually trigger the
				// process to set and sync the variable.
				//
				// 11/6/2018 rmd : Prevent setting the change bits/sync if we are a slave. See write up
				// accompanying the set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error
				// function.
				pset_func_ptr( pgroup, *pvalue, CHANGE_do_not_generate_change_line, CHANGE_REASON_RANGE_CHECK_FAILED, FLOWSENSE_get_controller_index(), set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error(), pchange_bitfield_to_set );
			}
		}
	}
	else
	{
		Alert_group_not_found();

		*pvalue = pdefault;
	}

	return( *pvalue );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_get_bool_64_bit_change_bits_group( void *const pgroup,
														 BOOL_32 *pvalue,
														 const BOOL_32 pdefault,
														 void (*pset_func_ptr)(void *const pgroup, BOOL_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_64_BIT *pchange_bitfield_to_set),
														 CHANGE_BITS_64_BIT *pchange_bitfield_to_set,
														 const char *const pvar_name )
{
	if( pgroup != NULL )
	{
		// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
		// the value by setting it to the default.
		if( RangeCheck_bool( pvalue, pdefault, ((GROUP_BASE_DEFINITION_STRUCT*)pgroup)->description, pvar_name ) == (false) )
		{
			if( pset_func_ptr != NULL )
			{
				// 2/2/2015 ajv : We fixed the value, so now trigger the set routine to actually trigger the
				// process to set and sync the variable.
				//
				// 11/6/2018 rmd : Prevent setting the change bits/sync if we are a slave. See write up
				// accompanying the set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error
				// function.
				pset_func_ptr( pgroup, *pvalue, CHANGE_do_not_generate_change_line, CHANGE_REASON_RANGE_CHECK_FAILED, FLOWSENSE_get_controller_index(), set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error(), pchange_bitfield_to_set );
			}
		}
	}
	else
	{
		Alert_group_not_found();

		*pvalue = pdefault;
	}

	return( *pvalue );
}

/* ---------------------------------------------------------- */
extern UNS_32 SHARED_get_uint32_32_bit_change_bits_group( void *const pgroup,
														  UNS_32 *pvalue,
														  const UNS_32 pmin,
														  const UNS_32 pmax,
														  const UNS_32 pdefault,
														  void (*pset_func_ptr)(void *const pgroup, UNS_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
														  CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
														  const char *const pvar_name )
{
	if( pgroup != NULL )
	{
		// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
		// the value by setting it to the default.
		if( RangeCheck_uint32( pvalue, pmin, pmax, pdefault, ((GROUP_BASE_DEFINITION_STRUCT*)pgroup)->description, pvar_name ) == (false) )
		{
			if( pset_func_ptr != NULL )
			{
				// 2/2/2015 ajv : We fixed the value, so now trigger the set routine to actually trigger the
				// process to set and sync the variable.
				//
				// 11/6/2018 rmd : Prevent setting the change bits/sync if we are a slave. See write up
				// accompanying the set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error
				// function.
				pset_func_ptr( pgroup, *pvalue, CHANGE_do_not_generate_change_line, CHANGE_REASON_RANGE_CHECK_FAILED, FLOWSENSE_get_controller_index(), set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error(), pchange_bitfield_to_set );
			}
		}
	}
	else
	{
		Alert_group_not_found();

		*pvalue = pdefault;
	}

	return( *pvalue );
}

/* ---------------------------------------------------------- */
extern UNS_32 SHARED_get_uint32_64_bit_change_bits_group( void *const pgroup,
														  UNS_32 *pvalue,
														  const UNS_32 pmin,
														  const UNS_32 pmax,
														  const UNS_32 pdefault,
														  void (*pset_func_ptr)(void *const pgroup, UNS_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_64_BIT *pchange_bitfield_to_set),
														  CHANGE_BITS_64_BIT *pchange_bitfield_to_set,
														  const char *const pvar_name )
{
	if( pgroup != NULL )
	{
		// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
		// the value by setting it to the default.
		if( RangeCheck_uint32( pvalue, pmin, pmax, pdefault, ((GROUP_BASE_DEFINITION_STRUCT*)pgroup)->description, pvar_name ) == (false) )
		{
			if( pset_func_ptr != NULL )
			{
				// 2/2/2015 ajv : We fixed the value, so now trigger the set routine to actually trigger the
				// process to set and sync the variable.
				//
				// 11/6/2018 rmd : Prevent setting the change bits/sync if we are a slave. See write up
				// accompanying the set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error
				// function.
				pset_func_ptr( pgroup, *pvalue, CHANGE_do_not_generate_change_line, CHANGE_REASON_RANGE_CHECK_FAILED, FLOWSENSE_get_controller_index(), set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error(), pchange_bitfield_to_set );
			}
		}
	}
	else
	{
		Alert_group_not_found();

		*pvalue = pdefault;
	}

	return( *pvalue );
}

/* ---------------------------------------------------------- */
extern INT_32 SHARED_get_int32_32_bit_change_bits_group( void *const pgroup,
														 INT_32 *pvalue,
														 const INT_32 pmin,
														 const INT_32 pmax,
														 const INT_32 pdefault,
														 void (*pset_func_ptr)(void *const pgroup, INT_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
														 CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
														 const char *const pvar_name )
{
	if( pgroup != NULL )
	{
		// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
		// the value by setting it to the default.
		if( RangeCheck_int32( pvalue, pmin, pmax, pdefault, ((GROUP_BASE_DEFINITION_STRUCT*)pgroup)->description, pvar_name ) == (false) )
		{
			if( pset_func_ptr != NULL )
			{
				// 2/2/2015 ajv : We fixed the value, so now trigger the set routine to actually trigger the
				// process to set and sync the variable.
				//
				// 11/6/2018 rmd : Prevent setting the change bits/sync if we are a slave. See write up
				// accompanying the set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error
				// function.
				pset_func_ptr( pgroup, *pvalue, CHANGE_do_not_generate_change_line, CHANGE_REASON_RANGE_CHECK_FAILED, FLOWSENSE_get_controller_index(), set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error(), pchange_bitfield_to_set );
			}
		}
	}
	else
	{
		Alert_group_not_found();

		*pvalue = pdefault;
	}

	return( *pvalue );
}

/* ---------------------------------------------------------- */
extern INT_32 SHARED_get_int32_64_bit_change_bits_group( void *const pgroup,
														 INT_32 *pvalue,
														 const INT_32 pmin,
														 const INT_32 pmax,
														 const INT_32 pdefault,
														 void (*pset_func_ptr)(void *const pgroup, INT_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_64_BIT *pchange_bitfield_to_set),
														 CHANGE_BITS_64_BIT *pchange_bitfield_to_set,
														 const char *const pvar_name )
{
	if( pgroup != NULL )
	{
		// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
		// the value by setting it to the default.
		if( RangeCheck_int32( pvalue, pmin, pmax, pdefault, ((GROUP_BASE_DEFINITION_STRUCT*)pgroup)->description, pvar_name ) == (false) )
		{
			if( pset_func_ptr != NULL )
			{
				// 2/2/2015 ajv : We fixed the value, so now trigger the set routine to actually trigger the
				// process to set and sync the variable.
				//
				// 11/6/2018 rmd : Prevent setting the change bits/sync if we are a slave. See write up
				// accompanying the set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error
				// function.
				pset_func_ptr( pgroup, *pvalue, CHANGE_do_not_generate_change_line, CHANGE_REASON_RANGE_CHECK_FAILED, FLOWSENSE_get_controller_index(), set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error(), pchange_bitfield_to_set );
			}
		}
	}
	else
	{
		Alert_group_not_found();

		*pvalue = pdefault;
	}

	return( *pvalue );
}

/* ---------------------------------------------------------- */
extern float SHARED_get_float_32_bit_change_bits_group( void *const pgroup,
														float *pvalue,
														const float pmin,
														const float pmax,
														const float pdefault,
														void (*pset_func_ptr)(void *const pgroup, float pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
														CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
														const char *const pvar_name )
{
	if( pgroup != NULL )
	{
		// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
		// the value by setting it to the default.
		if( RangeCheck_float( pvalue, pmin, pmax, pdefault, ((GROUP_BASE_DEFINITION_STRUCT*)pgroup)->description, pvar_name ) == (false) )
		{
			if( pset_func_ptr != NULL )
			{
				// 2/2/2015 ajv : We fixed the value, so now trigger the set routine to actually trigger the
				// process to set and sync the variable.
				//
				// 11/6/2018 rmd : Prevent setting the change bits/sync if we are a slave. See write up
				// accompanying the set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error
				// function.
				pset_func_ptr( pgroup, *pvalue, CHANGE_do_not_generate_change_line, CHANGE_REASON_RANGE_CHECK_FAILED, FLOWSENSE_get_controller_index(), set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error(), pchange_bitfield_to_set );
			}
		}
	}
	else
	{
		Alert_group_not_found();

		*pvalue = pdefault;
	}

	return( *pvalue );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_get_bool_from_array_32_bit_change_bits_group( void *const pgroup,
																	BOOL_32 *pvalue,
																	const UNS_32 pindex,
																	const UNS_32 parray_size,
																	const BOOL_32 pdefault,
																	void (*pset_func_ptr)(void *const pgroup, BOOL_32 pvalue, const UNS_32 pindex, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
																	CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
																	const char *const pvar_name )
{
	if( pgroup != NULL )
	{
		if( pindex < parray_size )
		{
			// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
			// the value by setting it to the default.
			if( RangeCheck_bool( pvalue, pdefault, ((GROUP_BASE_DEFINITION_STRUCT*)pgroup)->description, pvar_name ) == (false) )
			{
				if( pset_func_ptr != NULL )
				{
					// 2/2/2015 ajv : We fixed the value, so now trigger the set routine to actually trigger the
					// process to set and sync the variable.
					//
					// 11/6/2018 rmd : Prevent setting the change bits/sync if we are a slave. See write up
					// accompanying the set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error
					// function.
					pset_func_ptr( pgroup, *pvalue, pindex, CHANGE_do_not_generate_change_line, CHANGE_REASON_RANGE_CHECK_FAILED, FLOWSENSE_get_controller_index(), set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error(), pchange_bitfield_to_set );
				}
			}
		}
		else
		{
			Alert_index_out_of_range();

			*pvalue = pdefault;
		}
	}
	else
	{
		Alert_group_not_found();

		*pvalue = pdefault;
	}

	return( *pvalue );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SHARED_get_bool_from_array_64_bit_change_bits_group( void *const pgroup,
																	BOOL_32 *pvalue,
																	const UNS_32 pindex,
																	const UNS_32 parray_size,
																	const BOOL_32 pdefault,
																	void (*pset_func_ptr)(void *const pgroup, BOOL_32 pvalue, const UNS_32 pindex, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_64_BIT *pchange_bitfield_to_set),
																	CHANGE_BITS_64_BIT *pchange_bitfield_to_set,
																	const char *const pvar_name )
{
	if( pgroup != NULL )
	{
		if( pindex < parray_size )
		{
			// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
			// the value by setting it to the default.
			if( RangeCheck_bool( pvalue, pdefault, ((GROUP_BASE_DEFINITION_STRUCT*)pgroup)->description, pvar_name ) == (false) )
			{
				if( pset_func_ptr != NULL )
				{
					// 2/2/2015 ajv : We fixed the value, so now trigger the set routine to actually trigger the
					// process to set and sync the variable.
					//
					// 11/6/2018 rmd : Prevent setting the change bits/sync if we are a slave. See write up
					// accompanying the set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error
					// function.
					pset_func_ptr( pgroup, *pvalue, pindex, CHANGE_do_not_generate_change_line, CHANGE_REASON_RANGE_CHECK_FAILED, FLOWSENSE_get_controller_index(), set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error(), pchange_bitfield_to_set );
				}
			}
		}
		else
		{
			Alert_index_out_of_range();

			*pvalue = pdefault;
		}
	}
	else
	{
		Alert_group_not_found();

		*pvalue = pdefault;
	}

	return( *pvalue );
}

/* ---------------------------------------------------------- */
extern UNS_32 SHARED_get_uint32_from_array_32_bit_change_bits_group( void *const pgroup,
																	 const UNS_32 pindex,
																	 UNS_32 *pvalue,
																	 const UNS_32 parray_size,
																	 const UNS_32 pmin,
																	 const UNS_32 pmax,
																	 const UNS_32 pdefault,
																	 void (*pset_func_ptr)(void *const pgroup, const UNS_32 pindex, UNS_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
																	 CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
																	 const char *const pvar_name )
{
	if( pgroup != NULL )
	{
		if( pindex < parray_size )
		{
			// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
			// the value by setting it to the default.
			if( RangeCheck_uint32( pvalue, pmin, pmax, pdefault, ((GROUP_BASE_DEFINITION_STRUCT*)pgroup)->description, pvar_name ) == (false) )
			{
				if( pset_func_ptr != NULL )
				{
					// 2/2/2015 ajv : We fixed the value, so now trigger the set routine to actually trigger the
					// process to set and sync the variable.
					//
					// 11/6/2018 rmd : Prevent setting the change bits/sync if we are a slave. See write up
					// accompanying the set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error
					// function.
					pset_func_ptr( pgroup, pindex, *pvalue, CHANGE_do_not_generate_change_line, CHANGE_REASON_RANGE_CHECK_FAILED, FLOWSENSE_get_controller_index(), set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error(), pchange_bitfield_to_set );
				}
			}
		}
		else
		{
			Alert_index_out_of_range();

			*pvalue = pdefault;
		}
	}
	else
	{
		Alert_group_not_found();

		*pvalue = pdefault;
	}

	return( *pvalue );
}

/* ---------------------------------------------------------- */
extern INT_32 SHARED_get_int32_from_array_32_bit_change_bits_group( void *const pgroup,
																	const UNS_32 pindex,
																	INT_32 *pvalue,
																	const UNS_32 parray_size,
																	const UNS_32 pmin,
																	const UNS_32 pmax,
																	const UNS_32 pdefault,
																	void (*pset_func_ptr)(void *const pgroup, const UNS_32 pindex, INT_32 pvalue, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_32_BIT *pchange_bitfield_to_set),
																	CHANGE_BITS_32_BIT *pchange_bitfield_to_set,
																	const char *const pvar_name )
{
	if( pgroup != NULL )
	{
		if( pindex < parray_size )
		{
			// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
			// the value by setting it to the default.
			if( RangeCheck_int32( pvalue, pmin, pmax, pdefault, ((GROUP_BASE_DEFINITION_STRUCT*)pgroup)->description, pvar_name ) == (false) )
			{
				if( pset_func_ptr != NULL )
				{
					// 2/2/2015 ajv : We fixed the value, so now trigger the set routine to actually trigger the
					// process to set and sync the variable.
					//
					// 11/6/2018 rmd : Prevent setting the change bits/sync if we are a slave. See write up
					// accompanying the set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error
					// function.
					pset_func_ptr( pgroup, pindex, *pvalue, CHANGE_do_not_generate_change_line, CHANGE_REASON_RANGE_CHECK_FAILED, FLOWSENSE_get_controller_index(), set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error(), pchange_bitfield_to_set );
				}
			}
		}
		else
		{
			Alert_index_out_of_range();

			*pvalue = pdefault;
		}
	}
	else
	{
		Alert_group_not_found();

		*pvalue = pdefault;
	}

	return( *pvalue );
}

/* ---------------------------------------------------------- */
extern UNS_32 SHARED_get_uint32_from_array_64_bit_change_bits_group( void *const pgroup,
																	 UNS_32 *pvalue,
																	 const UNS_32 pindex,
																	 const UNS_32 parray_size,
																	 const UNS_32 pmin,
																	 const UNS_32 pmax,
																	 const UNS_32 pdefault,
																	 void (*pset_func_ptr)(void *const pgroup, UNS_32 pvalue, const UNS_32 pindex, const BOOL_32 pgenerate_change_line_bool, const UNS_32 preason_for_change, const UNS_32 pbox_index_0, const BOOL_32 pset_change_bits, CHANGE_BITS_64_BIT *pchange_bitfield_to_set),
																	 CHANGE_BITS_64_BIT *pchange_bitfield_to_set,
																	 const char *const pvar_name )
{
	if( pgroup != NULL )
	{
		if( pindex < parray_size )
		{
			// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
			// the value by setting it to the default.
			if( RangeCheck_uint32( pvalue, pmin, pmax, pdefault, ((GROUP_BASE_DEFINITION_STRUCT*)pgroup)->description, pvar_name ) == (false) )
			{
				if( pset_func_ptr != NULL )
				{
					// 2/2/2015 ajv : We fixed the value, so now trigger the set routine to actually trigger the
					// process to set and sync the variable.
					//
					// 11/6/2018 rmd : Prevent setting the change bits/sync if we are a slave. See write up
					// accompanying the set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error
					// function.
					pset_func_ptr( pgroup, pindex, *pvalue, CHANGE_do_not_generate_change_line, CHANGE_REASON_RANGE_CHECK_FAILED, FLOWSENSE_get_controller_index(), set_change_bits_in_GET_FUNCTION_when_there_is_a_range_check_error(), pchange_bitfield_to_set );
				}
			}
		}
		else
		{
			Alert_index_out_of_range();

			*pvalue = pdefault;
		}
	}
	else
	{
		Alert_group_not_found();

		*pvalue = pdefault;
	}

	return( *pvalue );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif // #ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

