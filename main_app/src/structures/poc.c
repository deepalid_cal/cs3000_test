/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"poc.h"

#include	"bithacks.h"

#include	"budgets.h"

#include	"cal_math.h"

#include	"change.h"

#include	"comm_mngr.h"

#include	"cs_mem.h"

#include	"dialog.h"

#include	"e_budgets.h"

#include	"group_base_file.h"

#include	"flash_storage.h"

#include	"flowsense.h"

#include	"foal_comm.h"

#include	"irrigation_system.h"

#include	"pdata_changes.h"

#include	"range_check.h"

#include	"shared.h"

#include	"tpmicro_comm.h"

#include	"app_startup.h"

#include	"r_alerts.h"

#include	"tpmicro_data.h"

#include	"controller_initiated.h"

#include	"chain_sync_vars.h"

#include	"chain_sync_funcs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static MIST_LIST_HDR_TYPE	poc_group_list_hdr;

// ----------

// 2/18/2016 rmd : Must include the shared c source after the poc_group_list_header
// declaration.
#include	"shared_poc.c"

// ----------

static const char POC_FILENAME[] =		"POC";

const char POC_DEFAULT_NAME[] =			"POC";

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static const float FLOW_METER_KVALUES[ (POC_FM_CHOICE_MAX + 1) ] =
{
	0.0F,		// None

	0.397368F,	// FM-1B

	0.76447F,	// FM-1.25B

	1.06526F,	// FM-1.5B

	1.697F,		// FM-1.5

	2.747F,		// FM-2B

	2.8429F,	// FM-2

	8.309F,		// FM-3

	0.0F,		// FMBX

	0.0F,		// BERMAD_0150 

	0.0F,		// BERMAD_0200 

	0.0F,		// BERMAD_0250 

	0.0F,		// BERMAD_0300R

	0.0F,		// BERMAD_0300 

	0.0F,		// BERMAD_0400 

	0.0F,		// BERMAD_0600 

	0.0F,		// BERMAD_0800 

	0.0F		// BERMAD_1000 
};

// ----------

static const float FLOW_METER_OFFSETS[ (POC_FM_CHOICE_MAX + 1) ] =
{
	0.0F,		// None

	0.261768F,	// FM-1B

	0.16489F,	// FM-1.25B

	0.0892F,	// FM-1.5B

	-0.316F,	// FM-1.5

	0.000F,		// FM-2B

	0.1435F,	// FM-2

	0.227F,		// FM-3

	0.0F,		// FMBX

	0.0F,		// BERMAD_0150 

	0.0F,		// BERMAD_0200 

	0.0F,		// BERMAD_0250 

	0.0F,		// BERMAD_0300R

	0.0F,		// BERMAD_0300 

	0.0F,		// BERMAD_0400 

	0.0F,		// BERMAD_0600 

	0.0F,		// BERMAD_0800 

	0.0F		// BERMAD_1000 
};

// ----------

static const UNS_32 FLOW_METER_SIZE_ORDER[ (POC_FM_CHOICE_MAX + 1) ] =
{
	POC_FM_NONE,
	POC_FM_100B,
	POC_FM_125B,
	POC_FM_150B,
	POC_FM_150,
	POC_FM_BERMAD_0150,
	POC_FM_200B,
	POC_FM_200,
	POC_FM_BERMAD_0200,
	POC_FM_BERMAD_0250,
	POC_FM_300,
	POC_FM_BERMAD_0300R,
	POC_FM_BERMAD_0300,
	POC_FM_BERMAD_0400,
	POC_FM_BERMAD_0600,
	POC_FM_BERMAD_0800,
	POC_FM_BERMAD_1000,
	POC_FM_FMBX
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void nm_POC_set_default_values( void *const ppoc, const BOOL_32 pset_change_bits );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	UNS_32	flow_meter_choice;

	UNS_32	kvalue_100000u;

	INT_32	offset_100000u;

} POC_FM_CHOICE_AND_K_AND_OFFSET_FOR_REVISION_0;

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	UNS_32			GID_irrigation_system;

	UNS_32			box_index_0;

	UNS_32			poc_type__file_type;

	UNS_32			decoder_serial_number[ POC_BYPASS_LEVELS_MAX ];

	BOOL_32			poc_physically_available;

	BOOL_32			individually_available[ POC_BYPASS_LEVELS_MAX ];

	UNS_32			poc_usage;

	UNS_32	  		master_valve_type[ POC_BYPASS_LEVELS_MAX ];

	POC_FM_CHOICE_AND_K_AND_OFFSET_FOR_REVISION_0	flow_meter[ POC_BYPASS_LEVELS_MAX ];

	UNS_32			bypass_number_of_levels;

	UNS_32			budget_option;

	UNS_32			budget_percent_of_et;

	UNS_32			budget_monthly[ MONTHS_IN_A_YEAR ];

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

} POC_GROUP_STRUCT_REV_0;

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	UNS_32			GID_irrigation_system;

	UNS_32			box_index_0;

	UNS_32			poc_type__file_type;

	UNS_32			decoder_serial_number[ POC_BYPASS_LEVELS_MAX ];

	BOOL_32			poc_physically_available;

	BOOL_32			individually_available[ POC_BYPASS_LEVELS_MAX ];

	UNS_32			poc_usage;

	UNS_32	  		master_valve_type[ POC_BYPASS_LEVELS_MAX ];

	UNS_32			unused_a[ 9 ];

	UNS_32			bypass_number_of_levels;

	UNS_32			unused_b[ 2 ];

	POC_FM_CHOICE_AND_RATE_DETAILS	flow_meter[ POC_BYPASS_LEVELS_MAX ];
	
	// -------------------------------------------- //
	//  CHANGE BITS
	// -------------------------------------------- //

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

}  POC_GROUP_STRUCT_REV_1;

// ----------

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	// -------------------------------------------- //
	//	SYSTEM_THIS_POC_BELONGS_TO
	// -------------------------------------------- //

	UNS_32			GID_irrigation_system;

	// -------------------------------------------- //
	//	WHICH PHYSICAL POC IS THIS? - THESE PARAMETERS PIN THAT DOWN
	// -------------------------------------------- //

	// 4/17/2014 ajv : The 0-based box (controller) index which coincides with
	// the controller's communication address.
	UNS_32			box_index_0;
	
	// ----------

	// 5/19/2014 rmd : One of these three types:	POC__FILE_TYPE__TERMINAL
	//  											POC__FILE_TYPE__DECODER_SINGLE
	//  											POC__FILE_TYPE__DECODER_BYPASS
	// 5/19/2014 rmd : Note the naming convention to help ensure no confusion on which defines
	// to use for this variable.
	UNS_32			poc_type__file_type;

	// ----------

	// 4/17/2014 ajv : The decoder serial number assigned to this POC. For
	// POC_TYPE_TERMINAL, this should always be 0. For Bypass Manifolds
	// comprised of multiple single-POC decoders; position 0 of the array
	// indicates stage 1 (the smallest level), position 1 is stage 2, and so on.
	UNS_32			decoder_serial_number[ POC_BYPASS_LEVELS_MAX ];

	// -------------------------------------------- //
	//	WHETHER THE POC MODULES ARE CONNECTED TO THE TP BOARD
	// -------------------------------------------- //

	// 3/4/2013 ajv : Tracks whether the POC card and terminal are both physically connected to
	// the TP Micro or not. This is a single flag for the whole POC. Even for the case of a
	// BYPASS. So if one decoder is missing the whole BYPASS is considered unavailable. This
	// flag is set upon creation of the bypass. And cleared at conclusion of a discovery if one
	// of the decoders is missing.
	BOOL_32			poc_physically_available;

	// 7/8/2014 rmd : A working variable needed to temporarily track decoder presence when
	// working to set the physically available variable following a decoder discovery. The
	// complication is driven by the presence of a BYPASS poc. The setting of this variable is
	// NOT to be distributed and written to the file. That is not needed. And this variable
	// should not be used for any purpose other than what it is already used for. It has no
	// other use.
	BOOL_32			individually_available[ POC_BYPASS_LEVELS_MAX ];

	// -------------------------------------------- //
	//	COMMON POC DETAILS
	// -------------------------------------------- //

	// 2011.12.20 ajv : We use the poc_usage setting to determine whether to
	// display the POC in the list of available POCs or not. This way, if the
	// chain only has one flow meter and master valve connected, only the single
	// POC is displayed when building a system.
	UNS_32			poc_usage;
	
	// -------------------------------------------- //

	// Normally OPEN or Normally CLOSED.
	UNS_32	  		master_valve_type[ POC_BYPASS_LEVELS_MAX ];

	// ----------
	
	// 1/29/2015 rmd : 36 unused bytes within the structure. This was the flow meter choice with
	// K and O. But was obsoleted when we added the reed_switch setting to the structure.
	UNS_32			unused_a[ 9 ];

	// -------------------------------------------- //
	//	BYPASS SPECIFICS
	// -------------------------------------------- //

	// 4/17/2014 ajv : The number of stages of the bypass manifold. For now,
	// this should always either be 2 or 3.
	UNS_32			bypass_number_of_levels;
	
	// ----------
	
	// 1/29/2015 rmd : Was preliminary budget variables. Made unused when structure revision
	// moved to revision 1.
	UNS_32			unused_b[ 2 ];

	// ----------
	
	// 1/29/2015 rmd : The new flow meter choice, K, O, and reed_switch information. Introduced
	// when structure revision moved from 0 to revsion 1.
	POC_FM_CHOICE_AND_RATE_DETAILS	flow_meter[ POC_BYPASS_LEVELS_MAX ];
	
	// -------------------------------------------- //
	//  CHANGE BITS
	// -------------------------------------------- //

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

	// ----------

	// 4/30/2015 ajv : Added in Rev 2

	// ----------
	
	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;
	
} POC_GROUP_STRUCT_REV_2;

// ----------

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	// -------------------------------------------- //
	//	SYSTEM_THIS_POC_BELONGS_TO
	// -------------------------------------------- //

	UNS_32			GID_irrigation_system;

	// -------------------------------------------- //
	//	WHICH PHYSICAL POC IS THIS? - THESE PARAMETERS PIN THAT DOWN
	// -------------------------------------------- //

	// 4/17/2014 ajv : The 0-based box (controller) index which coincides with
	// the controller's communication address.
	UNS_32			box_index_0;
	
	// ----------

	// 5/19/2014 rmd : One of these three types:	POC__FILE_TYPE__TERMINAL
	//  											POC__FILE_TYPE__DECODER_SINGLE
	//  											POC__FILE_TYPE__DECODER_BYPASS
	// 5/19/2014 rmd : Note the naming convention to help ensure no confusion on which defines
	// to use for this variable.
	UNS_32			poc_type__file_type;

	// ----------

	// 4/17/2014 ajv : The decoder serial number assigned to this POC. For
	// POC_TYPE_TERMINAL, this should always be 0. For Bypass Manifolds
	// comprised of multiple single-POC decoders; position 0 of the array
	// indicates stage 1 (the smallest level), position 1 is stage 2, and so on.
	UNS_32			decoder_serial_number[ POC_BYPASS_LEVELS_MAX ];

	// -------------------------------------------- //
	//	WHETHER THE POC MODULES ARE CONNECTED TO THE TP BOARD
	// -------------------------------------------- //

	// 3/4/2013 ajv : Tracks whether the POC card and terminal are both physically connected to
	// the TP Micro or not. This is a single flag for the whole POC. Even for the case of a
	// BYPASS. So if one decoder is missing the whole BYPASS is considered unavailable. This
	// flag is set upon creation of the bypass. And cleared at conclusion of a discovery if one
	// of the decoders is missing.
	BOOL_32			poc_physically_available;

	// 7/8/2014 rmd : A working variable needed to temporarily track decoder presence when
	// working to set the physically available variable following a decoder discovery. The
	// complication is driven by the presence of a BYPASS poc. The setting of this variable is
	// NOT to be distributed and written to the file. That is not needed. And this variable
	// should not be used for any purpose other than what it is already used for. It has no
	// other use.
	BOOL_32			individually_available[ POC_BYPASS_LEVELS_MAX ];

	// -------------------------------------------- //
	//	COMMON POC DETAILS
	// -------------------------------------------- //

	// 2011.12.20 ajv : We use the poc_usage setting to determine whether to
	// display the POC in the list of available POCs or not. This way, if the
	// chain only has one flow meter and master valve connected, only the single
	// POC is displayed when building a system.
	UNS_32			poc_usage;
	
	// -------------------------------------------- //

	// Normally OPEN or Normally CLOSED.
	UNS_32	  		master_valve_type[ POC_BYPASS_LEVELS_MAX ];

	// ----------
	
	// 1/29/2015 rmd : 36 unused bytes within the structure. This was the flow meter choice with
	// K and O. But was obsoleted when we added the reed_switch setting to the structure.
	UNS_32			unused_a[ 9 ];

	// -------------------------------------------- //
	//	BYPASS SPECIFICS
	// -------------------------------------------- //

	// 4/17/2014 ajv : The number of stages of the bypass manifold. For now,
	// this should always either be 2 or 3.
	UNS_32			bypass_number_of_levels;
	
	// ----------
	
	// 1/29/2015 rmd : Was preliminary budget variables. Made unused when structure revision
	// moved to revision 1.
	UNS_32		unused_b[ 2 ];

	// ----------
	
	// 1/29/2015 rmd : The new flow meter choice, K, O, and reed_switch information. Introduced
	// when structure revision moved from 0 to revsion 1.
	POC_FM_CHOICE_AND_RATE_DETAILS	flow_meter[ POC_BYPASS_LEVELS_MAX ];
	
	// -------------------------------------------- //
	//  CHANGE BITS
	// -------------------------------------------- //

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

	// ----------

	// 4/30/2015 ajv : Added in Rev 2

	// ----------
	
	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;
	
	// ----------
	
	// 9/9/2015 rmd : Added in Rev 3.

	// ----------
	
	// POC_BUDGET_GALLONS_ENTRY_OPTION_direct_entry (1)
	// POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_as_percent_of_historical_et (2)
	// POC_BUDGET_GALLONS_ENTRY_OPTION_projected_budget_from_water_bill (3)
	UNS_32		budget_gallons_entry_option;
	
	// 10/29/2015 rmd : Used if the user wants US to calculate the budget.
	UNS_32		budget_calculation_percent_of_et;

	// 9/18/2015 rmd : As real ET is placed in the table update the budget numbers. Update the
	// calculated or the 'projected budget' as supplied by the previous water bill.
	BOOL_32		budget_auto_adjust_budget_using_live_et;
	
	// 9/9/2015 rmd : When this POC use reaches the budget what to do? Alert everyday at
	// midnight I think goes without saying. The question is should this POC no longer open
	// during irrigation? In other words take out of use once budget has been reached. NOTE - if
	// the mainline has a second POC one could be setup to go offline when budget is hit and the
	// other POC to continue.
	// 01/22/2016 skc : This flag is only needed if the use_choice is set to Manual
	// system.use_choice	= Off - does nothing
	//						= Auto - not needed, the software prevents reaching budget
	//						= Manual - user is prompted (once? constantly?)
	//						= Stop - not needed, the software stops flow via this POC
	BOOL_32		budget_close_poc_at_budget;

	// ----------
	
	// 11/5/2015 rmd : This was the budget gallons by period. But that was moved to the
	// BUDGET_AND_REPORT_STRUCT list file. This structure space is now available for any future
	// use that comes along.
	// 01/04/2015 skc: We DO use this variable to store the budget settings.
	// A text search showed 34 instances in two files
	UNS_32		budget_gallons[ IRRIGATION_SYSTEM_BUDGET_ANNUAL_PERIODS_MAX ];
	
} POC_GROUP_STRUCT_REV_3;

// ----------

// 1/26/2015 rmd : The structure revision number. If the structure definition changes this
// value is to be changed. Typically incremented by 1. Then in the updater function handle
// the change specifics whatever they may be. Generally meaning initialize newly added
// values. But could be more extensive than that.
#define	POC_LATEST_FILE_REVISION		(5)

// ----------

const UNS_32 poc_list_item_sizes[ POC_LATEST_FILE_REVISION + 1 ] =
{
	// 1/26/2015 rmd : In order for this array to contain OLDER list item sizes the implication
	// is that the older structure definitions are to be retained. That means the newer
	// structure definition has a different name.

	// ----------
	
	// 1/30/2015 rmd : Revision 0 structure.
	sizeof( POC_GROUP_STRUCT_REV_0 ),

	// 1/30/2015 rmd : Revision 1 structure.
	sizeof( POC_GROUP_STRUCT_REV_1 ),

	// 1/30/2015 rmd : Revision 2 structure.
	sizeof( POC_GROUP_STRUCT_REV_2 ),
	
	// 9/10/2015 rmd : Revision 3 structure.
	sizeof( POC_GROUP_STRUCT_REV_3 ),

	// 01/22/2016 skc ; Revision 4 structure.
	sizeof( POC_GROUP_STRUCT ),

	// 10/8/2018 ajv : Revision 5 - the file size didn't change since we're repurposing an 
	// unused variable. 
	sizeof( POC_GROUP_STRUCT ),

};

/* ---------------------------------------------------------- */
static void nm_poc_structure_updater( UNS_32 pfrom_revision )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only when
	// the file revision is changed. The caller is to be holding the list_poc_recursive_MUTEX.

	// ----------
	
	POC_GROUP_STRUCT	*lpoc;
	
	UNS_32				iii;
	
	CHANGE_BITS_32_BIT	*lbitfield_of_changes;

	UNS_32	box_index_0;
	
	// ----------
	
	// 9/21/2015 rmd : Because of the order of the init_file function calls in the app_startup
	// task we know the controller index is valid for use at this [early] point during the boot
	// process that this updater function is called.
	box_index_0 = FLOWSENSE_get_controller_index();

	// ----------
	
	if( pfrom_revision == POC_LATEST_FILE_REVISION )
	{
		Alert_Message_va( "POC file unexpd update %u", pfrom_revision );	
	}
	else
	{
		Alert_Message_va( "POC file update : to revision %u from %u", POC_LATEST_FILE_REVISION, pfrom_revision );

		// ----------
		
		if( pfrom_revision == 0 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 0 to REVISION 1. Moving
			// to revision 1 involved a significant redesign. The structure size is actually the same
			// however several variables were redefined. Specifically the budget and the
			// FM_CHOICE_AND_K_AND_OFFSET structure.
			
			lpoc = nm_ListGetFirst( &poc_group_list_hdr );
		
			while( lpoc != NULL )
			{
				// 1/29/2015 rmd : First copy the fm choice to the new location.
				for( iii=0; iii<POC_BYPASS_LEVELS_MAX; iii++ )
				{
					// 1/29/2015 rmd : Take from where the old structure was, and place into the new structure.
					memcpy( &(lpoc->flow_meter[ iii ]), &(lpoc->unused_a[ iii * 3 ]), sizeof(POC_FM_CHOICE_AND_K_AND_OFFSET_FOR_REVISION_0) );

					lpoc->flow_meter[ iii ].reed_switch = POC_FM_REED_SWITCH_DEFAULT;
				}
				
				// ----------
				
				// 1/29/2015 rmd : Now initialize the unused data.
				memset( &(lpoc->unused_a), 0x00, 9 * sizeof(UNS_32) );

				memset( &(lpoc->unused_b), 0x00, 2 * sizeof(UNS_32) );
				
				// ----------
				
				// 1/30/2015 rmd : Because of revision 0 bugs these bits were not set. We need to set them
				// to guarantee all the program data is sent to the comm_server this one time.
				lpoc->changes_to_upload_to_comm_server = UNS_32_MAX;
				
				// ----------
				
				lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );
			}
			
			// ----------
			
			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 1 to 2 changes if
			// needed.
			pfrom_revision += 1;
		}
		
		// ----------
		
		if( pfrom_revision == 1 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 1 to REVISION 2.

			lpoc = nm_ListGetFirst( &poc_group_list_hdr );

			while( lpoc != NULL )
			{
				// 4/30/2015 ajv : Clear all of the pending Program Data bits.
				lpoc->changes_uploaded_to_comm_server_awaiting_ACK = CHANGE_BITS_32_BIT_ALL_CLEAR;

				lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );
			}
			pfrom_revision += 1;
		}
		
		// ----------
		
		if( pfrom_revision == 2 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REV 2 to REV 3.
			// 02/10/2016 skc : We don't need to do anything, the only changes were
			// for budgets which didn't actually get implemented until REV 4.
			pfrom_revision += 1;
		}

		if(  pfrom_revision == 3 )
		{
			// 01/22/2016 skc : This is the work to do when moving from REV 3 to REV 4
			lpoc = nm_ListGetFirst( &poc_group_list_hdr );

			while( lpoc != NULL )
			{
				lbitfield_of_changes = &lpoc->changes_to_send_to_master;

				// set defaults to the extra storage for budgets in REV 4
				nm_POC_set_budget_gallons_entry_option( lpoc, POC_BUDGET_GALLONS_ENTRY_OPTION_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
				
				nm_POC_set_budget_calculation_percent_of_et( lpoc, POC_BUDGET_CALCULATION_PERCENT_OF_ET_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				for( iii=0; iii<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS; iii++ )
				{
					nm_POC_set_budget_gallons( lpoc, iii, POC_BUDGET_GALLONS_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
				}

				lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );
			}
			pfrom_revision += 1;
		}
		
		// ----------
		
		if(  pfrom_revision == 4 )
		{
			// 10/8/2018 ajv : This is the work to do when moving from REV 4 to REV 5
			
			// 12/7/2018 Ryan : Only update the pump setting if the controller is the master of its
			// chain. This is because, in some cases where a delinking error has occurred on the chain,
			// causing an unlinked POC to still show up on slave controllers on the chain, then the
			// editing on the extra POCs on the slaves cause the A terminal to be deleted on the chain.
			// Not running this on the slaves would cause a desync, but the same update that has this
			// POC version also includes chain sync, so any POC syncing issues will be automatically
			// resolved.
			if( FLOWSENSE_we_are_a_master_one_way_or_another() )
			{
				lpoc = nm_ListGetFirst( &poc_group_list_hdr );
				
				while( lpoc != NULL )
				{
					lbitfield_of_changes = &lpoc->changes_to_send_to_master;
					
					Alert_Message_va( "poc type %d bitfield %x" , lpoc->poc_type__file_type , lbitfield_of_changes );
					
					nm_POC_set_has_pump_attached( lpoc, POC_PUMP_PHYSICALLY_ATTACHED_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );
	
					lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );
				}
			}
			pfrom_revision += 1;
		}
	}

	// ----------

	// 1/29/2015 rmd : Well after the updater has run the pfrom_revision is supposed to equal
	// the latest revision. By design of this update process.
	if( pfrom_revision != POC_LATEST_FILE_REVISION )
	{
		Alert_Message( "POC updater error" );
	}
}

/* ---------------------------------------------------------- */
extern void init_file_POC( void )
{
	FLASH_FILE_initialize_list_and_find_or_create_group_list_file(	FLASH_INDEX_1_GENERAL_STORAGE,
																	POC_FILENAME,
																	POC_LATEST_FILE_REVISION,
																	&poc_group_list_hdr,
																	poc_list_item_sizes,
																	list_poc_recursive_MUTEX,
																	&nm_poc_structure_updater,
																	&nm_POC_set_default_values,
																	(char*)POC_DEFAULT_NAME,
																	FF_POC );
}



extern void save_file_POC( void )
{
	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE,
															POC_FILENAME,
															POC_LATEST_FILE_REVISION,
															&poc_group_list_hdr,
															sizeof( POC_GROUP_STRUCT ),
															list_poc_recursive_MUTEX,
															FF_POC );
}

/* ---------------------------------------------------------- */
/**
 * Sets the default values for a POC group. If the passed in group is NULL, an
 * error is raised and this function takes no further action.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *  	  group whose values are being set isn't deleted during this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param ppoc A pointer to the group to set.
 *
 * @author 8/11/2011 Adrianusv
 *
 * @revisions
 *    8/11/2011  Initial release
 *    2/10/2012  Added check to ensure passed in group is not NULL
 *    4/5/2012   Added initialization of change bits
 */
static void nm_POC_set_default_values( void *const ppoc, const BOOL_32 pset_change_bits )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	char	str_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32	i;

	UNS_32	box_index_0;

	// ----------

	box_index_0 = FLOWSENSE_get_controller_index();

	// ------------------------

	if( ppoc != NULL )
	{
		// 5/14/2014 ajv : Clear all of the change bits for POC since it's just
		// been created. We'll then explicitly set each bit based upon whether
		// pset_change_bits is true or not.
		SHARED_clear_all_32_bit_change_bits( &((POC_GROUP_STRUCT *)ppoc)->changes_to_send_to_master, list_poc_recursive_MUTEX );
		SHARED_clear_all_32_bit_change_bits( &((POC_GROUP_STRUCT *)ppoc)->changes_to_distribute_to_slaves, list_poc_recursive_MUTEX );
		SHARED_clear_all_32_bit_change_bits( &((POC_GROUP_STRUCT *)ppoc)->changes_uploaded_to_comm_server_awaiting_ACK, list_poc_recursive_MUTEX );

		// 1/20/2015 ajv : However, we should explicitly set ALL bits for the Comm Server to ensure
		// changes are uploaded on a clean start.
		SHARED_set_all_32_bit_change_bits( &((POC_GROUP_STRUCT *)ppoc)->changes_to_upload_to_comm_server, list_poc_recursive_MUTEX );
		
		// ------------------------

		// 5/2/2014 ajv : When setting the default values, always set the changes to
		// be sent to the master. Once the master receives the changes, it will
		// distribute them back out to the slaves.
		lchange_bitfield_to_set = &((POC_GROUP_STRUCT *)ppoc)->changes_to_send_to_master;

		// ------------------------

		snprintf( str_48, NUMBER_OF_CHARS_IN_A_NAME, "Terminal Strip @ %c", box_index_0 + 'A' );
		nm_POC_set_name( ppoc, str_48, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_POC_set_box_index( ppoc, box_index_0, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		// 11/20/2018 Ryan : If a POC is created on the master controller in a flowsense chain, the
		// nm_POC_set_box_index function does not set a changeline when the default is created,
		// because the box index is already NULL before the set function is called. This causes
		// errors when sending the newly created POCs to slaves in the chain, as their check default
		// function call will not set the box index to zero, but instead their box index value. If
		// we set the change bit here, the slaves will know to overight their default value with
		// the correct box index value, zero.
		if( box_index_0 == 0 )
		{
			*lchange_bitfield_to_set |= ((UNS_32)1 << POC_CHANGE_BITFIELD_box_index_bit);
		}

		nm_POC_set_show_this_poc( ppoc, POC_SHOW_THIS_POC_DEFAULT, (false), CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_POC_set_poc_usage( ppoc, POC_USAGE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_POC_set_poc_type( ppoc, POC__FILE_TYPE__DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		for( i = 0; i < POC_BYPASS_LEVELS_MAX; ++i )
		{
			// 6/10/2014 rmd : A decoder serial number of 0 matches with the poc_file_type of terminal.
			// Shouldn't have one without the other. Hard coded here to prevent inadvertant minimum
			// decoder serial number change by someone in the future. Which may not hurt anything as we
			// would end up with a permantely dormant poc in the file. But this seems better.
			nm_POC_set_decoder_serial_number( ppoc, (i + 1), 0, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

			nm_POC_set_mv_type( ppoc, (i + 1), POC_MV_TYPE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

			nm_POC_set_fm_type( ppoc, (i + 1), POC_FM_CHOICE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

			nm_POC_set_kvalue( ppoc, (i + 1), (UNS_32)(POC_FM_KVALUE_DEFAULT * 100000.0F), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

			nm_POC_set_offset( ppoc, (i + 1), (UNS_32)(POC_FM_OFFSET_DEFAULT * 100000.0F), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
		}

		nm_POC_set_bypass_number_of_levels( ppoc, POC_BYPASS_LEVELS_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_POC_set_GID_irrigation_system( ppoc, nm_GROUP_get_group_ID( SYSTEM_get_group_at_this_index( 0 ) ), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_POC_set_budget_gallons_entry_option( ppoc, POC_BUDGET_GALLONS_ENTRY_OPTION_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_POC_set_budget_calculation_percent_of_et( ppoc, POC_BUDGET_CALCULATION_PERCENT_OF_ET_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		for( i=0; i<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS; i++ )
		{
			nm_POC_set_budget_gallons( ppoc, i, POC_BUDGET_GALLONS_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
		}

		nm_POC_set_has_pump_attached( ppoc, POC_PUMP_PHYSICALLY_ATTACHED_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
	}
	else
	{
		Alert_func_call_with_null_ptr();
	}
}

/* ---------------------------------------------------------- */
/**
 * Creates a new POC group and adds it to the POC list.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *        list isn't modified during this process.
 *
 * @executed Executed within the context of the key processing task when the
 *  		 user creates a new group; within the context of the CommMngr task
 *  		 when a new group is created using a central or handheld radio
 *  		 remote.
 * 
 * @return void* A pointer to the newly created group.
 * 
 * @author 8/11/2011 Adrianusv
 * 
 * @revisions
 *    8/11/2011 Initial release
 */
extern void *nm_POC_create_new_group( const UNS_32 pbox_index_0, const UNS_32 ppoc_type, const UNS_32 pdecoder_serial_number )
{
	POC_GROUP_STRUCT *lpoc, *lnext_poc;

	UNS_32	lnext_poc_GID;

	lnext_poc = nm_ListGetFirst( &poc_group_list_hdr );

	while( lnext_poc != NULL )
	{
		lnext_poc_GID = nm_GROUP_get_group_ID( lnext_poc );

		// 11/19/2014 ajv : If the box index of the POC to be created and added to the list is less
		// than the next one, stop and it insert here.
		if( pbox_index_0 < POC_get_box_index_0(lnext_poc_GID) )
		{
			break;
		}
		else
		if( pbox_index_0 == POC_get_box_index_0(lnext_poc_GID) )
		{
			// 11/19/2014 ajv : If the box index is the same, terminals are first, followed by single
			// POC decoders (sorted numerically by serial number), followed by bypass manifolds (sorted
			// numerically by serial number of the first decoder in the array).
			if( ppoc_type < POC_get_type_of_poc(lnext_poc_GID) )
			{
				// 11/19/2014 ajv : Insert the POC here
				break;
			}
			else
			if( ppoc_type == POC_get_type_of_poc(lnext_poc_GID) )
			{
				if( pdecoder_serial_number < POC_get_decoder_serial_number_for_this_poc_gid_and_level_0( lnext_poc_GID, 0 ) )
				{
					// 11/19/2014 ajv : Insert the POC here
					break;
				}
			}
		}

		lnext_poc = nm_ListGetNext( &poc_group_list_hdr, lnext_poc );
	}

	// 11/19/2014 ajv : No need to check if lnext_poc is NULL. If it is, the new POC is
	// automatically added to the end of the list.
	lpoc = nm_GROUP_create_new_group( &poc_group_list_hdr, (char*)POC_DEFAULT_NAME, &nm_POC_set_default_values, sizeof(POC_GROUP_STRUCT), (true), lnext_poc );

	return( lpoc );
}

/* ---------------------------------------------------------- */
/**
 * This function runs through the POC list and copies the contents of any changed POCs to
 * the passed in ucp.
 * 
 * @executed Executed within the context of the CommMngr task as a token, token response, or
 * Program Data message to the Comm Server is being built.
 * 
 * @param pucp A pointer to the pointer where the program data is being built.
 * 
 * @param pmem_used_so_far The amount of memory used so far. Once this reaches our limit for
 * a single token (MEMORY_TO_ALLOCATE_FOR_SENDING_CHANGES), processing of the remaining
 * changes stops until it's time to build the next token or token response.
 * 
 * @param pmem_used_so_far_is_less_than_allocated_memory A pointer to the flag which
 * indicates whether we're allowed to add more changes to the token or token response.
 * 
 * @param preason_data_is_being_built The reason why the data is being built to send. Based
 * upon this reason, we examine a different change bit structure to determine what to
 * send. Valid values are CHANGE_REASON_SYNC_SENDING_TO_MASTER,
 * CHANGE_REASON_SYNC_DISTRIBUTING_TO_SLAVES, CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER.
 * 
 * @return UNS_32 The total size of the added data. This is used to determine whether any
 * changes were detected so the appropriate file change bit can be set as well as to
 * determine how much memory must be allocated to store the changes.
 *
 * @author 9/14/2011 Adrianusv
 */
extern UNS_32 POC_build_data_to_send( UNS_8 **pucp,
									  const UNS_32 pmem_used_so_far,
									  BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
									  const UNS_32 preason_data_is_being_built,
									  const UNS_32 pallocated_memory )
{
	CHANGE_BITS_32_BIT	*lbitfield_of_changes_to_use_to_determine_what_to_send;

	CHANGE_BITS_32_BIT	lbitfield_of_changes_to_send;

	POC_GROUP_STRUCT	*lpoc;

	UNS_8	*llocation_of_num_changed_pocs;

	UNS_8	*llocation_of_bitfield;

	// 1/14/2015 ajv : Track how many POCs are added to the message.
	UNS_32	lnum_changed_pocs;

	// Store the amount of memory used for current POC. This is compared to the POC's overhead
	// to determine whether any of the POC's values were actually added or not.
	UNS_32	lmem_used_by_this_poc;

	// Determine how much overhead each POC requires in case we add the overhead and don't have
	// enough room to add of the actual changes.
	UNS_32	lmem_overhead_per_poc;

	UNS_32	rv;

	// ----------

	rv = 0;

	lnum_changed_pocs = 0;

	// ----------

	// 1/14/2015 ajv : Take the appropriate mutex to ensure no POCs are added or removed during
	// this process.
	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lpoc = nm_ListGetFirst( &poc_group_list_hdr );

	// 1/14/2015 ajv : Loop through each POC and count how many POCs have change bits set. This
	// is really just used to determine whether there's at least one POC which needs to be
	// examined.
	while( lpoc != NULL )
	{
		if( *(POC_get_change_bits_ptr( lpoc, preason_data_is_being_built )) != CHANGE_BITS_32_BIT_ALL_CLEAR )
		{
			lnum_changed_pocs++;

			// 1/14/2015 ajv : Since we don't actually care about the number of POCs that have changed
			// at this point - just that at least one has changed - we can break out of the loop now.
			break;
		}

		lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );
	}

	// ----------

	if( lnum_changed_pocs > 0 )
	{
		// 1/14/2015 ajv : Initialize the changed POCs back to 0. Since not all of the POCs may
		// be added due to token size limitations, we need to ensure that the receiving party only
		// processes the number of POCs actually added to the message.
		lnum_changed_pocs = 0;

		// ----------

		// 1/14/2015 ajv : Allocate space to store the number of changed POCs that are included in
		// the message so the receiving party can correctly parse the data. This will be filled in
		// later once we have an actual count.
		rv = PDATA_allocate_space_for_num_changed_groups_in_pucp( pucp, &llocation_of_num_changed_pocs, pmem_used_so_far, pmem_used_so_far_is_less_than_allocated_memory, pallocated_memory );

		// ----------

		// 4/28/2015 ajv : Only run through the POCs if there was enough room in the token to
		// include the number of changed POCs.
		if( *pmem_used_so_far_is_less_than_allocated_memory )
		{
			lpoc = nm_ListGetFirst( &poc_group_list_hdr );

			while( lpoc != NULL )
			{
				lbitfield_of_changes_to_use_to_determine_what_to_send = POC_get_change_bits_ptr( lpoc, preason_data_is_being_built );

				// 1/14/2015 ajv : If this POC has changed...
				if( *lbitfield_of_changes_to_use_to_determine_what_to_send != CHANGE_BITS_32_BIT_ALL_CLEAR )
				{
					// 2/9/2016 rmd : The OVERHEAD is the GID, the box index, the poc type, the ARRAY of 3
					// decoder serial numbers, an UNS_32 indicating the size of the bitfield, and the 32-bit
					// change bitfield itself.
					lmem_overhead_per_poc = (8 * sizeof(UNS_32));

					// 1/14/2015 ajv : If we still have room available in the message for the POC's overhead,
					// add it now.
					if( *pmem_used_so_far_is_less_than_allocated_memory && ((pmem_used_so_far + rv + lmem_overhead_per_poc) < pallocated_memory) )
					{
						PDATA_copy_var_into_pucp( pucp, &lpoc->base.group_identity_number, sizeof(lpoc->base.group_identity_number) );

						// If a change was detected, we need to include a number of other settings in the size
						// including box_index, POC type, and decoder serial number. These are used to determine
						// where to insert this POC into the list since we want them pre-sorted.
						PDATA_copy_var_into_pucp( pucp, &lpoc->box_index_0, sizeof(lpoc->box_index_0) );

						PDATA_copy_var_into_pucp( pucp, &lpoc->poc_type__file_type, sizeof(lpoc->poc_type__file_type) );

						// 3/24/2016 rmd : This is an ARRAY of THREE decoder serial numbers! 12 bytes added to the
						// message.
						PDATA_copy_var_into_pucp( pucp, &lpoc->decoder_serial_number, sizeof(lpoc->decoder_serial_number) );

						PDATA_copy_bitfield_info_into_pucp( pucp, sizeof(CHANGE_BITS_32_BIT), sizeof(UNS_32), &llocation_of_bitfield ); 
					}
					else
					{
						// 9/19/2013 ajv : Stop processing POCs if we've already filled out the token with the
						// maximum amount of data to send.
						*pmem_used_so_far_is_less_than_allocated_memory = (false);

						break;
					}

					lmem_used_by_this_poc = lmem_overhead_per_poc;

					// ----------

					// Initialize the bit field
					lbitfield_of_changes_to_send = 0x00;

					lmem_used_by_this_poc += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							POC_CHANGE_BITFIELD_description_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lpoc->changes_uploaded_to_comm_server_awaiting_ACK,
							&lpoc->base.description,
							NUMBER_OF_CHARS_IN_A_NAME,
							(lmem_used_by_this_poc + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_poc += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							POC_CHANGE_BITFIELD_GID_irri_system_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lpoc->changes_uploaded_to_comm_server_awaiting_ACK,
							&lpoc->GID_irrigation_system,
							sizeof(lpoc->GID_irrigation_system),
							(lmem_used_by_this_poc + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_poc += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							POC_CHANGE_BITFIELD_box_index_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lpoc->changes_uploaded_to_comm_server_awaiting_ACK,
							&lpoc->box_index_0,
							sizeof(lpoc->box_index_0),
							(lmem_used_by_this_poc + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_poc += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							POC_CHANGE_BITFIELD_type_of_poc_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lpoc->changes_uploaded_to_comm_server_awaiting_ACK,
							&lpoc->poc_type__file_type,
							sizeof(lpoc->poc_type__file_type),
							(lmem_used_by_this_poc + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_poc += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							POC_CHANGE_BITFIELD_decoder_serial_number_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lpoc->changes_uploaded_to_comm_server_awaiting_ACK,
							&lpoc->decoder_serial_number,
							sizeof(lpoc->decoder_serial_number),
							(lmem_used_by_this_poc + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_poc += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							POC_CHANGE_BITFIELD_poc_physically_available_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lpoc->changes_uploaded_to_comm_server_awaiting_ACK,
							&lpoc->show_this_poc,
							sizeof(lpoc->show_this_poc),
							(lmem_used_by_this_poc + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_poc += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							POC_CHANGE_BITFIELD_poc_usage_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lpoc->changes_uploaded_to_comm_server_awaiting_ACK,
							&lpoc->poc_usage,
							sizeof(lpoc->poc_usage),
							(lmem_used_by_this_poc + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_poc += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							POC_CHANGE_BITFIELD_master_valve_type_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lpoc->changes_uploaded_to_comm_server_awaiting_ACK,
							&lpoc->master_valve_type,
							sizeof(lpoc->master_valve_type),
							(lmem_used_by_this_poc + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_poc += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							POC_CHANGE_BITFIELD_flow_meter_type_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lpoc->changes_uploaded_to_comm_server_awaiting_ACK,
							&lpoc->flow_meter,
							sizeof(lpoc->flow_meter),
							(lmem_used_by_this_poc + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_poc += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							POC_CHANGE_BITFIELD_bypass_number_of_levels,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lpoc->changes_uploaded_to_comm_server_awaiting_ACK,
							&lpoc->bypass_number_of_levels,
							sizeof(lpoc->bypass_number_of_levels),
							(lmem_used_by_this_poc + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_poc += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							POC_CHANGE_BITFIELD_budget_gallons_entry_option_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lpoc->changes_uploaded_to_comm_server_awaiting_ACK,
							&lpoc->budget_gallons_entry_option,
							sizeof(lpoc->budget_gallons_entry_option),
							(lmem_used_by_this_poc + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_poc += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							POC_CHANGE_BITFIELD_budget_calc_percent_of_et_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lpoc->changes_uploaded_to_comm_server_awaiting_ACK,
							&lpoc->budget_calculation_percent_of_et,
							sizeof(lpoc->budget_calculation_percent_of_et),
							(lmem_used_by_this_poc + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_poc += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							POC_CHANGE_BITFIELD_budget_gallons_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lpoc->changes_uploaded_to_comm_server_awaiting_ACK,
							&lpoc->budget_gallons,
							sizeof(lpoc->budget_gallons),
							(lmem_used_by_this_poc + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					lmem_used_by_this_poc += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
							pucp,
							&lbitfield_of_changes_to_send,
							POC_CHANGE_BITFIELD_has_pump_attached_bit,
							lbitfield_of_changes_to_use_to_determine_what_to_send,
							&lpoc->changes_uploaded_to_comm_server_awaiting_ACK,
							&lpoc->has_pump_attached,
							sizeof(lpoc->has_pump_attached),
							(lmem_used_by_this_poc + rv + pmem_used_so_far),
							pmem_used_so_far_is_less_than_allocated_memory,
							pallocated_memory,
							preason_data_is_being_built );

					// ----------

					// 2/10/2016 rmd : If we saw any changed bits and ACTUALLY ADDED CONTENT BEYOND THE ORIGINAL
					// OVERHEAD then complete out the message by placing the bitfield in the overhead and
					// updating function housekeeping.
					if( lmem_used_by_this_poc > lmem_overhead_per_poc )
					{
						lnum_changed_pocs += 1;

						memcpy( llocation_of_bitfield, &lbitfield_of_changes_to_send, sizeof(lbitfield_of_changes_to_send) );
						
						rv += lmem_used_by_this_poc;
					}
					else
					{
						// 2/10/2016 rmd : Else back out from the message the overhead already added.
						*pucp -= lmem_overhead_per_poc;
					}
					
				}

				lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );
			}

			// ----------
			
			// 1/14/2015 ajv : If we've added the number of changes but didn't actually add any data
			// because we hit our limit, back out the number of changes.
			if( lnum_changed_pocs > 0 )
			{
				memcpy( llocation_of_num_changed_pocs, &lnum_changed_pocs, sizeof(lnum_changed_pocs) );
			}
			else
			{
				*pucp -= sizeof( lnum_changed_pocs );

				rv = 0;
			}
		}
		else
		{
			// 2/11/2016 rmd : We couldn't do anything, meaning couldn't even add in the
			// num_of_changed_sensors. In that case RV is still 0 and pucp hasn't moved so nothing to
			// do here.
		}
	}

	// ----------

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Copies the contents of the passed in group into GuiVars which are used for
 * editing by the easyGUI library.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *        passed in group is not deleted during this process.
 *
 * @executed Executed within the context of the display processing task when a
 *           new screen is being drawn.
 * 
 * @param ppoc A pointer to the POC group.
 *
 * @author 9/14/2011 Adrianusv
 *
 * @revisions
 *    9/14/2011 Initial release
 */
static UNS_32	g_POC_system_gid;

extern void POC_copy_group_into_guivars( const UNS_32 ppoc_index_0 )
{
	POC_GROUP_STRUCT *lpoc;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc = POC_get_group_at_this_index( ppoc_index_0 );

	g_GROUP_ID = nm_GROUP_get_group_ID( lpoc );

	strlcpy( GuiVar_GroupName, nm_GROUP_get_name( lpoc ), sizeof(GuiVar_GroupName) );

	GuiVar_POCBoxIndex = lpoc->box_index_0;

	GuiVar_POCDecoderSN1 = lpoc->decoder_serial_number[ 0 ];
	GuiVar_POCDecoderSN2 = lpoc->decoder_serial_number[ 1 ];
	GuiVar_POCDecoderSN3 = lpoc->decoder_serial_number[ 2 ];

	GuiVar_POCPhysicallyAvailable = lpoc->show_this_poc;

	GuiVar_POCType = lpoc->poc_type__file_type;

	GuiVar_POCUsage = lpoc->poc_usage;

	GuiVar_POCMVType1 = lpoc->master_valve_type[0];

	GuiVar_POCFlowMeterType1 = lpoc->flow_meter[ 0 ].flow_meter_choice;
	GuiVar_POCFlowMeterType2 = lpoc->flow_meter[ 1 ].flow_meter_choice;
	GuiVar_POCFlowMeterType3 = lpoc->flow_meter[ 2 ].flow_meter_choice;

	GuiVar_POCFMType1IsBermad = (GuiVar_POCFlowMeterType1 >= POC_FM_BERMAD_0150);
	GuiVar_POCFMType2IsBermad = (GuiVar_POCFlowMeterType2 >= POC_FM_BERMAD_0150);
	GuiVar_POCFMType3IsBermad = (GuiVar_POCFlowMeterType3 >= POC_FM_BERMAD_0150);

	GuiVar_POCFlowMeterK1 = ((float)(lpoc->flow_meter[ 0 ].kvalue_100000u) / 100000.0F);
	GuiVar_POCFlowMeterK2 = ((float)(lpoc->flow_meter[ 1 ].kvalue_100000u) / 100000.0F);
	GuiVar_POCFlowMeterK3 = ((float)(lpoc->flow_meter[ 2 ].kvalue_100000u) / 100000.0F);

	GuiVar_POCFlowMeterOffset1 = ((float)(lpoc->flow_meter[ 0 ].offset_100000u) / 100000.0F);
	GuiVar_POCFlowMeterOffset2 = ((float)(lpoc->flow_meter[ 1 ].offset_100000u) / 100000.0F);
	GuiVar_POCFlowMeterOffset3 = ((float)(lpoc->flow_meter[ 2 ].offset_100000u) / 100000.0F);

	GuiVar_POCReedSwitchType1 = lpoc->flow_meter[ 0 ].reed_switch;
	GuiVar_POCReedSwitchType2 = lpoc->flow_meter[ 1 ].reed_switch;
	GuiVar_POCReedSwitchType3 = lpoc->flow_meter[ 2 ].reed_switch;

	GuiVar_POCBypassStages = lpoc->bypass_number_of_levels;
	
	// Even though we don't use the system GID on this screen, grab it for when
	// we store the POC changes.
	g_POC_system_gid = lpoc->GID_irrigation_system;

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
}

// 3/22/2016 skc : Initialize UI elements for Budgets per POC
// Called on entrance to the screen where budget periods are set.
// A combination of POC and System variables are used to populate the fields.
extern void BUDGETS_copy_group_into_guivars( const UNS_32 ppoc_index_0, const UNS_32 pbudget_index_0 )
{
	POC_GROUP_STRUCT *lpoc;

	IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys;

	BUDGET_DETAILS_STRUCT bds;

	int i;

	// ----------

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc = POC_get_group_at_this_index( ppoc_index_0 );

	// ----------

	g_POC_system_gid = lpoc->GID_irrigation_system;

	ptr_sys = SYSTEM_get_group_with_this_GID( g_POC_system_gid );

	SYSTEM_get_budget_details( ptr_sys, &bds );

	// 3/23/2016 skc : Save the POC budget dates and values into global storage
	for( i=0; i<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS; ++i )
	{
		g_BUDGET_SETUP_meter_read_date[i] = bds.meter_read_date[i];

		g_BUDGET_SETUP_budget[i] = lpoc->budget_gallons[i];
	}		

	// 5/27/2016 skc : Show the associated mainline name
	strlcpy( GuiVar_BudgetMainlineName, nm_GROUP_get_name( ptr_sys), sizeof(GuiVar_BudgetMainlineName) );

	// ----------

	g_GROUP_ID = nm_GROUP_get_group_ID( lpoc );

	GuiVar_POCBoxIndex = lpoc->box_index_0;

	GuiVar_POCDecoderSN1 = lpoc->decoder_serial_number[ 0 ];

	GuiVar_POCType = lpoc->poc_type__file_type;
	
	// 8/14/2018 Ryan : Set annual budget based on saved monthly budgets
	GuiVar_BudgetAnnualValue = 0;
	
	for( i = 0; i < MONTHS_IN_A_YEAR; ++i )
	{
		GuiVar_BudgetAnnualValue += lpoc->budget_gallons[i];
	}
	
	// 10/15/2018 Ryan : Set holding variable to current budget value. 
	g_BUDGET_SETUP_annual_budget = GuiVar_BudgetAnnualValue;

	// 8/10/2018 Ryan : Set the GuiVar to the chosen budget entry option.
	GuiVar_BudgetEntryOptionIdx = lpoc->budget_gallons_entry_option;

	strlcpy( GuiVar_GroupName, nm_GROUP_get_name( lpoc ), sizeof(GuiVar_GroupName) );

	// ----------
	
	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Copies the contents of the passed in group into GuiVars which are used for
 * displaying information on the POC Preserves report screen.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *        passed in group is not deleted during this process.
 *
 * @executed Executed within the context of the display processing task when a
 *           new screen is being drawn.
 * 
 * @param ppoc A pointer to the POC group.
 *
 * @author 4/10/2012 Adrianusv
 *
 * @revisions
 *    4/10/2012 Initial release
 */
extern void POC_copy_preserve_info_into_guivars( UNS_32 const ppoc_preserves_index )
{
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	GuiVar_POCPreservesBoxIndex = poc_preserves.poc[ ppoc_preserves_index ].box_index;
	
	GuiVar_POCPreservesGroupID = poc_preserves.poc[ ppoc_preserves_index ].poc_gid;

	GuiVar_POCPreservesFileType = poc_preserves.poc[ ppoc_preserves_index ].poc_type__file_type;

	GuiVar_POCDecoderSN1 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 0 ].decoder_serial_number;

	GuiVar_POCPreservesDecoderSN1 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 0 ].decoder_serial_number;
	GuiVar_POCPreservesDecoderSN2 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 1 ].decoder_serial_number;
	GuiVar_POCPreservesDecoderSN3 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 2 ].decoder_serial_number;

	GuiVar_POCPreservesAccumPulses1 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 0 ].fm_accumulated_pulses_foal;
	GuiVar_POCPreservesAccumPulses2 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 1 ].fm_accumulated_pulses_foal;
	GuiVar_POCPreservesAccumPulses3 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 2 ].fm_accumulated_pulses_foal;

	GuiVar_POCPreservesAccumMS1 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 0 ].fm_accumulated_ms_foal;
	GuiVar_POCPreservesAccumMS2 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 1 ].fm_accumulated_ms_foal;
	GuiVar_POCPreservesAccumMS3 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 2 ].fm_accumulated_ms_foal;

	GuiVar_POCPreservesAccumGal1 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 0 ].accumulated_gallons_for_accumulators_foal;
	GuiVar_POCPreservesAccumGal2 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 1 ].accumulated_gallons_for_accumulators_foal;
	GuiVar_POCPreservesAccumGal3 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 2 ].accumulated_gallons_for_accumulators_foal;
	

	// 1/16/2015 rmd : We could also show the delta and the seconds since last pulse.


	GuiVar_POCPreservesDelivered5SecAvg1 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 0 ].delivered_5_second_average_gpm_irri;
	GuiVar_POCPreservesDelivered5SecAvg2 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 1 ].delivered_5_second_average_gpm_irri;
	GuiVar_POCPreservesDelivered5SecAvg3 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 2 ].delivered_5_second_average_gpm_irri;

	GuiVar_POCPreservesDeliveredMVCurrent1 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 0 ].mv_current_as_delivered_from_the_master_ma;
	GuiVar_POCPreservesDeliveredMVCurrent2 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 1 ].mv_current_as_delivered_from_the_master_ma;
	GuiVar_POCPreservesDeliveredMVCurrent3 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 2 ].mv_current_as_delivered_from_the_master_ma;

	GuiVar_POCPreservesDeliveredPumpCurrent1 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 0 ].pump_current_as_delivered_from_the_master_ma;
	GuiVar_POCPreservesDeliveredPumpCurrent2 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 1 ].pump_current_as_delivered_from_the_master_ma;
	GuiVar_POCPreservesDeliveredPumpCurrent3 = poc_preserves.poc[ ppoc_preserves_index ].ws[ 2 ].pump_current_as_delivered_from_the_master_ma;

	// 4/23/2012 rmd : If the gid is non-zero the record is in use. By convention.
	if( poc_preserves.poc[ ppoc_preserves_index ].poc_gid != 0 )
	{
		const POC_GROUP_STRUCT	*lpoc;
		
		// 2012.06.09 ajv : Since we're going to work with an item in the POC
		// list, make sure we take the appropriate mutex.
		xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

		// 4/16/2012 rmd : Now get the group. Then test stuff.
		lpoc = nm_GROUP_get_ptr_to_group_with_this_GID( &poc_group_list_hdr, poc_preserves.poc[ ppoc_preserves_index ].poc_gid, (false) );

		if( lpoc == NULL )
		{
			strlcpy( GuiVar_GroupName, "ERROR: NO MATCHING POC FOUND", sizeof(GuiVar_GroupName) );

			// 6/10/2014 ajv : To hide the letter, set it to an invalid value
			// (range is 0-11)
			GuiVar_POCBoxIndex = MAX_CHAIN_LENGTH;

			GuiVar_POCGroupID = 0;

			// 6/10/2014 ajv : To hide the type, set it to an invalid value
			// (range is 0x0a - 0x0c)
			GuiVar_POCType = 0;
		}
		else
		{
			strlcpy( GuiVar_GroupName, nm_GROUP_get_name(lpoc), sizeof(GuiVar_GroupName) );

			GuiVar_POCGroupID = nm_GROUP_get_group_ID( lpoc );

			GuiVar_POCBoxIndex = POC_get_box_index_0( GuiVar_POCGroupID );

			GuiVar_POCType = POC_get_type_of_poc( GuiVar_POCGroupID );
		}

		xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
	}
	else
	{
		strlcpy( GuiVar_GroupName, "ERROR: NO MATCHING POC FOUND", sizeof(GuiVar_GroupName) );

		// 6/10/2014 ajv : To hide the letter, set it to an invalid value (range
		// is 0-11)
		GuiVar_POCBoxIndex = MAX_CHAIN_LENGTH;

		GuiVar_POCGroupID = 0;

		// 6/10/2014 ajv : To hide the type, set it to an invalid value (range
		// is 0x0a - 0x0c)
		GuiVar_POCType = 0;
	}
	
	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Stores the group name. This routine is called when the user presses BACK to
 * close the on-screen keyboard so the list of groups is updated with the new
 * name.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user presses BACK to close the on-screen keyboard.
 *
 * @author 12/11/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void POC_extract_and_store_group_name_from_GuiVars( void )
{
	POC_GROUP_STRUCT	*lpoc;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc = POC_get_group_at_this_index( g_GROUP_list_item_index );

	if( lpoc != NULL )
	{
		SHARED_set_name_32_bit_change_bits( lpoc, GuiVar_GroupName, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, POC_get_change_bits_ptr( lpoc, CHANGE_REASON_KEYPAD ), &lpoc->changes_to_upload_to_comm_server, POC_CHANGE_BITFIELD_description_bit );
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Copies the contents of the GuiVars, used for editing by the easyGUI library,
 * into a temporary structure which is used for comparison with a group in the
 * list to identify changes.  Once this is done, the temporary group is passed
 * into the _store_changes routine where the settings are stored into memory.
 * 
 * @executed Executed within the context of the display processing task.
 * 
 * @author 8/11/2011 Adrianusv
 *
 * @revisions
 *    8/11/2011 Initial release
 *    4/5/2012  Added passing of (false) into set routine to indiciate the
 *  			change was made locally so change bits are set properly.
 *  			Modified so all memory allocation, storing of changes, and
 *  			memory deallocation are handled within this routine.
 */
extern void POC_extract_and_store_changes_from_GuiVars( void )
{
	POC_GROUP_STRUCT	*lpoc;

	POC_GROUP_STRUCT	*lfile_poc;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lpoc = mem_malloc( sizeof(POC_GROUP_STRUCT) );

	// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
	// update just the variable that's potentially changed in the UI.
	memcpy( lpoc, nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn(GuiVar_POCBoxIndex, GuiVar_POCType, GuiVar_POCDecoderSN1, POC_SET_TO_SHOW_ONLY), sizeof(POC_GROUP_STRUCT) );
	
	// ----------
	
	strlcpy( lpoc->base.description, GuiVar_GroupName, sizeof(lpoc->base.description) );

	// ----------

	lpoc->poc_usage = GuiVar_POCUsage;

	// ----------

	// 7/11/2014 ajv : When using a bypass manifold, prevent the same decoder from being
	// assigned to multiple levels.
	// 
	// 1/15/2015 ajv : However, only examine the third level if it's actually a three level
	// bypass manifold.
	if( ((GuiVar_POCDecoderSN1 == GuiVar_POCDecoderSN2) && (GuiVar_POCDecoderSN1 != DECODER_SERIAL_NUM_DEFAULT)) ||
		((GuiVar_POCBypassStages == POC_BYPASS_LEVELS_MAX) && (GuiVar_POCDecoderSN2 == GuiVar_POCDecoderSN3) && (GuiVar_POCDecoderSN2 != DECODER_SERIAL_NUM_DEFAULT)) ||
		((GuiVar_POCBypassStages == POC_BYPASS_LEVELS_MAX) && (GuiVar_POCDecoderSN1 == GuiVar_POCDecoderSN3) && (GuiVar_POCDecoderSN3 != DECODER_SERIAL_NUM_DEFAULT)) )
	{
		lfile_poc = POC_get_group_at_this_index( g_GROUP_list_item_index );

		GuiVar_POCDecoderSN1 = POC_get_decoder_serial_number_for_this_poc_gid_and_level_0( lfile_poc->base.group_identity_number, 0 );
		GuiVar_POCDecoderSN2 = POC_get_decoder_serial_number_for_this_poc_gid_and_level_0( lfile_poc->base.group_identity_number, 1 );

		if( GuiVar_POCBypassStages == POC_BYPASS_LEVELS_MAX )
		{
			GuiVar_POCDecoderSN3 = POC_get_decoder_serial_number_for_this_poc_gid_and_level_0( lfile_poc->base.group_identity_number, 2 );
		}
		else
		{
			// 1/15/2015 ajv : TODO If the bypass manifold only has two levels in use, should we clean
			// up the third level just to ensure we're not going to run into trouble later? For example,
			// if a duplicate serial number somehow ends up in the third level of the bypass and someone
			// changes it from a two level to a three level, they may end up in a loop they can't get
			// out of.
		}


		DIALOG_draw_ok_dialog( GuiStruct_dlgDuplicateDecoders_0 );
	}

	lpoc->decoder_serial_number[ 0 ] = GuiVar_POCDecoderSN1;
	lpoc->decoder_serial_number[ 1 ] = GuiVar_POCDecoderSN2;
	lpoc->decoder_serial_number[ 2 ] = GuiVar_POCDecoderSN3;

	// ----------

	lpoc->master_valve_type[ 0 ] = GuiVar_POCMVType1;

	// 1/30/2015 ajv : Although we don't display the master valve 2 and 3 states, it's important
	// that we set them in the lpoc structure. Failure to do so will result in the values being
	// 0 - which is NORMALLY OPEN and an invalid configuration for the bypass manifold.
	lpoc->master_valve_type[ 1 ] = POC_MV_TYPE_NORMALLY_CLOSED;
	lpoc->master_valve_type[ 2 ] = POC_MV_TYPE_NORMALLY_CLOSED;

	if( GuiVar_POCType == POC__FILE_TYPE__DECODER_BYPASS )
	{
		// 1/16/2015 ajv : Verify the flow meters are sized smallest to largest and display a
		// warning to the user if they're not.
		if( GuiVar_POCBypassStages < POC_BYPASS_LEVELS_MAX )
		{
			if( ((GuiVar_POCFlowMeterType1 == POC_FM_FMBX) && (GuiVar_POCFlowMeterType2 < POC_FM_FMBX)) || 
				((GuiVar_POCFlowMeterType1 != POC_FM_FMBX) && (FLOW_METER_SIZE_ORDER[ GuiVar_POCFlowMeterType1 ] > FLOW_METER_SIZE_ORDER[ GuiVar_POCFlowMeterType2 ])) )
			{
				// ERROR - FM1 > FM2
			}
		}
		else // if( GuiVar_POCBypassStages == POC_BYPASS_LEVELS_MAX )
		{
			if( ((GuiVar_POCFlowMeterType1 == POC_FM_FMBX) && (GuiVar_POCFlowMeterType2 < POC_FM_FMBX)) || 
				((GuiVar_POCFlowMeterType1 == POC_FM_FMBX) && (GuiVar_POCFlowMeterType3 < POC_FM_FMBX)) || 
				((GuiVar_POCFlowMeterType2 == POC_FM_FMBX) && (GuiVar_POCFlowMeterType3 < POC_FM_FMBX)) ||
				((GuiVar_POCFlowMeterType1 != POC_FM_FMBX) && (FLOW_METER_SIZE_ORDER[ GuiVar_POCFlowMeterType1 ] > FLOW_METER_SIZE_ORDER[ GuiVar_POCFlowMeterType2 ])) ||
				((GuiVar_POCFlowMeterType2 != POC_FM_FMBX) && (FLOW_METER_SIZE_ORDER[ GuiVar_POCFlowMeterType2 ] > FLOW_METER_SIZE_ORDER[ GuiVar_POCFlowMeterType3 ])) ||
				((GuiVar_POCFlowMeterType1 != POC_FM_FMBX) && (FLOW_METER_SIZE_ORDER[ GuiVar_POCFlowMeterType1 ] > FLOW_METER_SIZE_ORDER[ GuiVar_POCFlowMeterType3 ])) )
			{
				// ERROR - FM1 > FM2 or FM2 > FM3 or FM1 > FM3
			}
		}
	}

	lpoc->flow_meter[ 0 ].flow_meter_choice = GuiVar_POCFlowMeterType1;
	lpoc->flow_meter[ 1 ].flow_meter_choice = GuiVar_POCFlowMeterType2;
	lpoc->flow_meter[ 2 ].flow_meter_choice = GuiVar_POCFlowMeterType3;

	lpoc->flow_meter[ 0 ].kvalue_100000u = (UNS_32)(GuiVar_POCFlowMeterK1 * 100000.0F);
	lpoc->flow_meter[ 1 ].kvalue_100000u = (UNS_32)(GuiVar_POCFlowMeterK2 * 100000.0F);
	lpoc->flow_meter[ 2 ].kvalue_100000u = (UNS_32)(GuiVar_POCFlowMeterK3 * 100000.0F);

	lpoc->flow_meter[ 0 ].offset_100000u = (INT_32)(GuiVar_POCFlowMeterOffset1 * 100000.0F);
	lpoc->flow_meter[ 1 ].offset_100000u = (INT_32)(GuiVar_POCFlowMeterOffset2 * 100000.0F);
	lpoc->flow_meter[ 2 ].offset_100000u = (INT_32)(GuiVar_POCFlowMeterOffset3 * 100000.0F);

	lpoc->flow_meter[ 0 ].reed_switch = GuiVar_POCReedSwitchType1;
	lpoc->flow_meter[ 1 ].reed_switch = GuiVar_POCReedSwitchType2;
	lpoc->flow_meter[ 2 ].reed_switch = GuiVar_POCReedSwitchType3;
	
	// ----------

	lpoc->bypass_number_of_levels = GuiVar_POCBypassStages;

	// ----------

	// Even though we didn't use the system GID on this screen, we need to
	// ensure this temporary group's GID is what it was when we entered the
	// screen.
	lpoc->GID_irrigation_system = g_POC_system_gid;

	nm_POC_store_changes( lpoc, g_GROUP_creating_new, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

	// ----------

	mem_free( lpoc );

	// ----------

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	// Reset the Creating New flag to ensure we don't accidentally create
	// another new group when using the BACK key.
	g_GROUP_creating_new = (false);
}

/* ---------------------------------------------------------- */
/**
 * Copies the contents of the GuiVars, used for editing by the easyGUI library,
 * into a temporary structure <lpcc> which is used for comparison with a group in the
 * list to identify changes.  Once this is done, the temporary group is passed
 * into the _store_changes routine where the settings are stored into memory.
 * 
 * @executed Executed within the context of the display processing task.
 * 
 * @author 8/11/2011 Adrianusv
 *
 * @revisions
 *    8/11/2011 Initial release
 *    4/5/2012  Added passing of (false) into set routine to indiciate the
 *  			change was made locally so change bits are set properly.
 *  			Modified so all memory allocation, storing of changes, and
 *  			memory deallocation are handled within this routine.
 */
extern void BUDGETS_extract_and_store_changes_from_GuiVars( void )
{
	POC_GROUP_STRUCT	*lpoc;

	IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys;

	UNS_32 i;

	// ----------

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	// 3/24/2016 skc : Save the budget period dates
	ptr_sys = SYSTEM_get_group_with_this_GID( g_POC_system_gid );

	for( i=0; i<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS; i++ )
	{
		nm_SYSTEM_set_budget_period( ptr_sys, 
									i,
									g_BUDGET_SETUP_meter_read_date[i], 
									CHANGE_generate_change_line, 
									CHANGE_REASON_KEYPAD, 
									FLOWSENSE_get_controller_index(), 
									CHANGE_set_change_bits,
									SYSTEM_get_change_bits_ptr( ptr_sys, CHANGE_REASON_KEYPAD ) );

	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	// ----------

	// 5.19.2016 rmd : Take the MUTEX needed for the memcpy and store_changes function now.
	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc = mem_malloc( sizeof(POC_GROUP_STRUCT) );

	// 4/9/2016 ajv : Copy the contents of the file system's group into this one and then set
	// update just the variable that's potentially changed in the UI.
	memcpy( lpoc, nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn(GuiVar_POCBoxIndex, GuiVar_POCType, GuiVar_POCDecoderSN1, POC_SET_TO_SHOW_ONLY), sizeof(POC_GROUP_STRUCT) );

	for( i=0; i<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS; i++ )
	{
		lpoc->budget_gallons[i] = g_BUDGET_SETUP_budget[i];
	}

	nm_POC_store_changes( lpoc, g_GROUP_creating_new, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	mem_free( lpoc );
}

/* ---------------------------------------------------------- */
/**
 * Copies the group's name and settings into the Group List GuiVars to display
 * on the list of groups.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *  	  group whose value is being read isn't deleted during this process.
 *
 * @executed Executed within the context of the display processing task when the
 *           group list is displayed.
 * 
 * @param pindex_0 The 0-based index into the irrigation priorities group list.
 *
 * @author 9/14/2011 Adrianusv
 *
 * @revisions
 *    9/14/2011 Initial release
 */
extern void nm_POC_load_group_name_into_guivar( const INT_16 pindex_0_i16 )
{
	POC_GROUP_STRUCT *lpoc;

	lpoc = POC_get_group_at_this_index( (UNS_32)pindex_0_i16 );

	if( nm_OnList(&poc_group_list_hdr, lpoc) == (true) )
	{
		strlcpy( GuiVar_itmGroupName, nm_GROUP_get_name( lpoc ), NUMBER_OF_CHARS_IN_A_NAME );
	}
	else
	{
		Alert_group_not_found();
	}
}

/* ---------------------------------------------------------- */
extern UNS_32 POC_get_index_using_ptr_to_poc_struct( const POC_GROUP_STRUCT *const ppoc )
{
	POC_GROUP_STRUCT	*lpoc;

	UNS_32	rv;

	UNS_32	i;

	rv = 0;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc = nm_ListGetFirst( &poc_group_list_hdr );

	for( i = 0; i < nm_ListGetCount( &poc_group_list_hdr ); ++i )
	{
		if( lpoc == ppoc )
		{
			rv = i;

			break;
		}

		lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the terminal POC connected to the controller with the specified
 * box_index. If no matching POC is found, a NULL is returned.
 * 
 * @mutex_requirements Calling function must have the poc_recursive_MUTEX mutex
 * to ensure the group that's found isn't deleted before it's used.
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the COMM MNGR task when a new
 * hardware structure is received from the TP Micro.
 * 
 * @param pbox_index The 0-based box (controller) index which matches the
 * controller's communication address.
 * 
 * @return void* A pointer (of type POC_GROUP_STRUCT) to the POC that's found.
 * If no POC is found for the serial number, NULL is returned.
 *
 * @author 8/24/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void *nm_POC_get_pointer_to_terminal_poc_with_this_box_index_0( const UNS_32 pbox_index )
{
	POC_GROUP_STRUCT *lpoc;

	lpoc = nm_ListGetFirst( &poc_group_list_hdr );

	while( lpoc != NULL )
	{
		if( (lpoc->box_index_0 == pbox_index) && (lpoc->poc_type__file_type == POC__FILE_TYPE__TERMINAL) )
		{
			break;
		}

		lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );
	}

	return ( lpoc );
}

/* ---------------------------------------------------------- */
extern void *nm_POC_get_pointer_to_bypass_poc_with_this_box_index_0( const UNS_32 pbox_index_0, const BOOL_32 ptest_show_this_poc_to_be_true )
{
	POC_GROUP_STRUCT *lpoc;

	lpoc = nm_ListGetFirst( &poc_group_list_hdr );

	while( lpoc != NULL )
	{
		if( (lpoc->box_index_0 == pbox_index_0) && (lpoc->poc_type__file_type == POC__FILE_TYPE__DECODER_BYPASS) )
		{
			if( ptest_show_this_poc_to_be_true )
			{
				if( lpoc->show_this_poc )
				{
					break;
				}
			}
			else
			{
				break;
			}
		}

		lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );
	}

	return ( lpoc );
}

/* ---------------------------------------------------------- */
extern void *nm_POC_get_pointer_to_poc_with_this_gid_and_box_index( UNS_32 const ppoc_gid, UNS_32 const pbox_index )
{
	POC_GROUP_STRUCT *lpoc;

	lpoc = nm_ListGetFirst( &poc_group_list_hdr );

	while( lpoc != NULL )
	{
		if( (lpoc->box_index_0 == pbox_index) && (lpoc->base.group_identity_number == ppoc_gid) )
		{
			break;
		}

		lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );
	}

	return ( lpoc );
}

/* ---------------------------------------------------------- */
extern POC_GROUP_STRUCT *nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn( const UNS_32 pbox_index_0, const UNS_32 ppoc_file_type, const UNS_32 pdecoder_sn, const BOOL_32 ptest_show_this_poc_to_be_true )
{
	// 10/29/2014 rmd : Caller must be holding the poc_list MUTEX.

	POC_GROUP_STRUCT *rv;

	POC_GROUP_STRUCT *lpoc;
	
	// ----------
	
	rv = NULL;

	lpoc = nm_ListGetFirst( &poc_group_list_hdr );

	while( (lpoc != NULL) && (rv == NULL) )
	{
		// 5/31/2016 rmd : Check against the box index.
		if( lpoc->box_index_0 == pbox_index_0 )
		{
			// 6/1/2016 rmd : Handle the physically available criteria.
			if( (ptest_show_this_poc_to_be_true && lpoc->show_this_poc) || (!ptest_show_this_poc_to_be_true) )
			{
				if( lpoc->poc_type__file_type == ppoc_file_type )
				{
					if( ppoc_file_type == POC__FILE_TYPE__TERMINAL )
					{
						// 5/31/2016 rmd : We are done. Found the match.
						rv = lpoc;
					}
					else
					if( ppoc_file_type == POC__FILE_TYPE__DECODER_SINGLE )
					{
						if( lpoc->decoder_serial_number[ 0 ] == pdecoder_sn )
						{
							// 5/31/2016 rmd : Got a match on all 3 levels.
							rv = lpoc;
						}
					}
					else
					if( ppoc_file_type == POC__FILE_TYPE__DECODER_BYPASS )
					{
						// 6/1/2016 rmd : Well this is a match. There can ONLY be ONE bypass decoder per box
						// regardless of serial numbers.
						rv = lpoc;
					}

				}  // of file type match

			}  // of physically available test

		}  // of box_index match
		
		lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );

	}  // of while loop
	
	return ( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the group with the specified group ID. This is a wrapper
 * for the nm_GROUP_get_ptr_to_group_at_this_location_in_list function so we
 * don't need to expose the list header.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *        group that's found isn't deleted before it's used.
 *
 * @executed Executed within the context of the display processing task when the
 *  		 list and group screens are drawn; within the key processing task
 *  		 when the user navigates through the group list screen.
 * 
 * @param pindex_0 The index of the group to retrieve - 0 based.
 * 
 * @return void* A pointer to the group with the specified group ID. Or a NULL if the index
 * is beyond the lists range.
 *
 * @author 9/15/2011 Adrianusv
 *
 * @revisions
 *    9/15/2011 Initial release
 */
extern void *POC_get_group_at_this_index( const UNS_32 pindex_0 )
{
	POC_GROUP_STRUCT *lpoc;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc = nm_GROUP_get_ptr_to_group_at_this_location_in_list( &poc_group_list_hdr, pindex_0 );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( lpoc );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Returns the GID for the POC group at the index location within the
    list.

    @CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the DISPLAY PROCESSING task. When the poc
    accumulator report is being drawn.

    @RETURN The GID for the group at pindex location in the list of poc's. Or 0 if group not
    found.

	@ORIGINAL 2012.08.01 rmd

	@REVISIONS (none)
*/
extern UNS_32 POC_get_gid_of_group_at_this_index( const UNS_32 pindex_0 )
{
	UNS_32	rv;
	
	rv = 0;
	
	POC_GROUP_STRUCT *lpoc;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc = nm_GROUP_get_ptr_to_group_at_this_location_in_list( &poc_group_list_hdr, pindex_0 );
	
	if( lpoc != NULL )
	{
		rv = lpoc->base.group_identity_number;
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Accepts a group ID (e.g., GuiVar_GroupID) and returns the position of that
 * index in the associated list. This is used to ensure the group the user
 * created or was editing is higlighted when they return to the list of groups.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *  	  list doesn't change between locating the index and using it.
 *
 * @executed Executed within the context of the key processing task when the
 *           user selects a group on the group list screen.
 * 
 * @param ppoc_ID The group ID
 * 
 * @return UNS_32 A 0-based index of the requested group into the associated
 *                list.
 *
 * @author 12/15/2011 Adrianusv
 *
 * @revisions
 *    12/15/2011 Initial release
 */
extern UNS_32 POC_get_index_for_group_with_this_GID( const UNS_32 ppoc_ID )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	rv = nm_GROUP_get_index_for_group_with_this_GID( &poc_group_list_hdr, ppoc_ID );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the last POC group ID by retrieving the number_of_groups_ever_created
 * value from the head of the list.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure
 *  	  another group isn't added to the list between the number being
 *  	  retrieved and being used.
 *
 * @executed Executed within the context of the startup task when the stations 
 *  		 list is created for the first time; also executed within the
 *  		 display processing and key processing task as the user enters and
 *  		 leaves the editing screen.
 * 
 * @return UNS_32 The group ID of the last POC group.
 *
 * @author 9/7/2011 Adrianusv
 *
 * @revisions
 *    9/7/2011   Initial release
 *    2/10/2012	 Renamed to include nm_ prefix
 */
extern UNS_32 POC_get_last_group_ID( void )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	rv = ((GROUP_BASE_DEFINITION_STRUCT*)poc_group_list_hdr.phead)->number_of_groups_ever_created;

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the number of items in the list. This is a wrapper for the 
 * nm_ListGetCount macro so the list header doesn't need to be exposed. 
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure a
 *  	  group isn't added or removed between the count being retrieved and
 *  	  being used.
 *
 * @executed Executed within the context of the display processing task when the 
 *  		 list of groups is drawn; within the context of the key processing
 *  		 task when the user moves up and down the list of groups and when
 *  		 the user tries to delete a group.
 * 
 * @return UNS_32 The number of items in the list.
 *
 * @author 9/7/2011 Adrianusv
 *
 * @revisions
 *    9/7/2011   Initial release
 *    2/10/2012	 Renamed to include nm_ prefix
 */
extern UNS_32 POC_get_list_count( void )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	rv = nm_ListGetCount( &poc_group_list_hdr );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the irrigation system group ID associated with a particular POC. If the
 * group isn't valid (e.g., is NULL or is not on the list), an alert is
 * generated and 0 is returned.
 * 
 * @executed Executed within the context of the display processing task when the 
 *  		 POC scroll box is drawn on the irrigation system screen; within the
 *  		 context of the key processing task when the user navigates up or
 *  		 down the scroll box.
 * 
 * @param ppoc A pointer to the POC.
 * 
 * @return UNS_32 The irrigation system GID associated with the specified POC.
 *
 * @author 8/12/2011 Adrianusv
 *
 * @revisions
 *    8/12/2011 Initial release
 */
extern UNS_32 POC_get_GID_irrigation_system( POC_GROUP_STRUCT *const ppoc )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	// 2012.01.26 rmd : Return a 0 if out of range. I just picked some range numbers. Don't
	// really know what the upper limit should be on the system gid.
	rv = SHARED_get_uint32_32_bit_change_bits_group( ppoc,
													 &ppoc->GID_irrigation_system,
													 0,  // the min
													 SYSTEM_get_last_group_ID(),  // the max
													 0,  // the default
													 &nm_POC_set_GID_irrigation_system,  // the set function if out of range
													 &ppoc->changes_to_send_to_master,
													 POC_database_field_names[ POC_CHANGE_BITFIELD_GID_irri_system_bit ] );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Return the gid for the system this poc belongs to.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the TD_CHECK task. And likely the
    COMM_MNGR task. So far used when a poc_preserves record is initially registered or
    re-synced to the poc file or resynced tot he system_preserves.
	
    @RETURN The gid for the system that owns this poc. Else 0. As that is the
    way to indicate the poc isn't attached to any system.

	@AUTHOR 2012.01.26 rmd
*/
extern UNS_32 POC_get_GID_irrigation_system_using_POC_gid( const UNS_32 ppoc_gid )
{
	POC_GROUP_STRUCT	*lpoc_group;

	UNS_32	rv;

	// 8/2/2012 rmd : For the GROUP_get_ptr function we are to be holding this MUTEX.
	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc_group = nm_GROUP_get_ptr_to_group_with_this_GID( &poc_group_list_hdr, ppoc_gid, (false) );

	rv = POC_get_GID_irrigation_system( lpoc_group );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Load the array of 3 flow meter details pointed to by the fms_ptr. The pointer
 * must actually point to an array of POC_BYPASS_LEVELS_MAX (3) of
 * POC_FLOW_METER_STRUCT.
 * 
 * @executed Executed wihtin the context of the TD_CHECK task. And maybe the
 * COMM_MNGR task.
 * 
 * @param ppoc_gid The group ID of the POC.
 * @param pfms_ptr A pointer to an array of 3 of the structure which contains the flow meter
 *                 type, K value, and offset asociated with the specified POC.
 * 
 * @return BOOL_32 False if the gid is NOT found; otherwise, true. If function
 *  	         returns false, the caller should NOT use the K and O values to
 *  			 compute a flow rate. Perhaps the caller would represent the
 *               flow rate to be 0.
 *
 * @author 1/13/2011 BobD
 *
 * @revisions
 *    1/13/2011 Initial release
 *    2/10/2012 Updated to return correct K value and offset if the flow meter
 *              is a preset flow meter.
 */
extern BOOL_32 POC_get_fm_choice_and_rate_details( const UNS_32 ppoc_gid, POC_FM_CHOICE_AND_RATE_DETAILS pfms_ptr[] )
{
	POC_GROUP_STRUCT	*lpoc_group;

	char	lfield_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32	i;

	BOOL_32	rv;

	rv = (false);

	// 8/2/2012 rmd : For the GROUP_get_ptr function we are to be holding this MUTEX.
	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc_group = nm_GROUP_get_ptr_to_group_with_this_GID( &poc_group_list_hdr, ppoc_gid, (false) );

	if( lpoc_group != NULL )
	{
		// It is assumed the pointer the caller gave us points to an array of the flow meter
		// structs! Else something very bad will almost for sure happen.
		for( i = 0; i < POC_BYPASS_LEVELS_MAX; ++i )
		{
			snprintf( lfield_name, sizeof(lfield_name), "%sChoice%d", POC_database_field_names[ POC_CHANGE_BITFIELD_flow_meter_type_bit ], (i + 1) );

			// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
			// the value by setting it to the default.
			if( RangeCheck_uint32( &lpoc_group->flow_meter[ i ].flow_meter_choice, POC_FM_CHOICE_MIN, POC_FM_CHOICE_MAX, POC_FM_CHOICE_DEFAULT, NULL, lfield_name ) == (false) )
			{
				nm_POC_set_fm_type( lpoc_group,
									(i + 1),	// The set routine expects level to be 1-based
									lpoc_group->flow_meter[ i ].flow_meter_choice,
									CHANGE_do_not_generate_change_line,
									CHANGE_REASON_RANGE_CHECK_FAILED,
									FLOWSENSE_get_controller_index(),
									CHANGE_set_change_bits,
									&lpoc_group->changes_to_send_to_master );
			}

			pfms_ptr[ i ].flow_meter_choice = lpoc_group->flow_meter[ i ].flow_meter_choice;

			// 2012.02.10 ajv : We are expecting the K value and offset returned to be valid for the
			// choice of flow meter. Since only the FMBX has the K value and offset set by the user, we
			// need to pass back the appropriate K value and offset for preset flow meter sizes.
			if( pfms_ptr[ i ].flow_meter_choice == POC_FM_FMBX )
			{
				snprintf( lfield_name, sizeof(lfield_name), "%sK_Value%d", POC_database_field_names[ POC_CHANGE_BITFIELD_flow_meter_type_bit ], (i + 1) );

				// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
				// the value by setting it to the default.
				if( RangeCheck_uint32( &(lpoc_group->flow_meter[ i ].kvalue_100000u), (UNS_32)(POC_FM_KVALUE_MIN * 100000.0F), (UNS_32)(POC_FM_KVALUE_MAX * 100000.0F), (UNS_32)(POC_FM_KVALUE_DEFAULT * 100000.0F), NULL, lfield_name ) == (false) )
				{
					nm_POC_set_kvalue( lpoc_group,
									   (i + 1),	// The set routine expects level to be 1-based
									   lpoc_group->flow_meter[ i ].kvalue_100000u,
									   CHANGE_do_not_generate_change_line,
									   CHANGE_REASON_RANGE_CHECK_FAILED,
									   FLOWSENSE_get_controller_index(),
									   CHANGE_set_change_bits,
									   &lpoc_group->changes_to_send_to_master );
				}

				pfms_ptr[ i ].kvalue_100000u = lpoc_group->flow_meter[ i ].kvalue_100000u;

				// ----------

				snprintf( lfield_name, sizeof(lfield_name), "%sOffset_Value%d", POC_database_field_names[ POC_CHANGE_BITFIELD_flow_meter_type_bit ], (i + 1) );

				// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
				// the value by setting it to the default.
				if( RangeCheck_int32( &(lpoc_group->flow_meter[ i ].offset_100000u), (INT_32)(POC_FM_OFFSET_MIN * 100000.0F), (INT_32)(POC_FM_OFFSET_MAX * 100000.0F), (INT_32)(POC_FM_OFFSET_DEFAULT * 100000.0F), NULL, lfield_name ) == (false) )
				{
					nm_POC_set_offset( lpoc_group,
									   (i + 1),	// The set routine expects level to be 1-based
									   lpoc_group->flow_meter[ i ].offset_100000u,
									   CHANGE_do_not_generate_change_line,
									   CHANGE_REASON_RANGE_CHECK_FAILED,
									   FLOWSENSE_get_controller_index(),
									   CHANGE_set_change_bits,
									   &lpoc_group->changes_to_send_to_master );
				}

				pfms_ptr[ i ].offset_100000u = lpoc_group->flow_meter[ i ].offset_100000u;
			}
			else
			{
				pfms_ptr[ i ].kvalue_100000u = (UNS_32)(FLOW_METER_KVALUES[ pfms_ptr[ i ].flow_meter_choice ] * 100000.0F);

				pfms_ptr[ i ].offset_100000u = (INT_32)(FLOW_METER_OFFSETS[ pfms_ptr[ i ].flow_meter_choice ] * 100000.0F);
			}
			
			snprintf( lfield_name, sizeof(lfield_name), "%sReedSwitch%d", POC_database_field_names[ POC_CHANGE_BITFIELD_flow_meter_type_bit ], (i + 1) );

			// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
			// the value by setting it to the default.
			if( RangeCheck_uint32( &lpoc_group->flow_meter[ i ].reed_switch, POC_FM_REED_SWITCH_MIN, POC_FM_REED_SWITCH_MAX, POC_FM_REED_SWITCH_DEFAULT, NULL, lfield_name ) == (false) )
			{
				nm_POC_set_reed_switch( lpoc_group,
										(i + 1),	// The set routine expects level to be 1-based
										lpoc_group->flow_meter[ i ].reed_switch,
										CHANGE_do_not_generate_change_line,
										CHANGE_REASON_RANGE_CHECK_FAILED,
										FLOWSENSE_get_controller_index(),
										CHANGE_set_change_bits,
										&lpoc_group->changes_to_send_to_master );
			}

			pfms_ptr[ i ].reed_switch = lpoc_group->flow_meter[ i ].reed_switch;
		}

		rv = (true);
	}
	else
	{
		for( i = 0; i < POC_BYPASS_LEVELS_MAX; ++i )
		{
			pfms_ptr[ i ].flow_meter_choice = POC_FM_CHOICE_DEFAULT;

			pfms_ptr[ i ].kvalue_100000u = (UNS_32)(POC_FM_KVALUE_DEFAULT * 100000.0F);

			pfms_ptr[ i ].offset_100000u = (INT_32)(POC_FM_OFFSET_DEFAULT * 100000.0F);

			pfms_ptr[ i ].reed_switch = POC_FM_REED_SWITCH_DEFAULT;
		}

		Alert_group_not_found();
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 POC_get_usage( const UNS_32 ppoc_gid )
{
	POC_GROUP_STRUCT	*lpoc_group;

	UNS_32	rv;

	// 8/2/2012 rmd : For the GROUP_get_ptr function we are to be holding this MUTEX.
	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc_group = nm_GROUP_get_ptr_to_group_with_this_GID( &poc_group_list_hdr, ppoc_gid, (false) );

	rv = SHARED_get_uint32_32_bit_change_bits_group( lpoc_group,
													 &lpoc_group->poc_usage,
													 POC_USAGE_MIN,
													 POC_USAGE_MAX,
													 POC_USAGE_DEFAULT,
													 &nm_POC_set_poc_usage,
													 &lpoc_group->changes_to_send_to_master,
													 POC_database_field_names[ POC_CHANGE_BITFIELD_poc_usage_bit ] );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Load the argument with the 3 master valve normally open/normally closed
    types. The argument pmv_ptr must point to an array of UNS_32 with POC_BYPASS_LEVELS_MAX
    elements.
	
	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task when we are preparing the
    system flow rate summary.
	
    @RETURN (false) if the ppoc_gid is not found. In which case all the valves will be
    marked as NC. (true) if sucessful.

	@ORIGINAL 2012.05.03 rmd

	@REVISIONS
*/
extern BOOL_32 POC_get_master_valve_NO_or_NC( const UNS_32 ppoc_gid, UNS_32 pmv_ptr[] )
{
	POC_GROUP_STRUCT	*lpoc_group;

	char	lfield_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32	i;

	BOOL_32	rv;

	rv = (false);

	// 8/2/2012 rmd : For the GROUP_get_ptr function we are to be holding this MUTEX.
	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc_group = nm_GROUP_get_ptr_to_group_with_this_GID( &poc_group_list_hdr, ppoc_gid, (false) );

	if( lpoc_group != NULL )
	{
		// It is assumed the pointer the caller gave us points to an array of UNS_32's. Else
		// something very bad will almost for sure happen.
		for( i=0; i<POC_BYPASS_LEVELS_MAX; i++ )
		{
			snprintf( lfield_name, sizeof(lfield_name), "%s%d", POC_database_field_names[ POC_CHANGE_BITFIELD_master_valve_type_bit ], (i + 1) );

			// 2/2/2015 ajv : If the range check fails, the range check routine automatically corrects
			// the value by setting it to the default.
			if( RangeCheck_uint32( &lpoc_group->master_valve_type[ i ], POC_MV_TYPE_NORMALLY_OPEN, POC_MV_TYPE_NORMALLY_CLOSED, POC_MV_TYPE_DEFAULT, NULL, lfield_name ) == (false) )
			{
				nm_POC_set_mv_type( lpoc_group,
									(i + 1),	// The set routine expects level to be 1-based
									lpoc_group->master_valve_type[ i ],
									CHANGE_do_not_generate_change_line,
									CHANGE_REASON_RANGE_CHECK_FAILED,
									FLOWSENSE_get_controller_index(),
									CHANGE_set_change_bits,
									&lpoc_group->changes_to_send_to_master );
			}

			pmv_ptr[ i ] = lpoc_group->master_valve_type[ i ];
		}

		rv = (true);
	}
	else
	{
		for( i=0; i<POC_BYPASS_LEVELS_MAX; i++ )
		{
			pmv_ptr[ i ] = POC_MV_TYPE_DEFAULT;
		}

		Alert_group_not_found();
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Return the controller serial number for the presented poc_gid.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the TD_CHECK task. And likely the
    COMM_MNGR task.
	
	@RETURN The controller serial number for the poc_gid parameter.

	@AUTHOR 2012.01.13 rmd
*/
extern UNS_32 POC_get_box_index_0( const UNS_32 ppoc_gid )
{
	POC_GROUP_STRUCT	*lpoc_group;

	UNS_32	rv;

	// 8/2/2012 rmd : For the GROUP_get_ptr function we are to be holding this MUTEX.
	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc_group = nm_GROUP_get_ptr_to_group_with_this_GID( &poc_group_list_hdr, ppoc_gid, (false) );

	rv = SHARED_get_uint32_32_bit_change_bits_group( lpoc_group,
													 &lpoc_group->box_index_0,
													 BOX_INDEX_MINIMUM,
													 BOX_INDEX_MAXIMUM,
													 FLOWSENSE_get_controller_letter(),
													 &nm_POC_set_box_index,
													 &lpoc_group->changes_to_send_to_master,
													 POC_database_field_names[ POC_CHANGE_BITFIELD_box_index_bit ] );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Return the POC type for the presented poc_gid.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the TD_CHECK task. And likely the
    COMM_MNGR task.
	
	@RETURN The type of poc for the gid.

	@AUTHOR 2012.01.13 rmd
*/
extern UNS_32 POC_get_type_of_poc( const UNS_32 ppoc_gid )
{
	POC_GROUP_STRUCT	*lpoc_group;

	UNS_32	rv;

	// 8/2/2012 rmd : For the GROUP_get_ptr function we are to be holding this MUTEX.
	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc_group = nm_GROUP_get_ptr_to_group_with_this_GID( &poc_group_list_hdr, ppoc_gid, (false) );

	rv = SHARED_get_uint32_32_bit_change_bits_group( lpoc_group,
													 &lpoc_group->poc_type__file_type,
													 POC__FILE_TYPE__MIN,
													 POC__FILE_TYPE__MAX,
													 POC__FILE_TYPE__DEFAULT,
													 &nm_POC_set_poc_type,
													 &lpoc_group->changes_to_send_to_master,
													 POC_database_field_names[ POC_CHANGE_BITFIELD_type_of_poc_bit ] );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 nm_get_decoder_serial_number_support( POC_GROUP_STRUCT *const ppoc, const UNS_32 plevel_0 )
{
	char	lfield_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32	rv;

	// 6/4/2014 rmd : Give him the level he is requesting. Our file should be clean. Meaning
	// unused levels should be 0 (non-bypass, terminal based, etc).
	if( plevel_0 < POC_BYPASS_LEVELS_MAX )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", POC_database_field_names[ POC_CHANGE_BITFIELD_decoder_serial_number_bit ], (plevel_0 + 1) );

		rv = SHARED_get_uint32_from_array_32_bit_change_bits_group( ppoc,
																	plevel_0,
																	&ppoc->decoder_serial_number[ plevel_0 ],
																	POC_BYPASS_LEVELS_MAX,
																	DECODER_SERIAL_NUM_MIN,
																	DECODER_SERIAL_NUM_MAX,
																	DECODER_SERIAL_NUM_DEFAULT,
																	&nm_POC_set_decoder_serial_number,
																	&ppoc->changes_to_send_to_master,
																	lfield_name );
	}
	else
	{
		Alert_index_out_of_range();

		rv = DECODER_SERIAL_NUM_DEFAULT;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 POC_get_decoder_serial_number_for_this_poc_and_level_0( const POC_GROUP_STRUCT *const ppoc, const UNS_32 plevel_0 )
{
	UNS_32	rv;

	// 8/2/2012 rmd : For the GROUP_get_ptr function we are to be holding this MUTEX.
	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	rv = nm_get_decoder_serial_number_support( (void *const)ppoc, plevel_0 );
	
	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 POC_get_decoder_serial_number_for_this_poc_gid_and_level_0( const UNS_32 ppoc_gid, const UNS_32 plevel_0 )
{
	POC_GROUP_STRUCT	*lpoc;

	UNS_32	rv;

	// 8/2/2012 rmd : For the GROUP_get_ptr function we are to be holding this MUTEX.
	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc = nm_GROUP_get_ptr_to_group_with_this_GID( &poc_group_list_hdr, ppoc_gid, (false) );

	// 8/2/2012 rmd : The messages from the TP Micro are reliant upon the decoder serial number
	// (in the poc_preserves) being 0 when the POC is not a decoder type. This function returns
	// the decoder serial number used during the poc_preserves registration.

	rv = nm_get_decoder_serial_number_support( lpoc, plevel_0 );
	
	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 POC_get_decoder_serial_number_index_0( const POC_GROUP_STRUCT *const ppoc, const UNS_32 pdecoder_serial_num )
{
	UNS_32	rv;

	rv = 0;

	if( ppoc != NULL )
	{
		if( ppoc->decoder_serial_number[ 0 ] == pdecoder_serial_num )
		{
			rv = 0;
		}
		else if( ppoc->decoder_serial_number[ 1 ] == pdecoder_serial_num )
		{
			rv = 1;
		}
		else if( ppoc->decoder_serial_number[ 2 ] == pdecoder_serial_num )
		{
			rv = 2;
		}
		else
		{
			Alert_group_not_found();
		}
	}
	else
	{
		Alert_func_call_with_null_ptr();
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns TRUE if the POC card and terminal board are connected. If the POC
 * isn't valid, an alert is generated and FALSE is returned.
 * 
 * @mutex_requirements (none)
 * 
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the COMM_MNGR task when
 * building a controller information token response.
 * 
 * @param ppoc A pointer to the point of connection.
 * 
 * @return BOOL_32 True if the POC's card and terminal are both connected;
 * otherwise, false. Also returns false is the POC is not valid.
 *
 * @author 9/4/2013 Adrianusv
 *
 * @revisions (none)
 */
extern BOOL_32 POC_get_show_for_this_poc( POC_GROUP_STRUCT *const ppoc )
{
	BOOL_32	rv;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_bool_32_bit_change_bits_group( ppoc,
												   &ppoc->show_this_poc,
												   POC_SHOW_THIS_POC_DEFAULT,
												   &nm_POC_set_show_this_poc,
												   &ppoc->changes_to_send_to_master,
												   POC_database_field_names[ POC_CHANGE_BITFIELD_poc_physically_available_bit ] );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 POC_get_bypass_number_of_levels( POC_GROUP_STRUCT *const ppoc )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( ppoc,
													 &ppoc->bypass_number_of_levels,
													 POC_BYPASS_LEVELS_MIN,
													 POC_BYPASS_LEVELS_MAX,
													 POC_BYPASS_LEVELS_DEFAULT,
													 &nm_POC_set_bypass_number_of_levels,
													 &ppoc->changes_to_send_to_master,
													 POC_database_field_names[ POC_CHANGE_BITFIELD_bypass_number_of_levels ] );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the budget entry option assigned to this POC
 * 
 * @executed Executed within the context of loading the GuiVars for the budget set up screen
 * 
 * @param ppoc A pointer to the POC.
 * 
 * @return UNS_32 The budget entry option value.
 *
 * @author 8/9/2018 Ryan
 *
 * @revisions
 *    none
 */
extern UNS_32 POC_get_budget_gallons_entry_option( POC_GROUP_STRUCT *const ppoc )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( ppoc,
													 &ppoc->budget_gallons_entry_option,
													 POC_BUDGET_GALLONS_ENTRY_OPTION_MIN,
													 POC_BUDGET_GALLONS_ENTRY_OPTION_MAX,
													 POC_BUDGET_GALLONS_ENTRY_OPTION_DEFAULT, 
													 &nm_POC_set_budget_gallons_entry_option,  
													 &ppoc->changes_to_send_to_master,
													 POC_database_field_names[ POC_CHANGE_BITFIELD_budget_gallons_entry_option_bit ] );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the budget entry option assigned to this POC
 * 
 * @executed Executed within the context of loading the GuiVars for the budget set up screen
 * 
 * @param ppoc A pointer to the POC.
 * 
 * @return UNS_32 The budget entry option value.
 *
 * @author 8/9/2018 Ryan
 *
 * @revisions
 *    none
 */
extern UNS_32 POC_get_budget_percent_ET( POC_GROUP_STRUCT *const ppoc )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( ppoc,
													 &ppoc->budget_calculation_percent_of_et,
													 POC_BUDGET_CALCULATION_PERCENT_OF_ET_MIN,
													 POC_BUDGET_CALCULATION_PERCENT_OF_ET_MAX,
													 POC_BUDGET_CALCULATION_PERCENT_OF_ET_DEFAULT, 
													 &nm_POC_set_budget_calculation_percent_of_et,  
													 &ppoc->changes_to_send_to_master,
													 POC_database_field_names[ POC_CHANGE_BITFIELD_budget_calc_percent_of_et_bit ] );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 POC_show_poc_menu_items( void )
{
	// 5/20/2016 rmd : This function sees how many POC's are physically available within the
	// site (network). And is used to suppress certain POC related menu items.

	POC_GROUP_STRUCT	*lpoc;

	BOOL_32	rv;

	rv = (false);

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc = nm_ListGetFirst( &poc_group_list_hdr );

	while( lpoc != NULL )
	{
		// 5/19/2016 rmd : AJ and I discussed this function. First we discussed about the POC_usage
		// variable and how it is supposed to work (it presently doesn't do anything). Add the 
		// Physically Available variable to that and things get fairly complicated. It seems the
		// POC_usage variable is supposed to suppress the MV and PUMP output activity. But I'm not
		// sure. Phsically available reflects just that - is it physically here.
		//
		// Physically available is set (false) if the terminal or card is missing. It is also set
		// false following the discovery results if ONE decoder of a bypass is missing. Or if the
		// decoder of a decoder based POC is missing. Physically available is also set false at the
		// conclusion of a scan for all POC's belonging to boxes that did not show up in the scan.
		if( lpoc->show_this_poc )
		{
			rv = (true);
			
			break;
		}
		
		lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 POC_at_least_one_POC_has_a_flow_meter( void )
{
	POC_GROUP_STRUCT	*lpoc;

	UNS_32	f;

	BOOL_32	rv;

	rv = (false);

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc = nm_ListGetFirst( &poc_group_list_hdr );

	while( lpoc != NULL )
	{
		// 6/6/2016 rmd : Only look at operational POCs.
		if( lpoc->show_this_poc && (lpoc->poc_usage != POC_USAGE_NOT_USED) )
		{
			for( f = 0; f < POC_BYPASS_LEVELS_MAX; ++f )
			{
				if( lpoc->flow_meter[ f ].flow_meter_choice != POC_FM_NONE )
				{
					rv = (true);

					// 6/1/2015 ajv : We found one - no need to continue looking for more.
					break;
				}
			}
		}

		lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 POC_at_least_one_POC_is_a_bypass_manifold( void )
{
	POC_GROUP_STRUCT	*lpoc;

	BOOL_32	rv;

	rv = (false);

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc = nm_ListGetFirst( &poc_group_list_hdr );

	while( lpoc != NULL )
	{
		// 6/6/2016 rmd : Only look at operational POCs.
		if( lpoc->show_this_poc && (lpoc->poc_usage != POC_USAGE_NOT_USED) )
		{
			if( lpoc->poc_type__file_type == POC__FILE_TYPE__DECODER_BYPASS )
			{
				rv = (true);

				// 6/1/2015 ajv : We found one - no need to continue looking for more.
				break;
			}
		}

		lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 POC_get_budget( POC_GROUP_STRUCT *const ppoc, UNS_32 idx_budget )
{
	// 12/22/2015 skc : Return the specified budget value
	
	UNS_32	rv;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_from_array_32_bit_change_bits_group( ppoc,
																idx_budget,
																&ppoc->budget_gallons[idx_budget],
																IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS,
																POC_BUDGET_GALLONS_MIN,
																POC_BUDGET_GALLONS_MAX,
																POC_BUDGET_GALLONS_DEFAULT,
																&nm_POC_set_budget_gallons,
																&ppoc->changes_to_send_to_master,
																POC_database_field_names[ POC_CHANGE_BITFIELD_bypass_number_of_levels ] );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 POC_get_has_pump_attached( POC_GROUP_STRUCT *const ppoc )
{
	BOOL_32	rv;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_bool_32_bit_change_bits_group( ppoc,
												   &ppoc->has_pump_attached,
												   POC_PUMP_PHYSICALLY_ATTACHED_DEFAULT,
												   &nm_POC_set_has_pump_attached,
												   &ppoc->changes_to_send_to_master,
												   POC_database_field_names[ POC_CHANGE_BITFIELD_has_pump_attached_bit ] );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	return( rv );
}


/* ---------------------------------------------------------- */
extern void POC_set_budget( POC_GROUP_STRUCT *ppoc, UNS_32 idx_budget, UNS_32 budget )
{
	// 02/01/2016 skc : Set the specified budget value
	
	UNS_32	box_index_0 = FLOWSENSE_get_controller_index();

	CHANGE_BITS_32_BIT	*lbitfield_of_changes = &ppoc->changes_to_send_to_master;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	nm_POC_set_budget_gallons( ppoc, idx_budget, budget, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
// 8/1/2016 skc : Return T/F for whether a POC should be included in budget stuff. This is
// based on whether all POC on a system have flow meters or not.
// Mutex requirements: None
// idx - index into poc_preserves
extern BOOL_32 POC_use_for_budget( UNS_32 idx )
{
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	BOOL_32 rv;
	UNS_32 i;
	POC_FM_CHOICE_AND_RATE_DETAILS pfms_ptr[ POC_BYPASS_LEVELS_MAX ];
	UNS_32 num_fm; // number of flow meters in the system
	UNS_32 levels; // number of bypass valves
	const BY_POC_RECORD * const pbpr = &poc_preserves.poc[ idx ];

	rv = false; // pessimistic default

	// invalid/non-existent POCs are never included
	if( pbpr->poc_gid != 0 )
	{
		// check for mixed use of POCs with and without flow meters
		num_fm = pbpr->this_pocs_system_preserves_ptr->sbf.number_of_flow_meters_in_this_sys;
		if( num_fm > 0 ) // we have at least one flow meter, only add budgets from POCs with a flow meter
		{
			// get number of flow meters for this POC
			levels = 1;
			if( POC_get_type_of_poc( pbpr->poc_gid ) == POC__FILE_TYPE__DECODER_BYPASS )
			{
				levels = POC_get_bypass_number_of_levels( POC_get_group_at_this_index(POC_get_index_for_group_with_this_GID( pbpr->poc_gid )) );
			}

			POC_get_fm_choice_and_rate_details( pbpr->poc_gid, pfms_ptr );
			for( i = 0; i < levels; i++ ) //loop possible flow meters for this POC
			{
				// check for flow meter existence
				if( pfms_ptr[i].flow_meter_choice != POC_FM_NONE )
				{
					rv = true;
					break; // we found one, done with loop
				}
			}
		}
		else // no flow meters, use all enabled POCs
		{
			rv = true;
		}
	}

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

	return rv;
}

/* ---------------------------------------------------------- */
extern CHANGE_BITS_32_BIT *POC_get_change_bits_ptr( POC_GROUP_STRUCT *ppoc, const UNS_32 pchange_reason )
{
	return( SHARED_get_32_bit_change_bits_ptr( pchange_reason, &ppoc->changes_to_send_to_master, &ppoc->changes_to_distribute_to_slaves, &ppoc->changes_to_upload_to_comm_server ) );
}

/* ---------------------------------------------------------- */
extern void POC_clean_house_processing( void )
{
	// 4/16/2014 rmd : Executed within the context of the comm_mngr task. ONLY BY SLAVES. THE
	// CHANGES MADE HERE DO NOT BE DISTRIBUTED. THE STATION LIST IS BEING WIPED IN PREPARATION
	// OF BEING ADDED AS A NEW MEMBER TO A CHAIN. Called when the result of a scan indicates
	// this controller is a new slave in the chain. Used when building up chains. Likely ONLY
	// used when assembling a chain for the first time or when increasing the length of a chain
	// to a length it has never 'run' at before. Or if you changed the LETTER of a slave in a
	// chain.
	
	void	*tptr;
	
	// ----------

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );
	
	// ----------
	
	Alert_Message_va( "POC, clean house list count = %d", POC_get_list_count() );
	
	// ----------
	tptr = nm_ListRemoveHead( &poc_group_list_hdr );

	while( tptr != NULL )
	{
		mem_free( tptr );

		tptr = nm_ListRemoveHead( &poc_group_list_hdr );
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	// ----------

	// 5/13/2014 ajv : Since we've removed POCs, synchronize to the POC preserves
	POC_PRESERVES_synchronize_preserves_to_file();
	
	// ----------

	// 11/18/2014 ajv : No need to create a "dummy" POC here. Simply save the file with the
	// changes and allow POCs to be created using the normal method.

	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE, POC_FILENAME, POC_LATEST_FILE_REVISION, &poc_group_list_hdr, sizeof( POC_GROUP_STRUCT ), list_poc_recursive_MUTEX, FF_POC );
}

/* ---------------------------------------------------------- */
/*
extern void POC_for_pocs_on_this_box_set_physically_available_based_upon_discovery_results( void )
{
	// 5/20/2016 rmd : Executed within the context of the comm_mngr. When the discovery results
	// are delivered from the TPMicro. So this is executed on the IRRI side. Which I think is okay.
	
	// ----------
	
	POC_GROUP_STRUCT	*lpoc;
	
	UNS_32				our_box_index_0, levels, lll, ddd;
	
	BOOL_32				all_present;

	// ----------

	our_box_index_0 = FLOWSENSE_get_controller_index();
	
	// ----------

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	lpoc = nm_ListGetFirst( &poc_group_list_hdr );

	while( lpoc != NULL )
	{
		// 7/8/2014 rmd : Only interested in POCs listed as attached to this box.
		if( lpoc->box_index_0 == our_box_index_0 )
		{
			// 7/8/2014 rmd : And must be a decoder based type of poc.
			if( (lpoc->poc_type__file_type == POC__FILE_TYPE__DECODER_SINGLE) || (lpoc->poc_type__file_type == POC__FILE_TYPE__DECODER_BYPASS) )
			{
				// 7/8/2014 rmd : Set all three levels of the temporary flag to (false) meaning couldn't
				// find a decoder for this level.
				lpoc->individually_available[ 0 ] = (false);
				lpoc->individually_available[ 1 ] = (false);
				lpoc->individually_available[ 2 ] = (false);
				
				// ----------
				
				// 7/8/2014 rmd : Find out how many levels.
				levels = 1;
				
				if( lpoc->poc_type__file_type == POC__FILE_TYPE__DECODER_BYPASS )
				{
					// 7/8/2014 rmd : Use the formal GET function so value is range checked.
					levels = POC_get_bypass_number_of_levels( lpoc );
				}
				
				// ----------
				
				for( lll=0; lll<levels; lll++ )
				{
					for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
					{
						// 7/8/2014 rmd : If the serial number is 0 we've reached the end of populated entries in
						// the list.
						if( !tpmicro_data.decoder_info[ ddd ].sn )
						{
							break;					
						}
		
						if( tpmicro_data.decoder_info[ ddd ].sn == lpoc->decoder_serial_number[ lll ] )
						{
							lpoc->individually_available[ lll ] = (true);

							break;
						}
					}
					
				}  // for each level

			}  // of if decoder based poc
			
		}  // of if poc belongs to our box index
		
		lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );

	}  // of while for each poc

	// ----------

	// 7/8/2014 rmd : Now pass back through the list and assess if the pocs have the needed
	// decoders present. If so set physically available (true). If not set physically available
	// (false).

	lpoc = nm_ListGetFirst( &poc_group_list_hdr );

	while( lpoc != NULL )
	{
		// 7/8/2014 rmd : Only interested in POCs listed as attached to this box.
		if( lpoc->box_index_0 == our_box_index_0 )
		{
			// 7/8/2014 rmd : And must be a decoder based type of poc.
			if( (lpoc->poc_type__file_type == POC__FILE_TYPE__DECODER_SINGLE) || (lpoc->poc_type__file_type == POC__FILE_TYPE__DECODER_BYPASS) )
			{
				// 7/8/2014 rmd : Find out how many levels.
				levels = 1;
				
				if( lpoc->poc_type__file_type == POC__FILE_TYPE__DECODER_BYPASS )
				{
					// 7/8/2014 rmd : Use the formal GET function so value is range checked.
					levels = POC_get_bypass_number_of_levels( lpoc );
				}
				
				// ----------
				
				all_present = (true);
				
				for( lll=0; lll<levels; lll++ )
				{
					if( !lpoc->individually_available[ lll ] )
					{
						all_present = (false);	
					}
				}  // for each level
				
				// ----------
				
				if( all_present )
				{
					// 7/8/2014 rmd : Set physically available to (true). If already (true) no effect or change
					// distribution takes place.
					nm_POC_set_physically_available( lpoc, (true), CHANGE_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, our_box_index_0, (true), POC_get_change_bits_ptr( lpoc, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED ) );
				}
				else
				{
					// 7/8/2014 rmd : Set physically available to (false). If already (false) no effect or
					// change distribution takes place.
					nm_POC_set_physically_available( lpoc, (false), CHANGE_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_REMOVED, our_box_index_0, (true), POC_get_change_bits_ptr( lpoc, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_REMOVED ) );
				}


			}  // of if decoder based poc
			
		}  // of if poc belongs to our box index
		
		lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );

	}  // of while for each poc

	// ----------

	// 7/8/2014 rmd : And to avoid any problems ensure preserves is indeed insync before moving
	// along.
	POC_PRESERVES_synchronize_preserves_to_file();
	
	// ----------

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
}
*/

/* ---------------------------------------------------------- */
extern void POC_set_bits_on_all_pocs_to_cause_distribution_in_the_next_token( void )
{
	POC_GROUP_STRUCT	*lpoc;

	// ----------

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	Alert_Message_va(" POC count to distribute = %d", POC_get_list_count() );
	
	lpoc = nm_ListGetFirst( &poc_group_list_hdr );

	while( lpoc != NULL )
	{
		// 5/1/2014 rmd : This function ONLY being called at the conclusion of a SCAN by the master
		// of a real multiple controller chain - we want to broadcast to the slaves using tokens
		// about all the hardware we presently know about. In this case the stations.
		SHARED_set_all_32_bit_change_bits( &lpoc->changes_to_distribute_to_slaves, list_poc_recursive_MUTEX );

		lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	// ----------

	// 4/29/2014 ajv : Set a flag in the changes structure to notify this
	// controller to examine its lists for changes to distribute to the other
	// controllers in the chain in it's next token resposne. The flag is part of
	// the mutex protected comm_mngr struct so take the MUTEX.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	comm_mngr.changes.distribute_changes_to_slave = (true);

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void POC_on_all_pocs_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear )
{
	POC_GROUP_STRUCT	*lpoc;

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc = nm_ListGetFirst( &poc_group_list_hdr );

	while( lpoc != NULL )
	{
		if( pset_or_clear == COMMSERVER_CHANGE_BITS_ALL_CLEARED )
		{
			SHARED_clear_all_32_bit_change_bits( &lpoc->changes_to_upload_to_comm_server, list_poc_recursive_MUTEX );
		}
		else
		{
			SHARED_set_all_32_bit_change_bits( &lpoc->changes_to_upload_to_comm_server, list_poc_recursive_MUTEX );
		}

		lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * @mutex_requirements Requires the caller to have the pending_changes_recursive_MUTEX to
 * ensure no other function tries to modify the pending_changes_to_send_to_comm_server flag.
 *
 * @task_execution Executed within the context of the CONTROLLER INITIATED task when it
 * first starts and when the waiting response flags are cleared after a communication.
 */
extern void nm_POC_update_pending_change_bits( const UNS_32 pcomm_error_occurred )
{
	POC_GROUP_STRUCT	*lpoc;

	BOOL_32	lfile_save_necessary;

	lfile_save_necessary = (false);

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lpoc = nm_ListGetFirst( &poc_group_list_hdr );

	while( lpoc != NULL )
	{
		// 4/30/2015 ajv : No reason to process this if none of the bits are set. This also helps
		// reduce the file save activity.
		if( lpoc->changes_uploaded_to_comm_server_awaiting_ACK != CHANGE_BITS_32_BIT_ALL_CLEAR )
		{
			// 4/29/2015 ajv : If a communication error occurred while sending Program Data, copy the
			// bits of variables that were sent back into the original bit field to ensure they're sent
			// again during the next attempt. Otherwise, clear the pending bits.
			if( pcomm_error_occurred )
			{
				lpoc->changes_to_upload_to_comm_server |= lpoc->changes_uploaded_to_comm_server_awaiting_ACK;

				// 4/30/2015 ajv : Since we've determined there were changes that were not successfully
				// sent, set the flag to trigger another attempt
				weather_preserves.pending_changes_to_send_to_comm_server = (true);

				// 4/30/2015 ajv : Also set the timer to force the data to send. If the timer's already
				// running, this will simply restart it.
				// 
				// 4/30/2015 ajv : BECAUSE of the potential that this function is executed many times (once
				// for each item changed) we could not start our pdata timer with a post to the CI queue.
				// The queue would overflow. So start the timer directly.
				xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGES_NOT_SENT_MS ), portMAX_DELAY );
			}
			else
			{
				SHARED_clear_all_32_bit_change_bits( &lpoc->changes_uploaded_to_comm_server_awaiting_ACK, list_program_data_recursive_MUTEX );
			}

			// 4/30/2015 ajv : At least one system was updated. So make sure we save the file.
			lfile_save_necessary = (true);
		}

		lpoc = nm_ListGetNext( &poc_group_list_hdr, lpoc );
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	if( lfile_save_necessary )
	{
		// 4/29/2015 ajv : Now that the bits are set or cleared accordingly, trigger a file save.
		// If power fails between building the message and receiving the ACK, the original bit field
		// hasn't been saved to flash yet, so it will resend the associated variables during the
		// next attempt.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_POC, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
	}
}

/* ---------------------------------------------------------- */
extern void POC_load_fields_syncd_to_the_poc_preserves( const POC_GROUP_STRUCT *const ppoc, POC_PRESERVES_SYNCD_ITEMS_STRUCT *psync )
{
	// 6/11/2014 rmd : Called during the sync process. Generally within the context of the
	// TDCHECK task. However at the two places where POCs are added to the file [for the
	// terminal and decoder poc during receipt of message from the tpmicro] the poc_preserves
	// sync may be called directly instead of during the routine 1 hertz call. If so that is
	// done within the context of the COMM_MNGR task.

	// ----------

	UNS_32	level;

	// ----------

	memset( psync, 0x00, sizeof(POC_PRESERVES_SYNCD_ITEMS_STRUCT) );
	
	// ----------
	
	// 6/12/2014 rmd : A simple check against NULL pointer.
	if( ppoc != NULL )
	{
		xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );
	
		if( ppoc != NULL )
		{
			psync->poc_gid = ppoc->base.group_identity_number;
	
			psync->box_index = ppoc->box_index_0;
	
			psync->poc_type__file_type = ppoc->poc_type__file_type;
	
			psync->show_this_poc = ppoc->show_this_poc;
	
			psync->usage = ppoc->poc_usage;
	
			for( level=0; level<POC_BYPASS_LEVELS_MAX; level++ )
			{
				psync->decoder_serial_number[ level ] = ppoc->decoder_serial_number[ level ];
	
				psync->master_valve_type[ level ] = ppoc->master_valve_type[ level ];
			}
			
			psync->this_pocs_system_preserves_ptr = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( ppoc->GID_irrigation_system );
		}
		else
		{
			Alert_func_call_with_null_ptr();
		}
	
		xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
	}

}

/* ---------------------------------------------------------- */
BOOL_32 POC_calculate_chain_sync_crc( UNS_32 const pff_name )
{
	// 8/30/2018 rmd : Executed in the context of the comm_mngr task when token responses are
	// being made. And that's it.

	// ----------

	POC_GROUP_STRUCT	*lpoc_ptr;
	
	UNS_8				*checksum_start;

	UNS_32				checksum_length;

	UNS_8				*ucp;
	
	BOOL_32				rv;

	// ----------

	rv = (false);
 
	// ----------
	
	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	// 8/30/2018 rmd : So the CRC calculation function wants a continuous memory space and a
	// length to calculate the crc off. In this function we will pick out the desired data, and
	// simply pile it up on itself, list item by list item. We don't care how messy the pile is
	// because we never attempt to reference any data once it is added to the pile. The data is
	// only there to include in the checksum.
	//
	// 8/30/2018 rmd : Well we know it can't be bigger than the list item count times the size
	// of each list item, so start with a memory block that big.
	if( mem_obtain_a_block_if_available( (poc_group_list_hdr.count * sizeof(POC_GROUP_STRUCT)), (void**)&checksum_start ) )
	{
		// 9/5/2018 rmd : Well then the calculation will be successful. Nothing else can prevent
		// that outcome.
		rv = (true);
		
		// ----------
		
		ucp = checksum_start;
		
		checksum_length = 0;
		
		// ----------
		
		// 8/30/2018 rmd : Rattle though the list and push the fields we want to include in the sync
		// test onto the block to be checksummed.
		lpoc_ptr = nm_ListGetFirst( &poc_group_list_hdr );
		
		while( lpoc_ptr != NULL )
		{
			// 9/4/2018 rmd : Make sure you include only TRUE USER SETTINGS, no flags, or pointers to
			// memory, etc...
			
			// ----------
			
			// 11/29/2018 dmd: Check if this is a stale poc entry on the list. We do not want to
			// calculate crc for those duplicate entries on the master, since the slaves will create
			// only the new POCs entries in the list after a clean up, due to CRC error.
			// This avoids mis-match in the number of POC entries used for CRC calculations over a
			// chain.
			if( lpoc_ptr->show_this_poc )
			{
				// 11/2/2018 rmd : Due to the inconsistent way the group names strings are handled during
				// the sync (we do not regligiously take all 48 characters and always carry them through the
				// sync process, there are places we only copy up until the null), the part of the string
				// following the null char is not guaranteed to be in sync. So only include up until the
				// null in the crc test.
				checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lpoc_ptr->base.description, strlen(lpoc_ptr->base.description) );
	
				checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lpoc_ptr->GID_irrigation_system, sizeof(lpoc_ptr->GID_irrigation_system) );
				
				checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lpoc_ptr->box_index_0, sizeof(lpoc_ptr->box_index_0) );
	
				checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lpoc_ptr->poc_type__file_type, sizeof(lpoc_ptr->poc_type__file_type) );
				
				checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lpoc_ptr->decoder_serial_number, sizeof(lpoc_ptr->decoder_serial_number) );
				
				checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lpoc_ptr->show_this_poc, sizeof(lpoc_ptr->show_this_poc) );
				
				// skipping the individually available variable, it is not a user setting, and is not syncd
				 
				checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lpoc_ptr->poc_usage, sizeof(lpoc_ptr->poc_usage) );
				
				checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lpoc_ptr->master_valve_type, sizeof(lpoc_ptr->master_valve_type) );
				
				// skipping the unused_a variable
				
				checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lpoc_ptr->bypass_number_of_levels, sizeof(lpoc_ptr->bypass_number_of_levels) );
				
				// skipping the unused_b variable
	
				checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lpoc_ptr->flow_meter, sizeof(lpoc_ptr->flow_meter) );
				
				// skipping the changes bit fields
				
				checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lpoc_ptr->budget_gallons_entry_option, sizeof(lpoc_ptr->budget_gallons_entry_option) );
				
				checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lpoc_ptr->budget_calculation_percent_of_et, sizeof(lpoc_ptr->budget_calculation_percent_of_et) );
				
				checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lpoc_ptr->has_pump_attached, sizeof(lpoc_ptr->has_pump_attached) );
				
				// skipping the unused_d variable
				
				checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lpoc_ptr->budget_gallons, sizeof(lpoc_ptr->budget_gallons) );
				
			}
			
			// END
			
			// ----------
		
			lpoc_ptr = nm_ListGetNext( &poc_group_list_hdr, lpoc_ptr );
		}
		
		// ----------
		
		// 8/30/2018 rmd : Now calculate the checksum.
		cscs.the_crc[ pff_name ] = CRC_calculate_32bit_big_endian( checksum_start, checksum_length );
		
		// ----------
		
		// 9/4/2018 rmd : We're done with the block, so of course free it.
		mem_free( checksum_start );
	}
	else
	{
		// 9/5/2018 rmd : So by design it's okay if there is no memory. The checksum won't be set
		// valid, and the next time we try to include it in a token response we'll try to calculate
		// it again. Self protecting and retrying. But let's alert about this so we know it's taking
		// place.
		Alert_Message_va( "SYNC: no mem to calc checksum %s", chain_sync_file_pertinants[ pff_name ].file_name_string );
	}
	
	// ----------
	
	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

