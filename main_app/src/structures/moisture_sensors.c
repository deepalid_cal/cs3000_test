/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	<stdlib.h>

#include	"moisture_sensors.h"

#include	"moisture_sensor_recorder.h"

#include	"alerts.h"

#include	"bithacks.h"

#include	"cal_math.h"

#include	"change.h"

#include	"comm_mngr.h"

#include	"cs_mem.h"

#include	"dialog.h"

#include	"group_base_file.h"

#include	"flash_storage.h"

#include	"flowsense.h"

#include	"foal_comm.h"

#include	"irrigation_system.h"

#include	"pdata_changes.h"

#include	"range_check.h"

#include	"shared.h"

#include	"tpmicro_comm.h"

#include	"app_startup.h"

#include	"r_alerts.h"

#include	"tpmicro_data.h"

#include	"controller_initiated.h"

#include	"chain_sync_vars.h"

#include	"chain_sync_funcs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static MIST_LIST_HDR_TYPE	moisture_sensor_group_list_hdr;

// ----------

// 2/18/2016 rmd : Must include after the list header declaration.
#include	"shared_moisture_sensors.c"

// ----------

static const char MOISTURE_SENSOR_FILENAME[] =	"MOIS_SENSORS";

const char MOISTURE_SENSOR_DEFAULT_GROUP_NAME[] = "MOISTURE SENSOR";

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void nm_MOISTURE_SENSOR_set_default_values( void *const pmois, const BOOL_32 pset_change_bits );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	// ----------

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;
	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

	// ----------
	
	// 2/2/2016 rmd : The identifying parameters that identify a moisture sensor decoder. These
	// two are not directly user editable but indeed are sync'd (with the server and the other
	// boxes on the chain if any).
	UNS_32			box_index_0;

	UNS_32			decoder_serial_number;

	// ----------
	
	// 2/2/2016 rmd : This is set (true) when the user has, post discovery, indicated that he
	// wishes to make available for use this moisture decoder. The moisture sensor list item is
	// created and marked physically available and in_use. If a subsequent scan does NOT find
	// this decoder then we find him in the moisture sensor file list and mark this physically
	// available (false). A subsequent scan can mark this back (true) if the decoder is once
	// again found. This variable is sync'd (with the server and the other boxes on the chain if
	// any).
	BOOL_32			physically_available;

	// 2/2/2016 rmd : The user sets this to indicate he wishes to make this sensor available for
	// assignment to control a station group. This variable is sync'd (with the server and the
	// other boxes on the chain if any). NOTE - this variable is not actually used. Following
	// the discovery all DISCOVERED moisture sensors are 'added to/ensured to be in' the file
	// list and are physically available and in_use.
	BOOL_32			in_use;

	// ----------
	
	// 2/4/2016 rmd : Three modes allowed. HIGH THRESHOLD mode. LOW THRESHOLD mode. And both
	// meaning LOW to start and HIGH to stop irrigation. This variable is sync'd (with the
	// server and the other boxes on the chain if any).
	UNS_32			moisture_control_mode;
	

	// 2/29/2016 rmd : This value reflects a percentage called volumetric water content (VWC).
	// With our sensor completely submerged the percentage we calculate apparently comes in at
	// approx 75%. So I would imagine the user set point with the sensor in the ground is going
	// to be something like 35%. We shall see. This variable is sync'd (with the server and the
	// other boxes on the chain if any).
	UNS_32			moisture_high_set_point;

	// 2/2/2016 rmd : Unknown scaling at this time. This variable is sync'd (with the server and
	// the other boxes on the chain if any).
	UNS_32			moisture_low_set_point;

	// ----------
	
	// 2/2/2016 rmd : Unknown scaling at this time. This variable is sync'd (with the server and
	// the other boxes on the chain if any).
	UNS_32			low_temp_point;
	
	// ----------
	
	// 2/2/2016 rmd : We allow multiple station groups to use the same sensor. This is because
	// station groups are formed based upon other criteria than simply which sensor controls the
	// station in it. The consequence however is 1) we assume all station groups that use the
	// sensor have the same start time and 2) that at least one of the stations in the group or
	// groups irrigates the sensor. But we don't know which one. So to be sure to allow water
	// time to soak in after the last station is irrigated (keeping cycles balanced) before we
	// start the next cycle, we force an additional 20 minutes soak-in time. The present plan is
	// this variable is not exposed to the user. It will however be sync'd (with the server and
	// the other boxes on the chain if any).
	UNS_32			additional_soak_seconds;
	
	// ----------
	
	// 2/18/2016 rmd : After power up at the 75 second mark the TPMicro obtains the latest
	// moisture/temp/EC readings from the decoder and then goes about it's business of handing
	// it up to the CS3000. As received from the TPMicro the reading is stored in our raw
	// reading variable below. Once received at the MASTER we pick the reading apart into its
	// components and perform any conversion math needed. And we set the time stamp so we know
	// the age of the reading.
	
	// 2/23/2016 rmd : These are the variables involved as the reading is received from the
	// TPMicro. The reading is then to be included in the next token response. Are to be
	// considered SLAVE side variables. NOT SYNC'D TO SERVER.
	MOISTURE_SENSOR_DECODER_RESPONSE	reading_string_storage__slave;
	
	// 2/23/2016 rmd : A SLAVE side variable. NOT SYNC'D TO SERVER.
	BOOL_32								reading_string_send_to_master__slave;
	
	// ----------
	
	// 2/23/2016 rmd : These are the latest readings as received from the slave. The range on
	// the readings typically produced is from 0.0 to about 80.0. And is truly a decimal number
	// meaning will show 41.2% and such. Is to be consider a MASTER side variable. NOT SYNC'D TO
	// SERVER.
	float		latest_moisture_vwc_percentage;

	// 3/21/2016 rmd : Note is a SIGNED INT as temperature can go negative. Is to be consider a
	// MASTER side variable. NOT SYNC'D TO SERVER.
	INT_32		latest_temperature_fahrenheit;

	// 3/29/2016 rmd : Has a range of 0 through 72,000. Is to be consider a MASTER side
	// variable. NOT SYNC'D TO SERVER.
	UNS_32		latest_conductivity_micro_siemen_per_cm;
	
	// 2/18/2016 rmd : This is set (false) during code start. And is a flag in case the
	// application wants to use the latest reading before it has arrived. REMEMBER - as the
	// latest readings arrive from the sensor and written to the list item the file IS NOT
	// SAVED. Therefore upon application startup when the file is read out the latest readings
	// are not valid until a new reading arrives from the TPMicro. Which should happen within 2
	// minutes from startup (given both the TPMicro and Main Board applications restarted). This
	// is a MASTER side variable. NOT SYNC'D TO SERVER.
	BOOL_32		latest_reading_valid;
	
	// 2/23/2016 rmd : This flag works in addition to the latest_reading_valid. The latest
	// reading valid flag is really only used till the first reading arrives. When that is true
	// we also demand the reading be not more than 20 minutes old. The logic being if something
	// is wrong with the sensor let the TPMicro try twice [TPMicro reads the sensor decoder once
	// every 10 minutes]. This is a MASTER side variable. NOT SYNC'D TO SERVER.
	UNS_32		latest_reading_time_stamp;
	
	// 3/29/2016 rmd : This is just a character value that we assign to this 32-bit variable.
	// Takes on the value 'x' or 'z'. NOT SYNC'D TO SERVER. Not saved to the file when updated.
	UNS_32		detected_sensor_model;
	
	// ----------
	
	UNS_32		unused_a;
	
	BOOL_32		unused_b;
	
} MOISTURE_SENSOR_GROUP_STRUCT_0_and_1;

// ----------

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	// ----------

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;
	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

	// ----------
	
	// 2/2/2016 rmd : The identifying parameters that identify a moisture sensor decoder. These
	// two are not directly user editable but indeed are sync'd (with the server and the other
	// boxes on the chain if any).
	UNS_32			box_index_0;

	UNS_32			decoder_serial_number;

	// ----------
	
	// 2/2/2016 rmd : This is set (true) when the user has, post discovery, indicated that he
	// wishes to make available for use this moisture decoder. The moisture sensor list item is
	// created and marked physically available and in_use. If a subsequent scan does NOT find
	// this decoder then we find him in the moisture sensor file list and mark this physically
	// available (false). A subsequent scan can mark this back (true) if the decoder is once
	// again found. This variable is sync'd (with the server and the other boxes on the chain if
	// any).
	BOOL_32			physically_available;

	// 2/2/2016 rmd : The user sets this to indicate he wishes to make this sensor available for
	// assignment to control a station group. This variable is sync'd (with the server and the
	// other boxes on the chain if any). NOTE - this variable is not actually used. Following
	// the discovery all DISCOVERED moisture sensors are 'added to/ensured to be in' the file
	// list and are physically available and in_use.
	BOOL_32			in_use;

	// ----------
	
	// 2/4/2016 rmd : Three modes allowed. HIGH THRESHOLD mode. LOW THRESHOLD mode. And both
	// meaning LOW to start and HIGH to stop irrigation. This variable is sync'd (with the
	// server and the other boxes on the chain if any).
	UNS_32			moisture_control_mode;
	

	// 2/29/2016 rmd : This value reflects a percentage called volumetric water content (VWC).
	// With our sensor completely submerged the percentage we calculate apparently comes in at
	// approx 75%. So I would imagine the user set point with the sensor in the ground is going
	// to be something like 35%. We shall see. This variable is sync'd (with the server and the
	// other boxes on the chain if any).
	UNS_32			moisture_high_set_point;

	// 2/2/2016 rmd : Unknown scaling at this time. This variable is sync'd (with the server and
	// the other boxes on the chain if any).
	UNS_32			moisture_low_set_point;

	// ----------
	
	// 2/2/2016 rmd : Unknown scaling at this time. This variable is sync'd (with the server and
	// the other boxes on the chain if any).
	UNS_32			low_temp_point;
	
	// ----------
	
	// 2/2/2016 rmd : We allow multiple station groups to use the same sensor. This is because
	// station groups are formed based upon other criteria than simply which sensor controls the
	// station in it. The consequence however is 1) we assume all station groups that use the
	// sensor have the same start time and 2) that at least one of the stations in the group or
	// groups irrigates the sensor. But we don't know which one. So to be sure to allow water
	// time to soak in after the last station is irrigated (keeping cycles balanced) before we
	// start the next cycle, we force an additional 20 minutes soak-in time. The present plan is
	// this variable is not exposed to the user. It will however be sync'd (with the server and
	// the other boxes on the chain if any).
	UNS_32			additional_soak_seconds;
	
	// ----------
	
	// 2/18/2016 rmd : After power up at the 75 second mark the TPMicro obtains the latest
	// moisture/temp/EC readings from the decoder and then goes about it's business of handing
	// it up to the CS3000. As received from the TPMicro the reading is stored in our raw
	// reading variable below. Once received at the MASTER we pick the reading apart into its
	// components and perform any conversion math needed. And we set the time stamp so we know
	// the age of the reading.
	
	// 2/23/2016 rmd : These are the variables involved as the reading is received from the
	// TPMicro. The reading is then to be included in the next token response. Are to be
	// considered SLAVE side variables. NOT SYNC'D TO SERVER.
	MOISTURE_SENSOR_DECODER_RESPONSE	reading_string_storage__slave;
	
	// 2/23/2016 rmd : A SLAVE side variable. NOT SYNC'D TO SERVER.
	BOOL_32								reading_string_send_to_master__slave;
	
	// ----------
	
	// 2/23/2016 rmd : These are the latest readings as received from the slave. The range on
	// the readings typically produced is from 0.0 to about 80.0. And is truly a decimal number
	// meaning will show 41.2% and such. Is to be consider a MASTER side variable. NOT SYNC'D TO
	// SERVER.
	float		latest_moisture_vwc_percentage;

	// 3/21/2016 rmd : Note is a SIGNED INT as temperature can go negative. Is to be consider a
	// MASTER side variable. NOT SYNC'D TO SERVER.
	INT_32		latest_temperature_fahrenheit;

	// 3/29/2016 rmd : Has a range of 0 through 72,000. Is to be consider a MASTER side
	// variable. NOT SYNC'D TO SERVER.
	UNS_32		latest_conductivity_micro_siemen_per_cm;
	
	// 2/18/2016 rmd : This is set (false) during code start. And is a flag in case the
	// application wants to use the latest reading before it has arrived. REMEMBER - as the
	// latest readings arrive from the sensor and written to the list item the file IS NOT
	// SAVED. Therefore upon application startup when the file is read out the latest readings
	// are not valid until a new reading arrives from the TPMicro. Which should happen within 2
	// minutes from startup (given both the TPMicro and Main Board applications restarted). This
	// is a MASTER side variable. NOT SYNC'D TO SERVER.
	BOOL_32		latest_reading_valid;
	
	// 2/23/2016 rmd : This flag works in addition to the latest_reading_valid. The latest
	// reading valid flag is really only used till the first reading arrives. When that is true
	// we also demand the reading be not more than 20 minutes old. The logic being if something
	// is wrong with the sensor let the TPMicro try twice [TPMicro reads the sensor decoder once
	// every 10 minutes]. This is a MASTER side variable. NOT SYNC'D TO SERVER.
	UNS_32		latest_reading_time_stamp;
	
	// 3/29/2016 rmd : This is just a character value that we assign to this 32-bit variable.
	// Takes on the value 'x' or 'z'. NOT SYNC'D TO SERVER. Not saved to the file when updated.
	UNS_32		detected_sensor_model;
	
	// ----------
	
	// 5/23/2018 rmd : The was an unused in rev 1. Renamed in rev 2 to use as a protection
	// variable so irrigation doesn't forever stall during some unforseen malfunction with the
	// 2W network or the moisture decoder itself. Should be zeroed upon code startup, and at a
	// start time, and allow say 5 minutes to get a valid and recent reading, before allowing
	// another cycle to irrigate. After allowing a new cycle do not need to again zero this, as
	// all station need to reach the required cycle count before the next use of this variable.
	UNS_32		invalid_or_old_reading_seconds;

	BOOL_32		unused_b;
	
	// ----------
	
	// 4/8/2016 rmd : Added in Rev 2

	// 5/10/2018 rmd : A variable to support the moisture sensing logic during irrigation. Set
	// to 0 at the start time. Each second as part of the maintain function we check if a
	// moisture sensor reading has crossed the set point. If it has, this is set (true). NOT
	// SYNC'D DOWN THE CHAIN OR TO COMMSERVER. BUT SHOULD BE SAVED WHEN SET TO TRUE OR FALSE
	// BECAUSE IT CONTROLS IRRIGATION AND SHOULD BE PRESERVED THROUGH A POWER FAILURE.
	BOOL_32			moisture_sensor_control__saved__has_crossed_the_set_point;
	

	// 5/10/2018 rmd : A variable to support the moisture sensing logic during irrigation. Set
	// to 0 at the start time. And, when a station turns ON, if this variable is less than the
	// station history rip cycle count, is incremented. Used as part of the list removal
	// criteria if the sensor reading has crossed the set point is TRUE. NOT SYNC'D DOWN THE
	// CHAIN OR TO COMMSERVER. BUT SHOULD BE SAVED WHEN SET TO TRUE OR FALSE BECAUSE IT CONTROLS
	// IRRIGATION AND SHOULD BE PRESERVED THROUGH A POWER FAILURE.
	UNS_32			moisture_sensor_control__saved__required_cycles_before_removal;


	// 5/17/2018 rmd : This one initialized to (false) each second. Indicates this sensor is not
	// being used to control irrigation right this moment. This variable is dynamic, and not
	// useful outside of the foal irri maintain function. As it changes no file save is
	// necessary.
	BOOL_32 		moisture_sensor_control__transient__has_actively_irrigating_stations;

	// 5/10/2018 rmd : This variable is dynamic, and not useful outside of the foal irri
	// maintain function. As it changes no file save is necessary.
	BOOL_32 		moisture_sensor_control__transient__all_stations_are_off_and_soaking_completed;

	// 5/10/2018 rmd : This variable is dynamic, and not useful outside of the foal irri
	// maintain function. As it changes no file save is necessary.
	BOOL_32 		moisture_sensor_control__transient__all_stations_have_reached_the_required_cycles;
	
} MOISTURE_SENSOR_GROUP_STRUCT_2;

// ----------

// ----------

typedef struct
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	// ----------

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;
	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

	// ----------
	
	// 2/2/2016 rmd : The identifying parameters that identify a moisture sensor decoder. These
	// two are not directly user editable but indeed are sync'd (with the server and the other
	// boxes on the chain if any).
	UNS_32			box_index_0;

	UNS_32			decoder_serial_number;

	// ----------
	
	// 2/2/2016 rmd : This is set (true) when the user has, post discovery, indicated that he
	// wishes to make available for use this moisture decoder. The moisture sensor list item is
	// created and marked physically available and in_use. If a subsequent scan does NOT find
	// this decoder then we find him in the moisture sensor file list and mark this physically
	// available (false). A subsequent scan can mark this back (true) if the decoder is once
	// again found. This variable is sync'd (with the server and the other boxes on the chain if
	// any).
	BOOL_32			physically_available;

	// 2/2/2016 rmd : The user sets this to indicate he wishes to make this sensor available for
	// assignment to control a station group. This variable is sync'd (with the server and the
	// other boxes on the chain if any). NOTE - this variable is not actually used. Following
	// the discovery all DISCOVERED moisture sensors are 'added to/ensured to be in' the file
	// list and are physically available and in_use.
	BOOL_32			in_use;

	// ----------
	
	// 2/4/2016 rmd : Three modes allowed. HIGH THRESHOLD mode. LOW THRESHOLD mode. And both
	// meaning LOW to start and HIGH to stop irrigation. This variable is sync'd (with the
	// server and the other boxes on the chain if any).
	UNS_32			moisture_control_mode;
	

	// 2/29/2016 rmd : This value reflects a percentage called volumetric water content (VWC).
	// With our sensor completely submerged the percentage we calculate apparently comes in at
	// approx 75%. So I would imagine the user set point with the sensor in the ground is going
	// to be something like 35%. We shall see. This variable is sync'd (with the server and the
	// other boxes on the chain if any).
	//
	// 10/5/2018 rmd : In file Revision 3 these were rendered obsolete. Replaced with floating
	// point versions you'll see later in this structure.
	UNS_32			nlu_moisture_high_set_point_uns32;

	UNS_32			nlu_moisture_low_set_point_uns32;

	// ----------
	
	// 2/2/2016 rmd : This variable is sync'd (with the server and the other boxes on the chain
	// if any).
	UNS_32			low_temp_point;
	
	// ----------
	
	// 2/2/2016 rmd : We allow multiple station groups to use the same sensor. This is because
	// station groups are formed based upon other criteria than simply which sensor controls the
	// station in it. The consequence however is 1) we assume all station groups that use the
	// sensor have the same start time and 2) that at least one of the stations in the group or
	// groups irrigates the sensor. But we don't know which one. So to be sure to allow water
	// time to soak in after the last station is irrigated (keeping cycles balanced) before we
	// start the next cycle, we force an additional 20 minutes soak-in time. The present plan is
	// this variable is not exposed to the user. It will however be sync'd (with the server and
	// the other boxes on the chain if any).
	UNS_32			additional_soak_seconds;
	
	// ----------
	
	// 2/18/2016 rmd : After power up at the 75 second mark the TPMicro obtains the latest
	// moisture/temp/EC readings from the decoder and then goes about it's business of handing
	// it up to the CS3000. As received from the TPMicro the reading is stored in our raw
	// reading variable below. Once received at the MASTER we pick the reading apart into its
	// components and perform any conversion math needed. And we set the time stamp so we know
	// the age of the reading.
	
	// 2/23/2016 rmd : These are the variables involved as the reading is received from the
	// TPMicro. The reading is then to be included in the next token response. Are to be
	// considered SLAVE side variables. NOT SYNC'D TO SERVER.
	MOISTURE_SENSOR_DECODER_RESPONSE	reading_string_storage__slave;
	
	// 2/23/2016 rmd : A SLAVE side variable. NOT SYNC'D TO SERVER.
	BOOL_32								reading_string_send_to_master__slave;
	
	// ----------
	
	// 2/23/2016 rmd : These are the latest readings as received from the slave. The range on
	// the readings typically produced is from 0.0 to about 80.0. And is truly a decimal number
	// meaning will show 41.2% and such. Is to be consider a MASTER side variable.
	//
	// 10/5/2018 rmd : In file Rev 3 we changed the scaling on this variable to be between 0 and
	// 1.0 (roughly speaking - as AJ and Ryan report values can be negative).
	// NOT SYNC'D TO SERVER.
	float		latest_moisture_vwc_percentage;

	// 3/21/2016 rmd : Note is a SIGNED INT as temperature can go negative. Is to be consider a
	// MASTER side variable. NOT SYNC'D TO SERVER.
	INT_32		latest_temperature_fahrenheit;

	// 3/29/2016 rmd : Has a range of 0 through 72,000. Is to be consider a MASTER side
	// variable. NOT SYNC'D TO SERVER.
	//
	// 8/17/2018 Ryan : Renamed from micro_siemen_per_cm to
	// conductivity_reading_deci_siemen_per_m to reflect the new units being used. Also changed
	// from UNS_32 to float.
	float		latest_conductivity_reading_deci_siemen_per_m;
	
	// 2/18/2016 rmd : This is set (false) during code start. And is a flag in case the
	// application wants to use the latest reading before it has arrived. REMEMBER - as the
	// latest readings arrive from the sensor and written to the list item the file IS NOT
	// SAVED. Therefore upon application startup when the file is read out the latest readings
	// are not valid until a new reading arrives from the TPMicro. Which should happen within 2
	// minutes from startup (given both the TPMicro and Main Board applications restarted). This
	// is a MASTER side variable. NOT SYNC'D TO SERVER.
	BOOL_32		latest_reading_valid;
	
	// 2/23/2016 rmd : This flag works in addition to the latest_reading_valid. The latest
	// reading valid flag is really only used till the first reading arrives. When that is true
	// we also demand the reading be not more than 20 minutes old. The logic being if something
	// is wrong with the sensor let the TPMicro try twice [TPMicro reads the sensor decoder once
	// every 10 minutes]. This is a MASTER side variable. NOT SYNC'D TO SERVER.
	UNS_32		latest_reading_time_stamp;
	
	// 3/29/2016 rmd : This is just a character value that we assign to this 32-bit variable.
	// Takes on the value 'x' or 'z'. NOT SYNC'D TO SERVER. Not saved to the file when updated.
	UNS_32		detected_sensor_model;
	
	// ----------
	
	// 5/23/2018 rmd : The was an unused in rev 1. Renamed in rev 2 to use as a protection
	// variable so irrigation doesn't forever stall during some unforseen malfunction with the
	// 2W network or the moisture decoder itself. Should be zeroed upon code startup, and at a
	// start time, and allow say 5 minutes to get a valid and recent reading, before allowing
	// another cycle to irrigate. After allowing a new cycle do not need to again zero this, as
	// all station need to reach the required cycle count before the next use of this variable.
	UNS_32		invalid_or_old_reading_seconds;

	BOOL_32		unused_b;
	
	// ----------
	
	// 4/8/2016 rmd : Added in Rev 2

	// 5/10/2018 rmd : A variable to support the moisture sensing logic during irrigation. Set
	// to 0 at the start time. Each second as part of the maintain function we check if a
	// moisture sensor reading has crossed the set point. If it has, this is set (true). NOT
	// SYNC'D DOWN THE CHAIN OR TO COMMSERVER. BUT SHOULD BE SAVED WHEN SET TO TRUE OR FALSE
	// BECAUSE IT CONTROLS IRRIGATION AND SHOULD BE PRESERVED THROUGH A POWER FAILURE.
	BOOL_32			moisture_sensor_control__saved__has_crossed_the_set_point;
	

	// 5/10/2018 rmd : A variable to support the moisture sensing logic during irrigation. Set
	// to 0 at the start time. And, when a station turns ON, if this variable is less than the
	// station history rip cycle count, is incremented. Used as part of the list removal
	// criteria if the sensor reading has crossed the set point is TRUE. NOT SYNC'D DOWN THE
	// CHAIN OR TO COMMSERVER. BUT SHOULD BE SAVED WHEN SET TO TRUE OR FALSE BECAUSE IT CONTROLS
	// IRRIGATION AND SHOULD BE PRESERVED THROUGH A POWER FAILURE.
	UNS_32			moisture_sensor_control__saved__required_cycles_before_removal;


	// 5/17/2018 rmd : This one initialized to (false) each second. Indicates this sensor is not
	// being used to control irrigation right this moment. This variable is dynamic, and not
	// useful outside of the foal irri maintain function. As it changes no file save is
	// necessary.
	BOOL_32 		moisture_sensor_control__transient__has_actively_irrigating_stations;

	// 5/10/2018 rmd : This variable is dynamic, and not useful outside of the foal irri
	// maintain function. As it changes no file save is necessary.
	BOOL_32 		moisture_sensor_control__transient__all_stations_are_off_and_soaking_completed;

	// 5/10/2018 rmd : This variable is dynamic, and not useful outside of the foal irri
	// maintain function. As it changes no file save is necessary.
	BOOL_32 		moisture_sensor_control__transient__all_stations_have_reached_the_required_cycles;
	
	// ----------
	
	// 10/5/2108 rmd : Added in Rev 3

	// 2/29/2016 rmd : This value reflects a percentage called volumetric water content (VWC).
	// With our sensor completely submerged the percentage we calculate apparently comes in at
	// approx 75%. So I would imagine the user set point with the sensor in the ground is going
	// to be something like 35%. We shall see. This variable is sync'd (with the server and the
	// other boxes on the chain if any).
	//
	// 10/5/2018 rmd : In structure Rev 3 we added this variable, allowing us to change the setpoint
	// type from an UNS_32 to a float. This was done to accomodate a change in the moisture reading
	// scaling to be in a range of 0 to 1. The default is 0.32.
	float			moisture_high_set_point_float;

	// 10/5/2018 rmd : In structure Rev 3 we added this variable, allowing us to change the setpoint
	// type from an UNS_32 to a float. This was done to accomodate a change in the moisture reading
	// scaling to be in a range of 0 to 1. The default is 0.32. THIS VARIABLE CURRENTLY NOT USED.
	float			moisture_low_set_point_float;

} MOISTURE_SENSOR_GROUP_STRUCT_3;

// 1/26/2015 rmd : The structure revision number. If the structure definition changes this
// value is to be changed. Typically incremented by 1. Then in the updater function handle
// the change specifics whatever they may be. Generally meaning initialize newly added
// values. But could be more extensive than that.
//
// 9/21/2016 rmd : To revision 1 - we scaled the moisture set point such that a set point of
// 72 coorelated to a moisture sensor raw reading of 30. On the update we multiplied all set
// points by 2.4 to scale them up to a nominal 72. And then limited them to 100.
//
// 10/8/2018 ajv : From 2 to 3, added new float moisture set points to replaced the
// depracated UNS_32 ones.
// 
// 10/8/2018 ajv : From 3 to 4, added variables used to alert user when upper and lower 
// thresholds are crossed for soil moisture, temperature, and electrical conductivity. 
//  
#define	MOISTURE_SENSOR_LATEST_FILE_REVISION		(4)

// ----------

const UNS_32 moisture_sensor_list_item_sizes[ MOISTURE_SENSOR_LATEST_FILE_REVISION + 1 ] =
{
	// 1/26/2015 rmd : In order for this array to contain OLDER list item sizes the implication
	// is that the older structure definitions are to be retained. That means the newer
	// structure definition has a different name.

	// ----------
	
	// 1/30/2015 rmd : Revision 0 structure.
	sizeof( MOISTURE_SENSOR_GROUP_STRUCT_0_and_1 ),
	
	// 9/21/2016 rmd : Revision 1 structure. The size remains the same. Only needed to run
	// through the sensors and scale the set point.
	sizeof( MOISTURE_SENSOR_GROUP_STRUCT_0_and_1 ),
	
	// 10/5/2018 rmd : Revision 2 structure. Added new variables to fix how the moisture sensor
	// interacted with irrigation. There was a 'cycle leveling' bug that needed to be fixed.
	sizeof( MOISTURE_SENSOR_GROUP_STRUCT_2 ),
	
	// ----------
	
	// 10/5/2018 rmd : Revision 3 structure. Changed the EC reading to a float, and added two new moisture
	// setpoints as floats.
	sizeof( MOISTURE_SENSOR_GROUP_STRUCT_3 ),
	
	// 10/14/2018 ajv : Rev 4 added a variety of new variables including high and low 
	// temperature and electrical conductivity sensors and the ability to assign actions if/when
	// a threshold is crossed. 
	sizeof( MOISTURE_SENSOR_GROUP_STRUCT ),

};

/* ---------------------------------------------------------- */
static void nm_moisture_sensor_structure_updater( UNS_32 pfrom_revision )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only when
	// the file revision is changed. The caller is to be holding the
	// moisture_sensor_items_recursive_MUTEX.

	// ----------
	
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;
	
	float	lhigh_set_point_float;

	CHANGE_BITS_32_BIT	*lbitfield_of_changes;

	UNS_32	box_index_0;

	// 9/21/2016 rmd : See note associated with the MOISTURE_SENSOR_MOISTURE_SET_POINT_DEFAULT
	// define. A scaling factor of 2.4 presents the user with a nominal set point of 72.
	const volatile float TWO_POINT_FOUR = 2.4;

	// 10/5/2018 rmd : For revision 3 we undo the Calsense scaling, back to a 0.0 to 1.0 range.
	const volatile float ONE_HUNDRED_POINT_ZERO = 100.0;

	// ----------
	
	box_index_0 = FLOWSENSE_get_controller_index();

	// ----------
	
	if( pfrom_revision == MOISTURE_SENSOR_LATEST_FILE_REVISION )
	{
		Alert_Message_va( "MOISTURE_SENSOR file unexpd update %u", pfrom_revision );	
	}
	else
	{
		Alert_Message_va( "MOISTURE_SENSOR file update : to revision %u from %u", MOISTURE_SENSOR_LATEST_FILE_REVISION, pfrom_revision );

		// ----------
		
		if( pfrom_revision == 0 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 0 to REVISION 1.

			lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );
		
			while( lmois != NULL )
			{
				// 2/4/2016 rmd : Do upgrade work here.
				
				lbitfield_of_changes = &lmois->changes_to_send_to_master;

				// 9/21/2016 rmd : Scale set points to 72. We know when coming from rev 0 to rev 1 the
				// nominal reading is the range of 30. If someone has a set point of 40 they ain't using the
				// sensor. So I'll just draw that as my limit above which I'm moving the setpoint to
				// 72.
				//
				// 10/24/2018 rmd : NOTE - we don't have to use the SET functions when setting these old
				// setpoint variables because they are not sync'd to the commserver or the chain.
				if( lmois->nlu_moisture_high_set_point_uns32 >= 40 )
				{
					lmois->nlu_moisture_high_set_point_uns32 = nlu_MOISTURE_SENSOR_MOISTURE_SET_POINT_UNS32_DEFAULT;
				}
				else
				{
					lmois->nlu_moisture_high_set_point_uns32 = (lmois->nlu_moisture_high_set_point_uns32 * TWO_POINT_FOUR);
				}

				lmois->nlu_moisture_low_set_point_uns32 = nlu_MOISTURE_SENSOR_MOISTURE_SET_POINT_UNS32_MIN;
				
				// ----------
				
				lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
			}
			
			// ----------
			
			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 1 to 2 changes if
			// needed.
			pfrom_revision += 1;
		}
		
		// ----------
		
		if( pfrom_revision == 1 )
		{
			// 5/10/2018 rmd : Actually moving to REV 2 there is no updating work to do. The variables
			// are only used after being intiaized at a start time. Oh, but wait a second, after further
			// thought, suppose we update during irrigation, better set the new variables to something
			// reasonable.
			
			// 5/10/2018 rmd : This is the work to do when moving from REVISION 1 to REVISION 2.
			lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

			while( lmois != NULL )
			{
				// 5/17/2018 rmd : Set to no threat of removal.
				lmois->moisture_sensor_control__saved__has_crossed_the_set_point = (false);

				// 5/17/2018 rmd : Set to MAX so stations are not under the threat of being removed.
				lmois->moisture_sensor_control__saved__required_cycles_before_removal = UNS_32_MAX;

				// ----------
				
				lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
			}

			// ----------

			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 2 to 3 changes if
			// needed.
			pfrom_revision += 1;
		}
		
		// ----------
		
		if( pfrom_revision == 2 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 2 to REVISION 3.

			lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );
		
			while( lmois != NULL )
			{
				// 2/4/2016 rmd : Do upgrade work here.
				
				lbitfield_of_changes = &lmois->changes_to_send_to_master;

				// 10/5/2018 rmd : Undo the Calsense scaling. First divide by 2.4. Then divide by 100. Just
				// to be sure, do some basic range checking first using the old MIN and MAX values.
				
				if( (lmois->nlu_moisture_high_set_point_uns32 > nlu_MOISTURE_SENSOR_MOISTURE_SET_POINT_UNS32_MAX) || (lmois->nlu_moisture_high_set_point_uns32 < nlu_MOISTURE_SENSOR_MOISTURE_SET_POINT_UNS32_MIN) )
				{
					lhigh_set_point_float = MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_DEFAULT;	
				}
				else
				{
					// 10/5/2018 rmd : Undo the previous scaling.
					lhigh_set_point_float = ( ( ((float)lmois->nlu_moisture_high_set_point_uns32) / TWO_POINT_FOUR ) / ONE_HUNDRED_POINT_ZERO );
				}
				
				// 10/5/2018 rmd : Now set the new float variables with the rendition of the old uns_32
				// version.
				nm_MOISTURE_SENSOR_set_moisture_high_set_point_float( lmois, lhigh_set_point_float, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				// 10/5/2018 rmd : I guess we'll just set the low set point, as of yet a programatically
				// unused variable, to the MINIMUM.
				nm_MOISTURE_SENSOR_set_moisture_low_set_point_float( lmois, MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_MIN, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes );

				// ----------
				
				lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
			}
			
			// ----------
			
			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 3 to 4 changes if
			// needed.
			pfrom_revision += 1;
		}
		
		if( pfrom_revision == 3 )
		{
			// 10/8/2018 ajv : This is the work to do when moving from REVISION 3 to REVISION 4.

			lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

			while( lmois != NULL )
			{
				lbitfield_of_changes = &lmois->changes_to_send_to_master;

				// ----------

				// 10/14/2018 ajv : Update the Moisture Sensor Control Mode variable since we added "Do 
				// Nothing" and "Alert Only" actions. Note that there's no need to check to see what the 
				// value currently is because previous revisions only allowed a single mode - stop 
				// irrigation. 
				nm_MOISTURE_SENSOR_set_moisture_control_mode( lmois, MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes ); 

				// ----------

				// 10/8/2018 ajv : And now initialize the new variables
				nm_MOISTURE_SENSOR_set_high_temp_point( lmois, MOISTURE_SENSOR_HIGH_TEMP_POINT_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes ); 

				nm_MOISTURE_SENSOR_set_high_temp_action( lmois, MOISTURE_SENSOR_HIGH_TEMP_ACTION_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes ); 

				nm_MOISTURE_SENSOR_set_low_temp_action( lmois, MOISTURE_SENSOR_LOW_TEMP_ACTION_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes ); 

				nm_MOISTURE_SENSOR_set_high_ec_point( lmois, MOISTURE_SENSOR_HIGH_EC_POINT_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes ); 

				nm_MOISTURE_SENSOR_set_low_ec_point( lmois, MOISTURE_SENSOR_LOW_EC_POINT_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes ); 

				nm_MOISTURE_SENSOR_set_high_ec_action( lmois, MOISTURE_SENSOR_HIGH_EC_ACTION_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes ); 

				nm_MOISTURE_SENSOR_set_low_ec_action( lmois, MOISTURE_SENSOR_LOW_EC_ACTION_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lbitfield_of_changes ); 

				// ----------

				lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
			}

			// ----------

			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 3 to 4 changes if
			// needed.
			pfrom_revision += 1;
		}

	}

	// ----------

	// 1/29/2015 rmd : Well after the updater has run the pfrom_revision is supposed to equal
	// the latest revision. By design of this update process.
	if( pfrom_revision != MOISTURE_SENSOR_LATEST_FILE_REVISION )
	{
		Alert_Message( "MOISTURE_SENSOR updater error" );
	}
}

/* ---------------------------------------------------------- */
extern void init_file_moisture_sensor( void )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;
	
	const volatile float ZERO_POINT_ZERO = 0.0;

	// ----------
	
	FLASH_FILE_initialize_list_and_find_or_create_group_list_file(	FLASH_INDEX_1_GENERAL_STORAGE,
																	MOISTURE_SENSOR_FILENAME,
																	MOISTURE_SENSOR_LATEST_FILE_REVISION,
																	&moisture_sensor_group_list_hdr,
																	moisture_sensor_list_item_sizes,
																	moisture_sensor_items_recursive_MUTEX,
																	&nm_moisture_sensor_structure_updater,
																	&nm_MOISTURE_SENSOR_set_default_values,
																	(char*)MOISTURE_SENSOR_DEFAULT_GROUP_NAME,
																	FF_MOISTURE_SENSORS );
																	
	// ----------
	
	// 2/18/2016 rmd : Set the latest reading flag to false. The readings in the file are not
	// considered real until fresh readings have arrived. As readings arrive they are not saved
	// to the file.

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

	while( lmois != NULL )
	{
		lmois->latest_reading_valid = (false);

		lmois->latest_reading_time_stamp = 0;

		// 5/23/2018 rmd : And make sure to zero this variable each time the code starts. Don't want
		// to start with a value that happened to get saved to the file.
		lmois->invalid_or_old_reading_seconds = 0;
		
		// 2/18/2016 rmd : As a matter of fact set the values to something 'marking' their state.
		lmois->latest_moisture_vwc_percentage = ZERO_POINT_ZERO;

		lmois->latest_temperature_fahrenheit = 0;

		lmois->latest_conductivity_reading_deci_siemen_per_m = ZERO_POINT_ZERO;
		
		// ----------

		// 10/8/2018 ajv : Default all of the "crossed threshold" flags to FALSE on power up. These 
		// are not required to be preserved on a power fail.
		lmois->moisture_threshold_crossed = (false);

		lmois->temperature_threshold_crossed = (false);

		lmois->conductivity_threshold_crossed = (false);

		// ----------
		
		lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
	}
		
	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void save_file_moisture_sensor( void )
{
	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE,
															MOISTURE_SENSOR_FILENAME,
															MOISTURE_SENSOR_LATEST_FILE_REVISION,
															&moisture_sensor_group_list_hdr,
															sizeof( MOISTURE_SENSOR_GROUP_STRUCT ),
															moisture_sensor_items_recursive_MUTEX,
															FF_MOISTURE_SENSORS );
}

/* ---------------------------------------------------------- */
static void nm_MOISTURE_SENSOR_set_default_values( void *const pmois, const BOOL_32 pset_change_bits )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	UNS_32	box_index_0;

	// ----------

	box_index_0 = FLOWSENSE_get_controller_index();

	// ------------------------

	if( pmois != NULL )
	{
		// 5/14/2014 ajv : Clear all of the change bits for SENSOR since it's just been created.
		// We'll then explicitly set each bit based upon whether pset_change_bits is true or not.
		SHARED_clear_all_32_bit_change_bits( &((MOISTURE_SENSOR_GROUP_STRUCT *)pmois)->changes_to_send_to_master, moisture_sensor_items_recursive_MUTEX );
		SHARED_clear_all_32_bit_change_bits( &((MOISTURE_SENSOR_GROUP_STRUCT *)pmois)->changes_to_distribute_to_slaves, moisture_sensor_items_recursive_MUTEX );
		SHARED_clear_all_32_bit_change_bits( &((MOISTURE_SENSOR_GROUP_STRUCT *)pmois)->changes_uploaded_to_comm_server_awaiting_ACK, moisture_sensor_items_recursive_MUTEX );

		// 1/20/2015 ajv : However, we should explicitly set ALL bits for the Comm Server to ensure
		// changes are uploaded on a clean start.
		//
		// 4/12/2016 rmd : WELL for moisture sensors this gets all VERY complicated. Bottom line is
		// we do not want the first dummy moisture decoder list item to be sent to the COMM_SERVER.
		// Because after a we do a discovery we will take over this decoder with one of the
		// discovered ones. And when we do that we will set all bits to send to the comm_server.
		// HOWEVER when we create the second or third in the list we want to set the bits like
		// normal.
		if( nm_ListGetCount( &moisture_sensor_group_list_hdr ) != 0 )
		{
			SHARED_set_all_32_bit_change_bits( &((MOISTURE_SENSOR_GROUP_STRUCT *)pmois)->changes_to_upload_to_comm_server, moisture_sensor_items_recursive_MUTEX );
		}
		
		// ------------------------

		// 5/2/2014 ajv : When setting the default values, always set the changes to be sent to the
		// master. Once the master receives the changes, it will distribute them back out to the
		// slaves.
		lchange_bitfield_to_set = &((MOISTURE_SENSOR_GROUP_STRUCT *)pmois)->changes_to_send_to_master;

		// ------------------------

		// 2/5/2016 rmd : Let's NOT set the default name. There is actually already one made. It is
		// MOISTURE SENSOR + %d(GID) which is fashioned in a low level generic list group create
		// function. If we don't like what we see we could change it here.
		/*
		snprintf( str_48, NUMBER_OF_CHARS_IN_A_NAME, "Moisture Sensor @ %c", box_index_0 + 'A' );
		nm_MOISTURE_SENSOR_set_name( pmois, str_48, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
		*/
		
		nm_MOISTURE_SENSOR_set_box_index( pmois, box_index_0, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_MOISTURE_SENSOR_set_decoder_serial_number( pmois, DECODER_SERIAL_NUM_MIN, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_MOISTURE_SENSOR_set_physically_available( pmois, MOISTURE_SENSOR_PHYSICALLY_AVAILABLE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_MOISTURE_SENSOR_set_in_use( pmois, MOISTURE_SENSOR_IN_USE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_MOISTURE_SENSOR_set_moisture_control_mode( pmois, MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );
		
		nm_MOISTURE_SENSOR_set_low_temp_point( pmois, MOISTURE_SENSOR_LOW_TEMP_POINT_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_MOISTURE_SENSOR_set_additional_soak_seconds( pmois, MOISTURE_SENSOR_ADDITIONAL_SOAK_SECONDS_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_MOISTURE_SENSOR_set_moisture_high_set_point_float( pmois, MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_MOISTURE_SENSOR_set_moisture_low_set_point_float( pmois, MOISTURE_SENSOR_MOISTURE_SET_POINT_FLOAT_MIN, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set );

		nm_MOISTURE_SENSOR_set_high_temp_point( pmois, MOISTURE_SENSOR_HIGH_TEMP_POINT_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set ); 

		nm_MOISTURE_SENSOR_set_high_temp_action( pmois, MOISTURE_SENSOR_HIGH_TEMP_ACTION_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set ); 

		nm_MOISTURE_SENSOR_set_low_temp_action( pmois, MOISTURE_SENSOR_LOW_TEMP_ACTION_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set ); 

		nm_MOISTURE_SENSOR_set_high_ec_point( pmois, MOISTURE_SENSOR_HIGH_EC_POINT_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set ); 

		nm_MOISTURE_SENSOR_set_low_ec_point( pmois, MOISTURE_SENSOR_LOW_EC_POINT_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set ); 

		nm_MOISTURE_SENSOR_set_high_ec_action( pmois, MOISTURE_SENSOR_HIGH_EC_ACTION_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set ); 

		nm_MOISTURE_SENSOR_set_low_ec_action( pmois, MOISTURE_SENSOR_LOW_EC_ACTION_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, pset_change_bits, lchange_bitfield_to_set ); 

	}
	else
	{
		Alert_func_call_with_null_ptr();
	}
}

/* ---------------------------------------------------------- */
/**
 * Creates a new MOISTURE SENSOR group and adds it to the MOISTURE SENSOR list.
 * 
 * @mutex Calling function must have the moisture_sensor_items_recursive_MUTEX mutex to
 *  	  ensure the list isn't modified during this process.
 *
 * @executed Executed within the context of the key processing task when the
 *  		 user creates a new group; within the context of the CommMngr task
 *  		 when a new group is created using a central or handheld radio
 *  		 remote.
 * 
 * @return void* A pointer to the newly created group.
 * 
 * @author 8/11/2011 Adrianusv
 * 
 * @revisions
 *    8/11/2011 Initial release
 */
extern void *nm_MOISTURE_SENSOR_create_new_group( void )
{
	// 2/26/2016 rmd : Creates a new group with physically available and in_use set to (false).
	// Adds the decoder to the END of the list.
	return( nm_GROUP_create_new_group( &moisture_sensor_group_list_hdr, (char*)MOISTURE_SENSOR_DEFAULT_GROUP_NAME, &nm_MOISTURE_SENSOR_set_default_values, sizeof(MOISTURE_SENSOR_GROUP_STRUCT), (true), NULL ) );
}

/* ---------------------------------------------------------- */
/**
 * This function runs through the POC list and copies the contents of any changed POCs to
 * the passed in ucp.
 * 
 * @executed Executed within the context of the CommMngr task as a token, token response, or
 * Program Data message to the Comm Server is being built.
 * 
 * @param pucp A pointer to the pointer where the program data is being built.
 * 
 * @param pmem_used_so_far The amount of memory used so far. Once this reaches our limit for
 * a single token (MEMORY_TO_ALLOCATE_FOR_SENDING_CHANGES), processing of the remaining
 * changes stops until it's time to build the next token or token response.
 * 
 * @param pmem_used_so_far_is_less_than_allocated_memory A pointer to the flag which
 * indicates whether we're allowed to add more changes to the token or token response.
 * 
 * @param preason_data_is_being_built The reason why the data is being built to send. Based
 * upon this reason, we examine a different change bit structure to determine what to
 * send. Valid values are CHANGE_REASON_SYNC_SENDING_TO_MASTER,
 * CHANGE_REASON_SYNC_DISTRIBUTING_TO_SLAVES, CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER.
 * 
 * @return UNS_32 The total size of the added data. This is used to determine whether any
 * changes were detected so the appropriate file change bit can be set as well as to
 * determine how much memory must be allocated to store the changes.
 *
 * @author 9/14/2011 Adrianusv
 */
extern UNS_32 MOISTURE_SENSOR_build_data_to_send(	UNS_8 **pucp,
													const UNS_32 pmem_used_so_far,
													BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
													const UNS_32 preason_data_is_being_built,
													const UNS_32 pallocated_memory )
{
	CHANGE_BITS_32_BIT	*lptr_to_bitfield_of_changes_to_send;

	CHANGE_BITS_32_BIT	lbitfield_of_changes_in_the_msg;
	
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	UNS_8	*lptr_to_num_changed_sensors;

	UNS_8	*lptr_where_to_put_the_bitfield;

	UNS_32	lchanged_sensors_in_the_message;

	// Store the amount of memory used for current POC. This is compared to the POC's overhead
	// to determine whether any of the POC's values were actually added or not.
	UNS_32	lmem_used_by_this_sensor;

	// Determine how much overhead each POC requires in case we add the overhead and don't have
	// enough room to add of the actual changes.
	UNS_32	lmem_overhead_per_sensor;

	UNS_32	rv;

	// ----------

	rv = 0;

	lchanged_sensors_in_the_message = 0;

	// ----------

	// 1/14/2015 ajv : Take the appropriate list mutex.
	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

	// 1/14/2015 ajv : Loop through each and count how many have change bits set. This is really
	// just used to determine whether there's at least one list item which needs to be examined.
	while( lmois != NULL )
	{
		if( *(MOISTURE_SENSOR_get_change_bits_ptr( lmois, preason_data_is_being_built )) != CHANGE_BITS_32_BIT_ALL_CLEAR )
		{
			lchanged_sensors_in_the_message += 1;

			// 1/14/2015 ajv : Since we don't actually care about the number of POCs that have changed
			// at this point - just that at least one has changed - we can break out of the loop now.
			break;
		}

		lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
	}

	// ----------

	if( lchanged_sensors_in_the_message > 0 )
	{
		// 1/14/2015 ajv : Initialize the changed POCs back to 0. Since not all of the POCs may
		// be added due to token size limitations, we need to ensure that the receiving party only
		// processes the number of POCs actually added to the message.
		lchanged_sensors_in_the_message = 0;

		// ----------

		// 2/9/2016 rmd : The very first piece of content is an UNS_32 which is the count of how
		// many changed sensors are included in the msg. If we have already run out of room from
		// prior message content then this function returns 0. We fill in the actual sensor count
		// into the message after it is built.
		rv = PDATA_allocate_space_for_num_changed_groups_in_pucp( pucp, &lptr_to_num_changed_sensors, pmem_used_so_far, pmem_used_so_far_is_less_than_allocated_memory, pallocated_memory );

		// ----------

		// 2/9/2016 rmd : Well there was at least enough space for the 2 bytes for the count of
		// number of changed groups.
		if( *pmem_used_so_far_is_less_than_allocated_memory )
		{
			lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

			while( lmois != NULL )
			{
				lptr_to_bitfield_of_changes_to_send = MOISTURE_SENSOR_get_change_bits_ptr( lmois, preason_data_is_being_built );

				// 1/14/2015 ajv : If this has changed...
				if( *lptr_to_bitfield_of_changes_to_send != CHANGE_BITS_32_BIT_ALL_CLEAR )
				{
					// 2/9/2016 rmd : The OVERHEAD is the decoder serial number, an UNS_32 indicating the size
					// of the bitfield, and the 32-bit change bitfield itself.
					lmem_overhead_per_sensor = (3 * sizeof(UNS_32));

					// 1/14/2015 ajv : If we still have room available in the message for the overhead, add it
					// now.
					if( *pmem_used_so_far_is_less_than_allocated_memory && ((pmem_used_so_far + rv + lmem_overhead_per_sensor) < pallocated_memory) )
					{
						PDATA_copy_var_into_pucp( pucp, &lmois->decoder_serial_number, sizeof(lmois->decoder_serial_number) );

						PDATA_copy_bitfield_info_into_pucp( pucp, sizeof(CHANGE_BITS_32_BIT), sizeof(UNS_32), &lptr_where_to_put_the_bitfield ); 
					}
					else
					{
						// 9/19/2013 ajv : Stop processing if we've already filled out the token with the maximum
						// amount of data to send.
						*pmem_used_so_far_is_less_than_allocated_memory = (false);

						// 2/11/2016 rmd : No overhead added so leave pucp alone.
						
						break;
					}

					lmem_used_by_this_sensor = lmem_overhead_per_sensor;

					// ----------

					// Initialize the bit field
					lbitfield_of_changes_in_the_msg = 0x00;

					lmem_used_by_this_sensor += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(	pucp,
																										&lbitfield_of_changes_in_the_msg,
																										MOISTURE_SENSOR_CHANGE_BITFIELD_name_bit,
																										lptr_to_bitfield_of_changes_to_send,
																										&lmois->changes_uploaded_to_comm_server_awaiting_ACK,
																										&lmois->base.description,
																										NUMBER_OF_CHARS_IN_A_NAME,
																										(lmem_used_by_this_sensor + rv + pmem_used_so_far),
																										pmem_used_so_far_is_less_than_allocated_memory,
																										pallocated_memory,
																										preason_data_is_being_built );

					lmem_used_by_this_sensor += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(	pucp,
																										&lbitfield_of_changes_in_the_msg,
																										MOISTURE_SENSOR_CHANGE_BITFIELD_box_index_bit,
																										lptr_to_bitfield_of_changes_to_send,
																										&lmois->changes_uploaded_to_comm_server_awaiting_ACK,
																										&lmois->box_index_0,
																										sizeof(lmois->box_index_0),
																										(lmem_used_by_this_sensor + rv + pmem_used_so_far),
																										pmem_used_so_far_is_less_than_allocated_memory,
																										pallocated_memory,
																										preason_data_is_being_built );

					lmem_used_by_this_sensor += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(	pucp,
																										&lbitfield_of_changes_in_the_msg,
																										MOISTURE_SENSOR_CHANGE_BITFIELD_decoder_serial_number_bit,
																										lptr_to_bitfield_of_changes_to_send,
																										&lmois->changes_uploaded_to_comm_server_awaiting_ACK,
																										&lmois->decoder_serial_number,
																										sizeof(lmois->decoder_serial_number),
																										(lmem_used_by_this_sensor + rv + pmem_used_so_far),
																										pmem_used_so_far_is_less_than_allocated_memory,
																										pallocated_memory,
																										preason_data_is_being_built );

					lmem_used_by_this_sensor += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(	pucp,
																										&lbitfield_of_changes_in_the_msg,
																										MOISTURE_SENSOR_CHANGE_BITFIELD_physically_available_bit,
																										lptr_to_bitfield_of_changes_to_send,
																										&lmois->changes_uploaded_to_comm_server_awaiting_ACK,
																										&lmois->physically_available,
																										sizeof(lmois->physically_available),
																										(lmem_used_by_this_sensor + rv + pmem_used_so_far),
																										pmem_used_so_far_is_less_than_allocated_memory,
																										pallocated_memory,
																										preason_data_is_being_built );

					lmem_used_by_this_sensor += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(	pucp,
																										&lbitfield_of_changes_in_the_msg,
																										MOISTURE_SENSOR_CHANGE_BITFIELD_in_use_bit,
																										lptr_to_bitfield_of_changes_to_send,
																										&lmois->changes_uploaded_to_comm_server_awaiting_ACK,
																										&lmois->in_use,
																										sizeof(lmois->in_use),
																										(lmem_used_by_this_sensor + rv + pmem_used_so_far),
																										pmem_used_so_far_is_less_than_allocated_memory,
																										pallocated_memory,
																										preason_data_is_being_built );

					lmem_used_by_this_sensor += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(	pucp,
																										&lbitfield_of_changes_in_the_msg,
																										MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_control_mode,
																										lptr_to_bitfield_of_changes_to_send,
																										&lmois->changes_uploaded_to_comm_server_awaiting_ACK,
																										&lmois->moisture_control_mode,
																										sizeof(lmois->moisture_control_mode),
																										(lmem_used_by_this_sensor + rv + pmem_used_so_far),
																										pmem_used_so_far_is_less_than_allocated_memory,
																										pallocated_memory,
																										preason_data_is_being_built );
																										
					lmem_used_by_this_sensor += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(	pucp,
																										&lbitfield_of_changes_in_the_msg,
																										MOISTURE_SENSOR_CHANGE_BITFIELD_low_temp_point_bit,
																										lptr_to_bitfield_of_changes_to_send,
																										&lmois->changes_uploaded_to_comm_server_awaiting_ACK,
																										&lmois->low_temp_point,
																										sizeof(lmois->low_temp_point),
																										(lmem_used_by_this_sensor + rv + pmem_used_so_far),
																										pmem_used_so_far_is_less_than_allocated_memory,
																										pallocated_memory,
																										preason_data_is_being_built );

					lmem_used_by_this_sensor += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(	pucp,
																										&lbitfield_of_changes_in_the_msg,
																										MOISTURE_SENSOR_CHANGE_BITFIELD_additional_soak_seconds_bit,
																										lptr_to_bitfield_of_changes_to_send,
																										&lmois->changes_uploaded_to_comm_server_awaiting_ACK,
																										&lmois->additional_soak_seconds,
																										sizeof(lmois->additional_soak_seconds),
																										(lmem_used_by_this_sensor + rv + pmem_used_so_far),
																										pmem_used_so_far_is_less_than_allocated_memory,
																										pallocated_memory,
																										preason_data_is_being_built );

					lmem_used_by_this_sensor += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(	pucp,
																										&lbitfield_of_changes_in_the_msg,
																										MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_high_set_point_float_bit,
																										lptr_to_bitfield_of_changes_to_send,
																										&lmois->changes_uploaded_to_comm_server_awaiting_ACK,
																										&lmois->moisture_high_set_point_float,
																										sizeof(lmois->moisture_high_set_point_float),
																										(lmem_used_by_this_sensor + rv + pmem_used_so_far),
																										pmem_used_so_far_is_less_than_allocated_memory,
																										pallocated_memory,
																										preason_data_is_being_built );

					lmem_used_by_this_sensor += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(	pucp,
																										&lbitfield_of_changes_in_the_msg,
																										MOISTURE_SENSOR_CHANGE_BITFIELD_moisture_low_set_point_float_bit,
																										lptr_to_bitfield_of_changes_to_send,
																										&lmois->changes_uploaded_to_comm_server_awaiting_ACK,
																										&lmois->moisture_low_set_point_float,
																										sizeof(lmois->moisture_low_set_point_float),
																										(lmem_used_by_this_sensor + rv + pmem_used_so_far),
																										pmem_used_so_far_is_less_than_allocated_memory,
																										pallocated_memory,
																										preason_data_is_being_built );

					lmem_used_by_this_sensor += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(	pucp,
																										&lbitfield_of_changes_in_the_msg,
																										MOISTURE_SENSOR_CHANGE_BITFIELD_high_temp_point_bit, 
																										lptr_to_bitfield_of_changes_to_send,
																										&lmois->changes_uploaded_to_comm_server_awaiting_ACK,
																										&lmois->high_temp_point,
																										sizeof(lmois->high_temp_point),
																										(lmem_used_by_this_sensor + rv + pmem_used_so_far),
																										pmem_used_so_far_is_less_than_allocated_memory,
																										pallocated_memory,
																										preason_data_is_being_built );

					lmem_used_by_this_sensor += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(	pucp,
																										&lbitfield_of_changes_in_the_msg,
																										MOISTURE_SENSOR_CHANGE_BITFIELD_high_temp_action_bit, 
																										lptr_to_bitfield_of_changes_to_send,
																										&lmois->changes_uploaded_to_comm_server_awaiting_ACK,
																										&lmois->high_temp_action,
																										sizeof(lmois->high_temp_action),
																										(lmem_used_by_this_sensor + rv + pmem_used_so_far),
																										pmem_used_so_far_is_less_than_allocated_memory,
																										pallocated_memory,
																										preason_data_is_being_built );

					lmem_used_by_this_sensor += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(	pucp,
																										&lbitfield_of_changes_in_the_msg,
																										MOISTURE_SENSOR_CHANGE_BITFIELD_low_temp_action_bit, 
																										lptr_to_bitfield_of_changes_to_send,
																										&lmois->changes_uploaded_to_comm_server_awaiting_ACK,
																										&lmois->low_temp_action,
																										sizeof(lmois->low_temp_action),
																										(lmem_used_by_this_sensor + rv + pmem_used_so_far),
																										pmem_used_so_far_is_less_than_allocated_memory,
																										pallocated_memory,
																										preason_data_is_being_built );

					lmem_used_by_this_sensor += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(	pucp,
																										&lbitfield_of_changes_in_the_msg,
																										MOISTURE_SENSOR_CHANGE_BITFIELD_high_ec_point_bit, 
																										lptr_to_bitfield_of_changes_to_send,
																										&lmois->changes_uploaded_to_comm_server_awaiting_ACK,
																										&lmois->high_ec_point,
																										sizeof(lmois->high_ec_point),
																										(lmem_used_by_this_sensor + rv + pmem_used_so_far),
																										pmem_used_so_far_is_less_than_allocated_memory,
																										pallocated_memory,
																										preason_data_is_being_built );

					lmem_used_by_this_sensor += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(	pucp,
																										&lbitfield_of_changes_in_the_msg,
																										MOISTURE_SENSOR_CHANGE_BITFIELD_low_ec_point_bit, 
																										lptr_to_bitfield_of_changes_to_send,
																										&lmois->changes_uploaded_to_comm_server_awaiting_ACK,
																										&lmois->low_ec_point,
																										sizeof(lmois->low_ec_point),
																										(lmem_used_by_this_sensor + rv + pmem_used_so_far),
																										pmem_used_so_far_is_less_than_allocated_memory,
																										pallocated_memory,
																										preason_data_is_being_built );

					lmem_used_by_this_sensor += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(	pucp,
																										&lbitfield_of_changes_in_the_msg,
																										MOISTURE_SENSOR_CHANGE_BITFIELD_high_ec_action_bit, 
																										lptr_to_bitfield_of_changes_to_send,
																										&lmois->changes_uploaded_to_comm_server_awaiting_ACK,
																										&lmois->high_ec_action,
																										sizeof(lmois->high_ec_action),
																										(lmem_used_by_this_sensor + rv + pmem_used_so_far),
																										pmem_used_so_far_is_less_than_allocated_memory,
																										pallocated_memory,
																										preason_data_is_being_built );

					lmem_used_by_this_sensor += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(	pucp,
																										&lbitfield_of_changes_in_the_msg,
																										MOISTURE_SENSOR_CHANGE_BITFIELD_low_ec_action_bit, 
																										lptr_to_bitfield_of_changes_to_send,
																										&lmois->changes_uploaded_to_comm_server_awaiting_ACK,
																										&lmois->low_ec_action,
																										sizeof(lmois->low_ec_action),
																										(lmem_used_by_this_sensor + rv + pmem_used_so_far),
																										pmem_used_so_far_is_less_than_allocated_memory,
																										pallocated_memory,
																										preason_data_is_being_built );

					// ----------

					// 2/10/2016 rmd : If we saw any changed bits and ACTUALLY ADDED CONTENT BEYOND THE ORIGINAL
					// OVERHEAD then complete out the message by placing the bitfield in the overhead and
					// updating function housekeeping.
					if( lmem_used_by_this_sensor > lmem_overhead_per_sensor )
					{
						// 2/10/2016 rmd : Bump the count of changed list items in the message. Place the bitfield
						// of changes into the overhead. And update the return value.
						lchanged_sensors_in_the_message += 1;
						
						memcpy( lptr_where_to_put_the_bitfield, &lbitfield_of_changes_in_the_msg, sizeof(lbitfield_of_changes_in_the_msg) );
						
						rv += lmem_used_by_this_sensor;
					}
					else
					{
						// 2/10/2016 rmd : Else back out from the message the overhead already added.
						*pucp -= lmem_overhead_per_sensor;
					}
					
				}

				lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
			}

			// ----------
			
			// 1/14/2015 ajv : If we've added the number of changes but didn't actually add any data
			// because we hit our limit, back out the number of changes.
			if( lchanged_sensors_in_the_message > 0 )
			{
				memcpy( lptr_to_num_changed_sensors, &lchanged_sensors_in_the_message, sizeof(lchanged_sensors_in_the_message) );
			}
			else
			{
				// 2/10/2016 rmd : If we didn't add any content beyond moving the msg pointer over where the
				// number of changed groups goes, then put the pointer back and return a 0 to indicate
				// nothing added.
				*pucp -= sizeof( lchanged_sensors_in_the_message );

				rv = 0;
			}
		}
		else
		{
			// 2/11/2016 rmd : We couldn't do anything, meaning couldn't even add in the
			// num_of_changed_sensors. In that case RV is still 0 and pucp hasn't moved so nothing to
			// do here.
		}
	}

	// ----------

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
extern void MOISTURE_SENSOR_copy_group_into_guivars( const UNS_32 pmois_index_0 )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	lmois = MOISTURE_SENSOR_get_group_at_this_index( pmois_index_0 );

	g_GROUP_ID = nm_GROUP_get_group_ID( lmois );

	strlcpy( GuiVar_GroupName, nm_GROUP_get_name( lmois ), sizeof(GuiVar_GroupName) );

	GuiVar_MoisBoxIndex = lmois->box_index_0;

	GuiVar_MoisDecoderSN = lmois->decoder_serial_number;

	GuiVar_MoisInUse = lmois->in_use;

	GuiVar_MoisPhysicallyAvailable = lmois->physically_available;

	// ----------

	// 4/4/2016 ajv : Not yet used, but let's populate it now while we're doing the work so it's
	// ready in the future.
	GuiVar_MoisControlMode = lmois->moisture_control_mode;

	GuiVar_MoisSetPoint_High = lmois->moisture_high_set_point_float;

	// 4/4/2016 ajv : Not yet used, but let's populate it now while we're doing the work so it's
	// ready in the future.
	GuiVar_MoisSetPoint_Low = lmois->moisture_low_set_point_float;

	// 4/4/2016 ajv : Not editable, but let's populate it now while we're doing the work so it's
	// ready in case we want to allow the user to edit it in the future.
	GuiVar_MoisAdditionalSoakSeconds = lmois->additional_soak_seconds;

	// ----------

	GuiVar_MoisTempPoint_High = lmois->high_temp_point;

	GuiVar_MoisTempPoint_Low = lmois->low_temp_point;

	GuiVar_MoisTempAction_High = lmois->high_temp_action; 

	GuiVar_MoisTempAction_Low = lmois->low_temp_action; 

	// ----------

	GuiVar_MoisECPoint_High = lmois->high_ec_point; 

	GuiVar_MoisECPoint_Low = lmois->low_ec_point;

	GuiVar_MoisECAction_High = lmois->high_ec_action; 

	GuiVar_MoisECAction_Low = lmois->low_ec_action; 

	// ----------

	GuiVar_MoistureTabToDisplay = 0;	// MOISTURE_SENSOR_TAB_MOISTURE defined in e_moisture_sensors.c

	// ----------

	// 1/5/2018 ajv : An 'x' means model 5TM. A 'z' means the model 5TE, which includes 
	// electrical conductivity. If we detect this as the model, we display the EC reading (in 
	// microsiemens per cm) on the Moisture Sensor screen.
	if( lmois->detected_sensor_model == 'z' ) 
	{
		GuiVar_MoisSensorIncludesEC = (true);
	}
	else
	{
		GuiVar_MoisSensorIncludesEC = (false);
	}

	// ----------
	
	// 4/4/2016 ajv : Current sensor measurements are only available at the FLOWSENSE master.
	// Therefore, we're grabbing the controller's address so we can at least display the info on
	// the "A" controller.
	GuiVar_FLControllerIndex_0 = FLOWSENSE_get_controller_index();

	// ----------

	GuiVar_MoisReading_Current = lmois->latest_moisture_vwc_percentage;

	GuiVar_MoisTemp_Current = lmois->latest_temperature_fahrenheit;

	GuiVar_MoisEC_Current = lmois->latest_conductivity_reading_deci_siemen_per_m; 

	// ----------
	
	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void MOISTURE_SENSOR_copy_latest_readings_into_guivars( const UNS_32 pmois_index_0 )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	// 4/22/2016 ajv : Current sensor measurements are only available at the FLOWSENSE master so
	// skip this step if we're not the master.
	if( FLOWSENSE_we_are_a_master_one_way_or_another() )
	{
		xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

		// ----------

		lmois = MOISTURE_SENSOR_get_group_at_this_index( pmois_index_0 );

		GuiVar_MoisReading_Current = lmois->latest_moisture_vwc_percentage;

		GuiVar_MoisTemp_Current = lmois->latest_temperature_fahrenheit;

		GuiVar_MoisEC_Current = lmois->latest_conductivity_reading_deci_siemen_per_m;

		// ----------

		xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
	}
}

/* ---------------------------------------------------------- */
/**
 * Stores the group name. This routine is called when the user presses BACK to
 * close the on-screen keyboard so the list of groups is updated with the new
 * name.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user presses BACK to close the on-screen keyboard.
 *
 * @author 12/11/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void MOISTURE_SENSOR_extract_and_store_group_name_from_GuiVars( void )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	lmois = MOISTURE_SENSOR_get_group_at_this_index( g_GROUP_list_item_index );

	if( lmois != NULL )
	{
		SHARED_set_name_32_bit_change_bits( lmois, GuiVar_GroupName, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, MOISTURE_SENSOR_get_change_bits_ptr( lmois, CHANGE_REASON_KEYPAD ), &lmois->changes_to_upload_to_comm_server, MOISTURE_SENSOR_CHANGE_BITFIELD_name_bit );
	}

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Copies the contents of the GuiVars, used for editing by the easyGUI library,
 * into a temporary structure which is used for comparison with a group in the
 * list to identify changes.  Once this is done, the temporary group is passed
 * into the _store_changes routine where the settings are stored into memory.
 * 
 * @executed Executed within the context of the display processing task.
 * 
 * @author 8/11/2011 Adrianusv
 *
 * @revisions
 *    8/11/2011 Initial release
 *    4/5/2012  Added passing of (false) into set routine to indiciate the
 *  			change was made locally so change bits are set properly.
 *  			Modified so all memory allocation, storing of changes, and
 *  			memory deallocation are handled within this routine.
 */
extern void MOISTURE_SENSOR_extract_and_store_changes_from_GuiVars( void )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lmois = mem_malloc( sizeof(MOISTURE_SENSOR_GROUP_STRUCT) );

	memcpy( lmois, MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number( GuiVar_MoisDecoderSN ), sizeof(MOISTURE_SENSOR_GROUP_STRUCT) );
	
	// ----------

	strlcpy( lmois->base.description, GuiVar_GroupName, sizeof(lmois->base.description) );

	// ----------

	lmois->moisture_control_mode = GuiVar_MoisControlMode;

	lmois->moisture_high_set_point_float = GuiVar_MoisSetPoint_High;

	lmois->moisture_low_set_point_float = GuiVar_MoisSetPoint_Low;

	lmois->additional_soak_seconds = GuiVar_MoisAdditionalSoakSeconds;


	lmois->high_temp_point = GuiVar_MoisTempPoint_High;

	lmois->low_temp_point = GuiVar_MoisTempPoint_Low;

	lmois->high_temp_action = GuiVar_MoisTempAction_High; 

	lmois->low_temp_action = GuiVar_MoisTempAction_Low; 

	// ----------

	lmois->high_ec_point = GuiVar_MoisECPoint_High;

	lmois->low_ec_point = GuiVar_MoisECPoint_Low;

	lmois->high_ec_action = GuiVar_MoisECAction_High; 

	lmois->low_ec_action = GuiVar_MoisECAction_Low; 

	// ----------

	nm_MOISTURE_SENSOR_store_changes( lmois, g_GROUP_creating_new, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, CHANGE_REASON_KEYPAD, 0 );

	// Reset the Creating New flag to ensure we don't accidentally create
	// another new group when using the BACK key.
	g_GROUP_creating_new = (false);

	// ----------

	mem_free( lmois );

	// ----------
	
	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Copies the group's name and settings into the Group List GuiVars to display
 * on the list of groups.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *  	  group whose value is being read isn't deleted during this process.
 *
 * @executed Executed within the context of the display processing task when the
 *           group list is displayed.
 * 
 * @param pindex_0 The 0-based index into the irrigation priorities group list.
 *
 * @author 9/14/2011 Adrianusv
 *
 * @revisions
 *    9/14/2011 Initial release
 */
extern void nm_MOISTURE_SENSOR_load_group_name_into_guivar( const INT_16 pindex_0_i16 )
{
	MOISTURE_SENSOR_GROUP_STRUCT *lmois;

	lmois = MOISTURE_SENSOR_get_group_at_this_index( (UNS_32)pindex_0_i16 );

	if( nm_OnList(&moisture_sensor_group_list_hdr, lmois) )
	{
		strlcpy( GuiVar_itmGroupName, nm_GROUP_get_name( lmois ), NUMBER_OF_CHARS_IN_A_NAME );
	}
	else
	{
		Alert_group_not_found();
	}
}

/* ---------------------------------------------------------- */
extern UNS_32 MOISTURE_SENSOR_get_index_using_ptr_to_mois_struct( const MOISTURE_SENSOR_GROUP_STRUCT *const pmois )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	UNS_32	rv;

	UNS_32	i;

	rv = 0;

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

	for( i=0; i<nm_ListGetCount( &moisture_sensor_group_list_hdr ); ++i )
	{
		if( lmois == pmois )
		{
			rv = i;

			break;
		}

		lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
	}

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern MOISTURE_SENSOR_GROUP_STRUCT *nm_MOISTURE_SENSOR_get_pointer_to_file_moisture_sensor_with_this_box_index_and_decoder_serial_number( const UNS_32 pbox_index, const UNS_32 pdecoder_sn )
{
	// 10/29/2014 rmd : Caller must be holding the moisture_sensor_items_recursive_MUTEX.
	// Returns NULL is sensor not found.
	
	// ----------
	
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

	while( lmois != NULL )
	{
		if( (lmois->box_index_0 == pbox_index) && (lmois->decoder_serial_number == pdecoder_sn) )
		{
			break;
		}

		lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
	}

	return ( lmois );
}

/* ---------------------------------------------------------- */
extern MOISTURE_SENSOR_GROUP_STRUCT *MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number( UNS_32 pserial_number )
{
	// 3/30/2016 rmd : Will return a NULL if cannot find a moisture decoder in the list with
	// the serial number of interest. Also if the caller uses a 0 serial number we return a NULL
	// as a 0 for the serial number means no moisture sensing in use.
	
	MOISTURE_SENSOR_GROUP_STRUCT	*rv;

	rv = NULL;
	
	// 3/31/2016 rmd : This is important to explicitly check for a 0 serial number as the
	// station group uses a 0 decoder serial number as the flag that there is no moisture sensor
	// attached to the station group.
	if( pserial_number != 0 )
	{
		xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );
	
		rv = nm_ListGetFirst( &moisture_sensor_group_list_hdr );
	
		while( rv != NULL )
		{
			if( rv->decoder_serial_number == pserial_number )
			{
				break;
			}
	
			rv = nm_ListGetNext( &moisture_sensor_group_list_hdr, rv );
		}
	
		xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
BOOL_32 MOISTURE_SENSOR_this_decoder_is_in_use_and_physically_available( UNS_32 pserial_number  )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois_ptr;
	
	BOOL_32		rv;
	
	rv = (false);

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );
	
	lmois_ptr = MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number( pserial_number );

	if( lmois_ptr )
	{
		rv = ( (lmois_ptr->in_use) && (lmois_ptr->physically_available) );
	}

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the group with the specified group ID. This is a wrapper
 * for the nm_GROUP_get_ptr_to_group_at_this_location_in_list function so we
 * don't need to expose the list header.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure the
 *        group that's found isn't deleted before it's used.
 *
 * @executed Executed within the context of the display processing task when the
 *  		 list and group screens are drawn; within the key processing task
 *  		 when the user navigates through the group list screen.
 * 
 * @param pindex_0 The index of the group to retrieve - 0 based.
 * 
 * @return void* A pointer to the group with the specified group ID. Or a NULL if the index
 * is beyond the lists range.
 *
 * @author 9/15/2011 Adrianusv
 *
 * @revisions
 *    9/15/2011 Initial release
 */
extern void *MOISTURE_SENSOR_get_group_at_this_index( const UNS_32 pindex_0 )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	lmois = nm_GROUP_get_ptr_to_group_at_this_location_in_list( &moisture_sensor_group_list_hdr, pindex_0 );

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	return( lmois );
}

/* ---------------------------------------------------------- */
extern void *MOISTURE_SENSOR_get_group_with_this_GID( const UNS_32 pgroup_ID )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	lmois = nm_GROUP_get_ptr_to_group_with_this_GID( &moisture_sensor_group_list_hdr, pgroup_ID, (false) );

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	return( lmois );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Returns the GID for the POC group at the index location within the
    list.

    @CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the DISPLAY PROCESSING task. When the poc
    accumulator report is being drawn.

    @RETURN The GID for the group at pindex location in the list of poc's. Or 0 if group not
    found.

	@ORIGINAL 2012.08.01 rmd

	@REVISIONS (none)
*/
extern UNS_32 MOISTURE_SENSOR_get_gid_of_group_at_this_index( const UNS_32 pindex_0 )
{
	UNS_32	rv;
	
	rv = 0;
	
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	lmois = nm_GROUP_get_ptr_to_group_at_this_location_in_list( &moisture_sensor_group_list_hdr, pindex_0 );
	
	if( lmois != NULL )
	{
		rv = lmois->base.group_identity_number;
	}

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * THIS FUNCTION IS ONLY ALLOWED FROM THE STATION GROUPS SCREEN! It is relies on the fact
 * that there is one extra index (0) which is used to signify there is no Moisture Sensor
 * assigned to the group.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task when building
 * a list of moisture sensors to assign to a Station Group.
 * 
 * @param pserial_num The serial number of the moisture sensor decoder currently assigned to
 * the Station Group. A serial number of 0 is used to signify there is no sensor assigned.
 *
 * @return UNS_32 The 1-based index of the moisture sensor decoder. If a 0 is passed in, a 0
 * is returned to indicate NONE.
 *
 * @author 4/4/2016 AdrianusV
 */
extern UNS_32 MOISTURE_SENSOR_get_index_for_group_with_this_serial_number( const UNS_32 pserial_num )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lsensor;

	UNS_32	rv;

	// 4/4/2016 ajv : We use a 1-based index, rather than the traditional 0-based index because
	// a value of 0 indicates there is no sensor assigned to a group.
	rv = 1;

	// ----------

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	if( pserial_num == 0 )
	{
		// 4/4/2016 ajv : A serial number of 0 means there is no serial number assigned.
		rv = 0;
	}
	else // (pserial_num > 0)
	{
		// 4/4/2016 ajv : Traverse through the list looking for the group that matches the passed in
		// serial number
		lsensor = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

		while( lsensor != NULL )
		{
			if( lsensor->decoder_serial_number == pserial_num )
			{
				break;
			}

			lsensor = nm_ListGetNext( &moisture_sensor_group_list_hdr, lsensor );

			++rv;
		}

		// 4/4/2016 ajv : If we've reached the end of the list without finding a match, return 0 to
		// display NONE
		if( lsensor == NULL )
		{
			Alert_group_not_found();

			rv = 0;
		}
	}

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Gets the last POC group ID by retrieving the number_of_groups_ever_created
 * value from the head of the list.
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure
 *  	  another group isn't added to the list between the number being
 *  	  retrieved and being used.
 *
 * @executed Executed within the context of the startup task when the stations 
 *  		 list is created for the first time; also executed within the
 *  		 display processing and key processing task as the user enters and
 *  		 leaves the editing screen.
 * 
 * @return UNS_32 The group ID of the last POC group.
 *
 * @author 9/7/2011 Adrianusv
 *
 * @revisions
 *    9/7/2011   Initial release
 *    2/10/2012	 Renamed to include nm_ prefix
 */
extern UNS_32 MOISTURE_SENSOR_get_last_group_ID( void )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	rv = ((GROUP_BASE_DEFINITION_STRUCT*)moisture_sensor_group_list_hdr.phead)->number_of_groups_ever_created;

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the number of items in the list. This is a wrapper for the 
 * nm_ListGetCount macro so the list header doesn't need to be exposed. 
 * 
 * @mutex Calling function must have the poc_recursive_MUTEX mutex to ensure a
 *  	  group isn't added or removed between the count being retrieved and
 *  	  being used.
 *
 * @executed Executed within the context of the display processing task when the 
 *  		 list of groups is drawn; within the context of the key processing
 *  		 task when the user moves up and down the list of groups and when
 *  		 the user tries to delete a group.
 * 
 * @return UNS_32 The number of items in the list.
 *
 * @author 9/7/2011 Adrianusv
 *
 * @revisions
 *    9/7/2011   Initial release
 *    2/10/2012	 Renamed to include nm_ prefix
 */
extern UNS_32 MOISTURE_SENSOR_get_list_count( void )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	rv = nm_ListGetCount( &moisture_sensor_group_list_hdr );

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 MOISTURE_SENSOR_get_physically_available( MOISTURE_SENSOR_GROUP_STRUCT *const pmois )
{
	BOOL_32	rv;

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_bool_32_bit_change_bits_group( pmois,
												   &pmois->physically_available,
												   MOISTURE_SENSOR_PHYSICALLY_AVAILABLE_DEFAULT,
												   &nm_MOISTURE_SENSOR_set_physically_available,
												   &pmois->changes_to_send_to_master,
												   MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_physically_available_bit ] );

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 MOISTURE_SENSOR_get_decoder_serial_number( MOISTURE_SENSOR_GROUP_STRUCT *const pmois )
{
	BOOL_32	rv;

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32_32_bit_change_bits_group( pmois,
													 &pmois->decoder_serial_number,
													 DECODER_SERIAL_NUM_MIN,
													 DECODER_SERIAL_NUM_MAX,
													 DECODER_SERIAL_NUM_DEFAULT,
													 &nm_MOISTURE_SENSOR_set_decoder_serial_number,
													 &pmois->changes_to_send_to_master,
													 MOISTURE_SENSOR_database_field_names[ MOISTURE_SENSOR_CHANGE_BITFIELD_decoder_serial_number_bit ] );

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 MOISTURE_SENSOR_get_num_of_moisture_sensors_connected( void )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	UNS_32	rv;

	rv = 0;

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

	while( lmois != NULL )
	{
		if( lmois->physically_available && lmois->in_use )
		{
			rv += 1;
		}

		lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
	}

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 MOISTURE_SENSOR_fill_out_recorder_record( MOISTURE_SENSOR_GROUP_STRUCT *pmois, MOISTURE_SENSOR_RECORDER_RECORD *pmsrr )
{
	// 2/18/2016 rmd : Return false if pmois is not on file list. If it is fill out the recorder
	// record.
	
	// ----------
	
	BOOL_32	rv;

	rv = (false);

	// ----------
	
	// 2/18/2016 rmd : Since we are working with the file list.
	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	// 2/18/2016 rmd : Since we are working with the recorder array.
	xSemaphoreTakeRecursive( moisture_sensor_recorder_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	if( nm_OnList( &moisture_sensor_group_list_hdr, pmois ) )
	{
		pmsrr->moisture_vwc_percentage = pmois->latest_moisture_vwc_percentage;

		pmsrr->temperature_fahrenheit = pmois->latest_temperature_fahrenheit;

		pmsrr->latest_conductivity_reading_deci_siemen_per_m = pmois->latest_conductivity_reading_deci_siemen_per_m;

		pmsrr->decoder_serial_number = pmois->decoder_serial_number;

		// 3/29/2016 rmd : Okay to assign the UNS_32 to the UNS_8. Only holds the character values
		// 'x' or 'z'.
		pmsrr->sensor_type = pmois->detected_sensor_model;
		
		// 2/18/2016 rmd : And return (true) indicating success.
		rv = (true);
	}
	else
	{
		Alert_Message( "Moisture Report: Sensor not on list!" );
	}

	// ----------
	
	xSemaphoreGiveRecursive( moisture_sensor_recorder_recursive_MUTEX );

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern CHANGE_BITS_32_BIT *MOISTURE_SENSOR_get_change_bits_ptr( MOISTURE_SENSOR_GROUP_STRUCT *pmois, const UNS_32 pchange_reason )
{
	return( SHARED_get_32_bit_change_bits_ptr( pchange_reason, &pmois->changes_to_send_to_master, &pmois->changes_to_distribute_to_slaves, &pmois->changes_to_upload_to_comm_server ) );
}

/* ---------------------------------------------------------- */
extern void MOISTURE_SENSOR_clean_house_processing( void )
{
	// 4/16/2014 rmd : Executed within the context of the comm_mngr task. ONLY BY SLAVES. THE
	// CHANGES MADE HERE DO NOT BE DISTRIBUTED. THE STATION LIST IS BEING WIPED IN PREPARATION
	// OF BEING ADDED AS A NEW MEMBER TO A CHAIN. Called when the result of a scan indicates
	// this controller is a new slave in the chain. Used when building up chains. Likely ONLY
	// used when assembling a chain for the first time or when increasing the length of a chain
	// to a length it has never 'run' at before. Or if you changed the LETTER of a slave in a
	// chain.
	
	// ----------

	void	*lmois;
	
	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );
	
	lmois = nm_ListRemoveHead( &moisture_sensor_group_list_hdr );

	while( lmois != NULL )
	{
		mem_free( lmois );

		lmois = nm_ListRemoveHead( &moisture_sensor_group_list_hdr );
	}

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	// ----------

	// 2/19/2016 rmd : It's okay to save the empty file here. Save and allow moisture sensors to
	// be created unsing the normal method.
	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE, MOISTURE_SENSOR_FILENAME, MOISTURE_SENSOR_LATEST_FILE_REVISION, &moisture_sensor_group_list_hdr, sizeof( MOISTURE_SENSOR_GROUP_STRUCT ), moisture_sensor_items_recursive_MUTEX, FF_MOISTURE_SENSORS );

	// ----------
	
	// 2/19/2016 rmd : I think we should free the recorder records and null the msrcs too.
	xSemaphoreTakeRecursive( moisture_sensor_recorder_recursive_MUTEX, portMAX_DELAY );

	if( msrcs.original_allocation != NULL )
	{
		mem_free( msrcs.original_allocation );
	}
	
	memset( &msrcs, 0x00, sizeof(MOISTURE_SENSOR_RECORDER_CONTROL_STRUCT) );
	
	xSemaphoreGiveRecursive( moisture_sensor_recorder_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void MOISTURE_SENSOR_set_not_physically_available_based_upon_communication_scan_results( void )
{
	// 4/24/2014 rmd : Executed within the context of the comm_mngr. At the conclusion of the
	// scan by masters (chains and standalones). By standalones so they could clean their list
	// in case they were in a chain previously. The changes made here should be marked for
	// distribution.
	
	// ----------
	
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	UNS_32	our_box_index_0;

	// ----------

	our_box_index_0 = FLOWSENSE_get_controller_index();
	
	// ----------

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

	while( lmois != NULL )
	{
		// 4/24/2014 rmd : Can only HIDE stations based upon the scan results. Revealing or station
		// creating takes place when a controller receives its whats installed from its OWN tpmicro.
		if( !chain.members[ lmois->box_index_0 ].saw_during_the_scan )
		{
			// 4/24/2014 rmd : Set physically_available (false) if needed. Set the change bits so the
			// change is distributed.
			nm_MOISTURE_SENSOR_set_physically_available( lmois, (false), CHANGE_generate_change_line, CHANGE_REASON_NORMAL_STARTUP, our_box_index_0, (true), MOISTURE_SENSOR_get_change_bits_ptr( lmois, CHANGE_REASON_NORMAL_STARTUP ) );
		}
		
		// ----------
		
		lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
	}

	// ----------

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void MOISTURE_SENSOR_set_physically_available_or_create_new_moisture_sensors_as_needed_based_upon_the_discovery_results( void )
{
	// 2/23/2016 rmd : Executed within the context of the comm_mngr upon receipt of the
	// DISCOVERY results from the TPMicro. Think of this as executing at the SLAVE. We look for
	// moisture sensor decoders that are not yet in the file list and create them if not.

	// ----------
	
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;
	
	UNS_32				our_box_index_0, ddd;
	
	BOOL_32				decoder_present;

	char	str_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	// ----------

	our_box_index_0 = FLOWSENSE_get_controller_index();
	
	// ----------

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	// 2/23/2016 rmd : FIRST go through the decoders delivered from the TPMicro looking for
	// matched in the file list. If no match found create the decoder in the list.
	ddd = 0;
	
	while( tpmicro_data.decoder_info[ ddd ].sn )
	{
		// 2/23/2016 rmd : ONLY if the decoder type is a moisture sensor decoder should it be in the
		// moisture sensor file.
		if( tpmicro_data.decoder_info[ ddd ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__MOISTURE )
		{
			lmois = MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number( tpmicro_data.decoder_info[ ddd ].sn );
	
			if( lmois )
			{
				// 4/13/2016 rmd : We found it. It already exists by serial number in the file list.
				
				lchange_bitfield_to_set = MOISTURE_SENSOR_get_change_bits_ptr( lmois, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED );
			
				// ----------
				
				// 2/23/2016 rmd : So the group already exists. Set his physically available to (true).
				nm_MOISTURE_SENSOR_set_physically_available( lmois, (true), CHANGE_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, our_box_index_0, (true), MOISTURE_SENSOR_get_change_bits_ptr( lmois, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED ) );

				// 2/23/2016 rmd : So the group already exists. Well if we found it during a discovery we
				// should also set the in_use variable to (true). Not sure how in_use and mositure decoders
				// play together anyway.
				nm_MOISTURE_SENSOR_set_in_use( lmois, (true), CHANGE_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, our_box_index_0, (true), MOISTURE_SENSOR_get_change_bits_ptr( lmois, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED ) );

				// 4/12/2016 rmd : And make sure box index is correct incase he moved the sensor from one 2W
				// box cable to another WITHIN the same NETWORK ID.
				nm_MOISTURE_SENSOR_set_box_index( lmois, our_box_index_0, CHANGE_do_not_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, our_box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
			}
			else
			{
				// 4/11/2016 rmd : Before we go to create a list item see if the list ONLY has the 'DUMMY'
				// place-holder decoder. With a 0 serial number. This would be the list item we made when
				// the file was created. If that is the ONLY list item we'll take it over. This step helps
				// AJ with the UI and cleans up some list presentation issues. How to easily skip over dummy
				// list items. He says so.
				lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

				// 4/11/2016 rmd : There should always be at least ONE list item but I'm checking for NULL
				// just in case.
				if( (lmois != NULL) && (nm_ListGetCount( &moisture_sensor_group_list_hdr ) == 1) && (lmois->decoder_serial_number == 0) )
				{
					// 4/11/2016 rmd : Do not create one. We have a pointer to the 'dummy' list item that has a
					// 0 serial number. We will use it below. BUT we must set ALL the bits to ensure this ENTIRE
					// list item is sent to the CommServer because it has not yet been sent at all. By choice.
					SHARED_set_all_32_bit_change_bits( &(lmois->changes_to_upload_to_comm_server), moisture_sensor_items_recursive_MUTEX );
				}
				else
				{
					// 2/26/2016 rmd : Create a new group with physically available set to (false) and in_use
					// set to (false). We need to explicitly set those two to (true).
					lmois = nm_MOISTURE_SENSOR_create_new_group();
				}
				
				// ----------
				
				lchange_bitfield_to_set = MOISTURE_SENSOR_get_change_bits_ptr( lmois, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED );
			
				// ----------
				
				nm_MOISTURE_SENSOR_set_box_index( lmois, our_box_index_0, CHANGE_do_not_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, our_box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
		
				nm_MOISTURE_SENSOR_set_decoder_serial_number( lmois, tpmicro_data.decoder_info[ ddd ].sn, CHANGE_do_not_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, our_box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
		
				// 2/26/2016 rmd : Set the name first so that when we set the physically available and the
				// in_use variables the CHANGE line uses this GROUP NAME. Otherwise it uses a GID as the
				// identifier of which decoder the changes happened to.
				//
				// 9/21/2016 rmd : Becareful of the name length. It has to fit reasonably on the screen when
				// looking at the list of moisture sensors when setting the set point.
				snprintf( str_48, NUMBER_OF_CHARS_IN_A_NAME, "Decoder %07d @ %c", tpmicro_data.decoder_info[ ddd ].sn, (our_box_index_0 + 'A') );
				nm_MOISTURE_SENSOR_set_name( lmois, str_48, CHANGE_do_not_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, our_box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );

				// 2/26/2016 rmd : Set BOTH physically available and in_use true here. So far the plan is
				// throughout the application ONLY the physically available variable needs to be tested to
				// decide to make a sensor available to the user.
				nm_MOISTURE_SENSOR_set_physically_available( lmois, (true), CHANGE_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, our_box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );

				// 2/26/2016 rmd : I decided we only need to show the PHYSICALLY AVAILABLE change line. So
				// suppress the change line for this change.
				nm_MOISTURE_SENSOR_set_in_use( lmois, (true), CHANGE_do_not_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_ADDED, our_box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
			}
		}
		
		// ----------
		
		ddd += 1;
	}
	
	// ----------
	
	// 2/23/2016 rmd : SECOND now go through the file list and look for decoders that are no
	// longer in the TPMicro array so that we can mark them NOT physically available.
	lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

	while( lmois != NULL )
	{
		// 7/8/2014 rmd : Only interested in moisture sensors listed as attached to this box.
		if( lmois->box_index_0 == our_box_index_0 )
		{
			// 2/12/2016 rmd : Initialize our tracker.
			decoder_present = (false);
			
			for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
			{
				// 7/8/2014 rmd : If the serial number is 0 we've reached the end of populated entries in
				// the list.
				if( !tpmicro_data.decoder_info[ ddd ].sn )
				{
					break;					
				}

				if( tpmicro_data.decoder_info[ ddd ].sn == lmois->decoder_serial_number )
				{
					decoder_present = (true);

					break;
				}
			}
			
			if( !decoder_present )
			{
				// 2/23/2016 rmd : Set physically available to (false). If already (false) no effect or
				// change distribution takes place.
				nm_MOISTURE_SENSOR_set_physically_available( lmois, (false), CHANGE_generate_change_line, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_REMOVED, our_box_index_0, (true), MOISTURE_SENSOR_get_change_bits_ptr( lmois, CHANGE_REASON_TERMINAL_CARD_OR_DECODER_REMOVED ) );
			}
			
		}  // of if the moisture sensor is specified as being on our box
		
		lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );

	}  // of while for each moisture sensor group item

	// ----------

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void MOISTURE_SENSORS_check_each_sensor_for_a_crossed_threshold( void )
{
	// 10/9/2018 ajv : Called within the context of the TDCHECK task, at a one hertz rate each
	// time the maintain function executes. Called only ONCE per second.
	
	// 5/17/2018 rmd : For each moisture sensor see if it is appropriate to take a reading, and
	// check if one of the thresholds has been crossed and, if so, take the user-specified 
	// action.
	
	// ----------
	
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

	while( lmois != NULL )
	{
		// 10/9/2018 ajv : Check if the reading is recent enough to use. Remember we are taking a
		// reading once every 2 minutes. That is plenty frequently enough to manage irrigation with.
		// Give some margin up and above the read frequency. Let's use one minute margin.
		if( lmois->latest_reading_valid && (((UNS_32)xTaskGetTickCount() - lmois->latest_reading_time_stamp) < (MS_to_TICKS( TPMICRO_MS_BETWEEN_GETTING_MOISTURE_READING_FROM_DECODERS + (60*1000)))) )
		{
			// 10/11/2018 ajv : First, let's see if we've crossed the Moisture threshold
			if( (lmois->moisture_threshold_crossed == (false)) && (lmois->latest_moisture_vwc_percentage > lmois->moisture_high_set_point_float) )
			{
				// 10/9/2018 ajv : Negate the threshold_crossed variable to set it to the new correct value.
				lmois->moisture_threshold_crossed = !lmois->moisture_threshold_crossed;

				if( lmois->moisture_control_mode == MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_alert_only ) 
				{
					Alert_soil_moisture_crossed_threshold( lmois->decoder_serial_number, lmois->latest_moisture_vwc_percentage, lmois->moisture_low_set_point_float, lmois->moisture_high_set_point_float, MOISTURE_SENSOR_THRESHOLD_CROSSED_UPPER ); 
				}
			}
			else if( (lmois->moisture_threshold_crossed == (false)) && (lmois->latest_moisture_vwc_percentage < lmois->moisture_low_set_point_float) )
			{
				// 10/9/2018 ajv : Negate the threshold_crossed variable to set it to the new correct value.
				lmois->moisture_threshold_crossed = !lmois->moisture_threshold_crossed;

				if( lmois->moisture_control_mode == MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_alert_only ) 
				{
					Alert_soil_moisture_crossed_threshold( lmois->decoder_serial_number, lmois->latest_moisture_vwc_percentage, lmois->moisture_low_set_point_float, lmois->moisture_high_set_point_float, MOISTURE_SENSOR_THRESHOLD_CROSSED_LOWER ); 
				}
			}
			else if( (lmois->moisture_threshold_crossed == (true)) && (lmois->latest_moisture_vwc_percentage > lmois->moisture_low_set_point_float) && (lmois->latest_moisture_vwc_percentage < lmois->moisture_high_set_point_float) )
			{
				// 10/9/2018 ajv : Negate the threshold_crossed variable to set it to the new correct value.
				lmois->moisture_threshold_crossed = !lmois->moisture_threshold_crossed;

				if( lmois->moisture_control_mode == MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_alert_only ) 
				{
					Alert_soil_moisture_crossed_threshold( lmois->decoder_serial_number, lmois->latest_moisture_vwc_percentage, lmois->moisture_low_set_point_float, lmois->moisture_high_set_point_float, MOISTURE_SENSOR_THRESHOLD_WITHIN_RANGE ); 
				}
			}

			// 10/11/2018 ajv : Next, let's see if we've crossed the temperature threshold
			if( (lmois->temperature_threshold_crossed == (false)) && (lmois->latest_temperature_fahrenheit > lmois->high_temp_point) ) 
			{
				// 10/9/2018 ajv : Negate the threshold_crossed variable to set it to the new correct value.
				lmois->temperature_threshold_crossed = !lmois->temperature_threshold_crossed;
			
				if( lmois->high_temp_action == MOISTURE_SENSOR_HIGH_TEMP_ACTION_alert_only ) 
				{
					Alert_soil_temperature_crossed_threshold( lmois->decoder_serial_number, lmois->latest_temperature_fahrenheit, lmois->low_temp_point, lmois->high_temp_point, MOISTURE_SENSOR_THRESHOLD_CROSSED_UPPER ); 
				}
			}
			else if( (lmois->temperature_threshold_crossed == (false)) && (lmois->latest_temperature_fahrenheit < lmois->low_temp_point) )
			{
				// 10/9/2018 ajv : Negate the threshold_crossed variable to set it to the new correct value.
				lmois->temperature_threshold_crossed = !lmois->temperature_threshold_crossed;

				if( lmois->low_temp_action == MOISTURE_SENSOR_LOW_TEMP_ACTION_alert_only ) 
				{
					Alert_soil_temperature_crossed_threshold( lmois->decoder_serial_number, lmois->latest_temperature_fahrenheit, lmois->low_temp_point, lmois->high_temp_point, MOISTURE_SENSOR_THRESHOLD_CROSSED_LOWER ); 
				}
			}
			else if( (lmois->temperature_threshold_crossed == (true)) && (lmois->latest_temperature_fahrenheit > lmois->low_temp_point) && (lmois->latest_temperature_fahrenheit < lmois->high_temp_point) )
			{
				// 10/9/2018 ajv : Negate the threshold_crossed variable to set it to the new correct value.
				lmois->temperature_threshold_crossed = !lmois->temperature_threshold_crossed;

				if( (lmois->high_temp_action == MOISTURE_SENSOR_HIGH_TEMP_ACTION_alert_only) || (lmois->low_temp_action == MOISTURE_SENSOR_LOW_TEMP_ACTION_alert_only) ) 
				{
					Alert_soil_temperature_crossed_threshold( lmois->decoder_serial_number, lmois->latest_temperature_fahrenheit, lmois->low_temp_point, lmois->high_temp_point, MOISTURE_SENSOR_THRESHOLD_WITHIN_RANGE ); 
				}
			}

			// 10/11/2018 ajv : Finally, check if we've crossed the EC threshold
			if( (lmois->conductivity_threshold_crossed == (false)) && (lmois->latest_conductivity_reading_deci_siemen_per_m > lmois->high_ec_point) ) 
			{
				// 10/9/2018 ajv : Negate the threshold_crossed variable to set it to the new correct value.
				lmois->conductivity_threshold_crossed = !lmois->conductivity_threshold_crossed;

				if( lmois->high_ec_action == MOISTURE_SENSOR_HIGH_EC_ACTION_alert_only ) 
				{
					Alert_soil_conductivity_crossed_threshold( lmois->decoder_serial_number, lmois->latest_conductivity_reading_deci_siemen_per_m, lmois->low_ec_point, lmois->high_ec_point, MOISTURE_SENSOR_THRESHOLD_CROSSED_UPPER ); 
				}
			}
			else if( (lmois->conductivity_threshold_crossed == (false)) && (lmois->latest_conductivity_reading_deci_siemen_per_m < lmois->low_ec_point) )
			{
				// 10/9/2018 ajv : Negate the threshold_crossed variable to set it to the new correct value.
				lmois->conductivity_threshold_crossed = !lmois->conductivity_threshold_crossed;

				if( lmois->low_ec_action == MOISTURE_SENSOR_LOW_EC_ACTION_alert_only ) 
				{
					Alert_soil_conductivity_crossed_threshold( lmois->decoder_serial_number, lmois->latest_conductivity_reading_deci_siemen_per_m, lmois->low_ec_point, lmois->high_ec_point, MOISTURE_SENSOR_THRESHOLD_CROSSED_LOWER ); 
				}
			}
			else if( (lmois->conductivity_threshold_crossed == (true)) && (lmois->latest_conductivity_reading_deci_siemen_per_m > lmois->low_ec_point) && (lmois->latest_conductivity_reading_deci_siemen_per_m < lmois->high_ec_point) )
			{
				// 10/9/2018 ajv : Negate the threshold_crossed variable to set it to the new correct value.
				lmois->conductivity_threshold_crossed = !lmois->conductivity_threshold_crossed;

				if( (lmois->high_ec_action == MOISTURE_SENSOR_HIGH_EC_ACTION_alert_only) || (lmois->low_ec_action == MOISTURE_SENSOR_LOW_EC_ACTION_alert_only) ) 
				{
					Alert_soil_conductivity_crossed_threshold( lmois->decoder_serial_number, lmois->latest_conductivity_reading_deci_siemen_per_m, lmois->low_ec_point, lmois->high_ec_point, MOISTURE_SENSOR_THRESHOLD_WITHIN_RANGE ); 
				}
			}
		}
		// ----------

		lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
	}

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
}


/* ---------------------------------------------------------- */
extern void MOISTURE_SENSOR_set_bits_on_all_moisture_sensors_to_cause_distribution_in_the_next_token( void )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	// ----------

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

	while( lmois != NULL )
	{
		// 5/1/2014 rmd : This function ONLY being called at the conclusion of a SCAN by the master
		// of a real multiple controller chain - we want to broadcast to the slaves using tokens
		// about all the hardware we presently know about. In this case the stations.
		SHARED_set_all_32_bit_change_bits( &lmois->changes_to_distribute_to_slaves, moisture_sensor_items_recursive_MUTEX );

		lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
	}

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	// ----------

	// 4/29/2014 ajv : Set a flag in the changes structure to notify this
	// controller to examine its lists for changes to distribute to the other
	// controllers in the chain in it's next token resposne. The flag is part of
	// the mutex protected comm_mngr struct so take the MUTEX.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	comm_mngr.changes.distribute_changes_to_slave = (true);

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void MOISTURE_SENSOR_on_all_moisture_sensors_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

	while( lmois != NULL )
	{
		if( pset_or_clear == COMMSERVER_CHANGE_BITS_ALL_CLEARED )
		{
			SHARED_clear_all_32_bit_change_bits( &lmois->changes_to_upload_to_comm_server, moisture_sensor_items_recursive_MUTEX );
		}
		else
		{
			SHARED_set_all_32_bit_change_bits( &lmois->changes_to_upload_to_comm_server, moisture_sensor_items_recursive_MUTEX );
		}

		lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
	}

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * @mutex_requirements Requires the caller to have the pending_changes_recursive_MUTEX to
 * ensure no other function tries to modify the pending_changes_to_send_to_comm_server flag.
 *
 * @task_execution Executed within the context of the CONTROLLER INITIATED task when it
 * first starts and when the waiting response flags are cleared after a communication.
 */
extern void nm_MOISTURE_SENSOR_update_pending_change_bits( const UNS_32 pcomm_error_occurred )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	BOOL_32	lfile_save_necessary;

	lfile_save_necessary = (false);

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

	while( lmois != NULL )
	{
		// 4/30/2015 ajv : No reason to process this if none of the bits are set. This also helps
		// reduce the file save activity.
		if( lmois->changes_uploaded_to_comm_server_awaiting_ACK != CHANGE_BITS_32_BIT_ALL_CLEAR )
		{
			// 4/29/2015 ajv : If a communication error occurred while sending Program Data, copy the
			// bits of variables that were sent back into the original bit field to ensure they're sent
			// again during the next attempt. Otherwise, clear the pending bits.
			if( pcomm_error_occurred )
			{
				lmois->changes_to_upload_to_comm_server |= lmois->changes_uploaded_to_comm_server_awaiting_ACK;

				// 4/30/2015 ajv : Since we've determined there were changes that were not successfully
				// sent, set the flag to trigger another attempt
				weather_preserves.pending_changes_to_send_to_comm_server = (true);

				// 4/30/2015 ajv : Also set the timer to force the data to send. If the timer's already
				// running, this will simply restart it.
				// 
				// 4/30/2015 ajv : BECAUSE of the potential that this function is executed many times (once
				// for each item changed) we could not start our pdata timer with a post to the CI queue.
				// The queue would overflow. So start the timer directly.
				xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGES_NOT_SENT_MS ), portMAX_DELAY );
			}
			else
			{
				SHARED_clear_all_32_bit_change_bits( &lmois->changes_uploaded_to_comm_server_awaiting_ACK, list_program_data_recursive_MUTEX );
			}

			// 4/30/2015 ajv : At least one system was updated. So make sure we save the file.
			lfile_save_necessary = (true);
		}

		lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
	}

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );

	if( lfile_save_necessary )
	{
		// 4/29/2015 ajv : Now that the bits are set or cleared accordingly, trigger a file save.
		// If power fails between building the message and receiving the ACK, the original bit field
		// hasn't been saved to flash yet, so it will resend the associated variables during the
		// next attempt.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_MOISTURE_SENSORS, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
	}
}

/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_set_new_reading_slave_side_variables( MOISTURE_SENSOR_DECODER_RESPONSE *presponse_ptr, MOISTURE_SENSOR_GROUP_STRUCT *lmois )
{
	if( (lmois != NULL) && nm_OnList( &moisture_sensor_group_list_hdr, lmois ) )
	{
		lmois->reading_string_storage__slave = *presponse_ptr;
		
		lmois->reading_string_send_to_master__slave = (true);
	}
}

/* ---------------------------------------------------------- */
extern UNS_8* MOISTURE_SENSOR_find_a_sensor_reading_to_send_to_the_master( UNS_32 *phow_many_bytes_ptr )
{
	// 2/25/2016 rmd : This function is called in the context of the COMM_MNGR task. There are
	// no MUTEX requirements placed upon the caller.
	
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	UNS_8	*rv;
	
	UNS_8	*ucp;
	
	// ----------
	
	rv = NULL;
	
	*phow_many_bytes_ptr = 0;
	
	// ----------
	
	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

	while( lmois != NULL )
	{
		if( lmois->reading_string_send_to_master__slave )
		{
			// 2/25/2016 rmd : First of all we are only sending one moisture sensor reading at a time to
			// the master. One at a time will be ample. After all, readings from a given decoder arrive
			// from the TPMicro once every 10 minutes. So they will make their way to the master in
			// plenty of time even sending one per token_resp. And this reduces the complexity of our
			// content in the token_resp. There is only one reading. What we do deliver is the decoder
			// serial number and the raw reading string.
			if( mem_obtain_a_block_if_available( (sizeof(UNS_32) + sizeof(MOISTURE_SENSOR_DECODER_RESPONSE)), (void**)&ucp ) )
			{
				rv = ucp;
				
				// ----------
				
				memcpy( ucp, &lmois->decoder_serial_number, sizeof(UNS_32) );
				
				ucp += sizeof(UNS_32);
				
				*phow_many_bytes_ptr += sizeof(UNS_32);
				

				memcpy( ucp, &lmois->reading_string_storage__slave, sizeof(MOISTURE_SENSOR_DECODER_RESPONSE) );
				
				*phow_many_bytes_ptr += sizeof(MOISTURE_SENSOR_DECODER_RESPONSE);
				
				// ----------
				
				// 2/26/2016 rmd : And set the flag false requesting the reading be sent to the master.
				lmois->reading_string_send_to_master__slave = (false);
				
				// ----------
				
				// 2/25/2016 rmd : DONE! All set to return indicating response ptr and length. And REMEMBER
				// we are only sending one reading at a time - so git outta here!
				break;
			}
			else
			{
				// 2/25/2016 rmd : There is NO else to the if. An error message was generated about this
				// attempt to obtain memory failing. Function returns as if nothing to send.
			}
		}

		lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
	}
	
	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/** 
 * Determine the soil type being used with the moisure sensor and returning the bulk 
 * density of that soil type.
 * 
 * @mutex There are no MUTEX requirements for the caller.
 *
 * @executed Executed within the context of the COMM_MNGR task while the token
 *  		 response is pulled apart.
 * 
 * @return Returns bulk denisty of soil as a float.
 * 
 * @param decoder_serial_number is the serial number related to the moisure sensor.
 */
static float MOISTURE_SENSOR_find_soil_bulk_density( const UNS_32 decoder_serial_number )
{
	STATION_GROUP_STRUCT	*ms_group;
	
	UNS_32	i;
	
	INT_32	soil_type;
	
	BOOL_32	soil_type_clash;
	
	float	soil_bulk_density;
	
	// ----------
		
	soil_type_clash = (false);
	
	soil_type = -1;
	
	soil_bulk_density = 1.0;
	
	// ----------
	
	// 10/15/2018 ajv : Take the Station Group mutex to ensure the list contents don't change as 
	// we're traversing through it. 
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	for( i = 0; i < STATION_GROUP_get_num_groups_in_use(); ++i )
	{
		ms_group = STATION_GROUP_get_group_at_this_index( i );
		
		if( STATION_GROUPS_if_the_moisture_decoder_is_available_return_decoder_serial_number_for_this_station_group( ms_group ) == decoder_serial_number )
		{
			if( soil_type == -1 )
			{
				soil_type = STATION_GROUP_get_soil_type( ms_group );
			}
			else
			{
				if( (soil_type != STATION_GROUP_get_soil_type(ms_group)) && !soil_type_clash )
				{
					Alert_Message( "Moisture Sensor assigned to different soil types" );

					soil_type_clash = (true);
				}
			}
		}
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------
					
	if( (soil_type == STATION_GROUP_SOIL_TYPE_CLAY) || (soil_type == STATION_GROUP_SOIL_TYPE_SILTY_CLAY) )
	{
		soil_bulk_density = 1.1;
	}
	else if( soil_type == STATION_GROUP_SOIL_TYPE_CLAY_LOAM )
	{
		soil_bulk_density = 1.25;
	}
	else if( (soil_type == STATION_GROUP_SOIL_TYPE_LOAM) || (soil_type == STATION_GROUP_SOIL_TYPE_SANDY_LOAM) )
	{
		soil_bulk_density = 1.4;
	}
	else if( (soil_type == STATION_GROUP_SOIL_TYPE_LOAMY_SAND) || (soil_type == STATION_GROUP_SOIL_TYPE_SAND) )
	{
		soil_bulk_density = 1.6;
	}
	
	return( soil_bulk_density );
}

/* ---------------------------------------------------------- */
/** Executes on the FOAL side extracting the moisture reading out of the token response. 
 *  Converts the moisture, temperature, and conductivity readings and makes them available
 *  for the master to control irrigation with them. Also creates a report record with the
 *  new readings. Which is transferred to the WEB.
 * 
 * @mutex There are no MUTEX requirements for the caller.
 *
 * @executed Executed within the context of the COMM_MNGR task while the token
 *  		 response is pulled apart.
 * 
 * @return Returns (true) if the moisture sensor decoder can be found in the file list of
 * moisture sensor decoders. Returns (false) if not which should be used to indicate faulty
 * or suspicious message content.
 * 
 * @param Upon exit pucp_ptr is to point at the next byte of message content beyond the
 * moisture reading content.
 */
extern BOOL_32 MOISTURE_SENSOR_extract_moisture_reading_from_token_response( UNS_8	**pucp_ptr,
																			 UNS_32	const pcontroller_index )
{
	// 8/22/2016 rmd : Right now moisture readings are taken once every two minutes. So for each
	// sensor in the network this function will execute once each 2 minutes. The 2 minute
	// reading rate is experimental and I'm confident will eventually be lowered to something
	// like once every 10 minutes.

	// ----------
	
	// 8/22/2016 rmd : To manage how many report records and alert lines are made.
	static UNS_32	record_count;
	
	// ----------

	BOOL_32	rv;
	
	UNS_32	ldecoder_serial_number;
	
	MOISTURE_SENSOR_DECODER_RESPONSE	lreading;
	
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	UNS_32	moisture_reading, conductivity_reading;

	INT_32	temperature_reading;
	
	UNS_8	*ucp;
	
	BOOL_32		readings_within_range;
	
	// ----------
	
	// 2/29/2016 rmd : DEFINES to avoid GCC floating point compiler bug.

	const volatile float ONE_HUNDRED_POINT_ZERO = 100.0;

	const volatile float FIFTY_POINT_ZERO = 50.0;

	const volatile float FIRST_TERM = 0.0000043;

	const volatile float SECOND_TERM = 0.00055;

	const volatile float THIRD_TERM = 0.0292;

	const volatile float FOURTH_TERM = 0.053;

	// ----------

	// 10/8/2018 ajv : Define to ensure we avoid the GCC floating-point compiler bug when 
	// converting from Celsius to Fahrenheit.
	const volatile float ONE_POINT_EIGHT = 1.8;

	// ----------
	
	// 10/5/2018 rmd : Not sure about the need for all these doubles. They are here, and Ryan
	// and Sara tested with them, so I'll leave them, but I betcha they aren't necessary, floats
	// would do just fine.

	double	mois_raw_double;
	
	float	mois_raw_float;
	
	double	mois_squared_double;
	
	double	mois_cubed_double;

	double	mois_final_double;
	
	float	mois_final_float;
	
	// ----------
	
	INT_32	temp_celsius;
	
	// ----------
	
	// 8/15/2018 Ryan : used in the calculation of the saturated electrical conductivity.

	const volatile float ONE_POINT_ZERO = 1.0;
	
	const volatile float EIGHTY_POINT_ZERO = 80.0;
				
	const volatile float TWO_POINT_SIX_FIVE = 2.65;
	
	const volatile float FOUR_POINT_ONE = 4.1;
				
	float	soil_bulk_density;
	
	// ----------
	
	rv = (true);
	
	// ----------
	
	memcpy( &ldecoder_serial_number, *pucp_ptr, sizeof(UNS_32) );

	*pucp_ptr += sizeof(UNS_32);
	

	memcpy( &lreading, *pucp_ptr, sizeof(MOISTURE_SENSOR_DECODER_RESPONSE) );

	*pucp_ptr += sizeof(MOISTURE_SENSOR_DECODER_RESPONSE);
	
	// ----------
	
	// 2/29/2016 rmd : Check if there was any trouble obtaining a sensor reading. This does not
	// mean there isn't a reading. What it means is during one of the reading attempts the logic
	// broke down and the expected reading didn't arrive or its checksum failed. REMEMBER the
	// moisture sensor decoder reads its sensor one per minute. But the TPMicro only gathers
	// that reading from the decoder once every 10 minutes. So we have 10 readings during which
	// one of the attempts could have gone bad. The status byte tracks that. Keep in mind this
	// doesn't mean we don't have a valid reading to work with.
	if( lreading.status & MOISTURE_DATA_STATUS_INVALID )
	{
		Alert_Message_va( "Moisture Sensor reading problem @ S/N %u", ldecoder_serial_number );
	}

	// ----------
	
	// 2/29/2016 rmd : Next check is if there is reading content to work with. That is reflected
	// in the lower 5 bits of the status byte. If any of the bits are set the reading is
	// guaranteed to be valid. That is the way the decoder forms the status byte.
	if( lreading.status & MOISTURE_STATUS_DATA_STRLEN_MASK )
	{
		xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );
	
		lmois = nm_MOISTURE_SENSOR_get_pointer_to_file_moisture_sensor_with_this_box_index_and_decoder_serial_number( pcontroller_index, ldecoder_serial_number );
	
		if( lmois )
		{
			// 2/25/2016 rmd : OKAY here comes the work to pull the reading apart! NOTE - the reading
			// starts right off with ascii chars that represent an integer moisture reading. Until the
			// first space. Then the conductivity integer - which can be just a single 0 if we do not
			// have the conductivity sensor wired to the decoder. Followed by the temperature integer.
			// Followed by a CR which indicates the end of the readings. Followed by a single character,
			// 'x' or 'z', which identifies the type of sensor.
			
			// ----------
			
			ucp = lreading.response_string;
			
			// ----------
			
			// 2/29/2016 rmd : Moisture Reading is first.
			moisture_reading = atoi( (char*)ucp );
			
			// 2/29/2016 rmd : Roll through the characters till we bump into the next space. Then move
			// one more to the next digit.
			while( *ucp != 0x20 ) ucp += 1;
			ucp += 1;
			
			// ----------
			
			conductivity_reading = atoi( (char*)ucp );
			
			// ----------
			
			// 2/29/2016 rmd : On to the temperature reading.
			while( *ucp != 0x20 )
			{
				ucp += 1;
			}
			ucp += 1;
			

			temperature_reading = atoi( (char*)ucp );

			// ----------
			
			// 2/29/2016 rmd : Move ahead to the CR which signals the end of the measurement string.
			while( *ucp != 0x0d )
			{
				ucp += 1;
			}
			ucp += 1;
			
			// 2/29/2016 rmd : Extract the sensor model type byte. An 'x' means model 5TM. A 'z' means
			// the model 5TE conductivity model. NOTE - assigning an UNS_8 to the UNS_32 only works
			// cause this is a LITTLE ENDIAN machine and we clear the variable out to all 0's first.
			lmois->detected_sensor_model = 0;
			lmois->detected_sensor_model = *ucp;
			
			// ----------
			
			// 2/29/2016 rmd : RANGE CHECKING - let's do some range checking.
			readings_within_range = (true);
			
			// 2/29/2016 rmd : I measured about 55 in raw air. And 3800 in a cup of water.
			//
			// 10/8/2018 rmd : I believe that when Sara did her EC work she complained my range tests
			// were too tight, so I'm relaxing. The moisture test was 40 and 4000. I'm making 30 and
			// 5000. My EC check was not to exceed 2000. I'm making 3000 now. 
			if( (moisture_reading < 30) || (moisture_reading > 5000) )
			{
				readings_within_range = (false);
			}
			
			if( lmois->detected_sensor_model == 'x' )
			{
				// 2/29/2016 rmd : Model 5TM with no conductivity measurement. Conductivity reading should
				// be 0.
				if( conductivity_reading )
				{
					readings_within_range = (false);
				}
			}
			else
			if( lmois->detected_sensor_model == 'z' )
			{
				// 2/29/2016 rmd : So we have a 5TE sensor that provides the conductivity measurement. The
				// range for EC according to some brief reading I did is 100uS/cm to 55000us/cm (fresh water
				// to salt water). So a very wide range. The RAW value we get is (dS/m * 100). We are
				// actually eventually converting to uS/cm EC which is (dS/m * 1000). Anyway doing the math
				// a RAW range of 0 through 2000 gives us a range of 0 through 72000 uS/cm which covers
				// fresh water through some pretty darn salty seawater. BY THE WAY - the Atlantic Ocean has
				// an EC of 43,000. The Great Salt Lake has an EC of 158,000. If our ground is beyond 72,000
				// I think the plants are in real trouble!!!
				//
				// 10/8/2018 rmd : I believe that when Sara did her EC work she complained my range tests
				// were too tight, so I'm relaxing. The moisture test was 40 and 4000. I'm making 30 and
				// 5000. My EC check was not to exceed 2000. I'm making 3000 now. 
				if( conductivity_reading > 3000 )
				{
					readings_within_range = (false);
				}
			}
			else
			{
				// 3/25/2016 rmd : UNKNOWN sensor model.
				readings_within_range = (false);
			}

			// 2/29/2016 rmd : Doing the math a Traw of 950 represents a temperature of 75 degrees C or
			// 167 degrees Fahrenheit. That should be plenty ample I'd say.
			if( temperature_reading > 950 )
			{
				readings_within_range = (false);
			}

			// ----------

			if( readings_within_range )
			{
				// 3/21/2016 rmd : CONVERT THE RAW MOISTURE READING TO A 0-100 VOLUME WATER CONTENT
				// PERCENTAGE SCALE.
				//
				// 3/15/2016 rmd : NOTE - for more details about the math refer to the DECAGON DEVICES 5TM /
				// 5TE user and integrators guides. There you will find the math and calibration
				// details. There is also a moisture reading conversion SPREADSHEET in the same directory as
				// the main.c file.

				// 2/29/2016 rmd : Calculate the VWC percentage. The raw result comes from the sensor 50
				// times up.
				mois_raw_double = ((double)moisture_reading / FIFTY_POINT_ZERO);

				// 10/5/2018 rmd : And capture a float representation of this value for use when converting
				// bulk EC to saturated EC.
				mois_raw_float = mois_raw_double;
				
				// ----------
				
				mois_cubed_double = (mois_raw_double * mois_raw_double * mois_raw_double);
				
				mois_squared_double = (mois_raw_double * mois_raw_double);
				
				mois_final_double = (FIRST_TERM * mois_cubed_double) - (SECOND_TERM * mois_squared_double) + (THIRD_TERM * mois_raw_double) - (FOURTH_TERM);

				// 10/5/2018 rmd : Capture a float representation of the final reading.
				mois_final_float = mois_final_double;

				// ----------
				
				// 10/5/2018 rmd : Capture the reading as the latest.
				lmois->latest_moisture_vwc_percentage = mois_final_float;
            
				// 3/15/2016 rmd : Get the system tick count as an easy way to stamp the reading so that
				// upon use the user can see how old the reading is. We don't tolerate more than 20 minutes
				// old. See this variable declaration for a more elaborate comment. ALSO indicate we finally
				// have a valid reading.
				lmois->latest_reading_time_stamp = xTaskGetTickCount();

				lmois->latest_reading_valid = (true);
				
				// ----------

				// 3/21/2016 rmd : CONVERT THE EC TO uS/cm.
				
				// 3/21/2016 rmd : AGAIN - for more details about the math refer to the DECAGON DEVICES 5TM
				// and 5TE user and integrators guides. There you will find the conductivity conversion
				// math.
				
				if( conductivity_reading > 700 )
				{
					// 3/21/2016 rmd : Greater than 700 first have to decompress.
					conductivity_reading = ( (5 * (conductivity_reading - 700) ) + 700 );
				}
				
				// 3/25/2016 rmd : They want us to divide the number by 100 to get dS/m. But we want uS/cm
				// as a more commonly used value (also means we don't need to support a fractional value)
				// and the relationship between dS/m and uS/cm is : dS/m * 1000 = uS/cm. The net result
				// being multiply the RAW decompressed value by 10 to get uS/cm.
				//
				// 7/31/2018 Sara : I found that dS/m are more commonly used when looking at EC of soil and 
                // almost all reference guides on EC are in these units.
				lmois->latest_conductivity_reading_deci_siemen_per_m = ( (float)conductivity_reading / ONE_HUNDRED_POINT_ZERO );
				
				// 8/15/2018 Ryan : Find the bulk density related to the assigned station group soil type.
				soil_bulk_density = MOISTURE_SENSOR_find_soil_bulk_density( lmois->decoder_serial_number );
				
				// 7/31/2018 Sara : Converting bulk EC to saturated EC because it is much more commonly used
				// in published data and more representative of the actual amount of salt that the plants
				// encounter. This equation is given in the Decagon soil EC beginners guide to measurements.
				lmois->latest_conductivity_reading_deci_siemen_per_m =	(   ( EIGHTY_POINT_ZERO * mois_final_float * lmois->latest_conductivity_reading_deci_siemen_per_m  ) /
																			( ( ONE_POINT_ZERO - (soil_bulk_density / TWO_POINT_SIX_FIVE) ) * (mois_raw_float - FOUR_POINT_ONE) )
																		);
                
				// ----------
				
				// 3/21/2016 rmd : CONVERT THE TEMPERATURE TO CELSIUS. AND THEN TO FAHRENHEIT.
				
				// 3/21/2016 rmd : AGAIN - for more details about the math refer to the DECAGON DEVICES 5TM
				// and 5TE user and integrators guides. There you will find the temperature conversion
				// math.
				
				if( temperature_reading > 900 )
				{
					// 3/21/2016 rmd : Greater than 900 first have to decompress.
					temperature_reading = ( (5 * (temperature_reading - 900) ) + 900 );
				}
				
				// 3/21/2016 rmd : Now convert to Celsius.
				temp_celsius = ( (temperature_reading - 400) / 10 );
				
				// 3/21/2016 rmd : And now finally convert to fahrenheit.
				lmois->latest_temperature_fahrenheit = ( (temp_celsius * ONE_POINT_EIGHT) + 32 );
				
                // ----------
			
				// 8/22/2016 rmd : And manage how many of the readings make their way into the alerts and
				// report data records. I think once every 20 minutes is enough information.
				//
				// 9/7/2016 rmd : Except for production who needs to see readings during decoder test.
				record_count += 1;
				
				#if BUILT_FOR_TWO_WIRE_DECODER_ASSEMBLY
					if( (record_count % 1) == 0 )
				#else
					if( (record_count % 10) == 0 )
				#endif
				{
					// 2/29/2016 rmd : And record this record.
					MOISTURE_SENSOR_RECORDER_add_a_record( lmois );
					
					// ----------
					
					// 3/21/2016 rmd : Temporary alert showing reading obtained.
					Alert_moisture_reading_obtained( lmois->latest_moisture_vwc_percentage, lmois->latest_temperature_fahrenheit, lmois->latest_conductivity_reading_deci_siemen_per_m );

					// ----------
					
					record_count = 0;
				}
				
			}
			else
			{
				Alert_moisture_reading_out_of_range( lmois->detected_sensor_model, moisture_reading, temperature_reading, conductivity_reading );
			}
			
		}
		else
		{
			Alert_Message( "MOISTURE: list item not found!" );
	
	
			// 2/25/2016 rmd : This should not happen. Signal content extract failure.
			rv = (false);
		}
	
		xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/17/2018 rmd : And now the irrigation support functions.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void MOISTURE_SENSORS_initialize__saved__irrigation_control_variables_for_this_moisture_sensor( UNS_32 const psensor_serial_number )
{
	// 5/10/2018 rmd : This function is called within the context of the TDCHECK task. It is
	// called at the START TIME when there is a station to irrigate. It is called over and over
	// again for each station in the station group - but that's okay, we only save the file once
	// due to the file save delay, so a small inefficiency to set the variables multiple times.

	// ----------
	
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;
	
	BOOL_32							file_save_needed;
	
	// ----------
	
	file_save_needed = (false);
	
	// ----------
	
	// 5/17/2018 rmd : If 0 serial number there is no sensor being used by this station. This is
	// the typical case, since using moisture sensors is an edge case - not popular.
	if( psensor_serial_number )
	{
		xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );
	
		lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );
	
		while( lmois != NULL )
		{
			if( lmois->decoder_serial_number == psensor_serial_number )
			{
				// 5/17/2018 rmd : Initialize both saved variables, regardless if the sensor is in use or
				// not. Doesn't hurt to initialize even if not in use. If not in use the other decision
				// making functions do not consider this sensor.
				
				// 5/17/2018 rmd : Crossed the set point is (false) at the start of irrgation when this
				// function is called.
				lmois->moisture_sensor_control__saved__has_crossed_the_set_point = (false);
		
				// 5/17/2018 rmd : Required cycles set to 0 initially to allow the first reading capture and
				// possibly an irrigation cycle to take place.
				lmois->moisture_sensor_control__saved__required_cycles_before_removal = 0;
				
				file_save_needed = (true);
				
				// ----------
				
				// 5/23/2018 rmd : Additionally at the START TIME start this protective count over again.
				lmois->invalid_or_old_reading_seconds = 0;
				
				// ----------
				
				// 5/17/2018 rmd : THIS IS AN ERROR CONDITION. We allow the user to assign the sensor to
				// more than one station group, BUT we require those station groups to have the same start
				// time. [I think this whole business about using a single moisture sensor to control
				// multiple station groups is tricky for the user, all the station groups should have
				// identical schedules]. ANYWAY, if the schedules are skewed we can detect some of that. If
				// a start time is hit while others are in the list actively using this sensor we can see
				// that and alert.
				if( lmois->moisture_sensor_control__transient__has_actively_irrigating_stations )
				{
					Alert_Message( "MOISTURE SENSOR: WARNING - sensor with multiple station groups and skewed start times!!" );
					
					// 5/17/2018 rmd : Is okay to clear this so we only get one alert for this station group
					// SINCE this variable is re-generated each time the maintain function runs.
					lmois->moisture_sensor_control__transient__has_actively_irrigating_stations = (false);
				}
			}
	
			// ----------
			
			lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
		}
	
		xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
	}
	
	// ----------
	
	if( file_save_needed )
	{
		// 5/17/2018 rmd : Save the file 2 seconds from now. Pushing out the file save to allow for
		// this function to repetitively be called at the start time, once for each starting station
		// which is using a moisture sensor.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_MOISTURE_SENSORS, 2 );
	}
}

/* ---------------------------------------------------------- */
void MOISTURE_SENSORS_initialize__transient__control_variables_for_all_sensors( void )
{
	// 5/17/2018 rmd : Called within the context of the TDCHECK task, at a one hertz rate each
	// time the maintain function executes. Called only once per second.
	
	// ----------
	
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

	while( lmois != NULL )
	{
		// 5/17/2018 rmd : This one initialized to (false) indicating this sensor is not being used
		// to control irrigation right this moment.
		lmois->moisture_sensor_control__transient__has_actively_irrigating_stations = (false);
		
		// 5/17/2018 rmd : Initialize both transient variables to (true). If one station in the ILC
		// irrigation list isn't satisfied, the variable(s) get set (false).
		lmois->moisture_sensor_control__transient__all_stations_are_off_and_soaking_completed = (true);

		lmois->moisture_sensor_control__transient__all_stations_have_reached_the_required_cycles = (true);

		// ----------
		
		lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
	}

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
	
	// 5/17/2018 rmd : NO FILE SAVE needed on the __transient__ variables. They are developed
	// fresh each second in the irrigation maintain function.
}

/* ---------------------------------------------------------- */
void MOISTURE_SENSORS_update__transient__control_variables_for_this_station( IRRIGATION_LIST_COMPONENT *pilc )
{
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	// ----------
	
	// 5/17/2018 rmd : If 0 serial number there is no sensor being used by this station. This is
	// the typical case, since using moisture sensors is an edge case - not popular.
	if( pilc->moisture_sensor_decoder_serial_number )
	{
		xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );
	
		lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );
	
		while( lmois != NULL )
		{
			if( lmois->decoder_serial_number == pilc->moisture_sensor_decoder_serial_number )
			{
				// 5/17/2018 rmd : Update the saved variables. I suppose don't care if the sensor is in _use
				// and physically available at this time. We'll test that as part of list removal criteria.
				
				// 5/17/2018 rmd : Well obviously this sensor is actively controlling irrigation.
				lmois->moisture_sensor_control__transient__has_actively_irrigating_stations = (true);

				if( pilc->bbf.station_is_ON || pilc->soak_seconds_remaining_ul )
				{
					// 5/17/2018 rmd : Well either he is ON or SOAKING. Update our flag.
					lmois->moisture_sensor_control__transient__all_stations_are_off_and_soaking_completed = (false);
				}
				
				// ----------
				
				// 3/31/2016 rmd : We are going to read from the station preserves so take this mutex.
				xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );
			
				// 5/17/2018 rmd : Test for < to. Allows for odd case if a station somehow gets ahead of the
				// required count, that does not trigger the variable to go (false). Shouldn't be needed but
				// seems safer to me.
				if( station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_number_of_repeats < lmois->moisture_sensor_control__saved__required_cycles_before_removal )
				{
					lmois->moisture_sensor_control__transient__all_stations_have_reached_the_required_cycles = (false);
				}

				xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
				
				// ----------
				
				// 5/17/2018 rmd : We found the sensor so end the loop. Any serial number should ONLY be in
				// the list ONCE.
				break;
			}
	
			// ----------
			
			lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
		}
	
		xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
	}
}

/* ---------------------------------------------------------- */
BOOL_32 MOISTURE_SENSORS_ilc_is_allowed_to_be_removed_from_the_list( IRRIGATION_LIST_COMPONENT const *const pilc )
{
	// 5/17/2018 rmd : Called in the context of TDCHECK. Called ONLY when the station is known
	// to be OFF and the station is in the list for PROGRAMMED_IRRIGATION. This function MUST
	// return (false) if the station does not have a moisture sensor decoder serial number, or
	// if using moisture sensing the moisture sensing removal criteria has not been met.
	
	// ----------
	
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	BOOL_32		rv;
	
	// ----------
	
	// 5/17/2018 rmd : Start out (false).
	rv = (false);
	
	// ----------
	
	// 5/17/2018 rmd : If 0 serial number there is no sensor being used by this station. This is
	// the typical case, since using moisture sensors is an edge case - not popular.
	if( pilc->moisture_sensor_decoder_serial_number )
	{
		xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );
	
		lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );
	
		while( lmois != NULL )
		{
			if( lmois->decoder_serial_number == pilc->moisture_sensor_decoder_serial_number )
			{
				// 5/17/2018 rmd : So test the list removal criteria. The station is already known to be OFF
				// so don't have to test that. We have to see that the sensor reading crossed its set point.
				// And that this station has the required number of cycles.
				if( lmois->moisture_sensor_control__saved__has_crossed_the_set_point )
				{
					// 3/31/2016 rmd : We are going to read from the station preserves so take this mutex.
					xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );
				
					// 5/17/2018 rmd : Test if the station has irrigated at least the required number of cycles.
					// It should actually be a match, but if the logic fails we'll allow list removal. Shouldn't
					// be needed but seems safer to me.
					if( station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_number_of_repeats >= lmois->moisture_sensor_control__saved__required_cycles_before_removal )
					{
						// 10/15/2018 ajv : TODO Before stoppping irrigation due to crossing a set point, it's 
						// important to verify that we're operating in a mode that allows irrigation to be stopped. 
						// This requires checking the whether 
						//  
						// 	  moisture_control_mode == MOISTURE_SENSOR_MOISTURE_CONTROL_MODE_stop_at_high_threshold
						//
						// This may not be the right place to do this, though, so research or confirm with Bob
						// before making the change.
						rv = (true);
					}
	
					xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
				}
				
				// ----------
				
				// 5/17/2018 rmd : We found the sensor so end the loop. Any serial number should ONLY be in
				// the list ONCE. End the while loop.
				break;
			}
	
			// ----------
			
			lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
		}
	
		xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
void MOISTURE_SENSORS_check_each_sensor_to_possibly_update__saved__control_variables( void )
{
	// 5/17/2018 rmd : Called within the context of the TDCHECK task, at a one hertz rate each
	// time the maintain function executes. Called only ONCE per second.
	
	// 5/17/2018 rmd : For each moisture sensor see if it is appropriate to take a reading, and
	// check if the set point has been crossed, and if not bump the required cycles.
	
	// ----------
	
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	BOOL_32							file_save_needed;
	
	// ----------
	
	file_save_needed = (false);

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

	while( lmois != NULL )
	{
		// 5/17/2018 rmd : Only need to do this to sensors with irrigating stations. This test
		// prevents incrementing the required count each second on 'unused' sensors, not that that
		// would hurt anything it just doesn't seem right to do.
		if( lmois->moisture_sensor_control__transient__has_actively_irrigating_stations )
		{
			if( lmois->moisture_sensor_control__transient__all_stations_are_off_and_soaking_completed && lmois->moisture_sensor_control__transient__all_stations_have_reached_the_required_cycles )
			{
				// 5/17/2018 rmd : So everybody is OFF and waiting, and BALANCED cycle wise. By the way,
				// this is the state just after the start time hits before anybody has turned on.
				
				// 4/1/2016 rmd : Now check if the reading is recent enough to use. Remember we are taking a
				// reading once every 2 minutes. That is plenty frequently enough to manage irrigation with.
				// Give some margin up and above the read frequency. Let's use one minute margin.
				if( lmois->latest_reading_valid && (((UNS_32)xTaskGetTickCount() - lmois->latest_reading_time_stamp) < (MS_to_TICKS( TPMICRO_MS_BETWEEN_GETTING_MOISTURE_READING_FROM_DECODERS + (60*1000)))) )
				{
					// 5/17/2018 rmd : Either way we need to save the file.
					file_save_needed = (true);
					
					// 4/1/2016 rmd : OKAY we have a reading less than 3 minutes old! Now check against the set
					// point to see if it is higher. And we should abandon the rest of irrigation.
					if( lmois->latest_moisture_vwc_percentage >= lmois->moisture_high_set_point_float )
					{
						// 5/17/2018 rmd : WELL, flag that we have crossed the SET POINT! Once set (true) this is
						// latched true. Only set (false) at the start time.
						lmois->moisture_sensor_control__saved__has_crossed_the_set_point = (true);
					}
					else
					{
						// 5/17/2018 rmd : And then this means we need to run another cycle, so bump the required
						// count.
						lmois->moisture_sensor_control__saved__required_cycles_before_removal += 1;
					}
				}
				else
				{
					lmois->invalid_or_old_reading_seconds += 1;
					
					// 5/23/2018 rmd : I allow 5 minutes of invalid or stale readings since the start time
					// before we effectively begin to ignore the moisture sensor. If this mechanism kicks in the
					// end result will be that all the requested time will be irrigated. And that's good!
					if( lmois->invalid_or_old_reading_seconds > (5*60) )
					{
						// 5/23/2018 rmd : Will cause another cycle to run. Eventually stations should be removed
						// due to running out of time.
						lmois->moisture_sensor_control__saved__required_cycles_before_removal += 1;
						
						Alert_Message_va( "MOISTURE SENSOR: questionable sensor override, sn %d, additional cycle irrigated", lmois->decoder_serial_number );
					}
				}
				
			}  // of all waiting

		}  // of sensor is actively managing stations trying to irrigate

		// ----------
		
		lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
	}

	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
	
	// ----------
	
	if( file_save_needed )
	{
		// 5/17/2018 rmd : Save the file rigth away. No need to wait. We've gone through the list.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_MOISTURE_SENSORS, 0 );
	}
}
	
/* ---------------------------------------------------------- */
BOOL_32 MOISTURE_SENSORS_this_ilc_has_irrigated_the_required_cycles( IRRIGATION_LIST_COMPONENT const * const pilc )
{
	// 5/17/2018 rmd : Called in the context of TDCHECK. Called during the hunt for one to turn
	// ON. This function must return false if moisture sensing not used for this station. Called
	// ONLY when the station is known to be OFF and the station is in the list for
	// PROGRAMMED_IRRIGATION. This function MUST return (false) if the station does not have a
	// moisture sensor decoder serial number.
	
	// ----------
	
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;

	BOOL_32		rv;
	
	// ----------
	
	// 5/17/2018 rmd : Start out (false).
	rv = (false);
	
	// ----------
	
	// 5/17/2018 rmd : If 0 serial number there is no sensor being used by this station. This is
	// the typical case, since using moisture sensors is an edge case - not popular.
	if( pilc->moisture_sensor_decoder_serial_number )
	{
		xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );
	
		lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );
	
		while( lmois != NULL )
		{
			if( lmois->decoder_serial_number == pilc->moisture_sensor_decoder_serial_number )
			{
				// 3/31/2016 rmd : We are going to read from the station preserves so take this mutex.
				xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );
			
				// 5/17/2018 rmd : If the station has irrigated the required cycles, return (true). It
				// should actually be a match, but if the logic fails and the station gets ahead of the
				// required cycles we'll keep functioning.
				if( station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_number_of_repeats >= lmois->moisture_sensor_control__saved__required_cycles_before_removal )
				{
					rv = (true);
				}

				xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
				
				// ----------
				
				// 5/17/2018 rmd : We found the sensor so end the loop. Any serial number should ONLY be in
				// the list ONCE. End the while loop.
				break;
			}
	
			// ----------
			
			lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
		}
	
		xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
BOOL_32 MOISTURE_SENSOR_calculate_chain_sync_crc( UNS_32 const pff_name )
{
	// 8/30/2018 rmd : Executed in the context of the comm_mngr task when token responses are
	// being made. And that's it.

	// ----------

	MOISTURE_SENSOR_GROUP_STRUCT	*lmois_ptr;
	
	UNS_8				*checksum_start;

	UNS_32				checksum_length;

	UNS_8				*ucp;
	
	BOOL_32				rv;

	// ----------

	rv = (false);

	// ----------
	
	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	// 8/30/2018 rmd : So the CRC calculation function wants a continuous memory space and a
	// length to calculate the crc off. In this function we will pick out the desired data, and
	// simply pile it up on itself, list item by list item. We don't care how messy the pile is
	// because we never attempt to reference any data once it is added to the pile. The data is
	// only there to include in the checksum.
	//
	// 8/30/2018 rmd : Well we know it can't be bigger than the list item count times the size
	// of each list item, so start with a memory block that big.
	if( mem_obtain_a_block_if_available( (moisture_sensor_group_list_hdr.count * sizeof(MOISTURE_SENSOR_GROUP_STRUCT)), (void**)&checksum_start ) )
	{
		// 9/5/2018 rmd : Well then the calculation will be successful. Nothing else can prevent
		// that outcome.
		rv = (true);
		
		// ----------
		
		ucp = checksum_start;
		
		checksum_length = 0;
		
		// ----------
		
		// 8/30/2018 rmd : Rattle though the list and push the fields we want to include in the sync
		// test onto the block to be checksummed.
		lmois_ptr = nm_ListGetFirst( &moisture_sensor_group_list_hdr );
		
		while( lmois_ptr != NULL )
		{
			// 9/4/2018 rmd : Make sure you include only TRUE USER SETTINGS, no flags, or pointers to
			// memory, etc...
			
			// ----------
			
			// 11/2/2018 rmd : Due to the inconsistent way the group names strings are handled during
			// the sync (we do not regligiously take all 48 characters and always carry them through the
			// sync process, there are places we only copy up until the null), the part of the string
			// following the null char is not guaranteed to be in sync. So only include up until the
			// null in the crc test.
			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmois_ptr->base.description, strlen(lmois_ptr->base.description) );

			// skipping the changes bit fields
			
			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmois_ptr->box_index_0, sizeof(lmois_ptr->box_index_0) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmois_ptr->decoder_serial_number, sizeof(lmois_ptr->decoder_serial_number) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmois_ptr->physically_available, sizeof(lmois_ptr->physically_available) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmois_ptr->in_use, sizeof(lmois_ptr->in_use) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmois_ptr->moisture_control_mode, sizeof(lmois_ptr->moisture_control_mode) );

			// skipping the two nlu set point variables
			
			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmois_ptr->low_temp_point, sizeof(lmois_ptr->low_temp_point) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmois_ptr->additional_soak_seconds, sizeof(lmois_ptr->additional_soak_seconds) );

			// skipping two variables (NOT SYNC'D)
			
			// skipping 5 'latest' variables (NOT SYNC'D)
			
			// skipping detected sensor model (NOT SYNC'D)

			// skipping invalid or old reading seconds variable (NOT SYNC'D)

			// skipping unused_b

			// skipping 5 'control' variables (NOT SYNC'D)
			
			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmois_ptr->moisture_high_set_point_float, sizeof(lmois_ptr->moisture_high_set_point_float) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmois_ptr->moisture_low_set_point_float, sizeof(lmois_ptr->moisture_low_set_point_float) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmois_ptr->high_temp_point, sizeof(lmois_ptr->high_temp_point) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmois_ptr->high_temp_action, sizeof(lmois_ptr->high_temp_action) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmois_ptr->low_temp_action, sizeof(lmois_ptr->low_temp_action) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmois_ptr->high_ec_point, sizeof(lmois_ptr->high_ec_point) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmois_ptr->low_ec_point, sizeof(lmois_ptr->low_ec_point) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmois_ptr->high_ec_action, sizeof(lmois_ptr->high_ec_action) );

			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &lmois_ptr->low_ec_action, sizeof(lmois_ptr->low_ec_action) );

			// END
			
			// ----------
		
			lmois_ptr = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois_ptr );
		}
		
		// ----------
		
		// 8/30/2018 rmd : Now calculate the checksum.
		cscs.the_crc[ pff_name ] = CRC_calculate_32bit_big_endian( checksum_start, checksum_length );
		
		// ----------
		
		// 9/4/2018 rmd : We're done with the block, so of course free it.
		mem_free( checksum_start );
	}
	else
	{
		// 9/5/2018 rmd : So by design it's okay if there is no memory. The checksum won't be set
		// valid, and the next time we try to include it in a token response we'll try to calculate
		// it again. Self protecting and retrying. But let's alert about this so we know it's taking
		// place.
		Alert_Message_va( "SYNC: no mem to calc checksum %s", chain_sync_file_pertinants[ pff_name ].file_name_string );
	}
	
	// ----------
	
	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/*
void MOISTURE_SENSORS_test_reading( void )
{
	// 5/24/2018 rmd : Function used during development test. Tied to a key on the main menu to
	// refresh and alter the sensor reading.
	
	// ----------
	
	MOISTURE_SENSOR_GROUP_STRUCT	*lmois;
	
	// ----------
	
	// 2/18/2016 rmd : Set the latest reading flag to false. The readings in the file are not
	// considered real until fresh readings have arrived. As readings arrive they are not saved
	// to the file.

	xSemaphoreTakeRecursive( moisture_sensor_items_recursive_MUTEX, portMAX_DELAY );

	lmois = nm_ListGetFirst( &moisture_sensor_group_list_hdr );

	while( lmois != NULL )
	{
		lmois->latest_reading_time_stamp = xTaskGetTickCount();
		
		lmois->latest_moisture_vwc_percentage += 0.5;
		

		Alert_Message_va( "Latest Reading is %5.2f", lmois->latest_moisture_vwc_percentage );


		lmois = nm_ListGetNext( &moisture_sensor_group_list_hdr, lmois );
	}
		
	xSemaphoreGiveRecursive( moisture_sensor_items_recursive_MUTEX );
}
*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

