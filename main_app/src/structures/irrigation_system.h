/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_IRRIGATION_SYSTEM_H
#define _INC_IRRIGATION_SYSTEM_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"cal_list.h"

#include	"group_base_file.h"

#include	"foal_defs.h"

#include	"pdata_changes.h"

// 8/26/2015 ajv : Required for SCHEDULE_OFF_TIME definition
#include	"station_groups.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define IRRIGATION_SYSTEM_USED_FOR_IRRI_DEFAULT				(true)

#define	IRRIGATION_SYSTEM_CAPACITY_IN_USE_DEFAULT			(false)

#define	IRRIGATION_SYSTEM_CAPACITY_MIN						(1)
#define	IRRIGATION_SYSTEM_CAPACITY_MAX						(MAXIMUM_SYSTEM_CAPACITY)
#define	IRRIGATION_SYSTEM_CAPACITY_PUMP_DEFAULT				(200)
#define	IRRIGATION_SYSTEM_CAPACITY_NONPUMP_DEFAULT			(200)

#define	IRRIGATION_SYSTEM_MLB_MIN							(1)
#define	IRRIGATION_SYSTEM_MLB_MAX							(MAXIMUM_SYSTEM_CAPACITY)
#define	IRRIGATION_SYSTEM_MLB_DURING_IRRI_DEFAULT			(400)
#define	IRRIGATION_SYSTEM_MLB_DURING_MVOR_DEFAULT			(150)
#define	IRRIGATION_SYSTEM_MLB_ALL_OTHER_DEFAULT				(150)

#define	IRRIGATION_SYSTEM_FLOW_CHECKING_IN_USE_DEFAULT		(false)

#define	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_MIN			(10)
#define	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_MAX			(MAXIMUM_SYSTEM_CAPACITY)

#define	IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_RANGES		(3)

static UNS_32 const IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS[ IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_RANGES ] =
{
	30,
	65,
	100
};

#define	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MIN			(1)
#define	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MAX			(200)

#define	IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_TOLERANCES	(IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_RANGES + 1)

static UNS_32 const IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS[ IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_TOLERANCES ] =
{
	5,
	10,
	10,
	15
};

#define IRRIGATION_SYSTEM_DERATE_ALLOWED_TO_LOCK_DEFAULT	(true)

// 6/22/2012 rmd : See notes associated with the DERATE_TABLE_CELLS define. Remember this is
// (max station minus ONE) times (the number of gpm slots) = number of derate cells.
#define	IRRIGATION_SYSTEM_DERATE_GPM_SLOT_SIZE_DEFAULT			(1)
#define	IRRIGATION_SYSTEM_DERATE_MAX_STATIONS_ON_DEFAULT		(10)
#define	IRRIGATION_SYSTEM_DERATE_NUMBER_OF_GPM_SLOTS_DEFAULT	(500)

// 5/31/2012 rmd : I just picked 1 and 8 as the min and max.
#define	IRRIGATION_SYSTEM_DERATE_STATION_CYCLES_MIN				(1)
#define	IRRIGATION_SYSTEM_DERATE_STATION_CYCLES_MAX				(8)
#define	IRRIGATION_SYSTEM_DERATE_STATION_CYCLES_DEFAULT			(6)

// 5/31/2012 rmd : I just picked 1 and 8 as the min and max.
#define	IRRIGATION_SYSTEM_DERATE_CELL_ITERATIONS_MIN			(1)
#define	IRRIGATION_SYSTEM_DERATE_CELL_ITERATIONS_MAX			(8)
#define	IRRIGATION_SYSTEM_DERATE_CELL_ITERATIONS_DEFAULT		(6)

// ----------

#define	MVOR_ACTION_OPEN_MASTER_VALVE							(0)
#define	MVOR_ACTION_CLOSE_MASTER_VALVE							(1)
#define	MVOR_ACTION_CANCEL_MASTER_VALVE_OVERRIDE				(2)

// ----------

#define	IRRIGATION_SYSTEM_MVOR_OPEN_TIME_MIN					(0)
#define	IRRIGATION_SYSTEM_MVOR_OPEN_TIME_MAX					(SCHEDULE_OFF_TIME)
#define	IRRIGATION_SYSTEM_MVOR_OPEN_TIME_DEFAULT				(SCHEDULE_OFF_TIME)

#define	IRRIGATION_SYSTEM_MVOR_CLOSE_TIME_MIN					(0)
#define	IRRIGATION_SYSTEM_MVOR_CLOSE_TIME_MAX					(SCHEDULE_OFF_TIME)
#define	IRRIGATION_SYSTEM_MVOR_CLOSE_TIME_DEFAULT				(SCHEDULE_OFF_TIME)

// ----------

#define	IRRIGATION_SYSTEM_BUDGET_IN_USE_NO						(0)
#define	IRRIGATION_SYSTEM_BUDGET_IN_USE_YES						(1)

#define IRRIGATION_SYSTEM_BUDGET_MODE_ALERT_ONLY				(0)
#define	IRRIGATION_SYSTEM_BUDGET_MODE_AUTOMATIC					(1)
// 02/10/2016 skc : These options don't make the initial release
//#define IRRIGATION_SYSTEM_BUDGET_MODE_manual_reduction		(2) 
//#define IRRIGATION_SYSTEM_BUDGET_MODE_CHOICE_stop_at_budget	(3)
// 
#define	IRRIGATION_SYSTEM_BUDGET_MODE_MIN (IRRIGATION_SYSTEM_BUDGET_MODE_ALERT_ONLY) 
#define IRRIGATION_SYSTEM_BUDGET_MODE_MAX (IRRIGATION_SYSTEM_BUDGET_MODE_AUTOMATIC) 
#define IRRIGATION_SYSTEM_BUDGET_MODE_DEFAULT (IRRIGATION_SYSTEM_BUDGET_MODE_ALERT_ONLY) 

// ----------

#define	IRRIGATION_SYSTEM_BUDGET_METER_READ_TIME_MIN			(TIME_MINIMUM)
#define	IRRIGATION_SYSTEM_BUDGET_METER_READ_TIME_MAX			(TIME_MAXIMUM)
// 9/14/2015 rmd : Meter read default is 12 noon.
#define	IRRIGATION_SYSTEM_BUDGET_METER_READ_TIME_DEFAULT		(TIME_NOON)

// ----------

// 01/21/2016 skc : The concept of "# annual periods" will probably go away
// We only care about the number of meter read dates
#define	IRRIGATION_SYSTEM_BUDGET_ANNUAL_PERIODS_MIN			(6)
#define	IRRIGATION_SYSTEM_BUDGET_ANNUAL_PERIODS_MAX			(MONTHS_IN_A_YEAR)
#define	IRRIGATION_SYSTEM_BUDGET_ANNUAL_PERIODS_DEFAULT		(MONTHS_IN_A_YEAR)
#define IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS				(24)

// ----------

// 7/14/2016 skc : Fogbugz #3281
#define	IRRIGATION_SYSTEM_BUDGET_PERIOD_DATE_MIN			(DATE_MINIMUM)

// 3/25/2016 skc : Define maximum budget period width, depends on 12 or 6 budgets per year
#define	IRRIGATION_SYSTEM_BUDGET_MAXIMUM_PERIOD_12_DAYS		(35)
#define	IRRIGATION_SYSTEM_BUDGET_MAXIMUM_PERIOD_6_DAYS		(68)

#define	IRRIGATION_SYSTEM_BUDGET_PERIOD_DATE_MAX			(DATE_MAXIMUM)

#define	IRRIGATION_SYSTEM_BUDGET_PERIOD_DATE_DEFAULT		(IRRIGATION_SYSTEM_BUDGET_PERIOD_DATE_MIN)

// ----------

#define	IRRIGATION_SYSTEM_BUDGET_HALT_IRRI_DEFAULT			(false)

// 4/19/2016 skc : We want to use all 32 bits in the bitfield.
#define IRRIGATION_SYSTEM_BUDGET_FLOW_TYPE_MIN				(INT_32_MIN)
#define IRRIGATION_SYSTEM_BUDGET_FLOW_TYPE_MAX				(INT_32_MAX)
#define IRRIGATION_SYSTEM_BUDGET_FLOW_TYPE_DEFAULT			(FLOW_TYPE_DEFAULT)

// ----------

// 9/22/2015 ajv : All mainlines are "in use" until deleted by the user.
#define IRRIGATION_SYSTEM_IN_USE_DEFAULT					(true)

// ---------- 
 
// 7/11/2018 ajv : In irrigation, we historically tally the flow rate of all POCs associated 
// with a mainline. This is because POCs are traditionally installed in parallel with one 
// another. This flag maintains that behavior and is the default. 
#define	IRRIGATION_SYSTEM_FLOW_ACCUMULATOR_ACTION_tally_POC_flows	(0)

// 7/11/2018 ajv : In other applications, such as aquaponics or hydroponics, flow sensors 
// may be installed in serial, where tallying is less valuable than comparing the flow rates 
// to identify anomalies between flow sensors. 
#define	IRRIGATION_SYSTEM_FLOW_ACCUMULATOR_ACTION_compare_POC_flows	(1)

#define	IRRIGATION_SYSTEM_FLOW_ACCUMULATOR_ACTION_MIN		(IRRIGATION_SYSTEM_FLOW_ACCUMULATOR_ACTION_tally_POC_flows)
#define	IRRIGATION_SYSTEM_FLOW_ACCUMULATOR_ACTION_MAX		(IRRIGATION_SYSTEM_FLOW_ACCUMULATOR_ACTION_compare_POC_flows)
#define	IRRIGATION_SYSTEM_FLOW_ACCUMULATOR_ACTION_DEFAULT	(IRRIGATION_SYSTEM_FLOW_ACCUMULATOR_ACTION_tally_POC_flows)

// ----------

// 10/24/2018 rmd : AJ added these, as support for a future functionality. NOT YET USED.
#define IRRIGATION_SYSTEM_FLOW_CHECK_DURING_MVOR_DEFAULT	(false)
#define IRRIGATION_SYSTEM_ACTIVATE_PUMP_DURING_MVOR_DEFAULT	(false)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2011.11.08 rmd : This define sets the size of the battery backed by system structure.
// Which includes the system report data line in process. AJ and I agreed this would be an
// ample number of systems within a network. This define should also be used in the making
// of system groups to limit the current number to 8 and explain to the user the you cannot
// make more.
// 5/25/2012 rmd : Guess what. System include the derate table and we want to include them
// within system_preserves. And this max number of systems at EIGHT has been bugging me for
// a while. It's just too many. And we need the battery backed SRAM. So we are lowering it
// to a max of FOUR. Some small percentage of users will need TWO. And of those some number
// will want a water feature or a main line monitoring. So that would consume TWO more for a
// total of FOUR in the extreme case.
//
// 2/11/2016 rmd : UPDATE - this is not the maximum number of systems in the system list.
// The list holds systems in_use as well as not in use (DELETED). We do not actually delete
// systems not in use we mark them as not in use to indicate deleted.
#define		MAX_POSSIBLE_SYSTEMS	(4)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct IRRIGATION_SYSTEM_GROUP_STRUCT IRRIGATION_SYSTEM_GROUP_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern const char IRRIGATION_SYSTEM_DEFAULT_NAME[];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_file_irrigation_system( void );

extern void save_file_irrigation_system( void );



extern void *nm_SYSTEM_create_new_group( const UNS_32 pchanges_received_from );




extern UNS_32 SYSTEM_build_data_to_send( UNS_8 **pucp,
										 const UNS_32 pmem_used_so_far,
										 BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
										 const UNS_32 preason_data_is_being_built,
										 const UNS_32 pallocated_memory );

extern void nm_SYSTEM_set_budget_period(	void *const psystem,
											const UNS_32 pperiod_index0,
											UNS_32 pstart_date,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0,
											const BOOL_32 pset_change_bits,
											CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											#ifdef _MSC_VER
											, char *pmerge_update_str,
											char *pmerge_insert_fields_str,
											char *pmerge_insert_values_str
											#endif
										  );

extern UNS_32 nm_SYSTEM_extract_and_store_changes_from_comm( const unsigned char *pucp,
															 const UNS_32 preason_for_change,
															 const BOOL_32 pset_change_bits,
															 const UNS_32 pchanges_received_from
															 #ifdef _MSC_VER
															 , char *pSQL_statements,
															 char *pSQL_search_condition_str,
															 char *pSQL_update_str,
															 char *pSQL_insert_fields_str,
															 char *pSQL_insert_values_str,
															 char *pSQL_single_merge_statement_buf,
															 const UNS_32 pnetwork_ID
															 #endif
														   );

// ----------

#ifdef _MSC_VER

extern INT_32 SYSTEM_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order );

#endif

// ----------

extern void SYSTEM_copy_group_into_guivars( const UNS_32 psystem_index_0 );

extern void MAINLINE_BREAK_copy_group_into_guivars( void );

extern void SYSTEM_CAPACITY_copy_group_into_guivars( void );

extern void FLOW_CHECKING_copy_group_into_guivars( const UNS_32 psystem_index_0 );

extern void MVOR_copy_group_into_guivars( const UNS_32 psystem_index_0 );

extern void BUDGET_SETUP_copy_group_into_guivars( const UNS_32 psystem_index_0 );

extern void BUDGET_FLOW_TYPES_copy_group_into_guivars( const UNS_32 psystem_index_0 );



extern void SYSTEM_extract_and_store_group_name_from_GuiVars( void );

extern void SYSTEM_extract_and_store_changes_from_GuiVars( void );

extern void MAINLINE_BREAK_extract_and_store_changes_from_GuiVars( void );

extern void SYSTEM_CAPACITY_extract_and_store_changes_from_GuiVars( void );

extern void FLOW_CHECKING_extract_and_store_changes_from_GuiVars( void );

extern void MVOR_extract_and_store_changes_from_GuiVars( void );

extern void BUDGET_SETUP_extract_and_store_changes_from_GuiVars( void );

extern void BUDGET_FLOW_TYPES_extract_and_store_changes_from_GuiVars( void );

extern void nm_SYSTEM_copy_record_in_progress_into_guivars( const void *const psystem );

extern void nm_SYSTEM_copy_report_data_into_guivars( const void *const psystem, const UNS_32 pindex_0 );



extern void SYSTEM_load_system_name_into_scroll_box_guivar( const INT_16 pindex_0_i16 );



extern UNS_32 SYSTEM_get_inc_by_for_MLB_and_capacity( const UNS_32 prepeats );

// ----------

extern void *SYSTEM_get_group_with_this_GID( const UNS_32 pgroup_ID );

extern void *SYSTEM_get_group_at_this_index( const UNS_32 pindex_0 );

extern UNS_32 SYSTEM_get_index_for_group_with_this_GID( const UNS_32 psystem_ID );

extern UNS_32 SYSTEM_get_last_group_ID( void );

extern UNS_32 SYSTEM_get_list_count( void );

extern UNS_32 SYSTEM_num_systems_in_use( void );

// ---------------------------

extern BOOL_32 SYSTEM_get_used_for_irri_bool( UNS_32 const psystem_gid );


extern UNS_32 SYSTEM_get_mlb_during_irri_gpm( const UNS_32 psystem_gid );

extern UNS_32 SYSTEM_get_mlb_during_mvor_opened_gpm( const UNS_32 psystem_gid );

extern UNS_32 SYSTEM_get_mlb_during_all_other_times_gpm( const UNS_32 psystem_gid );


extern BOOL_32 SYSTEM_get_capacity_in_use_bool( const UNS_32 psystem_gid );

extern UNS_32 SYSTEM_get_capacity_with_pump_gpm( const UNS_32 psystem_gid );

extern UNS_32 SYSTEM_get_capacity_without_pump_gpm( const UNS_32 psystem_gid );


extern BOOL_32 SYSTEM_get_flow_checking_in_use( const UNS_32 psystem_gid );


extern UNS_32 SYSTEM_get_required_station_cycles_before_flow_checking( const UNS_32 psystem_gid );

extern UNS_32 SYSTEM_get_required_cell_iterations_before_flow_checking( const UNS_32 psystem_gid );

extern BOOL_32 SYSTEM_get_allow_table_to_lock( const UNS_32 psystem_gid );

extern UNS_32 SYSTEM_get_derate_table_gpm_slot_size( const UNS_32 psystem_gid );

extern UNS_32 SYSTEM_get_derate_table_max_stations_ON( const UNS_32 psystem_gid );

extern UNS_32 SYSTEM_get_derate_table_number_of_gpm_slots( const UNS_32 psystem_gid );

// ---------------------------

extern void SYSTEM_get_flow_check_ranges_gpm( const UNS_32 psystem_gid, UNS_32 *parray );

extern void SYSTEM_get_flow_check_tolerances_plus_gpm( const UNS_32 psystem_gid, UNS_32 *parray );

extern void SYSTEM_get_flow_check_tolerances_minus_gpm( const UNS_32 psystem_gid, UNS_32 *parray );

// ---------------------------

extern CHANGE_BITS_32_BIT *SYSTEM_get_change_bits_ptr( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem, const UNS_32 pchange_reason );

// ---------------------------

extern void IRRIGATION_SYSTEM_set_bits_on_all_systems_to_cause_distribution_in_the_next_token( void );

extern void SYSTEM_clean_house_processing( void );

extern void SYSTEM_on_all_systems_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear );

extern void nm_SYSTEM_update_pending_change_bits( const UNS_32 pcomm_error_occurred );

// ---------------------------

extern void FDTO_SYSTEM_load_system_name_into_guivar( const UNS_32 pindex_0 );

// ---------------------------

extern BOOL_32 SYSTEM_at_least_one_system_has_flow_checking( void );

extern UNS_32 SYSTEM_get_flow_checking_status( const UNS_32 pindex_0 );

extern UNS_32 SYSTEM_get_highest_flow_checking_status_for_any_system( void );

extern void SYSTEM_clear_mainline_break( const UNS_32 psystem_GID );

extern void SYSTEM_check_for_mvor_schedule_start( const DATE_TIME_COMPLETE_STRUCT *const pdtcs_ptr );

// ----------

extern BOOL_32 SYSTEM_this_system_is_in_use( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem_ptr );

// ----------

typedef struct
{
	BOOL_32 in_use;
	UNS_32	meter_read_time;
	UNS_32	number_of_annual_periods;
	UNS_32	meter_read_date[IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS];
	UNS_32	mode;
} BUDGET_DETAILS_STRUCT;

extern BOOL_32 SYSTEM_get_budget_in_use( const UNS_32 psystem_gid );
extern void SYSTEM_get_budget_details( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem_ptr, BUDGET_DETAILS_STRUCT *pbds_ptr );
extern UNS_32 SYSTEM_get_budget_period_index( BUDGET_DETAILS_STRUCT* pbds_ptr, DATE_TIME dt );
extern BOOL_32 SYSTEM_at_least_one_system_has_budget_enabled( void );
extern UNS_32 nm_SYSTEM_get_budget( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem_ptr, UNS_32 idx_budget );
extern UNS_32 nm_SYSTEM_get_used( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem_ptr );
extern BOOL_32 nm_SYSTEM_get_budget_flow_type( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem_ptr, UNS_32 idx );

extern float nm_SYSTEM_get_Vp( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem_ptr );
extern void nm_SYSTEM_set_Vp( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem_ptr, float pVp );
extern float nm_SYSTEM_get_poc_ratio( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem_ptr );
extern void nm_SYSTEM_set_poc_ratio(IRRIGATION_SYSTEM_GROUP_STRUCT *psystem_ptr, float pratio );

// ----------

extern void IRRIGATION_SYSTEM_load_ftimes_list( void );

// ----------

extern BOOL_32 IRRIGATION_SYSTEM_calculate_chain_sync_crc( UNS_32 const pff_name );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

