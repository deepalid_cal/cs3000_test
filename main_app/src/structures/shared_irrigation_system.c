/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_SHARED_IRRIGATION_SYSTEM_C
#define _INC_SHARED_IRRIGATION_SYSTEM_C

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"group_base_file.h"

#include	"cal_td_utils.h"

#include	"change.h"

#ifndef	_MSC_VER

	#include	"alerts.h"

	#include	"app_startup.h"
	
	#include	"cs_mem.h"

	#include	"battery_backed_vars.h"

#else

	#include	<stdlib.h>

	#include	"sql_merge.h"

#endif




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_description_bit						(0)

#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_used_with_irri_bit					(1)

#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_in_use_bit					(2)
#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_with_pump_bit				(3)
#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_without_pump_bit				(4)

#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_during_irri_bit					(5)
#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_during_mvor_bit					(6)
#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_all_other_times_bit				(7)

#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_in_use_bit				(8)
#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_ranges_bit				(9)
#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_tolerances_plus_bit		(10)
#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_tolerances_minus_bit	(11)

#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_mvor_open_times_bit					(12)
#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_mvor_close_times_bit					(13)

#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_in_use_bit						(14)
#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_meter_read_time_bit			(15)
#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_annual_periods_bit				(16)
#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_meter_read_date_bit			(17)
#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_mode_bit						(18)

#define	IRRIGATION_SYSTEM_CHANGE_BITFIELD_in_use_bit							(19)

#define IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_flow_type_bit					(20)

// 4/12/2016 skc : Always keep this as the last number in actual use
#define IRRIGATION_SYSTEM_CHANGE_BITFIELD_last_number							(20)

// ----------

static char* const SYSTEM_database_field_names[ (IRRIGATION_SYSTEM_CHANGE_BITFIELD_last_number + 1) ] =
{
	"SystemName",

	"UsedForIrrig",

	"CapacityInUse",
	"CapacityWithPump",
	"CapacityWithoutPump",

	"MLBDuringIrrig",
	"MLBDuringMVOR",
	"MLBAllOtherTimes",

	"FlowCheckingInUse",
	"FlowCheckingRange",	// FlowCheckingRangeX
	"FlowTolerancePlus",	// FlowTolerancePlusX
	"FlowToleranceMinus",	// FlowToleranceMinusX

	"MVORScheduleOpen",		// MVORScheduleOpen0 .. 6
	"MVORScheduleClose",	// MVORScheduleClose0 .. 6
	
	"BudgetInUse",
	"BudgetMeterReadTime",
	"BudgetAnnualPeriods",
	"BudgetMeterReadDate",	// 1..24
	"BudgetMode",

	"InUse",

	"BudgetFlowType"		// ..Irrigation, Program, ManualProgram, NA3, Manual, WalkThru, Test, Mobile, MVOR, NonController
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * The definition of the group's structure. If the contents of this structure
 * are changed or reordered, you <b>MUST</b> update the 
 * SYSTEM_LATEST_FILE_VERSION located in this structure's header file. 
 * 
 * @revisions
 *    2/8/2012  Updated variables to be 32-bit unsigned and BOOL_32
 *    4/3/2012  Added change bits to track whether changes have been distributed
 */
struct IRRIGATION_SYSTEM_GROUP_STRUCT
{
	GROUP_BASE_DEFINITION_STRUCT	base;

	// 3/8/2016 rmd : This variable is presently unused. It's original intention was along the
	// lines of setting up a Mainline with POC to be used to monitor only for mainline break. Or
	// simply to accumulate consumption. However all that can be done without this variable.
	// Furthermore the station groups control if a mainline is used for irrigation by their
	// attachment to the mainline.
	BOOL_32							used_for_irrigation_bool;

	BOOL_32							capacity_in_use_bool;
	UNS_32							capacity_with_pump_gpm;
	UNS_32							capacity_without_pump_gpm;

	UNS_32							mlb_during_irri_gpm;
	UNS_32							mlb_during_mvor_gpm;
	UNS_32							mlb_all_other_times_gpm;

	BOOL_32							flow_checking_in_use_bool;
	UNS_32							flow_checking_ranges_gpm[ IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_RANGES ];

	UNS_32							flow_checking_tolerances_plus_gpm[ IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_TOLERANCES ];
	UNS_32							flow_checking_tolerances_minus_gpm[ IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_TOLERANCES ];


	/* ------------------------------------------------------- *
	 *                      Change Bits
	 * ------------------------------------------------------- */

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

	/* ------------------------------------------------------- *
	 *                  Derate Table Information
	 * ------------------------------------------------------- */
	
	// 5/25/2012 rmd : The system derate tables (one per system) are stored within the
	// battery_backed system_preserves.

	BOOL_32		derate_table_allowed_to_lock;
	
	// The derate table is 3200 floats. And the defaults are 1 gpm slot size, 8 station ON, and
	// 400 gpm max.
	UNS_32		derate_table_gpm_slot_size;				// default of 1

	UNS_32		derate_table_max_stations_ON;			// default of 8

	UNS_32		derate_table_number_of_gpm_slots;		// default of 400


	// Variables that control derate table learning prior to flow checking. When these
	// requirements are meet actual flow checking begins taking place. And if locking is allowed
	// (the default) then when these criteria are meet the cell is locked.
	UNS_32		derate_table_before_checking_number_of_station_cycles;		// default of 6

	UNS_32		derate_table_before_checking_number_of_cell_iterations;		// default of 6
    
	// ----------
	
	// 4/30/2015 ajv : Added in Rev 2.

	// ----------
	
	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;

	// ----------
	
	// 9/21/2015 rmd : Added in Rev 3 and REDEFINED as two separate arrays (instead of a single
	// array of MVOR_SCHEDULE structures) in Rev 5.

	// ----------
	
	UNS_32				mvor_open_times[ DAYS_IN_A_WEEK ];

	UNS_32				mvor_close_times[ DAYS_IN_A_WEEK ];

	// ----------
	
	// 9/9/2015 rmd : Added in Rev 4.

	// ----------
	
	// 12/14/2015 rmd : Budget variables.

	// 03/22/2016 skc : Alert Only, Automatic Reduction, Manual Reduction, Stop at Budget
	UNS_32		budget_mode;
	
	// 11/4/2015 rmd : This time defaults to NOON and is NOT currently available for the user to
	// edit.
	UNS_32		budget_meter_read_time;
	
	// ----------
	
	// 3/24/2016 skc : This will be either 12 or 6
	UNS_32		budget_number_of_annual_periods;

	// 3/24/2016 skc : We decided to only support a maximum of 12 budget periods for display purposes. 
	// Thus, we have some extra room at the end of this array.
	UNS_32		budget_meter_read_date[ IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS ];

	// 3/22/2016 skc : Is budget control On or Off?
	BOOL_32		budget_in_use;
	
	// ----------
	
	// 3/7/2016 rmd : Rev 5 redefined part of the MVOR variables.
	
	// ----------
	
	// 9/22/2015 ajv : Added in Rev 6.
	// 
	// By default, all mainline are "in use". If a user deletes a mainline, this
	// flag is set false. Although this keeps the item in memory and in the file, this is
	// necessary so the change can propagate through the chain and to the CommServer. If this
	// flag is FALSE, it will be hidden from view and completely inaccessible to the user.
	BOOL_32		in_use;


	// 02/25/2016 skc : Added in Rev 8. These are strictly temporary local variables, don't need
	// to be preserved, no bit fields, no comms action either. Intended for master controller
	// use only. No wrapper functions are needed because no validation is required.
	// 
	// Each mainline has an associated predicted volume for a budget period.
	float 		volume_predicted;

	// Each mainline also has a "weakest link" POC ratio used in calculation of irrigation time.
	// We find value of the POC with the smallest ratio and use in budget calculations.
	float 		poc_ratio;

	// 4/19/2016 skc : Added in Rev 8. 
	// 
	// 4/19/2016 skc : The MVOR and Non-controller usage can optionally be not included in
	// the budget accumulations. Each byte allows the user to specify what types of flow
	// through a POC should be counted against the budget. The values default to the
	// equivalent of FLOW_TYPE_DEFAULT(0xC0F6), set values to 0 to stop including that
	// type of flow to the sum. The indices come from the IN_LIST_FOR_XXX and
	// FLOW_TYPE_XXX values found in foal_defs.h.
	BOOL_32 	budget_flow_type[ FLOW_TYPE_NUM_TYPES ];

	// ---------- 
	
	#if 0

	// Added in Rev 9
	 
	// 7/11/2018 ajv : 
	UNS_32		flow_accumulator_action;

	// 7/11/2018 ajv : 
	BOOL_32		flow_check_during_MVOR;
	
	// 7/11/2018 ajv : 
	BOOL_32		activate_pump_during_MVOR;

	#endif
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Sets whether a particular irrigation system is used for irrigation or not. If
 * the passed in group is NULL, an error is raised and a 0 is returned.
 * Additionally, if the parameter that's passed in is out-of-range, an error is
 * raised and the value isn't updated.
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 *  	  ensure the group whose value is being changed isn't deleted during
 *  	  this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param psystem A pointer to the group to update.
 * @param pused_for_irrigation_bool True if the group will be used for
 *                                  irrigation; otherwise, false.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 8/10/2011 Adrianusv
 *
 * @revisions
 *    8/10/2011 Initial release
 *    4/4/2012  Added setting of change bits to distribute changes
 */
static void nm_SYSTEM_set_system_used_for_irri( void *const psystem,
												BOOL_32 pused_for_irrigation_bool,
												const BOOL_32 pgenerate_change_line_bool,
												const UNS_32 preason_for_change,
												const UNS_32 pbox_index_0,
												const BOOL_32 pset_change_bits,
												CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												#ifdef _MSC_VER
												, char *pmerge_update_str,
												char *pmerge_insert_fields_str,
												char *pmerge_insert_values_str
												#endif
											  )
{
	SHARED_set_bool_with_32_bit_change_bits_group( psystem,
												   &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->used_for_irrigation_bool),
												   pused_for_irrigation_bool,
												   IRRIGATION_SYSTEM_USED_FOR_IRRI_DEFAULT,
												   pgenerate_change_line_bool,
												   CHANGE_IRRIGATION_SYSTEM_USED_FOR_IRRI,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->changes_to_upload_to_comm_server),
												   IRRIGATION_SYSTEM_CHANGE_BITFIELD_used_with_irri_bit,
												   SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_used_with_irri_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
/**
 * Sets whether a particular irrigation schedule uses system capacity or not. If
 * the passed in group is NULL, an error is raised and a 0 is returned.
 * Additionally, if the parameter that's passed in is out-of-range, an error is
 * raised and the value isn't updated.
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 *  	  ensure the group whose value is being changed isn't deleted during
 *  	  this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param psystem A pointer to the group to update.
 * @param pcapacity_in_use_bool True if the group will use system capacity;
 *                              otherwise, false.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 8/10/2011 Adrianusv
 *
 * @revisions
 *    8/10/2011 Initial release
 *    4/4/2012  Added setting of change bits to distribute changes
 */
static void nm_SYSTEM_set_capacity_in_use( void *const psystem,
										   BOOL_32 pcapacity_in_use_bool,
										   const BOOL_32 pgenerate_change_line_bool,
										   const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const BOOL_32 pset_change_bits,
										   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , char *pmerge_update_str,
										   char *pmerge_insert_fields_str,
										   char *pmerge_insert_values_str
										   #endif
										 )
{
	SHARED_set_bool_with_32_bit_change_bits_group( psystem,
												   &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->capacity_in_use_bool),
												   pcapacity_in_use_bool,
												   IRRIGATION_SYSTEM_CAPACITY_IN_USE_DEFAULT,
												   pgenerate_change_line_bool,
												   CHANGE_IRRIGATION_SYSTEM_CAPACITY_IN_USE,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->changes_to_upload_to_comm_server),
												   IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_in_use_bit,
												   SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_in_use_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
/**
 * Sets the system capacity with pump flow rate for a particular irrigation
 * system group. If the passed in group is NULL, an error is raised and a 0 is
 * returned. Additionally, if the parameter that's passed in is out-of-range, an
 * error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 *  	  ensure the group whose value is being changed isn't deleted during
 *  	  this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param psystem A pointer to the group to update.
 * @param pcapacity_with_pump The system capacity with pump flow rate to assign
 *                            to the group.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 8/10/2011 Adrianusv
 *
 * @revisions
 *    8/10/2011 Initial release
 *    4/4/2012  Added setting of change bits to distribute changes
 */
static void nm_SYSTEM_set_capacity_with_pump( void *const psystem,
											  UNS_32 pcapacity_with_pump,
											  const BOOL_32 pgenerate_change_line_bool,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
											)
{
	SHARED_set_uint32_with_32_bit_change_bits_group( psystem,
													 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->capacity_with_pump_gpm),
													 pcapacity_with_pump,
													 IRRIGATION_SYSTEM_CAPACITY_MIN,
													 IRRIGATION_SYSTEM_CAPACITY_MAX,
													 IRRIGATION_SYSTEM_CAPACITY_PUMP_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_IRRIGATION_SYSTEM_CAPACITY_WITH_PUMP,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->changes_to_upload_to_comm_server),
													 IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_with_pump_bit,
													 SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_with_pump_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
/**
 * Sets the system capacity without pump flow rate for a particular irrigation
 * system group. If the passed in group is NULL, an error is raised and a 0 is
 * returned. Additionally, if the parameter that's passed in is out-of-range, an
 * error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 *  	  ensure the group whose value is being changed isn't deleted during
 *  	  this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param psystem A pointer to the group to update.
 * @param pcapacity_without_pump The system capacity without pump flow rate to
 *  							 assign to the group.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 8/10/2011 Adrianusv
 *
 * @revisions
 *    8/10/2011 Initial release
 *    4/4/2012  Added setting of change bits to distribute changes
 */
static void nm_SYSTEM_set_capacity_without_pump( void *const psystem,
												 UNS_32 pcapacity_without_pump,
												 const BOOL_32 pgenerate_change_line_bool,
												 const UNS_32 preason_for_change,
												 const UNS_32 pbox_index_0,
												 const BOOL_32 pset_change_bits,
												 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												 #ifdef _MSC_VER
												 , char *pmerge_update_str,
												 char *pmerge_insert_fields_str,
												 char *pmerge_insert_values_str
												 #endif
											   )
{
	SHARED_set_uint32_with_32_bit_change_bits_group( psystem,
													 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->capacity_without_pump_gpm),
													 pcapacity_without_pump,
													 IRRIGATION_SYSTEM_CAPACITY_MIN,
													 IRRIGATION_SYSTEM_CAPACITY_MAX,
													 IRRIGATION_SYSTEM_CAPACITY_NONPUMP_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_IRRIGATION_SYSTEM_CAPACITY_WITHOUT_PUMP,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->changes_to_upload_to_comm_server),
													 IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_without_pump_bit,
													 SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_without_pump_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
/**
 * Sets the mainline break during irrigation flow rate for a particular
 * irrigation system group. If the passed in group is NULL, an error is raised
 * and a 0 is returned. Additionally, if the parameter that's passed in is
 * out-of-range, an error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 *  	  ensure the group whose value is being changed isn't deleted during
 *  	  this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param psystem A pointer to the group to update.
 * @param pmlb_during_irri The mainline break during irrigation flow rate to
 *  					   assign to the group.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 8/10/2011 Adrianusv
 *
 * @revisions
 *    8/10/2011 Initial release
 *    4/4/2012  Added setting of change bits to distribute changes
 */
static void nm_SYSTEM_set_mlb_during_irri( void *const psystem,
										   UNS_32 pmlb_during_irri,
										   const BOOL_32 pgenerate_change_line_bool,
										   const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const BOOL_32 pset_change_bits,
										   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , char *pmerge_update_str,
										   char *pmerge_insert_fields_str,
										   char *pmerge_insert_values_str
										   #endif
										 )
{
	SHARED_set_uint32_with_32_bit_change_bits_group( psystem,
													 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->mlb_during_irri_gpm),
													 pmlb_during_irri,
													 IRRIGATION_SYSTEM_MLB_MIN,
													 IRRIGATION_SYSTEM_MLB_MAX,
													 IRRIGATION_SYSTEM_MLB_DURING_IRRI_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_IRRIGATION_SYSTEM_MLB_DURING_IRRI,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->changes_to_upload_to_comm_server),
													 IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_during_irri_bit,
													 SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_during_irri_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
/**
 * Sets the mainline break during master valve override flow rate for a
 * particular irrigation system group. If the passed in group is NULL, an error
 * is raised and a 0 is returned. Additionally, if the parameter that's passed
 * in is out-of-range, an error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 *  	  ensure the group whose value is being changed isn't deleted during
 *  	  this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param psystem A pointer to the group to update.
 * @param pmlb_during_irri The mainline break during master valve override flow
 *  					   rate to assign to the group.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 8/10/2011 Adrianusv
 *
 * @revisions
 *    8/10/2011 Initial release
 *    4/4/2012  Added setting of change bits to distribute changes
 */
static void nm_SYSTEM_set_mlb_during_mvor( void *const psystem,
										   UNS_32 pmlb_during_mvor,
										   const BOOL_32 pgenerate_change_line_bool,
										   const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const BOOL_32 pset_change_bits,
										   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , char *pmerge_update_str,
										   char *pmerge_insert_fields_str,
										   char *pmerge_insert_values_str
										   #endif
										 )
{
	SHARED_set_uint32_with_32_bit_change_bits_group( psystem,
													 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->mlb_during_mvor_gpm),
													 pmlb_during_mvor,
													 IRRIGATION_SYSTEM_MLB_MIN,
													 IRRIGATION_SYSTEM_MLB_MAX,
													 IRRIGATION_SYSTEM_MLB_DURING_MVOR_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_IRRIGATION_SYSTEM_MLB_DURING_MVOR,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->changes_to_upload_to_comm_server),
													 IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_during_mvor_bit,
													 SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_during_mvor_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
/**
 * Sets the mainline break during all other times flow rate for a particular
 * irrigation system group. If the passed in group is NULL, an error is raised
 * and a 0 is returned. Additionally, if the parameter that's passed in is
 * out-of-range, an error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 *  	  ensure the group whose value is being changed isn't deleted during
 *  	  this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param psystem A pointer to the group to update.
 * @param pmlb_all_other_times The mainline break during all other times flow
 *  						   rate to assign to the group.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 8/10/2011 Adrianusv
 *
 * @revisions
 *    8/10/2011 Initial release
 *    4/4/2012  Added setting of change bits to distribute changes
 */
static void nm_SYSTEM_set_mlb_all_other_times( void *const psystem,
											   UNS_32 pmlb_all_other_times,
											   const BOOL_32 pgenerate_change_line_bool,
											   const UNS_32 preason_for_change,
											   const UNS_32 pbox_index_0,
											   const BOOL_32 pset_change_bits,
											   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											   #ifdef _MSC_VER
											   , char *pmerge_update_str,
											   char *pmerge_insert_fields_str,
											   char *pmerge_insert_values_str
											   #endif
											 )
{
	SHARED_set_uint32_with_32_bit_change_bits_group( psystem,
													 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->mlb_all_other_times_gpm),
													 pmlb_all_other_times,
													 IRRIGATION_SYSTEM_MLB_MIN,
													 IRRIGATION_SYSTEM_MLB_MAX,
													 IRRIGATION_SYSTEM_MLB_ALL_OTHER_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_IRRIGATION_SYSTEM_MLB_ALL_OTHER_TIMES,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->changes_to_upload_to_comm_server),
													 IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_all_other_times_bit,
													 SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_all_other_times_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
/**
 * Sets whether a particular irrigation system uses flow checking or not. If the
 * passed in group is NULL, an error is raised and a 0 is returned.
 * Additionally, if the parameter that's passed in is out-of-range, an error is
 * raised and the value isn't updated.
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 *  	  ensure the group whose value is being changed isn't deleted during
 *  	  this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param psystem A pointer to the group to update.
 * @param pflow_checking_in_use_bool True if the group will use flow checking;
 *                                   otherwise, false.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 10/25/2011 Adrianusv
 *
 * @revisions
 *    10/25/2011 Initial release
 *    4/4/2012   Added setting of change bits to distribute changes
 */
static void nm_SYSTEM_set_flow_checking_in_use( void *const psystem,
												BOOL_32 pflow_checking_in_use_bool,
												const BOOL_32 pgenerate_change_line_bool,
												const UNS_32 preason_for_change,
												const UNS_32 pbox_index_0,
												const BOOL_32 pset_change_bits,
												CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												#ifdef _MSC_VER
												, char *pmerge_update_str,
												char *pmerge_insert_fields_str,
												char *pmerge_insert_values_str
												#endif
											  )
{
	if( SHARED_set_bool_with_32_bit_change_bits_group( psystem,
													   &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->flow_checking_in_use_bool),
													   pflow_checking_in_use_bool,
													   IRRIGATION_SYSTEM_FLOW_CHECKING_IN_USE_DEFAULT,
													   pgenerate_change_line_bool,
													   CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_IN_USE,
													   preason_for_change,
													   pbox_index_0,
													   pset_change_bits,
													   pchange_bitfield_to_set,
													   &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->changes_to_upload_to_comm_server),
													   IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_in_use_bit,
													   SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_in_use_bit ]
													   #ifdef _MSC_VER
													   , pmerge_update_str,
													   pmerge_insert_fields_str,
													   pmerge_insert_values_str
													   #endif
													 ) == (true) )
	{
		#ifndef _MSC_VER

			// 2012.06.22 ajv : The system_preserves contain a copy of certain user settings. And proper
			// operation requires an accurate reflection of those settings. The system preserves uses
			// them in a variety of ways, including but not limited to flow checking. Request a
			// re-sync as this is one of those variables.
			SYSTEM_PRESERVES_request_a_resync( ((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->base.group_identity_number );

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * Sets the flow checking range for a particular level of a particular
 * irrigation system group. If the passed in group is NULL, an error is raised
 * and a 0 is returned. Additionally, if the parameter that's passed in is
 * out-of-range, an error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 *  	  ensure the group whose value is being changed isn't deleted during
 *  	  this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param psystem A pointer to the group to update.
 * @param prange_level The flow checking level (1-3) to assign the flow rate to.
 * @param pflow_rate_gpm The flow rate to assign to the flow checking level of
 *                       the group.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 10/25/2011 Adrianusv
 *
 * @revisions
 *    10/25/2011 Initial release
 *    4/4/2012   Added setting of change bits to distribute changes
 */
static void nm_SYSTEM_set_flow_checking_range( void *const psystem,
											   const UNS_32 prange_level,
											   UNS_32 pflow_rate_gpm,
											   const BOOL_32 pgenerate_change_line_bool,
											   const UNS_32 preason_for_change,
											   const UNS_32 pbox_index_0,
											   const BOOL_32 pset_change_bits,
											   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											   #ifdef _MSC_VER
											   , char *pmerge_update_str,
											   char *pmerge_insert_fields_str,
											   char *pmerge_insert_values_str
											   #endif
											 )
{
	char	lfield_name[ 32 ];

	if( (prange_level >= 1) && (prange_level <= IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_RANGES) )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_ranges_bit ], prange_level );

		if( SHARED_set_uint32_with_32_bit_change_bits_group( psystem,
															 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->flow_checking_ranges_gpm[ (prange_level - 1) ]),
															 pflow_rate_gpm,
															 IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_MIN,
															 IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_MAX,
															 IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS[ (prange_level - 1) ],
															 pgenerate_change_line_bool,
															 (CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_1 + (prange_level - 1)),
															 preason_for_change,
															 pbox_index_0,
															 pset_change_bits,
															 pchange_bitfield_to_set,
															 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->changes_to_upload_to_comm_server),
															 IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_ranges_bit,
															 lfield_name
															 #ifdef _MSC_VER
															 , pmerge_update_str,
															 pmerge_insert_fields_str,
															 pmerge_insert_values_str
															 #endif
														   ) == (true) )
		{
			#ifndef _MSC_VER

				// 2012.06.22 ajv : The system_preserves contain a copy of certain user settings. And proper
				// operation requires an accurate reflection of those settings. The system preserves uses
				// them in a variety of ways, including but not limited to flow checking. Request a
				// re-sync as this is one of those variables.
				SYSTEM_PRESERVES_request_a_resync( ((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->base.group_identity_number );

			#endif
		}
	}
	else
	{
		#ifndef _MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * Sets the positive flow checking tolerance for a particular level of a
 * particular irrigation system group. If the passed in group is NULL, an error
 * is raised and a 0 is returned. Additionally, if the parameter that's passed
 * in is out-of-range, an error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 *  	  ensure the group whose value is being changed isn't deleted during
 *  	  this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param psystem A pointer to the group to update.
 * @param prange_level The flow checking level (1-4) to assign the tolerance to.
 * @param ptolerance_gpm The positive tolerance to assign to the flow checking
 *  					 level of the group.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 * 
 * @author 10/25/2011 Adrianusv
 *
 * @revisions
 *    10/25/2011 Initial release
 *    4/4/2012   Added setting of change bits to distribute changes
 */
static void nm_SYSTEM_set_flow_checking_tolerance_plus( void *const psystem,
														const UNS_32 prange_level,
														UNS_32 ptolerance_gpm,
														const BOOL_32 pgenerate_change_line_bool,
														const UNS_32 preason_for_change,
														const UNS_32 pbox_index_0,
														const BOOL_32 pset_change_bits,
														CHANGE_BITS_32_BIT *pchange_bitfield_to_set
														#ifdef _MSC_VER
														, char *pmerge_update_str,
														char *pmerge_insert_fields_str,
														char *pmerge_insert_values_str
														#endif
													  )
{
	char	lfield_name[ 32 ];

	if( (prange_level >= 1) && (prange_level <= IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_TOLERANCES) )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_tolerances_plus_bit ], prange_level );

		if( SHARED_set_uint32_with_32_bit_change_bits_group( psystem,
															 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->flow_checking_tolerances_plus_gpm[ (prange_level - 1) ]),
															 ptolerance_gpm,
															 IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MIN,
															 IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MAX,
															 IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS[ (prange_level - 1) ],
															 pgenerate_change_line_bool,
															 (CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_TOL_PLUS_1 + (prange_level - 1)),
															 preason_for_change,
															 pbox_index_0,
															 pset_change_bits,
															 pchange_bitfield_to_set,
															 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->changes_to_upload_to_comm_server),
															 IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_tolerances_plus_bit,
															 lfield_name
															 #ifdef _MSC_VER
															 , pmerge_update_str,
															 pmerge_insert_fields_str,
															 pmerge_insert_values_str
															 #endif
														   ) == (true) )
		{
			#ifndef _MSC_VER

				// 2012.06.22 ajv : The system_preserves contain a copy of certain user settings. And proper
				// operation requires an accurate reflection of those settings. The system preserves uses
				// them in a variety of ways, including but not limited to flow checking. Request a
				// re-sync as this is one of those variables.
				SYSTEM_PRESERVES_request_a_resync( ((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->base.group_identity_number );

			#endif
		}
	}
	else
	{
		#ifndef _MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * Sets the negative flow checking tolerance for a particular level of a
 * particular irrigation system group. If the passed in group is NULL, an error
 * is raised and a 0 is returned. Additionally, if the parameter that's passed
 * in is out-of-range, an error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 *  	  ensure the group whose value is being changed isn't deleted during
 *  	  this process.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param psystem A pointer to the group to update.
 * @param prange_level The flow checking level (1-4) to assign the tolerance to.
 * @param ptolerance_gpm The negative tolerance to assign to the flow checking
 *  					 level of the group.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * 
 * @author 10/25/2011 Adrianusv
 *
 * @revisions
 *    10/25/2011 Initial release
 *    4/4/2012   Added setting of change bits to distribute changes
 */
static void nm_SYSTEM_set_flow_checking_tolerance_minus( void *const psystem,
														 const UNS_32 prange_level,
														 UNS_32 ptolerance_gpm,
														 const BOOL_32 pgenerate_change_line_bool,
														 const UNS_32 preason_for_change,
														 const UNS_32 pbox_index_0,
														 const BOOL_32 pset_change_bits,
														 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
														 #ifdef _MSC_VER
														 , char *pmerge_update_str,
														 char *pmerge_insert_fields_str,
														 char *pmerge_insert_values_str
														 #endif
													   )
{
	char	lfield_name[ 32 ];

	if( (prange_level >= 1) && (prange_level <= IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_TOLERANCES) )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_tolerances_minus_bit ], prange_level );

		if( SHARED_set_uint32_with_32_bit_change_bits_group( psystem,
															 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->flow_checking_tolerances_minus_gpm[ (prange_level - 1) ]),
															 ptolerance_gpm,
															 IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MIN,
															 IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_MAX,
															 IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS[ (prange_level - 1) ],
															 pgenerate_change_line_bool,
															 (CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_TOL_MINUS_1 + (prange_level - 1)),
															 preason_for_change,
															 pbox_index_0,
															 pset_change_bits,
															 pchange_bitfield_to_set,
															 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->changes_to_upload_to_comm_server),
															 IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_tolerances_minus_bit,
															 lfield_name
															 #ifdef _MSC_VER
															 , pmerge_update_str,
															 pmerge_insert_fields_str,
															 pmerge_insert_values_str
															 #endif
														   ) == (true) )
		{
			#ifndef _MSC_VER

				// 2012.06.22 ajv : The system_preserves contain a copy of certain user settings. And proper
				// operation requires an accurate reflection of those settings. The system preserves uses
				// them in a variety of ways, including but not limited to flow checking. Request a
				// re-sync as this is one of those variables.
				SYSTEM_PRESERVES_request_a_resync( ((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->base.group_identity_number );

			#endif
		}
	}
	else
	{
		#ifndef _MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
static void nm_SYSTEM_set_mvor_schedule_open_time( void *const psystem,
												   const UNS_32 pday_0,
												   UNS_32 popen_time,
												   const BOOL_32 pgenerate_change_line_bool,
												   const UNS_32 preason_for_change,
												   const UNS_32 pbox_index_0,
												   const BOOL_32 pset_change_bits,
												   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												   #ifdef _MSC_VER
												   , char *pmerge_update_str,
												   char *pmerge_insert_fields_str,
												   char *pmerge_insert_values_str
												   #endif
												 )
{
	char	lfield_name[ 32 ];

	if( (pday_0 >= 0) && (pday_0 < DAYS_IN_A_WEEK) )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_mvor_open_times_bit ], pday_0 );

		// ----------

		SHARED_set_uint32_with_32_bit_change_bits_group( psystem,
														 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->mvor_open_times[ pday_0 ]),
														 popen_time,
														 IRRIGATION_SYSTEM_MVOR_OPEN_TIME_MIN,
														 IRRIGATION_SYSTEM_MVOR_OPEN_TIME_MAX,
														 IRRIGATION_SYSTEM_MVOR_OPEN_TIME_DEFAULT,
														 pgenerate_change_line_bool,
														 (CHANGE_MVOR_OPEN_TIME_0 + pday_0),
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->changes_to_upload_to_comm_server),
														 IRRIGATION_SYSTEM_CHANGE_BITFIELD_mvor_open_times_bit,
														 lfield_name
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   );
	}
	else
	{
		#ifndef _MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
static void nm_SYSTEM_set_mvor_schedule_close_time( void *const psystem,
													const UNS_32 pday_0,
													UNS_32 popen_time,
													const BOOL_32 pgenerate_change_line_bool,
													const UNS_32 preason_for_change,
													const UNS_32 pbox_index_0,
													const BOOL_32 pset_change_bits,
													CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													#ifdef _MSC_VER
													, char *pmerge_update_str,
													char *pmerge_insert_fields_str,
													char *pmerge_insert_values_str
													#endif
												  )
{
	char	lfield_name[ 32 ];

	if( (pday_0 >= 0) && (pday_0 < DAYS_IN_A_WEEK) )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_mvor_close_times_bit ], pday_0 );

		// ----------

		SHARED_set_uint32_with_32_bit_change_bits_group( psystem,
														 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->mvor_close_times[ pday_0 ]),
														 popen_time,
														 IRRIGATION_SYSTEM_MVOR_CLOSE_TIME_MIN,
														 IRRIGATION_SYSTEM_MVOR_CLOSE_TIME_MAX,
														 IRRIGATION_SYSTEM_MVOR_CLOSE_TIME_DEFAULT,
														 pgenerate_change_line_bool,
														 (CHANGE_MVOR_CLOSE_TIME_0 + pday_0),
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->changes_to_upload_to_comm_server),
														 IRRIGATION_SYSTEM_CHANGE_BITFIELD_mvor_close_times_bit,
														 lfield_name
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   );
	}
	else
	{
		#ifndef _MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
static void nm_SYSTEM_set_budget_in_use(	void *const psystem,
											UNS_32 pbudget_in_use,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0,
											const BOOL_32 pset_change_bits,
											CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											#ifdef _MSC_VER
											, char *pmerge_update_str,
											char *pmerge_insert_fields_str,
											char *pmerge_insert_values_str
											#endif
										)
{
	SHARED_set_bool_with_32_bit_change_bits_group( psystem,
													 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->budget_in_use),
													 pbudget_in_use,
													 IRRIGATION_SYSTEM_BUDGET_IN_USE_NO,
													 pgenerate_change_line_bool,
													 CHANGE_IRRIGATION_SYSTEM_BUDGET_IN_USE,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->changes_to_upload_to_comm_server),
													 IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_in_use_bit,
													 SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_in_use_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_SYSTEM_set_budget_mode(	void *const psystem,
										UNS_32 pbudget_mode,
										const BOOL_32 pgenerate_change_line_bool,
										const UNS_32 preason_for_change,
										const UNS_32 pbox_index_0,
										const BOOL_32 pset_change_bits,
										CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										#ifdef _MSC_VER
										, char *pmerge_update_str,
										char *pmerge_insert_fields_str,
										char *pmerge_insert_values_str
										#endif
									  )
{
	SHARED_set_uint32_with_32_bit_change_bits_group( psystem,
													 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->budget_mode),
													 pbudget_mode,
													 IRRIGATION_SYSTEM_BUDGET_MODE_MIN,
													 IRRIGATION_SYSTEM_BUDGET_MODE_MAX,
													 IRRIGATION_SYSTEM_BUDGET_MODE_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_IRRIGATION_SYSTEM_BUDGET_MODE,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->changes_to_upload_to_comm_server),
													 IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_mode_bit,
													 SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_mode_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_SYSTEM_set_budget_meter_read_time(	void *const psystem,
													UNS_32 pbudget_meter_read_time,
													const BOOL_32 pgenerate_change_line_bool,
													const UNS_32 preason_for_change,
													const UNS_32 pbox_index_0,
													const BOOL_32 pset_change_bits,
													CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													#ifdef _MSC_VER
													, char *pmerge_update_str,
													char *pmerge_insert_fields_str,
													char *pmerge_insert_values_str
													#endif
												)
{
	SHARED_set_uint32_with_32_bit_change_bits_group( psystem,
													 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->budget_meter_read_time),
													 pbudget_meter_read_time,
													 IRRIGATION_SYSTEM_BUDGET_METER_READ_TIME_MIN,
													 IRRIGATION_SYSTEM_BUDGET_METER_READ_TIME_MAX,
													 IRRIGATION_SYSTEM_BUDGET_METER_READ_TIME_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_READ_TIME,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->changes_to_upload_to_comm_server),
													 IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_meter_read_time_bit,
													 SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_meter_read_time_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}

/* ---------------------------------------------------------- */
static void nm_SYSTEM_set_budget_number_of_annual_periods(	void *const psystem,
															UNS_32 pbudget_number_of_annual_periods,
															const BOOL_32 pgenerate_change_line_bool,
															const UNS_32 preason_for_change,
															const UNS_32 pbox_index_0,
															const BOOL_32 pset_change_bits,
															CHANGE_BITS_32_BIT *pchange_bitfield_to_set
															#ifdef _MSC_VER
															, char *pmerge_update_str,
															char *pmerge_insert_fields_str,
															char *pmerge_insert_values_str
															#endif
												)
{
	SHARED_set_uint32_with_32_bit_change_bits_group( psystem,
													 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->budget_number_of_annual_periods),
													 pbudget_number_of_annual_periods,
													 IRRIGATION_SYSTEM_BUDGET_ANNUAL_PERIODS_MIN,
													 IRRIGATION_SYSTEM_BUDGET_ANNUAL_PERIODS_MAX,
													 IRRIGATION_SYSTEM_BUDGET_ANNUAL_PERIODS_DEFAULT,
													 pgenerate_change_line_bool,
													 CHANGE_IRRIGATION_SYSTEM_BUDGET_PERIODS_PER_YEAR_IDX,
													 preason_for_change,
													 pbox_index_0,
													 pset_change_bits,
													 pchange_bitfield_to_set,
													 &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->changes_to_upload_to_comm_server),
													 IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_annual_periods_bit,
													 SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_annual_periods_bit ]
													 #ifdef _MSC_VER
													 , pmerge_update_str,
													 pmerge_insert_fields_str,
													 pmerge_insert_values_str
													 #endif
												   );
}



/* ---------------------------------------------------------- */
extern void nm_SYSTEM_set_budget_period(	void *const psystem,
											const UNS_32 pperiod_index0,
											UNS_32 pstart_date,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0,
											const BOOL_32 pset_change_bits,
											CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											#ifdef _MSC_VER
											, char *pmerge_update_str,
											char *pmerge_insert_fields_str,
											char *pmerge_insert_values_str
											#endif
										  )
{
	char	lfield_name[ 32 ];

	IRRIGATION_SYSTEM_GROUP_STRUCT* ptr_sys;

	ptr_sys = (IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem;

	if( (pperiod_index0 >= 0) && (pperiod_index0 < IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS) )
	{
		// 9/17/2015 rmd : Develop the DB field name. Wissam names them 1..12.
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_meter_read_date_bit], pperiod_index0+1 );

		// ----------

		SHARED_set_uint32_with_32_bit_change_bits_group( psystem,
														 &(ptr_sys->budget_meter_read_date[ pperiod_index0 ]),
														 pstart_date,
														 IRRIGATION_SYSTEM_BUDGET_PERIOD_DATE_MIN,
														 IRRIGATION_SYSTEM_BUDGET_PERIOD_DATE_MAX,
														 IRRIGATION_SYSTEM_BUDGET_PERIOD_DATE_DEFAULT,
														 pgenerate_change_line_bool,
														 (CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_0 + pperiod_index0),
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &(ptr_sys->changes_to_upload_to_comm_server),
														 IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_meter_read_date_bit,
														 lfield_name
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   );
	}
	else
	{
		#ifndef _MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
static void nm_SYSTEM_set_budget_flow_type( void *const psystem,
											const UNS_32 pindex0,
											BOOL_32 pbudget_flow_type,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0,
											const BOOL_32 pset_change_bits,
											CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											#ifdef _MSC_VER
											, char *pmerge_update_str,
											char *pmerge_insert_fields_str,
											char *pmerge_insert_values_str
											#endif
											)
{
	char lfield_name[ 32 ];

	IRRIGATION_SYSTEM_GROUP_STRUCT* ptr_sys;

	// ----------

	ptr_sys = (IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem;

	// ----------

	if( pindex0 < FLOW_TYPE_NUM_TYPES )
	{
		// 4/19/2016 skc : Develop the DB field name.
		snprintf( lfield_name, sizeof(lfield_name), "%s%s", SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_flow_type_bit], GetFlowTypeStr(pindex0) );


		SHARED_set_bool_with_32_bit_change_bits_group( psystem,
														 &ptr_sys->budget_flow_type[pindex0],
														 pbudget_flow_type,
														 (true),
														 pgenerate_change_line_bool,
														 CHANGE_IRRIGATION_SYSTEM_BUDGET_FLOW_TYPE_IRRIGATION + pindex0,
														 preason_for_change,
														 pbox_index_0,
														 pset_change_bits,
														 pchange_bitfield_to_set,
														 &ptr_sys->changes_to_upload_to_comm_server,
														 IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_flow_type_bit,
														 lfield_name
														 #ifdef _MSC_VER
														 , pmerge_update_str,
														 pmerge_insert_fields_str,
														 pmerge_insert_values_str
														 #endif
													   );
	}
	else
	{
		#ifndef _MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
static void nm_SYSTEM_set_system_in_use(	void *const psystem,
											BOOL_32 pin_use,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0,
											const BOOL_32 pset_change_bits,
											CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											#ifdef _MSC_VER
											, char *pmerge_update_str,
											char *pmerge_insert_fields_str,
											char *pmerge_insert_values_str
											#endif
									   )
{
	SHARED_set_bool_with_32_bit_change_bits_group( psystem,
												   &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->in_use),
												   pin_use,
												   IRRIGATION_SYSTEM_IN_USE_DEFAULT,
												   pgenerate_change_line_bool,
												   CHANGE_IRRIGATION_SYSTEM_IN_USE,
												   preason_for_change,
												   pbox_index_0,
												   pset_change_bits,
												   pchange_bitfield_to_set,
												   &(((IRRIGATION_SYSTEM_GROUP_STRUCT*)psystem)->changes_to_upload_to_comm_server),
												   IRRIGATION_SYSTEM_CHANGE_BITFIELD_in_use_bit,
												   SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_in_use_bit ]
												   #ifdef _MSC_VER
												   , pmerge_update_str,
												   pmerge_insert_fields_str,
												   pmerge_insert_values_str
												   #endif
												 );
}

/* ---------------------------------------------------------- */
/**
 * Locates the group in the list that matches the passed in temporary group and
 * uses the structure's set routines to compare the two and make any changes to
 * the list's group. If any changes are made during this process, a function is
 * called to store the changes in the list.
 * 
 * @mutex Calling function must have the list_system_recursive_MUTEX mutex to
 * ensure the list isn't modified during this process.
 *
 * @executed Executed within the context of the key processing task when the
 *  		 user leaves the screen; within the context of the CommMngr task
 *  		 when a programming change is made from the central or handheld
 *  		 radio remote.
 * 
 * @param ptemporary_group A pointer to a temporary group which contains the
 *                         changes to store.
 * @param ptype_of_change Whether the change is an add, delete, or edit.
 * @param preason_for_change The reason for the change, such as keypad.
 * @param pbox_index_0 The serial number of the controller
 *                                    which initiated the change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 8/10/2011 Adrianusv
 *
 * @revisions
 *    8/10/2011 Initial release
 *    4/4/2012  Removed unnecessary variables and function calls
 *  			Added passing of pset_change_bits into set routine to indicate
 *  			whether the change was made locally so change bits are set
 *  			properly.
 *  			Made procedure static since it's now only called from within
 *  			this file.
 */
static void nm_SYSTEM_store_changes( IRRIGATION_SYSTEM_GROUP_STRUCT *const ptemporary_group,
									 const BOOL_32 psystem_created,
									 const UNS_32 preason_for_change,
									 const UNS_32 pbox_index_0,
									 const BOOL_32 pset_change_bits,
									 const UNS_32 pchanges_received_from,
									 CHANGE_BITS_32_BIT pbitfield_of_changes
									 #ifdef _MSC_VER
									 , char *pmerge_update_str,
									 char *pmerge_insert_fields_str,
								  	 char *pmerge_insert_values_str
									 #endif
								   )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	CHANGE_BITS_32_BIT				*lchange_bitfield_to_set;

	UNS_32	i;

	// ----------

	#ifndef _MSC_VER

	nm_GROUP_find_this_group_in_list( &system_group_list_hdr, ptemporary_group, (void**)&lsystem );

	if( lsystem != NULL )
	#else

	lsystem = ptemporary_group;

	#endif
	{
		#ifndef _MSC_VER

			// 5/12/2014 ajv : Only generate the alert that the group was created if
			// it was actually created AND it wasn't created as part of a token or
			// token response receipt.
			if( (psystem_created == (true)) && ((pchanges_received_from != CHANGE_REASON_CHANGED_AT_SLAVE) && (pchanges_received_from != CHANGE_REASON_DISTRIBUTED_BY_MASTER)) )
			{
				Alert_ChangeLine_Group( CHANGE_GROUP_CREATED, nm_GROUP_get_name( ptemporary_group ), NULL, NULL, NUMBER_OF_CHARS_IN_A_NAME, preason_for_change, pbox_index_0 );
			}	

		#endif

		#ifndef _MSC_VER

			lchange_bitfield_to_set = SYSTEM_get_change_bits_ptr( lsystem, pchanges_received_from );

		#else

			// 7/28/2015 ajv : Initialize to NULL since this variable is used by the CommServer but
			// expected to be NULL.
			lchange_bitfield_to_set = NULL;

		#endif

		// 4/24/2015 ajv : Only process the change if it was either initiated via the keypad
		// (locally) or if the change bit was set (when called via communications)
		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_description_bit ) == (true)) )
		{
			SHARED_set_name_32_bit_change_bits( lsystem, nm_GROUP_get_name( ptemporary_group ), !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set, &lsystem->changes_to_upload_to_comm_server, IRRIGATION_SYSTEM_CHANGE_BITFIELD_description_bit
												#ifdef _MSC_VER
												, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_description_bit ], pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												#endif
											  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_in_use_bit ) == (true)) )
		{
			nm_SYSTEM_set_capacity_in_use( lsystem, ptemporary_group->capacity_in_use_bool, !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
										   #endif
										 );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_with_pump_bit ) == (true)) )
		{
			nm_SYSTEM_set_capacity_with_pump( lsystem, ptemporary_group->capacity_with_pump_gpm, !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
											  #endif
											);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_without_pump_bit ) == (true)) )
		{
			nm_SYSTEM_set_capacity_without_pump( lsystem, ptemporary_group->capacity_without_pump_gpm, !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												 #ifdef _MSC_VER
												 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												 #endif
											   );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_during_irri_bit ) == (true)) )
		{
			nm_SYSTEM_set_mlb_during_irri( lsystem, ptemporary_group->mlb_during_irri_gpm, !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
										   #endif
										 );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_during_mvor_bit ) == (true)) )
		{
			nm_SYSTEM_set_mlb_during_mvor( lsystem, ptemporary_group->mlb_during_mvor_gpm, !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
										   #endif
										 );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_all_other_times_bit ) == (true)) )
		{
			nm_SYSTEM_set_mlb_all_other_times( lsystem, ptemporary_group->mlb_all_other_times_gpm, !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											   #ifdef _MSC_VER
											   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
											   #endif
											 );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_in_use_bit ) == (true)) )
		{
			nm_SYSTEM_set_flow_checking_in_use( lsystem, ptemporary_group->flow_checking_in_use_bool, !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												#ifdef _MSC_VER
												, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												#endif
											  );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_ranges_bit ) == (true)) )
		{
			for( i = 0; i < IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_RANGES; ++i )
			{
				nm_SYSTEM_set_flow_checking_range( lsystem, (i + 1), ptemporary_group->flow_checking_ranges_gpm[ i ], !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												   #ifdef _MSC_VER
												   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												   #endif
												 );
			}
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_tolerances_plus_bit ) == (true)) )
		{
			for( i = 0; i < IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_TOLERANCES; ++i )
			{
				nm_SYSTEM_set_flow_checking_tolerance_plus( lsystem, (i + 1), ptemporary_group->flow_checking_tolerances_plus_gpm[ i ], !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
															#ifdef _MSC_VER
															, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
															#endif
														  );
			}
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_tolerances_minus_bit ) == (true)) )
		{
			for( i = 0; i < IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_TOLERANCES; ++i )
			{
				nm_SYSTEM_set_flow_checking_tolerance_minus( lsystem, (i + 1), ptemporary_group->flow_checking_tolerances_minus_gpm[ i ], !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
															 #ifdef _MSC_VER
															 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
															 #endif
														   );
			}
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_mvor_open_times_bit ) == (true)) )
		{
			for( i = 0; i < DAYS_IN_A_WEEK; ++i )
			{
				nm_SYSTEM_set_mvor_schedule_open_time( lsystem, i, ptemporary_group->mvor_open_times[ i ], !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
													   #ifdef _MSC_VER
													   , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
													   #endif
													 );
			}
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_mvor_close_times_bit ) == (true)) )
		{
			for( i = 0; i < DAYS_IN_A_WEEK; ++i )
			{
				nm_SYSTEM_set_mvor_schedule_close_time( lsystem, i, ptemporary_group->mvor_close_times[ i ], !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
														#ifdef _MSC_VER
														, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
														#endif
													  );
			}
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_in_use_bit ) == (true)) )
		{
			nm_SYSTEM_set_budget_in_use(	lsystem, ptemporary_group->budget_in_use, !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
											#ifdef _MSC_VER
											, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
											#endif
										);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_meter_read_time_bit ) == (true)) )
		{
			nm_SYSTEM_set_budget_meter_read_time(	lsystem, ptemporary_group->budget_meter_read_time, !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
													#ifdef _MSC_VER
													, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
													#endif
												);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_annual_periods_bit ) == (true)) )
		{
			nm_SYSTEM_set_budget_number_of_annual_periods(	lsystem, ptemporary_group->budget_number_of_annual_periods, !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
													#ifdef _MSC_VER
													, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
													#endif
												);
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_meter_read_date_bit ) == (true)) )
		{
			for( i=0; i<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS; i++ )
			{
				nm_SYSTEM_set_budget_period(	lsystem, i, ptemporary_group->budget_meter_read_date[ i ], !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												#ifdef _MSC_VER
												, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												#endif
											);
			}
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_mode_bit ) == (true)) )
		{
			nm_SYSTEM_set_budget_mode(	lsystem, ptemporary_group->budget_mode, !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
										#ifdef _MSC_VER
										, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
										#endif
									 );
		}

		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_flow_type_bit ) == (true)) )
		{
			for( i=0; i<FLOW_TYPE_NUM_TYPES; i++ )
			{
				nm_SYSTEM_set_budget_flow_type(	lsystem, i, ptemporary_group->budget_flow_type[ i ], !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
												#ifdef _MSC_VER
												, pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
												#endif
												);
			}
		}
		if( (preason_for_change == CHANGE_REASON_KEYPAD) || (B_IS_SET( pbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_in_use_bit ) == (true)) )
		{
			#ifndef _MSC_VER

				// 2/11/2016 ajv : If the group was deleted, reorder the list by moving the deleted item to
				// the end of the list.
				if( (lsystem->in_use) && (!ptemporary_group->in_use) )
				{
					nm_ListRemove( &system_group_list_hdr, lsystem );

					nm_ListInsertTail( &system_group_list_hdr, lsystem );
				}

			#endif

			nm_SYSTEM_set_system_in_use( lsystem, ptemporary_group->in_use, !(psystem_created), preason_for_change, pbox_index_0, pset_change_bits, lchange_bitfield_to_set
										 #ifdef _MSC_VER
										 , pmerge_update_str, pmerge_insert_fields_str, pmerge_insert_values_str
										 #endif
									   );
		}

		#ifndef _MSC_VER

			// 5/13/2014 ajv : Since we've modified the system definition,
			// synchronize the changes to the system preserves structure.
			SYSTEM_PRESERVES_synchronize_preserves_to_file();

		#endif
	}
	#ifndef _MSC_VER
	else
	{
		Alert_group_not_found();
	}
	#endif
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex
 *  	  to ensure no list items are added or removed during this process.
 *
 * @executed Executed within the context of the CommMngr task as a token or
 *           token response is being built.
 * 
 * @param pucp A pointer to an unsigned character pointer which contains the
 *             information that was received.
 * @param preason_for_change The reason for the change, such as keypad.
 * 
 * @return UNS_32 The total size of the extracted data. This is used to
 *                increment the ucp in the calling function.
 *
 * @author 4/4/2012 Adrianusv
 *
 * @revisions
 *    4/4/2012 Initial release
 */
extern UNS_32 nm_SYSTEM_extract_and_store_changes_from_comm( const unsigned char *pucp,
															 const UNS_32 preason_for_change,
															 const BOOL_32 pset_change_bits,
															 const UNS_32 pchanges_received_from
															 #ifdef _MSC_VER
															 , char *pSQL_statements,
															 char *pSQL_search_condition_str,
															 char *pSQL_update_str,
															 char *pSQL_insert_fields_str,
															 char *pSQL_insert_values_str,
															 char *pSQL_single_merge_statement_buf,
															 const UNS_32 pnetwork_ID
															 #endif
														   )
{
	CHANGE_BITS_32_BIT	lbitfield_of_changes;

	IRRIGATION_SYSTEM_GROUP_STRUCT 	*ltemporary_system;

	#ifndef _MSC_VER

	IRRIGATION_SYSTEM_GROUP_STRUCT 	*lmatching_system;

	#endif

	UNS_32	lnum_changed_systems;

	UNS_32	lsize_of_bitfield;

	BOOL_32	lsystem_created;

	UNS_32	lsystem_id;

	UNS_32	i;

	UNS_32	rv;

	// ----------

	rv = 0;

	lsystem_created = (false);

	// ----------

	// Extract the number of changed groups
	memcpy( &lnum_changed_systems, pucp, sizeof(lnum_changed_systems) );
	pucp += sizeof( lnum_changed_systems );
	rv += sizeof( lnum_changed_systems );

	/*
	// 8/8/2016 rmd : For debug only.
	if( pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE )
	{
		Alert_Message_va( "rcvd from slave: SYSTEM CHANGES for %u groups", lnum_changed_systems );
	}
	else
	if( pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER )
	{
		Alert_Message_va( "rcvd from master: SYSTEM CHANGES for %u groups", lnum_changed_systems );
	}
	else
	{
		Alert_Message_va( "rcvd from UNK: SYSTEM CHANGES for %u groups", lnum_changed_systems );
	}
	*/
	
	// ----------
	
	for( i = 0; i < lnum_changed_systems; ++i )
	{
		memcpy( &lsystem_id, pucp, sizeof(lsystem_id) );
		pucp += sizeof( lsystem_id );
		rv += sizeof( lsystem_id );

		memcpy( &lsize_of_bitfield, pucp, sizeof(lsize_of_bitfield) );
		pucp += sizeof( lsize_of_bitfield );
		rv += sizeof( lsize_of_bitfield );

		memcpy( &lbitfield_of_changes, pucp, sizeof(lbitfield_of_changes) );
		pucp += sizeof( lbitfield_of_changes );
		rv += sizeof( lbitfield_of_changes );

		// 1/8/2015 ajv : When compiling for the CommServer, we build a MERGE statement if any of
		// the station's settings have changed. Therefore, we can completely skip over searching for
		// the station - this will fail anyway since the list doesn't exist in the comm server.
		#ifndef _MSC_VER

			// Find the existing group with the group ID
			lmatching_system = nm_GROUP_get_ptr_to_group_with_this_GID( &system_group_list_hdr, lsystem_id, (true) );

			// 3/6/2013 ajv : If the specified group doesn't exist yet, we need to
			// create it and assign it the group ID.
			if( lmatching_system == NULL )
			{
				lmatching_system = nm_SYSTEM_create_new_group( pchanges_received_from );

				// Force the new Group ID to the passed-in group ID.
				lmatching_system->base.group_identity_number = lsystem_id;

				lsystem_created = (true);

				// 5/7/2014 ajv : Since we've added a new system to the file force a
				// sync with the preserves.
				SYSTEM_PRESERVES_synchronize_preserves_to_file();

				// ----------

				if( pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER )
				{
					// 5/2/2014 ajv : Any group creation caused by receipt of a
					// token is  for stations not at this box. And therefore
					// we don't have to resend out these changes. The distribution
					// of this activity creating groups and assigning the values
					// STOPS here in this case.
					SHARED_clear_all_32_bit_change_bits( &lmatching_system->changes_to_send_to_master, list_system_recursive_MUTEX );

					//Alert_Message_va( "PDATA: System %u created cleared", lsystem_id );
				}
				else
				{
					//Alert_Message_va( "PDATA: System %u created", lsystem_id );
				}
			}
			else
			{
				//Alert_Message_va( "PDATA: System %u found 0x%08x", lsystem_id, lbitfield_of_changes );
			}

		#endif

		#ifndef _MSC_VER

		if( lmatching_system != NULL )

		#endif
		{
			// Allocate memory for, and initialize, the temporary group
			#ifndef _MSC_VER
				ltemporary_system = mem_malloc( sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT) );
			#else
				ltemporary_system = malloc( sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT) );
			#endif

			memset( ltemporary_system, 0x00, sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT) );

			#ifndef _MSC_VER

				// Make a copy of the group. We do this so we can change the settings
				// and pass it to the store_changes routine which will actually update
				// the group's settings.
				memcpy( ltemporary_system, lmatching_system, sizeof(IRRIGATION_SYSTEM_GROUP_STRUCT) );

			#else

				ltemporary_system->base.group_identity_number = lsystem_id;

			#endif

			// ------------------

			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_description_bit ) == (true) )
			{
				memcpy( &ltemporary_system->base.description, pucp, NUMBER_OF_CHARS_IN_A_NAME );
				pucp += NUMBER_OF_CHARS_IN_A_NAME;
				rv += NUMBER_OF_CHARS_IN_A_NAME;
			}

			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_used_with_irri_bit ) == (true) )
			{
				memcpy( &ltemporary_system->used_for_irrigation_bool, pucp, sizeof(ltemporary_system->used_for_irrigation_bool) );
				pucp += sizeof( ltemporary_system->used_for_irrigation_bool );
				rv += sizeof( ltemporary_system->used_for_irrigation_bool );
			}

			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_in_use_bit ) == (true) )
			{
				memcpy( &ltemporary_system->capacity_in_use_bool, pucp, sizeof(ltemporary_system->capacity_in_use_bool) );
				pucp += sizeof( ltemporary_system->capacity_in_use_bool );
				rv += sizeof( ltemporary_system->capacity_in_use_bool );
			}

			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_with_pump_bit ) == (true) )
			{
				memcpy( &ltemporary_system->capacity_with_pump_gpm, pucp, sizeof(ltemporary_system->capacity_with_pump_gpm) );
				pucp += sizeof( ltemporary_system->capacity_with_pump_gpm );
				rv += sizeof( ltemporary_system->capacity_with_pump_gpm );
			}

			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_without_pump_bit ) == (true) )
			{
				memcpy( &ltemporary_system->capacity_without_pump_gpm, pucp, sizeof(ltemporary_system->capacity_without_pump_gpm) );
				pucp += sizeof( ltemporary_system->capacity_without_pump_gpm );
				rv += sizeof( ltemporary_system->capacity_without_pump_gpm );
			}

			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_during_irri_bit ) == (true) )
			{
				memcpy( &ltemporary_system->mlb_during_irri_gpm, pucp, sizeof(ltemporary_system->mlb_during_irri_gpm) );
				pucp += sizeof( ltemporary_system->mlb_during_irri_gpm );
				rv += sizeof( ltemporary_system->mlb_during_irri_gpm );
			}

			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_during_mvor_bit ) == (true) )
			{
				memcpy( &ltemporary_system->mlb_during_mvor_gpm, pucp, sizeof(ltemporary_system->mlb_during_mvor_gpm) );
				pucp += sizeof( ltemporary_system->mlb_during_mvor_gpm );
				rv += sizeof( ltemporary_system->mlb_during_mvor_gpm );
			}

			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_all_other_times_bit ) == (true) )
			{
				memcpy( &ltemporary_system->mlb_all_other_times_gpm, pucp, sizeof(ltemporary_system->mlb_all_other_times_gpm) );
				pucp += sizeof( ltemporary_system->mlb_all_other_times_gpm );
				rv += sizeof( ltemporary_system->mlb_all_other_times_gpm );
			}

			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_in_use_bit ) == (true) )
			{
				memcpy( &ltemporary_system->flow_checking_in_use_bool, pucp, sizeof(ltemporary_system->flow_checking_in_use_bool) );
				pucp += sizeof( ltemporary_system->flow_checking_in_use_bool );
				rv += sizeof( ltemporary_system->flow_checking_in_use_bool );
			}

			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_ranges_bit ) == (true) )
			{
				memcpy( &ltemporary_system->flow_checking_ranges_gpm, pucp, sizeof(ltemporary_system->flow_checking_ranges_gpm) );
				pucp += sizeof( ltemporary_system->flow_checking_ranges_gpm );
				rv += sizeof( ltemporary_system->flow_checking_ranges_gpm );
			}

			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_tolerances_plus_bit ) == (true) )
			{
				memcpy( &ltemporary_system->flow_checking_tolerances_plus_gpm, pucp, sizeof(ltemporary_system->flow_checking_tolerances_plus_gpm) );
				pucp += sizeof( ltemporary_system->flow_checking_tolerances_plus_gpm );
				rv += sizeof( ltemporary_system->flow_checking_tolerances_plus_gpm );
			}

			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_tolerances_minus_bit ) == (true) )
			{
				memcpy( &ltemporary_system->flow_checking_tolerances_minus_gpm, pucp, sizeof(ltemporary_system->flow_checking_tolerances_minus_gpm) );
				pucp += sizeof( ltemporary_system->flow_checking_tolerances_minus_gpm );
				rv += sizeof( ltemporary_system->flow_checking_tolerances_minus_gpm );
			}

			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_mvor_open_times_bit ) == (true) )
			{
				memcpy( &ltemporary_system->mvor_open_times, pucp, sizeof(ltemporary_system->mvor_open_times) );
				pucp += sizeof( ltemporary_system->mvor_open_times );
				rv += sizeof( ltemporary_system->mvor_open_times );
			}

			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_mvor_close_times_bit ) == (true) )
			{
				memcpy( &ltemporary_system->mvor_close_times, pucp, sizeof(ltemporary_system->mvor_close_times) );
				pucp += sizeof( ltemporary_system->mvor_close_times );
				rv += sizeof( ltemporary_system->mvor_close_times );
			}

			// ----------
			
			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_in_use_bit ) == (true) )
			{
				memcpy( &ltemporary_system->budget_in_use, pucp, sizeof(ltemporary_system->budget_in_use) );
				pucp += sizeof( ltemporary_system->budget_in_use );
				rv += sizeof( ltemporary_system->budget_in_use );
			}
			
			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_meter_read_time_bit ) == (true) )
			{
				memcpy( &ltemporary_system->budget_meter_read_time, pucp, sizeof(ltemporary_system->budget_meter_read_time) );
				pucp += sizeof( ltemporary_system->budget_meter_read_time );
				rv += sizeof( ltemporary_system->budget_meter_read_time );
			}

			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_annual_periods_bit ) == (true) )
			{
				memcpy( &ltemporary_system->budget_number_of_annual_periods, pucp, sizeof(ltemporary_system->budget_number_of_annual_periods) );
				pucp += sizeof( ltemporary_system->budget_number_of_annual_periods );
				rv += sizeof( ltemporary_system->budget_number_of_annual_periods );
			}
			
			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_meter_read_date_bit ) == (true) )
			{
				memcpy( &ltemporary_system->budget_meter_read_date, pucp, sizeof(ltemporary_system->budget_meter_read_date) );
				pucp += sizeof( ltemporary_system->budget_meter_read_date );
				rv += sizeof( ltemporary_system->budget_meter_read_date );
			}

			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_mode_bit ) == (true) )
			{
				memcpy( &ltemporary_system->budget_mode, pucp, sizeof(ltemporary_system->budget_mode) );
				pucp += sizeof( ltemporary_system->budget_mode );
				rv += sizeof( ltemporary_system->budget_mode );
			}

			// ----------

			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_in_use_bit ) == (true) )
			{
				memcpy( &ltemporary_system->in_use, pucp, sizeof(ltemporary_system->in_use) );
				pucp += sizeof( ltemporary_system->in_use );
				rv += sizeof( ltemporary_system->in_use );
			}

			if( B_IS_SET( lbitfield_of_changes, IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_flow_type_bit ) == (true) )
			{
				memcpy( &ltemporary_system->budget_flow_type, pucp, sizeof(ltemporary_system->budget_flow_type) );
				pucp += sizeof( ltemporary_system->budget_flow_type );
				rv += sizeof( ltemporary_system->budget_flow_type );
			}

			// ----------
			
			#ifdef _MSC_VER

				// 1/16/2015 ajv : Make sure the strings are fully initialized to NULL since we rely on this
				// to determine whether the string is empty or not.
				memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

				// 4/11/2016 ajv : Insert the network ID and GID in case the system doesn't exist in the
				// database yet.
				SQL_MERGE_build_upsert_uint32( pnetwork_ID, SQL_MERGE_include_field_name_in_insert_clause, "NetworkID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
				SQL_MERGE_build_upsert_uint32( ltemporary_system->base.group_identity_number, SQL_MERGE_include_field_name_in_insert_clause, "SystemGID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

			#endif

			// ----------

			nm_SYSTEM_store_changes( ltemporary_system, lsystem_created, preason_for_change, 0, pset_change_bits, pchanges_received_from, lbitfield_of_changes
									 #ifdef _MSC_VER
									 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
									 #endif
								   );

			// ----------
			
			#ifndef _MSC_VER

				mem_free( ltemporary_system );

			#else

				SQL_MERGE_build_search_condition_uint32( "NetworkID", pnetwork_ID, pSQL_search_condition_str );
				SQL_MERGE_build_search_condition_uint32( "SystemGID", ltemporary_system->base.group_identity_number, pSQL_search_condition_str );

				SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements( DATABASE_TABLE_NAME_SYSTEMS, pSQL_search_condition_str, SQL_MERGE_include_field_name_in_insert_clause, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf );

				// ----------

				free( ltemporary_system );

			#endif
		}
		#ifndef _MSC_VER
		else
		{
			Alert_group_not_found();
		}
		#endif
	}

	#ifndef _MSC_VER

		// Only save the file if we've extracted information out of the ucp. This is
		// a sanity check to ensure the bit is only set if data was included.
		if( rv > 0 )
		{
			// 5/13/2015 ajv : We want to save the file in the following cases:
			// 
			//   - A message from the CommServer since we've responded that we received it and need to
			//     ensure it's saved.
			//   - A token response from the slave to the master
			//   - A token from the master to the slave
			//   
			// We DO NOT want to save the file when a token is received by the master since we've
			// already saved the file for one of the reasons above. This will otherwise result in a
			// double save for each change - once when it comes in and once when it's received when the
			// master distributes the changes back out.
			if( (pchanges_received_from == CHANGE_REASON_CENTRAL_OR_MOBILE) ||
				(pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE) ||
				((pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER) && (!FLOWSENSE_we_are_a_master_one_way_or_another())) )
			{
				// 12/10/2014 ajv : Schedule a save rather than saving immediately to prevent stacking of
				// saves. Additionally, set a relatively short time to help cover cases where users make a
				// lot of changes in a short period.
				FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_IRRIGATION_SYSTEM, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
			}
		}
		else
		{
			Alert_bit_set_with_no_data();
		}

	#endif

	return ( rv );
}

#ifdef _MSC_VER

/* ---------------------------------------------------------- */
extern INT_32 SYSTEM_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT	*lsystem;

	INT_32	rv;

	// ----------

	// 4/15/2016 ajv : In most cases, we're going to pass back an array_index of 0. However, in
	// the event the value in question is part of an array, we're going to pass back the actual
	// index so that the CommServer can sort not just on the bit number, but also the
	// array_index for values that share the same bit number.
	*psecondary_sort_order = 0;

	rv = -1;

	// ----------

	if( strncmp( pfield_name, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_description_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_description_bit;
		*psize_of_var_ptr = sizeof(lsystem->base.description);
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_used_with_irri_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_used_with_irri_bit;
		*psize_of_var_ptr = sizeof(lsystem->used_for_irrigation_bool);
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_in_use_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_in_use_bit;
		*psize_of_var_ptr = sizeof(lsystem->capacity_in_use_bool);
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_with_pump_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_with_pump_bit;
		*psize_of_var_ptr = sizeof(lsystem->capacity_with_pump_gpm);
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_without_pump_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_capacity_without_pump_bit;
		*psize_of_var_ptr = sizeof(lsystem->capacity_without_pump_gpm);
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_during_irri_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_during_irri_bit;
		*psize_of_var_ptr = sizeof(lsystem->mlb_during_irri_gpm);
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_during_mvor_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_during_mvor_bit;
		*psize_of_var_ptr = sizeof(lsystem->mlb_during_mvor_gpm);
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_all_other_times_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_mlb_all_other_times_bit;
		*psize_of_var_ptr = sizeof(lsystem->mlb_all_other_times_gpm);
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_in_use_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_in_use_bit;
		*psize_of_var_ptr = sizeof(lsystem->flow_checking_in_use_bool);
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_ranges_bit ], 17 ) == 0 )
	{
		// 1/13/2015 ajv : Note the above condition is not checking the entire length of the field
		// name. This is because there are 3 unique flow checking range values: FlowCheckingRange1 -
		// FlowCheckingRange3. By only comparing the first 17 characters, we can ensure we catch all
		// 3 in one condition.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "3" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}

		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_ranges_bit;
		*psize_of_var_ptr = sizeof(lsystem->flow_checking_ranges_gpm[0]);
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_tolerances_plus_bit ], 17 ) == 0 )
	{
		// 1/13/2015 ajv : Note the above condition is not checking the entire length of the field
		// name. This is because there are 4 unique flow checking tolerance values:
		// FlowTolerancePlus1 - FlowTolerancePlus4. By only comparing the first 17 characters, we
		// can ensure we catch all 4 in one condition.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "3" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if (strstr(pfield_name, "4") != NULL)
		{
			*psecondary_sort_order = 3;
		}

		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_tolerances_plus_bit;
		*psize_of_var_ptr = sizeof(lsystem->flow_checking_tolerances_plus_gpm[0]);
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_tolerances_minus_bit ], 18 ) == 0 )
	{
		// 1/13/2015 ajv : Note the above condition is not checking the entire length of the field
		// name. This is because there are 4 unique flow checking tolerance values:
		// FlowToleranceMinus1 - FlowToleranceMinus4. By only comparing the first 18 characters, we
		// can ensure we catch all 4 in one condition.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "3" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if (strstr(pfield_name, "4") != NULL)
		{
			*psecondary_sort_order = 3;
		}

		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_flow_checking_tolerances_minus_bit;
		*psize_of_var_ptr = sizeof(lsystem->flow_checking_tolerances_minus_gpm[0]);
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_mvor_open_times_bit ], 16 ) == 0 )
	{
		// 9/10/2015 ajv : Note the above condition is not checking the entire length of the field
		// name. This is because there are 7 unique MVOR open times values: MVORScheduleOpen0 - 
		// MVORScheduleOpen6. By only comparing the first 16 characters, we can ensure we catch all
		// 7 in one condition.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "0" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "1" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "2" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "3" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}
		else if( strstr( pfield_name, "4" ) != NULL )
		{
			*psecondary_sort_order = 4;
		}
		else if( strstr( pfield_name, "5" ) != NULL )
		{
			*psecondary_sort_order = 5;
		}
		else if( strstr( pfield_name, "6" ) != NULL )
		{
			*psecondary_sort_order = 6;
		}

		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_mvor_open_times_bit;
		*psize_of_var_ptr = sizeof(lsystem->mvor_open_times[0]);
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_mvor_close_times_bit ], 17 ) == 0 )
	{
		// 9/10/2015 ajv : Note the above condition is not checking the entire length of the field
		// name. This is because there are 7 unique MVOR close times values: MVORScheduleClose0 - 
		// MVORScheduleClose6. By only comparing the first 17 characters, we can ensure we catch all
		// 7 in one condition.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "0" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		else if( strstr( pfield_name, "1" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "2" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "3" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}
		else if( strstr( pfield_name, "4" ) != NULL )
		{
			*psecondary_sort_order = 4;
		}
		else if( strstr( pfield_name, "5" ) != NULL )
		{
			*psecondary_sort_order = 5;
		}
		else if( strstr( pfield_name, "6" ) != NULL )
		{
			*psecondary_sort_order = 6;
		}

		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_mvor_close_times_bit;
		*psize_of_var_ptr = sizeof(lsystem->mvor_close_times[0]);
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_in_use_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_in_use_bit;
		*psize_of_var_ptr = sizeof(lsystem->budget_in_use);
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_meter_read_time_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_meter_read_time_bit;
		*psize_of_var_ptr = sizeof(lsystem->budget_meter_read_time);
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_annual_periods_bit], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_annual_periods_bit;
		*psize_of_var_ptr = sizeof( lsystem->budget_number_of_annual_periods );
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_meter_read_date_bit ], 19 ) == 0 )
	{
		// 9/21/2015 ajv : Note the above condition is not checking the entire length of the field
		// name. This is because there are 24 unique period start values: BudgetPeriodStart0 - 
		// BudgetPeriodStart23. By only comparing the first 17 characters, we can ensure we catch all
		// 24 in one condition.
		// 05/19/2016 skc : Convert string to array index. Yes, this code can certainly be cleaned up.
		// 07/27/2016 skc : Do the search in reverse order, some strings overlap! (1 and 10,11,12 etc)
		if( strstr( pfield_name, "24" ) != NULL )
		{
			*psecondary_sort_order = 23;
		}
		else if( strstr( pfield_name, "23" ) != NULL )
		{
			*psecondary_sort_order = 22;
		}
		else if( strstr( pfield_name, "22" ) != NULL )
		{
			*psecondary_sort_order = 21;
		}
		else if( strstr( pfield_name, "21" ) != NULL )
		{
			*psecondary_sort_order = 20;
		}
		else if( strstr( pfield_name, "20" ) != NULL )
		{
			*psecondary_sort_order = 19;
		}
		else if( strstr( pfield_name, "19" ) != NULL )
		{
			*psecondary_sort_order = 18;
		}
		else if( strstr( pfield_name, "18" ) != NULL )
		{
			*psecondary_sort_order = 17;
		}
		else if( strstr( pfield_name, "17" ) != NULL )
		{
			*psecondary_sort_order = 16;
		}
		else if( strstr( pfield_name, "16" ) != NULL )
		{
			*psecondary_sort_order = 15;
		}
		else if( strstr( pfield_name, "15" ) != NULL )
		{
			*psecondary_sort_order = 14;
		}
		else if( strstr( pfield_name, "14" ) != NULL )
		{
			*psecondary_sort_order = 13;
		}
		else if( strstr( pfield_name, "13" ) != NULL )
		{
			*psecondary_sort_order = 12;
		}
		else if( strstr( pfield_name, "12" ) != NULL )
		{
			*psecondary_sort_order = 11;
		}
		else if( strstr( pfield_name, "11" ) != NULL )
		{
			*psecondary_sort_order = 10;
		}
		else if( strstr( pfield_name, "10" ) != NULL )
		{
			*psecondary_sort_order = 9;
		}
		else if( strstr( pfield_name, "9" ) != NULL )
		{
			*psecondary_sort_order = 8;
		}
		else if( strstr( pfield_name, "8" ) != NULL )
		{
			*psecondary_sort_order = 7;
		}
		else if( strstr( pfield_name, "7" ) != NULL )
		{
			*psecondary_sort_order = 6;
		}
		else if( strstr( pfield_name, "6" ) != NULL )
		{
			*psecondary_sort_order = 5;
		}
		else if( strstr( pfield_name, "5" ) != NULL )
		{
			*psecondary_sort_order = 4;
		}
		else if( strstr( pfield_name, "4" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}
		else if( strstr( pfield_name, "3" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}

		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_meter_read_date_bit;
		*psize_of_var_ptr = sizeof(lsystem->budget_meter_read_date[0]);
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_mode_bit], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_mode_bit;
		*psize_of_var_ptr = sizeof( lsystem->budget_mode );
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[ IRRIGATION_SYSTEM_CHANGE_BITFIELD_in_use_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_in_use_bit;
		*psize_of_var_ptr = sizeof(lsystem->in_use);
	}
	else if( strncmp( pfield_name, SYSTEM_database_field_names[IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_flow_type_bit], 14 ) == 0 )
	{
		if( strstr( pfield_name, "Irrigation" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}
		// 07/27/2016 skc : Make sure to test longer strings first!
		else if( strstr( pfield_name, "ManualProgram" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "Program" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "NA3" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}
		else if( strstr( pfield_name, "Manual" ) != NULL )
		{
			*psecondary_sort_order = 4;
		}
		else if( strstr( pfield_name, "WalkThru" ) != NULL )
		{
			*psecondary_sort_order = 5;
		}
		else if( strstr( pfield_name, "Test" ) != NULL )
		{
			*psecondary_sort_order = 6;
		}
		else if( strstr( pfield_name, "Mobile" ) != NULL )
		{
			*psecondary_sort_order = 7;
		}
		else if( strstr( pfield_name, "MVOR" ) != NULL )
		{
			*psecondary_sort_order = 8;
		}
		else if( strstr( pfield_name, "NonController" ) != NULL )
		{
			*psecondary_sort_order = 9;
		}

		rv = IRRIGATION_SYSTEM_CHANGE_BITFIELD_budget_flow_type_bit;
		*psize_of_var_ptr = sizeof( lsystem->budget_flow_type[0] );
	}

	return( rv );
}

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

