/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/**
 * If you run into issues with a compiler warning that reads "comparison is 
 * always false due to limited range of data type [-Werror=type-limits]", when 
 * checking if an unsigned value is < 0, you can ignore the type-limits warning 
 * by using the following code:
 *
	// If the pMin is 0, the *pValue < pMin expression results in a "comparison
	// is always false due to limited range of data type [-Werror=type-limits]"
	// since pValue is unsigned. To prevent this, ignore the type-limits
	// warning for the following conditional.
	// 
	// Note: This warning is occurs if the -Wextra C compiler option is enabled.
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wtype-limits"
    if( *pValue > pMax )
	{
    	Value = pDefault;
    	lRangeCheckPassed = (false);
    }
    else if( *pValue < pMin )
    {
    	pValue = pDefault;
    	lRangeCheckPassed = (false);
	}
	#pragma GCC diagnostic pop
 
 * Warning: Doing so will break portability if we ever move to a compiler other 
 * than gcc.
 */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"range_check.h"

#include	"ctype.h"

#include	"r_alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*
 * See range_check.h for associated comment headers
 */
#if ALERT_INCLUDE_FILENAME
extern BOOL_32 RC_bool_with_filename( BOOL_32 *const pValue, const BOOL_32 pDefault, const char *const pGroupName, const char *const pVariableName, const char *const pFilename, const UNS_32 pLine )
#else
extern BOOL_32 RC_bool_without_filename( BOOL_32 *const pValue, const BOOL_32 pDefault, const char *const pGroupName, const char *const pVariableName )
#endif
{
	BOOL_32	rv;

	rv = (true);

	if( (*pValue != (true)) && (*pValue != (false)) )
	{
		#ifndef _MSC_VER

			#if ALERT_INCLUDE_FILENAME
				Alert_range_check_failed_bool_with_filename( pVariableName, pGroupName, *pValue, pDefault, CS_FILE_NAME( (char*)pFilename ), pLine );
			#else
				Alert_range_check_failed_bool_without_filename( pVariableName, pGroupName, *pValue, pDefault );
			#endif

		#endif

		// ----------

		// 9/23/2014 ajv : Now that we've stamped what the value was and what it
		// changed to, fix it.
		*pValue = pDefault;

		// ----------
		
		rv = (false);
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/*
 * See range_check.h for associated comment headers
 */
#if ALERT_INCLUDE_FILENAME
extern BOOL_32 RC_date_with_filename( UNS_32 *const pValue, const UNS_32 pMin, const UNS_32 pMax, const UNS_32 pDefault, const char *const pGroupName, const char *const pVariableName, const char *const pFilename, const UNS_32 pLine )
#else
extern BOOL_32 RC_date_without_filename( UNS_32 *const pValue, const UNS_32 pMin, const UNS_32 pMax, const UNS_32 pDefault, const char *const pGroupName, const char *const pVariableName )
#endif
{
	BOOL_32	rv;

	rv = (true);

	if( (*pValue < pMin) || (*pValue > pMax) )
	{
		// 9/23/2014 ajv : Fix it first since we don't show what invalid value
		// was in the alert.
		*pValue = pDefault;

		// ----------

		#ifndef _MSC_VER

			#if ALERT_INCLUDE_FILENAME
				Alert_range_check_failed_date_with_filename( pVariableName, pGroupName, *pValue, CS_FILE_NAME( (char*)pFilename ), pLine );
			#else
				Alert_range_check_failed_date_without_filename( pVariableName, pGroupName, *pValue );
			#endif

		#endif

		// ----------

		rv = (false);
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/*
 * See range_check.h for associated comment headers
 */
#if ALERT_INCLUDE_FILENAME
extern BOOL_32 RC_int32_with_filename( INT_32 *const pValue, const INT_32 pMin, const INT_32 pMax, const INT_32 pDefault, const char *const pGroupName, const char *const pVariableName, const char *const pFilename, const UNS_32 pLine )
#else
extern BOOL_32 RC_int32_without_filename( INT_32 *const pValue, const INT_32 pMin, const INT_32 pMax, const INT_32 pDefault, const char *const pGroupName, const char *const pVariableName )
#endif
{
	BOOL_32	rv;

	rv = (true);

	if( (*pValue < pMin) || (*pValue > pMax) )
	{
		#ifndef _MSC_VER

			#if ALERT_INCLUDE_FILENAME
				Alert_range_check_failed_int32_with_filename( pVariableName, pGroupName, *pValue, pDefault, CS_FILE_NAME( (char*)pFilename ), pLine );
			#else
				Alert_range_check_failed_int32_without_filename( pVariableName, pGroupName, *pValue, pDefault );
			#endif

		#endif

		// ----------

		// 9/23/2014 ajv : Now that we've stamped what the value was and what it
		// changed to, fix it.
		*pValue = pDefault;

		// ----------
		
		rv = (false);
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/*
 * See range_check.h for associated comment headers
 */
#if ALERT_INCLUDE_FILENAME
extern BOOL_32 RC_string_with_filename( char *const pString, const UNS_32 pLength, const char *const pVariableName, const char *const pFilename, const UNS_32 pLine )
#else
extern BOOL_32 RC_string_without_filename( char *const pString, const UNS_32 pLength, const char *const pVariableName )
#endif
{
	const char	INVALID_CHAR_REPLACEMENT = '?'; // 0x3F

	char		*ptr;

	UNS_32		i;

	BOOL_32		rv;

	rv = (true);

	for( i = 0, ptr = pString; ((i < pLength) && (*ptr != 0x00)); ++i, ++ptr )
	{
		if( isprint( *ptr ) == (false) )
		{
			// 9/6/2011 ajv : Since the character's unprintable, replace it with
			// a printable character.
			*ptr = INVALID_CHAR_REPLACEMENT;

			rv = (false);
		}
	}

	if( (i == pLength) && (*ptr != 0x00) )
	{
		// 9/6/2011 ajv : Since we're at the end of the string and there's no
		// NULL terminator was found, force the last character to be a NULL
		// terminator.
		*ptr = 0x00;
	}

	if( rv == (false) )
	{
		#ifndef _MSC_VER

			#if ALERT_INCLUDE_FILENAME
				Alert_range_check_failed_string_with_filename( pVariableName, pString, CS_FILE_NAME( (char*)pFilename ), pLine );
			#else
				Alert_range_check_failed_string_without_filename( pVariableName, pString );
			#endif

		#endif
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/*
 * See range_check.h for associated comment headers
 */
#if ALERT_INCLUDE_FILENAME
extern BOOL_32 RC_time_with_filename( UNS_32 *const pValue, const UNS_32 pMin, const UNS_32 pMax, const UNS_32 pDefault, const char *const pGroupName, const char *const pVariableName, const char *const pFilename, const UNS_32 pLine )
#else
extern BOOL_32 RC_time_without_filename( UNS_32 *const pValue, const UNS_32 pMin, const UNS_32 pMax, const UNS_32 pDefault, const char *const pGroupName, const char *const pVariableName )
#endif
{
	BOOL_32	rv;

	rv = (true);

	if( (*pValue < pMin) || (*pValue > pMax) )
	{
		// 9/23/2014 ajv : Fix it first since we don't show what invalid value
		// was in the alert.
		*pValue = pDefault;

		// ----------

		#ifndef _MSC_VER

			#if ALERT_INCLUDE_FILENAME
				Alert_range_check_failed_time_with_filename( pVariableName, pGroupName, *pValue, CS_FILE_NAME( (char*)pFilename ), pLine );
			#else
				Alert_range_check_failed_time_without_filename( pVariableName, pGroupName, *pValue );
			#endif

		#endif

		// ----------

		rv = (false);
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/*
 * See range_check.h for associated comment headers
 */
#if ALERT_INCLUDE_FILENAME
extern BOOL_32 RC_uint32_with_filename( UNS_32 *const pValue, const UNS_32 pMin, const UNS_32 pMax, const UNS_32 pDefault, const char *const pGroupName, const char *const pVariableName, const char *const pFilename, const UNS_32 pLine )
#else
extern BOOL_32 RC_uint32_without_filename( UNS_32 *const pValue, const UNS_32 pMin, const UNS_32 pMax, const UNS_32 pDefault, const char *const pGroupName, const char *const pVariableName )
#endif
{
	BOOL_32	rv;

	rv = (true);

	if( (*pValue < pMin) || (*pValue > pMax) )
	{
		#ifndef _MSC_VER

			#if ALERT_INCLUDE_FILENAME
				Alert_range_check_failed_uint32_with_filename( pVariableName, pGroupName, *pValue, pDefault, CS_FILE_NAME( (char*)pFilename ), pLine );
			#else
				Alert_range_check_failed_uint32_without_filename( pVariableName, pGroupName, *pValue, pDefault );
			#endif

		#endif

		// ----------

		// 9/23/2014 ajv : Now that we've stamped what the value was and what it
		// changed to, fix it.
		*pValue = pDefault;

		// ----------
		
		rv = (false);
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/*
 * See range_check.h for associated comment headers
 */
#if ALERT_INCLUDE_FILENAME
extern BOOL_32 RC_uns8_with_filename( UNS_8 *const pValue, const UNS_8 pMin, const UNS_8 pMax, const UNS_8 pDefault, const char *const pGroupName, const char *const pVariableName, const char *const pFilename, const UNS_32 pLine )
#else
extern BOOL_32 RC_uns8_without_filename( UNS_8 *const pValue, const UNS_8 pMin, const UNS_8 pMax, const UNS_8 pDefault, const char *const pGroupName, const char *const pVariableName )
#endif
{
	BOOL_32	rv;

	rv = (true);

	if( (*pValue < pMin) || (*pValue > pMax) )
	{
		// 5/13/2014 rmd : I ain't making new alert lines for the UNS_8 case. The UNS_32 lines will
		// work fine, I think.
		#ifndef _MSC_VER

			#if ALERT_INCLUDE_FILENAME
				Alert_range_check_failed_uint32_with_filename( pVariableName, pGroupName, *pValue, pDefault, CS_FILE_NAME( (char*)pFilename ), pLine );
			#else
				Alert_range_check_failed_uint32_without_filename( pVariableName, pGroupName, *pValue, pDefault );
			#endif

		#endif

		// ----------

		// 9/23/2014 ajv : Now that we've stamped what the value was and what it
		// changed to, fix it.
		*pValue = pDefault;

		// ----------
		
		rv = (false);
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/*
 * See range_check.h for associated comment headers
 */
#if ALERT_INCLUDE_FILENAME
extern BOOL_32 RC_float_with_filename( float *const pValue, const float pMin, const float pMax, const float pDefault, const char *const pGroupName, const char *const pVariableName, const char *const pFilename, const UNS_32 pLine )
#else
extern BOOL_32 RC_float_without_filename( float *const pValue, const float pMin, const float pMax, const float pDefault, const char *const pGroupName, const char *const pVariableName )
#endif
{
	BOOL_32	rv;

	rv = (true);

	if( (*pValue < pMin) || (*pValue > pMax) )
	{
		#ifndef _MSC_VER

			#if ALERT_INCLUDE_FILENAME
				Alert_range_check_failed_float_with_filename( pVariableName, pGroupName, *pValue, pDefault, CS_FILE_NAME( (char*)pFilename ), pLine );
			#else
				Alert_range_check_failed_float_without_filename( pVariableName, pGroupName, *pValue, pDefault );
			#endif

		#endif

		// ----------

		// 9/23/2014 ajv : Now that we've stamped what the value was and what it
		// changed to, fix it.
		*pValue = pDefault;

		// ----------
		
		rv = (false);
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

