/*  file = configuration_network.c            11.15.2011 rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy
#include	<string.h>

#include	"configuration_network.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"bithacks.h"

#include	"change.h"

#include	"configuration_controller.h"

#include	"cs_mem.h"

#include	"epson_rx_8025sa.h"

#include	"flash_storage.h"

#include	"flowsense.h"

#include	"foal_comm.h"

#include	"foal_irri.h"

#include	"irri_irri.h"

#include	"range_check.h"

#include	"shared.h"

#include	"shared_configuration_network.c"

#include	"controller_initiated.h"

#include	"watersense.h"

#include	"chain_sync_vars.h"

#include	"chain_sync_funcs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static const char NETWORK_CONFIG_FILENAME[] =	"NETWORK_CONFIGURATION";

const char CONTROLLER_DEFAULT_NAME[] = "This Controller (not yet named)";

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static void network_config_set_default_values( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	UNS_32			network_ID;

	BOOL_32			request_a_full_file_download_from_the_web_app;

	UNS_32			start_of_irrigation_day;

	BOOL_32			scheduled_irrigation_is_off;

	UNS_32			time_zone;

	UNS_32			electrical_limit[ MAX_CHAIN_LENGTH ];

	BOOL_32			dls_in_use;

	DAYLIGHT_SAVING_TIME_STRUCT		dls_spring_ahead;

	DAYLIGHT_SAVING_TIME_STRUCT		dls_fall_back;

	BOOL_32				rain_polling_enabled;

	BOOL_32				send_flow_recording;

	DATE_TIME			no_longer_used_dt;

	UNS_16				padding_16_bit;		// To line up with DATE_TIME field
	UNS_32				padding_32_bit_1;
	UNS_32				padding_32_bit_2;
	UNS_32				padding_32_bit_3;
	UNS_32				padding_32_bit_4;
	UNS_32				padding_32_bit_5;
	UNS_32				padding_32_bit_6;

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

} NETWORK_CONFIGURATION_STRUCT_REV_1;

// ----------

typedef struct
{
	UNS_32			network_ID;

	BOOL_32			request_a_full_file_download_from_the_web_app;

	UNS_32			start_of_irrigation_day;

	BOOL_32			scheduled_irrigation_is_off;

	UNS_32			time_zone;

	UNS_32			electrical_limit[ MAX_CHAIN_LENGTH ];

	BOOL_32			dls_in_use;

	DAYLIGHT_SAVING_TIME_STRUCT		dls_spring_ahead;

	DAYLIGHT_SAVING_TIME_STRUCT		dls_fall_back;

	BOOL_32				rain_polling_enabled;

	BOOL_32				send_flow_recording;

	DATE_TIME			no_longer_used_dt;

	UNS_16				padding_16_bit;		// To line up with DATE_TIME field
	UNS_32				padding_32_bit_1;
	UNS_32				padding_32_bit_2;
	UNS_32				padding_32_bit_3;
	UNS_32				padding_32_bit_4;
	UNS_32				padding_32_bit_5;
	UNS_32				padding_32_bit_6;

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

	char				controller_name[ MAX_CHAIN_LENGTH ][ NUMBER_OF_CHARS_IN_A_NAME ];

} NETWORK_CONFIGURATION_STRUCT_REV_2;

// ----------

typedef struct
{
	UNS_32			network_ID;

	BOOL_32			request_a_full_file_download_from_the_web_app;

	UNS_32			start_of_irrigation_day;

	BOOL_32			scheduled_irrigation_is_off;

	UNS_32			time_zone;

	UNS_32			electrical_limit[ MAX_CHAIN_LENGTH ];

	BOOL_32			dls_in_use;

	DAYLIGHT_SAVING_TIME_STRUCT		dls_spring_ahead;

	DAYLIGHT_SAVING_TIME_STRUCT		dls_fall_back;

	BOOL_32				rain_polling_enabled;

	BOOL_32				send_flow_recording;

	DATE_TIME			no_longer_used_dt;

	UNS_16				padding_16_bit;		// To line up with DATE_TIME field
	UNS_32				padding_32_bit_1;
	UNS_32				padding_32_bit_2;
	UNS_32				padding_32_bit_3;
	UNS_32				padding_32_bit_4;
	UNS_32				padding_32_bit_5;
	UNS_32				padding_32_bit_6;

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

	char				controller_name[ MAX_CHAIN_LENGTH ][ NUMBER_OF_CHARS_IN_A_NAME ];

} NETWORK_CONFIGURATION_STRUCT_REV_3;

// ----------


// 6/23/2015 rmd : REVISION HISTORY

// 6/23/2015 rmd : FROM 5 TO REVISION 6
// This was done to wipe the derate table, the cell iterations, and the flow check station
// cycles count. Because of bugs in the derate table math I could not trust any of the
// learned derate values.

// 10/15/2015 ajv : FROM 6 TO REVISION 7
// We're using this opportunity to initialize a portion of the STATION_PRESERVES record
// since the contents changed due to the moisture balance being added to the Station History
// record. Normally, this would result in a structure size change but I was trying to be
// smart and was able to remove 4-bytes from the end of the structure, compenstating for the
// size difference. Unfortunately, this resulted in the contents after the Station History
// RIP being garbage including Station Report Data RIP, leftover irrigation minutes, and so
// on.
#define	NETWORK_CONFIG_LATEST_FILE_REVISION		(7)

// ----------

const UNS_32 network_config_item_sizes[ NETWORK_CONFIG_LATEST_FILE_REVISION + 1 ] =
{
	// 1/26/2015 rmd : In order for this array to contain OLDER list item sizes the implication
	// is that the older structure definitions are to be retained. That means the newer
	// structure definition has a different name.

	// ----------
	
	sizeof( NETWORK_CONFIGURATION_STRUCT_REV_1 ),

	// 4/23/2015 ajv : Revision 1 is the same size. Needed a variable set. See updater below.
	sizeof( NETWORK_CONFIGURATION_STRUCT_REV_1 ),

	sizeof( NETWORK_CONFIGURATION_STRUCT_REV_2 ),

	// 6/3/2015 ajv : Revisino 3 is the same size. Need to adjust the time zones to support
	// Microsoft Time Zone values.
	sizeof( NETWORK_CONFIGURATION_STRUCT_REV_3 ),

	sizeof( NETWORK_CONFIGURATION_STRUCT ),

	sizeof( NETWORK_CONFIGURATION_STRUCT ),

	// 6/23/2015 rmd : No change to the actual structure when moving from 5 to 6.
	sizeof( NETWORK_CONFIGURATION_STRUCT ),

	// 10/15/2015 ajv : No change to the actual structure when moving from 6 to 7. We're using
	// this opportunity to initialize a portion of the STATION_PRESERVES record since the
	// contents changed due to the moisture balance being added to the Station History record.
	// Normally, this would result in a structure size change but I was trying to be smart and
	// was able to remove 4-bytes from the end of the structure, compenstating for the size
	// difference. Unfortunately, this resulted in the contents after the Station History RIP
	// being garbage including Station Report Data RIP, leftover irrigation minutes, and so on.
	sizeof( NETWORK_CONFIGURATION_STRUCT )
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void network_config_updater( UNS_32 pfrom_revision )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only when
	// the file revision is changed. FYI the network config data structure has no MUTEX.

	// ----------

	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	UNS_32	box_index_0;

	UNS_32  i;

	UNS_32	new_time_zone;

	// 10/15/2015 ajv : After intialization, the station report record has a bogus time and date
	// in it (zeros). Since the record here in the preserves really does exist and potentially
	// will have it's accumulators incremented let's put in the present time and date.
	DATE_TIME	ldt;

	// ----------

	lchange_bitfield_to_set = &config_n.changes_to_send_to_master;

	box_index_0 = FLOWSENSE_get_controller_index();

	EPSON_obtain_latest_time_and_date( &ldt );

	// ----------
	
	if( pfrom_revision == NETWORK_CONFIG_LATEST_FILE_REVISION )
	{
		Alert_Message_va( "NET CONFIG file unexpd update %u", pfrom_revision );	
	}
	else
	{
		Alert_Message_va( "NET CONFIG file update : to revision %u from %u", NETWORK_CONFIG_LATEST_FILE_REVISION, pfrom_revision );

		// ----------
		
		if( pfrom_revision == 0 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 0 to REVISION 1.

			// 2/2/2015 rmd : The REVISION was moved to 1 for the sole purpose of giving us the
			// opportunity to set the upload_to_comm_server distribution bits. Because of revision 0
			// bugs these bits were not set. We need to set them to guarantee all the program data is
			// sent to the comm_server this one time.
			config_n.changes_to_upload_to_comm_server = UNS_32_MAX;
			
			// ----------
			
			// 2/20/2015 rmd : Perform a very SPECIAL activity here. When the file system migrates to
			// the new DIR_ENTRY structure format (which holds the file revision in a new loaction
			// within the structure) there is a quirk that arises. The TPMicro file needs to be
			// RE-WRITTEN to prevent repeated ISP updates.
			//
			// 2/20/2015 rmd : NOTE this line setting this variable could've been included in any of the
			// files that performed a revision # update. The NETWORK CONFIG file just turned out to be
			// the lucky winner.
			//
			// 2/20/2015 rmd : This variable and the associated code can be REMOVED once all controllers
			// out there have been updated to this new file format. That would be after say MARCH 1,
			// 2015.
			weather_preserves.write_tpmicro_file_in_new_file_format = (true);

			// ----------
			
			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 1 to 2 changes if
			// needed.
			pfrom_revision += 1;
		}
		
		// ----------
		
		if( pfrom_revision == 1 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 1 to REVISION 2.
			
			// 2/25/2015 rmd : We have decided that upon a code update it is a good practice to
			// re-register. For one thing that is how the web server updates the code revision the main
			// board is running. The registration carries that information and the database is updated
			// when the reg is received. The code to normally achieve this functionality is tied to the
			// code receipt process. Whent he ACK is sent we set the flag to re-reg. HOWEVER to support
			// updating from older versions of code that do not have this re-reg code in place in the
			// code receipt process we force it here upon this file revision update. Actually this is
			// the only reason for the revision update from revision 1 to 2. (It turns out this re-reg
			// was very inportant to Wissam and the web server when trasitioning off of the test server.
			// Hence this special effort here.)
			CONTROLLER_INITIATED_update_comm_server_registration_info();
			
			// ----------

			pfrom_revision += 1;
		}

		// ----------

		if( pfrom_revision == 2 )
		{
			// 4/23/2015 ajv : This is the work to do when moving from REVISION 2 to REVISION 3.

			// 5/6/2015 ajv : Since we're grabbing 12 48-character arrays for the names, it's important
			// to pre-initialize the memory to NULLs. Then, when strlcpy fills the name, it will NULL
			// terminate the name, but we can be assured the remainder is NULL as well.
			memset( config_n.controller_name, 0x00, sizeof(config_n.controller_name) );

			for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
			{
				NETWORK_CONFIG_set_controller_name( CONTROLLER_DEFAULT_NAME, i, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
			}

			// ----------

			pfrom_revision += 1;
		}

		// ----------

		if( pfrom_revision == 3 )
		{
			// 4/27/2015 ajv : Clear all of the pending Program Data bits.
			config_n.changes_uploaded_to_comm_server_awaiting_ACK = CHANGE_BITS_32_BIT_ALL_CLEAR;

			// ----------

			pfrom_revision += 1;
		}

		if( pfrom_revision == 4 )
		{
			#define tzMOUNTAIN		(3)
			#define tzCENTRAL 		(4)
			#define tzEASTERN		(5)

			// 6/3/2015 ajv : Initialize to prevent a compiler warning.
			new_time_zone = config_n.time_zone;

			// 6/3/2015 ajv : Update the time zone indices now that we have new time zones.
			if( config_n.time_zone == tzMOUNTAIN )
			{
				new_time_zone  = tzMOUNTAIN_STANDARD_TIME;
			}
			else if( config_n.time_zone == tzCENTRAL )
			{
				new_time_zone = tzCENTRAL_STANDARD_TIME;
			}
			else if( config_n.time_zone == tzEASTERN )
			{
				new_time_zone = tzEASTERN_STANDARD_TIME;
			}

			// 6/3/2015 ajv : If there was a change, save it.
			if( new_time_zone != config_n.time_zone )
			{
				NETWORK_CONFIG_set_time_zone( new_time_zone, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
			}

			// ----------

			pfrom_revision += 1;
		}

		if( pfrom_revision == 5 )
		{
			xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

			xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

			// 6/23/2015 rmd : Because of a bug in the code the derate table was unreliable. We used the
			// network revision value to spike the complete derate and flow checking cleaning that
			// follows here.
			for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
			{
				memset( &system_preserves.system[ i ].derate_table_10u, 0, sizeof(system_preserves.system[ i ].derate_table_10u) );
			
				memset( &system_preserves.system[ i ].derate_cell_iterations, 0, sizeof(system_preserves.system[ i ].derate_cell_iterations) );
			}

			for( i=0; i<STATION_PRESERVE_ARRAY_SIZE; i++ )
			{
				station_preserves.sps[ i ].spbf.flow_check_station_cycles_count = 0;
			}
			
			// 6/30/2015 rmd : Furthermore there was a problem with syncing the user settings to the
			// system preserves. Bug. So as part of this update we need to force a sync.
			for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
			{
				if( system_preserves.system[ i ].system_gid )
				{
					SYSTEM_PRESERVES_request_a_resync( system_preserves.system[ i ].system_gid );
				}
			}	

			xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );

			xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
			
			// ----------
			
			pfrom_revision += 1;
		}

		if( pfrom_revision == 6 )
		{
			xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

			// 6/23/2015 rmd : Because of a bug in the code the derate table was unreliable. We used the
			// network revision value to spike the complete derate and flow checking cleaning that
			// follows here.
			for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
			{
				memset( &system_preserves.system[ i ].derate_table_10u, 0, sizeof(system_preserves.system[ i ].derate_table_10u) );

				memset( &system_preserves.system[ i ].derate_cell_iterations, 0, sizeof(system_preserves.system[ i ].derate_cell_iterations) );
			}

			// 10/15/2015 ajv : Because the contents of Station Preserves changed without a size
			// change (due to moving 4-bytes from the bottom of the Preserves structure into the Station
			// History RIP) we need to initialize everything in the record after the Station History
			// RIP.
			for( i=0; i<STATION_PRESERVE_ARRAY_SIZE; i++ )
			{
				// 10/15/2015 ajv : First, initialize the newly added variables (moisture balance percentage
				// and expansion bytes) in the Station History RIP. This was done in the file, but not the
				// battery-backed structure.
				station_preserves.sps[ i ].station_history_rip.pi_moisture_balance_percentage_after_schedule_completes_100u = (INT_16)(WATERSENSE_MAXIMUM_MOISTURE_BALANCE_ALLOWED_FOR_IRRI * 100 * 100);

				station_preserves.sps[ i ].station_history_rip.expansion_u16 = 0;

				// ----------

				nm_init_station_report_data_record( &station_preserves.sps[ i ].station_report_data_rip );

				// 10/15/2015 ajv : Set the current date/time as the record start.
				station_preserves.sps[ i ].station_report_data_rip.record_start_date = ldt.D;

				station_preserves.sps[ i ].station_report_data_rip.record_start_time = ldt.T;

				// ----------

				station_preserves.sps[ i ].skip_irrigation_till_due_to_manual_NOW_time = TIME_DEFAULT;

				station_preserves.sps[ i ].skip_irrigation_till_due_to_calendar_NOW_time = TIME_DEFAULT;

				station_preserves.sps[ i ].skip_irrigation_till_due_to_manual_NOW_date = DATE_MINIMUM;

				station_preserves.sps[ i ].skip_irrigation_till_due_to_calendar_NOW_date = DATE_MINIMUM;

				// ----------
				
				station_preserves.sps[ i ].left_over_irrigation_seconds = 0;

				station_preserves.sps[ i ].rain_minutes_10u = 0;
				
				// ----------

				// 10/15/2015 ajv : Initialize the entire bit field and then explicitiy update specific
				// values within it. The explicit settings are emulated from the
				// init_battery_backed_station_preserves routine.
				memset( &station_preserves.sps[ i ].spbf, 0, sizeof(STATION_PRESERVES_BIT_FIELD) );

				station_preserves.sps[ i ].spbf.flow_status = STATION_PRESERVES_FLOW_STATUS_normal_flow;

				station_preserves.sps[ i ].spbf.i_status = STATION_PRESERVES_I_STATUS_no_error;

				station_preserves.sps[ i ].spbf.did_not_irrigate_last_time = (false);

				// ----------

				station_preserves.sps[ i ].last_measured_current_ma = 0;
			}

			xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );

			// ----------

			pfrom_revision += 1;
		}
	}

	// ----------

	// 1/29/2015 rmd : Well after the updater has run the pfrom_revision is supposed to equal
	// the latest revision. By design of this update process.
	if( pfrom_revision != NETWORK_CONFIG_LATEST_FILE_REVISION )
	{
		Alert_Message( "NET CONFIG updater error" );
	}
}

/* ---------------------------------------------------------- */
extern void init_file_configuration_network( void )
{
	FLASH_FILE_find_or_create_variable_file(	FLASH_INDEX_1_GENERAL_STORAGE,
												NETWORK_CONFIG_FILENAME,
												NETWORK_CONFIG_LATEST_FILE_REVISION,
												&config_n,
												sizeof(NETWORK_CONFIGURATION_STRUCT),
												NULL,  // no MUTEX on this structure
												&network_config_updater,
												&network_config_set_default_values,
												FF_NETWORK_CONFIGURATION );
}

/* ---------------------------------------------------------- */
extern void save_file_configuration_network( void )
{
	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE,
															NETWORK_CONFIG_FILENAME,
															NETWORK_CONFIG_LATEST_FILE_REVISION,
															&config_n,
															sizeof(config_n),
															NULL,
															FF_NETWORK_CONFIGURATION );
}

/* ---------------------------------------------------------- */
/**
 * Processes a "Controller OFF" command. When the user presses the TURN OFF
 * button from a controller, this routine is called as part of the process of
 * saving the "controller off" variable. It stops all ongoing irrigation at the
 * local IRRI machine. The "controller off" variable is then synced and the
 * master and all other slaves run through the same routine to stop their local
 * irrigation. Additionally, the master stops all FOAL-related irrigation
 * activity and sets the appropriate flags in Station History.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user presses the TURN OFF button and within the context of the COMM MNGR
 * task when the "controller off" variable is synced.
 *
 * @author 11/1/2013 AdrianusV
 *
 * @revisions (none)
 */
extern void __turn_scheduled_irrigation_OFF( void )
{
	// 4/5/2013 ajv : Since this is called from both the FOAL masters and slaves when the
	// setting is sync'd, and we ONLY need to do this at the master. So if we're the master
	// clear out the irrigation list.
	if( FLOWSENSE_we_are_a_master_one_way_or_another() )
	{
		FOAL_IRRI_remove_all_stations_from_the_irrigation_lists( ACTION_REASON_TURN_OFF_AND_REMOVE_controller_turned_off );
	}
}

/* ---------------------------------------------------------- */
static void network_config_set_default_values( void )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	UNS_32	i;

	UNS_32	box_index_0;

	box_index_0 = FLOWSENSE_get_controller_index();

	// ----------

	// 6/12/2015 rmd : Initialize the no longer used dead space within the file.
	config_n.unused_a = 0;
	config_n.unused_b = 0;

	config_n.unused_c.D = 0;
	config_n.unused_c.T = 0;

	// 9/28/2018 rmd : Only added zeroing of these 'padding' fields today. So in older
	// controllers variables may not be guaranteed to be 0.
	config_n.padding_16_bit = 0;
	config_n.padding_32_bit_1 = 0;
	config_n.padding_32_bit_2 = 0;
	config_n.padding_32_bit_3 = 0;
	config_n.padding_32_bit_4 = 0;
	config_n.padding_32_bit_5 = 0;
	config_n.padding_32_bit_6 = 0;
	
	// ----------

	// 5/6/2014 ajv : Clear the change bits
	SHARED_clear_all_32_bit_change_bits( &config_n.changes_to_send_to_master, list_program_data_recursive_MUTEX );
	SHARED_clear_all_32_bit_change_bits( &config_n.changes_to_distribute_to_slaves, list_program_data_recursive_MUTEX );
	SHARED_clear_all_32_bit_change_bits( &config_n.changes_uploaded_to_comm_server_awaiting_ACK, list_program_data_recursive_MUTEX );

	// 1/20/2015 ajv : However, we should explicitly set ALL bits for the Comm Server to ensure
	// changes are uploaded on a clean start.
	SHARED_set_all_32_bit_change_bits( &config_n.changes_to_upload_to_comm_server, list_program_data_recursive_MUTEX );

	// ----------

	// 5/2/2014 ajv : When setting the default values, always set the changes to
	// be sent to the master. Once the master receives the changes, it will
	// distribute them back out to the slaves.
	lchange_bitfield_to_set = &config_n.changes_to_send_to_master;

	// ----------
	
	NETWORK_CONFIG_set_network_id( NETWORK_CONFIG_NETWORK_ID_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );

	NETWORK_CONFIG_set_start_of_irri_day( NETWORK_CONFIG_START_OF_IRRIGATION_DAY_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );

	NETWORK_CONFIG_set_scheduled_irrigation_off( (false), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );

	NETWORK_CONFIG_set_time_zone( NETWORK_CONFIG_TIME_ZONE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );

	NETWORK_CONFIG_set_send_flow_recording( (false), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );

	// ---------------------

	DAYLIGHT_SAVING_TIME_STRUCT	dls;

	dls.month = NETWORK_CONFIG_DLS_SPRING_AHEAD_MONTH;
	dls.min_day = NETWORK_CONFIG_DLS_SPRING_AHEAD_MIN_DAY;
	dls.max_day = NETWORK_CONFIG_DLS_SPRING_AHEAD_MAX_DAY;

	NETWORK_CONFIG_set_dls_spring_ahead( dls, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );

	// ---------------------
	
	dls.month = NETWORK_CONFIG_DLS_FALL_BACK_MONTH;
	dls.min_day = NETWORK_CONFIG_DLS_FALL_BACK_MIN_DAY;
	dls.max_day = NETWORK_CONFIG_DLS_FALL_BACK_MAX_DAY;

	NETWORK_CONFIG_set_dls_fall_back( dls, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );

	// ----------

	// 5/6/2015 ajv : Since we're grabbing 12 48-character arrays for the names, it's important
	// to pre-initialize the memory to NULLs. Then, when strlcpy fills the name, it will NULL
	// terminate the name, but we can be assured the remainder is NULL as well.
	memset( config_n.controller_name, 0x00, sizeof(config_n.controller_name) );

	for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
	{
		NETWORK_CONFIG_set_electrical_limit( ELECTRICAL_LIMIT_DEFAULT, i, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );

		NETWORK_CONFIG_set_controller_name( CONTROLLER_DEFAULT_NAME, i, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	}

	NETWORK_CONFIG_set_water_units( NETWORK_CONFIG_WATER_UNIT_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
}

/* ---------------------------------------------------------- */
/**
 * This function runs through the network configuration structure and copies the contents of
 * any changed settings to the passed in ucp.
 * 
 * @executed Executed within the context of the CommMngr task as a token, token response, or
 * Program Data message to the Comm Server is being built.
 * 
 * @param pucp A pointer to the pointer where the program data is being built.
 * 
 * @param pmem_used_so_far The amount of memory used so far. Once this reaches our limit for
 * a single token (MEMORY_TO_ALLOCATE_FOR_SENDING_CHANGES), processing of the remaining
 * changes stops until it's time to build the next token or token response.
 * 
 * @param pmem_used_so_far_is_less_than_allocated_memory A pointer to the flag which
 * indicates whether we're allowed to add more changes to the token or token response.
 * 
 * @param preason_data_is_being_built The reason why the data is being built to send. Based
 * upon this reason, we examine a different change bit structure to determine what to
 * send. Valid values are CHANGE_REASON_SYNC_SENDING_TO_MASTER,
 * CHANGE_REASON_SYNC_DISTRIBUTING_TO_SLAVES, CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER.
 * 
 * @return UNS_32 The total size of the added data. This is used to determine whether any
 * changes were detected so the appropriate file change bit can be set as well as to
 * determine how much memory must be allocated to store the changes.
 *
 * @author Adrianusv
 */
extern UNS_32 NETWORK_CONFIG_build_data_to_send( UNS_8 **pucp,
												 const UNS_32 pmem_used_so_far,
												 BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
												 const UNS_32 preason_data_is_being_built,
												 const UNS_32 pallocated_memory )
{
	CHANGE_BITS_32_BIT	*lbitfield_of_changes_to_use_to_determine_what_to_send;

	CHANGE_BITS_32_BIT	lbitfield_of_changes_to_send;

	UNS_8	*llocation_of_bitfield;

	UNS_32	lsize_of_bitfield;

	UNS_32	rv;

	// ----------

	rv = 0;

	lsize_of_bitfield = sizeof( CHANGE_BITS_32_BIT );

	// ----------

	lbitfield_of_changes_to_use_to_determine_what_to_send = NETWORK_CONFIG_get_change_bits_ptr( preason_data_is_being_built );

	// ----------

	// 1/14/2015 ajv : If any changes are detected...
	if( lbitfield_of_changes_to_use_to_determine_what_to_send != CHANGE_BITS_32_BIT_ALL_CLEAR )
	{
		// 9/19/2013 ajv : Don't bother to try to add any network configuration data if we've
		// already filled out the token with the maximum amount of data to send.
		if( (*pmem_used_so_far_is_less_than_allocated_memory == (true)) && (pmem_used_so_far < pallocated_memory) )
		{
			// Initialize the bit field
			lbitfield_of_changes_to_send = 0;

			rv = PDATA_copy_bitfield_info_into_pucp( pucp,
													 lsize_of_bitfield,
													 sizeof( lbitfield_of_changes_to_send ),
													 &llocation_of_bitfield );

			// ----------

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					NETWORK_CONFIGURATION_CHANGE_BITFIELD_network_id,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&config_n.changes_uploaded_to_comm_server_awaiting_ACK,
					&config_n.network_ID,
					sizeof(config_n.network_ID),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					NETWORK_CONFIGURATION_CHANGE_BITFIELD_start_of_irri_day,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&config_n.changes_uploaded_to_comm_server_awaiting_ACK,
					&config_n.start_of_irrigation_day,
					sizeof(config_n.start_of_irrigation_day),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					NETWORK_CONFIGURATION_CHANGE_BITFIELD_irrigation_off,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&config_n.changes_uploaded_to_comm_server_awaiting_ACK,
					&config_n.scheduled_irrigation_is_off,
					sizeof(config_n.scheduled_irrigation_is_off),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					NETWORK_CONFIGURATION_CHANGE_BITFIELD_time_zone,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&config_n.changes_uploaded_to_comm_server_awaiting_ACK,
					&config_n.time_zone,
					sizeof(config_n.time_zone),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					NETWORK_CONFIGURATION_CHANGE_BITFIELD_dls_spring_ahead,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&config_n.changes_uploaded_to_comm_server_awaiting_ACK,
					&config_n.dls_spring_ahead,
					sizeof(config_n.dls_spring_ahead),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					NETWORK_CONFIGURATION_CHANGE_BITFIELD_dls_fall_back,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&config_n.changes_uploaded_to_comm_server_awaiting_ACK,
					&config_n.dls_fall_back,
					sizeof(config_n.dls_fall_back),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					NETWORK_CONFIGURATION_CHANGE_BITFIELD_electrical_limit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&config_n.changes_uploaded_to_comm_server_awaiting_ACK,
					&config_n.electrical_limit,
					sizeof(config_n.electrical_limit),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					NETWORK_CONFIGURATION_CHANGE_BITFIELD_send_flow_recording,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&config_n.changes_uploaded_to_comm_server_awaiting_ACK,
					&config_n.send_flow_recording,
					sizeof(config_n.send_flow_recording),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					NETWORK_CONFIGURATION_CHANGE_BITFIELD_controller_names,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&config_n.changes_uploaded_to_comm_server_awaiting_ACK,
					&config_n.controller_name,
					sizeof(config_n.controller_name),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					NETWORK_CONFIGURATION_CHANGE_BITFIELD_water_units,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&config_n.changes_uploaded_to_comm_server_awaiting_ACK,
					&config_n.water_units,
					sizeof(config_n.water_units),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			// ----------

			// 1/14/2015 ajv : If changes were detected, we need to copy the bits into location we
			// created above. Otherwise, we need to back out the space allocated and reset the rv to 0.
			if( lbitfield_of_changes_to_send > 0 )
			{
				// 1/14/2015 ajv : Add the actual bit field to the message now that it's been completely
				// filled in.
				memcpy( llocation_of_bitfield, &lbitfield_of_changes_to_send, sizeof(lbitfield_of_changes_to_send) );
			}
			else
			{
				// 1/14/2015 ajv : If we've added the the bit field structure to the message but didn't
				// actually add any data because we hit our limit, back out the bit field data.
				*pucp -= sizeof( lsize_of_bitfield );
				rv -= sizeof( lsize_of_bitfield );

				*pucp -= sizeof( lbitfield_of_changes_to_send );
				rv -= sizeof( lbitfield_of_changes_to_send );
			}
		}
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 NETWORK_CONFIG_get_network_id( void )
{
	return( config_n.network_ID );
}

/* ---------------------------------------------------------- */
extern UNS_32 NETWORK_CONFIG_get_start_of_irrigation_day( void )
{
	UNS_32	rv;

	rv = SHARED_get_uint32( &config_n.start_of_irrigation_day,
							NETWORK_CONFIG_START_OF_IRRIGATION_DAY_MIN,
							NETWORK_CONFIG_START_OF_IRRIGATION_DAY_MAX,
							NETWORK_CONFIG_START_OF_IRRIGATION_DAY_DEFAULT,
							&NETWORK_CONFIG_set_start_of_irri_day,
							&config_n.changes_to_send_to_master,
							NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_start_of_irri_day ] );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 NETWORK_CONFIG_get_controller_off( void )
{
	BOOL_32	rv;

	rv = SHARED_get_bool( &config_n.scheduled_irrigation_is_off,
						  (false),
						  &NETWORK_CONFIG_set_scheduled_irrigation_off,
						  &config_n.changes_to_send_to_master,
						  NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_irrigation_off ] );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 NETWORK_CONFIG_get_time_zone( void )
{
	UNS_32	rv;

	rv = SHARED_get_uint32( &config_n.time_zone,
							tzHAWAIIAN_STANDARD_TIME,
							tzUS_EASTERN_STANDARD_TIME,
							NETWORK_CONFIG_TIME_ZONE_DEFAULT,
							&NETWORK_CONFIG_set_time_zone,
							&config_n.changes_to_send_to_master,
							NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_time_zone ] );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 NETWORK_CONFIG_get_is_dls_used_for_the_selected_time_zone( void )
{
	BOOL_32	rv;

	UNS_32	timezone;

	// ----------
	
	timezone = NETWORK_CONFIG_get_time_zone();

	if( (timezone == tzHAWAIIAN_STANDARD_TIME) || (timezone == tzUS_MOUNTAIN_STANDARD_TIME) || (timezone == tzUS_EASTERN_STANDARD_TIME) )
	{
		rv = (false);
	}
	else
	{
		rv = (true);
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 NETWORK_CONFIG_get_electrical_limit( const UNS_32 pbox_index_0 )
{
	UNS_32	rv;

	char	lfield_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	if( pbox_index_0 < MAX_CHAIN_LENGTH )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_electrical_limit ], (pbox_index_0 + 1) );

		rv = SHARED_get_uint32_from_array( &config_n.electrical_limit[ pbox_index_0 ],
										   pbox_index_0,
										   MAX_CHAIN_LENGTH,
										   ELECTRICAL_LIMIT_MINIMUM,
										   ELECTRICAL_LIMIT_MAXIMUM,
										   ELECTRICAL_LIMIT_DEFAULT,
										   &NETWORK_CONFIG_set_electrical_limit,
										   &config_n.changes_to_send_to_master,
										   lfield_name );
	}
	else
	{
		Alert_index_out_of_range();

		rv = ELECTRICAL_LIMIT_DEFAULT;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern DAYLIGHT_SAVING_TIME_STRUCT *NETWORK_CONFIG_get_dls_ptr( const BOOL_32 pspring_ahead )
{
	// 2/3/2015 ajv : TODO Implement range checking on these values similar to what was done for
	// irrigation system flow checking ranges and tolerances.
#
	if( pspring_ahead == (true) )
	{
		return( &config_n.dls_spring_ahead );
	}
	else
	{
		return( &config_n.dls_fall_back );
	}
}

/* ---------------------------------------------------------- */
extern BOOL_32 NETWORK_CONFIG_get_send_flow_recording( void )
{
	BOOL_32	rv;

	rv = SHARED_get_bool( &config_n.send_flow_recording,
						  (false),
						  &NETWORK_CONFIG_set_send_flow_recording,
						  &config_n.changes_to_send_to_master,
						  NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_send_flow_recording ] );

	return( rv );
}

/* ---------------------------------------------------------- */
extern char *NETWORK_CONFIG_get_controller_name( char *pcontroller_name, const UNS_32 psize_of_name, const UNS_32 pbox_index_0 )
{
	strlcpy( pcontroller_name, config_n.controller_name[ pbox_index_0 ], psize_of_name );

	return( pcontroller_name );
}

/* ---------------------------------------------------------- */
extern char *NETWORK_CONFIG_get_controller_name_str_for_ui( const UNS_32 pbox_index_0, char *guivar_ptr )
{
	// 10/21/2013 ajv : If there is no controller name (i.e., the string is empty), show the
	// controller's FLOWSENSE address/scan character.
	if( (strncmp(config_n.controller_name[ pbox_index_0 ], CONTROLLER_DEFAULT_NAME, NUMBER_OF_CHARS_IN_A_NAME) == 0) || (config_n.controller_name[ pbox_index_0 ][ 0 ] == 0x00) )
	{
		if( FLOWSENSE_we_are_poafs() == (true) )
		{
			snprintf( guivar_ptr, NUMBER_OF_CHARS_IN_A_NAME, "%s %c", GuiLib_GetTextPtr( GuiStruct_txtController_0, 0 ), (pbox_index_0 + 'A') );
		}
		else
		{
			strlcpy( guivar_ptr, GuiLib_GetTextPtr( GuiStruct_txtThisController_0, 0 ), NUMBER_OF_CHARS_IN_A_NAME );
		}
	}
	else
	{
		strlcpy( guivar_ptr, config_n.controller_name[ pbox_index_0 ], NUMBER_OF_CHARS_IN_A_NAME );
	}
	
	return( guivar_ptr );
}

/* ---------------------------------------------------------- */
// 6/8/2016 skc : Users can display water in units of gallons(0) or HCF(1)
extern UNS_32 NETWORK_CONFIG_get_water_units( void )
{
	UNS_32	rv;

	rv = SHARED_get_uint32( &config_n.water_units,
							NETWORK_CONFIG_WATER_UNIT_GALLONS,
							NETWORK_CONFIG_WATER_UNIT_HCF,
							NETWORK_CONFIG_WATER_UNIT_DEFAULT,
							&NETWORK_CONFIG_set_water_units,
							&config_n.changes_to_send_to_master,
							NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_water_units ] );

	return( rv );
}

/* ---------------------------------------------------------- */
extern CHANGE_BITS_32_BIT *NETWORK_CONFIG_get_change_bits_ptr( const UNS_32 pchange_reason )
{
	return( SHARED_get_32_bit_change_bits_ptr( pchange_reason, &config_n.changes_to_send_to_master, &config_n.changes_to_distribute_to_slaves, &config_n.changes_to_upload_to_comm_server ) );
}

/* ---------------------------------------------------------- */
extern void NETWORK_CONFIG_set_bits_on_all_settings_to_cause_distribution_in_the_next_token( void )
{
	// 5/1/2014 rmd : This function ONLY being called at the conclusion of a SCAN by the master
	// of a real multiple controller chain - we want to broadcast to the slaves using tokens
	// about all the hardware we presently know about.
	SHARED_set_all_32_bit_change_bits( &config_n.changes_to_distribute_to_slaves, list_program_data_recursive_MUTEX );

	// ----------

	// 4/29/2014 ajv : Set a flag in the changes structure to notify this
	// controller to examine its lists for changes to distribute to the other
	// controllers in the chain in it's next token resposne. The flag is part of
	// the mutex protected comm_mngr struct so take the MUTEX.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	comm_mngr.changes.distribute_changes_to_slave = (true);

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void NETWORK_CONFIG_on_all_settings_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear )
{
	if( pset_or_clear == COMMSERVER_CHANGE_BITS_ALL_CLEARED )
	{
		SHARED_clear_all_32_bit_change_bits( &config_n.changes_to_upload_to_comm_server, list_program_data_recursive_MUTEX );
	}
	else
	{
		SHARED_set_all_32_bit_change_bits( &config_n.changes_to_upload_to_comm_server, list_program_data_recursive_MUTEX );
	}
}

/* ---------------------------------------------------------- */
/**
 * @mutex_requirements Requires the caller to have the pending_changes_recursive_MUTEX to
 * ensure no other function tries to modify the pending_changes_to_send_to_comm_server flag.
 *
 * @task_execution Executed within the context of the CONTROLLER INITIATED task when it
 * first starts and when the waiting response flags are cleared after a communication.
 */
extern void nm_NETWORK_CONFIG_update_pending_change_bits( const UNS_32 pcomm_error_occurred )
{
	// 4/30/2015 ajv : No reason to process this if none of the bits are set. This also helps
	// reduce the file save activity.
	if( config_n.changes_uploaded_to_comm_server_awaiting_ACK != CHANGE_BITS_32_BIT_ALL_CLEAR )
	{
		// 4/29/2015 ajv : If a communication error occurred while sending Program Data, copy the
		// bits of variables that were sent back into the original bit field to ensure they're sent
		// again during the next attempt. Otherwise, clear the pending bits.
		if( pcomm_error_occurred )
		{
			xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

			config_n.changes_to_upload_to_comm_server |= config_n.changes_uploaded_to_comm_server_awaiting_ACK;

			xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

			// 4/30/2015 ajv : Since we've determined there were changes that were not successfully
			// sent, set the flag to trigger another attempt
			weather_preserves.pending_changes_to_send_to_comm_server = (true);

			// 4/30/2015 ajv : Also set the timer to force the data to send. If the timer's already
			// running, this will simply restart it.
			// 
			// 4/30/2015 ajv : BECAUSE of the potential that this function is executed many times (once
			// for each item changed) we could not start our pdata timer with a post to the CI queue.
			// The queue would overflow. So start the timer directly.
			xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGES_NOT_SENT_MS ), portMAX_DELAY );
		}
		else
		{
			SHARED_clear_all_32_bit_change_bits( &config_n.changes_uploaded_to_comm_server_awaiting_ACK, list_program_data_recursive_MUTEX );
		}

		// 4/29/2015 ajv : Now that the bits are set or cleared accordingly, trigger a file save.
		// If power fails between building the message and receiving the ACK, the original bit field
		// hasn't been saved to flash yet, so it will resend the associated variables during the
		// next attempt.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_NETWORK_CONFIGURATION, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
	}
}

/* ---------------------------------------------------------- */
void NETWORK_CONFIG_brute_force_set_network_ID_to_zero( void )
{
	// 6/19/2018 rmd : To support panel swap, without using the formal set function, because
	// that goes through a whole formal process, wrapping the change up in a token, sending from
	// the IRRI side to the FOAL side, and then distributing the change before finally saving
	// the change. We want to circumvent all of that and get right at the NID and step on it to
	// support the pending controller reset the caller is going to do! lso decided to not use
	// the MUTEX either - this is an atomic operation that will correctly complete.
	config_n.network_ID = 0;
}

/* ---------------------------------------------------------- */
BOOL_32 NETWORK_CONFIG_calculate_chain_sync_crc( UNS_32 const pff_name )
{
	// 8/30/2018 rmd : Executed in the context of the comm_mngr task when token responses are
	// being made. And that's it.

	// ----------

	UNS_8				*checksum_start;

	UNS_32				checksum_length;

	UNS_8				*ucp;
	
	BOOL_32				rv;

	// ----------

	rv = (false);

	// ----------
	
	// 9/28/2018 rmd : Normally we would take the MUTEX for the structure here, but there
	// doesn't seem to be one for the network config structure!
	
	// ----------

	// 8/30/2018 rmd : So the CRC calculation function wants a continuous memory space and a
	// length to calculate the crc off. In this function we will pick out the desired data, and
	// simply pile it up on itself, list item by list item. We don't care how messy the pile is
	// because we never attempt to reference any data once it is added to the pile. The data is
	// only there to include in the checksum.
	//
	// 9/27/2018 rmd : In this case the weather control is a single variable, no list involved.
	// So handle appropriately.
	if( mem_obtain_a_block_if_available( sizeof(NETWORK_CONFIGURATION_STRUCT), (void**)&checksum_start ) )
	{
		// 9/5/2018 rmd : Well then the calculation will be successful. Nothing else can prevent
		// that outcome.
		rv = (true);
		
		// ----------
		
		ucp = checksum_start;
		
		checksum_length = 0;
		
		// ----------
		
		// 9/4/2018 rmd : Make sure you include only TRUE USER SETTINGS, no flags, or pointers to
		// memory, etc...
		
		// ----------
		
		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &config_n.network_ID, sizeof(config_n.network_ID) );

		// skipping unused_a

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &config_n.start_of_irrigation_day, sizeof(config_n.start_of_irrigation_day) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &config_n.scheduled_irrigation_is_off, sizeof(config_n.scheduled_irrigation_is_off) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &config_n.time_zone, sizeof(config_n.time_zone) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &config_n.electrical_limit, sizeof(config_n.electrical_limit) );
		
		// skipping unused_b

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &config_n.dls_spring_ahead, sizeof(config_n.dls_spring_ahead) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &config_n.dls_fall_back, sizeof(config_n.dls_fall_back) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &config_n.water_units, sizeof(config_n.water_units) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &config_n.send_flow_recording, sizeof(config_n.send_flow_recording) );

		// skipping unused_c, and 7 padding fields
		
		// skipping 3 change bits fields

		// ----------

		UNS_32	ccc;
		
		for( ccc=0; ccc<MAX_CHAIN_LENGTH; ccc++ )
		{
			// 10/1/2018 rmd : NOTE - only include up to the first NULL terminate in the controller name
			// string. That is all we can count upon to be sync'd with the master. This is because of
			// the way the sync data is written to this string - only up to first NULL. This is unlike
			// the 'description' field in 'list file' data, in that case we do a blind memcpy of the
			// full 48 characters. I din't want to change the network configuration string method
			// because it felt too complicated to find exactly where the offense was. And this approach
			// solved the problem (the network config continually failed its crc sync test).
			checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &config_n.controller_name, strlen( config_n.controller_name[ ccc ] ) );
		}

		// ----------
		
		// skipping a change bits field

		// END
		
		// ----------
		
		// 8/30/2018 rmd : Now calculate the checksum.
		cscs.the_crc[ pff_name ] = CRC_calculate_32bit_big_endian( checksum_start, checksum_length );
		
		// ----------
		
		// 9/4/2018 rmd : We're done with the block, so of course free it.
		mem_free( checksum_start );
	}
	else
	{
		// 9/5/2018 rmd : So by design it's okay if there is no memory. The checksum won't be set
		// valid, and the next time we try to include it in a token response we'll try to calculate
		// it again. Self protecting and retrying. But let's alert about this so we know it's taking
		// place.
		Alert_Message_va( "SYNC: no mem to calc checksum %s", chain_sync_file_pertinants[ pff_name ].file_name_string );
	}
	
	// ----------
	
	// 9/28/2018 rmd : Normally we would give the MUTEX for the structure here, but we didn't
	// take one to begin with, because there doesn't seem to be one for the network config
	// structure!
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

