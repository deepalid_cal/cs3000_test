/*  file = configuration_network.h            11.15.2011 rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_CONFIGURATION_NETWORK_H
#define _INC_CONFIGURATION_NETWORK_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"cal_td_utils.h"

#include	"pdata_changes.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define tzHAWAIIAN_STANDARD_TIME		(0) // Hawaii - DOES NOT OBSERVE DLS
#define tzALASKAN_STANDARD_TIME			(1)
#define tzPACIFIC_STANDARD_TIME			(2)
#define tzUS_MOUNTAIN_STANDARD_TIME		(3)	// Arizona - DOES NOT OBSERVE DLS
#define tzMOUNTAIN_STANDARD_TIME		(4)
#define tzCENTRAL_STANDARD_TIME 		(5)
#define tzEASTERN_STANDARD_TIME			(6)
#define tzUS_EASTERN_STANDARD_TIME		(7)	// Indiana (East) - DOES NOT OBSERVE DLS

// ----------

#define	NETWORK_CONFIG_NETWORK_ID_MIN					(0)
#define	NETWORK_CONFIG_NETWORK_ID_MAX					(UNS_32_MAX)
#define	NETWORK_CONFIG_NETWORK_ID_DEFAULT				(0)

// ----------

#define	NETWORK_CONFIG_START_OF_IRRIGATION_DAY_MIN		(0)		// 12:00 AM
#define	NETWORK_CONFIG_START_OF_IRRIGATION_DAY_MAX		(85800)	// 11:50 PM
#define	NETWORK_CONFIG_START_OF_IRRIGATION_DAY_DEFAULT	(72000)	// 8:00 PM

// ----------

#define	NETWORK_CONFIG_TIME_ZONE_DEFAULT				(tzPACIFIC_STANDARD_TIME)

// ----------

#define NETWORK_CONFIG_DLS_SPRING_AHEAD_MONTH			(3)		// March
#define NETWORK_CONFIG_DLS_SPRING_AHEAD_MIN_DAY			(7)		// Second week
#define NETWORK_CONFIG_DLS_SPRING_AHEAD_MAX_DAY			(15)	// Second week

#define NETWORK_CONFIG_DLS_FALL_BACK_MONTH				(11)	// November
#define NETWORK_CONFIG_DLS_FALL_BACK_MIN_DAY			(0)		// First week
#define NETWORK_CONFIG_DLS_FALL_BACK_MAX_DAY			(8)		// Second week

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern const char CONTROLLER_DEFAULT_NAME[];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	UNS_32	month;

	UNS_32	min_day;

	UNS_32	max_day;

} DAYLIGHT_SAVING_TIME_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define NETWORK_CONFIG_WATER_UNIT_GALLONS	(0)
#define NETWORK_CONFIG_WATER_UNIT_HCF		(1)

// 3/9/2017 rmd : I changed this from HCF to Gallons to match what we have done in past for
// all prior generation of controllers. CS3000 code was released and shipped with the
// default as HCF. So my guess is several hundred controllers shipped with the default units
// forced to HCF. But going forward the default as shipped will be GALLONS.
#define NETWORK_CONFIG_WATER_UNIT_DEFAULT	(NETWORK_CONFIG_WATER_UNIT_GALLONS)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct NETWORK_CONFIGURATION_STRUCT	NETWORK_CONFIGURATION_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_file_configuration_network( void );

extern void save_file_configuration_network( void );

// ----------

extern void __turn_scheduled_irrigation_OFF( void );

// ----------

extern UNS_32 NETWORK_CONFIG_build_data_to_send( UNS_8 **pucp,
												 const UNS_32 pmem_used_so_far,
												 BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
												 const UNS_32 preason_data_is_being_built,
												 const UNS_32 pallocated_memory );

extern UNS_32 NETWORK_CONFIG_set_controller_name( const char *pcontroller_name,
												  const UNS_32 pcontroller_index_0,
												  const BOOL_32 pgenerate_change_line,
												  const UNS_32 preason_for_change,
												  const UNS_32 pbox_index_0,
												  const BOOL_32 pset_change_bits,
												  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , char *pmerge_update_str,
												  char *pmerge_insert_fields_str,
												  char *pmerge_insert_values_str
												  #endif
												);

extern void NETWORK_CONFIG_set_water_units( UNS_32 punits,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0,
											const BOOL_32 pset_change_bits,
											CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											#ifdef _MSC_VER
											, char *pmerge_update_str,
											char *pmerge_insert_fields_str,
											char *pmerge_insert_values_str
											#endif
										  );

extern UNS_32 NETWORK_CONFIG_extract_and_store_changes_from_comm( const UNS_8 *pucp,
																  const UNS_32 preason_for_change,
																  const BOOL_32 pset_change_bits,
																  const UNS_32 pchanges_received_from
																  #ifdef _MSC_VER
																  , char *pSQL_statements,
																  char *pSQL_search_condition_str,
																  char *pSQL_update_str,
																  char *pSQL_insert_fields_str,
																  char *pSQL_insert_values_str,
																  char *pSQL_single_merge_statement_buf,
																  const UNS_32 pnetwork_ID
																  #endif
																 );

// ----------

#ifdef _MSC_VER

extern INT_32 NETWORK_CONFIG_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order );

#endif

// ----------

extern UNS_32 NETWORK_CONFIG_set_network_id( UNS_32 pnetwork_id,
											 const BOOL_32 pgenerate_change_line_bool,
											 const UNS_32 preason_for_change,
											 const UNS_32 pbox_index_0,
											 const BOOL_32 pset_change_bits,
											 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											 #ifdef _MSC_VER
											 , char *pmerge_update_str,
											 char *pmerge_insert_fields_str,
											 char *pmerge_insert_values_str
											 #endif
										   );
										   
extern void NETWORK_CONFIG_set_scheduled_irrigation_off( BOOL_32 pturn_irrigation_off,
														   const BOOL_32 pgenerate_change_line_bool,
														   const UNS_32 preason_for_change,
														   const UNS_32 pbox_index_0,
														   const BOOL_32 pset_change_bits,
														   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
														   #ifdef _MSC_VER
														   , char *pmerge_update_str,
														   char *pmerge_insert_fields_str,
														   char *pmerge_insert_values_str
														   #endif
														 );

extern void NETWORK_CONFIG_set_time_zone( UNS_32 ptime_zone,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0,
											const BOOL_32 pset_change_bits,
											CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											#ifdef _MSC_VER
											, char *pmerge_update_str,
											char *pmerge_insert_fields_str,
											char *pmerge_insert_values_str
											#endif
										  );

extern void NETWORK_CONFIG_set_send_flow_recording( BOOL_32 psend_flow_recording,
													  const BOOL_32 pgenerate_change_line_bool,
													  const UNS_32 preason_for_change,
													  const UNS_32 pbox_index_0,
													  const BOOL_32 pset_change_bits,
													  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													  #ifdef _MSC_VER
													  , char *pmerge_update_str,
													  char *pmerge_insert_fields_str,
													  char *pmerge_insert_values_str
													  #endif
													);

// ----------

extern UNS_32 NETWORK_CONFIG_get_network_id( void );

extern UNS_32 NETWORK_CONFIG_get_start_of_irrigation_day( void );

extern BOOL_32 NETWORK_CONFIG_get_controller_off( void );

extern UNS_32 NETWORK_CONFIG_get_time_zone( void );

extern BOOL_32 NETWORK_CONFIG_get_is_dls_used_for_the_selected_time_zone( void );

extern UNS_32 NETWORK_CONFIG_get_electrical_limit( const UNS_32 pbox_index_0 );

extern DAYLIGHT_SAVING_TIME_STRUCT *NETWORK_CONFIG_get_dls_ptr( const BOOL_32 pspring_ahead );

extern BOOL_32 NETWORK_CONFIG_get_send_flow_recording( void );

extern char *NETWORK_CONFIG_get_controller_name( char *pcontroller_name, const UNS_32 psize_of_name, const UNS_32 pbox_index_0 );

extern char *NETWORK_CONFIG_get_controller_name_str_for_ui( const UNS_32 pbox_index_0, char *guivar_ptr );

extern UNS_32 NETWORK_CONFIG_get_water_units( void );

// ----------

extern CHANGE_BITS_32_BIT *NETWORK_CONFIG_get_change_bits_ptr( const UNS_32 pchange_reason );

extern void NETWORK_CONFIG_set_bits_on_all_settings_to_cause_distribution_in_the_next_token( void );

extern void NETWORK_CONFIG_on_all_settings_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear );

extern void nm_NETWORK_CONFIG_update_pending_change_bits( const UNS_32 pcomm_error_occurred );


extern void NETWORK_CONFIG_brute_force_set_network_ID_to_zero( void );


extern BOOL_32 NETWORK_CONFIG_calculate_chain_sync_crc( UNS_32 const pff_name );



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

