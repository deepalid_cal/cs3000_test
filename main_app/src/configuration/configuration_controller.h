/*  file = configuration_controller.h         11.15.2011 rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_CONFIGURATION_CONTROLLER_H
#define _INC_CONFIGURATION_CONTROLLER_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	"lpc_types.h"

#include	"cal_string.h"

// 6/25/2014 ajv : Required for UPORT_TOTAL_SYSTEM_PORTS
#include	"gpio_setup.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2011.10.20 rmd : The ET2000e ranges from 30000 to 49999. As of this date we are at 37000.
// The handheld RRe-TRAN's are in the 20000 to 29999. Presently at 21700. So I've penciled
// in the CS3000 starting at 50,000 and ending at 99,999. We store the CS3000 serial number
// as an UNS_32 value. However keep in mind we do send the serial number as a destination
// address with the packets as a 3 byte value.

#define CONTROLLER_MINIMUM_SERIAL_NUMBER	(50000)

#define CONTROLLER_MAXIMUM_SERIAL_NUMBER	(99999)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
typedef struct
{
	union
	{
		UNS_16		size_of_the_union;

		struct
		{
			// NOTE: This is the LS bit in GCC.

			// ----------
			
			BITFIELD_BOOL		option_FL				: 1;

			// ----------
			
			// 9/10/2013 ajv : To help users quickly identify what type of
			// enclosure the controller is installed (often useful when
			// troubleshooting issues over the phone), track whether the
			// controller is in an SSE or SSE-D. If no enclosure type is listed,
			// it's assumed to be in a standard gray powered-coated box.
			BITFIELD_BOOL		option_SSE				: 1;
			BITFIELD_BOOL		option_SSE_D			: 1;

			// ----------
			
			// 6/28/2017 rmd : This is the purchased option bit for the HUB capability. It should NEVER
			// be referenced directly to determine "is this controller a hub?". There is CONFIG function
			// that is to be used to do that: CONFIG_this_controller_is_a_configured_hub().
			BITFIELD_BOOL		option_HUB				: 1;

			// ----------
			
			// 9/15/2017 rmd : The next 8 bits are not actually purchased options, but some details of
			// the radios on the ports. This is useful information for the commserver when optimizing
			// the retry timing for 2000e's behind a hub. We also use this information when setting the
			// packet rate during code distribution.
			UNS_32				port_a_raveon_radio_type				: 2;  // SONIK, M5, or M7 (not valid unless config.port_A_device_index is COMM_DEVICE_LR_RAVEON) 
			BITFIELD_BOOL		port_a_freewave_lr_set_for_repeater		: 1;  // yes or no
			BITFIELD_BOOL		port_a_freewave_sr_set_for_repeater		: 1;  // yes or no

			UNS_32				port_b_raveon_radio_type				: 2;  // SONIK, M5, or M7 (not valid unless config.port_B_device_index is COMM_DEVICE_LR_RAVEON)
			BITFIELD_BOOL		port_b_freewave_lr_set_for_repeater		: 1;  // yes or no
			BITFIELD_BOOL		port_b_freewave_sr_set_for_repeater		: 1;  // yes or no

			// ----------
			
			BITFIELD_BOOL		option_AQUAPONICS		: 1;
			BITFIELD_BOOL		unused_13				: 1;
			BITFIELD_BOOL		unused_14				: 1;
			BITFIELD_BOOL		unused_15				: 1;

		};

	};

} PURCHASED_OPTIONS_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	union
	{
		UNS_16		size_of_the_union;

		struct
		{
			// NOTE: This is the LS bit in GCC.

			BITFIELD_BOOL		transport_om_show_unexpected_behavior_alerts	: 1;

			// 2000e AJ : This alert should default ON so field service can see it occuring and actively
			// make the decision to improve communications or suppress the alert. I believe this should
			// ONLY be used to suppress the alert in conjunction with an SR radio that is running the
			// FLOWSENSE chain traffic.
			BITFIELD_BOOL		transport_om_show_status_timer_expired_alert	: 1;



			BITFIELD_BOOL		use_new_k_and_offset_numbers					: 1;

			BITFIELD_BOOL		show_flow_table_interaction						: 1;

		};

	};

} DEBUG_BITS_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*					
1.	Packets sent out this port must wait for the token. 

2.  Send flowsense packets out this port.

3.	Allow packets to the central to pass out this port. This implies there is a link to the central down the line.

4.	I'm a straggler ... because this determines how he responds to the scan ping.

5.	"No more controllers between this port and the central". This question is focused on incoming messages. If the
	port a cent message is from is a direct shot back to the central it may be processed right away i.e. do not
	have to wait for the token. An example of this is UPORT_USB. Also a port with an LR radio on it.

6.  Allow packets from the central to go out this port	


7. What to do with packets from the central. Present to each port to consider if if wants to take a copy and xmit it.

	Allow packets to Central to be routed
	
	Allow packets from Central to be routed
	
	FLOWSENSE packets should be sent out this port?  YES or NO?
	
	Drop all packets received (used if an SR radio is installed and acting soley as a repeater).  YES or NO?
	
	Packets to the central going out this port have to wait for the token.  YES or NO?
														
*/ 

// ----------

// 6/19/2013 rmd : The Port A and B device types. Used to customize any of the
// signal/programming behaviors of the port activites. ONLY for devices on ports A & B.

#define	COMM_DEVICE_NONE_PRESENT	(0)		// needed so we don't try to do something (like program) a device that isn't there

#define	COMM_DEVICE_LR_RAVEON		(1)		// 460MHz radio using Raveon StingRay or FireLine

#define	COMM_DEVICE_LR_FREEWAVE		(2)		// 460MHz radio using Freewave LRS455

#define	COMM_DEVICE_SR_FREEWAVE		(3)		// 900MHz spread spectrum using Freewave FGR2

#define	COMM_DEVICE_EN_UDS1100		(4)		// Ethernet using Lantronix UDS1100-IAP

#define	COMM_DEVICE_GR_AIRLINK		(5)		// 3G cellular using Sierra Wireless Airlink LS300

#define COMM_DEVICE_WEN_PREMIERWAVE	(6)		// Wi-Fi using Lantronix PremierWave XN

#define COMM_DEVICE_GR_PREMIERWAVE	(7)		// 3.5G cellular using Lantronix PremierWave XC HSPA+

#define COMM_DEVICE_EN_PREMIERWAVE	(8)		// Ethernet using Lantronix PremierWave XE

#define COMM_DEVICE_WEN_WIBOX		(9)		// Wi-Fi using Lantronix WiBox

#define COMM_DEVICE_MULTITECH_LTE	(10)	// MultiTech MTSMC-LAT1 LTE cell unit

// 9/10/2013 rmd : This define is needed during error protection when reading the analog
// voltage from the device interface card. Without this define we would need to expand our
// arrays below to the maximum number of devices. Which is 28 by the way.
//
// 8/8/2014 rmd : NOTE - when you increase this array index there are several implications.
// First of all we have a new device we are working with. We need to identify the
// communication card resistor value in the 'resistor scheme' spreadsheet. Second we should
// add two new files for the device control. These are the 'device' files. And thirdly there
// is an array of descriptive strings below as you can see and in EASY-GUI that needs to be
// set.
#define	NUMBER_OF_DEFINED_DEVICES	(11)

// ----------

typedef struct
{
	// ----------

	// 8/28/2013 rmd : These settings are not user editable. They are fixed behaviours developed
	// for the device by engineering during integration.
	
	// ----------

	// 10/26/2016 rmd : This device on PORT A gives the controller the Enables
	BOOL_32		on_port_A_enables_controller_to_make_CI_messages;
	
	// ----------

	UNS_32		baud_rate;

	// ----------------
	// INPUTS
	// ----------------

	// 9/27/2012 rmd : CTS is implemented as a GPIO inputs. When the signal is read this is the
	// FINAL bit level returned by the read. So this is the bit level reported by the GPIO state
	// register.
	UNS_32		cts_when_OK_to_send;

	// 7/30/2013 rmd : The GPI state level when the device is connected. Only used on ports A &
	// B.
	UNS_32		cd_when_connected;

	UNS_32		ri_polarity;

	// ----------------
	// OUTPUTS
	// ----------------

	// 6/19/2013 rmd : Self explanatory. Once set, don't know why the application would ever
	// change this level to the device. We are supposed to ALWAYS be ready to accept more data.
	UNS_32		rts_level_to_cause_device_to_send;

	UNS_32		dtr_level_to_connect;
	
	// 6/19/2013 rmd : The level to put the device into its reset state.
	UNS_32		reset_active_level;
	
	// ----------

	// 6/28/2013 rmd : The function that can be called to power up or down the device on the
	// port.
	void		(*__power_device_ptr)( UNS_32 pport, BOOL_32 pon_or_off );
	
	// ----------

	// 6/28/2013 rmd : The function called prior to the connection process.
	void		(*__initialize_the_connection_process)( UNS_32 pport );

	// 6/28/2013 rmd : The process executed to cause a connection.
	void		(*__connection_processing)( UNS_32 pevent );
	
	// ----------
	
	// 7/25/2013 rmd : The function called to see if the device is presently connected.
	BOOL_32		(*__is_connected)( UNS_32 pport );

	// ----------

	// 3/13/2014 rmd : The two functions used to manage the device programming or settings
	// read-out sequence.
	void		(*__initialize_device_exchange)( void );

	// 3/18/2014 rmd : Had to use a void ptr instead of COMM_MNGR_EVENT_QUEUE_STRUCT to get it
	// to compile (circular reference).
	void		(*__device_exchange_processing)( void *pq_msg );

} PORT_DEVICE_SETTINGS_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	union
	{
		UNS_32		size_of_the_union;

		struct
		{
			// NOTE: This is the LS bit in GCC.

			BITFIELD_BOOL		nlu_bit_0					: 1;
			BITFIELD_BOOL		nlu_bit_1					: 1;
			BITFIELD_BOOL		nlu_bit_2					: 1;
			BITFIELD_BOOL		nlu_bit_3					: 1;
			BITFIELD_BOOL		nlu_bit_4					: 1;


			// 2012.03.07 rmd : The default is to show crc errors. However this can be a nusiance alert
			// in some cases. For example an LR radio which is receiving packets from other controllers.
			// Packets that would be headed to the HUB. The RF link is not tested for this pathway and
			// in some cases the result is we get bits and pieces of packets.
			BITFIELD_BOOL		alert_about_crc_errors		: 1;

		};
	};

} CONFIGURATION_PORT_CONTROL_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
typedef struct
{
	// 3/25/2014 rmd : NOTE - the contents of this structure make up the configuration file and
	// is designed to hold variables not normally touched by the end user. Therefore this
	// cluster of variables is held together in a file stored in FLASH_0. This flash is
	// physically a separate flash chip and the file system within is rarely written to. It
	// holds only this file and the code image files (so far). The argument being less file
	// manipulation should be a more robust storage location.

	// ----------

	// 10/26/2016 rmd : The controller name has been moved to the NETWORK file. This is a bunch
	// of available bytes.
	char						nlu_controller_name[ NUMBER_OF_CHARS_IN_A_NAME ];
	
	// 2011.08.17 rmd : The new communications address. In the communication packet the address is actuall sent as 3
	// bytes. Just to be take up the same space as when we used the 3 digit ascii-type address. Three bytes allows us
	// 16,777,216 serial numbers. That will hold us for quite a while I'd say! We store it here however as a long.
	UNS_32  					serial_number;


	PURCHASED_OPTIONS_STRUCT	purchased_options;

	// ----------

	// The new port behavior settings. Size the array to include the TP board RS-485 ports as well.
	CONFIGURATION_PORT_CONTROL_STRUCT	port_settings[ UPORT_TOTAL_SYSTEM_PORTS ];

	// ----------

	// 6/19/2013 rmd : Only for ports A & B. The other ports do not have variable settings and
	// behaviours.
	UNS_32							port_A_device_index;
	
	UNS_32							port_B_device_index;

	// ----------

	// 8/27/2013 rmd : The comm_server IP address. Can be made editable via user
	// interface. It should be in the format "192.168.254.128".
	// 
	// 12/2/2013 ajv : NOTE: The Lantronix UDS1100-IAP device DOES NOT support
	// use of a hostname. Trying to connect using a hostname puts the device
	// into Monitor Mode.
	char	comm_server_ip_address[ 16 ];

	// 8/27/2013 rmd : And this is the port number. As a string so we can manage the format
	// provided to the device. As a number we would have to convert to a string when sending
	// to the device and we may have difficulty managing the exact string format we want. So
	// this way the exact string sent is here.
	char	comm_server_port[ 8 ];
	

	// 8/27/2013 rmd : If a second connection ip is desired, it may be managed by introducing
	// another message class. It would be the message class that knew which connect string to
	// use. The application would request the connection using one message class or the other.

	// ----------

	// 2/7/2017 rmd : Available space. No longer used.
	UNS_32	nlu_prevent_automated_CI_transmissions;
		
	// ----------

	DEBUG_BITS_STRUCT		debug;

	/* ------------------------------------------------------- *
	 *          Some Low Level Communication Behaviours
	 * ------------------------------------------------------- */

	// 4/3/2014 rmd : Was a 'programmable' minutes between rescan attempts. I've hardcoded it
	// now at 15 minutes.
	UNS_32	dummy;


	// OM_Originator Retries : Outgoing Message Retries : Only the FOAL master uses this
	// setting. For FOAL messages we will have 1 retry i.e. the original message will go out and
	// one more attempt to get a status will happen. The ORIGIN message will exist for
	// (OM_Originator_Retries * OM_Seconds_for_Status_FOAL). Upon the second timeout the message
	// will be destroyed by the transport and the application will recieve a failure message.
	// This fialure message will inturn cause any ORIGIN type OM's to be destroyed (probably
	// none in this scenario) and mark up a failure at the FOAL level for the controller we were
	// trying to talk to.
	UNS_32	OM_Originator_Retries;


	// OM_Seconds_for_Status_FOAL : Outgoing Message Seconds to Wait for Status : We have
	// decided that we only want one end doing retries in order to avoid collisions that would
	// result when the transport timeouts collide with the application timeouts. Explaination:
	// Take the FOAL controller and a remote controller playing foal. If the status to the OM to
	// the irri machine doesn't come back the foal machine transport will do a retry in
	// OM_Seconds_for_Status_FOAL seconds. If somehow the actual resp shows up however we would
	// like two things 1) that when the response shows the application destroys the OM (so that
	// it doesn't continue to retry) and 2) we we like that response to have a chance to show
	// before the retry of the OM occurs. Number 1) is satified using the
	// "DestroyAll_MESSAGE_TYPE_ORIGIN_MESSAGES" called from the application when a response is
	// received and 2) would be satified by setting the OM_Seconds_for_Status_FOAL to the
	// longest time it takes for the recieving controller to get around to sending the actual
	// RESP to the FOAL master. Remember the time we are interested in is the time it takes to
	// actually get the longest FOAL response back to the FOAL master. Well a full start time
	// for 40 stations results in a 3500 byte message. Through SKYLINE radios this can take 8
	// seconds to reach the master. So you say set the retry to 12 seconds or so.
	//
	// Well it gets more complicated. Suppose we also have a request for PData. The when the token hits
	// the remote controller the traffic that comes out is 2 status's followed by PData followed by the
	// 3.5K response to the foal master. In our worst case scenario the status to the foal master is 
	// missed and now we don't want to do a retry until all this data is out of the way - So technically
	// you would set the retry timer to the 15 seconds for PData plus the 8 seconds for the foal resp.
	// 
	// So that would get us to set the Seconds for Status timer to 15 + 8 plus a little margin = 25 seconds.
	//
	// What all this doesn't take into account is if the central asks for us to resend a packet. The
	// transport has to be smart enough to queue up retries and release them at the next received token. This
	// is because its quite likely FOAL traffic has resumed and any retry must be coordinated with that
	// traffic. But this retry activity doesn't effect the setting of OM_Seconds_for_Status_FOAL.
	UNS_32	OM_Seconds_for_Status_FOAL;

	UNS_32	OM_Minutes_To_Exist;
	
	// --------

	// 8/30/2012 rmd : The time used when the user presses TEST key on certain screens. Such as
	// the Station screen.
	UNS_32	test_seconds;
	
	// --------

	// 6/18/2013 ajv : The last decoder serial number that was assigned using
	// this controller. During debug, this will help ensure the same serial
	// number isn't assigned multiple times and expedite the process if power is
	// lost after 
	UNS_32	last_assigned_decoder_serial_number;

	// ----------
	// 6/26/2017 rmd : END OF REVISION 0 definition.
	// ----------

	// 6/28/2017 rmd : Flag added in Revision 1 to allow the user to turn on and off the HUB
	// behavior. There is a purchased options bit called option_HUB, that is set by purchasing
	// the option. Once purchased however, the user may want the controller to NOT act as a HUB,
	// and since there is no way to UN-PURCHASE the option, we introduced this variable. Setting
	// (false) would OVERRIDE the purchased option HUB bit.
	//
	// 6/28/2017 rmd : This variable should NEVER be referenced directly to determine "is this
	// controller a hub?". There is CONFIG function that is to be used to do that:
	// CONFIG_this_controller_is_a_configured_hub().
	BOOL_32	hub_enabled_user_setting;

	// ----------
	// 6/26/2017 rmd : END OF REVISION 1 definition.
	// ----------
	
} CONTROLLER_CONFIGURATION_STRUCT;


extern CONTROLLER_CONFIGURATION_STRUCT		config_c;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern const char* const comm_device_names[];

extern const PORT_DEVICE_SETTINGS_STRUCT port_device_table[];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_file_configuration_controller( void );

extern void save_file_configuration_controller( void );


extern BOOL_32 CONFIG_this_controller_originates_commserver_messages( void );

extern BOOL_32 CONFIG_is_connected( void );

extern BOOL_32 CONFIG_this_controller_is_a_configured_hub( void );

extern BOOL_32 CONFIG_this_controller_is_behind_a_hub( void );

extern BOOL_32 CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets( UNS_32 pport );


extern void init_a_to_d( void );

extern void CONFIG_device_discovery( UNS_32 pport );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

