/*  file = configuration_controller.c         11.15.2011 rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcpy
#include	<string.h>

#include	"configuration_controller.h"

#include	"app_startup.h"

#include	"flash_storage.h"

#include	"contrast_and_speaker_vol.h"

#include	"irri_comm.h"

#include	"lcd_init.h"

#include	"foal_defs.h"

#include	"flash_storage.h"

#include	"alerts.h"

#include	"lpc32xx_tsc.h"

#include	"lpc32xx_clkpwr_driver.h"

#include	"lpc32xx_intc_driver.h"

#include	"device_EN_UDS1100.h"

#include	"device_WEN_wibox.h"

#include	"device_WEN_PremierWaveXN.h"

#include	"device_GR_AIRLINK.h"

#include	"device_GR_MultiTech_MTSMC_LTE.h"

#include	"device_SR_FREEWAVE.h"

#include	"device_LR_RAVEON.h"

#include	"device_LR_FREEWAVE.h"

#include	"cal_math.h"

#include	"flowsense.h"

#include	"cs_mem.h"

#include	"device_GR_PremierWaveXC.h"

#include	"device_GENERIC_GR_card.h"

#include	"tpmicro_comm.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/4/2014 rmd : The index into the following arrays comes from the resistor value
// installed on the communication interface card. The resultant analog value is converted
// and run through some math to develop the card INDEX. This way different devices can use a
// disticntly different set of function calls to perform their activites.
const char* const comm_device_names[ NUMBER_OF_DEFINED_DEVICES ] = { "NONE INSTALLED", "FireLine or StingRay", "Freewave LRS455", "Freewave FGR2", "Lantronix UDS1100", "Sierra Wireless LS300", "Lantronix PremierWave XN", "Lantronix PremierWave XC", "Lantronix PremierWave XE", "Lantronix WiBox", "MultiTech LTE" };

const PORT_DEVICE_SETTINGS_STRUCT port_device_table[ NUMBER_OF_DEFINED_DEVICES ] =
{
	// 6/18/2013 rmd : On Port A a HIGH RTS Level in this table give a HIGH at the 20 pin which
	// gives a -ve low to the device. Same with RTS setting.

	// ----------
	
	// NO DEVICE PRESENT

	// For the case of no device present a LOW CTS value in this table ensure anything sent to
	// the port will be transmitted without a CTS timeout error. This is because the CTS signal
	// is pulled on the main board.
	//
	// 4/24/2015 rmd : And set the CD when connected level to HIGH so the serial port monitor
	// screen show the empty port as IDLE (not connected).
	{		(false),	// Device used for commserver messages
			57600,		// Baudrate
			LOW,		// CTS when OK to send
			HIGH,		// CD level when connected
			LOW,		// RI level when active
			LOW,		// RTS level to send
			LOW,		// DTR normal level
			LOW,		// Reset level when active
			NULL,		// function : Power ON/OFF
			NULL,		// function : initialize the connection process
			NULL,		// function : connection processing
			NULL,		// function : is connected
			NULL,		// function : initialize device exchange
			NULL,		// function : device exchange processing
	},
	
	// ----------

	// RAVEON LICENSED BAND 450MHZ STINGRAY (-LR)

	// 460MHz radio using Raveon StingRay
	{		(true),	// Device used for commserver messages
			9600,		// Baudrate
			LOW,		// CTS when OK to send
			LOW,		// CD level when connected
			LOW,		// RI level when active
			LOW,		// RTS level to send
			LOW,		// DTR normal level
			LOW,		// Reset level when active
			__GENERIC_gr_card_power_control,			// function : Power ON/OFF
			RAVEON_initialize_the_connection_process,	// function : initialize the connection process
			RAVEON_connection_processing,				// function : connection processing
			RAVEON_is_connected,						// function : is connected
			LR_RAVEON_initialize_device_exchange,		// function : initialize device exchange
			LR_RAVEON_exchange_processing,		// function : device exchange processing
	},
	
	// ----------

	// FREEWAVE LICENSED BAND 450MHZ LRS-455 (-LR)

	// 460MHz radio using Freewave LRS-455
	{		(true),	// Device used for commserver messages
			57600,		// Baudrate
			LOW,		// CTS when OK to send
			LOW,		// CD level when connected
			LOW,		// RI level when active
			LOW,		// RTS level to send
			LOW,		// DTR normal level
			LOW,		// Reset level when active
			LR_FREEWAVE_power_control,						// function : Power ON/OFF
			LR_FREEWAVE_initialize_the_connection_process,	// function : initialize the connection process
			LR_FREEWAVE_connection_processing,				// function : connection processing
			__GENERIC_gr_card_is_connected,					// function : is connected
			LR_FREEWAVE_initialize_device_exchange,			// function : initialize device exchange
			LR_FREEWAVE_exchange_processing,				// function : device exchange processing
	},
	
	// ----------

	// FREEWAVE SPREAD SPECTRUM 900 MHZ FGRS (-SR)

	// 900MHz spread spectrum using Freewave FGR2
	{		(true),	// Device used for commserver messages
			57600,		// Baudrate
			LOW,		// CTS when OK to send
			LOW,		// CD level when connected
			LOW,		// RI level when active
			LOW,		// RTS level to send
			LOW,		// DTR normal level
			LOW,		// Reset level when active
			SR_FREEWAVE_power_control,						// function : Power ON/OFF
			SR_FREEWAVE_initialize_the_connection_process,	// function : initialize the connection process
			SR_FREEWAVE_connection_processing,				// function : connection processing
			__GENERIC_gr_card_is_connected,					// function : is connected
			SR_FREEWAVE_initialize_device_exchange,			// function : initialize device exchange
			SR_FREEWAVE_exchange_processing,				// function : device exchange processing
	},
	
	// ----------

	// LANTRONIX UDS-1100-IAP ETHERNET

	// connection sequence tested

	// Ethernet using Lantronix UDS1100-IAP RS-232 device connected via a GR-CARD. The GR CARD
	// translates the signals from the 3.3VDC CMOS level to the 232 level (approx +/- 5VDC).
	// WITH AN INVERSION. This table must account for the inversion. So if we want a 232 level
	// HI at the device connector place a LOW in the table. Here's what we know so far. At its
	// RS232 connector the UDS1100 wants to see DTR HI to connect and LO to disconnect. RTS
	// should always be HI during operation. CD will be HI when the unit is connected and LO
	// when not connected. And CTS is HI when safe to send data to the UDS1100, LO when we
	// should pause. So in the table here you'll see all those levels reversed due to the GR
	// Interface card.
	{		(true),		// Device used for commserver messages
			115200,		// Baudrate
			LOW,		// CTS when OK to send
			LOW,		// CD level when connected
			LOW,		// RI level when active
			LOW,		// RTS level to send
			LOW,		// DTR normal level
			LOW,		// Reset level when active
			__GENERIC_gr_card_power_control,			// function : Power ON/OFF
			UDS1100_initialize_the_connection_process,	// function : initialize the connection process
			UDS1100_connection_processing,				// function : connection processing
			__GENERIC_gr_card_is_connected,				// function : is connected
			DEVICE_initialize_device_exchange,			// function : initialize device exchange
			DEVICE_exchange_processing,					// function : device exchange processing
	},
	
	// ----------

	// SIERRA WIRELESS AIRLINK RAVEN/LS300 3G CELL

	// 1/24/2017 rmd : NO LONGER USING THIS DEVICE!!!!

	// 2G/3G cellular using Sierra Wireless Airlink RAVEN X/LS300.
	// 3/11/2014 rmd : Here is the low down on the Airlink cell units. High (232) means a
	// positive voltage when looking at the 232 wires.
	//
	// REMEMBER the signal level specified here in this table are logic level. The 232 levels
	// are the inverse. Therefore in the discussion that follows you'll find the corresponding
	// table value is the opposite polarity.
	//
	// For the 2000e have been programmed for a baud rate of 19200. For the CS3000 I am changing
	// that to 57600. Also changing the behaviour of the DCD output (see below).
	//
	// DSR:	Output from the radio has previously (as in the past) BEEN programmed to go high
	//  	(232 level) when in data mode. I'm changing the programming going forward for units
	//  	mounted on the CS3000. Setting DSR to go high when in coverage. This is more useful
	//  	- especially considering the DCD line does the data mode behaviour.
	//
	// DTR: Input to the radio. To be driven high (232). Not sure of its impact within the unit.
	// 		I didn't see anything in the manual.
	//
	// DCD: Output from the radio. When connected and in data mode is driven High(232).
	//
	// RTS: Input to the radio. We drive High (232) to the radio to indicate we are always ready
	// to receive serial data.
	//
	// CTS: Output from the radio. High (232) when okay to send data to the radio. Goes low when
	// we should stop.
	//
	// RI: May or may not 'ring' when connecting. I can't tell on the unit I have as I may have
	// blown up the RI output by tying 12VDC to the pin of an unpowered unit.
	{		(false),	// Device used for commserver messages
			57600,		// Baudrate
			LOW,		// CTS when OK to send
			LOW,		// CD level when connected
			LOW,		// RI level when active
			LOW,		// RTS level to send
			LOW,		// DTR normal level
			LOW,		// Reset level when active
			__GENERIC_gr_card_power_control,		// function : Power ON/OFF
			NULL,									// function : initialize the connection process
			NULL,									// function : connection processing
			__GENERIC_gr_card_is_connected,			// function : is connected
			GR_AIRLINK_initialize_device_exchange,	// function : initialize device exchange
			GR_AIRLINK_exchange_processing,			// function : device exchange processing
	},
	
	// ----------

	// LANTRONIX PREMIERWAVE XN WIFI

	// 1/24/2017 rmd : NO LONGER USING THIS DEVICE!!!!
	//
	// 3/13/2017 rmd : While not OFFICIALLY supported there are people and installations (mostly
	// engineering related) that are using incorrect combinations of interface card and
	// communication device. For example Able has a XE card and a Lantronix SGX 5150 WiFi
	// device. Electrically it works. Instead of blocking these combination out I will cheat
	// here and use the WIBOX connection processing suport. To allow this device to continue to
	// work on such 'incorrectly assembled' controllers.
	

	// Wi-Fi using Lantronix PremierWave XN RS-232 connected via a GR-CARD. The GR CARD
	// translates the signals from the 3.3VDC CMOS level to the 232 level (approx +/- 5VDC).
	// WITH AN INVERSION. This table must account for the inversion. So if we want a 232 level
	// HI at the device connector place a LOW in the table. Here's what we know so far.
	{		(true),		// Device used for commserver messages
			115200,		// Baudrate
			LOW,		// CTS when OK to send
			LOW,		// CD level when connected
			LOW,		// RI level when active
			LOW,		// RTS level to send
			LOW,		// DTR normal level
			LOW,		// Reset level when active
			__GENERIC_gr_card_power_control,		// function : Power ON/OFF
			WIBOX_initialize_the_connection_process,	// function : initialize the connection process
			WIBOX_connection_processing,				// function : connection processing
			__GENERIC_gr_card_is_connected,			// function : is connected
			DEVICE_initialize_device_exchange,		// function : initialize device exchange
			DEVICE_exchange_processing,				// function : device exchange processing
	},
	
	// ----------
	
	// LANTRONIX PREMIERWAVE XC 3G CELL

	// connection sequence tested

	// 3.5G cellular using Lantronix PremierWave XC HSPA+ RS-232 connected via a GR-CARD. The GR
	// CARD translates the signals from the 3.3VDC CMOS level to the 232 level (approx +/-
	// 5VDC). WITH AN INVERSION. This table must account for the inversion. So if we want a 232
	// level HI at the device connector place a LOW in the table. Some special notes about the
	// Lantronix GR: the unit has a serial port that looks like a PC. That is different from
	// most devices we interface to (both the WBX2100 and the PremierWave serial ports look like
	// a PC). Because of that we wire to them differently than the say the Raveon LR radios.
	// Specifically CTS is an input to them so we feed them RTS. And their RTS signal is an
	// output which we wire to our CTS. And furthermore their CD is an input. We are really used
	// to CD being an output and need that to tell if the device has an active connection. We
	// programmed the unit to signal an active connection with DTR and wire their DTR output to
	// our CD input. See the device wiring Excel document for further details.
	{		(true),		// Device used for commserver messages
			115200,		// Baudrate
			LOW,		// CTS when OK to send
			LOW,		// CD level when connected
			LOW,		// RI level when active
			LOW,		// RTS level to send
			LOW,		// DTR normal level
			LOW,		// Reset level when active
			__GENERIC_gr_card_power_control,		// function : Power ON/OFF
			PWXC_initialize_the_connection_process,	// function : initialize the connection process
			PWXC_connection_processing,				// function : connection processing
			__GENERIC_gr_card_is_connected,			// function : is connected
			DEVICE_initialize_device_exchange,		// function : initialize device exchange
			DEVICE_exchange_processing,				// function : device exchange processing
	},
	
	// ----------
	
	// LANTRONIX PREMIERWAVE XE ETHERNET

	// 1/24/2017 rmd : NO LONGER USING THIS DEVICE!!!!
	//
	// 3/13/2017 rmd : While not OFFICIALLY supported there are people and installations (mostly
	// engineering related) that are using incorrect combinations of interface card and
	// communication device. For example Able has a XE card and a Lantronix SGX 5150 WiFi
	// device. Electrically it works. Instead of blocking these combination out I will cheat
	// here and use the WIBOX connection processing suport. To allow this device to continue to
	// work on such 'incorrectly assembled' controllers.
	
	// Ethernet using Lantronix PremierWave XE RS-232 connected via a GR-CARD. The GR CARD
	// translates the signals from the 3.3VDC CMOS level to the 232 level (approx +/- 5VDC).
	// WITH AN INVERSION. This table must account for the inversion. So if we want a 232 level
	// HI at the device connector place a LOW in the table. Here's what we know so far.
	{		(true),		// Device used for commserver messages
			115200,		// Baudrate
			LOW,		// CTS when OK to send
			LOW,		// CD level when connected
			LOW,		// RI level when active
			LOW,		// RTS level to send
			LOW,		// DTR normal level
			LOW,		// Reset level when active
			__GENERIC_gr_card_power_control,		// function : Power ON/OFF
			WIBOX_initialize_the_connection_process,	// function : initialize the connection process
			WIBOX_connection_processing,				// function : connection processing
			__GENERIC_gr_card_is_connected,			// function : is connected
			DEVICE_initialize_device_exchange,		// function : initialize device exchange
			DEVICE_exchange_processing,				// function : device exchange processing
	},
	
	// ----------
	
	// LANTRONIX WIBOX 

	// Wi-Fi using Lantronix Wibox RS-232 connected via a GR-CARD. The GR CARD translates the
	// signals from the 3.3VDC CMOS level to the 232 level (approx +/- 5VDC). WITH AN INVERSION.
	// This table must account for the inversion. So if we want a 232 level HI at the device
	// connector place a LOW in the table. Here's what we know so far.
	{		(true),		// Device used for commserver messages
			115200,		// Baudrate
			LOW,		// CTS when OK to send
			LOW,		// CD level when connected
			LOW,		// RI level when active
			LOW,		// RTS level to send
			LOW,		// DTR normal level
			LOW,		// Reset level when active
			__GENERIC_gr_card_power_control,		// function : Power ON/OFF
			WIBOX_initialize_the_connection_process,	// function : initialize the connection process
			WIBOX_connection_processing,				// function : connection processing
			__GENERIC_gr_card_is_connected,			// function : is connected
			DEVICE_initialize_device_exchange,		// function : initialize device exchange
			DEVICE_exchange_processing,				// function : device exchange processing
	},
	
	// ----------
	
	// MULTITECH MTSMC-LAT1 LTE CELL
	
	// connection sequence tested

	{		(true),		// Device used for commserver messages
			115200,		// Baudrate
			LOW,		// CTS when OK to send
			LOW,		// CD level when connected
			LOW,		// RI level when active
			LOW,		// RTS level to send
			LOW,		// DTR normal level
			LOW,		// Reset level when active
			__GENERIC_gr_card_power_control,				// function : Power ON/OFF

			MTSMC_LAT_initialize_the_connection_process,	// function : initialize the connection process
			MTSMC_LAT_connection_processing,				// function : connection processing

			__GENERIC_gr_card_is_connected,			// function : is connected

			DEVICE_initialize_device_exchange,		// function : initialize device exchange
			DEVICE_exchange_processing,				// function : device exchange processing
	},
	
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// filenames ARE ALLOWED 32 CHARS ... truncated if exceeds this limit

static const char CONTROLLER_CONFIG_FILENAME[] =	"CONTROLLER_CONFIGURATION";

// The structure revision number. This is a number!
#define	CONTROLLER_CONFIG_LATEST_FILE_REVISION		(2)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/24/2018 rmd : This variable is initialized to ZERO on C startup. Then a copy of the
// variable is read out of the flash file system into this memory space (SDRAM).
CONTROLLER_CONFIGURATION_STRUCT		config_c;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void init_config_controller( void )
{
	// 10/24/2018 rmd : One thing we can count on is the entire config_c variable is zero in
	// SDRAM to start. So the first time the file is made, and this function is called, the
	// whole thing is ZERO. Meaning any variables or part of the strucutre not explicitly set to
	// ZERO in this function, are implicitly zero. I think that can be counted on.
	
	// ----------
	
	UNS_32	p;
	
	// ----------
	
	// DEBUG

	config_c.debug.use_new_k_and_offset_numbers = (true);

	config_c.debug.show_flow_table_interaction = (true);

	config_c.debug.transport_om_show_unexpected_behavior_alerts = (true);

	config_c.debug.transport_om_show_status_timer_expired_alert = (true);

	// ----------

	// PURCHASED OPTIONS
	
	config_c.purchased_options.option_FL = (false);

	config_c.purchased_options.option_SSE = (false);
	
	config_c.purchased_options.option_SSE_D = (false);

	config_c.purchased_options.option_HUB = (false);

	config_c.purchased_options.option_AQUAPONICS = (false);

	// ----------
	
	// 9/15/2017 rmd : This field not to be used unless config.port_A_device_index is
	// COMM_DEVICE_LR_RAVEON.
	config_c.purchased_options.port_a_raveon_radio_type = RAVEON_RADIO_ID_M7;

	config_c.purchased_options.port_a_freewave_lr_set_for_repeater = (false);

	// 9/15/2017 rmd : NOTE - as of this writingthere is no user choice to make an SR radio a
	// repeater or not, we ALWAYS program the radio as if there is a repeater. This has to be
	// changed ... as a future project ... so in the meantime set this flag true to reflect what
	// is programmed into the radio.
	config_c.purchased_options.port_a_freewave_sr_set_for_repeater = (true);

	// 9/15/2017 rmd : This field not to be used unless config.port_B_device_index
	// is COMM_DEVICE_LR_RAVEON.
	config_c.purchased_options.port_b_raveon_radio_type = RAVEON_RADIO_ID_M7;

	config_c.purchased_options.port_b_freewave_lr_set_for_repeater = (false);

	// 9/15/2017 rmd : NOTE - as of this writingthere is no user choice to make an SR radio a
	// repeater or not, we ALWAYS program the radio as if there is a repeater. This has to be
	// changed ... as a future project ... so in the meantime set this flag true to reflect what
	// is programmed into the radio.
	config_c.purchased_options.port_b_freewave_sr_set_for_repeater = (true);
	
	// ----------

	// PORT BEHAVIOR - largely neutured. Only the CRC flag left.
	for( p=0; p<UPORT_TOTAL_SYSTEM_PORTS; p++ )
	{
		config_c.port_settings[ p ].size_of_the_union = 0;

		// 2/28/2017 rmd : If there is an LR on a port we may want to turn this off.
		config_c.port_settings[ p ].alert_about_crc_errors = (true);
	}
	
	// ----------

	// 6/18/2013 rmd : Set the NONE_PRESENT port A & B settings. When a device is formally
	// detected during the serial driver task initialization this value is overwritten.
	config_c.port_A_device_index = 0;

	config_c.port_B_device_index = 0;
	
	// ----------

	// 9/10/2013 rmd : Used during development. Specifics for my computer.
	//snprintf( config_c.comm_server_ip, sizeof(config_c.comm_server_ip), "192.168.254.128" );
	//snprintf( config_c.comm_server_port, sizeof(config_c.comm_server_port), "2000" );

	// 10/20/2014 rmd : Most important for the LS300 radios that the IP address octects not
	// contain any leading zeros. The atdt string will not work in that case. It's okay for the
	// Lantronix units. But not for the LS300.
	snprintf( config_c.comm_server_ip_address, sizeof(config_c.comm_server_ip_address), "64.73.242.99" );

	snprintf( config_c.comm_server_port, sizeof(config_c.comm_server_port), "16001" );

	// 9/10/2013 rmd : The IP specifics for the CALSENSE web server databases.
	//snprintf( config_c.comm_server_ip_address, sizeof(config_c.comm_server_ip_address), "192.168.254.135" );
	//snprintf( config_c.comm_server_port, sizeof(config_c.comm_server_port), "16001" );
	
	// ----------

	config_c.nlu_prevent_automated_CI_transmissions = 0;
	
	// ----------

	// 2/28/2017 rmd : DEFAULT SERIAL NUMBER - 50065. Set it.
	config_c.serial_number = (CONTROLLER_MINIMUM_SERIAL_NUMBER + 'A');
	
	// ----------

	config_c.dummy = 0;
	config_c.OM_Originator_Retries = 1;
	config_c.OM_Seconds_for_Status_FOAL = 25;
	config_c.OM_Minutes_To_Exist = 5;

	// ----------

	// 8/30/2012 rmd : The old traditional default test length.
	config_c.test_seconds = 120;

	// ----------

	// 6/18/2013 ajv : Assign the last decoder to the minimum of 0x400.
	config_c.last_assigned_decoder_serial_number = 0x400;

	// ----------

	// 6/21/2017 ajv : The default should be (true) so that when the option is purchased the
	// feature is enabled, without further action.
	config_c.hub_enabled_user_setting = (true);
}

/* ---------------------------------------------------------- */
static void controller_config_updater( UNS_32 pfrom_revision )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only when
	// the file revision is changed. FYI there is no MUTEX on the controller configuration
	// structure.

	// ----------
	
	if( pfrom_revision == CONTROLLER_CONFIG_LATEST_FILE_REVISION )
	{
		Alert_Message_va( "CONTLR CONFIG file unexpd update %u", pfrom_revision );	
	}
	else
	{
		Alert_Message_va( "CONTLR CONFIG file update : to revision %u from %u", CONTROLLER_CONFIG_LATEST_FILE_REVISION, pfrom_revision );

		// ----------
		// ----------
		// ----------
		
		// 9/17/2015 rmd : NOTE - DO NOT USE THE FORMAL VARIABLE SET FUNCTIONS IN THIS FUNCTION. THE
		// CONTROLLER INDEX IS NOT YET KNOWN. Is this okay? I'm not sure but what I do know is the
		// controller index is not yet available due to the order of file inits called during the
		// startup process. AJ has just informed me the controller config variables are NOT sync'd
		// so not using the SET routines is fine.
		
		// ----------
		// ----------
		// ----------
		
		if( pfrom_revision == 0 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 0 to REVISION 1.

			// 6/21/2017 ajv : The default should be (true) so that when the option is purchased the
			// feature is enabled, without further action.
			config_c.hub_enabled_user_setting = (true);
			
			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 1 to 2 changes if
			// needed.
			pfrom_revision += 1;
		}
		
		// ----------
		
		if( pfrom_revision == 1 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 1 to REVISION 2.
				
			// 9/15/2017 rmd : This field not to be used unless config.port_A_device_index is
			// COMM_DEVICE_LR_RAVEON.
			config_c.purchased_options.port_a_raveon_radio_type = RAVEON_RADIO_ID_M7;
		
			config_c.purchased_options.port_a_freewave_lr_set_for_repeater = (false);
		
			// 9/15/2017 rmd : NOTE - as of this writingthere is no user choice to make an SR radio a
			// repeater or not, we ALWAYS program the radio as if there is a repeater. This has to be
			// changed ... as a future project ... so in the meantime set this flag true to reflect what
			// is programmed into the radio.
			config_c.purchased_options.port_a_freewave_sr_set_for_repeater = (true);
		
			// 9/15/2017 rmd : This field not to be used unless config.port_B_device_index
			// is COMM_DEVICE_LR_RAVEON.
			config_c.purchased_options.port_b_raveon_radio_type = RAVEON_RADIO_ID_M7;
		
			config_c.purchased_options.port_b_freewave_lr_set_for_repeater = (false);
		
			// 9/15/2017 rmd : NOTE - as of this writing there is no user choice to make an SR radio a
			// repeater or not, we ALWAYS program the radio as if there is a repeater. This has to be
			// changed ... as a future project ... so in the meantime set this flag true to reflect what
			// is programmed into the radio.
			config_c.purchased_options.port_b_freewave_sr_set_for_repeater = (true);

			pfrom_revision += 1;
		}

		/*
		if( pfrom_revision == 2 )
		{ 
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 2 to REVISION 3.
				

			pfrom_revision += 1;
		}
		*/
	}

	// ----------

	// 1/29/2015 rmd : Well after the updater has run the pfrom_revision is supposed to equal
	// the latest revision. By design of this update process.
	if( pfrom_revision != CONTROLLER_CONFIG_LATEST_FILE_REVISION )
	{
		Alert_Message( "CONTLR CONFIG updater error" );
	}
}

/* ---------------------------------------------------------- */
extern void init_file_configuration_controller( void )
{
	// 3/25/2014 rmd : NOTE - the configuration file is designed to hold variables not normally
	// touched by the end user. Therefore this cluster of variables is held together in a file
	// stored in FLASH_0. This flash is physically a separate flash chip and the file system
	// within is rarely written to. It holds only this file and the code image files (so far).
	// The argument being less file manipulation should be a more robust storage location.

	FLASH_FILE_find_or_create_variable_file(	FLASH_INDEX_0_BOOTCODE_AND_EXE,
												CONTROLLER_CONFIG_FILENAME,
												CONTROLLER_CONFIG_LATEST_FILE_REVISION,
												&config_c,
												sizeof( config_c ),
												NULL,
												&controller_config_updater,
												&init_config_controller,
												FF_CONTROLLER_CONFIGURATION );
}

/* ---------------------------------------------------------- */
extern void save_file_configuration_controller( void )
{
	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file( FLASH_INDEX_0_BOOTCODE_AND_EXE,
															CONTROLLER_CONFIG_FILENAME,
															CONTROLLER_CONFIG_LATEST_FILE_REVISION,
															&config_c,
															sizeof(config_c),
															NULL,
															FF_CONTROLLER_CONFIGURATION );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern BOOL_32 CONFIG_this_controller_originates_commserver_messages( void )
{
	// 10/26/2016 rmd : This controller originates commserver messages if it has a GR, EN, WEN,
	// LR, or SR on the A port of the A controller. We use the device configuration table to
	// guide us if the device can be used to pass CI traffic bound for the commserver. Being a
	// MASTER (one way or another is not enough), the controller must also have a device on
	// Port A.
	
	// 6/4/2015 rmd : NOTE - this function is called from a wide variety of tasks. Including the
	// POWER_FAIL TASK. So do not add code that involves lengthy execution times because in that
	// task when this function is called we are trying to shut the system down. Quickly! That
	// means keep it tight. And no I don't have a usec number for you.
	
	BOOL_32		rv;
	
	rv = (false);
	
	if( FLOWSENSE_we_are_a_master_one_way_or_another() )
	{
		if( port_device_table[ config_c.port_A_device_index ].on_port_A_enables_controller_to_make_CI_messages )
		{
			rv = (true);
		}
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 CONFIG_is_connected( void )
{
	BOOL_32	rv;

	rv = (false);

	// 11/10/2016 rmd : For controllers that are allowed to generate CI messages (meaning they
	// are a master and have an appropriate device) proceed with the test if they are connected.
	if( CONFIG_this_controller_originates_commserver_messages() )
	{
		if( port_device_table[ config_c.port_A_device_index ].__is_connected != NULL )
		{
			rv = port_device_table[ config_c.port_A_device_index ].__is_connected( UPORT_A );
		}
		else
		{
			Alert_Message( "CI: cannot check for connection" );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 CONFIG_this_controller_is_a_configured_hub( void )
{
	// 6/28/2017 rmd : Returns (true) if you have the hub purchase option set, and the user
	// override setting is (true), and this controller is a MASTER, and has a PORT A device to
	// communicate with the commserver, and has a PORT B device that allows HUB traffic.

	// ----------
	
	BOOL_32	rv;

	rv = (false);
	
	// 5/16/2017 rmd : First requirement is that this has the HUB option purchased, and this
	// controller can make controller initiated messages, meaning it directly connects to the
	// commserver.
	if( config_c.purchased_options.option_HUB && config_c.hub_enabled_user_setting )
	{
		if( CONFIG_this_controller_originates_commserver_messages() )
		{
			// 5/16/2017 rmd : Second requirement is the correct choice of device on port B.
			if( (config_c.port_B_device_index == COMM_DEVICE_LR_FREEWAVE) || (config_c.port_B_device_index == COMM_DEVICE_LR_RAVEON) || (config_c.port_B_device_index == COMM_DEVICE_SR_FREEWAVE) )
			{
				rv = (true);
			}
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 CONFIG_this_controller_is_behind_a_hub( void )
{
	BOOL_32	rv;

	rv = (false);

	// 2/13/2017 rmd : Identify controllers that have a hub between them and the commserver. DO
	// NOT include the hub itself. Controllers that use a hub are a MASTER. And have a -SR or
	// -LR on their port A. DOES NOT INCLUDE THE HUB ITSELF.
	if( FLOWSENSE_we_are_a_master_one_way_or_another() )
	{
		if( (config_c.port_A_device_index == COMM_DEVICE_LR_FREEWAVE) || (config_c.port_A_device_index == COMM_DEVICE_LR_RAVEON) || (config_c.port_A_device_index == COMM_DEVICE_SR_FREEWAVE) )
		{
			rv = (true);
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets( UNS_32 pport )
{
	BOOL_32	rv;
	
	UNS_32	device_index;
	
	rv = (false);

	// 5/16/2017 rmd : Currently only the FREEWAVE SR radio is eligible for these packet
	// classes
	//					case MSG_CLASS_CS3000_SCAN:
	//					case MSG_CLASS_CS3000_SCAN_RESP:
	//					case MSG_CLASS_CS3000_TOKEN:
	//					case MSG_CLASS_CS3000_TOKEN_RESP:
	//					case MSG_CLASS_CS3000__FROM_CONTROLLER__CODE_DISTRIBUTION:

	if( (pport == UPORT_A) || (pport == UPORT_B) )
	{
		if( pport == UPORT_A )
		{
			device_index = config_c.port_A_device_index;
		}
		else
		{
			device_index = config_c.port_B_device_index;
		}
		
		if( device_index == COMM_DEVICE_SR_FREEWAVE )
		{
			rv = (true);
		}
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern void init_a_to_d( void )
{
	LPC3250_ADCTSC_REGS_T	*ladc;

	ladc = ADC;
	
	// ----------
	
	// 8/28/2013 rmd : Setup to use the PERIPHERAL CLOCK. And divide by 4. The value in the
	// register is one less than the clock divider. So you see a 3 below and that gives a
	// divider of 4. The PCLK is 13MHz. So we get an adc clock rate of 3.25MHz. The maximum
	// allowable if 4.5MHz.
	clkpwr_setup_adc_ts( 1, 3 );
	
	// 8/28/2013 rmd : Enable the clock to the adc block.
	clkpwr_clk_en_dis( CLKPWR_ADC_CLK, 1 );
	
	// ----------
	
	// 8/28/2013 rmd : Note the touch screen controller is part of the ADC in the LPC3250 hence
	// the TS nomenclature. It seem a requirement to program the ADC channel in the INTERRUPT
	// CONTROLLER to get the polling of the adc bit in the RSR register working correctly.
	xDisable_ISR( TS_IRQ_INT );

	xSetISR_Vector( TS_IRQ_INT, INTERRUPT_PRIORITY_a_to_d, ISR_TRIGGER_HIGH_LEVEL, NULL, NULL );
	
	// 8/28/2013 rmd : Am using the A-D in polled mode for now. So the ADC interrupt is not
	// actually enabled.
	
	// ----------
	
	// 8/28/2013 rmd : Select A/D mode. And channel 0.
	ladc->adc_sel = ADC_SEL_MASK;
	
	// 8/28/2013 rmd : Power up AD. Do not start a conversion.
	ladc->adc_con = TSC_ADCCON_POWER_UP;
	
	// ----------
}


/* ---------------------------------------------------------- */
static UNS_32 AD_conversion_and_return_results( UNS_32 pchannel )
{
	// 8/28/2013 rmd : Use channel 0 to read PORT_A device. Channel 1 to read PORT_B device.

	// ----------

	LPC3250_ADCTSC_REGS_T	*ladc;

	UNS_32	count;
	
	UNS_32	results;

	// ----------
	
	ladc = ADC;
	
	// 8/28/2013 rmd : Read and discard the results register to guarantee start with a cleared
	// ADC_INT.
	results = (ladc->adc_dat & TSC_ADCDAT_VALUE_MASK);

	// 8/28/2013 rmd : And read the INTC RSR register. Though this should not be needed as the
	// INTC is not set up for an edge triggered interrupt.
	SIC1_RSR = SIC1_RSR_ADC_INT;
	
	// ----------
	
	if( pchannel == 0 )
	{
		ladc->adc_sel = 0x284;			
	}
	else
	if( pchannel == 1 )
	{
		ladc->adc_sel = 0x294;			
	}
	else
	{
		Alert_Message( "AD: poor channel choice." );
	}
	
	// ----------

	// 8/28/2013 rmd : Start the conversion.
	ladc->adc_con |= (TSC_ADCCON_ADC_STROBE);
	
	// ----------

	// 8/28/2013 rmd : Now poll the interrupt controller. Waiting for conversion complete.
	count = 0;
	
	do
	{
		// 8/28/2013 rmd : By dividing our 13MHz clock down by 4, the conversion time is about
		// 3.5usec. In practice, with debug version code, I saw the count read about 25. I
		// suppose release code will count higher. But not 8 times higher.
		if( ++count > 200 )
		{
			Alert_Message( "AD: never finishes" );
			
			// 8/28/2013 rmd : And give a way out.
			break;
		}
	
	} while( !(SIC1_RSR & SIC1_RSR_ADC_INT) );

	// ----------

	// 8/28/2013 rmd : Read and return the results.
	return( (ladc->adc_dat & TSC_ADCDAT_VALUE_MASK) );

	// ----------
}

/* ---------------------------------------------------------- */
extern void CONFIG_device_discovery( UNS_32 pport )
{
	// 6/19/2013 rmd : Function to read the analog voltage set by the resistor divider on the
	// interface card plugged into the port. This identifies the device and sets the
	// config_c.port_settings_# appropriately.
	
	// 6/19/2013 rmd : If changed from the existing config_c ID setting should save the file.
	
	UNS_32	conversion_value_1, conversion_value_2, tries;

	UNS_32	conversion_index;
	
	BOOL_32	save_file;
	
	char	lstr_64[ 64 ], port_char;
	
	const volatile float ONE_THOUSAND_TWENTY_FOUR_POINT_O = 1024.0;

	const volatile float THIRTY_TWO_POINT_O = 32.0;

	// ----------
	
	save_file = (false);
	
	// ----------

	if( (pport == UPORT_A) || (pport == UPORT_B) )
	{
		if( pport == UPORT_A )
		{
			port_char = 'A';
		}
		else
		{
			port_char = 'B';
		}
		
		// ----------
	
		// 8/28/2013 rmd : We are working with a 10-bit ADC. That yields 1024 counts. The A-D
		// maximum DC voltage allowed is 3 volts. The resistor values provided give us 16 steps from
		// 0 to the 60% mark. Why 60%? Because 60% of 5VDC is 3VDC. See the communication interface
		// card schematic and the resistor divider document to learn more. Each step is 4% of the
		// full 5V scale.
		//
		// 1/27/2014 rmd : Note there is complication because some of the boards have 3.3VDC at the
		// top of their resistor divider and some have 5VDC at their top. This is handled by the
		// choice of resistor. In either case there are 16 step from 0 to full count of 1024. Hence
		// the 16 in the math below.
		//
		// 3/7/2014 rmd : UPDATE - I was not comfortable with the 16 step plan. We lost the first
		// position and last steps the way it all worked out. Leaving us 14 steps. Of which 6 are
		// already occupied. Leaving 8 free. Well that just ain't enough. So since I'm a nice guy
		// I'm going to extra effort and gonna change it now before we ship and have code & hardware
		// backwards compatibility problems. So I've decided to just double the whole mess up. The
		// A-D is easily quite and accurate enough to support such. So now there are 32 steps out of
		// which we can support 28 DEVICES. So now we have plenty of room for a variety of future
		// devices. You're welcome.
		//
		// 6/29/2015 rmd : We've seen a few cases of an LR showing up when nothing is plugged into
		// the port. This could be noise. The first 25 boards only had a 1M ohm pull down. This was
		// changed for the Rev H boards to 240K. Should be much better. For the first 25 boards lets
		// set up a double read requirement. Which won't hurt to keep for the long run. Note each
		// step is 32 counts.
		tries = 0;

		do
		{
			tries += 1;
			
			if( pport == UPORT_A )
			{
				conversion_value_1 = AD_conversion_and_return_results( 0 );

				conversion_value_2 = AD_conversion_and_return_results( 0 );
			}
			else
			{
				conversion_value_1 = AD_conversion_and_return_results( 1 );

				conversion_value_2 = AD_conversion_and_return_results( 1 );
			}

		} while( conversion_value_1 != conversion_value_2 );
		
		// 8/19/2016 rmd : For debug only. To assess the reads necessary for matching results.
		// Usually saw values of 1 or 2. So supresssed now.
		//Alert_Message_va( "port id conversions: %u", tries );
		
		// ----------
		
		conversion_index = __round_UNS16( (((float)conversion_value_1 / ONE_THOUSAND_TWENTY_FOUR_POINT_O) * THIRTY_TWO_POINT_O) );
		
		// ----------
	
		if( conversion_index < NUMBER_OF_DEFINED_DEVICES )
		{
			if( pport == UPORT_A )
			{
				if( conversion_index != config_c.port_A_device_index )
				{
					config_c.port_A_device_index = conversion_index;
					
					save_file = (true);
				}
			}
			else
			{
				if( conversion_index != config_c.port_B_device_index )
				{
					config_c.port_B_device_index = conversion_index;
					
					save_file = (true);
				}
			}

		}
		else
		{
			snprintf( lstr_64, sizeof(lstr_64), "Port %c ID error. (Interface card resistors?)", port_char );

			Alert_Message( lstr_64 );

			// ----------
			
			// 4/24/2015 rmd : And set the device index to 0 if not already there.

			if( pport == UPORT_A )
			{
				if( config_c.port_A_device_index != 0 )
				{
					config_c.port_A_device_index = 0;
					
					save_file = (true);
				}
			}
			else
			{
				if( config_c.port_B_device_index != 0 )
				{
					config_c.port_B_device_index = 0;
					
					save_file = (true);
				}
			}

		}

		// ----------
	
		if( save_file )
		{
			snprintf( lstr_64, sizeof(lstr_64), "New Port %c Device: %s", port_char, comm_device_names[ conversion_index ] );

			Alert_Message( lstr_64 );

			// ----------
			
			// 9/15/2017 rmd : Use a 4 second delay, so that the RAVEON ID effort has time to run. It
			// also can result in a config file save, by making this 4 seconds, and the file write there
			// 2 seconds, we end up with a single file write. Not big deal, but I thought of it so
			// implemented.
			FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_CONTROLLER_CONFIGURATION, 4 );
			
			// ----------

			// 5/1/2014 rmd : Take the mutex to protect against us missing a change due to multiple
			// processes manipulating the send_box_configuration_to_master flag.
			xSemaphoreTakeRecursive( irri_comm_recursive_MUTEX, portMAX_DELAY );

			// 4/16/2014 rmd : And trigger the configuration change to be sent to the master when the
			// next token arrives.
			irri_comm.send_box_configuration_to_master = (true);

			xSemaphoreGiveRecursive( irri_comm_recursive_MUTEX );
		}
		else
		{
			snprintf( lstr_64, sizeof(lstr_64), "Same Port %c Device: %s", port_char, comm_device_names[ conversion_index ] );

			Alert_Message( lstr_64 );
		}
		
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

