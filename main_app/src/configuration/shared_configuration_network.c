/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_SHARED_CONFIGURATION_NETWORK_C
#define _INC_SHARED_CONFIGURATION_NETWORK_C

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"bithacks.h"

#include	"change.h"

#include	"configuration_network.h"

#include	"foal_defs.h"

#include	"range_check.h"

#include	"shared.h"

#ifndef	_MSC_VER

	#include	"alerts.h"

	#include	"app_startup.h"
	
	#include	"cs_mem.h"

	#include	"background_calculations.h"

	#include	"battery_backed_vars.h"

	#include	"controller_initiated.h"

#else

	#include	<stdlib.h>

	#include	"sql_merge.h"

#endif




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	NETWORK_CONFIGURATION_CHANGE_BITFIELD_network_id			(0)
#define	NETWORK_CONFIGURATION_CHANGE_BITFIELD_start_of_irri_day		(1)
#define	NETWORK_CONFIGURATION_CHANGE_BITFIELD_irrigation_off		(2)
#define	NETWORK_CONFIGURATION_CHANGE_BITFIELD_time_zone				(3)
//#define	NETWORK_CONFIGURATION_CHANGE_BITFIELD_nlu_dls_in_use		(4)
#define	NETWORK_CONFIGURATION_CHANGE_BITFIELD_dls_spring_ahead		(5)
#define	NETWORK_CONFIGURATION_CHANGE_BITFIELD_dls_fall_back			(6)
#define	NETWORK_CONFIGURATION_CHANGE_BITFIELD_electrical_limit		(7)
//#define	NETWORK_CONFIGURATION_CHANGE_BITFIELD_nlu_rain_polling_enabled	(8)
#define	NETWORK_CONFIGURATION_CHANGE_BITFIELD_send_flow_recording	(9)
#define nlu_NETWORK_CONFIGURATION_CHANGE_BITFIELD_last_communicated	(10)
#define NETWORK_CONFIGURATION_CHANGE_BITFIELD_controller_names		(11)
#define NETWORK_CONFIGURATION_CHANGE_BITFIELD_water_units			(12)

// ----------

static char* const NETWORK_CONFIG_database_field_names[ (NETWORK_CONFIGURATION_CHANGE_BITFIELD_water_units + 1) ] =
{
	"NetworkID",
	"StartOfIrrigationDay",
	"ScheduledIrrigationIsOff",
	"TimeZone",
	"",
	"",			// The DLS Spring Ahead structure is stored in three fields: DlsSpringMonth, DlsSpringMinDay, DlsSpringMaxDay 
	"",			// The DLS Fall Back structure is stored in three fields: DlsFallMonth, DlsFallMinDay, DlsFallMaxDay 
	"ElectricalStationOnLimit", 	// ElectricalStationOnLimit1 .. 12
	"",
	"SendFlowRecording",
	"", 		// NO LONGER USED - used to be last communicated
	"BoxName",	// BoxName1 .. 12
	"WaterUnits" // 0-gallons, 1-HCF
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

struct NETWORK_CONFIGURATION_STRUCT
{
	// ----------
	
	// 12/4/2013 rmd : The network ID. A unique value assigned by the CommServer web
	// application. Identifies this controller or group of controllers to the web app.
	// Initialized to 0. When comm server receives the 0 it recognizes that as a new chain,
	// chooses the next new value, and returns it to us to set.
	//
	// There is also the concept that if the user edits it on this end as part of a panel
	// replacement, the comm server can send all the data to the panel to restore the panels
	// file base. This variable is sync'd down the chain and therefore there is a change bit for
	// it.
	UNS_32			network_ID;
	
	// ----------
	


	// 12/4/2013 rmd : Perhaps this is set when we detect the user has manually edited the
	// value. I think this is a MASTER only value. Meaning this value is only valid at the
	// master 'A' panel in a chain. This variable is NOT sync'd down the chain therefore we do
	// not need a change bit for it.
	// 
	// 12/3/2014 ajv : The above comment is incorrect. While this value may not be synced down
	// the chain, a change bit is still required if/when the value is changed from Command
	// Center Online. Additionally, since we use the same routines for syncing as we do for
	// processing data to and from Command Center Online, this will inherently become synced.
	// 
	// Regardless, I think that this value is deprecated now that we automatically check for
	// updates every day so we're going to pretend this variable doesn't exist for now. I
	// suspect we'll likely change it to an unused variable to use for some other purpose
	// later.
	UNS_32			unused_a;

	// ----------

	// 12/4/2013 rmd : Is a sync'd variable.
	UNS_32			start_of_irrigation_day;

	// 12/4/2013 rmd : Is a sync'd variable.
	BOOL_32			scheduled_irrigation_is_off;

	// 12/4/2013 rmd : Is a sync'd variable.
	UNS_32			time_zone;

	// 5/5/2014 ajv : Since we need the electrical limit to be distributed down
	// the chain AND we need to know what our local electrical limit is if we're
	// not part of a chain, store the information here.
	UNS_32			electrical_limit[ MAX_CHAIN_LENGTH ];

	// ----------
	
	UNS_32				unused_b;

	// ----------
	
	/* ---------------------------------------------------------- */
	/* DAYLIGHT SAVING */
	/* ---------------------------------------------------------- */

	// 12/4/2013 rmd : The day light savings SPRING AHEAD settings. Is a sync'd variable.
	DAYLIGHT_SAVING_TIME_STRUCT		dls_spring_ahead;

	// 12/4/2013 rmd : The day light savings FALL BACK settings. Is a sync'd variable.
	DAYLIGHT_SAVING_TIME_STRUCT		dls_fall_back;

	/* ---------------------------------------------------------- */

	// 6/8/2016 skc : Site wide display of water in units of gallons or HCF.
	UNS_32				water_units;

	/* ---------------------------------------------------------- */

	// 10/3/2014 ajv : A flag which indicates whether the controller is allowed
	// to send flow recording or not. Since flow recording is typically used to
	// troubleshoot a temporary flow issue, there's no reason to increase data
	// usage and send this all the time. Once implemented, this flag will be
	// able to be set at Command Center Online and will allow sending of flow
	// recording for a specified period of time (e.g., 1 week).
	// 
	// Note that the value is defaulted to FALSE which should prevent sending of
	// flow recording. However, we haven't implemented this yet, so there's no
	// harm in defaulting it to false.
	BOOL_32				send_flow_recording;

	/* ---------------------------------------------------------- */

	// 10/3/2014 ajv : Store the last successful communication with Command
	// Center Online for display on the Real-Time Communications screen. This is
	// actually a WaterSense requirement (has to show that communication is
	// active).
	//
	// 2/20/2015 rmd : Rendered obsolete when we began the move to a permanent server
	// connection. Plan is to show a 'connected' status on status screen.
	DATE_TIME			unused_c;

	UNS_16				padding_16_bit;		// To line up with DATE_TIME field

	/* ---------------------------------------------------------- */

	UNS_32				padding_32_bit_1;
	UNS_32				padding_32_bit_2;
	UNS_32				padding_32_bit_3;
	UNS_32				padding_32_bit_4;
	UNS_32				padding_32_bit_5;
	UNS_32				padding_32_bit_6;

	/* ---------------------------------------------------------- */
	/* CHANGE BITS */
	/* ---------------------------------------------------------- */

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

	/* ---------------------------------------------------------- */
	
	// 4/23/2015 ajv : Added in Rev 2
	char				controller_name[ MAX_CHAIN_LENGTH ][ NUMBER_OF_CHARS_IN_A_NAME ];

	/* ---------------------------------------------------------- */

	// 4/30/2015 ajv : Added in Rev 3
	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;
};
 
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static NETWORK_CONFIGURATION_STRUCT config_n;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern UNS_32 NETWORK_CONFIG_set_network_id( UNS_32 pnetwork_id,
											 const BOOL_32 pgenerate_change_line_bool,
											 const UNS_32 preason_for_change,
											 const UNS_32 pbox_index_0,
											 const BOOL_32 pset_change_bits,
											 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											 #ifdef _MSC_VER
											 , char *pmerge_update_str,
											 char *pmerge_insert_fields_str,
											 char *pmerge_insert_values_str
											 #endif
										   )
{
	UNS_32	rv;

	rv = SHARED_set_uint32_controller( &config_n.network_ID,
									   pnetwork_id,
									   NETWORK_CONFIG_NETWORK_ID_MIN,
									   NETWORK_CONFIG_NETWORK_ID_MAX,
									   NETWORK_CONFIG_NETWORK_ID_DEFAULT,
									   pgenerate_change_line_bool,
									   CHANGE_NETWORK_CONFIG_NETWORK_ID,
									   preason_for_change,
									   pbox_index_0,
									   pset_change_bits,
									   pchange_bitfield_to_set,
									   &config_n.changes_to_upload_to_comm_server,
									   NETWORK_CONFIGURATION_CHANGE_BITFIELD_network_id,
									   NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_network_id ]
									   #ifdef _MSC_VER
									   , pmerge_update_str,
									   pmerge_insert_fields_str,
									   pmerge_insert_values_str
									   #endif
									 );
									 
	// ----------
	
	#ifndef _MSC_VER

		// 9/8/2015 rmd : If a change to the NETWORK ID was made force the controller to
		// re-register. This will produce one of two results. If the controller was previosly
		// registered the commserver will set back the network ID to what the commserver DB says it
		// should be. If the controller was never registered registration will proceed at the
		// commserver regardless of the reported networkID value (0 or otherwise). Note if the
		// commserver orders a change in the controllers network ID via the registration ACK, you
		// will see this line causes yet another registration. Though redundant and unecessary I've
		// deemed acceptable. Hard to stop that yet keep in place the need to register when a change
		// is made via the keypad. Besides its reassurance the controller and commserver DB are in
		// sync and only rarely ever takes place. Perhpas once once when the controller is born.
		if( rv )
		{
			CONTROLLER_INITIATED_update_comm_server_registration_info();
		}

	#endif
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
static void NETWORK_CONFIG_set_start_of_irri_day( UNS_32 pstart_of_irrigation_day,
												  const BOOL_32 pgenerate_change_line_bool,
												  const UNS_32 preason_for_change,
												  const UNS_32 pbox_index_0,
												  const BOOL_32 pset_change_bits,
												  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , char *pmerge_update_str,
												  char *pmerge_insert_fields_str,
												  char *pmerge_insert_values_str
												  #endif
												)
{
	SHARED_set_uint32_controller( &config_n.start_of_irrigation_day,
									   pstart_of_irrigation_day,
									   NETWORK_CONFIG_START_OF_IRRIGATION_DAY_MIN,
									   NETWORK_CONFIG_START_OF_IRRIGATION_DAY_MAX,
									   NETWORK_CONFIG_START_OF_IRRIGATION_DAY_DEFAULT,
									   pgenerate_change_line_bool,
									   CHANGE_NETWORK_CONFIG_START_OF_IRRI_DAY,
									   preason_for_change,
									   pbox_index_0,
									   pset_change_bits,
									   pchange_bitfield_to_set,
									   &config_n.changes_to_upload_to_comm_server,
									   NETWORK_CONFIGURATION_CHANGE_BITFIELD_start_of_irri_day,
									   NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_start_of_irri_day ]
									   #ifdef _MSC_VER
									   , pmerge_update_str,
									   pmerge_insert_fields_str,
									   pmerge_insert_values_str
									   #endif
									 );
}

/* ---------------------------------------------------------- */
extern void NETWORK_CONFIG_set_scheduled_irrigation_off( BOOL_32 pturn_irrigation_off,
														   const BOOL_32 pgenerate_change_line_bool,
														   const UNS_32 preason_for_change,
														   const UNS_32 pbox_index_0,
														   const BOOL_32 pset_change_bits,
														   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
														   #ifdef _MSC_VER
														   , char *pmerge_update_str,
														   char *pmerge_insert_fields_str,
														   char *pmerge_insert_values_str
														   #endif
														 )
{
	#ifndef _MSC_VER

	// 4/5/2013 ajv : If the controller is not OFF, but we've received a change
	// to set it to OFF, stop the irrigation.
	if( (config_n.scheduled_irrigation_is_off == (false)) && (pturn_irrigation_off == (true)) )
	{
		// 4/5/2013 ajv : Remove the appropriate irrigation from both the FOAL
		// and IRRI side. When the change is synced to the other controllers in
		// the chain, they'll stop their own IRRI side irrigation as well.
		__turn_scheduled_irrigation_OFF();
	}
	
	#endif

	SHARED_set_bool_controller( &config_n.scheduled_irrigation_is_off,
									 pturn_irrigation_off,
									 (true),
									 pgenerate_change_line_bool,
									 CHANGE_NETWORK_CONFIG_SCHEDULED_IRRI_IS_OFF,
									 preason_for_change,
									 pbox_index_0,
									 pset_change_bits,
									 pchange_bitfield_to_set,
									 &config_n.changes_to_upload_to_comm_server,
									 NETWORK_CONFIGURATION_CHANGE_BITFIELD_irrigation_off,
									 NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_irrigation_off ]
									 #ifdef _MSC_VER
									 , pmerge_update_str,
									 pmerge_insert_fields_str,
									 pmerge_insert_values_str
									 #endif
								   );

	#ifndef _MSC_VER

		// 10/5/2015 ajv : Trigger a Next Scheduled Irrigation calculation to ensure the controller
		// reports that it's turned OFF or that there's something scheduled.
		postBackground_Calculation_Event( BKGRND_CALC_NEXT_SCHEDULED );

	#endif
}

/* ---------------------------------------------------------- */
/**
 * Sets the time zone.
 * 
 * @executed Executed within the context of the key procesing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param ptime_zone The time zone to set.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * 
 * @return UNS_32 1 if the value was changed; otherwise, 0.
 *
 * @author 10/4/2011 Adrianusv
 *
 * @revisions
 *    10/4/2011 Initial release
 */
extern void NETWORK_CONFIG_set_time_zone( UNS_32 ptime_zone,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0,
											const BOOL_32 pset_change_bits,
											CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											#ifdef _MSC_VER
											, char *pmerge_update_str,
											char *pmerge_insert_fields_str,
											char *pmerge_insert_values_str
											#endif
										  )
{
	SHARED_set_uint32_controller( &config_n.time_zone,
									   ptime_zone,
									   tzHAWAIIAN_STANDARD_TIME,
									   tzUS_EASTERN_STANDARD_TIME,
									   NETWORK_CONFIG_TIME_ZONE_DEFAULT,
									   pgenerate_change_line_bool,
									   CHANGE_NETWORK_CONFIG_TIME_ZONE,
									   preason_for_change,
									   pbox_index_0,
									   pset_change_bits,
									    pchange_bitfield_to_set,
									   &config_n.changes_to_upload_to_comm_server,
									   NETWORK_CONFIGURATION_CHANGE_BITFIELD_time_zone,
									   NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_time_zone ]
									   #ifdef _MSC_VER
									   , pmerge_update_str,
									   pmerge_insert_fields_str,
									   pmerge_insert_values_str
									   #endif
									 );
}

/* ---------------------------------------------------------- */
static void NETWORK_CONFIG_set_electrical_limit( UNS_32 pelectrical_limit,
												   const UNS_32 pcontroller_index,
												   const BOOL_32 pgenerate_change_line_bool,
												   const UNS_32 preason_for_change,
												   const UNS_32 pbox_index_0,
												   const BOOL_32 pset_change_bits,
												   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												   #ifdef _MSC_VER
												   , char *pmerge_update_str,
												   char *pmerge_insert_fields_str,
												   char *pmerge_insert_values_str
												   #endif
												 )
{
	char	lfield_name[ 32 ];

	if( pcontroller_index <= MAX_CHAIN_LENGTH )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_electrical_limit ], (pcontroller_index + 1) );

		SHARED_set_uint32_controller( &config_n.electrical_limit[ pcontroller_index ],
										   pelectrical_limit,
										   ELECTRICAL_LIMIT_MINIMUM,
										   ELECTRICAL_LIMIT_MAXIMUM,
										   ELECTRICAL_LIMIT_DEFAULT,
										   pgenerate_change_line_bool,
										   (CHANGE_NETWORK_CONFIG_ELECTRICAL_LIMIT1 + pcontroller_index),
										   preason_for_change,
										   pbox_index_0,
										   pset_change_bits,
										   pchange_bitfield_to_set,
										   &config_n.changes_to_upload_to_comm_server,
										   NETWORK_CONFIGURATION_CHANGE_BITFIELD_electrical_limit,
										   lfield_name
										   #ifdef _MSC_VER
										   , pmerge_update_str,
										   pmerge_insert_fields_str,
										   pmerge_insert_values_str
										   #endif
										 );
	}
	else
	{
		#ifndef _MSC_VER

		Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
static void NETWORK_CONFIG_set_dls_spring_ahead( DAYLIGHT_SAVING_TIME_STRUCT pdls,
												   const BOOL_32 pgenerate_change_line_bool,
												   const UNS_32 preason_for_change,
												   const UNS_32 pbox_index_0,
												   const BOOL_32 pset_change_bits,
												   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												   #ifdef _MSC_VER
												   , char *pmerge_update_str,
												   char *pmerge_insert_fields_str,
												   char *pmerge_insert_values_str
												   #endif
												 )
{
	SHARED_set_DLS_controller( &config_n.dls_spring_ahead,
									pdls,
									NETWORK_CONFIG_DLS_SPRING_AHEAD_MONTH,
									NETWORK_CONFIG_DLS_SPRING_AHEAD_MIN_DAY,
									NETWORK_CONFIG_DLS_SPRING_AHEAD_MAX_DAY,
									pgenerate_change_line_bool,
									CHANGE_NETWORK_CONFIG_DLS_SPRING_AHEAD,
									preason_for_change,
									pbox_index_0,
									pset_change_bits,
									pchange_bitfield_to_set,
									&config_n.changes_to_upload_to_comm_server,
									NETWORK_CONFIGURATION_CHANGE_BITFIELD_dls_spring_ahead,
									"DLSSpringMonth"
									#ifdef _MSC_VER
									, "DlsSpringMinDay",
									"DlsSpringMaxDay",
									pmerge_update_str,
									pmerge_insert_fields_str,
									pmerge_insert_values_str
									#endif
								  );
}

/* ---------------------------------------------------------- */
static void NETWORK_CONFIG_set_dls_fall_back( DAYLIGHT_SAVING_TIME_STRUCT pdls,
												const BOOL_32 pgenerate_change_line_bool,
												const UNS_32 preason_for_change,
												const UNS_32 pbox_index_0,
												const BOOL_32 pset_change_bits,
												CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												#ifdef _MSC_VER
												, char *pmerge_update_str,
												char *pmerge_insert_fields_str,
												char *pmerge_insert_values_str
												#endif
											  )
{
	SHARED_set_DLS_controller( &config_n.dls_fall_back,
									pdls,
									NETWORK_CONFIG_DLS_FALL_BACK_MONTH,
									NETWORK_CONFIG_DLS_FALL_BACK_MIN_DAY,
									NETWORK_CONFIG_DLS_FALL_BACK_MAX_DAY,
									pgenerate_change_line_bool,
									CHANGE_NETWORK_CONFIG_DLS_FALL_BACK,
									preason_for_change,
									pbox_index_0,
									pset_change_bits,
									pchange_bitfield_to_set,
									&config_n.changes_to_upload_to_comm_server,
									NETWORK_CONFIGURATION_CHANGE_BITFIELD_dls_fall_back,
									"DLSFallBackMonth"
									#ifdef _MSC_VER
									, "DlsFallBackMinDay",
									"DlsFallBackMaxDay",
									pmerge_update_str,
									pmerge_insert_fields_str,
									pmerge_insert_values_str
									#endif
								  );
}

/* ---------------------------------------------------------- */
extern void NETWORK_CONFIG_set_send_flow_recording( BOOL_32 psend_flow_recording,
													  const BOOL_32 pgenerate_change_line_bool,
													  const UNS_32 preason_for_change,
													  const UNS_32 pbox_index_0,
													  const BOOL_32 pset_change_bits,
													  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													  #ifdef _MSC_VER
													  , char *pmerge_update_str,
													  char *pmerge_insert_fields_str,
													  char *pmerge_insert_values_str
													  #endif
													)
{
	SHARED_set_bool_controller( &config_n.send_flow_recording,
									 psend_flow_recording,
									 (false),
									 pgenerate_change_line_bool,
									 CHANGE_NETWORK_CONFIG_SEND_FLOW_RECORDING,
									 preason_for_change,
									 pbox_index_0,
									 pset_change_bits,
									 pchange_bitfield_to_set,
									 &config_n.changes_to_upload_to_comm_server,
									 NETWORK_CONFIGURATION_CHANGE_BITFIELD_send_flow_recording,
									 NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_send_flow_recording ]
									 #ifdef _MSC_VER
									 , pmerge_update_str,
									 pmerge_insert_fields_str,
									 pmerge_insert_values_str
									 #endif
								   );
}

/* ---------------------------------------------------------- */
extern UNS_32 NETWORK_CONFIG_set_controller_name( const char *pcontroller_name,
												  const UNS_32 pcontroller_index_0,
												  const BOOL_32 pgenerate_change_line,
												  const UNS_32 preason_for_change,
												  const UNS_32 pbox_index_0,
												  const BOOL_32 pset_change_bits,
												  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , char *pmerge_update_str,
												  char *pmerge_insert_fields_str,
												  char *pmerge_insert_values_str
												  #endif
												)
{
	char	lfield_name[ 32 ];

	UNS_32	rv;

	rv = 0;

	if( pcontroller_index_0 < MAX_CHAIN_LENGTH )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_controller_names ], (pcontroller_index_0 + 1) );

		rv = SHARED_set_string_controller( config_n.controller_name[ pcontroller_index_0 ],
										   NUMBER_OF_CHARS_IN_A_NAME,
										   pcontroller_name,
										   pgenerate_change_line,
										   CHANGE_NETWORK_CONFIG_CONTROLLER_NAME,
										   preason_for_change,
										   pbox_index_0,
										   pset_change_bits,
										   pchange_bitfield_to_set,
										   &config_n.changes_to_upload_to_comm_server,
										   NETWORK_CONFIGURATION_CHANGE_BITFIELD_controller_names,
										   lfield_name
										   #ifdef _MSC_VER
										   , pmerge_update_str,
										   pmerge_insert_fields_str,
										   pmerge_insert_values_str
										   #endif
										 );
	}
	else
	{
		#ifndef _MSC_VER

			Alert_index_out_of_range();

		#endif
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Sets the display units for water. Mostly applies to Budget screens.
 * 
 * @executed Executed within the context of the key procesing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param punits The display units to use.
 * @param pgenerate_change_line_bool True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *                                    change.
 * 
 * @return void.
 *
 * @author 06/08/2016 skc
 *
 * @revisions
 *    06/08/2016 Initial release
 */
extern void NETWORK_CONFIG_set_water_units( UNS_32 punits,
											const BOOL_32 pgenerate_change_line_bool,
											const UNS_32 preason_for_change,
											const UNS_32 pbox_index_0,
											const BOOL_32 pset_change_bits,
											CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											#ifdef _MSC_VER
											, char *pmerge_update_str,
											char *pmerge_insert_fields_str,
											char *pmerge_insert_values_str
											#endif
										  )
{
	SHARED_set_uint32_controller( &config_n.water_units,
									   punits,
									   NETWORK_CONFIG_WATER_UNIT_GALLONS,
									   NETWORK_CONFIG_WATER_UNIT_HCF,
									   NETWORK_CONFIG_WATER_UNIT_DEFAULT,
									   pgenerate_change_line_bool,
									   CHANGE_NETWORK_CONFIG_WATER_UNITS,
									   preason_for_change,
									   pbox_index_0,
									   pset_change_bits,
									   pchange_bitfield_to_set,
									   &config_n.changes_to_upload_to_comm_server,
									   NETWORK_CONFIGURATION_CHANGE_BITFIELD_water_units,
									   NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_water_units ]
									   #ifdef _MSC_VER
									   , pmerge_update_str,
									   pmerge_insert_fields_str,
									   pmerge_insert_values_str
									   #endif
									 );
}

/* ---------------------------------------------------------- */
extern UNS_32 NETWORK_CONFIG_extract_and_store_changes_from_comm( const UNS_8 *pucp,
																  const UNS_32 preason_for_change,
																  const BOOL_32 pset_change_bits,
																  const UNS_32 pchanges_received_from
																  #ifdef _MSC_VER
																  , char *pSQL_statements,
																  char *pSQL_search_condition_str,
																  char *pSQL_update_str,
																  char *pSQL_insert_fields_str,
																  char *pSQL_insert_values_str,
																  char *pSQL_single_merge_statement_buf,
																  const UNS_32 pnetwork_ID
																  #endif
																 )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	CHANGE_BITS_32_BIT	lbitfield_of_changes;

	UNS_32	lsize_of_bitfield;

	UNS_32	ltemporary_uns_32;

	BOOL_32	ltemporary_bool_32;

	char	ltemporary_str_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	DAYLIGHT_SAVING_TIME_STRUCT	ltemporary_dls_struct;

	UNS_32	i;

	UNS_32	rv;

	// ----------

	rv = 0;

	// ----------

	#ifndef _MSC_VER
	
		/*
		// 8/8/2016 rmd : For debug only.
		if( pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE )
		{
			Alert_Message( "rcvd from slave: NETWORK CONFIG changes" );
		}
		else
		if( pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER )
		{
			Alert_Message( "rcvd from master: NETWORK CONFIG changes" );
		}
		else
		{
			Alert_Message( "rcvd from UNK: NETWORK CONFIG changes" );
		}
		*/
		
		lchange_bitfield_to_set = NETWORK_CONFIG_get_change_bits_ptr( pchanges_received_from );

	#else

		// 7/28/2015 ajv : Initialize to NULL since this variable is used by the CommServer but
		// expected to be NULL.
		lchange_bitfield_to_set = NULL;

		// ----------

		// 1/16/2015 ajv : Make sure the strings are fully initialized to NULL since we rely on this
		// to determine whether the string is empty or not.
		memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

	#endif

	// ----------

	memcpy( &lsize_of_bitfield, pucp, sizeof(lsize_of_bitfield) );
	pucp += sizeof( lsize_of_bitfield );
	rv += sizeof( lsize_of_bitfield );

	memcpy( &lbitfield_of_changes, pucp, sizeof(lbitfield_of_changes) );
	pucp += sizeof( lbitfield_of_changes );
	rv += sizeof( lbitfield_of_changes );

	if( B_IS_SET( lbitfield_of_changes, NETWORK_CONFIGURATION_CHANGE_BITFIELD_network_id ) == (true) )
	{
		memcpy( &ltemporary_uns_32, pucp, sizeof(config_n.network_ID) );
		pucp += sizeof( config_n.network_ID );
		rv += sizeof( config_n.network_ID );

		NETWORK_CONFIG_set_network_id( ltemporary_uns_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
									   #ifdef _MSC_VER
									   , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
									   #endif
									 );
	}

	if( B_IS_SET( lbitfield_of_changes, NETWORK_CONFIGURATION_CHANGE_BITFIELD_start_of_irri_day ) == (true) )
	{
		memcpy( &ltemporary_uns_32, pucp, sizeof(config_n.start_of_irrigation_day) );
		pucp += sizeof( config_n.start_of_irrigation_day );
		rv += sizeof( config_n.start_of_irrigation_day );

		NETWORK_CONFIG_set_start_of_irri_day( ltemporary_uns_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											  #endif
											);
	}

	if( B_IS_SET( lbitfield_of_changes, NETWORK_CONFIGURATION_CHANGE_BITFIELD_irrigation_off ) == (true) )
	{
		memcpy( &ltemporary_bool_32, pucp, sizeof(config_n.scheduled_irrigation_is_off) );
		pucp += sizeof( config_n.scheduled_irrigation_is_off );
		rv += sizeof( config_n.scheduled_irrigation_is_off );

		NETWORK_CONFIG_set_scheduled_irrigation_off( ltemporary_bool_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
													 #ifdef _MSC_VER
													 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
													 #endif
												   );
	}

	if( B_IS_SET( lbitfield_of_changes, NETWORK_CONFIGURATION_CHANGE_BITFIELD_time_zone ) == (true) )
	{
		memcpy( &ltemporary_uns_32, pucp, sizeof(config_n.time_zone) );
		pucp += sizeof( config_n.time_zone );
		rv += sizeof( config_n.time_zone );

		NETWORK_CONFIG_set_time_zone( ltemporary_uns_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
									  #ifdef _MSC_VER
									  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
									  #endif
									);
	}

	if( B_IS_SET( lbitfield_of_changes, NETWORK_CONFIGURATION_CHANGE_BITFIELD_dls_spring_ahead ) == (true) )
	{
		memcpy( &ltemporary_dls_struct, pucp, sizeof(DAYLIGHT_SAVING_TIME_STRUCT) );
		pucp += sizeof( DAYLIGHT_SAVING_TIME_STRUCT );
		rv += sizeof( DAYLIGHT_SAVING_TIME_STRUCT );

		NETWORK_CONFIG_set_dls_spring_ahead( ltemporary_dls_struct, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
											 #ifdef _MSC_VER
											 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											 #endif
										   );
	}

	if( B_IS_SET( lbitfield_of_changes, NETWORK_CONFIGURATION_CHANGE_BITFIELD_dls_fall_back ) == (true) )
	{
		memcpy( &ltemporary_dls_struct, pucp, sizeof(DAYLIGHT_SAVING_TIME_STRUCT) );
		pucp += sizeof( DAYLIGHT_SAVING_TIME_STRUCT );
		rv += sizeof( DAYLIGHT_SAVING_TIME_STRUCT );

		NETWORK_CONFIG_set_dls_fall_back( ltemporary_dls_struct, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
										  #ifdef _MSC_VER
										  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
										  #endif
										);
	}

	if( B_IS_SET( lbitfield_of_changes, NETWORK_CONFIGURATION_CHANGE_BITFIELD_electrical_limit ) == (true) )
	{
		for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
		{
			memcpy( &ltemporary_uns_32, pucp, sizeof(config_n.electrical_limit[ i ]) );
			pucp += sizeof( config_n.electrical_limit[ i ] );
			rv += sizeof( config_n.electrical_limit[ i ] );

			NETWORK_CONFIG_set_electrical_limit( ltemporary_uns_32, i, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
												 #ifdef _MSC_VER
												 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												 #endif
											   );
		}
	}

	if( B_IS_SET( lbitfield_of_changes, NETWORK_CONFIGURATION_CHANGE_BITFIELD_send_flow_recording ) == (true) )
	{
		memcpy( &ltemporary_bool_32, pucp, sizeof(config_n.send_flow_recording) );
		pucp += sizeof( config_n.send_flow_recording );
		rv += sizeof( config_n.send_flow_recording );

		NETWORK_CONFIG_set_send_flow_recording( ltemporary_bool_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
												#ifdef _MSC_VER
												, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												#endif
											  );
	}

	if( B_IS_SET( lbitfield_of_changes, NETWORK_CONFIGURATION_CHANGE_BITFIELD_controller_names ) == (true) )
	{
		for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
		{
			memcpy( &ltemporary_str_48, pucp, sizeof(config_n.controller_name[i]) );
			pucp += sizeof( config_n.controller_name[i] );
			rv += sizeof( config_n.controller_name[i] );

			NETWORK_CONFIG_set_controller_name( ltemporary_str_48, i, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
												#ifdef _MSC_VER
												, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												#endif
											  );
		}
	}

	if( B_IS_SET( lbitfield_of_changes, NETWORK_CONFIGURATION_CHANGE_BITFIELD_water_units ) == (true) )
	{
		memcpy( &ltemporary_uns_32, pucp, sizeof(config_n.water_units) );
		pucp += sizeof( config_n.water_units );
		rv += sizeof( config_n.water_units );

		NETWORK_CONFIG_set_water_units( ltemporary_uns_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
												#ifdef _MSC_VER
												, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												#endif
											  );
	}

	#ifndef _MSC_VER

		if( rv > 0 )
		{
			// 5/13/2015 ajv : We want to save the file in the following cases:
			// 
			//   - A message from the CommServer since we've responded that we received it and need to
			//     ensure it's saved.
			//   - A token response from the slave to the master
			//   - A token from the master to the slave
			//   
			// We DO NOT want to save the file when a token is received by the master since we've
			// already saved the file for one of the reasons above. This will otherwise result in a
			// double save for each change - once when it comes in and once when it's received when the
			// master distributes the changes back out.
			if( (pchanges_received_from == CHANGE_REASON_CENTRAL_OR_MOBILE) ||
				(pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE) ||
				((pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER) && (!FLOWSENSE_we_are_a_master_one_way_or_another())) )
			{
				// 12/10/2014 ajv : Schedule a save rather than saving immediately to prevent stacking of
				// saves. Additionally, set a relatively short time to help cover cases where users make a
				// lot of changes in a short period.
				FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_NETWORK_CONFIGURATION, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
			}
		}
		else
		{
			Alert_bit_set_with_no_data();
		}

	#else

		SQL_MERGE_build_search_condition_uint32( "NetworkID", pnetwork_ID, pSQL_search_condition_str );

		SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements( DATABASE_TABLE_NAME_NETWORKS, pSQL_search_condition_str, SQL_MERGE_include_field_name_in_insert_clause, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf );

	#endif

	return( rv );
}

#ifdef _MSC_VER

/* ---------------------------------------------------------- */
extern INT_32 NETWORK_CONFIG_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order )
{
	INT_32	rv;

	// ----------

	// 4/15/2016 ajv : In most cases, we're going to pass back an array_index of 0. However, in
	// the event the value in question is part of an array, we're going to pass back the actual
	// index so that the CommServer can sort not just on the bit number, but also the
	// array_index for values that share the same bit number.
	*psecondary_sort_order = 0;

	rv = -1;

	// ----------

	if( strncmp( pfield_name, NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_start_of_irri_day ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = NETWORK_CONFIGURATION_CHANGE_BITFIELD_start_of_irri_day;
		*psize_of_var_ptr = sizeof(config_n.start_of_irrigation_day);
	}
	else if( strncmp( pfield_name, NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_irrigation_off ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = NETWORK_CONFIGURATION_CHANGE_BITFIELD_irrigation_off;
		*psize_of_var_ptr = sizeof(config_n.scheduled_irrigation_is_off);
	}
	else if( strncmp( pfield_name, NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_time_zone ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = NETWORK_CONFIGURATION_CHANGE_BITFIELD_time_zone;
		*psize_of_var_ptr = sizeof(config_n.time_zone);
	}
	else if( strncmp( pfield_name, "DlsSpringMonth", NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		// 4/18/2016 ajv : Although the Daylight Saving Time values are not arrays, they comprise a
		// structure which requires that the order be maintained. Therefore, set the secondary sort
		// order accordingly.
		*psecondary_sort_order = 0;

		rv = NETWORK_CONFIGURATION_CHANGE_BITFIELD_dls_spring_ahead;
		*psize_of_var_ptr = sizeof(config_n.dls_spring_ahead.month);
	}
	else if( strncmp( pfield_name, "DlsSpringMinDay", NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		// 4/18/2016 ajv : Although the Daylight Saving Time values are not arrays, they comprise a
		// structure which requires that the order be maintained. Therefore, set the secondary sort
		// order accordingly.
		*psecondary_sort_order = 1;

		rv = NETWORK_CONFIGURATION_CHANGE_BITFIELD_dls_spring_ahead;
		*psize_of_var_ptr = sizeof(config_n.dls_spring_ahead.min_day);
	}
	else if( strncmp( pfield_name, "DlsSpringMaxDay", NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		// 4/18/2016 ajv : Although the Daylight Saving Time values are not arrays, they comprise a
		// structure which requires that the order be maintained. Therefore, set the secondary sort
		// order accordingly.
		*psecondary_sort_order = 2;

		rv = NETWORK_CONFIGURATION_CHANGE_BITFIELD_dls_spring_ahead;
		*psize_of_var_ptr = sizeof(config_n.dls_spring_ahead.max_day);
	}
	else if( strncmp( pfield_name, "DlsFallBackMonth", NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		// 4/18/2016 ajv : Although the Daylight Saving Time values are not arrays, they comprise a
		// structure which requires that the order be maintained. Therefore, set the secondary sort
		// order accordingly.
		*psecondary_sort_order = 0;

		rv = NETWORK_CONFIGURATION_CHANGE_BITFIELD_dls_fall_back;
		*psize_of_var_ptr = sizeof(config_n.dls_fall_back.month);
	}
	else if( strncmp( pfield_name, "DlsFallBackMinDay", NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		// 4/18/2016 ajv : Although the Daylight Saving Time values are not arrays, they comprise a
		// structure which requires that the order be maintained. Therefore, set the secondary sort
		// order accordingly.
		*psecondary_sort_order = 1;

		rv = NETWORK_CONFIGURATION_CHANGE_BITFIELD_dls_fall_back;
		*psize_of_var_ptr = sizeof(config_n.dls_fall_back.min_day);
	}
	else if( strncmp( pfield_name, "DlsFallBackMaxDay", NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		// 4/18/2016 ajv : Although the Daylight Saving Time values are not arrays, they comprise a
		// structure which requires that the order be maintained. Therefore, set the secondary sort
		// order accordingly.
		*psecondary_sort_order = 2;

		rv = NETWORK_CONFIGURATION_CHANGE_BITFIELD_dls_fall_back;
		*psize_of_var_ptr = sizeof(config_n.dls_fall_back.max_day);
	}
	else if( strncmp( pfield_name, NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_electrical_limit ], 23 ) == 0 )
	{
		// 1/13/2015 ajv : Note the above condition is not checking the entire length of the field
		// name. This is because there are 12 unique values: ElectricalStationOnLimit1 -
		// ElectricalStationOnLimit12. By only comparing the first 23 characters, we can ensure we
		// catch all 12 in one condition.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "12" ) != NULL )
		{
			*psecondary_sort_order = 11;
		}
		else if( strstr( pfield_name, "11" ) != NULL )
		{
			*psecondary_sort_order = 10;
		}
		else if( strstr( pfield_name, "10" ) != NULL )
		{
			*psecondary_sort_order = 9;
		}
		else if( strstr( pfield_name, "9" ) != NULL )
		{
			*psecondary_sort_order = 8;
		}
		else if( strstr( pfield_name, "8" ) != NULL )
		{
			*psecondary_sort_order = 7;
		}
		else if( strstr( pfield_name, "7" ) != NULL )
		{
			*psecondary_sort_order = 6;
		}
		else if( strstr( pfield_name, "6" ) != NULL )
		{
			*psecondary_sort_order = 5;
		}
		else if( strstr( pfield_name, "5" ) != NULL )
		{
			*psecondary_sort_order = 4;
		}
		else if( strstr( pfield_name, "4" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}
		else if( strstr( pfield_name, "3" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}

		rv = NETWORK_CONFIGURATION_CHANGE_BITFIELD_electrical_limit;
		*psize_of_var_ptr = sizeof(config_n.electrical_limit[ 0 ]);
	}
	else if( strncmp( pfield_name, NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_send_flow_recording ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = NETWORK_CONFIGURATION_CHANGE_BITFIELD_send_flow_recording;
		*psize_of_var_ptr = sizeof(config_n.send_flow_recording);
	}
	else if( strncmp( pfield_name, NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_controller_names ], 7 ) == 0 )
	{
		// 1/13/2015 ajv : Note the above condition is not checking the entire length of the field
		// name. This is because there are 12 unique values: BoxIndex1 - BoxIndex12. By only comparing
		// the first 7 characters, we can ensure we catch all 12 in one condition.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "12" ) != NULL )
		{
			*psecondary_sort_order = 11;
		}
		else if( strstr( pfield_name, "11" ) != NULL )
		{
			*psecondary_sort_order = 10;
		}
		else if( strstr( pfield_name, "10" ) != NULL )
		{
			*psecondary_sort_order = 9;
		}
		else if( strstr( pfield_name, "9" ) != NULL )
		{
			*psecondary_sort_order = 8;
		}
		else if( strstr( pfield_name, "8" ) != NULL )
		{
			*psecondary_sort_order = 7;
		}
		else if( strstr( pfield_name, "7" ) != NULL )
		{
			*psecondary_sort_order = 6;
		}
		else if( strstr( pfield_name, "6" ) != NULL )
		{
			*psecondary_sort_order = 5;
		}
		else if( strstr( pfield_name, "5" ) != NULL )
		{
			*psecondary_sort_order = 4;
		}
		else if( strstr( pfield_name, "4" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}
		else if( strstr( pfield_name, "3" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}

		rv = NETWORK_CONFIGURATION_CHANGE_BITFIELD_controller_names;
		*psize_of_var_ptr = sizeof(config_n.controller_name[ 0 ]);
	}
	else if( strncmp( pfield_name, NETWORK_CONFIG_database_field_names[ NETWORK_CONFIGURATION_CHANGE_BITFIELD_water_units ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = NETWORK_CONFIGURATION_CHANGE_BITFIELD_water_units;
		*psize_of_var_ptr = sizeof(config_n.water_units);
	}

	return( rv );
}

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

