/*  file 	 alerts.h    12.12.01  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_ALERTS_H
#define _INC_ALERTS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	"lpc_types.h"

// 6/18/2014 ajv : Required for the definition of ALERTS_PILE_STRUCT.
#include	"battery_backed_vars.h"

// 6/18/2014 ajv : Requred for DATE_TIME
#include	"cal_td_utils.h"

// 6/18/2014 ajv : Required to ensure ALERT_INCLUDE_FILENAME and associated
// macros are defined
#include	"cs_common.h"

// 6/18/2014 ajv : Required for the definition of the DATA_HANDLE structure.
#include	"general_picked_support.h"

// 6/18/2014 ajv : Required for COMM_COMMANDS
#include	"packet_definitions.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	ALERTS_PILE_USER			(0)
#define	ALERTS_PILE_CHANGES			(1)
#define	ALERTS_PILE_TP_MICRO		(2)
#define	ALERTS_PILE_ENGINEERING		(3)
#define	ALERTS_PILE_TEMP_FROM_COMM	(4)

#define	ALERTS_NUMBER_OF_PILES		(5)


typedef struct
{
	// A pointer to the address of the alert pile in SRAM.
	void	*addr;

	// The size of the pile in bytes. This should be the same define used when
	// the unsigned character array was defined.
	UNS_32	pile_size;

} ALERT_DEFS;


extern ALERT_DEFS const alert_piles[ ALERTS_NUMBER_OF_PILES ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2012.05.24 rmd : Don't push these values up past 255 as we push them into the
// alert as an UNS_8. The order and value used here MUST have a corresponding text string in
// the irrigation_skipped_reason_str array found in alerts_parsing.c.
#define 	SKIPPED_DUE_TO_CONTROLLER_OFF				(0)
#define 	SKIPPED_DUE_TO_MLB	 						(1)
#define 	SKIPPED_DUE_TO_MANUAL_NOW					(2)
#define 	SKIPPED_DUE_TO_CALENDAR_NOW					(3)
#define 	SKIPPED_DUE_TO_MVOR_CLOSED					(4)
#define 	SKIPPED_DUE_TO_RAIN_TABLE					(5)
#define 	SKIPPED_DUE_TO_RAIN_NEGATIVE_TIME			(6)
#define 	SKIPPED_DUE_TO_RAIN_SWITCH					(7)
#define 	SKIPPED_DUE_TO_FREEZE_SWITCH				(8)
#define 	SKIPPED_DUE_TO_WIND							(9)
#define 	SKIPPED_DUE_TO_WATERSENSE_MIN_CYCLE			(10)
#define 	SKIPPED_DUE_TO_STOP_TIME					(11)
#define 	SKIPPED_DUE_TO_TOO_MANY_STATIONS			(12)
#define		SKIPPED_DUE_TO_MOW_DAY						(13)
#define		SKIPPED_DUE_TO_TWO_WIRE_CABLE_OVERHEATED	(14)
#define		SKIPPED_DUE_TO_STATION_NOT_IN_USE			(15)
#define		SKIPPED_DUE_TO_MOISTURE_BALANCE				(16)
#define		SKIPPED_DUE_TO_MVOR_NO_CLOSE_TIME			(17)
#define		SKIPPED_DUE_TO_MVOR_SAME_OPEN_CLOSE_TIMES	(18)
#define		SKIPPED_DUE_TO_MVOR_CLOSE_BEFORE_OPEN		(19)


/* ---------------------------------------------------------- */

// 7/31/2015 mpd : Added for Lights. 
#define 	LIGHTS_TEXT_SCHEDULED_ON					( 0 )
#define 	LIGHTS_TEXT_SCHEDULED_OFF					( 1 )
#define 	LIGHTS_TEXT_MANUAL_ON						( 2 )
#define 	LIGHTS_TEXT_MANUAL_OFF						( 3 )
#define 	LIGHTS_TEXT_MOBILE_ON						( 4 )
#define 	LIGHTS_TEXT_MOBILE_OFF						( 5 )
#define 	LIGHTS_TEXT_SKIPPED_NO_STOP_TIME			( 6 )
#define 	LIGHTS_TEXT_SKIPPED_NO_RUN_TIME				( 7 )
#define 	nlu_LIGHTS_TEXT_OFF_MODIFIED_BY_MOBILE		( 8 )
#define 	nlu_LIGHTS_TEXT_OFF_MODIFIED_BY_MANUAL		( 9 )
#define 	nlu_LIGHTS_TEXT_OFF_EXTENDED_BY_SCHED		( 10 )
#define 	nlu_LIGHTS_TEXT_FORCED_OFF_CHAIN_DOWN		( 11 )
#define 	nlu_LIGHTS_TEXT_FORCED_OFF_WATCHDOG			( 12 )
#define 	nlu_LIGHTS_TEXT_IRRI_ON_COMPLETED			( 13 )
#define 	nlu_LIGHTS_TEXT_IRRI_OFF_COMPLETED			( 14 )
#define 	nlu_LIGHTS_TEXT_IRRI_REMOVED				( 15 )
#define 	LIGHTS_TEXT_OUTPUT_SHORT_DETECTED			( 16 )
#define 	LIGHTS_TEXT_OUTPUT_NO_CURRENT_DETECTED		( 17 )

/* ---------------------------------------------------------- */

typedef 	UNS_8	REASON_TO_SET_CLOCK;
#define		CLOCKSET_KEYPAD					(0)
#define		CLOCKSET_COMMLINK				(1)
#define		CLOCKSET_DAYLIGHTSAVING			(2)

/* ---------------------------------------------------------- */

typedef 	UNS_8	INITIATED_VIA_ENUM;
#define		INITIATED_VIA_KEYPAD			(0)
#define		INITIATED_VIA_COMM				(1)
#define		INITIATED_VIA_DIRECT_ACCESS		(2)
#define		INITIATED_VIA_MVSHORT			(3)
#define		INITIATED_VIA_MVOR_SCHEDULE		(4)
#define		INITIATED_VIA_MLB				(5)
#define		INITIATED_VIA_SYSTEM_TIMEOUT	(6)
#define		INITIATED_VIA_RADIO_REMOTE		(7)
#define		INITIATED_VIA_LAST_TYPE			(8) // for indexing purposes

/* ---------------------------------------------------------- */

typedef 	UNS_8	CLEARED_MLB_HOW;
#define		CLEAR_MLB_FROM_KEYPAD			(0)
#define		CLEAR_MLB_FROM_COMMLINK			(1)
#define		CLEAR_MLB_SHORT					(2)

/* ---------------------------------------------------------- */
#if 0
typedef 	UNS_8	PASSWORD_RESET_REASON;
#define		PWRESET_UPDATE_FROM_UNKNOWN_ROM_VERSION	(0)
#define		PWRESET_CMOS_UNITIALIZED				(1)
#define		PWRESET_COMMLINK						(2)
 
typedef 	UNS_8	PASSWORD_CANCEL_UNLOCK_REASON;
#define		PW_CANCEL_UNLOCK_KEYPAD_TIMEOUT			(0)
#define		PW_CANCEL_UNLOCK_USER_CANCELLED			(1)

typedef 	UNS_8	PASSWORD_LOGOUT_REASON;
#define		PW_LOGOUT_KEYPAD_TIMEOUT				(0)
#define		PW_LOGOUT_USER_LOGGEDOUT				(1)
#endif
/* ---------------------------------------------------------- */

typedef 	UNS_8	REASONS_TO_PAUSE_RESUME_IRRIGATION;
#define		PR_DUE_TO_WIND							(0)
#define		PR_DUE_TO_KEYPAD						(1)
#define		PR_DUE_TO_CENTRAL_COMMLINK				(2)

/* ---------------------------------------------------------- */

typedef		UNS_8	REASONS_TO_INIT_BATTERY_BACKED_VARS;
#define		INIT_ONLY_IF_CHECK_FAILS				(0)
#define		INIT_BY_FORCE							(1)

/* ---------------------------------------------------------- */

typedef		UNS_8	REASONS_FOR_CTS_TIMEOUT;
#define		CTS_IDLE								(0)
#define		CTS_WAITING								(1)
#define		CTS_TRANSMITTING						(2)

/* ---------------------------------------------------------- */

typedef		UNS_8	MOISTURE_SENSOR_ALERT_TYPE;
#define		MOISTURE_SENSOR_THRESHOLD_CROSSED_UPPER		(0)
#define		MOISTURE_SENSOR_THRESHOLD_CROSSED_LOWER		(1)
#define		MOISTURE_SENSOR_THRESHOLD_WITHIN_RANGE		(2)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// ALERT ID's  ALERT ID's  ALERT ID's  ALERT ID's  ALERT ID's

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/31/2012 rmd : About ALERT ID limitations - first of all note that the alert ID is
// stored as an UNS_16 within the pile itself. This is done to provide the best alert
// density. The higher density resuts in larger display indicie arrays. But this is a
// reasonable 2.4K byte hit wihtin our 8M byte SDRAM.
//
// Note however routine references to the alert id are made as an UNS_32. This is to save
// 400 some bytes of extra code used to manipulate it as an UNS_16. For example in
// application level function calls which have the alert id as an argument.
//
// Therefore the range of the actual alert ids is from 0 to 0xFFFF.

// ----------

#define	AID_ALERT_DIVIDER_LARGE						(0)

#define	AID_USER_RESET								(1)

#define	AID_ALERT_DIVIDER_SMALL						(2)

#define	AID_POWER_FAIL								(3)
#define AID_POWER_FAIL_BROWNOUT						(4)
#define	AID_FIRMWARE_UPDATE							(5)
//#define	AID_FIRMWARE_VERSION_UNRECOGNIZED			(6)
#define AID_MAIN_CODE_NEEDS_UPDATING_AT_SLAVE		(7)
#define AID_TPMICRO_CODE_NEEDS_UPDATING_AT_SLAVE	(8)
#define	AID_PROGRAM_RESTART							(9)
//#define	AID_EXCEPTION								(10)
#define	AID_ERROR									(11)
#define AID_TP_MICRO_ERROR							(12)
#define	AID_WDT										(13)
// 14
#define	AID_ALERTS_PILE_USER_INITIALIZED			(15)
#define	AID_ALERTS_PILE_CHANGE_LINES_INITIALIZED	(16)
#define	AID_ALERTS_PILE_TP_MICRO_INITIALIZED		(17)
#define	AID_ALERTS_PILE_ENGINEERING_INITIALIZED		(18)
//19
#define AID_BATTERY_BACKED_VAR_INITIALIZED			(20)
#define AID_BATTERY_BACKED_VAR_VALID				(21)
// 22
// 23
// 24
#define AID_TASK_FROZEN								(25)

// 26 .. 29

#define	AID_GROUP_NOT_FOUND							(30)
#define	AID_GROUP_NOT_FOUND_WITH_FILENAME			(31)

#define AID_STATION_NOT_FOUND						(32)
#define AID_STATION_NOT_FOUND_WITH_FILENAME			(33)

#define AID_STATION_NOT_IN_GROUP					(34)
#define AID_STATION_NOT_IN_GROUP_WITH_FILENAME		(35)

#define AID_FUNC_CALL_WITH_NULL_PTR					(36)
#define	AID_FUNC_CALL_WITH_NULL_PTR_WITH_FILENAME	(37)

#define AID_INDEX_OUT_OF_RANGE						(38)
#define AID_INDEX_OUT_OF_RANGE_WITH_FILENAME		(39)

#define	AID_ITEM_NOT_ON_LIST						(40)
#define	AID_ITEM_NOT_ON_LIST_WITH_FILENAME			(41)

// 42 
// 43
// 44

#define AID_BIT_SET_WITH_NO_DATA					(45)
#define AID_BIT_SET_WITH_NO_DATA_WITH_FILENAME		(46)

// 47
// 48

#define AID_FLASH_FILE_SYSTEM_PASSED				(49)
#define AID_FLASH_FILE_FOUND						(50)
#define	AID_FLASH_FILE_SIZE_ERROR					(51)
#define AID_FLASH_FILE_FOUND_OLD_VERSION			(52)
#define AID_FLASH_FILE_NOT_FOUND					(53)
#define AID_FLASH_FILE_DELETING_OBSOLETE			(54)
#define AID_FLASH_FILE_OBSOLETE_NOT_FOUND			(55)
#define AID_FLASH_FILE_DELETING						(56)
#define AID_FLASH_FILE_OLD_VERSION_NOT_FOUND		(57)
#define AID_FLASH_FILE_WRITING						(58)
#define AID_FLASH_FILE_WRITE_POSTPONED				(59)

#define	AID_GROUP_NOT_WATERSENSE_COMPLIANT			(60)

#define	AID_SYSTEM_PRESERVES_ACTIVITY				(61)
#define	AID_POC_PRESERVES_ACTIVITY					(62)

// 63 .. 99

#define	AID_UNIT_COMMUNICATED						(100)
#define	AID_COMM_COMMAND_RCVD						(101)
#define	AID_COMM_COMMAND_INIT_ISSUED				(102)
#define	AID_COMM_COMMAND_FAILED						(103)
//#define AID_COMM_COMMAND_MAX_ERROR_COUNT_EXCEEDED	(104)

#define	AID_COMM_CRC_FAILED							(105)
#define	AID_CTS_TIMEOUT								(106)
#define AID_COMM_PACKET_GREATER_THAN_512			(107)
#define AID_COMM_STATUS_TIMER_EXPIRED				(108)

#define AID_OUTBOUND_MESSAGE_SIZE					(109)
#define AID_INBOUND_MESSAGE_SIZE					(110)

#define	AID_NEW_CONNECTION_DETECTED					(111)

#define	AID_DEVICE_POWERED_ON_OR_OFF				(112)

#define	AID_MSG_RESPONSE_TIMEOUT					(113)

#define	AID_COMM_MNGR_BLOCKED_MSG_DURING_IDLE		(114)

#define	AID_LR_RADIO_NOT_COMPAT_WITH_HUB_OPT			(115)
#define	AID_LR_RADIO_NOT_COMPAT_WITH_HUB_OPT_REMINDER	(116)

// 117 .. 149

#define	AID_RANGE_CHECK_FAILED_BOOL								(150)
#define	AID_RANGE_CHECK_FAILED_BOOL_WITH_FILENAME				(151)
#define	AID_RANGE_CHECK_FAILED_BOOL_WITH_GROUP					(152)
#define	AID_RANGE_CHECK_FAILED_BOOL_WITH_GROUP_WITH_FILENAME	(153)

#define	AID_RANGE_CHECK_FAILED_DATE								(155)
#define	AID_RANGE_CHECK_FAILED_DATE_WITH_FILENAME				(156)
#define	AID_RANGE_CHECK_FAILED_DATE_WITH_GROUP					(157)
#define	AID_RANGE_CHECK_FAILED_DATE_WITH_GROUP_WITH_FILENAME	(158)

#define	AID_RANGE_CHECK_FAILED_FLOAT							(160)
#define	AID_RANGE_CHECK_FAILED_FLOAT_WITH_FILENAME				(161)
#define	AID_RANGE_CHECK_FAILED_FLOAT_WITH_GROUP					(162)
#define	AID_RANGE_CHECK_FAILED_FLOAT_WITH_GROUP_WITH_FILENAME	(163)

#define	AID_RANGE_CHECK_FAILED_INT32							(165)
#define	AID_RANGE_CHECK_FAILED_INT32_WITH_FILENAME				(166)
#define	AID_RANGE_CHECK_FAILED_INT32_WITH_GROUP					(167)
#define	AID_RANGE_CHECK_FAILED_INT32_WITH_GROUP_WITH_FILENAME	(168)

#define	AID_RANGE_CHECK_FAILED_STRING_INVALID_CHARS					(170)
#define	AID_RANGE_CHECK_FAILED_STRING_INVALID_CHARS_WITH_FILENAME	(171)

#define	AID_RANGE_CHECK_FAILED_STRING_TOO_LONG						(172)

#define	AID_RANGE_CHECK_FAILED_TIME								(175)
#define	AID_RANGE_CHECK_FAILED_TIME_WITH_FILENAME				(176)
#define	AID_RANGE_CHECK_FAILED_TIME_WITH_GROUP					(177)
#define	AID_RANGE_CHECK_FAILED_TIME_WITH_GROUP_WITH_FILENAME	(178)

#define	AID_RANGE_CHECK_FAILED_UINT32							(180)
#define	AID_RANGE_CHECK_FAILED_UINT32_WITH_FILENAME				(181)
#define	AID_RANGE_CHECK_FAILED_UINT32_WITH_GROUP				(182)
#define	AID_RANGE_CHECK_FAILED_UINT32_WITH_GROUP_WITH_FILENAME	(183)

// 184

#define AID_DERATE_TABLE_UPDATE_FAILED_NO_FLOW		(185)
#define AID_DERATE_TABLE_UPDATE_FAILED_30_PCT_DIFF	(186)
#define AID_DERATE_TABLE_UPDATE_FAILED_50_PCT_DIFF	(187)
#define AID_DERATE_TABLE_UPDATE_SUCCESSFUL			(188)
#define AID_DERATE_TABLE_UPDATE_STATION_COUNT		(189)

// ----------

// 4/29/2015 rmd : Used for both the initial occurrence and the mid-night reminder.
#define	AID_MLB										(201)

//#define	AID_MLB_CLEAR_REQUEST_KEYPAD				(202)
//#define	AID_MLB_CLEAR_REQUEST_COMMLINK				(203)
#define	AID_MLB_CLEARED								(204)

//#define	NLU_AID_MLB_BREAK_INITIAL_REPORT_OF				(205)

// ----------

#define	AID_CONVENTIONAL_STATION_SHORT				(210)

#define	AID_CONVENTIONAL_MV_SHORT					(211)

#define	AID_CONVENTIONAL_PUMP_SHORT					(212)

//#define	AID_CONVENTIONAL_LIGHTS_SHORT				(213)

#define	AID_CONVENTIONAL_OUTPUT_UNKNOWN_SHORT		(214)

#define	AID_CONVENTIONAL_STATION_NO_CURRENT			(215)

#define AID_CONVENTIONAL_MV_NO_CURRENT				(216)

#define AID_CONVENTIONAL_PUMP_NO_CURRENT			(217)


#define	AID_FLOW_NOT_CHECKED_NO_REASONS				(222)
#define AID_FLOW_NOT_CHECKED_FLOW_TOO_HIGH			(223)
#define AID_FLOW_NOT_CHECKED_TOO_MANY_STATIONS		(224)
//#define	AID_FLOW_CHECK_PASSED						(225)
//#define	AID_FLOW_CHECK_LEARNING						(226)

#define	AID_FINAL_HIGHFLOW							(232)
#define	AID_FIRST_HIGHFLOW							(233)

#define	AID_FINAL_LOWFLOW							(242)
#define	AID_FIRST_LOWFLOW							(243)

// ----------

#define	AID_TWO_WIRE_CABLE_EXCESSIVE_CURRENT		(245)

#define	AID_TWO_WIRE_CABLE_OVER_HEATED				(246)

#define	AID_TWO_WIRE_CABLE_COOLED_OFF				(247)

#define	AID_TWO_WIRE_STATION_DECODER_FAULT			(248)

#define	AID_TWO_WIRE_POC_DECODER_FAULT				(249)

// ----------


#define	AID_SCHEDULED_IRRIGATION_STARTED				(250)

#define	AID_SOME_OR_ALL_STATIONS_SKIPPED_THEIR_START	(251)

#define	AID_PROGRAMMED_IRRIGATION_STILL_RUNNING			(252)

#define	AID_HIT_STOPTIME								(253)

#define	AID_IRRIGATION_ENDED							(254)

#define	AID_WALK_THRU_STARTED							(255)

// ----------

#define AID_FLOW_NOT_CHECKED__CYCLE_TOO_SHORT			(256)

#define AID_FLOW_NOT_CHECKED__UNSTABLE_FLOW				(257)

// ----------

#define	AID_POC_NOT_FOUND_EXTRACTING_TOKEN_RESP			(300)

#define	AID_TOKEN_RESP_EXTRACTION_ERROR					(301)

//#define	AID_IRRIGATION_TURNED_OFF_WITH_REASON		(306)
//#define	AID_IRRIGATION_TURNED_ON_WITH_REASON		(307)

// ----------

#define AID_TOKEN_RECEIVED_WITH_NO_FL					(310)
#define AID_TOKEN_RECEIVED_WITH_FL_TURNED_OFF			(311)
#define AID_TOKEN_RECEIVED_BY_FL_MASTER					(312)

#define AID_HUB_RECEIVED_DATA							(313)
#define AID_HUB_FORWARDING_DATA							(314)

#define AID_ROUTER_RECEIVED_UNEXP_CLASS						(315)
#define AID_ROUTER_RECEIVED_UNEXP_BASE_CLASS				(316)
#define AID_ROUTER_RECEIVED_PACKET_NOT_ON_HUB_LIST			(317)
#define AID_ROUTER_RECEIVED_PACKET_NOT_ON_HUB_LIST_WITH_SN	(318)
#define AID_ROUTER_RECEIVED_PACKET_NOT_FOR_US				(319)
#define AID_ROUTER_UNEXP_TO_ADDR							(320)
#define AID_ROUTER_UNK_PORT									(321)
#define AID_ROUTER_RECEIVED_UNEXP_TOKEN_RESP				(322)

// ----------

#define	AID_CI_QUEUED_MSG									(330)

#define	AID_CI_WAITING_FOR_DEVICE							(331)

#define	AID_CI_STARTING_CONNECTION_PROCESS					(332)

#define	AID_CELLULAR_SOCKET_ATTEMPT							(333)

#define	AID_CELLULAR_DATA_CONNECTION_ATTEMPT				(334)

#define	AID_MSG_TRANSACTION_TIME							(335)

#define	AID_LARGEST_TOKEN_SIZE								(336)

// ----------

#define AID_BUDGET_UNDER_BUDGET						(349)
#define	AID_BUDGET_OVER_BUDGET						(350)
#define AID_BUDGET_WILL_EXCEED_BUDGET				(351)
#define AID_BUDGET_NO_BUDGET_VALUES					(352)
//#define AID_BUDGET_ZERO_VALUE						(353)
#define AID_BUDGET_GROUP_REDUCTION					(354)
#define AID_BUDGET_PERIOD_ENDED						(355)
#define AID_BUDGET_OVER_BUDGET_PERIOD_ENDING_TODAY							(356)	// This depracates AIDs 349, 350, and 351

// ----------

//#define	AID_SETHO_BYSTATION							(356)
#define	AID_RESET_MOISTURE_BALANCE_BYGROUP			(357)
#define	AID_RESET_MOISTURE_BALANCE_ALLSTATIONS		(358)

// ----------

#define	AID_SETNOWDAYS_BYSTATION					(359)

#define	AID_SETNOWDAYS_BYGROUP						(360)

#define	AID_SETNOWDAYS_BYALL						(361)

#define	AID_SETNOWDAYS_BYBOX						(362)

// ----------

//#define	AID_PASSWORDS_RESET_FIRMWARE_VERSION		(370)
//#define	AID_PASSWORDS_RESET_SRAM					(371)
//#define	AID_PASSWORDS_RESET_COMMLINK				(372)

// ----------

//#define	AID_PASSWORDS_RECEIVED_FROM_PC				(375)
//#define	AID_PASSWORDS_SENT_TO_PC					(376)

//#define	AID_PASSWORDS_UNLOCKED						(377)
//#define	AID_PASSWORDS_LOGIN							(378)

//#define	AID_PASSWORDS_LOGOUT_KEYPAD_TIMEOUT			(379)
//#define	AID_PASSWORDS_LOGOUT_USER_LOGGED_OUT		(380)
	
//#define	AID_PASSWORDS_CANCEL_UNLOCK_KEYPAD_TIMEOUT	(381)
//#define	AID_PASSWORDS_CANCEL_UNLOCK_USER_CANCELLED	(382)

// ----------

//#define	AID_SYSTEM_CAPACITY_AUTOMATICALLY_ADJUSTED_UP	(400)

// ----------

#define	AID_MVOR_REQUESTED							(420)
#define	AID_MVOR_SKIPPED							(421)

// ----------

#define	AID_ENTERED_FORCED							(430)
#define	AID_LEAVING_FORCED							(431)
//#define	nlu_AID_STARTING_RESCAN						(432)
#define	AID_STARTING_SCAN_WITH_REASON				(433)
#define AID_CHAIN_IS_SAME							(434)
#define AID_CHAIN_HAS_CHANGED						(435)

#define	AID_REBOOT_REQUEST							(436)

// ----------

// AID_RAIN_SWITCH_ACTIVE							(440)
// AID_RAIN_SWITCH_INACTIVE							(441)

// AID_MANUALHO_SKIPPED								(450)
// AID_MANUALPROG_SKIPPED							(451)

// ----------

#define	AID_ETGAGE_FIRST_ZERO						(461)
#define	AID_ETGAGE_SECOND_OR_MORE_ZEROS				(462)

#define	AID_ETGAGE_PERCENT_FULL_EDITED				(463)
#define AID_ETGAGE_PULSE_BUT_NO_GAGE_IN_USE			(464)
#define	AID_ETGAGE_PULSE							(465)
#define	AID_ETGAGE_RUNAWAY							(466)
#define AID_ETGAGE_CLEARED_RUNAWAY					(467)
	
#define	AID_ETGAGE_PERCENT_FULL_WARNING				(468)

// ----------

#define	AID_RAIN_AFFECTING_IRRIGATION				(470)
#define	AID_RAIN_24HOUR_TOTAL						(471)
#define AID_RAIN_PULSE_BUT_NO_BUCKET_IN_USE			(472)

// ----------

#define	AID_RAIN_SWITCH_AFFECTING_IRRIGATION		(473)
#define	AID_FREEZE_SWITCH_AFFECTING_IRRIGATION		(474)

// ----------

#define AID_WIND_DETECTED_BUT_NO_GAGE_IN_USE		(475)

// ----------

#define	AID_STOP_KEY_PRESSED						(476)
 
// ----------

#define	AID_WIND_PAUSED								(477)
#define	AID_WIND_RESUMED							(478)

// ----------

#define	AID_ETTABLE_LOADED							(480)

#define	AID_ACCUM_RAIN_SET_BY_STATION				(482)

#define	AID_FUSE_REPLACED							(485)
#define	AID_FUSE_BLOWN								(486)
//#define	AID_FUSE_REMINDER							(487)

// ----------

#define	AID_MOBILE_STATION_ON						(514)
#define	AID_MOBILE_STATION_OFF						(515)

// ----------

#define	AID_TEST_STATION_STARTED_WITH_REASON		(520)
//#define	AID_TEST_PROGRAM_STARTED_WITH_REASON		(521)
//#define	AID_TEST_ALL_STARTED_WITH_REASON			(522)

// ----------

#define	AID_MANUAL_WATER_STATION_STARTED_WITH_REASON		(525)
#define	AID_MANUAL_WATER_PROGRAM_STARTED_WITH_REASON		(526)
#define	AID_MANUAL_WATER_ALL_STARTED_WITH_REASON			(527)

// ----------

//#define	AID_DIRECT_ACCESS_ENTERED					(530)
//#define	AID_DIRECT_ACCESS_EXITED					(531)
//#define	AID_DIRECT_ACCESS_TIMEDOUT					(532)

// ----------

//#define	AID_CH1794_FAILED_INIT						(545)

// ----------

#define	AID_ET_TABLE_SUBSTITUTION					(552)
#define	AID_ET_TABLE_LIMITED						(553)

// ----------

#define	AID_LIGHT_ID_WITH_TEXT						(560)

// ----------

// at the irri machine when the request comes through these are used...
// could be requested via central comm link
//#define	AID_IRRIGATION_PAUSED						(569)
//#define	AID_IRRIGATION_RESUMED						(570)

// ----------

//#define	AID_POC_MLB_BREAK							(600)
//#define	AID_POC_MLB_REMINDER						(601)
//#define	AID_POC_MLB_CLEARED_KEYPAD					(602)
//#define	AID_POC_MLB_CLEARED_COMMLINK				(603)

//#define	AID_POC_MV_SHORT							(604)

//#define	AID_POC_STARTED_USE							(605)
//#define	AID_POC_HIT_ENDTIME							(606)
//#define	AID_POC_STOPPED_USE							(607)

//#define	AID_POC_MLB_CLEARED_SHORT					(608)

//#define	AID_POC_MV_SHORT_DURING_MLB					(609)

// ----------

//#define	AID_USERDEFINED_PROGRAMTAG_CHANGED			(620)
//#define	AID_PROGRAMTAG_CHANGED						(621)

// ----------

//#define	AID_PERCENTADJUST_ALL_PROGS					(630)
//#define	AID_PERCENTADJUST_BY_PROG					(631)

// ----------

//#define	AID_RRE_PASSWORD_LOGIN						(632)

// ----------

//#define	AID_SIC_STATE_CHANGED						(635)

#define AID_RAIN_SWITCH_ACTIVE						(636)
#define AID_RAIN_SWITCH_INACTIVE					(637)
#define AID_FREEZE_SWITCH_ACTIVE					(638)
#define AID_FREEZE_SWITCH_INACTIVE					(639)

// ----------

#define	NLU_AID_MOISTURE_READING_OBTAINED			(640)
#define	AID_MOISTURE_READING_OUT_OF_RANGE			(641)

#define	AID_SOIL_MOISTURE_CROSSED_THRESHOLD			(642)
#define	AID_SOIL_TEMPERATURE_CROSSED_THRESHOLD		(643)
#define	AID_SOIL_CONDUCTIVITY_CROSSED_THRESHOLD		(644)

#define	AID_MOISTURE_READING_OBTAINED				(650)

// ----------

#define AID_STATION_ADDED_TO_GROUP					(700)
#define AID_STATION_REMOVED_FROM_GROUP				(701)

#define	AID_STATION_COPIED							(702)

#define AID_STATION_MOVED_TO_ANOTHER_GROUP			(703)

// ----------

#define AID_STATION_CARD_ADDED_OR_REMOVED			(705)
#define AID_LIGHTS_CARD_ADDED_OR_REMOVED			(706)
#define AID_POC_CARD_ADDED_OR_REMOVED				(707)
#define AID_WEATHER_CARD_ADDED_OR_REMOVED			(708)
#define AID_COMMUNICATION_CARD_ADDED_OR_REMOVED		(709)
#define AID_STATION_TERMINAL_ADDED_OR_REMOVED		(710)
#define AID_LIGHTS_TERMINAL_ADDED_OR_REMOVED		(711)
#define AID_POC_TERMINAL_ADDED_OR_REMOVED			(712)
#define AID_WEATHER_TERMINAL_ADDED_OR_REMOVED		(713)
#define AID_COMMUNICATION_TERMINAL_ADDED_OR_REMOVED	(714)
#define AID_TWO_WIRE_TERMINAL_ADDED_OR_REMOVED		(715)

// ----------

#define AID_POC_ASSIGNED_TO_MAINLINE				(716)
#define AID_STATION_GROUP_ASSIGNED_TO_MAINLINE		(717)

#define AID_STATION_GROUP_ASSIGNED_TO_A_MOISTURE_SENSOR	(718)

// ----------

#define AID_WALK_THRU_STATION_ADDED_OR_REMOVED	(720)

// ----------

#define AID_ALERT_MESSAGE_FROM_THE_CS3000_COMMSERVER	(1000)

#define AID_ALERT_MESSAGE_FROM_THE_CS3000_WEB_APP		(1001)

#define AID_ALERT_MESSAGE_FROM_THE_CS3000_MOBILE_APP	(1002)

// ----------



/* ---------------------------------------------------------- */
	
// The base change line for indexed variables from which all PData change lines
// are created. There will be ranges of alert ID's for certain variables which
// represent group, controller, and station information.
#define	AID_CHANGE_LINE_BASE						0xC000

// AID's from 0xC000 through 0xFFFF (16,384 of 'em) are taken by the change
// lines. DO NOT USE THEM.

/* ---------------------------------------------------------- */

// 9/10/2014 rmd : Some defines used for the PSEUDO_MS modification we make to the alerts
// seconds time stamp. The 10 high order bits are a counting value used to make alerts made
// within the same second unique. So they aren't rejected as duplicates when rcvd by the web
// app.

// 6/3/2015 rmd : In the commserver this value is treated as ACTUAL milli-seconds. And
// therefore in the commserver cannot exceed 999 otherwise the seconds will bump up by one
// when the value is added to the alerts database time stamp.
#define PSEUDO_MS_MAX	(999)

#define	PSEUDO_MS_SHIFT_10_BIT_VALUE_TO_MSB		(22)

// 9/10/2014 rmd : Clean the 10 high order bits.
#define	PSEUDO_MS_ANDING_MASK_TO_REMOVE			(0x003FFFFF)

/* ---------------------------------------------------------- */

// 10/29/2014 rmd : Fault codes used with the station and poc decoder alert at time of
// fault. KEEP CODES LESS THAN 0XFF. Because we only place a byte on the pile for the code.
#define	STATION_DECODER_FAULT_ALERT_CODE_voltage_too_low	(1)

#define	STATION_DECODER_FAULT_ALERT_CODE_solenoid_short		(2)

#define	STATION_DECODER_FAULT_ALERT_CODE_inoperative		(3)

#define	STATION_DECODER_FAULT_ALERT_CODE_no_current			(4)


#define	POC_DECODER_FAULT_ALERT_CODE_voltage_too_low		(1)

#define	POC_DECODER_FAULT_ALERT_CODE_solenoid_short			(2)

#define	POC_DECODER_FAULT_ALERT_CODE_inoperative			(3)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CS_FILE_NAME( fname ) (RemovePathFromFileName( fname ))

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern BOOL_32 ALERTS_need_to_sync;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

	extern BOOL_32 ALERTS_timestamps_are_synced( void );

	extern DATE_TIME ALERTS_get_oldest_timestamp( void );

	extern BOOL_32 ALERTS_need_latest_timestamp_for_this_controller( const UNS_32 pindex_0 );

	extern BOOL_32 ALERTS_store_latest_timestamp_for_this_controller( UNS_8 **pucp_ptr, const UNS_32 pindex_0 );

	extern BOOL_32 ALERTS_extract_and_store_alerts_from_comm( UNS_8 **pucp, ALERTS_PILE_STRUCT *ppile );

	extern UNS_32 ALERTS_for_controller_to_controller_sync_return_size_of_and_build_alerts_to_send( UNS_8 *pto_where_ptr, ALERTS_PILE_STRUCT *const ppile, const DATE_TIME pdt );
																

	extern void ALERTS_test_all_piles( const BOOL_32 pforce_the_initialization );

	// TODO : Make this static when Bobby's done testing TP Micro messages
	extern void ALERTS_restart_pile( ALERTS_PILE_STRUCT *const ppile, const BOOL_32 pforce_the_initialization );

	extern void ALERTS_restart_all_piles( void );

	// ----------

	extern void ALERTS_inc_index( ALERTS_PILE_STRUCT const *ppile, UNS_32 *pindex_ptr, const UNS_32 phowmany );

	// ----------

	extern void ALERTS_add_alert( const UNS_32 paid, DATA_HANDLE pdh );

	extern void *ALERTS_load_common( UNS_8 *pucp, const UNS_32 paid, DATA_HANDLE *pdhp );

	extern unsigned char *ALERTS_load_object( unsigned char *pucp, const void *from_ptr, const UNS_32 from_ptr_size, DATA_HANDLE *const pdh );

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/**
 * FUNCTION CALLS TO GENERATE ALERTS
 */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifdef ALERT_MESSAGE_NORMAL 

    extern void Alert_Message( char *pmessage );

	extern void Alert_Message_va( char *fmt, ... );
	
	extern void Alert_Message_dt( char *pmessage, const DATE_TIME pdt );

#elif defined( ALERT_MESSAGE_WRITES_TO_DEBUG_PRINTF ) 

    extern void Alert_Message( char *pmessage );

	extern void Alert_Message_va( char *fmt, ... );

	extern void Alert_Message_dt( char *pmessage, const DATE_TIME pdt );

#elif defined( ALERT_MESSAGE_HALTS_AT_CALLER_LOCATION )

	#define Alert_Message( a ) for(;;)

	#define Alert_Message_va( a, ... ) for(;;)
	
	#define Alert_Message_dt( a, b ) for(;;)

#elif defined( ALERT_MESSAGE_GENERATES_NO_CODE_AT_ALL )

	#define Alert_Message( a )

	#define Alert_Message_va( a, ... )
	
	#define Alert_Message_dt( a, b )

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void Alert_power_fail_idx( const DATE_TIME *pdt_ptr, const UNS_32 pcontroller_index );

extern void Alert_power_fail_brownout_idx( const DATE_TIME *pdt_ptr, const UNS_32 pcontroller_index );

extern void Alert_program_restart_idx( const UNS_32 pcontroller_index );

extern void Alert_user_reset_idx( const UNS_32 pcontroller_index );

extern void Alert_firmware_update_idx( char *pfrom, char *pto, const UNS_32 pcontroller_index );

extern void Alert_main_code_needs_updating_at_slave_idx( const UNS_32 pbox_index_0 );

extern void Alert_tpmicro_code_needs_updating_at_slave_idx( const UNS_32 pbox_index_0 );



#if ALERT_INCLUDE_FILENAME

	#define Alert_group_not_found()						Alert_group_not_found_with_filename( __FILE__, __LINE__ )

	#define Alert_station_not_found()					Alert_station_not_found_with_filename( __FILE__, __LINE__ )

	#define Alert_station_not_in_group()				Alert_station_not_in_group_with_filename( __FILE__, __LINE__ )

	#define Alert_func_call_with_null_ptr()				Alert_func_call_with_null_ptr_with_filename( __FILE__, __LINE__ )

	#define Alert_index_out_of_range()					Alert_index_out_of_range_with_filename( __FILE__, __LINE__ )

	#define Alert_item_not_on_list( pitem, plist )		Alert_item_not_on_list_with_filename( #pitem, #plist, __FILE__, __LINE__ )

	#define Alert_bit_set_with_no_data()				Alert_bit_set_with_no_data_with_filename( __FILE__, __LINE__ )


	extern void Alert_group_not_found_with_filename( char *pfilename, const UNS_32 pline_num );

	extern void Alert_station_not_found_with_filename( char *pfilename, const UNS_32 pline_num );

	extern void Alert_station_not_in_group_with_filename( char *pfilename, const UNS_32 pline_num );

	extern void Alert_func_call_with_null_ptr_with_filename( char *pfilename, const UNS_32 pline_num );

	extern void Alert_index_out_of_range_with_filename( char *pfilename, const UNS_32 pline_num );

	extern void Alert_item_not_on_list_with_filename( char *pitem_name, char *plist_name, char *pfilename, const UNS_32 pline_num );

	extern void Alert_bit_set_with_no_data_with_filename( char *pfilename, const UNS_32 pline_num );


	extern void Alert_range_check_failed_bool_with_filename( const char *const pvariable_name, const char *const pgroup_name, const BOOL_32 pvalue_bool, const BOOL_32 pdefault_bool, const char *const pfilename, const UNS_32 pline_num );

	extern void Alert_range_check_failed_int32_with_filename( const char *const pvariable_name, const char *const pgroup_name, const INT_32 pvalue_int32, const INT_32 pdefault_int32, const char *const pfilename, const UNS_32 pline_num );

	extern void Alert_range_check_failed_float_with_filename( const char *const pvariable_name, const char *const pgroup_name, const float pvalue_float, const float pdefault_float, const char *const pfilename, const UNS_32 pline_num );

	extern void Alert_range_check_failed_date_with_filename( const char *const pvariable_name, const char *const pgroup_name, const UNS_32 pdate, const char *const pfilename, const UNS_32 pline_num );

	extern void Alert_range_check_failed_time_with_filename( const char *const pvariable_name, const char *const pgroup_name, const UNS_32 ptime, const char *const pfilename, const UNS_32 pline_num );

	extern void Alert_range_check_failed_string_with_filename( const char *const pvariable_name, const char *const pstr, const char *const pfilename, const UNS_32 pline_num );

	extern void Alert_range_check_failed_uint32_with_filename( const char *const pvariable_name, const char *const pgroup_name, const UNS_32 pvalue, const UNS_32 pdefault, const char *const pfilename, const UNS_32 pline_num );

#else	// !ALERT_INCLUDE_FILENAME

	#define Alert_group_not_found()						Alert_group_not_found_without_filename()

	#define Alert_station_not_found()					Alert_station_not_found_without_filename()

	#define Alert_station_not_in_group()				Alert_station_not_in_group_without_filename()

	#define Alert_func_call_with_null_ptr()				Alert_func_call_with_null_ptr_without_filename()

	#define Alert_index_out_of_range()					Alert_index_out_of_range_without_filename()

	#define Alert_item_not_on_list( pitem, plist )		Alert_item_not_on_list_without_filename( #pitem, #plist )

	#define Alert_bit_set_with_no_data()				Alert_bit_set_with_no_data_without_filename()

	//#define Alert_range_check_failed_bool( pvariable_name, pgroup_name, pvalue_bool )		Alert_range_check_failed_bool_without_filename( pvariable_name, pgroup_name, pvalue_bool )

	//#define Alert_range_check_failed_int32( pvariable_name, pgroup_name, pvalue_int32 )		Alert_range_check_failed_int32_without_filename( pvariable_name, pgroup_name, pvalue_int32 )

	//#define Alert_range_check_failed_float( pvariable_name, pgroup_name, pvalue_float )		Alert_range_check_failed_float_without_filename( pvariable_name, pgroup_name, pvalue_float )

	//#define Alert_range_check_failed_date( pvariable_name, pgroup_name, pdate )				Alert_range_check_failed_date_without_filename( pvariable_name, pgroup_name, pdate )

	//#define Alert_range_check_failed_time( pvariable_name, pgroup_name, ptime )				Alert_range_check_failed_time_without_filename( pvariable_name, pgroup_name, ptime )

	//#define Alert_range_check_failed_string( pvariable_name, pstr )							Alert_range_check_failed_string_without_filename( pvariable_name, pstr )

	//#define Alert_range_check_failed_uint32( pvariable_name, pgroup_name, pvalue )			Alert_range_check_failed_uint32_without_filename( pvariable_name, pgroup_name, pvalue )


	extern void Alert_group_not_found_without_filename( void );

	extern void Alert_station_not_found_without_filename( void );

	extern void Alert_station_not_in_group_without_filename( void );

	extern void Alert_func_call_with_null_ptr_without_filename( void );

	extern void Alert_index_out_of_range_without_filename( void );

	extern void Alert_item_not_on_list_without_filename( char *pitem_name, char *plist_name );

	extern void Alert_bit_set_with_no_data_without_filename( void );

	extern void Alert_range_check_failed_bool_without_filename( const char *const pvariable_name, const char *const pgroup_name, const BOOL_32 pvalue_bool, const BOOL_32 pdefault_bool );

	extern void Alert_range_check_failed_int32_without_filename( const char *const pvariable_name, const char *const pgroup_name, const INT_32 pvalue_int32, const INT_32 pdefault_int32 );

	extern void Alert_range_check_failed_float_without_filename( const char *const pvariable_name, const char *const pgroup_name, const float pvalue_float, const float pdefault_float );

	extern void Alert_range_check_failed_date_without_filename( const char *const pvariable_name, const char *const pgroup_name, const UNS_32 pdate );

	extern void Alert_range_check_failed_time_without_filename( const char *const pvariable_name, const char *const pgroup_name, const UNS_32 ptime );

	extern void Alert_range_check_failed_string_without_filename( const char *const pvariable_name, const char *const pstr );

	extern void Alert_range_check_failed_uint32_without_filename( const char *const pvariable_name, const char *const pgroup_name, const UNS_32 pvalue, const UNS_32 pdefault );

#endif	// ALERT_INCLUDE_FILENAME


extern void Alert_string_too_long( const char *const pstr, const UNS_32 pallowed_length );

extern void Alert_flash_file_system_passed( const UNS_32 pflash_index );

extern void Alert_flash_file_found( char *pfilename, const INT_32 pversion, const INT_32 pedit_count, const INT_32 pitem_size );

extern void Alert_flash_file_size_error( char *pfilename );

extern void Alert_flash_file_found_old_version( char *pfilename );

extern void Alert_flash_file_not_found( char *pfilename );

extern void Alert_flash_file_deleting_obsolete( const UNS_32 pflash_index, char *pfilename );

extern void Alert_flash_file_obsolete_file_not_found( char *pfilename );

extern void Alert_flash_file_deleting( const UNS_32 pflash_index, char *pfilename, const INT_32 pversion, const INT_32 pedit_count, const INT_32 pitem_size );

extern void Alert_flash_file_old_version_not_found( const UNS_32 pflash_index, char *pfilename );

extern void Alert_flash_file_writing( const UNS_32 pflash_index, char *pfilename, const INT_32 pversion, const INT_32 pedit_count, const INT_32 pitem_size );
	
// ------------------

// 8/17/2012 rmd : The 'T' means an alert rcvd via the communications and therefore
// generated by the TP MICRO itself. The 'M' means an alert generated here at the MAIN
// BOARD. These alerts are tpmicro related of course.
extern void Alert_message_on_tpmicro_pile_M( char *pmessage );

extern void Alert_message_on_tpmicro_pile_T( char *pmessage );

// ------------------

extern void Alert_user_pile_restarted( const BOOL_32 preason );

extern void Alert_change_pile_restarted( const BOOL_32 preason );

extern void Alert_tp_micro_pile_restarted( const BOOL_32 preason );

extern void Alert_engineering_pile_restarted( const BOOL_32 preason );

extern void Alert_battery_backed_var_initialized( char *pvar_name, const BOOL_32 preason );

extern void Alert_battery_backed_var_valid( char *pvar_name );

extern void Alert_comm_crc_failed( const UNS_32 pport );

extern void Alert_cts_timeout( const UNS_32 pport, const REASONS_FOR_CTS_TIMEOUT preason );

// ----------

extern void Alert_mainline_break( char * const ppre_str,
								  char * const psystem_name,
								  UNS_32 const pduring_type,
								  UNS_32 const pmeasured,
								  UNS_32 const pallowed );
								  
extern void Alert_mainline_break_cleared( const UNS_32 psystem_gid );

// ----------

extern void Alert_flow_not_checked_with_no_reasons_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0 );

extern void Alert_MVOR_started( char *pgroup_name, const UNS_32 pmvor_action_to_take, const UNS_32 mvor_seconds, const /*INITIATED_VIA_ENUM*/ UNS_8 pinitiated_by );

extern void Alert_MVOR_skipped( char *pgroup_name, const UNS_32 preason_skipped );

extern void Alert_entering_forced( void );

extern void Alert_leaving_forced( void );

extern void Alert_starting_scan_with_reason( const UNS_32 preason );

extern void Alert_reboot_request( const UNS_32 preason );

extern void Alert_rain_affecting_irrigation( void );

extern void Alert_24HourRainTotal( const float prain );

extern void Alert_rain_switch_affecting_irrigation( void );

extern void Alert_freeze_switch_affecting_irrigation( void );

extern void Alert_ET_table_loaded( void );

extern void Alert_accum_rain_set_by_station( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, const UNS_32 pvalue_10u, const INITIATED_VIA_ENUM pwho_initiated_it );

extern void Alert_fuse_replaced_idx( const UNS_32 pbox_index_0 );

extern void Alert_fuse_blown_idx( const UNS_32 pbox_index_0 );

// ----------

// AID = 250
extern void Alert_scheduled_irrigation_started( const UNS_32 preason_in_list );

// AID = 251
extern void Alert_some_or_all_skipping_their_start( const UNS_32 preason_in_list, const UNS_32 preason_skipped );

// AID = 252
extern void Alert_programmed_irrigation_still_running_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0 );

// AID = 253
extern void Alert_hit_stop_time( void );

// AID = 254
extern void Alert_irrigation_ended( const UNS_32 preason_in_list );

// ----------

extern void Alert_flow_not_checked_with_reason_idx( const UNS_32 paid, const UNS_8 pbox_index_0, const UNS_8 pstation_number_0 );

// ----------

extern void Alert_test_station_started_idx( const UNS_32 pbox_index_0, const UNS_32 pstation, const INITIATED_VIA_ENUM pwho_initiated_it );

// AID = 255
extern void Alert_walk_thru_started( char *pgroup_name );

extern void Alert_manual_water_station_started_idx( const UNS_32 pserial_number, const UNS_32 pstation, const INITIATED_VIA_ENUM pwho_initiated_it );
extern void Alert_manual_water_program_started( char *pgroup_name, const INITIATED_VIA_ENUM pwho_initiated_it );
extern void Alert_manual_water_all_started( const INITIATED_VIA_ENUM pwho_initiated_it );

// ----------

extern void Alert_flow_error_idx( const UNS_32 paid,
							  const UNS_32 preason_in_list,    
							  const UNS_32 pbox_index_0,
							  const UNS_32 pstation_number_0,
							  const UNS_32 psystem_expected,
							  const UNS_32 psystem_derated_expected,
							  const UNS_32 psystem_limit,
							  const UNS_32 psystem_actual_flow,
							  const UNS_32 pstation_derated_expected,
							  const UNS_32 pstation_share_of_actual_flow,
							  const UNS_32 pstations_on );

extern void Alert_stop_key_pressed_idx( const UNS_32 psystem_GID, const UNS_32 phighest_reason_in_list, const UNS_32 pbox_index_0 );

extern void Alert_derate_table_update_failed( const UNS_32 paid, const UNS_32 pmeasured, const UNS_32 pexpected, char *pvalves_on_str );

extern void Alert_derate_table_update_successful( const UNS_32 pvalves_on, const UNS_32 pflow_rate, const UNS_32 pindex, const UNS_32 pcount_from, const UNS_32 pcount_to, const INT_16 pvalue_from, const INT_16 pvalue_to );

extern void Alert_derate_table_update_station_count_idx( const UNS_32 pcontroller_index, const UNS_32 pstation_number_0, const UNS_32 pcount );





extern void Alert_conventional_mv_no_current_idx( const UNS_32 pbox_index_0 );

extern void Alert_conventional_pump_no_current_idx( const UNS_32 pbox_index_0 );

extern void Alert_conventional_station_no_current_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0 );


extern void Alert_conventional_station_short_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0 );

extern void Alert_conventional_output_unknown_short_idx( const UNS_32 pbox_index_0 );


extern void Alert_conventional_mv_short_idx( const UNS_32 pbox_index_0 );

extern void Alert_conventional_pump_short_idx( const UNS_32 pbox_index_0 );


extern void Alert_two_wire_cable_excessive_current_idx( const UNS_32 pbox_index_0 );

extern void Alert_two_wire_cable_over_heated_idx( const UNS_32 pbox_index_0 );

extern void Alert_two_wire_cable_cooled_off_idx( const UNS_32 pbox_index_0 );


extern void Alert_two_wire_station_decoder_fault_idx( const UNS_32 pfault_code, const UNS_32 pbox_index_0, const UNS_32 pdecoder_serial_number, const UNS_32 pstation_number_0 );


extern void Alert_two_wire_poc_decoder_fault_idx( const UNS_32 pfault_code, const UNS_32 pbox_index_0, const UNS_32 pdecoder_serial_number, char *ppoc_name );



extern void Alert_ET_Table_substitution_100u( char *pstr, const UNS_32 pdate, const UNS_32 poriginal_table_value_100u, const UNS_32 pnew_value_100u );

extern void Alert_ET_Table_limited_entry_100u( char *pstr, const UNS_32 pdate, const UNS_32 poriginal_table_value_100u, const UNS_32 pnew_value_100u );

extern void Alert_light_ID_with_text( const UNS_32 pbox_index_0, const UNS_32 poutput_index, const UNS_32 text_index );

#if 0
extern void Alert_manual_light_on( const UNS_32 pbox_index_0, const UNS_32 poutput_index );

extern void Alert_manual_light_off( const UNS_32 pbox_index_0, const UNS_32 poutput_index );

extern void Alert_mobile_light_on( const UNS_32 pbox_index_0, const UNS_32 poutput_index );

extern void Alert_mobile_light_off( const UNS_32 pbox_index_0, const UNS_32 poutput_index );

//extern void Alert_test_light_started_idx( const UNS_32 pbox_index_0, const UNS_32 poutput_index, const INITIATED_VIA_ENUM pwho_initiated_it );

extern void Alert_scheduled_lighting_started( const UNS_32 pbox_index_0, const UNS_32 poutput_index );

//extern void Alert_lighting_ended( const UNS_32 preason_in_list );

extern void Alert_light_hit_stop_time( const UNS_32 pbox_index_0, const UNS_32 poutput_index  );

extern void Alert_light_skipped_start( const UNS_32 pbox_index_0, const UNS_32 poutput_index, const UNS_32 preason_in_list, const UNS_32 preason_skipped );
#endif

extern void Alert_unit_communicated( char *pstr );

extern void Alert_set_no_water_days_by_station_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, const UNS_32 pnow_days );

extern void Alert_set_no_water_days_by_group( char *pstr, const UNS_32 pgroup_ID, const UNS_32 pnowdays );

extern void Alert_set_no_water_days_all_stations( const UNS_32 pnowdays );

extern void Alert_set_no_water_days_by_box( const UNS_32 pbox_index_0, const UNS_32 pnow_days );

// ----------

extern void Alert_reset_moisture_balance_all_stations( void );

extern void Alert_reset_moisture_balance_by_group( char *pstr, const UNS_32 pgroup_ID );

// ----------

extern void Alert_ETGage_Pulse( const UNS_32 ppulses );

extern void Alert_ETGage_RunAway( void );

extern void Alert_ETGage_RunAway_Cleared( void );

extern void Alert_ETGage_First_Zero( void );

extern void Alert_ETGage_Second_or_More_Zeros( void );

extern void Alert_ETGage_percent_full_warning( const UNS_32 ppercent_full );

extern void Alert_remaining_gage_pulses_changed( const UNS_32 pfrom, const UNS_32 pto );


extern void Alert_comm_command_received( const COMM_COMMANDS pMID );

extern void Alert_comm_command_sent( const COMM_COMMANDS pMID );

extern void Alert_comm_command_failure( const UNS_32 pci_event );

extern void Alert_outbound_message_size( const UNS_16 pmid, const UNS_32 psize, const UNS_16 ppackets );

extern void Alert_inbound_message_size( const UNS_16 pmid, const UNS_32 psize, const UNS_16 ppackets );

extern void Alert_new_connection_detected( void );

extern void Alert_device_powered_on_or_off( const UNS_32 pport, const BOOL_32 pon_or_off, char *pdevice_name );


extern void Alert_divider_large( void );

extern void Alert_divider_small( void );


extern void Alert_watchdog_timeout( void );

extern void Alert_task_frozen( char *ptaskname, const UNS_32 pseconds );

extern void Alert_flash_file_write_postponed( char *pfilename );

extern void Alert_group_not_watersense_compliant( char *pgroup_name );


extern void Alert_system_preserves_activity( const UNS_32 pactivity );

extern void Alert_poc_preserves_activity( const UNS_32 pactivity );


extern void Alert_packet_greater_than_512( const UNS_32 pport );

extern void Alert_derate_table_flow_too_high( void );

extern void Alert_derate_table_too_many_stations( void );

extern void Alert_chain_is_the_same_idx( const UNS_32 pcontroller_index );

extern void Alert_chain_has_changed_idx( const UNS_32 pcontroller_index );

extern void Alert_et_gage_pulse_but_no_gage_in_use( void );

extern void Alert_rain_pulse_but_no_bucket_in_use( void );

extern void Alert_wind_detected_but_no_gage_in_use( void );

extern void Alert_wind_paused( const UNS_32 pwind_speed_mph );

extern void Alert_wind_resumed( const UNS_32 pwind_speed_mph );

extern void Alert_station_added_to_group_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, char *pgroupname, const UNS_32 preason );

extern void Alert_station_removed_from_group_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, char *pgroupname, const UNS_32 preason );

extern void Alert_station_copied_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, char *pgroupname, const UNS_32 preason );

extern void Alert_station_moved_from_one_group_to_another_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, char *poldgroupname, char *pnewgroupname, const UNS_32 preason );

extern void Alert_status_timer_expired_idx( const UNS_32 pcontroller_index );

extern void Alert_msg_response_timeout_idx( const UNS_32 pcontroller_index );

extern void Alert_comm_mngr_blocked_msg_during_idle( const UNS_32 pcomm_mngr_mode, const BOOL_32 ppending_device_exchange_request, const BOOL_32 ppending_scan, const BOOL_32 ppending_code_receipt );

extern void Alert_lr_radio_not_compatible_with_hub_opt( void );

extern void Alert_lr_radio_not_compatible_with_hub_opt_reminder( void );

extern void Alert_rain_switch_active( void );

extern void Alert_rain_switch_inactive( void );

extern void Alert_freeze_switch_active( void );

extern void Alert_freeze_switch_inactive( void );

extern void Alert_moisture_reading_obtained( const float pmoisture, const INT_32 ptemperature, const float pconductivity );

extern void Alert_moisture_reading_out_of_range( const UNS_32 psensor_model, const UNS_32 pmoisture, const INT_32 ptemperature, const UNS_32 pconductivity );

extern void Alert_soil_moisture_crossed_threshold( const UNS_32 pdecoder_serial_number, const float pmoisture, const float plower_threshold, const float pupper_threshold, const MOISTURE_SENSOR_ALERT_TYPE pmois_alert_type );

extern void Alert_soil_temperature_crossed_threshold( const UNS_32 pdecoder_serial_number, const INT_32 ptemperature, const INT_32 plower_threshold, const INT_32 pupper_threshold, const MOISTURE_SENSOR_ALERT_TYPE pmois_alert_type );

extern void Alert_soil_conductivity_crossed_threshold( const UNS_32 pdecoder_serial_number, const float pconductivity, const float plower_threshold, const float pupper_threshold, const MOISTURE_SENSOR_ALERT_TYPE pmois_alert_type );

extern void Alert_station_card_added_or_removed_idx( const UNS_32 pbox_index_0, const UNS_32 pcard_number_0, const BOOL_32 pinstalled );

extern void Alert_lights_card_added_or_removed_idx( const UNS_32 pbox_index_0, const BOOL_32 pinstalled );

extern void Alert_poc_card_added_or_removed_idx( const UNS_32 pbox_index_0, const BOOL_32 pinstalled );

extern void Alert_weather_card_added_or_removed_idx( const UNS_32 pbox_index_0, const BOOL_32 pinstalled );

extern void Alert_communication_card_added_or_removed_idx( const UNS_32 pbox_index_0, const BOOL_32 pinstalled );

extern void Alert_station_terminal_added_or_removed_idx( const UNS_32 pbox_index_0, const UNS_32 pterminal_number_0, const BOOL_32 pinstalled );

extern void Alert_lights_terminal_added_or_removed_idx( const UNS_32 pbox_index_0, const BOOL_32 pinstalled );

extern void Alert_poc_terminal_added_or_removed_idx( const UNS_32 pbox_index_0, const BOOL_32 pinstalled );

extern void Alert_weather_terminal_added_or_removed_idx( const UNS_32 pbox_index_0, const BOOL_32 pinstalled );

extern void Alert_communication_terminal_added_or_removed_idx( const UNS_32 pbox_index_0, const BOOL_32 pinstalled );

extern void Alert_two_wire_terminal_added_or_removed_idx( const UNS_32 pbox_index_0, const BOOL_32 pinstalled );

extern void Alert_poc_assigned_to_mainline( char *ppoc_name, char *psystem_name, const UNS_32 preason, const UNS_32 pbox_index_0 );

extern void Alert_station_group_assigned_to_mainline( char *pgroup_name, char *psystem_name, const UNS_32 preason, const UNS_32 pbox_index_0 );

extern void Alert_station_group_assigned_to_a_moisture_sensor( char *pstation_group_name, char *pmoisture_sensor_name, const UNS_32 preason, const UNS_32 pbox_index_0 );



extern void Alert_mobile_station_on( const UNS_32 pbox_index_0, const UNS_32 pstation );

extern void Alert_mobile_station_off( const UNS_32 pbox_index_0, const UNS_32 pstation );

extern void Alert_poc_not_found_extracting_token_resp( const UNS_32 pdecoder_sn, const UNS_32 pbox_index_0 );

extern void Alert_token_resp_extraction_error( void );

extern void Alert_token_resp_extraction_error( void );

extern void Alert_token_rcvd_with_no_FL( void );

extern void Alert_token_rcvd_with_FL_turned_off( void );

extern void Alert_token_rcvd_by_FL_master( void );

extern void Alert_hub_rcvd_packet( const UNS_32 prouter_type, const UNS_32 pfrom_which_port, const UNS_32 prouting_class_base, const UNS_32 pdata_len );

extern void Alert_hub_forwarding_packet( const UNS_32 prouter_type, const UNS_32 pfrom_which_port, const UNS_32 prouting_class_base, const UNS_32 pdata_len );

extern void Alert_router_rcvd_unexp_class( const UNS_32 prouter_type, const UNS_32 pfrom_which_port, const UNS_32 prouting_class_base, const UNS_32 prouting_class );

extern void Alert_router_rcvd_unexp_base_class( const UNS_32 prouter_type, const UNS_32 pfrom_which_port, const UNS_32 prouting_class_base );

extern void Alert_router_rcvd_packet_not_on_hub_list( const UNS_32 prouter_type, const UNS_32 pfrom_which_port, const UNS_32 prouting_class_base );

extern void Alert_router_rcvd_packet_not_on_hub_list_with_sn( const UNS_32 prouter_type, const UNS_32 pfrom_which_port, const UNS_32 prouting_class_base, const UNS_32 pserial_num );

extern void Alert_router_rcvd_packet_not_for_us( const UNS_32 prouter_type, const UNS_32 pfrom_which_port, const UNS_32 prouting_class_base );

extern void Alert_router_unexp_to_addr_port( const UNS_32 prouter_type, const UNS_32 pfrom_which_port );

extern void Alert_router_unk_port( const UNS_32 prouter_type );

extern void Alert_router_received_unexp_token_resp( const UNS_32 prouter_type, const UNS_32 pfrom_which_port );

extern void Alert_ci_queued_msg( const UNS_32 psend_to_front, const UNS_32 pevent );

extern void Alert_ci_waiting_for_device( void );

extern void Alert_ci_starting_connection_process( void );

extern void Alert_cellular_socket_attempt( void );

extern void Alert_cellular_data_connection_attempt( void );

extern void Alert_msg_transaction_time( const UNS_32 ptransact_time );

extern void Alert_largest_token_size( const UNS_32 ptoken_size, const BOOL_32 pis_resp );

extern void Alert_budget_under_budget( char* ppoc_name, UNS_32 percent );

extern void Alert_budget_over_budget( char* ppoc_name, UNS_32 percent, UNS_32 percent2 );

extern void Alert_budget_will_exceed_budget( char* ppoc_name, UNS_32 percent );

extern void Alert_budget_values_not_set( char* psys_name, char* ppoc_name );

extern void Alert_budget_group_reduction( char* pgroup_name, UNS_32 reduction, BOOL_32 limit_hit );

extern void Alert_budget_period_ended( char* poc_name, UNS_32 budget, UNS_32 used );

extern void Alert_budget_over_budget_period_ending_today( char *ppoc_name, const UNS_32 ppercent_over_budget );

extern void Alert_walk_thru_station_added_or_removed( const UNS_32 pbox_index_0, char *pgroupname, const INT_32 pstation, const BOOL_32 padded, const UNS_32 preason );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void ALERTS_generate_all_alerts_for_debug( const BOOL_32 preset_all_piles );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

