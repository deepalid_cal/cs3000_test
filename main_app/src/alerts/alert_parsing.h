/*  file = alert_parsing.h                  2014.06.03   ajv  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_ALERT_PARSING_H
#define _INC_ALERT_PARSING_H


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs3000_comm_server_common.h"

#include	"lpc_types.h"

// 6/18/2014 ajv : Required for the definition of ALERTS_PILE_STRUCT.
#include	"battery_backed_vars.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2002.02.14 rmd : This defines the storage area used when parsing for display
// (we also parse to tell line byte count). WE ARE ASSUMING WHEN WE STORE THE
// MAXIMUM LENGTH TEXT STRING THAT SUCH LINES DO NOT INCLUDE ANYTHING ELSE (they
// can by the factor of 20 parsed charaters - but shouldn't). Need +1 for the
// NULL char! Currently this equates to 99.
//
// 2002.02.20 rmd : Well this doesn't play out too well because of lines like
// HIGH FLOW with umpteen variables to display so just kick it up to 256 bytes.
//
#define ALERTS_MAX_TOTAL_PARSED_LENGTH		(256)

// ----------

#ifdef _MSC_VER

	// 6/18/2014 ajv : Define the ALERTS_PILE_STRUCT as an unsigned char for the
	// CommServer since it doesn't require the rest of the information
	// associated with the pile
	#define ALERTS_PILE_STRUCT	unsigned char

#endif

// ----------

#define PARSE_FOR_LENGTH_ONLY				(100)
#define PARSE_FOR_LENGTH_AND_TEXT			(200)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

extern void ALERTS_parse_comm_command_string( const UNS_32 mid, void *pdest, const UNS_32 pdest_size );

extern char *ALERTS_get_station_number_with_or_without_two_wire_indicator( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, char *sta_num_str, const UNS_32 size_of_str );

#endif

// ----------

extern UNS_32 ALERTS_pull_object_off_pile( ALERTS_PILE_STRUCT *const ppile, void *toptr, UNS_32 *pindex, const UNS_32 psize_of_object );

extern UNS_32 ALERTS_pull_string_off_pile( ALERTS_PILE_STRUCT *const ppile, char *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr );

// ----------

DLLEXPORT BOOL_32 ALERTS_alert_is_visible_to_the_user( const UNS_32 paid, const BOOL_32 pinclude_alerts, const BOOL_32 pinclude_changes );

DLLEXPORT UNS_32 nm_ALERT_PARSING_parse_alert_and_return_length( const UNS_32 paid, ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, char *pdest_ptr, const UNS_32 pallowable_size, UNS_32 pindex );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

