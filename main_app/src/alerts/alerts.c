/*  file = alerts.c                            12.12.01  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for va_list variable type and va_ routines
#include	<stdarg.h>

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"alerts.h"

#include	"alert_parsing.h"

#include	"app_startup.h"

#include	"battery_backed_vars.h"

#include	"change.h"

#include	"configuration_network.h"

#include	"d_process.h"

#include	"epson_rx_8025sa.h"

#include	"flowsense.h"

#include	"irri_comm.h"

#include	"m_main.h"

#include	"r_alerts.h"

#include	"controller_initiated.h"

#include	"wdt_and_powerfail.h"

#include	"packet_router.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/*
  THIS FILE SHOULD NOT CONTAIN ANY MEM_MALLOC OR MEM_FREE CALLS. DUE TO THE
  POSSIBLITY OF A DEADLOCK BETWEEN THE MEM_MALLOC FUNCTIONS AND THE
  ALERT_MESSAGE FUNCTIONS. IT IS EVEN MORE POSSIBLE IF YOU ARE LOOKING AT THE
  ALERTS DISPLAY AND WE ARE REDRAWING IT AND THAT MAKES A MEM_MALLOC OR MEM_FREE
  CALL.

  The strict rule is that in between taking the alerts_pile_recursive_MUTEX and
  releasing it we cannot use mem_malloc or mem_free. Because if those functions
  have an anomaly and attempt an alert they would be blocked until the
  alerts_pile_recursive_MUTEX was released. And if releasing it took a
  mem_malloc ... well then both tasks would be blocked. Forever.

  Same argument goes for displaying alerts. No calls to mem_malloc or mem_free.
*/

#undef mem_malloc
#define mem_malloc( u ) (do not use in this file)

#undef mem_free
#define mem_free( p ) (do not use in this file)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

ALERTS_PILE_STRUCT	alerts_struct_temp_from_comm;	// Not battery backed

unsigned char		alerts_pile_temp_from_comm[ ALERT_PILE_BYTES ];	// Not battery backed

// ----------

/**
 * The alert piles themselves are now stored independent of the alert
 * structures. The structure simply contains an index into this array which
 * indicates which alert pile the structure uses.
 */
ALERT_DEFS const alert_piles[ ALERTS_NUMBER_OF_PILES ] =
{
	{ &alerts_pile_user, ALERT_PILE_BYTES },
	{ &alerts_pile_changes, ALERT_PILE_BYTES },
	{ &alerts_pile_tp_micro, TPMICRO_ALERTS_PILE_BYTES },
	{ &alerts_pile_engineering, ENGINEERING_PILE_BYTES },
	{ &alerts_pile_temp_from_comm, ALERT_PILE_BYTES }
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/6/2015 rmd : These string are limited to 16 bytes. And MUST be changed if the
// accompanying structure definition they are used in is changed.

// 8/6/2015 rmd : Do not exceed (BB_PRE_STRING_LENGTH - 1) visible characters. If you do you
// can end up changing the version number with no recognition of the change. Follow the code
// through to understand this.

static const char ALERTS_USER_VERIFY_STRING_PRE[]  			= "USER v01";

static const char ALERTS_CHANGES_VERIFY_STRING_PRE[]		= "CHANGES v01";

static const char ALERTS_TP_MICRO_VERIFY_STRING_PRE[]		= "TPMICRO v01";

static const char ALERTS_ENGINEERING_VERIFY_STRING_PRE[]	= "ENGINEER v01";

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/9/2014 rmd : The psudo milli-second counter used to insert something unique into alerts
// made within the same second. Able to make up to 999 of the same alert within a second
// appear as unique. So they stay in the proper chronological order in the commserver alerts
// database.
static UNS_32 pseudo_ms;

static UNS_32 pseudo_ms_last_second_an_alert_was_made;

// ----------

// NOTE: When being set, this MUST be protected by the
// alerts_pile_recursive_MUTEX to ensure a new alert isn't added while an
// action is being taken based upon this variable's value.
BOOL_32 ALERTS_need_to_sync;

// The latest alert timestamp for each controller in the chain. Whenever a new
// alert is added to the FOAL master's user alert pile, this setting is
// initialized to the minimum date/time. This triggers foal_comm to request th
// latest timestamp from each slave to determine which alerts to send. When
// finally ready to distribute the alerts, the FOAL master sends all of the
// alerts after and including the oldest timestamp.
static DATE_TIME ALERTS_latest_timestamps_by_controller[ MAX_CHAIN_LENGTH ];

/* ---------------------------------------------------------- */

static BOOL_32 ALERTS_add_alert_to_pile( ALERTS_PILE_STRUCT *const ppile, DATA_HANDLE pdh );

/* ---------------------------------------------------------- */
/**
 * Checks whether all of the active controllers have sent their latest alert
 * timestamp to the master or not.
 *
 * @mutex_requirements Doesn't require the caller to have a specific mutex, but
 * this function takes the iccc_recursive_MUTEX mutex.
 *
 * @task_execution Executed within the context of the COMM_MNGR task when
 * building an irrigation token after a new alert was added to the FOAL master.
 *
 * @return BOOL_32 True if all of the active controllers have sent their latest
 * alert timestamp; otherwise, false.
 *
 * @author 8/3/2012 Adrianusv
 *
 * @revisions (none)
 */
extern BOOL_32 ALERTS_timestamps_are_synced( void )
{
	// 4/3/2014 rmd : This function is called within the context of the comm_mngr task. While
	// constructing the irrigation token. I believe it is only called by the master.
	
	// ----------
	
	UNS_32  i;

	BOOL_32 rv;

	rv = (true);

	// ----------
	
	// Take the iccc recursive mutex to ensure it doesn't change while checking the state of
	// each controller.
	xSemaphoreTakeRecursive( chain_members_recursive_MUTEX, portMAX_DELAY );

	for( i=0; i<MAX_CHAIN_LENGTH; i++ )
	{
		// 4/16/2014 rmd : This function is called while constructing the irrigation token. Only by
		// the master. Therefore the chain members saw_during_the_scan variable is valid.
		//
		// If we find a controller which is active but the timestamp is not set yet, return false
		// and terminate the loop.
		if( chain.members[ i ].saw_during_the_scan && (ALERTS_latest_timestamps_by_controller[ i ].D == DATE_MINIMUM) )
		{
			rv = (false);

			break;
		}
	}

	xSemaphoreGiveRecursive( chain_members_recursive_MUTEX );
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Traverses through the ALERTS_latest_timestamps_by_controller array and
 * identifies the oldest alert timestamp reported by a controller on the
 * FLOWSENSE chain.
 *
 * @mutex_requirements Doesn't require the caller to have a specific mutex, but
 * this function takes the iccc_recursive_MUTEX mutex.
 *
 * @task_execution Executed within the context of the COMM_MNGR task when
 * building an irrigation token after a new alert was added to the FOAL master.
 *
 * @return DATE_TIME The oldest alert timestamp stored in the
 * ALERTS_latest_timestamps_by_controller array. If this routine is somehow
 * called after the array has been cleared (which shouldn't happen), the
 * DATE_DEFAULT and TIME_DEFAULT are returned.
 *
 * @author 8/3/2012 Adrianusv
 *
 * @revisions (none)
 */
extern DATE_TIME ALERTS_get_oldest_timestamp( void )
{
	// 4/16/2014 rmd : This function is called within the context of the comm_mngr task. While
	// constructing the irrigation token. Only by the master. Therefore the chain members saw
	// during the scan variable is valid.

	// ----------
	
	DATE_TIME   ldt;

	UNS_32  i;

	ldt.D = DATE_MINIMUM;
	ldt.T = TIME_DEFAULT;

	// Take the iccc recursive mutex to ensure it doesn't change while checking the state of
	// each controller.
	xSemaphoreTakeRecursive( chain_members_recursive_MUTEX, portMAX_DELAY );

	for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
	{
		// 4/16/2014 rmd : This function is called while constructing the irrigation token. Only by
		// the master. Therefore the chain members saw_during_the_scan variable is valid.
		if( chain.members[ i ].saw_during_the_scan && (ALERTS_latest_timestamps_by_controller[ i ].D != DATE_MINIMUM) )
		{
			// Initialize ldt to the first valid date and time we find. Any
			// subsequent date and times are compared to ldt to determine which
			// is the oldest.
			if( (ldt.D == DATE_MINIMUM) && (ldt.T == TIME_DEFAULT) )
			{
				ldt.D = ALERTS_latest_timestamps_by_controller[ i ].D;
				ldt.T = ALERTS_latest_timestamps_by_controller[ i ].T;
			}
			else if( DT1_IsBiggerThan_DT2( &ldt, &ALERTS_latest_timestamps_by_controller[ i ] ) == (true) )
			{
				ldt.D = ALERTS_latest_timestamps_by_controller[ i ].D;
				ldt.T = ALERTS_latest_timestamps_by_controller[ i ].T;
			}
		}
	}

	xSemaphoreGiveRecursive( chain_members_recursive_MUTEX );

	return( ldt );
}

/* ---------------------------------------------------------- */
/**
 * Determines whether the latest alert timestamp for the specified controller is
 * up-to-date or not.
 *
 * @task_execution Executed within the context of the COMM_MNGR task when
 * building an irrigation token after a new alert was added to the FOAL master.
 *
 * @param pindex_0 The 0-based index into the
 * ALERTS_latest_timestamps_by_controller array. This should typically be
 * next_contact.index.
 *
 * @return BOOL_32 True if the specified controller's latest timestamp needs to be
 * retrieved; otherwise, false.
 *
 * @author 8/3/2012 Adrianusv
 *
 * @revisions (none)
 */
extern BOOL_32 ALERTS_need_latest_timestamp_for_this_controller( const UNS_32 pindex_0 )
{
	BOOL_32     rv;

	rv = (false);

	if( (ALERTS_latest_timestamps_by_controller[ pindex_0 ].D == DATE_MINIMUM) && (ALERTS_latest_timestamps_by_controller[ pindex_0 ].T == TIME_DEFAULT) )
	{
		rv = (true);
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Stores the latest timestamp from the specified controller by pulling it out
 * of the token response.
 *
 * @task_execution Executed within the context of the COMM_MNGR task when the
 * FOAL master processes an irrigation data token response containing an alert
 * timestamp.
 *
 * @param pucp_ptr A pointer to a pointer of unsigned bytes which contains the
 * timestamp of the latest alert.
 *
 * @param pindex_0 The 0-based index into the
 * ALERTS_latest_timestamps_by_controller array.
 *
 * @return BOOL_32 True if the process was successful; otherwise, false. If
 * something fails, the token response parsing should be terminated.
 *
 * @author 8/3/2012 Adrianusv
 *
 * @revisions (none)
 */
extern BOOL_32 ALERTS_store_latest_timestamp_for_this_controller( UNS_8 **pucp_ptr, const UNS_32 pindex_0 )
{
	BOOL_32 rv;

	rv = (true);

	memcpy( &ALERTS_latest_timestamps_by_controller[ pindex_0 ], *pucp_ptr, sizeof(DATE_TIME) );

	*pucp_ptr += sizeof(DATE_TIME);

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Determines whether the alert specified by the passed-in data handle already
 * exists in the specified alert pile or not.
 *
 * @mutex_requirements Doesn't require the caller to have a specific mutex, but
 * this function takes the alerts_pile_recursive_MUTEX mutex.
 *
 * @task_execution Executed within the context of the COMM_MNGR task when the
 * FOAL slave processes an incoming irrigation data token containing alert data.
 *
 * @param ppile A pointer to the pile to check. So far, this should only be the
 * alerts_struct_user pile.
 *
 * @param pdh The data handle containing the alert data.
 *
 * @return BOOL_32 True if the passed-in alert already exists in the specified
 * pile; otherwise, false.
 *
 * @author 8/3/2012 Adrianusv
 *
 * @revisions (none)
 */
static BOOL_32 ALERTS_alert_already_exists_on_pile( ALERTS_PILE_STRUCT *const ppile, DATA_HANDLE pdh )
{
	ALERTS_DISPLAY_STRUCT   *ldisplay_struct;

	unsigned char *ucp_to_user_pile, *ucp_to_new_alert;

	DATE_TIME   ldt, lnew_dt;

	UNS_32      laid, lnew_aid;

	UNS_32      lalert_length;

	UNS_32      lindex_of_start_of_alert;

	UNS_32      lindex_of_aid_and_dt;

	UNS_32      i, j;

	BOOL_32     rv;


	ldisplay_struct = ALERTS_get_display_structure_for_pile( ppile );

	rv = (false);

	// Take the alerts mutex to ensure no alerts are added or rolled out during
	// this process
	xSemaphoreTakeRecursive( alerts_pile_recursive_MUTEX, portMAX_DELAY );

	// ----------

	// 10/31/2012 rmd : The alert ID exists on the pile as an UNS_16. It is best to work outside
	// the pile with the alert ID as an UNS_32. That is more efficient code space wise. Use a
	// temporary UNS_16 variable to obtain the aid and assign to the broadly lnew_aid.
	UNS_16  tmp_aid;

	memcpy( &tmp_aid, pdh.dptr, sizeof(UNS_16) );

	lnew_aid = tmp_aid;

	// 10/31/2012 rmd : Pull the date and time.
	memcpy( &lnew_dt, (pdh.dptr + sizeof(UNS_16)), sizeof(lnew_dt) );

	// ----------

	for( i = 0; i < ppile->count; ++i )
	{
		// This array contains the start indicies from NEWEST to OLDEST
		lindex_of_start_of_alert = ldisplay_struct->all_start_indicies[ i ];

		// Get a copy of the entire alert so we can pull of the alert ID and
		// date and time without moving the index of the start of the alert.
		lindex_of_aid_and_dt = lindex_of_start_of_alert;

		// 10/31/2012 rmd : Get the alert ID which exists on the pile as an UNS_16. It is best to
		// work outside the pile with the alert ID as an UNS_32. That is more efficient code space
		// wise. Use a temporary UNS_16 variable to obtain the aid and assign to the broadly
		// lnew_aid.
		ALERTS_pull_object_off_pile( ppile, &tmp_aid, &lindex_of_aid_and_dt, sizeof(UNS_16) );

		laid = tmp_aid;

		// Next, get the timestamp of the alert
		ALERTS_pull_object_off_pile( ppile, &ldt, &lindex_of_aid_and_dt, sizeof(ldt) );

		// If the alert IDs and time stamps match, we need to check the rest of
		// the alert to see whether it matches.
		if( (laid == lnew_aid) && (DT1_IsEqualTo_DT2( &ldt, &lnew_dt ) == (true)) )
		{
			// Assume they match at the outset. If a byte doesn't match during
			// the comparison, this will be set false.
			rv = (true);

			// Get the size of the alert
			lalert_length = nm_ALERTS_parse_alert_and_return_length( ppile, PARSE_FOR_LENGTH_ONLY, NULL, 0, lindex_of_start_of_alert );

			// 7/18/2013 ajv : Point to the new alert's memory location so we
			// can traverse through the alert byte-by-byte without affecting the
			// passed-in pointer.
			ucp_to_new_alert = pdh.dptr;

			// Perform a byte-by-byte comparison to identify whether the alert
			// matches or not.
			for( j = 0; j < lalert_length; ++j )
			{
				ucp_to_user_pile = ( ((UNS_8*)alert_piles[ ppile->alerts_pile_index ].addr) + lindex_of_start_of_alert );

				// 7/18/2013 ajv : This byte doesn't match so the alert is
				// different.
				if( *ucp_to_new_alert != *ucp_to_user_pile )
				{
					rv = (false);
				}

				ALERTS_inc_index( ppile, &lindex_of_start_of_alert, 1 );

				++ucp_to_new_alert;
			}
		}

		// 10/31/2012 rmd : If we found a match we're done.
		if( rv == (true) )
		{
			// 7/18/2013 ajv : We found a match, so move the pdh pointer to
			// the next alert so it can be processed. If we didn't find a match,
			// the pointer should stay pointing to the beginning of the alert to
			// ensure it's added to the pile properly by the
			// ALERTS_add_alert_to_pile routine.
			pdh.dptr += lalert_length;

			break;
		}
	}

	xSemaphoreGiveRecursive( alerts_pile_recursive_MUTEX );

	return( rv );
}
/* ---------------------------------------------------------- */
/**
 * Extracts the alerts from the passed-in pointer and adds any which don't exist in this
 * controller's copy of the specified pile. Any alerts that already exist are discarded.
 *
 * @mutex_requirements Doesn't require the caller to have a specific mutex, but this
 * function takes the alerts_pile_recursive_MUTEX mutex.
 *
 * @task_execution Executed within the context of the COMM_MNGR task when the FOAL slave
 * processes an incoming irrigation data token containing alert data.
 *
 * @param pucp A pointer to the unsigned byte pointer containing the alert data, ordered
 * from OLDEST to NEWEST.
 *
 * @return BOOL_32 True if the data was able to be parsed; otherwise, false.
 *
 * @author 8/6/2012 Adrianusv
 *
 * @revisions
 *  11/25/2014 ajv : Add protection to prevent a buffer overflow if the indicated size of
 *  the alerts is larger than the alerts pile itself.
 */
extern BOOL_32 ALERTS_extract_and_store_alerts_from_comm( UNS_8 **pucp, ALERTS_PILE_STRUCT *ppile )
{
	DATA_HANDLE     ldh;

	UNS_32  lsize_of_incoming_alerts;

	UNS_32  lparsed_bytes;

	BOOL_32  rv;

	// ----------

	// 11/25/2014 ajv : Default the return value to true. Only if we encounter a problem will we
	// set this to false.
	rv = (true);

	// ----------

	// Extract the size of the incoming alerts.
	memcpy( &lsize_of_incoming_alerts, *pucp, sizeof(lsize_of_incoming_alerts) );
	*pucp += sizeof( lsize_of_incoming_alerts );

	// ----------

	// 11/25/2014 ajv : In the event the data is trashed, we need to ensure we never try to
	// write more than the alert pile can hold. Therefore, we'll check the value here.
	if( lsize_of_incoming_alerts <= ALERT_PILE_BYTES )
	{
		// Take the alerts mutex to ensure the temporary alert structure isn't
		// modified during this process
		xSemaphoreTakeRecursive( alerts_pile_recursive_MUTEX, portMAX_DELAY );

		// Initialize the temporary alert pile used by communications
		ALERTS_restart_pile( &alerts_struct_temp_from_comm, (true) );

		// Extract all of the alerts into a temporary alert pile
		memcpy( alert_piles[ alerts_struct_temp_from_comm.alerts_pile_index ].addr, *pucp, lsize_of_incoming_alerts );
		*pucp += lsize_of_incoming_alerts;

		// ----------

		lparsed_bytes = 0;

		while( lparsed_bytes < lsize_of_incoming_alerts )
		{
			ldh.dptr = (alert_piles[ alerts_struct_temp_from_comm.alerts_pile_index ].addr + lparsed_bytes);

			// Get the size of the alert
			ldh.dlen = nm_ALERTS_parse_alert_and_return_length( &alerts_struct_temp_from_comm, PARSE_FOR_LENGTH_ONLY, NULL, 0, lparsed_bytes );

			if( ALERTS_alert_already_exists_on_pile( ppile, ldh ) == (false) )
			{
				// 7/1/2015 mpd : FogBugz #3150. The "ALERTS_add_alert_to_pile()" function will add a length byte to the
				// end of the alert as it is put on the pile. This would be in addition to the length byte that existed 
				// at the end of this alert on the master's pile. Temporarily reduce the alert length by one byte so the 
				// addition does not include the original length byte. NOTE: Removing the length byte on the master side
				// created pile corruption that successfully synced down the chain (synchronized pile corruption). Not
				// adding a length byte within "ALERTS_add_alert_to_pile()" is not practical since it is used by several
				// callers; most of them needing a length.
				ldh.dlen--;

				// Add the alert to the pile since it's not a duplicate
				ALERTS_add_alert_to_pile( ppile, ldh );

				// 7/1/2015 mpd : FogBugz #3150. The length was temporarily reduce by one byte to avoid adding a 
				// secondary length byte to the alert on the pile. That length byte still exists in the token and must
				// be included in the parsed byte length. Restore the original value.
				ldh.dlen++;
			}

			// Increment the parsed bytes by the alert's length
			lparsed_bytes += ldh.dlen;
		}

		// ----------
		 
		xSemaphoreGiveRecursive( alerts_pile_recursive_MUTEX );
	}
	else
	{
		Alert_Message( "Incoming alerts size too big" );

		// 11/25/2014 ajv : Since the amount of data received was larger than we're allowed to
		// allocate, make sure we flag the content_good as FALSE to prevent further processing.
		rv = (false);
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Traverses through the specified alert pile and populates the passed-in
 * pointer with all of the alerts since and including the specified time. These
 * alerts are ordered OLDEST to NEWEST, which is opposite of how the pile is
 * stored, to accomodate easier processing on receiving side.
 *
 * @mutex_requirements Doesn't require the caller to have a specific mutex, but
 * this function takes the alerts_pile_recursive_MUTEX mutex.
 *
 * @task_execution Executed within the context of the COMM_MNGR task when
 * building an irrigation token after a new alert was added to the FOAL master. Also in the
 * COMM_MNGR when responding to a central request for alerts.
 *
 * @param pto_where_ptr A pointer to block of unsigned bytes where the alerts
 * to send will be stored. The appopriate amount of memory (the size of the
 * pile itself) has already been allocated since this file does not allow calls
 * to mem_malloc.
 *
 * @param ppile A pointer to the pile to pull the alerts to send from. So far,
 * this should only be the alerts_struct_user pile.
 *
 * @param pdt The oldest timestamp reported by the controllers on the FLOWSENSE
 * chain. The FOAL master will send all alerts since and including this
 * timestamp.
 *
 * @return UNS_32 The total size, in bytes, of alert data to be attached to the
 * token. If no alerts need to be sent, this will be 0.
 *
 * @author 8/3/2012 Adrianusv
 *
 * @revisions (none)
 */
extern UNS_32 ALERTS_for_controller_to_controller_sync_return_size_of_and_build_alerts_to_send( UNS_8 *pto_where_ptr, ALERTS_PILE_STRUCT *const ppile, const DATE_TIME pdt )
{
	ALERTS_DISPLAY_STRUCT   *ldisplay_struct;

	UNS_8           *lalert_pile;

	DATE_TIME       ldt;

	UNS_32          lalert_length, lindex_of_start_of_alert, lindex_of_aid_and_dt;

	UNS_32          i, j, rv;

	// ----------

	rv = 0;

	// ----------

	ldisplay_struct = ALERTS_get_display_structure_for_pile( ppile );

	// ----------

	// Take the alerts mutex to ensure no alerts are added during this process
	if( xSemaphoreTakeRecursive( alerts_pile_recursive_MUTEX, portMAX_DELAY ) == (true) )
	{
		i = ppile->count;

		// 10/30/2012 rmd : Using the display indicies array, for the alerts in the pile see which
		// ones satisfy the date criteria (greater than or equal to the date). But only if there is
		// at least one in the pile! And note the display indicies array has the alert starting
		// indicies from NEWEST to OLDEST. And we want to ship them from OLDEST to NEWEST.
		while( i>0 )
		{
			// This array contains the start indicies from NEWEST to OLDEST. The NEWEST alert is at
			// index 0 in the alert.
			lindex_of_start_of_alert = ldisplay_struct->all_start_indicies[ (i-1) ];

			// Get a copy of the alert so we can pull off the alert ID and date and time without moving
			// the index of the start of the alert.
			lindex_of_aid_and_dt = lindex_of_start_of_alert;

			// 10/31/2012 rmd : Just skip over the alert id. We don't specifically need it.
			ALERTS_inc_index( ppile, &lindex_of_aid_and_dt, sizeof(UNS_16) );

			// 10/31/2012 rmd : Now get what we're after - the time stamp.
			ALERTS_pull_object_off_pile( ppile, &ldt, &lindex_of_aid_and_dt, sizeof(DATE_TIME) );

			if( DT1_IsBiggerThanOrEqualTo_DT2( &ldt, &pdt ) == (true) )
			{
				// Get the size of the alert (this is the length with a 32 bit Alert ID, if we are packaging
				// as 16 bit, we need to modify this number by 2 bytes)
				lalert_length = nm_ALERTS_parse_alert_and_return_length( ppile, PARSE_FOR_LENGTH_ONLY, NULL, 0, lindex_of_start_of_alert );

				// ----------

				// 10/30/2012 rmd : Initialize this pointer to start of this particular pile.
				lalert_pile = (UNS_8*)(alert_piles[ ppile->alerts_pile_index ].addr);

				// ----------

				// 10/30/2012 rmd : Pull the remaining bytes for the alert from the pile.
				for( j=0; j<lalert_length; j++ )
				{
					*pto_where_ptr = *(lalert_pile + lindex_of_start_of_alert);

					pto_where_ptr += 1;

					ALERTS_inc_index( ppile, &lindex_of_start_of_alert, 1 );
				}

				// ----------

				rv += lalert_length;

			}  // of if the alert date-time dictates we include

			// 10/30/2012 rmd : Move to the next NEWEST alert indicie.
			i -= 1;

		}  // of each alert in the indicie array

		xSemaphoreGiveRecursive( alerts_pile_recursive_MUTEX );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Restarts the indicated alerts pile by reseting the the count, first, and next
 * pointers, as well as initializing the pre and post verification strings.
 *
 * @mutex Takes and gives the alerts_pile_recursive_MUTEX mutex. Since this
 *  	  mutex is not recursive, DO NOT TAKE before calling this routine.
 *
 * @executed
 *
 * @param ppile A pointer to the alert pile to restart. This can be
 *  			alerts_user_pile, alerts_change_pile, or alerts_tp_micro_pile.
 *
 * @author 5/29/2012 Adrianusv
 *
 * @revisions
 *    5/29/2012 Initial release
 */
extern void ALERTS_restart_pile( ALERTS_PILE_STRUCT *const ppile, const BOOL_32 pforce_the_initialization )
{
	// ----------

	xSemaphoreTakeRecursive( alerts_pile_recursive_MUTEX, portMAX_DELAY );

	// ----------

	if( ppile == &alerts_struct_user )
	{
		ppile->alerts_pile_index = ALERTS_PILE_USER;

		strlcpy( ppile->verify_string_pre, ALERTS_USER_VERIFY_STRING_PRE, BB_PRE_STRING_LENGTH );
	}
	else
	if( ppile == &alerts_struct_changes )
	{
		ppile->alerts_pile_index = ALERTS_PILE_CHANGES;

		strlcpy( ppile->verify_string_pre, ALERTS_CHANGES_VERIFY_STRING_PRE, BB_PRE_STRING_LENGTH );
	}
	else
	if( ppile == &alerts_struct_tp_micro )
	{
		ppile->alerts_pile_index = ALERTS_PILE_TP_MICRO;

		strlcpy( ppile->verify_string_pre, ALERTS_TP_MICRO_VERIFY_STRING_PRE, BB_PRE_STRING_LENGTH );
	}
	else
	if( ppile == &alerts_struct_engineering )
	{
		ppile->alerts_pile_index = ALERTS_PILE_ENGINEERING;

		strlcpy( ppile->verify_string_pre, ALERTS_ENGINEERING_VERIFY_STRING_PRE, BB_PRE_STRING_LENGTH );
	}
	else
	if( ppile == &alerts_struct_temp_from_comm )
	{
		ppile->alerts_pile_index = ALERTS_PILE_TEMP_FROM_COMM;
	}

	if( ppile->alerts_pile_index <= ALERTS_PILE_TEMP_FROM_COMM )
	{
		// 2012.06.06 ajv : Initialize the actual pile contents. 10/9/2013 rmd : I can see that. Now
		// explain why you do this?
		memset( alert_piles[ ppile->alerts_pile_index ].addr, 0x00, alert_piles[ ppile->alerts_pile_index ].pile_size );
	}
	else
	{
		Alert_Message( "Invalid alerts pile index" );
	}

	// ----------

	// Just because the next == the first doesn't mean the pile is empty and just because they
	// are both at 0 doesn't mean the pile is empty. We need a flag that says if there are any
	// alerts on the pile. I think we should actually keep count. That would be interesting
	// anyway. So when all three are 0 that means the pile is empty!
	ppile->first = 0;

	ppile->next = 0;

	ppile->count = 0;

	// ----------

	// 8/30/2013 rmd : And of course zero out the controller initiated support.
	ppile->first_to_send = 0;

	ppile->pending_first_to_send = 0;

	// 10/9/2013 rmd : Always indicate no exchange is underway at startup. If one was it will be
	// re-done.
	ppile->pending_first_to_send_in_use = (false);
	
	// ----------

	// Restart the display control structure for all except the temporary pile
	// used for alerts received through communications.
	if( ppile->alerts_pile_index < ALERTS_PILE_TEMP_FROM_COMM )
	{
		nm_ALERTS_refresh_all_start_indicies( ppile );
	}

	// ----------

	ppile->ready_to_use_bool = (true);

	// ----------

	// 2012.06.06 ajv : Make sure we only change the filter when we're working with the user
	// pile, which is the only time filters are used. Otherwise, we've seen a situation where we
	// use the user pile before it's initialized.
	if( ppile->alerts_pile_index == ALERTS_PILE_USER )
	{
		nm_ALERTS_change_filter( R_ALERTS_FILTER_SHOW_ALL, (false) );
	}

	// ----------

	xSemaphoreGiveRecursive( alerts_pile_recursive_MUTEX );

	// Now that the pile has been reset, stamp an alert that the pile has been restarted.
	if( ppile == &alerts_struct_user )
	{
		Alert_user_pile_restarted( pforce_the_initialization );
	}
	else if( ppile == &alerts_struct_changes )
	{
		Alert_change_pile_restarted( pforce_the_initialization );
	}
	else if( ppile == &alerts_struct_tp_micro )
	{
		Alert_tp_micro_pile_restarted( pforce_the_initialization );
	}
	else if( ppile == &alerts_struct_engineering )
	{
		Alert_engineering_pile_restarted( pforce_the_initialization );
	}
	else if( ppile == &alerts_struct_temp_from_comm )
	{
		//Alert_Message( "fix me!" );
	}
}

/* ---------------------------------------------------------- */
/**
 * Tests the specified alert pile to ensure it is valid by checking the pre and
 * post verification strings. This function should be called very early on,
 * BEFORE any use of the pile.
 *
 * @executed Executed within the context of the system startup task.
 *
 * @param pforce_the_initialization True if the pile should be forcefully
 *  								restarted; otherwise, false.
 * @param ppile A pointer to the alert pile to test. This can be
 *  			alerts_user_pile, alerts_change_pile, or alerts_tp_micro_pile.
 *
 * @author 5/29/2012 Adrianusv
 *
 * @revisions
 *    5/29/2012 Initial release
 */
static void ALERTS_test_the_pile( ALERTS_PILE_STRUCT *const ppile, const BOOL_32 pforce_the_initialization )
{
	if( alerts_pile_recursive_MUTEX != NULL )
	{
		// 8/7/2015 rmd : If the string isn't intact signal we've failed.

		if( pforce_the_initialization == (true) )
		{
			ALERTS_restart_pile( ppile, pforce_the_initialization );
		}
		else
		if( (ppile == &alerts_struct_user) && (strncmp( ppile->verify_string_pre, ALERTS_USER_VERIFY_STRING_PRE, sizeof(ALERTS_USER_VERIFY_STRING_PRE) ) != 0) )
		{
			ALERTS_restart_pile( ppile, pforce_the_initialization );
		}
		else
		if( (ppile == &alerts_struct_changes) && (strncmp( ppile->verify_string_pre, ALERTS_CHANGES_VERIFY_STRING_PRE, sizeof(ALERTS_CHANGES_VERIFY_STRING_PRE) ) != 0) )
		{
			ALERTS_restart_pile( ppile, pforce_the_initialization );
		}
		else
		if( (ppile == &alerts_struct_tp_micro) && (strncmp( ppile->verify_string_pre, ALERTS_TP_MICRO_VERIFY_STRING_PRE, sizeof(ALERTS_TP_MICRO_VERIFY_STRING_PRE) ) != 0) )
		{
			ALERTS_restart_pile( ppile, pforce_the_initialization );
		}
		else
		if( (ppile == &alerts_struct_engineering) && (strncmp( ppile->verify_string_pre, ALERTS_ENGINEERING_VERIFY_STRING_PRE, sizeof(ALERTS_ENGINEERING_VERIFY_STRING_PRE) ) != 0) )
		{
			ALERTS_restart_pile( ppile, pforce_the_initialization );
		}
		else
		{
			xSemaphoreTakeRecursive( alerts_pile_recursive_MUTEX, portMAX_DELAY );

			// ----------

			// 10/9/2013 rmd : Always indicate no exchange is underway at startup. If one was it will be
			// re-done.
			ppile->pending_first_to_send_in_use = (false);
			
			// ----------

			// Restart the display control structure - these must get executed
			// at a minimum during pile startup
			nm_ALERTS_refresh_all_start_indicies( ppile );
			
			// ----------

			g_ALERTS_pile_to_show = ALERTS_PILE_USER;
			
			// ----------

			// 2012.06.06 ajv : Make sure we only change the filter when we're
			// working with the user pile, which is the only time filters are
			// used. Otherwise, we've seen a situation where we use the user
			// pile before it's initialized.
			if( ppile->alerts_pile_index == ALERTS_PILE_USER )
			{
				nm_ALERTS_change_filter( R_ALERTS_FILTER_SHOW_ALL, (false) );
			}
			
			// ----------

			ppile->ready_to_use_bool = (true);
			
			// ----------

			xSemaphoreGiveRecursive( alerts_pile_recursive_MUTEX );
		}
	}
}

/* ---------------------------------------------------------- */
/**
 * Determines whether the passed-in alert ID should be added to the specified
 * pile or not.
 *
 * @executed
 *
 * @param ppile A pointer to an alerts pile.
 * @param paid The alert ID.
 *
 * @return BOOL_32 True if the alert should be added to the pile; otherwise,
 *               false.
 *
 * @author 5/31/2012 Adrianusv
 *
 * @revisions
 *    5/31/2012 Initial release
 */
static BOOL_32 ALERTS_alert_belongs_on_pile( const ALERTS_PILE_STRUCT *const ppile, const UNS_32 paid )
{
	BOOL_32 rv;

	rv = (false);

	if( (ppile == &alerts_struct_user) || (ppile == &alerts_struct_temp_from_comm) )
	{
		rv = ALERTS_alert_is_visible_to_the_user( paid, (true), (false) );
	}
	else if( ppile == &alerts_struct_changes )
	{
		rv = ALERTS_alert_is_visible_to_the_user( paid, (false), (true) );
	}
	else if( ppile == &alerts_struct_tp_micro )
	{
		switch( paid )
		{
			// 2012.06.11 ajv : Explicitly set which alerts are visible when viewing TP Micro alerts.
			case AID_TP_MICRO_ERROR:
				rv = (true);
				break;

			default:
				rv = (false);
		}
	}
	else if( ppile == &alerts_struct_engineering )
	{
		switch( paid )
		{
			// 2012.06.11 ajv : Explicitly set which alerts are *NOT* visible when viewing Engineering
			// alerts. More than likely the list of what NOT to show will match the case statement of
			// what we DO show for the user pile.
			case AID_TP_MICRO_ERROR:
				rv = (false);
				break;

			default:
				rv = (true);
		}
	}

	return( rv );
}

static BOOL_32 ALERTS_currently_being_displayed( void )
{
	BOOL_32  rv;

	rv = (false);

	switch( ScreenHistory[ screen_history_index ]._02_menu )
	{
		case SCREEN_MENU_DIAGNOSTICS:
			if( (GuiVar_MenuScreenToShow == CP_DIAGNOSTICS_MENU_ALERTS) ||
				(GuiVar_MenuScreenToShow == CP_DIAGNOSTICS_MENU_CHANGE_LINES) )
			{
				rv = (true);
			}
			break;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Updates the r_alerts display structure when a new alert is added to ensure
 * the report is updated accordingly.
 *
 * @mutex Calling function must have the alerts_pile_recursive_MUTEX mutex to
 *  	  ensure the number of alerts doesn't change during this process. This
 *  	  is called when both adding an alert which causes the pile to change.
 *  	  This could lead to an unpredictable display if the mutex is not taken.
 *
 * @executed
 *
 * @param ppile A pointer to the alerts pile the alert is being added to.
 * @param pnew_start_index The first byte of where the new alert will reside.
 *
 * @author 5/31/2012 Adrianusv
 *
 * @revisions
 *    5/31/2012 Initial release
 */
static void nm_ALERTS_update_display_after_adding_alert( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pnew_start_index )
{
	// 8/8/2013 rmd : NOTE - If you write code that adds alerts to the pile recognize this: code
	// will execute very differently timing wise if alerts are added to the pile while looking
	// at the alerts versus not looking at the alert. It takes a WHOPPING 40ms to add an alert
	// to the pile while viewing the alerts on the screen. CHALLENGE during alerts re-design is
	// to knock this either way down or eliminate all together. To eliminate it would probably
	// mean lowering the keypad and display task priorities. I include keypad there as I think
	// it has to follow that the keypad task is always at or below the display task priority
	// else you can end up pushing keys and not seeing the effect on the display till a moment
	// later!
	
	ALERTS_DISPLAY_STRUCT   *ldisplay_struct;

	DISPLAY_EVENT_STRUCT lde;

	// If we are displaying alerts from the specified pile
	if( (ALERTS_currently_being_displayed() == (true)) && (g_ALERTS_pile_to_show == ppile->alerts_pile_index) )
	{
		ldisplay_struct = ALERTS_get_display_structure_for_pile( ppile );

		// When we add one, we may have to add it to the ones to display
		if( ALERT_this_alert_is_to_be_displayed( ppile, g_ALERTS_filter_to_show, pnew_start_index ) == (true) )
		{
			nm_ALERTS_roll_and_add_new_display_start_index( ppile, ldisplay_struct, pnew_start_index );

			// If we are looking at the most recent alert, we need to update the
			// display to show the new one.
			if( ldisplay_struct->display_index_of_first_line == 0 )
			{
				// This is a special case where we are showing the first line
				// and the bar is at the top also. For this case, we want to
				// leave the bar and keep showing the new alert
				ldisplay_struct->reparse = (true);
			}
			else
			{
				// But something funny happens if we keep incrementing the index
				// of the first line when we do not have a full display of
				// alerts - we never fill out the display. It remains with
				// however many lines are on it.
				if( ldisplay_struct->display_total > ALERTS_MAX_ALERTS_TO_SHOW )
				{
					// If the top line is anything but the first line, to keep
					// the display the same, we need to push the index of the
					// start line and the bar by one then reparse and redraw
					// (would need to reparse if we removed two lines to make
					// room for this single alert)
					ldisplay_struct->display_index_of_first_line += 1;
				}

				// Need to reparse them as we are now showing one line that
				// wasn't there before.
				ldisplay_struct->reparse = (true);
			}

			if( ldisplay_struct->reparse == (true) )
			{
				// Cause a refresh of the alerts display
				lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
				lde._04_func_ptr = (void*)&FDTO_ALERTS_redraw_scrollbox;
				Display_Post_Command( &lde );
			}
		}
	}
}

/* ---------------------------------------------------------- */
/**
 * Updates the all_start_indicies array used to display the alerts report. The
 * pile's count has already been incremented to reflect the newly added alert,
 * however the array of indicies does not match the count. This routine brings
 * the two into agreement. This is called whenever a new alert is added.
 *
 * @mutex Calling function must have the alerts_pile_recursive_MUTEX mutex to
 *  	  ensure the number of alerts doesn't change during this process. This
 *  	  is called when both adding an alert which causes the pile to change.
 *  	  This could lead to an unpredictable display if the mutex is not taken.
 *
 * @executed
 *
 * @param ppile A pointer to the alerts pile the alert is being added to.
 * @param new_start_index The first byte of where the new alert will reside.
 *
 * @author 5/29/2012 Adrianusv
 *
 * @revisions
 *    5/29/2012 Initial release
 */
static void nm_ALERTS_update_start_indicies_array( ALERTS_PILE_STRUCT *ppile, const UNS_32 new_start_index )
{
	ALERTS_DISPLAY_STRUCT   *ldisplay_struct;

	UNS_32  from_index, to_index;

	if( ppile->count == 0 )
	{
		// Nothing to do... Should never be called for this case
	}
	else if( ppile->count == 1 )
	{
		// Also nothing to do. Remember, the count includes the yet to be added
		// alert so if it says 1 there are none in the start indicies pile.
	}
	else
	{
		ldisplay_struct = ALERTS_get_display_structure_for_pile( ppile );

		from_index = (ppile->count - 2);
		to_index = (ppile->count - 1);

		do
		{
			ldisplay_struct->all_start_indicies[ to_index ] = ldisplay_struct->all_start_indicies[ from_index ];

			// Leave if we are done
			if( from_index == 0 )
			{
				break;
			}

			// Move down through the pile
			--to_index;
			--from_index;

		} while( (true) );

		// Add the new one!
		ldisplay_struct->all_start_indicies[ 0 ] = (UNS_16)new_start_index;
	}
}

/* ---------------------------------------------------------- */
/**
 * Increments the next index of the specified alerts pile. Additionally, if the
 * alerts report is being displayed for that alerts pile, the display is
 * refreshsed.
 *
 * @mutex Calling function must have the alerts_pile_recursive_MUTEX mutex to
 *  	  ensure the number of alerts doesn't change during this process. This
 *  	  is called when both adding an alert which causes the pile to change.
 *  	  This could lead to an unpredictable display if the mutex is not taken.
 *
 * @executed
 *
 * @param ppile A pointer to the alerts pile being incremented.
 *
 * @author 5/29/2012 Adrianusv
 *
 * @revisions
 *    5/29/2012 Initial release
 */
static void nm_ALERTS_while_adding_alert_increment_index_to_next_available( ALERTS_PILE_STRUCT *const ppile )
{
	// ----------

	BOOL_32		need_to_move_first_to_send, need_to_move_NEW_first_to_send;
	
	// ----------

	// 8/13/2013 rmd : HOW to use this function - caller is to WRITE to the next byte location
	// first, THEN call this function. And here in this function, FIRST increment next byte
	// location, and THEN execute the if-statement comparing the next to the first. And if they
	// match wipe that alert off the pile. It means the pile is full and we've bumped up into
	// the oldest alert. This means we're sacrificing an alert when the pile is full, we will
	// otherwise overwrite the alert ID of the first item in the list next time we increment the
	// index. Once this occurs, we can no longer parse the oldest alert to remove it,
	// invalidating the pile beyond that point.
	ALERTS_inc_index( ppile, &ppile->next, 1 );

	// ----------

	// 10/26/2012 rmd : Did we just move the index to the first byte of the OLDEST alert?
	// Remember we are about to write into where next points to. So squash that alert to make
	// room for a new one. But only if there are alerts on the pile!
	if( (ppile->next == ppile->first) && (ppile->count > 0) )
	{
		// ----------
	
		// 8/30/2013 rmd : If the next alert to send is also the same one that is pointed to by the
		// first pointer, then whatever happens to first pointer has to happen to the next to send
		// pointer.
		if( ppile->first_to_send == ppile->first )
		{
			need_to_move_first_to_send = (true);
		}
		else
		{
			need_to_move_first_to_send = (false);
		}
		
		// 8/30/2013 rmd : ADDITIONALLY (and this is very complicated) if during the wait for a
		// response from the comm_server, we add so many alerts that we wrap around and bang into
		// the first_byte_to_send_copy, that ALSO have to move. I believe that if this happens all
		// three (first, first_byte_to_send, and the
		// new_first_byte_to_send_while_waiting_for_a_response) would be equal. THINK OF IT THIS
		// WAY, while waiting for a response we switch from testing the first_byte_to_send to the
		// new_first_byte_to_send. But its okay not to introduce the complication of which to test,
		// and always test both cases. BECAUSE the new_first is irrelavant when NOT WAITING for a
		// response.
		if( ppile->pending_first_to_send == ppile->first )
		{
			need_to_move_NEW_first_to_send = (true);
		}
		else
		{
			need_to_move_NEW_first_to_send = (false);
		}
		
		// ----------

		// 10/26/2012 rmd : Move the first index the size of the alert first now points to.
		ALERTS_inc_index( ppile, &ppile->first, nm_ALERTS_parse_alert_and_return_length( ppile, PARSE_FOR_LENGTH_ONLY, NULL, 0, ppile->first ) );

		// There is now one less alert on the pile, so decrement the total alert
		// count. Decreasing this count effectively REMOVES it from the array of
		// all indicies. THIS REMOVES THE OLDEST INDEX.
		ppile->count -= 1;

		// ----------
		
		// 8/30/2013 rmd : Okay update first to send if needed.
		if( need_to_move_first_to_send )
		{
			ppile->first_to_send = ppile->first;
		}
		
		// 8/30/2013 rmd : And check if the pending new FIRST needs updating.
		if( need_to_move_NEW_first_to_send )
		{
			ppile->pending_first_to_send = ppile->first;
		}
		
		// ----------

		// If we're displaying alerts from this pile, we need may need to update what's being
		// displayed.
		if( (ALERTS_currently_being_displayed() == (true)) && (g_ALERTS_pile_to_show == ppile->alerts_pile_index) )
		{
			ALERTS_DISPLAY_STRUCT   *ldisplay_struct;

			ldisplay_struct = ALERTS_get_display_structure_for_pile( ppile );

			// First update the display start indicies to reflect that we just
			// removed the last one IF it is the last one

			// 11/27/2012 rmd : HOW do we KNOW display_total is > than 1 here??????????????????? TODO
			if( ldisplay_struct->display_start_indicies[ (ldisplay_struct->display_total - 1) ] == ppile->next )
			{
				ldisplay_struct->display_total -= 1;
			}

			// Now check to see if the one we removed was being actually
			// displayed. If so move up to reflect that this alert is actually
			// gone.
			if( ldisplay_struct->display_index_of_first_line > (ldisplay_struct->display_total - ALERTS_MAX_ALERTS_TO_SHOW) )
			{
				// In order to keep a full display, move the starting index up
				// by one.
				ldisplay_struct->display_index_of_first_line -= 1;

				// Need to reparse them as we are now showing one line that
				// wasn't there before
				ldisplay_struct->reparse = (true);
			}
		}
	}
}

/* ---------------------------------------------------------- */
/**
 * Adds an alert to the specified alerts pile.
 *
 * @mutex This routine takes and releases the alerts_pile_recursive_MUTEX.
 *
 * @executed
 *
 * @param ppile A pointer to the alerts pile to add the alert to.
 * @param pdh The data handle containing the alert to add to the pile.
 *
 * @author 5/31/2012 Adrianusv
 *
 * @revisions
 *    5/31/2012 Initial release
 */
static BOOL_32 ALERTS_add_alert_to_pile( ALERTS_PILE_STRUCT *const ppile, DATA_HANDLE pdh )
{
	// 10/11/2013 rmd : This function returns (false) if the pile is not ready for use. Meaning
	// it has not been tested/initialized yet.

	// ----------

	UNS_8   *pile_start_ptr;

	UNS_32  new_start_index;

	UNS_32  i;
	
	BOOL_32	rv;
	
	rv = (false);

	// ----------

	// only if the alert pile is okay to use should we proceed
	if( ppile->ready_to_use_bool == (true) )
	{
		// 10/11/2013 rmd : Tell the caller we added an alert. One way or another an alert will be
		// added. In the face of an anomaly not the intended alert ... but an alert is added.
		rv = (true);
		
		// ----------
		
		// We are going to protect the alerts pile with a MUTEX. Because of this
		// you CANNOT make an alert from within an ISR. That's because you
		// cannot TAKE OR GIVE a mutex from within an ISR.
		//
		// The MUTEX protects the alerts pile from a task switch that in turn
		// attempts to make another alert. Surely a situation that would crash
		// the pile.
		//
		// ADDITIONALLY because the display of alerts is so intertwined with the
		// adding of alerts (some of the same function calls, when adding the
		// indicies of what you are displaying can become invalid, etc.) the two
		// activities [adding and displaying] must also be mutally exclusive. So
		// the display function (must only be one) will also request this same
		// alerts_pile_recursive_MUTEX.

		// MUST get the semaphore that assures this process of adding the alert
		// and changing the pile won't cause the pile to corrupt (because we
		// preempted an adding of an alert already in process) or that we
		// confuse the display of alerts functions. Those functions would not
		// tolerate the indicies changing mid stream. The alerts display
		// functions are only invoked via the display task and are also
		// protected by the SAME alerts_pile_recursive_MUTEX.
		xSemaphoreTakeRecursive( alerts_pile_recursive_MUTEX, portMAX_DELAY );

		if( ppile->next >= alert_piles[ ppile->alerts_pile_index ].pile_size )
		{
			// Reset the pile and generate an alert that the next is out of
			// range
			ALERTS_restart_pile( ppile, INIT_BY_FORCE );

			// EVEN though generating an alert here seems impossible,
			// (recursive) it is okay because MUTEX is recursive and we've just
			// cleaned up the pile so to call AP_AddAlert will (should) work.
			Alert_Message( "Alerts NEXT ptr out of range" );
		}
		else if( ppile->first >= alert_piles[ ppile->alerts_pile_index ].pile_size )
		{
			// Reset the pile and generate an alert that the first is out of
			// range
			ALERTS_restart_pile( ppile, INIT_BY_FORCE );

			// EVEN though generating an alert here seems impossible,
			// (recursive) it is okay because MUTEX is recursive and we've just
			// cleaned up the pile so to call AP_AddAlert will (should) work.
			Alert_Message( "Alerts FIRST ptr out of range" );
		}
		// 10/5/2012 rmd : The piles are somewhat arbritarily sized and not guaranteed to be an
		// integer number of MIN ALERT SIZE. So first of all the division tells us the minimum
		// number of whole alerts of the minimum size that will fit. And there probably is a
		// remainder (4096 / 11 = 372.2 alerts). In any case, remainder or not, we should allow the
		// full integer division result ( 372 ) to be in the pile. So test that the count is GREATER
		// THAN the max calculated size.
		else if( ppile->count >= (alert_piles[ ppile->alerts_pile_index ].pile_size / ALERTS_MIN_STORED_BYTE_COUNT) )
		{
			// Reset the pile and generate an alert that the count is out of
			// range
			ALERTS_restart_pile( ppile, INIT_BY_FORCE );

			// EVEN though generating an alert here seems impossible,
			// (recursive) it is okay because MUTEX is recursive and we've just
			// cleaned up the pile so to call AP_AddAlert will (should) work.
			Alert_Message_va( "Pile: %u, Alerts COUNT ptr out of range", ppile->alerts_pile_index );
		}
		else
		{
			// It's safe to add the alert - NOT NECESSARILY see note at start of
			// this function.

			// Get the start index of the alert we are going to add. This would be where the first byte
			// of this new alert is going to reside. We need this to update the start indicies array.
			new_start_index = ppile->next;

			// ----------

			// 10/25/2012 rmd : Byte by byte add the alert. 
			pile_start_ptr = alert_piles[ ppile->alerts_pile_index ].addr;  // acquire the start if the pile

			for( i=0; i<pdh.dlen; i++ )
			{
				*(pile_start_ptr + (ppile->next)) = *pdh.dptr++;

				nm_ALERTS_while_adding_alert_increment_index_to_next_available( ppile );
			}

			// ----------

			// Now add the LENGTH BYTE to the end of the alert. This byte is an aid to help displaying
			// the alerts. It is quite hard when scrolling in the older alerts direction to know where
			// the next alert starts. This LENGTH BYTE is the byte count of the complete alert INCLUDING
			// THE LENGTH BYTE! So we need to bump it by one.
			*(pile_start_ptr + (ppile->next)) = (UNS_8)(pdh.dlen + 1);

			nm_ALERTS_while_adding_alert_increment_index_to_next_available( ppile );

			// ----------

			// We added an alert, so increment the count on the pile.
			ppile->count += 1;

			// ----------

			// Update the display control structure for all except the temporary pile used for alerts
			// received through communications.
			if( ppile->alerts_pile_index < ALERTS_PILE_TEMP_FROM_COMM )
			{
				// Always keep these up to date
				nm_ALERTS_update_start_indicies_array( ppile, new_start_index );

				nm_ALERTS_update_display_after_adding_alert( ppile, new_start_index );
			}

			// If we added this alert to the user alerts, we need to initialize the latest_timestamps
			// structure to cause the FOAL master to request the latest timestamps from each of its
			// slaves.
			if( ppile == &alerts_struct_user )
			{
				for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
				{
					ALERTS_latest_timestamps_by_controller[ i ].D = DATE_MINIMUM;
					ALERTS_latest_timestamps_by_controller[ i ].T = TIME_DEFAULT;
				}

				// ----------
				
				// 8/13/2013 rmd : And since this is for the USER pile set our flag that triggers a sync of
				// this alert to other potential controllers on the chain.
				ALERTS_need_to_sync = (true);
			}
		}

		xSemaphoreGiveRecursive( alerts_pile_recursive_MUTEX );
	}
	
	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Increments pdh to make room for the the passed-in string to the alerts pile.
 * This routine keeps pdh up-to-date for the functino atht at will actually put
 * the alert on the pile. It only needs to add to the .dlen portion of the
 * handle to reflect the newly added characters.
 *
 * @executed
 *
 * @param pucp A pointer to the location to start putting the string. THIS IS
 *  		   NOT THE ALERTS PILE. This is important because we can't increment
 *             the pointer for that.
 * @param frombuffer The string to add to the alerts pile.
 * @param pdh The data handle for the alert. pdh MUST be initialized before
 *            calling this function.
 *
 * @return unsigned char* A pointer to the next available place to add the
 *                        alert.
 *
 * @author 5/29/2012 Adrianusv
 *
 * @revisions
 *    5/29/2012 Initial release
 */
static unsigned char *ALERTS_load_string( unsigned char *pucp, char *frombuffer, DATA_HANDLE *pdh )
{
	UNS_32  len;

	len = 0;

	while( *frombuffer != 0x00 )
	{
		*pucp++ = *frombuffer++;

		++len;

		// 7/28/2016 rmd : The maximum individual string (WHICH INCLUDES THE NULL) stored within an
		// alert is 128 characters. So limit to 127 chars plus the NULL char.
		if( len == (ALERTS_MAX_STORAGE_OF_TEXT_CHARS - 1) )
		{
			break;
		}
	}

	*pucp++ = 0x00;	 // put in the null AND IT POINTS TO THE NEXT PLACE TO PUT DATA!!

	++len;	// the null counts as one char

	pdh->dlen += len;

	return( pucp );
}

/* ---------------------------------------------------------- */
/**
 * Restarts all of the alert piles. This is ONLY to be used for debugging. This
 * routine should be removed once debugging is complete.
 *
 * @executed Executed within the context of the key processing task when the
 *           user presses the "Erase Alerts" button.
 *
 * @author 5/29/2012 Adrianusv
 *
 * @revisions
 *    5/29/2012 Initial release
 */
extern void ALERTS_restart_all_piles( void )
{
	ALERTS_restart_pile( &alerts_struct_engineering, INIT_BY_FORCE );

	ALERTS_restart_pile( &alerts_struct_user, INIT_BY_FORCE );

	ALERTS_restart_pile( &alerts_struct_changes, INIT_BY_FORCE );

	ALERTS_restart_pile( &alerts_struct_tp_micro,INIT_BY_FORCE );
}

/* ---------------------------------------------------------- */
/**
 * Tests the all of the alert piles to ensure they are valid by checking the pre
 * and post verification strings. This function should be called very early on,
 * BEFORE any use of the pile.
 *
 * @executed Executed within the context of the system startup task.
 *
 * @param pforce_the_initialization True if thes pile should be forcefully
 *                                  restarted; otherwise, false.
 *
 * @author 5/29/2012 Adrianusv
 *
 * @revisions
 *    5/29/2012 Initial release
 */
extern void ALERTS_test_all_piles( const BOOL_32 pforce_the_initialization )
{
	// 2012.05.29 ajv : Since we now have multiple alert piles, it's
	// important that we test the piles in the correct order to ensure the
	// appropriate pile is ready to be written to if an error occurs.
	// Therefore, we're going to test, and potentially reinitialize, the
	// engineering pile first since any alert messages are written there by
	// default.
	ALERTS_test_the_pile( &alerts_struct_engineering, pforce_the_initialization );

	// 2012.05.29 ajv : Now that the engineering pile has been verified as
	// intact, we can test the other piles. This routine checks that the pile is
	// initialized. If it's not, a pile restart is performed.
	ALERTS_test_the_pile( &alerts_struct_user, pforce_the_initialization );

	ALERTS_test_the_pile( &alerts_struct_changes, pforce_the_initialization );

	ALERTS_test_the_pile( &alerts_struct_tp_micro, pforce_the_initialization );
}

/* ---------------------------------------------------------- */
/**
 * Increments the index, taking into account the end of the buffer since the
 * alert piles are circular buffers.
 *
 * @executed
 *
 * @param ppile A pointer to the alert pile to increment.
 * @param u32_ptr A pointer to the index of the appropriate alerts pile array.
 * @param howmany The number to increment by.
 *
 * @author 5/29/2012 Adrianusv
 *
 * @revisions
 *    5/29/2012 Initial release
 */
extern void ALERTS_inc_index( ALERTS_PILE_STRUCT const *ppile, UNS_32 *pindex_ptr, const UNS_32 phowmany )
{
	UNS_32  i;

	// 10/25/2012 rmd : pindex_ptr points to the index of a byte within the pile array.
	// Increment the index taking into account the end of the buffer. The AlertPile is a ring
	// buffer.
	for( i=0; i<phowmany; i++ )
	{
		*pindex_ptr += 1;

		// It should never go greater than the max, but cover that just in case.
		if( *pindex_ptr >= alert_piles[ ppile->alerts_pile_index ].pile_size )
		{
			// Index is 0-based.
			*pindex_ptr = 0;
		}

	}
}

/* ---------------------------------------------------------- */
/**
 * Adds the specified alert to the appropriate alert pile(s).
 *
 * @executed
 *
 * @param paid The Alert ID of the alert to add to the pile.
 * @param pdh The data handle describing the alert.
 *
 * @author 5/29/2012 Adrianusv
 *
 * @revisions
 *    5/29/2012 Initial release
 */
extern void ALERTS_add_alert( const UNS_32 paid, DATA_HANDLE pdh )
{
	if( ALERTS_alert_belongs_on_pile( &alerts_struct_user, paid ) == (true) )
	{
		ALERTS_add_alert_to_pile( &alerts_struct_user, pdh );
		
		// 8/25/2014 rmd : Alert added or not start the CI timer. CI functionality has protection
		// against uninitialized pile. We are using the addition to the user pile as the guide to
		// decide when to send the alerts. So we manage the number of connections made during the
		// day. Do remember when the timer fires we send the ENGINEERING pile.
		CONTROLLER_INITIATED_alerts_timer_upkeep( alerts_struct_engineering.ci_duration_ms, (false) );
	}

	// ----------
	
	if( ALERTS_alert_belongs_on_pile( &alerts_struct_changes, paid ) == (true) )
	{
		ALERTS_add_alert_to_pile( &alerts_struct_changes, pdh );

		CONTROLLER_INITIATED_alerts_timer_upkeep( alerts_struct_engineering.ci_duration_ms, (false) );
	}

	// ----------
	
	if( ALERTS_alert_belongs_on_pile( &alerts_struct_tp_micro, paid ) == (true) )
	{
		ALERTS_add_alert_to_pile( &alerts_struct_tp_micro, pdh );

		// 9/2/2014 rmd : Start the timer here to when tp micro alerts are added. We've temporarily
		// added tpmicro alerts to the engineering pile. To avoid pile roll out start the timer for
		// each tpmicro alert.
		CONTROLLER_INITIATED_alerts_timer_upkeep( alerts_struct_engineering.ci_duration_ms, (false) );
	}

	// ----------
	
	// 8/25/2014 rmd : FOR DEVELOPMENT!!!!!!! Force the TPMICRO alerts to appear in the
	// ENGINEERING PILE. And start the CI alerts timer when we add a TPMICRO  alert (above). So
	// we can see what is happening on the two-wire cable.
	if( ALERTS_alert_belongs_on_pile( &alerts_struct_engineering, paid ) || ALERTS_alert_belongs_on_pile( &alerts_struct_tp_micro, paid ) )
	{
		// 9/2/2014 rmd : Decide not to start the timer here cause it is self perputing in that the
		// comm COMMAND alert when sending the alerts would then again restart the timer to send,
		// making another comm command alert ... etc.

		ALERTS_add_alert_to_pile( &alerts_struct_engineering, pdh );
	}

	// ----------
	
	if( ALERTS_alert_belongs_on_pile( &alerts_struct_temp_from_comm, paid) == (true) )
	{
		ALERTS_add_alert_to_pile( &alerts_struct_temp_from_comm, pdh );
	}
}

/* ---------------------------------------------------------- */
/**
 * Initializes and increments pdhp to make room for the specified alert ID in
 * the alerts pile. Also includes room for the current date and time. This
 * function also brings pdhp up-to-date to reflect this common up-front block.
 *
 * Note, this routine is different from the load_string and load_object routines
 * because it initializes the return value and pdhp. The other routines simply
 * increment them.
 *
 * @executed
 *
 * @param pucp A pointer to where to load the alert ID followed by the time
 *             (where the alert starts)
 * @param paid THe alert ID to add to the pile.
 * @param pdhp The data handle to the alert. It acn be totally uninitialized
 *  		   when given to this functino, which is how we expect it.
 * @param pdt The date and time to add to the pile.
 *
 * @return void* Returns a pointer to the next place to put the data (for the
 *               caller if he needs to do that)
 *
 * @author 5/29/2012 Adrianusv
 *
 * @revisions
 *    5/29/2012 Initial release
 */
/* ---------------------------------------------------------- */
static void *ALERTS_load_common_with_dt( UNS_8 *pucp, const UNS_32 paid, DATA_HANDLE *pdh_ptr, const DATE_TIME *pdt_ptr )
{
	UNS_16  aid_short;
	
	UNS_32	pseudo_field;
	
	// 9/9/2014 rmd : Use a local variable so we don't mess with the callers date_time
	// structure. He may not expect such.
	DATE_TIME	ldt;
	
	// ----------
	
	// 10/30/2012 rmd : All alerts created begin with a call to this function. And upon entry
	// the pdhp is uninitialized. Out job is to init it here.
	pdh_ptr->dptr = pucp;

	// ----------

	// 10/31/2012 rmd : NOW HERE is where we convert the AID to an UNS_16. And as an UNS_16 it
	// is stored in the pile.
	aid_short = (UNS_16)paid;

	memcpy( pucp, &aid_short, sizeof(UNS_16) );

	pucp += sizeof(UNS_16);

	pdh_ptr->dlen = sizeof(UNS_16);	 // not += cause we are initializing it here

	// ----------

	// 9/9/2014 rmd : Here is where we add the pseudo milliseconds to the date and time seconds
	// field. The so-called milliseconds is really just a field that counts up to 999 and starts
	// over at 0 with each new second. The field is OR'd into the 32-bit seconds field in the
	// high order bit positions. And is treated as actual milliseconds in the commserver to
	// ensure alerts with the same date & time stay in the correct order when displayed.
	//
	// 6/3/2015 rmd : NOTE - this logic supports keeping the correct order for up to 1000 alerts
	// with the same MID made within the same second.
	if( (pseudo_ms < PSEUDO_MS_MAX) && (pdt_ptr->T == pseudo_ms_last_second_an_alert_was_made) )
	{
		pseudo_ms += 1;
	}
	else
	{
		pseudo_ms = 0;
	}
	
	// 6/3/2015 rmd : Always update the pseudo second stamp.
	pseudo_ms_last_second_an_alert_was_made = pdt_ptr->T;
	
	// ----------
	
	// 9/9/2014 rmd : Now OR to time field.
	pseudo_field = (pseudo_ms << PSEUDO_MS_SHIFT_10_BIT_VALUE_TO_MSB);
	
	ldt = *pdt_ptr;
	
	ldt.T |= pseudo_field;
	
	// ----------
	
	// 9/9/2014 rmd : Now copy the pseudo-modified date and time to the location.
	memcpy( pucp, &ldt, sizeof( DATE_TIME ) );

	pucp += sizeof( DATE_TIME );  // Now it points to the next place to put data

	pdh_ptr->dlen += sizeof( DATE_TIME );	// add to it

	// ----------

	// 10/31/2012 rmd : Return where the pointer to put more data now points to.
	return( pucp );
}

extern void *ALERTS_load_common( UNS_8 *pucp, const UNS_32 paid, DATA_HANDLE *pdhp )
{
	DATE_TIME   ldt;

	// ----------

	// 10/9/2013 rmd : We need to set the value for ALL piles because this alert could be added
	// to multiple piles. So far only the engineering pile actually uses the ci_duration. But
	// set all piles up for future potential use. A particular alert construction function needs
	// to specifically change the value if needed. For example the MLB alert function may set
	// this to a shorter duration (within that function ... NOT here).
	alerts_struct_user.ci_duration_ms = CONTROLLER_INITIATED_DEFAULT_SEND_ALERTS_TIMEOUT_MS;

	alerts_struct_changes.ci_duration_ms = CONTROLLER_INITIATED_DEFAULT_SEND_ALERTS_TIMEOUT_MS;

	alerts_struct_tp_micro.ci_duration_ms = CONTROLLER_INITIATED_DEFAULT_SEND_ALERTS_TIMEOUT_MS;

	alerts_struct_engineering.ci_duration_ms = CONTROLLER_INITIATED_DEFAULT_SEND_ALERTS_TIMEOUT_MS;
	
	// ----------

	EPSON_obtain_latest_time_and_date( &ldt );

	// 10/31/2012 rmd : The function that loads the data returns the pointer to put more data.
	// And that is what we are supposed to return from this function.
	return( ALERTS_load_common_with_dt( pucp, paid, pdhp, &ldt ) );
}

/* ---------------------------------------------------------- */
/**
 * Increments pdhp to make room for the specified alert ID in the alerts pile.
 * Also includes room for the current date and time. This function also brings
 * pdhp up-to-date for the function that actually puts the alert on the pile.
 *
 * @executed
 *
 * @param pucp A pointer to the location to start putting the characters.
 * @param from_ptr A pointer to the object that will be added to the alert.
 * @param from_ptr_size The size of the pointer
 * @param pdh
 *
 * @return unsigned char* A pointer to the next available place to add the
 *                        alert.
 *
 * @author 6/4/2012 Adrianusv
 *
 * @revisions
 *    6/4/2012 Initial release
 */
extern unsigned char *ALERTS_load_object( unsigned char *pucp, const void *from_ptr, const UNS_32 from_ptr_size, DATA_HANDLE *const pdh )
{
	// Put the data in place
	memcpy( pucp, from_ptr, from_ptr_size );

	// Get the return value pointing to where to add the next data, if any
	pucp += from_ptr_size;

	// Update the data handle
	pdh->dlen += from_ptr_size;

	return( pucp );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Adds an alert ID to the pile. This "simple alert" is parsed into a
 * user-friendly string when viewing the Alerts report.
 *
 * @executed
 *
 * @param paid The alert ID to add to the alerts pile.
 *
 * @author 5/29/2012 Adrianusv
 *
 * @revisions
 *    5/29/2012 Initial release
 */
static void ALERTS_build_simple_alert( const UNS_32 paid )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array


	ALERTS_load_common( (UNS_8*)&lalert, paid, &ldh );

	ALERTS_add_alert( paid, ldh );
}

#if ALERT_INCLUDE_FILENAME

/* ---------------------------------------------------------- */
/**
 * Adds an alert ID to the pile with the filename and line number that the
 * alert was generated on. This "simple alert" is parsed into a
 * user-friendly string when viewing the Alerts report.
 *
 * @executed
 *
 * @param paid The alert ID to add to the alerts pile.
 *
 * @author 6/11/2012 Adrianusv
 *
 * @revisions
 *    6/11/2012 Initial release
 */
static void ALERTS_build_simple_alert_with_filename( const UNS_32 paid, char *pfilename, const UNS_32 pline_num )
{
	DATA_HANDLE ldh;

	// Build the alert on the stack in this array. Must be big enough to
	// hold the 128 characters maximum string length plus the rest of the
	// alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	ucp = ALERTS_load_common( (UNS_8*)&lalert, paid, &ldh );

	ucp = ALERTS_load_string( ucp, pfilename, &ldh );
	ucp = ALERTS_load_object( ucp, &pline_num, sizeof(pline_num), &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( paid, ldh );
}

#endif

/* ---------------------------------------------------------- */
extern void Alert_power_fail_idx( const DATE_TIME *pdt_ptr, const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common_with_dt( (unsigned char*)&lalert, AID_POWER_FAIL, &ldh, pdt_ptr );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );  // only take the first byte

	ALERTS_add_alert( AID_POWER_FAIL, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_power_fail_brownout_idx( const DATE_TIME *pdt_ptr, const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common_with_dt( (unsigned char*)&lalert, AID_POWER_FAIL_BROWNOUT, &ldh, pdt_ptr );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );  // only take the first byte

	ALERTS_add_alert( AID_POWER_FAIL_BROWNOUT, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_program_restart_idx( const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_PROGRAM_RESTART, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );  // only take the first byte

	ALERTS_add_alert( AID_PROGRAM_RESTART, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_user_reset_idx( const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_USER_RESET, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );  // only take the first byte

	ALERTS_add_alert( AID_USER_RESET, ldh );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#if ALERT_INCLUDE_FILENAME

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void Alert_group_not_found_with_filename( char *pfilename, const UNS_32 pline_num )
{
	ALERTS_build_simple_alert_with_filename( AID_GROUP_NOT_FOUND_WITH_FILENAME, CS_FILE_NAME( pfilename ), pline_num );
}

/* ---------------------------------------------------------- */
extern void Alert_station_not_found_with_filename( char *pfilename, const UNS_32 pline_num )
{
	ALERTS_build_simple_alert_with_filename( AID_STATION_NOT_FOUND_WITH_FILENAME, CS_FILE_NAME( pfilename ), pline_num );
}

/* ---------------------------------------------------------- */
extern void Alert_station_not_in_group_with_filename( char *pfilename, const UNS_32 pline_num )
{
	ALERTS_build_simple_alert_with_filename( AID_STATION_NOT_IN_GROUP_WITH_FILENAME, CS_FILE_NAME( pfilename ), pline_num );
}

/* ---------------------------------------------------------- */
extern void Alert_func_call_with_null_ptr_with_filename( char *pfilename, const UNS_32 pline_num )
{
	ALERTS_build_simple_alert_with_filename( AID_FUNC_CALL_WITH_NULL_PTR_WITH_FILENAME, CS_FILE_NAME( pfilename ), pline_num );
}

/* ---------------------------------------------------------- */
extern void Alert_index_out_of_range_with_filename( char *pfilename, const UNS_32 pline_num )
{
	ALERTS_build_simple_alert_with_filename( AID_INDEX_OUT_OF_RANGE_WITH_FILENAME, CS_FILE_NAME( pfilename ), pline_num );
}

/* ---------------------------------------------------------- */
extern void Alert_item_not_on_list_with_filename( char *pitem_name, char *plist_name, char *pfilename, const UNS_32 pline_num )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to
	// hold the 128 characters maximum string length plus the rest of the
	// alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ITEM_NOT_ON_LIST_WITH_FILENAME, &ldh );

	ucp = ALERTS_load_string( ucp, pitem_name, &ldh );
	ucp = ALERTS_load_string( ucp, plist_name, &ldh );

	ucp = ALERTS_load_string( ucp, CS_FILE_NAME( pfilename ), &ldh );
	ucp = ALERTS_load_object( ucp, &pline_num, sizeof(pline_num), &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_ITEM_NOT_ON_LIST_WITH_FILENAME, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_bit_set_with_no_data_with_filename( char *pfilename, const UNS_32 pline_num )
{
	ALERTS_build_simple_alert_with_filename( AID_BIT_SET_WITH_NO_DATA_WITH_FILENAME, CS_FILE_NAME( pfilename ), pline_num );
}

/* ---------------------------------------------------------- */
extern void Alert_range_check_failed_bool_with_filename( const char *const pvariable_name, const char *const pgroup_name, const BOOL_32 pvalue_bool, const BOOL_32 pdefault_bool, const char *const pfilename, const UNS_32 pline_num )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to
	// hold the 128 characters maximum string length plus the rest of the
	// alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_BOOL_WITH_GROUP_WITH_FILENAME, &ldh );
	}
	else
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_BOOL_WITH_FILENAME, &ldh );
	}

	ucp = ALERTS_load_string( ucp, (char *)pvariable_name, &ldh );

	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_string( ucp, (char *)pgroup_name, &ldh );
	}

	ucp = ALERTS_load_object( ucp, &pvalue_bool, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pdefault_bool, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_string( ucp, (char *)pfilename, &ldh );
	ucp = ALERTS_load_object( ucp, &pline_num, sizeof(pline_num), &ldh );

	// now put the alert on the pile
	if( pgroup_name != NULL )
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_BOOL_WITH_GROUP_WITH_FILENAME, ldh );
	}
	else
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_BOOL_WITH_FILENAME, ldh );
	}
}

/* ---------------------------------------------------------- */
extern void Alert_range_check_failed_int32_with_filename( const char *const pvariable_name, const char *const pgroup_name, const INT_32 pvalue_int32, const INT_32 pdefault_int32, const char *const pfilename, const UNS_32 pline_num )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to
	// hold the 128 characters maximum string length plus the rest of the
	// alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_INT32_WITH_GROUP_WITH_FILENAME, &ldh );
	}
	else
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_INT32_WITH_FILENAME, &ldh );
	}

	ucp = ALERTS_load_string( ucp, (char *)pvariable_name, &ldh );

	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_string( ucp, (char *)pgroup_name, &ldh );
	}

	ucp = ALERTS_load_object( ucp, &pvalue_int32, sizeof(pvalue_int32), &ldh );
	ucp = ALERTS_load_object( ucp, &pdefault_int32, sizeof(pdefault_int32), &ldh );
	ucp = ALERTS_load_string( ucp, (char *)pfilename, &ldh );
	ucp = ALERTS_load_object( ucp, &pline_num, sizeof(pline_num), &ldh );

	// now put the alert on the pile
	if( pgroup_name != NULL )
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_INT32_WITH_GROUP_WITH_FILENAME, ldh );
	}
	else
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_INT32_WITH_FILENAME, ldh );
	}
}

/* ---------------------------------------------------------- */
extern void Alert_range_check_failed_float_with_filename( const char *const pvariable_name, const char *const pgroup_name, const float pvalue_float, const float pdefault_float, const char *const pfilename, const UNS_32 pline_num )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to
	// hold the 128 characters maximum string length plus the rest of the
	// alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_FLOAT_WITH_GROUP_WITH_FILENAME, &ldh );
	}
	else
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_FLOAT_WITH_FILENAME, &ldh );
	}

	ucp = ALERTS_load_string( ucp, (char *)pvariable_name, &ldh );

	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_string( ucp, (char *)pgroup_name, &ldh );
	}

	ucp = ALERTS_load_object( ucp, &pvalue_float, sizeof(pvalue_float), &ldh );
	ucp = ALERTS_load_object( ucp, &pdefault_float, sizeof(pdefault_float), &ldh );
	ucp = ALERTS_load_string( ucp, (char *)pfilename, &ldh );
	ucp = ALERTS_load_object( ucp, &pline_num, sizeof(pline_num), &ldh );

	// now put the alert on the pile
	if( pgroup_name != NULL )
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_FLOAT_WITH_GROUP_WITH_FILENAME, ldh );
	}
	else
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_FLOAT_WITH_FILENAME, ldh );
	}
}

/* ---------------------------------------------------------- */
extern void Alert_range_check_failed_date_with_filename( const char *const pvariable_name, const char *const pgroup_name, const UNS_32 pdate, const char *const pfilename, const UNS_32 pline_num )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to
	// hold the 128 characters maximum string length plus the rest of the
	// alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_DATE_WITH_GROUP_WITH_FILENAME, &ldh );
	}
	else
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_DATE_WITH_FILENAME, &ldh );
	}

	ucp = ALERTS_load_string( ucp, (char *)pvariable_name, &ldh );

	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_string( ucp, (char *)pgroup_name, &ldh );
	}

	ucp = ALERTS_load_object( ucp, &pdate, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_string( ucp, (char *)pfilename, &ldh );
	ucp = ALERTS_load_object( ucp, &pline_num, sizeof(pline_num), &ldh );

	// now put the alert on the pile
	if( pgroup_name != NULL )
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_DATE_WITH_GROUP_WITH_FILENAME, ldh );
	}
	else
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_DATE_WITH_FILENAME, ldh );
	}
}

/* ---------------------------------------------------------- */
extern void Alert_range_check_failed_time_with_filename( const char *const pvariable_name, const char *const pgroup_name, const UNS_32 ptime, const char *const pfilename, const UNS_32 pline_num )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to
	// hold the 128 characters maximum string length plus the rest of the
	// alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_TIME_WITH_GROUP_WITH_FILENAME, &ldh );
	}
	else
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_TIME_WITH_FILENAME, &ldh );
	}

	ucp = ALERTS_load_string( ucp, (char *)pvariable_name, &ldh );

	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_string( ucp, (char *)pgroup_name, &ldh );
	}

	ucp = ALERTS_load_object( ucp, &ptime, sizeof(ptime), &ldh );
	ucp = ALERTS_load_string( ucp, (char *)pfilename, &ldh );
	ucp = ALERTS_load_object( ucp, &pline_num, sizeof(pline_num), &ldh );

	// now put the alert on the pile
	if( pgroup_name != NULL )
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_TIME_WITH_GROUP_WITH_FILENAME, ldh );
	}
	else
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_TIME_WITH_FILENAME, ldh );
	}
}

/* ---------------------------------------------------------- */
extern void Alert_range_check_failed_string_with_filename( const char *const pvariable_name, const char *const pstr, const char *const pfilename, const UNS_32 pline_num )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to
	// hold the 128 characters maximum string length plus the rest of the
	// alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_STRING_INVALID_CHARS_WITH_FILENAME, &ldh );

	ucp = ALERTS_load_string( ucp, (char*)pvariable_name, &ldh );
	ucp = ALERTS_load_string( ucp, (char*)pstr, &ldh );

	ucp = ALERTS_load_string( ucp, (char *)pfilename, &ldh );
	ucp = ALERTS_load_object( ucp, &pline_num, sizeof(pline_num), &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_RANGE_CHECK_FAILED_STRING_INVALID_CHARS_WITH_FILENAME, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_range_check_failed_uint32_with_filename( const char *const pvariable_name, const char *const pgroup_name, const UNS_32 pvalue, const UNS_32 pdefault, const char *const pfilename, const UNS_32 pline_num )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to
	// hold the 128 characters maximum string length plus the rest of the
	// alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_UINT32_WITH_GROUP_WITH_FILENAME, &ldh );
	}
	else
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_UINT32_WITH_FILENAME, &ldh );
	}

	ucp = ALERTS_load_string( ucp, (char *)pvariable_name, &ldh );

	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_string( ucp, (char *)pgroup_name, &ldh );
	}

	ucp = ALERTS_load_object( ucp, &pvalue, sizeof(pvalue), &ldh );
	ucp = ALERTS_load_object( ucp, &pdefault, sizeof(pdefault), &ldh );
	ucp = ALERTS_load_string( ucp, (char *)pfilename, &ldh );
	ucp = ALERTS_load_object( ucp, &pline_num, sizeof(pline_num), &ldh );

	// now put the alert on the pile
	if( pgroup_name != NULL )
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_UINT32_WITH_GROUP_WITH_FILENAME, ldh );
	}
	else
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_UINT32_WITH_FILENAME, ldh );
	}
}

#else // !ALERT_INCLUDE_FILENAME

/* ---------------------------------------------------------- */
extern void Alert_group_not_found_without_filename( void )
{
	ALERTS_build_simple_alert( AID_GROUP_NOT_FOUND );
}

/* ---------------------------------------------------------- */
extern void Alert_station_not_found_without_filename( void )
{
	ALERTS_build_simple_alert( AID_STATION_NOT_FOUND );
}

/* ---------------------------------------------------------- */
extern void Alert_station_not_in_group_without_filename( void )
{
	ALERTS_build_simple_alert( AID_STATION_NOT_IN_GROUP );
}

/* ---------------------------------------------------------- */
extern void Alert_func_call_with_null_ptr_without_filename( void )
{
	ALERTS_build_simple_alert( AID_FUNC_CALL_WITH_NULL_PTR );
}

/* ---------------------------------------------------------- */
extern void Alert_index_out_of_range_without_filename( void )
{
	ALERTS_build_simple_alert( AID_INDEX_OUT_OF_RANGE );
}

/* ---------------------------------------------------------- */
extern void Alert_item_not_on_list_without_filename( char *pitem_name, char *plist_name )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to
	// hold the 128 characters maximum string length plus the rest of the
	// alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ITEM_NOT_ON_LIST, &ldh );

	ucp = ALERTS_load_string( ucp, (char *)pitem_name, &ldh );
	ucp = ALERTS_load_string( ucp, (char *)plist_name, &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_ITEM_NOT_ON_LIST, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_bit_set_with_no_data_without_filename( void )
{
	ALERTS_build_simple_alert( AID_BIT_SET_WITH_NO_DATA );
}

/* ---------------------------------------------------------- */
extern void Alert_range_check_failed_bool_without_filename( const char *const pvariable_name, const char *const pgroup_name, const BOOL_32 pvalue_bool, const BOOL_32 pdefault_bool )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to
	// hold the 128 characters maximum string length plus the rest of the
	// alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_BOOL_WITH_GROUP, &ldh );
	}
	else
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_BOOL, &ldh );
	}

	ucp = ALERTS_load_string( ucp, (char *)pvariable_name, &ldh );

	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_string( ucp, (char *)pgroup_name, &ldh );
	}

	ucp = ALERTS_load_object( ucp, &pvalue_bool, sizeof(UNS_8), &ldh );

	// now put the alert on the pile
	if( pgroup_name != NULL )
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_BOOL_WITH_GROUP, ldh );
	}
	else
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_BOOL, ldh );
	}
}

/* ---------------------------------------------------------- */
extern void Alert_range_check_failed_int32_without_filename( const char *const pvariable_name, const char *const pgroup_name, const INT_32 pvalue_int32, const INT_32 pdefault_int32 )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to
	// hold the 128 characters maximum string length plus the rest of the
	// alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_INT32_WITH_GROUP, &ldh );
	}
	else
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_INT32, &ldh );
	}

	ucp = ALERTS_load_string( ucp, (char *)pvariable_name, &ldh );

	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_string( ucp, (char *)pgroup_name, &ldh );
	}

	ucp = ALERTS_load_object( ucp, &pvalue_int32, sizeof(pvalue_int32), &ldh );
	ucp = ALERTS_load_object( ucp, &pdefault_int32, sizeof(pdefault_int32), &ldh );

	// now put the alert on the pile
	if( pgroup_name != NULL )
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_INT32_WITH_GROUP, ldh );
	}
	else
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_INT32, ldh );
	}
}

/* ---------------------------------------------------------- */
extern void Alert_range_check_failed_float_without_filename( const char *const pvariable_name, const char *const pgroup_name, const float pvalue_float, const float pdefault_float )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to
	// hold the 128 characters maximum string length plus the rest of the
	// alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_FLOAT_WITH_GROUP, &ldh );
	}
	else
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_FLOAT, &ldh );
	}

	ucp = ALERTS_load_string( ucp, (char *)pvariable_name, &ldh );

	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_string( ucp, (char *)pgroup_name, &ldh );
	}

	ucp = ALERTS_load_object( ucp, &pvalue_float, sizeof(pvalue_float), &ldh );
	ucp = ALERTS_load_object( ucp, &pdefault_float, sizeof(pdefault_float), &ldh );

	// now put the alert on the pile
	if( pgroup_name != NULL )
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_FLOAT_WITH_GROUP, ldh );
	}
	else
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_FLOAT, ldh );
	}
}

/* ---------------------------------------------------------- */
extern void Alert_range_check_failed_date_without_filename( const char *const pvariable_name, const char *const pgroup_name, const UNS_32 pdate )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to
	// hold the 128 characters maximum string length plus the rest of the
	// alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_DATE_WITH_GROUP, &ldh );
	}
	else
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_DATE, &ldh );
	}

	ucp = ALERTS_load_string( ucp, (char *)pvariable_name, &ldh );

	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_string( ucp, (char *)pgroup_name, &ldh );
	}

	ucp = ALERTS_load_object( ucp, &pdate, sizeof(UNS_16), &ldh );

	// now put the alert on the pile
	if( pgroup_name != NULL )
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_DATE_WITH_GROUP, ldh );
	}
	else
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_DATE, ldh );
	}
}

/* ---------------------------------------------------------- */
extern void Alert_range_check_failed_time_without_filename( const char *const pvariable_name, const char *const pgroup_name, const UNS_32 ptime )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to
	// hold the 128 characters maximum string length plus the rest of the
	// alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_TIME_WITH_GROUP, &ldh );
	}
	else
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_TIME, &ldh );
	}

	ucp = ALERTS_load_string( ucp, (char *)pvariable_name, &ldh );

	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_string( ucp, (char *)pgroup_name, &ldh );
	}

	ucp = ALERTS_load_object( ucp, &ptime, sizeof(ptime), &ldh );

	// now put the alert on the pile
	if( pgroup_name != NULL )
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_TIME_WITH_GROUP, ldh );
	}
	else
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_TIME, ldh );
	}
}

/* ---------------------------------------------------------- */
extern void Alert_range_check_failed_string_without_filename( const char *const pvariable_name, const char *const pstr )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to
	// hold the 128 characters maximum string length plus the rest of the
	// alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_STRING_INVALID_CHARS, &ldh );

	ucp = ALERTS_load_string( ucp, (char*)pvariable_name, &ldh );
	ucp = ALERTS_load_string( ucp, (char*)pstr, &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_RANGE_CHECK_FAILED_STRING_INVALID_CHARS, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_range_check_failed_uint32_without_filename( const char *const pvariable_name, const char *const pgroup_name, const UNS_32 pvalue, const UNS_32 pdefault )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to
	// hold the 128 characters maximum string length plus the rest of the
	// alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_UINT32_WITH_GROUP, &ldh );
	}
	else
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_UINT32, &ldh );
	}

	ucp = ALERTS_load_string( ucp, (char *)pvariable_name, &ldh );

	if( pgroup_name != NULL )
	{
		ucp = ALERTS_load_string( ucp, (char *)pgroup_name, &ldh );
	}

	ucp = ALERTS_load_object( ucp, &pvalue, sizeof(pvalue), &ldh );
	ucp = ALERTS_load_object( ucp, &pdefault, sizeof(pdefault), &ldh );

	// now put the alert on the pile
	if( pgroup_name != NULL )
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_UINT32_WITH_GROUP, ldh );
	}
	else
	{
		ALERTS_add_alert( AID_RANGE_CHECK_FAILED_UINT32, ldh );
	}
}

#endif	// ALERT_INCLUDE_FILENAME

/* ---------------------------------------------------------- */
extern void Alert_string_too_long( const char *const pstr, const UNS_32 pallowed_length )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to hold
	// the 128 characters maximum string length plus the rest of the alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RANGE_CHECK_FAILED_STRING_TOO_LONG, &ldh );

	ucp = ALERTS_load_string( ucp, (char*)pstr, &ldh );
	ucp = ALERTS_load_object( ucp, &pallowed_length, sizeof(pallowed_length), &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_RANGE_CHECK_FAILED_STRING_TOO_LONG, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_flash_file_system_passed( const UNS_32 pflash_index )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to hold
	// the 128 characters maximum string length plus the rest of the alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_FLASH_FILE_SYSTEM_PASSED, &ldh );

	ucp = ALERTS_load_object( ucp, &pflash_index, sizeof(UNS_8), &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_FLASH_FILE_SYSTEM_PASSED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_flash_file_found( char *pfilename, const INT_32 pversion, const INT_32 pedit_count, const INT_32 pitem_size )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to hold
	// the 128 characters maximum string length plus the rest of the alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_FLASH_FILE_FOUND, &ldh );

	ucp = ALERTS_load_string( ucp, pfilename, &ldh );
	ucp = ALERTS_load_object( ucp, &pversion, sizeof(pversion), &ldh );
	ucp = ALERTS_load_object( ucp, &pedit_count, sizeof(pedit_count), &ldh );
	ucp = ALERTS_load_object( ucp, &pitem_size, sizeof(pitem_size), &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_FLASH_FILE_FOUND, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_flash_file_size_error( char *pfilename )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to hold
	// the 128 characters maximum string length plus the rest of the alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_FLASH_FILE_SIZE_ERROR, &ldh );

	ucp = ALERTS_load_string( ucp, pfilename, &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_FLASH_FILE_SIZE_ERROR, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_flash_file_found_old_version( char *pfilename )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to hold
	// the 128 characters maximum string length plus the rest of the alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_FLASH_FILE_FOUND_OLD_VERSION, &ldh );

	ucp = ALERTS_load_string( ucp, pfilename, &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_FLASH_FILE_FOUND_OLD_VERSION, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_flash_file_not_found( char *pfilename )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to hold
	// the 128 characters maximum string length plus the rest of the alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_FLASH_FILE_NOT_FOUND, &ldh );

	ucp = ALERTS_load_string( ucp, pfilename, &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_FLASH_FILE_NOT_FOUND, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_flash_file_deleting_obsolete( const UNS_32 pflash_index, char *pfilename  )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to hold
	// the 128 characters maximum string length plus the rest of the alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_FLASH_FILE_DELETING_OBSOLETE, &ldh );

	ucp = ALERTS_load_object( ucp, &pflash_index, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_string( ucp, pfilename, &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_FLASH_FILE_DELETING_OBSOLETE, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_flash_file_obsolete_file_not_found( char *pfilename )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to hold
	// the 128 characters maximum string length plus the rest of the alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_FLASH_FILE_OBSOLETE_NOT_FOUND, &ldh );

	ucp = ALERTS_load_string( ucp, pfilename, &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_FLASH_FILE_OBSOLETE_NOT_FOUND, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_flash_file_deleting( const UNS_32 pflash_index, char *pfilename, const INT_32 pversion, const INT_32 pedit_count, const INT_32 pitem_size  )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to hold
	// the 128 characters maximum string length plus the rest of the alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_FLASH_FILE_DELETING, &ldh );

	ucp = ALERTS_load_object( ucp, &pflash_index, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_string( ucp, pfilename, &ldh );
	ucp = ALERTS_load_object( ucp, &pversion, sizeof(pversion), &ldh );
	ucp = ALERTS_load_object( ucp, &pedit_count, sizeof(pedit_count), &ldh );
	ucp = ALERTS_load_object( ucp, &pitem_size, sizeof(pitem_size), &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_FLASH_FILE_DELETING, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_flash_file_old_version_not_found( const UNS_32 pflash_index, char *pfilename )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to hold
	// the 128 characters maximum string length plus the rest of the alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_FLASH_FILE_OLD_VERSION_NOT_FOUND, &ldh );

	ucp = ALERTS_load_object( ucp, &pflash_index, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_string( ucp, pfilename, &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_FLASH_FILE_OLD_VERSION_NOT_FOUND, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_flash_file_writing( const UNS_32 pflash_index, char *pfilename, const INT_32 pversion, const INT_32 pedit_count, const INT_32 pitem_size  )
{
	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to hold
	// the 128 characters maximum string length plus the rest of the alert.
	unsigned char   lalert[ 256 ];

	unsigned char   *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_FLASH_FILE_WRITING, &ldh );

	ucp = ALERTS_load_object( ucp, &pflash_index, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_string( ucp, pfilename, &ldh );
	ucp = ALERTS_load_object( ucp, &pversion, sizeof(pversion), &ldh );
	ucp = ALERTS_load_object( ucp, &pedit_count, sizeof(pedit_count), &ldh );
	ucp = ALERTS_load_object( ucp, &pitem_size, sizeof(pitem_size), &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_FLASH_FILE_WRITING, ldh );
}

/* ---------------------------------------------------------- */
static void __alert_tp_micro_message( const char pwho_generated_letter, char *pmessage )
{
	// ---------

	UNS_8   str_128[ 128 ];

	snprintf( (char*)str_128, sizeof(str_128 ), "%c: %s", pwho_generated_letter, pmessage );

	// ---------

	unsigned char   *ucp;

	DATA_HANDLE     ldh;

	// Build the alert on the stack in this array. Must be big enough to hold the 128 characters
	// maximum string length plus the rest of the alert.
	UNS_8   lalert[ 256 ];

	// ---------

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_TP_MICRO_ERROR, &ldh );

	ucp = ALERTS_load_string( ucp, (char*)str_128, &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_TP_MICRO_ERROR, ldh );
}

// ---------------

// 8/17/2012 rmd : The 'T' means an alert rcvd via the communications and therefore
// generated by the TP MICRO itself. The 'M' means an alert generated here at the MAIN
// BOARD. These alerts are tpmicro related of course.

/* ---------------------------------------------------------- */
extern void Alert_message_on_tpmicro_pile_M( char *pmessage )
{
	__alert_tp_micro_message( 'M', pmessage );
}

/* ---------------------------------------------------------- */
extern void Alert_message_on_tpmicro_pile_T( char *pmessage )
{
	__alert_tp_micro_message( 'T', pmessage );
}

/* ---------------------------------------------------------- */
extern void Alert_user_pile_restarted( const BOOL_32 preason )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ALERTS_PILE_USER_INITIALIZED, &ldh );

	ucp = ALERTS_load_object( ucp, &preason, sizeof(REASONS_TO_INIT_BATTERY_BACKED_VARS), &ldh );

	ALERTS_add_alert( AID_ALERTS_PILE_USER_INITIALIZED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_change_pile_restarted( const BOOL_32 preason )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ALERTS_PILE_CHANGE_LINES_INITIALIZED, &ldh );

	ucp = ALERTS_load_object( ucp, &preason, sizeof(REASONS_TO_INIT_BATTERY_BACKED_VARS), &ldh );

	ALERTS_add_alert( AID_ALERTS_PILE_CHANGE_LINES_INITIALIZED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_tp_micro_pile_restarted( const BOOL_32 preason )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ALERTS_PILE_TP_MICRO_INITIALIZED, &ldh );

	ucp = ALERTS_load_object( ucp, &preason, sizeof(REASONS_TO_INIT_BATTERY_BACKED_VARS), &ldh );

	ALERTS_add_alert( AID_ALERTS_PILE_TP_MICRO_INITIALIZED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_engineering_pile_restarted( const BOOL_32 preason )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ALERTS_PILE_ENGINEERING_INITIALIZED, &ldh );

	ucp = ALERTS_load_object( ucp, &preason, sizeof(REASONS_TO_INIT_BATTERY_BACKED_VARS), &ldh );

	ALERTS_add_alert( AID_ALERTS_PILE_ENGINEERING_INITIALIZED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_battery_backed_var_initialized( char *pvar_name, const BOOL_32 preason )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_BATTERY_BACKED_VAR_INITIALIZED, &ldh );

	ucp = ALERTS_load_string( ucp, pvar_name, &ldh );
	ucp = ALERTS_load_object( ucp, &preason, sizeof(REASONS_TO_INIT_BATTERY_BACKED_VARS), &ldh );

	ALERTS_add_alert( AID_BATTERY_BACKED_VAR_INITIALIZED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_battery_backed_var_valid( char *pvar_name )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_BATTERY_BACKED_VAR_VALID, &ldh );

	ucp = ALERTS_load_string( ucp, pvar_name, &ldh );

	ALERTS_add_alert( AID_BATTERY_BACKED_VAR_VALID, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_comm_crc_failed( const UNS_32 pport )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_COMM_CRC_FAILED, &ldh );

	ucp = ALERTS_load_object( ucp, &pport, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_COMM_CRC_FAILED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_cts_timeout( const UNS_32 pport, const REASONS_FOR_CTS_TIMEOUT preason )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_CTS_TIMEOUT, &ldh );

	ucp = ALERTS_load_object( ucp, &pport, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &preason, sizeof(preason), &ldh );

	ALERTS_add_alert( AID_CTS_TIMEOUT, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_derate_table_update_failed( const UNS_32 paid, const UNS_32 pmeasured, const UNS_32 pexpected, char *pvalves_on_str )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];

	unsigned char *ucp;


	ucp = ALERTS_load_common( (UNS_8*)&lalert, paid, &ldh );

	ucp = ALERTS_load_object( ucp, &pmeasured, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &pexpected, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_string( ucp, pvalves_on_str, &ldh );

	ALERTS_add_alert( paid, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_derate_table_update_successful( const UNS_32 pvalves_on, const UNS_32 pflow_rate, const UNS_32 pindex, const UNS_32 pcount_from, const UNS_32 pcount_to, const INT_16 pvalue_from, const INT_16 pvalue_to )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_DERATE_TABLE_UPDATE_SUCCESSFUL, &ldh );

	ucp = ALERTS_load_object( ucp, &pvalves_on, sizeof(UNS_32), &ldh );
	ucp = ALERTS_load_object( ucp, &pflow_rate, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &pindex, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &pcount_from, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pcount_to, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pvalue_from, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &pvalue_to, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_DERATE_TABLE_UPDATE_SUCCESSFUL, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_derate_table_update_station_count_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, const UNS_32 pcount )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_DERATE_TABLE_UPDATE_STATION_COUNT, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );  // only take the first byte
	ucp = ALERTS_load_object( ucp, &pstation_number_0, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &pcount, sizeof(pcount), &ldh );

	ALERTS_add_alert( AID_DERATE_TABLE_UPDATE_STATION_COUNT, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_unit_communicated( char *pstr )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_UNIT_COMMUNICATED, &ldh );

	ucp = ALERTS_load_string( ucp, pstr, &ldh );

	ALERTS_add_alert( AID_UNIT_COMMUNICATED, ldh );
}

/* ---------------------------------------------------------- */
#ifdef ALERT_MESSAGE_NORMAL

/* ---------------------------------------------------------- */
extern void Alert_Message( char *pmessage )
{
	// For general purpose message alert message use...because the string is the alert this alert is not a very efficient
	// use of the pile so don't use for routine alerts.

	DATA_HANDLE     ldh;

	unsigned char   lalert[ 256 ];	// Build the alert on the stack in this array. Must be big enough to hold
									// the 128 characters maximum string length plus the rest of the alert.

	unsigned char   *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ERROR, &ldh );

	ucp = ALERTS_load_string( ucp, pmessage, &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_ERROR, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_Message_va( char *fmt, ... )
{
	// For general purpose message alert message use...not very efficient use of the pile so don't use for
	// routine alerts. Mostly for detected error conditions.

	DATA_HANDLE     ldh;

	// 2011.08.31 rmd : It is unfortunate that we cannot use mem_malloc to get memory from our
	// memory pool for this function. It is hard enough to set the stack size without having a
	// sporadic need for an extra 512 bytes for these two arrays. But see the top of this file.
	// There is a note about not using the mem_malloc and mem_free functions. While holding the
	// alerts_pile_mutex. Hmmm so if this really was a problem this could be done. But boy to
	// test that....I think I'll leave it alone the way it is declaring these two arrays. As
	// I've got quite alot of testing on it now. Specifically testing aimed at the deadlock
	// issue.
	char            str_text[ 256 ], lalert[ 256 ];

	unsigned char   *ucp;

	va_list         arg_ptr;


	va_start( arg_ptr, fmt );

	vsnprintf( str_text, sizeof(str_text), fmt, arg_ptr );

	va_end( arg_ptr );


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ERROR, &ldh );

	ucp = ALERTS_load_string( ucp, str_text, &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_ERROR, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_Message_dt( char *pmessage, const DATE_TIME pdt )
{
	// what makes this special is we can pass the DT that we want to stamp on the line

	// for general purpose message alert message use...not very efficient use of the pile so don't use for
	// routine alerts

	DATA_HANDLE ldh;

	unsigned char   lalert[ 256 ];	// Build the alert on the stack in this array. Must be big enough to hold
									// the 128 characters maximum string length plus the rest of the alert.
	unsigned char *ucp;


	ucp = ALERTS_load_common_with_dt( (unsigned char*)&lalert, AID_ERROR, &ldh, &pdt );

	ucp = ALERTS_load_string( ucp, pmessage, &ldh );

	// now put the alert on the pile
	ALERTS_add_alert( AID_ERROR, ldh );
}

#elif defined( ALERT_MESSAGE_WRITES_TO_DEBUG_PRINTF )

extern void Alert_Message( char *pmessage )
{
	debug_printf( pmessage );
	debug_printf( "\r\n" );
}

// NEED TODO an Alert_Message_va() !!

extern void Alert_Message_dt( char *pmessage, const DATE_TIME pdt )
{
	debug_printf( pmessage );
	debug_printf( "\r\n" );
}

#elif defined( ALERT_MESSAGE_HALTS_AT_CALLER_LOCATION )

// nothing to do here

#elif defined( ALERT_MESSAGE_GENERATES_NO_CODE_AT_ALL )

// nothing to do here

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*
extern void Alert_Exception( EXCEPTION_LINE_TYPE XType )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];  // build the alert on the stack in this array

	EXCEPTION_LINE_TYPE *xltp;

	xltp = AP_LoadCommon( (unsigned char*)&lalert, AID_EXCEPTION, &ldh );

	*xltp = XType;
	ldh.dlen += sizeof( EXCEPTION_LINE_TYPE );

	ALERTS_add_alert( AID_EXCEPTION, ldh );
}
*/

/* ---------------------------------------------------------- */
/**
 * Adds the mainline break alert to the alerts pile.
 *
 * @executed
 *
 * @param pmeasured_u16 The measured flow rate which tripped the mainline break.
 * @param pallowed_u16 The allowed flow rate before a mainline break is tripped.
 * @param pmlb_type_u8 The type of mainline break that was tripped - during
 *  				   irrigation, during mvor opened, or during all other
 *  				   times.
 *
 * @author 5/18/2012 Adrianusv
 *
 * @revisions
 *    5/18/2012 Initial release
 */
extern void Alert_mainline_break( char * const ppre_str,
								  char * const psystem_name,
								  UNS_32 const pduring_type,
								  UNS_32 const pmeasured,
								  UNS_32 const pallowed )
{
	DATA_HANDLE ldh;

	// 4/29/2015 rmd : Build the alert on the stack. APPARENTLY NOT ALLOWED TO USE OUR MEMORY
	// POOL (some old reason that I'm not going to dig into now - maybe alerts made within the
	// memory pool code? - if so I'd eliminate them now.)
	UNS_8	lalert[ 256 ];

	UNS_8	*ucp;

	ucp = ALERTS_load_common( lalert, AID_MLB, &ldh );

	ucp = ALERTS_load_string( ucp, (char * const)ppre_str, &ldh );

	ucp = ALERTS_load_string( ucp, psystem_name, &ldh );
	
	// 4/29/2015 rmd : Place the during_type on the stack as a single byte.
	ucp = ALERTS_load_object( ucp, &pduring_type, sizeof(UNS_8), &ldh );

	ucp = ALERTS_load_object( ucp, &pmeasured, sizeof(UNS_16), &ldh );

	ucp = ALERTS_load_object( ucp, &pallowed, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_MLB, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_mainline_break_cleared( const UNS_32 psystem_gid )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MLB_CLEARED, &ldh );

	ucp = ALERTS_load_object( ucp, &psystem_gid, sizeof( psystem_gid ), &ldh );

	ALERTS_add_alert( AID_MLB_CLEARED, ldh );
}

#if 0
/* ---------------------------------------------------------- */
extern void Alert_MainlineBreak_request_to_clear( const CLEARED_MLB_HOW pcleared_how )
{
	if( pcleared_how == CLEAR_MLB_FROM_KEYPAD )
	{
		ALERTS_build_simple_alert( AID_MLB_CLEAR_REQUEST_KEYPAD );
	}
	else if( pcleared_how == CLEAR_MLB_FROM_COMMLINK )
	{
		ALERTS_build_simple_alert( AID_MLB_CLEAR_REQUEST_COMMLINK );
	}
}
#endif

/* ---------------------------------------------------------- */
extern void Alert_flow_not_checked_with_no_reasons_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	// 6/20/2012 rmd : We want to manage the size of the alert and therefore use
	// reduced variable sizes where we can. This technique of taking say only
	// the first two bytes of an UNS_32 and calling it an UNS_16 only works
	// cause this is a LITTLE-ENDIAN machine. It will not work on BIG-ENDIAN
	// devices.

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_FLOW_NOT_CHECKED_NO_REASONS, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pstation_number_0, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_FLOW_NOT_CHECKED_NO_REASONS, ldh );
}

/* ---------------------------------------------------------- */
/*
extern void Alert_FlowCheckPassed( const UNS_8 more_than_one_ON,
								   const char addr,
								   const UNS_16 station,
								   const UNS_16 expected,
								   const INT_32 derate_amount,
								   const UNS_16 plus_window,
								   const UNS_16 minus_window,
								   const UNS_16 hi_limit,
								   const UNS_16 lo_limit,
								   const UNS_16 rate )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 96 ];  // this is where the alert gets built!

	unsigned char *ucp;

	ucp = AP_LoadCommon( (unsigned char*)&lalert, AID_FLOW_CHECK_PASSED, &ldh );

	ucp = AP_LoadObject( ucp, &more_than_one_ON, sizeof(more_than_one_ON), &ldh );

	ucp = AP_LoadObject( ucp, &addr, sizeof(addr), &ldh );

	ucp = AP_LoadObject( ucp, &station, sizeof(station), &ldh );
	ucp = AP_LoadObject( ucp, &expected, sizeof(expected), &ldh );
	ucp = AP_LoadObject( ucp, &derate_amount, sizeof(derate_amount), &ldh );

	ucp = AP_LoadObject( ucp, &plus_window, sizeof(plus_window), &ldh );
	ucp = AP_LoadObject( ucp, &minus_window, sizeof(minus_window), &ldh );
	ucp = AP_LoadObject( ucp, &hi_limit, sizeof(hi_limit), &ldh );
	ucp = AP_LoadObject( ucp, &lo_limit, sizeof(lo_limit), &ldh );

	ucp = AP_LoadObject( ucp, &rate, sizeof(rate), &ldh );

	ALERTS_add_alert( AID_FLOW_CHECK_PASSED, ldh );
}
*/

/* ---------------------------------------------------------- */
extern void Alert_flow_error_idx( const UNS_32 paid,
							  const UNS_32 preason_in_list,
							  const UNS_32 pbox_index_0,
							  const UNS_32 pstation_number_0,
							  const UNS_32 psystem_expected,
							  const UNS_32 psystem_derated_expected,
							  const UNS_32 psystem_limit,
							  const UNS_32 psystem_actual_flow,
							  const UNS_32 pstation_derated_expected,
							  const UNS_32 pstation_share_of_actual_flow,
							  const UNS_32 pstations_on )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 96 ];	 // this is where the alert gets built!

	unsigned char *ucp;


	// 6/20/2012 rmd : We want to manage the size of the alert and therefore use reduced
	// variable sizes where we can. This technique of taking say only the first two bytes of an
	// UNS_32 and calling it an UNS_16 only works cause this is a LITTLE-ENDIAN machine. It will
	// not work on BIG-ENDIAN devices.

	ucp = ALERTS_load_common( (unsigned char*)&lalert, paid, &ldh );

	ucp = ALERTS_load_object( ucp, &preason_in_list, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pstation_number_0, sizeof(UNS_16), &ldh );

	ucp = ALERTS_load_object( ucp, &psystem_expected, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &psystem_derated_expected, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &psystem_limit, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &psystem_actual_flow, sizeof(UNS_16), &ldh );

	ucp = ALERTS_load_object( ucp, &pstation_derated_expected, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &pstation_share_of_actual_flow, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &pstations_on, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( paid, ldh );
}

#if 0
/* ---------------------------------------------------------- */
extern void Alert_flow_check_learning( const UNS_8 pcontroller_addr, const UNS_16 pstation )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_FLOW_CHECK_LEARNING, &ldh );

	ucp = ALERTS_load_object( ucp, &pcontroller_addr, sizeof(pcontroller_addr), &ldh );
	ucp = ALERTS_load_object( ucp, &pstation, sizeof(pstation), &ldh );

	ALERTS_add_alert( AID_FLOW_CHECK_LEARNING, ldh );
}
#endif

/* ---------------------------------------------------------- */
extern void Alert_conventional_mv_no_current_idx( const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	UNS_32	laid;

	laid = AID_CONVENTIONAL_MV_NO_CURRENT;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, laid, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( laid, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_conventional_pump_no_current_idx( const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	UNS_32	laid;

	laid = AID_CONVENTIONAL_PUMP_NO_CURRENT;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, laid, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( laid, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_conventional_station_no_current_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_CONVENTIONAL_STATION_NO_CURRENT, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ucp = ALERTS_load_object( ucp, &pstation_number_0, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_CONVENTIONAL_STATION_NO_CURRENT, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_conventional_station_short_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_CONVENTIONAL_STATION_SHORT, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );  // take only the first byte

	ucp = ALERTS_load_object( ucp, &pstation_number_0, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_CONVENTIONAL_STATION_SHORT, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_conventional_output_unknown_short_idx( const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_CONVENTIONAL_OUTPUT_UNKNOWN_SHORT, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_CONVENTIONAL_OUTPUT_UNKNOWN_SHORT, ldh );
}

#if 0
/* ---------------------------------------------------------- */
extern void Alert_conventional_lights_short( const UNS_16 plights_station )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SHORT_LIGHT, &ldh );

	ucp = ALERTS_load_object( ucp, &plights_station, sizeof(plights_station), &ldh );

	ALERTS_add_alert( AID_SHORT_LIGHT, ldh );
}
#endif

/* ---------------------------------------------------------- */
extern void Alert_conventional_mv_short_idx( const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_CONVENTIONAL_MV_SHORT, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_CONVENTIONAL_MV_SHORT, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_conventional_pump_short_idx( const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_CONVENTIONAL_PUMP_SHORT, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_CONVENTIONAL_PUMP_SHORT, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_two_wire_cable_excessive_current_idx( const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_TWO_WIRE_CABLE_EXCESSIVE_CURRENT, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_TWO_WIRE_CABLE_EXCESSIVE_CURRENT, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_two_wire_cable_over_heated_idx( const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_TWO_WIRE_CABLE_OVER_HEATED, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_TWO_WIRE_CABLE_OVER_HEATED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_two_wire_cable_cooled_off_idx( const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_TWO_WIRE_CABLE_COOLED_OFF, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_TWO_WIRE_CABLE_COOLED_OFF, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_two_wire_station_decoder_fault_idx( const UNS_32 pfault_code, const UNS_32 pbox_index_0, const UNS_32 pdecoder_serial_number, const UNS_32 pstation_number_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_TWO_WIRE_STATION_DECODER_FAULT, &ldh );

	ucp = ALERTS_load_object( ucp, &pfault_code, sizeof(UNS_8), &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ucp = ALERTS_load_object( ucp, &pdecoder_serial_number, sizeof(UNS_32), &ldh );

	ucp = ALERTS_load_object( ucp, &pstation_number_0, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_TWO_WIRE_STATION_DECODER_FAULT, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_two_wire_poc_decoder_fault_idx( const UNS_32 pfault_code, const UNS_32 pbox_index_0, const UNS_32 pdecoder_serial_number, char *ppoc_name )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 64 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_TWO_WIRE_POC_DECODER_FAULT, &ldh );

	ucp = ALERTS_load_object( ucp, &pfault_code, sizeof(UNS_8), &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ucp = ALERTS_load_object( ucp, &pdecoder_serial_number, sizeof(UNS_32), &ldh );

	ucp = ALERTS_load_string( ucp, (char *)ppoc_name, &ldh );

	ALERTS_add_alert( AID_TWO_WIRE_POC_DECODER_FAULT, ldh );
}

/* ---------------------------------------------------------- */
#if 0
extern void Alert_IrrigationTurnedOFF_with_reason( const INITIATED_VIA_ENUM pwho_did_it )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_IRRIGATION_TURNED_OFF_WITH_REASON, &ldh );

	ucp = ALERTS_load_object( ucp, &pwho_did_it, sizeof(pwho_did_it), &ldh );

	ALERTS_add_alert( AID_IRRIGATION_TURNED_OFF_WITH_REASON, ldh );
}
#endif

/* ---------------------------------------------------------- */
#if 0
extern void Alert_IrrigationTurnedON_with_reason( const INITIATED_VIA_ENUM pwho_did_it )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_IRRIGATION_TURNED_ON_WITH_REASON, &ldh );

	ucp = ALERTS_load_object( ucp, &pwho_did_it, sizeof(pwho_did_it), &ldh );

	ALERTS_add_alert( AID_IRRIGATION_TURNED_ON_WITH_REASON, ldh );
}
#endif

/* ---------------------------------------------------------- */
#if 0
extern void Alert_IrrigationIsOFF_Reminder( void )
{
	ALERTS_build_simple_alert( AID_IRRIGATION_IS_OFF_REMINDER );
}
#endif

/* ---------------------------------------------------------- */
#if 0
extern void Alert_SetHO_ByStation( const UNS_16 pstation, const UNS_32 pho )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SETHO_BYSTATION, &ldh );

	ucp = ALERTS_load_object( ucp, &pstation, sizeof(pstation), &ldh );
	ucp = ALERTS_load_object( ucp, &pho, sizeof(pho), &ldh );

	ALERTS_add_alert( AID_SETHO_BYSTATION, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_SetHO_ByProgram( const UNS_8 pprog, const UNS_32 pho )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SETHO_BYPROG, &ldh );

	ucp = ALERTS_load_object( ucp, &pprog, sizeof(pprog), &ldh );
	ucp = ALERTS_load_object( ucp, &pho, sizeof(pho), &ldh );

	ALERTS_add_alert( AID_SETHO_BYPROG, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_SetHO_AllStations( const UNS_32 pho )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SETHO_ALLSTATIONS, &ldh );

	ucp = ALERTS_load_object( ucp, &pho, sizeof(pho), &ldh );

	ALERTS_add_alert( AID_SETHO_ALLSTATIONS, ldh );
}
#endif

/* ---------------------------------------------------------- */
#if 0
extern void Alert_SetNOWDays_ByStation( const UNS_16 pstation, const UNS_16 pnowdays )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SETNOWDAYS_BYSTATION, &ldh );

	ucp = ALERTS_load_object( ucp, &pstation, sizeof(pstation), &ldh );
	ucp = ALERTS_load_object( ucp, &pnowdays, sizeof(pnowdays), &ldh );

	ALERTS_add_alert( AID_SETNOWDAYS_BYSTATION, ldh );
}
#endif

/* ---------------------------------------------------------- */
extern void Alert_set_no_water_days_by_station_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, const UNS_32 pnow_days )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SETNOWDAYS_BYSTATION, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pstation_number_0, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &pnow_days, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_SETNOWDAYS_BYSTATION, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_set_no_water_days_by_group( char *pstr, const UNS_32 pgroup_ID, const UNS_32 pnowdays )
{
	DATA_HANDLE ldh;

	// 8/29/2013 ajv : Since we're passing in a string that's up to 48
	// characters, make sure the buffer is big enough to support it plus the
	// rest of the associated data.
	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SETNOWDAYS_BYGROUP, &ldh );
	
	ucp = ALERTS_load_string( ucp, pstr, &ldh );
	ucp = ALERTS_load_object( ucp, &pgroup_ID, sizeof(UNS_32), &ldh );
	ucp = ALERTS_load_object( ucp, &pnowdays, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_SETNOWDAYS_BYGROUP, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_set_no_water_days_all_stations( const UNS_32 pnowdays )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SETNOWDAYS_BYALL , &ldh );

	ucp = ALERTS_load_object( ucp, &pnowdays, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_SETNOWDAYS_BYALL, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_set_no_water_days_by_box( const UNS_32 pbox_index_0, const UNS_32 pnowdays )
{
	DATA_HANDLE ldh;

	unsigned char lalert[32];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SETNOWDAYS_BYBOX, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pnowdays, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_SETNOWDAYS_BYBOX, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_reset_moisture_balance_all_stations( void )
{
	ALERTS_build_simple_alert( AID_RESET_MOISTURE_BALANCE_ALLSTATIONS );
}

/* ---------------------------------------------------------- */
extern void Alert_reset_moisture_balance_by_group( char *pstr, const UNS_32 pgroup_ID )
{
	DATA_HANDLE ldh;

	// 3/15/2017 ajv : Since we're passing in a string that's up to 48 characters, make sure the
	// buffer is big enough to support it plus the rest of the associated data.
	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RESET_MOISTURE_BALANCE_BYGROUP, &ldh );
	
	ucp = ALERTS_load_string( ucp, pstr, &ldh );
	ucp = ALERTS_load_object( ucp, &pgroup_ID, sizeof(UNS_32), &ldh );

	ALERTS_add_alert( AID_RESET_MOISTURE_BALANCE_BYGROUP, ldh );
}

#if 0
/* ---------------------------------------------------------- */
extern void Alert_passwords_reset( const PASSWORD_RESET_REASON preason )
{
	if( preason == PWRESET_UPDATE_FROM_UNKNOWN_ROM_VERSION )
	{
		ALERTS_build_simple_alert( AID_PASSWORDS_RESET_FIRMWARE_VERSION );
	}
	else if( preason == PWRESET_CMOS_UNITIALIZED )
	{
		ALERTS_build_simple_alert( AID_PASSWORDS_RESET_SRAM );
	}
	else if( preason == PWRESET_COMMLINK )
	{
		ALERTS_build_simple_alert( AID_PASSWORDS_RESET_COMMLINK );
	}
}

/* ---------------------------------------------------------- */
extern void Alert_PasswordsReceivedFromPC( void )
{
	ALERTS_build_simple_alert( AID_PASSWORDS_RECEIVED_FROM_PC );
}

/* ---------------------------------------------------------- */
extern void Alert_PasswordsSentToPC( void )
{
	ALERTS_build_simple_alert( AID_PASSWORDS_SENT_TO_PC );
}

/* ---------------------------------------------------------- */
extern void Alert_PasswordsUnlocked( void )
{
	ALERTS_build_simple_alert( AID_PASSWORDS_UNLOCKED );
}

/* ---------------------------------------------------------- */
extern void Alert_PasswordLogIn( const UNS_16 plevel, char *pstr )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_PASSWORDS_LOGIN , &ldh );

	ucp = ALERTS_load_object( ucp, &plevel, sizeof(plevel), &ldh );

	ucp = ALERTS_load_string( ucp, pstr, &ldh );

	ALERTS_add_alert( AID_PASSWORDS_LOGIN, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_RRE_PasswordLogIn( const UNS_8 plevel, char *p_login_str, char *p_handheld_name_str )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 192 ];  // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RRE_PASSWORD_LOGIN, &ldh );

	ucp = ALERTS_load_object( ucp, &plevel, sizeof(plevel), &ldh );

	ucp = ALERTS_load_string( ucp, p_login_str, &ldh );

	ucp = ALERTS_load_string( ucp, p_handheld_name_str, &ldh );

	ALERTS_add_alert( AID_RRE_PASSWORD_LOGIN, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_PasswordUnlockedCanceled( const PASSWORD_CANCEL_UNLOCK_REASON preason )
{
	if( preason == PW_CANCEL_UNLOCK_KEYPAD_TIMEOUT )
	{
		ALERTS_build_simple_alert( AID_PASSWORDS_CANCEL_UNLOCK_KEYPAD_TIMEOUT );
	}
	else if( preason == PW_CANCEL_UNLOCK_USER_CANCELLED )
	{
		ALERTS_build_simple_alert( AID_PASSWORDS_CANCEL_UNLOCK_USER_CANCELLED );
	}
}

/* ---------------------------------------------------------- */
extern void Alert_PasswordLoggedOut( const PASSWORD_LOGOUT_REASON preason, const UNS_16 plevel )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	UNS_8   *ucp;

	UNS_32  laid;

	if( preason == PW_LOGOUT_KEYPAD_TIMEOUT )
	{
		laid = AID_PASSWORDS_LOGOUT_KEYPAD_TIMEOUT;
	}
	else if( preason == PW_LOGOUT_USER_LOGGEDOUT )
	{
		laid = AID_PASSWORDS_LOGOUT_USER_LOGGED_OUT;
	}
	else
	{
		laid = 0;
	}

	if( laid != 0 )
	{
		ucp = ALERTS_load_common( (UNS_8*)&lalert, laid, &ldh );

		ucp = ALERTS_load_object( ucp, &plevel, sizeof(plevel), &ldh );

		ALERTS_add_alert( laid, ldh );
	}
}

/* ---------------------------------------------------------- */
extern void Alert_SystemCapacityAutomaticallyAdjusted( const char paddr, const UNS_16 pstation, const UNS_16 pfrom, const UNS_16 pto )
{
	DATA_HANDLE ldh;

	unsigned char tstr[ 16 ], lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SYSTEM_CAPACITY_AUTOMATICALLY_ADJUSTED_UP, &ldh );

	sprintf( (char*)tstr, "%c%d", paddr, pstation+1 );

	ucp = ALERTS_load_string( ucp, (char*)tstr, &ldh );
	ucp = ALERTS_load_object( ucp, &pfrom, sizeof(pfrom), &ldh );
	ucp = ALERTS_load_object( ucp, &pto, sizeof(pto), &ldh );

	ALERTS_add_alert( AID_SYSTEM_CAPACITY_AUTOMATICALLY_ADJUSTED_UP, ldh );
}
#endif

#if 0
/* ---------------------------------------------------------- */
// EXPLANATION: The MVOR gets initiated (requested) from either the keyboard or
// comm link at 1 controller in the chain. The mvor gets started at each irri
// machine as the mvor gets passed around via the comm link. The mvor condition
// gets cancelled at 1 controller (irri machine). That request gets passed to
// the foal machine who in turn passes it back to all irri machines (with an
// absence of any further mvor bit set). The causes each irri machine to stop
// the mvor condition.
extern void Alert_MVOR_Requested( const INITIATED_VIA_ENUM pwho_initiated, const UNS_16 pclosed, const UNS_32 pseconds )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	// when closed == 1 that means a close the mvor

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MVOR_REQUESTED, &ldh );

	ucp = ALERTS_load_object( ucp, &pwho_initiated, sizeof(pwho_initiated), &ldh );
	ucp = ALERTS_load_object( ucp, &pclosed, sizeof(pclosed), &ldh );
	ucp = ALERTS_load_object( ucp, &pseconds, sizeof(pseconds), &ldh );

	ALERTS_add_alert( AID_MVOR_REQUESTED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_MVOR_Started( const UNS_16 pclosed, const UNS_32 pseconds )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	// when closed == 1 that means a close the mvor

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MVOR_STARTED, &ldh );

	ucp = ALERTS_load_object( ucp, &pclosed, sizeof(pclosed), &ldh );
	ucp = ALERTS_load_object( ucp, &pseconds, sizeof(pseconds), &ldh );

	ALERTS_add_alert( AID_MVOR_STARTED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_MVOR_Cancelled( const INITIATED_VIA_ENUM pwho_initiated, const UNS_16 pclosed )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	// when closed == 1 that means we are cancelling a closed mvor condition

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MVOR_CANCELLED, &ldh );

	ucp = ALERTS_load_object( ucp, &pwho_initiated, sizeof(pwho_initiated), &ldh );
	ucp = ALERTS_load_object( ucp, &pclosed, sizeof(pclosed), &ldh );

	ALERTS_add_alert( AID_MVOR_CANCELLED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_MVOR_Stopped( const UNS_16 pclosed )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	// when closed == 1 that means we are stopping a closed mvor condition

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MVOR_STOPPED, &ldh );

	ucp = ALERTS_load_object( ucp, &pclosed, sizeof(pclosed), &ldh );

	ALERTS_add_alert( AID_MVOR_STOPPED, ldh );
}
#endif

/* ---------------------------------------------------------- */
extern void Alert_MVOR_started( char *pgroup_name, const UNS_32 pmvor_action_to_take, const UNS_32 mvor_seconds, const INITIATED_VIA_ENUM pinitiated_by )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 64 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MVOR_REQUESTED, &ldh );

	ucp = ALERTS_load_string( ucp, pgroup_name, &ldh );
	ucp = ALERTS_load_object( ucp, &pmvor_action_to_take, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &mvor_seconds, sizeof(UNS_32), &ldh );
	ucp = ALERTS_load_object( ucp, &pinitiated_by, sizeof(pinitiated_by), &ldh );

	ALERTS_add_alert( AID_MVOR_REQUESTED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_MVOR_skipped( char *pgroup_name, const UNS_32 preason_skipped )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 64 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MVOR_SKIPPED, &ldh );

	ucp = ALERTS_load_string( ucp, pgroup_name, &ldh );
	ucp = ALERTS_load_object( ucp, &preason_skipped, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_MVOR_SKIPPED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_entering_forced( void )
{
	ALERTS_build_simple_alert( AID_ENTERED_FORCED );
}

/* ---------------------------------------------------------- */
extern void Alert_leaving_forced( void )
{
	ALERTS_build_simple_alert( AID_LEAVING_FORCED );
}

/* ---------------------------------------------------------- */
extern void Alert_starting_scan_with_reason( const UNS_32 preason )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	// 12/1/2014 ajv : Since standalone and slave controllers start scans - or at least begin
	// the process before falling into else-statements - we only want to generate this alert if
	// we're the master of a flowsense chain.
	if( FLOWSENSE_we_are_the_master_of_a_multiple_controller_network() )
	{
		ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_STARTING_SCAN_WITH_REASON, &ldh );

		ucp = ALERTS_load_object( ucp, &preason, sizeof(UNS_8), &ldh );

		ALERTS_add_alert( AID_STARTING_SCAN_WITH_REASON, ldh );
	}
}

/* ---------------------------------------------------------- */
extern void Alert_reboot_request( const UNS_32 preason )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_REBOOT_REQUEST, &ldh );

	ucp = ALERTS_load_object( ucp, &preason, sizeof(UNS_32), &ldh );

	ALERTS_add_alert( AID_REBOOT_REQUEST, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_remaining_gage_pulses_changed( const UNS_32 pfrom, const UNS_32 pto )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ETGAGE_PERCENT_FULL_EDITED, &ldh );

	ucp = ALERTS_load_object( ucp, &pfrom, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &pto, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_ETGAGE_PERCENT_FULL_EDITED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_ETGage_Pulse( const UNS_32 ppulses )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ETGAGE_PULSE, &ldh );

	ucp = ALERTS_load_object( ucp, &ppulses, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_ETGAGE_PULSE, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_ETGage_RunAway( void )
{
	ALERTS_build_simple_alert( AID_ETGAGE_RUNAWAY );
}

/* ---------------------------------------------------------- */
extern void Alert_ETGage_RunAway_Cleared( void )
{
	ALERTS_build_simple_alert( AID_ETGAGE_CLEARED_RUNAWAY );
}

/* ---------------------------------------------------------- */
extern void Alert_ETGage_First_Zero( void )
{
	ALERTS_build_simple_alert( AID_ETGAGE_FIRST_ZERO );
}

/* ---------------------------------------------------------- */
extern void Alert_ETGage_Second_or_More_Zeros( void )
{
	ALERTS_build_simple_alert( AID_ETGAGE_SECOND_OR_MORE_ZEROS );
}

/* ---------------------------------------------------------- */
extern void Alert_rain_affecting_irrigation( void )
{
	ALERTS_build_simple_alert( AID_RAIN_AFFECTING_IRRIGATION );
}

/* ---------------------------------------------------------- */
extern void Alert_24HourRainTotal( const float prain )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_RAIN_24HOUR_TOTAL, &ldh );

	ucp = ALERTS_load_object( ucp, &prain, sizeof(float), &ldh );

	ALERTS_add_alert( AID_RAIN_24HOUR_TOTAL, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_rain_switch_affecting_irrigation( void )
{
	ALERTS_build_simple_alert( AID_RAIN_SWITCH_AFFECTING_IRRIGATION );
}

/* ---------------------------------------------------------- */
extern void Alert_freeze_switch_affecting_irrigation( void )
{
	ALERTS_build_simple_alert( AID_FREEZE_SWITCH_AFFECTING_IRRIGATION );
}

/* ---------------------------------------------------------- */
extern void Alert_ETGage_percent_full_warning( const UNS_32 ppercent_full )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ETGAGE_PERCENT_FULL_WARNING, &ldh );

	ucp = ALERTS_load_object( ucp, &ppercent_full, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_ETGAGE_PERCENT_FULL_WARNING, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_stop_key_pressed_idx( const UNS_32 psystem_GID, const UNS_32 phighest_reason_in_list, const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ ALERTS_MAX_STORED_BYTE_COUNT ];  // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_STOP_KEY_PRESSED, &ldh );

	ucp = ALERTS_load_object( ucp, &psystem_GID, sizeof(psystem_GID), &ldh );
	ucp = ALERTS_load_object( ucp, &phighest_reason_in_list, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );  // only take first byte

	ALERTS_add_alert( AID_STOP_KEY_PRESSED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_ET_table_loaded( void )
{
	ALERTS_build_simple_alert( AID_ETTABLE_LOADED );
}

/* ---------------------------------------------------------- */
extern void Alert_accum_rain_set_by_station( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, const UNS_32 pvalue_10u, const INITIATED_VIA_ENUM pwho_initiated_it )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ ALERTS_MAX_STORED_BYTE_COUNT ];  // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ACCUM_RAIN_SET_BY_STATION, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pstation_number_0, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &pvalue_10u, sizeof(UNS_32), &ldh );
	ucp = ALERTS_load_object( ucp, &pwho_initiated_it, sizeof(pwho_initiated_it), &ldh );

	ALERTS_add_alert( AID_ACCUM_RAIN_SET_BY_STATION, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_fuse_replaced_idx( const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_FUSE_REPLACED, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof( UNS_8 ), &ldh );

	ALERTS_add_alert( AID_FUSE_REPLACED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_fuse_blown_idx( const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_FUSE_BLOWN, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof( UNS_8 ), &ldh );

	ALERTS_add_alert( AID_FUSE_BLOWN, ldh );
}

#if 0
/* ---------------------------------------------------------- */
extern void Alert_FuseBlown_Reminder( void )
{
	ALERTS_build_simple_alert( AID_FUSE_REMINDER );
}
#endif

/* ---------------------------------------------------------- */
// AID = 250
extern void Alert_scheduled_irrigation_started( const UNS_32 preason_in_list )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SCHEDULED_IRRIGATION_STARTED, &ldh );

	// 10/11/2012 rmd : Notice we write only the first byte from the UNS_32 reason. This is a
	// non-portable little endian only method to keep the alerts pile storage down. And works
	// because the reason never grows beyond 255. Also keeps code space down by working with the
	// reason as an UNS_32 outside of the pile.
	ucp = ALERTS_load_object( ucp, &preason_in_list, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_SCHEDULED_IRRIGATION_STARTED, ldh );
}

/* ---------------------------------------------------------- */
// AID = 251
extern void Alert_some_or_all_skipping_their_start( const UNS_32 preason_in_list, const UNS_32 preason_skipped )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SOME_OR_ALL_STATIONS_SKIPPED_THEIR_START, &ldh );

	// 10/10/2012 rmd : Reasons in list range is only 0 through 15. So UNS_8 is fine.
	ucp = ALERTS_load_object( ucp, &preason_in_list, sizeof(UNS_8), &ldh );

	// 10/10/2012 rmd : Reason skipped is less than 255.  So UNS_8 is fine.
	ucp = ALERTS_load_object( ucp, &preason_skipped, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_SOME_OR_ALL_STATIONS_SKIPPED_THEIR_START, ldh );
}

/* ---------------------------------------------------------- */
// AID = 252
extern void Alert_programmed_irrigation_still_running_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_PROGRAMMED_IRRIGATION_STILL_RUNNING, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pstation_number_0, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_PROGRAMMED_IRRIGATION_STILL_RUNNING, ldh );
}

/* ---------------------------------------------------------- */
// AID = 253
extern void Alert_hit_stop_time( void )
{
	ALERTS_build_simple_alert( AID_HIT_STOPTIME );
}

/* ---------------------------------------------------------- */
// AID = 254
extern void Alert_irrigation_ended( const UNS_32 preason_in_list )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_IRRIGATION_ENDED, &ldh );

	ucp = ALERTS_load_object( ucp, &preason_in_list, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_IRRIGATION_ENDED, ldh );
}

#if 0
/* ---------------------------------------------------------- */
// AID = 255
extern void Alert_Walk_Thru_Start( void )
{
	ALERTS_build_simple_alert( AID_WALK_THRU_STARTED );
}
#endif

/* ---------------------------------------------------------- */
// AID = 256 & 257
extern void Alert_flow_not_checked_with_reason_idx( const UNS_32 paid, const UNS_8 pbox_index_0, const UNS_8 pstation_number_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, paid, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pstation_number_0, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( paid, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_test_station_started_idx( const UNS_32 pbox_index_0, const UNS_32 pstation, const INITIATED_VIA_ENUM pwho_initiated_it )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_TEST_STATION_STARTED_WITH_REASON, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pstation, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &pwho_initiated_it, sizeof(pwho_initiated_it), &ldh );

	ALERTS_add_alert( AID_TEST_STATION_STARTED_WITH_REASON, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_walk_thru_started( char *pgroup_name )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_WALK_THRU_STARTED, &ldh );

	ucp = ALERTS_load_string( ucp, pgroup_name, &ldh );

	ALERTS_add_alert( AID_WALK_THRU_STARTED, ldh );
}

#if 0
/* ---------------------------------------------------------- */
extern void Alert_TestProgramStarted( const UNS_8 pprog, const INITIATED_VIA_ENUM pwho_initiated_it )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_TEST_PROGRAM_STARTED_WITH_REASON, &ldh );

	ucp = ALERTS_load_object( ucp, &pprog, sizeof(pprog), &ldh );
	ucp = ALERTS_load_object( ucp, &pwho_initiated_it, sizeof(pwho_initiated_it), &ldh );

	ALERTS_add_alert( AID_TEST_PROGRAM_STARTED_WITH_REASON, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_TestAllStarted( const INITIATED_VIA_ENUM pwho_initiated_it )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_TEST_ALL_STARTED_WITH_REASON, &ldh );

	ucp = ALERTS_load_object( ucp, &pwho_initiated_it, sizeof(pwho_initiated_it), &ldh );

	ALERTS_add_alert( AID_TEST_ALL_STARTED_WITH_REASON, ldh );
}
#endif

/* ---------------------------------------------------------- */
extern void Alert_manual_water_station_started_idx( const UNS_32 pbox_index_0, const UNS_32 pstation, const INITIATED_VIA_ENUM pwho_initiated_it )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MANUAL_WATER_STATION_STARTED_WITH_REASON, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pstation, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &pwho_initiated_it, sizeof(pwho_initiated_it), &ldh );

	ALERTS_add_alert( AID_MANUAL_WATER_STATION_STARTED_WITH_REASON, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_manual_water_program_started( char *pgroup_name, const INITIATED_VIA_ENUM pwho_initiated_it )
{
	DATA_HANDLE ldh;

	// 8/29/2013 ajv : Since we're passing in a string that's up to 48
	// characters, make sure the buffer is big enough to support it plus the
	// rest of the associated data.
	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MANUAL_WATER_PROGRAM_STARTED_WITH_REASON, &ldh );

	ucp = ALERTS_load_string( ucp, pgroup_name, &ldh );
	ucp = ALERTS_load_object( ucp, &pwho_initiated_it, sizeof(pwho_initiated_it), &ldh );

	ALERTS_add_alert( AID_MANUAL_WATER_PROGRAM_STARTED_WITH_REASON, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_manual_water_all_started( const INITIATED_VIA_ENUM pwho_initiated_it )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MANUAL_WATER_ALL_STARTED_WITH_REASON, &ldh );

	ucp = ALERTS_load_object( ucp, &pwho_initiated_it, sizeof(pwho_initiated_it), &ldh );

	ALERTS_add_alert( AID_MANUAL_WATER_ALL_STARTED_WITH_REASON, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_mobile_station_on( const UNS_32 pbox_index_0, const UNS_32 pstation )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MOBILE_STATION_ON, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ucp = ALERTS_load_object( ucp, &pstation, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_MOBILE_STATION_ON, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_mobile_station_off( const UNS_32 pbox_index_0, const UNS_32 pstation )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MOBILE_STATION_OFF, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ucp = ALERTS_load_object( ucp, &pstation, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_MOBILE_STATION_OFF, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_poc_not_found_extracting_token_resp( const UNS_32 pdecoder_sn, const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_POC_NOT_FOUND_EXTRACTING_TOKEN_RESP, &ldh );

	ucp = ALERTS_load_object( ucp, &pdecoder_sn, sizeof(pdecoder_sn), &ldh );
	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_POC_NOT_FOUND_EXTRACTING_TOKEN_RESP, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_token_resp_extraction_error( void )
{
	ALERTS_build_simple_alert( AID_TOKEN_RESP_EXTRACTION_ERROR );
}

/* ---------------------------------------------------------- */
extern void Alert_token_rcvd_with_no_FL( void )
{
	ALERTS_build_simple_alert( AID_TOKEN_RECEIVED_WITH_NO_FL ); 
}

/* ---------------------------------------------------------- */
extern void Alert_token_rcvd_with_FL_turned_off( void )
{
	ALERTS_build_simple_alert( AID_TOKEN_RECEIVED_WITH_FL_TURNED_OFF ); 
}

/* ---------------------------------------------------------- */
extern void Alert_token_rcvd_by_FL_master( void )
{
	ALERTS_build_simple_alert( AID_TOKEN_RECEIVED_BY_FL_MASTER ); 
}

/* ---------------------------------------------------------- */
extern void Alert_hub_rcvd_packet( const UNS_32 prouter_type, const UNS_32 pfrom_which_port, const UNS_32 prouting_class_base, const UNS_32 pdata_len )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_HUB_RECEIVED_DATA, &ldh );

	ucp = ALERTS_load_object( ucp, &prouter_type, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pfrom_which_port, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &prouting_class_base, sizeof(prouting_class_base), &ldh );
	ucp = ALERTS_load_object( ucp, &pdata_len, sizeof(pdata_len), &ldh ); 

	ALERTS_add_alert( AID_HUB_RECEIVED_DATA, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_hub_forwarding_packet( const UNS_32 prouter_type, const UNS_32 pfrom_which_port, const UNS_32 prouting_class_base, const UNS_32 pdata_len )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_HUB_FORWARDING_DATA, &ldh );

	ucp = ALERTS_load_object( ucp, &prouter_type, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pfrom_which_port, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &prouting_class_base, sizeof(prouting_class_base), &ldh );
	ucp = ALERTS_load_object( ucp, &pdata_len, sizeof(pdata_len), &ldh ); 

	ALERTS_add_alert( AID_HUB_FORWARDING_DATA, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_router_rcvd_unexp_class( const UNS_32 prouter_type, const UNS_32 pfrom_which_port, const UNS_32 prouting_class_base, const UNS_32 prouting_class )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ROUTER_RECEIVED_UNEXP_CLASS, &ldh );

	ucp = ALERTS_load_object( ucp, &prouter_type, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pfrom_which_port, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &prouting_class_base, sizeof(prouting_class_base), &ldh );
	ucp = ALERTS_load_object( ucp, &prouting_class, sizeof(prouting_class), &ldh );

	ALERTS_add_alert( AID_ROUTER_RECEIVED_UNEXP_CLASS, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_router_rcvd_unexp_base_class( const UNS_32 prouter_type, const UNS_32 pfrom_which_port, const UNS_32 prouting_class_base )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ROUTER_RECEIVED_UNEXP_BASE_CLASS, &ldh );

	ucp = ALERTS_load_object( ucp, &prouter_type, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pfrom_which_port, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &prouting_class_base, sizeof(prouting_class_base), &ldh );

	ALERTS_add_alert( AID_ROUTER_RECEIVED_UNEXP_BASE_CLASS, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_router_rcvd_packet_not_on_hub_list( const UNS_32 prouter_type, const UNS_32 pfrom_which_port, const UNS_32 prouting_class_base )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ROUTER_RECEIVED_PACKET_NOT_ON_HUB_LIST, &ldh );

	ucp = ALERTS_load_object( ucp, &prouter_type, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pfrom_which_port, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &prouting_class_base, sizeof(prouting_class_base), &ldh );

	ALERTS_add_alert( AID_ROUTER_RECEIVED_PACKET_NOT_ON_HUB_LIST, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_router_rcvd_packet_not_on_hub_list_with_sn( const UNS_32 prouter_type, const UNS_32 pfrom_which_port, const UNS_32 prouting_class_base, const UNS_32 pserial_num )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ROUTER_RECEIVED_PACKET_NOT_ON_HUB_LIST_WITH_SN, &ldh );

	ucp = ALERTS_load_object( ucp, &prouter_type, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pfrom_which_port, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &prouting_class_base, sizeof(prouting_class_base), &ldh );
	ucp = ALERTS_load_object( ucp, &pserial_num, sizeof(pserial_num), &ldh );

	ALERTS_add_alert( AID_ROUTER_RECEIVED_PACKET_NOT_ON_HUB_LIST_WITH_SN, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_router_rcvd_packet_not_for_us( const UNS_32 prouter_type, const UNS_32 pfrom_which_port, const UNS_32 prouting_class_base )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ROUTER_RECEIVED_PACKET_NOT_FOR_US, &ldh );

	ucp = ALERTS_load_object( ucp, &prouter_type, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pfrom_which_port, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &prouting_class_base, sizeof(prouting_class_base), &ldh );

	ALERTS_add_alert( AID_ROUTER_RECEIVED_PACKET_NOT_FOR_US, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_router_unexp_to_addr_port( const UNS_32 prouter_type, const UNS_32 pfrom_which_port )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char *)&lalert, AID_ROUTER_UNEXP_TO_ADDR, &ldh ); 

	ucp = ALERTS_load_object( ucp, &prouter_type, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pfrom_which_port, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_ROUTER_UNEXP_TO_ADDR, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_router_unk_port( const UNS_32 prouter_type )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ROUTER_UNK_PORT, &ldh );

	ucp = ALERTS_load_object( ucp, &prouter_type, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_ROUTER_UNK_PORT, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_router_received_unexp_token_resp( const UNS_32 prouter_type, const UNS_32 pfrom_which_port )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char *)&lalert, AID_ROUTER_RECEIVED_UNEXP_TOKEN_RESP, &ldh ); 

	ucp = ALERTS_load_object( ucp, &prouter_type, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pfrom_which_port, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_ROUTER_RECEIVED_UNEXP_TOKEN_RESP, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_ci_queued_msg( const UNS_32 psend_to_front, const UNS_32 pevent )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char *)&lalert, AID_CI_QUEUED_MSG, &ldh ); 

	ucp = ALERTS_load_object( ucp, &psend_to_front, sizeof(UNS_32), &ldh );
	ucp = ALERTS_load_object( ucp, &pevent, sizeof(UNS_32), &ldh );

	ALERTS_add_alert( AID_CI_QUEUED_MSG, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_ci_waiting_for_device( void )
{
	ALERTS_build_simple_alert( AID_CI_WAITING_FOR_DEVICE );
}

/* ---------------------------------------------------------- */
extern void Alert_ci_starting_connection_process( void )
{
	ALERTS_build_simple_alert( AID_CI_STARTING_CONNECTION_PROCESS );
}

/* ---------------------------------------------------------- */
extern void Alert_cellular_socket_attempt( void )
{
	ALERTS_build_simple_alert( AID_CELLULAR_SOCKET_ATTEMPT );
}

/* ---------------------------------------------------------- */
extern void Alert_cellular_data_connection_attempt( void )
{
	ALERTS_build_simple_alert( AID_CELLULAR_DATA_CONNECTION_ATTEMPT );
}

/* ---------------------------------------------------------- */
extern void Alert_msg_transaction_time( const UNS_32 ptransact_time )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MSG_TRANSACTION_TIME, &ldh );

	ucp = ALERTS_load_object( ucp, &ptransact_time, sizeof(ptransact_time), &ldh );

	ALERTS_add_alert( AID_MSG_TRANSACTION_TIME, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_largest_token_size( const UNS_32 ptoken_size, const BOOL_32 pis_resp )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_LARGEST_TOKEN_SIZE, &ldh );

	ucp = ALERTS_load_object( ucp, &ptoken_size, sizeof(ptoken_size), &ldh );
	ucp = ALERTS_load_object( ucp, &pis_resp, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_LARGEST_TOKEN_SIZE, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_budget_under_budget( char* ppoc_name, UNS_32 percent )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_BUDGET_UNDER_BUDGET, &ldh );

	ucp = ALERTS_load_string( ucp, ppoc_name, &ldh );
	ucp = ALERTS_load_object( ucp, &percent, sizeof(percent), &ldh );

	ALERTS_add_alert( AID_BUDGET_UNDER_BUDGET, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_budget_over_budget( char* ppoc_name, UNS_32 percent, UNS_32 percent2 )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_BUDGET_OVER_BUDGET, &ldh );

	ucp = ALERTS_load_string( ucp, ppoc_name, &ldh );
	ucp = ALERTS_load_object( ucp, &percent, sizeof(percent), &ldh );
	ucp = ALERTS_load_object( ucp, &percent2, sizeof(percent2), &ldh );

	ALERTS_add_alert( AID_BUDGET_OVER_BUDGET, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_budget_will_exceed_budget( char* ppoc_name, UNS_32 percent )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_BUDGET_WILL_EXCEED_BUDGET, &ldh );

	ucp = ALERTS_load_string( ucp, ppoc_name, &ldh );
	ucp = ALERTS_load_object( ucp, &percent, sizeof(percent), &ldh );

	ALERTS_add_alert( AID_BUDGET_WILL_EXCEED_BUDGET, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_budget_values_not_set( char* psys_name, char* ppoc_name )
{
	DATA_HANDLE ldh;

	// 4/13/2016 skc : Each string can be up to 48 chars, make sure the buffer is big enough to support it plus the
	// rest of the associated data.
	unsigned char lalert[ 256 ];

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_BUDGET_NO_BUDGET_VALUES, &ldh );

	ucp = ALERTS_load_string( ucp, psys_name, &ldh );
	ucp = ALERTS_load_string( ucp, ppoc_name, &ldh );

	ALERTS_add_alert( AID_BUDGET_NO_BUDGET_VALUES, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_budget_group_reduction( char* pgroup_name, UNS_32 reduction, BOOL_32 limit_hit )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 256 ];

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_BUDGET_GROUP_REDUCTION, &ldh );

	ucp = ALERTS_load_string( ucp, pgroup_name, &ldh );
	ucp = ALERTS_load_object( ucp, &reduction, sizeof(reduction), &ldh );
	ucp = ALERTS_load_object( ucp, &limit_hit, sizeof(limit_hit), &ldh );

	ALERTS_add_alert( AID_BUDGET_GROUP_REDUCTION, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_budget_period_ended( char* poc_name, UNS_32 budget, UNS_32 used )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 256 ];

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_BUDGET_PERIOD_ENDED, &ldh );

	ucp = ALERTS_load_string( ucp, poc_name, &ldh );
	ucp = ALERTS_load_object( ucp, &budget, sizeof(budget), &ldh );
	ucp = ALERTS_load_object( ucp, &used, sizeof(used), &ldh );

	ALERTS_add_alert( AID_BUDGET_PERIOD_ENDED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_budget_over_budget_period_ending_today( char *ppoc_name, const UNS_32 ppercent_over_budget )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_BUDGET_OVER_BUDGET_PERIOD_ENDING_TODAY, &ldh );

	ucp = ALERTS_load_string( ucp, ppoc_name, &ldh );
	ucp = ALERTS_load_object( ucp, &ppercent_over_budget, sizeof(ppercent_over_budget), &ldh );

	ALERTS_add_alert( AID_BUDGET_OVER_BUDGET_PERIOD_ENDING_TODAY, ldh );
}

/* ---------------------------------------------------------- */
#if 0
extern void Alert_DirectAccess_entered( void )
{
	ALERTS_build_simple_alert( AID_DIRECT_ACCESS_ENTERED );
}

/* ---------------------------------------------------------- */
extern void Alert_DirectAccess_exited( void )
{
	ALERTS_build_simple_alert( AID_DIRECT_ACCESS_EXITED );
}

/* ---------------------------------------------------------- */
extern void Alert_DirectAccess_timedout( void )
{
	ALERTS_build_simple_alert( AID_DIRECT_ACCESS_TIMEDOUT );
}
#endif

#if 0
/* ---------------------------------------------------------- */
extern void Alert_MVOR_open_request_ignored( void )
{
	ALERTS_build_simple_alert( AID_MVOR_OPEN_REQUEST_IGNORED );
}
#endif

#if 0
/* ---------------------------------------------------------- */
extern void Alert_CTS_timeout( const UNS_16 pmid_for_port )
{
	ALERTS_build_simple_alert( pmid_for_port );
}
#endif

/* ---------------------------------------------------------- */
extern void Alert_ET_Table_substitution_100u( char *pstr, const UNS_32 pdate, const UNS_32 poriginal_table_value_100u, const UNS_32 pnew_value_100u )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;


	// using the new value instead of the original table value
	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ET_TABLE_SUBSTITUTION, &ldh );

	ucp = ALERTS_load_string( ucp, pstr, &ldh );
	ucp = ALERTS_load_object( ucp, &pdate, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &poriginal_table_value_100u, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &pnew_value_100u, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_ET_TABLE_SUBSTITUTION, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_ET_Table_limited_entry_100u( char *pstr, const UNS_32 pdate, const UNS_32 poriginal_table_value_100u, const UNS_32 pnew_value_100u )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;


	// limiting the original value to the new value
	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_ET_TABLE_LIMITED, &ldh );

	ucp = ALERTS_load_string( ucp, pstr, &ldh );
	ucp = ALERTS_load_object( ucp, &pdate, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &poriginal_table_value_100u, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &pnew_value_100u, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_ET_TABLE_LIMITED, ldh );
}

/* ---------------------------------------------------------- */
// AID = 560
extern void Alert_light_ID_with_text( const UNS_32 pbox_index_0, const UNS_32 poutput_index, const UNS_32 text_index )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_LIGHT_ID_WITH_TEXT, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ucp = ALERTS_load_object( ucp, &poutput_index, sizeof(UNS_8), &ldh );

	ucp = ALERTS_load_object( ucp, &text_index, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_LIGHT_ID_WITH_TEXT, ldh );
}

#if 0
/* ---------------------------------------------------------- */
// AID = 560
extern void Alert_manual_light_on( const UNS_32 pbox_index_0, const UNS_32 poutput_index )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MANUAL_LIGHT_ON, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ucp = ALERTS_load_object( ucp, &poutput_index, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_MANUAL_LIGHT_ON, ldh );
}

/* ---------------------------------------------------------- */
// AID = 561
extern void Alert_manual_light_off( const UNS_32 pbox_index_0, const UNS_32 poutput_index )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MANUAL_LIGHT_OFF, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ucp = ALERTS_load_object( ucp, &poutput_index, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_MANUAL_LIGHT_OFF, ldh );
}

/* ---------------------------------------------------------- */
// AID = 562
extern void Alert_mobile_light_on( const UNS_32 pbox_index_0, const UNS_32 poutput_index )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MOBILE_LIGHT_ON, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ucp = ALERTS_load_object( ucp, &poutput_index, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_MOBILE_LIGHT_ON, ldh );
}

/* ---------------------------------------------------------- */
// AID = 563
extern void Alert_mobile_light_off( const UNS_32 pbox_index_0, const UNS_32 poutput_index )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MOBILE_LIGHT_OFF, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ucp = ALERTS_load_object( ucp, &poutput_index, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_MOBILE_LIGHT_OFF, ldh );
}

/* ---------------------------------------------------------- */
// AID = 564
extern void Alert_test_light_started_idx( const UNS_32 pbox_index_0, const UNS_32 poutput_index, const INITIATED_VIA_ENUM pwho_initiated_it )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_TEST_LIGHT_STARTED_WITH_REASON, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &poutput_index, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &pwho_initiated_it, sizeof(pwho_initiated_it), &ldh );

	ALERTS_add_alert( AID_TEST_LIGHT_STARTED_WITH_REASON, ldh );
}

/* ---------------------------------------------------------- */
// AID = 565
extern void Alert_scheduled_lighting_started( const UNS_32 pbox_index_0, const UNS_32 poutput_index )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SCHEDULED_LIGHTING_STARTED, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ucp = ALERTS_load_object( ucp, &poutput_index, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_SCHEDULED_LIGHTING_STARTED, ldh );
}

/* ---------------------------------------------------------- */
// AID = 566
// 9/9/2015 mpd : TODO: This is never being called!!!
extern void Alert_lighting_ended( const UNS_32 preason_in_list )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SCHEDULED_LIGHTING_ENDED, &ldh );

	ucp = ALERTS_load_object( ucp, &preason_in_list, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_SCHEDULED_LIGHTING_ENDED, ldh );
}

/* ---------------------------------------------------------- */
// AID = 567
extern void Alert_light_hit_stop_time( const UNS_32 pbox_index_0, const UNS_32 poutput_index  )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SCHEDULED_LIGHTING_HIT_STOP, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ucp = ALERTS_load_object( ucp, &poutput_index, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_SCHEDULED_LIGHTING_HIT_STOP, ldh );
}

/* ---------------------------------------------------------- */
// AID = 568
extern void Alert_light_skipped_start( const UNS_32 pbox_index_0, const UNS_32 poutput_index,  const UNS_32 preason_in_list, const UNS_32 preason_skipped )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;

	// 9/9/2015 mpd : TODO: This needs the light box and index.

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SCHEDULED_LIGHTING_SKIPPED_START, &ldh );

	// 10/10/2012 rmd : Reasons in list range is only 0 through 15. So UNS_8 is fine.
	ucp = ALERTS_load_object( ucp, &preason_in_list, sizeof(UNS_8), &ldh );

	// 10/10/2012 rmd : Reason skipped is less than 255.  So UNS_8 is fine.
	ucp = ALERTS_load_object( ucp, &preason_skipped, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_SCHEDULED_LIGHTING_SKIPPED_START, ldh );
}
#endif

/* ---------------------------------------------------------- */
extern void Alert_comm_command_received( const COMM_COMMANDS pMID )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_COMM_COMMAND_RCVD, &ldh );

	ucp = ALERTS_load_object( ucp, &pMID, sizeof(COMM_COMMANDS), &ldh );

	ALERTS_add_alert( AID_COMM_COMMAND_RCVD, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_comm_command_sent( const COMM_COMMANDS pMID )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_COMM_COMMAND_INIT_ISSUED, &ldh );

	ucp = ALERTS_load_object( ucp, &pMID, sizeof(COMM_COMMANDS), &ldh );

	ALERTS_add_alert( AID_COMM_COMMAND_INIT_ISSUED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_comm_command_failure( const UNS_32 pci_event )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_COMM_COMMAND_FAILED, &ldh );

	ucp = ALERTS_load_object( ucp, &pci_event, sizeof(UNS_32), &ldh );

	ALERTS_add_alert( AID_COMM_COMMAND_FAILED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_outbound_message_size( const UNS_16 pmid, const UNS_32 psize, const UNS_16 ppackets )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_OUTBOUND_MESSAGE_SIZE, &ldh );

	ucp = ALERTS_load_object( ucp, &pmid, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &psize, sizeof(UNS_32), &ldh );
	ucp = ALERTS_load_object( ucp, &ppackets, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_OUTBOUND_MESSAGE_SIZE, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_inbound_message_size( const UNS_16 pmid, const UNS_32 psize, const UNS_16 ppackets )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_INBOUND_MESSAGE_SIZE, &ldh );

	ucp = ALERTS_load_object( ucp, &pmid, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &psize, sizeof(UNS_32), &ldh );
	ucp = ALERTS_load_object( ucp, &ppackets, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_INBOUND_MESSAGE_SIZE, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_new_connection_detected( void )
{
	ALERTS_build_simple_alert( AID_NEW_CONNECTION_DETECTED );
}

/* ---------------------------------------------------------- */
extern void Alert_device_powered_on_or_off( const UNS_32 pport, const BOOL_32 pon_or_off, char *pdevice_name )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_DEVICE_POWERED_ON_OR_OFF, &ldh );

	ucp = ALERTS_load_object( ucp, &pport, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pon_or_off, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_string( ucp, pdevice_name, &ldh );

	ALERTS_add_alert( AID_DEVICE_POWERED_ON_OR_OFF, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_divider_large( void )
{
	ALERTS_build_simple_alert( AID_ALERT_DIVIDER_LARGE );
}

/* ---------------------------------------------------------- */
extern void Alert_divider_small( void )
{
	ALERTS_build_simple_alert( AID_ALERT_DIVIDER_SMALL );
}

#if 0
/* ---------------------------------------------------------- */
extern void Alert_Lights_Activity( const UNS_32 paid, const UNS_16 pstation, const UNS_16 preason )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, paid, &ldh );

	ucp = ALERTS_load_object( ucp, &pstation, sizeof(pstation), &ldh );
	ucp = ALERTS_load_object( ucp, &preason, sizeof(preason), &ldh );

	ALERTS_add_alert( paid, ldh );
}
#endif

#if 0
/* ---------------------------------------------------------- */
extern void Alert_wind_pause( const UNS_16 ppaused_speed )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_WIND_PAUSE, &ldh );

	ucp = ALERTS_load_object( ucp, &ppaused_speed, sizeof(ppaused_speed), &ldh );

	ALERTS_add_alert( AID_WIND_PAUSE, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_wind_resume( const UNS_16 presumed_speed )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_WIND_RESUME, &ldh );

	ucp = ALERTS_load_object( ucp, &presumed_speed, sizeof(presumed_speed), &ldh );

	ALERTS_add_alert( AID_WIND_RESUME, ldh );
}
#endif

#if 0
/* ---------------------------------------------------------- */
extern void Alert_irrigation_paused( const UNS_16 pwhy )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_IRRIGATION_PAUSED, &ldh );

	ucp = ALERTS_load_object( ucp, &pwhy, sizeof(pwhy), &ldh );

	ALERTS_add_alert( AID_IRRIGATION_PAUSED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_irrigation_resumed( const UNS_16 pwhy )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_IRRIGATION_RESUMED, &ldh );

	ucp = ALERTS_load_object( ucp, &pwhy, sizeof(pwhy), &ldh );

	ALERTS_add_alert( AID_IRRIGATION_RESUMED, ldh );
}
#endif

#if 0
/* ---------------------------------------------------------- */
extern void Alert_POC_MainlineBreak( const UNS_16 pPOC_index, const UNS_16 pmeasured, const UNS_16 pallowed )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_POC_MLB_BREAK, &ldh );

	ucp = ALERTS_load_object( ucp, &pPOC_index, sizeof(pPOC_index), &ldh );
	ucp = ALERTS_load_object( ucp, &pmeasured, sizeof(pmeasured), &ldh );
	ucp = ALERTS_load_object( ucp, &pallowed, sizeof(pallowed), &ldh );

	ALERTS_add_alert( AID_POC_MLB_BREAK, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_POC_MainlineBreak_reminder( const UNS_16 pPOC_index )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_POC_MLB_REMINDER, &ldh );

	ucp = ALERTS_load_object( ucp, &pPOC_index, sizeof(pPOC_index), &ldh );

	ALERTS_add_alert( AID_POC_MLB_REMINDER, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_POC_MainlineBreak_cleared( const UNS_16 pPOC_index, const CLEARED_MLB_HOW pcleared_how )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	UNS_8   *ucp;

	UNS_32  laid;

	if( pcleared_how == CLEAR_MLB_FROM_KEYPAD )
	{
		laid = AID_POC_MLB_CLEARED_KEYPAD;
	}
	else if( pcleared_how == CLEAR_MLB_FROM_COMMLINK )
	{
		laid = AID_POC_MLB_CLEARED_COMMLINK;
	}
	else if( pcleared_how == CLEAR_MLB_SHORT )
	{
		laid = AID_POC_MLB_CLEARED_SHORT;
	}
	else
	{
		laid = 0;
	}

	if( laid != 0 )
	{
		ucp = ALERTS_load_common( (UNS_8*)&lalert, laid, &ldh );

		ucp = ALERTS_load_object( ucp, &pPOC_index, sizeof(pPOC_index), &ldh );

		ALERTS_add_alert( laid, ldh );
	}
}

/* ---------------------------------------------------------- */
extern void Alert_POC_MV_Short( const UNS_16 pPOC_index, const UNS_8 paddr )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_POC_MV_SHORT, &ldh );

	ucp = ALERTS_load_object( ucp, &pPOC_index, sizeof(pPOC_index), &ldh );
	ucp = ALERTS_load_object( ucp, &paddr, sizeof(paddr), &ldh );

	ALERTS_add_alert( AID_POC_MV_SHORT, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_POC_MV_Short_During_MLB( const UNS_16 pPOC_index, const UNS_16 phow_many_repeats )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_POC_MV_SHORT_DURING_MLB, &ldh );

	ucp = ALERTS_load_object( ucp, &pPOC_index, sizeof(pPOC_index), &ldh );
	ucp = ALERTS_load_object( ucp, &phow_many_repeats, sizeof(phow_many_repeats), &ldh );

	ALERTS_add_alert( AID_POC_MV_SHORT_DURING_MLB, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_POC_Started_Use( const UNS_16 pPOC_index, const UNS_32 pseconds )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_POC_STARTED_USE, &ldh );

	ucp = ALERTS_load_object( ucp, &pPOC_index, sizeof(pPOC_index), &ldh );
	ucp = ALERTS_load_object( ucp, &pseconds, sizeof(pseconds), &ldh );

	ALERTS_add_alert( AID_POC_STARTED_USE, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_POC_Hit_EndTime( const UNS_16 pPOC_index )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_POC_HIT_ENDTIME, &ldh );

	ucp = ALERTS_load_object( ucp, &pPOC_index, sizeof(pPOC_index), &ldh );

	ALERTS_add_alert( AID_POC_HIT_ENDTIME, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_POC_Stopped_Use( const UNS_16 pPOC_index )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_POC_STOPPED_USE, &ldh );

	ucp = ALERTS_load_object( ucp, &pPOC_index, sizeof(pPOC_index), &ldh );

	ALERTS_add_alert( AID_POC_STOPPED_USE, ldh );
}
#endif

#if 0
/* ---------------------------------------------------------- */
extern void Alert_UserDefined_Program_Tag_Changed( const UNS_16 pprog, char *pfrom_str, char *pto_str )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 192 ];  // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_USERDEFINED_PROGRAMTAG_CHANGED, &ldh );

	ucp = ALERTS_load_object( ucp, &pprog, sizeof(pprog), &ldh );
	ucp = ALERTS_load_string( ucp, pfrom_str, &ldh );
	ucp = ALERTS_load_string( ucp, pto_str, &ldh );

	ALERTS_add_alert( AID_USERDEFINED_PROGRAMTAG_CHANGED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_Program_Tag_Changed( const UNS_16 pprog, char *pfrom_str, char *pto_str )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 192 ];  // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_PROGRAMTAG_CHANGED, &ldh );

	ucp = ALERTS_load_object( ucp, &pprog, sizeof(pprog), &ldh );
	ucp = ALERTS_load_string( ucp, pfrom_str, &ldh );
	ucp = ALERTS_load_string( ucp, pto_str, &ldh );

	ALERTS_add_alert( AID_PROGRAMTAG_CHANGED, ldh );
}
#endif

#if 0
/* ---------------------------------------------------------- */
extern void Alert_PercentAdjust_AllProgs( const UNS_16 ppercentage, const UNS_16 pdays )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_PERCENTADJUST_ALL_PROGS, &ldh );

	ucp = ALERTS_load_object( ucp, &ppercentage, sizeof(ppercentage), &ldh );
	ucp = ALERTS_load_object( ucp, &pdays, sizeof(pdays), &ldh );

	ALERTS_add_alert( AID_PERCENTADJUST_ALL_PROGS, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_PercentAdjust_ByProg( const UNS_16 pprog, const UNS_16 ppercentage, const UNS_16 pdays )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 32 ];	 // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_PERCENTADJUST_BY_PROG, &ldh );

	ucp = ALERTS_load_object( ucp, &pprog, sizeof(pprog), &ldh );
	ucp = ALERTS_load_object( ucp, &ppercentage, sizeof(ppercentage), &ldh );
	ucp = ALERTS_load_object( ucp, &pdays, sizeof(pdays), &ldh );

	ALERTS_add_alert( AID_PERCENTADJUST_BY_PROG, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_SIC_State_Changed( const UNS_8 paddr, const UNS_8 pfunc_or_action, const UNS_8 pstate, char *pname )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SIC_STATE_CHANGED, &ldh );

	ucp = ALERTS_load_object( ucp, &paddr, sizeof(paddr), &ldh );
	ucp = ALERTS_load_object( ucp, &pfunc_or_action, sizeof(pfunc_or_action), &ldh );
	ucp = ALERTS_load_object( ucp, &pstate, sizeof(pstate), &ldh );
	ucp = ALERTS_load_string( ucp, pname, &ldh );

	ALERTS_add_alert( AID_SIC_STATE_CHANGED, ldh );
}
#endif

/* ---------------------------------------------------------- */
extern void Alert_firmware_update_idx( char *pfrom, char *pto, const UNS_32 pbox_index_0 )
{
	DATA_HANDLE	ldh;

	UNS_8 lalert[ 128 ];

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_FIRMWARE_UPDATE, &ldh );

	ucp = ALERTS_load_string( ucp, pfrom, &ldh );
	ucp = ALERTS_load_string( ucp, pto, &ldh );
	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );  // only take the first byte

	ALERTS_add_alert( AID_FIRMWARE_UPDATE, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_main_code_needs_updating_at_slave_idx( const UNS_32 pbox_index_0 )
{
	DATA_HANDLE	ldh;

	UNS_8 lalert[ 128 ];

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MAIN_CODE_NEEDS_UPDATING_AT_SLAVE, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );  // only take the first byte

	ALERTS_add_alert( AID_MAIN_CODE_NEEDS_UPDATING_AT_SLAVE, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_tpmicro_code_needs_updating_at_slave_idx( const UNS_32 pbox_index_0 )
{
	DATA_HANDLE	ldh;

	UNS_8 lalert[ 128 ];

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_TPMICRO_CODE_NEEDS_UPDATING_AT_SLAVE, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );  // only take the first byte

	ALERTS_add_alert( AID_TPMICRO_CODE_NEEDS_UPDATING_AT_SLAVE, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_watchdog_timeout( void )
{
	ALERTS_build_simple_alert( AID_WDT );
}

/* ---------------------------------------------------------- */
extern void Alert_task_frozen( char *ptaskname, const UNS_32 pseconds )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_TASK_FROZEN, &ldh );

	ucp = ALERTS_load_string( ucp, ptaskname, &ldh );
	ucp = ALERTS_load_object( ucp, &pseconds, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_TASK_FROZEN, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_flash_file_write_postponed( char *pfilename )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_FLASH_FILE_WRITE_POSTPONED, &ldh );

	ucp = ALERTS_load_string( ucp, pfilename, &ldh );

	ALERTS_add_alert( AID_FLASH_FILE_WRITE_POSTPONED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_group_not_watersense_compliant( char *pgroup_name )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_GROUP_NOT_WATERSENSE_COMPLIANT, &ldh );

	ucp = ALERTS_load_string( ucp, pgroup_name, &ldh );

	ALERTS_add_alert( AID_GROUP_NOT_WATERSENSE_COMPLIANT, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_system_preserves_activity( const UNS_32 pactivity )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SYSTEM_PRESERVES_ACTIVITY, &ldh );
	ucp = ALERTS_load_object( ucp, &pactivity, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_SYSTEM_PRESERVES_ACTIVITY, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_poc_preserves_activity( const UNS_32 pactivity )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_POC_PRESERVES_ACTIVITY, &ldh );
	ucp = ALERTS_load_object( ucp, &pactivity, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_POC_PRESERVES_ACTIVITY, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_packet_greater_than_512( const UNS_32 pport )
{
	DATA_HANDLE ldh;

	unsigned char lalert[ 128 ];  // build the alert on the stack in this array

	unsigned char *ucp;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_COMM_PACKET_GREATER_THAN_512, &ldh );

	ucp = ALERTS_load_object( ucp, &pport, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_COMM_PACKET_GREATER_THAN_512, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_derate_table_flow_too_high( void )
{
	ALERTS_build_simple_alert( AID_FLOW_NOT_CHECKED_FLOW_TOO_HIGH );
}

/* ---------------------------------------------------------- */
extern void Alert_derate_table_too_many_stations( void )
{
	ALERTS_build_simple_alert( AID_FLOW_NOT_CHECKED_TOO_MANY_STATIONS );
}

/* ---------------------------------------------------------- */
extern void Alert_chain_is_the_same_idx( const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_CHAIN_IS_SAME, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );  // only take the first byte

	ALERTS_add_alert( AID_CHAIN_IS_SAME, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_chain_has_changed_idx( const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_CHAIN_HAS_CHANGED, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );  // only take the first byte

	ALERTS_add_alert( AID_CHAIN_HAS_CHANGED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_et_gage_pulse_but_no_gage_in_use( void )
{
	ALERTS_build_simple_alert( AID_ETGAGE_PULSE_BUT_NO_GAGE_IN_USE );
}

/* ---------------------------------------------------------- */
extern void Alert_rain_pulse_but_no_bucket_in_use( void )
{
	ALERTS_build_simple_alert( AID_RAIN_PULSE_BUT_NO_BUCKET_IN_USE );
}

/* ---------------------------------------------------------- */
extern void Alert_wind_detected_but_no_gage_in_use( void )
{
	ALERTS_build_simple_alert( AID_WIND_DETECTED_BUT_NO_GAGE_IN_USE );
}

/* ---------------------------------------------------------- */
extern void Alert_wind_paused( const UNS_32 pwind_speed_mph )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_WIND_PAUSED, &ldh );

	ucp = ALERTS_load_object( ucp, &pwind_speed_mph, sizeof(UNS_32), &ldh );

	ALERTS_add_alert( AID_WIND_PAUSED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_wind_resumed( const UNS_32 pwind_speed_mph )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_WIND_RESUMED, &ldh );

	ucp = ALERTS_load_object( ucp, &pwind_speed_mph, sizeof(UNS_32), &ldh );

	ALERTS_add_alert( AID_WIND_RESUMED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_station_added_to_group_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, char *pgroupname, const UNS_32 preason )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_STATION_ADDED_TO_GROUP, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pstation_number_0, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_string( ucp, pgroupname, &ldh );
	ucp = ALERTS_load_object( ucp, &preason, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_STATION_ADDED_TO_GROUP, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_station_removed_from_group_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, char *pgroupname, const UNS_32 preason )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_STATION_REMOVED_FROM_GROUP, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pstation_number_0, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_string( ucp, pgroupname, &ldh );
	ucp = ALERTS_load_object( ucp, &preason, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_STATION_REMOVED_FROM_GROUP, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_station_copied_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, char *pgroupname, const UNS_32 preason )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_STATION_COPIED, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pstation_number_0, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_string( ucp, pgroupname, &ldh );
	ucp = ALERTS_load_object( ucp, &preason, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_STATION_COPIED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_station_moved_from_one_group_to_another_idx( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, char *pfrom_group, char *pto_group, const UNS_32 preason )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_STATION_MOVED_TO_ANOTHER_GROUP, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pstation_number_0, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_string( ucp, pfrom_group, &ldh );
	ucp = ALERTS_load_string( ucp, pto_group, &ldh );
	ucp = ALERTS_load_object( ucp, &preason, sizeof(UNS_16), &ldh );

	ALERTS_add_alert( AID_STATION_MOVED_TO_ANOTHER_GROUP, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_status_timer_expired_idx( const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_COMM_STATUS_TIMER_EXPIRED, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );  // only take the first byte
	
	ALERTS_add_alert( AID_COMM_STATUS_TIMER_EXPIRED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_msg_response_timeout_idx( const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MSG_RESPONSE_TIMEOUT, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );  // only take the first byte
	
	ALERTS_add_alert( AID_MSG_RESPONSE_TIMEOUT, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_comm_mngr_blocked_msg_during_idle( const UNS_32 pcomm_mngr_mode, const BOOL_32 ppending_device_exchange_request, const BOOL_32 ppending_scan, const BOOL_32 ppending_code_receipt )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_COMM_MNGR_BLOCKED_MSG_DURING_IDLE, &ldh );

	ucp = ALERTS_load_object( ucp, &pcomm_mngr_mode, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &ppending_device_exchange_request, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &ppending_scan, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &ppending_code_receipt, sizeof(UNS_8), &ldh );
	
	ALERTS_add_alert( AID_COMM_MNGR_BLOCKED_MSG_DURING_IDLE, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_lr_radio_not_compatible_with_hub_opt( void )
{
	ALERTS_build_simple_alert( AID_LR_RADIO_NOT_COMPAT_WITH_HUB_OPT );
}

/* ---------------------------------------------------------- */
extern void Alert_lr_radio_not_compatible_with_hub_opt_reminder( void )
{
	ALERTS_build_simple_alert( AID_LR_RADIO_NOT_COMPAT_WITH_HUB_OPT_REMINDER );
}

/* ---------------------------------------------------------- */
extern void Alert_rain_switch_active( void )
{
	ALERTS_build_simple_alert( AID_RAIN_SWITCH_ACTIVE );
}

/* ---------------------------------------------------------- */
extern void Alert_rain_switch_inactive( void )
{
	ALERTS_build_simple_alert( AID_RAIN_SWITCH_INACTIVE );
}

/* ---------------------------------------------------------- */
extern void Alert_freeze_switch_active( void )
{
	ALERTS_build_simple_alert( AID_FREEZE_SWITCH_ACTIVE );
}

/* ---------------------------------------------------------- */
extern void Alert_freeze_switch_inactive( void )
{
	ALERTS_build_simple_alert( AID_FREEZE_SWITCH_INACTIVE );
}

/* ---------------------------------------------------------- */
extern void Alert_moisture_reading_obtained( const float pmoisture, const INT_32 ptemperature, const float pconductivity )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MOISTURE_READING_OBTAINED, &ldh );

	ucp = ALERTS_load_object( ucp, &pmoisture, sizeof(float), &ldh );
	ucp = ALERTS_load_object( ucp, &ptemperature, sizeof(INT_32), &ldh );
	ucp = ALERTS_load_object( ucp, &pconductivity, sizeof(float), &ldh );

	ALERTS_add_alert( AID_MOISTURE_READING_OBTAINED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_moisture_reading_out_of_range( const UNS_32 psensor_model, const UNS_32 pmoisture, const INT_32 ptemperature, const UNS_32 pconductivity )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_MOISTURE_READING_OUT_OF_RANGE, &ldh );

	ucp = ALERTS_load_object( ucp, &psensor_model, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pmoisture, sizeof(UNS_32), &ldh );
	ucp = ALERTS_load_object( ucp, &ptemperature, sizeof(INT_32), &ldh );
	ucp = ALERTS_load_object( ucp, &pconductivity, sizeof(UNS_32), &ldh );

	ALERTS_add_alert( AID_MOISTURE_READING_OUT_OF_RANGE, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_soil_moisture_crossed_threshold( const UNS_32 pdecoder_serial_number, const float pmoisture, const float plower_threshold, const float pupper_threshold, const MOISTURE_SENSOR_ALERT_TYPE pmois_alert_type )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SOIL_MOISTURE_CROSSED_THRESHOLD, &ldh );

	ucp = ALERTS_load_object( ucp, &pdecoder_serial_number, sizeof(UNS_32), &ldh );
	ucp = ALERTS_load_object( ucp, &pmoisture, sizeof(float), &ldh );
	ucp = ALERTS_load_object( ucp, &plower_threshold, sizeof(float), &ldh );
	ucp = ALERTS_load_object( ucp, &pupper_threshold, sizeof(float), &ldh );
	ucp = ALERTS_load_object( ucp, &pmois_alert_type, sizeof(MOISTURE_SENSOR_ALERT_TYPE), &ldh );

	ALERTS_add_alert( AID_SOIL_MOISTURE_CROSSED_THRESHOLD, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_soil_temperature_crossed_threshold( const UNS_32 pdecoder_serial_number, const INT_32 ptemperature, const INT_32 plower_threshold, const INT_32 pupper_threshold, const MOISTURE_SENSOR_ALERT_TYPE pmois_alert_type )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SOIL_TEMPERATURE_CROSSED_THRESHOLD, &ldh );

	ucp = ALERTS_load_object( ucp, &pdecoder_serial_number, sizeof(UNS_32), &ldh );
	ucp = ALERTS_load_object( ucp, &ptemperature, sizeof(INT_32), &ldh );
	ucp = ALERTS_load_object( ucp, &plower_threshold, sizeof(INT_32), &ldh );
	ucp = ALERTS_load_object( ucp, &pupper_threshold, sizeof(INT_32), &ldh );
	ucp = ALERTS_load_object( ucp, &pmois_alert_type, sizeof(MOISTURE_SENSOR_ALERT_TYPE), &ldh );

	ALERTS_add_alert( AID_SOIL_TEMPERATURE_CROSSED_THRESHOLD, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_soil_conductivity_crossed_threshold( const UNS_32 pdecoder_serial_number, const float pconductivity, const float plower_threshold, const float pupper_threshold, const MOISTURE_SENSOR_ALERT_TYPE pmois_alert_type )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_SOIL_CONDUCTIVITY_CROSSED_THRESHOLD, &ldh );

	ucp = ALERTS_load_object( ucp, &pdecoder_serial_number, sizeof(UNS_32), &ldh );
	ucp = ALERTS_load_object( ucp, &pconductivity, sizeof(float), &ldh );
	ucp = ALERTS_load_object( ucp, &plower_threshold, sizeof(float), &ldh );
	ucp = ALERTS_load_object( ucp, &pupper_threshold, sizeof(float), &ldh );
	ucp = ALERTS_load_object( ucp, &pmois_alert_type, sizeof(MOISTURE_SENSOR_ALERT_TYPE), &ldh );

	ALERTS_add_alert( AID_SOIL_CONDUCTIVITY_CROSSED_THRESHOLD, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_station_card_added_or_removed_idx( const UNS_32 pbox_index_0, const UNS_32 pcard_number_0, const BOOL_32 pinstalled )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_STATION_CARD_ADDED_OR_REMOVED, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pcard_number_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pinstalled, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_STATION_CARD_ADDED_OR_REMOVED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_lights_card_added_or_removed_idx( const UNS_32 pbox_index_0, const BOOL_32 pinstalled )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_LIGHTS_CARD_ADDED_OR_REMOVED, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pinstalled, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_LIGHTS_CARD_ADDED_OR_REMOVED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_poc_card_added_or_removed_idx( const UNS_32 pbox_index_0, const BOOL_32 pinstalled )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_POC_CARD_ADDED_OR_REMOVED, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pinstalled, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_POC_CARD_ADDED_OR_REMOVED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_weather_card_added_or_removed_idx( const UNS_32 pbox_index_0, const BOOL_32 pinstalled )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_WEATHER_CARD_ADDED_OR_REMOVED, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pinstalled, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_WEATHER_CARD_ADDED_OR_REMOVED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_communication_card_added_or_removed_idx( const UNS_32 pbox_index_0, const BOOL_32 pinstalled )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_COMMUNICATION_CARD_ADDED_OR_REMOVED, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pinstalled, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_COMMUNICATION_CARD_ADDED_OR_REMOVED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_station_terminal_added_or_removed_idx( const UNS_32 pbox_index_0, const UNS_32 pterminal_number_0, const BOOL_32 pinstalled )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_STATION_TERMINAL_ADDED_OR_REMOVED, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pterminal_number_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pinstalled, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_STATION_TERMINAL_ADDED_OR_REMOVED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_lights_terminal_added_or_removed_idx( const UNS_32 pbox_index_0, const BOOL_32 pinstalled )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_LIGHTS_TERMINAL_ADDED_OR_REMOVED, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pinstalled, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_LIGHTS_TERMINAL_ADDED_OR_REMOVED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_poc_terminal_added_or_removed_idx( const UNS_32 pbox_index_0, const BOOL_32 pinstalled )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_POC_TERMINAL_ADDED_OR_REMOVED, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pinstalled, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_POC_TERMINAL_ADDED_OR_REMOVED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_weather_terminal_added_or_removed_idx( const UNS_32 pbox_index_0, const BOOL_32 pinstalled )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_WEATHER_TERMINAL_ADDED_OR_REMOVED, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pinstalled, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_WEATHER_TERMINAL_ADDED_OR_REMOVED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_communication_terminal_added_or_removed_idx( const UNS_32 pbox_index_0, const BOOL_32 pinstalled )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_COMMUNICATION_TERMINAL_ADDED_OR_REMOVED, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pinstalled, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_COMMUNICATION_TERMINAL_ADDED_OR_REMOVED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_two_wire_terminal_added_or_removed_idx( const UNS_32 pbox_index_0, const BOOL_32 pinstalled )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_TWO_WIRE_TERMINAL_ADDED_OR_REMOVED, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_object( ucp, &pinstalled, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_TWO_WIRE_TERMINAL_ADDED_OR_REMOVED, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_poc_assigned_to_mainline( char *ppoc_name, char *psystem_name, const UNS_32 preason, const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_POC_ASSIGNED_TO_MAINLINE, &ldh );

	ucp = ALERTS_load_string( ucp, ppoc_name, &ldh );
	ucp = ALERTS_load_string( ucp, psystem_name, &ldh );
	ucp = ALERTS_load_object( ucp, &preason, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_POC_ASSIGNED_TO_MAINLINE, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_station_group_assigned_to_mainline( char *pgroup_name, char *psystem_name, const UNS_32 preason, const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_STATION_GROUP_ASSIGNED_TO_MAINLINE, &ldh );

	ucp = ALERTS_load_string( ucp, pgroup_name, &ldh );
	ucp = ALERTS_load_string( ucp, psystem_name, &ldh );
	ucp = ALERTS_load_object( ucp, &preason, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_STATION_GROUP_ASSIGNED_TO_MAINLINE, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_station_group_assigned_to_a_moisture_sensor( char *pstation_group_name, char *pmoisture_sensor_name, const UNS_32 preason, const UNS_32 pbox_index_0 )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_STATION_GROUP_ASSIGNED_TO_A_MOISTURE_SENSOR, &ldh );

	ucp = ALERTS_load_string( ucp, pstation_group_name, &ldh );
	ucp = ALERTS_load_string( ucp, pmoisture_sensor_name, &ldh );
	ucp = ALERTS_load_object( ucp, &preason, sizeof(UNS_16), &ldh );
	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );

	ALERTS_add_alert( AID_STATION_GROUP_ASSIGNED_TO_A_MOISTURE_SENSOR, ldh );
}

/* ---------------------------------------------------------- */
extern void Alert_walk_thru_station_added_or_removed( const UNS_32 pbox_index_0,  char *pgroupname, const INT_32 pstation, const BOOL_32 padded, const UNS_32 preason )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 128 ];  // build the alert on the stack in this array

	UNS_8 *ucp;

	ucp = ALERTS_load_common( (unsigned char*)&lalert, AID_WALK_THRU_STATION_ADDED_OR_REMOVED, &ldh );

	ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof(UNS_8), &ldh );
	ucp = ALERTS_load_string( ucp, pgroupname, &ldh );
	ucp = ALERTS_load_object( ucp, &pstation, sizeof( pstation ), &ldh );
	ucp = ALERTS_load_object( ucp, &padded, sizeof( padded ), &ldh );
	ucp = ALERTS_load_object( ucp, &preason, sizeof(UNS_16), &ldh );
	
	ALERTS_add_alert( AID_WALK_THRU_STATION_ADDED_OR_REMOVED, ldh );
}

/* ---------------------------------------------------------- */

#if ALERTS_ABILITY_TO_GENERATE_ALL_ALERTS_AND_REPORTS_FOR_DEBUG

extern void ALERTS_generate_all_alerts_for_debug( const BOOL_32 preset_all_piles )
{
	// 10/7/2013 rmd : Generally called within context of KEY_PROCESS task as this was
	// originally conceived as a user initiated activity during development only. We use the
	// 'unused' attribute so we can easily comment out certain lines. As we had to do during
	// some debug effort.
	
/*
	char pfilename[] __attribute__((unused)) = "Test_File";
	char pgroup_name[] __attribute__((unused)) = "Test_Group";
	char pitem_name[] __attribute__((unused)) = "Test_Item";
	char plist_name[] __attribute__((unused)) = "Test_List";
	char pmessage[] __attribute__((unused)) = "Test_Message";
	char pstr[] __attribute__((unused)) = "Test_String";
	char pvar_name[] __attribute__((unused)) = "Test_Var";
	BOOL_32 pvalue_bool __attribute__((unused)) = (true);
	float pvalue_float __attribute__((unused)) = 12.345f;
	INT_32 pedit_count __attribute__((unused)) = 12345;
	INT_32 pitem_size __attribute__((unused)) = 12345;
	INT_32 pvalue_int32 __attribute__((unused)) = 12345;
	INT_32 pversion __attribute__((unused)) = 12345;
	UNS_32 pallowed_length __attribute__((unused)) = 12345;
	UNS_32 pflash_index __attribute__((unused)) = 123;
	UNS_32 pgid __attribute__((unused)) = 12345;
	UNS_32 pinit_reason __attribute__((unused)) = INIT_BY_FORCE;
	UNS_32 pinitiated_by __attribute__((unused)) = INITIATED_VIA_KEYPAD;
	UNS_32 pmlb_type __attribute__((unused)) = MLB_TYPE_DURING_IRRIGATION;
	UNS_32 poutput_type __attribute__((unused)) = OUTPUT_TYPE_TERMINAL_MASTER_VALVE;
	UNS_32 pport __attribute__((unused)) = UPORT_A;
	UNS_32 preason_for_change __attribute__((unused)) = CHANGE_REASON_KEYPAD;
	UNS_32 preason_for_CTS_timeout __attribute__((unused)) = CTS_IDLE;
	UNS_32 preason_in_list __attribute__((unused)) = IN_LIST_FOR_PROGRAMMED_IRRIGATION;
	UNS_32 preason_skipped __attribute__((unused)) = SKIPPED_DUE_TO_MLB;
	UNS_32 pstation __attribute__((unused)) = 123;
	UNS_32 pvalue __attribute__((unused)) = 12345;
	UNS_32 pvalue_uns8 __attribute__((unused)) = 123;
	DATE_TIME pdate_time __attribute__((unused)) = { 12345, 12345 };
	UNS_32 pline_num __attribute__((unused)) = 12345;
	UNS_32 pbox_index_0 __attribute__((unused)) = 0;
	UNS_32 poutput_index_0 __attribute__((unused)) = 0;
	UNS_32 pdecoder_serial_number __attribute__((unused)) = 12345;
	UNS_32 preason_in_lights_list __attribute__((unused)) = IN_LIST_FOR_PROGRAMMED_LIGHTS;
	UNS_32 preason_light_skipped __attribute__((unused)) = LIGHTS_TEXT_SKIPPED_NO_STOP_TIME;
	
	// 2/11/2013 ajv : If requested, reset all of the piles before generating
	// the alerts. This can help clean up the pile to simplify debugging with
	// Command Center.
	if( preset_all_piles == (true) )
	{
		ALERTS_restart_all_piles();
	}

	// 10/6/2015 ajv : The following alerts are ordered in ascending order by the Alert ID.
	// There's no necessity for this, but it keeps it organized.

	Alert_divider_small();

	Alert_user_reset_idx( pbox_index_0 );

	Alert_program_restart_idx( pbox_index_0 );
	Alert_power_fail_idx( &pdate_time, pbox_index_0 );
	Alert_power_fail_brownout_idx( &pdate_time, pbox_index_0 );
	Alert_firmware_update_idx( pstr, pstr, pbox_index_0 );
	Alert_main_code_needs_updating_at_slave_idx( pbox_index_0 );
	Alert_tpmicro_code_needs_updating_at_slave_idx( pbox_index_0 );
	Alert_program_restart_idx( pbox_index_0 );

	Alert_Message( pmessage );
	Alert_message_on_tpmicro_pile_M( pmessage );

	Alert_watchdog_timeout();

	Alert_user_pile_restarted( pinit_reason );
	Alert_change_pile_restarted( pinit_reason );
	Alert_tp_micro_pile_restarted( pinit_reason );
	Alert_engineering_pile_restarted( pinit_reason );

	Alert_battery_backed_var_initialized( pvar_name, pinit_reason );
	Alert_battery_backed_var_valid( pvar_name );

	Alert_task_frozen( pstr, pvalue );

	#if ALERT_INCLUDE_FILENAME

		Alert_group_not_found_with_filename( pfilename, pline_num );
		Alert_station_not_found_with_filename( pfilename, pline_num );
		Alert_station_not_in_group_with_filename( pfilename, pline_num );
		Alert_func_call_with_null_ptr_with_filename( pfilename, pline_num );
		Alert_index_out_of_range_with_filename( pfilename, pline_num );
		Alert_item_not_on_list_with_filename( pitem_name, plist_name, pfilename, pline_num );
		Alert_bit_set_with_no_data_with_filename( pfilename, pline_num );

	#else

		Alert_group_not_found_without_filename();
		Alert_station_not_found_without_filename();
		Alert_station_not_in_group_without_filename();
		Alert_func_call_with_null_ptr_without_filename();
		Alert_index_out_of_range_without_filename();
		Alert_item_not_on_list_without_filename( pitem_name, plist_name );
		Alert_bit_set_with_no_data_without_filename();

	#endif

	Alert_flash_file_system_passed( pflash_index );
	Alert_flash_file_found( pfilename, pversion, pedit_count, pitem_size );
	Alert_flash_file_size_error( pfilename );
	Alert_flash_file_found_old_version( pfilename );
	Alert_flash_file_not_found( pfilename );
	Alert_flash_file_deleting_obsolete( pflash_index, pfilename );
	Alert_flash_file_obsolete_file_not_found( pfilename );
	Alert_flash_file_deleting( pflash_index, pfilename, pversion, pedit_count, pitem_size );
	Alert_flash_file_old_version_not_found( pflash_index, pfilename );
	Alert_flash_file_writing( pflash_index, pfilename, pversion, pedit_count, pitem_size );
	Alert_flash_file_write_postponed( pfilename );

	Alert_group_not_watersense_compliant( pgroup_name );

	Alert_unit_communicated( pstr );
	Alert_comm_command_received( MID_FROM_COMMSERVER_ENGINEERING_ALERTS_full_receipt_ack );
	Alert_comm_command_sent( MID_TO_COMMSERVER_ENGINEERING_ALERTS_init_packet );
	Alert_comm_crc_failed( pport );

	Alert_cts_timeout( pport, preason_for_CTS_timeout );
	Alert_packet_greater_than_512( pport );
	Alert_status_timer_expired_idx( pbox_index_0 );

	#if ALERT_INCLUDE_FILENAME

		// 7/22/2015 mpd : Added defaults (param 3) to calls requiring one. Setting an out-of-range value as the default 
		// is not logical, but the compiler is happy!
		Alert_range_check_failed_bool_with_filename( pvar_name, NULL, ( pvalue_bool ), pvalue_bool, pfilename, pline_num );
		Alert_range_check_failed_bool_with_filename( pvar_name, pgroup_name, ( pvalue_bool ), pvalue_bool, pfilename, pline_num );

		Alert_range_check_failed_date_with_filename( pvar_name, NULL, pdate_time.D, pfilename, pline_num );
		Alert_range_check_failed_date_with_filename( pvar_name, pgroup_name, pdate_time.D, pfilename, pline_num );

		Alert_range_check_failed_float_with_filename( pvar_name, NULL, ( pvalue_float ), pvalue_float, pfilename, pline_num );
		Alert_range_check_failed_float_with_filename( pvar_name, pgroup_name, ( pvalue_float ), pvalue_float, pfilename, pline_num );

		Alert_range_check_failed_int32_with_filename( pvar_name, NULL, ( pvalue_int32 ), pvalue_int32, pfilename, pline_num );
		Alert_range_check_failed_int32_with_filename( pvar_name, pgroup_name, ( pvalue_int32 ), pvalue_int32, pfilename, pline_num );

		Alert_range_check_failed_string_with_filename( pvar_name, pstr, pfilename, pline_num );

		Alert_string_too_long( pstr, pallowed_length );

		Alert_range_check_failed_time_with_filename( pvar_name, NULL, pdate_time.T, pfilename, pline_num );
		Alert_range_check_failed_time_with_filename( pvar_name, pgroup_name, pdate_time.T, pfilename, pline_num );

		Alert_range_check_failed_uint32_with_filename( pvar_name, NULL, ( pvalue ), pvalue, pfilename, pline_num );
		Alert_range_check_failed_uint32_with_filename( pvar_name, pgroup_name, ( pvalue ), pvalue, pfilename, pline_num );

	#else

		Alert_range_check_failed_bool_without_filename( pvar_name, NULL, pvalue_bool );
		Alert_range_check_failed_bool_without_filename( pvar_name, pgroup_name, pvalue_bool );
		Alert_range_check_failed_date_without_filename( pvar_name, NULL, pdate_time.D );
		Alert_range_check_failed_date_without_filename( pvar_name, pgroup_name, pdate_time.D );
		Alert_range_check_failed_float_without_filename( pvar_name, NULL, pvalue_float );
		Alert_range_check_failed_float_without_filename( pvar_name, pgroup_name, pvalue_float );
		Alert_range_check_failed_int32_without_filename( pvar_name, NULL, pvalue_int32 );
		Alert_range_check_failed_int32_without_filename( pvar_name, pgroup_name, pvalue_int32 );
		Alert_range_check_failed_string_without_filename( pvar_name, pstr );
		Alert_string_too_long( pstr, pallowed_length );
		Alert_range_check_failed_time_without_filename( pvar_name, NULL, pdate_time.T );
		Alert_range_check_failed_time_without_filename( pvar_name, pgroup_name, pdate_time.T );
		Alert_range_check_failed_uint32_without_filename( pvar_name, NULL, pvalue );
		Alert_range_check_failed_uint32_without_filename( pvar_name, pgroup_name, pvalue );

	#endif

	Alert_derate_table_update_failed( AID_DERATE_TABLE_UPDATE_FAILED_NO_FLOW, pvalue, pvalue, pstr );
	Alert_derate_table_update_failed( AID_DERATE_TABLE_UPDATE_FAILED_30_PCT_DIFF, pvalue, pvalue, pstr );
	Alert_derate_table_update_failed( AID_DERATE_TABLE_UPDATE_FAILED_50_PCT_DIFF, pvalue, pvalue, pstr );
	Alert_derate_table_update_successful( pvalue, pvalue, pvalue, pvalue_uns8, pvalue_uns8, pvalue, pvalue );

	Alert_derate_table_update_station_count_idx( pbox_index_0, pstation, pvalue );

	Alert_mainline_break( "", pgroup_name, MLB_TYPE_DURING_IRRIGATION, pvalue, pvalue );
	Alert_mainline_break_cleared( pgid );

	Alert_conventional_station_short_idx( pbox_index_0, pstation );
	Alert_conventional_mv_short_idx( pbox_index_0 );
	Alert_conventional_pump_short_idx( pbox_index_0 );
	Alert_conventional_output_unknown_short_idx( pbox_index_0 );
	Alert_conventional_station_no_current_idx( pbox_index_0, pstation );
	Alert_conventional_mv_no_current_idx( pbox_index_0 );
	Alert_conventional_pump_no_current_idx( pbox_index_0 );

	Alert_flow_not_checked_with_no_reasons_idx( pbox_index_0, pstation );
	Alert_derate_table_flow_too_high();
	Alert_derate_table_too_many_stations();
	Alert_flow_error_idx( AID_FINAL_HIGHFLOW, preason_in_list, pbox_index_0, pstation, pvalue, pvalue, pvalue, pvalue, pvalue, pvalue, pvalue );
	Alert_flow_error_idx( AID_FIRST_HIGHFLOW, preason_in_list, pbox_index_0, pstation, pvalue, pvalue, pvalue, pvalue, pvalue, pvalue, pvalue );
	Alert_flow_error_idx( AID_FINAL_LOWFLOW, preason_in_list, pbox_index_0, pstation, pvalue, pvalue, pvalue, pvalue, pvalue, pvalue, pvalue );
	Alert_flow_error_idx( AID_FIRST_LOWFLOW, preason_in_list, pbox_index_0, pstation, pvalue, pvalue, pvalue, pvalue, pvalue, pvalue, pvalue );

	Alert_two_wire_cable_excessive_current_idx( pbox_index_0 );
	Alert_two_wire_cable_over_heated_idx( pbox_index_0 );
	Alert_two_wire_cable_cooled_off_idx( pbox_index_0 );
	Alert_two_wire_station_decoder_fault_idx( pbox_index_0, pstation, pstation, pdecoder_serial_number );
	Alert_two_wire_poc_decoder_fault_idx( POC_DECODER_FAULT_ALERT_CODE_voltage_too_low, pbox_index_0, pdecoder_serial_number, pgroup_name );

	Alert_scheduled_irrigation_started( preason_in_list );
	Alert_some_or_all_skipping_their_start( preason_in_list, preason_skipped );
	Alert_programmed_irrigation_still_running_idx( pbox_index_0, pstation );
	Alert_hit_stop_time();
	Alert_irrigation_ended( preason_in_list );

	Alert_flow_not_checked_with_reason_idx( AID_FLOW_NOT_CHECKED__CYCLE_TOO_SHORT, pbox_index_0, pstation );
	Alert_flow_not_checked_with_reason_idx( AID_FLOW_NOT_CHECKED__UNSTABLE_FLOW, pbox_index_0, pstation );

	Alert_set_no_water_days_by_station_idx( pbox_index_0, pstation, 10 );
	Alert_set_no_water_days_by_group( pstr, pgid, 11 );
	Alert_set_no_water_days_all_stations( 12 );

	Alert_MVOR_started( pgroup_name, MVOR_ACTION_OPEN_MASTER_VALVE, pvalue, pinitiated_by);
	Alert_MVOR_skipped( pgroup_name, preason_skipped );

	Alert_entering_forced();
	Alert_leaving_forced();
	Alert_starting_scan_with_reason( COMM_MNGR_reason_to_scan_startup );
	Alert_chain_is_the_same_idx( pbox_index_0 );
	Alert_chain_has_changed_idx( pbox_index_0 );

	Alert_reboot_request( SYSTEM_SHUTDOWN_EVENT_factory_reset_via_keypad );

	Alert_ETGage_First_Zero();
	Alert_ETGage_Second_or_More_Zeros();
	Alert_remaining_gage_pulses_changed( pvalue, pvalue );
	Alert_ETGage_Pulse( pvalue_uns8 );
	Alert_et_gage_pulse_but_no_gage_in_use();
	Alert_ETGage_RunAway();
	Alert_ETGage_RunAway_Cleared();
	Alert_ETGage_percent_full_warning( pvalue );

	Alert_rain_affecting_irrigation();
	Alert_24HourRainTotal( pvalue_float );
	Alert_rain_pulse_but_no_bucket_in_use();

	Alert_rain_switch_affecting_irrigation();
	Alert_freeze_switch_affecting_irrigation();

	Alert_wind_detected_but_no_gage_in_use();

	Alert_stop_key_pressed_idx( pgid, preason_in_list, pbox_index_0 );

	Alert_wind_paused( pvalue );
	Alert_wind_resumed( pvalue );

	Alert_ET_table_loaded();

	Alert_accum_rain_set_by_station( pbox_index_0, pstation, pvalue,pinitiated_by );

	Alert_fuse_replaced_idx( pbox_index_0 );
	Alert_fuse_blown_idx( pbox_index_0 );

	Alert_mobile_station_on( pbox_index_0, pstation );
	Alert_mobile_station_off( pbox_index_0, pstation );

	Alert_test_station_started_idx( pbox_index_0, pstation, pinitiated_by );
	Alert_manual_water_station_started_idx( pbox_index_0, pstation, pinitiated_by );
	Alert_manual_water_program_started( pstr, pinitiated_by );
	Alert_manual_water_all_started( pinitiated_by );

	Alert_ET_Table_substitution_100u( pstr, pdate_time.D, pvalue, pvalue );
	Alert_ET_Table_limited_entry_100u( pstr, pdate_time.D, pvalue, pvalue );

	Alert_light_ID_with_text( pbox_index_0, poutput_index_0, LIGHTS_TEXT_SCHEDULED_ON );

	Alert_rain_switch_active();
	Alert_rain_switch_inactive();
	Alert_freeze_switch_active();
	Alert_freeze_switch_inactive();

	Alert_station_added_to_group_idx( pbox_index_0, pstation, pgroup_name, preason_for_change );
	Alert_station_removed_from_group_idx( pbox_index_0, pstation, pgroup_name, preason_for_change );
	Alert_station_copied_idx( pbox_index_0, pstation, pgroup_name, preason_for_change );
	Alert_station_moved_from_one_group_to_another_idx( pbox_index_0, pstation, pgroup_name, pgroup_name, preason_for_change );

	Alert_station_card_added_or_removed_idx( pbox_index_0, 0, (true) );
	Alert_lights_card_added_or_removed_idx( pbox_index_0, (true) );
	Alert_poc_card_added_or_removed_idx( pbox_index_0, (true) );
	Alert_weather_card_added_or_removed_idx( pbox_index_0, (true) );
	Alert_communication_card_added_or_removed_idx( pbox_index_0, (true) );
	Alert_station_terminal_added_or_removed_idx( pbox_index_0, 0, (true) );
	Alert_lights_terminal_added_or_removed_idx( pbox_index_0, (true) );
	Alert_poc_terminal_added_or_removed_idx( pbox_index_0, (true) );
	Alert_weather_terminal_added_or_removed_idx( pbox_index_0, (true) );
	Alert_communication_terminal_added_or_removed_idx( pbox_index_0, (true) );
	Alert_two_wire_terminal_added_or_removed_idx( pbox_index_0, (true) );

	Alert_poc_assigned_to_mainline( pgroup_name, pgroup_name, preason_for_change, pbox_index_0 );
	Alert_station_group_assigned_to_mainline( pgroup_name, pgroup_name, preason_for_change, pbox_index_0 );

	// ----------

	// 10/4/2013 ajv : Although we probably need to test all of the change lines
	// long term, that's a signficant amount of work which is difficult to
	// justfiy at the moment. However, in the interim, generate one of each type
	// of change line (group, station, and controller), so Rodger can verify
	// that his generic parsing of change lines works.
	const UNS_32 pfrom = (false);
	const UNS_32 pto = (true);
	
	// 10/4/2013 ajv : Sample Group Change Line
	Alert_ChangeLine_Group( CHANGE_IRRIGATION_SYSTEM_CAPACITY_IN_USE, pgroup_name, &pfrom, &pto, sizeof( pto ), preason_for_change, pbox_index_0 );

	// 10/4/2013 ajv : Sample Station Change Line
	Alert_ChangeLine_Station( CHANGE_STATION_IN_USE, pbox_index_0, pstation, &pfrom, &pto, sizeof( pto ), preason_for_change, pbox_index_0 );

	// 7/22/2015 mpd : The original sample define no longer exists. A time zone change will do.
	// 10/4/2013 ajv : Sample Controller Change Line
	Alert_ChangeLine_Controller( CHANGE_NETWORK_CONFIG_TIME_ZONE, pbox_index_0, &pfrom, &pto, sizeof( pto ), preason_for_change, pbox_index_0 );

	*/
	
	// ----------
	
	// 4/11/2018 rmd : And the alerts AJ recently added.
	Alert_poc_not_found_extracting_token_resp( 54321, 2 );
	Alert_token_resp_extraction_error();
	Alert_token_rcvd_with_no_FL();
	Alert_token_rcvd_with_FL_turned_off();
	Alert_token_rcvd_by_FL_master();
	
	Alert_hub_rcvd_packet( ROUTER_TYPE_FL_SLAVE, UPORT_A, ROUTING_CLASS_IS_2000_BASED, 7890 );
	
	Alert_hub_forwarding_packet( ROUTER_TYPE_BEHIND_HUB, UPORT_TP, ROUTING_CLASS_IS_3000_BASED, 3456 );
	
	Alert_router_rcvd_unexp_class( ROUTER_TYPE_BEHIND_HUB, UPORT_TP, ROUTING_CLASS_IS_3000_BASED, MSG_CLASS_CS3000__FROM_HUB__CODE_DISTRIBUTION_ADDRESSABLE );
	Alert_router_rcvd_unexp_base_class( ROUTER_TYPE_BEHIND_HUB, UPORT_B, ROUTING_CLASS_IS_3000_BASED );
	Alert_router_rcvd_packet_not_on_hub_list( ROUTER_TYPE_FL_MASTER, UPORT_B, ROUTING_CLASS_IS_2000_BASED );
	Alert_router_rcvd_packet_not_on_hub_list_with_sn( ROUTER_TYPE_HUB, UPORT_A, ROUTING_CLASS_IS_2000_BASED, 12345 );
	Alert_router_rcvd_packet_not_for_us( ROUTER_TYPE_BEHIND_HUB, UPORT_B, ROUTING_CLASS_IS_3000_BASED );
	Alert_router_unexp_to_addr_port( ROUTER_TYPE_HUB, UPORT_A );
	
	Alert_router_unk_port( ROUTER_TYPE_BEHIND_HUB );
	Alert_router_unk_port( ROUTER_TYPE_FL_MASTER );
	Alert_router_unk_port( ROUTER_TYPE_FL_SLAVE );
	Alert_router_unk_port( ROUTER_TYPE_HUB );
	Alert_router_unk_port( ROUTER_TYPE_RRE );
	
	Alert_router_received_unexp_token_resp( ROUTER_TYPE_BEHIND_HUB, UPORT_TP );
	Alert_router_received_unexp_token_resp( ROUTER_TYPE_FL_MASTER, UPORT_A );
	Alert_router_received_unexp_token_resp( ROUTER_TYPE_FL_SLAVE, UPORT_B );
	
	Alert_ci_queued_msg( CI_POST_TO_FRONT, CI_MESSAGE_send_flow_recording );
	Alert_ci_queued_msg( CI_POST_TO_BACK, CI_MESSAGE_send_rain_indication );
	
	Alert_ci_waiting_for_device();
	Alert_ci_starting_connection_process();
	Alert_cellular_socket_attempt();
	Alert_cellular_data_connection_attempt();
	
	Alert_msg_transaction_time( 12 );  // this is seconds
	
	Alert_largest_token_size( 1200, (false) );
	Alert_largest_token_size( 2200, (true) );
	
	Alert_budget_over_budget_period_ending_today( "tricky poc name", 169 );
	
} 

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

