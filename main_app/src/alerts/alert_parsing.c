/*  file = alert_parsing.c                  2014.06.03   ajv  */

// Our scheme is to allocate memory for the text to be displayed. It will be an
// array of array of chars. Each line will have a maximum of 64 chars of text.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

// 6/18/2014 ajv : Required for memset
#include	<string.h>

#include	"alert_parsing.h"

#include	"alerts.h"

// 9/3/2014 ajv : Required for NUMBER_OF_CHARS_IN_A_NAME
#include	"cal_string.h"

// 6/18/2014 ajv : Required for DATE_TIME
#include	"cal_td_utils.h"

// 6/18/2014 ajv : Required for the change line ID definitions
#include	"change.h"

#include	"comm_mngr.h"

#include	"configuration_network.h"

#include	"controller_initiated.h"

// 9/3/2014 ajv : Required for MAX_ALERT_LENGTH and OUTPUT_TYPE_TERMINAL_MASTER_VALVE
#include	"cs3000_tpmicro_common.h"

#include	"et_data.h"

// 9/3/2014 ajv : Required for MLB_TYPE_DURING_IRRIGATION
#include	"foal_comm.h"

// 6/18/2014 ajv : Required for COMM_COMMANDS
#include	"packet_definitions.h"

// 6/18/2014 ajv : Required for the ON_AT_A_TIME_X definition
#include	"station_groups.h"

#include	"weather_tables.h"

#include	"wdt_and_powerfail.h"

#include	"irrigation_system.h"

#include	"walk_thru.h"

// 3/20/2015 ajv : Required for Packet > 512 alert since it's not actually 512 any longer,
// but rather a calculated value which includes the CRC length.
#include	"crc.h"

#ifndef _MSC_VER

	#include	"flowsense.h"

	#include	"foal_irri.h"

#else

	const char* const port_names[ 6 ] = { "Port TP", "Port A", "Port B", "Port RRE", "Port USB", "Port M1" };

	#define	FLOWSENSE_we_are_poafs()				(true)

#endif



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	MAX_ALERT_LENGTH					(ALERTS_MAX_TOTAL_PARSED_LENGTH)

// ----------


#define	ALERT_INCLUDES_GROUP_NAME			(true)
#define	ALERT_DOES_NOT_INCLUDE_GROUP_NAME	(false)

// ----------

#define	ALERT_INCLUDES_FILENAME				(true)
#define	ALERT_DOES_NOT_INCLUDE_FILENAME		(false)

// ----------

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static char* const who_initiated_str[ (INITIATED_VIA_LAST_TYPE + 1) ] =
{
	" (keypad)",
	" (comm)",
	" (direct access)",
	" (mv short)",
	" (scheduled)",
	" (mainline break)",
	" (system timeout)",
	" (mobile)",
	" (UNK REASON)"
};

// ----------

static const char *const irrigation_skipped_reason_str[ ] =
{
	"Controller OFF",
	"Mainline Break",
	"No Water Day",
	"Calendar No Water Day",
	"Master Valve Closed in Effect",
	"Rain in the Rain Table",
	"Accumulated Rain",
	"Rain Switch",
	"Freeze Switch",
	"Wind",
	"Cycle Too Short",
	"Stop Time SAME AS Start Time!",
	"Station Quantity",
	"Mow Day",
	"2-Wire Cable Overheated",
	"Station Not In Use",
	"Accumulated Rain",	// formerly Moisture Balance
	"No Close Time",
	"Open and Close Times the Same",
	"Close Time is Before Open Time"
};

// ----------

static const char* init_reason_str[ ] =
{
	"(integrity test failed)",
	"(by request)"
};

// ----------

static const char *init_reason_unk = "(unknown reason)";

// ----------

static const char* installed_or_removed_str[ ] =
{
	"removed",
	"installed"
};

// ----------

static const char* scan_start_request_str[ ] =
{
	"startup",
	"keypad",
	"rescan",
	"slave requested"
};

static const char *const lights_text_str[ ] =
{
	"START:",							// ON reason
	"END:",								// OFF reason
	"START: Manual",					// ON reason
	"END: Manual",						// OFF reason
	"START: Mobile",					// ON reason
	"END: Mobile",						// OFF reason
	"No scheduled Stop Time!",			// Skipped reason
	"Stop Time SAME AS Start Time!",	// Skipped reason
	"",	// NLU "Off time modified by mobile",
	"",	// NLU "Off time modified manually",
	"",	// NLU "Off time extended by schedule",
	"",	// NLU "Being forced OFF by chain down",
	"",	// NLU "Being forced OFF by watchdog",
	"",	// NLU "IRRI ON completed",
	"",	// NLU "IRRI OFF completed",
	"",	// NLU "IRRI removed"
};

// ----------

static const char* router_type_str[ ] =
{
	"HUB",
	"On a Hub",
	"Non-Hub Master",
	"Slave",
	"RRe"
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern char *ALERTS_get_station_number_with_or_without_two_wire_indicator( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, char *sta_num_str, const UNS_32 size_of_str )
{
	char    str_16[ 16 ];

	// 10/21/2013 ajv : Only show the controller letter if the controller is part of a FLOWSENSE
	// chain.
	if( FLOWSENSE_we_are_poafs() )
	{
		snprintf( sta_num_str, size_of_str, "%c %s", (pbox_index_0 + 'A'), STATION_get_station_number_string( pbox_index_0, pstation_number_0, (char*)&str_16, sizeof(str_16) ) );
	}
	else
	{
		snprintf( sta_num_str, size_of_str, "%s", STATION_get_station_number_string( pbox_index_0, pstation_number_0, (char*)&str_16, sizeof(str_16) ) );
	}

	return( sta_num_str );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Note the use of the DLLEXPORT macro here. This exposes the routine to the
 * outside world for use in a dll. The CS3000 Comm Server uses this routine to
 * determine whether or not an alert it receives is intended to be viewed by the
 * user or not.
 */
DLLEXPORT BOOL_32 ALERTS_alert_is_visible_to_the_user( const UNS_32 paid, const BOOL_32 pinclude_alerts, const BOOL_32 pinclude_changes )
{
	BOOL_32 rv;

	rv = (false);

	if( pinclude_alerts == (true) )
	{
		switch( paid )
		{
			// 9/9/2014 ajv : Explicitly set which alerts are visible to the user.
			case AID_ALERT_DIVIDER_LARGE:

			case AID_USER_RESET:
			case AID_POWER_FAIL:
			case AID_POWER_FAIL_BROWNOUT:

			case AID_FIRMWARE_UPDATE:
			case AID_MAIN_CODE_NEEDS_UPDATING_AT_SLAVE:
			case AID_TPMICRO_CODE_NEEDS_UPDATING_AT_SLAVE:
			case AID_PROGRAM_RESTART:

			// 10/3/2014 ajv : Suppressed communication attempts and successes
			// from appearing in the user alerts.
			//case AID_COMM_COMMAND_RCVD: 
			//case AID_COMM_COMMAND_INIT_ISSUED: 

			// 4/7/2017 ajv : Suppressed communication errors in the user pile
			//case AID_COMM_COMMAND_FAILED:
			//case AID_COMM_CRC_FAILED:
			//case AID_CTS_TIMEOUT:
			//case AID_COMM_PACKET_GREATER_THAN_512:

			case AID_LR_RADIO_NOT_COMPAT_WITH_HUB_OPT:
			case AID_LR_RADIO_NOT_COMPAT_WITH_HUB_OPT_REMINDER:

			case AID_COMM_STATUS_TIMER_EXPIRED:
			case AID_MSG_RESPONSE_TIMEOUT:

			// ----------

			case AID_MLB:
			case AID_MLB_CLEARED:

			// ----------

			case AID_CONVENTIONAL_STATION_SHORT:
			case AID_CONVENTIONAL_MV_SHORT:
			case AID_CONVENTIONAL_PUMP_SHORT:
			case AID_CONVENTIONAL_OUTPUT_UNKNOWN_SHORT:

			case AID_CONVENTIONAL_STATION_NO_CURRENT:
			case AID_CONVENTIONAL_MV_NO_CURRENT:
			case AID_CONVENTIONAL_PUMP_NO_CURRENT:

			case AID_TWO_WIRE_CABLE_EXCESSIVE_CURRENT:
			case AID_TWO_WIRE_CABLE_OVER_HEATED:
			case AID_TWO_WIRE_CABLE_COOLED_OFF:

			case AID_TWO_WIRE_STATION_DECODER_FAULT:
			case AID_TWO_WIRE_POC_DECODER_FAULT:

			case AID_FINAL_HIGHFLOW:
			case AID_FINAL_LOWFLOW:

			case AID_SCHEDULED_IRRIGATION_STARTED:
			case AID_SOME_OR_ALL_STATIONS_SKIPPED_THEIR_START:
			case AID_PROGRAMMED_IRRIGATION_STILL_RUNNING:

			case AID_HIT_STOPTIME:
			case AID_IRRIGATION_ENDED:
			// 10/27/2014 ajv : TODO These are suppressed for now until we see
			// how prevalent they are. Jim has asked for them to be added based
			// on On-at-a-Time data he saw at March JPA. However, there's a
			// possiblity that these may become overwhelming in large chains
			// with multiple valves on-at-a-time.
			// case AID_FLOW_NOT_CHECKED__CYCLE_TOO_SHORT:
			// case AID_FLOW_NOT_CHECKED__UNSTABLE_FLOW:

			// 7/31/2015 mpd : Added Lights alerts. 
			case AID_LIGHT_ID_WITH_TEXT:
			//case AID_SCHEDULED_LIGHTING_STARTED:
			//case AID_SCHEDULED_LIGHTING_ENDED:
			//case AID_SCHEDULED_LIGHTING_HIT_STOP:
			//case AID_SCHEDULED_LIGHTING_SKIPPED_START: 

			// 4/1/2016 skc : Added budget alerts
			case AID_BUDGET_UNDER_BUDGET:
			case AID_BUDGET_OVER_BUDGET:
			case AID_BUDGET_WILL_EXCEED_BUDGET:
			case AID_BUDGET_NO_BUDGET_VALUES:
			case AID_BUDGET_GROUP_REDUCTION:
			case AID_BUDGET_PERIOD_ENDED:
			case AID_BUDGET_OVER_BUDGET_PERIOD_ENDING_TODAY:

			case AID_SETNOWDAYS_BYSTATION:
			case AID_SETNOWDAYS_BYGROUP:
			case AID_SETNOWDAYS_BYALL:
			case AID_SETNOWDAYS_BYBOX:

			case AID_MVOR_REQUESTED:
			case AID_MVOR_SKIPPED:

			case AID_ENTERED_FORCED:
			case AID_LEAVING_FORCED:
			case AID_STARTING_SCAN_WITH_REASON:

			case AID_REBOOT_REQUEST:

			case AID_ETGAGE_FIRST_ZERO:
			case AID_ETGAGE_SECOND_OR_MORE_ZEROS:
			case AID_ETGAGE_PULSE:
			case AID_ETGAGE_RUNAWAY:
			case AID_ETGAGE_CLEARED_RUNAWAY:
			case AID_ETGAGE_PERCENT_FULL_WARNING:
			case AID_RAIN_AFFECTING_IRRIGATION:
			case AID_RAIN_24HOUR_TOTAL:
			case AID_RAIN_SWITCH_AFFECTING_IRRIGATION:
			case AID_FREEZE_SWITCH_AFFECTING_IRRIGATION:
			case AID_ACCUM_RAIN_SET_BY_STATION:

			case AID_STOP_KEY_PRESSED:

			case AID_WIND_PAUSED:
			case AID_WIND_RESUMED:

			case AID_ETTABLE_LOADED:

			case AID_FUSE_REPLACED:
			case AID_FUSE_BLOWN:

			case AID_TEST_STATION_STARTED_WITH_REASON:
			
			case AID_WALK_THRU_STARTED:

			case AID_MOBILE_STATION_ON:
			case AID_MOBILE_STATION_OFF:

			case AID_MANUAL_WATER_STATION_STARTED_WITH_REASON:
			case AID_MANUAL_WATER_PROGRAM_STARTED_WITH_REASON:
			case AID_MANUAL_WATER_ALL_STARTED_WITH_REASON:

			case AID_RAIN_SWITCH_ACTIVE:
			case AID_RAIN_SWITCH_INACTIVE:
			case AID_FREEZE_SWITCH_ACTIVE:
			case AID_FREEZE_SWITCH_INACTIVE:

			case AID_STATION_CARD_ADDED_OR_REMOVED:
			case AID_LIGHTS_CARD_ADDED_OR_REMOVED:
			case AID_POC_CARD_ADDED_OR_REMOVED:
			case AID_WEATHER_CARD_ADDED_OR_REMOVED:
			case AID_COMMUNICATION_CARD_ADDED_OR_REMOVED:
			case AID_STATION_TERMINAL_ADDED_OR_REMOVED:
			case AID_LIGHTS_TERMINAL_ADDED_OR_REMOVED:
			case AID_POC_TERMINAL_ADDED_OR_REMOVED:
			case AID_WEATHER_TERMINAL_ADDED_OR_REMOVED:
			case AID_COMMUNICATION_TERMINAL_ADDED_OR_REMOVED:
			case AID_TWO_WIRE_TERMINAL_ADDED_OR_REMOVED:

			case AID_RESET_MOISTURE_BALANCE_BYGROUP:
			case AID_RESET_MOISTURE_BALANCE_ALLSTATIONS:

			case AID_SOIL_MOISTURE_CROSSED_THRESHOLD:
			case AID_SOIL_TEMPERATURE_CROSSED_THRESHOLD:
			case AID_SOIL_CONDUCTIVITY_CROSSED_THRESHOLD:

				rv = (true);
				break;
		}
	}

	if( pinclude_changes == (true) )
	{
		switch( paid )
		{
			case AID_ETGAGE_PERCENT_FULL_EDITED:

			case AID_STATION_ADDED_TO_GROUP:
			case AID_STATION_REMOVED_FROM_GROUP:
			case AID_STATION_COPIED:
			case AID_STATION_MOVED_TO_ANOTHER_GROUP:

			case AID_POC_ASSIGNED_TO_MAINLINE:
			case AID_STATION_GROUP_ASSIGNED_TO_MAINLINE:
			case AID_STATION_GROUP_ASSIGNED_TO_A_MOISTURE_SENSOR:
			case AID_WALK_THRU_STATION_ADDED_OR_REMOVED:

				rv = (true);
				break;

			default:
				if( paid >= CHANGE_GROUP_BASE )
				{
					rv = (true);
				}
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Pull any sized object from the pile, starting with index pointed to by
 * pindex. It increments what pindex points to so the caller has pindex ready to
 * be the "from index" for the next piece of data you want to pull.
 * 
 * @executed 
 * 
 * @param pdest A pointer to the object to pull from the alert pile.
 * @param pindex_ptr A pointer to the index of the alert.
 * @param psize_of_object The size of object pointed to by pdest.
 * 
 * @return UNS_32 The byte count of the object retrieved from the pile.
 *
 * @author 5/10/2012 Adrianusv
 *
 * @revisions
 *    5/10/2012 Initial release
 */
extern UNS_32 ALERTS_pull_object_off_pile( ALERTS_PILE_STRUCT *const ppile, void *pdest, UNS_32 *pindex_ptr, const UNS_32 psize_of_object )
{
	#ifndef _MSC_VER

		unsigned char *lalert_pile;

		UNS_32 i;

		char *cp;

		cp = pdest;

		for( i = 0; i < psize_of_object; ++i )
		{
			lalert_pile = alert_piles[ ppile->alerts_pile_index ].addr;
			lalert_pile += *pindex_ptr;

			*cp++ = *lalert_pile;

			ALERTS_inc_index( ppile, pindex_ptr, 1 );
		}

		return( psize_of_object );

	#else

		unsigned char *lalert_pile;

		UNS_32 i;

		char *cp;

		cp = pdest;

		for( i = 0; i < psize_of_object; ++i )
		{
			lalert_pile = ppile;
			lalert_pile += *pindex_ptr;

			*cp++ = *lalert_pile;

			*pindex_ptr += 1;
		}

		return( psize_of_object );

	#endif
}

/* ---------------------------------------------------------- */
/**
 * Pulls a NULL terminated string up to MAX_ALERT_LENGTH from the alert pile, starting with
 * the index pointed to by pindex. It increments what pindex points to so the caller has
 * pindex ready to be the "from pointer" for the next piece of data you want to pull.
 * 
 * @executed 
 * 
 * @param pdest A pointer to the character array to copy the string into.
 * @param pdest_size The size of the character array. 
 * @param pindex_ptr The index of the alert.
 * 
 * @return UNS_32 The byte count of the string retrieved from the pile,
 *                <i>including</i> the NULL.
 *
 * @author 5/10/2012 Adrianusv
 *
 * @revisions
 *    5/10/2012 Initial release
 */
extern UNS_32 ALERTS_pull_string_off_pile( ALERTS_PILE_STRUCT *const ppile, char *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	// 7/28/2016 rmd : The size of the string on the pile and the pdest_size parameter are not
	// related. This function must do 3 things.
	//
	// 1) Pull the ENTIRE string from the pile and return that length. The returned length must
	//    include the NULL char.
	//
	// 2) Take up to pdest_size characters of that string and put them into the pdest location.
	//    What ends up in pdest must include a NULL. So in fact pdest can only hold
	//    (pdest_size-1) visible characters.
	//
	// 3) When we return pindex_ptr is to be pointing to the NEXT byte in the pile beyond this
	//    string - that is beyond the NULL.

	// ----------

	UNS_8       *lalert_pile;

	UNS_32      pulled, in_dest;

	UNS_32      rv;

	// ----------

	rv = 0;

	pulled = 0;

	in_dest = 0;

	// ----------

	// 7/28/2016 rmd : Clean the dest so when we stop placing chars into the dest the NULL is
	// already in place.
	memset( pdest, 0x00, pdest_size );

	// ----------

	do
	{
		// 7/28/2016 rmd : Form the pointer to the byte on the pile. How convoluted! But is what it
		// is.
		#ifndef _MSC_VER
			lalert_pile = alert_piles[ ppile->alerts_pile_index ].addr;
		#else
			lalert_pile = ppile;
		#endif

		lalert_pile += *pindex_ptr;

		// ----------

		// 7/28/2016 rmd : Increment in_dest FIRST so we inherently leave room for the final NULL
		// when we test against pdest_size.
		in_dest += 1;

		if( in_dest < pdest_size )
		{
			*pdest++ = *lalert_pile;
		}

		// ----------

		// 7/28/2016 rmd : Increment how many pulled. Even if its a NULL.
		rv += 1;

		// 7/28/2016 rmd : Get pointing to next byte in the pile. Even if this is the NULL char.
		#ifndef _MSC_VER
			ALERTS_inc_index( ppile, pindex_ptr, 1 );
		#else
			*pindex_ptr += 1;
		#endif

		// 7/28/2016 rmd : TEST for the end of the string or a run-away condition. At the WEB - for
		// the run-away condition the rv will likely not match the alert length byte value and
		// generate an exception.

	} while( (*lalert_pile != 0x00) || (rv == MAX_ALERT_LENGTH) );

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_power_fail_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lcontroller_index;

	UNS_32  rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_index, pindex_ptr, sizeof(UNS_8) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "POWER FAIL: Controller %c", lcontroller_index+'A' );
		}
		else
		{
			snprintf( pdest, pdest_size, "POWER FAIL" );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_power_fail_brownout_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lcontroller_index;

	UNS_32  rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_index, pindex_ptr, sizeof(UNS_8) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "POWER FAIL (BROWN OUT): Controller %c", lcontroller_index+'A' );
		}
		else
		{
			snprintf( pdest, pdest_size, "POWER FAIL (BROWN OUT)" );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_program_restart_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lcontroller_index;

	UNS_32  rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_index, pindex_ptr, sizeof(UNS_8) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "Firmware Restart: Controller %c", lcontroller_index+'A' );
		}
		else
		{
			snprintf( pdest, pdest_size, "Firmware Restart" );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_user_reset_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lcontroller_index;

	UNS_32  rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_index, pindex_ptr, sizeof(UNS_8) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "RESET TO FACTORY DEFAULTS: Controller %c", lcontroller_index+'A' );
		}
		else
		{
			snprintf( pdest, pdest_size, "RESET TO FACTORY DEFAULTS" );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_firmware_update_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lfrom_str[ 64 ], lto_str[ 64 ];

	UNS_8   lcontroller_index;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&lfrom_str, sizeof(lfrom_str), pindex_ptr );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&lto_str, sizeof(lto_str), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_index, pindex_ptr, sizeof(UNS_8) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		// 3/31/2014 rmd : Reversed string order. Was from to. Made it to from. To make it easier to
		// read on the controller alerts. Prior to change had to scroll line to see what update
		// to.
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "Firmware Updated to \"%s\" from \"%s\": Controller %c", lto_str, lfrom_str, lcontroller_index+'A' );
		}
		else
		{
			snprintf( pdest, pdest_size, "Firmware Updated to \"%s\" from \"%s\"", lto_str, lfrom_str );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_main_code_needs_updating_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lcontroller_index;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_index, pindex_ptr, sizeof(UNS_8) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "CS3000 firmware at Controller %c does not match Controller A", (lcontroller_index + 'A') );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_tpmicro_code_firmware_needs_updating_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lcontroller_index;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_index, pindex_ptr, sizeof(UNS_8) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "TP Micro firmware at Controller %c does not match Controller A", (lcontroller_index + 'A') );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_alerts_pile_init_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	REASONS_TO_INIT_BATTERY_BACKED_VARS lreason;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lreason, pindex_ptr, sizeof(lreason) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		strlcpy( pdest, "User Alerts cleared ", pdest_size );

		if( lreason < N_ARRAY_ELEMENTS( init_reason_str ) )
		{
			strlcat( pdest, init_reason_str[ lreason ], pdest_size );
		}
		else
		{
			strlcat( pdest, init_reason_unk, pdest_size );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_change_pile_init_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	REASONS_TO_INIT_BATTERY_BACKED_VARS lreason;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lreason, pindex_ptr, sizeof(lreason) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		strlcpy( pdest, "Change Lines cleared ", pdest_size );

		if( lreason < N_ARRAY_ELEMENTS( init_reason_str ) )
		{
			strlcat( pdest, init_reason_str[ lreason ], pdest_size );
		}
		else
		{
			strlcat( pdest, init_reason_unk, pdest_size );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_tp_micro_pile_init_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	REASONS_TO_INIT_BATTERY_BACKED_VARS lreason;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lreason, pindex_ptr, sizeof(lreason) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		strlcpy( pdest, "TP Micro messages cleared ", pdest_size );

		if( lreason < N_ARRAY_ELEMENTS( init_reason_str ) )
		{
			strlcat( pdest, init_reason_str[ lreason ], pdest_size );
		}
		else
		{
			strlcat( pdest, init_reason_unk, pdest_size );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_engineering_pile_init_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	REASONS_TO_INIT_BATTERY_BACKED_VARS lreason;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lreason, pindex_ptr, sizeof(lreason) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		strlcpy( pdest, "Engineering Alerts cleared ", pdest_size );

		if( lreason < N_ARRAY_ELEMENTS( init_reason_str ) )
		{
			strlcat( pdest, init_reason_str[ lreason ], pdest_size );
		}
		else
		{
			strlcat( pdest, init_reason_unk, pdest_size );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_battery_backed_var_init_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	REASONS_TO_INIT_BATTERY_BACKED_VARS lreason;

	unsigned char   var_name_str_64[ 64 ];

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&var_name_str_64, sizeof(var_name_str_64), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lreason, pindex_ptr, sizeof(lreason) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "%s initialized ", var_name_str_64 );

		if( lreason < N_ARRAY_ELEMENTS( init_reason_str ) )
		{
			strlcat( pdest, init_reason_str[ lreason ], pdest_size );
		}
		else
		{
			strlcat( pdest, init_reason_unk, pdest_size );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_battery_backed_var_valid_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	unsigned char   var_name_str_64[ 64 ];

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&var_name_str_64, sizeof(var_name_str_64), pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "%s valid", var_name_str_64 );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_task_frozen_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   ltaskname[ 64 ];

	UNS_16  lseconds;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&ltaskname, sizeof(ltaskname), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lseconds, pindex_ptr, sizeof(lseconds) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Iced Task: %s, %u ms ago", ltaskname, lseconds );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_group_not_found_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	unsigned char   str_64[ 64 ];

	UNS_32  lline_num;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_64, sizeof(str_64), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lline_num, pindex_ptr, sizeof(lline_num) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Group not found : %s, %d", str_64, lline_num );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_station_not_found_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	unsigned char   str_64[ 64 ];

	UNS_32  lline_num;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_64, sizeof(str_64), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lline_num, pindex_ptr, sizeof(lline_num) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Station not found : %s, %d", str_64, lline_num );
	}

	return( rv );
}
/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_station_not_in_group_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	unsigned char   str_64[ 64 ];

	UNS_32  lline_num;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_64, sizeof(str_64), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lline_num, pindex_ptr, sizeof(lline_num) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Station not in group : %s, %d", str_64, lline_num );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_func_call_with_null_ptr_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	unsigned char   str_64[ 64 ];

	UNS_32  lline_num;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_64, sizeof(str_64), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lline_num, pindex_ptr, sizeof(lline_num) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Function call with NULL ptr : %s, %d", str_64, lline_num );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_index_out_of_range_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	unsigned char   str_64[ 64 ];

	UNS_32  lline_num;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_64, sizeof(str_64), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lline_num, pindex_ptr, sizeof(lline_num) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Index out of range : %s, %d", str_64, lline_num );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_item_not_on_list_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr, const UNS_32 paid )
{
	unsigned char   litem_name_str_64[ 64 ];

	unsigned char   llist_name_str_64[ 64 ];

	unsigned char   lfilename_str_64[ 64 ];

	UNS_32  lline_num;

	UNS_32  rv;

	rv = 0;

	rv += ALERTS_pull_string_off_pile( ppile, (char*)&litem_name_str_64, sizeof(litem_name_str_64), pindex_ptr );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&llist_name_str_64, sizeof(llist_name_str_64), pindex_ptr );

	if( paid == AID_ITEM_NOT_ON_LIST_WITH_FILENAME )
	{
		rv += ALERTS_pull_string_off_pile( ppile, (char*)&lfilename_str_64, sizeof(lfilename_str_64), pindex_ptr );
		rv += ALERTS_pull_object_off_pile( ppile, &lline_num, pindex_ptr, sizeof(lline_num) );
	}

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( paid == AID_ITEM_NOT_ON_LIST_WITH_FILENAME )
		{
			//snprintf( pdest, pdest_size, "Item not on list : %s, %d", lfilename_str_64, lline_num ); 
			snprintf( pdest, pdest_size, "%s not on %s list : %s, %d", litem_name_str_64, llist_name_str_64, lfilename_str_64, lline_num ); 
		}
		else
		{
			//snprintf( pdest, pdest_size, "Item not on list" ); 
			snprintf( pdest, pdest_size, "%s not on %s list", litem_name_str_64, llist_name_str_64 ); 
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_bit_set_with_no_data_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	unsigned char   str_64[ 64 ];

	UNS_32  lline_num;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_64, sizeof(str_64), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lline_num, pindex_ptr, sizeof(lline_num) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Bit set with no data : %s, %d", str_64, lline_num );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_flash_file_system_passed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lflash_index;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lflash_index, pindex_ptr, sizeof(lflash_index) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "File System Passed : chip %d", lflash_index );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_flash_file_found_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	unsigned char   str_32[ 32 ];

	INT_32  lversion;

	INT_32  ledit_count;

	INT_32  litem_size;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_32, sizeof(str_32), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lversion, pindex_ptr, sizeof(lversion) );
	rv += ALERTS_pull_object_off_pile( ppile, &ledit_count, pindex_ptr, sizeof(ledit_count) );
	rv += ALERTS_pull_object_off_pile( ppile, &litem_size, pindex_ptr, sizeof(litem_size) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "%s: found version %ld, edit %ld, %ld bytes", str_32, lversion, ledit_count, litem_size );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_flash_file_size_error_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	unsigned char   str_32[ 32 ];

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_32, sizeof(str_32), pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "File size error: initializing %s", str_32 );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_flash_file_found_old_version_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	unsigned char   str_32[ 32 ];

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_32, sizeof(str_32), pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Found older file version: initializing %s", str_32 );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_flash_file_not_found_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	unsigned char   str_32[ 32 ];

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_32, sizeof(str_32), pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "File Not Found: initializing %s", str_32 );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_flash_file_deleting_obsolete_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	unsigned char   str_32[ 32 ];

	UNS_8   lflash_index;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lflash_index, pindex_ptr, sizeof(lflash_index) );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_32, sizeof(str_32), pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "DELETING OBSOLETE: %d, %s", lflash_index, str_32 );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_flash_file_obsolete_not_found_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	unsigned char   str_32[ 32 ];

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_32, sizeof(str_32), pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "No obsolete files found: %s", str_32 );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_flash_file_deleting_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	unsigned char   str_32[ 32 ];

	UNS_8   lflash_index;

	INT_32  lversion;

	INT_32  ledit_count;

	INT_32  litem_size;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lflash_index, pindex_ptr, sizeof(lflash_index) );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_32, sizeof(str_32), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lversion, pindex_ptr, sizeof(lversion) );
	rv += ALERTS_pull_object_off_pile( ppile, &ledit_count, pindex_ptr, sizeof(ledit_count) );
	rv += ALERTS_pull_object_off_pile( ppile, &litem_size, pindex_ptr, sizeof(litem_size) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "DELETED: %d, %s version %ld, edit %ld, %ld bytes", lflash_index, str_32, lversion, ledit_count, litem_size );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_flash_file_old_version_not_found_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	unsigned char   str_32[ 32 ];

	UNS_8   lflash_index;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lflash_index, pindex_ptr, sizeof(lflash_index) );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_32, sizeof(str_32), pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Old Version Delete: %d, %s, no old versions found", lflash_index, str_32 );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_flash_writing_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	unsigned char   str_32[ 32 ];

	UNS_8   lflash_index;

	INT_32  lversion;

	INT_32  ledit_count;

	INT_32  litem_size;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lflash_index, pindex_ptr, sizeof(lflash_index) );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_32, sizeof(str_32), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lversion, pindex_ptr, sizeof(lversion) );
	rv += ALERTS_pull_object_off_pile( ppile, &ledit_count, pindex_ptr, sizeof(ledit_count) );
	rv += ALERTS_pull_object_off_pile( ppile, &litem_size, pindex_ptr, sizeof(litem_size) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "WROTE: %d, %s version %ld, edit %ld, %ld bytes", lflash_index, str_32, lversion, ledit_count, litem_size );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_flash_write_postponed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lfilename[ 32 ];

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&lfilename, sizeof(lfilename), pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "FILE: postponed save - %s", lfilename );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_group_not_watersense_complaint_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lgroup_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&lgroup_name, sizeof(lgroup_name), pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Station Group \"%s\" is not WaterSense compliant - requires use of ET and Rain", lgroup_name );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_system_preserves_activity_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	const char* const lactivity_str[ 3 ] = { "Clearing obsolete", "System not found - registering", "System found - refreshing" };

	UNS_8   lactivity;

	UNS_32  rv;

	// ----------

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lactivity, pindex_ptr, sizeof(lactivity) );

	// ----------

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( lactivity <= PRESERVES_REFRESHING_EXISTING )
		{
			snprintf( pdest, pdest_size, "System_Preserves: %s", lactivity_str[ lactivity ] );
		}
		else
		{
			snprintf( pdest, pdest_size, "System_Preserves: Unknown activity" );
		}
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_poc_preserves_activity_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	const char* const lactivity_str[ 3 ] = { "Clearing obsolete", "POC not found - registering", "POC found - refreshing" };

	UNS_8   lactivity;

	UNS_32  rv;

	// ----------

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lactivity, pindex_ptr, sizeof(lactivity) );

	// ----------

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( lactivity <= PRESERVES_REFRESHING_EXISTING )
		{
			snprintf( pdest, pdest_size, "POC_Preserves: %s", lactivity_str[ lactivity ] );
		}
		else
		{
			snprintf( pdest, pdest_size, "POC_Preserves: Unknown activity" );
		}
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
extern void ALERTS_parse_comm_command_string( const UNS_32 mid, void *pdest, const UNS_32 pdest_size )
{
	switch( mid )
	{
		case MID_TO_COMMSERVER_REGISTRATION_DATA_init_packet:
			strlcpy( pdest, "Sending Registration Data", pdest_size );
			break;

		case MID_FROM_COMMSERVER_REGISTRATION_DATA_full_receipt_ack:
			strlcpy( pdest, "Registration Data Sent", pdest_size );
			break;

		case MID_FROM_COMMSERVER_REGISTRATION_DATA_NAK:
			strlcpy( pdest, "Registration Data: COMM_SERVER EXCEPTION", pdest_size );
			break;

			// ----------

		case MID_TO_COMMSERVER_ENGINEERING_ALERTS_init_packet:
			strlcpy( pdest, "Sending Alerts", pdest_size );
			break;

		case MID_FROM_COMMSERVER_ENGINEERING_ALERTS_full_receipt_ack:
			strlcpy( pdest, "Alerts Sent", pdest_size );
			break;

		case MID_FROM_COMMSERVER_ENGINEERING_ALERTS_NAK:
			strlcpy( pdest, "Alerts: COMM_SERVER EXCEPTION", pdest_size );
			break;

			// ----------

		case MID_TO_COMMSERVER_FLOW_RECORDING_init_packet:
			strlcpy( pdest, "Sending Flow Recording", pdest_size );
			break;

		case MID_FROM_COMMSERVER_FLOW_RECORDING_full_receipt_ack:
			strlcpy( pdest, "Flow Recording Sent", pdest_size );
			break;

		case MID_FROM_COMMSERVER_FLOW_RECORDING_NAK:
			strlcpy( pdest, "Flow Recording: COMM_SERVER EXCEPTION", pdest_size );
			break;

			// ----------

		case MID_TO_COMMSERVER_CHECK_FOR_UPDATES_init_packet:
			strlcpy( pdest, "Checking for Updates", pdest_size );
			break;

		case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates:
			strlcpy( pdest, "No Updates Found", pdest_size );
			break;

		case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_one_update_available:
			strlcpy( pdest, "One Update Found", pdest_size );
			break;

		case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_two_updates_available:
			strlcpy( pdest, "Two Updates Found", pdest_size );
			break;

		case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_one_update_but_distribute_both:
			strlcpy( pdest, "Hub: one update, distribute both", pdest_size );
			break;

		case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_both:
			strlcpy( pdest, "Hub: up to date, distribute both", pdest_size );
			break;

		case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_main:
			strlcpy( pdest, "Hub: up to date, distribute main", pdest_size );
			break;

		case MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_tpmicro:
			strlcpy( pdest, "Hub: up to date, distribute tpmicro", pdest_size );
			break;

			// ----------

		case MID_TO_COMMSERVER_STATION_HISTORY_init_packet:
			strlcpy( pdest, "Sending Station History", pdest_size );
			break;

		case MID_FROM_COMMSERVER_STATION_HISTORY_full_receipt_ack:
			strlcpy( pdest, "Station History Sent", pdest_size );
			break;

		case MID_FROM_COMMSERVER_STATION_HISTORY_NAK:
			strlcpy( pdest, "Station History: COMM_SERVER EXCEPTION", pdest_size );
			break;

			// ----------

		case MID_TO_COMMSERVER_STATION_REPORT_DATA_init_packet:
			strlcpy( pdest, "Sending Station Report Data", pdest_size );
			break;

		case MID_FROM_COMMSERVER_STATION_REPORT_DATA_full_receipt_ack:
			strlcpy( pdest, "Station Report Data Sent", pdest_size );
			break;

		case MID_FROM_COMMSERVER_STATION_REPORT_DATA_NAK:
			strlcpy( pdest, "Station Report Data: COMM_SERVER EXCEPTION", pdest_size );
			break;

			// ----------

		case MID_TO_COMMSERVER_POC_REPORT_DATA_init_packet:
			strlcpy( pdest, "Sending POC Report Data", pdest_size );
			break;

		case MID_FROM_COMMSERVER_POC_REPORT_DATA_full_receipt_ack:
			strlcpy( pdest, "POC Report Data Sent", pdest_size );
			break;

		case MID_FROM_COMMSERVER_POC_REPORT_DATA_NAK:
			strlcpy( pdest, "POC Report Data: COMM_SERVER EXCEPTION", pdest_size );
			break;

			// ----------

		case MID_TO_COMMSERVER_MOISTURE_SENSOR_RECORDER_init_packet:
			strlcpy( pdest, "Sending Moisture Report Data", pdest_size );
			break;

		case MID_FROM_COMMSERVER_MOISTURE_SENSOR_RECORDER_full_receipt_ack:
			strlcpy( pdest, "Moisture Report Data Sent", pdest_size );
			break;

		case MID_FROM_COMMSERVER_MOISTURE_SENSOR_RECORDER_NAK:
			strlcpy( pdest, "Moisture Report Data: COMM_SERVER EXCEPTION", pdest_size );
			break;

			// ----------

		case MID_TO_COMMSERVER_SYSTEM_REPORT_DATA_init_packet:
			strlcpy( pdest, "Sending Mainline Report Data", pdest_size );
			break;

		case MID_FROM_COMMSERVER_SYSTEM_REPORT_DATA_full_receipt_ack:
			strlcpy( pdest, "Mainline Report Data Sent", pdest_size );
			break;

		case MID_FROM_COMMSERVER_SYSTEM_REPORT_DATA_NAK:
			strlcpy( pdest, "System Report Data: COMM_SERVER EXCEPTION", pdest_size );
			break;

			// ----------

		case MID_TO_COMMSERVER_LIGHTS_REPORT_DATA_init_packet:
			strlcpy( pdest, "Sending Lights Data", pdest_size );
			break;

		case MID_FROM_COMMSERVER_LIGHTS_REPORT_DATA_full_receipt_ack:
			strlcpy( pdest, "Lights Data Sent", pdest_size );
			break;

		case MID_FROM_COMMSERVER_LIGHTS_REPORT_DATA_NAK:
			strlcpy( pdest, "Lights Report Data: COMM_SERVER EXCEPTION", pdest_size );
			break;

			// ----------

		case MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet:
		case MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet2:
			strlcpy( pdest, "Sending Budget Report Data", pdest_size );
			break;

		case MID_FROM_COMMSERVER_BUDGET_REPORT_DATA_full_receipt_ack:
		case MID_FROM_COMMSERVER_BUDGET_REPORT_DATA_full_receipt_ack2:
			strlcpy( pdest, "Budget Report Data Sent", pdest_size );
			break;

		case MID_FROM_COMMSERVER_BUDGET_REPORT_DATA_NAK:
		case MID_FROM_COMMSERVER_BUDGET_REPORT_DATA_NAK2:
			strlcpy( pdest, "Budget Report Data: COMM SERVER EXCEPTION", pdest_size );
			break;

			// ----------

		case MID_TO_COMMSERVER_WEATHER_DATA_init_packet:
			strlcpy( pdest, "Sending ET and Rain", pdest_size );
			break;

		case MID_FROM_COMMSERVER_WEATHER_DATA_receipt_ack:
			strlcpy( pdest, "ET and Rain Sent", pdest_size );
			break;

		case MID_FROM_COMMSERVER_WEATHER_DATA_NAK:
			strlcpy( pdest, "ET-RAIN FOR TODAY: COMM_SERVER EXCEPTION", pdest_size );
			break;

			// ----------

		case MID_FROM_COMMSERVER_WEATHER_DATA_packet:
			strlcpy( pdest, "Receiving ET and Rain", pdest_size );
			break;

		case MID_TO_COMMSERVER_WEATHER_DATA_receipt_ack:
			strlcpy( pdest, "ET and Rain Received", pdest_size );
			break;

			// ----------

		case MID_TO_COMMSERVER_RAIN_INDICATION_init_packet:
			strlcpy( pdest, "Sending Rain Indication", pdest_size );
			break;

		case MID_FROM_COMMSERVER_RAIN_INDICATION_receipt_ack:
			strlcpy( pdest, "Rain Indication Sent", pdest_size );
			break;

		case MID_FROM_COMMSERVER_RAIN_INDICATION_NAK:
			strlcpy( pdest, "Rain Indication: COMM_SERVER EXCEPTION", pdest_size );
			break;

			// ----------

		case MID_FROM_COMMSERVER_RAIN_SHUTDOWN_packet:
			strlcpy( pdest, "Receiving Rain Shutdown", pdest_size );
			break;

		case MID_TO_COMMSERVER_RAIN_SHUTDOWN_receipt_ack:
			strlcpy( pdest, "Rain Shutdown Received", pdest_size );
			break;

			// ----------

		case MID_FROM_COMMSERVER_send_a_status_screen:
			strlcpy( pdest, "Request to begin with Mobile", pdest_size );
			break;

		case MID_TO_COMMSERVER_send_a_status_screen_ack :
			strlcpy( pdest, "Mobile Request processed", pdest_size );
			break;

			// ----------

		case MID_TO_COMMSERVER_status_screen_init_packet:
			strlcpy( pdest, "Sending Status Screen for Mobile", pdest_size );
			break;

		case MID_FROM_COMMSERVER_status_screen_ack_packet:
			strlcpy( pdest, "Status Screen for Mobile Sent", pdest_size );
			break;

			// ----------

		case MID_FROM_COMMSERVER_mobile_station_ON:
			strlcpy( pdest, "Mobile Station ON", pdest_size );
			break;

		case MID_TO_COMMSERVER_mobile_station_ON_ack:
			strlcpy( pdest, "Mobile Station ON rcvd", pdest_size );
			break;

		case MID_FROM_COMMSERVER_mobile_station_OFF:
			strlcpy( pdest, "Mobile Station OFF", pdest_size );
			break;

		case MID_TO_COMMSERVER_mobile_station_OFF_ack:
			strlcpy( pdest, "Mobile Station OFF rcvd", pdest_size );
			break;

			// ----------

			// 5/18/2015 rmd : Related to controller sending program data to the commserver.

		case MID_TO_COMMSERVER_VERIFY_FIRMWARE_VERSION_init_packet:
		case MID_TO_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_init_packet:
			strlcpy( pdest, "Checking Firmware Version", pdest_size );
			break;

		case MID_FROM_COMMSERVER_VERIFY_FIRMWARE_VERSION_ACK_FW_UP_TO_DATE:
		case MID_FROM_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_ACK_FW_UP_TO_DATE:
			strlcpy( pdest, "Firmware Up-to-Date", pdest_size );
			break;

		case MID_FROM_COMMSERVER_VERIFY_FIRMWARE_VERSION_ACK_FW_OUT_OF_DATE:
		case MID_FROM_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_ACK_FW_OUT_OF_DATE:
			strlcpy( pdest, "Firmware Out-of-Date", pdest_size );
			break;

		case MID_TO_COMMSERVER_PROGRAM_DATA_init_packet:
			strlcpy( pdest, "Sending Program Data", pdest_size );
			break;

		case MID_FROM_COMMSERVER_PROGRAM_DATA_receipt_ack:
			strlcpy( pdest, "Program Data Sent", pdest_size );
			break;

		case MID_FROM_COMMSERVER_PROGRAM_DATA_NAK:
			strlcpy( pdest, "PDATA to commserver: COMM_SERVER EXCEPTION", pdest_size );
			break;

			// ----------

			// 5/18/2015 rmd : Related to the commserver sending program data to the controller.

		case MID_FROM_COMMSERVER_IHSFY_program_data:
			strlcpy( pdest, "IHSFY Program Data", pdest_size );
			break;

		case MID_TO_COMMSERVER_PROGRAM_DATA_REQUEST_init_packet:
			strlcpy( pdest, "Requesting Program Data", pdest_size );
			break;

		case MID_FROM_COMMSERVER_PROGRAM_DATA_REQUEST_ACK_NO_CHANGES:
			strlcpy( pdest, "No PData Changes Coming", pdest_size );
			break;

		case MID_FROM_COMMSERVER_PROGRAM_DATA_REQUEST_ACK_CHANGES_AVAILABLE:
			strlcpy( pdest, "PData Changes Coming", pdest_size );
			break;

		case MID_FROM_COMMSERVER_PROGRAM_DATA_REQUEST_NAK:
			strlcpy( pdest, "PDATA REQUEST: COMM_SERVER EXCEPTION", pdest_size );
			break;

		case MID_FROM_COMMSERVER_PROGRAM_DATA_init_packet:
			strlcpy( pdest, "Receiving Program Data", pdest_size );
			break;

		case MID_TO_COMMSERVER_PROGRAM_DATA_receipt_ack:
			strlcpy( pdest, "Program Data Received", pdest_size );
			break;

			// ----------

		case MID_FROM_COMMSERVER_set_your_time_and_date:
			strlcpy( pdest, "Set Date & Time Received", pdest_size );
			break;

			// ----------

		case MID_FROM_COMMSERVER_panel_swap_factory_reset_to_new_panel:
			strlcpy( pdest, "New Panel Swap Factory RESET", pdest_size );
			break;

		case MID_TO_COMMSERVER_panel_swap_factory_reset_to_new_panel_ack :
			strlcpy( pdest, "New Panel Swap Factory RESET processed", pdest_size );
			break;

		case MID_FROM_COMMSERVER_panel_swap_factory_reset_to_old_panel:
			strlcpy( pdest, "Old Panel Swap Factory RESET", pdest_size );
			break;

		case MID_TO_COMMSERVER_panel_swap_factory_reset_to_old_panel_ack :
			strlcpy( pdest, "Old Panel Swap Factory RESET processed", pdest_size );
			break;

		case MID_FROM_COMMSERVER_set_all_bits:
			strlcpy( pdest, "Receiving Send All Program Data", pdest_size );
			break;

		case MID_TO_COMMSERVER_set_all_bits_ack :
			strlcpy( pdest, "Send All Program Data Received", pdest_size );
			break;

			// ----------

		case MID_TO_COMMSERVER_ET_AND_RAIN_TABLE_init_packet:
			strlcpy( pdest, "Sending ET/Rain Table", pdest_size );
			break;

		case MID_FROM_COMMSERVER_ET_AND_RAIN_TABLE_full_receipt_ack:
			strlcpy( pdest, "ET/Rain Table Sent", pdest_size );
			break;

		case MID_FROM_COMMSERVER_ET_AND_RAIN_TABLE_NAK:
			strlcpy( pdest, "ET/Rain Table: COMM_SERVER EXCEPTION", pdest_size );
			break;

			// ----------

		case MID_FROM_COMMSERVER_mobile_MVOR:
			strlcpy( pdest, "Mobile MVOR Request", pdest_size );
			break;

			// ----------

		case MID_FROM_COMMSERVER_mobile_light_cmd:
			strlcpy( pdest, "Mobile Lights Request", pdest_size );
			break;

			// ----------

		case MID_FROM_COMMSERVER_mobile_turn_controller_on_off:
			strlcpy( pdest, "Mobile On/Off Request", pdest_size );
			break;

			// ----------

		case MID_FROM_COMMSERVER_mobile_now_days_all_stations:
			strlcpy( pdest, "Mobile No Water Days for All Stations", pdest_size );
			break;

		case MID_FROM_COMMSERVER_mobile_now_days_by_group:
			strlcpy( pdest, "Mobile No Water Days by Group", pdest_size );
			break;

		case MID_FROM_COMMSERVER_mobile_now_days_by_box:
			strlcpy(pdest, "Mobile No Water Days by Controller", pdest_size);
			break;

		case MID_FROM_COMMSERVER_mobile_now_days_by_station:
			strlcpy( pdest, "Mobile No Water Days by Station", pdest_size );
			break;

			// ----------

		case MID_FROM_COMMSERVER_force_registration:
			strlcpy( pdest, "Registration Request", pdest_size );
			break;

			// ----------

		case MID_FROM_COMMSERVER_enable_or_disable_FL_option:
			strlcpy( pdest, "Set CS3-FL Option", pdest_size );
			break;

			// ----------

		case MID_FROM_COMMSERVER_clear_MLB:
			strlcpy( pdest, "Clear Mainline Break", pdest_size );
			break;

			// ----------

		case MID_FROM_COMMSERVER_stop_all_irrigation:
			strlcpy( pdest, "Stop All Irrigation", pdest_size );
			break;

			// ----------

		case MID_FROM_COMMSERVER_stop_irrigation:
			strlcpy( pdest, "Stop Irrigation", pdest_size );
			break;

			// ----------

		case MID_FROM_COMMSERVER_check_for_updates:
			strlcpy( pdest, "Check for Updates", pdest_size );
			break;

			// ----------

		case MID_FROM_COMMSERVER_perform_two_wire_discovery:
			strlcpy(pdest, "Perform 2-Wire Discovery", pdest_size);
			break;

			// ----------

		case MID_FROM_COMMSERVER_mobile_acquire_expected_flow_by_station:
			strlcpy(pdest, "Mobile Acquire Expected Request", pdest_size);
			break;

			// ----------

		case MID_FROM_COMMSERVER_enable_or_disable_HUB_option:
			strlcpy( pdest, "Set CS3-HUB Option", pdest_size );
			break;

			// ----------

		case MID_FROM_COMMSERVER_go_ahead_send_your_messages:
			strlcpy(pdest, "Hub Poll (send your messages)", pdest_size);
			break;


		case MID_TO_COMMSERVER_no_more_messages_init_packet:
			strlcpy( pdest, "Sending End of Poll Period (no more msgs)", pdest_size );
			break;

		case MID_FROM_COMMSERVER_no_more_messages_ack:
			strlcpy( pdest, "End of Poll Sent", pdest_size );
			break;


		case MID_TO_COMMSERVER_hub_is_busy_init_packet:
			strlcpy( pdest, "Sending Hub is Busy (no more msgs)", pdest_size );
			break;

		case MID_FROM_COMMSERVER_hub_is_busy_ack:
			strlcpy( pdest, "Hub is Busy Sent", pdest_size );
			break;

		// ----------
		
		case MID_FROM_COMMSERVER_here_is_the_hub_list:
			strlcpy( pdest, "Receiving Hub List", pdest_size );
			break;
			
		case MID_TO_COMMSERVER_here_is_the_hub_list_ack:
			strlcpy( pdest, "Hub List Received", pdest_size );
			break;

		// ----------
		
		case MID_FROM_COMMSERVER_stop_and_cancel_hub_code_distribution:
			strlcpy( pdest, "Stop Hub Code Distribution", pdest_size );
			break;
			
		case MID_TO_COMMSERVER_stop_and_cancel_hub_code_distribution_ack:
			strlcpy( pdest, "Stop Hub Code Distribution", pdest_size );
			break;

		// ----------
		
		case MID_FROM_COMMSERVER_start_the_hub_code_distribution:
			strlcpy( pdest, "Start Hub Code Distribution", pdest_size );
			break;
			
		case MID_TO_COMMSERVER_start_the_hub_code_distribution_ack:
			strlcpy( pdest, "Start Hub Code Distribution", pdest_size );
			break;

		// ----------
		
		case MID_FROM_COMMSERVER_send_engineering_alerts:
			strlcpy( pdest, "Request to Send Alerts", pdest_size );
			break;

		// ----------

		case MID_FROM_COMMSERVER_enable_or_disable_AQUAPONICS_option:
			strlcpy( pdest, "Set CS3-AQUAPONICS Option", pdest_size );
			break;

		/* ---------------------------------------------------------- */

		// 3/31/2014 rmd : Note the code distribution alerts are so thorough we do not need yet
		// another about the INIT or DATA packet command being rcvd. So do not include here. They'll
		// never be used.

		// ----------

		default:
			snprintf( pdest, pdest_size, "Unknown Command (%d)", mid );
			break;
	}
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_COMM_COMMAND_RCVD_OR_ISSUED_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	COMM_COMMANDS mid;

	UNS_32 rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &mid, pindex_ptr, sizeof( COMM_COMMANDS ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		ALERTS_parse_comm_command_string( mid, pdest, pdest_size );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_COMM_COMMAND_FAILED_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_32 pci_event;

	UNS_32 rv;

	// ----------

	rv = 0;

	// ----------

	rv += ALERTS_pull_object_off_pile( ppile, &pci_event, pindex_ptr, sizeof(UNS_32) );

	// ----------

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		switch( pci_event )
		{
			case CI_EVENT_cts_error_make_an_alert:
				strlcpy( pdest, "Message failed: CTS Timeout", pdest_size );
				break;
				
			case CI_EVENT_connection_process_error:
				strlcpy( pdest, "Message failed: Unable to Connect", pdest_size );
				break;

			case CI_EVENT_message_termination_due_to_time:
				strlcpy( pdest, "Message failed: No response timeout", pdest_size );
				break;

			case CI_EVENT_message_termination_due_to_new_inbound_message:
				strlcpy( pdest, "Message failed: No response, new inbound arrived", pdest_size );
				break;

			default:
				snprintf( pdest, pdest_size, "Message failed: Unknown reason %d", pci_event );
				break;
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_outbound_message_size_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16  lmid;

	UNS_32  lsize;

	UNS_16  lpackets;

	UNS_32  rv;

	// ----------

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lmid, pindex_ptr, sizeof(lmid) );
	rv += ALERTS_pull_object_off_pile( ppile, &lsize, pindex_ptr, sizeof(lsize) );
	rv += ALERTS_pull_object_off_pile( ppile, &lpackets, pindex_ptr, sizeof(lpackets) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Outbound Msg:  Size: %d   Packets: %d", lsize, lpackets );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_inbound_message_size_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16  lmid;

	UNS_32  lsize;

	UNS_16  lpackets;

	UNS_32  rv;

	// ----------

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lmid, pindex_ptr, sizeof(lmid) );
	rv += ALERTS_pull_object_off_pile( ppile, &lsize, pindex_ptr, sizeof(lsize) );
	rv += ALERTS_pull_object_off_pile( ppile, &lpackets, pindex_ptr, sizeof(lpackets) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "From CommServer:  Size: %d   Packets: %d", lsize, lpackets );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_device_powered_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	static const char* on_off_str[ ] =
	{
		"down",
		"up"
	};

	char    ldevice_name_64[ 64 ];

	UNS_8   lport;

	UNS_8   lon_or_off;

	UNS_32  rv;

	// ----------

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lport, pindex_ptr, sizeof(lport) );
	rv += ALERTS_pull_object_off_pile( ppile, &lon_or_off, pindex_ptr, sizeof(lon_or_off) );
	rv += ALERTS_pull_string_off_pile( ppile, ldevice_name_64, sizeof(ldevice_name_64), pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Port %c: %s powered %s", lport == UPORT_A ? 'A' : 'B', ldevice_name_64, on_off_str[ lon_or_off ] );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_comm_crc_failed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lport;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lport, pindex_ptr, sizeof(lport) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Data damaged during transmission (CRC failed): %s", port_names[ lport ] );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_cts_timeout_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	static const char *cts_timeout_reason_text[ ] =
	{
		"idle",
		"waiting",
		"transmitting"
	};

	REASONS_FOR_CTS_TIMEOUT lreason;

	UNS_8   lport;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lport, pindex_ptr, sizeof(lport) );
	rv += ALERTS_pull_object_off_pile( ppile, &lreason, pindex_ptr, sizeof(lreason) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "CTS Timeout while %s: %s", cts_timeout_reason_text[ lreason ], port_names[ lport ] );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_comm_packet_greater_than_512_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lport;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lport, pindex_ptr, sizeof(lport) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Data damaged during transmission (packet > %d): %s", SYSTEM_WIDE_LARGEST_AMBLE_TO_AMBLE_PACKET_SIZE, port_names[ lport ] );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_comm_status_timer_expired_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lcontroller_index;

	UNS_32  rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_index, pindex_ptr, sizeof(UNS_8) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "No response from Controller %c (status timer expired)", (lcontroller_index + 'A') );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_msg_response_timeout_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lcontroller_index;

	UNS_32  rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_index, pindex_ptr, sizeof(UNS_8) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "No response from Controller %c (resp timer expired)", (lcontroller_index + 'A') );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_comm_mngr_blocked_msg_during_idle_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char str_128[ 128 ];

	UNS_8   pcomm_mngr_mode;

	UNS_8   ppending_device_exchange_request;

	UNS_8   ppending_scan;

	UNS_8   ppending_code_receipt;

	UNS_32  rv;

	// ----------

	rv = 0;

	// ----------

	rv += ALERTS_pull_object_off_pile( ppile, &pcomm_mngr_mode, pindex_ptr, sizeof(UNS_8) );
	rv += ALERTS_pull_object_off_pile( ppile, &ppending_device_exchange_request, pindex_ptr, sizeof(UNS_8) );
	rv += ALERTS_pull_object_off_pile( ppile, &ppending_scan, pindex_ptr, sizeof(UNS_8) );
	rv += ALERTS_pull_object_off_pile( ppile, &ppending_code_receipt, pindex_ptr, sizeof(UNS_8) );

	// ----------

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		memset( str_128, 0x00, sizeof(str_128) );

		switch( pcomm_mngr_mode )
		{
			case COMM_MNGR_MODE_tpmicro_isp:
				strlcpy( str_128, "ISP", sizeof(str_128) );
				break;
			case COMM_MNGR_MODE_scanning:
				strlcpy( str_128, "Scanning", sizeof(str_128) );
				break;
			case COMM_MNGR_MODE_operational:
				strlcpy( str_128, "Operational", sizeof(str_128) );
				break;
			case COMM_MNGR_MODE_device_exchange:
				strlcpy( str_128, "Device Exch", sizeof(str_128) );
				break;

			default:
				strlcpy( str_128, "Unk Mode", sizeof(str_128) );
		}

		if( ppending_device_exchange_request )
		{
			strlcat( str_128, " (Device Exch Pending)", sizeof(str_128) );
		}

		if( ppending_scan )
		{
			strlcat( str_128, " (Scan Pending)", sizeof(str_128) );
		}

		if( ppending_code_receipt )
		{
			strlcat( str_128, " (Code Receipt Pending)", sizeof(str_128) );
		}

		snprintf( pdest, pdest_size, "CI: During IDLE comm_mngr block msg transmission: %s", str_128 );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_range_check_failed_bool_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr, const BOOL_32 pincludes_group_name, const BOOL_32 pincludes_location )
{
	char    lvariable_name_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	char    lgroup_name_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	char    lfilename_64[ 64 ];

	UNS_8   lvalue_bool, ldefault_bool;

	UNS_32  lline_num;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, lvariable_name_48, sizeof(lvariable_name_48), pindex_ptr );

	if( pincludes_group_name == ALERT_INCLUDES_GROUP_NAME )
	{
		rv += ALERTS_pull_string_off_pile( ppile, lgroup_name_48, sizeof(lgroup_name_48), pindex_ptr );
	}

	rv += ALERTS_pull_object_off_pile( ppile, &lvalue_bool, pindex_ptr, sizeof(lvalue_bool) );
	rv += ALERTS_pull_object_off_pile( ppile, &ldefault_bool, pindex_ptr, sizeof(ldefault_bool) );

	if( pincludes_location == ALERT_INCLUDES_FILENAME )
	{
		rv += ALERTS_pull_string_off_pile( ppile, (char*)&lfilename_64, sizeof(lfilename_64), pindex_ptr );
		rv += ALERTS_pull_object_off_pile( ppile, &lline_num, pindex_ptr, sizeof(lline_num) );
	}

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( pincludes_group_name == ALERT_INCLUDES_GROUP_NAME )
		{
			snprintf( pdest, pdest_size, "%s for %s out of range (%d); reset to %s", lvariable_name_48, lgroup_name_48, lvalue_bool, GetNoYesStr( ldefault_bool ) );
		}
		else
		{
			snprintf( pdest, pdest_size, "%s out of range (%d); reset to %s", lvariable_name_48, lvalue_bool, GetNoYesStr( ldefault_bool ) );
		}

		if( pincludes_location == ALERT_INCLUDES_FILENAME )
		{
			sp_strlcat( pdest, pdest_size, ": %s:%lu", lfilename_64, lline_num );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_range_check_failed_date_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr, const BOOL_32 pincludes_group_name, const BOOL_32 pincludes_location )
{
	char    lvariable_name_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	char    lgroup_name_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	char    lfilename_64[ 64 ];

	char    ldate_str_32[ 32 ];

	UNS_16  ldate;

	UNS_32  lline_num;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, lvariable_name_48, sizeof(lvariable_name_48), pindex_ptr );

	if( pincludes_group_name == ALERT_INCLUDES_GROUP_NAME )
	{
		rv += ALERTS_pull_string_off_pile( ppile, lgroup_name_48, sizeof(lgroup_name_48), pindex_ptr );
	}

	rv += ALERTS_pull_object_off_pile( ppile, &ldate, pindex_ptr, sizeof(ldate) );

	if( pincludes_location == ALERT_INCLUDES_FILENAME )
	{
		rv += ALERTS_pull_string_off_pile( ppile, (char*)&lfilename_64, sizeof(lfilename_64), pindex_ptr );
		rv += ALERTS_pull_object_off_pile( ppile, &lline_num, pindex_ptr, sizeof(lline_num) );
	}

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( pincludes_group_name == ALERT_INCLUDES_GROUP_NAME )
		{
			snprintf( pdest, pdest_size, "%s for %s out of range; reset to %s", lvariable_name_48, lgroup_name_48, GetDateStr(ldate_str_32, sizeof(ldate_str_32), ldate, DATESTR_show_long_year, DATESTR_show_dow_not) );
		}
		else
		{
			snprintf( pdest, pdest_size, "%s out of range; reset to %s", lvariable_name_48, GetDateStr(ldate_str_32, sizeof(ldate_str_32), ldate, DATESTR_show_long_year, DATESTR_show_dow_not) );
		}

		if( pincludes_location == ALERT_INCLUDES_FILENAME )
		{
			sp_strlcat( pdest, pdest_size, ": %s:%lu", lfilename_64, lline_num );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_range_check_failed_float_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr, const BOOL_32 pincludes_group_name, const BOOL_32 pincludes_location )
{
	char    lvariable_name_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	char    lgroup_name_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	char    lfilename_64[ 64 ];

	float   lvalue_float, ldefault_float;

	UNS_32  lline_num;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, lvariable_name_48, sizeof(lvariable_name_48), pindex_ptr );

	if( pincludes_group_name == ALERT_INCLUDES_GROUP_NAME )
	{
		rv += ALERTS_pull_string_off_pile( ppile, lgroup_name_48, sizeof(lgroup_name_48), pindex_ptr );
	}

	rv += ALERTS_pull_object_off_pile( ppile, &lvalue_float, pindex_ptr, sizeof(lvalue_float) );
	rv += ALERTS_pull_object_off_pile( ppile, &ldefault_float, pindex_ptr, sizeof(ldefault_float) );

	if( pincludes_location == ALERT_INCLUDES_FILENAME )
	{
		rv += ALERTS_pull_string_off_pile( ppile, (char*)&lfilename_64, sizeof(lfilename_64), pindex_ptr );
		rv += ALERTS_pull_object_off_pile( ppile, &lline_num, pindex_ptr, sizeof(lline_num) );
	}

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( pincludes_group_name == ALERT_INCLUDES_GROUP_NAME )
		{
			snprintf( pdest, pdest_size, "%s for %s out of range (%7.3f); reset to %7.3f", lvariable_name_48, lgroup_name_48, (double)lvalue_float, (double)ldefault_float );
		}
		else
		{
			snprintf( pdest, pdest_size, "%s out of range (%7.3f); reset to %7.3f", lvariable_name_48, (double)lvalue_float, (double)ldefault_float );
		}

		if( pincludes_location == ALERT_INCLUDES_FILENAME )
		{
			sp_strlcat( pdest, pdest_size, ": %s:%lu", lfilename_64, lline_num );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_range_check_failed_int32_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr, const BOOL_32 pincludes_group_name, const BOOL_32 pincludes_location )
{
	char    lvariable_name_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	char    lgroup_name_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	char    lfilename_64[ 64 ];

	INT_32  lvalue_int32, ldefault_int32;

	UNS_32  lline_num;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, lvariable_name_48, sizeof(lvariable_name_48), pindex_ptr );

	if( pincludes_group_name == ALERT_INCLUDES_GROUP_NAME )
	{
		rv += ALERTS_pull_string_off_pile( ppile, lgroup_name_48, sizeof(lgroup_name_48), pindex_ptr );
	}

	rv += ALERTS_pull_object_off_pile( ppile, &lvalue_int32, pindex_ptr, sizeof(lvalue_int32) );
	rv += ALERTS_pull_object_off_pile( ppile, &ldefault_int32, pindex_ptr, sizeof(ldefault_int32) );

	if( pincludes_location == ALERT_INCLUDES_FILENAME )
	{
		rv += ALERTS_pull_string_off_pile( ppile, (char*)&lfilename_64, sizeof(lfilename_64), pindex_ptr );
		rv += ALERTS_pull_object_off_pile( ppile, &lline_num, pindex_ptr, sizeof(lline_num) );
	}

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( pincludes_group_name == ALERT_INCLUDES_GROUP_NAME )
		{
			snprintf( pdest, pdest_size, "%s for %s out of range (%d); reset to %d", lvariable_name_48, lgroup_name_48, lvalue_int32, ldefault_int32 );
		}
		else
		{
			snprintf( pdest, pdest_size, "%s out of range (%d); reset to %d", lvariable_name_48, lvalue_int32, ldefault_int32 );
		}

		if( pincludes_location == ALERT_INCLUDES_FILENAME )
		{
			sp_strlcat( pdest, pdest_size, ": %s:%lu", lfilename_64, lline_num );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_range_check_failed_string_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr, const BOOL_32 pincludes_location )
{
	char    lvariable_name_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	char    lstr_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	char    lfilename_64[ 64 ];

	UNS_32  lline_num;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, lvariable_name_48, sizeof(lvariable_name_48), pindex_ptr );
	rv += ALERTS_pull_string_off_pile( ppile, lstr_48, sizeof(lstr_48), pindex_ptr );

	if( pincludes_location == ALERT_INCLUDES_FILENAME )
	{
		rv += ALERTS_pull_string_off_pile( ppile, (char*)&lfilename_64, sizeof(lfilename_64), pindex_ptr );
		rv += ALERTS_pull_object_off_pile( ppile, &lline_num, pindex_ptr, sizeof(lline_num) );
	}

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "%s contains invalid characters; invalid chars replaced with '?'", lvariable_name_48 );

		if( pincludes_location == ALERT_INCLUDES_FILENAME )
		{
			sp_strlcat( pdest, pdest_size, ": %s:%lu", lfilename_64, lline_num );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_range_check_failed_string_too_long_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char    str_64[ 64 ];

	UNS_32  lallowed_length;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_64, sizeof(str_64), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lallowed_length, pindex_ptr, sizeof(lallowed_length) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "%s truncated to %d chars", str_64, lallowed_length );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_range_check_failed_time_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr, const BOOL_32 pincludes_group_name, const BOOL_32 pincludes_location )
{
	char    lvariable_name_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	char    lgroup_name_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	char    lfilename_64[ 64 ];

	char    ltime_str_32[ 32 ];

	UNS_32  ltime;

	UNS_32  lline_num;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, lvariable_name_48, sizeof(lvariable_name_48), pindex_ptr );

	if( pincludes_group_name == ALERT_INCLUDES_GROUP_NAME )
	{
		rv += ALERTS_pull_string_off_pile( ppile, lgroup_name_48, sizeof(lgroup_name_48), pindex_ptr );
	}

	rv += ALERTS_pull_object_off_pile( ppile, &ltime, pindex_ptr, sizeof(ltime) );

	if( pincludes_location == ALERT_INCLUDES_FILENAME )
	{
		rv += ALERTS_pull_string_off_pile( ppile, (char*)&lfilename_64, sizeof(lfilename_64), pindex_ptr );
		rv += ALERTS_pull_object_off_pile( ppile, &lline_num, pindex_ptr, sizeof(lline_num) );
	}

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( pincludes_group_name == ALERT_INCLUDES_GROUP_NAME )
		{
			snprintf( pdest, pdest_size, "%s for %s out of range; reset to %s", lvariable_name_48, lgroup_name_48, TDUTILS_time_to_time_string_with_ampm(ltime_str_32, sizeof(ltime_str_32), ltime, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING) );
		}
		else
		{
			snprintf( pdest, pdest_size, "%s out of range; reset to %s", lvariable_name_48, TDUTILS_time_to_time_string_with_ampm(ltime_str_32, sizeof(ltime_str_32), ltime, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING) );
		}

		if( pincludes_location == ALERT_INCLUDES_FILENAME )
		{
			sp_strlcat( pdest, pdest_size, ": %s:%lu", lfilename_64, lline_num );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_range_check_failed_uint32_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr, const BOOL_32 pincludes_group_name, const BOOL_32 pincludes_location )
{
	char    lvariable_name_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	char    lgroup_name_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	char    lfilename_64[ 64 ];

	UNS_32  lvalue, ldefault;

	UNS_32  lline_num;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, lvariable_name_48, sizeof(lvariable_name_48), pindex_ptr );

	if( pincludes_group_name == ALERT_INCLUDES_GROUP_NAME )
	{
		rv += ALERTS_pull_string_off_pile( ppile, lgroup_name_48, sizeof(lgroup_name_48), pindex_ptr );
	}

	rv += ALERTS_pull_object_off_pile( ppile, &lvalue, pindex_ptr, sizeof(lvalue) );
	rv += ALERTS_pull_object_off_pile( ppile, &ldefault, pindex_ptr, sizeof(ldefault) );

	if( pincludes_location == ALERT_INCLUDES_FILENAME )
	{
		rv += ALERTS_pull_string_off_pile( ppile, (char*)&lfilename_64, sizeof(lfilename_64), pindex_ptr );
		rv += ALERTS_pull_object_off_pile( ppile, &lline_num, pindex_ptr, sizeof(lline_num) );
	}

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( pincludes_group_name == ALERT_INCLUDES_GROUP_NAME )
		{
			snprintf( pdest, pdest_size, "%s for %s out of range (%d); reset to %d", lvariable_name_48, lgroup_name_48, lvalue, ldefault );
		}
		else
		{
			snprintf( pdest, pdest_size, "%s out of range (%d); reset to %d", lvariable_name_48, lvalue, ldefault );
		}

		if( pincludes_location == ALERT_INCLUDES_FILENAME )
		{
			sp_strlcat( pdest, pdest_size, ": %s:%lu", lfilename_64, lline_num );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_derate_table_update_failed_off_pile( const UNS_32 paid, ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16  lmeasured;

	UNS_16  lexpected;

	char    str_128[ 128 ];

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lmeasured, pindex_ptr, sizeof(lmeasured) );
	rv += ALERTS_pull_object_off_pile( ppile, &lexpected, pindex_ptr, sizeof(lexpected) );
	rv += ALERTS_pull_string_off_pile( ppile, str_128, sizeof(str_128), pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( paid == AID_DERATE_TABLE_UPDATE_FAILED_NO_FLOW )
		{
			snprintf( pdest, pdest_size, "Unable to update: measured 0 gpm, expected %u - valves ON are%s", lexpected, str_128 );
		}
		else if( paid == AID_DERATE_TABLE_UPDATE_FAILED_30_PCT_DIFF )
		{
			snprintf( pdest, pdest_size, "Unable to update: (differ by more than 30%%) exp %u, meas %u - valves ON are%s", lexpected, lmeasured, str_128 );
		}
		else // if( paid == AID_DERATE_TABLE_UPDATE_FAILED_50_PCT_DIFF )
		{
			snprintf( pdest, pdest_size, "Unable to update: (differ by more than 50%%) exp %u, meas %u - valves ON are%s", lexpected, lmeasured, str_128 );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_derate_table_update_successful_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_32 lvalves_on;

	UNS_16 lflow_rate;

	UNS_16 lindex;

	UNS_8 lcount_from;

	UNS_8 lcount_to;

	INT_16 lvalue_from;

	INT_16 lvalue_to;

	UNS_32  rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &lvalves_on, pindex_ptr, sizeof(UNS_32) );
	rv += ALERTS_pull_object_off_pile( ppile, &lflow_rate, pindex_ptr, sizeof(UNS_16) );
	rv += ALERTS_pull_object_off_pile( ppile, &lindex, pindex_ptr, sizeof(UNS_16) );
	rv += ALERTS_pull_object_off_pile( ppile, &lcount_from, pindex_ptr, sizeof(UNS_8) );
	rv += ALERTS_pull_object_off_pile( ppile, &lcount_to, pindex_ptr, sizeof(UNS_8) );
	rv += ALERTS_pull_object_off_pile( ppile, &lvalue_from, pindex_ptr, sizeof(INT_16) );
	rv += ALERTS_pull_object_off_pile( ppile, &lvalue_to, pindex_ptr, sizeof(INT_16) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Table Update: [%u valve(s) on, %u gpm], index: %u, count: %u to %u, value: %5.1f to %5.1f", lvalves_on, lflow_rate, lindex, lcount_from, lcount_to, (lvalue_from/10.0), (lvalue_to/10.0) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_derate_table_update_station_count_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lcontroller_index;

	UNS_16  lstation_number_0_u16;

	UNS_32  lcount;

	char    str_8[ 8 ];

	UNS_32  rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_index, pindex_ptr, sizeof(UNS_8) );
	rv += ALERTS_pull_object_off_pile( ppile, &lstation_number_0_u16, pindex_ptr, sizeof(lstation_number_0_u16) );
	rv += ALERTS_pull_object_off_pile( ppile, &lcount, pindex_ptr, sizeof(lcount) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Updating: Increment Station Count: sta %s to %u", ALERTS_get_station_number_with_or_without_two_wire_indicator( lcontroller_index, lstation_number_0_u16, (char*)&str_8, sizeof(str_8) ), lcount );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_mainline_break_cleared_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_32  lsystem_gid;

	UNS_32  rv;

	rv = 0;

	// 10/29/2013 ajv : For now, although it's included in the alert definition,
	// don't display the system GID. It can be used later to display the name of
	// the system for which the mainline break was cleared. However, we need to
	// extract it to keep the pointer intact.
	rv += ALERTS_pull_object_off_pile( ppile, &lsystem_gid, pindex_ptr, sizeof(lsystem_gid) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		strlcpy( pdest, "Mainline Break Cleared", pdest_size );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Builds the string for the mainline break in pdest. It needs to keep pindex
 * current to the next place the data comes from.
 * 
 * @executed 
 * 
 * @param pparse_why The reason for parsing the alert. This can either be
 *  				 PARSE_FOR_LENGTH_ONLY or PARSE_FOR_LENGTH_AND_TEXT.
 * @param pdest Points to a string and can be concatenated to as a string
 * @param pdest_size The available space to place the text into.
 * @param pindex_ptr A pointer to the index in the pile to the start of the
 *        alert.
 * 
 * @return UNS_32 The byte count of the parsed alert. This count is needed to
 *                move ahead the FIRST index when adding an alert.
 *
 * @author 5/18/2012 Adrianusv
 *
 * @revisions
 *    5/18/2012 Initial release
 */
static UNS_32 ALERTS_pull_mainline_break_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char    pre_str_32[ 32 ];

	char    system_name_str_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	char    during_str_16[ 16 ];

	UNS_16  lmeasured_u16, lallowed_u16;

	UNS_8   lduring_type_u8;

	UNS_32  rv;

	rv = 0;

	// 4/29/2015 rmd : Get the PRE string. Which when the alert is initially made is a NULL
	// string. At mid-night this contains the REMINDER phrase.
	rv += ALERTS_pull_string_off_pile( ppile, pre_str_32, sizeof(pre_str_32), pindex_ptr );

	// 4/29/2015 rmd : Pull the SYSTEM name. This may truncate the system name it potentially
	// being 48 characters. But truncates SAFELY so okay.
	rv += ALERTS_pull_string_off_pile( ppile, system_name_str_48, sizeof(system_name_str_48), pindex_ptr );

	// 4/29/2015 rmd : Pull during irrigation, MVOR, or IDLE.
	rv += ALERTS_pull_object_off_pile( ppile, &lduring_type_u8, pindex_ptr, sizeof(UNS_8) );

	rv += ALERTS_pull_object_off_pile( ppile, &lmeasured_u16, pindex_ptr, sizeof(UNS_16) );

	rv += ALERTS_pull_object_off_pile( ppile, &lallowed_u16, pindex_ptr, sizeof(UNS_16) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		// Needs a NULL char to initialize the string.
		during_str_16[ 0 ] = 0;

		if( lduring_type_u8 == MLB_TYPE_DURING_IRRIGATION )
		{
			strlcpy( during_str_16, "IRRIGATING", sizeof(during_str_16) );
		}
		else
			if( lduring_type_u8 == MLB_TYPE_DURING_MVOR )
		{
			strlcpy( during_str_16, "MV OVERRIDE", sizeof(during_str_16) );
		}
		else
			if( lduring_type_u8 == MLB_TYPE_DURING_ALL_OTHER_TIMES )
		{
			strlcpy( during_str_16, "NOT IRRIGATING", sizeof(during_str_16) );
		}
		else
		{
			strlcpy( during_str_16, "<parse error>", sizeof(during_str_16) );
		}

		snprintf( pdest, pdest_size, "MAINLINE BREAK %s: \"%s\" while %s: saw %d gpm, allowed %d gpm", pre_str_32, system_name_str_48, during_str_16, lmeasured_u16, lallowed_u16 );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_no_current_master_valve_or_pump_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr, const UNS_32 paid )
{
	UNS_8   box_index_0;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof( box_index_0 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		// 9/28/2015 ajv : Only show the controller letter if the controller is part of a FLOWSENSE
		// chain.
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "NO ELECTRICAL CURRENT: Controller %c %s", (box_index_0 + 'A'), (paid == AID_CONVENTIONAL_MV_NO_CURRENT) ? "Master Valve" : "Pump"  );
		}
		else
		{
			snprintf( pdest, pdest_size, "NO ELECTRICAL CURRENT: %s", (paid == AID_CONVENTIONAL_MV_NO_CURRENT) ? "Master Valve" : "Pump"  );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_no_current_station_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   box_index_0;

	UNS_16  station_0_u16;

	char str_16[ 16 ];

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof( box_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &station_0_u16, pindex_ptr, sizeof( station_0_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "NO ELECTRICAL CURRENT: Station %s", ALERTS_get_station_number_with_or_without_two_wire_indicator( box_index_0, station_0_u16, (char*)&str_16, sizeof(str_16) ) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_short_station_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   box_index_0;

	UNS_16  station_0_u16;

	char    str_16[ 16 ];

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof(UNS_8) );
	rv += ALERTS_pull_object_off_pile( ppile, &station_0_u16, pindex_ptr, sizeof( station_0_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "SOLENOID SHORT: Station %s", ALERTS_get_station_number_with_or_without_two_wire_indicator( box_index_0, station_0_u16, (char*)&str_16, sizeof(str_16) ) );
	}

	return( rv );
}

#if 0
/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_Short_LIGHT_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 station_0_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &station_0_u16, pindex_ptr, sizeof( station_0_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "SHORT: Lights %d suspected", (station_0_u16 + 1) );
	}

	return( rv );
}
#endif

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_short_station_unknown( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   box_index_0;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof(UNS_8) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "ELECTRICAL SHORT UNKNOWN OUTPUT: Controller %c", (box_index_0 + 'A') );
		}
		else
		{
			snprintf( pdest, pdest_size, "ELECTRICAL SHORT UNKNOWN OUTPUT" );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_conventional_MV_short_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lbox_index_0;

	UNS_32  rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "SOLENOID SHORT: Master Valve at Controller %c", (lbox_index_0 + 'A') );
		}
		else
		{
			strlcpy( pdest, "SOLENOID SHORT: Master Valve", pdest_size );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_conventional_PUMP_short_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lbox_index_0;

	UNS_32  rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() == (true) )
		{
			snprintf( pdest, pdest_size, "ELECTRICAL SHORT: Pump at Controller %c", (lbox_index_0 + 'A') );
		}
		else
		{
			strlcpy( pdest, "ELECTRICAL SHORT: Pump", pdest_size );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_flow_not_checked_no_reasons_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   box_index_0;

	UNS_16  station_number_0;

	char    str_16[ 16 ];

	UNS_32  rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof(box_index_0) );
	rv += ALERTS_pull_object_off_pile( ppile, &station_number_0, pindex_ptr, sizeof(station_number_0) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Unable to check flow: Sta %s", ALERTS_get_station_number_with_or_without_two_wire_indicator( box_index_0, station_number_0, (char*)&str_16, sizeof(str_16) ) );
	}

	return( rv );
}

#if 0
/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_FlowCheckLearning_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8 addr_u8;

	UNS_16 station_0_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &addr_u8, pindex_ptr, sizeof( addr_u8 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &station_0_u16, pindex_ptr, sizeof( station_0_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Learning to Check Flow: Station %c%02d", addr_u8, (station_0_u16 + 1) );
	}

	return( rv );
}
#endif

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_flow_error_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, const UNS_32 paid, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	// This function is responsible to build the string for the high flow alert
	// in pdest. It needs to keep pindex_ptr current to the next place the data
	// comes from. Must return how many bytes it pulls off of the pile for this
	// alert.
	//
	// pdest points to a string and can be concatenated to as a string

	UNS_8 reason_in_list_u8;

	UNS_8  box_index_0;
	UNS_16 station_0_u16;

	UNS_16 system_expected_u16;
	UNS_16 system_derated_expected_u16;
	UNS_16 system_limit_u16;
	UNS_16 system_actual_flow_u16;

	UNS_16 station_derated_expected_u16;
	UNS_16 station_share_of_actual_u16;
	UNS_16 stations_on_u16;

	char str_32[ 32 ];

	char str_16[ 16 ];

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &reason_in_list_u8, pindex_ptr, sizeof( reason_in_list_u8 ) );

	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof( box_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &station_0_u16, pindex_ptr, sizeof( station_0_u16 ) );

	rv += ALERTS_pull_object_off_pile( ppile, &system_expected_u16, pindex_ptr, sizeof( system_expected_u16 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &system_derated_expected_u16, pindex_ptr, sizeof( system_derated_expected_u16 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &system_limit_u16, pindex_ptr, sizeof( system_limit_u16 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &system_actual_flow_u16, pindex_ptr, sizeof( system_actual_flow_u16 ) );

	rv += ALERTS_pull_object_off_pile( ppile, &station_derated_expected_u16, pindex_ptr, sizeof( station_derated_expected_u16 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &station_share_of_actual_u16, pindex_ptr, sizeof( station_share_of_actual_u16 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &stations_on_u16, pindex_ptr, sizeof( stations_on_u16 ) );


	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( paid == AID_FIRST_LOWFLOW )
		{
			if( system_actual_flow_u16 == 0 )
			{
				strlcpy( str_32, "1st NO FLOW", sizeof(str_32) );
			}
			else
			{
				strlcpy( str_32, "1st LOW FLOW", sizeof(str_32) );
			}
		}
		else if( paid == AID_FINAL_LOWFLOW )
		{
			if( system_actual_flow_u16 == 0 )
			{
				strlcpy( str_32, "NO FLOW", sizeof(str_32) );
			}
			else
			{
				strlcpy( str_32, "LOW FLOW", sizeof(str_32) );
			}
		}
		else if( paid == AID_FIRST_HIGHFLOW )
		{
			strlcpy( str_32, "1st HIGH FLOW", sizeof(str_32) );
		}
		else if( paid == AID_FINAL_HIGHFLOW )
		{
			strlcpy( str_32, "HIGH FLOW", sizeof(str_32) );
		}

		// -----------------------

		if( (paid == AID_FINAL_LOWFLOW) && (system_actual_flow_u16 == 0) )
		{
			snprintf( pdest,
					  pdest_size,
					  "NO FLOW DETECTED BY FLOW METER Station %s",
					  ALERTS_get_station_number_with_or_without_two_wire_indicator( box_index_0, station_0_u16, (char*)&str_16, sizeof(str_16) ) );
		}
		else
		{
			snprintf( pdest,
					  pdest_size,
					  "%s Station %s: saw %d gpm, expected %d gpm",
					  str_32,
					  ALERTS_get_station_number_with_or_without_two_wire_indicator( box_index_0, station_0_u16, (char*)&str_16, sizeof(str_16) ),
					  station_share_of_actual_u16,
					  station_derated_expected_u16 );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_two_wire_cable_excessive_current_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lbox_index_0;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() == (true) )
		{
			snprintf( pdest, pdest_size, "2-Wire Cable Excessive Electrical Current: Controller %c", (lbox_index_0 + 'A') );
		}
		else
		{
			snprintf( pdest, pdest_size, "2-Wire Cable Excessive Electrical Current" );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_two_wire_cable_over_heated_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lbox_index_0;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() == (true) )
		{
			snprintf( pdest, pdest_size, "2-Wire Terminal Overheated: Controller %c", (lbox_index_0 + 'A') );
		}
		else
		{
			snprintf( pdest, pdest_size, "2-Wire Terminal Overheated" );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_two_wire_cable_cooled_off_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lbox_index_0;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() == (true) )
		{
			snprintf( pdest, pdest_size, "2-Wire Terminal Cooled Off: Controller %c", (lbox_index_0 + 'A') );
		}
		else
		{
			snprintf( pdest, pdest_size, "2-Wire Terminal Cooled Off" );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_two_wire_station_decoder_fault_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lfault_code;

	UNS_8   lbox_index_0;

	UNS_32  ldecoder_serial_number;

	UNS_8   lstation_number_0;

	char    str_16[ 16 ];

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lfault_code, pindex_ptr, sizeof( lfault_code ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &ldecoder_serial_number, pindex_ptr, sizeof( ldecoder_serial_number ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lstation_number_0, pindex_ptr, sizeof( lstation_number_0 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( lfault_code == STATION_DECODER_FAULT_ALERT_CODE_voltage_too_low )
		{
			snprintf( pdest, pdest_size, "STATION DECODER VOLTAGE TOO LOW: Decoder S/N %07d (Station %s)", ldecoder_serial_number, ALERTS_get_station_number_with_or_without_two_wire_indicator( lbox_index_0, lstation_number_0, (char*)&str_16, sizeof(str_16) ) );
		}
		else
			if( lfault_code == STATION_DECODER_FAULT_ALERT_CODE_solenoid_short )
		{
			// 10/29/2014 rmd : Because this one is likely more common I've re-arranged to a more
			// traditional format.
			snprintf( pdest, pdest_size, "SOLENOID SHORT: Station %s on Decoder S/N %07d", ALERTS_get_station_number_with_or_without_two_wire_indicator( lbox_index_0, lstation_number_0, (char*)&str_16, sizeof(str_16) ), ldecoder_serial_number );
		}
		else
			if( lfault_code == STATION_DECODER_FAULT_ALERT_CODE_inoperative )
		{
			// 10/29/2014 rmd : The 'code' is if the box index if 0xFF the decoder has no stations
			// assigned to it. Don't like using the magic number approach but efficient in this case.
			if( lbox_index_0 != 0xFF )
			{
				// 10/27/2014 rmd : If the alert was generated the station number is valid. That used to not
				// be the case.
				snprintf( pdest, pdest_size, "STATION DECODER NOT RESPONDING: Decoder S/N %07d (Station %s)", ldecoder_serial_number, ALERTS_get_station_number_with_or_without_two_wire_indicator( lbox_index_0, lstation_number_0, (char*)&str_16, sizeof(str_16) ) );
			}
			else
			{
				snprintf( pdest, pdest_size, "STATION DECODER NOT RESPONDING: Decoder S/N %07d (no stations assigned)", ldecoder_serial_number );
			}
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_two_wire_poc_decoder_fault_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lfault_code;

	UNS_8   lbox_index_0;

	UNS_32  ldecoder_serial_number;

	char    str_64[ 64 ];

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lfault_code, pindex_ptr, sizeof( lfault_code ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &ldecoder_serial_number, pindex_ptr, sizeof( ldecoder_serial_number ) );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_64, sizeof(str_64), pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( lfault_code == POC_DECODER_FAULT_ALERT_CODE_voltage_too_low )
		{
			snprintf( pdest, pdest_size, "POC DECODER VOLTAGE TOO LOW: Decoder S/N %07d (\"%s\")", ldecoder_serial_number, str_64 );
		}
		else
			if( lfault_code == POC_DECODER_FAULT_ALERT_CODE_solenoid_short )
		{
			// 10/29/2014 rmd : Because this one is likely more common I've re-arranged to a more
			// traditional format.
			snprintf( pdest, pdest_size, "SOLENOID SHORT: Master Valve on POC Decoder S/N %07d (\"%s\")", ldecoder_serial_number, str_64 );
		}
		else
			if( lfault_code == POC_DECODER_FAULT_ALERT_CODE_inoperative )
		{
			snprintf( pdest, pdest_size, "POC DECODER NOT RESPONDING: Decoder S/N %07d (\"%s\")", ldecoder_serial_number, str_64 );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_poc_not_found_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_32	ldecoder_sn;

	UNS_8	lbox_index_0;

	UNS_32	rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &ldecoder_sn, pindex_ptr, sizeof( ldecoder_sn ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( ldecoder_sn )
		{
			snprintf( pdest, pdest_size, "Error: Decoder based POC @ %c (S/N %07u) reporting in", (lbox_index_0 + 'A'), ldecoder_sn );
		}
		else
		{
			snprintf( pdest, pdest_size, "Error: Terminal based POC @ %c reporting in", (lbox_index_0 + 'A') );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_hub_rcvd_data_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8	lrouter_type;

	UNS_8	lport;

	UNS_32	lcontroller_model;

	UNS_32	ldata_len;

	UNS_32	rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lrouter_type, pindex_ptr, sizeof( lrouter_type ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lport, pindex_ptr, sizeof( lport ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_model, pindex_ptr, sizeof( lcontroller_model ) );
	rv += ALERTS_pull_object_off_pile( ppile, &ldata_len, pindex_ptr, sizeof( ldata_len ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "%s ROUTER: %s packet in %s %u bytes", router_type_str[ lrouter_type ], lcontroller_model == ROUTING_CLASS_IS_2000_BASED ? "ET2000" : "CS3000", port_names[ lport ], ldata_len ); 
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_hub_forwarding_data_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8	lrouter_type;

	UNS_8	lport;

	UNS_32	lcontroller_model;

	UNS_32	ldata_len;

	UNS_32	rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lrouter_type, pindex_ptr, sizeof( lrouter_type ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lport, pindex_ptr, sizeof( lport ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_model, pindex_ptr, sizeof( lcontroller_model ) );
	rv += ALERTS_pull_object_off_pile( ppile, &ldata_len, pindex_ptr, sizeof( ldata_len ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "%s ROUTER: %s, forwarding %s packet %u bytes", router_type_str[ lrouter_type ], port_names[ lport ], lcontroller_model == ROUTING_CLASS_IS_2000_BASED ? "ET2000" : "CS3000", ldata_len ); 
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_router_rcvd_unexp_class_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8	lrouter_type;

	UNS_8	lport;

	UNS_32	lcontroller_model;

	UNS_32	lclass;

	UNS_32	rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lrouter_type, pindex_ptr, sizeof( lrouter_type ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lport, pindex_ptr, sizeof( lport ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_model, pindex_ptr, sizeof( lcontroller_model ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lclass, pindex_ptr, sizeof( lclass ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "%s ROUTER: %s unexp %s packet of class %u", router_type_str[ lrouter_type ], port_names[ lport ], lcontroller_model == ROUTING_CLASS_IS_2000_BASED ? "ET2000" : "CS3000", lclass ); 
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_router_rcvd_unexp_base_class_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8	lrouter_type;

	UNS_8	lport;

	UNS_32	lcontroller_model;

	UNS_32	rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lrouter_type, pindex_ptr, sizeof( lrouter_type ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lport, pindex_ptr, sizeof( lport ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_model, pindex_ptr, sizeof( lcontroller_model ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "%s ROUTER: %s unknown routing class BASE %u", router_type_str[ lrouter_type ], port_names[ lport ], lcontroller_model ); 
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_router_rcvd_packet_not_on_hub_list_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8	lrouter_type;

	UNS_8	lport;

	UNS_32	lcontroller_model;

	UNS_32	rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lrouter_type, pindex_ptr, sizeof( lrouter_type ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lport, pindex_ptr, sizeof( lport ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_model, pindex_ptr, sizeof( lcontroller_model ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "ROUTER: %s packet to %s not on hub list", port_names[ lport ], lcontroller_model == ROUTING_CLASS_IS_2000_BASED ? "ET2000" : "CS3000" ); 
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_router_rcvd_packet_not_on_hub_list_with_sn_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8	lrouter_type;

	UNS_8	lport;

	UNS_32	lcontroller_model;

	UNS_32	lserial_num;

	UNS_32	rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lrouter_type, pindex_ptr, sizeof( lrouter_type ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lport, pindex_ptr, sizeof( lport ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_model, pindex_ptr, sizeof( lcontroller_model ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lserial_num, pindex_ptr, sizeof( lserial_num ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "%s ROUTER: %s from %u, dropping %s packet not on hub list", router_type_str[ lrouter_type ], port_names[ lport ], lserial_num, lcontroller_model == ROUTING_CLASS_IS_2000_BASED ? "ET2000" : "CS3000" ); 
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_router_rcvd_packet_not_for_us_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8	lrouter_type;

	UNS_8	lport;

	UNS_32	lcontroller_model;

	UNS_32	rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lrouter_type, pindex_ptr, sizeof( lrouter_type ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lport, pindex_ptr, sizeof( lport ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_model, pindex_ptr, sizeof( lcontroller_model ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "%s ROUTER: %s, %s packet not to us", router_type_str[ lrouter_type ], port_names[ lport ], lcontroller_model == ROUTING_CLASS_IS_2000_BASED ? "ET2000" : "CS3000" ); 
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_router_unexp_to_addr_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8	lrouter_type;

	UNS_8	lport;

	UNS_32	rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lrouter_type, pindex_ptr, sizeof( lrouter_type ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lport, pindex_ptr, sizeof( lport ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "%s ROUTER: unexp TO addr from %s", router_type_str[ lrouter_type ], port_names[ lport ] ); 
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_router_unk_port_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8	lrouter_type;

	UNS_32	rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lrouter_type, pindex_ptr, sizeof( lrouter_type ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "%s ROUTER: UNK port", router_type_str[ lrouter_type ] ); 
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_router_rcvd_unexp_token_resp_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8	lrouter_type;

	UNS_8	lport;

	UNS_32	rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lrouter_type, pindex_ptr, sizeof( lrouter_type ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lport, pindex_ptr, sizeof( lport ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "%s ROUTER: %s rcvd token/scan RESP not for us", router_type_str[ lrouter_type ], port_names[ lport ] ); 
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_ci_queued_msg_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_32	lsend_to_front;

	UNS_32	levent;

	UNS_32	rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lsend_to_front, pindex_ptr, sizeof( lsend_to_front ) );
	rv += ALERTS_pull_object_off_pile( ppile, &levent, pindex_ptr, sizeof( levent ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		//snprintf( pdest, pdest_size, "CI queued msg to %s %u", (lsend_to_front == CI_POST_TO_FRONT) ? "front" : "back", levent ); 
		// 3/28/2018 rmd : The above line won't compile into the comm server. The define comes from
		// FreeRTOS, so I decided to take a quick and dirty approach, by referencing a direct
		// value.
		snprintf( pdest, pdest_size, "CI queued msg to %s %u", (lsend_to_front == 1) ? "front" : "back", levent ); 
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_msg_transaction_time_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_32	ltransact_time;

	UNS_32	rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &ltransact_time, pindex_ptr, sizeof( ltransact_time ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "msg transaction seconds is %u", ltransact_time ); 
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_largest_token_size_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_32	llargest_token_size;

	UNS_8	pis_resp;

	UNS_32	rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &llargest_token_size, pindex_ptr, sizeof( llargest_token_size ) );
	rv += ALERTS_pull_object_off_pile( ppile, &pis_resp, pindex_ptr, sizeof( pis_resp ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( pis_resp == (true) )
		{
			snprintf( pdest, pdest_size, "largest token resp %u bytes", llargest_token_size );
		}
		else
		{
			snprintf( pdest, pdest_size, "largest token %u bytes", llargest_token_size );
		}
	}

	return( rv );
}


/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_budget_under_budget_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char    lpoc_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32	lpercent_under_budget;

	UNS_32	rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&lpoc_name, sizeof(lpoc_name), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lpercent_under_budget, pindex_ptr, sizeof( lpercent_under_budget ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		// 9/16/2016 skc : Different text for 0 percent deviation from budget
		if( lpercent_under_budget == 0 )
		{
			snprintf( pdest, pdest_size, "POC \"%s\" expected to be at budget at end of period", lpoc_name ); 
		}
		else
		{
			snprintf( pdest, pdest_size, "POC \"%s\" expected to be under budget by %d%% at end of period", lpoc_name, lpercent_under_budget ); 
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_budget_over_budget_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char    lpoc_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32	lpercent_over_budget;

	UNS_32	lpredicted_usage_percent;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&lpoc_name, sizeof(lpoc_name), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lpercent_over_budget, pindex_ptr, sizeof( lpercent_over_budget ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lpredicted_usage_percent, pindex_ptr, sizeof( lpredicted_usage_percent ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "POC \"%s\" is OVER BUDGET by %d%%. Expected to be %d%% over budget at end of period", lpoc_name, lpercent_over_budget, lpredicted_usage_percent ); 
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_budget_will_exceed_budget_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char    lpoc_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32	lpercent_over_budget;

	UNS_32	rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&lpoc_name, sizeof(lpoc_name), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lpercent_over_budget, pindex_ptr, sizeof( lpercent_over_budget ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "POC \"%s\" expected to be OVER BUDGET by %d%% at end of period", lpoc_name, lpercent_over_budget);
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_budget_no_budget_values_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char	lmainline_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	char	lpoc_name[ NUMBER_OF_CHARS_IN_A_NAME ]; 

	UNS_32	rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&lmainline_name, sizeof(lmainline_name), pindex_ptr );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&lpoc_name, sizeof(lpoc_name), pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		// 7/21/2016 skc : FogBugs #3283
		snprintf( pdest, pdest_size, "POC \"%s\" on Mainline \"%s\" has a budget of 0 gallons", lpoc_name, lmainline_name );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_budget_group_reduction_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char	lgroup_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32	lreduction_percent;

	BOOL_32	lreached_limit;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&lgroup_name, sizeof(lgroup_name), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lreduction_percent, pindex_ptr, sizeof( lreduction_percent ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lreached_limit, pindex_ptr, sizeof( lreached_limit ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( lreached_limit == (false) )	// normal reduction
		{
			snprintf( pdest, pdest_size, "Station Group \"%s\" run times reduced by %d%% to stay within budget", lgroup_name, lreduction_percent );
		}
		else // reduction hit the limit value
		{
			snprintf( pdest, pdest_size, "Station Group \"%s\" run times reduced by %d%% (LIMIT REACHED). May go over budget", lgroup_name, lreduction_percent );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_budget_period_ended_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char	lpoc_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32	lbudgeted_use;

	UNS_32	lactual_use;

	UNS_32	rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&lpoc_name, sizeof(lpoc_name), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lbudgeted_use, pindex_ptr, sizeof( lactual_use ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lactual_use, pindex_ptr, sizeof( lactual_use ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( lbudgeted_use == 0)
		{
			snprintf( pdest, pdest_size, "Budget Period End - No budget value");
		}
		else if( lbudgeted_use >= lactual_use )
		{
			snprintf( pdest, pdest_size, "Budget Period End - POC \"%s\" UNDER BUDGET by %d%%", lpoc_name, 100 * (lbudgeted_use-lactual_use)/lbudgeted_use );
		}
		else // reduction hit the limit value
		{
			snprintf( pdest, pdest_size, "Budget Period End - POC \"%s\" OVER BUDGET by %d%%", lpoc_name, 100 * (lactual_use-lbudgeted_use)/lbudgeted_use );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_budget_over_budget_with_period_ending_today_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char    lpoc_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32	lpercent_over_budget;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&lpoc_name, sizeof(lpoc_name), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lpercent_over_budget, pindex_ptr, sizeof( lpercent_over_budget ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "POC \"%s\" is OVER BUDGET by %d%% with period ending today", lpoc_name, lpercent_over_budget ); 
	}

	return( rv );
}

/* ---------------------------------------------------------- */
#if 0
static UNS_32 ALERTS_pull_SetHOByProg_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{

	UNS_8 prog_u8;

	INT_32 holdover;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &prog_u8, pindex_ptr, sizeof( prog_u8 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &holdover, pindex_ptr, sizeof( holdover ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		// TODO commented out to get it to compile
	#if 0
		snprintf( pdest, pdest_size, "Hold-Over Set: %s to %.1f mins", E_ProgStrWithoutColon[ prog_u8 ], (holdover / 60.0) ); 
	#else
		(void)pdest;
		(void)pdest_size;
	#endif
	}

	return( rv );

}
#endif

/* ---------------------------------------------------------- */
#if 0
static UNS_32 ALERTS_pull_SetHOAllStations_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	INT_32 holdover;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &holdover, pindex_ptr, sizeof( holdover ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Hold-Over Set: All Stations to %.1f mins", (holdover / 60.0) ); 
	}

	return( rv );
}
#endif

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_set_no_water_days_by_station_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   box_index_0;

	UNS_16  station_0_u16, nowdays_u16;

	char    str_16[ 16 ];

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof( box_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &station_0_u16, pindex_ptr, sizeof( station_0_u16 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &nowdays_u16, pindex_ptr, sizeof( nowdays_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "No Water Days Set: Station %s to %d days", ALERTS_get_station_number_with_or_without_two_wire_indicator( box_index_0, station_0_u16, (char*)&str_16, sizeof(str_16) ), nowdays_u16 );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_set_no_water_days_by_group_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char    str_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_16  nowdays_u16;

	UNS_32  lgroup_ID;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_48, sizeof(str_48), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lgroup_ID, pindex_ptr, sizeof( lgroup_ID ) );
	rv += ALERTS_pull_object_off_pile( ppile, &nowdays_u16, pindex_ptr, sizeof( nowdays_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "No Water Days Set: Station Group \"%s\" to %d days", str_48, nowdays_u16 ); 
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_SetNOWDaysByAll_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 nowdays_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &nowdays_u16, pindex_ptr, sizeof( nowdays_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "No Water Days Set: All Stations to %d days", nowdays_u16 ); 
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_set_no_water_days_by_box_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   box_index_0;

	UNS_16  nowdays_u16;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof(box_index_0) );
	rv += ALERTS_pull_object_off_pile( ppile, &nowdays_u16, pindex_ptr, sizeof(nowdays_u16) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		// 2/11/2016 wjb : Only show the controller letter if the controller is part of a FLOWSENSE
		// chain.
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "No Water Days Set: All Stations in Controller %c to %d days", (box_index_0 + 'A'), nowdays_u16 );
		}
		else
		{
			snprintf( pdest, pdest_size, "No Water Days Set: All Stations to %d days", nowdays_u16 );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_reset_moisture_balance_by_group_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char    str_48[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32  lgroup_ID;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_48, sizeof(str_48), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lgroup_ID, pindex_ptr, sizeof( lgroup_ID ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Accumulated Rain Cleared: Station Group \"%s\"", str_48 ); 
	}

	return( rv );
}

/* ---------------------------------------------------------- */
#if 0
static UNS_32 ALERTS_pull_PasswordLogin_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 level_u16;

	char str_16[ 16 ];

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &level_u16, pindex_ptr, sizeof( level_u16 ) );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_16, sizeof(str_16), pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		// snprintf( pdest, pdest_size, "Level %d logged in: %s", level_u16, str_16 );

		snprintf( pdest, pdest_size, "Level %d logged in: ***", level_u16 );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_RRE_PasswordLogin_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char str_16[ 16 ], str_64[ 64 ];

	UNS_8 level_u8;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &level_u8, pindex_ptr, sizeof( level_u8 ) );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_16, sizeof(str_16), pindex_ptr );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_64, sizeof(str_64), pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( (level_u8 & 0x80) == 0x80 )
		{
			// fix up the level ie clear the high order bit
			//
			level_u8 &= 0x7F;

			// if the high order bit is set the guy is actually logged in - if not he didn't need to
			// log in to make the changes
			//
			// snprintf( pdest, pdest_size, "Level %d logged in: %s", level_u8, str_64 );

			snprintf( pdest, pdest_size, "Changes by RRe %s Level %d Logged In: ***", str_64, level_u8 );
		}
		else
		{
			// snprintf( pdest, pdest_size, "Level %d logged in: %s", level_u8, str_64 );

			snprintf( pdest, pdest_size, "Changes by RRe %s - No login needed", str_64 );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_PasswordLogout_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, const UNS_32 paid, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 level_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &level_u16, pindex_ptr, sizeof( level_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( paid == AID_PASSWORDS_LOGOUT_KEYPAD_TIMEOUT )
		{
			snprintf( pdest, pdest_size, "Level %d Logged Out (keypad timeout)", level_u16 ); 
		}
		else if( paid == AID_PASSWORDS_LOGOUT_USER_LOGGED_OUT )
		{
			snprintf( pdest, pdest_size, "Level %d Logged Out (user logged out)", level_u16 ); 
		}
	}

	return( rv );
}
#endif

/* ---------------------------------------------------------- */
#if 0
static UNS_32 ALERTS_pull_SystemCapacityAutomaticallyAdjusted_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char str_16[ 16 ];

	UNS_16 from_u16, to_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_16, sizeof(str_16), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &from_u16, pindex_ptr, sizeof( from_u16 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &to_u16, pindex_ptr, sizeof( to_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "System Capacity Adjusted From %d To %d gpm for Station %s", from_u16, to_u16, str_16 );
	}

	return( rv );
}
#endif

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_MVOR_alert_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	// 8/28/2015 ajv : When functions are declared as static, the compiler may inline the
	// functions and optimize out floating-point constant literals from the calculations. This
	// can result in a division-by-0 error. In order to resolve this, we declare the constant
	// literal as volatile which tells the compile to not optimize the value.
	static const volatile float SECONDS_PER_HOUR = 3600.0F;

	UNS_8   lgroup_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_8   lmvor_action_to_take;

	UNS_32  lmvor_seconds;

	INITIATED_VIA_ENUM  linitiated_by;

	UNS_32  rv;

	// ----------

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)lgroup_name, sizeof(lgroup_name), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lmvor_action_to_take, pindex_ptr, sizeof(UNS_8) );
	rv += ALERTS_pull_object_off_pile( ppile, &lmvor_seconds, pindex_ptr, sizeof(UNS_32) );
	rv += ALERTS_pull_object_off_pile( ppile, &linitiated_by, pindex_ptr, sizeof(INITIATED_VIA_ENUM) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		// 8/28/2015 ajv : Since we're indexing into the who_initiated_str ENUM, make sure the value
		// is in range. If not, set it to the last type which will display "(UNK REASON)"
		if( linitiated_by > INITIATED_VIA_LAST_TYPE )
		{
			linitiated_by = INITIATED_VIA_LAST_TYPE;
		}

		switch( lmvor_action_to_take )
		{
			case MVOR_ACTION_OPEN_MASTER_VALVE:
				snprintf( pdest, pdest_size, "Master Valve Override: Mainline \"%s\" OPENED for %.1f hours%s", lgroup_name, (lmvor_seconds / SECONDS_PER_HOUR), who_initiated_str[ linitiated_by ] );
				break;

			case MVOR_ACTION_CLOSE_MASTER_VALVE:
				snprintf( pdest, pdest_size, "Master Valve Override: Mainline \"%s\" CLOSED for %.1f hours%s", lgroup_name, (lmvor_seconds / SECONDS_PER_HOUR), who_initiated_str[ linitiated_by ] );
				break;

			case MVOR_ACTION_CANCEL_MASTER_VALVE_OVERRIDE:
				if( linitiated_by == INITIATED_VIA_MVOR_SCHEDULE )
				{
					snprintf( pdest, pdest_size, "Master Valve Override: Mainline \"%s\" ended", lgroup_name );
				}
				else
				{
					snprintf( pdest, pdest_size, "Master Valve Override: Mainline \"%s\" cancelled%s", lgroup_name, who_initiated_str[ linitiated_by ] );
				}
				break;
		}
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_MVOR_skipped_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lgroup_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_8   lreason_skipped;

	UNS_32  rv;

	// ----------

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)lgroup_name, sizeof(lgroup_name), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lreason_skipped, pindex_ptr, sizeof(UNS_8) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "MASTER VALVE OVERRIDE SKIPPED: Mainline \"%s\": %s", lgroup_name, irrigation_skipped_reason_str[ lreason_skipped ] );
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_starting_scan_with_reason_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8 lreason;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lreason, pindex_ptr, sizeof(UNS_8) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "FLOWSENSE Scan Started: %s", scan_start_request_str[ lreason ] );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_chain_is_same_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8 lcontroller_index;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_index, pindex_ptr, sizeof(UNS_8) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Foal: \"%c\" says network is the same", ('A' + lcontroller_index) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_chain_has_changed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8 lcontroller_index;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lcontroller_index, pindex_ptr, sizeof(UNS_8) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Foal: \"%c\" says network is new", ('A' + lcontroller_index) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_reboot_request_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char    lreason_str[ 64 ];

	UNS_32  lreason;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lreason, pindex_ptr, sizeof(lreason) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		switch( lreason )
		{
			case SYSTEM_SHUTDOWN_EVENT_factory_reset_via_keypad:
				strlcpy( lreason_str, "reset to factory defaults via KEYPAD", sizeof(lreason_str) );
				break;

			case SYSTEM_SHUTDOWN_EVENT_factory_reset_via_cloud_panel_swap_for_new_panel:
			case SYSTEM_SHUTDOWN_EVENT_factory_reset_via_cloud_panel_swap_for_old_panel:
				strlcpy( lreason_str, "reset to factory defaults via CLOUD", sizeof(lreason_str) );
				break;

			case SYSTEM_SHUTDOWN_EVENT_main_code_file_updated:
				strlcpy( lreason_str, "CS3000 firmware updated", sizeof(lreason_str) );
				break;

			case SYSTEM_SHUTDOWN_EVENT_tpmicro_code_file_updated:
				strlcpy( lreason_str, "TP Micro firmware updated", sizeof(lreason_str) );
				break;

			case SYSTEM_SHUTDOWN_EVENT_main_code_distribution_completed:
				strlcpy( lreason_str, "CS3000 firmware distributed", sizeof(lreason_str) );
				break;

			case SYSTEM_SHUTDOWN_EVENT_tpmicro_code_distribution_completed:
				strlcpy( lreason_str, "TP Micro firmware distributed", sizeof(lreason_str) );
				break;

			case SYSTEM_SHUTDOWN_EVENT_code_distribution_error:
				strlcpy( lreason_str, "firmware distribution error", sizeof(lreason_str) );
				break;

			case SYSTEM_SHUTDOWN_EVENT_code_receipt_error:
				strlcpy( lreason_str, "firmware receipt error", sizeof(lreason_str) );
				break;

			case SYSTEM_SHUTDOWN_EVENT_one_of_the_slaves_rebooted:
				strlcpy( lreason_str, "a FLOWSENSE slave rebooted", sizeof(lreason_str) );
				break;

			case SYSTEM_SHUTDOWN_EVENT_hub_to_distribute_tpmicro_binary:
				strlcpy( lreason_str, "hub distributing TP Micro firmware", sizeof(lreason_str) );
				break;

			case SYSTEM_SHUTDOWN_EVENT_hub_to_distribute_main_binary:
				strlcpy( lreason_str, "hub distributing CS3000 firmware", sizeof(lreason_str) );
				break;

			case SYSTEM_SHUTDOWN_EVENT_hub_to_distribute_both_binaries:
				strlcpy( lreason_str, "Hub distributing TPMicro and CS3000 firmware", sizeof(lreason_str) );
				break;

			default:
				strlcpy( lreason_str, "reason unknown", sizeof(lreason_str) );
		}

		snprintf( pdest, pdest_size, "Controller rebooted: %s", lreason_str );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_ETGAGE_percent_full_edited_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 from_u16, to_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &from_u16, pindex_ptr, sizeof( from_u16 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &to_u16, pindex_ptr, sizeof( to_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		#ifndef _MSC_VER

			// Don't display the "Change:" text if the alert is being generated
			// for the Change Lines report.
			if( ppile == &alerts_struct_changes )
			{
				strlcpy( pdest, "\0", pdest_size );
			}
			else
			{
				strlcpy( pdest, "Change: ", pdest_size );
			}

		#else

			strlcpy( pdest, "Change: ", pdest_size );

		#endif

		sp_strlcat( pdest, pdest_size, "ET Gage Percent Full from %2d%% to %2d%% (keypad)", (UNS_32)(((float)from_u16 / ET_GAGE_PULSE_CAPACITY) * 100.0), (UNS_32)(((float)to_u16 / ET_GAGE_PULSE_CAPACITY) * 100.0) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_ETGAGE_pulse_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8 pulses;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &pulses, pindex_ptr, sizeof( pulses ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "ET Gage Pulse %d", pulses );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_RAIN_24HourTotal_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	float rain_fl;

	UNS_32 rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &rain_fl, pindex_ptr, sizeof(float) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Midnight-to-Midnight Rainfall is %.2f inches", rain_fl );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_ETGAGE_PercentFull_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 percent_full_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &percent_full_u16, pindex_ptr, sizeof( percent_full_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "ET Gage Low: only %2d%% full", percent_full_u16 );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_stop_key_pressed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_32  lsystem_GID;

	UNS_8   lhighest_reason_in_list_u8;

	UNS_8   lbox_index_0;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lsystem_GID, pindex_ptr, sizeof(lsystem_GID) );
	rv += ALERTS_pull_object_off_pile( ppile, &lhighest_reason_in_list_u8, pindex_ptr, sizeof(lhighest_reason_in_list_u8) );
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof(UNS_8) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		// TODO Display the system name associated with the GID
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "STOPPED: %s (Controller %c)", GetReasonOnStr( (UNS_32)lhighest_reason_in_list_u8 ), (lbox_index_0 + 'A') );
		}
		else
		{
			snprintf( pdest, pdest_size, "STOPPED: %s (keypad)", GetReasonOnStr( (UNS_32)lhighest_reason_in_list_u8 ) );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_wind_paused_or_resumed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr, const UNS_32 paid )
{
	UNS_32  lwind_speed_mph;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lwind_speed_mph, pindex_ptr, sizeof(lwind_speed_mph) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		switch( paid )
		{
			case AID_WIND_PAUSED:
				strlcpy( pdest, "Wind: Paused Irrigation", pdest_size );

				if( lwind_speed_mph > 0 )
				{
					sp_strlcat( pdest, pdest_size, " at %u mph", lwind_speed_mph );
				}
				break;

			case AID_WIND_RESUMED:
				snprintf( pdest, pdest_size, "Wind: Resumed Irrigation at %u mph", lwind_speed_mph );
				break;

			default:
				snprintf( pdest, pdest_size, "Wind: Unknown at %u mph", lwind_speed_mph );
				break;
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
#if 0
static UNS_32 ALERTS_pull_IRRIGATION_TURNED_OFF_WITH_REASON_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	INITIATED_VIA_ENUM who_initiated;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &who_initiated, pindex_ptr, sizeof( who_initiated ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "SCHEDULED IRRIGATION TURNED OFF%s", e_who_initiated_string[ who_initiated ] );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_IRRIGATION_TURNED_ON_WITH_REASON_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )\
{
	INITIATED_VIA_ENUM who_initiated;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &who_initiated, pindex_ptr, sizeof( who_initiated ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "SCHEDULED IRRIGATION TURNED ON%s", e_who_initiated_string[ who_initiated ] );
	}

	return( rv );
}
#endif

/* ---------------------------------------------------------- */
#if 0
static UNS_32 ALERTS_pull_HoldOverChanged_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 station_0_u16;

	INT_32 prev_holdover, new_holdover;

	INITIATED_VIA_ENUM who_initiated;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &station_0_u16, pindex_ptr, sizeof( station_0_u16 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &prev_holdover, pindex_ptr, sizeof( prev_holdover ) );
	rv += ALERTS_pull_object_off_pile( ppile, &new_holdover, pindex_ptr, sizeof( new_holdover ) );
	rv += ALERTS_pull_object_off_pile( ppile, &who_initiated, pindex_ptr, sizeof( who_initiated ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Sta %d Hold-Over from %.1f to %.1f%s", (station_0_u16 + 1), (prev_holdover / 60.0), (new_holdover / 60.0), e_who_initiated_string[ who_initiated ] );
	}

	return( rv );
}
#endif

/* ---------------------------------------------------------- */
#if 0
static UNS_32 ALERTS_pull_Ping_SWREV_Error_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char str_32[ 32 ];

	UNS_8 addr_u8;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_32, sizeof(str_32), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &addr_u8, pindex_ptr, sizeof( addr_u8 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Update ROMs at %c (has %s)", addr_u8, str_32 );
	}

	return( rv );
}
#endif

/* ---------------------------------------------------------- */
// AID = 250
static UNS_32 ALERTS_pull_scheduled_irrigation_started_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_32  rv;

	rv = 0;

	// ----------

	// 10/25/2012 rmd : NOTE - either of these two methods works for handling the single byte to
	// 4 byte transition for the reason in the list. Declaring the variable as an UNS_8 leaves
	// it up to the compiler to do the work. Declaring it as an UNS_32 means we have to
	// explicitly do the work. I believe letting the compiler take care of its business is the
	// most efficient code space wise (we're only talking about maybe 4 bytes diff). So that's
	// the way we'll go.

	// 10/25/2012 rmd : METHOD 1 Only pulling and stuffing the first byte into this 4 byte
	// variable. ZERO it out first.
	//UNS_32	reason_in_list;
	//reason_in_list = 0;

	// 10/25/2012 rmd : METHOD 2 Declare as an UNS_8 to begin with.
	UNS_8   reason_in_list;

	// ----------

	// 10/11/2012 rmd : Pull the reason which was only stored as a single byte.
	rv += ALERTS_pull_object_off_pile( ppile, &reason_in_list, pindex_ptr, sizeof(UNS_8) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "START: %s", GetReasonOnStr( reason_in_list ) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
// AID = 251
static UNS_32 ALERTS_pull_some_or_all_skipped_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	// This function is responsible to build the string appropiate for the alert in pdest. It
	// needs to keep pindex_ptr current to the next place the data comes from. Must return how
	// many bytes it pulls off of the pile for this alert.
	//
	// pdest points to a string and can be concatenated to as a string

	UNS_8   reason_in_list, reason_skipped;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &reason_in_list, pindex_ptr, sizeof(reason_in_list) );
	rv += ALERTS_pull_object_off_pile( ppile, &reason_skipped, pindex_ptr, sizeof(reason_skipped) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "SKIPPED: Some or All %s: %s", GetReasonOnStr( reason_in_list ), irrigation_skipped_reason_str[ reason_skipped ] );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
// AID = 252
static UNS_32 ALERTS_pull_programmed_irrigation_still_running_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   box_index_0;

	UNS_16  station_number_0_u16;

	char    str_16[ 16 ];

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof(box_index_0) );
	rv += ALERTS_pull_object_off_pile( ppile, &station_number_0_u16, pindex_ptr, sizeof(station_number_0_u16) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Irrigation not finished at next start time: Station %s", ALERTS_get_station_number_with_or_without_two_wire_indicator( box_index_0, station_number_0_u16, (char*)&str_16, sizeof(str_16) ) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */

// AID = 253  is a simple alert and requires no function to achieve the parse

/* ---------------------------------------------------------- */
// AID = 254
static UNS_32 ALERTS_pull_irrigation_ended_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lreason_u8;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lreason_u8, pindex_ptr, sizeof(lreason_u8) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "END: %s", GetReasonOnStr( lreason_u8 ) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
// AID = 256 & 257
static UNS_32 ALERTS_pull_flow_not_checked_with_reason_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr, const UNS_32 paid )
{
	UNS_8   lbox_index_0;

	UNS_8   lstation_number;

	char    str_8[ 8 ];

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof(lbox_index_0) );
	rv += ALERTS_pull_object_off_pile( ppile, &lstation_number, pindex_ptr, sizeof(lstation_number) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Unable to check flow: Sta %s (", ALERTS_get_station_number_with_or_without_two_wire_indicator( (UNS_32)lbox_index_0, (UNS_32)lstation_number, str_8, sizeof(str_8)) );

		switch( paid )
		{
			case AID_FLOW_NOT_CHECKED__CYCLE_TOO_SHORT:
				strlcat( pdest, "Cycle Too Short)", pdest_size);
				break;

			case AID_FLOW_NOT_CHECKED__UNSTABLE_FLOW:
				strlcat( pdest, "Unstable Flow)", pdest_size);
				break;

			default:
				strlcat( pdest, "Unknown Reason)", pdest_size);
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_fuse_blown_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lbox_index_0;

	UNS_32  rv;

	rv = 0;

	// 10/29/2013 ajv : For now, although it's included in the alert definition,
	// don't display the serial number. However, we need to extract it to keep
	// the pointer intact.
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof(lbox_index_0) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "FUSE BLOWN: Controller %c", (lbox_index_0 + 'A') );
		}
		else
		{
			snprintf( pdest, pdest_size, "FUSE BLOWN" );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_accum_rain_set_by_station_cleared_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	// 8/28/2015 ajv : When functions are declared as static, the compiler may inline the
	// functions and optimize out floating-point constant literals from the calculations. This
	// can result in a division-by-0 error. In order to resolve this, we declare the constant
	// literal as volatile which tells the compile to not optimize the value.
	static const volatile float TEN = 10.0F;

	UNS_8   lbox_index_0;

	UNS_16  lstation_number_0;

	UNS_32  lvalue_10u;

	INITIATED_VIA_ENUM  linitiated_by;

	char str_16[ 16 ];

	UNS_32  rv;

	// ----------

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof(lbox_index_0) );
	rv += ALERTS_pull_object_off_pile( ppile, &lstation_number_0, pindex_ptr, sizeof(lstation_number_0) );
	rv += ALERTS_pull_object_off_pile( ppile, &lvalue_10u, pindex_ptr, sizeof(lvalue_10u) );
	rv += ALERTS_pull_object_off_pile( ppile, &linitiated_by, pindex_ptr, sizeof(INITIATED_VIA_ENUM) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( lvalue_10u == 0 )
		{
			snprintf( pdest, pdest_size, "Accumulated Rain Cleared: Station %s%s", ALERTS_get_station_number_with_or_without_two_wire_indicator( lbox_index_0, lstation_number_0, (char*)&str_16, sizeof(str_16) ), who_initiated_str[ linitiated_by ] ); 
		}
		else
		{
			snprintf( pdest, pdest_size, "Accumulated Rain Set: Station %s to %.1f%s", ALERTS_get_station_number_with_or_without_two_wire_indicator( lbox_index_0, lstation_number_0, (char*)&str_16, sizeof(str_16) ), (lvalue_10u / TEN), who_initiated_str[ linitiated_by ] ); 
		}
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_fuse_replaced_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lbox_index_0;

	UNS_32  rv;

	rv = 0;

	// 10/29/2013 ajv : For now, although it's included in the alert definition,
	// don't display the serial number. However, we need to extract it to keep
	// the pointer intact.
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof(lbox_index_0) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "Blown Fuse Replaced: Controller %c", (lbox_index_0 + 'A') );
		}
		else
		{
			snprintf( pdest, pdest_size, "Blown Fuse Replaced" );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_mobile_station_on_from_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8 box_index_0;

	UNS_16 station_0_u16;

	char str_16[ 16 ];

	UNS_32 rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof(UNS_8) );

	rv += ALERTS_pull_object_off_pile( ppile, &station_0_u16, pindex_ptr, sizeof(UNS_16) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Mobile: Station %s ON", ALERTS_get_station_number_with_or_without_two_wire_indicator( box_index_0, station_0_u16, (char*)&str_16, sizeof(str_16) ) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_mobile_station_off_from_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8 box_index_0;

	UNS_16 station_0_u16;

	char str_16[ 16 ];

	UNS_32 rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof(UNS_8) );

	rv += ALERTS_pull_object_off_pile( ppile, &station_0_u16, pindex_ptr, sizeof(UNS_16) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Mobile: Station %s OFF", ALERTS_get_station_number_with_or_without_two_wire_indicator( box_index_0, station_0_u16, (char*)&str_16, sizeof(str_16) ) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_test_station_started_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8 box_index_0;

	UNS_16 station_0_u16;

	INITIATED_VIA_ENUM who_initiated;

	char str_16[ 16 ];

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof( box_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &station_0_u16, pindex_ptr, sizeof( station_0_u16 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &who_initiated, pindex_ptr, sizeof( INITIATED_VIA_ENUM ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "START: Test Station %s%s", ALERTS_get_station_number_with_or_without_two_wire_indicator( box_index_0, station_0_u16, (char*)&str_16, sizeof(str_16) ), who_initiated_str[ who_initiated ] );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_walk_thru_started_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lgroup_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)lgroup_name, sizeof(lgroup_name), pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "START: Walk-Thru \"%s\"", lgroup_name );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_manual_water_station_started_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8 box_index_0;

	UNS_16 station_0_u16;

	INITIATED_VIA_ENUM who_initiated;

	char str_16[ 16 ];

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof( box_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &station_0_u16, pindex_ptr, sizeof( station_0_u16 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &who_initiated, pindex_ptr, sizeof( INITIATED_VIA_ENUM ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "START: Manual Watering Station %s%s", ALERTS_get_station_number_with_or_without_two_wire_indicator( box_index_0, station_0_u16, (char*)&str_16, sizeof(str_16) ), who_initiated_str[ who_initiated ] );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_manual_water_program_started_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char    str_64[ 64 ];

	INITIATED_VIA_ENUM who_initiated;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)str_64, sizeof(str_64), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &who_initiated, pindex_ptr, sizeof( INITIATED_VIA_ENUM ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "START: Manual Watering Station Group \"%s\"%s", str_64, who_initiated_str[ who_initiated ] );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_manual_water_all_started_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	INITIATED_VIA_ENUM who_initiated;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &who_initiated, pindex_ptr, sizeof( INITIATED_VIA_ENUM ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "START: Manual Watering All Stations%s", who_initiated_str[ who_initiated ] );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
#if 0
static UNS_32 ALERTS_pull_TestProgramStarted_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8 prog_u8;

	INITIATED_VIA_ENUM who_initiated;

	UNS_32 rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &prog_u8, pindex_ptr, sizeof( prog_u8 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &who_initiated, pindex_ptr, sizeof( who_initiated ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		// TODO commented out to get it to compile
	#if 0
		snprintf( pdest, pdest_size, "START: Test %s%s", E_ProgStrWithoutColon[ prog_u8 ], e_who_initiated_string[ who_initiated ] );
	#else
		(void)pdest;
		(void)pdest_size;
	#endif
	}

	return( rv );
}
#endif

/* ---------------------------------------------------------- */
#if 0
static UNS_32 ALERTS_pull_TestAllStarted_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	INITIATED_VIA_ENUM who_initiated;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &who_initiated, pindex_ptr, sizeof( INITIATED_VIA_ENUM ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "START: Test All Stations%s", e_who_initiated_string[ who_initiated ] );
	}

	return( rv );
}
#endif

/* ---------------------------------------------------------- */
#if 0
static UNS_32 ALERTS_pull_RRe_station_on_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 station_0_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &station_0_u16, pindex_ptr, sizeof( station_0_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Radio Remote Sta %d ON", (station_0_u16 + 1) );
	}

	return( rv );
}
#endif

/* ---------------------------------------------------------- */
#if 0
static UNS_32 ALERTS_pull_RRe_station_off_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 station_0_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &station_0_u16, pindex_ptr, sizeof( station_0_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Radio Remote Sta %d OFF", (station_0_u16 + 1) );
	}

	return( rv );
}
#endif

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_ETTable_Substitution_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char str_128[ 128 ];

	char ldate_str_32[ 32 ];

	UNS_16  ldate, original_table_value_u16_100u, new_value_u16_100u;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)str_128, sizeof(str_128), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &ldate, pindex_ptr, sizeof( ldate ) );
	rv += ALERTS_pull_object_off_pile( ppile, &original_table_value_u16_100u, pindex_ptr, sizeof( original_table_value_u16_100u ) );
	rv += ALERTS_pull_object_off_pile( ppile, &new_value_u16_100u, pindex_ptr, sizeof( new_value_u16_100u ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "ET Substitution: %s replaced %.2f historical with %.2f", GetDateStr( ldate_str_32, sizeof(ldate_str_32), ldate, DATESTR_show_year_not, DATESTR_show_dow_not ), (original_table_value_u16_100u / 100.0), (new_value_u16_100u / 100.0) );
		//snprintf( pdest, pdest_size, "%s ET: using gage %.2f instead of %s's historical %.2f", str_64, (new_value_u16_100u / 100.0), GetDateStr( ldate_str_32, sizeof(ldate_str_32), ldate, DATESTR_show_year_not, DATESTR_show_dow_not ), (original_table_value_u16_100u / 100.0) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_ETTable_LimitedEntry_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char    str_128[ 128 ];

	char    ldate_str_32[ 32 ];

	UNS_16  ldate, original_table_value_u16_100u, new_value_u16_100u;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)str_128, sizeof(str_128), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &ldate, pindex_ptr, sizeof( ldate ) );
	rv += ALERTS_pull_object_off_pile( ppile, &original_table_value_u16_100u, pindex_ptr, sizeof( original_table_value_u16_100u ) );
	rv += ALERTS_pull_object_off_pile( ppile, &new_value_u16_100u, pindex_ptr, sizeof( new_value_u16_100u ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Historical ET Limit: %s replaced %.2f with %.2f", GetDateStr( ldate_str_32, sizeof(ldate_str_32), ldate, DATESTR_show_year_not, DATESTR_show_dow_not ), (original_table_value_u16_100u / 100.0), (new_value_u16_100u / 100.0) );
		//snprintf( pdest, pdest_size, "%s ET: limiting gage %.2f to %.2f", str_64, (original_table_value_u16_100u / 100.0), (new_value_u16_100u / 100.0) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
// AID = 560
static UNS_32 ALERTS_pull_light_ID_with_text_from_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8 box_index_0;

	UNS_8 output_index_0;

	UNS_8 text_index;

	UNS_32 rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof( UNS_8 ) );

	rv += ALERTS_pull_object_off_pile( ppile, &output_index_0, pindex_ptr, sizeof( UNS_8 ) );

	rv += ALERTS_pull_object_off_pile( ppile, &text_index, pindex_ptr, sizeof( UNS_8 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( (text_index >= LIGHTS_TEXT_SCHEDULED_ON) && (text_index <= LIGHTS_TEXT_MOBILE_OFF) )
		{
			if( FLOWSENSE_we_are_poafs() )
			{
				snprintf( pdest, pdest_size, "%s Light Output %c %d", lights_text_str[text_index], (box_index_0 + 'A'), (output_index_0 + 1) );
			}
			else
			{
				snprintf( pdest, pdest_size, "%s Lights Output %d", lights_text_str[text_index], (output_index_0 + 1) );
			}
		}
		else
			if( (text_index == LIGHTS_TEXT_SKIPPED_NO_STOP_TIME) || (text_index == LIGHTS_TEXT_SKIPPED_NO_RUN_TIME) )
		{
			if( FLOWSENSE_we_are_poafs() )
			{
				snprintf( pdest, pdest_size, "SKIPPED: Lights Output %c %d: %s", (box_index_0 + 'A'), (output_index_0 + 1), lights_text_str[text_index] );
			}
			else
			{
				snprintf( pdest, pdest_size, "SKIPPED: Lights Output %d: %s", (output_index_0 + 1), lights_text_str[text_index] );
			}
		}
		else
			if( text_index == LIGHTS_TEXT_OUTPUT_SHORT_DETECTED )
		{
			if( FLOWSENSE_we_are_poafs() )
			{
				snprintf( pdest, pdest_size, "ELECTRICAL SHORT: Lights Output %c %d", (box_index_0 + 'A'), (output_index_0 + 1) );
			}
			else
			{
				snprintf( pdest, pdest_size, "ELECTRICAL SHORT: Lights Output %d", (output_index_0 + 1) );
			}
		}
		else
			if( text_index == LIGHTS_TEXT_OUTPUT_NO_CURRENT_DETECTED )
		{
			if( FLOWSENSE_we_are_poafs() )
			{
				snprintf( pdest, pdest_size, "NO ELECTRICAL CURRENT: Lights Output %c %d", (box_index_0 + 'A'), (output_index_0 + 1) );
			}
			else
			{
				snprintf( pdest, pdest_size, "NO ELECTRICAL CURRENT: Lights Output %d", (output_index_0 + 1) );
			}
		}
		else
		{
			snprintf( pdest, pdest_size, "Light %d at %c: Unknown Reason", (output_index_0 + 1), (box_index_0 + 'A') );
		}
	}

	return( rv );
}

#if 0
/* ---------------------------------------------------------- */
// AID = 560
static UNS_32 ALERTS_pull_manual_light_on_from_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8 box_index_0;

	UNS_8 output_index_0;

	UNS_32 rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof( UNS_8 ) );

	rv += ALERTS_pull_object_off_pile( ppile, &output_index_0, pindex_ptr, sizeof( UNS_8 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "MANUAL: Light %c @ %c is ON", ( box_index_0 + 'A' ), ( output_index_0 + '1' ) );
	}

	return( rv );

}

/* ---------------------------------------------------------- */
// AID = 561
static UNS_32 ALERTS_pull_manual_light_off_from_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8 box_index_0;

	UNS_8 output_index_0;

	UNS_32 rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof( UNS_8 ) );

	rv += ALERTS_pull_object_off_pile( ppile, &output_index_0, pindex_ptr, sizeof( UNS_8 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "MANUAL: Light %c @ %c is OFF", ( box_index_0 + 'A' ), ( output_index_0 + '1' ) );
	}

	return( rv );

}

/* ---------------------------------------------------------- */
// AID = 562
static UNS_32 ALERTS_pull_mobile_light_on_from_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8 box_index_0;

	UNS_8 output_index_0;

	UNS_32 rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof( UNS_8 ) );

	rv += ALERTS_pull_object_off_pile( ppile, &output_index_0, pindex_ptr, sizeof( UNS_8 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "MOBILE: Light %c @ %c is ON", ( box_index_0 + 'A' ), ( output_index_0 + '1' ) );
	}

	return( rv );

}

/* ---------------------------------------------------------- */
// AID = 563
static UNS_32 ALERTS_pull_mobile_light_off_from_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8 box_index_0;

	UNS_8 output_index_0;

	UNS_32 rv;

	rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof( UNS_8 ) );

	rv += ALERTS_pull_object_off_pile( ppile, &output_index_0, pindex_ptr, sizeof( UNS_8 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "MOBILE: Light %c @ %c is OFF", ( box_index_0 + 'A' ), ( output_index_0 + '1' ) );
	}

	return( rv );

}

/* ---------------------------------------------------------- */
// AID = 564
static UNS_32 ALERTS_pull_test_light_started_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8 box_index_0;

	UNS_8 output_index_0;

	INITIATED_VIA_ENUM who_initiated;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof( box_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &output_index_0, pindex_ptr, sizeof( output_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &who_initiated, pindex_ptr, sizeof( INITIATED_VIA_ENUM ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "START: Test Light %c @ %c %s", ( box_index_0 + 'A' ), ( output_index_0 + '1' ), who_initiated_str[ who_initiated ] );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
// AID = 565
static UNS_32 ALERTS_pull_scheduled_lighting_started_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_32  rv;

	UNS_8 box_index_0;

	UNS_8 output_index_0;

	rv = 0;

	// ----------

	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof( UNS_8 ) );

	rv += ALERTS_pull_object_off_pile( ppile, &output_index_0, pindex_ptr, sizeof( UNS_8 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "PROGRAMMED: Light %c @ %c is ON", ( box_index_0 + 'A' ), ( output_index_0 + '1' ) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
// AID = 566
static UNS_32 ALERTS_pull_lighting_ended_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lreason_u8;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lreason_u8, pindex_ptr, sizeof(lreason_u8) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "END: %s", GetReasonOnStr( lreason_u8 ) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
// AID = 567 
static UNS_32 ALERTS_pull_scheduled_lighting_ended_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_32  rv;

	UNS_8 box_index_0;

	UNS_8 output_index_0;

	rv = 0;

	// ----------

	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof( UNS_8 ) );

	rv += ALERTS_pull_object_off_pile( ppile, &output_index_0, pindex_ptr, sizeof( UNS_8 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "PROGRAMMED: Light %c @ %c is OFF", ( box_index_0 + 'A' ), ( output_index_0 + '1' ) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
// AID = 568
static UNS_32 ALERTS_pull_lighting_skipped_start_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	// This function is responsible to build the string appropiate for the alert in pdest. It
	// needs to keep pindex_ptr current to the next place the data comes from. Must return how
	// many bytes it pulls off of the pile for this alert.
	//
	// pdest points to a string and can be concatenated to as a string

	UNS_8   reason_in_list, reason_skipped;

	UNS_8 box_index_0;

	UNS_8 output_index_0;

	UNS_32  rv = 0;

	rv += ALERTS_pull_object_off_pile( ppile, &box_index_0, pindex_ptr, sizeof( UNS_8 ) );

	rv += ALERTS_pull_object_off_pile( ppile, &output_index_0, pindex_ptr, sizeof( UNS_8 ) );

	rv += ALERTS_pull_object_off_pile( ppile, &reason_in_list, pindex_ptr, sizeof(reason_in_list) );

	rv += ALERTS_pull_object_off_pile( ppile, &reason_skipped, pindex_ptr, sizeof(reason_skipped) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		// 7/31/2015 mpd : The irrigation reason strings are valid for the limited values used for lights.
		snprintf( pdest, pdest_size, "SKIPPED: Some or All %s (%s)", GetReasonOnStr( reason_in_list ), irrigation_skipped_reason_str[ reason_skipped ] );
	}

	return( rv );
}
#endif

#if 0
/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_LIGHTS_Activity_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, const UNS_32 paid, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 station_0_u16

	UNS_16 reason_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_OBJECT_off_pile( &station_0_u16, pindex_ptr, sizeof( station_0_u16 ) );
	rv += ALERTS_pull_OBJECT_off_pile( &reason_u16, pindex_ptr, sizeof( reason_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Lights %d turned ", (station_0_u16 + 1) );

		if( paid == AID_LIGHTS_STATION_OFF )
		{
			strlcat( pdest, "OFF", pdest_size );
		}
		else
		{
			strlcat( pdest, "ON", pdest_size );
		}

		if( reason_u16 == LIGHTS_REASON__ON_DUE_TO_COMMUNICATIONS )
		{
			strlcat( pdest, " (comm)", pdest_size );
		}
		else if( reason_u16 == LIGHTS_REASON__ON_DUE_TO_MANUALLY_STARTED )
		{
			strlcat( pdest, " (keypad)", pdest_size );
		}
		else if( reason_u16 == LIGHTS_REASON__ON_DUE_TO_ON_TIME )
		{
			strlcat( pdest, " (start time)", pdest_size );
		}
		else if( reason_u16 == LIGHTS_REASON__OFF_DUE_TO_OFF_TIME )
		{
			strlcat( pdest, " (stop time)", pdest_size );
		}
		else if( reason_u16 == nlu_LIGHTS_REASON__OFF_DUE_TO_PROGRAM_RESTART )
		{
			strlcat( pdest, " (program restart)", pdest_size );
		}
		else if( reason_u16 == LIGHTS_REASON__OFF_DUE_TO_STOP_KEY )
		{
			strlcat( pdest, " (keypad)", pdest_size );
		}
		else if( reason_u16 == nlu_LIGHTS_REASON__OFF_DUE_TO_TIME_EXPIRED )
		{
			strlcat( pdest, " (stop time)", pdest_size );
		}
		else if( reason_u16 == LIGHTS_REASON__OFF_DUE_TO_USER_TURNED_OFF )
		{
			strlcat( pdest, " (keypad)", pdest_size );
		}
	}

	return( rv );
}
#endif

/* ---------------------------------------------------------- */
#if 0
static UNS_32 ALERTS_pull_PAUSE_RESUME_alerts_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, const UNS_32 paid, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 reason_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &reason_u16, pindex_ptr, sizeof( reason_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( paid == AID_IRRIGATION_PAUSED )
		{
			strlcpy( pdest, "Irrigation Paused: ", pdest_size );
		}
		else if( paid == AID_IRRIGATION_RESUMED )
		{
			strlcpy( pdest, "Irrigation Resumed: ", pdest_size );
		}

		if( reason_u16 == PR_DUE_TO_WIND )
		{
			strlcat( pdest, "WIND", pdest_size );
		}
		else if( reason_u16 == PR_DUE_TO_KEYPAD )
		{
			strlcat( pdest, "KEYPAD", pdest_size );
		}
		else if( reason_u16 == PR_DUE_TO_CENTRAL_COMMLINK )
		{
			strlcat( pdest, "CENTRAL", pdest_size );
		}
		else
		{
			strlcat( pdest, "unknown", pdest_size );
		}
	}

	return( rv );
}
#endif

/* ---------------------------------------------------------- */
#if 0
static UNS_32 ALERTS_pull_POC_MainlineBreak_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	// This function is responsible to build the string for the mainline break
	// alert in pdest. It needs to keep pindex_ptr current to the next place the
	// data comes from. Must return how many bytes it pulls off of the pile for
	// this alert.
	//
	// pdest points to a string and can be concatenated to as a string

	UNS_16 poc_index_u16, measured_u16, allowed_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &poc_index_u16, pindex_ptr, sizeof( poc_index_u16 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &measured_u16, pindex_ptr, sizeof( measured_u16 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &allowed_u16, pindex_ptr, sizeof( allowed_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "MAINLINE BREAK POC %d: measured %d gpm / allowed %d gpm", (poc_index_u16 + 1), measured_u16, allowed_u16 );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_POC_MainlineBreak_reminder_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 poc_index_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &poc_index_u16, pindex_ptr, sizeof( poc_index_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "MAINLINE BREAK POC %d (reminder)", (poc_index_u16 + 1) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_POC_MainlineBreak_cleared_keypad_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 poc_index_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &poc_index_u16, pindex_ptr, sizeof( poc_index_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Mainline Break Cleared POC %d (keypad)", (poc_index_u16 + 1) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_POC_MainlineBreak_cleared_commlink_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 poc_index_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &poc_index_u16, pindex_ptr, sizeof( poc_index_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Mainline Break Cleared POC %d (comm)", (poc_index_u16 + 1) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_POC_MainlineBreak_cleared_short_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 poc_index_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &poc_index_u16, pindex_ptr, sizeof( poc_index_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Mainline Break Cleared POC %d (MV Short)", (poc_index_u16 + 1));
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_POC_MV_Short_During_MLB_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 poc_index_u16;

	UNS_16 repeats_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &poc_index_u16, pindex_ptr, sizeof( poc_index_u16 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &repeats_u16, pindex_ptr, sizeof( repeats_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "SHORT: MV %d during MLB (Repeats: %d)", (poc_index_u16 + 1), repeats_u16 );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_POC_Hit_StartTime_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 poc_index_u16;

	UNS_32 seconds;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_OBJECT_off_pile( &poc_index_u16, pindex_ptr, sizeof( UNS_16 ) );
	rv += ALERTS_pull_OBJECT_off_pile( &seconds, pindex_ptr, sizeof( unsigned long ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( seconds < 3600 )
		{
			snprintf( pdest, pdest_size, "POC %d Use Started for %d min", (poc_index_u16 + 1), (seconds / 60) );
		}
		else
		{
			snprintf( pdest, pdest_size, "POC %d Use Started for %d hr %d min", (poc_index_u16 + 1), (seconds / 3600), ((seconds % 3600) / 60) );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_POC_Hit_EndTime_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 poc_index_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &poc_index_u16, pindex_ptr, sizeof( poc_index_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "POC %d Use Ended", (poc_index_u16 + 1) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_POC_Stopped_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 poc_index_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &poc_index_u16, pindex_ptr, sizeof( poc_index_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "POC %d Use Stopped", (poc_index_u16 + 1) );
	}

	return( rv );
}
#endif

/* ---------------------------------------------------------- */
#if 0
static UNS_32 ALERTS_pull_UserDefined_ProgramTag_Changed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 prog_u16;

	char from_str_24[ 24 ], to_str_24[ 24 ];

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &prog_u16, pindex_ptr, sizeof( prog_u16 ) );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&from_str_24, sizeof(from_str_24), pindex_ptr );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&to_str_24, sizeof(to_str_24), pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
	#ifndef _MSC_VER

		// Don't display the "Change:" text if the alert is being generated
		// for the Change Lines report.
		if( ppile == &alerts_struct_changes )
		{
			strlcpy( pdest, "\0", pdest_size );
		}
		else
		{
			strlcpy( pdest, "Change: ", pdest_size );
		}

	#else

		strlcpy( pdest, "Change: ", pdest_size );

	#endif


		sp_strlcat( pdest, pdest_size, "%s User Defined Tag from %s to %s", E_ProgStrWithoutColon[ prog_u16 ], from_str_24, to_str_24 );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_ProgramTag_Changed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 prog_u16;

	char from_str_24[ 24 ], to_str_24[ 24 ];

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &prog_u16, pindex_ptr, sizeof( prog_u16 ) );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&from_str_24, sizeof(from_str_24), pindex_ptr );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&to_str_24, sizeof(to_str_24), pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
	#ifndef _MSC_VER

		// Don't display the "Change:" text if the alert is being generated
		// for the Change Lines report.
		if( ppile == &alerts_struct_changes )
		{
			strlcpy( pdest, "\0", pdest_size );
		}
		else
		{
			strlcpy( pdest, "Change: ", pdest_size );
		}

	#else

		strlcpy( pdest, "Change: ", pdest_size );

	#endif


		sp_strlcat( pdest, pdest_size, "%s Tag from %s to %s", E_ProgStrWithoutColon[ prog_u16 ], from_str_24, to_str_24 ) 
	}

	return( rv );
}
#endif

/* ---------------------------------------------------------- */
#if 0
static UNS_32 ALERTS_pull_PercentAdjustAllProgs_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 percentage_u16;

	UNS_16 days_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_OBJECT_off_pile( &percentage_u16, pindex_ptr, sizeof( percentage_u16 ) );
	rv += ALERTS_pull_OBJECT_off_pile( &days_u16, pindex_ptr, sizeof( days_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( percentage_u16 == 100 )
		{
			if( days_u16 == PERCENT_ADJUST_NO_EXPIRE )
			{
				snprintf( pdest, pdest_size, "Percent Adjust Set: All Programs to %d%% forever", (percentage_u16 - 100) ); 
			}
			else
			{
				snprintf( pdest, pdest_size, "Percent Adjust Set: All Programs to %d%% for %d days", (percentage_u16 - 100), days_u16 ); 
			}
		}
		else if( percentage_u16 > 100 )
		{
			if( days_u16 == PERCENT_ADJUST_NO_EXPIRE )
			{
				snprintf( pdest, pdest_size, "Percent Adjust Set: All Programs to +%d%% forever", (percentage_u16 - 100) ); 
			}
			else
			{
				snprintf( pdest, pdest_size, "Percent Adjust Set: All Programs to +%d%% for %d days", (percentage_u16 - 100), days_u16 ); 
			}
		}
		else // if ( percentage_u16 < 100 )
		{
			if( days_u16 == PERCENT_ADJUST_NO_EXPIRE )
			{
				snprintf( pdest, pdest_size, "Percent Adjust Set: All Programs to -%d%% forever", (100 - percentage_u16) ); 
			}
			else
			{
				snprintf( pdest, pdest_size, "Percent Adjust Set: All Programs to -%d%% for %d days", (100 - percentage_u16), days_u16 ); 
			}
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_PercentAdjustByProg_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_16 prog_u16;

	UNS_16 percentage_u16;

	UNS_16 days_u16;

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_OBJECT_off_pile( &prog_u16, pindex_ptr, sizeof( prog_u16 ) );
	rv += ALERTS_pull_OBJECT_off_pile( &percentage_u16, pindex_ptr, sizeof( percentage_u16 ) );
	rv += ALERTS_pull_OBJECT_off_pile( &days_u16, pindex_ptr, sizeof( days_u16 ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( percentage_u16 == 100 )
		{
			if( days_u16 == PERCENT_ADJUST_NO_EXPIRE )
			{
				snprintf( pdest, pdest_size, "Percent Adjust Set: %s to %d%% forever", E_ProgStrWithoutColon[ prog_u16 ], (percentage_u16 - 100) ); 
			}
			else
			{
				snprintf( pdest, pdest_size, "Percent Adjust Set: %s to %d%% for %d days", E_ProgStrWithoutColon[ prog_u16 ], (percentage_u16 - 100), days_u16 ); 
			}
		}
		else if( percentage_u16 > 100 )
		{
			if( days_u16 == PERCENT_ADJUST_NO_EXPIRE )
			{
				snprintf( pdest, pdest_size, "Percent Adjust Set: %s to +%d%% forever", E_ProgStrWithoutColon[ prog_u16 ], (percentage_u16 - 100) ); 
			}
			else
			{
				snprintf( pdest, pdest_size, "Percent Adjust Set: %s to +%d%% for %d days", E_ProgStrWithoutColon[ prog_u16 ], (percentage_u16 - 100), days_u16 ); 
			}
		}
		else // if ( percentage_u16 < 100 )
		{
			if( days_u16 == PERCENT_ADJUST_NO_EXPIRE )
			{
				snprintf( pdest, pdest_size, "Percent Adjust Set: %s to -%d%% forever", E_ProgStrWithoutColon[ prog_u16 ], (100 - percentage_u16) ); 
			}
			else
			{
				snprintf( pdest, pdest_size, "Percent Adjust Set: %s to -%d%% for %d days", E_ProgStrWithoutColon[ prog_u16 ], (100 - percentage_u16), days_u16 ); 
			}
		}
	}

	return( rv );
}
#endif

/* ---------------------------------------------------------- */
#if 0
static UNS_32 ALERTS_pull_SICStateChanged_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8 addr_u8;

	UNS_8 function_u8;

	UNS_8 state_u8;

	char addr_switch_is_on_str_8[ 8 ];

	char switch_name[ 25 ];

	UNS_32 rv;

	rv = 0;
	rv += ALERTS_pull_OBJECT_off_pile( &addr_u8, pindex_ptr, sizeof( addr_u8 ) );
	rv += ALERTS_pull_OBJECT_off_pile( &function_u8, pindex_ptr, sizeof( function_u8 ) );
	rv += ALERTS_pull_OBJECT_off_pile( &state_u8, pindex_ptr, sizeof( state_u8 ) );
	rv += ALERTS_pull_STRING_off_pile( &switch_name, pindex_ptr );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( addr_u8 == ConfigTable.Items.MY_COMMUNICATION_ADDR[ 2 ] )
		{
			// Since the switch is on this controller, don't include the address
			strlcpy( addr_switch_is_on_str_8, "", sizeof(addr_switch_is_on_str_8) );
		}
		else
		{
			snprintf( addr_switch_is_on_str_8, sizeof(addr_switch_is_on_str_8), "at \"%c\" ", addr_u8 );
		}

		if( function_u8 == SIC_FUNCTIONALITY_operate_as_a_rain_switch )
		{
			if( state_u8 == SIC_SWITCH_STATUS_active )
			{
				snprintf( pdest, pdest_size, "Rain Switch %sshowing Rain", addr_switch_is_on_str_8 );
			}
			else
			{
				snprintf( pdest, pdest_size, "Rain Switch %shas dried out", addr_switch_is_on_str_8 );
			}
		}
		else if( function_u8 == SIC_FUNCTIONALITY_operate_as_a_freeze_switch )
		{
			if( state_u8 == SIC_SWITCH_STATUS_active )
			{
				snprintf( pdest, pdest_size, "Freeze Switch %spreventing irrigation", addr_switch_is_on_str_8 );
			}
			else
			{
				snprintf( pdest, pdest_size, "Freeze Switch %snow above freezing", addr_switch_is_on_str_8 );
			}
		}
		else if( function_u8 == SIC_ACTION_TO_TAKE_stop__throw_away_remaining_time )
		{
			if( state_u8 == SIC_SWITCH_STATUS_active )
			{
				snprintf( pdest, pdest_size, "%s %spreventing irrigation", switch_name, addr_switch_is_on_str_8 );
			}
			else
			{
				snprintf( pdest, pdest_size, "%s %sallowing irrigation again", switch_name, addr_switch_is_on_str_8 );
			}
		}
		else if( function_u8 == SIC_ACTION_TO_TAKE_pause )
		{
			if( state_u8 == SIC_SWITCH_STATUS_active )
			{
				snprintf( pdest, pdest_size, "%s %spausing irrigation", switch_name, addr_switch_is_on_str_8 );
			}
			else
			{
				snprintf( pdest, pdest_size, "%s %sallowing irrigation again", switch_name, addr_switch_is_on_str_8 );
			}
		}
	}

	return( rv );
} 
#endif

/* ---------------------------------------------------------- */
static UNS_32 nlu_ALERTS_pull_moisture_reading_obtained_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	float   lmoisture_vwc;

	INT_32  ltemperature;

	UNS_32  lconductivity;

	UNS_32  rv;

	// ----------

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lmoisture_vwc, pindex_ptr, sizeof( lmoisture_vwc ) );
	rv += ALERTS_pull_object_off_pile( ppile, &ltemperature, pindex_ptr, sizeof( ltemperature ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lconductivity, pindex_ptr, sizeof( lconductivity ) );

	// ----------

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Mois: VWC=%5.3f T=%d EC=%u", lmoisture_vwc, ltemperature, lconductivity );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_moisture_reading_obtained_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	float   lmoisture_vwc;

	INT_32  ltemperature;

	float  lconductivity;

	UNS_32  rv;

	// ----------

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lmoisture_vwc, pindex_ptr, sizeof( lmoisture_vwc ) );
	rv += ALERTS_pull_object_off_pile( ppile, &ltemperature, pindex_ptr, sizeof( ltemperature ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lconductivity, pindex_ptr, sizeof( lconductivity ) );

	// ----------

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Mois: VWC=%5.3f T=%d EC=%5.3f", lmoisture_vwc, ltemperature, lconductivity );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_moisture_reading_out_of_range_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lmodel;

	UNS_32  lmoisture_vwc;

	INT_32  ltemperature;

	UNS_32  lconductivity;

	UNS_32  rv;

	// ----------

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lmodel, pindex_ptr, sizeof( lmodel ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lmoisture_vwc, pindex_ptr, sizeof( lmoisture_vwc ) );
	rv += ALERTS_pull_object_off_pile( ppile, &ltemperature, pindex_ptr, sizeof( ltemperature ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lconductivity, pindex_ptr, sizeof( lconductivity ) );

	// ----------

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "Mois out of range: VWC=%u T=%d EC=%u (model=%u)", lmoisture_vwc, ltemperature, lconductivity, lmodel );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_soil_moisture_crossed_threshold_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_32  ldecoder_serial_num;

	float	lmoisture;

	float	llower_threshold;

	float	lupper_threshold;

	MOISTURE_SENSOR_ALERT_TYPE	lmois_alert_type;

	UNS_32  rv;

	// ----------

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &ldecoder_serial_num, pindex_ptr, sizeof( ldecoder_serial_num ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lmoisture, pindex_ptr, sizeof( lmoisture ) );
	rv += ALERTS_pull_object_off_pile( ppile, &llower_threshold, pindex_ptr, sizeof( llower_threshold ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lupper_threshold, pindex_ptr, sizeof( lupper_threshold ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lmois_alert_type, pindex_ptr, sizeof( lmois_alert_type ) );

	// ----------

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( lmois_alert_type == MOISTURE_SENSOR_THRESHOLD_CROSSED_UPPER )
		{
			snprintf( pdest, pdest_size, "Decoder S/N %07d soil moisture level %5.3f above acceptable level %5.3f", ldecoder_serial_num, lmoisture, lupper_threshold );
		}
		else if( lmois_alert_type == MOISTURE_SENSOR_THRESHOLD_CROSSED_LOWER )
		{
			snprintf( pdest, pdest_size, "Decoder S/N %07d soil moisture level %5.3f below acceptable level %5.3f", ldecoder_serial_num, lmoisture, llower_threshold );
		}
		else	// MOISTURE_SENSOR_THRESHOLD_WITHIN_RANGE
		{
			snprintf( pdest, pdest_size, "Decoder S/N %07d soil moisture level %5.3f back within range", ldecoder_serial_num, lmoisture );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_soil_temperature_crossed_threshold_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_32  ldecoder_serial_num;

	INT_32	ltemperature;

	INT_32	llower_threshold;

	INT_32	lupper_threshold;

	MOISTURE_SENSOR_ALERT_TYPE	lmois_alert_type;

	UNS_32  rv;

	// ----------

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &ldecoder_serial_num, pindex_ptr, sizeof( ldecoder_serial_num ) );
	rv += ALERTS_pull_object_off_pile( ppile, &ltemperature, pindex_ptr, sizeof( ltemperature ) );
	rv += ALERTS_pull_object_off_pile( ppile, &llower_threshold, pindex_ptr, sizeof( llower_threshold ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lupper_threshold, pindex_ptr, sizeof( lupper_threshold ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lmois_alert_type, pindex_ptr, sizeof( lmois_alert_type ) );

	// ----------

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( lmois_alert_type == MOISTURE_SENSOR_THRESHOLD_CROSSED_UPPER )
		{
			snprintf( pdest, pdest_size, "Decoder S/N %07d soil temperature %d F above acceptable level %d", ldecoder_serial_num, ltemperature, lupper_threshold );
		}
		else if( lmois_alert_type == MOISTURE_SENSOR_THRESHOLD_CROSSED_LOWER )
		{
			snprintf( pdest, pdest_size, "Decoder S/N %07d soil temperature %d F below acceptable level %d", ldecoder_serial_num, ltemperature, llower_threshold );
		}
		else	// MOISTURE_SENSOR_THRESHOLD_WITHIN_RANGE
		{
			snprintf( pdest, pdest_size, "Decoder S/N %07d soil temperature %d F back within range", ldecoder_serial_num, ltemperature );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_soil_conductivity_crossed_threshold_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_32  ldecoder_serial_num;

	float	lconductivity;

	float	llower_threshold;

	float	lupper_threshold;

	MOISTURE_SENSOR_ALERT_TYPE	lmois_alert_type;

	UNS_32  rv;

	// ----------

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &ldecoder_serial_num, pindex_ptr, sizeof( ldecoder_serial_num ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lconductivity, pindex_ptr, sizeof( lconductivity ) );
	rv += ALERTS_pull_object_off_pile( ppile, &llower_threshold, pindex_ptr, sizeof( llower_threshold ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lupper_threshold, pindex_ptr, sizeof( lupper_threshold ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lmois_alert_type, pindex_ptr, sizeof( lmois_alert_type ) );

	// ----------

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( lmois_alert_type == MOISTURE_SENSOR_THRESHOLD_CROSSED_UPPER )
		{
			snprintf( pdest, pdest_size, "Decoder S/N %07d soil conductivity %5.3f above acceptable level %5.3f", ldecoder_serial_num, lconductivity, lupper_threshold );
		}
		else if( lmois_alert_type == MOISTURE_SENSOR_THRESHOLD_CROSSED_LOWER )
		{
			snprintf( pdest, pdest_size, "Decoder S/N %07d soil conductivity %5.3f below acceptable level %5.3f", ldecoder_serial_num, lconductivity, llower_threshold );
		}
		else	// MOISTURE_SENSOR_THRESHOLD_WITHIN_RANGE
		{
			snprintf( pdest, pdest_size, "Decoder S/N %07d soil conductivity %5.3f back within range", ldecoder_serial_num, lconductivity );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_station_added_to_group_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char str_64[ 64 ];

	char str_16[ 16 ];

	UNS_8   lbox_index_0;

	UNS_16  lstation_number_0;

	UNS_16  lreason_for_change;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lstation_number_0, pindex_ptr, sizeof( lstation_number_0 ) );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)str_64, sizeof(str_64), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lreason_for_change, pindex_ptr, sizeof( lreason_for_change ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		#ifndef _MSC_VER

			// Don't display the "Change:" text if the alert is being generated
			// for the Change Lines report.
			if( ppile == &alerts_struct_changes )
			{
				strlcpy( pdest, "\0", pdest_size );
			}
			else
			{
				strlcpy( pdest, "Change: ", pdest_size );
			}

		#else

			strlcpy( pdest, "Change: ", pdest_size );

		#endif


		sp_strlcat( pdest, pdest_size, "Station %s assigned to \"%s\"%s", ALERTS_get_station_number_with_or_without_two_wire_indicator( lbox_index_0, lstation_number_0, (char*)&str_16, sizeof(str_16) ), str_64, GetChangeReasonStr( lreason_for_change ) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_station_removed_from_group_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char str_64[ 64 ];

	char str_16[ 16 ];

	UNS_8   lbox_index_0;

	UNS_16  lstation_number_0;

	UNS_16  lreason_for_change;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lstation_number_0, pindex_ptr, sizeof( lstation_number_0 ) );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)str_64, sizeof(str_64), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lreason_for_change, pindex_ptr, sizeof( lreason_for_change ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
#ifndef _MSC_VER

		// Don't display the "Change:" text if the alert is being generated
		// for the Change Lines report.
		if( ppile == &alerts_struct_changes )
		{
			strlcpy( pdest, "\0", pdest_size );
		}
		else
		{
			strlcpy( pdest, "Change: ", pdest_size );
		}

#else

		strlcpy( pdest, "Change: ", pdest_size );

#endif


		sp_strlcat( pdest, pdest_size, "Station %s removed from \"%s\"%s", ALERTS_get_station_number_with_or_without_two_wire_indicator( lbox_index_0, lstation_number_0, (char*)&str_16, sizeof(str_16) ), str_64, GetChangeReasonStr( lreason_for_change ) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_station_copied_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char str_64[ 64 ];

	char str_16[ 16 ];

	UNS_8   lbox_index_0;

	UNS_16  lstation_number_0;

	UNS_16  lreason_for_change;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lstation_number_0, pindex_ptr, sizeof( lstation_number_0 ) );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)str_64, sizeof(str_64), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lreason_for_change, pindex_ptr, sizeof( lreason_for_change ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
#ifndef _MSC_VER

		// Don't display the "Change:" text if the alert is being generated
		// for the Change Lines report.
		if( ppile == &alerts_struct_changes )
		{
			strlcpy( pdest, "\0", pdest_size );
		}
		else
		{
			strlcpy( pdest, "Change: ", pdest_size );
		}

#else

		strlcpy( pdest, "Change: ", pdest_size );

#endif


		sp_strlcat( pdest, pdest_size, "Station %s settings copied to all stations in \"%s\"%s", ALERTS_get_station_number_with_or_without_two_wire_indicator( lbox_index_0, lstation_number_0, (char*)&str_16, sizeof(str_16) ), str_64, GetChangeReasonStr( lreason_for_change ) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_station_moved_from_one_group_to_another_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char from_group_64[ 64 ];

	char to_group_64[ 64 ];

	char str_16[ 16 ];

	UNS_8   lbox_index_0;

	UNS_16  lstation_number_0;

	UNS_16  lreason_for_change;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lstation_number_0, pindex_ptr, sizeof( lstation_number_0 ) );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)from_group_64, sizeof(from_group_64), pindex_ptr );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)to_group_64, sizeof(to_group_64), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lreason_for_change, pindex_ptr, sizeof( lreason_for_change ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
#ifndef _MSC_VER

		// Don't display the "Change:" text if the alert is being generated
		// for the Change Lines report.
		if( ppile == &alerts_struct_changes )
		{
			strlcpy( pdest, "\0", pdest_size );
		}
		else
		{
			strlcpy( pdest, "Change: ", pdest_size );
		}

#else

		strlcpy( pdest, "Change: ", pdest_size );

#endif

		sp_strlcat( pdest, pdest_size, "Station %s moved from \"%s\" to \"%s\"%s", ALERTS_get_station_number_with_or_without_two_wire_indicator( lbox_index_0, lstation_number_0, (char*)&str_16, sizeof(str_16) ), from_group_64, to_group_64, GetChangeReasonStr( lreason_for_change ) );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_station_card_added_or_removed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lbox_index_0;

	UNS_8   lcard_number_0;

	UNS_8   linstalled;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lcard_number_0, pindex_ptr, sizeof( lcard_number_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &linstalled, pindex_ptr, sizeof( linstalled ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "Station Card %d-%d %s: Controller %c", (8 * (lcard_number_0 + 1) - 7), (8 * (lcard_number_0 + 1)), installed_or_removed_str[ linstalled ], (lbox_index_0 + 'A') );
		}
		else
		{
			snprintf( pdest, pdest_size, "Station Card %d-%d %s", (8 * (lcard_number_0 + 1) - 7), (8 * (lcard_number_0 + 1)), installed_or_removed_str[ linstalled ] );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_lights_card_added_or_removed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lbox_index_0;

	UNS_8   linstalled;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &linstalled, pindex_ptr, sizeof( linstalled ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "Lights Card %s: Controller %c", installed_or_removed_str[ linstalled ], (lbox_index_0 + 'A') );
		}
		else
		{
			snprintf( pdest, pdest_size, "Lights Card %s", installed_or_removed_str[ linstalled ] );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_poc_card_added_or_removed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lbox_index_0;

	UNS_8   linstalled;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &linstalled, pindex_ptr, sizeof( linstalled ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "POC Card %s: Controller %c", installed_or_removed_str[ linstalled ], (lbox_index_0 + 'A') );
		}
		else
		{
			snprintf( pdest, pdest_size, "POC Card %s", installed_or_removed_str[ linstalled ] );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_weather_card_added_or_removed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lbox_index_0;

	UNS_8   linstalled;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &linstalled, pindex_ptr, sizeof( linstalled ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "Weather Card %s: Controller %c", installed_or_removed_str[ linstalled ], (lbox_index_0 + 'A') );
		}
		else
		{
			snprintf( pdest, pdest_size, "Weather Card %s", installed_or_removed_str[ linstalled ] );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_communication_card_added_or_removed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lbox_index_0;

	UNS_8   linstalled;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &linstalled, pindex_ptr, sizeof( linstalled ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "Communication Card %s: Controller %c", installed_or_removed_str[ linstalled ], (lbox_index_0 + 'A') );
		}
		else
		{
			snprintf( pdest, pdest_size, "Communication Card %s", installed_or_removed_str[ linstalled ] );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_station_terminal_added_or_removed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lbox_index_0;

	UNS_8   lterminal_number_0;

	UNS_8   linstalled;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lterminal_number_0, pindex_ptr, sizeof( lterminal_number_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &linstalled, pindex_ptr, sizeof( linstalled ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "Station Terminal %d-%d %s: Controller %c", (8 * (lterminal_number_0 + 1) - 7), (8 * (lterminal_number_0 + 1)), installed_or_removed_str[ linstalled ], (lbox_index_0 + 'A') );
		}
		else
		{
			snprintf( pdest, pdest_size, "Station Terminal %d-%d %s", (8 * (lterminal_number_0 + 1) - 7), (8 * (lterminal_number_0 + 1)), installed_or_removed_str[ linstalled ] );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_lights_terminal_added_or_removed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lbox_index_0;

	UNS_8   linstalled;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &linstalled, pindex_ptr, sizeof( linstalled ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "Lights Terminal %s: Controller %c", installed_or_removed_str[ linstalled ], (lbox_index_0 + 'A') );
		}
		else
		{
			snprintf( pdest, pdest_size, "Lights Terminal %s", installed_or_removed_str[ linstalled ] );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_poc_terminal_added_or_removed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lbox_index_0;

	UNS_8   linstalled;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &linstalled, pindex_ptr, sizeof( linstalled ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "POC Terminal %s: Controller %c", installed_or_removed_str[ linstalled ], (lbox_index_0 + 'A') );
		}
		else
		{
			snprintf( pdest, pdest_size, "POC Terminal %s", installed_or_removed_str[ linstalled ] );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_weather_terminal_added_or_removed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lbox_index_0;

	UNS_8   linstalled;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &linstalled, pindex_ptr, sizeof( linstalled ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "Weather Terminal %s: Controller %c", installed_or_removed_str[ linstalled ], (lbox_index_0 + 'A') );
		}
		else
		{
			snprintf( pdest, pdest_size, "Weather Terminal %s", installed_or_removed_str[ linstalled ] );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_communication_terminal_added_or_removed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lbox_index_0;

	UNS_8   linstalled;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &linstalled, pindex_ptr, sizeof( linstalled ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "Communication Terminal %s: Controller %c", installed_or_removed_str[ linstalled ], (lbox_index_0 + 'A') );
		}
		else
		{
			snprintf( pdest, pdest_size, "Communication Terminal %s", installed_or_removed_str[ linstalled ] );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_two_wire_terminal_added_or_removed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	UNS_8   lbox_index_0;

	UNS_8   linstalled;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );
	rv += ALERTS_pull_object_off_pile( ppile, &linstalled, pindex_ptr, sizeof( linstalled ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( FLOWSENSE_we_are_poafs() )
		{
			snprintf( pdest, pdest_size, "2-Wire Terminal %s: Controller %c", installed_or_removed_str[ linstalled ], (lbox_index_0 + 'A') );
		}
		else
		{
			snprintf( pdest, pdest_size, "2-Wire Terminal %s", installed_or_removed_str[ linstalled ] );
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_poc_assigned_to_mainline_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char lpoc_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	char lsystem_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_8   lbox_index_0;

	UNS_16  lreason_for_change;

	UNS_32  rv;

	// ----------

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)lpoc_name, sizeof(lpoc_name), pindex_ptr );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)lsystem_name, sizeof(lsystem_name), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lreason_for_change, pindex_ptr, sizeof( lreason_for_change ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );

	// ----------

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
#ifndef _MSC_VER

		// Don't display the "Change:" text if the alert is being generated
		// for the Change Lines report.
		if( ppile == &alerts_struct_changes )
		{
			strlcpy( pdest, "\0", pdest_size );
		}
		else
		{
			strlcpy( pdest, "Change: ", pdest_size );
		}

#else

		strlcpy( pdest, "Change: ", pdest_size );

#endif

		sp_strlcat( pdest, pdest_size, "%s assigned to %s%s", lpoc_name, lsystem_name, GetChangeReasonStr( lreason_for_change ) );
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_station_group_assigned_to_mainline_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char lgroup_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	char lsystem_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_8   lbox_index_0;

	UNS_16  lreason_for_change;

	UNS_32  rv;

	// ----------

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)lgroup_name, sizeof(lgroup_name), pindex_ptr );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)lsystem_name, sizeof(lsystem_name), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lreason_for_change, pindex_ptr, sizeof( lreason_for_change ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );

	// ----------

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
#ifndef _MSC_VER

		// Don't display the "Change:" text if the alert is being generated
		// for the Change Lines report.
		if( ppile == &alerts_struct_changes )
		{
			strlcpy( pdest, "\0", pdest_size );
		}
		else
		{
			strlcpy( pdest, "Change: ", pdest_size );
		}

#else

		strlcpy( pdest, "Change: ", pdest_size );

#endif

		sp_strlcat( pdest, pdest_size, "%s assigned to %s%s", lgroup_name, lsystem_name, GetChangeReasonStr( lreason_for_change ) );
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_station_group_assigned_to_a_moisture_sensor_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char lstation_group_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	char lmoisture_sensor_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_8   lbox_index_0;

	UNS_16  lreason_for_change;

	UNS_32  rv;

	// ----------

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)lstation_group_name, sizeof(lstation_group_name), pindex_ptr );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)lmoisture_sensor_name, sizeof(lmoisture_sensor_name), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lreason_for_change, pindex_ptr, sizeof( lreason_for_change ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );

	// ----------

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
#ifndef _MSC_VER

		// Don't display the "Change:" text if the alert is being generated
		// for the Change Lines report.
		if( ppile == &alerts_struct_changes )
		{
			strlcpy( pdest, "\0", pdest_size );
		}
		else
		{
			strlcpy( pdest, "Change: ", pdest_size );
		}

#else

		strlcpy( pdest, "Change: ", pdest_size );

#endif

		sp_strlcat( pdest, pdest_size, "Station Group \"%s\" uses Moisture Sensor \"%s\"%s", lstation_group_name, lmoisture_sensor_name, GetChangeReasonStr( lreason_for_change ) );
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 ALERTS_pull_walk_thru_station_added_or_removed_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, void *pdest, const UNS_32 pdest_size, UNS_32 *pindex_ptr )
{
	char str_16[ 16 ];

	UNS_8   lbox_index_0;
	
	char lwalk_thru_group_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	INT_32	lstation;
	
	BOOL_32	ladded;

	UNS_16  lreason_for_change;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, pindex_ptr, sizeof( lbox_index_0 ) );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)lwalk_thru_group_name, sizeof(lwalk_thru_group_name), pindex_ptr );
	rv += ALERTS_pull_object_off_pile( ppile, &lstation, pindex_ptr, sizeof( lstation ) );
	rv += ALERTS_pull_object_off_pile( ppile, &ladded, pindex_ptr, sizeof( ladded ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lreason_for_change, pindex_ptr, sizeof( lreason_for_change ) );

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
#ifndef _MSC_VER

		// Don't display the "Change:" text if the alert is being generated
		// for the Change Lines report.
		if( ppile == &alerts_struct_changes )
		{
			strlcpy( pdest, "\0", pdest_size );
		}
		else
		{
			strlcpy( pdest, "Change: ", pdest_size );
		}

#else

		strlcpy( pdest, "Change: ", pdest_size );

#endif

		if( ladded == WALK_THRU_STATION_ADDED )
		{
			sp_strlcat( pdest, pdest_size, "Station %s added to walk thru group \"%s\" %s", ALERTS_get_station_number_with_or_without_two_wire_indicator( lbox_index_0, lstation, (char*)&str_16, sizeof(str_16) ), lwalk_thru_group_name, GetChangeReasonStr( lreason_for_change ) );
		}
		else
		{
			sp_strlcat( pdest, pdest_size, "Station %s removed in walk thru group \"%s\" %s", ALERTS_get_station_number_with_or_without_two_wire_indicator( lbox_index_0, lstation, (char*)&str_16, sizeof(str_16) ), lwalk_thru_group_name, GetChangeReasonStr( lreason_for_change ) );
		}
		
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Pulls a change line off of the alerts pile and parses it into a printable
 * string for display on the Alerts report screen.
 *
 * @executed Executed within the context of the display processing task when the
 *           Alerts report is being generated.
 *
 * @param pparse_why The reason to parse the alert. Valid value are
 *                   PARSE_FOR_LENGTH_ONLY and PARSE_FOR_LENGTH_AND_TEXT.
 * @param paid The alert of the ID of the alert to parse.
 * @param toptr A pointer to the buffer to parse the alert into.
 * @param iptr A pointer to the buffer containing the information associated
 *             with the alert to parse.
 *
 * @return UNS_32 The total byte count of the data pulled from iptr.
 *
 * @author 10/21/2011 Adrianusv
 *
 * @revisions
 *    10/21/2011 Initial release
 *    2/17/2012  Updated scratch_from and to variables to reflect new 32-bit
 *  			 sizes.
 *    3/21/2012  Added processing of controller serial number, communication
 *  			 address, part of a chain, and number of controllers in the
 *  			 chain.
 */
static UNS_32 ALERTS_pull_change_line_off_pile( ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, const UNS_32 paid, void *const toptr, UNS_32 *iptr )
{
	typedef union
	{
		char            sizer[ MAX_CHANGE_LINE_VARIABLE_SIZE ];
		DATE_TIME       dt;
		float           fl;
		BOOL_32         b32;
		INT_32          i32;
		UNS_32          u32;
		UNS_16          u16;
		UNS_8           u8;

	} CHANGE_ALERT_SCRATCHPAD;

	CHANGE_ALERT_SCRATCHPAD lscratch_to, lscratch_from;

	char lgroup_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	char from_str_32[ 32 ], to_str_32[ 32 ];

	char lstation_number_str[ 16 ];

	UNS_32  lbox_index_0;

	UNS_32  lstation_number_0;

	UNS_32  lbox_index_of_initiator_0;

	UNS_32  lreason;

	UNS_32  lsize;

	UNS_32  rv;

	rv = 0;
	rv += ALERTS_pull_object_off_pile( ppile, &lreason, iptr, sizeof( lreason ) );
	rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_of_initiator_0, iptr, sizeof( lbox_index_of_initiator_0 ) );

	if( (paid >= CHANGE_GROUP_BASE) && (paid < CHANGE_STATION_BASE) )
	{
		rv += ALERTS_pull_object_off_pile( ppile, &lgroup_name, iptr, NUMBER_OF_CHARS_IN_A_NAME );
	}
	else if( (paid >= CHANGE_STATION_BASE) && (paid < CHANGE_CONTROLLER_BASE) )
	{
		rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, iptr, sizeof( lbox_index_0 ) );
		rv += ALERTS_pull_object_off_pile( ppile, &lstation_number_0, iptr, sizeof( lstation_number_0 ) );

		ALERTS_get_station_number_with_or_without_two_wire_indicator( lbox_index_0, lstation_number_0, (char*)&lstation_number_str, sizeof(lstation_number_str) );
	}
	else // if( paid >= CHANGE_ALL_OTHER_BASE )
	{
		rv += ALERTS_pull_object_off_pile( ppile, &lbox_index_0, iptr, sizeof( lbox_index_0 ) );
	}

	rv += ALERTS_pull_object_off_pile( ppile, &lsize, iptr, sizeof( lsize ) );

	if( lsize <= MAX_CHANGE_LINE_VARIABLE_SIZE )
	{
		if( lsize > 0 )
		{
			// Then we have the group name and the to and from variables present
			// to pull
			rv += ALERTS_pull_object_off_pile( ppile, &lscratch_from, iptr, lsize );
			rv += ALERTS_pull_object_off_pile( ppile, &lscratch_to, iptr, lsize );
		}

		if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
		{
			// If its greater than the CHANGE_LINE_BASE it is a change line.
			if( paid >= AID_CHANGE_LINE_BASE )
			{
#ifndef _MSC_VER

				if( ppile == &alerts_struct_engineering )
				{
					strlcpy( toptr, "Change: ", MAX_ALERT_LENGTH );
				}
				else
				{
					strlcpy( toptr, "\0", MAX_ALERT_LENGTH );
				}

#else

				strlcpy( toptr, "Change: ", MAX_ALERT_LENGTH );

#endif

				switch( paid )
				{
					case CHANGE_GROUP_CREATED:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "%s created", lgroup_name );
						break;

					case CHANGE_GROUP_DELETED:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "%s deleted", lgroup_name );
						break;

					case CHANGE_GROUP_NAME:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Group Name to %s", lgroup_name );
						break;

					case CHANGE_IRRIGATION_PRIORITY_LEVEL:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Priority Level for %s from %s to %s", lgroup_name, GetPriorityLevelStr( lscratch_from.u32 ), GetPriorityLevelStr( lscratch_to.u32 ) );
						break;

					case CHANGE_ALERT_ACTION_HIGH:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "High Flow Alert Action for %s from %s to %s", lgroup_name, GetAlertActionStr( lscratch_from.u32 ), GetAlertActionStr( lscratch_to.u32 ) );
						break;

					case CHANGE_ALERT_ACTION_LOW:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Low Flow Alert Action for %s from %s to %s", lgroup_name, GetAlertActionStr( lscratch_from.u32 ), GetAlertActionStr( lscratch_to.u32 ) );
						break;

					case CHANGE_ET_MODE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "ET in Use for %s from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_LINE_FILL_TIME:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Line Fill Time for %s from %d to %d sec", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_DELAY_BETWEEN_VALVES:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Delay Between Valves for %s from %d to %d sec", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_WIND_IN_USE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Wind In Use for %s from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_RAIN_IN_USE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Rain In Use for %s from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_PUMP_IN_USE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Pump In Use for %s from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_PERCENT_ADJUST:
						if( lscratch_from.u32 >= 100 )
						{
							if( lscratch_to.u32 >= 100 )
							{
								sp_strlcat( toptr, MAX_ALERT_LENGTH, "Percent Adjust for %s from +%d%% to +%d%%", lgroup_name, (lscratch_from.u32 - 100), (lscratch_to.u32 - 100) );
							}
							else
							{
								sp_strlcat( toptr, MAX_ALERT_LENGTH, "Percent Adjust for %s from +%d%% to %d%%", lgroup_name, (lscratch_from.u32 - 100), (lscratch_to.u32 - 100) );
							}
						}
						else
						{
							if( lscratch_to.u32 >= 100 )
							{
								sp_strlcat( toptr, MAX_ALERT_LENGTH, "Percent Adjust for %s from %d%% to +%d%%", lgroup_name, (lscratch_from.u32 - 100), (lscratch_to.u32 - 100) );
							}
							else
							{
								sp_strlcat( toptr, MAX_ALERT_LENGTH, "Percent Adjust for %s from %d%% to %d%%", lgroup_name, (lscratch_from.u32 - 100), (lscratch_to.u32 - 100) );
							}
						}
						break;

					case CHANGE_PERCENT_ADJUST_START_DATE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Percent Adjust Start Date for %s from %s to %s", lgroup_name, GetDateStr( from_str_32, sizeof(from_str_32), (UNS_32)lscratch_from.u32, DATESTR_show_long_year, DATESTR_show_long_dow ), GetDateStr( to_str_32, sizeof(to_str_32), (UNS_32)lscratch_to.u32, DATESTR_show_long_year, DATESTR_show_long_dow ) );
						break;

					case CHANGE_PERCENT_ADJUST_END_DATE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Percent Adjust End Date for %s from %s to %s", lgroup_name, GetDateStr( from_str_32, sizeof(from_str_32), (UNS_32)lscratch_from.u32, DATESTR_show_long_year, DATESTR_show_long_dow ), GetDateStr( to_str_32, sizeof(to_str_32), (UNS_32)lscratch_to.u32, DATESTR_show_long_year, DATESTR_show_long_dow ) );
						break;

					case CHANGE_ON_AT_A_TIME_IN_GROUP:
						if( lscratch_from.u32 == ON_AT_A_TIME_X )
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "On-at-a-Time in Group for %s from -- to %d", lgroup_name, lscratch_to.u32 );
						}
						else if( lscratch_to.u32 == ON_AT_A_TIME_X )
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "On-at-a-Time in Group for %s from %d to --", lgroup_name, lscratch_from.u32 );
						}
						else
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "On-at-a-Time in Group for %s from %d to %d", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						}
						break;

					case CHANGE_ON_AT_A_TIME_IN_SYSTEM:
						if( lscratch_from.u32 == ON_AT_A_TIME_X )
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "On-at-a-Time in System for %s from -- to %d", lgroup_name, lscratch_to.u32 );
						}
						else if( lscratch_to.u32 == ON_AT_A_TIME_X )
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "On-at-a-Time in System for %s from %d to --", lgroup_name, lscratch_from.u32 );
						}
						else
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "On-at-a-Time in System for %s from %d to %d", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						}
						break;

					case CHANGE_IRRIGATION_SCHEDULE_ENABLED:
						// 2012.08.15 ajv : This change line has been suppressed
						// for now since it's not a user settable variable.
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Schedule Enabled for %s from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_IRRIGATION_SCHEDULE_START_TIME:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Start Time for %s from %s to %s", lgroup_name, TDUTILS_time_to_time_string_with_ampm( from_str_32, sizeof( from_str_32 ), lscratch_from.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ), TDUTILS_time_to_time_string_with_ampm( to_str_32, sizeof( to_str_32 ), lscratch_to.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ) );
						break;

					case CHANGE_IRRIGATION_SCHEDULE_STOP_TIME:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Stop Time for %s from %s to %s", lgroup_name, TDUTILS_time_to_time_string_with_ampm( from_str_32, sizeof( from_str_32 ), lscratch_from.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ), TDUTILS_time_to_time_string_with_ampm( to_str_32, sizeof( to_str_32 ), lscratch_to.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ) );
						break;

					case CHANGE_IRRIGATION_SCHEDULE_MOW_DAY:
						if( lscratch_from.u32 == DAYS_IN_A_WEEK )
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "Mow Day for %s from %s to %s", lgroup_name, "None", GetDayLongStr( lscratch_to.u32 ) );
						}
						else if( lscratch_to.u32 == DAYS_IN_A_WEEK )
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "Mow Day for %s from %s to %s", lgroup_name, GetDayLongStr( lscratch_from.u32 ), "None" );
						}
						else
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "Mow Day for %s from %s to %s", lgroup_name, GetDayLongStr( lscratch_from.u32 ), GetDayLongStr( lscratch_to.u32 ) );
						}
						break;

					case CHANGE_IRRIGATION_SCHEDULE_TYPE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Days to Water for %s from ", lgroup_name );

						if( (lscratch_from.u32 >= SCHEDULE_TYPE_EVERY_THREE_DAYS) && (lscratch_from.u32 <= SCHEDULE_TYPE_EVERY_FOURTEEN_DAYS) )
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "Every %s days to ", GetScheduleTypeStr( lscratch_from.u32 ) );
						}
						else
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "%s to ", GetScheduleTypeStr( lscratch_from.u32 ) );
						}

						if( (lscratch_to.u32 >= SCHEDULE_TYPE_EVERY_THREE_DAYS) && (lscratch_to.u32 <= SCHEDULE_TYPE_EVERY_FOURTEEN_DAYS) )
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "Every %s days", GetScheduleTypeStr( lscratch_to.u32 ) );
						}
						else
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "%s", GetScheduleTypeStr( lscratch_to.u32 ) );
						}
						break;

					case CHANGE_IRRIGATION_SCHEDULE_WATER_DAY_SUN:
					case CHANGE_IRRIGATION_SCHEDULE_WATER_DAY_MON:
					case CHANGE_IRRIGATION_SCHEDULE_WATER_DAY_TUE:
					case CHANGE_IRRIGATION_SCHEDULE_WATER_DAY_WED:
					case CHANGE_IRRIGATION_SCHEDULE_WATER_DAY_THU:
					case CHANGE_IRRIGATION_SCHEDULE_WATER_DAY_FRI:
					case CHANGE_IRRIGATION_SCHEDULE_WATER_DAY_SAT:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Irrigate %s on %s from %s to %s", lgroup_name, GetDayShortStr( (UNS_32)(paid - CHANGE_IRRIGATION_SCHEDULE_WATER_DAY_SUN) ), GetNoYesStr( (UNS_32)lscratch_from.b32 ), GetNoYesStr( (UNS_32)lscratch_to.b32 ) );
						break;

					case CHANGE_IRRIGATION_SCHEDULE_BEGIN_DATE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Next Scheduled Date for %s from %s to %s", lgroup_name, GetDateStr( (char*)from_str_32, sizeof( from_str_32 ), lscratch_from.u32, DATESTR_show_long_year, DATESTR_show_long_dow ), GetDateStr( (char*)to_str_32, sizeof( to_str_32 ), lscratch_to.u32, DATESTR_show_long_year, DATESTR_show_long_dow ) );
						break;

					case CHANGE_IRRIGATION_SCHEDULE_IRRIGATE_ON_29_or_31:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Irrigate on 29th or 31st for %s from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_IRRIGATION_SYSTEM_USED_FOR_IRRI:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Mainline Used for Irrigation for %s from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_IRRIGATION_SYSTEM_CAPACITY_IN_USE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Mainline Capacity in Use for %s from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_IRRIGATION_SYSTEM_CAPACITY_WITH_PUMP:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Mainline Capacity with Pump for %s from %d gpm to %d gpm", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_IRRIGATION_SYSTEM_CAPACITY_WITHOUT_PUMP:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Mainline Capacity without Pump for %s from %d gpm to %d gpm", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_IRRIGATION_SYSTEM_MLB_DURING_IRRI:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Mainline Break during Irrigation for %s from %d gpm to %d gpm", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_IRRIGATION_SYSTEM_MLB_DURING_MVOR:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Mainline Break during MVOR for %s from %d gpm to %d gpm", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_IRRIGATION_SYSTEM_MLB_ALL_OTHER_TIMES:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Mainline Break All Other Times for %s from %d gpm to %d gpm", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_IN_USE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Flow Checking In Use for %s from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_1:
					case CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_2:
					case CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_3:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Flow Checking Top of Range %d for %s from %d gpm to %d gpm", (paid - CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_1 + 1), lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_TOL_PLUS_1:
					case CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_TOL_PLUS_2:
					case CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_TOL_PLUS_3:
					case CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_TOL_PLUS_4:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Flow Checking Flow Window %d for %s from +%d gpm to +%d gpm", (paid - CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_TOL_PLUS_1 + 1), lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_TOL_MINUS_1:
					case CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_TOL_MINUS_2:
					case CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_TOL_MINUS_3:
					case CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_TOL_MINUS_4:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Flow Checking Flow Window %d for %s from -%d gpm to -%d gpm", (paid - CHANGE_IRRIGATION_SYSTEM_FLOW_CHECKING_TOL_MINUS_1 + 1), lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_POC_BOX_INDEX:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "POC Controller Index for %s from %d to %d", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_POC_PHYSICALLY_AVAILABLE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "POC Physically Available for %s from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.u32 ), GetNoYesStr( lscratch_to.u32 ) );
						break;

					case CHANGE_POC_USAGE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "POC Usage for %s from %s to %s", lgroup_name, GetPOCUsageStr( lscratch_from.u32 ), GetPOCUsageStr( lscratch_to.u32 ) );
						break;

						// 7/15/2014 rmd : Best I can tell the following two defines are not used. They were likely
						// conceptual. But now incorporated into the POC_USAGE and POC__FILE_TYPE settings.
						/*
						case CHANGE_POC_USED_FOR_IRRIGATION: 
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "POC Used for Irrigation for %s from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
							break;
	
						case CHANGE_POC_USED_AS_BYPASS:
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "POC Used as a Bypass Manifold for %s from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
							break;
						*/

					case CHANGE_POC_MV_TYPE1:
					case CHANGE_POC_MV_TYPE2:
					case CHANGE_POC_MV_TYPE3:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Master Valve %d for %s from %s to %s", (paid - CHANGE_POC_MV_TYPE1 + 1), lgroup_name, GetMasterValveStr( lscratch_from.u32 ), GetMasterValveStr( lscratch_to.u32 ) );
						break;

					case CHANGE_POC_FM_TYPE1:
					case CHANGE_POC_FM_TYPE2:
					case CHANGE_POC_FM_TYPE3:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Flow Meter %d Type for %s from %s to %s", (paid - CHANGE_POC_FM_TYPE1 + 1), lgroup_name, GetFlowMeterStr( lscratch_from.u32 ), GetFlowMeterStr( lscratch_to.u32 ) );
						break;

					case CHANGE_POC_K_VALUE1:
					case CHANGE_POC_K_VALUE2:
					case CHANGE_POC_K_VALUE3:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "K value %d for %s from %7.3f to %7.3f", (UNS_32)(paid - CHANGE_POC_K_VALUE1 + 1), lgroup_name, (double)(lscratch_from.u32 / 100000.0), (double)(lscratch_to.u32 / 100000.0) );
						break;

					case CHANGE_POC_OFFSET1:
					case CHANGE_POC_OFFSET2:
					case CHANGE_POC_OFFSET3:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Offset %d for %s from %7.3f to %7.3f", (INT_32)(paid - CHANGE_POC_OFFSET1 + 1), lgroup_name, (double)(lscratch_from.i32 / 100000.0), (double)(lscratch_to.i32 / 100000.0) );
						break;

					case CHANGE_POC_REED_SWITCH_1:
					case CHANGE_POC_REED_SWITCH_2:
					case CHANGE_POC_REED_SWITCH_3:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Reed Switch %d for %s from %s to %s", (INT_32)(paid - CHANGE_POC_REED_SWITCH_1 + 1), lgroup_name, GetReedSwitchStr( lscratch_from.u32 ), GetReedSwitchStr( lscratch_to.u32 ) );
						break;

					case CHANGE_POC_BYPASS_NUMBER_OF_LEVELS:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Bypass Manifold Stages for %s from %d to %d", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_POC_MV_CLOSE_DELAY:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Master Valve Close Delay for %s from %d to %d sec", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_MANUAL_PROGRAMS_START_TIME_1:
					case CHANGE_MANUAL_PROGRAMS_START_TIME_2:
					case CHANGE_MANUAL_PROGRAMS_START_TIME_3:
					case CHANGE_MANUAL_PROGRAMS_START_TIME_4:
					case CHANGE_MANUAL_PROGRAMS_START_TIME_5:
					case CHANGE_MANUAL_PROGRAMS_START_TIME_6:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Start Time %d for %s from %s to %s", (paid - CHANGE_MANUAL_PROGRAMS_START_TIME_1 + 1), lgroup_name, TDUTILS_time_to_time_string_with_ampm( from_str_32, sizeof( from_str_32 ), lscratch_from.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ), TDUTILS_time_to_time_string_with_ampm( to_str_32, sizeof( to_str_32 ), lscratch_to.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ) );
						break;

					case CHANGE_MANUAL_PROGRAMS_WATER_DAY_SUN:
					case CHANGE_MANUAL_PROGRAMS_WATER_DAY_MON:
					case CHANGE_MANUAL_PROGRAMS_WATER_DAY_TUE:
					case CHANGE_MANUAL_PROGRAMS_WATER_DAY_WED:
					case CHANGE_MANUAL_PROGRAMS_WATER_DAY_THU:
					case CHANGE_MANUAL_PROGRAMS_WATER_DAY_FRI:
					case CHANGE_MANUAL_PROGRAMS_WATER_DAY_SAT:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Irrigate %s on %s from %s to %s", lgroup_name, GetDayShortStr( (UNS_32)(paid - CHANGE_MANUAL_PROGRAMS_WATER_DAY_SUN) ), GetNoYesStr( (UNS_32)lscratch_from.b32 ), GetNoYesStr( (UNS_32)lscratch_to.b32 ) );
						break;

					case CHANGE_MANUAL_PROGRAMS_START_DATE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Start Date for %s from %s to %s", lgroup_name, GetDateStr( (char*)from_str_32, sizeof( from_str_32 ), lscratch_from.u32, DATESTR_show_long_year, DATESTR_show_long_dow ), GetDateStr( (char*)to_str_32, sizeof( to_str_32 ), lscratch_to.u32, DATESTR_show_long_year, DATESTR_show_long_dow ) );
						break;

					case CHANGE_MANUAL_PROGRAMS_STOP_DATE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "End Date for %s from %s to %s", lgroup_name, GetDateStr( (char*)from_str_32, sizeof( from_str_32 ), lscratch_from.u32, DATESTR_show_long_year, DATESTR_show_long_dow ), GetDateStr( (char*)to_str_32, sizeof( to_str_32 ), lscratch_to.u32, DATESTR_show_long_year, DATESTR_show_long_dow ) );
						break;

					case CHANGE_MANUAL_PROGRAMS_RUN_TIME:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "%s Run Time from %d to %d min", lgroup_name, (lscratch_from.u32 / 60), (lscratch_to.u32 / 60) );
						break;

					case CHANGE_STATION_PHYSICALLY_AVAILABLE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s Installed from %s to %s", lstation_number_str, GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_STATION_IN_USE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s In Use from %s to %s", lstation_number_str, GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_STATION_TOTAL_RUN_MINUTES:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s Total Minutes from %0.1f to %0.1f min", lstation_number_str, (double)(lscratch_from.u32 / 10.0), (double)(lscratch_to.u32 / 10.0) );
						break;

					case CHANGE_STATION_CYCLE_MINUTES:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s Cycle Time from %0.1f to %0.1f min", lstation_number_str, (double)(lscratch_from.u32 / 10.0), (double)(lscratch_to.u32 / 10.0) );
						break;

					case CHANGE_STATION_SOAK_MINUTES:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s Soak-In Time from %d to %d min", lstation_number_str, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_STATION_ET_FACTOR:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s Adjust Factor from %d%% to %d%%", lstation_number_str, (lscratch_from.u32 - 100), (lscratch_to.u32 - 100) );
						break;

					case CHANGE_STATION_EXPECTED_FLOW_RATE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s Expected Flow Rate from %d to %d gpm", lstation_number_str, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_STATION_DISTRIBUTION_UNIFORMITY:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s Distribution Uniformity from %d%% to %d%%", lstation_number_str, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_STATION_NO_WATER_DAYS:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s No Water Days from %d to %d", lstation_number_str, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_STATION_DESCRIPTION:
						// TODO Determine how to display change line for station
						// description
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s Description", lstation_number_str );
						break;

					case CHANGE_STATION_DECODER_SERIAL_NUMBER:
						if( lscratch_from.u32 == 0 )
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s Assigned to Decoder S/N %07d", lstation_number_str, lscratch_to.u32 );
						}
						else if( lscratch_to.u32 == 0 )
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s No Longer Assigned to Decoder S/N %07d", lstation_number_str, lscratch_from.u32 );
						}
						else
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s Decoder Assignment from S/N %d to %07d", lstation_number_str, lscratch_from.u32, lscratch_to.u32 );
						}
						break;

					case CHANGE_STATION_DECODER_OUTPUT:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s Assigned to Decoder Output %c", lstation_number_str, (lscratch_to.u32 + 'A') );
						break;

					case CHANGE_STATION_MANUAL_PROG_A_RUN_TIME:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s Manual A Run Time from %d to %d min", lstation_number_str, (lscratch_from.u32 / 60), (lscratch_to.u32 / 60) );
						break;

					case CHANGE_STATION_MANUAL_PROG_B_RUN_TIME:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s Manual B Run Time from %d to %d min", lstation_number_str, (lscratch_from.u32 / 60), (lscratch_to.u32 / 60) );
						break;

					case CHANGE_STATION_NUMBER:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s Number from %d to %d", lstation_number_str, (lscratch_from.u32 + 1), (lscratch_to.u32 + 1) );
						break;

					case CHANGE_STATION_BOX_INDEX:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %d Controller Index from %d to %d", lstation_number_0 + 1, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_STATION_MOISTURE_BALANCE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s Moisture Balance from %.4f%% to %.4f%%", lstation_number_str, (double)(lscratch_from.u32 / 1000.0), (double)(lscratch_to.u32 / 1000.0) );
						break;

					case CHANGE_STATION_MOISTURE_BALANCE_FL:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s Moisture Balance from %.4f%% to %.4f%%", lstation_number_str, (lscratch_from.fl * 100.0F), (lscratch_to.fl * 100.0F) );
						break;

					case CHANGE_STATION_SQUARE_FOOTAGE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s Square Footage from %u to %u", lstation_number_str, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_STATION_IGNORE_MOISTURE_BALANCE_FLAG:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station %s accumulated rain reset", lstation_number_str );
						break;

					case CHANGE_DATE_TIME_CLOCK:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Clock set from %s to %s", DATE_TIME_to_DateTimeStr_32( from_str_32, sizeof( from_str_32 ), lscratch_from.dt ), DATE_TIME_to_DateTimeStr_32( to_str_32, sizeof( to_str_32 ), lscratch_to.dt ) );
						break;

					case CHANGE_NETWORK_CONFIG_TIME_ZONE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Time Zone from %s to %s", GetTimeZoneStr( lscratch_from.u8 ), GetTimeZoneStr( lscratch_to.u8 ) );
						break;

					case CHANGE_NETWORK_CONFIG_START_OF_IRRI_DAY:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Start of Irrigation Day from %s to %s", TDUTILS_time_to_time_string_with_ampm( from_str_32, sizeof( from_str_32 ), lscratch_from.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ), TDUTILS_time_to_time_string_with_ampm( to_str_32, sizeof( to_str_32 ), lscratch_to.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ) );
						break;

					case CHANGE_NETWORK_CONFIG_SCHEDULED_IRRI_IS_OFF:
						if( lscratch_to.b32 == (true) )
						{
							snprintf( toptr, MAX_ALERT_LENGTH, "SCHEDULED IRRIGATION TURNED OFF" );
						}
						else
						{
							snprintf( toptr, MAX_ALERT_LENGTH, "SCHEDULED IRRIGATION TURNED ON" );
						}
						break;

					case CHANGE_SOIL_STORAGE_CAPACITY:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Soil Storage Capacity for %s from %0.2f in. to %0.2f in.", lgroup_name, (double)(lscratch_from.u32 / 100.0), (double)(lscratch_to.u32 / 100.0) );
						break;

					case CHANGE_STATION_GROUP_HEAD_TYPE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Head Type for %s from %s to %s", lgroup_name, GetHeadTypeStr( lscratch_from.u32 ), GetHeadTypeStr( lscratch_to.u32 ) );
						break;

					case CHANGE_STATION_GROUP_PRECIP_RATE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Head Type Precipitation Rate for %s from %5.2f to %5.2f in/hr", lgroup_name, (double)(lscratch_from.u32 / 100000.0), (double)(lscratch_to.u32 / 100000.0) );
						break;

					case CHANGE_STATION_GROUP_SOIL_TYPE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Soil Type for %s from %s to %s", lgroup_name, GetSoilTypeStr( lscratch_from.u32 ), GetSoilTypeStr( lscratch_to.u32 ) );
						break;

					case CHANGE_STATION_GROUP_SLOPE_PERCENTAGE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Slope Percentage for %s from %s to %s", lgroup_name, GetSlopePercentageStr( lscratch_from.u32 ), GetSlopePercentageStr( lscratch_to.u32 ) );
						break;

					case CHANGE_STATION_GROUP_PLANT_TYPE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Plant Type for %s from %s to %s", lgroup_name, GetPlantTypeStr( lscratch_from.u32 ), GetPlantTypeStr( lscratch_to.u32 ) );
						break;

					case CHANGE_STATION_GROUP_CROP_COEFF_1:
					case CHANGE_STATION_GROUP_CROP_COEFF_2:
					case CHANGE_STATION_GROUP_CROP_COEFF_3:
					case CHANGE_STATION_GROUP_CROP_COEFF_4:
					case CHANGE_STATION_GROUP_CROP_COEFF_5:
					case CHANGE_STATION_GROUP_CROP_COEFF_6:
					case CHANGE_STATION_GROUP_CROP_COEFF_7:
					case CHANGE_STATION_GROUP_CROP_COEFF_8:
					case CHANGE_STATION_GROUP_CROP_COEFF_9:
					case CHANGE_STATION_GROUP_CROP_COEFF_10:
					case CHANGE_STATION_GROUP_CROP_COEFF_11:
					case CHANGE_STATION_GROUP_CROP_COEFF_12:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "%s Crop Coeffiecient for %s from %5.2f to %5.2f", GetMonthShortStr( (UNS_32)(paid - CHANGE_STATION_GROUP_CROP_COEFF_1) ), lgroup_name, (double)(lscratch_from.u32 / 100.0), (double)(lscratch_to.u32 / 100.0) );
						break;

					case CHANGE_STATION_GROUP_EXPOSURE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Exposure for %s from %s to %s", lgroup_name, GetExposureStr( lscratch_from.u32 ), GetExposureStr( lscratch_to.u32 ) );
						break;

					case CHANGE_STATION_GROUP_USABLE_RAIN:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Usable Rain for %s from %d%% to %d%%", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_STATION_GROUP_ALLOWABLE_DEPLETION:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Allowable Depletion for %s from %d to %d", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_STATION_GROUP_AVAILABLE_WATER:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Available Water for %s from %5.2f to %5.2f", lgroup_name, (double)(lscratch_from.u32 / 100.0), (double)(lscratch_to.u32 / 100.0) );
						break;

					case CHANGE_STATION_GROUP_ROOT_ZONE_DEPTH:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Root Zone Depth for %s from %5.2f to %5.2f", lgroup_name, (double)(lscratch_from.u32 / 100.0), (double)(lscratch_to.u32 / 100.0) );
						break;

					case CHANGE_STATION_GROUP_SPECIES_FACTOR:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Crop Species Factor for %s from %5.2f to %5.2f", lgroup_name, (double)(lscratch_from.u32 / 100.0), (double)(lscratch_to.u32 / 100.0) );
						break;

					case CHANGE_STATION_GROUP_DENSITY_FACTOR:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Crop Density Factor for %s from %5.2f to %5.2f", lgroup_name, (double)(lscratch_from.u32 / 100.0), (double)(lscratch_to.u32 / 100.0) );
						break;

					case CHANGE_STATION_GROUP_MICROCLIMATE_FACTOR:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Crop Microclimate Factor for %s from %5.2f to %5.2f", lgroup_name, (double)(lscratch_from.u32 / 100.0), (double)(lscratch_to.u32 / 100.0) );
						break;

					case CHANGE_STATION_GROUP_SOIL_INTAKE_RATE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Soil Intake Rate for %s from %5.2f to %5.2f", lgroup_name, (double)(lscratch_from.u32 / 100.0), (double)(lscratch_to.u32 / 100.0) );
						break;

					case CHANGE_STATION_GROUP_ALLOWABLE_SURFACE_ACCUM:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Allowable Surface Accumulation for %s from %5.2f to %5.2f", lgroup_name, (double)(lscratch_from.u32 / 100.0), (double)(lscratch_to.u32 / 100.0) );
						break;

					case CHANGE_LIGHTS_NAME:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Light Name to %s", lgroup_name );
						break;

					case CHANGE_LIGHTS_BOX_INDEX:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Light Controller Address for %s from %c to %c", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_LIGHTS_OUTPUT_INDEX:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Light Output Index for %s from %d to %d", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_LIGHTS_PHYSICALLY_AVAILABLE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Light Physically Available for %s from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.u32 ), GetNoYesStr( lscratch_to.u32 ) );
						break;

					case CHANGE_LIGHTS_DAY_00_START_TIME_1:
					case CHANGE_LIGHTS_DAY_01_START_TIME_1:
					case CHANGE_LIGHTS_DAY_02_START_TIME_1:
					case CHANGE_LIGHTS_DAY_03_START_TIME_1:
					case CHANGE_LIGHTS_DAY_04_START_TIME_1:
					case CHANGE_LIGHTS_DAY_05_START_TIME_1:
					case CHANGE_LIGHTS_DAY_06_START_TIME_1:
					case CHANGE_LIGHTS_DAY_07_START_TIME_1:
					case CHANGE_LIGHTS_DAY_08_START_TIME_1:
					case CHANGE_LIGHTS_DAY_09_START_TIME_1:
					case CHANGE_LIGHTS_DAY_10_START_TIME_1:
					case CHANGE_LIGHTS_DAY_11_START_TIME_1:
					case CHANGE_LIGHTS_DAY_12_START_TIME_1:
					case CHANGE_LIGHTS_DAY_13_START_TIME_1:
						// 7/21/2015 mpd : TBD: The day value is currently displayed 0 - 13 which is appropriate for the engineering alerts. Should it be 1 - 14?
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Light: %s Day %d Start Time 1 from %s to %s", lgroup_name, ( paid - CHANGE_LIGHTS_DAY_00_START_TIME_1 ), 
									TDUTILS_time_to_time_string_with_ampm( from_str_32, sizeof( from_str_32 ), lscratch_from.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ), 
									TDUTILS_time_to_time_string_with_ampm( to_str_32, sizeof( to_str_32 ), lscratch_to.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ) );
						break;

					case CHANGE_LIGHTS_DAY_00_START_TIME_2:
					case CHANGE_LIGHTS_DAY_01_START_TIME_2:
					case CHANGE_LIGHTS_DAY_02_START_TIME_2:
					case CHANGE_LIGHTS_DAY_03_START_TIME_2:
					case CHANGE_LIGHTS_DAY_04_START_TIME_2:
					case CHANGE_LIGHTS_DAY_05_START_TIME_2:
					case CHANGE_LIGHTS_DAY_06_START_TIME_2:
					case CHANGE_LIGHTS_DAY_07_START_TIME_2:
					case CHANGE_LIGHTS_DAY_08_START_TIME_2:
					case CHANGE_LIGHTS_DAY_09_START_TIME_2:
					case CHANGE_LIGHTS_DAY_10_START_TIME_2:
					case CHANGE_LIGHTS_DAY_11_START_TIME_2:
					case CHANGE_LIGHTS_DAY_12_START_TIME_2:
					case CHANGE_LIGHTS_DAY_13_START_TIME_2:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Light: %s Day %d Start Time 2 from %s to %s", lgroup_name, ( paid - CHANGE_LIGHTS_DAY_00_START_TIME_2 ), 
									TDUTILS_time_to_time_string_with_ampm( from_str_32, sizeof( from_str_32 ), lscratch_from.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ), 
									TDUTILS_time_to_time_string_with_ampm( to_str_32, sizeof( to_str_32 ), lscratch_to.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ) );
						break;

					case CHANGE_LIGHTS_DAY_00_STOP_TIME_1:
					case CHANGE_LIGHTS_DAY_01_STOP_TIME_1:
					case CHANGE_LIGHTS_DAY_02_STOP_TIME_1:
					case CHANGE_LIGHTS_DAY_03_STOP_TIME_1:
					case CHANGE_LIGHTS_DAY_04_STOP_TIME_1:
					case CHANGE_LIGHTS_DAY_05_STOP_TIME_1:
					case CHANGE_LIGHTS_DAY_06_STOP_TIME_1:
					case CHANGE_LIGHTS_DAY_07_STOP_TIME_1:
					case CHANGE_LIGHTS_DAY_08_STOP_TIME_1:
					case CHANGE_LIGHTS_DAY_09_STOP_TIME_1:
					case CHANGE_LIGHTS_DAY_10_STOP_TIME_1:
					case CHANGE_LIGHTS_DAY_11_STOP_TIME_1:
					case CHANGE_LIGHTS_DAY_12_STOP_TIME_1:
					case CHANGE_LIGHTS_DAY_13_STOP_TIME_1:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Light: %s Day %d Stop Time 1 from %s to %s", lgroup_name, ( paid - CHANGE_LIGHTS_DAY_00_STOP_TIME_1 ), 
									TDUTILS_time_to_time_string_with_ampm( from_str_32, sizeof( from_str_32 ), lscratch_from.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ), 
									TDUTILS_time_to_time_string_with_ampm( to_str_32, sizeof( to_str_32 ), lscratch_to.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ) );
						break;

					case CHANGE_LIGHTS_DAY_00_STOP_TIME_2:
					case CHANGE_LIGHTS_DAY_01_STOP_TIME_2:
					case CHANGE_LIGHTS_DAY_02_STOP_TIME_2:
					case CHANGE_LIGHTS_DAY_03_STOP_TIME_2:
					case CHANGE_LIGHTS_DAY_04_STOP_TIME_2:
					case CHANGE_LIGHTS_DAY_05_STOP_TIME_2:
					case CHANGE_LIGHTS_DAY_06_STOP_TIME_2:
					case CHANGE_LIGHTS_DAY_07_STOP_TIME_2:
					case CHANGE_LIGHTS_DAY_08_STOP_TIME_2:
					case CHANGE_LIGHTS_DAY_09_STOP_TIME_2:
					case CHANGE_LIGHTS_DAY_10_STOP_TIME_2:
					case CHANGE_LIGHTS_DAY_11_STOP_TIME_2:
					case CHANGE_LIGHTS_DAY_12_STOP_TIME_2:
					case CHANGE_LIGHTS_DAY_13_STOP_TIME_2:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Light: %s Day %d Stop Time 2 from %s to %s", lgroup_name, ( paid - CHANGE_LIGHTS_DAY_00_STOP_TIME_2 ), 
									TDUTILS_time_to_time_string_with_ampm( from_str_32, sizeof( from_str_32 ), lscratch_from.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ), 
									TDUTILS_time_to_time_string_with_ampm( to_str_32, sizeof( to_str_32 ), lscratch_to.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ) );
						break;

					case CHANGE_MVOR_OPEN_TIME_0:
					case CHANGE_MVOR_OPEN_TIME_1:
					case CHANGE_MVOR_OPEN_TIME_2:
					case CHANGE_MVOR_OPEN_TIME_3:
					case CHANGE_MVOR_OPEN_TIME_4:
					case CHANGE_MVOR_OPEN_TIME_5:
					case CHANGE_MVOR_OPEN_TIME_6:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "MVOR Open Time on %s for %s from %s to %s", lgroup_name, GetDayLongStr( (paid - CHANGE_MVOR_OPEN_TIME_0) ), TDUTILS_time_to_time_string_with_ampm( from_str_32, sizeof( from_str_32 ), lscratch_from.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ), TDUTILS_time_to_time_string_with_ampm( to_str_32, sizeof( to_str_32 ), lscratch_to.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ) );
						break;

					case CHANGE_MVOR_CLOSE_TIME_0:
					case CHANGE_MVOR_CLOSE_TIME_1:
					case CHANGE_MVOR_CLOSE_TIME_2:
					case CHANGE_MVOR_CLOSE_TIME_3:
					case CHANGE_MVOR_CLOSE_TIME_4:
					case CHANGE_MVOR_CLOSE_TIME_5:
					case CHANGE_MVOR_CLOSE_TIME_6:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "MVOR Close Time on %s for %s from %s to %s", lgroup_name, GetDayLongStr( (paid - CHANGE_MVOR_CLOSE_TIME_0) ), TDUTILS_time_to_time_string_with_ampm( from_str_32, sizeof( from_str_32 ), lscratch_from.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ), TDUTILS_time_to_time_string_with_ampm( to_str_32, sizeof( to_str_32 ), lscratch_to.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ) );
						break;

						// ----------

					case CHANGE_IRRIGATION_SYSTEM_BUDGET_IN_USE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Budget Use for %s from %s to %s", lgroup_name, GetNoYesStr(lscratch_from.u32), GetNoYesStr(lscratch_to.u32) );
						break;

					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_READ_TIME:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Budget Meter Read Time for %s from %s to %s", lgroup_name, TDUTILS_time_to_time_string_with_ampm( from_str_32, sizeof( from_str_32 ), lscratch_from.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ), TDUTILS_time_to_time_string_with_ampm( to_str_32, sizeof( to_str_32 ), lscratch_to.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ) );
						break;

					case CHANGE_IRRIGATION_SYSTEM_BUDGET_PERIODS_PER_YEAR_IDX:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Budget Annual Periods to Use for %s from %d to %d", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_0:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_1:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_2:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_3:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_4:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_5:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_6:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_7:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_8:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_9:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_10:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_11:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_12:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_13:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_14:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_15:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_16:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_17:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_18:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_19:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_20:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_21:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_22:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_23:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "%s Budget Period %d Meter Read Date from %s to %s", lgroup_name, (paid - CHANGE_IRRIGATION_SYSTEM_BUDGET_METER_DATE_0), GetDateStr( (char*)from_str_32, sizeof( from_str_32 ), lscratch_from.u32, DATESTR_show_long_year, DATESTR_show_short_dow ), GetDateStr( (char*)to_str_32, sizeof( to_str_32 ), lscratch_to.u32, DATESTR_show_long_year, DATESTR_show_short_dow ) );
						break;

					case CHANGE_IRRIGATION_SYSTEM_BUDGET_MODE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Budget Mode for %s from %s to %s", lgroup_name, GetBudgetModeStr(lscratch_from.u32), GetBudgetModeStr(lscratch_to.u32) );
						break;

					case CHANGE_STATION_GROUP_BUDGET_REDUCTION_CAP:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Budget Reduction Limit for %s from %d to %d", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

						// ----------

					case CHANGE_POC_BUDGET_GALLONS_ENTRY_OPTION:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Budget Entry Option for %s from %s to %s", lgroup_name, GetPOCBudgetEntryStr(lscratch_from.u32), GetPOCBudgetEntryStr(lscratch_to.u32) );
						break;

					case CHANGE_POC_BUDGET_CALCULATION_PERCENT_OF_ET:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Budget Calc Percent of ET for %s from %d to %d", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_IRRIGATION_SYSTEM_BUDGET_FLOW_TYPE_IRRIGATION:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_FLOW_TYPE_PROGRAM:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_FLOW_TYPE_MANUAL_PROGRAM:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_FLOW_TYPE_NA3:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_FLOW_TYPE_MANUAL:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_FLOW_TYPE_WALK_THRU:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_FLOW_TYPE_TEST:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_FLOW_TYPE_MOBILE:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_FLOW_TYPE_MVOR:
					case CHANGE_IRRIGATION_SYSTEM_BUDGET_FLOW_TYPE_NON_CONTROLLER:
						// 7/25/2016 skc : FogBugz #14, change alert text.
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "%s: Budget Flow Type %s from %s to %s", lgroup_name, GetFlowTypeStr(paid - CHANGE_IRRIGATION_SYSTEM_BUDGET_FLOW_TYPE_IRRIGATION), GetOffOnStr(lscratch_from.u32), GetOffOnStr(lscratch_to.u32) ); 
						break;

#if 0
//03/08/2016 skc : Not supported in first release using budgets
					case CHANGE_POC_BUDGET_CLOSE_POC_AT_BUDGET:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Close POC at Budget for %s from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.u32 ), GetNoYesStr( lscratch_to.u32 ) );
						break;
#endif

					case CHANGE_POC_BUDGET_GALLONS_0:
					case CHANGE_POC_BUDGET_GALLONS_1:
					case CHANGE_POC_BUDGET_GALLONS_2:
					case CHANGE_POC_BUDGET_GALLONS_3:
					case CHANGE_POC_BUDGET_GALLONS_4:
					case CHANGE_POC_BUDGET_GALLONS_5:
					case CHANGE_POC_BUDGET_GALLONS_6:
					case CHANGE_POC_BUDGET_GALLONS_7:
					case CHANGE_POC_BUDGET_GALLONS_8:
					case CHANGE_POC_BUDGET_GALLONS_9:
					case CHANGE_POC_BUDGET_GALLONS_10:
					case CHANGE_POC_BUDGET_GALLONS_11:
					case CHANGE_POC_BUDGET_GALLONS_12:
					case CHANGE_POC_BUDGET_GALLONS_13:
					case CHANGE_POC_BUDGET_GALLONS_14:
					case CHANGE_POC_BUDGET_GALLONS_15:
					case CHANGE_POC_BUDGET_GALLONS_16:
					case CHANGE_POC_BUDGET_GALLONS_17:
					case CHANGE_POC_BUDGET_GALLONS_18:
					case CHANGE_POC_BUDGET_GALLONS_19:
					case CHANGE_POC_BUDGET_GALLONS_20:
					case CHANGE_POC_BUDGET_GALLONS_21:
					case CHANGE_POC_BUDGET_GALLONS_22:
					case CHANGE_POC_BUDGET_GALLONS_23:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "%s Budget for Billing Period %d from %d gallons to %d", lgroup_name, (paid - CHANGE_POC_BUDGET_GALLONS_0), lscratch_from.u32, lscratch_to.u32 );
						break;

						// 8/1/2016 skc : Added so comm server can display budget changes in HCF units.
					case CHANGE_POC_BUDGET_HCF_0:
					case CHANGE_POC_BUDGET_HCF_1:
					case CHANGE_POC_BUDGET_HCF_2:
					case CHANGE_POC_BUDGET_HCF_3:
					case CHANGE_POC_BUDGET_HCF_4:
					case CHANGE_POC_BUDGET_HCF_5:
					case CHANGE_POC_BUDGET_HCF_6:
					case CHANGE_POC_BUDGET_HCF_7:
					case CHANGE_POC_BUDGET_HCF_8:
					case CHANGE_POC_BUDGET_HCF_9:
					case CHANGE_POC_BUDGET_HCF_10:
					case CHANGE_POC_BUDGET_HCF_11:
					case CHANGE_POC_BUDGET_HCF_12:
					case CHANGE_POC_BUDGET_HCF_13:
					case CHANGE_POC_BUDGET_HCF_14:
					case CHANGE_POC_BUDGET_HCF_15:
					case CHANGE_POC_BUDGET_HCF_16:
					case CHANGE_POC_BUDGET_HCF_17:
					case CHANGE_POC_BUDGET_HCF_18:
					case CHANGE_POC_BUDGET_HCF_19:
					case CHANGE_POC_BUDGET_HCF_20:
					case CHANGE_POC_BUDGET_HCF_21:
					case CHANGE_POC_BUDGET_HCF_22:
					case CHANGE_POC_BUDGET_HCF_23:
						// 8/1/2016 skc : parenthesis allows the volatile declaration to compile.
						{
							const volatile float HCF_CONVERSION = 748.05f;
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "%s Budget for Billing Period %d from %0.1f HCF to %0.1f", lgroup_name, (paid - CHANGE_POC_BUDGET_HCF_0), lscratch_from.u32/HCF_CONVERSION, lscratch_to.u32/HCF_CONVERSION );
						}
						break;

						// ----------

					case CHANGE_POC_HAS_PUMP_ATTACHED:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "POC %s has pump attached from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.u32 ), GetNoYesStr( lscratch_to.u32 ) );
						break;

					case CHANGE_IRRIGATION_SYSTEM_IN_USE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Mainline %s deleted", lgroup_name );
						break;

					case CHANGE_STATION_GROUP_IN_USE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station Group %s deleted", lgroup_name );
						break;

					case CHANGE_MANUAL_PROGRAMS_IN_USE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Manual Program %s deleted", lgroup_name );
						break;

					case CHANGE_STATION_GROUP_SYSTEM_GID:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "%s Mainline Assignment from %d to %d", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

						// ----------

						// 2/26/2016 rmd : MOISTURE SENSING change lines.

					case CHANGE_MOISTURE_SENSOR_BOX_INDEX:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Moisture Sensor Controller Index for %s from %d to %d", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_MOISTURE_SENSOR_DECODER_SERIAL_NUMBER:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Moisture Sensor Decoder Serial Number for %s from %d to %d", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_MOISTURE_SENSOR_PHYSICALLY_AVAILABLE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Moisture Sensor Physically Available for %s from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.u32 ), GetNoYesStr( lscratch_to.u32 ) );
						break;

					case CHANGE_MOISTURE_SENSOR_IN_USE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Moisture Sensor In Use for %s from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.u32 ), GetNoYesStr( lscratch_to.u32 ) );
						break;

					case CHANGE_MOISTURE_SENSOR_MOISTURE_CONTROL_MODE:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Moisture Sensor Irri Control Mode for %s from %s to %s", lgroup_name, Get_MoistureSensor_MoistureControlMode_Str( lscratch_from.u32 ), Get_MoistureSensor_MoistureControlMode_Str( lscratch_to.u32 ) );
						break;

					// 10/5/2018 rmd : May be nlu, but leave intact so commserver can still parse if arrives!
					case nlu_CHANGE_MOISTURE_SENSOR_MOISTURE_HIGH_SET_POINT_uns32:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Moisture Sensor Stop Irrigation Set Point for %s from %d to %d", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					// 10/5/2018 rmd : May be nlu, but leave intact so commserver can still parse if arrives!
					case nlu_CHANGE_MOISTURE_SENSOR_MOISTURE_LOW_SET_POINT_uns32:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Moisture Sensor Allow Irrigation Set Point for %s from %d to %d", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_MOISTURE_SENSOR_LOW_TEMP_POINT:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Moisture Sensor Low Temperature Limit for %s from %d to %d", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_MOISTURE_SENSOR_ADDITIONAL_SOAK:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Moisture Sensor Additional Soak Seconds for %s from %d to %d", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_STATION_GROUP_MOISTURE_SENSOR_SERIAL_NUMBER:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "%s Moisture Sensor Used from Decoder S/N %d to %d", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_MOISTURE_SENSOR_MOISTURE_HIGH_SET_POINT_float:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Moisture Sensor Stop Irrigation Set Point for %s from %5.3f to %5.3f", lgroup_name, lscratch_from.fl, lscratch_to.fl );
						break;

					case CHANGE_MOISTURE_SENSOR_MOISTURE_LOW_SET_POINT_float:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Moisture Sensor Allow Irrigation Set Point for %s from %5.3f to %5.3f", lgroup_name, lscratch_from.fl, lscratch_to.fl );
						break;
						
					case CHANGE_MOISTURE_SENSOR_HIGH_TEMP_POINT:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Moisture Sensor High Temperature Limit for %s from %d to %d", lgroup_name, lscratch_from.i32, lscratch_to.i32 );
						break;

					case CHANGE_MOISTURE_SENSOR_HIGH_TEMP_ACTION:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Moisture Sensor High Temperature Action for %s from %s to %s", lgroup_name, Get_MoistureSensor_HighTemperatureAction_Str(lscratch_from.u32), Get_MoistureSensor_HighTemperatureAction_Str(lscratch_to.u32) );
						break;

					case CHANGE_MOISTURE_SENSOR_LOW_TEMP_ACTION:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Moisture Sensor Low Temperature Action for %s from %s to %s", lgroup_name, Get_MoistureSensor_LowTemperatureAction_Str(lscratch_from.u32), Get_MoistureSensor_LowTemperatureAction_Str(lscratch_to.u32) );
						break;

					case CHANGE_MOISTURE_SENSOR_HIGH_EC_POINT:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Moisture Sensor High Conductivity Limit for %s from %5.3f to %5.3f", lgroup_name, lscratch_from.fl, lscratch_to.fl );
						break;

					case CHANGE_MOISTURE_SENSOR_LOW_EC_POINT:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Moisture Sensor Low Conductivity Limit for %s from %5.3f to %5.3f", lgroup_name, lscratch_from.fl, lscratch_to.fl );
						break;

					case CHANGE_MOISTURE_SENSOR_HIGH_EC_ACTION:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Moisture Sensor High Conductivity Action for %s from %s to %s", lgroup_name, Get_MoistureSensor_HighECAction_Str(lscratch_from.u32), Get_MoistureSensor_HighECAction_Str(lscratch_to.u32) );
						break;

					case CHANGE_MOISTURE_SENSOR_LOW_EC_ACTION:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Moisture Sensor Low Conductivity Action for %s from %s to %s", lgroup_name, Get_MoistureSensor_LowECAction_Str(lscratch_from.u32), Get_MoistureSensor_LowECAction_Str(lscratch_to.u32) );
						break;

					// ----------
					
					case CHANGE_ET_USE_ET_AVERAGING:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Use ET Averaging for %s from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_ACQUIRE_EXPECTEDS:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Acquire Expecteds for %s from %s to %s", lgroup_name, GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_POC_DECODER_SERIAL_NUMBER1:
					case CHANGE_POC_DECODER_SERIAL_NUMBER2:
					case CHANGE_POC_DECODER_SERIAL_NUMBER3:
						if( lscratch_from.u32 == 0 )
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "POC %s Assigned to Decoder S/N %07d", lgroup_name, lscratch_to.u32 );
						}
						else if( lscratch_to.u32 == 0 )
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "POC %s No Longer Assigned to Decoder S/N %07d", lgroup_name, lscratch_from.u32 );
						}
						else
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "POC %s Decoder Assignment from S/N %d to %07d", lgroup_name, lscratch_from.u32, lscratch_to.u32 );
						}
						break;

					case CHANGE_CONTROLLER_NAME:
						// TODO Determine how to display change line for the
						// controller name
#if 1
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Controller Name" );
#endif
						break;

					case CHANGE_CONTROLLER_SERIAL_NUMBER:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Controller Serial Number from %d to %d", lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_CONTROLLER_LETTER:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Communication Address from %c to %c", lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_CONTROLLER_PART_OF_A_CHAIN:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Part of a FLOWSENSE Chain from %s to %s", GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_CONTROLLER_CONTROLLERS_IN_CHAIN:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Number of Controllers in Chain from %d to %d", lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_NETWORK_CONFIG_NETWORK_ID:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Network ID from %d to %d", lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_NETWORK_CONFIG_ELECTRICAL_LIMIT1:
					case CHANGE_NETWORK_CONFIG_ELECTRICAL_LIMIT2:
					case CHANGE_NETWORK_CONFIG_ELECTRICAL_LIMIT3:
					case CHANGE_NETWORK_CONFIG_ELECTRICAL_LIMIT4:
					case CHANGE_NETWORK_CONFIG_ELECTRICAL_LIMIT5:
					case CHANGE_NETWORK_CONFIG_ELECTRICAL_LIMIT6:
					case CHANGE_NETWORK_CONFIG_ELECTRICAL_LIMIT7:
					case CHANGE_NETWORK_CONFIG_ELECTRICAL_LIMIT8:
					case CHANGE_NETWORK_CONFIG_ELECTRICAL_LIMIT9:
					case CHANGE_NETWORK_CONFIG_ELECTRICAL_LIMIT10:
					case CHANGE_NETWORK_CONFIG_ELECTRICAL_LIMIT11:
					case CHANGE_NETWORK_CONFIG_ELECTRICAL_LIMIT12:
						if( FLOWSENSE_we_are_poafs() )
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "Controller %c Electrical Limit from %d to %d", (lbox_index_0 + 'A'), lscratch_from.u32, lscratch_to.u32 );
						}
						else
						{
							sp_strlcat( toptr, MAX_ALERT_LENGTH, "Electrical Limit from %d to %d", lscratch_from.u32, lscratch_to.u32 );
						}
						break;

					case CHANGE_NETWORK_CONFIG_SEND_FLOW_RECORDING:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Send Flow Recording from %s to %s", GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_NETWORK_CONFIG_CONTROLLER_NAME:
						// TODO Determine how to display change line for the
						// controller name
#if 1
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Controller Name" );
#endif
						break;

					case CHANGE_NETWORK_CONFIG_WATER_UNITS:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Water Units from %s to %s", GetWaterUnitsStr( lscratch_from.u32 ), GetWaterUnitsStr( lscratch_to.u32 ) );
						break;

					case CHANGE_CONTROLLER_ACTS_AS_A_HUB:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "This Controller Acts as Hub from %s to %s", GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_RAIN_BUCKET_BOX_INDEX:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Controller Connected to Rain Bucket from %c to %c", (lscratch_from.u32 + 'A'), (lscratch_to.u32 + 'A') );
						break;

					case CHANGE_RAIN_BUCKET_CONNECTED:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Rain Bucket Connected from %s to %s", GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_RAIN_BUCKET_MINIMUM:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Minimum Rain Needed to Stop Irrigation from %0.2f to %0.2f", (lscratch_from.u32 / 100.0), (lscratch_to.u32 / 100.0) );
						break;

					case CHANGE_RAIN_BUCKET_MAXIMUM_HOURLY:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Maximum Hourly Rain from %0.2f to %0.2f", (lscratch_from.u32 / 100.0), (lscratch_to.u32 / 100.0) );
						break;

					case CHANGE_RAIN_BUCKET_MAXIMUM_24_HOUR:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Maximum 24 Hour Rain from %0.2f to %0.2f", (lscratch_from.u32 / 100.0), (lscratch_to.u32 / 100.0) );
						break;

					case CHANGE_ET_GAGE_BOX_INDEX:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Controller Connected to ET Gage from %c to %c", (lscratch_from.u32 + 'A'), (lscratch_to.u32 + 'A') );
						break;

					case CHANGE_ET_GAGE_CONNECTED:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "ET Gage Connected from %s to %s", GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_ET_LOG_PULSES:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Log ET Gage Pulses from %s to %s", GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_ET_START_OF_DAY:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Start of Irrigation Day from %s to %s", TDUTILS_time_to_time_string_with_ampm( from_str_32, sizeof( from_str_32 ), lscratch_from.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ), TDUTILS_time_to_time_string_with_ampm( to_str_32, sizeof( to_str_32 ), lscratch_to.u32, TDUTILS_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ) );
						break;

					case CHANGE_ET_PERCENT_OF_HISTORICAL_CAP:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "ET Maximum Percent of Historical ET Number from %d%% to %d%%", lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_WIND_GAGE_BOX_INDEX:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Controller Connected to Wind Gage from %c to %c", (lscratch_from.u32 + 'A'), (lscratch_to.u32 + 'A') );
						break;

					case CHANGE_WIND_GAGE_CONNECTED:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Wind Gage Connected from %s to %s", GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_WIND_PAUSE_SPEED:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Wind Pause Speed from %d mph to %d mph", lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_WIND_RESUME_SPEED:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Wind Resume Speed from %d mph to %d mph", lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_REFERENCE_ET_USE_YOUR_OWN:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Use Your Own Reference ET Numbers from %s to %s", GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_REFERENCE_ET_NUMBER_1:
					case CHANGE_REFERENCE_ET_NUMBER_2:
					case CHANGE_REFERENCE_ET_NUMBER_3:
					case CHANGE_REFERENCE_ET_NUMBER_4:
					case CHANGE_REFERENCE_ET_NUMBER_5:
					case CHANGE_REFERENCE_ET_NUMBER_6:
					case CHANGE_REFERENCE_ET_NUMBER_7:
					case CHANGE_REFERENCE_ET_NUMBER_8:
					case CHANGE_REFERENCE_ET_NUMBER_9:
					case CHANGE_REFERENCE_ET_NUMBER_10:
					case CHANGE_REFERENCE_ET_NUMBER_11:
					case CHANGE_REFERENCE_ET_NUMBER_12:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "%s Reference ET Number from %0.2f to %0.2f", GetMonthLongStr( (paid - CHANGE_REFERENCE_ET_NUMBER_1) ), (lscratch_from.u32 / 100.0), (lscratch_to.u32 / 100.0) );
						break;

					case CHANGE_REFERENCE_ET_STATE_INDEX:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Reference ET State from %s to %s", States[ lscratch_from.u32 ], States[ lscratch_to.u32 ] );
						break;

					case CHANGE_REFERENCE_ET_COUNTY_INDEX:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Reference ET County from %s to %s", Counties[ lscratch_from.u32 ].county, Counties[ lscratch_to.u32 ].county );
						break;

					case CHANGE_REFERENCE_ET_CITY_INDEX:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Reference ET City from %s to %s", ET_PredefinedData[ lscratch_from.u32 ].city, ET_PredefinedData[ lscratch_to.u32 ].city );
						break;

					case CHANGE_REFERENCE_ET_USER_DEFINED_STATE:
						// TODO Determine how to display change line for name
#if 1
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Reference ET State" );
#endif
						break;

					case CHANGE_REFERENCE_ET_USER_DEFINED_COUNTY:
						// TODO Determine how to display change line for name
#if 1
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Reference ET County" );
#endif
						break;

					case CHANGE_REFERENCE_ET_USER_DEFINED_CITY:
						// TODO Determine how to display change line for name
#if 1
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Reference ET City" );
#endif
						break;

					case CHANGE_RAIN_SWITCH_NAME:
						// TODO Determine how to display change line for name
#if 1
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Rain Switch Name" );
#endif
						break;

					case CHANGE_RAIN_SWITCH_CONNECTED:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Rain Switch Connected from %s to %s", GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_RAIN_SWITCH_NORMALLY_CLOSED:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Rain Switch Contact Type from %s to %s", GetMasterValveStr( lscratch_from.b32 ), GetMasterValveStr( lscratch_to.b32 ) );
						break;

					case CHANGE_RAIN_SWITCH_STOPS_IRRIGATION:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Rain Switch Stops Irrigation from %s to %s", GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_RAIN_SWITCH_IRRIGATION_TO_AFFECT:
						// TODO
#if 1
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Rain Switch Affects" );
#endif
						break;

					case CHANGE_FREEZE_SWITCH_NAME:
						// TODO Determine how to display change line for name
#if 1
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Freeze Switch Name" );
#endif
						break;

					case CHANGE_FREEZE_SWITCH_CONNECTED:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Freeze Switch Connected from %s to %s", GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_FREEZE_SWITCH_NORMALLY_CLOSED:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Freeze Switch Contact Type from %s to %s", GetMasterValveStr( lscratch_from.b32 ), GetMasterValveStr( lscratch_to.b32 ) );
						break;

					case CHANGE_FREEZE_SWITCH_STOPS_IRRIGATION:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Freeze Switch Stops Irrigation from %s to %s", GetNoYesStr( lscratch_from.b32 ), GetNoYesStr( lscratch_to.b32 ) );
						break;

					case CHANGE_FREEZE_SWITCH_IRRIGATION_TO_AFFECT:
#if 1
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Freeze Switch Affects" );
#endif
						break;

					case CHANGE_ET_PERCENT_OF_HISTORICAL_MIN:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "ET Minimum Percent of Historical ET Number from %d%% to %d%%", lscratch_from.u32, lscratch_to.u32 );
						break;

					case CHANGE_ET_AND_RAIN_TABLE_SHARED_ET:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "ET to %0.2f inches", (lscratch_to.u16 / 10000.0) );
						break;

					case CHANGE_ET_AND_RAIN_TABLE_SHARED_RAIN:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Rain to %0.2f inches", (lscratch_to.u16 / 100.0) );
						break;
						
					// ----------
					
					case CHANGE_WALK_THRU_STATION_RUN_TIME:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Station Run Time from %0.1f min to %0.1f min", ( lscratch_from.u32 / 10.0 ), ( lscratch_to.u32 / 10.0 ) );
						break;
					
					case CHANGE_WALK_THRU_DELAY_BEFORE_START:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Delay Before Start from %d seconds to %d seconds", lscratch_from.u32, lscratch_to.u32 );
						break;
						
					case CHANGE_WALK_THRU_BOX_INDEX_0:
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Controller used in Walk Thru from %c to %c", (lscratch_from.u32 + 'A'), (lscratch_to.u32 + 'A') );
						break;

					// ----------

						// TODO
					default:
						// 03/08/2016 skc : Add the offending change line id for debugging purposes.
						sp_strlcat( toptr, MAX_ALERT_LENGTH, "Unknown change line %d", paid );
				}
			}

			// If we're part of a FLOWSENSE change and the change was made via a
			// keypad, show which controller's keypad made the change.
			// Otherwise, just show the reason with no additional information.
			if( (FLOWSENSE_we_are_poafs()) && (lreason == CHANGE_REASON_KEYPAD) )
			{
				sp_strlcat( toptr, MAX_ALERT_LENGTH, " (Controller %c)", lbox_index_of_initiator_0 + 'A' );
			}
			else
			{
				strlcat( toptr, GetChangeReasonStr( lreason ), MAX_ALERT_LENGTH );
			}
		}
	}
	else
	{
		// TODO: The alert is invalid.
	}

	return( rv );
}

#ifdef _MSC_VER
/* ---------------------------------------------------------- */
/**
* Validates each alert, based on it's ID, as its received by the CommServer and determines 
* whether the CommServer should store it in the database or not.
* 
* @param paid The alert ID to validate.
* 
* @return BOOL_32 TRUE if the alert should be stored in the database; otherwise, FALSE. 
*
* @author 9/21/2017 adrianusv
*
* @revisions (none)
*/
static BOOL_32 store_this_alert_in_the_database( const UNS_32 paid, const char *const tmpbuf )
{
	BOOL_32	parse_alert_for_db;

	// 9/21/2017 ajv : By default, parse every alert and change line unless explicitly excluded 
	// within the switch statement below. 
	parse_alert_for_db = (true);
	
	// ----------

// 3/6/2018 ajv : I've commented this out for now. Wissam can activate when he's content with the
// way this is done.

	// 9/21/2017 ajv : Ultimately, we should consider suppressing these alerts altogether as 
	// they not just database memory, but also consume memory in the controller and data when 
	// trasmitting. However, for now, Bob wants to retain them for troubleshooting. 
	switch( paid )
	{
		case AID_ALERT_DIVIDER_LARGE:
		case AID_ALERT_DIVIDER_SMALL:
		//case AID_COMM_COMMAND_RCVD:			// Should consider reducing these which will cut down one 1 alert per communication
		//case AID_COMM_COMMAND_INIT_ISSUED:	// Should consider reducing these which will cut down one 1 alert per communication
		case AID_OUTBOUND_MESSAGE_SIZE:
		case AID_INBOUND_MESSAGE_SIZE:
    	case AID_CI_QUEUED_MSG:
    	case AID_CI_WAITING_FOR_DEVICE:
    	case AID_CELLULAR_DATA_CONNECTION_ATTEMPT:
    	case AID_MSG_TRANSACTION_TIME:
    	case AID_LARGEST_TOKEN_SIZE:
    		parse_alert_for_db = (false);
			break;

    	default:
    		// 3/21/2018 ajv - This is where we can do a strncmp (string compare) for any
			// alert_message strings which don't have a formal alert assigned to them.
			break;
	};


	return( parse_alert_for_db );
}
#endif

/* ---------------------------------------------------------- */
DLLEXPORT UNS_32 nm_ALERT_PARSING_parse_alert_and_return_length( const UNS_32 paid, ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, char *pdest_ptr, const UNS_32 pallowable_size, UNS_32 pindex )
{
	// 11/18/2014 rmd : Function returns ZERO if there is a parsing error. That would be an
	// unrecognized AID or any range check error that fails.

	// ----------

	UNS_32 rv;

	BOOL_32 failure_occurred;

	// Make the temporary buffer big so we can fit almost any string into the
	// buffer. This is not the total alert length. But close. We are careful to
	// use the protected string functions, snprintf and strlcat, to avoid
	// overruns.
	char tmpbuf[ MAX_ALERT_LENGTH ];

	// ----------

	rv = 0;

	// NULL string to start.
	memset( tmpbuf, 0x00, sizeof(tmpbuf) );

	// ----------

	failure_occurred = (false);

	switch( paid )
	{
		// Remember, rv is returning the byte count on the alert pile of the
		// items we pull off due for this particular alert. For the case of the
		// "simple" alerts, rv is not incremented during this text generation
		// stage.
		
		case AID_ALERT_DIVIDER_LARGE:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "--------------------------------------", sizeof(tmpbuf) );
			}
			break;

		case AID_PROGRAM_RESTART:
			rv += ALERTS_pull_program_restart_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ALERT_DIVIDER_SMALL:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "-------", sizeof(tmpbuf) );
			}
			break;

		case AID_POWER_FAIL:
			rv += ALERTS_pull_power_fail_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_POWER_FAIL_BROWNOUT:
			rv += ALERTS_pull_power_fail_brownout_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_USER_RESET:
			rv += ALERTS_pull_user_reset_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case AID_FIRMWARE_UPDATE:
			rv += ALERTS_pull_firmware_update_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_MAIN_CODE_NEEDS_UPDATING_AT_SLAVE:
			rv += ALERTS_pull_main_code_needs_updating_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_TPMICRO_CODE_NEEDS_UPDATING_AT_SLAVE:
			rv += ALERTS_pull_tpmicro_code_firmware_needs_updating_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ERROR:
		case AID_TP_MICRO_ERROR:
			rv += ALERTS_pull_string_off_pile( ppile, (char*)&tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_WDT:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "WATCHDOG TIMEOUT", sizeof(tmpbuf) );
			}
			break;

		case AID_ALERTS_PILE_USER_INITIALIZED:
			rv += ALERTS_pull_alerts_pile_init_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ALERTS_PILE_CHANGE_LINES_INITIALIZED:
			rv += ALERTS_pull_change_pile_init_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ALERTS_PILE_TP_MICRO_INITIALIZED:
			rv += ALERTS_pull_tp_micro_pile_init_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ALERTS_PILE_ENGINEERING_INITIALIZED:
			rv += ALERTS_pull_engineering_pile_init_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_BATTERY_BACKED_VAR_INITIALIZED:
			rv += ALERTS_pull_battery_backed_var_init_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_BATTERY_BACKED_VAR_VALID:
			rv += ALERTS_pull_battery_backed_var_valid_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_TASK_FROZEN:
			rv += ALERTS_pull_task_frozen_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_GROUP_NOT_FOUND:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Group not found", sizeof(tmpbuf) );
			}
			break;

		case AID_GROUP_NOT_FOUND_WITH_FILENAME:
			rv += ALERTS_pull_group_not_found_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_STATION_NOT_FOUND:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Station not found", sizeof(tmpbuf) );
			}
			break;

		case AID_STATION_NOT_FOUND_WITH_FILENAME:
			rv += ALERTS_pull_station_not_found_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_STATION_NOT_IN_GROUP:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Station not in group", sizeof(tmpbuf) );
			}
			break;

		case AID_STATION_NOT_IN_GROUP_WITH_FILENAME:
			rv += ALERTS_pull_station_not_in_group_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_FUNC_CALL_WITH_NULL_PTR:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Function call with NULL ptr", sizeof(tmpbuf) );
			}
			break;

		case AID_FUNC_CALL_WITH_NULL_PTR_WITH_FILENAME:
			rv += ALERTS_pull_func_call_with_null_ptr_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_INDEX_OUT_OF_RANGE:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Index out of range", sizeof(tmpbuf) );
			}
			break;

		case AID_INDEX_OUT_OF_RANGE_WITH_FILENAME:
			rv += ALERTS_pull_index_out_of_range_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ITEM_NOT_ON_LIST:
		case AID_ITEM_NOT_ON_LIST_WITH_FILENAME:
			rv += ALERTS_pull_item_not_on_list_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, paid );
			break;

		case AID_BIT_SET_WITH_NO_DATA:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Bit set with no data", sizeof(tmpbuf) );
			}
			break;

		case AID_BIT_SET_WITH_NO_DATA_WITH_FILENAME:
			rv += ALERTS_pull_bit_set_with_no_data_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_FLASH_FILE_SYSTEM_PASSED:
			rv += ALERTS_pull_flash_file_system_passed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_FLASH_FILE_FOUND:
			rv += ALERTS_pull_flash_file_found_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_FLASH_FILE_SIZE_ERROR:
			rv += ALERTS_pull_flash_file_size_error_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_FLASH_FILE_FOUND_OLD_VERSION:
			rv += ALERTS_pull_flash_file_found_old_version_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_FLASH_FILE_NOT_FOUND:
			rv += ALERTS_pull_flash_file_not_found_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_FLASH_FILE_DELETING_OBSOLETE:
			rv += ALERTS_pull_flash_file_deleting_obsolete_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_FLASH_FILE_OBSOLETE_NOT_FOUND:
			rv += ALERTS_pull_flash_file_obsolete_not_found_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_FLASH_FILE_DELETING:
			rv += ALERTS_pull_flash_file_deleting_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_FLASH_FILE_OLD_VERSION_NOT_FOUND:
			rv += ALERTS_pull_flash_file_old_version_not_found_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_FLASH_FILE_WRITING:
			rv += ALERTS_pull_flash_writing_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_FLASH_FILE_WRITE_POSTPONED:
			rv += ALERTS_pull_flash_write_postponed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_GROUP_NOT_WATERSENSE_COMPLIANT:
			rv += ALERTS_pull_group_not_watersense_complaint_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_SYSTEM_PRESERVES_ACTIVITY:
			rv += ALERTS_pull_system_preserves_activity_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_POC_PRESERVES_ACTIVITY:
			rv += ALERTS_pull_poc_preserves_activity_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_UNIT_COMMUNICATED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Unit Communicated: ", sizeof(tmpbuf) );
			}
			rv += ALERTS_pull_string_off_pile(ppile, (char*)&tmpbuf[strlen(tmpbuf)], ((UNS_32)sizeof(tmpbuf) - (UNS_32)strlen(tmpbuf)), &pindex);
			break;

		case AID_COMM_COMMAND_RCVD:
		case AID_COMM_COMMAND_INIT_ISSUED:
			rv += ALERTS_pull_COMM_COMMAND_RCVD_OR_ISSUED_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_COMM_COMMAND_FAILED:
			rv += ALERTS_pull_COMM_COMMAND_FAILED_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_OUTBOUND_MESSAGE_SIZE:
			rv += ALERTS_pull_outbound_message_size_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_INBOUND_MESSAGE_SIZE:
			rv += ALERTS_pull_inbound_message_size_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_NEW_CONNECTION_DETECTED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "CI: new connection detected", sizeof(tmpbuf) );
			}
			break;

		case AID_DEVICE_POWERED_ON_OR_OFF:
			rv += ALERTS_pull_device_powered_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_COMM_CRC_FAILED:
			rv += ALERTS_pull_comm_crc_failed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_CTS_TIMEOUT:
			rv += ALERTS_pull_cts_timeout_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_COMM_PACKET_GREATER_THAN_512:
			rv += ALERTS_pull_comm_packet_greater_than_512_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_COMM_STATUS_TIMER_EXPIRED:
			rv += ALERTS_pull_comm_status_timer_expired_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_MSG_RESPONSE_TIMEOUT:
			rv += ALERTS_pull_msg_response_timeout_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_COMM_MNGR_BLOCKED_MSG_DURING_IDLE:
			rv += ALERTS_comm_mngr_blocked_msg_during_idle_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_LR_RADIO_NOT_COMPAT_WITH_HUB_OPT:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "LR Radio not compatible with CS3-HUB-OPT. Upgrade radio to Raveon RV-M7.", sizeof(tmpbuf) );
			}
			break;

		case AID_LR_RADIO_NOT_COMPAT_WITH_HUB_OPT_REMINDER:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "LR Radio not compatible with CS3-HUB-OPT. Upgrade radio to Raveon RV-M7. (Reminder)", sizeof(tmpbuf) );
			}
			break;

			// ----------

		case AID_RANGE_CHECK_FAILED_BOOL:
			rv += ALERTS_pull_range_check_failed_bool_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_DOES_NOT_INCLUDE_GROUP_NAME, ALERT_DOES_NOT_INCLUDE_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_BOOL_WITH_FILENAME:
			rv += ALERTS_pull_range_check_failed_bool_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_DOES_NOT_INCLUDE_GROUP_NAME, ALERT_INCLUDES_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_BOOL_WITH_GROUP:
			rv += ALERTS_pull_range_check_failed_bool_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_INCLUDES_GROUP_NAME, ALERT_DOES_NOT_INCLUDE_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_BOOL_WITH_GROUP_WITH_FILENAME:
			rv += ALERTS_pull_range_check_failed_bool_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_INCLUDES_GROUP_NAME, ALERT_INCLUDES_FILENAME );
			break;

			// ----------

		case AID_RANGE_CHECK_FAILED_DATE:
			rv += ALERTS_pull_range_check_failed_date_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_DOES_NOT_INCLUDE_GROUP_NAME, ALERT_DOES_NOT_INCLUDE_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_DATE_WITH_FILENAME:
			rv += ALERTS_pull_range_check_failed_date_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_DOES_NOT_INCLUDE_GROUP_NAME, ALERT_INCLUDES_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_DATE_WITH_GROUP:
			rv += ALERTS_pull_range_check_failed_date_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_INCLUDES_GROUP_NAME, ALERT_DOES_NOT_INCLUDE_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_DATE_WITH_GROUP_WITH_FILENAME:
			rv += ALERTS_pull_range_check_failed_date_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_INCLUDES_GROUP_NAME, ALERT_INCLUDES_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_FLOAT:
			rv += ALERTS_pull_range_check_failed_float_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_DOES_NOT_INCLUDE_GROUP_NAME, ALERT_DOES_NOT_INCLUDE_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_FLOAT_WITH_FILENAME:
			rv += ALERTS_pull_range_check_failed_float_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_DOES_NOT_INCLUDE_GROUP_NAME, ALERT_INCLUDES_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_FLOAT_WITH_GROUP:
			rv += ALERTS_pull_range_check_failed_float_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_INCLUDES_GROUP_NAME, ALERT_DOES_NOT_INCLUDE_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_FLOAT_WITH_GROUP_WITH_FILENAME:
			rv += ALERTS_pull_range_check_failed_float_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_INCLUDES_GROUP_NAME, ALERT_INCLUDES_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_INT32:
			rv += ALERTS_pull_range_check_failed_int32_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_DOES_NOT_INCLUDE_GROUP_NAME, ALERT_DOES_NOT_INCLUDE_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_INT32_WITH_FILENAME:
			rv += ALERTS_pull_range_check_failed_int32_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_DOES_NOT_INCLUDE_GROUP_NAME, ALERT_INCLUDES_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_INT32_WITH_GROUP:
			rv += ALERTS_pull_range_check_failed_int32_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_INCLUDES_GROUP_NAME, ALERT_DOES_NOT_INCLUDE_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_INT32_WITH_GROUP_WITH_FILENAME:
			rv += ALERTS_pull_range_check_failed_int32_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_INCLUDES_GROUP_NAME, ALERT_INCLUDES_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_STRING_INVALID_CHARS:
			rv += ALERTS_pull_range_check_failed_string_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_DOES_NOT_INCLUDE_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_STRING_INVALID_CHARS_WITH_FILENAME:
			rv += ALERTS_pull_range_check_failed_string_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_INCLUDES_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_STRING_TOO_LONG:
			rv += ALERTS_pull_range_check_failed_string_too_long_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_RANGE_CHECK_FAILED_TIME:
			rv += ALERTS_pull_range_check_failed_time_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_DOES_NOT_INCLUDE_GROUP_NAME, ALERT_DOES_NOT_INCLUDE_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_TIME_WITH_FILENAME:
			rv += ALERTS_pull_range_check_failed_time_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_DOES_NOT_INCLUDE_GROUP_NAME, ALERT_INCLUDES_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_TIME_WITH_GROUP:
			rv += ALERTS_pull_range_check_failed_time_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_INCLUDES_GROUP_NAME, ALERT_DOES_NOT_INCLUDE_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_TIME_WITH_GROUP_WITH_FILENAME:
			rv += ALERTS_pull_range_check_failed_time_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_INCLUDES_GROUP_NAME, ALERT_INCLUDES_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_UINT32:
			rv += ALERTS_pull_range_check_failed_uint32_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_DOES_NOT_INCLUDE_GROUP_NAME, ALERT_DOES_NOT_INCLUDE_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_UINT32_WITH_FILENAME:
			rv += ALERTS_pull_range_check_failed_uint32_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_DOES_NOT_INCLUDE_GROUP_NAME, ALERT_INCLUDES_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_UINT32_WITH_GROUP:
			rv += ALERTS_pull_range_check_failed_uint32_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_INCLUDES_GROUP_NAME, ALERT_DOES_NOT_INCLUDE_FILENAME );
			break;

		case AID_RANGE_CHECK_FAILED_UINT32_WITH_GROUP_WITH_FILENAME:
			rv += ALERTS_pull_range_check_failed_uint32_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, ALERT_INCLUDES_GROUP_NAME, ALERT_INCLUDES_FILENAME );
			break;

		case AID_DERATE_TABLE_UPDATE_FAILED_NO_FLOW:
		case AID_DERATE_TABLE_UPDATE_FAILED_30_PCT_DIFF:
		case AID_DERATE_TABLE_UPDATE_FAILED_50_PCT_DIFF:
			rv += ALERTS_derate_table_update_failed_off_pile( paid, ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_DERATE_TABLE_UPDATE_SUCCESSFUL:
			rv += ALERTS_derate_table_update_successful_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_DERATE_TABLE_UPDATE_STATION_COUNT:
			rv += ALERTS_derate_table_update_station_count_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

			// ----------

		case AID_MLB:
			rv += ALERTS_pull_mainline_break_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_MLB_CLEARED:
			rv += ALERTS_pull_mainline_break_cleared_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

#if 0
		case AID_MLB_CLEAR_REQUEST_KEYPAD:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Mainline Break Clear Request (keypad)", sizeof(tmpbuf) );
			}
			break;

		case AID_MLB_CLEAR_REQUEST_COMMLINK:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Mainline Break Clear Request (keypad)", sizeof(tmpbuf) );
			}
			break;
#endif

			// ----------

		case AID_CONVENTIONAL_STATION_SHORT:
			rv += ALERTS_pull_short_station_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_CONVENTIONAL_MV_NO_CURRENT:
		case AID_CONVENTIONAL_PUMP_NO_CURRENT:
			rv += ALERTS_pull_no_current_master_valve_or_pump_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, paid );
			break;

		case AID_CONVENTIONAL_STATION_NO_CURRENT:
			rv += ALERTS_pull_no_current_station_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

#if 0
		case AID_SHORT_LIGHT:
			rv += ALERTS_pull_Short_LIGHT_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;
#endif

		case AID_CONVENTIONAL_OUTPUT_UNKNOWN_SHORT:
			rv += ALERTS_pull_short_station_unknown( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_CONVENTIONAL_MV_SHORT:
			rv += ALERTS_pull_conventional_MV_short_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_CONVENTIONAL_PUMP_SHORT:
			rv += ALERTS_pull_conventional_PUMP_short_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_FLOW_NOT_CHECKED_NO_REASONS:
			rv += ALERTS_pull_flow_not_checked_no_reasons_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_FLOW_NOT_CHECKED_FLOW_TOO_HIGH:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Unable to check flow: Flow too high", sizeof(tmpbuf) );
			}
			break;

		case AID_FLOW_NOT_CHECKED_TOO_MANY_STATIONS:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Unable to check flow: Too many stations on", sizeof(tmpbuf) );
			}
			break;

#if 0
		case AID_FLOW_CHECK_LEARNING:
			rv += ALERTS_pull_FlowCheckLearning_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;
#endif

		case AID_FIRST_HIGHFLOW:
		case AID_FINAL_HIGHFLOW:
		case AID_FIRST_LOWFLOW:
		case AID_FINAL_LOWFLOW:
			rv += ALERTS_pull_flow_error_off_pile( ppile, pparse_why, paid, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

			// ----------

		case AID_TWO_WIRE_CABLE_EXCESSIVE_CURRENT:
			rv += ALERTS_pull_two_wire_cable_excessive_current_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_TWO_WIRE_CABLE_OVER_HEATED:
			rv += ALERTS_pull_two_wire_cable_over_heated_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_TWO_WIRE_CABLE_COOLED_OFF:
			rv += ALERTS_pull_two_wire_cable_cooled_off_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_TWO_WIRE_STATION_DECODER_FAULT:
			rv += ALERTS_pull_two_wire_station_decoder_fault_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_TWO_WIRE_POC_DECODER_FAULT:
			rv += ALERTS_pull_two_wire_poc_decoder_fault_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

			// ----------

			// AID = 250
		case AID_SCHEDULED_IRRIGATION_STARTED:
			rv += ALERTS_pull_scheduled_irrigation_started_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

			// AID = 251
		case AID_SOME_OR_ALL_STATIONS_SKIPPED_THEIR_START:
			rv += ALERTS_pull_some_or_all_skipped_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

			// AID = 252
		case AID_PROGRAMMED_IRRIGATION_STILL_RUNNING:
			rv += ALERTS_pull_programmed_irrigation_still_running_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

			// AID = 253
		case AID_HIT_STOPTIME:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "STOPPED: Programmed Irrigation (stop time)", sizeof(tmpbuf) );
			}
			break;  

			// AID = 254
		case AID_IRRIGATION_ENDED:
			rv += ALERTS_pull_irrigation_ended_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;
#if 0
			// AID = 255
		case AID_WALK_THRU_STARTED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "START: Walk-Thru", sizeof(tmpbuf) );
			}
			break;
#endif
		case AID_FLOW_NOT_CHECKED__CYCLE_TOO_SHORT:
		case AID_FLOW_NOT_CHECKED__UNSTABLE_FLOW:
			rv += ALERTS_pull_flow_not_checked_with_reason_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, paid );
			break;

		case AID_POC_NOT_FOUND_EXTRACTING_TOKEN_RESP:
			rv += ALERTS_pull_poc_not_found_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_TOKEN_RESP_EXTRACTION_ERROR:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "FOAL_COMM: token_resp extract error", sizeof(tmpbuf) );
			}
			break;

			// ----------

		case AID_TOKEN_RECEIVED_WITH_NO_FL:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "ROUTER: rcvd token with no -FL", sizeof(tmpbuf) );
			}
			break;

		case AID_TOKEN_RECEIVED_WITH_FL_TURNED_OFF:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "ROUTER: rcvd token - not in a FLOWSENSE chain", sizeof(tmpbuf) );
			}
			break;

		case AID_TOKEN_RECEIVED_BY_FL_MASTER:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "ROUTER: master received TOKEN or SCAN packet", sizeof(tmpbuf) );
			}
			break;

		case AID_HUB_RECEIVED_DATA:
			rv += ALERTS_pull_hub_rcvd_data_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_HUB_FORWARDING_DATA:
			rv += ALERTS_pull_hub_forwarding_data_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ROUTER_RECEIVED_UNEXP_CLASS:
			rv += ALERTS_pull_router_rcvd_unexp_class_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ROUTER_RECEIVED_UNEXP_BASE_CLASS:
			rv += ALERTS_pull_router_rcvd_unexp_base_class_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ROUTER_RECEIVED_PACKET_NOT_ON_HUB_LIST:
			rv += ALERTS_pull_router_rcvd_packet_not_on_hub_list_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ROUTER_RECEIVED_PACKET_NOT_ON_HUB_LIST_WITH_SN:
			rv += ALERTS_pull_router_rcvd_packet_not_on_hub_list_with_sn_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ROUTER_RECEIVED_PACKET_NOT_FOR_US:
			rv += ALERTS_pull_router_rcvd_packet_not_for_us_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ROUTER_UNEXP_TO_ADDR:
			rv += ALERTS_pull_router_unexp_to_addr_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ROUTER_UNK_PORT:
			rv += ALERTS_pull_router_unk_port_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ROUTER_RECEIVED_UNEXP_TOKEN_RESP:
			rv += ALERTS_pull_router_rcvd_unexp_token_resp_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_CI_QUEUED_MSG:
			rv += ALERTS_pull_ci_queued_msg_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_CI_WAITING_FOR_DEVICE:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "CI: waiting for device to connect - message build skipped", sizeof(tmpbuf) );
			}
			break;

		case AID_CI_STARTING_CONNECTION_PROCESS:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Port A: starting the connection process", sizeof(tmpbuf) );
			}
			break;

		case AID_CELLULAR_SOCKET_ATTEMPT:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "socket attempt", sizeof(tmpbuf) );
			}
			break;

		case AID_CELLULAR_DATA_CONNECTION_ATTEMPT:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "data connection attempt", sizeof(tmpbuf) );
			}
			break;

		case AID_MSG_TRANSACTION_TIME:
			rv += ALERTS_pull_msg_transaction_time_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_LARGEST_TOKEN_SIZE:
			rv += ALERTS_pull_largest_token_size_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		// ----------

		case AID_BUDGET_UNDER_BUDGET:
			rv += ALERTS_pull_budget_under_budget_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case AID_BUDGET_OVER_BUDGET:
			rv += ALERTS_pull_budget_over_budget_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case AID_BUDGET_WILL_EXCEED_BUDGET:
			rv += ALERTS_pull_budget_will_exceed_budget_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case AID_BUDGET_NO_BUDGET_VALUES:
			rv += ALERTS_pull_budget_no_budget_values_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_BUDGET_GROUP_REDUCTION:
			rv += ALERTS_pull_budget_group_reduction_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_BUDGET_PERIOD_ENDED:
			rv += ALERTS_pull_budget_period_ended_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_BUDGET_OVER_BUDGET_PERIOD_ENDING_TODAY:
			rv += ALERTS_pull_budget_over_budget_with_period_ending_today_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

#if 0
		case AID_SETHO_BYSTATION:
			rv += ALERTS_pull_SetHOByStation_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_SETHO_BYPROG:
			rv += ALERTS_pull_SetHOByProg_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_SETHO_ALLSTATIONS:
			rv += ALERTS_pull_SetHOAllStations_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;
#endif

			// ----------

		case AID_SETNOWDAYS_BYSTATION:
			rv += ALERTS_pull_set_no_water_days_by_station_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_SETNOWDAYS_BYGROUP:
			rv += ALERTS_pull_set_no_water_days_by_group_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_SETNOWDAYS_BYALL:
			rv += ALERTS_pull_SetNOWDaysByAll_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_SETNOWDAYS_BYBOX:
			rv += ALERTS_pull_set_no_water_days_by_box_off_pile(ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex);
			break;

		case AID_RESET_MOISTURE_BALANCE_ALLSTATIONS:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Accumulated Rain Cleared: All Stations", sizeof(tmpbuf) );
			}
			break;

		case AID_RESET_MOISTURE_BALANCE_BYGROUP:
			rv += ALERTS_pull_reset_moisture_balance_by_group_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

			// ----------

#if 0
		case AID_SETNOWDAYS_ALLSTATIONS:
			rv += ALERTS_pull_SetNOWDaysAllStations_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_TEST_PROGRAM_STARTED_WITH_REASON:
			rv += ALERTS_pull_TestProgramStarted_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_TEST_ALL_STARTED_WITH_REASON:
			rv += ALERTS_pull_TestAllStarted_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

			// ----------

		case AID_IRRIGATION_TURNED_OFF_WITH_REASON:
			rv += ALERTS_pull_IRRIGATION_TURNED_OFF_WITH_REASON_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case AID_IRRIGATION_TURNED_ON_WITH_REASON:
			rv += ALERTS_pull_IRRIGATION_TURNED_ON_WITH_REASON_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case AID_IRRIGATION_IS_OFF_REMINDER:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "IRRIGATION IS OFF (reminder)", sizeof(tmpbuf) );
			}
			break;

			// ----------

		case AID_PASSWORDS_RESET_FIRMWARE_VERSION:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Passwords Set To Factory Default (Firmware Version Unk)", sizeof(tmpbuf) );
			}
			break;

		case AID_PASSWORDS_RESET_SRAM:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Passwords Set To Factory Default (SRAM Uninitialized)", sizeof(tmpbuf) );
			}
			break;

		case AID_PASSWORDS_RESET_COMMLINK:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Passwords Set To Factory Default (comm)", sizeof(tmpbuf) );
			}
			break;

		case AID_PASSWORDS_RECEIVED_FROM_PC:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Password Settings Received (comm)", sizeof(tmpbuf) );
			}
			break;

		case AID_PASSWORDS_SENT_TO_PC:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Password Settings Sent (comm)", sizeof(tmpbuf) );
			}
			break;

		case AID_PASSWORDS_UNLOCKED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Password UNLOCKED (comm)", sizeof(tmpbuf) );
			}
			break;

		case AID_PASSWORDS_LOGIN:
			rv += ALERTS_pull_PasswordLogin_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_RRE_PASSWORD_LOGIN:
			rv += ALERTS_pull_RRE_PasswordLogin_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_PASSWORDS_LOGOUT_KEYPAD_TIMEOUT:
		case AID_PASSWORDS_LOGOUT_USER_LOGGED_OUT:
			rv += ALERTS_pull_PasswordLogout_off_pile( ppile, pparse_why, aid_32, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_PASSWORDS_CANCEL_UNLOCK_KEYPAD_TIMEOUT:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Password UNLOCK ended (keypad timeout)", sizeof(tmpbuf) );
			}
			break;

		case AID_PASSWORDS_CANCEL_UNLOCK_USER_CANCELLED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Password UNLOCK ended (user cancelled)", sizeof(tmpbuf) );
			}
			break;

		case AID_SYSTEM_CAPACITY_AUTOMATICALLY_ADJUSTED_UP:
			rv += ALERTS_pull_SystemCapacityAutomaticallyAdjusted_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;
#endif
		case AID_MVOR_REQUESTED:
			rv += ALERTS_pull_MVOR_alert_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_MVOR_SKIPPED:
			rv += ALERTS_pull_MVOR_skipped_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ENTERED_FORCED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "FLOWSENSE Communication Down - IRRIGATION WILL NOT RUN!", sizeof(tmpbuf) );
			}
			break;

		case AID_LEAVING_FORCED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "FLOWSENSE Scan Successful", sizeof(tmpbuf) );
			}
			break;

		case AID_STARTING_SCAN_WITH_REASON:
			rv += ALERTS_pull_starting_scan_with_reason_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_CHAIN_IS_SAME:
			rv += ALERTS_pull_chain_is_same_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_CHAIN_HAS_CHANGED:
			rv += ALERTS_pull_chain_has_changed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_REBOOT_REQUEST:
			rv += ALERTS_pull_reboot_request_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ETGAGE_FIRST_ZERO:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "ET Gage - 0 Pulses", sizeof(tmpbuf) );
			}
			break;

		case AID_ETGAGE_SECOND_OR_MORE_ZEROS:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "ET Gage - 0 Pulses Multiple Days", sizeof(tmpbuf) );
			}
			break;

		case AID_ETGAGE_PERCENT_FULL_EDITED:
			rv += ALERTS_pull_ETGAGE_percent_full_edited_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ETGAGE_PULSE_BUT_NO_GAGE_IN_USE:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "ET Gage pulse received but no ET Gages in use", sizeof(tmpbuf) );
			}
			break;

		case AID_ETGAGE_PULSE:
			rv += ALERTS_pull_ETGAGE_pulse_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ETGAGE_RUNAWAY:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Runaway ET Gage", sizeof(tmpbuf) );
			}
			break;

		case AID_ETGAGE_CLEARED_RUNAWAY:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Runaway ET Gage cleared", sizeof(tmpbuf) );
			}
			break;

		case AID_ETGAGE_PERCENT_FULL_WARNING :
			rv += ALERTS_pull_ETGAGE_PercentFull_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_RAIN_AFFECTING_IRRIGATION:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "STOPPED: Programmed Irrigation (rain)", sizeof(tmpbuf) );
			}
			break;

		case AID_RAIN_SWITCH_AFFECTING_IRRIGATION:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "STOPPED: Programmed Irrigation (rain switch)", sizeof(tmpbuf) );
			}
			break;

		case AID_FREEZE_SWITCH_AFFECTING_IRRIGATION:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "STOPPED: Programmed Irrigation (freeze switch)", sizeof(tmpbuf) );
			}
			break;

		case AID_RAIN_24HOUR_TOTAL:
			rv += ALERTS_pull_RAIN_24HourTotal_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_RAIN_PULSE_BUT_NO_BUCKET_IN_USE:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Rain pulse received but no Rain Buckets in use", sizeof(tmpbuf) );
			}
			break;

		case AID_WIND_DETECTED_BUT_NO_GAGE_IN_USE:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Wind detected but no Wind Gages in use", sizeof(tmpbuf) );
			}
			break;

		case AID_STOP_KEY_PRESSED:
			rv += ALERTS_pull_stop_key_pressed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_WIND_PAUSED:
		case AID_WIND_RESUMED:
			rv += ALERTS_pull_wind_paused_or_resumed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex, paid );
			break;

		case AID_ETTABLE_LOADED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "ET Table: New historical values loaded", sizeof(tmpbuf) );
			}
			break;

		case AID_ACCUM_RAIN_SET_BY_STATION:
			rv += ALERTS_pull_accum_rain_set_by_station_cleared_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_FUSE_REPLACED:
			rv += ALERTS_pull_fuse_replaced_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_FUSE_BLOWN:
			rv += ALERTS_pull_fuse_blown_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_MOBILE_STATION_ON:
			rv += ALERTS_pull_mobile_station_on_from_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_MOBILE_STATION_OFF:
			rv += ALERTS_pull_mobile_station_off_from_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_TEST_STATION_STARTED_WITH_REASON:
			rv += ALERTS_pull_test_station_started_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;
			
		case AID_WALK_THRU_STARTED:
			rv += ALERTS_pull_walk_thru_started_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_MANUAL_WATER_STATION_STARTED_WITH_REASON:
			rv += ALERTS_pull_manual_water_station_started_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_MANUAL_WATER_PROGRAM_STARTED_WITH_REASON:
			rv += ALERTS_pull_manual_water_program_started_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_MANUAL_WATER_ALL_STARTED_WITH_REASON:
			rv += ALERTS_pull_manual_water_all_started_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;
#if 0
		case AID_FUSE_REMINDER:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "FUSE BLOWN (reminder)", sizeof(tmpbuf) );
			}
			break;

		case AID_RRE_STATION_ON:
			rv += ALERTS_pull_RRe_station_on_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_RRE_STATION_OFF:
			rv += ALERTS_pull_RRe_station_off_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_DIRECT_ACCESS_ENTERED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Direct Access Entered", sizeof(tmpbuf) );
			}
			break;

		case AID_DIRECT_ACCESS_EXITED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Direct Access Exited", sizeof(tmpbuf) );
			}
			break;

		case AID_DIRECT_ACCESS_TIMEDOUT:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Direct Access Timed Out", sizeof(tmpbuf) );
			}
			break;

		case AID_CH1794_FAILED_INIT:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Phone Modem Failed to Initialize", sizeof(tmpbuf) );
			}
			break;
#endif
		case AID_ET_TABLE_SUBSTITUTION:
			rv += ALERTS_pull_ETTable_Substitution_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_ET_TABLE_LIMITED:
			rv += ALERTS_pull_ETTable_LimitedEntry_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_LIGHT_ID_WITH_TEXT:
			rv += ALERTS_pull_light_ID_with_text_from_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

#if 0
		case AID_WIND_PAUSE:
		case AID_WIND_RESUME:
			rv += ALERTS_pull_WIND_alerts_off_pile( ppile, pparse_why, aid_32, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_IRRIGATION_PAUSED:
		case AID_IRRIGATION_RESUMED:
			rv += ALERTS_pull_PAUSE_RESUME_alerts_off_pile( ppile, pparse_why, aid_32, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_POC_MLB_BREAK:
			rv += ALERTS_pull_POC_MainlineBreak_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_POC_MLB_REMINDER:
			rv += ALERTS_pull_POC_MainlineBreak_reminder_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case AID_POC_MLB_CLEARED_KEYPAD:
			rv += ALERTS_pull_POC_MainlineBreak_cleared_keypad_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case AID_POC_MLB_CLEARED_COMMLINK:
			rv += ALERTS_pull_POC_MainlineBreak_cleared_commlink_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case AID_POC_MLB_CLEARED_SHORT:
			rv += ALERTS_pull_POC_MainlineBreak_cleared_short_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_POC_MV_SHORT_DURING_MLB:
			rv += ALERTS_pull_POC_MV_Short_During_MLB_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_POC_STARTED_USE:
			rv += ALERTS_pull_POC_Hit_StartTime_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_POC_HIT_ENDTIME:
			rv += ALERTS_pull_POC_Hit_EndTime_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_POC_STOPPED_USE:
			rv += ALERTS_pull_POC_Stopped_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_USERDEFINED_PROGRAMTAG_CHANGED:
			rv += ALERTS_pull_UserDefined_ProgramTag_Changed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_PROGRAMTAG_CHANGED:
			rv += ALERTS_pull_ProgramTag_Changed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_PERCENTADJUST_ALL_PROGS:
			rv += ALERTS_pull_PercentAdjustAllProgs_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_PERCENTADJUST_BY_PROG:
			rv += ALERTS_pull_PercentAdjustByProg_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_SIC_STATE_CHANGED:
			rv += ALERTS_pull_SICStateChanged_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;
#endif	
		case AID_RAIN_SWITCH_ACTIVE:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Rain Switch showing rain", sizeof(tmpbuf) );
			}
			break;

		case AID_RAIN_SWITCH_INACTIVE:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Rain Switch dried out", sizeof(tmpbuf) );
			}
			break;

		case AID_FREEZE_SWITCH_ACTIVE:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Freeze Switch showing freezing temperatures", sizeof(tmpbuf) );
			}
			break;

		case AID_FREEZE_SWITCH_INACTIVE:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Freeze Switch warmed up", sizeof(tmpbuf) );
			}
			break;

		case NLU_AID_MOISTURE_READING_OBTAINED:
			rv += nlu_ALERTS_pull_moisture_reading_obtained_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;
			
		case AID_MOISTURE_READING_OBTAINED:
			rv += ALERTS_pull_moisture_reading_obtained_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_MOISTURE_READING_OUT_OF_RANGE:
			rv += ALERTS_pull_moisture_reading_out_of_range_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_SOIL_MOISTURE_CROSSED_THRESHOLD:
			rv += ALERTS_pull_soil_moisture_crossed_threshold_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_SOIL_TEMPERATURE_CROSSED_THRESHOLD:
			rv += ALERTS_pull_soil_temperature_crossed_threshold_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_SOIL_CONDUCTIVITY_CROSSED_THRESHOLD:
			rv += ALERTS_pull_soil_conductivity_crossed_threshold_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;
 
		case AID_STATION_ADDED_TO_GROUP:
			rv += ALERTS_pull_station_added_to_group_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_STATION_REMOVED_FROM_GROUP:
			rv += ALERTS_pull_station_removed_from_group_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_STATION_COPIED:
			rv += ALERTS_pull_station_copied_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_STATION_MOVED_TO_ANOTHER_GROUP:
			rv += ALERTS_pull_station_moved_from_one_group_to_another_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_STATION_CARD_ADDED_OR_REMOVED:
			rv += ALERTS_pull_station_card_added_or_removed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_LIGHTS_CARD_ADDED_OR_REMOVED:
			rv += ALERTS_pull_lights_card_added_or_removed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_POC_CARD_ADDED_OR_REMOVED:
			rv += ALERTS_pull_poc_card_added_or_removed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_WEATHER_CARD_ADDED_OR_REMOVED:
			rv += ALERTS_pull_weather_card_added_or_removed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_COMMUNICATION_CARD_ADDED_OR_REMOVED:
			rv += ALERTS_pull_communication_card_added_or_removed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_STATION_TERMINAL_ADDED_OR_REMOVED:
			rv += ALERTS_pull_station_terminal_added_or_removed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_LIGHTS_TERMINAL_ADDED_OR_REMOVED:
			rv += ALERTS_pull_lights_terminal_added_or_removed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_POC_TERMINAL_ADDED_OR_REMOVED:
			rv += ALERTS_pull_poc_terminal_added_or_removed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_WEATHER_TERMINAL_ADDED_OR_REMOVED:
			rv += ALERTS_pull_weather_terminal_added_or_removed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_COMMUNICATION_TERMINAL_ADDED_OR_REMOVED:
			rv += ALERTS_pull_communication_terminal_added_or_removed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_TWO_WIRE_TERMINAL_ADDED_OR_REMOVED:
			rv += ALERTS_pull_two_wire_terminal_added_or_removed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_POC_ASSIGNED_TO_MAINLINE:
			rv += ALERTS_pull_poc_assigned_to_mainline_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_STATION_GROUP_ASSIGNED_TO_MAINLINE:
			rv += ALERTS_pull_station_group_assigned_to_mainline_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case AID_STATION_GROUP_ASSIGNED_TO_A_MOISTURE_SENSOR:
			rv += ALERTS_pull_station_group_assigned_to_a_moisture_sensor_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;
			
		case AID_WALK_THRU_STATION_ADDED_OR_REMOVED:
			rv += ALERTS_pull_walk_thru_station_added_or_removed_off_pile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		default:

			// Alert IDs > than 0xA000 are for ALL the change lines.
			if( paid >= CHANGE_GROUP_BASE )
			{
				rv += ALERTS_pull_change_line_off_pile( ppile, pparse_why, paid, &tmpbuf, &pindex );   
			}
			else
			{
				// 11/19/2014 rmd : We fall through to here for unrecognized AID's. If we were passed an
				// unrecognized AID set the error flag.
				failure_occurred = (true);

				if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
				{
					snprintf( tmpbuf, sizeof(tmpbuf), "UNPARSED LINE TYPE %05d", paid );
				}
			}
			break;
	}

	// ----------

	// 11/18/2014 rmd : And if the parsing was sucessful add the length byte to the length of
	// the alert. All alerts contain the length byte as their last byte. REMEMBER the goal is to
	// return a ZERO if soemthing went wrong during the parsing. So don't bump the returned
	// length if it is still 0.
	if( failure_occurred )
	{
		rv = 0;
	}
	else
	{
		rv += 1;
	}

	// ----------

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		// 9/21/2017 ajv : To cut down on superfluous alerts in the database (e.g., dashed lines, 
		// msg transaction times, and so on), explicitly block particular alerts from being written 
		// to the database.
		#ifdef _MSC_VER

			if( store_this_alert_in_the_database( paid, tmpbuf ) == (true) )
			{
				// 8/30/2013 rmd : Copy the results to the callers destination. Being careful not to overrun
				// his buffer. Note - tmpbuf starts out as a NULL string. That is its first character is a
				// NULL meaning a zero length string. Providing some protection if the parsing effort failed
				// in that we are returning a NULL string. NOTE - 'strlcpy' is GUARANTEED to NULL terminate
				// the string.
				strlcpy( pdest_ptr, tmpbuf, pallowable_size );
			}

		#else

			// 8/30/2013 rmd : Copy the results to the callers destination. Being careful not to overrun
			// his buffer. Note - tmpbuf starts out as a NULL string. That is its first character is a
			// NULL meaning a zero length string. Providing some protection if the parsing effort failed
			// in that we are returning a NULL string. NOTE - 'strlcpy' is GUARANTEED to NULL terminate
			// the string.
			strlcpy( pdest_ptr, tmpbuf, pallowable_size );

		#endif

	}

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

