/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"change.h"

#include	"alert_parsing.h"

#include	"alerts.h"

#include	"general_picked_support.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Generates a change line and adds it to the alerts pile.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user leaves a screen; within the context of the COMM_MNGR task when a
 * change is made from the web or mobile app.
 * 
 * @param paid The change Alert ID which identifies which variable was changed.
 * A list of these is available in change.h.
 * 
 * @param pgroup_name The name of the group being changed. Only applies to
 * changes within CHANGE_GROUP_BASE; otherwise, NULL.
 * 
 * @param pbox_index_0 The 0-based box index of the controller which the change
 * was made on, or which the changed station is on. This value correlates to the
 * controller's single letter communication address. Only applies to changes
 * within CHANGE_STATION_BASE and CHANGE_CONTROLLER_BASE; otherwise, 0.
 * 
 * @param pstation_number_0 The 0-based station number that changed. Only
 * applies to changes within CHANGE_STATION_BASE; otherwise, 0.
 * 
 * @param pfrom_ptr A pointer to what the variable was changed from.
 * 
 * @param pto_ptr A pointer to what the variable was changed to.
 * 
 * @param psize The size of the variable.
 * 
 * @param preason The reason for the change.
 * 
 * @param pbox_index_of_initiator_0 The 0-based index of the controller which
 * initiated the change.
 *
 * @author 10/21/2011 AdrianusV
 *
 * @revisions
 *   4/18/2014 ajv Changed controller_serial_number parameters to box_index_0.
 */
static void Alert_ChangeLine( const UNS_32 paid,
							  const char *pgroup_name,
							  const UNS_32 pbox_index_0,
							  const UNS_32 pstation_number_0,
							  const void *const pfrom_ptr,
							  const void *const pto_ptr,
							  const UNS_32 psize,
							  const UNS_32 preason,
							  const UNS_32 pbox_index_of_initiator_0 )
{
	DATA_HANDLE ldh;

	UNS_8 lalert[ 96 ];  // this is where the alert gets built!

	UNS_8 *ucp;

	UNS_32 lsize;


	ucp = ALERTS_load_common( (unsigned char*)&lalert, paid, &ldh );

	ucp = ALERTS_load_object( ucp, &preason, sizeof( preason ), &ldh );
	ucp = ALERTS_load_object( ucp, &pbox_index_of_initiator_0, sizeof( pbox_index_of_initiator_0 ), &ldh );

	if( (paid >= CHANGE_GROUP_BASE) && (paid < CHANGE_STATION_BASE) )
	{
		ucp = ALERTS_load_object( ucp, pgroup_name, NUMBER_OF_CHARS_IN_A_NAME, &ldh );
	}
	else if( (paid >= CHANGE_STATION_BASE) && (paid < CHANGE_CONTROLLER_BASE) )
	{
		ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof( pbox_index_0 ), &ldh );
		ucp = ALERTS_load_object( ucp, &pstation_number_0, sizeof( pstation_number_0 ), &ldh );
	}
	else // if( paid >= CHANGE_ALL_OTHER_BASE )
	{
		ucp = ALERTS_load_object( ucp, &pbox_index_0, sizeof( pbox_index_0 ), &ldh );
	}

	// The granularity for the most part leaves most of the pieces we are
	// storing to be small - HOWEVER we have decided that not all variables are
	// at the lowest level (those that we don't care to know all the details
	// about what changed). For these we set the size to a 0 and thats our flag
	// that there are no to and from pieces to the alert.
	if( psize <= MAX_CHANGE_LINE_VARIABLE_SIZE )
	{
		ucp = ALERTS_load_object( ucp, &psize, sizeof( psize ), &ldh );

		// What it changed from
		ucp = ALERTS_load_object( ucp, pfrom_ptr, psize, &ldh );

		// What it changed to
		ucp = ALERTS_load_object( ucp, pto_ptr, psize, &ldh );
	}
	else
	{
		// Use a local variable to set the size "flag" to 0.
		lsize = 0;
		ucp = ALERTS_load_object( ucp, &lsize, sizeof( lsize ), &ldh );
	}

	ALERTS_add_alert( paid, ldh );
}

/* ---------------------------------------------------------- */
/**
 * Generates a controller change line and adds it to the alerts pile.
 *
 * @executed Executed within the context of the key processing task when the
 * user leaves a screen; within the context of the CommMngr task when a change
 * is made from the central or handheld radio remote.
 *
 * @param paid The change Alert ID which identifies which variable was changed.
 * A list of these is available in change.h.
 * 
 * @param pbox_index_0 The 0-based index of the controller, which correlates
 * to the controller's single letter communication address.
 * 
 * @param pfrom_ptr A pointer to what the variable was changed from.
 * 
 * @param pto_ptr A pointer to what the variable was changed to.
 * 
 * @param psize The size of the variable.
 * 
 * @param preason The reason for the change.
 * 
 * @param pbox_index_of_initiator_0 The 0-based index of the controller which
 * initiated the change.
 *
 * @author 10/21/2011 Adrianusv
 *
 * @revisions
 *    10/21/2011 Initial release
 */
extern void Alert_ChangeLine_Controller( const UNS_32 paid, const UNS_32 pbox_index_0, const void *const pfrom_ptr, const void *const pto_ptr, const UNS_32 psize, const UNS_32 preason, const UNS_32 pbox_index_of_initiator_0 )
{
	Alert_ChangeLine( paid, NULL, pbox_index_0, 0, pfrom_ptr, pto_ptr, psize, preason, pbox_index_of_initiator_0 );
}

/* ---------------------------------------------------------- */
/**
 * Generates a group change line and adds it to the alerts pile.
 *
 * @executed Executed within the context of the key processing task when the
 * user leaves a screen; within the context of the CommMngr task when a change
 * is made from the central or handheld radio remote.
 *
 * @param paid The change Alert ID which identifies which variable was changed.
 *  		   A list of these is available in change.h.
 * @param pgroup_name The name of the group being changed.
 * @param pfrom_ptr A pointer to what the variable was changed from.
 * @param pto_ptr A pointer to what the variable was changed to.
 * @param psize The size of the variable.
 * @param preason The reason for the change.
 * @param pbox_index_of_initiator_0 The 0-based index of the controller which
 * initiated the change.
 *
 * @author 10/21/2011 Adrianusv
 *
 * @revisions
 *    10/21/2011 Initial release
 *    3/19/2012  Added check to make sure this controller is the master before
 *    			 a change line is generated
 */
extern void Alert_ChangeLine_Group( const UNS_32 paid, const char *pgroup_name, const void *const pfrom_ptr, const void *const pto_ptr, const UNS_32 psize, const UNS_32 preason, const UNS_32 pbox_index_of_initiator_0 )
{
	Alert_ChangeLine( paid, pgroup_name, 0, 0, pfrom_ptr, pto_ptr, psize, preason, pbox_index_of_initiator_0 );
}

/* ---------------------------------------------------------- */
/**
 * Generates a station change line and adds it to the alerts pile.
 *
 * @executed Executed within the context of the key processing task when the
 * user leaves a screen; within the context of the CommMngr task when a change
 * is made from the central or handheld radio remote.
 *
 * @param paid The change Alert ID which identifies which variable was changed.
 *  		   A list of these is available in change.h.
 * @param pbox_index_0 The 0-based box index of the controller which the change
 * was made on. This value correlates to the controller's single letter
 * communication address. 
 * @param pstation_number_0 The station number that changed.
 * @param pfrom_ptr A pointer to what the variable was changed from.
 * @param pto_ptr A pointer to what the variable was changed to.
 * @param psize The size of the variable.
 * @param preason The reason for the change.
 * @param pbox_index_of_initiator_0 The 0-based index of the controller which
 * initiated the change.
 *
 * @author 10/21/2011 Adrianusv
 *
 * @revisions
 *    10/21/2011 Initial release
 *    3/19/2012  Added check to make sure this controller is the master before
 *    			 a change line is generated
 */
extern void Alert_ChangeLine_Station( const UNS_32 paid, const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, const void *const pfrom_ptr, const void *const pto_ptr, const UNS_32 psize, const UNS_32 preason, const UNS_32 pbox_index_of_initiator_0 )
{
	Alert_ChangeLine( paid, NULL, pbox_index_0, pstation_number_0, pfrom_ptr, pto_ptr, psize, preason, pbox_index_of_initiator_0 );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

