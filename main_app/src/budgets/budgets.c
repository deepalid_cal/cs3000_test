/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"alerts.h"

#include	"app_startup.h"

#include	"bithacks.h"

#include	"budgets.h"

#include	"budget_report_data.h"

#include	"change.h"

#include	"configuration_network.h"

#include	"epson_rx_8025sa.h"

#include	"et_data.h"

#include	"flowsense.h"

#include	"group_base_file.h"

#include	"irrigation_system.h"

#include	"manual_programs.h"

#include	"math.h"	// for INFINITY, isinf()

#include	"poc_report_data.h"

#include	"station_groups.h"

#include	"stations.h"

#include	"stdlib.h"	// for abs()

#include	"weather_tables.h"

#include	"cal_math.h"

/* ---------------------------------------------------------- */
// Forward declarations
static void update_running_dtcs( DATE_TIME_COMPLETE_STRUCT *running_dtcs, UNS_32 d );
static void nm_handle_budget_reset_record( UNS_32 pp_idx );
static void nm_reset_meter_read_dates( IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys, UNS_32 month, UNS_32 year, UNS_32 delta );
static void nm_reset_budget_values( IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys );
static float getNumStarts( const DATE_TIME_COMPLETE_STRUCT *const start, const DATE_TIME_COMPLETE_STRUCT *const end, UNS_32 start_time, const STATION_STRUCT_for_budgets * const pssfb, STATION_GROUP_STRUCT * const pgroup_ptr );
static float getVolume(const DATE_TIME_COMPLETE_STRUCT *const startday, float days_on, const STATION_STRUCT_for_budgets * const pssfb);
static UNS_32 getNumManualStarts( const DATE_TIME_COMPLETE_STRUCT *const today, const BUDGET_DETAILS_STRUCT * const pbds, const STATION_STRUCT_for_budgets * const pssfb, MANUAL_PROGRAMS_GROUP_STRUCT *pman );
static float getIrrigationListVp( UNS_32 system_GID );
static float getScheduledVp( UNS_32 system_GID, BUDGET_DETAILS_STRUCT *const bds, const DATE_TIME_COMPLETE_STRUCT *const today, const DATE_TIME right_now, BOOL_32 use_limits );
static void nm_calc_POC_ratios( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem );

/* ---------------------------------------------------------- */
// 9/14/2018 Ryan : External variable used for budgets with et entry option selected.
BOOL_32	g_BUDGET_using_et_calculate_budget;

// 8/20/2018 Ryan : Constant declarations
static const float	SQUARE_FOOTAGE_CONSTANT = 96.25f;
static const float 	MINUTES_PER_HOUR = 60.0f;
static const float	percip_100000u	= 100000.0f;


/* ---------------------------------------------------------- */
// 12/24/2015 skc : We need a fully loaded DATE_TIME_COMPLETE_STRUCT, 
// to determine if a schedule is to be run on a specific day.
// Use the value of 'd' to stuff 'running_dtcs'.
static void update_running_dtcs( DATE_TIME_COMPLETE_STRUCT *running_dtcs, UNS_32 d )
{
	UNS_32	lday_of_month, lmonth_1_12, lyear, ldow;

	running_dtcs->date_time.D = d; // Set the day for the running date

	// Calculate the other date values from the new day number
	DateToDMY( running_dtcs->date_time.D, &lday_of_month, &lmonth_1_12, &lyear, &ldow );

	running_dtcs->__month = lmonth_1_12;

	running_dtcs->__day = lday_of_month;

	running_dtcs->__year = lyear;

	running_dtcs->__dayofweek = ldow;
}

/* ---------------------------------------------------------- */
/* 
03/31/2016 skc : Reset the budget preserves values at the the start of a budget period. 
 
Mutex: Requires system_preserves_recursive_MUTEX, poc_preserves_recursive_MUTEX 
 
pp_idx - index into poc_preserves  
*/ 
static void nm_handle_budget_reset_record( UNS_32 pp_idx )
{
	BY_POC_BUDGET_RECORD *pbpbr;

	SYSTEM_BUDGET_REPORT_RECORD *psbrr;

	// ----------

	psbrr = &poc_preserves.poc[pp_idx].this_pocs_system_preserves_ptr->budget.rip;

	psbrr->predicted_use_to_end_of_period = 0;
	psbrr->reduction_gallons = 0;
	psbrr->closing_record_for_the_period = (false);

	// ----------

	pbpbr = &poc_preserves.poc[pp_idx].budget;

	memset( pbpbr, 0, sizeof(BY_POC_BUDGET_RECORD) );
}

/* ---------------------------------------------------------- */
// 4/26/2016 skc : Low level helper function, reset all the meter read dates. Handles both
// 12 and 6 annual periods.
static void nm_reset_meter_read_dates( IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys, UNS_32 month, UNS_32 year, UNS_32 delta )
{
	UNS_32 i;
	UNS_32 box_index;
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	// ----------

	box_index = FLOWSENSE_get_controller_index();
	lchange_bitfield_to_set = SYSTEM_get_change_bits_ptr( ptr_sys, CHANGE_REASON_KEYPAD );

	for( i=0; i<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS; i++ )
	{
		nm_SYSTEM_set_budget_period( ptr_sys, i, 
									 DMYToDate( DATE_MINIMUM_DAY_OF_MONTH, month, year ), 
									 CHANGE_generate_change_line, 
									 CHANGE_REASON_KEYPAD, 
									 box_index, 
									 CHANGE_set_change_bits, 
									 lchange_bitfield_to_set ); 

		month += delta;
		if( month > MONTHS_IN_A_YEAR )
		{
			month -= MONTHS_IN_A_YEAR;
			year += 1;
		}
	}
}

// 4/26/2016 skc : Set the existing budget report data to zero, this happens when a budget
// report is closed, and when the user changes number of annual periods.
static void nm_reset_budget_values( IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys )
{
	UNS_32 i;
	UNS_32 j;
	UNS_32 system_gid;
	POC_GROUP_STRUCT *ptr_poc;

	// ----------

	system_gid = nm_GROUP_get_group_ID( ptr_sys );

	// 6/10/2016 rmd : Pass through the preserves looking for active preserves records (gid !=
	// 0), that are for a POC belonging to the SYSTEM we are interested in. [The ppoc_preserves
	// by its definition holds ALL active POCs in the network, so we will see all the pocs.]
	for( i=0; i<MAX_POCS_IN_NETWORK; i++ )
	{
		const BY_POC_RECORD * const pbpr = &poc_preserves.poc[ i ];

		if( (pbpr->poc_gid != 0) &&
			(pbpr->this_pocs_system_preserves_ptr->system_gid == system_gid) )
		{
			ptr_poc = POC_get_group_at_this_index( POC_get_index_for_group_with_this_GID(pbpr->poc_gid) );
			
			// 8/16/2018 Ryan : Fix for budget only being set to zero for one of the budget period found
			// while adding annual budget option.
			for( j=0; j<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS; ++j )
			{
				POC_set_budget( ptr_poc, j, 0 );
			}

			// 4/26/2016 skc : Clear the report record also.
			nm_handle_budget_reset_record( i );
		}
	}
}

/* ---------------------------------------------------------- */
// 03/09/2016 skc : Return the number of irrigation starts in a specified time period
// 02/18/2016 skc : Note - Is is assumed this function is called prior to stations 
// being added to the irrigation list.
// 03/07/2016 skc : Add end date of interest. When using ET, a budget period may straddle
// multiple ET periods, thus we need to calculate days to the end of the month, and then run
// a second time for the rest of the budget period. The calling code is repsonsible for
// figuring what the days are.
static float getNumStarts( const DATE_TIME_COMPLETE_STRUCT *const start, 
						   const DATE_TIME_COMPLETE_STRUCT *const end, 
						   UNS_32 start_time, 
						   const STATION_STRUCT_for_budgets * const pssfb, 
						   STATION_GROUP_STRUCT * const pgroup_ptr )
{
	UNS_32 d;

	DATE_TIME_COMPLETE_STRUCT dtcs;

	float rv;

	// ----------

	rv = 0;

	for(  d=start->date_time.D; d<=end->date_time.D; ++d )
	{

		// Test no water days first, most likely to fail of the tests?
		if( d - pssfb->no_water_days < start->date_time.D ) 
		{
			continue;
		}

		// handle meter_read_time specially on the first and last day
		// test if we have entered the time period
		if( (d == start->date_time.D) && (start_time < start->date_time.T) )
		{
			continue;
		}

		// test if we have passed the budget period
		if( (d == end->date_time.D) && (start_time > end->date_time.T) )
		{
			continue;
		}

		// 02/24/2016 skc : If the day is today, and the current time is past the start time,
		// then this schedule either already ran, or is currently running. Either way, nothing to do here
		if( (d == start->date_time.D) && (start->date_time.T > start_time) )
		{
			continue;
		}


		// Make sure the schedule will run on this day,
		update_running_dtcs( &dtcs, d ); // set day, month, year from d

		if( SCHEDULE_does_it_irrigate_on_this_date( pgroup_ptr, &dtcs, SCHEDULE_BEGIN_DATE_ACTION_do_nothing ) )
		{
			rv += 1;
		}
	}

	return rv;
}

/* ---------------------------------------------------------- */
// 03/09/2016 skc : Return the predicted volume to be used, for the specifed time interval
// This method is a replacement for nm_WATERSENSE_calculate_run_time(). 
// We skip error checks, and have better control of et and crop_coeff values.
// This function exists because a budget period may straddle months, using multiple ET values.
// When using ET, days_on is the number of days in the budget period for the month with the ET value.
static float getVolume( const DATE_TIME_COMPLETE_STRUCT *const startday,
						float days_on, 
						const STATION_STRUCT_for_budgets * const pssfb )
					   
{
	const volatile float TEN = 10.f;

	float et_hist;

	float et_ratio;

	float precip_rate;

	float unif;

	float cc;

	float et_factor;

	float rt;

	float rv;

	// ----------

	if( pssfb->uses_et )
	{
		// 03/10/2016 skc : Skip the calls to nm_WATERSENSE_calculate_run_time and nm_WATERSENSE_calculate_adjusted_run_time.
		// We are opting for simple ET calculation, and use the guts of the WATERSENSE calls here,
		// skipping pointer checks as being redundant.
		// 
		// This call is not for the faint hearted, too many indices to try to control
		//dailyet = WATERSENSE_get_et_value_after_substituting_or_limiting( startday, pgroup_ptr, et_index, false ); 

		// 8/11/2016 skc : ET values have an interesting feature, the value on the 1st day of a
		// month uses the previous months data. However, crop coefficient works on a strict monthly
		// basis. This means the 1st day of the month for periods that wrap a month will be
		// "different".

		// Simple ET calculation, ET (in/month)
		if( startday->__day == 1)
		{
			// 8/11/2016 skc : Treat 1st day of the month differently
			et_hist = ET_DATA_et_number_for_the_month(startday->__month - 1) / NumberOfDaysInMonth(startday->__month - 1, startday->__year);
		}
		else
		{
			et_hist = ET_DATA_et_number_for_the_month(startday->__month) / NumberOfDaysInMonth(startday->__month, startday->__year);
		}

		et_ratio = WEATHER_TABLES_get_et_ratio(); 

		precip_rate = pssfb->precip_rate;

		unif = pssfb->uniformity;

		cc = STATION_GROUP_get_crop_coefficient_100u( pssfb->GID_station_group, (UNS_32)startday->__month);

		et_factor = pssfb->et_factor_100u; // This is "Station adjust"

		// This is what we end with after combining all the 100% and scaling factors
		rt = 60000.f * cc * et_hist * et_ratio * et_factor / (unif * precip_rate);

		rv = days_on * rt * pssfb->expected_flow_rate_gpm;

		// 8/10/2016 skc : Add any previous left-overs due to minimum run time setting. 
		UNS_32 idx;
		STATION_PRESERVES_get_index_using_box_index_and_station_number( pssfb->box_index_0,
																		pssfb->station_number_0,
																		&idx );
		rv += station_preserves.sps[idx].left_over_irrigation_seconds * pssfb->expected_flow_rate_gpm / 60.f;
	}
	else
	{
		// 03/10/2016 skc : Short cut the following, completely skip the second, cuz we already applied %adjust.
		//rt = nm_WATERSENSE_calculate_run_time( pstation_ptr, pgroup_ptr, startday, (pssfb->uniformity / 100.0F) );
		//rt = nm_WATERSENSE_calculate_adjusted_run_time( rt, pstation_ptr, pssfb->et_factor_100u, 100 );

		rt = pssfb->total_run_minutes_10u / TEN;

		rv = days_on * rt * pssfb->expected_flow_rate_gpm;
	}

	return rv;
}

/* ---------------------------------------------------------- */
// 03/04/2016 skc : Find the number of start times this manual program has in the budget period
static UNS_32 getNumManualStarts( const DATE_TIME_COMPLETE_STRUCT *const today, const BUDGET_DETAILS_STRUCT * const pbds, const STATION_STRUCT_for_budgets * const pssfb, MANUAL_PROGRAMS_GROUP_STRUCT *pman )
{
	UNS_32 d;

	UNS_32 mpstart; // loop counter

	UNS_32 start_time;

	UNS_32 start_day; // first day manual is allowed to run

	UNS_32 end_day; // last day manual is allowed to run

	UNS_32 rv;

	// ----------

	rv = 0;

	start_day = MANUAL_PROGRAMS_get_start_date( pman );

	end_day = MANUAL_PROGRAMS_get_end_date( pman );

	// 03/17/2016 skc : Test for inclusion in the manual running date period.
	// This is so we don't enter the loop if we have bogus dates.
	// We could also just exit the function here, but code standards say no.
	if( (today->date_time.D <= end_day) &&
		(pbds->meter_read_date[1] >= start_day) )
	{
		// 03/17/2016 skc: Note that no_water_days is included here.
		for( d = today->date_time.D + pssfb->no_water_days; d<=pbds->meter_read_date[1]; ++d )
		{
			// 03/17/2016 skc: Test for membership in allowed manual run days.
			if( (d > end_day) || (d < start_day) ) 
			{
				continue;
			}

			// 03/18/2016 skc : We need this to test if a manual program will run
			UNS_32 dow; 
			DateToDMY( d, NULL, NULL, NULL, &dow );

			if( MANUAL_PROGRAMS_today_is_a_water_day( pman, dow ) )
			{
				// Each manual program can have multiple start times
				for( mpstart = 0; mpstart < MANUAL_PROGRAMS_NUMBER_OF_START_TIMES; mpstart++ )
				{
					start_time = MANUAL_PROGRAMS_get_start_time( pman, mpstart );

					// 4/26/2016 skc : First see if this start time is turned on.
					if( start_time == SCHEDULE_OFF_TIME)
					{
						continue;
					}
					// handle meter_read_time specially on the first and last day of budget
					// test if we have entered the budget period
					if( (d == pbds->meter_read_date[0]) && (start_time < pbds->meter_read_time) )
					{
						continue;
					}

					// test if we have passed the budget period
					if( (d == pbds->meter_read_date[1]) && (start_time > pbds->meter_read_time) )
					{
						continue;
					}
					// 02/24/2016 skc : If the day is today, and the current time is past the start time,
					// then this schedule either already ran, or is currently running. Either way, nothing to do here
					if( (d == today->date_time.D) && (today->date_time.T > start_time) )
					{
						continue;
					}

					++rv; // add one start time to sum
				} // end for(mpstart)
			}
		} // end for(d)
	}

	return rv;
}

/* ---------------------------------------------------------- */
// 02/24/2016 skc : Return the expected volume from stations in the irrigation list
// @mutex_requirements -	list_program_data_recursive_MUTEX
static float getIrrigationListVp( UNS_32 system_GID )
{
	static const volatile float SIXTY = 60.f;

	IRRIGATION_LIST_COMPONENT *pilc;

	UNS_32 flow_rate;

	float delta;

	float rv;

	STATION_STRUCT *ptr_station;

	// ----------

	rv = 0;

	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );

	pilc = nm_ListGetFirst( &foal_irri.list_of_foal_all_irrigation );

	while( pilc != NULL )
	{
		// Make sure this is the right mainline
		if( system_GID == pilc->bsr->system_gid )
		{
			// we need to figure out the flow rate
			ptr_station = nm_STATION_get_pointer_to_station( pilc->box_index_0, pilc->station_number_0_u8 );

			flow_rate = nm_STATION_get_expected_flow_rate_gpm( ptr_station );

			delta = (pilc->requested_irrigation_seconds_ul + pilc->remaining_seconds_ON) * flow_rate / SIXTY;

			rv += delta;
		}
		
		pilc = nm_ListGetNext( &foal_irri.list_of_foal_all_irrigation, pilc );
	}

	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );

	return rv;
}

/* ---------------------------------------------------------- */
// 8/16/2016 skc : Helper function for applying percent adjust
float calc_percent_adjust( float days_on, INT_32 *percent_adjust_days, float percent_adjust, UNS_32 days_to_end_of_month )
{
	float rv;

	// 03/17/2016 skc : Factor in percent adjust
	// 03/16/2016 skc : Skip if zero adjust days are in effect
	if( *percent_adjust_days > 0 )
	{
		if( days_on <= *percent_adjust_days ) // if more adjust days than days left in period
		{
			days_on *= percent_adjust; // adjust all the days
		}
		else // if less adjust days than days left in period
		{
			// days_on is combination of adjusted and non-adjusted days
			days_on += *percent_adjust_days * (percent_adjust - 1.f);
		}
	}

	// 03/17/2016 skc : Reset for next calcs
	*percent_adjust_days -= days_to_end_of_month;
	if( *percent_adjust_days < 0 ) // 03/16/2016 skc : Prevent illegal values
	{
		*percent_adjust_days = 0;
	}

	rv = days_on;
	return rv;
}

/* ---------------------------------------------------------- */
// 03/03/2016 skc : Helper function, return the scheduled (automatic, manual)
// irrigation volume for the budget period.
// The intent is to be able to compute the predicted volume in two steps.
// One for scheduled irrigation, and the second for stations in the irrigation list.
// This allows us to take advantage of background calculations, to lighten cpu load
// at a start time.
// @param ptr_system - System object to be examined
// @param today - day to start the calculation, proceed until end of budget period
// @mutex_requirements -	list_program_data_recursive_MUTEX
// 8/18/2016 skc : Added use_limits, allow the calc to proceed using the reduction
// limits for scheduled irrigation time
static float getScheduledVp( UNS_32 system_GID, 
							 BUDGET_DETAILS_STRUCT *const bds, 
							 const DATE_TIME_COMPLETE_STRUCT *const today,
							 const DATE_TIME right_now,
							 BOOL_32 use_limits )
{
	static const volatile float SIXTY = 60.f;

	static const volatile float HUNDRED = 100.f;

	float rv; // return value, predicted volume

	MANUAL_PROGRAMS_GROUP_STRUCT *ptr_manprog = NULL;

	STATION_GROUP_STRUCT	*ptr_group;

	STATION_STRUCT	*ptr_station;

	STATION_STRUCT_for_budgets budget_data;

	UNS_32 start_time; // = SCHEDULE_get_start_time(group)

	// How manys days will this station run for? Including partial days due to "PERCENT_ADJUST"
	float days_on; // Number of days a station will run for in this budget

	UNS_32 times_on; // Number of starts for a manual program in this budget

	float volume; // temporary variable

	float volume_scheduled; // volume attributed to scheduled irrigation

	UNS_32 days_to_end_of_month;

	UNS_32 days_to_end_of_period;

	UNS_32 days_in_month;

	DATE_TIME_COMPLETE_STRUCT startday;

	DATE_TIME_COMPLETE_STRUCT endday;

	float percent_adjust;

	INT_32 percent_adjust_days;

	UNS_32 budget_idx;

	float sf; // for handling predicted volume with reductions applied

	// ----------

	sf = 1.f;
	rv = 0.f;

	// each POC for a mainline shares the same budget period details
	budget_idx = SYSTEM_get_budget_period_index( bds, today->date_time );

	/* ---------------------------------------------------------- */
	
	// We now have the current date and the active budget period
	// Estimate how much more water we will be consuming in this budget period
	ptr_station = nm_ListGetFirst( &station_info_list_hdr );

	while( ptr_station != NULL )
	{
		volume_scheduled = 0.f;
		days_on = 0; // initialize for each station

		// get usefull data in one call
		nm_STATION_get_Budget_data( ptr_station, &budget_data );

		// 3/23/2016 skc : If we are examining a future budget, make sure no_water_days is set correctly.
		if( budget_idx > 0 )
		{
			// 4/29/2016 skc : Adjust no_water_days, subtract days from the previous period.
			// Only if the no water days continues into this future budget period.
			if( budget_data.no_water_days > (bds->meter_read_date[budget_idx] - right_now.D + 1) )
			{
				budget_data.no_water_days -= (bds->meter_read_date[budget_idx] - right_now.D + 1);
			}
		}

		// check this station is actually being used and exists
		// ensure this station has a valid station group
		// ensure this station uses the specified system
		// if no water days takes us past the end of the budget period, do nothing	
 		if( (0 != budget_data.GID_station_group) &&
		    (budget_data.GID_irrigation_system == system_GID) &&
			(budget_data.in_use_bool && budget_data.physically_available) &&
			(bds->meter_read_date[budget_idx+1] >= today->date_time.D + budget_data.no_water_days) )
		{
			// get pointers to the Group and any Manual programs
			ptr_group = STATION_GROUP_get_group_with_this_GID( budget_data.GID_station_group );

			if( NULL != ptr_group ) // sanity check
			{
				// 5/8/2015 ajv : Added check to ensure the start and stop times are not equal since this
				// will prevent the schedule from running.
				start_time = SCHEDULE_get_start_time(ptr_group);

				if( start_time != SCHEDULE_get_stop_time(ptr_group) )
				{
					// 8/18/2016 skc : Adjust the scheduled run time if the flag is true. We want to calculate
					// the predicted volume using reduced irrigation time. At start time, we scale the
					// requested irrigation time using the same ratio.
					if( use_limits )
					{
						IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys = SYSTEM_get_group_with_this_GID( system_GID );
						float ratio = nm_SYSTEM_get_poc_ratio( ptr_sys ) / nm_SYSTEM_get_Vp( ptr_sys );
						float rl = 1 - (STATION_GROUP_get_budget_reduction_limit( ptr_group ) / 100.f);

						if( ratio > 1) // then do nothing
						{
							sf = 1.f;
						}
						else if( ratio <= 0 ) // over budget
						{
							sf = rl; 
						}
						else if( ratio > rl ) // reduce less than limit
						{
							sf = ratio;
						}
						else // use the reduction limit
						{
							sf = rl;
						}
					}

					// 03/17/2016 skc : Bump up the startday by number of no water days.
					// We will also bump down percent adjust days
					startday = *today; // initialize date objects

					startday.date_time.D += budget_data.no_water_days; // Handle no water days

					update_running_dtcs( &startday, startday.date_time.D ); // set calendar data

					// Grab the parameters for percent adjust
					percent_adjust = PERCENT_ADJUST_get_percentage_100u_for_this_gid( budget_data.GID_station_group, startday.date_time.D ) / HUNDRED;

					percent_adjust_days = PERCENT_ADJUST_get_remaining_days_for_this_gid( budget_data.GID_station_group, startday.date_time.D );

// 4/5/2016 skc : percent_adjust_days already accounts for no_water_days when we get it
#if 0
					percent_adjust_days -= budget_data.no_water_days; // Handle no water days

					if( percent_adjust_days < 0 ) // 03/16/skc : Prevent invalid value.
					{
						percent_adjust_days = 0;
					}
#endif

					budget_data.no_water_days = 0; // we already accounted for this, so set to zero

					// ----------
					////////////////////////////////////////////////////////////////////////////////
					// Calculate volume in steps. One for days to the end of a month,
					// and then to the end of the budget period if that ends after the end of the month We
					// may actually come through here more than once, ex. a period of 03/15 - 05/15. If the
					// budget period ends before or at the end of the month, then the loop is skipped. Each
					// month uses different ET and crop coefficients, this is why we do it like this.

					// 8/16/2016 skc : Seperate calculations for ET and time based irrigation
					if( budget_data.uses_et )
					{
						do
						{
							days_in_month = NumberOfDaysInMonth( startday.__month, startday.__year ) - startday.__day + 1;
							days_to_end_of_month = days_in_month;
							days_to_end_of_period = bds->meter_read_date[budget_idx+1] - startday.date_time.D;

							// 03/17/2016 skc : Add additional day if we have yet to run today.
							if( startday.date_time.T <= start_time )
							{
								days_to_end_of_month += 1;
								days_to_end_of_period += 1;
							}

							// special filter for 1st day of the month, due to how we use ET values
							if( startday.__day == 1) //&& 
								//(days_to_end_of_period < days_to_end_of_month) )
							{
								days_to_end_of_month -= days_in_month;
							}

							// if all the days are within a month
							if( days_to_end_of_period <= days_to_end_of_month )
							{
								// 03/17/2016 skc : Factor in percent adjust
								// 03/16/2016 skc : Skip if zero adjust days are in effect
								days_on = calc_percent_adjust( days_to_end_of_period, &percent_adjust_days, percent_adjust, days_to_end_of_month );

								// calculate portion to the end of the month
								// Bypass nm_WATERSENSE_calculate_run_time(), skip error checks, better control of et and crop_coeff values!
								if( days_on > 0 ) // no running days means no volume, we're on the last day of month, after schedule start
								{
									volume = getVolume( &startday, days_on, &budget_data );
									volume_scheduled += volume;
								}

								break; // done, shouldn't have to, but let's make sure
							}
							else
							{
								// 03/17/2016 skc : Factor in percent adjust
								// 03/16/2016 skc : Skip if zero adjust days are in effect
								days_on = calc_percent_adjust( days_to_end_of_month, &percent_adjust_days, percent_adjust, days_to_end_of_month );

								if( days_on > 0 ) // no running days means no volume, we're on the last day of month, after schedule start
								{
									volume = getVolume( &startday, days_on, &budget_data );
									volume_scheduled += volume;
								}

								startday.date_time.D += days_to_end_of_month;
								if( startday.date_time.T  > start_time )
								{
									startday.date_time.D += 1;
								}
								startday.date_time.T = 0;
								update_running_dtcs( &startday, startday.date_time.D ); // set calendar data
							}
						} while( days_to_end_of_period > days_to_end_of_month );
					}
					else // using time based irrigation
					{
						update_running_dtcs( &startday, startday.date_time.D ); // make sure the day, month, year values are set

						endday.date_time.D = bds->meter_read_date[budget_idx+1];
						endday.date_time.T = bds->meter_read_time;

						days_on = getNumStarts( &startday, &endday, start_time, &budget_data, ptr_group );

						// 03/16/2016 skc : Skip if no adjust days are in effect
						days_to_end_of_period = endday.date_time.D - startday.date_time.D;
							 
						days_on = calc_percent_adjust( days_on, &percent_adjust_days, percent_adjust, days_to_end_of_period );

						// Bypass nm_WATERSENSE_calculate_run_time(), skip error checks, better control of et and crop_coeff values!
						volume = getVolume( &startday, days_on, &budget_data );

						volume_scheduled += volume;
					}

				} // end if(SCHEDULE)

				// 8/25/2016 skc : Apply reductions to irrigation volume
				rv += sf * volume_scheduled;

				// ----------

				// Manual programs exist seperately from Schedules
				if( budget_data.GID_manual_program_A ) // only if the manual program exists
				{
					// Don't call if we have no manual program, will generate an alert
					ptr_manprog = MANUAL_PROGRAMS_get_group_with_this_GID( budget_data.GID_manual_program_A );

					if( ptr_manprog )
					{
						// Get the portion of the predicted volume attributed to manual program A
						times_on = getNumManualStarts( today, bds, &budget_data, ptr_manprog );

						volume = times_on * MANUAL_PROGRAMS_get_run_time( ptr_manprog ) * budget_data.expected_flow_rate_gpm / SIXTY;

						rv += volume;
					}
				}

				if( budget_data.GID_manual_program_B ) // only if the manual program exists
				{
					// Don't call if we have no manual program, will generate an alert
					ptr_manprog = MANUAL_PROGRAMS_get_group_with_this_GID( budget_data.GID_manual_program_B );

					if( ptr_manprog )
					{
						// Get the portion of the predicted volume attributed to manual program B
						times_on = getNumManualStarts( today, bds, &budget_data, ptr_manprog );

						volume = times_on * MANUAL_PROGRAMS_get_run_time( ptr_manprog ) * budget_data.expected_flow_rate_gpm / SIXTY;

						rv += volume;
					}
				}

			} // end if(ptr_group)
		} // end if(in_use) 

		ptr_station = nm_ListGetNext( &station_info_list_hdr, ptr_station );
	} // end while( ptr_station )

	return rv;
}

/* ---------------------------------------------------------- */
// 4/4/2016 skc : Calculate how much each POC for a system has used from recent history.
// gid_sys - The system to be examind
// poc_usage[] - Array of size MAX_POCS_IN_NETWORK, must be allocated by caller
// Returns - The volume for the entire system during the search.
// 7/28/2016 skc : The POC report records only record flow made with a flow meter.
// Systems without flow meters will have values of 0 in the POC reports.
// 7/29/2016 skc : Search all the records
static UNS_32 nm_BUDGET_get_poc_usage( UNS_32 gid_sys, UNS_32 poc_usage[] )
{
	POC_REPORT_RECORD	*lprr;

	UNS_32 idx_poc;

	UNS_32 rv_sys_usage;

	// ----------

	rv_sys_usage = 0;
	
	xSemaphoreTakeRecursive( poc_report_completed_records_recursive_MUTEX, portMAX_DELAY );

	// 7/28/2016 skc : This only counts flow via a flow meter. Systems without flowmeter will
	// have values 0f 0.
	lprr = nm_POC_REPORT_RECORDS_get_most_recently_completed_record();

	while( lprr != NULL )
	{
		// if this station is running on this system
		if( gid_sys == POC_get_GID_irrigation_system_using_POC_gid( lprr->poc_gid ) )
		{
			// 4/4/2016 skc : We only use irrigation usage to calculate the POC ratios. Other usage
			// types most likely don't represent normal flow and pressure in the system, thus are
			// unreliable for comparing POC flow rates

			// sum usage for this system
			rv_sys_usage += lprr->gallons_during_irrigation;

			// get the index into poc_preserves[]
			POC_PRESERVES_get_poc_preserve_for_this_poc_gid( lprr->poc_gid, &idx_poc );

			// sum irrigation usage for each POC
			poc_usage[idx_poc] += lprr->gallons_during_irrigation;
		}

		lprr = nm_POC_REPORT_RECORDS_get_previous_completed_record( lprr );
	}

	xSemaphoreGiveRecursive( poc_report_completed_records_recursive_MUTEX );

	// We can compare to budget_preserves.bpr[n].used_gallons to test consistency, if we wanted to

	return rv_sys_usage;
}

// 7/28/2016 skc : Find the ratio each POC uses in the system.
// Assumes rpoc is valid and sized MAX_POCS_IN_NETWORK.
// Fills the rpoc array with the percentage use for each POC in the system
// @called by - td_check, screen display
// @mutex_requirements -  poc_preserves_recursive_MUTEX
extern void nm_BUDGET_calc_rpoc( UNS_32 gid_sys, 
								 float rpoc[] )
{
	UNS_32 idx_poc; 
	UNS_32 gid_poc;
	UNS_32 poc_count; // how many POC's connected to a system?
	UNS_32 sys_usage; // track sum of POC usage
	UNS_32 poc_usage[ MAX_POCS_IN_NETWORK ]; // useage for a particular POC
	BY_SYSTEM_RECORD *bsr;
	POC_GROUP_STRUCT * pgs;

	memset( poc_usage, 0, sizeof(poc_usage) );

	// Collect the historical data
	sys_usage = nm_BUDGET_get_poc_usage( gid_sys, poc_usage );

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	// 8/2/2016 skc : Can't use this because it doesn't account for lack of flow meters
	//poc_count = bsr->sbf.number_of_pocs_in_this_system;

	// 8/2/2016 skc : We need to find the number of VALID pocs on this system
	// 8/22/2016 skc : A POC must have a budget value to be counted.
	poc_count = 0;
	for( idx_poc = 0; idx_poc < MAX_POCS_IN_NETWORK; ++idx_poc )
	{
		gid_poc = poc_preserves.poc[idx_poc].poc_gid;
		bsr = poc_preserves.poc[idx_poc].this_pocs_system_preserves_ptr;

		// 8/23/2016 skc : Validate the gid's and POC usage
		if( (0 != gid_poc) &&
			(gid_sys == bsr->system_gid)  &&
			POC_use_for_budget( idx_poc ) )
		{
			// 8/23/2016 skc : Only POCs with valid budget values get to participate
			pgs = nm_POC_get_pointer_to_poc_with_this_gid_and_box_index( gid_poc, POC_get_box_index_0( gid_poc) );
			if( (pgs != NULL) && (POC_get_budget( pgs, 0 ) > 0) )
			{
				poc_count++;
			}
		}
	}

	// ----------

	// Find Rp for each valid POC on this system
	for( idx_poc = 0; idx_poc < MAX_POCS_IN_NETWORK; ++idx_poc )
	{
		// get index into list
		// verify poc is in use, and is connected to this system
		gid_poc = poc_preserves.poc[idx_poc].poc_gid;

		bsr = poc_preserves.poc[idx_poc].this_pocs_system_preserves_ptr;

		// 8/2/2016 skc : Filter out POC with no gid, don't match the system, not used for budgets
		// 8/22/2016 skc : POCs with no budgets don't participate 
		if( (0 == gid_poc) || 
			(gid_sys != bsr->system_gid) || 
			!POC_use_for_budget( idx_poc ) )
		{
			continue;
		}

		pgs = nm_POC_get_pointer_to_poc_with_this_gid_and_box_index( gid_poc, POC_get_box_index_0( gid_poc) );
		if( (pgs == NULL) || (POC_get_budget( pgs, 0 ) == 0) )
		{
			continue;
		}

		// ----------
		// 8/23/2016 skc : Made it here, this POC is to be used in budget calculations
		
		// 4/4/2016 skc : If only one POC is attached to the mainline, skip ratio
		// calculations.
		if( 1 == poc_count )
		{
			rpoc[idx_poc] = 1; // By definition, it's one-to-one
		}
		else if( poc_count > 1 )// multiple POCs
		{
			if( sys_usage == 0 ) // The system has seen no flow. Either never ran, or no flow meters.
			{
				if( bsr->sbf.number_of_flow_meters_in_this_sys > 0 )
				{
					// 8/2/2016 skc : Only show message if flow meters are involved
					Alert_Message_va("BUDGET %d/%d - No measured volume", gid_sys, gid_poc);
				}

				// 8/2/2016 skc : Systems with no flow meters divide evenly. If we really have yet to
				// measure flow, then also divide evenly until next time when we will have measured some.
				rpoc[idx_poc] = 1.f / (float)poc_count;
			}
			else
			{
				if ( poc_usage[idx_poc] == 0 ) // No flow measured on the POC
				{
					rpoc[idx_poc] = 0; // got a better idea?
				}
				else // get this POC percentage of the system flow
				{
					rpoc[idx_poc] = (float)poc_usage[idx_poc] / (float)sys_usage;
				}
			}
		}
	} // end for( idx_poc )

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
// 02/24/2016 skc : Calculate the percentage each POC uses from the total POC usage
// For systems with flow meters, use the POC report data to determine historical usage,
// otherwise use (1/num POCs) as the ratio.
// @mutex_requirements -  poc_preserves_recursive_MUTEX, list_system_recursive_MUTEX
static void nm_calc_POC_ratios( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem )
{
	UNS_32 idx_poc;

	UNS_32 gid_sys;

	UNS_32 gid_poc;

	float Rpoc[ MAX_POCS_IN_NETWORK ]; // ratio for a particular POC

	float budget;

	float usedpoc;

	float newval;


	// Clear/initialize data
	memset( Rpoc, 0, sizeof(Rpoc) );

	// Find the number of POCs for this system, we are also initializing poc_ratio to INFINITY
	nm_SYSTEM_set_poc_ratio( psystem, INFINITY ); // we will be finding smaller values

	gid_sys = nm_GROUP_get_group_ID(psystem); // look up only once per POC loop

	// 7/28/2016 skc : Get each POCs percentage of system usage
	nm_BUDGET_calc_rpoc( gid_sys, Rpoc );

	// ----------

	// find the minimum (Vpoc - Usedpoc)/Rp for the system, this will be our final ratio
	for( idx_poc = 0; idx_poc < MAX_POCS_IN_NETWORK; ++idx_poc )
	{
		gid_poc = poc_preserves.poc[idx_poc].poc_gid;

		// 8/2/2016 skc : Filter out POC with no gid, don't match the system
		if( (0 == gid_poc) || 
			(gid_sys != poc_preserves.poc[idx_poc].this_pocs_system_preserves_ptr->system_gid) )
		{
			continue;
		}

		// 8/2/2016 skc : Filter out POC not used for budgets
		if( !POC_use_for_budget( idx_poc ) )
		{
			// 3/21/2018 ajv : Suppressed. Information is considered proprietary. 
			// Alert_Message_va("POC Ratio: (%d:%d) not used", gid_sys, gid_poc);
			continue;
		}

		budget = POC_get_budget( POC_get_group_at_this_index(POC_get_index_for_group_with_this_GID(gid_poc)), 0 );
	
		usedpoc = nm_BUDGET_get_used( idx_poc );

		// 8/25/2016 skc : Because the flow sensors are not guaranteed to be exact same as the
		// official flow meter, we introduce a 4% fudge factor to keep under budget.
		newval = ((0.96 * budget) - usedpoc) / Rpoc[idx_poc];

#if 0
		// 5/23/2016 skc : Gallons to HCF conversion factor, handle FP bug.
		const volatile float HCF_CONVERSION = 748.05f;

		// 6/7/2016 skc : Slight differences in text format for gallons vs HCF units.
		if( NETWORK_CONFIG_get_water_units() == NETWORK_CONFIG_WATER_UNIT_GALLONS)
		{
			// 3/21/2018 ajv : Suppressed. Information is considered proprietary. 
			// Alert_Message_va("POC Ratio: (%d:%d) B: %0.0f U: %0.0f r: %0.2f sf: %0.2f", gid_sys, gid_poc, budget, usedpoc, Rpoc[idx_poc], newval);
		}
		else // units of HCF
		{
			float sf = 1.f / HCF_CONVERSION;
			// 3/21/2018 ajv : Suppressed. Information is considered proprietary. 
			// Alert_Message_va("POC Ratio: (%d:%d) B: %0.1f U: %0.1f r: %0.2f sf: %0.2f", gid_sys, gid_poc, sf * budget, sf * usedpoc, Rpoc[idx_poc], newval);
		}
#endif

		// 8/22/2016 skc : The budget value must be > 0 to set the POC ratio. If all the POCs for a
		// system have zero budget the poc ratio will be INFINITY, this is ok. The code that
		// determines the actual run time never returns more than the actual requested irrigation
		// time. Also, the code at start time checks again for Systems with zero budget, and will
		// skip the budget calcs.
		if( budget > 0 )
		{
			if ( newval < nm_SYSTEM_get_poc_ratio(psystem) )
			{
				nm_SYSTEM_set_poc_ratio( psystem, newval );
			}
		}
	}
}

// 3/31/2016 skc : This function counts on the predicted volume and POC ratios
// recently being calculated. Typically, the software will come here multiple times
// consecutively as a result of a start time occuring, all using the same predicted and
// ratio values.
static float calc_time_adjustment_low_level( float t_orig, UNS_32 mode, IRRIGATION_SYSTEM_GROUP_STRUCT* const system_ptr, STATION_STRUCT* const pss_ptr)
{
	float rv_t;

	float Vp;	// The predicted volume needed to the end of the budget period

	float ratio;

	UNS_32 reduction_limit; // Each station_group has a percentage max reduction limit

	UNS_32 station_preserves_index;

	UNS_8 pi_flag2; // bitfield of budget reasons

	// ----------

	pi_flag2 = 0;

	// Calculate a new time
	rv_t = t_orig;

	Vp = nm_SYSTEM_get_Vp(system_ptr ); // precomputed predicted volume

	ratio = nm_SYSTEM_get_poc_ratio(system_ptr );
	//Alert_Message_va("cta %d %0.0f %0.3f", idx_sys, Vp, ratio); 

	// 02/05/2016 skc : Can this ever happen?
	// We've made sure we have valid system, POC, group, station, budget period, etc.
	// Yet we may still get a zero predicted volume?
	if( Vp != 0) // prevent divide by zero
	{
		rv_t = t_orig * ratio / Vp;

		#if 0
		// 4/5/2016 skc : Notes for priority scheme
		// Vp = sum(fr * t),	Sum of flowrate * time for station groups in this system, for one starttime
		// (%)Vp = x(G0),	The priority percent * Vt = x * (Group Usage), solve for x - the new time factor
		// Thus x = (%)Vp / G0, multiply the time for this group by x to get budget time with priority watering cycle

		// The predicted volume for a station group over a start time is scaled by the POC's priority percentage.
		// sg - station group priority number
		// sum(sg) - sum of the station group priority numbers
		// Vp - total predicted volume for this start time
		// Gp - predicted volume for the station group
		// t = (1/(sg / sum(sg))) * Vp / Gp * T

		// The predicted could also be calculated to the end of the budget period
		// We may want to calculate predicteds using station groups instead of systems
		#endif
	}

	// 4/13/2016 skc : Reduction limit is % of the original requested time.
	// If the limit is 25, this means the time can be reduced at most to 75% of t_orig.
	// The 0.01f term converts to percentage units.
	reduction_limit = STATION_GROUP_get_budget_reduction_limit_for_this_station( pss_ptr );
	if( rv_t < t_orig * (100 - reduction_limit) * 0.01f )
	{
		rv_t = t_orig * (100 - reduction_limit) * 0.01f;

		// 7/13/2016 skc : Set flag indicating the budget reduction limit was used when reducing
		// irrigation time.
		B_SET( pi_flag2, STATION_history_bit_budget_reduction_limit );
	}

	// Catch time of less than zero 
	if( rv_t < 0 )
	{
		rv_t = 0.f;
	}

	// If the requested time is less than or equal to the budget calculated time, give it to the
	// station.
	if( t_orig <= rv_t )
	{
		rv_t = t_orig;
	}
	else // we reduced irrigation due to budgets
	{
		// 7/13/2016 skc : apply rule that only one of the budget bits can be set
		if( !B_IS_SET( pi_flag2, STATION_history_bit_budget_reduction_limit ) )
		{
			B_SET( pi_flag2, STATION_history_bit_budget_applied );
		}
	}

	// ----------

	// save the bits into the station preserves history record
	// 8/5/2016 skc : But not in alert only mode, FogBugz #24
	if( mode != IRRIGATION_SYSTEM_BUDGET_MODE_ALERT_ONLY )
	{
        if( STATION_PRESERVES_get_index_using_ptr_to_station_struct( pss_ptr, &station_preserves_index ) )
		{
			xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

			// 7/25/2016 skc : Make sure the first two bits are set correctly.
			B_UNSET( station_preserves.sps[ station_preserves_index ].station_history_rip.pi_flag2, STATION_history_bit_budget_applied );
			B_UNSET( station_preserves.sps[ station_preserves_index ].station_history_rip.pi_flag2, STATION_history_bit_budget_reduction_limit );
			
			// 8/19/2016 skc : If reduction limit is zero, then don't set the bits, FogBugz #29
			if( reduction_limit > 0 )
			{
				// 7/13/2016 rmd : In the flag field set only the bit we're here to set. Preserve the other bits
				// in the field. This is accomplished with the |= and knowing our local field is 0 to begin with.
				station_preserves.sps[ station_preserves_index ].station_history_rip.pi_flag2 |= pi_flag2;
			}

			xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
		}
	}

#if 0
	Alert_Message_va("Calc_time (%d:%d): Mode: %d Ratio: %0.2f Vp: %0.0f, RL: %d T0: %0.2f T1: %0.2f\n", 
					 nm_STATION_get_box_index_0(pss_ptr), nm_STATION_get_station_number_0(pss_ptr)+1, 
					 mode, ratio, Vp, reduction_limit, t_orig, rv_t);
#endif

	return rv_t;
}

/* ---------------------------------------------------------- */
/* 
3/28/2016 skc : Update the budget preserves and send a report to comm server. 
Once a day we send a budget report. At the time of the report, make sure the data values 
are uptodate by copying into a BUDGET_REPORT_RECORD object.
 
Task: Called under TD_CHECK 
 
Mutex: Requires poc_preserves_recursive_MUTEX 
*/ 
static void  nm_handle_sending_budget_record( UNS_32 system_gid, const BUDGET_DETAILS_STRUCT * const pbds, DATE_TIME_COMPLETE_STRUCT today )
{
	BY_POC_BUDGET_RECORD *pbrr;

	SYSTEM_BUDGET_REPORT_RECORD *sbrr;

	IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys;

	BY_SYSTEM_RECORD *bsr;

	// 3/31/2016 skc : Make a copy of the data and save to the file backed report structure.
	BUDGET_REPORT_RECORD report = {}; // Compile time initialize all members to 0

	float predicted;

	float used;

	UNS_32 i;

	BUDGET_DETAILS_STRUCT bds;

	// ----------

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	ptr_sys = SYSTEM_get_group_with_this_GID( system_gid );

	// ----------

	// 3/31/2016 skc : Set data for all related POCs
	//
	// 6/10/2016 rmd : Pass through the preserves looking for active preserves records (gid !=
	// 0), that are for a POC belonging to the SYSTEM we are interested in. [The ppoc_preserves
	// by its definition holds ALL active POCs in the network, so we will see all the pocs.]
	for( i=0; i<MAX_POCS_IN_NETWORK; ++i )
	{
		// only proceed if the POC is available and is attached to the system
		if( poc_preserves.poc[i].poc_gid == 0 )
		{
			continue;
		}

		bsr = poc_preserves.poc[i].this_pocs_system_preserves_ptr; 

		if( bsr->system_gid == system_gid )
		{
			pbrr = &poc_preserves.poc[i].budget;

			pbrr->poc_gid = poc_preserves.poc[i].poc_gid;

			xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY ); 
			pbrr->budget = POC_get_budget( POC_get_group_at_this_index(POC_get_index_for_group_with_this_GID(pbrr->poc_gid)), 0 );
			xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

			//pbrr->used_gallons = // Already set, once per second 

			//pbrr->on_at_start_time = // Set elsewhere

			//pbrr->off_at_start_time = // Set elsewhere

			report.bpbr[i] = *pbrr; // copy structure values
		}
	}

	// ----------

	report.record_date = today.date_time.D;

	// 3/31/2016 skc : Get current predicted and used amounts
	SYSTEM_get_budget_details( ptr_sys, &bds );

	predicted = nm_BUDGET_predicted_volume( system_gid, &bds, &today, today.date_time, false );
	used = nm_SYSTEM_get_used( ptr_sys );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	// ----------

	sbrr = &SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( system_gid )->budget.rip; 

	sbrr->system_gid = system_gid;

	sbrr->in_use = pbds->in_use;

	sbrr->mode = pbds->mode;

	sbrr->start_date = pbds->meter_read_date[0];

	sbrr->end_date = pbds->meter_read_date[1];

	sbrr->meter_read_time = pbds->meter_read_time;

	sbrr->predicted_use_to_end_of_period = (UNS_32)(predicted + used + 0.5f);

	//sbrr->reduction_gallons = // Set elsewhere 

	//sbrr->closing_record_for_this_period = // Set elsewhere

	// ----------

	report.sbrr = *sbrr; // copy structure values

	nm_BUDGET_REPORT_DATA_close_and_start_a_new_record( &report );
}

/* ---------------------------------------------------------- */
// 4/21/2016 skc : Update the budget values by one month when a rollback is detected. If
// the time ever gets set backwards, this function helps keep the data straight.
// Mutex: Requires poc_preserves_recursive_MUTEX
// bp_idx - array index into poc_preserves
static void nm_handle_budget_value_rollback( UNS_32 bp_idx, const BUDGET_DETAILS_STRUCT * const pbds )
{
	INT_32 j;

	POC_GROUP_STRUCT *ptr_poc;

	UNS_32 saved_budget;

	// ----------

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	ptr_poc = POC_get_group_at_this_index( POC_get_index_for_group_with_this_GID(poc_preserves.poc[ bp_idx ].poc_gid) );

	// 4/21/2016 skc : Update the budget values
	// 1) Save the value to be rolled
	// 2) Push the last value off the array
	// 3) Place the saved value at index position 0
	saved_budget = POC_get_budget( ptr_poc, pbds->number_of_annual_periods - 1 );

	// Move the data back one index in the array
	for( j=IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS-2; j>=0; j-- )
	{
		POC_set_budget( ptr_poc, j+1, POC_get_budget( ptr_poc, j ) );
	}

	// Use the saved budget value as the new first value in the array
	POC_set_budget( ptr_poc, 0, saved_budget );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
// 3/28/2016 skc : Update the budget values after a rollover is detected
// Mutex: Requires poc_preserves_recursive_MUTEX
// bp_idx - array index into poc_preserves
static void nm_handle_budget_value_updates( UNS_32 bp_idx, const BUDGET_DETAILS_STRUCT * const pbds )
{
	int j;

	POC_GROUP_STRUCT *ptr_poc;

	UNS_32 saved_budget;

	// ----------

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	ptr_poc = POC_get_group_at_this_index( POC_get_index_for_group_with_this_GID(poc_preserves.poc[ bp_idx ].poc_gid) );

	// 3/24/2016 skc : Update the budget values
	// 1) Save the value to be rolled
	// 2) Pop the first value off the array
	// 3) Place the saved value at the number of annual periods index position
	saved_budget = POC_get_budget( ptr_poc, 0 );

	// Move the data up one index in the array
	for( j=1; j<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS; ++j )
	{
		POC_set_budget( ptr_poc, j-1, POC_get_budget( ptr_poc, j ) );
	}

	// Use the saved budget value as the new value a (year - 1 budget period) from now
	POC_set_budget( ptr_poc, pbds->number_of_annual_periods - 1, saved_budget );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
// 4/21/2016 skc : Udpate the budget period values after a rollback is detected
// Mutex: Requires list_system_recursive_MUTEX
static void nm_handle_budget_period_rollback( IRRIGATION_SYSTEM_GROUP_STRUCT* psys, BUDGET_DETAILS_STRUCT * pbds )
{
	INT_32 j;

	UNS_32 newday, day, month, year, dow;

	// 4/21/2016 skc : Update the budget dates, we are assuming budgets match up with years
	// 1) Push the last value off the array
	// 2) Figure out the new date and place at index position 0

	// Move the data back one index in the array
	for( j=IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS-2; j>=0; j-- )
	{
		pbds->meter_read_date[j+1] = pbds->meter_read_date[j];
	}

	// 4/21/2016 skc : Calculate the new budget date
	newday = pbds->meter_read_date[ 1 ];
	DateToDMY( newday, &day, &month, &year, &dow );
	if( pbds->number_of_annual_periods == IRRIGATION_SYSTEM_BUDGET_ANNUAL_PERIODS_MAX)
	{
		month -= 1;
	}
	else // assume 2 month cycles
	{
		month -= 2;
	}

	// handle wraparound
	if( (INT_32)month < 0 ) //remember, UNS_32 is always > 0
	{
		month += (MONTHS_IN_A_YEAR - 1);
		year -= 1;
	}
	newday = DMYToDate( day, month, year );

	// set the new first budget period date at index zero
	pbds->meter_read_date[ 0 ] = newday;

	// ----------

	// Save values and send change line alerts
	for( j=0; j<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS; ++j )
	{
		nm_SYSTEM_set_budget_period( psys, 
									j,
									pbds->meter_read_date[j], 
									CHANGE_do_not_generate_change_line, 
									CHANGE_REASON_DAY_CHANGE, 
									FLOWSENSE_get_controller_index(), 
									CHANGE_set_change_bits,
									SYSTEM_get_change_bits_ptr( psys, CHANGE_REASON_DAY_CHANGE ) );
	}
}

/* ---------------------------------------------------------- */
// 3/28/2016 skc : Update the budget values after a rollover is detected
// Mutex: Requires list_system_recursive_MUTEX
static void nm_handle_budget_period_updates( IRRIGATION_SYSTEM_GROUP_STRUCT* psys, BUDGET_DETAILS_STRUCT * pbds )
{
	UNS_32 j;

	UNS_32 newday, day, month, year, dow;

	// 3/24/2016 skc : Update the budget dates, we are assuming budgets match up with years
	// 1) Pop the first value off the array
	// 2) Add one year to the first date
	// 3) Place this value at the number of annual periods index position

	// Move the data up one index in the array
	for( j=1; j<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS; ++j )
	{
		pbds->meter_read_date[j-1] = pbds->meter_read_date[j];
	}

	// 3/28/2016 skc : Aadd one year to the first date value, and save at the end of the list.
	// Update the other dates in the future also.
	for( j=0; j<pbds->number_of_annual_periods; j++ )
	{
		newday = pbds->meter_read_date[j];
		DateToDMY( newday, &day, &month, &year, &dow );
		year += 1;
		newday = DMYToDate( day, month, year );

		// set the budget period date for a year from now
		pbds->meter_read_date[ j + pbds->number_of_annual_periods ] = newday;
	}

	// Save values and send change line alerts
	for( j=0; j<IRRIGATION_SYSTEM_BUDGET_NUMBER_PERIODS; ++j )
	{
		nm_SYSTEM_set_budget_period( psys, 
									j,
									pbds->meter_read_date[j], 
									CHANGE_do_not_generate_change_line, 
									CHANGE_REASON_DAY_CHANGE, 
									FLOWSENSE_get_controller_index(), 
									CHANGE_set_change_bits,
									SYSTEM_get_change_bits_ptr( psys, CHANGE_REASON_DAY_CHANGE ) );
	}
}

/* ---------------------------------------------------------- */
/*
03/30/2016 skc : Given a system, increment the attached POC's to be on/off at start time 
 
Mutex: list_system_recursive_MUTEX, poc_preserves_recursive_MUTEX 
*/
static void nm_BUDGET_increment_off_at_start_time( IRRIGATION_SYSTEM_GROUP_STRUCT* psystem )
{
	UNS_32 i;

	UNS_32 gid_system;

	// ----------

	gid_system = nm_GROUP_get_group_ID( psystem );

	for( i=0; i<MAX_POCS_IN_NETWORK; i++ )
	{
		BY_POC_RECORD * const pbpr = &poc_preserves.poc[i];

		// Find POCs that are enabled and attached to the system
		if( 0 != pbpr->poc_gid &&
			gid_system == pbpr->this_pocs_system_preserves_ptr->system_gid )
		{
			pbpr->budget.off_at_start_time += 1;
		}
	}
}

static void nm_BUDGET_increment_on_at_start_time( IRRIGATION_SYSTEM_GROUP_STRUCT* psystem )
{
	UNS_32 i;

	UNS_32 gid_system;

	// ----------

	gid_system = nm_GROUP_get_group_ID( psystem );

	for( i=0; i<MAX_POCS_IN_NETWORK; i++ )
	{
		BY_POC_RECORD * const pbpr = &poc_preserves.poc[i];

		// Find POCs that are enabled and attached to the system
		if( 0 != pbpr->poc_gid &&
			gid_system == pbpr->this_pocs_system_preserves_ptr->system_gid )
		{
			pbpr->budget.on_at_start_time += 1;
		}
	}
}

/* ---------------------------------------------------------- */
/**
  * <description> Returns the time allowed for a station under monthly budget constraint
  * Assumes the station has already been vetted for 'active', 'running', etc.
  * t = T * ratio / Vp
  *
  * @mutex_requirements -  list_system_recursive_MUTEX, list_program_data_recursive_MUTEX 
  *
  * @memory_responsibities - None
  * 
  * @task_execution - tdcheck
  *
  * @param t_orig - Time (minutes) the station says it wants after adjustments and such
  *
  * @param pss_ptr - The station needing time adjustment
  *
  * @return float - The time (minutes) the station is allowed to run for
  *
  * @author 01/13/2016 skc
  */
extern float nm_BUDGET_calc_time_adjustment( float t_orig, STATION_STRUCT *pss_ptr, DATE_TIME dt )
{
	float rv_t;	// Return value

	UNS_32 sys_gid; // The irrigation_gid for the station

	IRRIGATION_SYSTEM_GROUP_STRUCT* system_ptr;

	BUDGET_DETAILS_STRUCT bds; // a copy of system budget data

	BY_SYSTEM_RECORD *bsr; // for tracking report reduction_gallons

	// 3/31/2016 skc : If we alert the user about budgets, do it only once per start time
	static DATE_TIME dt_flag = {0,0}; // Initialize static to zero day and time, for budgets
	static UNS_32 saved_gid = 0;	// if a different system comes along, make an alert for it

	float saved; // Budget report data

	float Vp;	// used in alert message
	float ratio;// used in alert message

	// ----------
	saved = 0.f;

	rv_t = t_orig; // initial value for return

	// get pointers and indices to the mainline/POC for this station
	sys_gid = STATION_GROUP_get_GID_irrigation_system_for_this_station( pss_ptr );

	bsr = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( sys_gid );

	system_ptr = SYSTEM_get_group_with_this_GID( sys_gid );

	SYSTEM_get_budget_details( system_ptr, &bds );

	// Only adjust the time if budgets is turned on.
	// If we aren't in a budget period, then don't adjust the irrigation run time.
	// We also must have POCs
	// 5/25/2016 skc : And the budget value > 0
	if( (IRRIGATION_SYSTEM_BUDGET_IN_USE_YES == bds.in_use) && 
		(0 == SYSTEM_get_budget_period_index(&bds, dt)) &&
		(0 < bsr->sbf.number_of_pocs_in_this_system) &&
		(0 < nm_SYSTEM_get_budget(system_ptr, 0)) )
	{
		switch( bds.mode )
		{
			case IRRIGATION_SYSTEM_BUDGET_MODE_ALERT_ONLY:
				// Use the original time
				rv_t = calc_time_adjustment_low_level(t_orig, bds.mode, system_ptr, pss_ptr );

				if( rv_t < t_orig )
				{
					// Alert the user, only once per start time
					if( dt_flag.D == 0 || 
						DT1_IsBiggerThan_DT2( &dt, &dt_flag ) ||
						sys_gid != saved_gid )
					{
						// Send the alert about time adjustment
						// 9/12/2016 skc : Don't sent the alert
						//Alert_Message_va("Budget restrictions ignored - Alerts Only");

						dt_flag = dt; // copy time value for testing next time
						saved_gid = sys_gid;
					}
				
					rv_t = t_orig; // Don't actually adjust the time
				}

				saved = 0.f;

				break;

			case IRRIGATION_SYSTEM_BUDGET_MODE_AUTOMATIC:
				// Calculate a new cycle time
				rv_t = calc_time_adjustment_low_level(t_orig, bds.mode, system_ptr, pss_ptr );

				// Keep track of water "saved"
				if(  rv_t < t_orig )
				{
					saved = (t_orig - rv_t) * nm_STATION_get_expected_flow_rate_gpm( pss_ptr ) / 60.f;
				}
				break;

// 02/10/2016 skc : These options don't appear in the initial release.
#if 0
			case IRRIGATION_SYSTEM_BUDGET_USE_CHOICE_manual_reduction:
				// implement a priority scheme
				t = t_orig; // UNTIL WE HAVE SOMETHING
				break;
			case IRRIGATION_SYSTEM_BUDGET_USE_CHOICE_stop_at_budget:
				// No time adjustments, when the budget is reached, then stop
				V = SYSTEM_get_budget( system_ptr, idx_budget );
				Used = SYSTEM_get_used( system_ptr, idx_budget );

				if( Used >= V ) // If we are at the budget limit
				{
					t = 0.;
				}
				else // this station will get some water
				{
					// How much time if all the volume went to this station?
					xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );
					float flowrate = nm_STATION_get_expected_flow_rate_gpm( pss_ptr ) / SIXTY;
					xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

					t = (V - Used) / flowrate;

					if( t > t_orig ) // Don't give the user more than they asked for
					{
						t = t_orig;
					}
					
					if( t < 0 ) // Catch time of less than zero 
					{
						t = 0.;
					}
				}
				break;
#endif
			default: // shouldn't ever get to here
				Alert_Message_va("Invalid budget mode: %d, POC gid: %d", bds.mode, sys_gid);
		}

		// Alert the user, only once per start time, (once per day?)
		if( dt_flag.D == 0 || 
			DT1_IsBiggerThan_DT2( &dt, &dt_flag ) ||
			sys_gid != saved_gid )
		{
			// Send the alert about time adjustment
			Vp = nm_SYSTEM_get_Vp(system_ptr ); // precomputed predicted volume
			ratio = nm_SYSTEM_get_poc_ratio(system_ptr ); // precomputed poc ratio

			Alert_Message_va("Calc_time (%d): M: %d R: %0.2f Vp: %0.0f", 
							 sys_gid, bds.mode, ratio, Vp);

			// reset values for later comparison
			dt_flag = dt; // Reset the time
			saved_gid = sys_gid;
		}


	}
	else // Budgets are turned off, failed to get a valid budget period, budget value is 0
	{
		rv_t = t_orig;	// no adjustment made
		saved = 0.f;
		//Alert_Message_va("calc_time (%d:%d): %d, %0.2f, %0.2f", nm_STATION_get_box_index_0(pss_ptr), nm_STATION_get_station_number_0(pss_ptr)+1, -1, t_orig, t);
	}

	// ----------
	 
	// 4/21/2016 skc : Save the calculation ratio and saved amount into budget report record.
	bsr->budget.rip.reduction_gallons += saved;

	// 7/8/2016 skc : Ratio of actual to requested irrigation time.
	bsr->budget.rip.ratio = rv_t / t_orig; 

	// 7/8/2016 skc : Trap for divide by zero here (shouldn't happen)
	if( !isnormal(bsr->budget.rip.ratio) )
	{
		bsr->budget.rip.ratio = 0.f;
	}

	return rv_t;
}

/* ---------------------------------------------------------- */
// 02/24/2016 skc : For a given system, returns the predicted volume until the end of the budget period
// Multiple POCs can exist on a mainline system
// @mutex_requirements -	list_system_recursive_MUTEX
// 							list_program_data_recursive_MUTEX
// Assumes we are actually in a budget period
extern float nm_BUDGET_predicted_volume( UNS_32 system_GID, 
										 BUDGET_DETAILS_STRUCT *const bds, 
										 const DATE_TIME_COMPLETE_STRUCT *const today,
										 const DATE_TIME right_now,
										 BOOL_32 use_limits )
{
	float rv; // return value, predicted volume

	// ----------

	rv = 0.f;

	if( !NETWORK_CONFIG_get_controller_off() )
	{
		// The predicted volume for automatic and manual watering
		rv += getScheduledVp( system_GID, bds, today, right_now, use_limits );

		// Now check the list of running stations
		rv += getIrrigationListVp( system_GID );
	}

	return rv;
}

/* ---------------------------------------------------------- */
/*
02/09/2016 skc : This function calculates and stores data used to determine a budget time adjustment.
Call this function once immediately prior to calling BUDGET_calc_time_adjustment for stations. This
method calculates the budget ratio in effect and saves the value in system.poc_ratio[]. When budgets 
require water reductions, we display a note on the screen. The display task will examine this value and
show the message on the screen. 
 
03/30/2016 skc : This function is SUPPOSED to be only called once for each start time. Because of this, 
we will set the budget records for on/off at start time here. 
 
04/14/2016 skc : To support other functionality, this method needs to be called at times other than scheduled 
start times, the is_start_time param was added to accomodate this.
 
@mutex_requirements -	list_program_data_recursive_MUTEX, list_system_recursive_MUTEX 
 
@task_execution TD_CHECK 
*/ 
extern void BUDGET_calculate_ratios( const DATE_TIME_COMPLETE_STRUCT *const ptoday_ptr, BOOL_32 is_start_time )
{
	float Vp;

	IRRIGATION_SYSTEM_GROUP_STRUCT *psystem;

	BUDGET_DETAILS_STRUCT bds;

	UNS_32 numsys;

	UNS_32 i;

	BY_SYSTEM_RECORD *bsr;
	UNS_32 num_pocs;

	// 5/25/2016 skc : If no POCs exist, then don't call WEATHER_TABLES_set_et_ratio()
	BOOL_32 sys_has_poc_flag;

	// ----------

	sys_has_poc_flag = false;

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	numsys = SYSTEM_num_systems_in_use();

	for( i = 0; i < numsys; ++i )
	{
		psystem = SYSTEM_get_group_at_this_index( i );

		// 5/19/2016 skc : Get the number of POCs attached to this system.
		bsr = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid(  nm_GROUP_get_group_ID(psystem) );
		xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
		num_pocs = bsr->sbf.number_of_pocs_in_this_system;
		xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

		SYSTEM_get_budget_details( psystem, &bds );

		// This system must be using budgets and be in the current budget period
		// 5/19/2016 skc : And there must be at least one POC on this system.
		if( (bds.in_use == IRRIGATION_SYSTEM_BUDGET_IN_USE_NO) ||
			(0 != SYSTEM_get_budget_period_index(&bds, ptoday_ptr->date_time)) || 
			(0 == num_pocs) )
		{
			nm_SYSTEM_set_Vp( psystem, 0.f ); // reasonable defaults

			nm_SYSTEM_set_poc_ratio( psystem, 0.f );

			// 4/14/2016 skc : only do this at scheduled start times.
			if( is_start_time )
			{
				xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

				// 3/30/2016 skc : Increment POC's off_at_start_time values.
				nm_BUDGET_increment_off_at_start_time( psystem ); 

				xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
			}
		}
		else
		{
			// 5/25/2016 skc : Go ahead and call WEATHER_TABLES_set_et_ratio()
			sys_has_poc_flag = true;

			xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

			// 4/14/2016 skc : only do this at scheduled start times.
			if( is_start_time )
			{
				// 3/30/2016 skc : Increment POC's on_at_start_time values.
				nm_BUDGET_increment_on_at_start_time( psystem );
			}

			// 03/11/2016 skc : Only calc the ratio if budgets are in use for this system
			nm_calc_POC_ratios( psystem );

			xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

			// 8/23/2016 skc : Predicted volume depends on the POC ratios
			Vp = nm_BUDGET_predicted_volume( nm_GROUP_get_group_ID(psystem), &bds, ptoday_ptr, ptoday_ptr->date_time, false ); // predicted usage to end of period

			nm_SYSTEM_set_Vp( psystem, Vp );

		}

		//Alert_Message_va("bcr(%d) %d|%d %0.2f\n", i, ptoday_ptr->date_time.D, ptoday_ptr->date_time.T, bss.Vp[i]);
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	// 4/28/2016 skc : Precalculate the ET ratio of daily/historical
	if( true == sys_has_poc_flag )
	{
		WEATHER_TABLES_set_et_ratio( ET_DAYS_IN_RATIO );
	}
}

/* ---------------------------------------------------------- */
// 02/11/2016 skc : The status screen would like to know if budget reductions are active,
// system.POC_ratio[] determines this.
// Return 1 if any system is under budget constraints, else return 0.
// @mutex_requirements caller does not need to acquire any mutexes
// @task_execution - Display task
// 4/14/2016 skc : Change return value to 1 to be compatible with EasyGui and indexed
// structures.
extern UNS_32 BUDGET_in_effect_for_any_system()
{
	UNS_32 i;

	IRRIGATION_SYSTEM_GROUP_STRUCT *psystem;

	BUDGET_DETAILS_STRUCT bds;

	UNS_32 rv;

	float ratio;

	UNS_32 numsys;

	BY_SYSTEM_RECORD *bsr;

	// ----------

	rv = 0; // pessimistic default

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	numsys = SYSTEM_num_systems_in_use();
	for( i = 0; i < numsys; ++i )
	{
		psystem = SYSTEM_get_group_at_this_index( i );

		// 5/19/2016 skc : Only if there are POCs attached to this system.
		bsr = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( nm_GROUP_get_group_ID(psystem) );
		if( bsr->sbf.number_of_pocs_in_this_system > 0)
		{
			SYSTEM_get_budget_details(psystem, &bds );

			// 4/14/2016 skc : Only if budgets are enabled and in automatic mode
			if( (bds.in_use != IRRIGATION_SYSTEM_BUDGET_IN_USE_NO) &&
				(bds.mode != IRRIGATION_SYSTEM_BUDGET_MODE_ALERT_ONLY) )
			{
				if( nm_SYSTEM_get_Vp(psystem) > 0 ) // prevent divide by zero
				{
					ratio = nm_SYSTEM_get_poc_ratio(psystem) / nm_SYSTEM_get_Vp(psystem);

					if ( ratio < 1 )
					{
						rv = 1;

						break; // we only need one hit to know the result
					}
				}
			}
		}
	}
	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	return rv;
}

/* ---------------------------------------------------------- */
/*
04/13/2016 skc : Check the system for potential or actual over budget situation. 
We also check for the case where all the budget values for a system are zero.
This gets called at midnight. If budgets is disabled for this system, then we shouldn't get here.
 
@mutex_requirements: No external needed 
@task_execution - TDCheck
*/
extern void BUDGET_handle_alerts_at_midnight( UNS_32 system_gid, DATE_TIME_COMPLETE_STRUCT today )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys;
	BUDGET_DETAILS_STRUCT bds;
	UNS_32 budget;
	float predicted;
	float usedpoc;
	UNS_32 idx_poc;			// index
	POC_GROUP_STRUCT *ptr_poc;
	BOOL_32 flag_no_budget;
	BY_SYSTEM_RECORD *bsr;
	UNS_32 num_pocs;
	float Rpoc[ MAX_POCS_IN_NETWORK ]; // ratio for a particular POC
	float ratio;
	UNS_32 predicted_percent_for_alert;
	UNS_32 budget_entry_option;

	// ----------
	
	// 9/19/2018 Ryan : set to default just in case there are somehow no POCs attached and
	// enabled on system.
	budget_entry_option = POC_BUDGET_GALLONS_ENTRY_OPTION_DEFAULT;

	// 5/19/2016 skc : If there are no POCs, then we can't have a poc budget alert.
	bsr = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( system_gid );

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
	// 5/19/2016 skc : I don't want to block this MUTEX the whole time just for one simple
	// variable.
	num_pocs = bsr->sbf.number_of_pocs_in_this_system;
	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	if( num_pocs > 0)
	{
		xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );
	
		ptr_sys = SYSTEM_get_group_with_this_GID( system_gid );

		SYSTEM_get_budget_details( ptr_sys, &bds );

		// 4/13/2016 skc : If budgets is disabled, then no alerts.
		// 11/2/2016 skc : We can remove this test, leave for now
		if( bds.in_use )
		{
			// 7/28/2016 skc : Get each POCs percentage of system usage
			memset( Rpoc, 0, sizeof(Rpoc) );
			nm_BUDGET_calc_rpoc( system_gid, Rpoc );
			// get the system predicted volume, adjusted by reduction limits
			 
			// 9/13/2016 skc : don't calculate the "reduced" volume in Alert Only mode
			predicted = nm_BUDGET_predicted_volume( system_gid, &bds, &today, today.date_time, 
													(bds.mode != IRRIGATION_SYSTEM_BUDGET_MODE_ALERT_ONLY ? true : false) );

			xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

			for( idx_poc=0; idx_poc<MAX_POCS_IN_NETWORK; idx_poc++ )
			{
				const BY_POC_RECORD * const pbpr = &poc_preserves.poc[ idx_poc ];

				// 7/26/2016 skc : Test if this POC is enabled and attached to the system.
				if( (pbpr->poc_gid != 0) &&
					(pbpr->this_pocs_system_preserves_ptr->system_gid == system_gid) && 
					(system_gid != 0) &&
					POC_use_for_budget( idx_poc ) )
				{
					xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );
					ptr_poc = POC_get_group_at_this_index( POC_get_index_for_group_with_this_GID(pbpr->poc_gid) );

					// 9/19/2018 Ryan : Find entry option currently seleted for the system.
					budget_entry_option = POC_get_budget_gallons_entry_option( ptr_poc );
					
					// 4/13/2016 skc : If the budget value is zero, then send an alert
					flag_no_budget = true; //initialize for each POC, assume the budget is zero
					if( 0 < POC_get_budget( ptr_poc, 0 ) )
					{
						// 4/13/2016 skc : Any nonzero value will turn the flag off.
						flag_no_budget = false;
					}
					xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

					if( flag_no_budget )
					{
						Alert_budget_values_not_set( nm_GROUP_get_name(ptr_sys), nm_GROUP_get_name(ptr_poc) );
					}
					else // continue on with alert checking
					{
						budget = POC_get_budget( POC_get_group_at_this_index(POC_get_index_for_group_with_this_GID(pbpr->poc_gid)), 0 );
						usedpoc = nm_BUDGET_get_used( idx_poc );
						ratio = (budget-usedpoc) / (Rpoc[idx_poc] * predicted);

						// ----------
						
						float reduction = 100.f * (1.f - ratio);
						if( reduction > 100 ) // limit value to 100% reduction
						{
							reduction = 100;
						}

						// 8/29/2016 skc : The reduction_flag exists because the predicted volume is not 100%
						// accurate. Even with 100% limits, the calculated predicted usage may exceed the budget. If
						// we show the actual data, the user will be confused. Thus we make an educated guess as to
						// where we will actually end up in this budget period.
						// 8/30/2016 skc : The reduction must be less than ALL the reduction limits. Groups that
						// have a limit of 100 allow full reduction, they don't clear the flag.
						BOOL_32 reduction_flag = true;
						UNS_32 num_groups = STATION_GROUP_get_num_groups_in_use();
						for( UNS_32 i = 0; i < num_groups; i++ )
						{
							STATION_GROUP_STRUCT *ptr_group = STATION_GROUP_get_group_at_this_index( i );
							if( STATION_GROUP_get_GID_irrigation_system(ptr_group) == system_gid )
							{
								// Test if the reduction limit applies
								UNS_32 limit = STATION_GROUP_get_budget_reduction_limit(ptr_group);
								if( (reduction > limit)  && (limit < 100) )
								{
									reduction_flag = false; // we can't guarantee budget compliance
									break; // we only need one hit to be done
								}
							}
						}
						
						// ---------- 
						// 3/21/2018 ajv : Suppressed as it divulges proprietary information 
						// Alert_Message_va("SKC: %0.2f %d %0.0f %0.2f %0.0f %0.1f %d",ratio, budget, usedpoc, 
						// Rpoc[idx_poc], predicted, reduction, reduction_flag); 

						if( ratio >= 1 ) // plenty of budget available
						{
							predicted_percent_for_alert = 100 - (100 * ((Rpoc[idx_poc] * predicted) + usedpoc) / budget); 
							Alert_budget_under_budget( nm_GROUP_get_name(ptr_poc), predicted_percent_for_alert );
						}
						else if( ratio >= 0 ) // we're going over budget
						{
							// the budget reduction limits are all more than the required reduction, we will use entire
							// budget.
							if( (reduction_flag == true) && (bds.mode != IRRIGATION_SYSTEM_BUDGET_MODE_ALERT_ONLY) )
							{
								Alert_budget_under_budget( nm_GROUP_get_name(ptr_poc), 0 );
							}
							else // reduction limits in effect
							{
								// predict the amount we will go over budget
								predicted_percent_for_alert = 100 * ((Rpoc[idx_poc] * predicted) + usedpoc - budget) / budget;
								Alert_budget_will_exceed_budget( nm_GROUP_get_name(ptr_poc), predicted_percent_for_alert );
							}
						}
						else // we are already over budget
						{
							// 3/6/2018 ajv : If the period ends today, don't show the predicted usage as it 
							// (incorrectly) incorporates the entire day's usage, not just the usage leading up to noon, 
							// when the period actually ends. 
							if( bds.meter_read_date[ 0 ] == today.date_time.D )
							{
								Alert_budget_over_budget_period_ending_today( nm_GROUP_get_name( ptr_poc ), (100 * (usedpoc - budget) / budget) );
							}
							else
							{
								// predict the amount we will go over budget
								predicted_percent_for_alert = 100 * ((Rpoc[idx_poc] * predicted) + usedpoc - budget) / budget;

								Alert_budget_over_budget( nm_GROUP_get_name(ptr_poc), 100 * (usedpoc - budget) / budget, predicted_percent_for_alert );
							}
						}
					}
				}
			}
			
			// 9/18/2018 Ryan : If budget entry mode is set to et option, reclaculate budget based on
			// updated POC ratio
			
			if( budget_entry_option == POC_BUDGET_GALLONS_ENTRY_OPTION_calculated_using_et )
			{
				BUDGET_calc_POC_monthly_budget_using_et( ptr_sys, BUDGET_et_not_changed );
			}

			xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
		}

	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
// 8/16/2016 skc : Send alerts at programmed start time
extern void BUDGET_handle_alerts_at_start_time( UNS_32 system_gid, STATION_GROUP_STRUCT *psg )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys;
	BUDGET_DETAILS_STRUCT bds;
	BY_SYSTEM_RECORD *bsr;
	UNS_32 num_pocs;
	float ratio;
	UNS_32 value;
	BOOL_32 flag;

	// ----------

	// 5/19/2016 skc : If there are no POCs, then we can't have a poc budget alert.
	bsr = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( system_gid );

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
	// 5/19/2016 skc : I don't want to block this MUTEX the whole time just for one simple
	// variable.
	num_pocs = bsr->sbf.number_of_pocs_in_this_system;
	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	if( num_pocs > 0)
	{
		xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

		ptr_sys = SYSTEM_get_group_with_this_GID( system_gid );

		SYSTEM_get_budget_details( ptr_sys, &bds );

		// 4/13/2016 skc : If budgets is disabled, then no alerts.
		// 9/12/2016 skc : Don't send alerts in "Alert Only" mode
		if( bds.in_use && (bds.mode != IRRIGATION_SYSTEM_BUDGET_MODE_ALERT_ONLY) )
		{
			// use precomputed predicted volume
			ratio = nm_SYSTEM_get_poc_ratio(ptr_sys) / nm_SYSTEM_get_Vp(ptr_sys );
			if( ratio < 1 ) // if reductions will be made
			{
				// 4/13/2016 skc : Reduction limit is % of the original requested time.
				// If the limit is 25, this means the time can be reduced at most to 75% of t_orig.
				float reduction_limit = (100.f - STATION_GROUP_get_budget_reduction_limit( psg ));
				reduction_limit /= 100.f; // convert to same scale as ratio

				if( ratio >= reduction_limit )
				{
					// convert parameter into number from 0-100
					value = (1 - ratio) * 100.f;
					flag = false;
				}
				else
				{
					// convert parameter into number from 0-100
					value = (1 - reduction_limit) * 100.f;

					// 7/13/2016 skc : Set flag indicating the budget reduction limit was used when reducing
					// irrigation time.
					flag = true;
				}
				Alert_budget_group_reduction( nm_GROUP_get_name(psg), value, flag );
			}
		}

		xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
	}

}


/* ---------------------------------------------------------- */
/*
02/16/2016 skc - Helper function called once per second to handle book-keeping for  
budgets. Check if we entered a new budget period, or the date has changed to before or
after any budget periods. 
 
Send a report at meter_read_time each day. 
 
Check for potential over budget at midnight each day and send an alert. 
 
We don't want to come here if there are no POCs in the system. 
*/
extern void BUDGET_timer_upkeep( DATE_TIME_COMPLETE_STRUCT pdtcs )
{
	// 8/24/2016 skc : When in the debugger, I occasionally receive very bad data data. This
	// value is an attempt to trap and prevent resetting the budget dates due to a bogus date
	// value.
	static DATE_TIME dt_bug_check = {};

	DATE_TIME dt; // simplify reading

	DATE_TIME dt_budget0; // DATE_TIME at start of budget period

	DATE_TIME dt_budget1; // DATE_TIME at end of budget period

	UNS_32 numsys;

	UNS_32 i; // index

	IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys;

	UNS_32 system_gid;

	BUDGET_DETAILS_STRUCT bds; // system variables, not POC

	UNS_32 bp; // index

	BY_SYSTEM_RECORD *bsr;

	UNS_32 safety_count; // prevent out of control looping

	// ----------

	dt = pdtcs.date_time; // simplify reading

	// 8/24/2016 skc : We will trigger here the first time through, and then two times for each
	// incident that occurs, which is hopefully only during debugging. This function is called
	// at 1Hz, we can endure for two cycles until the date values recover.
	if( (dt_bug_check.D != 0) && (abs(dt.D - dt_bug_check.D) > 365) )
	{
		// 10/12/2016 skc : I see "BUG DATE: 165/165/2165" in the alerts
		Alert_Message_va("caught BUG DATE: %d/%d/%d", pdtcs.__month, pdtcs.__day, pdtcs.__year);
		dt_bug_check = dt; // save value
		return;
	}

	dt_bug_check = dt; // Always save value for the next check

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	// loop all possible mainlines
	numsys = SYSTEM_num_systems_in_use();

	// 3/7/2016 rmd : This loop counts on the fact that the 'OUT OF USE' systems are at the END
	// of the list. The count obtained by the SYSTEM_num_systems_in_use() function identifies
	// how many are in use and that is how many we rattle off of the top of the list.
	for( i = 0; i < numsys; ++i )
	{
		ptr_sys = SYSTEM_get_group_at_this_index( i );

		system_gid = nm_GROUP_get_group_ID( ptr_sys );

		// ----------
		// 
		// 5/20/2016 skc : If the system has no POCs, then we don't want to go any further. The
		// existence of poc_preserves is suspect in this case.
		bsr = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( system_gid );
		if( 0 == bsr->sbf.number_of_pocs_in_this_system )
		{
			continue; // check the next available system
		}

		// ----------

		SYSTEM_get_budget_details( ptr_sys, &bds );

		dt_budget0.D = bds.meter_read_date[0];
		dt_budget0.T = bds.meter_read_time;

		dt_budget1.D = bds.meter_read_date[1];
		dt_budget1.T = bds.meter_read_time;

		// Handle case of dates before budget period start date
		if( DT1_IsBiggerThan_DT2(&dt_budget0, &dt) )
		{
			// 4/22/2016 skc : Rollback the budget data until the budget periods match with the date
			safety_count = 0; // Save us if the time gets set too far back
			while( DT1_IsBiggerThanOrEqualTo_DT2(&dt_budget0, &dt) && (safety_count < 24) )
			{ 

				++safety_count; // bump the safety counter

				// find all the related POCs and reset them
				for( bp=0; bp<MAX_POCS_IN_NETWORK; ++bp )
				{
					// 7/26/2016 skc : Intentionally declared inside loop for const correctness.
					const BY_POC_RECORD * const pbpr = &poc_preserves.poc[ bp ];

					// If the POC isn't enabled, then get next one.
					if( pbpr->poc_gid == 0 )
					{
						continue;
					}

					bsr = pbpr->this_pocs_system_preserves_ptr; 

					// Prevent excess setting, check for special value of UNS_32_MAX
					if( (bsr->system_gid == system_gid) &&
						(bsr->budget.last_rollover_day != UNS_32_MAX) )
					{
						// Set budget report data to zero
						nm_handle_budget_reset_record( bp );

						bsr->budget.last_rollover_day = UNS_32_MAX; // Indicates BEFORE budget period
					}

					if( bsr->system_gid == system_gid )
					{
						// roll back the budget values for the POC
						nm_handle_budget_value_rollback( bp, &bds );
					}
				}
				// roll back the budget period values for the System
				nm_handle_budget_period_rollback( ptr_sys, &bds );

				// ----------

				// update the date values in the test
				dt_budget0.D = bds.meter_read_date[0];
				dt_budget0.T = bds.meter_read_time;
			}

			if( safety_count == 24 ) // This is bad
			{
				Alert_Message("Budget Rollback Check: Too far back");
			}
		}
		// Handle case of in the first budget period
		else if( DT1_IsBiggerThan_DT2(&dt_budget1, &dt))
		{
			for( bp=0; bp<MAX_POCS_IN_NETWORK; ++bp )
			{
				// 7/26/2016 skc : Intentionally declared inside loop for const correctness.
				const BY_POC_RECORD * const pbpr = &poc_preserves.poc[ bp ];

				// If the POC isn't enabled, then get next one.
				if( pbpr->poc_gid == 0 ) 
				{
					continue;
				}

				bsr = pbpr->this_pocs_system_preserves_ptr; 

				// Prevent excess setting, check for special values of 0, UNS_32_MAX
				if( ((bsr->budget.last_rollover_day == 0) || 
					 (bsr->budget.last_rollover_day == UNS_32_MAX)) &&
					(bsr->system_gid == system_gid) )
				{
					// Set budget report data to zero
					nm_handle_budget_reset_record( bp );

					bsr->budget.last_rollover_day = dt.D; // Indicates IN the budget period
				}
			}

			// 9/12/2016 skc : Only send the report if budgets is enabled.
			if( bds.in_use)
			{
				// At budget meter read time, send budget report
				if( dt.T == bds.meter_read_time )
				{
					nm_handle_sending_budget_record( system_gid, &bds, pdtcs );
				}

				// At midnight, check for alerts
				if( dt.T == 0 )
				{
					BUDGET_handle_alerts_at_midnight( system_gid, pdtcs );
				}
			}
		}
		// Then we must be past the end of the first budget period
		else
		{
			// Keep looping until we get a valid budget period,
			// i.e. maybe the controller has been turned off for several months
			safety_count = 0; // Save us if we have really bad date data
			while( DT1_IsBiggerThanOrEqualTo_DT2(&dt, &dt_budget1) && (safety_count < 24) )
			{
				++safety_count; // bump the safety counter

				// Update all the budget_preserves data
				//
				// 6/10/2016 rmd : Pass through the preserves looking for active preserves records (gid !=
				// 0), that are for a POC belonging to the SYSTEM we are interested in. [The ppoc_preserves
				// by its definition holds ALL active POCs in the network, so we will see all the pocs.]
				for( bp=0; bp<MAX_POCS_IN_NETWORK; ++bp )
				{
					// 7/26/2016 skc : Intentionally declared inside loop for const correctness.
					const BY_POC_RECORD * const pbpr = &poc_preserves.poc[ bp ];

					// If the POC isn't enabled, then get next one.
					if( pbpr->poc_gid == 0 ) 
					{
						continue;
					}

					bsr = pbpr->this_pocs_system_preserves_ptr; 

					if( bsr->system_gid == system_gid )
					{
						bsr->budget.rip.closing_record_for_the_period = (true);

						// 11/2/2016 skc : No report if no budgets in use
						if( POC_use_for_budget(bp) && bds.in_use )
						{
							POC_GROUP_STRUCT *group;
							xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );
							group = POC_get_group_at_this_index( POC_get_index_for_group_with_this_GID(pbpr->poc_gid) );
							//debug_printf("bend: (%d) %d %d\n", POC_get_budget( group, 0 ), pbpr->budget.budget, (UNS_32)nm_BUDGET_get_used(bp) );
							Alert_budget_period_ended(nm_GROUP_get_name(group), 
													  POC_get_budget( group, 0 ), 
													  (UNS_32)nm_BUDGET_get_used(bp) );
							xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
						}
					}
				}

				// 3/28/2016 skc : Save the budget record. Only creates a new record if
				// closing_record_for_the_period is true.
				// 9/12/2016 skc : Only send the report if budgets is enabled
				if( bds.in_use)
				{
					nm_handle_sending_budget_record( system_gid, &bds, pdtcs );
				}

				// 4/20/2016 skc : After we send the record, we can clean up
				for( bp=0; bp<MAX_POCS_IN_NETWORK; ++bp )
				{
					// 7/26/2016 skc : Intentionally declared inside loop for const correctness.
					const BY_POC_RECORD * const pbpr = &poc_preserves.poc[ bp ];

					// If the POC isn't enabled, then get next one.
					if( pbpr->poc_gid == 0 )
					{
						continue;
					}

					bsr = poc_preserves.poc[bp].this_pocs_system_preserves_ptr; 

					if( bsr->system_gid == system_gid )
					{
						// Reset the budget report data
						nm_handle_budget_reset_record( bp );

						bsr->budget.last_rollover_day = 0;

						// roll forward the budget values for the POC
						nm_handle_budget_value_updates( bp, &bds );
					}
				}

				// roll forward the budget period values for the System
				nm_handle_budget_period_updates( ptr_sys, &bds );

				// ----------

				// reset for next comparison
				dt_budget1.D = bds.meter_read_date[1];
				dt_budget1.T = bds.meter_read_time;
			}

			if( safety_count == 24 ) // This is bad
			{
				Alert_Message("Budget Rollover Check: BAD DATE DATA");
			}

		}			
	} // end for( sys )

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

// 4/11/2016 skc : Returns the gallons used by a POC. Depending on settings, different types
// of usage may or may not be included in the total. Note the inverse logic of the flag.
//
// Mutex : requires the poc_preserves_recursive_MUTEX
extern double nm_BUDGET_get_used( UNS_32 idx_poc_preserves )
{
	double rv;

	IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys;

	const BY_POC_RECORD * const pbpr = &poc_preserves.poc[ idx_poc_preserves ];

	// ----------

	rv = 0;

	ptr_sys = SYSTEM_get_group_with_this_GID( pbpr->this_pocs_system_preserves_ptr->system_gid );

	////////// 
	// 4/19/2016 skc : If the flow type is true, then add it to the sum.
	if( nm_SYSTEM_get_budget_flow_type( ptr_sys, IN_LIST_FOR_PROGRAMMED_IRRIGATION ) )
	{
		rv += pbpr->budget.used_programmed_gallons;
	}
	if( nm_SYSTEM_get_budget_flow_type( ptr_sys, IN_LIST_FOR_MANUAL_PROGRAM ) )
	{
		rv += pbpr->budget.used_manual_programmed_gallons;
	}
	if( nm_SYSTEM_get_budget_flow_type( ptr_sys, IN_LIST_FOR_MANUAL ) )
	{
		rv += pbpr->budget.used_manual_gallons;
	}
	if( nm_SYSTEM_get_budget_flow_type( ptr_sys, IN_LIST_FOR_WALK_THRU ) )
	{
		rv += pbpr->budget.used_walkthru_gallons;
	}
	if( nm_SYSTEM_get_budget_flow_type( ptr_sys, IN_LIST_FOR_TEST ) )
	{
		rv += pbpr->budget.used_test_gallons;
	}
	if( nm_SYSTEM_get_budget_flow_type( ptr_sys, IN_LIST_FOR_MOBILE ) )
	{
		rv += pbpr->budget.used_mobile_gallons;
	}
	if( nm_SYSTEM_get_budget_flow_type( ptr_sys, FLOW_TYPE_MVOR) )
	{
		rv += pbpr->budget.used_mvor_gallons;
	}
	if( nm_SYSTEM_get_budget_flow_type( ptr_sys, FLOW_TYPE_NON_CONTROLLER) )
	{
		rv += pbpr->budget.used_idle_gallons;
	}
 
	return rv;
}

// 4/26/2016 skc : Initialize budget dates, set budget values and report data to 0
// Task : called from key_processing task
//
// month, year - the month and year to start the meter read dates with
// delta - monthly difference between budget periods, 1 is for 12 annual periods,
// 2 is for 6 annual periods
extern void BUDGET_reset_budget_values( IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys, UNS_32 month, UNS_32 year, UNS_32 delta )
{
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );
 
	nm_reset_meter_read_dates( ptr_sys, month, year, delta );
	nm_reset_budget_values(ptr_sys );

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Calculates square footage of each station in a mainline with a default square footage
 * value.
 * 
 * @executed Executed within the context of the budgets task in the process groups function
 * of the budget setup screen when the Calculate square footage button is pressed.
 * 
 * @param system_index, used to identify the mainline in use for the budget
 * 
 * @return void
 *
 * @author 8/22/2018 Ryan
 *
 * @revisions
 *    8/22/2018 Initial release
 */

extern BOOL_32 BUDGET_calculate_square_footage( const UNS_32 system_index )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT 	*lsystem;

	STATION_STRUCT					*lstation;
		
	float							square_footage;
	
	STATION_STRUCT_for_budgets 		budget_data;
	
	BOOL_32							square_footage_calculation_occured;

	// ----------
	
	square_footage_calculation_occured = false;
	
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lsystem = SYSTEM_get_group_at_this_index( system_index );

	lstation = nm_ListGetFirst( &station_info_list_hdr );

	// 8/21/2018 Ryan : For each station, if it is assigned to the system at this index, and has
	// the default square footage value, then calculate it's square footage.
	while( lstation != NULL )
	{
		// 8/22/2018 Ryan : if the station has no station group
		// STATION_GROUP_get_GID_irrigation_system_for_this_station returns the first mainline.
		// added additional check so these stations are not included in the square footage
		// calculations.
		if( ( SYSTEM_get_index_for_group_with_this_GID( STATION_GROUP_get_GID_irrigation_system_for_this_station( lstation ) ) == system_index ) && ( STATION_get_GID_station_group( lstation ) > 0 ) )
		{
			if( STATION_get_square_footage( lstation ) == STATION_SQUARE_FOOTAGE_DEFAULT )
			{
				nm_STATION_get_Budget_data( lstation, &budget_data );
				
				// 11/5/2018 Ryan : Equation to calculate the square footage = constant to convert gpm into
				// in per hour / min per hour * expected flow rate / percip rate. The percip_100000u
				// constant is to correct the units of precip rate to in/hour.
				square_footage = ( ( ( SQUARE_FOOTAGE_CONSTANT / MINUTES_PER_HOUR ) * (float)budget_data.expected_flow_rate_gpm * percip_100000u ) / budget_data.precip_rate );
				
				nm_STATION_set_square_footage( lstation, __round_float( square_footage, 0 ) , CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, budget_data.box_index_0, CHANGE_set_change_bits, STATION_get_change_bits_ptr( lstation, CHANGE_REASON_KEYPAD ) );
				
				// 11/1/2018 Ryan : set flag so we know if any stations square footages were calculated.
				square_footage_calculation_occured = true;
			}
		}
		
		lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	
	return( square_footage_calculation_occured );
}

/* ---------------------------------------------------------- */
/**
 * Calculates square footage of each station in a mainline with a default square footage
 * value.
 * 
 * @executed Executed within the context of the budgets task when called by the
 * BUDGET_calculate_gallons_per_inch_for_square_footage_of_mainline function to find the
 * total square footage of the system.
 * 
 * @param system_index, used to identify the mainline in use for the budget
 * 
 * @return float square_footage - total square footage of all station assigned to system.
 *
 * @author 8/22/2018 Ryan
 *
 * @revisions
 *    8/22/2018 Initial release
 */

static float BUDGET_total_square_footage( const UNS_32 system_index )
{
	IRRIGATION_SYSTEM_GROUP_STRUCT 	*lsystem;

	STATION_STRUCT					*lstation;
	
	float							square_footage;

	// ----------

	square_footage = 0;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	lsystem = SYSTEM_get_group_at_this_index( system_index );

	lstation = nm_ListGetFirst( &station_info_list_hdr );

	// 8/27/2018 Ryan : For each station, if it is assigned to the system at this index add it's
	// square footage to the total.
	while( lstation != NULL )
	{
		// 8/27/2018 Ryan : if the station has no station group
		// STATION_GROUP_get_GID_irrigation_system_for_this_station returns the first mainline.
		// added additional check so these stations are not included in the total square footage.
		if( ( SYSTEM_get_index_for_group_with_this_GID( STATION_GROUP_get_GID_irrigation_system_for_this_station( lstation ) ) == system_index ) && ( STATION_get_GID_station_group( lstation ) > 0 ) )
		{
			square_footage += STATION_get_square_footage( lstation );
		}
	
		lstation = nm_ListGetNext( &station_info_list_hdr, lstation );
	}

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	return( square_footage );
}

/* ---------------------------------------------------------- */
/**
 * Calculates the gallons per inch for square footage of mainline for use in making a annual
 * budget for the ET entry mode
 * 
 * @executed Executed within the context of the budgets takes by the
 * BUDGET_calc_POC_monthly_budget_using_et function to find the gallons per inch for square
 * footage for the mainline
 * 
 * @param system_index, used to identify the mainline in use when getting the total square
 * footage.
 * 
 * @return float lgallons_per_inch_for_square_footage - gallons per inch for square footage
 * for the system with the matching system_index.
 *
 * @author 8/22/2018 Ryan
 *
 * @revisions
 *    8/22/2018 Initial release
 */

static float BUDGET_calculate_gallons_per_inch_for_square_footage_of_mainline( const UNS_32 system_index )
{
	
	UNS_32							ltotal_square_footage;

	float							lgallons_per_inch_for_square_footage;
	
	const volatile float			CONVERSION_RATE_GALLONS_PER_ACRE = 27154.25;

	const volatile float			CONVERSION_RATE_SQUARE_FEET_PER_ACRE = 43560.0;

	// ----------

	ltotal_square_footage = BUDGET_total_square_footage( system_index );

	lgallons_per_inch_for_square_footage = (float)ltotal_square_footage * CONVERSION_RATE_GALLONS_PER_ACRE / CONVERSION_RATE_SQUARE_FEET_PER_ACRE;

	return( lgallons_per_inch_for_square_footage );
}

/* ---------------------------------------------------------- */
/**
 * Calculate the monthly budgets for every POC in a system based on percent_ET and POC
 * ratio.
 * 
 * @executed Executed within the context of of the budgets task when leaving the budget
 * setup screen when the historical ET function is chosen, also called at midnight to update
 * the budget using updated POC usages.
 * 
 * @param	psystem, system group struct for this budget.
 *
 * @param	percent_et_changed, let the function know if it is being called because the
 *  percent et has changed.
 * 
 * @return void
 *
 * @author 9/6/2018 Ryan
 *
 * @revisions
 *    9/6/2018 Initial release
 */

extern void BUDGET_calc_POC_monthly_budget_using_et( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem, BOOL_32 percent_et_changed )
{
	UNS_32 idx_poc;

	UNS_32 gid_sys;

	UNS_32 gid_poc;

	float lgallons_per_inch_for_square_footage;

	float lbudget_per_month[ MONTHS_IN_A_YEAR ];
	
	BUDGET_DETAILS_STRUCT lbds_ptr;
	
	DATE_TIME_COMPLETE_STRUCT	ldtcs_ptr;
		
	POC_GROUP_STRUCT	*lpoc;
	
	BOOL_32	monthy_budget_for_system_calculated;
	
	UNS_32	i;
	
	UNS_32	poc_count;
	
	UNS_32 poc_usage[ MAX_POCS_IN_NETWORK ]; // useage for a particular POC
	
	UNS_32 sys_usage;
	
	float poc_ratio;
	
	float new_calculated_budget;
	
	const volatile float			ONE_HUNDRED = 100.0f;
	
	// ----------
	
	// 9/27/2018 Ryan : Constant used in checking if new budget is signifigantly different than
	// current budget.
	#define	budget_percent_difference_min	(4)
	
	// ----------
	
	// 11/1/2018 Ryan : Set default as false, just in case budget is not calculated.
	monthy_budget_for_system_calculated = (false);

	// 11/1/2018 Ryan : get the system usage for all of the POCs in the mainline.
	memset( poc_usage, 0, sizeof(poc_usage) );

	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	gid_sys = nm_GROUP_get_group_ID(psystem);
	
	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );
	
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );
	
	sys_usage = nm_BUDGET_get_poc_usage( gid_sys, poc_usage );
	
	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
	
	// 11/1/2018 Ryan : get budget details for mainline.
	SYSTEM_get_budget_details(psystem, &lbds_ptr );
	
	poc_count = 0;
	
	// ----------
	
	// 9/11/2018 Ryan : Get number of POCs in budget.
	for( idx_poc = 0; idx_poc < MAX_POCS_IN_NETWORK; ++idx_poc )
	{
		gid_poc = poc_preserves.poc[idx_poc].poc_gid;

		// 9/11/2018 Ryan : Validate the gid's and POC usage
		if( (0 != gid_poc) &&
			(gid_sys == poc_preserves.poc[idx_poc].this_pocs_system_preserves_ptr->system_gid)  &&
			POC_use_for_budget( idx_poc ) )
		{
			poc_count++;
		}
	}

	// 9/6/2018 Ryan : Go through every POC in network to see if they are in this system.
	for( idx_poc = 0; idx_poc < MAX_POCS_IN_NETWORK; ++idx_poc )
	{
		gid_poc = poc_preserves.poc[idx_poc].poc_gid;
		lpoc = POC_get_group_at_this_index( idx_poc );

		// 9/6/2018 Ryan : Filter out POC with no gid, don't match the system
		if( (0 == gid_poc) || (gid_sys != poc_preserves.poc[idx_poc].this_pocs_system_preserves_ptr->system_gid) )
		{
			continue;
		}

		// 9/6/2018 Ryan : Filter out POC not used for budgets
		if( !POC_use_for_budget( idx_poc ) )
		{
			continue;
		}

		// 9/7/2018 Ryan : For one of the POCs, use the stored budget percent ET to calculate the
		// lgallons_per_inch_for_square_footage and the lbudget_per_month
		if( monthy_budget_for_system_calculated == (false) )
		{
			lgallons_per_inch_for_square_footage = BUDGET_calculate_gallons_per_inch_for_square_footage_of_mainline( SYSTEM_get_index_for_group_with_this_GID( gid_sys ) );
	
			for( i = 1; i <= MONTHS_IN_A_YEAR; i++ )
			{
				lbudget_per_month[ i - 1 ] = lgallons_per_inch_for_square_footage * ET_DATA_et_number_for_the_month( i ) * (float)POC_get_budget_percent_ET( lpoc ) / ONE_HUNDRED;
			}
			
			monthy_budget_for_system_calculated = (true);
		}
		
		// 9/11/2018 Ryan : find the ratio for this POC
		if( 1 == poc_count )
		{
			poc_ratio = 1; // By definition, it's one-to-one
		}
		else if( poc_count > 1 )// multiple POCs
		{
			if( sys_usage == 0 ) // The system has seen no flow. Either never ran, or no flow meters.
			{
				// 9/13/2018 Ryan : Systems with no flow meters divide evenly. If we really have yet to
				// measure flow, then also divide evenly until next time when we will have measured some.
				poc_ratio = 1.f / (float)poc_count;
			}
			else
			{
				if ( poc_usage[idx_poc] == 0 ) // No flow measured on the POC
				{
					poc_ratio = 0; // got a better idea?
				}
				else // get this POC percentage of the system flow
				{
					poc_ratio = (float)poc_usage[idx_poc] / (float)sys_usage;
				}
			}
		}
		
		// 9/6/2018 Ryan : For each month, change budget depending on current POC ratio.
		for( i = 0; i < MONTHS_IN_A_YEAR; i++ )
		{
			DateAndTimeToDTCS( lbds_ptr.meter_read_date[i], 0, &ldtcs_ptr );
			
			// 9/6/2018 Ryan : 4% fudge factor used in other places to make sure the budget in not
			// exceeded.
			new_calculated_budget = ( 0.96 * lbudget_per_month[ ldtcs_ptr.__month - 1 ] ) * poc_ratio;
			
			// 9/25/2018 Ryan : Only change the budget if change is at least a 4% different, or if
			// percent et is changed.
			if( ( ( abs( new_calculated_budget - POC_get_budget( lpoc, i ) ) * ONE_HUNDRED ) / POC_get_budget( lpoc, i ) > budget_percent_difference_min ) || ( percent_et_changed  == (true) ) )
			{
				POC_set_budget( lpoc, i, new_calculated_budget );
			}
		}	
	}
}
