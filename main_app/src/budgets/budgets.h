/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef BUDGETS_H
#define BUDGETS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"
#include	"epson_rx_8025sa.h"
#include	"irrigation_system.h"
#include	"lpc_types.h"
#include	"poc.h"
#include	"stations.h"



#define		BUDGET_et_changed				(true)
#define		BUDGET_et_not_changed			(false)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/14/2018 Ryan : External variable used for budgets with et entry option selected.
extern BOOL_32 g_BUDGET_using_et_calculate_budget;

// Return station runtime based on budget constraints
extern float nm_BUDGET_calc_time_adjustment( float t_orig, STATION_STRUCT *pss_ptr, DATE_TIME dt );

// Precalculate data needed to compute time adjustment
extern void BUDGET_calculate_ratios( const DATE_TIME_COMPLETE_STRUCT *const ptoday_ptr, BOOL_32 is_start_time );

// Return the predicted volume for a particular system
extern float nm_BUDGET_predicted_volume( UNS_32 system_gid, 
										 BUDGET_DETAILS_STRUCT *const bds, 
										 const DATE_TIME_COMPLETE_STRUCT *const ptoday_ptr,
										 const DATE_TIME right_now,
										 BOOL_32 use_limits );

// Return the relative usage ratios for POCs on a system
extern void nm_BUDGET_calc_rpoc( UNS_32 gid_sys, float rpoc[] );

// Returns true if any system is under budget restrictions
extern UNS_32 BUDGET_in_effect_for_any_system();

// Test if budget is over or will be over limit
extern void BUDGET_handle_alerts_at_midnight( UNS_32 system_gid, DATE_TIME_COMPLETE_STRUCT today );

extern void BUDGET_handle_alerts_at_start_time( UNS_32 system_gid, STATION_GROUP_STRUCT *psg );
// 
// Called by TD_Check to allow budgets to take care of date changes and alerts
extern void BUDGET_timer_upkeep( DATE_TIME_COMPLETE_STRUCT tdcqs );

// 4/11/2016 skc : Return the used_irrigation_gallons plus (optionally) the
// used_mvor_gallons and used_idle_gallons values
extern double nm_BUDGET_get_used( UNS_32 idx_poc_preserves );

// 4/26/2016 skc : Initialize budget dates, set budget values and report data to 0
extern void BUDGET_reset_budget_values( IRRIGATION_SYSTEM_GROUP_STRUCT *ptr_sys, UNS_32 month, UNS_32 year, UNS_32 delta );

// 8/21/2018 Ryan : Calculate square footage for all default value station in a mainline.
extern BOOL_32 BUDGET_calculate_square_footage( const UNS_32 system_index );

// 9/7/2018 Ryan : Used when budgets calculated using ET need to be update either
// because of ET percentage change or possible POC ration differences.
extern void BUDGET_calc_POC_monthly_budget_using_et( IRRIGATION_SYSTEM_GROUP_STRUCT *psystem, BOOL_32 percent_et_changed );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif
 
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

