/* file = tdcheck.c                           2011.05.26  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"td_check.h"

#include    "alerts.h"

#include    "app_startup.h"

#include	"budgets.h"

#include    "comm_mngr.h"

#include    "configuration_network.h"

#include    "dialog.h"

#include	"d_comm_options.h"

#include	"d_device_exchange.h"

#include	"d_two_wire_debug.h"

#include	"d_two_wire_discovery.h"

#include    "e_about.h"

#include	"e_comm_test.h"

#include	"e_flowsense.h"

#include    "e_stations.h"

#include    "e_status.h"

#include    "e_test_sequential.h"

#include    "e_lights.h"

#include    "e_test_lights.h"

#include    "e_time_and_date.h"

#include    "epson_rx_8025sa.h"

#include    "foal_comm.h"

#include    "foal_flow.h"

#include    "foal_irri.h"

#include    "foal_lights.h"

#include    "gpio_setup.h"

#include    "irri_comm.h"

#include    "irri_irri.h"

#include    "irri_lights.h"

#include	"lpc3250_chip.h"

#include	"lpc32xx_gpio.h"

#include    "m_main.h"

#include	"r_budgets.h"

#include    "r_decoder_stats.h"

#include    "r_et_rain_table.h"

#include    "r_flow_checking.h"

#include    "r_flow_recording.h"

#include    "r_flow_table.h"

#include    "r_flowsense_stats.h"

#include    "r_irri_details.h"

#include    "r_memory_use.h"

#include    "r_poc_preserves.h"

#include    "r_poc_report.h"

#include    "r_lights.h"

#include	"r_rt_bypass_flow.h"

#include	"r_rt_communications.h"

#include	"r_rt_electrical.h"

#include	"r_rt_poc_flow.h"

#include	"r_rt_system_flow.h"

#include	"r_rt_weather.h"

#include	"r_rt_lights.h"

#include    "r_serial_port_info.h"

#include    "r_station_history.h"

#include    "r_station_report.h"

#include    "r_system_preserves.h"

#include    "r_system_report.h"

#include    "r_tpmicro_debug.h"

#include    "scrollbox.h"

#include    "wdt_and_powerfail.h"

#include    "weather_control.h"

#include	"controller_initiated.h"

#include	"code_distribution_task.h"

#include	"flowsense.h"

#include	"d_process.h"

#include	"bypass_operation.h"

#include	"watersense.h"

#include	"e_mvor.h"

#include	"e_moisture_sensors.h"

#include	"radio_test.h"

#include	"e_ftimes.h"

#include	"ftimes_task.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void timer_250ms_callback( xTimerHandle pxTimer )
{
	// This is NOT an interrupt. Executed within the context of the Timer Task. This function
	// will be called once each 250ms. Given nothing of a higher priority needs to be done. But
	// the Timer Task is a VERY high priority.

	DISPLAY_EVENT_STRUCT	lde;

	switch( GuiLib_CurStructureNdx )
	{
		case GuiStruct_dlgDownloadingFirmware_0:
			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = &FDTO_CODE_DOWNLOAD_update_dialog;
			Display_Post_Command( &lde );
			break;

		case GuiStruct_scrFLOWSENSE_0:
			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = (void*)&FDTO_FLOWSENSE_update_screen;
			Display_Post_Command( &lde );
			break;

		case GuiStruct_scrCommTest_0:
			// 12/22/2014 ajv : Since the GuiVar_CommTestStatus screen is updated asynchronously to the
			// all we need to do is refresh the auto-redraw fields for the screen to update.
			Refresh_Screen();
			break;

		case GuiStruct_rptSerialPortActivity_0:
			lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
			lde._04_func_ptr = (void*)&FDTO_SERIAL_PORT_INFO_draw_report;
			lde._06_u32_argument1 = (false);
			Display_Post_Command( &lde );
			break;

		case GuiStruct_rptFLOWSENSEStats_0:
			lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
			lde._04_func_ptr = (void*)&FDTO_FLOWSENSE_STATS_draw_report;
			lde._06_u32_argument1 = (false);
			Display_Post_Command( &lde );
			break;

		case GuiStruct_rptMemoryUsage_0:
			lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
			lde._04_func_ptr = (void*)&FDTO_MEM_draw_report;
			lde._06_u32_argument1 = (false);
			Display_Post_Command( &lde );
			break;

		case GuiStruct_rptFlowTable_0:
			lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
			lde._04_func_ptr = (void*)&FDTO_FLOW_TABLE_redraw_scrollbox;
			lde._06_u32_argument1 = (false);
			Display_Post_Command( &lde );
			break;

		case GuiStruct_rptFlowCheckingStats_0:
			lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
			lde._04_func_ptr = (void*)&FDTO_FLOW_CHECKING_STATS_draw_report;
			lde._06_u32_argument1 = (false);
			Display_Post_Command( &lde );
			break;
			
		#if 0
		NOT CURRENTLY USED
		case GuiStruct_rptSystemPreserves_0:
			lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
			lde._04_func_ptr = (void*)&FDTO_SYSTEM_PRESERVES_draw_report;
			lde._06_u32_argument1 = (false);
			Display_Post_Command( &lde );
			break;

		case GuiStruct_rptPOCPreserves_0:
			lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
			lde._04_func_ptr = (void*)&FDTO_POC_PRESERVES_draw_report;
			lde._06_u32_argument1 = (false);
			Display_Post_Command( &lde );
			break;
		#endif

	}
}

/* ---------------------------------------------------------- */
extern void TD_CHECK_task( void *pvParameters )
{
	// A task that receives queue messages with the latest time & date from the EPSON rtc task. This task also creates
	// and runs a 250ms timer used to drive particular diagnostic screen updates.

	TD_CHECK_QUEUE_STRUCT	tdcqs;

	BYPASS_EVENT_QUEUE_STRUCT	beqs;
	
	xTimerHandle			timer_250_handle;

	UNS_32					lgid, s, mlb_during_when, mlb_measured, mlb_limit;
	
	BY_SYSTEM_RECORD		*bsr_ptr;

	BOOL_32					lnetwork_available;

	// 5/15/2015 rmd : For the GCC floating point optimization bug. Where it produces garbage
	// results.
	const volatile float ONE_HUNDRED_POINT_O = 100.0;
	
	// ----------
	
	// 10/23/2012 rmd : To support the WDT activity monitor.
	UNS_32		task_index;
	
	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// ----------

	if( (timer_250_handle = xTimerCreate( (const signed char*)"250ms timer", MS_to_TICKS(250), TRUE, NULL, timer_250ms_callback )) == NULL )
	{
		Alert_Message( "Timer 250ms NOT CREATED!" );
	}
	else
	{
		// Start the 250ms timer.
		//
		// 3/14/2014 rmd : Being that the timer task is a high priority task using portMAX_DELAY
		// when posting is safe. Anything on the timer queue is quickly processed and therefore the
		// queue is effectively always empty.
		xTimerStart( timer_250_handle, portMAX_DELAY );
	}
	
	while( (true) )
	{
		// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
		if ( xQueueReceive( TD_CHECK_queue, &tdcqs, MS_to_TICKS( 500 ) ) == pdTRUE ) 
		{
			switch( tdcqs.command )
			{
				case TDQ_COMMAND_its_a_new_second:

					// ------------

					// 5/6/2014 rmd : Evaluate for subsequent use.

					GuiVar_LiveScreensCommFLEnabled = FLOWSENSE_we_are_poafs();

					GuiVar_LiveScreensCommFLInForced = comm_mngr.chain_is_down;

					// 8/20/2015 ajv : Note that this GuiVar IS NOT NetworkAvailable. It's actually COMM_MNGR
					// mode! Therefore, DO NOT USE IT!!! It is only to be used for updating the network icon on
					// the screen.
					GuiVar_NetworkAvailable = comm_mngr.mode;

					// ----------

					// 8/20/2015 ajv : Grab a local copy of whether the network is available since it's used in
					// mutliple places below.
					lnetwork_available = COMM_MNGR_network_is_available_for_normal_use();
	
					// ------------
	
					if( tdcqs.dtcs.date_time.T == 0 )
					{
						// 10/24/2013 rmd : This is where we can add the MIDNIGHT activites.

						// ----------
						
						// 9/8/2016 rmd : Set the flag to clear the accumulated error count. The count is cleared at
						// the generation of the next token with the chain up.
						comm_mngr.we_have_crossed_midnight_so_clear_the_error_counts = (true);
						
						// ----------

						if( !lnetwork_available )
						{
							STATION_chain_down_NOW_days_midnight_maintenance();
						}

						// ----------

						// 7/10/2014 rmd : Signal the BYPASS operation task to clear its invalid configuration alert
						// flag. So that invalid bypass setup generates one alert at midnight.
						beqs.event = BYPASS_EVENT_midnight;
						
						if( xQueueSendToBack( BYPASS_event_queue, &beqs, 0 ) != pdPASS )
						{
							ALERT_MESSAGE_WITH_FILE_NAME( "BYPASS queue full" );
						}

						// ----------
						
						// 6/4/2015 rmd : Its midnight. Deal with the xmit and rcvd byte totals.
						SERIAL_at_midnight_manage_xmit_and_rcvd_accumulators_and_make_alert_lines( &tdcqs.dtcs );
						
						// ----------
						
						#if SHOW_STUCK_KEY_ALERTS_AT_MIDNIGHT
						
							UNS_32	kkk;
							
							for( kkk=0; kkk<NUMBER_OF_KEYS; kkk++ )
							{
								if( stuck_key_data[ kkk ].recorded_repeats >= STUCK_KEY_REPEAT_COUNT_THRESHOLD )
								{
									Alert_Message_va( "Stuck Key: %s, %u repeats", stuck_key_data[ kkk ].key_name_string, stuck_key_data[ kkk ].recorded_repeats );
								}

								// 9/10/2018 rmd : Always clear each count at midnight to see if it happens again the next
								// day.
								stuck_key_data[ kkk ].recorded_repeats = 0;
							}
			
						#endif
						
					}  // of it is mid-night
					 
					// ----------
					
					// 6/24/2015 rmd : Bump the mobile session count.
					CONTROLLER_INITIATED_post_event( CI_EVENT_bump_mobile_session_seconds_count );
					
					// ----------
					
					// 2/9/2015 rmd : Is it 12:30am?
					if( tdcqs.dtcs.date_time.T == (30*60) )
					{
						// 2/9/2015 rmd : There were cases when only the report data was sent to the comm_server for
						// whole 24hour periods. And when looking at alerts you would not see any recent alert
						// lines. Cause there was no events that caused alerts to be sent. It made it look like
						// welll maybe something wasn't working correctly when viewing alerts. This causes us to at
						// least send once per day. And will show the transmission of the report data records at a
						// minimum. Set to force sending in 15 seconds (REGARDLESS if alerts timer already started
						// or not).
						CONTROLLER_INITIATED_alerts_timer_upkeep( CONTROLLER_INITIATED_ms_after_midnight_to_send_this_controllers_daily_records( config_c.serial_number ), (true) );
					}

					// ----------
					
					// 6/11/2015 rmd : Everyday at 3PM check if there is a code update available for us from the
					// Comm_server. This used to only take place if the network was up but I moved it out to
					// here. So regardless of network status (chain down or not) check for updates. After all it
					// may be the chain is down for a bug that the update could fix. If this controller has an
					// internet connected device or not is a consideration dealt with within the CI task itself.
					//
					// 4/7/2017 rmd : Changed to 4:15PM. Had requests to move later in the day. 3PM still
					// confilicted with some user activity. And didn't want to conflict with the 4PM time & date
					// update the commserver sends.
					if( tdcqs.dtcs.date_time.T == ((16*60*60)+(15*60)) )
					{
						// 4/12/2017 rmd : If this controller is on a HUB it is not allowed to make this check for
						// updates check. Only the hub makes this test. Controllers on a hub have their code version
						// checked when pdata is exchanged, either direction, and that is tracked by the commserver
						// to know the hub must be updated (perhaps), and direct the hub to do a code distribution.
						if( !CONFIG_this_controller_is_behind_a_hub() )
						{
							CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_check_for_updates, NULL, CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD, CI_POST_TO_BACK );
						}
					}
						
					// ----------
					
					// 4/30/2014 rmd : Only if the comm_mngr is doing as it normally does, that is making
					// tokens, and the chain isn't down.
					if( lnetwork_available )
					{
						// 7/9/2014 rmd : TAKE CARE OF THE PRESERVES SYNC FIRST.
						
						// 2012.01.20 rmd : We call the poc_preserves and system_preserves file synchonization
						// functionc once per second BEFORE we call all the flow and irrigation functions. Those
						// activities rely upon each SYSTEM and POC having a corresponding item in their respective
						// preserves array.
						SYSTEM_PRESERVES_synchronize_preserves_to_file();

						// 7/18/2012 rmd : Must come AFTER the system_preserves synchronization. As the
						// poc_preserves contains a pointer to the system_preserve record the poc belongs to.
						POC_PRESERVES_synchronize_preserves_to_file();

						// 7/9/2014 rmd : NOTE - the STATION PRESERVES are not sync'd. There is a permanent fixed
						// array indexed into using the station number and box_index.
						
						// ----------

						// 7/10/2014 rmd : Signal the BYPASS operation task that another second has gone by.
						beqs.event = BYPASS_EVENT_one_hertz_execution;
						
						if( xQueueSendToBack( BYPASS_event_queue, &beqs, 0 ) != pdPASS )
						{
							ALERT_MESSAGE_WITH_FILE_NAME( "BYPASS queue full" );
						}
						
						// ----------

						// 4/30/2014 rmd : If we are a master do these things.
						if( FLOWSENSE_we_are_a_master_one_way_or_another() )
						{
							if( tdcqs.dtcs.date_time.T == 0 )
							{
								// 4/29/2015 rmd : At the master make MLB reminder alerts at mid-night. As we are about to
								// peruse through the system_preserves take the associated MUTEX.
								xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
								
								// 4/29/2015 rmd : Because we call the nm_GROUP_get_name function take this MUTEX,
								xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );
								
								for( s=0; s<SYSTEM_num_systems_in_use(); s++ )
								{
									// 4/29/2015 rmd : The functions have ZERO and NULL protection built in. So we can use
									// functions as parameters safely.
									lgid = nm_GROUP_get_group_ID( SYSTEM_get_group_at_this_index( s ) );
							
									if( lgid == 0 )
									{
										// 4/11/2012 rmd : This should never happen. Abandon loop if so.
										ALERT_MESSAGE_WITH_FILE_NAME( "SYSTEM_PRESERVES : unexp NULL gid" );
							
										break;
									}
							
									bsr_ptr = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( lgid );
									
									// 4/29/2015 rmd : Make sure not NULL!
									if( bsr_ptr != NULL )
									{
										// 4/29/2015 rmd : Test for a MLB using the FOAL side MLB record since this code is nested
										// inside the section for a MASTER controller.
										if( SYSTEM_PRESERVES_there_is_a_mlb( &(bsr_ptr->latest_mlb_record) ) )
										{
											if( bsr_ptr->latest_mlb_record.there_was_a_MLB_during_irrigation )
											{
												mlb_during_when = MLB_TYPE_DURING_IRRIGATION;
												
												mlb_measured = bsr_ptr->latest_mlb_record.mlb_measured_during_irrigation_gpm;

												mlb_limit = bsr_ptr->latest_mlb_record.mlb_limit_during_irrigation_gpm;
											}
											else
											if( bsr_ptr->latest_mlb_record.there_was_a_MLB_during_mvor_closed )
											{
												mlb_during_when = MLB_TYPE_DURING_MVOR;
												
												mlb_measured = bsr_ptr->latest_mlb_record.mlb_measured_during_mvor_closed_gpm;

												mlb_limit = bsr_ptr->latest_mlb_record.mlb_limit_during_mvor_closed_gpm;
											}
											else
											{
												mlb_during_when = MLB_TYPE_DURING_ALL_OTHER_TIMES;
												
												mlb_measured = bsr_ptr->latest_mlb_record.mlb_measured_during_all_other_times_gpm;

												mlb_limit = bsr_ptr->latest_mlb_record.mlb_limit_during_all_other_times_gpm;
											}
											
											Alert_mainline_break( "REMINDER", nm_GROUP_get_name( SYSTEM_get_group_at_this_index( s ) ), mlb_during_when, mlb_measured, mlb_limit );
										}
									}
								}
								
								xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

								xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
								
								// ----------
								
								// 4/27/2015 rmd : Make an alert about the midnight to midnight rain. This code is inside
								// the if for a master because he is the one who sees all the pulses as they come in the
								// token responses. Also only show the alert if the user wants to use a bucket.
								if( WEATHER_get_rain_bucket_is_in_use() == (true) )
								{
									// 4/27/2015 rmd : Only alert if we have some rain.
									if( weather_preserves.rain.midnight_to_midnight_raw_pulse_count != 0 )
									{
										// 5/15/2015 rmd : FLOATING POINT CALC DOESN'T WORK IN RELEASE CODE! WORKS FOR DEBUG.
										//Alert_24HourRainTotal( (weather_preserves.rain.midnight_to_midnight_raw_pulse_count / 100.0) );

										// 5/15/2015 rmd : FLOATING POINT CALC DOESN'T WORK IN RELEASE CODE! WORKS FOR DEBUG.
										//Alert_24HourRainTotal( (float)(weather_preserves.rain.midnight_to_midnight_raw_pulse_count / 100.0) );
										
										// 5/15/2015 rmd : FLOATING POINT CALC WORKS IN BOTH RELEASE AND DEBUG.
										Alert_24HourRainTotal( (weather_preserves.rain.midnight_to_midnight_raw_pulse_count / ONE_HUNDRED_POINT_O) );
									}
								}
								
								// 4/27/2015 rmd : Regardless of if we are using or not zero the count.
								weather_preserves.rain.midnight_to_midnight_raw_pulse_count = 0;

								// ----------

								// 7/14/2015 ajv : Also stamp an alert for any Station Groups that are not WaterSense
								// complaint each day at midnight.
								WATERSENSE_verify_watersense_compliance();
							}
							
							// ----------
							
							WEATHER_TABLES_roll_et_rain_tables( &(tdcqs.dtcs.date_time) );
							
							if( tdcqs.dtcs.date_time.T == 0 )
							{
								// 10/28/2015 rmd : After the tables rolled (at 8pm) at midnight attempt to send the et and
								// rain tables. We are not using a timer so this is a direct attempt at midnight to send the
								// needed table entries. If the attempt fails there are no retries. We try again the next
								// night.
								CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_et_rain_tables, NULL, CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD, CI_POST_TO_BACK );
							}
							
							// ----------

							// 6/16/2015 rmd : At the table roll time if there is a gage or bucket in use send our
							// weather data to the commserver.
							if( tdcqs.dtcs.date_time.T == WEATHER_get_et_and_rain_table_roll_time() )
							{
								if( (WEATHER_get_et_gage_is_in_use()) || (WEATHER_get_rain_bucket_is_in_use()) )
								{
									CONTROLLER_INITIATED_post_event( CI_EVENT_start_send_weather_data_timer );
								}
							}
							
							// ----------

							// 01/26/2016 skc : Let's check if we are starting a new budget period
							// If so, then pop the old budget data off the top
							// Also, reset the budget_preserves
							// 5/12/2016 skc : If there are no POCs in the system, things get ugly with
							// invalid/unitialized preserves and POC variables. In this case, we do not want
							// to do anything with budgets.
							BUDGET_timer_upkeep( tdcqs.dtcs );

							// ----------

							// 5/29/2014 rmd : FIRST build up the poc flow rates. Calculating actual 5 second flow rates
							// at each terminal or decoder within each poc.
							FOAL_FLOW__A__build_poc_flow_rates();
							
							// 5/29/2014 rmd : SECOND using the individual poc flow rates, build up the system averages
							// and the stability averages.
							FOAL_FLOW__B__build_system_flow_rates();
	
							// 4/30/2014 rmd : And only if the chain is not down.
							REPORT_DATA_maintain_all_accumulators( &(tdcqs.dtcs.date_time) );
							
							// ----------
							
							TDCHECK_at_1Hz_rate_check_for_irrigation_schedule_start( &(tdcqs.dtcs) );
		
							SYSTEM_check_for_mvor_schedule_start( &(tdcqs.dtcs) );

							// ----------
							
							FOAL_IRRI_maintain_irrigation_list( &(tdcqs.dtcs.date_time) );
	
							// ----------

							TDCHECK_at_1Hz_rate_check_for_lights_schedule_start( &( tdcqs.dtcs ) );

							FOAL_LIGHTS_maintain_lights_list( &( tdcqs.dtcs.date_time ) );

							// ----------

							// 2/20/2013 rmd : The order here may be critical (I say may as I suspect so). When it comes
							// to flow checking coupled with the slow closing count down is it possible there is a
							// sliver of time when the slow closing valve timer counts down to zero and we do not yet
							// have a valve ON and we fall into MLB checking for no valves ON? TODO investigate this.
							FOAL_FLOW_check_for_MLB();
		
							FOAL_FLOW_check_valve_flow();
						}

						// 8/18/2014 rmd : And all controllers, MASTERS and SLAVES must execute this function (if
						//  the chain is not down). If the chain is down the irri irrigation list should be empty by
						//  definition.
						IRRI_maintain_irrigation_list();
						
						// 8/13/2015 mpd : All controllers, MASTERS and SLAVES must execute this function (if the chain 
						// is not down). If the chain is down, the lights ON list will be cleared after a 120 second
						// wait time.
						IRRI_LIGHTS_maintain_lights_list( &( tdcqs.dtcs.date_time ) );

						// ----------
						
						// 7/19/2018 rmd : So I think it only makes sense to run the ftimes calculation if the
						// NETWORK is up and stable. So this attempt to restart the calculation is nested within
						// this section of 'network available' code. NOTE - ftimes is available on slave controllers
						// too, so outside that section about masters only.
						//
						// 7/19/2018 rmd : I have decided that, besides after any pdata change, we will also execute
						// the ftimes calculation, after the chain is up, once after a reboot, and once per day
						// after the ET table roll time. After the reboot is kind of obvious, to have the answer
						// ready when the user enters the screen. After the ET Table roll because different ET
						// numbers change the calculation results. We also want to run the calculation once per day,
						// just because, to cover unforseen reasons the calculation results are not current.
						if( !ftcs.calculation_has_run_since_reboot || (tdcqs.dtcs.date_time.T == WEATHER_get_et_and_rain_table_roll_time()) )
						{
							FTIMES_TASK_restart_calculation();
							
							// 7/19/2018 rmd : Can always set this (true) now, cause the calculation will run.
							ftcs.calculation_has_run_since_reboot = (true);
						}
						
					}
					else
					{
						UNS_32		i;
						
						// 5/7/2014 rmd : What to do while the network is NOT available? Well for one I think we
						// should keep the POC preserves flow readings at 0. This comes into play when a chain goes
						// down. The readings should be zeroed. Also on power up this same function is independently
						// called.
						init_battery_backed_poc_preserves( (false) );
						
						// ----------
						
						// 5/7/2014 rmd : Have got to be very careful here NOT to wipe out the irrigation. As this
						// code will execute on a reboot before the scan has completed. Our goal with this code here
						// is to zero the flow readings in the SYSTEM_PRESERVES. But when is this needed. When the
						// cahin officially goes down this is already called when the irrigation is wiped. On
						// startup this is already called. It would only be when a scan commences while the chain is
						// up say at user request. This is housekeeping that seems right to me but have no proof I'm
						// fixing anything by doing this. Except maybe zeroing the displayed flow reading while the
						// scan is taking place.
						xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

						for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
						{
							// 5/7/2014 rmd : AND after the chain comes up flow checking is blocked for 150 seconds. And
							// that is a good thing to prevent (false) MLB's that may occur as large main lines
							// fill.
							nm_SYSTEM_PRESERVES_restart_irrigation_and_block_flow_checking_for_150_seconds( &(system_preserves.system[ i ]) );
						}
					
						xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
						
					}
					
					// ----------
				
					// DO THE TIME & DATE SCREEN UPDATE LAST. The display task is a high priority and takes
					// takes a long time. Posting to the display queue will cause the display task to run. I'd
					// rather make sure the other once per second house keeping is complete FIRST. Sometimes by
					// observing any abnormalities with the time update (seconds move erratically for example) I
					// can tell something about system behavior.
					DISPLAY_EVENT_STRUCT	lde;
	
					// ------------
				
					xSemaphoreTakeRecursive( chain_members_recursive_MUTEX, portMAX_DELAY );
					
					// 10/1/2012 rmd : Set a flag to display that the chain is down on all screens. Calling
					// GuiLib_Refresh only as needed.
					if( GuiVar_StatusChainDown != comm_mngr.chain_is_down )
					{
						GuiVar_StatusChainDown = comm_mngr.chain_is_down;
						
						Refresh_Screen();
					}

					xSemaphoreGiveRecursive( chain_members_recursive_MUTEX );
	
					// ------------
				
					// now go queue up to display it
					lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
					lde._04_func_ptr = &FDTO_show_time_date;
					lde._06_u32_argument1 = (UNS_32)(&(tdcqs.dtcs.date_time));
					Display_Post_Command( &lde );
	
					// --------
					
					switch( GuiLib_CurStructureNdx )
					{
						case GuiStruct_rptFinishTimes_0:
						case GuiStruct_rptFinishTimes_1:
							// 7/19/2018 Ryan : bring up finish time calculation status dialog if calcutiong begins
							// while on the page.
							if( ftcs.mode == FTIMES_MODE_calculating )
							{
								lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
								lde._04_func_ptr = &FINISH_TIMES_draw_dialog;
								Display_Post_Command( &lde );
							}
							// 7/19/2018 Ryan : If, since the screen was last drawn, the finish time calculation ran
							// again, drawn the screen to relect the newly calculated values.
							else if( ftcs.duration_start_time_stamp != FINISH_TIME_calc_start_timestamp )
							{
								FINISH_TIME_scrollbox_index = GuiLib_ScrollBox_GetActiveLine( 0, 0 );
								
								lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
								lde._04_func_ptr = (void*)&FDTO_FINISH_TIMES_draw_screen;
								lde._06_u32_argument1 = (false);
								Display_Post_Command( &lde );
							}
							
							break;
							
						case GuiStruct_dlgFinishTimesCalc_0:
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = &FINISH_TIMES_update_dialog;
							Display_Post_Command( &lde );
							break;
				
						case GuiStruct_scrRadioTest_0:
							RADIO_TEST_post_event( RADIO_TEST_EVENT_1hz_screen_update );
							break;

						case GuiStruct_dlgDiscoveringDecoders_0:
							lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
							lde._04_func_ptr = &FDTO_TWO_WIRE_update_discovery_dialog;
							lde._06_u32_argument1 = (false);
							Display_Post_Command( &lde );
							break;

						case GuiStruct_dlgDeviceExchange_0:
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = &FDTO_DEVICE_EXCHANGE_update_dialog;
							Display_Post_Command( &lde );
							break;

						case GuiStruct_scrMainMenu_0:
							// 6/5/2015 ajv : Only redraw the STATUS screen if we're actually viewing the Status screen.
							if( (GuiLib_ActiveCursorFieldNo == CP_MAIN_MENU_STATUS) || (GuiVar_StatusShowLiveScreens) )
							{
								STATUS_update_screen( (false) );
							}
							break;

						case GuiStruct_rptIrriDetails_0:
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_IRRI_DETAILS_redraw_scrollbox;
							Display_Post_Command( &lde );
							break;

						case GuiStruct_rptRTSystemFlow_0:
							lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
							lde._04_func_ptr = (void*)&FDTO_REAL_TIME_SYSTEM_FLOW_draw_report;
							lde._06_u32_argument1 = (false);
							Display_Post_Command( &lde );
							break;

						case GuiStruct_rptRTPOCFlow_0:
							lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
							lde._04_func_ptr = (void*)&FDTO_REAL_TIME_POC_FLOW_draw_report;
							lde._06_u32_argument1 = (false);
							Display_Post_Command( &lde );
							break;

						case GuiStruct_rptRTBypassFlow_0:
							lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
							lde._04_func_ptr = (void*)&FDTO_REAL_TIME_BYPASS_FLOW_draw_report;
							lde._06_u32_argument1 = (false);
							Display_Post_Command( &lde );
							break;

						case GuiStruct_rptRTElectrical_0:
							lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
							lde._04_func_ptr = (void*)&FDTO_REAL_TIME_ELECTRICAL_draw_report;
							lde._06_u32_argument1 = (false);
							Display_Post_Command( &lde );
							break;

						case GuiStruct_rptRTCommunications_0:
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_REAL_TIME_COMMUNICATIONS_update_report;
							Display_Post_Command( &lde );
							break;

						case GuiStruct_rptRTWeather_0:
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_REAL_TIME_WEATHER_update_report;
							Display_Post_Command( &lde );
							break;

						case GuiStruct_scrWeatherSensors_0:
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_WEATHER_update_real_time_weather_GuiVars;
							Display_Post_Command( &lde );
							break;

						case GuiStruct_rptStationHistory_0:
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_STATION_HISTORY_redraw_scrollbox;
							Display_Post_Command( &lde );
							break;

						case GuiStruct_rptStationSummary_0:
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_STATION_REPORT_redraw_scrollbox;
							Display_Post_Command( &lde );
							break;

						case GuiStruct_rptPOCSummary_0:
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_POC_USAGE_redraw_scrollbox;
							Display_Post_Command( &lde );
							break;

						case GuiStruct_rptSystemSummary_0:
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_SYSTEM_REPORT_redraw_scrollbox; 
							Display_Post_Command( &lde );
							break;

						case GuiStruct_rptETRainTable_0:
							lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
							lde._04_func_ptr = (void*)&FDTO_ET_RAIN_TABLE_update_report;
							// 4/29/2015 rmd : Set argument to YES refresh the screen.
							lde._06_u32_argument1 = (true);
							Display_Post_Command( &lde );
							break;

						case GuiStruct_scrDateTime_0:
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_TIME_DATE_update_screen;
							Display_Post_Command( &lde );
							break;

						case GuiStruct_rptDecoderStats_0:
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)FDTO_TWO_WIRE_update_decoder_stats;
							Display_Post_Command( &lde );
							break;

						case GuiStruct_scrTestSequential_0:
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)FDTO_TEST_SEQ_update_screen;
							Display_Post_Command( &lde );
							break;

						case GuiStruct_scrMVOR_0:
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)FDTO_MVOR_update_screen;
							Display_Post_Command( &lde );
							break;

						case GuiStruct_scrLightsTest_0:
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)FDTO_LIGHTS_TEST_update_screen;
							Display_Post_Command( &lde );
							break;

						case GuiStruct_rptRTLights_0:
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)FDTO_REAL_TIME_LIGHTS_redraw_scrollbox;
							Display_Post_Command( &lde );
							break;

						case GuiStruct_rptLightsSummary_0:
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&FDTO_LIGHTS_REPORT_redraw_scrollbox;
							Display_Post_Command( &lde );
							break;

						case GuiStruct_scrMoisture_1:
							// 4/22/2016 ajv : Keep the current moisture and temperature measurements up-to-date.
							lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
							lde._04_func_ptr = (void*)&MOISTURE_SENSOR_update_measurements;
							Display_Post_Command( &lde );
							break;
					}

                    // Turn on LED for 20ms ... LAST
					CS_LED_GREEN_ON;
					vTaskDelay( MS_to_TICKS( 20 ) );
					CS_LED_GREEN_OFF;
					break;

					
				default:
					Alert_Message( "TD_CHECK: unk queue command!" );

			}

		}  // of taking one from the queue

		// ----------
		
		// 10/23/2012 rmd : Keep the activity indication going.
		task_last_execution_stamp[ task_index ] = xTaskGetTickCount();

		// ----------

	}  // of task while for-ever task loop

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

