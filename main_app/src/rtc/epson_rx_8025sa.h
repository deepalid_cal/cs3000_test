/*  file = epson_rx_8025sa.h                  2011.05.02 rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_EPSON_RX_8025SA_H
#define _INC_EPSON_RX_8025SA_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"cal_td_utils.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define TDQ_COMMAND_its_a_new_second			(0x1111)
#define TDQ_COMMAND_its_a_new_quarter_second	(0x2222)

typedef struct
{

	UNS_32						command;

	DATE_TIME_COMPLETE_STRUCT	dtcs;
	
} TD_CHECK_QUEUE_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


extern void EPSON_rtc_control_task( void *pvParameters );

extern void EPSON_obtain_latest_complete_time_and_date( DATE_TIME_COMPLETE_STRUCT *lcdt );

extern void EPSON_obtain_latest_time_and_date( DATE_TIME *ldt );


extern void EXCEPTION_USE_ONLY_obtain_latest_time_and_date( DATE_TIME *ldt );

// 11/30/2015 skc : Helper function for setting date and time.
// Makes a call to EPSON_set_the_time_and_date().
extern BOOL_32 EPSON_set_date_time( const DATE_TIME pdt, const UNS_32 preason );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

