/* file = epson_rx_8025sa.c                    2011.05.02 rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy
#include	<string.h>

// 12//01/2015 skc : Required for abs
#include	<stdlib.h>

#include	"epson_rx_8025sa.h"

#include	"lpc32xx_intc_driver.h"

#include	"lpc32xx_clkpwr_driver.h"

#include	"lpc32xx_i2c_driver.h"

#include	"lpc32xx_i2c.h"

#include	"cal_td_utils.h"

#include	"gpio_setup.h"

#include	"td_check.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"battery_backed_vars.h"

#include	"configuration_network.h"

#include	"trcuser.h"

#include	"background_calculations.h"

// 12/01/2015 skc : Support calling Alert_ChangeLine_Controller().
#include	"change.h"

#include	"flowsense.h"

// forward declarations
static void set_the_time_and_date( DATE_TIME_COMPLETE_STRUCT *ldt );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define EPSON_QCMD_SEEN_AN_ERROR						0x11
#define EPSON_QCMD_HERE_IS_THE_POWERUP_STATUS			0x22
#define EPSON_QCMD_COLD_START_COMPLETED					0x33
#define EPSON_QCMD_GOT_A_1HZ_TICK						0x44
#define EPSON_QCMD_HERE_IS_THE_LASTEST_TIME_DATE		0x55
#define EPSON_QCMD_SET_THE_TIME_DATE					0x66
#define EPSON_QCMD_TIME_DATE_HAS_BEEN_SET				0x77
// Another example cmd might be to set an alarm for Day Light Savings.

#define EPSON_RTC_NUMBER_OF_QUEUE_ITEMS		(10)

#define EPSON_DATA_BUFFER_SIZE				(16)

typedef struct
{
	UNS_32	cmd;

	UNS_32	status;

	UNS_8	data[ EPSON_DATA_BUFFER_SIZE ];

} EPSON_RTC_QUEUE_STRUCT;

xQueueHandle	Epson_RTC_Queue;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/19/2012 rmd : The TDCheck queue is added to at a 1Hz rate. Does any other task write
// to the TD_CHECK_queue? No. Only the EPSON rtc task itself.
#define		TD_CHECK_QUEUE_DEPTH	30

xQueueHandle	TD_CHECK_queue;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/11/2012 rmd : When the chip is programmed for the very first time we set this time and
// date into it. NOTE - not to use the default cal_td_utils.h defaults as the year for one
// is a 4 digit year and the rtc chip only takes a 2 digit year.
#define		EPSON_DEFAULT_TD_SECONDS		0
#define		EPSON_DEFAULT_TD_MINUTES		0
#define		EPSON_DEFAULT_TD_HOURS			12
#define		EPSON_DEFAULT_TD_WEEKDAY		SUNDAY
#define		EPSON_DEFAULT_TD_DAY_OF_MONTH	1
#define		EPSON_DEFAULT_TD_MONTH			1
#define		EPSON_DEFAULT_TD_YEAR			12

#define		EPSON_DATE_DEFAULT	(DMYToDate( EPSON_DEFAULT_TD_DAY_OF_MONTH, EPSON_DEFAULT_TD_MONTH, EPSON_DEFAULT_TD_YEAR ))

#define		EPSON_TIME_DEFAULT	(HMSToTime( EPSON_DEFAULT_TD_HOURS, EPSON_DEFAULT_TD_MINUTES, EPSON_DEFAULT_TD_SECONDS ))

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	volatile UNS_8		our_tx_fifo[ EPSON_DATA_BUFFER_SIZE ];

	volatile UNS_8		our_rx_fifo[ EPSON_DATA_BUFFER_SIZE ];

	volatile UNS_32		remaining_to_xmit;

	volatile UNS_32		remaining_to_rcv;
	volatile UNS_32		bytes_in_the_rcvd_fifo;
	volatile UNS_32		isr_bytes_to_be_feed_to_cause_rcv;

	volatile UNS_32		busy;

	volatile UNS_32		qcmd;

} EPSON_I2C_CONTROL_STRUCT;


static volatile EPSON_I2C_CONTROL_STRUCT	i2c1cs;  // LOCAL Control structure for the FIRST I2C channel.


#define	EPSON_I2C_READ_ADDR		0x65
#define	EPSON_I2C_WRITE_ADDR	0x64

// EPSON REGISTER ADDRESSES
#define EPSON_REG_SECONDS		0x00

#define EPSON_REG_CONTROL_1		0xE0

#define EPSON_REG_CONTROL_2		0xF0


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define		REG_F_VDET_BIT			_BIT(6)
#define		REG_F_XST_BIT			_BIT(5)
#define		REG_F_PON_BIT			_BIT(4)

#define		REG_F_DETECTION_MASK	0x70

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


// FILE PRIVATE VARIABLE - NOT to be referred to outside of this file
static DATE_TIME_COMPLETE_STRUCT	local_dt;


static xSemaphoreHandle	Time_and_date_MUTEX;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
UNS_32 hextobcd( UNS_32 input_val )
{
	// This can handle input_vals from 0 up to 99999999.
	unsigned long bcdresult = 0; char i;

    for( i=0; input_val; i++ )
	{
		((char*)&bcdresult)[i / 2] |= i & 1 ? (input_val % 10) << 4 : (input_val % 10) & 0xf;
		input_val /= 10;
	}

	return( bcdresult );
}

/* ---------------------------------------------------------- */
void epson_periodic_isr( void )
{
	EPSON_RTC_QUEUE_STRUCT		lerqs;

	portBASE_TYPE				higher_priority_task_woken;

	higher_priority_task_woken = pdFALSE;

	// Indicate that a second has passed.
	lerqs.cmd = EPSON_QCMD_GOT_A_1HZ_TICK;

	xQueueSendToFrontFromISR( Epson_RTC_Queue, &lerqs, &higher_priority_task_woken );

	// The task that processes the Epson_RTC_Queue queue is a HIGH priority task. One of the highest.
	// So let's get that new time and date information asap.
	if( higher_priority_task_woken == pdTRUE )
	{
		portYIELD_FROM_ISR();
	}
}

/* ---------------------------------------------------------- */
void epson_i2c1_write_isr( void )
{
	I2C_REGS_T					*reg_ptr = (I2C_REGS_T*)I2C1_BASE;

	EPSON_RTC_QUEUE_STRUCT		lerqs;

	portBASE_TYPE				higher_priority_task_woken;

	lerqs.status = reg_ptr->i2c_stat;

	if( (lerqs.status & (I2C_NAI | I2C_AFI)) != 0 )
	{
		// could set up for re-transmissions here...but as we are the only device on the bus not necessary
		lerqs.cmd = EPSON_QCMD_SEEN_AN_ERROR;
        // Post to our queue. We don't really care if there was an error ie queue full because at least some of these messages
		// would make it to the alerts pile.
		xQueueSendToBackFromISR( Epson_RTC_Queue, &lerqs, &higher_priority_task_woken );

		// Restart the i2c machine. Disabling interrupts, clearing the status register and resetting the i2c state to idle.
		reg_ptr->i2c_ctrl = I2C_RESET;

		i2c1cs.busy = FALSE;  // free to use again
	}
	else
	if( (lerqs.status & I2C_TDI) != 0)
	{
		// Clear the TDI bit. And reset the full i2c machine for that matter.
		reg_ptr->i2c_ctrl = I2C_RESET;

		i2c1cs.busy = FALSE;  // free to use again

		// Send the indication to the task.
		lerqs.cmd = i2c1cs.qcmd;

		// Post to the FRONT of our queue. So the rcvd data is made available or processed as
		// needed first. Before any other action takes place.
		xQueueSendToFrontFromISR( Epson_RTC_Queue, &lerqs, &higher_priority_task_woken );
	}
	else
	{
		// Feed the TX FIFO if needed.
		while( (i2c1cs.remaining_to_xmit > 0) && ((reg_ptr->i2c_stat & I2C_TFF) == 0) )
		{
			i2c1cs.remaining_to_xmit -= 1;

			if( i2c1cs.remaining_to_xmit == 0 )
			{
				reg_ptr->i2c_ctrl = ( I2C_NAIE | I2C_AFIE | I2C_TDIE );  // No longer need the FIFO available interrupts enabled.
				reg_ptr->i2c_txrx = I2C_STOP | i2c1cs.our_tx_fifo[ 0 ];
			}
			else
			{
				reg_ptr->i2c_txrx = i2c1cs.our_tx_fifo[ i2c1cs.remaining_to_xmit ];
			}
		}
	}
}

/* ---------------------------------------------------------- */
void epson_i2c1_read_isr( void )
{
	I2C_REGS_T					*reg_ptr = (I2C_REGS_T*)I2C1_BASE;

	EPSON_RTC_QUEUE_STRUCT		lerqs;

	portBASE_TYPE				higher_priority_task_woken;

	lerqs.status = reg_ptr->i2c_stat;

	if( (lerqs.status & (I2C_NAI | I2C_AFI)) != 0 )
	{
		// could set up for re-transmissions here...but as we are the only device on the bus not necessary
		lerqs.cmd = EPSON_QCMD_SEEN_AN_ERROR;
		// Post to our queue. We don't really care if there was an error ie queue full because at least some of these messages
		// would make it to the alerts pile.
		xQueueSendToBackFromISR( Epson_RTC_Queue, &lerqs, &higher_priority_task_woken );

		// Restart the i2c machine. Disabling interrupts, clearing the status register and resetting the i2c state to idle.
		reg_ptr->i2c_ctrl = I2C_RESET;

		i2c1cs.busy = FALSE;  // free to use again
	}
	else
	if( (lerqs.status & I2C_TDI) != 0)
	{
		// Transaction complete isr. First empty the receive FIFO as an INTERESTING CONDITION TAKES PLACE ONCE IN A WHILE. And this may be more an
		// interaction of the development environment than anything. But hard to tell. And this fix does not seem to have negative consequences for
		// none development environments. The ISSUE: it seems we get the TDI and the RFE interrupts at the same time. And the way our code is
		// written in that case the RFE information will be ignored. I have only seen this occur during startup and only for certain code configurations.
		// It doesn't happen all the time. But certainly it can. And this definately helps. Besides when I was developing this I2C driver I wondered
		// about this. That is the RFE and TDI interrupts occuring simultaneously. A kind of race. And we should never allow race conditions to break
		// the code of course.
		// 
		// Without this check on the RFE interrupt we would sometimes see on startup the RTC do a cold start because Reg_F would never actually be read
		// out of the RX FIFO. And therefore it seemed as if the oscillator stopped thereby triggering a cold start.
		while( (i2c1cs.remaining_to_rcv > 0) && ((reg_ptr->i2c_stat & I2C_RFE) == 0))
		{
			i2c1cs.our_rx_fifo[ i2c1cs.bytes_in_the_rcvd_fifo++ ] = reg_ptr->i2c_txrx;

			i2c1cs.remaining_to_rcv--;
		}

		// Clear the TDI bit. And reset the full i2c machine for that matter.
		reg_ptr->i2c_ctrl = I2C_RESET;

		i2c1cs.busy = FALSE;  // free to use again

		// Send the results to the task.
		lerqs.cmd = i2c1cs.qcmd;

		memcpy( &(lerqs.data), &(i2c1cs.our_rx_fifo), EPSON_DATA_BUFFER_SIZE );  // get a copy of the rcvd data so it can't be accidentally overwritten

		// Post to the FRONT of our queue. So the rcvd data is made available or processed as needed
		// first. Before any other action takes place.
		xQueueSendToFrontFromISR( Epson_RTC_Queue, &lerqs, &higher_priority_task_woken );
	}
	else
	{
		// So we've received an interrupt under normal transmission/receive conditions. First empty the receive FIFO.
		while( (i2c1cs.remaining_to_rcv > 0) && ((reg_ptr->i2c_stat & I2C_RFE) == 0))
		{
			i2c1cs.our_rx_fifo[ i2c1cs.bytes_in_the_rcvd_fifo++ ] = reg_ptr->i2c_txrx;

			i2c1cs.remaining_to_rcv--;
		}

		// Not all the data required for the receipt has been pushed into the Tx FIFO ... push in some more as needed.
		while( (i2c1cs.isr_bytes_to_be_feed_to_cause_rcv > 0) && ((reg_ptr->i2c_stat & I2C_TFF) == 0))
		{
			i2c1cs.isr_bytes_to_be_feed_to_cause_rcv -= 1;

			if( i2c1cs.isr_bytes_to_be_feed_to_cause_rcv == 0 )
			{
				reg_ptr->i2c_txrx = I2C_STOP;  // last byte indication
			}
			else
			{
				reg_ptr->i2c_txrx = 0x00;  // data unimportant
			}
		}

	}  // no error condition detected
}

/* ---------------------------------------------------------- */
void _epson_read_REG_F( void )
{
	I2C_REGS_T	*reg_ptr = (I2C_REGS_T*)I2C1_BASE;
	
	// For some reason that I do not fully understand it seems to be very important (won't work properly otherwise) to
	// have the I2C channel interrupt DISABLED during the setup of the transmission. I don't know why they can't be enabled
	// in the ctrl register and then the write to the txrx reg cause an immediate interrupt. Intead we have to keep the MIC
	// disabled until the end. And that makes everything work A-OK.
	xDisable_ISR( I2C_1_INT );
	
	xSetISR_Vector( I2C_1_INT, INTERRUPT_PRIORITY_epson_i2c_channel, ISR_TRIGGER_LOW_LEVEL, epson_i2c1_read_isr, NULL );
	
	reg_ptr->i2c_ctrl = I2C_RESET;  // perform soft reset...self clearing
	
	i2c1cs.remaining_to_rcv = 1;  // we want to receive ONE byte

	i2c1cs.bytes_in_the_rcvd_fifo = 0;  // we want to receive ONE byte
	
	i2c1cs.isr_bytes_to_be_feed_to_cause_rcv = 0;
	
	i2c1cs.busy = TRUE;
	
	i2c1cs.qcmd = EPSON_QCMD_HERE_IS_THE_POWERUP_STATUS;  // for when the isr completes the job

	reg_ptr->i2c_ctrl = ( I2C_RFDAIE | I2C_RFFIE | I2C_DRMIE | I2C_NAIE | I2C_AFIE | I2C_TDIE );  // Enable the interrupts...which are needed to complete the receipt of the data.
	
	reg_ptr->i2c_txrx = (I2C_START | EPSON_I2C_READ_ADDR );  // Send the FIRST byte. Interrupt driven transmission will cause the receipt of the single byte.

	reg_ptr->i2c_txrx = (I2C_STOP);  // AND send the dummy byte to cause receipt of a rcvd byte and trigger an interupt. With STOP set cause this is the only (and last) byte to recieve.
	
	xEnable_ISR( I2C_1_INT );  // Enable interrupt in the mic for this channel.
}

/* ---------------------------------------------------------- */
void _epson_read_the_time_and_date( void )
{
	I2C_REGS_T	*reg_ptr = (I2C_REGS_T*)I2C1_BASE;


	// For some reason that I do not fully understand it seems to be very important (won't work properly otherwise) to
	// have the I2C channel interrupt DISABLED during the setup of the transmission. I don't know why they can't be enabled
	// in the ctrl register and then the write to the txrx reg cause an immediate interrupt. Intead we have to keep the MIC
	// disabled until the end. And that makes everything work A-OK.
	xDisable_ISR( I2C_1_INT );

	xSetISR_Vector( I2C_1_INT, INTERRUPT_PRIORITY_epson_i2c_channel, ISR_TRIGGER_LOW_LEVEL, epson_i2c1_read_isr, NULL );

	reg_ptr->i2c_ctrl = I2C_RESET;  // perform soft reset...self clearing

	i2c1cs.remaining_to_rcv = 8;  // REG_F plus the 7 time & date registers

	i2c1cs.bytes_in_the_rcvd_fifo = 0;  // we want to receive ONE byte

	i2c1cs.isr_bytes_to_be_feed_to_cause_rcv = 6;  // need to add 6 more total to the tx_fifo to cause the receipt of our 8
												   // NOTE: we push in TWO below

	i2c1cs.busy = TRUE;

	i2c1cs.qcmd = EPSON_QCMD_HERE_IS_THE_LASTEST_TIME_DATE;  // for when the isr completes the job

	reg_ptr->i2c_ctrl = ( I2C_RFDAIE | I2C_RFFIE | I2C_DRMIE | I2C_NAIE | I2C_AFIE | I2C_TDIE );  // Enable the interrupts...which are needed to complete the receipt of the data.

	reg_ptr->i2c_txrx = (I2C_START | EPSON_I2C_READ_ADDR );  // Send the FIRST byte.

	reg_ptr->i2c_txrx = 0x00;  // AND send a dummy byte to cause receipt of a rcvd byte and trigger an interupt.

	reg_ptr->i2c_txrx = 0x00;  // AND send a dummy byte to cause receipt of a rcvd byte and trigger an interupt.

	xEnable_ISR( I2C_1_INT );  // Enable interrupt in the mic for this channel.
}

/* ---------------------------------------------------------- */
void _epson_set_the_time_and_date( EPSON_RTC_QUEUE_STRUCT *ptr_rtcq )
{
	// First we write the two control registers (E & F) such that we are all set up. Including the 1Hz interrupt. However note
	// this INT_A input to the LPC3250 (GPI_04) is first disabled. Also consider that this interrupt is disabled from a powerup (reset) condition.
	// 
	// Then we continue the write taking advantage of the auto address increment function. And set the default date and time. When the queue receives
	// the sucessful completion message about this write we will enable the GPI_04_INT interrupt in the LPC3250.

	I2C_REGS_T	*reg_ptr = (I2C_REGS_T*)I2C1_BASE;
	
	// For some reason that I do not fully understand it seems to be very important (won't work properly otherwise) to
	// have the I2C channel interrupt DISABLED during the setup of the transmission. I don't know why they can't be enabled
	// in the ctrl register and then the write to the txrx reg cause an immediate interrupt. Intead we have to keep the MIC
	// disabled until the end. And that makes everything work A-OK.
	xDisable_ISR( I2C_1_INT );
	
	xSetISR_Vector( I2C_1_INT, INTERRUPT_PRIORITY_epson_i2c_channel, ISR_TRIGGER_LOW_LEVEL, epson_i2c1_write_isr, NULL );
	

	reg_ptr->i2c_ctrl = I2C_RESET;  // perform soft reset...self clearing


	i2c1cs.qcmd = EPSON_QCMD_TIME_DATE_HAS_BEEN_SET;  // for when the isr completes the job

	// NOTE: they actually have to be loaded into our tx fifo in reverse! (Shown here in the order they arrive at the RTC.)
	i2c1cs.our_tx_fifo[  7 ] = 0x00;  // Address setting and transfer mode to write. Start at address 0.

	i2c1cs.our_tx_fifo[  6 ] = hextobcd( ptr_rtcq->data[ 0 ] );			// REG 0 - the seconds
	i2c1cs.our_tx_fifo[  5 ] = hextobcd( ptr_rtcq->data[ 1 ] );			// REG 1 - the minutes
	i2c1cs.our_tx_fifo[  4 ] = hextobcd( ptr_rtcq->data[ 2 ] );			// REG 2 - the hours
	i2c1cs.our_tx_fifo[  3 ] = hextobcd( ptr_rtcq->data[ 3 ] );			// REG 3 - the day of week
	i2c1cs.our_tx_fifo[  2 ] = hextobcd( ptr_rtcq->data[ 4 ] );			// REG 4 - the days
	i2c1cs.our_tx_fifo[  1 ] = hextobcd( ptr_rtcq->data[ 5 ] );			// REG 5 - the months
	i2c1cs.our_tx_fifo[  0 ] = hextobcd( ptr_rtcq->data[ 6 ] );			// REG 6 - the years
	
	i2c1cs.remaining_to_xmit = 1 + 7;

	i2c1cs.busy = TRUE;

	reg_ptr->i2c_ctrl = ( I2C_TFFIE | I2C_DRMIE | I2C_NAIE | I2C_AFIE | I2C_TDIE );  // Enable the interrupts...which are needed to complete the transmission.

	reg_ptr->i2c_txrx = (I2C_START | EPSON_I2C_WRITE_ADDR);  // Send the FIRST byte. Interrupt driven transmission will commence.


	xEnable_ISR( I2C_1_INT );  // Enable interrupt in the mic for this channel.
}

/* ---------------------------------------------------------- */
void _epson_rtc_cold_start( void )
{
	// First we write the two control registers (E & F) such that we are all set up. Including the 1Hz interrupt. However note
	// this INT_A input to the LPC3250 (GPI_04) is first disabled. Also consider that this interrupt is disabled from a powerup (reset) condition.
	// 
	// Then we continue the write taking advantage of the auto address increment function. And set the default date and time. When the queue receives
	// the sucessful completion message about this write we will enable the GPI_04_INT interrupt in the LPC3250.

	I2C_REGS_T	*reg_ptr = (I2C_REGS_T*)I2C1_BASE;
	
	// For some reason that I do not fully understand it seems to be very important (won't work properly otherwise) to
	// have the I2C channel interrupt DISABLED during the setup of the transmission. I don't know why they can't be enabled
	// in the ctrl register and then the write to the txrx reg cause an immediate interrupt. Intead we have to keep the MIC
	// disabled until the end. And that makes everything work A-OK.
	xDisable_ISR( I2C_1_INT );
	
	xSetISR_Vector( I2C_1_INT, INTERRUPT_PRIORITY_epson_i2c_channel, ISR_TRIGGER_LOW_LEVEL, epson_i2c1_write_isr, NULL );
	

	reg_ptr->i2c_ctrl = I2C_RESET;  // perform soft reset...self clearing


	i2c1cs.qcmd = EPSON_QCMD_COLD_START_COMPLETED;  // for when the isr completes the job

	// NOTE: they actually have to be loaded into our tx fifo in reverse! (Shown here in the order they arrive at the RTC.)
	i2c1cs.our_tx_fifo[  9 ] = 0xE0;  // Address setting and transfer mode to write
	i2c1cs.our_tx_fifo[  8 ] = 0x33;  // REG E working towards FOUT off, set 24 HOUR mode, set 1 Hz pulse mode for INT_A output
	i2c1cs.our_tx_fifo[  7 ] = 0x28;  // REG F complete FOUT off, set the XST bit and clear ALL the others

	i2c1cs.our_tx_fifo[  6 ] = hextobcd( EPSON_DEFAULT_TD_SECONDS );			// REG 0 - the seconds
	i2c1cs.our_tx_fifo[  5 ] = hextobcd( EPSON_DEFAULT_TD_MINUTES );			// REG 1 - the minutes
	i2c1cs.our_tx_fifo[  4 ] = hextobcd( EPSON_DEFAULT_TD_HOURS );				// REG 2 - the hours
	i2c1cs.our_tx_fifo[  3 ] = hextobcd( EPSON_DEFAULT_TD_WEEKDAY );			// REG 3 - the day of week
	i2c1cs.our_tx_fifo[  2 ] = hextobcd( EPSON_DEFAULT_TD_DAY_OF_MONTH );		// REG 4 - the days
	i2c1cs.our_tx_fifo[  1 ] = hextobcd( EPSON_DEFAULT_TD_MONTH );				// REG 5 - the months
	i2c1cs.our_tx_fifo[  0 ] = hextobcd( EPSON_DEFAULT_TD_YEAR );				// REG 6 - the years
	
	i2c1cs.remaining_to_xmit = 3 + 7;

	i2c1cs.busy = TRUE;

	reg_ptr->i2c_ctrl = ( I2C_TFFIE | I2C_DRMIE | I2C_NAIE | I2C_AFIE | I2C_TDIE );  // Enable the interrupts...which are needed to complete the transmission.

	reg_ptr->i2c_txrx = (I2C_START | EPSON_I2C_WRITE_ADDR);  // Send the FIRST byte. Interrupt driven transmission will commence.


	xEnable_ISR( I2C_1_INT );  // Enable interrupt in the mic for this channel.
}

/* ---------------------------------------------------------- */
void _init_epson_i2c_channel( void )
{
	// Initialize the I2C1 hardware.
	// Determine if the RTC battery backup supply has been valid while the power was out.
	// Read out the T&D into our local variable.

	I2C_REGS_T	*reg_ptr = (I2C_REGS_T*)I2C1_BASE;

	volatile UNS_32 tmp, high, low;

	clkpwr_clk_en_dis( CLKPWR_I2C1_CLK, 1 );

	// Setup a 200Khz 50% duty cycle channel.
	low = high = 50;
	tmp = clkpwr_get_base_clock_rate( CLKPWR_HCLK );
	reg_ptr->i2c_clk_lo = (low * (tmp / (high + low))) / (200000);
	reg_ptr->i2c_clk_hi = (high * (tmp / (high + low))) / (200000);

	clkpwr_set_i2c_driver( 1, I2C_PINS_HIGH_DRIVE );

	reg_ptr->i2c_ctrl = I2C_RESET;  // perform soft reset...self clearing

	i2c1cs.busy = FALSE;

	// We will set up the ISR routine to use when read or write sequence is put together. For now disable the isr.
	xDisable_ISR( I2C_1_INT );

	// And disable the 1Hz interrupt from interrupting the core. This will become enabled either upon a good power on status
	// detected OR after the cold start is completed.
	xDisable_ISR( GPI_04_INT );

	// The epson part can output a pulse mode int signal. Meaning it stays low until we write a bit in one of its registers. That's a pain to do when in the isr.
	// We'd have to disable the interrupt in the 3250 intc and setup up the i2c transfer to clear the int. The upon sucessful completion re-enable the intc for this
	// int. Easier to just use edge triggered and set the part up to output a 1hz square wave on the INT_A pin. Which we do.
	xSetISR_Vector( GPI_04_INT, INTERRUPT_PRIORITY_epson_1hz, ISR_TRIGGER_NEGATIVE_EDGE, epson_periodic_isr, NULL );

	// This will result in a queue message about the power on status. The results of which are analyzed to decide
	// if we read the RTC or set the default T&D into place.
	_epson_read_REG_F();
}

/* ---------------------------------------------------------- */
void EPSON_rtc_control_task( void *pvParameters )
{
	EPSON_RTC_QUEUE_STRUCT		lerqs;

	UNS_32	new_seconds;

	UNS_8	powerup_status;
	
	TD_CHECK_QUEUE_STRUCT		ltdcqs;

	DATE_TIME_COMPLETE_STRUCT	ldtcs;

	DAYLIGHT_SAVING_TIME_STRUCT	*ldls_spring_ahead, *ldls_fall_back;

	// ----------
	
	// 10/17/2012 rmd : Do not move this create to our function in app_startup that creates all.
	// This task is created early before that function call and obviously requires this queue.
	Epson_RTC_Queue = xQueueCreate( EPSON_RTC_NUMBER_OF_QUEUE_ITEMS, sizeof(EPSON_RTC_QUEUE_STRUCT) );
	vTraceSetQueueName( Epson_RTC_Queue, "RTC Queue" );

	// --------
	
	// 10/17/2012 rmd : Do not move this create to our function in app_startup that creates all.
	// This task is created early before that function call and obviously requires this mutex.
	Time_and_date_MUTEX = xSemaphoreCreateMutex();
	vTraceSetQueueName( Time_and_date_MUTEX, "Time_Date MUTEX" );
	
	// --------
	
	// 10/17/2012 rmd : Do not move this create to our function in app_startup that creates all.
	// This task is created early before that function call and requires this mutex.
	TD_CHECK_queue = xQueueCreate( TD_CHECK_QUEUE_DEPTH, sizeof( TD_CHECK_QUEUE_STRUCT ) );
	vTraceSetQueueName( TD_CHECK_queue, "TD_CHECK Queue" );
	
	// ----------
	
	// Kick off the hardware. Set the clock rate, drive the lines high, and idle the device.
	_init_epson_i2c_channel();

	// ----------
	
	new_seconds = 0;

	// ----------
	
	while( (true) )
	{
		if( Epson_RTC_Queue != NULL )  // Was it not made?
		{
			// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
			if( xQueueReceive( Epson_RTC_Queue, &lerqs, MS_to_TICKS( 500 ) ) == pdTRUE )
			{
				switch( lerqs.cmd )
				{
					case EPSON_QCMD_SEEN_AN_ERROR:
						Alert_Message( "Epson RTC I2C: isr error detected" );
						break;
						
					case EPSON_QCMD_HERE_IS_THE_POWERUP_STATUS:
						//Alert_Message( "Epson RTC I2C: got power up status" );

						powerup_status = lerqs.data[ 0 ];

						if( (powerup_status & REG_F_DETECTION_MASK) != REG_F_XST_BIT )
						{
							// So one of the VDET or PON bits are set, OR the crystal oscillation bit XST is not still set. So we need to do
							// a complete restart. Pushing in the default date and time.
							_epson_rtc_cold_start();
                        }
						else
						{
							// Read the clock or enable the 1hz interrupt? One or the other.
							xEnable_ISR( GPI_04_INT );  // Enable the interrupt in the mic for the 1 Hz waveform.
						}
						break;
	
					case EPSON_QCMD_COLD_START_COMPLETED:
						Alert_Message( "Epson RTC: cold start completed" );

						xEnable_ISR( GPI_04_INT );  // Enable the interrupt in the mic for the 1 Hz waveform.
						break;

					case EPSON_QCMD_SET_THE_TIME_DATE:
						_epson_set_the_time_and_date( &lerqs );
						break;

					case EPSON_QCMD_GOT_A_1HZ_TICK:
						_epson_read_the_time_and_date();
						break;

					case EPSON_QCMD_HERE_IS_THE_LASTEST_TIME_DATE:
						// Get it transferred into our local variable. So it is available when others request it.
						// The queue item holds the data. And by definition the first byte is REG_F. So let's do
						// some error checking on it.
						if( (lerqs.data[ 0 ] & REG_F_DETECTION_MASK) != REG_F_XST_BIT )
						{
							Alert_Message( "RTC 1 hz error" );
						}
						else
						{
							// -----------------------
							
							xSemaphoreTake( Time_and_date_MUTEX, portMAX_DELAY );

							local_dt.__seconds = ( (lerqs.data[ 1 ]>>4)*10) + (lerqs.data[ 1 ]& 0x0f);
							local_dt.__minutes = ( (lerqs.data[ 2 ]>>4)*10) + (lerqs.data[ 2 ]& 0x0f);
							local_dt.__hours = ( (lerqs.data[ 3 ]>>4)*10) + (lerqs.data[ 3 ]& 0x0f);

							local_dt.__dayofweek = lerqs.data[ 4 ];  // no translation needed
							local_dt.__day = ( (lerqs.data[ 5 ]>>4)*10) + (lerqs.data[ 5 ]& 0x0f);
							local_dt.__month = ( (lerqs.data[ 6 ]>>4)*10) + (lerqs.data[ 6 ]& 0x0f);
							local_dt.__year = 2000 + ( (lerqs.data[ 7 ]>>4)*10) + (lerqs.data[ 7 ]& 0x0f);

							local_dt.date_time.T = HMSToTime( local_dt.__hours, local_dt.__minutes, local_dt.__seconds );
							local_dt.date_time.D = DMYToDate( local_dt.__day, local_dt.__month, local_dt.__year );

							// Release the MUTEX before going any further.
							xSemaphoreGive( Time_and_date_MUTEX );

							// -----------------------
							
							// The STARTUP_TASK is waiting till this RTC task has gotten this far through its
							// initialization. So that a real time and date is available for use. Like in an alert line
							// made during startup. The startup task being a low priority task will not actually resume
							// till this task blocks on its queue.
							xSemaphoreGive( startup_rtc_task_sync_semaphore );

							// -----------------------
							
							// NOTE: on power up we will usually get two interrupts which trigger two reads of
							// the RTC for the same second. I have usually seen this as the first two reads.
							// But also have seen this for the 2nd and 3rd reads which were for a different
							// second than the first read. Be aware of this. Not sure this is a problem. But
							// the code should expect this possiblity upon powerup.
							//
							// And under normal operating conditions (ie not a startup) we would be telling
							// some kind of tdcheck task that there is a new T & D to check against. Which in
							// turn could cause a screen write of the new time & date. Or the detection of a
							// start time. Now that could make a problem. Also need to consider the effects to
							// the day light savings logic. So make it so the first 3 seconds do not exist.
							// It's as if the power was out for 3 more seconds - eh?

							// Each restart new_seconds starts over again from 0. It will count up for more
							// than 100 yrs before rolling over! It counts seconds up and running since the
							// last powerup.
							new_seconds += 1;
	
							// The first few time & date retrievals can produce a duplicate time & date. So
							// drop the first three from a restart.
							if( new_seconds >= 3 )
							{
								xSemaphoreTake( Time_and_date_MUTEX, portMAX_DELAY );

								/* ------------------------------- */
								/* ------------------------------- */
								// Do the DLS activities now. If we are going to spring forward then there is
								// NO 1AM. It becomes 2AM. Falling back is more complicated because we have to
								// traverse the 1AM-2AM hour twice.

								// Manage the day light savings eligibility variable. Even if we are not being asked to make
								// the dls time adjustment. So its ready for use.
								if( local_dt.date_time.D != weather_preserves.dls_saved_date )
								{
									weather_preserves.dls_eligible_to_fall_back = (true);
									
									weather_preserves.dls_saved_date = local_dt.date_time.D;
								}

								// Reset the very powerful ignore start times setting. You want any 2AM start times to get
								// picked up on the second pass of 2AM (in the fall). And you want to protect from a power
								// failure causing us to miss the second traversing of 2AM (in the fall back case). It is
								// normally the traversing of 2AM for the second time that would cause us to reset this very
								// powerful 'ignore sxs' variable. However in the case of a powerfail that causes us to miss
								// when to reset it use an actual DATE and TIME to check against.
								if( weather_preserves.dls_after_fall_back_ignore_sxs == (true) )
								{
									if( DT1_IsBiggerThanOrEqualTo_DT2( &local_dt.date_time, &weather_preserves.dls_after_fall_when_to_clear_ignore_sxs ) == TRUE )
									{
										weather_preserves.dls_after_fall_back_ignore_sxs = (false);
									}
								}


								if ( NETWORK_CONFIG_get_is_dls_used_for_the_selected_time_zone() == (true) )
								{
									if ( local_dt.date_time.T == 7200 ) {  // DAYLIGHT SAVINGS - is it 2:00AM
							
										if ( local_dt.__dayofweek == 0 ) {  // is it Sunday

											ldls_spring_ahead = NETWORK_CONFIG_get_dls_ptr( (true) );

											ldls_fall_back = NETWORK_CONFIG_get_dls_ptr( (false) );
							
											// Test to SPRING AHEAD.
											if ( local_dt.__month == ldls_spring_ahead->month )  // is it March
											{
												if ( (local_dt.__day > ldls_spring_ahead->min_day) && (local_dt.__day < ldls_spring_ahead->max_day) )  // it is the second sunday of March - do it
												{
														ldtcs = local_dt;

														ldtcs.date_time.T += 3600;

														ldtcs.__hours += 1;

														set_the_time_and_date( &ldtcs );
												}
											}
											else
											if ( local_dt.__month == ldls_fall_back->month ) { // is it November
							
												if ( (local_dt.__day > ldls_fall_back->min_day) && (local_dt.__day < ldls_fall_back->max_day) )  // it is the first Sunday of November  - do it
												{
													// The eligibility is set once to TRUE at the detected change of day. You can see that
													// above. Once used we must set it back FALSE to prevent FALLING BACK again when we hit 2AM
													// again. Which after you fall back of course you do - that is re-run through time.
													if ( weather_preserves.dls_eligible_to_fall_back )
													{
														// Capture when to reset the skipping of start times.
														weather_preserves.dls_after_fall_when_to_clear_ignore_sxs = local_dt.date_time;
														
														ldtcs = local_dt;
														
														ldtcs.date_time.T -= 3600;

														ldtcs.__hours -= 1;

														set_the_time_and_date( &ldtcs );

														weather_preserves.dls_after_fall_back_ignore_sxs = (true);  // so not to double SXs 1AM to 2AM
														
														weather_preserves.dls_eligible_to_fall_back = (false);  // not again please
													}
							
												}  // is the day <= 7
							
											}  // what month is it
							
										}  // of if it is Sunday
							
									}  // of if the time is 2AM
							
								}  // of if daylight saving enabled
								else
								{
									// MAKE sure this is set FALSE when we are not doing day light savings.
									weather_preserves.dls_after_fall_back_ignore_sxs = (false);
								}

								// ----------
								
								// Acquire the result from the battery backed variable.
								local_dt.dls_after_fall_back_ignore_start_times = weather_preserves.dls_after_fall_back_ignore_sxs;

								// ----------
								
								// May NOW post the time keeper task queue with a new updated T&D to look at.
								ltdcqs.command = TDQ_COMMAND_its_a_new_second;

								// Get a copy of the official internal date and time structure for the queue.
								ltdcqs.dtcs = local_dt;
							
								// Give the MUTEX back when we're done and before we post to the TD Check task queue. Just
								// cause that seems better to do. Though there should be no real reason to GIVE the mutex
								// before the post to the queue.
								xSemaphoreGive( Time_and_date_MUTEX );

								if ( xQueueSendToBack( TD_CHECK_queue, &ltdcqs, 0 ) != pdPASS )  // do not wait for queue space ... flag an error
								{
									// 10/19/2012 rmd : If the TD_CHECK task is being starved or the TD_CHECK task itself is
									// 'broken' this alert will appear once per second. UNTIL the watchdog strikes which occurs
									// in no more than it's timeout period (2 seconds).
									Alert_Message( "TD_CHECK QUEUE OVERFLOW!" );
								}

							}  // Of if 3 1Hz interrupts have occurred. Which may not be 3 seconds. As there
							   // seems to be some juggling that takes place before the rythm is smooth?

						}
						break;
						
				}  // of the switch statement of what we got from the queue 
	
				// ----------
				
				// We wait here. After we've pulled an item from the queue. This is so in the case of say reading the latest time & date
				// or pulling the power on status those results are pushed to the front of the queue and will be available first. EVEN if
				// another process or even ourselves post another queue command.
				// 
				// Sit tight for a bit if the hardware is busy. If we wait for too long go ahead anyway but flag the error.
				// For each sequence we send to the DAC we perform a soft RESET on the LPC3250 I2C state machine. So if there
				// is some sort of an issue at least the reset would allow us to start over.
				// 
				// Be AWARE. If we accidentally OVERFLOW the TX FIFO there will be a DATA ABORT exception generated so we really do
				// not want to allow this to happen. It ain't pretty!
				UNS_32	waited;
	
				waited = 0;
	
				while( i2c1cs.busy == TRUE )
				{
					// 2012.01.26 rmd : This used to be a 1ms delay and that was fine with a 1kHz tick rate.
					// However in order to reduce the OS overhead of such a high tick rate I have reduced the
					// tick rate to 200Hz. And as such there can be no delays less than 5ms. So make this 5ms.
					// NOTE: even at 5 ms the actual delay is anywhere from 0 to 5ms. Because we do not know
					// where we are in the tick process. ACTUALLY the safest way to set this delay is as 1 tick.
					// Not related to ms. That way even if the tick rate is lowered we still get at least 1 tick
					// and not 0 ticks which is what would happen if we asked for 1ms when the tick rate was
					// 5ms.
					vTaskDelay( 1 );  // To read the RTC takes no more than 1ms normally. Set to 1 TICK! NOT 1 MS.
	
					if( ++waited == 10 )
					{
						// So we waited 100 ms for the process to end gracefully. Flag an alert and
						// move ahead. Counting on the soft RESET to clean up the hardware.
						Alert_Message( "Unexpected EPSON RTC I2C delay!" );
						break;  // Git out of here! and try the i2c device again.
					}
				}

			}  // of if the queue returned an item 

			// ----------
			
			// 10/24/2012 rmd : Keep the task activity marker updated.
			rtc_task_last_x_stamp =  xTaskGetTickCount();
			
			// ----------

		}
		else
		{
			Alert_Message( "EPSON RTC queue handle not valid!" );  // Should not get here
		}

	}
}

/* ---------------------------------------------------------- */
static void set_the_time_and_date( DATE_TIME_COMPLETE_STRUCT *ldt )
{
	// We are assuming the caller has loaded the individual hours, mins, seconds, day of week, day, month, yr fields in the
	// pointed to structure argument.

	EPSON_RTC_QUEUE_STRUCT	lerqs;

	lerqs.cmd = EPSON_QCMD_SET_THE_TIME_DATE;

	lerqs.data[ 0 ] = ldt->__seconds;		// REG 0 - the seconds
	lerqs.data[ 1 ] = ldt->__minutes;		// REG 1 - the minutes
	lerqs.data[ 2 ] = ldt->__hours;			// REG 2 - the hours
	lerqs.data[ 3 ] = ldt->__dayofweek;		// REG 3 - the day of week
	lerqs.data[ 4 ] = ldt->__day;			// REG 4 - the days
	lerqs.data[ 5 ] = ldt->__month;			// REG 5 - the months
	lerqs.data[ 6 ] = ldt->__year - 2000;	// REG 6 - the years after 2000
	 
	// post the event using the queue handle for this serial port driver
	if ( xQueueSendToBack( Epson_RTC_Queue,  &lerqs, 0 ) != pdPASS )  // do not wait for queue space ... flag an error
	{
		Alert_Message( "EPSON RTC QUEUE OVERFLOW!" );
	}

	// ----------

	// 9/5/2014 ajv : Additionally, post a message to update the GuiVars used to display the
	// next scheduled irrigation on the Status screen. This ensures the Status screen is
	// up-to-date after someone changes the date/time.
	// 
	// 9/5/2014 ajv : NOTE: Even though the RTC task has a priority level of 14, it's likely
	// that the calculation will start BEFORE the information has been written to the RTC and
	// re-read into the variables used by the background calculation to determine the current
	// date and time. This results in the previous time being used rather than the new time.
	// Therefore, we've introduced a slight delay in the processing of this event to ensure the
	// RTC task has an updated copy to work with before we start the calculation. See
	// background_calculations.c for more information.
	// 
	// 8/13/2015 ajv : This may seem like an odd place to do this, but the goal is to ensure the
	// calculation is performed regardless of where the change comes from - both from the UI as
	// well as from Command Center Online. Bob discovered this call was missing from Command
	// Center Online when his controller updated the time a 4:00 PM.
	postBackground_Calculation_Event( BKGRND_CALC_NEXT_SCHEDULED );
}

/* ---------------------------------------------------------- */
void EPSON_obtain_latest_complete_time_and_date( DATE_TIME_COMPLETE_STRUCT *lcdt )
{
	// Alert lines attempt to obtain the latest time & date. And it is easy to have code attempt an alert line too early in the startup
	// sequence. If the MUTEX is a NULL the xSemaphoreTake function invokes assert which in turn attempts another alert line = CRASH.
	// Well we don't want it to crash so at this level we should protect against the NULL mutex. Elsewhere in the alert generation we
	// protect against the pile not being ready.
	if( Time_and_date_MUTEX != NULL )
	{
		// Guard against the variable being modified while we are accessing it. This function can be called from any task within the application to
		// get the system time & date. Prevent the TDCheck task from writing to the local variable while we are reading it.
		xSemaphoreTake( Time_and_date_MUTEX, portMAX_DELAY );
		
		*lcdt = local_dt;
	
		xSemaphoreGive( Time_and_date_MUTEX );
	}
	else
	{
		lcdt->__day = EPSON_DEFAULT_TD_DAY_OF_MONTH;
		lcdt->__dayofweek = EPSON_DEFAULT_TD_WEEKDAY;
		lcdt->__hours = EPSON_DEFAULT_TD_HOURS;
		lcdt->__minutes = EPSON_DEFAULT_TD_MINUTES;
		lcdt->__month = EPSON_DEFAULT_TD_MONTH;
		lcdt->__seconds = EPSON_DEFAULT_TD_SECONDS;
		lcdt->__year = EPSON_DEFAULT_TD_YEAR;
		lcdt->date_time.D = EPSON_DATE_DEFAULT;
		lcdt->date_time.T = EPSON_TIME_DEFAULT;
	}
}

/* ---------------------------------------------------------- */
void EPSON_obtain_latest_time_and_date( DATE_TIME *ldt )
{
	// Alert lines attempt to obtain the latest time & date. And it is easy to have code attempt an alert line too early in the startup
	// sequence. If the MUTEX is a NULL the xSemaphoreTake function invokes assert which in turn attempts another alert line = CRASH.
	// Well we don't want it to crash so at this level we should protect against the NULL mutex. Elsewhere in the alert generation we
	// protect against the pile not being ready.
	if( Time_and_date_MUTEX != NULL )
	{
		// Guard against the variable being modified while we are accessing it. This function can be called from any task within the application to
		// get the system time & date. Prevent the TDCheck task from writing to the local variable while we are reading it.
		xSemaphoreTake( Time_and_date_MUTEX, portMAX_DELAY );
		
		*ldt = local_dt.date_time;
	
		xSemaphoreGive( Time_and_date_MUTEX );
	}
	else
	{
		ldt->D = EPSON_DATE_DEFAULT;
		ldt->T = EPSON_TIME_DEFAULT;
	}
}

/* ---------------------------------------------------------- */
extern void EXCEPTION_USE_ONLY_obtain_latest_time_and_date( DATE_TIME *ldt )
{
	// 10/22/2012 rmd : During the processing of an exception (data abort, undefined instruc, or
	// prefetch abort) we are in an ISR like state. I don't think it is a good idea to attempt
	// to take the Time_and_date_MUTEX as that is a blocking call. Just grab the time & date.
	// Will be used in the alert line made upon startup.
	*ldt = local_dt.date_time;
}

/* ---------------------------------------------------------- */
// 12/03/2015 skc : This is a simplified time setting function. Returns true if the value is
// set, false if not set. Called form comm_mngr and keypress tasks, this is ok because the
// EPSON family runs in it's own taks with Mutexes
extern BOOL_32 EPSON_set_date_time( const DATE_TIME pdt, const UNS_32 preason )
{
	DATE_TIME	controller_dt;
	
	BOOL_32		set_d_and_t;
	
	DATE_TIME_COMPLETE_STRUCT	dtcs;
	
	UNS_32 lday, lmonth, lyear, ldow, lhour, lmin, lsec;
	
	// ----------
	
	// 4/13/2016 ajv : Grab the current date and time so the change line has the correct "FROM"
	// value
	EPSON_obtain_latest_time_and_date( &controller_dt );

	// ----------

	// 1/29/2016 rmd : Only when the date and time came in via the communications do we test for
	// the 30 second skew. When the user changes the date_time manually via the keypad all
	// clocks are set in the chain regardless of skew.
	if( preason == CHANGE_REASON_CENTRAL_OR_MOBILE )
	{
		// 6/9/2015 rmd : CAUTION we are assuming the time and date command from the commserver is
		// not sent around mid-night. The logic here only supports figuring the difference between
		// the message time and the controller time away from mid-night. The plan is to send the
		// time and date command at 4PM. So that the controller time is correct for upcoming 8PM
		// weather sharing activites.
		set_d_and_t = (false);
	
		if( controller_dt.D != pdt.D )
		{
			set_d_and_t = (true);
		}
		
		if( abs( controller_dt.T - pdt.T ) > 30 )
		{
			set_d_and_t = (true);
		}
	}
	else
	{
		// 1/29/2016 rmd : When from the KEYPAD we always set the date and time.
		set_d_and_t = (true);
	}

	// ----------
	
	if( set_d_and_t )
	{
		dtcs.date_time = pdt;
		
		DateToDMY( pdt.D, &lday, &lmonth, &lyear, &ldow );

		TimeToHMS( pdt.T, &lhour, &lmin, &lsec );

		dtcs.__day = lday;
		dtcs.__month = lmonth;
		dtcs.__year = lyear;
		dtcs.__dayofweek = ldow;
		dtcs.__hours = lhour;
		dtcs.__minutes = lmin;
		dtcs.__seconds = lsec;
		
		set_the_time_and_date( &dtcs );

		Alert_ChangeLine_Controller( CHANGE_DATE_TIME_CLOCK, FLOWSENSE_get_controller_index(), &controller_dt, &pdt, sizeof(DATE_TIME), preason, FLOWSENSE_get_controller_index() );
	}

	// ----------
	
	return( set_d_and_t );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

