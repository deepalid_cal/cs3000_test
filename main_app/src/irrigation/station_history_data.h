/*  file = report_data.h                      09.13.2011  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_STATION_HISTORY_DATA_H
#define _INC_STATION_HISTORY_DATA_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	"lpc_types.h"

#include	"foal_defs.h"

#include	"report_data.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*  STATION HISTORY STRUCTURE */
/* ---------------------------------------------------------- */

typedef union
{ 
	UNS_32	overall_size;

	struct
	{
		// NOTE: In GCC ARM this is the Least Significant Bit
		
		// -------------------------------

		// 6/4/2012 rmd : Used so we know if we've filled out the flow data within the line yet. If
		// the valve is involved in a flow error on it's first cycle that is not the data we want in
		// the line. Ideally we wait until either it fails its flow checking or it is involved in a
		// group that passes. But that may not be the first cycle of the night for either condition.
		// So we can't use the number_of_repeats as our indicator to assign or not. And instead of
		// initializing the flow reading to a magic number use this flag. We're talking about the pi
		// flow data.
		BITFIELD_BOOL		pi_flow_data_has_been_stamped					: 1;
		
		// -------------------------------
		
		BITFIELD_BOOL		controller_turned_off							: 1;

		BITFIELD_BOOL		hit_stop_time									: 1;

		BITFIELD_BOOL		stop_key_pressed								: 1;

		BITFIELD_BOOL		current_short									: 1;
		BITFIELD_BOOL		current_none									: 1;
		BITFIELD_BOOL		current_low										: 1;
		BITFIELD_BOOL		current_high									: 1;
		
		BITFIELD_BOOL		watersense_min_cycle_eliminated_a_cycle			: 1;

		// 7/6/2015 ajv : FOR NOW - we've repurposed this for WaterSense to indicate that the
		// runtime was too short (under 3 minutes).
		BITFIELD_BOOL		watersense_min_cycle_zeroed_the_irrigation_time	: 1;
		
		BITFIELD_BOOL		flow_low										: 1;
		BITFIELD_BOOL		flow_high										: 1;

		// 6/5/2012 rmd : This gets stamped only for the case of programmed irrigation if the system
		// is all set to check flow and the valve is too. But never does. We won't know the exact
		// reason. Could've have too short a cycle time (as compared to say the line fill time).
		// Flow may never have gone stable. And more.
		BITFIELD_BOOL		flow_never_checked								: 1;
		
		BITFIELD_BOOL		no_water_by_manual_prevented					: 1;
		BITFIELD_BOOL		no_water_by_calendar_prevented					: 1;

		BITFIELD_BOOL		mlb_prevented_or_curtailed						: 1;
		BITFIELD_BOOL		mvor_closed_prevented_or_curtailed				: 1;

		BITFIELD_BOOL		rain_as_negative_time_prevented_irrigation		: 1;
		BITFIELD_BOOL		rain_as_negative_time_reduced_irrigation		: 1;

		// 10/12/2012 rmd : At the start time either there was an R in the table, we had crossed the
		// minimum, or we were polled to. Or during irrigation we crossed the minimum or were polled
		// to. Any of those conditions set this flag.
		BITFIELD_BOOL		rain_table_R_M_or_Poll_prevented_or_curtailed	: 1;

		BITFIELD_BOOL		switch_rain_prevented_or_curtailed				: 1;
		BITFIELD_BOOL		switch_freeze_prevented_or_curtailed			: 1;
		BITFIELD_BOOL		wind_conditions_prevented_or_curtailed			: 1;
		
		BITFIELD_BOOL		mois_cause_cycle_skip							: 1;
		BITFIELD_BOOL		mois_max_water_day								: 1;
		
		// 8/27/2012 rmd : When a MV or PUMP short is reported by a TPMicro such a short happened at
		// a POC. That poc belongs to a system. All irrigation for that system is in turn cancelled.
		// Stations in the list for ProgrammedIrrigation at the time of the short have this flag set
		// when they are removed.
		BITFIELD_BOOL		poc_short_cancelled_irrigation					: 1;

		BITFIELD_BOOL		mow_day											: 1;
		

		// 11/1/2013 rmd : Even if this is not a two-wire station. When there is a two-wire cable
		// anomaly (excessive current or overheated terminal) we kill all irrigation for all systems
		// at all controllers. This bit indicates this station was enveloped in that process.
		BITFIELD_BOOL		two_wire_cable_problem							: 1;

		// ----------
		
		// 11/19/2014 rmd : Station history lines are closed and the rip initialized at that time.
		// If we were to show the rip BEFORE the next start time water day combination we would see
		// the ZEROED line. And it is confusing. We will set this flag when the rip is valid to
		// show. And that of course would be when the start time is crossed on a water day.
		BITFIELD_BOOL		rip_valid_to_show								: 1;

		// ----------

		BITFIELD_BOOL		two_wire_station_decoder_inoperative			: 1;

		BITFIELD_BOOL		two_wire_poc_decoder_inoperative				: 1;

		// ----------

		BITFIELD_BOOL		moisture_balance_prevented_irrigation			: 1;

		// ----------
		
		// 7/9/2015 ajv : Presently using ALL 32 bits out of the 32 available.

		// ----------
		
		// NOTE: This is the MS bit in GCC ARM.
	};

} STATION_HISTORY_BITFIELD;

// ----------

// 8/11/2016 rmd : The flags for PI_FLAG2, the second flag variable in the station history
// line.

// 7/13/2016 skc : Bitfield of budget reasons for a station irrigation. Only one of these
// two can be on at one time. If we reduce irrigation due to budgets use the first bit. If
// the reduction limit prevents full budget adjustment, enable the sceond bit, (disabling
// the first bit). THESE ARE BIT NUMBERS, NOT BIT MASKS.
#define STATION_history_bit_budget_applied	 			0x00

#define STATION_history_bit_budget_reduction_limit		0x01


// ----------

// 2011.09.16 rmd : We are going to manually pack this structure. That will result in not
// only the most efficient code but the most efficient storage. We could simply pack the
// structure and organize it any way we want. But the code to read and write the members of
// the structure will be bulky. I'll try it this way ... that is manually organizing the
// members to achieve the pack and code efficiency. The trade off is the structure appears
// dis-organized here in the definition. But only here in the definition.
typedef struct
{
	// ------------------------

	// 4/27/2012 rmd : The pi reference refers to 'programmed irrigation'.

	// ------------------------
	
	//  Time associated with record start. This is ALSO the programmed irrigation start time.
	//  BECAUSE we run these records from start time to start time. And only make a new one at
	//  the start time ON A WATERING DAY.
	UNS_32			record_start_time;

	// ------------------------

	//	Time stamp for the start of the first cycle.
	UNS_32			pi_first_cycle_start_time;

	//	Time stamp when the irrigation ended.
	UNS_32			pi_last_cycle_end_time;

	// ------------------------

	//	In seconds. A long. As a short is too ... short. May need to accomodate 24+ hours.
	UNS_32			pi_seconds_irrigated_ul;

	// ------------------------

	// We store it here as a float in order to accumulate fractional amounts of gallons on a
	// second by second basis. We will ship it to the central as an ul_100u.
	float			pi_gallons_irrigated_fl;
	
	// ------------------------

	// 7/20/2012 rmd : And Rodger and I agreed to no longer accumulate irrigated inches.
	// Somewhat related to this is that we are also doing away with the station square footage
	// as a user setting. We do enter a direct PR by station. And it is used to calculate the
	// run time when irrigating by ET. But accumulating irrigated inches using only time and the
	// PR could lead to some funny results. Especially say if there was no flow rate (alerts no
	// action for low flow) and we kept irrigating accumulating inches but no gallons.

	// ------------------------

	STATION_HISTORY_BITFIELD		pi_flag;

	// ------------------------

	// 6/28/2012 rmd : What group ids do we need to keep? Running reports about say the test use
	// for the turf stations belonging to a particular system over a period of time sounds
	// reasonable. So here they are.
	
	// 6/28/2012 rmd : The log lines need the system and the schedule GID's and that's all. The
	// station by station needs 'em all as you see below.

	UNS_16			GID_irrigation_system;

	UNS_16			GID_irrigation_schedule;

	// ------------------------
	
	//  Date associated with record start. This is ALSO the date of the programmed irrigation.
	//  BECAUSE we run these records from start time to start time. And only make a new one at
	//  the start time ON A WATERING DAY.
	UNS_16			record_start_date;			//	Date associated with record start. Should occur each and every day!

	// ------------------------

	UNS_16			pi_first_cycle_start_date;	//	Date stamp for the start of the first cycle
	
	UNS_16			pi_last_cycle_end_date;		//	Date stamp when the irrigation ended

	// ------------------------

	// We are storing the total requested run time as minutes_us_10u. First of all we do want it
	// a short to manage storage space. And at 10u will provide 6 seconds resolution.
	UNS_16			pi_total_requested_minutes_us_10u;

	// -----------------------------
	
	// 6/30/2015 ajv : Store the amount of time that the WaterSense equations indicate the
	// station should run. When the station history line closes, we'll compare this number to
	// pi_seconds_irrigated_ul and adjust the station's moisture balance based upon the lower of
	// the two. This ensures that the moisture balance isn't impacted by adjustment factors such
	// as station adjust (formerly percent of ET) and percent adjust.
	UNS_32			pi_watersense_requested_seconds;

	// -----------------------------
	
	// Kept as a positive number. But it does represent in a sense negative time. It is time
	// that we will not be irrigating!
	UNS_16			pi_rain_at_start_time_before_working_down__minutes_10u;

	UNS_16			pi_rain_at_start_time_after_working_down__minutes_10u;

	// ------------------------

	// 9/16/2013 ajv : Include the last measured current as a diagnostic tool.
	// Since this is stamped at the start time, it should not be considered
	// as the current for this irrigation, but rather what it was the last time
	// it ran, similar to rain minutes.
	UNS_16			pi_last_measured_current_ma;

	// -----------------------------
	
	// 6/28/2012 rmd : This group is stamped after the line fill, valve close, etc timers
	// expire. And the station is ready to check flow (or update the flow table).
	UNS_16			pi_flow_check_share_of_actual_gpm;
	
	UNS_16			pi_flow_check_share_of_hi_limit_gpm;
	
	UNS_16			pi_flow_check_share_of_lo_limit_gpm;

	// -----------------------------
	
	// 6/28/2012 rmd : From 0 to 127 representing station 1 through 128 at a box.
	UNS_8			station_number;
	
	UNS_8			pi_number_of_repeats;

	// 4/18/2014 rmd : This is the 0..11 index of the box the station belongs to.
	UNS_8			box_index_0;

	// 7/13/2016 skc : Take over unused 8 bits for budget data, we can't use a struct due to
	//packing; 
	UNS_8 pi_flag2; 

	// ----------
	
	// 9/15/2015 rmd : REV 3 additions.
	
	// ----------
	
	// 9/15/2015 rmd : For REV 3 AJ added as part of watersense these two. The second 2 bytes
	// are added by the compiler which he has explicitly shown as available for expansion.

	INT_16			pi_moisture_balance_percentage_after_schedule_completes_100u;

	UNS_16			expansion_u16;
	
	// ----------
	
	// 9/16/2013 ajv : The record size is 60 bytes at the moment.
	// 4/18/2014 rmd : Removed the UNS_32 serial_number. Added back in the UNS_8 box_index_0.
	// Size is still at 60. Meaning there are 3 unused bytes available.
	// 5/6/2014 rmd : Reduced measured current to 2 bytes from 4. And moved it within the
	// structure. This resulted in a record size of 56 bytes.
	// 7/17/2015 ajv : With the addition of the moisture balance percentage, we've now increase
	// the size of this structure to 60-bytes..
	
} STATION_HISTORY_RECORD;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// The storage of the completed records. This will be a structure. Stored in SDRAM. It is
// about 600 kbyte in size. The array of records contained within. And some variables which
// help know where to append the next completed record. Once full it will just keep wrapping
// around. After each start time, meaning a groupd of new records was added, we'll save the
// file.

// 1/23/2015 rmd : If the number of records kept is changed should update revision number
// else you will lose the file contents.


// 8/12/2015 rmd : For file versions 0 and 1 we kept 14 records per station.
#define STATION_HISTORY_IRRIGATIONS_KEPT_V0_V1	(14)

#define STATION_HISTORY_MAX_RECORDS_V0_V1	((MAX_STATIONS_PER_NETWORK) * STATION_HISTORY_IRRIGATIONS_KEPT_V0_V1)

// ----------

#define STATION_HISTORY_IRRIGATIONS_KEPT	(5)

#define STATION_HISTORY_MAX_RECORDS			((MAX_STATIONS_PER_NETWORK) * STATION_HISTORY_IRRIGATIONS_KEPT)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	REPORT_DATA_FILE_BASE_STRUCT	rdfb;

	// ----------
	
	// 8/1/2012 rmd : Remember the roll time for these records is when EACH individual station
	// completes its scheduled programmed irrgiation.
	STATION_HISTORY_RECORD	shr[ STATION_HISTORY_MAX_RECORDS ];

} COMPLETED_STATION_HISTORY_STRUCT;

extern COMPLETED_STATION_HISTORY_STRUCT		station_history_completed;	// Held in SDRAM and saved to a file in the GENERAL STORAGE flash device.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

// ---------------------

// 8/11/2015 rmd : The scheme is to start the station histroy file save timer when a record
// closes. If it is already running we do not want to restart it. This would push us towards
// the possibility of prolonging the file save to the point where the rip had to be reused
// for another program start time. If that occurred and then there were a power failure we
// would lose records. This could happen even if a program didn't run for 24 hours. A
// combination of run times, program starts from multiple programs, and power failures push
// us to say if the timer is running let it run.
//
// The second consideration when setting this time is look at the worst case of saving the
// file once every 2 hours. That's 12 times a day. Well with a 100K flash endurance we could
// do that for 22+ years. And that's the worst case. It takes a very abnormal program build
// up to cause that to happen. Even for one week. Much less 22 years.
//
// So I've settled on a two hour file save timer. REMEMBER this whole scheme of marking the
// rips as part of the not yet saved file and stretching the time between saves a bit came
// about because the prior scheme saved the station history file every time a record closed.
// We were seeing dozens of saves per day. And that could wear out the flash. So moving to
// once per 2 hours max is a big step forward and covers the flash wear limit.
#define		STATION_HISTORY_FILE_WRITE_DELAY_SECONDS		(2 * 3600)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void nm_init_station_history_record( STATION_HISTORY_RECORD *pshr_ptr );

extern void init_file_station_history( void );

extern void save_file_station_history( void );

extern void STATION_HISTORY_start_ci_timer_if_it_is_not_running__seconds( UNS_32 const ptimer_seconds );

extern void nm_STATION_HISTORY_inc_index( UNS_32 *pindex_ptr );

extern void nm_STATION_HISTORY_increment_next_avail_ptr( void );

// ---------------------

extern UNS_32 STATION_HISTORY_fill_ptrs_and_return_how_many_lines( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0 );

extern void STATION_HISTORY_draw_scroll_line( const INT_16 pline_index_0_i16 );

extern float STATION_HISTORY_get_rain_min( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0 );

extern void STATION_HISTORY_set_rain_min_10u( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, const UNS_32 pvalue_10u, const UNS_8 pwho_initiated_it );

// ---------------------

extern void STATION_HISTORY_generate_lines_for_DEBUG( void );

// ---------------------

extern STATION_HISTORY_RECORD *nm_STATION_HISTORY_get_most_recently_completed_record( void );

extern STATION_HISTORY_RECORD *nm_STATION_HISTORY_get_previous_completed_record( STATION_HISTORY_RECORD *pshr_ptr );

extern void nm_STATION_HISTORY_close_and_start_a_new_record( UNS_32 const pstation_index );

// ---------------------

extern void STATION_HISTORY_free_report_support( void );

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

