/*  file = dynamic_manifold_recorder.c  05/21/08  ajv  */

/* ---------------------------------- */
/* ---------------------------------- */

#include	"etcommon.h"

#include	"cmos.h"

#include	"mem.h"

#include	"dynamic_manifold.h"

#include	"dynamic_manifold_recorder.h"

// Used for debugging
// 
/*
#include	"string.h"
#include	"tdutils.h"
#include	"uport_c.h"
*/

/* ---------------------------------- */
/* ---------------------------------- */

DMC_RECORDER_CONTROL_STRUCT dmc_rcs;

/* ---------------------------------- */
/* ---------------------------------- */

// Used for debugging
// 
/*
void stream_dmc_recorder_data_to_frontport( NEW_DMC_RECORDER_RECORD *dmcrr ) {

char str_128[ 128 ], ldate_str_32[ 32 ], ltime_str_32[ 32 ];

DATA_HANDLE dh;

	if ( dmc_rcs.allocated == TRUE ) {

		str_128[ 0 ] = 0x00;
		sp_strcat( str_128, "%s", GetDateStr( ldate_str_32, dmcrr->dt.D, DATESTR_include_year, DATESTR_short_year_not, DATESTR_show_dow_not ) );
		sp_strcat( str_128, " " );
		sp_strcat( str_128, "%s", TDUTILS_time_to_time_string_with_ampm( dmcrr->dt.T, ltime_str_32, FALSE, TRUE, FALSE ), 0x08, 0x08 );
	
		sp_strcat( str_128, " - " );
	
		sp_strcat( str_128, "%d %d %d %d", dmcrr->level_1_mv_state_uc, dmcrr->level_2_mv_state_uc, dmcrr->level_1_avg_flow_us, dmcrr->level_2_avg_flow_us );

		sp_strcat( str_128, "%c%c", 0x0A, 0x0D );
	
		dh.dlen = strlen( str_128 );
		dh.dptr = (unsigned char*)str_128;
	
		C_AddCopyOfBlockToXmitList( dh );

	}

}
*/
/* ---------------------------------- */
/* ---------------------------------- */

void init_dmc_recorder( void ) {

	// on startup this runs
	//
	// Note that Flow Recording does not allocate space for itself on startup - it does so on a 1 Hz
	// basis to ensure that the memory is freed if the user unchecks the CMOS bit to disable Flow
	// Recording.
	// 
	// We're not concerned about that here, so we're just going to grab our 8k block and be done with it
	//
	dmc_rcs.allocated = TRUE;

	dmc_rcs.original_allocation = mem_malloc( DMC_RECORDER_MAX_RECORDS * sizeof( NEW_DMC_RECORDER_RECORD ) );

	// start the pointers at the beginning
	//
	dmc_rcs.first_used = dmc_rcs.original_allocation;

	dmc_rcs.next_available = dmc_rcs.original_allocation;

	// and 0 out the memory for good luck
	//
	memset( dmc_rcs.original_allocation, 0x00, DMC_RECORDER_MAX_RECORDS * sizeof( NEW_DMC_RECORDER_RECORD ) );

}

/* ---------------------------------- */
/* ---------------------------------- */

void dmc_recorder_inc_pointer( unsigned char **ppdmcrr ) {

	//
	// increment the pointer while staying within the bounds of the allocated space
	//

	*ppdmcrr += sizeof( NEW_DMC_RECORDER_RECORD );

	if ( *ppdmcrr >= dmc_rcs.original_allocation + ( DMC_RECORDER_MAX_RECORDS * sizeof( NEW_DMC_RECORDER_RECORD ) ) ) {

		*ppdmcrr = dmc_rcs.original_allocation;

	}

}

/* ---------------------------------- */
/* ---------------------------------- */

void dmc_recorder_add( unsigned char pnum_stations_on,
					   unsigned char plevel_1_mv_state_uc,
					   unsigned char plevel_2_mv_state_uc,
					   unsigned char plevel_3_mv_state_uc,
					   float plevel_1_avg_flow_fl,
					   float plevel_2_avg_flow_fl,
					   float plevel_3_avg_flow_fl,
					   unsigned short plevel_1_trip_point_us,
					   unsigned short plevel_2_trip_point_us,
					   unsigned short ptrip_point_up_us,
					   unsigned short ptrip_point_down_us,
					   unsigned short psystem_flowrate_us,
					   unsigned short pexpected_flowrate_us ) {

	NEW_DMC_RECORDER_RECORD *dmcrr;

	if ( dmc_rcs.allocated == TRUE ) {

		dmcrr = (NEW_DMC_RECORDER_RECORD*)dmc_rcs.next_available;

		dmcrr->dt = Gl_DateTime;

		dmcrr->num_stations_on_uc = pnum_stations_on;

		dmcrr->level_1_mv_state_uc = plevel_1_mv_state_uc;
		dmcrr->level_2_mv_state_uc = plevel_2_mv_state_uc;

		if ( DMC.levels_uc == 3 ) {

			dmcrr->level_3_mv_state_uc = plevel_3_mv_state_uc;

		} else {

			dmcrr->level_3_mv_state_uc = MV_STATE_not_in_use;

		}

		dmcrr->level_1_avg_flow_us = (unsigned short)plevel_1_avg_flow_fl;
		dmcrr->level_2_avg_flow_us = (unsigned short)plevel_2_avg_flow_fl;

		if ( DMC.levels_uc == 3 ) {

			dmcrr->level_3_avg_flow_us = (unsigned short)plevel_3_avg_flow_fl;

		} else {

			dmcrr->level_3_avg_flow_us = 0;

		}

		dmcrr->level_1_trip_point_us = plevel_1_trip_point_us;

		if ( DMC.levels_uc == 3 ) {

			dmcrr->level_2_trip_point_us = plevel_2_trip_point_us;

		} else {

			dmcrr->level_2_trip_point_us = 0;

		}

		dmcrr->trip_point_up_us = ptrip_point_up_us;
		dmcrr->trip_point_down_us = ptrip_point_down_us;

		dmcrr->system_flowrate_us = psystem_flowrate_us;

		dmcrr->expected_flowrate_us = pexpected_flowrate_us;

		// Used for debugging
		// 
		// stream_dmc_recorder_data_to_frontport( dmcrr );

		dmc_recorder_inc_pointer( &dmc_rcs.next_available );

		// the only time the first_used pointer is equal to the next_available pointer is when the table
		// is first created...once a record is in the table we cannot leave this function with these two
		// pointers equal else the send to the central won't work...once the table is full always keep the
		// first_used one ahead of the next_available...this logic is simple...in order to keep it simple
		// however we sacrifice one slot...but that's okay
		//
		if ( dmc_rcs.next_available == dmc_rcs.first_used ) {

			dmc_recorder_inc_pointer( &dmc_rcs.first_used );

		}

	}

}

/* ---------------------------------- */
/* ---------------------------------- */
/* ---------------------------------- */
/* ---------------------------------- */

