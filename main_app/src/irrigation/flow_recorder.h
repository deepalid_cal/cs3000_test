/*  file = flow_recorder.h          10/27/00 2011.08.08  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/27/2012 rmd : Update to the CS3000 environment.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_FLOW_RECORDER_H
#define _INC_FLOW_RECORDER_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	"lpc_types.h"

#include	"cal_td_utils.h"

// 6/26/2014 ajv : Required to access timers.h
#include	"cs_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define FLOW_RECORDER_MAX_RECORDS			 	(1600)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/1/2012 rmd : Flow Recorder Flag (FRF) for the flow checking results.
#define FRF_CHECK_STATUS_cleared				(0)
#define FRF_CHECK_STATUS_passed					(1)
#define FRF_CHECK_STATUS_hi_flow				(2)
#define FRF_CHECK_STATUS_lo_flow				(3)
// 5/31/2012 rmd : And for this case see the reason flag(s).
#define FRF_CHECK_STATUS_not_going_to_perform	(4)

typedef union
{
	// 5/1/2012 rmd : Note - the minimum size using this union construct appears to be 4 bytes.
	// Unless we use the packed attribute.
	UNS_16	overall_size;

	struct
	{
		// NOTE: In GCC this is the Least Significant Bit (Little Endian)

		/* ---------------------------------- */

		// 5/31/2012 rmd : I've expanded this from what was 2-bits in the 2000e to 3 bits here in
		// the CS3000. So we could be more clear with the flag settings. There was no cleared
		// setting before for starters.
		UNS_32		flow_check_status										: 3;

		/* ---------------------------------- */

		BITFIELD_BOOL		FRF_NO_CHECK_REASON_indicies_out_of_range				: 1;
		BITFIELD_BOOL		FRF_NO_CHECK_REASON_station_cycles_too_low				: 1;
		BITFIELD_BOOL		FRF_NO_CHECK_REASON_cell_iterations_too_low				: 1;
		BITFIELD_BOOL		FRF_NO_CHECK_REASON_no_flow_meter						: 1;
		BITFIELD_BOOL		FRF_NO_CHECK_REASON_not_enabled_by_user_setting			: 1;

		BITFIELD_BOOL		FRF_NO_CHECK_REASON_acquiring_expected					: 1;
		BITFIELD_BOOL		FRF_NO_CHECK_REASON_combo_not_allowed_to_check			: 1;
		BITFIELD_BOOL		FRF_NO_CHECK_REASON_unstable_flow						: 1;
		BITFIELD_BOOL		FRF_NO_CHECK_REASON_cycle_time_too_short				: 1;
		BITFIELD_BOOL		FRF_NO_CHECK_REASON_reboot_or_chain_went_down			: 1;
		BITFIELD_BOOL		FRF_NO_CHECK_REASON_not_supposed_to_check				: 1;

		BITFIELD_BOOL		FRF_NO_UPDATE_REASON_zero_flow_rate						: 1;
		BITFIELD_BOOL		FRF_NO_UPDATE_REASON_thirty_percent						: 1;

		/* ---------------------------------- */

		// NOTE: This is the MS bit in GCC.

	} __attribute__((packed));  // this packed keeps the bit-field struct at 2-bytes - instead of 4

} __attribute__((packed)) FLOW_RECORDER_FLAG_BIT_FIELD;  // this packed keeps the union at 2-bytes - instead of 4

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	DATE_TIME	dt;

	// 4/27/2012 rmd : The location of the station_num UNS_16 after the DATE_TIME is efficient
	// use of space. As the UNS_32 controller_ser_num variable will be aligned to a 4 byte
	// boundary by the compiler.
	UNS_16		station_num;

	// 4/18/2014 rmd : Have done away with the controller serial number. Now only use the box
	// index (0..11) instead. In doing so reduced the size of this variable from 4 bytes to 2.
	UNS_16		box_index_0;

	// ---------------------------

	UNS_16		expected_flow;

	UNS_16		derated_expected_flow;

	UNS_16		hi_limit;

	UNS_16		lo_limit;

	UNS_16		portion_of_actual;

	// ---------------------------

	FLOW_RECORDER_FLAG_BIT_FIELD	flag;

	// 4/27/2012 rmd : The whole structure size (like any structure) will be sized as a multiple
	// of 4 bytes. And this structure size is 24 bytes. Every byte (of size) is important as
	// there are 1600 flow recording records per system.

} CS3000_FLOW_RECORDER_RECORD;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/21/2013 rmd : Send 15 minutes after a record is made. If more records are made within
// that 15 minute period they'll be sent too.
//
// 6/11/2015 rmd : Updated to one hour to manange the number of transmissions during
// irrigation.
#define		CONTROLLER_INITIATED_FLOW_RECORDING_TRANSMISSION_TIMER_MS		(60*60*1000)

typedef struct
{
	// ----------

	// 10/21/2013 rmd : The start of the allocated memory for the records.
	UNS_8		*original_allocation;

	// ----------

	// 10/21/2013 rmd : The memory location for the next line to be built.
	UNS_8		*next_available;

	// ----------

	// 10/23/2013 rmd : For display support only.
	UNS_8		*first_to_display;

	// ----------

	// 10/21/2013 rmd : The first line to send to the comm_server.
	UNS_8		*first_to_send;
	
	// 10/21/2013 rmd : During the communication exchange a copy of the PENDING first pointer to
	// send is kept. Until the reponse is rcvd. Only then do we update the first_to_send with
	// the pending_first_to_send. We take this approach for two reasons. First is until the
	// COMM_SERVER acknowledges us we do not want to move our pointer regarding lines sent. And
	// secondly during the potentially time consuming comm_server exchange the pile is live.
	// Records may be added to the pile. So we need a copy of where we got to when we sent the
	// records. When we receieve the ACK from the comm_server can't assume all records that
	// exist have been sent.
	UNS_8		*pending_first_to_send;

	// 10/21/2013 rmd : At times flow recording lines can be made quite frequently. To protect
	// against the flow recording timer firing again, after it just fired and made an outgoing
	// message, but before the communication transaction had completed we set this flag which
	// blocks the creation of another controller initiated message before any prior created
	// messages have either been sent. Or the response timer expired. BUT WAIT THERE'S MORE.
	// BECAUSE of the complication of having 4 flow recording arrays, one per system, there is a
	// problem knowing, within the controller initiated task, which flag to clear if we don't
	// receive a response. So we always clear all 4 then. And when a flow recording timer fires,
	// all 4 flags must be cleared in order to build a new flow recording transmission.
	BOOL_32		pending_first_to_send_in_use;

	// ----------
	
	// 10/21/2013 rmd : And the controller initiated timer. Being that flow recording is a
	// by system entity, one timer per system.
	xTimerHandle	when_to_send_timer;
	
	// ----------
	
} FLOW_RECORDING_CONTROL_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void nm_flow_recorder_inc_pointer( UNS_8 **ppfrr, UNS_8 const (*const pstart_of_block) );

extern void nm_flow_recorder_add( void *pilc_ptr, UNS_16 pderated_expected_flow, UNS_16 phi_limit, UNS_16 plo_limit, UNS_16 pportion_of_actual, FLOW_RECORDER_FLAG_BIT_FIELD pflag );


extern void FLOW_RECORDING_generate_lines_for_DEBUG( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

