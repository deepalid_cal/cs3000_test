/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"moisture_sensor_recorder.h"

#include	"epson_rx_8025sa.h"

#include	"foal_irri.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"change.h"

#include	"controller_initiated.h"

#include	"configuration_controller.h"

#include	"cs_mem.h"

#include	"d_process.h"

#include	"moisture_sensors.h"



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/17/2016 rmd : Allocation for the moisture sensor recorder control structure.
MOISTURE_SENSOR_RECORDER_CONTROL_STRUCT	msrcs;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void ci_moisture_sensor_recorder_timer_callback( xTimerHandle pxTimer )
{
	// 10/21/2013 rmd : Remember this is executed within the context of the high priority timer
	// task. And is executed when it it time to send flow recording. Which is normally 15
	// minutes after a line is made.
	CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_moisture_sensor_recorder_records, NULL, CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD, CI_POST_TO_BACK );
}

/* ---------------------------------------------------------- */
static void nm_moisture_sensor_recorder_verify_allocation( void )
{
	// 2/17/2016 rmd : Call prior to making a moisture recorder line. Remember the moisture
	// recording memory is on the heap and therefore does not survive through a power fail. This
	// function ensures allocation.
	//
	// 2/17/2016 rmd : Caller to be holding the moisture_sensor_recoder_MUTEX.
	
	// ----------
	
	if( msrcs.original_allocation == NULL )
	{
		msrcs.original_allocation = mem_malloc( MOISTURE_SENSOR_RECORDER_MAX_RECORDS * sizeof( MOISTURE_SENSOR_RECORDER_RECORD ) );

		// Start all the pointers at the beginning.
		msrcs.next_available = msrcs.original_allocation;

		msrcs.first_to_display = msrcs.original_allocation;

		msrcs.first_to_send = msrcs.original_allocation;

		msrcs.pending_first_to_send = msrcs.original_allocation;
		
		// 10/28/2015 rmd : No message can be underway yet. And this should clearly be set false to
		// begin with otherwise a msg will never be created and sent.
		msrcs.pending_first_to_send_in_use = (false);
		
		// ----------
	
		// 10/21/2013 rmd : Create the flow recording timer to manage sending to the
		// comm_server.
		msrcs.when_to_send_timer = xTimerCreate( (const signed char*)"", MS_to_TICKS(CI_MOISTURE_SENSOR_RECORDER_TRANSMISSION_TIMER_MS), FALSE, NULL, ci_moisture_sensor_recorder_timer_callback );
			
		if( msrcs.when_to_send_timer == NULL )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "Timer NOT CREATED" );
		}
	}
}

/* ---------------------------------------------------------- */
extern void nm_MOISTURE_SENSOR_RECORDER_inc_pointer( UNS_8 **ppmsrr, UNS_8 const (*const pstart_of_block) )
{
	// 2/17/2016 rmd : Caller to be holding the moisture_sensor_recoder_MUTEX.
	
	// ----------
	
	*ppmsrr += sizeof( MOISTURE_SENSOR_RECORDER_RECORD );

	if( *ppmsrr >= pstart_of_block + (MOISTURE_SENSOR_RECORDER_MAX_RECORDS * sizeof(MOISTURE_SENSOR_RECORDER_RECORD)) )
	{
		*ppmsrr = (UNS_8*)pstart_of_block;
	}
}

/* ---------------------------------------------------------- */
extern void MOISTURE_SENSOR_RECORDER_add_a_record( void *pmois )
{
	// 2/18/2016 rmd : The pmois paramter is to be a pointer to a MOISTURE_SENSOR_GROUP_STRUCT
	// record in the moisture sensor file. The assumption is the new reading data is in place in
	// that record. Using a GET function we'll get a copy of it for the recorder record.
	
	// ----------

	MOISTURE_SENSOR_RECORDER_RECORD		*msrr;
	
	msrr = pmois;
	
	// ----------
	
	xSemaphoreTakeRecursive( moisture_sensor_recorder_recursive_MUTEX, portMAX_DELAY );

	nm_moisture_sensor_recorder_verify_allocation();

	// ----------
	
	msrr = ((MOISTURE_SENSOR_RECORDER_RECORD*)(msrcs.next_available));

	MOISTURE_SENSOR_fill_out_recorder_record( pmois, msrr );
	
	EPSON_obtain_latest_time_and_date( &(msrr->dt) );
	
	// ----------
	
	// 10/21/2013 rmd : Move ahead to the next available slot.
	nm_MOISTURE_SENSOR_RECORDER_inc_pointer( &(msrcs.next_available), msrcs.original_allocation );

	// ----------

	// 10/23/2013 rmd : If we've wrapped around bump ahead the first to display. Yes this
	// crushes a line before it's time (as the line is still valid until a new one is made) but
	// I think it keep the logic simple about how many lines are in the pile to show. At the
	// sacrafice of 1 line. And that's fine.
	if( msrcs.next_available == msrcs.first_to_display )
	{
		nm_MOISTURE_SENSOR_RECORDER_inc_pointer( &(msrcs.first_to_display), msrcs.original_allocation );
	}
	
	// ----------

	// 10/21/2013 rmd : And check if we are now pointing to the first one to send. If so the
	// first to send needs to be moved. Technically it doesn't need to move until a new line is
	// made at it's location. BUT I think it makes the code more simple to move it now. The
	// effect is as if the buffer was smaller by 1 less line. But that's okay.
	if( msrcs.next_available == msrcs.first_to_send )
	{
		nm_MOISTURE_SENSOR_RECORDER_inc_pointer( &(msrcs.first_to_send), msrcs.original_allocation );
	}

	// 10/21/2013 rmd : Also if next available has landed on the pending_first_to_send it means
	// that during the CI session we have actually written so many lines we wrapped all the way
	// around. In other words we wrote more lines than we hold during the session. So move that
	// pointer too.
	if( msrcs.next_available == msrcs.pending_first_to_send )
	{
		nm_MOISTURE_SENSOR_RECORDER_inc_pointer( &(msrcs.pending_first_to_send), msrcs.original_allocation );
	}

	// ----------

	// 8/12/2014 rmd : If the timer is not running go ahead and start it. If it is running leave
	// alone. The effect of this is that within a maximum of (the timer) minutes from the time a
	// record is added it will be sent. This means for the case of moisture every (timer
	// minutes) moisture recording will be sent to the comm_server_app.
	if( xTimerIsTimerActive( msrcs.when_to_send_timer ) == pdFALSE )
	{
		// 10/23/2013 rmd : Start the timer to trigger the controller initiated send.
		//
		// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
		// when posting is safe. Anything on the timer queue is quickly processed and therefore the
		// queue is effectively always empty.
		xTimerChangePeriod( msrcs.when_to_send_timer, MS_to_TICKS( CI_MOISTURE_SENSOR_RECORDER_TRANSMISSION_TIMER_MS ), portMAX_DELAY );
	}

	// ----------

	xSemaphoreGiveRecursive( moisture_sensor_recorder_recursive_MUTEX );
	
	// ----------
	
	/*
	for reference - from flow recording

	// 10/29/2013 ajv : If the user is viewing the Flow Recording report
	// when a new line is added, force the scroll box to be updated.
	if( GuiLib_CurStructureNdx == GuiStruct_rptFlowRecording_0 )
	{
		DISPLAY_EVENT_STRUCT	lde;

		lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
		lde._04_func_ptr = &FDTO_FLOW_RECORDING_redraw_scrollbox;
		Display_Post_Command( &lde );
	}
	*/
	
}

/* ---------------------------------------------------------- */

#if ALERTS_ABILITY_TO_GENERATE_ALL_ALERTS_AND_REPORTS_FOR_DEBUG

extern void MOISTURE_SENSOR_RECORDER_generate_lines_for_DEBUG( void )
{
/*
	still setup for flow recording - not moisture recorder



	UNS_32	lll;
	
	CS3000_FLOW_RECORDER_RECORD		*frr;

	BY_SYSTEM_RECORD				*bsr;

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	bsr = &(system_preserves.system[0]);

	_nm_flow_recorder_verify_allocation( bsr );

	for( lll=0; lll<FLOW_RECORDER_MAX_RECORDS-1; lll++ )
	{
		frr = (CS3000_FLOW_RECORDER_RECORD*)bsr->frcs.next_available;

		EPSON_obtain_latest_time_and_date( &(frr->dt) );

		frr->station_num = (lll % 128);
		frr->box_index_0 = 0;

		frr->expected_flow = (lll % 100);
		frr->derated_expected_flow = (lll % 200);

		frr->hi_limit = (lll % 300);
		frr->lo_limit = (lll % 400);

		frr->portion_of_actual = (lll % 500);

		frr->flag.overall_size = 0;
		// ----------
		
		// 10/21/2013 rmd : Move ahead to the next available slot.
		nm_flow_recorder_inc_pointer( &(bsr->frcs.next_available), bsr->frcs.original_allocation );

		// ----------

		// 10/23/2013 rmd : If we've wrapped around bump ahead the first to display. Yes this
		// crushes a line before it's time (as the line is still valid until a new one is made) but
		// I think it keep the logic simple about how many lines are in the pile to show. At the
		// sacrafice of 1 line. And that's fine.
		if( bsr->frcs.next_available == bsr->frcs.first_to_display )
		{
			nm_flow_recorder_inc_pointer( &(bsr->frcs.first_to_display), bsr->frcs.original_allocation );
		}
		
		// ----------

		// 10/21/2013 rmd : And check if we are now pointing to the first one to send. If so the
		// first to send needs to be moved. Technically it doesn't need to move until a new line is
		// made at it's location. BUT I think it makes the code more simple to move it now. The
		// effect is as if the buffer was smaller by 1 less line. But that's okay.
		if( bsr->frcs.next_available == bsr->frcs.first_to_send )
		{
			nm_flow_recorder_inc_pointer( &(bsr->frcs.first_to_send), bsr->frcs.original_allocation );
		}

		// 10/21/2013 rmd : Also if next available has landed on the pending_first_to_send it means
		// that during the exchange we have actually written so many lines we wrapped all the way
		// around. In other words more lines than we hold. So move that pointer too.
		if( bsr->frcs.next_available == bsr->frcs.pending_first_to_send )
		{
			nm_flow_recorder_inc_pointer( &(bsr->frcs.pending_first_to_send), bsr->frcs.original_allocation );
		}

	}

	// ----------

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
*/
}

#endif

/* ---------------------------------------------------------- */
/*
extern void set_flow_recording_timer_to_15_seconds( void )
{
	BY_SYSTEM_RECORD				*bsr;

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	bsr = &(system_preserves.system[0]);

	_nm_flow_recorder_verify_allocation( bsr );

	// ----------

	// 10/23/2013 rmd : Start the timer to trigger the controller initiated send.
	//
	// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
	// when posting is safe. Anything on the timer queue is quickly processed and therefore the
	// queue is effectively always empty.
	xTimerChangePeriod( bsr->frcs.when_to_send_timer, MS_to_TICKS( 15*1000 ), portMAX_DELAY );

	// ----------

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}
*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

