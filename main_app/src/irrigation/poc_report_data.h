/*  file = poc_report_data.h                  09.13.2011  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_POC_REPORT_DATA_H
#define _INC_POC_REPORT_DATA_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"battery_backed_vars.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/30/2012 rmd : Let's see how this goes. Suppose we set this up so that for the case of a
// single POC we keep more than 1 years worth of records. Remember they are stored in a
// single file and shared amongst the number of pocs the NETWORK has. So a dual poc network
// get half as many days of record keeping for each poc. And of course a 12 poc network is
// worst off. But with 400 records will still keep at least one months worth of records.
#define POC_REPORT_RECORDS_TO_KEEP		(400)

// -----------------------------

typedef struct
{
	REPORT_DATA_FILE_BASE_STRUCT	rdfb;

	// ----------
	
	POC_REPORT_RECORD				prr[ POC_REPORT_RECORDS_TO_KEEP ];

} COMPLETED_POC_REPORT_RECORDS_STRUCT;

// Held in SDRAM and saved to a file in the GENERAL STORAGE flash device.
extern COMPLETED_POC_REPORT_RECORDS_STRUCT	poc_report_data_completed;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// ---------------------

extern void init_file_poc_report_records( void );

extern void save_file_poc_report_records( void );

// ---------------------

extern POC_REPORT_RECORD *nm_POC_REPORT_RECORDS_get_previous_completed_record( POC_REPORT_RECORD *pprr_ptr );

extern POC_REPORT_RECORD *nm_POC_REPORT_RECORDS_get_most_recently_completed_record( void );

// ---------------------

extern UNS_32 FDTO_POC_REPORT_fill_ptrs_and_return_how_many_lines( const UNS_32 plist_index );

extern void FDTO_POC_REPORT_load_guivars_for_scroll_line( const INT_16 pline_index_0_i16 );

// ---------------------

extern void POC_REPORT_DATA_start_the_ci_timer_if_it_is_not_running( void );

extern void nm_POC_REPORT_DATA_inc_index( UNS_32 *pindex_ptr );

extern void nm_POC_REPORT_DATA_close_and_start_a_new_record( BY_POC_RECORD *pbpr_ptr, const DATE_TIME *pdate_time );

extern void POC_REPORT_DATA_generate_lines_for_DEBUG( void );

// ---------------------

extern void POC_REPORT_free_report_support( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

