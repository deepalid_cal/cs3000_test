/*  file = battery_backed_vars.c             2011.08.01  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"battery_backed_vars.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/5/2015 rmd : Some HISTORICAL NOTES follow first below. There used to be both the
// variable definitions and code with this battery_backed_vars.c file. We separated that
// today. Creating the batter_backed _funcs.c file. I've left the historical notes for
// reference.

// 10/19/2012 rmd : A note on code optimisation - the release code is set to optimize for
// size. The compiler has the liberty to and apparently does re-order the battery backed
// variable memory map with optimisation. So when us poor engineers move from the debug
// version to the release version we may notice all or some of the battery backed vars will
// be initiazed because they fail their pre/post string test. I looked into preventing this
// and as of this date have not found a desirable means of preventing this. It really isn't
// a field problem. More of an engineering inconvience. To COMBAT this problem I have set
// this file to ALWAYS optimize. See the following pragma. If you must debug code within you
// likely will want to temporarily comment out this pragma.

// 10/25/2012 rmd : DOES NOT WORK. VARIABLES STILL RE-ORDERED BETWEEN DEBUG AND RELEASE
// VERSIONS!

//#pragma GCC optimize( "Os" )

// 10/26/2012 rmd : OKAY. I found something that works. Set THIS file to be optimized for
// SIZE at the FILE LEVEL in ROWLEY CROSSWORKS. This is the same optimization level used for
// the relase code. And setting it in ROWLEY seems to make the difference. The battery
// backed variables between release and debug version are now in the same locations. If you
// need to step through this code you may need to temporarily change this file level
// property to level 0 (no optimization).

// 8/5/2015 rmd : Notes as of this day. This file contains only the variable declarations
// for the variables placed into the battery backed SRAM section. The discussion above
// regarding optimization still holds. However things are better than before. Before we
// forced the optimisation to 'for size' because there was code in the file and I wasn't
// willing to take the code size hit associated with turning off the opmization. Now there
// is no code so we can turn off the optimization, not take a hit to code size, and have
// absolute control over the order of variables within the batter backed sram. And the order
// is of concern. We want to maintain the same order between code updates. Else you will
// trigger a reinit of some or all of the battery backed vars. Potentially losing various
// report data, your alerts, and potentially any on-going irrigation at the time of the
// update. If you allow optimization the variable order in memory is not what you might
// guess.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/5/2015 rmd : RULES RULES RULES RULES RULES !!!!!!!!!!!!!!
//
//  1) Do not change the order of the variable declarations. Unless you know what you are
//  doing.
//
//  2) Make sure optimization is turned OFF in the crossworks project for this file.
//
//	3) Add new variables to the end of this file.
//
//  4) If you change the definition and/or size of an exisiting variable you must change
//     the PRE string in order to cause re-initialization of that structure.

// ----------

// 8/5/2015 rmd : Today we placed the variables in an order we think will allow changes to
// the variables most likely to change. While triggering a re-init to the least number of
// variables. An order that allows changes making the least amount of disruption. This is
// the order you'll find below.

// 9/29/2015 ajv : Note because optimization is turned off, the declaration order is the
// order in memory.

// ----------

UNS_8 alerts_pile_engineering[ ENGINEERING_PILE_BYTES ] __attribute__( BATTERY_BACKED_VARS );
UNS_8 alerts_pile_tp_micro[ TPMICRO_ALERTS_PILE_BYTES ] __attribute__( BATTERY_BACKED_VARS );
UNS_8 alerts_pile_changes[ ALERT_PILE_BYTES ] __attribute__( BATTERY_BACKED_VARS );
UNS_8 alerts_pile_user[ ALERT_PILE_BYTES ] __attribute__( BATTERY_BACKED_VARS );

ALERTS_PILE_STRUCT alerts_struct_engineering __attribute__( BATTERY_BACKED_VARS );
ALERTS_PILE_STRUCT alerts_struct_tp_micro __attribute__( BATTERY_BACKED_VARS );
ALERTS_PILE_STRUCT alerts_struct_changes __attribute__( BATTERY_BACKED_VARS );
ALERTS_PILE_STRUCT alerts_struct_user __attribute__( BATTERY_BACKED_VARS );

// ----------

// 10/16/2012 rmd : The init function for the restart_info is kept in wdt_and_powerfail.c.
RESTART_INFORMATION_STRUCT			restart_info __attribute__(BATTERY_BACKED_VARS);

// ----------

FOAL_IRRI_BB_STRUCT					foal_irri __attribute__(BATTERY_BACKED_VARS);

// ----------

GENERAL_USE_BB_STRUCT				battery_backed_general_use __attribute__(BATTERY_BACKED_VARS);

// ----------

WEATHER_PRESERVES_STRUCT			weather_preserves __attribute__( BATTERY_BACKED_VARS );

// ----------

POC_BB_STRUCT						poc_preserves __attribute__( BATTERY_BACKED_VARS );

// ----------

SYSTEM_BB_STRUCT					system_preserves __attribute__( BATTERY_BACKED_VARS );

// ----------

STATION_PRESERVES_STRUCT			station_preserves __attribute__( BATTERY_BACKED_VARS );

// ----------

// 10/1/2012 rmd : After scanning, this structure becomes valid at all controllers in a
// chain. It is used to recognize when the chain definition has changed. Which is why it is
// battery backed. It is protected by chain_members_recursive_MUTEX and all accesses should
// be managed within the MUTEX.
CHAIN_MEMBERS_STRUCT 				chain __attribute__(BATTERY_BACKED_VARS);

// ----------

LIGHTS_BB_STRUCT					lights_preserves __attribute__( BATTERY_BACKED_VARS );

FOAL_LIGHTS_BATTERY_BACKED_STRUCT	foal_lights __attribute__(BATTERY_BACKED_VARS);

// ----------

// 11/21/2014 rmd : Being battery backed variables DO NOT move them to another file. DO NOT
// change their location within this file. DO NOT change their location relative to one
// another. Any changes will change their location within the battery backed section.
// Leading to all battery backed variables initialized upon a code update.

// ----------

// 8/5/2015 rmd : NEW variables are added here!

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

