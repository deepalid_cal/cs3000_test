/*   file = report_data.c                    08.02.2010  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/16/2015 ajv : Required for memcpy and memset function calls
#include	<string.h>

#include	"battery_backed_vars.h"

#include	"bithacks.h"

#include	"budget_report_data.h"

#include	"moisture_sensors.h"

#include	"report_data.h"

#ifndef	_MSC_VER

	#include	"app_startup.h"

	#include	"foal_irri.h"

	#include	"foal_lights.h"

	#include	"lights_report_data.h"

	#include	"poc_report_data.h"

	#include	"r_alerts.h"

	#include	"station_history_data.h"

	#include	"station_report_data.h"

	#include	"system_report_data.h"

#else	// #ifdef _MSC_VER
	
	// 7/8/2016 skc : For isinf().
	#include	<math.h>

	#include	"sql_merge.h"

#endif




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static UNS_32 SYSTEM_REPORT_DATA_extract_and_store_changes( UNS_8 *pucp,
															const UNS_32 pdlen
															#ifdef _MSC_VER
															, const UNS_32 pnetwork_ID,
															const UNS_16 lastReportDate,
															const UNS_32 lastReportTime,
															char *pSQL_statements,
															char *pSQL_search_condition_str,
															char *pSQL_insert_values_str,
															char *pSQL_single_merge_statement_buf
															#endif
														  )
{
	SYSTEM_REPORT_RECORD	bsr;

	UNS_32	lrecord_count;

	UNS_32	rv;

	UNS_32	i;

	// ----------
	
	rv = 0;

	lrecord_count = (pdlen / sizeof(SYSTEM_REPORT_RECORD));

	// ----------

	#ifdef _MSC_VER

		#define		SYSTEM_REPORT_DATA_database_column_network_ID			(0)
		#define		SYSTEM_REPORT_DATA_database_column_system_ID			(1)
		#define		SYSTEM_REPORT_DATA_database_column_start_timestamp		(2)
		#define		SYSTEM_REPORT_DATA_database_column_rainfall_total		(3)
		#define		SYSTEM_REPORT_DATA_database_column_rr_seconds			(4)
		#define		SYSTEM_REPORT_DATA_database_column_rr_gallons			(5)
		#define		SYSTEM_REPORT_DATA_database_column_test_seconds			(6)
		#define		SYSTEM_REPORT_DATA_database_column_test_gallons			(7)
		#define		SYSTEM_REPORT_DATA_database_column_walk_thru_seconds	(8)
		#define		SYSTEM_REPORT_DATA_database_column_walk_thru_gallons	(9)
		#define		SYSTEM_REPORT_DATA_database_column_manual_seconds		(10)
		#define		SYSTEM_REPORT_DATA_database_column_manual_gallons		(11)
		#define		SYSTEM_REPORT_DATA_database_column_manual_prog_seconds	(12)
		#define		SYSTEM_REPORT_DATA_database_column_manual_prog_gallons	(13)
		#define		SYSTEM_REPORT_DATA_database_column_prog_seconds			(14)
		#define		SYSTEM_REPORT_DATA_database_column_prog_gallons			(15)
		#define		SYSTEM_REPORT_DATA_database_column_non_cont_seconds		(16)
		#define		SYSTEM_REPORT_DATA_database_column_non_cont_gallons		(17)

		static char* const SYSTEM_REPORT_DATA_database_field_names[] =
		{
			"NetworkID",
			"SystemID",
			"StartTimeStamp",
			"RainFallTotal",
			"RRSeconds",
			"RRGallons",
			"TestSeconds",
			"TestGallons",
			"WalkThruSeconds",
			"WalkThruGallons",
			"ManualSeconds",
			"ManualGallons",
			"ManualProgramSeconds",
			"ManualProgramGallons",
			"ProgramedIrrigSeconds",
			"ProgramedIrrigGallons",
			"NonControllerSeconds",
			"NonControllerGallons"
		};

		BOOL_32 include_merge;

		DATE_TIME last_timestamp_DB;

		DATE_TIME newest_timestamp;

		last_timestamp_DB.D = lastReportDate;
		last_timestamp_DB.T = lastReportTime;

		newest_timestamp = last_timestamp_DB;

		char* lreport_last_timestamp_column_name = "LastTimestamp_SystemReport";

	#endif

	// ----------
	
	for( i = 0; i < lrecord_count; ++i )
	{
		memcpy( &bsr, pucp, sizeof(SYSTEM_REPORT_RECORD) );
		pucp += sizeof(SYSTEM_REPORT_RECORD);
		rv += sizeof(SYSTEM_REPORT_RECORD);

		#ifndef _MSC_VER

			// 9/16/2015 ajv : TODO Insert the record(s) into the System report data file and trigger a file save

		#else

			// 10/15/2015 ajv : Only try to insert valid records - that is records that have a valid
			// start date.
			if( bsr.start_dt.D != 0 )
			{
				// 9/16/2015 ajv : Make sure the various SQL clause strings are fully initialized to NULL
				// since we rely on this to determine whether the string is empty or not.
				memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );
				memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				
				// ----------

				// 1/16/2017 wjb : always save the latest timestamp to update the db with the newest timestamp.
				if (i==0 || (DT1_IsBiggerThanOrEqualTo_DT2( &bsr.start_dt, &newest_timestamp)))
				{
					newest_timestamp = bsr.start_dt;
				}

				// ----------

				// 1/16/2017 wjb : include the Merge statement only if the record datetime is less than the last 
				// timestamp we have saved in the db, that's mean we may have this record been inserted in the db before.
				include_merge = DT1_IsBiggerThanOrEqualTo_DT2(&last_timestamp_DB, &bsr.start_dt);

				if (include_merge)
				{
					// 9/16/2015 ajv : Build the search condition (e.g., "ON NetworkID=:NetworkID AND ...")
					memset(pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE);

					SQL_MERGE_build_search_condition_uint32(SYSTEM_REPORT_DATA_database_field_names[SYSTEM_REPORT_DATA_database_column_network_ID], pnetwork_ID, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_uint32(SYSTEM_REPORT_DATA_database_field_names[SYSTEM_REPORT_DATA_database_column_system_ID], bsr.system_gid, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_timestamp(SYSTEM_REPORT_DATA_database_field_names[SYSTEM_REPORT_DATA_database_column_start_timestamp], bsr.start_dt, pSQL_search_condition_str);
				}
			
				// ----------
				
				// 9/16/2015 ajv : Populate the INSERT and VALUES clauses of the MERGE statement with the
				// values to insert.
				SQL_MERGE_build_insert_uint32_without_field_names( pnetwork_ID, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( bsr.system_gid, pSQL_insert_values_str );
				SQL_MERGE_build_insert_timestamp_without_field_names( bsr.start_dt, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( bsr.rainfall_raw_total_100u, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( bsr.rre_seconds, pSQL_insert_values_str );
				SQL_MERGE_build_insert_float_without_field_names( bsr.rre_gallons_fl, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( bsr.test_seconds, pSQL_insert_values_str );
				SQL_MERGE_build_insert_float_without_field_names( bsr.test_gallons_fl, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( bsr.walk_thru_seconds, pSQL_insert_values_str );
				SQL_MERGE_build_insert_float_without_field_names( bsr.walk_thru_gallons_fl, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( bsr.manual_seconds, pSQL_insert_values_str );
				SQL_MERGE_build_insert_float_without_field_names( bsr.manual_gallons_fl, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( bsr.manual_program_seconds, pSQL_insert_values_str );
				SQL_MERGE_build_insert_float_without_field_names( bsr.manual_program_gallons_fl, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( bsr.programmed_irrigation_seconds, pSQL_insert_values_str );
				SQL_MERGE_build_insert_float_without_field_names( bsr.programmed_irrigation_gallons_fl, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( bsr.non_controller_seconds, pSQL_insert_values_str );
				SQL_MERGE_build_insert_float_without_field_names( bsr.non_controller_gallons_fl, pSQL_insert_values_str );

				// ----------

				if (include_merge)
				{
					// 9/16/2015 ajv : Finally, build the MERGE statement and append it to the list of SQL
					// statements.
					SQL_MERGE_build_insert_merge_and_append_to_SQL_statements(DATABASE_TABLE_NAME_SYSTEM_REPORT_DATA, pSQL_search_condition_str, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
				}
				else
				{
					// 1/16/2017 wjb : Finally, build the INSERT statement and append it to the list of SQL
					// statements.
					SQL_INSERT_build_insert_and_append_to_SQL_statements(DATABASE_TABLE_NAME_SYSTEM_REPORT_DATA, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
				}

			}

		#endif
	}

	#ifdef _MSC_VER
		// 1/16/2017 wjb : update the last timestamp in the db for this report with the latest timestamp
		update_cs3000_last_timestamp_in_network_table(pnetwork_ID, newest_timestamp, lreport_last_timestamp_column_name, pSQL_statements, pSQL_single_merge_statement_buf);
	#endif

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 POC_REPORT_DATA_extract_and_store_changes( UNS_8 *pucp,
														 const UNS_32 pdlen
														 #ifdef _MSC_VER
														 , const UNS_32 pnetwork_ID,
														 const UNS_16 lastReportDate,
														 const UNS_32 lastReportTime,
														 char *pSQL_statements,
														 char *pSQL_search_condition_str,
														 char *pSQL_insert_values_str,
														 char *pSQL_single_merge_statement_buf
														 #endif
													   )
{
	POC_REPORT_RECORD	bpr;

	UNS_32	lrecord_count;

	UNS_32	rv;

	UNS_32	i;

	// ----------

	rv = 0;

	lrecord_count = (pdlen / sizeof(POC_REPORT_RECORD));

	// ----------

	#ifdef _MSC_VER

		#define		POC_REPORT_DATA_database_column_network_ID			(0)
		#define		POC_REPORT_DATA_database_column_poc_ID				(1)
		#define		POC_REPORT_DATA_database_column_start_timestamp		(2)
		#define		POC_REPORT_DATA_database_column_total_gallons		(3)
		#define		POC_REPORT_DATA_database_column_total_seconds		(4)
		#define		POC_REPORT_DATA_database_column_idle_gallons		(5)
		#define		POC_REPORT_DATA_database_column_idle_seconds		(6)
		#define		POC_REPORT_DATA_database_column_mvor_gallons		(7)
		#define		POC_REPORT_DATA_database_column_mvor_seconds		(8)
		#define		POC_REPORT_DATA_database_column_irri_gallons		(9)
		#define		POC_REPORT_DATA_database_column_irri_seconds		(10)

		static char* const POC_REPORT_DATA_database_field_names[] =
		{
			"NetworkID",
			"POC_ID",
			"StartTimeStamp",
			"GallonsTotal",
			"SecondsTotal",
			"IdleGallons",
			"IdleSeconds",
			"MVORGallons",
			"MVORSeconds",
			"IrriGallons",
			"IrriSeconds"
		};

		DATE_TIME	ldt;

		// ----------

		BOOL_32 include_merge;

		DATE_TIME last_timestamp_DB;

		DATE_TIME newest_timestamp;

		last_timestamp_DB.D = lastReportDate;
		last_timestamp_DB.T = lastReportTime;

		newest_timestamp = last_timestamp_DB;

		char* lreport_last_timestamp_column_name = "LastTimestamp_POCReport";

		// ----------

	#endif

	// ----------
	
	for( i = 0; i < lrecord_count; ++i )
	{
		memcpy( &bpr, pucp, sizeof(POC_REPORT_RECORD) );
		pucp += sizeof(POC_REPORT_RECORD);
		rv += sizeof(POC_REPORT_RECORD);

		#ifndef _MSC_VER

			// 9/16/2015 ajv : TODO Insert the record(s) into the POC report data file and trigger a file save

		#else

			// 10/15/2015 ajv : Only try to insert valid records - that is records that have a valid
			// start date.
			if( bpr.start_date != 0 )
			{
				// 9/16/2015 ajv : Since the database expects a TIMESTAMP field rather than a seperate date
				// and time, copy the start date and time into a DATE_TIME record so the SQL_MERGE functions
				// can build the appropriate TIMESTAMP string.
				ldt.D = bpr.start_date;
				ldt.T = bpr.start_time;

				// ----------

				// 9/16/2015 ajv : Make sure the various SQL clause strings are fully initialized to NULL
				// since we rely on this to determine whether the string is empty or not.
				memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );
				memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

				// ----------

				// 1/16/2017 wjb : always save the latest timestamp to update the db with the newest timestamp.
				if (i == 0 || (DT1_IsBiggerThanOrEqualTo_DT2(&ldt, &newest_timestamp)))
				{
					newest_timestamp = ldt;
				}

				// ----------

				// 1/16/2017 wjb : include the Merge statement only if the record datetime is less than the last 
				// timestamp we have saved in the db, that's mean we may have this record been inserted in the db before.
				include_merge = DT1_IsBiggerThanOrEqualTo_DT2(&last_timestamp_DB, &ldt);

				if (include_merge)
				{
					// 9/16/2015 ajv : Build the search condition (e.g., "ON NetworkID=:NetworkID AND ...")
					memset(pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE);
				
					SQL_MERGE_build_search_condition_uint32(POC_REPORT_DATA_database_field_names[POC_REPORT_DATA_database_column_network_ID], pnetwork_ID, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_uint32(POC_REPORT_DATA_database_field_names[POC_REPORT_DATA_database_column_poc_ID], bpr.poc_gid, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_timestamp(POC_REPORT_DATA_database_field_names[POC_REPORT_DATA_database_column_start_timestamp], ldt, pSQL_search_condition_str);
				}
			
				// ----------
				
				// 9/16/2015 ajv : Populate the INSERT and VALUES clauses of the MERGE statement with the
				// values to insert.
				SQL_MERGE_build_insert_uint32_without_field_names( pnetwork_ID, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( bpr.poc_gid, pSQL_insert_values_str );
				SQL_MERGE_build_insert_timestamp_without_field_names( ldt, pSQL_insert_values_str );
				SQL_MERGE_build_insert_double_without_field_names( bpr.gallons_total, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( bpr.seconds_of_flow_total, pSQL_insert_values_str );
				SQL_MERGE_build_insert_double_without_field_names( bpr.gallons_during_idle, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( bpr.seconds_of_flow_during_idle, pSQL_insert_values_str );
				SQL_MERGE_build_insert_double_without_field_names( bpr.gallons_during_mvor, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( bpr.seconds_of_flow_during_mvor, pSQL_insert_values_str );
				SQL_MERGE_build_insert_double_without_field_names( bpr.gallons_during_irrigation, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( bpr.seconds_of_flow_during_irrigation, pSQL_insert_values_str );

				// ----------

				if (include_merge)
				{
					// 9/16/2015 ajv : Finally, build the MERGE statement and append it to the list of SQL
					// statements.
					SQL_MERGE_build_insert_merge_and_append_to_SQL_statements(DATABASE_TABLE_NAME_POC_REPORT_DATA, pSQL_search_condition_str, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
				}
				else
				{
					// 1/16/2017 wjb : Finally, build the INSERT statement and append it to the list of SQL
					// statements.
					SQL_INSERT_build_insert_and_append_to_SQL_statements(DATABASE_TABLE_NAME_POC_REPORT_DATA, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
				}
			}

		#endif
	}

	// ----------

	#ifdef _MSC_VER
		// 1/16/2017 wjb : update the last timestamp in the db for this report with the latest timestamp
		update_cs3000_last_timestamp_in_network_table(pnetwork_ID, newest_timestamp, lreport_last_timestamp_column_name, pSQL_statements, pSQL_single_merge_statement_buf);
	#endif

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 STATION_REPORT_DATA_extract_and_store_changes( UNS_8 *pucp,
															 const UNS_32 pdlen
															 #ifdef _MSC_VER
															 , const UNS_32 pnetwork_ID,
															 const UNS_16 lastReportDate,
															 const UNS_32 lastReportTime,
															 char *pSQL_statements,
															 char *pSQL_search_condition_str,
															 char *pSQL_insert_values_str,
															 char *pSQL_single_merge_statement_buf
															 #endif
														   )
{
	STATION_REPORT_DATA_RECORD	lrecord;

	UNS_32	lrecord_count;

	UNS_32	rv;

	UNS_32	i;

	// ----------

	rv = 0;

	lrecord_count = (pdlen / sizeof(STATION_REPORT_DATA_RECORD));

	// ----------

	#ifdef _MSC_VER

		#define		STATION_REPORT_DATA_database_column_station_num			(0)
		#define		STATION_REPORT_DATA_database_column_box_index			(1)
		#define		STATION_REPORT_DATA_database_column_station_gid			(2)
		#define		STATION_REPORT_DATA_database_column_prog_seconds		(3)
		#define		STATION_REPORT_DATA_database_column_prog_gallons		(4)
		#define		STATION_REPORT_DATA_database_column_manual_prog_seconds	(5)
		#define		STATION_REPORT_DATA_database_column_manual_prog_gallons	(6)
		#define		STATION_REPORT_DATA_database_column_manual_seconds		(7)
		#define		STATION_REPORT_DATA_database_column_manual_gallons		(8)
		#define		STATION_REPORT_DATA_database_column_walk_thru_seconds	(9)
		#define		STATION_REPORT_DATA_database_column_walk_thru_gallons	(10)
		#define		STATION_REPORT_DATA_database_column_test_seconds		(11)
		#define		STATION_REPORT_DATA_database_column_test_gallons		(12)
		#define		STATION_REPORT_DATA_database_column_rr_seconds			(13)
		#define		STATION_REPORT_DATA_database_column_rr_gallons			(14)
		#define		STATION_REPORT_DATA_database_column_timestamp			(15)
		#define		STATION_REPORT_DATA_database_column_network_ID			(16)

		static char* const STATION_REPORT_DATA_database_field_names[] =
		{
			"StationID",
			"BoxIndex",
			"StationGroupID",
			"ProgramedIrrigSeconds",
			"ProgramedIrrigGallons",
			"ManualProgramSeconds",
			"ManualProgramGallons",
			"ManualSeconds",
			"ManualGallons",
			"WalkThruSeconds",
			"WalkThruGallons",
			"TestSeconds",
			"TestGallons",
			"RRSeconds",
			"RRGallons",
			"TimeStamp",
			"NetworkID"
		};

		// ----------

		BOOL_32 include_merge;

		DATE_TIME last_timestamp_DB;

		DATE_TIME newest_timestamp;

		last_timestamp_DB.D = lastReportDate;
		last_timestamp_DB.T = lastReportTime;

		newest_timestamp = last_timestamp_DB;

		char* lreport_last_timestamp_column_name = "LastTimestamp_StationReport";

		// ----------

		DATE_TIME	ldt;

	#endif

	// ----------

	for( i = 0; i < lrecord_count; ++i )
	{
		memcpy( &lrecord, pucp, sizeof(STATION_REPORT_DATA_RECORD) );
		pucp += sizeof(STATION_REPORT_DATA_RECORD);
		rv += sizeof(STATION_REPORT_DATA_RECORD);

		#ifndef _MSC_VER

			// 9/16/2015 ajv : TODO Insert the record(s) into the Station report data file and trigger a file save

		#else

			// 10/15/2015 ajv : Only try to insert valid records - that is records that have a valid
			// start date.
			
			// 10/20/2015 ajv : TODO REMOVE lrecord.programmed_irrigation_seconds_irrigated_ul CHECK!
			// This was added to resolve an issue where the station report record RIP wasn't initialized on a firmware update.
		if ((lrecord.record_start_date != 0) && (lrecord.programmed_irrigation_seconds_irrigated_ul < 86400))
		{
			// 9/16/2015 ajv : Since the database expects a TIMESTAMP field rather than a seperate date
			// and time, copy the start date and time into a DATE_TIME record so the SQL_MERGE functions
			// can build the appropriate TIMESTAMP string.
			ldt.D = lrecord.record_start_date;
			ldt.T = lrecord.record_start_time;

			// ----------

			// 9/16/2015 ajv : Make sure the various SQL clause strings are fully initialized to NULL
			// since we rely on this to determine whether the string is empty or not.
			memset(pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT);
			memset(pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE);

			// ----------

			// 1/16/2017 wjb : always save the latest timestamp to update the db with the newest timestamp.
			if (i == 0 || (DT1_IsBiggerThanOrEqualTo_DT2(&ldt, &newest_timestamp)))
			{
				newest_timestamp = ldt;
			}

			// ----------

			// 1/16/2017 wjb : include the Merge statement only if the record datetime is less than the last 
			// timestamp we have saved in the db, that's mean we may have this record been inserted in the db before.
			include_merge = DT1_IsBiggerThanOrEqualTo_DT2(&last_timestamp_DB, &ldt);

			if (include_merge)
			{
				// 9/16/2015 ajv : Build the search condition (e.g., "ON NetworkID=:NetworkID AND ...")
				memset(pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE);

				SQL_MERGE_build_search_condition_uint32(STATION_REPORT_DATA_database_field_names[STATION_REPORT_DATA_database_column_network_ID], pnetwork_ID, pSQL_search_condition_str);
				SQL_MERGE_build_search_condition_uint32(STATION_REPORT_DATA_database_field_names[STATION_REPORT_DATA_database_column_station_num], lrecord.station_number, pSQL_search_condition_str);
				SQL_MERGE_build_search_condition_uint32(STATION_REPORT_DATA_database_field_names[STATION_REPORT_DATA_database_column_box_index], lrecord.box_index_0, pSQL_search_condition_str);
				SQL_MERGE_build_search_condition_timestamp(STATION_REPORT_DATA_database_field_names[STATION_REPORT_DATA_database_column_timestamp], ldt, pSQL_search_condition_str);
			}

			// ----------

			// 9/16/2015 ajv : Populate the INSERT and VALUES clauses of the MERGE statement with the
			// values to insert.
			SQL_MERGE_build_insert_uint32_without_field_names(lrecord.station_number, pSQL_insert_values_str);
			SQL_MERGE_build_insert_uint32_without_field_names(lrecord.box_index_0, pSQL_insert_values_str);
			SQL_MERGE_build_insert_uint32_without_field_names(lrecord.GID_station_group, pSQL_insert_values_str);
			SQL_MERGE_build_insert_uint32_without_field_names(lrecord.programmed_irrigation_seconds_irrigated_ul, pSQL_insert_values_str);
			SQL_MERGE_build_insert_float_without_field_names(lrecord.programmed_irrigation_gallons_irrigated_fl, pSQL_insert_values_str);
			SQL_MERGE_build_insert_uint32_without_field_names(lrecord.manual_program_seconds_us, pSQL_insert_values_str);
			SQL_MERGE_build_insert_float_without_field_names(lrecord.manual_program_gallons_fl, pSQL_insert_values_str);
			SQL_MERGE_build_insert_uint32_without_field_names(lrecord.manual_seconds_us, pSQL_insert_values_str);
			SQL_MERGE_build_insert_float_without_field_names(lrecord.manual_gallons_fl, pSQL_insert_values_str);
			SQL_MERGE_build_insert_uint32_without_field_names(lrecord.walk_thru_seconds_us, pSQL_insert_values_str);
			SQL_MERGE_build_insert_float_without_field_names(lrecord.walk_thru_gallons_fl, pSQL_insert_values_str);
			SQL_MERGE_build_insert_uint32_without_field_names(lrecord.test_seconds_us, pSQL_insert_values_str);
			SQL_MERGE_build_insert_float_without_field_names(lrecord.test_gallons_fl, pSQL_insert_values_str);
			SQL_MERGE_build_insert_uint32_without_field_names(lrecord.mobile_seconds_us, pSQL_insert_values_str);
			SQL_MERGE_build_insert_float_without_field_names(lrecord.mobile_gallons_fl, pSQL_insert_values_str);
			SQL_MERGE_build_insert_timestamp_without_field_names(ldt, pSQL_insert_values_str);
			SQL_MERGE_build_insert_uint32_without_field_names(pnetwork_ID, pSQL_insert_values_str);

			// ----------

			if (include_merge)
			{
				// 9/16/2015 ajv : Finally, build the MERGE statement and append it to the list of SQL
				// statements.
				SQL_MERGE_build_insert_merge_and_append_to_SQL_statements(DATABASE_TABLE_NAME_STATION_REPORT_DATA, pSQL_search_condition_str, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
			}
			else
			{
				// 1/16/2017 wjb : Finally, build the INSERT statement and append it to the list of SQL
				// statements.
				SQL_INSERT_build_insert_and_append_to_SQL_statements(DATABASE_TABLE_NAME_STATION_REPORT_DATA, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
			}
		}

		#endif
	}

	// ----------

	#ifdef _MSC_VER
		// 1/16/2017 wjb : update the last timestamp in the db for this report with the latest timestamp
		update_cs3000_last_timestamp_in_network_table(pnetwork_ID, newest_timestamp, lreport_last_timestamp_column_name, pSQL_statements, pSQL_single_merge_statement_buf);
	#endif

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 STATION_HISTORY_extract_and_store_changes( UNS_8 *pucp,
														 const UNS_32 pdlen
														 #ifdef _MSC_VER
														 , const UNS_32 pnetwork_ID,
														 const UNS_16 lastReportDate,
														 const UNS_32 lastReportTime,
														 char *pSQL_statements,
														 char *pSQL_search_condition_str,
														 char *pSQL_insert_values_str,
														 char *pSQL_single_merge_statement_buf
														 #endif
													   )
{
	STATION_HISTORY_RECORD	lrecord;

	UNS_32	lrecord_count;

	UNS_32	rv;

	UNS_32	i;

	// ----------

	rv = 0;

	lrecord_count = (pdlen / sizeof(STATION_HISTORY_RECORD));

	// ----------

	#ifdef _MSC_VER

		#define		STATION_HISTORY_database_column_start_timestamp			(0)
		#define		STATION_HISTORY_database_column_station_num				(1)
		#define		STATION_HISTORY_database_column_system_gid				(2)
		#define		STATION_HISTORY_database_column_station_group_gid		(3)
		#define		STATION_HISTORY_database_column_first_cycle_timestamp	(4)
		#define		STATION_HISTORY_database_column_last_cycle_timestamp	(5)
		#define		STATION_HISTORY_database_column_irrig_seconds			(6)
		#define		STATION_HISTORY_database_column_last_current			(7)
		#define		STATION_HISTORY_database_column_irrig_gallons			(8)
		#define		STATION_HISTORY_database_column_total_min				(9)
		#define		STATION_HISTORY_database_column_rain_at_start			(10)
		#define		STATION_HISTORY_database_column_rain_at_end				(11)
		#define		STATION_HISTORY_database_column_portion_of_actual		(12)
		#define		STATION_HISTORY_database_column_high_limit				(13)
		#define		STATION_HISTORY_database_column_low_limit				(14)
		#define		STATION_HISTORY_database_column_number_of_repeats		(15)
		#define		STATION_HISTORY_database_column_flow_data_stamped		(16)
		#define		STATION_HISTORY_database_column_controller_off			(17)
		#define		STATION_HISTORY_database_column_hit_stop_time			(18)
		#define		STATION_HISTORY_database_column_stop_key_pressed		(19)
		#define		STATION_HISTORY_database_column_short					(20)
		#define		STATION_HISTORY_database_column_no_current				(21)
		#define		STATION_HISTORY_database_column_low_current				(22)
		#define		STATION_HISTORY_database_column_high_current			(23)
		#define		STATION_HISTORY_database_column_watersense_modified		(24)
		#define		STATION_HISTORY_database_column_watersense_zeroed		(25)
		#define		STATION_HISTORY_database_column_low_flow				(26)
		#define		STATION_HISTORY_database_column_high_flow				(27)
		#define		STATION_HISTORY_database_column_flow_not_checked		(28)
		#define		STATION_HISTORY_database_column_no_water_by_manual		(29)
		#define		STATION_HISTORY_database_column_no_water_by_calendar	(30)
		#define		STATION_HISTORY_database_column_mlb						(31)
		#define		STATION_HISTORY_database_column_mvor_closed				(32)
		#define		STATION_HISTORY_database_column_rain_prevented			(33)
		#define		STATION_HISTORY_database_column_rain_reduced			(34)
		#define		STATION_HISTORY_database_column_rain_table				(35)
		#define		STATION_HISTORY_database_column_rain_switch				(36)
		#define		STATION_HISTORY_database_column_freeze_switch			(37)
		#define		STATION_HISTORY_database_column_wind					(38)
		#define		STATION_HISTORY_database_column_moisture				(39)
		#define		STATION_HISTORY_database_column_max_water_day			(40)
		#define		STATION_HISTORY_database_column_poc_short				(41)
		#define		STATION_HISTORY_database_column_mow_day					(42)
		#define		STATION_HISTORY_database_column_two_wire_cable_problem	(43)
		#define		STATION_HISTORY_database_column_network_ID				(44)
		#define		STATION_HISTORY_database_column_box_index				(45)
		#define		STATION_HISTORY_database_column_rip_valid				(46)
		#define		STATION_HISTORY_database_column_station_decoder_problem	(47)
		#define		STATION_HISTORY_database_column_poc_decoder_problem		(48)
		#define		STATION_HISTORY_database_column_moisture_balance		(49)
		#define		STATION_HISTORY_database_column_watersense_min			(50)
		#define		STATION_HISTORY_database_column_budget_used				(51)
		#define		STATION_HISTORY_database_column_budget_reduction_limit	(52)
		#define		STATION_HISTORY_database_column_skipped_due_to_MB		(53)

		static char* const STATION_HISTORY_database_field_names[] =
		{
			"StartTimeStamp",
			"StationID",
			"SystemID",
			"StationGroupID",
			"FirstCycleStartTimeStamp",
			"LastCycleEndTimeStamp",
			"SecondsIrrigated",
			"LastMeasuredCurrent",
			"GallonsIrrigated",
			"TotalRequestedMinutes",
			"RainAtStartBefore",
			"RainAtStartAfter",
			"ActualFlowCheckShare",
			"HiLimitFlowCheckShare",
			"LoLimitFlowCheckShare",
			"NumberOfRepeats",
			"FlowDataStamped",
			"ControllerTurnedOff",
			"HitStopTime",
			"StopKeyPressed",
			"CurrentShort",
			"CurrentNone",
			"CurrentLow",
			"CurrentHigh",
			"TailendsModifiedCycle",
			"TailendsZeroedIrrig",
			"FlowLow",
			"FlowHigh",
			"FlowNeverChecked",
			"NoWaterByManual",
			"NoWaterByCalendar",
			"MlbPrevented",
			"MvorClosed",
			"RainPrevented",
			"RainReducedIrrig",
			"RainTable",
			"SwitchRain",
			"SwitchFreeze",
			"WindConditions",
			"MoisCauseCycleSkip",
			"MoisMaxWaterDay",
			"PocShort",
			"MowDay",
			"TwoWireCable",
			"NetworkID",
			"BoxIndex",
			"RipValid",
			"twStationDecoder",
			"twPOCDecoder",
			"MoistureBalancePerc",
			"WaterSenseMinutes",
			"BudgetUsed",
			"BudgetReductionLimit",
			"MBPrevented"
		};

		DATE_TIME	lstart_dt;

		DATE_TIME	lfirst_cycle_dt;

		DATE_TIME	llast_cycle_dt;

		// ----------

		BOOL_32 include_merge;

		DATE_TIME last_timestamp_DB;

		DATE_TIME newest_timestamp;

		last_timestamp_DB.D = lastReportDate;
		last_timestamp_DB.T = lastReportTime;

		newest_timestamp = last_timestamp_DB;

		char* lreport_last_timestamp_column_name = "LastTimestamp_StationHistory";

		// ----------

	#endif

	// ----------

	for( i = 0; i < lrecord_count; ++i )
	{
		memcpy( &lrecord, pucp, sizeof(STATION_HISTORY_RECORD) );
		pucp += sizeof(STATION_HISTORY_RECORD);
		rv += sizeof(STATION_HISTORY_RECORD);

		#ifndef _MSC_VER

			// 9/16/2015 ajv : TODO Insert the record(s) into the Station report data file and trigger a file save

		#else

			// 10/15/2015 ajv : Only try to insert valid records - that is records that have a valid
			// start date.
			if( lrecord.record_start_date != 0 )
			{
				// 9/16/2015 ajv : Since the database expects a TIMESTAMP field rather than a seperate date
				// and time, copy the start date and time into a DATE_TIME record so the SQL_MERGE functions
				// can build the appropriate TIMESTAMP string.
				lstart_dt.D = lrecord.record_start_date;
				lstart_dt.T = lrecord.record_start_time;

				lfirst_cycle_dt.D = lrecord.pi_first_cycle_start_date;
				lfirst_cycle_dt.T = lrecord.pi_first_cycle_start_time;

				llast_cycle_dt.D = lrecord.pi_last_cycle_end_date;
				llast_cycle_dt.T = lrecord.pi_last_cycle_end_time;

				// ----------

				// 9/16/2015 ajv : Make sure the various SQL clause strings are fully initialized to NULL
				// since we rely on this to determine whether the string is empty or not.
				memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );
				memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

				// ----------

				// 1/16/2017 wjb : always save the latest timestamp to update the db with the newest timestamp.
				if (i == 0 || (DT1_IsBiggerThanOrEqualTo_DT2(&llast_cycle_dt, &newest_timestamp)))
				{
					newest_timestamp = llast_cycle_dt;
				}

				// ----------

				// 1/16/2017 wjb : include the Merge statement only if the record datetime is less than the last 
				// timestamp we have saved in the db, that's mean we may have this record been inserted in the db before.
				include_merge = DT1_IsBiggerThanOrEqualTo_DT2(&last_timestamp_DB, &llast_cycle_dt);

				if (include_merge)
				{
					// 9/16/2015 ajv : Build the search condition (e.g., "ON NetworkID=:NetworkID AND ...")
					memset(pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE);

					SQL_MERGE_build_search_condition_uint32(STATION_HISTORY_database_field_names[STATION_HISTORY_database_column_network_ID], pnetwork_ID, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_uint32(STATION_HISTORY_database_field_names[STATION_HISTORY_database_column_station_num], lrecord.station_number, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_uint32(STATION_HISTORY_database_field_names[STATION_HISTORY_database_column_box_index], lrecord.box_index_0, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_timestamp(STATION_HISTORY_database_field_names[STATION_HISTORY_database_column_start_timestamp], lstart_dt, pSQL_search_condition_str);
				}

				// ----------

				// 9/16/2015 ajv : Populate the INSERT and VALUES clauses of the MERGE statement with the
				// values to insert.
				SQL_MERGE_build_insert_timestamp_without_field_names( lstart_dt, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrecord.station_number, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrecord.GID_irrigation_system, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrecord.GID_irrigation_schedule, pSQL_insert_values_str );
				SQL_MERGE_build_insert_timestamp_without_field_names( lfirst_cycle_dt, pSQL_insert_values_str );
				SQL_MERGE_build_insert_timestamp_without_field_names( llast_cycle_dt, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrecord.pi_seconds_irrigated_ul, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrecord.pi_last_measured_current_ma, pSQL_insert_values_str );
				SQL_MERGE_build_insert_float_without_field_names( lrecord.pi_gallons_irrigated_fl, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrecord.pi_total_requested_minutes_us_10u, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrecord.pi_rain_at_start_time_before_working_down__minutes_10u, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrecord.pi_rain_at_start_time_after_working_down__minutes_10u, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrecord.pi_flow_check_share_of_actual_gpm, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrecord.pi_flow_check_share_of_hi_limit_gpm, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrecord.pi_flow_check_share_of_lo_limit_gpm, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrecord.pi_number_of_repeats, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.pi_flow_data_has_been_stamped, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.controller_turned_off, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.hit_stop_time, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.stop_key_pressed, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.current_short, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.current_none, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.current_low, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.current_high, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.watersense_min_cycle_eliminated_a_cycle, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.watersense_min_cycle_zeroed_the_irrigation_time, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.flow_low, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.flow_high, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.flow_never_checked, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.no_water_by_manual_prevented, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.no_water_by_calendar_prevented, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.mlb_prevented_or_curtailed, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.mvor_closed_prevented_or_curtailed, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.rain_as_negative_time_prevented_irrigation, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.rain_as_negative_time_reduced_irrigation, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.rain_table_R_M_or_Poll_prevented_or_curtailed, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.switch_rain_prevented_or_curtailed, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.switch_freeze_prevented_or_curtailed, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.wind_conditions_prevented_or_curtailed, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.mois_cause_cycle_skip, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.mois_max_water_day, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.poc_short_cancelled_irrigation, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.mow_day, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.two_wire_cable_problem, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( pnetwork_ID, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrecord.box_index_0, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.rip_valid_to_show, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.two_wire_station_decoder_inoperative, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.two_wire_poc_decoder_inoperative, pSQL_insert_values_str );
				SQL_MERGE_build_insert_int32_without_field_names( lrecord.pi_moisture_balance_percentage_after_schedule_completes_100u, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrecord.pi_watersense_requested_seconds, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( B_IS_SET( lrecord.pi_flag2, STATION_history_bit_budget_applied ), pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( B_IS_SET( lrecord.pi_flag2, STATION_history_bit_budget_reduction_limit ), pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( lrecord.pi_flag.moisture_balance_prevented_irrigation, pSQL_insert_values_str );

				// ----------

				if (include_merge)
				{
					// 9/16/2015 ajv : Finally, build the MERGE statement and append it to the list of SQL
					// statements.
					SQL_MERGE_build_insert_merge_and_append_to_SQL_statements(DATABASE_TABLE_NAME_STATION_HISTORY, pSQL_search_condition_str, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
				}
				else
				{
					// 1/16/2017 wjb : Finally, build the INSERT statement and append it to the list of SQL
					// statements.
					SQL_INSERT_build_insert_and_append_to_SQL_statements(DATABASE_TABLE_NAME_STATION_HISTORY, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
				}
			}

		#endif
	}

	// ----------

	#ifdef _MSC_VER
		// 1/16/2017 wjb : update the last timestamp in the db for this report with the latest timestamp
		update_cs3000_last_timestamp_in_network_table(pnetwork_ID, newest_timestamp, lreport_last_timestamp_column_name, pSQL_statements, pSQL_single_merge_statement_buf);
	#endif

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 FLOW_RECORDING_extract_and_store_changes( UNS_8 *pucp,
														const UNS_32 pdlen
														#ifdef _MSC_VER
														, const UNS_32 pnetwork_ID,
														const UNS_16 lastReportDate,
														const UNS_32 lastReportTime,
														char *pSQL_statements,
														char *pSQL_search_condition_str,
														char *pSQL_insert_values_str,
														char *pSQL_single_merge_statement_buf
														#endif
													  )
{
	CS3000_FLOW_RECORDER_RECORD		frr;

	UNS_32	lsystem_gid;

	UNS_32	lrecord_count;

	UNS_32	rv;

	UNS_32	i;

	// ----------

	rv = 0;

	// 9/17/2015 ajv : Make sure we deduct the system GID that preceeds the data from the length
	// before calculating the record count.
	lrecord_count = ((pdlen - sizeof(UNS_32)) / sizeof(CS3000_FLOW_RECORDER_RECORD));

	// ----------

	#ifdef _MSC_VER

		#define		FLOW_RECORDING_database_column_network_ID			(0)
		#define		FLOW_RECORDING_database_column_system_ID			(1)
		#define		FLOW_RECORDING_database_column_flow_timestamp		(2)
		#define		FLOW_RECORDING_database_column_box_index			(3)
		#define		FLOW_RECORDING_database_column_station				(4)
		#define		FLOW_RECORDING_database_column_expected_flow		(5)
		#define		FLOW_RECORDING_database_column_derated_expected		(6)
		#define		FLOW_RECORDING_database_column_high_limit			(7)
		#define		FLOW_RECORDING_database_column_low_limit			(8)
		#define		FLOW_RECORDING_database_column_portion_of_actual	(9)
		#define		FLOW_RECORDING_database_column_flag					(10)

		static char* const FLOW_RECORDING_database_field_names[] =
		{
			"NetworkID",
			"SystemGID",
			"FlowTimeStamp",
			"Controller",
			"Station",
			"ExpectedFlow",
			"DeratedExpectedFlow",
			"HiLimit",
			"LoLimit",
			"PortionOfActual",
			"Flag"
		};

		// ----------

		BOOL_32 include_merge;

		DATE_TIME last_timestamp_DB;

		DATE_TIME newest_timestamp;

		last_timestamp_DB.D = lastReportDate;
		last_timestamp_DB.T = lastReportTime;

		newest_timestamp = last_timestamp_DB;

		char* lreport_last_timestamp_column_name = "LastTimestamp_FlowRec";

		// ----------

	#endif

	// ----------

	memcpy( &lsystem_gid, pucp, sizeof(UNS_32) );
	pucp += sizeof(UNS_32);
	rv += sizeof(UNS_32);

	for( i = 0; i < lrecord_count; ++i )
	{
		memcpy( &frr, pucp, sizeof(CS3000_FLOW_RECORDER_RECORD) );
		pucp += sizeof(CS3000_FLOW_RECORDER_RECORD);
		rv += sizeof(CS3000_FLOW_RECORDER_RECORD);

		#ifndef _MSC_VER

			// 9/16/2015 ajv : TODO Insert the record(s) into the battery-backed Flow Recording structure

		#else

			// 10/15/2015 ajv : Only try to insert valid records - that is records that have a valid
			// start date.
			if( frr.dt.D != 0 )
			{
				// 9/16/2015 ajv : Make sure the various SQL clause strings are fully initialized to NULL
				// since we rely on this to determine whether the string is empty or not.
				memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );
				memset(pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE);

				// ----------

				// 1/16/2017 wjb : always save the latest timestamp to update the db with the newest timestamp.
				if (i == 0 || (DT1_IsBiggerThanOrEqualTo_DT2(&frr.dt, &newest_timestamp)))
				{
					newest_timestamp = frr.dt;
				}

				// ----------

				// 1/16/2017 wjb : include the Merge statement only if the record datetime is less than the last 
				// timestamp we have saved in the db, that's mean we may have this record been inserted in the db before.
				include_merge = DT1_IsBiggerThanOrEqualTo_DT2(&last_timestamp_DB, &frr.dt);

				if (include_merge)
				{
					// 9/16/2015 ajv : Build the search condition (e.g., "ON NetworkID=:NetworkID AND ...")
					memset(pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE);

					SQL_MERGE_build_search_condition_uint32(FLOW_RECORDING_database_field_names[FLOW_RECORDING_database_column_network_ID], pnetwork_ID, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_uint32(FLOW_RECORDING_database_field_names[FLOW_RECORDING_database_column_system_ID], lsystem_gid, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_uint32(FLOW_RECORDING_database_field_names[FLOW_RECORDING_database_column_box_index], frr.box_index_0, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_uint32(FLOW_RECORDING_database_field_names[FLOW_RECORDING_database_column_station], frr.station_num, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_timestamp(FLOW_RECORDING_database_field_names[FLOW_RECORDING_database_column_flow_timestamp], frr.dt, pSQL_search_condition_str);
				}

				// ----------

				// 9/16/2015 ajv : Populate the INSERT and VALUES clauses of the MERGE statement with the
				// values to insert.
				SQL_MERGE_build_insert_uint32_without_field_names( pnetwork_ID, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names(lsystem_gid, pSQL_insert_values_str);
				SQL_MERGE_build_insert_timestamp_without_field_names(frr.dt, pSQL_insert_values_str);
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)frr.box_index_0, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)frr.station_num, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)frr.expected_flow, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)frr.derated_expected_flow, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)frr.hi_limit, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)frr.lo_limit, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)frr.portion_of_actual, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)frr.flag.overall_size, pSQL_insert_values_str );

				// ----------

				if (include_merge)
				{
					// 9/16/2015 ajv : Finally, build the MERGE statement and append it to the list of SQL
					// statements.
					SQL_MERGE_build_insert_merge_and_append_to_SQL_statements(DATABASE_TABLE_NAME_FLOW_RECORDING, pSQL_search_condition_str, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
				}
				else
				{
					// 1/16/2017 wjb : Finally, build the INSERT statement and append it to the list of SQL
					// statements.
					SQL_INSERT_build_insert_and_append_to_SQL_statements(DATABASE_TABLE_NAME_FLOW_RECORDING, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
				}
			}

		#endif
	}

	// ----------

	#ifdef _MSC_VER
		// 1/16/2017 wjb : update the last timestamp in the db for this report with the latest timestamp
		update_cs3000_last_timestamp_in_network_table(pnetwork_ID, newest_timestamp, lreport_last_timestamp_column_name, pSQL_statements, pSQL_single_merge_statement_buf);
	#endif

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 LIGHTS_REPORT_DATA_extract_and_store_changes( UNS_8 *pucp,
															const UNS_32 pdlen
															#ifdef _MSC_VER
															, const UNS_32 pnetwork_ID,
															const UNS_16 lastReportDate,
															const UNS_32 lastReportTime,
															char *pSQL_statements,
															char *pSQL_search_condition_str,
															char *pSQL_insert_values_str,
															char *pSQL_single_merge_statement_buf
															#endif
														  )
{
	LIGHTS_REPORT_RECORD	lrr;

	UNS_32	lrecord_count;

	UNS_32	rv;

	UNS_32	i;

	// ----------

	rv = 0;

	lrecord_count = (pdlen / sizeof(LIGHTS_REPORT_RECORD));

	// ----------

	#ifdef _MSC_VER

		#define		LIGHTS_REPORT_DATA_database_column_network_ID			(0)
		#define		LIGHTS_REPORT_DATA_database_column_box_index			(1)
		#define		LIGHTS_REPORT_DATA_database_column_output_index			(2)
		#define		LIGHTS_REPORT_DATA_database_column_start_timestamp		(3)
		#define		LIGHTS_REPORT_DATA_database_column_programmed_seconds	(4)
		#define		LIGHTS_REPORT_DATA_database_column_manual_seconds		(5)
		#define		LIGHTS_REPORT_DATA_database_column_cycles				(6)

		static char* const LIGHTS_REPORT_DATA_database_field_names[] =
		{
			"NetworkID",
			"BoxIndex",
			"OutputID",
			"StartTimeStamp",
			"ProgrammedSeconds",
			"ManualSeconds",
			"NumberOfRepeats"
		};

		DATE_TIME	ldt;

		// ----------

		BOOL_32 include_merge;

		DATE_TIME last_timestamp_DB;

		DATE_TIME newest_timestamp;

		last_timestamp_DB.D = lastReportDate;
		last_timestamp_DB.T = lastReportTime;

		newest_timestamp = last_timestamp_DB;

		char* lreport_last_timestamp_column_name = "LastTimestamp_BudgetReport";

		// ----------

	#endif

	// ----------
	
	for( i = 0; i < lrecord_count; ++i )
	{
		memcpy( &lrr, pucp, sizeof(LIGHTS_REPORT_RECORD) );
		pucp += sizeof(LIGHTS_REPORT_RECORD);
		rv += sizeof(LIGHTS_REPORT_RECORD);

		#ifndef _MSC_VER

			// 9/16/2015 ajv : TODO Insert the record(s) into the lights report data file and trigger a file save

		#else

			// 10/15/2015 ajv : Only try to insert valid records - that is records that have a valid
			// start date.
			if( lrr.record_start_date != 0 )
			{
				// 9/16/2015 ajv : Since the database expects a TIMESTAMP field rather than a seperate date
				// and time, copy the start date and time into a DATE_TIME record so the SQL_MERGE functions
				// can build the appropriate TIMESTAMP string.
				ldt.D = lrr.record_start_date;
				ldt.T = 0;

				// ----------

				// 9/16/2015 ajv : Make sure the various SQL clause strings are fully initialized to NULL
				// since we rely on this to determine whether the string is empty or not.
				memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );
				memset(pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE);

				// ----------

				// 1/16/2017 wjb : always save the latest timestamp to update the db with the newest timestamp.
				if (i == 0 || (DT1_IsBiggerThanOrEqualTo_DT2(&ldt, &newest_timestamp)))
				{
					newest_timestamp = ldt;
				}

				// ----------

				// 1/16/2017 wjb : include the Merge statement only if the record datetime is less than the last 
				// timestamp we have saved in the db, that's mean we may have this record been inserted in the db before.
				include_merge = DT1_IsBiggerThanOrEqualTo_DT2(&last_timestamp_DB, &ldt);

				if (include_merge)
				{
					// 9/16/2015 ajv : Build the search condition (e.g., "ON NetworkID=:NetworkID AND ...")
					memset(pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE);

					SQL_MERGE_build_search_condition_uint32(LIGHTS_REPORT_DATA_database_field_names[LIGHTS_REPORT_DATA_database_column_network_ID], pnetwork_ID, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_uint32(LIGHTS_REPORT_DATA_database_field_names[LIGHTS_REPORT_DATA_database_column_box_index], lrr.box_index_0, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_uint32(LIGHTS_REPORT_DATA_database_field_names[LIGHTS_REPORT_DATA_database_column_output_index], lrr.output_index_0, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_timestamp(LIGHTS_REPORT_DATA_database_field_names[LIGHTS_REPORT_DATA_database_column_start_timestamp], ldt, pSQL_search_condition_str);
				}

				// ----------
				
				// 9/16/2015 ajv : Populate the INSERT and VALUES clauses of the MERGE statement with the
				// values to insert.
				SQL_MERGE_build_insert_uint32_without_field_names( pnetwork_ID, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrr.box_index_0, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrr.output_index_0, pSQL_insert_values_str );
				SQL_MERGE_build_insert_timestamp_without_field_names( ldt, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrr.programmed_seconds, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrr.manual_seconds, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( lrr.number_of_on_cycles, pSQL_insert_values_str );

				// ----------

				if (include_merge)
				{
					// 9/16/2015 ajv : Finally, build the MERGE statement and append it to the list of SQL
					// statements.
					SQL_MERGE_build_insert_merge_and_append_to_SQL_statements(DATABASE_TABLE_NAME_LIGHTS_REPORT_DATA, pSQL_search_condition_str, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
				}
				else
				{
					SQL_INSERT_build_insert_and_append_to_SQL_statements(DATABASE_TABLE_NAME_LIGHTS_REPORT_DATA, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
				}
			}

		#endif
	}

	// ----------

	#ifdef _MSC_VER
		// 1/16/2017 wjb : update the last timestamp in the db for this report with the latest timestamp
		update_cs3000_last_timestamp_in_network_table(pnetwork_ID, newest_timestamp, lreport_last_timestamp_column_name, pSQL_statements, pSQL_single_merge_statement_buf);
	#endif

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 WEATHER_TABLES_extract_and_store_changes( UNS_8 *pucp,
														const UNS_32 pdlen
														#ifdef _MSC_VER
														, const UNS_32 pnetwork_ID,
														char *pSQL_statements,
														char *pSQL_search_condition_str,
														char *pSQL_update_str,
														char *pSQL_insert_fields_str,
														char *pSQL_insert_values_str,
														char *pSQL_single_merge_statement_buf
														#endif
													  )
{
	UNS_32	lrecord_count;

	UNS_32	rv;

	UNS_32	i;

	UNS_32	ldate;

	ET_TABLE_ENTRY		let_entry;

	RAIN_TABLE_ENTRY	lrain_entry;

	// ----------

	rv = 0;

	// ----------

	#ifdef _MSC_VER

		#define		WEATHER_TABLE_database_column_network_ID			(0)
		#define		WEATHER_TABLE_database_column_date					(1)
		#define		WEATHER_TABLE_database_column_et_value				(2)
		#define		WEATHER_TABLE_database_column_et_status				(3)
		#define		WEATHER_TABLE_database_column_rain_value			(4)
		#define		WEATHER_TABLE_database_column_rain_status			(5)

		static char* const WEATHER_TABLE_database_field_names[] =
		{
			"NetworkID",
			"WeatherDate",
			"ETValue",
			"ETStatus",
			"RainValue",
			"RainStatus"
		};

	#endif

	// ----------

	// 9/17/2015 ajv : The beginning of the message includes the record count since the
	// WEATHER_TABLES_STRUCT isn't organized in a way where we could easy access a particular
	// day's record.
	memcpy( &lrecord_count, pucp, sizeof(UNS_32) );
	pucp += sizeof(UNS_32);
	rv += sizeof(UNS_32);

	memcpy( &ldate, pucp, sizeof(UNS_32) );
	pucp += sizeof(UNS_32);
	rv += sizeof(UNS_32);

	for( i = 0; i < lrecord_count; ++i )
	{
		memcpy( &let_entry, pucp, sizeof(ET_TABLE_ENTRY) );
		pucp += sizeof(ET_TABLE_ENTRY);
		rv += sizeof(ET_TABLE_ENTRY);

		memcpy( &lrain_entry, pucp, sizeof(RAIN_TABLE_ENTRY) );
		pucp += sizeof(RAIN_TABLE_ENTRY);
		rv += sizeof(RAIN_TABLE_ENTRY);

		#ifndef _MSC_VER

			// 9/16/2015 ajv : TODO Insert the record(s) into the ET and Rain table

		#else

			// 9/16/2015 ajv : Make sure the various SQL clause strings are fully initialized to NULL
			// since we rely on this to determine whether the string is empty or not.
			memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );
			memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
			memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
			memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
			memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

			// ----------

			// 9/16/2015 ajv : Build the search condition (e.g., "ON NetworkID=:NetworkID AND ...")
			SQL_MERGE_build_search_condition_uint32( WEATHER_TABLE_database_field_names[ WEATHER_TABLE_database_column_network_ID ], pnetwork_ID, pSQL_search_condition_str );

			SQL_MERGE_build_search_condition_date( WEATHER_TABLE_database_field_names[ WEATHER_TABLE_database_column_date ], ldate, pSQL_search_condition_str );

			// ----------

			// 9/16/2015 ajv : Populate the INSERT and VALUES clauses of the MERGE statement with the
			// values to insert.
			SQL_MERGE_build_upsert_uint32( pnetwork_ID, SQL_MERGE_include_field_name_in_insert_clause, WEATHER_TABLE_database_field_names[ WEATHER_TABLE_database_column_network_ID ], pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_date( ldate, SQL_MERGE_include_field_name_in_insert_clause, WEATHER_TABLE_database_field_names[ WEATHER_TABLE_database_column_date ], pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_uint32( (UNS_32)let_entry.et_inches_u16_10000u, SQL_MERGE_include_field_name_in_insert_clause, WEATHER_TABLE_database_field_names[ WEATHER_TABLE_database_column_et_value ], pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_uint32( (UNS_32)let_entry.status, SQL_MERGE_include_field_name_in_insert_clause, WEATHER_TABLE_database_field_names[ WEATHER_TABLE_database_column_et_status ], pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_uint32( (UNS_32)lrain_entry.rain_inches_u16_100u, SQL_MERGE_include_field_name_in_insert_clause, WEATHER_TABLE_database_field_names[ WEATHER_TABLE_database_column_rain_value ], pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_uint32( (UNS_32)lrain_entry.status, SQL_MERGE_include_field_name_in_insert_clause, WEATHER_TABLE_database_field_names[ WEATHER_TABLE_database_column_rain_status ], pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

			// ----------

			// 9/16/2015 ajv : Finally, build the MERGE statement and append it to the list of SQL
			// statements.
			SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements( DATABASE_TABLE_NAME_ET_RAIN_TABLE, pSQL_search_condition_str, SQL_MERGE_include_field_name_in_insert_clause, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf );

		#endif
		
		// ----------
		
		// 10/28/2015 rmd : Prepare for the next entry. Remember the ET/RAIN tables work backwards
		// in time.
		ldate -= 1;
	}

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 MOISTURE_RECORDER_extract_and_store_changes(	UNS_8 *pucp,
															const UNS_32 pdlen
															#ifdef _MSC_VER
															, const UNS_32 pnetwork_ID,
															const UNS_16 lastReportDate,
															const UNS_32 lastReportTime,
															char *pSQL_statements,
															char *pSQL_search_condition_str,
															char *pSQL_insert_values_str,
															char *pSQL_single_merge_statement_buf
															#endif
														  )
{
	MOISTURE_SENSOR_RECORDER_RECORD		msrr;

	UNS_32	lrecord_count;

	UNS_32	rv;

	UNS_32	i;

	// ----------

	rv = 0;

	// 3/25/2016 rmd : The content delivered is nothing but a blob of records.
	lrecord_count = (pdlen / sizeof(MOISTURE_SENSOR_RECORDER_RECORD));

	// ----------

	#ifdef _MSC_VER

		#define		MOISTURE_RECORDER_database_column_network_ID						(0)
		#define		MOISTURE_RECORDER_database_column_timestamp							(1)
		#define		MOISTURE_RECORDER_database_column_decoder_serial_number				(2)
		#define		MOISTURE_RECORDER_database_column_moisture_vwc_percentage			(3)
		#define		MOISTURE_RECORDER_database_column_temperature_fahrenheit			(4)
		#define		MOISTURE_RECORDER_database_column_conductivity_deci_siemen_per_m	(5)
		#define		MOISTURE_RECORDER_database_column_sensor_type						(6)

		static char* const MOISTURE_RECORDER_database_field_names[] =
		{
			"NetworkID",
			"RecordTimeStamp",
			"DecoderSerialNumber",
			"VWC_percentage",
			"Temp_fahrenheit",
			"EC_deci_Siemen_per_m",
			"SensorType"
		};

		// ----------

		BOOL_32 include_merge;

		DATE_TIME last_timestamp_DB;

		DATE_TIME newest_timestamp;

		last_timestamp_DB.D = lastReportDate;
		last_timestamp_DB.T = lastReportTime;

		newest_timestamp = last_timestamp_DB;

		char* lreport_last_timestamp_column_name = "LastTimestamp_MoistureRec";

		// ----------

	#endif

	// ----------

	for( i = 0; i < lrecord_count; ++i )
	{
		memcpy( &msrr, pucp, sizeof(MOISTURE_SENSOR_RECORDER_RECORD) );
		pucp += sizeof(MOISTURE_SENSOR_RECORDER_RECORD);
		rv += sizeof(MOISTURE_SENSOR_RECORDER_RECORD);

		#ifndef _MSC_VER

			// 9/16/2015 ajv : TODO Insert the record(s) into the battery-backed Moisture
			// Recorder structure.

		#else

			// 10/15/2015 ajv : Only try to insert valid records - that is records that have a valid
			// start date.
			if( msrr.dt.D != 0 )
			{
				// 9/16/2015 ajv : Make sure the various SQL clause strings are fully initialized to NULL
				// since we rely on this to determine whether the string is empty or not.
				memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );
				memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				// ----------

				// 1/16/2017 wjb : always save the latest timestamp to update the db with the newest timestamp.
				if (i == 0 || (DT1_IsBiggerThanOrEqualTo_DT2(&msrr.dt, &newest_timestamp)))
				{
					newest_timestamp = msrr.dt;
				}

				// ----------

				// 1/16/2017 wjb : include the Merge statement only if the record datetime is less than the last 
				// timestamp we have saved in the db, that's mean we may have this record been inserted in the db before.
				include_merge = DT1_IsBiggerThanOrEqualTo_DT2(&last_timestamp_DB, &msrr.dt);

				if (include_merge)
				{
					// 9/16/2015 ajv : Build the search condition (e.g., "ON NetworkID=:NetworkID AND ...")
					memset(pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE);

					SQL_MERGE_build_search_condition_uint32(MOISTURE_RECORDER_database_field_names[MOISTURE_RECORDER_database_column_network_ID], pnetwork_ID, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_timestamp(MOISTURE_RECORDER_database_field_names[MOISTURE_RECORDER_database_column_timestamp], msrr.dt, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_uint32(MOISTURE_RECORDER_database_field_names[MOISTURE_RECORDER_database_column_decoder_serial_number], msrr.decoder_serial_number, pSQL_search_condition_str);
				}

				// ----------

				// 9/16/2015 ajv : Populate the INSERT and VALUES clauses of the MERGE statement with the
				// values to insert.
				SQL_MERGE_build_insert_uint32_without_field_names( pnetwork_ID, pSQL_insert_values_str );
				SQL_MERGE_build_insert_timestamp_without_field_names(msrr.dt, pSQL_insert_values_str);
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)msrr.decoder_serial_number, pSQL_insert_values_str );
				SQL_MERGE_build_insert_float_without_field_names( (float)msrr.moisture_vwc_percentage, pSQL_insert_values_str );
				SQL_MERGE_build_insert_int32_without_field_names( (INT_32)msrr.temperature_fahrenheit, pSQL_insert_values_str );
				// 10/23/2018 rmd : Changed from an UNS_32 to a float by Ryan/Sara per AJ's direction.
				SQL_MERGE_build_insert_float_without_field_names( (float)msrr.latest_conductivity_reading_deci_siemen_per_m, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)msrr.sensor_type, pSQL_insert_values_str );

				// ----------

				if (include_merge)
				{
					// 9/16/2015 ajv : Finally, build the MERGE statement and append it to the list of SQL
					// statements.
					SQL_MERGE_build_insert_merge_and_append_to_SQL_statements(DATABASE_TABLE_NAME_MOISTURE_RECORDER, pSQL_search_condition_str, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
				}
				else
				{
					// 1/16/2017 wjb : Finally, build the INSERT statement and append it to the list of SQL
					// statements.
					SQL_INSERT_build_insert_and_append_to_SQL_statements(DATABASE_TABLE_NAME_MOISTURE_RECORDER, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
				}
			}

		#endif
	}
	
	// ----------

	#ifdef _MSC_VER
		// 1/16/2017 wjb : update the last timestamp in the db for this report with the latest timestamp
		update_cs3000_last_timestamp_in_network_table(pnetwork_ID, newest_timestamp, lreport_last_timestamp_column_name, pSQL_statements, pSQL_single_merge_statement_buf);
	#endif

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 BUDGET_REPORT_DATA_extract_and_store_changes( UNS_8 *pucp,
															const UNS_32 pdlen
															#ifdef _MSC_VER
															, const UNS_32 pnetwork_ID,
															const UNS_16 lastReportDate,
															char *pSQL_statements,
															char *pSQL_search_condition_str,
															char *pSQL_insert_values_str,
															char *pSQL_single_merge_statement_buf
															#endif
														  )
{
	BUDGET_REPORT_RECORD	brr;

	UNS_32	lrecord_count;

	UNS_32	rv;

	UNS_32	i;

	// ----------

	rv = 0;

	lrecord_count = (pdlen / sizeof(BUDGET_REPORT_RECORD));

	// ----------

	#ifdef _MSC_VER

	SYSTEM_BUDGET_REPORT_RECORD* psbrr;

	BY_POC_BUDGET_RECORD* pbpbr;

	#define		BUDGET_REPORT_DATA_database_column_network_ID				(0)
	#define		BUDGET_REPORT_DATA_database_column_system_gid				(1)
	#define		BUDGET_REPORT_DATA_database_column_poc_gid					(2)
	#define		BUDGET_REPORT_DATA_database_column_record_date				(3)
	#define		BUDGET_REPORT_DATA_database_column_in_use					(4)
	#define		BUDGET_REPORT_DATA_database_column_start_date				(5)
	#define		BUDGET_REPORT_DATA_database_column_end_date					(6)
	#define		BUDGET_REPORT_DATA_database_column_meter_read_time			(7)
	#define		BUDGET_REPORT_DATA_database_column_predicted_use			(8)
	#define		BUDGET_REPORT_DATA_database_column_reduction_gallons		(9)
	#define		BUDGET_REPORT_DATA_database_column_ratio					(10)
	#define		BUDGET_REPORT_DATA_database_column_closing_record			(11)
	#define		BUDGET_REPORT_DATA_database_column_budget					(12)
	#define		BUDGET_REPORT_DATA_database_column_used_total_gallons		(13)
	#define		BUDGET_REPORT_DATA_database_column_used_irrigation_gallons	(14)
	#define		BUDGET_REPORT_DATA_database_column_used_mvor_gallons		(15)
	#define		BUDGET_REPORT_DATA_database_column_used_idle_gallons		(16)
	#define		BUDGET_REPORT_DATA_database_column_used_programmed_gallons	(17)
	#define		BUDGET_REPORT_DATA_database_column_used_manual_programmed_gallons	(18)
	#define		BUDGET_REPORT_DATA_database_column_used_manual_gallons		(19)
	#define		BUDGET_REPORT_DATA_database_column_used_walkthru_gallons	(20)
	#define		BUDGET_REPORT_DATA_database_column_used_test_gallons		(21)
	#define		BUDGET_REPORT_DATA_database_column_used_mobile_gallons		(22)
	#define		BUDGET_REPORT_DATA_database_column_on_at_start_time			(23)
	#define		BUDGET_REPORT_DATA_database_column_off_at_start_time		(24)

	static char* const BUDGET_REPORT_DATA_database_field_names[] =
	{
		"NetworkID",
		"SystemGID",
		"PocGID",
		"RecordDate",
		"InUse",
		"StartDate",
		"EndDate",
		"MeterReadTime",
		"PredictedUse",
		"ReductionGallons",
		"Ratio",
		"ClosingRecord",
		"Budget",
		"UsedTotalGallons",
		"UsedIrrigationGallons",
		"UsedMVORGallons",
		"UsedIdleGallons",
		"UsedProgrammedGallons",
		"UsedManualProgrammedGallons",
		"UsedManualGallons",
		"UsedWalkthruGallons",
		"UsedTestGallons",
		"UsedMobileGallons",
		"OnAtStartTime",
		"OffAtStartTime"
	};

	// ----------

	BOOL_32 include_merge;

	DATE_TIME newest_timestamp;

	newest_timestamp.D = lastReportDate;
	newest_timestamp.T = 0;

	char* lreport_last_timestamp_column_name = "LastTimestamp_BudgetReport";

	// ----------

	#endif

	// ----------
	
	for( i = 0; i < lrecord_count; ++i )
	{
		memcpy( &brr, pucp, sizeof(BUDGET_REPORT_RECORD) );
		pucp += sizeof(BUDGET_REPORT_RECORD);
		rv += sizeof(BUDGET_REPORT_RECORD);

		#ifndef _MSC_VER

			// 4/05/2016 skc : TODO Insert the record(s) into the budget report data file and trigger a
			// file save

		#else

			// 4/06/2016 skc : Send one record for each possible POC, nothing if the POC is not used.
			for( UNS_32 idx_poc = 0; idx_poc < MAX_POCS_IN_NETWORK; idx_poc++ )
			{
				// 4/07/2016 skc : Get pointers to structure members
				psbrr = &brr.sbrr;
				pbpbr = &brr.bpbr[idx_poc]; // one for each possible POC

				// 1/16/2017 wjb : always save the latest timestamp to update the db with the newest timestamp.
				if (i == 0 || (brr.record_date>= lastReportDate))
				{
					newest_timestamp.D = brr.record_date;
				}

				// ----------

				// 4/06/2016 skc : If the POC is not used, don't import it
				if( pbpbr->poc_gid == 0 )
					continue;


				// 4/06/2016 skc : Make sure the various SQL clause strings are fully initialized to NULL
				// since we rely on this to determine whether the string is empty or not.
				memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );
				memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

				// ----------

				// 1/16/2017 wjb : include the Merge statement only if the record datetime is less than the last 
				// timestamp we have saved in the db, that's mean we may have this record been inserted in the db before.
				include_merge = (lastReportDate >= brr.record_date);

				if (include_merge)
				{
					// 9/16/2015 ajv : Build the search condition (e.g., "ON NetworkID=:NetworkID AND ...")
					memset(pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE);

					// 4/06/2016 skc : Build the search condition (e.g., "ON SystemGID=:SystemGID AND ...")
					SQL_MERGE_build_search_condition_uint32(BUDGET_REPORT_DATA_database_field_names[BUDGET_REPORT_DATA_database_column_network_ID], pnetwork_ID, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_uint32(BUDGET_REPORT_DATA_database_field_names[BUDGET_REPORT_DATA_database_column_system_gid], psbrr->system_gid, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_uint32(BUDGET_REPORT_DATA_database_field_names[BUDGET_REPORT_DATA_database_column_poc_gid], pbpbr->poc_gid, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_date(BUDGET_REPORT_DATA_database_field_names[BUDGET_REPORT_DATA_database_column_record_date], brr.record_date, pSQL_search_condition_str);
				}

				// ----------

				// 4/06/2016 skc : Populate the INSERT and VALUES clauses of the MERGE statement with the
				// values to insert.
				SQL_MERGE_build_insert_uint32_without_field_names( pnetwork_ID, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( psbrr->system_gid, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( pbpbr->poc_gid, pSQL_insert_values_str );
				SQL_MERGE_build_insert_date_without_field_names( brr.record_date, pSQL_insert_values_str );

				SQL_MERGE_build_insert_uint32_without_field_names( psbrr->in_use, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( psbrr->mode, pSQL_insert_values_str );
				//SQL_MERGE_build_insert_timestamp_without_field_names( ldt, pSQL_insert_values_str );
				SQL_MERGE_build_insert_date_without_field_names( psbrr->start_date, pSQL_insert_values_str );
				SQL_MERGE_build_insert_date_without_field_names( psbrr->end_date, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( psbrr->meter_read_time, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( psbrr->predicted_use_to_end_of_period, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( psbrr->reduction_gallons, pSQL_insert_values_str );

				// 7/8/2016 skc : Trap possible divide by zeros (-1.#INF), the database doesn't like INF values.
				// 3/1/2016 skc : Might as well trap #Nan also.
				if( !isnormal( psbrr->ratio ) )
				{
					psbrr->ratio = 0.f;
				}

				SQL_MERGE_build_insert_float_without_field_names( psbrr->ratio, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( psbrr->closing_record_for_the_period, pSQL_insert_values_str );

				SQL_MERGE_build_insert_uint32_without_field_names( pbpbr->budget, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_total_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_irrigation_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_mvor_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_idle_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_programmed_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_manual_programmed_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_manual_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_walkthru_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_test_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_mobile_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( pbpbr->on_at_start_time, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( pbpbr->off_at_start_time, pSQL_insert_values_str );

				// ----------

				if (include_merge)
				{
					// 9/16/2015 ajv : Build the MERGE statement and append it to the list of SQL statements.
					SQL_MERGE_build_insert_merge_and_append_to_SQL_statements(DATABASE_TABLE_NAME_BUDGET_REPORT_DATA, pSQL_search_condition_str, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
				}
				else
				{
					SQL_INSERT_build_insert_and_append_to_SQL_statements(DATABASE_TABLE_NAME_BUDGET_REPORT_DATA, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
				}
			}

		#endif
	}

	// ----------

	#ifdef _MSC_VER
		// 1/16/2017 wjb : update the last timestamp in the db for this report with the latest timestamp
		update_cs3000_last_timestamp_in_network_table(pnetwork_ID, newest_timestamp, lreport_last_timestamp_column_name, pSQL_statements, pSQL_single_merge_statement_buf);
	#endif

	// ----------

	return( rv );
}

static UNS_32 BUDGET_REPORT_DATA_extract_and_store_changes2( UNS_8 *pucp,
															const UNS_32 pdlen
															#ifdef _MSC_VER
															, const UNS_32 pnetwork_ID,
															const UNS_16 lastReportDate,
															char *pSQL_statements,
															char *pSQL_search_condition_str,
															char *pSQL_insert_values_str,
															char *pSQL_single_merge_statement_buf
															#endif
														  )
{
	BUDGET_REPORT_RECORD	brr;// = {0};

	UNS_32	rv;

	//UNS_32	i;

	UNS_32 num_poc;

	// ----------

	rv = 0;

	// ----------

	#ifdef _MSC_VER

	SYSTEM_BUDGET_REPORT_RECORD* psbrr;

	BY_POC_BUDGET_RECORD* pbpbr;

	#define		BUDGET_REPORT_DATA_database_column_network_ID				(0)
	#define		BUDGET_REPORT_DATA_database_column_system_gid				(1)
	#define		BUDGET_REPORT_DATA_database_column_poc_gid					(2)
	#define		BUDGET_REPORT_DATA_database_column_record_date				(3)
	#define		BUDGET_REPORT_DATA_database_column_in_use					(4)
	#define		BUDGET_REPORT_DATA_database_column_start_date				(5)
	#define		BUDGET_REPORT_DATA_database_column_end_date					(6)
	#define		BUDGET_REPORT_DATA_database_column_meter_read_time			(7)
	#define		BUDGET_REPORT_DATA_database_column_predicted_use			(8)
	#define		BUDGET_REPORT_DATA_database_column_reduction_gallons		(9)
	#define		BUDGET_REPORT_DATA_database_column_ratio					(10)
	#define		BUDGET_REPORT_DATA_database_column_closing_record			(11)
	#define		BUDGET_REPORT_DATA_database_column_budget					(12)
	#define		BUDGET_REPORT_DATA_database_column_used_total_gallons		(13)
	#define		BUDGET_REPORT_DATA_database_column_used_irrigation_gallons	(14)
	#define		BUDGET_REPORT_DATA_database_column_used_mvor_gallons		(15)
	#define		BUDGET_REPORT_DATA_database_column_used_idle_gallons		(16)
	#define		BUDGET_REPORT_DATA_database_column_used_programmed_gallons	(17)
	#define		BUDGET_REPORT_DATA_database_column_used_manual_programmed_gallons	(18)
	#define		BUDGET_REPORT_DATA_database_column_used_manual_gallons		(19)
	#define		BUDGET_REPORT_DATA_database_column_used_walkthru_gallons	(20)
	#define		BUDGET_REPORT_DATA_database_column_used_test_gallons		(21)
	#define		BUDGET_REPORT_DATA_database_column_used_mobile_gallons		(22)
	#define		BUDGET_REPORT_DATA_database_column_on_at_start_time			(23)
	#define		BUDGET_REPORT_DATA_database_column_off_at_start_time		(24)

	static char* const BUDGET_REPORT_DATA_database_field_names[] =
	{
		"NetworkID",
		"SystemGID",
		"PocGID",
		"RecordDate",
		"InUse",
		"StartDate",
		"EndDate",
		"MeterReadTime",
		"PredictedUse",
		"ReductionGallons",
		"Ratio",
		"ClosingRecord",
		"Budget",
		"UsedTotalGallons",
		"UsedIrrigationGallons",
		"UsedMVORGallons",
		"UsedIdleGallons",
		"UsedProgrammedGallons",
		"UsedManualProgrammedGallons",
		"UsedManualGallons",
		"UsedWalkthruGallons",
		"UsedTestGallons",
		"UsedMobileGallons",
		"OnAtStartTime",
		"OffAtStartTime"
	};

	// ----------

	BOOL_32 include_merge;

	DATE_TIME newest_timestamp;

	newest_timestamp.D = 0; //lastReportDate;
	newest_timestamp.T = 0;

	char* lreport_last_timestamp_column_name = "LastTimestamp_BudgetReport";

	// ----------

	#endif

	// ----------
	
	while( rv < pdlen )
	{
		//memcpy( &brr, pucp, sizeof(BUDGET_REPORT_RECORD) );
		//pucp += sizeof(BUDGET_REPORT_RECORD);
		//rv += sizeof(BUDGET_REPORT_RECORD);

		memcpy( &brr.sbrr, pucp, sizeof(SYSTEM_BUDGET_REPORT_RECORD) );
		pucp += sizeof(SYSTEM_BUDGET_REPORT_RECORD);
		rv += sizeof(SYSTEM_BUDGET_REPORT_RECORD);

		memcpy( &num_poc, pucp, sizeof(num_poc) );
		pucp += sizeof(num_poc);
		rv += sizeof(num_poc);

		memcpy( brr.bpbr, pucp, num_poc * sizeof(BY_POC_BUDGET_RECORD) );
		pucp += num_poc * sizeof(BY_POC_BUDGET_RECORD);
		rv += num_poc * sizeof(BY_POC_BUDGET_RECORD);

		memcpy( &brr.record_date, pucp, sizeof(brr.record_date) );
		pucp += sizeof(brr.record_date);
		rv += sizeof(brr.record_date);

		#ifndef _MSC_VER

			// 4/05/2016 skc : TODO Insert the record(s) into the budget report data file and trigger a
			// file save

		#else

			// 4/06/2016 skc : Send one record for each possible POC, nothing if the POC is not used.
			for( UNS_32 idx_poc = 0; idx_poc < num_poc; idx_poc++ )
			{
				// 4/07/2016 skc : Get pointers to structure members
				psbrr = &brr.sbrr;
				pbpbr = &brr.bpbr[idx_poc]; // one for each possible POC

				// 1/16/2017 wjb : always save the latest timestamp to update the db with the newest timestamp.
				//if (i == 0 || (brr.record_date>= lastReportDate))
				if (newest_timestamp.D == 0 || (brr.record_date>= lastReportDate))
				{
					newest_timestamp.D = brr.record_date;
				}

				// ----------

				// 4/06/2016 skc : If the POC is not used, don't import it
				// 2/27/2017 skc : Don't need this anymore
				if( pbpbr->poc_gid == 0 )
					continue;


				// 4/06/2016 skc : Make sure the various SQL clause strings are fully initialized to NULL
				// since we rely on this to determine whether the string is empty or not.
				memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );
				memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

				// ----------

				// 1/16/2017 wjb : include the Merge statement only if the record datetime is less than the last 
				// timestamp we have saved in the db, that's mean we may have this record been inserted in the db before.
				include_merge = (lastReportDate >= brr.record_date);

				if (include_merge)
				{
					// 9/16/2015 ajv : Build the search condition (e.g., "ON NetworkID=:NetworkID AND ...")
					memset(pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE);

					// 4/06/2016 skc : Build the search condition (e.g., "ON SystemGID=:SystemGID AND ...")
					SQL_MERGE_build_search_condition_uint32(BUDGET_REPORT_DATA_database_field_names[BUDGET_REPORT_DATA_database_column_network_ID], pnetwork_ID, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_uint32(BUDGET_REPORT_DATA_database_field_names[BUDGET_REPORT_DATA_database_column_system_gid], psbrr->system_gid, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_uint32(BUDGET_REPORT_DATA_database_field_names[BUDGET_REPORT_DATA_database_column_poc_gid], pbpbr->poc_gid, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_date(BUDGET_REPORT_DATA_database_field_names[BUDGET_REPORT_DATA_database_column_record_date], brr.record_date, pSQL_search_condition_str);
				}

				// ----------

				// 4/06/2016 skc : Populate the INSERT and VALUES clauses of the MERGE statement with the
				// values to insert.
				SQL_MERGE_build_insert_uint32_without_field_names( pnetwork_ID, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( psbrr->system_gid, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( pbpbr->poc_gid, pSQL_insert_values_str );
				SQL_MERGE_build_insert_date_without_field_names( brr.record_date, pSQL_insert_values_str );

				SQL_MERGE_build_insert_uint32_without_field_names( psbrr->in_use, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( psbrr->mode, pSQL_insert_values_str );
				//SQL_MERGE_build_insert_timestamp_without_field_names( ldt, pSQL_insert_values_str );
				SQL_MERGE_build_insert_date_without_field_names( psbrr->start_date, pSQL_insert_values_str );
				SQL_MERGE_build_insert_date_without_field_names( psbrr->end_date, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( psbrr->meter_read_time, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( psbrr->predicted_use_to_end_of_period, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( psbrr->reduction_gallons, pSQL_insert_values_str );

				// 7/8/2016 skc : Trap possible divide by zeros (-1.#INF), the database doesn't like INF values.
				// 3/1/2016 skc : Might as well trap #Nan also.
				if( !isnormal( psbrr->ratio ) )
				{
					psbrr->ratio = 0.f;
				}

				SQL_MERGE_build_insert_float_without_field_names( psbrr->ratio, pSQL_insert_values_str );
				SQL_MERGE_build_insert_bool_without_field_names( psbrr->closing_record_for_the_period, pSQL_insert_values_str );

				SQL_MERGE_build_insert_uint32_without_field_names( pbpbr->budget, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_total_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_irrigation_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_mvor_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_idle_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_programmed_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_manual_programmed_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_manual_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_walkthru_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_test_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( (UNS_32)pbpbr->used_mobile_gallons, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( pbpbr->on_at_start_time, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( pbpbr->off_at_start_time, pSQL_insert_values_str );

				// ----------

				if (include_merge)
				{
					// 9/16/2015 ajv : Build the MERGE statement and append it to the list of SQL statements.
					SQL_MERGE_build_insert_merge_and_append_to_SQL_statements(DATABASE_TABLE_NAME_BUDGET_REPORT_DATA, pSQL_search_condition_str, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
				}
				else
				{
					SQL_INSERT_build_insert_and_append_to_SQL_statements(DATABASE_TABLE_NAME_BUDGET_REPORT_DATA, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
				}
			}

		#endif
	}

	// ----------

	#ifdef _MSC_VER
		// 1/16/2017 wjb : update the last timestamp in the db for this report with the latest timestamp
		update_cs3000_last_timestamp_in_network_table(pnetwork_ID, newest_timestamp, lreport_last_timestamp_column_name, pSQL_statements, pSQL_single_merge_statement_buf);
	#endif

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
DLLEXPORT UNS_32 REPORTS_extract_and_store_changes( const UNS_32 pMID,
													UNS_8 *pucp,
													const UNS_32 pdata_len
													#ifdef _MSC_VER
													, const UNS_32 pnetwork_ID,
													const UNS_16 lastReportDate,
													const UNS_32 lastReportTime,
													char *pSQL_statements,
													char *pSQL_search_condition_str,
													char *pSQL_update_str,
													char *pSQL_insert_fields_str,
													char *pSQL_insert_values_str,
													char *pSQL_single_merge_statement_buf
													#endif
												  )
{
	UNS_32	rv;

	// ----------

	rv = 0;

	// ----------
	
	#ifdef _MSC_VER

	memset( pSQL_statements, 0x00, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS );

	// 12/4/2015 ajv : In most cases, we don't perform an update - just a straight insert if the
	// record doesn't exist. However, for the case of the ET & Rain table, we send two records
	// and need to make sure slot 1's record is updated properly. To support this, Wissam has
	// added the pSQL_update_str buffer. However, since we only use it in one case, let's NULL
	// it just in case we get something that's not already NULL.
	memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

	// 12/4/2015 ajv : Same as above applies to the pSQL_insert_fields_str buffer.
	memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

	#endif

	switch( pMID )
	{
		case MID_TO_COMMSERVER_FLOW_RECORDING_data_packet:
		case MID_TO_COMMSERVER_FLOW_RECORDING_init_packet:
			rv += FLOW_RECORDING_extract_and_store_changes( pucp,
															pdata_len
															#ifdef _MSC_VER
															, pnetwork_ID,
															lastReportDate,
															lastReportTime,
															pSQL_statements,
															pSQL_search_condition_str,
															pSQL_insert_values_str,
															pSQL_single_merge_statement_buf
															#endif
														  );
			break;

		case MID_TO_COMMSERVER_STATION_HISTORY_init_packet:
		case MID_TO_COMMSERVER_STATION_HISTORY_data_packet:
			rv += STATION_HISTORY_extract_and_store_changes( pucp,
															 pdata_len
															 #ifdef _MSC_VER
															 , pnetwork_ID,
															 lastReportDate,
															 lastReportTime,
															 pSQL_statements,
															 pSQL_search_condition_str,
															 pSQL_insert_values_str,
															 pSQL_single_merge_statement_buf
															  #endif
															);
			break;

		case MID_TO_COMMSERVER_STATION_REPORT_DATA_init_packet:
		case MID_TO_COMMSERVER_STATION_REPORT_DATA_data_packet:
			rv += STATION_REPORT_DATA_extract_and_store_changes( pucp,
																 pdata_len
																 #ifdef _MSC_VER
																 , pnetwork_ID,
																 lastReportDate,
																 lastReportTime,
																 pSQL_statements,
																 pSQL_search_condition_str,
																 pSQL_insert_values_str,
																 pSQL_single_merge_statement_buf
																 #endif
															   );
			break;

		case MID_TO_COMMSERVER_POC_REPORT_DATA_init_packet:
		case MID_TO_COMMSERVER_POC_REPORT_DATA_data_packet:
			rv += POC_REPORT_DATA_extract_and_store_changes( pucp,
															 pdata_len
															 #ifdef _MSC_VER
															 , pnetwork_ID,
															 lastReportDate,
															 lastReportTime,
															 pSQL_statements,
															 pSQL_search_condition_str,
															 pSQL_insert_values_str,
															 pSQL_single_merge_statement_buf
															 #endif
														   );
			break;

		case MID_TO_COMMSERVER_SYSTEM_REPORT_DATA_init_packet:
		case MID_TO_COMMSERVER_SYSTEM_REPORT_DATA_data_packet:
			rv += SYSTEM_REPORT_DATA_extract_and_store_changes( pucp,
																pdata_len
																#ifdef _MSC_VER
																, pnetwork_ID,
																lastReportDate,
																lastReportTime,
																pSQL_statements,
																pSQL_search_condition_str,
																pSQL_insert_values_str,
																pSQL_single_merge_statement_buf
																#endif
															  );
			break;

		case MID_TO_COMMSERVER_LIGHTS_REPORT_DATA_init_packet:
		case MID_TO_COMMSERVER_LIGHTS_REPORT_DATA_data_packet:
			rv += LIGHTS_REPORT_DATA_extract_and_store_changes( pucp,
																pdata_len
																#ifdef _MSC_VER
																, pnetwork_ID,
																lastReportDate,
																lastReportTime,
																pSQL_statements,
																pSQL_search_condition_str,
																pSQL_insert_values_str,
																pSQL_single_merge_statement_buf
																#endif
															  );
			break;

		case MID_TO_COMMSERVER_ET_AND_RAIN_TABLE_init_packet:
		case MID_TO_COMMSERVER_ET_AND_RAIN_TABLE_data_packet:
			rv += WEATHER_TABLES_extract_and_store_changes( pucp,
															pdata_len
															#ifdef _MSC_VER
															, pnetwork_ID,
															pSQL_statements,
															pSQL_search_condition_str,
															pSQL_update_str,
															pSQL_insert_fields_str,
															pSQL_insert_values_str,
															pSQL_single_merge_statement_buf
															#endif
														  );
			break;

		case MID_TO_COMMSERVER_MOISTURE_SENSOR_RECORDER_init_packet:
		case MID_TO_COMMSERVER_MOISTURE_SENSOR_RECORDER_data_packet:
			rv += MOISTURE_RECORDER_extract_and_store_changes(	pucp,
																pdata_len
																#ifdef _MSC_VER
																, pnetwork_ID,
																lastReportDate,
																lastReportTime,
																pSQL_statements,
																pSQL_search_condition_str,
																pSQL_insert_values_str,
																pSQL_single_merge_statement_buf
																#endif
															 );
			break;
		case MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet:
		case MID_TO_COMMSERVER_BUDGET_REPORT_DATA_data_packet:
			rv += BUDGET_REPORT_DATA_extract_and_store_changes( pucp,
																pdata_len
																#ifdef _MSC_VER
																, pnetwork_ID,
																lastReportDate,
																pSQL_statements,
																pSQL_search_condition_str,
																pSQL_insert_values_str,
																pSQL_single_merge_statement_buf
																#endif
															  );
			break;
		case MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet2:
		case MID_TO_COMMSERVER_BUDGET_REPORT_DATA_data_packet2:
			rv += BUDGET_REPORT_DATA_extract_and_store_changes2( pucp,
																pdata_len
																#ifdef _MSC_VER
																, pnetwork_ID,
																lastReportDate,
																pSQL_statements,
																pSQL_search_condition_str,
																pSQL_insert_values_str,
																pSQL_single_merge_statement_buf
																#endif
															  );
		break;

	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// 4/20/2016 skc : Add the specified amount of flow to the budget report record
static void add_amount_to_budget_record( double amount_to_add, BY_POC_RECORD *precord )
{
	UNS_32 run_reason;

	// ----------
	 
	// 4/19/2016 skc : We track different types of usage for budgets.
	// Use the reason to fully specify the usage.
	run_reason = precord->this_pocs_system_preserves_ptr->reason_in_running_list;

	precord->budget.used_irrigation_gallons += amount_to_add;

	// 5/18/2016 rmd : The 'else' between the reasons in the list is CRITICAL because we
	// could have more than 1 bit at a time set. Specifically for the reasons of
	// PROGRAMMED_IRRIGATION and MANUAL_PROGRAMS. Yes they can irrigate at the same time.
	// In the case of more than one set the else serves to prioritize the use to
	// PROGRAMMED_IRRIGATION and to not double add the use. Counting it all towards
	// one category okay.

	if( B_IS_SET(run_reason, IN_LIST_FOR_PROGRAMMED_IRRIGATION) )
	{
		precord->budget.used_programmed_gallons += amount_to_add;
	}
	else if( B_IS_SET(run_reason, IN_LIST_FOR_MANUAL_PROGRAM) )
	{
		precord->budget.used_manual_programmed_gallons += amount_to_add;
	}
	else if( B_IS_SET(run_reason, IN_LIST_FOR_MANUAL) )
	{
		precord->budget.used_manual_gallons += amount_to_add;
	}
	else if( B_IS_SET(run_reason, IN_LIST_FOR_WALK_THRU) )
	{
		precord->budget.used_walkthru_gallons += amount_to_add;
	}
	else if( B_IS_SET(run_reason, IN_LIST_FOR_TEST) )
	{
		precord->budget.used_test_gallons += amount_to_add;
	}
	else if( B_IS_SET(run_reason, IN_LIST_FOR_MOBILE) )
	{
		precord->budget.used_mobile_gallons += amount_to_add;
	}
}


/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called at a one hertz rate to add to the report data accumulators. But only
    if we are making tokens ie we are a master.
	
	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task. Called at a one hertz
    rate.
	
	@RETURN (none)

	@ORIGINAL 2012.06.04 rmd

	@REVISIONS (none)
*/
extern void REPORT_DATA_maintain_all_accumulators( DATE_TIME *pdt_ptr )
{
	UNS_32	sss;
	
	UNS_32	ppp;
	
	UNS_32	fff;
	
	IRRIGATION_LIST_COMPONENT	*lilc;
	
	LIGHTS_LIST_COMPONENT 		*lllc;

	float	amount_to_add;

	UNS_32	lights_array_index;

	BY_POC_RECORD *precord;	// convenience pointer

	// ----------
	
	// 7/19/2012 rmd : Is it time to take the station_preserves records for the active stations
	// in the network and add them to the file.
	if( pdt_ptr->T == station_report_data_completed.rdfb.roll_time )
	{
		// 8/1/2012 rmd : We are potentially going to close the station report rip.
		xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );
		
		for( sss=0; sss<(STATION_PRESERVE_ARRAY_SIZE); sss++ )
		{
			// 10/15/2015 rmd : Review when this flag is set and what happens to stations taken out of
			// use ie no longer physically available. Do they keep on closing and starting another new
			// record? Meaning once we make one record for a station we never stop? WHICH IS WRONG!!!!!
			// Stations no longer present should not keep on making records. We should close what is in
			// process and stop.
			if( station_preserves.sps[ sss ].spbf.station_report_data_record_is_in_use == (true) )
			{
				// 7/19/2012 rmd : Close the record and add it to the file. Doing so cleans the battery
				// backed record we are sending to the file.
				nm_STATION_REPORT_DATA_close_and_start_a_new_record( &(station_preserves.sps[ sss ].station_report_data_rip), pdt_ptr );

				// 7/20/2012 rmd : Also clear the in use flag. Which will be set again 1 second from now if
				// the station is still exists. This flag is not an indication of the classic 'station in
				// use' setting (used to hide stations from the display).
				station_preserves.sps[ sss ].spbf.station_report_data_record_is_in_use = (false);
			}
			
		}
		
		xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );

	}
	
	// --------------------------

	if( pdt_ptr->T == poc_report_data_completed.rdfb.roll_time )
	{
		// 8/1/2012 rmd : We are going to use and write to the poc rip.
		xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );
	
		for( ppp=0; ppp<MAX_POCS_IN_NETWORK; ppp++ )
		{
			// If the record is in use.
			if( poc_preserves.poc[ ppp ].poc_gid != 0 )
			{
				nm_POC_REPORT_DATA_close_and_start_a_new_record( &(poc_preserves.poc[ ppp ]), pdt_ptr );
			}
	
		}

		xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
	}
	
	// --------------------------

	xSemaphoreTakeRecursive( lights_report_completed_records_recursive_MUTEX, portMAX_DELAY );

	// 9/10/2015 mpd : TODO: This needs protection in case the system is down at midnight.
	if( pdt_ptr->T == lights_report_data_completed.rdfb.roll_time )
	{
		// 8/1/2012 rmd : We are going to use and write to the lights lrr.
		xSemaphoreTakeRecursive( lights_preserves_recursive_MUTEX, portMAX_DELAY );

		for( ppp = 0; ppp < MAX_LIGHTS_IN_NETWORK; ++ppp )
		{
			// If the record is in use.
			// 10/15/2015 rmd : Also should question the use of this term here. SEE STATION REPORT DATA
			// ABOVE.
			if( lights_preserves.lbf[ ppp ].light_is_available )
			{
				nm_LIGHTS_REPORT_DATA_close_and_start_a_new_record( &( lights_preserves.lights[ ppp ].lrr ), pdt_ptr );
			}
		}

		xSemaphoreGiveRecursive( lights_preserves_recursive_MUTEX );
	}

	xSemaphoreGiveRecursive( lights_report_completed_records_recursive_MUTEX );

	// --------------------------

	if( pdt_ptr->T == system_report_data_completed.rdfb.roll_time )
	{
		// 8/1/2012 rmd : We are going to use and write to the system rip.
		xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
	
		for( sss=0; sss<MAX_POSSIBLE_SYSTEMS; sss++ )
		{
			// If the record is in use.
			if( system_preserves.system[ sss ].system_gid != 0 )
			{
				nm_SYSTEM_REPORT_close_and_start_a_new_record( &(system_preserves.system[ sss ]), pdt_ptr );
			}
	
		}

		xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
	}
	
	// --------------------------

	// 9/5/2012 rmd : We are manipulating the station preserves rip.
	xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

	// 6/27/2012 rmd : We are going to traverse the ON list. 
	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );

	// 6/27/2012 rmd : And we are going to add to the system accumulators.
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
	
	// --------------------------
	// 4/11/2016 skc : Initialize to 0
	for( sss=0; sss<MAX_POSSIBLE_SYSTEMS; sss++ )
	{
		if( system_preserves.system[ sss ].system_gid != 0 )
		{
			system_preserves.system[ sss ].reason_in_running_list = 0;
		}
	}

	if( foal_irri.list_of_foal_stations_ON.count > 0 )
	{
		lilc = nm_ListGetFirst( &(foal_irri.list_of_foal_stations_ON) );

		while( lilc != NULL )
		{
			// 4/11/2016 skc : Set the reason this station is running. A station can be in the running
			// list at most one time.
			//
			// 5/18/2016 rmd : While this is okay because of the way the function that assigns the flow
			// uses this bitfield, just NOTE - the result of setting the bit like this for each station
			// in the ON list is we may end up with the PROGRAMMED_IRRIGATION bit set AND the MANUAL_PROGRAMS
			// bit set. Yes both bits set - because these two reasons for irrigation can co-mingle. Meaning
			// stations can be simultaneously ON for both those types of irrigation. This is only true for
			// these irrigation reasons.
			B_SET( lilc->bsr->reason_in_running_list, lilc->bbf.w_reason_in_list );

			// ----------

			// 6/3/2014 rmd : Remember this function runs at a one hertz rate. However not every second
			// is there flow available to add. This is due to the nature of the token rate coupled with
			// the poc decoder flow data gathering - which occurs something like once every 10 seconds.

			// 3/11/2016 rmd : If there are missing flow meters in the system, that is pocs without a
			// flow meter, we drive accumulators with the expecteds.
			// 8/1/2016 skc : If we have a system with and without flow meters (Bogus situation),
			// then we ignore the POCs without flow meters.

			// If there are NO flow meters in this system
			if( lilc->bsr->sbf.number_of_flow_meters_in_this_sys == 0 )
			{
				// 6/3/2014 rmd : This means there are no flow meters in the system. So the share of flow
				// for each station is equal to their expected flow rate. That's all there is to that. And
				// every second we will add incrementally.
				amount_to_add = (lilc->expected_flow_rate_gpm_u16 / 60.0);

				// 8/01/2016 skc : For BUDGETS, with no flow meters in the system we use the expected and
				// divide it up equally among each of the POC's. Because we have no measured flow, this
				// is where the flow volume data is added to poc_preserves records.

				xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

				// Adjust the POC used gallons by its portion (1/n)
				for( ppp=0; ppp<MAX_POCS_IN_NETWORK; ++ppp )
				{
					if( (poc_preserves.poc[ ppp ].poc_gid != 0) &&
						(poc_preserves.poc[ ppp ].this_pocs_system_preserves_ptr->system_gid == lilc->bsr->system_gid) )
					{
						poc_preserves.poc[ ppp ].budget.used_total_gallons += (amount_to_add / lilc->bsr->sbf.number_of_pocs_in_this_system);

						precord = &poc_preserves.poc[ ppp ];
						add_amount_to_budget_record( amount_to_add / lilc->bsr->sbf.number_of_pocs_in_this_system, precord );
					}
				}

				xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
			}
			else// at least one POC has a flow meter
			{
				// 7/17/2015 rmd : Figure up the amount of gallons to add by station. Protect against the
				// divide by zero. Actually test all the terms for safety and/or to help produce the desired
				// results.
				// 8/1/2016 skc : The only flow to be recorded is when the accumulators have data. POCs
				// without a flow meter do not put data into the accumulators, thus the code will never make
				// it past this check.
				if( (lilc->expected_flow_rate_gpm_u16 > 0) && 
					(lilc->bsr->ufim_expected_flow_rate_for_those_ON > 0) && 
					(lilc->bsr->accumulated_gallons_for_accumulators_foal > 0.0) )
				{
					amount_to_add = ((float)(lilc->expected_flow_rate_gpm_u16) / (float)(lilc->bsr->ufim_expected_flow_rate_for_those_ON)) * lilc->bsr->accumulated_gallons_for_accumulators_foal;
				}
				else // we measured no flow
				{
					amount_to_add = 0.0;
				}
			}

			// ----------

			switch( lilc->bbf.w_reason_in_list )
			{
				case IN_LIST_FOR_PROGRAMMED_IRRIGATION:
					station_preserves.sps[ lilc->station_preserves_index ].station_history_rip.pi_last_cycle_end_date = pdt_ptr->D;
	
					station_preserves.sps[ lilc->station_preserves_index ].station_history_rip.pi_last_cycle_end_time = pdt_ptr->T;
					
					// ---------------------------
	
					station_preserves.sps[ lilc->station_preserves_index ].station_history_rip.pi_seconds_irrigated_ul += 1;
	
					station_preserves.sps[ lilc->station_preserves_index ].station_history_rip.pi_gallons_irrigated_fl += amount_to_add;
	
					// ---------------------------
	
					station_preserves.sps[ lilc->station_preserves_index ].station_report_data_rip.programmed_irrigation_seconds_irrigated_ul += 1;
	
					station_preserves.sps[ lilc->station_preserves_index ].station_report_data_rip.programmed_irrigation_gallons_irrigated_fl += amount_to_add;
	
					// ---------------------------
	
					lilc->bsr->rip.programmed_irrigation_seconds += 1;
					
					lilc->bsr->rip.programmed_irrigation_gallons_fl += amount_to_add;
					
					break ;
		
				case IN_LIST_FOR_MANUAL_PROGRAM:
					station_preserves.sps[ lilc->station_preserves_index ].station_report_data_rip.manual_program_seconds_us += 1;
	
					station_preserves.sps[ lilc->station_preserves_index ].station_report_data_rip.manual_program_gallons_fl += amount_to_add;
	
					// ---------------------------
	
					lilc->bsr->rip.manual_program_seconds += 1;
					
					lilc->bsr->rip.manual_program_gallons_fl += amount_to_add;
					
					break ;
		
				case IN_LIST_FOR_MANUAL:
					station_preserves.sps[ lilc->station_preserves_index ].station_report_data_rip.manual_seconds_us += 1;
	
					station_preserves.sps[ lilc->station_preserves_index ].station_report_data_rip.manual_gallons_fl += amount_to_add;
	
					// ---------------------------
	
					lilc->bsr->rip.manual_seconds += 1;
					
					lilc->bsr->rip.manual_gallons_fl += amount_to_add;
					
					break ;
		
				case IN_LIST_FOR_WALK_THRU:
					station_preserves.sps[ lilc->station_preserves_index ].station_report_data_rip.walk_thru_seconds_us += 1;
	
					station_preserves.sps[ lilc->station_preserves_index ].station_report_data_rip.walk_thru_gallons_fl += amount_to_add;
	
					// ---------------------------
	
					lilc->bsr->rip.walk_thru_seconds += 1;
					
					lilc->bsr->rip.walk_thru_gallons_fl += amount_to_add;
					
					break ;

				case IN_LIST_FOR_TEST:
					station_preserves.sps[ lilc->station_preserves_index ].station_report_data_rip.test_seconds_us += 1;
	
					station_preserves.sps[ lilc->station_preserves_index ].station_report_data_rip.test_gallons_fl += amount_to_add;
	
					// ---------------------------
	
					lilc->bsr->rip.test_seconds += 1;
					
					lilc->bsr->rip.test_gallons_fl += amount_to_add;
					
					break ;
		
				case IN_LIST_FOR_MOBILE:
					station_preserves.sps[ lilc->station_preserves_index ].station_report_data_rip.mobile_seconds_us += 1;
	
					station_preserves.sps[ lilc->station_preserves_index ].station_report_data_rip.mobile_gallons_fl += amount_to_add;
	
					// ---------------------------
	
					lilc->bsr->rip.rre_seconds += 1;
					
					lilc->bsr->rip.rre_gallons_fl += amount_to_add;
					
					break ;
					
			}

			lilc = nm_ListGetNext( &(foal_irri.list_of_foal_stations_ON), lilc );
		}
		
	}
	else
	{
		// 7/17/2012 rmd : So no stations are ON. Add any flow to the system non-controller
		// accumulator. Must do for each system in play.
		for( sss=0; sss<MAX_POSSIBLE_SYSTEMS; sss++ )
		{
			// 6/3/2014 rmd : The TIME that non-controller flow is happening is an approximation.
			// Because of the way flow works with the accumulated gallons being 0 part of the time
			// between tokens and such (think poc decoders), we make an attempt to see if we should be
			// accumulating time by looking at the most recent 5 second avg flow. This probably works
			// pretty well. But do recognize the two (accumulated pulses and latest_5_second_avg) are
			// disjointed and do not necessarily track each other perfectly. The accumulated gallons is
			// however as accurate as we can be.
			if( system_preserves.system[ sss ].system_master_most_recent_5_second_average > 0 )
			{
				system_preserves.system[ sss ].rip.non_controller_seconds += 1;
			}

			system_preserves.system[ sss ].rip.non_controller_gallons_fl += system_preserves.system[ sss ].accumulated_gallons_for_accumulators_foal;
		}
	}
	
	// ----------
	
	// 6/3/2014 rmd : So we are actually done using the SYSTEM accumulated gallons number. We
	// MUST zero it out here so it does not add again to the accumulators the next second.
	// Remember the accumulated gallons is not a second by second number.
	for( sss=0; sss<MAX_POSSIBLE_SYSTEMS; sss++ )
	{
		system_preserves.system[ sss ].accumulated_gallons_for_accumulators_foal = 0.0;
	}

	// ----------
	
	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );

	xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );

	// --------------------------

	// 7/17/2012 rmd : Now for the POC level accumulators. Take the poc_preserves MUTEX as we
	// are going to add to the poc rip accumulators.
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	for( ppp=0; ppp<MAX_POCS_IN_NETWORK; ppp++ )
	{
		// If the record is in use.
		if( poc_preserves.poc[ ppp ].poc_gid != 0 )
		{
			amount_to_add = 0.0;
			
			// 7/17/2012 rmd : For each potential flow meter in the POC.
			for( fff=0; fff<POC_BYPASS_LEVELS_MAX; fff++ )
			{
				// 7/17/2012 rmd : Flow meter or not, bypass manifold or not, each of the THREE flow
				// averages are valid. If it is not a bypass or otherwise the unused level is guaranteed to
				// be 0.
				amount_to_add += poc_preserves.poc[ ppp ].ws[ fff ].accumulated_gallons_for_accumulators_foal;
			}
			
			// ----------

			// 4/11/2016 skc : Assign convenience variables
			precord = &poc_preserves.poc[ ppp ];

			if( amount_to_add > 0.0 )
			{
				// 01/14/2016 skc : All usage is counted against budgets.
				precord->budget.used_total_gallons += amount_to_add;

				// ----------
				
				// 7/17/2012 rmd : Then there is flow.
				precord->rip.seconds_of_flow_total += 1;

				precord->rip.gallons_total += amount_to_add;
				
				if( (precord->this_pocs_system_preserves_ptr)->system_master_number_of_valves_ON > 0 )
				{
					// 7/17/2012 rmd : Then there is irrigation taking place.
					precord->rip.seconds_of_flow_during_irrigation += 1;
	
					precord->rip.gallons_during_irrigation += amount_to_add;

					// 4/19/2016 skc : We track different types of usage for budgets.
					// Use the reason to fully specify the usage. Only one reason is
					// valid at a time.
					add_amount_to_budget_record( amount_to_add, precord );
				}
				else
				if( precord->this_pocs_system_preserves_ptr->sbf.MVOR_in_effect_opened || precord->this_pocs_system_preserves_ptr->sbf.MVOR_in_effect_closed )
				{
					// 7/17/2012 rmd : If MVOR is in effect direct the flow total to these accumulators.
					precord->rip.seconds_of_flow_during_mvor += 1;
	
					precord->rip.gallons_during_mvor += amount_to_add;

					// 4/11/2016 skc : We track different types of usage for budgets
					precord->budget.used_mvor_gallons += amount_to_add;
				}
				else
				{
					precord->rip.seconds_of_flow_during_idle += 1;
	
					precord->rip.gallons_during_idle += amount_to_add;

					// 4/11/2016 skc : We track different types of usage for budgets
					precord->budget.used_idle_gallons += amount_to_add;
				}
					
			}
			
			// ----------
			
			// 6/5/2014 rmd : Now that we are done with the accumulated gallons for each poc zero them
			// so the values aren't double counted.
			for( fff=0; fff<POC_BYPASS_LEVELS_MAX; fff++ )
			{
				poc_preserves.poc[ ppp ].ws[ fff ].accumulated_gallons_for_accumulators_foal = 0.0;
			}
			
		}  // of if active poc

	}  // of all preserves

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

	// --------------------------

	// 9/10/2015 mpd : Lights accumulators.
	xSemaphoreTakeRecursive( list_foal_lights_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( lights_preserves_recursive_MUTEX, portMAX_DELAY );

	if( foal_lights.header_for_foal_lights_ON_list.count > 0 )
	{
		lllc = nm_ListGetFirst( &(foal_lights.header_for_foal_lights_ON_list) );

		while( lllc != NULL )
		{
			if( lllc->light_is_energized )
			{
				lights_array_index = LIGHTS_get_lights_array_index( lllc->box_index_0, lllc->output_index_0 );

				if( lllc->reason_in_list == IN_LIST_FOR_PROGRAMMED_LIGHTS )
				{
					lights_preserves.lights[ lights_array_index ].lrr.programmed_seconds++;
				}
				else	// in list for MANUAL, MOBILE, and so on
				{
					lights_preserves.lights[ lights_array_index ].lrr.manual_seconds++;					
				}
			} 

			lllc = nm_ListGetNext( &(foal_lights.header_for_foal_lights_ON_list), lllc );

		}  
	}

	xSemaphoreGiveRecursive( lights_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_foal_lights_recursive_MUTEX );

	// --------------------------

}
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

