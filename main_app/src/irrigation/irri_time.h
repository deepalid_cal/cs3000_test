/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_IRRI_TIME_H
#define _INC_IRRI_TIME_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cal_td_utils.h"

#include	"lpc_types.h"

#include	"station_groups.h"

#include	"stations.h"

#include	"station_history_data.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// These two defines are used to decide when to log ET Table Substitution and ET
// Table Limit activity
#define IRRITIME_LOG_EVENTS			(true)
#define IRRITIME_DONT_LOG_EVENTS	(false)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern BOOL_32 nm_IRRITIME_this_program_is_scheduled_to_run( STATION_STRUCT *const pss_ptr, STATION_GROUP_STRUCT *const psgs_ptr );

extern float WATERSENSE_get_et_value_after_substituting_or_limiting(	BOOL_32 const pfor_ftimes,
																		STATION_STRUCT const *const pss_ptr,
																		DATE_TIME_COMPLETE_STRUCT const *const pdtcs_ptr,
																		UNS_32 const pet_table_index,
																		BOOL_32 const plog_events );

extern UNS_32 WATERSENSE_make_min_cycle_adjustment(	BOOL_32 const pfor_ftimes,
													UNS_32 *ptotal_seconds_ptr,
													const UNS_32 pcycle_seconds,
													STATION_HISTORY_RECORD *pshr_rip_ptr );

extern float nm_WATERSENSE_calculate_run_minutes_for_this_station(	BOOL_32 pfor_ftimes,
																	STATION_STRUCT *pss_ptr,
																	STATION_GROUP_STRUCT *const psgs_ptr,
																	FT_STATION_STRUCT* pft_station_ptr,
																	const DATE_TIME_COMPLETE_STRUCT *pdtcs_ptr,
																	const float pdistribution_uniformity_100u );
																	
extern float nm_WATERSENSE_calculate_adjusted_run_time(	BOOL_32	pfor_ftimes,
														STATION_STRUCT *pss_ptr,
														FT_STATION_STRUCT *pft_station_ptr,
														float prun_time_min,
														const UNS_32 pet_factor_100u,
														const UNS_32 ppercent_adjust_factor_100u );
														
extern void IRRITIME_make_ETTable_substitute_and_limit_alerts_after_we_know_program_ran(	STATION_GROUP_STRUCT const *const psgs_ptr,
																							STATION_STRUCT const *const pss_ptr,
																							DATE_TIME_COMPLETE_STRUCT const *const pdtcs_ptr );
																							
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif //_MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

