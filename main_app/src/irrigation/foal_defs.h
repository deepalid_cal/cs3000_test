/*  file = foal_defs.h              10/27/00 2011.08.01  rmd  */

/* ---------------------------------------------------------- */

#ifndef _INC_FOAL_DEFS_H
#define _INC_FOAL_DEFS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// keep common FOAL definitions all together


// MAX_CHAIN_LENGTH cannot be set differently for say 523 versus 573...it must be the same for
// all ROM's that are going to play together
#define MAX_CHAIN_LENGTH					(12)

// ------------

// 9/6/2016 rmd : This is a physical limitation. The hardware supports 48 conventional
// stations using plug in cards and terminals.
#define MAX_CONVENTIONAL_STATIONS			(48)

// 9/6/2016 rmd : We have decided (pretty much Byma, AJ, and I a few yrs ago) that the
// controller would support 128 2 wire stations. The original plan was to support EITHER 128
// two wire stations OR (48 conventional + 80 two-wire stations). Engineering got crunch
// into supporting the 128 two-wire stations (jobs installed already) and the easiest
// approach to providing 128 two-wire stations appeared to be to just add the 128 to the end
// of the 48 conventional. In other words the controller actually supports 48 + 128 or 176
// stations. Exceeding the cut-sheet requirement.
#define MAX_2_WIRE_STATIONS					(128)

#define MAX_STATIONS_PER_CONTROLLER			(MAX_CONVENTIONAL_STATIONS + MAX_2_WIRE_STATIONS)

// 2011.09.02 rmd : With a MAX_CHAIN_LENGTH of 12 and MAX_STATIONS_PER_CONTROLLER of 128 one
// would guess we could have (12*128)=1536 stations. But that is not the case. In order to
// limit resources required and overall complication I have just decided to set the limit to
// half that. Or 768 stations max. We shall see how that works out.
//
// 9/6/2016 rmd : Regardless of how many stations per box we support, the total number of
// stations per network remains fixed at 768 stations total.
#define MAX_STATIONS_PER_NETWORK			(768)

// ----------

// And set the maximum number of irrigation items to control.
#define FOAL_MAX_IRRIGATION_LIST_ITEMS		(MAX_STATIONS_PER_NETWORK)

// ------------

// 605.a ajv - Added for mainline break limit, system capacity limit,
// station flow rate limit, and POC 2 and POC 3 limits
#define	MAXIMUM_SYSTEM_CAPACITY				(4000)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef enum {  NORMAL_I, 
				UNDER_I, 
				OVER_I
} I_STATUS;

// ----------

// 10/2/2012 rmd : This is the UPPER BOUND on box electrical limit. The default is 6 but
// we'll let service employees move that to 8.
#define ELECTRICAL_LIMIT_MAXIMUM			(8)
#define ELECTRICAL_LIMIT_MINIMUM			(1)
#define ELECTRICAL_LIMIT_DEFAULT			(6)

// ----------

// Some defines that work in the "weight" byte of the station list record. The numbers
// correspond to the 4 bit location of the reason bits in the ilc bit field.

// !!! if we add one here we would need to add to: 
//      - the flow ACCUMULATORS switch statement !!!
//      - the STOP screen "list_has" array
//		- in r_alerts.c the reasons_in_list_text array

// AND MANY other places - too many to list - so change the name of one of the exisiting
// ones to find 'em all

// IF you change the ORDER of these you may effect the equality statement in the
// "TheStationToAddWeighsMOREThanTheListStation()" function. BE CAREFUL!! ALSO there would
// be equality statements in FOAL_MaintainIrriList function that may not work anymore (we
// count on sms1, sms2, programmed, and sms_ho being the lowest priority reasons - in that
// order.

// 2011.09.27 Expanding the reasons in the list from 3 bits to 4 bits. Since in the 2000e we
// were tapped out.
#define	IN_LIST_FOR_MAXIMUM_DIFFERENT_REASONS		(16)

// ----------

// 10/4/2012 rmd : From LEAST priority to HIGHEST. TEST is a higher priority than
// PROGRAMMED_IRRIGATION.
#define nlu_IN_LIST_FOR_not_in_use					(0)

#define IN_LIST_FOR_PROGRAMMED_IRRIGATION			(1)

// 7/29/2015 mpd : Most of the irrigation reasons have generic names that work for LIGHTS. 
// This one didn't. LIGHTS does not need weighted priority logic since there is no limit
// to the number of concurrent lights ON. The reason a light is on the list may be useful
// and has been implemented identically to irrigation code.   
#define IN_LIST_FOR_PROGRAMMED_LIGHTS				( IN_LIST_FOR_PROGRAMMED_IRRIGATION )

// 7/16/2015 rmd : In the 2000e there were truly two manual programs (and only 2) and a yes
// a station could be in both of them. But they were identifed as MP1 and MP2. In the CS3000
// the world is very different. Within one single manual program that a guy builds so
// station within it may be tied to that MP with the first MP GID slot in the
// station_structure. And some stations may be tied to that MP with the second GID slot in
// the station_strcut. If we tracked stations reason in the list by the GID slot within
// their station_strucutre we would end up with an unexpected irrigation order. The slot in
// the station_structure that a MP occupies depends upon the order that station was added to
// the MP. If the station already belonged to another MP it would occupy the second slot.
// And at the same time other stations in the same MP could occupy the first GID slot. So I
// decided the best course of action here would be to have only one reason in the list for
// MP's. And this was a good compromise to other issues that can arise if you stacked
// starts.
#define IN_LIST_FOR_MANUAL_PROGRAM					(2)

// 7/16/2015 rmd : Was manual program B in the 2000e.
#define IN_LIST_FOR_no_longer_used					(3)

#define IN_LIST_FOR_MANUAL							(4)

#define IN_LIST_FOR_WALK_THRU						(5)

#define IN_LIST_FOR_TEST							(6)

#define IN_LIST_FOR_MOBILE							(7)


// 9/12/2014 ajv : Used for indexing to display reason on strings.
#define IN_LIST_FOR_HIGHEST_WEIGHTED_REASON			(IN_LIST_FOR_MOBILE)

// ----------

// 10/10/2012 rmd : This is used to initialize the highest reason in the list in the
// system_preserves.
#define IN_LIST_FOR_LOWEST_WEIGHTED_REASON			(nlu_IN_LIST_FOR_not_in_use)

// ----------
// 4/11/2016 skc : These are also other reasons that flow is occuring. These do not appear
// in "reason for being in the running list". The code for budgets uses these to determine
// which types of flow to include when tabulating water used in a budget period.
#define FLOW_TYPE_IRRIGATION						(0)
#define FLOW_TYPE_MVOR								(8)
#define FLOW_TYPE_NON_CONTROLLER					(9)
#define FLOW_TYPE_DEFAULT							((1<<FLOW_TYPE_IRRIGATION)+(1<<IN_LIST_FOR_PROGRAMMED_IRRIGATION)+(1<<IN_LIST_FOR_MANUAL_PROGRAM)+(1<<IN_LIST_FOR_MANUAL)+(1<<IN_LIST_FOR_WALK_THRU)+(1<<IN_LIST_FOR_TEST)+(1<<IN_LIST_FOR_MOBILE)+(1<<FLOW_TYPE_MVOR)+(1<<FLOW_TYPE_NON_CONTROLLER))
#define FLOW_TYPE_NUM_TYPES							(10)


/* ---------------------------------------------------------- */

// 6/1/2012 rmd : The four flow check groups are defined by their ALERT_ACTION settings. And
// MUST be 0 through 3 as we use the stations flow check group value as an index into an
// array of the count of valves ON in each group. There is supposed to ONLY be valves ON for
// a single group at a time. With some exceptions. Like radio remote and test.

// 10/2/2015 rmd : WARNING - DO NOT MONKEY WITH THESE VALUES. THEY MUST REMAIN AT 0, 1, 2, 3
#define		FLOW_CHECK_GROUP_no_flow_checking				(0)

#define		FLOW_CHECK_GROUP_check_for_low_flow_only		(1)

#define		FLOW_CHECK_GROUP_check_for_high_flow_only		(2)

#define		FLOW_CHECK_GROUP_check_for_high_and_low_flow	(3)

/* ---------------------------------------------------------- */
typedef struct
{
	union
	{
		UNS_64	whole_thing;

		struct
		{
			UNS_32		no_longer_used_01									: 4;
			
			// ----------
			
			// Station Priority : this determines what priority level a specific program is set to.
			// these are used when determining whether a ilc that is to be added to the list and are
			// specified on the start times and water days screen. With 2 bits we get 4 priority levels
			// possible. NOTE: use caution when doing comparisons with this variable.  The HIGH priority
			// is a 1 and the LOW is a 3, so you have to use inverse logic when performing the
			// comparisons.
			UNS_32		station_priority									: 2;
		
			// The "w_" signifies terms used in the weighting evaluation. Maximum 16 different reasons.
			UNS_32		w_reason_in_list									: 4;

			// Set the Set Expected bit. When PROGRAMMED_IRRIGATION, or TEST, or RRE irrigation is
			// initiated this bit may be set.
			BITFIELD_BOOL		w_to_set_expected									: 1;

			BITFIELD_BOOL		w_uses_the_pump										: 1;

			BITFIELD_BOOL		w_did_not_irrigate_last_time						: 1;

			// This is the bit that gets the station to turn ON first cause its in the problem list and
			// we always choose one from the list...after we check flow we reset this bit which
			// potentially could allow another from the problem list to turn ON or the stations ON could
			// finish out their cycle and then another from the problem list would come ON.
			BITFIELD_BOOL		w_involved_in_a_flow_problem						: 1;
			
			// ----------------------------

			// 6/11/2012 rmd : These take on the alert_action setting and direct the behaviour when a
			// flow checking error occurs.
			UNS_32		flow_check_hi_action								: 2;

			UNS_32		flow_check_lo_action								: 2;
			
			// The following are the bits that control the ALERT_ACTION versus flow testing behavior ...
			// they define groups of valves that are allowed to come ON together. The rules should apply
			// to all irrigation reasons that test flow.
			//
			//					HI						\					LO					\	GROUP
			//		------------------------------------\---------------------------------------\----------
			//		Alert/Shutoff OR Alert/No Action	\	Alert/Shutoff OR Alert/No Action	\	  3 		
			//											\										|
			//		Alert/Shutoff OR Alert/No Action	\			No Alert/No Shutoff			\	  2
			//											\										|
			//				No Alert/No Shutoff			\	Alert/Shutoff OR Alert/No Action	\	  1
			//											\										|
			//				No Alert/No Shutoff			\			No Alert/No Shutoff			\	  0
			//
			//
			// This depicts the four groups. We use this value to prevent valves from different groups
			// irrigating at the same time. The value is used as a low weighted list ordering
			// mechanism. Though the actual order of irrigation of which flow group goes when is not
			// guaranteed. As other station conditions a weighted far heavier than the flow check group
			// the valve is in.
			UNS_32		flow_check_group									: 2;

			// ----------------------------

			// If this station is on a program that is allowed to respond to a wind shutdown indicate
			// that here by setting this bit high.
			BITFIELD_BOOL		responds_to_wind									: 1;

			// If this station is on a program that is allowed to respond to rain indicate that here by
			// setting this bit high.
			BITFIELD_BOOL		responds_to_rain									: 1;

			// Flag that the station is currently turned ON.
			BITFIELD_BOOL		station_is_ON										: 1;

			// ----------
			
			BITFIELD_BOOL		no_longer_used_02									: 1;
			
			// ----------
			
			// 6/1/2012 rmd : Set when the station is added to the list. Uses the reason in the list to
			// decide if this is true or false.
			BITFIELD_BOOL		flow_check_when_possible_based_on_reason_in_list	: 1;

			// We set this bit indicating the valve, originally involved in flow tests, failed a flow
			// test twice (to the true failure level), but was set to NOT shutoff - so now it receives a
			// low list priority and will belong to the group of valves that does no flow checking.
			BITFIELD_BOOL		flow_check_to_be_excluded_from_future_checking		: 1;
			
			// This bit is used to signify that an RRE command came in asking a station to pause - a
			// station remains paused until the keypad timer expires (at which time the station is
			// REMOVED from the irri list) or an RRe command is received requesting the station to
			// resume.
			BITFIELD_BOOL		rre_station_is_paused								: 1;
			
			// 6/5/2012 rmd : Two flags that track wether flow was checked at all during the course of
			// programmed irrigation. The at_some_point_should_check_flow flag is set when the ilc is
			// added to the irrigation list at the programmed irrigation start time. We figure that out
			// using a variety of system settings and states. The at_some_point_have_checked flag is set
			// in the lfow checking function when a flow check is made and either fails or passes.
			BITFIELD_BOOL		at_some_point_should_check_flow						: 1;

			BITFIELD_BOOL		at_some_point_flow_was_checked						: 1;


			// Indications for the RRe to indicate the station is on the sxr list to either PAUSE or
			// turn OFF - it is on the sxr list on the IRRI side awaiting its token at which time the
			// action will actually be taken and the item xferred tot he FOAL machine
			// - that is when we reset these bits - also using one to show that a station is in
			// the process of turning ON - will reset that one when the station actually turns ON.
			BITFIELD_BOOL		rre_on_sxr_to_pause									: 1;

			BITFIELD_BOOL		rre_on_sxr_to_turn_OFF								: 1;

			BITFIELD_BOOL		rre_in_process_to_turn_ON							: 1;

			// ----------
			
			BITFIELD_BOOL		no_longer_used_03									: 1;
			
			// ----------
			
			// 5/2/2012 rmd : Used to distribute the station ilc record when it is added to the ilc
			// list. Or upon program start. Or upon scan completion.
			BITFIELD_BOOL		xfer_to_irri_machines								: 1;

			// ----------

			// 10/12/2012 rmd : These are copies of the behaviour directions accompanying the ilc when
			// it was added to the list. They are used to curtail irrigation if any of these conditions
			// dynamically 'appear' while irrigaiton is underway.
			BITFIELD_BOOL		directions_honor_RAIN_TABLE							: 1;

			BITFIELD_BOOL		directions_honor_controller_set_to_OFF				: 1;

			BITFIELD_BOOL		directions_honor_MANUAL_NOW							: 1;

			BITFIELD_BOOL		directions_honor_CALENDAR_NOW						: 1;

			BITFIELD_BOOL		directions_honor_RAIN_SWITCH						: 1;

			BITFIELD_BOOL		directions_honor_FREEZE_SWITCH						: 1;

			BITFIELD_BOOL		directions_honor_WIND_PAUSE							: 1;

			// ----------

			// -----------------------------
			// 6/5/2012 rmd : So far using 41 bits out of the 64 allocated. Still lots of expansion
			// room.
			// -----------------------------

		};

	};

} BIG_BIT_FIELD_FOR_ILC_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/31/2018 rmd : The system preserves bit field is defined here to avoid complicated
// circular dependencies that were hard to break and get the code to compile.
typedef struct
{
    union
	{
		UNS_64	overall_size;

		struct
		{
			// NOTE: In GCC this is the Least Significant Bit (Little Endian)

			// ----------
			
			// 3/11/2016 rmd : Was previously occupoed by the number_of_flow_meters but now available
			// for any new need.
			UNS_32		unused_four_bits	: 4;

			// ----------
			
			// 5/3/2012 rmd : Used when deciding if a master valve somewhere in the system just opened.
			// I believe this is a FOAL side variable only.
			BITFIELD_BOOL		master_valve_has_at_least_one_normally_closed	: 1;

			// -------------------------------

			// 8/8/2012 rmd : The by system indication which is to be shared to all the IRRI machines
			// indicating for which SYSTEMS the master valves are to be opened. Affects only the POC's
			// attached to this system. Is the resultant variable down-stream of the anti-chatter
			// countdown. In other words this is has the anti-chatter logic already incorporated. To be
			// set on the FOAL side and then shared via TOKEN to all the irri machines. On the IRRI side
			// is used in conjunction with the MLB and MVOR status to determine what to do with the POC
			// MV output.
			BITFIELD_BOOL		mv_open_for_irrigation			: 1;

			// -------------------------------

			// 8/8/2012 rmd : The by system indication which is to be shared to all the IRRI machines
			// indicating for which SYSTEMS the pump output is to be energized. Affects only the POC's
			// attached to this system. Is the resultant variable down-stream of the anti-chatter
			// countdown. In other words this is has the anti-chatter logic already incorporated. For
			// FOAL side use only.
			BITFIELD_BOOL		pump_activate_for_irrigation	: 1;

			// -------------------------------

			// 5/31/2012 rmd : Not sure what we are going to do with all these flow check status flags.
			// But they are easier to set this way than a single flag that is supposed to come out
			// representing the 'most important' state. The logic in the flow check function is very
			// complicated and confusing. Having a separate bit for each makes it easier.
			//
			// These flags are all cleared and set as appropriate at a 1 hz rate within the function
			// that performs the flow checking test.
			//
			// This group of flags provides the logic for the irri machines to blink their "GPM" to
			// indicate we are waiting to check flow. Other indication can also be put on the screen
			// based on them.
			//
			// These flags are also useful in deciding when to throw the flow not checked flag. The old
			// way this flag was thrown was cycle by cycle but that has proven to be not only a nusiance
			// but uneccessary. A better way is to throw this flag only at the end of programmed
			// irrigation and only if we never tested flow - but only if we were waiting to test flow.
			// Hmmm. Maybe that requires another flag or two separate from this group.

			BITFIELD_BOOL		system_level_no_valves_ON_therefore_no_flow_checking			: 1;

			BITFIELD_BOOL		system_level_valves_are_ON_but_will_not_be_checking_flow		: 1;

			BITFIELD_BOOL		system_level_valves_are_ON_and_waiting_to_check_flow			: 1;

			BITFIELD_BOOL		system_level_valves_are_ON_and_actively_checking				: 1;

			BITFIELD_BOOL		system_level_valves_are_ON_and_waiting_to_update_derate_table 	: 1;

			BITFIELD_BOOL		system_level_valves_are_ON_and_has_updated_the_derate_table		: 1;

			BITFIELD_BOOL		system_level_valves_are_ON_and_waiting_to_acquire_expected		: 1;

			// ----------
			
			// 3/11/2016 rmd : 14 bits used so far.
			
			// ----------
			
			// Flag used to understand if we've checked flow or updated the derate table AND made flow
			// recording lines for the group of valves now ON.
			BITFIELD_BOOL		checked_or_updated_and_made_flow_recording_lines		: 1;

			// 2012.01.31 rmd : This flag is set when the SYSTEM flow averages are computed. Which is at
			// a 1Hz rate. And it is (true) if there are flow meters and the user has (the user setting)
			// flow checking enabled within the system.
			BITFIELD_BOOL		flow_checking_enabled_and_allowed 		: 1;

			// 5/31/2012 rmd : This is also set when the SYSTEM flow averages are computed. Which is at
			// a 1Hz rate. This variable is an aid for use during the flow checking function to help
			// with flag setting.
			BITFIELD_BOOL		flow_checking_enabled_by_user_setting	: 1;

			// ----------
			
			// 3/11/2016 rmd : If there is a mis-match between the number of POCs and the number of flow
			// meters this will be (true). It's a bit complicated to determine this as we analyze bypass
			// manifolds also to have a flow meter for each active level. When (true) the expecteds
			// drive the accumulators and I believe perhaps the flow on the status screen - but I'm not
			// sure about that. This flag is used when driving the accumulators in the report_data.c 
			// file. We demand that each POC have a flow meter to drive the accumulators because if
			// there is one missing flow meter we really can't use the flow data. I've decided when that
			// condition exists the flow we do see is not valid and should not be used.
			BITFIELD_BOOL		there_are_pocs_without_flow_meters		: 1;

			// ----------
			
			// This is the determination of stability. Only valid at the MASTER. (This does NOT need to
			// be battery backed).
			BITFIELD_BOOL		stable_flow								: 1;

			// ----------
			
			// 9/24/2015 ajv : These battery-backed variables are intended to control the actual MVOR.
			// In the event of a powerfail, these are analyzed and the MVOR continues if it hasn't
			// crossed the stop date/time yet. These are ONLY VALID AT THE MASTER. However, they are
			// sycned down the chain into the "delivered_" copies of these to ensure that each of the
			// IRRI machines energize their respective POCs.
			BITFIELD_BOOL		MVOR_in_effect_opened					: 1;

			BITFIELD_BOOL		MVOR_in_effect_closed					: 1;

			// ----------
			
			// 3/11/2016 rmd : 21 bits used so far.
			
			// ----------
			
			// 5/21/2012 rmd : These are the global by system copy of what WAS in the list and are set
			// (false) at any irrigation restart. In the irrigation maintenance function we develop a
			// local copy of these flags and then compare to these actual globals. As the means to see
			// when irrigation is over. Activities such as the STOP may need to clear these externally
			// to the maintenance function. As part of the STOP key processing. In order to avoid Alert
			// lines indicating the end of irrigation.
			BITFIELD_BOOL		one_or_more_in_list_for_programmed_irrigation			: 1;
			BITFIELD_BOOL		one_or_more_in_list_for_manual_program					: 1;
			BITFIELD_BOOL		no_longer_used_01										: 1;
			BITFIELD_BOOL		one_or_more_in_list_for_manual							: 1;
			BITFIELD_BOOL		one_or_more_in_list_for_walk_thru						: 1;
			BITFIELD_BOOL		one_or_more_in_list_for_test							: 1;
			BITFIELD_BOOL		one_or_more_in_list_for_rre								: 1;

			// ----------
			
			// 3/11/2016 rmd : 28 bits used so far.
			
			// ----------

			// 5/21/2012 rmd : These are the so called 'local' copy. Which is set (false) at the
			// beginning of each pass through the foal irri irrigation maintenance list.
			BITFIELD_BOOL		ufim_one_or_more_in_list_for_programmed_irrigation		: 1;
			BITFIELD_BOOL		ufim_one_or_more_in_list_for_manual_program				: 1;
			BITFIELD_BOOL		no_longer_used_02										: 1;
			BITFIELD_BOOL		ufim_one_or_more_in_list_for_manual						: 1;
			BITFIELD_BOOL		ufim_one_or_more_in_list_for_walk_thru					: 1;
			BITFIELD_BOOL		ufim_one_or_more_in_list_for_test						: 1;
			BITFIELD_BOOL		ufim_one_or_more_in_list_for_mobile						: 1;

			// ----------
			
			// 3/11/2016 rmd : 35 bits used so far.
			
			// ----------
			
			// 6/18/2012 rmd : This should be set (true) as part of the 'set' functions. For the
			// variable that we keep copies of here in the system record.
			BITFIELD_BOOL		due_to_edit_resync_to_the_system_list					: 1;

			// -----------------------------

			// Used when we are performing list maintenance. That is testing if each system gid has a
			// matching system_preserve record. And there are NO system_preserve records with gid's that
			// no longer exist.
			BITFIELD_BOOL		accounted_for											: 1;

			// -----------------------------

			// 9/24/2015 ajv : IRRI-side copies of MVOR_in_effect variables so slaves energize their
			// outputs. DO NOT need to be battery-backed (only the FOAL-side ones do), but this was a
			// convenient place and coincides with how we manage the MLB records.
			BITFIELD_BOOL		delivered_MVOR_in_effect_opened							: 1;

			BITFIELD_BOOL		delivered_MVOR_in_effect_closed							: 1;

			// ----------
			
			// 3/11/2016 rmd : 39 bits used so far.
			
			// ----------
			
			// 3/11/2016 rmd : Being that we can have 12 bypass manifolds we could have 36 flow meters
			// in a single system (mainline). This however is EXTREMELY unlikely. However we allow and
			// so I'll provide enough bits to count up to that (6 bits). This count is evaluated once
			// per second in the foal_flow.c file. So far I beleive this variable is more of an
			// interesting count (a curiousity) than having an actual use.
			UNS_32		number_of_flow_meters_in_this_sys	: 6;
			
			// 3/11/2016 rmd : This variable can only count up to 12 - the maximum number of POCs
			// allowed.
			UNS_32		number_of_pocs_in_this_system		: 4;
			
			// ----------
			
			// NOTE: This is the MS bit in GCC.
		};

	};

} SYSTEM_BIT_FIELD_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

