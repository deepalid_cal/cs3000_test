
/*  file = irri_lights.h              						  */

/* ---------------------------------------------------------- */

#ifndef _INC_IRRI_LIGHTS_H
#define _INC_IRRI_LIGHTS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"foal_comm.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{ 
    union
	{
		UNS_32	overall_size;

		struct
		{
			// NOTE: In GCC this is the Least Significant Bit (Little Endian)

			// -------------------------------

			// 6/19/2012 rmd : This index is used as an integrity test for the stations journey from the
			// foal side to the irri side. Pretty much just watching the chain configuration is known
			// and agreed upon. This will take on the value of the serial numbers comm char
			// - 'A'. Which should also be it's owners index into our comm_mngr_shared_info array.
			// Range is 0 to (MAX_CHAIN_LENGTH-1). Must add 'A' to this to show the comm letter.
			//
			// 4/18/2014 rmd : Replaced with box_index_0 inmain structure.
			UNS_32		no_longer_used							: 4;

			// -------------------------------

			// The "w_" signifies terms used in the weighting evaluation. Maximum 16 different reasons.
			UNS_32		w_reason_in_list						: 4;
			
			// -------------------------------

			// Flag that the station is currently turned ON. If it is not ON and there is remaining time
			// then it is soaking.
			BITFIELD_BOOL		light_is_energized				: 1;

			// -----------------------------

			// NOTE: This is the MS bit in GCC.
		};

	};

	// This structure is 4 bytes long on 8/19/2015.

} IRRI_LIGHTS_BIT_FIELD_STRUCT;

/* ---------------------------------------------------------- */

// The IRRI machines holds a list of energized lights in SDRAM. This list obviously is not battery backed and therefore 
// does not survive a program restart (due to power failure or otherwise). It is intended first off as a way to turn off 
// lights that seemingly are running past their time. The assumption is that something happened where they were never
// told by the master to turn off. 

typedef struct
{
	// This is a list of the lights that are on or going on. This is similar in function to a combination of the station 
	// "main" (aka "all") and "action needed" lists. Since there is no limit to the number of lights that can be on 
	// simultaneously, there is no need to queue lights and weigh them for priority. This single list can handle lights. 
	MIST_DLINK_TYPE			list_linkage_for_irri_lights_ON_list;

	// 8/7/2015 mpd : TBD: This data has arrived on the IRRI side via a token. I see no need to continue using small, 
	// non-native containers. IRRI lights is using 32 bit fields.

	// 8/13/2015 mpd : TBD: FOAL fields not needed on the IRRI side have been commented out. They will be removed if no
	// use for them is discoveded by the end of implementation.

	// The IRRI side does not maintain an action needed list. This field tells the "IRRI_LIGHTS_maintain_lights_list()" 
	// function what should be done with this light.
	UNS_32					action_needed;

	// 8/31/2015 mpd : This field had been eliminated to save space. It has been returned to service for the real time
	// screen. This is not a duplication of the action field. This value is static for the duration of the ON time.
	UNS_32					reason_on_list;

	BOOL_32					light_is_energized;

	// Stop time is an absolute date & time so know when to turn off the light in the case of a power failure. Stop
	// time is evaluated at the start time as the light is added to the ON list. A stop time is required. 
	UNS_32					stop_datetime_t;

	UNS_32					stop_datetime_d;

	// 8/19/2015 mpd : TBD: The box and output index use 48 * 8 bytes of SDRAM. They are lightly used and could be 
	// replaced by 8 bytes on the stack in exchange for some processing time. Given a pointer to an 
	// IRRI_LIGHTS_LIST_COMPONENT struct, the 0 - 47 array index could be returned by a looping search function. That 
	// value would be used to calculate the box and index.

	UNS_32					box_index_0;

	UNS_32					output_index_0;  

	// This structure is 40 bytes long on 8/31/2015.

} IRRI_LIGHTS_LIST_COMPONENT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
typedef struct
{
	// This is a linked list of lights the FOAL side has delivered via tokens. Any light on this list should be 
	// energized or about to be energized by a subsequent "action needed" (likely in the same token). This should be an
	// abbreviated copy of the FOAL lights ON list. On the IRRI side, we only need the fields required to operate the 
	// lights and display system-wide lights info to the user.
	// 
	// This list is not in battery backed memory. If the chain goes down, the FOAL side must send us a new list. We will
	// maintain the last list for up to 120 seconds after the chain goes down. Any lights that are energized during this
	// brief outage will remain energized if the chain is recovered and the FOAL side includes that light on the updated
	// ON list. Any light scheduled to turn off during that brief outage will be turned off via an action needed record
	// in a subsequent token.
	// 
	// Another thing this list will do is to ensure that even if the chain is up that lights do not become turned ON and 
	// then stay ON for ever. This list will be 'maintained' and monitor stop date/time. If a light remains energized 
	// for 120 seconds beyond the stop date/time stored in the ON list entry, it will be shut OFF.
	MIST_LIST_HDR_TYPE		header_for_irri_lights_ON_list;

	// We define the illc's in the IRRI lights list as an array instead of the dynamic memory approach because we can.
	// There are only 48 possible lights in the system and the struct is small. This box only controls 4 of the lights, 
	// but we need full system knowledge for user display screens.
	IRRI_LIGHTS_LIST_COMPONENT	illcs[ MAX_LIGHTS_IN_NETWORK ];

	// This structure is 1748 bytes long on 8/19/2015.

} IRRI_LIGHTS;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// irri_lights is NOT battery backed. The approach in the CS3000 is if the chain goes down for longer than a brief 
// (2 minute) buffer period, we will NOT try to continue (pick up lighting where we left off) for each box in the chain. 
// Boxes will not operate as independents. If there is a power fail, when the chain comes back up the master will tell 
// the irri machines all that is happening with lights. 
extern IRRI_LIGHTS	irri_lights;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// If the master does not respond within a reasonable time (chain down), the IRRI machine watchdog will turn OFF the 
// lights. We do not wait for the stop time to turn off lights if we are part of a chain and it is down. 
#define IRRI_LIGHTS_SECONDS_TO_WAIT_FOR_MASTER				( 120 )

// Station defines exist in "irri_irri.h" that allow the slaves to continue irrigation after the master is manually
// taken off line. There are also defines for orphaned stations. Neither seem to be implemented, but they exist. Similar
// defines are NOT being created for lights. If the master goes away, you have 2 minutes of lighting remaining. This is
// done because of the CS3000 master/slave smart token architecture. With no master communication, there is virtually
// no light (or station) control at the slaves, via mobile, or via the web. Shutting down is the safest action.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_irri_lights( void );

extern void IRRI_LIGHTS_restart_all_lights( void );

extern BOOL_32 IRRI_LIGHTS_light_is_energized( const UNS_32 light_index_0_47 );

extern UNS_32 IRRI_LIGHTS_get_number_of_energized_lights( void );

extern UNS_32 IRRI_LIGHTS_populate_pointers_of_energized_lights( void );

extern IRRI_LIGHTS_LIST_COMPONENT *IRRI_LIGHTS_get_ptr_to_energized_light( const UNS_32 pline_index );

extern UNS_32 IRRI_LIGHTS_get_box_index( const UNS_32 pline_index );

extern UNS_32 IRRI_LIGHTS_get_output_index( const UNS_32 pline_index );

extern UNS_32 IRRI_LIGHTS_get_reason_on_list( const UNS_32 pline_index );

extern UNS_32 IRRI_LIGHTS_get_stop_date( const UNS_32 pline_index );

extern UNS_32 IRRI_LIGHTS_get_stop_time( const UNS_32 pline_index );

extern DATE_TIME IRRI_LIGHTS_get_light_stop_date_and_time( const UNS_32 light_index_0_47 );

extern void IRRI_LIGHTS_maintain_lights_list( const DATE_TIME *const dt_ptr );

extern void IRRI_LIGHTS_process_rcvd_lights_on_list_record( LIGHTS_LIST_XFER_RECORD const *const plxr );

extern void IRRI_LIGHTS_process_rcvd_lights_action_needed_record( LIGHTS_ACTION_XFER_RECORD const *const plxr );

extern BOOL_32 IRRI_LIGHTS_request_light_on( const UNS_32 light_index_0_47, const UNS_32 stop_datetime_d, const UNS_32 stop_datetime_t );

extern BOOL_32 IRRI_LIGHTS_request_light_off( const UNS_32 light_index_0_47 );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

