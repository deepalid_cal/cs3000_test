/*  file = irri_flow.c                         2011.07.20 rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strncmp
#include	<string.h>

#include	"irri_flow.h"

#include	"irri_irri.h"

#include	"irri_comm.h"

#include	"foal_comm.h"

#include	"foal_flow.h"

#include	"battery_backed_vars.h"

#include	"configuration_controller.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"station_groups.h"

#include	"comm_mngr.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*
void StreamDetailedFlowInformationToFrontPort( void ) {

char str_64[ 64 ], time_str[ 20 ];
float actualFlowRate;
DATA_HANDLE dh;

	if ( DisplayedHeaderForStreamedFlowInformation == FALSE ) {

		str_64[ 0 ] = 0x00;
		sp_strcat( str_64, "Time, Hz, Actual GPM, Computed GPM, 5 Sec Avg, 20 Sec Avg%c%c", 0x0A, 0x0D );

		dh.dlen = strlen( str_64 );
		dh.dptr = (unsigned char*)str_64;

		C_AddCopyOfBlockToXmitList( dh );

		DisplayedHeaderForStreamedFlowInformation = TRUE;

	}



	// ONLY streams FM_0
	
	if ( FlowReadings1[ 0 ] != 0 ) {

		if ( poc[ 0 ].fm_enter_k_and_offset_bool == TRUE ) {

			actualFlowRate = ( FM_User_K_ul_100000u[ 0 ] / 100000.0 ) * ( FlowReadings1[ 0 ] + ( FM_User_O_sl_100000u[ 0 ] / 100000.0 ) );

		} else {

			actualFlowRate = FlowMeter_Ks[ FlowMeters[ 0 ] ] * ( FlowReadings1[ 0 ] + FlowMeter_Os[ FlowMeters[ 0 ] ] );

		}

	} else { 

		actualFlowRate = 0;

	}

	str_64[ 0 ] = 0x00;
//	sp_strcat( str_64, "%d", Gl_DateTime.T );
	sp_strcat( str_64, "%s", TDUTILS_time_to_time_string_without_ampm( Gl_DateTime.T, time_str, FALSE, TRUE, FALSE ), 0x08, 0x08 );
	sp_strcat( str_64, ", " );
	sp_strcat( str_64, "%d", FlowReadings1[ 0 ] );
	sp_strcat( str_64, ", " );
	sp_strcat( str_64, "%5.1f", actualFlowRate );
	sp_strcat( str_64, ", " );
	sp_strcat( str_64, "%5.1f", IrriFlow.system_info.flowrate_actual_fl );
	sp_strcat( str_64, ", " );
	sp_strcat( str_64, "%d", IrriFlow.system_info.flowrate_avg_display_us );
	sp_strcat( str_64, ", " );
	sp_strcat( str_64, "%d", IrriFlow.system_info.flowrate_avg_flowchecking_us );
	sp_strcat( str_64, "%c%c", 0x0A, 0x0D );


	dh.dlen = strlen( str_64 );
	dh.dptr = (unsigned char*)str_64;

	C_AddCopyOfBlockToXmitList( dh );

}
*/

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Information for each system is placed within the token. So that each irri
    machine gets a copy. pucp points to the first byte. The content is defined by the foal
    machine and we must follow of course. Return the number of bytes extracted. If the first
    byte is a 0 that would be an error. Why would the number of systems be 0? The caller of
    this function should add the return value to his pointer within the message.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the COMM_MNGR task when we are pulling apart
    the rcvd token.
	
    @RETURN (true) if no extraction range tests failed, if so (false)

	@AUTHOR 2012.02.01 rmd
*/
extern BOOL_32 IRRI_FLOW_extract_system_info_out_of_msg_from_main( UNS_8 **pucp_ptr, UNS_32 pfrom_serial_number )
{
/*
	Depiction of structure we are extracting.

	// 6/26/2015 mpd : FogBugz #3113. Added "number_of_valves_ON" to the structure.
    
	-------------------------
	| # of Systems's (uns_8)
	|
	| |----------------------
    | |
	| |System GID (uns_16)
    | |
	| |system_token_bit_field_struct (sizeof(SYSTEM_BIT_FIELD_STRUCT)
	| |
	| |number_of_valves_ON (uns_16)
	| |
	| |system_expected_flow_rate_for_those_ON (uns_16)
	| |
	| |system_master_most_recent_5_second_average (float)
    | |
	| |----------------------
    |
	-------------------------
*/
	UNS_32	sss;

	BOOL_32	rv;
	
	rv = (true);
	
	UNS_32	number_of_systems;

	
	// The first byte should be the number of system. And we will plunk it into an UNS_32
	// variable. Because that is the native compiler size. The code as is putting the single
	// byte into the larger 4 byte variable will not work in a BIG_ENDIAN SYSTEM. The ARM
	// platform we are on now is LITTLE_ENDIAN. Make sure to first clear the 32-bit variable.
	number_of_systems = 0;

	memcpy( &number_of_systems, *pucp_ptr, 1 );
	
	*pucp_ptr += 1;
	
	if( (number_of_systems == 0) || (number_of_systems > MAX_POSSIBLE_SYSTEMS) )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "IRRI: system data error in token" );

		// And results in a return value of (false) as something is wrong within the
		// payload.
		rv = (false);
	}
	else
	{
		for( sss=0; sss<number_of_systems; sss++ )
		{
			UNS_16	lgid;
			memcpy( &lgid, *pucp_ptr, sizeof(UNS_16) );
			*pucp_ptr += sizeof(UNS_16);

			SYSTEM_BIT_FIELD_STRUCT	lsbf;
			memcpy( &lsbf, *pucp_ptr, sizeof(SYSTEM_BIT_FIELD_STRUCT) );
			*pucp_ptr += sizeof(SYSTEM_BIT_FIELD_STRUCT);

			// 6/26/2015 mpd : FogBugz #3113. Added "number_of_valves_ON" to the structure.
			UNS_16	l_number_of_valves_ON;
			memcpy( &l_number_of_valves_ON, *pucp_ptr, sizeof(UNS_16) );
			*pucp_ptr += sizeof(UNS_16);

			UNS_16	l_flow_for_those_ON;
			memcpy( &l_flow_for_those_ON, *pucp_ptr, sizeof(UNS_16) );
			*pucp_ptr += sizeof(UNS_16);

			float	lavg_flow;
			memcpy( &lavg_flow, *pucp_ptr, sizeof(float) );
			*pucp_ptr += sizeof(float);

			BOOL_32	lmvor_in_effect_opened;
			memcpy( &lmvor_in_effect_opened, *pucp_ptr, sizeof(BOOL_32) );
			*pucp_ptr += sizeof(BOOL_32);

			BOOL_32	lmvor_in_effect_closed;
			memcpy( &lmvor_in_effect_closed, *pucp_ptr, sizeof(BOOL_32) );
			*pucp_ptr += sizeof(BOOL_32);

			UNS_32	lmvor_remaining_seconds;
			memcpy( &lmvor_remaining_seconds, *pucp_ptr, sizeof(UNS_32) );
			*pucp_ptr += sizeof(UNS_32);

			BY_SYSTEM_RECORD	*lbsr;
			
			// 2012.02.01 rmd : Do we really need to take this MUTEX. Well I think we had better. There
			// may be opportunity especially in a MASTER controller to be manipulating the system
			// preserves structure from several tasks. But that actually shouldn't be a problem. Why?
			// Cause the tasks are at different priorities and the variables we are setting here are 4
			// byte atomic operations. I think. But to be safe take it.
			xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

			lbsr = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( lgid );
			
			if( lbsr != NULL )
			{
				// 2012.02.01 rmd : Found the appropriate record. Dump in the values. But hey ... wait a
				// minute. For the case of a master why take a copy of the bit field from the system
				// preserve, send it through the communications, and then overwrite the VERY SAME location
				// in memory with it. This actually may be dangerous if TD_CHECK task executed between the
				// generation of the token and parsing of it (and TD_CHECK could very well being at a higher
				// priority). As values set in TD_CHECK will be reverted! So when the message came from
				// ourselves do not overwrite the variables that do not have an IRRI side equivalent!
				if( pfrom_serial_number != config_c.serial_number )
				{
					lbsr->sbf = lsbf;

					lbsr->ufim_expected_flow_rate_for_those_ON = l_flow_for_those_ON;
				}

				// 6/26/2015 mpd : FogBugz #3113. Added "number_of_valves_ON" to the structure. Add the extracted
				// value to the IRRI side variable.
				lbsr->system_rcvd_most_recent_number_of_valves_ON = l_number_of_valves_ON;

				// And this one is its OWN irri side variable so is safe to write even if the message is
				// from ourselves.
				lbsr->system_rcvd_most_recent_token_5_second_average = lavg_flow;

				// 9/24/2015 ajv : Extract the MVOR in effect flags to ensure slaves energize their
				// respective master valves when necessary.
				lbsr->sbf.delivered_MVOR_in_effect_opened = lmvor_in_effect_opened;

				lbsr->sbf.delivered_MVOR_in_effect_closed = lmvor_in_effect_closed;

				lbsr->delivered_MVOR_remaining_seconds = lmvor_remaining_seconds;
				
			}
			else
			{
				// 5/5/2014 ajv : Re-enable this warning once the syncing work has been completed.
				// Currently, with the changes made to syncing of Program Data, the preserves at the slave
				// is out-of-sync.
				//
				// 7/10/2014 rmd : I re-enabled.
				ALERT_MESSAGE_WITH_FILE_NAME( "IRRI_FLOW: system not found" );
				
				// 7/19/2012 rmd : And again either something is wrong with the payload or the system is
				// just plain out of sync.
				rv = (false);
			}
			
			xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

		}  // for each system

	}  // if we have systems ... which here we always should
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 IRRI_FLOW_extract_the_poc_info_from_the_token( UNS_8 **pucp_ptr )
{
	BOOL_32		rv;

	UNS_8		number_of_pocs;
	
	UNS_32		i, lll;

	POC_DISTRIBUTION_FROM_MASTER	pdfm;

	BY_POC_RECORD	*bpr;

	// ----------
	
	rv = (true);  // signal caller extraction went well
	
	// ----------
	
	// The first byte should be the number of pocs.
	number_of_pocs = *(*pucp_ptr);

	*pucp_ptr += sizeof(UNS_8);
	
	// ----------

	if( (number_of_pocs == 0) || (number_of_pocs > MAX_POCS_IN_NETWORK) )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "IRRI: poc data error in token" );
		
		// 4/12/2012 rmd : And signal something wrong in the data.
		rv = (false);
	}
	else
	{

		for( i=0; i<number_of_pocs; i++ )
		{
			memcpy( &pdfm, *pucp_ptr, sizeof(POC_DISTRIBUTION_FROM_MASTER) );

			*pucp_ptr += sizeof(POC_DISTRIBUTION_FROM_MASTER);
			
			// ----------

			// 2012.02.01 rmd : Take MUTEX so that while we obtain an write to a poc_preserves element
			// the entire array remains stable.
			xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

			bpr = POC_PRESERVES_get_poc_preserve_ptr_for_these_details( pdfm.box_index, pdfm.poc_type__file_type, pdfm.decoder_serial_number );
			
			if( bpr != NULL )
			{
				// 7/18/2012 rmd : We used the gid to find a match in the preserves, now we'll perform a
				// simple sanity check.
				if( pdfm.box_index != bpr->box_index )
				{
					ALERT_MESSAGE_WITH_FILE_NAME( "Irri_Flow: poc_preserves mis-match." );

					rv = (false);
				}
				else
				{
					for( lll=0; lll<POC_BYPASS_LEVELS_MAX; lll++ )
					{
						bpr->ws[ lll ].delivered_5_second_average_gpm_irri = pdfm.latest_5_second_average_gpm_foal[ lll ];
	
						bpr->ws[ lll ].mv_current_as_delivered_from_the_master_ma = pdfm.mv_current_for_distribution_ma[ lll ];
						
						bpr->ws[ lll ].pump_current_as_delivered_from_the_master_ma = pdfm.pump_current_for_distribution_ma[ lll ];
					}
				}
			}
			else
			{
				// 4/12/2012 rmd : This is an error. And we should break from any further parsing of this
				// message!
				ALERT_MESSAGE_WITH_FILE_NAME( "IRRI_FLOW: poc not found" );
		
				// 8/17/2012 rmd : Setting the return value to (false) will cause us break when done with
				// this poc. Which we are now.
				rv = (false);
			}
			
			xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

			if( rv == (false) )
			{
				// 4/12/2012 rmd : Break from the for loop in event of an error. Done! Git.
				break;
			}

		}  // for each delivered poc

	}  // if we have pocs ... which here we always should
	
	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

