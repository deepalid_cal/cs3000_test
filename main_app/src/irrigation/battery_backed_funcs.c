/*  file = battery_backed_vars.c             2011.08.01  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"battery_backed_vars.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"epson_rx_8025sa.h"

#include	"range_check.h"

#include	"station_groups.h"

#include	"cs_mem.h"

#include	"configuration_network.h"

#include	"flowsense.h"

#include	"irri_comm.h"

#include	"flash_storage.h"

#include	"controller_initiated.h"

#include	"lpc3250_chip.h"

#include	"lpc32xx_gpio.h"

#include	"bithacks.h"

#include	"e_comm_test.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/5/2015 rmd : This file holds MOST of the functions associated with the battery backed
// data structures. I say most because a few like the foal_irri battery backed structure are
// better initialized and surrounded by its own related code. The functions held within this
// file have nothing to do with nor do they affect the precious and to be retained order of
// the battery backed variables in the battery backed SRAM memory. This file DOES NOT and
// should not contain any battery backed variable definitions. All battery backed variables
// are to be declared in the battery_backed_vars.c file. And that is where the variable
// ordering is defined.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/6/2015 rmd : Do not exceed (BB_PRE_STRING_LENGTH - 1) visible characters. If you do you
// can end up changing the version number with no recognition of the change. Follow the code
// through to understand this.
static const char	WEATHER_PRESERVES_VERIFY_STRING_PRE[] = "WEATHER v01";

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called during program startup.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the STARTUP task.

	@RETURN (none)

	@ORIGINAL 2012.07.23 rmd

	@REVISIONS (none)
*/
extern void init_battery_backed_weather_preserves( const BOOL_32 pforce_the_initialization )
{
	xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	// 8/7/2015 rmd : If the string isn't intact signal we've failed.
	if( pforce_the_initialization || (strncmp( weather_preserves.verify_string_pre, WEATHER_PRESERVES_VERIFY_STRING_PRE, sizeof(WEATHER_PRESERVES_VERIFY_STRING_PRE) ) != 0) )
	{
		// ----------------------

		Alert_battery_backed_var_initialized( "Weather Preserves", pforce_the_initialization );

		// ----------------------

		// 9/25/2012 rmd : The following memset does indeed (of course) set all the UNS_32 numbers
		// to 0, and all BOOL_32 variables to (false).
		memset( &weather_preserves, 0x0, sizeof(weather_preserves) );
		
		// ----------
		
		weather_preserves.dls_saved_date = DATE_MINIMUM;

		weather_preserves.dls_eligible_to_fall_back = (false); 

		weather_preserves.dls_after_fall_back_ignore_sxs = (false);

		weather_preserves.dls_after_fall_when_to_clear_ignore_sxs.D = DATE_MINIMUM; 

		weather_preserves.dls_after_fall_when_to_clear_ignore_sxs.T = TIME_DEFAULT;

		// ----------
		
		// 7/26/2012 rmd : If the et table file is initialized that process will set this variable
		// true. Also changing county/city will trigger a rewrite of all values. We do not have to
		// set (true) here. Should be set (false).
		weather_preserves.et_table_update_all_historical_values = (false);

		// 7/23/2012 rmd : Don't use the gage today as we just set the pulses so far today to ZERO.
		// And we don't want to use a partial day of pulses.
		weather_preserves.dont_use_et_gage_today = (true);

		// 7/23/2012 rmd : And fill the capacity.
		weather_preserves.remaining_gage_pulses = ET_GAGE_PULSE_CAPACITY;

		// 9/20/2012 rmd : The rip status should default to as if the number came from a gage. At
		// the roll time we check if there is a gage in the system and therefore will not just
		// blindly fill the table with zeros. If there is a gage we should be using this number. So
		// a default from the et gage is appropiate.
		weather_preserves.et_rip.status = ET_STATUS_FROM_ET_GAGE;

		weather_preserves.rain.rip.status = RAIN_STATUS_FROM_RAIN_BUCKET_M;

		// ----------
		
		// 5/8/2015 rmd : Case 1 - a first time startup. By the time this function runs all the FILE
		// INTIIALIZATIONS have run. And they HAVE set this variable (true) as they created their
		// default records. Therefore we are entering this function with this set (true). So make
		// sure it stays that way![Interesting note - the file initialization functions also tried
		// to start the pdata timer but could not as it was not yet created. It will however be
		// started when the CI task is created and sees this variable set (true)].
		//
		// 5/8/2015 rmd : Case 2 - battery backed fails for some reason. In that case if this
		// variable isn't already (true) we pay a communciation penalty for setting it (true). But
		// that's okay. This will rarely happen (dead battery or intentional battery backed
		// structure change).
		weather_preserves.pending_changes_to_send_to_comm_server = (true);
		
		// ----------
		
		strlcpy( weather_preserves.verify_string_pre, WEATHER_PRESERVES_VERIFY_STRING_PRE, BB_PRE_STRING_LENGTH );
	}
	else
	{
		#if ALERT_SHOW_ROUTINE_INITIALIZATIONS_DURING_STARTUP
			Alert_battery_backed_var_valid( "Weather Preserves" );
		#endif

	}

	// ----------

	/* ---------------------------------------------------------- */
	// AND THE STUFF WE ALWAYS INITIALIZE
	/* ---------------------------------------------------------- */
	
	// 10/28/2014 rmd : And keep the unused expansion portion of the structure set to zeros. So
	// that in the future as one goes to use it [meaning they made some variables and reduced
	// the 'expansion' size accordingly] the new variables will start life as zero.
	weather_preserves.ununsed_uns8_1 = 0x00;

	memset( &weather_preserves.expansion, 0, sizeof(weather_preserves.expansion) );
	
	// ----------
	
	xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/16/2013 ajv : Rolled to version 4 when new pi_last_measured_current_ma
// variable was added to STATION_HISTORY_STRUCT
//
// 8/6/2015 rmd : Do not exceed (BB_PRE_STRING_LENGTH - 1) visible characters. If you do you
// can end up changing the version number with no recognition of the change. Follow the code
// through to understand this.
//
// 9/6/2016 rmd : V05 - string changed to force station preserves to re-initialize when
// making change to support 128 two-wire stations. Needed to reinit structure because of the
// stucture size increase. We added 48 records per box during this change.
static const char STATION_PRESERVES_VERIFY_STRING_PRE[] = "STATION v05";

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called on startup prior to most but not all tasks running. A requirement to
    call on each program start to determine validity of the array.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the STARTUP task.

	@RETURN (none)

	@AUTHOR 2011.11.08 rmd
*/
extern void init_battery_backed_station_preserves( const BOOL_32 pforce_the_initialization )
{
	// 11/16/2012 rmd : We use a local variable to see if our string failed.
	BOOL_32		lmy_strings_failed;
	
	BOOL_32		found_rips;

	UNS_32		iii;
	
	// ----------
	
	xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	lmy_strings_failed = (false);

	// 8/7/2015 rmd : If the string isn't intact signal we've failed.
	if( strncmp( station_preserves.verify_string_pre, STATION_PRESERVES_VERIFY_STRING_PRE, sizeof(STATION_PRESERVES_VERIFY_STRING_PRE) ) != 0 )
	{
		lmy_strings_failed = (true);
	}
	
	// ----------

	if( pforce_the_initialization || lmy_strings_failed )
	{
		// ----------------------

		Alert_battery_backed_var_initialized( "Station Preserves", pforce_the_initialization );

		// 8/8/2012 rmd : Zero out the whole dang thing.
		memset( &station_preserves, 0x00, sizeof(STATION_PRESERVES_STRUCT) );

		// 7/19/2012 rmd : After the intialization the station report record has a bogus time and
		// date in it (zeros). Since the record here in the preserves really does exist and
		// potentially will have it's accumulators incremented let's put in the present time and
		// date.
		DATE_TIME	ldt;

		EPSON_obtain_latest_time_and_date( &ldt );

		INT_32	i;

		for( i=0; i<STATION_PRESERVE_ARRAY_SIZE; i++ )
		{
			// ----------------------------

			station_preserves.sps[ i ].skip_irrigation_till_due_to_manual_NOW_time = TIME_DEFAULT;

			station_preserves.sps[ i ].skip_irrigation_till_due_to_calendar_NOW_time = TIME_DEFAULT;

			// ----------------------------

			station_preserves.sps[ i ].skip_irrigation_till_due_to_manual_NOW_date = DATE_MINIMUM;

			station_preserves.sps[ i ].skip_irrigation_till_due_to_calendar_NOW_date = DATE_MINIMUM;

			// ----------------------------

			station_preserves.sps[ i ].spbf.flow_status = STATION_PRESERVES_FLOW_STATUS_normal_flow;

			station_preserves.sps[ i ].spbf.i_status = STATION_PRESERVES_I_STATUS_no_error;

			// ----------------------------

			// 2011.12.03 rmd : Set to (false) as the starting point. Meaning it DID irrigate last time.
			// And therefore will NOT get special weighting. Explicitly intialized for clarity sake.
			station_preserves.sps[ i ].spbf.did_not_irrigate_last_time = (false);

			// ----------
			
			nm_init_station_report_data_record( &(station_preserves.sps[ i ].station_report_data_rip) );

			// 7/19/2012 rmd : Well this IS the start time & date for the record.
			station_preserves.sps[ i ].station_report_data_rip.record_start_date = ldt.D;

			station_preserves.sps[ i ].station_report_data_rip.record_start_time = ldt.T;

			// ----------
			
			nm_init_station_history_record( &(station_preserves.sps[ i ].station_history_rip) );

			// 8/10/2015 rmd : For the case of station history the lines ORIGINATE at the start time on
			// a water day. And the start date and time are stamped then. So not needed here.
		}

		strlcpy( station_preserves.verify_string_pre, STATION_PRESERVES_VERIFY_STRING_PRE, BB_PRE_STRING_LENGTH );
	}
	else
	{
		#if ALERT_SHOW_ROUTINE_INITIALIZATIONS_DURING_STARTUP
			Alert_battery_backed_var_valid( "Station Preserves" );
		#endif
		
		// 8/11/2015 rmd : In this case look through the stations looking for station history rips
		// to append to the SDRAM complete station history records structure. And to start the
		// station history file save timer if we found any.
		found_rips = (false);
		
		xSemaphoreTakeRecursive( station_history_completed_records_recursive_MUTEX, portMAX_DELAY );

		// 8/12/2015 rmd : To run through the whole array like this takes about 1.6ms. Manageable
		// but quite a long time. I experimented with bit masking approaches and the time was still
		// never less than 1.6ms. Using the bitfield name directly is just about as efficient as any
		// other method - surprisingly.
		for( iii=0; iii<STATION_PRESERVE_ARRAY_SIZE; iii++ )
		{
			if( station_preserves.sps[ iii ].spbf.station_history_rip_needs_to_be_saved )
			{
				found_rips = (true);	

				station_history_completed.shr[ station_history_completed.rdfb.index_of_next_available ] = station_preserves.sps[ iii ].station_history_rip;
				
				nm_STATION_HISTORY_increment_next_avail_ptr();
			}
		}
		
		xSemaphoreGiveRecursive( station_history_completed_records_recursive_MUTEX );

		if( found_rips )
		{
			// 8/11/2015 rmd : In this case save the file quickly. Not the 2 hour usual delay. We
			// apparently had a code restart. Probably from a power failure. And there were unsaved
			// station history records preserved in the battery backed rip. What we don't know is how
			// close we are to another START TIME which then could put us at risk (if there were another
			// power failure) of record loss. So get the file saved! Saving clears the flag and the rip.
			FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_STATION_HISTORY, 5 );
			
			// 8/12/2015 rmd : We could also start the controller initiated timer to send the records.
			// But that would do so in an hour. Better is to post directly to the CI task to send them
			// now.
			CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_station_history, NULL, CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD, CI_POST_TO_BACK );
		}
	}

	xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Assigns the index for the station_preserves array based upon the station
    number and serial number. Station Number is 0 based. We reference iccc for our deduction
    of the index to use. If the sync is not completed then we return an error (false). If we
    can't find the controller serial number in the iccc array then we return (false).

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task. As part of start time
    detection. And again TD_CHECK when the acquire expected executes. As we need the station
    index into the station preserves to clear the station count (for the derate to
    re-learn). I also expect within the context of the COMM_MNGR when new expecteds are
    sent. Also executed within the context of the DISPLAY task when drawing reports. When
    after the rip record for a station.

	@RETURN (true) if all goes well and the index is valid. If (false) do not use the index.

	@ORIGINAL 2012.06.04 rmd

	@REVISIONS (none)
*/
extern BOOL_32 STATION_PRESERVES_get_index_using_box_index_and_station_number( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, UNS_32 *pindex_ptr )
{
	BOOL_32	rv;

	// ----------
	
	rv = (true);
	
	// 4/18/2014 rmd : Make sure what we've been handed to work with is within an acceptable
	// range. If out of range return false else return true.
	if( !RangeCheck_uint32( &pbox_index_0, BOX_INDEX_MINIMUM, BOX_INDEX_MAXIMUM, BOX_INDEX_DEFAULT, NULL, "Box Index" ) )
	{
		rv = (false);
	}

	if( !RangeCheck_uint32( &pstation_number_0, 0, (MAX_STATIONS_PER_CONTROLLER-1), 0, NULL, "Sta_Num" ) )
	{
		rv = (false);
	}

	// ----------
	
	// 4/18/2014 rmd : And the answer is...
	*pindex_ptr = (pbox_index_0 * MAX_STATIONS_PER_CONTROLLER) + pstation_number_0;
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION Assigns the index for the station_preserves array or the report rip. We
	reference iccc for our deduction of the index to use. If the sync is not completed then we
	return an error (false). If we can't find the controller serial number then we return
	(false).

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task. As part of start time
    detection. And again TD_CHECK when the acquire expected executes. As we need the station
    index into the station preserves to clear the station count (for the derate to
    re-learn). I also expect within the context of the COMM_MNGR when new expecteds are
    sent.

	@RETURN (true) if all goes well and the index is valid. If (false) do not use the index.

	@ORIGINAL 2012.06.04 rmd

	@REVISIONS (none)
*/
extern BOOL_32 STATION_PRESERVES_get_index_using_ptr_to_station_struct( STATION_STRUCT *const pss_ptr, UNS_32 *pindex_ptr )
{
	UNS_32	box_index_0, station_number_0;

	// ----------
	
	// 6/25/2012 rmd : We are suppose to be holding this to call the STATION get functions.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	box_index_0 = nm_STATION_get_box_index_0( pss_ptr );

	station_number_0 = nm_STATION_get_station_number_0( pss_ptr );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------

	return( STATION_PRESERVES_get_index_using_box_index_and_station_number( box_index_0, station_number_0, pindex_ptr ) );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/15/2013 ajv : Whenever the size of the System Preserves structure changes
// or the definition of the SYSTEM_BIT_FIELD_STRUCT change, the version must be
// rolled here to ensure the existing structure is re-initialized.
//
// 8/6/2015 rmd : Do not exceed (BB_PRE_STRING_LENGTH - 1) visible characters. If you do you
// can end up changing the version number with no recognition of the change. Follow the code
// through to understand this.
static const char SYSTEM_PRESERVES_VERIFY_STRING_PRE[] = "SYSTEM v04";

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called on startup prior to most but not all tasks running if the SYSTEM
    PRESERVES is found to be invalid. This is also used at the report roll time after we
    close out a record and save it to the system file. We restart the report record with
    this function.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the STARTUP task. And the TD_CHECK task when we
    close out the rip portion.

	@RETURN (none)

	@AUTHOR 2011.11.08 rmd
*/
extern void init_rip_portion_of_system_record( SYSTEM_REPORT_RECORD *psrr )
{
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	// 7/17/2012 rmd : Flood the record with zeros.
	memset( psrr, 0, sizeof( SYSTEM_REPORT_RECORD ) );

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Initialize the ufim portion for a single system_preserves structure. Most
    of these variables are used internal to the foal_irri maintenance function. And that
    function starts off by calling this function. HOWEVER some of the variables are
    referenced outside of the foal_irri irrigation list maintenance function. And therefore
    this is called as part of the program start process.

    @CALLER_MUTEX_REQUIREMENTS The caller should be holding the SYSTEM_PRESERVES_MUTEX for
    obvious reasons.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task during the foal_irri
    irrigation list maintenance function. And in the context of the STARTUP task when the
    system_preserves is readied upon program startup.

	@RETURN (none)

	@ORIGINAL 2012.04.26 rmd

	@REVISIONS
*/
static void _nm_system_preserves_init_ufim_variables( BY_SYSTEM_RECORD *pbsr_ptr )
{
	// 4/16/2012 rmd : Initialize to lowest weighted reason in prep for ratcheting it up as we
	// pass through the list. Because this variable is use outside the list maintenance function
	// must initialize on program startup.
	pbsr_ptr->highest_reason_in_list = IN_LIST_FOR_LOWEST_WEIGHTED_REASON;

	// 5/21/2012 rmd : Each pass these are reset and developed. They represent what is presently
	// in the list. And these varialbes are compared to another 'copy' of what was is the list
	// last pass through. And if the was is indicates an end to irrigation an alert line is made
	// about such. These are by system.
	pbsr_ptr->sbf.ufim_one_or_more_in_list_for_programmed_irrigation = (false);
	pbsr_ptr->sbf.ufim_one_or_more_in_list_for_manual_program = (false);
	pbsr_ptr->sbf.ufim_one_or_more_in_list_for_manual = (false);
	pbsr_ptr->sbf.ufim_one_or_more_in_list_for_walk_thru = (false);
	pbsr_ptr->sbf.ufim_one_or_more_in_list_for_test = (false);
	pbsr_ptr->sbf.ufim_one_or_more_in_list_for_mobile = (false);

	// 2011.11.09 rmd : DO NOT reset the "what_we_are_turning_on" variable here. It is intended
	// to be preserved between calls to the foal irrigation maintenance function. The rule here
	// is to initialize those to be LEARNED each pass through the maintenance list. And they are
	// only used within the list maintenance function.

	pbsr_ptr->ufim_valves_in_the_list_for_this_system = UNS_32_MIN;


	pbsr_ptr->ufim_maximum_valves_in_system_we_can_have_ON_now = UNS_32_MAX;


	pbsr_ptr->ufim_one_ON_from_the_problem_list_b = (false);


	pbsr_ptr->ufim_highest_priority_pump_waiting = PRIORITY_LOW;

	pbsr_ptr->ufim_highest_priority_non_pump_waiting = PRIORITY_LOW;


	pbsr_ptr->ufim_list_contains_waiting_programmed_irrigation_b = (false);

	pbsr_ptr->ufim_list_contains_waiting_pump_valves_b = (false);

	pbsr_ptr->ufim_list_contains_waiting_non_pump_valves_b = (false);


	pbsr_ptr->ufim_highest_reason_of_OFF_valve_to_set_expected = 0;  // 0 is the lowest reason in the list

	pbsr_ptr->ufim_one_ON_to_set_expected_b = (false);

	pbsr_ptr->ufim_list_contains_some_to_setex_that_are_not_ON_b = (false);


	pbsr_ptr->ufim_one_RRE_ON_to_set_expected_b = (false);

	pbsr_ptr->ufim_list_contains_some_RRE_to_setex_that_are_not_ON_b = (false);


	pbsr_ptr->ufim_list_contains_some_pump_to_setex_that_are_not_ON_b = (false);

	pbsr_ptr->ufim_list_contains_some_non_pump_to_setex_that_are_not_ON_b = (false);


	pbsr_ptr->ufim_there_is_a_PUMP_mix_condition_b = (false);

	pbsr_ptr->ufim_based_on_reason_in_list_at_least_one_valve_is_ON_that_wants_to_check_flow = (false);

	// 6/1/2012 rmd : The way the logic works this one is set true before we pass through the
	// list of those ON.
	pbsr_ptr->ufim_the_valves_ON_meet_the_flow_checking_cycles_requirement = (true);

	// Capture this for use in the turn on logic. If there is a mix condition we don't want to
	// aggrevate the mix in the case of programmed irrigation by adding more valves to the mix.
	// We want programmed irrigation valves to run down until there is no longer a mix - then we may
	// turn on programmed irrigation valves that are ready. Set these in support of determining
	// ThereIsAMixOfPumpAndNonPumpValvesOn. And also used to signal the IRRI MACHINES to
	// activate their pump output.
	pbsr_ptr->ufim_stations_ON_with_the_pump_b = (false);
	pbsr_ptr->ufim_stations_ON_without_the_pump_b = (false);


	// Can start at a 0 that's fine - even though we are using priority level 0 (SMS_HO).
	pbsr_ptr->ufim_highest_reason_in_list_available_to_turn_ON = UNS_32_MIN;
	pbsr_ptr->ufim_highest_pump_reason_in_list_available_to_turn_ON = UNS_32_MIN;
	pbsr_ptr->ufim_highest_non_pump_reason_in_list_available_to_turn_ON = UNS_32_MIN;
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION As part of an edit to particular irrigation system variables this function
    should be called. To cause a re-read of the irrigation system variables to the system
    preserves. A re-sync of sorts. Does not cause a restart of irrigation.

    @CALLER MUTEX REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES

    @EXECUTED Designed to be called from the SET functions within the irrigation system.
    Which would happen within the context of the KEY_PROCESS task. And perhaps within the
    COMM_MNGR task context.

	@RETURN (none)

	@ORIGINAL 2012.06.18 rmd

	@REVISIONS
*/
extern void SYSTEM_PRESERVES_request_a_resync( UNS_32 const psystem_gid )
{
	// 7/17/2012 rmd : We are about to perform a bit field manipulation within the system
	// preserves. Such work can take many instructions and should never be thought of as an
	// atomic operation.
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	BY_SYSTEM_RECORD	*bsr_ptr;

	bsr_ptr = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( psystem_gid );

	bsr_ptr->sbf.due_to_edit_resync_to_the_system_list = (true);

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Using the GID (assumed valid) use the GET functions to obtain from the
    irrigation_system list the variable settings of interest.

    @CALLER MUTEX REQUIREMENTS The caller should be holding the system_preserves mutex. As
    we are changing many of the system preserves variables.

	@MEMORY_RESPONSIBILITES

    @EXECUTED Called whenever a sync is performed to the system prexerves. This happens upon
    code startup. So within the context of the STARTUP task. And after certain user
    variables have changed. The edit of those variables sets the flag that causes these to
    be updated within the system_preserves. And the update happens within the context of the
    TD_CHECK task.

	@RETURN (none)

	@ORIGINAL 2012.05.04 rmd

	@REVISIONS
*/
static void _nm_sync_users_settings_to_this_system_preserve( BY_SYSTEM_RECORD	*pbsr_ptr )
{
	// 6/8/2012 rmd : To avoid picking these settings up out of the systems list each and every
	// second we acquire them all here at the registration. And when there has been a change in
	// their value.
	pbsr_ptr->flow_check_required_cell_iteration = SYSTEM_get_required_cell_iterations_before_flow_checking( pbsr_ptr->system_gid );

	pbsr_ptr->flow_check_required_station_cycles = SYSTEM_get_required_station_cycles_before_flow_checking( pbsr_ptr->system_gid );

	pbsr_ptr->flow_check_allow_table_to_lock = SYSTEM_get_allow_table_to_lock( pbsr_ptr->system_gid );

	pbsr_ptr->flow_check_derate_table_gpm_slot_size = SYSTEM_get_derate_table_gpm_slot_size( pbsr_ptr->system_gid );

	pbsr_ptr->flow_check_derate_table_max_stations_ON = SYSTEM_get_derate_table_max_stations_ON( pbsr_ptr->system_gid );

	pbsr_ptr->flow_check_derate_table_number_of_gpm_slots = SYSTEM_get_derate_table_number_of_gpm_slots( pbsr_ptr->system_gid );

	pbsr_ptr->sbf.flow_checking_enabled_by_user_setting = (BITFIELD_BOOL)SYSTEM_get_flow_checking_in_use( pbsr_ptr->system_gid );

	SYSTEM_get_flow_check_ranges_gpm( pbsr_ptr->system_gid, &(pbsr_ptr->flow_check_ranges_gpm[ 0 ]) );

	SYSTEM_get_flow_check_tolerances_plus_gpm( pbsr_ptr->system_gid, &(pbsr_ptr->flow_check_tolerance_plus_gpm[ 0 ]) );

	SYSTEM_get_flow_check_tolerances_minus_gpm( pbsr_ptr->system_gid, &(pbsr_ptr->flow_check_tolerance_minus_gpm[ 0 ]) );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Resets the dynamic variables that manage irrigation. Called on a normal
    program startup. And also when the chain falls into forced. Or comes out of forced. Both
    under control of the COMM_MNGR. By definition because these variables are reset on
    program startup they do not need to be battery backed but are for convience (attached to
    the system).

    One important recognition here. This function does not restart the flow_recording. On a
    program start the chain of function calls does lead us to restart the flow recording
    through another mechanism. That is because it is held in dynamic memory. This function
    (without restarting flow recording) can be called directly when the chain transitions in
    and out of forced.

    @CALLER MUTEX REQUIREMENTS The caller should be holding the system_preserves mutex. As
    we are changing many of the system variables.

	@MEMORY_RESPONSIBILITES

    @EXECUTED Executed within the context of the STARTUP task. And the COMM_MNGR task. See
    description above.

	@RETURN (none)

	@ORIGINAL 2012.05.04 rmd

	@REVISIONS
*/
extern void nm_SYSTEM_PRESERVES_restart_irrigation_and_block_flow_checking_for_150_seconds( BY_SYSTEM_RECORD *pbsr_ptr )
{
	// 5/7/2014 rmd : Note this function is executed from various tasks. Primarily the
	// comm_mngr, td_check, and the startup task. The startup task on reboot. So the 150 second
	// flow check block out is approriate. While scanning this function is also called via
	// td_check task as the NETWORK is deemed unavailable. Again the 150 second flow check
	// blockout is appropriate cause following the scan it is an approprate safety measure to
	// avoid false MLB's.
	
	UNS_32	j;

	// ----------
	
	// 5/3/2012 rmd : Because some of the ufim variables are used outside of the
	// foal_irri_list_maintenance function they must be initialized at startup.
	_nm_system_preserves_init_ufim_variables( pbsr_ptr );

	// ---------------------------------

	// 5/30/2012 rmd : The following group of variables are cleared on program startup. And as
	// this function says when irrigation is restarted. They are dynamically maintained and NOT
	// reset and rebuilt each time the foal_irri maintenance function runs. These variables are
	// kept accurate by adjusting them as events (turn ON's and OFF's) take place.

	// - the expected flow rate
	pbsr_ptr->ufim_expected_flow_rate_for_those_ON = 0;

	// - the number ON
	pbsr_ptr->system_master_number_of_valves_ON = 0;

	// - the variable that controls which segregated group we are turning ON -
	// segregated by if we are actively testing flow for the valves ON...this grouping is
	// controlled by the ALERT ACTIONS and the exclusion bit setting and if the reason in the
	// list dictates we will not measure flow.
	memset( pbsr_ptr->ufim_flow_check_group_count_of_ON, (0), sizeof(pbsr_ptr->ufim_flow_check_group_count_of_ON) );


	pbsr_ptr->ufim_number_ON_during_test = 0;


	// ---------------------------------

	pbsr_ptr->inhibit_next_turn_ON_remaining_seconds = 0;

	// ---------------------------------

	// 2012.01.05 rmd : These are reset too. These variables don't even need to be battery
	// backed but are for coding ease for now. If battery backed SRAM becomes an issue they can
	// be moved to SDRAM. And we'll have to create a by system array in SDRAM to do this. Also
	// see the poc_preserves. Similar issue.
	for( j=0; j<SYSTEM_5_SECOND_AVERAGES_KEPT; j++ )
	{
		pbsr_ptr->system_master_5_second_averages_ring[ j ] = 0.0;
	}

	pbsr_ptr->system_master_5_sec_avgs_next_index = 0;

	pbsr_ptr->system_master_most_recent_5_second_average = 0.0;

	pbsr_ptr->system_rcvd_most_recent_token_5_second_average = 0.0;


	pbsr_ptr->accumulated_gallons_for_accumulators_foal = 0.0;


	for( j=0; j<SYSTEM_STABILITY_AVERAGES_KEPT; j++ )
	{
		pbsr_ptr->system_stability_averages_ring[ j ] = 0.0;
	}

	pbsr_ptr->stability_avgs_index_of_last_computed = 0;


	pbsr_ptr->sbf.stable_flow = (false);


	pbsr_ptr->sbf.number_of_flow_meters_in_this_sys = 0;

	pbsr_ptr->sbf.number_of_pocs_in_this_system = 0;

	// 3/11/2016 rmd : Initialize to (true) and let the normal process set to (false) when the
	// flow meters are found. And to prevent flow checking or tallying until set (false).
	pbsr_ptr->sbf.there_are_pocs_without_flow_meters = (true);


	pbsr_ptr->sbf.flow_checking_enabled_and_allowed = (false);

	pbsr_ptr->sbf.flow_checking_enabled_by_user_setting = (false);

	pbsr_ptr->last_off__station_number_0 = 0;

	pbsr_ptr->last_off__reason_in_list = IN_LIST_FOR_MOBILE;  // Does this matter what it is intialized to?

	// -------------------------------

	pbsr_ptr->sbf.mv_open_for_irrigation = (false);

	pbsr_ptr->sbf.pump_activate_for_irrigation = (false);

	pbsr_ptr->sbf.master_valve_has_at_least_one_normally_closed = (false);
	
	// ----------
	
	// 10/17/2016 rmd : And wipe the MVOR delivered (IRRI side) settings. They will either get
	// restored when the chain comes up OR when the chain is declared as DOWN get initialized as
	// part of the FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down function.
	pbsr_ptr->sbf.delivered_MVOR_in_effect_opened = (false);
	pbsr_ptr->sbf.delivered_MVOR_in_effect_closed = (false);
	pbsr_ptr->delivered_MVOR_remaining_seconds = 0;
	
	// ----------
	
	// 7/2/2014 rmd : On program startup the valve open and close timer which blocks out MLB
	// checking and flow checking is cleared to 0. This has been decided when working on startup
	// that the MVJO timer will be the one to block flow checking and MLB checking on startup.
	// It could of been this one but the MVJO timer has the exception for flow checking about
	// one valve ON during test ignores the MVJO timer. And I wanted to retain that behaviour so
	// when a user powers up and performs a flow check expecting to see an overflow he gets it.
	// Will not get a MLB for 2 1/2 minutes though.
	pbsr_ptr->flow_checking_block_out_remaining_seconds = 0;

	// 5/3/2012 rmd : On program start the MVJO count down is set to 0. SEE NOTE ABOVE REGARDING
	// MLB BLOCKOUT BEHAVIOUR ON STARTUP USING THIS TIMER.
	//
	// (Additionally is there a concern to consider with a power failure at a slave controller
	// during irrigation? Should the MVJO countdown be loaded? The slave would have to tell us
	// - the master - about his restart.)
	pbsr_ptr->timer_MVJO_flow_checking_blockout_seconds_remaining = MLB_AND_FLOW_CHECKING_MVJO_BLOCKOUT_SECONDS;

	// 5/3/2012 rmd : Cleared to 0 on program start.
	pbsr_ptr->timer_MLB_just_stopped_irrigating_blockout_seconds_remaining = 0;
	
	// -------------------------------

	pbsr_ptr->transition_timer_all_stations_are_OFF = 0;

	pbsr_ptr->transition_timer_all_pump_valves_are_OFF = 0;

	// -------------------------------

	pbsr_ptr->sbf.system_level_no_valves_ON_therefore_no_flow_checking = (false);
	pbsr_ptr->sbf.system_level_valves_are_ON_and_actively_checking = (false);
	pbsr_ptr->sbf.system_level_valves_are_ON_and_has_updated_the_derate_table = (false);
	pbsr_ptr->sbf.system_level_valves_are_ON_and_waiting_to_acquire_expected = (false);
	pbsr_ptr->sbf.system_level_valves_are_ON_and_waiting_to_check_flow = (false);
	pbsr_ptr->sbf.system_level_valves_are_ON_and_waiting_to_update_derate_table = (false);
	pbsr_ptr->sbf.system_level_valves_are_ON_but_will_not_be_checking_flow = (false);

	pbsr_ptr->sbf.checked_or_updated_and_made_flow_recording_lines = (false);

	// -------------------------------

	// 5/21/2012 rmd : These trigger alert lines when transitioning from (true) to (false).
	// Clearing them on a startup generates no alert activity until an irrigation that was in
	// the list finishes. So even if the list is populated this is the correct way to start
	// up.
	pbsr_ptr->sbf.one_or_more_in_list_for_programmed_irrigation = (false);
	pbsr_ptr->sbf.one_or_more_in_list_for_manual_program = (false);
	pbsr_ptr->sbf.one_or_more_in_list_for_manual = (false);
	pbsr_ptr->sbf.one_or_more_in_list_for_walk_thru = (false);
	pbsr_ptr->sbf.one_or_more_in_list_for_test = (false);
	pbsr_ptr->sbf.one_or_more_in_list_for_rre = (false);

	// -------------------------------

	// 6/1/2012 rmd : Obtain these here. Once as opposed to every second. Which we would be
	// forced to do during the normal course of flow checking. And I'm trying to avoid referring
	// to the system list on a 1 hz basis.

	// 6/6/2012 rmd : But BEWARE. Since we like to keep even the unused record 'clean' this
	// function in turn is called as part of the complete initialization of a record. But there
	// is a problem. Here we are trying to obtain settings within supposedly actual systems. But
	// their system_gid is not valid. As a matter of fact it is 0. So we can avoid the ensueing
	// errors by testing for this 0.

	// 6/6/2012 rmd : We also call this function for each and every system_preserve record upon
	// program startup. And some records are not in use. But we initialize them anyway. To keep
	// things cleaned up.

	if( pbsr_ptr->system_gid == 0 )
	{
		pbsr_ptr->flow_check_required_cell_iteration = 0;

		pbsr_ptr->flow_check_required_station_cycles = 0;

		pbsr_ptr->flow_check_allow_table_to_lock = 0;

		pbsr_ptr->flow_check_derate_table_gpm_slot_size = 0;

		pbsr_ptr->flow_check_derate_table_max_stations_ON = 0;

		pbsr_ptr->flow_check_derate_table_number_of_gpm_slots = 0;

		// 7/17/2012 rmd : And for completeness sakes 0 these copies of user settings too.
		memset( pbsr_ptr->flow_check_ranges_gpm, (0), sizeof(pbsr_ptr->flow_check_ranges_gpm) );
		memset( pbsr_ptr->flow_check_tolerance_plus_gpm, (0), sizeof(pbsr_ptr->flow_check_tolerance_plus_gpm) );
		memset( pbsr_ptr->flow_check_tolerance_minus_gpm, (0), sizeof(pbsr_ptr->flow_check_tolerance_minus_gpm) );
	}
	else
	{
		// 6/18/2012 rmd : Remember SYSTEM_PRESERVES_restart_irrigation (this function we are in
		// now) gets called during program startup. And we certainly want to perform this sync to
		// assure we are aligned with the irrigation system file settings. Which assumes the system
		// file list is intact, in SDRAM memory (meaning it has already been read out of the file
		// system).
		_nm_sync_users_settings_to_this_system_preserve( pbsr_ptr );
	}

	pbsr_ptr->sbf.due_to_edit_resync_to_the_system_list = (false);

	// ---------------------------------

	pbsr_ptr->flow_check_derated_expected = 0;

	pbsr_ptr->flow_check_hi_limit = 0;

	pbsr_ptr->flow_check_lo_limit = 0;

	// -------------------------------
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Intitializes the part of the system record that is always intialized on
    each program startup. Which in itself by definition indicates the variables in this
    portion do not need to be battery backed. But remember are kept here for convenience.

    @CALLER MUTEX REQUIREMENTS The caller should be holding the system_prerves mutex. Not
    exactly sure of an instance that this is true. But sure seems like the right thing to do
    considering what we are doing.

    @EXECUTED Executed within the context of the STARTUP task. The TD_CHECK task. And quite
    possibly the COMM_MNGR task.

	@RETURN (none)

	@AUTHOR 2012.01.00 rmd
*/
static void _nm_system_preserves_init_portion_initialized_on_each_program_start( BY_SYSTEM_RECORD *pbsr_ptr )
{
	// 4/30/2012 rmd : Because the flow recording lines are held in dynamic memory (SDRAM) they
	// do not exist upon program restart. So indicate that by indicating the allocation is a
	// NULL. This is important as a test to see if the memory needs to be allocated and the flow
	// recording CI timer created.
	pbsr_ptr->frcs.original_allocation = NULL;

	// 10/21/2013 rmd : And important to clear this flag. If this flag is set on startup this
	// system will never send his flow recording to the web_app. On startup this flag should
	// always be (false) as a controller initiated send of flow recording cannot be in process
	// upon a restart.
	pbsr_ptr->frcs.pending_first_to_send_in_use = (false);

	// ----------
	
	// 6/8/2012 rmd : We do not need to do more. The act of the foal_irri init will take care of
	// restarting the irrigation. Including restarting the system_preserves. As a matter of fact
	// if we we're to restart the system_preserves here and there was a station ON we would get
	// errors when the foal_irri went to turn the stations OFF. Errors like the expected flow
	// rate being 0 when a station is ON. And other unexpected values. So let the foal_irri do
	// the irrigation and system_preserve restart.

	// ----------
	
	// 10/28/2014 rmd : And keep the unused expansion portion of the structure set to zeros. So
	// that in the future as one goes to use it [meaning they made some variables and reduced
	// the 'expansion' size accordingly] the new variables will start life as zero.
	memset( &pbsr_ptr->expansion, 0, sizeof(pbsr_ptr->expansion) );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Intitializes the part of the system record that is ONLY intialized when the
    desire is to COMPLETELY restart the record. Which will happen when we assign a SYSTEM to
    a record and when the battery backed SRAM test strings fail for this structure.

    @CALLER MUTEX REQUIREMENTS The caller should be holding the system_prerves mutex. As we
    are manuipulating multiple parameters of the system preserves record.

    @EXECUTED Executed within the context of the STARTUP task. The TD_CHECK task. And quite
    possibly the COMM_MNGR task.

	@RETURN (none)

	@AUTHOR 2012.01.00 rmd
*/
extern void nm_SYSTEM_PRESERVES_complete_record_initialization( BY_SYSTEM_RECORD *pbsr_ptr )
{
	pbsr_ptr->system_gid = 0;  // The not in use value is 0.

	// ----------------------

	// 2011.11.09 rmd : Note this variable tracking pump or non-pump turn ON mode is meant to be
	// preserved from call to call of the foal irrigation maintenance function. Be careful NOT
	// to reset it in the general ufim clearing function called at the top of the FOAL
	// irrigation maintenance function. Also be careful not to reset it at the report roll time.
	// We DO NOT want to reset this variable during that activity. AND do not reset upon code
	// startup. I think it should persist throughout a powerfail and not be reset. So that we
	// continue turning on what we were turning on with respect to pump or non-pump.
	pbsr_ptr->ufim_what_are_we_turning_on_b = TURNING_ON_PUMP_VALVES;

	// ----------------------

	// Initialize the MVOR here. These variables are to survive a power fail.
	pbsr_ptr->sbf.MVOR_in_effect_opened = (false);
	pbsr_ptr->sbf.MVOR_in_effect_closed = (false);
	pbsr_ptr->MVOR_remaining_seconds = 0;

	pbsr_ptr->sbf.delivered_MVOR_in_effect_opened = (false);
	pbsr_ptr->sbf.delivered_MVOR_in_effect_closed = (false);
	pbsr_ptr->delivered_MVOR_remaining_seconds = 0;

	// ----------------------

	pbsr_ptr->latest_mlb_record.there_was_a_MLB_during_irrigation = (false);
	pbsr_ptr->latest_mlb_record.there_was_a_MLB_during_mvor_closed = (false);
	pbsr_ptr->latest_mlb_record.there_was_a_MLB_during_all_other_times = (false);

	pbsr_ptr->latest_mlb_record.mlb_measured_during_irrigation_gpm = 0;
	pbsr_ptr->latest_mlb_record.mlb_limit_during_irrigation_gpm = 0;

	pbsr_ptr->latest_mlb_record.mlb_measured_during_mvor_closed_gpm = 0;
	pbsr_ptr->latest_mlb_record.mlb_limit_during_mvor_closed_gpm = 0;

	pbsr_ptr->latest_mlb_record.mlb_measured_during_all_other_times_gpm = 0;
	pbsr_ptr->latest_mlb_record.mlb_limit_during_all_other_times_gpm = 0;


	pbsr_ptr->delivered_mlb_record.there_was_a_MLB_during_irrigation = (false);
	pbsr_ptr->delivered_mlb_record.there_was_a_MLB_during_mvor_closed = (false);
	pbsr_ptr->delivered_mlb_record.there_was_a_MLB_during_all_other_times = (false);

	pbsr_ptr->delivered_mlb_record.mlb_measured_during_irrigation_gpm = 0;
	pbsr_ptr->delivered_mlb_record.mlb_limit_during_irrigation_gpm = 0;

	pbsr_ptr->delivered_mlb_record.mlb_measured_during_mvor_closed_gpm = 0;
	pbsr_ptr->delivered_mlb_record.mlb_limit_during_mvor_closed_gpm = 0;

	pbsr_ptr->delivered_mlb_record.mlb_measured_during_all_other_times_gpm = 0;
	pbsr_ptr->delivered_mlb_record.mlb_limit_during_all_other_times_gpm = 0;

	// ----------------------

	_nm_system_preserves_init_portion_initialized_on_each_program_start( pbsr_ptr );

	nm_SYSTEM_PRESERVES_restart_irrigation_and_block_flow_checking_for_150_seconds( pbsr_ptr );

	init_rip_portion_of_system_record( &(pbsr_ptr->rip) );

	// 9/8/2016 skc : Make sure the budget record is set to zero also
	memset( &pbsr_ptr->budget, 0, sizeof( BY_SYSTEM_BUDGET_RECORD ) );

	// 5/29/2012 rmd : Initialize the derate table and cell iterations record. Battery backed to
	// preserve throughout power fails. Only initialized when the entire system_preserve record
	// is being restarted.
	memset( &(pbsr_ptr->derate_table_10u), 0, sizeof((pbsr_ptr->derate_table_10u)) );

	memset( &(pbsr_ptr->derate_cell_iterations), 0, sizeof((pbsr_ptr->derate_cell_iterations)) );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called on startup prior to most but not all tasks running to determine
    SYSTEM PRESERVES validity.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the STARTUP task.

	@RETURN (none)

	@AUTHOR 2011.11.08 rmd
*/
extern void init_battery_backed_system_preserves( const BOOL_32 pforce_the_initialization )
{
	INT_32	i;

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	// 11/16/2012 rmd : We use a local variable to see if our strings failed.
	BOOL_32	lmy_strings_failed;
	
	lmy_strings_failed = (false);

	// 8/7/2015 rmd : If the string isn't intact signal we've failed.
	if( strncmp( system_preserves.verify_string_pre, SYSTEM_PRESERVES_VERIFY_STRING_PRE, sizeof(SYSTEM_PRESERVES_VERIFY_STRING_PRE) ) != 0 )
	{
		lmy_strings_failed = (true);
	}
	
	// ----------

	if( pforce_the_initialization || lmy_strings_failed )
	{
		Alert_battery_backed_var_initialized( "System Preserves", pforce_the_initialization );

		for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
		{
			nm_SYSTEM_PRESERVES_complete_record_initialization( &(system_preserves.system[ i ]) );
		}

		strlcpy( system_preserves.verify_string_pre, SYSTEM_PRESERVES_VERIFY_STRING_PRE, BB_PRE_STRING_LENGTH );
	}
	else
	{
		#if ALERT_SHOW_ROUTINE_INITIALIZATIONS_DURING_STARTUP
			Alert_battery_backed_var_valid( "System Preserves" );
		#endif

		// Do only the items we ALWAYS set to their defaults.
		for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
		{
			_nm_system_preserves_init_portion_initialized_on_each_program_start( &(system_preserves.system[ i ]) );
		}
	}

	// ----------

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Invoked on a 1Hz basis. Roll through each system and set to 0xFFFF the
    number of valves allowed ON. Also set the present number of valves ON to zero. It
    doesn't matter if the system if in use or not. Meaning does it have a non-zero gid or
    not.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the TD_CHECK task as part of foal irrigation
    list maintenance function.

	@RETURN (none)

	@AUTHOR 2011.11.02 rmd
*/
extern void SYSTEM_PRESERVES_init_system_preserves_ufim_variables_for_all_systems( void )
{
	// 2011.11.08 rmd : Take the system_preserves MUTEX to protect against a change via another
	// task. Though so far that isn't possible that I know of cause all manipulation of this
	// data is done within the context fo the TD_CHECK task.
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	INT_32	i;

	for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
	{
		_nm_system_preserves_init_ufim_variables( &(system_preserves.system[ i ]) );
	}

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Returns pointer to one of the system_preserves records. If a record isn't
    found to have been previously assigned to this GID then this function returns a NULL.
    And the user must consider that. The intention is that the housekeeping function that
    aligns the system_preserves array to the system file has been previously called. This
    function must return a NULL for that process to work. Subsequent users of this function
    expect a non-NULL return. If it is NULL I recommend you abandon whatever it is you are
    doing! Because it should be considered an unexpected error.

	@CALLER_MUTEX_REQUIREMENTS (none)

    @EXECUTED Executed within the context of the TD_CHECK task. And the COMM_MNGR task.

    @RETURN A pointer to the system preserves array entry if one can be obtained. Or NULL if
    such an assignment has not been made.

	@AUTHOR 2012.01.20 rmd
*/
extern BY_SYSTEM_RECORD* SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( UNS_32 const psystem_gid )
{
	BY_SYSTEM_RECORD	*rv;

	rv = NULL;

	// 2012.01.31 rmd : Do not consider a system_gid of 0. That would match up with an
	// UN-ASSIGNED system_preserves record.
	if( psystem_gid == 0 )
	{
		Alert_Message( "System GID should not be 0" );
	}
	else
	{
		xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

		INT_32	i;

		for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
		{
			if( system_preserves.system[ i ].system_gid == psystem_gid )
			{
				rv = &system_preserves.system[ i ];

				break ;
			}
		}

		xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION The assumption is that the caller has previously checked and there is no
    system preserve record for the passed system_gid. The function scrolls through the array
    of system_preserves looking for an unassigned entry to take. If one cannot found (WHICH
    SHOULD NEVER HAPPEN) we return a NULL. And make an alert line about that occurrance. We
    also will first check t make sure a record hasn't already been assigned. As that would
    be unexpected by design of this functions use.

	@CALLER MUTEX REQUIREMENTS The caller should be holding the system_preserves MUTEX.

    @EXECUTED Executed within the context of the TD_CHECK task. And COMM_MNGR task.

    @RETURN A pointer to the system preserves array entry if one can be obtained. Otherwise
    a NULL is returned.

	@AUTHOR 2012.01.20 rmd
*/
static BY_SYSTEM_RECORD* _nm_SYSTEM_register_system_preserve_for_this_system_gid( UNS_32 const psystem_gid )
{
	DATE_TIME			ldt;

	BY_SYSTEM_RECORD	*rv;

	INT_32				i;

	// ----------
	
	rv = NULL;

	// There should not be a system_preserve with this gid.
	for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
	{
		if( system_preserves.system[ i ].system_gid == psystem_gid )
		{
			Alert_Message( "SYSTEM: unexpected assignment request" );

			rv = &(system_preserves.system[ i ]);

			break ;
		}
	}

	// RV should be NULL. Now go about the real business here. Find an unused entry.
	if( rv == NULL )
	{
		for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
		{
			// 6/6/2012 rmd : Test if the record is in use.
			if( system_preserves.system[ i ].system_gid == 0 )
			{
				// 6/6/2012 rmd : NOTE - this is a delicate and less than easy sequence (unfortunately). The
				// act of cleaning the record completely sets the gid to 0. And with the gid set to 0 we do
				// not progperly initialize the system_preserves user settings extracted from the SYSTEM
				// file. But we want to clean up the record. So initialize it ... which sets the system gid
				// to 0. Then set the system gid. Then call the restart_irrigation function which will then
				// properly extract the SYSTEM file values. And finally mark when the recording period began
				// for the report rip. This first record is sort of a bastard in that the period is from the
				// time the record was sync'd till the roll time. Not sure if anything to do about that.
				// Chuck the first created record?

				// ** STEP 1 : see NOTE.
				nm_SYSTEM_PRESERVES_complete_record_initialization( &(system_preserves.system[ i ]) );

				// ** STEP 2 : see NOTE.
				system_preserves.system[ i ].system_gid = (UNS_16)psystem_gid;  // Occupy it!

				// ** STEP 3 : see NOTE.
				nm_SYSTEM_PRESERVES_restart_irrigation_and_block_flow_checking_for_150_seconds( &(system_preserves.system[ i ]) );

				// ----------
				
				// ** STEP 4 : Initialize the rip. see NOTE.

				// 5/16/2018 rmd : This line, assigning the gid, was missing, until I fixed it here. Without
				// this line, after a mainline was created, the first report record had a 0 gid!
				system_preserves.system[ i ].rip.system_gid = psystem_gid;
				
				EPSON_obtain_latest_time_and_date( &ldt );
				
				// 7/24/2012 rmd : Set the report start time. This should only come into play when a system
				// preserves record is synchronized to the system file. Normally this is set at the actual
				// roll time.
				system_preserves.system[ i ].rip.start_dt = ldt;

				// ----------------------

				rv = &system_preserves.system[ i ];

				break ;
			}
		}
	}

	// If still no answer I want to hear about this.
	if( rv == NULL )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "System Preserves problem" );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This is the system_preserves housekeeping function. Will result is a system
    preserves array that is sync'd with the systems declared within the system file. Though
    erros could result in this not being ture so the user must not assume there is always a
    preserves record available for each gid in the file. Something could go wrong.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the TD_CHECK task. And COMM_MNGR task.

    @RETURN (none)

	@AUTHOR 2012.01.20 rmd
*/
extern void SYSTEM_PRESERVES_synchronize_preserves_to_file( void )
{
	// 6/24/2015 rmd : Called at a ONE HERTZ rate from TD_CHECK.
	
	// 4/11/2012 rmd : If we get an error I am going to clean all the poc preserves. Even though
	// a corrupt poc_preserves may not be the actual cause of the error. But since there is an
	// integrity problem somewhere let's clean some house.
	BOOL_32	lerror;

	lerror = (false);

	// For each system make sure there is a system_preserve record. Also making sure there
	// aren't any so called 'left-over' system preserve records that no longer have a matching
	// system gid in the system file. This will be the case when a system is deleted.
	UNS_32	s, total_systems;

	void	*system_ptr;

	UNS_32	lgid;

	// --------------------------------

	// 2012.01.17 rmd : Yes we are about to peruse through the system_preserves. And they can
	// also be changed by the COMM_MNGR task. So protect against potential clash.
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	// 2012.02.16 ajv : Since we're accessing the irrigation system list and are
	// expecting the contents of the list to remain static during this process,
	// take the list_poc_recursive_MUTEX to prevent anyone else from modifying
	// the list.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	// --------------------------------

	for( s=0; s<MAX_POSSIBLE_SYSTEMS; s++ )
	{
		system_preserves.system[ s ].sbf.accounted_for = (false);
	}

	// --------------------------------

	total_systems = SYSTEM_get_list_count();

	for( s=0; s<total_systems; s++ )
	{
		system_ptr = SYSTEM_get_group_at_this_index( s );

		if( system_ptr == NULL )
		{
			// 4/11/2012 rmd : This should of course never happen.
			ALERT_MESSAGE_WITH_FILE_NAME( "SYSTEM_PRESERVES : unexp NULL ptr" );

			lerror = (true);

			break;
		}

		lgid = nm_GROUP_get_group_ID( system_ptr );

		if( lgid == 0 )
		{
			// 4/11/2012 rmd : This should of course never happen.
			ALERT_MESSAGE_WITH_FILE_NAME( "SYSTEM_PRESERVES : unexp NULL gid" );

			lerror = (true);

			break;
		}

		// 2/11/2016 ajv : If the file entry is in use, we want to include it in the preserves. If
		// it's not in use but already exists in the preserves (meaning it was deleted by the user)
		// this will result in the system being removed, which is what we want.
		if( SYSTEM_this_system_is_in_use( system_ptr ) )
		{
			// 4/11/2012 rmd : Scroll through the preserves and see if we can find a match.
			BY_SYSTEM_RECORD	*bsr_ptr;

			bsr_ptr = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( lgid );

			// It could be NULL if this is a NEW system! That's the point of what we're doing.
			if( bsr_ptr != NULL )
			{
				// And mark the record as checked. So we don't think it is an obsolete record.
				bsr_ptr->sbf.accounted_for = (true);
			}
		}
	}

	// --------------------------------

	// 4/24/2012 rmd : If there was an error let's just get to the end where we clean the system
	// preserves array.
	if( lerror == (false) )
	{
		// NOW test for any system_preserve records that have a gid and have not been checked.
		// Because that condition means they are left over obsolete records from systems that once
		// existed.
		for( s=0; s<MAX_POSSIBLE_SYSTEMS; s++ )
		{
			// 7/17/2014 rmd : If we don't test also for the gid to be non-zero then we would be running
			// through and performing complete record intializations every second.
			if( (system_preserves.system[ s ].system_gid != 0) && (system_preserves.system[ s ].sbf.accounted_for == (false)) )
			{
				// Well this is what we call housekeeping. When one deletes a SYSTEM I would expect to see
				// this message. As we 'recover' the once used system preserves record. Probably comment
				// this line out once proven to work if it is a nuisance.
				Alert_system_preserves_activity( PRESERVES_CLEANING_OBSOLETE );

				// ----------
				
				// 6/6/2012 rmd : What about freeing the flow recording memory. If it has been allocated.
				if( system_preserves.system[ s ].frcs.original_allocation != NULL )
				{
					// 10/23/2013 rmd : Free the allocation. The pointer is set to NULL (critical to flow
					// recording as how it recognizes the allocation has not been made) as part of the complete
					// record init below.
					mem_free( system_preserves.system[ s ].frcs.original_allocation );
					
					// 10/21/2013 rmd : And free the memory associated with the timer. If the allocation was
					// made the timer was also made. The system_preserves MUTEX assures us of this.
					xTimerDelete( system_preserves.system[ s ].frcs.when_to_send_timer, portMAX_DELAY );
				}
				
				// ----------
				
				// Clean it up. Completely. Probably only have to set the gid = to 0 but let's keep the
				// records put out of use clean.
				nm_SYSTEM_PRESERVES_complete_record_initialization( &(system_preserves.system[ s ]) );
			}

		}  // of for each system

	}  // of if no error

	// 4/24/2012 rmd : If there was an error let's just get to the end where we clean the system
	// preserves array.
	if( lerror == (false) )
	{
		// OKAY so now we have a system_preserves array that MAY not contain ALL the systems. If a
		// NEW system was created the first time this is called this is the case. So pass back
		// through the list. This time if there is no system preserve for a particular gid go make
		// the assignment.
		total_systems = SYSTEM_num_systems_in_use();

		for( s=0; s<total_systems; s++ )
		{
			system_ptr = SYSTEM_get_group_at_this_index( s );

			if( system_ptr == NULL )
			{
				// 4/11/2012 rmd : This should of course never happen.
				ALERT_MESSAGE_WITH_FILE_NAME( "SYSTEM_PRESERVES : unexp NULL ptr" );

				lerror = (true);

				break;
			}

			lgid = nm_GROUP_get_group_ID( system_ptr );

			if( lgid == 0 )
			{
				// 4/11/2012 rmd : This should of course never happen.
				ALERT_MESSAGE_WITH_FILE_NAME( "SYSTEM_PRESERVES : unexp NULL gid" );

				lerror = (true);

				break;
			}

			// So now we have the GID of one of the groups. Scroll through the system preserve records
			// to find it. And assign it if not found.
			BY_SYSTEM_RECORD	*bsr_ptr;

			// Try to find the exisiting assignment.
			bsr_ptr = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( lgid );

			// It could be NULL if this is a NEW poc! That's what the sync is all about.
			if( bsr_ptr == NULL )
			{
				// 8/21/2015 rmd : Show us what is happening.
				Alert_system_preserves_activity( PRESERVES_REGISTERING_NEW );


				// Go establish a new connection.
				bsr_ptr = _nm_SYSTEM_register_system_preserve_for_this_system_gid( lgid );
			}
			else
			if( bsr_ptr->sbf.due_to_edit_resync_to_the_system_list == (true) )
			{
				// 8/21/2015 rmd : Show us what is happening.
				Alert_system_preserves_activity( PRESERVES_REFRESHING_EXISTING );

				_nm_sync_users_settings_to_this_system_preserve( bsr_ptr );
			}

			bsr_ptr->sbf.due_to_edit_resync_to_the_system_list = (false);

		}  // of for each system

	}  // of if no error

	if( lerror == (true) )
	{
		// 4/24/2012 rmd : We've decided to clean all the system preserve records when an error
		// occurs. There isn't a particular fix trying to be accomplished here. But seems like a
		// reasonable thing to do. Though this may cause even more confusion and errors elsewhere as
		// there are NO valid system_preserve records.
		for( s=0; s<MAX_POSSIBLE_SYSTEMS; s++ )
		{
			nm_SYSTEM_PRESERVES_complete_record_initialization( &(system_preserves.system[ s ]) );
		}
	}

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Used when a station is detected ON already and we are developing the
    present system limit based on the on_at_a_time groups for the stations ON. Also used
    when we are actually turning valves ON. Both of those moments are within the foal
    irrigation maintenance function.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the TD_CHECK task as part of foal irrigation
    list maintenance function.

	@RETURN (none)

	@AUTHOR 2011.11.08 rmd
*/
extern void SYSTEM_PRESERVES_merge_new_allowed_ON_into_the_limit( BY_SYSTEM_RECORD *pbsr, UNS_16 const ponatatime_gid )
{
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	if( pbsr != NULL)
	{
		UNS_32	allowed_ON_within_system;

		allowed_ON_within_system = ON_AT_A_TIME_get_on_at_a_time_within_system_for_this_gid( ponatatime_gid );

		if( allowed_ON_within_system < pbsr->ufim_maximum_valves_in_system_we_can_have_ON_now )
		{
			// If it's less update the limit.
			pbsr->ufim_maximum_valves_in_system_we_can_have_ON_now = allowed_ON_within_system;
		}

		// By the way now that we have adjusted the limit can we test against how many are ON. This
		// should not fail. Else we have turned on more valves than allowed and our logic failed to
		// get to this point.
		//
		// Beaware - if the ON_AT_A_TIME get function fails it will return a 1. And that could cause us
		// to fail this! I still think if things like this fail we should kill the irrigation that
		// is taking place else the alerts will drown. To think about.
		if( pbsr->system_master_number_of_valves_ON > pbsr->ufim_maximum_valves_in_system_we_can_have_ON_now )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "FOAL_IRRI: too many valves ON." );
		}
	}
	else
	{
		// 2011.11.09 rmd : This cannot happen since I re-wrote the get pointer function to never
		// return a NULL.
		ALERT_MESSAGE_WITH_FILE_NAME( "system_preserve problem" );
	}

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION We have a valve we are considering to turn ON. The system and on_at_a_time
    gid's for this valve are passed in. There are two tests to make. First for the system
    this valve belongs to are we at the limit already for the number of valves allowed ON.
    The second test would be to get the system limit for this valve (remember it is NOT ON
    yet so that limit is not yet incorporated into the system limit we have here) and to see
    if we turn this valve ON would we exceed that new system limit.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the TD_CHECK task as part of foal irrigation
    list maintenance function.

    @RETURN If we can turn the valve ON return (true), if not allowed to come ON return
    (false).

	@AUTHOR 2011.11.08 rmd
*/
extern BOOL_32 SYSTEM_PRESERVES_is_this_valve_allowed_ON( BY_SYSTEM_RECORD *pbsr, UNS_16 const ponatatime_gid )
{
	BOOL_32	rv;

	rv = (false);

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	if( pbsr != NULL )
	{
		// FIRST test. Are we under the limit for the valves on within this system?
		if( pbsr->system_master_number_of_valves_ON < pbsr->ufim_maximum_valves_in_system_we_can_have_ON_now )
		{
			// If it's less update the limit. There is room according to the rules for the valves
			// already ON. Now test according to the rule for the valve we are planning on turning ON.
			UNS_32	new_allowed_ON_within_system;

			new_allowed_ON_within_system = ON_AT_A_TIME_get_on_at_a_time_within_system_for_this_gid( ponatatime_gid );

			if( new_allowed_ON_within_system > pbsr->system_master_number_of_valves_ON )
			{
				// This means the number presently ON is less than the system limit for the valve we are
				// asking to turn ON. So it could come ON and all system rules would be satified.
				rv = (true);
			}
		}
		else
		if( pbsr->system_master_number_of_valves_ON > pbsr->ufim_maximum_valves_in_system_we_can_have_ON_now )
		{
			// A Sanity check. This should not be possible. No action I guess. But alert about it.
			ALERT_MESSAGE_WITH_FILE_NAME( "FOAL_IRRI: too many valves ON." );
		}
	}
	else
	{
		// 2011.11.09 rmd : This cannot happen since I re-wrote the get pointer function to never
		// return a NULL.
		Alert_func_call_with_null_ptr();
	}

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SYSTEM_PRESERVES_there_is_a_mlb( SYSTEM_MAINLINE_BREAK_RECORD *psmlbr_ptr )
{
	// 7/17/2014 rmd : Called from a variety of tasks. And is called from both the IRRI side and
	// the FOAL side. No MUTEX needed even if the data is changing this is not a concern. Either
	// it returns (true) or (false) now or the next time executed.
	
	// 7/17/2014 rmd : Returns (true) if a mlb exists.
	
	// ----------
	
	return( psmlbr_ptr->there_was_a_MLB_during_all_other_times || psmlbr_ptr->there_was_a_MLB_during_irrigation || psmlbr_ptr->there_was_a_MLB_during_mvor_closed );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/6/2015 rmd : Do not exceed (BB_PRE_STRING_LENGTH - 1) visible characters. If you do you
// can end up changing the version number with no recognition of the change. Follow the code
// through to understand this.
static const char POC_PRESERVES_VERIFY_STRING_PRE[] = "POC v02";

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Intitializes the part of the poc record that is always intialized on each
    program startup. Which in itself probably indicates the variables in this portion do not
    need to be battery backed. But remember are kept here for convenience. When a record is
    first picked out and assigned to a POC we also call this function. That happens at
    various yet TBD spots in the code.

    @CALLER MUTEX REQUIREMENTS The caller should be holding the poc_preserves mutex. As we
    are manuipulating multiple parameters of the poc preserves record.

    @EXECUTED Executed within the context of the STARTUP task. The TD_CHECK task. And quite
    possibly the COMM_MNGR task.

	@RETURN (none)

	@AUTHOR 2012.01.00 rmd
*/
static void nm_poc_preserves_init_portion_initialized_on_each_program_start( BY_POC_RECORD *pbpr_ptr )
{
	UNS_32	b;

	// ----------

	// 5/28/2014 rmd : Leave the rip alone on routine program start.
	
	// ----------
	
	for( b=0; b<POC_BYPASS_LEVELS_MAX; b++ )
	{
		// 8/13/2012 rmd : Regarding these current measurement variables and their accompanying
		// flags. What to do on program start? I've decided not to 0 the last recevied current
		// measurements on program startup. I will set their distribution flags to false though. The
		// argument is that these numbers are the last measured current and should be preserved.

		pbpr_ptr->ws[ b ].fm_accumulated_pulses_irri = 0;
		pbpr_ptr->ws[ b ].fm_accumulated_ms_irri = 0;
		pbpr_ptr->ws[ b ].fm_latest_5_second_pulse_count_irri = 0;
		pbpr_ptr->ws[ b ].fm_delta_between_last_two_fm_pulses_4khz_irri = 0;
		pbpr_ptr->ws[ b ].fm_seconds_since_last_pulse_irri = 0;

		// 6/4/2014 rmd : These 5 are only used at the master.
		pbpr_ptr->ws[ b ].fm_accumulated_pulses_foal = 0;
		pbpr_ptr->ws[ b ].fm_accumulated_ms_foal = 0;
		pbpr_ptr->ws[ b ].fm_latest_5_second_pulse_count_foal = 0;
		pbpr_ptr->ws[ b ].fm_delta_between_last_two_fm_pulses_4khz_foal = 0;
		pbpr_ptr->ws[ b ].fm_seconds_since_last_pulse_foal = 0;

		pbpr_ptr->ws[ b ].accumulated_gallons_for_accumulators_foal = 0.0;

		// ----------
		
		pbpr_ptr->ws[ b ].latest_5_second_average_gpm_foal = 0.0;

		pbpr_ptr->ws[ b ].delivered_5_second_average_gpm_irri = 0.0;

		// ----------

		pbpr_ptr->ws[ b ].pbf.there_is_flow_meter_count_data_to_send_to_the_master = (false);

		pbpr_ptr->ws[ b ].pbf.send_mv_pump_milli_amp_measurements_to_the_master = (false);

		// ----------

		// 8/8/2012 rmd : And these two IRRI side variables. They should start (false) with every
		// program start. And let communication with the master FOAL machine cause them to be set.
		pbpr_ptr->ws[ b ].pbf.master_valve_energized_irri = (false);
		pbpr_ptr->ws[ b ].pbf.pump_energized_irri = (false);
	}

	// ----------
	
	// 5/29/2014 rmd : Indicate the bypass is not open.
	pbpr_ptr->bypass_activate = (false);

	pbpr_ptr->msgs_to_tpmicro_with_no_valves_ON = 0;

	// ----------
	
	// 10/28/2014 rmd : And keep the unused expansion portion of the structure set to zeros. So
	// that in the future as one goes to use it [meaning they made some variables and reduced
	// the 'expansion' size accordingly] the new variables will start life as zero.
	memset( &pbpr_ptr->expansion, 0, sizeof(pbpr_ptr->expansion) );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Intitializes the part of the poc record that is ONLY intialized when the
    desire is to COMPLETELY restart the record. Which will happen when we assign a POC to a
    record and when the battery backed SRAM test strings fail for this structure.

    @CALLER MUTEX REQUIREMENTS The caller should be holding the poc_preserves mutex. As we
    are manuipulating multiple parameters of the poc preserves record.

    @EXECUTED Executed within the context of the STARTUP task. The TD_CHECK task. And quite
    possibly the COMM_MNGR task.

	@RETURN (none)

	@AUTHOR 2012.01.00 rmd
*/
static void nm_poc_preserves_complete_record_initialization( BY_POC_RECORD *pbpr_ptr )
{
	// 8/13/2012 rmd : Fill the whole record with zeros. This does it all. Including the report
	// record contained within the structure. No need to perform any other specific
	// initialization steps.
	memset( pbpr_ptr, 0x00, sizeof(BY_POC_RECORD) );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called on startup prior to most but not all tasks running to determine
    SYSTEM PRESERVES validity.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the STARTUP task.

	@RETURN (none)

	@AUTHOR 2011.11.08 rmd
*/
extern void init_battery_backed_poc_preserves( const BOOL_32 pforce_the_initialization )
{
	INT_32	i;

	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	// 11/16/2012 rmd : We use a local variable to see if our strings failed.
	BOOL_32	lmy_strings_failed;
	
	lmy_strings_failed = (false);

	// 8/7/2015 rmd : If the string isn't intact signal we've failed.
	if( strncmp( poc_preserves.verify_string_pre, POC_PRESERVES_VERIFY_STRING_PRE, sizeof(POC_PRESERVES_VERIFY_STRING_PRE) ) != 0 )
	{
		lmy_strings_failed = (true);
	}
	
	// ----------

	if( pforce_the_initialization || lmy_strings_failed )
	{
		Alert_battery_backed_var_initialized( "POC Preserves", pforce_the_initialization );

		for( i=0; i<MAX_POCS_IN_NETWORK; i++ )
		{
			nm_poc_preserves_complete_record_initialization( &(poc_preserves.poc[ i ]) );
		}

		strlcpy( poc_preserves.verify_string_pre, POC_PRESERVES_VERIFY_STRING_PRE, BB_PRE_STRING_LENGTH );
	}
	else
	{
		#if ALERT_SHOW_ROUTINE_INITIALIZATIONS_DURING_STARTUP
			Alert_battery_backed_var_valid( "POC Preserves" );
		#endif
	}

	// ----------
	
	// Now for the items we ALWAYS set to their defaults. NOTE: mostly these are the items that
	// DO NOT need to be battery backed. (But for association ease I have added them to the poc
	// battery backed structure.)
	for( i=0; i<MAX_POCS_IN_NETWORK; i++ )
	{
		nm_poc_preserves_init_portion_initialized_on_each_program_start( &(poc_preserves.poc[ i ]) );
	}
	
	// 7/1/2014 rmd : And ensure the system preserves pointer in each poc_preserves entry is
	// accurate. As well as any other settings stored in the poc file. I suppose the
	// system_preserves address can't move. But this seems like a prudent safety measure to
	// take. Will resync at first td_check second detection.
	poc_preserves.perform_a_full_resync = (true);
	
	// ----------
	
	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern BY_POC_RECORD* POC_PRESERVES_get_poc_preserve_for_this_poc_gid( UNS_32 const ppoc_gid, UNS_32 *pindex_ptr )
{
	// 8/8/2016 rmd : This function needs to be used with CAUTION. For a given POC on a chain
	// the GID for it is not guaranteed to match across the boxes. In other words do not use GID
	// as a key field to find a POC in the preserves. [One can get away with this if working
	// within a single box.]

	// ----------

	BY_POC_RECORD	*rv;

	UNS_32	i;

	rv = NULL;

	*pindex_ptr = 0;

	// 4/5/2012 rmd : We are running through the array. Keep it stable.
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	for( i=0; i<MAX_POCS_IN_NETWORK; i++ )
	{
		if( poc_preserves.poc[ i ].poc_gid == ppoc_gid )
		{
			rv = &poc_preserves.poc[ i ];

			*pindex_ptr = i;

			break ;
		}
	}

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BY_POC_RECORD* POC_PRESERVES_get_poc_preserve_ptr_for_these_details( const UNS_32 pbox_index, const UNS_32 ppoc_file_type, const UNS_32 pdecoder_serial_num )
{
	BY_POC_RECORD	*rv;

	UNS_32	i;

	// ----------

	rv = NULL;

	// ----------

	// 4/5/2012 rmd : We are running through the array. Keep it stable.
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	for( i=0; i<MAX_POCS_IN_NETWORK; i++ )
	{
		// 1/25/2018 Ryan : Box index and terminal type must match.
		if( poc_preserves.poc[ i ].box_index == pbox_index )
		{
			if( poc_preserves.poc[ i ].poc_type__file_type == ppoc_file_type )
			{
				// 8/8/2016 rmd : If it is a terminal or bypass we've got a match. Don't need to check
				// serial number for these cases. Because we can only have one of each of these at a given
				// box_index.
				if( (poc_preserves.poc[ i ].poc_type__file_type == POC__FILE_TYPE__TERMINAL) || (poc_preserves.poc[ i ].poc_type__file_type == POC__FILE_TYPE__DECODER_BYPASS) )
				{
					rv = &poc_preserves.poc[ i ];
				}
                else
                {
					// 1/26/2018 Ryan : this is for the case of the SINGLE decoder based POC, of which we can
					// several at a box. Therefore we have to test the decoder SN to make sure we get the one we
					// want.
					if( poc_preserves.poc[ i ].ws[ 0 ].decoder_serial_number == pdecoder_serial_num )
                    {
						rv = &poc_preserves.poc[ i ];
					}
				}
			}
		}

		// 8/8/2016 rmd : If we found one git.
		if( rv )
		{
			break;
		}
	}

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern BY_POC_RECORD* POC_PRESERVES_get_poc_preserve_for_the_sync_items( POC_PRESERVES_SYNCD_ITEMS_STRUCT const *const psync )
{
	BY_POC_RECORD	*rv;

	UNS_32			i;
	
	UNS_32			level;
	
	POC_PRESERVES_SYNCD_ITEMS_STRUCT	lsync;
	
	// ----------

	rv = NULL;
	
	// ----------
	
	// 4/5/2012 rmd : We are running through the array. Keep it stable.
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	for( i=0; i<MAX_POCS_IN_NETWORK; i++ )
	{
		memset( &lsync, 0x00, sizeof(POC_PRESERVES_SYNCD_ITEMS_STRUCT) );
		
		// 6/6/2016 rmd : The 'show' variable is not saved into the preserves. We KNOW the setting
		// of this variable in the psync is (true) by definition. So set our local copy to match so
		// the comparision can be made.
		lsync.show_this_poc = (true);

		lsync.poc_gid = poc_preserves.poc[ i ].poc_gid;
		
		lsync.box_index = poc_preserves.poc[ i ].box_index;
		
		lsync.poc_type__file_type = poc_preserves.poc[ i ].poc_type__file_type;
		
		lsync.usage = poc_preserves.poc[ i ].usage;
		
		for( level=0; level<POC_BYPASS_LEVELS_MAX; level++ )
		{
			lsync.decoder_serial_number[ level ] = poc_preserves.poc[ i ].ws[ level ].decoder_serial_number;

			lsync.master_valve_type[ level ] = poc_preserves.poc[ i ].ws[ level ].master_valve_type;
		}
		
		lsync.this_pocs_system_preserves_ptr = poc_preserves.poc[ i ].this_pocs_system_preserves_ptr;

		// ----------
		
		if( memcmp( &lsync, psync, sizeof(POC_PRESERVES_SYNCD_ITEMS_STRUCT) ) == 0 )
		{
			rv = &poc_preserves.poc[ i ];
			
			break;
		}
		
	}

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern POC_DECODER_OR_TERMINAL_WORKING_STRUCT* POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters( UNS_32 const pbox_index, UNS_32 const pdecoder_ser_num, BY_POC_RECORD **pbpr_ptr, UNS_32 *this_decoders_level )
{
	// 5/27/2014 rmd : This function is used in a sense on the irri side. Because we are working
	// with POC's as they have been delivered from the tpmicro. We really don't know if they
	// belong to a bypass or not. Nor do we care. If the serial number is non-zero then it must
	// be decoder based.
	
	// ----------
	
	UNS_32	rrr, lll;

	BY_POC_RECORD	*lbpr;

	POC_DECODER_OR_TERMINAL_WORKING_STRUCT	*rv;

	rv = NULL;
	
	*pbpr_ptr = NULL;
	
	// ----------
	
	// 4/5/2012 rmd : We are running through the array. Keep it stable.
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	for( rrr=0; rrr<MAX_POCS_IN_NETWORK; rrr++ )
	{
		lbpr = &(poc_preserves.poc[ rrr ]);

		// 8/3/2012 rmd : Is the poc preserves record in use? And does it match our box index?
		if( (lbpr->poc_gid != 0) && (lbpr->box_index == pbox_index) )
		{
			if( pdecoder_ser_num )
			{
				// 6/4/2014 rmd : So the caller has included a non-zero serial number. Look through the
				// levels for a match. We don't need to concern ourselves with the poc type and the number
				// of valid levels. The poc preserves should reflect the file and that should be clean.
				// Meaning if a level is not in use the serial number should be 0.
				for( lll=0; lll<POC_BYPASS_LEVELS_MAX; lll++ )
				{
					if( lbpr->ws[ lll ].decoder_serial_number == pdecoder_ser_num )
					{
						// 5/27/2014 rmd : Bingo. Then we've found it.
						rv = &(lbpr->ws[ lll ]);
						
						// 7/10/2014 rmd : And provide the pointer to the poc_preserves record and set the decoders
						// level for the caller.
						*pbpr_ptr = lbpr;
						
						*this_decoders_level = lll;
						
						break;
					}
				}
			}
			else
			{
				// 6/4/2014 rmd : Then is it a terminal type with a serial number of 0?
				if( (lbpr->poc_type__file_type == POC__FILE_TYPE__TERMINAL) && !lbpr->ws[ 0 ].decoder_serial_number )
				{
					// 5/27/2014 rmd : If so then we've found it. By definition the TERMINAL poc always works
					// with index 0 of the working structure.
					rv = &(lbpr->ws[ 0 ]);

					// 7/10/2014 rmd : And provide the pointer to the poc_preserves record.
					*pbpr_ptr = lbpr;

					*this_decoders_level = 0;
				}
			}
		}
		
		// ----------
		
		// 5/27/2014 rmd : If we've found it (rv != NULL) quit.
		if( rv )
		{
			break;	
		}
	}

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This function is called either due to an edit of the poc user settings or
    the system that owns the poc. Using the poc GID (assumed to be valid) use the GET
    functions to obtain the settings needed.

    @CALLER MUTEX REQUIREMENTS The caller should be holding the poc_preserves mutex. As we
    are changing the poc_preserves variables.

	@MEMORY_RESPONSIBILITES

    @EXECUTED Called when an initial poc record registration is required or if a re-sync is
    required due to use variables being edited. This can happen upon code startup within the
    context of the STARTUP task. And after certain user variables have changed. The edit of
    those variables sets the flag in the poc_preserves record that causes this function to
    be called. In this case the re-sync occurs happens within the context of the TD_CHECK
    task.

	@RETURN (none)

	@ORIGINAL 2012.07.18 rmd

	@REVISIONS
*/
static void _nm_sync_users_settings_to_this_poc_preserve( BY_POC_RECORD	*pbpr_ptr, POC_PRESERVES_SYNCD_ITEMS_STRUCT const *const psync )
{
	UNS_32	iii;
	
	// ----------

	// 6/12/2014 rmd : Capture the gid and OCCUPY the record!
	pbpr_ptr->poc_gid = psync->poc_gid;
	
	pbpr_ptr->box_index = psync->box_index;

	pbpr_ptr->poc_type__file_type = psync->poc_type__file_type;

	pbpr_ptr->usage = psync->usage;

	for( iii=0; iii<POC_BYPASS_LEVELS_MAX; iii++ )
	{
		pbpr_ptr->ws[ iii ].decoder_serial_number = psync->decoder_serial_number[ iii ];

		pbpr_ptr->ws[ iii ].master_valve_type = psync->master_valve_type[ iii ];
	}

	// ----------
	
	// 7/17/2012 rmd : Set the system preserves ptr for the system this poc belongs to.
	pbpr_ptr->this_pocs_system_preserves_ptr = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( POC_get_GID_irrigation_system_using_POC_gid( psync->poc_gid ) );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION The assumption is that the caller has previously checked and there is no
    poc preserve record for the passed poc_gid. The function scrolls through the array of
    poc_preserves looking for an unassigned entry to take. If one cannot found (WHICH
    SHOULD NEVER HAPPEN) we return a NULL. And make an alert line about that occurrance. We
    also will first check t make sure a record hasn't already been assigned. As that would
    be unexpected by design of this functions use.

	@CALLER MUTEX REQUIREMENTS The caller should be holding the poc_preserves MUTEX.

    @EXECUTED Executed within the context of the TD_CHECK task. And COMM_MNGR task.

    @RETURN A pointer to the poc_preserves array entry if one can be obtained. Otherwise a
    NULL is returned.

	@AUTHOR 2012.01.20 rmd
*/
static BY_POC_RECORD* nm_POC_register_poc_preserve_for_these_parameters( POC_PRESERVES_SYNCD_ITEMS_STRUCT const *const psync )
{
	BY_POC_RECORD	*rv;

	UNS_32	i;

	rv = NULL;

	for( i=0; i<MAX_POCS_IN_NETWORK; i++ )
	{
		if( poc_preserves.poc[ i ].poc_gid == 0 )
		{
			// Because we are assigning this record for use clean it up. Even though should already be
			// clean - but to be sure clean the ENTIRE record.
			nm_poc_preserves_complete_record_initialization( &(poc_preserves.poc[ i ]) );

			// ---------------

			_nm_sync_users_settings_to_this_poc_preserve( &(poc_preserves.poc[ i ]), psync );

			// ---------------

			// 8/2/2012 rmd : Assign the rip record start time and date as well as set the poc gid.
			DATE_TIME	ldt;

			EPSON_obtain_latest_time_and_date( &ldt );

			poc_preserves.poc[ i ].rip.start_date = ldt.D;

			poc_preserves.poc[ i ].rip.start_time = ldt.T;

			poc_preserves.poc[ i ].rip.poc_gid = psync->poc_gid;

			// ---------------

			rv = &(poc_preserves.poc[ i ]);

			break ;
		}
	}

	// If still no answer I want to hear about this.
	if( rv == NULL )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "POC Preserves : no open records" );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This is the poc_preserves housekeeping function. Will result in a
    poc_preserves array that is sync'd with the pocs declared within the poc file. This
    function is called once per second. The other approach would be to explicitly call this
    when needed (program start, changes to the poc file). I settled on once per second.

    The criteria for a match is the poc gid, the controller serial number, the type of poc,
    and the decoder serial number. Before, we only used the poc gid from the group list as
    the primary indicator of a match but this doesn't reflect enough about the POC. A fault
    with this was seen during manual editing of a POC paramter. If we changed say the type
    of poc the poc preserves would not re-sync that record because the gid had not
    changed.

	2012.01.27 I think I'm only going to call this once per second. Else I get into the
    position of calling it once per second and for every incoming token that is carrying poc
    information. I dunno, that seems like it could be  alot. Hmmm. Well we'll see.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the TD_CHECK task. And COMM_MNGR task.

    @RETURN (none)

	@AUTHOR 2012.01.20 rmd
*/
extern void POC_PRESERVES_synchronize_preserves_to_file( void )
{
	// 4/11/2012 rmd : If we get an error I am going to clean all the poc preserves. Even though
	// a corrupt poc_preserves may not be the actual cause of the error. But since there is an
	// integrity problem somewhere let's clean some house.
	BOOL_32	lerror;

	// For each poc make sure there is a poc_preserve record. Also making sure there aren't any
	// so called 'left-over' poc preserve records that no longer have a matching poc gid in the
	// poc file. This will be the case when a poc is deleted from the network.
	UNS_32	p;
	
	UNS_32	total_pocs;

	void	*group_ptr;

	BY_POC_RECORD	*bpr_ptr;

	POC_PRESERVES_SYNCD_ITEMS_STRUCT	sync;
	
	// ----------
	
	lerror = (false);
	
	// ----------
	
	// 2012.01.17 rmd : Yes we are about to peruse through the poc_preserves. And they can also
	// be changed by the COMM_MNGR task. So protect against potential clash.
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	// 2012.02.16 ajv : Since we're accessing the POC list and are expecting the
	// contents of the list to remain static during this process, take the
	// list_poc_recursive_MUTEX to prevent anyone else from modifying the list.
	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	// --------------------------------

	for( p=0; p<MAX_POCS_IN_NETWORK; p++ )
	{
		poc_preserves.poc[ p ].accounted_for = (false);
	}

	// --------------------------------

	total_pocs = POC_get_list_count();

	for( p=0; p<total_pocs; p++ )
	{
		group_ptr = POC_get_group_at_this_index( p );

		if( group_ptr == NULL )
		{
			// 4/11/2012 rmd : This should of course never happen.
			ALERT_MESSAGE_WITH_FILE_NAME( "POC_PRESERVES : unexp NULL ptr" );

			lerror = (true);

			break;
		}

		// ----------
		
		POC_load_fields_syncd_to_the_poc_preserves( group_ptr, &sync );
		
		// 6/6/2016 rmd : If the POC is available to be 'shown', and the POC is in use, it is
		// available to be sync'd to the preserves.
		if( sync.show_this_poc && (sync.usage != POC_USAGE_NOT_USED) )
		{
			// 4/11/2012 rmd : See if we can find a poc preserves that matches.
			bpr_ptr = POC_PRESERVES_get_poc_preserve_for_the_sync_items( &sync );
	
			// 6/12/2014 rmd : If it is in the preserves then keep it in the preserves. When working
			// with a newly created POC from the file the preserves pointer will be NULL.
			if( bpr_ptr != NULL )
			{
				// And mark the record as checked. So we don't think it is an obsolete record.
				bpr_ptr->accounted_for = (true);
			}
		}

	}  // for each poc in the list

	// ----------
	
	if( lerror == (false) )
	{
		// 4/11/2012 rmd : NOW test for any poc_preserve records that have their accounted for flag
		// set to (false). Those should be initialized. To avoid unessarily initializing a multitude
		// of records each second when this function is run also check if the gid is non-zero to see
		// if we should bother to initialize. (We are cleaning left over obsolete records from pocs
		// that once existed or pocs that were in use and are now not.)
		for( p=0; p<MAX_POCS_IN_NETWORK; p++ )
		{
			// 6/12/2014 rmd : The GID is all we need to test. The other fields can be zero when in use.
			// The GID is not supposed to ever be 0 for a record in use.
			if( poc_preserves.poc[ p ].poc_gid != 0 )
			{
				if( !poc_preserves.poc[ p ].accounted_for )
				{
					// Well this is what we call housekeeping. When one deletes a POC I would expect to see this
					// message. As we 'recover' the once used poc_preserves record. Probably comment this line
					// out once proven to work if it is a nuisance. But gee. Should rarely see.
					Alert_poc_preserves_activity( PRESERVES_CLEANING_OBSOLETE );

					// Clean it up. Completely. Probably only have to set the gid = to 0 but let's keep the
					// records put out of use clean.
					nm_poc_preserves_complete_record_initialization( &(poc_preserves.poc[ p ]) );
				}
			}
		}
	}  // of if no error

	// ----------
	
	if( lerror == (false) )
	{
		// OKAY so now we have a poc_preserves array that MAY not contain ALL the pocs. If a NEW poc
		// was created the first time after that we run through this function it will not be
		// accounted for at this point. So pass back through the list. This time if there is no
		// poc_preserve for a poc from the list go make the assignment.
		total_pocs = POC_get_list_count();

		for( p=0; p<total_pocs; p++ )
		{
			group_ptr = POC_get_group_at_this_index( p );
	
			if( group_ptr == NULL )
			{
				// 4/11/2012 rmd : This should of course never happen.
				ALERT_MESSAGE_WITH_FILE_NAME( "POC_PRESERVES : unexp NULL ptr" );
	
				lerror = (true);
	
				break;
			}
	
			// ----------
			
			// 4/11/2012 rmd : See if we can find a poc preserves that matches.
			POC_load_fields_syncd_to_the_poc_preserves( group_ptr, &sync );
			
			// 6/6/2016 rmd : If the POC is avaialbe to be 'shown' and the POC is in use it is available
			// to be sync'd to the preserves.
			if( sync.show_this_poc && (sync.usage != POC_USAGE_NOT_USED) )
			{
				bpr_ptr = POC_PRESERVES_get_poc_preserve_for_the_sync_items( &sync );
	
				// 6/12/2014 rmd : It could be NULL if this is a NEW poc! That's what the sync is all about.
				if( bpr_ptr == NULL )
				{
					// 8/21/2015 rmd : Show us what is happening.
					Alert_poc_preserves_activity( PRESERVES_REGISTERING_NEW );
					
					// Go establish a new connection.
					bpr_ptr = nm_POC_register_poc_preserve_for_these_parameters( &sync );
	
					// 4/11/2012 rmd : If there is no record assigned, there is already an alert message created
					// in the registration function.
				}
				else
				if( poc_preserves.perform_a_full_resync )
				{
					// 8/21/2015 rmd : Show us what is happening.
					//
					// 6/3/2016 rmd : Commented out this alert. Became too much. At start up we repeatedly set
					// the perform_a_full_sync flag (I believe tied to what's installed) and we see this line
					// for each POC. So on a chain with 2 poc's we may see 6 instances of this line.
					//Alert_poc_preserves_activity( PRESERVES_REFRESHING_EXISTING );
					
					// 7/1/2014 rmd : FULL resync requested so resync each preserve entry to all settings
					// extracted from the POC file.
					_nm_sync_users_settings_to_this_poc_preserve( bpr_ptr, &sync );
				}
			}
			
		}  // of for each poc

	}  // of if no error

	// ----------
	
	// 7/1/2014 rmd : Clear the resync flag. We've done it.
	poc_preserves.perform_a_full_resync = (false);

	// ----------

	if( lerror )
	{
		for( p=0; p<MAX_POCS_IN_NETWORK; p++ )
		{
			// 4/11/2012 rmd : In case of error clean. This will probably trigger other errors. Such as
			// a TP Micro reporting flow to us but now where to put the data. So is clearing here a good
			// idea? Not sure. Time will tell.
			nm_poc_preserves_complete_record_initialization( &(poc_preserves.poc[ p ]) );
		}
	}

	// ----------
	
	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION As part of edits to particular poc variables this function should be
    called. And triggers a re-sync for each of the pocs. In other words a sync is forced for
    each of the pocs.

    @CALLER MUTEX REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES

    @EXECUTED Designed to be called from the SET functions. And also when the system a poc
    belongs to changes. This can happen within the context of the KEY_PROCESS task. And I
    believe within the COMM_MNGR task context.

	@RETURN (none)

	@ORIGINAL 2012.07.18 rmd

	@REVISIONS
*/
extern void POC_PRESERVES_request_a_resync( void )
{
	// 7/1/2014 rmd : Nothing more to it than setting this variable true.
	poc_preserves.perform_a_full_resync = (true);
}
	
/* ---------------------------------------------------------- */
extern void POC_PRESERVES_set_master_valve_energized_bit( POC_DECODER_OR_TERMINAL_WORKING_STRUCT *pws, UNS_32 const paction )
{
	// 7/14/2014 rmd : Can be called by ANY task. To set the MV bits. These settings may get
	// over-written prior to sending the requested state to the TPMICRO. For example a MLB will
	// at the last minute take precedence over prior calls of this function.

	// ----------
	
	// 7/10/2014 rmd : Take MUTEX so the structure remain stable during our use.
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	if( paction == MASTER_VALVE_ACTION_CLOSE )
	{
		// 7/11/2014 rmd : If the valve is NC deenergize it; otherwise energize.
		if( pws->master_valve_type == POC_MV_TYPE_NORMALLY_CLOSED )
		{
			pws->pbf.master_valve_energized_irri = (false);
		}
		else
		{
			pws->pbf.master_valve_energized_irri = (true);
		}
	}
	else
	if( paction == MASTER_VALVE_ACTION_OPEN )
	{
		// 7/11/2014 rmd : If the valve is NC energize it; otherwise deenergize.
		if( pws->master_valve_type == POC_MV_TYPE_NORMALLY_CLOSED )
		{
			pws->pbf.master_valve_energized_irri = (true);
		}
		else
		{
			pws->pbf.master_valve_energized_irri = (false);
		}
	}
	else
	{
		// 7/14/2014 rmd : The default will catch for the case of DEENERGIZE as well as misuse of
		// the function parameters. In any case deenergize the valve.
		pws->pbf.master_valve_energized_irri = (false);
	}

	// ----------

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/6/2015 rmd : Do not exceed (BB_PRE_STRING_LENGTH - 1) visible characters. If you do you
// can end up changing the version number with no recognition of the change. Follow the code
// through to understand this.
static const char	CM_PRE_TEST_STRING[] = "Chain v01";

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void load_this_box_configuration_with_our_own_info( BOX_CONFIGURATION_STRUCT *pbox_ptr )
{
	// 4/24/2014 rmd : ONLY to be used when building a token RESPONSE. That's all it seems
	// appropriate for.
	
	// ----------

	pbox_ptr->serial_number = config_c.serial_number;

	memcpy( &pbox_ptr->purchased_options, &config_c.purchased_options, sizeof(PURCHASED_OPTIONS_STRUCT) );

	// ----------
	
	// 2/6/2015 rmd : Get the whats installed from the temporary holding. Where the wi is held
	// after receipt from the tpmicro. Using the tpmicro_data MUTEX to protect this variable
	// during writes and copies of it.
	xSemaphoreTakeRecursive( tpmicro_data_recursive_MUTEX, portMAX_DELAY );

	memcpy( &pbox_ptr->wi, &tpmicro_comm.wi_holding, sizeof(WHATS_INSTALLED_STRUCT) );

	xSemaphoreGiveRecursive( tpmicro_data_recursive_MUTEX );

	// ----------
	
	pbox_ptr->port_A_device_index = config_c.port_A_device_index;

	pbox_ptr->port_B_device_index = config_c.port_B_device_index;
}
	
/* ---------------------------------------------------------- */
extern void init_battery_backed_chain_members( const BOOL_32 pforce_the_initialization )
{
	UNS_32	iii;
	
	// 11/16/2012 rmd : Local variable to see if our string failed.
	BOOL_32	lmy_strings_failed;

	// ----------
	
	xSemaphoreTakeRecursive( chain_members_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	lmy_strings_failed = (false);

	// 8/7/2015 rmd : If the string isn't intact signal we've failed.
	if( strncmp( chain.verify_string_pre, CM_PRE_TEST_STRING, sizeof(CM_PRE_TEST_STRING) ) != 0 )
	{
		lmy_strings_failed = (true);
	}
	
	// ----------

	if( pforce_the_initialization || lmy_strings_failed )
	{
		// ----------------------

		Alert_battery_backed_var_initialized( "Chain Members", pforce_the_initialization );
		
		// ------------

		// 10/1/2012 rmd : This will set the stage such that no matter what shows up via the
		// communications it will be flagged as a 'new' chain. This sets all controllers as
		// inactive. Nulls out all the serial numbers. And more.
		memset( &chain, 0x00, sizeof(CHAIN_MEMBERS_STRUCT) );
		
		// ------------

		strlcpy( chain.verify_string_pre, CM_PRE_TEST_STRING, BB_PRE_STRING_LENGTH );
	}
	else
	{
		#if ALERT_SHOW_ROUTINE_INITIALIZATIONS_DURING_STARTUP
			Alert_battery_backed_var_valid( "Chain Members" );
		#endif

		// ----------
		
		// 10/28/2014 rmd : And keep the unused expansion portion of the structure set to zeros. So
		// that in the future as one goes to use it [meaning they made some variables and reduced
		// the 'expansion' size accordingly] the new variables will start life as zero.
		for( iii=0; iii<MAX_CHAIN_LENGTH; iii++ )
		{
			memset( &chain.members[ iii ].expansion, 0, sizeof(chain.members[ iii ].expansion) );
		}
	}
	
	// ----------
	
	xSemaphoreGiveRecursive( chain_members_recursive_MUTEX );
}


/* ---------------------------------------------------------- */
extern BOOL_32 controller_is_2_wire_only( const UNS_32 pbox_index_0 )
{
	// 9/12/2016 rmd : A sanity range check. During startup the alerts initialization runs
	// through all of the alerts developing display ptrs. I have seen a trashed pile result in
	// an unbootable condition where the box_index passed in was trashed (a giant number) and
	// caused a DATA ABORT error here. Unit was not bootable. Using the debugger I tracked it
	// down to the index into the chain_members array. It was a very large 32-bit value. Which I
	// suppose creates an out of address space address for the code.
	if( pbox_index_0 < MAX_CHAIN_LENGTH )
	{
		return( (chain.members[ pbox_index_0 ].box_configuration.wi.two_wire_terminal_present) &&
				!(( chain.members[ pbox_index_0 ].box_configuration.wi.stations[ 0 ].card_present ) && ( chain.members[ pbox_index_0 ].box_configuration.wi.stations[ 0 ].tb_present )) &&
				!(( chain.members[ pbox_index_0 ].box_configuration.wi.stations[ 1 ].card_present ) && ( chain.members[ pbox_index_0 ].box_configuration.wi.stations[ 1 ].tb_present )) &&
				!(( chain.members[ pbox_index_0 ].box_configuration.wi.stations[ 2 ].card_present ) && ( chain.members[ pbox_index_0 ].box_configuration.wi.stations[ 2 ].tb_present )) &&
				!(( chain.members[ pbox_index_0 ].box_configuration.wi.stations[ 3 ].card_present ) && ( chain.members[ pbox_index_0 ].box_configuration.wi.stations[ 3 ].tb_present )) &&
				!(( chain.members[ pbox_index_0 ].box_configuration.wi.stations[ 4 ].card_present ) && ( chain.members[ pbox_index_0 ].box_configuration.wi.stations[ 4 ].tb_present )) &&
				!(( chain.members[ pbox_index_0 ].box_configuration.wi.stations[ 5 ].card_present ) && ( chain.members[ pbox_index_0 ].box_configuration.wi.stations[ 5 ].tb_present )) );
	}
	else
	{
		return( (false) );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/6/2015 rmd : Do not exceed (BB_PRE_STRING_LENGTH - 1) visible characters. If you do you
// can end up changing the version number with no recognition of the change. Follow the code
// through to understand this.
static const char	GU_PRE_TEST_STRING[] = "general v01";

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void init_battery_backed_general_use( BOOL_32 const pforce_the_initialization )
{
	// 11/24/2014 rmd : Invoked within the context of the app_startup task. At each program
	// start.
	
	// ----------

	// 8/7/2015 rmd : Initialize if the caller request or the string isn't intact.
	if( pforce_the_initialization || (strncmp( battery_backed_general_use.verify_string_pre, GU_PRE_TEST_STRING, sizeof(GU_PRE_TEST_STRING) ) != 0) )
	{
		Alert_battery_backed_var_initialized( "General Use", pforce_the_initialization );
		
		// ----------

		// 11/11/2014 rmd : Zero the whole foal_irri structure. Which of course sets all booleans to
		// (false).
		memset( &battery_backed_general_use, 0x00, sizeof(GENERAL_USE_BB_STRUCT) );
		
		// ----------
		
		strlcpy( battery_backed_general_use.verify_string_pre, GU_PRE_TEST_STRING, BB_PRE_STRING_LENGTH );
	}
	else
	{
		#if ALERT_SHOW_ROUTINE_INITIALIZATIONS_DURING_STARTUP
			Alert_battery_backed_var_valid( "General Use" );
		#endif

		// ----------
		
		// 11/24/2014 rmd : Nothing to do if structure is intact. That is the point of the variables
		// being battery backed. To preserve their state upon a restart.
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Intitializes the part of the lights record that is always intialized on each
    program startup. Which in itself probably indicates the variables in this portion do not
    need to be battery backed. But remember are kept here for convenience. When a record is
    first picked out and assigned to a LIGHTS we also call this function. That happens at
    various yet TBD spots in the code.

    @CALLER MUTEX REQUIREMENTS The caller should be holding the lights_preserves mutex. As we
    are manuipulating multiple parameters of the lights preserves record.

    @EXECUTED Executed within the context of the STARTUP task. The TD_CHECK task. And quite
    possibly the COMM_MNGR task.

	@RETURN (none)

	@AUTHOR 2012.01.00 rmd
*/
static void nm_lights_preserves_init_portion_initialized_on_each_program_start( UNS_32 const box_index, const UNS_32 output_index )
{
	UNS_32 array_index = LIGHTS_get_lights_array_index( box_index, output_index );
	
	// ----------
	
	// The Master will figure out what lights are available and which ones should be turned back on after a restart. 
	lights_preserves.lbf[ array_index ].light_is_available = ( false );
	lights_preserves.lbf[ array_index ].light_is_energized = ( false );
	lights_preserves.lbf[ array_index ].reason = ( false );

	// ----------

	// We have been restarted. The TPMicro needs to give us the latest values for these.
	lights_preserves.lights[ array_index ].last_measured_current_ma = 0;
	lights_preserves.lbf[ array_index ].no_current_output = ( false );
	lights_preserves.lbf[ array_index ].shorted_output = ( false );
	lights_preserves.lights[ array_index ].expansion_bytes = 0;

}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Resets the dynamic variables that manage lighting. Called on a normal
    program startup. And also when the chain falls into forced. Or comes out of forced. Both
    under control of the COMM_MNGR. By definition because these variables are reset on
    program startup they do not need to be battery backed but are for convience (attached to
    the system).

    @CALLER MUTEX REQUIREMENTS The caller should be holding the system_preserves mutex. As
    we are changing many of the system variables.

	@MEMORY_RESPONSIBILITES

    @EXECUTED Executed within the context of the STARTUP task. And the COMM_MNGR task. See
    description above.

	@RETURN (none)

	@ORIGINAL 2012.05.04 rmd

	@REVISIONS
*/
extern void nm_LIGHTS_PRESERVES_restart_lights( BY_LIGHTS_RECORD *pblr_ptr )
{
	// 5/7/2014 rmd : Note this function is executed from various tasks. Primarily the
	// comm_mngr, td_check, and the startup task. The startup task on reboot. While scanning 
	// this function is also called via td_check task as the NETWORK is deemed unavailable. 
	
	// 9/16/2015 mpd : These values are battery backed for a reason. Most should not be cleared on a restart.
	pblr_ptr->last_measured_current_ma = 0;
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Intitializes the part of the lights record that is ONLY intialized when the
    desire is to COMPLETELY restart the record. Which will happen when we assign a LIGHTS to a
    record and when the battery backed SRAM test strings fail for this structure.

    @CALLER MUTEX REQUIREMENTS The caller should be holding the lights_preserves mutex. As we
    are manuipulating multiple parameters of the lights preserves record.

    @EXECUTED Executed within the context of the STARTUP task. The TD_CHECK task. And quite
    possibly the COMM_MNGR task.

	@RETURN (none)

	@AUTHOR 2012.01.00 rmd
*/
extern void nm_LIGHTS_PRESERVES_complete_record_initialization( BY_LIGHTS_RECORD *pblr_ptr )
{
	UNS_32 array_index;
	UNS_32 box_index;
	UNS_32 output_index;
	DATE_TIME ldt;

	EPSON_obtain_latest_time_and_date( &ldt );

	// Fill the whole record with zeros. 
	memset( pblr_ptr, 0x00, sizeof(BY_LIGHTS_RECORD) );

	// A few fields need to be initialized to non-zero values.
	for( box_index = 0; box_index < MAX_CHAIN_LENGTH; ++box_index )
	{
		for( output_index = 0; output_index < MAX_LIGHTS_IN_A_BOX; ++output_index )
		{
			array_index = LIGHTS_get_lights_array_index( box_index, output_index );
			lights_preserves.lights[ array_index ].lrr.record_start_date = ldt.D;
			lights_preserves.lights[ array_index ].lrr.box_index_0 = box_index;
			lights_preserves.lights[ array_index ].lrr.output_index_0 = output_index;
		}
	}

}

// 8/6/2015 rmd : Do not exceed (BB_PRE_STRING_LENGTH - 1) visible characters. If you do you
// can end up changing the version number with no recognition of the change. Follow the code
// through to understand this.
static const char	LIGHTS_PRE_TEST_STRING[] = "lights v03";

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called on startup prior to most but not all tasks running to determine
    SYSTEM PRESERVES validity.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the STARTUP task.

	@RETURN (none)

	@AUTHOR 2011.11.08 rmd
*/
extern void init_battery_backed_lights_preserves( const BOOL_32 pforce_the_initialization )
{
	INT_32	i, j;

	xSemaphoreTakeRecursive( lights_preserves_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	// 11/16/2012 rmd : We use a local variable to see if our strings failed.
	BOOL_32	lmy_strings_failed;
	
	lmy_strings_failed = (false);

	// 11/16/2012 rmd : If the strings aren't intact signal we've failed. This will trigger the
	// startup task to initialize ALL battery backed structures (except alerts).
	if( strncmp( lights_preserves.verify_string_pre, LIGHTS_PRE_TEST_STRING, sizeof(LIGHTS_PRE_TEST_STRING) ) != 0 )
	{
		lmy_strings_failed = (true);
	}
	
	// ----------

	if( pforce_the_initialization || lmy_strings_failed )
	{
		Alert_battery_backed_var_initialized( "LIGHTS Preserves", pforce_the_initialization );

		for( i = 0; i < MAX_LIGHTS_IN_NETWORK; ++i )
		{
			nm_LIGHTS_PRESERVES_complete_record_initialization( &( lights_preserves.lights[ i ] ) );
		}

		strlcpy( lights_preserves.verify_string_pre, LIGHTS_PRE_TEST_STRING, BB_PRE_STRING_LENGTH );
	}
	else
	{
		#if ALERT_SHOW_ROUTINE_INITIALIZATIONS_DURING_STARTUP
			Alert_battery_backed_var_valid( "LIGHTS Preserves" );
		#endif
	}

	// ----------
	
	// Now for the items we ALWAYS set to their defaults. NOTE: mostly these are the items that
	// DO NOT need to be battery backed. (But for association ease I have added them to the lights
	// battery backed structure.)
	for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
	{
		for( j = 0; j < MAX_LIGHTS_IN_A_BOX; ++j )
		{
			nm_lights_preserves_init_portion_initialized_on_each_program_start( i, j );
		}
	}
	
	// ----------
	
	xSemaphoreGiveRecursive( lights_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This function gets the index into the light preserves array.

	@CALLER MUTEX REQUIREMENTS none

    @PARAM box_index The 0 - 11 box index of the target light.
 
    @PARAM output_index The 0 - 3 output index of the target light.
 
    @RETURN UNS_32 The 0 - 47 index of the target light's structure in the preserves array.
 
	@AUTHOR 7/27/2015 mpd
*/
/* ---------------------------------------------------------- */
extern UNS_32 obsolete_LIGHTS_PRESERVES_get_preserves_index( const UNS_32 box_index, const UNS_32 output_index )
{ 
	return( ( box_index * MAX_LIGHTS_IN_A_BOX ) + output_index );

}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This function sets or clears specific bits in the lights preserves bit field.

	@CALLER MUTEX REQUIREMENTS This function handles the lights_preserves MUTEX.

    @PARAM box_index The 0 - 11 box index of the target light.
 
    @PARAM output_index The 0 - 3 output index of the target light.
 
    @PARAM bit The bit position in the bit field to be modified.
 
    @PARAM set_bit What to do with the bit. A "true" sets the bit. A "false" resets the bit.
 
    @RETURN void
 
	@AUTHOR 7/27/2015 mpd
*/
/* ---------------------------------------------------------- */
extern void LIGHTS_PRESERVES_set_bit_field_bit( const UNS_32 box_index, const UNS_32 output_index, const UNS_32 bit, const BOOL_32 set_bit )
{
	UNS_32 bb_index;

	if( ( bit <= LIGHTS_BITFIELD_BOOL_MAX_BIT ) && ( box_index < MAX_CHAIN_LENGTH ) && ( output_index < MAX_LIGHTS_IN_A_BOX ) )
	{
		bb_index = LIGHTS_get_lights_array_index( box_index, output_index );

		// 7/27/2015 mpd : Take the MUTEX.
		xSemaphoreTakeRecursive( lights_preserves_recursive_MUTEX, portMAX_DELAY );
		
		// ----------

		switch( bit )
		{
			case LIGHTS_BITFIELD_BOOL_light_is_available_bit:
				set_bit ? ( lights_preserves.lbf[ bb_index ].light_is_available = ( true ) ) : ( lights_preserves.lbf[ bb_index ].light_is_available = ( false ) );
				break;

			case LIGHTS_BITFIELD_BOOL_light_energized_irri_bit:
				set_bit ? ( lights_preserves.lbf[ bb_index ].light_is_energized = ( true ) ) : ( lights_preserves.lbf[ bb_index ].light_is_energized = ( false ) );
				break;

			case LIGHTS_BITFIELD_BOOL_shorted_output_bit:
				set_bit ? ( lights_preserves.lbf[ bb_index ].shorted_output = ( true ) ) : ( lights_preserves.lbf[ bb_index ].shorted_output = ( false ) );
				break;

			case LIGHTS_BITFIELD_BOOL_no_current_output_bit:
				set_bit ? ( lights_preserves.lbf[ bb_index ].no_current_output = ( true ) ) : ( lights_preserves.lbf[ bb_index ].no_current_output = ( false ) );
				break;

			default:
				// 7/27/2015 mpd : This should not happen.
				Alert_Message( "Setting invalid Lights bit" );
		}

		// ----------

		xSemaphoreGiveRecursive( lights_preserves_recursive_MUTEX );
	}

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/7/2015 rmd : NOTE - as you add new battery backed structure initialization functions be
// sure to include a call to that function (forcing initialization) as part of a FACTORY
// RESET.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

