/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_WATERSENSE_H
#define _INC_WATERSENSE_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"station_groups.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/27/2015 ajv : All runtimes (irrigation cycles) that occur during the test period must
// be greater than three minutes in duration. Water applied during the irrigation events
// totaling three minutes or less shall be excluded from the daily water balance
// calculation. - WaterSense Specification for Weather-Based Irrigation Controllers, Version
// 1.0 (November 3, 2011), Section 3.1.1.
#define	WATERSENSE_MINIMUM_RUN_MINUTES		(3)

// 7/9/2015 ajv : Our goal with WaterSense is to ensure that we always apply any water
// that's depleted via ET while allowing sufficient room for rain. To that end, we've
// decided to skip irrigation any time the moisture balance is above 50%. If this occurs, a
// user alert is stamped and a Station History flag is set.
#define	WATERSENSE_MAXIMUM_MOISTURE_BALANCE_ALLOWED_FOR_IRRI	(0.50F)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern const float Ks[ (STATION_GROUP_PLANT_TYPE_MAX + 1) ];

extern const float Kd[ (STATION_GROUP_PLANT_TYPE_MAX + 1) ][ (STATION_GROUP_DENSITY_FACTOR_HIGH + 1) ];

extern const float Kmc[ (STATION_GROUP_PLANT_TYPE_MAX + 1) ][ (STATION_GROUP_MICROCLIMATE_FACTOR_HIGH + 1) ];

extern const float ROOT_ZONE_DEPTH[ (STATION_GROUP_PLANT_TYPE_MAX + 1) ][ (STATION_GROUP_SOIL_TYPE_MAX + 1) ];

extern const float MAD[ (STATION_GROUP_SOIL_TYPE_MAX + 1) ];

extern const float AW[ (STATION_GROUP_SOIL_TYPE_MAX + 1) ];

extern const float IR[ (STATION_GROUP_SOIL_TYPE_MAX + 1) ];

extern const float ASA[ (STATION_GROUP_SOIL_TYPE_MAX + 1) ][ (STATION_GROUP_SLOPE_PERCENTAGE_MAX + 1) ];

extern const float PR[ (STATION_GROUP_HEAD_TYPE_MAX + 1) ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern UNS_32 WATERSENSE_get_Kmc_index_based_on_exposure( const UNS_32 pexposure );

extern float WATERSENSE_get_RZWWS( const float allowable_depletion, const float available_water, const float root_zone_depth );

extern float WATERSENSE_get_Kl( const float pKs, const float pKd, const float pKmc );

extern float WATERSENSE_get_cycle_time( const float allowable_surface_accum, const float soil_intake_rate, const float precip_rate );

extern float WATERSENSE_get_soak_time__cycle_end_to_cycle_start( const float allowable_surface_accum, const float soil_intake_rate, const float precip_rate, const float E );

// ----------

extern float WATERSENSE_reduce_moisture_balance_by_ETc( const float Kl, const float ETo, const float MBo );

extern float WATERSENSE_increase_moisture_balance_by_irrigation( const float Rt, const float E, const float PR, const float RZWWS, const float MBo );

extern float WATERSENSE_increase_moisture_balance_by_rain( const float R, const float RZWWS, const float usable_rain, float MBo );

// ----------

extern void WATERSENSE_verify_watersense_compliance( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

