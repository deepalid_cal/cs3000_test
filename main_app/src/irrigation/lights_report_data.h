/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_LIGHTS_REPORT_DATA_H
#define _INC_LIGHTS_REPORT_DATA_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	"lpc_types.h"

#include	"report_data.h"

#include 	"lights.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// The storage of the completed records. This will be a structure. Stored in SDRAM. It is
// about 1MByte in size! The array of records contained within. And some variables which
// help know where to append the next completed record. Once full it will just keep wrapping
// around. After each start time, meaning a groupd of new records was added, we'll save the
// file.

// 1/23/2015 rmd : If the number of records kept is changed should update revision number
// else you will lose the file contents.

#define LIGHTS_REPORT_DATA_MIN_DAYS_KEPT		( 14 )

#define LIGHTS_REPORT_DATA_MAX_LIGHTS			( MAX_CHAIN_LENGTH * MAX_LIGHTS_IN_A_BOX )

#define LIGHTS_REPORT_DATA_MAX_RECORDS			( LIGHTS_REPORT_DATA_MAX_LIGHTS * LIGHTS_REPORT_DATA_MIN_DAYS_KEPT )

// -----------------------------

typedef struct
{
	// 7/24/2015 mpd : This structure is arranged by field size to maximize efficiency. Time and date are stored 
	// individually instead of in a DATE_TIME structure for this reason.

	// 9/29/2015 ajv : This is the total PROGRAMMED on time. It does NOT include manual and
	// mobile.
	UNS_32  		programmed_seconds;

	// 9/29/2015 ajv : This is the manual and mobile seconds on.
	UNS_32			manual_seconds;			

	//	Date associated with record start. Should occur each and every day!
	UNS_16			record_start_date;			

	// 7/22/2015 mpd : This is the number of times the light went on.
	UNS_16  		number_of_on_cycles;

	// 4/18/2014 rmd : This is the 0..11 index of the box the light belongs to.
	UNS_8			box_index_0;

	// 7/22/2015 mpd : From 0 to 3 representing light output 1 through 4.
	UNS_8			output_index_0;

	// 7/17/2015 mpd : Maintain word boundaries.
	UNS_16			lrr_pad_bytes_avaiable_for_use;

	// ------------------------
	
	// This structure is 16 bytes long on 8/19/2015.

} LIGHTS_REPORT_RECORD;

// ----------

typedef struct
{
	REPORT_DATA_FILE_BASE_STRUCT	rdfb;

	LIGHTS_REPORT_RECORD			lrr[ LIGHTS_REPORT_DATA_MAX_RECORDS ];

	// This structure is 10812 bytes long on 8/19/2015.

} COMPLETED_LIGHTS_REPORT_DATA_STRUCT;

extern COMPLETED_LIGHTS_REPORT_DATA_STRUCT	lights_report_data_completed;	// Held in SDRAM and saved to a file in the GENERAL STORAGE flash device.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

// ---------------------

extern void init_file_lights_report_data( void );

extern void save_file_lights_report_data( void );

extern void LIGHTS_REPORT_DATA_start_the_ci_timer_if_it_is_not_running( void );

extern void nm_LIGHTS_REPORT_DATA_inc_index( UNS_32 *pindex_ptr );

// ---------------------

extern UNS_32 LIGHTS_REPORT_DATA_fill_ptrs_and_return_how_many_lines( const UNS_32 pbox_index_0, const UNS_32 poutout_index_0 );

extern void LIGHTS_REPORT_draw_scroll_line( const INT_16 pline_index_0_i16 );

// ---------------------

extern void nm_LIGHTS_REPORT_DATA_close_and_start_a_new_record( LIGHTS_REPORT_RECORD *psrdr_ptr, const DATE_TIME *pdate_time );

// ---------------------

extern void LIGHTS_REPORT_DATA_free_report_support( void );

#if ALERTS_ABILITY_TO_GENERATE_ALL_ALERTS_AND_REPORTS_FOR_DEBUG

extern void LIGHTS_REPORT_DATA_generate_lines_for_DEBUG( void );

#endif

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

