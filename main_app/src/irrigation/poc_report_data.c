/*   file = poc_report_data.c                08.02.2010  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memset
#include	<string.h>

#include	"poc_report_data.h"

#include	"alerts.h"

#include	"flash_storage.h"

#include	"td_check.h"

#include	"foal_irri.h"

#include	"app_startup.h"

#include	"r_poc_report.h"

#include	"cs_mem.h"

#include	"d_process.h"

#include	"epson_rx_8025sa.h"

#include	"flowsense.h"

#include	"e_poc.h"

#include	"controller_initiated.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

COMPLETED_POC_REPORT_RECORDS_STRUCT		poc_report_data_completed;	// Held in SDRAM and saved to a file in the GENERAL STORAGE flash device.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/27/2012 rmd : Support for displaying the station history or station report data. This
// is a pointer to a dynamic block of memory that is acquired when we enter the poc report
// screen and freed when we leave the respective screen. The memory holds a ptr to each of
// the records being displayed. We count on the C startup code to intialize this to a NULL.
static POC_REPORT_RECORD	*(*poc_report_ptrs)[];
 
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/2/2014 rmd : Store the timer outside the file structure. It is a dynamic memory
// allocation. And not desirable nor needed to preserve through a power failure.
static xTimerHandle		poc_report_data_ci_timer;
	
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called as part of the poc_preserves initial verification during startup.
    Also called at each roll time for the poc report data to restart the report data rip in
    the poc preserves.

    @CALLER_MUTEX_REQUIREMENTS The caller is to be holding the appropriate MUTEX. Given
    where the partiuclar record is located. Either battery backed SRAM or in the
    SDRAM.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the APP_STARTUP task. And from the TD_CHECK
    task. Daily at the station report data roll time as specified within the station report
    data file.

	@RETURN (none)

	@ORIGINAL 2012.06.04 rmd

	@REVISIONS (none)
*/
static void nm_init_poc_report_record( POC_REPORT_RECORD *pprr_ptr )
{
	// 7/30/2012 rmd : All that is needed is to flood the record with zeros. Identification of
	// completed records is tracked by structure management variables.
	memset( pprr_ptr, 0, sizeof(POC_REPORT_RECORD) );
}

/* ---------------------------------------------------------- */
/*
Function to be called ONLY when the file is first created.
*/ 
static void nm_init_poc_report_records( void )
{
	UNS_32	i;
	
	// 1/23/2015 rmd : This sets all the control variables to 0 or (false). And the roll time to
	// midnight. Which is where we want it.
	memset( &poc_report_data_completed.rdfb, 0x00, sizeof(REPORT_DATA_FILE_BASE_STRUCT) );

	// ----------
	
	for( i=0; i<(POC_REPORT_RECORDS_TO_KEEP); i++ )
	{
		nm_init_poc_report_record( &(poc_report_data_completed.prr[ i ]) );
	}
	
	// ----------
	
	// 7/19/2012 rmd : Regarding the default ROLL-TIME of 0 (mid-night). I actually believe the
	// default of NOON is better when it comes to POC budgets. As meter read times are during
	// the day and therefore NOON would better represent billing periods.
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// filenames ARE ALLOWED 32 CHARS ... truncated if exceeds this limit

const char POC_REPORT_RECORDS_FILENAME[] =	"POC_REPORT_RECORDS";

// ----------

// 1/21/2015 rmd : This is the structure revision number. It started at 0. And should be
// incremented by 1 as changes are made to the report data record or station history record.
//
// 1/28/2015 rmd : Move to revision 1 in order fully intialize the report file. We likely
// could count on the file size to do this. But this will for sure accomplish the same thing
// where as the size approach may not activiate.

#define	POC_REPORT_RECORDS_LATEST_FILE_REVISION		(1)


const UNS_32 poc_revision_record_sizes[ POC_REPORT_RECORDS_LATEST_FILE_REVISION + 1 ] =
{
	// 1/22/2015 rmd : The original base file. Which is going to be standardized to our new
	// report file format. The restructuring to the standard format creates revision 1.
	sizeof( POC_REPORT_RECORD ),

	// 1/22/2015 rmd : The move to version 1 is to restructure the file contents to or new
	// standardized report file format. There was no report record size change.
	sizeof( POC_REPORT_RECORD )
};

const UNS_32 poc_revision_record_counts[ POC_REPORT_RECORDS_LATEST_FILE_REVISION + 1 ] =
{
	// 1/22/2015 rmd : The original base file. Which is going to be standardized to our new
	// report file format. The restructuring to the standard format creates revision 1.
	POC_REPORT_RECORDS_TO_KEEP,

	// 1/22/2015 rmd : The move to version 1 is to restructure the file contents to or new
	// standardized report file format. There was no report record count change.
	POC_REPORT_RECORDS_TO_KEEP
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void nm_poc_report_data_updater( UNS_32 pfrom_revision )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only when
	// the file revision is changed. The caller is to be holding the poc report completed
	// records recursive_MUTEX.

	// ----------
	
	if( pfrom_revision == POC_REPORT_RECORDS_LATEST_FILE_REVISION )
	{
		Alert_Message_va( "POC RPRT file unexpd update %u", pfrom_revision );	
	}
	else
	{
		Alert_Message_va( "POC RPRT file update : to revision %u from %u", POC_REPORT_RECORDS_LATEST_FILE_REVISION, pfrom_revision );

		// ----------
		
		if( pfrom_revision == 0 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 0 to REVISION 1. Moving
			// up from revision 0 requires a complete file restart. The file content is invalid due to
			// the restructuring to the standardized report file format.
			nm_init_poc_report_records();
			
			// ----------
			
			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 1 to 2 changes if
			// needed.
			pfrom_revision += 1;
		}
		
		// ----------
		
		/*
		if( pfrom_revision == 1 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 1 to REVISION 2.
				


			pfrom_revision += 1;
		}
		*/
	}

	// ----------

	// 1/29/2015 rmd : Well after the updater has run the pfrom_revision is supposed to equal
	// the latest revision. By design of this update process.
	if( pfrom_revision != POC_REPORT_RECORDS_LATEST_FILE_REVISION )
	{
		Alert_Message( "POC RPRT updater error" );
	}
}

/* ---------------------------------------------------------- */
extern void init_file_poc_report_records( void )
{
	// 8/15/2014 rmd : This function is called on each and every startup - and ONLY then. Within
	// the context of the app_startup task.
	
	FLASH_FILE_find_or_create_reports_file(	FLASH_INDEX_1_GENERAL_STORAGE,
											POC_REPORT_RECORDS_FILENAME,
											POC_REPORT_RECORDS_LATEST_FILE_REVISION,
											
											&poc_report_data_completed,
											
											poc_revision_record_sizes,
											poc_revision_record_counts,
											sizeof( COMPLETED_POC_REPORT_RECORDS_STRUCT ),
											
											poc_report_completed_records_recursive_MUTEX,
											
											&nm_poc_report_data_updater,
											&nm_init_poc_report_records,
											
											FF_POC_REPORT_DATA );
												
	// ----------
	
	// 9/5/2014 rmd : Because we have included the controller initiated control variables in the
	// saved file there is some consideration here. First of all the first_to_send is assumed to
	// be valid and up to date. I think there are no real holes here. If the file didn't save
	// after closing and adding a line (power failure) the first to send may not be accurate. Or
	// if after receiving the data ACK from the web_app there is a power failure before the file
	// is saved we would send dups to the web_app. But that causes no harm. The problem to watch
	// out for is that the pending_first_send_in_use is saved into the file set (true). That
	// could happen I suppose. So that variable in particular on STARTUP should be set to
	// (false).
	poc_report_data_completed.rdfb.pending_first_to_send_in_use = (false);
}

/* ---------------------------------------------------------- */
extern void save_file_poc_report_records( void )
{
	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE,
															POC_REPORT_RECORDS_FILENAME,
															POC_REPORT_RECORDS_LATEST_FILE_REVISION,
															&poc_report_data_completed,
															sizeof( COMPLETED_POC_REPORT_RECORDS_STRUCT ),
															poc_report_completed_records_recursive_MUTEX,
															FF_POC_REPORT_DATA );
}

/* ---------------------------------------------------------- */
static void poc_report_data_ci_timer_callback( xTimerHandle pxTimer )
{
	// 10/2/2014 rmd : Remember this is executed within the context of the high priority timer
	// task. And is executed when it it time to send some poc data records. Which is normally 15
	// minutes after a record is closed.
	CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_poc_report_data, NULL, CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD, CI_POST_TO_BACK );
}

extern void POC_REPORT_DATA_start_the_ci_timer_if_it_is_not_running( void )
{
	// 10/2/2014 rmd : On startup the poc report data ci timer pointer is NULL. If so let's
	// create the timer. This is a convienient way to create the timer before use.
	if( poc_report_data_ci_timer == NULL )
	{
		// 7/13/2017 rmd : The time used when the timer is created is not important as we always
		// explicitly set a time when we start the timer.
		poc_report_data_ci_timer = xTimerCreate( (const signed char*)"", MS_to_TICKS(60000), FALSE, (void*)NULL, poc_report_data_ci_timer_callback );
			
		if( poc_report_data_ci_timer == NULL )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "Timer NOT CREATED" );
		}
	}
	
	// ----------
	
	// 10/2/2014 rmd : If it exists do what we came for. Start the timer.
	if( poc_report_data_ci_timer != NULL )
	{
		// 10/2/2014 rmd : Only if it is not running. Which means effectively a maximum of 15
		// minutes after a record is closed it will be sent.
		if( xTimerIsTimerActive( poc_report_data_ci_timer ) == pdFALSE )
		{
			// 10/2/2014 rmd : Start the timer to trigger the controller initiated send.
			xTimerChangePeriod( poc_report_data_ci_timer, MS_to_TICKS( CONTROLLER_INITIATED_ms_after_midnight_to_send_this_controllers_daily_records( config_c.serial_number ) ), portMAX_DELAY );
		}
	}
}

/* ---------------------------------------------------------- */
extern void nm_POC_REPORT_DATA_inc_index( UNS_32 *pindex_ptr )
{
	// 10/2/2014 rmd : This function is used by the CONTROLLER_INITIATED task as well as here
	// within this file when closing a record (that is in the context of the TD_CHECK task).

	*pindex_ptr += 1;
	
	if( *pindex_ptr >= POC_REPORT_RECORDS_TO_KEEP )
	{
		// 6/26/2012 rmd : Okay so we have gone past the end of the array. Wrap around.
		*pindex_ptr = 0;
	}
}


/**
    @DESCRIPTION Figures where to put the next completed poc report record. The records are
    stored in a ring buffer fashion. There are no failure modes. The function always set the
    pointer to the next record.
	
    @CALLER_MUTEX_REQUIREMENTS The caller is to be holding the poc report completed records
    mutex.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task. At the POC report roll
    time.
	
	@RETURN (none)

	@ORIGINAL 2012.06.04 rmd

	@REVISIONS (none)
*/
static void nm_POC_increment_next_avail_ptr( void )
{
	nm_POC_REPORT_DATA_inc_index( &poc_report_data_completed.rdfb.index_of_next_available );

	if( poc_report_data_completed.rdfb.index_of_next_available == 0 )
	{
		// 6/26/2012 rmd : Okay so we have gone past the end of the array and wrapped. Set the
		// wrapped indication (true). Support for the display functions.
		poc_report_data_completed.rdfb.have_wrapped = (true);
	}
	
	// ----------
	
	// 10/2/2014 rmd : If we have bumped into the first to send move it along too. This means we
	// made more lines than we hold in between send to comm server attempts.
	if( poc_report_data_completed.rdfb.index_of_next_available == poc_report_data_completed.rdfb.first_to_send )
	{
		nm_POC_REPORT_DATA_inc_index( &poc_report_data_completed.rdfb.first_to_send );
	}

	// 10/2/2014 rmd : Also if next available has landed on the pending_first_to_send it means
	// that during the CI session we have actually written so many lines we wrapped all the way
	// around. In other words we wrote more lines than we hold during the session (which during
	// normal operation is impossible - a debug function used during development could do this).
	// So move that index too.
	if( poc_report_data_completed.rdfb.index_of_next_available == poc_report_data_completed.rdfb.pending_first_to_send )
	{
		nm_POC_REPORT_DATA_inc_index( &poc_report_data_completed.rdfb.pending_first_to_send );
	}
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This function is to be used in conjunction with the accompanying 'get
    first' function. Following the get first the user may repeatedly call the get next
    function until a NULL is returned. That will signal that he has seen all the completed
    lines. The entire get first and get next sequence is to be encapsulated within a take
    and give mutex pair for the poc report data completed records mutex. The pointer
    argument points to the last record seen.
	
    @CALLER_MUTEX_REQUIREMENTS The caller is to be holding the poc report data completed
    records mutex during the entire iterative process calling this function again and again.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the DISPLAY PROCESSING task. When drawing the
    report data to the screen. May also be used in the COMM_MNGR as part of send the report
    records to the central.

    @RETURN If the last record (oldest) has been encountered will return a NULL. Otherwise a
    pointer to the next older record. The records are returned in reverse chronological
    order.

	@ORIGINAL 2012.07.30 rmd 

	@REVISIONS (none)
*/
extern POC_REPORT_RECORD *nm_POC_REPORT_RECORDS_get_previous_completed_record( POC_REPORT_RECORD *pprr_ptr )
{
	POC_REPORT_RECORD	*rv;

	if( (pprr_ptr < &(poc_report_data_completed.prr[ 0 ])) || (pprr_ptr >= &(poc_report_data_completed.prr[ POC_REPORT_RECORDS_TO_KEEP ])) )
	{
		// 6/26/2012 rmd : Out of range. Do not use.
		rv = NULL;
	}
	else
	if( poc_report_data_completed.rdfb.have_returned_next_available_record == (true) )
	{
		// 6/29/2012 rmd : If we have wrapped all the way around and are back to where we started
		// then we have returned all possible records.
		rv = NULL;	
	}
	else
	{
		if( pprr_ptr == &(poc_report_data_completed.prr[ 0 ]) )
		{
			if( poc_report_data_completed.rdfb.have_wrapped == (true) )
			{
				// 6/26/2012 rmd : Back up at the top.
				rv = &(poc_report_data_completed.prr[ ( (POC_REPORT_RECORDS_TO_KEEP) - 1 ) ]);
			}
			else
			{
				rv = NULL;
			}
		}
		else
		{
			rv = pprr_ptr;
	
			rv -= 1;
		}
	}
	
	if( rv == &(poc_report_data_completed.prr[ poc_report_data_completed.rdfb.index_of_next_available ]) )
	{
		// 6/29/2012 rmd : If we've returned records all the way back to the start indicate this is
		// the last one. So the next call to this function will return a NULL.
		poc_report_data_completed.rdfb.have_returned_next_available_record = (true);
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Call this function first. In preparation to retrieve a ptr to each of the
    completed lines by repeatedly calling the accompanying 'get next' function. If there are
    no lines this function returns a NULL. Otherwise a ptr to the first (newest) completed
    line.
	
    @CALLER_MUTEX_REQUIREMENTS The caller is to be holding the poc report data completed
    records mutex during the entire process calling this function and the accompanying
    'previous' function.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the DISPLAY PROCESSING task. When drawing the
    report data to the screen. May also be used in the COMM_MNGR as part of send the report
    records to the central.

    @RETURN The return value points to the most recently completed record if there is one.
    Otherwise NULL - meaning there are no completed records.

	@ORIGINAL 2012.06.29 rmd 

	@REVISIONS (none)
*/
extern POC_REPORT_RECORD *nm_POC_REPORT_RECORDS_get_most_recently_completed_record( void )
{
	POC_REPORT_RECORD	*rv;
	
	// 6/29/2012 rmd : And set the logic to support the repetitive extraction calls.
	poc_report_data_completed.rdfb.have_returned_next_available_record = (false);
	
	rv = nm_POC_REPORT_RECORDS_get_previous_completed_record( &(poc_report_data_completed.prr[ poc_report_data_completed.rdfb.index_of_next_available ]) );
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Using only the poc gid count how many matching lines are in the database.
    And during that process maintains an array of ptrs to each of the records.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed ONLY within the context of the DISPLAY_PROCESS task. As it's name
    indicates. This occurs when the screen is initially drawn. And at a 1 Hz rate as part of
    a screen update process.

    @RETURN The matching number of lines. Will always be at least 1 as the rip always
    exists.

	@ORIGINAL 2012.07.27 rmd

	@REVISIONS (none)
*/
extern UNS_32 FDTO_POC_REPORT_fill_ptrs_and_return_how_many_lines( const UNS_32 plist_index )
{
	UNS_32	rv;
	
	rv = 0;

	// ------------------------
	
	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	UNS_32	lpoc_gid;

	lpoc_gid = POC_get_gid_of_group_at_this_index( POC_get_index_using_ptr_to_poc_struct( POC_get_ptr_to_physically_available_poc( ( plist_index ) ) ) );
	
	if( lpoc_gid == 0 )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "POC: unexpd results." );
	}

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	// ------------------------
	
	// 8/1/2012 rmd : Besides running through the completed records finding matches we should be
	// concerned with the poc_report_ptrs memory allocation. Suppose one were to "change
	// screens" right at the time td_check was running through filling the array with ptrs (this
	// function). That could be a big problem is the the change screen routine "freed" the
	// memory and set poc_report_ptr to NULL before this function completed.
	xSemaphoreTakeRecursive( poc_report_completed_records_recursive_MUTEX, portMAX_DELAY );

	// ----------------------

	if( poc_report_ptrs == NULL )
	{
		// 7/27/2012 rmd : Oh just grab the maximum size ever necessary. Otherwise we get into the
		// potential of having to free the memory and re-allocing as the number of lines grows. That
		// could be done but I decided to just grab the worst case up front. Also we'd have to go
		// through all the report records twice. Once to learn how many. And once to fill the array.
		// 7/27/2012 rmd : Oh and the story gets even more complicated. Cause if the database is
		// full and another line is added the count of the number of lines for this station may stay
		// the same but the ptrs to the lines are now different? Not sure about that thought.
		poc_report_ptrs = mem_malloc( POC_REPORT_RECORDS_TO_KEEP * sizeof(void *) );
	}
	
	// ----------------------

	// 7/27/2012 rmd : Always load the ptr to the rip.
	UNS_32	lindex;
	
	POC_PRESERVES_get_poc_preserve_for_this_poc_gid( lpoc_gid, &lindex );

	(*poc_report_ptrs)[ rv++ ] = &(poc_preserves.poc[ lindex ].rip);
	
	// ----------------------

	POC_REPORT_RECORD	*lprr;

	lprr = nm_POC_REPORT_RECORDS_get_most_recently_completed_record();

	while( lprr != NULL )
	{
		if( lprr->poc_gid == lpoc_gid )
		{
			(*poc_report_ptrs)[ rv++ ] = lprr;
		}

		// 7/27/2012 rmd : So as to not write beyond our allocated space.
		if( rv >= POC_REPORT_RECORDS_TO_KEEP )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "REPORTS: why so many records?" );
			
			break ;
		}

		lprr = nm_POC_REPORT_RECORDS_get_previous_completed_record( lprr );
	}

	// ----------------------

	xSemaphoreGiveRecursive( poc_report_completed_records_recursive_MUTEX );

	// ----------------------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Draws an individual scroll line for the Station Report. This is used as the
    callback routine for the GuiLib_ScrollBox_Init routine. And uses the array of pointers
    created in the STATION_REPORT_DATA_fill_ptrs_and_return_how_many_lines function.
	
    @CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed ONLY within the context of the DISPLAY_PROCESS task. As it's name
    indicates. This occurs when the screen is initially drawn. And when the user navigates
    up or down the scroll box.

	@RETURN (none)

	@ORIGINAL 2012.08.03 rmd

	@REVISIONS (none)
*/
extern void FDTO_POC_REPORT_load_guivars_for_scroll_line( const INT_16 pline_index_0_i16 )
{
	POC_REPORT_RECORD	*lprr;

	lprr = (*poc_report_ptrs)[ pline_index_0_i16 ];

	// 8/1/2012 rmd : Taking this completed records MUTEX protects the array of ptrs from
	// changing if we happen to be viewing at the roll time.
	xSemaphoreTakeRecursive( poc_report_completed_records_recursive_MUTEX, portMAX_DELAY );

	char	dt_buf[ 16 ];

	strlcpy( GuiVar_RptDate, GetDateStr( dt_buf, sizeof(dt_buf), lprr->start_date, DATESTR_show_year_not, DATESTR_show_dow_not ), sizeof(GuiVar_RptDate) );

	GuiVar_RptIrrigMin = ((float)(lprr->seconds_of_flow_during_irrigation) / 60.0F);
	GuiVar_RptIrrigGal = lprr->gallons_during_irrigation;

	GuiVar_RptManualMin = ((float)(lprr->seconds_of_flow_during_mvor) / 60.0F);
	GuiVar_RptManualGal = lprr->gallons_during_mvor;

	GuiVar_RptNonCMin = ((float)(lprr->seconds_of_flow_during_idle) / 60.0F);
	GuiVar_RptNonCGal = lprr->gallons_during_idle;

	xSemaphoreGiveRecursive( poc_report_completed_records_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Take the existing record referenced by the pointer and merge into the
    completed lines database.

    @CALLER_MUTEX_REQUIREMENTS The caller is to be holding the poc preserves mutex.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task. Each day at the station poc
    report roll time (noon? midnight? start of irrigation day?).
	
	@RETURN (none)

	@ORIGINAL 2012.08.01 rmd

	@REVISIONS (none)
*/
extern void nm_POC_REPORT_DATA_close_and_start_a_new_record( BY_POC_RECORD *pbpr_ptr, const DATE_TIME *pdate_time )
{
	// 10/3/2014 rmd : REMEMBER the caller is to be holding the poc preserves recursive MUTEX.
	// Because we are using and initializing the rip which is stored in the preserves.

	// ----------

	// 8/1/2012 rmd : As we are writing to the completed records base take the MUTEX.
	xSemaphoreTakeRecursive( poc_report_completed_records_recursive_MUTEX, portMAX_DELAY );

	// 6/26/2012 rmd : Move the rip into the completed.
	poc_report_data_completed.prr[ poc_report_data_completed.rdfb.index_of_next_available ] = pbpr_ptr->rip;
	
	nm_POC_increment_next_avail_ptr();

	// 8/1/2012 rmd : Done with the requirement to hold this MUTEX. Hold onto the preserves
	// MUTEX till after the rip is intiialized!
	xSemaphoreGiveRecursive( poc_report_completed_records_recursive_MUTEX );

	// ----------
	
	// 10/2/2014 rmd : Start the ci timer so the data is sent to the comm server.
	POC_REPORT_DATA_start_the_ci_timer_if_it_is_not_running();
	
	// ----------
	
	// 6/26/2012 rmd : Schedule a save of the station report data file. In 2 seconds. For each
	// station that hits a start time we call this. The idea is it gets called over and over
	// again once per station and restarts the timer. So it doesn't expire until we've made all
	// our lines. And we do a single file save 2 seconds after the last request.
	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_POC_REPORT_DATA, FLASH_STORAGE_seconds_to_delay_file_save_after_rolling_report_data );
	
	// ----------
	
	// 10/3/2014 rmd : Restarting the rip record. The caller is to be holding the poc preserves
	// MUTEX.
	nm_init_poc_report_record( &(pbpr_ptr->rip) );

	// 6/4/2012 rmd : Stamp the record origin. The time will be the roll time.
	pbpr_ptr->rip.start_date = pdate_time->D;
	pbpr_ptr->rip.start_time = pdate_time->T;

	// 8/2/2012 rmd : Assign the poc gid to the record. The other place this is done is when the
	// poc_preserves actually syncs a preserves record to a poc in the file list. But that is
	// only done once. And since we just zeroed the record we must do again else when the
	// completed record is saved to the file (24hrs from now) it wont be owned by its poc.
	pbpr_ptr->rip.poc_gid = pbpr_ptr->poc_gid;

	// ----------
	
	// 10/29/2013 ajv : If the user is viewing the POC Summary report when a new
	// line is added, force the scroll box to be updated.
	if( GuiLib_CurStructureNdx == GuiStruct_rptPOCSummary_0 )
	{
		DISPLAY_EVENT_STRUCT	lde;

		lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
		lde._04_func_ptr = &FDTO_POC_USAGE_redraw_scrollbox;
		Display_Post_Command( &lde );
	}
}

/* ---------------------------------------------------------- */

#if ALERTS_ABILITY_TO_GENERATE_ALL_ALERTS_AND_REPORTS_FOR_DEBUG

extern void POC_REPORT_DATA_generate_lines_for_DEBUG( void )
{
	DATE_TIME_COMPLETE_STRUCT lcdt;

	UNS_32	lbox_index_0, lgroup_id, i;

	UNS_32	lindex;

	EPSON_obtain_latest_complete_time_and_date( &lcdt );

	lbox_index_0 = FLOWSENSE_get_controller_index();

	// ----------

	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	lgroup_id = nm_GROUP_get_group_ID( POC_get_group_at_this_index( 0 ) );

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	// ----------

	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	POC_PRESERVES_get_poc_preserve_for_this_poc_gid( lgroup_id, &lindex );

	for( i = 0; i < (POC_REPORT_RECORDS_TO_KEEP / MAX_POCS_IN_NETWORK); i++ )
	{
		poc_preserves.poc[ lindex ].box_index = lbox_index_0;

		poc_preserves.poc[ lindex ].rip.start_date = (lcdt.date_time.D - (POC_REPORT_RECORDS_TO_KEEP / MAX_POCS_IN_NETWORK) + i);
		poc_preserves.poc[ lindex ].rip.start_time = lcdt.date_time.T;

		nm_POC_REPORT_DATA_close_and_start_a_new_record( &(poc_preserves.poc[ lindex ]), &(lcdt.date_time) );
	}

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
}

#endif

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Returns the memory used by the POC accumulator report.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the KEY_PROCESS task. As part of each and every
    screen change. I suppose can also be called as part of TD_CHECK when a screen change is
    triggered by a time out.
	
	@RETURN (none)

	@ORIGINAL 2012.07.30 rmd

	@REVISIONS (none)
*/
extern void POC_REPORT_free_report_support( void )
{
	// 8/1/2012 rmd : Suppose one were to "change" screens right at the time td_check was
	// running through filling the array with ptrs. That could be a big problem is the the
	// change screen routine "freed" the memory and set poc_report_ptr to NULL before the
	// function was done filling the array ie using the ptr value.
	xSemaphoreTakeRecursive( poc_report_completed_records_recursive_MUTEX, portMAX_DELAY );

	if( poc_report_ptrs != NULL )
	{
		mem_free( poc_report_ptrs );
		
		// 7/27/2012 rmd : Beacuse the free doesn't set the ptr to NULL we do so. In order not to
		// try and free again as we move around screens.
		poc_report_ptrs = NULL;
	}

	xSemaphoreGiveRecursive( poc_report_completed_records_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

