/* file = bypass_operation.h  7/10/2014  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_BYPASS_OPERATION_H
#define _INC_BYPASS_OPERATION_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"lpc_types.h"

#include	"battery_backed_vars.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/10/2014 rmd : Notes from the 2000e -
// This is set artificially high. For normal irrigation, we keep 30 flow readings to
// calculate a 20 second average. For the DMC, we are defaulting to a 20 second average, but
// will allow this value to be changed via CMOS. Therefore, we must have enough raw flow
// readings to accomodate up to a 60 second average.
// 
// rmd: On SECOND thought - we have tried and proven averages working in the FOAL machine.
// They use a 20 second average that is then run through the 70/30 weighting equation. We're
// gonna do the same in the DMC.
//
// 10/3/2014 rmd : BUT things are different when it comes to getting flow rates from POC
// DECODERS. We only get an update once every 10 seconds. So if flow spikes when a valve
// opens we may respond by opening a higher stage only to then 30 seconds later switch back
// to the smaller. The sort of behavior we have observed at the CATO controller in
// Bakerfield. So by increasing the number of readings kept and averaged over to 42 we know
// we can dampen the responsiveness and assure us 4 POC decoder readings involved to make
// the bypass switching decision. Anyway that's my way of looking at it.
#define BYPASS_READINGS_IN_FLOW_AVG	(42)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	BYPASS_EVENT_one_hertz_execution		(1000)

#define	BYPASS_EVENT_flow_reading_update		(2000)

#define	BYPASS_EVENT_midnight					(3000)


typedef struct
{
	// To identify the event.
	UNS_32		event;

	UNS_32		decoder_serial_number;
	
	UNS_32		fm_latest_5_second_count;

	UNS_32		fm_delta_between_last_two_fm_pulses_4khz;
	
	// 1/13/2015 rmd : This is used to deduce that the flow rate has gone below a minimum agreed
	// upon value. For example for the 1 pulse per 10 gallons transducer if this value is
	// greater than 60 seconds that means the flow rate is less than 10 gpm. And the main board
	// then can make the decision to ignore the delta and call the flow rate 0.
	UNS_32		fm_seconds_since_last_pulse;
	
} BYPASS_EVENT_QUEUE_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define BYPASS_MODE_passive_deenergized		(100)

#define BYPASS_MODE_operational				(200)


#define BYPASS_STATE_first_open				(10)

#define BYPASS_STATE_second_open			(20)

#define BYPASS_STATE_third_open				(30)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 7/10/2014 rmd : Two pointers that if correct indicate the bypass structure has been
	// operating and was initialized and linked up to the preserves and poc file.
	BY_POC_RECORD		*bpr;
	
	void				*lpoc;
	
	// ----------
	
	// 7/11/2014 rmd : Machine control variables.
	UNS_32		mode;

	UNS_32		state;
	
	// 7/11/2014 rmd : (true) if linked to a bypass in the poc preserves and file. When true the
	// two pointers are assumed to be valid. That assumption is however usually verfied prior to
	// using them.
	BOOL_32		linked;

	BOOL_32		alert_about_invalid_configrations;
	
	// ----------
	
	float		latest_flow_rate[ POC_BYPASS_LEVELS_MAX ];

	float		flow_rate_history[ POC_BYPASS_LEVELS_MAX ][ BYPASS_READINGS_IN_FLOW_AVG ];
	
	float		final_avg[ POC_BYPASS_LEVELS_MAX ];
	
	// ----------
	
	UNS_32		seconds_since_went_operational;

	// ----------

	// 608.b ajv moved into DMC Struct to make accessible to DMC Recorder
	float		trip_point_up;
	
	float   	trip_point_down;

} BYPASS_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern BYPASS_STRUCT	bypass;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern UNS_32 BYPASS_get_transition_rate_for_this_flow_meter_type( const UNS_32 pfm_type );

extern void BYPASS_task( void *pvParameters );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

