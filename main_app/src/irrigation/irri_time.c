/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"irri_time.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"cal_math.h"

#include	"change.h"

#include	"configuration_controller.h"

#include	"et_data.h"

#include	"station_groups.h"

#include	"watersense.h"

#include	"weather_control.h"

#include	"weather_tables.h"

#include	"ftimes_funcs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern BOOL_32 nm_IRRITIME_this_program_is_scheduled_to_run( STATION_STRUCT *const pss_ptr, STATION_GROUP_STRUCT *const psgs_ptr )
{
	UNS_32	lschedule_type;

	UNS_32	lday;

	BOOL_32 rv;

	// ----------

	rv = (false);
	
	// 3/13/2015 ajv : If there is a start time, it might have some water days in which case it
	// would be scheduled to run.
	if ( SCHEDULE_get_enabled( psgs_ptr ) == (true) )
	{
		lschedule_type = SCHEDULE_get_schedule_type( psgs_ptr );

		switch( lschedule_type )
		{
			case SCHEDULE_TYPE_WEEKLY_SCHEDULE:
				// 3/13/2015 ajv : If the user has a weekly schedule, we check to see whether they have at
				// least one day scheduled - if so, return TRUE.
				for( lday = 0; lday < DAYS_IN_A_WEEK; ++lday )
				{
					if( SCHEDULE_get_station_waters_this_day( pss_ptr, lday ) == (true) )
					{
						rv = (true);

						break;
					}
				}
				break;

			default:
				// 3/13/2015 ajv : All other schedule types have water days, so return TRUE.
				rv = (true);
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the number of days in the irrigation period using the start time and time and
 * date parameter to understand where in the schedule we are.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex as we have
 *  	  a pointer to a station group structure.
 *
 * @executed Executed within the context of the TD check task when a start time is crossed.
 * Also executed when the station information screen is drawn as we calcualte the run time.
 * 
 * @param psgs_ptr A pointer to the schedule to investigate.
 * 
 * @param pdtcs_ptr A pointer to the current time and date.
 * 
 */
static void increment_day_in_weekly_array( UNS_32 *pday )
{
	*pday += 1;
	
	if( *pday >= DAYS_IN_A_WEEK )
	{
		*pday = 0;
	}
}

static void decrement_day_in_weekly_array( UNS_32 *pday )
{
	if( *pday == 0 )
	{
		*pday = DAYS_IN_A_WEEK;
	}

	*pday -= 1;
}

extern float days_in_this_irrigation_period_for_a_weekly_schedule( RUN_TIME_CALC_SUPPORT_STRUCT	*psupport_ptr, const DATE_TIME_COMPLETE_STRUCT *pdtcs_ptr )
{
	float		rv;
	
	UNS_32		ddd;
	
	UNS_32		days_set;
	
	const volatile float ZERO_POINT_ZERO = 0.0;

	const volatile float ONE_POINT_O = 1.0;

	const volatile float SEVEN_POINT_O = 7.0;

	// ----------
	
	// 4/5/2016 rmd : Verify at least one day is set so we don't have to think about an empty
	// schedule in the logic that follows.
	days_set = 0;
	
	for( ddd=0; ddd<DAYS_IN_A_WEEK; ddd++ )
	{
		if( psupport_ptr->wday[ ddd ] )
		{
			days_set += 1;
		}
	}

	// ----------
	
	if( days_set == 0 )
	{
		// 4/5/2016 rmd : The caller must be able to handle a return of 0.0 days in the period. And
		// I have checked such with this in mind.
		rv = ZERO_POINT_ZERO;
	}
	else
	if( psupport_ptr->uses_et_averaging )
	{
		// 4/8/2016 rmd : NOTE - ONLY when irrigating using the weekly scheduling, where you specify
		// the specific days of the week to irrigate, does ET averaging have an affect on the run
		// time. This can return values like 3.5 for example with the case of 2 days per week set.
		rv = ( SEVEN_POINT_O / (float)days_set );
	}
	else
	{
		// 4/5/2016 rmd : FIRST. Start at today. And check if we are past the start time yet. If so
		// move ahead one day to the next irrigation period.
		ddd = pdtcs_ptr->__dayofweek;	

		// 4/5/2016 rmd : If past the start time we are actually in the next period already so we
		// count start with tomorrow. NOTE if the time is equal to the sx we are AT the start time
		// and do not move ahead.
		if( pdtcs_ptr->date_time.T > psupport_ptr->sx )
		{
			increment_day_in_weekly_array( &ddd );
		}

		// ----------
		
		// 4/5/2016 rmd : SECOND move forward to the next watering day. We already tested for and we
		// know there is a watering day in the array so this cannot be an endless loop.
		while( !psupport_ptr->wday[ ddd ])
		{
			increment_day_in_weekly_array( &ddd );
		}
		
		// ----------
		
		// 4/5/2016 rmd : THIRD we are now on a future watering day. We should begin decrementing
		// until we find the next previous day.
		rv = ZERO_POINT_ZERO;
		
		do
		{
			rv += ONE_POINT_O;
			
			decrement_day_in_weekly_array( &ddd );
			
		} while( !psupport_ptr->wday[ ddd ] );
		
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the number of days in this irrigation period.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex to ensure
 *  	  the station whose values are being read isn't deleted or changed during this
 *  	  process.
 *
 * @executed Executed within the context of the TD check task when a start time is crossed.
 * 
 * @param pss_ptr A pointer to the station.
 * 
 * @param psgs_ptr A pointer to the schedule the station is assigned to.
 * 
 * @param pdtcs_ptr A pointer to the date and time this function was called.
 * 
 * @return float The number of days in this irrigation period.
 *
 * @author 7/9/2012 Adrianusv
 *
 * @revisions
 *    7/9/2012 Initial release
 */
// 2/22/2018 rmd : Declared an EXTERN to avoid the GCC compiler floating point bug.
extern float nm_WATERSENSE_days_in_this_irrigation_period(	BOOL_32 pfor_ftimes,
															STATION_STRUCT *const pss_ptr,
															STATION_GROUP_STRUCT *const psgs_ptr,
															FT_STATION_STRUCT* pft_station_ptr,
															const DATE_TIME_COMPLETE_STRUCT *pdtcs_ptr )
{
	UNS_32	lprev_month;

	UNS_32	lprev_year;

	float rv;

	BOOL_32		loaded;
	
	RUN_TIME_CALC_SUPPORT_STRUCT	support;
	
	UNS_32	last_day_of_this_month;
	
	UNS_32	second_from_last_day_of_this_month;
	
	UNS_32	last_day_of_previous_month;
	
	// ----------
	
	const volatile float ZERO_POINT_ZERO = 0.0;

	const volatile float ONE_POINT_O = 1.0;

	const volatile float TWO_POINT_O = 2.0;

	const volatile float THREE_POINT_O = 3.0;

	const volatile float TWENTY_ONE_POINT_O = 21.0;

	const volatile float TWENTY_EIGHT_POINT_O = 28.0;

	// ----------

	rv = ZERO_POINT_ZERO;

	// ----------
	
	if( pfor_ftimes )
	{
		// 2/22/2018 rmd : Tests for non-NULL ft_station_group_ptr.
		loaded = FTIMES_FUNCS_load_run_time_calculation_support( pft_station_ptr->station_group_ptr, &support );
	}
	else
	{
		// 4/7/2016 rmd : Get the support settings we need. If the psgs_ptr parameter seems invalid
		// this function will return (false) in which case we should return 0 days in the period.
		loaded = SCHEDULE_get_run_time_calculation_support( psgs_ptr, &support );
	}

	// ----------
	
	// 2/22/2018 rmd : If we are at the start time we use todays date. If we are past the start
	// time 


	// ----------
	
	// 4/8/2016 rmd : If the schedule is not enabled return a 0.0 number of days in the period.
	if( loaded && support.schedule_enabled ) 
	{
		// 4/7/2016 rmd : Crank up some support values regarding this month and the previous month.
		last_day_of_this_month = NumberOfDaysInMonth( pdtcs_ptr->__month, pdtcs_ptr->__year );
		
		second_from_last_day_of_this_month = (last_day_of_this_month - 1);
		
		if( pdtcs_ptr->__month > 1 )
		{
			lprev_month = pdtcs_ptr->__month - 1;
			lprev_year = pdtcs_ptr->__year;
		}
		else
		{
			lprev_month = MONTHS_IN_A_YEAR;
			lprev_year = pdtcs_ptr->__year - 1;
		}
	
		last_day_of_previous_month = NumberOfDaysInMonth( lprev_month, lprev_year );
		
		// ----------

		switch( support.schedule_type )
		{
			case SCHEDULE_TYPE_EVERY_DAY:
				// 4/7/2016 rmd : Regardless of the time of day versus the SX the answer is always 1.
				rv = ONE_POINT_O;
				break;

			case SCHEDULE_TYPE_EVEN_DAYS:
				if( (pdtcs_ptr->__day == second_from_last_day_of_this_month) || (pdtcs_ptr->__day == last_day_of_this_month) )
				{
					if( (last_day_of_this_month % 2) == 0 )
					{
						// 4/7/2016 rmd : If this number of days in this month is EVEN then we are always in a 2 day
						// period.
						rv = TWO_POINT_O;
					}
					else
					if( ( (pdtcs_ptr->__day == second_from_last_day_of_this_month) && (pdtcs_ptr->date_time.T > support.sx) ) || (pdtcs_ptr->__day == last_day_of_this_month) )
					{
						// 4/7/2016 rmd : If we are in a month with an ODD number of days and we are after the last
						// start time of this ODD day month or we are in the last day then we are in a 3 day period.
						rv = THREE_POINT_O;
					}
					else
					{
						// 4/7/2016 rmd : This can ONLY mean we are in an ODD DAY month and on the second to last
						// day of the month AT OR BEFORE the start time. So then we are in a 2 day period.
						rv = TWO_POINT_O;
					}
				}
				else
				if( (pdtcs_ptr->__day == 1) || (pdtcs_ptr->__day == 2) )
				{
					if( (last_day_of_previous_month % 2) == 0 )
					{
						// 4/7/2016 rmd : For the case the previous month had an even number of days the irrigation
						// period is always 2 days.
						rv = TWO_POINT_O;
					}
					else
					if( (pdtcs_ptr->__day == 1) || ((pdtcs_ptr->__day == 2) && (pdtcs_ptr->date_time.T <= support.sx)) )
					{
						// 4/7/2016 rmd : The previous month ended on an ODD day. If this is the 1st or this is the
						// (2nd day of the month and we are AT or BEFORE the start time) then this is a 3 day
						// period. Otherwise a 2 day period.
						rv = THREE_POINT_O;
					}
					else
					{
						// 4/7/2016 rmd : And this is the case where the previous month had an ODD number of days
						// and we are on day 2 of the new month AFTER the start time. So are back into 2 day
						// periods.
						rv = TWO_POINT_O;
					}
				}
				else
				{
					// 4/7/2016 rmd : For all other days of the month we are in a 2 day period.
					rv = TWO_POINT_O;
				}
				break;

			case SCHEDULE_TYPE_ODD_DAYS:
				if( pdtcs_ptr->__day == last_day_of_this_month )
				{
					if( (last_day_of_this_month % 2) == 0 )
					{
						// 4/7/2016 rmd : If this number of days in this month is EVEN then we are always in a 2 day
						// period.
						rv = TWO_POINT_O;
					}
					else
					if( pdtcs_ptr->date_time.T <= support.sx )
					{
						// 4/7/2016 rmd : Okay so we are in a month with an ODD number of days, we are in the last
						// day of the month and we are BEFORE the start time. So we are in a 2 day period.
						rv = TWO_POINT_O;
					}
					else
					if( support.irrigates_after_29th_or_31st )
					{
						// 4/7/2016 rmd : Okay so we are in a month with an ODD number of days, we are on the last
						// day, AFTER the start time. And we are going to irrigate tomorrow, FIRST day of the new
						// month. So we are in a ONE day period.
						rv = ONE_POINT_O;
					}
					else
					{
						rv = THREE_POINT_O;
					}
				}
				else
				if( pdtcs_ptr->__day <= 3 )
				{
					if( (last_day_of_previous_month % 2) == 0 )
					{
						// 4/7/2016 rmd : For the case the previous month had an EVEN number of days the irrigation
						// period is always 2 days.
						rv = TWO_POINT_O;
					}
					else
					if( pdtcs_ptr->__day == 1 )
					{
						// 4/8/2016 rmd : So we have the previous had an ODD number of days and we are on the first
						// day of the new month.
						if( pdtcs_ptr->date_time.T <= support.sx )
						{
							// 4/8/2016 rmd : So we are BEFORE the start time on the first day of the new month.
							if( support.irrigates_after_29th_or_31st )
							{
								rv = ONE_POINT_O;
							}
							else
							{
								rv = THREE_POINT_O;
							}
						}
						else
						{
							// 4/7/2016 rmd : So we are AFTER the start time on the first day of the month.
							if( support.irrigates_after_29th_or_31st )
							{
								rv = TWO_POINT_O;
							}
							else
							{
								rv = THREE_POINT_O;
							}
						}
					}
					else
					if( (pdtcs_ptr->__day == 2) || ((pdtcs_ptr->__day == 3) && (pdtcs_ptr->date_time.T <= support.sx)) )
					{
						// 4/7/2016 rmd : New month following a month that had an ODD number of days. At (day 2) or
						// (day 3 up to the start time).
						if( support.irrigates_after_29th_or_31st )
						{
							rv = TWO_POINT_O;
						}
						else
						{
							rv = THREE_POINT_O;
						}
					}
					else
					{
						// 4/7/2016 rmd : So this is the case we are at day 3 AFTER the start time and back into the
						// normal 2 day periods.
						rv = TWO_POINT_O;
					}
				}
				else
				{
					// 4/7/2016 rmd : For all other days of the month except the 1st, the 2nd, the 3rd, and the
					// last day of the month. Those are all explicitly covered above!
					rv = TWO_POINT_O;
				}
				break;

			case SCHEDULE_TYPE_EVERY_OTHER_DAY:
				rv = TWO_POINT_O;
				break;

			case SCHEDULE_TYPE_WEEKLY_SCHEDULE:
				// 4/8/2016 rmd : This function call incorporates ETAveraging logic. And can return values
				// such as 3.5 days.
				rv = days_in_this_irrigation_period_for_a_weekly_schedule( &support, pdtcs_ptr );
				break;

			case SCHEDULE_TYPE_EVERY_THREE_DAYS:
			case SCHEDULE_TYPE_EVERY_FOUR_DAYS:
			case SCHEDULE_TYPE_EVERY_FIVE_DAYS:
			case SCHEDULE_TYPE_EVERY_SIXTH_DAYS:
			case SCHEDULE_TYPE_EVERY_SEVEN_DAYS:
			case SCHEDULE_TYPE_EVERY_EIGHT_DAYS:
			case SCHEDULE_TYPE_EVERY_NINE_DAYS:
			case SCHEDULE_TYPE_EVERY_TEN_DAYS:
			case SCHEDULE_TYPE_EVERY_ELEVEN_DAYS:
			case SCHEDULE_TYPE_EVERY_TWELVE_DAYS:
			case SCHEDULE_TYPE_EVERY_THIRTEEN_DAYS:
			case SCHEDULE_TYPE_EVERY_FOURTEEN_DAYS:
				// 3/13/2015 ajv : Since every Xth day starts at every third day, add 3 to the difference.
				rv = (float)((support.schedule_type - SCHEDULE_TYPE_EVERY_THREE_DAYS) + 3);
				break;

			case SCHEDULE_TYPE_ONCE_EVERY_THREE_WEEKS:
				rv = TWENTY_ONE_POINT_O;
				break;

			case SCHEDULE_TYPE_ONCE_EVERY_FOUR_WEEKS:
				rv = TWENTY_EIGHT_POINT_O;
				break;
		}

	}  // of if we could get the support information from the station group and the schedule is enabled

	return( rv );
}

/* ---------------------------------------------------------- */
static BOOL_32 get_substitute_et_value( const float pone_days_worth_of_et, const float pdaily_limit, float *psubstitute_amount_ptr )
{
	// 11/5/2015 ajv : When functions are declared as static, the compiler may inline the
	// functions and optimize out floating-point constant literals from the calculations. This
	// can result in a division-by-0 error. In order to resolve this, we declare the constant
	// literal as volatile which tells the compile to not optimize the value.
	static const volatile float TWENTY_PERCENT = 0.20F;
	
	static const volatile float TEN_THOUSAND = 10000.0F;

	// ----------

	ET_TABLE_ENTRY	et_entry;

	float	tmp_et_fl;

	float	substitute_20_percent_limit_fl;

	BOOL_32	substitute_have_one_bool;

	UNS_32	i;

	// ----------

	// 8/17/2012 rmd : Keep compiler happy about uninitialized variable.
	*psubstitute_amount_ptr = 0.0F;

	// Let's secure a gage reading to replace the historical numbers in the table. Some people
	// can't share everyday and using the historical in place of the gage may give very
	// different results - the two readings can be far apart from one another.
	substitute_have_one_bool = (false);

	// ----------

	substitute_20_percent_limit_fl = (pone_days_worth_of_et * TWENTY_PERCENT);

	// ----------

	// Since the ET table rolls at the DailyET_StartOfDay, we always start with index 1, not
	// index 0.
	for( i = 1; i < 5; ++i )	// Go back 4 days
	{
		if( WEATHER_TABLES_get_et_table_entry_for_index( i, &et_entry ) == (true) )
		{
			// 7/22/2015 rmd : When developing the value to substitute accept any data from another
			// controller (shared) or from WeatherSense.
			if( (et_entry.status == ET_STATUS_FROM_ET_GAGE) || (et_entry.status == ET_STATUS_SHARED_FROM_THE_CENTRAL) || (et_entry.status == ET_STATUS_FROM_WEATHERSENSE) )
			{
				// Need to control the value of what we are using to substitue with. There have been
				// reported cases of a zero getting into the table from say a broken gage followed by the
				// central not sharing. This sets the stage for some dangerous substitutions - stuffing a
				// zero (or other low value) into multiple days of historicals.
				//
				// If the value is less than 20% of historical, it is not eligible for use in substitution.
				tmp_et_fl = ((float)et_entry.et_inches_u16_10000u / TEN_THOUSAND);

				if( (tmp_et_fl >= substitute_20_percent_limit_fl) && (tmp_et_fl < pdaily_limit) )
				{
					substitute_have_one_bool = (true);

					*psubstitute_amount_ptr = tmp_et_fl;

					break;
				}  
			}
		}
	}

	return( substitute_have_one_bool );
}

/* ---------------------------------------------------------- */
extern float WATERSENSE_get_et_value_after_substituting_or_limiting(	BOOL_32 const pfor_ftimes,
																		STATION_STRUCT const *const pss_ptr,
																		DATE_TIME_COMPLETE_STRUCT const *const pdtcs_ptr,
																		UNS_32 const pet_table_index,
																		BOOL_32 const plog_events )
{
	STATION_GROUP_STRUCT	*lstation_group_ptr;

	ET_TABLE_ENTRY	let_entry;

	float	lone_days_worth_of_et, ldaily_limit;

	float	lsubstitute_amount;

	float	rv;

	// ----------

	// 11/5/2015 ajv : Keep compiler happy about uninitialized variable.
	rv = 0.0F;

	// ----------
	
	lstation_group_ptr = NULL;
	
	if( !pfor_ftimes && (plog_events == IRRITIME_LOG_EVENTS) )
	{
		lstation_group_ptr = STATION_GROUP_get_group_with_this_GID( STATION_get_GID_station_group( (void*)pss_ptr ) );
	}

	// ----------
	
	if( WEATHER_TABLES_get_et_table_entry_for_index( pet_table_index, &let_entry ) == (true) )
	{
		rv = ((float)let_entry.et_inches_u16_10000u / 10000.0);

		// ----------

		// 8/16/2016 skc : Prevent mixing ET values from one month and historical from a different
		// month.
		if( pdtcs_ptr->__day == 1)
		{
			DATE_TIME_COMPLETE_STRUCT dt;
			UNS_32 D = pdtcs_ptr->date_time.D - 1; // move back a day
			DateAndTimeToDTCS( D, pdtcs_ptr->date_time.T, &dt);
			lone_days_worth_of_et = ( ET_DATA_et_number_for_the_month( dt.__month ) / (float)NumberOfDaysInMonth( dt.__month, dt.__year ) );
		}
		else // every day but the 1st of the month
		{
			lone_days_worth_of_et = ( ET_DATA_et_number_for_the_month( pdtcs_ptr->__month ) / (float)NumberOfDaysInMonth( pdtcs_ptr->__month, pdtcs_ptr->__year ) );
		}

		ldaily_limit = (lone_days_worth_of_et * (WEATHER_get_percent_of_historical_cap_100u() / 100.0) );

		// ----------

		// Check if it's necessary to substitute the ET value.
		if( (let_entry.status == ET_STATUS_HISTORICAL) && (get_substitute_et_value( lone_days_worth_of_et, ldaily_limit, &lsubstitute_amount ) == (true)) )
		{
			rv = lsubstitute_amount;

			// The alert line was structured to accept the old us_100u values and to avoid that change
			// keep on using them
			if( (plog_events == IRRITIME_LOG_EVENTS) && (lstation_group_ptr != NULL) )
			{
				Alert_ET_Table_substitution_100u( nm_GROUP_get_name( lstation_group_ptr ), (pdtcs_ptr->date_time.D - (pet_table_index + 1)), (let_entry.et_inches_u16_10000u / 100), (UNS_32)(lsubstitute_amount * 100.0) );
			}
		}
		else
		if( rv > ldaily_limit )
		{
			// 9/21/2012 rmd : Gee. We used to test if the reading was from the gage or the central or
			// weather sense to decide if it was eligible to cap. I've thought about this. And it seems
			// we're allowing the poor guy at the controller to hand edit his value to something
			// potentially bad. That's called letting your customer step in a pile of you know what. So
			// hey the limit is the limit. If he wants a lot of irrigation he's using the wrong tool.
			// Use percent adjust or something else.
			rv = ldaily_limit;

			if( (plog_events == IRRITIME_LOG_EVENTS) && (lstation_group_ptr != NULL) )
			{
				Alert_ET_Table_limited_entry_100u( nm_GROUP_get_name( lstation_group_ptr ), (pdtcs_ptr->date_time.D - (pet_table_index + 1)), (let_entry.et_inches_u16_10000u / 100), (UNS_32)(ldaily_limit * 100.0) );
			}
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the daily average ET. This process takes into account ET averaging to determine
 * the days since the last irrigation and also performs any substitution of historical
 * values or limiting of real-time ET values.
 * 
 * IMPORTANT - The CALLERS of this function must have made the tests that the program for
 * the month in question is scheduled to run and is in Daily ET.
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex to ensure
 *  	  the station whose values are being read isn't deleted or changed during this
 *  	  process.
 *
 * @executed Executed within the context of the TD check task when a start time is crossed.
 * 
 * @param pss_ptr A pointer to the station.
 * 
 * @param pdtcs_ptr A pointer to the date and time this function was called.
 * 
 * @param plog_events True if the ET limiting and substitution alerts should be generated;
 * otherwise, false.
 * 
 * @return float The average daily ET since the last irrigation. In inches.
 *
 * @author 7/9/2012 Adrianusv
 *
 * @revisions
 *    7/9/2012 Initial release
 */
extern float nm_WATERSENSE_average_et_in_this_irrigation_period(	BOOL_32 const pfor_ftimes,
																	STATION_STRUCT const *const pss_ptr,
																	UNS_32 const pdays_in_this_period,
																	DATE_TIME_COMPLETE_STRUCT const *const pdtcs_ptr,
																	BOOL_32 const plog_events )
{
	UNS_32	lday;

	float	rv;

	const volatile float ZERO_POINT_ZERO = 0.0;

	// --------

	rv = ZERO_POINT_ZERO;
	
	// 4/8/2016 rmd : Protect against a potential divide by 0.
	if( pdays_in_this_period > 0 )
	{
		for( lday = 0; lday < pdays_in_this_period; ++lday )
		{
			// 2/15/2018 rmd : Index 0 in the ET table is the entry 'in process' sort of speak. It's
			// loaded with an historical value, and should generally be thought of as tomorrows ET.
			rv += WATERSENSE_get_et_value_after_substituting_or_limiting( pfor_ftimes, pss_ptr, pdtcs_ptr, (lday + 1), plog_events );
		}
	
		rv = (rv / (float)pdays_in_this_period);
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*
This was the VERY beginning of an attempt to be more precise when predicting the run time in 
the future. I was going to build a table of the dates involved in the irrigation period in
question, and then in a second function add the ET to use to each table entry. I think right
now we ALWAYS use the ET TABLE when figuring ET, even for future dates. And the historical 
we use is based upon a fixed date, not a day by day figuring.

Anyway after awhile, my head was spinning, and I've abandoned the idea. We passed the 
WaterSense test with the present code, and I think the majority of the time the present code
works well. And I think it works fine for actual irrigation, it's the future irrigations 
that concern me i.e. ftimes and/or budgets.
 
typedef struct { 
	UNS_32	date;
	
	UNS_32	et_inches;

} IRRIGATION_PERIOD_DATES_AND_ET_STRUCT;

// 2/22/2018 rmd : 28 is the maximum number of days in any irrigation period
static IRRIGATION_PERIOD_DATES_AND_ET_STRUCT dates_and_et[ 28 ];

static float nm_load_array_with_irrigation_period_dates( BOOL_32 pfor_ftimes, STATION_STRUCT *pss_ptr, STATION_GROUP_STRUCT *const psgs_ptr, FT_STATION_STRUCT* pft_station_ptr, const DATE_TIME_COMPLETE_STRUCT *pdtcs_ptr )
{
	RUN_TIME_CALC_SUPPORT_STRUCT	support;

	float	rv;
	
	BOOL_32		loaded;
	
	const volatile float ZERO_POINT_ZERO = 0.0;

	const volatile float ONE_POINT_O = 1.0;

	const volatile float TWO_POINT_O = 2.0;

	const volatile float THREE_POINT_O = 3.0;

	const volatile float TWENTY_ONE_POINT_O = 21.0;

	const volatile float TWENTY_EIGHT_POINT_O = 28.0;

	// ----------

	rv = ZERO_POINT_ZERO;

	// ----------
	
	if( pfor_ftimes )
	{
		// 2/22/2018 rmd : Tests for non-NULL ft_station_group_ptr.
		loaded = FTIMES_FUNCS_load_run_time_calculation_support( pft_station_ptr->station_group_ptr, &support );
	}
	else
	{
		// 4/7/2016 rmd : Get the support settings we need. If the psgs_ptr parameter seems invalid
		// this function will return (false) in which case we should return 0 days in the period.
		loaded = SCHEDULE_get_run_time_calculation_support( psgs_ptr, &support );
	}

	// ----------
	
	// 2/22/2018 rmd : If we are at the start time we use todays date. If we are past the start
	// time 


	// ----------
	
	if( loaded && support.schedule_enabled ) 
	{
		switch( support.schedule_type )
		{
			case SCHEDULE_TYPE_EVERY_DAY:
				break;

			case SCHEDULE_TYPE_EVEN_DAYS:
				break;

			case SCHEDULE_TYPE_ODD_DAYS:
				break;

			case SCHEDULE_TYPE_EVERY_OTHER_DAY:
				break;

			case SCHEDULE_TYPE_WEEKLY_SCHEDULE:
				break;

			case SCHEDULE_TYPE_EVERY_THREE_DAYS:
			case SCHEDULE_TYPE_EVERY_FOUR_DAYS:
			case SCHEDULE_TYPE_EVERY_FIVE_DAYS:
			case SCHEDULE_TYPE_EVERY_SIXTH_DAYS:
			case SCHEDULE_TYPE_EVERY_SEVEN_DAYS:
			case SCHEDULE_TYPE_EVERY_EIGHT_DAYS:
			case SCHEDULE_TYPE_EVERY_NINE_DAYS:
			case SCHEDULE_TYPE_EVERY_TEN_DAYS:
			case SCHEDULE_TYPE_EVERY_ELEVEN_DAYS:
			case SCHEDULE_TYPE_EVERY_TWELVE_DAYS:
			case SCHEDULE_TYPE_EVERY_THIRTEEN_DAYS:
			case SCHEDULE_TYPE_EVERY_FOURTEEN_DAYS:
				break;

			case SCHEDULE_TYPE_ONCE_EVERY_THREE_WEEKS:
				break;

			case SCHEDULE_TYPE_ONCE_EVERY_FOUR_WEEKS:
				break;
		}
	} 

	return( rv );
}
*/
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/**
 * Called at the start time detect for IN_LIST_FOR_MANUAL and
 * IN_LIST_FOR_PROGRAMMED_IRRIGATION. This functions job is to to eliminate any programmed
 * time or cycle time that is less than or equal to 3 minutes, the minimum required run time
 * for WaterSense.
 * 
 * If the total seconds is 0 this is okay. Because 0 mod anything is 0. So there will be no
 * adjustment.
 *
 * @task_execution Executed within the context of the TD_CHECK task when a start time is
 * detected.
 */
extern UNS_32 WATERSENSE_make_min_cycle_adjustment(	BOOL_32 const pfor_ftimes,
													UNS_32 *ptotal_seconds_ptr,
													const UNS_32 pcycle_seconds,
													STATION_HISTORY_RECORD *pshr_rip_ptr )
{
	UNS_32	remainder_seconds;

	// 7/17/2015 ajv : Store the amount of time that was thrown away so it can be added back
	// into the next irrigation as left-over irrigation.
	UNS_32	rv;

	// ----------

	rv = 0;

	// ----------
	
	if( !pfor_ftimes && (pcycle_seconds == 0) )
	{
		Alert_Message( "WaterSense: cycle=0 (out or range)" );
	}
	else
	{
		// 7/8/2015 ajv : If the total run time or the cycle time is not greater than 3-minutes,
		// throw away the time. Otherwise, check to see if there's a cycle that will run for less
		// than 3-minutes and, if so, discard that cycle.
		if( (*ptotal_seconds_ptr <= (WATERSENSE_MINIMUM_RUN_MINUTES * 60)) || (pcycle_seconds <= (WATERSENSE_MINIMUM_RUN_MINUTES * 60)) )
		{
			rv = *ptotal_seconds_ptr;

			// We are going to throw away the time by setting the amount to irrigate to 0.
			*ptotal_seconds_ptr = 0;
		}
		else
		{
			remainder_seconds = ( (*ptotal_seconds_ptr) % (pcycle_seconds) );

			if( (remainder_seconds > 0) && (remainder_seconds <= (WATERSENSE_MINIMUM_RUN_MINUTES * 60)) )
			{
				// 7/8/2015 ajv : There will be a cycle less than the required 3-minutes. Therefore, deduct
				// that time from the total run time so eliminate that cycle all-together.
				*ptotal_seconds_ptr -= remainder_seconds;

				// 7/17/2015 ajv : The only time we collect left-over irrigation is when we throw away time
				// because a cycle is 3-minutes or less. This is added back in next time the station tries
				// to irrigate to ensure we refill the moisture bucket.
				rv = remainder_seconds;

				// 2/22/2018 rmd : When we call it for finish times we call it with a NULL pointer.
				if( !pfor_ftimes && (pshr_rip_ptr != NULL) )
				{
					pshr_rip_ptr->pi_flag.watersense_min_cycle_eliminated_a_cycle = (true);
				}

			}
		}
	}

	// ----------

	return( rv );
}
			
/* ---------------------------------------------------------- */
/**
 * Returns the run-time for a particular station.
 *   - When in minutes, the total_run_minutes_10u value is not touched within this function
 *   - When in Daily ET, we calculate the run-time based on the WaterSense calculation.
 * 
 * @warning There IS cause to be concerned with processing time here. Consider a start time
 * for a chain with 768 stations where all 768 stations are going to run! Think about that!
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex to ensure
 * the station whose values are being read isn't deleted or changed during this process.
 *
 * @executed Executed within the context of the TD check task when a start time is crossed.
 */
extern float nm_WATERSENSE_calculate_run_minutes_for_this_station(	BOOL_32 pfor_ftimes,
																	STATION_STRUCT *pss_ptr,
																	STATION_GROUP_STRUCT *const psgs_ptr,
																	FT_STATION_STRUCT* pft_station_ptr,
																	const DATE_TIME_COMPLETE_STRUCT *pdtcs_ptr,
																	const float pdistribution_uniformity_100u )
{
	float	rv;

	float	days_in_irri_period;

	float	average_daily_et;

	float	precip_rate, crop_coeff_100u;

	float	etc;

	float	ldu;
	
	BOOL_32	attempt_calculation;

	BOOL_32	uses_daily_et;
	
	const volatile float ZERO_POINT_ZERO = 0.0;

	const volatile float TEN_POINT_ZERO = 10.0;

	const volatile float SIXTY_POINT_ZERO = 60.0;

	const volatile float ONE_HUNDRED_POINT_ZERO = 100.0;

	const volatile float ONE_HUNDRED_THOUSAND_POINT_ZERO = 100000.0;

	// ----------

	rv = ZERO_POINT_ZERO;
	
	// ----------
	
	attempt_calculation = (true);

	if( pfor_ftimes == FOR_FTIMES )
	{
		if( pft_station_ptr == NULL ) attempt_calculation = (false);
	}
	else
	{
		// 4/30/2018 rmd : For the non-ftimes case.
		if( psgs_ptr == NULL ) attempt_calculation = (false);

		if( pss_ptr == NULL ) attempt_calculation = (false);

		// 4/30/2018 rmd : Guard against using a NULL pss_ptr.
		if( attempt_calculation )
		{
			if( !STATION_station_is_available_for_use( pss_ptr ) ) attempt_calculation = (false);
		}
	}
	
	// ----------
	
	// 4/8/2016 rmd : If the station is not assigned to a station group (a valid configuration)
	// I suppose possibly the psgs_ptr could be NULL. But I think there are other checks in
	// place made by the caller. In any case we cover that possibility here. And check the
	// station pointer is not NULL and that the station is in use. If not we return a 0.0 run
	// time.
	if( attempt_calculation )
	{
		if( pfor_ftimes )
		{
			uses_daily_et = pft_station_ptr->station_group_ptr->et_in_use;
		}
		else
		{
			uses_daily_et = WEATHER_get_station_uses_daily_et( pss_ptr );
		}
		
		// ----------
		
		if( uses_daily_et )
		{
			/*
			// 2/21/2018 rmd : I think the figuring of the average_daily_et is somewhat flawed,
			// especially when the irrigation period marches ahead into the future, and crosses a month
			// boundary. We are not using the right table values and et values. I believe at the time of
			// irrigation this is all sorted out and works correctly. It is when we are trying to
			// predict future amounts that there are some subtle flaws. BUT I'm not going to fix them
			// now (this means they will likely never be fixed).
			
			// 2/22/2018 rmd : THE BEGINNING OF MY ATTEMPT TO FIX. ABANDONED AS IT GOT COMPLICATED, AND 
			DEVIATING FROM TASK AT HAND. First load an array of up to 28, with dates indicating the
			dates in the // present irrigation period. These can be past, present, or future dates. 
			nm_load_array_with_irrigation_period_dates( BOOL_32 pfor_times, STATION_STRUCT 
			*pss_ptr, STATION_GROUP_STRUCT *const psgs_ptr, FT_STATION_STRUCT* pft_station_ptr, const 
			 DATE_TIME_COMPLETE_STRUCT *pdtcs_ptr );
			*/
			
			// ----------
			
			// 2/15/2018 rmd : Convert the DU from a percentage to a decimal value.
			ldu = (pdistribution_uniformity_100u / ONE_HUNDRED_POINT_ZERO);
			
			// ----------
			
			// 4/8/2016 rmd : This function can return 0.0 days if there are no water days set in a
			// weekly schedule, or a fractional amounts of days when et averaging is considered, or
			// whole days.
			days_in_irri_period = nm_WATERSENSE_days_in_this_irrigation_period( pfor_ftimes, pss_ptr, psgs_ptr, pft_station_ptr, pdtcs_ptr );

			// 4/8/2016 rmd : When figuring the average ET over the period use the rounded number of
			// days. REMEMBER days in period can be a fractional amount with ET AVERAGING enabled.
			average_daily_et = nm_WATERSENSE_average_et_in_this_irrigation_period( pfor_ftimes, pss_ptr, (UNS_32)(roundf(days_in_irri_period)), pdtcs_ptr, IRRITIME_DONT_LOG_EVENTS );

			// Get the precipitation rate in inches per minute rather than inches per hour (which is how
			// we store it).
			if( pfor_ftimes )
			{
				precip_rate = ((float)pft_station_ptr->station_group_ptr->precip_rate_in_100000u / ONE_HUNDRED_THOUSAND_POINT_ZERO);
				
				// 12/11/2018 dmd : Bug found in crop coeff calculation, when month index is December. This
				// led to Ftimes not working when ET enabled for Decemeber. The months array is 0-based and
				// hence to get the correct crop coeff for that month, we need to index off of (month - 1)
				crop_coeff_100u = ((float)pft_station_ptr->station_group_ptr->crop_coefficient_100u[ pdtcs_ptr->__month - 1]);
			}
			else
			{
				precip_rate = ((float)STATION_GROUP_get_station_precip_rate_in_per_hr_100000u( pss_ptr ) / ONE_HUNDRED_THOUSAND_POINT_ZERO);

				crop_coeff_100u = STATION_GROUP_get_station_crop_coefficient_100u( pss_ptr, (UNS_32)pdtcs_ptr->__month );
			}

			etc = ( (crop_coeff_100u / ONE_HUNDRED_POINT_ZERO) * average_daily_et );

			// ----------
			
			// 4/8/2016 rmd : A final check against ZEROS. To prevent divide by zeros or other insanity.
			if( (days_in_irri_period > ZERO_POINT_ZERO) && (etc > ZERO_POINT_ZERO) && (precip_rate > ZERO_POINT_ZERO) && (ldu > ZERO_POINT_ZERO) )
			{
				rv = ( (etc * days_in_irri_period) / (precip_rate / SIXTY_POINT_ZERO) );
				
				rv = (rv / ldu);
			}
		}
		else
		{
			// 4/8/2016 rmd : When running by minutes.
			if( pfor_ftimes )
			{
				rv = ((float)pft_station_ptr->user_programmed_seconds / SIXTY_POINT_ZERO);	
			}
			else
			{
				rv = (STATION_get_total_run_minutes_10u( pss_ptr ) / TEN_POINT_ZERO);
			}
		}
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns the adjust run-time for a particular station based on the station adjust factor
 * (if in ET) and/or percent adjust.
 * 
 * @warning There IS cause to be concerned with processing time here. Consider a start time
 * for a chain with 768 stations where all 768 stations are going to run! Think about that!
 * 
 * @mutex Calling function must have the list_program_data_recursive_MUTEX mutex to ensure
 * the station whose values are being read isn't deleted or changed during this process.
 *
 * @executed Executed within the context of the TD check task when a start time is crossed.
 */
extern float nm_WATERSENSE_calculate_adjusted_run_time(	BOOL_32	pfor_ftimes,
														STATION_STRUCT *pss_ptr,
														FT_STATION_STRUCT *pft_station_ptr,
														float prun_time_min,
														const UNS_32 pet_factor_100u,
														const UNS_32 ppercent_adjust_factor_100u )
{
	float	rv;

	BOOL_32	is_in_use;
	
	BOOL_32	uses_daily_et;
	
	const volatile float ZERO_POINT_ZERO = 0.0;

	const volatile float ZERO_POINT_ONE = 0.1;

	const volatile float ONE_HUNDRED_POINT_ZERO = 100.0;

	// ----------
	
	// 4/8/2016 rmd : The default return run time if the station is not in use.
	rv = ZERO_POINT_ZERO;
	
	// ----------
	
	if( pfor_ftimes )
	{
		// 2/22/2018 rmd : By definition it is in use and physically available, otherwise it
		// wouldn't be in our ftimes station array. We tested for this when loading the stations
		// into the array.
		is_in_use = (true);
		
		uses_daily_et = pft_station_ptr->station_group_ptr->et_in_use;
	}
	else
	{
		is_in_use = STATION_station_is_available_for_use( pss_ptr );
		
		uses_daily_et = WEATHER_get_station_uses_daily_et( pss_ptr );
	}


	if( is_in_use )
	{
		if( uses_daily_et )
		{
			prun_time_min = ( (float)(pet_factor_100u / ONE_HUNDRED_POINT_ZERO) * prun_time_min );
		}

		// ----------
		
		// Compute run time using the percent adjust factor
		rv = ( prun_time_min * (float)(ppercent_adjust_factor_100u / ONE_HUNDRED_POINT_ZERO) );

		// ----------
		
		// Don't allow percent adjust to create a 0 runtime. Instead, if it's 0.0, use 0.1 instead
		//
		// 4/8/2016 rmd : The original comment is above. And I'm not sure who wrote or why -
		// probably carried over from the 2000e. But it makes no sense to me. We are returning a 0
		// if the station is not in use. And if the run time was driven to less than 6 seconds I say
		// we return a 0. The logic I found here was the other way around. If the run time was less
		// than 0.1 we made the run time 0.1. Not sure why we would do that. ADDITIONALLY what would
		// happen if we entered the function with a run time of 0.0? We would set it to 0.1. That
		// seems plain wrong to me.
		if( rv < ZERO_POINT_ONE )
		{
			rv = ZERO_POINT_ZERO;
		}
	}

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Generates the ET Table Limited and Substituted alert lines AFTER a scheduled irrigation
 * start time has been crossed and it's been decided the station will run.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the TD CHECK task when a start time is
 * crossed and at least one station is using Daily ET.
 * 
 * @param pss_ptr A pointer to the station. This pointer is expected to be valid. If a NULL
 * pointer is passed in, an alert is generated.
 * 
 * @param pdtcs_ptr A pointer to the date and time this function was called. This pointer is
 * expected to be valid. If a NULL pointer is passed in, an alert is generated.
 *
 * @author 7/12/2012 Adrianusv
 *
 * @revisions
 *   2/21/2013 Added check to make sure station pointer is not NULL.
 */
extern void IRRITIME_make_ETTable_substitute_and_limit_alerts_after_we_know_program_ran(	STATION_GROUP_STRUCT const *const psgs_ptr,
																							STATION_STRUCT const *const pss_ptr,
																							DATE_TIME_COMPLETE_STRUCT const *const pdtcs_ptr )
{
	float	days_in_irri_period;
	
	if( (psgs_ptr != NULL) && (pss_ptr != NULL) && (pdtcs_ptr != NULL) )
	{
		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		// Where this function is called from we already KNOW we irrigated a station so there's no
		// need to test if the program will run. However, we do need to verify that the station's
		// using Daily ET.
		if( WEATHER_get_station_uses_daily_et( (STATION_STRUCT *const)pss_ptr ) )
		{
			// 4/8/2016 rmd : This function can return 0.0 days, or fractional amounts of days, or whole
			// days.
			days_in_irri_period = nm_WATERSENSE_days_in_this_irrigation_period( NOT_FOR_FTIMES, (STATION_STRUCT *const)pss_ptr, (STATION_GROUP_STRUCT *const)psgs_ptr, NULL, pdtcs_ptr );

			// 4/8/2016 rmd : When figuring the average ET over the period use the rounded number of
			// days. REMEMBER days in period can be a fractional amount with ET AVERAGING enabled.
			nm_WATERSENSE_average_et_in_this_irrigation_period( NOT_FOR_FTIMES, pss_ptr, (UNS_32)(roundf(days_in_irri_period)), pdtcs_ptr, IRRITIME_LOG_EVENTS );
		}

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}
	else
	{
		Alert_func_call_with_null_ptr();
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

