/*  file = dynamic_manifold_recorder.h  05/21/08  ajv  */

/* ---------------------------------- */

#ifndef _INC_DMC_RECORDER_H
#define _INC_DMC_RECORDER_H

/* ---------------------------------- */
/* ---------------------------------- */

// Limit the number of records to keep us from taking the largest memory block 
// 
#define DMC_RECORDER_MAX_RECORDS 200

#define	MV_STATE_closed			0
#define	MV_STATE_open			1
#define	MV_STATE_not_in_use		2

/* ---------------------------------- */
/* ---------------------------------- */

typedef struct {

	DATE_TIME   dt;

	unsigned char   num_stations_on_uc;

	unsigned char   level_1_mv_state_uc;
	unsigned char   level_2_mv_state_uc;
	unsigned char   level_3_mv_state_uc;

	unsigned short  level_1_avg_flow_us;
	unsigned short  level_2_avg_flow_us;
	unsigned short  level_3_avg_flow_us;

	unsigned short  level_1_trip_point_us;
	unsigned short  level_2_trip_point_us;

	unsigned short  trip_point_up_us;
	unsigned short  trip_point_down_us;

	unsigned short  system_flowrate_us;

	unsigned short  expected_flowrate_us;

} NEW_DMC_RECORDER_RECORD;	// 28 bytes!


typedef struct {

	unsigned char   allocated;

	unsigned char   *original_allocation;

	// ring buffer pointers
	//
	unsigned char   *next_available;

	unsigned char   *first_used;

} DMC_RECORDER_CONTROL_STRUCT;


extern DMC_RECORDER_CONTROL_STRUCT dmc_rcs;

/* ---------------------------------- */
/* ---------------------------------- */

void init_dmc_recorder( void );

void dmc_recorder_inc_pointer( unsigned char **ppdmcr );

void dmc_recorder_add( unsigned char pnum_stations_on,
					   unsigned char plevel_1_mv_state_uc,
					   unsigned char plevel_2_mv_state_uc,
					   unsigned char plevel_3_mv_state_uc,
					   float plevel_1_avg_flow_fl,
					   float plevel_2_avg_flow_fl,
					   float plevel_3_avg_flow_fl,
					   unsigned short plevel_1_trip_point_us,
					   unsigned short plevel_2_trip_point_us,
					   unsigned short ptrip_point_up_us,
					   unsigned short ptrip_point_down_us,
					   unsigned short psystem_flowrate_us,
					   unsigned short pexpected_flowrate_us );

/* ---------------------------------- */
/* ---------------------------------- */

#endif

/* ---------------------------------- */
/* ---------------------------------- */
/* ---------------------------------- */
/* ---------------------------------- */

