/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_ET_DATA_H
#define _INC_ET_DATA_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define NO_OF_STATES	(13)
#define NO_OF_COUNTIES	(65)
#define NO_OF_CITIES	(154)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// The maximum length of 24 was derived by looking at the longest City, County,
// and State names and selecting the longest of the three. For reference, they
// are as follows:
// 
//   City: Winchester-on-the-Severn (24)
//   County: Northumberland			(14)
//   State: Rhode Island			(12)
// 
#define	MAX_REFERENCE_ET_NAME_LENGTH	(24)

#define MAX_COUNTIES	(20)			// The highest number of counties within a state
#define MAX_CITIES		(15)			// The highest number of cities within a county

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	char county[ MAX_REFERENCE_ET_NAME_LENGTH ];

	UNS_32 state_index;

} COUNTY_INFO;

typedef struct
{
	char city[ MAX_REFERENCE_ET_NAME_LENGTH ];

	UNS_32 county_index;

	// 6/18/2014 ajv : Allocate one ET value for each month of the year.
	UNS_32 ET_100u[ 12 ];

} CITY_INFO;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern const char States[ NO_OF_STATES ][ MAX_REFERENCE_ET_NAME_LENGTH ];

extern const COUNTY_INFO Counties[ NO_OF_COUNTIES ];

extern const CITY_INFO ET_PredefinedData[ NO_OF_CITIES ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

extern float ET_DATA_et_number_for_the_month( const UNS_32 pmonth1_12 );

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

