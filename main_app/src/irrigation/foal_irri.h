/*  file = foal_irri.h              10/27/00 2011.08.01  rmd  */

/* ---------------------------------------------------------- */

#ifndef _INC_FOAL_IRRI_H
#define _INC_FOAL_IRRI_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"lpc_types.h"

#include	"foal_defs.h"

#include	"switch_input.h"

#include	"battery_backed_vars.h"

#include	"tpmicro_data.h"

#include	"walk_thru.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef	_MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// derived from number of controller by the reasoning that each contact with an irri machine
// potentially can restart the timer and between contacts we don't want the timer to expire
// so...notice the 5000 + .... we can be sloppy on the rain switch cause it doesn't matter
// if it stays in effect for say 10 or 15 seconds after it actually goes out of effect
// (in contrast the master valve timeout (implemented in a similiar way but the timer is kept 
// at the irri machines) must be quite a bit tighter cause it is important to close the master
// valve quickly after the last station turn off - OR IS IT? for the pump it is - OR IS IT?
//
// used to be derived from NOCIC along with controllers we can cantact per second
// now I've just flat set it to 20 seconds
//
// and now with the introduction of the programmable input (sic) and the awareness of 10 controller
// chains actually in the field with multiple SR's and repeators it needs to be smarter and tied to NOCIC
//
#define SIC_RECORD_APPEARANCE_TIMEOUT_MILLISECONDS	(FoalFlow.NOCIC * 4000)


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 9/24/2013 rmd : Because the short record on its own does not contain the information
	// needed to set the flags.
	UNS_32									box_index_0;

	TERMINAL_SHORT_OR_NO_CURRENT_STRUCT		srs;

} FOAL_TO_IRRI_TERMINAL_SHORT_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation( void );


void init_battery_backed_foal_irri( const BOOL_32 pforce_the_initialization );


extern void FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list( BOOL_32 pfor_ftimes, void *pstation_list_element );


extern IRRIGATION_LIST_COMPONENT* nm_nm_FOAL_if_station_is_ON_turn_it_OFF( MIST_LIST_HDR_TYPE_PTR plist_hdr, IRRIGATION_LIST_COMPONENT *const pilc_ptr, UNS_32 const paction_reason );

extern IRRIGATION_LIST_COMPONENT* nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( MIST_LIST_HDR_TYPE_PTR plist_hdr, IRRIGATION_LIST_COMPONENT *const pilc_ptr, UNS_32 const paction_reason );



extern UNS_32 FOAL_IRRI_load_sfml_for_distribution_to_slaves( UNS_32 pcontact_index, UNS_8 **pdata_ptr );


extern void nm_FOAL_add_to_action_needed_list( IRRIGATION_LIST_COMPONENT *pilc, UNS_32 paction_reason );


extern void FOAL_IRRI_set_no_current_flag_in_station_history_line_if_appropriate( UNS_32 pcontroller_index, UNS_32 pstation_number );

// ----------

extern void FOAL_IRRI_remove_all_instances_of_this_station_from_the_irrigation_lists( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, const UNS_32 paction_reason );


extern void FOAL_IRRI_remove_all_stations_ON_at_this_box_from_the_irrigation_lists( const UNS_32 pcontroller_index, const UNS_32 paction_reason );


extern void FOAL_IRRI_remove_this_station_for_this_reason_from_the_irrigation_lists( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, const UNS_32 preason_in_list, const UNS_32 paction_reason );


extern void FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists( const UNS_32 pcontroller_index, const UNS_32 paction_reason );


extern void FOAL_IRRI_remove_all_stations_from_the_irrigation_lists( const UNS_32 paction_reason );



#define REMOVE_FOR_ALL_REASONS (true)

#define REMOVE_ONLY_FOR_THE_REASON_SPECIFIED (false)

extern BOOL_32 FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists( BY_SYSTEM_RECORD const *const pbsr, UNS_32 const pfor_this_reason, BOOL_32 pfor_all_reasons, UNS_32 const paction_reason );

// ----------

extern BOOL_32 FOAL_extract_clear_mlb_gids( UNS_8 **pucp_ptr );

extern UNS_32 FOAL_IRRI_load_mlb_info_into_outgoing_token( UNS_8 **mlb_info_data_ptr );


extern UNS_32 FOAL_IRRI_buildup_action_needed_records_for_token( UNS_8 **pdata_ptr );


extern void FOAL_IRRI_maintain_system_timers( void );

// ----------

extern UNS_32 FOAL_IRRI_translate_alert_actions_to_flow_checking_group( BIG_BIT_FIELD_FOR_ILC_STRUCT *pbbf_ptr );

extern BOOL_32 FOAL_IRRI_we_test_flow_when_ON_for_this_reason( UNS_32 const preason_in_list );

// ----------

extern UNS_32 FOAL_IRRI_return_stop_date_according_to_stop_time( DATE_TIME const *const pcurrent_datetime, UNS_32 const pstop_time );


extern void TDCHECK_at_1Hz_rate_check_for_irrigation_schedule_start( const DATE_TIME_COMPLETE_STRUCT *const pdtcs_ptr );

// ----------

extern BOOL_32 FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list( UNS_32 preason, UNS_32 phighest_reason );

extern BOOL_32 FOAL_IRRI_there_are_other_flow_check_groups_already_ON( BIG_BIT_FIELD_FOR_ILC_STRUCT *pbbf_ptr, UNS_32 *parray_ptr );

extern BOOL_32 FOAL_IRRI_there_is_more_than_one_flow_group_ON( UNS_32 *parray_ptr );

// ----------

extern void FOAL_IRRI_maintain_irrigation_list( const DATE_TIME *const pdate_time );

// --------

typedef struct
{
	// 8/31/2012 rmd : Only one station at a time can be sent to the master. This is used on the
	// irri side to enable the key for the user. Once pressed this is (true) until the next
	// token_resp is sent. The user should see a pop up indicating to try-again after their last
	// station has turned ON.
	BOOL_32	in_use;
	
	// --------

	// 8/30/2012 rmd : The details necessary to start the test for a particular station.
	// Remember any controller could request to TEST any station in the network.
	UNS_32	box_index_0;
	
	UNS_32	station_number;
	
	UNS_32	time_seconds;
	
	BOOL_32	set_expected;
	
} TEST_AND_MOBILE_KICK_OFF_STRUCT;

extern void FOAL_IRRI_initiate_a_station_test( TEST_AND_MOBILE_KICK_OFF_STRUCT *tkos, const UNS_32 preason_in_list );

// ----------

extern void FOAL_IRRI_start_a_walk_thru( UNS_32 pwalk_thru_gid );

// ----------

#define		MANUAL_WATER_A_STATION		(0)
#define		MANUAL_WATER_A_PROGRAM		(1)
#define		MANUAL_WATER_ALL			(2)

typedef struct
{
	BOOL_32	in_use;

	// --------

	UNS_32	manual_how;

	// --------

	UNS_32	box_index_0;

	UNS_32	station_number;

	UNS_32	manual_seconds;

	// --------

	UNS_32	program_GID;

} MANUAL_WATER_KICK_OFF_STRUCT;

extern void FOAL_IRRI_initiate_manual_watering( MANUAL_WATER_KICK_OFF_STRUCT *mwkos );

// ----------

typedef struct
{
	BOOL_32		in_use;

	// ----------

	UNS_32		mvor_action_to_take;

	UNS_32		mvor_seconds;

	// ----------

	UNS_32		initiated_by;

	// ----------

	UNS_32		system_gid;

} MVOR_KICK_OFF_STRUCT;

extern void FOAL_IRRI_initiate_or_cancel_a_master_valve_override( const UNS_32 psystem_gid, const UNS_32 pmvor_action_to_take, const UNS_32 mvor_seconds, const /*INITIATED_VIA_ENUM*/ UNS_8 pinitiated_by );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

