/*   file = station_history_data.c           08.02.2010  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memset
#include	<string.h>

#include	"station_history_data.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"controller_initiated.h"

#include	"cs_mem.h"

#include	"d_process.h"

#include	"epson_rx_8025sa.h"

#include	"flash_storage.h"

#include	"r_station_history.h"

#include	"watersense.h"

#include	"bithacks.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

COMPLETED_STATION_HISTORY_STRUCT		station_history_completed;	// Held in SDRAM and saved to a file in the GENERAL STORAGE flash device.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/27/2012 rmd : Support for displaying the station history and station report data. This
// is a pointer to a dynamic block of memory that is acquired when we enter either report
// display screen and freed when we leave the respective screen. The memory holds a ptr to
// each of the records being displayed. We count on the C startup code to intialize this to
// a NULL.
static STATION_HISTORY_RECORD		*(*station_history_report_ptrs)[];
 
// ----------
 
// filenames ARE ALLOWED 32 CHARS ... truncated if exceeds this limit
const char STATION_HISTORY_FILENAME[] =	"STATION_HISTORY_RECORDS";
 
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
 
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called as part of the station_preserves initial verification during
    startup. Also called at each roll time for the station history data to restart the
    station history rip in the station preserves.

    @CALLER_MUTEX_REQUIREMENTS The caller is to be holding the appropriate MUTEX. Given
    where the partiuclar record is located. Either battery backed SRAM or in the
    SDRAM.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the APP_STARTUP task. And from the TD_CHECK
    task. At the programmed irrigation start time on a water day.

	@RETURN (none)

	@ORIGINAL 2012.06.04 rmd

	@REVISIONS (none)
*/
extern void nm_init_station_history_record( STATION_HISTORY_RECORD *pshr_ptr )
{
	// Flood the record with zeros.
	memset( pshr_ptr, 0, sizeof(STATION_HISTORY_RECORD) );
}

/* ---------------------------------------------------------- */
static void nm_init_station_history_records( void )
{
	// 1/23/2015 rmd : Function caller is to be holding the station_history_completed MUTEX.
	// This function is only called within the context of the STARTUP task via the initial file
	// check.

	// ----------

	UNS_32	i;

	// ----------

	// 1/23/2015 rmd : This sets all the control variables to 0 or (false). And the roll time to
	// midnight. Which is where we want it.
	memset( &station_history_completed.rdfb, 0x00, sizeof(REPORT_DATA_FILE_BASE_STRUCT) );
	
	// ----------
	
	for( i=0; i<(STATION_HISTORY_MAX_RECORDS); i++ )
	{
		nm_init_station_history_record( &(station_history_completed.shr[ i ]) );
	}
		
	// ----------
	
	// 8/15/2014 rmd : The variable that control which records to send are saved with the file.
	// And that is important to recognize. When the ACK is rcvd the file must be saved. In order
	// to lock in those sent if there were a power failure. The nature of this is if there were
	// a power fail after the ACK was rcvd but before the file was saved we would send the lines
	// twice. Which is okay. We just don't want to do that all the time.
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 1/22/2015 rmd : REVISION 1 - this was a file re-definition that affected all report data
// type files. And resulted in a complete intialization of the file. The goal being to
// restructure the file contents to better support report record revisions in the future.
// This update, from revision 0, triggered a complete file intialization.

// 8/12/2015 rmd : REVISION 2 - this revision kept the record size the same but reduced the
// number of records stored to 5 per station. Being station history records this reflects 5
// irrigations per station. Not days. This move was prompted by the fact that the CS3000
// sends its records almost real time to the cloud. And that most all controllers have an
// internet link. And that most users ever look at station history at the controller. And if
// so for a diagnostic purpose and 5 records is enough for that.
//
// There is also a risk to keeping more records. The file write time is larger. And I mean
// it takes many seconds to write this file. During which time we are exposed to record loss
// if there were a power failure. There is a window where the file is assumed to have
// completed its write where if a power outage occurs records can be lost.
// 
// 7/17/2015 ajv : VERSION 3 - introduced 2-byte moisture balance percentage and 2-byte
// expansion.

typedef struct
{
	UNS_32			record_start_time;

	UNS_32			pi_first_cycle_start_time;

	UNS_32			pi_last_cycle_end_time;

	UNS_32			pi_seconds_irrigated_ul;

	float			pi_gallons_irrigated_fl;
	
	STATION_HISTORY_BITFIELD		pi_flag;

	UNS_16			GID_irrigation_system;

	UNS_16			GID_irrigation_schedule;

	UNS_16			record_start_date;			//	Date associated with record start. Should occur each and every day!

	UNS_16			pi_first_cycle_start_date;	//	Date stamp for the start of the first cycle
	
	UNS_16			pi_last_cycle_end_date;		//	Date stamp when the irrigation ended

	UNS_16			pi_total_requested_minutes_us_10u;

	UNS_32			pi_watersense_requested_seconds;

	UNS_16			pi_rain_at_start_time_before_working_down__minutes_10u;

	UNS_16			pi_rain_at_start_time_after_working_down__minutes_10u;

	UNS_16			pi_last_measured_current_ma;

	UNS_16			pi_flow_check_share_of_actual_gpm;
	
	UNS_16			pi_flow_check_share_of_hi_limit_gpm;
	
	UNS_16			pi_flow_check_share_of_lo_limit_gpm;

	UNS_8			station_number;
	
	UNS_8			pi_number_of_repeats;

	UNS_8			box_index_0;
	
} STATION_HISTORY_RECORD_REV_0;

// ----------

#define	STATION_HISTORY_LATEST_FILE_REVISION		(3)

// ----------

const UNS_32 station_history_revision_record_sizes[ STATION_HISTORY_LATEST_FILE_REVISION + 1 ] =
{
	// 1/22/2015 rmd : The original base file. Which is going to be standardized to our new
	// report file format. The restructuring to the standard format creates revision 1.
	sizeof( STATION_HISTORY_RECORD_REV_0 ),

	// 1/22/2015 rmd : The move to version 1 is to restructure the file contents to or new
	// standardized report file format. There was no report record size change.
	sizeof( STATION_HISTORY_RECORD_REV_0 ),

	// 8/12/2015 rmd :  Move to version 2 made no record size change. This was simply to reduce
	// the number of records in the file.
	sizeof( STATION_HISTORY_RECORD_REV_0 ),

	// 7/17/2015 ajv : Version 3 introduces the 2-byte moisture balance percentage.
	sizeof( STATION_HISTORY_RECORD )
};

// ----------

const UNS_32 station_history_revision_record_counts[ STATION_HISTORY_LATEST_FILE_REVISION + 1 ] =
{
	// 1/22/2015 rmd : The original base file. Which is going to be standardized to our new
	// report file format. The restructuring to the standard format creates revision 1.
	STATION_HISTORY_MAX_RECORDS_V0_V1,

	// 1/22/2015 rmd : The move to version 1 is to restructure the file contents to or new
	// standardized report file format. There was no report record count change.
	STATION_HISTORY_MAX_RECORDS_V0_V1,

	// 8/12/2015 ajv : Reduced the number of available records.
	STATION_HISTORY_MAX_RECORDS,

	// 7/17/2015 ajv : No change to the number of records in version 3.
	STATION_HISTORY_MAX_RECORDS
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/11/2015 rmd : One hour after a record closes transmit it. And this would be a maximum
// time because the function to start this does not restart the timer if it is already
// running. This is important else it may keep restarting and be a long time before a record
// is sent to the web. Another consideration with this time is not to make it too short
// otherwise we end up with more and more transmissions that aren't needed. A one hour lag
// is plenty reasonable for the user when it comes to station history records.
#define		CONTROLLER_INITIATED_STATION_HISTORY_TRANSMISSION_TIMER_SECONDS		(60*60)

// 8/15/2014 rmd : Store the timer outside the file structure. It is a dynamic memory
// allocation. And not desireable to preserve through a power failure.
static xTimerHandle		station_history_ci_timer;
	
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
	
/* ---------------------------------------------------------- */
static void nm_station_history_updater( UNS_32 pfrom_revision )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only when
	// the file revision is changed. The caller is to be holding the station history completed
	// records recursive_MUTEX.

	// ----------

	UNS_32	i;

	// ----------
	
	if( pfrom_revision == STATION_HISTORY_LATEST_FILE_REVISION )
	{
		Alert_Message_va( "STA_HSTRY file unexpd update %u", pfrom_revision );	
	}
	else
	{
		Alert_Message_va( "STA_HSTRY file update : to revision %u from %u", STATION_HISTORY_LATEST_FILE_REVISION, pfrom_revision );

		// ----------
		
		if( pfrom_revision == 0 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 0 to REVISION 1. Moving
			// up from revision 0 requires a complete file restart. The file content is invalid due to
			// the restructuring to the standardized report file format.
			nm_init_station_history_records();
			
			// ----------
			
			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 1 to 2 changes if
			// needed.
			pfrom_revision += 1;
		}
		
		// ----------
		
		if( pfrom_revision == 1 )
		{
			// 8/12/2015 rmd : There is no work to do when moving from REVISION 1 to REVISION 2. And
			// actually on the update to revision 2 this updater function will not even execute because
			// we reduced the file record count. See the find or create function to understand.

			pfrom_revision += 1;
		}

		if( pfrom_revision == 2 )
		{
			// 8/18/2015 ajv : This is the work to do when moving from REVISION 2 to REVISION 3.
			
			for( i = 0; i < STATION_HISTORY_MAX_RECORDS; ++i )
			{
				// 7/17/2015 ajv : Initialize the new moisture balance percentage to 50%
				station_history_completed.shr[ i ].pi_moisture_balance_percentage_after_schedule_completes_100u = (INT_16)(WATERSENSE_MAXIMUM_MOISTURE_BALANCE_ALLOWED_FOR_IRRI * 100 * 100);

				// 7/17/2015 ajv : And why not initialize the 2-byte expansion that's left over.
				station_history_completed.shr[ i ].expansion_u16 = 0;
			}

			pfrom_revision += 1;
		}

		// ----------

		/*
		if( pfrom_revision == 2 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 2 to REVISION 3.
				


			pfrom_revision += 1;
		}
		*/
	}

	// ----------

	// 1/29/2015 rmd : Well after the updater has run the pfrom_revision is supposed to equal
	// the latest revision. By design of this update process.
	if( pfrom_revision != STATION_HISTORY_LATEST_FILE_REVISION )
	{
		Alert_Message( "STA_HSTRY updater error" );
	}
}

/* ---------------------------------------------------------- */
extern void init_file_station_history( void )
{
	// 1/23/2015 rmd : Function caller is to be holding the station_history_completed MUTEX.
	// This function is only called within the context of the STARTUP task via the initial file
	// check.

	// ----------
	
	FLASH_FILE_find_or_create_reports_file(	FLASH_INDEX_1_GENERAL_STORAGE,
											STATION_HISTORY_FILENAME,
											STATION_HISTORY_LATEST_FILE_REVISION,

											&station_history_completed,

											station_history_revision_record_sizes,
											station_history_revision_record_counts,
											sizeof( COMPLETED_STATION_HISTORY_STRUCT ),

											station_history_completed_records_recursive_MUTEX,

											&nm_station_history_updater,
											&nm_init_station_history_records,

											FF_STATION_HISTORY );

	// ----------
	
	// 9/5/2014 rmd : Because we have included the controller initiated control variables in the
	// saved file there is some consideration here. First of all the first_to_send is assumed to
	// be valid and up to date. I think there are no real holes here. If the file didn't save
	// after closing and adding a line (power failure) the first to send may not be accurate. Or
	// if after receiving the data ACK from the web_app there is a power failure before the file
	// is saved we would send dups to the web_app. But that causes no harm. The problem to watch
	// out for is that the pending_first_send_in_use is saved into the file set (true). That
	// could happen I suppose. So that variable in particular on STARTUP should be set to
	// (false).
	station_history_completed.rdfb.pending_first_to_send_in_use = (false);
}

/* ---------------------------------------------------------- */
extern void save_file_station_history( void )
{
	// 8/7/2015 rmd : Take the recursive MUTEX to protect the battery backed holding structure.
	// It is an accurate rendering of the SDRAM station history completed structure at this
	// point. And the file system takes a copy of the SDRAM in preparation to write the file.
	// And then we wipe the battery backed holding. During these operation we must protect
	// against any modifications to either the SDRAM copy or the battery backed holding. So the
	// MUTEX effectively freezes them for us.
	xSemaphoreTakeRecursive( station_history_completed_records_recursive_MUTEX, portMAX_DELAY );

	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE,
															STATION_HISTORY_FILENAME,
															STATION_HISTORY_LATEST_FILE_REVISION,
															&station_history_completed,
															sizeof( station_history_completed ),
															station_history_completed_records_recursive_MUTEX,
															FF_STATION_HISTORY );
															
	xSemaphoreGiveRecursive( station_history_completed_records_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
static void station_history_ci_timer_callback( xTimerHandle pxTimer )
{
	// 10/21/2013 rmd : Remember this is executed within the context of the high priority timer
	// task. And is executed when it it time to send some station history records. Which is
	// normally 15 minutes after a record is closed.
	CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_station_history, NULL, CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD, CI_POST_TO_BACK );
}

/* ---------------------------------------------------------- */
extern void STATION_HISTORY_start_ci_timer_if_it_is_not_running__seconds( UNS_32 const ptimer_seconds )
{
	// 8/27/2014 rmd : On startup the station history ci timer pointer is NULL. If so let's
	// create the timer. This is a convienient way to create the timer before use.
	if( station_history_ci_timer == NULL )
	{
		station_history_ci_timer = xTimerCreate( (const signed char*)"", MS_to_TICKS(ptimer_seconds * 1000), FALSE, (void*)NULL, station_history_ci_timer_callback );
			
		if( station_history_ci_timer == NULL )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "Timer NOT CREATED" );
		}
	}
	
	// ----------
	
	// 8/27/2014 rmd : If it exists do what we came for. Start the timer.
	if( station_history_ci_timer != NULL )
	{
		// 8/15/2014 rmd : Only if it is not running. Which means effectively a maximum of 15
		// minutes after a record is closed it will be sent.
		if( xTimerIsTimerActive( station_history_ci_timer ) == pdFALSE )
		{
			// 8/15/2014 rmd : Start the timer to trigger the controller initiated send.
			xTimerChangePeriod( station_history_ci_timer, MS_to_TICKS(ptimer_seconds * 1000), portMAX_DELAY );
		}
	}
}

/* ---------------------------------------------------------- */
extern void nm_STATION_HISTORY_inc_index( UNS_32 *pindex_ptr )
{
	// 8/15/2014 rmd : This function is used by the controller initiated task as well as here
	// within this file when closing a record (that is in the context of the TD_CHECK task).

	*pindex_ptr += 1;
	
	if( *pindex_ptr >= STATION_HISTORY_MAX_RECORDS )
	{
		// 6/26/2012 rmd : Okay so we have gone past the end of the array. Wrap around.
		*pindex_ptr = 0;
	}
}

/* ---------------------------------------------------------- */
extern void nm_STATION_HISTORY_increment_next_avail_ptr( void )
{
	nm_STATION_HISTORY_inc_index( &station_history_completed.rdfb.index_of_next_available );

	if( station_history_completed.rdfb.index_of_next_available == 0 )
	{
		// 6/26/2012 rmd : Okay so we have gone past the end of the array and wrapped. Set the
		// wrapped indication (true).
		station_history_completed.rdfb.have_wrapped = (true);
	}
	
	// ----------
	
	// 8/14/2014 rmd : If we have bumped into the first to send move it along too.
	if( station_history_completed.rdfb.index_of_next_available == station_history_completed.rdfb.first_to_send )
	{
		nm_STATION_HISTORY_inc_index( &station_history_completed.rdfb.first_to_send );
	}

	// 10/21/2013 rmd : Also if next available has landed on the pending_first_to_send it means
	// that during the CI session we have actually written so many lines we wrapped all the way
	// around. In other words we wrote more lines than we hold during the session (which during
	// normal operation is impossible - a debug function used during development could do this).
	// So move that index too.
	if( station_history_completed.rdfb.index_of_next_available == station_history_completed.rdfb.pending_first_to_send )
	{
		nm_STATION_HISTORY_inc_index( &station_history_completed.rdfb.pending_first_to_send );
	}
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This function is to be used in conjunction with the accompanying 'get
    first' function. Following the get first the user may repeatedly call the get next
    function until a NULL is returned. That will signal that he has seen all the completed
    lines. The entire get first and get next sequence is to be encapsulated within a take
    and give mutex pair for the station report data mutex. The pointer argument points to
    the last record seen.
	
    @CALLER_MUTEX_REQUIREMENTS The caller is to be holding the station history completed
    records mutex during the entire iterative process calling this function again and again.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the DISPLAY PROCESSING task. When drawing the
    report data to the screen. May also be used in the COMM_MNGR as part of send the report
    records to the central.

    @RETURN If the last record (oldest) has been encountered will return a NULL. Otherwise a
    pointer to the next older record. The records are returned in reverse chronological
    order.

	@ORIGINAL 2012.06.29 rmd 

	@REVISIONS (none)
*/
extern STATION_HISTORY_RECORD *nm_STATION_HISTORY_get_previous_completed_record( STATION_HISTORY_RECORD *pshr_ptr )
{
	STATION_HISTORY_RECORD	*rv;

	if( (pshr_ptr < &(station_history_completed.shr[ 0 ])) || (pshr_ptr >= &(station_history_completed.shr[ STATION_HISTORY_MAX_RECORDS ])) )
	{
		// 6/26/2012 rmd : Out of range. Do not use.
		rv = NULL;
	}
	else
	if( station_history_completed.rdfb.have_returned_next_available_record == (true) )
	{
		// 6/29/2012 rmd : If we have wrapped all the way around and are back to where we started
		// then we have returned all possible records.
		rv = NULL;	
	}
	else
	{
		if( pshr_ptr == &(station_history_completed.shr[ 0 ]) )
		{
			if( station_history_completed.rdfb.have_wrapped == (true) )
			{
				// 6/26/2012 rmd : Back up at the top.
				rv = &(station_history_completed.shr[ (STATION_HISTORY_MAX_RECORDS - 1) ]);
			}
			else
			{
				rv = NULL;
			}
		}
		else
		{
			rv = pshr_ptr;
	
			rv -= 1;
		}
	}
	
	if( rv == &(station_history_completed.shr[ station_history_completed.rdfb.index_of_next_available ]) )
	{
		// 6/29/2012 rmd : If we've returned records all the way back to the start indicate this is
		// the last one. So the next call to this function will return a NULL.
		station_history_completed.rdfb.have_returned_next_available_record = (true);
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Call this function first. In preparation to retrieve a ptr to each of the
    completed lines by repeatedly calling the accompanying 'get next' function. If there are
    no lines this function returns a NULL. Otherwise a ptr to the first (newest) completed
    line.
	
    @CALLER_MUTEX_REQUIREMENTS The caller is to be holding the station history completed
    records mutex during this and the entire iterative process calling the get next
    function.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the DISPLAY PROCESSING task. When drawing the
    report data to the screen. May also be used in the COMM_MNGR as part of send the report
    records to the central.

    @RETURN The return value points to the most recently completed record if there is one.
    Otherwise NULL - meaning there are no completed records.

	@ORIGINAL 2012.06.29 rmd 

	@REVISIONS (none)
*/
extern STATION_HISTORY_RECORD *nm_STATION_HISTORY_get_most_recently_completed_record( void )
{
	STATION_HISTORY_RECORD	*rv;
	
	// 6/29/2012 rmd : And set the logic to support the repetitive extraction calls.
	station_history_completed.rdfb.have_returned_next_available_record = (false);
	
	rv = nm_STATION_HISTORY_get_previous_completed_record( &(station_history_completed.shr[ station_history_completed.rdfb.index_of_next_available ]) );
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Using the station and serial number argument counts how many matching lines
    are in the database. And during that process maintains an array of ptrs to each of the
    records.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the DISPLAY task. In preparation for screen
    draw and once per second as part of a screen update.

    @RETURN The matching number of lines. Will always be at least 1 as the rip always
    exists.

	@ORIGINAL 2012.07.27 rmd

	@REVISIONS (none)
*/
extern UNS_32 STATION_HISTORY_fill_ptrs_and_return_how_many_lines( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0 )
{
	UNS_32	rv;
	
	rv = 0;

	// 8/1/2012 rmd : Besides running through the completed records finding matches we should be
	// concerned with the report_ptrs memory allocation. Suppose one were to "change" screens
	// right at the time td_check was running through filling the array with ptrs. That could be
	// a big problem is the the change screen routine "freed" the memory and set poc_report_ptr
	// to NULL before this function completed.
	xSemaphoreTakeRecursive( station_history_completed_records_recursive_MUTEX, portMAX_DELAY );

	// ----------------------

	if( station_history_report_ptrs == NULL )
	{
		// 7/27/2012 rmd : Oh just grab the maximum size ever necessary. Otherwise we get into the
		// potential of having to free the memory and re-allocing as the number of lines grows. That
		// could be done but I decided to just grab the worst case up front. Also we'd have to go
		// through twice once to learn how many. And once to fill the array.
		// 7/27/2012 rmd : Oh and the story gets even more complicated. Cause if the database is
		// full and another line is added the count of the number of lines for this station may stay
		// the same but the ptrs to the lines are now different? Not sure about that thought.
		station_history_report_ptrs = mem_malloc( STATION_HISTORY_MAX_RECORDS * sizeof(void *) );
	}
	
	// ----------------------

	UNS_32	lindex;

	STATION_PRESERVES_get_index_using_box_index_and_station_number( pbox_index_0, pstation_number_0, &lindex );

	// 12/4/2014 ajv : If the RIP is valid to show (meaning that the record has been opened and
	// there is data available), include it in the display as the very first record.
	//
	// 8/12/2015 rmd : If the "needs to be saved flag" is set that implies the record has been
	// added to the SDRAM array of completed records. So in that case do not show else we would
	// show the record twice - once from the rip and once from the completed array.
	if( (station_preserves.sps[ lindex ].station_history_rip.pi_flag.rip_valid_to_show) && (!(station_preserves.sps[ lindex ].spbf.station_history_rip_needs_to_be_saved)) )
	{
		(*station_history_report_ptrs)[ rv++ ] = &(station_preserves.sps[ lindex ].station_history_rip);
	}
	
	// ----------------------

	STATION_HISTORY_RECORD	*lrecord;

	lrecord = nm_STATION_HISTORY_get_most_recently_completed_record();

	while( lrecord != NULL )
	{
		if( (lrecord->box_index_0 == pbox_index_0) && (lrecord->station_number == pstation_number_0) )
		{
			(*station_history_report_ptrs)[ rv++ ] = lrecord;
		}

		// 7/27/2012 rmd : So as to not write beyond our allocated space.
		if( rv >= STATION_HISTORY_MAX_RECORDS )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "REPORTS: why so many records?" );
			
			break ;
		}

		lrecord = nm_STATION_HISTORY_get_previous_completed_record( lrecord );
	}

	// ----------------------

	xSemaphoreGiveRecursive( station_history_completed_records_recursive_MUTEX );

	// ----------------------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the string representing the passed-in station history
 * flags.
 * 
 * @executed Executed within the context of the display processing task when
 *           each scroll line is drawn.
 * 
 * @param pstr A pointer to the character array to copy the text into.
 * @param size_of_str The size of the character array.
 * @param pflag The flow recording flag bit field to parse.
 * 
 * @return char* A pointer to the text string based upon the passed in value.
 *
 * @author 5/15/2012 Adrianusv
 *
 * @revisions
 *    5/15/2012 Initial release
 */
static char *STATION_HISTORY_get_flag_str( char *pstr, const UNS_32 size_of_str, const STATION_HISTORY_BITFIELD pflag, const UNS_8 pflag_2 )
{
	// 11/14/2014 rmd : Start with a NULL string.
	*pstr = 0x00;
	
	if( pflag.mlb_prevented_or_curtailed )
	{
		strlcat( pstr, "MLB,", size_of_str );
	}

	if( pflag.controller_turned_off )
	{
		strlcat( pstr, "OFF,", size_of_str );
	}

	if( pflag.mow_day )
	{
		strlcat( pstr, "Mow,", size_of_str );
	}

	if( pflag.rain_as_negative_time_prevented_irrigation )
	{
		strlcat( pstr, "Rain1,", size_of_str );
	}

	if( pflag.rain_table_R_M_or_Poll_prevented_or_curtailed )
	{
		strlcat( pstr, "Rain2,", size_of_str );
	}

	if( pflag.no_water_by_calendar_prevented )
	{
		strlcat( pstr, "NoW1,", size_of_str );
	}

	if( pflag.no_water_by_manual_prevented )
	{
		strlcat( pstr, "NoW2,", size_of_str );
	}

	if( pflag.two_wire_cable_problem )
	{
		strlcat( pstr, "2WC,", size_of_str );
	}

	if( pflag.two_wire_station_decoder_inoperative )
	{
		strlcat( pstr, "2WS,", size_of_str );
	}

	if( pflag.two_wire_poc_decoder_inoperative )
	{
		strlcat( pstr, "2WP,", size_of_str );
	}

	if( pflag.poc_short_cancelled_irrigation )
	{
		strlcat( pstr, "ShrtP,", size_of_str );
	}

	if( pflag.current_short )
	{
		strlcat( pstr, "Shrt,", size_of_str );
	}

	if( pflag.flow_high )
	{
		strlcat( pstr, "Hi,", size_of_str );
	}

	if( pflag.current_none )
	{
		strlcat( pstr, "NoC,", size_of_str );
	}

	if( pflag.rain_as_negative_time_reduced_irrigation )
	{
		strlcat( pstr, "Rain3,", size_of_str );
	}

	if( B_IS_SET( pflag_2, STATION_history_bit_budget_applied ) || B_IS_SET( pflag_2, STATION_history_bit_budget_reduction_limit ) )
	{
		strlcat( pstr, "Budget,", size_of_str );
	}

	if( pflag.mois_cause_cycle_skip )
	{
		strlcat( pstr, "Mois1,", size_of_str );
	}

	if( pflag.mvor_closed_prevented_or_curtailed )
	{
		strlcat( pstr, "MVOR,", size_of_str );
	}

	if( pflag.hit_stop_time )
	{
		strlcat( pstr, "StopT,", size_of_str );
	}

	if( pflag.stop_key_pressed )
	{
		strlcat( pstr, "StopK,", size_of_str );
	}

	if( pflag.switch_rain_prevented_or_curtailed )
	{
		strlcat( pstr, "SwRn,", size_of_str );
	}

	if( pflag.switch_freeze_prevented_or_curtailed )
	{
		strlcat( pstr, "SwFrz,", size_of_str );
	}

	if( pflag.wind_conditions_prevented_or_curtailed )
	{
		strlcat( pstr, "Wind,", size_of_str );
	}

	if( pflag.flow_low )
	{
		strlcat( pstr, "Low,", size_of_str );
	}

	if( pflag.current_low )
	{
		strlcat( pstr, "O1,", size_of_str );
	}

	if( pflag.current_high )
	{
		strlcat( pstr, "O2,", size_of_str );
	}

	if( pflag.flow_never_checked )
	{
		strlcat( pstr, "NotChk,", size_of_str );
	}

	if( pflag.mois_max_water_day )
	{
		strlcat( pstr, "MoisMax,", size_of_str );
	}

	// 8/11/2016 rmd : This flag is NORMALLY set true. The logic should be if the flag is not
	// set to show the NoData string.
	if( !(pflag.pi_flow_data_has_been_stamped) )
	{
		strlcat( pstr, "NoData,", size_of_str );
	}

	if( pflag.watersense_min_cycle_zeroed_the_irrigation_time )
	{
		strlcat( pstr, "WS1,", size_of_str );
	}

	if( pflag.watersense_min_cycle_eliminated_a_cycle )
	{
		strlcat( pstr, "WS2,", size_of_str );
	}

	// 11/14/2014 rmd : And wipe the final ",".
	pstr[ strlen(pstr) - 1 ] = 0x00;

	return( pstr );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Draws an individual scroll line for the Station Report. This is used as the
    callback routine for the GuiLib_ScrollBox_Init routine. And uses the array of pointers
    created in the STATION_REPORT_DATA_fill_ptrs_and_return_how_many_lines function.
	
    @CALLER_MUTEX_REQUIREMENTS Calling function must have the
    station_report_data_completed_records_recursive_MUTEX to ensure no records are added or
    changed during this process.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the DISPLAY processing task when the screen is
    drawn; within the context of the key processing task when the user navigates up or down
    the scroll box.

	@RETURN (none)

	@ORIGINAL 2012.07.27 rmd

	@REVISIONS (none)
*/
extern void STATION_HISTORY_draw_scroll_line( const INT_16 pline_index_0_i16 )
{
	const volatile float ONE_HUNDRED_THOUSAND = 100000.0;

	const volatile float ONE_HUNDRED = 100.0;

	const volatile float SIXTY = 60.0;

	float	MB_inches_beyond_50_percent_of_RZWWS, precip_rate_in_per_min;

	STATION_STRUCT	*lstation;

	// ----------

	// 8/1/2012 rmd : Taking this completed records MUTEX protects the array of ptrs from
	// changing if we happen to be viewing at the roll time. Additionally protect from the
	// memory being freed using this MUTEX.
	xSemaphoreTakeRecursive( station_history_completed_records_recursive_MUTEX, portMAX_DELAY );

	// 8/2/2012 rmd : Is the ptr NULL? Being careful. Cheap insurance code space wise.
	if( station_history_report_ptrs != NULL )
	{
		STATION_HISTORY_RECORD	*lrecord;
		
		lrecord = (*station_history_report_ptrs)[ pline_index_0_i16 ];
	
		// ----------------
		
		char	str_64[ 64 ];
	
		strlcpy( GuiVar_RptDate, GetDateStr( str_64, sizeof(str_64), lrecord->record_start_date, DATESTR_show_year_not, DATESTR_show_dow_not ), sizeof(GuiVar_RptDate) );

		// 9/4/2014 ajv : Case 3093: Historically we have shown the station's
		// first cycle time. However, Bob believes the user expects to see the
		// scheduled start time. Therefore, this was changed to use
		// record_start_time instead of pi_first_cycle_start_time.
		strlcpy( GuiVar_RptStartTime, TDUTILS_time_to_time_string_with_ampm( str_64, sizeof(str_64), lrecord->record_start_time, TDUTILS_DONT_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING ), sizeof(GuiVar_RptStartTime) );
	
		// ----------------
		
		GuiVar_RptCycles = lrecord->pi_number_of_repeats;
	
		GuiVar_RptScheduledMin = ((float)(lrecord->pi_total_requested_minutes_us_10u) / 10.0F);
	
		GuiVar_RptIrrigMin = ((float)(lrecord->pi_seconds_irrigated_ul) / 60.0F);
		GuiVar_RptIrrigGal = lrecord->pi_gallons_irrigated_fl;
	
		GuiVar_RptFlow = lrecord->pi_flow_check_share_of_actual_gpm;
	
		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		lstation = nm_STATION_get_pointer_to_station( lrecord->box_index_0, lrecord->station_number );

		if( WEATHER_get_station_uses_daily_et(lstation) == (true) )
		{
			// 3/14/2017 ajv : The balance beyond 50% is calculated by taking the current moisture
			// balance and subtracting 50% of the RZWWS.
			MB_inches_beyond_50_percent_of_RZWWS = lrecord->pi_moisture_balance_percentage_after_schedule_completes_100u - (STATION_MOISTURE_BALANCE_DEFAULT * (STATION_GROUP_get_soil_storage_capacity_inches_100u(lstation) / ONE_HUNDRED));

			// 3/15/2017 ajv : If the MB is over 50% and the flag to clear it has NOT been set,
			// calculate the accumulated rain.
			if( (MB_inches_beyond_50_percent_of_RZWWS > 0) && (nm_STATION_get_ignore_moisture_balance_at_next_irrigation_flag(lstation) == STATION_MOISTURE_BALANCE_CLEAR_IGNORE_FLAG) )
			{
				precip_rate_in_per_min = (float)((STATION_GROUP_get_station_precip_rate_in_per_hr_100000u(lstation) / ONE_HUNDRED_THOUSAND) / SIXTY);

				GuiVar_RptRainMin = (MB_inches_beyond_50_percent_of_RZWWS / precip_rate_in_per_min);
			}
			else
			{
				GuiVar_RptRainMin = 0.0F;
			}
		}
		else
		{
			GuiVar_RptRainMin = ((float)(lrecord->pi_rain_at_start_time_after_working_down__minutes_10u) / 10.0F);
		}

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

		GuiVar_RptLastMeasuredCurrent = ((float)lrecord->pi_last_measured_current_ma / 1000.0F);
	
		strlcpy( GuiVar_RptFlags, STATION_HISTORY_get_flag_str( (char *)str_64, sizeof(str_64), lrecord->pi_flag, lrecord->pi_flag2 ), sizeof(GuiVar_RptFlags) );
	}
	
	xSemaphoreGiveRecursive( station_history_completed_records_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern float STATION_HISTORY_get_rain_min( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0 )
{
	float	rv;

	UNS_32	lindex;

	STATION_PRESERVES_get_index_using_box_index_and_station_number( pbox_index_0, pstation_number_0, &lindex );

	xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

	rv = ((float)(station_preserves.sps[ lindex ].rain_minutes_10u) / 10.0F);

	xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern void STATION_HISTORY_set_rain_min_10u( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, const UNS_32 pvalue_10u, const UNS_8 pwho_initiated_it )
{
	UNS_32	lindex;

	STATION_PRESERVES_get_index_using_box_index_and_station_number( pbox_index_0, pstation_number_0, &lindex );

	xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

	if( station_preserves.sps[ lindex ].rain_minutes_10u != pvalue_10u )
	{
		station_preserves.sps[ lindex ].rain_minutes_10u = pvalue_10u;

		Alert_accum_rain_set_by_station( pbox_index_0, pstation_number_0, pvalue_10u, pwho_initiated_it );
	}

	xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void nm_STATION_HISTORY_close_and_start_a_new_record( UNS_32 const pstation_index )
{
	// 8/11/2015 rmd : Executed within the context of the TD_CHECK task. As part of general
	// station maintenance while a station is ON. Can also execute at the start time if a
	// station isn't going to run (NOW days for example).

	// 10/3/2014 rmd : REMEMBER the caller is to be holding the station preserves recursive
	// MUTEX. Because we are using and initializing the rip which is stored in the preserves.
	// And remember the caller should ONLY call this function 

	// ----------

	// 8/1/2012 rmd : As we are writing to the completed records base take the MUTEX.
	xSemaphoreTakeRecursive( station_history_completed_records_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	if( pstation_index >= STATION_PRESERVE_ARRAY_SIZE )
	{
		Alert_Message( "Station History close index OOR" );
	}
	else
	{
		// 6/26/2012 rmd : Move the rip into the completed.
		station_history_completed.shr[ station_history_completed.rdfb.index_of_next_available ] = station_preserves.sps[ pstation_index ].station_history_rip;
		
		nm_STATION_HISTORY_increment_next_avail_ptr();

		// ----------
		
		// 9/6/2016 rmd : DO NOT INITIALIZE THE STATION_PRESERVES RIP HERE. That is done at the
		// start time on a water day for the station. This has to do with what happens if there is a
		// power failure before the TWO HOURS save elapses. Well the flag you see being set tells
		// us, on a RESTART (i.e. power failure) , to AGAIN add the station preserves record to the
		// file array in SDRAM. And then save the file.
		//
		// 8/11/2015 rmd : Caller to be already holding the station preserves MUTEX. Make the flag
		// indicating the rip copy is in the SDRAM complete records structure and needs to be
		// saved.
		station_preserves.sps[ pstation_index ].spbf.station_history_rip_needs_to_be_saved = (true);
	}

	// ----------
	
	// 8/1/2012 rmd : Done with the requirement to hold the MUTEX. The file save activity
	// re-takes the MUTEX when needed.
	xSemaphoreGiveRecursive( station_history_completed_records_recursive_MUTEX );

	// ----------
	
	// 8/11/2015 rmd : In order to manage the number of station history file writes start this
	// timer with a long 2 hour delay. That is documented where the delay is defined. Note we
	// only start the timer if it is not running. The result of this is at most we can have 12
	// file writes per day. Probably typical number will be 4 or 5.
	FLASH_STORAGE_if_not_running_start_file_save_timer( FF_STATION_HISTORY, STATION_HISTORY_FILE_WRITE_DELAY_SECONDS );
	
	// ----------
	
	// 8/15/2014 rmd : Start the ci timer if it is not running.
	STATION_HISTORY_start_ci_timer_if_it_is_not_running__seconds( CONTROLLER_INITIATED_STATION_HISTORY_TRANSMISSION_TIMER_SECONDS );

	// ----------
	
	// 10/29/2013 ajv : If the user is viewing the Station History report when a
	// new line is added, force the scroll box to be updated.
	if( GuiLib_CurStructureNdx == GuiStruct_rptStationHistory_0 )
	{
		DISPLAY_EVENT_STRUCT	lde;

		lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
		lde._04_func_ptr = &FDTO_STATION_HISTORY_redraw_scrollbox;
		Display_Post_Command( &lde );
	}
}

/* ---------------------------------------------------------- */

#if ALERTS_ABILITY_TO_GENERATE_ALL_ALERTS_AND_REPORTS_FOR_DEBUG

/* ---------------------------------------------------------- */
extern void STATION_HISTORY_generate_lines_for_DEBUG( void )
{
	DATE_TIME_COMPLETE_STRUCT lcdt;

	UNS_32	lbox_index_0, lstation_num_0, lday_0;

	UNS_32	lindex;

	EPSON_obtain_latest_complete_time_and_date( &lcdt );

	xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

	lbox_index_0 = 0;
	//for( lbox_index_0 = 0; lbox_index_0 < MAX_CHAIN_LENGTH; lbox_index_0++ )
	{
		for( lstation_num_0 = 0; lstation_num_0 < (MAX_STATIONS_PER_CONTROLLER / 2); lstation_num_0++ )
		{
			STATION_PRESERVES_get_index_using_box_index_and_station_number( lbox_index_0, lstation_num_0, &lindex );

			for( lday_0 = 0; lday_0 < STATION_HISTORY_IRRIGATIONS_KEPT; lday_0++ )
			{
				station_preserves.sps[ lindex ].station_history_rip.box_index_0 =  lbox_index_0;
				station_preserves.sps[ lindex ].station_history_rip.station_number = lstation_num_0;

				station_preserves.sps[ lindex ].station_history_rip.record_start_date = (lcdt.date_time.D - STATION_HISTORY_IRRIGATIONS_KEPT + lday_0);
				station_preserves.sps[ lindex ].station_history_rip.record_start_time = lcdt.date_time.T;

				nm_STATION_HISTORY_close_and_start_a_new_record( lindex );
			}
		}
	}

	xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
}

#endif

/* ---------------------------------------------------------- */
extern void STATION_HISTORY_free_report_support( void )
{
	// 8/2/2012 rmd : We use the completed records MUTEX to also protect against changes to the
	// ptrs value. While it is in use (display work) we hold this same MUTEX. It certainly would
	// be a disaster to free the memory and NULL the ptr while in use. So take the same MUTEX to
	// prevent.
	xSemaphoreTakeRecursive( station_history_completed_records_recursive_MUTEX, portMAX_DELAY );

	if( station_history_report_ptrs != NULL )
	{
		mem_free( station_history_report_ptrs );
		
		// 7/27/2012 rmd : Beacuse the free doesn't set the ptr to NULL we do so. In order not to
		// try and free again as we move around screens.
		station_history_report_ptrs = NULL;
	}

	xSemaphoreGiveRecursive( station_history_completed_records_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

