/* file = bypass_operation.c   07/08/2014  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"bypass_operation.h"

#include	"string.h"

#include	"alerts.h"

#include	"flowsense.h"

#include	"tpmicro_comm.h"

#include	"app_startup.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
// 9/11/2014 rmd : NOTE - this file is set to NO OPTIMIZATION in the CROSSWORKS project.
// Because of at least one proven case, and possibly repeated cases, of bypass switching
// decision MATH ERRORS when this code is optimized. Due to an apparent GCC optimization
// bug.
//
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/14/2014 rmd : Set the following to (false) to suppress alert lines showing the details
// of the bypass operation.
#define	SHOW_BYPASS_INVALID_CONFIGURATION_LINES		(true)
#define	SHOW_BYPASS_OPERATION_LINES					(true)


BYPASS_STRUCT	bypass;

// ----------

// 7/15/2014 rmd : When we first open the largest stage we do need to wait before responding
// to the averaged flow before making a valve change decision. Because the average moves
// somewhat slowly (about 25 seconds to get where it is going) we cannot look at the average
// right away as it will still say 0. Combine this with the fact that the decoders only
// report flow once every 10 seconds and I decided to wait 30 seconds after beginning
// operation before deciding if the big one should remain open. Following that fist decision
// the NATURAL effect of the averaging allows us to second by second continue to make valve
// decisions even immediately following the prior decision. This number was 21 seconds for
// the 2000e - but it is a different system without decoders.
//
// Note for 2000e - With an impulse change in flow - 21 seconds later the weighted avg is
// 90% of the actual flow rate so I can argue that's all we need to wait. Remember why are
// we waiting - to prevent the state machine from reacting to the averaged flow (which is
// still climbing) and dropping back down a stage. This is a dwell time to say we will stay
// at this stage for at least this long.
//
// 10/3/2014 rmd : I have since decided I might as well make this match the number of
// readings kept. As that ensures we won't begin action following the switch to OPERATIONAL
// until we have at least a full buffer of NEW readings to make averages with.
#define BYPASS_SECONDS_TO_WAIT_AFTER_GOING_OPERATIONAL		(BYPASS_READINGS_IN_FLOW_AVG)

// ----------

// The following table shows the reference numbers that were used to
// come up with the minimum, maximum, and default flow rates below
//
//	 ------------------------------------------------
//	|            |    Suggested     | Velocity Flows |
//	| Flow Meter | Operating Range* |   @ 10 fps**   |
//	 ------------------------------------------------
//	| FM-1B      |      2-40        |      33.5      | 100 hz
//	| FM-1.25B   |      3-60        |      53.8      |  80 hz
//	| FM-1.5B    |      4-80        |      70.7      |  75 hz
//	| FM-1.5     |      5-100       |      70.7      |  69 hz
//	| FM-2B      |     10-100       |     111.0      |  36 hz
//	| FM-2       |     10-200       |     111.0      |  70 hz
//	| FM-3       |     20-300       |     242.3      |  36 hz
//	 ------------------------------------------------
//	  * suggested operating range obtained from Badger
//	    Meter's printed documentation
//	
//	 ** velocity flows obtained from appendix of Turf
//	    Irrigation Manual by James C. Watkins (Table
//	    5a Velocity Flows - SDR 21 PVC Pipe)

// 7/11/2014 rmd : Include +1 so it actually does include the last flow meter type.
// Currently the BERMAD.

// 9/10/2014 rmd : We've seen some problems if the 'right' numbers aren't used. For example
// if a bypass is made up of a 1B and a 3 inch. The 1B threshold was 25 gpm. If the flow
// rate was say 28 gpm the bypass would indeed switch to the 3 inch level. However that is
// at the low end for a 3 inch. And it would report a low enough flow to switch back down to
// the 1B (25 - 10% = 22.5 gpm). Such a flow rate (28gpm) cause the stages to flip back and
// forth. My first order solution is to come up with a different set of number that run each
// meter more up towards the end of its suggested operating range. Let's use 80 to 85% of
// the suggested operating range as the trip points.
//
// OLD TABLE
/*
const UNS_32 BYPASS_transition_flow_rates[ POC_FM_CHOICE_MAX + 1 ] =	{		0,		// none
																			   25,      // fm-1b
																			   45,  	// fm-1.25b
																			   70,  	// fm-1.5b
																			   80,  	// fm-1.5
																			  100,  	// fm-2b
																			  130,  	// fm-2
																			  250,  	// fm-3
																			  350,  	// fmbx
																			  200   	// bermad (untested! just a number picked by rmd)
																		};
*/
//
// NEW TABLE
const UNS_32 BYPASS_transition_flow_rates[ POC_FM_CHOICE_MAX + 1 ] =	{		0,		// none
																			   34,      // fm-1b
																			   51,  	// fm-1.25b
																			   68,  	// fm-1.5b
																			   85,  	// fm-1.5
																			   85,  	// fm-2b
																			  160,  	// fm-2
																			  250,  	// fm-3
																			  350,  	// fmbx
																			  200,   	// bermad 1 pulse/1 gal (untested! just a number picked by rmd)
																			  200   	// bermad 1 pulse/10 gal (untested! just a number picked by rmd)
																		};



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void init_bypass( void )
{
	UNS_32	iii, rrr;

	memset( &bypass, 0x00, sizeof(BYPASS_STRUCT) );
	
	// 7/10/2014 rmd : Because I'm not sure if you can load floating point with zeros using
	// memset set them explicitly.
	for( iii=0; iii<POC_BYPASS_LEVELS_MAX; iii++ )
	{
		bypass.latest_flow_rate[ iii ] = 0.0;

		for( rrr=0; rrr<BYPASS_READINGS_IN_FLOW_AVG; rrr++ )
		{
			bypass.flow_rate_history[ iii ][ rrr ] = 0.0;
		}
		
		bypass.final_avg[ iii ] = 0.0;
	}

	// 7/11/2014 rmd : An explicit reminder.
	bypass.linked = (false);

	// 7/14/2014 rmd : Because during the transitory building phase of the bypass manifold it
	// momentraily has no flow meters and maybe missing decoder serial numbers. Until the user
	// is done fully configuring. During this time the bypass state machine may try to operate.
	// And we want to suppress operation until the configuration is valid. But we only want to
	// alert about such at midnight.
	bypass.alert_about_invalid_configrations = (false);

	bypass.mode = BYPASS_MODE_passive_deenergized;
	
	// 7/11/2014 rmd : Bypass state is irrelevant. When transitions out of passive mode always
	// opens highest level.
}

/* ---------------------------------------------------------- */
static BOOL_32 bypass_configuration_is_valid( UNS_32 const plevels )
{
	// 7/14/2014 rmd : plevels is either a 2 or a 3.
	
	// ----------

	BOOL_32		rv;

	UNS_32		lll;

	POC_FM_CHOICE_AND_RATE_DETAILS	fm_choice_and_rate_details[ POC_BYPASS_LEVELS_MAX ];

	// ----------

	rv = (true);

	// ----------
	
	POC_get_fm_choice_and_rate_details( bypass.bpr->poc_gid, fm_choice_and_rate_details );
	
	// ----------

	for( lll=0; lll<plevels; lll++ )
	{
		// 7/11/2014 rmd : Both level two and three of a bypass are to be NC master valves. Let's
		// make sure starting at the mid sized level!
		if( lll >= 1 )
		{
			if( bypass.bpr->ws[ lll ].master_valve_type != POC_MV_TYPE_NORMALLY_CLOSED )
			{
				rv = (false);

				if( bypass.alert_about_invalid_configrations && SHOW_BYPASS_INVALID_CONFIGURATION_LINES )
				{
					Alert_Message_va( "BYPASS level %u not a NC MV", lll+1 );
				}
			}
		}
		
		// 7/14/2014 rmd : Also should have a decoder serial number set.
		if( bypass.bpr->ws[ lll ].decoder_serial_number == DECODER_SERIAL_NUM_DEFAULT )
		{
			rv = (false);

			if( bypass.alert_about_invalid_configrations && SHOW_BYPASS_INVALID_CONFIGURATION_LINES )
			{
				Alert_Message_va( "BYPASS level %u HAS NO DECODER", lll+1 );
			}
		}

		// 7/14/2014 rmd : And should have a flow meter.
		if( fm_choice_and_rate_details[ lll ].flow_meter_choice == POC_FM_NONE )
		{
			rv = (false);

			if( bypass.alert_about_invalid_configrations && SHOW_BYPASS_INVALID_CONFIGURATION_LINES )
			{
				// 8/21/2014 rmd : This is a normal state that exists after decoders are discovered and made
				// to be part of a bypass. During that time we get a whole pile of these alerts. Add to that
				// that if someone intentionally and temporarily turns off a flow meter (sets to none) the
				// alerts will be filled with this alert in a matter of minutes. So suppress.
				//Alert_Message_va( "BYPASS level %u HAS NO FLOW METER", lll+1 );
			}
		}
	}
	
	// ----------
	
	// 7/14/2014 rmd : Since we ran this function clear the alert variable. Till mid-night or a
	// new bypass link sets it once again.
	bypass.alert_about_invalid_configrations = (false);

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
static void verify_or_link_bypass_engine( void )
{
	// 7/11/2014 rmd : This function is called each time either a new flow meter reading arrives
	// or the one hertz marker arrives (both of course via a queue message). We roll through the
	// poc_preserves and find the first bypass that belongs to this box. And using it verify or
	// set the two pointer fields in the bypass engine structure.
	
	UNS_32	ppp, box_index;
	
	void	*lpoc;
	
	BOOL_32	found_one;
	
	// ----------
	
	found_one = (false);
	
	// 7/11/2014 rmd : Think of the whole bypass engine as running on the IRRI side. Only at the
	// box that physically owns the two-wire cable with the bypass decoders on it. So get our
	// own box index.
	box_index = FLOWSENSE_get_controller_index();

	// ----------
	
	for( ppp=0; ppp<MAX_POCS_IN_NETWORK; ppp++ )
	{
		if( (poc_preserves.poc[ ppp ].poc_type__file_type == POC__FILE_TYPE__DECODER_BYPASS) && (poc_preserves.poc[ ppp ].box_index == box_index) )
		{
			if( !found_one )
			{
				found_one = (true);

				lpoc = nm_POC_get_pointer_to_poc_with_this_gid_and_box_index( poc_preserves.poc[ ppp ].poc_gid, box_index );
				
				if( !bypass.linked || (bypass.lpoc != lpoc) || (bypass.bpr != &poc_preserves.poc[ ppp ]) )
				{
					// 7/11/2014 rmd : Then we need to link the bypass engine to the bypass poc. We do that by
					// first cleaning up the bypass structure and then setting the two pointers.
					Alert_Message( "Linking Bypass" );
	
					// 7/14/2014 rmd : Remember when we init the bypass sets the flag (true) to alert about
					// invalid configurations.
					init_bypass();
					
					bypass.lpoc = lpoc;
					
					bypass.bpr = &(poc_preserves.poc[ ppp ]);

					bypass.linked = (true);
				}
			}
			else
			{
				// 7/11/2014 rmd : A sanoty check.
				Alert_Message( "More than ONE BYPASS?" );
			}
		}
	}
	
	if( !found_one )
	{
		// 7/11/2014 rmd : Keep it cleaned up. Also this way the one hertz processing in this task
		// below knows to bail.
		init_bypass();	
	}
}

/* ---------------------------------------------------------- */
// 8/22/2014 rmd : Fighting a mother of a problem. The bypass stage decision making math
// DOES NOT WORK unless either the code is un-optimized. Or we force this function to
// "noinline". The optimizer will inline this function since it is a static and only called
// from one place. That's standard operating procedure for an optimizer. Another way to make
// itwork is to specify the function as an extern. That has the same end result though. That
// is not to allow inlining of the function.
// 8/27/2014 rmd : As noted below AJ also found an alternate way to go at this problem.
// Which I think I like better. The use of volatile constants that is.

static void receive_flow_reading_update( BYPASS_EVENT_QUEUE_STRUCT *pbeqs_ptr )
//extern void receive_flow_reading_update( BYPASS_EVENT_QUEUE_STRUCT *pbeqs_ptr )
//static void __attribute__((noinline)) receive_flow_reading_update( BYPASS_EVENT_QUEUE_STRUCT *pbeqs_ptr )
{
	// 8/25/2014 ajv : For some reason, when this function is declared as static, the compiler
	// is inlining this function and optimizing out the 100000.0 term from the flow rate
	// calculation, resulting in a division by 0. In order to resolve this, we declare the
	// constant literal as volatile which tells the compile to not optimize the value.

	const volatile float ONE_HUNDRED_THOUSAND = 100000.0;

	const volatile float FIVE_POINT_O = 5.0;

	const volatile float ZERO_POINT_ZERO = 0.0;

	// ----------

	BY_POC_RECORD	*lbpr_ptr;
	
	void			*lpoc;
	
	POC_DECODER_OR_TERMINAL_WORKING_STRUCT	*wds;
	
	UNS_32			this_decoders_level;

	POC_FM_CHOICE_AND_RATE_DETAILS	fm_choice_and_rate_details[ POC_BYPASS_LEVELS_MAX ];
	
	float	k_times_offset_term, k_times_freq_term;

	UNS_32	time_limit;
	
	float 	gpm_multiplier;
	
	// ----------
	
	// 7/11/2014 rmd : Test if we are linked to the preserves.
	verify_or_link_bypass_engine();

	// 7/11/2014 rmd : NOTE - if we received the queue message that invokes this function there
	// IS SUPPOSED to be a BYPASS poc for this box in the preserves and poc file. So proceed
	// with that assumption.
	
	// ----------
	
	// 7/10/2014 rmd : First verify that our bypass preserves pointer and poc file pointer are
	// valid. If not drop the reading.

	wds = NULL;
	
	lbpr_ptr = NULL;
	
	lpoc = NULL;
	
	this_decoders_level = 0;

	// ----------

	// 7/11/2014 rmd : NOTE - we are not utilizing the bypass.linked term here but instead
	// arriving at our own pointers and verifying the link ourselves. A sanity check approach.

	wds = POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters( FLOWSENSE_get_controller_index(), pbeqs_ptr->decoder_serial_number, &lbpr_ptr, &this_decoders_level );
	
	if( this_decoders_level >= POC_BYPASS_LEVELS_MAX )
	{
		Alert_Message( "Bypass: levels out of range" );	
	}

	// 7/10/2014 rmd : If successful find in the preserves see if we can find in the poc file.
	if( (this_decoders_level < POC_BYPASS_LEVELS_MAX) && (wds != NULL) && (lbpr_ptr != NULL) )
	{
		// 7/11/2014 rmd : Some sanity checks.
		lpoc = nm_POC_get_pointer_to_poc_with_this_gid_and_box_index( lbpr_ptr->poc_gid, lbpr_ptr->box_index );
		
		if( (lpoc != bypass.lpoc) || (lbpr_ptr != bypass.bpr) )
		{
			// 7/11/2014 rmd : Some sort of sync/linkage problem.
			Alert_Message( "BYPASS sync or linkage problem" );
		}
		else
		{
			// 7/11/2014 rmd : Use the reading. Remember this reading is fresh from the TPMICRO. We need
			// to obtain the K and O numbers and calculate the gpm.
			if( POC_get_fm_choice_and_rate_details( lbpr_ptr->poc_gid, fm_choice_and_rate_details ) )
			{
				// 7/11/2014 rmd : If the flow meter choice is NONE should we alert to that? For now not.
				if( fm_choice_and_rate_details[ this_decoders_level ].flow_meter_choice != POC_FM_NONE )
				{
					// 7/11/2014 rmd : Calculate the latest flow rate which will be used at the one hertz rate
					// to feed the bypass flow rate history array. Which in turn calucaltes the final working
					// average used to make the manifold transition decisions.
					
					// 1/15/2015 rmd : Bermad goes at the calculation differently. Cull them off.
					if( (fm_choice_and_rate_details[ this_decoders_level ].flow_meter_choice >= POC_FM_BERMAD_0150) && (fm_choice_and_rate_details[ this_decoders_level ].flow_meter_choice <= POC_FM_BERMAD_1000) )
					{
						// 1/16/2015 rmd : These flow meters use a 1 pulse per gallon or 10 pulse per gallon scheme.
						// Goal here is to calculate two values. The latest average flow rate and the latest gallons
						// for the accumulators.
						
						// 1/30/2015 rmd : To come up with a flow rate we look at the delta_4khz between the last 2
						// pulses and the time since the last pulse.

						if( (fm_choice_and_rate_details[ this_decoders_level ].reed_switch == POC_FM_REED_SWITCH_1_TO_1) || (fm_choice_and_rate_details[ this_decoders_level ].reed_switch == POC_FM_REED_SWITCH_1_TO_10) )
						{
							if( fm_choice_and_rate_details[ this_decoders_level ].reed_switch == POC_FM_REED_SWITCH_1_TO_1 )
							{
								time_limit = 6;
								
								gpm_multiplier = 60.0;
							}
							else
							{
								time_limit = 60;

								gpm_multiplier = 600.0;
							}
							
							// 1/30/2015 rmd : ONLY use the delta between pulses if it isn't zero.
							if( (pbeqs_ptr->fm_delta_between_last_two_fm_pulses_4khz>0) && (pbeqs_ptr->fm_seconds_since_last_pulse < time_limit) )
							{
								// 1/30/2015 rmd : The equation is 1 divided by seconds between last two pulses times the
								// gpm_multiplier. Number of seconds is 4khz count divided by 4000.
								bypass.latest_flow_rate[ this_decoders_level ] = ( (4000.0 / pbeqs_ptr->fm_delta_between_last_two_fm_pulses_4khz) * gpm_multiplier );
							}
							else
							{
								bypass.latest_flow_rate[ this_decoders_level ] = ZERO_POINT_ZERO;
							}
						}
						else
						{
							bypass.latest_flow_rate[ this_decoders_level ] = ZERO_POINT_ZERO;
							
							Alert_Message( "BYPASS : Unexpected Bermad Reed choice" );
						}
						
					}
					else
					{
						// 7/11/2014 rmd : ONLY if the reading is non-zero do we go ahead and compute the 5 second
						// gpm value. Because the equation produces a gpm even if the value is 0. And that would
						// ALWAYS show a flow rate. Incorrectly.
						if( pbeqs_ptr->fm_latest_5_second_count > 0 )
						{
							// 6/3/2014 rmd : At a one hertz rate the equation is : gpm = (K * count) + (K * offset)
							//
							// So if we take the count for the full 5 seconds and plug it into this equation we get the
							// gpm as if all that flow occurred in one second. So divide by 5 to get the average for the
							// 5 second window. BUT the K*O term is meant to be added in each second. So add that in 5
							// times before dividing the total by 5.
							k_times_freq_term = ( (fm_choice_and_rate_details[ this_decoders_level ].kvalue_100000u / ONE_HUNDRED_THOUSAND) * pbeqs_ptr->fm_latest_5_second_count );
							
							k_times_offset_term = ( FIVE_POINT_O * (fm_choice_and_rate_details[ this_decoders_level ].offset_100000u / ONE_HUNDRED_THOUSAND) * (fm_choice_and_rate_details[ this_decoders_level ].offset_100000u / ONE_HUNDRED_THOUSAND) );
							
							bypass.latest_flow_rate[ this_decoders_level ] = ( (k_times_freq_term + k_times_offset_term) / FIVE_POINT_O );
							
							// 9/12/2012 rmd : With a NEGATIVE offset (see the 1.5 PVC meter) if the pulse count is low
							// enough we can actually develop a negative flow rate here. That needs to be suppressed. So
							// the user doesn't see a negative flow rate and raise questions.
							if( bypass.latest_flow_rate[ this_decoders_level ] < ZERO_POINT_ZERO )
							{
								bypass.latest_flow_rate[ this_decoders_level ] = ZERO_POINT_ZERO;
							}
						}
						else
						{
							bypass.latest_flow_rate[ this_decoders_level ] = ZERO_POINT_ZERO;
						}
					}
				}
				else
				{
					// 1/15/2015 rmd : For the case of no flow meter choosen.
					bypass.latest_flow_rate[ this_decoders_level ] = ZERO_POINT_ZERO;
				}
				
			}
			else
			{
				Alert_Message( "BYPASS READING K & O error" );
			}
			
		}
		
	}
	else
	{
		Alert_Message( "BYPASS could not use flow reading!" );
	}
}

/* ---------------------------------------------------------- */
static void calculate_working_averages( UNS_32 const plevels )
{
	// 7/14/2014 rmd : Calculate the averages for the specified levels. If only two levels
	// specified, ZERO out the third level values. Thinking is not to leave residual values in
	// place if user is messing with the number of levels. To avoid unexpected operation.
	//
	// The plevels variable is to be either a two or a three.
	
	// ----------

	// 9/10/2014 rmd : We have seen elsewhere in the code that optimized code with literal
	// floating point values in equations produces garbage outcomes. I believe this to be a
	// known GCC compiler bug. As test show the problem corrected in GCC 4.8 something. A
	// workaround AJ came up with is to use these volatile constants.

	// 10/3/2014 rmd : The weighted average settings used to produce the averaged flow rate used
	// to make the switching decisions. Was using 0.7 / 0.3 but seemed in one case (the CATO
	// controller at Bakersfield) the bypass would bounce around between stages a bit too
	// quickly as flow was settling. It seemed appropriate to dampen the decision making some.
	// So I've moved these constants to 0.75 / 0.25 to give that a try. That will cause the
	// average to lag behind changing flow rates. At the same time we have also increased the
	// number of readings that this average is developed over from 20 to 40. See the discussion
	// there for explanation of this.
	
	// 10/6/2014 rmd : Decided to leave the .7 and .3 factors in place. Thinking the doubling of
	// the sample array size may be enough.
	const volatile float ZERO_POINT_SEVEN = 0.7;

	const volatile float ZERO_POINT_THREE = 0.3;

	//const volatile float ZERO_POINT_SEVENTY_FIVE = 0.75;

	//const volatile float ZERO_POINT_TWENTY_FIVE = 0.25;


	const volatile float ZERO_POINT_ZERO = 0.0;
	
	// ----------
	
	UNS_32	lll, iii;
	
	float	total_rate;
	
	// ----------
	
	for( lll=0; lll<3; lll++ )
	{
		if( lll < plevels )
		{
			Roll_FIFO( bypass.flow_rate_history[ lll ], sizeof( float ), BYPASS_READINGS_IN_FLOW_AVG );
		
			bypass.flow_rate_history[ lll ][ 0 ] = bypass.latest_flow_rate[ lll ];
			
			// ----------
			
			total_rate = ZERO_POINT_ZERO;
	
			for( iii=0; iii<BYPASS_READINGS_IN_FLOW_AVG; iii++ )
			{
				total_rate += bypass.flow_rate_history[ lll ][ iii ];
			}
			
			total_rate = (total_rate / BYPASS_READINGS_IN_FLOW_AVG);
			
			// 7/14/2014 rmd : Using a weighted average figure the new final average used to make the
			// manifold decisions.
			bypass.final_avg[ lll ] = (ZERO_POINT_SEVEN * bypass.final_avg[ lll ]) + (ZERO_POINT_THREE * total_rate);
		}
		else
		{
			// 7/14/2014 rmd : ZERO out the unused manifold stage. Only the last stage, stage 3 will
			// possibly fall through to this case.
			for( iii=0; iii<BYPASS_READINGS_IN_FLOW_AVG; iii++ )
			{
				bypass.flow_rate_history[ lll ][ iii ] = ZERO_POINT_ZERO;
			}

			bypass.final_avg[ lll ] = ZERO_POINT_ZERO;
		}
	}
}

/* ---------------------------------------------------------- */
// 8/27/2014 rmd : DO NOT BE TEMPTED TO CHANGE THIS TO A STATIC DECLARATION! You may awaken
// the compiler optimization bug documented ealier above within this file.
extern void one_hertz_processing( void )
{
	// 7/11/2014 rmd : This function is called at approximately a one hertz rate via a queue
	// message to this task from the td_check task. We update the working average for all three
	// levels. And then evaluate the bypass state based upon its present state and flow rate.

	// ----------

	// 7/11/2014 rmd : A fixed 10% hysteresis value to prevent oscillation between levels.
	//
	// 9/10/2014 rmd : We have seen elsewhere in the code that optimized code with literal
	// floating point values in equations produces garbage outcomes. I believe this to be a
	// known GCC compiler bug. As test show the problem corrected in GCC 4.8 something. A
	// workaround AJ came up with is to use these volatile constants.
	const volatile float BYPASS_HYSTERESIS_UP = 1.10;

	const volatile float BYPASS_HYSTERESIS_DOWN = 0.90;
	
	// ----------
	
	float	total_avg_flow;
	
	UNS_32	lll, levels;
	
	POC_FM_CHOICE_AND_RATE_DETAILS	fm_choice_and_rate_details[ POC_BYPASS_LEVELS_MAX ];
	
	// 9/11/2014 rmd : For the alert lines.
	float	flow_f, trip_f;

	// ----------
	
	// 7/11/2014 rmd : First verify the bypass engine is in sync with a bypass manifold.
	verify_or_link_bypass_engine();
	
	// 7/11/2014 rmd : If not linked that means there is no bypass to tend to.
	if( bypass.linked )
	{
		levels = POC_get_bypass_number_of_levels( bypass.lpoc );
		
		// 7/14/2014 rmd : Make some integrity checks. And possibly alert if invalid.
		if( bypass_configuration_is_valid( levels ) )
		{
			// 7/14/2014 rmd : Using the levels we just obtained calculate the working flow average used
			// to make the manifold decisions.
			calculate_working_averages( levels );
			
			// ----------
			
			// 7/11/2014 rmd : NOTE - MLB and MVOR are incorporated when the message is made to the
			// TPMICRO. And will supercede any MV energizing states that are made here.
	
			if( POC_get_fm_choice_and_rate_details( bypass.bpr->poc_gid, fm_choice_and_rate_details ) )
			{
				if( bypass.bpr->bypass_activate )
				{
					// 7/11/2014 rmd : Compute the total flow for the manifold. Using the flow rate of ALL the
					// flow meters is a copy of the way the 2000e came up with the flow rate used to make valve
					// decisions with. AJ and I discussed and decided to leave the same for the 3000. Though it
					// doesn't seem to be the "purist" way of doing it. I would think we would only use the flow
					// from the stage that is open. But this has worked well so far in the 2000e so we decided
					// to leave.
					total_avg_flow = 0.0;
					
					for( lll=0; lll<levels; lll++ )
					{
						total_avg_flow += bypass.final_avg[ lll ];
					}
			
					// ----------
					
					if( bypass.mode == BYPASS_MODE_passive_deenergized )
					{
						// 7/11/2014 rmd : If wasn't active switch to active mode and ALWAYS start by opening the
						// largest stage of the manifold. Comment for 2000e follows:
						//
						// we'll when we begin irrigation jump right up and open the biggest valve there is - close
						// all others this way we fill the lines and get the flow where it needs to be without the
						// risk of forcing abnormally high flow rates through the smaller valves - we also block out
						// any ratcheting down the ladder for 180 seconds (again a line fill and flow stablization
						// consideration - also if we responded to the averaged flow say a second from now it would
						// still be low)
						
						bypass.mode = BYPASS_MODE_operational;
						
						#if SHOW_BYPASS_OPERATION_LINES
							Alert_Message( "Switching to OPERATIONAL" );
						#endif

						if( levels == 2 )
						{
							bypass.state = BYPASS_STATE_second_open;
							
							#if SHOW_BYPASS_OPERATION_LINES
								Alert_Message( "Second level opened" );
							#endif
						}
						else
						{
							bypass.state = BYPASS_STATE_third_open;

							#if SHOW_BYPASS_OPERATION_LINES
								Alert_Message( "Third level opened" );
							#endif
						}
		
						bypass.seconds_since_went_operational = 0;
					}
					else
					{
						// 7/11/2014 rmd : OPERATIONAL MODE
	
						bypass.seconds_since_went_operational += 1;
						
						// ----------
						
						switch( bypass.state )
						{
							case BYPASS_STATE_first_open:
		
								// 7/11/2014 rmd : only one decision to make - to go up to go beyond the smallest level. The
								// trip point is pushed up (hysteresis).
								bypass.trip_point_up = (  BYPASS_HYSTERESIS_UP * BYPASS_transition_flow_rates[ fm_choice_and_rate_details[ 0 ].flow_meter_choice ] );
								
								if ( total_avg_flow >= bypass.trip_point_up )
								{
									#if SHOW_BYPASS_OPERATION_LINES
										flow_f = total_avg_flow;

										trip_f = bypass.trip_point_up;

										Alert_Message_va( "Second level opened f: %5.2f, t: %5.2f", flow_f, trip_f );
									#endif

									bypass.state = BYPASS_STATE_second_open;
								}
								break;
							
							case BYPASS_STATE_second_open:
								if( bypass.seconds_since_went_operational >= BYPASS_SECONDS_TO_WAIT_AFTER_GOING_OPERATIONAL )
								{
									// 7/11/2014 rmd : We are beyond the block out period that goes into effect when irrigation
									// first starts. When in the second stage we have two decisions to make - to go down or to
									// go up.
									
									// 7/11/2014 rmd : Figure the trip point to fall down to the smallest using the smallest
									// stage meter selection.
									bypass.trip_point_down = ( BYPASS_HYSTERESIS_DOWN * BYPASS_transition_flow_rates[ fm_choice_and_rate_details[ 0 ].flow_meter_choice ] );
	
									// 7/11/2014 rmd : Figure the trip point to push up to the largest stage using the mid level
									// meter selection. See if the flow is out-running this level.
									bypass.trip_point_up = ( BYPASS_HYSTERESIS_UP * BYPASS_transition_flow_rates[ fm_choice_and_rate_details[ 1 ].flow_meter_choice ] );
									
									if( total_avg_flow <= bypass.trip_point_down )
									{
										#if SHOW_BYPASS_OPERATION_LINES
											flow_f = total_avg_flow;
	
											trip_f = bypass.trip_point_down;
	
											Alert_Message_va( "First level opened f: %5.2f, t: %5.2f", flow_f, trip_f );
										#endif

										bypass.state = BYPASS_STATE_first_open;
									}
									else
									if( total_avg_flow >= bypass.trip_point_up )
									{
										// 7/11/2014 rmd : But only if there is a third level.
										if( levels == 3 )
										{
											#if SHOW_BYPASS_OPERATION_LINES
												flow_f = total_avg_flow;
		
												trip_f = bypass.trip_point_up;
		
												Alert_Message_va( "Third level opened f: %5.2f, t: %5.2f", flow_f, trip_f );
											#endif
	
											bypass.state = BYPASS_STATE_third_open;
										}
									}
								}
								break;
							
							case BYPASS_STATE_third_open:
								if( bypass.seconds_since_went_operational >= BYPASS_SECONDS_TO_WAIT_AFTER_GOING_OPERATIONAL )
								{
									// 7/14/2014 rmd : We are beyond the block out period that goes into effect when irrigation
									// first starts. There is only one decision to make - to fall down to the second level or
									// not.
									
									// 7/11/2014 rmd : Figure the trip point to fall down to the middle stage value.
									bypass.trip_point_down = ( BYPASS_HYSTERESIS_DOWN * BYPASS_transition_flow_rates[ fm_choice_and_rate_details[ 1 ].flow_meter_choice ] );
	
									if( total_avg_flow <= bypass.trip_point_down )
									{
										#if SHOW_BYPASS_OPERATION_LINES
											flow_f = total_avg_flow;
	
											trip_f = bypass.trip_point_down;
	
											Alert_Message_va( "Second level opened f: %5.2f, t: %5.2f", flow_f, trip_f );
										#endif

										bypass.state = BYPASS_STATE_second_open;
									}
								}
								break;
	
						}  // of switch for the states
						
					}  // of the two different modes
					
					// ----------
					
					// 7/11/2014 rmd : Now based upon the state set the mv energized bits.
					if( bypass.state == BYPASS_STATE_first_open )
					{
						POC_PRESERVES_set_master_valve_energized_bit( &(bypass.bpr->ws[ 0 ]), MASTER_VALVE_ACTION_OPEN );
	
						POC_PRESERVES_set_master_valve_energized_bit( &(bypass.bpr->ws[ 1 ]), MASTER_VALVE_ACTION_CLOSE );
	
						POC_PRESERVES_set_master_valve_energized_bit( &(bypass.bpr->ws[ 2 ]), MASTER_VALVE_ACTION_CLOSE );
					}
					else
					if( bypass.state == BYPASS_STATE_second_open )
					{
						POC_PRESERVES_set_master_valve_energized_bit( &(bypass.bpr->ws[ 0 ]), MASTER_VALVE_ACTION_CLOSE );
	
						POC_PRESERVES_set_master_valve_energized_bit( &(bypass.bpr->ws[ 1 ]), MASTER_VALVE_ACTION_OPEN );
	
						POC_PRESERVES_set_master_valve_energized_bit( &(bypass.bpr->ws[ 2 ]), MASTER_VALVE_ACTION_CLOSE );
					}
					else
					if( bypass.state == BYPASS_STATE_third_open )
					{
						if( levels != 3 )
						{
							Alert_Message( "BYPASS not 3 stages!" );
						}
						
						POC_PRESERVES_set_master_valve_energized_bit( &(bypass.bpr->ws[ 0 ]), MASTER_VALVE_ACTION_CLOSE );
	
						POC_PRESERVES_set_master_valve_energized_bit( &(bypass.bpr->ws[ 1 ]), MASTER_VALVE_ACTION_CLOSE );
	
						POC_PRESERVES_set_master_valve_energized_bit( &(bypass.bpr->ws[ 2 ]), MASTER_VALVE_ACTION_OPEN );
					}
					else
					{
						// 7/11/2014 rmd : Cover the unexpected.
						
						Alert_Message( "BYPASS state out of range" );
						
						bypass.mode = BYPASS_MODE_passive_deenergized;
						
						// 7/11/2014 rmd : Set ALL levels de-energized.
						for( lll=0; lll<levels; lll++ )
						{
							POC_PRESERVES_set_master_valve_energized_bit( &(bypass.bpr->ws[ lll ]), MASTER_VALVE_ACTION_DEENERGIZE );
						}
					}
					
				}  // of if meant to be active
				else
				{
					// 7/11/2014 rmd : Set the mode back to passive_deenergized. So we can detect the transition
					// again. And deenergize all valves.
					#if SHOW_BYPASS_OPERATION_LINES
						if( bypass.mode != BYPASS_MODE_passive_deenergized ) Alert_Message( "Switching to PASSIVE" );
					#endif

					bypass.mode = BYPASS_MODE_passive_deenergized;

					for( lll=0; lll<levels; lll++ )
					{
						POC_PRESERVES_set_master_valve_energized_bit( &(bypass.bpr->ws[ lll ]), MASTER_VALVE_ACTION_DEENERGIZE );
					}
					
				}
				
			}  // of successful K & O retrieval
			else
			{
				Alert_Message( "BYPASS OPERATION K & O error" );
		
				// 7/11/2014 rmd : And I suppose for safety all levels are de-energized?
				for( lll=0; lll<levels; lll++ )
				{
					POC_PRESERVES_set_master_valve_energized_bit( &(bypass.bpr->ws[ lll ]), MASTER_VALVE_ACTION_DEENERGIZE );
				}
			}
	
		}  // of if bypass configuration valid
		else
		{
			// 7/14/2014 rmd : Do not alert about this would fill alerts as this function runs at a one
			// hertz rate. Alerts are made at midnight however.
		}
		
	}  // of if bypass is linked
	
}

/* ---------------------------------------------------------- */
extern UNS_32 BYPASS_get_transition_rate_for_this_flow_meter_type( const UNS_32 pfm_type )
{
	UNS_32	rv;

	rv = 0;

	if( pfm_type <= POC_FM_300 )
	{
		rv = BYPASS_transition_flow_rates[ pfm_type ];
	}

	return ( rv );
}

/* ---------------------------------------------------------- */
extern void BYPASS_task( void *pvParameters )
{
	BYPASS_EVENT_QUEUE_STRUCT	beqs;

	// ----------

	// 10/23/2012 rmd : To support the WDT activity monitor.
	UNS_32  task_index;

	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// ----------

	init_bypass();
	
	// ----------

	while( (true) )
	{
		// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
		if( xQueueReceive( BYPASS_event_queue, &beqs, MS_to_TICKS( 500 ) ) == pdTRUE )
		{
			// 7/10/2014 rmd : Take both MUTEXES so the structures remain stable during our use.
			xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );
			
			xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );
		
			// ----------

			switch( beqs.event )
			{
				case BYPASS_EVENT_flow_reading_update:

					receive_flow_reading_update( &beqs );
					
					break;


				case BYPASS_EVENT_one_hertz_execution:
				
					one_hertz_processing();
								
					break;


				case BYPASS_EVENT_midnight:
				
					// 7/14/2014 rmd : Just set the alert flag so we can again make one alert about an invalid
					// bypass confiuration.
					bypass.alert_about_invalid_configrations = (true);
					
					break;

			}  // of event processing

			// ----------
		
			xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );
		
			xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

		}  // of a sucessful queue item is available

		// ----------

		// 10/23/2012 rmd : Keep the activity indication going.
		task_last_execution_stamp[ task_index ] = xTaskGetTickCount();

		// ----------

	}  // of forever task loop

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

