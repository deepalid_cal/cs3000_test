/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy
#include	<string.h>

#include	"weather_control.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"bithacks.h"

#include	"cal_math.h"

#include	"change.h"

#include	"configuration_controller.h"

#include	"e_historical_et_setup.h"

#include	"e_weather_sensors.h"

#include	"epson_rx_8025sa.h"

#include	"et_data.h"

#include	"flash_storage.h"

#include	"flowsense.h"

#include	"irri_comm.h"

#include	"range_check.h"

#include	"screen_utils.h"

#include	"shared.h"

#include	"shared_weather_control.c"

#include	"speaker.h"

#include	"tpmicro_comm.h"

#include	"tpmicro_data.h"

#include	"controller_initiated.h"

#include	"chain_sync_vars.h"

#include	"chain_sync_funcs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

const char WEATHER_CONTROL_FILENAME[] =	"WEATHER_CONTROL";

/* ---------------------------------------------------------- */

static const char RAIN_SWITCH_DEFAULT_NAME[] =		"Rain Switch";

static const char FREEZE_SWITCH_DEFAULT_NAME[] =	"Freeze Switch";

static const char STATE_DEFAULT_NAME[] =			"(state not yet set)";

static const char COUNTY_DEFAULT_NAME[] =			"(county not yet set)";

static const char CITY_DEFAULT_NAME[] =				"(city not yet set)";

/* ---------------------------------------------------------- */

// 10/17/2014 ajv : The following external variables are used only for display
// purposes. They're not defined as GuiVars in easyGUI, though, because they're
// only referenced in our code to index into the
// WEATHER_GuiVar_box_indexes_with_dash_w_option array.
UNS_32 WEATHER_GuiVar_ETGageInstalledAtIndex;

UNS_32 WEATHER_GuiVar_RainBucketInstalledAtIndex;

UNS_32 WEATHER_GuiVar_WindGageInstalledAtIndex;

// ----------

UNS_32 WEATHER_GuiVar_box_indexes_with_dash_w_option[ MAX_CHAIN_LENGTH ];

/* ---------------------------------------------------------- */

static void nm_weather_control_set_default_values( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	UNS_32	et_and_rain_table_roll_time;

	UNS_32	rain_bucket_box_index;

	BOOL_32	rain_bucket_is_there_a_rain_bucket;

	UNS_32	rain_bucket_minimum_inches_100u;

	UNS_32	rain_bucket_max_hourly_inches_100u;

	UNS_32	rain_bucket_max_24_hour_inches_100u;

	UNS_32	etg_box_index;

	BOOL_32	etg_is_in_use;

	BOOL_32	etg_log_et_pulses;

	UNS_32	etg_percent_of_historical_cap_100u;

	UNS_32	etg_percent_of_historical_min_100u;

	UNS_32	wind_gage_box_index;

	BOOL_32	wind_is_there_a_wind_gage;

	WIND_SETTINGS_STRUCT	wss;

	BOOL_32	reference_et_use_your_own;

	UNS_32	reference_et_user_numbers_100u[ MONTHS_IN_A_YEAR ];

	UNS_32	reference_et_state_index;

	UNS_32	reference_et_county_index;

	UNS_32	reference_et_city_index;

	char	user_defined_state[ MAX_REFERENCE_ET_NAME_LENGTH ];

	char	user_defined_county[ MAX_REFERENCE_ET_NAME_LENGTH ];

	char	user_defined_city[ MAX_REFERENCE_ET_NAME_LENGTH ];

	BOOL_32	rain_switch_is_there_a_rain_switch;

	BOOL_32	rain_switch_controllers_connected_to_switch[ MAX_CHAIN_LENGTH ];

	BOOL_32	freeze_switch_is_there_a_freeze_switch;

	BOOL_32	freeze_switch_controllers_connected_to_switch[ MAX_CHAIN_LENGTH ];

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

} WEATHER_CONTROL_STRUCT_REV_1;

// ----------

// 1/21/2015 rmd : This is the structure revision number. It started at 0. And should be
// incremented by 1 as changes are made to the weather_control structure. New variables are
// to be added to the end.
#define	WEATHER_CONTROL_LATEST_FILE_REVISION	(3)

// ----------

const UNS_32 weather_control_item_sizes[ WEATHER_CONTROL_LATEST_FILE_REVISION + 1 ] =
{
	// 1/26/2015 rmd : In order for this array to contain OLDER list item sizes the implication
	// is that the older structure definitions are to be retained. That means the newer
	// structure definition has a different name.
	
	sizeof( WEATHER_CONTROL_STRUCT_REV_1 ),

	// 4/23/2015 ajv : Revision 1 is the same size. Needed a variable set. See updater below.
	sizeof( WEATHER_CONTROL_STRUCT_REV_1 ),

	sizeof( WEATHER_CONTROL_STRUCT )
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void nm_weather_control_updater( UNS_32 pfrom_revision )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only when
	// the file revision is changed. The caller is to be holding the weather_control
	// recursive_MUTEX.

	// ----------
	
	if( pfrom_revision == WEATHER_CONTROL_LATEST_FILE_REVISION )
	{
		Alert_Message_va( "W CONTROL file unexpd update %u", pfrom_revision );	
	}	
	else
	{
		Alert_Message_va( "WEATHER CONTROL file update : to revision %u from %u", WEATHER_CONTROL_LATEST_FILE_REVISION, pfrom_revision );

		// ----------
		
		if( pfrom_revision == 0 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 0 to REVISION 1.

			// 2/2/2015 rmd : The REVISION was moved to 1 for the sole purpose of giving us the
			// opportunity to set the upload_to_comm_server distribution bits. Because of revision 0
			// bugs these bits were not set. We need to set them to guarantee all the program data is
			// sent to the comm_server this one time.
			weather_control.changes_to_upload_to_comm_server = UNS_32_MAX;
			
			// ----------
			
			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 1 to 2 changes if
			// needed.
			pfrom_revision += 1;
		}
		
		// ----------
		

		if( pfrom_revision == 1 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 1 to REVISION 2.

			// 3/19/2015 ajv : After speaking with David, we've increased the default historical ET cap
			// from 150% to 300% to help ensure ET is not limited during the WaterSense test. The user
			// can always reduce this if necessary.
			nm_WEATHER_set_percent_of_historical_et_cap( WEATHER_ET_PERCENT_OF_HISTORICAL_CAP_DEFAULT_100u, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, &weather_control.changes_to_send_to_master );

			pfrom_revision += 1;
		}

		// ----------

		if( pfrom_revision == 2 )
		{
			// 4/29/2015 ajv : Clear all of the pending Program Data bits.
			weather_control.changes_uploaded_to_comm_server_awaiting_ACK = CHANGE_BITS_32_BIT_ALL_CLEAR;

			// ----------

			pfrom_revision += 1;
		}
	}

	// ----------

	// 1/29/2015 rmd : Well after the updater has run the pfrom_revision is supposed to equal
	// the latest revision. By design of this update process.
	if( pfrom_revision != WEATHER_CONTROL_LATEST_FILE_REVISION )
	{
		Alert_Message( "W CONTROL updater error" );
	}
}

/* ---------------------------------------------------------- */
extern void init_file_weather_control( void )
{
	FLASH_FILE_find_or_create_variable_file(	FLASH_INDEX_1_GENERAL_STORAGE,
												WEATHER_CONTROL_FILENAME,
												WEATHER_CONTROL_LATEST_FILE_REVISION,
												&weather_control,
												sizeof( weather_control ),
												weather_control_recursive_MUTEX,
												&nm_weather_control_updater,
												&nm_weather_control_set_default_values,
												FF_WEATHER_CONTROL );
}

/* ---------------------------------------------------------- */
extern void save_file_weather_control( void )
{
	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE,
															WEATHER_CONTROL_FILENAME,
													        WEATHER_CONTROL_LATEST_FILE_REVISION,
														    &weather_control,
														    sizeof(weather_control),
															weather_control_recursive_MUTEX,
															FF_WEATHER_CONTROL );
}

/* ---------------------------------------------------------- */
/**
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up.
 *
 * @author 7/9/2012 Adrianusv
 *
 * @revisions
 *    7/9/2012 Initial release
 */
static void nm_weather_control_set_default_values( void )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	UNS_32	i;

	UNS_32	box_index_0;

	box_index_0 = FLOWSENSE_get_controller_index();

	// ------------------------

	// Clear all of the change bits
	SHARED_clear_all_32_bit_change_bits( &weather_control.changes_to_send_to_master, weather_control_recursive_MUTEX );
	SHARED_clear_all_32_bit_change_bits( &weather_control.changes_to_distribute_to_slaves, weather_control_recursive_MUTEX );
	SHARED_clear_all_32_bit_change_bits( &weather_control.changes_uploaded_to_comm_server_awaiting_ACK, weather_control_recursive_MUTEX );


	// 1/20/2015 ajv : However, we should explicitly set ALL bits for the Comm Server to ensure
	// changes are uploaded on a clean start.
	SHARED_set_all_32_bit_change_bits( &weather_control.changes_to_upload_to_comm_server, weather_control_recursive_MUTEX );

	// ------------------------

	// 5/2/2014 ajv : When setting the default values, always set the changes to
	// be sent to the master. Once the master receives the changes, it will
	// distribute them back out to the slaves.
	lchange_bitfield_to_set = &weather_control.changes_to_send_to_master;

	// ------------------------

	nm_WEATHER_set_rain_bucket_box_index( box_index_0, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_rain_bucket_connected( WEATHER_RAIN_BUCKET_CONNECTED_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_rain_bucket_minimum_inches( WEATHER_RAIN_BUCKET_MINIMUM_DEFAULT_100u, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_rain_bucket_max_hourly( WEATHER_RAIN_BUCKET_MAXIMUM_HOURLY_DEFAULT_100u, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_rain_bucket_max_24_hour( WEATHER_RAIN_BUCKET_MAXIMUM_24_HOUR_DEFAULT_100u, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );

	nm_WEATHER_set_et_gage_box_index( box_index_0, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_et_gage_connected( WEATHER_ET_GAGE_CONNECTED_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_et_gage_log_pulses( WEATHER_ET_LOG_PULSES_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_roll_time( WEATHER_ET_RAIN_TABLE_ROLL_TIME_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_percent_of_historical_et_cap( WEATHER_ET_PERCENT_OF_HISTORICAL_CAP_DEFAULT_100u, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_percent_of_historical_et_min( WEATHER_ET_PERCENT_OF_HISTORICAL_MIN_DEFAULT_100u, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );

	// --------------------------
	
	nm_WEATHER_set_wind_gage_box_index( box_index_0, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_wind_gage_connected( WEATHER_WIND_GAGE_CONNECTED_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	WEATHER_set_wind_pause_speed( WEATHER_WIND_PAUSE_SPEED_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	WEATHER_set_wind_resume_speed( WEATHER_WIND_RESUME_SPEED_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );

	// 7/26/2012 rmd : These two are not editable by the user but exist. I have kept them for
	// continuity sakes between previous generation product. They default to 10 minutes.
	weather_control.wss.wind_pause_minutes = 10;
	weather_control.wss.wind_resume_minutes = 10;

	// --------------------------
	
	nm_WEATHER_set_reference_et_use_your_own( WEATHER_REFERENCE_ET_USE_YOUR_OWN_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );

	UNS_32	lmonth1_12;

	for( lmonth1_12 = 1; lmonth1_12 <= MONTHS_IN_A_YEAR; ++lmonth1_12 )
	{
		nm_WEATHER_set_reference_et_number( ET_PredefinedData[ WEATHER_REFERENCE_ET_DEFAULT_CITY_INDEX ].ET_100u[ (lmonth1_12 - 1) ], lmonth1_12, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	}
	
	nm_WEATHER_set_reference_et_state_index( WEATHER_REFERENCE_ET_STATE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_reference_et_county_index( WEATHER_REFERENCE_ET_COUNTY_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_reference_et_city_index( WEATHER_REFERENCE_ET_CITY_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );

	nm_WEATHER_set_user_defined_state( STATE_DEFAULT_NAME, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_user_defined_county( COUNTY_DEFAULT_NAME, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_user_defined_city( CITY_DEFAULT_NAME, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );

	// ------------------------

	nm_WEATHER_set_rain_switch_in_use( WEATHER_RAIN_SWITCH_IN_USE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );

	nm_WEATHER_set_freeze_switch_in_use( WEATHER_FREEZE_SWITCH_IN_USE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );

	for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
	{
		nm_WEATHER_set_rain_switch_connected( WEATHER_RAIN_SWITCH_CONNECTED_DEFAULT, i, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );

		nm_WEATHER_set_freeze_switch_connected( WEATHER_FREEZE_SWITCH_CONNECTED_DEFAULT, i, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, box_index_0, CHANGE_do_not_set_change_bits, lchange_bitfield_to_set );
	}

	// ------------------------
}

/* ---------------------------------------------------------- */
/**
 * This function runs through the weather structure and copies the contents of any changed
 * settings to the passed in ucp.
 * 
 * @executed Executed within the context of the CommMngr task as a token, token response, or
 * Program Data message to the Comm Server is being built.
 * 
 * @param pucp A pointer to the pointer where the program data is being built.
 * 
 * @param pmem_used_so_far The amount of memory used so far. Once this reaches our limit for
 * a single token (MEMORY_TO_ALLOCATE_FOR_SENDING_CHANGES), processing of the remaining
 * changes stops until it's time to build the next token or token response.
 * 
 * @param pmem_used_so_far_is_less_than_allocated_memory A pointer to the flag which
 * indicates whether we're allowed to add more changes to the token or token response.
 * 
 * @param preason_data_is_being_built The reason why the data is being built to send. Based
 * upon this reason, we examine a different change bit structure to determine what to
 * send. Valid values are CHANGE_REASON_SYNC_SENDING_TO_MASTER,
 * CHANGE_REASON_SYNC_DISTRIBUTING_TO_SLAVES, CHANGE_REASON_SYNC_UPLOADING_TO_COMM_SERVER.
 * 
 * @return UNS_32 The total size of the added data. This is used to determine whether any
 * changes were detected so the appropriate file change bit can be set as well as to
 * determine how much memory must be allocated to store the changes.
 *
 * @author 7/12/2012 Adrianusv
 */
extern UNS_32 WEATHER_build_data_to_send( UNS_8 **pucp,
										  const UNS_32 pmem_used_so_far,
										  BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
										  const UNS_32 preason_data_is_being_built,
										  const UNS_32 pallocated_memory )
{
	CHANGE_BITS_32_BIT	*lbitfield_of_changes_to_use_to_determine_what_to_send;

	CHANGE_BITS_32_BIT	lbitfield_of_changes_to_send;

	UNS_8	*llocation_of_bitfield;

	UNS_32	lsize_of_bitfield;

	UNS_32	rv;

	// ----------

	rv = 0;

	lsize_of_bitfield = sizeof( CHANGE_BITS_32_BIT );

	// ----------

	lbitfield_of_changes_to_use_to_determine_what_to_send = WEATHER_get_change_bits_ptr( preason_data_is_being_built );

	// ----------

	// 1/14/2015 ajv : If any changes are detected...
	if( lbitfield_of_changes_to_use_to_determine_what_to_send != CHANGE_BITS_32_BIT_ALL_CLEAR )
	{
		// 9/19/2013 ajv : Don't bother to try to add any weather data if we've already filled out
		// the token with the maximum amount of data to send.
		if( (*pmem_used_so_far_is_less_than_allocated_memory == (true)) && (pmem_used_so_far < pallocated_memory) )
		{
			// Initialize the bit field
			lbitfield_of_changes_to_send = 0;

			rv = PDATA_copy_bitfield_info_into_pucp( pucp,
													 lsize_of_bitfield,
													 sizeof( lbitfield_of_changes_to_send ),
													 &llocation_of_bitfield );

			// ----------

			// 1/14/2015 ajv : Take the appropriate mutex since we have to not only copy the setting,
			// but also reset the associated change bit and we don't want either of those changing
			// outside of this function in the middle of it.
			xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

			// ----------

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_roll_time_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.et_and_rain_table_roll_time,
					sizeof(weather_control.et_and_rain_table_roll_time),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_rb_box_index_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.rain_bucket_box_index,
					sizeof(weather_control.rain_bucket_box_index),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_rb_in_use_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.rain_bucket_is_there_a_rain_bucket,
					sizeof(weather_control.rain_bucket_is_there_a_rain_bucket),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_rb_minimum_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.rain_bucket_minimum_inches_100u,
					sizeof(weather_control.rain_bucket_minimum_inches_100u),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_rb_max_hourly_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.rain_bucket_max_hourly_inches_100u,
					sizeof(weather_control.rain_bucket_max_hourly_inches_100u),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_rb_max_24_hour_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.rain_bucket_max_24_hour_inches_100u,
					sizeof(weather_control.rain_bucket_max_24_hour_inches_100u),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_etg_box_index_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.etg_box_index,
					sizeof(weather_control.etg_box_index),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_etg_in_use_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.etg_is_in_use,
					sizeof(weather_control.etg_is_in_use),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_etg_log_pulses_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.etg_log_et_pulses,
					sizeof(weather_control.etg_log_et_pulses),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_etg_historical_cap_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.etg_percent_of_historical_cap_100u,
					sizeof(weather_control.etg_percent_of_historical_cap_100u),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_etg_historical_min_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.etg_percent_of_historical_min_100u,
					sizeof(weather_control.etg_percent_of_historical_min_100u),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_wg_box_index_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.wind_gage_box_index,
					sizeof(weather_control.wind_gage_box_index),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_wg_in_use_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.wind_is_there_a_wind_gage,
					sizeof(weather_control.wind_is_there_a_wind_gage),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_wg_pause_speed_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.wss.wind_pause_mph,
					sizeof(weather_control.wss.wind_pause_mph),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_wg_resume_speed_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.wss.wind_resume_mph,
					sizeof(weather_control.wss.wind_resume_mph),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_use_your_own_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.reference_et_use_your_own,
					sizeof(weather_control.reference_et_use_your_own),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_monthlies_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.reference_et_user_numbers_100u,
					sizeof(weather_control.reference_et_user_numbers_100u),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_state_idx_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.reference_et_state_index,
					sizeof(weather_control.reference_et_state_index),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_county_idx_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.reference_et_county_index,
					sizeof(weather_control.reference_et_county_index),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_city_idx_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.reference_et_city_index,
					sizeof(weather_control.reference_et_city_index),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_state_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.user_defined_state,
					sizeof(weather_control.user_defined_state),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_county_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.user_defined_county,
					sizeof(weather_control.user_defined_county),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_city_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.user_defined_city,
					sizeof(weather_control.user_defined_city),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_in_use_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.rain_switch_is_there_a_rain_switch,
					sizeof(weather_control.rain_switch_is_there_a_rain_switch),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_controllers_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.rain_switch_controllers_connected_to_switch,
					sizeof(weather_control.rain_switch_controllers_connected_to_switch),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_freeze_switch_in_use_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.freeze_switch_is_there_a_freeze_switch,
					sizeof(weather_control.freeze_switch_is_there_a_freeze_switch),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			rv += PDATA_copy_var_into_pucp_and_set_32_bit_change_bits(
					pucp,
					&lbitfield_of_changes_to_send,
					WEATHER_CONTROL_CHANGE_BITFIELD_freeze_switch_controllers_bit,
					lbitfield_of_changes_to_use_to_determine_what_to_send,
					&weather_control.changes_uploaded_to_comm_server_awaiting_ACK,
					&weather_control.freeze_switch_controllers_connected_to_switch,
					sizeof(weather_control.freeze_switch_controllers_connected_to_switch),
					(rv + pmem_used_so_far),
					pmem_used_so_far_is_less_than_allocated_memory,
					pallocated_memory,
					preason_data_is_being_built );

			// ----------

			// 1/14/2015 ajv : If changes were detected, we need to copy the bits into location we
			// created above. Otherwise, we need to back out the space allocated and reset the rv to 0.
			if( lbitfield_of_changes_to_send > 0 )
			{
				// 1/14/2015 ajv : Add the actual bit field to the message now that it's been completely
				// filled in.
				memcpy( llocation_of_bitfield, &lbitfield_of_changes_to_send, sizeof(lbitfield_of_changes_to_send) );
			}
			else
			{
				// 1/14/2015 ajv : If we've added the the bit field structure to the message but didn't
				// actually add any data because we hit our limit, back out the bit field data.
				*pucp -= sizeof( lsize_of_bitfield );
				rv -= sizeof( lsize_of_bitfield );

				*pucp -= sizeof( lbitfield_of_changes_to_send );
				rv -= sizeof( lbitfield_of_changes_to_send );
			}
		}
	}

	// ----------

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 *
 * @author 2/7/2013 Adrianusv
 *
 * @revisions (none)
 */
extern void FDTO_WEATHER_update_real_time_weather_GuiVars( void )
{
	#define	CP_ET_GAGE_CLEAR_RUNAWAY_GAGE			(26)

	// ----------

	xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );

	if( weather_preserves.et_rip.status == ET_STATUS_FROM_ET_GAGE )
	{
		GuiVar_StatusETGageReading = (float)(weather_preserves.et_rip.et_inches_u16_10000u / 10000.0);
	}

	// 4/28/2015 rmd : This used to be the roll time to roll time pulse count. I switched to
	// mid-night to mid-night. That seems to relate better to the user.
	GuiVar_StatusRainBucketReading = (float)(weather_preserves.rain.midnight_to_midnight_raw_pulse_count / 100.0);

	// 10/13/2014 ajv : There's no need to set GuiVar_StatusWindGageReading
	// here. This is handled as part of the incoming token from the master.

	xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );

	// ----------

	if( GuiVar_ETGageRunawayGage != weather_preserves.run_away_gage )
	{
		GuiVar_ETGageRunawayGage = weather_preserves.run_away_gage;

		// If the cursor is on the Clear Runaway Gage button when it disppears,
		// the cursor will be lost as well. Therefore, move the cursor up before
		// redrawing the screen to hide the button.
		if( GuiLib_ActiveCursorFieldNo == CP_ET_GAGE_CLEAR_RUNAWAY_GAGE )
		{
			GuiLib_Cursor_Up();
		}

		FDTO_Redraw_Screen( (false) );
	}
	else
	{
		GuiLib_Refresh();
	}

	xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Copies the ET Gage variables, used by the ET Gage Setup screen, into the
 * appropriate GuiVars.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the ET Gage Setup screen is drawn.
 *
 * @author 8/30/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void WEATHER_copy_et_gage_settings_into_GuiVars( void )
{
	UNS_32	i;

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	GuiVar_ETGageInUse = weather_control.etg_is_in_use;

	GuiVar_ETGageLogPulses = weather_control.etg_log_et_pulses;

	// 10/17/2014 ajv : Display the controller where the ET Gage is connected to
	for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
	{
		if( weather_control.etg_box_index == WEATHER_GuiVar_box_indexes_with_dash_w_option[ i ] )
		{
			WEATHER_GuiVar_ETGageInstalledAtIndex = i;

			snprintf( GuiVar_ETGageInstalledAt, sizeof(GuiVar_ETGageInstalledAt), "%c", WEATHER_GuiVar_box_indexes_with_dash_w_option[ i ] + 'A' );
		}
	}

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );


	xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );

	GuiVar_ETGagePercentFull = (((float)weather_preserves.remaining_gage_pulses / ET_GAGE_PULSE_CAPACITY) * 100.0);

	GuiVar_ETGageSkipTonight = weather_preserves.dont_use_et_gage_today;

	GuiVar_StatusETGageReading = (float)(weather_preserves.et_rip.et_inches_u16_10000u / 10000.0);

	GuiVar_ETGageRunawayGage = weather_preserves.run_away_gage;

	xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Copies the Rain Bucket variables, used by the Rain Bucket Setup screen, into
 * the appropriate GuiVars.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the Rain Bucket Setup screen is drawn.
 *
 * @author 8/30/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void WEATHER_copy_rain_bucket_settings_into_GuiVars( void )
{
	UNS_32	i;

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	GuiVar_RainBucketInUse = weather_control.rain_bucket_is_there_a_rain_bucket;

	GuiVar_RainBucketMinimum = (float)(weather_control.rain_bucket_minimum_inches_100u / 100.0);

	GuiVar_RainBucketMaximumHourly = (float)(weather_control.rain_bucket_max_hourly_inches_100u / 100.0);

	GuiVar_RainBucketMaximum24Hour = (float)(weather_control.rain_bucket_max_24_hour_inches_100u / 100.0);


	// 9/25/2012 rmd : Show the actual amount of rain the bucket has seen. Not the 'massaged'
	// amount headed for the table.
	//
	// 4/28/2015 rmd : This used to be the roll time to roll time pulse count. I switched to
	// mid-night to mid-night. That seems to relate better to the user.
	GuiVar_StatusRainBucketReading = (float)(weather_preserves.rain.midnight_to_midnight_raw_pulse_count / 100.0);
	

	// 9/25/2012 rmd : BE FOREWARNED ABOUT THE FLOATING POINT ISSUE. SEE ET GAGE DECRIPTION
	// DETERMINING IF THERE WAS A CHANGE OR NOT.

	// 10/17/2014 ajv : Display the controller where the Rain Bucket is
	// connected to
	for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
	{
		if( weather_control.rain_bucket_box_index == WEATHER_GuiVar_box_indexes_with_dash_w_option[ i ] )
		{
			WEATHER_GuiVar_RainBucketInstalledAtIndex = i;

			snprintf( GuiVar_RainBucketInstalledAt, sizeof(GuiVar_RainBucketInstalledAt), "%c", WEATHER_GuiVar_box_indexes_with_dash_w_option[ i ] + 'A' );
		}
	}

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Copies the Wind Gage variables, used by the Wind Gage Setup screen, into the
 * appropriate GuiVars.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the Wind Gage Setup screen is drawn.
 *
 * @author 8/30/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void WEATHER_copy_wind_gage_settings_into_GuiVars( void )
{
	UNS_32	i;

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	GuiVar_WindGageInUse = weather_control.wind_is_there_a_wind_gage;
	GuiVar_WindGagePauseSpeed = weather_control.wss.wind_pause_mph;
	GuiVar_WindGageResumeSpeed = weather_control.wss.wind_resume_mph;

	// 10/17/2014 ajv : Display the controller where the Wind Gage is connected
	// to
	for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
	{
		if( weather_control.wind_gage_box_index == WEATHER_GuiVar_box_indexes_with_dash_w_option[ i ] )
		{
			WEATHER_GuiVar_WindGageInstalledAtIndex = i;

			snprintf( GuiVar_WindGageInstalledAt, sizeof(GuiVar_WindGageInstalledAt), "%c", WEATHER_GuiVar_box_indexes_with_dash_w_option[ i ] + 'A' );
		}
	}

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	// ----------

	// 10/13/2014 ajv : There's no need to set GuiVar_StatusWindGageReading
	// here. This is handled as part of the incoming token from the master.
}

/* ---------------------------------------------------------- */
/**
 * Copies the Rain Switch variables, used by the Rain Switch Setup screen, into
 * the appropriate GuiVars.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the Rain Switch Setup screen is drawn.
 *
 * @author 8/30/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void WEATHER_copy_rain_switch_settings_into_GuiVars( void )
{
	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	GuiVar_RainSwitchInUse = weather_control.rain_switch_is_there_a_rain_switch;

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Copies the Freeze Switch variables, used by the Freeze Switch Setup screen,
 * into the appropriate GuiVars.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the Freeze Switch Setup screen is drawn.
 *
 * @author 8/30/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void WEATHER_copy_freeze_switch_settings_into_GuiVars( void )
{
	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	GuiVar_FreezeSwitchInUse = weather_control.freeze_switch_is_there_a_freeze_switch;

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Copies the Historical ET variables, used by the Historical ET Setup screen, into the
 * appropriate GuiVars.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the Historical ET Setup screen is drawn.
 *
 * @author 7/12/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void WEATHER_copy_historical_et_settings_into_GuiVars( void )
{
	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	GuiVar_HistoricalETUseYourOwn = weather_control.reference_et_use_your_own;

	g_HISTORICAL_ET_state_index = weather_control.reference_et_state_index;
	strlcpy( GuiVar_HistoricalETState, States[ g_HISTORICAL_ET_state_index ], sizeof(GuiVar_HistoricalETState) );

	g_HISTORICAL_ET_county_index = weather_control.reference_et_county_index;
	strlcpy( GuiVar_HistoricalETCounty, Counties[ g_HISTORICAL_ET_county_index ].county, sizeof(GuiVar_HistoricalETCounty) );

	g_HISTORICAL_ET_city_index = weather_control.reference_et_city_index;
	strlcpy( GuiVar_HistoricalETCity, ET_PredefinedData[ g_HISTORICAL_ET_city_index ].city, sizeof(GuiVar_HistoricalETCity) );

	GuiVar_ETGageMaxPercent = weather_control.etg_percent_of_historical_cap_100u;

	GuiVar_HistoricalET1 = ((float)ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 0 ] / 100.0F);
	GuiVar_HistoricalET2 = ((float)ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 1 ] / 100.0F);
	GuiVar_HistoricalET3 = ((float)ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 2 ] / 100.0F);
	GuiVar_HistoricalET4 = ((float)ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 3 ] / 100.0F);
	GuiVar_HistoricalET5 = ((float)ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 4 ] / 100.0F);
	GuiVar_HistoricalET6 = ((float)ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 5 ] / 100.0F);
	GuiVar_HistoricalET7 = ((float)ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 6 ] / 100.0F);
	GuiVar_HistoricalET8 = ((float)ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 7 ] / 100.0F);
	GuiVar_HistoricalET9 = ((float)ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 8 ] / 100.0F);
	GuiVar_HistoricalET10 = ((float)ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 9 ] / 100.0F);
	GuiVar_HistoricalET11 = ((float)ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 10 ] / 100.0F);
	GuiVar_HistoricalET12 = ((float)ET_PredefinedData[ g_HISTORICAL_ET_city_index ].ET_100u[ 11 ] / 100.0F);

	strlcpy( GuiVar_HistoricalETUserState, weather_control.user_defined_state, sizeof(GuiVar_HistoricalETState) );
	strlcpy( GuiVar_HistoricalETUserCounty, weather_control.user_defined_county, sizeof(GuiVar_HistoricalETCounty) );
	strlcpy( GuiVar_HistoricalETUserCity, weather_control.user_defined_city, sizeof(GuiVar_HistoricalETCity) );

	GuiVar_HistoricalETUser1 = ((float)weather_control.reference_et_user_numbers_100u[ 0 ] / 100.0F);
	GuiVar_HistoricalETUser2 = ((float)weather_control.reference_et_user_numbers_100u[ 1 ] / 100.0F);
	GuiVar_HistoricalETUser3 = ((float)weather_control.reference_et_user_numbers_100u[ 2 ] / 100.0F);
	GuiVar_HistoricalETUser4 = ((float)weather_control.reference_et_user_numbers_100u[ 3 ] / 100.0F);
	GuiVar_HistoricalETUser5 = ((float)weather_control.reference_et_user_numbers_100u[ 4 ] / 100.0F);
	GuiVar_HistoricalETUser6 = ((float)weather_control.reference_et_user_numbers_100u[ 5 ] / 100.0F);
	GuiVar_HistoricalETUser7 = ((float)weather_control.reference_et_user_numbers_100u[ 6 ] / 100.0F);
	GuiVar_HistoricalETUser8 = ((float)weather_control.reference_et_user_numbers_100u[ 7 ] / 100.0F);
	GuiVar_HistoricalETUser9 = ((float)weather_control.reference_et_user_numbers_100u[ 8 ] / 100.0F);
	GuiVar_HistoricalETUser10 = ((float)weather_control.reference_et_user_numbers_100u[ 9 ] / 100.0F);
	GuiVar_HistoricalETUser11 = ((float)weather_control.reference_et_user_numbers_100u[ 10 ] / 100.0F);
	GuiVar_HistoricalETUser12 = ((float)weather_control.reference_et_user_numbers_100u[ 11 ] / 100.0F);

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Copies the ET Gage GuiVars into the appropriate ET Gage variables, both in
 * the weather_config structure and weather_preserves.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user leaves the ET Gage Setup screen.
 *
 * @author 8/30/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void WEATHER_extract_and_store_et_gage_changes_from_GuiVars( void )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	lchange_bitfield_to_set = WEATHER_get_change_bits_ptr( CHANGE_REASON_KEYPAD );

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	nm_WEATHER_set_et_gage_connected( GuiVar_ETGageInUse, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_et_gage_box_index( WEATHER_GuiVar_box_indexes_with_dash_w_option[ WEATHER_GuiVar_ETGageInstalledAtIndex ], CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_et_gage_log_pulses( GuiVar_ETGageLogPulses, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, lchange_bitfield_to_set );

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	// ---------------

	xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );

	weather_preserves.dont_use_et_gage_today = GuiVar_ETGageSkipTonight;

	if( weather_preserves.remaining_gage_pulses != (GuiVar_ETGagePercentFull * (ET_GAGE_PULSE_CAPACITY / 100)) )
	{
		Alert_remaining_gage_pulses_changed( weather_preserves.remaining_gage_pulses, (GuiVar_ETGagePercentFull * (ET_GAGE_PULSE_CAPACITY / 100)) );

		weather_preserves.remaining_gage_pulses = (GuiVar_ETGagePercentFull * (ET_GAGE_PULSE_CAPACITY / 100));
	}

	xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Copies the Rain Bucket GuiVars into the appropriate Rain Bucket variables.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user leaves the Rain Bucket Setup screen.
 *
 * @author 8/30/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void WEATHER_extract_and_store_rain_bucket_changes_from_GuiVars( void )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	UNS_32	box_index_0;

	box_index_0 = FLOWSENSE_get_controller_index();

	lchange_bitfield_to_set = WEATHER_get_change_bits_ptr( CHANGE_REASON_KEYPAD );

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	nm_WEATHER_set_rain_bucket_connected( GuiVar_RainBucketInUse, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_rain_bucket_box_index( WEATHER_GuiVar_box_indexes_with_dash_w_option[ WEATHER_GuiVar_RainBucketInstalledAtIndex ], CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_rain_bucket_minimum_inches( (UNS_32)(GuiVar_RainBucketMinimum * 100), CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_rain_bucket_max_hourly( (UNS_32)(GuiVar_RainBucketMaximumHourly * 100), CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_rain_bucket_max_24_hour( (UNS_32)(GuiVar_RainBucketMaximum24Hour * 100), CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Copies the Wind Gage GuiVars into the appropriate Wind Gage variables.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user leaves the Wind Gage Setup screen.
 *
 * @author 8/30/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void WEATHER_extract_and_store_wind_gage_changes_from_GuiVars( void )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	UNS_32	box_index_0;

	box_index_0 = FLOWSENSE_get_controller_index();

	lchange_bitfield_to_set = WEATHER_get_change_bits_ptr( CHANGE_REASON_KEYPAD );

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	nm_WEATHER_set_wind_gage_connected( GuiVar_WindGageInUse, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_wind_gage_box_index( WEATHER_GuiVar_box_indexes_with_dash_w_option[ WEATHER_GuiVar_WindGageInstalledAtIndex ], CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	WEATHER_set_wind_pause_speed( GuiVar_WindGagePauseSpeed, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	WEATHER_set_wind_resume_speed( GuiVar_WindGageResumeSpeed, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Copies the Rain Switch GuiVars into the appropriate Rain Switch variables.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user leaves the Rain Switch Setup screen.
 *
 * @author 8/30/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void WEATHER_extract_and_store_rain_switch_changes_from_GuiVars( void )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	lchange_bitfield_to_set = WEATHER_get_change_bits_ptr( CHANGE_REASON_KEYPAD );

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	nm_WEATHER_set_rain_switch_in_use( GuiVar_RainSwitchInUse, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, lchange_bitfield_to_set );

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Copies the Freeze Switch GuiVars into the appropriate Freeze Switch
 * variables.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user leaves the Freeze Switch Setup screen.
 *
 * @author 8/30/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void WEATHER_extract_and_store_freeze_switch_changes_from_GuiVars( void )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	lchange_bitfield_to_set = WEATHER_get_change_bits_ptr( CHANGE_REASON_KEYPAD );

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	nm_WEATHER_set_freeze_switch_in_use( GuiVar_FreezeSwitchInUse, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, lchange_bitfield_to_set );

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * Copies the Historical ET GuiVars into the appropriate Historical ET variables.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user leaves the Historical ET Setup screen.
 *
 * @author 7/12/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void WEATHER_extract_and_store_historical_et_changes_from_GuiVars( void )
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	UNS_32	box_index_0;

	box_index_0 = FLOWSENSE_get_controller_index();

	lchange_bitfield_to_set = WEATHER_get_change_bits_ptr( CHANGE_REASON_KEYPAD );

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	nm_WEATHER_set_reference_et_use_your_own( GuiVar_HistoricalETUseYourOwn, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );

	nm_WEATHER_set_reference_et_state_index( g_HISTORICAL_ET_state_index, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_reference_et_county_index( g_HISTORICAL_ET_county_index, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_reference_et_city_index( g_HISTORICAL_ET_city_index, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );

	nm_WEATHER_set_percent_of_historical_et_cap( GuiVar_ETGageMaxPercent, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );

	nm_WEATHER_set_reference_et_number( (UNS_32)(GuiVar_HistoricalETUser1 * 100), 1, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_reference_et_number( (UNS_32)(GuiVar_HistoricalETUser2 * 100), 2, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_reference_et_number( (UNS_32)(GuiVar_HistoricalETUser3 * 100), 3, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_reference_et_number( (UNS_32)(GuiVar_HistoricalETUser4 * 100), 4, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_reference_et_number( (UNS_32)(GuiVar_HistoricalETUser5 * 100), 5, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_reference_et_number( (UNS_32)(GuiVar_HistoricalETUser6 * 100), 6, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_reference_et_number( (UNS_32)(GuiVar_HistoricalETUser7 * 100), 7, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_reference_et_number( (UNS_32)(GuiVar_HistoricalETUser8 * 100), 8, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_reference_et_number( (UNS_32)(GuiVar_HistoricalETUser9 * 100), 9, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_reference_et_number( (UNS_32)(GuiVar_HistoricalETUser10 * 100), 10, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_reference_et_number( (UNS_32)(GuiVar_HistoricalETUser11 * 100), 11, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_reference_et_number( (UNS_32)(GuiVar_HistoricalETUser12 * 100), 12, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );

	nm_WEATHER_set_user_defined_state( GuiVar_HistoricalETUserState, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_user_defined_county( GuiVar_HistoricalETUserCounty, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );
	nm_WEATHER_set_user_defined_city( GuiVar_HistoricalETUserCity, CHANGE_generate_change_line, CHANGE_REASON_KEYPAD, box_index_0, CHANGE_set_change_bits, lchange_bitfield_to_set );

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern BOOL_32 WEATHER_there_is_a_weather_option_in_the_chain( void )
{
	UNS_32	i;

	BOOL_32	rv;

	rv = (false);

	// ----------
	
	// 5/1/2014 rmd : If the chain is down or the comm_mngr is not in its normal
	// token making state prevent the user from seeing this screen.
	if( COMM_MNGR_network_is_available_for_normal_use() )
	{
		xSemaphoreTakeRecursive( chain_members_recursive_MUTEX, portMAX_DELAY );

		for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
		{
			// 6/7/2016 ajv : Only count weather and lights options for controllers that were detected
			// in the last scan.
			if( chain.members[ i ].saw_during_the_scan )
			{
				if( chain.members[ i ].box_configuration.wi.weather_card_present && chain.members[ i ].box_configuration.wi.weather_terminal_present )
				{
					rv = (true);
	
					// 11/8/2013 ajv : Since we've found at least one weather option,
					// terminate the loop.
					break;
				}
			}
		}
		
		xSemaphoreGiveRecursive( chain_members_recursive_MUTEX );
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */

extern UNS_32 WEATHER_get_et_gage_box_index( void )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32( &weather_control.etg_box_index,
							BOX_INDEX_MINIMUM,
							BOX_INDEX_MAXIMUM,
							BOX_INDEX_DEFAULT,
							&nm_WEATHER_set_et_gage_box_index,
							&weather_control.changes_to_send_to_master,
							WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_etg_box_index_bit ] );

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 WEATHER_get_et_gage_is_in_use( void )
{
	BOOL_32	rv;

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	// 2/3/2015 ajv : It's possible that the ET Gage in use flag was set TRUE at some point when
	// a Weather option was installed, but the weather option has since been removed. Therefore,
	// only look at the "in use" value if there's actually a weather option available somewhere.
	if ( WEATHER_there_is_a_weather_option_in_the_chain() )
	{
		rv = SHARED_get_bool( &weather_control.etg_is_in_use,
							  WEATHER_ET_GAGE_CONNECTED_DEFAULT,
							  &nm_WEATHER_set_et_gage_connected,
							  &weather_control.changes_to_send_to_master,
							  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_etg_in_use_bit ] );
	}
	else
	{
		// 2/3/2015 ajv : If there's no weather option anywhere in the chain, always return FALSE.
		rv = (false);
	}

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 WEATHER_get_et_gage_log_pulses( void )
{
	BOOL_32	rv;

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_bool( &weather_control.etg_log_et_pulses,
						  WEATHER_ET_LOG_PULSES_DEFAULT,
						  &nm_WEATHER_set_et_gage_log_pulses,
						  &weather_control.changes_to_send_to_master,
						  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_etg_log_pulses_bit ] );

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 WEATHER_get_rain_bucket_box_index( void )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32( &weather_control.rain_bucket_box_index,
							BOX_INDEX_MINIMUM,
							BOX_INDEX_MAXIMUM,
							BOX_INDEX_DEFAULT,
							&nm_WEATHER_set_rain_bucket_box_index,
							&weather_control.changes_to_send_to_master,
							WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rb_box_index_bit ] );

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 WEATHER_get_rain_bucket_is_in_use( void )
{
	BOOL_32	rv;

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	// 2/3/2015 ajv : It's possible that the Rain Bucket in use flag was set TRUE at some point
	// when a Weather option was installed, but the weather option has since been removed.
	// Therefore, only look at the "in use" value if there's actually a weather option available
	// somewhere.
	if ( WEATHER_there_is_a_weather_option_in_the_chain() )
	{
		rv = SHARED_get_bool( &weather_control.rain_bucket_is_there_a_rain_bucket,
							  WEATHER_RAIN_BUCKET_CONNECTED_DEFAULT,
							  &nm_WEATHER_set_rain_bucket_connected,
							  &weather_control.changes_to_send_to_master,
							  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rb_in_use_bit ] );
	}
	else
	{
		// 2/3/2015 ajv : If there's no weather option anywhere in the chain, always return FALSE.
		rv = (false);
	}

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 WEATHER_get_rain_bucket_minimum_inches_100u( void )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32( &weather_control.rain_bucket_minimum_inches_100u,
							WEATHER_RAIN_BUCKET_MINIMUM_MIN_100u,
							WEATHER_RAIN_BUCKET_MINIMUM_MAX_100u,
							WEATHER_RAIN_BUCKET_MINIMUM_DEFAULT_100u,
							&nm_WEATHER_set_rain_bucket_minimum_inches,
							&weather_control.changes_to_send_to_master,
							WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rb_minimum_bit ] );

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 WEATHER_get_rain_bucket_max_hourly_inches_100u( void )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32( &weather_control.rain_bucket_max_hourly_inches_100u,
							WEATHER_RAIN_BUCKET_MAXIMUM_HOURLY_MIN_100u,
							WEATHER_RAIN_BUCKET_MAXIMUM_HOURLY_MAX_100u,
							WEATHER_RAIN_BUCKET_MAXIMUM_HOURLY_DEFAULT_100u,
							&nm_WEATHER_set_rain_bucket_max_hourly,
							&weather_control.changes_to_send_to_master,
							WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rb_max_hourly_bit ] );

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 WEATHER_get_rain_bucket_max_24_hour_inches_100u( void )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32( &weather_control.rain_bucket_max_24_hour_inches_100u,
							WEATHER_RAIN_BUCKET_MAXIMUM_24_HOUR_MIN_100u,
							WEATHER_RAIN_BUCKET_MAXIMUM_24_HOUR_MAX_100u,
							WEATHER_RAIN_BUCKET_MAXIMUM_24_HOUR_DEFAULT_100u,
							&nm_WEATHER_set_rain_bucket_max_24_hour,
							&weather_control.changes_to_send_to_master,
							WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rb_max_24_hour_bit ] );

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 WEATHER_get_wind_gage_box_index( void )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32( &weather_control.wind_gage_box_index,
							BOX_INDEX_MINIMUM,
							BOX_INDEX_MAXIMUM,
							BOX_INDEX_DEFAULT,
							&nm_WEATHER_set_wind_gage_box_index,
							&weather_control.changes_to_send_to_master,
							WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_wg_box_index_bit ] );

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Returns whether a wind gage is in use within the chain or not/
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when drawing the weather menu because the "Use of Wind" menu item is
 * unavailable if there's no Wind Gage; within the context of the KEY PROCESSING
 * task when the user tries to access the "Use of Wind" screen.
 * 
 * @return BOOL_32 True if a wind gage is in use; otherwise, false.
 *
 * @author 8/30/2012 Adrianusv
 *
 * @revisions (none)
 */
extern BOOL_32 WEATHER_get_wind_gage_in_use( void )
{
	BOOL_32	rv;

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	// 2/3/2015 ajv : It's possible that the Wind Gage in use flag was set TRUE at some point
	// when a Weather option was installed, but the weather option has since been removed.
	// Therefore, only look at the "in use" value if there's actually a weather option available
	// somewhere.
	if ( WEATHER_there_is_a_weather_option_in_the_chain() )
	{
		rv = SHARED_get_bool( &weather_control.wind_is_there_a_wind_gage,
							  WEATHER_WIND_GAGE_CONNECTED_DEFAULT,
							  &nm_WEATHER_set_wind_gage_connected,
							  &weather_control.changes_to_send_to_master,
							  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_wg_in_use_bit ] );
	}
	else
	{
		// 2/3/2015 ajv : If there's no weather option anywhere in the chain, always return FALSE.
		rv = (false);
	}

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 WEATHER_get_rain_switch_in_use( void )
{
	BOOL_32	rv;

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	// 2/3/2015 ajv : It's possible that the Rain Switch in use flag was set TRUE at some point
	// when a Weather option was installed, but the weather option has since been removed.
	// Therefore, only look at the "in use" value if there's actually a weather option available
	// somewhere.
	if ( WEATHER_there_is_a_weather_option_in_the_chain() )
	{
		rv = SHARED_get_bool( &weather_control.rain_switch_is_there_a_rain_switch,
							  WEATHER_RAIN_SWITCH_IN_USE_DEFAULT,
							  &nm_WEATHER_set_rain_switch_in_use,
							  &weather_control.changes_to_send_to_master,
							  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_in_use_bit ] );
	}
	else
	{
		// 2/3/2015 ajv : If there's no weather option anywhere in the chain, always return FALSE.
		rv = (false);
	}

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 WEATHER_get_SW1_as_rain_switch_in_use( void )
{
	// 3/14/2016 mjb : Call to check and return boolean true if we don't have a full weather
	// option installed AND we have POC card/terminals installed. This will allow us to use SW1
	// as a rain switch.

	BOOL_32	rv;

	UNS_32 i;

	// ----------
	
	// 2/3/2015 ajv : If there's no weather option anywhere in the chain, always return FALSE.
	rv = (false);
	
	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	for( i = 0; i < MAX_CHAIN_LENGTH; ++i )
	{
		// 3/14/2016 mjb : Check that all necessary cards and terminals are either installed or not
		// installed to determine if we are potentially using SW1 as a rain switch.
		//
		// 6/7/2016 ajv : Only look for controllers that were detected in the last scan.
		if( chain.members[ i ].saw_during_the_scan )
		{
			if ((chain.members[ i ].box_configuration.wi.poc.tb_present && chain.members[ i ].box_configuration.wi.poc.card_present) &&
				 (!chain.members[ i ].box_configuration.wi.weather_card_present || !chain.members[ i ].box_configuration.wi.weather_terminal_present))
			{
				rv = SHARED_get_bool( &weather_control.rain_switch_is_there_a_rain_switch,
									  WEATHER_RAIN_SWITCH_IN_USE_DEFAULT,
									  &nm_WEATHER_set_rain_switch_in_use,
									  &weather_control.changes_to_send_to_master,
									  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_in_use_bit ] );
				break;
			}
		}
	}

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 WEATHER_get_rain_switch_connected_to_this_controller( const UNS_32 pindex_0 )
{
	char	lfield_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	BOOL_32	rv;

	if( pindex_0 < MAX_CHAIN_LENGTH )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_controllers_bit ], (pindex_0 + 1) );

		rv = SHARED_get_bool_from_array( &weather_control.rain_switch_controllers_connected_to_switch[ pindex_0 ],
										 pindex_0,
										 MAX_CHAIN_LENGTH,
										 WEATHER_RAIN_SWITCH_CONNECTED_DEFAULT,
										 &nm_WEATHER_set_rain_switch_connected,
										 &weather_control.changes_to_send_to_master,
										 lfield_name );
	}
	else
	{
		Alert_index_out_of_range();

		rv = WEATHER_RAIN_SWITCH_CONNECTED_DEFAULT;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 WEATHER_sw1_is_available_and_enabled_as_a_rain_switch_at_this_controller( const UNS_32 pindex_0 )
{
	// 6/7/2016 rmd : Executed in the context of the comm_mngr task. Is presently only executed
	// upon receipt of and parsing a message from the tpmicro (IRRI SIDE). Is used when deciding
	// to honor the sw1 switch input state as reported by the tpmicro. If this controller has a
	// POC kit, and doesn't have a WEATHER kit, and the top level user setting to use a rain
	// switch is (true), and the rain switch is indicated to be connected to this controller,
	// then after all that we return (true);
	
	// ----------
	
	BOOL_32		rv;
	
	BOOL_32		top_level_enable;
	
	char		lfield_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	// ----------
	
	rv = (false);
	
	// ----------
	
	// 6/7/2016 rmd : Cautionary range check.
	if( pindex_0 < MAX_CHAIN_LENGTH )
	{
		xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );
	
		// ----------
		
		// 6/7/2016 rmd : Has the user set the top level YES or NO about using any kind of a rain
		// switch?
		top_level_enable = SHARED_get_bool(	&weather_control.rain_switch_is_there_a_rain_switch,
											WEATHER_RAIN_SWITCH_IN_USE_DEFAULT,
											&nm_WEATHER_set_rain_switch_in_use,
											&weather_control.changes_to_send_to_master,
											WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_in_use_bit ] );
											
		if( top_level_enable )
		{
			// 6/7/2016 rmd : Because we are only using this function during parsing of the message from
			// the tpmicro the chain members info for this controller MUST be valid. So we do not really
			// need to make this test. However if one was to expand the use of this function to execute
			// from the MASTER (or even at a slave because remember chain_members is valid at all
			// controllers in a chain) investigate if a particular slave has a POC based rain switch
			// enabled this saw_during_the_scan test should be made. So I'll include it for
			// completeness.
			if( chain.members[ pindex_0 ].saw_during_the_scan )
			{
				if ((chain.members[ pindex_0 ].box_configuration.wi.poc.tb_present && chain.members[ pindex_0 ].box_configuration.wi.poc.card_present) &&
					 (!chain.members[ pindex_0 ].box_configuration.wi.weather_card_present || !chain.members[ pindex_0 ].box_configuration.wi.weather_terminal_present))
				{
					snprintf( lfield_name, sizeof(lfield_name), "%s%d", WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_controllers_bit ], (pindex_0 + 1) );
			
					rv = SHARED_get_bool_from_array( &weather_control.rain_switch_controllers_connected_to_switch[ pindex_0 ],
													 pindex_0,
													 MAX_CHAIN_LENGTH,
													 WEATHER_RAIN_SWITCH_CONNECTED_DEFAULT,
													 &nm_WEATHER_set_rain_switch_connected,
													 &weather_control.changes_to_send_to_master,
													 lfield_name );
				}
			}
		}
	
		// ----------
		
		xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );
	}
	else
	{
		Alert_index_out_of_range();
	}

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 WEATHER_get_freeze_switch_in_use( void )
{
	BOOL_32	rv;

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	// 2/3/2015 ajv : It's possible that the Freeze Switch in use flag was set TRUE at some
	// point when a Weather option was installed, but the weather option has since been removed.
	// Therefore, only look at the "in use" value if there's actually a weather option available
	// somewhere.
	if ( WEATHER_there_is_a_weather_option_in_the_chain() )
	{
		rv = SHARED_get_bool( &weather_control.freeze_switch_is_there_a_freeze_switch,
							  WEATHER_FREEZE_SWITCH_IN_USE_DEFAULT,
							  &nm_WEATHER_set_freeze_switch_in_use,
							  &weather_control.changes_to_send_to_master,
							  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_freeze_switch_in_use_bit ] );
	}
	else
	{
		// 2/3/2015 ajv : If there's no weather option anywhere in the chain, always return FALSE.
		rv = (false);
	}

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 WEATHER_get_freeze_switch_connected_to_this_controller( const UNS_32 pindex_0 )
{
	char	lfield_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	BOOL_32	rv;

	if( pindex_0 < MAX_CHAIN_LENGTH )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_freeze_switch_controllers_bit ], (pindex_0 + 1) );

		rv = SHARED_get_bool_from_array( &weather_control.freeze_switch_controllers_connected_to_switch[ pindex_0 ],
										 pindex_0,
										 MAX_CHAIN_LENGTH,
										 WEATHER_FREEZE_SWITCH_CONNECTED_DEFAULT,
										 &nm_WEATHER_set_freeze_switch_connected,
										 &weather_control.changes_to_send_to_master,
										 lfield_name );
	}
	else
	{
		Alert_index_out_of_range();

		rv = WEATHER_RAIN_SWITCH_CONNECTED_DEFAULT;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION To obtaina copy of the wind settings for delivery to the tp micro.

	@CALLER_MUTEX_REQUIREMENTS (none)
	
	@MEMORY_RESPONSIBILITES (none)
	
    @EXECUTED Executed within the context of the COMM_MNGR task when building an outoging
    message for the tp micro.

	@RETURN (none)
	
	@ORIGINAL 2013.03.25 rmd
*/ 
extern void WEATHER_get_a_copy_of_the_wind_settings( WIND_SETTINGS_STRUCT *wss_ptr )
{
	// 3/25/2013 rmd : I am taking the MUTEX here cause we are taking a set of numbers. Not just
	// a single UNS_32. And they outhgt to be taken as a set. It then follows that when these
	// values are moved to/from the guivars the MUTEX also is to be taken!
	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	*wss_ptr = weather_control.wss;

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @executed 
 * 
 * @return UNS_32 
 *
 * @author 7/9/2012 Adrianusv
 *
 * @revisions
 *    7/9/2012 Initial release
 */
extern UNS_32 WEATHER_get_percent_of_historical_cap_100u( void )
{
	UNS_32	rv;

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_uint32( &weather_control.etg_percent_of_historical_cap_100u,
							WEATHER_ET_PERCENT_OF_HISTORICAL_CAP_MIN_100u,
							WEATHER_ET_PERCENT_OF_HISTORICAL_CAP_MAX_100u,
							WEATHER_ET_PERCENT_OF_HISTORICAL_CAP_DEFAULT_100u,
							&nm_WEATHER_set_percent_of_historical_et_cap,
							&weather_control.changes_to_send_to_master,
							WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_etg_historical_cap_bit ] );

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @executed 
 * 
 * @return BOOL_32 
 *
 * @author 7/9/2012 Adrianusv
 *
 * @revisions
 *    7/9/2012 Initial release
 */
extern BOOL_32 WEATHER_get_reference_et_use_your_own( void )
{
	BOOL_32	rv;

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	rv = SHARED_get_bool( &weather_control.reference_et_use_your_own,
						  WEATHER_REFERENCE_ET_USE_YOUR_OWN_DEFAULT,
						  &nm_WEATHER_set_reference_et_use_your_own,
						  &weather_control.changes_to_send_to_master,
						  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_use_your_own_bit ] );

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex 
 *
 * @executed 
 * 
 * @param pmonth1_12 
 * 
 * @return UNS_32 
 *
 * @author 7/9/2012 Adrianusv
 *
 * @revisions
 *    7/9/2012 Initial release
 */
extern UNS_32 WEATHER_get_reference_et_number( const UNS_32 pmonth1_12 )
{
	char	lfield_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	UNS_32	rv;

	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	if( (pmonth1_12 >= 1) && (pmonth1_12 <= MONTHS_IN_A_YEAR) )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_monthlies_bit ], pmonth1_12 );

		rv = SHARED_get_uint32_from_array( &weather_control.reference_et_user_numbers_100u[ (pmonth1_12-1) ],
										   (pmonth1_12-1),
										   MONTHS_IN_A_YEAR,
										   WEATHER_REFERENCE_ET_MIN_100u,
										   WEATHER_REFERENCE_ET_MAX_100u,
										   (UNS_32)ET_PredefinedData[ WEATHER_REFERENCE_ET_DEFAULT_CITY_INDEX ].ET_100u[ (pmonth1_12-1) ],
										   &nm_WEATHER_set_reference_et_number,
										   &weather_control.changes_to_send_to_master,
										   lfield_name );

	}
	else
	{
		Alert_index_out_of_range();

		rv = (UNS_32)ET_PredefinedData[ WEATHER_REFERENCE_ET_DEFAULT_CITY_INDEX ].ET_100u[ 0 ];
	}

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @executed 
 * 
 * @return UNS_32 
 *
 * @author 7/9/2012 Adrianusv
 *
 * @revisions
 *    7/9/2012 Initial release
 */
extern UNS_32 WEATHER_get_reference_et_city_index( void )
{
	UNS_32	rv;

	// 2012.07.18 ajv : No mutex protection is necessary here because the copy
	// is an atomic operation.

	rv = weather_control.reference_et_city_index;

	RangeCheck_uint32( &rv, WEATHER_REFERENCE_ET_CITY_MIN, WEATHER_REFERENCE_ET_CITY_MAX, WEATHER_REFERENCE_ET_CITY_DEFAULT, NULL, "Reference ET City" );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called from TD_CHECK every second as part of the et and rain table roll
    maintenance functions.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task once per second.
	
	@RETURN (none)

	@ORIGINAL 2012.09.14 rmd

	@REVISIONS (none)
*/
extern UNS_32 WEATHER_get_et_and_rain_table_roll_time( void )
{
	UNS_32	rv;

	// 2012.07.18 ajv : No mutex protection is necessary here because the copy
	// is an atomic operation.
	rv = weather_control.et_and_rain_table_roll_time;

	RangeCheck_uint32( &rv, WEATHER_ET_RAIN_TABLE_ROLL_TIME_MINIMUM, WEATHER_ET_RAIN_TABLE_ROLL_TIME_MAXIMUM, WEATHER_ET_RAIN_TABLE_ROLL_TIME_DEFAULT, NULL, "ET roll time" );

	return( rv );
}

/* ---------------------------------------------------------- */
extern CHANGE_BITS_32_BIT *WEATHER_get_change_bits_ptr( const UNS_32 pchange_reason )
{
	return( SHARED_get_32_bit_change_bits_ptr( pchange_reason, &weather_control.changes_to_send_to_master, &weather_control.changes_to_distribute_to_slaves, &weather_control.changes_to_upload_to_comm_server ) );
}

/* ---------------------------------------------------------- */
extern void WEATHER_CONTROL_set_bits_on_all_settings_to_cause_distribution_in_the_next_token( void )
{
	// 5/1/2014 rmd : This function ONLY being called at the conclusion of a SCAN by the master
	// of a real multiple controller chain - we want to broadcast to the slaves using tokens
	// about all the hardware we presently know about.
	SHARED_set_all_32_bit_change_bits( &weather_control.changes_to_distribute_to_slaves, weather_control_recursive_MUTEX );

	// ----------

	// 4/29/2014 ajv : Set a flag in the changes structure to notify this
	// controller to examine its lists for changes to distribute to the other
	// controllers in the chain in it's next token resposne. The flag is part of
	// the mutex protected comm_mngr struct so take the MUTEX.
	xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

	comm_mngr.changes.distribute_changes_to_slave = (true);

	xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void WEATHER_on_all_settings_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear )
{
	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

	if( pset_or_clear == COMMSERVER_CHANGE_BITS_ALL_CLEARED )
	{
		SHARED_clear_all_32_bit_change_bits( &weather_control.changes_to_upload_to_comm_server, weather_control_recursive_MUTEX );
	}
	else
	{
		SHARED_set_all_32_bit_change_bits( &weather_control.changes_to_upload_to_comm_server, weather_control_recursive_MUTEX );
	}

	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );
}


/* ---------------------------------------------------------- */
/**
 * @mutex_requirements Requires the caller to have the pending_changes_recursive_MUTEX to
 * ensure no other function tries to modify the pending_changes_to_send_to_comm_server flag.
 *
 * @task_execution Executed within the context of the CONTROLLER INITIATED task when it
 * first starts and when the waiting response flags are cleared after a communication.
 */
extern void nm_WEATHER_update_pending_change_bits( const UNS_32 pcomm_error_occurred )
{
	// 4/30/2015 ajv : No reason to process this if none of the bits are set. This also helps
	// reduce the file save activity.
	if( weather_control.changes_uploaded_to_comm_server_awaiting_ACK != CHANGE_BITS_32_BIT_ALL_CLEAR )
	{
		// 4/29/2015 ajv : If a communication error occurred while sending Program Data, copy the
		// bits of variables that were sent back into the original bit field to ensure they're sent
		// again during the next attempt. Otherwise, clear the pending bits.
		if( pcomm_error_occurred )
		{
			xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );

			weather_control.changes_to_upload_to_comm_server |= weather_control.changes_uploaded_to_comm_server_awaiting_ACK;

			xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );

			// 4/30/2015 ajv : Since we've determined there were changes that were not successfully
			// sent, set the flag to trigger another attempt
			weather_preserves.pending_changes_to_send_to_comm_server = (true);

			// 4/30/2015 ajv : Also set the timer to force the data to send. If the timer's already
			// running, this will simply restart it.
			// 
			// 4/30/2015 ajv : BECAUSE of the potential that this function is executed many times (once
			// for each item changed) we could not start our pdata timer with a post to the CI queue.
			// The queue would overflow. So start the timer directly.
			xTimerChangePeriod( cics.pdata_timer, MS_to_TICKS( CI_PDATA_TIMER_CHANGES_NOT_SENT_MS ), portMAX_DELAY );
		}
		else
		{
			SHARED_clear_all_32_bit_change_bits( &weather_control.changes_uploaded_to_comm_server_awaiting_ACK, weather_control_recursive_MUTEX );
		}

		// 4/29/2015 ajv : Now that the bits are set or cleared accordingly, trigger a file save.
		// If power fails between building the message and receiving the ACK, the original bit field
		// hasn't been saved to flash yet, so it will resend the associated variables during the
		// next attempt.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_WEATHER_CONTROL, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
	}
}

/* ---------------------------------------------------------- */
BOOL_32 WEATHER_CONTROL_calculate_chain_sync_crc( UNS_32 const pff_name )
{
	// 8/30/2018 rmd : Executed in the context of the comm_mngr task when token responses are
	// being made. And that's it.

	// ----------

	UNS_8				*checksum_start;

	UNS_32				checksum_length;

	UNS_8				*ucp;
	
	BOOL_32				rv;

	// ----------

	rv = (false);

	// ----------
	
	xSemaphoreTakeRecursive( weather_control_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	// 8/30/2018 rmd : So the CRC calculation function wants a continuous memory space and a
	// length to calculate the crc off. In this function we will pick out the desired data, and
	// simply pile it up on itself, list item by list item. We don't care how messy the pile is
	// because we never attempt to reference any data once it is added to the pile. The data is
	// only there to include in the checksum.
	//
	// 9/27/2018 rmd : In this case the weather control is a single variable, no list involved.
	// So handle appropriately.
	if( mem_obtain_a_block_if_available( sizeof(WEATHER_CONTROL_STRUCT), (void**)&checksum_start ) )
	{
		// 9/5/2018 rmd : Well then the calculation will be successful. Nothing else can prevent
		// that outcome.
		rv = (true);
		
		// ----------
		
		ucp = checksum_start;
		
		checksum_length = 0;
		
		// ----------
		
		// 9/4/2018 rmd : Make sure you include only TRUE USER SETTINGS, no flags, or pointers to
		// memory, etc...
		
		// ----------
		
		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.et_and_rain_table_roll_time, sizeof(weather_control.et_and_rain_table_roll_time) );


		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.rain_bucket_box_index, sizeof(weather_control.rain_bucket_box_index) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.rain_bucket_is_there_a_rain_bucket, sizeof(weather_control.rain_bucket_is_there_a_rain_bucket) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.rain_bucket_minimum_inches_100u, sizeof(weather_control.rain_bucket_minimum_inches_100u) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.rain_bucket_max_hourly_inches_100u, sizeof(weather_control.rain_bucket_max_hourly_inches_100u) );
		
		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.rain_bucket_max_24_hour_inches_100u, sizeof(weather_control.rain_bucket_max_24_hour_inches_100u) );


		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.etg_box_index, sizeof(weather_control.etg_box_index) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.etg_is_in_use, sizeof(weather_control.etg_is_in_use) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.etg_log_et_pulses, sizeof(weather_control.etg_log_et_pulses) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.etg_percent_of_historical_cap_100u, sizeof(weather_control.etg_percent_of_historical_cap_100u) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.etg_percent_of_historical_min_100u, sizeof(weather_control.etg_percent_of_historical_min_100u) );


		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.wind_gage_box_index, sizeof(weather_control.wind_gage_box_index) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.wind_is_there_a_wind_gage, sizeof(weather_control.wind_is_there_a_wind_gage) );


		// 9/27/2018 rmd : NOTE - the pause and resume MINUTES are not sync'd - so do not inlcude

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.wss.wind_pause_mph, sizeof(weather_control.wss.wind_pause_mph) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.wss.wind_resume_mph, sizeof(weather_control.wss.wind_resume_mph) );


		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.reference_et_use_your_own, sizeof(weather_control.reference_et_use_your_own) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.reference_et_user_numbers_100u, sizeof(weather_control.reference_et_user_numbers_100u) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.reference_et_state_index, sizeof(weather_control.reference_et_state_index) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.reference_et_county_index, sizeof(weather_control.reference_et_county_index) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.reference_et_city_index, sizeof(weather_control.reference_et_city_index) );

		// 11/2/2018 rmd : Due to the inconsistent way the group names strings are handled during
		// the sync (we do not regligiously take all 48 characters and always carry them through the
		// sync process, there are places we only copy up until the null), the part of the string
		// following the null char is not guaranteed to be in sync. So only include up until the
		// null in the crc test.
		//
		// 11/2/2018 rmd : The same argument about group names strings should apply here, though
		// might not be needed, but that's okay. All we care about being in sync is up to the NULL
		// char, so only include that much, making sure we avoid testing the chars after the NULL.
		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.user_defined_state, strlen(weather_control.user_defined_state) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.user_defined_county, strlen(weather_control.user_defined_county) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.user_defined_city, strlen(weather_control.user_defined_city) );


		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.rain_switch_is_there_a_rain_switch, sizeof(weather_control.rain_switch_is_there_a_rain_switch) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.rain_switch_controllers_connected_to_switch, sizeof(weather_control.rain_switch_controllers_connected_to_switch) );


		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.freeze_switch_is_there_a_freeze_switch, sizeof(weather_control.freeze_switch_is_there_a_freeze_switch) );

		checksum_length += CHAIN_SYNC_push_data_onto_block( &ucp, &weather_control.freeze_switch_controllers_connected_to_switch, sizeof(weather_control.freeze_switch_controllers_connected_to_switch) );


		// skipping over the 4 change bit fields
		
		// END
		
		// ----------
		
		// 8/30/2018 rmd : Now calculate the checksum.
		cscs.the_crc[ pff_name ] = CRC_calculate_32bit_big_endian( checksum_start, checksum_length );
		
		// ----------
		
		// 9/4/2018 rmd : We're done with the block, so of course free it.
		mem_free( checksum_start );
	}
	else
	{
		// 9/5/2018 rmd : So by design it's okay if there is no memory. The checksum won't be set
		// valid, and the next time we try to include it in a token response we'll try to calculate
		// it again. Self protecting and retrying. But let's alert about this so we know it's taking
		// place.
		Alert_Message_va( "SYNC: no mem to calc checksum %s", chain_sync_file_pertinants[ pff_name ].file_name_string );
	}
	
	// ----------
	
	xSemaphoreGiveRecursive( weather_control_recursive_MUTEX );
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

