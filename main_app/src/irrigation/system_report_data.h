/*  file = system_report_data.h               09.04.2012  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_SYSTEM_REPORT_DATA_H
#define _INC_SYSTEM_REPORT_DATA_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"battery_backed_vars.h"

#include	"cal_td_utils.h"

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/4/2012 rmd : We used to keep 34 days worth in the ET2000e. But that was limited by
// battery backed storage. In the CS3000 we only keep the rip in battery backed. The
// completed records are held in SDRAM and stored within the flash file system. Figure we
// want in the worst case (4 systems) 6 months. So 6 * 30 * 4 = 720 system report records.
// For the case of a single system this is almost two years. Which seems alot. So I'll
// throttle that back some. To 480.
#define	SYSTEM_REPORT_RECORDS_TO_KEEP		(480)

// -----------------------------

typedef struct
{
	REPORT_DATA_FILE_BASE_STRUCT	rdfb;

	// ----------
	
	SYSTEM_REPORT_RECORD			srr[ SYSTEM_REPORT_RECORDS_TO_KEEP ];

} COMPLETED_SYSTEM_REPORT_RECORDS_STRUCT;

// Held in SDRAM and saved to a file in the GENERAL STORAGE flash device.
extern COMPLETED_SYSTEM_REPORT_RECORDS_STRUCT	system_report_data_completed;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// ---------------------

extern void init_file_system_report_records( void );

extern void save_file_system_report_records( void );

// ---------------------

extern SYSTEM_REPORT_RECORD *nm_SYSTEM_REPORT_RECORDS_get_previous_completed_record( SYSTEM_REPORT_RECORD *psrr_ptr );

extern SYSTEM_REPORT_RECORD *nm_SYSTEM_REPORT_RECORDS_get_most_recently_completed_record( void );

// ---------------------

extern UNS_32 FDTO_SYSTEM_REPORT_fill_ptrs_and_return_how_many_lines( const UNS_32 plist_index_0 );

extern void FDTO_SYSTEM_REPORT_load_guivars_for_scroll_line( const INT_16 pline_index_0_i16 );

// ---------------------

extern void SYSTEM_REPORT_DATA_start_the_ci_timer_if_it_is_not_running( void );

extern void nm_SYSTEM_REPORT_DATA_inc_index( UNS_32 *pindex_ptr );

extern void nm_SYSTEM_REPORT_close_and_start_a_new_record( BY_SYSTEM_RECORD *pbsr_ptr, const DATE_TIME *pdate_time );

extern void SYSTEM_REPORT_DATA_generate_lines_for_DEBUG( void );

// ---------------------

extern void SYSTEM_REPORT_free_report_support( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

