/*   file = station_report_data.c            08.02.2010  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memset
#include	<string.h>

#include	"station_report_data.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"controller_initiated.h"

#include	"cs_mem.h"

#include	"d_process.h"

#include	"epson_rx_8025sa.h"

#include	"flash_storage.h"

#include	"r_station_report.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

COMPLETED_STATION_REPORT_DATA_STRUCT	station_report_data_completed;	// Held in SDRAM and saved to a file in the GENERAL STORAGE flash device.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/27/2012 rmd : Support for displaying the station history and station report data. This
// is a pointer to a dynamic block of memory that is acquired when we enter either report
// display screen and freed when we leave the respective screen. The memory holds a ptr to
// each of the records being displayed. We count on the C startup code to intialize this to
// a NULL.
static STATION_REPORT_DATA_RECORD	*(*station_report_data_ptrs)[];
 
// ----------

// filenames ARE ALLOWED 32 CHARS ... truncated if exceeds this limit

const char STATION_REPORT_DATA_FILENAME[] =	"STATION_REPORT_RECORDS";
 
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called as part of the station_preserves initial verification during
    startup. Also called at each roll time for the station report data to restart the report
    data rip in the station preserves.

    @CALLER_MUTEX_REQUIREMENTS The caller is to be holding the appropriate MUTEX. Given
    where the partiuclar record is located. Either battery backed SRAM or in the
    SDRAM.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the APP_STARTUP task. And from the TD_CHECK
    task. Daily at the station report data roll time as specified within the station report
    data file.

	@RETURN (none)

	@ORIGINAL 2012.06.04 rmd

	@REVISIONS (none)
*/
extern void nm_init_station_report_data_record( STATION_REPORT_DATA_RECORD *psrdr_ptr )
{
	memset( psrdr_ptr, 0, sizeof(STATION_REPORT_DATA_RECORD) );
}

/* ---------------------------------------------------------- */
static void nm_init_station_report_data_records( void )
{
	// 1/23/2015 rmd : Function caller is to be holding the station_report_data_completed MUTEX.
	// This function is only called within the context of the STARTUP task via the initial file
	// check.

	// ----------

	UNS_32	i;

	// ----------

	// 1/23/2015 rmd : This sets all the control variables to 0 or (false). And the roll time to
	// midnight. Which is where we want it.
	memset( &station_report_data_completed.rdfb, 0x00, sizeof(REPORT_DATA_FILE_BASE_STRUCT) );
	
	// ----------
	
	for( i=0; i<(STATION_REPORT_DATA_MAX_RECORDS); i++ )
	{
		nm_init_station_report_data_record( &(station_report_data_completed.srdr[ i ]) );
	}
		
	// ----------
	
	// 8/15/2014 rmd : The variable that control which records to send are saved with the file.
	// And that is important to recognize. When the ACK is rcvd the file must be saved. In order
	// to lock in those sent if there were a power failure. The nature of this is if there were
	// a power fail after the ACK was rcvd but before the file was saved we would send the lines
	// twice. Which is okay. We just don't want to do that all the time.
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 1/21/2015 rmd : This is the structure revision number. It started at 0. And should be
// incremented by 1 as changes are made to the SIZE OF OR COUNT OF the report data record or
// station history record.
// 
// 1/22/2015 rmd : VERSION 1 - this was a file re-definition that affected all report data
// type files. And resulted in a complete intialization of the file. The goal being to
// restructure the file contents to better support report record revisions in the future.
// This update, from revision 0, triggered a complete file intialization.

#define	STATION_REPORT_DATA_LATEST_FILE_REVISION	(1)

// ----------

const UNS_32 station_report_data_revision_record_sizes[ STATION_REPORT_DATA_LATEST_FILE_REVISION + 1 ] =
{
	// 1/22/2015 rmd : The original base file. Which is going to be standardized to our new
	// report file format. The restructuring to the standard format creates revision 1.
	sizeof( STATION_REPORT_DATA_RECORD ),

	// 1/22/2015 rmd : The move to version 1 is to restructure the file contents to or new
	// standardized report file format. There was no report record size change.
	sizeof( STATION_REPORT_DATA_RECORD )
};

// ----------

const UNS_32 station_report_data_revision_record_counts[ STATION_REPORT_DATA_LATEST_FILE_REVISION + 1 ] =
{
	// 1/22/2015 rmd : The original base file. Which is going to be standardized to our new
	// report file format. The restructuring to the standard format creates revision 1.
	STATION_REPORT_DATA_MAX_RECORDS,

	// 1/22/2015 rmd : The move to version 1 is to restructure the file contents to or new
	// standardized report file format. There was no report record count change.
	STATION_REPORT_DATA_MAX_RECORDS
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/15/2014 rmd : Store the timer outside the file structure. It is a dynamic memory
// allocation. And not desirable nor needed to preserve through a power failure.
static xTimerHandle		station_report_data_ci_timer;
	
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
	
/* ---------------------------------------------------------- */
static void nm_station_report_data_updater( UNS_32 pfrom_revision )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only when
	// the file revision is changed. The caller is to be holding the station report data
	// completed records recursive_MUTEX.

	// ----------
	
	if( pfrom_revision == STATION_REPORT_DATA_LATEST_FILE_REVISION )
	{
		Alert_Message_va( "STA_REPRT_DATA file unexpd update %u", pfrom_revision );	
	}
	else
	{
		Alert_Message_va( "STA_REPRT_DATA file update : to revision %u from %u", STATION_REPORT_DATA_LATEST_FILE_REVISION, pfrom_revision );

		// ----------
		
		if( pfrom_revision == 0 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 0 to REVISION 1. Moving
			// up from revision 0 requires a complete file restart. The file content is invalid due to
			// the restructuring to the standardized report file format.
			nm_init_station_report_data_records();
			
			// ----------
			
			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 1 to 2 changes if
			// needed.
			pfrom_revision += 1;
		}
		
		// ----------
		
		/*
		if( pfrom_revision == 1 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 1 to REVISION 2.
				


			pfrom_revision += 1;
		}
		*/
	}

	// ----------

	// 1/29/2015 rmd : Well after the updater has run the pfrom_revision is supposed to equal
	// the latest revision. By design of this update process.
	if( pfrom_revision != STATION_REPORT_DATA_LATEST_FILE_REVISION )
	{
		Alert_Message( "STA_REPRT_DATA updater error" );
	}
}

/* ---------------------------------------------------------- */
extern void init_file_station_report_data( void )
{
	// 8/15/2014 rmd : This function is called on each and every startup - and ONLY then. Within
	// the context of the app_startup task.
	
	FLASH_FILE_find_or_create_reports_file(	FLASH_INDEX_1_GENERAL_STORAGE,
											STATION_REPORT_DATA_FILENAME,
											STATION_REPORT_DATA_LATEST_FILE_REVISION,

											&station_report_data_completed,

											station_report_data_revision_record_sizes,
											station_report_data_revision_record_counts,
											sizeof( COMPLETED_STATION_REPORT_DATA_STRUCT ),

											station_report_data_completed_records_recursive_MUTEX,

											&nm_station_report_data_updater,
											&nm_init_station_report_data_records,

											FF_STATION_REPORT_DATA );
												
	// ----------
	
	// 9/5/2014 rmd : Because we have included the controller initiated control variables in the
	// saved file there is some consideration here. First of all the first_to_send is assumed to
	// be valid and up to date. I think there are no real holes here. If the file didn't save
	// after closing and adding a line (power failure) the first to send may not be accurate. Or
	// if after receiving the data ACK from the web_app there is a power failure before the file
	// is saved we would send dups to the web_app. But that causes no harm. The problem to watch
	// out for is that the pending_first_send_in_use is saved into the file set (true). That
	// could happen I suppose. So that variable in particular on STARTUP should be set to
	// (false).
	station_report_data_completed.rdfb.pending_first_to_send_in_use = (false);
}

/* ---------------------------------------------------------- */
extern void save_file_station_report_data( void )
{
	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE,
															STATION_REPORT_DATA_FILENAME,
															STATION_REPORT_DATA_LATEST_FILE_REVISION,
															&station_report_data_completed,
															sizeof( station_report_data_completed ),
															station_report_data_completed_records_recursive_MUTEX,
															FF_STATION_REPORT_DATA );
}

/* ---------------------------------------------------------- */
static void station_report_data_ci_timer_callback( xTimerHandle pxTimer )
{
	// 10/21/2013 rmd : Remember this is executed within the context of the high priority timer
	// task. And is executed when it it time to send some station history records. Which is
	// normally 15 minutes after a record is closed.
	CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_station_report_data, NULL, CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD, CI_POST_TO_BACK );
}

/* ---------------------------------------------------------- */
extern void STATION_REPORT_DATA_start_the_ci_timer_if_it_is_not_running( void )
{
	// 8/27/2014 rmd : On startup the station report data ci timer pointer is NULL. If so let's
	// create the timer. This is a convienient way to create the timer before use.
	if( station_report_data_ci_timer == NULL )
	{
		// 7/13/2017 rmd : The time used when the timer is created is not important as we always
		// explicitly set a time when we start the timer.
		station_report_data_ci_timer = xTimerCreate( (const signed char*)"", MS_to_TICKS(60000), FALSE, (void*)NULL, station_report_data_ci_timer_callback );
			
		if( station_report_data_ci_timer == NULL )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "Timer NOT CREATED" );
		}
	}
	
	// ----------
	
	// 8/27/2014 rmd : If it exists do what we came for. Start the timer.
	if( station_report_data_ci_timer != NULL )
	{
		// 8/15/2014 rmd : Only if it is not running. Which means effectively a maximum of 15
		// minutes after a record is closed it will be sent.
		if( xTimerIsTimerActive( station_report_data_ci_timer ) == pdFALSE )
		{
			// 8/15/2014 rmd : Start the timer to trigger the controller initiated send.
			xTimerChangePeriod( station_report_data_ci_timer, MS_to_TICKS( CONTROLLER_INITIATED_ms_after_midnight_to_send_this_controllers_daily_records( config_c.serial_number ) ), portMAX_DELAY );
		}
	}
}

/* ---------------------------------------------------------- */
extern void nm_STATION_REPORT_DATA_inc_index( UNS_32 *pindex_ptr )
{
	// 8/15/2014 rmd : This function is used by the controller initiated task as well as here
	// within this file when closing a record (that is in the context of the TD_CHECK task).

	*pindex_ptr += 1;
	
	if( *pindex_ptr >= STATION_REPORT_DATA_MAX_RECORDS )
	{
		// 6/26/2012 rmd : Okay so we have gone past the end of the array. Wrap around.
		*pindex_ptr = 0;
	}
}

/* ---------------------------------------------------------- */
static void nm_STATION_REPORT_DATA_increment_next_avail_ptr( void )
{
	nm_STATION_REPORT_DATA_inc_index( &station_report_data_completed.rdfb.index_of_next_available );

	if( station_report_data_completed.rdfb.index_of_next_available == 0 )
	{
		// 6/26/2012 rmd : Okay so we have gone past the end of the array and wrapped. Set the
		// wrapped indication (true).
		station_report_data_completed.rdfb.have_wrapped = (true);
	}
	
	// ----------
	
	// 8/14/2014 rmd : If we have bumped into the first to send move it along too.
	if( station_report_data_completed.rdfb.index_of_next_available == station_report_data_completed.rdfb.first_to_send )
	{
		nm_STATION_REPORT_DATA_inc_index( &station_report_data_completed.rdfb.first_to_send );
	}

	// 10/21/2013 rmd : Also if next available has landed on the pending_first_to_send it means
	// that during the CI session we have actually written so many lines we wrapped all the way
	// around. In other words we wrote more lines than we hold during the session (which during
	// normal operation is impossible - a debug function used during development could do this).
	// So move that index too.
	if( station_report_data_completed.rdfb.index_of_next_available == station_report_data_completed.rdfb.pending_first_to_send )
	{
		nm_STATION_REPORT_DATA_inc_index( &station_report_data_completed.rdfb.pending_first_to_send );
	}
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This function is to be used in conjunction with the accompanying 'get
    first' function. Following the get first the user may repeatedly call the get next
    function until a NULL is returned. That will signal that he has seen all the completed
    lines. The entire get first and get next sequence is to be encapsulated within a take
    and give mutex pair for the station report data mutex. The pointer argument points to
    the last record seen.
	
    @CALLER_MUTEX_REQUIREMENTS The caller is to be holding the station report data completed
    records mutex during the entire iterative process calling this function again and again.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the DISPLAY PROCESSING task. When drawing the
    report data to the screen. May also be used in the COMM_MNGR as part of send the report
    records to the central.

    @RETURN If the last record (oldest) has been encountered will return a NULL. Otherwise a
    pointer to the next older record. The records are returned in reverse chronological
    order.

	@ORIGINAL 2012.06.29 rmd 

	@REVISIONS (none)
*/
extern STATION_REPORT_DATA_RECORD *nm_STATION_REPORT_DATA_get_previous_completed_record( STATION_REPORT_DATA_RECORD *psrdr_ptr )
{
	STATION_REPORT_DATA_RECORD	*rv;

	if( (psrdr_ptr < &(station_report_data_completed.srdr[ 0 ])) || (psrdr_ptr >= &(station_report_data_completed.srdr[ STATION_REPORT_DATA_MAX_RECORDS ])) )
	{
		// 6/26/2012 rmd : Out of range. Do not use.
		rv = NULL;
	}
	else
	if( station_report_data_completed.rdfb.have_returned_next_available_record == (true) )
	{
		// 6/29/2012 rmd : If we have wrapped all the way around and are back to where we started
		// then we have returned all possible records.
		rv = NULL;	
	}
	else
	{
		if( psrdr_ptr == &(station_report_data_completed.srdr[ 0 ]) )
		{
			if( station_report_data_completed.rdfb.have_wrapped == (true) )
			{
				// 6/26/2012 rmd : Back up at the top.
				rv = &(station_report_data_completed.srdr[ (STATION_REPORT_DATA_MAX_RECORDS - 1) ]);
			}
			else
			{
				rv = NULL;
			}
		}
		else
		{
			rv = psrdr_ptr;
	
			rv -= 1;
		}
	}
	
	if( rv == &(station_report_data_completed.srdr[ station_report_data_completed.rdfb.index_of_next_available ]) )
	{
		// 6/29/2012 rmd : If we've returned records all the way back to the start indicate this is
		// the last one. So the next call to this function will return a NULL.
		station_report_data_completed.rdfb.have_returned_next_available_record = (true);
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Call this function first. In preparation to retrieve a ptr to each of the
    completed lines by repeatedly calling the accompanying 'get next' function. If there are
    no lines this function returns a NULL. Otherwise a ptr to the first (newest) completed
    line.
	
    @CALLER_MUTEX_REQUIREMENTS The caller is to be holding the station report data completed
    records mutex during this and the entire iterative process calling the get next
    function.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the DISPLAY PROCESSING task. When drawing the
    report data to the screen. May also be used in the COMM_MNGR as part of send the report
    records to the central.

    @RETURN The return value points to the most recently completed record if there is one.
    Otherwise NULL - meaning there are no completed records.

	@ORIGINAL 2012.06.29 rmd 

	@REVISIONS (none)
*/
extern STATION_REPORT_DATA_RECORD *nm_STATION_REPORT_DATA_get_most_recently_completed_record( void )
{
	STATION_REPORT_DATA_RECORD	*rv;
	
	// 6/29/2012 rmd : And set the logic to support the repetitive extraction calls.
	station_report_data_completed.rdfb.have_returned_next_available_record = (false);
	
	rv = nm_STATION_REPORT_DATA_get_previous_completed_record( &(station_report_data_completed.srdr[ station_report_data_completed.rdfb.index_of_next_available ]) );
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Using the station and serial number argument counts how many matching lines
    are in the database. And during that process maintains an array of ptrs to each of the
    records.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the DISPLAY task. In preparation for screen
    draw and once per second as part of a screen update.

    @RETURN The matching number of lines. Will always be at least 1 as the rip always
    exists.

	@ORIGINAL 2012.07.27 rmd

	@REVISIONS (none)
*/
extern UNS_32 STATION_REPORT_DATA_fill_ptrs_and_return_how_many_lines( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0 )
{
	UNS_32	rv;
	
	rv = 0;

	// 8/1/2012 rmd : Besides running through the completed records finding matches we should be
	// concerned with the report_ptrs memory allocation. Suppose one were to "change" screens
	// right at the time td_check was running through filling the array with ptrs. That could be
	// a big problem is the the change screen routine "freed" the memory and set poc_report_ptr
	// to NULL before this function completed.
	xSemaphoreTakeRecursive( station_report_data_completed_records_recursive_MUTEX, portMAX_DELAY );

	// ----------------------

	if( station_report_data_ptrs == NULL )
	{
		// 7/27/2012 rmd : Oh just grab the maximum size ever necessary. Otherwise we get into the
		// potential of having to free the memory and re-allocing as the number of lines grows. That
		// could be done but I decided to just grab the worst case up front. Also we'd have to go
		// through twice once to learn how many. And once to fill the array.
		// 7/27/2012 rmd : Oh and the story gets even more complicated. Cause if the database is
		// full and another line is added the count of the number of lines for this station may stay
		// the same but the ptrs to the lines are now different? Not sure about that thought.
		station_report_data_ptrs = mem_malloc( STATION_REPORT_DATA_MAX_RECORDS * sizeof(void *) );
	}
	
	// ----------------------

	// 7/27/2012 rmd : Always load the ptr to the rip.
	UNS_32	lindex;
	
	STATION_PRESERVES_get_index_using_box_index_and_station_number( pbox_index_0, pstation_number_0, &lindex );
	
	(*station_report_data_ptrs)[ rv++ ] = &(station_preserves.sps[ lindex ].station_report_data_rip);
	
	// ----------------------

	STATION_REPORT_DATA_RECORD	*lrecord;

	lrecord = nm_STATION_REPORT_DATA_get_most_recently_completed_record();

	while( lrecord != NULL )
	{
		if( (lrecord->box_index_0 == pbox_index_0) && (lrecord->station_number == pstation_number_0) )
		{
			(*station_report_data_ptrs)[ rv++ ] = lrecord;
		}

		// 7/27/2012 rmd : So as to not write beyond our allocated space.
		if( rv >= STATION_REPORT_DATA_MAX_RECORDS )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "REPORTS: why so many records?" );
			
			break ;
		}

		lrecord = nm_STATION_REPORT_DATA_get_previous_completed_record( lrecord );
	}

	// ----------------------

	xSemaphoreGiveRecursive( station_report_data_completed_records_recursive_MUTEX );

	// ----------------------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Draws an individual scroll line for the Station Report. This is used as the
    callback routine for the GuiLib_ScrollBox_Init routine. And uses the array of pointers
    created in the STATION_REPORT_DATA_fill_ptrs_and_return_how_many_lines function.
	
    @CALLER_MUTEX_REQUIREMENTS Calling function must have the
    station_report_data_completed_records_recursive_MUTEX to ensure no records are added or
    changed during this process.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the DISPLAY processing task when the screen is
    drawn; within the context of the key processing task when the user navigates up or down
    the scroll box.

	@RETURN (none)

	@ORIGINAL 2012.07.27 rmd

	@REVISIONS (none)
*/
extern void STATION_REPORT_draw_scroll_line( const INT_16 pline_index_0_i16 )
{
	// 8/1/2012 rmd : Taking this completed records MUTEX protects the array of ptrs from
	// changing if we happen to be viewing at the roll time. Additionally protect from the
	// memory being freed using this MUTEX.
	xSemaphoreTakeRecursive( station_report_data_completed_records_recursive_MUTEX, portMAX_DELAY );

	// 8/2/2012 rmd : Is the ptr NULL? Being careful. Cheap insurance code space wise.
	if( station_report_data_ptrs != NULL )
	{
		STATION_REPORT_DATA_RECORD	*lrecord;
		
		lrecord = (*station_report_data_ptrs)[ pline_index_0_i16 ];
	
		// --------------
		
		char	dt_buf[ 16 ];
	
		strlcpy( GuiVar_RptDate, GetDateStr( dt_buf, sizeof(dt_buf), lrecord->record_start_date, DATESTR_show_year_not, DATESTR_show_dow_not ), sizeof(GuiVar_RptDate) );
	
		// --------------
		
		GuiVar_RptIrrigMin = ((float)(lrecord->programmed_irrigation_seconds_irrigated_ul) / 60.0F);
		GuiVar_RptIrrigGal = lrecord->programmed_irrigation_gallons_irrigated_fl;
	
		GuiVar_RptManualPMin = ((float)(lrecord->manual_program_seconds_us) / 60.0F);
		GuiVar_RptManualPGal = lrecord->manual_program_gallons_fl;
	
		GuiVar_RptManualMin = ((float)(lrecord->manual_seconds_us) / 60.0F);
		GuiVar_RptManualGal = lrecord->manual_gallons_fl;
	
		GuiVar_RptTestMin = ((float)(lrecord->test_seconds_us) / 60.0F);
		GuiVar_RptTestGal = lrecord->test_gallons_fl;
	
		GuiVar_RptWalkThruMin = ((float)(lrecord->walk_thru_seconds_us) / 60.0F);
		GuiVar_RptWalkThruGal = lrecord->walk_thru_gallons_fl;
	
		GuiVar_RptRReMin = ((float)(lrecord->mobile_seconds_us) / 60.0F);
		GuiVar_RptRReGal = lrecord->mobile_gallons_fl;
	}	

	xSemaphoreGiveRecursive( station_report_data_completed_records_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Take the existing record referenced by the pointer and merge into the
    completed lines database.

    @CALLER_MUTEX_REQUIREMENTS The caller is to be holding the station preserves mutex.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task. Each day at the station
    report data roll time (midnight? start of irrigation day?).
	
	@RETURN (none)

	@ORIGINAL 2012.06.29 rmd

	@REVISIONS (none)
*/
extern void nm_STATION_REPORT_DATA_close_and_start_a_new_record( STATION_REPORT_DATA_RECORD *psrdr_ptr, const DATE_TIME *pdate_time )
{
	// 10/3/2014 rmd : REMEMBER the caller is to be holding the station preserves recursive
	// MUTEX. Because we are using and initializing the rip which is stored in the preserves.

	// ----------
	
	// 8/1/2012 rmd : As we are writing to the completed records base take the MUTEX.
	xSemaphoreTakeRecursive( station_report_data_completed_records_recursive_MUTEX, portMAX_DELAY );

	// 6/26/2012 rmd : Move the rip into the completed.
	station_report_data_completed.srdr[ station_report_data_completed.rdfb.index_of_next_available ] = *psrdr_ptr;
	
	nm_STATION_REPORT_DATA_increment_next_avail_ptr();

	// 8/1/2012 rmd : Done with the requirement to hold the MUTEX. The file save activity
	// re-takes the MUTEX when needed.
	xSemaphoreGiveRecursive( station_report_data_completed_records_recursive_MUTEX );

	// ----------
	
	// 8/15/2014 rmd : Start the ci timer if it is not running.
	STATION_REPORT_DATA_start_the_ci_timer_if_it_is_not_running();

	// ----------
	
	// 6/26/2012 rmd : Schedule a save of the station report data file. In 2 seconds. For each
	// station that hits a start time we call this. The idea is it gets called over and over
	// again once per station and restarts the timer. So it doesn't expire until we've made all
	// our lines. And we do a single file save 2 seconds after the last request.
	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_STATION_REPORT_DATA, FLASH_STORAGE_seconds_to_delay_file_save_after_rolling_report_data );

	// ----------
	
	// 8/1/2012 rmd : Restarting the rip record. The caller is to be holding the station
	// preserves MUTEX.
	nm_init_station_report_data_record( psrdr_ptr );

	// 6/4/2012 rmd : Stamp the record origin. The time will be the roll time.
	psrdr_ptr->record_start_date = pdate_time->D;
	psrdr_ptr->record_start_time = pdate_time->T;

	// ----------
	
	// 10/29/2013 ajv : If the user is viewing the Station Summary report when a
	// new line is added, force the scroll box to be updated.
	if( GuiLib_CurStructureNdx == GuiStruct_rptStationSummary_0 )
	{
		DISPLAY_EVENT_STRUCT	lde;

		lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
		lde._04_func_ptr = &FDTO_STATION_REPORT_redraw_scrollbox;
		Display_Post_Command( &lde );
	}
}

/* ---------------------------------------------------------- */

#if ALERTS_ABILITY_TO_GENERATE_ALL_ALERTS_AND_REPORTS_FOR_DEBUG

/* ---------------------------------------------------------- */
extern void STATION_REPORT_DATA_generate_lines_for_DEBUG( void )
{
	DATE_TIME_COMPLETE_STRUCT lcdt;

	UNS_32	lbox_index_0, lstation_num_0, lday_0;

	UNS_32	lindex;

	UNS_32	lsize_of_data;

	lsize_of_data = 0;

	EPSON_obtain_latest_complete_time_and_date( &lcdt );

	xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

	lbox_index_0 = 0;

	// 7/28/2017 rmd : Manipulate for how many boxes worth you want to send here.
//	for( lbox_index_0 = 0; lbox_index_0 < MAX_CHAIN_LENGTH; lbox_index_0++ )
	for( lbox_index_0 = 0; lbox_index_0 < 2; lbox_index_0++ )
	{
		for( lstation_num_0 = 0; lstation_num_0 < (MAX_STATIONS_PER_CONTROLLER /*/ 2*/); lstation_num_0++ )
		{
			STATION_PRESERVES_get_index_using_box_index_and_station_number( lbox_index_0, lstation_num_0, &lindex );

			for( lday_0 = 0; lday_0 < STATION_REPORT_DATA_MIN_DAYS_KEPT; lday_0++ )
			{
				station_preserves.sps[ lindex ].station_report_data_rip.box_index_0 =  lbox_index_0;
				station_preserves.sps[ lindex ].station_report_data_rip.station_number = lstation_num_0;

				station_preserves.sps[ lindex ].station_report_data_rip.record_start_date = (lcdt.date_time.D - STATION_REPORT_DATA_MIN_DAYS_KEPT + lday_0);
				station_preserves.sps[ lindex ].station_report_data_rip.record_start_time = lcdt.date_time.T;

				nm_STATION_REPORT_DATA_close_and_start_a_new_record( &(station_preserves.sps[ lindex ].station_report_data_rip), &(lcdt.date_time) );

				lsize_of_data += sizeof(STATION_REPORT_DATA_RECORD);
			}
		}
	}

	xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
}

#endif

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Returns the memory used by the STATION REPORT DATA report or the
    STATION HISTORY report.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the KEY_PROCESS task. As part of each and every
    screen change. I suppose can also be called as part of TD_CHECK when a screen change is
    triggered by a time out.
	
	@RETURN (none)

	@ORIGINAL 2012.07.30 rmd

	@REVISIONS (none)
*/
extern void STATION_REPORT_DATA_free_report_support( void )
{
	// 8/2/2012 rmd : We use the completed records MUTEX to also protect against changes to the
	// ptrs value. While it is in use (display work) we hold this same MUTEX. It certainly would
	// be a disaster to free the memory and NULL the ptr while in use. So take the same MUTEX to
	// prevent.
	xSemaphoreTakeRecursive( station_report_data_completed_records_recursive_MUTEX, portMAX_DELAY );

	if( station_report_data_ptrs != NULL )
	{
		mem_free( station_report_data_ptrs );
		
		// 7/27/2012 rmd : Beacuse the free doesn't set the ptr to NULL we do so. In order not to
		// try and free again as we move around screens.
		station_report_data_ptrs = NULL;
	}

	xSemaphoreGiveRecursive( station_report_data_completed_records_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

