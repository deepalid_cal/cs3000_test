/*  file = report_data.h                      09.13.2011  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_REPORT_DATA_H
#define _INC_REPORT_DATA_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/16/2015 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	"cal_td_utils.h"

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 1/22/2015 rmd : This structure is a common report data file definition that preceeds the
// actual array of the report data. It is used by the completed records structures for the
// POC report data, STATION report data, STATION_HISTORY report data, and the SYSTEM report
// data. Note the expansion room at the bottom to allow for changes without forcing a
// resizing effort in the revision update function. NOTE - the size of this structure is not
// to change. If it does we break the existing report file update scheme.

typedef struct
{
	// 7/19/2012 rmd : The user programmable roll time. For the CS3000 this is now mid-night.
	// For many years and to this day in the 2000e it is ET Roll Time (8PM). This generated no
	// end of questions and confusion. This time is not used for Station_History.
	UNS_32						roll_time;
	
	// ----------
	
	// 8/1/2012 rmd : Where to put the next completed record. Place it then increment. Perhaps
	// wrapping. We're using the variable as an index, rather than a pointer, because this is
	// stored in a file, and the file is read out into memory, and as code changes occur, the
	// memory location of the record will change.
	UNS_32						index_of_next_available;

	// 6/28/2012 rmd : Have we ever wrapped. This is useful to know which records are valid.
	// Another way, I suppose, is to look at the start time and date stamp to be the MIN_DATE.
	BOOL_32						have_wrapped;

	// ----------
	
	// 6/28/2012 rmd : To support the logic involved extracting all the records from the array.
	// When have_wrapped is (true) all records are valid. And we need to know when to stop
	// returning (true) records to the caller of the decrement function. Used solely to support
	// the local controller report display.
	BOOL_32						have_returned_next_available_record;

	// ----------
	
	// 8/14/2014 rmd : Three variables to support the controller initiated sending to the
	// comm_server app. Designed to allow lines to be added while the CI message is in transit.
	// The pending in use variable also protects against multiple station history CI messages
	// being generated resulting in a new pending_first_to_send index. Which would be used in
	// error for the first response that came in.
	//
	// 8/15/2014 rmd : Note that ideally these variables would be stored in battery-backed
	// memory. We however opted (cause easier) to include them in the station history structure.
	// Therefore the mechanism to preserve through a power fail is by writing them to the file.
	// The implication is that when the controller initiated ACK is received the station history
	// file must be saved.
	UNS_32					first_to_send;
	
	UNS_32					pending_first_to_send;

	BOOL_32					pending_first_to_send_in_use;
	
	// ----------
	
	// 1/22/2015 rmd : Thirty-two bytes of unused space for expansion. Allows for new variables
	// without changing the structure size.
	UNS_32					unused_array[ 8 ];

	// ----------
	
	// 1/22/2015 rmd : DO NOT CHANGE THE SIZE OF THIS STRUCTURE! DOING SO BREAKS THE REPORT FILE
	// UPDATE SCHEME.

} REPORT_DATA_FILE_BASE_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/23/2016 rmd : NOTE - the moisture recorder is held in dynamic memory. In the same
// manner as flow recording. If the code reboots whatever hasn't been transmitted to the
// commserver is LOST. And that is okay. Also whenever there is a power failure we will lose
// up to several hours (the interval between transmissions) worth of readings.

// 2/16/2016 rmd : These records are supposed to be be sent to the server maybe say once
// every 3 hours. Not being battery backed that means of course we could lose up to 3 hours
// worth of readings at a power failure. Anyway, how many should we store. Well assuming 24
// moisture sensors on a network (24 is a number AJ and I came up with) and a new reading
// once every 10 minutes gives us 144 readings per hour. Multiple that by 3 hours and we
// come up with 432 readings.
//
// 9/23/2016 rmd : We needed a define for two reasons. One, is for display purposes,
// building an array of pointers to the moisture sensors in the file linked list. And two is
// to size the number of report records held in memory. Well AJ and I just came up with 24
// as a reasonable number. Exceeding that won't necessarily break the recorder (meaning you
// overrun records before you transmit them) but will have an effect on some areas in the
// display. An alert line is generated in the display case.
#define	MOISTURE_SENSORS_MAX_NUMBER_PER_NETWORK		(24)

// 3/29/2016 rmd : I WANT the moisture recorder to fit in one of the 8448 byte SDRAM memory
// pool partitions. The record is now 24 bytes so 24 * 342 = 8208 bytes. Which seems like a
// nice number of records! And close enough to the full partition for me. Considering this I
// am going to send moisture readings once every 2 hours. Which will comfortably hold enough
// readings for 24 sensors given one new reading every 10 minutes.
//
// 9/23/2016 rmd : Update we NOW only make a report record once every 20 minutes. So there
// is ample records to have communication problems for quite a period of time. And not lose
// any data.
#define MOISTURE_SENSOR_RECORDER_MAX_RECORDS	(342)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 3/25/2016 rmd : The order of this record is intentional to allow the compiler to pack the
	// content.
	
	// ----------
	
	DATE_TIME	dt;

	// 2/18/2016 rmd : An UNS_16 to allow the compiler to PACK it with the 6 byte DATE_TIME
	// structure. INT_16 as can go negative.
	INT_16		temperature_fahrenheit;

	UNS_32		decoder_serial_number;
	
	float		moisture_vwc_percentage;
	
	// 8/17/2018 Ryan : Changed from an UNS_32 to a float. And name changed to reflect units now
	// in use.
	float		latest_conductivity_reading_deci_siemen_per_m;
	
	UNS_8		sensor_type;

	// 3/29/2016 rmd : Compiler padding.
	UNS_8		unused_01;
	UNS_8		unused_02;
	UNS_8		unused_03;
	
	// ----------
	
	// 2/17/2016 rmd : The whole structure size (like any structure) will be sized as a multiple
	// of 4 bytes. In this case there are currently no pad bytes. And the structure size is 24
	// bytes.

	// 3/29/2016 rmd : CAUTION - If you change the SIZE of this structure you must consider the
	// storage implications. Its size and the number of records is crafted to support a good
	// number of moisture decoders, the once per 10 minute reading delivery, the once per 2
	// hours transmission, and fit in an 8448 byte partition.

	// 3/29/2016 rmd : We are treating the moisture recorder just like flow recording in that
	// there is no file and we are on a you get what you get basis. Meaning if there is a power
	// failure you loose what hasn't been sent to the web service.
	
} MOISTURE_SENSOR_RECORDER_RECORD;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

DLLEXPORT UNS_32 REPORTS_extract_and_store_changes( const UNS_32 pMID,
													UNS_8 *pucp,
													const UNS_32 pdata_len
													#ifdef _MSC_VER
													, const UNS_32 pnetwork_ID,
													const UNS_16 lastReportDate,
													const UNS_32 lastReportTime,
													char *pSQL_statements,
													char *pSQL_search_condition_str,
													char *pSQL_update_str,
													char *pSQL_insert_fields_str,
													char *pSQL_insert_values_str,
													char *pSQL_single_merge_statement_buf
													#endif
												  );

#ifndef _MSC_VER

extern void REPORT_DATA_maintain_all_accumulators( DATE_TIME *pdt_ptr );

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

