
/*  file = irri_lights.c                                      */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Required for memcpy and memset
#include	<string.h>

#include	"irri_lights.h"

#include	"irri_comm.h"

#include	"app_startup.h"

#include	"configuration_controller.h"

#include	"alerts.h"

#include	"stations.h"

#include	"foal_lights.h"

#include	"battery_backed_vars.h"

#include	"epson_rx_8025sa.h"

#include	"lights_report_data.h"

#include	"flowsense.h"

#include	"cs_mem.h"

#include	"alert_parsing.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// irri_lights is NOT battery backed. The approach in the CS3000 is if the chain goes down for longer than a brief 
// (2 minute) buffer period, we will NOT try to continue (pick up lighting where we left off) for each box in the chain. 
// Boxes will not operate as independents. If there is a power fail, when the chain comes back up the master will tell 
// the irri machine all that is happening with lights. If we fall into forced or the chain is detected as 'different' 
// then all the lighting is cleared. Any on-going lighting is turned off.
IRRI_LIGHTS	irri_lights;

// Lights use stop date and time to turn OFF, not remaining seconds as stations do. We need a way to measure how long 
// the chain is down before turning OFF all lights in this box.
UNS_32 IRRI_LIGHTS_seconds_since_chain_went_down = 0;


typedef struct
{
	void		*light_ptr;

} IRRI_LIGHTS_MENU_SCROLL_BOX_ITEM;

static IRRI_LIGHTS_MENU_SCROLL_BOX_ITEM IRRI_LIGHTS_MENU_items[ MAX_LIGHTS_IN_NETWORK ];

/* ---------------------------------------------------------- */
extern void init_irri_lights( void )
{
	UNS_32 box_index;
	UNS_32 output_index;
	UNS_32 array_index;

	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );

	// Must initalize the lights list. Unlike stations, light lists are not in dynamic memory.
	nm_ListInit( &irri_lights.header_for_irri_lights_ON_list, offsetof( IRRI_LIGHTS_LIST_COMPONENT, list_linkage_for_irri_lights_ON_list ) );

	// This box just restarted due to a power failure or a software reason. We need to clear the IRRI lights ON list to 
	// be ready to accept new lights instructions from the FOAL side. We are only a slave. We don't make decisions.
	for( box_index = 0; box_index < MAX_CHAIN_LENGTH; ++box_index )
	{
		for( output_index = 0; output_index < MAX_LIGHTS_IN_A_BOX; ++output_index )
		{
			array_index = LIGHTS_get_lights_array_index( box_index, output_index );

			// Initialize all IRRI_LIGHTS_LIST_COMPONENT fields.
			irri_lights.illcs[ array_index ].list_linkage_for_irri_lights_ON_list.pListHdr = NULL;
			irri_lights.illcs[ array_index ].list_linkage_for_irri_lights_ON_list.pNext = NULL;
			irri_lights.illcs[ array_index ].list_linkage_for_irri_lights_ON_list.pPrev = NULL;
			irri_lights.illcs[ array_index ].box_index_0 = box_index;
			irri_lights.illcs[ array_index ].output_index_0 = output_index;
			irri_lights.illcs[ array_index ].light_is_energized = ( false );
			irri_lights.illcs[ array_index ].action_needed = ACTION_NEEDED_NONE;
			irri_lights.illcs[ array_index ].stop_datetime_d = 0;
			irri_lights.illcs[ array_index ].stop_datetime_t = 0;
		}
	}

	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Restarts all the lighting activity lists and variables. As part of
    falling into or coming out of forced. Basically when a chain configuration CHANGE is
    detected. This is very similar to what happens upon startup as the irri structures for
    lights are NOT battery backed. So upon each program start they are also intialized.
    In this case however we attend to the possible memory allocations taken for the
    lights and action needed lists.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the COMM_MNGR task when a chain configuration
    change is detected.
	
	@RETURN (none)

	@AUTHOR 2012.02.07 rmd
 
	@REVISIONS 8/7/2015 mpd Adapted station function for lights.
*/
extern void IRRI_LIGHTS_restart_all_lights( void )
{
	IRRI_LIGHTS_LIST_COMPONENT	*lillc;

	// Then one thing we do is send the light list over to this controllers irri machine.
	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	// 10/23/2015 ajv : Because of the complexity where there's an ON list which actually just
	// points to array item elements, we've seperated out clearing the array and clearing the ON
	// list.

	// 10/23/2015 ajv : First, initialize the various fields in the array.
	lillc = nm_ListGetFirst( &irri_lights.header_for_irri_lights_ON_list );
	
	while( lillc != NULL )
	{
		// Indicate it is OFF!
		lillc->light_is_energized = ( false );
		lillc->stop_datetime_d = 0;
		lillc->stop_datetime_t = 0;

		lillc = nm_ListGetNext( &irri_lights.header_for_irri_lights_ON_list, lillc );
	}

	// ----------

	// 10/23/2015 ajv : Then loop through the ON list and remove any lights on it.
	lillc = nm_ListGetFirst( &irri_lights.header_for_irri_lights_ON_list );

	while( lillc != NULL )
	{
		lillc = nm_ListRemoveHead( &irri_lights.header_for_irri_lights_ON_list );
	}

	// ----------

	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern BOOL_32 IRRI_LIGHTS_light_is_energized( const UNS_32 light_index_0_47 )
{
	BOOL_32 light_is_energized = false;

	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );
	
	light_is_energized = irri_lights.illcs[ light_index_0_47 ].light_is_energized;

	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

	return( light_is_energized );

}

/* ---------------------------------------------------------- */
/**
 * Returns a pointer to the group with the specified group ID. This is a wrapper
 * for the nm_GROUP_get_ptr_to_group_at_this_location_in_list function so we
 * don't need to expose the list header.
 * 
 * @mutex Calling function must have the light_recursive_MUTEX mutex to ensure the
 *        group that's found isn't deleted before it's used.
 *
 * @executed Executed within the context of the display processing task when the
 *  		 list and group screens are drawn; within the key processing task
 *  		 when the user navigates through the group list screen.
 * 
 * @param pindex_0 The index of the group to retrieve - 0 based.
 * 
 * @return void* A pointer to the group with the specified group ID. Or a NULL if the index
 * is beyond the lists range.
 *
 * @author 9/15/2011 Adrianusv
 *
 * @revisions
 *    9/15/2011 Initial release
 */
static void *IRRI_LIGHTS_get_light_at_this_index( const UNS_32 pindex_0 )
{
	IRRI_LIGHTS_LIST_COMPONENT *llight;

	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );

	llight = &irri_lights.illcs[ pindex_0 ];

	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

	return( llight );
}

// 9/1/2015 mpd : TODO: Fix comment block.
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Used by external functions to obtain the current status of a specific light.

	@CALLER MUTEX REQUIREMENTS (none)

	@RETURN true if the light is energized.

	@AUTHOR 8/21/2015 mpd
*/
extern UNS_32 IRRI_LIGHTS_get_number_of_energized_lights( void )
{
	UNS_32 number_of_energized_lights;

	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );
	
	number_of_energized_lights = irri_lights.header_for_irri_lights_ON_list.count;

	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

	return( number_of_energized_lights );

}

// 9/1/2015 mpd : TODO: Fix comment box.
/* ---------------------------------------------------------- */
/**
 * Populates the "LIGHTS_MENU_items" array with pointers to all the availale lights.
 * 
 * @executed Executed within the context of the KEY PROCESSING task.
 * 
 * @param void
 *
 * @return UNS_32 The number of available lights.
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern UNS_32 IRRI_LIGHTS_populate_pointers_of_energized_lights( void )
{
	IRRI_LIGHTS_LIST_COMPONENT *lillc;

	UNS_32	ladded_LIGHTs;

	UNS_32	i;

	ladded_LIGHTs = 0;

	memset( IRRI_LIGHTS_MENU_items, 0x00, sizeof( IRRI_LIGHTS_MENU_items ) );

	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );

	for( i = 0 ; i < MAX_LIGHTS_IN_NETWORK; i++ )
	{
		lillc = IRRI_LIGHTS_get_light_at_this_index( i );

		if( ( lillc != NULL) && ( IRRI_LIGHTS_light_is_energized( i ) ) )
		{
			IRRI_LIGHTS_MENU_items[ ladded_LIGHTs ].light_ptr = lillc;

			ladded_LIGHTs++;
		}
	}

	if( IRRI_LIGHTS_get_number_of_energized_lights() != ladded_LIGHTs )
	{
		Alert_Message( "Num of Energized Lights Mismatch");
	}

	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

	return ( ladded_LIGHTs );

}

// 9/1/2015 mpd : TODO: Fix comment box.
/* ---------------------------------------------------------- */
/**
 * Rturns the "LIGHTS_GROUP_STRUCT" pointer for the specified light index.
 * 
 * @task_execution Executed within the context of the KEY PROCESSING task.
 * 
 * @param pindex_0 The index of the light in the "LIGHTS_MENU_items" array..
 *
 * @return LIGHTS_GROUP_STRUCT Pointer to the light structure for this light.
 *
 * @author 7/20/2015 mpd
 * 
 * @revisions
 *    7/20/2015 Initial release
 */
extern IRRI_LIGHTS_LIST_COMPONENT *IRRI_LIGHTS_get_ptr_to_energized_light( const UNS_32 pline_index )
{
	return( IRRI_LIGHTS_MENU_items[ pline_index ].light_ptr );
}

// 9/1/2015 mpd : TODO: Fix comment block.
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Used by external functions to obtain the current status of a specific light.

	@CALLER MUTEX REQUIREMENTS (none)

	@RETURN true if the light is energized.

	@AUTHOR 8/21/2015 mpd
*/
extern UNS_32 IRRI_LIGHTS_get_box_index( const UNS_32 pline_index )
{
	IRRI_LIGHTS_LIST_COMPONENT *llight;

	UNS_32 box_index;

	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );
	
	llight = IRRI_LIGHTS_get_ptr_to_energized_light( pline_index );

	box_index = llight->box_index_0;

	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

	return( box_index );

}

// 9/1/2015 mpd : TODO: Fix comment block.
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Used by external functions to obtain the current status of a specific light.

	@CALLER MUTEX REQUIREMENTS (none)

	@RETURN true if the light is energized.

	@AUTHOR 8/21/2015 mpd
*/
extern UNS_32 IRRI_LIGHTS_get_output_index( const UNS_32 pline_index )
{
	IRRI_LIGHTS_LIST_COMPONENT *llight;

	UNS_32 output_index;

	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );
	
	llight = IRRI_LIGHTS_get_ptr_to_energized_light( pline_index );

	output_index = llight->output_index_0;

	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

	return( output_index );

}

// 9/1/2015 mpd : TODO: Fix comment block.
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Used by external functions to obtain the current status of a specific light.

	@CALLER MUTEX REQUIREMENTS (none)

	@RETURN true if the light is energized.

	@AUTHOR 8/21/2015 mpd
*/
extern UNS_32 IRRI_LIGHTS_get_reason_on_list( const UNS_32 pline_index )
{
	IRRI_LIGHTS_LIST_COMPONENT *llight;

	UNS_32 reason_on_list;

	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );
	
	llight = IRRI_LIGHTS_get_ptr_to_energized_light( pline_index );

	reason_on_list = llight->reason_on_list;

	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

	return( reason_on_list );

}

// 9/1/2015 mpd : TODO: Fix comment block.
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Used by external functions to obtain the current status of a specific light.

	@CALLER MUTEX REQUIREMENTS (none)

	@RETURN true if the light is energized.

	@AUTHOR 8/21/2015 mpd
*/
extern UNS_32 IRRI_LIGHTS_get_stop_date( const UNS_32 pline_index )
{
	IRRI_LIGHTS_LIST_COMPONENT *llight;

	UNS_32 stop_datetime_d;

	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );
	
	llight = IRRI_LIGHTS_get_ptr_to_energized_light( pline_index );

	stop_datetime_d = llight->stop_datetime_d;

	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

	return( stop_datetime_d );

}

// 9/1/2015 mpd : TODO: Fix comment block.
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Used by external functions to obtain the current status of a specific light.

	@CALLER MUTEX REQUIREMENTS (none)

	@RETURN true if the light is energized.

	@AUTHOR 8/21/2015 mpd
*/
extern UNS_32 IRRI_LIGHTS_get_stop_time( const UNS_32 pline_index )
{
	IRRI_LIGHTS_LIST_COMPONENT *llight;

	UNS_32 stop_datetime_t;

	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );
	
	llight = IRRI_LIGHTS_get_ptr_to_energized_light( pline_index );

	stop_datetime_t = llight->stop_datetime_t;

	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

	return( stop_datetime_t );

}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Used by external functions to obtain the stop date of a specific light.

	@CALLER MUTEX REQUIREMENTS (none)

	@RETURN DATE_TIME Stop date and time if the light is energized. Zeros if not energized.

	@AUTHOR 8/24/2015 mpd
*/
extern DATE_TIME IRRI_LIGHTS_get_light_stop_date_and_time( const UNS_32 light_index_0_47 )
{
	DATE_TIME stop_datetime;

	stop_datetime.D = 0;
	stop_datetime.T = 0;

	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );
	
	if( irri_lights.illcs[ light_index_0_47 ].light_is_energized )
	{
		stop_datetime.D = irri_lights.illcs[ light_index_0_47 ].stop_datetime_d;
		stop_datetime.T = irri_lights.illcs[ light_index_0_47 ].stop_datetime_t;
	}

	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

	return( stop_datetime );

}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called at a 1 hertz rate when the communications sync indicates completion.
    This function acts as a fail safe task guarding lighting. To make sure lights don't
    run well past their run time. It is the master that makes all the formal decisions about
    when to turn ON and OFF. But if communications were to fail we count on this backup.
    Think of it as a watchdog on the lighting process. The other function of this task is
    to indicate in the global TP_BOARD structure which lights are ON.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the TD_CHECK task.
	
	@RETURN (none)

	@AUTHOR 2012.02.08 rmd
 
	@REVISIONS 8/7/2015 mpd Adapted station function for lights.
*/
extern void IRRI_LIGHTS_maintain_lights_list( const DATE_TIME *const dt_ptr )
{
	DATE_TIME	watchdog_dt;

	IRRI_LIGHTS_LIST_COMPONENT	*ills;

	// ----------

	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );

	//xSemaphoreTakeRecursive( lights_preserves_recursive_MUTEX, portMAX_DELAY );

	// Lights in this box will be given some grace time before shutting down. This is where we keep time.
	if( FLOWSENSE_we_are_poafs() && comm_mngr.chain_is_down )
	{
		// Put a cap on the increment so we never roll over.
		if( IRRI_LIGHTS_seconds_since_chain_went_down < SCHEDULE_OFF_TIME )
		{
			IRRI_LIGHTS_seconds_since_chain_went_down++;
		}
	}
	else
	{
		IRRI_LIGHTS_seconds_since_chain_went_down = 0;
	}

	// ----------
	
	ills = nm_ListGetFirst( &irri_lights.header_for_irri_lights_ON_list );

	while( ills != NULL )
	{
		BOOL_32	flagged_for_removal = ( false );

		// Remove any lights from the ON list immediately if they are not on this box and the chain is down. Also
		// remove them if we are not on a chain (we were at some time or they would not exist on the ON list).
		if( !FLOWSENSE_we_are_poafs() || comm_mngr.chain_is_down )
		{
			if( ills->box_index_0 != FLOWSENSE_get_controller_index() )
			{
				flagged_for_removal = ( true );
			}
		}

		// ----------
		
		if( !flagged_for_removal ) 
		{
			// If this light belongs to us we perform the watchdog duties. 
			if( ills->box_index_0 == FLOWSENSE_get_controller_index() )
			{
				if( ills->light_is_energized == ( true ) )
				{
					// If the master does not deliver a token turning OFF the light within 120 seconds of the chain 
					// going down, we shall take matter into our own hands. With the introduction of the smart token the 
					// synchronization between master and slave should be fairly tight.
					if( IRRI_LIGHTS_seconds_since_chain_went_down > ( IRRI_LIGHTS_SECONDS_TO_WAIT_FOR_MASTER ) )
					{
						// Turn it OFF and remove it from the list.
						ills->light_is_energized = ( false ); 

						flagged_for_removal = ( true );

						// 8/19/2015 mpd : TBD: The IRRI side probably does not belong in the lights preserves at all.

						// Keep the preserves up-to-date.
						//lights_preserves.lbf[ LIGHTS_get_lights_array_index( ills->output_index_0, ills->box_index_0 ) ].light_is_energized = ( false );
					}

					// Set the date and time the watchdog will bite.
					watchdog_dt.D = ills->stop_datetime_d;
					watchdog_dt.T = ( ills->stop_datetime_t + IRRI_LIGHTS_SECONDS_TO_WAIT_FOR_MASTER );

					// Adjust date and time if the wait time rolled past midnight.
					if( watchdog_dt.T >= SCHEDULE_OFF_TIME )
					{
						watchdog_dt.D = ( watchdog_dt.D + 1 );
						watchdog_dt.T = ( watchdog_dt.T - SCHEDULE_OFF_TIME );
					}

					// If the master did not deliver a token turning OFF the light within 120 seconds of the stop time, we 
					// will take matter into our own hands. 
					if( DT1_IsBiggerThanOrEqualTo_DT2( dt_ptr, &watchdog_dt ) == ( true ) )
					{
						// Turn it OFF and remove it from the list.
						ills->light_is_energized = ( false ); 

						flagged_for_removal = ( true );
					}

				} // END Energized.

			} // END light is in our box. 

			// 9/15/2015 mpd : This block has been moved outside the "light is in our box" block. Every IRRI machine 
			// needs to track the energized state of every light in every box or calls to 
			// "IRRI_LIGHTS_light_is_energized()" will return incorrect information. 
			if( ( !flagged_for_removal ) && ( ills->light_is_energized == ( false ) ) )
			{
				// The light is not energized. Do we need to complete the ON action?
				if( ( ills->action_needed == ACTION_NEEDED_TURN_ON_programmed_light ) ||
					( ills->action_needed == ACTION_NEEDED_TURN_ON_manual_light     ) ||
					( ills->action_needed == ACTION_NEEDED_TURN_ON_mobile_light     ) ||
					( ills->action_needed == ACTION_NEEDED_TURN_ON_test_light       ) )
				{
					// Indicate it is ON! Stop date/time was set when the light was added to the ON list.
					ills->light_is_energized = ( true );

					// ----------
					
					// 9/30/2015 rmd : Since we are energizing the lights output take the preserves MUTEX and
					// clear the IRRI side short and no current flags for this lights output.
					xSemaphoreTakeRecursive( lights_preserves_recursive_MUTEX, portMAX_DELAY );
					
					lights_preserves.lbf[ LIGHTS_get_lights_array_index( ills->box_index_0, ills->output_index_0 ) ].shorted_output = (false);

					lights_preserves.lbf[ LIGHTS_get_lights_array_index( ills->box_index_0, ills->output_index_0 ) ].no_current_output = (false);
					
					xSemaphoreGiveRecursive( lights_preserves_recursive_MUTEX );

					// ----------
					
					// Indicate this light needs no further action.
					ills->action_needed = ACTION_NEEDED_NONE;
				}
			}

		} // END !flagged_for_removal. 
			
		// ----------

		if( flagged_for_removal )
		{
			void	*list_item_to_delete;

			list_item_to_delete = ills;

			// Indicate it is OFF!
			ills->light_is_energized = ( false );
			ills->stop_datetime_d = 0;
			ills->stop_datetime_t = 0;

			ills = nm_ListGetNext( &irri_lights.header_for_irri_lights_ON_list, ills );

			// Remove it from the irri_lights ON list.
			nm_ListRemove( &irri_lights.header_for_irri_lights_ON_list, list_item_to_delete );

			// 8/10/2015 mpd : IRRI Light lists are not in dynamic memory. 
			//mem_free( list_item_to_delete );
		}
		else
		{
			ills = nm_ListGetNext( &irri_lights.header_for_irri_lights_ON_list, ills );
		}

	}  // of while looping through all in list

	//xSemaphoreGiveRecursive( lights_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This ON list record was received as part of a token from the master. Look
    for the light on the IRRI ON list. If it's there, update the stop date/time. If it's
    not there, add it.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the COMM_MNGR task during the token processing
    function.
	
	@RETURN (none)

	@AUTHOR 8/13/2015 mpd 
*/
extern void IRRI_LIGHTS_process_rcvd_lights_on_list_record( LIGHTS_LIST_XFER_RECORD const *const plxr )
{
	// To save space in the token, box and output values were combined into a single UNS_8. Extract the individual 
	// values here.
	UNS_32 box_index = ( plxr->light_index_0_47 / MAX_LIGHTS_IN_A_BOX );

	BOOL_32 still_sane = ( true );

	IRRI_LIGHTS_LIST_COMPONENT	*ills;

	DATE_TIME ldt, temp_dt;

	// ----------

	// Put the received stop date and time into a DATE_TIME struct for the upcoming comparison. 
	temp_dt.D = plxr->stop_datetime_d;
	temp_dt.T = plxr->stop_datetime_t;

	EPSON_obtain_latest_time_and_date( &ldt );

	// We should only be here to turn ON a light.
	if( ( plxr->action_needed != ACTION_NEEDED_TURN_ON_programmed_light ) &&
		( plxr->action_needed != ACTION_NEEDED_TURN_ON_manual_light     ) &&
		( plxr->action_needed != ACTION_NEEDED_TURN_ON_mobile_light     ) &&
		( plxr->action_needed != ACTION_NEEDED_TURN_ON_test_light       ) )
	{
		still_sane = ( false );
		Alert_Message( "I_L: Bad action (1)");
	}

	// Sanity check.
	if( ( still_sane ) && ( plxr->light_index_0_47 >= MAX_LIGHTS_IN_NETWORK ) )
	{
		still_sane = ( false );
		Alert_Message( "I_L: Bad light index (1)");
	}

	// Get the IRRI_LIGHTS_LIST_COMPONENT pointer for this light.
	if( ( still_sane ) && ( ( ills = &irri_lights.illcs[ plxr->light_index_0_47 ] ) == NULL ) ) 
	{
		still_sane = ( false );
		Alert_Message( "I_L: Bad illc pointer (1)");
	}

	// The master should not have sent an ON with no energized time remaining. This is to catch really slow tokens!
	if( ( still_sane ) && ( DT1_IsBiggerThanOrEqualTo_DT2( &ldt, &temp_dt ) == ( true ) ) )
	{
		still_sane = ( false );

		// 8/13/2015 mpd : TODO: This could be a problem. Clocks on slaves are not currently synced with the master. 
		// That can lead to all kinds of stop time differences between boxes. This alert really needs to get an AID so
		// it appears in the user alerts. The master needs to be notified if we don't sync the clocks in the near 
		// future.
		 
		// Only make noise if this light is in our box.
		if( ( !FLOWSENSE_we_are_poafs() ) || ( box_index == FLOWSENSE_get_controller_index() ) )
		{
			Alert_Message( "I_L: ON received wih no time left");
		}
	}

	if( still_sane )
	{
		// Because we are traversing through the list and changing the values within an entry take this mutex.
		xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );

		// See if the target light's illc is already on the ON list.
		if( !nm_OnList( &irri_lights.header_for_irri_lights_ON_list, ills ) )
		{
			// The illc is not on the ON list. Add it.
			if( nm_ListInsertTail( &irri_lights.header_for_irri_lights_ON_list, ( void* )ills ) != MIST_SUCCESS )
			{
				Alert_Message( "I_L: list insert fault" );
			}
		}

		// The "IRRI_LIGHTS_maintain_lights_list()" function needs to know what to do with this light.
		ills->action_needed = plxr->action_needed;

		// There are 4 possible actions that map directly to 4 possible reasons for lights being on. This mapping saves
		// an extra field in the transfer record. Note that action values do not equal reason values.
		switch( plxr->action_needed )
		{
			case ACTION_NEEDED_TURN_ON_programmed_light:
				ills->reason_on_list = IN_LIST_FOR_PROGRAMMED_LIGHTS;
					break;

			case ACTION_NEEDED_TURN_ON_manual_light:
				ills->reason_on_list = IN_LIST_FOR_MANUAL;
					break;

			case ACTION_NEEDED_TURN_ON_mobile_light:
				ills->reason_on_list = IN_LIST_FOR_MOBILE;
					break;

			case ACTION_NEEDED_TURN_ON_test_light:
				ills->reason_on_list = IN_LIST_FOR_TEST;
					break;

			default:
				break;
		}

		// Save the stop date/time. If the light was already ON, overwrite the old values with the latest from the 
		// master.
		ills->stop_datetime_d = plxr->stop_datetime_d;
		ills->stop_datetime_t = plxr->stop_datetime_t;


		// Leave these fields alone. Initialization, maintenance, and action functions will deal with them.
		//ills->box_index_0 
		//ills->output_index_0
		//ills->light_is_energized
		//lights_preserves.lbf[ plxr->light_index_0_47 ].light_is_energized

		xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );
	}
	else
	{
		Alert_Message( "I_L: light index fault (1)" );
	}

}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This action needed record was received as part of a token from the master.
    Find the light the xfer is referring to and take the appropriate action if it is in
	this box. 

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the COMM_MNGR task during the token processing
    function.
	
	@RETURN (none)

	@AUTHOR 2012.02.10 rmd
 
	@REVISIONS 8/7/2015 mpd Adapted station function for lights.
*/
extern void IRRI_LIGHTS_process_rcvd_lights_action_needed_record( LIGHTS_ACTION_XFER_RECORD const *const plxr )
{
	BOOL_32 still_sane = ( true );

	IRRI_LIGHTS_LIST_COMPONENT	*ills;

	// Sanity check.
	if( ( still_sane ) && ( plxr->light_index_0_47 >= MAX_LIGHTS_IN_NETWORK ) )
	{
		still_sane = ( false );
		Alert_Message( "I_L: Bad light index (2)");
	}

	// Get the IRRI_LIGHTS_LIST_COMPONENT pointer for this light.
	if( ( still_sane ) && ( ( ills = & irri_lights.illcs[ plxr->light_index_0_47 ] ) == NULL ) )
	{
		still_sane = ( false );
		Alert_Message( "I_L: Bad illc pointer (2)");
	}

	if( still_sane )
	{
		// Because we are changing the values within an entry take this mutex.
		xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );

		switch( plxr->action_needed )
		{
			// The "IRRI_LIGHTS_maintain_lights_list()" function correctly handles turning on a light once the light is
			// added to the IRRI ON list. The FOAL side no longer sends action items for ON operations.

			case ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_output_short:
			case ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_hit_stop_time:
			case ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_manual_off:
			case ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_mobile_off:
			case ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_test_off:
			case ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_time_unexpectedly_zero:
			case ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_controller_turned_off:

				// 9/30/2015 rmd : For the case of a short set the IRRI side gui indication. Is cleared the
				// next time this output turns ON in the MAINTAIN function.
				if( plxr->action_needed == ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_output_short )
				{
					lights_preserves.lbf[ plxr->light_index_0_47 ].shorted_output = (true);
				}
				
				// ----------
				
				// Indicate it is OFF!
				ills->light_is_energized = ( false );
				ills->stop_datetime_d = 0;
				ills->stop_datetime_t = 0;

				// Remove the llc from the ON list. This is static memory. Do not do a free.
				nm_ListRemove( &irri_lights.header_for_irri_lights_ON_list, ( void* )ills );
				break;

			case ACTION_NEEDED_NONE:
			default:
				// 8/17/2015 mpd : TODO: This can go away afer development.
				Alert_Message( "I_L: Bad action (2)");
				break;
		}

		// The ills array lives in static memory. Indicate this light needs no further action.
		ills->action_needed = ACTION_NEEDED_NONE;

		xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

	}
	else
	{
		Alert_Message( "I_L: light index fault (2)" );
	}

}

/* ---------------------------------------------------------- */
extern BOOL_32 IRRI_LIGHTS_request_light_on( const UNS_32 light_index_0_47, const UNS_32 stop_datetime_d, const UNS_32 stop_datetime_t )
{
	UNS_32	still_sane = true;
	
	DATE_TIME ldt, temp_dt;

	// ----------

	// We can only sent one ON request per token response. 	
	if( irri_comm.light_on_request.in_use == ( false ) )
	{
		// Put the requested stop date and time into a DATE_TIME struct for the upcoming comparison.
		temp_dt.D = stop_datetime_d;
		temp_dt.T = stop_datetime_t;

		EPSON_obtain_latest_time_and_date( &ldt );

		// Sanity check.
		if( light_index_0_47 >= MAX_LIGHTS_IN_NETWORK )
		{
			still_sane = ( false );
			Alert_Message( "I_L: Bad light index (3)");
		}

		// The GUI should not have sent an ON with no energized time remaining. This is to make sure.
		if( ( still_sane ) && ( DT1_IsBiggerThanOrEqualTo_DT2( &ldt, &temp_dt ) == ( true ) ) )
		{
			still_sane = ( false );
			Alert_Message( "I_L: ON received wih no time left (2)");
		}

		if( still_sane )
		{
			// Block the irri_comm request holding structure from accepting another request.
			irri_comm.light_on_request.in_use = ( true );

			// Fill the request structure with the details. Time is in seconds.
			irri_comm.light_on_request.light_index_0_47 = light_index_0_47;
			irri_comm.light_on_request.stop_datetime_d  = stop_datetime_d;
			irri_comm.light_on_request.stop_datetime_t  = stop_datetime_t;
		}

		// That's it. IRRI_COMM will ship the request to the master when it sees the "reason_in_list" in the structure.
	}
	else
	{
		// There is an ON request in the structure waiting to be sent to the master. This one will have to be retried
		// by the user. No alert is necessary.
		still_sane = ( false );
	}

	return( still_sane );

}

/* ---------------------------------------------------------- */
extern BOOL_32 IRRI_LIGHTS_request_light_off( const UNS_32 light_index_0_47 )
{
	BOOL_32	rv;

	// ----------

	rv = (false);
	
	// ----------

	// We can only sent one OFF request per token response. 	
	if( !irri_comm.light_on_request.in_use )
	{
		// Sanity check.
		if( light_index_0_47 >= MAX_LIGHTS_IN_NETWORK )
		{
			Alert_index_out_of_range();
		}
		else
		{
			// Block the irri_comm request holding structure from accepting another request.
			irri_comm.light_off_request.in_use = (true);

			// Fill the request structure with the last detail.
			irri_comm.light_off_request.light_index_0_47 = light_index_0_47;

			rv = (true);
		}

		// That's it. IRRI_COMM will ship the request to the master when it sees the "reason_in_list" in the structure.
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

