/*  file = foal_flow.h              10/27/00 2011.08.01  rmd  */

/* ---------------------------------------------------------- */

#ifndef _INC_FOAL_FLOW_H
#define _INC_FOAL_FLOW_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"irri_flow.h"

#include	"foal_irri.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FOAL_FLOW__A__build_poc_flow_rates( void );

extern void FOAL_FLOW__B__build_system_flow_rates( void );



extern BOOL_32 FOAL_FLOW_extract_poc_update_from_token_response( UNS_8 **pucp_ptr, UNS_32 const pcontroller_sn );


extern UNS_32 FOAL_FLOW_load_system_info_for_distribution_to_slaves( UNS_8 **pdata_ptr );

extern UNS_32 FOAL_FLOW_load_poc_info_for_distribution_to_slaves( UNS_8 **pdata_ptr );


extern void FOAL_FLOW_check_for_MLB( void );

// ----------------------

#define	FLOW_RECORDING_OPTION_YES_mark_as_flow_checked				(true)
#define	FLOW_RECORDING_OPTION_YES_stamp_the_report_rip				(true)
#define	FLOW_RECORDING_OPTION_YES_make_flow_recording_lines			(true)

#define	FLOW_RECORDING_OPTION_NO_DONT_mark_as_flow_checked			(false)
#define	FLOW_RECORDING_OPTION_NO_DONT_stamp_the_report_rip			(false)
#define	FLOW_RECORDING_OPTION_NO_DONT_make_flow_recording_lines		(false)

extern void FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowrecordinglines(	BY_SYSTEM_RECORD				*pbsr_ptr,

																				BOOL_32							pmark_as_flow_checked,
																				BOOL_32							pstamp_report_rip,
																				BOOL_32							pmake_flow_recorder_line,

																				FLOW_RECORDER_FLAG_BIT_FIELD	pflow_recorder_flag,
																				UNS_32							psystem_actual_flow );
																				
// ----------------------

extern void FOAL_FLOW_check_valve_flow( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

