/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy
#include	<string.h>

#include	"weather_tables.h"

#include	"flash_storage.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"cal_math.h"

#include	"epson_rx_8025sa.h"

#include	"et_data.h"

#include	"station_groups.h"

#include	"weather_control.h"

#include	"configuration_controller.h"

#include	"cs_mem.h"

#include	"controller_initiated.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 1/21/2015 rmd : This is the structure revision number. It started at 0. And should be
// incremented by 1 as changes are made to the weather_tables structure. New variables are
// to be added to the end.

#define	WEATHER_TABLES_LATEST_FILE_REVISION		(2)

const char WEATHER_TABLES_FILENAME[] = "WEATHER_TABLES";

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 7/20/2012 rmd : This is the date attached to entry 0 in the table. And moving into the
	// table moves backwards in time.
	UNS_32  et_table_date;

	// 9/13/2012 rmd : The ET and RAIN tables. The entry in process, that is the one
	// accumulating the et or rain pulses is held in the battery backed weather preserves. To
	// load the ET table with a new historical there is a flag (also in battery backed
	// weather_preserves). Within the roll function that flag is acted upon and reset.
	ET_TABLE_ENTRY      et_table[ ET_RAIN_DAYS_IN_TABLE ];

	// --------

	// 7/20/2012 rmd : This is the date attached to entry 0 in the table. And moving into the
	// table moves backwards in time.
	UNS_32  rain_table_date;

	// 9/13/2012 rmd : See comment above for et table.
	RAIN_TABLE_ENTRY    rain_table[ ET_RAIN_DAYS_IN_TABLE ];

} WEATHER_TABLES_STRUCT_REV_0;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

WEATHER_TABLES_STRUCT    weather_tables;

/* ---------------------------------------------------------- */

#define	ET_TABLE_ET_100U_DEFAULT		(20)

#define	RAIN_TABLE_RAIN_100U_DEFAULT	(0)

/* ---------------------------------------------------------- */
static void nm_WEATHER_TABLES_set_default_values( void )
{
	// 1/23/2015 rmd : Function caller is to be holding the weather_tables MUTEX. This function
	// is only called within the context of the STARTUP task via the initial file check.

	// ----------

	UNS_32  i;

	// 7/23/2012 rmd : But do populate the table with a value (just any reasonable value) and
	// set the status to HISTORICAL. So that when we call the function to fill the table with
	// historical values it will do so. If the status is not HISTORICAL it won't overwrite.

	for( i=0; i<ET_RAIN_DAYS_IN_TABLE; i++ )
	{
		// ET TABLE - For now fill the table with a daily value of .2 inches (must be historical to
		// be overwritten).
		weather_tables.et_table[ i ].et_inches_u16_10000u = (ET_TABLE_ET_100U_DEFAULT * 100);

		// 9/20/2012 rmd : The default et table value is as if it is an historical amount. Wouldn't
		// want to show as from the et gage as then they would be eligible to be used by the
		// substitution algorithm. Historical is the conventional default.
		weather_tables.et_table[ i ].status = ET_STATUS_HISTORICAL;

		// ----------------

		// RAIN TABLE
		weather_tables.rain_table[ i ].rain_inches_u16_100u = RAIN_TABLE_RAIN_100U_DEFAULT;

		// 9/20/2012 rmd : I've decided it makes sense that the default status is as if the number
		// came from the rain bucket. And the number is 0. So it's a valid cell with 0 inches of
		// rain reported by the rain bucket.
		weather_tables.rain_table[ i ].status = RAIN_STATUS_FROM_RAIN_BUCKET_M;
	}

	// --------

	// 10/28/2015 rmd : What should the table date be. I suppose the MIN_DATE value. Notice the
	// date I added this. I guess it was left at ? when the file was first created?
	weather_tables.et_rain_table_date = DATE_MINIMUM;
	
	// --------

	// 10/27/2015 rmd : Initialize the first_to_send_index to the end meaning send the whole
	// table. And make the pending false indicating no message in process.
	weather_tables.records_to_send = ET_RAIN_DAYS_IN_TABLE;

	weather_tables.pending_records_to_send_in_use = (false);
}


/* ---------------------------------------------------------- */
static void nm_weather_tables_updater( UNS_32 pfrom_revision )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only when
	// the file revision is changed. The caller is to be holding the weather_tables
	// recursive_MUTEX.

	// ----------

	if( pfrom_revision == WEATHER_TABLES_LATEST_FILE_REVISION )
	{
		Alert_Message_va( "W TABLES file unexpd update %u", pfrom_revision );   
	}
	else
	{
		Alert_Message_va( "WEATHER TABLES file update : to revision %u from %u", WEATHER_TABLES_LATEST_FILE_REVISION, pfrom_revision );

		// ----------

		if( pfrom_revision == 0 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 0 to REVISION 1.

			// 10/29/2015 rmd : Don't do anything. Do all the work in the move from REV 1 to REV 2.
			
			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 1 to 2 changes if
			// needed.
			pfrom_revision += 1;
		}

		// ----------

		if( pfrom_revision == 1 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 1 to REVISION 2.
				
			// 10/28/2015 rmd : Send the whole table.
			weather_tables.records_to_send = ET_RAIN_DAYS_IN_TABLE;

			// 10/28/2015 rmd : And no msg in process.
			weather_tables.pending_records_to_send_in_use = (false);

			pfrom_revision += 1;
		}
	}

	// ----------

	// 1/29/2015 rmd : Well after the updater has run the pfrom_revision is supposed to equal
	// the latest revision. By design of this update process.
	if( pfrom_revision != WEATHER_TABLES_LATEST_FILE_REVISION )
	{
		Alert_Message( "W TABLES updater error" );
	}
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION Initializes the weather tables file by either finding it or creating it.
	This function is called at each program start from the startup task.

	@CALLER_MUTEX_REQUIREMENTS (none)
	
	@MEMORY_RESPONSIBILITES (none)
	
	@EXECUTED Executed within the context of the STARTUP task.
	
	@RETURN (none)
	
	@ORIGINAL 2012.09.14 rmd
	
	@REVISIONS (none)
*/
extern void init_file_weather_tables( void )
{
	FLASH_FILE_find_or_create_variable_file(    FLASH_INDEX_1_GENERAL_STORAGE,
												WEATHER_TABLES_FILENAME,
												WEATHER_TABLES_LATEST_FILE_REVISION,
												&weather_tables,
												sizeof( weather_tables ),
												weather_tables_recursive_MUTEX,
												&nm_weather_tables_updater,
												&nm_WEATHER_TABLES_set_default_values,
												FF_WEATHER_TABLES );

	// ----------
	
	// 9/5/2014 rmd : Because we have included the controller initiated control variable in the
	// saved file there is some consideration here. First of all the first_to_send is assumed to
	// be valid and up to date. I think there are no real holes here. The problem to watch
	// out for is that the pending_first_send_in_use is saved into the file set (true). That
	// could happen I suppose. So that variable in particular on STARTUP should be set to
	// (false).
	weather_tables.pending_records_to_send_in_use = (false);
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION Saves the file. If the memory is not available to do so a postponed file
	save is performed.

	@CALLER_MUTEX_REQUIREMENTS (none)
	
	@MEMORY_RESPONSIBILITES (none)
	
	@EXECUTED Executed within the context of the TD_CHECK task when the ET or RAIN table
	rolls or is changed. Hmmmm. I think this could also execute from the COMM_MNGR task
	too. As rain events happen. Such as crossing the minimum.
	
	@RETURN (none)
	
	@ORIGINAL 2012.09.14 rmd
	
	@REVISIONS (none)
*/
extern void save_file_weather_tables( void )
{
	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE,
															WEATHER_TABLES_FILENAME,
															WEATHER_TABLES_LATEST_FILE_REVISION,
															&weather_tables,
															sizeof(weather_tables),
															weather_tables_recursive_MUTEX,
															FF_WEATHER_TABLES );
}

/* ---------------------------------------------------------- */
extern UNS_32 WEATHER_TABLES_get_et_table_date( void )
{
	// 2012.07.18 ajv : No mutex protection is necessary here because the copy
	// is an atomic operation.
	return( weather_tables.et_rain_table_date );
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION Using the argument ptr, gets the et table entry for the index specified.
	Note: the index is 0 based.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

	@EXECUTED Executed within the context of the TD_CHECK task. As part of the function that
	scans for start times. Actually multiple places within that function.
	
	@RETURN (true) if there were no errors. The only error can be the index is out of range.
	If out of range (false). The structure pointed to by the pointer argument is stuffed
	with the table entry.

	@ORIGINAL 2012.07.25 rmd

	@REVISIONS (none)
*/
extern BOOL_32 WEATHER_TABLES_get_et_table_entry_for_index( const UNS_32 pindex_0, ET_TABLE_ENTRY *et_entry_ptr )
{
	BOOL_32 rv;

	// 7/25/2012 rmd : The ET and RAIN table is protected with this MUTEX. So take it before
	// reading any table values.
	xSemaphoreTakeRecursive( weather_tables_recursive_MUTEX, portMAX_DELAY );

	if( pindex_0 >= ET_RAIN_DAYS_IN_TABLE )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "WEATHER: index out of range" );

		// 7/25/2012 rmd : Load with some semi-reasonable numbers. Just in case the caller uses the
		// structure. Which he should NOT.
		et_entry_ptr->et_inches_u16_10000u = (ET_TABLE_ET_100U_DEFAULT * 100);
		et_entry_ptr->status = ET_STATUS_HISTORICAL;

		// 7/25/2012 rmd : The user SHOULD NOT USE his structure.
		rv = (false);
	}
	else
	{
		*et_entry_ptr = weather_tables.et_table[ pindex_0 ];

		rv = (true);
	}

	xSemaphoreGiveRecursive( weather_tables_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern void WEATHER_TABLES_update_et_table_entry_for_index( const UNS_32 pindex_0, ET_TABLE_ENTRY *et_entry_ptr )
{
	// 2/7/2013 ajv : The ET and RAIN table is protected with this MUTEX, so
	// take it before updating the table.
	xSemaphoreTakeRecursive( weather_tables_recursive_MUTEX, portMAX_DELAY );

	if( pindex_0 >= ET_RAIN_DAYS_IN_TABLE )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "WEATHER: index out of range" );
	}
	else
	{
		weather_tables.et_table[ pindex_0 ].et_inches_u16_10000u = et_entry_ptr->et_inches_u16_10000u;
		weather_tables.et_table[ pindex_0 ].status = et_entry_ptr->status;
	}

	xSemaphoreGiveRecursive( weather_tables_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION Using the argument ptr, gets the rain table entry for the index specified.
	Note: the index is 0 based.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

	@EXECUTED Executed within the context of the TD_CHECK task. As part of the function that
	scans for start times. Actually multiple places within that function.
	
	@RETURN (true) if there were no errors. The only error can be the index is out of range.
	If out of range (false). The structure pointed to by the pointer argument is stuffed
	with the table entry.

	@ORIGINAL 2012.07.25 rmd

	@REVISIONS (none)
*/
extern BOOL_32 WEATHER_TABLES_get_rain_table_entry_for_index( const UNS_32 pindex_0, RAIN_TABLE_ENTRY *rain_entry_ptr )
{
	BOOL_32 rv;

	// 7/25/2012 rmd : The ET and RAIN table is protected with this MUTEX. So take it before
	// reading any table values.
	xSemaphoreTakeRecursive( weather_tables_recursive_MUTEX, portMAX_DELAY );

	if( pindex_0 >= ET_RAIN_DAYS_IN_TABLE )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "WEATHER: index out of range" );

		// -------------

		// 7/25/2012 rmd : Load with some semi-reasonable numbers. Just in case the caller uses the
		// structure. Which he should NOT. Remember the default status is as if the amount is from
		// the rain bucket. And the default amount is 0.
		rain_entry_ptr->rain_inches_u16_100u = RAIN_TABLE_RAIN_100U_DEFAULT;

		rain_entry_ptr->status = RAIN_STATUS_FROM_RAIN_BUCKET_M;

		// -------------

		// 7/25/2012 rmd : The user SHOULD NOT USE his structure.
		rv = (false);
	}
	else
	{
		*rain_entry_ptr = weather_tables.rain_table[ pindex_0 ];

		rv = (true);
	}

	xSemaphoreGiveRecursive( weather_tables_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
extern void WEATHER_TABLES_update_rain_table_entry_for_index( const UNS_32 pindex_0, RAIN_TABLE_ENTRY *rain_entry_ptr )
{
	// 9/10/2014 ajv : The ET and RAIN table is protected with this MUTEX, so
	// take it before updating the table.
	xSemaphoreTakeRecursive( weather_tables_recursive_MUTEX, portMAX_DELAY );

	if( pindex_0 >= ET_RAIN_DAYS_IN_TABLE )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "WEATHER: index out of range" );
	}
	else
	{
		weather_tables.rain_table[ pindex_0 ].rain_inches_u16_100u = rain_entry_ptr->rain_inches_u16_100u;
		weather_tables.rain_table[ pindex_0 ].status = rain_entry_ptr->status;
	}

	xSemaphoreGiveRecursive( weather_tables_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION Sets the battery backed weather_preserves variables that triggers all
	existing HISTORICAL values in the et table to be recalculated. The values will be
	actually reloaded at the next TD_CHECK 1 hz recognition.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

	@EXECUTED Can be executed from the STARTUP task when et table file is first created. Or
	from the KEY_PROCESS task as the user changes the Count/City. Or from the COMM_MNGR as
	the central loads changes.

	@RETURN (none)

	@ORIGINAL 2012.07.25 rmd

	@REVISIONS (none)
*/
extern void WEATHER_TABLES_cause_et_table_historical_values_to_be_updated( void )
{
	// 7/25/2012 rmd : Before changing the preserves value take this MUTEX.
	xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );

	weather_preserves.et_table_update_all_historical_values = (true);

	xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION This function is to be called at the station start time (regardless of if
	its a water day or not) to take todays rain in the table and turn it into negative time.

	@CALLER_MUTEX_REQUIREMENTS The caller is to be holding the station preserves MUTEX. As
	we are going potentially modify several station preserves values.

	@MEMORY_RESPONSIBILITES (none)

	@EXECUTED Executed within the context of the TD_CHECK task. At the station start
	time.
	
	@RETURN (none)

	@ORIGINAL 2012.06.04 rmd

	@REVISIONS
		3/11/2013 ajv : Refactored local variable names to be more explicit
		regarding their use and units. Also fixed an issue where the rain
		minutes were calculated incorrectly.
*/
extern void nm_RAIN_for_this_station_generate_rain_time_for_raintable_slot1( STATION_STRUCT *const pss_ptr, const UNS_32 pstation_preserves_index )
{
	float           precip_rate_in_per_min;

	UNS_32          rain_max_minutes_10u;

	if( pstation_preserves_index >= STATION_PRESERVE_ARRAY_SIZE )
	{
		Alert_index_out_of_range();	 // Should of course never happen.
	}
	else
	{
		// 09.20.2011 rmd :  If a station was irrigating at the time this was called you may not end
		// up with the expected negative amount after that station turned off - but the condition to
		// have a station irrigating when this is called are complex - I'm not sure it can happen.
		// It shouldn't! (If the station is ON when we share an 'R' we turn it off - I don't think a
		// station could be ON at its start time if there's an 'R' in the table.)
		//
		// TODO check behavior of -ve rain time in the station preserves if irrigating when a start
		// time is hit.

		// 7/23/2012 rmd : The ET and RAIN table is protected with this MUTEX. So take it before
		// reading any table values.
		xSemaphoreTakeRecursive( weather_tables_recursive_MUTEX, portMAX_DELAY );

		if( (weather_tables.rain_table[ 1 ].rain_inches_u16_100u > 0) && 
			(
			(weather_tables.rain_table[ 1 ].status == RAIN_STATUS_FROM_RAIN_BUCKET_R) || 
			(weather_tables.rain_table[ 1 ].status == RAIN_STATUS_SHARED_FROM_THE_CENTRAL) || 
			(weather_tables.rain_table[ 1 ].status == RAIN_STATUS_FROM_WEATHERSENSE)
			)
		  )
		{
			// 9/24/2012 rmd : Do not make 'rain time' for stations not in use. Remember these are
			// effectively no water days. So from that point of view we wouldn't. Additionally the math
			// for stations not in use may produce very large numbers which could produce an exception
			// or other undesireable result. NOTE: at the start time on a water day stations not in use
			// have their 'rain time' set to 0.
			if( STATION_station_is_available_for_use( pss_ptr ) == (true) )
			{
				// So far this station is in use and a member of the schedule group
				if( WEATHER_get_station_uses_rain( pss_ptr ) == (true) )
				{
					// 3/11/2013 ajv : Since the precip rate entered by the user
					// is inches/hr, divide the value by 60 to get the
					// inches/min.
					precip_rate_in_per_min = (float)((STATION_GROUP_get_station_precip_rate_in_per_hr_100000u( pss_ptr ) / 100000.0) / 60.0);

					// There is an interesting thought about if we should be including some factor
					// for the station related to the stations percent of ET...upon analysis this
					// factor would actually have to be the inverse of the % of ET to modify the rain
					// the right direction...upon further thought you see we effectively have a factor
					// by program that takes on the value of 0% or 100% - it is called rain by program
					//
					// After david byma and I thought about it for a while we concluded that the station
					// % of ET probably (&intentionally) encompasses factors that should not be used to
					// modify the rain with
					station_preserves.sps[ pstation_preserves_index ].rain_minutes_10u += (UNS_16)(10 * ( ((float)weather_tables.rain_table[ 1 ].rain_inches_u16_100u / 100.0) / precip_rate_in_per_min ));

					// Now check against the MaxInchesOfRainHO.
					rain_max_minutes_10u = 10 * (UNS_32)( ((float)STATION_GROUP_get_soil_storage_capacity_inches_100u( pss_ptr ) / 100.0) / precip_rate_in_per_min );

					if( (UNS_32)station_preserves.sps[ pstation_preserves_index ].rain_minutes_10u > rain_max_minutes_10u )
					{
						station_preserves.sps[ pstation_preserves_index ].rain_minutes_10u = (UNS_16)rain_max_minutes_10u;
					}

					// ----------------
				}
			}
		}

		xSemaphoreGiveRecursive( weather_tables_recursive_MUTEX );

	}
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION Using the known et gage capacity, returns what percentage of the et gage
	capacity the passed argument value represents. Likely used by the display task to
	display this information.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

	@EXECUTED Executed within the context of the DISPLAY task.
	
	@RETURN Percentage full pamount represents.

	@ORIGINAL 2012.07.23 rmd

	@REVISIONS (none)
*/
static UNS_32 ET_GAGE_percent_full( UNS_32 pamount )
{
	UNS_32  rv;

	rv = (UNS_32)(((float)pamount / ET_GAGE_PULSE_CAPACITY) * 100.0);

	return( rv );
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION This function inserts the current count into slot 1. Is called as part of
	rolling the table. And therefore called at the ET Table roll time. And should be called
	only by MASTERS. The master controller maintains the et table. And distributes the table
	when it changes.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

	@EXECUTED Executed within the context of the TD_CHECK task as part of the et table
	roll.
	
	@RETURN (none)

	@ORIGINAL 2012.09.14 rmd

	@REVISIONS (none)
*/
static void insert_et_gage_number_into_table( void )
{
	// 9/14/2012 rmd : The reading and other settings are held in the preserves. Ensure
	// stability.
	xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );

	// 9/14/2012 rmd : If a there is a weather card in the chain and the et_gage is set to be
	// IN-USE then let's proceed.
	if( WEATHER_get_et_gage_is_in_use() == (true) )
	{
		if( weather_preserves.run_away_gage == (true) )
		{
			Alert_ETGage_RunAway();
		}
		else
			if( weather_preserves.dont_use_et_gage_today == (true) )
		{
			weather_preserves.dont_use_et_gage_today = (false);
		}
		else
		{
			// 9/14/2012 rmd : ok...so there is a gage and there aren't any problems. Normally we are
			// going to set the status of slot 1 - except if this is the second day in a row of 0
			// pulses. As a mechanism to prevent 0's filling the table.
			if( weather_preserves.et_rip.et_inches_u16_10000u == 0 )
			{
				// If yesterdays ET was also 0 we are going to leave the Historical value in the table.
				if( weather_preserves.yesterdays_ET_was_zero == (true) )
				{
					Alert_ETGage_Second_or_More_Zeros();

					// 9/14/2012 rmd : And leave the historical value in place.
				}
				else
				{
					Alert_ETGage_First_Zero();

					weather_tables.et_table[ 1 ] = weather_preserves.et_rip;
				}

				// 9/14/2012 rmd : Set the flag.
				weather_preserves.yesterdays_ET_was_zero = (true);
			}
			else
			{
				// 9/14/2012 rmd : This is IT. Let the table reflect the gage reading.
				weather_tables.et_table[ 1 ] = weather_preserves.et_rip;

				// 9/14/2012 rmd : Clear the flag.
				weather_preserves.yesterdays_ET_was_zero = (false);

				// 9/17/2012 rmd : Well I suppose if all is normal and the accumulated count has not been
				// set by the user (it is from the gage) then we will track the remaining level.
				if( weather_preserves.et_rip.status == ET_STATUS_FROM_ET_GAGE )
				{
					if( weather_preserves.remaining_gage_pulses >= (weather_preserves.et_rip.et_inches_u16_10000u / 100) )
					{
						weather_preserves.remaining_gage_pulses -= (weather_preserves.et_rip.et_inches_u16_10000u / 100);
					}
					else
					{
						weather_preserves.remaining_gage_pulses = 0;
					}
				}
			}

		}  // of we are going to insert a number into the table (unless its zero again)

		// --------

		// 9/17/2012 rmd : Make a gage full alert each night at the roll time. Regardless of if we
		// used the et_rip or not. If the resevoir is down alert them.

		UNS_32  percent_full;

		percent_full = ET_GAGE_percent_full( weather_preserves.remaining_gage_pulses );

		if( percent_full < 15 )
		{
			Alert_ETGage_percent_full_warning( percent_full );
		}

	}  // of if we have an active et gage

	// --------

	// 9/14/2012 rmd : Regardless of what happened above reset the preserves pulse count entry.
	// Including the status to clear if the user edited.
	weather_preserves.et_rip.et_inches_u16_10000u = 0;

	weather_preserves.et_rip.status = ET_STATUS_FROM_ET_GAGE;

	// --------

	xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void WEATHER_TABLES_load_record_with_historical_et( UNS_32 ptable_index, ET_TABLE_ENTRY *pptr_table_record )
{
	// 9/17/2012 rmd : Loads a slot in the ET table with its historical value and status.
	// Remember the table runs backwards into time. And the table date is tied to slot 0.
	UNS_32  LD, LM, LY_4digit, LDOW;

	// 10/14/2015 rmd : So we are not in the middle of rolling the table or editing when this
	// function is called.
	xSemaphoreTakeRecursive( weather_tables_recursive_MUTEX, portMAX_DELAY );

	if( ptable_index < ET_RAIN_DAYS_IN_TABLE )
	{
		DateToDMY( (weather_tables.et_rain_table_date - ptable_index), &LD, &LM, &LY_4digit, &LDOW );

		pptr_table_record->et_inches_u16_10000u = __round_UNS16( 10000.0 * ET_DATA_et_number_for_the_month( LM ) / (float)NumberOfDaysInMonth( LM, LY_4digit ) );
	}
	else
	{
		Alert_Message( "Table Index OOR" );

		pptr_table_record->et_inches_u16_10000u = (ET_TABLE_ET_100U_DEFAULT * 100);
	}

	pptr_table_record->status = ET_STATUS_HISTORICAL;

	xSemaphoreGiveRecursive( weather_tables_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION Adds in the accumulated hourly rain. This function manages the rolling of
	the et table. And is called once per second. If we are at the roll time action is taken.
	If the table date does not reflect the present date and time action is taken. If there
	is a request to fill the table with historical action is taken.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

	@EXECUTED Executed within the context of the TD_CHECK task once per second.
	
	@RETURN (none)

	@ORIGINAL 2012.09.14 rmd

	@REVISIONS (none)
*/
static void __check_if_time_to_add_the_hourly_rain_to_the_rip( DATE_TIME const *const pdate_time, UNS_32 const proll_time )
{
	// 9/24/2012 rmd : Are we at the 'top of the hour' as defined by the roll time? Remember the
	// roll time doesn't necessarily have to be at the top of a clock hour.
	if( (pdate_time->T % 3600) == (proll_time % 3600) )
	{
		// 9/14/2012 rmd : The reading and other settings are held in the preserves. Ensure
		// stability. Especially when testing the poll count.
		xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );

		// ----------------

		// 9/24/2012 rmd : If the user has not edited the rip check to see if the new hourly total
		// plus the total already in the table exceed the minimum. If the user has edited the rip is
		// no longer valid for this check.
		if( (weather_preserves.rain.rip.status == RAIN_STATUS_FROM_RAIN_BUCKET_R) || (weather_preserves.rain.rip.status == RAIN_STATUS_FROM_RAIN_BUCKET_M) )
		{
			// 9/24/2012 rmd : Obtain the needed limits.
			UNS_32  minimum_to_inhibit_irrigation, maximum_24hr;

			minimum_to_inhibit_irrigation = WEATHER_get_rain_bucket_minimum_inches_100u();

			maximum_24hr = WEATHER_get_rain_bucket_max_24_hour_inches_100u();

			// -------------

			// 9/24/2012 rmd : The hourly accumulated amount should be clean and under the hourly max
			// amount. Because as we add to the hourly amount we test that it does not exceed the
			// allowed hourly amount.

			if( weather_preserves.rain.rip.status == RAIN_STATUS_FROM_RAIN_BUCKET_R )
			{
				// 9/24/2012 rmd : Directly add it to the on-going total.
				weather_preserves.rain.rip.rain_inches_u16_100u += (UNS_16)weather_preserves.rain.hourly_total_inches_100u;
			}
			else
				if( (weather_preserves.rain.rip.rain_inches_u16_100u + weather_preserves.rain.hourly_total_inches_100u) >= minimum_to_inhibit_irrigation )
			{
				// 9/24/2012 rmd : Add and subtract back out the minimum. Be careful to perform this math as
				// discrete steps otherwise could generate an 'underflow'.
				weather_preserves.rain.rip.rain_inches_u16_100u += (UNS_16)weather_preserves.rain.hourly_total_inches_100u;

				weather_preserves.rain.rip.rain_inches_u16_100u -= (UNS_16)minimum_to_inhibit_irrigation;

				// 9/24/2012 rmd : Remove the 'funny' if it ends up to be 0. Just makes questions.
				if( weather_preserves.rain.rip.rain_inches_u16_100u == 0 )
				{
					weather_preserves.rain.rip.rain_inches_u16_100u = 1;
				}

				// 9/24/2012 rmd : Set the status.
				weather_preserves.rain.rip.status = RAIN_STATUS_FROM_RAIN_BUCKET_R;

				// 9/24/2012 rmd : NOTE - we already caught the crossing of the MINIMUM in FOAL_COMM when
				// the pulse came in on the token_resp. And set the inhibit flag as a result. Therefore that
				// need not be done here. However we could run a simple sanity test to ensure code is
				// performing as expected.
				if( weather_preserves.rain.inhibit_irrigation_have_crossed_the_minimum == (false) )
				{
					// 9/25/2012 rmd : I do not EVER expect to see this alert.
					ALERT_MESSAGE_WITH_FILE_NAME( "WEATHER: flag not set" );

					// 9/25/2012 rmd : I suppose correct the unexpected state. But really the code should be
					// understood as to why this state exists.
					weather_preserves.rain.inhibit_irrigation_have_crossed_the_minimum = (true);
				}

			}
			else
			{
				// 9/25/2012 rmd : Add to the rip. The total will remain below the minimum.
				weather_preserves.rain.rip.rain_inches_u16_100u += (UNS_16)weather_preserves.rain.hourly_total_inches_100u;
			}

			// 9/24/2012 rmd : Check to see that the accumulated amount does not exceed the 24 hour
			// limit.
			if( weather_preserves.rain.rip.rain_inches_u16_100u > maximum_24hr )
			{
				weather_preserves.rain.rip.rain_inches_u16_100u = (UNS_16)maximum_24hr;
			}
		}

		// 9/25/2012 rmd : Always reset the hourly accumulator.
		weather_preserves.rain.hourly_total_inches_100u = 0;

		// ----------------

		xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );
	}
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION This function inserts the current count into slot 1. Is called as part of
	rolling the table. And therefore called at the Rain Table roll time. And should be
	called only by MASTERS which is the case the way the roll is called via td check. (The
	master controller maintains the rain table. And distributes the table when it changes.)

	@CALLER_MUTEX_REQUIREMENTS Caller to be holding the weather_preserves MUTEX as reading
	and setting values within that structure.

	@MEMORY_RESPONSIBILITES (none)

	@EXECUTED Executed within the context of the TD_CHECK task as part of the rain table
	roll.
	
	@RETURN (false) is did not change slot 1. (true) if we did.

	@ORIGINAL 2012.09.21 rmd

	@REVISIONS (none)
*/
static BOOL_32 nm_insert_rain_number( void )
{
	BOOL_32 rv;

	// ----------

	rv = (false);

	// 9/21/2012 rmd : We could mostly get away without checking if there is a bucket in the
	// chain here. That is because we makes this test as we accumulate the rip values. There is
	// hole though if the guy turns off the rain bucket use. If we didn't check here and pulses
	// had accumulated in the rip they'd still show in the table. So best to check again here.
	if( WEATHER_get_rain_bucket_is_in_use() == (true) )
	{
		// 9/21/2012 rmd : I think it is very straight forward. Take the rip and write it to slot
		// 1. Whether the rip is a user edited value or not. We assign to slot 1. Table distribution
		// to all controllers is controlled in the roll function whena table change is detected. One
		// way is our return value signals a change.
		weather_tables.rain_table[ 1 ] = weather_preserves.rain.rip;

		rv = (true);
	}

	// ----------------

	// 9/21/2012 rmd : Clear the rip back to default. Whether or not there is a bucket in the
	// network.
	weather_preserves.rain.rip.rain_inches_u16_100u = 0;

	weather_preserves.rain.rip.status = RAIN_STATUS_FROM_RAIN_BUCKET_M;

	// ----------------

	return( rv );
}

/* ---------------------------------------------------------- */
extern void WEATHER_TABLES_roll_et_rain_tables( DATE_TIME const *const pdate_time )
{
	UNS_32  i, target_date;

	UNS_32  roll_time;

	BOOL_32 table_has_changed;

	// 9/17/2012 rmd : We are going to roll the table. So take the MUTEX so other task doesn't
	// try to use mid-stream.
	xSemaphoreTakeRecursive( weather_tables_recursive_MUTEX, portMAX_DELAY );

	table_has_changed = (false);

	// ----------------

	// 9/17/2012 rmd : The table date is tomorrows date after the roll time. That is the way the
	// table works.
	roll_time = WEATHER_get_et_and_rain_table_roll_time();

	if( pdate_time->T < roll_time )
	{
		target_date = pdate_time->D;
	}
	else
	{
		target_date = pdate_time->D + 1;
	}

	// ----------------

	if( weather_tables.et_rain_table_date != target_date )
	{
		// 9/17/2012 rmd : Well we know we are going to roll the table at least one slot.
		table_has_changed = (true);

		// ----------
		
		if( weather_tables.et_rain_table_date < target_date )
		{
			// 10/27/2015 rmd : How much of the table needs to be sent? Well if it more than ONE day
			// difference just send the whole thing. Normally it will only be one day difference and
			// we'll make sure the index is at least 1.
			if( (target_date - weather_tables.et_rain_table_date) > 1 )
			{
				// 10/28/2015 rmd : Send 'em all.
				weather_tables.records_to_send = ET_RAIN_DAYS_IN_TABLE;
			}
			else
			{
				// 10/27/2015 rmd : This is the normal everyday case. The day has moved by one. And when we
				// enter this function the records_to_send is 0. Which means we successfully sent the data
				// from the last time we rolled. If the message failed the records_to_send would already be
				// at TWO. This should be very rare and indicates comm issues. Think about if the power were
				// to go out say after we rolled but before midnight. When power was restored the
				// records_to_send would be 2 yet and when this function gets called we have 1 or more days
				// to roll possibly etc.... To cover all out-of-the-normal scenarios I am going to just send
				// the WHOLE table if we are not the usual case.
				if( weather_tables.records_to_send == 0 )
				{
					weather_tables.records_to_send = 2;
				}
				else
				{
					weather_tables.records_to_send = ET_RAIN_DAYS_IN_TABLE;
				}
			}

			// ----------
			
			// 9/17/2012 rmd : Speed up long rolls (when date is moved a long way).
			if( (target_date - weather_tables.et_rain_table_date) > ET_RAIN_DAYS_IN_TABLE )
			{
				// 9/17/2012 rmd : Make the target date just old enough to cause the whole table to fill
				// with recent data.
				weather_tables.et_rain_table_date = target_date - ET_RAIN_DAYS_IN_TABLE - 1;
			}

			while( weather_tables.et_rain_table_date < target_date )
			{
				weather_tables.et_rain_table_date += 1;

				// ----------
				
				// 9/17/2012 rmd : The table date is less than the target so move the table forward in time.
				for( i=(ET_RAIN_DAYS_IN_TABLE-1); i>0; i-- )
				{
					weather_tables.et_table[ i ] = weather_tables.et_table[ i - 1 ];

					weather_tables.rain_table[ i ] = weather_tables.rain_table[ i - 1 ];
				}

				// ----------
				
				// 10/28/2015 rmd : This function MUST have the et_rain_table_date changed before calling.
				// It uses the table date to figure out the correct historical value to put into the table.
				WEATHER_TABLES_load_record_with_historical_et( 0, &weather_tables.et_table[ 0 ] );
				
				// ----------
				
				weather_tables.rain_table[ 0 ].rain_inches_u16_100u = 0;

				weather_tables.rain_table[ 0 ].status = RAIN_STATUS_FROM_RAIN_BUCKET_M;
			}
		}
		else
		{
			// 10/27/2015 rmd : If we are rolling backwards send the whole thing. This is the exception
			// and should rarely happen.
			weather_tables.records_to_send = ET_RAIN_DAYS_IN_TABLE;
			
			// ----------

			// 9/21/2012 rmd : Table date must be greater than the target. Speed up long rolls (when
			// date is moved a long way).
			if( (weather_tables.et_rain_table_date - target_date) > ET_RAIN_DAYS_IN_TABLE )
			{
				// 9/17/2012 rmd : Make the target date just old enough to cause the whole table to fill
				// with recent data.
				weather_tables.et_rain_table_date = target_date + ET_RAIN_DAYS_IN_TABLE + 1;
			}

			while( weather_tables.et_rain_table_date > target_date )
			{
				weather_tables.et_rain_table_date -= 1;
				
				// ----------
				
				// 9/17/2012 rmd : The table date is less than the target so move the table forward in time.
				for( i=0; i<(ET_RAIN_DAYS_IN_TABLE-1); i++ )
				{
					weather_tables.et_table[ i ] = weather_tables.et_table[ i + 1 ];

					weather_tables.rain_table[ i ] = weather_tables.rain_table[ i + 1 ];
				}

				// ----------
				
				// 10/28/2015 rmd : This function MUST have the et_rain_table_date changed before calling.
				// It uses the table date to figure out the correct historical value to put into the table.
				WEATHER_TABLES_load_record_with_historical_et( (ET_RAIN_DAYS_IN_TABLE - 1), &weather_tables.et_table[ (ET_RAIN_DAYS_IN_TABLE - 1) ] );

				// ----------
				
				// 9/17/2012 rmd : Load the last entry in the table.
				weather_tables.rain_table[ (ET_RAIN_DAYS_IN_TABLE - 1) ].rain_inches_u16_100u = 0;

				weather_tables.rain_table[ (ET_RAIN_DAYS_IN_TABLE - 1) ].status = RAIN_STATUS_FROM_RAIN_BUCKET_M;
			}
		}
	}

	// ----------
	
	// 9/17/2012 rmd : NOW independently of the ripple see if we are supposed to fill in the
	// table with new historical numbers cause either we are a startup or the city index changed
	// or one of the user entered monthly ET values were changed.
	if( weather_preserves.et_table_update_all_historical_values )
	{
		// 9/17/2012 rmd : Assume we are going to change at least one historical. Even if we don't
		// this request is made so infrequently this assumption is fine. The result is a table
		// re-sync to all devices.
		table_has_changed = (true);

		// ----------
		
		// 10/28/2015 rmd : Because we are modifying the entire table send 'em all to the web
		// server.
		weather_tables.records_to_send = ET_RAIN_DAYS_IN_TABLE;
		
		// ----------
		
		for( i=0; i<ET_RAIN_DAYS_IN_TABLE; i++ )
		{
			// 9/17/2012 rmd : Only replace historical entries.
			if( weather_tables.et_table[ i ].status == ET_STATUS_HISTORICAL )
			{
				WEATHER_TABLES_load_record_with_historical_et( i, &weather_tables.et_table[ i ] );
			}
		}

		weather_preserves.et_table_update_all_historical_values = (false);

		Alert_ET_table_loaded();
	}

	// ----------
	
	// 9/24/2012 rmd : UPDATE the rip. See if we should incorporate the hourly rain bucket
	// amount into the rip.
	__check_if_time_to_add_the_hourly_rain_to_the_rip( pdate_time, roll_time );

	// ----------------

	// 9/25/2012 rmd : If at the roll time insert the rip into slot 1. The rip could be a rain
	// bucket value or an edited value.
	if( pdate_time->T == roll_time )
	{
		table_has_changed = (true);
		
		// ----------
		
		// 10/27/2015 rmd : If we insert the gage number we send the first 2 records [index 0 and
		// index 1]. Probably we just rolled the table and therefore records_to_send is already set
		// but just in case check here.
		if( weather_tables.records_to_send < 2 )
		{
			weather_tables.records_to_send = 2;
		}

		// ----------
		
		// 9/14/2012 rmd : The reading and other settings are held in the preserves. Ensure
		// stability. Especially when testing the poll count.
		xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );

		// ----------
		
		// 9/17/2012 rmd : If we happen to be at the roll time place the gage number into the table.
		// Which could be an accumulated gage reading or a user edited value.
		insert_et_gage_number_into_table();
		
		// ----------
		
		// 9/25/2012 rmd : If we change slot 1 returns (true).
		if( nm_insert_rain_number() )
		{
			table_has_changed = (true);
		}

		// 9/21/2012 rmd : And reset if we crossed the minimum or not. Also performing a sanity
		// check [if this was set (true) that we crossed the minimum, there surely has to be a RAIN
		// value in slot 1 now - check for that case].
		if( weather_preserves.rain.inhibit_irrigation_have_crossed_the_minimum == (true) )
		{
			if( weather_tables.rain_table[ 1 ].status != RAIN_STATUS_FROM_RAIN_BUCKET_R )
			{
				Alert_Message( "WEATHER: unexp rain table status" );
			}

			// 9/21/2012 rmd : And clear it. We can clear this because our OWN rain table now has an R
			// in slot 1 which will inhibit irrigation that honors the rain table.
			weather_preserves.rain.inhibit_irrigation_have_crossed_the_minimum = (false);
		}

		// ----------

		// 6/16/2015 rmd : At the roll time decrement the count by 1. See the variable definition
		// for an explanation of the scheme.
		if( weather_preserves.rain.rain_shutdown_rcvd_from_commserver_uns32 > 0 )
		{
			weather_preserves.rain.rain_shutdown_rcvd_from_commserver_uns32 -= 1;
		}

		// ----------

		// 9/21/2012 rmd : What are we doing with the raw pulse count? Well anyway it needs to be
		// zeroed here.
		weather_preserves.rain.roll_to_roll_raw_pulse_count = 0;

		// ----------
		
		xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );
	}

	// ----------

	if( table_has_changed )
	{
		// 9/18/2012 rmd : If the table has changed we want to save the file. Why delayed? Well
		// highly likely we're going to roll and save the RAIN table too. The rain table delayed
		// save will restart the timer for this file. And the result will be a single write to the
		// flash file system.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_WEATHER_TABLES, FLASH_STORAGE_seconds_to_delay_file_save_after_rolling_report_data );

		// ----------

		// 4/27/2015 rmd : Set the flag to sync the rain table. Because multiple tasks set and clear
		// the sync_the_rain_table flag we should take the MUTEX.
		xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );

		weather_preserves.sync_the_et_rain_tables = (true);
		
		xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );
	}

	// ----------------

	xSemaphoreGiveRecursive( weather_tables_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION If the request is (true) allocates memory for the et table and moves a copy
	of it there. Along with the date first.

	@CALLER_MUTEX_REQUIREMENTS (none)
	
	@MEMORY_RESPONSIBILITES (none)
	
	@EXECUTED Executed within the context of the COMM_MNGR task when the outgoing flowsense
	token is being built.
	
	@RETURN The length of the block or 0 if nothing to send.

	@ORIGINAL 2012.09.18 rmd
	
	@REVISIONS (none)
*/
extern UNS_32 WEATHER_TABLES_load_et_rain_tables_into_outgoing_token( UNS_8 **pet_rain_tables_data_ptr )
{
	UNS_32  rv;

	UNS_8   *ucp;

	rv = 0;

	*pet_rain_tables_data_ptr = NULL;

	// ----------
	
	if( weather_preserves.sync_the_et_rain_tables )
	{
		// 9/18/2012 rmd : We are taking a copy of the whole table so take the MUTEX so other tasks
		// don't change the table mid-stream.
		xSemaphoreTakeRecursive( weather_tables_recursive_MUTEX, portMAX_DELAY );
	
		// 4/27/2015 rmd : Because multiple tasks set and clear the sync_the_rain_table flag we
		// should take the MUTEX.
		xSemaphoreTakeRecursive( weather_preserves_recursive_MUTEX, portMAX_DELAY );
	
		// ----------------
	
		rv = ( sizeof(weather_tables.et_rain_table_date) + sizeof(weather_tables.et_table) +  sizeof(weather_tables.rain_table) );

		ucp = mem_malloc( rv );

		*pet_rain_tables_data_ptr = ucp;


		memcpy( ucp, &(weather_tables.et_rain_table_date), sizeof(weather_tables.et_rain_table_date) );

		ucp += sizeof(weather_tables.et_rain_table_date);


		memcpy( ucp, &(weather_tables.et_table), sizeof(weather_tables.et_table) );

		ucp += sizeof(weather_tables.et_table);


		memcpy( ucp, &(weather_tables.rain_table), sizeof(weather_tables.rain_table) );

		ucp += sizeof(weather_tables.rain_table);
		
		// ----------
		
		// 4/27/2015 rmd : Clear the flag.
		weather_preserves.sync_the_et_rain_tables = (false);

		// ----------
		
		xSemaphoreGiveRecursive( weather_preserves_recursive_MUTEX );
	
		xSemaphoreGiveRecursive( weather_tables_recursive_MUTEX );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION Think of this as an irri side function. It is called when parsing the
	incoming token from the foal machine.

	@CALLER_MUTEX_REQUIREMENTS (none)
	
	@MEMORY_RESPONSIBILITES (none)
	
	@EXECUTED Executed within the context of the COMM_MNGR task when the incoming flowsense
	token is received by the irri machine.
	
	@RETURN Always returns (true). There is no error checking. And always moves the message
	pointer the appropriate amount.

	@ORIGINAL 2012.09.18 rmd
	
	@REVISIONS (none)
*/
extern BOOL_32 WEATHER_TABLES_extract_et_rain_tables_out_of_msg_from_main( UNS_8 **pucp_ptr, UNS_32 pfrom_serial_number )
{
	BOOL_32 rv;

	rv = (true);

	// 9/18/2012 rmd : If this message is from ourselves no sense saving the et table again. But
	// that doesn't mean the message is bad. Remember the token is broadcast.
	if( config_c.serial_number == pfrom_serial_number )
	{
		*pucp_ptr += sizeof(weather_tables.et_rain_table_date) + sizeof(weather_tables.et_table) + sizeof(weather_tables.rain_table);
	}
	else
	{
		// 9/18/2012 rmd : We are moving in a copy to the whole table so take the MUTEX so other
		// tasks don't change the table mid-stream.
		xSemaphoreTakeRecursive( weather_tables_recursive_MUTEX, portMAX_DELAY );

		memcpy( &(weather_tables.et_rain_table_date), *pucp_ptr, sizeof(weather_tables.et_rain_table_date) );
		*pucp_ptr += sizeof(weather_tables.et_rain_table_date);

		memcpy( &(weather_tables.et_table), *pucp_ptr, sizeof(weather_tables.et_table) );
		*pucp_ptr += sizeof(weather_tables.et_table);

		memcpy( &(weather_tables.rain_table), *pucp_ptr, sizeof(weather_tables.rain_table) );
		*pucp_ptr += sizeof(weather_tables.rain_table);

		xSemaphoreGiveRecursive( weather_tables_recursive_MUTEX );

		// ----------------

		// 9/18/2012 rmd : Use the delayed file save as the message could contain the rain table as
		// well.
		FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_WEATHER_TABLES, FLASH_STORAGE_seconds_to_delay_file_save_after_rolling_report_data );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/28/2016 skc : Support for budget calculations.
static float et_ratio = 1.0f;

/* ---------------------------------------------------------- */
// 4/28/2016 skc : Calculate the average daily ET value from the weather table data
// Mutex : none required
// Task: called from TD_CHECK
//
// num_days - number of days to check the weather table, max is (60-1)
extern void WEATHER_TABLES_set_et_ratio( UNS_32 num_days )
{
	float sum;

	float i;

	ET_TABLE_ENTRY et_entry;

	UNS_32 count; // number of entries used in average

	float et_hist;

	DATE_TIME_COMPLETE_STRUCT today;

	UNS_32 days_in_month;

	float et_limit; // upper limit on daily ET as percent of Historical ET

	// ----------

	sum = 0.f;
	count = 0;
	et_limit = WEATHER_get_percent_of_historical_cap_100u() * 0.01f; // convert to units of ratio

	// trap invalid parameter, set the max allowed index
	if( num_days > ET_RAIN_DAYS_IN_TABLE - 1 )
	{
		num_days = ET_RAIN_DAYS_IN_TABLE - 1;
	}

	et_hist = 0.f;

	// 7/11/2016 skc : Match up the dates with the ET table indices.
	EPSON_obtain_latest_complete_time_and_date( &today );
	today.date_time.D -= 1;

	// ----------

	xSemaphoreTakeRecursive( weather_tables_recursive_MUTEX, portMAX_DELAY );

	et_ratio = 1.0; // nice default value

	if( num_days > 0 )
	{
		// 4/28/2016 skc : Skip element zero
		for( i=1; i<=num_days; i++ )
		{
			// get day, month data out of date_time.D
			DateAndTimeToDTCS( today.date_time.D, today.date_time.T, &today );
			days_in_month = NumberOfDaysInMonth(today.__month, today.__year);

			// 7/11/2016 skc : Get the historical ET value for this day (x10000).
			et_hist = 10000 * ET_DATA_et_number_for_the_month(today.__month) / days_in_month;

			// 4/28/2016 skc : We already validated the index values
			WEATHER_TABLES_get_et_table_entry_for_index( i, &et_entry);

			// 5/18/2016 rmd : Count ALL entries regardless of status. What is driving irrigation? This table
			// is. If it is all full of historical we want our ratio to end up being 1.0. If it is full of half
			// historical we don't want to leave out the fact that a large part of our irrigation is being driven
			// by historical. So no test needed on the ET_TABLE_ENTRY status of the entry. I have even considered
			// what if the user is editing the entries. Well then he is and they are controlling irrigation - so
			// include 'em. That IS the point of the ratio.

			// 7/11/2016 skc : If we use a mix of historical and daily ET here, we are then projecting
			// possible lapses in comms or WeatherSense onto future dates.

			// 7/12/2016 skc : We are finding the average ratio over the time period.
			sum += (float)et_entry.et_inches_u16_10000u / et_hist;
			++count;

			// 7/11/2016 skc : Move date back one day.
			today.date_time.D--;
		}

		et_ratio = sum / count; // take average

		// 7/12/2016 skc : Don't let ratio exceed the ET limit value
		if( et_ratio > et_limit )
		{
			et_ratio = et_limit;
		}

		// 5/16/2016 skc : Let us know what the value we use is.
		// 3/21/2018 ajv : Suppressed as it divulges proprietary information
		//Alert_Message_va( "ETR: %0.2f %0.2f", et_ratio, et_limit );
	}

	xSemaphoreGiveRecursive( weather_tables_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
// 4/28/2016 skc : Return the average daily ET value
// Mutex : none required
// Task: called from TD_CHECK
extern float WEATHER_TABLES_get_et_ratio()
{
	return et_ratio;
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
