/*  file = irri_subs.c             02/09/01  2011.08.01  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memcpy
#include	<string.h>

#include	"irri_subs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#if 0


/*--------------------------------------------------------------------------
	Name:           IRRI_Load_SMSSEQ( void )

	Description:    Called by the time machine when the smsseq startin timer transitions from a 1
					to a 0.
					
					Look through the sequence for stations < MAX_STATIONS that are in use. Add them to
					the irri list.
					
	Parameters:     none
	
	Return Values:  none
	  
	Date:           rmd 05/24/01
	
-------------------------------------------------------------------------*/
void IRRI_Load_SMSSEQ( void ) {
	
unsigned short i;

unsigned long irrigation_seconds;

BOOLEAN log_it;

char str_64[ 64 ];

	log_it = FALSE;

	// 604.m.12 11.07.2006 ajv - IRRI_PutOnListOfThoseToAdd displays an advisory
	// whenever a station is added to the list when a mainline break is in
	// effect.  For the case of walk-thrus, however, IRRI_PutOnListOfThoseToAdd
	// is only called if irrigation_seconds > 0.  Since we set the seconds = 0
	// if a mainline break is in effect, no advisory is displayed.  Therefore,
	// before doing any operations to the run times, let's check if there is a
	// mainline break and display an advisory if there is.
	//
	if ( IrriFlow.MainlineBreakInEffect == TRUE ) {

		// 605.b.1 ajv - To avoid having the same text in muliple
		// places, use functions to display the warning.
		//
		IRRI_PutUpStationNotTurnedOnWarning_MLB( IN_LIST_FOR_SMS_SEQUENCE );

		// make an alert about this activity
		//
		sprintf( str_64, "Walk-Thru Skipped: (MAINLINE BREAK)" );
		Alert_Message( str_64 );

	} else if( IrriFlow.MVOR_InEffect_Closed == TRUE ) {
								   
		// 605.b.1 ajv - To avoid having the same text in muliple
		// places, use functions to display the warning.
		//
		IRRI_PutUpStationNotTurnedOnWarning_MVOR( IN_LIST_FOR_SMS_SEQUENCE );

		// make an alert about this activity
		//
		sprintf( str_64, "Walk-Thru Skipped: (MV CLOSED)" );
		Alert_Message( str_64 );

	}

	for ( i=0; i<MAX_STATIONS; i++ ) {
	
		// if the slot is not set to "--" and the station is in use (the in use test is to plug one
		// last hole if the user takes a station out of use after the sequence if created but before
		// it fires - a VERY unlikely occurance)
		//
		if ( (sms_seq.station[ i ] < MAX_STATIONS) && (BS_StationInUse[ sms_seq.station[ i ] ] == TRUE) ) {

		
			irrigation_seconds = sms_seq.runfor_seconds;
			
			//
			// This is the walk-thru sequence - there are FEW reason we will not perform the walk-thru.
			//
			if( IrriFlow.MainlineBreakInEffect == TRUE ) {
										   
				irrigation_seconds = 0;

			}


			if( IrriFlow.MVOR_InEffect_Closed == TRUE ) {
										   
				irrigation_seconds = 0;

			}
			

			
			if ( irrigation_seconds > 0 )  {
				
				log_it = TRUE;

				// can load stop time with Gl_DateTime because it won't be used for this irrigation reason
				IRRI_PutOnListOfThoseToAdd( sms_seq.station[ i ],		// station number 
											IN_LIST_FOR_SMS_SEQUENCE,	// reason in list
											
											irrigation_seconds,  		// irrigation seconds normal
											irrigation_seconds,			// !during MSEQ cycle=irrigation time
											
											0, 							// irrigation seconds ho
											0,							// cycle seconds ho
											
											FALSE,						// to set expected
											FALSE, 						// just to read sensor
											Gl_DateTime );				// when to turn off - not used during MPROG

			}
			

		}
	
	}
	

	if ( log_it == TRUE ) {
	
		Alert_ManualSequenceStart();
		
	}				

}


#endif


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

