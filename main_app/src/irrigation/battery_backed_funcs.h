/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_BATTERY_BACKED_FUNCS_H
#define _INC_BATTERY_BACKED_FUNCS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"stations.h"

#include	"weather_tables.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_battery_backed_weather_preserves( const BOOL_32 pforce_the_initialization );

// -----------------------

extern void init_battery_backed_station_preserves( const BOOL_32 pforce_the_initialization );


extern BOOL_32 STATION_PRESERVES_get_index_using_box_index_and_station_number( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, UNS_32 *pindex_ptr );

extern BOOL_32 STATION_PRESERVES_get_index_using_ptr_to_station_struct( STATION_STRUCT *const pss_ptr, UNS_32 *pindex_ptr );

// -----------------------

extern void init_rip_portion_of_system_record( SYSTEM_REPORT_RECORD *psdr );

extern void nm_SYSTEM_PRESERVES_restart_irrigation_and_block_flow_checking_for_150_seconds( BY_SYSTEM_RECORD *pbsr_ptr );

extern void nm_SYSTEM_PRESERVES_complete_record_initialization( BY_SYSTEM_RECORD *pbsr_ptr );

extern void init_battery_backed_system_preserves( const BOOL_32 pforce_the_initialization );

extern BY_SYSTEM_RECORD* SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( UNS_32 const psystem_gid );

extern void SYSTEM_PRESERVES_synchronize_preserves_to_file( void );


extern void SYSTEM_PRESERVES_request_a_resync( UNS_32 const psystem_gid );


extern void SYSTEM_PRESERVES_init_system_preserves_ufim_variables_for_all_systems( void );


extern void SYSTEM_PRESERVES_merge_new_allowed_ON_into_the_limit( BY_SYSTEM_RECORD *pbsr, UNS_16 const ponatatime_gid );

extern BOOL_32 SYSTEM_PRESERVES_is_this_valve_allowed_ON(  BY_SYSTEM_RECORD *pbsr, UNS_16 const ponatatime_gid );

extern BOOL_32 SYSTEM_PRESERVES_there_is_a_mlb( SYSTEM_MAINLINE_BREAK_RECORD *psmlbr_ptr );


// ----------

extern void init_battery_backed_poc_preserves( const BOOL_32 pforce_the_initialization );


extern BY_POC_RECORD* POC_PRESERVES_get_poc_preserve_for_this_poc_gid( UNS_32 const ppoc_gid, UNS_32 *pindex_ptr );

extern BY_POC_RECORD* POC_PRESERVES_get_poc_preserve_ptr_for_these_details( const UNS_32 pbox_index, const UNS_32 ppoc_file_type, const UNS_32 pdecoder_serial_num );

extern POC_DECODER_OR_TERMINAL_WORKING_STRUCT* POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters( UNS_32 const pbox_index, UNS_32 const pdecoder_ser_num, BY_POC_RECORD **pbpr_ptr, UNS_32 *this_decoders_level );


extern void POC_PRESERVES_synchronize_preserves_to_file( void );

extern void POC_PRESERVES_request_a_resync( void );

// ----------

#define		MASTER_VALVE_ACTION_OPEN		(333)
#define		MASTER_VALVE_ACTION_CLOSE		(444)
#define		MASTER_VALVE_ACTION_DEENERGIZE	(555)

extern void POC_PRESERVES_set_master_valve_energized_bit( POC_DECODER_OR_TERMINAL_WORKING_STRUCT *pws, UNS_32 const paction );

// ----------

extern void load_this_box_configuration_with_our_own_info( BOX_CONFIGURATION_STRUCT *pbox_ptr );

extern void init_battery_backed_chain_members( const BOOL_32 pforce_the_initialization );

extern BOOL_32 controller_is_2_wire_only( const UNS_32 pbox_index_0 );

// ----------

extern void init_battery_backed_general_use( const BOOL_32 pforce_the_initialization );

// ----------

extern void nm_LIGHTS_PRESERVES_complete_record_initialization( BY_LIGHTS_RECORD *pbpr_ptr );

extern void nm_LIGHTS_PRESERVES_restart_lights( BY_LIGHTS_RECORD *pblr_ptr );

extern void init_battery_backed_lights_preserves( const BOOL_32 pforce_the_initialization );

extern UNS_32 obsolete_LIGHTS_PRESERVES_get_preserves_index( const UNS_32 box_index, const UNS_32 output_index );

extern void LIGHTS_PRESERVES_set_bit_field_bit( const UNS_32 box_index, const UNS_32 output_index, const UNS_32 bit, const BOOL_32 set_bit );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

