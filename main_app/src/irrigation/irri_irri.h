/*  file = irri_irri.h              10/27/00 2011.08.01  rmd  */

/* ---------------------------------------------------------- */

#ifndef _INC_IRRI_IRRI_H
#define _INC_IRRI_IRRI_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"foal_comm.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef	_MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{ 
    union
	{
		UNS_32	overall_size;

		struct
		{
			// NOTE: In GCC this is the Least Significant Bit (Little Endian)

			// -------------------------------

			// 6/19/2012 rmd : This index is used as an integrity test for the stations journey from the
			// foal side to the irri side. Pretty much just watching the chain configuration is known
			// and agreed upon. This will take on the value of the serial numbers comm char
			// - 'A'. Which should also be it's owners index into our comm_mngr_shared_info array.
			// Range is 0 to (MAX_CHAIN_LENGTH-1). Must add 'A' to this to show the comm letter.
			//
			// 4/18/2014 rmd : Replaced with box_index_0 inmain structure.
			UNS_32		no_longer_used							: 4;

			// -------------------------------

			// The "w_" signifies terms used in the weighting evaluation. Maximum 16 different reasons.
			UNS_32		w_reason_in_list						: 4;
			
			// -------------------------------

			// Flag that the station is currently turned ON. If it is not ON and there is remaining time
			// then it is soaking.
			BITFIELD_BOOL		station_is_ON					: 1;

			// -----------------------------

			// NOTE: This is the MS bit in GCC.
		};

	};

} IRRI_IRRI_BIT_FIELD_STRUCT;

/* ---------------------------------------------------------- */

// The IRRI machines holds a list of irrigating stations in SDRAM. This list obviously is
// not battery backed and therefore does not survive a program restart (due to power failure
// or otherwise). It is intended first off as a way to turn off stations that seemingly are
// running past their time. The assumption there is something happened where they were never
// told by the master to turn off. Additionally this list can be used as a database of sorts
// to drive live reports.

// 4/26/2012 rmd : The list can grow to 768 (MAX_STATIONS in the network) items. And being
// dynamically allocated has a big impact on the memory pool system. We must have a block
// sized properly for this record.

typedef struct
{
	MIST_DLINK_TYPE				list_support_irri_all_irrigation;

	// 4/24/2012 rmd : This list is the re-do of what was the SXR list in the ET2000e. That list
	// was problematic as a station could be on the list to turn ON and at the same time be on
	// the list to say turn OFF. There was also the issue of the sxr record in the list pointed
	// to an ilc that perhaps wasn't in the irrigation list anymore. Instead of having a
	// separate sxr list, all in all it seemed more logical to create a new list using the ilc
	// elements themselves. This way a particular station (an ilc) can only be on the list ONCE
	// for ONE reason. And when we remove an ilc from the main list we will correspondingly
	// remove the ilc from this new list at the same time.
	MIST_DLINK_TYPE				list_support_irri_action_needed;

	// 4/25/2012 rmd : This is only relevant when this station is on the action_needed list.
	UNS_16						action_reason;

	// ---------------------------

	// 8/27/2012 rmd : The system_gid as passed via the token to the irri machine. This
	// identifies which SYSTEM this station belongs to.
	UNS_32						system_gid;

	// ---------------------------

	// The total amount left to irrigate. As cycle take place this amount is decreased by the
	// cycle amount. Must be able to hold the longest amount we would ever want to irrigate for.
	// Hence it is a 32-bit long.
	UNS_32						requested_irrigation_seconds_u32;

	UNS_32						box_index_0;
	
	UNS_8						station_number_0_u8;
	
	// ---------------------------

	// This can go negative. And a negative value is in fact our indication that the station has
	// run past its turn off time. We reach a threshold (maybe a minute or two) where we turn it
	// off and flag as an error as something must be wrong with the irrigation.
	INT_32						remaining_ON_or_soak_seconds;
	
	// 6/26/2012 rmd : The programmed and tail-ends massaged cycle time. Transmitted from the
	// foal side when the station is added to the list on this side.
	UNS_32						cycle_seconds;
	
	// ---------------------------

	// 2012.01.31 rmd : And here we have a bunch of vitals that are used during the system
	// operation AND are also desirable to send to each slave in the network. We've wrapped them
	// up into a bit field for tranfer efficiency.
	IRRI_IRRI_BIT_FIELD_STRUCT		ibf;
	
} IRRI_MAIN_LIST_OF_STATIONS_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
typedef struct
{
	// 2012.02.07 rmd : This is a list of the stations presently in the foal_irri irrigation
	// list. At least it is supposed to be by virtue of functioning communications. So it should
	// be a copy of the irrigation list in the foal machine. However it is not made up of ILC's.
	// We have defined an abbreviated ilc. Over here in the IRRI machine we do not perform all
	// the bells and whistles regarding deciding who goes ON next so we do not need a large
	// protion of the ilc definition. This is a list of IRRI_MAIN_LIST_OF_STATIONS_STRUCT. And
	// will provide each irri machine (box) in the chain with the specific information about
	// what is queued to irrigate, what is ON, etc., etc. What about storage of this list? Does
	// it need to be in battery backed SRAM??? No. And here's why. I am adopting the position
	// that after a power failure if we drop into forced that the ongoing irrigation is lost.
	// The master list is stored in battery backed SRAM. But the irri lists will not be. If the
	// chain goes down, when a controller falls into forced (takes several minutes) all stations
	// are turned OFF. Any irrigation that was taking place is to be cancelled. If a slave only
	// loses power it will come back up, rejoin the chain, AND if the chain is not NEW, will
	// inform the master that it needs the ongoing irrigation list.
	//
	// Another thing this list will do is to ensure that even if the chain is up that valves do
	// not become turned ON and then stay ON for ever. This list will be 'maintained' and have a
	// count down of remaining time. And if a station runs too far negative will preventively be
	// shutdown.
	MIST_LIST_HDR_TYPE		list_of_irri_all_irrigation;

} IRRI_IRRI;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


// 2012.02.07 rmd : irri_irri is NOT battery backed. The approach in the CS3000 is if the
// chain goes down we will NOT try to continue (pick up irrigation where we left off) for
// each box in the chain. Operating as independents. If there is a power fail when the cahin
// comes back up the master will tell the irri machine all that is happening irrigation
// wise. If we fall into forced or the chain is detected as 'different' then all the
// irrigation is cleared. Any on-going irrigation is lost.
extern IRRI_IRRI	irri_irri;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// usually when we manually kill a master we must allow his slaves to irrigate all of their
// remaining time - when the master is being removed due to a moisture reading exceeding the
// set point we need to set the slaves up to irrigate the same number of repeats as the master
//
#define KILL_MOIS_if_killing_a_master_set_slaves_to_go_ahead	4000

#define KILL_MOIS_slaves_continue_to_irrigate_up_to_master		5000


// Orphan control - there are various reasons we are killing a station in the list - stop key, stop time,
// just plain done, flow error, etc. For most of these reasons we want the irrigated last time variable
// to be overridden and set TRUE so that we do not generate orphans (stop key) - for some reasons we WANT
// the orphan indication to be preserved (stop time, shorts on the first cycle) - ALL DURING PROGRAMMED
// IRRIGATION ONLY!
//
#define KILL_ORPHAN_if_its_an_orphan_allow_it_to_remain_that_way	8000

#define KILL_ORPHAN_do_not_allow_it_to_be_an_orphan					9000


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_irri_irri( void );


extern void IRRI_restart_all_of_irrigation( void );


extern void IRRI_maintain_irrigation_list( void );


extern void IRRI_process_rcvd_action_needed_record( ACTION_NEEDED_XFER_RECORD const *const panxr );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

