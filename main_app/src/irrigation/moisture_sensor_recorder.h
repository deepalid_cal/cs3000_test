/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_MOISTURE_SENSOR_RECORDER_H
#define _INC_MOISTURE_SENSOR_RECORDER_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#ifndef _MSC_VER

	#include	"lpc_types.h"
	
	#include	"cal_td_utils.h"
	
	// 6/26/2014 ajv : Required to access timers.h
	#include	"cs_common.h"
	
#endif

#include	"report_data.h"



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/18/2016 rmd : To avoid CIRCULAR REFERENCES the MOISTURE_SENSOR_RECORDER_RECORD has been
// moved to the MOISTURE_SENSORS.H FILE.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/18/2016 rmd : Keep in mind sensor readings are coming in frequently. Like once every 10
// minutes. To manage communication overhead I would like to transfer these records to the
// server say once every 2 hours. Being that the records are held in SDRAM they are vurnable
// to loss at each power fail. And we will loss up to 2 hours worth of readings. This should
// be fine though as most sites are reliably powered.
//
// 3/29/2016 rmd : Send moisture readings once every 2 hours. See the definition of how many
// records are stored if you change this. The value of this define is detailed there.
//
// 9/23/2016 rmd : Update - here is the bottom line as of this time. We hold 342 readings in
// SDRAM waiting for transmission. If you had the maximum number of moisture sensors (24)
// and do as we are doing now making a record once every 20 minutes, we could hold 4.75
// hours worth of records. Therefore our transmission interval can safely be set to 2 or 3
// hours. Keep in mind 24 sensors is quite a boatload. Typically a site will have less than
// 10. So in reality we could probably live with a transmission time of once every 6 hours.
// We'll see how this goes. FOR NOW we are using a maximum of 24 sensors per network (site).
#define		CI_MOISTURE_SENSOR_RECORDER_TRANSMISSION_TIMER_MS		(2*60*60*1000)

typedef struct
{
	// ----------

	// 10/21/2013 rmd : The start of the allocated memory for the records.
	UNS_8		*original_allocation;

	// ----------

	// 10/21/2013 rmd : The memory location for the next line to be built.
	UNS_8		*next_available;

	// ----------

	// 10/23/2013 rmd : For display support only.
	UNS_8		*first_to_display;

	// ----------

	// 10/21/2013 rmd : The first line to send to the comm_server.
	UNS_8		*first_to_send;
	
	// 10/21/2013 rmd : During the communication exchange a copy of the PENDING first pointer to
	// send is kept. Until the reponse is rcvd. Only then do we update the first_to_send with
	// the pending_first_to_send. We take this approach for two reasons. First is until the
	// COMM_SERVER acknowledges us we do not want to move our pointer regarding lines sent. And
	// secondly during the potentially time consuming comm_server exchange the pile is live.
	// Records may be added to the pile. So we need a copy of where we got to when we sent the
	// records. When we receieve the ACK from the comm_server can't assume all records that
	// exist have been sent.
	UNS_8		*pending_first_to_send;

	// 10/21/2013 rmd : At times flow recording lines can be made quite frequently. To protect
	// against the flow recording timer firing again, after it just fired and made an outgoing
	// message, but before the communication transaction had completed we set this flag which
	// blocks the creation of another controller initiated message before any prior created
	// messages have either been sent. Or the response timer expired. BUT WAIT THERE'S MORE.
	// BECAUSE of the complication of having 4 flow recording arrays, one per system, there is a
	// problem knowing, within the controller initiated task, which flag to clear if we don't
	// receive a response. So we always clear all 4 then. And when a flow recording timer fires,
	// all 4 flags must be cleared in order to build a new flow recording transmission.
	BOOL_32		pending_first_to_send_in_use;

	// ----------
	
	// 10/21/2013 rmd : And the controller initiated timer. Being that flow recording is a
	// by system entity, one timer per system.
	xTimerHandle	when_to_send_timer;
	
	// ----------
	
} MOISTURE_SENSOR_RECORDER_CONTROL_STRUCT;


extern MOISTURE_SENSOR_RECORDER_CONTROL_STRUCT	msrcs;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void nm_MOISTURE_SENSOR_RECORDER_inc_pointer( UNS_8 **ppfrr, UNS_8 const (*const pstart_of_block) );

extern void MOISTURE_SENSOR_RECORDER_add_a_record( void *pmois );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

