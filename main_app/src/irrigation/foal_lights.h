
/*  file = foal_lights.h    				   7/28/2015 mpd  */

/* ---------------------------------------------------------- */

#ifndef _INC_FOAL_LIGHTS_H
#define _INC_FOAL_LIGHTS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"lpc_types.h"

#include	"foal_defs.h"

#include	"switch_input.h"

#include	"battery_backed_vars.h"

#include	"tpmicro_data.h"

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef	_MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights( void );

extern void init_battery_backed_foal_lights( const BOOL_32 pforce_the_initialization );

extern UNS_32 FOAL_LIGHTS_load_lights_on_list_into_outgoing_token( UNS_8 **pdata_ptr );

extern UNS_32 FOAL_LIGHTS_load_lights_action_needed_list_into_outgoing_token( UNS_8 **pdata_ptr );

extern void FOAL_LIGHTS_process_light_on_from_token_response( LIGHTS_ON_XFER_RECORD *lxr_ptr );


extern void FOAL_LIGHTS_turn_off_this_lights_output( UNS_32 pbox_index_0, UNS_32 poutput_index_0, UNS_32 paction_needed, UNS_32 palerts_reason );

extern void FOAL_LIGHTS_turn_off_all_lights_at_this_box( UNS_32 pcontroller_index, UNS_32 paction_needed, UNS_32 palerts_reason );


extern void FOAL_LIGHTS_process_light_off_from_token_response( LIGHTS_OFF_XFER_RECORD *lxr_ptr );

extern void TDCHECK_at_1Hz_rate_check_for_lights_schedule_start( const DATE_TIME_COMPLETE_STRUCT *const pdtcs_ptr );

extern void FOAL_LIGHTS_maintain_lights_list( const DATE_TIME *const pdate_time );

extern void FOAL_LIGHTS_initiate_a_mobile_light_ON( LIGHTS_ON_XFER_RECORD *tkos );

extern void FOAL_LIGHTS_initiate_a_mobile_light_OFF( LIGHTS_OFF_XFER_RECORD *lxr );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

