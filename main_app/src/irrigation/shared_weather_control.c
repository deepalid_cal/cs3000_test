/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_SHARED_WEATHER_CONTROL_C
#define _INC_SHARED_WEATHER_CONTROL_C

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"bithacks.h"

#include	"change.h"

#include	"et_data.h"

#include	"range_check.h"

#include	"shared.h"

#include	"weather_control.h"

#ifndef	_MSC_VER

	#include	"alerts.h"

	#include	"app_startup.h"
	
	#include	"battery_backed_vars.h"

	#include	"cs_mem.h"

#else

	#include	<stdlib.h>

	#include	"sql_merge.h"

#endif




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define WEATHER_CONTROL_CHANGE_BITFIELD_roll_time_bit					(0)

#define WEATHER_CONTROL_CHANGE_BITFIELD_rb_box_index_bit				(1)
#define WEATHER_CONTROL_CHANGE_BITFIELD_rb_in_use_bit					(2)
#define WEATHER_CONTROL_CHANGE_BITFIELD_rb_minimum_bit					(3)
#define WEATHER_CONTROL_CHANGE_BITFIELD_rb_max_hourly_bit				(4)
#define WEATHER_CONTROL_CHANGE_BITFIELD_rb_max_24_hour_bit				(5)

#define WEATHER_CONTROL_CHANGE_BITFIELD_etg_box_index_bit				(6)
#define WEATHER_CONTROL_CHANGE_BITFIELD_etg_in_use_bit					(7)
#define WEATHER_CONTROL_CHANGE_BITFIELD_etg_log_pulses_bit				(8)
#define WEATHER_CONTROL_CHANGE_BITFIELD_etg_historical_cap_bit			(9)
#define WEATHER_CONTROL_CHANGE_BITFIELD_etg_historical_min_bit			(10)

#define WEATHER_CONTROL_CHANGE_BITFIELD_wg_box_index_bit				(11)
#define WEATHER_CONTROL_CHANGE_BITFIELD_wg_in_use_bit					(12)

#define WEATHER_CONTROL_CHANGE_BITFIELD_wg_pause_speed_bit				(13)

// 4/14/2016 rmd : The user is presently not allowed to edit the pause and resume MINUTES.
// There is no reason to exchange these fields either among controllers or to the comm
// server. The set_defaults function set them both to 10 minutes when the file is created.
// So all controllers have this 10 minute value in their file. So I have taken these bits
// OUT OF SERVICE.
//#define nlu_WEATHER_CONTROL_CHANGE_BITFIELD_wg_pause_min_bit				(14)

#define WEATHER_CONTROL_CHANGE_BITFIELD_wg_resume_speed_bit				(15)

//#define nlu_WEATHER_CONTROL_CHANGE_BITFIELD_wg_resume_min_bit				(16)

#define WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_use_your_own_bit			(17)
#define WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_monthlies_bit			(18)
#define WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_state_idx_bit			(19)
#define WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_county_idx_bit			(20)
#define WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_city_idx_bit				(21)

#define WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_state_bit				(22)
#define WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_county_bit				(23)
#define WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_city_bit					(24)

#define WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_in_use_bit			(25)
#define WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_controllers_bit		(26)

#define WEATHER_CONTROL_CHANGE_BITFIELD_freeze_switch_in_use_bit		(27)
#define WEATHER_CONTROL_CHANGE_BITFIELD_freeze_switch_controllers_bit	(28)

// ----------

static char* const WEATHER_CONTROL_database_field_names[ (WEATHER_CONTROL_CHANGE_BITFIELD_freeze_switch_controllers_bit + 1) ] =
{
	"ET_RainTableRollTime",
	"RainBucketBoxIndex",
	"RainBucket_isInUse",
	"RainBucketMinInches",
	"RainBucketMaxHourly",
	"RainBucketMax24Hour",
	"ETGageBoxIndex",
	"ETG_isInUse",
	"ETG_LogPulses",
	"ETG_HistCapPercent",
	"ETG_HistMinPercent",
	"WindGageBoxIndex",
	"WindGage_isInUse",
	"WindPauseMPG",
	"", //"WindPauseMinutes",
	"WindResumeMPG",
	"", //"WindResumeMinutes",
	"UseYourOwnET",
	"ETUserMonth",	// ETUserMonth1 .. 12
	"ET_StateIndex",
	"ET_CountyIndex",
	"ET_CityIndex",
	"UserDefinedState",
	"UserDefinedCounty",
	"UserDefinedCity",
	"RainSwitch_IsInUse",
	"HasRainSwitch",	// HasRainSwitch1 .. 12
	"FreezeSwitch_isInUse",
	"HasFreezeSwitch"	// HasFreezeSwitch1 .. 12
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// The intent of this structure is to hold items that setup the controller on
// the user level. Not low-level obscure settings.
struct WEATHER_CONTROL_STRUCT
{
	// --------

	// 7/20/2012 rmd : Traditionally 8PM. However can be moved around as needed.
	UNS_32	et_and_rain_table_roll_time;

	// --------

	// The serial number of the controller where the Rain Bucket is connected. This controller
	// must have the WEATHER option.
	UNS_32	rain_bucket_box_index;

	BOOL_32	rain_bucket_is_there_a_rain_bucket;

	UNS_32	rain_bucket_minimum_inches_100u;

	UNS_32	rain_bucket_max_hourly_inches_100u;

	// 9/24/2012 rmd : The 'massaged' cap. Meaning the total after passing through the minimum
	// and the hourly caps.
	UNS_32	rain_bucket_max_24_hour_inches_100u;
	
	// --------

	// The serial number of the controller where the ET Gage is connected.
	// This controller must have the WEATHER option.
	UNS_32	etg_box_index;

	BOOL_32	etg_is_in_use;

	BOOL_32	etg_log_et_pulses;

	UNS_32	etg_percent_of_historical_cap_100u;

	UNS_32	etg_percent_of_historical_min_100u;
	
	// --------

	// The serial number of the controller where the Wind Gage is connected.
	// This controller must have the WEATHER option.
	UNS_32	wind_gage_box_index;

	BOOL_32	wind_is_there_a_wind_gage;


	WIND_SETTINGS_STRUCT	wss;
	
	// --------

	BOOL_32	reference_et_use_your_own;

	UNS_32	reference_et_user_numbers_100u[ MONTHS_IN_A_YEAR ];

	UNS_32	reference_et_state_index;

	UNS_32	reference_et_county_index;

	UNS_32	reference_et_city_index;

	char	user_defined_state[ MAX_REFERENCE_ET_NAME_LENGTH ];

	char	user_defined_county[ MAX_REFERENCE_ET_NAME_LENGTH ];

	char	user_defined_city[ MAX_REFERENCE_ET_NAME_LENGTH ];
	
	// --------

	BOOL_32	rain_switch_is_there_a_rain_switch;

	// Tracks which controller, or controllers, have a rain switch connected.
	// The first rain switch to trip stops irrigation, while the last one to dry
	// out resumes irrigation.
	BOOL_32	rain_switch_controllers_connected_to_switch[ MAX_CHAIN_LENGTH ];
	
	// --------

	BOOL_32	freeze_switch_is_there_a_freeze_switch;

	// Tracks which controller, or controllers, have a freeze switch connected.
	// The first freeze switch to trip stops irrigation, while the last one to
	// thaw out resumes irrigation.
	BOOL_32	freeze_switch_controllers_connected_to_switch[ MAX_CHAIN_LENGTH ];
	
	/* ---------------------------------------------------------- */
	/* CHANGE BITS */
	/* ---------------------------------------------------------- */

	CHANGE_BITS_32_BIT	changes_to_send_to_master;
	CHANGE_BITS_32_BIT	changes_to_distribute_to_slaves;
	CHANGE_BITS_32_BIT	changes_to_upload_to_comm_server;

	// -------------------------------------------- //

	// 4/30/2015 ajv : Added in Rev 3
	CHANGE_BITS_32_BIT	changes_uploaded_to_comm_server_awaiting_ACK;
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static WEATHER_CONTROL_STRUCT weather_control;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Sets the start of an ET day. If the parameter that's passed in is out-of-
 * range, an error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param proll_time 
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_roll_time( UNS_32 proll_time,
									  const BOOL_32 pgenerate_change_line,
									  const UNS_32 preason_for_change,
									  const UNS_32 pbox_index_0,
									  const BOOL_32 pset_change_bits,
									  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
									  #ifdef _MSC_VER
									  , char *pmerge_update_str,
									  char *pmerge_insert_fields_str,
									  char *pmerge_insert_values_str
									  #endif
									)
{
	SHARED_set_time_controller( &weather_control.et_and_rain_table_roll_time,
								proll_time,
								WEATHER_ET_RAIN_TABLE_ROLL_TIME_MINIMUM,
								WEATHER_ET_RAIN_TABLE_ROLL_TIME_MAXIMUM,
								WEATHER_ET_RAIN_TABLE_ROLL_TIME_DEFAULT,
								pgenerate_change_line,
								CHANGE_ET_START_OF_DAY,
								preason_for_change,
								pbox_index_0,
								pset_change_bits,
								pchange_bitfield_to_set,
								&weather_control.changes_to_upload_to_comm_server,
								WEATHER_CONTROL_CHANGE_BITFIELD_roll_time_bit,
								WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_roll_time_bit ]
								#ifdef _MSC_VER
								, pmerge_update_str,
								pmerge_insert_fields_str,
								pmerge_insert_values_str
								#endif
							  );
}

/* ---------------------------------------------------------- */
/**
 * Sets the controller serial number of the controller where the Rain Bucket is
 * physically attached. If the parameter that's passed in is out-of-range, an
 * error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param pbox_index The serial number to assign to the variable.
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_rain_bucket_box_index( UNS_32 pbox_index,
												  const BOOL_32 pgenerate_change_line,
												  const UNS_32 preason_for_change,
												  const UNS_32 pbox_index_of_initiator_0,
												  const BOOL_32 pset_change_bits,
												  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , char *pmerge_update_str,
												  char *pmerge_insert_fields_str,
												  char *pmerge_insert_values_str
												  #endif
												)
{
	SHARED_set_uint32_controller( &weather_control.rain_bucket_box_index,
								  pbox_index,
								  BOX_INDEX_MINIMUM,
								  BOX_INDEX_MAXIMUM,
								  BOX_INDEX_DEFAULT,
								  pgenerate_change_line,
								  CHANGE_RAIN_BUCKET_BOX_INDEX,
								  preason_for_change,
								  pbox_index_of_initiator_0,
								  pset_change_bits,
								  pchange_bitfield_to_set,
								  &weather_control.changes_to_upload_to_comm_server,
								  WEATHER_CONTROL_CHANGE_BITFIELD_rb_box_index_bit,
								  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rb_box_index_bit ]
								  #ifdef _MSC_VER
								  , pmerge_update_str,
								  pmerge_insert_fields_str,
								  pmerge_insert_values_str
								  #endif
								);
}

/* ---------------------------------------------------------- */
/**
 * Sets whether a rain bucket is connected or not. If the parameter that's
 * passed in is out-of-range, an error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param prain_bucket_is_there_a_rain_bucket
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_rain_bucket_connected( BOOL_32 prain_bucket_is_there_a_rain_bucket,
												  const BOOL_32 pgenerate_change_line,
												  const UNS_32 preason_for_change,
												  const UNS_32 pbox_index_0,
												  const BOOL_32 pset_change_bits,
												  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , char *pmerge_update_str,
												  char *pmerge_insert_fields_str,
												  char *pmerge_insert_values_str
												  #endif
											    )
{
	SHARED_set_bool_controller( &weather_control.rain_bucket_is_there_a_rain_bucket,
								prain_bucket_is_there_a_rain_bucket,
								WEATHER_RAIN_BUCKET_CONNECTED_DEFAULT,
								pgenerate_change_line,
								CHANGE_RAIN_BUCKET_CONNECTED,
								preason_for_change,
								pbox_index_0,
								pset_change_bits,
								pchange_bitfield_to_set,
								&weather_control.changes_to_upload_to_comm_server,
								WEATHER_CONTROL_CHANGE_BITFIELD_rb_in_use_bit,
								WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rb_in_use_bit ]
								#ifdef _MSC_VER
								, pmerge_update_str,
								pmerge_insert_fields_str,
								pmerge_insert_values_str
								#endif
							  );
}

/* ---------------------------------------------------------- */
/**
 * Sets the minimum inches of rainfall necessary to stop irrigation. If the
 * parameter is out-of-range, an error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param prain_bucket_minimum_inches_100u 
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_rain_bucket_minimum_inches( UNS_32 prain_bucket_minimum_inches_100u,
													   const BOOL_32 pgenerate_change_line,
													   const UNS_32 preason_for_change,
													   const UNS_32 pbox_index_0,
													   const BOOL_32 pset_change_bits,
													   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													   #ifdef _MSC_VER
													   , char *pmerge_update_str,
													   char *pmerge_insert_fields_str,
													   char *pmerge_insert_values_str
													   #endif
													 )
{
	SHARED_set_uint32_controller( &weather_control.rain_bucket_minimum_inches_100u,
								  prain_bucket_minimum_inches_100u,
								  WEATHER_RAIN_BUCKET_MINIMUM_MIN_100u,
								  WEATHER_RAIN_BUCKET_MINIMUM_MAX_100u,
								  WEATHER_RAIN_BUCKET_MINIMUM_DEFAULT_100u,
								  pgenerate_change_line,
								  CHANGE_RAIN_BUCKET_MINIMUM,
								  preason_for_change,
								  pbox_index_0,
								  pset_change_bits,
								  pchange_bitfield_to_set,
								  &weather_control.changes_to_upload_to_comm_server,
								  WEATHER_CONTROL_CHANGE_BITFIELD_rb_minimum_bit,
								  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rb_minimum_bit ]
								  #ifdef _MSC_VER
								  , pmerge_update_str,
								  pmerge_insert_fields_str,
								  pmerge_insert_values_str
								  #endif
								);
}

/* ---------------------------------------------------------- */
/**
 * Sets the maximum inches of rainfall allowed within an hour. If the parameter
 * is out-of-range, an error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param prain_bucket_max_hourly_inches_100u 
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_rain_bucket_max_hourly( UNS_32 prain_bucket_max_hourly_inches_100u,
												   const BOOL_32 pgenerate_change_line,
												   const UNS_32 preason_for_change,
												   const UNS_32 pbox_index_0,
												   const BOOL_32 pset_change_bits,
												   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												   #ifdef _MSC_VER
												   , char *pmerge_update_str,
												   char *pmerge_insert_fields_str,
												   char *pmerge_insert_values_str
												   #endif
											     )
{
	SHARED_set_uint32_controller( &weather_control.rain_bucket_max_hourly_inches_100u,
								  prain_bucket_max_hourly_inches_100u,
								  WEATHER_RAIN_BUCKET_MAXIMUM_HOURLY_MIN_100u,
								  WEATHER_RAIN_BUCKET_MAXIMUM_HOURLY_MAX_100u,
								  WEATHER_RAIN_BUCKET_MAXIMUM_HOURLY_DEFAULT_100u,
								  pgenerate_change_line,
								  CHANGE_RAIN_BUCKET_MAXIMUM_HOURLY,
								  preason_for_change,
								  pbox_index_0,
								  pset_change_bits,
								  pchange_bitfield_to_set,
								  &weather_control.changes_to_upload_to_comm_server,
								  WEATHER_CONTROL_CHANGE_BITFIELD_rb_max_hourly_bit,
								  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rb_max_hourly_bit ]
								  #ifdef _MSC_VER
								  , pmerge_update_str,
								  pmerge_insert_fields_str,
								  pmerge_insert_values_str
								  #endif
								);
}

/* ---------------------------------------------------------- */
/**
 * Sets the maximum inches of rainfall allowed within a 24-hour period. If the
 * parameter is out-of-range, an error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param prain_bucket_max_24_hour_inches_100u 
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_rain_bucket_max_24_hour( UNS_32 prain_bucket_max_24_hour_inches_100u,
													const BOOL_32 pgenerate_change_line,
													const UNS_32 preason_for_change,
													const UNS_32 pbox_index_0,
													const BOOL_32 pset_change_bits,
													CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													#ifdef _MSC_VER
													, char *pmerge_update_str,
													char *pmerge_insert_fields_str,
													char *pmerge_insert_values_str
													#endif
												  )
{
	SHARED_set_uint32_controller( &weather_control.rain_bucket_max_24_hour_inches_100u,
								  prain_bucket_max_24_hour_inches_100u,
								  WEATHER_RAIN_BUCKET_MAXIMUM_24_HOUR_MIN_100u,
								  WEATHER_RAIN_BUCKET_MAXIMUM_24_HOUR_MAX_100u,
								  WEATHER_RAIN_BUCKET_MAXIMUM_24_HOUR_DEFAULT_100u,
								  pgenerate_change_line,
								  CHANGE_RAIN_BUCKET_MAXIMUM_24_HOUR,
								  preason_for_change,
								  pbox_index_0,
								  pset_change_bits,
								  pchange_bitfield_to_set,
								  &weather_control.changes_to_upload_to_comm_server,
								  WEATHER_CONTROL_CHANGE_BITFIELD_rb_max_24_hour_bit,
								  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rb_max_24_hour_bit ]
								  #ifdef _MSC_VER
								  , pmerge_update_str,
								  pmerge_insert_fields_str,
								  pmerge_insert_values_str
								  #endif
								);
}

/* ---------------------------------------------------------- */
/**
 * Sets the controller serial number of the controller where the ET Gage is
 * physically attached. If the parameter that's passed in is out-of-range, an
 * error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param pbox_index_0 The serial number to assign to the variable.
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_et_gage_box_index( UNS_32 pbox_index_0,
											  const BOOL_32 pgenerate_change_line,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_of_initiator_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
										    )
{
	SHARED_set_uint32_controller( &weather_control.etg_box_index,
								  pbox_index_0,
								  BOX_INDEX_MINIMUM,
								  BOX_INDEX_MAXIMUM,
								  BOX_INDEX_DEFAULT,
								  pgenerate_change_line,
								  CHANGE_ET_GAGE_BOX_INDEX,
								  preason_for_change,
								  pbox_index_of_initiator_0,
								  pset_change_bits,
								  pchange_bitfield_to_set,
								  &weather_control.changes_to_upload_to_comm_server,
								  WEATHER_CONTROL_CHANGE_BITFIELD_etg_box_index_bit,
								  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_etg_box_index_bit ]
								  #ifdef _MSC_VER
								  , pmerge_update_str,
								  pmerge_insert_fields_str,
								  pmerge_insert_values_str
								  #endif
								);
}

/* ---------------------------------------------------------- */
/**
 * Sets whether an ET Gage is connected or not. If the parameter that's passed
 * in is out-of-range, an error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param petg_is_there_an_et_gage 
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_et_gage_connected( BOOL_32 petg_is_there_an_et_gage,
											  const BOOL_32 pgenerate_change_line,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
										    )
{
	SHARED_set_bool_controller( &weather_control.etg_is_in_use,
								petg_is_there_an_et_gage,
								WEATHER_ET_GAGE_CONNECTED_DEFAULT,
								pgenerate_change_line,
								CHANGE_ET_GAGE_CONNECTED,
								preason_for_change,
								pbox_index_0,
								pset_change_bits,
								pchange_bitfield_to_set,
								&weather_control.changes_to_upload_to_comm_server,
								WEATHER_CONTROL_CHANGE_BITFIELD_etg_in_use_bit,
								WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_etg_in_use_bit ]
								#ifdef _MSC_VER
								, pmerge_update_str,
								pmerge_insert_fields_str,
								pmerge_insert_values_str
								#endif
							   );
}

/* ---------------------------------------------------------- */
/**
 * Sets whether the controller should stamp an alert each time an ET Gage pulse
 * is detected or not. If the parameter that's passed in is out-of-range, an
 * error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param petg_log_et_pulses 
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_et_gage_log_pulses( BOOL_32 petg_log_et_pulses,
											   const BOOL_32 pgenerate_change_line,
											   const UNS_32 preason_for_change,
											   const UNS_32 pbox_index_0,
											   const BOOL_32 pset_change_bits,
											   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											   #ifdef _MSC_VER
											   , char *pmerge_update_str,
											   char *pmerge_insert_fields_str,
											   char *pmerge_insert_values_str
											   #endif
										     )
{
	SHARED_set_bool_controller( &weather_control.etg_log_et_pulses,
								petg_log_et_pulses,
								WEATHER_ET_LOG_PULSES_DEFAULT,
								pgenerate_change_line,
								CHANGE_ET_LOG_PULSES,
								preason_for_change,
								pbox_index_0,
								pset_change_bits,
								pchange_bitfield_to_set,
								&weather_control.changes_to_upload_to_comm_server,
								WEATHER_CONTROL_CHANGE_BITFIELD_etg_log_pulses_bit,
								WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_etg_log_pulses_bit ]
								#ifdef _MSC_VER
								, pmerge_update_str,
								pmerge_insert_fields_str,
								pmerge_insert_values_str
								#endif
							   );
}

/* ---------------------------------------------------------- */
/**
 * Sets the maximum percent of historical ET that can be used. If the parameter
 * that's passed in is out-of-range, an error is raised and the value isn't
 * updated.
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param petg_percent_of_historical_cap_100u 
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
extern void nm_WEATHER_set_percent_of_historical_et_cap( UNS_32 petg_percent_of_historical_cap_100u,
														 const BOOL_32 pgenerate_change_line,
														 const UNS_32 preason_for_change,
														 const UNS_32 pbox_index_0,
														 const BOOL_32 pset_change_bits,
														 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
														 #ifdef _MSC_VER
														 , char *pmerge_update_str,
														 char *pmerge_insert_fields_str,
														 char *pmerge_insert_values_str
														 #endif
													   )
{
	SHARED_set_uint32_controller( &weather_control.etg_percent_of_historical_cap_100u,
								  petg_percent_of_historical_cap_100u,
								  WEATHER_ET_PERCENT_OF_HISTORICAL_CAP_MIN_100u,
								  WEATHER_ET_PERCENT_OF_HISTORICAL_CAP_MAX_100u,
								  WEATHER_ET_PERCENT_OF_HISTORICAL_CAP_DEFAULT_100u,
								  pgenerate_change_line,
								  CHANGE_ET_PERCENT_OF_HISTORICAL_CAP,
								  preason_for_change,
								  pbox_index_0,
								  pset_change_bits,
								  pchange_bitfield_to_set,
								  &weather_control.changes_to_upload_to_comm_server,
								  WEATHER_CONTROL_CHANGE_BITFIELD_etg_historical_cap_bit,
								  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_etg_historical_cap_bit ]
								  #ifdef _MSC_VER
								  , pmerge_update_str,
								  pmerge_insert_fields_str,
								  pmerge_insert_values_str
								  #endif
								);
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param petg_percent_of_historical_min_100u 
 * @param pgenerate_change_line 
 * @param preason_for_change 
 * @param pbox_index_0 
 * @param pset_change_bits 
 *
 * @author 2/5/2013 Adrianusv
 *
 * @revisions (none)
 */
static void nm_WEATHER_set_percent_of_historical_et_min( UNS_32 petg_percent_of_historical_min_100u,
														 const BOOL_32 pgenerate_change_line,
														 const UNS_32 preason_for_change,
														 const UNS_32 pbox_index_0,
														 const BOOL_32 pset_change_bits,
														 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
														 #ifdef _MSC_VER
														 , char *pmerge_update_str,
														 char *pmerge_insert_fields_str,
														 char *pmerge_insert_values_str
														 #endif
													   )
{
	SHARED_set_uint32_controller( &weather_control.etg_percent_of_historical_min_100u,
								  petg_percent_of_historical_min_100u,
								  WEATHER_ET_PERCENT_OF_HISTORICAL_MIN_MIN_100u,
								  WEATHER_ET_PERCENT_OF_HISTORICAL_MIN_MAX_100u,
								  WEATHER_ET_PERCENT_OF_HISTORICAL_MIN_DEFAULT_100u,
								  pgenerate_change_line,
								  CHANGE_ET_PERCENT_OF_HISTORICAL_MIN,
								  preason_for_change,
								  pbox_index_0,
								  pset_change_bits,
								  pchange_bitfield_to_set,
								  &weather_control.changes_to_upload_to_comm_server,
								  WEATHER_CONTROL_CHANGE_BITFIELD_etg_historical_min_bit,
								  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_etg_historical_min_bit ]
								  #ifdef _MSC_VER
								  , pmerge_update_str,
								  pmerge_insert_fields_str,
								  pmerge_insert_values_str
								  #endif
								);
}

/* ---------------------------------------------------------- */
/**
 * Sets the controller serial number of the controller where the Wind Gage is
 * physically attached. If the parameter that's passed in is out-of-range, an
 * error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param pbox_index_0 The serial number to assign to the variable.
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_wind_gage_box_index( UNS_32 pbox_index_0,
												const BOOL_32 pgenerate_change_line,
												const UNS_32 preason_for_change,
												const UNS_32 pbox_index_of_initiator_0,
												const BOOL_32 pset_change_bits,
												CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												#ifdef _MSC_VER
												, char *pmerge_update_str,
												char *pmerge_insert_fields_str,
												char *pmerge_insert_values_str
												#endif
											  )
{
	SHARED_set_uint32_controller( &weather_control.wind_gage_box_index,
								  pbox_index_0,
								  BOX_INDEX_MINIMUM,
								  BOX_INDEX_MAXIMUM,
								  BOX_INDEX_DEFAULT,
								  pgenerate_change_line,
								  CHANGE_WIND_GAGE_BOX_INDEX,
								  preason_for_change,
								  pbox_index_of_initiator_0,
								  pset_change_bits,
								  pchange_bitfield_to_set,
								  &weather_control.changes_to_upload_to_comm_server,
								  WEATHER_CONTROL_CHANGE_BITFIELD_wg_box_index_bit,
								  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_wg_box_index_bit ]
								  #ifdef _MSC_VER
								  , pmerge_update_str,
								  pmerge_insert_fields_str,
								  pmerge_insert_values_str
								  #endif
								);
}

/* ---------------------------------------------------------- */
/**
 * Sets whether a wind gage is connected or not. If the parameter that's passed
 * in is out-of-range, an error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param pwind_is_there_a_wind_gage 
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_wind_gage_connected( BOOL_32 pwind_is_there_a_wind_gage,
												const BOOL_32 pgenerate_change_line,
												const UNS_32 preason_for_change,
												const UNS_32 pbox_index_0,
												const BOOL_32 pset_change_bits,
												CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												#ifdef _MSC_VER
												, char *pmerge_update_str,
												char *pmerge_insert_fields_str,
												char *pmerge_insert_values_str
												#endif
											  )
{
	SHARED_set_bool_controller( &weather_control.wind_is_there_a_wind_gage,
								pwind_is_there_a_wind_gage,
								WEATHER_WIND_GAGE_CONNECTED_DEFAULT,
								pgenerate_change_line,
								CHANGE_WIND_GAGE_CONNECTED,
								preason_for_change,
								pbox_index_0,
								pset_change_bits,
								pchange_bitfield_to_set,
								&weather_control.changes_to_upload_to_comm_server,
								WEATHER_CONTROL_CHANGE_BITFIELD_wg_in_use_bit,
								WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_wg_in_use_bit ]
								#ifdef _MSC_VER
								, pmerge_update_str,
								pmerge_insert_fields_str,
								pmerge_insert_values_str
								#endif
							   );
}

/* ---------------------------------------------------------- */
/**
 * Sets the sustained speed, in miles per hour, that a wind gage must read
 * before irrigation is paused. If the parameter that's passed in is
 * out-of-range, an error is raised and the value isn't updated.
 * 
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the STARTUP task on an initial
 * boot up; within the context of the KEY PROCESSING task when the user leaves
 * the screen; and within the context of the COMM_MNGR task when a change is
 * made from the central or handheld radio remote.
 * 
 * @param pwind_pause_speed_mph The pause speed to assign to the wind gage.
 * 
 * @param pgenerate_change_line True if this controller is allowed to generate a
 * change line; otherwise, false.
 * 
 * @param preason_for_change The reason the change occurred. Possible reason are
 * defined in change.h.
 * 
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 * change.
 * 
 * @param pset_change_bits True if the change bits should be set; otherwise,
 * false. Change bits should NOT be set if the change came through
 * communications or if the controller's flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *  7/3/2013 ajv : Added syncing of weather settings with the TP Micro if a
 *  change was detected.
 */
static void WEATHER_set_wind_pause_speed( UNS_32 pwind_pause_speed_mph,
										  const BOOL_32 pgenerate_change_line,
										  const UNS_32 preason_for_change,
										  const UNS_32 pbox_index_0,
										  const BOOL_32 pset_change_bits,
										  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										  #ifdef _MSC_VER
										  , char *pmerge_update_str,
										  char *pmerge_insert_fields_str,
										  char *pmerge_insert_values_str
										  #endif
										)
{
	if( SHARED_set_uint32_controller( &weather_control.wss.wind_pause_mph,
									  pwind_pause_speed_mph,
									  WEATHER_WIND_PAUSE_SPEED_MIN,
									  WEATHER_WIND_PAUSE_SPEED_MAX,
									  WEATHER_WIND_PAUSE_SPEED_DEFAULT,
									  pgenerate_change_line,
									  CHANGE_WIND_PAUSE_SPEED,
									  preason_for_change,
									  pbox_index_0,
									  pset_change_bits,
									  pchange_bitfield_to_set,
									  &weather_control.changes_to_upload_to_comm_server,
									  WEATHER_CONTROL_CHANGE_BITFIELD_wg_pause_speed_bit,
									  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_wg_pause_speed_bit ]
									  #ifdef _MSC_VER
									  , pmerge_update_str,
									  pmerge_insert_fields_str,
									  pmerge_insert_values_str
									  #endif
									) == (true) )
	{
		#ifndef	_MSC_VER

		// 7/3/2013 ajv : If a change was detected, notify the COMM MNGR to send
		// the updated wind structure to the TP Micro since it handles the
		// actual processing of wind.
		TP_MICRO_COMM_resync_wind_settings();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * Sets the sustained speed, in miles per hour, that a wind gage must read
 * before irrigation is resumed from a paused state. If the parameter that's
 * passed in is out-of-range, an error is raised and the value isn't updated.
 * 
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the STARTUP task on an initial
 * boot up; within the context of the KEY PROCESSING task when the user leaves
 * the screen; and within the context of the COMM_MNGR task when a change is
 * made from the central or handheld radio remote.
 * 
 * @param pwind_resume_speed_mph The resume speed to assign to the wind gage.
 * 
 * @param pgenerate_change_line True if this controller is allowed to generate a
 * change line; otherwise, false.
 * 
 * @param preason_for_change The reason the change occurred. Possible reason are
 * defined in change.h.
 * 
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 * change.
 * 
 * @param pset_change_bits True if the change bits should be set; otherwise,
 * false. Change bits should NOT be set if the change came through
 * communications or if the controller's flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *  7/3/2013 ajv : Added syncing of weather settings with the TP Micro if a
 *  change was detected.
 */
static void WEATHER_set_wind_resume_speed( UNS_32 pwind_resume_speed_mph,
										   const BOOL_32 pgenerate_change_line,
										   const UNS_32 preason_for_change,
										   const UNS_32 pbox_index_0,
										   const BOOL_32 pset_change_bits,
										   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , char *pmerge_update_str,
										   char *pmerge_insert_fields_str,
										   char *pmerge_insert_values_str
										   #endif
										 )
{
	if( SHARED_set_uint32_controller( &weather_control.wss.wind_resume_mph,
									  pwind_resume_speed_mph,
									  WEATHER_WIND_RESUME_SPEED_MIN,
									  WEATHER_WIND_RESUME_SPEED_MAX,
									  WEATHER_WIND_RESUME_SPEED_DEFAULT,
									  pgenerate_change_line,
									  CHANGE_WIND_RESUME_SPEED,
									  preason_for_change,
									  pbox_index_0,
									  pset_change_bits,
									  pchange_bitfield_to_set,
									  &weather_control.changes_to_upload_to_comm_server,
									  WEATHER_CONTROL_CHANGE_BITFIELD_wg_resume_speed_bit,
									  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_wg_resume_speed_bit ]
									  #ifdef _MSC_VER
									  , pmerge_update_str,
									  pmerge_insert_fields_str,
									  pmerge_insert_values_str
									  #endif
									) == (true) )
	{
		#ifndef	_MSC_VER

		// 7/3/2013 ajv : If a change was detected, notify the COMM MNGR to send
		// the updated wind structure to the TP Micro since it handles the
		// actual processing of wind.
		TP_MICRO_COMM_resync_wind_settings();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * Sets whether to use user-entered reference ET values or whether to use the
 * values associated with a particular state/county/city. If the passed in
 * parameter is out-of-range, an error is raised and the value isn't updated.
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param preference_et_use_your_own 
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_reference_et_use_your_own( BOOL_32 preference_et_use_your_own,
													  const BOOL_32 pgenerate_change_line,
													  const UNS_32 preason_for_change,
													  const UNS_32 pbox_index_0,
													  const BOOL_32 pset_change_bits,
													  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													  #ifdef _MSC_VER
													  , char *pmerge_update_str,
													  char *pmerge_insert_fields_str,
													  char *pmerge_insert_values_str
													  #endif
												    )
{
	// Since we have to update the ET table if any of these values changed,
	// check the return value of the set routine to verify that the value
	// was, in fact, changed.
	if( SHARED_set_bool_controller( &weather_control.reference_et_use_your_own,
									preference_et_use_your_own,
									WEATHER_REFERENCE_ET_USE_YOUR_OWN_DEFAULT,
									pgenerate_change_line,
									CHANGE_REFERENCE_ET_USE_YOUR_OWN,
									preason_for_change,
									pbox_index_0,
									pset_change_bits,
									pchange_bitfield_to_set,
									&weather_control.changes_to_upload_to_comm_server,
									WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_use_your_own_bit,
									WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_use_your_own_bit ]
									#ifdef _MSC_VER
									, pmerge_update_str,
									pmerge_insert_fields_str,
									pmerge_insert_values_str
									#endif
								   ) == (true) )
	{
		#ifndef	_MSC_VER

		// 7/26/2012 rmd : Cause the table to roll. Filling the table with the new reference ET
		// value.
		WEATHER_TABLES_cause_et_table_historical_values_to_be_updated();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * Sets the user-entered reference ET value for the specified month. If the
 * passed in parameter is out-of-range, an error is raised and the value isn't
 * updated.
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param preference_et_user_number_100u 
 * @param pmonth1_12 The month to assign the new ET value to.
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_reference_et_number( UNS_32 preference_et_user_number_100u,
												const UNS_32 pmonth1_12,
												const BOOL_32 pgenerate_change_line,
												const UNS_32 preason_for_change,
												const UNS_32 pbox_index_0,
												const BOOL_32 pset_change_bits,
												CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												#ifdef _MSC_VER
												, char *pmerge_update_str,
												char *pmerge_insert_fields_str,
												char *pmerge_insert_values_str
												#endif
											  )
{
	UNS_32	ldefault_value;

	char	lfield_name[ 32 ];

	if( (pmonth1_12 >= 1) && (pmonth1_12 <= MONTHS_IN_A_YEAR) )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_monthlies_bit ], pmonth1_12 );

		ldefault_value = ET_PredefinedData[ WEATHER_REFERENCE_ET_DEFAULT_CITY_INDEX ].ET_100u[ (pmonth1_12-1) ];

		// Since we have to update the ET table if any of these values changed,
		// check the return value of the set routine to verify that the value
		// was, in fact, changed.
		if (SHARED_set_uint32_controller( &weather_control.reference_et_user_numbers_100u[ (pmonth1_12-1) ],
										  preference_et_user_number_100u,
										  WEATHER_REFERENCE_ET_MIN_100u,
										  WEATHER_REFERENCE_ET_MAX_100u,
										  ldefault_value,
										  pgenerate_change_line,
										  (CHANGE_REFERENCE_ET_NUMBER_1 + (pmonth1_12-1)),
										  preason_for_change,
										  pbox_index_0,
										  pset_change_bits,
										  pchange_bitfield_to_set,
										  &weather_control.changes_to_upload_to_comm_server,
										  WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_monthlies_bit,
										  lfield_name
										  #ifdef _MSC_VER
										  , pmerge_update_str,
										  pmerge_insert_fields_str,
										  pmerge_insert_values_str
										  #endif
										) == (true) )
		{
			#ifndef	_MSC_VER

				// 7/26/2012 rmd : Cause the table to roll. Filling the table with the new reference ET
				// value.
				WEATHER_TABLES_cause_et_table_historical_values_to_be_updated();

			#endif
		}
	}
	else
	{
		#ifndef	_MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param preference_et_state_index 
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_reference_et_state_index( UNS_32 preference_et_state_index,
													 const BOOL_32 pgenerate_change_line,
													 const UNS_32 preason_for_change,
													 const UNS_32 pbox_index_0,
													 const BOOL_32 pset_change_bits,
													 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													 #ifdef _MSC_VER
													 , char *pmerge_update_str,
													 char *pmerge_insert_fields_str,
													 char *pmerge_insert_values_str
													 #endif
												   )
{
	// Since we have to update the ET table if any of these values changed,
	// check the return value of the set routine to verify that the value
	// was, in fact, changed.
	if (SHARED_set_uint32_controller( &weather_control.reference_et_state_index,
									  preference_et_state_index,
									  WEATHER_REFERENCE_ET_STATE_MIN,
									  WEATHER_REFERENCE_ET_STATE_MAX,
									  WEATHER_REFERENCE_ET_STATE_DEFAULT,
									  pgenerate_change_line,
									  CHANGE_REFERENCE_ET_STATE_INDEX,
									  preason_for_change,
									  pbox_index_0,
									  pset_change_bits,
									  pchange_bitfield_to_set,
									  &weather_control.changes_to_upload_to_comm_server,
									  WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_state_idx_bit,
									  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_state_idx_bit ]
									  #ifdef _MSC_VER
									  , pmerge_update_str,
									  pmerge_insert_fields_str,
									  pmerge_insert_values_str
									  #endif
									) == (true) )

	{
		#ifndef	_MSC_VER

		// 7/26/2012 rmd : Cause the table to roll. Filling the table with the
		// new reference ET value.
		WEATHER_TABLES_cause_et_table_historical_values_to_be_updated();
		
		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param preference_et_county_index 
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_reference_et_county_index( UNS_32 preference_et_county_index,
													  const BOOL_32 pgenerate_change_line,
													  const UNS_32 preason_for_change,
													  const UNS_32 pbox_index_0,
													  const BOOL_32 pset_change_bits,
													  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													  #ifdef _MSC_VER
													  , char *pmerge_update_str,
													  char *pmerge_insert_fields_str,
													  char *pmerge_insert_values_str
													  #endif
												    )
{
	// Since we have to update the ET table if any of these values changed,
	// check the return value of the set routine to verify that the value
	// was, in fact, changed.
	if( SHARED_set_uint32_controller( &weather_control.reference_et_county_index,
									  preference_et_county_index,
									  WEATHER_REFERENCE_ET_COUNTY_MIN,
									  WEATHER_REFERENCE_ET_COUNTY_MAX,
									  WEATHER_REFERENCE_ET_COUNTY_DEFAULT,
									  pgenerate_change_line,
									  CHANGE_REFERENCE_ET_COUNTY_INDEX,
									  preason_for_change,
									  pbox_index_0,
									  pset_change_bits,
									  pchange_bitfield_to_set,
									  &weather_control.changes_to_upload_to_comm_server,
									  WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_county_idx_bit,
									  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_county_idx_bit ]
									  #ifdef _MSC_VER
									  , pmerge_update_str,
									  pmerge_insert_fields_str,
									  pmerge_insert_values_str
									  #endif
									) == (true) )
	{
		#ifndef	_MSC_VER

		// 7/26/2012 rmd : Cause the table to roll. Filling the table with the
		// new reference ET value.
		WEATHER_TABLES_cause_et_table_historical_values_to_be_updated();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param preference_et_city_index 
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_reference_et_city_index( UNS_32 preference_et_city_index,
													const BOOL_32 pgenerate_change_line,
													const UNS_32 preason_for_change,
													const UNS_32 pbox_index_0,
													const BOOL_32 pset_change_bits,
													CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													#ifdef _MSC_VER
													, char *pmerge_update_str,
													char *pmerge_insert_fields_str,
													char *pmerge_insert_values_str
													#endif
												  )
{
	// Since we have to update the ET table if any of these values changed,
	// check the return value of the set routine to verify that the value
	// was, in fact, changed.
	if( SHARED_set_uint32_controller( &weather_control.reference_et_city_index,
									  preference_et_city_index,
									  WEATHER_REFERENCE_ET_CITY_MIN,
									  WEATHER_REFERENCE_ET_CITY_MAX,
									  WEATHER_REFERENCE_ET_CITY_DEFAULT,
									  pgenerate_change_line,
									  CHANGE_REFERENCE_ET_CITY_INDEX,
									  preason_for_change,
									  pbox_index_0,
									  pset_change_bits,
									  pchange_bitfield_to_set,
									  &weather_control.changes_to_upload_to_comm_server,
									  WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_city_idx_bit,
									  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_city_idx_bit ]
									  #ifdef _MSC_VER
									  , pmerge_update_str,
									  pmerge_insert_fields_str,
									  pmerge_insert_values_str
									  #endif
									) == (true) )
	{
		#ifndef	_MSC_VER

		// 7/26/2012 rmd : Cause the table to roll. Filling the table with thenew reference ET
		// value.
		WEATHER_TABLES_cause_et_table_historical_values_to_be_updated();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param puser_defined_state 
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_user_defined_state( const char *puser_defined_state,
											   const BOOL_32 pgenerate_change_line,
											   const UNS_32 preason_for_change,
											   const UNS_32 pbox_index_0,
											   const BOOL_32 pset_change_bits,
											   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											   #ifdef _MSC_VER
											   , char *pmerge_update_str,
											   char *pmerge_insert_fields_str,
											   char *pmerge_insert_values_str
											   #endif
										     )
{
	SHARED_set_string_controller( weather_control.user_defined_state,
								  MAX_REFERENCE_ET_NAME_LENGTH,
								  puser_defined_state,
								  pgenerate_change_line,
								  CHANGE_REFERENCE_ET_USER_DEFINED_STATE,
								  preason_for_change,
								  pbox_index_0,
								  pset_change_bits,
								  pchange_bitfield_to_set,
								  &weather_control.changes_to_upload_to_comm_server,
								  WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_state_bit,
								  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_state_bit ]
								  #ifdef _MSC_VER
								  , pmerge_update_str,
								  pmerge_insert_fields_str,
								  pmerge_insert_values_str
								  #endif
								);
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param puser_defined_county 
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_user_defined_county( const char *puser_defined_county,
												const BOOL_32 pgenerate_change_line,
												const UNS_32 preason_for_change,
												const UNS_32 pbox_index_0,
												const BOOL_32 pset_change_bits,
												CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												#ifdef _MSC_VER
												, char *pmerge_update_str,
												char *pmerge_insert_fields_str,
												char *pmerge_insert_values_str
												#endif
											  )
{
	SHARED_set_string_controller( weather_control.user_defined_county,
								  MAX_REFERENCE_ET_NAME_LENGTH,
								  puser_defined_county,
								  pgenerate_change_line,
								  CHANGE_REFERENCE_ET_USER_DEFINED_COUNTY,
								  preason_for_change,
								  pbox_index_0,
								  pset_change_bits,
								  pchange_bitfield_to_set,
								  &weather_control.changes_to_upload_to_comm_server,
								  WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_county_bit,
								  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_county_bit ]
								  #ifdef _MSC_VER
								  , pmerge_update_str,
								  pmerge_insert_fields_str,
								  pmerge_insert_values_str
								  #endif
								);
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex Calling function must have the weather_control_recursive_MUTEX mutex.
 *
 * @executed Executed within the context of the Startup task on an initial boot
 *  	     up; within the context of the key processing task when the user
 *  		 leaves the screen; and within the context of the CommMngr task when
 *           a change is made from the central or handheld radio remote.
 * 
 * @param puser_defined_city 
 * @param pgenerate_change_line True if this controller is allowed to
 *  								 generate a change line; otherwise, false.
 * @param preason_for_change The reason the change occurred. Possible reason are
 *                           defined in change.h.
 * @param pbox_index_0 The 0-based controller index of whoever initiated the
 *  								  change.
 * @param pset_change_bits True if the change bits should be set; otherwise,
 *  					   false. Change bits should NOT be set if the change
 *  					   came through communications or if the controller's
 *  					   flash is initialized.
 *
 * @author 7/10/2012 Adrianusv
 *
 * @revisions
 *    7/10/2012 Initial release
 */
static void nm_WEATHER_set_user_defined_city( const char *puser_defined_city,
											  const BOOL_32 pgenerate_change_line,
											  const UNS_32 preason_for_change,
											  const UNS_32 pbox_index_0,
											  const BOOL_32 pset_change_bits,
											  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , char *pmerge_update_str,
											  char *pmerge_insert_fields_str,
											  char *pmerge_insert_values_str
											  #endif
										    )
{
	SHARED_set_string_controller( weather_control.user_defined_city,
								  MAX_REFERENCE_ET_NAME_LENGTH,
								  puser_defined_city,
								  pgenerate_change_line,
								  CHANGE_REFERENCE_ET_USER_DEFINED_CITY,
								  preason_for_change,
								  pbox_index_0,
								  pset_change_bits,
								  pchange_bitfield_to_set,
								  &weather_control.changes_to_upload_to_comm_server,
								  WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_city_bit,
								  WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_city_bit ]
								  #ifdef _MSC_VER
								  , pmerge_update_str,
								  pmerge_insert_fields_str,
								  pmerge_insert_values_str
								  #endif
								);
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param pin_use 
 * @param pdefault_value 
 * @param pgenerate_change_line 
 * @param preason_for_change 
 * @param pbox_index_0 
 * @param pset_change_bits 
 *
 * @author 8/27/2012 Adrianusv
 *
 * @revisions (none)
 */
static void nm_WEATHER_set_rain_switch_in_use( BOOL_32 pin_use,
											   const BOOL_32 pgenerate_change_line,
											   const UNS_32 preason_for_change,
											   const UNS_32 pbox_index_0,
											   const BOOL_32 pset_change_bits,
											   CHANGE_BITS_32_BIT *pchange_bitfield_to_set
											   #ifdef _MSC_VER
											   , char *pmerge_update_str,
											   char *pmerge_insert_fields_str,
											   char *pmerge_insert_values_str
											   #endif
										     )
{
	SHARED_set_bool_controller( &weather_control.rain_switch_is_there_a_rain_switch,
								pin_use,
								WEATHER_RAIN_SWITCH_IN_USE_DEFAULT,
								pgenerate_change_line,
								CHANGE_RAIN_SWITCH_CONNECTED,
								preason_for_change,
								pbox_index_0,
								pset_change_bits,
								pchange_bitfield_to_set,
								&weather_control.changes_to_upload_to_comm_server,
								WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_in_use_bit,
								WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_in_use_bit ]
								#ifdef _MSC_VER
								, pmerge_update_str,
								pmerge_insert_fields_str,
								pmerge_insert_values_str
								#endif
							  );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param pconnected 
 * @param pcontroller_index_0 
 * @param pdefault_value 
 * @param pgenerate_change_line 
 * @param preason_for_change 
 * @param pbox_index_0 
 * @param pset_change_bits 
 *
 * @author 8/27/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void nm_WEATHER_set_rain_switch_connected( BOOL_32 pconnected,
												  const UNS_32 pcontroller_index_0,
												  const BOOL_32 pgenerate_change_line,
												  const UNS_32 preason_for_change,
												  const UNS_32 pbox_index_0,
												  const BOOL_32 pset_change_bits,
												  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , char *pmerge_update_str,
												  char *pmerge_insert_fields_str,
												  char *pmerge_insert_values_str
												  #endif
												)
{
	char	lfield_name[ 32 ];

	if( pcontroller_index_0 < MAX_CHAIN_LENGTH )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_controllers_bit ], (pcontroller_index_0 + 1) );

		SHARED_set_bool_controller( &weather_control.rain_switch_controllers_connected_to_switch[ pcontroller_index_0 ],
									pconnected,
									WEATHER_RAIN_SWITCH_CONNECTED_DEFAULT,
									pgenerate_change_line,
									CHANGE_RAIN_SWITCH_CONNECTED,
									preason_for_change,
									pbox_index_0,
									pset_change_bits,
									pchange_bitfield_to_set,
									&weather_control.changes_to_upload_to_comm_server,
									WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_controllers_bit,
									lfield_name
									#ifdef _MSC_VER
									, pmerge_update_str,
									pmerge_insert_fields_str,
									pmerge_insert_values_str
									#endif
								  );
	}
	else
	{
		#ifndef _MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param pin_use 
 * @param pdefault_value 
 * @param pgenerate_change_line 
 * @param preason_for_change 
 * @param pbox_index_0 
 * @param pset_change_bits 
 *
 * @author 8/27/2012 Adrianusv
 *
 * @revisions (none)
 */
static void nm_WEATHER_set_freeze_switch_in_use( BOOL_32 pin_use,
												 const BOOL_32 pgenerate_change_line,
												 const UNS_32 preason_for_change,
												 const UNS_32 pbox_index_0,
												 const BOOL_32 pset_change_bits,
												 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												 #ifdef _MSC_VER
												 , char *pmerge_update_str,
												 char *pmerge_insert_fields_str,
												 char *pmerge_insert_values_str
												 #endif
											   )
{
	SHARED_set_bool_controller( &weather_control.freeze_switch_is_there_a_freeze_switch,
								pin_use,
								WEATHER_FREEZE_SWITCH_IN_USE_DEFAULT,
								pgenerate_change_line,
								CHANGE_FREEZE_SWITCH_CONNECTED,
								preason_for_change,
								pbox_index_0,
								pset_change_bits,
								pchange_bitfield_to_set,
								&weather_control.changes_to_upload_to_comm_server,
								WEATHER_CONTROL_CHANGE_BITFIELD_freeze_switch_in_use_bit,
								WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_freeze_switch_in_use_bit ]
								#ifdef _MSC_VER
								, pmerge_update_str,
								pmerge_insert_fields_str,
								pmerge_insert_values_str
								#endif
							  );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param pconnected 
 * @param pcontroller_index_0 
 * @param pdefault_value 
 * @param pgenerate_change_line 
 * @param preason_for_change 
 * @param pbox_index_0 
 * @param pset_change_bits 
 *
 * @author 8/27/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void nm_WEATHER_set_freeze_switch_connected( BOOL_32 pconnected,
													const UNS_32 pcontroller_index_0,
													const BOOL_32 pgenerate_change_line,
													const UNS_32 preason_for_change,
													const UNS_32 pbox_index_0,
													const BOOL_32 pset_change_bits,
													CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													#ifdef _MSC_VER
													, char *pmerge_update_str,
													char *pmerge_insert_fields_str,
													char *pmerge_insert_values_str
													#endif
												  )
{
	char	lfield_name[ 32 ];

	if( pcontroller_index_0 < MAX_CHAIN_LENGTH )
	{
		snprintf( lfield_name, sizeof(lfield_name), "%s%d", WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_freeze_switch_controllers_bit ], (pcontroller_index_0 + 1) );

		SHARED_set_bool_controller( &weather_control.freeze_switch_controllers_connected_to_switch[ pcontroller_index_0 ],
									pconnected,
									WEATHER_FREEZE_SWITCH_CONNECTED_DEFAULT,
									pgenerate_change_line,
									CHANGE_FREEZE_SWITCH_CONNECTED,
									preason_for_change,
									pbox_index_0,
									pset_change_bits,
									pchange_bitfield_to_set,
									&weather_control.changes_to_upload_to_comm_server,
									WEATHER_CONTROL_CHANGE_BITFIELD_freeze_switch_controllers_bit,
									lfield_name
									#ifdef _MSC_VER
									, pmerge_update_str,
									pmerge_insert_fields_str,
									pmerge_insert_values_str
									#endif
								  );
	}
	else
	{
		#ifndef _MSC_VER

			Alert_index_out_of_range();

		#endif
	}
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param pucp 
 * @param preason_for_change 
 * 
 * @return UNS_32 
 *
 * @author 7/12/2012 Adrianusv
 *
 * @revisions (none)
 */
extern UNS_32 nm_WEATHER_extract_and_store_changes_from_comm( const unsigned char *pucp,
															  const UNS_32 preason_for_change,
															  const BOOL_32 pset_change_bits,
															  const UNS_32 pchanges_received_from
															  #ifdef _MSC_VER
															  , char *pSQL_statements,
															  char *pSQL_search_condition_str,
															  char *pSQL_update_str,
															  char *pSQL_insert_fields_str,
															  char *pSQL_insert_values_str,
															  char *pSQL_single_merge_statement_buf,
															  const UNS_32 pnetwork_ID
															  #endif
															)
{
	CHANGE_BITS_32_BIT	*lchange_bitfield_to_set;

	CHANGE_BITS_32_BIT	lbitfield_of_changes;

	UNS_32	lsize_of_bitfield;

	UNS_32	ltemporary_uns_32;

	BOOL_32	ltemporary_bool_32;

	char	ltemporary_str_48[ MAX_REFERENCE_ET_NAME_LENGTH ];

	UNS_32	i;

	UNS_32	rv;

	// ----------

	rv = 0;

	// ----------

	#ifndef _MSC_VER

		/*
		// 8/8/2016 rmd : For debug only.
		if( pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE )
		{
			Alert_Message( "rcvd from slave: WEATHER changes" );
		}
		else
		if( pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER )
		{
			Alert_Message( "rcvd from master: WEATHER changes" );
		}
		else
		{
			Alert_Message( "rcvd from UNK: WEATHER changes" );
		}
		*/
		
		lchange_bitfield_to_set = WEATHER_get_change_bits_ptr( pchanges_received_from );

	#else

		// 7/28/2015 ajv : Initialize to NULL since this variable is used by the CommServer but
		// expected to be NULL.
		lchange_bitfield_to_set = NULL;

		// ----------

		// 1/16/2015 ajv : Make sure the strings are fully initialized to NULL since we rely on this
		// to determine whether the string is empty or not.
		memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

		// 1/19/2015 ajv : Insert the network ID in case no weather information has been added to
		// the database yet.
		SQL_MERGE_build_upsert_uint32( pnetwork_ID, SQL_MERGE_include_field_name_in_insert_clause, "NetworkID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	#endif

	// ----------

	memcpy( &lsize_of_bitfield, pucp, sizeof(lsize_of_bitfield) );
	pucp += sizeof( lsize_of_bitfield );
	rv += sizeof( lsize_of_bitfield );

	memcpy( &lbitfield_of_changes, pucp, sizeof(lbitfield_of_changes) );
	pucp += sizeof( lbitfield_of_changes );
	rv += sizeof( lbitfield_of_changes );

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_roll_time_bit ) == (true) )
	{
		memcpy( &ltemporary_uns_32, pucp, sizeof(weather_control.et_and_rain_table_roll_time) );
		pucp += sizeof( weather_control.et_and_rain_table_roll_time );
		rv += sizeof( weather_control.et_and_rain_table_roll_time );

		nm_WEATHER_set_roll_time( ltemporary_uns_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
								  #ifdef _MSC_VER
								  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
								  #endif
								);
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_rb_box_index_bit ) == (true) )
	{
		memcpy( &ltemporary_uns_32, pucp, sizeof(weather_control.rain_bucket_box_index) );
		pucp += sizeof( weather_control.rain_bucket_box_index );
		rv += sizeof( weather_control.rain_bucket_box_index );

		nm_WEATHER_set_rain_bucket_box_index( ltemporary_uns_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											  #endif
											);
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_rb_in_use_bit ) == (true) )
	{
		memcpy( &ltemporary_bool_32, pucp, sizeof(weather_control.rain_bucket_is_there_a_rain_bucket) );
		pucp += sizeof( weather_control.rain_bucket_is_there_a_rain_bucket );
		rv += sizeof( weather_control.rain_bucket_is_there_a_rain_bucket );

		nm_WEATHER_set_rain_bucket_connected( ltemporary_bool_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
											  #ifdef _MSC_VER
											  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											  #endif
											);
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_rb_minimum_bit ) == (true) )
	{
		memcpy( &ltemporary_uns_32, pucp, sizeof(weather_control.rain_bucket_minimum_inches_100u) );
		pucp += sizeof( weather_control.rain_bucket_minimum_inches_100u );
		rv += sizeof( weather_control.rain_bucket_minimum_inches_100u );

		nm_WEATHER_set_rain_bucket_minimum_inches( ltemporary_uns_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
												   #ifdef _MSC_VER
												   , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												   #endif
												 );
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_rb_max_hourly_bit ) == (true) )
	{
		memcpy( &ltemporary_uns_32, pucp, sizeof(weather_control.rain_bucket_max_hourly_inches_100u) );
		pucp += sizeof( weather_control.rain_bucket_max_hourly_inches_100u );
		rv += sizeof( weather_control.rain_bucket_max_hourly_inches_100u );

		nm_WEATHER_set_rain_bucket_max_hourly( ltemporary_uns_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
											   #ifdef _MSC_VER
											   , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											   #endif
											 );
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_rb_max_24_hour_bit ) == (true) )
	{
		memcpy( &ltemporary_uns_32, pucp, sizeof(weather_control.rain_bucket_max_24_hour_inches_100u) );
		pucp += sizeof( weather_control.rain_bucket_max_24_hour_inches_100u );
		rv += sizeof( weather_control.rain_bucket_max_24_hour_inches_100u );

		nm_WEATHER_set_rain_bucket_max_24_hour( ltemporary_uns_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
											    #ifdef _MSC_VER
												, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											    #endif
											  );
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_etg_box_index_bit ) == (true) )
	{
		memcpy( &ltemporary_uns_32, pucp, sizeof(weather_control.etg_box_index) );
		pucp += sizeof( weather_control.etg_box_index );
		rv += sizeof( weather_control.etg_box_index );

		nm_WEATHER_set_et_gage_box_index( ltemporary_uns_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
										  #ifdef _MSC_VER
										  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
										  #endif
										);
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_etg_in_use_bit ) == (true) )
	{
		memcpy( &ltemporary_bool_32, pucp, sizeof(weather_control.etg_is_in_use) );
		pucp += sizeof( weather_control.etg_is_in_use );
		rv += sizeof( weather_control.etg_is_in_use );

		nm_WEATHER_set_et_gage_connected( ltemporary_bool_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
										  #ifdef _MSC_VER
										  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
										  #endif
										);
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_etg_log_pulses_bit ) == (true) )
	{
		memcpy( &ltemporary_bool_32, pucp, sizeof(weather_control.etg_log_et_pulses) );
		pucp += sizeof( weather_control.etg_log_et_pulses );
		rv += sizeof( weather_control.etg_log_et_pulses );

		nm_WEATHER_set_et_gage_log_pulses( ltemporary_bool_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
										   #endif
										 );
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_etg_historical_cap_bit ) == (true) )
	{
		memcpy( &ltemporary_uns_32, pucp, sizeof(weather_control.etg_percent_of_historical_cap_100u) );
		pucp += sizeof( weather_control.etg_percent_of_historical_cap_100u );
		rv += sizeof( weather_control.etg_percent_of_historical_cap_100u );

		nm_WEATHER_set_percent_of_historical_et_cap( ltemporary_uns_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
													 #ifdef _MSC_VER
													 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
													 #endif
												   );
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_etg_historical_min_bit ) == (true) )
	{
		memcpy( &ltemporary_uns_32, pucp, sizeof(weather_control.etg_percent_of_historical_min_100u) );
		pucp += sizeof( weather_control.etg_percent_of_historical_min_100u );
		rv += sizeof( weather_control.etg_percent_of_historical_min_100u );

		nm_WEATHER_set_percent_of_historical_et_min( ltemporary_uns_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
													 #ifdef _MSC_VER
													 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
													 #endif
												   );
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_wg_box_index_bit ) == (true) )
	{
		memcpy( &ltemporary_uns_32, pucp, sizeof(weather_control.wind_gage_box_index) );
		pucp += sizeof( weather_control.wind_gage_box_index );
		rv += sizeof( weather_control.wind_gage_box_index );

		nm_WEATHER_set_wind_gage_box_index( ltemporary_uns_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
											#ifdef _MSC_VER
											, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											#endif
										  );
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_wg_in_use_bit ) == (true) )
	{
		memcpy( &ltemporary_bool_32, pucp, sizeof(weather_control.wind_is_there_a_wind_gage) );
		pucp += sizeof( weather_control.wind_is_there_a_wind_gage );
		rv += sizeof( weather_control.wind_is_there_a_wind_gage );

		nm_WEATHER_set_wind_gage_connected( ltemporary_bool_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
											#ifdef _MSC_VER
											, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											#endif
										  );
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_wg_pause_speed_bit ) == (true) )
	{
		memcpy( &ltemporary_uns_32, pucp, sizeof(weather_control.wss.wind_pause_mph) );
		pucp += sizeof( weather_control.wss.wind_pause_mph );
		rv += sizeof( weather_control.wss.wind_pause_mph );

		WEATHER_set_wind_pause_speed( ltemporary_uns_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
									  #ifdef _MSC_VER
									  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
									  #endif
									);
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_wg_resume_speed_bit ) == (true) )
	{
		memcpy( &ltemporary_uns_32, pucp, sizeof(weather_control.wss.wind_resume_mph) );
		pucp += sizeof( weather_control.wss.wind_resume_mph );
		rv += sizeof( weather_control.wss.wind_resume_mph );

		WEATHER_set_wind_resume_speed( ltemporary_uns_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
									   #ifdef _MSC_VER
									   , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
									   #endif
									 );
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_use_your_own_bit ) == (true) )
	{
		memcpy( &ltemporary_bool_32, pucp, sizeof(weather_control.reference_et_use_your_own) );
		pucp += sizeof( weather_control.reference_et_use_your_own );
		rv += sizeof( weather_control.reference_et_use_your_own );

		nm_WEATHER_set_reference_et_use_your_own( ltemporary_bool_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												  #endif
												);
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_monthlies_bit ) == (true) )
	{
		for( i = 0; i < MONTHS_IN_A_YEAR; i++ )
		{
			memcpy( &ltemporary_uns_32, pucp, sizeof(weather_control.reference_et_user_numbers_100u[i]) );
			pucp += sizeof( weather_control.reference_et_user_numbers_100u[i] );
			rv += sizeof( weather_control.reference_et_user_numbers_100u[i] );

			nm_WEATHER_set_reference_et_number( ltemporary_uns_32, (i + 1), CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
												#ifdef _MSC_VER
												, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												#endif
											  );
		}
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_state_idx_bit ) == (true) )
	{
		memcpy( &ltemporary_uns_32, pucp, sizeof(weather_control.reference_et_state_index) );
		pucp += sizeof( weather_control.reference_et_state_index );
		rv += sizeof( weather_control.reference_et_state_index );

		nm_WEATHER_set_reference_et_state_index( ltemporary_uns_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
												 #ifdef _MSC_VER
												 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												 #endif
											   );
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_county_idx_bit ) == (true) )
	{
		memcpy( &ltemporary_uns_32, pucp, sizeof(weather_control.reference_et_county_index) );
		pucp += sizeof( weather_control.reference_et_county_index );
		rv += sizeof( weather_control.reference_et_county_index );

		nm_WEATHER_set_reference_et_county_index( ltemporary_uns_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												  #endif
												);
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_city_idx_bit ) == (true) )
	{
		memcpy( &ltemporary_uns_32, pucp, sizeof(weather_control.reference_et_city_index) );
		pucp += sizeof( weather_control.reference_et_city_index );
		rv += sizeof( weather_control.reference_et_city_index );

		nm_WEATHER_set_reference_et_city_index( ltemporary_uns_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
												#ifdef _MSC_VER
												, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												#endif
											  );
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_state_bit ) == (true) )
	{
		memcpy( &ltemporary_str_48, pucp, MAX_REFERENCE_ET_NAME_LENGTH );
		pucp += MAX_REFERENCE_ET_NAME_LENGTH;
		rv += MAX_REFERENCE_ET_NAME_LENGTH;

		nm_WEATHER_set_user_defined_state( (char *)&ltemporary_str_48, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
										   #endif
										 );
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_county_bit ) == (true) )
	{
		memcpy( &ltemporary_str_48, pucp, MAX_REFERENCE_ET_NAME_LENGTH );
		pucp += MAX_REFERENCE_ET_NAME_LENGTH;
		rv += MAX_REFERENCE_ET_NAME_LENGTH;

		nm_WEATHER_set_user_defined_county( (char *)&ltemporary_str_48, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
											#ifdef _MSC_VER
											, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											#endif
										  );
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_city_bit ) == (true) )
	{
		memcpy( &ltemporary_str_48, pucp, MAX_REFERENCE_ET_NAME_LENGTH );
		pucp += MAX_REFERENCE_ET_NAME_LENGTH;
		rv += MAX_REFERENCE_ET_NAME_LENGTH;

		nm_WEATHER_set_user_defined_city( (char *)&ltemporary_str_48, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
										  #ifdef _MSC_VER
										  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
										  #endif
										);
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_in_use_bit ) == (true) )
	{
		memcpy( &ltemporary_bool_32, pucp, sizeof(weather_control.rain_switch_is_there_a_rain_switch) );
		pucp += sizeof( weather_control.rain_switch_is_there_a_rain_switch );
		rv += sizeof( weather_control.rain_switch_is_there_a_rain_switch );

		nm_WEATHER_set_rain_switch_in_use( ltemporary_bool_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
										   #ifdef _MSC_VER
										   , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
										   #endif
										 );
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_controllers_bit ) == (true) )
	{
		for( i = 0; i < MAX_CHAIN_LENGTH; i++ )
		{
			memcpy( &ltemporary_uns_32, pucp, sizeof(weather_control.rain_switch_controllers_connected_to_switch[i]) );
			pucp += sizeof( weather_control.rain_switch_controllers_connected_to_switch[i] );
			rv += sizeof( weather_control.rain_switch_controllers_connected_to_switch[i] );

			nm_WEATHER_set_rain_switch_connected( ltemporary_uns_32, i, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
												  #endif
												);
		}
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_freeze_switch_in_use_bit ) == (true) )
	{
		memcpy( &ltemporary_bool_32, pucp, sizeof(weather_control.freeze_switch_is_there_a_freeze_switch) );
		pucp += sizeof( weather_control.freeze_switch_is_there_a_freeze_switch );
		rv += sizeof( weather_control.freeze_switch_is_there_a_freeze_switch );

		nm_WEATHER_set_freeze_switch_in_use( ltemporary_bool_32, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
											 #ifdef _MSC_VER
											 , pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
											 #endif
										   );
	}

	if( B_IS_SET( lbitfield_of_changes, WEATHER_CONTROL_CHANGE_BITFIELD_freeze_switch_controllers_bit ) == (true) )
	{
		for( i = 0; i < MAX_CHAIN_LENGTH; i++ )
		{
			memcpy( &ltemporary_uns_32, pucp, sizeof(weather_control.freeze_switch_controllers_connected_to_switch[i]) );
			pucp += sizeof( weather_control.freeze_switch_controllers_connected_to_switch[i] );
			rv += sizeof( weather_control.freeze_switch_controllers_connected_to_switch[i] );

			nm_WEATHER_set_freeze_switch_connected( ltemporary_uns_32, i, CHANGE_generate_change_line, preason_for_change, 0, pset_change_bits, lchange_bitfield_to_set
													#ifdef _MSC_VER
													, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str
													#endif
												  );
		}
	}

	#ifndef _MSC_VER

		if( rv > 0 )
		{
			// 5/13/2015 ajv : We want to save the file in the following cases:
			// 
			//   - A message from the CommServer since we've responded that we received it and need to
			//     ensure it's saved.
			//   - A token response from the slave to the master
			//   - A token from the master to the slave
			//   
			// We DO NOT want to save the file when a token is received by the master since we've
			// already saved the file for one of the reasons above. This will otherwise result in a
			// double save for each change - once when it comes in and once when it's received when the
			// master distributes the changes back out.
			if( (pchanges_received_from == CHANGE_REASON_CENTRAL_OR_MOBILE) ||
				(pchanges_received_from == CHANGE_REASON_CHANGED_AT_SLAVE) ||
				((pchanges_received_from == CHANGE_REASON_DISTRIBUTED_BY_MASTER) && (!FLOWSENSE_we_are_a_master_one_way_or_another())) )
			{
				// 12/10/2014 ajv : Schedule a save rather than saving immediately to prevent stacking of
				// saves. Additionally, set a relatively short time to hel cover cases where users make a
				// lot of changes in a short period.
				FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds( FF_WEATHER_CONTROL, FLASH_STORAGE_seconds_to_delay_file_save_after_programming_change );
			}
		}
		else
		{
			Alert_bit_set_with_no_data();
		}

	#else

		SQL_MERGE_build_search_condition_uint32( "NetworkID", pnetwork_ID, pSQL_search_condition_str );

		SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements( DATABASE_TABLE_NAME_WEATHER, pSQL_search_condition_str, SQL_MERGE_include_field_name_in_insert_clause, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf );

	#endif

	return( rv );
}

#ifdef _MSC_VER

/* ---------------------------------------------------------- */
extern INT_32 WEATHER_CONTROL_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order )
{
	INT_32	rv;

	// ----------

	// 4/15/2016 ajv : In most cases, we're going to pass back an array_index of 0. However, in
	// the event the value in question is part of an array, we're going to pass back the actual
	// index so that the CommServer can sort not just on the bit number, but also the
	// array_index for values that share the same bit number.
	*psecondary_sort_order = 0;

	rv = -1;

	// ----------

	if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_roll_time_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_roll_time_bit;
		*psize_of_var_ptr = sizeof(weather_control.et_and_rain_table_roll_time);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rb_box_index_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_rb_box_index_bit;
		*psize_of_var_ptr = sizeof(weather_control.rain_bucket_box_index);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rb_in_use_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_rb_in_use_bit;
		*psize_of_var_ptr = sizeof(weather_control.rain_bucket_is_there_a_rain_bucket);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rb_minimum_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_rb_minimum_bit;
		*psize_of_var_ptr = sizeof(weather_control.rain_bucket_minimum_inches_100u);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rb_max_hourly_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_rb_max_hourly_bit;
		*psize_of_var_ptr = sizeof(weather_control.rain_bucket_max_hourly_inches_100u);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rb_max_24_hour_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_rb_max_24_hour_bit;
		*psize_of_var_ptr = sizeof(weather_control.rain_bucket_max_24_hour_inches_100u);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_etg_box_index_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_etg_box_index_bit;
		*psize_of_var_ptr = sizeof(weather_control.etg_box_index);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_etg_in_use_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_etg_in_use_bit;
		*psize_of_var_ptr = sizeof(weather_control.etg_is_in_use);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_etg_log_pulses_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_etg_log_pulses_bit;
		*psize_of_var_ptr = sizeof(weather_control.etg_log_et_pulses);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_etg_historical_cap_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_etg_historical_cap_bit;
		*psize_of_var_ptr = sizeof(weather_control.etg_percent_of_historical_cap_100u);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_etg_historical_min_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_etg_historical_min_bit;
		*psize_of_var_ptr = sizeof(weather_control.etg_percent_of_historical_min_100u);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_wg_box_index_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_wg_box_index_bit;
		*psize_of_var_ptr = sizeof(weather_control.wind_gage_box_index);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_wg_in_use_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_wg_in_use_bit;
		*psize_of_var_ptr = sizeof(weather_control.wind_is_there_a_wind_gage);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_wg_pause_speed_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_wg_pause_speed_bit;
		*psize_of_var_ptr = sizeof(weather_control.wss.wind_pause_mph);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_wg_resume_speed_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_wg_resume_speed_bit;
		*psize_of_var_ptr = sizeof(weather_control.wss.wind_resume_mph);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_use_your_own_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_use_your_own_bit;
		*psize_of_var_ptr = sizeof(weather_control.reference_et_use_your_own);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_monthlies_bit ], 11 ) == 0 )
	{
		// 1/13/2015 ajv : Note the above condition is not checking the entire length of the field
		// name. This is because there are 12 unique monthly values: ETUserMonth1 - ETUserMonth12.
		// By only comparing the first 11 characters, we can ensure we catch all 12 in one
		// condition.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "12" ) != NULL )
		{
			*psecondary_sort_order = 11;
		}
		else if( strstr( pfield_name, "11" ) != NULL )
		{
			*psecondary_sort_order = 10;
		}
		else if( strstr( pfield_name, "10" ) != NULL )
		{
			*psecondary_sort_order = 9;
		}
		else if( strstr( pfield_name, "9" ) != NULL )
		{
			*psecondary_sort_order = 8;
		}
		else if( strstr( pfield_name, "8" ) != NULL )
		{
			*psecondary_sort_order = 7;
		}
		else if( strstr( pfield_name, "7" ) != NULL )
		{
			*psecondary_sort_order = 6;
		}
		else if( strstr( pfield_name, "6" ) != NULL )
		{
			*psecondary_sort_order = 5;
		}
		else if( strstr( pfield_name, "5" ) != NULL )
		{
			*psecondary_sort_order = 4;
		}
		else if( strstr( pfield_name, "4" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}
		else if( strstr( pfield_name, "3" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}

		rv = WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_monthlies_bit;
		*psize_of_var_ptr = sizeof(weather_control.reference_et_user_numbers_100u[ 0 ]);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_state_idx_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_state_idx_bit;
		*psize_of_var_ptr = sizeof(weather_control.reference_et_state_index);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_county_idx_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_county_idx_bit;
		*psize_of_var_ptr = sizeof(weather_control.reference_et_county_index);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_city_idx_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_city_idx_bit;
		*psize_of_var_ptr = sizeof(weather_control.reference_et_city_index);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_state_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_state_bit;
		*psize_of_var_ptr = sizeof(weather_control.user_defined_state);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_county_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_county_bit;
		*psize_of_var_ptr = sizeof(weather_control.user_defined_county);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_city_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_ref_et_city_bit;
		*psize_of_var_ptr = sizeof(weather_control.user_defined_city);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_in_use_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_in_use_bit;
		*psize_of_var_ptr = sizeof(weather_control.rain_switch_is_there_a_rain_switch);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_controllers_bit ], 13 ) == 0 )
	{
		// 1/13/2015 ajv : Note the above condition is not checking the entire length of the field
		// name. This is because there are 12 values: HasRainSwitch1 - HasRainSwitch12. By only
		// comparing the first 13 characters, we can ensure we catch all 12 in one condition.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "12" ) != NULL )
		{
			*psecondary_sort_order = 11;
		}
		else if( strstr( pfield_name, "11" ) != NULL )
		{
			*psecondary_sort_order = 10;
		}
		else if( strstr( pfield_name, "10" ) != NULL )
		{
			*psecondary_sort_order = 9;
		}
		else if( strstr( pfield_name, "9" ) != NULL )
		{
			*psecondary_sort_order = 8;
		}
		else if( strstr( pfield_name, "8" ) != NULL )
		{
			*psecondary_sort_order = 7;
		}
		else if( strstr( pfield_name, "7" ) != NULL )
		{
			*psecondary_sort_order = 6;
		}
		else if( strstr( pfield_name, "6" ) != NULL )
		{
			*psecondary_sort_order = 5;
		}
		else if( strstr( pfield_name, "5" ) != NULL )
		{
			*psecondary_sort_order = 4;
		}
		else if( strstr( pfield_name, "4" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}
		else if( strstr( pfield_name, "3" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}

		rv = WEATHER_CONTROL_CHANGE_BITFIELD_rain_switch_controllers_bit;
		*psize_of_var_ptr = sizeof(weather_control.rain_switch_controllers_connected_to_switch[ 0 ]);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_freeze_switch_in_use_bit ], NUMBER_OF_CHARS_IN_A_NAME ) == 0 )
	{
		rv = WEATHER_CONTROL_CHANGE_BITFIELD_freeze_switch_in_use_bit;
		*psize_of_var_ptr = sizeof(weather_control.freeze_switch_is_there_a_freeze_switch);
	}
	else if( strncmp( pfield_name, WEATHER_CONTROL_database_field_names[ WEATHER_CONTROL_CHANGE_BITFIELD_freeze_switch_controllers_bit ], 15 ) == 0 )
	{
		// 1/13/2015 ajv : Note the above condition is not checking the entire length of the field
		// name. This is because there are 12 values: HasFreezeSwitch1 - HasFreezeSwitch12. By only
		// comparing the first 15 characters, we can ensure we catch all 12 in one condition.

		// 4/15/2016 ajv : Now, figure out which array index based on the numeric value in the
		// passed-in field name.
		if( strstr( pfield_name, "12" ) != NULL )
		{
			*psecondary_sort_order = 11;
		}
		else if( strstr( pfield_name, "11" ) != NULL )
		{
			*psecondary_sort_order = 10;
		}
		else if( strstr( pfield_name, "10" ) != NULL )
		{
			*psecondary_sort_order = 9;
		}
		else if( strstr( pfield_name, "9" ) != NULL )
		{
			*psecondary_sort_order = 8;
		}
		else if( strstr( pfield_name, "8" ) != NULL )
		{
			*psecondary_sort_order = 7;
		}
		else if( strstr( pfield_name, "7" ) != NULL )
		{
			*psecondary_sort_order = 6;
		}
		else if( strstr( pfield_name, "6" ) != NULL )
		{
			*psecondary_sort_order = 5;
		}
		else if( strstr( pfield_name, "5" ) != NULL )
		{
			*psecondary_sort_order = 4;
		}
		else if( strstr( pfield_name, "4" ) != NULL )
		{
			*psecondary_sort_order = 3;
		}
		else if( strstr( pfield_name, "3" ) != NULL )
		{
			*psecondary_sort_order = 2;
		}
		else if( strstr( pfield_name, "2" ) != NULL )
		{
			*psecondary_sort_order = 1;
		}
		else if( strstr( pfield_name, "1" ) != NULL )
		{
			*psecondary_sort_order = 0;
		}

		rv = WEATHER_CONTROL_CHANGE_BITFIELD_freeze_switch_controllers_bit;
		*psize_of_var_ptr = sizeof(weather_control.freeze_switch_controllers_connected_to_switch[ 0 ]);
	}

	return( rv );
}

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

