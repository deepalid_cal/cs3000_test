/*  file = irri_irri.c                       2011.08.01  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"irri_irri.h"

#include	"irri_comm.h"

#include	"app_startup.h"

#include	"configuration_controller.h"

#include	"irri_flow.h"

#include	"alerts.h"

#include	"stations.h"

#include	"foal_irri.h"

#include	"battery_backed_vars.h"

#include	"epson_rx_8025sa.h"

#include	"flowsense.h"

#include	"cs_mem.h"

#include	"alert_parsing.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2012.02.07 rmd : irri_irri is NOT battery backed. The approach in the CS3000 is if the
// chain goes down we will NOT try to continue (pick up irrigation where we left off) for
// each box in the chain. Operating as independents. If there is a power fail when the cahin
// comes back up the master will tell the irri machine all that is happening irrigation
// wise. If we fall into forced or the chain is detected as 'different' then all the
// irrigation is cleared. Any on-going irrigation is lost.
IRRI_IRRI	irri_irri;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called upon each program startup. Initializes the irri_irri structure. This
    structure is not battery backed and therefore requires this step.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the STARTUP task.
	
	@RETURN (none)

	@AUTHOR 2012.02.07 rmd
*/
extern void init_irri_irri( void )
{
	xSemaphoreTakeRecursive( irri_irri_recursive_MUTEX, portMAX_DELAY );

	// Must initalize the irrigation list. Remember these list items are in dynamic memory.
	nm_ListInit( &irri_irri.list_of_irri_all_irrigation, offsetof( IRRI_MAIN_LIST_OF_STATIONS_STRUCT, list_support_irri_all_irrigation ) );

	xSemaphoreGiveRecursive( irri_irri_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Restarts all the irrigation activity lists and variables. As part of
    falling into or coming out of forced. Basically when a chain configuration CHANGE is
    detected. This is very similar to what happens upon startup as the irri structures for
    irrigation are NOT battery backed. So upon each program start they are also intialized.
    In this case however we attend to the possible memory allocations taken for the
    irrigation and SXRU list.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the COMM_MNGR task when a chain configuration
    change is detected.
	
	@RETURN (none)

	@AUTHOR 2012.02.07 rmd
*/
extern void IRRI_restart_all_of_irrigation( void )
{
	// Then one thing we do is send the irrigation list over to this controllers irri machine.
	xSemaphoreTakeRecursive( irri_irri_recursive_MUTEX, portMAX_DELAY );
	
	// We do not have to be specific about what we are pointing to.
	void	*liml_ptr;
	
	liml_ptr = nm_ListGetFirst( &irri_irri.list_of_irri_all_irrigation );
	
	while( irri_irri.list_of_irri_all_irrigation.count > 0 )
	{
		liml_ptr = nm_ListRemoveHead( &irri_irri.list_of_irri_all_irrigation );

		mem_free( liml_ptr );
	}

	xSemaphoreGiveRecursive( irri_irri_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called at a 1 hertz rate when the communications sync indicates completion.
    This function acts as a fail safe task guarding irrigation. To make sure stations don't
    run well past their run time. It is the master that makes all the formal decisions about
    when to turn ON and OFF. But if communications were to fail we count on this backup.
    Think of it as a watchdog on the irrigation process. The other function of this task is
    to indicate in the global TP_BOARD structure which stations are ON.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the TD_CHECK task.
	
	@RETURN (none)

	@AUTHOR 2012.02.08 rmd
*/
extern void IRRI_maintain_irrigation_list( void )
{
	char	str_8[ 8 ];
	
	BOOL_32	flagged_for_removal;

	IRRI_MAIN_LIST_OF_STATIONS_STRUCT	*imls;

	// ----------
	
	xSemaphoreTakeRecursive( irri_irri_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	// 2012.02.08 rmd : Check against its STOP TIME. We could do this as a form of watchdog
	// activity. But for now I've opted to not. We will be checking the remaining run time
	// however. For a station to fail either the stop time or the remaining run time test here
	// the communications must fail at a very particular spot. And I beleive the chances of that
	// or so rare that I contend we do not need to make the STOP time test. Keep in mind though
	// to run past the STOP time could really get an operator in TROUBLE. So that would be our
	// justification to make this test. We do not want this to happen. So TODO : add in the stop
	// time test for programmed irrigation, and hold-over irrigation. What about manual
	// programs?

	imls = nm_ListGetFirst( &irri_irri.list_of_irri_all_irrigation );

	while( imls != NULL )
	{
		// 2012.02.21 rmd : For the case when the master fails to tell us to turn OFF the station
		// the station ON time watchdog catches this and we turn the station OFF and remove the
		// station from this list.
		flagged_for_removal = (false);
		
		// ----------
		
		// 11/26/2014 rmd : House cleaning when abnormal chain events take place. This may or may
		// not be necessary as should happen when chain goes down. But what happens if during
		// irrigation someone changes from being part of a chain to not part of a chain? I think
		// irrigation is wiped but this adds to that effort.
		if( !FLOWSENSE_we_are_poafs() || comm_mngr.chain_is_down )
		{
			if( imls->box_index_0 != FLOWSENSE_get_controller_index() )
			{
				flagged_for_removal = (true);
			}
		}

		// ----------
		
		// 8/31/2015 rmd : Sanity Check - this tests we don't have stations in our list for boxes
		// that our chain members don't show as active and part of the chain. This may not be needed
		// but is a reasonable housekeeping measure. Interesting to see if the alert shows. I think
		// it shouldn't ever show.
		if( !flagged_for_removal && !chain.members[ imls->box_index_0 ].saw_during_the_scan )
		{
			flagged_for_removal = (true);
			
			Alert_Message_va( "I Maintain: box not active %s", ALERTS_get_station_number_with_or_without_two_wire_indicator( imls->box_index_0, imls->station_number_0_u8, str_8, sizeof(str_8) ) );
		}
		
		// ----------

		if( !flagged_for_removal )
		{
			if( imls->ibf.station_is_ON == (true) )
			{
				// For all stations decrement the remaining time. We need to do this for all stations in
				// order we can display remaining time for all.
				imls->remaining_ON_or_soak_seconds -= 1;
				
				// ----------
				
				// 2012.02.08 rmd : If this station belongs to us we perform the watchdog duties. And
				// indicate if the TP Board should activate the output.
				if( imls->box_index_0 == FLOWSENSE_get_controller_index() )
				{
					// 2012.02.08 rmd : So if the master does not deliver a token turning OFF the station within
					// 45 seconds of the expiration we shall take matter into our own hands. Remember with the
					// introduction of the smart token the synchronization between master and slave should be
					// fairly tight.
					if( imls->remaining_ON_or_soak_seconds < (-45) )
					{
						// 2012.02.08 rmd : Make an alert line?
						ALERTS_get_station_number_with_or_without_two_wire_indicator( imls->box_index_0, imls->station_number_0_u8, str_8, sizeof(str_8) );
						
						Alert_Message_va( "IRRI: station %s being forced OFF by irrigation watchdog", str_8 );
						
						// ----------
						
						// 2012.02.21 rmd : Turn it OFF and remove it from the list. Something is wrong. And asking
						// the master to at this point seems problematic. We need to take the action!
						imls->ibf.station_is_ON = (false);  // Not that we have to do this. But make the point I suppose.
						
						flagged_for_removal = (true);
					}
	
				}
				
			}
			else
			{
				// 2012.02.08 rmd : Well now the stuff to do if the station is OFF.
	
				// 2012.02.08 rmd : Decrement the soak time. This is so the display reflects about what is
				// going on over on the foal side. It is not a functional item. The real soak time is kept
				// in the FOAL machine.
				if( imls->remaining_ON_or_soak_seconds > 0 )
				{
					imls->remaining_ON_or_soak_seconds -= 1;
				}
				
			}  // of if station is OFF
		}
			
		// ----------

		if( flagged_for_removal )
		{
			void	*list_item_to_delete;

			list_item_to_delete = imls;

			imls = nm_ListGetNext( &irri_irri.list_of_irri_all_irrigation, imls );

			// Remove it from the irri_irri irrigation list.
			nm_ListRemove( &irri_irri.list_of_irri_all_irrigation, list_item_to_delete );

			// Do not forget to free the associated memory for the list item.
			mem_free( list_item_to_delete );
		}
		else
		{
			imls = nm_ListGetNext( &irri_irri.list_of_irri_all_irrigation, imls );
		}

	}  // of while looping through all in list

	xSemaphoreGiveRecursive( irri_irri_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This action_needed record was received as part of a token from the master.
    Find the station the xfer is referring to and take the appropriate action. Alert about
    anomolies.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the COMM_MNGR task during the token processing
    function.
	
	@RETURN (none)

	@AUTHOR 2012.02.10 rmd
*/
extern void IRRI_process_rcvd_action_needed_record( ACTION_NEEDED_XFER_RECORD const *const panxr )
{
	char	str_8[ 8 ];

	UNS_32	lstation_preserves_index;
		
	IRRI_MAIN_LIST_OF_STATIONS_STRUCT	*imlss;

	// 2011.11.21 rmd : Because we are traversing through the list and changing the values
	// within an entry take this mutex.
	xSemaphoreTakeRecursive( irri_irri_recursive_MUTEX, portMAX_DELAY );

	imlss = nm_ListGetFirst( &irri_irri.list_of_irri_all_irrigation );
	
	while( imlss != NULL )
	{
		if( (panxr->box_index_0 == imlss->box_index_0) &&
				(panxr->w_irrigation_reason_in_list_u8 == imlss->ibf.w_reason_in_list) &&
					(panxr->station_number_0_u8 == imlss->station_number_0_u8) )
		{
			// We've got a match. It is in the list.
			break;
		}

		imlss = nm_ListGetNext( &irri_irri.list_of_irri_all_irrigation, imlss );
	}

	if( imlss == NULL )
	{
		// 2012.02.06 rmd : What should we do. Hmmm. If we cannot find a station to turn ON should
		// we tell the foal machine. What we want to prevent is PUMP activiation when no valves are
		// ON. Not sure how extensive to get here. Like telling the FOAL machine to kill a station
		// in his list cause it ain't in our list and he asked it to turn ON?

		// 9/21/2012 rmd : One way this can happen is if the user rapid fires a request to test a
		// station. And there are already stations ON up to the electrical limit. So the foal
		// machine recognizes this and makes a request to remove the station from the irri list.
		// This however happens before the token was made delivering the action needed record ADDING
		// it to the irri list in the first place. And remember a station can only be on the action
		// needed list for a single reason. So when we add the station to the list and find it on
		// the list for the reason to add it to the irri list wecould decide to remove the station
		// from the action needed list all together. Or let it come on through here and catch it
		// here and not flag it as an error. I do that so we know we don't somehow end up with a
		// station on the irri list here and no action needed record to remove it.
		//if( panxr->sxru_xfer_reason_u8 != ACTION_REASON_TO_PERFORM_A_LIST_REMOVAL_hit_electrical_limit)
		{
			Alert_Message_va( "IRRI: rcvd station %s not found", ALERTS_get_station_number_with_or_without_two_wire_indicator( panxr->box_index_0, panxr->station_number_0_u8, (char*)&str_8, sizeof(str_8) ) );
		}
	}
	else
	{
		if( STATION_PRESERVES_get_index_using_box_index_and_station_number( imlss->box_index_0, imlss->station_number_0_u8, &lstation_preserves_index ) == (true) )
		{
			// 6/27/2012 rmd : Okay so the station_preserves index is good.
			
			// ----------
			
			// 11/13/2014 rmd : Always update these two witht he freshly delivered values. It works for
			// all cases of the action reason the record was delivered.
			imlss->remaining_ON_or_soak_seconds = panxr->remaining_ON_or_soak_seconds;

			imlss->requested_irrigation_seconds_u32 = panxr->updated_requested_irrigation_seconds_ul;
			
			// ----------

			// 11/13/2014 rmd : Now look at the reason for special processing.
			if( (panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_ON_and_clear_flow_status) ||
			    (panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_ON_and_leave_flow_status)
			  )
			{
				// Indicate it is ON!
				imlss->ibf.station_is_ON = (true);
				
				// 6/26/2012 rmd : We could update the cycle seconds here with the remaining seconds ON. As
				// that reflect the actual initial amount of this cycle. But I think we'll just let stand
				// the programmed cycle amount.
				//imlss->cycle_seconds = panxr->remaining_ON_or_soak_seconds;
				
				if( panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_ON_and_clear_flow_status )
				{
					// 6/27/2012 rmd : When working with the bit-field should take the mutex. Multiple
					// instructions used to maipulate a bit-field.
					xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

					station_preserves.sps[ lstation_preserves_index ].spbf.flow_status = STATION_PRESERVES_FLOW_STATUS_normal_flow;

					xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
				}
				
				// 6/27/2012 rmd : We always clear this flag for any turn ON reason in the list. The i
				// status is a passive alert for display purposes only.
				station_preserves.sps[ lstation_preserves_index ].spbf.i_status = STATION_PRESERVES_I_STATUS_no_error;
				
				// ----------
				
				// 11/12/2014 rmd : Okay here's the deal. We wait until we get a request to turn a station
				// ON till clearing the 2W cable excessive current flag in the tpmicro. This is to prevent
				// the tpmicro from energizing the cable if there is a POC decoder (flow meter readings). If
				// there was a cable short we would end up powering up and down the cable repeating the
				// short detection process. So there are two flags that should both be in sync but to cover
				// the possibility they get out of sync we test here for one or the other condition to
				// decide to tell the tpmicro to clear his excessive current condition.
				if( irri_comm.two_wire_cable_excessive_current[ imlss->box_index_0 ] || (tpmicro_data.two_wire_cable_excessive_current_xmission_state = TWO_WIRE_CABLE_ANOMALY_STATE_waiting_for_reponse_from_master) )
				{
					// 11/11/2014 rmd : Always clear the two_wire cable excessive current display indication.
					// This is a user informative variable only. And performs no functional purpose with respect
					// to irrigation.
					irri_comm.two_wire_cable_excessive_current[ imlss->box_index_0 ] = (false);
					
					// 11/12/2014 rmd : And if this station is on our own box clear the two_wire cable short
					// indication in the tpmicro to allow the tpmicro to send commands to the decoders. If there
					// is still a cable short it will come back.
					if( imlss->box_index_0 == FLOWSENSE_get_controller_index() )
					{
						tpmicro_data.two_wire_cable_excessive_current_xmission_state = TWO_WIRE_CABLE_ANOMALY_STATE_send_ack_to_tpmicro;
					}
				}
			}
			else
			if( (panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_cause_cycle_has_completed) ||

				(panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_first_flow_fault) ||
				(panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_hi_flow_fault) ||
				(panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_lo_flow_fault) ||

				(panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_pause_rre_commanded) ||

				(panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_pause_wind) ||

				(panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_pause_for_a_higher_reason_in_the_list) ||

				(panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_have_acquired_expected)
			  )
			{
				// Indicate it is OFF!
				imlss->ibf.station_is_ON = (false);
				
				// ----------
				
				// 11/13/2014 rmd : Soak seconds already set above outside of the special cases processing.

				// ----------

				xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );
				
				if( panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_hi_flow_fault )
				{

					// 6/27/2012 rmd : Set the flow status appropriately. That is the point of using these
					// action needed reasons that indicate which flow error we are turning OFF for.
					station_preserves.sps[ lstation_preserves_index ].spbf.flow_status = STATION_PRESERVES_FLOW_STATUS_high_flow;
				}
				else
				if( panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_lo_flow_fault )
				{

					// 6/27/2012 rmd : Set the flow status appropriately. That is the point of using these
					// action needed reasons that indicate which flow error we are turning OFF for.
					station_preserves.sps[ lstation_preserves_index ].spbf.flow_status = STATION_PRESERVES_FLOW_STATUS_lo_flow;
				}
				
				xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
			}
			else
			{
				// 6/4/2012 rmd : For all other reasons we are here to remove the ilc from the irri list.
				nm_ListRemove( &irri_irri.list_of_irri_all_irrigation, imlss );
				
				// And do not forget to free the associated memory for this list item.
				mem_free( imlss );
				
				// ----------
				
				// 8/17/2012 rmd : Manipulating a bit field. Most likely NOT atomic! So take the MUTEX.
				xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

				if( panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_AND_REMOVE_hi_flow_fault )
				{
					// 6/27/2012 rmd : Set the flow status appropriately. That is the point of using these
					// action needed reasons that indicate which flow error we are turning OFF for.
					station_preserves.sps[ lstation_preserves_index ].spbf.flow_status = STATION_PRESERVES_FLOW_STATUS_high_flow;
				}
				else
				if( panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_AND_REMOVE_lo_flow_fault )
				{
					// 6/27/2012 rmd : Set the flow status appropriately. That is the point of using these
					// action needed reasons that indicate which flow error we are turning OFF for.
					station_preserves.sps[ lstation_preserves_index ].spbf.flow_status = STATION_PRESERVES_FLOW_STATUS_lo_flow;
				}
				else
				if( panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_AND_REMOVE_conventional_station_short )
				{
					// 11/5/2014 rmd : Set flag. Note that this is only used for display purposes.
					station_preserves.sps[ lstation_preserves_index ].spbf.i_status = STATION_PRESERVES_I_STATUS_conventional_station_short;
				}
				else
				if( panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_AND_REMOVE_conventional_mv_short )
				{
					// 11/5/2014 rmd : Set flag. Note that this is only used for display purposes.
					station_preserves.sps[ lstation_preserves_index ].spbf.i_status = STATION_PRESERVES_I_STATUS_conventional_mv_short;
				}
				else
				if( panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_AND_REMOVE_conventional_pump_short )
				{
					// 11/5/2014 rmd : Set flag. Note that this is only used for display purposes.
					station_preserves.sps[ lstation_preserves_index ].spbf.i_status = STATION_PRESERVES_I_STATUS_conventional_pump_short;
				}
				else

				if( panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_AND_REMOVE_station_decoder_voltage_too_low )
				{
					// 11/5/2014 rmd : Set flag. Note that this is only used for display purposes.
					station_preserves.sps[ lstation_preserves_index ].spbf.i_status = STATION_PRESERVES_I_STATUS_station_decoder_voltage_too_low;
				}
				else
				if( panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_AND_REMOVE_station_decoder_solenoid_short )
				{
					// 11/5/2014 rmd : Set flag. Note that this is only used for display purposes.
					station_preserves.sps[ lstation_preserves_index ].spbf.i_status = STATION_PRESERVES_I_STATUS_station_decoder_short;
				}
				else
				if( panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_AND_REMOVE_station_decoder_inoperative )
				{
					// 11/5/2014 rmd : Set flag. Note that this is only used for display purposes.
					station_preserves.sps[ lstation_preserves_index ].spbf.i_status = STATION_PRESERVES_I_STATUS_station_decoder_inop;
				}
				else

				if( panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_AND_REMOVE_poc_decoder_voltage_too_low )
				{
					// 11/5/2014 rmd : Set flag. Note that this is only used for display purposes.
					station_preserves.sps[ lstation_preserves_index ].spbf.i_status = STATION_PRESERVES_I_STATUS_poc_decoder_voltage_too_low;
				}
				else
				if( panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_AND_REMOVE_poc_decoder_solenoid_short )
				{
					// 11/5/2014 rmd : Set flag. Note that this is only used for display purposes.
					station_preserves.sps[ lstation_preserves_index ].spbf.i_status = STATION_PRESERVES_I_STATUS_poc_decoder_short;
				}
				else
				if( panxr->sxru_xfer_reason_u8 == ACTION_REASON_TURN_OFF_AND_REMOVE_poc_decoder_inoperative )
				{
					// 11/5/2014 rmd : Set flag. Note that this is only used for display purposes.
					station_preserves.sps[ lstation_preserves_index ].spbf.i_status = STATION_PRESERVES_I_STATUS_poc_decoder_inop;
				}

				xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
			}
			
			//Alert_Message_va( "IRRI: an unhandled action needed #%u", panxr->sxru_xfer_reason_u8 );
	
			// TODO 2012.02.08 rmd : Note from the 2000e: for the case of TEST valves we want to remove
			// the from the list and give the user an advisory about why it can't come ON when they
			// can't for reasons like too many on etc. There is about 4 such reasons. 
		}

	}  // of if we do find the station in the irri list
	
	xSemaphoreGiveRecursive( irri_irri_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

