/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_WEATHER_CONTROL_H
#define _INC_WEATHER_CONTROL_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"foal_defs.h"

#include	"cs3000_tpmicro_common.h"

#include	"pdata_changes.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define		WEATHER_OPTION_INSTALLED_DEFAULT					(false)

#define		WEATHER_RAIN_BUCKET_CONNECTED_DEFAULT				(false)

#define		WEATHER_RAIN_BUCKET_MINIMUM_MIN_100u				(1)
#define		WEATHER_RAIN_BUCKET_MINIMUM_MAX_100u				(200)
#define		WEATHER_RAIN_BUCKET_MINIMUM_DEFAULT_100u			(10)

#define		WEATHER_RAIN_BUCKET_MAXIMUM_HOURLY_MIN_100u			(1)
#define		WEATHER_RAIN_BUCKET_MAXIMUM_HOURLY_MAX_100u			(200)
#define		WEATHER_RAIN_BUCKET_MAXIMUM_HOURLY_DEFAULT_100u		(20)

#define		WEATHER_RAIN_BUCKET_MAXIMUM_24_HOUR_MIN_100u		(1)
#define		WEATHER_RAIN_BUCKET_MAXIMUM_24_HOUR_MAX_100u		(600)
#define		WEATHER_RAIN_BUCKET_MAXIMUM_24_HOUR_DEFAULT_100u	(60)

// ----------------------

#define		WEATHER_ET_GAGE_CONNECTED_DEFAULT					(false)

#define		WEATHER_ET_LOG_PULSES_DEFAULT						(false)

// ----------------------

#define		WEATHER_ET_RAIN_TABLE_ROLL_TIME_MINIMUM				(17 * 60 * 60)		// 17 hours = 5:00 PM

// 7/26/2012 rmd : 11:59:59AM - can't use midnight cause that is 0 seconds and would trigger
// range check issues when compared to the MINIMUM specified here. Keep it simple and just
// use 11PM.
#define		WEATHER_ET_RAIN_TABLE_ROLL_TIME_MAXIMUM				(23 * 60 * 60)		// 23 hours = 11:00 PM

#define		WEATHER_ET_RAIN_TABLE_ROLL_TIME_DEFAULT				(20 * 60 * 60)		// 20 hours = 8:00 PM

// ----------------------

#define		WEATHER_ET_PERCENT_OF_HISTORICAL_CAP_MIN_100u		(10)
#define		WEATHER_ET_PERCENT_OF_HISTORICAL_CAP_MAX_100u		(500)

// 3/19/2015 ajv : After speaking with David, we've increased the default historical ET cap
// from 150% to 300% to help ensure ET is not limited during the WaterSense test. The user
// can always reduce this if necessary.
// 
// 4/15/2016 ajv : We've opted to set the default back to 150%. If there's a concern that
// we'll get much higher values during the WaterSense test, we can adjust our documentation
// to instruct the user to increase the cap.
#define		WEATHER_ET_PERCENT_OF_HISTORICAL_CAP_DEFAULT_100u	(150)

#define		WEATHER_ET_PERCENT_OF_HISTORICAL_MIN_MIN_100u		(0)
#define		WEATHER_ET_PERCENT_OF_HISTORICAL_MIN_MAX_100u		(100)
#define		WEATHER_ET_PERCENT_OF_HISTORICAL_MIN_DEFAULT_100u	(0)

#define		WEATHER_WIND_GAGE_CONNECTED_DEFAULT					(false)

#define		WEATHER_WIND_PAUSE_SPEED_MIN						(2)
#define		WEATHER_WIND_PAUSE_SPEED_MAX						(99)
#define		WEATHER_WIND_PAUSE_SPEED_DEFAULT					(15)

// 7/26/2012 rmd : The DEFAULT resume speed should be (or is it MUST be) be less than the
// PAUSE speed.
#define		WEATHER_WIND_RESUME_SPEED_MIN						(WEATHER_WIND_PAUSE_SPEED_MIN-1)
#define		WEATHER_WIND_RESUME_SPEED_MAX						(WEATHER_WIND_PAUSE_SPEED_MAX-1)
#define		WEATHER_WIND_RESUME_SPEED_DEFAULT					(10)

#define		WEATHER_REFERENCE_ET_USE_YOUR_OWN_DEFAULT			(false)

#define		WEATHER_REFERENCE_ET_MIN_100u						(0)
#define		WEATHER_REFERENCE_ET_MAX_100u						(2000)
#define		WEATHER_REFERENCE_ET_DEFAULT_CITY_INDEX				(21)	// Glendora

#define		WEATHER_REFERENCE_ET_STATE_MIN						(0)
#define		WEATHER_REFERENCE_ET_STATE_MAX						(NO_OF_STATES-1)
#define		WEATHER_REFERENCE_ET_STATE_DEFAULT					(0)		// California

#define		WEATHER_REFERENCE_ET_COUNTY_MIN						(0)
#define		WEATHER_REFERENCE_ET_COUNTY_MAX						(NO_OF_COUNTIES-1)
#define		WEATHER_REFERENCE_ET_COUNTY_DEFAULT					(2)		// Kern County

#define		WEATHER_REFERENCE_ET_CITY_MIN						(0)
#define		WEATHER_REFERENCE_ET_CITY_MAX						(NO_OF_CITIES-1)
#define		WEATHER_REFERENCE_ET_CITY_DEFAULT					(13)	// China Lake

#define		WEATHER_RAIN_SWITCH_IN_USE_DEFAULT					(false)
#define		WEATHER_RAIN_SWITCH_CONNECTED_DEFAULT				(false)

// 11/12/2014 rmd : I think if the next token to arrive with a 'switched' rain switch takes
// more than this time to arrive the rain switch indication will chatter (flip-flop that
// is). I think this timeout is pretty damn tight at 20 seconds. A long SR chain may be
// longer. The down side of a longer time is once the rain switch 'dries out' the user wiats
// this amount of time before seeing the stauts screen change. And that may be confusing
// during testing. Freeze switch is the same story.
#define		WEATHER_RAIN_SWITCH_TIMEOUT_MILLISECONDS			(20000)

#define		WEATHER_FREEZE_SWITCH_IN_USE_DEFAULT				(false)
#define		WEATHER_FREEZE_SWITCH_CONNECTED_DEFAULT				(false)
#define		WEATHER_FREEZE_SWITCH_TIMEOUT_MILLISECONDS			(20000)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct WEATHER_CONTROL_STRUCT WEATHER_CONTROL_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern UNS_32 WEATHER_GuiVar_ETGageInstalledAtIndex;

extern UNS_32 WEATHER_GuiVar_RainBucketInstalledAtIndex;

extern UNS_32 WEATHER_GuiVar_WindGageInstalledAtIndex;

// ----------

extern UNS_32 WEATHER_GuiVar_box_indexes_with_dash_w_option[ MAX_CHAIN_LENGTH ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_file_weather_control( void );

extern void save_file_weather_control( void );


extern UNS_32 WEATHER_build_data_to_send( UNS_8 **pucp,
										  const UNS_32 pmem_used_so_far,
										  BOOL_32 *pmem_used_so_far_is_less_than_allocated_memory,
										  const UNS_32 preason_data_is_being_built,
										  const UNS_32 pallocated_memory );

extern UNS_32 nm_WEATHER_extract_and_store_changes_from_comm( const unsigned char *pucp,
															  const UNS_32 preason_for_change,
															  const BOOL_32 pset_change_bits,
															  const UNS_32 pchanges_received_from
															  #ifdef _MSC_VER
															  , char *pSQL_statements,
															  char *pSQL_search_condition_str,
															  char *pSQL_update_str,
															  char *pSQL_insert_fields_str,
															  char *pSQL_insert_values_str,
															  char *pSQL_single_merge_statement_buf,
															  const UNS_32 pnetwork_ID
															  #endif
															);

// ----------

#ifdef _MSC_VER

extern INT_32 WEATHER_CONTROL_get_change_bit_number( char *pfield_name, UNS_32 *psize_of_var_ptr, UNS_32 *psecondary_sort_order );

#endif

// ----------

extern void FDTO_WEATHER_update_real_time_weather_GuiVars( void );

extern void WEATHER_copy_et_gage_settings_into_GuiVars( void );

extern void WEATHER_copy_rain_bucket_settings_into_GuiVars( void );

extern void WEATHER_copy_wind_gage_settings_into_GuiVars( void );

extern void WEATHER_copy_rain_switch_settings_into_GuiVars( void );

extern void WEATHER_copy_freeze_switch_settings_into_GuiVars( void );

extern void WEATHER_copy_historical_et_settings_into_GuiVars( void );


extern void WEATHER_extract_and_store_et_gage_changes_from_GuiVars( void );

extern void WEATHER_extract_and_store_rain_bucket_changes_from_GuiVars( void );

extern void WEATHER_extract_and_store_wind_gage_changes_from_GuiVars( void );

extern void WEATHER_extract_and_store_rain_switch_changes_from_GuiVars( void );

extern void WEATHER_extract_and_store_freeze_switch_changes_from_GuiVars( void );

extern void WEATHER_extract_and_store_historical_et_changes_from_GuiVars( void );

// ----------------------

extern void nm_WEATHER_set_percent_of_historical_et_cap( UNS_32 petg_percent_of_historical_cap_100u,
														 const BOOL_32 pgenerate_change_line,
														 const UNS_32 preason_for_change,
														 const UNS_32 pbox_index_0,
														 const BOOL_32 pset_change_bits,
														 CHANGE_BITS_32_BIT *pchange_bitfield_to_set
														 #ifdef _MSC_VER
														 , char *pmerge_update_str,
														 char *pmerge_insert_fields_str,
														 char *pmerge_insert_values_str
														 #endif
													   );

extern void nm_WEATHER_set_rain_switch_connected( BOOL_32 pconnected,
												  const UNS_32 pcontroller_index_0,
												  const BOOL_32 pgenerate_change_line,
												  const UNS_32 preason_for_change,
												  const UNS_32 pbox_index_0,
												  const BOOL_32 pset_change_bits,
												  CHANGE_BITS_32_BIT *pchange_bitfield_to_set
												  #ifdef _MSC_VER
												  , char *pmerge_update_str,
												  char *pmerge_insert_fields_str,
												  char *pmerge_insert_values_str
												  #endif
												);

extern void nm_WEATHER_set_freeze_switch_connected( BOOL_32 pconnected,
													const UNS_32 pcontroller_index_0,
													const BOOL_32 pgenerate_change_line,
													const UNS_32 preason_for_change,
													const UNS_32 pbox_index_0,
													const BOOL_32 pset_change_bits,
													CHANGE_BITS_32_BIT *pchange_bitfield_to_set
													#ifdef _MSC_VER
													, char *pmerge_update_str,
													char *pmerge_insert_fields_str,
													char *pmerge_insert_values_str
													#endif
												  );

// ----------------------

extern BOOL_32 WEATHER_there_is_a_weather_option_in_the_chain( void );


extern UNS_32 WEATHER_get_et_gage_box_index( void );

extern BOOL_32 WEATHER_get_et_gage_is_in_use( void );

extern BOOL_32 WEATHER_get_et_gage_log_pulses( void );


extern UNS_32 WEATHER_get_rain_bucket_box_index( void );

extern BOOL_32 WEATHER_get_rain_bucket_is_in_use( void );

extern UNS_32 WEATHER_get_rain_bucket_minimum_inches_100u( void );

extern UNS_32 WEATHER_get_rain_bucket_max_hourly_inches_100u( void );

extern UNS_32 WEATHER_get_rain_bucket_max_24_hour_inches_100u( void );


extern UNS_32 WEATHER_get_wind_gage_box_index( void );

extern BOOL_32 WEATHER_get_wind_gage_in_use( void );




extern BOOL_32 WEATHER_get_rain_switch_in_use( void );

// 3/9/2016 mjb : Added this to use for SW1 as a rain switch in use indicator.
extern BOOL_32 WEATHER_get_SW1_as_rain_switch_in_use( void );

extern BOOL_32 WEATHER_get_rain_switch_connected_to_this_controller( const UNS_32 pindex_0 );

extern BOOL_32 WEATHER_sw1_is_available_and_enabled_as_a_rain_switch_at_this_controller( const UNS_32 pindex_0 );




extern BOOL_32 WEATHER_get_freeze_switch_in_use( void );

extern BOOL_32 WEATHER_get_freeze_switch_connected_to_this_controller( const UNS_32 pindex_0 );

extern void WEATHER_get_a_copy_of_the_wind_settings( WIND_SETTINGS_STRUCT *wss_ptr );


extern UNS_32 WEATHER_get_percent_of_historical_cap_100u( void );

extern BOOL_32 WEATHER_get_reference_et_use_your_own( void );

extern UNS_32 WEATHER_get_reference_et_number( const UNS_32 pmonth1_12 );

extern UNS_32 WEATHER_get_reference_et_city_index( void );


extern UNS_32 WEATHER_get_et_and_rain_table_roll_time( void );



extern CHANGE_BITS_32_BIT *WEATHER_get_change_bits_ptr( const UNS_32 pchange_reason );

extern void WEATHER_CONTROL_set_bits_on_all_settings_to_cause_distribution_in_the_next_token( void );

extern void WEATHER_on_all_settings_set_or_clear_commserver_change_bits( UNS_32 const pset_or_clear );

extern void nm_WEATHER_update_pending_change_bits( const UNS_32 pcomm_error_occurred );


extern BOOL_32 WEATHER_CONTROL_calculate_chain_sync_crc( UNS_32 const pff_name );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

