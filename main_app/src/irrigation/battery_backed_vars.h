/*  file = battery_backed_vars.h             2011.08.01  rmd  */

/* ---------------------------------------------------------- */

#ifndef _INC_BATTERY_BACKED_VARS_H
#define _INC_BATTERY_BACKED_VARS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	"cal_td_utils.h"

#include	"lpc_types.h"

#include	"foal_comm.h"

#include	"flow_recorder.h"

#include	"station_history_data.h"

#include	"station_report_data.h"

#include	"lights_report_data.h"

#include	"irrigation_system.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/6/2015 rmd : The length I have decided to use for the structure test PRE strings. Gives
// me enough to work with. Probably could have reduced to 8. But 

#define	BB_PRE_STRING_LENGTH		(16)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// This is the setting that ultimately controls how many alerts there will be.
// By default, alert piles will use 4k of SRAM.
#define ALERT_PILE_BYTES 			(0x1000)

// 10/11/2013 rmd : Mucho big. All text lines in the tp micro pile.
#define TPMICRO_ALERTS_PILE_BYTES	(0x2000)

// 10/11/2013 rmd : This is the setting that controls how many engineering-type alerts there
// will be. This pile is twice the size of other alert piles, for a total of 8k of SRAM.
// This is largely drive by the fact that most engineering alerts are text strings. This is
// also used to size the alerts display arrays.
#define ENGINEERING_PILE_BYTES		(0x2000)

// 7/26/2013 rmd : !!! NOTE !!! - the present (God awful) design dictates if you change the
// size of a pile you must go and change the associated pre or post string definition. Think
// of that string as a version number. This is because the pile itself is not an integral
// piece of the structure. A very questionable design.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	PRESERVES_CLEANING_OBSOLETE		(0)
#define	PRESERVES_REGISTERING_NEW		(1)
#define	PRESERVES_REFRESHING_EXISTING	(2)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern UNS_8 alerts_pile_user[ ALERT_PILE_BYTES ] __attribute__( BATTERY_BACKED_VARS );

extern UNS_8 alerts_pile_changes[ ALERT_PILE_BYTES ] __attribute__( BATTERY_BACKED_VARS );

extern UNS_8 alerts_pile_tp_micro[ TPMICRO_ALERTS_PILE_BYTES ] __attribute__( BATTERY_BACKED_VARS );

extern UNS_8 alerts_pile_engineering[ ENGINEERING_PILE_BYTES ] __attribute__( BATTERY_BACKED_VARS );

// ----------

typedef struct
{
	// 7/26/2013 rmd : THIS is borderline assinine! This structure does not contain the pile
	// itself. So the usefulness of the pre and post strings is limited. For example I changed
	// the size of the TPMicro pile and the verification test passed. Because both the pre and
	// post strings were intact. Yet the pile itself was no longer valid. My alerts screen was
	// full of repetitive junk. LITTLE DOES ONE KNOW - if you change the size of the alerts pile
	// you must jiggle the pre or post string to cause them to fail.
	//
	// I can't wait to re-do alerts.

	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION. We use the PRE string during startup to
	// see if the battery backed SRAM space for this structure has been initialized. When unit
	// is BORN it will not be equal to the string we are looking for. And we will initialize the
	// space.
	char	verify_string_pre[ BB_PRE_STRING_LENGTH ];

	// ----------
	// The index into the alert_piles array which defines the alert pile by its
	// memory location in SRAM and the size of the array.
	UNS_32	alerts_pile_index;

	// ----------

	// 8/30/2013 rmd : The following are array indicies into the pile array.
	
	// 8/13/2013 rmd : Points to the start of the oldest alert in the pile.
	UNS_32	first;

	// 8/13/2013 rmd : Points to the next available byte in the pile where a new alert would go.
	UNS_32	next;

	// ----------

	UNS_32	count;

	// Until the pile has been tested, do not add any alerts. This must be done
	// very early on since creating mutexes and tasks require memory allocation
	// and, if the allocation fails, there are attempts to make an alert line.
	// These attempts must be denied until the pile has been checked out.
	BOOL_32	ready_to_use_bool;

	// ----------
	
	// CONTROLLER INITIATED SUPPORT

	// 8/13/2013 rmd : The controller initiated support for sending of alerts. This is the
	// indicie of the first byte which needs to be sent. We would send until we hit the 'next'
	// indicie.
	UNS_32		first_to_send;

	// 8/30/2013 rmd : This is to keep a record of what has been sent to the comm server while
	// waiting for the response. The response is our confirmation to go ahead and update the
	// first_byte_to_send_variable.
	UNS_32		pending_first_to_send;

	// 10/9/2013 rmd : To protect an attempted reuse of the preceeding variable from the time
	// the message is created till the time it is actually sent. The CONTROLLER_INITIATED flag
	// about expecting an alerts ACK is set when the message is actually pulled from the list of
	// messages and sent. It is at THAT time we are expecting the ACK. If we set the CI flag
	// here, then we are potentially setting many flags, one for each queued message. And would
	// open the door to receiving ACK's when we truly are not waiting for that particular one.
	// And that is the whole point of the flags system in the first place. This flag is always
	// cleared on powerup to indicate no transaction is underway. It also must be cleared when
	// we are expecting an alerts_response AND we either got a response or the response timer
	// timed out.
	BOOL_32		pending_first_to_send_in_use;

	// ----------

	// 10/9/2013 rmd : A convenience variable to help us set the controller initiated timer
	// duration. When an alert is first start initially set to the default time. If needed an
	// alert construction function may set to an alternate (shorter usually) timeout.
	UNS_32		ci_duration_ms;

	// ----------
	

	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION.

} ALERTS_PILE_STRUCT;


extern ALERTS_PILE_STRUCT alerts_struct_user		__attribute__( BATTERY_BACKED_VARS );
extern ALERTS_PILE_STRUCT alerts_struct_changes		__attribute__( BATTERY_BACKED_VARS );
extern ALERTS_PILE_STRUCT alerts_struct_tp_micro	__attribute__( BATTERY_BACKED_VARS );
extern ALERTS_PILE_STRUCT alerts_struct_engineering	__attribute__( BATTERY_BACKED_VARS );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION. We use the PRE string during startup to
	// see if the battery backed SRAM space for this structure has been initialized. When unit
	// is BORN it will not be equal to the string we are looking for. And we will initialize the
	// space.
	char				verify_string_pre[ BB_PRE_STRING_LENGTH ];

	// ----------

	// 10/19/2012 rmd : Used to determine if the CS3000 main app was updated since last running.
	char				main_app_code_revision_string[ 48 ];
	
	// ----------

	// 4/21/2014 rmd : On a restart we look at and analyze the restart_info very early in the
	// startup sequence. Before the controller_letter is read out from the FLOWSENSE file. We
	// however want to make alert line during this very early analysis so we include the
	// controller index in the restart_info structure. And when the index becomes available load
	// it. We also must load it if the controller letter is changed by the user. On the FIRST
	// startup there is a small hole where the index may be incorrect. But even may not be the
	// case as a default controller index is 0 anyway. So adding a copy of the controller index
	// to the battery backed restart_info struct may indeed be a very good solution.
	UNS_32				controller_index;

	// ----------

	// 10/16/2012 rmd : The flag that any task running can use to see if there is an impending
	// power failure. When the pwr fail int fires this flag is set (true). The only use so far
	// is to signal the FLASH FILE SYSTEM to begin an orderly abort of it's present ongoing
	// operation (if any).
	BOOL_32				SHUTDOWN_impending;
	
	DATE_TIME			shutdown_td;

	// ----------

	UNS_32				shutdown_reason;
	
	// ----------

	// 10/16/2012 rmd : So we know what the flash file system was doing when the pwr fail INT
	// fired. Upon the restart we'll inspect these flags and make alerts if set (true).
	BOOL_32				we_were_writing_spi_flash_data;

	BOOL_32				we_were_reading_spi_flash_data;
	

	// ----------

	// 10/22/2012 rmd : For the data abort, undefined instruction, and prefetch abort handler
	// the variables in this section are filled out.
	BOOL_32				exception_noted;

	DATE_TIME			exception_td;

	char				exception_current_task[ 48 ];

	char				exception_description_string[ 48 ];

	// ----------

	// 10/22/2012 rmd : Support for the assert function.
	BOOL_32				assert_noted;

	DATE_TIME			assert_td;

	// 8/20/2014 rmd : Task names are kept short.
	char				assert_current_task[ 24 ];

	// 8/20/2014 rmd : Had this at 48. Some expressions need more than this when the expression,
	// then filename, then line number are all included.
	char				assert_description_string[ 64 ];

	// ----------
	
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION.

} RESTART_INFORMATION_STRUCT;


extern RESTART_INFORMATION_STRUCT	restart_info __attribute__(BATTERY_BACKED_VARS);

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
typedef struct
{
	// 7/23/2012 rmd : Used to build up rain as the pulses are delivered from the TP Micro. The
	// rain reflected in this is the "massaged" rain collected by the bucket. It is massaged by
	// the hourly cap and the 24 hour cap as well as by the minimum. Aditionally this is the
	// entry put in rain table slot 1 just AFTER the table rolls.
	RAIN_TABLE_ENTRY	rip;

	// ---------------

	// 9/21/2012 rmd : Used to keep the accumulated total within the hourly limit the user has
	// set. At the roll time the hourly total should be added to the final amount. This
	// structures the hourly total such that it is based upon the roll time. For example is the
	// roll time is 15 minutes after the hour then the hourly total will accumulate on the 15
	// minute marks.
	UNS_32				hourly_total_inches_100u;

	// ---------------

	// 9/21/2012 rmd : The true count of the rain pulses reported by the tpmicro designated with
	// the rain bucket. In other words not the 'massaged' amount. Accumulated from the roll time
	// to the roll time. THIS IS NOT USED ANYWHERE.
	UNS_32				roll_to_roll_raw_pulse_count;

	// 9/21/2012 rmd : A count of the rain pulses reported from midnight to midnight. Used to
	// stamp an alert line at mid-night. And shown to the user on a variety of screens as the
	// collected rain so far today.
	UNS_32				midnight_to_midnight_raw_pulse_count;

	// ---------------

	// 6/16/2015 rmd : When the message arrives from the commserver this variable is set to a
	// TWO. And each time we cross the roll time the value is decremented by one. In the
	// irrigation maintenance function the value is tested and if !=0 most irrigation is halted.
	// Also at start times if the value is !=0 most irrigation is prevented.
	//
	// 6/16/2015 rmd : EXPLANATION why this is a count - this has to do with how to clear this
	// flag and the receipt of weather from the commserver. It is very important not to irrigate
	// during or just after a rain event. If we clear this flag was a BOOL and we cleared it
	// when the table rolls we leave a hole between then (8PM) and the time that weather arrives
	// from the commserver. Or more importantly consider if weather never arrives from the
	// commserver. So by making this a count at least from the time we get the shutdown command
	// till when we cross 8PM and for a full 24 more hours we will not irrigate. Even if there
	// is no commserver communication during that time period.
	UNS_32				rain_shutdown_rcvd_from_commserver_uns32;
	
	// ---------------

	// 9/21/2012 rmd : As the pulses come in and we cross the minimum this flips to (true). And
	// this flag is checked in the foal irri maintenance function to stop any ongoing
	// irrigation.
	//
	// 6/16/2015 rmd : NOTE - this flag is also used to qualify the sending of rain indication
	// to the commserver. We need a way to allow retrying sending the rain indication message to
	// the commserver. And we should keep trying until our rain table has rolled. And when the
	// table rolls this flag is cleared. So it has the ability to be dual used.
	BOOL_32				inhibit_irrigation_have_crossed_the_minimum;

	// ---------------

	// 9/21/2012 rmd : Not sure yet why the rip has to be broadcast and up to date at all boxes
	// within the network. I suppose for display issues. I don't have a reason from a functional
	// point of view yet.
	BOOL_32				needs_to_be_broadcast;

} RAIN_STATE;


typedef struct
{
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION. We use the PRE string during startup to
	// see if the battery backed SRAM space for this structure has been initialized. When unit
	// is BORN it will not be equal to the string we are looking for. And we will initialize the
	// space.
	char	verify_string_pre[ BB_PRE_STRING_LENGTH ];

	// ----------

	// 10/28/2014 rmd : I moved these battery backed control variables to here inside the
	// weather structure as a convenient somewhat logically related to weather location to avoid
	// a new set of pre/post strings and init function.
	
	// 10/28/2014 rmd : These day light savings control variables need to be battery backed in
	// order to ensure proper behavior if there were a power failure in that 1am to 3am time
	// frame on the morning day light savings does its thing.
	UNS_16				dls_saved_date;
	
	BOOL_32				dls_eligible_to_fall_back;
	
	BOOL_32				dls_after_fall_back_ignore_sxs;
	
	DATE_TIME			dls_after_fall_when_to_clear_ignore_sxs;
	
	// ----------------

	// 7/23/2012 rmd : Keeps track of the pulses so far today. And contains the status so that
	// if the user edited we set to an 'E'.
	ET_TABLE_ENTRY		et_rip;

	// ----------------

	// 9/21/2012 rmd : This is likely to be shared around the chain as it changes. And contains
	// the state of various items such as have we been polled to and asked to stop irrigating.
	RAIN_STATE			rain;

	// ----------------

	// 9/18/2012 rmd : After the master controller makes a table change and saves the file we
	// sync the table with the other controllers on the chain. Set this (true) to cause the et
	// and rain tables to be distributed in the next outgoing token.
	BOOL_32				sync_the_et_rain_tables;

	BOOL_32				nlu_old_rain_table_sync_variable;

	// ----------------

	// 7/26/2012 rmd : Changing County/City will trigger this to be set. Also when the file
	// holding the et table is first created. And there are other ways (i.e. central). All
	// HISTORICAL values in the table will be overwritten with newly calculated values.
	// Non-historical values are left in-tact.
	BOOL_32				et_table_update_all_historical_values;

	// ----------------

	// 7/23/2012 rmd : Set to (true) to skip the gage reading in the current 24 hour
	// accumulation period. Useful when first bringing a gage online so we don't use a partial
	// days worth of et to control irrigation with. Also may be set (true) when priming the gage
	// as that usually generates extra pulses.
	BOOL_32				dont_use_et_gage_today;

	// 7/23/2012 rmd : The TP Micro makes this determination. And once set (true) we ignore the
	// gage until the user clears this setting.
	BOOL_32				run_away_gage;

	// 7/23/2012 rmd : Used to allow ONE zero gage reading in the table. But not two in a
	// row.
	BOOL_32				yesterdays_ET_was_zero;

	// 9/14/2012 rmd : As pulses come in from the et gage this value is decremented by 1. At the
	// roll time we evaluate to decide to issue an alert about the remaining gage level.
	UNS_32				remaining_gage_pulses;

	// 2/7/2013 ajv : This flag is used by the FOAL master to notify the IRRI
	// machines to send a clear runaway gage command to their respective TP
	// Micro boards.
	BOOL_32				clear_runaway_gage;

	// 6/14/2013 ajv : This flag is used to indicate a rain switch is active
	// somewhere in the chain.
	BOOL_32				rain_switch_active;

	// 6/14/2013 ajv : This flag is used to indicate a freeze switch is active
	// somewhere in the chain.
	BOOL_32				freeze_switch_active;

	// ----------

	// 2/20/2015 rmd : A very special variable. Only used ONCE. When the file system is upgraded
	// to our new DIRECTORY ENTRY format there is a bug. The TPMICRO file still has been written
	// in the old file format. And therefore we endlessly repeat the TPMicro ISP process until
	// the tpmicro code image is re-written to the file system. Setting this true causes that to
	// happen. And this variable is only used that one time.
	//
	// 2/20/2015 rmd : This variable and the associated code within this IF can be REMOVED once
	// all controllers out there have been updated to this new file format. That would be after
	// say MARCH 1, 2015.

	BOOL_32				write_tpmicro_file_in_new_file_format;

	// 2/16/2016 rmd : This flag though stored here in weather_preserves is a GLOBALLY used
	// flag. Indicating there is a change in one or more files that needs to be sent to the
	// central. This flag guarantees a try to send pdata to the server when power returns if the
	// files have been saved (remember counts on bits set in the files themselves).
	BOOL_32				pending_changes_to_send_to_comm_server;

	// ----------
	
	// 6/3/2015 rmd : Some accumulators used to tally commserver data usage. Primarily for the
	// purpose of understanding the impact to AT&T cell billing. There is only one set of these
	// variables - not one set per UPORT. These variables are intended to ONLY be used for the
	// port with the internet enabled device tied to it.
	UNS_32				commserver_monthly_rcvd_bytes;

	UNS_32				commserver_monthly_xmit_bytes;

	UNS_32				commserver_monthly_code_receipt_bytes;
	
	// ----------

	// 5/9/2016 rmd : Sure does seems like a trange place to keep these variables. However it
	// was a matter of convenience. The expansion variables were here and I used them.
	
	// 9/3/2015 rmd : NOTE - these 2 FACTORY_RESET variables are in battery backed to protect
	// their settings until the clearing actions occur. So that if there were a power failure we
	// aren't at risk of say sending the default pdata and over writing the precious pdata the
	// web has in the midst of a panel swap procedure.
	//
	// 9/3/2015 rmd : Using inverse logic for this variable as much as I hate to, so on the
	// update to the code that includes this new variable definition [being already set to
	// (false)] we are allowed to send program data like normal.
	UNS_8				factory_reset_do_not_allow_pdata_to_be_sent;
	
	UNS_8				factory_reset_registration_to_tell_commserver_to_clear_pdata;

	// 5/9/2016 rmd : This variable is also related to the factory reset activity (panel swap or
	// user initiated). After a complete reset, if there is a 2-Wire Terminal installed, we
	// should ask the tpmicro to do a discovery in order to learn what decoders are out there.
	// At the conclusion of the discovery the TPMicro file will be written.
	UNS_8				perform_a_discovery_following_a_FACTORY_reset;

	// ----------
	
	// 10/24/2014 rmd : Added so that if one wanted to add content to this structure they could
	// take from this array without changing the size of the structure. And therefore also not
	// trigger a full initialization of this structure. And likely OTHER battery backed
	// structures as all their locations would have moved. Is kept initialized to 0's so when
	// comes time to use you have a defined starting value the new variable can count on.

	UNS_8				ununsed_uns8_1;
	
	// ----------

	// 8/19/2016 rmd : An accumulator used to tally commserver data usage. THIS IS STATUS SCREEN
	// UPDATES ONLY (that could be changed to include all mobile commands in and out). Primarily
	// for the purpose of understanding the impact to AT&T cell billing. There is only one of
	// these variables - not one set per UPORT. This variable reflects the data ONLY through the
	// port with the internet enabled device tied to it. Port A of the A controller!
	UNS_32				commserver_monthly_mobile_status_updates_bytes;

	// ----------

	// 5/23/2017 rmd : These two are set upon the restart following the commserver having
	// delivering one or both binaries.
	BOOL_32				hub_needs_to_distribute_tpmicro_binary;

	BOOL_32				hub_needs_to_distribute_main_binary;
	
	// ----------
	
	UNS_32				expansion[ 23 ];
	
	// ----------
	
	// 8/6/2015 rmd : IF THIS STRUCTURE SIZE IS CHANGED THE PRE STRING MUST ALSO BE CHANGED TO
	// TRIGGER A STRUCTURE RE-INITIALIZATION.

} WEATHER_PRESERVES_STRUCT;

extern WEATHER_PRESERVES_STRUCT		weather_preserves __attribute__( BATTERY_BACKED_VARS );



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define		STATION_PRESERVES_FLOW_STATUS_normal_flow		(0)

#define		STATION_PRESERVES_FLOW_STATUS_high_flow			(1)

#define		STATION_PRESERVES_FLOW_STATUS_lo_flow			(2)

#define		STATION_PRESERVES_FLOW_STATUS_no_flow			(3)

// -----------------------

#define		STATION_PRESERVES_I_STATUS_no_error								(0)

#define		STATION_PRESERVES_I_STATUS_conventional_no_current				(1)

#define		STATION_PRESERVES_I_STATUS_conventional_low_current				(2)

#define		STATION_PRESERVES_I_STATUS_conventional_high_current			(3)

#define		STATION_PRESERVES_I_STATUS_conventional_station_short			(4)

#define		STATION_PRESERVES_I_STATUS_conventional_mv_short				(4)

#define		STATION_PRESERVES_I_STATUS_conventional_pump_short				(4)

#define		STATION_PRESERVES_I_STATUS_station_decoder_no_current			(5)

#define		STATION_PRESERVES_I_STATUS_station_decoder_low_current			(6)

#define		STATION_PRESERVES_I_STATUS_station_decoder_high_current			(7)

#define		STATION_PRESERVES_I_STATUS_station_decoder_voltage_too_low		(8)

#define		STATION_PRESERVES_I_STATUS_station_decoder_short				(9)

// 11/6/2014 rmd : Note - the next 4 reasons can only be set if the station was in the list
// when the event occurred. And some of these events can indeed happen with no stations in
// the list. For example the INOP decoder can be detected during the periodic stat command.
// And that happens once every 15 minutes. And an activity like MVOR could cause the event
// with no stations in thelist. In both cases an alert line is generated but no status
// screen indication will result. (That is what these flags do .... drive the status
// screen).
#define		STATION_PRESERVES_I_STATUS_station_decoder_inop					(10)

#define		STATION_PRESERVES_I_STATUS_poc_decoder_voltage_too_low			(11)

#define		STATION_PRESERVES_I_STATUS_poc_decoder_short					(12)

#define		STATION_PRESERVES_I_STATUS_poc_decoder_inop						(13)

// ----------

// 6/13/2012 rmd : Based on the 4 bits allocated to this number.
#define		STATION_PRESERVES_MAXIMUM_FLOW_CHECK_COUNT	(15)

typedef union
{
	// 5/1/2012 rmd : Note - the minimum size using this union construct appears to be 4 bytes.
	// Unless we use the packed attribute.
	UNS_16	overall_size;

	struct
	{
		// NOTE: In GCC this is the Least Significant Bit (Little Endian)

		// ------------------------

		// For each station keep track of how many cycles have run with flow checking enabled - what
		// we are concerned with is the first 6 cycles during which we will actually not test flow
		// but WILL learn the derate table - this is part of the support of automatic derate table
		// learning. 4 bits so can count to 15.
		//
		// TODO : this needs to be reset to 0 if the comm mngr contact table changes. AT least for
		// the indicies for the entry that changed.
		//
		// TODO : this flow_check_count may need to be saved in a file. Tied to the saving of the
		// derate table. Else potentially something like a code update could cause the count to
		// start over if this structure was reset. Hmmmm.
		//
		// 6/13/2012 rmd : See the define above limiting the count to 15. Based on the 4 bits
		// allocated to this number.
		UNS_32			flow_check_station_cycles_count					: 4;

		// ------------------------

		// 6/27/2012 rmd : Think of this as an IRRI side indicator. It is managed via the
		// communication. And really is for display purposes only. The defines are above this
		// structure definition. To be preserved during a power fail!
		UNS_32			flow_status										: 2;

		// ------------------------

		// 11/6/2014 rmd : For display purposes only. A variety of status indications. Really all to
		// do with electrical issues. We capture just the last event that caused us to write to
		// these bits. Meaning over-writing any prior. But as that turns out not such a bad thing as
		// generally only ONE of these conditions can exist at a time. To be preserved during a
		// power fail! And cleared when an attempt is made to turn ON the station.
		UNS_32			i_status										: 4;

		// ------------------------

		BITFIELD_BOOL		skip_irrigation_due_to_manual_NOW				: 1;

		BITFIELD_BOOL		skip_irrigation_due_to_calendar_NOW				: 1;

		// To control the weighting of orphans...orphans are ONLY generated during programmed
		// irrigation and are given priority during the next Programmed Irrigation.
		BITFIELD_BOOL		did_not_irrigate_last_time						: 1;

		// ------------------------

		// 7/19/2012 rmd : We use this flag to know which report_data records to close and write to
		// the station report data file. The flag is set during the routine second by second check
		// for a start time. During which we traverse the entire list of stations. When we close a
		// record (cause this flag is set at the roll time), we clear this flag. If the station is
		// in use at the next second the flag will once again be set. This action serves to save
		// records to the file that were at one time in use during the prior 24hr period.
		BITFIELD_BOOL		station_report_data_record_is_in_use   			: 1;

		// ----------
		
		// 8/11/2015 rmd : When the battery backed station history rip is closed and added to the
		// completed records structure in SDRAM we set this flag. When a copy of the SDRAM structure
		// of completed records is taken to be saved we clear this flag in all records. On a code
		// startup any records with this flag set are added to the completed records structure.
		BITFIELD_BOOL		station_history_rip_needs_to_be_saved			: 1;
		
		// ----------
		
		// 8/8/2012 rmd : Set (true) when a DIFFERENT reading is written to
		// last_measured_current_ma. Not set (true) each time a reading is written. As we would like
		// to minimize the network traffic. If the value hasn't changed no need to distribute again.
		BITFIELD_BOOL		distribute_last_measured_current_ma				: 1;

		// NOTE: This is the MS bit in GCC.

	} __attribute__((packed));  // this packed keeps the bit-field struct at 2-bytes - instead of 4

} __attribute__((packed)) STATION_PRESERVES_BIT_FIELD;  // this packed keeps the union at 2-bytes - instead of 4




// 2011.09.02 rmd : This is an array of particular data that must be first of all preserved
// throughout a power failure. We could have attached it to the station file list but my
// first thought was since it changes often - typically once per day - we'd be needing to
// save that file often. Maybe that is not such a big deal? Secondly these values -
// particularly the no water days need to be sent to the slaves. So that in case the chain
// goes down those controllers do not autonomously irrigate when the user has scheduled no
// water days! Hmmmm. Why can't the scheduled now water day settings be shared instead?????
// Maybe they can as an alternative. RAIN would be another item one might want to have an
// accurate copy of at the slaves. Needs to be further thought out!

// The indexing to get at a particular item requires the stations serial number and station
// number. There is a translation from serial number to controller number in the chain. The
// comm_mngr can do that. And then we index from there to the station number. Go see and
// you'll see.

typedef struct
{
	// 2011.09.23 rmd : NOTE : This structure is oraganized as you see to be space efficient.
	// The DATE_TIME structure is broken up for this purpose.

	// ---------------------------------

	// (rip = Records In Process)

	// 7/19/2012 rmd : These records are rolled at the station start time. Only valid at the
	// master.
	STATION_HISTORY_RECORD			station_history_rip;

	// 7/19/2012 rmd : And these are rolled at the roll time specified within the completed
	// recrods structure. Only valid at the master.
	STATION_REPORT_DATA_RECORD		station_report_data_rip;

	// ---------------------------------

	// 2011.09.08 rmd : This is used to EXTEND the effective period past the last start time
	// encountered. To the first 5AM crossing after that.
	UNS_32			skip_irrigation_till_due_to_manual_NOW_time;

	// 2011.09.08 rmd : This would be dynamically set by reading the calendar scheduled no water
	// days. The 1Hz reading of the calendar directly sets this variable?
	UNS_32			skip_irrigation_till_due_to_calendar_NOW_time;

	// ---------------------------------

	// 2011.09.08 rmd : This is used to EXTEND the effective period past the last start time
	// encountered. To the first 5AM crossing after that.
	UNS_16			skip_irrigation_till_due_to_manual_NOW_date;

	// 2011.09.08 rmd : This would be dynamically set by reading the calendar scheduled no water
	// days. The 1Hz reading of the calendar directly sets this variable?
	UNS_16			skip_irrigation_till_due_to_calendar_NOW_date;

	// ---------------------------------

	// 2011.09.22 rmd : Remember there are 760 of these records so store as a short to conserve
	// battery backed SRAM. And as a short it can safely be stored in seconds because this value
	// will never exceed 3.0 minutes as it's only set when a cycle is 3-minutes or less.
	UNS_16			left_over_irrigation_seconds;

	// Kept as a positive number. But it does represent in a sense negative time. It is time
	// that we will not be irrigating! (Used to be HoldOver in the 2000e.)
	UNS_16			rain_minutes_10u;

	// ---------------------------------

	// 5/6/2014 rmd : This bit-field is an UNS_16.
	STATION_PRESERVES_BIT_FIELD		spbf;

	// ---------------------------------

	// 8/8/2012 rmd : As reported by the tpmicro. This needs to be distributed to the master
	// controller. Somehow. TBD!! TODO   Units are milli-amps. Stored as an UNS_16 for space
	// savings in the precious battery backed SRAM. Comes from the tpmicro as an UNS_32 but is
	// converted as received.
	UNS_16			last_measured_current_ma;

	// ----------

	// 10/29/2014 rmd : Have decided NOT to add any EXPANSION space to this structure. Since
	// each byte added here is replicated 1536 times in the station_preserves array! I think it
	// is a space prohibited feature.

	// ----------
	
	// 5/6/2014 rmd : Present size is 128 bytes. And I believe that there are at least 3 unused
	// bytes within that due to compiler 32-bit alignment rules.
	// 
	// 7/22/2015 ajv : We've now added the moisture_balance value to the STATION_HISTORY_RECORD
	// structure, which you'd think would increase the size by 4-bytes. However, we also moved
	// distribute_last_measured_current_ma into the STATION_PRESERVES_BIT_FIELD, so it's a push.
	// We're still at 128-bytes, but we no longer have any compiler-added padding.

	// ----------
	

	// ----------
	
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION.

} STATION_PRESERVES_RECORD;


// 2011.09.02 rmd : This is NOT the maximum number of stations allowed within a network. It
// is TWICE that value of 768. We have decided to take the easy way out for storing this
// data. And just make an array of 128 stations per box times the number of boxes allowed in
// a chain (128 * 12). That makes the indexing easy. Otherwise we'd have to have some other
// scheme.
#define STATION_PRESERVE_ARRAY_SIZE		(MAX_CHAIN_LENGTH * MAX_STATIONS_PER_CONTROLLER)

typedef struct
{
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION. We use the PRE string during startup to
	// see if the battery backed SRAM space for this structure has been initialized. When unit
	// is BORN it will not be equal to the string we are looking for. And we will initialize the
	// space.
	char	verify_string_pre[ BB_PRE_STRING_LENGTH ];

	// ----------

	STATION_PRESERVES_RECORD	sps[ STATION_PRESERVE_ARRAY_SIZE ];

	// ----------
	
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION.

} STATION_PRESERVES_STRUCT;


extern STATION_PRESERVES_STRUCT	station_preserves __attribute__( BATTERY_BACKED_VARS );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*  														  */
/*  	S Y S T E M 										  */
/*  														  */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifdef USE_ENGINEERING_VALUES

	#define		MLB_AND_FLOW_CHECKING_MVJO_BLOCKOUT_SECONDS				(20)

	#define		MLB_JUST_STOPPED_IRRIGATING_CHECKING_BLOCKOUT_SECONDS	(20)

#else

	// 5/3/2012 rmd : MVJO block out time has traditionally been 150 seconds. For many years in
	// all prior controllers. And apparently works well. So leave at 150 seconds.
	#define		MLB_AND_FLOW_CHECKING_MVJO_BLOCKOUT_SECONDS				(150)

	// 5/3/2012 rmd : When the last valve turns off we block MLB checking against the
	// non-irrigation number. Till the system has had time to settle. And all valves are truly
	// closed.
	#define		MLB_JUST_STOPPED_IRRIGATING_CHECKING_BLOCKOUT_SECONDS	(150)

#endif

// -------------------------------

// Do not change this. We use this value when composing the 20 second averages used for the
// stability test and the value used during flow checking.
#define		SYSTEM_5_SECOND_AVERAGES_KEPT	(20)

// If you change how many stability averages are kept you need to review the math used when
// determining system flow stability.
#define		SYSTEM_STABILITY_AVERAGES_KEPT	(30)

// -------------------------------

#define		TURNING_ON_NON_PUMP_VALVES		(false)

#define		TURNING_ON_PUMP_VALVES			(true)

// -------------------------------

// 5/25/2012 rmd : The size of the derate table has grown several times. When I first did
// FlowSense it was penciled in at around the 600 cell range. That quickly proved too small.
// Was doubled to 1350 cells where it stands today in the ET2000e. Yet that requires
// adjustment in enough of the systems out there to warrant another size increase. Our goal
// being that the default axis settings work 99% of the time. As we really do not want the
// subject of the derate table to even come up with customers. Competitors are trying to
// copy (and some know about it).

// For the derate table to work really well you do not want cells to be double used for
// different groups of valves. A slot size of 1 gpm is a very reasonable gpm slot size. We
// do not use the derate table with 1 valve ON. FLow checking does not derate in that case.
// So if we use 1 gpm and 500 gpm max gal and 10 valves on we need 500 * (10-1) = 4500
// cells. And that should cover us 99% of the time.

// Do not define this as gpm-by-station array cause the design supports re-dimesioning the
// axis. And that is controlled with the system settings. We need to make this a pile of
// 4500 floats. And we index into that array based upon our axis settings.

// 6/22/2012 rmd : To save space we have decided to store as a 10U UNS_16. Just be careful
// when updating the cell value to perform the floating point math to calc the new value.
#define DERATE_TABLE_CELLS	(4500)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 5/8/2012 rmd : When a MLB is detected, if this flag is not set, the measured and limit
	// values are recorded and this flag is set. This NEEDS TO BE battery backed so that it
	// persists through a power fail. When set the MLB information is part of each token.
	//
	// When set station belonging to this system are prohibited from being added to the
	// foal_irri irrigation list.
	//
	// The specific MLB action on the FOAL side is to remove all ilcs for this system from the
	// main list. And to tell each irri machine while the condition exists. On the IRRI side the
	// rcvd MLB indication causes list removal for all stations in the specified system. And
	// display activity will result.
	//
	// This flag is set FALSE in startup only for a complete reset (protected ram var)
	// - on normal program restart it is left alone.
	//
	// This flag DOES NOT prevent continued checking for MLB - that continues but does not
	// update the specifics since the first detection.
	UNS_8		there_was_a_MLB_during_irrigation;

	UNS_8		there_was_a_MLB_during_mvor_closed;

	UNS_8		there_was_a_MLB_during_all_other_times;

	// ---------------------

	UNS_8		dummy_byte;

	// ---------------------

	// 608.a rmd : Moved these from the foalflow definition - they are used to show the user
	// only. Each irri machine picks the data up from the foal master.
	UNS_16		mlb_measured_during_irrigation_gpm;
	UNS_16		mlb_limit_during_irrigation_gpm;

	UNS_16		mlb_measured_during_mvor_closed_gpm;
	UNS_16		mlb_limit_during_mvor_closed_gpm;

	UNS_16		mlb_measured_during_all_other_times_gpm;
	UNS_16		mlb_limit_during_all_other_times_gpm;

} SYSTEM_MAINLINE_BREAK_RECORD;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// associated with above code not for MSC_VER
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 9/4/2012 rmd : With the introduction of a pile of SYSTEM report lines we need to include
	// the gid of the system this record 'belongs' to.
	UNS_32			system_gid;

	// ---------------------

	// 7/24/2012 rmd : The record start time could be eliminated. It is only different from the
	// end time for one record. When the system_preserve entry is synchronized with the system
	// file list. Otherwise both the record start and end times are the same and are equal to
	// the roll time. AHHHHH. What about when there is a power fail that lasts for days? In that
	// case the record would span days.
	DATE_TIME		start_dt;

	DATE_TIME		no_longer_used_end_dt;

	// ---------------------

	// 7/20/2012 rmd : Raw rainfall TOTAL over the accumulation period. Which defaults mid-night
	// to mid-night. But the user can change. We also keep a mid-night to mid-night accumulator
	// whose value is posted into an alert line each mid-night.
	UNS_32			rainfall_raw_total_100u;

	// ---------------------

	// When it comes to the seconds accumulators an UNS_16 is only good for 18.2 hours. So the
	// seconds accumulators must be UNS_32 to hold the entire potential 24 hour count.
	UNS_32			rre_seconds;						// Radio Remote usage
	float			rre_gallons_fl;

	UNS_32			test_seconds;						// Test
	float			test_gallons_fl;

	UNS_32			walk_thru_seconds;					// Walk Thru
	float			walk_thru_gallons_fl;

	UNS_32			manual_seconds;						// Manual & Manual Sequence
	float			manual_gallons_fl;

	UNS_32			manual_program_seconds;				// Manual Programs
	float			manual_program_gallons_fl;

	UNS_32			programmed_irrigation_seconds;		// Programmed Irrigation
	float			programmed_irrigation_gallons_fl;

	UNS_32			non_controller_seconds;				// Non-Controller
	float			non_controller_gallons_fl;

	// ----------
	
	// 10/29/2014 rmd : NOTE - this structure is passed to the comm_server. Therefore if you
	// change it there are implications up the line. Do not change without this in mind.

} SYSTEM_REPORT_RECORD;

// 3/30/2016 skc : This structure is saved as part of budget reports.
typedef struct
{
	UNS_32 			system_gid;

	UNS_32 			in_use;
	UNS_32 			mode;	// Alerts / Automatic

	UNS_32			start_date;
	UNS_32 			end_date;
	UNS_32 			meter_read_time;

	// 3/30/2016 skc : The measured use + the predicted to end of budget period.
	UNS_32			predicted_use_to_end_of_period;

	// 3/30/2016 skc : Keep track of water saved.
	UNS_32 			reduction_gallons;

	// ----------
	
	// 4/21/2016 skc : Save the final budget calculation value in the report. [(B-U)/Vp]
	//
	// 5/17/2016 rmd : Where B is the budgeted amount, U is the used, and Vp is the predicted
	// use to the end of the period. The range on this variable is apparently 0 to a big number.
	// And reflects how much of your present schedule could be applied without going over budget.
	// So 0 means NONE, 1.0 means all of it, 1.2 would mean 20% more than currently scheduled.
	//
	// 7/8/2016 skc : Ratio is (actual/requested) irrigation time for the system.

	// Is this still driven by the formula above?
	// Example values to expect are?
	// What is the possible range?
	// What happens with this value during the billing cycle? What should we expect?

	float			ratio;

	// ----------

	// 3/28/2016 skc : A flag set when this is the final record for the period. 
	UNS_32			closing_record_for_the_period;

	// ----------

	// 10/29/2014 rmd : NOTE - this structure is passed to the comm_server. Therefore if you
	// change it there are implications up the line. Do not change without this in mind.

} SYSTEM_BUDGET_REPORT_RECORD;

// 3/30/2016 skc : Each system has budget related variables to track.
typedef struct
{
	SYSTEM_BUDGET_REPORT_RECORD rip;

	// 6/8/2016 skc : Was display units of Gallons or HCF
	UNS_32 			unused_0; 

	// 3/30/2016 skc : Helps prevent excessive churn during date changes.
	UNS_32 			last_rollover_day;

} BY_SYSTEM_BUDGET_RECORD;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// A value of 0 means it ain't in use. We look for a matching gid or find an struct not in
	// use and take it. When the roll time hits we close the report record and release the
	// structure by setting the system_gid to 0. We we try to add to a record or set a value if
	// we do not find a matching gid we make such a record and zero out the entries (picture
	// trying to add to an accumulator and we cannot find the system gid match...so we take the
	// next available and 0 the record then add to the accumulator.
	UNS_32					system_gid;

	// ----------------------------------------

	// One of these records. The record in process for this system.
	SYSTEM_REPORT_RECORD	rip;

	// ----------------------------------------

	// 4/16/2012 rmd : Doesn't need to be battery backed but this is the logical place to hold
	// this variable. With the other list maintenance type variables. So far this one is only
	// used during the STOP key processing. And with its introduction I am going to take the
	// SYSTEM_PRESERVES mutex in the list maintenance function. Because this variable is
	// developed in the list maintenance function as we traverse the irrigation list. And when
	// we go to use it we should also be holding the system_preserves mutex.
	UNS_32		highest_reason_in_list;

	// ----------------------------------------

	// 2012.01.24 rmd : ufim = USED_FOR_IRRIGATION_MANAGEMENT. Be careful using these variables
	// outside of the foal irri maintenance function. In general they are not designed to be
	// used that way. However as time has gone on I have begun such. In particular cases. And
	// it's safe to do as long as you consider MUTEX use for protection. And study perhaps how
	// the variable is assigned it's value.
	// 5/30/2012 rmd : Now that the system_preserves MUTEX is taken for the duration of the
	// foal_irri maintenance function it is perfectly safe to use ALL these system_preserves
	// variables out side of the foal_irri maintenance function. As long as you, the user of
	// such, also take the system_preserves MUTEX. HOWEVER I would always recommend studying the
	// use and development of these system values prior to use outside of the foal_irri
	// maintenance function.

	// ----------------------------------------

	// This is used when deciding to switch from turnon pump to non-pump.
	UNS_32		ufim_valves_in_the_list_for_this_system;

	// 2011.11.03 rmd : And now irrigation support. These variables are used via the foal
	// irrigation maintenance function called when this controllers comm_mngr is making tokens.
	// These are the by system stats about what's ON. Used to decide what can come ON.
	//
	// Their names begin with 'ufim' to leave no doubt these are not normally editable
	// variables. They are 'used for irrigation maintenance'.

	/** 2011.11.03 rmd : How many valves can we turn ON. This value is developed by repeatively
	    taking the minimum number from the on-at-a-time group the station belongs to. To be
	    reset to the largest value possible UNS_32_MAX. */
	UNS_32		ufim_maximum_valves_in_system_we_can_have_ON_now;

	// ------------------

	// 6/26/2015 mpd : FogBugz #3113. Renamed "number_of_valves_ON" to "system_master_number_of_valves_ON". Also added
	// the IRRI side variable "system_rcvd_most_recent_number_of_valves_ON" at the end of this structure.

	// 6/15/2012 rmd : Broadly used by several tasks. Not set to 0 on each call to the foal irri
	// list maintenance function. Though use requires possesion of the system_preserves
	// MUTEX.
	UNS_32		system_master_number_of_valves_ON;

	// ------------------

	// ET2000e note : This was introduced to handle the unique situation of when there are
	// non-pump valves irrigating with both pump and non-pump in the list. If we reached the
	// point where all valves turned off and both pump and no-pump are eligible to come on we
	// would want to continue with the non-pumpers for continuity sakes. We use this variable to
	// override the pump priority in the list for ONE pass through the turn on loop. Without
	// this variable we would switch to pumpers even though we were just doing non-pumpers. This
	// may not be a problem but doesn't seem like the most expected behavior one would expect.
	//
	// ET2000e note : This variable protects us from jumping to pump valves when we are
	// irrigating non-pump valves just cause the pumper are in the list and ready and the valves
	// on dropped to zero. It may have dropped to zero for just the brief instant between valve
	// transitions. This allows us to continue with non-pumpers. THE FLIP SIDE OF THIS is if we
	// are irrigating pumpers and the on count drops to zero. If there are any pumpers in the
	// list we will continue with them due to the priority of the list organization - pumpers
	// are looked at first.
	//
	// ET2000e note : I'm actually not sure why we need this variable anymore...the list
	// organization should cover the pump non-pump segregation???? but i'm chicken to remove it
	//
	// 2011.11.09 rmd : Note this variable tracking pump or non-pump turn ON mode is meant to be
	// preserved from call to call of the foal irrigation maintenance function. Be careful not
	// to reset it at the report roll time. We DO NOT want to reset this variable during that
	// activity.
	BOOL_32		ufim_what_are_we_turning_on_b;

	// --------------------------

	// 5/30/2012 rmd : This value is computed under protection of the system_preserves MUTEX and
	// is available for the COMM_MNGR to distribute via the token if desired. It is equal to the
	// sum of the expecteds of those valves ON in the system (flow meter or no flow meter within
	// the system). When 'tallys_driven_with_expecteds' is true then this is the value to
	// display as the current flow rate. The current flow rate is the sum of the expecteds for
	// those ON.
	UNS_32		ufim_expected_flow_rate_for_those_ON;

	// --------------------------

	// 5/30/2012 rmd : This variable is used during the turn ON decision and the also during
	// flow checking. It's accuracy can lag certain actions such as the STOP key or a mainline
	// break. And that may be a concern during flow checking. But I think not as all the valves
	// are OFF for those events. For its use during flow checking see the notes that follow:

	// <notes regarding use during flow checking> Here is a scenario that occurs with the new
	// flow table learning. Two valves learn all the way so they are checking. If they are on
	// together and we get a flow error both go on the problem list - the problem comes about
	// when the valve that we just saw a possible problem with and is now on the problem list
	// turns back on it may be joined with other valves that cause them to use a cell not yet
	// learned! and therefore the group will not check flow. We have decided that when one of
	// the valves ON is from the problem list that causes us to disregard the learning
	// requirement of the cell in order to test flow. We can assume the valve has run enough
	// cycles as it was involved in a flow test to begin with.
	//
	// This was argued as no worse off than we were before the flow table learning was
	// introduced and we ran that way for two years and did not have any problems.
	//
	// The question comes in that if the flow passes should we then learn the derate table if it
	// hasn't learned its iterations - probably so. We are trying to characterize the system in
	// the derate table and any opportunities we have to record more about the flow behaviour we
	// should. One concern is that it didn't fail cause it wasn't learned and now we are
	// learning under broken system conditions. Again not worse than we did it without flow
	// table learning.
	BOOL_32		ufim_one_ON_from_the_problem_list_b;

	// --------------------------

	UNS_32		ufim_highest_priority_pump_waiting;

	UNS_32		ufim_highest_priority_non_pump_waiting;

	// ----------
	
	// 4/1/2016 rmd : NOTE - this flag is NOT USED in the CS3000 code. The is no positive HO in
	// the CS3000 product like there is in the 2000e. The 2000e comment follows below.
	//
	// In order to complete regular irrigation before hold-over is started we need to KNOW if
	// there are any with programmed time that are ready to go - if so we must skip over
	// stations with only HO to irrigate.
	BOOL_32		ufim_list_contains_waiting_programmed_irrigation_b;

	// ----------
	
	BOOL_32		ufim_list_contains_waiting_pump_valves_b;

	BOOL_32		ufim_list_contains_waiting_non_pump_valves_b;

	// --------------------------

	UNS_32		ufim_highest_reason_of_OFF_valve_to_set_expected;

	BOOL_32		ufim_one_ON_to_set_expected_b;

	BOOL_32		ufim_list_contains_some_to_setex_that_are_not_ON_b;

	// 608.rmd to support the new rre valve mode where we want to have only a single valve ON in
	// the whole system - including no other rre valves - the result when one of these types
	// hits the list is to pause everything
	BOOL_32		ufim_one_RRE_ON_to_set_expected_b;

	BOOL_32		ufim_list_contains_some_RRE_to_setex_that_are_not_ON_b;


	BOOL_32		ufim_list_contains_some_pump_to_setex_that_are_not_ON_b;

	BOOL_32		ufim_list_contains_some_non_pump_to_setex_that_are_not_ON_b;


	// 2012.01.05 rmd : Old 2000e note. Not sure if it is even applicable. As I would think the
	// Walk-Thru valve would SMSThis can happen if we're doing say a SMS with non-pump valves
	// and someone walks up to the controller and pushes TEST for a PUMP valve. The TEST will
	// come on and the pump will be running. When the current SMS valves expire only PUMP SMS
	// valves can come on (can replace SMS with ProgrammedIrrigation in this story). This
	// variable is kept up to date in the maintain irri list function of the FOAL machine.
	BOOL_32		ufim_there_is_a_PUMP_mix_condition_b;


	BOOL_32		ufim_based_on_reason_in_list_at_least_one_valve_is_ON_that_wants_to_check_flow;

	BOOL_32		ufim_the_valves_ON_meet_the_flow_checking_cycles_requirement;


	// 5/30/2012 rmd : Used to decide if we should throw the "mix of flow groups on" error
	// message. Also used during flow checking. If more than ONE valve ON for test we do not
	// test flow. Not sure why. But the 2000e works this way. This variable is initialized to
	// zero when irrigation restarts (program startup, scan, etc..) and dynamically maintained
	// as turn ON and OFF events occur.
	UNS_32		ufim_number_ON_during_test;

	// ------------------

	// 8/8/2012 rmd : When (true) means that the pump output for all the POCs attached to this
	// system should be energized. This information is communicated to all the IRRI machines.
	// Which in-turn use it to inform the POCs attached to this system that are owned by their
	// respective box. When referring to this value one must possess the system_preserves MUTEX.
	// As the foal maintenance function manipulates the value in such a way there are moments of
	// time when it is invalid!
	BOOL_32   	ufim_stations_ON_with_the_pump_b;

	BOOL_32		ufim_stations_ON_without_the_pump_b;

	// ------------------

	UNS_32		ufim_highest_reason_in_list_available_to_turn_ON;

	UNS_32		ufim_highest_pump_reason_in_list_available_to_turn_ON;

	UNS_32		ufim_highest_non_pump_reason_in_list_available_to_turn_ON;


	// Now for the variable that controls which segregated group we are turning ON - segregated
	// by if we are actively testing flow for the valves ON...this grouping is controlled by the
	// ALERT ACTIONS and the exclusion bit setting and if the reason in the list dictates we
	// will not measure flow.
	UNS_32		ufim_flow_check_group_count_of_ON[ 4 ];

	// 2/20/2013 rmd : There are various reasons we block flow checking. This timer is loaded up
	// at two times. First when valves are opened. At that point it takes on the line fill
	// value. Another time we would load up this timer is when a valve is closed with the slow
	// closing valve value or a minimum of 15 seconds. Besides allowing the valve to close, the
	// valve close block out is important when the last valve turns off. There is a period of
	// time when no valves are ON yet the master valve is open. And flow checking would be
	// allowed as the Master Valve just closed block out isn't populated till the MV anti
	// chatter period has expired.
	UNS_32		flow_checking_block_out_remaining_seconds;

	// 2/20/2013 rmd : This timer is used to help slow closing valves close. By blocking the
	// turn ON of the next valve until this timer counts down to zero. The default is 0 but the
	// user can set. When any valve closes it is reloaded. And literally blocks subsequent
	// valves from energizing till counts down to zero.
	UNS_32		inhibit_next_turn_ON_remaining_seconds;

	// ----------------------------------------

	// This value is ONLY valid at the MASTER. And is not what you display. For each system the
	// most recent value in this array is distributed with each token. Each second the 20 values
	// are averaged to create another weighted stability average. (This does NOT need to be
	// battery backed).
	float		system_master_5_second_averages_ring[ SYSTEM_5_SECOND_AVERAGES_KEPT ];

	// 2012.01.24 rmd : This is ONLY valid at the master. And is actually the last entry into
	// the above array. To avoid us having to keep taking the index and figuring out the pior
	// index value (DEC_RING_BUFFER_INDEX) where ever we want this value, we just assign it when
	// made.
	float		system_master_most_recent_5_second_average;

	// Index to where the next calculated 5 seconds average goes.
	UNS_32		system_master_5_sec_avgs_next_index;

	// This is the value pulled out of the token and made available for display. Think of it as
	// an IRRI side variable. (This does NOT actually need to be battery backed).
	float		system_rcvd_most_recent_token_5_second_average;

	// 6/3/2014 rmd : This is ONLY valid at the master. And is not a user variable. It really is
	// simply a means to exchange information between the function that computes the system
	// average flow rates and the report accumulator function that drives all the accumulators.
	float		accumulated_gallons_for_accumulators_foal;

	// ----------------------------------------

	// This is the array of weighted averages used to evaluate if we have stability. Only valid
	// at the MASTER. (This does NOT actually need to be battery backed).
	float		system_stability_averages_ring[ SYSTEM_STABILITY_AVERAGES_KEPT ];

	// This index should be thought of as always pointing to the last stability average that was
	// created. Its value is NOT the index of the NEXT stability average to compute. It's value
	// is the index of the LAST stability average computed. So during flow checking one can use
	// it directly to get at the stability average to use during the flow testing (of course
	// taking the system_preserves MUTEX first).
	UNS_32		stability_avgs_index_of_last_computed;

	// ----------------------------------------

	// This group is used to control the recording of the residual flow from the last valve to
	// turn off. We divert it away from non-controller even though no valves are ON - this is to
	// alleviate the funny of always having non-controller accumulate as the last valve is
	// turning off. This group does not need to be battery backed. It should be intialized upon
	// program startup.
	//
	// 4/4/2018 rmd : I DON'T THINK THESE ARE USED!!
	UNS_32		last_off__station_number_0;

	UNS_32		last_off__reason_in_list;

	// ----------------------------------------

	// 9/24/2015 ajv : FOAL-side remaining seconds. Valid at the MASTER only. Synced to the
	// slaves for display via the associated delivered_MVOR_remaining_seconds.
	UNS_32		MVOR_remaining_seconds;

	// ----------------------------------------

	// 8/8/2012 rmd : The ANTI_CHATTER timers! Used to manage MV and PUMP behaviours through
	// valve transitions. We do not want to respond to temporary states when no valves are ON.
	// These occur between tokens as part of regular list maintenance. Effects such as opening
	// and closing the MV (MV chatter) are very bad. So this timer is used to blur the
	// transition line. It intentionally counts down only when the list maintenance function can
	// run. Meaning the network is not busy turning on or off valves. So during periods of valve
	// stability, when all the valves are OFF, these count down. Well not exactly the pump
	// counts down when there are no pump valves ON.
	UNS_32		transition_timer_all_stations_are_OFF;

	UNS_32		transition_timer_all_pump_valves_are_OFF;

	// ----------------------------------------

	// 5/3/2012 rmd : When the MVJO contains a time both MLB checking and valve level flow
	// checking are inhibited. At program start the countdown time is set to 0. When a
	// transition to open any MV in this system takes place the timer is loaded with the block
	// out count - 150 seconds traditionally. The test if it is okay to check for MLB counts
	// down the time.
	UNS_32		timer_MVJO_flow_checking_blockout_seconds_remaining;

	// ----------------------------------------

	// 5/3/2012 rmd : the MLB_JustStoppedIrrigating subsystem works as follows: 1) on power up
	// MLB_JustStoppedIrrigating is set FALSE 2) The foal machine blocks out MLB checking when
	// the count in the irrigation ON list transitions from >0 to 0. This transition signifies
	// an end of irrigation and we don't want to start checking against the NonIrrigation MLB
	// for 150 seconds. When we are irrigating we stop the timer and set this FALSE. This is
	// done in the top of the check for MLB.
	UNS_32  	timer_MLB_just_stopped_irrigating_blockout_seconds_remaining;

	// ----------------------------------------

	// 2012.01.31 rmd : And here we have a bunch of vitals that are used during the system
	// operation AND are also desirable to send to each slave in the network. We've wrapped them
	// up into a bit field for tranfer efficiency.
	SYSTEM_BIT_FIELD_STRUCT		sbf;

	// ----------------------------------------

	// 4/30/2012 rmd : The system by system flow recording control structure. The actual flow
	// recording memory is in SDRAM and comes from our managed memory pool. This flow recording
	// control structure does NOT need to be battery backed but is logically located here for
	// convienence.
	//
	// 10/2/2014 rmd : NO LONGER TRUE. It contains the controller initiated control variables
	// (first_to_send, etc). And those need to be preserved through a power fail by either using
	// a file or the battery backed memory as they do now. I think using the file becomes
	// complicated as the variables are by system.
	FLOW_RECORDING_CONTROL_STRUCT	frcs;

	// ----------------------------------------

	// 7/17/2014 rmd : FOAL side variable.
	SYSTEM_MAINLINE_BREAK_RECORD	latest_mlb_record;

	// 7/17/2014 rmd : IRRI side variable.
	SYSTEM_MAINLINE_BREAK_RECORD	delivered_mlb_record;

	// ----------------------------------------

	INT_16		derate_table_10u[ DERATE_TABLE_CELLS ];

	UNS_8		derate_cell_iterations[ DERATE_TABLE_CELLS ];

	// ----------------------------------------

	// 6/1/2012 rmd : To avoid drilling into the SYSTEM list (and taking the associated MUTEX)
	// each second, we capture the flow checking requirements when the sync occurs. This will
	// require the awareness to update these values if when they are edited.
	UNS_32		flow_check_required_station_cycles;

	UNS_32		flow_check_required_cell_iteration;

	BOOL_32		flow_check_allow_table_to_lock;

	UNS_32		flow_check_derate_table_gpm_slot_size;

	UNS_32		flow_check_derate_table_max_stations_ON;

	UNS_32		flow_check_derate_table_number_of_gpm_slots;


	// ----------------------------------------

	UNS_32		flow_check_ranges_gpm[ IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_RANGES ];

	UNS_32		flow_check_tolerance_plus_gpm[ IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_TOLERANCES ];

	UNS_32		flow_check_tolerance_minus_gpm[ IRRIGATION_SYSTEM_NUMBER_OF_FLOW_CHECK_TOLERANCES ];

	// ----------------------------------------

	// 6/8/2012 rmd : And some output from the flow checking function. Used internally.
	UNS_32		flow_check_derated_expected;

	UNS_32		flow_check_hi_limit;

	UNS_32		flow_check_lo_limit;

	// ----------

	// 6/26/2015 mpd : FogBugz #3113. Added "system_rcvd_most_recent_number_of_valves_ON" and reduced "expansion[ 16 ]"
	// to "expansion[ 15 ]". Also renamed "number_of_valves_ON" to "system_master_number_of_valves_ON".
	UNS_32 		system_rcvd_most_recent_number_of_valves_ON;

	// ----------

	// 8/27/2015 ajv : In the event of a powerfail, we need to track when the Master Valve
	// Override was scheduled to end. Therefore, capture the date and time whenever a scheduled
	// start occurs or someone manually performs the override.
	UNS_32		mvor_stop_date;

	UNS_32		mvor_stop_time;

	// 9/24/2015 ajv : Also, track the remaining seconds on the IRRI-side. Does not need to be
	// battery-backed but concides with how we track MLB on FOAL- vs. IRRI-side.
	UNS_32		delivered_MVOR_remaining_seconds;
	
	// ----------

	// 3/30/2016 skc : For budgets, it was decided to integrate data for budgets into the 
	// poc and system preserves settings. We know that this will force changes in battery backed.
	// We will take the hit and allocate more space at this time, intentionally not using expansion.
	BY_SYSTEM_BUDGET_RECORD 	budget;

	// ----------

	// 4/11/2016 skc : Temporary variable for tracking water usage by irrigation reason. As we
	// loop the list that holds the running stations, we save the reason so the budget report
	// can put the usage into the proper bin. We can do this because the station_preserves and
	// the poc_preserves both hold a pointer to the same system_preserves object. Note that a
	// system can be on for both programmed and manual_programmed reasons at the same time. The
	// values are stored as a bit field based on the values in foal_defs.h.
	UNS_32 		reason_in_running_list;

	// 10/24/2014 rmd : Added so that if one wanted to add content to this structure they could
	// take from this array without changing the size of the structure. And therefore also not
	// trigger a full initialization of this structure. And likely OTHER battery backed
	// structures as all their locations would have moved. Is kept initialized to 0's so when
	// comes time to use they have a defined value the new variable can count on.
	UNS_32		expansion[ 11 ];
	
	// ----------
	
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION.

} BY_SYSTEM_RECORD;

typedef struct
{
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION. We use the PRE string during startup to
	// see if the battery backed SRAM space for this structure has been initialized. When unit
	// is BORN it will not be equal to the string we are looking for. And we will initialize the
	// space.
	char	verify_string_pre[ BB_PRE_STRING_LENGTH ];

	// ----------

	// One of these records. The record in process for this system.
	BY_SYSTEM_RECORD			system[ MAX_POSSIBLE_SYSTEMS ];

	// ----------
	
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION.

} SYSTEM_BB_STRUCT;


extern SYSTEM_BB_STRUCT	system_preserves __attribute__( BATTERY_BACKED_VARS );

/* ---------------------------------------------------------- */
/*  				  POC STRUCTURES						  */
/* ---------------------------------------------------------- */

#include	"poc.h"

// ----------

typedef struct
{
	UNS_8	raw_one_second_count;

	UNS_8	need_to_add_to_the_accumulator;

	// Note this structure (two UNS_8 variables) is 4 bytes in size.

} POC_MASTER_COUNT_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// associated with above code not for MSC_VER
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 8/1/2012 rmd : With the introduction of a pile of POC report lines we need to include the
	// gid of the poc this record 'belongs' to.
	UNS_32	poc_gid;

	// ------------------------

	// 8/1/2012 rmd : I suppose if there were to be pwr fail we could be off for days. So
	// include both the start and end time & dates. BUT isn't the end time_date of this record
	// the start time_date of the next record? Yes. So no need for both start and end.
	UNS_32	start_time;

	UNS_16	start_date;

	// 7/11/2014 ajv : To assist in comm server development, we've added this
	// explicit 16-bit unsigned integer into the structure. The compile adds it
	// anyway to ensure the following double is on a 4-byte alignment.
	UNS_16	pad_bytes_avaiable_for_use;

	// ------------------------

	// A confusing subject. But the number of significant digits in a float is not all that
	// great. It is guaranteed to be at least 6. It may be 7 I think after reading. It is
	// certainly not eight. So tallying numbers after about 10 million gallons COULD start to
	// get pretty inaccurate. So not to take a chance we will store this accumulator as a
	// double.
	double	gallons_total;

	// 12/13/2012 ajv : Bob and I discussed the usefulnes of the seconds of flow
	// values at the POC level. We were unable to come up with any specific use
	// for these accumulators. However, rather than remove them, we will
	// continue to tally the seconds of actual flow and store them but simply
	// not display them at the controller or the central for now. This provides
	// some flexibility if someone comes up with a purpose for them in the
	// future.
	UNS_32	seconds_of_flow_total;

	double	gallons_during_idle;

	// 12/13/2012 ajv : Bob and I discussed the usefulnes of the seconds of flow
	// values at the POC level. We were unable to come up with any specific use
	// for these accumulators. However, rather than remove them, we will
	// continue to tally the seconds of actual flow and store them but simply
	// not display them at the controller or the central for now. This provides
	// some flexibility if someone comes up with a purpose for them in the
	// future.
	UNS_32	seconds_of_flow_during_idle;

	double	gallons_during_mvor;

	// 12/13/2012 ajv : Bob and I discussed the usefulnes of the seconds of flow
	// values at the POC level. We were unable to come up with any specific use
	// for these accumulators. However, rather than remove them, we will
	// continue to tally the seconds of actual flow and store them but simply
	// not display them at the controller or the central for now. This provides
	// some flexibility if someone comes up with a purpose for them in the
	// future.
	UNS_32	seconds_of_flow_during_mvor;

	double	gallons_during_irrigation;

	// 12/13/2012 ajv : Bob and I discussed the usefulnes of the seconds of flow
	// values at the POC level. We were unable to come up with any specific use
	// for these accumulators. However, rather than remove them, we will
	// continue to tally the seconds of actual flow and store them but simply
	// not display them at the controller or the central for now. This provides
	// some flexibility if someone comes up with a purpose for them in the
	// future.
	UNS_32	seconds_of_flow_during_irrigation;

} POC_REPORT_RECORD;

// 3/30/2016 skc : This structure is saved as part of budget reports.
typedef struct
{
	UNS_32 poc_gid;

	// 3/30/2016 skc : The assigned budget value for this POC. If the user changes the budget we
	// will catch this.
	UNS_32 budget;

	// 3/30/2016 skc : We use incremental addition over the entire budget period,
	// 4/8/2016 skc : Keep track of water usage for different flow reasons.
	double used_total_gallons; 		// all water thru the POC is accounted for
	double used_irrigation_gallons; // accumulated during irrigation cycles
	double used_mvor_gallons;		// accumulated when running MVOR
	double used_idle_gallons;		// detected flow not attributed to running a valve

	// 4/11/2016 skc : used_irrigation_gallons has even more specfic use cases. During
	// accumulation, the reason for a station in the running list is placed into the
	// system_preserves[].reason_in_running_list variable. This variable is then used to
	// determine how to account for POC usage. The different reasons are defined in foal_defs.h.
	double used_programmed_gallons;			// IN_LIST_FOR_PROGRAMMED_IRRIGATION, bit #1
	double used_manual_programmed_gallons;  // IN_LIST_FOR_MANUAL_PROGRAM, bit #2
	double used_manual_gallons;				// IN_LIST_FOR_MANUAL, bit #4
	double used_walkthru_gallons;			// IN_LIST_FOR_WALK_THRU, bit #5
	double used_test_gallons;				// IN_LIST_FOR_TEST, bit %6
	double used_mobile_gallons;				// IN_LIST_FOR_MOBILE, bit #7

	// 3/30/2016 skc : Keep track of the number of times budgets were in use at each start time.
	// There is concern that users will try to game the system by turning off budgets.
	UNS_32 on_at_start_time;
	UNS_32 off_at_start_time;

} BY_POC_BUDGET_RECORD;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	union
	{
		// 7/19/2012 rmd : If you want this to be 16 bits the bit field and the union must be
		// packed. See station_preserves for an example bit-field.
		UNS_32	overall_size;
		
		struct
		{
			// NOTE: In GCC this is the Least Significant Bit (Little Endian)
			
			// -------------
			
			// 8/8/2012 rmd : For the case of the regular resident and decoder based POC this means just
			// what it says. When (true) to energize the MV output. For the case of the BYPASS decoder
			// (true) means to be in the active state - that is autonomously opening and closing in
			// response to flow rates. This is to be used as an IRRI side variable. Meaning valid at all
			// slave controllers.
			BITFIELD_BOOL		master_valve_energized_irri			: 1;
			
			// 7/18/2012 rmd : Takes on a value of (true) when energized. (false) when not. See comment
			// for the 'master_valve_energized' variable. Same applies. This is an IRRI side variable.
			BITFIELD_BOOL		pump_energized_irri					: 1;
			
			
			// -------------
			
			// 8/9/2012 rmd : As measurements arrive from the tpmicro they are distributed around the
			// network. This flag indicates that need for new arrivals from the tpmicro to be sent to
			// the master. Note: to conserve flow_sense comms channel bandwidth, new arrivals are only
			// distributed if the number is NEW (different). This flag should be thought of as an IRRI
			// side flag. There is only one of these control variables for both the pump and mv as they
			// are always sent as a pair. And makes no difference if its a regular POC or decoder type
			// POC. On program startup set (false).
			BITFIELD_BOOL		send_mv_pump_milli_amp_measurements_to_the_master		: 1;
			
			// -------------
			
			// 8/10/2012 rmd : Set (true) when fm count data arrives from the tp micro. Which happens
			// once every 5 seconds for the terminal POC. And maybe once every 10 to 15 seconds for
			// decoder based POC's.
			BITFIELD_BOOL		there_is_flow_meter_count_data_to_send_to_the_master	: 1;
			
			// ----------
			
			// 8/21/2012 rmd : Set (true) when this tpmicro level poc was determined to have a short or
			// no current. Cleared when???
			BITFIELD_BOOL		shorted_mv												: 1;
			
			BITFIELD_BOOL		shorted_pump											: 1;
			
			BITFIELD_BOOL		no_current_mv											: 1;
			
			BITFIELD_BOOL		no_current_pump											: 1;
			
			// -------------
			// NOTE: This is the MS bit in GCC.
			// -------------
		};
	
	};
	
} POC_BIT_FIELD_STRUCT;


typedef struct
{
	POC_BIT_FIELD_STRUCT	pbf;

	// ----------
	
	// 6/4/2014 rmd : And for the case of a TERMINAL POC this is 0.
	UNS_32				decoder_serial_number;
	
	// ----------
	
	// 6/12/2014 rmd : Takes on the poc file POC_NORMALLY_OPEN or POC_NORMALLY_CLOSED values.
	UNS_32				master_valve_type;

	// ----------

	// 5/27/2014 rmd : As the raw flow meter data arrives from the tpmicro it is placed here.
	// Awaiting shipment to the master in the next token response. Think of this as an irri side
	// group of variables.
	UNS_32				fm_accumulated_pulses_irri;

	UNS_32				fm_accumulated_ms_irri;

	UNS_32				fm_latest_5_second_pulse_count_irri;
	
	UNS_32				fm_delta_between_last_two_fm_pulses_4khz_irri;
	
	UNS_32				fm_seconds_since_last_pulse_irri;
	
	// ----------
	
	// 5/27/2014 rmd : And then in the master as we pull out of the token response we collect
	// into this variable. And set our flag indicating need to compute real flow rate and add to
	// the report accumulators. Which maybe we can do real time upon receipt.
	UNS_32				fm_accumulated_pulses_foal;

	UNS_32				fm_accumulated_ms_foal;

	UNS_32				fm_latest_5_second_pulse_count_foal;
	
	UNS_32				fm_delta_between_last_two_fm_pulses_4khz_foal;
	
	// 1/13/2015 rmd : This is used to deduce that the flow rate has gone below a minimum agreed
	// upon value. For example for the 1 pulse per 10 gallons transducer if this value is
	// greater than 60 seconds that means the flow rate is less than 10 gpm. And the main board
	// then can make the decision to ignore the delta and call the flow rate 0.
	UNS_32				fm_seconds_since_last_pulse_foal;
	
	// ----------
	
	// 6/3/2014 rmd : This value is to hold the converted accumulated pulses converted to
	// gallons. There is no time base. Meaning it does not represent a number of gallons over a
	// known period of time. It is simply a number of gallons that needs to be feed to the
	// accumulators. In the case of this poc this value will drive the poc accumulator. It will
	// also be added to a system accumulated gallons to drive system and station level
	// accumulators. This variable is not available for any other use. Its value is only valid
	// for short periods of time.
	float				accumulated_gallons_for_accumulators_foal;
	
	// ----------
	
	// 5/27/2014 rmd : Once per second compute the latest 5 second average gpm value for each
	// flow meter in each POC. These are the values that get distributed for display. But we are
	// careful to ONLY send the values for the non-zero numbers in order to minimize token size.
	// Also once per second we also add these up to create all by system 5 second average flow
	// rate for each system. We keep the most recent 20 of them. And use them to create the
	// stability average each second. (This does NOT need to be battery backed.)Only valid at
	// the MASTER (a FOAL side variable).
	float				latest_5_second_average_gpm_foal;

	// ---------------

	// With each token from the master we deliver the latest calculated flow rate for EACH
	// terminal or decoder based poc in the entire network. (This does NOT need to be battery
	// backed.) An IRRI side variable.
	float				delivered_5_second_average_gpm_irri;
	
	// ----------
	
	// 8/13/2012 rmd : Regarding these current measurement variables and their accompanying
	// flags. What to do on program start? I've decided not to 0 the last recevied current
	// measurements on program startup. I will set their distribution flags to false though. The
	// argument is that these numbers are the last measured current and should be preserved.

	// 8/10/2012 rmd : As delivered from the TPMICRO. Once per turn ON event. Meaning when
	// something new is turned ON we get the last measured current for each triac that is ON.
	// Units are milli-amps. These two are IRRI side variables.
	UNS_32				mv_last_measured_current_from_the_tpmicro_ma;
	// 8/10/2012 rmd : See the mv variable just prior. Same comment applies.
	UNS_32				pump_last_measured_current_from_the_tpmicro_ma;

	// ----------
	
	// 8/10/2012 rmd : When the readings are rcvd from the slave box as part of the TOKEN_RESP
	// this is where they are stored. These two are FOAL side variables.
	UNS_32				mv_current_for_distribution_in_the_token_ma;
	// 8/10/2012 rmd : See the mv variable just prior. Same comment applies.
	UNS_32				pump_current_for_distribution_in_the_token_ma;

	// ----------
	
	// 8/10/2012 rmd : The original numbers from the tp micro are sent to the master then
	// redistributed to all slaves. When the values are received in the token from the master
	// this is where they are to be stored. These are the values to display! These two are IRRI
	// side variables.
	UNS_32				mv_current_as_delivered_from_the_master_ma;
	// 8/10/2012 rmd : See the mv variable just prior. Same comment applies.
	UNS_32				pump_current_as_delivered_from_the_master_ma;

} POC_DECODER_OR_TERMINAL_WORKING_STRUCT;


typedef struct
{
	// 5/27/2014 rmd : Used when we are syncing the preserves to the poc file. That is testing
	// if each poc gid has a matching poc_preserve record. And there are NO poc_preserve records
	// with gid's that no longer exist.
	BOOL_32				accounted_for;

	// ----------

	// A value of 0 means it ain't in use. We look for a matching gid or find a struct not in
	// use and take it. When we try to add to a record or set a value if we do not find a
	// matching gid we make such a record and zero out the entries (picture trying to add to an
	// accumulator and we cannot find the system gid match...so we take the next available and 0
	// the record then add to the accumulator.
	UNS_32				poc_gid;

	// ----------
	
	// 8/9/2012 rmd : All POCs - terminal or decoder based - are physically tied
	// to a box. This is the box index. Is used in a variety of ways. For
	// example to know which POCs to keep the tpmicro informed about as far as
	// mv and pump output state. And the reverse, when receiving poc
	// measurements from the tpmicro to allow us to find the poc to write the
	// received measurements to.
	UNS_32				box_index;

	// ----------

	// 5/19/2014 rmd : One of these three types:	POC__FILE_TYPE__TERMINAL
	//  											POC__FILE_TYPE__DECODER_SINGLE
	//  											POC__FILE_TYPE__DECODER_BYPASS
	// 5/19/2014 rmd : Note the naming convention to help ensure no confusion on which defines
	// to use for this variable.
	UNS_32				poc_type__file_type;

	// ----------
	
	BOOL_32				unused_01;

	// ----------
	
	// 6/12/2014 rmd : At the poc file level. Not at the decoder level.
	UNS_32				usage;
	
	// ----------

	// For convenience. Set during the sync. Each second as we add to the POC accumulators we
	// need information about the system this POC belongs to. This is an easy way to get that
	// information without having to keep looking up the system the POC belongs to.
	BY_SYSTEM_RECORD		*this_pocs_system_preserves_ptr;

	// ----------
	
	// 5/27/2014 rmd : The rip's are at the file level POC definition. Meaning one rip per
	// bypass. AJ and I spoke about this and decided we did not need the rip to be at the
	// tpmicro type level (one rip per decoder or terminal poc). The reasoning is tied to the
	// poc report data is to reflect the CITY poc meter. And bypass or no bypass is not a
	// factor. One city meter - one report rip. Which will also support budgets.
	POC_REPORT_RECORD		rip;

	// ----------
	
	// 5/27/2014 rmd : One for each potential physical terminal or decoder based item involved
	// in this poc. Think of this as an irri side variable. (This does not need to be battery
	// backed.)
	POC_DECODER_OR_TERMINAL_WORKING_STRUCT	ws[ POC_BYPASS_LEVELS_MAX ];

	// ----------

	// 5/29/2014 rmd : This flag is an irri side variable. It is set upon token receipt. And
	// indicates to the bypass control alogorithm that the bypass should be active. Meaning
	// opening valves according to flow. The bypass control alogorithm will set each of the
	// individual MV energize bits.
	UNS_32					bypass_activate;

	// ----------
	
	// 10/12/2012 rmd : A safety catch variable. For one of the worst possible errors. Which is
	// telling the tpmicro to energize the pump with no valves ON. And certain communication
	// errors could lead to this. For example if the broadcast TOKEN informing the IRRI machine
	// to turn ON a valve is missed (due to a comm error). The master would be delivering POC
	// information to open MV's and possibly energize pumps while no valves are opened. This
	// state would continue till the next vavle transition. This variable is used to track that
	// condition and no energive MV and PUMP when no valves are ON for some number of tpmicro
	// messages. This variable does not need to be battery backed but is part of the
	// poc_preserves for convience. And is valid at each controller.
	UNS_32				msgs_to_tpmicro_with_no_valves_ON;

	// ----------

	// 3/30/2016 skc : For budgets, it was decided to integrate data for budgets into the 
	// poc and system preserves settings. We know that this will force changes in battery backed.
	// We will take the hit and allocate more space at this time, intentionally not using expansion.
	BY_POC_BUDGET_RECORD 	budget;

	// ----------

	// 10/24/2014 rmd : Added so that if one wanted to add content to this structure they could
	// take from this array without changing the size of the structure. And therefore also not
	// trigger a full initialization of this structure. And likely OTHER battery backed
	// structures as all their locations would have moved. Is kept initialized to 0's so when
	// comes time to use they have a defined value the new variable can count on.
	UNS_32				expansion[ 4 ];
	
	// ----------
	
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION.

} BY_POC_RECORD;

typedef struct
{
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION. We use the PRE string during startup to
	// see if the battery backed SRAM space for this structure has been initialized. When unit
	// is BORN it will not be equal to the string we are looking for. And we will initialize the
	// space.
	char	verify_string_pre[ BB_PRE_STRING_LENGTH ];

	// ----------

	// 6/18/2012 rmd : This should be set (true) as part of the 'set' functions and various
	// other activities. Such as moving a POC to another system.
	BOOL_32				perform_a_full_resync;

	// One of these records. The record in process for this system.
	BY_POC_RECORD		poc[ MAX_POCS_IN_NETWORK ];

	// ----------
	
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION.

	// 3/30/2016 skc : This string can be found in battery_backed_funcs.c, POC_PRESERVES_VERIFY_STRING_PRE
	

} POC_BB_STRUCT;


extern POC_BB_STRUCT	poc_preserves __attribute__( BATTERY_BACKED_VARS );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 4/4/2014 rmd : This is the information that is acquired from each slave, assembled at the
	// master, and then fully shared with each slave as it changes.

	// 4/3/2014 rmd : Means the controller responded to the scan. And that's all. Means nothing
	// about if has exceeded the communication error limit. This variable is valid at both
	// MASTERS and SLAVES.
	BOOL_32						saw_during_the_scan;
	
	// 9/4/2013 ajv : For each controller in the chain the following structure stores
	// controller's configuration including its name, serial number, purchased software options
	// such as the -FL option, and the full what's installed structure. This portion
	// chain_members is valid at both the MASTER and SLAVES after the valid for use flag is
	// set.
	BOX_CONFIGURATION_STRUCT	box_configuration;
	
	// ----------

	// 10/24/2014 rmd : Added so that if one wanted to add content to this structure they could
	// take from this array without changing the size of the structure. And therefore also not
	// trigger a full initialization of this structure. And likely OTHER battery backed
	// structures as all their locations would have moved. Is kept initialized to 0's so when
	// comes time to use they have a defined value the new variable can count on.
	UNS_32		expansion[ 4 ];

	// ----------
	
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION.

} CHAIN_MEMBERS_SHARED_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION. We use the PRE string during startup to
	// see if the battery backed SRAM space for this structure has been initialized. When unit
	// is BORN it will not be equal to the string we are looking for. And we will initialize the
	// space.
	char	verify_string_pre[ BB_PRE_STRING_LENGTH ];

	// ----------

	// 4/16/2014 rmd : The members structure is rather large at 1488 bytes. I mention this as we
	// do include it in the token when distribution is necessary.
	CHAIN_MEMBERS_SHARED_STRUCT			members[ MAX_CHAIN_LENGTH ];

	// ----------
	
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION.

} CHAIN_MEMBERS_STRUCT;

// 10/1/2012 rmd : After scanning, this structure becomes valid at all
// controllers in a chain. It is used to recognize when the chain definition
// has changed. Which is why it is battery backed. It is protected by
// chain_members_recursive_MUTEX and all accesses should be managed within the
// MUTEX.
extern CHAIN_MEMBERS_STRUCT chain __attribute__(BATTERY_BACKED_VARS);

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2011.11.15 rmd : This structure is defined within the battery_backed_vars.h file for
// compilation reasons. There were circular dependencies that were hard to break.
typedef struct
{
	MIST_DLINK_TYPE			list_support_foal_all_irrigation;

	MIST_DLINK_TYPE			list_support_foal_stations_ON;

	// 4/24/2012 rmd : This list is the re-do of what was the SXR list in the ET2000e. That list
	// was problematic as a station could be on the list to turn ON and at the same time be on
	// the list to say turn OFF. There was also the issue of the sxr record in the list pointed
	// to an ilc that perhaps wasn't in the irrigation list anymore. Instead of having a
	// separate sxr list, all in all it seemed more logical to create a new list using the ilc
	// elements themselves. This way a particular station (an ilc) can only be on the list ONCE
	// for ONE reason. And when we remove an ilc from the main list we will correspondingly
	// remove the ilc from this new list at the same time.
	MIST_DLINK_TYPE			list_support_foal_action_needed;

	// 4/25/2012 rmd : This is only relevant when this station is on the action_needed list.
	UNS_16					action_reason;

	// 2011.11.09 rmd : This variable was introduced as a convenience and a performance
	// improver. To avoid continually having to look up which system_preserves record this ilc
	// is using during the foal irrigation maintenance process. Since the system preserves is a
	// battery backed list and the ilcs list is also battery backed this survives a power
	// failure nicely. Additionally the system is not allowed to be edited during irrigation
	// which means this pointer is valid. During the code update process this could be rendered
	// invalid. Therefore irrigation will no survive through a code update. Meaning if it was
	// irrigating before the code update it won't be afterwards.
	BY_SYSTEM_RECORD		*bsr;

	// 6/15/2012 rmd : NOTE - note an attempt was made to keep in this structure a pointer to
	// the actual station this ilc represents. Well that's really a bad idea. As the station
	// list is held in dynamic memory. And is dynamically created as the list is read out of the
	// file or as stations are added to the system in some other fashion. So through a power
	// failure there is no guarantee the list occupies the same SDRAM blocks of memory. Just a
	// bad idea!

	// 2011.12.03 rmd : Again a variable of convenience. To prevent us from having to go find
	// this index every time we want to update the report data for the station as it is running.
	// An INT_32 just to be compatible with the function that finds this index.
	UNS_32					station_preserves_index;


	// When a station is ON this is loaded with how long it is to run. Can go negative if we
	// count down below 0. Should be happening here in the FOAL machine. The IRRI side keeps an
	// abreviated list. Which contains a copy that also can run negative. As a matter of fact
	// due to token delay will almost always run negative before turning off via a command from
	// the FOAL machine.
	INT_32					remaining_seconds_ON;

	// The total amount left to irrigate. As cycle take place this amount is decreased by the
	// cycle amount. Must be able to hold the longest amount we would ever want to irrigate for.
	// Hence it is a 32-bit long. (I've tried to come up with a different name but have
	// struggled. Maybe yet_to_be_started_remaining_irrigation_seconds)
	UNS_32					requested_irrigation_seconds_ul;

	UNS_32					cycle_seconds_ul;

	UNS_32					soak_seconds_remaining_ul;

	// 6/26/2012 rmd : This is the user setting.
	UNS_32					soak_seconds_ul;

	// Need to fill out the stop time as an absolute date & time so that in case we suffer a
	// power fail during the 'stop time' when the power returns we know to turn off the
	// stations...the stop time must be evaluated at the start time not when the station is
	// added to the irrigation list.
	UNS_32					stop_datetime_t;


	// 2011.09.28 rmd : Big Bit Field. Hence bbf. This will encompass the 4 bits of why we are
	// in the list. Plus all the other weighting bits, flow error status, if this station
	// reponds to rain and wind. Flow recording lines support. And more.
	BIG_BIT_FIELD_FOR_ILC_STRUCT	bbf;

	UNS_16					expected_flow_rate_gpm_u16;


	UNS_16					line_fill_seconds;

	// 2/20/2013 rmd : The user setting for this station to handle slow close valves by blocking
	// the turn ON of the next valve for this long.
	UNS_16					slow_closing_valve_seconds;


	// We carry the on at a time GID. And that is used to track the on at a time counts within
	// the actual on at a time (file) list. This may prove to be a problem. Because it means
	// during irrigation we take the PROGRAM DATA MUTEX to diddle the count of how many are on
	// within each group. And I thought maybe I saw a problem taking this MUTEX during
	// irrigation while also holding the foal irri list mutex. We'll see.
	UNS_16					GID_on_at_a_time;


	// Need to fill out the stop time as an absolute date & time so that in case we suffer a
	// power fail during the 'stop time' when the power returns we know to turn off the
	// stations...the stop time must be evaluated at the start time not when the station is
	// added to the irrigation list.
	UNS_16					stop_datetime_d;


	UNS_8					station_number_0_u8;  // 0 based - as always - max 128

	// ----------
	
	// 4/21/2014 rmd : Who it belongs to. Values 0 through (MAX_CHAIN_LENGTH - 1). Reflects the
	// comm letter minus 'A'. At a savings of 3K of battery backed memory space and no
	// noticeable code size hit (looking at optimized code) we make this an 8-bit variable. The
	// savings is in the size of the foal_irri permantely allocated ilc array of 768 stations.
	UNS_8					box_index_0;

	// ----------
	
	// 3/30/2016 rmd : To save us from having to find the station in the file list using the
	// box_index and station number held herein, then find the station group this station
	// belongs to, then get the decoder serial number to see if this station is using moisture
	// sensing. And having to do this each second for each station in the irrigation list. So we
	// do that up front when the station is added to the irrigation list.
	UNS_32					moisture_sensor_decoder_serial_number;
	
	// ----------
	
	// 3/30/2016 rmd : Structure is now at 100 bytes after adding the moisture sensor decoder
	// serial number.

} IRRIGATION_LIST_COMPONENT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION. We use the PRE string during startup to
	// see if the battery backed SRAM space for this structure has been initialized. When unit
	// is BORN it will not be equal to the string we are looking for. And we will initialize the
	// space.
	char	verify_string_pre[ BB_PRE_STRING_LENGTH ];

	// ----------
	
	// List of station involved in irrigation (everybody who is ON or wants to come ON)
	//
	// 9/9/2014 rmd : NETWORK LEVEL LIST!
	MIST_LIST_HDR_TYPE	list_of_foal_all_irrigation;
	
	// List of all those ON - useful in many areas ( including tallys )
	//
	// 9/9/2014 rmd : Hey wake up! Here's an interesting fact to remember. This list is a
	// NETWORK level list. In other words those ON from perhaps more than 1 system!
	MIST_LIST_HDR_TYPE	list_of_foal_stations_ON;

	// 4/24/2012 rmd : This list is the re-do of what was the SXR list in the ET2000e. That list
	// was problematic as a station could be on the list to turn ON and at the same time be on
	// the list to say turn OFF. There was also the issue of the sxr record in the list pointed
	// to an ilc that perhaps wasn't in the irrigation list anymore. Instead of having a
	// separate sxr list, all in all it seemed more logical to create a new list using the ilc
	// elements themselves. This way a particular station (an ilc) can only be on the list ONCE
	// for ONE reason. And when we remove an ilc from the main list we will correspondingly
	// remove the ilc from this new list at the same time.
	MIST_LIST_HDR_TYPE	list_of_foal_stations_with_action_needed;

	// ----------------------------

	// 6/21/2012 rmd : If a group of valves are turning OFF within the same pass of the
	// maintenance function we don't want to make a flow recording group of lines for each turn
	// OFF. We want to show the whole group ONCE in the flow recording. So prior to the first
	// turn OFF make the lines for all the valves ON. But don't keep making the lines as the
	// valves are whittled down. For example for valve 1 2 3 4. Then valves 2 3 4. Then valves 3
	// 4. Then valve 4. Kust make the first group. And this variable is used to control that.
	// This is also used for function like the STOP key processing. And other events that
	// trigger all stations ON to be turned OFF.
	// 6/27/2012 rmd : Use where-ever we are going to loop through and potentially turn OFF more
	// than 1 valve ON.
	BOOL_32	flow_recording_group_made_during_this_turn_OFF_loop;

	// ----------------------------

	// 5/9/2012 rmd : Instead of the list we used to keep this is more efficient for the battery
	// backed SRAM use. This is what we compare to during the turn ON electrical limit decision.
	// These values may be used outside of the foal_irri list maintenance function. But their
	// use should be protected by the foal_irri MUTEX because in the list maintenance function
	// we set the whole array to zero and rebuild the count each second. We do this to guarantee
	// it's accuracy for it's intended purpose ... managing the electrical limit.
	UNS_32	stations_ON_by_controller[ MAX_CHAIN_LENGTH ];

	// ----------------------------

	xTimerHandle	timer_rain_switch;		// intialized on program start via init_foal_irri

	xTimerHandle	timer_freeze_switch;	// intialized on program start via init_foal_irri

	// ----------------------------

	// This is the variable that the FOAL machine uses to decide if we should turn valves ON or
	// not.
	BOOL_32			wind_paused;

	// ----------------------------

	// We define the ilc's in the FOAL irrigation list as an array instead of the dynamic memory
	// approach because we must maintain list integrity throughout a power failure and that
	// means a program restart. On program restarts dynamic memory allocations are not
	// maintained.
	//
	// Do not reset on program start - reset when user requests reset or rom update.
	IRRIGATION_LIST_COMPONENT	ilcs[ MAX_STATIONS_PER_NETWORK ];

	// ----------------------------

	BOOL_32						need_to_distribute_twci;
	
	TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT	twccm[ MAX_CHAIN_LENGTH ];

	// ----------

	// 10/24/2014 rmd : Added so that if one wanted to add content to this structure they could
	// take from this array without changing the size of the structure. And therefore also not
	// trigger a full initialization of this structure. And likely OTHER battery backed
	// structures as all their locations would have moved. Is kept initialized to 0's so when
	// comes time to use they have a defined value the new variable can count on.
	UNS_32		expansion[ 32 ];
	
	// ----------
	
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION.

} FOAL_IRRI_BB_STRUCT;


// 11/21/2014 rmd : Being battery backed variables DO NOT move them to another file. DO NOT
// change their location within this file. DO NOT change their location relative to one
// another. Any changes may change their location within the batter backed section. Leading
// to variable initialization upon a code update.

// 11/21/2014 rmd : This structure occurs LAST in the battery backed section. And that is
// due to the link order that happens under direction of Crossworks. It's just the way it
// turned out. See the following variable note.
extern FOAL_IRRI_BB_STRUCT		foal_irri __attribute__(BATTERY_BACKED_VARS);


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION. We use the PRE string during startup to
	// see if the battery backed SRAM space for this structure has been initialized. When unit
	// is BORN it will not be equal to the string we are looking for. And we will initialize the
	// space.
	char	verify_string_pre[ BB_PRE_STRING_LENGTH ];

	// ----------
	
	// 8/14/2014 rmd : If the network ID is a 0 no other CI data can be transferred till the
	// unit registers with the comm_server. Whenver any ci data xfer timer fires a check is made
	// against the network ID and if zero will send the registration instead. Additionally we
	// test this flag defined here and if true will also send the registration instead of the
	// data the timer is requesting.
	//
	// 8/14/2014 rmd : The two variables work together to plug the hole where the application
	// indicates it wants to send the registration data in the middle of a CI session that just
	// sent the registration data. When we recieve the ACK we would clear the request if only a
	// single variable were used. The pending variable is only evaluated when the CI task is
	// IDLE. And that's the key.
	//
	// 2/6/2015 rmd : And important to be battery backed so that once the request is made even
	// if there is a power failure the re-registration will eventually occur when power resumes.
	BOOL_32			pending_request_to_send_registration;
	
	BOOL_32			need_to_send_registration;

	// ----------
	
	// 11/21/2014 rmd : Available for future use as the need comes up.
	UNS_32			ununsed[ 24 ];

	// ----------
	
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION.

} GENERAL_USE_BB_STRUCT;

// 11/21/2014 rmd : As just noted this variable is occurs last in the batter backed SRAM. We
// made this so intentionally cause we added this varialbe after the fact and wanted to
// avoid exisitng variable integrity test failures.
//
// 11/21/2014 rmd : This variable can be use by any task as general use batter backed
// variables. Take new variable space from the "unused" array within the structure.
extern GENERAL_USE_BB_STRUCT	battery_backed_general_use __attribute__(BATTERY_BACKED_VARS);

/* ---------------------------------------------------------- */
/*  				 LIGHTS STRUCTURES						  */
/* ---------------------------------------------------------- */

#define	LIGHTS_BITFIELD_BOOL_light_is_available_bit			(0)
#define	LIGHTS_BITFIELD_BOOL_light_energized_irri_bit		(1)
#define	LIGHTS_BITFIELD_BOOL_shorted_output_bit				(2)
#define	LIGHTS_BITFIELD_BOOL_no_current_output_bit			(3)
#define	LIGHTS_BITFIELD_BOOL_MAX_BIT						(LIGHTS_BITFIELD_BOOL_no_current_output_bit)

typedef union
{
	UNS_8	overall_size;

	struct
	{
		// NOTE: In GCC this is the Least Significant Bit (Little Endian)

		// -------------

		// Takes on a value of (true) when the light is determined to be physically available. This is an IRRI side 
		// variable.
		BITFIELD_BOOL		light_is_available											: 1;

		// Takes on a value of (true) when energized. (false) when not. This is an IRRI side variable.
		BITFIELD_BOOL		light_is_energized											: 1;

		// Set (true) when this tpmicro level lights was determined to have a short. This is cleared when the light 
		// is turned on. This IRRI variable is synced with the FOAL side using token responses and tokens.
		BITFIELD_BOOL		shorted_output												: 1;

		// Set (true) when this tpmicro level lights was determined to have no current flow on the output. This is 
		// cleared when the light is turned on. This IRRI variable is synced with the FOAL side using token 
		// responses and tokens.
		BITFIELD_BOOL		no_current_output											: 1;

		// This FOAL side field indicates why the the light was turned on. It is currently only used for the real time
		// screen. This occasional reference justifies it's location in the bit field.
		BITFIELD_BOOL		reason														: 4;

	} __attribute__((packed)) ;
	
	// 9/29/2015 ajv : This structure is 1 byte.

} __attribute__((packed)) LIGHTS_BIT_FIELD;

// ----------

typedef struct
{
	// 7/16/2015 mpd : The LIGHTS_REPORT_RECORD for this light on the current day. It will be rolled into the
	// "lights_report_data_completed" structure at midnight. This is 16-bytes long.
	LIGHTS_REPORT_RECORD		lrr;

	// 8/8/2012 rmd : As reported by the tpmicro. This needs to be distributed to the master
	// controller. Somehow. TBD!! TODO   Units are milli-amps. Stored as an UNS_16 for space
	// savings in the precious battery backed SRAM. Comes from the tpmicro as an UNS_32 but is
	// converted as received.
	UNS_16						last_measured_current_ma;

	UNS_16						expansion_bytes;

	// ----------

	// 9/29/2015 ajv : This structure is 20-bytes

} BY_LIGHTS_RECORD;

// ----------

typedef struct
{
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION. We use the PRE string during startup to
	// see if the battery backed SRAM space for this structure has been initialized. When unit
	// is BORN it will not be equal to the string we are looking for. And we will initialize the
	// space.
	char	verify_string_pre[ BB_PRE_STRING_LENGTH ];

	// ----------

	// One of these records. The record in process for this system. This is 24-bytes long.
	BY_LIGHTS_RECORD		lights[ MAX_LIGHTS_IN_NETWORK ];

	// 8/12/2015 mpd : Packed to UNS_8 and moved to the LIGHTS_BB_STRUCT. This change requires using the array index 
	// for "lbf" in addition to the current use for "BY_LIGHTS_RECORD" and "LIGHTS_STRUCT" arrays. The packing saves 244 
	// bytes of battery backed memory. 
	LIGHTS_BIT_FIELD		lbf[ MAX_LIGHTS_IN_NETWORK ];

	// ----------
	
	// 8/6/2015 rmd : IF THIS STRUCTURE DEFINITION IS CHANGED THE PRE STRING MUST ALSO BE
	// CHANGED TO TRIGGER A STRUCTURE RE-INITIALIZATION.

	// 9/29/2015 ajv : This structure is 1,216 bytes long.

} LIGHTS_BB_STRUCT;

extern LIGHTS_BB_STRUCT	lights_preserves __attribute__( BATTERY_BACKED_VARS );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// This is a list of the lights that are on or going on. This is similar in function to a combination of the station 
	// "main" (aka "all") and "action needed" lists. Since there is no limit to the number of lights that can be on 
	// simultaneously, there is no need to queue lights and weigh them for priority. This single list can handle lights. 
	MIST_DLINK_TYPE			list_linkage_for_foal_lights_ON_list;

	// This list is functionally identical to the stations "action needed" list. It is a list of lights that needs to be 
	// communicated to the slaves to keep them in sync. 
	MIST_DLINK_TYPE			list_linkage_for_foal_lights_action_needed_list;

	// 8/4/2015 mpd : TBD: IRRI uses a Big Bit Field here for all the station fields. Lights has a lot fewer fields to
	// store. Use UNS_8 containers for now. Create a bit field if the storage space outweighs processing time.

	// This is only relevant when this station is on the action_needed list.
	UNS_8					action_needed;

	UNS_8					reason_in_list;

	UNS_8					xfer_to_irri_machines;

	UNS_8					light_is_energized;

	// Stop time is an absolute date & time so know when to turn off the light in the case of a power failure. Stop
	// time is evaluated at the start time as the light is added to the ON list. A stop time is required. 
	UNS_32					stop_datetime_t;

	UNS_16					stop_datetime_d;

	// ----------
	
	// 4/21/2014 rmd : Who it belongs to. Values 0 through (MAX_CHAIN_LENGTH - 1). Reflects the
	// comm letter minus 'A'. At a savings of 3K of battery backed memory space and no
	// noticeable code size hit (looking at optimized code) we make this an 8-bit variable. The
	// savings is in the size of the foal_irri permantely allocated ilc array of 768 stations.
	UNS_8					box_index_0;

	UNS_8					output_index_0;  
	// ----------
	
	// This structure is 36 bytes long on 8/19/2015.

} LIGHTS_LIST_COMPONENT;

// ----------

typedef struct
{
	char	verify_string_pre[ BB_PRE_STRING_LENGTH ];

	// ----------
	
	// List of station involved in irrigation (everybody who is ON or wants to come ON). This is a NETWORK LEVEL LIST!
	// Unlike the "foal_irri.c" code, there is no "list_of_foal_all_lights" because there is no limit to the number of 
	// lights that can be ON at once. There is no weighting, no mow days, no rain, freeze switch, or MLB issues. If a 
	// light is to go ON, we put it on the "list_of_foal_lights_ON" list.
	MIST_LIST_HDR_TYPE	header_for_foal_lights_ON_list;
	
	// 4/24/2012 rmd : This list is the re-do of what was the SXR list in the ET2000e. That list
	// was problematic as a station could be on the list to turn ON and at the same time be on
	// the list to say turn OFF. There was also the issue of the sxr record in the list pointed
	// to an llc that perhaps wasn't in the irrigation list anymore. Instead of having a
	// separate sxr list, all in all it seemed more logical to create a new list using the llc
	// elements themselves. This way a particular station (an llc) can only be on the list ONCE
	// for ONE reason. And when we remove an llc from the main list we will correspondingly
	// remove the llc from this new list at the same time.
	MIST_LIST_HDR_TYPE	header_for_foal_lights_with_action_needed_list;

	// We define the llc's in the FOAL lights list as an array instead of the dynamic memory
	// approach because we must maintain list integrity throughout a power failure and that
	// means a program restart. On program restarts dynamic memory allocations are not
	// maintained.
	//
	// Do not reset on program start - reset when user requests reset or rom update.
	LIGHTS_LIST_COMPONENT	llcs[ MAX_LIGHTS_IN_NETWORK ];

	// ----------
	
	// 9/29/2015 ajv : This structure is 1,800 bytes long.

} FOAL_LIGHTS_BATTERY_BACKED_STRUCT;

// 11/21/2014 rmd : Being battery backed variables DO NOT move them to another file. DO NOT
// change their location within this file. DO NOT change their location relative to one
// another. Any changes may change their location within the batter backed section. Leading
// to variable initialization upon a code update.

// 11/21/2014 rmd : This structure occurs LAST in the battery backed section. And that is
// due to the link order that happens under direction of Crossworks. It's just the way it
// turned out. See the following variable note.
extern FOAL_LIGHTS_BATTERY_BACKED_STRUCT		foal_lights __attribute__(BATTERY_BACKED_VARS);

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/6/2015 rmd : The battery_backed_funcs file was created as part of a re-organization
// that split the battery_backed_vars.c file into the vars and funcs files. Previously there
// was only a ...vars.c file and the ...vars.h file was/is included in many fiels. After the
// file split to avoid changing all those files I simple chained the includes here. The
// result is whenever battery_backed_vars.h is included both the vars.h and funcs.h are
// included.
#include	"battery_backed_funcs.h"

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// associated with above code not for MSC_VER
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

