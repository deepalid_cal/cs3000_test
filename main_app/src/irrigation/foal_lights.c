
/*  file = foal_lights.c                		7/28/2015 mpd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memset
#include	<string.h>

#include	"foal_lights.h"

#include	"lights.h"

#include	"e_lights.h"

#include	"irri_lights.h"

#include	"app_startup.h"

#include	"configuration_controller.h"

#include	"configuration_network.h"

#include	"td_check.h"

#include	"alerts.h"

#include	"comm_mngr.h"

#include	"battery_backed_vars.h"

#include	"r_alerts.h"

#include	"report_data.h"

#include	"freeRTOSconfig.h"

#include	"change.h"

#include	"range_check.h"

#include	"cs_mem.h"

#include	"alert_parsing.h"


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static const char FOAL_LIGHTS_PRE_TEST_STRING[ BB_PRE_STRING_LENGTH ] = "f lights v03";

/* ---------------------------------------------------------- */

// For debug purposes to measure how long it takes to do the processing for a start time when all 48 lights are 
// starting. It takes ??? ms. The token stops for that period as the TD_CHECK task is at a higher priority than the 
// COMM_MNGR task.
UNS_32	__largest_lights_start_time_delta;


/* ---------------------------------------------------------- */
/*  					Lights Requirements					  */
/* ---------------------------------------------------------- */
/**
    The single requirement at the beginning of Lights implementation was to "do what the
	2000e does". The following text tries to elaborate on that.
				 
    In an effort to be flexible, this code is implemented in a modular manner so any item
    "required" can be turned on and off as the finer details of lights functionality are
    discussed. The goal is autonomous code blocks for each requirement.
 
    This is intended to be a complete list. The functionality is distributed throughout
    the lights code. It's not all done in this module.
 
    "OFF" in the following discussion refers to a valid midnight-to-midnight time. The
    lower-case "off" value displayed on	the screen to indicate disabled time fields is
	NOT a valid "OFF" time.
				 
    All "ON" date/times require an "OFF" date/time recorded in the lights preserves
    structure. For programmed lights, there is a 13 day, 23 hour, and 50 minute limit.
    This can effectively be infinite by creating overlapping times with the 2 available
    start/stop time sets. This limit differs from the 2000e which does not check for a
	stop date/time. (limit is not yet implemented)
				 
    All "ON" date/times require an "OFF" date/time recorded in the lights preserves
    structure. For manual lights, there is a 4 hour default that can be extended up to
    the 13 day, 23 hour, and 50 minute limit. This differs from the 2000e which does not
	limit the manual stop date/time. (limit is not yet implemented)
				 
    Before taking a light off the "ON list" at the saved stop date/time, a check will be
    made to see if there is an immediate start time for that light. If there is, it will
    not be turned off to add a new "ON list" entry. Instead, the stop date/time in the
	existing entry will be updated with the new scheduled value. 
				 
    When a light is turned ON, we clear the short and open flags. The TPMicro will notify
    the IRRI machine if the problem still exists and it will take action.
 
    The two start and stop times in the 14-day rotating schedule for each light are not
    "paired". They are independent values. They can be set in any order. Start 2 can
    preceed start 1. Times can overlap. They can be nested. They can extend to future
	days. 
				 
    The stop date/time is recorded in the ON list entry at the time the light is added
    to the ON list (or updated by a subsequent request). Lights code does not monitor the
	daily schedule looking for newer stop date/time values.

    For a programmed start, if a light is already on the "ON list", it will not be turned
    off to add a new "ON list" entry. Instead, the stop date/time in the existing entry
    will be extended IF the next stop date/time is later. No change will be made if it is
	sooner. This only applies to programmed starts.
 
    The stop date and time for manual lights on the CS3000 is handled differently from
    the 2000e. The stop date and time are now visable and must be adjusted BEFORE turning
    on the light. In the 2000e, the values did not appear until the light was on. They
    could then be adjusted. The IRRI ON list is the reason for the change. Every 10 minute
    adjustment after the light gets put on the ON list would require token responses from
    the IRRI machine the user is at (one step less for web or mobile), followed by
	broadcast tokens to inform the other IRRI machines.
 
    The "Turn Lights On or Off" screen has a single button that toggles between "Turn On"
    and "Turn Off" depending on the energized state of the light. That means a user cannot
    extend the time a light will remain On from that screen without first turning it Off.
    All time extensions will be done from the mobile or web apps.
 
    For a Mobile or Web ON requests, if a light is already on the "ON list", it will not
    be turned off to add a new "ON list" entry. Instead, the stop date/time in the
	existing entry will be overwritten by the stop date/time specified by the new request.
 
    The stop time may not be today. To keep this modular, don't count on it being
    tomorrow. It could also be a "stop_2" time when we are processing a "start_1".
 
    If a start time exists but a stop time does not exist, the light will skip scheduled
    ON times. The user was notified of the issue on the screen at programming time, and is
	reminded of the issue in daily midnight alerts.
 
    If a stop time exists but a start time does not exist, the light will never be turned
    on programmatically. The user was notified of the issue on the screen at programming
	time. There are no daily alerts for this issue.
 
    There are a maximum of 48 lights in the system and each of the lights lists can have
    up to 48 entries so there is no worry we will run out. This does not mean MIST list
    entries are in any fixed order. Lights do not use weighted priorities. There is no
    list sorting. Lights are added to the list in order of their start times, and removed
	as they hit the stop date/time recorded in the list entry.  
 
*/

/* ---------------------------------------------------------- */
/*  					Mutex Requirements					  */
/* ---------------------------------------------------------- */
/**
    Since this file has become a convenient location for misc Lights info, here are all
	the Mutex calls used by lights along with the structures and variables they protect.
 
	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );
    LIGHT_STRUCT
   	light_list_hdr
	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX, );
 
	xSemaphoreTakeRecursive( list_foal_lights_recursive_MUTEX, portMAX_DELAY );
	foal_lights
	FOAL_LIGHTS_BATTERY_BACKED_STRUCT
	header_for_foal_lights_ON_list
	header_for_foal_lights_with_action_needed_list
	xSemaphoreGiveRecursive( list_foal_lights_recursive_MUTEX );
 
	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );
	irri_lights
	IRRI_LIGHTS
	header_for_irri_lights_ON_list
	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );

	xSemaphoreTakeRecursive( lights_preserves_recursive_MUTEX, portMAX_DELAY );
	lights_preserves
	LIGHTS_BB_STRUCT
   	xSemaphoreGiveRecursive( lights_preserves_recursive_MUTEX );
 
	xSemaphoreTakeRecursive( lights_report_completed_records_recursive_MUTEX, portMAX_DELAY );
	lights_report_data_completed
	COMPLETED_LIGHTS_REPORT_DATA_STRUCT
	LIGHTS_REPORT_RECORD
	xSemaphoreGiveRecursive( lights_report_completed_records_recursive_MUTEX );
 
*/
   

/* ---------------------------------------------------------- */
/*  				FOAL Pointer Functions					  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Used to determine if a light llc is on a specific list. Returns a pointer
	to the matching list entry or a NULL if the box and output are not on the specified list.

    @CALLER MUTEX REQUIREMENTS The caller of this function must hold the
    list_foal_lights_recursive_MUTEX. As we are counting on a stable list subsequent to the
    determination made within this function.
	
    @EXECUTED Executed within the context of the TD_CHECK task. And may also be executed
    from within the COMM_MNGR task.
	
    @RETURN A pointer to the llc for the specified light. Returns NULL if none the llc is
	not on the specified list. 
	
	@AUTHOR 8/3/2015 mpd
*/
/* ---------------------------------------------------------- */
static LIGHTS_LIST_COMPONENT *nm_FOAL_LIGHTS_find_this_light_on_the_list( MIST_LIST_HDR_TYPE_PTR plist_hdr, const UNS_32 pbox_index_0, const UNS_32 poutput_index_0 )
{
	LIGHTS_LIST_COMPONENT	*llc_ptr = NULL;

	// 8/4/2015 mpd : The ON and action needed lists for lights contain 0 - 48 entries. They are linked in chronological
	// order as they are added and removed. There is no reordering or weighing for lights. The lists are populated with
	// the 48 llc array elements. These are in a fixed location allowing direct access with a formula.

	// Perform some local range checking to preserve incorrect values for debugging.
	if( ( pbox_index_0 < MAX_CHAIN_LENGTH ) && ( poutput_index_0 < MAX_LIGHTS_IN_A_BOX ) )
	{
		// Get the llc for this light.
		llc_ptr = &foal_lights.llcs[ LIGHTS_get_lights_array_index( pbox_index_0, poutput_index_0 ) ];  

		if( llc_ptr != NULL )
		{
			// Look at the list header for the list we are checking. If it's not NULL, the target light is on that list. 
			// Return the calculated llc pointer. Clear the llc pointer if the header is NULL.

			if( plist_hdr == &foal_lights.header_for_foal_lights_ON_list ) 
			{
				if( llc_ptr->list_linkage_for_foal_lights_ON_list.pListHdr == NULL )
				{
					llc_ptr = NULL;
				}
			}
			else
			if( plist_hdr == &foal_lights.header_for_foal_lights_with_action_needed_list )
			{
				if( llc_ptr->list_linkage_for_foal_lights_action_needed_list.pListHdr == NULL )
				{
					llc_ptr = NULL;
				}
			}
			else
			{
				llc_ptr = NULL;

				// Programming error.
				Alert_Message( "F_L: invalid list pointer" );
			}
		}
	}
	else
	{
		// 8/18/2015 mpd : TODO: This can go away after development.
		Alert_Message( "F_L: Bad inputs to find_on_a_list");
	}
	
	return( llc_ptr );

}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Returns the llc pointer for the specified light. The box and output fields
	are initialized. All other fields are cleared before exiting.

    @CALLER MUTEX REQUIREMENTS The caller of this function must hold the
    list_foal_lights_recursive_MUTEX. As we are counting on a stable list subsequent to the
    determination made within this function.
	
    @EXECUTED Executed within the context of the TD_CHECK task. And may also be executed
    from within the COMM_MNGR task.
	
    @RETURN A pointer to the llc for the specified light.  
	
	@AUTHOR 8/3/2015 mpd
*/
static LIGHTS_LIST_COMPONENT *nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light( const UNS_32 box_index_0, const UNS_32 output_index_0, BOOL_32 initialize )
{
	LIGHTS_LIST_COMPONENT	*llc_ptr = NULL;

	// 8/4/2015 mpd : The ON and action needed lists for lights contain 0 - 48 entries. They are linked in chronological
	// order as they are added and removed. There is no reordering or weighing for lights. The lists are populated with
	// the 48 llc array elements. These are in a fixed location allowing direct access with a formula.
	
	// Perform some local range checking to preserve incorrect values for debugging.
	if( ( box_index_0 < MAX_CHAIN_LENGTH ) && ( output_index_0 < MAX_LIGHTS_IN_A_BOX ) )
	{
		// Get the llc for this light.
		llc_ptr = &foal_lights.llcs[ LIGHTS_get_lights_array_index( box_index_0, output_index_0 ) ];  

		if( llc_ptr != NULL )
		{
			if( initialize )
			{
				// 8/4/2015 mpd : TBD: Is there anything we might want to save from the last time this light was on a list?
				// Fully clean it.
				memset( llc_ptr, 0, sizeof(LIGHTS_LIST_COMPONENT) );

				// We just wiped the llc contents. Set values for the static llc fields.
				llc_ptr->box_index_0    = box_index_0;
				llc_ptr->output_index_0 = output_index_0;
			}
		}
		else
		{
			// Programming error.
			Alert_Message( "F_L: llc pointer failure" );
		}
	}
	else
	{
		// 8/18/2015 mpd : TODO: This can go away after development.
		Alert_Message( "F_L: Bad inputs to return llc pointer");
	}

	return( llc_ptr );

}


/* ---------------------------------------------------------- */
/*  				FOAL Utility Functions					  */
/* ---------------------------------------------------------- */

// 8/6/2015 mpd : TODO: Complete this comment block.
/* ---------------------------------------------------------- */
/**
    @CALLER MUTEX REQUIREMENTS The caller of this function must hold the
    list_foal_lights_recursive_MUTEX. As we are counting on a stable list subsequent to the
    determination made within this function.
*/
static BOOL_32 __foal_lights_maintenance_may_run( void )
{
	BOOL_32	rv;
	
	rv = ( true );
	
	// This function follows the same policy used for stations. Let the lists get in sync. Over on the IRRI side there 
	// may be turn OFFs that need to take place or list removals that need to happen. It seems better to let those 
	// activities take palce before trying to yet turn on other lights. 
	if( foal_lights.header_for_foal_lights_with_action_needed_list.count != 0 )
	{
		rv = ( false );	
	}

	return( rv );

}


/* ---------------------------------------------------------- */
/*  			FOAL Date and Time Functions				  */
/* ---------------------------------------------------------- */

// 7/30/2015 mpd : TODO: Update this comment
static BOOL_32 FOAL_LIGHTS_set_stop_date_and_time_to_furthest( UNS_16 *target_date, UNS_32 *target_time, const UNS_32 stop_date, const UNS_32 stop_time )
{
	BOOL_32 target_was_modified;

	target_was_modified = (false);

	// ----------

	// 7/30/2015 mpd : Compare the target date and time with the stop values. Replace the target 
	// values if the stop values are farther in the future. 
	//
	// 7/20/2017 ajv : The original logic (*target_date <= stop_date) did not work. It only
	// allowed lights to turn on if the stop time was later than the start time, regardless of
	// whether the stop date was further in the future. There was no way to schedule lights to 
	// turn on tonight and turn off tomorrow morning. 
	if( *target_date == stop_date )
	{
		// 7/20/2017 ajv : The start and stop dates are the same so make sure the stop time is later
		// than the start time. Also verify the stop date is not set to OFF
		if( (*target_time < stop_time) && (stop_time < SCHEDULE_OFF_TIME) )
		{
			*target_date = stop_date;
			*target_time = stop_time;

			target_was_modified = (true);
		}
	}
	else if( *target_date < stop_date )
	{
		// 7/20/2017 ajv : The stop date is in the future. No need to compare the times but we 
		// should at least verify the stop date is not set to OFF.
		if( stop_time < SCHEDULE_OFF_TIME )
		{
			*target_date = stop_date;
			*target_time = stop_time;

			target_was_modified = (true);
		}
	}

	return( target_was_modified );
}

#define FOAL_LIGHTS_INVALID_SOONEST_VALUE	( 0x0FFFFFFF )

/* ---------------------------------------------------------- */
/**
 * Determines if a valid stop time exists anywhere in the 14 day schedule. If multiple
 * stop times exist, the date and time closest to the reference time will be set using
 * parameter pointers. 
 * 
 * @EXECUTED Executed within the context of the TD_CHECK task.
 *	
 * @CALLER MUTEX REQUIREMENTS list_lights_recursive_MUTEX.
 *
 * @param void
 *
 * @return BOOL_32 Indicates if a valid stop time exists in the 14 day schedule.
 *
 * @author 7/27/2015 mpd
 * 
 * @revisions
 *    7/27/2015 Initial release
 */
static BOOL_32 nm_FOAL_LIGHTS_get_soonest_stop_time( const DATE_TIME_COMPLETE_STRUCT *const pdtcs_ptr, LIGHT_STRUCT *const lls_ptr, UNS_32 *soonest_stop_date, UNS_32 *soonest_stop_time )
{
	UNS_32 day_counter, todays_index;
	UNS_32 temp_stop_date = FOAL_LIGHTS_INVALID_SOONEST_VALUE;
	UNS_32 temp_stop_time = FOAL_LIGHTS_INVALID_SOONEST_VALUE;
	UNS_32 stop_time_in_schedule;
	BOOL_32 stop_1_is_usable = false;
	BOOL_32 stop_2_is_usable = false;

	// This function returns the earliest stop time on the schedule for this light. It may be later today, or up to 14 
	// days from now. It can be in "stop_time_1" or "stop_time_2" fields. We have to look at both daily values since 
	// there is no restrictions on the time order. There may be no stop time scheduled.

	// Determine what day this is in the 14 day lights schedule.
	todays_index = LIGHTS_get_day_index( pdtcs_ptr->date_time.D );

	// Get the first stop time on this day. Use a local to save 3 more calls to the "get" function.
	stop_time_in_schedule = LIGHTS_get_stop_time_1( lls_ptr, todays_index );

	if( stop_time_in_schedule < SCHEDULE_OFF_TIME )
	{
		// This is a valid stop time. Is it later today, or did it already pass? "Now" counts as later today.
		if( stop_time_in_schedule >= pdtcs_ptr->date_time.T )
		{
			// This stop time is later today. Save it.
			stop_1_is_usable = true;
			temp_stop_date = pdtcs_ptr->date_time.D;
			temp_stop_time = stop_time_in_schedule;
		}
	}

	// Get the last stop time on this day. Use the same local.
	stop_time_in_schedule = LIGHTS_get_stop_time_2( lls_ptr, todays_index );

	if( stop_time_in_schedule < SCHEDULE_OFF_TIME )
	{
		// This is a valid stop time. Is it later today, or did it already pass? "Now" counts as later today.
		if( stop_time_in_schedule >= pdtcs_ptr->date_time.T )
		{
			// This stop time is later today.  
			stop_2_is_usable = true;

			// It it earlier than what we already have saved? 
			if( temp_stop_time > stop_time_in_schedule )
			{
				temp_stop_date = pdtcs_ptr->date_time.D;
				temp_stop_time = stop_time_in_schedule;
			}
		}
	}

	if( !stop_1_is_usable && !stop_2_is_usable )
	{
		// There is not a valid stop time later today. Search the 14 day schedule starting with tomorrow. Note the "<="
		// for the loop end condition. The two stop times on today's schedule were either invalid or already passed. We
		// need to look at them again as day 14 times. The extra loop will do it.
		for( day_counter = 1; day_counter <= LIGHTS_SCHEDULE_DAYS_MAX; ++day_counter )
		{
			// The loop counter is cycling 1 - 13. "Tomorrow" is somewhere in the circular 0 - 13 daily schedule based
			// on "todays_index". It needs to wrap as we pass 13. Use the modulo operator for the wrap. 
			stop_time_in_schedule = LIGHTS_get_stop_time_1( lls_ptr, ( ( todays_index + day_counter ) % LIGHTS_SCHEDULE_DAYS_MAX ) );

			if( stop_time_in_schedule < SCHEDULE_OFF_TIME )
			{
				stop_1_is_usable = true;

				// Save this valid time value. We will use it if nothing sooner is found.
				temp_stop_date = ( pdtcs_ptr->date_time.D + day_counter );
				temp_stop_time = stop_time_in_schedule;
			}

			stop_time_in_schedule = LIGHTS_get_stop_time_2( lls_ptr, ( ( todays_index + day_counter ) % LIGHTS_SCHEDULE_DAYS_MAX ) );

			if( stop_time_in_schedule < SCHEDULE_OFF_TIME )
			{
				// This is a valid time. It it earlier than what we already have saved? 
				if( temp_stop_time > stop_time_in_schedule )
				{
					stop_2_is_usable = true;

					temp_stop_date = ( pdtcs_ptr->date_time.D + day_counter );
					temp_stop_time = stop_time_in_schedule;
				}
			}

			if( stop_1_is_usable || stop_2_is_usable )
			{
				// We found a date and time. Quit looking.
				break;
			}
		}
	}

	if( stop_1_is_usable || stop_2_is_usable )
	{
		// We found a date and time.
		*soonest_stop_date = temp_stop_date;
		*soonest_stop_time = temp_stop_time;
	}
	else
	{
		// This should never happen. Force the stop time to "now".
		*soonest_stop_date = pdtcs_ptr->date_time.D;
		*soonest_stop_time = pdtcs_ptr->date_time.T;
		Alert_Message( "F_L: No Stop Time" );
	}

	return( stop_1_is_usable || stop_2_is_usable );

}

/* ---------------------------------------------------------- */
/*  			FOAL Initialization Functions				  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This function is called on a program start. Unlike irrigation, we do not
    want to turn OFF all lights in the chain on restart. Lighting can take a significant
	amount of time to turn back on. 
 
    Also unlike irrigation, lights does not have an "all lights" list containing a queue
	of lights waiting to go ON. There is a battery backed lights ON list that survived this
    restart along with the action nedded list. We are not going to clear them.
 
    This box just restarted. Obviously, any of the 4 light outputs that were ON went
    OFF momentarily. We can't do anything about that. Remote IRRI machines may not have
    experienced this restart. In that scenario, with a little effort, we may be able to
    handle a brief chain-down event to keep remote lights up and running. The first TOKEN
    to the IRRI machines will tell them to accept a new lights list. The IRRI side should
    NOT restart lighting! It must use the new lights list to make changes to existing
	light outputs.
 
	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the APP_STARTUP task as part of progam startup
    initialization. Also executed when the chain transitions to DOWN.

	@RETURN (none)

	@AUTHOR 8/3/2015 mpd
*/
static void FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down( BOOL_32 pcompletely_wipe )
{
	// Then one thing we do is send the lights list over to this controllers irri machine.

	// The "pcompletely_wipe" should rarely be true. Notes in calling functions indicate this only happens "if we are in 
	// forced and performing a periodic rescan, if the chain comes up new we are going to completely wipe the lights". 
	// If we are performing our scan as part of a normal restart if the chain is the same, we leave the ON and action 
	// needed lists unchanged.
	
	// ----------
	
	LIGHTS_LIST_COMPONENT	*lllc_ptr;

	UNS_32 box_index, output_index;

	// ----------
	
	// Protect the lists as we traverse and change them.
	xSemaphoreTakeRecursive( list_foal_lights_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
	
	// ----------
	
	// When the chain goes down we completely wipe the list.
	if( pcompletely_wipe )
	{
		// 5/11/2012 rmd : No dynamic memory to free in these lists. Permantely allocated battery
		// backed array of llcs. Therefore re-initializing the lists WILL NOT LEAD TO MEMORY LEAKS.
		
		// 8/3/2015 mpd : Unlike the "foal_irri.c" code, there is no "list_of_foal_all_lights" because there is no limit
		// to the number of lights that can be ON at once. There is no weighting, no mow days, no rain, freeze switch, 
		// or MLB issues. If a light is to go ON, we put it on the "list_of_foal_lights_ON" list.
		nm_ListInit( &foal_lights.header_for_foal_lights_ON_list, offsetof( LIGHTS_LIST_COMPONENT, list_linkage_for_foal_lights_ON_list ) );
	
		// Initialize the action needed list.
		nm_ListInit( &foal_lights.header_for_foal_lights_with_action_needed_list, offsetof( LIGHTS_LIST_COMPONENT, list_linkage_for_foal_lights_action_needed_list ) );
	
		// 8/10/2015 mpd : Resets for the reentry Booleans were not done for stations at the time this code was cloned 
		// for lights. This fix is intended to be in the approximate place is was added for stations. Relocate it if
		// necessary.
		foal_lights.header_for_foal_lights_ON_list.InUse = ( false );
		foal_lights.header_for_foal_lights_with_action_needed_list.InUse = ( false );

		// To make sure ALL the list links are 0. 
		memset( &foal_lights.llcs, 0, sizeof(foal_lights.llcs) );
	}
	else
	{
		#if defined(LIST_DEBUG) && LIST_DEBUG
			// 8/10/2015 rmd : Now here is a very subtle issue that only can happen with battery backed
			// list headers! If there is a code restart (power failure or whatever) at just the right
			// time when one of the list INSERT or DELETE functions are executing for a list with a
			// battery backed list header, the IN_USE flag could be set true. Well upon a startup being
			// battery backed that flag is still set (true) and of course the list is not in use. And
			// with this flag SET (true) the list functions (insert or delete) will report a FATAL
			// error. So picture the flag is set. And the controller keeps trying to perform a list
			// insert or delete and that triggers a restart. The right thing to do is to set these
			// flags, rightfully so, (false) upon code startup.
			//
			// 8/10/2015 rmd : Note - I have verified, as of this point in time, these 3 list are the
			// only battery backed lists in the system. Ahh ... there is actually a fourth one. The
			// LIGHTS list.
			foal_lights.header_for_foal_lights_ON_list.InUse = (false);

			foal_lights.header_for_foal_lights_with_action_needed_list.InUse = (false);

		#endif
	}
	
	// Loop through every light in the system. The following initialization is performed independent of the wipe.
	for( box_index = 0; box_index < MAX_CHAIN_LENGTH; ++box_index )
	{
		for( output_index = 0; output_index < MAX_LIGHTS_IN_A_BOX; ++output_index )
		{
			// Get the llc for this light.
			lllc_ptr = &foal_lights.llcs[ LIGHTS_get_lights_array_index( box_index, output_index ) ];
			  
			// Set values for the static llc fields.
			lllc_ptr->box_index_0 = box_index;
			lllc_ptr->output_index_0 = output_index;

			// If this light is on the ON list, we need to inform the IRRI side.
			if( ( nm_FOAL_LIGHTS_find_this_light_on_the_list( &( foal_lights.header_for_foal_lights_ON_list ), box_index, output_index ) ) != NULL )
			{
				// This light is on the ON list. Sent this entry to the IRRI side in the new lights list.
				lllc_ptr->xfer_to_irri_machines = ( true );
			}
		}
	}

	// -----------------------------

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
	
	xSemaphoreGiveRecursive( list_foal_lights_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION A function called from the COMM_MNGR context and KEY_PROCESS task context. Really
    could be called from any task. But is rarely and very specially used. Generally during chain
    building when NEW controllers are found or when the user switches a controller from being in
	a chain to a standalone.
	
	@RETURN (none)

	@EXECUTED Executed within the context of the STARTUP task.

	@AUTHOR 2011.10.21 rmd
 
	@REVISIONS 8/3/2015 mpd Adapted station function for lights.
*/
extern void FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights( void )
{
	FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down( ( true ) );
	
	IRRI_LIGHTS_restart_all_lights();

}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION The initialization function called on EVERY program start. It may be called
    a second time if the user requests a full restart.
	
	@RETURN (none)

	@EXECUTED Executed within the context of the STARTUP task.

	@AUTHOR 2011.10.21 rmd
 
	@REVISIONS 8/3/2015 mpd Adapted station function for lights.
*/ 
void init_battery_backed_foal_lights( const BOOL_32 pforce_the_initialization )
{
	xSemaphoreTakeRecursive( list_foal_lights_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	// Forced intiialization is only set ture if pre/post string test fails or as when complete reset requested by the 
	// user.
	if( pforce_the_initialization || (strncmp( foal_lights.verify_string_pre, FOAL_LIGHTS_PRE_TEST_STRING, sizeof(FOAL_LIGHTS_PRE_TEST_STRING) ) != 0) )
	{
		Alert_battery_backed_var_initialized( "Foal Lights", pforce_the_initialization );
		
		// ----------

		// Zero the whole foal_lights structure.
		memset( &foal_lights, 0x00, sizeof(FOAL_LIGHTS_BATTERY_BACKED_STRUCT) );
		
		// ----------
		
		FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down( ( true ) );
		
		// ----------

		strlcpy( foal_lights.verify_string_pre, FOAL_LIGHTS_PRE_TEST_STRING, sizeof(foal_lights.verify_string_pre)  );
	}
	else
	{
		#if ALERT_SHOW_ROUTINE_INITIALIZATIONS_DURING_STARTUP
			Alert_battery_backed_var_valid( "Foal Lights structure" );
		#endif

		// ----------
		
		// Coming from a power fail this function is executed. And you might expect to reset aspects of the lights list.
		FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down( ( false ) );
	}
	
	// ----------
	
	xSemaphoreGiveRecursive( list_foal_lights_recursive_MUTEX );

}


/* ---------------------------------------------------------- */
/*  				FOAL Light OFF Functions				  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Will add the pllc lights list item to the action needed list.

    @CALLER_MUTEX_REQUIREMENTS The caller MUST be holding the foal_lights_recursive_MUTEX. We
    are messing with the action needed list.
	
	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task while performing the
    foal_lights list maintenance.

	@RETURN (none)

	@ORIGINAL 2012.03.12 rmd
 
	@REVISIONS 8/3/2015 mpd Adapted station function for lights.
*/
static void nm_FOAL_LIGHTS_add_to_action_needed_list( LIGHTS_LIST_COMPONENT *pllc )
{
	// The FOAL side maintains this action needed list to use for building irrigation tokens. The IRRI side acts on the
	// action needed field contained in the IRRI ON list. The token already contains ON list content complete with the
	// specific ON reason. There is not need to send it as an additional action. It could even cause errors if the ON 
	// action arrived before the light made it into the IRRI ON list. Only OFF actions need to be sent to the IRRI side
	// using this function.
	if( ( pllc->action_needed != ACTION_NEEDED_TURN_ON_programmed_light ) &&
		( pllc->action_needed != ACTION_NEEDED_TURN_ON_manual_light     ) &&
		( pllc->action_needed != ACTION_NEEDED_TURN_ON_mobile_light     ) &&
		( pllc->action_needed != ACTION_NEEDED_TURN_ON_test_light       ) &&
	    ( pllc->action_needed != ACTION_NEEDED_NONE ) )
	{
		// See if the target light's llc is already on the action needed list.
		if( !nm_OnList( &foal_lights.header_for_foal_lights_with_action_needed_list, ( void* )pllc ) )
		{
			// It is not on the action needed list. Add it.
			nm_ListInsertTail( &foal_lights.header_for_foal_lights_with_action_needed_list, ( void* )pllc );

			// That's it. There are no other fields to set. Data in the action needed list is automatically broadcast to 
			// the IRRI side in the next token.
		}
		//else
		//{
			// There is no problem if the light's llc is already on the action needed list. It can only be on there 
			// once. The caller to this function just updated all the important fields. Whatever it was originally put 
			// on the list for no longer matters. 
		//}
	}
	else
	{
		// 8/17/2015 mpd : TODO: Remove after development.
		Alert_Message( "F_L: Non-off sent to add to A N list" );
	}

}

/* ---------------------------------------------------------- */
static LIGHTS_LIST_COMPONENT* nm_remove_from_the_lights_ON_list( LIGHTS_LIST_COMPONENT *const pllc_ptr )
{
	LIGHTS_LIST_COMPONENT	*next_llc_ptr;
	
	// ----------
	
	next_llc_ptr = NULL;
	
	// ----------
	
	// See if the light is on the ON list.
	if( nm_OnList( &( foal_lights.header_for_foal_lights_ON_list ), ( void* )pllc_ptr ) == ( true ) )
	{
		// Get the next llc for the return value.
		next_llc_ptr = nm_ListGetNext( &( foal_lights.header_for_foal_lights_ON_list ), ( void* )pllc_ptr );
		
		if( nm_ListRemove( &( foal_lights.header_for_foal_lights_ON_list ), ( void* )pllc_ptr ) != MIST_SUCCESS )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "F_L: Error removing from ON list" );
		}

		// Any modifications to the ON list require an action needed entry to keep the IRRI side in sync.
		nm_FOAL_LIGHTS_add_to_action_needed_list( pllc_ptr );
			
	}  // END the pllc is on the ON list

	// ----------
	
	return( next_llc_ptr );
}

/* ---------------------------------------------------------- */
static LIGHTS_LIST_COMPONENT* nm_turn_OFF_this_light( LIGHTS_LIST_COMPONENT *const pllc_ptr )
{
	LIGHTS_LIST_COMPONENT	*next_llc_ptr;

	// ----------

	// Turn OFF the light if it is ON. 
	pllc_ptr->light_is_energized = false;

	next_llc_ptr = nm_remove_from_the_lights_ON_list( pllc_ptr );
	
	// ------------
	
	// MUST tell the IRRI side about what is happening on the FOAL side. It will already be on the list if the light was 
	// ON when the prior function call was made. And that's okay. This covers the case where it was OFF.
	nm_FOAL_LIGHTS_add_to_action_needed_list( pllc_ptr );
	
	// ----------
	
	// NOW - Remove from the lights list. That is what this function is all about after all. This does not affect the 
	// fact that the llc is on the ACTION NEEDED list.
	nm_ListRemove( &(foal_lights.header_for_foal_lights_ON_list), ( void* )pllc_ptr );

	// ----------
	
	return( next_llc_ptr );
}

/* ---------------------------------------------------------- */
/*  				FOAL Light ON Functions					  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION All lights attempting to energize in ANY way MUST pass through this
    function. The caller must already possess the foal light list MUTEX (though we take it
    again) because the caller has taken the next available llc and it isn't all the way
	filled out. Seems best to keep the list frozen.

    This function is invoked via two methods. First is local start time detection. For that
    matter any scheduled start detection. The second would be a UI initiated request to turn
    on a light. This request would arrive with the token.
	
    Following sucessful addition to the foal lights ON list such entries (in an abreviated
    form) must be sent to each controller in the chain. This allows for reports of what is
    energized to be shown at any controller but also to provide the saftey of making sure
    lights ON are turned OFF in a timely fashion when something goes wrong. A code bug of
    communication fault.

    @param pllc_ptr is a pointer to the LIGHTS_LIST_COMPONENT for the light we are adding.
    @param plgs_ptr is a pointer to the LIGHTS_GROUP_STRUCT for the light we are adding.
	 
    @CALLER MUTEX REQUIREMENTS The caller is expected to be holding THREE mutexs!!! The
    list_lights_recursive_MUTEX, the lights_preserves MUTEX, and the foal_lights MUTEX.

    @EXECUTED Executed within the context of the TD_CHECK task. And the COMM_MNGR task as
    requests come in to turn lights ON. Such as for TEST. And from the TD_CHECK task via
    our check for start times.

    @RETURN Returns ( true ) if added to the foal lights ON list. Returns ( false ) if not. 
    Whn returns ( false ) the target light will not be energized. Also returns ( true ) for the
    case of being in the ON list for manual operation weither or not the remaining time was
	adjusted.
	
	@AUTHOR 2012.10.10 rmd
 
	@REVISIONS 8/3/2015 mpd Adapted station function for lights.
*/
static BOOL_32 _nm_nm_nm_add_to_the_lights_ON_list( LIGHTS_LIST_COMPONENT *const pllc_ptr, LIGHT_STRUCT *const plgs_ptr )
{ 
	BOOL_32	success = ( false );
	
	// The following items MUST already be filled out in the LLC.
	//   BOX_INDEX_0
	//   OUTPUT_INDEX_0
	//   ACTION REASON
	//   REASON IN LIST (info only, lights don't do weighting)
	//   STOP TIME & DATE (required for programmed or manual lighting)
	
	// Preparation to add it to the list after it is filled out.
	xSemaphoreTakeRecursive( list_foal_lights_recursive_MUTEX, portMAX_DELAY );

	LIGHTS_LIST_COMPONENT	*lllc_ptr;

	// Determine if the target light is already on the ON list. Stations allow multiple entries for multiple reasons.
	// Lights don't. A light can only be on the list once, and for the last reason that entry was touched.
	if( ( lllc_ptr = nm_FOAL_LIGHTS_find_this_light_on_the_list( &( foal_lights.header_for_foal_lights_ON_list ), pllc_ptr->box_index_0, pllc_ptr->output_index_0 ) ) == NULL )
	{
		// The target light is not on the target list. Add it.
		if( nm_ListInsertTail( &foal_lights.header_for_foal_lights_ON_list, ( void* )pllc_ptr ) == MIST_SUCCESS )
		{
			// Indicate this was handled normally.
			success = ( true );

			// Indicat the light has not been energized yet. The maintenance function will complete the operation.
			pllc_ptr->light_is_energized = ( false );

			// Mark for inclusion in the next outgoing token. Remember the token (from the master) is broadcast to all 
			// controllers in the chain.
			pllc_ptr->xfer_to_irri_machines = ( true );
		}
	}
	else
	{
		// The target light is on the target list. We will modify the existing entry. Indicate this was handled 
		// normally. 
		success = ( true );

		// Leave this field alone. If the light is ON, nothing more will be done to turn it on. If it has not gone ON 
		// yet, touching this would prevent it from ever going ON.
		//pllc_ptr->light_is_energized = ( true );

		// Mark for inclusion in the next outgoing token. It's possible the current action will leave the llc unchanged,
		// but that's unlikely.
		pllc_ptr->xfer_to_irri_machines = ( true );
	}

	// The llc fields not addressed here are left to the caller to handle (before or after this function). The job here 
	// was to get this light's llc linked to the ON and action needed lists. That's accomplished.

	xSemaphoreGiveRecursive( list_foal_lights_recursive_MUTEX );
		
	return( success );

}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called when the token is being assembled.

    @CALLER_MUTEX_REQUIREMENTS The caller should be holding the list_foal_lights_recursive_MUTEX to protect
    the pllc_ptr list item data. AND the lights_preserves_recursive_MUTEX as we are going to 
    set values in the lights_preserves.
	
	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Called with the context of the TD_CHECK task. As part of the foal_lights list
    maintenance function. When it is decided to turn ON a light.

	@RETURN (none)

	@ORIGINAL 2012.04.26 rmd
 
	@REVISIONS 8/3/2015 mpd Adapted station function for lights.
*/
static void _nm_nm_FOAL_LIGHTS_complete_the_action_needed_turn_ON( LIGHTS_LIST_COMPONENT *pllc_ptr )
{
	UNS_32 array_index = LIGHTS_get_lights_array_index( pllc_ptr->box_index_0, pllc_ptr->output_index_0 );

	if(	pllc_ptr->action_needed != ACTION_NEEDED_NONE )
	{
		// This is the only (non-test) location where we clear the shorted output Boolean. If the output is still 
		// shorted, the TPMmicro will shut it off immediately after energizing and start the whole notification process 
		// over again.
		lights_preserves.lbf[ array_index ].shorted_output = ( false );

		// This is the only (non-test) location where we clear the no current output Boolean. If the output is still 
		// open, the TPMmicro will set this bit again and start the whole notification process over again. There is no 
		// need to turn off a light with a no current output.
		lights_preserves.lbf[ array_index ].no_current_output = ( false );

		// This is the only place where we increment the cycle count. Logically, this should be done by the IRRI side as 
		// the light is energized. It's done here because light preserves are implemented on the FOAL side and it's not
		// worth adding content to the token response. This covers programmed, manual, and mobile ON requests.
		lights_preserves.lights[ array_index ].lrr.number_of_on_cycles++;

		if( nm_OnList( &foal_lights.header_for_foal_lights_ON_list, pllc_ptr) == ( false ) )
		{
			Alert_item_not_on_list( pllc_ptr, foal_lights.header_for_foal_lights_ON_list );
		}
		else
		{
			// We are sending the action_needed record to turn ON the light. This is the only place we set this true. 
			// The "lights_preserves" energized flag is on the IRRI side. It will be set when the appropriate IRRI box
			// turns on the light.  
			pllc_ptr->light_is_energized = ( true );

		}  // END of if the pllc_ptr is on the list

	} // END action is needed

}
	
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION A user selects a light ON action from a mobile device or web interface.
    That information arrives at the master through a communication port. The master (foal
    machine) comm_mngr upon receipt of the command will call this function to actually
	turn on the light.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the comm_mngr task when a mobile ON command is
	received.
	
	@RETURN (none)

	@ORIGINAL 2012.08.30 rmd
 
	@REVISIONS 8/3/2015 mpd Adapted station function for lights.
*/
extern void FOAL_LIGHTS_initiate_a_mobile_light_ON( LIGHTS_ON_XFER_RECORD *lxr )
{
	UNS_32 box_index_0    = ( lxr->light_index_0_47 / MAX_LIGHTS_IN_A_BOX );
	UNS_32 output_index_0 = ( lxr->light_index_0_47 % MAX_LIGHTS_IN_A_BOX );

	LIGHT_STRUCT	*lls_ptr;

	// ----------

	if( lxr->light_index_0_47 < MAX_LIGHTS_IN_NETWORK )
	{
		xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

		xSemaphoreTakeRecursive( lights_preserves_recursive_MUTEX, portMAX_DELAY );

		xSemaphoreTakeRecursive( list_foal_lights_recursive_MUTEX, portMAX_DELAY );

		// ----------

		// Note: The stop date/time is recorded in the ON list entry at the time the light is added to the list or 
		// updated by another request. The stop date/time included in a Manual,	Mobile, or Test request will override 
		// any previously saved stop date/time in the ON list entry.

		lls_ptr = nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index( box_index_0, output_index_0 );

		if( lls_ptr == NULL )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "F_L: lls not found" );
		}
		else
		{
			// ----------

			LIGHTS_LIST_COMPONENT	*lllc_ptr;

			// First check to see if the light is already on the ON list.
			if( ( lllc_ptr = nm_FOAL_LIGHTS_find_this_light_on_the_list( &( foal_lights.header_for_foal_lights_ON_list ), box_index_0, output_index_0 ) ) != NULL )
			{
				lllc_ptr->reason_in_list = IN_LIST_FOR_MOBILE;
				lllc_ptr->action_needed = ACTION_NEEDED_TURN_ON_mobile_light;

				// A mobile request overrides the stop time for a light on the ON list. Copy the stop date and time from 
				// the transfer record.
				lllc_ptr->stop_datetime_d = lxr->stop_datetime_d;
				lllc_ptr->stop_datetime_t = lxr->stop_datetime_t;

				// Mark for inclusion in the next outgoing token. Remember the token (from the master) is broadcast 
				// to all controllers in the chain.
				lllc_ptr->xfer_to_irri_machines = ( true );
			}
			else
			{
				// Get the llc for this light.
				lllc_ptr = nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light( box_index_0, output_index_0, false );

				if( lllc_ptr == NULL )
				{
					ALERT_MESSAGE_WITH_FILE_NAME( "F_L: llc not available" );
				}
				else
				{
					lllc_ptr->reason_in_list = IN_LIST_FOR_MOBILE;
					lllc_ptr->action_needed = ACTION_NEEDED_TURN_ON_mobile_light;

					// Copy the stop date and time from the transfer record.
					lllc_ptr->stop_datetime_d = lxr->stop_datetime_d;
					lllc_ptr->stop_datetime_t = lxr->stop_datetime_t;

					// Add the llc to the ON list.
					if( _nm_nm_nm_add_to_the_lights_ON_list( lllc_ptr, lls_ptr ) )
					{
						Alert_light_ID_with_text( box_index_0, output_index_0, LIGHTS_TEXT_MOBILE_ON );
					}

					// Indicate the light has not been energized yet. The maintenance function will complete the 
					// operation.
					lllc_ptr->light_is_energized = ( false );

					// Mark for inclusion in the next outgoing token. Remember the token (from the master) is broadcast 
					// to all controllers in the chain.
					lllc_ptr->xfer_to_irri_machines = ( true );
				}  
			}
		}

		xSemaphoreGiveRecursive( list_foal_lights_recursive_MUTEX );

		xSemaphoreGiveRecursive( lights_preserves_recursive_MUTEX );

		xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );
	}
	else
	{
		Alert_Message( "F_L: Bad index in mobile on" );
	}

}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION A user selects a light ON action from a mobile device or web interface.
    That information arrives at the master through a communication port. The master (foal
    machine) comm_mngr upon receipt of the command will call this function to actually
	turn on the light.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the comm_mngr task when a mobile ON command is
	received.
	
	@RETURN (none)

	@ORIGINAL 2012.08.30 rmd
 
	@REVISIONS 8/3/2015 mpd Adapted station function for lights.
*/
extern void FOAL_LIGHTS_initiate_a_mobile_light_OFF( LIGHTS_OFF_XFER_RECORD *lxr )
{
	UNS_32 box_index_0    = ( lxr->light_index_0_47 / MAX_LIGHTS_IN_A_BOX );
	UNS_32 output_index_0 = ( lxr->light_index_0_47 % MAX_LIGHTS_IN_A_BOX );

	LIGHTS_LIST_COMPONENT	*lllc_ptr;

	// ----------

	if( lxr->light_index_0_47 < MAX_LIGHTS_IN_NETWORK )
	{
		xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

		xSemaphoreTakeRecursive( lights_preserves_recursive_MUTEX, portMAX_DELAY );

		xSemaphoreTakeRecursive( list_foal_lights_recursive_MUTEX, portMAX_DELAY );

		// ----------

		// Verify the light is on the ON list.
		if( ( lllc_ptr = nm_FOAL_LIGHTS_find_this_light_on_the_list( &( foal_lights.header_for_foal_lights_ON_list ), box_index_0, output_index_0 ) ) != NULL )
		{
			// 8/31/2015 mpd : The light is about to be removed from the list. Clear the reason.
			lllc_ptr->reason_in_list = nlu_IN_LIST_FOR_not_in_use;

			lllc_ptr->action_needed = ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_mobile_off;

			// Execute a turn OFF. This is where we actually turn OFF the light. It is removed from the ON list and added to 
			// the action needed list which is then sent to the IRRI machines in the next token. The IRRI box containing the 
			// target light will perform the actual turn OFF. This CS3000 centralized scheme takes the thinking away from 
			// the IRRI machines reducing them to slaves.
			lllc_ptr = nm_turn_OFF_this_light( lllc_ptr );

			Alert_light_ID_with_text( box_index_0, output_index_0, LIGHTS_TEXT_MOBILE_OFF );
		}
		else
		{
			// 8/12/2015 mpd : TBD: Is this necessary?
			//Alert_Message( "F_L: light is not on" );
		}

		xSemaphoreGiveRecursive( list_foal_lights_recursive_MUTEX );

		xSemaphoreGiveRecursive( lights_preserves_recursive_MUTEX );

		xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );
	}
	else
	{
		Alert_Message( "F_L: Bad index in mobile off" );
	}

}


/* ---------------------------------------------------------- */
/*  		Outgoing Token (FOAL to IRRI) Functions			  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION When lights are added to the foal lights list and also following a
    power failure during lighting, the lights in the llc are marked to send to each of
    the irri machines in the network. When the outgoing token is made this function is
    called. This function will allocate and fill the needed memory. Setting the pointer
    included as a argument to the start of the block. And will return the length of the
    block. The length reflects the size of the block to be placed within the token. The
    length should be returned as 0 if the block is not to be sent. The memory block will be
    dynamically allocated and must be freed after the caller takes a copy of the data and
    implants it into the message.
	
	@CALLER MUTEX REQUIREMENTS (none) 

    @EXECUTED Executed within the context of the COMM_MNGR task.
	
    @RETURN Set the pdata_ptr to the start of the block if there is data to ship, NULL other
    wise. The return value is the length of the block to ship. And should return 0 if there
    is no data to include within the token.

	@AUTHOR 2012.03.06 rmd
 
	@REVISIONS 8/3/2015 mpd Adapted station function for lights.
*/
extern UNS_32 FOAL_LIGHTS_load_lights_on_list_into_outgoing_token( UNS_8 **pdata_ptr )
{
	UNS_32	bytes_to_send;

	// lfol = Light From On List
	
	// Indicate nothing to send by setting botht the returned length and pointer argument to
	// 0.
	bytes_to_send = 0;
	
	*pdata_ptr = NULL;
	
	// Take this MUTEX because we are looping through the list and we do not want the list to change during our 
	// activity.
	xSemaphoreTakeRecursive( list_foal_lights_recursive_MUTEX, portMAX_DELAY );
	
	// We use an UNS_16 to save two bytes during the transmission.
	UNS_16	lnumber_of_lights = 0;
	
	LIGHTS_LIST_COMPONENT	*lllc_ptr;
	
	// Loop through the lights ON list.	
	lllc_ptr = nm_ListGetFirst( &foal_lights.header_for_foal_lights_ON_list );
	
	while( lllc_ptr != NULL )
	{
		// Is there a request to send this light?
		if( lllc_ptr->xfer_to_irri_machines == ( true ) )
		{
			lnumber_of_lights += 1;
			
			// There are a maximum of 48 lights in the system. If all hit a start time and are added to the list all at 
			// once, we would hit the recommended limit suggested for stations. Since the lights struct is much smaller 
			// than the stations struct, we are well within a reasonable token size.
			// 
			// Per the warning in stations code, we should ALSO prohibit attempted turn ON's when there are any llc's in 
			// the list that needs transfer. If we didn't take this step we could send a message to an IRRI machine to 
			// turn ON a light that wasn't yet in his list. And that would be flagged as an error.
		}
		
		lllc_ptr = nm_ListGetNext( &foal_lights.header_for_foal_lights_ON_list, lllc_ptr );
	}
	
	if( lnumber_of_lights > 0 )
	{
		// llength will also be used in a sanity check.
		UNS_32	llength;
		
		UNS_8	*ucp;
		
		// We send the light count short plus only the necessary pieces from the llc. At this time, the 
		// ACTION_NEEDED_LIGHTS_XFER_RECORD contains everything we need to send. Use it.
		llength = sizeof(UNS_16) + ( lnumber_of_lights * sizeof( LIGHTS_LIST_XFER_RECORD ) );
		
		ucp = mem_malloc( llength );

		*pdata_ptr = ucp;  // set the pointer for the caller
		
		bytes_to_send = llength;  // and set the return value now cause we know the length

		memcpy( ucp, &lnumber_of_lights, sizeof(UNS_16) );
		
		ucp += sizeof(UNS_16);
		
		llength -= sizeof(UNS_16);

		lllc_ptr = nm_ListGetFirst( &foal_lights.header_for_foal_lights_ON_list );
		
		while( lllc_ptr != NULL )
		{
			if( lllc_ptr->xfer_to_irri_machines == ( true ) )
			{
				if( lnumber_of_lights == 0)
				{
					// This is not a fatal error but we'll note for now.
					ALERT_MESSAGE_WITH_FILE_NAME( "FOAL light list must have changed" );
					
					break;  // We are done! So we don't overrun the memory space allocated.
				}
				
				lnumber_of_lights -= 1;
				
				LIGHTS_LIST_XFER_RECORD	lxr;
				
				lxr.stop_datetime_d  = lllc_ptr->stop_datetime_d;
				lxr.stop_datetime_t  = lllc_ptr->stop_datetime_t;
				lxr.light_index_0_47 = LIGHTS_get_lights_array_index( lllc_ptr->box_index_0, lllc_ptr->output_index_0 );
				lxr.action_needed    = lllc_ptr->action_needed;
				
				memcpy( ucp, &lxr, sizeof( LIGHTS_LIST_XFER_RECORD ) );
				
				ucp += sizeof( LIGHTS_LIST_XFER_RECORD );
				
				// Clear the request. 
				lllc_ptr->xfer_to_irri_machines = ( false );
				
				// Adjust the remaining length for post sanity check
				llength -= sizeof( LIGHTS_LIST_XFER_RECORD );
			}
			
			lllc_ptr = nm_ListGetNext( &foal_lights.header_for_foal_lights_ON_list, lllc_ptr );

		}  // END looping through the ON list
		
		if( lnumber_of_lights || llength )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "F_L: ON list token error" );
		}

	}  // END if there are lights to send

	xSemaphoreGiveRecursive( list_foal_lights_recursive_MUTEX );

	return( bytes_to_send );

}

/* ---------------------------------------------------------- */
extern UNS_32 FOAL_LIGHTS_load_lights_action_needed_list_into_outgoing_token( UNS_8 **pdata_ptr )
{
	UNS_32	rv;
	
	// 2012.05.06 rmd : The return value is the size of the xfer.
	rv = 0;
	
	*pdata_ptr = NULL;
	
	// 11/12/2014 rmd : Take the list MUTEX as we are traversing the action_needed list.
	xSemaphoreTakeRecursive( list_foal_lights_recursive_MUTEX, portMAX_DELAY );

	// 2012.05.06 rmd : This variable is purposely an UNS_16. We include the count in the
	// message and to save space we only have this as a 2-byte count.
	UNS_16	action_records_to_xfer;
	
	// 4/17/2012 rmd : Since we are using a broadcast TOKEN all the action_needed records on the
	// list go.
	action_records_to_xfer = foal_lights.header_for_foal_lights_with_action_needed_list.count;
	
	if( action_records_to_xfer > 0 )
	{
		// So there is at least one xfer to include. So build up the transfer.
		UNS_32	llength;
		
		UNS_8	*ucp;

		// Include the 2-bytes that says how many xfers are included. And for each record the size
		// of the sxru struct.
		llength = ( 2 + ( action_records_to_xfer * sizeof( LIGHTS_ACTION_XFER_RECORD ) ) );
		
		ucp = mem_malloc( llength );
		
		*pdata_ptr = ucp;  // set the pointer for the caller
		
		rv = llength;  // and set the return value now cause we know the length
		
			
		memcpy( ucp, &action_records_to_xfer, sizeof(UNS_16) );
		ucp += sizeof( UNS_16);
		llength -= sizeof( UNS_16);
		

		LIGHTS_LIST_COMPONENT	*lllc;
	
		lllc = nm_ListRemoveHead( &foal_lights.header_for_foal_lights_with_action_needed_list );

		while( lllc != NULL )
		{
			LIGHTS_ACTION_XFER_RECORD	llxr;
			
			// Zero the record to be clean.
			memset( &llxr, 0x00, sizeof( LIGHTS_ACTION_XFER_RECORD ) );

			llxr.light_index_0_47 = LIGHTS_get_lights_array_index( lllc->box_index_0, lllc->output_index_0 );
			llxr.action_needed   = lllc->action_needed;

			// ----------
			
			// Copy the completed action transfer record into the message.
			memcpy( ucp, &llxr, sizeof( LIGHTS_ACTION_XFER_RECORD ) );
			ucp += sizeof( LIGHTS_ACTION_XFER_RECORD );
			llength -= sizeof( LIGHTS_ACTION_XFER_RECORD );
			action_records_to_xfer -= 1;
			
			// ----------
			
			// Get the next entry off the list.
			lllc = nm_ListRemoveHead( &foal_lights.header_for_foal_lights_with_action_needed_list );
		}

		// ----------
		
		if( llength || action_records_to_xfer )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "F_L: A_N token error" );
		}

	}  // of if the number of action_needed light in the list is not 0
	
	xSemaphoreGiveRecursive( list_foal_lights_recursive_MUTEX );
	
	return( rv );

}

/* ---------------------------------------------------------- */
/*  	Incoming Token Response (IRRI to FOAL) Functions	  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void FOAL_LIGHTS_process_light_on_from_token_response( LIGHTS_ON_XFER_RECORD *lxr_ptr )
{
	UNS_32 box_index_0    = ( lxr_ptr->light_index_0_47 / MAX_LIGHTS_IN_A_BOX );
	UNS_32 output_index_0 = ( lxr_ptr->light_index_0_47 % MAX_LIGHTS_IN_A_BOX );

	LIGHT_STRUCT	*lls_ptr;

	// ----------

	// Executed within the context of the comm_mngr task when a manual ON command is received.
	
	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( lights_preserves_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( list_foal_lights_recursive_MUTEX, portMAX_DELAY );

	// ----------

	lls_ptr = nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index( box_index_0, output_index_0 );
	
	if( lls_ptr == NULL )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "F_L: light not found" );
	}
	else
	{
		// ----------
		
		LIGHTS_LIST_COMPONENT	*lllc_ptr;
	
		// First check to see if the light is already on the ON list.
		if( ( lllc_ptr = nm_FOAL_LIGHTS_find_this_light_on_the_list( &( foal_lights.header_for_foal_lights_ON_list ), box_index_0, output_index_0 ) ) != NULL )
		{
			// 8/31/2015 mpd : There is only 1 reason a light request will be sent from the IRRI side: manual ON. The 
			// reason field in the transfer record is unnecessary and has been removed.
			lllc_ptr->reason_in_list = IN_LIST_FOR_MANUAL;
			lllc_ptr->action_needed = ACTION_NEEDED_TURN_ON_manual_light;

			// A manual request overrides the stop time for a light on the ON list. Copy the stop date and time from the 
			// token response.
			lllc_ptr->stop_datetime_d = lxr_ptr->stop_datetime_d;
			lllc_ptr->stop_datetime_t = lxr_ptr->stop_datetime_t;

			// Mark for inclusion in the next outgoing token.
			lllc_ptr->xfer_to_irri_machines = ( true );
		}
		else
		{
			// Get the llc for this light.
			lllc_ptr = nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light( box_index_0, output_index_0, false );

			if( lllc_ptr == NULL )
			{
				ALERT_MESSAGE_WITH_FILE_NAME( "F_L: llc not available" );
			}
			else
			{
				// 8/31/2015 mpd : There is only 1 reason a light request will be sent from the IRRI side: manual ON. The 
				// field in the transfer record is unnecessary.
				lllc_ptr->reason_in_list = IN_LIST_FOR_MANUAL;
				lllc_ptr->action_needed = ACTION_NEEDED_TURN_ON_manual_light;

				// Copy the stop date and time from the token response.
				lllc_ptr->stop_datetime_d = lxr_ptr->stop_datetime_d;
				lllc_ptr->stop_datetime_t = lxr_ptr->stop_datetime_t;

				// Add the llc to the ON list.
				if( _nm_nm_nm_add_to_the_lights_ON_list( lllc_ptr, lls_ptr ) )
				{
					Alert_light_ID_with_text( box_index_0, output_index_0, LIGHTS_TEXT_MANUAL_ON );
				}

				// Indicate the light has not been energized yet. The maintenance function will complete the operation.
				lllc_ptr->light_is_energized = ( false );

				// Mark for inclusion in the next outgoing token. Remember the token (from the master) is broadcast to 
				// all controllers in the chain.
				lllc_ptr->xfer_to_irri_machines = ( true );
			}  
		}
	}

	xSemaphoreGiveRecursive( list_foal_lights_recursive_MUTEX );

	xSemaphoreGiveRecursive( lights_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
extern void FOAL_LIGHTS_turn_off_this_lights_output( UNS_32 pbox_index_0, UNS_32 poutput_index_0, UNS_32 paction_needed, UNS_32 palerts_reason )
{
	// 9/30/2015 rmd : Written by rmd. Needed a way to turn off lights. The code Mike wrote was
	// so confusing I had to copy one area he had and make it generic to support turning off the
	// lights in the event of a short. The code that follows is simply a copy of Mikes 'method'
	// he put to gether. If you follow it through you WILL shake your head. I can promise you
	// that.
	
	// ----------
	
	LIGHTS_LIST_COMPONENT	*lllc_ptr;

	// ----------
	
	xSemaphoreTakeRecursive( lights_preserves_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( list_foal_lights_recursive_MUTEX, portMAX_DELAY );

	// ----------

	// Verify the light is on the ON list.
	if( ( lllc_ptr = nm_FOAL_LIGHTS_find_this_light_on_the_list( &( foal_lights.header_for_foal_lights_ON_list ), pbox_index_0, poutput_index_0 ) ) != NULL )
	{
		// 8/31/2015 mpd : The light is about to be removed from the list. Clear the reason.
		lllc_ptr->reason_in_list = nlu_IN_LIST_FOR_not_in_use;

		lllc_ptr->action_needed = paction_needed;

		// Execute a turn OFF. This is where we actually turn OFF the light. It is removed from the ON list and added to 
		// the action needed list which is then sent to the IRRI machines in the next token. The IRRI box containing the 
		// target light will perform the actual turn OFF. This CS3000 centralized scheme takes the thinking away from 
		// the IRRI machines reducing them to slaves.
		lllc_ptr = nm_turn_OFF_this_light( lllc_ptr );

		Alert_light_ID_with_text( pbox_index_0, poutput_index_0, palerts_reason );
	}
	else
	{
		// 8/12/2015 mpd : TBD: Is this necessary?
		//Alert_Message( "F_L: light is not on" );
	}

	xSemaphoreGiveRecursive( list_foal_lights_recursive_MUTEX );

	xSemaphoreGiveRecursive( lights_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void FOAL_LIGHTS_turn_off_all_lights_at_this_box( UNS_32 pcontroller_index, UNS_32 paction_needed, UNS_32 palerts_reason )
{
	UNS_32	iii;
	
	for( iii=0; iii<MAX_LIGHTS_IN_A_BOX; iii++ )
	{
		FOAL_LIGHTS_turn_off_this_lights_output( pcontroller_index, iii, paction_needed, palerts_reason );
	}
}

/* ---------------------------------------------------------- */
extern void FOAL_LIGHTS_process_light_off_from_token_response( LIGHTS_OFF_XFER_RECORD *lxr_ptr )
{
	UNS_32	box_index_0;

	UNS_32	output_index_0;

	// ----------

	box_index_0 = ( lxr_ptr->light_index_0_47 / MAX_LIGHTS_IN_A_BOX );
	
	output_index_0 = ( lxr_ptr->light_index_0_47 % MAX_LIGHTS_IN_A_BOX );
	
	FOAL_LIGHTS_turn_off_this_lights_output( box_index_0, output_index_0, ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_manual_off, LIGHTS_TEXT_MANUAL_OFF );
}


/* ---------------------------------------------------------- */
/*  					FOAL 1 Hz Functions					  */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called at a 1Hz rate. For each light in the system it looks at it's
    schedule to see if the start time is now. If so, this function partially fills out the
    llc and then call the function to finish off adding it to the list of all lights
    waiting to be energized. This function should be thought about as being on the FOAL
	side. It is called from within the TD_CHECK	task. 
	
	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the TD_CHECK task.
	
	@RETURN (none)

	@AUTHOR 2011.10.25 rmd
 
	@REVISIONS 8/3/2015 mpd Adapted station function for lights.
*/
extern void TDCHECK_at_1Hz_rate_check_for_lights_schedule_start( const DATE_TIME_COMPLETE_STRUCT *const pdtcs_ptr )
{
	UNS_32 						temp_date = FOAL_LIGHTS_INVALID_SOONEST_VALUE;

	UNS_32 						temp_time = FOAL_LIGHTS_INVALID_SOONEST_VALUE;

	LIGHT_STRUCT				*lls_ptr;

	UNS_32 						box_index_0;

	UNS_32						output_index_0;

	UNS_32						day_index;

	UNS_32						llights_preserves_index;
	
	LIGHTS_LIST_COMPONENT		*lllc_ptr;

	DATE_TIME					stop_dt;

	// ----------

	// Obtain the MUTEX that allows us to work with the lists of light data and controller settings.
	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );
	
	// And the MUTEX that allows work with the lights_preserves array.
	xSemaphoreTakeRecursive( lights_preserves_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( list_foal_lights_recursive_MUTEX, portMAX_DELAY );

	// ----------

	// For debug purposes to measure how long it takes to do the processing for a start time when all 48 lights are 
	// starting. 
	UNS_32	__this_time_delta;

	__this_time_delta = my_tick_count;

	// ----------

	lls_ptr = nm_ListGetFirst( &light_list_hdr );
	
	while( lls_ptr != NULL )
	{
		box_index_0 = LIGHTS_get_box_index( lls_ptr );

		output_index_0 = LIGHTS_get_output_index( lls_ptr );

		llights_preserves_index = LIGHTS_get_lights_array_index( box_index_0, output_index_0 );

		// There is no need to repeat this every second. The source of this function in "foal_irri.c" does. Why?
		//lights_preserves.lights[ llights_preserves_index ].lrr.box_index_0 = lls_ptr->box_index_0;
		//lights_preserves.lights[ llights_preserves_index ].lrr.output_index_0 = lls_ptr->output_index_0;

		// ----------
		// PROGRAMMED_LIGHTS START
		// ----------

		// Determine what day this is in the 14 day lights schedule.
		day_index = LIGHTS_get_day_index( pdtcs_ptr->date_time.D );

		// Note: The check for physically available at start time is significant. If the light is not physically 
		// available at that moment, it will miss the entire programmed period. The time will not match one second 
		// from now when we test again.

		// See if either start time on today's schedule matches the current time. Be sure the light is physically 
		// available. Ignore the start if we are in that 01:00 to 02:00 DST fallback window that happens every 
		// autumn. We would have already handled the start an hour ago. Note parens: ( ( 1 || 2 ) && 3 && 4 ).
		if( ( ( LIGHTS_get_start_time_1( lls_ptr, day_index ) == pdtcs_ptr->date_time.T ) || 
			  ( LIGHTS_get_start_time_2( lls_ptr, day_index ) == pdtcs_ptr->date_time.T ) ) && 
			( LIGHTS_get_physically_available( lls_ptr ) ) 								 &&
			( !pdtcs_ptr->dls_after_fall_back_ignore_start_times ) )
		{
			// The current time is a programmed start time for this light.

			// This light may already be on due to manual operation or an overlapping programmed start time. These 
			// are not valves. We can't just turn them off momentarily to create a clean starting point for a new 
			// list item. If the turn-off action made it all the way to the hardware before we completed the new 
			// turn-on process, the hardware could take a LONG time to turn back on. 

			// * If the light is already on the "ON list", it will not be turned off to add a new "ON list" entry. 
			// Instead, the stop date/time in the existing entry will be extended IF the stop date/time associated 
			// with this start is later. No change will be made if it is sooner.

			// * The stop time may not be today. To keep this modular, don't count on it being tomorrow. It could 
			// also be a "stop_2" time when we are processing a "start_1". 

			// * There are a maximum of 48 lights in the system and each of the lights list can have up to 48 
			// entries so there is no worry we will run out. This does not mean they are in any order. Lights do
			// not use weighted priorities. There is no list sorting. Lights are added to the list in order of their
			// start times, and removed as they hit the stop time (recorded in the ON list entry).  

			// Programmed lighting will not be started if there is no programmed stop time.
			if( LIGHTS_has_a_stop_time(lls_ptr) )
			{
				// See if the target light is already on the ON list. 
				lllc_ptr = nm_FOAL_LIGHTS_find_this_light_on_the_list( &( foal_lights.header_for_foal_lights_ON_list ), LIGHTS_get_box_index( lls_ptr ), LIGHTS_get_output_index( lls_ptr ) );

				// Add the light to the list if it is not already there.
				if( lllc_ptr == NULL )
				{
					// The target light is not already on the ON list. Get the llc for this light.
					lllc_ptr = nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light( LIGHTS_get_box_index( lls_ptr ), LIGHTS_get_output_index( lls_ptr ), false );

					if( lllc_ptr != NULL )
					{
						lllc_ptr->reason_in_list = IN_LIST_FOR_PROGRAMMED_LIGHTS;
						lllc_ptr->action_needed = ACTION_NEEDED_TURN_ON_programmed_light;

						// Initialize the time to "now" (which would make the light not turn ON). We will update it 
						// before leaving this function.
						lllc_ptr->stop_datetime_d = pdtcs_ptr->date_time.D;
						lllc_ptr->stop_datetime_t = pdtcs_ptr->date_time.T;

						// Link the llc to the ON list. The light will still be in a de-energized state (unless it 
						// is already energized from a previous event). The function 
						// "FOAL_LIGHTS_maintain_lights_list()" will continue the energizing process within 1 
						// second. 
						if( _nm_nm_nm_add_to_the_lights_ON_list( lllc_ptr, lls_ptr ) )
						{
							// 9/9/2015 mpd : Don't post the alert here. It will be done after an additional time 
							// check.
							//Alert_light_ID_with_text( box_index_0, output_index_0, LIGHTS_TEXT_SCHEDULED_ON );
						}
						else 
						{
							// This should never happen.
							Alert_Message( "F_L: ON add failed" );
						}
					}
					else
					{
						// This can't happen.
						Alert_Message( "F_L: Too many lights" );
					}
				}

				if( lllc_ptr != NULL )
				{
					// We have a pointer to a LIGHTS_LIST_COMPONENT entry on the lights "ON list". It may be from a 
					// manual ON, a previous (overlapping) programmed start, or one we just created. The next task 
					// is to update the time fields and possibly the reason.

					// Note: The stop date/time is recorded in the ON list entry at the time the light is added to 
					// the list or updated by another request. The stop date/time included in a Manual,	Mobile, or 
					// Test request will override any previously saved stop date/time in the ON list entry.

					// 8/24/2015 mpd : TODO: Check if the Boolean return to this is doing what is needed.
					// Get the next stop time from the saved 14 day schedule. Note: This will not pick up current
					// user edited values until they exit the screen for the modified light. 
					if( nm_FOAL_LIGHTS_get_soonest_stop_time( pdtcs_ptr, lls_ptr, &temp_date, &temp_time ) )
					{
						// Set the "ON list" stop date and time to the highest value (the existing value, or the 
						// scheduled stop time we just found).
						if ( FOAL_LIGHTS_set_stop_date_and_time_to_furthest( &( lllc_ptr->stop_datetime_d ), &( lllc_ptr->stop_datetime_t ), temp_date, temp_time ) )
						{
							// We just pushed out the stop time time based on a programmed schedule. Make sure the 
							// reason agrees. 
							lllc_ptr->reason_in_list = IN_LIST_FOR_PROGRAMMED_LIGHTS;
						}
					}
					else
					{
						// We better never hit this. We are inside a block where "no_stop_time" is false. 
						Alert_Message( "F_L: no stop time error" );
					}

					// Sanity check. The stop date cannot be greater than 14 days from now. The stop time must be a 
					// valid second in the day.
					if( ( lllc_ptr->stop_datetime_d > ( pdtcs_ptr->date_time.D + LIGHTS_SCHEDULE_DAYS_MAX ) ) ||
						( lllc_ptr->stop_datetime_t >= SCHEDULE_OFF_TIME ) ) 
					{
						Alert_Message( "F_L: unexpected date/time" );
						lllc_ptr->stop_datetime_d = pdtcs_ptr->date_time.D;
						lllc_ptr->stop_datetime_t = pdtcs_ptr->date_time.T;
					}

					// The upcoming comparison requires a DATE_TIME struct which we do not have in the llc.
					stop_dt.D = lllc_ptr->stop_datetime_d;
					stop_dt.T = lllc_ptr->stop_datetime_t;

					// Is there any lighting time remaining? Note that the comparison does not include start time 
					// equal to stop time. 
					if( DT1_IsBiggerThan_DT2( &stop_dt, &( pdtcs_ptr->date_time ) ) == ( true ) )
					{
						Alert_light_ID_with_text( box_index_0, output_index_0, LIGHTS_TEXT_SCHEDULED_ON );

						// 7/31/2015 mpd : TODO: Create a similar screen update trigger for lights.
						// 4/17/2015 ajv : The next scheduled lights is normally kept up-to-date by
						// recalculating every time a change is made. However, after a start time is crossed, it's
						// important to update the screen to indicate the NEXT scheduled start. Therefore, post a
						// message to update the GuiVars used to display the next scheduled lights.
						//postBackground_Calculation_Event( BKGRND_CALC_NEXT_SCHEDULED );

						// Now handle the light preserves.

						// 7/31/2015 mpd : TODO: Get this from somewhere.
						lights_preserves.lights[ llights_preserves_index ].last_measured_current_ma = 0;

						lights_preserves.lbf[ llights_preserves_index ].light_is_available = true;
						lights_preserves.lbf[ llights_preserves_index ].light_is_energized = true;

						// Add to the cycle count. This is the only place this is incremented for scheduled starts.
						//lights_preserves.lights[ llights_preserves_index ].lrr.number_of_on_cycles++;

						// Don't touch these here.
						//lights_preserves.lights[ llights_preserves_index ].lrr.record_start_date 
						//lights_preserves.lights[ llights_preserves_index ].lrr.record_start_time 
						//lights_preserves.lights[ llights_preserves_index ].lrr.actual_light_seconds_on
					}
					else
					{
						// This is odd, but possible. We hit a start time, and after all the searching, we ended up 
						// with a concurrent stop time. That means this light is going off now.

						// The purpose of this function is to add lights to the "ON list" at their start times. We 
						// may have done that needlessly for this light (if it was not already ON with a stop time 
						// equal to now). It's not our job to remove lights from the list. Generate an alert as is 
						// done for stations.
						Alert_light_ID_with_text( box_index_0, output_index_0, LIGHTS_TEXT_SKIPPED_NO_RUN_TIME );
					}
				}
				else
				{
					// This should never happen either.
					Alert_Message( "F_L: no ON list pointer" );
				}

			} // END there is a programmed stop time.

			else // No programmed stop time.
			{
				// Programmed lighting was skipped because the user did not include a stop time.
				Alert_light_ID_with_text( box_index_0, output_index_0, LIGHTS_TEXT_SKIPPED_NO_STOP_TIME );
			}

		} // END this is a start time for this light.

		// ----------
		// PROGRAMMED_LIGHTS END
		// ----------

		lls_ptr = nm_ListGetNext( &light_list_hdr, (void *)lls_ptr );

	}  // END of light by light loop

	__this_time_delta = (my_tick_count - __this_time_delta);
	
	if( __this_time_delta > __largest_lights_start_time_delta )
	{
		__largest_lights_start_time_delta = __this_time_delta;
	}
	
	// -----------------------------------
	
	// Return the MUTEXES.
	xSemaphoreGiveRecursive( list_foal_lights_recursive_MUTEX );

	xSemaphoreGiveRecursive( lights_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX );

}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called on at a 1 hz rate if we are in sync and generating tokens.  

    @CALLER MUTEX REQUIREMENTS (none) This function will internally need to take and give
    certain needed list mutexes. One obvious one is the foal irri list mutex. It should be
    noted that when the TD_CHECK task is calling this function it is holding the iccc MUTEX.
	
    @EXECUTED Executed within the context of the TD_CHECK task.
	
	@RETURN (none)

	@AUTHOR 2011.10.31 rmd
*/
extern void FOAL_LIGHTS_maintain_lights_list( const DATE_TIME *const pdate_time )
{
	LIGHTS_LIST_COMPONENT *lllc, *current_llc;

	DATE_TIME	stop_dt;

	BOOL_32 get_the_next_llc = true;

	// ------------------------------
	
	// Protect the foal_lights lists (lights, sxru, etc.) from changes while we work with it.
	xSemaphoreTakeRecursive( list_foal_lights_recursive_MUTEX, portMAX_DELAY );

	// ------------------------------
	
	// Unlike stations, the llc for a light can only be on a list once. There is no worry we will put it on for one 
	// reason and add it again for a second reason before the list can be worked down. Still, to be consistent with 
	// stations code, this function will insist on an EMPTY action needed list before proceeding. This may affect 
	// performance in certain ways. Resulting in delays at times as the list is worked down. 
	if( __foal_lights_maintenance_may_run() == ( true ) )
	{
		// 9/9/2015 mpd : There are only 48 possible lights in the system and time values have 10 minute granularity. We
		// can handle one alert for every light going ON and OFF. Skip the Boolean.

		// In the following pass through the lights list we may remove one or more lights for the reasons of hitting the 
		// stop time. We only want to see one alert line about such.
		//BOOL_32	alert_already_made_for_stop_time = ( false );

		// --------------------------
		
		lllc = nm_ListGetFirst( &(foal_lights.header_for_foal_lights_ON_list) );
		
		// THIS MUST LOOP THROUGH ALL LIGHTS ON THE ON LIST. Note that this is a single loop through the ON list that
		// handles ON and OFF operations. This is significantly different than stations which complete OFF operations
		// and block subsequent ON operations until the action needed list is empty. We can do this here because a light 
		// can only be on the list once, and we don't reorder the list as we loop through it. That means we will not see
		// the same light again before leaving this function.
		while( lllc != NULL )
		{
			// Sanity checks. 
			RangeCheck_uns8( &(lllc->box_index_0), BOX_INDEX_MINIMUM, BOX_INDEX_MAXIMUM, BOX_INDEX_DEFAULT, NULL, "Box Index" );
			RangeCheck_uns8( &(lllc->output_index_0), LIGHTS_OUTPUT_INDEX_MIN, LIGHTS_OUTPUT_INDEX_MAX, LIGHTS_OUTPUT_INDEX_DEFAULT, NULL, "Output Index" );

			// Upcoming comparisons requires a DATE_TIME struct which we do not have in the llc. Use a local.
			stop_dt.D = lllc->stop_datetime_d;
			stop_dt.T = lllc->stop_datetime_t;

			// Expect to call the "next" function.
			get_the_next_llc = ( true );

			// Save the current light's llc before any ON or OFF calls modify the pointer.
			current_llc = lllc;

			// This light is on the ON list. It is either energized, or about to be energized (meaning it just passed a 
			// start time or was manually turned ON). Determine the energized state here.
			if( lllc->light_is_energized )
			{
				// 8/6/2015 mpd : TODO: Manual, Test, and Mobile stops should modify the llc fields to force a stop. The 
				// stop date/time should be "now".

				// ----------------
				// Hitting the stop time is the primary way to remove a light from the ON list. 
				// ----------------

				// This light is energized. Check the stop time. The reason it's on the list is important for stations. 
				// It doesn't matter for lights. Note that the comparison will include stop times that have already 
				// passed in case this function was not called exactly at stop time.
				if( DT1_IsBiggerThanOrEqualTo_DT2( pdate_time, &stop_dt ) == ( true ) )
				{
					// This light has reached it's stop time. 

					// 8/13/2015 mpd : TODO: Check for a concurrent start time. Don't turn it off if it is supposed to 
					// come back on immediately.
					 
					// * Before taking a light off the "ON list" as the remaining time hits zero, a check will be made to 
					// see if there is an immediate start time for that light. If there is, it will not be turned off to 
					// add a new "ON list" entry. Instead, the stop date/time in the existing entry will be updated with the 
					// new scheduled value. 
					// 

					// Indicate what we want done to this light.
					lllc->action_needed = ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_hit_stop_time;

					// Execute a turn OFF. This is where we actually turn OFF the light. It is removed from the ON list 
					// and added to the action needed list which is then sent to the IRRI machines in the next token. 
					// The IRRI box containing the target light will perform the actual turn OFF. This CS3000 
					// centralized scheme takes the thinking away from the IRRI machines reducing them to slaves.
					lllc = nm_turn_OFF_this_light( lllc );

					// 9/11/2015 mpd : This is for debug. Remove from release code.
					//Alert_Message_va( "F_L Debug: Light ON seconds: %d", lights_preserves.lights[ LIGHTS_get_lights_array_index( current_llc->box_index_0, current_llc->output_index_0 ) ].lrr.actual_light_seconds_on);

					// We just added to the action needed list which blocks the majority of this function from running
					// until the token is sent (which clears the action needed list). Only the "actual_light_seconds_on" 
					// field will be maintained until then. 

					Alert_light_ID_with_text( current_llc->box_index_0, current_llc->output_index_0, LIGHTS_TEXT_SCHEDULED_OFF );

					// The OFF function returns the next llc if it was ON; NULL if it was not. 
					if( lllc != NULL )
					{
						// This flag will prevent us from skipping too far ahead.
						get_the_next_llc = ( false );
					}

				} // END We have reached the stop time.

			} // END Light is energized

			else // Light is not energized yet. 
			{
				// Complete the tasks to energize it only if there is time remaining before the stop. Note that the 
				// comparison does not include start and stop times that are equal.
				if( DT1_IsBiggerThan_DT2( &stop_dt, pdate_time ) == ( true ) )
				{
					// The INTENTION IS this is the ONLY place in the code where we energize lights. A single point was 
					// mandatory for stations. Lights do not have the dynamic propety values to maintain as stations do. 
					// Maintaining this single ON location is still good coding style.   

					// 8/31/2015 mpd : TBD: This should have already been handled. Verify.
					//llc->action_needed = FOAL_LIGHTS_get_action_needed_for_reason( llc->reason_in_list, FOAL_LIGHT_ON );

					// Complete the turn ON. The function "TDCHECK_at_1Hz_rate_check_for_lights_schedule_start()" or a
					// manual operation put this light on the ON list in a denergized state. This function call will 
					// energize it no matter where it is physically connected in the chain.
					_nm_nm_FOAL_LIGHTS_complete_the_action_needed_turn_ON( lllc );

					// 8/18/2015 mpd : No action need list entry is needed for ON.
					// Add to the action_needed list so we tell all the IRRI machines. 
					//nm_FOAL_LIGHTS_add_to_action_needed_list( llc );

					// We just added to the action needed list which blocks the majority of this function from running
					// until the token is sent (which clears the action needed list). Only the "actual_light_seconds_on" 
					// field will be maintained until then. 
				}
				else // Stop time is NOW (or in the past)
				{
					// Somehow the delta between now and the stop time became zero. This should not have happened since 
					// "pdate_time" was passed in as a constant and our stop time was greater a few hundred lines ago. 
					// This CYA "else" exists in stations code, so it will remain here for lights.

					// Indicate what we want done to this light.
					lllc->action_needed = ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_time_unexpectedly_zero;

					// Execute a turn OFF. 
					lllc = nm_turn_OFF_this_light( lllc );

					// We just added to the action needed list which blocks the majority of this function from running
					// until the token is sent (which clears the action needed list). Only the "actual_light_seconds_on" 
					// field will be maintained until then. 

					Alert_light_ID_with_text( current_llc->box_index_0, current_llc->output_index_0, LIGHTS_TEXT_SKIPPED_NO_RUN_TIME );

					// The OFF function returns the next llc if it was ON; NULL if it was not. 
					if( lllc != NULL )
					{
						// This flag will prevent us from skipping too far ahead.
						get_the_next_llc = ( false );
					}

				}  // END Stop time is now or in the past.

			} // END Light is not energized.

			// Several functions we may have called return the next llc. Only get the next if we didn't get it already.
			if( get_the_next_llc )
			{
				lllc = nm_ListGetNext( &(foal_lights.header_for_foal_lights_ON_list), lllc );
			}

		}  // END while looping through ON list.

		// We just completed looping through all the lights on the ON list.

	}  // END this maintenance function was okayed to run

	xSemaphoreGiveRecursive( list_foal_lights_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


