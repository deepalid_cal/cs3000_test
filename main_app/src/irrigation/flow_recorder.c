/* file = flow_recorder.c           02.06.03  2011.08.08  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"flow_recorder.h"

#include	"epson_rx_8025sa.h"

#include	"foal_irri.h"

#include	"alerts.h"

#include	"app_startup.h"

#include	"change.h"

#include	"controller_initiated.h"

#include	"configuration_controller.h"

#include	"cs_mem.h"

#include	"d_process.h"

#include	"r_flow_recording.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void ci_flow_recording_timer_callback( xTimerHandle pxTimer )
{
	// 10/21/2013 rmd : Remember this is executed within the context of the high priority timer
	// task. And is executed when it it time to send flow recording. Which is normally 15
	// minutes after a line is made.

	// 8/12/2014 rmd : When the timer is created the timer ID is set. Its value is a pointer to
	// the system that wants to send the flow recording records.
	CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_flow_recording, pvTimerGetTimerID(pxTimer), CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD, CI_POST_TO_BACK );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called each time before we are going to make a line. Checks to see if the
    flow recording system has been used since the system_preserve record was first
    registered. On power up will always need to run through this once for each system as the
    flow recording memory non-volatile (memory pool on the heap).

    @CALLER_MUTEX_REQUIREMENTS The caller needs to be holding the system preserves mutex as
    we are going to potentially change shared variables.

	@MEMORY_RESPONSIBILITES

    @EXECUTED Executed within the context of the COMM_MNGR task when we are making the
    outgoing token and complete the action_needed turn ONs and turn OFFs. And possibly the
    TD_CHECK task.

    @RETURN (none)

	@ORIGINAL 2012.04.30 rmd

	@REVISIONS
*/
static void _nm_flow_recorder_verify_allocation( BY_SYSTEM_RECORD *pbsr_ptr )
{
	FLOW_RECORDING_CONTROL_STRUCT *frcs_ptr;

	frcs_ptr = &(pbsr_ptr->frcs);
	
	// ----------
	
	if( frcs_ptr->original_allocation == NULL )
	{
		frcs_ptr->original_allocation = mem_malloc( FLOW_RECORDER_MAX_RECORDS * sizeof( CS3000_FLOW_RECORDER_RECORD ) );

		// Start all the pointers at the beginning.
		frcs_ptr->next_available = frcs_ptr->original_allocation;

		frcs_ptr->first_to_display = frcs_ptr->original_allocation;

		frcs_ptr->first_to_send = frcs_ptr->original_allocation;

		frcs_ptr->pending_first_to_send = frcs_ptr->original_allocation;
		
		// 10/28/2015 rmd : No message can be underway yet. And this should clearly be set false to
		// begin with otherwise a msg will never be created and sent.
		frcs_ptr->pending_first_to_send_in_use = (false);
		
		// ----------
	
		// 10/21/2013 rmd : Create the flow recording timer to manage sending to the
		// comm_server.
		frcs_ptr->when_to_send_timer = xTimerCreate( (const signed char*)"", MS_to_TICKS(CONTROLLER_INITIATED_FLOW_RECORDING_TRANSMISSION_TIMER_MS), FALSE, (void*)pbsr_ptr, ci_flow_recording_timer_callback );
			
		if( frcs_ptr->when_to_send_timer == NULL )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "Timer NOT CREATED" );
		}
	}
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Increment the pointer while staying within the bounds of the allocated
    space.

    @CALLER_MUTEX_REQUIREMENTS The caller should be holding the system_preserves_MUTEX. As
    we are changing it parts of the system_preserve record.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task when we are making the
    outgoing token and complete the action_needed turn ONs and turn OFFs. And possibly the
    TD_CHECK task.

	@RETURN (none)

	@ORIGINAL 2012.04.26 rmd

	@REVISIONS
*/
extern void nm_flow_recorder_inc_pointer( UNS_8 **ppfrr, UNS_8 const (*const pstart_of_block) )
{
	*ppfrr += sizeof( CS3000_FLOW_RECORDER_RECORD );

	if( *ppfrr >= pstart_of_block + (FLOW_RECORDER_MAX_RECORDS * sizeof(CS3000_FLOW_RECORDER_RECORD)) )
	{
		*ppfrr = (UNS_8*)pstart_of_block;
	}
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Adds the next flow recording line based on the function arguments. pilc_ptr
    is tested to be on the ilc list.

    @CALLER_MUTEX_REQUIREMENTS The caller should be holding the list_foal_irri_MUTEX.
    Because we are counting on a stable list while we hold the pointer to the list item.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task when we are making the
    outgoing token and complete the action_needed turn ONs and turn OFFs. And possibly the
    TD_CHECK task.

	@RETURN (none)

	@ORIGINAL 2012.04.26 rmd

	@REVISIONS
*/
extern void nm_flow_recorder_add( void *pilc_ptr, UNS_16 pderated_expected_flow, UNS_16 phi_limit, UNS_16 plo_limit, UNS_16 pportion_of_actual, FLOW_RECORDER_FLAG_BIT_FIELD pflag )
{
	IRRIGATION_LIST_COMPONENT		*lilc;

	CS3000_FLOW_RECORDER_RECORD		*frr;
	
	// ----------
	
	// 10/21/2013 rmd : The pilc_ptr had to be passed as a void* to solve circular references
	// between the BY_SYSTEM_RECORD in battery_backed_vars.h and IRRIGATION_LIST_COMPONENT that
	// would have be used in this function prototype.
	lilc = pilc_ptr;

	// ----------
	
	if( nm_OnList( &(foal_irri.list_of_foal_all_irrigation), pilc_ptr ) == (false) )
	{
		//ALERT_MESSAGE_WITH_FILE_NAME( "FLOW_RECORDER: pilc not on list." );
		Alert_item_not_on_list( pilc_ptr, foal_irri.list_of_foal_all_irrigation );
	}
	else
	{
		xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

		_nm_flow_recorder_verify_allocation( lilc->bsr );

		frr = ((CS3000_FLOW_RECORDER_RECORD*)(lilc->bsr->frcs.next_available));

		EPSON_obtain_latest_time_and_date( &(frr->dt) );

		frr->station_num = lilc->station_number_0_u8;

		frr->box_index_0 = lilc->box_index_0;

		frr->expected_flow = lilc->expected_flow_rate_gpm_u16;

		frr->derated_expected_flow = pderated_expected_flow;

		frr->hi_limit = phi_limit;

		frr->lo_limit = plo_limit;

		frr->portion_of_actual = pportion_of_actual;

		frr->flag = pflag;

		// ----------
		
		// 10/21/2013 rmd : Move ahead to the next available slot.
		nm_flow_recorder_inc_pointer( &(lilc->bsr->frcs.next_available), lilc->bsr->frcs.original_allocation );

		// ----------

		// 10/23/2013 rmd : If we've wrapped around bump ahead the first to display. Yes this
		// crushes a line before it's time (as the line is still valid until a new one is made) but
		// I think it keep the logic simple about how many lines are in the pile to show. At the
		// sacrafice of 1 line. And that's fine.
		if( lilc->bsr->frcs.next_available == lilc->bsr->frcs.first_to_display )
		{
			nm_flow_recorder_inc_pointer( &(lilc->bsr->frcs.first_to_display), lilc->bsr->frcs.original_allocation );
		}
		
		// ----------

		// 10/21/2013 rmd : And check if we are now pointing to the first one to send. If so the
		// first to send needs to be moved. Technically it doesn't need to move until a new line is
		// made at it's location. BUT I think it makes the code more simple to move it now. The
		// effect is as if the buffer was smaller by 1 less line. But that's okay.
		if( lilc->bsr->frcs.next_available == lilc->bsr->frcs.first_to_send )
		{
			nm_flow_recorder_inc_pointer( &(lilc->bsr->frcs.first_to_send), lilc->bsr->frcs.original_allocation );
		}

		// 10/21/2013 rmd : Also if next available has landed on the pending_first_to_send it means
		// that during the CI session we have actually written so many lines we wrapped all the way
		// around. In other words we wrote more lines than we hold during the session. So move that
		// pointer too.
		if( lilc->bsr->frcs.next_available == lilc->bsr->frcs.pending_first_to_send )
		{
			nm_flow_recorder_inc_pointer( &(lilc->bsr->frcs.pending_first_to_send), lilc->bsr->frcs.original_allocation );
		}

		// ----------

		// 8/12/2014 rmd : If the timer is not running go ahead and start it. If it is running leave
		// alone. The effect of this is that within a maximum of 15 minutes from the time a record
		// is added it will be sent. Not sure if this is wise. As it essentially means that during
		// irrigation every 15 minutes flow recording will be sent to the comm_server_app.
		if( xTimerIsTimerActive( lilc->bsr->frcs.when_to_send_timer ) == pdFALSE )
		{
			// 10/23/2013 rmd : Start the timer to trigger the controller initiated send.
			//
			// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
			// when posting is safe. Anything on the timer queue is quickly processed and therefore the
			// queue is effectively always empty.
			xTimerChangePeriod( lilc->bsr->frcs.when_to_send_timer, MS_to_TICKS( CONTROLLER_INITIATED_FLOW_RECORDING_TRANSMISSION_TIMER_MS ), portMAX_DELAY );
		}

		// ----------

		xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
		
		// ----------
		
		// 10/29/2013 ajv : If the user is viewing the Flow Recording report
		// when a new line is added, force the scroll box to be updated.
		if( GuiLib_CurStructureNdx == GuiStruct_rptFlowRecording_0 )
		{
			DISPLAY_EVENT_STRUCT	lde;

			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = &FDTO_FLOW_RECORDING_redraw_scrollbox;
			Display_Post_Command( &lde );
		}

	}  // of if pilc is not on the irrigation list

}

/* ---------------------------------------------------------- */

#if ALERTS_ABILITY_TO_GENERATE_ALL_ALERTS_AND_REPORTS_FOR_DEBUG

extern void FLOW_RECORDING_generate_lines_for_DEBUG( void )
{
	UNS_32	lll;
	
	CS3000_FLOW_RECORDER_RECORD		*frr;

	BY_SYSTEM_RECORD				*bsr;

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	bsr = &(system_preserves.system[0]);

	_nm_flow_recorder_verify_allocation( bsr );

	for( lll=0; lll<FLOW_RECORDER_MAX_RECORDS-1; lll++ )
	{
		frr = (CS3000_FLOW_RECORDER_RECORD*)bsr->frcs.next_available;

		EPSON_obtain_latest_time_and_date( &(frr->dt) );

		frr->station_num = (lll % 128);
		frr->box_index_0 = 0;

		frr->expected_flow = (lll % 100);
		frr->derated_expected_flow = (lll % 200);

		frr->hi_limit = (lll % 300);
		frr->lo_limit = (lll % 400);

		frr->portion_of_actual = (lll % 500);

		frr->flag.overall_size = 0;
		// ----------
		
		// 10/21/2013 rmd : Move ahead to the next available slot.
		nm_flow_recorder_inc_pointer( &(bsr->frcs.next_available), bsr->frcs.original_allocation );

		// ----------

		// 10/23/2013 rmd : If we've wrapped around bump ahead the first to display. Yes this
		// crushes a line before it's time (as the line is still valid until a new one is made) but
		// I think it keep the logic simple about how many lines are in the pile to show. At the
		// sacrafice of 1 line. And that's fine.
		if( bsr->frcs.next_available == bsr->frcs.first_to_display )
		{
			nm_flow_recorder_inc_pointer( &(bsr->frcs.first_to_display), bsr->frcs.original_allocation );
		}
		
		// ----------

		// 10/21/2013 rmd : And check if we are now pointing to the first one to send. If so the
		// first to send needs to be moved. Technically it doesn't need to move until a new line is
		// made at it's location. BUT I think it makes the code more simple to move it now. The
		// effect is as if the buffer was smaller by 1 less line. But that's okay.
		if( bsr->frcs.next_available == bsr->frcs.first_to_send )
		{
			nm_flow_recorder_inc_pointer( &(bsr->frcs.first_to_send), bsr->frcs.original_allocation );
		}

		// 10/21/2013 rmd : Also if next available has landed on the pending_first_to_send it means
		// that during the exchange we have actually written so many lines we wrapped all the way
		// around. In other words more lines than we hold. So move that pointer too.
		if( bsr->frcs.next_available == bsr->frcs.pending_first_to_send )
		{
			nm_flow_recorder_inc_pointer( &(bsr->frcs.pending_first_to_send), bsr->frcs.original_allocation );
		}

	}

	// ----------

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

#endif

/* ---------------------------------------------------------- */
/*
extern void set_flow_recording_timer_to_15_seconds( void )
{
	BY_SYSTEM_RECORD				*bsr;

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	bsr = &(system_preserves.system[0]);

	_nm_flow_recorder_verify_allocation( bsr );

	// ----------

	// 10/23/2013 rmd : Start the timer to trigger the controller initiated send.
	//
	// 3/14/2014 rmd : Being that the timer task is a high priority task, using portMAX_DELAY
	// when posting is safe. Anything on the timer queue is quickly processed and therefore the
	// queue is effectively always empty.
	xTimerChangePeriod( bsr->frcs.when_to_send_timer, MS_to_TICKS( 15*1000 ), portMAX_DELAY );

	// ----------

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}
*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

