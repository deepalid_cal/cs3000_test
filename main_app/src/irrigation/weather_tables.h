/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_WEATHER_TABLES_H
#define _INC_WEATHER_TABLES_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	"lpc_types.h"

#include	"cal_td_utils.h"

#include	"stations.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/14/2012 rmd : The gage actually holds 12 inches of water or 1200 pulses. But we give
// one inch of margin. This value is used when issuing the gage empty warning.
#define	ET_GAGE_PULSE_CAPACITY	(1100)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// We used to only keep 31 but after some brief thought I said hey why not keep more? We have room.
#define ET_RAIN_DAYS_IN_TABLE	(60)

// 4/28/2016 skc : Number of days to use for calculating ET ratio used in the budgets
// calculation. And maybe in the finish times calc.
//
// 5/19/2016 rmd : I picked this number sort of seat of the pants. There is nothing to say
// it couldn't be 15 days or 25 days. The intent was a long enough period of time to produce
// a reasonable representation of what was in the table as compared to historical. We use
// the ratio produced to calculate how long future irrigations will run for.
#define ET_DAYS_IN_RATIO	(20)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/28/2015 rmd : NOTE - the weather tables strucutre is exposed to make it easier for the
// controller initiated task to build the outbound message. This is what we do for all
// report structures. The choice was expose parts of the controller initiated task (now
// static) or expose this structure. Since all the other report datas have theirs exposed I
// opted to expose this.
typedef struct
{
	// 7/20/2012 rmd : This is the date attached to entry 0 in the table. And moving into the
	// table moves backwards in time.
	UNS_32  et_rain_table_date;

	// 9/13/2012 rmd : The ET and RAIN tables. The entry in process, that is the one
	// accumulating the et or rain pulses is held in the battery backed weather preserves. To
	// load the ET table with a new historical there is a flag (also in battery backed
	// weather_preserves). Within the roll function that flag is acted upon and reset.
	ET_TABLE_ENTRY      et_table[ ET_RAIN_DAYS_IN_TABLE ];

	// --------

	// 10/28/2015 rmd : This used to be the rain table date. But is now no longer used. The
	// et_rain tables are linked together. They both roll together. In effect they are ONE
	// table.
	UNS_32  nlu_rain_table_date;

	// 9/13/2012 rmd : See comment above for et table.
	RAIN_TABLE_ENTRY    rain_table[ ET_RAIN_DAYS_IN_TABLE ];

	// ----------

	// 10/27/2015 rmd : Three variables to support the controller initiated sending to the
	// comm_server app. Designed to allow the table to change while the CI message is in
	// transit. The pending in use variable also protects against multiple CI messages being
	// generated resulting in a new pending_first_to_send index. Which would be used in error
	// for the first response that came in.
	//
	// 8/15/2014 rmd : Note that ideally these variables would be stored in battery-backed
	// memory. We however opted (cause easier) to include them in the file. Therefore the
	// mechanism to preserve through a power fail is by writing them to the file. The
	// implication is that when the controller initiated ACK is received the file must be saved.

	// 10/27/2015 rmd : This is the only variable of these 3 that persists through a power
	// failure. The other 2 do not have to. And we are using ONE index variable for both the ET
	// and RAIN tables.
	UNS_32                  records_to_send;

	BOOL_32                 pending_records_to_send_in_use;

	// ----------

	UNS_32					nlu;  // no_longer_used - available for new use

	// ----------

	// 10/27/2015 rmd : When this structure is updated can ONLY add to the end of the structure.

} WEATHER_TABLES_STRUCT;

extern WEATHER_TABLES_STRUCT    weather_tables;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_file_weather_tables( void );

extern void save_file_weather_tables( void );

// ----------

extern UNS_32 WEATHER_TABLES_get_et_table_date( void );

extern BOOL_32 WEATHER_TABLES_get_et_table_entry_for_index( const UNS_32 pindex_0, ET_TABLE_ENTRY *et_entry_ptr );

extern void WEATHER_TABLES_update_et_table_entry_for_index( const UNS_32 pindex_0, ET_TABLE_ENTRY *et_entry_ptr );

extern BOOL_32 WEATHER_TABLES_get_rain_table_entry_for_index( const UNS_32 pindex_0, RAIN_TABLE_ENTRY *rain_entry_ptr );

extern void WEATHER_TABLES_update_rain_table_entry_for_index( const UNS_32 pindex_0, RAIN_TABLE_ENTRY *rain_entry_ptr );

extern void WEATHER_TABLES_cause_et_table_historical_values_to_be_updated( void );

extern void nm_RAIN_for_this_station_generate_rain_time_for_raintable_slot1( STATION_STRUCT *const pss_ptr, const UNS_32 pstation_preserves_index );


extern void WEATHER_TABLES_load_record_with_historical_et( UNS_32 ptable_index, ET_TABLE_ENTRY *pptr_table_record );

extern void WEATHER_TABLES_roll_et_rain_tables( DATE_TIME const *const pdate_time );


extern UNS_32 WEATHER_TABLES_load_et_rain_tables_into_outgoing_token( UNS_8 **pet_rain_tables_data_ptr );

extern BOOL_32 WEATHER_TABLES_extract_et_rain_tables_out_of_msg_from_main( UNS_8 **pucp_ptr, UNS_32 pfrom_serial_number );


extern void WEATHER_TABLES_set_first_to_send_index_to_at_least_this( UNS_32 pto_what );

extern BOOL_32 WEATHER_TABLES_pending_first_to_send_index_is_in_use( void );

extern void WEATHER_TABLES_set_first_to_send_index_to_pending_value( void );

// ----------
// 4/28/2016 skc : Support for budget calculations
extern void WEATHER_TABLES_set_et_ratio( UNS_32 pnum_days );

extern float WEATHER_TABLES_get_et_ratio();



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

