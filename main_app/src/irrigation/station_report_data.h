/*  file = report_data.h                      09.13.2011  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_STATION_REPORT_DATA_H
#define _INC_STATION_REPORT_DATA_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	"lpc_types.h"

#include	"foal_defs.h"

#include	"report_data.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2011.09.16 rmd : We are going to manually pack this structure. That will result in not
// only the most efficient code but the most efficient storage. We could simply pack the
// structure and organize it any way we want. But the code to read and write the members of
// the structure will be bulky. I'll try it this way ... that is manually organizing the
// members to achieve the pack and code efficiency. The trade off is the structure appears
// dis-organized here in the definition. But only here in the definition.
typedef struct
{
	UNS_32			record_start_time;			//	Time associated with record start

	// ------------------------
	
	// We store these accumulators as floats in order to accumulate fractional amounts of a
	// gallon on a second by second basis. We ship to the central as an ul_100u.
	float			programmed_irrigation_gallons_irrigated_fl;
	
	float			manual_program_gallons_fl;

	float			manual_gallons_fl;

	float			walk_thru_gallons_fl;

	float			test_gallons_fl;

	float			mobile_gallons_fl;

	// ------------------------
	
	//	In seconds. A long. As a short is too ... short. May need to accomodate 24+ hours.
	UNS_32			programmed_irrigation_seconds_irrigated_ul;

	// ------------------------
	
	// 6/28/2012 rmd : What group ids do we need to keep? Running reports about say the test use
	// for the turf stations belonging to a particular system over a period of time sounds
	// reasonable. So here they are.
	
	// 6/28/2012 rmd : The log lines need the system and the schedule GID's and that's all. The
	// station by station needs 'em all as you see below. To be used as report data filters and
	// organizers.
	UNS_16			GID_station_group;

	// -----------------------------

	UNS_16			record_start_date;			//	Date associated with record start. Should occur each and every day!

	// -----------------------------

	UNS_16  		mobile_seconds_us;

	UNS_16  		test_seconds_us;

	UNS_16  		walk_thru_seconds_us;

	UNS_16  		manual_seconds_us;

	UNS_16  		manual_program_seconds_us;

	// -----------------------------
	
	// 6/28/2012 rmd : From 0 to 127 representing station 1 through 128 at a box.
	UNS_8			station_number;
	
	// 4/18/2014 rmd : This is the 0..11 index of the box the station belongs to.
	UNS_8			box_index_0;

	// -----------------------------
	
	// 6/29/2012 rmd : The record size is 64 bytes at the moment.
	// 4/21/2014 rmd : After removing the UNS_32 serial number the record size is now at 48
	// bytes.
	
	// -----------------------------
	
} STATION_REPORT_DATA_RECORD;

// -----------------------------

// The storage of the completed records. This will be a structure. Stored in SDRAM. The
// array of records contained within. And some variables which help know where to append the
// next completed record. Once full it will just keep wrapping around. After each start
// time, meaning a groupd of new records was added, we'll save the file.

// 7/28/2017 rmd : I believe the max size is around 520KBytes.

// 1/23/2015 rmd : If the number of records kept is changed should update revision number
// else you will lose the file contents.

#define STATION_REPORT_DATA_MIN_DAYS_KEPT		(2 * DAYS_IN_A_WEEK)

#define STATION_REPORT_DATA_MAX_RECORDS			( MAX_STATIONS_PER_NETWORK * STATION_REPORT_DATA_MIN_DAYS_KEPT )

// -----------------------------

typedef struct
{
	REPORT_DATA_FILE_BASE_STRUCT	rdfb;

	// ----------
	
	STATION_REPORT_DATA_RECORD		srdr[ STATION_REPORT_DATA_MAX_RECORDS ];

} COMPLETED_STATION_REPORT_DATA_STRUCT;

extern COMPLETED_STATION_REPORT_DATA_STRUCT	station_report_data_completed;	// Held in SDRAM and saved to a file in the GENERAL STORAGE flash device.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

// ---------------------

extern void nm_init_station_report_data_record( STATION_REPORT_DATA_RECORD *psrdr_ptr );

extern void init_file_station_report_data( void );

extern void save_file_station_report_data( void );

extern void STATION_REPORT_DATA_start_the_ci_timer_if_it_is_not_running( void );

extern void nm_STATION_REPORT_DATA_inc_index( UNS_32 *pindex_ptr );

// ---------------------

extern UNS_32 STATION_REPORT_DATA_fill_ptrs_and_return_how_many_lines( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0 );

extern void STATION_REPORT_draw_scroll_line( const INT_16 pline_index_0_i16 );

// ---------------------

extern STATION_REPORT_DATA_RECORD *nm_STATION_REPORT_DATA_get_previous_completed_record( STATION_REPORT_DATA_RECORD *psrdr_ptr );

extern STATION_REPORT_DATA_RECORD *nm_STATION_REPORT_DATA_get_most_recently_completed_record( void );

extern void nm_STATION_REPORT_DATA_close_and_start_a_new_record( STATION_REPORT_DATA_RECORD *psrdr_ptr, const DATE_TIME *pdate_time );


extern void STATION_REPORT_DATA_generate_lines_for_DEBUG( void );


// ---------------------

extern void STATION_REPORT_DATA_free_report_support( void );

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

