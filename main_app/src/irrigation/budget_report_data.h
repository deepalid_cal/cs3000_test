/*  file = budget_report_data.h                  02.22.2016  skc */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_BUDGET_REPORT_DATA_H
#define _INC_BUDGET_REPORT_DATA_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"
#include	"battery_backed_vars.h"

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4 years with 4 systems @ 12 reports per system per year
#define BUDGET_REPORT_RECORDS_TO_KEEP		(192)

// -----------------------------

// 3/30/2016 skc : This is what is sent in a budget report.
// We send each day at the meter_read_time (Noon). At the end of the budget period we
// stamp the closing_record_for_the_period to mark the end of the period.
typedef struct
{
	SYSTEM_BUDGET_REPORT_RECORD sbrr;

	BY_POC_BUDGET_RECORD bpbr[ MAX_POCS_IN_NETWORK ];

	UNS_32 record_date;

} BUDGET_REPORT_RECORD;

typedef struct
{
	REPORT_DATA_FILE_BASE_STRUCT	rdfb;

	// ----------
	
	BUDGET_REPORT_RECORD			brr[ BUDGET_REPORT_RECORDS_TO_KEEP ];

} COMPLETED_BUDGET_REPORT_RECORDS_STRUCT;

// Held in SDRAM and saved to a file in the GENERAL STORAGE flash device.
extern COMPLETED_BUDGET_REPORT_RECORDS_STRUCT	budget_report_data_completed;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// ---------------------

extern void init_file_budget_report_records( void );
extern void save_file_budget_report_records( void );

// ---------------------

extern BUDGET_REPORT_RECORD *nm_BUDGET_REPORT_RECORDS_get_previous_completed_record( BUDGET_REPORT_RECORD *pbrr_ptr );
extern BUDGET_REPORT_RECORD *nm_BUDGET_REPORT_RECORDS_get_most_recently_completed_record( void );

// ---------------------

extern UNS_32 FDTO_BUDGET_REPORT_fill_ptrs_and_return_how_many_lines( const UNS_32 plist_index );
extern void FDTO_BUDGET_REPORT_load_guivars_for_scroll_line( const INT_16 pline_index_0_i16 );

// ---------------------

extern void BUDGET_REPORT_DATA_start_the_ci_timer_if_it_is_not_running( void );

extern void nm_BUDGET_REPORT_DATA_inc_index( UNS_32 *pindex_ptr );

extern void nm_BUDGET_REPORT_DATA_close_and_start_a_new_record( BUDGET_REPORT_RECORD *pbrr_ptr );

extern void POC_BUDGET_DATA_generate_lines_for_DEBUG( void );

// ---------------------

extern void BUDGET_REPORT_free_report_support( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

