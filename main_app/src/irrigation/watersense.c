
/* ---------------------------------------------------------- */

#include	"watersense.h"

#include	"alerts.h"

#include	"station_groups.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/25/2015 ajv : Since there's no need to cycle and soak if the the soil intake rate is
// greater than the precip rate, set the minimum soak time to some arbitrary value. For
// now, we'll use 20 minutes.
// 
// 10/13/2015 ajv : We discovered that, as the precip rate approaches the intake rate,
// extremely high soak times can exist as well. After a discussion between David, Bob, Rick,
// and myself, we're going to change the this minimum soak time to 120-minutes which
// conincides with the longest soak-in time with our current ASA and IR values and a
// worst-case DU of 100% (clay with 0-3% slope).
#define	MINIMUM_SOAK_TIME_IF_INTAKE_RATE_GT_PRECIP_RATE		(120)

// 6/25/2015 ajv : Since there's no need to cycle and soak if the the soil intake rate is
// greater than the precip rate, set the max cycle time to some arbitrary value. For now,
// we'll use 60 minutes.
// 
// 10/13/2015 ajv : We discovered that, as the precip rate approaches the intake rate,
// extremely high cycle times can exist as well. For example, when setting a precip rate of
// 0.36 in/hr on a site that has loam, the cycle time calculates out to 1,800 minutes -
// something which is clearly beyond a reasonable value. Therefore, after a discussion
// between David, Bob, Rick, and myself, we're going to set the maximum cycle time to
// 300-minutes when either of these scenarios occurs. By the way, the maximum cycle time
// given the current ASA and IR values is 2,400-minutes (sandy loam with 0-3% slope).
// 
// Keep in mind, however, if the WaterSense test protocol parameters change, we may need to
// consider whether this is still a valid limitation. We're technically not violating
// WaterSense by doing this because it's a MAXIMUM cycle time and we're reducing it to
// something lower, but it may be unexpected.
#define	MAXIMUM_CYCLE_TIME_IF_INTAKE_RATE_GT_PRECIP_RATE	(300)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/27/2015 ajv : Allowable Surface Accumultion is free standing water created on top of
// the soil surface by application rates that exceed soil intake rates that is generally
// restrained from running off by the combined effects of surface detention and the presence
// of the crop canopy, thatch layer, or accumulated vegetative waste.
// 
// The following ASA values were retrieved from Table 3 of the WaterSense Specification for
// Weather-Based Irrigation Controllers, Version 1.0 (November 3, 2011)
const float ASA[ (STATION_GROUP_SOIL_TYPE_MAX + 1) ][ (STATION_GROUP_SLOPE_PERCENTAGE_MAX + 1) ] =
{
	// 		 SLOPE PERCENTAGE
	//	 0-3%	 4-6%	7-12%	13% <
	{	0.20F,	0.15F,	0.10F,	0.10F	},	// Clay
	{	0.23F,	0.19F,	0.16F,	0.13F	},	// Silty Clay
	{	0.26F,	0.22F,	0.18F,	0.15F	},	// Clay Loam
	{	0.30F,	0.25F,	0.21F,	0.17F	},	// Loam
	{	0.33F,	0.29F,	0.24F,	0.20F	},	// Sandy Loam
	{	0.36F,	0.30F,	0.26F,	0.22F	},	// Loamy Sand
	{	0.40F,	0.35F,	0.30F,	0.25F	}	// Sand
};

// ----------

// 3/13/2015 ajv : Estimated values for species factor used to determine the landscape
// coefficient of the specified vegetation type as specified in Table 2 of Estimating Water
// Requirements of Landscape Plantings leaflet.
const float Ks[ (STATION_GROUP_PLANT_TYPE_MAX + 1) ] =
{
	0.80F,	// Cool Season Turf
	0.60F,	// Warm Season Turf
	0.75F,	// Combined Turf
	0.50F,	// Annuals
	0.50F,	// Trees
	0.50F,	// Ground Cover
	0.70F,	// Shrubs - High Water Use
	0.50F,	// Shrubs - Medium Water Use
	0.20F,	// Shrubs - Low Water Use
	0.90F,	// Mixed - High Water Use
	0.50F,	// Mixed - Medimum Water Use
	0.20F,	// Mixed - Low Water Use
	0.50F,	// Native Shrubs / Trees
	0.70F	// Native Grasses
};

// ----------

// 3/13/2015 ajv : Estimated values for density factor used to determine the landscape
// coefficient of the specified vegetation type as specified in Table 2 of Estimating Water
// Requirements of Landscape Plantings leaflet.
const float Kd[ (STATION_GROUP_PLANT_TYPE_MAX + 1) ][ (STATION_GROUP_DENSITY_FACTOR_HIGH + 1) ] =
{
	//	Low				Average			High
	{	0.60F,			1.00F,			1.00F },	// Cool Season Turf
	{	0.60F,			1.00F,			1.00F },	// Warm Season Turf
	{	0.50F,			1.00F,			1.10F },	// Combined Turf
	{	0.50F,			1.00F,			1.10F },	// Annuals
	{	0.50F,			1.00F,			1.30F },	// Trees
	{	0.50F,			1.00F,			1.10F },	// Ground Cover
	{	0.50F,			1.00F,			1.10F },	// Shrubs - High Water Use
	{	0.50F,			1.00F,			1.10F },	// Shrubs - Medium Water Use
	{	0.50F,			1.00F,			1.10F },	// Shrubs - Low Water Use
	{	0.60F,			1.10F,			1.30F },	// Mixed - High Water Use
	{	0.60F,			1.10F,			1.30F },	// Mixed - Medimum Water Use
	{	0.60F,			1.10F,			1.30F },	// Mixed - Low Water Use
	{	0.50F,			1.00F,			1.10F },	// Native Shrubs / Trees
	{	0.60F,			1.00F,			1.00F }		// Native Grasses
};

// ----------

// 3/13/2015 ajv : Estimated values for microclimate factor used to determine the landscape
// coefficient of the specified vegetation type as specified in Table 2 of Estimating Water
// Requirements of Landscape Plantings leaflet.
const float Kmc[ (STATION_GROUP_PLANT_TYPE_MAX + 1) ][ (STATION_GROUP_MICROCLIMATE_FACTOR_HIGH + 1) ] =
{
	//	Low				Average			High
	{	0.80F,			1.00F,			1.20F },	// Cool Season Turf
	{	0.80F,			1.00F,			1.20F },	// Warm Season Turf
	{	0.80F,			1.00F,			1.20F },	// Combined Turf
	{	0.50F,			1.00F,			1.30F },	// Annuals
	{	0.50F,			1.00F,			1.40F },	// Trees
	{	0.50F,			1.00F,			1.20F },	// Ground Cover
	{	0.50F,			1.00F,			1.30F },	// Shrubs - High Water Use
	{	0.50F,			1.00F,			1.30F },	// Shrubs - Medium Water Use
	{	0.50F,			1.00F,			1.30F },	// Shrubs - Low Water Use
	{	0.50F,			1.00F,			1.40F },	// Mixed - High Water Use
	{	0.50F,			1.00F,			1.40F },	// Mixed - Medimum Water Use
	{	0.50F,			1.00F,			1.40F },	// Mixed - Low Water Use
	{	0.50F,			1.00F,			1.30F },	// Native Shrubs / Trees
	{	0.80F,			1.00F,			1.20F }		// Native Grasses
};

// ----------

// 3/13/2015 ajv : The active root zone is the soil depth from which a plant extracts most
// of its water needs.
// 
// Typical root zone depth as specified Table 2 of the WeatherTRAK Central Web User's Manual
// (September, 2013)
const float ROOT_ZONE_DEPTH[ (STATION_GROUP_PLANT_TYPE_MAX + 1) ][ (STATION_GROUP_SOIL_TYPE_MAX + 1) ] = 
{
	//  Clay		Silty Clay		Clay Loam		Loam		Sandy Loam		Loamy Sand		Sand
	{	 4.0F,		 4.0F,			 4.0F,			 6.0F,		 6.0F,			 6.0F,			 6.0F }, 	// Cool Season Turf
	{	 4.0F,		 4.0F,			 4.0F,			 6.0F,		 6.0F,			 6.0F,			 6.0F },	// Warm Season Turf
	{	 4.0F,		 4.0F,			 4.0F,			 6.0F,		 6.0F,			 6.0F,			 6.0F },	// Combined Turf
	{	 6.0F,		 6.0F,			 6.0F,			 6.0F,		 6.0F,			 6.0F,			 6.0F },	// Annuals
	{	24.0F,		24.0F,			24.0F,			24.0F,		24.0F,			24.0F,			24.0F },	// Trees
	{	 4.0F,		 4.0F,			 4.0F,			 6.0F,		 6.0F,			 6.0F,			 6.0F },	// Ground Cover
	{	12.0F,		12.0F,			12.0F,			12.0F,		12.0F,			12.0F,			12.0F },	// Shrubs - High Water Use
	{	12.0F,		12.0F,			12.0F,			12.0F,		12.0F,			12.0F,			12.0F },	// Shrubs - Medium Water Use
	{	12.0F,		12.0F,			12.0F,			12.0F,		12.0F,			12.0F,			12.0F },	// Shrubs - Low Water Use
	{	12.0F,		12.0F,			12.0F,			12.0F,		12.0F,			12.0F,			12.0F },	// Mixed - High Water Use
	{	12.0F,		12.0F,			12.0F,			12.0F,		12.0F,			12.0F,			12.0F },	// Mixed - Medium Water Use
	{	12.0F,		12.0F,			12.0F,			12.0F,		12.0F,			12.0F,			12.0F },	// Mixed - Low Water Use
	{	18.0F,		18.0F,			18.0F,			18.0F,		18.0F,			18.0F,			18.0F },	// Native Shrubs / Trees
	{	 6.0F,		 6.0F,			 6.0F,			 6.0F,		 6.0F,			 6.0F,			 6.0F }		// Native Grasses
};	

// ----------

// 3/13/2015 ajv : Mangmeent allowable depletion is the maximum amount of plant available
// water, expressed as a percent, that is allowed to be removed from the soil before an
// irrigation cycle occurs. According the the LIA Manual, 50 percent MAD represents a
// reasonable overalll value for the typical soils and plants used in landscape.
// 
// Management allowable depletion as specified in Table 8-1c of the Irrigation Association
// Landscape Irrigation Auditor Manual, 2nd Edition (2010):
// 
//  Coarse (Sand, fine sand, loam sand)								: 50%
//  Moderately coarse (Sandy loam)									: 50%
//  Medium (Loam, silty loam, silt)									: 50%
//  Moderately Fine (Silty clay loam, sandy clay loam, clay loam)	: 50%
//  Fine (Sandy clay, silty clay, clay) 							: 50%
// 
// Allowable depletion as specified below Table 1 of the WaterSense Specification for
// Weather-Based Irrigation Controllers, Version 1.0 (November 3, 2011)
const float MAD[ (STATION_GROUP_SOIL_TYPE_MAX + 1) ] =
{
	0.35F,	// Clay
	0.40F,	// Silty Clay
	0.50F,	// Clay Loam
	0.50F,	// Loam
	0.55F,	// Sandy Loam
	0.50F,	// Loamy Sand
	0.50F	// Sand	(estimated using IA LIA manual)
};

// ----------

// 3/13/2015 ajv : The available water in a soil is the amount of water stored between field
// capacity and the wilting point - the percentage of soil water present in the soil but
// unavailable to plants. 
// 
// Typical available water holding capacities as specified in Table 8-1b of the Irrigation
// Association Landscape Irrigation Auditor Manual, 2nd Edition (2010):
// 
//  Coarse (Sand, fine sand, loam sand)								: 0.06
//  Moderately coarse (Sandy loam)									: 0.11
//  Medium (Loam, silty loam, silt)									: 0.18
//  Moderately Fine (Silty clay loam, sandy clay loam, clay loam)	: 0.16
//  Fine (Sandy clay, silty clay, clay) 							: 0.14
// 
// Available water as specified below Table 1 of the WaterSense Specification for
// Weather-Based Irrigation Controllers, Version 1.0 (November 3, 2011)
const float AW[ (STATION_GROUP_SOIL_TYPE_MAX + 1) ] =
{
	0.17F,	// Clay
	0.17F,	// Silty Clay
	0.18F,	// Clay Loam
	0.17F,	// Loam
	0.13F,	// Sandy Loam
	0.09F,	// Loamy Sand
	0.06F	// Sand	(estimated using IA LIA manual)
};

// ----------

// 2/26/2015 ajv : Infiltration rate is a measure of how quickly water enters a soil. The
// basic intake rate is the rate at which water percolates into the soil after infiltration
// has slowed to a nearly constant rate.
// 
// Soil intake rates as specified in Table 3 of the WaterSense Specification for
// Weather-Based Irrigation Controllers, Version 1.0 (November 3, 2011)
const float IR[ (STATION_GROUP_SOIL_TYPE_MAX + 1) ] =
{
	0.10F,	// Clay
	0.15F,	// Silty Clay
	0.20F,	// Clay Loam
	0.35F,	// Loam
	0.40F,	// Sandy Loam
	0.50F,	// Loamy Sand
	0.60F	// Sand
};

// ----------

// 3/13/2015 ajv : Preciptation rates as specified by WeatherTRAK Site Profile Worksheet
const float PR[ (STATION_GROUP_HEAD_TYPE_MAX + 1) ] =
{
	1.70F,	// Spray Head
	0.75F,	// Spray Head, Stream
	0.50F,	// Spray Head, High Efficiency
	0.50F,	// Rotor, Full Circle
	1.00F,	// Rotor, Part Circle
	0.75F,	// Rotor, Mixed
	0.65F,	// Rotor, Stream
	0.50F,	// Impact, Full Circle
	1.00F,	// Impact, Part Circle
	0.75F,	// Impact, Mixed
	2.55F,	// Bubbler
	0.20F,	// Drip Emitter
	1.00F	// Subsurface Drip
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


// 2/27/2015 ajv : When functions are declared as static, the compiler may inline the
// functions and optimize out floating-point constant literals from the calculations. This
// can result in a division-by-0 error. In order to resolve this, we declare the constant
// literal as volatile which tells the compile to not optimize the value.
static const volatile float MINUTES_PER_HOUR = 60.0F;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern UNS_32 WATERSENSE_get_Kmc_index_based_on_exposure( const UNS_32 pexposure )
{
	UNS_32 lmicroclimate_factor_index;

	// 3/20/2015 ajv : According to the Estimating Water Requirements of Landscape Plantings:
	// The Landscape Coefficient Method, Kmc - the microclimate factor that affects the
	// landscape coefficient value - can vary based on a variety of factors. One of those
	// factors is exposure. Since our user selects an exposure, we're going to adjust the Kmc
	// value accordingly based on what they select. For now, we'll use HIGH for Full Sun, LOW
	// for Full Shade, and AVERAGE for everything else. The user can directly edit the Kl value
	// anyway. This just helps them get a decent value to start from.
	switch( pexposure )
	{
		case STATION_GROUP_EXPOSURE_FULL_SUN:
			lmicroclimate_factor_index = STATION_GROUP_MICROCLIMATE_FACTOR_HIGH;
			break;

		case STATION_GROUP_EXPOSURE_25_PCT_SHADE:
		case STATION_GROUP_EXPOSURE_50_PCT_SHADE:
		case STATION_GROUP_EXPOSURE_75_PCT_SHADE:
			lmicroclimate_factor_index = STATION_GROUP_MICROCLIMATE_FACTOR_AVERAGE;
			break;

		case STATION_GROUP_EXPOSURE_FULL_SHADE:
			lmicroclimate_factor_index = STATION_GROUP_MICROCLIMATE_FACTOR_LOW;
			break;

		default:
			lmicroclimate_factor_index = STATION_GROUP_MICROCLIMATE_FACTOR_AVERAGE;
	}

	return( lmicroclimate_factor_index );
}

/* ---------------------------------------------------------- */
/**
 * Calculate root zone working water storage.
 * 
 * @mutex_requirements (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param vegetation 
 * 
 * @param soil_type 
 * 
 * @return float Root zone working water storage (in.)
 *
 * @author 2/27/2015 AdrianusV
 *
 * @revisions (none)
 */
extern float WATERSENSE_get_RZWWS( const float allowable_depletion, const float available_water, const float root_zone_depth )
{
	float RZWWS;

	RZWWS = ( allowable_depletion * available_water * root_zone_depth );

	return( RZWWS );
}

/* ---------------------------------------------------------- */
/**
 * Calculate landscape coefficient.
 * 
 * @mutex_requirements (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param Ks Species factor
 * 
 * @param Kd Density factor
 * 
 * @param Kmc Microclimate factor
 * 
 * @return float Landscape coefficient
 *
 * @author 2/27/2015 AdrianusV
 *
 * @revisions (none)
 */
extern float WATERSENSE_get_Kl( const float pKs, const float pKd, const float pKmc )
{
	float	Kl;

	Kl = ( pKs * pKd * pKmc );

	return( Kl );
}

/* ---------------------------------------------------------- */
/**
 * Calculate rain. Allows for an arbritrary loss of 20% of the rainfall to non-uniformity
 * and runoff.
 * 
 * @mutex_requirements (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param R Gross amount of daily rainfall as reported (in.)
 * 
 * @param usable_rain
 * 
 * @return float Net amount of rainfall to be used in moisture balance calculation (in.)
 *
 * @author 2/27/2015 AdrianusV
 *
 * @revisions (none)
 */
static float WATERSENSE_get_Rn( const float R, const float usable_rain )
{
	float	Rn;

	Rn = (usable_rain * R);

	return( Rn );
}

/* ---------------------------------------------------------- */
/**
 * Calculate minumum soak time
 * 
 * @mutex_requirements (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param Da Design application (in.)
 * 
 * @param IR Basic soil intake rate (in./hr)
 * 
 * @return float Required minimum time between the start of consecutive irrigation cycles
 * (min.)
 *
 * @author 2/27/2015 AdrianusV
 *
 * @revisions (none)
 */
static float WATERSENSE_get_St( const float Da, const float IR )
{
	// 2/27/2015 ajv : When functions are declared as static, the compiler may inline the
	// functions and optimize out floating-point constant literals from the calculations. This
	// can result in a division-by-0 error. In order to resolve this, we declare the constant
	// literal as volatile which tells the compile to not optimize the value.
	const volatile float ONE_HUNDRED = 100.0F;

	float St;

	St = (((Da * MINUTES_PER_HOUR) / IR) / ONE_HUNDRED);

	return( St );
}

#if 0
/* ---------------------------------------------------------- */
/**
 * Calculate free water.
 * 
 * @mutex_requirements (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param Rt System runtime per cycle, min.
 * 
 * @param PR Precipitation rate (in./hr)
 * 
 * @param IR Basic soil intake rate (in./h)
 * 
 * @return float Free water, water applied that exceeds soil intake properties (in.)
 *
 * @author 2/27/2015 AdrianusV
 *
 * @revisions (none)
 */
static float WATERSENSE_get_Fw( const float Rt, const float PR, const float IR )
{
	float Fw;

	if( PR > IR )
	{
		Fw = ((Rt * (PR - IR)) / MINUTES_PER_HOUR );
	}
	else
	{
		// 7/8/2015 ajv : Since the soil intake rate is greater than the precip rate, there
		// is no possibility of free water.
		Fw = 0.0F;
	}

	return( Fw );
}
#endif

/* ---------------------------------------------------------- */
/**
 * Calculate maximum allowable runtime to avoid runoff.
 * 
 * @mutex_requirements (none) 
 *
 * @task_execution Executed within the context of the
 * 
 * @precondition The precipitation rate (PR) should be greater than the soil intake rate
 * (IR) when calling this function.
 * 
 * @param soil_type 
 * 
 * @param slope 
 * 
 * @param PR Precipitation rate (in./hr)
 * 
 * @return float Maximum allowable runtime to avoid runoff (min.)
 *
 * @author 2/27/2015 AdrianusV
 *
 * @revisions (none)
 */
static float WATERSENSE_get_Rt_max( const float ASA, const float IR, const float PR )
{
	float Rt_max;

	if( PR > IR )
	{
		Rt_max = ((MINUTES_PER_HOUR * ASA) / (PR - IR));
	}
	else
	{
		// 7/8/2015 ajv : We've already covered this case in each place this function is called.
		// However, in the event someone calls this function in the future without such a check,
		// we'll set a value.
		// 
		// 3/16/2015 ajv : The soil infiltration rate is greater than or equal to the precip rate.
		// What does this mean? Well, it means that the soil can accept the water faster than the
		// system can apply it. Therefore, there's no need to cycle and soak. So, we're going to set
		// the cycle time to some arbitrary value
		Rt_max = MAXIMUM_CYCLE_TIME_IF_INTAKE_RATE_GT_PRECIP_RATE;

		// 7/8/2015 ajv : This should never happen since we're supposed to perform the check before
		// calling this function.
		Alert_Message( "Rt Max - PR > IR" );
	}

	return( Rt_max );
}

/* ---------------------------------------------------------- */
/**
 * Calculate gross irrigation amount
 * 
 * @mutex_requirements (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param Rt System runtime per cycle (min.)
 * 
 * @param PR Precipitation rate (in./hr)
 * 
 * @return float Gross irrigation water applied (in.)
 *
 * @author 2/27/2015 AdrianusV
 *
 * @revisions (none)
 */
static float WATERSENSE_get_I( const float Rt, const float PR )
{
	float I;

	I = ((Rt * PR) / MINUTES_PER_HOUR);

	return( I );
}

/* ---------------------------------------------------------- */
/**
 * Calculate net irrigation.
 * 
 * @mutex_requirements (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param I Gross irrigation water applied (in.)
 * 
 * @param E Irrigations ystem application efficiency (%)
 * 
 * @return float Design application (in.)
 *
 * @author 2/27/2015 AdrianusV
 *
 * @revisions (none)
 */
static float WATERSENSE_get_Da( const float I, const float E )
{
	float Da;

	Da = (I * E);

	return( Da );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param exposure 
 * 
 * @param vegetation 
 * 
 * @param sprinkler_type
 * 
 * @param density_factor
 * 
 * @param microclimate_factor
 * 
 * @param ETo Reference crop evapotranspiration (in./d)
 * 
 * @param E Irrigation system application efficiency - also known as distribution
 * uniformity, or DU (%)
 * 
 * @author 2/27/2015 AdrianusV
 *
 * @revisions (none)
 */
extern float WATERSENSE_get_cycle_time( const float allowable_surface_accum, const float soil_intake_rate, const float precip_rate )
{
	float	Rt_max;

	if( precip_rate > soil_intake_rate )
	{
		Rt_max = WATERSENSE_get_Rt_max( allowable_surface_accum, soil_intake_rate, precip_rate );

		// ----------

		// 10/14/2015 ajv : We've made the decision to cap the maximum cycle time to something
		// reasonable for the user. See the comments associated with the define for more detail.
		if( Rt_max > MAXIMUM_CYCLE_TIME_IF_INTAKE_RATE_GT_PRECIP_RATE )
		{
			Rt_max = MAXIMUM_CYCLE_TIME_IF_INTAKE_RATE_GT_PRECIP_RATE;
		}
	}
	else
	{
		// 3/17/2015 ajv : Since there's no need to cycle and soak since the soil infiltration rate
		// is greater than the precip rate, set the maximum cycle time to some arbitrary value.
		Rt_max = MAXIMUM_CYCLE_TIME_IF_INTAKE_RATE_GT_PRECIP_RATE;
	}

	return( Rt_max );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the 
 * 
 * @param soil_texture 
 * 
 * @param slope 
 * 
 * @param PR Precipitation rate (in./hr)
 * 
 * @param E Irrigation system application efficiency (%)
 *
 * @author 2/27/2015 AdrianusV
 *
 * @revisions (none)
 */
extern float WATERSENSE_get_soak_time__cycle_end_to_cycle_start( const float allowable_surface_accum, const float soil_intake_rate, const float precip_rate, const float E )
{
	// Maximum allowable runtime to avoid runoff (min.)
	float Rt_max;

	// Maximum gross irrigation water applied (in.)
	float I_max;

	// Maximum design application (in.)
	float Da_max;

	// Required minimum time between the start of consecutive irrigation cycles (min.)
	float St;

	// Require minimum time between the end of an irrigation cycle and the start of the next
	// consecutive cycle (min.)
	float cycle_end_to_cycle_start_soak_time;

	// ----------

	if( precip_rate > soil_intake_rate )
	{
		Rt_max = WATERSENSE_get_Rt_max( allowable_surface_accum, soil_intake_rate, precip_rate );

		I_max = WATERSENSE_get_I( Rt_max, precip_rate );

		Da_max = WATERSENSE_get_Da( I_max, E );

		St = WATERSENSE_get_St( Da_max, soil_intake_rate );

		// ----------

		// 10/14/2015 ajv : Since we're technically soaking while we're cycling, we need to deduct
		// the cycle time from the calculated soak-in time to get the cycle-end to cycle-start
		// soak-in time. That's what we're going to return.
		cycle_end_to_cycle_start_soak_time = (St - Rt_max);
	}
	else
	{
		// 3/17/2015 ajv : Since there's no need to cycle and soak since the soil infiltration rate
		// is greater than the precip rate, set the minimum soak time to some arbitrary value.
		cycle_end_to_cycle_start_soak_time = MINIMUM_SOAK_TIME_IF_INTAKE_RATE_GT_PRECIP_RATE;
	}

	// ----------

	return( cycle_end_to_cycle_start_soak_time );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @task_execution Executed within the context of the 
 * 
 * @param Kl Crop (turf) or landscape coefficient
 * 
 * @param ETo Reference crop evapotranspiration (in./d)
 * 
 * @param MBo Beginning daily moisture balance (in.)
 */
extern float WATERSENSE_reduce_moisture_balance_by_ETc( const float Kl, const float ETo, const float MBo )
{
	// Daily calculation of root zone moisture balance (in.)
	float MB;

	// Turf of landscape moisture requirements (in./d)
	float ETc;

	// ----------

	ETc = (Kl * ETo);

	MB = MBo - ETc;

	// ----------

	return( MB );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @task_execution Executed within the context of the 
 * 
 * @param Rt System total runtime
 * 
 * @param E Irrigation system application efficiency (%)
 *
 * @param PR Precipitation rate (in./hr)
 * 
 * @param RZWWS Root zone working water storage (in.)
 *
 * @param MBo Beginning daily moisture balance (in.)
 */
extern float WATERSENSE_increase_moisture_balance_by_irrigation( const float Rt, const float E, const float PR, const float RZWWS, const float MBo )
{
	// Daily calculation of root zone moisture balance (in.)
	float MB;

	// Gross irrigation water applied (in.)
	float I;

	// Maximum amount of RZZWS available before allowing for rainfall strorage (in.)
	float I_max;

	// ----------

	// 3/20/2015 ajv : Note that "I" must be corrected for direct and soak runoff. It is also
	// limited to the maximum amount of RZWWS available BEFORE allowing for rainfall storage.
	// This is different from the original SWAT test which uses rain before the irrigation.

	// 6/30/2015 ajv : First, let's figure out how much room we have left based on our current
	// moisture balance and the RZWWS for this zone.
	I_max = RZWWS - MBo;

	// 6/30/2015 ajv : Then, calculate "I"
	I = WATERSENSE_get_I( Rt, PR );

	// 6/30/2015 ajv : Finally, adjust "I" to the maximum allowed if we've gone out the top of
	// the RZWWS.
	if( I > I_max )
	{
		I = I_max;
	}

	// ----------

	MB = MBo + (I * E);

	// ----------

	return( MB );
}

/* ---------------------------------------------------------- */
/**
 * 
 * 
 * @task_execution Executed within the context of the 
 * 
 * @param R Gross amount of daily rainfall (in.)
 * 
 * @param RZWWS Root zone working water storage (in.)
 *
 * @param MBo Beginning daily moisture balance (in.)
 */
extern float WATERSENSE_increase_moisture_balance_by_rain( const float R, const float RZWWS, const float usable_rain, float MBo )
{
	// Daily calculation of root zone moisture balance (in.)
	float MB;

	// Net amount of rainfall to be used in moisture balance calculation (in.)
	float Rn;

	// ----------

	// Maximum amount of RZZWS available for rain (in.)
	float R_max;

	// ----------

	// 3/20/2015 ajv : "R" is limited to the maximum amount of RZWWS available for rainfall storage.
	// 
	// 9/3/2015 ajv : UPDATE: We've learned that, while the documentation says "R", they
	// actually meant "Rn". This was learned by reviewing the WaterSense software used by the
	// certification bodies.
	Rn = WATERSENSE_get_Rn( R, usable_rain );

	// ----------

	// 3/14/2017 ajv : We've opted to back this change out as it can cause issues when users'
	// soil is dried out due to normal scheduling (e.g., every three-days) and a small amount of
	// rain falls. This shouldn't result in the MB exceeding 50%. We've already taken care of
	// the aforementioned deficit issues by snapping the MB to 50% at the conclusion of
	// irrigation.
	#if 0
	// 10/28/2016 ajv : If the beginning moisture balance is under 50% (for example due to
	// budgets, soil moisture, high flows, and so on), reset it back to 50% if we have
	// measurable rain BEFORE we apply the rain.
	// 
	if( (Rn > 0) && (MBo < STATION_MOISTURE_BALANCE_DEFAULT) )
	{
		MBo = STATION_MOISTURE_BALANCE_DEFAULT;
	}
	#endif

	// ----------

	// 6/30/2015 ajv : Let's figure out how much room we have left based on our current moisture
	// balance and the RZWWS for this zone.
	R_max = RZWWS - MBo;

	// 6/30/2015 ajv : Then, adjust "R" to the maximum allowed if we've gone out the top of
	// the RZWWS.
	if( Rn > R_max )
	{
		Rn = R_max;
	}

	// ----------

	MB = MBo + Rn;

	// ----------

	// 3/14/2017 ajv : We found an issue at Estrella Partnership Committee where, because the
	// Soil Moisture Capacity (RZWWS) was reduced, the moisture balance ended up greater than
	// 100%, which should not be possible. Therefore, force it back to 100% if it went over.
	if( MB > STATION_MOISTURE_BALANCE_MAX )
	{
		Alert_Message_va( "MB out of range (%0.2f); reset to %0.2f", MB, STATION_MOISTURE_BALANCE_DEFAULT );

		MB = STATION_MOISTURE_BALANCE_DEFAULT;
	}

	// ----------

	return( MB );
}

/* ---------------------------------------------------------- */
/**
 * @task_execution Executed within the context of the TD CHECK task at midnight each day to
 * verify that every Station Group is WaterSense compliant and generate an alert for each
 * group that is not.
 */
extern void WATERSENSE_verify_watersense_compliance( void )
{
	STATION_GROUP_STRUCT	*lgroup;

	UNS_32	i;

	// ----------

	for( i = 0; i < STATION_GROUP_get_num_groups_in_use(); ++i )
	{
		lgroup = STATION_GROUP_get_group_at_this_index( i );

		if( lgroup != NULL )
		{
			// 7/14/2015 ajv : We define being WaterSense compliant as a Station Group that is running
			// in daily ET AND using rain. However, we skip any groups which aren't enabled.
			if( (SCHEDULE_get_enabled(lgroup)) && ((!DAILY_ET_get_et_in_use(lgroup)) || (!RAIN_get_rain_in_use(lgroup)) ) )
			{
				Alert_group_not_watersense_compliant( nm_GROUP_get_name(lgroup) );
			}
		}
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

